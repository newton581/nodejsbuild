	.file	"csrsbcs.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711NGramParserD2Ev
	.type	_ZN6icu_6711NGramParserD2Ev, @function
_ZN6icu_6711NGramParserD2Ev:
.LFB2057:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2057:
	.size	_ZN6icu_6711NGramParserD2Ev, .-_ZN6icu_6711NGramParserD2Ev
	.globl	_ZN6icu_6711NGramParserD1Ev
	.set	_ZN6icu_6711NGramParserD1Ev,_ZN6icu_6711NGramParserD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711NGramParser8nextByteEPNS_9InputTextE
	.type	_ZN6icu_6711NGramParser8nextByteEPNS_9InputTextE, @function
_ZN6icu_6711NGramParser8nextByteEPNS_9InputTextE:
.LFB2063:
	.cfi_startproc
	endbr64
	movslq	32(%rdi), %rax
	cmpl	8(%rsi), %eax
	jge	.L5
	movq	(%rsi), %rdx
	leal	1(%rax), %ecx
	movl	%ecx, 32(%rdi)
	movzbl	(%rdx,%rax), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE2063:
	.size	_ZN6icu_6711NGramParser8nextByteEPNS_9InputTextE, .-_ZN6icu_6711NGramParser8nextByteEPNS_9InputTextE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718NGramParser_IBM420D2Ev
	.type	_ZN6icu_6718NGramParser_IBM420D2Ev, @function
_ZN6icu_6718NGramParser_IBM420D2Ev:
.LFB2070:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2070:
	.size	_ZN6icu_6718NGramParser_IBM420D2Ev, .-_ZN6icu_6718NGramParser_IBM420D2Ev
	.globl	_ZN6icu_6718NGramParser_IBM420D1Ev
	.set	_ZN6icu_6718NGramParser_IBM420D1Ev,_ZN6icu_6718NGramParser_IBM420D2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718NGramParser_IBM4208nextByteEPNS_9InputTextE
	.type	_ZN6icu_6718NGramParser_IBM4208nextByteEPNS_9InputTextE, @function
_ZN6icu_6718NGramParser_IBM4208nextByteEPNS_9InputTextE:
.LFB2074:
	.cfi_startproc
	endbr64
	movl	32(%rdi), %edx
	cmpl	8(%rsi), %edx
	jge	.L14
	movslq	%edx, %rcx
	addq	(%rsi), %rcx
	movzbl	(%rcx), %eax
	testb	%al, %al
	je	.L14
	leal	-178(%rax), %esi
	cmpl	$1, %esi
	jbe	.L15
	leal	-180(%rax), %esi
	cmpl	$1, %esi
	jbe	.L11
	subl	$184, %eax
	cmpl	$1, %eax
	jbe	.L12
	movl	$0, 48(%rdi)
	movzbl	(%rcx), %eax
	addl	$1, %edx
	leaq	_ZN6icu_67L17unshapeMap_IBM420E(%rip), %rcx
	movl	%edx, 32(%rdi)
	movzbl	(%rcx,%rax), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	addl	$1, %edx
	movl	$86, 48(%rdi)
	movl	$177, %eax
	movl	%edx, 32(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	addl	$1, %edx
	movl	$71, 48(%rdi)
	movl	$177, %eax
	movl	%edx, 32(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	addl	$1, %edx
	movl	$73, 48(%rdi)
	movl	$177, %eax
	movl	%edx, 32(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE2074:
	.size	_ZN6icu_6718NGramParser_IBM4208nextByteEPNS_9InputTextE, .-_ZN6icu_6718NGramParser_IBM4208nextByteEPNS_9InputTextE
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"ISO-8859-1"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6719CharsetRecog_8859_17getNameEv
	.type	_ZNK6icu_6719CharsetRecog_8859_17getNameEv, @function
_ZNK6icu_6719CharsetRecog_8859_17getNameEv:
.LFB2092:
	.cfi_startproc
	endbr64
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE2092:
	.size	_ZNK6icu_6719CharsetRecog_8859_17getNameEv, .-_ZNK6icu_6719CharsetRecog_8859_17getNameEv
	.section	.rodata.str1.1
.LC1:
	.string	"ISO-8859-2"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6719CharsetRecog_8859_27getNameEv
	.type	_ZNK6icu_6719CharsetRecog_8859_27getNameEv, @function
_ZNK6icu_6719CharsetRecog_8859_27getNameEv:
.LFB2098:
	.cfi_startproc
	endbr64
	leaq	.LC1(%rip), %rax
	ret
	.cfi_endproc
.LFE2098:
	.size	_ZNK6icu_6719CharsetRecog_8859_27getNameEv, .-_ZNK6icu_6719CharsetRecog_8859_27getNameEv
	.section	.rodata.str1.1
.LC2:
	.string	"ISO-8859-5"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6719CharsetRecog_8859_57getNameEv
	.type	_ZNK6icu_6719CharsetRecog_8859_57getNameEv, @function
_ZNK6icu_6719CharsetRecog_8859_57getNameEv:
.LFB2103:
	.cfi_startproc
	endbr64
	leaq	.LC2(%rip), %rax
	ret
	.cfi_endproc
.LFE2103:
	.size	_ZNK6icu_6719CharsetRecog_8859_57getNameEv, .-_ZNK6icu_6719CharsetRecog_8859_57getNameEv
	.section	.rodata.str1.1
.LC3:
	.string	"ru"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722CharsetRecog_8859_5_ru11getLanguageEv
	.type	_ZNK6icu_6722CharsetRecog_8859_5_ru11getLanguageEv, @function
_ZNK6icu_6722CharsetRecog_8859_5_ru11getLanguageEv:
.LFB2108:
	.cfi_startproc
	endbr64
	leaq	.LC3(%rip), %rax
	ret
	.cfi_endproc
.LFE2108:
	.size	_ZNK6icu_6722CharsetRecog_8859_5_ru11getLanguageEv, .-_ZNK6icu_6722CharsetRecog_8859_5_ru11getLanguageEv
	.globl	_ZNK6icu_6719CharsetRecog_KOI8_R11getLanguageEv
	.set	_ZNK6icu_6719CharsetRecog_KOI8_R11getLanguageEv,_ZNK6icu_6722CharsetRecog_8859_5_ru11getLanguageEv
	.globl	_ZNK6icu_6725CharsetRecog_windows_125111getLanguageEv
	.set	_ZNK6icu_6725CharsetRecog_windows_125111getLanguageEv,_ZNK6icu_6722CharsetRecog_8859_5_ru11getLanguageEv
	.section	.rodata.str1.1
.LC4:
	.string	"ISO-8859-6"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6719CharsetRecog_8859_67getNameEv
	.type	_ZNK6icu_6719CharsetRecog_8859_67getNameEv, @function
_ZNK6icu_6719CharsetRecog_8859_67getNameEv:
.LFB2114:
	.cfi_startproc
	endbr64
	leaq	.LC4(%rip), %rax
	ret
	.cfi_endproc
.LFE2114:
	.size	_ZNK6icu_6719CharsetRecog_8859_67getNameEv, .-_ZNK6icu_6719CharsetRecog_8859_67getNameEv
	.section	.rodata.str1.1
.LC5:
	.string	"ar"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722CharsetRecog_8859_6_ar11getLanguageEv
	.type	_ZNK6icu_6722CharsetRecog_8859_6_ar11getLanguageEv, @function
_ZNK6icu_6722CharsetRecog_8859_6_ar11getLanguageEv:
.LFB2119:
	.cfi_startproc
	endbr64
	leaq	.LC5(%rip), %rax
	ret
	.cfi_endproc
.LFE2119:
	.size	_ZNK6icu_6722CharsetRecog_8859_6_ar11getLanguageEv, .-_ZNK6icu_6722CharsetRecog_8859_6_ar11getLanguageEv
	.globl	_ZNK6icu_6722CharsetRecog_IBM420_ar11getLanguageEv
	.set	_ZNK6icu_6722CharsetRecog_IBM420_ar11getLanguageEv,_ZNK6icu_6722CharsetRecog_8859_6_ar11getLanguageEv
	.globl	_ZNK6icu_6725CharsetRecog_windows_125611getLanguageEv
	.set	_ZNK6icu_6725CharsetRecog_windows_125611getLanguageEv,_ZNK6icu_6722CharsetRecog_8859_6_ar11getLanguageEv
	.section	.rodata.str1.1
.LC6:
	.string	"ISO-8859-7"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6719CharsetRecog_8859_77getNameEv
	.type	_ZNK6icu_6719CharsetRecog_8859_77getNameEv, @function
_ZNK6icu_6719CharsetRecog_8859_77getNameEv:
.LFB2125:
	.cfi_startproc
	endbr64
	leaq	.LC6(%rip), %rax
	ret
	.cfi_endproc
.LFE2125:
	.size	_ZNK6icu_6719CharsetRecog_8859_77getNameEv, .-_ZNK6icu_6719CharsetRecog_8859_77getNameEv
	.section	.rodata.str1.1
.LC7:
	.string	"el"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722CharsetRecog_8859_7_el11getLanguageEv
	.type	_ZNK6icu_6722CharsetRecog_8859_7_el11getLanguageEv, @function
_ZNK6icu_6722CharsetRecog_8859_7_el11getLanguageEv:
.LFB2130:
	.cfi_startproc
	endbr64
	leaq	.LC7(%rip), %rax
	ret
	.cfi_endproc
.LFE2130:
	.size	_ZNK6icu_6722CharsetRecog_8859_7_el11getLanguageEv, .-_ZNK6icu_6722CharsetRecog_8859_7_el11getLanguageEv
	.section	.rodata.str1.1
.LC8:
	.string	"ISO-8859-8"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6719CharsetRecog_8859_87getNameEv
	.type	_ZNK6icu_6719CharsetRecog_8859_87getNameEv, @function
_ZNK6icu_6719CharsetRecog_8859_87getNameEv:
.LFB2136:
	.cfi_startproc
	endbr64
	leaq	.LC8(%rip), %rax
	ret
	.cfi_endproc
.LFE2136:
	.size	_ZNK6icu_6719CharsetRecog_8859_87getNameEv, .-_ZNK6icu_6719CharsetRecog_8859_87getNameEv
	.section	.rodata.str1.1
.LC9:
	.string	"ISO-8859-8-I"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6724CharsetRecog_8859_8_I_he7getNameEv
	.type	_ZNK6icu_6724CharsetRecog_8859_8_I_he7getNameEv, @function
_ZNK6icu_6724CharsetRecog_8859_8_I_he7getNameEv:
.LFB2141:
	.cfi_startproc
	endbr64
	leaq	.LC9(%rip), %rax
	ret
	.cfi_endproc
.LFE2141:
	.size	_ZNK6icu_6724CharsetRecog_8859_8_I_he7getNameEv, .-_ZNK6icu_6724CharsetRecog_8859_8_I_he7getNameEv
	.section	.rodata.str1.1
.LC10:
	.string	"he"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6724CharsetRecog_8859_8_I_he11getLanguageEv
	.type	_ZNK6icu_6724CharsetRecog_8859_8_I_he11getLanguageEv, @function
_ZNK6icu_6724CharsetRecog_8859_8_I_he11getLanguageEv:
.LFB2142:
	.cfi_startproc
	endbr64
	leaq	.LC10(%rip), %rax
	ret
	.cfi_endproc
.LFE2142:
	.size	_ZNK6icu_6724CharsetRecog_8859_8_I_he11getLanguageEv, .-_ZNK6icu_6724CharsetRecog_8859_8_I_he11getLanguageEv
	.globl	_ZNK6icu_6722CharsetRecog_IBM424_he11getLanguageEv
	.set	_ZNK6icu_6722CharsetRecog_IBM424_he11getLanguageEv,_ZNK6icu_6724CharsetRecog_8859_8_I_he11getLanguageEv
	.globl	_ZNK6icu_6722CharsetRecog_8859_8_he11getLanguageEv
	.set	_ZNK6icu_6722CharsetRecog_8859_8_he11getLanguageEv,_ZNK6icu_6724CharsetRecog_8859_8_I_he11getLanguageEv
	.section	.rodata.str1.1
.LC11:
	.string	"ISO-8859-9"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6719CharsetRecog_8859_97getNameEv
	.type	_ZNK6icu_6719CharsetRecog_8859_97getNameEv, @function
_ZNK6icu_6719CharsetRecog_8859_97getNameEv:
.LFB2154:
	.cfi_startproc
	endbr64
	leaq	.LC11(%rip), %rax
	ret
	.cfi_endproc
.LFE2154:
	.size	_ZNK6icu_6719CharsetRecog_8859_97getNameEv, .-_ZNK6icu_6719CharsetRecog_8859_97getNameEv
	.section	.rodata.str1.1
.LC12:
	.string	"tr"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722CharsetRecog_8859_9_tr11getLanguageEv
	.type	_ZNK6icu_6722CharsetRecog_8859_9_tr11getLanguageEv, @function
_ZNK6icu_6722CharsetRecog_8859_9_tr11getLanguageEv:
.LFB2159:
	.cfi_startproc
	endbr64
	leaq	.LC12(%rip), %rax
	ret
	.cfi_endproc
.LFE2159:
	.size	_ZNK6icu_6722CharsetRecog_8859_9_tr11getLanguageEv, .-_ZNK6icu_6722CharsetRecog_8859_9_tr11getLanguageEv
	.section	.rodata.str1.1
.LC13:
	.string	"windows-1256"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6725CharsetRecog_windows_12567getNameEv
	.type	_ZNK6icu_6725CharsetRecog_windows_12567getNameEv, @function
_ZNK6icu_6725CharsetRecog_windows_12567getNameEv:
.LFB2165:
	.cfi_startproc
	endbr64
	leaq	.LC13(%rip), %rax
	ret
	.cfi_endproc
.LFE2165:
	.size	_ZNK6icu_6725CharsetRecog_windows_12567getNameEv, .-_ZNK6icu_6725CharsetRecog_windows_12567getNameEv
	.section	.rodata.str1.1
.LC14:
	.string	"windows-1251"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6725CharsetRecog_windows_12517getNameEv
	.type	_ZNK6icu_6725CharsetRecog_windows_12517getNameEv, @function
_ZNK6icu_6725CharsetRecog_windows_12517getNameEv:
.LFB2172:
	.cfi_startproc
	endbr64
	leaq	.LC14(%rip), %rax
	ret
	.cfi_endproc
.LFE2172:
	.size	_ZNK6icu_6725CharsetRecog_windows_12517getNameEv, .-_ZNK6icu_6725CharsetRecog_windows_12517getNameEv
	.section	.rodata.str1.1
.LC15:
	.string	"KOI8-R"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6719CharsetRecog_KOI8_R7getNameEv
	.type	_ZNK6icu_6719CharsetRecog_KOI8_R7getNameEv, @function
_ZNK6icu_6719CharsetRecog_KOI8_R7getNameEv:
.LFB2179:
	.cfi_startproc
	endbr64
	leaq	.LC15(%rip), %rax
	ret
	.cfi_endproc
.LFE2179:
	.size	_ZNK6icu_6719CharsetRecog_KOI8_R7getNameEv, .-_ZNK6icu_6719CharsetRecog_KOI8_R7getNameEv
	.section	.rodata.str1.1
.LC16:
	.string	"IBM424_rtl"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6726CharsetRecog_IBM424_he_rtl7getNameEv
	.type	_ZNK6icu_6726CharsetRecog_IBM424_he_rtl7getNameEv, @function
_ZNK6icu_6726CharsetRecog_IBM424_he_rtl7getNameEv:
.LFB2191:
	.cfi_startproc
	endbr64
	leaq	.LC16(%rip), %rax
	ret
	.cfi_endproc
.LFE2191:
	.size	_ZNK6icu_6726CharsetRecog_IBM424_he_rtl7getNameEv, .-_ZNK6icu_6726CharsetRecog_IBM424_he_rtl7getNameEv
	.section	.rodata.str1.1
.LC17:
	.string	"IBM424_ltr"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6726CharsetRecog_IBM424_he_ltr7getNameEv
	.type	_ZNK6icu_6726CharsetRecog_IBM424_he_ltr7getNameEv, @function
_ZNK6icu_6726CharsetRecog_IBM424_he_ltr7getNameEv:
.LFB2197:
	.cfi_startproc
	endbr64
	leaq	.LC17(%rip), %rax
	ret
	.cfi_endproc
.LFE2197:
	.size	_ZNK6icu_6726CharsetRecog_IBM424_he_ltr7getNameEv, .-_ZNK6icu_6726CharsetRecog_IBM424_he_ltr7getNameEv
	.section	.rodata.str1.1
.LC18:
	.string	"IBM420_rtl"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6726CharsetRecog_IBM420_ar_rtl7getNameEv
	.type	_ZNK6icu_6726CharsetRecog_IBM420_ar_rtl7getNameEv, @function
_ZNK6icu_6726CharsetRecog_IBM420_ar_rtl7getNameEv:
.LFB2209:
	.cfi_startproc
	endbr64
	leaq	.LC18(%rip), %rax
	ret
	.cfi_endproc
.LFE2209:
	.size	_ZNK6icu_6726CharsetRecog_IBM420_ar_rtl7getNameEv, .-_ZNK6icu_6726CharsetRecog_IBM420_ar_rtl7getNameEv
	.section	.rodata.str1.1
.LC19:
	.string	"IBM420_ltr"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6726CharsetRecog_IBM420_ar_ltr7getNameEv
	.type	_ZNK6icu_6726CharsetRecog_IBM420_ar_ltr7getNameEv, @function
_ZNK6icu_6726CharsetRecog_IBM420_ar_ltr7getNameEv:
.LFB2215:
	.cfi_startproc
	endbr64
	leaq	.LC19(%rip), %rax
	ret
	.cfi_endproc
.LFE2215:
	.size	_ZNK6icu_6726CharsetRecog_IBM420_ar_ltr7getNameEv, .-_ZNK6icu_6726CharsetRecog_IBM420_ar_ltr7getNameEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711NGramParserD0Ev
	.type	_ZN6icu_6711NGramParserD0Ev, @function
_ZN6icu_6711NGramParserD0Ev:
.LFB2059:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2059:
	.size	_ZN6icu_6711NGramParserD0Ev, .-_ZN6icu_6711NGramParserD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718NGramParser_IBM420D0Ev
	.type	_ZN6icu_6718NGramParser_IBM420D0Ev, @function
_ZN6icu_6718NGramParser_IBM420D0Ev:
.LFB2072:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2072:
	.size	_ZN6icu_6718NGramParser_IBM420D0Ev, .-_ZN6icu_6718NGramParser_IBM420D0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719CharsetRecog_8859_1D2Ev
	.type	_ZN6icu_6719CharsetRecog_8859_1D2Ev, @function
_ZN6icu_6719CharsetRecog_8859_1D2Ev:
.LFB2088:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6717CharsetRecog_sbcsE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	.cfi_endproc
.LFE2088:
	.size	_ZN6icu_6719CharsetRecog_8859_1D2Ev, .-_ZN6icu_6719CharsetRecog_8859_1D2Ev
	.globl	_ZN6icu_6719CharsetRecog_8859_1D1Ev
	.set	_ZN6icu_6719CharsetRecog_8859_1D1Ev,_ZN6icu_6719CharsetRecog_8859_1D2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719CharsetRecog_8859_1D0Ev
	.type	_ZN6icu_6719CharsetRecog_8859_1D0Ev, @function
_ZN6icu_6719CharsetRecog_8859_1D0Ev:
.LFB2090:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6717CharsetRecog_sbcsE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2090:
	.size	_ZN6icu_6719CharsetRecog_8859_1D0Ev, .-_ZN6icu_6719CharsetRecog_8859_1D0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719CharsetRecog_8859_2D2Ev
	.type	_ZN6icu_6719CharsetRecog_8859_2D2Ev, @function
_ZN6icu_6719CharsetRecog_8859_2D2Ev:
.LFB2094:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6717CharsetRecog_sbcsE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	.cfi_endproc
.LFE2094:
	.size	_ZN6icu_6719CharsetRecog_8859_2D2Ev, .-_ZN6icu_6719CharsetRecog_8859_2D2Ev
	.globl	_ZN6icu_6719CharsetRecog_8859_2D1Ev
	.set	_ZN6icu_6719CharsetRecog_8859_2D1Ev,_ZN6icu_6719CharsetRecog_8859_2D2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719CharsetRecog_8859_2D0Ev
	.type	_ZN6icu_6719CharsetRecog_8859_2D0Ev, @function
_ZN6icu_6719CharsetRecog_8859_2D0Ev:
.LFB2096:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6717CharsetRecog_sbcsE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2096:
	.size	_ZN6icu_6719CharsetRecog_8859_2D0Ev, .-_ZN6icu_6719CharsetRecog_8859_2D0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725CharsetRecog_windows_1256D2Ev
	.type	_ZN6icu_6725CharsetRecog_windows_1256D2Ev, @function
_ZN6icu_6725CharsetRecog_windows_1256D2Ev:
.LFB2162:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6717CharsetRecog_sbcsE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	.cfi_endproc
.LFE2162:
	.size	_ZN6icu_6725CharsetRecog_windows_1256D2Ev, .-_ZN6icu_6725CharsetRecog_windows_1256D2Ev
	.globl	_ZN6icu_6725CharsetRecog_windows_1256D1Ev
	.set	_ZN6icu_6725CharsetRecog_windows_1256D1Ev,_ZN6icu_6725CharsetRecog_windows_1256D2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725CharsetRecog_windows_1256D0Ev
	.type	_ZN6icu_6725CharsetRecog_windows_1256D0Ev, @function
_ZN6icu_6725CharsetRecog_windows_1256D0Ev:
.LFB2164:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6717CharsetRecog_sbcsE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2164:
	.size	_ZN6icu_6725CharsetRecog_windows_1256D0Ev, .-_ZN6icu_6725CharsetRecog_windows_1256D0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725CharsetRecog_windows_1251D2Ev
	.type	_ZN6icu_6725CharsetRecog_windows_1251D2Ev, @function
_ZN6icu_6725CharsetRecog_windows_1251D2Ev:
.LFB2169:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6717CharsetRecog_sbcsE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	.cfi_endproc
.LFE2169:
	.size	_ZN6icu_6725CharsetRecog_windows_1251D2Ev, .-_ZN6icu_6725CharsetRecog_windows_1251D2Ev
	.globl	_ZN6icu_6725CharsetRecog_windows_1251D1Ev
	.set	_ZN6icu_6725CharsetRecog_windows_1251D1Ev,_ZN6icu_6725CharsetRecog_windows_1251D2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725CharsetRecog_windows_1251D0Ev
	.type	_ZN6icu_6725CharsetRecog_windows_1251D0Ev, @function
_ZN6icu_6725CharsetRecog_windows_1251D0Ev:
.LFB2171:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6717CharsetRecog_sbcsE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2171:
	.size	_ZN6icu_6725CharsetRecog_windows_1251D0Ev, .-_ZN6icu_6725CharsetRecog_windows_1251D0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719CharsetRecog_KOI8_RD2Ev
	.type	_ZN6icu_6719CharsetRecog_KOI8_RD2Ev, @function
_ZN6icu_6719CharsetRecog_KOI8_RD2Ev:
.LFB2176:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6717CharsetRecog_sbcsE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	.cfi_endproc
.LFE2176:
	.size	_ZN6icu_6719CharsetRecog_KOI8_RD2Ev, .-_ZN6icu_6719CharsetRecog_KOI8_RD2Ev
	.globl	_ZN6icu_6719CharsetRecog_KOI8_RD1Ev
	.set	_ZN6icu_6719CharsetRecog_KOI8_RD1Ev,_ZN6icu_6719CharsetRecog_KOI8_RD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719CharsetRecog_KOI8_RD0Ev
	.type	_ZN6icu_6719CharsetRecog_KOI8_RD0Ev, @function
_ZN6icu_6719CharsetRecog_KOI8_RD0Ev:
.LFB2178:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6717CharsetRecog_sbcsE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2178:
	.size	_ZN6icu_6719CharsetRecog_KOI8_RD0Ev, .-_ZN6icu_6719CharsetRecog_KOI8_RD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722CharsetRecog_8859_5_ruD2Ev
	.type	_ZN6icu_6722CharsetRecog_8859_5_ruD2Ev, @function
_ZN6icu_6722CharsetRecog_8859_5_ruD2Ev:
.LFB2105:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6717CharsetRecog_sbcsE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	.cfi_endproc
.LFE2105:
	.size	_ZN6icu_6722CharsetRecog_8859_5_ruD2Ev, .-_ZN6icu_6722CharsetRecog_8859_5_ruD2Ev
	.globl	_ZN6icu_6722CharsetRecog_8859_5_ruD1Ev
	.set	_ZN6icu_6722CharsetRecog_8859_5_ruD1Ev,_ZN6icu_6722CharsetRecog_8859_5_ruD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722CharsetRecog_8859_5_ruD0Ev
	.type	_ZN6icu_6722CharsetRecog_8859_5_ruD0Ev, @function
_ZN6icu_6722CharsetRecog_8859_5_ruD0Ev:
.LFB2107:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6717CharsetRecog_sbcsE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2107:
	.size	_ZN6icu_6722CharsetRecog_8859_5_ruD0Ev, .-_ZN6icu_6722CharsetRecog_8859_5_ruD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722CharsetRecog_8859_6_arD2Ev
	.type	_ZN6icu_6722CharsetRecog_8859_6_arD2Ev, @function
_ZN6icu_6722CharsetRecog_8859_6_arD2Ev:
.LFB2116:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6717CharsetRecog_sbcsE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	.cfi_endproc
.LFE2116:
	.size	_ZN6icu_6722CharsetRecog_8859_6_arD2Ev, .-_ZN6icu_6722CharsetRecog_8859_6_arD2Ev
	.globl	_ZN6icu_6722CharsetRecog_8859_6_arD1Ev
	.set	_ZN6icu_6722CharsetRecog_8859_6_arD1Ev,_ZN6icu_6722CharsetRecog_8859_6_arD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722CharsetRecog_8859_6_arD0Ev
	.type	_ZN6icu_6722CharsetRecog_8859_6_arD0Ev, @function
_ZN6icu_6722CharsetRecog_8859_6_arD0Ev:
.LFB2118:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6717CharsetRecog_sbcsE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2118:
	.size	_ZN6icu_6722CharsetRecog_8859_6_arD0Ev, .-_ZN6icu_6722CharsetRecog_8859_6_arD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722CharsetRecog_8859_7_elD2Ev
	.type	_ZN6icu_6722CharsetRecog_8859_7_elD2Ev, @function
_ZN6icu_6722CharsetRecog_8859_7_elD2Ev:
.LFB2127:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6717CharsetRecog_sbcsE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	.cfi_endproc
.LFE2127:
	.size	_ZN6icu_6722CharsetRecog_8859_7_elD2Ev, .-_ZN6icu_6722CharsetRecog_8859_7_elD2Ev
	.globl	_ZN6icu_6722CharsetRecog_8859_7_elD1Ev
	.set	_ZN6icu_6722CharsetRecog_8859_7_elD1Ev,_ZN6icu_6722CharsetRecog_8859_7_elD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722CharsetRecog_8859_7_elD0Ev
	.type	_ZN6icu_6722CharsetRecog_8859_7_elD0Ev, @function
_ZN6icu_6722CharsetRecog_8859_7_elD0Ev:
.LFB2129:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6717CharsetRecog_sbcsE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2129:
	.size	_ZN6icu_6722CharsetRecog_8859_7_elD0Ev, .-_ZN6icu_6722CharsetRecog_8859_7_elD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724CharsetRecog_8859_8_I_heD2Ev
	.type	_ZN6icu_6724CharsetRecog_8859_8_I_heD2Ev, @function
_ZN6icu_6724CharsetRecog_8859_8_I_heD2Ev:
.LFB2138:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6717CharsetRecog_sbcsE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	.cfi_endproc
.LFE2138:
	.size	_ZN6icu_6724CharsetRecog_8859_8_I_heD2Ev, .-_ZN6icu_6724CharsetRecog_8859_8_I_heD2Ev
	.globl	_ZN6icu_6724CharsetRecog_8859_8_I_heD1Ev
	.set	_ZN6icu_6724CharsetRecog_8859_8_I_heD1Ev,_ZN6icu_6724CharsetRecog_8859_8_I_heD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724CharsetRecog_8859_8_I_heD0Ev
	.type	_ZN6icu_6724CharsetRecog_8859_8_I_heD0Ev, @function
_ZN6icu_6724CharsetRecog_8859_8_I_heD0Ev:
.LFB2140:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6717CharsetRecog_sbcsE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2140:
	.size	_ZN6icu_6724CharsetRecog_8859_8_I_heD0Ev, .-_ZN6icu_6724CharsetRecog_8859_8_I_heD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722CharsetRecog_8859_8_heD2Ev
	.type	_ZN6icu_6722CharsetRecog_8859_8_heD2Ev, @function
_ZN6icu_6722CharsetRecog_8859_8_heD2Ev:
.LFB2145:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6717CharsetRecog_sbcsE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	.cfi_endproc
.LFE2145:
	.size	_ZN6icu_6722CharsetRecog_8859_8_heD2Ev, .-_ZN6icu_6722CharsetRecog_8859_8_heD2Ev
	.globl	_ZN6icu_6722CharsetRecog_8859_8_heD1Ev
	.set	_ZN6icu_6722CharsetRecog_8859_8_heD1Ev,_ZN6icu_6722CharsetRecog_8859_8_heD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722CharsetRecog_8859_8_heD0Ev
	.type	_ZN6icu_6722CharsetRecog_8859_8_heD0Ev, @function
_ZN6icu_6722CharsetRecog_8859_8_heD0Ev:
.LFB2147:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6717CharsetRecog_sbcsE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2147:
	.size	_ZN6icu_6722CharsetRecog_8859_8_heD0Ev, .-_ZN6icu_6722CharsetRecog_8859_8_heD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722CharsetRecog_8859_9_trD2Ev
	.type	_ZN6icu_6722CharsetRecog_8859_9_trD2Ev, @function
_ZN6icu_6722CharsetRecog_8859_9_trD2Ev:
.LFB2156:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6717CharsetRecog_sbcsE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	.cfi_endproc
.LFE2156:
	.size	_ZN6icu_6722CharsetRecog_8859_9_trD2Ev, .-_ZN6icu_6722CharsetRecog_8859_9_trD2Ev
	.globl	_ZN6icu_6722CharsetRecog_8859_9_trD1Ev
	.set	_ZN6icu_6722CharsetRecog_8859_9_trD1Ev,_ZN6icu_6722CharsetRecog_8859_9_trD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722CharsetRecog_8859_9_trD0Ev
	.type	_ZN6icu_6722CharsetRecog_8859_9_trD0Ev, @function
_ZN6icu_6722CharsetRecog_8859_9_trD0Ev:
.LFB2158:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6717CharsetRecog_sbcsE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2158:
	.size	_ZN6icu_6722CharsetRecog_8859_9_trD0Ev, .-_ZN6icu_6722CharsetRecog_8859_9_trD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6726CharsetRecog_IBM424_he_rtlD2Ev
	.type	_ZN6icu_6726CharsetRecog_IBM424_he_rtlD2Ev, @function
_ZN6icu_6726CharsetRecog_IBM424_he_rtlD2Ev:
.LFB2188:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6717CharsetRecog_sbcsE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	.cfi_endproc
.LFE2188:
	.size	_ZN6icu_6726CharsetRecog_IBM424_he_rtlD2Ev, .-_ZN6icu_6726CharsetRecog_IBM424_he_rtlD2Ev
	.globl	_ZN6icu_6726CharsetRecog_IBM424_he_rtlD1Ev
	.set	_ZN6icu_6726CharsetRecog_IBM424_he_rtlD1Ev,_ZN6icu_6726CharsetRecog_IBM424_he_rtlD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6726CharsetRecog_IBM424_he_rtlD0Ev
	.type	_ZN6icu_6726CharsetRecog_IBM424_he_rtlD0Ev, @function
_ZN6icu_6726CharsetRecog_IBM424_he_rtlD0Ev:
.LFB2190:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6717CharsetRecog_sbcsE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2190:
	.size	_ZN6icu_6726CharsetRecog_IBM424_he_rtlD0Ev, .-_ZN6icu_6726CharsetRecog_IBM424_he_rtlD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6726CharsetRecog_IBM424_he_ltrD2Ev
	.type	_ZN6icu_6726CharsetRecog_IBM424_he_ltrD2Ev, @function
_ZN6icu_6726CharsetRecog_IBM424_he_ltrD2Ev:
.LFB2194:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6717CharsetRecog_sbcsE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	.cfi_endproc
.LFE2194:
	.size	_ZN6icu_6726CharsetRecog_IBM424_he_ltrD2Ev, .-_ZN6icu_6726CharsetRecog_IBM424_he_ltrD2Ev
	.globl	_ZN6icu_6726CharsetRecog_IBM424_he_ltrD1Ev
	.set	_ZN6icu_6726CharsetRecog_IBM424_he_ltrD1Ev,_ZN6icu_6726CharsetRecog_IBM424_he_ltrD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6726CharsetRecog_IBM424_he_ltrD0Ev
	.type	_ZN6icu_6726CharsetRecog_IBM424_he_ltrD0Ev, @function
_ZN6icu_6726CharsetRecog_IBM424_he_ltrD0Ev:
.LFB2196:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6717CharsetRecog_sbcsE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2196:
	.size	_ZN6icu_6726CharsetRecog_IBM424_he_ltrD0Ev, .-_ZN6icu_6726CharsetRecog_IBM424_he_ltrD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6726CharsetRecog_IBM420_ar_rtlD2Ev
	.type	_ZN6icu_6726CharsetRecog_IBM420_ar_rtlD2Ev, @function
_ZN6icu_6726CharsetRecog_IBM420_ar_rtlD2Ev:
.LFB2206:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6717CharsetRecog_sbcsE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	.cfi_endproc
.LFE2206:
	.size	_ZN6icu_6726CharsetRecog_IBM420_ar_rtlD2Ev, .-_ZN6icu_6726CharsetRecog_IBM420_ar_rtlD2Ev
	.globl	_ZN6icu_6726CharsetRecog_IBM420_ar_rtlD1Ev
	.set	_ZN6icu_6726CharsetRecog_IBM420_ar_rtlD1Ev,_ZN6icu_6726CharsetRecog_IBM420_ar_rtlD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6726CharsetRecog_IBM420_ar_rtlD0Ev
	.type	_ZN6icu_6726CharsetRecog_IBM420_ar_rtlD0Ev, @function
_ZN6icu_6726CharsetRecog_IBM420_ar_rtlD0Ev:
.LFB2208:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6717CharsetRecog_sbcsE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2208:
	.size	_ZN6icu_6726CharsetRecog_IBM420_ar_rtlD0Ev, .-_ZN6icu_6726CharsetRecog_IBM420_ar_rtlD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6726CharsetRecog_IBM420_ar_ltrD2Ev
	.type	_ZN6icu_6726CharsetRecog_IBM420_ar_ltrD2Ev, @function
_ZN6icu_6726CharsetRecog_IBM420_ar_ltrD2Ev:
.LFB2212:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6717CharsetRecog_sbcsE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	.cfi_endproc
.LFE2212:
	.size	_ZN6icu_6726CharsetRecog_IBM420_ar_ltrD2Ev, .-_ZN6icu_6726CharsetRecog_IBM420_ar_ltrD2Ev
	.globl	_ZN6icu_6726CharsetRecog_IBM420_ar_ltrD1Ev
	.set	_ZN6icu_6726CharsetRecog_IBM420_ar_ltrD1Ev,_ZN6icu_6726CharsetRecog_IBM420_ar_ltrD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6726CharsetRecog_IBM420_ar_ltrD0Ev
	.type	_ZN6icu_6726CharsetRecog_IBM420_ar_ltrD0Ev, @function
_ZN6icu_6726CharsetRecog_IBM420_ar_ltrD0Ev:
.LFB2214:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6717CharsetRecog_sbcsE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2214:
	.size	_ZN6icu_6726CharsetRecog_IBM420_ar_ltrD0Ev, .-_ZN6icu_6726CharsetRecog_IBM420_ar_ltrD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711NGramParserC2EPKiPKh
	.type	_ZN6icu_6711NGramParserC2EPKiPKh, @function
_ZN6icu_6711NGramParserC2EPKiPKh:
.LFB2054:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711NGramParserE(%rip), %rax
	movl	$0, 8(%rdi)
	movq	%rax, (%rdi)
	movq	%rsi, 16(%rdi)
	movq	%rdx, 40(%rdi)
	movq	$0, 24(%rdi)
	movl	$0, 32(%rdi)
	ret
	.cfi_endproc
.LFE2054:
	.size	_ZN6icu_6711NGramParserC2EPKiPKh, .-_ZN6icu_6711NGramParserC2EPKiPKh
	.globl	_ZN6icu_6711NGramParserC1EPKiPKh
	.set	_ZN6icu_6711NGramParserC1EPKiPKh,_ZN6icu_6711NGramParserC2EPKiPKh
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711NGramParser6searchEPKii
	.type	_ZN6icu_6711NGramParser6searchEPKii, @function
_ZN6icu_6711NGramParser6searchEPKii:
.LFB2060:
	.cfi_startproc
	endbr64
	movl	$224, %eax
	movl	$96, %edi
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$16, %ecx
	cmpl	%edx, 128(%rsi)
	movl	$16, %r8d
	movl	$192, %r10d
	movl	$32, %r9d
	cmovle	%rax, %rdi
	movl	$48, %eax
	movl	$160, %r11d
	cmovle	%rax, %rcx
	cmovg	%r8d, %eax
	movl	$64, %r8d
	cmovle	%r10, %r8
	movl	$0, %r10d
	cmovg	%r9, %r11
	cmovle	%r9, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movl	$0, %r9d
	movl	$32, %ebx
	cmovle	%ebx, %r9d
	cmpl	%edx, (%rsi,%r8)
	cmovg	%r11, %rdi
	cmovg	%r10, %rcx
	cmovg	%r9d, %eax
	cmpl	%edx, (%rsi,%rdi)
	jg	.L87
	addl	$8, %eax
	movslq	%eax, %rcx
.L87:
	cmpl	%edx, 16(%rsi,%rcx,4)
	jg	.L88
	addl	$4, %eax
	movslq	%eax, %rcx
.L88:
	cmpl	%edx, 8(%rsi,%rcx,4)
	jg	.L89
	addl	$2, %eax
	movslq	%eax, %rcx
.L89:
	cmpl	%edx, 4(%rsi,%rcx,4)
	jg	.L90
	leal	1(%rax), %r8d
	movslq	%r8d, %rcx
	movl	(%rsi,%rcx,4), %ecx
	cmpl	%ecx, %edx
	jl	.L91
.L92:
	cmpl	%ecx, %edx
	movl	$-1, %eax
	cmovne	%eax, %r8d
.L84:
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L90:
	.cfi_restore_state
	movl	(%rsi,%rcx,4), %ecx
	cmpl	%edx, %ecx
	jle	.L95
	subl	$1, %eax
	cmpl	$-1, %eax
	je	.L96
.L91:
	movslq	%eax, %rcx
	movl	%eax, %r8d
	movl	(%rsi,%rcx,4), %ecx
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L95:
	movl	%eax, %r8d
	jmp	.L92
.L96:
	movl	$-1, %r8d
	jmp	.L84
	.cfi_endproc
.LFE2060:
	.size	_ZN6icu_6711NGramParser6searchEPKii, .-_ZN6icu_6711NGramParser6searchEPKii
	.section	.rodata.str1.1
.LC20:
	.string	"windows-1252"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6719CharsetRecog_8859_15matchEPNS_9InputTextEPNS_12CharsetMatchE
	.type	_ZNK6icu_6719CharsetRecog_8859_15matchEPNS_9InputTextEPNS_12CharsetMatchE, @function
_ZNK6icu_6719CharsetRecog_8859_15matchEPNS_9InputTextEPNS_12CharsetMatchE:
.LFB2091:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	_ZN6icu_67L13ngrams_8859_1E(%rip), %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -160(%rbp)
	leaq	.LC20(%rip), %rdx
	movq	%rdi, -120(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 24(%rsi)
	leaq	.LC0(%rip), %rax
	movl	$-1, -124(%rbp)
	cmovne	%rdx, %rax
	movq	%rax, -152(%rbp)
	.p2align 4,,10
	.p2align 3
.L112:
	movq	256(%r15), %rax
	leaq	_ZNK6icu_6717CharsetRecog_sbcs10match_sbcsEPNS_9InputTextEPKiPKh(%rip), %rsi
	movq	%rax, -136(%rbp)
	movq	-120(%rbp), %rax
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L100
	movl	$0, -104(%rbp)
	xorl	%r14d, %r14d
	movl	8(%r13), %r9d
	xorl	%edx, %edx
	leaq	16+_ZTVN6icu_6711NGramParserE(%rip), %rax
	movq	%r15, -96(%rbp)
	movq	%r15, %rsi
	xorl	%ebx, %ebx
	movq	%rax, -112(%rbp)
	xorl	%r11d, %r11d
	leaq	-112(%rbp), %rdi
	movl	%r14d, %r15d
	leaq	_ZN6icu_67L14charMap_8859_1E(%rip), %r8
	movq	$0, -88(%rbp)
	movq	%r8, -72(%rbp)
	movl	$0, -80(%rbp)
.L108:
	movslq	%ebx, %rax
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L105:
	cmpl	%r9d, %ebx
	jge	.L128
	movq	0(%r13), %rcx
	addl	$1, %ebx
	movzbl	(%rcx,%rax), %ecx
	addq	$1, %rax
	movzbl	(%r8,%rcx), %r12d
	movl	$1, %ecx
	testb	%r12b, %r12b
	je	.L105
	movl	%ebx, -80(%rbp)
	cmpb	$32, %r12b
	jne	.L117
	testb	%r11b, %r11b
	je	.L117
.L106:
	cmpb	$32, %r12b
	sete	%r11b
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L128:
	movl	%r14d, %r12d
	movl	%r15d, %r14d
	movq	%rsi, %r15
	testb	%cl, %cl
	je	.L102
	movl	%ebx, -80(%rbp)
.L102:
	sall	$8, %edx
	addl	$1, %r12d
	movq	%r15, %rsi
	movl	$98, %ebx
	addl	$32, %edx
	movl	%r12d, -88(%rbp)
	andl	$16777215, %edx
	movl	%edx, -104(%rbp)
	call	_ZN6icu_6711NGramParser6searchEPKii
	pxor	%xmm0, %xmm0
	pxor	%xmm1, %xmm1
	cmpl	$-2147483648, %eax
	cvtsi2sdl	%r12d, %xmm1
	adcl	$0, %r14d
	cvtsi2sdl	%r14d, %xmm0
	divsd	%xmm1, %xmm0
	comisd	.LC21(%rip), %xmm0
	ja	.L110
	mulsd	.LC22(%rip), %xmm0
	cvttsd2sil	%xmm0, %ebx
.L110:
	cmpl	-124(%rbp), %ebx
	jg	.L129
.L111:
	addq	$264, %r15
	leaq	2640+_ZN6icu_67L13ngrams_8859_1E(%rip), %rax
	cmpq	%r15, %rax
	jne	.L112
	movl	-124(%rbp), %eax
	testl	%eax, %eax
	setg	%al
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L130
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L129:
	.cfi_restore_state
	movq	-136(%rbp), %r9
	movq	-120(%rbp), %rdx
	movl	%ebx, %ecx
	movq	%r13, %rsi
	movq	-152(%rbp), %r8
	movq	-160(%rbp), %rdi
	call	_ZN6icu_6712CharsetMatch3setEPNS_9InputTextEPKNS_17CharsetRecognizerEiPKcS7_@PLT
	movl	%ebx, -124(%rbp)
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L117:
	sall	$8, %edx
	movzbl	%r12b, %eax
	addl	$1, %r14d
	movl	%r9d, -128(%rbp)
	addl	%eax, %edx
	movq	%rdi, -144(%rbp)
	andl	$16777215, %edx
	movl	%r14d, -88(%rbp)
	movl	%edx, -104(%rbp)
	call	_ZN6icu_6711NGramParser6searchEPKii
	movq	-144(%rbp), %rdi
	movl	-128(%rbp), %r9d
	leaq	_ZN6icu_67L14charMap_8859_1E(%rip), %r8
	testl	%eax, %eax
	js	.L106
	addl	$1, %r15d
	movl	%r15d, -84(%rbp)
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L100:
	movq	-120(%rbp), %rdi
	leaq	_ZN6icu_67L14charMap_8859_1E(%rip), %rcx
	movq	%r15, %rdx
	movq	%r13, %rsi
	call	*%rax
	movl	%eax, %ebx
	jmp	.L110
.L130:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2091:
	.size	_ZNK6icu_6719CharsetRecog_8859_15matchEPNS_9InputTextEPNS_12CharsetMatchE, .-_ZNK6icu_6719CharsetRecog_8859_15matchEPNS_9InputTextEPNS_12CharsetMatchE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722CharsetRecog_IBM420_ar10match_sbcsEPNS_9InputTextEPKiPKh
	.type	_ZNK6icu_6722CharsetRecog_IBM420_ar10match_sbcsEPNS_9InputTextEPKiPKh, @function
_ZNK6icu_6722CharsetRecog_IBM420_ar10match_sbcsEPNS_9InputTextEPKiPKh:
.LFB2204:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	movq	%rdx, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	8(%rax), %edi
	movq	%fs:40, %rbx
	movq	%rbx, -56(%rbp)
	xorl	%ebx, %ebx
	leaq	16+_ZTVN6icu_6718NGramParser_IBM420E(%rip), %rbx
	movl	$0, -104(%rbp)
	movq	%rdx, -96(%rbp)
	movq	%rcx, -72(%rbp)
	movq	$0, -88(%rbp)
	movl	$0, -80(%rbp)
	movq	%rbx, -112(%rbp)
	movl	$0, -64(%rbp)
	movl	%edi, -116(%rbp)
	testl	%edi, %edi
	jle	.L132
	xorl	%r12d, %r12d
	movq	(%rax), %r11
	movq	%rcx, %rax
	movl	$1, %r14d
	movl	%r12d, %ecx
	xorl	%r9d, %r9d
	xorl	%edx, %edx
	xorl	%r10d, %r10d
	leaq	-112(%rbp), %rdi
	movq	%rax, %r12
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L182:
	leaq	_ZN6icu_67L17unshapeMap_IBM420E(%rip), %rbx
	cltq
	movl	%r14d, -80(%rbp)
	movl	%r14d, %r8d
	movzbl	(%rbx,%rax), %eax
	movl	$0, -64(%rbp)
	leal	1(%r9), %ebx
	movzbl	(%r12,%rax), %r13d
	movl	%edx, %eax
	sall	$8, %eax
	testb	%r13b, %r13b
	je	.L141
	xorl	%r15d, %r15d
.L150:
	cmpb	$32, %r13b
	jne	.L160
.L183:
	testb	%r10b, %r10b
	je	.L160
.L144:
	cmpb	$32, %r13b
	sete	%r10b
	testl	%r15d, %r15d
	je	.L141
.L143:
	movzbl	(%r12,%r15), %r13d
	testb	%r13b, %r13b
	je	.L141
	cmpb	$32, %r13b
	jne	.L161
	testb	%r10b, %r10b
	je	.L161
.L146:
	cmpb	$32, %r13b
	sete	%r10b
.L141:
	addq	$1, %r11
	addl	$1, %r14d
	cmpl	-116(%rbp), %r8d
	jge	.L179
.L133:
	movzbl	(%r11), %eax
	testb	%al, %al
	je	.L180
	leal	-178(%rax), %r8d
	cmpl	$1, %r8d
	jbe	.L181
	leal	-180(%rax), %r8d
	cmpl	$1, %r8d
	jbe	.L139
	leal	-184(%rax), %r8d
	cmpl	$1, %r8d
	ja	.L182
	movl	$86, -64(%rbp)
	movl	$86, %r15d
.L142:
	movl	%edx, %eax
	movl	%r14d, -80(%rbp)
	movl	%r14d, %r8d
	leal	1(%r9), %ebx
	movzbl	177(%r12), %r13d
	sall	$8, %eax
	testb	%r13b, %r13b
	je	.L143
	cmpb	$32, %r13b
	je	.L183
	.p2align 4,,10
	.p2align 3
.L160:
	movzbl	%r13b, %edx
	movq	%r11, -144(%rbp)
	addl	%eax, %edx
	movl	%ecx, -132(%rbp)
	andl	$16777215, %edx
	movl	%r8d, -120(%rbp)
	movq	%rdi, -128(%rbp)
	movl	%edx, -104(%rbp)
	movl	%ebx, -88(%rbp)
	call	_ZN6icu_6711NGramParser6searchEPKii
	movq	-128(%rbp), %rdi
	movl	-120(%rbp), %r8d
	leal	1(%rbx), %r10d
	movl	%eax, %r9d
	movl	%edx, %eax
	movl	-132(%rbp), %ecx
	movq	-144(%rbp), %r11
	sall	$8, %eax
	testl	%r9d, %r9d
	js	.L156
	addl	$1, %ecx
	movl	%ebx, %r9d
	movl	%r10d, %ebx
	movl	%ecx, -84(%rbp)
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L139:
	movl	$73, -64(%rbp)
	movl	$73, %r15d
	jmp	.L142
	.p2align 4,,10
	.p2align 3
.L181:
	movl	%edx, %eax
	movl	$71, -64(%rbp)
	movl	%r14d, %r8d
	leal	1(%r9), %ebx
	movzbl	177(%r12), %r13d
	movl	%r14d, -80(%rbp)
	sall	$8, %eax
	movl	$71, %r15d
	testb	%r13b, %r13b
	je	.L143
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L161:
	movzbl	%r13b, %edx
	movq	%r11, -144(%rbp)
	addl	%eax, %edx
	movl	%ecx, -132(%rbp)
	andl	$16777215, %edx
	movl	%r8d, -120(%rbp)
	movq	%rdi, -128(%rbp)
	movl	%edx, -104(%rbp)
	movl	%ebx, -88(%rbp)
	call	_ZN6icu_6711NGramParser6searchEPKii
	movq	-128(%rbp), %rdi
	movl	-120(%rbp), %r8d
	leal	1(%rbx), %r10d
	movl	%eax, %r9d
	movl	%edx, %eax
	movl	-132(%rbp), %ecx
	movq	-144(%rbp), %r11
	sall	$8, %eax
	testl	%r9d, %r9d
	js	.L157
	addl	$1, %ecx
	movl	%ebx, %r9d
	movl	%r10d, %ebx
	movl	%ecx, -84(%rbp)
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L156:
	movl	%ebx, %r9d
	movl	%r10d, %ebx
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L157:
	movl	%ebx, %r9d
	movl	%r10d, %ebx
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L180:
	sall	$8, %edx
	movl	%ecx, %r12d
	leal	1(%r9), %ebx
	addl	$32, %edx
	andl	$16777215, %edx
.L134:
	movl	%edx, -104(%rbp)
	movl	%ebx, -88(%rbp)
	call	_ZN6icu_6711NGramParser6searchEPKii
	testl	%eax, %eax
	js	.L148
.L151:
	addl	$1, %r12d
.L148:
	pxor	%xmm0, %xmm0
	pxor	%xmm1, %xmm1
	movl	$98, %eax
	cvtsi2sdl	%r12d, %xmm0
	cvtsi2sdl	%ebx, %xmm1
	divsd	%xmm1, %xmm0
	comisd	.LC21(%rip), %xmm0
	ja	.L131
.L152:
	mulsd	.LC22(%rip), %xmm0
	cvttsd2sil	%xmm0, %eax
.L131:
	movq	-56(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L184
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L179:
	.cfi_restore_state
	leal	32(%rax), %edx
	movl	%ecx, %r12d
	andl	$16777215, %edx
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L132:
	leaq	-112(%rbp), %rdi
	movl	$32, %edx
	xorl	%r12d, %r12d
	movl	$32, -104(%rbp)
	movl	$1, -88(%rbp)
	movl	$1, %ebx
	call	_ZN6icu_6711NGramParser6searchEPKii
	pxor	%xmm0, %xmm0
	testl	%eax, %eax
	jns	.L151
	jmp	.L152
.L184:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2204:
	.size	_ZNK6icu_6722CharsetRecog_IBM420_ar10match_sbcsEPNS_9InputTextEPKiPKh, .-_ZNK6icu_6722CharsetRecog_IBM420_ar10match_sbcsEPNS_9InputTextEPKiPKh
	.section	.rodata.str1.1
.LC24:
	.string	"windows-1250"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6719CharsetRecog_8859_25matchEPNS_9InputTextEPNS_12CharsetMatchE
	.type	_ZNK6icu_6719CharsetRecog_8859_25matchEPNS_9InputTextEPNS_12CharsetMatchE, @function
_ZNK6icu_6719CharsetRecog_8859_25matchEPNS_9InputTextEPNS_12CharsetMatchE:
.LFB2097:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	_ZN6icu_67L13ngrams_8859_2E(%rip), %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -152(%rbp)
	leaq	.LC24(%rip), %rdx
	movq	%rdi, -128(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 24(%rsi)
	leaq	.LC1(%rip), %rax
	movl	$-1, -116(%rbp)
	cmovne	%rdx, %rax
	movq	%rax, -144(%rbp)
.L199:
	movq	256(%r15), %rax
	leaq	_ZNK6icu_6717CharsetRecog_sbcs10match_sbcsEPNS_9InputTextEPKiPKh(%rip), %rsi
	movq	%rax, -136(%rbp)
	movq	-128(%rbp), %rax
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L187
	movl	$0, -104(%rbp)
	xorl	%r14d, %r14d
	movl	8(%r13), %r9d
	xorl	%edx, %edx
	leaq	16+_ZTVN6icu_6711NGramParserE(%rip), %rax
	movq	%r15, -96(%rbp)
	movq	%r15, %rsi
	xorl	%ebx, %ebx
	movq	%rax, -112(%rbp)
	xorl	%r11d, %r11d
	leaq	-112(%rbp), %rdi
	movl	%r14d, %r15d
	leaq	_ZN6icu_67L14charMap_8859_2E(%rip), %r8
	movq	$0, -88(%rbp)
	movq	%r8, -72(%rbp)
	movl	$0, -80(%rbp)
.L195:
	movslq	%ebx, %rax
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L192:
	cmpl	%r9d, %ebx
	jge	.L215
	movq	0(%r13), %rcx
	addl	$1, %ebx
	movzbl	(%rcx,%rax), %ecx
	addq	$1, %rax
	movzbl	(%r8,%rcx), %r12d
	movl	$1, %ecx
	testb	%r12b, %r12b
	je	.L192
	movl	%ebx, -80(%rbp)
	cmpb	$32, %r12b
	jne	.L204
	testb	%r11b, %r11b
	je	.L204
.L193:
	cmpb	$32, %r12b
	sete	%r11b
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L215:
	movl	%r14d, %r12d
	movl	%r15d, %r14d
	movq	%rsi, %r15
	testb	%cl, %cl
	je	.L189
	movl	%ebx, -80(%rbp)
.L189:
	sall	$8, %edx
	addl	$1, %r12d
	movq	%r15, %rsi
	movl	$98, %ebx
	addl	$32, %edx
	movl	%r12d, -88(%rbp)
	andl	$16777215, %edx
	movl	%edx, -104(%rbp)
	call	_ZN6icu_6711NGramParser6searchEPKii
	pxor	%xmm0, %xmm0
	pxor	%xmm1, %xmm1
	cmpl	$-2147483648, %eax
	cvtsi2sdl	%r12d, %xmm1
	adcl	$0, %r14d
	cvtsi2sdl	%r14d, %xmm0
	divsd	%xmm1, %xmm0
	comisd	.LC21(%rip), %xmm0
	ja	.L197
	mulsd	.LC22(%rip), %xmm0
	cvttsd2sil	%xmm0, %ebx
.L197:
	cmpl	-116(%rbp), %ebx
	jg	.L216
.L198:
	addq	$264, %r15
	leaq	1056+_ZN6icu_67L13ngrams_8859_2E(%rip), %rax
	cmpq	%r15, %rax
	jne	.L199
	movl	-116(%rbp), %eax
	testl	%eax, %eax
	setg	%al
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L217
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L216:
	.cfi_restore_state
	movq	-136(%rbp), %r9
	movq	-128(%rbp), %rdx
	movl	%ebx, %ecx
	movq	%r13, %rsi
	movq	-144(%rbp), %r8
	movq	-152(%rbp), %rdi
	call	_ZN6icu_6712CharsetMatch3setEPNS_9InputTextEPKNS_17CharsetRecognizerEiPKcS7_@PLT
	movl	%ebx, -116(%rbp)
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L204:
	sall	$8, %edx
	movzbl	%r12b, %eax
	addl	$1, %r14d
	movl	%r9d, -120(%rbp)
	addl	%eax, %edx
	movq	%rdi, -160(%rbp)
	andl	$16777215, %edx
	movl	%r14d, -88(%rbp)
	movl	%edx, -104(%rbp)
	call	_ZN6icu_6711NGramParser6searchEPKii
	movq	-160(%rbp), %rdi
	movl	-120(%rbp), %r9d
	leaq	_ZN6icu_67L14charMap_8859_2E(%rip), %r8
	testl	%eax, %eax
	js	.L193
	addl	$1, %r15d
	movl	%r15d, -84(%rbp)
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L187:
	movq	-128(%rbp), %rdi
	leaq	_ZN6icu_67L14charMap_8859_2E(%rip), %rcx
	movq	%r15, %rdx
	movq	%r13, %rsi
	call	*%rax
	movl	%eax, %ebx
	jmp	.L197
.L217:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2097:
	.size	_ZNK6icu_6719CharsetRecog_8859_25matchEPNS_9InputTextEPNS_12CharsetMatchE, .-_ZNK6icu_6719CharsetRecog_8859_25matchEPNS_9InputTextEPNS_12CharsetMatchE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6726CharsetRecog_IBM420_ar_rtl5matchEPNS_9InputTextEPNS_12CharsetMatchE
	.type	_ZNK6icu_6726CharsetRecog_IBM420_ar_rtl5matchEPNS_9InputTextEPNS_12CharsetMatchE, @function
_ZNK6icu_6726CharsetRecog_IBM420_ar_rtl5matchEPNS_9InputTextEPNS_12CharsetMatchE:
.LFB2210:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -120(%rbp)
	leaq	_ZNK6icu_6722CharsetRecog_IBM420_ar10match_sbcsEPNS_9InputTextEPKiPKh(%rip), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L219
	leaq	16+_ZTVN6icu_6718NGramParser_IBM420E(%rip), %rax
	leaq	_ZN6icu_67L20ngrams_IBM420_ar_rtlE(%rip), %rsi
	movl	$0, -104(%rbp)
	movq	%rax, -112(%rbp)
	movl	8(%r14), %eax
	leaq	_ZN6icu_67L17charMap_IBM420_arE(%rip), %r11
	movq	%rsi, -96(%rbp)
	movq	%r11, -72(%rbp)
	movq	$0, -88(%rbp)
	movl	$0, -80(%rbp)
	movl	$0, -64(%rbp)
	testl	%eax, %eax
	jle	.L220
	movq	(%r14), %rdi
	subl	$1, %eax
	xorl	%ebx, %ebx
	xorl	%r9d, %r9d
	addq	$2, %rax
	movq	%r15, -160(%rbp)
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	movq	%rdi, -144(%rbp)
	movl	$1, %r13d
	leaq	-112(%rbp), %rdi
	movl	%ebx, %r15d
	movq	%r14, -168(%rbp)
	movl	%r9d, %r14d
	movq	%rax, -152(%rbp)
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L266:
	cltq
	leaq	_ZN6icu_67L17unshapeMap_IBM420E(%rip), %rbx
	movl	$0, -64(%rbp)
	movzbl	(%rbx,%rax), %eax
	movl	%r13d, -80(%rbp)
	movzbl	(%r11,%rax), %eax
	testb	%al, %al
	jne	.L228
.L233:
	movl	%edx, %eax
	leal	1(%r14), %ebx
	sall	$8, %eax
.L229:
	addq	$1, %r13
	cmpq	-152(%rbp), %r13
	je	.L263
.L221:
	movq	-144(%rbp), %rax
	movzbl	-1(%rax,%r13), %eax
	testb	%al, %al
	je	.L264
	leal	-178(%rax), %ebx
	movl	%r13d, %ecx
	cmpl	$1, %ebx
	jbe	.L265
	leal	-180(%rax), %ebx
	cmpl	$1, %ebx
	jbe	.L226
	leal	-184(%rax), %ebx
	cmpl	$1, %ebx
	ja	.L266
	movl	$86, -64(%rbp)
	movl	$86, %r12d
.L230:
	sall	$8, %edx
	movl	%ecx, -80(%rbp)
	leal	1(%r14), %ecx
	addl	$177, %edx
	movl	%ecx, -88(%rbp)
	andl	$16777215, %edx
	movl	%ecx, -132(%rbp)
	movq	%rdi, -128(%rbp)
	movl	%edx, -104(%rbp)
	call	_ZN6icu_6711NGramParser6searchEPKii
	xorl	%r8d, %r8d
	movq	-128(%rbp), %rdi
	movl	-132(%rbp), %ecx
	testl	%eax, %eax
	leaq	_ZN6icu_67L17charMap_IBM420_arE(%rip), %r11
	js	.L267
.L231:
	addl	$1, %r15d
	movl	%ecx, %r14d
	movl	%r15d, -84(%rbp)
.L234:
	movl	%edx, %eax
	leal	1(%rcx), %ebx
	sall	$8, %eax
	testl	%r12d, %r12d
	je	.L229
.L242:
	movzbl	(%r11,%r12), %r12d
	testb	%r12b, %r12b
	je	.L229
	cmpb	$32, %r12b
	jne	.L249
	testb	%r8b, %r8b
	je	.L249
.L235:
	cmpb	$32, %r12b
	sete	%r8b
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L265:
	movl	$71, -64(%rbp)
	movl	$71, %r12d
	xorl	%r8d, %r8d
	movl	$177, %eax
	movl	%r13d, -80(%rbp)
.L225:
	sall	$8, %edx
	leal	1(%r14), %ecx
	movb	%r8b, -133(%rbp)
	addl	%eax, %edx
	movl	%ecx, -88(%rbp)
	andl	$16777215, %edx
	movl	%ecx, -132(%rbp)
	movq	%rdi, -128(%rbp)
	movl	%edx, -104(%rbp)
	call	_ZN6icu_6711NGramParser6searchEPKii
	movl	-132(%rbp), %ecx
	movq	-128(%rbp), %rdi
	leaq	_ZN6icu_67L17charMap_IBM420_arE(%rip), %r11
	testl	%eax, %eax
	movzbl	-133(%rbp), %r8d
	movl	%ecx, %r14d
	jns	.L231
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L228:
	cmpb	$32, %al
	movl	%r8d, %ecx
	setne	%bl
	xorl	$1, %ecx
	cmpb	$32, %al
	sete	%r8b
	orb	%cl, %bl
	je	.L233
	xorl	%r12d, %r12d
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L226:
	movl	$73, -64(%rbp)
	movl	$73, %r12d
	jmp	.L230
	.p2align 4,,10
	.p2align 3
.L249:
	movzbl	%r12b, %edx
	movq	%rdi, -128(%rbp)
	addl	%eax, %edx
	movl	%ebx, -88(%rbp)
	andl	$16777215, %edx
	movl	%edx, -104(%rbp)
	call	_ZN6icu_6711NGramParser6searchEPKii
	movq	-128(%rbp), %rdi
	leal	1(%rbx), %ecx
	leaq	_ZN6icu_67L17charMap_IBM420_arE(%rip), %r11
	movl	%eax, %r8d
	movl	%edx, %eax
	sall	$8, %eax
	testl	%r8d, %r8d
	js	.L246
	addl	$1, %r15d
	movl	%ebx, %r14d
	movl	%ecx, %ebx
	movl	%r15d, -84(%rbp)
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L246:
	movl	%ebx, %r14d
	movl	%ecx, %ebx
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L267:
	movl	%edx, %eax
	leal	2(%r14), %ebx
	movl	%ecx, %r14d
	sall	$8, %eax
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L219:
	leaq	_ZN6icu_67L17charMap_IBM420_arE(%rip), %rcx
	leaq	_ZN6icu_67L20ngrams_IBM420_ar_rtlE(%rip), %rdx
	call	*%rax
	testl	%eax, %eax
	movl	%eax, %ecx
	setg	%r12b
.L239:
	movq	-120(%rbp), %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	_ZN6icu_6712CharsetMatch3setEPNS_9InputTextEPKNS_17CharsetRecognizerEiPKcS7_@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L268
	addq	$136, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L264:
	.cfi_restore_state
	sall	$8, %edx
	movl	%r14d, %r9d
	movl	%r15d, %ebx
	movq	-168(%rbp), %r14
	addl	$32, %edx
	movq	-160(%rbp), %r15
	leal	1(%r9), %r12d
	andl	$16777215, %edx
.L222:
	leaq	_ZN6icu_67L20ngrams_IBM420_ar_rtlE(%rip), %rsi
	movl	%edx, -104(%rbp)
	movl	%r12d, -88(%rbp)
	call	_ZN6icu_6711NGramParser6searchEPKii
	testl	%eax, %eax
	js	.L237
.L240:
	addl	$1, %ebx
	movl	%ebx, -84(%rbp)
.L237:
	pxor	%xmm0, %xmm0
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm0
	cvtsi2sdl	%r12d, %xmm1
	divsd	%xmm1, %xmm0
	comisd	.LC21(%rip), %xmm0
	ja	.L247
.L241:
	mulsd	.LC22(%rip), %xmm0
	cvttsd2sil	%xmm0, %ecx
	testl	%ecx, %ecx
	setg	%r12b
	jmp	.L239
	.p2align 4,,10
	.p2align 3
.L247:
	movl	$1, %r12d
	movl	$98, %ecx
	jmp	.L239
	.p2align 4,,10
	.p2align 3
.L263:
	leal	32(%rax), %edx
	movl	%ebx, %r12d
	movq	-168(%rbp), %r14
	movl	%r15d, %ebx
	andl	$16777215, %edx
	movq	-160(%rbp), %r15
	jmp	.L222
.L220:
	leaq	-112(%rbp), %rdi
	movl	$32, %edx
	movl	$32, -104(%rbp)
	xorl	%ebx, %ebx
	movl	$1, -88(%rbp)
	movl	$1, %r12d
	call	_ZN6icu_6711NGramParser6searchEPKii
	pxor	%xmm0, %xmm0
	testl	%eax, %eax
	jns	.L240
	jmp	.L241
.L268:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2210:
	.size	_ZNK6icu_6726CharsetRecog_IBM420_ar_rtl5matchEPNS_9InputTextEPNS_12CharsetMatchE, .-_ZNK6icu_6726CharsetRecog_IBM420_ar_rtl5matchEPNS_9InputTextEPNS_12CharsetMatchE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6726CharsetRecog_IBM420_ar_ltr5matchEPNS_9InputTextEPNS_12CharsetMatchE
	.type	_ZNK6icu_6726CharsetRecog_IBM420_ar_ltr5matchEPNS_9InputTextEPNS_12CharsetMatchE, @function
_ZNK6icu_6726CharsetRecog_IBM420_ar_ltr5matchEPNS_9InputTextEPNS_12CharsetMatchE:
.LFB2216:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -120(%rbp)
	leaq	_ZNK6icu_6722CharsetRecog_IBM420_ar10match_sbcsEPNS_9InputTextEPKiPKh(%rip), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L270
	leaq	16+_ZTVN6icu_6718NGramParser_IBM420E(%rip), %rax
	leaq	_ZN6icu_67L20ngrams_IBM420_ar_ltrE(%rip), %rsi
	movl	$0, -104(%rbp)
	movq	%rax, -112(%rbp)
	movl	8(%r14), %eax
	leaq	_ZN6icu_67L17charMap_IBM420_arE(%rip), %r11
	movq	%rsi, -96(%rbp)
	movq	%r11, -72(%rbp)
	movq	$0, -88(%rbp)
	movl	$0, -80(%rbp)
	movl	$0, -64(%rbp)
	testl	%eax, %eax
	jle	.L271
	movq	(%r14), %rdi
	subl	$1, %eax
	xorl	%ebx, %ebx
	xorl	%r9d, %r9d
	addq	$2, %rax
	movq	%r15, -160(%rbp)
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	movq	%rdi, -144(%rbp)
	movl	$1, %r13d
	leaq	-112(%rbp), %rdi
	movl	%ebx, %r15d
	movq	%r14, -168(%rbp)
	movl	%r9d, %r14d
	movq	%rax, -152(%rbp)
	jmp	.L272
	.p2align 4,,10
	.p2align 3
.L317:
	cltq
	leaq	_ZN6icu_67L17unshapeMap_IBM420E(%rip), %rbx
	movl	$0, -64(%rbp)
	movzbl	(%rbx,%rax), %eax
	movl	%r13d, -80(%rbp)
	movzbl	(%r11,%rax), %eax
	testb	%al, %al
	jne	.L279
.L284:
	movl	%edx, %eax
	leal	1(%r14), %ebx
	sall	$8, %eax
.L280:
	addq	$1, %r13
	cmpq	-152(%rbp), %r13
	je	.L314
.L272:
	movq	-144(%rbp), %rax
	movzbl	-1(%rax,%r13), %eax
	testb	%al, %al
	je	.L315
	leal	-178(%rax), %ebx
	movl	%r13d, %ecx
	cmpl	$1, %ebx
	jbe	.L316
	leal	-180(%rax), %ebx
	cmpl	$1, %ebx
	jbe	.L277
	leal	-184(%rax), %ebx
	cmpl	$1, %ebx
	ja	.L317
	movl	$86, -64(%rbp)
	movl	$86, %r12d
.L281:
	sall	$8, %edx
	movl	%ecx, -80(%rbp)
	leal	1(%r14), %ecx
	addl	$177, %edx
	movl	%ecx, -88(%rbp)
	andl	$16777215, %edx
	movl	%ecx, -132(%rbp)
	movq	%rdi, -128(%rbp)
	movl	%edx, -104(%rbp)
	call	_ZN6icu_6711NGramParser6searchEPKii
	xorl	%r8d, %r8d
	movq	-128(%rbp), %rdi
	movl	-132(%rbp), %ecx
	testl	%eax, %eax
	leaq	_ZN6icu_67L17charMap_IBM420_arE(%rip), %r11
	js	.L318
.L282:
	addl	$1, %r15d
	movl	%ecx, %r14d
	movl	%r15d, -84(%rbp)
.L285:
	movl	%edx, %eax
	leal	1(%rcx), %ebx
	sall	$8, %eax
	testl	%r12d, %r12d
	je	.L280
.L293:
	movzbl	(%r11,%r12), %r12d
	testb	%r12b, %r12b
	je	.L280
	cmpb	$32, %r12b
	jne	.L300
	testb	%r8b, %r8b
	je	.L300
.L286:
	cmpb	$32, %r12b
	sete	%r8b
	jmp	.L280
	.p2align 4,,10
	.p2align 3
.L316:
	movl	$71, -64(%rbp)
	movl	$71, %r12d
	xorl	%r8d, %r8d
	movl	$177, %eax
	movl	%r13d, -80(%rbp)
.L276:
	sall	$8, %edx
	leal	1(%r14), %ecx
	movb	%r8b, -133(%rbp)
	addl	%eax, %edx
	movl	%ecx, -88(%rbp)
	andl	$16777215, %edx
	movl	%ecx, -132(%rbp)
	movq	%rdi, -128(%rbp)
	movl	%edx, -104(%rbp)
	call	_ZN6icu_6711NGramParser6searchEPKii
	movl	-132(%rbp), %ecx
	movq	-128(%rbp), %rdi
	leaq	_ZN6icu_67L17charMap_IBM420_arE(%rip), %r11
	testl	%eax, %eax
	movzbl	-133(%rbp), %r8d
	movl	%ecx, %r14d
	jns	.L282
	jmp	.L285
	.p2align 4,,10
	.p2align 3
.L279:
	cmpb	$32, %al
	movl	%r8d, %ecx
	setne	%bl
	xorl	$1, %ecx
	cmpb	$32, %al
	sete	%r8b
	orb	%cl, %bl
	je	.L284
	xorl	%r12d, %r12d
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L277:
	movl	$73, -64(%rbp)
	movl	$73, %r12d
	jmp	.L281
	.p2align 4,,10
	.p2align 3
.L300:
	movzbl	%r12b, %edx
	movq	%rdi, -128(%rbp)
	addl	%eax, %edx
	movl	%ebx, -88(%rbp)
	andl	$16777215, %edx
	movl	%edx, -104(%rbp)
	call	_ZN6icu_6711NGramParser6searchEPKii
	movq	-128(%rbp), %rdi
	leal	1(%rbx), %ecx
	leaq	_ZN6icu_67L17charMap_IBM420_arE(%rip), %r11
	movl	%eax, %r8d
	movl	%edx, %eax
	sall	$8, %eax
	testl	%r8d, %r8d
	js	.L297
	addl	$1, %r15d
	movl	%ebx, %r14d
	movl	%ecx, %ebx
	movl	%r15d, -84(%rbp)
	jmp	.L286
	.p2align 4,,10
	.p2align 3
.L297:
	movl	%ebx, %r14d
	movl	%ecx, %ebx
	jmp	.L286
	.p2align 4,,10
	.p2align 3
.L318:
	movl	%edx, %eax
	leal	2(%r14), %ebx
	movl	%ecx, %r14d
	sall	$8, %eax
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L270:
	leaq	_ZN6icu_67L17charMap_IBM420_arE(%rip), %rcx
	leaq	_ZN6icu_67L20ngrams_IBM420_ar_ltrE(%rip), %rdx
	call	*%rax
	testl	%eax, %eax
	movl	%eax, %ecx
	setg	%r12b
.L290:
	movq	-120(%rbp), %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	_ZN6icu_6712CharsetMatch3setEPNS_9InputTextEPKNS_17CharsetRecognizerEiPKcS7_@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L319
	addq	$136, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L315:
	.cfi_restore_state
	sall	$8, %edx
	movl	%r14d, %r9d
	movl	%r15d, %ebx
	movq	-168(%rbp), %r14
	addl	$32, %edx
	movq	-160(%rbp), %r15
	leal	1(%r9), %r12d
	andl	$16777215, %edx
.L273:
	leaq	_ZN6icu_67L20ngrams_IBM420_ar_ltrE(%rip), %rsi
	movl	%edx, -104(%rbp)
	movl	%r12d, -88(%rbp)
	call	_ZN6icu_6711NGramParser6searchEPKii
	testl	%eax, %eax
	js	.L288
.L291:
	addl	$1, %ebx
	movl	%ebx, -84(%rbp)
.L288:
	pxor	%xmm0, %xmm0
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm0
	cvtsi2sdl	%r12d, %xmm1
	divsd	%xmm1, %xmm0
	comisd	.LC21(%rip), %xmm0
	ja	.L298
.L292:
	mulsd	.LC22(%rip), %xmm0
	cvttsd2sil	%xmm0, %ecx
	testl	%ecx, %ecx
	setg	%r12b
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L298:
	movl	$1, %r12d
	movl	$98, %ecx
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L314:
	leal	32(%rax), %edx
	movl	%ebx, %r12d
	movq	-168(%rbp), %r14
	movl	%r15d, %ebx
	andl	$16777215, %edx
	movq	-160(%rbp), %r15
	jmp	.L273
.L271:
	leaq	-112(%rbp), %rdi
	movl	$32, %edx
	movl	$32, -104(%rbp)
	xorl	%ebx, %ebx
	movl	$1, -88(%rbp)
	movl	$1, %r12d
	call	_ZN6icu_6711NGramParser6searchEPKii
	pxor	%xmm0, %xmm0
	testl	%eax, %eax
	jns	.L291
	jmp	.L292
.L319:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2216:
	.size	_ZNK6icu_6726CharsetRecog_IBM420_ar_ltr5matchEPNS_9InputTextEPNS_12CharsetMatchE, .-_ZNK6icu_6726CharsetRecog_IBM420_ar_ltr5matchEPNS_9InputTextEPNS_12CharsetMatchE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717CharsetRecog_sbcs10match_sbcsEPNS_9InputTextEPKiPKh
	.type	_ZNK6icu_6717CharsetRecog_sbcs10match_sbcsEPNS_9InputTextEPKiPKh, @function
_ZNK6icu_6717CharsetRecog_sbcs10match_sbcsEPNS_9InputTextEPKiPKh:
.LFB2086:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r10d, %r10d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	leaq	-112(%rbp), %r15
	movq	%rdx, %rsi
	pushq	%r13
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$88, %rsp
	movl	8(%r14), %r9d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6711NGramParserE(%rip), %rax
	movq	%rcx, -72(%rbp)
	movq	%rax, -112(%rbp)
	movl	$0, -104(%rbp)
	movq	$0, -88(%rbp)
	movl	$0, -80(%rbp)
	movq	%rdx, -96(%rbp)
	xorl	%edx, %edx
.L328:
	movslq	%r13d, %rax
	xorl	%edi, %edi
	.p2align 4,,10
	.p2align 3
.L325:
	cmpl	%r9d, %r13d
	jge	.L343
	movq	(%r14), %rdi
	addl	$1, %r13d
	movzbl	(%rdi,%rax), %edi
	addq	$1, %rax
	movzbl	(%rcx,%rdi), %r8d
	movl	$1, %edi
	testb	%r8b, %r8b
	je	.L325
	movl	%r13d, -80(%rbp)
	cmpb	$32, %r8b
	jne	.L332
	testb	%r10b, %r10b
	je	.L332
.L326:
	cmpb	$32, %r8b
	sete	%r10b
	jmp	.L328
	.p2align 4,,10
	.p2align 3
.L343:
	testb	%dil, %dil
	je	.L322
	movl	%r13d, -80(%rbp)
.L322:
	sall	$8, %edx
	addl	$1, %r12d
	movq	%r15, %rdi
	addl	$32, %edx
	movl	%r12d, -88(%rbp)
	andl	$16777215, %edx
	movl	%edx, -104(%rbp)
	call	_ZN6icu_6711NGramParser6searchEPKii
	pxor	%xmm0, %xmm0
	pxor	%xmm1, %xmm1
	cmpl	$-2147483648, %eax
	cvtsi2sdl	%r12d, %xmm1
	movl	$98, %eax
	adcl	$0, %ebx
	cvtsi2sdl	%ebx, %xmm0
	divsd	%xmm1, %xmm0
	comisd	.LC21(%rip), %xmm0
	ja	.L320
	mulsd	.LC22(%rip), %xmm0
	cvttsd2sil	%xmm0, %eax
.L320:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L344
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L332:
	.cfi_restore_state
	movzbl	%r8b, %eax
	sall	$8, %edx
	addl	$1, %r12d
	movq	%r15, %rdi
	addl	%eax, %edx
	movq	%rcx, -128(%rbp)
	andl	$16777215, %edx
	movl	%r9d, -120(%rbp)
	movb	%r8b, -113(%rbp)
	movl	%edx, -104(%rbp)
	movl	%r12d, -88(%rbp)
	call	_ZN6icu_6711NGramParser6searchEPKii
	movzbl	-113(%rbp), %r8d
	movl	-120(%rbp), %r9d
	testl	%eax, %eax
	movq	-128(%rbp), %rcx
	js	.L326
	addl	$1, %ebx
	movl	%ebx, -84(%rbp)
	jmp	.L326
.L344:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2086:
	.size	_ZNK6icu_6717CharsetRecog_sbcs10match_sbcsEPNS_9InputTextEPKiPKh, .-_ZNK6icu_6717CharsetRecog_sbcs10match_sbcsEPNS_9InputTextEPKiPKh
	.align 2
	.p2align 4
	.type	_ZNK6icu_6717CharsetRecog_sbcs10match_sbcsEPNS_9InputTextEPKiPKh.constprop.0, @function
_ZNK6icu_6717CharsetRecog_sbcs10match_sbcsEPNS_9InputTextEPKiPKh.constprop.0:
.LFB2677:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rcx
	xorl	%r10d, %r10d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-112(%rbp), %r15
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$88, %rsp
	movl	8(%rdi), %r9d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6711NGramParserE(%rip), %rax
	movq	%rsi, -96(%rbp)
	movq	%rax, -112(%rbp)
	movl	$0, -104(%rbp)
	movq	$0, -88(%rbp)
	movl	$0, -80(%rbp)
	movq	%rdx, -72(%rbp)
	xorl	%edx, %edx
.L353:
	movslq	%r13d, %rax
	xorl	%edi, %edi
	.p2align 4,,10
	.p2align 3
.L350:
	cmpl	%r9d, %r13d
	jge	.L368
	movq	(%r14), %rdi
	addl	$1, %r13d
	movzbl	(%rdi,%rax), %edi
	addq	$1, %rax
	movzbl	(%rcx,%rdi), %r8d
	movl	$1, %edi
	testb	%r8b, %r8b
	je	.L350
	movl	%r13d, -80(%rbp)
	cmpb	$32, %r8b
	jne	.L357
	testb	%r10b, %r10b
	je	.L357
.L351:
	cmpb	$32, %r8b
	sete	%r10b
	jmp	.L353
	.p2align 4,,10
	.p2align 3
.L368:
	testb	%dil, %dil
	je	.L347
	movl	%r13d, -80(%rbp)
.L347:
	sall	$8, %edx
	addl	$1, %r12d
	movq	%r15, %rdi
	addl	$32, %edx
	movl	%r12d, -88(%rbp)
	andl	$16777215, %edx
	movl	%edx, -104(%rbp)
	call	_ZN6icu_6711NGramParser6searchEPKii
	pxor	%xmm0, %xmm0
	pxor	%xmm1, %xmm1
	cmpl	$-2147483648, %eax
	cvtsi2sdl	%r12d, %xmm1
	movl	$98, %eax
	adcl	$0, %ebx
	cvtsi2sdl	%ebx, %xmm0
	divsd	%xmm1, %xmm0
	comisd	.LC21(%rip), %xmm0
	ja	.L345
	mulsd	.LC22(%rip), %xmm0
	cvttsd2sil	%xmm0, %eax
.L345:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L369
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L357:
	.cfi_restore_state
	movzbl	%r8b, %eax
	sall	$8, %edx
	addl	$1, %r12d
	movq	%r15, %rdi
	addl	%eax, %edx
	movq	%rcx, -128(%rbp)
	andl	$16777215, %edx
	movl	%r9d, -120(%rbp)
	movb	%r8b, -113(%rbp)
	movl	%edx, -104(%rbp)
	movl	%r12d, -88(%rbp)
	call	_ZN6icu_6711NGramParser6searchEPKii
	movzbl	-113(%rbp), %r8d
	movl	-120(%rbp), %r9d
	testl	%eax, %eax
	movq	-128(%rbp), %rcx
	js	.L351
	addl	$1, %ebx
	movl	%ebx, -84(%rbp)
	jmp	.L351
.L369:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2677:
	.size	_ZNK6icu_6717CharsetRecog_sbcs10match_sbcsEPNS_9InputTextEPKiPKh.constprop.0, .-_ZNK6icu_6717CharsetRecog_sbcs10match_sbcsEPNS_9InputTextEPKiPKh.constprop.0
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722CharsetRecog_8859_5_ru5matchEPNS_9InputTextEPNS_12CharsetMatchE
	.type	_ZNK6icu_6722CharsetRecog_8859_5_ru5matchEPNS_9InputTextEPNS_12CharsetMatchE, @function
_ZNK6icu_6722CharsetRecog_8859_5_ru5matchEPNS_9InputTextEPNS_12CharsetMatchE:
.LFB2109:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	leaq	_ZNK6icu_6717CharsetRecog_sbcs10match_sbcsEPNS_9InputTextEPKiPKh(%rip), %rdx
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L371
	leaq	_ZN6icu_67L14charMap_8859_5E(%rip), %rdx
	leaq	_ZN6icu_67L16ngrams_8859_5_ruE(%rip), %rsi
	movq	%r13, %rdi
	call	_ZNK6icu_6717CharsetRecog_sbcs10match_sbcsEPNS_9InputTextEPKiPKh.constprop.0
	movl	%eax, %ebx
.L372:
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	%ebx, %ecx
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6712CharsetMatch3setEPNS_9InputTextEPKNS_17CharsetRecognizerEiPKcS7_@PLT
	testl	%ebx, %ebx
	popq	%rbx
	popq	%r12
	setg	%al
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L371:
	.cfi_restore_state
	leaq	_ZN6icu_67L14charMap_8859_5E(%rip), %rcx
	leaq	_ZN6icu_67L16ngrams_8859_5_ruE(%rip), %rdx
	call	*%rax
	movl	%eax, %ebx
	jmp	.L372
	.cfi_endproc
.LFE2109:
	.size	_ZNK6icu_6722CharsetRecog_8859_5_ru5matchEPNS_9InputTextEPNS_12CharsetMatchE, .-_ZNK6icu_6722CharsetRecog_8859_5_ru5matchEPNS_9InputTextEPNS_12CharsetMatchE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722CharsetRecog_8859_6_ar5matchEPNS_9InputTextEPNS_12CharsetMatchE
	.type	_ZNK6icu_6722CharsetRecog_8859_6_ar5matchEPNS_9InputTextEPNS_12CharsetMatchE, @function
_ZNK6icu_6722CharsetRecog_8859_6_ar5matchEPNS_9InputTextEPNS_12CharsetMatchE:
.LFB2120:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	leaq	_ZNK6icu_6717CharsetRecog_sbcs10match_sbcsEPNS_9InputTextEPKiPKh(%rip), %rdx
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L375
	leaq	_ZN6icu_67L14charMap_8859_6E(%rip), %rdx
	leaq	_ZN6icu_67L16ngrams_8859_6_arE(%rip), %rsi
	movq	%r13, %rdi
	call	_ZNK6icu_6717CharsetRecog_sbcs10match_sbcsEPNS_9InputTextEPKiPKh.constprop.0
	movl	%eax, %ebx
.L376:
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	%ebx, %ecx
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6712CharsetMatch3setEPNS_9InputTextEPKNS_17CharsetRecognizerEiPKcS7_@PLT
	testl	%ebx, %ebx
	popq	%rbx
	popq	%r12
	setg	%al
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L375:
	.cfi_restore_state
	leaq	_ZN6icu_67L14charMap_8859_6E(%rip), %rcx
	leaq	_ZN6icu_67L16ngrams_8859_6_arE(%rip), %rdx
	call	*%rax
	movl	%eax, %ebx
	jmp	.L376
	.cfi_endproc
.LFE2120:
	.size	_ZNK6icu_6722CharsetRecog_8859_6_ar5matchEPNS_9InputTextEPNS_12CharsetMatchE, .-_ZNK6icu_6722CharsetRecog_8859_6_ar5matchEPNS_9InputTextEPNS_12CharsetMatchE
	.section	.rodata.str1.1
.LC25:
	.string	"windows-1253"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722CharsetRecog_8859_7_el5matchEPNS_9InputTextEPNS_12CharsetMatchE
	.type	_ZNK6icu_6722CharsetRecog_8859_7_el5matchEPNS_9InputTextEPNS_12CharsetMatchE, @function
_ZNK6icu_6722CharsetRecog_8859_7_el5matchEPNS_9InputTextEPNS_12CharsetMatchE:
.LFB2131:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC6(%rip), %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	.LC25(%rip), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	leaq	_ZNK6icu_6717CharsetRecog_sbcs10match_sbcsEPNS_9InputTextEPKiPKh(%rip), %rdx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rax
	cmpb	$0, 24(%rsi)
	cmove	%r8, %r15
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L380
	leaq	_ZN6icu_67L14charMap_8859_7E(%rip), %rdx
	leaq	_ZN6icu_67L16ngrams_8859_7_elE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZNK6icu_6717CharsetRecog_sbcs10match_sbcsEPNS_9InputTextEPKiPKh.constprop.0
	movl	%eax, %ebx
.L381:
	movq	%r15, %r8
	movl	%ebx, %ecx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	leaq	.LC7(%rip), %r9
	call	_ZN6icu_6712CharsetMatch3setEPNS_9InputTextEPKNS_17CharsetRecognizerEiPKcS7_@PLT
	testl	%ebx, %ebx
	setg	%al
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L380:
	.cfi_restore_state
	leaq	_ZN6icu_67L14charMap_8859_7E(%rip), %rcx
	leaq	_ZN6icu_67L16ngrams_8859_7_elE(%rip), %rdx
	call	*%rax
	movl	%eax, %ebx
	jmp	.L381
	.cfi_endproc
.LFE2131:
	.size	_ZNK6icu_6722CharsetRecog_8859_7_el5matchEPNS_9InputTextEPNS_12CharsetMatchE, .-_ZNK6icu_6722CharsetRecog_8859_7_el5matchEPNS_9InputTextEPNS_12CharsetMatchE
	.section	.rodata.str1.1
.LC26:
	.string	"windows-1255"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6724CharsetRecog_8859_8_I_he5matchEPNS_9InputTextEPNS_12CharsetMatchE
	.type	_ZNK6icu_6724CharsetRecog_8859_8_I_he5matchEPNS_9InputTextEPNS_12CharsetMatchE, @function
_ZNK6icu_6724CharsetRecog_8859_8_I_he5matchEPNS_9InputTextEPNS_12CharsetMatchE:
.LFB2143:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC9(%rip), %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	.LC26(%rip), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	leaq	_ZNK6icu_6717CharsetRecog_sbcs10match_sbcsEPNS_9InputTextEPKiPKh(%rip), %rdx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rax
	cmpb	$0, 24(%rsi)
	cmove	%r8, %r15
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L386
	leaq	_ZN6icu_67L14charMap_8859_8E(%rip), %rdx
	leaq	_ZN6icu_67L18ngrams_8859_8_I_heE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZNK6icu_6717CharsetRecog_sbcs10match_sbcsEPNS_9InputTextEPKiPKh.constprop.0
	movl	%eax, %ebx
.L387:
	movq	%r15, %r8
	movl	%ebx, %ecx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	leaq	.LC10(%rip), %r9
	call	_ZN6icu_6712CharsetMatch3setEPNS_9InputTextEPKNS_17CharsetRecognizerEiPKcS7_@PLT
	testl	%ebx, %ebx
	setg	%al
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L386:
	.cfi_restore_state
	leaq	_ZN6icu_67L14charMap_8859_8E(%rip), %rcx
	leaq	_ZN6icu_67L18ngrams_8859_8_I_heE(%rip), %rdx
	call	*%rax
	movl	%eax, %ebx
	jmp	.L387
	.cfi_endproc
.LFE2143:
	.size	_ZNK6icu_6724CharsetRecog_8859_8_I_he5matchEPNS_9InputTextEPNS_12CharsetMatchE, .-_ZNK6icu_6724CharsetRecog_8859_8_I_he5matchEPNS_9InputTextEPNS_12CharsetMatchE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722CharsetRecog_8859_8_he5matchEPNS_9InputTextEPNS_12CharsetMatchE
	.type	_ZNK6icu_6722CharsetRecog_8859_8_he5matchEPNS_9InputTextEPNS_12CharsetMatchE, @function
_ZNK6icu_6722CharsetRecog_8859_8_he5matchEPNS_9InputTextEPNS_12CharsetMatchE:
.LFB2149:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC8(%rip), %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	.LC26(%rip), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	leaq	_ZNK6icu_6717CharsetRecog_sbcs10match_sbcsEPNS_9InputTextEPKiPKh(%rip), %rdx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rax
	cmpb	$0, 24(%rsi)
	cmove	%r8, %r15
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L392
	leaq	_ZN6icu_67L14charMap_8859_8E(%rip), %rdx
	leaq	_ZN6icu_67L16ngrams_8859_8_heE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZNK6icu_6717CharsetRecog_sbcs10match_sbcsEPNS_9InputTextEPKiPKh.constprop.0
	movl	%eax, %ebx
.L393:
	movq	%r15, %r8
	movl	%ebx, %ecx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	leaq	.LC10(%rip), %r9
	call	_ZN6icu_6712CharsetMatch3setEPNS_9InputTextEPKNS_17CharsetRecognizerEiPKcS7_@PLT
	testl	%ebx, %ebx
	setg	%al
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L392:
	.cfi_restore_state
	leaq	_ZN6icu_67L14charMap_8859_8E(%rip), %rcx
	leaq	_ZN6icu_67L16ngrams_8859_8_heE(%rip), %rdx
	call	*%rax
	movl	%eax, %ebx
	jmp	.L393
	.cfi_endproc
.LFE2149:
	.size	_ZNK6icu_6722CharsetRecog_8859_8_he5matchEPNS_9InputTextEPNS_12CharsetMatchE, .-_ZNK6icu_6722CharsetRecog_8859_8_he5matchEPNS_9InputTextEPNS_12CharsetMatchE
	.section	.rodata.str1.1
.LC27:
	.string	"windows-1254"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722CharsetRecog_8859_9_tr5matchEPNS_9InputTextEPNS_12CharsetMatchE
	.type	_ZNK6icu_6722CharsetRecog_8859_9_tr5matchEPNS_9InputTextEPNS_12CharsetMatchE, @function
_ZNK6icu_6722CharsetRecog_8859_9_tr5matchEPNS_9InputTextEPNS_12CharsetMatchE:
.LFB2160:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC11(%rip), %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	.LC27(%rip), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	leaq	_ZNK6icu_6717CharsetRecog_sbcs10match_sbcsEPNS_9InputTextEPKiPKh(%rip), %rdx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rax
	cmpb	$0, 24(%rsi)
	cmove	%r8, %r15
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L398
	leaq	_ZN6icu_67L14charMap_8859_9E(%rip), %rdx
	leaq	_ZN6icu_67L16ngrams_8859_9_trE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZNK6icu_6717CharsetRecog_sbcs10match_sbcsEPNS_9InputTextEPKiPKh.constprop.0
	movl	%eax, %ebx
.L399:
	movq	%r15, %r8
	movl	%ebx, %ecx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	leaq	.LC12(%rip), %r9
	call	_ZN6icu_6712CharsetMatch3setEPNS_9InputTextEPKNS_17CharsetRecognizerEiPKcS7_@PLT
	testl	%ebx, %ebx
	setg	%al
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L398:
	.cfi_restore_state
	leaq	_ZN6icu_67L14charMap_8859_9E(%rip), %rcx
	leaq	_ZN6icu_67L16ngrams_8859_9_trE(%rip), %rdx
	call	*%rax
	movl	%eax, %ebx
	jmp	.L399
	.cfi_endproc
.LFE2160:
	.size	_ZNK6icu_6722CharsetRecog_8859_9_tr5matchEPNS_9InputTextEPNS_12CharsetMatchE, .-_ZNK6icu_6722CharsetRecog_8859_9_tr5matchEPNS_9InputTextEPNS_12CharsetMatchE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6725CharsetRecog_windows_12565matchEPNS_9InputTextEPNS_12CharsetMatchE
	.type	_ZNK6icu_6725CharsetRecog_windows_12565matchEPNS_9InputTextEPNS_12CharsetMatchE, @function
_ZNK6icu_6725CharsetRecog_windows_12565matchEPNS_9InputTextEPNS_12CharsetMatchE:
.LFB2167:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	leaq	_ZNK6icu_6717CharsetRecog_sbcs10match_sbcsEPNS_9InputTextEPKiPKh(%rip), %rdx
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L403
	leaq	_ZN6icu_67L20charMap_windows_1256E(%rip), %rdx
	leaq	_ZN6icu_67L19ngrams_windows_1256E(%rip), %rsi
	movq	%r13, %rdi
	call	_ZNK6icu_6717CharsetRecog_sbcs10match_sbcsEPNS_9InputTextEPKiPKh.constprop.0
	movl	%eax, %ebx
.L404:
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	%ebx, %ecx
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6712CharsetMatch3setEPNS_9InputTextEPKNS_17CharsetRecognizerEiPKcS7_@PLT
	testl	%ebx, %ebx
	popq	%rbx
	popq	%r12
	setg	%al
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L403:
	.cfi_restore_state
	leaq	_ZN6icu_67L20charMap_windows_1256E(%rip), %rcx
	leaq	_ZN6icu_67L19ngrams_windows_1256E(%rip), %rdx
	call	*%rax
	movl	%eax, %ebx
	jmp	.L404
	.cfi_endproc
.LFE2167:
	.size	_ZNK6icu_6725CharsetRecog_windows_12565matchEPNS_9InputTextEPNS_12CharsetMatchE, .-_ZNK6icu_6725CharsetRecog_windows_12565matchEPNS_9InputTextEPNS_12CharsetMatchE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6725CharsetRecog_windows_12515matchEPNS_9InputTextEPNS_12CharsetMatchE
	.type	_ZNK6icu_6725CharsetRecog_windows_12515matchEPNS_9InputTextEPNS_12CharsetMatchE, @function
_ZNK6icu_6725CharsetRecog_windows_12515matchEPNS_9InputTextEPNS_12CharsetMatchE:
.LFB2174:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	leaq	_ZNK6icu_6717CharsetRecog_sbcs10match_sbcsEPNS_9InputTextEPKiPKh(%rip), %rdx
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L407
	leaq	_ZN6icu_67L20charMap_windows_1251E(%rip), %rdx
	leaq	_ZN6icu_67L19ngrams_windows_1251E(%rip), %rsi
	movq	%r13, %rdi
	call	_ZNK6icu_6717CharsetRecog_sbcs10match_sbcsEPNS_9InputTextEPKiPKh.constprop.0
	movl	%eax, %ebx
.L408:
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	%ebx, %ecx
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6712CharsetMatch3setEPNS_9InputTextEPKNS_17CharsetRecognizerEiPKcS7_@PLT
	testl	%ebx, %ebx
	popq	%rbx
	popq	%r12
	setg	%al
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L407:
	.cfi_restore_state
	leaq	_ZN6icu_67L20charMap_windows_1251E(%rip), %rcx
	leaq	_ZN6icu_67L19ngrams_windows_1251E(%rip), %rdx
	call	*%rax
	movl	%eax, %ebx
	jmp	.L408
	.cfi_endproc
.LFE2174:
	.size	_ZNK6icu_6725CharsetRecog_windows_12515matchEPNS_9InputTextEPNS_12CharsetMatchE, .-_ZNK6icu_6725CharsetRecog_windows_12515matchEPNS_9InputTextEPNS_12CharsetMatchE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6719CharsetRecog_KOI8_R5matchEPNS_9InputTextEPNS_12CharsetMatchE
	.type	_ZNK6icu_6719CharsetRecog_KOI8_R5matchEPNS_9InputTextEPNS_12CharsetMatchE, @function
_ZNK6icu_6719CharsetRecog_KOI8_R5matchEPNS_9InputTextEPNS_12CharsetMatchE:
.LFB2181:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	leaq	_ZNK6icu_6717CharsetRecog_sbcs10match_sbcsEPNS_9InputTextEPKiPKh(%rip), %rdx
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L411
	leaq	_ZN6icu_67L14charMap_KOI8_RE(%rip), %rdx
	leaq	_ZN6icu_67L13ngrams_KOI8_RE(%rip), %rsi
	movq	%r13, %rdi
	call	_ZNK6icu_6717CharsetRecog_sbcs10match_sbcsEPNS_9InputTextEPKiPKh.constprop.0
	movl	%eax, %ebx
.L412:
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	%ebx, %ecx
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6712CharsetMatch3setEPNS_9InputTextEPKNS_17CharsetRecognizerEiPKcS7_@PLT
	testl	%ebx, %ebx
	popq	%rbx
	popq	%r12
	setg	%al
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L411:
	.cfi_restore_state
	leaq	_ZN6icu_67L14charMap_KOI8_RE(%rip), %rcx
	leaq	_ZN6icu_67L13ngrams_KOI8_RE(%rip), %rdx
	call	*%rax
	movl	%eax, %ebx
	jmp	.L412
	.cfi_endproc
.LFE2181:
	.size	_ZNK6icu_6719CharsetRecog_KOI8_R5matchEPNS_9InputTextEPNS_12CharsetMatchE, .-_ZNK6icu_6719CharsetRecog_KOI8_R5matchEPNS_9InputTextEPNS_12CharsetMatchE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6726CharsetRecog_IBM424_he_rtl5matchEPNS_9InputTextEPNS_12CharsetMatchE
	.type	_ZNK6icu_6726CharsetRecog_IBM424_he_rtl5matchEPNS_9InputTextEPNS_12CharsetMatchE, @function
_ZNK6icu_6726CharsetRecog_IBM424_he_rtl5matchEPNS_9InputTextEPNS_12CharsetMatchE:
.LFB2192:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	leaq	_ZNK6icu_6717CharsetRecog_sbcs10match_sbcsEPNS_9InputTextEPKiPKh(%rip), %rdx
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L415
	leaq	_ZN6icu_67L17charMap_IBM424_heE(%rip), %rdx
	leaq	_ZN6icu_67L20ngrams_IBM424_he_rtlE(%rip), %rsi
	movq	%r13, %rdi
	call	_ZNK6icu_6717CharsetRecog_sbcs10match_sbcsEPNS_9InputTextEPKiPKh.constprop.0
	movl	%eax, %ebx
.L416:
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	%ebx, %ecx
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6712CharsetMatch3setEPNS_9InputTextEPKNS_17CharsetRecognizerEiPKcS7_@PLT
	testl	%ebx, %ebx
	popq	%rbx
	popq	%r12
	setg	%al
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L415:
	.cfi_restore_state
	leaq	_ZN6icu_67L17charMap_IBM424_heE(%rip), %rcx
	leaq	_ZN6icu_67L20ngrams_IBM424_he_rtlE(%rip), %rdx
	call	*%rax
	movl	%eax, %ebx
	jmp	.L416
	.cfi_endproc
.LFE2192:
	.size	_ZNK6icu_6726CharsetRecog_IBM424_he_rtl5matchEPNS_9InputTextEPNS_12CharsetMatchE, .-_ZNK6icu_6726CharsetRecog_IBM424_he_rtl5matchEPNS_9InputTextEPNS_12CharsetMatchE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6726CharsetRecog_IBM424_he_ltr5matchEPNS_9InputTextEPNS_12CharsetMatchE
	.type	_ZNK6icu_6726CharsetRecog_IBM424_he_ltr5matchEPNS_9InputTextEPNS_12CharsetMatchE, @function
_ZNK6icu_6726CharsetRecog_IBM424_he_ltr5matchEPNS_9InputTextEPNS_12CharsetMatchE:
.LFB2198:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	leaq	_ZNK6icu_6717CharsetRecog_sbcs10match_sbcsEPNS_9InputTextEPKiPKh(%rip), %rdx
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L419
	leaq	_ZN6icu_67L17charMap_IBM424_heE(%rip), %rdx
	leaq	_ZN6icu_67L20ngrams_IBM424_he_ltrE(%rip), %rsi
	movq	%r13, %rdi
	call	_ZNK6icu_6717CharsetRecog_sbcs10match_sbcsEPNS_9InputTextEPKiPKh.constprop.0
	movl	%eax, %ebx
.L420:
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	%ebx, %ecx
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6712CharsetMatch3setEPNS_9InputTextEPKNS_17CharsetRecognizerEiPKcS7_@PLT
	testl	%ebx, %ebx
	popq	%rbx
	popq	%r12
	setg	%al
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L419:
	.cfi_restore_state
	leaq	_ZN6icu_67L17charMap_IBM424_heE(%rip), %rcx
	leaq	_ZN6icu_67L20ngrams_IBM424_he_ltrE(%rip), %rdx
	call	*%rax
	movl	%eax, %ebx
	jmp	.L420
	.cfi_endproc
.LFE2198:
	.size	_ZNK6icu_6726CharsetRecog_IBM424_he_ltr5matchEPNS_9InputTextEPNS_12CharsetMatchE, .-_ZNK6icu_6726CharsetRecog_IBM424_he_ltr5matchEPNS_9InputTextEPNS_12CharsetMatchE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718NGramParser_IBM42015parseCharactersEPNS_9InputTextE
	.type	_ZN6icu_6718NGramParser_IBM42015parseCharactersEPNS_9InputTextE, @function
_ZN6icu_6718NGramParser_IBM42015parseCharactersEPNS_9InputTextE:
.LFB2075:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	_ZN6icu_6718NGramParser_IBM4208nextByteEPNS_9InputTextE(%rip), %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	_ZN6icu_67L17unshapeMap_IBM420E(%rip), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %rax
	movq	16(%rax), %r14
	.p2align 4,,10
	.p2align 3
.L423:
	cmpq	%r15, %r14
	jne	.L424
.L457:
	movl	32(%rbx), %edx
	cmpl	8(%r13), %edx
	jge	.L422
	movslq	%edx, %rsi
	addq	0(%r13), %rsi
	movzbl	(%rsi), %eax
	testb	%al, %al
	je	.L422
	leal	-178(%rax), %edi
	cmpl	$1, %edi
	jbe	.L456
	leal	-180(%rax), %edi
	cmpl	$1, %edi
	jbe	.L428
	subl	$184, %eax
	cmpl	$1, %eax
	jbe	.L429
	movl	$0, 48(%rbx)
	movzbl	(%rsi), %eax
	xorl	%r8d, %r8d
	movzbl	(%r12,%rax), %eax
.L427:
	addl	$1, %edx
	movl	%edx, 32(%rbx)
.L433:
	movq	40(%rbx), %r10
	movzbl	(%r10,%rax), %r9d
	testb	%r9b, %r9b
	je	.L434
	cmpb	$32, %r9b
	jne	.L435
.L459:
	testb	%cl, %cl
	je	.L435
.L436:
	cmpb	$32, %r9b
	sete	%cl
.L434:
	testl	%r8d, %r8d
	je	.L423
.L432:
	movzbl	%r8b, %r8d
	movzbl	(%r10,%r8), %r8d
	testb	%r8b, %r8b
	je	.L423
	cmpb	$32, %r8b
	jne	.L439
	testb	%cl, %cl
	je	.L439
.L440:
	cmpb	$32, %r8b
	sete	%cl
	cmpq	%r15, %r14
	je	.L457
.L424:
	movb	%cl, -49(%rbp)
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	*%r14
	testl	%eax, %eax
	jns	.L458
.L422:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L429:
	.cfi_restore_state
	movl	$86, 48(%rbx)
	movl	$86, %r8d
.L430:
	movq	40(%rbx), %r10
	addl	$1, %edx
	movl	%edx, 32(%rbx)
	movzbl	177(%r10), %r9d
	testb	%r9b, %r9b
	je	.L432
	cmpb	$32, %r9b
	je	.L459
	.p2align 4,,10
	.p2align 3
.L435:
	movl	8(%rbx), %edx
	movzbl	%r9b, %eax
	addl	$1, 24(%rbx)
	movq	%rbx, %rdi
	movq	16(%rbx), %rsi
	movl	%r8d, -56(%rbp)
	sall	$8, %edx
	movq	%r10, -64(%rbp)
	addl	%eax, %edx
	movb	%r9b, -49(%rbp)
	andl	$16777215, %edx
	movl	%edx, 8(%rbx)
	call	_ZN6icu_6711NGramParser6searchEPKii
	movzbl	-49(%rbp), %r9d
	movq	-64(%rbp), %r10
	testl	%eax, %eax
	movl	-56(%rbp), %r8d
	js	.L436
	addl	$1, 28(%rbx)
	jmp	.L436
	.p2align 4,,10
	.p2align 3
.L456:
	movl	$71, 48(%rbx)
	movl	$71, %r8d
	movl	$177, %eax
	jmp	.L427
	.p2align 4,,10
	.p2align 3
.L428:
	movl	$73, 48(%rbx)
	movl	$73, %r8d
	jmp	.L430
	.p2align 4,,10
	.p2align 3
.L458:
	movq	(%rbx), %rdx
	movl	48(%rbx), %r8d
	cltq
	movzbl	-49(%rbp), %ecx
	movq	16(%rdx), %r14
	jmp	.L433
	.p2align 4,,10
	.p2align 3
.L439:
	movl	8(%rbx), %edx
	movzbl	%r8b, %eax
	addl	$1, 24(%rbx)
	movq	%rbx, %rdi
	movq	16(%rbx), %rsi
	movb	%r8b, -49(%rbp)
	sall	$8, %edx
	addl	%eax, %edx
	andl	$16777215, %edx
	movl	%edx, 8(%rbx)
	call	_ZN6icu_6711NGramParser6searchEPKii
	movzbl	-49(%rbp), %r8d
	testl	%eax, %eax
	js	.L440
	addl	$1, 28(%rbx)
	jmp	.L440
	.cfi_endproc
.LFE2075:
	.size	_ZN6icu_6718NGramParser_IBM42015parseCharactersEPNS_9InputTextE, .-_ZN6icu_6718NGramParser_IBM42015parseCharactersEPNS_9InputTextE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711NGramParser15parseCharactersEPNS_9InputTextE
	.type	_ZN6icu_6711NGramParser15parseCharactersEPNS_9InputTextE, @function
_ZN6icu_6711NGramParser15parseCharactersEPNS_9InputTextE:
.LFB2064:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	jmp	.L462
	.p2align 4,,10
	.p2align 3
.L471:
	movq	40(%r13), %rdx
	cltq
	movzbl	(%rdx,%rax), %r14d
	testb	%r14b, %r14b
	jne	.L470
.L462:
	movq	0(%r13), %rax
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	*16(%rax)
	testl	%eax, %eax
	jns	.L471
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L470:
	.cfi_restore_state
	cmpb	$32, %r14b
	jne	.L463
	testb	%r12b, %r12b
	je	.L463
.L464:
	cmpb	$32, %r14b
	sete	%r12b
	jmp	.L462
	.p2align 4,,10
	.p2align 3
.L463:
	movl	8(%r13), %edx
	movzbl	%r14b, %eax
	addl	$1, 24(%r13)
	movq	%r13, %rdi
	movq	16(%r13), %rsi
	sall	$8, %edx
	addl	%eax, %edx
	andl	$16777215, %edx
	movl	%edx, 8(%r13)
	call	_ZN6icu_6711NGramParser6searchEPKii
	testl	%eax, %eax
	js	.L464
	addl	$1, 28(%r13)
	jmp	.L464
	.cfi_endproc
.LFE2064:
	.size	_ZN6icu_6711NGramParser15parseCharactersEPNS_9InputTextE, .-_ZN6icu_6711NGramParser15parseCharactersEPNS_9InputTextE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711NGramParser6lookupEi
	.type	_ZN6icu_6711NGramParser6lookupEi, @function
_ZN6icu_6711NGramParser6lookupEi:
.LFB2061:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	16(%rdi), %rax
	movl	$224, %edx
	movl	$96, %r8d
	addl	$1, 24(%rdi)
	movl	$16, %r9d
	movl	$16, %ecx
	movl	$192, %r11d
	movl	$32, %r10d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	$32, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	cmpl	128(%rax), %esi
	movl	$160, %ebx
	cmovge	%rdx, %r8
	movl	$48, %edx
	cmovl	%r10, %rbx
	cmovge	%rdx, %rcx
	cmovl	%r9d, %edx
	movl	$64, %r9d
	cmovge	%r11, %r9
	movl	$0, %r11d
	cmovge	%r10, %r11
	movl	$0, %r10d
	cmovge	%r12d, %r10d
	cmpl	(%rax,%r9), %esi
	cmovl	%rbx, %r8
	cmovl	%r11, %rcx
	cmovl	%r10d, %edx
	cmpl	(%rax,%r8), %esi
	jl	.L475
	addl	$8, %edx
	movslq	%edx, %rcx
.L475:
	cmpl	16(%rax,%rcx,4), %esi
	jl	.L476
	addl	$4, %edx
	movslq	%edx, %rcx
.L476:
	cmpl	8(%rax,%rcx,4), %esi
	jl	.L477
	addl	$2, %edx
	movslq	%edx, %rcx
.L477:
	cmpl	4(%rax,%rcx,4), %esi
	jl	.L478
	leal	1(%rdx), %ecx
	movslq	%ecx, %rcx
	movl	(%rax,%rcx,4), %ecx
	cmpl	%ecx, %esi
	jl	.L479
.L480:
	cmpl	%ecx, %esi
	jne	.L472
	addl	$1, 28(%rdi)
.L472:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L478:
	.cfi_restore_state
	movl	(%rax,%rcx,4), %ecx
	cmpl	%ecx, %esi
	jge	.L480
	subl	$1, %edx
	cmpl	$-1, %edx
	je	.L472
.L479:
	movslq	%edx, %rdx
	movl	(%rax,%rdx,4), %ecx
	jmp	.L480
	.cfi_endproc
.LFE2061:
	.size	_ZN6icu_6711NGramParser6lookupEi, .-_ZN6icu_6711NGramParser6lookupEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711NGramParser7addByteEi
	.type	_ZN6icu_6711NGramParser7addByteEi, @function
_ZN6icu_6711NGramParser7addByteEi:
.LFB2062:
	.cfi_startproc
	endbr64
	movl	%esi, %r8d
	movl	8(%rdi), %esi
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$224, %eax
	movq	16(%rdi), %r9
	addl	$1, 24(%rdi)
	movl	$96, %ecx
	movl	$16, %edx
	sall	$8, %esi
	movl	$192, %r11d
	movl	$32, %r10d
	addl	%r8d, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	movl	$16, %r8d
	andl	$16777215, %esi
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movl	$160, %ebx
	movl	$32, %r12d
	movl	%esi, 8(%rdi)
	cmpl	128(%r9), %esi
	cmovge	%rax, %rcx
	movl	$48, %eax
	cmovl	%r10, %rbx
	cmovge	%rax, %rdx
	cmovl	%r8d, %eax
	movl	$64, %r8d
	cmovge	%r11, %r8
	movl	$0, %r11d
	cmovge	%r10, %r11
	movl	$0, %r10d
	cmovge	%r12d, %r10d
	cmpl	(%r9,%r8), %esi
	cmovl	%rbx, %rcx
	cmovl	%r11, %rdx
	cmovl	%r10d, %eax
	cmpl	(%r9,%rcx), %esi
	jl	.L490
	addl	$8, %eax
	movslq	%eax, %rdx
.L490:
	cmpl	16(%r9,%rdx,4), %esi
	jl	.L491
	addl	$4, %eax
	movslq	%eax, %rdx
.L491:
	cmpl	8(%r9,%rdx,4), %esi
	jl	.L492
	addl	$2, %eax
	movslq	%eax, %rdx
.L492:
	cmpl	4(%r9,%rdx,4), %esi
	jl	.L493
	leal	1(%rax), %edx
	movslq	%edx, %rdx
	movl	(%r9,%rdx,4), %edx
	cmpl	%edx, %esi
	jl	.L494
.L495:
	cmpl	%edx, %esi
	jne	.L487
	addl	$1, 28(%rdi)
.L487:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L493:
	.cfi_restore_state
	movl	(%r9,%rdx,4), %edx
	cmpl	%edx, %esi
	jge	.L495
	subl	$1, %eax
	cmpl	$-1, %eax
	je	.L487
.L494:
	cltq
	movl	(%r9,%rax,4), %edx
	jmp	.L495
	.cfi_endproc
.LFE2062:
	.size	_ZN6icu_6711NGramParser7addByteEi, .-_ZN6icu_6711NGramParser7addByteEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711NGramParser5parseEPNS_9InputTextE
	.type	_ZN6icu_6711NGramParser5parseEPNS_9InputTextE, @function
_ZN6icu_6711NGramParser5parseEPNS_9InputTextE:
.LFB2065:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	call	*24(%rax)
	movl	8(%rbx), %edx
	movl	24(%rbx), %eax
	movq	%rbx, %rdi
	movq	16(%rbx), %rsi
	sall	$8, %edx
	leal	1(%rax), %r12d
	addl	$32, %edx
	movl	%r12d, 24(%rbx)
	andl	$16777215, %edx
	movl	%edx, 8(%rbx)
	call	_ZN6icu_6711NGramParser6searchEPKii
	testl	%eax, %eax
	movl	28(%rbx), %eax
	js	.L504
	addl	$1, %eax
	movl	%eax, 28(%rbx)
.L504:
	pxor	%xmm0, %xmm0
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm0
	movl	$98, %eax
	cvtsi2sdl	%r12d, %xmm1
	divsd	%xmm1, %xmm0
	comisd	.LC21(%rip), %xmm0
	ja	.L502
	mulsd	.LC22(%rip), %xmm0
	cvttsd2sil	%xmm0, %eax
.L502:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2065:
	.size	_ZN6icu_6711NGramParser5parseEPNS_9InputTextE, .-_ZN6icu_6711NGramParser5parseEPNS_9InputTextE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718NGramParser_IBM420C2EPKiPKh
	.type	_ZN6icu_6718NGramParser_IBM420C2EPKiPKh, @function
_ZN6icu_6718NGramParser_IBM420C2EPKiPKh:
.LFB2067:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6718NGramParser_IBM420E(%rip), %rax
	movl	$0, 8(%rdi)
	movq	%rsi, 16(%rdi)
	movq	%rdx, 40(%rdi)
	movq	$0, 24(%rdi)
	movl	$0, 32(%rdi)
	movq	%rax, (%rdi)
	movl	$0, 48(%rdi)
	ret
	.cfi_endproc
.LFE2067:
	.size	_ZN6icu_6718NGramParser_IBM420C2EPKiPKh, .-_ZN6icu_6718NGramParser_IBM420C2EPKiPKh
	.globl	_ZN6icu_6718NGramParser_IBM420C1EPKiPKh
	.set	_ZN6icu_6718NGramParser_IBM420C1EPKiPKh,_ZN6icu_6718NGramParser_IBM420C2EPKiPKh
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718NGramParser_IBM4209isLamAlefEi
	.type	_ZN6icu_6718NGramParser_IBM4209isLamAlefEi, @function
_ZN6icu_6718NGramParser_IBM4209isLamAlefEi:
.LFB2073:
	.cfi_startproc
	endbr64
	leal	-178(%rsi), %eax
	cmpl	$1, %eax
	jbe	.L511
	leal	-180(%rsi), %eax
	cmpl	$1, %eax
	jbe	.L512
	subl	$184, %esi
	cmpl	$2, %esi
	sbbl	%eax, %eax
	andl	$86, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L511:
	movl	$71, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L512:
	movl	$73, %eax
	ret
	.cfi_endproc
.LFE2073:
	.size	_ZN6icu_6718NGramParser_IBM4209isLamAlefEi, .-_ZN6icu_6718NGramParser_IBM4209isLamAlefEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717CharsetRecog_sbcsC2Ev
	.type	_ZN6icu_6717CharsetRecog_sbcsC2Ev, @function
_ZN6icu_6717CharsetRecog_sbcsC2Ev:
.LFB2080:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6717CharsetRecog_sbcsE(%rip), %rax
	movq	%rax, (%rdi)
	ret
	.cfi_endproc
.LFE2080:
	.size	_ZN6icu_6717CharsetRecog_sbcsC2Ev, .-_ZN6icu_6717CharsetRecog_sbcsC2Ev
	.globl	_ZN6icu_6717CharsetRecog_sbcsC1Ev
	.set	_ZN6icu_6717CharsetRecog_sbcsC1Ev,_ZN6icu_6717CharsetRecog_sbcsC2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717CharsetRecog_sbcsD2Ev
	.type	_ZN6icu_6717CharsetRecog_sbcsD2Ev, @function
_ZN6icu_6717CharsetRecog_sbcsD2Ev:
.LFB2083:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6717CharsetRecog_sbcsE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	.cfi_endproc
.LFE2083:
	.size	_ZN6icu_6717CharsetRecog_sbcsD2Ev, .-_ZN6icu_6717CharsetRecog_sbcsD2Ev
	.globl	_ZN6icu_6717CharsetRecog_sbcsD1Ev
	.set	_ZN6icu_6717CharsetRecog_sbcsD1Ev,_ZN6icu_6717CharsetRecog_sbcsD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717CharsetRecog_sbcsD0Ev
	.type	_ZN6icu_6717CharsetRecog_sbcsD0Ev, @function
_ZN6icu_6717CharsetRecog_sbcsD0Ev:
.LFB2085:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6717CharsetRecog_sbcsE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2085:
	.size	_ZN6icu_6717CharsetRecog_sbcsD0Ev, .-_ZN6icu_6717CharsetRecog_sbcsD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719CharsetRecog_8859_5D2Ev
	.type	_ZN6icu_6719CharsetRecog_8859_5D2Ev, @function
_ZN6icu_6719CharsetRecog_8859_5D2Ev:
.LFB2100:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6717CharsetRecog_sbcsE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	.cfi_endproc
.LFE2100:
	.size	_ZN6icu_6719CharsetRecog_8859_5D2Ev, .-_ZN6icu_6719CharsetRecog_8859_5D2Ev
	.globl	_ZN6icu_6719CharsetRecog_8859_5D1Ev
	.set	_ZN6icu_6719CharsetRecog_8859_5D1Ev,_ZN6icu_6719CharsetRecog_8859_5D2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719CharsetRecog_8859_5D0Ev
	.type	_ZN6icu_6719CharsetRecog_8859_5D0Ev, @function
_ZN6icu_6719CharsetRecog_8859_5D0Ev:
.LFB2102:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6717CharsetRecog_sbcsE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2102:
	.size	_ZN6icu_6719CharsetRecog_8859_5D0Ev, .-_ZN6icu_6719CharsetRecog_8859_5D0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719CharsetRecog_8859_6D2Ev
	.type	_ZN6icu_6719CharsetRecog_8859_6D2Ev, @function
_ZN6icu_6719CharsetRecog_8859_6D2Ev:
.LFB2111:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6717CharsetRecog_sbcsE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	.cfi_endproc
.LFE2111:
	.size	_ZN6icu_6719CharsetRecog_8859_6D2Ev, .-_ZN6icu_6719CharsetRecog_8859_6D2Ev
	.globl	_ZN6icu_6719CharsetRecog_8859_6D1Ev
	.set	_ZN6icu_6719CharsetRecog_8859_6D1Ev,_ZN6icu_6719CharsetRecog_8859_6D2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719CharsetRecog_8859_6D0Ev
	.type	_ZN6icu_6719CharsetRecog_8859_6D0Ev, @function
_ZN6icu_6719CharsetRecog_8859_6D0Ev:
.LFB2113:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6717CharsetRecog_sbcsE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2113:
	.size	_ZN6icu_6719CharsetRecog_8859_6D0Ev, .-_ZN6icu_6719CharsetRecog_8859_6D0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719CharsetRecog_8859_7D2Ev
	.type	_ZN6icu_6719CharsetRecog_8859_7D2Ev, @function
_ZN6icu_6719CharsetRecog_8859_7D2Ev:
.LFB2122:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6717CharsetRecog_sbcsE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	.cfi_endproc
.LFE2122:
	.size	_ZN6icu_6719CharsetRecog_8859_7D2Ev, .-_ZN6icu_6719CharsetRecog_8859_7D2Ev
	.globl	_ZN6icu_6719CharsetRecog_8859_7D1Ev
	.set	_ZN6icu_6719CharsetRecog_8859_7D1Ev,_ZN6icu_6719CharsetRecog_8859_7D2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719CharsetRecog_8859_7D0Ev
	.type	_ZN6icu_6719CharsetRecog_8859_7D0Ev, @function
_ZN6icu_6719CharsetRecog_8859_7D0Ev:
.LFB2124:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6717CharsetRecog_sbcsE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2124:
	.size	_ZN6icu_6719CharsetRecog_8859_7D0Ev, .-_ZN6icu_6719CharsetRecog_8859_7D0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719CharsetRecog_8859_8D2Ev
	.type	_ZN6icu_6719CharsetRecog_8859_8D2Ev, @function
_ZN6icu_6719CharsetRecog_8859_8D2Ev:
.LFB2133:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6717CharsetRecog_sbcsE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	.cfi_endproc
.LFE2133:
	.size	_ZN6icu_6719CharsetRecog_8859_8D2Ev, .-_ZN6icu_6719CharsetRecog_8859_8D2Ev
	.globl	_ZN6icu_6719CharsetRecog_8859_8D1Ev
	.set	_ZN6icu_6719CharsetRecog_8859_8D1Ev,_ZN6icu_6719CharsetRecog_8859_8D2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719CharsetRecog_8859_8D0Ev
	.type	_ZN6icu_6719CharsetRecog_8859_8D0Ev, @function
_ZN6icu_6719CharsetRecog_8859_8D0Ev:
.LFB2135:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6717CharsetRecog_sbcsE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2135:
	.size	_ZN6icu_6719CharsetRecog_8859_8D0Ev, .-_ZN6icu_6719CharsetRecog_8859_8D0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719CharsetRecog_8859_9D2Ev
	.type	_ZN6icu_6719CharsetRecog_8859_9D2Ev, @function
_ZN6icu_6719CharsetRecog_8859_9D2Ev:
.LFB2151:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6717CharsetRecog_sbcsE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	.cfi_endproc
.LFE2151:
	.size	_ZN6icu_6719CharsetRecog_8859_9D2Ev, .-_ZN6icu_6719CharsetRecog_8859_9D2Ev
	.globl	_ZN6icu_6719CharsetRecog_8859_9D1Ev
	.set	_ZN6icu_6719CharsetRecog_8859_9D1Ev,_ZN6icu_6719CharsetRecog_8859_9D2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719CharsetRecog_8859_9D0Ev
	.type	_ZN6icu_6719CharsetRecog_8859_9D0Ev, @function
_ZN6icu_6719CharsetRecog_8859_9D0Ev:
.LFB2153:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6717CharsetRecog_sbcsE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2153:
	.size	_ZN6icu_6719CharsetRecog_8859_9D0Ev, .-_ZN6icu_6719CharsetRecog_8859_9D0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722CharsetRecog_IBM424_heD2Ev
	.type	_ZN6icu_6722CharsetRecog_IBM424_heD2Ev, @function
_ZN6icu_6722CharsetRecog_IBM424_heD2Ev:
.LFB2183:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6717CharsetRecog_sbcsE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	.cfi_endproc
.LFE2183:
	.size	_ZN6icu_6722CharsetRecog_IBM424_heD2Ev, .-_ZN6icu_6722CharsetRecog_IBM424_heD2Ev
	.globl	_ZN6icu_6722CharsetRecog_IBM424_heD1Ev
	.set	_ZN6icu_6722CharsetRecog_IBM424_heD1Ev,_ZN6icu_6722CharsetRecog_IBM424_heD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722CharsetRecog_IBM424_heD0Ev
	.type	_ZN6icu_6722CharsetRecog_IBM424_heD0Ev, @function
_ZN6icu_6722CharsetRecog_IBM424_heD0Ev:
.LFB2185:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6717CharsetRecog_sbcsE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2185:
	.size	_ZN6icu_6722CharsetRecog_IBM424_heD0Ev, .-_ZN6icu_6722CharsetRecog_IBM424_heD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722CharsetRecog_IBM420_arD2Ev
	.type	_ZN6icu_6722CharsetRecog_IBM420_arD2Ev, @function
_ZN6icu_6722CharsetRecog_IBM420_arD2Ev:
.LFB2200:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6717CharsetRecog_sbcsE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	.cfi_endproc
.LFE2200:
	.size	_ZN6icu_6722CharsetRecog_IBM420_arD2Ev, .-_ZN6icu_6722CharsetRecog_IBM420_arD2Ev
	.globl	_ZN6icu_6722CharsetRecog_IBM420_arD1Ev
	.set	_ZN6icu_6722CharsetRecog_IBM420_arD1Ev,_ZN6icu_6722CharsetRecog_IBM420_arD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722CharsetRecog_IBM420_arD0Ev
	.type	_ZN6icu_6722CharsetRecog_IBM420_arD0Ev, @function
_ZN6icu_6722CharsetRecog_IBM420_arD0Ev:
.LFB2202:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6717CharsetRecog_sbcsE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6717CharsetRecognizerD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2202:
	.size	_ZN6icu_6722CharsetRecog_IBM420_arD0Ev, .-_ZN6icu_6722CharsetRecog_IBM420_arD0Ev
	.weak	_ZTSN6icu_677UMemoryE
	.section	.rodata._ZTSN6icu_677UMemoryE,"aG",@progbits,_ZTSN6icu_677UMemoryE,comdat
	.align 16
	.type	_ZTSN6icu_677UMemoryE, @object
	.size	_ZTSN6icu_677UMemoryE, 18
_ZTSN6icu_677UMemoryE:
	.string	"N6icu_677UMemoryE"
	.weak	_ZTIN6icu_677UMemoryE
	.section	.data.rel.ro._ZTIN6icu_677UMemoryE,"awG",@progbits,_ZTIN6icu_677UMemoryE,comdat
	.align 8
	.type	_ZTIN6icu_677UMemoryE, @object
	.size	_ZTIN6icu_677UMemoryE, 16
_ZTIN6icu_677UMemoryE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN6icu_677UMemoryE
	.weak	_ZTSN6icu_6711NGramParserE
	.section	.rodata._ZTSN6icu_6711NGramParserE,"aG",@progbits,_ZTSN6icu_6711NGramParserE,comdat
	.align 16
	.type	_ZTSN6icu_6711NGramParserE, @object
	.size	_ZTSN6icu_6711NGramParserE, 23
_ZTSN6icu_6711NGramParserE:
	.string	"N6icu_6711NGramParserE"
	.weak	_ZTIN6icu_6711NGramParserE
	.section	.data.rel.ro._ZTIN6icu_6711NGramParserE,"awG",@progbits,_ZTIN6icu_6711NGramParserE,comdat
	.align 8
	.type	_ZTIN6icu_6711NGramParserE, @object
	.size	_ZTIN6icu_6711NGramParserE, 24
_ZTIN6icu_6711NGramParserE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6711NGramParserE
	.quad	_ZTIN6icu_677UMemoryE
	.weak	_ZTSN6icu_6718NGramParser_IBM420E
	.section	.rodata._ZTSN6icu_6718NGramParser_IBM420E,"aG",@progbits,_ZTSN6icu_6718NGramParser_IBM420E,comdat
	.align 16
	.type	_ZTSN6icu_6718NGramParser_IBM420E, @object
	.size	_ZTSN6icu_6718NGramParser_IBM420E, 30
_ZTSN6icu_6718NGramParser_IBM420E:
	.string	"N6icu_6718NGramParser_IBM420E"
	.weak	_ZTIN6icu_6718NGramParser_IBM420E
	.section	.data.rel.ro._ZTIN6icu_6718NGramParser_IBM420E,"awG",@progbits,_ZTIN6icu_6718NGramParser_IBM420E,comdat
	.align 8
	.type	_ZTIN6icu_6718NGramParser_IBM420E, @object
	.size	_ZTIN6icu_6718NGramParser_IBM420E, 24
_ZTIN6icu_6718NGramParser_IBM420E:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6718NGramParser_IBM420E
	.quad	_ZTIN6icu_6711NGramParserE
	.weak	_ZTSN6icu_6717CharsetRecog_sbcsE
	.section	.rodata._ZTSN6icu_6717CharsetRecog_sbcsE,"aG",@progbits,_ZTSN6icu_6717CharsetRecog_sbcsE,comdat
	.align 16
	.type	_ZTSN6icu_6717CharsetRecog_sbcsE, @object
	.size	_ZTSN6icu_6717CharsetRecog_sbcsE, 29
_ZTSN6icu_6717CharsetRecog_sbcsE:
	.string	"N6icu_6717CharsetRecog_sbcsE"
	.weak	_ZTIN6icu_6717CharsetRecog_sbcsE
	.section	.data.rel.ro._ZTIN6icu_6717CharsetRecog_sbcsE,"awG",@progbits,_ZTIN6icu_6717CharsetRecog_sbcsE,comdat
	.align 8
	.type	_ZTIN6icu_6717CharsetRecog_sbcsE, @object
	.size	_ZTIN6icu_6717CharsetRecog_sbcsE, 24
_ZTIN6icu_6717CharsetRecog_sbcsE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6717CharsetRecog_sbcsE
	.quad	_ZTIN6icu_6717CharsetRecognizerE
	.weak	_ZTSN6icu_6719CharsetRecog_8859_1E
	.section	.rodata._ZTSN6icu_6719CharsetRecog_8859_1E,"aG",@progbits,_ZTSN6icu_6719CharsetRecog_8859_1E,comdat
	.align 16
	.type	_ZTSN6icu_6719CharsetRecog_8859_1E, @object
	.size	_ZTSN6icu_6719CharsetRecog_8859_1E, 31
_ZTSN6icu_6719CharsetRecog_8859_1E:
	.string	"N6icu_6719CharsetRecog_8859_1E"
	.weak	_ZTIN6icu_6719CharsetRecog_8859_1E
	.section	.data.rel.ro._ZTIN6icu_6719CharsetRecog_8859_1E,"awG",@progbits,_ZTIN6icu_6719CharsetRecog_8859_1E,comdat
	.align 8
	.type	_ZTIN6icu_6719CharsetRecog_8859_1E, @object
	.size	_ZTIN6icu_6719CharsetRecog_8859_1E, 24
_ZTIN6icu_6719CharsetRecog_8859_1E:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6719CharsetRecog_8859_1E
	.quad	_ZTIN6icu_6717CharsetRecog_sbcsE
	.weak	_ZTSN6icu_6719CharsetRecog_8859_2E
	.section	.rodata._ZTSN6icu_6719CharsetRecog_8859_2E,"aG",@progbits,_ZTSN6icu_6719CharsetRecog_8859_2E,comdat
	.align 16
	.type	_ZTSN6icu_6719CharsetRecog_8859_2E, @object
	.size	_ZTSN6icu_6719CharsetRecog_8859_2E, 31
_ZTSN6icu_6719CharsetRecog_8859_2E:
	.string	"N6icu_6719CharsetRecog_8859_2E"
	.weak	_ZTIN6icu_6719CharsetRecog_8859_2E
	.section	.data.rel.ro._ZTIN6icu_6719CharsetRecog_8859_2E,"awG",@progbits,_ZTIN6icu_6719CharsetRecog_8859_2E,comdat
	.align 8
	.type	_ZTIN6icu_6719CharsetRecog_8859_2E, @object
	.size	_ZTIN6icu_6719CharsetRecog_8859_2E, 24
_ZTIN6icu_6719CharsetRecog_8859_2E:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6719CharsetRecog_8859_2E
	.quad	_ZTIN6icu_6717CharsetRecog_sbcsE
	.weak	_ZTSN6icu_6719CharsetRecog_8859_5E
	.section	.rodata._ZTSN6icu_6719CharsetRecog_8859_5E,"aG",@progbits,_ZTSN6icu_6719CharsetRecog_8859_5E,comdat
	.align 16
	.type	_ZTSN6icu_6719CharsetRecog_8859_5E, @object
	.size	_ZTSN6icu_6719CharsetRecog_8859_5E, 31
_ZTSN6icu_6719CharsetRecog_8859_5E:
	.string	"N6icu_6719CharsetRecog_8859_5E"
	.weak	_ZTIN6icu_6719CharsetRecog_8859_5E
	.section	.data.rel.ro._ZTIN6icu_6719CharsetRecog_8859_5E,"awG",@progbits,_ZTIN6icu_6719CharsetRecog_8859_5E,comdat
	.align 8
	.type	_ZTIN6icu_6719CharsetRecog_8859_5E, @object
	.size	_ZTIN6icu_6719CharsetRecog_8859_5E, 24
_ZTIN6icu_6719CharsetRecog_8859_5E:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6719CharsetRecog_8859_5E
	.quad	_ZTIN6icu_6717CharsetRecog_sbcsE
	.weak	_ZTSN6icu_6719CharsetRecog_8859_6E
	.section	.rodata._ZTSN6icu_6719CharsetRecog_8859_6E,"aG",@progbits,_ZTSN6icu_6719CharsetRecog_8859_6E,comdat
	.align 16
	.type	_ZTSN6icu_6719CharsetRecog_8859_6E, @object
	.size	_ZTSN6icu_6719CharsetRecog_8859_6E, 31
_ZTSN6icu_6719CharsetRecog_8859_6E:
	.string	"N6icu_6719CharsetRecog_8859_6E"
	.weak	_ZTIN6icu_6719CharsetRecog_8859_6E
	.section	.data.rel.ro._ZTIN6icu_6719CharsetRecog_8859_6E,"awG",@progbits,_ZTIN6icu_6719CharsetRecog_8859_6E,comdat
	.align 8
	.type	_ZTIN6icu_6719CharsetRecog_8859_6E, @object
	.size	_ZTIN6icu_6719CharsetRecog_8859_6E, 24
_ZTIN6icu_6719CharsetRecog_8859_6E:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6719CharsetRecog_8859_6E
	.quad	_ZTIN6icu_6717CharsetRecog_sbcsE
	.weak	_ZTSN6icu_6719CharsetRecog_8859_7E
	.section	.rodata._ZTSN6icu_6719CharsetRecog_8859_7E,"aG",@progbits,_ZTSN6icu_6719CharsetRecog_8859_7E,comdat
	.align 16
	.type	_ZTSN6icu_6719CharsetRecog_8859_7E, @object
	.size	_ZTSN6icu_6719CharsetRecog_8859_7E, 31
_ZTSN6icu_6719CharsetRecog_8859_7E:
	.string	"N6icu_6719CharsetRecog_8859_7E"
	.weak	_ZTIN6icu_6719CharsetRecog_8859_7E
	.section	.data.rel.ro._ZTIN6icu_6719CharsetRecog_8859_7E,"awG",@progbits,_ZTIN6icu_6719CharsetRecog_8859_7E,comdat
	.align 8
	.type	_ZTIN6icu_6719CharsetRecog_8859_7E, @object
	.size	_ZTIN6icu_6719CharsetRecog_8859_7E, 24
_ZTIN6icu_6719CharsetRecog_8859_7E:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6719CharsetRecog_8859_7E
	.quad	_ZTIN6icu_6717CharsetRecog_sbcsE
	.weak	_ZTSN6icu_6719CharsetRecog_8859_8E
	.section	.rodata._ZTSN6icu_6719CharsetRecog_8859_8E,"aG",@progbits,_ZTSN6icu_6719CharsetRecog_8859_8E,comdat
	.align 16
	.type	_ZTSN6icu_6719CharsetRecog_8859_8E, @object
	.size	_ZTSN6icu_6719CharsetRecog_8859_8E, 31
_ZTSN6icu_6719CharsetRecog_8859_8E:
	.string	"N6icu_6719CharsetRecog_8859_8E"
	.weak	_ZTIN6icu_6719CharsetRecog_8859_8E
	.section	.data.rel.ro._ZTIN6icu_6719CharsetRecog_8859_8E,"awG",@progbits,_ZTIN6icu_6719CharsetRecog_8859_8E,comdat
	.align 8
	.type	_ZTIN6icu_6719CharsetRecog_8859_8E, @object
	.size	_ZTIN6icu_6719CharsetRecog_8859_8E, 24
_ZTIN6icu_6719CharsetRecog_8859_8E:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6719CharsetRecog_8859_8E
	.quad	_ZTIN6icu_6717CharsetRecog_sbcsE
	.weak	_ZTSN6icu_6719CharsetRecog_8859_9E
	.section	.rodata._ZTSN6icu_6719CharsetRecog_8859_9E,"aG",@progbits,_ZTSN6icu_6719CharsetRecog_8859_9E,comdat
	.align 16
	.type	_ZTSN6icu_6719CharsetRecog_8859_9E, @object
	.size	_ZTSN6icu_6719CharsetRecog_8859_9E, 31
_ZTSN6icu_6719CharsetRecog_8859_9E:
	.string	"N6icu_6719CharsetRecog_8859_9E"
	.weak	_ZTIN6icu_6719CharsetRecog_8859_9E
	.section	.data.rel.ro._ZTIN6icu_6719CharsetRecog_8859_9E,"awG",@progbits,_ZTIN6icu_6719CharsetRecog_8859_9E,comdat
	.align 8
	.type	_ZTIN6icu_6719CharsetRecog_8859_9E, @object
	.size	_ZTIN6icu_6719CharsetRecog_8859_9E, 24
_ZTIN6icu_6719CharsetRecog_8859_9E:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6719CharsetRecog_8859_9E
	.quad	_ZTIN6icu_6717CharsetRecog_sbcsE
	.weak	_ZTSN6icu_6722CharsetRecog_8859_5_ruE
	.section	.rodata._ZTSN6icu_6722CharsetRecog_8859_5_ruE,"aG",@progbits,_ZTSN6icu_6722CharsetRecog_8859_5_ruE,comdat
	.align 32
	.type	_ZTSN6icu_6722CharsetRecog_8859_5_ruE, @object
	.size	_ZTSN6icu_6722CharsetRecog_8859_5_ruE, 34
_ZTSN6icu_6722CharsetRecog_8859_5_ruE:
	.string	"N6icu_6722CharsetRecog_8859_5_ruE"
	.weak	_ZTIN6icu_6722CharsetRecog_8859_5_ruE
	.section	.data.rel.ro._ZTIN6icu_6722CharsetRecog_8859_5_ruE,"awG",@progbits,_ZTIN6icu_6722CharsetRecog_8859_5_ruE,comdat
	.align 8
	.type	_ZTIN6icu_6722CharsetRecog_8859_5_ruE, @object
	.size	_ZTIN6icu_6722CharsetRecog_8859_5_ruE, 24
_ZTIN6icu_6722CharsetRecog_8859_5_ruE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6722CharsetRecog_8859_5_ruE
	.quad	_ZTIN6icu_6719CharsetRecog_8859_5E
	.weak	_ZTSN6icu_6722CharsetRecog_8859_6_arE
	.section	.rodata._ZTSN6icu_6722CharsetRecog_8859_6_arE,"aG",@progbits,_ZTSN6icu_6722CharsetRecog_8859_6_arE,comdat
	.align 32
	.type	_ZTSN6icu_6722CharsetRecog_8859_6_arE, @object
	.size	_ZTSN6icu_6722CharsetRecog_8859_6_arE, 34
_ZTSN6icu_6722CharsetRecog_8859_6_arE:
	.string	"N6icu_6722CharsetRecog_8859_6_arE"
	.weak	_ZTIN6icu_6722CharsetRecog_8859_6_arE
	.section	.data.rel.ro._ZTIN6icu_6722CharsetRecog_8859_6_arE,"awG",@progbits,_ZTIN6icu_6722CharsetRecog_8859_6_arE,comdat
	.align 8
	.type	_ZTIN6icu_6722CharsetRecog_8859_6_arE, @object
	.size	_ZTIN6icu_6722CharsetRecog_8859_6_arE, 24
_ZTIN6icu_6722CharsetRecog_8859_6_arE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6722CharsetRecog_8859_6_arE
	.quad	_ZTIN6icu_6719CharsetRecog_8859_6E
	.weak	_ZTSN6icu_6722CharsetRecog_8859_7_elE
	.section	.rodata._ZTSN6icu_6722CharsetRecog_8859_7_elE,"aG",@progbits,_ZTSN6icu_6722CharsetRecog_8859_7_elE,comdat
	.align 32
	.type	_ZTSN6icu_6722CharsetRecog_8859_7_elE, @object
	.size	_ZTSN6icu_6722CharsetRecog_8859_7_elE, 34
_ZTSN6icu_6722CharsetRecog_8859_7_elE:
	.string	"N6icu_6722CharsetRecog_8859_7_elE"
	.weak	_ZTIN6icu_6722CharsetRecog_8859_7_elE
	.section	.data.rel.ro._ZTIN6icu_6722CharsetRecog_8859_7_elE,"awG",@progbits,_ZTIN6icu_6722CharsetRecog_8859_7_elE,comdat
	.align 8
	.type	_ZTIN6icu_6722CharsetRecog_8859_7_elE, @object
	.size	_ZTIN6icu_6722CharsetRecog_8859_7_elE, 24
_ZTIN6icu_6722CharsetRecog_8859_7_elE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6722CharsetRecog_8859_7_elE
	.quad	_ZTIN6icu_6719CharsetRecog_8859_7E
	.weak	_ZTSN6icu_6724CharsetRecog_8859_8_I_heE
	.section	.rodata._ZTSN6icu_6724CharsetRecog_8859_8_I_heE,"aG",@progbits,_ZTSN6icu_6724CharsetRecog_8859_8_I_heE,comdat
	.align 32
	.type	_ZTSN6icu_6724CharsetRecog_8859_8_I_heE, @object
	.size	_ZTSN6icu_6724CharsetRecog_8859_8_I_heE, 36
_ZTSN6icu_6724CharsetRecog_8859_8_I_heE:
	.string	"N6icu_6724CharsetRecog_8859_8_I_heE"
	.weak	_ZTIN6icu_6724CharsetRecog_8859_8_I_heE
	.section	.data.rel.ro._ZTIN6icu_6724CharsetRecog_8859_8_I_heE,"awG",@progbits,_ZTIN6icu_6724CharsetRecog_8859_8_I_heE,comdat
	.align 8
	.type	_ZTIN6icu_6724CharsetRecog_8859_8_I_heE, @object
	.size	_ZTIN6icu_6724CharsetRecog_8859_8_I_heE, 24
_ZTIN6icu_6724CharsetRecog_8859_8_I_heE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6724CharsetRecog_8859_8_I_heE
	.quad	_ZTIN6icu_6719CharsetRecog_8859_8E
	.weak	_ZTSN6icu_6722CharsetRecog_8859_8_heE
	.section	.rodata._ZTSN6icu_6722CharsetRecog_8859_8_heE,"aG",@progbits,_ZTSN6icu_6722CharsetRecog_8859_8_heE,comdat
	.align 32
	.type	_ZTSN6icu_6722CharsetRecog_8859_8_heE, @object
	.size	_ZTSN6icu_6722CharsetRecog_8859_8_heE, 34
_ZTSN6icu_6722CharsetRecog_8859_8_heE:
	.string	"N6icu_6722CharsetRecog_8859_8_heE"
	.weak	_ZTIN6icu_6722CharsetRecog_8859_8_heE
	.section	.data.rel.ro._ZTIN6icu_6722CharsetRecog_8859_8_heE,"awG",@progbits,_ZTIN6icu_6722CharsetRecog_8859_8_heE,comdat
	.align 8
	.type	_ZTIN6icu_6722CharsetRecog_8859_8_heE, @object
	.size	_ZTIN6icu_6722CharsetRecog_8859_8_heE, 24
_ZTIN6icu_6722CharsetRecog_8859_8_heE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6722CharsetRecog_8859_8_heE
	.quad	_ZTIN6icu_6719CharsetRecog_8859_8E
	.weak	_ZTSN6icu_6722CharsetRecog_8859_9_trE
	.section	.rodata._ZTSN6icu_6722CharsetRecog_8859_9_trE,"aG",@progbits,_ZTSN6icu_6722CharsetRecog_8859_9_trE,comdat
	.align 32
	.type	_ZTSN6icu_6722CharsetRecog_8859_9_trE, @object
	.size	_ZTSN6icu_6722CharsetRecog_8859_9_trE, 34
_ZTSN6icu_6722CharsetRecog_8859_9_trE:
	.string	"N6icu_6722CharsetRecog_8859_9_trE"
	.weak	_ZTIN6icu_6722CharsetRecog_8859_9_trE
	.section	.data.rel.ro._ZTIN6icu_6722CharsetRecog_8859_9_trE,"awG",@progbits,_ZTIN6icu_6722CharsetRecog_8859_9_trE,comdat
	.align 8
	.type	_ZTIN6icu_6722CharsetRecog_8859_9_trE, @object
	.size	_ZTIN6icu_6722CharsetRecog_8859_9_trE, 24
_ZTIN6icu_6722CharsetRecog_8859_9_trE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6722CharsetRecog_8859_9_trE
	.quad	_ZTIN6icu_6719CharsetRecog_8859_9E
	.weak	_ZTSN6icu_6725CharsetRecog_windows_1256E
	.section	.rodata._ZTSN6icu_6725CharsetRecog_windows_1256E,"aG",@progbits,_ZTSN6icu_6725CharsetRecog_windows_1256E,comdat
	.align 32
	.type	_ZTSN6icu_6725CharsetRecog_windows_1256E, @object
	.size	_ZTSN6icu_6725CharsetRecog_windows_1256E, 37
_ZTSN6icu_6725CharsetRecog_windows_1256E:
	.string	"N6icu_6725CharsetRecog_windows_1256E"
	.weak	_ZTIN6icu_6725CharsetRecog_windows_1256E
	.section	.data.rel.ro._ZTIN6icu_6725CharsetRecog_windows_1256E,"awG",@progbits,_ZTIN6icu_6725CharsetRecog_windows_1256E,comdat
	.align 8
	.type	_ZTIN6icu_6725CharsetRecog_windows_1256E, @object
	.size	_ZTIN6icu_6725CharsetRecog_windows_1256E, 24
_ZTIN6icu_6725CharsetRecog_windows_1256E:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6725CharsetRecog_windows_1256E
	.quad	_ZTIN6icu_6717CharsetRecog_sbcsE
	.weak	_ZTSN6icu_6725CharsetRecog_windows_1251E
	.section	.rodata._ZTSN6icu_6725CharsetRecog_windows_1251E,"aG",@progbits,_ZTSN6icu_6725CharsetRecog_windows_1251E,comdat
	.align 32
	.type	_ZTSN6icu_6725CharsetRecog_windows_1251E, @object
	.size	_ZTSN6icu_6725CharsetRecog_windows_1251E, 37
_ZTSN6icu_6725CharsetRecog_windows_1251E:
	.string	"N6icu_6725CharsetRecog_windows_1251E"
	.weak	_ZTIN6icu_6725CharsetRecog_windows_1251E
	.section	.data.rel.ro._ZTIN6icu_6725CharsetRecog_windows_1251E,"awG",@progbits,_ZTIN6icu_6725CharsetRecog_windows_1251E,comdat
	.align 8
	.type	_ZTIN6icu_6725CharsetRecog_windows_1251E, @object
	.size	_ZTIN6icu_6725CharsetRecog_windows_1251E, 24
_ZTIN6icu_6725CharsetRecog_windows_1251E:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6725CharsetRecog_windows_1251E
	.quad	_ZTIN6icu_6717CharsetRecog_sbcsE
	.weak	_ZTSN6icu_6719CharsetRecog_KOI8_RE
	.section	.rodata._ZTSN6icu_6719CharsetRecog_KOI8_RE,"aG",@progbits,_ZTSN6icu_6719CharsetRecog_KOI8_RE,comdat
	.align 16
	.type	_ZTSN6icu_6719CharsetRecog_KOI8_RE, @object
	.size	_ZTSN6icu_6719CharsetRecog_KOI8_RE, 31
_ZTSN6icu_6719CharsetRecog_KOI8_RE:
	.string	"N6icu_6719CharsetRecog_KOI8_RE"
	.weak	_ZTIN6icu_6719CharsetRecog_KOI8_RE
	.section	.data.rel.ro._ZTIN6icu_6719CharsetRecog_KOI8_RE,"awG",@progbits,_ZTIN6icu_6719CharsetRecog_KOI8_RE,comdat
	.align 8
	.type	_ZTIN6icu_6719CharsetRecog_KOI8_RE, @object
	.size	_ZTIN6icu_6719CharsetRecog_KOI8_RE, 24
_ZTIN6icu_6719CharsetRecog_KOI8_RE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6719CharsetRecog_KOI8_RE
	.quad	_ZTIN6icu_6717CharsetRecog_sbcsE
	.weak	_ZTSN6icu_6722CharsetRecog_IBM424_heE
	.section	.rodata._ZTSN6icu_6722CharsetRecog_IBM424_heE,"aG",@progbits,_ZTSN6icu_6722CharsetRecog_IBM424_heE,comdat
	.align 32
	.type	_ZTSN6icu_6722CharsetRecog_IBM424_heE, @object
	.size	_ZTSN6icu_6722CharsetRecog_IBM424_heE, 34
_ZTSN6icu_6722CharsetRecog_IBM424_heE:
	.string	"N6icu_6722CharsetRecog_IBM424_heE"
	.weak	_ZTIN6icu_6722CharsetRecog_IBM424_heE
	.section	.data.rel.ro._ZTIN6icu_6722CharsetRecog_IBM424_heE,"awG",@progbits,_ZTIN6icu_6722CharsetRecog_IBM424_heE,comdat
	.align 8
	.type	_ZTIN6icu_6722CharsetRecog_IBM424_heE, @object
	.size	_ZTIN6icu_6722CharsetRecog_IBM424_heE, 24
_ZTIN6icu_6722CharsetRecog_IBM424_heE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6722CharsetRecog_IBM424_heE
	.quad	_ZTIN6icu_6717CharsetRecog_sbcsE
	.weak	_ZTSN6icu_6726CharsetRecog_IBM424_he_rtlE
	.section	.rodata._ZTSN6icu_6726CharsetRecog_IBM424_he_rtlE,"aG",@progbits,_ZTSN6icu_6726CharsetRecog_IBM424_he_rtlE,comdat
	.align 32
	.type	_ZTSN6icu_6726CharsetRecog_IBM424_he_rtlE, @object
	.size	_ZTSN6icu_6726CharsetRecog_IBM424_he_rtlE, 38
_ZTSN6icu_6726CharsetRecog_IBM424_he_rtlE:
	.string	"N6icu_6726CharsetRecog_IBM424_he_rtlE"
	.weak	_ZTIN6icu_6726CharsetRecog_IBM424_he_rtlE
	.section	.data.rel.ro._ZTIN6icu_6726CharsetRecog_IBM424_he_rtlE,"awG",@progbits,_ZTIN6icu_6726CharsetRecog_IBM424_he_rtlE,comdat
	.align 8
	.type	_ZTIN6icu_6726CharsetRecog_IBM424_he_rtlE, @object
	.size	_ZTIN6icu_6726CharsetRecog_IBM424_he_rtlE, 24
_ZTIN6icu_6726CharsetRecog_IBM424_he_rtlE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6726CharsetRecog_IBM424_he_rtlE
	.quad	_ZTIN6icu_6722CharsetRecog_IBM424_heE
	.weak	_ZTSN6icu_6726CharsetRecog_IBM424_he_ltrE
	.section	.rodata._ZTSN6icu_6726CharsetRecog_IBM424_he_ltrE,"aG",@progbits,_ZTSN6icu_6726CharsetRecog_IBM424_he_ltrE,comdat
	.align 32
	.type	_ZTSN6icu_6726CharsetRecog_IBM424_he_ltrE, @object
	.size	_ZTSN6icu_6726CharsetRecog_IBM424_he_ltrE, 38
_ZTSN6icu_6726CharsetRecog_IBM424_he_ltrE:
	.string	"N6icu_6726CharsetRecog_IBM424_he_ltrE"
	.weak	_ZTIN6icu_6726CharsetRecog_IBM424_he_ltrE
	.section	.data.rel.ro._ZTIN6icu_6726CharsetRecog_IBM424_he_ltrE,"awG",@progbits,_ZTIN6icu_6726CharsetRecog_IBM424_he_ltrE,comdat
	.align 8
	.type	_ZTIN6icu_6726CharsetRecog_IBM424_he_ltrE, @object
	.size	_ZTIN6icu_6726CharsetRecog_IBM424_he_ltrE, 24
_ZTIN6icu_6726CharsetRecog_IBM424_he_ltrE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6726CharsetRecog_IBM424_he_ltrE
	.quad	_ZTIN6icu_6722CharsetRecog_IBM424_heE
	.weak	_ZTSN6icu_6722CharsetRecog_IBM420_arE
	.section	.rodata._ZTSN6icu_6722CharsetRecog_IBM420_arE,"aG",@progbits,_ZTSN6icu_6722CharsetRecog_IBM420_arE,comdat
	.align 32
	.type	_ZTSN6icu_6722CharsetRecog_IBM420_arE, @object
	.size	_ZTSN6icu_6722CharsetRecog_IBM420_arE, 34
_ZTSN6icu_6722CharsetRecog_IBM420_arE:
	.string	"N6icu_6722CharsetRecog_IBM420_arE"
	.weak	_ZTIN6icu_6722CharsetRecog_IBM420_arE
	.section	.data.rel.ro._ZTIN6icu_6722CharsetRecog_IBM420_arE,"awG",@progbits,_ZTIN6icu_6722CharsetRecog_IBM420_arE,comdat
	.align 8
	.type	_ZTIN6icu_6722CharsetRecog_IBM420_arE, @object
	.size	_ZTIN6icu_6722CharsetRecog_IBM420_arE, 24
_ZTIN6icu_6722CharsetRecog_IBM420_arE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6722CharsetRecog_IBM420_arE
	.quad	_ZTIN6icu_6717CharsetRecog_sbcsE
	.weak	_ZTSN6icu_6726CharsetRecog_IBM420_ar_rtlE
	.section	.rodata._ZTSN6icu_6726CharsetRecog_IBM420_ar_rtlE,"aG",@progbits,_ZTSN6icu_6726CharsetRecog_IBM420_ar_rtlE,comdat
	.align 32
	.type	_ZTSN6icu_6726CharsetRecog_IBM420_ar_rtlE, @object
	.size	_ZTSN6icu_6726CharsetRecog_IBM420_ar_rtlE, 38
_ZTSN6icu_6726CharsetRecog_IBM420_ar_rtlE:
	.string	"N6icu_6726CharsetRecog_IBM420_ar_rtlE"
	.weak	_ZTIN6icu_6726CharsetRecog_IBM420_ar_rtlE
	.section	.data.rel.ro._ZTIN6icu_6726CharsetRecog_IBM420_ar_rtlE,"awG",@progbits,_ZTIN6icu_6726CharsetRecog_IBM420_ar_rtlE,comdat
	.align 8
	.type	_ZTIN6icu_6726CharsetRecog_IBM420_ar_rtlE, @object
	.size	_ZTIN6icu_6726CharsetRecog_IBM420_ar_rtlE, 24
_ZTIN6icu_6726CharsetRecog_IBM420_ar_rtlE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6726CharsetRecog_IBM420_ar_rtlE
	.quad	_ZTIN6icu_6722CharsetRecog_IBM420_arE
	.weak	_ZTSN6icu_6726CharsetRecog_IBM420_ar_ltrE
	.section	.rodata._ZTSN6icu_6726CharsetRecog_IBM420_ar_ltrE,"aG",@progbits,_ZTSN6icu_6726CharsetRecog_IBM420_ar_ltrE,comdat
	.align 32
	.type	_ZTSN6icu_6726CharsetRecog_IBM420_ar_ltrE, @object
	.size	_ZTSN6icu_6726CharsetRecog_IBM420_ar_ltrE, 38
_ZTSN6icu_6726CharsetRecog_IBM420_ar_ltrE:
	.string	"N6icu_6726CharsetRecog_IBM420_ar_ltrE"
	.weak	_ZTIN6icu_6726CharsetRecog_IBM420_ar_ltrE
	.section	.data.rel.ro._ZTIN6icu_6726CharsetRecog_IBM420_ar_ltrE,"awG",@progbits,_ZTIN6icu_6726CharsetRecog_IBM420_ar_ltrE,comdat
	.align 8
	.type	_ZTIN6icu_6726CharsetRecog_IBM420_ar_ltrE, @object
	.size	_ZTIN6icu_6726CharsetRecog_IBM420_ar_ltrE, 24
_ZTIN6icu_6726CharsetRecog_IBM420_ar_ltrE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6726CharsetRecog_IBM420_ar_ltrE
	.quad	_ZTIN6icu_6722CharsetRecog_IBM420_arE
	.weak	_ZTVN6icu_6711NGramParserE
	.section	.data.rel.ro.local._ZTVN6icu_6711NGramParserE,"awG",@progbits,_ZTVN6icu_6711NGramParserE,comdat
	.align 8
	.type	_ZTVN6icu_6711NGramParserE, @object
	.size	_ZTVN6icu_6711NGramParserE, 48
_ZTVN6icu_6711NGramParserE:
	.quad	0
	.quad	_ZTIN6icu_6711NGramParserE
	.quad	_ZN6icu_6711NGramParserD1Ev
	.quad	_ZN6icu_6711NGramParserD0Ev
	.quad	_ZN6icu_6711NGramParser8nextByteEPNS_9InputTextE
	.quad	_ZN6icu_6711NGramParser15parseCharactersEPNS_9InputTextE
	.weak	_ZTVN6icu_6718NGramParser_IBM420E
	.section	.data.rel.ro.local._ZTVN6icu_6718NGramParser_IBM420E,"awG",@progbits,_ZTVN6icu_6718NGramParser_IBM420E,comdat
	.align 8
	.type	_ZTVN6icu_6718NGramParser_IBM420E, @object
	.size	_ZTVN6icu_6718NGramParser_IBM420E, 48
_ZTVN6icu_6718NGramParser_IBM420E:
	.quad	0
	.quad	_ZTIN6icu_6718NGramParser_IBM420E
	.quad	_ZN6icu_6718NGramParser_IBM420D1Ev
	.quad	_ZN6icu_6718NGramParser_IBM420D0Ev
	.quad	_ZN6icu_6718NGramParser_IBM4208nextByteEPNS_9InputTextE
	.quad	_ZN6icu_6718NGramParser_IBM42015parseCharactersEPNS_9InputTextE
	.weak	_ZTVN6icu_6717CharsetRecog_sbcsE
	.section	.data.rel.ro._ZTVN6icu_6717CharsetRecog_sbcsE,"awG",@progbits,_ZTVN6icu_6717CharsetRecog_sbcsE,comdat
	.align 8
	.type	_ZTVN6icu_6717CharsetRecog_sbcsE, @object
	.size	_ZTVN6icu_6717CharsetRecog_sbcsE, 64
_ZTVN6icu_6717CharsetRecog_sbcsE:
	.quad	0
	.quad	_ZTIN6icu_6717CharsetRecog_sbcsE
	.quad	__cxa_pure_virtual
	.quad	_ZNK6icu_6717CharsetRecognizer11getLanguageEv
	.quad	__cxa_pure_virtual
	.quad	0
	.quad	0
	.quad	_ZNK6icu_6717CharsetRecog_sbcs10match_sbcsEPNS_9InputTextEPKiPKh
	.weak	_ZTVN6icu_6719CharsetRecog_8859_1E
	.section	.data.rel.ro._ZTVN6icu_6719CharsetRecog_8859_1E,"awG",@progbits,_ZTVN6icu_6719CharsetRecog_8859_1E,comdat
	.align 8
	.type	_ZTVN6icu_6719CharsetRecog_8859_1E, @object
	.size	_ZTVN6icu_6719CharsetRecog_8859_1E, 64
_ZTVN6icu_6719CharsetRecog_8859_1E:
	.quad	0
	.quad	_ZTIN6icu_6719CharsetRecog_8859_1E
	.quad	_ZNK6icu_6719CharsetRecog_8859_17getNameEv
	.quad	_ZNK6icu_6717CharsetRecognizer11getLanguageEv
	.quad	_ZNK6icu_6719CharsetRecog_8859_15matchEPNS_9InputTextEPNS_12CharsetMatchE
	.quad	_ZN6icu_6719CharsetRecog_8859_1D1Ev
	.quad	_ZN6icu_6719CharsetRecog_8859_1D0Ev
	.quad	_ZNK6icu_6717CharsetRecog_sbcs10match_sbcsEPNS_9InputTextEPKiPKh
	.weak	_ZTVN6icu_6719CharsetRecog_8859_2E
	.section	.data.rel.ro._ZTVN6icu_6719CharsetRecog_8859_2E,"awG",@progbits,_ZTVN6icu_6719CharsetRecog_8859_2E,comdat
	.align 8
	.type	_ZTVN6icu_6719CharsetRecog_8859_2E, @object
	.size	_ZTVN6icu_6719CharsetRecog_8859_2E, 64
_ZTVN6icu_6719CharsetRecog_8859_2E:
	.quad	0
	.quad	_ZTIN6icu_6719CharsetRecog_8859_2E
	.quad	_ZNK6icu_6719CharsetRecog_8859_27getNameEv
	.quad	_ZNK6icu_6717CharsetRecognizer11getLanguageEv
	.quad	_ZNK6icu_6719CharsetRecog_8859_25matchEPNS_9InputTextEPNS_12CharsetMatchE
	.quad	_ZN6icu_6719CharsetRecog_8859_2D1Ev
	.quad	_ZN6icu_6719CharsetRecog_8859_2D0Ev
	.quad	_ZNK6icu_6717CharsetRecog_sbcs10match_sbcsEPNS_9InputTextEPKiPKh
	.weak	_ZTVN6icu_6719CharsetRecog_8859_5E
	.section	.data.rel.ro._ZTVN6icu_6719CharsetRecog_8859_5E,"awG",@progbits,_ZTVN6icu_6719CharsetRecog_8859_5E,comdat
	.align 8
	.type	_ZTVN6icu_6719CharsetRecog_8859_5E, @object
	.size	_ZTVN6icu_6719CharsetRecog_8859_5E, 64
_ZTVN6icu_6719CharsetRecog_8859_5E:
	.quad	0
	.quad	_ZTIN6icu_6719CharsetRecog_8859_5E
	.quad	_ZNK6icu_6719CharsetRecog_8859_57getNameEv
	.quad	_ZNK6icu_6717CharsetRecognizer11getLanguageEv
	.quad	__cxa_pure_virtual
	.quad	0
	.quad	0
	.quad	_ZNK6icu_6717CharsetRecog_sbcs10match_sbcsEPNS_9InputTextEPKiPKh
	.weak	_ZTVN6icu_6722CharsetRecog_8859_5_ruE
	.section	.data.rel.ro.local._ZTVN6icu_6722CharsetRecog_8859_5_ruE,"awG",@progbits,_ZTVN6icu_6722CharsetRecog_8859_5_ruE,comdat
	.align 8
	.type	_ZTVN6icu_6722CharsetRecog_8859_5_ruE, @object
	.size	_ZTVN6icu_6722CharsetRecog_8859_5_ruE, 64
_ZTVN6icu_6722CharsetRecog_8859_5_ruE:
	.quad	0
	.quad	_ZTIN6icu_6722CharsetRecog_8859_5_ruE
	.quad	_ZNK6icu_6719CharsetRecog_8859_57getNameEv
	.quad	_ZNK6icu_6722CharsetRecog_8859_5_ru11getLanguageEv
	.quad	_ZNK6icu_6722CharsetRecog_8859_5_ru5matchEPNS_9InputTextEPNS_12CharsetMatchE
	.quad	_ZN6icu_6722CharsetRecog_8859_5_ruD1Ev
	.quad	_ZN6icu_6722CharsetRecog_8859_5_ruD0Ev
	.quad	_ZNK6icu_6717CharsetRecog_sbcs10match_sbcsEPNS_9InputTextEPKiPKh
	.weak	_ZTVN6icu_6719CharsetRecog_8859_6E
	.section	.data.rel.ro._ZTVN6icu_6719CharsetRecog_8859_6E,"awG",@progbits,_ZTVN6icu_6719CharsetRecog_8859_6E,comdat
	.align 8
	.type	_ZTVN6icu_6719CharsetRecog_8859_6E, @object
	.size	_ZTVN6icu_6719CharsetRecog_8859_6E, 64
_ZTVN6icu_6719CharsetRecog_8859_6E:
	.quad	0
	.quad	_ZTIN6icu_6719CharsetRecog_8859_6E
	.quad	_ZNK6icu_6719CharsetRecog_8859_67getNameEv
	.quad	_ZNK6icu_6717CharsetRecognizer11getLanguageEv
	.quad	__cxa_pure_virtual
	.quad	0
	.quad	0
	.quad	_ZNK6icu_6717CharsetRecog_sbcs10match_sbcsEPNS_9InputTextEPKiPKh
	.weak	_ZTVN6icu_6722CharsetRecog_8859_6_arE
	.section	.data.rel.ro.local._ZTVN6icu_6722CharsetRecog_8859_6_arE,"awG",@progbits,_ZTVN6icu_6722CharsetRecog_8859_6_arE,comdat
	.align 8
	.type	_ZTVN6icu_6722CharsetRecog_8859_6_arE, @object
	.size	_ZTVN6icu_6722CharsetRecog_8859_6_arE, 64
_ZTVN6icu_6722CharsetRecog_8859_6_arE:
	.quad	0
	.quad	_ZTIN6icu_6722CharsetRecog_8859_6_arE
	.quad	_ZNK6icu_6719CharsetRecog_8859_67getNameEv
	.quad	_ZNK6icu_6722CharsetRecog_8859_6_ar11getLanguageEv
	.quad	_ZNK6icu_6722CharsetRecog_8859_6_ar5matchEPNS_9InputTextEPNS_12CharsetMatchE
	.quad	_ZN6icu_6722CharsetRecog_8859_6_arD1Ev
	.quad	_ZN6icu_6722CharsetRecog_8859_6_arD0Ev
	.quad	_ZNK6icu_6717CharsetRecog_sbcs10match_sbcsEPNS_9InputTextEPKiPKh
	.weak	_ZTVN6icu_6719CharsetRecog_8859_7E
	.section	.data.rel.ro._ZTVN6icu_6719CharsetRecog_8859_7E,"awG",@progbits,_ZTVN6icu_6719CharsetRecog_8859_7E,comdat
	.align 8
	.type	_ZTVN6icu_6719CharsetRecog_8859_7E, @object
	.size	_ZTVN6icu_6719CharsetRecog_8859_7E, 64
_ZTVN6icu_6719CharsetRecog_8859_7E:
	.quad	0
	.quad	_ZTIN6icu_6719CharsetRecog_8859_7E
	.quad	_ZNK6icu_6719CharsetRecog_8859_77getNameEv
	.quad	_ZNK6icu_6717CharsetRecognizer11getLanguageEv
	.quad	__cxa_pure_virtual
	.quad	0
	.quad	0
	.quad	_ZNK6icu_6717CharsetRecog_sbcs10match_sbcsEPNS_9InputTextEPKiPKh
	.weak	_ZTVN6icu_6722CharsetRecog_8859_7_elE
	.section	.data.rel.ro.local._ZTVN6icu_6722CharsetRecog_8859_7_elE,"awG",@progbits,_ZTVN6icu_6722CharsetRecog_8859_7_elE,comdat
	.align 8
	.type	_ZTVN6icu_6722CharsetRecog_8859_7_elE, @object
	.size	_ZTVN6icu_6722CharsetRecog_8859_7_elE, 64
_ZTVN6icu_6722CharsetRecog_8859_7_elE:
	.quad	0
	.quad	_ZTIN6icu_6722CharsetRecog_8859_7_elE
	.quad	_ZNK6icu_6719CharsetRecog_8859_77getNameEv
	.quad	_ZNK6icu_6722CharsetRecog_8859_7_el11getLanguageEv
	.quad	_ZNK6icu_6722CharsetRecog_8859_7_el5matchEPNS_9InputTextEPNS_12CharsetMatchE
	.quad	_ZN6icu_6722CharsetRecog_8859_7_elD1Ev
	.quad	_ZN6icu_6722CharsetRecog_8859_7_elD0Ev
	.quad	_ZNK6icu_6717CharsetRecog_sbcs10match_sbcsEPNS_9InputTextEPKiPKh
	.weak	_ZTVN6icu_6719CharsetRecog_8859_8E
	.section	.data.rel.ro._ZTVN6icu_6719CharsetRecog_8859_8E,"awG",@progbits,_ZTVN6icu_6719CharsetRecog_8859_8E,comdat
	.align 8
	.type	_ZTVN6icu_6719CharsetRecog_8859_8E, @object
	.size	_ZTVN6icu_6719CharsetRecog_8859_8E, 64
_ZTVN6icu_6719CharsetRecog_8859_8E:
	.quad	0
	.quad	_ZTIN6icu_6719CharsetRecog_8859_8E
	.quad	_ZNK6icu_6719CharsetRecog_8859_87getNameEv
	.quad	_ZNK6icu_6717CharsetRecognizer11getLanguageEv
	.quad	__cxa_pure_virtual
	.quad	0
	.quad	0
	.quad	_ZNK6icu_6717CharsetRecog_sbcs10match_sbcsEPNS_9InputTextEPKiPKh
	.weak	_ZTVN6icu_6724CharsetRecog_8859_8_I_heE
	.section	.data.rel.ro.local._ZTVN6icu_6724CharsetRecog_8859_8_I_heE,"awG",@progbits,_ZTVN6icu_6724CharsetRecog_8859_8_I_heE,comdat
	.align 8
	.type	_ZTVN6icu_6724CharsetRecog_8859_8_I_heE, @object
	.size	_ZTVN6icu_6724CharsetRecog_8859_8_I_heE, 64
_ZTVN6icu_6724CharsetRecog_8859_8_I_heE:
	.quad	0
	.quad	_ZTIN6icu_6724CharsetRecog_8859_8_I_heE
	.quad	_ZNK6icu_6724CharsetRecog_8859_8_I_he7getNameEv
	.quad	_ZNK6icu_6724CharsetRecog_8859_8_I_he11getLanguageEv
	.quad	_ZNK6icu_6724CharsetRecog_8859_8_I_he5matchEPNS_9InputTextEPNS_12CharsetMatchE
	.quad	_ZN6icu_6724CharsetRecog_8859_8_I_heD1Ev
	.quad	_ZN6icu_6724CharsetRecog_8859_8_I_heD0Ev
	.quad	_ZNK6icu_6717CharsetRecog_sbcs10match_sbcsEPNS_9InputTextEPKiPKh
	.weak	_ZTVN6icu_6722CharsetRecog_8859_8_heE
	.section	.data.rel.ro.local._ZTVN6icu_6722CharsetRecog_8859_8_heE,"awG",@progbits,_ZTVN6icu_6722CharsetRecog_8859_8_heE,comdat
	.align 8
	.type	_ZTVN6icu_6722CharsetRecog_8859_8_heE, @object
	.size	_ZTVN6icu_6722CharsetRecog_8859_8_heE, 64
_ZTVN6icu_6722CharsetRecog_8859_8_heE:
	.quad	0
	.quad	_ZTIN6icu_6722CharsetRecog_8859_8_heE
	.quad	_ZNK6icu_6719CharsetRecog_8859_87getNameEv
	.quad	_ZNK6icu_6722CharsetRecog_8859_8_he11getLanguageEv
	.quad	_ZNK6icu_6722CharsetRecog_8859_8_he5matchEPNS_9InputTextEPNS_12CharsetMatchE
	.quad	_ZN6icu_6722CharsetRecog_8859_8_heD1Ev
	.quad	_ZN6icu_6722CharsetRecog_8859_8_heD0Ev
	.quad	_ZNK6icu_6717CharsetRecog_sbcs10match_sbcsEPNS_9InputTextEPKiPKh
	.weak	_ZTVN6icu_6719CharsetRecog_8859_9E
	.section	.data.rel.ro._ZTVN6icu_6719CharsetRecog_8859_9E,"awG",@progbits,_ZTVN6icu_6719CharsetRecog_8859_9E,comdat
	.align 8
	.type	_ZTVN6icu_6719CharsetRecog_8859_9E, @object
	.size	_ZTVN6icu_6719CharsetRecog_8859_9E, 64
_ZTVN6icu_6719CharsetRecog_8859_9E:
	.quad	0
	.quad	_ZTIN6icu_6719CharsetRecog_8859_9E
	.quad	_ZNK6icu_6719CharsetRecog_8859_97getNameEv
	.quad	_ZNK6icu_6717CharsetRecognizer11getLanguageEv
	.quad	__cxa_pure_virtual
	.quad	0
	.quad	0
	.quad	_ZNK6icu_6717CharsetRecog_sbcs10match_sbcsEPNS_9InputTextEPKiPKh
	.weak	_ZTVN6icu_6722CharsetRecog_8859_9_trE
	.section	.data.rel.ro.local._ZTVN6icu_6722CharsetRecog_8859_9_trE,"awG",@progbits,_ZTVN6icu_6722CharsetRecog_8859_9_trE,comdat
	.align 8
	.type	_ZTVN6icu_6722CharsetRecog_8859_9_trE, @object
	.size	_ZTVN6icu_6722CharsetRecog_8859_9_trE, 64
_ZTVN6icu_6722CharsetRecog_8859_9_trE:
	.quad	0
	.quad	_ZTIN6icu_6722CharsetRecog_8859_9_trE
	.quad	_ZNK6icu_6719CharsetRecog_8859_97getNameEv
	.quad	_ZNK6icu_6722CharsetRecog_8859_9_tr11getLanguageEv
	.quad	_ZNK6icu_6722CharsetRecog_8859_9_tr5matchEPNS_9InputTextEPNS_12CharsetMatchE
	.quad	_ZN6icu_6722CharsetRecog_8859_9_trD1Ev
	.quad	_ZN6icu_6722CharsetRecog_8859_9_trD0Ev
	.quad	_ZNK6icu_6717CharsetRecog_sbcs10match_sbcsEPNS_9InputTextEPKiPKh
	.weak	_ZTVN6icu_6725CharsetRecog_windows_1256E
	.section	.data.rel.ro.local._ZTVN6icu_6725CharsetRecog_windows_1256E,"awG",@progbits,_ZTVN6icu_6725CharsetRecog_windows_1256E,comdat
	.align 8
	.type	_ZTVN6icu_6725CharsetRecog_windows_1256E, @object
	.size	_ZTVN6icu_6725CharsetRecog_windows_1256E, 64
_ZTVN6icu_6725CharsetRecog_windows_1256E:
	.quad	0
	.quad	_ZTIN6icu_6725CharsetRecog_windows_1256E
	.quad	_ZNK6icu_6725CharsetRecog_windows_12567getNameEv
	.quad	_ZNK6icu_6725CharsetRecog_windows_125611getLanguageEv
	.quad	_ZNK6icu_6725CharsetRecog_windows_12565matchEPNS_9InputTextEPNS_12CharsetMatchE
	.quad	_ZN6icu_6725CharsetRecog_windows_1256D1Ev
	.quad	_ZN6icu_6725CharsetRecog_windows_1256D0Ev
	.quad	_ZNK6icu_6717CharsetRecog_sbcs10match_sbcsEPNS_9InputTextEPKiPKh
	.weak	_ZTVN6icu_6725CharsetRecog_windows_1251E
	.section	.data.rel.ro.local._ZTVN6icu_6725CharsetRecog_windows_1251E,"awG",@progbits,_ZTVN6icu_6725CharsetRecog_windows_1251E,comdat
	.align 8
	.type	_ZTVN6icu_6725CharsetRecog_windows_1251E, @object
	.size	_ZTVN6icu_6725CharsetRecog_windows_1251E, 64
_ZTVN6icu_6725CharsetRecog_windows_1251E:
	.quad	0
	.quad	_ZTIN6icu_6725CharsetRecog_windows_1251E
	.quad	_ZNK6icu_6725CharsetRecog_windows_12517getNameEv
	.quad	_ZNK6icu_6725CharsetRecog_windows_125111getLanguageEv
	.quad	_ZNK6icu_6725CharsetRecog_windows_12515matchEPNS_9InputTextEPNS_12CharsetMatchE
	.quad	_ZN6icu_6725CharsetRecog_windows_1251D1Ev
	.quad	_ZN6icu_6725CharsetRecog_windows_1251D0Ev
	.quad	_ZNK6icu_6717CharsetRecog_sbcs10match_sbcsEPNS_9InputTextEPKiPKh
	.weak	_ZTVN6icu_6719CharsetRecog_KOI8_RE
	.section	.data.rel.ro.local._ZTVN6icu_6719CharsetRecog_KOI8_RE,"awG",@progbits,_ZTVN6icu_6719CharsetRecog_KOI8_RE,comdat
	.align 8
	.type	_ZTVN6icu_6719CharsetRecog_KOI8_RE, @object
	.size	_ZTVN6icu_6719CharsetRecog_KOI8_RE, 64
_ZTVN6icu_6719CharsetRecog_KOI8_RE:
	.quad	0
	.quad	_ZTIN6icu_6719CharsetRecog_KOI8_RE
	.quad	_ZNK6icu_6719CharsetRecog_KOI8_R7getNameEv
	.quad	_ZNK6icu_6719CharsetRecog_KOI8_R11getLanguageEv
	.quad	_ZNK6icu_6719CharsetRecog_KOI8_R5matchEPNS_9InputTextEPNS_12CharsetMatchE
	.quad	_ZN6icu_6719CharsetRecog_KOI8_RD1Ev
	.quad	_ZN6icu_6719CharsetRecog_KOI8_RD0Ev
	.quad	_ZNK6icu_6717CharsetRecog_sbcs10match_sbcsEPNS_9InputTextEPKiPKh
	.weak	_ZTVN6icu_6722CharsetRecog_IBM424_heE
	.section	.data.rel.ro._ZTVN6icu_6722CharsetRecog_IBM424_heE,"awG",@progbits,_ZTVN6icu_6722CharsetRecog_IBM424_heE,comdat
	.align 8
	.type	_ZTVN6icu_6722CharsetRecog_IBM424_heE, @object
	.size	_ZTVN6icu_6722CharsetRecog_IBM424_heE, 64
_ZTVN6icu_6722CharsetRecog_IBM424_heE:
	.quad	0
	.quad	_ZTIN6icu_6722CharsetRecog_IBM424_heE
	.quad	__cxa_pure_virtual
	.quad	_ZNK6icu_6722CharsetRecog_IBM424_he11getLanguageEv
	.quad	__cxa_pure_virtual
	.quad	0
	.quad	0
	.quad	_ZNK6icu_6717CharsetRecog_sbcs10match_sbcsEPNS_9InputTextEPKiPKh
	.weak	_ZTVN6icu_6726CharsetRecog_IBM424_he_rtlE
	.section	.data.rel.ro.local._ZTVN6icu_6726CharsetRecog_IBM424_he_rtlE,"awG",@progbits,_ZTVN6icu_6726CharsetRecog_IBM424_he_rtlE,comdat
	.align 8
	.type	_ZTVN6icu_6726CharsetRecog_IBM424_he_rtlE, @object
	.size	_ZTVN6icu_6726CharsetRecog_IBM424_he_rtlE, 64
_ZTVN6icu_6726CharsetRecog_IBM424_he_rtlE:
	.quad	0
	.quad	_ZTIN6icu_6726CharsetRecog_IBM424_he_rtlE
	.quad	_ZNK6icu_6726CharsetRecog_IBM424_he_rtl7getNameEv
	.quad	_ZNK6icu_6722CharsetRecog_IBM424_he11getLanguageEv
	.quad	_ZNK6icu_6726CharsetRecog_IBM424_he_rtl5matchEPNS_9InputTextEPNS_12CharsetMatchE
	.quad	_ZN6icu_6726CharsetRecog_IBM424_he_rtlD1Ev
	.quad	_ZN6icu_6726CharsetRecog_IBM424_he_rtlD0Ev
	.quad	_ZNK6icu_6717CharsetRecog_sbcs10match_sbcsEPNS_9InputTextEPKiPKh
	.weak	_ZTVN6icu_6726CharsetRecog_IBM424_he_ltrE
	.section	.data.rel.ro.local._ZTVN6icu_6726CharsetRecog_IBM424_he_ltrE,"awG",@progbits,_ZTVN6icu_6726CharsetRecog_IBM424_he_ltrE,comdat
	.align 8
	.type	_ZTVN6icu_6726CharsetRecog_IBM424_he_ltrE, @object
	.size	_ZTVN6icu_6726CharsetRecog_IBM424_he_ltrE, 64
_ZTVN6icu_6726CharsetRecog_IBM424_he_ltrE:
	.quad	0
	.quad	_ZTIN6icu_6726CharsetRecog_IBM424_he_ltrE
	.quad	_ZNK6icu_6726CharsetRecog_IBM424_he_ltr7getNameEv
	.quad	_ZNK6icu_6722CharsetRecog_IBM424_he11getLanguageEv
	.quad	_ZNK6icu_6726CharsetRecog_IBM424_he_ltr5matchEPNS_9InputTextEPNS_12CharsetMatchE
	.quad	_ZN6icu_6726CharsetRecog_IBM424_he_ltrD1Ev
	.quad	_ZN6icu_6726CharsetRecog_IBM424_he_ltrD0Ev
	.quad	_ZNK6icu_6717CharsetRecog_sbcs10match_sbcsEPNS_9InputTextEPKiPKh
	.weak	_ZTVN6icu_6722CharsetRecog_IBM420_arE
	.section	.data.rel.ro._ZTVN6icu_6722CharsetRecog_IBM420_arE,"awG",@progbits,_ZTVN6icu_6722CharsetRecog_IBM420_arE,comdat
	.align 8
	.type	_ZTVN6icu_6722CharsetRecog_IBM420_arE, @object
	.size	_ZTVN6icu_6722CharsetRecog_IBM420_arE, 64
_ZTVN6icu_6722CharsetRecog_IBM420_arE:
	.quad	0
	.quad	_ZTIN6icu_6722CharsetRecog_IBM420_arE
	.quad	__cxa_pure_virtual
	.quad	_ZNK6icu_6722CharsetRecog_IBM420_ar11getLanguageEv
	.quad	__cxa_pure_virtual
	.quad	0
	.quad	0
	.quad	_ZNK6icu_6722CharsetRecog_IBM420_ar10match_sbcsEPNS_9InputTextEPKiPKh
	.weak	_ZTVN6icu_6726CharsetRecog_IBM420_ar_rtlE
	.section	.data.rel.ro.local._ZTVN6icu_6726CharsetRecog_IBM420_ar_rtlE,"awG",@progbits,_ZTVN6icu_6726CharsetRecog_IBM420_ar_rtlE,comdat
	.align 8
	.type	_ZTVN6icu_6726CharsetRecog_IBM420_ar_rtlE, @object
	.size	_ZTVN6icu_6726CharsetRecog_IBM420_ar_rtlE, 64
_ZTVN6icu_6726CharsetRecog_IBM420_ar_rtlE:
	.quad	0
	.quad	_ZTIN6icu_6726CharsetRecog_IBM420_ar_rtlE
	.quad	_ZNK6icu_6726CharsetRecog_IBM420_ar_rtl7getNameEv
	.quad	_ZNK6icu_6722CharsetRecog_IBM420_ar11getLanguageEv
	.quad	_ZNK6icu_6726CharsetRecog_IBM420_ar_rtl5matchEPNS_9InputTextEPNS_12CharsetMatchE
	.quad	_ZN6icu_6726CharsetRecog_IBM420_ar_rtlD1Ev
	.quad	_ZN6icu_6726CharsetRecog_IBM420_ar_rtlD0Ev
	.quad	_ZNK6icu_6722CharsetRecog_IBM420_ar10match_sbcsEPNS_9InputTextEPKiPKh
	.weak	_ZTVN6icu_6726CharsetRecog_IBM420_ar_ltrE
	.section	.data.rel.ro.local._ZTVN6icu_6726CharsetRecog_IBM420_ar_ltrE,"awG",@progbits,_ZTVN6icu_6726CharsetRecog_IBM420_ar_ltrE,comdat
	.align 8
	.type	_ZTVN6icu_6726CharsetRecog_IBM420_ar_ltrE, @object
	.size	_ZTVN6icu_6726CharsetRecog_IBM420_ar_ltrE, 64
_ZTVN6icu_6726CharsetRecog_IBM420_ar_ltrE:
	.quad	0
	.quad	_ZTIN6icu_6726CharsetRecog_IBM420_ar_ltrE
	.quad	_ZNK6icu_6726CharsetRecog_IBM420_ar_ltr7getNameEv
	.quad	_ZNK6icu_6722CharsetRecog_IBM420_ar11getLanguageEv
	.quad	_ZNK6icu_6726CharsetRecog_IBM420_ar_ltr5matchEPNS_9InputTextEPNS_12CharsetMatchE
	.quad	_ZN6icu_6726CharsetRecog_IBM420_ar_ltrD1Ev
	.quad	_ZN6icu_6726CharsetRecog_IBM420_ar_ltrD0Ev
	.quad	_ZNK6icu_6722CharsetRecog_IBM420_ar10match_sbcsEPNS_9InputTextEPKiPKh
	.section	.rodata
	.align 32
	.type	_ZN6icu_67L16ngrams_8859_9_trE, @object
	.size	_ZN6icu_67L16ngrams_8859_9_trE, 256
_ZN6icu_67L16ngrams_8859_9_trE:
	.long	2122337
	.long	2122345
	.long	2122357
	.long	2122849
	.long	2122853
	.long	2123621
	.long	2123873
	.long	2124140
	.long	2124641
	.long	2124655
	.long	2125153
	.long	2125676
	.long	2126689
	.long	2126945
	.long	2127461
	.long	2128225
	.long	6365282
	.long	6384416
	.long	6384737
	.long	6384993
	.long	6385184
	.long	6385405
	.long	6386208
	.long	6386273
	.long	6386429
	.long	6386685
	.long	6388065
	.long	6449522
	.long	6578464
	.long	6579488
	.long	6580512
	.long	6627426
	.long	6627435
	.long	6644841
	.long	6647328
	.long	6648352
	.long	6648425
	.long	6648681
	.long	6909029
	.long	6909472
	.long	6909545
	.long	6910496
	.long	7102830
	.long	7102834
	.long	7103776
	.long	7103858
	.long	7217249
	.long	7217250
	.long	7217259
	.long	7234657
	.long	7234661
	.long	7234848
	.long	7235872
	.long	7235950
	.long	7273760
	.long	7498094
	.long	7535982
	.long	7759136
	.long	7954720
	.long	7958386
	.long	16608800
	.long	16608868
	.long	16609021
	.long	16642301
	.align 32
	.type	_ZN6icu_67L16ngrams_8859_8_heE, @object
	.size	_ZN6icu_67L16ngrams_8859_8_heE, 256
_ZN6icu_67L16ngrams_8859_8_heE:
	.long	2154725
	.long	2154732
	.long	2155753
	.long	2155756
	.long	2155758
	.long	2155760
	.long	2157040
	.long	2157810
	.long	2157817
	.long	2158053
	.long	2158057
	.long	2158565
	.long	2158569
	.long	2160869
	.long	2160873
	.long	2161376
	.long	2161381
	.long	2161385
	.long	14688484
	.long	14688492
	.long	14688493
	.long	14688506
	.long	14738464
	.long	14738916
	.long	14740512
	.long	14741024
	.long	14754020
	.long	14754029
	.long	14754042
	.long	14950628
	.long	14950633
	.long	14950636
	.long	14950637
	.long	14950639
	.long	14950648
	.long	14950650
	.long	15002656
	.long	15065120
	.long	15066144
	.long	15196192
	.long	15327264
	.long	15327520
	.long	15328288
	.long	15474916
	.long	15474925
	.long	15474938
	.long	15528480
	.long	15530272
	.long	15591913
	.long	15591920
	.long	15591928
	.long	15605988
	.long	15605997
	.long	15606010
	.long	15655200
	.long	15655968
	.long	15918112
	.long	16326884
	.long	16326893
	.long	16326906
	.long	16376864
	.long	16441376
	.long	16442400
	.long	16442857
	.align 32
	.type	_ZN6icu_67L18ngrams_8859_8_I_heE, @object
	.size	_ZN6icu_67L18ngrams_8859_8_I_heE, 256
_ZN6icu_67L18ngrams_8859_8_I_heE:
	.long	2154725
	.long	2154727
	.long	2154729
	.long	2154746
	.long	2154985
	.long	2154990
	.long	2155744
	.long	2155749
	.long	2155753
	.long	2155758
	.long	2155762
	.long	2155769
	.long	2155770
	.long	2157792
	.long	2157796
	.long	2158304
	.long	2159340
	.long	2161132
	.long	14744096
	.long	14950624
	.long	14950625
	.long	14950628
	.long	14950636
	.long	14950638
	.long	14950649
	.long	15001056
	.long	15065120
	.long	15068448
	.long	15068960
	.long	15071264
	.long	15071776
	.long	15278308
	.long	15328288
	.long	15328762
	.long	15329773
	.long	15330592
	.long	15331104
	.long	15333408
	.long	15333920
	.long	15474912
	.long	15474916
	.long	15523872
	.long	15524896
	.long	15540448
	.long	15540449
	.long	15540452
	.long	15540460
	.long	15540462
	.long	15540473
	.long	15655968
	.long	15671524
	.long	15787040
	.long	15788320
	.long	15788525
	.long	15920160
	.long	16261348
	.long	16312813
	.long	16378912
	.long	16392416
	.long	16392417
	.long	16392420
	.long	16392428
	.long	16392430
	.long	16392441
	.align 32
	.type	_ZN6icu_67L16ngrams_8859_7_elE, @object
	.size	_ZN6icu_67L16ngrams_8859_7_elE, 256
_ZN6icu_67L16ngrams_8859_7_elE:
	.long	2154989
	.long	2154992
	.long	2155497
	.long	2155753
	.long	2156016
	.long	2156320
	.long	2157281
	.long	2157797
	.long	2158049
	.long	2158368
	.long	2158817
	.long	2158831
	.long	2158833
	.long	2159604
	.long	2159605
	.long	2159847
	.long	2159855
	.long	14672160
	.long	14754017
	.long	14754036
	.long	14805280
	.long	14806304
	.long	14807292
	.long	14807584
	.long	14936545
	.long	15067424
	.long	15069728
	.long	15147252
	.long	15199520
	.long	15200800
	.long	15278324
	.long	15327520
	.long	15330014
	.long	15331872
	.long	15393257
	.long	15393268
	.long	15525152
	.long	15540449
	.long	15540453
	.long	15540464
	.long	15589664
	.long	15725088
	.long	15725856
	.long	15790069
	.long	15790575
	.long	15793184
	.long	15868129
	.long	15868133
	.long	15868138
	.long	15868144
	.long	15868148
	.long	15983904
	.long	15984416
	.long	15987951
	.long	16048416
	.long	16048617
	.long	16050157
	.long	16050162
	.long	16050666
	.long	16052000
	.long	16052213
	.long	16054765
	.long	16379168
	.long	16706848
	.align 32
	.type	_ZN6icu_67L16ngrams_8859_6_arE, @object
	.size	_ZN6icu_67L16ngrams_8859_6_arE, 256
_ZN6icu_67L16ngrams_8859_6_arE:
	.long	2148324
	.long	2148326
	.long	2148551
	.long	2152932
	.long	2154986
	.long	2155748
	.long	2156006
	.long	2156743
	.long	13050055
	.long	13091104
	.long	13093408
	.long	13095200
	.long	13100064
	.long	13100227
	.long	13100231
	.long	13100232
	.long	13100234
	.long	13100236
	.long	13100237
	.long	13100239
	.long	13100243
	.long	13100249
	.long	13100258
	.long	13100261
	.long	13100264
	.long	13100266
	.long	13100320
	.long	13100576
	.long	13100746
	.long	13115591
	.long	13181127
	.long	13181153
	.long	13181156
	.long	13181157
	.long	13181160
	.long	13246663
	.long	13574343
	.long	13617440
	.long	13705415
	.long	13748512
	.long	13836487
	.long	14229703
	.long	14279913
	.long	14805536
	.long	14950599
	.long	14993696
	.long	15001888
	.long	15002144
	.long	15016135
	.long	15058720
	.long	15059232
	.long	15066656
	.long	15081671
	.long	15147207
	.long	15189792
	.long	15255524
	.long	15263264
	.long	15278279
	.long	15343815
	.long	15343845
	.long	15343848
	.long	15386912
	.long	15388960
	.long	15394336
	.align 32
	.type	_ZN6icu_67L16ngrams_8859_5_ruE, @object
	.size	_ZN6icu_67L16ngrams_8859_5_ruE, 256
_ZN6icu_67L16ngrams_8859_5_ruE:
	.long	2150944
	.long	2151134
	.long	2151646
	.long	2152400
	.long	2152480
	.long	2153168
	.long	2153182
	.long	2153936
	.long	2153941
	.long	2154193
	.long	2154462
	.long	2154464
	.long	2154704
	.long	2154974
	.long	2154978
	.long	2155230
	.long	2156514
	.long	2158050
	.long	13688280
	.long	13689580
	.long	13884960
	.long	14015468
	.long	14015960
	.long	14016994
	.long	14017056
	.long	14164191
	.long	14210336
	.long	14211104
	.long	14216992
	.long	14407133
	.long	14407712
	.long	14413021
	.long	14536736
	.long	14538016
	.long	14538965
	.long	14538991
	.long	14540320
	.long	14540498
	.long	14557394
	.long	14557407
	.long	14557409
	.long	14602784
	.long	14602960
	.long	14603230
	.long	14604576
	.long	14605292
	.long	14605344
	.long	14606818
	.long	14671579
	.long	14672085
	.long	14672088
	.long	14672094
	.long	14733522
	.long	14734804
	.long	14803664
	.long	14803666
	.long	14803672
	.long	14806816
	.long	14865883
	.long	14868000
	.long	14868192
	.long	14871584
	.long	15196894
	.long	15459616
	.section	.rodata.str1.1
.LC28:
	.string	"cs"
.LC29:
	.string	"hu"
.LC30:
	.string	"pl"
.LC31:
	.string	"ro"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	_ZN6icu_67L13ngrams_8859_2E, @object
	.size	_ZN6icu_67L13ngrams_8859_2E, 1056
_ZN6icu_67L13ngrams_8859_2E:
	.long	2122016
	.long	2122361
	.long	2122863
	.long	2124389
	.long	2125409
	.long	2125413
	.long	2125600
	.long	2125668
	.long	2125935
	.long	2125938
	.long	2126072
	.long	2126447
	.long	2126693
	.long	2126703
	.long	2126708
	.long	2126959
	.long	2127392
	.long	2127481
	.long	2128481
	.long	6365296
	.long	6513952
	.long	6514720
	.long	6627440
	.long	6627443
	.long	6627446
	.long	6647072
	.long	6647533
	.long	6844192
	.long	6844260
	.long	6910836
	.long	6972704
	.long	7042149
	.long	7103776
	.long	7104800
	.long	7233824
	.long	7268640
	.long	7269408
	.long	7269664
	.long	7282800
	.long	7300206
	.long	7301737
	.long	7304052
	.long	7304480
	.long	7304801
	.long	7368548
	.long	7368554
	.long	7369327
	.long	7403621
	.long	7562528
	.long	7565173
	.long	7566433
	.long	7566441
	.long	7566446
	.long	7628146
	.long	7630573
	.long	7630624
	.long	7676016
	.long	12477728
	.long	14773997
	.long	15296623
	.long	15540336
	.long	15540339
	.long	15559968
	.long	16278884
	.quad	.LC28
	.long	2122016
	.long	2122106
	.long	2122341
	.long	2123111
	.long	2123116
	.long	2123365
	.long	2123873
	.long	2123887
	.long	2124147
	.long	2124645
	.long	2124649
	.long	2124790
	.long	2124901
	.long	2125153
	.long	2125157
	.long	2125161
	.long	2125413
	.long	2126714
	.long	2126949
	.long	2156915
	.long	6365281
	.long	6365291
	.long	6365293
	.long	6365299
	.long	6384416
	.long	6385184
	.long	6388256
	.long	6447470
	.long	6448494
	.long	6645625
	.long	6646560
	.long	6646816
	.long	6646885
	.long	6647072
	.long	6647328
	.long	6648421
	.long	6648864
	.long	6648933
	.long	6648948
	.long	6781216
	.long	6844263
	.long	6909556
	.long	6910752
	.long	7020641
	.long	7075450
	.long	7169383
	.long	7170414
	.long	7217249
	.long	7233899
	.long	7234923
	.long	7234925
	.long	7238688
	.long	7300985
	.long	7544929
	.long	7567973
	.long	7567988
	.long	7568097
	.long	7596391
	.long	7610465
	.long	7631904
	.long	7659891
	.long	8021362
	.long	14773792
	.long	15299360
	.quad	.LC29
	.long	2122618
	.long	2122863
	.long	2124064
	.long	2124389
	.long	2124655
	.long	2125153
	.long	2125161
	.long	2125409
	.long	2125417
	.long	2125668
	.long	2125935
	.long	2125938
	.long	2126697
	.long	2127648
	.long	2127721
	.long	2127737
	.long	2128416
	.long	2128481
	.long	6365296
	.long	6365303
	.long	6385257
	.long	6514720
	.long	6519397
	.long	6519417
	.long	6582048
	.long	6584937
	.long	6627440
	.long	6627443
	.long	6627447
	.long	6627450
	.long	6645615
	.long	6646304
	.long	6647072
	.long	6647401
	.long	6778656
	.long	6906144
	.long	6907168
	.long	6907242
	.long	7037216
	.long	7039264
	.long	7039333
	.long	7170405
	.long	7233824
	.long	7235937
	.long	7235941
	.long	7282800
	.long	7305057
	.long	7305065
	.long	7368556
	.long	7369313
	.long	7369327
	.long	7369338
	.long	7502437
	.long	7502457
	.long	7563754
	.long	7564137
	.long	7566433
	.long	7825765
	.long	7955304
	.long	7957792
	.long	8021280
	.long	8022373
	.long	8026400
	.long	15955744
	.quad	.LC30
	.long	2122016
	.long	2122083
	.long	2122593
	.long	2122597
	.long	2122607
	.long	2122613
	.long	2122853
	.long	2122857
	.long	2124897
	.long	2125153
	.long	2125925
	.long	2125938
	.long	2126693
	.long	2126819
	.long	2127214
	.long	2144873
	.long	2158190
	.long	6365283
	.long	6365284
	.long	6386277
	.long	6386720
	.long	6386789
	.long	6386976
	.long	6513010
	.long	6516590
	.long	6518048
	.long	6546208
	.long	6579488
	.long	6627425
	.long	6627427
	.long	6627428
	.long	6627440
	.long	6627443
	.long	6644000
	.long	6646048
	.long	6646885
	.long	6647412
	.long	6648692
	.long	6889569
	.long	6889571
	.long	6889572
	.long	6889584
	.long	6907168
	.long	6908192
	.long	6909472
	.long	7102752
	.long	7103776
	.long	7106418
	.long	7107945
	.long	7234848
	.long	7238770
	.long	7303712
	.long	7365998
	.long	7496992
	.long	7497057
	.long	7501088
	.long	7594784
	.long	7628064
	.long	7631477
	.long	7660320
	.long	7694624
	.long	7695392
	.long	12216608
	.long	15625760
	.quad	.LC31
	.section	.rodata.str1.1
.LC32:
	.string	"en"
.LC33:
	.string	"da"
.LC34:
	.string	"de"
.LC35:
	.string	"es"
.LC36:
	.string	"fr"
.LC37:
	.string	"it"
.LC38:
	.string	"nl"
.LC39:
	.string	"no"
.LC40:
	.string	"pt"
.LC41:
	.string	"sv"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZN6icu_67L13ngrams_8859_1E, @object
	.size	_ZN6icu_67L13ngrams_8859_1E, 2640
_ZN6icu_67L13ngrams_8859_1E:
	.long	2122016
	.long	2122094
	.long	2122341
	.long	2122607
	.long	2123375
	.long	2123873
	.long	2123877
	.long	2124142
	.long	2125153
	.long	2125670
	.long	2125938
	.long	2126437
	.long	2126689
	.long	2126708
	.long	2126952
	.long	2126959
	.long	2127720
	.long	6383972
	.long	6384672
	.long	6385184
	.long	6385252
	.long	6386464
	.long	6386720
	.long	6386789
	.long	6386793
	.long	6561889
	.long	6561908
	.long	6627425
	.long	6627443
	.long	6627444
	.long	6644768
	.long	6647412
	.long	6648352
	.long	6648608
	.long	6713202
	.long	6840692
	.long	6841632
	.long	6841714
	.long	6906912
	.long	6909472
	.long	6909543
	.long	6909806
	.long	6910752
	.long	7217249
	.long	7217268
	.long	7234592
	.long	7235360
	.long	7238688
	.long	7300640
	.long	7302688
	.long	7303712
	.long	7496992
	.long	7500576
	.long	7544929
	.long	7544948
	.long	7561577
	.long	7566368
	.long	7610484
	.long	7628146
	.long	7628897
	.long	7628901
	.long	7629167
	.long	7630624
	.long	7631648
	.quad	.LC32
	.long	2122086
	.long	2122100
	.long	2122853
	.long	2123118
	.long	2123122
	.long	2123375
	.long	2123873
	.long	2124064
	.long	2125157
	.long	2125671
	.long	2126053
	.long	2126697
	.long	2126708
	.long	2126953
	.long	2127465
	.long	6383136
	.long	6385184
	.long	6385252
	.long	6386208
	.long	6386720
	.long	6579488
	.long	6579566
	.long	6579570
	.long	6579572
	.long	6627443
	.long	6644768
	.long	6644837
	.long	6647328
	.long	6647396
	.long	6648352
	.long	6648421
	.long	6648608
	.long	6648864
	.long	6713202
	.long	6776096
	.long	6776174
	.long	6776178
	.long	6907749
	.long	6908960
	.long	6909543
	.long	7038240
	.long	7039845
	.long	7103858
	.long	7104871
	.long	7105637
	.long	7169380
	.long	7234661
	.long	7234848
	.long	7235360
	.long	7235429
	.long	7300896
	.long	7302432
	.long	7303712
	.long	7398688
	.long	7479396
	.long	7479397
	.long	7479411
	.long	7496992
	.long	7566437
	.long	7610483
	.long	7628064
	.long	7628146
	.long	7629164
	.long	7759218
	.quad	.LC33
	.long	2122094
	.long	2122101
	.long	2122341
	.long	2122849
	.long	2122853
	.long	2122857
	.long	2123113
	.long	2123621
	.long	2123873
	.long	2124142
	.long	2125161
	.long	2126691
	.long	2126693
	.long	2127214
	.long	2127461
	.long	2127471
	.long	2127717
	.long	2128501
	.long	6448498
	.long	6514720
	.long	6514789
	.long	6514804
	.long	6578547
	.long	6579566
	.long	6579570
	.long	6580581
	.long	6627428
	.long	6627443
	.long	6646126
	.long	6646132
	.long	6647328
	.long	6648352
	.long	6648608
	.long	6776174
	.long	6841710
	.long	6845472
	.long	6906728
	.long	6907168
	.long	6909472
	.long	6909541
	.long	6911008
	.long	7104867
	.long	7105637
	.long	7217249
	.long	7217252
	.long	7217267
	.long	7234592
	.long	7234661
	.long	7234848
	.long	7235360
	.long	7235429
	.long	7238757
	.long	7479396
	.long	7496805
	.long	7497065
	.long	7562088
	.long	7566437
	.long	7610468
	.long	7628064
	.long	7628142
	.long	7628146
	.long	7695972
	.long	7695975
	.long	7759218
	.quad	.LC34
	.long	2122016
	.long	2122593
	.long	2122607
	.long	2122853
	.long	2123116
	.long	2123118
	.long	2123123
	.long	2124142
	.long	2124897
	.long	2124911
	.long	2125921
	.long	2125935
	.long	2125938
	.long	2126197
	.long	2126437
	.long	2126693
	.long	2127214
	.long	2128160
	.long	6365283
	.long	6365284
	.long	6365285
	.long	6365292
	.long	6365296
	.long	6382441
	.long	6382703
	.long	6384672
	.long	6386208
	.long	6386464
	.long	6515187
	.long	6516590
	.long	6579488
	.long	6579564
	.long	6582048
	.long	6627428
	.long	6627429
	.long	6627436
	.long	6646816
	.long	6647328
	.long	6647412
	.long	6648608
	.long	6648692
	.long	6907246
	.long	6943598
	.long	7102752
	.long	7106419
	.long	7217253
	.long	7238757
	.long	7282788
	.long	7282789
	.long	7302688
	.long	7303712
	.long	7303968
	.long	7364978
	.long	7435621
	.long	7495968
	.long	7497075
	.long	7544932
	.long	7544933
	.long	7544944
	.long	7562528
	.long	7628064
	.long	7630624
	.long	7693600
	.long	15953440
	.quad	.LC35
	.long	2122101
	.long	2122607
	.long	2122849
	.long	2122853
	.long	2122869
	.long	2123118
	.long	2123124
	.long	2124897
	.long	2124901
	.long	2125921
	.long	2125935
	.long	2125938
	.long	2126197
	.long	2126693
	.long	2126703
	.long	2127214
	.long	2154528
	.long	6385268
	.long	6386793
	.long	6513952
	.long	6516590
	.long	6579488
	.long	6579571
	.long	6583584
	.long	6627425
	.long	6627427
	.long	6627428
	.long	6627429
	.long	6627436
	.long	6627440
	.long	6627443
	.long	6647328
	.long	6647412
	.long	6648352
	.long	6648608
	.long	6648864
	.long	6649202
	.long	6909806
	.long	6910752
	.long	6911008
	.long	7102752
	.long	7103776
	.long	7103859
	.long	7169390
	.long	7217252
	.long	7234848
	.long	7238432
	.long	7238688
	.long	7302688
	.long	7302772
	.long	7304562
	.long	7435621
	.long	7479404
	.long	7496992
	.long	7544929
	.long	7544932
	.long	7544933
	.long	7544940
	.long	7544944
	.long	7610468
	.long	7628064
	.long	7629167
	.long	7693600
	.long	7696928
	.quad	.LC36
	.long	2122092
	.long	2122600
	.long	2122607
	.long	2122853
	.long	2122857
	.long	2123040
	.long	2124140
	.long	2124142
	.long	2124897
	.long	2125925
	.long	2125938
	.long	2127214
	.long	6365283
	.long	6365284
	.long	6365296
	.long	6365299
	.long	6386799
	.long	6514789
	.long	6516590
	.long	6579564
	.long	6580512
	.long	6627425
	.long	6627427
	.long	6627428
	.long	6627433
	.long	6627436
	.long	6627440
	.long	6627443
	.long	6646816
	.long	6646892
	.long	6647412
	.long	6648352
	.long	6841632
	.long	6889569
	.long	6889571
	.long	6889572
	.long	6889587
	.long	6906144
	.long	6908960
	.long	6909472
	.long	6909806
	.long	7102752
	.long	7103776
	.long	7104800
	.long	7105633
	.long	7234848
	.long	7235872
	.long	7237408
	.long	7238757
	.long	7282785
	.long	7282788
	.long	7282793
	.long	7282803
	.long	7302688
	.long	7302757
	.long	7366002
	.long	7495968
	.long	7496992
	.long	7563552
	.long	7627040
	.long	7628064
	.long	7629088
	.long	7630624
	.long	8022383
	.quad	.LC37
	.long	2122092
	.long	2122341
	.long	2122849
	.long	2122853
	.long	2122857
	.long	2123109
	.long	2123118
	.long	2123621
	.long	2123877
	.long	2124142
	.long	2125153
	.long	2125157
	.long	2125680
	.long	2126949
	.long	2127457
	.long	2127461
	.long	2127471
	.long	2127717
	.long	2128489
	.long	6381934
	.long	6381938
	.long	6385184
	.long	6385252
	.long	6386208
	.long	6386720
	.long	6514804
	.long	6579488
	.long	6579566
	.long	6579570
	.long	6627426
	.long	6627446
	.long	6645102
	.long	6645106
	.long	6647328
	.long	6648352
	.long	6648435
	.long	6648864
	.long	6776174
	.long	6841716
	.long	6907168
	.long	6909472
	.long	6909543
	.long	6910752
	.long	7217250
	.long	7217252
	.long	7217253
	.long	7217256
	.long	7217263
	.long	7217270
	.long	7234661
	.long	7235360
	.long	7302756
	.long	7303026
	.long	7303200
	.long	7303712
	.long	7562088
	.long	7566437
	.long	7610468
	.long	7628064
	.long	7628142
	.long	7628146
	.long	7758190
	.long	7759218
	.long	7761775
	.quad	.LC38
	.long	2122100
	.long	2122102
	.long	2122853
	.long	2123118
	.long	2123122
	.long	2123375
	.long	2123873
	.long	2124064
	.long	2125157
	.long	2125671
	.long	2126053
	.long	2126693
	.long	2126699
	.long	2126703
	.long	2126708
	.long	2126953
	.long	2127465
	.long	2155808
	.long	6385252
	.long	6386208
	.long	6386720
	.long	6579488
	.long	6579566
	.long	6579572
	.long	6627443
	.long	6644768
	.long	6647328
	.long	6647397
	.long	6648352
	.long	6648421
	.long	6648864
	.long	6648948
	.long	6713202
	.long	6776174
	.long	6908779
	.long	6908960
	.long	6909543
	.long	7038240
	.long	7039845
	.long	7103776
	.long	7105637
	.long	7169380
	.long	7169390
	.long	7217267
	.long	7234848
	.long	7235360
	.long	7235429
	.long	7237221
	.long	7300896
	.long	7302432
	.long	7303712
	.long	7398688
	.long	7479411
	.long	7496992
	.long	7565165
	.long	7566437
	.long	7610483
	.long	7628064
	.long	7628142
	.long	7628146
	.long	7629164
	.long	7631904
	.long	7631973
	.long	7759218
	.quad	.LC39
	.long	2122016
	.long	2122607
	.long	2122849
	.long	2122853
	.long	2122863
	.long	2123040
	.long	2123123
	.long	2125153
	.long	2125423
	.long	2125600
	.long	2125921
	.long	2125935
	.long	2125938
	.long	2126197
	.long	2126437
	.long	2126693
	.long	2127213
	.long	6365281
	.long	6365283
	.long	6365284
	.long	6365296
	.long	6382693
	.long	6382703
	.long	6384672
	.long	6386208
	.long	6386273
	.long	6386464
	.long	6516589
	.long	6516590
	.long	6578464
	.long	6579488
	.long	6582048
	.long	6582131
	.long	6627425
	.long	6627428
	.long	6647072
	.long	6647412
	.long	6648608
	.long	6648692
	.long	6906144
	.long	6906721
	.long	7169390
	.long	7238757
	.long	7238767
	.long	7282785
	.long	7282787
	.long	7282788
	.long	7282789
	.long	7282800
	.long	7303968
	.long	7364978
	.long	7435621
	.long	7495968
	.long	7497075
	.long	7544929
	.long	7544932
	.long	7544933
	.long	7544944
	.long	7566433
	.long	7628064
	.long	7630624
	.long	7693600
	.long	14905120
	.long	15197039
	.quad	.LC40
	.long	2122100
	.long	2122102
	.long	2122853
	.long	2123118
	.long	2123510
	.long	2123873
	.long	2124064
	.long	2124142
	.long	2124655
	.long	2125157
	.long	2125667
	.long	2126053
	.long	2126699
	.long	2126703
	.long	2126708
	.long	2126953
	.long	2127457
	.long	2127465
	.long	2155634
	.long	6382693
	.long	6385184
	.long	6385252
	.long	6386208
	.long	6386804
	.long	6514720
	.long	6579488
	.long	6579566
	.long	6579570
	.long	6579572
	.long	6644768
	.long	6647328
	.long	6648352
	.long	6648864
	.long	6747762
	.long	6776174
	.long	6909036
	.long	6909543
	.long	7037216
	.long	7105568
	.long	7169380
	.long	7217267
	.long	7233824
	.long	7234661
	.long	7235360
	.long	7235429
	.long	7235950
	.long	7299944
	.long	7302432
	.long	7302688
	.long	7398688
	.long	7479393
	.long	7479411
	.long	7495968
	.long	7564129
	.long	7565165
	.long	7610483
	.long	7627040
	.long	7628064
	.long	7628146
	.long	7629164
	.long	7631904
	.long	7758194
	.long	14971424
	.long	16151072
	.quad	.LC41
	.section	.rodata
	.align 32
	.type	_ZN6icu_67L17charMap_IBM420_arE, @object
	.size	_ZN6icu_67L17charMap_IBM420_arE, 256
_ZN6icu_67L17charMap_IBM420_arE:
	.ascii	"@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
	.ascii	"@@@@@@BCDEFGHI@@@@@@@QR@@UVWXY@@@@@@@@bcdefghi@@@@@@pqrstuvw"
	.ascii	"xy@@@@@@\200\201\202\203\204\205\206\207\210\211\212\213\214"
	.ascii	"\215\216\217\220\221\222\223\224\225\226\227\230\231\232\233"
	.ascii	"\234\235\236\237\240@\242\243\244\245\246\247\250\251\252\253"
	.ascii	"\254\255\256\257\260\261\262\263\264\265@@\270\271\272\273\274"
	.ascii	"\275\276\277@\201\202\203\204\205\206\207\210\211@\313@\315@"
	.ascii	"\317@\221\222\223\224\225\226\227\230\231\332\333\334\335\336"
	.ascii	"\337@@\242\243\244\245\246\247\250\251\352\353@\355\356\357@"
	.ascii	"@@@@@@@@@@\373\374\375\376@"
	.align 32
	.type	_ZN6icu_67L20ngrams_IBM420_ar_ltrE, @object
	.size	_ZN6icu_67L20ngrams_IBM420_ar_ltrE, 256
_ZN6icu_67L20ngrams_IBM420_ar_ltrE:
	.long	4212310
	.long	4216507
	.long	4216511
	.long	4219507
	.long	4219509
	.long	4219569
	.long	4219579
	.long	4219612
	.long	4219734
	.long	4224342
	.long	4224476
	.long	4239702
	.long	4242262
	.long	4242774
	.long	4242875
	.long	4242895
	.long	4242908
	.long	4250289
	.long	4250795
	.long	4250801
	.long	4829526
	.long	5652566
	.long	5652568
	.long	5652578
	.long	5652579
	.long	5652595
	.long	5652597
	.long	5652600
	.long	5652634
	.long	5652657
	.long	5652667
	.long	5652669
	.long	5652671
	.long	5652698
	.long	5652700
	.long	5658688
	.long	5681494
	.long	5689152
	.long	5812566
	.long	6533462
	.long	6536534
	.long	6795606
	.long	6926678
	.long	7582038
	.long	7909718
	.long	10137942
	.long	11223138
	.long	11383126
	.long	11616354
	.long	11621952
	.long	11622095
	.long	11639360
	.long	11645248
	.long	12271714
	.long	12271836
	.long	12300630
	.long	12408384
	.long	12434240
	.long	13582434
	.long	13582556
	.long	13611350
	.long	14332314
	.long	14461760
	.long	14463318
	.align 32
	.type	_ZN6icu_67L20ngrams_IBM420_ar_rtlE, @object
	.size	_ZN6icu_67L20ngrams_IBM420_ar_rtlE, 256
_ZN6icu_67L20ngrams_IBM420_ar_rtlE:
	.long	4216497
	.long	4216509
	.long	4216918
	.long	4233905
	.long	4238300
	.long	4239793
	.long	4242365
	.long	4247382
	.long	5652566
	.long	5654080
	.long	5661504
	.long	5666112
	.long	5681472
	.long	5681481
	.long	5681494
	.long	5681496
	.long	5681507
	.long	5681511
	.long	5681513
	.long	5681523
	.long	5681528
	.long	5681562
	.long	5681581
	.long	5681595
	.long	5681615
	.long	5681628
	.long	5684032
	.long	5684544
	.long	5684579
	.long	5783638
	.long	6438998
	.long	6439083
	.long	6439089
	.long	6439099
	.long	6439119
	.long	6504534
	.long	7553110
	.long	7561792
	.long	7684182
	.long	7692864
	.long	7880790
	.long	10109014
	.long	10138074
	.long	11263040
	.long	11616342
	.long	11625024
	.long	11655744
	.long	11656256
	.long	12271702
	.long	12277312
	.long	12280384
	.long	12303680
	.long	12402774
	.long	12533846
	.long	12539456
	.long	13588145
	.long	13614400
	.long	14303318
	.long	14434390
	.long	14434491
	.long	14434511
	.long	14443072
	.long	14447936
	.long	14466368
	.align 32
	.type	_ZN6icu_67L17charMap_IBM424_heE, @object
	.size	_ZN6icu_67L17charMap_IBM424_heE, 256
_ZN6icu_67L17charMap_IBM424_heE:
	.string	"@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ABCDEFGHI@@@@@@@QRSTUVWXY@@@@@@@@bcdefghi@@@@@@@q@@@@@@@@@@@"
	.ascii	"@@@\201\202\203\204\205\206\207\210\211@@@@@@@\221\222\223\224"
	.ascii	"\225\226\227\230\231@@@@@@\240@\242\243\244\245\246\247\250\251"
	.ascii	"@@@@@@@@@@@@@@@@@@@@@@@\201\202\203\204\205\206\207\210\211@"
	.ascii	"@@@@@@\221\222\223\224\225\226\227\230\231@@@@@@@@\242\243\244"
	.ascii	"\245\246\247\250\251@@@@@@@@@@@@@@@@@@@@@@"
	.align 32
	.type	_ZN6icu_67L20ngrams_IBM424_he_ltrE, @object
	.size	_ZN6icu_67L20ngrams_IBM424_he_ltrE, 256
_ZN6icu_67L20ngrams_IBM424_he_ltrE:
	.long	4211014
	.long	4211028
	.long	4212049
	.long	4212052
	.long	4212054
	.long	4212056
	.long	4215128
	.long	4215906
	.long	4215913
	.long	4216134
	.long	4216145
	.long	4216646
	.long	4216657
	.long	4220998
	.long	4221009
	.long	4223297
	.long	4223302
	.long	4223313
	.long	4276293
	.long	4276308
	.long	4276309
	.long	4276337
	.long	4277568
	.long	4277829
	.long	4281408
	.long	4281920
	.long	4341829
	.long	4341845
	.long	4341873
	.long	4538437
	.long	4538449
	.long	4538452
	.long	4538453
	.long	4538455
	.long	4538472
	.long	4538481
	.long	4543552
	.long	4604224
	.long	4605248
	.long	4735296
	.long	5325120
	.long	5325376
	.long	5326144
	.long	5521477
	.long	5521493
	.long	5521521
	.long	5530176
	.long	5531968
	.long	5591377
	.long	5591384
	.long	5591400
	.long	5652549
	.long	5652565
	.long	5652593
	.long	5653056
	.long	5653824
	.long	6440256
	.long	6897733
	.long	6897749
	.long	6897777
	.long	6899008
	.long	7422272
	.long	7423296
	.long	7423569
	.align 32
	.type	_ZN6icu_67L20ngrams_IBM424_he_rtlE, @object
	.size	_ZN6icu_67L20ngrams_IBM424_he_rtlE, 256
_ZN6icu_67L20ngrams_IBM424_he_rtlE:
	.long	4211014
	.long	4211016
	.long	4211025
	.long	4211057
	.long	4211281
	.long	4211286
	.long	4212033
	.long	4212038
	.long	4212049
	.long	4212054
	.long	4212066
	.long	4212073
	.long	4212081
	.long	4215873
	.long	4215877
	.long	4216385
	.long	4219476
	.long	4221268
	.long	4288832
	.long	4538433
	.long	4538434
	.long	4538437
	.long	4538452
	.long	4538454
	.long	4538473
	.long	4539969
	.long	4604224
	.long	4609344
	.long	4609856
	.long	4614208
	.long	4616512
	.long	5324869
	.long	5326144
	.long	5326449
	.long	5329237
	.long	5330240
	.long	5330752
	.long	5335104
	.long	5337408
	.long	5521473
	.long	5521477
	.long	5521728
	.long	5522752
	.long	5587009
	.long	5587010
	.long	5587013
	.long	5587028
	.long	5587030
	.long	5587049
	.long	5653824
	.long	5718085
	.long	5784896
	.long	5787968
	.long	5787989
	.long	6444096
	.long	6832197
	.long	6836565
	.long	6902848
	.long	7422017
	.long	7422018
	.long	7422021
	.long	7422036
	.long	7422038
	.long	7422057
	.align 32
	.type	_ZN6icu_67L14charMap_KOI8_RE, @object
	.size	_ZN6icu_67L14charMap_KOI8_RE, 256
_ZN6icu_67L14charMap_KOI8_RE:
	.string	"                                       "
	.ascii	"                         abcdefghijklmnopqrstuvwxyz      abc"
	.ascii	"defghijklmnopqrstuvwxyz                                     "
	.ascii	"   \243               \243            \300\301\302\303\304\305"
	.ascii	"\306\307\310\311\312\313\314\315\316\317\320\321\322\323\324"
	.ascii	"\325\326\327\330\331\332\333\334\335\336\337\300\301\302\303"
	.ascii	"\304\305\306\307\310\311\312\313\314\315\316\317\320\321\322"
	.ascii	"\323\324\325\326\327\330\331\332\333\334\335\336\337"
	.align 32
	.type	_ZN6icu_67L13ngrams_KOI8_RE, @object
	.size	_ZN6icu_67L13ngrams_KOI8_RE, 256
_ZN6icu_67L13ngrams_KOI8_RE:
	.long	2147535
	.long	2148640
	.long	2149313
	.long	2149327
	.long	2150081
	.long	2150085
	.long	2150338
	.long	2150607
	.long	2150610
	.long	2151105
	.long	2151375
	.long	2151380
	.long	2151631
	.long	2152224
	.long	2152399
	.long	2153153
	.long	2153684
	.long	2154196
	.long	12701385
	.long	12702936
	.long	12963032
	.long	12963529
	.long	12964820
	.long	12964896
	.long	13094688
	.long	13181136
	.long	13223200
	.long	13224224
	.long	13226272
	.long	13419982
	.long	13420832
	.long	13424846
	.long	13549856
	.long	13550880
	.long	13552069
	.long	13552081
	.long	13553440
	.long	13553623
	.long	13574352
	.long	13574355
	.long	13574359
	.long	13617103
	.long	13617696
	.long	13618392
	.long	13618464
	.long	13620180
	.long	13621024
	.long	13621185
	.long	13684684
	.long	13685445
	.long	13685449
	.long	13685455
	.long	13812183
	.long	13813188
	.long	13881632
	.long	13882561
	.long	13882569
	.long	13882583
	.long	13944268
	.long	13946656
	.long	13946834
	.long	13948960
	.long	14272544
	.long	14603471
	.align 32
	.type	_ZN6icu_67L20charMap_windows_1256E, @object
	.size	_ZN6icu_67L20charMap_windows_1256E, 256
_ZN6icu_67L20charMap_windows_1256E:
	.string	"                                       "
	.ascii	"                         abcdefghijklmnopqrstuvwxyz      abc"
	.ascii	"defghijklmnopqrstuvwxyz      \201 \203    \210 \212 \234\215"
	.ascii	"\216\217\220       \230 \232 \234  \237          \252       "
	.ascii	"   \265          \300\301\302\303\304\305\306\307\310\311\312"
	.ascii	"\313\314\315\316\317\320\321\322\323\324\325\326 \330\331\332"
	.ascii	"\333\334\335\336\337\340\341\342\343\344\345\346\347\350\351"
	.ascii	"\352\353\354\355\356\357    \364    \371 \373\374  \377"
	.align 32
	.type	_ZN6icu_67L19ngrams_windows_1256E, @object
	.size	_ZN6icu_67L19ngrams_windows_1256E, 256
_ZN6icu_67L19ngrams_windows_1256E:
	.long	2148321
	.long	2148324
	.long	2148551
	.long	2153185
	.long	2153965
	.long	2154977
	.long	2155492
	.long	2156231
	.long	13050055
	.long	13091104
	.long	13093408
	.long	13095200
	.long	13099296
	.long	13099459
	.long	13099463
	.long	13099464
	.long	13099466
	.long	13099468
	.long	13099469
	.long	13099471
	.long	13099475
	.long	13099482
	.long	13099486
	.long	13099491
	.long	13099494
	.long	13099501
	.long	13099808
	.long	13100064
	.long	13100234
	.long	13115591
	.long	13181127
	.long	13181149
	.long	13181153
	.long	13181155
	.long	13181158
	.long	13246663
	.long	13574343
	.long	13617440
	.long	13705415
	.long	13748512
	.long	13836487
	.long	14295239
	.long	14344684
	.long	14544160
	.long	14753991
	.long	14797088
	.long	14806048
	.long	14806304
	.long	14885063
	.long	14927648
	.long	14928160
	.long	14935072
	.long	14950599
	.long	15016135
	.long	15058720
	.long	15124449
	.long	15131680
	.long	15474887
	.long	15540423
	.long	15540451
	.long	15540454
	.long	15583520
	.long	15585568
	.long	15590432
	.align 32
	.type	_ZN6icu_67L20charMap_windows_1251E, @object
	.size	_ZN6icu_67L20charMap_windows_1251E, 256
_ZN6icu_67L20charMap_windows_1251E:
	.string	"                                       "
	.ascii	"                         abcdefghijklmnopqrstuvwxyz      abc"
	.ascii	"defghijklmnopqrstuvwxyz     \220\203 \203      \232 \234\235"
	.ascii	"\236\237\220         \232 \234\235\236\237 \242\242\274 \264"
	.ascii	"  \270 \272    \277  \263\263\264\265  \270 \272 \274\276\276"
	.ascii	"\277\340\341\342\343\344\345\346\347\350\351\352\353\354\355"
	.ascii	"\356\357\360\361\362\363\364\365\366\367\370\371\372\373\374"
	.ascii	"\375\376\377\340\341\342\343\344\345\346\347\350\351\352\353"
	.ascii	"\354\355\356\357\360\361\362\363\364\365\366\367\370\371\372"
	.ascii	"\373\374\375\376\377"
	.align 32
	.type	_ZN6icu_67L19ngrams_windows_1251E, @object
	.size	_ZN6icu_67L19ngrams_windows_1251E, 256
_ZN6icu_67L19ngrams_windows_1251E:
	.long	2155040
	.long	2155246
	.long	2155758
	.long	2156512
	.long	2156576
	.long	2157280
	.long	2157294
	.long	2158048
	.long	2158053
	.long	2158305
	.long	2158574
	.long	2158576
	.long	2158816
	.long	2159086
	.long	2159090
	.long	2159342
	.long	2160626
	.long	2162162
	.long	14740968
	.long	14742268
	.long	14937632
	.long	15068156
	.long	15068648
	.long	15069682
	.long	15069728
	.long	15212783
	.long	15263008
	.long	15263776
	.long	15269664
	.long	15459821
	.long	15460384
	.long	15465709
	.long	15589408
	.long	15590688
	.long	15591653
	.long	15591679
	.long	15592992
	.long	15593186
	.long	15605986
	.long	15605999
	.long	15606001
	.long	15655456
	.long	15655648
	.long	15655918
	.long	15657248
	.long	15657980
	.long	15658016
	.long	15659506
	.long	15724267
	.long	15724773
	.long	15724776
	.long	15724782
	.long	15786210
	.long	15787492
	.long	15856352
	.long	15856354
	.long	15856360
	.long	15859488
	.long	15918571
	.long	15920672
	.long	15920880
	.long	15924256
	.long	16249582
	.long	16512288
	.align 32
	.type	_ZN6icu_67L14charMap_8859_9E, @object
	.size	_ZN6icu_67L14charMap_8859_9E, 256
_ZN6icu_67L14charMap_8859_9E:
	.string	"                                       "
	.ascii	"                         abcdefghijklmnopqrstuvwxyz      abc"
	.ascii	"defghijklmnopqrstuvwxyz                                     "
	.ascii	"          \252          \265    \272     \340\341\342\343\344"
	.ascii	"\345\346\347\350\351\352\353\354\355\356\357\360\361\362\363"
	.ascii	"\364\365\366 \370\371\372\373\374i\376\337\340\341\342\343\344"
	.ascii	"\345\346\347\350\351\352\353\354\355\356\357\360\361\362\363"
	.ascii	"\364\365\366 \370\371\372\373\374\375\376\377"
	.align 32
	.type	_ZN6icu_67L14charMap_8859_8E, @object
	.size	_ZN6icu_67L14charMap_8859_8E, 256
_ZN6icu_67L14charMap_8859_8E:
	.string	"                                       "
	.ascii	"                         abcdefghijklmnopqrstuvwxyz      abc"
	.ascii	"defghijklmnopqrstuvwxyz                                     "
	.ascii	"                     \265                                   "
	.ascii	"       \340\341\342\343\344\345\346\347\350\351\352\353\354\355"
	.ascii	"\356\357\360\361\362\363\364\365\366\367\370\371\372     "
	.align 32
	.type	_ZN6icu_67L14charMap_8859_7E, @object
	.size	_ZN6icu_67L14charMap_8859_7E, 256
_ZN6icu_67L14charMap_8859_7E:
	.string	"                                       "
	.ascii	"                         abcdefghijklmnopqrstuvwxyz      abc"
	.ascii	"defghijklmnopqrstuvwxyz                                     "
	.ascii	" \241\242                   \334 \335\336\337 \374 \375\376\300"
	.ascii	"\341\342\343\344\345\346\347\350\351\352\353\354\355\356\357"
	.ascii	"\360\361 \363\364\365\366\367\370\371\372\373\334\335\336\337"
	.ascii	"\340\341\342\343\344\345\346\347\350\351\352\353\354\355\356"
	.ascii	"\357\360\361\362\363\364\365\366\367\370\371\372\373\374\375"
	.ascii	"\376 "
	.align 32
	.type	_ZN6icu_67L14charMap_8859_6E, @object
	.size	_ZN6icu_67L14charMap_8859_6E, 256
_ZN6icu_67L14charMap_8859_6E:
	.string	"                                       "
	.ascii	"                         abcdefghijklmnopqrstuvwxyz      abc"
	.ascii	"defghijklmnopqrstuvwxyz                                     "
	.ascii	"                                 \301\302\303\304\305\306\307"
	.ascii	"\310\311\312\313\314\315\316\317\320\321\322\323\324\325\326"
	.ascii	"\327\330\331\332     \340\341\342\343\344\345\346\347\350\351"
	.ascii	"\352                     "
	.align 32
	.type	_ZN6icu_67L14charMap_8859_5E, @object
	.size	_ZN6icu_67L14charMap_8859_5E, 256
_ZN6icu_67L14charMap_8859_5E:
	.string	"                                       "
	.ascii	"                         abcdefghijklmnopqrstuvwxyz      abc"
	.ascii	"defghijklmnopqrstuvwxyz                                     "
	.ascii	" \361\362\363\364\365\366\367\370\371\372\373\374 \376\377\320"
	.ascii	"\321\322\323\324\325\326\327\330\331\332\333\334\335\336\337"
	.ascii	"\340\341\342\343\344\345\346\347\350\351\352\353\354\355\356"
	.ascii	"\357\320\321\322\323\324\325\326\327\330\331\332\333\334\335"
	.ascii	"\336\337\340\341\342\343\344\345\346\347\350\351\352\353\354"
	.ascii	"\355\356\357 \361\362\363\364\365\366\367\370\371\372\373\374"
	.ascii	" \376\377"
	.align 32
	.type	_ZN6icu_67L14charMap_8859_2E, @object
	.size	_ZN6icu_67L14charMap_8859_2E, 256
_ZN6icu_67L14charMap_8859_2E:
	.string	"                                       "
	.ascii	"                         abcdefghijklmnopqrstuvwxyz      abc"
	.ascii	"defghijklmnopqrstuvwxyz                                     "
	.ascii	" \261 \263 \265\266  \271\272\273\274 \276\277 \261 \263 \265"
	.ascii	"\266\267 \271\272\273\274 \276\277\340\341\342\343\344\345\346"
	.ascii	"\347\350\351\352\353\354\355\356\357\360\361\362\363\364\365"
	.ascii	"\366 \370\371\372\373\374\375\376\337\340\341\342\343\344\345"
	.ascii	"\346\347\350\351\352\353\354\355\356\357\360\361\362\363\364"
	.ascii	"\365\366 \370\371\372\373\374\375\376 "
	.align 32
	.type	_ZN6icu_67L14charMap_8859_1E, @object
	.size	_ZN6icu_67L14charMap_8859_1E, 256
_ZN6icu_67L14charMap_8859_1E:
	.string	"                                       "
	.ascii	"                         abcdefghijklmnopqrstuvwxyz      abc"
	.ascii	"defghijklmnopqrstuvwxyz                                     "
	.ascii	"          \252          \265    \272     \340\341\342\343\344"
	.ascii	"\345\346\347\350\351\352\353\354\355\356\357\360\361\362\363"
	.ascii	"\364\365\366 \370\371\372\373\374\375\376\337\340\341\342\343"
	.ascii	"\344\345\346\347\350\351\352\353\354\355\356\357\360\361\362"
	.ascii	"\363\364\365\366 \370\371\372\373\374\375\376\377"
	.align 32
	.type	_ZN6icu_67L17unshapeMap_IBM420E, @object
	.size	_ZN6icu_67L17unshapeMap_IBM420E, 256
_ZN6icu_67L17unshapeMap_IBM420E:
	.ascii	"@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
	.ascii	"@@@@@@BBDEFGGIJKLMNOPIRSTUVVXXZ[\\]^_`abcceeggijklmnoiqqstuv"
	.ascii	"wwyz{|}~\177\200\201\202\203\204\205\206\207\210\211\200\213"
	.ascii	"\213\215\215\217\220\221\222\223\224\225\226\227\230\231\232"
	.ascii	"\232\232\232\236\236\236\241\242\243\244\245\246\247\250\251"
	.ascii	"\236\253\253\255\255\257\257\261\262\263\264\265\266\267\270"
	.ascii	"\271\261\273\273\275\275\277\300\301\302\303\304\305\306\307"
	.ascii	"\310\311\312\277\314\277\316\317\320\321\322\323\324\325\326"
	.ascii	"\327\330\331\332\332\334\334\334\337\340\341\342\343\344\345"
	.ascii	"\346\347\350\351\352\353\354\355\356\357\360\361\362\363\364"
	.ascii	"\365\366\367\370\371\372\373\374\375\376\377"
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC21:
	.long	1374389535
	.long	1070931640
	.align 8
.LC22:
	.long	0
	.long	1081262080
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
