	.file	"number_capi.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl20UFormattedNumberImplC2Ev
	.type	_ZN6icu_676number4impl20UFormattedNumberImplC2Ev, @function
_ZN6icu_676number4impl20UFormattedNumberImplC2Ev:
.LFB2840:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_676number15FormattedNumberE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	48(%rdi), %rdi
	subq	$8, %rsp
	movl	$1430672896, -48(%rdi)
	movzbl	_ZN6icu_67L15kUndefinedFieldE(%rip), %esi
	movq	$0, -40(%rdi)
	movl	$1178881536, -32(%rdi)
	movq	%rax, 24(%rbx)
	movq	%rdi, 32(%rbx)
	movl	$0, 40(%rbx)
	call	_ZN6icu_6731FormattedValueStringBuilderImplC2ENS_22FormattedStringBuilder5FieldE@PLT
	leaq	16+_ZTVN6icu_676number4impl20UFormattedNumberDataE(%rip), %rax
	leaq	200(%rbx), %rdi
	movq	%rax, 48(%rbx)
	call	_ZN6icu_676number4impl15DecimalQuantityC1Ev@PLT
	leaq	24(%rbx), %rax
	movq	%rax, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2840:
	.size	_ZN6icu_676number4impl20UFormattedNumberImplC2Ev, .-_ZN6icu_676number4impl20UFormattedNumberImplC2Ev
	.globl	_ZN6icu_676number4impl20UFormattedNumberImplC1Ev
	.set	_ZN6icu_676number4impl20UFormattedNumberImplC1Ev,_ZN6icu_676number4impl20UFormattedNumberImplC2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl20UFormattedNumberImplD2Ev
	.type	_ZN6icu_676number4impl20UFormattedNumberImplD2Ev, @function
_ZN6icu_676number4impl20UFormattedNumberImplD2Ev:
.LFB2843:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	48(%rdi), %rdi
	subq	$8, %rsp
	movq	$0, -16(%rdi)
	call	_ZN6icu_676number4impl20UFormattedNumberDataD1Ev@PLT
	addq	$8, %rsp
	leaq	24(%rbx), %rdi
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676number15FormattedNumberD1Ev@PLT
	.cfi_endproc
.LFE2843:
	.size	_ZN6icu_676number4impl20UFormattedNumberImplD2Ev, .-_ZN6icu_676number4impl20UFormattedNumberImplD2Ev
	.globl	_ZN6icu_676number4impl20UFormattedNumberImplD1Ev
	.set	_ZN6icu_676number4impl20UFormattedNumberImplD1Ev,_ZN6icu_676number4impl20UFormattedNumberImplD2Ev
	.p2align 4
	.globl	unumf_openResult_67
	.type	unumf_openResult_67, @function
unumf_openResult_67:
.LFB2845:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movl	(%rdi), %eax
	testl	%eax, %eax
	jg	.L6
	movq	%rdi, %rbx
	movl	$272, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L8
	movl	$1430672896, (%rax)
	leaq	48(%rax), %rdi
	movzbl	_ZN6icu_67L15kUndefinedFieldE(%rip), %esi
	movq	$0, 8(%rax)
	movl	$1178881536, 16(%rax)
	leaq	16+_ZTVN6icu_676number15FormattedNumberE(%rip), %rax
	movq	%rax, 24(%r12)
	movq	%rdi, 32(%r12)
	movl	$0, 40(%r12)
	call	_ZN6icu_6731FormattedValueStringBuilderImplC2ENS_22FormattedStringBuilder5FieldE@PLT
	leaq	16+_ZTVN6icu_676number4impl20UFormattedNumberDataE(%rip), %rax
	leaq	200(%r12), %rdi
	movq	%rax, 48(%r12)
	call	_ZN6icu_676number4impl15DecimalQuantityC1Ev@PLT
	leaq	24(%r12), %rax
	movq	%rax, 8(%r12)
.L6:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L8:
	.cfi_restore_state
	movl	$7, (%rbx)
	jmp	.L6
	.cfi_endproc
.LFE2845:
	.size	unumf_openResult_67, .-unumf_openResult_67
	.p2align 4
	.globl	unumf_resultAsValue_67
	.type	unumf_resultAsValue_67, @function
unumf_resultAsValue_67:
.LFB2846:
	.cfi_startproc
	endbr64
	movl	(%rsi), %edx
	movq	%rdi, %rax
	testl	%edx, %edx
	jg	.L12
	testq	%rdi, %rdi
	je	.L16
	cmpl	$1178881536, 16(%rdi)
	jne	.L17
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	movl	$3, (%rsi)
.L12:
	xorl	%eax, %eax
.L18:
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	movl	$1, (%rsi)
	xorl	%eax, %eax
	jmp	.L18
	.cfi_endproc
.LFE2846:
	.size	unumf_resultAsValue_67, .-unumf_resultAsValue_67
	.p2align 4
	.globl	unumf_closeResult_67
	.type	unumf_closeResult_67, @function
unumf_closeResult_67:
.LFB2847:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L25
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	cmpl	$1178881536, 16(%rdi)
	jne	.L19
	movq	$0, 32(%rdi)
	leaq	48(%rdi), %rdi
	call	_ZN6icu_676number4impl20UFormattedNumberDataD1Ev@PLT
	leaq	24(%r12), %rdi
	call	_ZN6icu_676number15FormattedNumberD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE2847:
	.size	unumf_closeResult_67, .-unumf_closeResult_67
	.p2align 4
	.globl	_ZN6icu_676number4impl41validateUFormattedNumberToDecimalQuantityEPK16UFormattedNumberR10UErrorCode
	.type	_ZN6icu_676number4impl41validateUFormattedNumberToDecimalQuantityEPK16UFormattedNumberR10UErrorCode, @function
_ZN6icu_676number4impl41validateUFormattedNumberToDecimalQuantityEPK16UFormattedNumberR10UErrorCode:
.LFB2848:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L29
	testq	%rdi, %rdi
	je	.L33
	cmpl	$1178881536, 16(%rdi)
	leaq	200(%rdi), %rax
	jne	.L34
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	movl	$3, (%rsi)
.L29:
	xorl	%eax, %eax
.L35:
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	movl	$1, (%rsi)
	xorl	%eax, %eax
	jmp	.L35
	.cfi_endproc
.LFE2848:
	.size	_ZN6icu_676number4impl41validateUFormattedNumberToDecimalQuantityEPK16UFormattedNumberR10UErrorCode, .-_ZN6icu_676number4impl41validateUFormattedNumberToDecimalQuantityEPK16UFormattedNumberR10UErrorCode
	.p2align 4
	.globl	unumf_formatInt_67
	.type	unumf_formatInt_67, @function
unumf_formatInt_67:
.LFB2878:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L47
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	testq	%rdi, %rdi
	je	.L43
	cmpl	$1313231360, (%rdi)
	jne	.L50
	movq	%rdx, %rbx
	testq	%rdx, %rdx
	je	.L43
	cmpl	$1178881536, 16(%rbx)
	jne	.L50
	movq	%rsi, %r14
	leaq	56(%rbx), %rdi
	leaq	48(%rbx), %r15
	call	_ZN6icu_6722FormattedStringBuilder5clearEv@PLT
	leaq	200(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZN6icu_676number4impl15DecimalQuantity9setToLongEl@PLT
	addq	$8, %rsp
	movq	%r13, %rdx
	movq	%r15, %rsi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	leaq	8(%r12), %rdi
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_676number24LocalizedNumberFormatter10formatImplEPNS0_4impl20UFormattedNumberDataER10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L50:
	.cfi_restore_state
	movl	$3, 0(%r13)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	$1, 0(%r13)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2878:
	.size	unumf_formatInt_67, .-unumf_formatInt_67
	.p2align 4
	.globl	unumf_formatDouble_67
	.type	unumf_formatDouble_67, @function
unumf_formatDouble_67:
.LFB2879:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L62
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	testq	%rdi, %rdi
	je	.L58
	cmpl	$1313231360, (%rdi)
	jne	.L65
	movq	%rsi, %rbx
	testq	%rsi, %rsi
	je	.L58
	cmpl	$1178881536, 16(%rbx)
	jne	.L65
	leaq	56(%rbx), %rdi
	movsd	%xmm0, -40(%rbp)
	leaq	48(%rbx), %r14
	call	_ZN6icu_6722FormattedStringBuilder5clearEv@PLT
	movsd	-40(%rbp), %xmm0
	leaq	200(%rbx), %rdi
	call	_ZN6icu_676number4impl15DecimalQuantity11setToDoubleEd@PLT
	addq	$16, %rsp
	movq	%r13, %rdx
	movq	%r14, %rsi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	leaq	8(%r12), %rdi
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_676number24LocalizedNumberFormatter10formatImplEPNS0_4impl20UFormattedNumberDataER10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L65:
	.cfi_restore_state
	movl	$3, 0(%r13)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	ret
	.p2align 4,,10
	.p2align 3
.L58:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movl	$1, 0(%r13)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2879:
	.size	unumf_formatDouble_67, .-unumf_formatDouble_67
	.p2align 4
	.globl	unumf_formatDecimal_67
	.type	unumf_formatDecimal_67, @function
unumf_formatDecimal_67:
.LFB2880:
	.cfi_startproc
	endbr64
	movl	(%r8), %r9d
	testl	%r9d, %r9d
	jg	.L79
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	testq	%rdi, %rdi
	je	.L74
	cmpl	$1313231360, (%rdi)
	jne	.L82
	movq	%rcx, %rbx
	testq	%rcx, %rcx
	je	.L74
	cmpl	$1178881536, 16(%rbx)
	jne	.L82
	leaq	56(%rbx), %rdi
	movl	%edx, -52(%rbp)
	movq	%rsi, %r15
	leaq	48(%rbx), %r14
	call	_ZN6icu_6722FormattedStringBuilder5clearEv@PLT
	movl	-52(%rbp), %edx
	movq	%r12, %rcx
	movq	%r15, %rsi
	leaq	200(%rbx), %rdi
	call	_ZN6icu_676number4impl15DecimalQuantity14setToDecNumberENS_11StringPieceER10UErrorCode@PLT
	movl	(%r12), %eax
	testl	%eax, %eax
	jle	.L83
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L82:
	.cfi_restore_state
	movl	$3, (%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L83:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	addq	$24, %rsp
	leaq	8(%r13), %rdi
	movq	%r12, %rdx
	movq	%r14, %rsi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_676number24LocalizedNumberFormatter10formatImplEPNS0_4impl20UFormattedNumberDataER10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L74:
	.cfi_restore_state
	movl	$1, (%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2880:
	.size	unumf_formatDecimal_67, .-unumf_formatDecimal_67
	.p2align 4
	.globl	unumf_resultNextFieldPosition_67
	.type	unumf_resultNextFieldPosition_67, @function
unumf_resultNextFieldPosition_67:
.LFB2882:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L85
	testq	%rdi, %rdi
	je	.L94
	cmpl	$1178881536, 16(%rdi)
	jne	.L95
	movq	%rsi, %rbx
	testq	%rsi, %rsi
	je	.L96
	leaq	16+_ZTVN6icu_6713FieldPositionE(%rip), %rax
	movq	(%rbx), %rcx
	leaq	-64(%rbp), %r13
	addq	$48, %rdi
	movq	%rax, -64(%rbp)
	movl	8(%rbx), %eax
	movq	%r13, %rsi
	movq	%rcx, -56(%rbp)
	movl	%eax, -48(%rbp)
	call	_ZNK6icu_6731FormattedValueStringBuilderImpl17nextFieldPositionERNS_13FieldPositionER10UErrorCode@PLT
	movq	-52(%rbp), %rdx
	movq	%r13, %rdi
	testb	%al, %al
	movq	%rdx, 4(%rbx)
	setne	%r12b
	call	_ZN6icu_6713FieldPositionD1Ev@PLT
.L84:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L97
	addq	$40, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L95:
	.cfi_restore_state
	movl	$3, (%rdx)
.L85:
	xorl	%r12d, %r12d
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L96:
	movl	$1, (%rdx)
	xorl	%r12d, %r12d
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L94:
	movl	$1, (%rdx)
	jmp	.L85
.L97:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2882:
	.size	unumf_resultNextFieldPosition_67, .-unumf_resultNextFieldPosition_67
	.p2align 4
	.globl	unumf_close_67
	.type	unumf_close_67, @function
unumf_close_67:
.LFB2884:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L104
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	cmpl	$1313231360, (%rdi)
	jne	.L98
	leaq	8(%rdi), %rdi
	call	_ZN6icu_676number24LocalizedNumberFormatterD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L98:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L104:
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE2884:
	.size	unumf_close_67, .-unumf_close_67
	.p2align 4
	.globl	unumf_resultToString_67
	.type	unumf_resultToString_67, @function
unumf_resultToString_67:
.LFB2881:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L108
	movq	%rcx, %r12
	testq	%rdi, %rdi
	je	.L119
	cmpl	$1178881536, 16(%rdi)
	jne	.L120
	movl	%edx, %eax
	movq	%rsi, %rbx
	movl	%edx, %r13d
	shrl	$31, %eax
	testq	%rsi, %rsi
	je	.L121
	testb	%al, %al
	je	.L112
.L123:
	movl	$1, (%r12)
	xorl	%eax, %eax
.L107:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L122
	addq	$112, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L112:
	.cfi_restore_state
	leaq	-112(%rbp), %r14
	leaq	48(%rdi), %rsi
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	_ZNK6icu_6731FormattedValueStringBuilderImpl12toTempStringER10UErrorCode@PLT
	leaq	-120(%rbp), %rsi
	movq	%r12, %rcx
	movl	%r13d, %edx
	movq	%r14, %rdi
	movq	%rbx, -120(%rbp)
	call	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode@PLT
	movl	%eax, -132(%rbp)
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-132(%rbp), %eax
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L120:
	movl	$3, (%rcx)
.L108:
	xorl	%eax, %eax
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L121:
	testl	%r13d, %r13d
	setne	%al
	testb	%al, %al
	jne	.L123
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L119:
	movl	$1, (%rcx)
	xorl	%eax, %eax
	jmp	.L107
.L122:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2881:
	.size	unumf_resultToString_67, .-unumf_resultToString_67
	.p2align 4
	.globl	unumf_openForSkeletonAndLocaleWithError_67
	.type	unumf_openForSkeletonAndLocaleWithError_67, @function
unumf_openForSkeletonAndLocaleWithError_67:
.LFB2877:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movl	$464, %edi
	subq	$1256, %rsp
	movq	%rdx, -1272(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L125
	xorl	%eax, %eax
	movq	%r14, %rdi
	leaq	8(%r14), %r10
	movl	$58, %ecx
	rep stosq
	movl	$1313231360, (%r14)
	leaq	24(%r14), %rdi
	movq	$2, 12(%r14)
	movq	%r10, -1288(%rbp)
	call	_ZN6icu_6711MeasureUnitC1Ev@PLT
	leaq	48(%r14), %rdi
	call	_ZN6icu_6711MeasureUnitC1Ev@PLT
	movl	$-3, %eax
	movl	$-1, %edx
	movl	$0, 72(%r14)
	movw	%ax, 108(%r14)
	pxor	%xmm0, %xmm0
	movabsq	$30064771077, %rax
	leaq	224(%r14), %rdi
	movw	%dx, 132(%r14)
	movq	%rax, 160(%r14)
	movl	$4, 104(%r14)
	movl	$-2, 120(%r14)
	movb	$0, 140(%r14)
	movl	$0, 144(%r14)
	movq	$0, 152(%r14)
	movl	$2, 168(%r14)
	movl	$0, 176(%r14)
	movq	$0, 184(%r14)
	movl	$0, 192(%r14)
	movl	$3, 216(%r14)
	movups	%xmm0, 200(%r14)
	call	_ZN6icu_676LocaleC1Ev@PLT
	movq	%rbx, -1256(%rbp)
	xorl	%esi, %esi
	cmpl	$-1, %r12d
	leaq	-1248(%rbp), %rbx
	sete	%sil
	movl	%r12d, %ecx
	movq	$0, 448(%r14)
	movq	$0, 456(%r14)
	leaq	-1256(%rbp), %rdx
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	leaq	-960(%rbp), %r11
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	%r11, %rdi
	movq	%r11, -1280(%rbp)
	leaq	-1184(%rbp), %r12
	leaq	-512(%rbp), %r13
	call	_ZN6icu_676number15NumberFormatter11forSkeletonERKNS_13UnicodeStringER11UParseErrorR10UErrorCode@PLT
	movq	-1272(%rbp), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	movq	-1280(%rbp), %r11
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%r11, %rsi
	call	_ZNO6icu_676number26UnlocalizedNumberFormatter6localeERKNS_6LocaleE@PLT
	movq	-1288(%rbp), %r10
	movq	%r13, %rsi
	movq	%r10, %rdi
	call	_ZN6icu_676number24LocalizedNumberFormatteraSEOS1_@PLT
	movq	%r13, %rdi
	call	_ZN6icu_676number24LocalizedNumberFormatterD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	leaq	-744(%rbp), %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	leaq	-792(%rbp), %rdi
	call	_ZN6icu_676number5ScaleD1Ev@PLT
	leaq	-824(%rbp), %rdi
	call	_ZN6icu_676number4impl14SymbolsWrapperD1Ev@PLT
	leaq	-920(%rbp), %rdi
	call	_ZN6icu_6711MeasureUnitD1Ev@PLT
	leaq	-944(%rbp), %rdi
	call	_ZN6icu_6711MeasureUnitD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L124:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L129
	addq	$1256, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L125:
	.cfi_restore_state
	movl	$7, 0(%r13)
	jmp	.L124
.L129:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2877:
	.size	unumf_openForSkeletonAndLocaleWithError_67, .-unumf_openForSkeletonAndLocaleWithError_67
	.p2align 4
	.globl	unumf_openForSkeletonAndLocale_67
	.type	unumf_openForSkeletonAndLocale_67, @function
unumf_openForSkeletonAndLocale_67:
.LFB2849:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movl	$464, %edi
	subq	$1240, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L131
	xorl	%eax, %eax
	movq	%r12, %rdi
	leaq	8(%r12), %r9
	movl	$58, %ecx
	rep stosq
	movl	$1313231360, (%r12)
	leaq	24(%r12), %rdi
	movq	$2, 12(%r12)
	movq	%r9, -1280(%rbp)
	call	_ZN6icu_6711MeasureUnitC1Ev@PLT
	leaq	48(%r12), %rdi
	call	_ZN6icu_6711MeasureUnitC1Ev@PLT
	movl	$-3, %eax
	movl	$-1, %edx
	movl	$0, 72(%r12)
	movw	%ax, 108(%r12)
	pxor	%xmm0, %xmm0
	movabsq	$30064771077, %rax
	leaq	224(%r12), %rdi
	movw	%dx, 132(%r12)
	movq	%rax, 160(%r12)
	movl	$4, 104(%r12)
	movl	$-2, 120(%r12)
	movb	$0, 140(%r12)
	movl	$0, 144(%r12)
	movq	$0, 152(%r12)
	movl	$2, 168(%r12)
	movl	$0, 176(%r12)
	movq	$0, 184(%r12)
	movl	$0, 192(%r12)
	movl	$3, 216(%r12)
	movups	%xmm0, 200(%r12)
	call	_ZN6icu_676LocaleC1Ev@PLT
	movq	%rbx, -1256(%rbp)
	xorl	%esi, %esi
	cmpl	$-1, %r13d
	leaq	-1248(%rbp), %rbx
	sete	%sil
	movl	%r13d, %ecx
	movq	$0, 448(%r12)
	movq	$0, 456(%r12)
	leaq	-1256(%rbp), %rdx
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	leaq	-960(%rbp), %r10
	movq	%r14, %rdx
	leaq	-1184(%rbp), %r13
	movq	%rbx, %rsi
	movq	%r10, %rdi
	movq	%r10, -1272(%rbp)
	leaq	-512(%rbp), %r14
	call	_ZN6icu_676number15NumberFormatter11forSkeletonERKNS_13UnicodeStringER10UErrorCode@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	movq	-1272(%rbp), %r10
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%r10, %rsi
	call	_ZNO6icu_676number26UnlocalizedNumberFormatter6localeERKNS_6LocaleE@PLT
	movq	-1280(%rbp), %r9
	movq	%r14, %rsi
	movq	%r9, %rdi
	call	_ZN6icu_676number24LocalizedNumberFormatteraSEOS1_@PLT
	movq	%r14, %rdi
	call	_ZN6icu_676number24LocalizedNumberFormatterD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	leaq	-744(%rbp), %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	leaq	-792(%rbp), %rdi
	call	_ZN6icu_676number5ScaleD1Ev@PLT
	leaq	-824(%rbp), %rdi
	call	_ZN6icu_676number4impl14SymbolsWrapperD1Ev@PLT
	leaq	-920(%rbp), %rdi
	call	_ZN6icu_6711MeasureUnitD1Ev@PLT
	leaq	-944(%rbp), %rdi
	call	_ZN6icu_6711MeasureUnitD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L130:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L135
	addq	$1240, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L131:
	.cfi_restore_state
	movl	$7, (%r14)
	jmp	.L130
.L135:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2849:
	.size	unumf_openForSkeletonAndLocale_67, .-unumf_openForSkeletonAndLocale_67
	.p2align 4
	.globl	unumf_resultGetAllFieldPositions_67
	.type	unumf_resultGetAllFieldPositions_67, @function
unumf_resultGetAllFieldPositions_67:
.LFB2883:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L136
	movq	%rdi, %rbx
	movq	%rdx, %r12
	testq	%rdi, %rdi
	je	.L142
	cmpl	$1178881536, 16(%rdi)
	jne	.L147
	testq	%rsi, %rsi
	je	.L142
	leaq	-80(%rbp), %r13
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN6icu_6728FieldPositionIteratorHandlerC1EPNS_21FieldPositionIteratorER10UErrorCode@PLT
	leaq	48(%rbx), %rdi
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	_ZNK6icu_6731FormattedValueStringBuilderImpl20getAllFieldPositionsERNS_28FieldPositionIteratorHandlerER10UErrorCode@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6728FieldPositionIteratorHandlerD1Ev@PLT
.L136:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L148
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L147:
	.cfi_restore_state
	movl	$3, (%rdx)
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L142:
	movl	$1, (%r12)
	jmp	.L136
.L148:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2883:
	.size	unumf_resultGetAllFieldPositions_67, .-unumf_resultGetAllFieldPositions_67
	.section	.rodata
	.type	_ZN6icu_67L15kUndefinedFieldE, @object
	.size	_ZN6icu_67L15kUndefinedFieldE, 1
_ZN6icu_67L15kUndefinedFieldE:
	.zero	1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
