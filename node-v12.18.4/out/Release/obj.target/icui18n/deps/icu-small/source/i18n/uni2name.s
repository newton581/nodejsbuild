	.file	"uni2name.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6725UnicodeNameTransliterator17getDynamicClassIDEv
	.type	_ZNK6icu_6725UnicodeNameTransliterator17getDynamicClassIDEv, @function
_ZNK6icu_6725UnicodeNameTransliterator17getDynamicClassIDEv:
.LFB2346:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6725UnicodeNameTransliterator16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2346:
	.size	_ZNK6icu_6725UnicodeNameTransliterator17getDynamicClassIDEv, .-_ZNK6icu_6725UnicodeNameTransliterator17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725UnicodeNameTransliteratorD2Ev
	.type	_ZN6icu_6725UnicodeNameTransliteratorD2Ev, @function
_ZN6icu_6725UnicodeNameTransliteratorD2Ev:
.LFB2351:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6725UnicodeNameTransliteratorE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6714TransliteratorD2Ev@PLT
	.cfi_endproc
.LFE2351:
	.size	_ZN6icu_6725UnicodeNameTransliteratorD2Ev, .-_ZN6icu_6725UnicodeNameTransliteratorD2Ev
	.globl	_ZN6icu_6725UnicodeNameTransliteratorD1Ev
	.set	_ZN6icu_6725UnicodeNameTransliteratorD1Ev,_ZN6icu_6725UnicodeNameTransliteratorD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725UnicodeNameTransliteratorD0Ev
	.type	_ZN6icu_6725UnicodeNameTransliteratorD0Ev, @function
_ZN6icu_6725UnicodeNameTransliteratorD0Ev:
.LFB2353:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6725UnicodeNameTransliteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6714TransliteratorD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2353:
	.size	_ZN6icu_6725UnicodeNameTransliteratorD0Ev, .-_ZN6icu_6725UnicodeNameTransliteratorD0Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6725UnicodeNameTransliterator5cloneEv
	.type	_ZNK6icu_6725UnicodeNameTransliterator5cloneEv, @function
_ZNK6icu_6725UnicodeNameTransliterator5cloneEv:
.LFB2357:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movl	$88, %edi
	pushq	%r12
	.cfi_offset 12, -32
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L6
	movq	%rax, %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6714TransliteratorC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6725UnicodeNameTransliteratorE(%rip), %rax
	movq	%rax, (%r12)
.L6:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2357:
	.size	_ZNK6icu_6725UnicodeNameTransliterator5cloneEv, .-_ZNK6icu_6725UnicodeNameTransliterator5cloneEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725UnicodeNameTransliterator16getStaticClassIDEv
	.type	_ZN6icu_6725UnicodeNameTransliterator16getStaticClassIDEv, @function
_ZN6icu_6725UnicodeNameTransliterator16getStaticClassIDEv:
.LFB2345:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6725UnicodeNameTransliterator16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2345:
	.size	_ZN6icu_6725UnicodeNameTransliterator16getStaticClassIDEv, .-_ZN6icu_6725UnicodeNameTransliterator16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725UnicodeNameTransliteratorC2ERKS0_
	.type	_ZN6icu_6725UnicodeNameTransliteratorC2ERKS0_, @function
_ZN6icu_6725UnicodeNameTransliteratorC2ERKS0_:
.LFB2355:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_6714TransliteratorC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6725UnicodeNameTransliteratorE(%rip), %rax
	movq	%rax, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2355:
	.size	_ZN6icu_6725UnicodeNameTransliteratorC2ERKS0_, .-_ZN6icu_6725UnicodeNameTransliteratorC2ERKS0_
	.globl	_ZN6icu_6725UnicodeNameTransliteratorC1ERKS0_
	.set	_ZN6icu_6725UnicodeNameTransliteratorC1ERKS0_,_ZN6icu_6725UnicodeNameTransliteratorC2ERKS0_
	.section	.rodata.str2.2,"aMS",@progbits,2
	.align 2
.LC0:
	.string	"A"
	.string	"n"
	.string	"y"
	.string	"-"
	.string	"N"
	.string	"a"
	.string	"m"
	.string	"e"
	.string	""
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725UnicodeNameTransliteratorC2EPNS_13UnicodeFilterE
	.type	_ZN6icu_6725UnicodeNameTransliteratorC2EPNS_13UnicodeFilterE, @function
_ZN6icu_6725UnicodeNameTransliteratorC2EPNS_13UnicodeFilterE:
.LFB2348:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$8, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	movl	$1, %esi
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-112(%rbp), %r12
	leaq	-120(%rbp), %rdx
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	.LC0(%rip), %rax
	movq	%rax, -120(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6714TransliteratorC2ERKNS_13UnicodeStringEPNS_13UnicodeFilterE@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	16+_ZTVN6icu_6725UnicodeNameTransliteratorE(%rip), %rax
	movq	%rax, (%rbx)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L18
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L18:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2348:
	.size	_ZN6icu_6725UnicodeNameTransliteratorC2EPNS_13UnicodeFilterE, .-_ZN6icu_6725UnicodeNameTransliteratorC2EPNS_13UnicodeFilterE
	.globl	_ZN6icu_6725UnicodeNameTransliteratorC1EPNS_13UnicodeFilterE
	.set	_ZN6icu_6725UnicodeNameTransliteratorC1EPNS_13UnicodeFilterE,_ZN6icu_6725UnicodeNameTransliteratorC2EPNS_13UnicodeFilterE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6725UnicodeNameTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositiona
	.type	_ZNK6icu_6725UnicodeNameTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositiona, @function
_ZNK6icu_6725UnicodeNameTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositiona:
.LFB2358:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -248(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	uprv_getMaxCharNameLength_67@PLT
	testl	%eax, %eax
	jne	.L20
	movl	12(%r14), %eax
	movl	%eax, 8(%r14)
.L19:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L39
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	movslq	%eax, %rdi
	movl	%eax, %ebx
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L40
	movq	-248(%rbp), %rax
	leaq	-200(%rbp), %rdx
	movl	$3, %ecx
	xorl	%esi, %esi
	movq	%rdx, -224(%rbp)
	movl	12(%rax), %r15d
	movl	8(%rax), %r12d
	leaq	_ZN6icu_67L10OPEN_DELIME(%rip), %rax
	movq	%rax, -200(%rbp)
	leaq	-192(%rbp), %rax
	movq	%rax, %rdi
	movl	%r15d, -212(%rbp)
	movq	%rax, -240(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	cmpl	%r15d, %r12d
	jge	.L23
	leaq	-128(%rbp), %rax
	movq	%r13, %r15
	movq	%rax, -232(%rbp)
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L26:
	addl	%r13d, %r12d
	cmpl	%r12d, -212(%rbp)
	jle	.L23
.L24:
	movq	(%r15), %rax
	movl	%r12d, %esi
	movq	%r15, %rdi
	xorl	%r13d, %r13d
	call	*80(%rax)
	movq	-224(%rbp), %r8
	movl	%ebx, %ecx
	movq	%r14, %rdx
	cmpl	$65535, %eax
	movl	%eax, %edi
	movl	$2, %esi
	movl	$0, -200(%rbp)
	seta	%r13b
	call	u_charName_67@PLT
	addl	$1, %r13d
	movl	%eax, %r8d
	testl	%eax, %eax
	jle	.L26
	movl	-200(%rbp), %edx
	testl	%edx, %edx
	jg	.L26
	movzwl	-184(%rbp), %eax
	testw	%ax, %ax
	js	.L27
	movswl	%ax, %edx
	sarl	$5, %edx
.L28:
	cmpl	$3, %edx
	jbe	.L29
	andl	$31, %eax
	orl	$96, %eax
	movw	%ax, -184(%rbp)
.L29:
	movq	-232(%rbp), %rdi
	movl	%r8d, %edx
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movl	%r8d, -216(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EPKciNS0_10EInvariantE@PLT
	movswl	-120(%rbp), %ecx
	movq	-232(%rbp), %rsi
	movq	-240(%rbp), %rdi
	movl	%ecx, %eax
	sarl	$5, %ecx
	testw	%ax, %ax
	cmovs	-116(%rbp), %ecx
	xorl	%edx, %edx
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	leaq	-202(%rbp), %rsi
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%rax, %rdi
	movl	$125, %eax
	movw	%ax, -202(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movq	-232(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	(%r15), %rax
	leal	0(%r13,%r12), %edx
	movl	%r12d, %esi
	movq	-240(%rbp), %rcx
	movq	%r15, %rdi
	call	*32(%rax)
	movl	-216(%rbp), %r8d
	addl	$4, %r8d
	addl	%r8d, %r12d
	subl	%r13d, %r8d
	addl	%r8d, -212(%rbp)
	cmpl	%r12d, -212(%rbp)
	jg	.L24
	.p2align 4,,10
	.p2align 3
.L23:
	movq	-248(%rbp), %rbx
	movl	-212(%rbp), %ecx
	movq	%r14, %rdi
	movl	%ecx, %eax
	subl	12(%rbx), %eax
	addl	%eax, 4(%rbx)
	movl	%ecx, 12(%rbx)
	movl	%r12d, 8(%rbx)
	call	uprv_free_67@PLT
	movq	-240(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L27:
	movl	-180(%rbp), %edx
	jmp	.L28
.L39:
	call	__stack_chk_fail@PLT
.L40:
	movq	-248(%rbp), %rcx
	movl	12(%rcx), %eax
	movl	%eax, 8(%rcx)
	jmp	.L19
	.cfi_endproc
.LFE2358:
	.size	_ZNK6icu_6725UnicodeNameTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositiona, .-_ZNK6icu_6725UnicodeNameTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositiona
	.weak	_ZTSN6icu_6725UnicodeNameTransliteratorE
	.section	.rodata._ZTSN6icu_6725UnicodeNameTransliteratorE,"aG",@progbits,_ZTSN6icu_6725UnicodeNameTransliteratorE,comdat
	.align 32
	.type	_ZTSN6icu_6725UnicodeNameTransliteratorE, @object
	.size	_ZTSN6icu_6725UnicodeNameTransliteratorE, 37
_ZTSN6icu_6725UnicodeNameTransliteratorE:
	.string	"N6icu_6725UnicodeNameTransliteratorE"
	.weak	_ZTIN6icu_6725UnicodeNameTransliteratorE
	.section	.data.rel.ro._ZTIN6icu_6725UnicodeNameTransliteratorE,"awG",@progbits,_ZTIN6icu_6725UnicodeNameTransliteratorE,comdat
	.align 8
	.type	_ZTIN6icu_6725UnicodeNameTransliteratorE, @object
	.size	_ZTIN6icu_6725UnicodeNameTransliteratorE, 24
_ZTIN6icu_6725UnicodeNameTransliteratorE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6725UnicodeNameTransliteratorE
	.quad	_ZTIN6icu_6714TransliteratorE
	.weak	_ZTVN6icu_6725UnicodeNameTransliteratorE
	.section	.data.rel.ro._ZTVN6icu_6725UnicodeNameTransliteratorE,"awG",@progbits,_ZTVN6icu_6725UnicodeNameTransliteratorE,comdat
	.align 8
	.type	_ZTVN6icu_6725UnicodeNameTransliteratorE, @object
	.size	_ZTVN6icu_6725UnicodeNameTransliteratorE, 152
_ZTVN6icu_6725UnicodeNameTransliteratorE:
	.quad	0
	.quad	_ZTIN6icu_6725UnicodeNameTransliteratorE
	.quad	_ZN6icu_6725UnicodeNameTransliteratorD1Ev
	.quad	_ZN6icu_6725UnicodeNameTransliteratorD0Ev
	.quad	_ZNK6icu_6725UnicodeNameTransliterator17getDynamicClassIDEv
	.quad	_ZNK6icu_6725UnicodeNameTransliterator5cloneEv
	.quad	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableEii
	.quad	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableE
	.quad	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableER14UTransPositionRKNS_13UnicodeStringER10UErrorCode
	.quad	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableER14UTransPositioniR10UErrorCode
	.quad	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableER14UTransPositionR10UErrorCode
	.quad	_ZNK6icu_6714Transliterator21finishTransliterationERNS_11ReplaceableER14UTransPosition
	.quad	_ZNK6icu_6725UnicodeNameTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositiona
	.quad	_ZNK6icu_6714Transliterator21filteredTransliterateERNS_11ReplaceableER14UTransPositiona
	.quad	_ZNK6icu_6714Transliterator21filteredTransliterateERNS_11ReplaceableER14UTransPositionaa
	.quad	_ZNK6icu_6714Transliterator5getIDEv
	.quad	_ZNK6icu_6714Transliterator7toRulesERNS_13UnicodeStringEa
	.quad	_ZNK6icu_6714Transliterator18handleGetSourceSetERNS_10UnicodeSetE
	.quad	_ZNK6icu_6714Transliterator12getTargetSetERNS_10UnicodeSetE
	.section	.rodata
	.align 8
	.type	_ZN6icu_67L10OPEN_DELIME, @object
	.size	_ZN6icu_67L10OPEN_DELIME, 8
_ZN6icu_67L10OPEN_DELIME:
	.value	92
	.value	78
	.value	123
	.value	0
	.local	_ZZN6icu_6725UnicodeNameTransliterator16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6725UnicodeNameTransliterator16getStaticClassIDEvE7classID,1,1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
