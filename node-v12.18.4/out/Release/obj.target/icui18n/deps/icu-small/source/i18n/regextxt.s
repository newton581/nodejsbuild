	.file	"regextxt.cpp"
	.text
	.p2align 4
	.globl	uregex_utext_unescape_charAt_67
	.type	uregex_utext_unescape_charAt_67, @function
uregex_utext_unescape_charAt_67:
.LFB2224:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edi, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movl	8(%rsi), %eax
	movq	(%rsi), %rdi
	leal	1(%rax), %edx
	cmpl	%r13d, %edx
	je	.L16
	cmpl	%r13d, %eax
	jne	.L6
	movl	40(%rdi), %eax
	testl	%eax, %eax
	jle	.L7
	movq	48(%rdi), %rdx
	movslq	%eax, %rcx
	cmpw	$-10241, -2(%rdx,%rcx,2)
	jbe	.L17
.L7:
	call	utext_previous32_67@PLT
	movq	(%r12), %rdi
	movl	%eax, %ebx
	movl	40(%rdi), %eax
.L8:
	cmpl	%eax, 44(%rdi)
	jle	.L9
	movq	48(%rdi), %rcx
	movslq	%eax, %rdx
	cmpw	$-10241, (%rcx,%rdx,2)
	jbe	.L18
.L9:
	call	utext_next32_67@PLT
.L5:
	cmpl	$65536, %ebx
	movl	$0, %eax
	cmovnb	%eax, %ebx
	addq	$8, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	movl	%r13d, %esi
	subl	%eax, %esi
	subl	$1, %esi
	call	utext_moveIndex32_67@PLT
	movq	(%r12), %rdi
	movl	40(%rdi), %eax
	cmpl	44(%rdi), %eax
	jge	.L10
	movq	48(%rdi), %rcx
	movslq	%eax, %rdx
	movzwl	(%rcx,%rdx,2), %ebx
	cmpw	$-10241, %bx
	ja	.L10
.L14:
	addl	$1, %eax
	movl	%eax, 40(%rdi)
.L11:
	movl	%r13d, 8(%r12)
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L10:
	call	utext_next32_67@PLT
	movl	%eax, %ebx
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L16:
	movl	40(%rdi), %eax
	cmpl	44(%rdi), %eax
	jge	.L3
	movq	48(%rdi), %rcx
	movslq	%eax, %rdx
	movzwl	(%rcx,%rdx,2), %ebx
	cmpw	$-10241, %bx
	jbe	.L14
.L3:
	call	utext_next32_67@PLT
	movl	%eax, %ebx
	movl	8(%r12), %eax
	leal	1(%rax), %r13d
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L17:
	subl	$1, %eax
	movslq	%eax, %rcx
	movl	%eax, 40(%rdi)
	movzwl	(%rdx,%rcx,2), %ebx
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L18:
	addl	$1, %eax
	movl	%eax, 40(%rdi)
	jmp	.L5
	.cfi_endproc
.LFE2224:
	.size	uregex_utext_unescape_charAt_67, .-uregex_utext_unescape_charAt_67
	.p2align 4
	.globl	uregex_ucstr_unescape_charAt_67
	.type	uregex_ucstr_unescape_charAt_67, @function
uregex_ucstr_unescape_charAt_67:
.LFB2225:
	.cfi_startproc
	endbr64
	movslq	%edi, %rdi
	movzwl	(%rsi,%rdi,2), %eax
	ret
	.cfi_endproc
.LFE2225:
	.size	uregex_ucstr_unescape_charAt_67, .-uregex_ucstr_unescape_charAt_67
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
