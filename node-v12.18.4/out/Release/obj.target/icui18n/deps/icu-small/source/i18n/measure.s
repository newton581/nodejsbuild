	.file	"measure.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_677Measure17getDynamicClassIDEv
	.type	_ZNK6icu_677Measure17getDynamicClassIDEv, @function
_ZNK6icu_677Measure17getDynamicClassIDEv:
.LFB2237:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_677Measure16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2237:
	.size	_ZNK6icu_677Measure17getDynamicClassIDEv, .-_ZNK6icu_677Measure17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_677Measure5cloneEv
	.type	_ZNK6icu_677Measure5cloneEv, @function
_ZNK6icu_677Measure5cloneEv:
.LFB2251:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movl	$128, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L3
	leaq	16+_ZTVN6icu_677MeasureE(%rip), %rax
	leaq	8(%r12), %r13
	movq	%rax, (%r12)
	movq	%r13, %rdi
	call	_ZN6icu_6711FormattableC1Ev@PLT
	movq	$0, 120(%r12)
	cmpq	%rbx, %r12
	je	.L3
	leaq	8(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6711FormattableaSERKS0_@PLT
	movq	120(%rbx), %rdi
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	%rax, 120(%r12)
.L3:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2251:
	.size	_ZNK6icu_677Measure5cloneEv, .-_ZNK6icu_677Measure5cloneEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_677Measure16getStaticClassIDEv
	.type	_ZN6icu_677Measure16getStaticClassIDEv, @function
_ZN6icu_677Measure16getStaticClassIDEv:
.LFB2236:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_677Measure16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2236:
	.size	_ZN6icu_677Measure16getStaticClassIDEv, .-_ZN6icu_677Measure16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_677MeasureC2Ev
	.type	_ZN6icu_677MeasureC2Ev, @function
_ZN6icu_677MeasureC2Ev:
.LFB2239:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_677MeasureE(%rip), %rax
	addq	$8, %rdi
	movq	%rax, -8(%rdi)
	jmp	_ZN6icu_6711FormattableC1Ev@PLT
	.cfi_endproc
.LFE2239:
	.size	_ZN6icu_677MeasureC2Ev, .-_ZN6icu_677MeasureC2Ev
	.globl	_ZN6icu_677MeasureC1Ev
	.set	_ZN6icu_677MeasureC1Ev,_ZN6icu_677MeasureC2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_677MeasureC2ERKNS_11FormattableEPNS_11MeasureUnitER10UErrorCode
	.type	_ZN6icu_677MeasureC2ERKNS_11FormattableEPNS_11MeasureUnitER10UErrorCode, @function
_ZN6icu_677MeasureC2ERKNS_11FormattableEPNS_11MeasureUnitER10UErrorCode:
.LFB2242:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_677MeasureE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	8(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	movq	%r14, %rdi
	call	_ZN6icu_6711FormattableC1ERKS0_@PLT
	movl	0(%r13), %eax
	movq	%r12, 120(%rbx)
	testl	%eax, %eax
	jle	.L20
.L10:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	movq	%r14, %rdi
	call	_ZNK6icu_6711Formattable9isNumericEv@PLT
	testb	%al, %al
	je	.L15
	testq	%r12, %r12
	jne	.L10
.L15:
	popq	%rbx
	popq	%r12
	movl	$1, 0(%r13)
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2242:
	.size	_ZN6icu_677MeasureC2ERKNS_11FormattableEPNS_11MeasureUnitER10UErrorCode, .-_ZN6icu_677MeasureC2ERKNS_11FormattableEPNS_11MeasureUnitER10UErrorCode
	.globl	_ZN6icu_677MeasureC1ERKNS_11FormattableEPNS_11MeasureUnitER10UErrorCode
	.set	_ZN6icu_677MeasureC1ERKNS_11FormattableEPNS_11MeasureUnitER10UErrorCode,_ZN6icu_677MeasureC2ERKNS_11FormattableEPNS_11MeasureUnitER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_677MeasureC2ERKS0_
	.type	_ZN6icu_677MeasureC2ERKS0_, @function
_ZN6icu_677MeasureC2ERKS0_:
.LFB2248:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_677MeasureE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	8(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	%r13, %rdi
	call	_ZN6icu_6711FormattableC1Ev@PLT
	movq	$0, 120(%rbx)
	cmpq	%r12, %rbx
	je	.L21
	leaq	8(%r12), %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6711FormattableaSERKS0_@PLT
	movq	120(%r12), %rdi
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	%rax, 120(%rbx)
.L21:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2248:
	.size	_ZN6icu_677MeasureC2ERKS0_, .-_ZN6icu_677MeasureC2ERKS0_
	.globl	_ZN6icu_677MeasureC1ERKS0_
	.set	_ZN6icu_677MeasureC1ERKS0_,_ZN6icu_677MeasureC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_677MeasureaSERKS0_
	.type	_ZN6icu_677MeasureaSERKS0_, @function
_ZN6icu_677MeasureaSERKS0_:
.LFB2250:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpq	%rsi, %rdi
	je	.L25
	movq	120(%rdi), %rdi
	movq	%rsi, %rbx
	testq	%rdi, %rdi
	je	.L26
	movq	(%rdi), %rax
	call	*8(%rax)
.L26:
	leaq	8(%rbx), %rsi
	leaq	8(%r12), %rdi
	call	_ZN6icu_6711FormattableaSERKS0_@PLT
	movq	120(%rbx), %rdi
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	%rax, 120(%r12)
.L25:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2250:
	.size	_ZN6icu_677MeasureaSERKS0_, .-_ZN6icu_677MeasureaSERKS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_677MeasureeqERKNS_7UObjectE
	.type	_ZNK6icu_677MeasureeqERKNS_7UObjectE, @function
_ZNK6icu_677MeasureeqERKNS_7UObjectE:
.LFB2256:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	cmpq	%rsi, %rdi
	je	.L35
	movq	(%rdi), %rax
	movq	%rdi, %r13
	movq	%rsi, %rbx
	movq	-8(%rax), %rax
	movq	8(%rax), %rdi
	movq	(%rsi), %rax
	movq	-8(%rax), %rax
	movq	8(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L33
	xorl	%r12d, %r12d
	cmpb	$42, (%rdi)
	je	.L31
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L31
.L33:
	leaq	8(%rbx), %rsi
	leaq	8(%r13), %rdi
	call	_ZNK6icu_6711FormattableeqERKS0_@PLT
	movl	%eax, %r12d
	testb	%al, %al
	je	.L31
	movq	120(%r13), %rdi
	movq	120(%rbx), %rsi
	testq	%rdi, %rdi
	sete	%dl
	testq	%rsi, %rsi
	sete	%al
	xorl	%r12d, %r12d
	cmpb	%al, %dl
	je	.L45
.L31:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	.cfi_restore_state
	movl	$1, %r12d
	testq	%rdi, %rdi
	je	.L31
	movq	(%rdi), %rax
	call	*32(%rax)
	testb	%al, %al
	setne	%r12b
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L35:
	addq	$8, %rsp
	movl	$1, %r12d
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2256:
	.size	_ZNK6icu_677MeasureeqERKNS_7UObjectE, .-_ZNK6icu_677MeasureeqERKNS_7UObjectE
	.align 2
	.p2align 4
	.globl	_ZN6icu_677MeasureD2Ev
	.type	_ZN6icu_677MeasureD2Ev, @function
_ZN6icu_677MeasureD2Ev:
.LFB2253:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_677MeasureE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	120(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L47
	movq	(%rdi), %rax
	call	*8(%rax)
.L47:
	leaq	8(%r12), %rdi
	call	_ZN6icu_6711FormattableD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE2253:
	.size	_ZN6icu_677MeasureD2Ev, .-_ZN6icu_677MeasureD2Ev
	.globl	_ZN6icu_677MeasureD1Ev
	.set	_ZN6icu_677MeasureD1Ev,_ZN6icu_677MeasureD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_677MeasureD0Ev
	.type	_ZN6icu_677MeasureD0Ev, @function
_ZN6icu_677MeasureD0Ev:
.LFB2255:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_677MeasureE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	120(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L53
	movq	(%rdi), %rax
	call	*8(%rax)
.L53:
	leaq	8(%r12), %rdi
	call	_ZN6icu_6711FormattableD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2255:
	.size	_ZN6icu_677MeasureD0Ev, .-_ZN6icu_677MeasureD0Ev
	.weak	_ZTSN6icu_677MeasureE
	.section	.rodata._ZTSN6icu_677MeasureE,"aG",@progbits,_ZTSN6icu_677MeasureE,comdat
	.align 16
	.type	_ZTSN6icu_677MeasureE, @object
	.size	_ZTSN6icu_677MeasureE, 18
_ZTSN6icu_677MeasureE:
	.string	"N6icu_677MeasureE"
	.weak	_ZTIN6icu_677MeasureE
	.section	.data.rel.ro._ZTIN6icu_677MeasureE,"awG",@progbits,_ZTIN6icu_677MeasureE,comdat
	.align 8
	.type	_ZTIN6icu_677MeasureE, @object
	.size	_ZTIN6icu_677MeasureE, 24
_ZTIN6icu_677MeasureE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_677MeasureE
	.quad	_ZTIN6icu_677UObjectE
	.weak	_ZTVN6icu_677MeasureE
	.section	.data.rel.ro.local._ZTVN6icu_677MeasureE,"awG",@progbits,_ZTVN6icu_677MeasureE,comdat
	.align 8
	.type	_ZTVN6icu_677MeasureE, @object
	.size	_ZTVN6icu_677MeasureE, 48
_ZTVN6icu_677MeasureE:
	.quad	0
	.quad	_ZTIN6icu_677MeasureE
	.quad	_ZN6icu_677MeasureD1Ev
	.quad	_ZN6icu_677MeasureD0Ev
	.quad	_ZNK6icu_677Measure17getDynamicClassIDEv
	.quad	_ZNK6icu_677Measure5cloneEv
	.local	_ZZN6icu_677Measure16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_677Measure16getStaticClassIDEvE7classID,1,1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
