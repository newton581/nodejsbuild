	.file	"cpdtrans.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722CompoundTransliterator17getDynamicClassIDEv
	.type	_ZNK6icu_6722CompoundTransliterator17getDynamicClassIDEv, @function
_ZNK6icu_6722CompoundTransliterator17getDynamicClassIDEv:
.LFB2372:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6722CompoundTransliterator16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2372:
	.size	_ZNK6icu_6722CompoundTransliterator17getDynamicClassIDEv, .-_ZNK6icu_6722CompoundTransliterator17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722CompoundTransliterator8getCountEv
	.type	_ZNK6icu_6722CompoundTransliterator8getCountEv, @function
_ZNK6icu_6722CompoundTransliterator8getCountEv:
.LFB2404:
	.cfi_startproc
	endbr64
	movl	96(%rdi), %eax
	ret
	.cfi_endproc
.LFE2404:
	.size	_ZNK6icu_6722CompoundTransliterator8getCountEv, .-_ZNK6icu_6722CompoundTransliterator8getCountEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722CompoundTransliterator17getTransliteratorEi
	.type	_ZNK6icu_6722CompoundTransliterator17getTransliteratorEi, @function
_ZNK6icu_6722CompoundTransliterator17getTransliteratorEi:
.LFB2405:
	.cfi_startproc
	endbr64
	movq	88(%rdi), %rax
	movslq	%esi, %rsi
	movq	(%rax,%rsi,8), %rax
	ret
	.cfi_endproc
.LFE2405:
	.size	_ZNK6icu_6722CompoundTransliterator17getTransliteratorEi, .-_ZNK6icu_6722CompoundTransliterator17getTransliteratorEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722CompoundTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositiona
	.type	_ZNK6icu_6722CompoundTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositiona, @function
_ZNK6icu_6722CompoundTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositiona:
.LFB2412:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$40, %rsp
	movl	12(%rdx), %eax
	movl	96(%rdi), %edx
	movq	%rsi, -56(%rbp)
	movl	%eax, -64(%rbp)
	testl	%edx, %edx
	jle	.L18
	movl	8(%rbx), %r15d
	movq	%rdi, %r13
	testb	%cl, %cl
	jne	.L19
	xorl	%r14d, %r14d
	xorl	%r12d, %r12d
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L20:
	movq	88(%r13), %rdx
	xorl	%ecx, %ecx
	movq	-56(%rbp), %rsi
	movq	(%rdx,%r14,8), %rdi
	movq	%rbx, %rdx
	movq	(%rdi), %r10
	call	*88(%r10)
	movl	12(%rbx), %edx
	cmpl	%edx, 8(%rbx)
	movl	-60(%rbp), %eax
	je	.L17
	movl	%edx, 8(%rbx)
.L17:
	movl	%edx, %ecx
	addq	$1, %r14
	subl	%eax, %ecx
	addl	%ecx, %r12d
	cmpl	%r14d, 96(%r13)
	jle	.L14
	movl	%edx, %eax
.L8:
	cmpl	%eax, %r15d
	movl	%r15d, 8(%rbx)
	movl	%eax, -60(%rbp)
	jne	.L20
.L14:
	addl	-64(%rbp), %r12d
	movl	%r12d, 12(%rbx)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	movsbl	%cl, %r14d
	movl	%eax, %r9d
	xorl	%r10d, %r10d
	xorl	%r12d, %r12d
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L21:
	movq	88(%r13), %rdx
	movq	-56(%rbp), %rsi
	movq	%r10, -72(%rbp)
	movl	%r14d, %ecx
	movq	(%rdx,%r10,8), %rdi
	movq	%rbx, %rdx
	movq	(%rdi), %r11
	call	*88(%r11)
	movl	12(%rbx), %edx
	movl	-60(%rbp), %r9d
	movq	-72(%rbp), %r10
	subl	%r9d, %edx
	movl	8(%rbx), %r9d
	addl	%edx, %r12d
	addq	$1, %r10
	movl	%r9d, 12(%rbx)
	cmpl	%r10d, 96(%r13)
	jle	.L14
.L11:
	cmpl	%r9d, %r15d
	movl	%r15d, 8(%rbx)
	movl	%r9d, -60(%rbp)
	jne	.L21
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L18:
	movl	%eax, 8(%rbx)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2412:
	.size	_ZNK6icu_6722CompoundTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositiona, .-_ZNK6icu_6722CompoundTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositiona
	.align 2
	.p2align 4
	.type	_ZN6icu_6722CompoundTransliterator4initERNS_7UVectorE15UTransDirectionaR10UErrorCode.part.0, @function
_ZN6icu_6722CompoundTransliterator4initERNS_7UVectorE15UTransDirectionaR10UErrorCode.part.0:
.LFB3071:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$104, %rsp
	movl	96(%rdi), %esi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$1, %edx
	sete	%r12b
	testb	%cl, %cl
	setne	%al
	andl	%eax, %r12d
	testl	%esi, %esi
	jle	.L23
	testl	%edx, %edx
	jne	.L44
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L26:
	movq	88(%r13), %rax
	movl	%r12d, %esi
	movq	%rbx, %rdi
	leaq	(%rax,%r12,8), %r14
	addq	$1, %r12
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movl	96(%r13), %esi
	movq	%rax, (%r14)
	cmpl	%r12d, %esi
	jg	.L26
.L25:
	testl	%esi, %esi
	jle	.L42
	movq	88(%r13), %rdx
	leal	-1(%rsi), %ecx
	xorl	%esi, %esi
	leaq	8(%rdx), %rax
	leaq	(%rax,%rcx,8), %rcx
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L63:
	addq	$8, %rax
.L40:
	movq	(%rdx), %rdx
	movl	80(%rdx), %edx
	cmpl	%edx, %esi
	cmovl	%edx, %esi
	movq	%rax, %rdx
	cmpq	%rax, %rcx
	jne	.L63
.L39:
	movq	%r13, %rdi
	call	_ZN6icu_6714Transliterator23setMaximumContextLengthEi@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L64
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	.cfi_restore_state
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L24:
	movq	88(%r13), %rax
	subl	$1, %esi
	movq	%rbx, %rdi
	subl	%r14d, %esi
	leaq	(%rax,%r14,8), %r15
	addq	$1, %r14
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movl	96(%r13), %esi
	movq	%rax, (%r15)
	cmpl	%r14d, %esi
	jg	.L24
	testb	%r12b, %r12b
	je	.L25
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %edi
	movq	%rax, -128(%rbp)
	movw	%di, -120(%rbp)
	testl	%esi, %esi
	jle	.L65
	xorl	%ebx, %ebx
	leaq	-128(%rbp), %r12
	leaq	-130(%rbp), %r14
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L66:
	sarl	$5, %ecx
.L62:
	xorl	%edx, %edx
	movq	%r12, %rdi
	addq	$1, %rbx
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	cmpl	%ebx, 96(%r13)
	jle	.L31
	movl	$59, %edx
	movl	$1, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movw	%dx, -130(%rbp)
	xorl	%edx, %edx
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
.L27:
	movq	88(%r13), %rax
	movq	(%rax,%rbx,8), %rdi
	movq	(%rdi), %rax
	call	*104(%rax)
	movswl	8(%rax), %ecx
	movq	%rax, %rsi
	testw	%cx, %cx
	jns	.L66
	movl	12(%rax), %ecx
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L23:
	testb	%r12b, %r12b
	je	.L42
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	leaq	-128(%rbp), %r12
	movq	%rax, -128(%rbp)
	movl	$2, %eax
	leaq	-130(%rbp), %r14
	movw	%ax, -120(%rbp)
	.p2align 4,,10
	.p2align 3
.L31:
	leaq	8(%r13), %r15
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movw	%cx, -130(%rbp)
	movq	%r15, %rdi
	movl	$1, %ecx
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movzwl	16(%r13), %eax
	testw	%ax, %ax
	js	.L67
	movswl	%ax, %edx
	sarl	$5, %edx
	leal	-1(%rdx), %ecx
	testb	$1, %al
	jne	.L33
	cmpl	%edx, %ecx
	jnb	.L36
.L41:
	andl	$31, %eax
	sall	$5, %ecx
	orl	%ecx, %eax
	movw	%ax, 16(%r13)
.L36:
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	96(%r13), %esi
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L67:
	movl	20(%r13), %edx
	leal	-1(%rdx), %ecx
	testb	$1, %al
	je	.L35
.L33:
	testl	%ecx, %ecx
	je	.L68
.L35:
	cmpl	%ecx, %edx
	jbe	.L36
	cmpl	$1023, %ecx
	jle	.L41
	orl	$-32, %eax
	movl	%ecx, 20(%r13)
	movq	%r12, %rdi
	movw	%ax, 16(%r13)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	96(%r13), %esi
	jmp	.L25
.L68:
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L42:
	xorl	%esi, %esi
	jmp	.L39
.L64:
	call	__stack_chk_fail@PLT
.L65:
	leaq	-128(%rbp), %r12
	leaq	-130(%rbp), %r14
	jmp	.L31
	.cfi_endproc
.LFE3071:
	.size	_ZN6icu_6722CompoundTransliterator4initERNS_7UVectorE15UTransDirectionaR10UErrorCode.part.0, .-_ZN6icu_6722CompoundTransliterator4initERNS_7UVectorE15UTransDirectionaR10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722CompoundTransliterator16getStaticClassIDEv
	.type	_ZN6icu_6722CompoundTransliterator16getStaticClassIDEv, @function
_ZN6icu_6722CompoundTransliterator16getStaticClassIDEv:
.LFB2371:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6722CompoundTransliterator16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2371:
	.size	_ZN6icu_6722CompoundTransliterator16getStaticClassIDEv, .-_ZN6icu_6722CompoundTransliterator16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722CompoundTransliteratorC2ERKNS_13UnicodeStringERNS_7UVectorEPNS_13UnicodeFilterEiR11UParseErrorR10UErrorCode
	.type	_ZN6icu_6722CompoundTransliteratorC2ERKNS_13UnicodeStringERNS_7UVectorEPNS_13UnicodeFilterEiR11UParseErrorR10UErrorCode, @function
_ZN6icu_6722CompoundTransliteratorC2ERKNS_13UnicodeStringERNS_7UVectorEPNS_13UnicodeFilterEiR11UParseErrorR10UErrorCode:
.LFB2383:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	16(%rbp), %r14
	movq	%rdx, %r13
	movq	%rcx, %rdx
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%r8d, %ebx
	call	_ZN6icu_6714TransliteratorC2ERKNS_13UnicodeStringEPNS_13UnicodeFilterE@PLT
	movl	(%r14), %edx
	movl	%ebx, 100(%r12)
	leaq	16+_ZTVN6icu_6722CompoundTransliteratorE(%rip), %rax
	movq	%rax, (%r12)
	movq	$0, 88(%r12)
	testl	%edx, %edx
	jle	.L78
.L70:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L78:
	.cfi_restore_state
	movslq	8(%r13), %rdi
	movl	%edi, 96(%r12)
	salq	$3, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, 88(%r12)
	testq	%rax, %rax
	je	.L79
	movl	(%r14), %eax
	testl	%eax, %eax
	jg	.L70
	popq	%rbx
	movq	%r13, %rsi
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	popq	%r12
	xorl	%edx, %edx
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6722CompoundTransliterator4initERNS_7UVectorE15UTransDirectionaR10UErrorCode.part.0
.L79:
	.cfi_restore_state
	movl	$7, (%r14)
	jmp	.L70
	.cfi_endproc
.LFE2383:
	.size	_ZN6icu_6722CompoundTransliteratorC2ERKNS_13UnicodeStringERNS_7UVectorEPNS_13UnicodeFilterEiR11UParseErrorR10UErrorCode, .-_ZN6icu_6722CompoundTransliteratorC2ERKNS_13UnicodeStringERNS_7UVectorEPNS_13UnicodeFilterEiR11UParseErrorR10UErrorCode
	.globl	_ZN6icu_6722CompoundTransliteratorC1ERKNS_13UnicodeStringERNS_7UVectorEPNS_13UnicodeFilterEiR11UParseErrorR10UErrorCode
	.set	_ZN6icu_6722CompoundTransliteratorC1ERKNS_13UnicodeStringERNS_7UVectorEPNS_13UnicodeFilterEiR11UParseErrorR10UErrorCode,_ZN6icu_6722CompoundTransliteratorC2ERKNS_13UnicodeStringERNS_7UVectorEPNS_13UnicodeFilterEiR11UParseErrorR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722CompoundTransliteratorC2ERNS_7UVectorER11UParseErrorR10UErrorCode
	.type	_ZN6icu_6722CompoundTransliteratorC2ERNS_7UVectorER11UParseErrorR10UErrorCode, @function
_ZN6icu_6722CompoundTransliteratorC2ERNS_7UVectorER11UParseErrorR10UErrorCode:
.LFB2386:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	-112(%rbp), %r13
	pushq	%r12
	movq	%r13, %rsi
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rcx, %rbx
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%dx, -104(%rbp)
	xorl	%edx, %edx
	movq	%rax, -112(%rbp)
	call	_ZN6icu_6714TransliteratorC2ERKNS_13UnicodeStringEPNS_13UnicodeFilterE@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	(%rbx), %ecx
	leaq	16+_ZTVN6icu_6722CompoundTransliteratorE(%rip), %rax
	movq	$0, 88(%r12)
	movq	%rax, (%r12)
	movl	$0, 100(%r12)
	testl	%ecx, %ecx
	jle	.L89
.L80:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L90
	addq	$80, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L89:
	.cfi_restore_state
	movslq	8(%r14), %rdi
	movl	%edi, 96(%r12)
	salq	$3, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, 88(%r12)
	testq	%rax, %rax
	je	.L91
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L80
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6722CompoundTransliterator4initERNS_7UVectorE15UTransDirectionaR10UErrorCode.part.0
	jmp	.L80
.L90:
	call	__stack_chk_fail@PLT
.L91:
	movl	$7, (%rbx)
	jmp	.L80
	.cfi_endproc
.LFE2386:
	.size	_ZN6icu_6722CompoundTransliteratorC2ERNS_7UVectorER11UParseErrorR10UErrorCode, .-_ZN6icu_6722CompoundTransliteratorC2ERNS_7UVectorER11UParseErrorR10UErrorCode
	.globl	_ZN6icu_6722CompoundTransliteratorC1ERNS_7UVectorER11UParseErrorR10UErrorCode
	.set	_ZN6icu_6722CompoundTransliteratorC1ERNS_7UVectorER11UParseErrorR10UErrorCode,_ZN6icu_6722CompoundTransliteratorC2ERNS_7UVectorER11UParseErrorR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722CompoundTransliteratorC2ERNS_7UVectorEiR11UParseErrorR10UErrorCode
	.type	_ZN6icu_6722CompoundTransliteratorC2ERNS_7UVectorEiR11UParseErrorR10UErrorCode, @function
_ZN6icu_6722CompoundTransliteratorC2ERNS_7UVectorEiR11UParseErrorR10UErrorCode:
.LFB2389:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-128(%rbp), %r15
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	movl	$2, %edx
	movq	%r15, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%dx, -120(%rbp)
	xorl	%edx, %edx
	movq	%rax, -128(%rbp)
	call	_ZN6icu_6714TransliteratorC2ERKNS_13UnicodeStringEPNS_13UnicodeFilterE@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	(%rbx), %ecx
	movl	%r13d, 100(%r12)
	leaq	16+_ZTVN6icu_6722CompoundTransliteratorE(%rip), %rax
	movq	%rax, (%r12)
	movq	$0, 88(%r12)
	testl	%ecx, %ecx
	jle	.L101
.L92:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L102
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L101:
	.cfi_restore_state
	movslq	8(%r14), %rdi
	movl	%edi, 96(%r12)
	salq	$3, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, 88(%r12)
	testq	%rax, %rax
	je	.L103
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L92
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6722CompoundTransliterator4initERNS_7UVectorE15UTransDirectionaR10UErrorCode.part.0
	jmp	.L92
.L102:
	call	__stack_chk_fail@PLT
.L103:
	movl	$7, (%rbx)
	jmp	.L92
	.cfi_endproc
.LFE2389:
	.size	_ZN6icu_6722CompoundTransliteratorC2ERNS_7UVectorEiR11UParseErrorR10UErrorCode, .-_ZN6icu_6722CompoundTransliteratorC2ERNS_7UVectorEiR11UParseErrorR10UErrorCode
	.globl	_ZN6icu_6722CompoundTransliteratorC1ERNS_7UVectorEiR11UParseErrorR10UErrorCode
	.set	_ZN6icu_6722CompoundTransliteratorC1ERNS_7UVectorEiR11UParseErrorR10UErrorCode,_ZN6icu_6722CompoundTransliteratorC2ERNS_7UVectorEiR11UParseErrorR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722CompoundTransliterator4initERNS_7UVectorE15UTransDirectionaR10UErrorCode
	.type	_ZN6icu_6722CompoundTransliterator4initERNS_7UVectorE15UTransDirectionaR10UErrorCode, @function
_ZN6icu_6722CompoundTransliterator4initERNS_7UVectorE15UTransDirectionaR10UErrorCode:
.LFB2392:
	.cfi_startproc
	endbr64
	movl	(%r8), %r9d
	testl	%r9d, %r9d
	jle	.L115
	ret
	.p2align 4,,10
	.p2align 3
.L115:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%ecx, %ebx
	subq	$24, %rsp
	movslq	8(%rsi), %rdi
	movl	%edx, -44(%rbp)
	movq	%rsi, -40(%rbp)
	movl	%edi, 96(%r13)
	salq	$3, %rdi
	call	uprv_malloc_67@PLT
	movq	-40(%rbp), %rsi
	movl	-44(%rbp), %edx
	testq	%rax, %rax
	movq	%rax, 88(%r13)
	je	.L116
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L104
	addq	$24, %rsp
	movsbl	%bl, %ecx
	movq	%r13, %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6722CompoundTransliterator4initERNS_7UVectorE15UTransDirectionaR10UErrorCode.part.0
.L116:
	.cfi_restore_state
	movl	$7, (%r12)
.L104:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2392:
	.size	_ZN6icu_6722CompoundTransliterator4initERNS_7UVectorE15UTransDirectionaR10UErrorCode, .-_ZN6icu_6722CompoundTransliterator4initERNS_7UVectorE15UTransDirectionaR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722CompoundTransliterator7joinIDsEPKPNS_14TransliteratorEi
	.type	_ZN6icu_6722CompoundTransliterator7joinIDsEPKPNS_14TransliteratorEi, @function
_ZN6icu_6722CompoundTransliterator7joinIDsEPKPNS_14TransliteratorEi:
.LFB2393:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%si, 8(%rdi)
	movq	%rax, (%rdi)
	testl	%ecx, %ecx
	jle	.L117
	leal	-1(%rcx), %eax
	movq	%rdx, %rbx
	leaq	-42(%rbp), %r14
	leaq	8(%rdx,%rax,8), %r13
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L126:
	sarl	$5, %ecx
.L125:
	xorl	%edx, %edx
	movq	%r12, %rdi
	addq	$8, %rbx
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	cmpq	%r13, %rbx
	je	.L117
	movl	$59, %eax
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movw	%ax, -42(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
.L119:
	movq	(%rbx), %rdi
	movq	(%rdi), %rax
	call	*104(%rax)
	movswl	8(%rax), %ecx
	movq	%rax, %rsi
	testw	%cx, %cx
	jns	.L126
	movl	12(%rax), %ecx
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L117:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L127
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L127:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2393:
	.size	_ZN6icu_6722CompoundTransliterator7joinIDsEPKPNS_14TransliteratorEi, .-_ZN6icu_6722CompoundTransliterator7joinIDsEPKPNS_14TransliteratorEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722CompoundTransliterator19freeTransliteratorsEv
	.type	_ZN6icu_6722CompoundTransliterator19freeTransliteratorsEv, @function
_ZN6icu_6722CompoundTransliterator19freeTransliteratorsEv:
.LFB2401:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	88(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L129
	movl	96(%r13), %eax
	testl	%eax, %eax
	jle	.L130
	xorl	%ebx, %ebx
	leaq	_ZN6icu_6722CompoundTransliteratorD0Ev(%rip), %r14
	leaq	16+_ZTVN6icu_6722CompoundTransliteratorE(%rip), %r15
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L142:
	movq	%r15, (%r12)
	movq	%r12, %rdi
	call	_ZN6icu_6722CompoundTransliterator19freeTransliteratorsEv
	movq	%r12, %rdi
	call	_ZN6icu_6714TransliteratorD2Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	movl	96(%r13), %eax
	movq	88(%r13), %rdi
.L131:
	addq	$1, %rbx
	cmpl	%ebx, %eax
	jle	.L130
.L133:
	movq	(%rdi,%rbx,8), %r12
	testq	%r12, %r12
	je	.L131
	movq	(%r12), %rax
	movq	8(%rax), %rax
	cmpq	%r14, %rax
	je	.L142
	movq	%r12, %rdi
	addq	$1, %rbx
	call	*%rax
	movl	96(%r13), %eax
	movq	88(%r13), %rdi
	cmpl	%ebx, %eax
	jg	.L133
	.p2align 4,,10
	.p2align 3
.L130:
	call	uprv_free_67@PLT
.L129:
	movq	$0, 88(%r13)
	movl	$0, 96(%r13)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2401:
	.size	_ZN6icu_6722CompoundTransliterator19freeTransliteratorsEv, .-_ZN6icu_6722CompoundTransliterator19freeTransliteratorsEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722CompoundTransliteratorD2Ev
	.type	_ZN6icu_6722CompoundTransliteratorD2Ev, @function
_ZN6icu_6722CompoundTransliteratorD2Ev:
.LFB2398:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	16+_ZTVN6icu_6722CompoundTransliteratorE(%rip), %rbx
	subq	$8, %rsp
	movq	%rbx, (%rdi)
	movq	88(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L144
	movl	96(%r12), %eax
	testl	%eax, %eax
	jle	.L145
	xorl	%r15d, %r15d
	leaq	_ZN6icu_6722CompoundTransliteratorD0Ev(%rip), %r14
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L157:
	movq	%rbx, 0(%r13)
	movq	%r13, %rdi
	call	_ZN6icu_6722CompoundTransliterator19freeTransliteratorsEv
	movq	%r13, %rdi
	call	_ZN6icu_6714TransliteratorD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	movl	96(%r12), %eax
	movq	88(%r12), %rdi
.L146:
	addq	$1, %r15
	cmpl	%r15d, %eax
	jle	.L145
.L148:
	movq	(%rdi,%r15,8), %r13
	testq	%r13, %r13
	je	.L146
	movq	0(%r13), %rax
	movq	8(%rax), %rax
	cmpq	%r14, %rax
	je	.L157
	movq	%r13, %rdi
	addq	$1, %r15
	call	*%rax
	movl	96(%r12), %eax
	movq	88(%r12), %rdi
	cmpl	%r15d, %eax
	jg	.L148
	.p2align 4,,10
	.p2align 3
.L145:
	call	uprv_free_67@PLT
.L144:
	movq	$0, 88(%r12)
	movq	%r12, %rdi
	movl	$0, 96(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6714TransliteratorD2Ev@PLT
	.cfi_endproc
.LFE2398:
	.size	_ZN6icu_6722CompoundTransliteratorD2Ev, .-_ZN6icu_6722CompoundTransliteratorD2Ev
	.globl	_ZN6icu_6722CompoundTransliteratorD1Ev
	.set	_ZN6icu_6722CompoundTransliteratorD1Ev,_ZN6icu_6722CompoundTransliteratorD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722CompoundTransliteratorD0Ev
	.type	_ZN6icu_6722CompoundTransliteratorD0Ev, @function
_ZN6icu_6722CompoundTransliteratorD0Ev:
.LFB2400:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	16+_ZTVN6icu_6722CompoundTransliteratorE(%rip), %rbx
	subq	$8, %rsp
	movq	%rbx, (%rdi)
	movq	88(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L159
	movl	96(%r12), %eax
	testl	%eax, %eax
	jle	.L160
	xorl	%r15d, %r15d
	leaq	_ZN6icu_6722CompoundTransliteratorD0Ev(%rip), %r14
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L172:
	movq	%rbx, 0(%r13)
	movq	%r13, %rdi
	call	_ZN6icu_6722CompoundTransliterator19freeTransliteratorsEv
	movq	%r13, %rdi
	call	_ZN6icu_6714TransliteratorD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	movl	96(%r12), %eax
	movq	88(%r12), %rdi
.L161:
	addq	$1, %r15
	cmpl	%r15d, %eax
	jle	.L160
.L163:
	movq	(%rdi,%r15,8), %r13
	testq	%r13, %r13
	je	.L161
	movq	0(%r13), %rax
	movq	8(%rax), %rax
	cmpq	%r14, %rax
	je	.L172
	movq	%r13, %rdi
	addq	$1, %r15
	call	*%rax
	movl	96(%r12), %eax
	movq	88(%r12), %rdi
	cmpl	%r15d, %eax
	jg	.L163
	.p2align 4,,10
	.p2align 3
.L160:
	call	uprv_free_67@PLT
.L159:
	movq	$0, 88(%r12)
	movq	%r12, %rdi
	movl	$0, 96(%r12)
	call	_ZN6icu_6714TransliteratorD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2400:
	.size	_ZN6icu_6722CompoundTransliteratorD0Ev, .-_ZN6icu_6722CompoundTransliteratorD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722CompoundTransliteratoraSERKS0_
	.type	_ZN6icu_6722CompoundTransliteratoraSERKS0_, @function
_ZN6icu_6722CompoundTransliteratoraSERKS0_:
.LFB2402:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -56(%rbp)
	call	_ZN6icu_6714TransliteratoraSERKS0_@PLT
	movq	88(%r12), %rdi
	testq	%rdi, %rdi
	je	.L174
	movl	96(%r12), %edx
	testl	%edx, %edx
	jle	.L175
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L183:
	leaq	0(,%rbx,8), %rax
	movq	%rax, -64(%rbp)
	addq	%rdi, %rax
	movq	(%rax), %r15
	testq	%r15, %r15
	je	.L176
	movq	(%r15), %rax
	leaq	_ZN6icu_6722CompoundTransliteratorD0Ev(%rip), %rcx
	movq	8(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L177
	movq	88(%r15), %r9
	leaq	16+_ZTVN6icu_6722CompoundTransliteratorE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r9, %r9
	je	.L178
	movl	96(%r15), %edx
	testl	%edx, %edx
	jle	.L179
	xorl	%r13d, %r13d
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L213:
	leaq	16+_ZTVN6icu_6722CompoundTransliteratorE(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, (%r14)
	call	_ZN6icu_6722CompoundTransliterator19freeTransliteratorsEv
	movq	%r14, %rdi
	call	_ZN6icu_6714TransliteratorD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	movl	96(%r15), %edx
	movq	88(%r15), %r9
.L180:
	addq	$1, %r13
	cmpl	%r13d, %edx
	jle	.L179
.L182:
	movq	(%r9,%r13,8), %r14
	testq	%r14, %r14
	je	.L180
	movq	(%r14), %rdx
	leaq	_ZN6icu_6722CompoundTransliteratorD0Ev(%rip), %rax
	movq	8(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L213
	movq	%r14, %rdi
	addq	$1, %r13
	call	*%rdx
	movl	96(%r15), %edx
	movq	88(%r15), %r9
	cmpl	%r13d, %edx
	jg	.L182
	.p2align 4,,10
	.p2align 3
.L179:
	movq	%r9, %rdi
	call	uprv_free_67@PLT
.L178:
	movq	$0, 88(%r15)
	movq	%r15, %rdi
	movl	$0, 96(%r15)
	call	_ZN6icu_6714TransliteratorD2Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	movq	88(%r12), %rdi
	movq	-64(%rbp), %rax
	movl	96(%r12), %edx
	addq	%rdi, %rax
.L176:
	addq	$1, %rbx
	movq	$0, (%rax)
	cmpl	%ebx, %edx
	jg	.L183
.L215:
	movq	-56(%rbp), %rax
	movl	96(%rax), %eax
	cmpl	%edx, %eax
	jle	.L184
	testq	%rdi, %rdi
	je	.L185
.L191:
	call	uprv_free_67@PLT
	movq	-56(%rbp), %rax
	movl	96(%rax), %eax
.L185:
	movslq	%eax, %rdi
	salq	$3, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, 88(%r12)
	movq	%rax, %rdi
	movq	-56(%rbp), %rax
	movl	96(%rax), %eax
.L184:
	movl	%eax, 96(%r12)
	testq	%rdi, %rdi
	je	.L187
	testl	%eax, %eax
	jle	.L187
	xorl	%ebx, %ebx
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L214:
	addq	$1, %rbx
	cmpl	%ebx, 96(%r12)
	jle	.L187
.L189:
	movq	-56(%rbp), %rax
	leaq	(%rdi,%rbx,8), %r15
	movslq	%ebx, %r13
	movq	88(%rax), %rax
	movq	(%rax,%rbx,8), %r8
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*24(%rax)
	movq	88(%r12), %rdi
	movq	%rax, (%r15)
	cmpq	$0, (%rdi,%rbx,8)
	jne	.L214
	testl	%r13d, %r13d
	je	.L187
	leal	-1(%r13), %r15d
	leal	-1(%rbx), %eax
	movslq	%r15d, %r15
	subq	%rax, %r13
	salq	$3, %r15
	leaq	-16(,%r13,8), %rbx
	.p2align 4,,10
	.p2align 3
.L190:
	movq	(%rdi,%r15), %rdi
	call	uprv_free_67@PLT
	movq	88(%r12), %rdi
	movq	$0, (%rdi,%r15)
	subq	$8, %r15
	cmpq	%r15, %rbx
	jne	.L190
.L187:
	movq	-56(%rbp), %rax
	movl	100(%rax), %eax
	movl	%eax, 100(%r12)
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L177:
	.cfi_restore_state
	movq	%r15, %rdi
	addq	$1, %rbx
	call	*%rax
	movq	88(%r12), %rdi
	movq	-64(%rbp), %rax
	movl	96(%r12), %edx
	addq	%rdi, %rax
	movq	$0, (%rax)
	cmpl	%ebx, %edx
	jg	.L183
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L174:
	movq	-56(%rbp), %rax
	movl	96(%rax), %eax
	cmpl	%eax, 96(%r12)
	jl	.L185
.L211:
	movl	%eax, 96(%r12)
	jmp	.L187
	.p2align 4,,10
	.p2align 3
.L175:
	movq	-56(%rbp), %rax
	movl	96(%rax), %eax
	cmpl	%edx, %eax
	jg	.L191
	jmp	.L211
	.cfi_endproc
.LFE2402:
	.size	_ZN6icu_6722CompoundTransliteratoraSERKS0_, .-_ZN6icu_6722CompoundTransliteratoraSERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722CompoundTransliteratorC2ERKS0_
	.type	_ZN6icu_6722CompoundTransliteratorC2ERKS0_, @function
_ZN6icu_6722CompoundTransliteratorC2ERKS0_:
.LFB2395:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN6icu_6714TransliteratorC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6722CompoundTransliteratorE(%rip), %rax
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, (%r12)
	movabsq	$-4294967296, %rax
	movq	$0, 88(%r12)
	movq	%rax, 96(%r12)
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6722CompoundTransliteratoraSERKS0_
	.cfi_endproc
.LFE2395:
	.size	_ZN6icu_6722CompoundTransliteratorC2ERKS0_, .-_ZN6icu_6722CompoundTransliteratorC2ERKS0_
	.globl	_ZN6icu_6722CompoundTransliteratorC1ERKS0_
	.set	_ZN6icu_6722CompoundTransliteratorC1ERKS0_,_ZN6icu_6722CompoundTransliteratorC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722CompoundTransliterator5cloneEv
	.type	_ZNK6icu_6722CompoundTransliterator5cloneEv, @function
_ZNK6icu_6722CompoundTransliterator5cloneEv:
.LFB2403:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movl	$104, %edi
	pushq	%r12
	.cfi_offset 12, -32
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L218
	movq	%rax, %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6714TransliteratorC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6722CompoundTransliteratorE(%rip), %rax
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, (%r12)
	movabsq	$-4294967296, %rax
	movq	$0, 88(%r12)
	movq	%rax, 96(%r12)
	call	_ZN6icu_6722CompoundTransliteratoraSERKS0_
.L218:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2403:
	.size	_ZNK6icu_6722CompoundTransliterator5cloneEv, .-_ZNK6icu_6722CompoundTransliterator5cloneEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722CompoundTransliterator20adoptTransliteratorsEPPNS_14TransliteratorEi
	.type	_ZN6icu_6722CompoundTransliterator20adoptTransliteratorsEPPNS_14TransliteratorEi, @function
_ZN6icu_6722CompoundTransliterator20adoptTransliteratorsEPPNS_14TransliteratorEi:
.LFB2407:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	88(%rdi), %r8
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r8, %r8
	je	.L225
	movl	96(%rdi), %edx
	testl	%edx, %edx
	jle	.L226
	xorl	%r15d, %r15d
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L263:
	leaq	16+_ZTVN6icu_6722CompoundTransliteratorE(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, (%r14)
	call	_ZN6icu_6722CompoundTransliterator19freeTransliteratorsEv
	movq	%r14, %rdi
	call	_ZN6icu_6714TransliteratorD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	movl	96(%rbx), %edx
	movq	88(%rbx), %r8
.L227:
	addq	$1, %r15
	cmpl	%r15d, %edx
	jle	.L226
.L229:
	movq	(%r8,%r15,8), %r14
	testq	%r14, %r14
	je	.L227
	movq	(%r14), %rdx
	leaq	_ZN6icu_6722CompoundTransliteratorD0Ev(%rip), %rax
	movq	8(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L263
	movq	%r14, %rdi
	addq	$1, %r15
	call	*%rdx
	movl	96(%rbx), %edx
	movq	88(%rbx), %r8
	cmpl	%r15d, %edx
	jg	.L229
	.p2align 4,,10
	.p2align 3
.L226:
	movq	%r8, %rdi
	call	uprv_free_67@PLT
.L225:
	movq	%r12, 88(%rbx)
	movl	%r13d, 96(%rbx)
	testl	%r13d, %r13d
	jle	.L245
	leal	-1(%r13), %eax
	movq	%r12, %rsi
	xorl	%r8d, %r8d
	leaq	8(%r12,%rax,8), %rdx
	.p2align 4,,10
	.p2align 3
.L231:
	movq	(%rsi), %rax
	movl	80(%rax), %eax
	cmpl	%eax, %r8d
	cmovl	%eax, %r8d
	addq	$8, %rsi
	cmpq	%rsi, %rdx
	jne	.L231
.L230:
	movl	%r8d, %esi
	movq	%rbx, %rdi
	leaq	-128(%rbp), %r15
	call	_ZN6icu_6714Transliterator23setMaximumContextLengthEi@PLT
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rcx
	movl	96(%rbx), %eax
	movq	88(%rbx), %r12
	movq	%rcx, -128(%rbp)
	movl	$2, %ecx
	leaq	-130(%rbp), %r13
	movw	%cx, -120(%rbp)
	testl	%eax, %eax
	jle	.L232
	subl	$1, %eax
	leaq	-128(%rbp), %r15
	leaq	-130(%rbp), %r13
	leaq	8(%r12,%rax,8), %r14
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L264:
	sarl	$5, %ecx
	xorl	%edx, %edx
	movq	%r15, %rdi
	addq	$8, %r12
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	cmpq	%r14, %r12
	je	.L232
.L237:
	movl	$59, %eax
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movw	%ax, -130(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
.L233:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	call	*104(%rax)
	movswl	8(%rax), %ecx
	movq	%rax, %rsi
	testw	%cx, %cx
	jns	.L264
	movl	12(%rax), %ecx
	xorl	%edx, %edx
	movq	%r15, %rdi
	addq	$8, %r12
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	cmpq	%r12, %r14
	jne	.L237
.L232:
	leaq	8(%rbx), %r12
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	xorl	%edx, %edx
	movl	$1, %ecx
	movq	%r13, %rsi
	movw	%dx, -130(%rbp)
	movq	%r12, %rdi
	xorl	%edx, %edx
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movzwl	16(%rbx), %eax
	testw	%ax, %ax
	js	.L265
	movswl	%ax, %edx
	sarl	$5, %edx
	leal	-1(%rdx), %ecx
	testb	$1, %al
	je	.L266
	testl	%ecx, %ecx
	je	.L267
.L240:
	cmpl	%ecx, %edx
	jbe	.L241
	cmpl	$1023, %ecx
	jle	.L243
	orl	$-32, %eax
	movl	%ecx, 20(%rbx)
	movw	%ax, 16(%rbx)
.L241:
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L268
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L266:
	.cfi_restore_state
	cmpl	%edx, %ecx
	jnb	.L241
.L243:
	andl	$31, %eax
	sall	$5, %ecx
	orl	%ecx, %eax
	movw	%ax, 16(%rbx)
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L265:
	movl	20(%rbx), %edx
	leal	-1(%rdx), %ecx
	testb	$1, %al
	je	.L240
	testl	%ecx, %ecx
	jne	.L240
.L267:
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L245:
	xorl	%r8d, %r8d
	jmp	.L230
.L268:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2407:
	.size	_ZN6icu_6722CompoundTransliterator20adoptTransliteratorsEPPNS_14TransliteratorEi, .-_ZN6icu_6722CompoundTransliterator20adoptTransliteratorsEPPNS_14TransliteratorEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722CompoundTransliterator18setTransliteratorsEPKPNS_14TransliteratorEi
	.type	_ZN6icu_6722CompoundTransliterator18setTransliteratorsEPKPNS_14TransliteratorEi, @function
_ZN6icu_6722CompoundTransliterator18setTransliteratorsEPKPNS_14TransliteratorEi:
.LFB2406:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -56(%rbp)
	movslq	%edx, %rdi
	movl	%edi, -60(%rbp)
	movq	%rdi, %rbx
	salq	$3, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L269
	movq	%rax, %r12
	testl	%ebx, %ebx
	jle	.L272
	leal	-1(%rbx), %r14d
	xorl	%r15d, %r15d
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L281:
	leaq	1(%r15), %rax
	cmpq	%r15, %r14
	je	.L272
	movq	%rax, %r15
.L274:
	movq	0(%r13,%r15,8), %rdi
	movl	%r15d, %ebx
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	%rax, (%r12,%r15,8)
	testq	%rax, %rax
	jne	.L281
	testl	%r15d, %r15d
	je	.L272
	subl	$1, %ebx
	movslq	%ebx, %rbx
	.p2align 4,,10
	.p2align 3
.L276:
	movq	(%r12,%rbx,8), %rdi
	call	uprv_free_67@PLT
	movq	$0, (%r12,%rbx,8)
	subq	$1, %rbx
	cmpl	$-1, %ebx
	jne	.L276
.L269:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L272:
	.cfi_restore_state
	movl	-60(%rbp), %edx
	movq	-56(%rbp), %rdi
	addq	$24, %rsp
	movq	%r12, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6722CompoundTransliterator20adoptTransliteratorsEPPNS_14TransliteratorEi
	.cfi_endproc
.LFE2406:
	.size	_ZN6icu_6722CompoundTransliterator18setTransliteratorsEPKPNS_14TransliteratorEi, .-_ZN6icu_6722CompoundTransliterator18setTransliteratorsEPKPNS_14TransliteratorEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722CompoundTransliteratorC2EPKPNS_14TransliteratorEiPNS_13UnicodeFilterE
	.type	_ZN6icu_6722CompoundTransliteratorC2EPKPNS_14TransliteratorEiPNS_13UnicodeFilterE, @function
_ZN6icu_6722CompoundTransliteratorC2EPKPNS_14TransliteratorEiPNS_13UnicodeFilterE:
.LFB2374:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-128(%rbp), %r15
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%rcx, -160(%rbp)
	movl	$2, %ecx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%cx, -120(%rbp)
	movq	%rax, -128(%rbp)
	testl	%edx, %edx
	jle	.L283
	leal	-1(%rdx), %eax
	movq	%rsi, %rbx
	leaq	-128(%rbp), %r15
	leaq	8(%rsi,%rax,8), %rax
	movq	%rax, -152(%rbp)
	leaq	-130(%rbp), %rax
	movq	%rax, -168(%rbp)
	jmp	.L284
	.p2align 4,,10
	.p2align 3
.L294:
	sarl	$5, %ecx
.L293:
	xorl	%edx, %edx
	movq	%r15, %rdi
	addq	$8, %rbx
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	cmpq	-152(%rbp), %rbx
	je	.L283
	movl	$59, %eax
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	-168(%rbp), %rsi
	movw	%ax, -130(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
.L284:
	movq	(%rbx), %rdi
	movq	(%rdi), %rax
	call	*104(%rax)
	movswl	8(%rax), %ecx
	movq	%rax, %rsi
	testw	%cx, %cx
	jns	.L294
	movl	12(%rax), %ecx
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L283:
	movq	-160(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6714TransliteratorC2ERKNS_13UnicodeStringEPNS_13UnicodeFilterE@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	%r13d, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	$0, 88(%r12)
	leaq	16+_ZTVN6icu_6722CompoundTransliteratorE(%rip), %rax
	movq	%rax, (%r12)
	movq	$0, 96(%r12)
	call	_ZN6icu_6722CompoundTransliterator18setTransliteratorsEPKPNS_14TransliteratorEi
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L295
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L295:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2374:
	.size	_ZN6icu_6722CompoundTransliteratorC2EPKPNS_14TransliteratorEiPNS_13UnicodeFilterE, .-_ZN6icu_6722CompoundTransliteratorC2EPKPNS_14TransliteratorEiPNS_13UnicodeFilterE
	.globl	_ZN6icu_6722CompoundTransliteratorC1EPKPNS_14TransliteratorEiPNS_13UnicodeFilterE
	.set	_ZN6icu_6722CompoundTransliteratorC1EPKPNS_14TransliteratorEiPNS_13UnicodeFilterE,_ZN6icu_6722CompoundTransliteratorC2EPKPNS_14TransliteratorEiPNS_13UnicodeFilterE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722CompoundTransliterator27computeMaximumContextLengthEv
	.type	_ZN6icu_6722CompoundTransliterator27computeMaximumContextLengthEv, @function
_ZN6icu_6722CompoundTransliterator27computeMaximumContextLengthEv:
.LFB2413:
	.cfi_startproc
	endbr64
	movl	96(%rdi), %eax
	testl	%eax, %eax
	jle	.L299
	movq	88(%rdi), %rdx
	leal	-1(%rax), %ecx
	xorl	%esi, %esi
	leaq	8(%rdx), %rax
	leaq	(%rax,%rcx,8), %rcx
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L301:
	addq	$8, %rax
.L298:
	movq	(%rdx), %rdx
	movl	80(%rdx), %edx
	cmpl	%edx, %esi
	cmovl	%edx, %esi
	movq	%rax, %rdx
	cmpq	%rax, %rcx
	jne	.L301
	jmp	_ZN6icu_6714Transliterator23setMaximumContextLengthEi@PLT
	.p2align 4,,10
	.p2align 3
.L299:
	xorl	%esi, %esi
	jmp	_ZN6icu_6714Transliterator23setMaximumContextLengthEi@PLT
	.cfi_endproc
.LFE2413:
	.size	_ZN6icu_6722CompoundTransliterator27computeMaximumContextLengthEv, .-_ZN6icu_6722CompoundTransliterator27computeMaximumContextLengthEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722CompoundTransliterator4initERKNS_13UnicodeStringE15UTransDirectionaR10UErrorCode
	.type	_ZN6icu_6722CompoundTransliterator4initERKNS_13UnicodeStringE15UTransDirectionaR10UErrorCode, @function
_ZN6icu_6722CompoundTransliterator4initERKNS_13UnicodeStringE15UTransDirectionaR10UErrorCode:
.LFB2391:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%ecx, -196(%rbp)
	movl	(%r8), %r9d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r9d, %r9d
	jle	.L322
.L302:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L323
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L322:
	.cfi_restore_state
	leaq	-176(%rbp), %r14
	movq	%rdi, %r13
	movl	%edx, %r12d
	movq	%r8, %rbx
	movq	%rsi, -208(%rbp)
	movq	%r14, %rdi
	movq	%r8, %rsi
	leaq	-128(%rbp), %r15
	call	_ZN6icu_677UVectorC1ER10UErrorCode@PLT
	movl	$2, %ecx
	movq	%r15, %rdx
	movl	%r12d, %esi
	movq	-208(%rbp), %r10
	movw	%cx, -120(%rbp)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	leaq	-184(%rbp), %r8
	movq	%r14, %rcx
	movq	%rax, -128(%rbp)
	movq	$0, -184(%rbp)
	movq	%r10, %rdi
	call	_ZN6icu_6722TransliteratorIDParser15parseCompoundIDERKNS_13UnicodeStringEiRS1_RNS_7UVectorERPNS_10UnicodeSetE@PLT
	testb	%al, %al
	jne	.L304
	movq	-184(%rbp), %rdi
	movl	$65569, (%rbx)
	testq	%rdi, %rdi
	je	.L309
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
.L309:
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UVectorD1Ev@PLT
	jmp	.L302
	.p2align 4,,10
	.p2align 3
.L304:
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6722TransliteratorIDParser15instantiateListERNS_7UVectorER10UErrorCode@PLT
	movl	(%rbx), %edx
	testl	%edx, %edx
	jle	.L324
.L307:
	movq	-184(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L309
	movq	%r13, %rdi
	call	_ZN6icu_6714Transliterator11adoptFilterEPNS_13UnicodeFilterE@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UVectorD1Ev@PLT
	jmp	.L302
	.p2align 4,,10
	.p2align 3
.L324:
	movslq	-168(%rbp), %rax
	movl	%eax, 96(%r13)
	leaq	0(,%rax,8), %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, 88(%r13)
	testq	%rax, %rax
	je	.L325
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L307
	movsbl	-196(%rbp), %ecx
	movl	%r12d, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6722CompoundTransliterator4initERNS_7UVectorE15UTransDirectionaR10UErrorCode.part.0
	jmp	.L307
.L323:
	call	__stack_chk_fail@PLT
.L325:
	movl	$7, (%rbx)
	jmp	.L307
	.cfi_endproc
.LFE2391:
	.size	_ZN6icu_6722CompoundTransliterator4initERKNS_13UnicodeStringE15UTransDirectionaR10UErrorCode, .-_ZN6icu_6722CompoundTransliterator4initERKNS_13UnicodeStringE15UTransDirectionaR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722CompoundTransliteratorC2ERKNS_13UnicodeStringE15UTransDirectionPNS_13UnicodeFilterER11UParseErrorR10UErrorCode
	.type	_ZN6icu_6722CompoundTransliteratorC2ERKNS_13UnicodeStringE15UTransDirectionPNS_13UnicodeFilterER11UParseErrorR10UErrorCode, @function
_ZN6icu_6722CompoundTransliteratorC2ERKNS_13UnicodeStringE15UTransDirectionPNS_13UnicodeFilterER11UParseErrorR10UErrorCode:
.LFB2377:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edx, %r13d
	movq	%rcx, %rdx
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$168, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6714TransliteratorC2ERKNS_13UnicodeStringEPNS_13UnicodeFilterE@PLT
	movl	(%rbx), %esi
	leaq	16+_ZTVN6icu_6722CompoundTransliteratorE(%rip), %rax
	movq	$0, 88(%r12)
	movq	%rax, (%r12)
	movl	$0, 100(%r12)
	testl	%esi, %esi
	jle	.L346
.L326:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L347
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L346:
	.cfi_restore_state
	leaq	-176(%rbp), %r14
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_677UVectorC1ER10UErrorCode@PLT
	movl	$2, %ecx
	movl	%r13d, %esi
	movq	%r15, %rdi
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%cx, -120(%rbp)
	leaq	-184(%rbp), %r8
	movq	%r14, %rcx
	movq	%rax, -128(%rbp)
	leaq	-128(%rbp), %rax
	movq	%rax, %rdx
	movq	%rax, -200(%rbp)
	movq	$0, -184(%rbp)
	call	_ZN6icu_6722TransliteratorIDParser15parseCompoundIDERKNS_13UnicodeStringEiRS1_RNS_7UVectorERPNS_10UnicodeSetE@PLT
	testb	%al, %al
	jne	.L328
	movq	-184(%rbp), %rdi
	movl	$65569, (%rbx)
	testq	%rdi, %rdi
	je	.L333
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
.L333:
	movq	-200(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UVectorD1Ev@PLT
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L328:
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6722TransliteratorIDParser15instantiateListERNS_7UVectorER10UErrorCode@PLT
	movl	(%rbx), %edx
	testl	%edx, %edx
	jle	.L348
.L331:
	movq	-184(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L333
	movq	%r12, %rdi
	call	_ZN6icu_6714Transliterator11adoptFilterEPNS_13UnicodeFilterE@PLT
	jmp	.L333
	.p2align 4,,10
	.p2align 3
.L348:
	movslq	-168(%rbp), %rax
	movl	%eax, 96(%r12)
	leaq	0(,%rax,8), %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, 88(%r12)
	testq	%rax, %rax
	je	.L349
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L331
	movl	$1, %ecx
	movl	%r13d, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6722CompoundTransliterator4initERNS_7UVectorE15UTransDirectionaR10UErrorCode.part.0
	jmp	.L331
.L347:
	call	__stack_chk_fail@PLT
.L349:
	movl	$7, (%rbx)
	jmp	.L331
	.cfi_endproc
.LFE2377:
	.size	_ZN6icu_6722CompoundTransliteratorC2ERKNS_13UnicodeStringE15UTransDirectionPNS_13UnicodeFilterER11UParseErrorR10UErrorCode, .-_ZN6icu_6722CompoundTransliteratorC2ERKNS_13UnicodeStringE15UTransDirectionPNS_13UnicodeFilterER11UParseErrorR10UErrorCode
	.globl	_ZN6icu_6722CompoundTransliteratorC1ERKNS_13UnicodeStringE15UTransDirectionPNS_13UnicodeFilterER11UParseErrorR10UErrorCode
	.set	_ZN6icu_6722CompoundTransliteratorC1ERKNS_13UnicodeStringE15UTransDirectionPNS_13UnicodeFilterER11UParseErrorR10UErrorCode,_ZN6icu_6722CompoundTransliteratorC2ERKNS_13UnicodeStringE15UTransDirectionPNS_13UnicodeFilterER11UParseErrorR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722CompoundTransliteratorC2ERKNS_13UnicodeStringER11UParseErrorR10UErrorCode
	.type	_ZN6icu_6722CompoundTransliteratorC2ERKNS_13UnicodeStringER11UParseErrorR10UErrorCode, @function
_ZN6icu_6722CompoundTransliteratorC2ERKNS_13UnicodeStringER11UParseErrorR10UErrorCode:
.LFB2380:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$152, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6714TransliteratorC2ERKNS_13UnicodeStringEPNS_13UnicodeFilterE@PLT
	movl	(%rbx), %esi
	leaq	16+_ZTVN6icu_6722CompoundTransliteratorE(%rip), %rax
	movq	$0, 88(%r12)
	movq	%rax, (%r12)
	movl	$0, 100(%r12)
	testl	%esi, %esi
	jle	.L370
.L350:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L371
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L370:
	.cfi_restore_state
	leaq	-176(%rbp), %r14
	movq	%rbx, %rsi
	leaq	-128(%rbp), %r15
	movq	%r14, %rdi
	call	_ZN6icu_677UVectorC1ER10UErrorCode@PLT
	movl	$2, %ecx
	xorl	%esi, %esi
	movq	%r15, %rdx
	movw	%cx, -120(%rbp)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%r14, %rcx
	movq	%r13, %rdi
	leaq	-184(%rbp), %r8
	movq	%rax, -128(%rbp)
	movq	$0, -184(%rbp)
	call	_ZN6icu_6722TransliteratorIDParser15parseCompoundIDERKNS_13UnicodeStringEiRS1_RNS_7UVectorERPNS_10UnicodeSetE@PLT
	testb	%al, %al
	jne	.L352
	movq	-184(%rbp), %rdi
	movl	$65569, (%rbx)
	testq	%rdi, %rdi
	je	.L357
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
.L357:
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UVectorD1Ev@PLT
	jmp	.L350
	.p2align 4,,10
	.p2align 3
.L352:
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6722TransliteratorIDParser15instantiateListERNS_7UVectorER10UErrorCode@PLT
	movl	(%rbx), %edx
	testl	%edx, %edx
	jle	.L372
.L355:
	movq	-184(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L357
	movq	%r12, %rdi
	call	_ZN6icu_6714Transliterator11adoptFilterEPNS_13UnicodeFilterE@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UVectorD1Ev@PLT
	jmp	.L350
	.p2align 4,,10
	.p2align 3
.L372:
	movslq	-168(%rbp), %rax
	movl	%eax, 96(%r12)
	leaq	0(,%rax,8), %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, 88(%r12)
	testq	%rax, %rax
	je	.L373
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L355
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6722CompoundTransliterator4initERNS_7UVectorE15UTransDirectionaR10UErrorCode.part.0
	jmp	.L355
.L371:
	call	__stack_chk_fail@PLT
.L373:
	movl	$7, (%rbx)
	jmp	.L355
	.cfi_endproc
.LFE2380:
	.size	_ZN6icu_6722CompoundTransliteratorC2ERKNS_13UnicodeStringER11UParseErrorR10UErrorCode, .-_ZN6icu_6722CompoundTransliteratorC2ERKNS_13UnicodeStringER11UParseErrorR10UErrorCode
	.globl	_ZN6icu_6722CompoundTransliteratorC1ERKNS_13UnicodeStringER11UParseErrorR10UErrorCode
	.set	_ZN6icu_6722CompoundTransliteratorC1ERKNS_13UnicodeStringER11UParseErrorR10UErrorCode,_ZN6icu_6722CompoundTransliteratorC2ERKNS_13UnicodeStringER11UParseErrorR10UErrorCode
	.section	.rodata.str2.2,"aMS",@progbits,2
	.align 2
.LC0:
	.string	":"
	.string	":"
	.string	"N"
	.string	"u"
	.string	"l"
	.string	"l"
	.string	";"
	.string	""
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722CompoundTransliterator7toRulesERNS_13UnicodeStringEa
	.type	_ZNK6icu_6722CompoundTransliterator7toRulesERNS_13UnicodeStringEa, @function
_ZNK6icu_6722CompoundTransliterator7toRulesERNS_13UnicodeStringEa:
.LFB2409:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$248, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzwl	8(%rsi), %eax
	testb	$1, %al
	je	.L375
	movq	%rsi, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
.L376:
	movl	100(%r13), %r10d
	testl	%r10d, %r10d
	jg	.L425
.L380:
	movl	96(%r13), %esi
	testl	%esi, %esi
	jle	.L405
	movsbl	%bl, %eax
	leaq	-256(%rbp), %r14
	xorl	%ebx, %ebx
	movl	%eax, -276(%rbp)
	jmp	.L406
	.p2align 4,,10
	.p2align 3
.L426:
	movl	-276(%rbp), %edx
	movq	%r14, %rsi
	call	*112(%rax)
	cmpl	$1, 100(%r13)
	jle	.L386
	testq	%rbx, %rbx
	je	.L386
	movq	88(%r13), %rax
	movq	-8(%rax,%r15), %rdi
	movq	(%rdi), %rax
	call	*104(%rax)
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movl	$5, %r9d
	movq	%rax, %rdi
	leaq	_ZN6icu_6722CompoundTransliterator11PASS_STRINGE(%rip), %rcx
	movl	$5, %edx
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	testb	%al, %al
	jne	.L387
	leaq	_ZN6icu_6722CompoundTransliterator11PASS_STRINGE(%rip), %rsi
	leaq	-192(%rbp), %r15
	movl	$-1, %ecx
	movl	$1, %esi
	leaq	.LC0(%rip), %rax
	leaq	-264(%rbp), %rdx
	movq	%r15, %rdi
	movq	%rax, -264(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	leaq	-128(%rbp), %r8
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r8, %rdi
	movq	%r8, -288(%rbp)
	call	_ZN6icu_67plERKNS_13UnicodeStringES2_@PLT
	movq	-288(%rbp), %r8
	movq	%r14, %rdi
	movq	%r8, %rsi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	-288(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L386:
	movzwl	8(%r12), %edx
	testw	%dx, %dx
	js	.L391
.L427:
	movswl	%dx, %eax
	sarl	$5, %eax
.L392:
	testl	%eax, %eax
	je	.L393
	leal	-1(%rax), %ecx
	je	.L394
	andl	$2, %edx
	leaq	10(%r12), %rax
	jne	.L396
	movq	24(%r12), %rax
.L396:
	movslq	%ecx, %rcx
	cmpw	$10, (%rax,%rcx,2)
	jne	.L394
.L393:
	movswl	-248(%rbp), %ecx
	testw	%cx, %cx
	js	.L397
	sarl	$5, %ecx
.L398:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movzwl	8(%r12), %edx
	testw	%dx, %dx
	js	.L399
	movswl	%dx, %eax
	sarl	$5, %eax
.L400:
	testl	%eax, %eax
	je	.L401
	leal	-1(%rax), %ecx
	je	.L402
	andl	$2, %edx
	leaq	10(%r12), %rax
	jne	.L404
	movq	24(%r12), %rax
.L404:
	movslq	%ecx, %rcx
	cmpw	$59, (%rax,%rcx,2)
	jne	.L402
.L401:
	movq	%r14, %rdi
	addq	$1, %rbx
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpl	%ebx, 96(%r13)
	jle	.L405
.L406:
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %ecx
	leaq	0(,%rbx,8), %r15
	movq	%rax, -256(%rbp)
	movq	88(%r13), %rax
	movw	%cx, -248(%rbp)
	movq	(%rax,%rbx,8), %rdi
	movq	(%rdi), %rax
	call	*104(%rax)
	xorl	%esi, %esi
	movl	$5, %r9d
	xorl	%r8d, %r8d
	movq	%rax, %rdi
	leaq	_ZN6icu_6722CompoundTransliterator11PASS_STRINGE(%rip), %rcx
	movl	$5, %edx
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	leaq	_ZN6icu_6722CompoundTransliterator11PASS_STRINGE(%rip), %rsi
	testb	%al, %al
	movq	88(%r13), %rax
	movq	(%rax,%rbx,8), %rdi
	movq	(%rdi), %rax
	je	.L426
	call	*104(%rax)
	movswl	8(%rax), %ecx
	movq	%rax, %rdi
	testw	%cx, %cx
	js	.L388
	sarl	$5, %ecx
.L389:
	xorl	%edx, %edx
	movl	$59, %esi
	call	_ZNK6icu_6713UnicodeString9doIndexOfEDsii@PLT
	testl	%eax, %eax
	movq	88(%r13), %rax
	movq	(%rax,%r15), %rdi
	js	.L390
	movq	(%rdi), %rax
	movl	-276(%rbp), %edx
	movq	%r14, %rsi
	call	*112(%rax)
	movzwl	8(%r12), %edx
	testw	%dx, %dx
	jns	.L427
.L391:
	movl	12(%r12), %eax
	jmp	.L392
	.p2align 4,,10
	.p2align 3
.L399:
	movl	12(%r12), %eax
	jmp	.L400
	.p2align 4,,10
	.p2align 3
.L397:
	movl	-244(%rbp), %ecx
	jmp	.L398
	.p2align 4,,10
	.p2align 3
.L394:
	movl	$10, %edx
	leaq	-264(%rbp), %rsi
	movl	$1, %ecx
	movq	%r12, %rdi
	movw	%dx, -264(%rbp)
	xorl	%edx, %edx
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	jmp	.L393
	.p2align 4,,10
	.p2align 3
.L402:
	movl	$59, %eax
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	leaq	-264(%rbp), %rsi
	movw	%ax, -264(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	jmp	.L401
	.p2align 4,,10
	.p2align 3
.L388:
	movl	12(%rax), %ecx
	jmp	.L389
	.p2align 4,,10
	.p2align 3
.L387:
	leaq	_ZN6icu_6722CompoundTransliterator11PASS_STRINGE(%rip), %rax
	jmp	.L386
	.p2align 4,,10
	.p2align 3
.L390:
	movl	-276(%rbp), %edx
	movq	%r14, %rsi
	call	_ZNK6icu_6714Transliterator7toRulesERNS_13UnicodeStringEa@PLT
	jmp	.L386
	.p2align 4,,10
	.p2align 3
.L405:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L428
	addq	$248, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L375:
	.cfi_restore_state
	testw	%ax, %ax
	js	.L377
	movswl	%ax, %edx
	sarl	$5, %edx
.L378:
	testl	%edx, %edx
	je	.L376
	movl	100(%r13), %r10d
	andl	$31, %eax
	movw	%ax, 8(%r12)
	testl	%r10d, %r10d
	jle	.L380
.L425:
	movq	%r13, %rdi
	call	_ZNK6icu_6714Transliterator9getFilterEv@PLT
	testq	%rax, %rax
	je	.L380
	movl	$2, %r9d
	movl	$2, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	leaq	_ZL11COLON_COLON(%rip), %rsi
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%r9w, -120(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-128(%rbp), %r15
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZNK6icu_6714Transliterator9getFilterEv@PLT
	movsbl	%bl, %edx
	movq	%r15, %rsi
	leaq	8(%rax), %rdi
	movq	8(%rax), %rax
	call	*24(%rax)
	movzwl	8(%rax), %edx
	movq	%rax, %rsi
	testw	%dx, %dx
	js	.L382
	movswl	%dx, %ecx
	sarl	$5, %ecx
.L383:
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movl	$59, %r8d
	leaq	-264(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rax, %rdi
	movl	$1, %ecx
	movw	%r8w, -264(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	_ZL11COLON_COLON(%rip), %rax
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L380
	.p2align 4,,10
	.p2align 3
.L377:
	movl	12(%rsi), %edx
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L382:
	movl	12(%rax), %ecx
	jmp	.L383
.L428:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2409:
	.size	_ZNK6icu_6722CompoundTransliterator7toRulesERNS_13UnicodeStringEa, .-_ZNK6icu_6722CompoundTransliterator7toRulesERNS_13UnicodeStringEa
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722CompoundTransliterator18handleGetSourceSetERNS_10UnicodeSetE
	.type	_ZNK6icu_6722CompoundTransliterator18handleGetSourceSetERNS_10UnicodeSetE, @function
_ZNK6icu_6722CompoundTransliterator18handleGetSourceSetERNS_10UnicodeSetE:
.LFB2410:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-240(%rbp), %r14
	movq	%rdi, %r13
	pushq	%r12
	movq	%r14, %rdi
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$208, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet5clearEv@PLT
	movl	96(%r13), %eax
	testl	%eax, %eax
	jle	.L430
	xorl	%ebx, %ebx
	jmp	.L431
	.p2align 4,,10
	.p2align 3
.L438:
	addq	$1, %rbx
	cmpl	%ebx, 96(%r13)
	jle	.L430
.L431:
	movq	88(%r13), %rax
	movq	%r14, %rsi
	movq	(%rax,%rbx,8), %rdi
	call	_ZNK6icu_6714Transliterator12getSourceSetERNS_10UnicodeSetE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_6710UnicodeSet6addAllERKS0_@PLT
	movq	%r12, %rdi
	call	_ZNK6icu_6710UnicodeSet7isEmptyEv@PLT
	testb	%al, %al
	jne	.L438
.L430:
	movq	%r14, %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L439
	addq	$208, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L439:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2410:
	.size	_ZNK6icu_6722CompoundTransliterator18handleGetSourceSetERNS_10UnicodeSetE, .-_ZNK6icu_6722CompoundTransliterator18handleGetSourceSetERNS_10UnicodeSetE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722CompoundTransliterator12getTargetSetERNS_10UnicodeSetE
	.type	_ZNK6icu_6722CompoundTransliterator12getTargetSetERNS_10UnicodeSetE, @function
_ZNK6icu_6722CompoundTransliterator12getTargetSetERNS_10UnicodeSetE:
.LFB2411:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-240(%rbp), %r14
	movq	%rdi, %r13
	pushq	%r12
	movq	%r14, %rdi
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$208, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet5clearEv@PLT
	movl	96(%r13), %eax
	testl	%eax, %eax
	jle	.L441
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L442:
	movq	88(%r13), %rax
	movq	%r14, %rsi
	movq	(%rax,%rbx,8), %rdi
	addq	$1, %rbx
	movq	(%rdi), %rax
	call	*128(%rax)
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_6710UnicodeSet6addAllERKS0_@PLT
	cmpl	%ebx, 96(%r13)
	jg	.L442
.L441:
	movq	%r14, %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L446
	addq	$208, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L446:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2411:
	.size	_ZNK6icu_6722CompoundTransliterator12getTargetSetERNS_10UnicodeSetE, .-_ZNK6icu_6722CompoundTransliterator12getTargetSetERNS_10UnicodeSetE
	.weak	_ZTSN6icu_6722CompoundTransliteratorE
	.section	.rodata._ZTSN6icu_6722CompoundTransliteratorE,"aG",@progbits,_ZTSN6icu_6722CompoundTransliteratorE,comdat
	.align 32
	.type	_ZTSN6icu_6722CompoundTransliteratorE, @object
	.size	_ZTSN6icu_6722CompoundTransliteratorE, 34
_ZTSN6icu_6722CompoundTransliteratorE:
	.string	"N6icu_6722CompoundTransliteratorE"
	.weak	_ZTIN6icu_6722CompoundTransliteratorE
	.section	.data.rel.ro._ZTIN6icu_6722CompoundTransliteratorE,"awG",@progbits,_ZTIN6icu_6722CompoundTransliteratorE,comdat
	.align 8
	.type	_ZTIN6icu_6722CompoundTransliteratorE, @object
	.size	_ZTIN6icu_6722CompoundTransliteratorE, 24
_ZTIN6icu_6722CompoundTransliteratorE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6722CompoundTransliteratorE
	.quad	_ZTIN6icu_6714TransliteratorE
	.weak	_ZTVN6icu_6722CompoundTransliteratorE
	.section	.data.rel.ro._ZTVN6icu_6722CompoundTransliteratorE,"awG",@progbits,_ZTVN6icu_6722CompoundTransliteratorE,comdat
	.align 8
	.type	_ZTVN6icu_6722CompoundTransliteratorE, @object
	.size	_ZTVN6icu_6722CompoundTransliteratorE, 168
_ZTVN6icu_6722CompoundTransliteratorE:
	.quad	0
	.quad	_ZTIN6icu_6722CompoundTransliteratorE
	.quad	_ZN6icu_6722CompoundTransliteratorD1Ev
	.quad	_ZN6icu_6722CompoundTransliteratorD0Ev
	.quad	_ZNK6icu_6722CompoundTransliterator17getDynamicClassIDEv
	.quad	_ZNK6icu_6722CompoundTransliterator5cloneEv
	.quad	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableEii
	.quad	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableE
	.quad	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableER14UTransPositionRKNS_13UnicodeStringER10UErrorCode
	.quad	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableER14UTransPositioniR10UErrorCode
	.quad	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableER14UTransPositionR10UErrorCode
	.quad	_ZNK6icu_6714Transliterator21finishTransliterationERNS_11ReplaceableER14UTransPosition
	.quad	_ZNK6icu_6722CompoundTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositiona
	.quad	_ZNK6icu_6714Transliterator21filteredTransliterateERNS_11ReplaceableER14UTransPositiona
	.quad	_ZNK6icu_6714Transliterator21filteredTransliterateERNS_11ReplaceableER14UTransPositionaa
	.quad	_ZNK6icu_6714Transliterator5getIDEv
	.quad	_ZNK6icu_6722CompoundTransliterator7toRulesERNS_13UnicodeStringEa
	.quad	_ZNK6icu_6722CompoundTransliterator18handleGetSourceSetERNS_10UnicodeSetE
	.quad	_ZNK6icu_6722CompoundTransliterator12getTargetSetERNS_10UnicodeSetE
	.quad	_ZNK6icu_6722CompoundTransliterator8getCountEv
	.quad	_ZNK6icu_6722CompoundTransliterator17getTransliteratorEi
	.local	_ZZN6icu_6722CompoundTransliterator16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6722CompoundTransliterator16getStaticClassIDEvE7classID,1,1
	.globl	_ZN6icu_6722CompoundTransliterator11PASS_STRINGE
	.section	.rodata
	.align 8
	.type	_ZN6icu_6722CompoundTransliterator11PASS_STRINGE, @object
	.size	_ZN6icu_6722CompoundTransliterator11PASS_STRINGE, 12
_ZN6icu_6722CompoundTransliterator11PASS_STRINGE:
	.value	37
	.value	80
	.value	97
	.value	115
	.value	115
	.value	0
	.align 2
	.type	_ZL11COLON_COLON, @object
	.size	_ZL11COLON_COLON, 6
_ZL11COLON_COLON:
	.value	58
	.value	58
	.value	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
