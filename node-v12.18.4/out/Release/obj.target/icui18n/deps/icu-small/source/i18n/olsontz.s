	.file	"olsontz.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713OlsonTimeZone17getDynamicClassIDEv
	.type	_ZNK6icu_6713OlsonTimeZone17getDynamicClassIDEv, @function
_ZNK6icu_6713OlsonTimeZone17getDynamicClassIDEv:
.LFB3225:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6713OlsonTimeZone16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3225:
	.size	_ZNK6icu_6713OlsonTimeZone17getDynamicClassIDEv, .-_ZNK6icu_6713OlsonTimeZone17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713OlsonTimeZone12setRawOffsetEi
	.type	_ZN6icu_6713OlsonTimeZone12setRawOffsetEi, @function
_ZN6icu_6713OlsonTimeZone12setRawOffsetEi:
.LFB3244:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3244:
	.size	_ZN6icu_6713OlsonTimeZone12setRawOffsetEi, .-_ZN6icu_6713OlsonTimeZone12setRawOffsetEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713OlsonTimeZone5cloneEv
	.type	_ZNK6icu_6713OlsonTimeZone5cloneEv, @function
_ZNK6icu_6713OlsonTimeZone5cloneEv:
.LFB3239:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	$224, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L4
	movq	%rax, %rdi
	movq	%rbx, %rsi
	call	_ZN6icu_6713BasicTimeZoneC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6713OlsonTimeZoneE(%rip), %rax
	movq	$0, 216(%r12)
	movq	%rax, (%r12)
	movq	152(%rbx), %rax
	movq	$0, 128(%r12)
	movq	%rax, 152(%r12)
	movdqu	80(%rbx), %xmm1
	movups	%xmm1, 80(%r12)
	movq	96(%rbx), %rax
	movq	%rax, 96(%r12)
	movzwl	72(%rbx), %eax
	movw	%ax, 72(%r12)
	movzwl	74(%rbx), %eax
	movw	%ax, 74(%r12)
	movzwl	76(%rbx), %eax
	movw	%ax, 76(%r12)
	movzwl	104(%rbx), %eax
	movw	%ax, 104(%r12)
	movdqu	112(%rbx), %xmm2
	movups	%xmm2, 112(%r12)
	movq	128(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L6
	movq	(%rdi), %rax
	call	*96(%rax)
	movq	%rax, %rdi
.L6:
	movl	144(%rbx), %eax
	movsd	136(%rbx), %xmm0
	xorl	%edx, %edx
	movq	%rdi, 128(%r12)
	movq	$0, 208(%r12)
	movl	%eax, 144(%r12)
	xorl	%eax, %eax
	movw	%ax, 200(%r12)
	movw	%dx, 176(%r12)
	movsd	%xmm0, 136(%r12)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 160(%r12)
	movups	%xmm0, 184(%r12)
	movl	$0, 216(%r12)
	mfence
.L4:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3239:
	.size	_ZNK6icu_6713OlsonTimeZone5cloneEv, .-_ZNK6icu_6713OlsonTimeZone5cloneEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713OlsonTimeZone15useDaylightTimeEv
	.type	_ZNK6icu_6713OlsonTimeZone15useDaylightTimeEv, @function
_ZNK6icu_6713OlsonTimeZone15useDaylightTimeEv:
.LFB3248:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	uprv_getUTCtime_67@PLT
	movq	128(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L15
	comisd	136(%rbx), %xmm0
	jnb	.L41
.L15:
	leaq	-28(%rbp), %r9
	leaq	-32(%rbp), %r8
	leaq	-36(%rbp), %rcx
	leaq	-40(%rbp), %rdx
	leaq	-44(%rbp), %rsi
	leaq	-48(%rbp), %rdi
	call	_ZN6icu_675Grego12timeToFieldsEdRiS1_S1_S1_S1_S1_@PLT
	movl	-48(%rbp), %edi
	movl	$1, %edx
	xorl	%esi, %esi
	call	_ZN6icu_675Grego11fieldsToDayEiii@PLT
	movl	-48(%rbp), %eax
	movl	$1, %edx
	xorl	%esi, %esi
	movsd	.LC0(%rip), %xmm2
	leal	1(%rax), %edi
	mulsd	%xmm0, %xmm2
	movsd	%xmm2, -56(%rbp)
	call	_ZN6icu_675Grego11fieldsToDayEiii@PLT
	movzwl	72(%rbx), %edi
	movzwl	74(%rbx), %r9d
	movzwl	76(%rbx), %r8d
	mulsd	.LC0(%rip), %xmm0
	leal	(%r9,%rdi), %r10d
	addl	%r10d, %r8d
	testw	%r8w, %r8w
	jle	.L29
	movl	%edi, %esi
	movsd	-56(%rbp), %xmm2
	xorl	%edx, %edx
	negl	%esi
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L18:
	cmpw	%r9w, %si
	jl	.L42
	movl	%edx, %ecx
	movq	96(%rbx), %r11
	subl	%r10d, %ecx
	movswl	%cx, %ecx
	addl	%ecx, %ecx
	movslq	%ecx, %rcx
	movl	(%r11,%rcx,4), %eax
	movl	4(%r11,%rcx,4), %ecx
	salq	$32, %rax
	orq	%rcx, %rax
.L19:
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%rax, %xmm1
	comisd	%xmm0, %xmm1
	jnb	.L29
.L43:
	comisd	%xmm2, %xmm1
	jb	.L23
	movq	120(%rbx), %r11
	movq	112(%rbx), %rcx
	movzbl	(%r11,%rdx), %eax
	addl	%eax, %eax
	cltq
	movl	4(%rcx,%rax,4), %eax
	testl	%eax, %eax
	jne	.L32
	comisd	%xmm2, %xmm1
	jbe	.L23
	movl	$4, %eax
	testw	%dx, %dx
	je	.L25
	movzbl	-1(%r11,%rdx), %eax
	addl	%eax, %eax
	cltq
	leaq	4(,%rax,4), %rax
.L25:
	movl	(%rcx,%rax), %eax
	testl	%eax, %eax
	jne	.L32
.L23:
	addq	$1, %rdx
	addl	$1, %esi
	cmpw	%dx, %r8w
	jle	.L29
.L26:
	cmpw	%dx, %di
	jle	.L18
	movq	80(%rbx), %rcx
	pxor	%xmm1, %xmm1
	movl	(%rcx,%rdx,8), %eax
	movl	4(%rcx,%rdx,8), %ecx
	salq	$32, %rax
	orq	%rcx, %rax
	cvtsi2sdq	%rax, %xmm1
	comisd	%xmm0, %xmm1
	jb	.L43
	.p2align 4,,10
	.p2align 3
.L29:
	xorl	%eax, %eax
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L42:
	movq	88(%rbx), %rcx
	movswq	%si, %rax
	movslq	(%rcx,%rax,4), %rax
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L41:
	movq	(%rdi), %rax
	call	*72(%rax)
.L14:
	movq	-24(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L44
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore_state
	movl	$1, %eax
	jmp	.L14
.L44:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3248:
	.size	_ZNK6icu_6713OlsonTimeZone15useDaylightTimeEv, .-_ZNK6icu_6713OlsonTimeZone15useDaylightTimeEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713OlsonTimeZone12hasSameRulesERKNS_8TimeZoneE
	.type	_ZNK6icu_6713OlsonTimeZone12hasSameRulesERKNS_8TimeZoneE, @function
_ZNK6icu_6713OlsonTimeZone12hasSameRulesERKNS_8TimeZoneE:
.LFB3251:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpq	%rsi, %rdi
	je	.L58
	movq	%rsi, %rdi
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6713OlsonTimeZoneE(%rip), %rdx
	leaq	_ZTIN6icu_678TimeZoneE(%rip), %rsi
	call	__dynamic_cast@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L49
	movq	120(%rax), %rax
	cmpq	%rax, 120(%rbx)
	je	.L58
	movq	128(%rbx), %rdi
	movq	128(%r12), %rsi
	testq	%rdi, %rdi
	je	.L91
	testq	%rsi, %rsi
	je	.L49
	movq	(%rdi), %rax
	call	*24(%rax)
	testb	%al, %al
	je	.L49
	cmpq	$0, 128(%rbx)
	je	.L51
	movl	144(%r12), %eax
	cmpl	%eax, 144(%rbx)
	je	.L92
	.p2align 4,,10
	.p2align 3
.L49:
	xorl	%r13d, %r13d
.L45:
	addq	$24, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L58:
	.cfi_restore_state
	movl	$1, %r13d
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L91:
	testq	%rsi, %rsi
	jne	.L49
.L51:
	movzwl	104(%rbx), %r14d
	cmpw	104(%r12), %r14w
	jne	.L49
	movabsq	$281474976710655, %rdx
	movq	72(%rbx), %rax
	xorq	72(%r12), %rax
	testq	%rdx, %rax
	jne	.L49
	movq	80(%r12), %rsi
	movq	80(%rbx), %rdi
	movzwl	72(%rbx), %r15d
	movq	%rsi, %rax
	orq	%rdi, %rax
	je	.L52
	testq	%rdi, %rdi
	setne	%dl
	testq	%rsi, %rsi
	setne	%al
	xorl	%r13d, %r13d
	cmpb	%al, %dl
	jne	.L45
	cmpq	%rdi, %rsi
	je	.L52
	movswq	%r15w, %rdx
	salq	$3, %rdx
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L45
	.p2align 4,,10
	.p2align 3
.L52:
	movq	88(%r12), %rsi
	movq	88(%rbx), %rdi
	movzwl	74(%rbx), %ecx
	movq	%rsi, %rax
	orq	%rdi, %rax
	je	.L54
	testq	%rdi, %rdi
	setne	%dl
	testq	%rsi, %rsi
	setne	%al
	xorl	%r13d, %r13d
	cmpb	%al, %dl
	jne	.L45
	cmpq	%rdi, %rsi
	je	.L54
	movswq	%cx, %rdx
	movl	%ecx, -52(%rbp)
	salq	$2, %rdx
	call	memcmp@PLT
	movl	-52(%rbp), %ecx
	testl	%eax, %eax
	jne	.L45
.L54:
	movq	96(%r12), %rsi
	movq	96(%rbx), %rdi
	movzwl	76(%rbx), %r8d
	movq	%rsi, %rax
	orq	%rdi, %rax
	je	.L55
	testq	%rdi, %rdi
	setne	%dl
	testq	%rsi, %rsi
	setne	%al
	xorl	%r13d, %r13d
	cmpb	%al, %dl
	jne	.L45
	cmpq	%rdi, %rsi
	je	.L55
	movswq	%r8w, %rdx
	movl	%ecx, -56(%rbp)
	salq	$3, %rdx
	movl	%r8d, -52(%rbp)
	call	memcmp@PLT
	movl	-52(%rbp), %r8d
	movl	-56(%rbp), %ecx
	testl	%eax, %eax
	jne	.L45
.L55:
	movq	112(%r12), %rsi
	movq	112(%rbx), %rdi
	movq	%rsi, %rax
	orq	%rdi, %rax
	je	.L56
	testq	%rdi, %rdi
	setne	%dl
	testq	%rsi, %rsi
	setne	%al
	xorl	%r13d, %r13d
	cmpb	%al, %dl
	jne	.L45
	cmpq	%rdi, %rsi
	je	.L56
	movswq	%r14w, %rdx
	movl	%r8d, -56(%rbp)
	salq	$3, %rdx
	movl	%ecx, -52(%rbp)
	call	memcmp@PLT
	movl	-52(%rbp), %ecx
	movl	-56(%rbp), %r8d
	testl	%eax, %eax
	jne	.L45
.L56:
	movq	120(%r12), %rsi
	movq	120(%rbx), %rdi
	movl	$1, %r13d
	movq	%rsi, %rax
	orq	%rdi, %rax
	je	.L45
	testq	%rdi, %rdi
	setne	%dl
	testq	%rsi, %rsi
	setne	%al
	xorl	%r13d, %r13d
	cmpb	%al, %dl
	jne	.L45
	cmpq	%rdi, %rsi
	je	.L58
	leal	(%rcx,%r8), %edx
	addl	%r15d, %edx
	movswq	%dx, %rdx
	call	memcmp@PLT
	testl	%eax, %eax
	sete	%r13b
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L92:
	movsd	136(%rbx), %xmm0
	ucomisd	136(%r12), %xmm0
	jp	.L49
	je	.L51
	jmp	.L49
	.cfi_endproc
.LFE3251:
	.size	_ZNK6icu_6713OlsonTimeZone12hasSameRulesERKNS_8TimeZoneE, .-_ZNK6icu_6713OlsonTimeZone12hasSameRulesERKNS_8TimeZoneE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713OlsonTimeZone18getOffsetFromLocalEdiiRiS1_R10UErrorCode
	.type	_ZNK6icu_6713OlsonTimeZone18getOffsetFromLocalEdiiRiS1_R10UErrorCode, @function
_ZNK6icu_6713OlsonTimeZone18getOffsetFromLocalEdiiRiS1_R10UErrorCode:
.LFB3243:
	.cfi_startproc
	endbr64
	movl	(%r9), %eax
	testl	%eax, %eax
	jg	.L138
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	128(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L95
	comisd	136(%rbx), %xmm0
	jb	.L95
	movq	(%rdi), %rax
	movq	160(%rax), %rax
	addq	$40, %rsp
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L95:
	.cfi_restore_state
	movzwl	76(%rbx), %eax
	movzwl	74(%rbx), %edx
	movl	%esi, -60(%rbp)
	addw	72(%rbx), %dx
	addl	%edx, %eax
	testw	%ax, %ax
	movl	%eax, -52(%rbp)
	jg	.L143
	movq	112(%rbx), %rax
	imull	$1000, (%rax), %edx
	movl	%edx, (%r12)
	imull	$1000, 4(%rax), %eax
	movl	%eax, (%r8)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L138:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L143:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	%r8, -72(%rbp)
	divsd	.LC1(%rip), %xmm0
	call	uprv_floor_67@PLT
	movl	-60(%rbp), %esi
	movl	%r15d, %ecx
	movzwl	72(%rbx), %r14d
	andl	$3, %ecx
	movl	-52(%rbp), %eax
	andl	$12, %r15d
	movq	-72(%rbp), %r8
	movl	%ecx, -56(%rbp)
	movl	%r14d, %edx
	movl	%esi, %ecx
	andl	$12, %esi
	andl	$3, %ecx
	notl	%edx
	movl	%r15d, -64(%rbp)
	leal	-1(%rax), %edi
	movl	%ecx, -52(%rbp)
	addl	%eax, %edx
	movq	112(%rbx), %rcx
	movl	%esi, -60(%rbp)
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L98:
	movzwl	74(%rbx), %eax
	cmpw	%ax, %dx
	jl	.L144
	movl	%edx, %esi
	movq	96(%rbx), %r9
	subl	%eax, %esi
	movswl	%si, %esi
.L141:
	addl	%esi, %esi
	movslq	%esi, %rsi
	movl	(%r9,%rsi,4), %eax
	movl	4(%r9,%rsi,4), %esi
	salq	$32, %rax
	orq	%rsi, %rax
.L99:
	leaq	-86400(%rax), %rsi
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%rsi, %xmm1
	comisd	%xmm1, %xmm0
	jb	.L101
	movq	120(%rbx), %r9
	leal	-1(%rdi), %esi
	testw	%di, %di
	je	.L123
	movswq	%si, %rsi
	movzbl	(%r9,%rsi), %r10d
	addl	%r10d, %r10d
	movslq	%r10d, %r10
	salq	$2, %r10
	leaq	4(%r10), %rsi
	addq	%rcx, %r10
.L103:
	movl	(%r10), %r13d
	movswq	%di, %r10
	movl	(%rcx,%rsi), %esi
	movzbl	(%r9,%r10), %r10d
	addl	%esi, %r13d
	addl	%r10d, %r10d
	movslq	%r10d, %r10
	movl	4(%rcx,%r10,4), %r9d
	movl	(%rcx,%r10,4), %r11d
	addl	%r9d, %r11d
	testl	%esi, %esi
	setne	%r15b
	testl	%r9d, %r9d
	movslq	%r11d, %r10
	sete	%r11b
	andl	%r15d, %r11d
	testl	%r9d, %r9d
	setne	%r9b
	testl	%esi, %esi
	sete	%sil
	andl	%esi, %r9d
	cmpl	%r13d, %r10d
	js	.L104
	cmpl	$1, -52(%rbp)
	je	.L145
	cmpl	$3, -52(%rbp)
	je	.L146
.L108:
	cmpl	$12, -60(%rbp)
	je	.L142
.L118:
	addq	%r10, %rax
.L101:
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%rax, %xmm1
	comisd	%xmm1, %xmm0
	jnb	.L119
	subl	$1, %edi
	subl	$1, %edx
	cmpw	$-1, %di
	je	.L147
.L121:
	cmpw	%di, %r14w
	jle	.L98
	movq	80(%rbx), %r9
	movswl	%di, %esi
	jmp	.L141
	.p2align 4,,10
	.p2align 3
.L144:
	movq	88(%rbx), %rsi
	movswq	%dx, %rax
	movslq	(%rsi,%rax,4), %rax
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L149:
	testb	%r9b, %r9b
	jne	.L118
	testb	%r11b, %r11b
	je	.L115
	.p2align 4,,10
	.p2align 3
.L142:
	movslq	%r13d, %r13
	addq	%r13, %rax
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L104:
	cmpl	$1, -56(%rbp)
	je	.L148
	cmpl	$3, -56(%rbp)
	je	.L149
.L115:
	cmpl	$4, -64(%rbp)
	jne	.L118
	jmp	.L142
	.p2align 4,,10
	.p2align 3
.L123:
	movq	%rcx, %r10
	movl	$4, %esi
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L145:
	testb	%r11b, %r11b
	jne	.L142
	testb	%r9b, %r9b
	jne	.L118
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L146:
	testb	%r9b, %r9b
	jne	.L142
	testb	%r11b, %r11b
	jne	.L118
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L148:
	testb	%r11b, %r11b
	jne	.L118
	testb	%r9b, %r9b
	jne	.L142
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L119:
	movswq	%di, %rdi
	addq	120(%rbx), %rdi
	movzbl	(%rdi), %eax
	addl	%eax, %eax
	cltq
	imull	$1000, (%rcx,%rax,4), %eax
	movl	%eax, (%r12)
	movzbl	(%rdi), %eax
	addl	%eax, %eax
	cltq
	leaq	4(,%rax,4), %rax
.L122:
	imull	$1000, (%rcx,%rax), %eax
	movl	%eax, (%r8)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L147:
	.cfi_restore_state
	imull	$1000, (%rcx), %eax
	movl	%eax, (%r12)
	movl	$4, %eax
	jmp	.L122
	.cfi_endproc
.LFE3243:
	.size	_ZNK6icu_6713OlsonTimeZone18getOffsetFromLocalEdiiRiS1_R10UErrorCode, .-_ZNK6icu_6713OlsonTimeZone18getOffsetFromLocalEdiiRiS1_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713OlsonTimeZone16getStaticClassIDEv
	.type	_ZN6icu_6713OlsonTimeZone16getStaticClassIDEv, @function
_ZN6icu_6713OlsonTimeZone16getStaticClassIDEv:
.LFB3224:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6713OlsonTimeZone16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3224:
	.size	_ZN6icu_6713OlsonTimeZone16getStaticClassIDEv, .-_ZN6icu_6713OlsonTimeZone16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713OlsonTimeZone14constructEmptyEv
	.type	_ZN6icu_6713OlsonTimeZone14constructEmptyEv, @function
_ZN6icu_6713OlsonTimeZone14constructEmptyEv:
.LFB3226:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	pxor	%xmm0, %xmm0
	movl	$1, %edx
	movq	$0, 152(%rdi)
	movw	%ax, 76(%rdi)
	leaq	_ZN6icu_67L5ZEROSE(%rip), %rax
	movl	$0, 72(%rdi)
	movq	$0, 96(%rdi)
	movq	$0, 120(%rdi)
	movw	%dx, 104(%rdi)
	movq	%rax, 112(%rdi)
	movq	$0, 128(%rdi)
	movups	%xmm0, 80(%rdi)
	ret
	.cfi_endproc
.LFE3226:
	.size	_ZN6icu_6713OlsonTimeZone14constructEmptyEv, .-_ZN6icu_6713OlsonTimeZone14constructEmptyEv
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC2:
	.string	"transPre32"
.LC3:
	.string	"trans"
.LC4:
	.string	"transPost32"
.LC5:
	.string	"typeOffsets"
.LC6:
	.string	"typeMap"
.LC7:
	.string	"finalRule"
.LC8:
	.string	"finalRaw"
.LC9:
	.string	"finalYear"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713OlsonTimeZoneC2EPK15UResourceBundleS3_RKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6713OlsonTimeZoneC2EPK15UResourceBundleS3_RKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6713OlsonTimeZoneC2EPK15UResourceBundleS3_RKNS_13UnicodeStringER10UErrorCode:
.LFB3228:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$392, %rsp
	movq	%rsi, -416(%rbp)
	movq	%rcx, %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6713BasicTimeZoneC2ERKNS_13UnicodeStringE@PLT
	leaq	16+_ZTVN6icu_6713OlsonTimeZoneE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	$0, 128(%rbx)
	movq	%rax, (%rbx)
	xorl	%eax, %eax
	movw	%ax, 200(%rbx)
	xorl	%eax, %eax
	movq	$0, 208(%rbx)
	movq	$0, 216(%rbx)
	movw	%ax, 176(%rbx)
	movups	%xmm0, 160(%rbx)
	movups	%xmm0, 184(%rbx)
	movl	$0, 216(%rbx)
	mfence
	testq	%r13, %r13
	je	.L182
	testq	%r12, %r12
	je	.L182
	movl	(%r14), %r13d
	testl	%r13d, %r13d
	jg	.L178
	leaq	-256(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN6icu_6720StackUResourceBundleC1Ev@PLT
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	call	ures_getByKey_67@PLT
	leaq	-396(%rbp), %rax
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	movq	%rax, -408(%rbp)
	call	ures_getIntVector_67@PLT
	movl	-396(%rbp), %edx
	movq	%rax, 80(%rbx)
	movl	%edx, %eax
	sarl	%eax
	movw	%ax, 72(%rbx)
	movl	(%r14), %eax
	cmpl	$2, %eax
	je	.L193
	testl	%eax, %eax
	jle	.L194
.L157:
	movq	%r14, %rcx
	movq	%r13, %rdx
	leaq	.LC3(%rip), %rsi
	movq	%r12, %rdi
	call	ures_getByKey_67@PLT
	movq	-408(%rbp), %rsi
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	ures_getIntVector_67@PLT
	movl	-396(%rbp), %edx
	movq	%rax, 88(%rbx)
	movl	(%r14), %eax
	movw	%dx, 74(%rbx)
	cmpl	$2, %eax
	je	.L195
	testl	%eax, %eax
	jle	.L196
.L160:
	movq	%r14, %rcx
	movq	%r13, %rdx
	leaq	.LC4(%rip), %rsi
	movq	%r12, %rdi
	call	ures_getByKey_67@PLT
	movq	-408(%rbp), %rsi
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	ures_getIntVector_67@PLT
	movl	-396(%rbp), %edx
	movq	%rax, 96(%rbx)
	movl	%edx, %eax
	sarl	%eax
	movw	%ax, 76(%rbx)
	movl	(%r14), %eax
	cmpl	$2, %eax
	je	.L197
	testl	%eax, %eax
	jle	.L198
.L162:
	movq	%r14, %rcx
	movq	%r13, %rdx
	leaq	.LC5(%rip), %rsi
	movq	%r12, %rdi
	call	ures_getByKey_67@PLT
	movq	-408(%rbp), %rsi
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	ures_getIntVector_67@PLT
	movl	(%r14), %r8d
	movq	%rax, 112(%rbx)
	movl	-396(%rbp), %eax
	testl	%r8d, %r8d
	jle	.L164
.L165:
	cwtl
	movq	$0, 120(%rbx)
	sarl	%eax
	movw	%ax, 104(%rbx)
	movzwl	72(%rbx), %eax
	addw	74(%rbx), %ax
	addw	76(%rbx), %ax
	testw	%ax, %ax
	jg	.L199
.L168:
	movq	-408(%rbp), %rdx
	movq	%r14, %rcx
	leaq	.LC7(%rip), %rsi
	movq	%r12, %rdi
	call	ures_getStringByKey_67@PLT
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	leaq	.LC8(%rip), %rsi
	movq	%rax, -424(%rbp)
	call	ures_getByKey_67@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	ures_getInt_67@PLT
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	leaq	.LC9(%rip), %rsi
	movl	%eax, -428(%rbp)
	call	ures_getByKey_67@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	ures_getInt_67@PLT
	movl	%eax, %r11d
	movl	(%r14), %eax
	testl	%eax, %eax
	jle	.L200
	cmpl	$2, %eax
	jne	.L177
	movl	$0, (%r14)
.L177:
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_678ZoneMeta18getCanonicalCLDRIDERKNS_13UnicodeStringER10UErrorCode@PLT
	movq	%r13, %rdi
	movq	%rax, 152(%rbx)
	call	_ZN6icu_6720StackUResourceBundleD1Ev@PLT
	movl	(%r14), %ecx
	testl	%ecx, %ecx
	jle	.L152
	.p2align 4,,10
	.p2align 3
.L178:
	xorl	%eax, %eax
	movl	$1, %edx
	movq	$0, 152(%rbx)
	pxor	%xmm0, %xmm0
	movw	%ax, 76(%rbx)
	leaq	_ZN6icu_67L5ZEROSE(%rip), %rax
	movl	$0, 72(%rbx)
	movq	$0, 96(%rbx)
	movq	$0, 120(%rbx)
	movw	%dx, 104(%rbx)
	movq	%rax, 112(%rbx)
	movq	$0, 128(%rbx)
	movups	%xmm0, 80(%rbx)
.L152:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L201
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L182:
	.cfi_restore_state
	movl	(%r14), %r15d
	testl	%r15d, %r15d
	jg	.L178
	movl	$1, (%r14)
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L193:
	xorl	%r11d, %r11d
	movq	$0, 80(%rbx)
	movw	%r11w, 72(%rbx)
	movl	$0, (%r14)
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L199:
	movq	%r14, %rcx
	movq	%r13, %rdx
	leaq	.LC6(%rip), %rsi
	movq	%r12, %rdi
	call	ures_getByKey_67@PLT
	movq	-408(%rbp), %rsi
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	ures_getBinary_67@PLT
	movq	%rax, 120(%rbx)
	movl	(%r14), %eax
	cmpl	$2, %eax
	je	.L171
	testl	%eax, %eax
	jg	.L168
	movzwl	74(%rbx), %eax
	addw	72(%rbx), %ax
	addw	76(%rbx), %ax
	cwtl
	cmpl	-396(%rbp), %eax
	je	.L168
.L171:
	movl	$3, (%r14)
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L164:
	leal	-2(%rax), %edx
	cmpl	$32764, %edx
	ja	.L166
	testb	$1, %al
	je	.L165
.L166:
	movl	$3, (%r14)
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L195:
	xorl	%r10d, %r10d
	movq	$0, 88(%rbx)
	movw	%r10w, 74(%rbx)
	movl	$0, (%r14)
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L197:
	xorl	%r9d, %r9d
	movq	$0, 96(%rbx)
	movw	%r9w, 76(%rbx)
	movl	$0, (%r14)
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L194:
	cmpl	$32767, %edx
	jbe	.L202
.L158:
	movl	$3, (%r14)
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L196:
	cmpl	$32767, %edx
	jbe	.L160
	movl	$3, (%r14)
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L198:
	cmpl	$32767, %edx
	jbe	.L203
.L163:
	movl	$3, (%r14)
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L200:
	movq	-424(%rbp), %rax
	movl	-396(%rbp), %ecx
	leaq	-384(%rbp), %r12
	leaq	-392(%rbp), %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	movl	%r11d, -432(%rbp)
	movq	%rax, -392(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	-416(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r14, %rcx
	movq	%r12, %rsi
	call	_ZN6icu_678TimeZone8loadRuleEPK15UResourceBundleRKNS_13UnicodeStringEPS1_R10UErrorCode@PLT
	movq	-408(%rbp), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	movq	%rax, -408(%rbp)
	call	ures_getIntVector_67@PLT
	movl	(%r14), %edi
	movq	-408(%rbp), %r10
	testl	%edi, %edi
	jle	.L204
.L173:
	movl	$3, (%r14)
.L175:
	movq	%r10, %rdi
	call	ures_close_67@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L203:
	andl	$1, %edx
	je	.L162
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L202:
	andl	$1, %edx
	je	.L157
	jmp	.L158
	.p2align 4,,10
	.p2align 3
.L204:
	cmpl	$11, -396(%rbp)
	movl	-432(%rbp), %r11d
	jne	.L173
	movq	%rax, -416(%rbp)
	movl	$2, %esi
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$160, %edi
	movq	%r10, -408(%rbp)
	movl	%r11d, -432(%rbp)
	movq	%rax, -320(%rbp)
	movw	%si, -312(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	-408(%rbp), %r10
	testq	%rax, %rax
	movq	%rax, %rdi
	je	.L176
	movq	-416(%rbp), %rdx
	subq	$8, %rsp
	imull	$1000, -428(%rbp), %esi
	movq	%r10, -424(%rbp)
	leaq	-320(%rbp), %r10
	movq	%rdi, -416(%rbp)
	movsbl	8(%rdx), %r9d
	movsbl	4(%rdx), %r8d
	movq	%r10, -408(%rbp)
	imull	$1000, 40(%rdx), %eax
	movsbl	(%rdx), %ecx
	pushq	%r14
	pushq	%rax
	movl	36(%rdx), %eax
	pushq	%rax
	imull	$1000, 32(%rdx), %eax
	pushq	%rax
	movsbl	28(%rdx), %eax
	pushq	%rax
	movsbl	24(%rdx), %eax
	pushq	%rax
	movsbl	20(%rdx), %eax
	pushq	%rax
	movl	16(%rdx), %eax
	pushq	%rax
	imull	$1000, 12(%rdx), %eax
	movq	%r10, %rdx
	pushq	%rax
	call	_ZN6icu_6714SimpleTimeZoneC1EiRKNS_13UnicodeStringEaaaiNS0_8TimeModeEaaaiS4_iR10UErrorCode@PLT
	addq	$80, %rsp
	movl	$1, %edx
	xorl	%esi, %esi
	movq	-416(%rbp), %rdi
	movl	-432(%rbp), %r11d
	movq	%rdi, 128(%rbx)
	movl	%r11d, %edi
	movl	%r11d, 144(%rbx)
	call	_ZN6icu_675Grego11fieldsToDayEiii@PLT
	mulsd	.LC10(%rip), %xmm0
	movq	-424(%rbp), %r10
	movsd	%xmm0, 136(%rbx)
.L179:
	movq	-408(%rbp), %rdi
	movq	%r10, -416(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-416(%rbp), %r10
	jmp	.L175
.L201:
	call	__stack_chk_fail@PLT
.L176:
	leaq	-320(%rbp), %rax
	movl	$7, (%r14)
	movq	$0, 128(%rbx)
	movq	%rax, -408(%rbp)
	jmp	.L179
	.cfi_endproc
.LFE3228:
	.size	_ZN6icu_6713OlsonTimeZoneC2EPK15UResourceBundleS3_RKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6713OlsonTimeZoneC2EPK15UResourceBundleS3_RKNS_13UnicodeStringER10UErrorCode
	.globl	_ZN6icu_6713OlsonTimeZoneC1EPK15UResourceBundleS3_RKNS_13UnicodeStringER10UErrorCode
	.set	_ZN6icu_6713OlsonTimeZoneC1EPK15UResourceBundleS3_RKNS_13UnicodeStringER10UErrorCode,_ZN6icu_6713OlsonTimeZoneC2EPK15UResourceBundleS3_RKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713OlsonTimeZoneC2ERKS0_
	.type	_ZN6icu_6713OlsonTimeZoneC2ERKS0_, @function
_ZN6icu_6713OlsonTimeZoneC2ERKS0_:
.LFB3231:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZN6icu_6713BasicTimeZoneC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6713OlsonTimeZoneE(%rip), %rax
	movdqu	80(%r12), %xmm1
	movl	72(%r12), %edx
	movq	%rax, (%rbx)
	movq	152(%r12), %rax
	movdqu	112(%r12), %xmm2
	movl	%edx, 72(%rbx)
	movq	%rax, 152(%rbx)
	movq	96(%r12), %rax
	movq	128(%r12), %rdi
	movups	%xmm1, 80(%rbx)
	movq	%rax, 96(%rbx)
	movzwl	76(%r12), %eax
	movq	$0, 128(%rbx)
	movw	%ax, 76(%rbx)
	movzwl	104(%r12), %eax
	movq	$0, 216(%rbx)
	movw	%ax, 104(%rbx)
	movups	%xmm2, 112(%rbx)
	testq	%rdi, %rdi
	je	.L206
	movq	(%rdi), %rax
	call	*96(%rax)
	movq	%rax, %rdi
.L206:
	movl	144(%r12), %eax
	xorl	%edx, %edx
	movq	%rdi, 128(%rbx)
	movsd	136(%r12), %xmm0
	movw	%dx, 176(%rbx)
	movl	%eax, 144(%rbx)
	xorl	%eax, %eax
	movw	%ax, 200(%rbx)
	movq	$0, 208(%rbx)
	movsd	%xmm0, 136(%rbx)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 160(%rbx)
	movups	%xmm0, 184(%rbx)
	movl	$0, 216(%rbx)
	mfence
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3231:
	.size	_ZN6icu_6713OlsonTimeZoneC2ERKS0_, .-_ZN6icu_6713OlsonTimeZoneC2ERKS0_
	.globl	_ZN6icu_6713OlsonTimeZoneC1ERKS0_
	.set	_ZN6icu_6713OlsonTimeZoneC1ERKS0_,_ZN6icu_6713OlsonTimeZoneC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713OlsonTimeZoneaSERKS0_
	.type	_ZN6icu_6713OlsonTimeZoneaSERKS0_, @function
_ZN6icu_6713OlsonTimeZoneaSERKS0_:
.LFB3233:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	152(%rsi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	movq	%rax, 152(%rdi)
	movdqu	80(%rsi), %xmm1
	movups	%xmm1, 80(%rdi)
	movq	96(%rsi), %rax
	movq	%rax, 96(%rdi)
	movzwl	72(%rsi), %eax
	movw	%ax, 72(%rdi)
	movzwl	74(%rsi), %eax
	movw	%ax, 74(%rdi)
	movzwl	76(%rsi), %eax
	movw	%ax, 76(%rdi)
	movzwl	104(%rsi), %eax
	movw	%ax, 104(%rdi)
	movdqu	112(%rsi), %xmm2
	movups	%xmm2, 112(%rdi)
	movq	128(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L212
	movq	(%rdi), %rax
	call	*8(%rax)
.L212:
	movq	128(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L213
	movq	(%rdi), %rax
	call	*96(%rax)
	movq	%rax, %rdi
.L213:
	movl	144(%rbx), %eax
	movsd	136(%rbx), %xmm0
	xorl	%edx, %edx
	movq	%rdi, 128(%r12)
	movq	$0, 208(%r12)
	movl	%eax, 144(%r12)
	xorl	%eax, %eax
	movw	%ax, 200(%r12)
	movq	%r12, %rax
	movw	%dx, 176(%r12)
	movsd	%xmm0, 136(%r12)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 160(%r12)
	movups	%xmm0, 184(%r12)
	movl	$0, 216(%r12)
	mfence
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3233:
	.size	_ZN6icu_6713OlsonTimeZoneaSERKS0_, .-_ZN6icu_6713OlsonTimeZoneaSERKS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713OlsonTimeZone23transitionTimeInSecondsEs
	.type	_ZNK6icu_6713OlsonTimeZone23transitionTimeInSecondsEs, @function
_ZNK6icu_6713OlsonTimeZone23transitionTimeInSecondsEs:
.LFB3246:
	.cfi_startproc
	endbr64
	movzwl	72(%rdi), %eax
	cmpw	%si, %ax
	jg	.L226
	subl	%eax, %esi
	movzwl	74(%rdi), %eax
	cmpw	%si, %ax
	jg	.L227
	movq	96(%rdi), %rdx
	subl	%eax, %esi
.L225:
	movswl	%si, %esi
	addl	%esi, %esi
	movslq	%esi, %rsi
	movl	(%rdx,%rsi,4), %eax
	movl	4(%rdx,%rsi,4), %edx
	salq	$32, %rax
	orq	%rdx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L227:
	movq	88(%rdi), %rax
	movswq	%si, %rsi
	movslq	(%rax,%rsi,4), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L226:
	movq	80(%rdi), %rdx
	jmp	.L225
	.cfi_endproc
.LFE3246:
	.size	_ZNK6icu_6713OlsonTimeZone23transitionTimeInSecondsEs, .-_ZNK6icu_6713OlsonTimeZone23transitionTimeInSecondsEs
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713OlsonTimeZone19getHistoricalOffsetEdaiiRiS1_
	.type	_ZNK6icu_6713OlsonTimeZone19getHistoricalOffsetEdaiiRiS1_, @function
_ZNK6icu_6713OlsonTimeZone19getHistoricalOffsetEdaiiRiS1_:
.LFB3247:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%r9, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movzwl	74(%rdi), %r15d
	addw	72(%rdi), %r15w
	movl	%ecx, -64(%rbp)
	addw	76(%rdi), %r15w
	movq	%r8, -56(%rbp)
	movl	%esi, -60(%rbp)
	testw	%r15w, %r15w
	jg	.L279
	movq	112(%rbx), %rax
	imull	$1000, (%rax), %edx
	movl	%edx, (%r8)
	imull	$1000, 4(%rax), %eax
	movl	%eax, (%r9)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L279:
	.cfi_restore_state
	movl	%esi, %r13d
	movl	%edx, %r14d
	divsd	.LC1(%rip), %xmm0
	call	uprv_floor_67@PLT
	movl	-60(%rbp), %esi
	movzwl	72(%rbx), %r8d
	movl	-64(%rbp), %ecx
	testb	%sil, %sil
	jne	.L230
	testw	%r8w, %r8w
	jg	.L280
	movl	%r8d, %eax
	movzwl	74(%rbx), %edx
	negl	%eax
	cmpw	%dx, %ax
	jl	.L281
	subl	%edx, %eax
	movq	96(%rbx), %rsi
	movswl	%ax, %edx
	addl	%edx, %edx
	movslq	%edx, %rdx
	movl	(%rsi,%rdx,4), %eax
	movl	4(%rsi,%rdx,4), %edx
	salq	$32, %rax
	orq	%rdx, %rax
.L232:
	pxor	%xmm1, %xmm1
	movq	112(%rbx), %rdi
	cvtsi2sdq	%rax, %xmm1
	comisd	%xmm0, %xmm1
	jbe	.L234
	imull	$1000, (%rdi), %eax
	movq	-56(%rbp), %rbx
	movl	%eax, (%rbx)
	imull	$1000, 4(%rdi), %eax
	movl	%eax, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L230:
	.cfi_restore_state
	movq	112(%rbx), %rdi
.L234:
	movl	%ecx, %eax
	andl	$12, %ecx
	leal	-1(%r15), %esi
	andl	$3, %eax
	movl	%ecx, -72(%rbp)
	movl	%esi, %edx
	movl	%eax, -64(%rbp)
	movl	%r14d, %eax
	andl	$12, %r14d
	subl	%r8d, %edx
	andl	$3, %eax
	movl	%r14d, -68(%rbp)
	movl	%eax, -60(%rbp)
	.p2align 4,,10
	.p2align 3
.L260:
	cmpw	%si, %r8w
	jg	.L282
	movzwl	74(%rbx), %eax
	cmpw	%ax, %dx
	jl	.L283
	movl	%edx, %ecx
	movq	96(%rbx), %r9
	subl	%eax, %ecx
	movswl	%cx, %ecx
.L277:
	addl	%ecx, %ecx
	movslq	%ecx, %rcx
	movl	(%r9,%rcx,4), %eax
	movl	4(%r9,%rcx,4), %ecx
	salq	$32, %rax
	orq	%rcx, %rax
.L238:
	testb	%r13b, %r13b
	je	.L240
	leaq	-86400(%rax), %rcx
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%rcx, %xmm1
	comisd	%xmm1, %xmm0
	jb	.L240
	movq	120(%rbx), %r10
	leal	-1(%rsi), %ecx
	testw	%si, %si
	je	.L262
	movswq	%cx, %rcx
	movzbl	(%r10,%rcx), %r9d
	addl	%r9d, %r9d
	movslq	%r9d, %r9
	salq	$2, %r9
	leaq	4(%r9), %rcx
.L242:
	movl	(%rdi,%r9), %r14d
	movswq	%si, %r9
	movl	(%rdi,%rcx), %ecx
	movzbl	(%r10,%r9), %r10d
	addl	%ecx, %r14d
	addl	%r10d, %r10d
	movslq	%r10d, %r10
	movl	4(%rdi,%r10,4), %r9d
	movl	(%rdi,%r10,4), %r11d
	addl	%r9d, %r11d
	testl	%ecx, %ecx
	setne	%r15b
	testl	%r9d, %r9d
	movslq	%r11d, %r10
	sete	%r11b
	andl	%r15d, %r11d
	testl	%r9d, %r9d
	setne	%r9b
	testl	%ecx, %ecx
	sete	%cl
	andl	%ecx, %r9d
	cmpl	%r14d, %r10d
	js	.L243
	cmpl	$1, -60(%rbp)
	je	.L284
	cmpl	$3, -60(%rbp)
	je	.L285
.L247:
	cmpl	$12, -68(%rbp)
	jne	.L257
.L278:
	movslq	%r14d, %r14
	addq	%r14, %rax
	.p2align 4,,10
	.p2align 3
.L240:
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%rax, %xmm1
	comisd	%xmm1, %xmm0
	jnb	.L258
.L288:
	subl	$1, %esi
	subl	$1, %edx
	cmpw	$-1, %si
	jne	.L260
	imull	$1000, (%rdi), %eax
	movq	-56(%rbp), %rbx
	movl	%eax, (%rbx)
	movl	$4, %eax
.L261:
	imull	$1000, (%rdi,%rax), %eax
	movl	%eax, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L282:
	.cfi_restore_state
	movq	80(%rbx), %r9
	movswl	%si, %ecx
	jmp	.L277
	.p2align 4,,10
	.p2align 3
.L283:
	movq	88(%rbx), %rcx
	movswq	%dx, %rax
	movslq	(%rcx,%rax,4), %rax
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L243:
	cmpl	$1, -64(%rbp)
	je	.L286
	cmpl	$3, -64(%rbp)
	je	.L287
.L254:
	cmpl	$4, -72(%rbp)
	je	.L278
.L257:
	addq	%r10, %rax
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%rax, %xmm1
	comisd	%xmm1, %xmm0
	jb	.L288
	.p2align 4,,10
	.p2align 3
.L258:
	movswq	%si, %rsi
	addq	120(%rbx), %rsi
	movq	-56(%rbp), %rbx
	movzbl	(%rsi), %eax
	addl	%eax, %eax
	cltq
	imull	$1000, (%rdi,%rax,4), %eax
	movl	%eax, (%rbx)
	movzbl	(%rsi), %eax
	addl	%eax, %eax
	cltq
	leaq	4(,%rax,4), %rax
	jmp	.L261
	.p2align 4,,10
	.p2align 3
.L262:
	movl	$4, %ecx
	xorl	%r9d, %r9d
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L285:
	testb	%r9b, %r9b
	jne	.L278
	testb	%r11b, %r11b
	jne	.L257
	jmp	.L247
	.p2align 4,,10
	.p2align 3
.L286:
	testb	%r11b, %r11b
	jne	.L257
	testb	%r9b, %r9b
	jne	.L278
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L280:
	movq	80(%rbx), %rdx
	movl	(%rdx), %eax
	movl	4(%rdx), %edx
	salq	$32, %rax
	orq	%rdx, %rax
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L284:
	testb	%r11b, %r11b
	jne	.L278
	testb	%r9b, %r9b
	jne	.L257
	jmp	.L247
	.p2align 4,,10
	.p2align 3
.L287:
	testb	%r9b, %r9b
	jne	.L257
	testb	%r11b, %r11b
	jne	.L278
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L281:
	movq	88(%rbx), %rdx
	movswq	%ax, %rax
	movslq	(%rdx,%rax,4), %rax
	jmp	.L232
	.cfi_endproc
.LFE3247:
	.size	_ZNK6icu_6713OlsonTimeZone19getHistoricalOffsetEdaiiRiS1_, .-_ZNK6icu_6713OlsonTimeZone19getHistoricalOffsetEdaiiRiS1_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713OlsonTimeZone9getOffsetEhiiihiR10UErrorCode
	.type	_ZNK6icu_6713OlsonTimeZone9getOffsetEhiiihiR10UErrorCode, @function
_ZNK6icu_6713OlsonTimeZone9getOffsetEhiiihiR10UErrorCode:
.LFB3240:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %r10d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	24(%rbp), %rdx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$11, %ecx
	ja	.L310
	movq	(%rdi), %rax
	movq	%rdi, %r12
	movq	40(%rax), %r11
	movl	%ecx, %eax
	testb	$3, %r10b
	je	.L311
.L292:
	cltq
	leaq	_ZN6icu_675Grego12MONTH_LENGTHE(%rip), %rdi
	movzbl	%r9b, %ebx
	movzbl	%sil, %r13d
	movsbl	(%rdi,%rax), %edi
	leaq	_ZNK6icu_6713OlsonTimeZone9getOffsetEhiiihiiR10UErrorCode(%rip), %rax
	movl	%edi, %r14d
	cmpq	%rax, %r11
	jne	.L295
	movl	(%rdx), %r11d
	xorl	%eax, %eax
	testl	%r11d, %r11d
	jg	.L289
	cmpb	$1, %sil
	ja	.L296
	testl	%r8d, %r8d
	jle	.L296
	leal	-1(%r9), %eax
	cmpb	$6, %al
	ja	.L296
	cmpl	%r8d, %edi
	jl	.L296
	subl	$28, %r14d
	cmpb	$3, %r14b
	ja	.L296
	cmpl	$86399999, 16(%rbp)
	ja	.L296
	movl	%r10d, %eax
	movq	128(%r12), %r11
	negl	%eax
	testb	%sil, %sil
	cmove	%eax, %r10d
	testq	%r11, %r11
	je	.L301
	cmpl	%r10d, 144(%r12)
	jg	.L301
	movl	16(%rbp), %esi
	subq	$8, %rsp
	movq	(%r11), %rax
	movl	%ebx, %r9d
	pushq	%rdx
	movl	%r10d, %edx
	pushq	%rdi
	movq	%r11, %rdi
	pushq	%rsi
	movl	%r13d, %esi
	call	*40(%rax)
	addq	$32, %rsp
	.p2align 4,,10
	.p2align 3
.L289:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L312
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L311:
	.cfi_restore_state
	imull	$-1030792151, %r10d, %eax
	addl	$85899344, %eax
	movl	%eax, %edi
	rorl	$2, %edi
	cmpl	$42949672, %edi
	ja	.L294
	rorl	$4, %eax
	cmpl	$10737418, %eax
	jbe	.L294
	movl	%ecx, %eax
	jmp	.L292
	.p2align 4,,10
	.p2align 3
.L294:
	leal	12(%rcx), %eax
	jmp	.L292
	.p2align 4,,10
	.p2align 3
.L296:
	movl	$1, (%rdx)
	xorl	%eax, %eax
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L310:
	movl	(%rdx), %ebx
	testl	%ebx, %ebx
	jg	.L289
	movl	$1, (%rdx)
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L295:
	movl	16(%rbp), %eax
	subq	$8, %rsp
	movl	%ebx, %r9d
	movl	%r13d, %esi
	pushq	%rdx
	movl	%r10d, %edx
	pushq	%rdi
	movq	%r12, %rdi
	pushq	%rax
	call	*%r11
	addq	$32, %rsp
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L301:
	movl	%r8d, %edx
	movl	%ecx, %esi
	movl	%r10d, %edi
	call	_ZN6icu_675Grego11fieldsToDayEiii@PLT
	pxor	%xmm1, %xmm1
	leaq	-44(%rbp), %r9
	movq	%r12, %rdi
	cvtsi2sdl	16(%rbp), %xmm1
	leaq	-48(%rbp), %r8
	movl	$1, %ecx
	movl	$3, %edx
	mulsd	.LC10(%rip), %xmm0
	movl	$1, %esi
	addsd	%xmm1, %xmm0
	call	_ZNK6icu_6713OlsonTimeZone19getHistoricalOffsetEdaiiRiS1_
	movl	-44(%rbp), %eax
	addl	-48(%rbp), %eax
	jmp	.L289
.L312:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3240:
	.size	_ZNK6icu_6713OlsonTimeZone9getOffsetEhiiihiR10UErrorCode, .-_ZNK6icu_6713OlsonTimeZone9getOffsetEhiiihiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713OlsonTimeZone9getOffsetEhiiihiiR10UErrorCode
	.type	_ZNK6icu_6713OlsonTimeZone9getOffsetEhiiihiiR10UErrorCode, @function
_ZNK6icu_6713OlsonTimeZone9getOffsetEhiiihiiR10UErrorCode:
.LFB3241:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 12, -24
	movq	32(%rbp), %r10
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	(%r10), %r11d
	testl	%r11d, %r11d
	jg	.L313
	cmpb	$1, %sil
	ja	.L315
	testl	%r8d, %r8d
	jle	.L315
	cmpl	$11, %ecx
	ja	.L315
	leal	-1(%r9), %eax
	cmpb	$6, %al
	ja	.L315
	cmpl	24(%rbp), %r8d
	jg	.L315
	movl	24(%rbp), %eax
	subl	$28, %eax
	cmpl	$3, %eax
	ja	.L315
	cmpl	$86399999, 16(%rbp)
	ja	.L315
	movq	%rdi, %r12
	movl	%edx, %eax
	movl	%edx, %edi
	movq	128(%r12), %r11
	negl	%eax
	testb	%sil, %sil
	cmove	%eax, %edi
	testq	%r11, %r11
	je	.L320
	cmpl	%edi, 144(%r12)
	jg	.L320
	movl	24(%rbp), %edx
	subq	$8, %rsp
	movq	(%r11), %rax
	movzbl	%sil, %esi
	pushq	%r10
	movzbl	%r9b, %r9d
	pushq	%rdx
	movl	16(%rbp), %edx
	pushq	%rdx
	movl	%edi, %edx
	movq	%r11, %rdi
	call	*40(%rax)
	addq	$32, %rsp
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L315:
	movl	$1, (%r10)
	xorl	%eax, %eax
.L313:
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L327
	movq	-8(%rbp), %r12
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L320:
	.cfi_restore_state
	movl	%r8d, %edx
	movl	%ecx, %esi
	call	_ZN6icu_675Grego11fieldsToDayEiii@PLT
	leaq	-28(%rbp), %r9
	leaq	-32(%rbp), %r8
	movq	%r12, %rdi
	movl	$1, %ecx
	movl	$3, %edx
	movl	$1, %esi
	movsd	.LC10(%rip), %xmm1
	mulsd	%xmm0, %xmm1
	pxor	%xmm0, %xmm0
	cvtsi2sdl	16(%rbp), %xmm0
	addsd	%xmm1, %xmm0
	call	_ZNK6icu_6713OlsonTimeZone19getHistoricalOffsetEdaiiRiS1_
	movl	-28(%rbp), %eax
	addl	-32(%rbp), %eax
	jmp	.L313
.L327:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3241:
	.size	_ZNK6icu_6713OlsonTimeZone9getOffsetEhiiihiiR10UErrorCode, .-_ZNK6icu_6713OlsonTimeZone9getOffsetEhiiihiiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713OlsonTimeZone20clearTransitionRulesEv
	.type	_ZN6icu_6713OlsonTimeZone20clearTransitionRulesEv, @function
_ZN6icu_6713OlsonTimeZone20clearTransitionRulesEv:
.LFB3252:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	xorl	%edx, %edx
	pxor	%xmm0, %xmm0
	movq	$0, 208(%rdi)
	movw	%ax, 200(%rdi)
	movw	%dx, 176(%rdi)
	movups	%xmm0, 160(%rdi)
	movups	%xmm0, 184(%rdi)
	movl	$0, 216(%rdi)
	mfence
	ret
	.cfi_endproc
.LFE3252:
	.size	_ZN6icu_6713OlsonTimeZone20clearTransitionRulesEv, .-_ZN6icu_6713OlsonTimeZone20clearTransitionRulesEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713OlsonTimeZone21deleteTransitionRulesEv
	.type	_ZN6icu_6713OlsonTimeZone21deleteTransitionRulesEv, @function
_ZN6icu_6713OlsonTimeZone21deleteTransitionRulesEv:
.LFB3253:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	160(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L330
	movq	(%rdi), %rax
	call	*8(%rax)
.L330:
	movq	168(%r12), %rdi
	testq	%rdi, %rdi
	je	.L331
	movq	(%rdi), %rax
	call	*8(%rax)
.L331:
	movq	184(%r12), %rdi
	testq	%rdi, %rdi
	je	.L332
	movq	(%rdi), %rax
	call	*8(%rax)
.L332:
	movq	208(%r12), %rdi
	testq	%rdi, %rdi
	je	.L333
	movq	(%rdi), %rax
	call	*8(%rax)
.L333:
	movq	192(%r12), %r8
	testq	%r8, %r8
	je	.L334
	movzwl	200(%r12), %eax
	testw	%ax, %ax
	jle	.L335
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L339:
	movq	(%r8,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.L336
	movq	(%rdi), %rax
	addq	$1, %rbx
	call	*8(%rax)
	movq	192(%r12), %r8
	movswl	200(%r12), %edx
	movl	%edx, %eax
	cmpl	%ebx, %edx
	jg	.L339
.L335:
	movq	%r8, %rdi
	call	uprv_free_67@PLT
.L334:
	xorl	%eax, %eax
	xorl	%edx, %edx
	pxor	%xmm0, %xmm0
	movq	$0, 208(%r12)
	movw	%ax, 200(%r12)
	movw	%dx, 176(%r12)
	movups	%xmm0, 160(%r12)
	movups	%xmm0, 184(%r12)
	movl	$0, 216(%r12)
	mfence
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L336:
	.cfi_restore_state
	addq	$1, %rbx
	movswl	%ax, %edx
	cmpl	%ebx, %edx
	jg	.L339
	jmp	.L335
	.cfi_endproc
.LFE3253:
	.size	_ZN6icu_6713OlsonTimeZone21deleteTransitionRulesEv, .-_ZN6icu_6713OlsonTimeZone21deleteTransitionRulesEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713OlsonTimeZoneD2Ev
	.type	_ZN6icu_6713OlsonTimeZoneD2Ev, @function
_ZN6icu_6713OlsonTimeZoneD2Ev:
.LFB3235:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6713OlsonTimeZoneE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6713OlsonTimeZone21deleteTransitionRulesEv
	movq	128(%r12), %rdi
	testq	%rdi, %rdi
	je	.L357
	movq	(%rdi), %rax
	call	*8(%rax)
.L357:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6713BasicTimeZoneD2Ev@PLT
	.cfi_endproc
.LFE3235:
	.size	_ZN6icu_6713OlsonTimeZoneD2Ev, .-_ZN6icu_6713OlsonTimeZoneD2Ev
	.globl	_ZN6icu_6713OlsonTimeZoneD1Ev
	.set	_ZN6icu_6713OlsonTimeZoneD1Ev,_ZN6icu_6713OlsonTimeZoneD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713OlsonTimeZoneD0Ev
	.type	_ZN6icu_6713OlsonTimeZoneD0Ev, @function
_ZN6icu_6713OlsonTimeZoneD0Ev:
.LFB3237:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6713OlsonTimeZoneE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6713OlsonTimeZone21deleteTransitionRulesEv
	movq	128(%r12), %rdi
	testq	%rdi, %rdi
	je	.L363
	movq	(%rdi), %rax
	call	*8(%rax)
.L363:
	movq	%r12, %rdi
	call	_ZN6icu_6713BasicTimeZoneD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3237:
	.size	_ZN6icu_6713OlsonTimeZoneD0Ev, .-_ZN6icu_6713OlsonTimeZoneD0Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713OlsonTimeZoneeqERKNS_8TimeZoneE
	.type	_ZNK6icu_6713OlsonTimeZoneeqERKNS_8TimeZoneE, @function
_ZNK6icu_6713OlsonTimeZoneeqERKNS_8TimeZoneE:
.LFB3238:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	cmpq	%rsi, %rdi
	je	.L378
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	(%rdi), %rax
	movq	%rdi, %r12
	movq	-8(%rax), %rax
	movq	8(%rax), %rdi
	movq	(%rsi), %rax
	movq	-8(%rax), %rax
	movq	8(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L370
	cmpb	$42, (%rdi)
	je	.L372
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L370
.L372:
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L370:
	.cfi_restore_state
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK6icu_678TimeZoneeqERKS0_@PLT
	testb	%al, %al
	je	.L372
	movq	(%r12), %rax
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*88(%rax)
	popq	%r12
	popq	%r13
	testb	%al, %al
	popq	%rbp
	.cfi_def_cfa 7, 8
	setne	%al
	ret
	.p2align 4,,10
	.p2align 3
.L378:
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE3238:
	.size	_ZNK6icu_6713OlsonTimeZoneeqERKNS_8TimeZoneE, .-_ZNK6icu_6713OlsonTimeZoneeqERKNS_8TimeZoneE
	.section	.text._ZNK6icu_6714SimpleTimeZone9getOffsetEdaRiS1_R10UErrorCode,"axG",@progbits,_ZNK6icu_6714SimpleTimeZone9getOffsetEdaRiS1_R10UErrorCode,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6714SimpleTimeZone9getOffsetEdaRiS1_R10UErrorCode
	.type	_ZNK6icu_6714SimpleTimeZone9getOffsetEdaRiS1_R10UErrorCode, @function
_ZNK6icu_6714SimpleTimeZone9getOffsetEdaRiS1_R10UErrorCode:
.LFB2949:
	.cfi_startproc
	endbr64
	movsbl	%sil, %esi
	jmp	_ZNK6icu_678TimeZone9getOffsetEdaRiS1_R10UErrorCode@PLT
	.cfi_endproc
.LFE2949:
	.size	_ZNK6icu_6714SimpleTimeZone9getOffsetEdaRiS1_R10UErrorCode, .-_ZNK6icu_6714SimpleTimeZone9getOffsetEdaRiS1_R10UErrorCode
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713OlsonTimeZone9getOffsetEdaRiS1_R10UErrorCode
	.type	_ZNK6icu_6713OlsonTimeZone9getOffsetEdaRiS1_R10UErrorCode, @function
_ZNK6icu_6713OlsonTimeZone9getOffsetEdaRiS1_R10UErrorCode:
.LFB3242:
	.cfi_startproc
	endbr64
	movl	(%r8), %eax
	testl	%eax, %eax
	jg	.L413
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%esi, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$16, %rsp
	movq	128(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L384
	comisd	136(%r12), %xmm0
	jb	.L384
	movq	(%rdi), %rax
	leaq	_ZNK6icu_6714SimpleTimeZone9getOffsetEdaRiS1_R10UErrorCode(%rip), %rdx
	movsbl	%sil, %esi
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	movq	%r13, %rdx
	jne	.L386
	addq	$16, %rsp
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_678TimeZone9getOffsetEdaRiS1_R10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L384:
	.cfi_restore_state
	movzwl	74(%r12), %eax
	addw	72(%r12), %ax
	movl	%esi, -40(%rbp)
	addw	76(%r12), %ax
	testw	%ax, %ax
	movl	%eax, -36(%rbp)
	jg	.L417
	movq	112(%r12), %rax
	imull	$1000, (%rax), %edx
	movl	%edx, 0(%r13)
	imull	$1000, 4(%rax), %eax
	movl	%eax, (%r14)
.L382:
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L413:
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L417:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	divsd	.LC1(%rip), %xmm0
	call	uprv_floor_67@PLT
	movl	-40(%rbp), %esi
	movzwl	72(%r12), %r8d
	movl	-36(%rbp), %eax
	testb	%sil, %sil
	jne	.L388
	testw	%r8w, %r8w
	jg	.L418
	movl	%r8d, %edx
	movzwl	74(%r12), %ecx
	negl	%edx
	cmpw	%cx, %dx
	jl	.L419
	subl	%ecx, %edx
	movq	96(%r12), %rsi
	movswl	%dx, %ecx
	addl	%ecx, %ecx
	movslq	%ecx, %rcx
	movl	(%rsi,%rcx,4), %edx
	movl	4(%rsi,%rcx,4), %ecx
	salq	$32, %rdx
	orq	%rcx, %rdx
.L390:
	pxor	%xmm1, %xmm1
	movq	112(%r12), %rcx
	cvtsi2sdq	%rdx, %xmm1
	comisd	%xmm0, %xmm1
	jbe	.L392
	imull	$1000, (%rcx), %eax
	movl	%eax, 0(%r13)
	imull	$1000, 4(%rcx), %eax
	movl	%eax, (%r14)
	jmp	.L382
	.p2align 4,,10
	.p2align 3
.L388:
	movq	112(%r12), %rcx
.L392:
	leal	-1(%rax), %esi
	movl	%esi, %edx
	subl	%r8d, %edx
	jmp	.L401
	.p2align 4,,10
	.p2align 3
.L394:
	movzwl	74(%r12), %eax
	cmpw	%ax, %dx
	jl	.L420
	movl	%edx, %edi
	movq	96(%r12), %r9
	subl	%eax, %edi
	movswl	%di, %edi
.L416:
	addl	%edi, %edi
	movslq	%edi, %rdi
	movl	(%r9,%rdi,4), %eax
	movl	4(%r9,%rdi,4), %edi
	salq	$32, %rax
	orq	%rdi, %rax
.L395:
	testb	%r15b, %r15b
	je	.L397
	leaq	-86400(%rax), %rdi
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%rdi, %xmm1
	comisd	%xmm1, %xmm0
	jb	.L397
	movq	120(%r12), %r9
	movswq	%si, %rdi
	movzbl	(%r9,%rdi), %edi
	addl	%edi, %edi
	movslq	%edi, %rdi
	movl	(%rcx,%rdi,4), %r9d
	addl	4(%rcx,%rdi,4), %r9d
	movslq	%r9d, %rdi
	addq	%rdi, %rax
.L397:
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%rax, %xmm1
	comisd	%xmm1, %xmm0
	jnb	.L399
	subl	$1, %esi
	subl	$1, %edx
	cmpw	$-1, %si
	je	.L421
.L401:
	cmpw	%si, %r8w
	jle	.L394
	movq	80(%r12), %r9
	movswl	%si, %edi
	jmp	.L416
	.p2align 4,,10
	.p2align 3
.L420:
	movq	88(%r12), %rdi
	movswq	%dx, %rax
	movslq	(%rdi,%rax,4), %rax
	jmp	.L395
	.p2align 4,,10
	.p2align 3
.L399:
	movswq	%si, %rax
	addq	120(%r12), %rax
	movzbl	(%rax), %edx
	addl	%edx, %edx
	movslq	%edx, %rdx
	imull	$1000, (%rcx,%rdx,4), %edx
	movl	%edx, 0(%r13)
	movzbl	(%rax), %eax
	addl	%eax, %eax
	cltq
	leaq	4(,%rax,4), %rax
.L402:
	imull	$1000, (%rcx,%rax), %eax
	movl	%eax, (%r14)
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L386:
	.cfi_restore_state
	addq	$16, %rsp
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L418:
	.cfi_restore_state
	movq	80(%r12), %rcx
	movl	(%rcx), %edx
	movl	4(%rcx), %ecx
	salq	$32, %rdx
	orq	%rcx, %rdx
	jmp	.L390
	.p2align 4,,10
	.p2align 3
.L421:
	imull	$1000, (%rcx), %eax
	movl	%eax, 0(%r13)
	movl	$4, %eax
	jmp	.L402
	.p2align 4,,10
	.p2align 3
.L419:
	movq	88(%r12), %rcx
	movswq	%dx, %rdx
	movslq	(%rcx,%rdx,4), %rdx
	jmp	.L390
	.cfi_endproc
.LFE3242:
	.size	_ZNK6icu_6713OlsonTimeZone9getOffsetEdaRiS1_R10UErrorCode, .-_ZNK6icu_6713OlsonTimeZone9getOffsetEdaRiS1_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713OlsonTimeZone12getRawOffsetEv
	.type	_ZNK6icu_6713OlsonTimeZone12getRawOffsetEv, @function
_ZNK6icu_6713OlsonTimeZone12getRawOffsetEv:
.LFB3245:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movl	$0, -36(%rbp)
	movq	48(%rax), %rbx
	call	uprv_getUTCtime_67@PLT
	leaq	_ZNK6icu_6713OlsonTimeZone9getOffsetEdaRiS1_R10UErrorCode(%rip), %rax
	mulsd	.LC1(%rip), %xmm0
	cmpq	%rax, %rbx
	jne	.L423
	movl	-36(%rbp), %eax
	testl	%eax, %eax
	jg	.L424
	movq	128(%r12), %rdi
	testq	%rdi, %rdi
	je	.L425
	comisd	136(%r12), %xmm0
	jb	.L425
	movq	(%rdi), %rax
	leaq	-28(%rbp), %rcx
	leaq	-36(%rbp), %r8
	movl	$0, %esi
	leaq	_ZNK6icu_6714SimpleTimeZone9getOffsetEdaRiS1_R10UErrorCode(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	leaq	-32(%rbp), %rdx
	jne	.L427
	call	_ZNK6icu_678TimeZone9getOffsetEdaRiS1_R10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L424:
	movq	-24(%rbp), %rsi
	xorq	%fs:40, %rsi
	movl	-32(%rbp), %eax
	jne	.L434
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L425:
	.cfi_restore_state
	leaq	-28(%rbp), %r9
	leaq	-32(%rbp), %r8
	movl	$12, %ecx
	xorl	%esi, %esi
	movl	$4, %edx
	movq	%r12, %rdi
	call	_ZNK6icu_6713OlsonTimeZone19getHistoricalOffsetEdaiiRiS1_
	jmp	.L424
	.p2align 4,,10
	.p2align 3
.L423:
	leaq	-28(%rbp), %rcx
	leaq	-32(%rbp), %rdx
	xorl	%esi, %esi
	movq	%r12, %rdi
	leaq	-36(%rbp), %r8
	call	*%rbx
	jmp	.L424
	.p2align 4,,10
	.p2align 3
.L427:
	call	*%rax
	jmp	.L424
.L434:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3245:
	.size	_ZNK6icu_6713OlsonTimeZone12getRawOffsetEv, .-_ZNK6icu_6713OlsonTimeZone12getRawOffsetEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713OlsonTimeZone14inDaylightTimeEdR10UErrorCode
	.type	_ZNK6icu_6713OlsonTimeZone14inDaylightTimeEdR10UErrorCode, @function
_ZNK6icu_6713OlsonTimeZone14inDaylightTimeEdR10UErrorCode:
.LFB3250:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZNK6icu_6713OlsonTimeZone9getOffsetEdaRiS1_R10UErrorCode(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L436
	movl	(%rsi), %edx
	testl	%edx, %edx
	jg	.L437
	movq	128(%rdi), %r9
	testq	%r9, %r9
	je	.L438
	comisd	136(%rdi), %xmm0
	jb	.L438
	movq	(%r9), %rax
	leaq	_ZNK6icu_6714SimpleTimeZone9getOffsetEdaRiS1_R10UErrorCode(%rip), %rdx
	movq	%rsi, %r8
	movq	%r9, %rdi
	leaq	-12(%rbp), %rcx
	movl	$0, %esi
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	leaq	-16(%rbp), %rdx
	jne	.L447
	call	_ZNK6icu_678TimeZone9getOffsetEdaRiS1_R10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L437:
	movl	-12(%rbp), %eax
	testl	%eax, %eax
	setne	%al
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L448
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L438:
	.cfi_restore_state
	leaq	-12(%rbp), %r9
	leaq	-16(%rbp), %r8
	movl	$12, %ecx
	xorl	%esi, %esi
	movl	$4, %edx
	call	_ZNK6icu_6713OlsonTimeZone19getHistoricalOffsetEdaiiRiS1_
	jmp	.L437
	.p2align 4,,10
	.p2align 3
.L436:
	movq	%rsi, %r8
	leaq	-12(%rbp), %rcx
	leaq	-16(%rbp), %rdx
	xorl	%esi, %esi
.L447:
	call	*%rax
	jmp	.L437
.L448:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3250:
	.size	_ZNK6icu_6713OlsonTimeZone14inDaylightTimeEdR10UErrorCode, .-_ZNK6icu_6713OlsonTimeZone14inDaylightTimeEdR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713OlsonTimeZone13getDSTSavingsEv
	.type	_ZNK6icu_6713OlsonTimeZone13getDSTSavingsEv, @function
_ZNK6icu_6713OlsonTimeZone13getDSTSavingsEv:
.LFB3249:
	.cfi_startproc
	endbr64
	movq	128(%rdi), %r8
	testq	%r8, %r8
	je	.L450
	movq	(%r8), %rax
	movq	%r8, %rdi
	movq	104(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L450:
	jmp	_ZNK6icu_678TimeZone13getDSTSavingsEv@PLT
	.cfi_endproc
.LFE3249:
	.size	_ZNK6icu_6713OlsonTimeZone13getDSTSavingsEv, .-_ZNK6icu_6713OlsonTimeZone13getDSTSavingsEv
	.section	.rodata.str2.2,"aMS",@progbits,2
	.align 2
.LC11:
	.string	"("
	.string	"S"
	.string	"T"
	.string	"D"
	.string	")"
	.string	""
	.string	""
	.align 2
.LC12:
	.string	"("
	.string	"D"
	.string	"S"
	.string	"T"
	.string	")"
	.string	""
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713OlsonTimeZone19initTransitionRulesER10UErrorCode
	.type	_ZN6icu_6713OlsonTimeZone19initTransitionRulesER10UErrorCode, @function
_ZN6icu_6713OlsonTimeZone19initTransitionRulesER10UErrorCode:
.LFB3256:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$392, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -408(%rbp)
	movl	(%rsi), %r11d
	movq	%fs:40, %rdi
	movq	%rdi, -56(%rbp)
	xorl	%edi, %edi
	testl	%r11d, %r11d
	jle	.L519
.L451:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L520
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L519:
	.cfi_restore_state
	leaq	-320(%rbp), %rbx
	movq	%r12, %rdi
	leaq	-128(%rbp), %r15
	call	_ZN6icu_6713OlsonTimeZone21deleteTransitionRulesEv
	leaq	-352(%rbp), %r14
	leaq	8(%r12), %rsi
	movq	%rbx, %rdi
	movl	$2, %r10d
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rbx, -416(%rbp)
	movw	%r10w, -312(%rbp)
	movq	%rax, -320(%rbp)
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movl	$-1, %ecx
	movq	%r14, %rdx
	movq	%r15, %rdi
	leaq	.LC11(%rip), %rax
	movl	$1, %esi
	movq	%r14, -432(%rbp)
	movq	%rax, -352(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	leaq	-256(%rbp), %rax
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	%rax, %rdi
	movq	%rax, -392(%rbp)
	call	_ZN6icu_67plERKNS_13UnicodeStringES2_@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC12(%rip), %rsi
	movl	$-1, %ecx
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -352(%rbp)
	leaq	-192(%rbp), %r14
	movl	$1, %esi
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	%r14, -400(%rbp)
	call	_ZN6icu_67plERKNS_13UnicodeStringES2_@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	112(%r12), %rax
	movl	$80, %edi
	imull	$1000, (%rax), %r13d
	imull	$1000, 4(%rax), %r15d
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L453
	testl	%r15d, %r15d
	movq	%r14, %rsi
	movq	%rax, %rdi
	movl	%r15d, %ecx
	cmove	-392(%rbp), %rsi
	movl	%r13d, %edx
	call	_ZN6icu_6719InitialTimeZoneRuleC1ERKNS_13UnicodeStringEii@PLT
	movq	%rbx, 160(%r12)
	movzwl	74(%r12), %ebx
	addw	72(%r12), %bx
	addw	76(%r12), %bx
	movswl	%bx, %eax
	movl	%eax, -420(%rbp)
	testl	%eax, %eax
	jle	.L456
	xorl	%r9d, %r9d
	movq	120(%r12), %rdx
	xorl	%eax, %eax
	movw	%r9w, 176(%r12)
	.p2align 4,,10
	.p2align 3
.L458:
	cmpb	$0, (%rdx)
	jne	.L498
	addl	$1, %eax
	addq	$1, %rdx
	movw	%ax, 176(%r12)
	movl	%eax, %ecx
	cmpw	%bx, %ax
	jne	.L458
	cmpw	%cx, %bx
	jne	.L521
	.p2align 4,,10
	.p2align 3
.L456:
	movq	128(%r12), %rdi
	testq	%rdi, %rdi
	je	.L461
	movsd	136(%r12), %xmm0
	movq	(%rdi), %rax
	movsd	%xmm0, -360(%rbp)
	call	*72(%rax)
	movq	128(%r12), %rdi
	testb	%al, %al
	movq	(%rdi), %rax
	jne	.L522
	call	*96(%rax)
	movq	%rax, 208(%r12)
	testq	%rax, %rax
	je	.L518
	movq	128(%r12), %rax
	movq	-416(%rbp), %r14
	leaq	8(%rax), %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	128(%r12), %rdi
	movq	(%rdi), %rax
	call	*64(%rax)
	movl	$352, %edi
	movl	%eax, -376(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L518
	subq	$8, %rsp
	movl	-376(%rbp), %edx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	pushq	$2
	movl	$1, %r9d
	leaq	-360(%rbp), %r8
	movq	%r14, %rsi
	call	_ZN6icu_6721TimeArrayTimeZoneRuleC1ERKNS_13UnicodeStringEiiPKdiNS_12DateTimeRule12TimeRuleTypeE@PLT
	movl	-420(%rbp), %eax
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	jle	.L492
.L524:
	movq	120(%r12), %rax
	movswq	%bx, %rbx
	movzbl	-1(%rax,%rbx), %edx
	movq	192(%r12), %rax
	movq	(%rax,%rdx,8), %r8
	testq	%r8, %r8
	je	.L492
.L493:
	movl	$32, %edi
	movq	%r8, -376(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L494
	movq	%rax, %rdi
	call	_ZN6icu_6718TimeZoneTransitionC1Ev@PLT
	movq	%rbx, 184(%r12)
	movsd	-360(%rbp), %xmm0
	movq	%rbx, %rdi
	call	_ZN6icu_6718TimeZoneTransition7setTimeEd@PLT
	movq	-376(%rbp), %r8
	movq	184(%r12), %rbx
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*24(%rax)
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_6718TimeZoneTransition9adoptFromEPNS_12TimeZoneRuleE@PLT
	movq	184(%r12), %rdi
	movq	%r15, %rsi
	call	_ZN6icu_6718TimeZoneTransition7adoptToEPNS_12TimeZoneRuleE@PLT
.L461:
	movq	-400(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-392(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-416(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L451
.L522:
	call	*96(%rax)
	movq	%rax, 208(%r12)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L518
	movl	144(%r12), %esi
	call	_ZN6icu_6714SimpleTimeZone12setStartYearEi@PLT
	movq	-432(%rbp), %r15
	movq	%r15, %rdi
	call	_ZN6icu_6718TimeZoneTransitionC1Ev@PLT
	movq	208(%r12), %rdi
	movq	%r15, %rdx
	xorl	%esi, %esi
	movsd	-360(%rbp), %xmm0
	movq	(%rdi), %rax
	call	*112(%rax)
	movq	%r15, %rdi
	call	_ZNK6icu_6718TimeZoneTransition5getToEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L523
	movq	-432(%rbp), %r14
	movq	%r14, %rdi
	call	_ZNK6icu_6718TimeZoneTransition7getTimeEv@PLT
	movq	%r14, %rdi
	movsd	%xmm0, -360(%rbp)
	call	_ZN6icu_6718TimeZoneTransitionD1Ev@PLT
	movl	-420(%rbp), %eax
	testl	%eax, %eax
	jg	.L524
.L492:
	movq	160(%r12), %r8
	jmp	.L493
.L521:
	movswq	%bx, %rdi
	salq	$3, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L518
	movswq	104(%r12), %rdi
	xorl	%r15d, %r15d
	testw	%di, %di
	jle	.L479
	movq	%rax, %r14
	movl	%r15d, %r13d
	.p2align 4,,10
	.p2align 3
.L462:
	movswl	176(%r12), %r10d
	movl	%r10d, %eax
	cmpw	%bx, %r10w
	jge	.L465
	movswq	%r10w, %rdx
	movswl	%r13w, %esi
	addq	120(%r12), %rdx
	xorl	%r9d, %r9d
	jmp	.L472
	.p2align 4,,10
	.p2align 3
.L466:
	addl	$1, %eax
	addq	$1, %rdx
	movswl	%ax, %r10d
	cmpw	%ax, %bx
	je	.L525
.L472:
	movzbl	(%rdx), %ecx
	cmpl	%esi, %ecx
	jne	.L466
	movzwl	72(%r12), %ecx
	cmpw	%ax, %cx
	jg	.L526
	movl	%eax, %r11d
	movzwl	74(%r12), %r10d
	subl	%ecx, %r11d
	movswq	%r11w, %rcx
	cmpw	%r10w, %cx
	jl	.L527
	subl	%r10d, %ecx
	movq	96(%r12), %r8
	movswl	%cx, %ecx
	addl	%ecx, %ecx
	movslq	%ecx, %rcx
	movl	(%r8,%rcx,4), %r10d
	movl	4(%r8,%rcx,4), %ecx
	salq	$32, %r10
	orq	%r10, %rcx
.L468:
	cmpq	$0, 128(%r12)
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rcx, %xmm0
	mulsd	.LC1(%rip), %xmm0
	je	.L470
	movsd	136(%r12), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L466
.L470:
	movslq	%r9d, %rcx
	addl	$1, %eax
	addl	$1, %r9d
	addq	$1, %rdx
	movsd	%xmm0, (%r14,%rcx,8)
	movswl	%ax, %r10d
	cmpw	%ax, %bx
	jne	.L472
.L525:
	testl	%r9d, %r9d
	je	.L465
	movq	112(%r12), %rax
	addl	%esi, %esi
	movslq	%esi, %rsi
	imull	$1000, (%rax,%rsi,4), %r10d
	imull	$1000, 4(%rax,%rsi,4), %ecx
	cmpq	$0, 192(%r12)
	je	.L473
.L477:
	movl	$352, %edi
	movl	%ecx, -384(%rbp)
	movl	%r10d, -380(%rbp)
	movl	%r9d, -376(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movl	-376(%rbp), %r9d
	movl	-380(%rbp), %r10d
	testq	%rax, %rax
	movl	-384(%rbp), %ecx
	movq	%rax, %r15
	je	.L528
	testl	%ecx, %ecx
	movq	-400(%rbp), %rsi
	movq	%r14, %r8
	movl	%r10d, %edx
	cmove	-392(%rbp), %rsi
	subq	$8, %rsp
	movq	%rax, %rdi
	pushq	$2
	call	_ZN6icu_6721TimeArrayTimeZoneRuleC1ERKNS_13UnicodeStringEiiPKdiNS_12DateTimeRule12TimeRuleTypeE@PLT
	movswq	%r13w, %rdx
	movq	192(%r12), %rcx
	movswq	104(%r12), %rdi
	movq	%r15, (%rcx,%rdx,8)
	popq	%rsi
	popq	%r8
.L465:
	addl	$1, %r13d
	cmpw	%di, %r13w
	jl	.L462
	movq	%r14, %r13
.L479:
	movq	%r13, %rdi
	call	uprv_free_67@PLT
	movq	120(%r12), %rcx
	movswq	176(%r12), %rdx
	movzbl	(%rcx,%rdx), %r15d
	movq	%rdx, %rax
	movzwl	72(%r12), %edx
	cmpw	%dx, %ax
	jl	.L529
	subl	%edx, %eax
	movzwl	74(%r12), %edx
	cmpw	%dx, %ax
	jl	.L530
	subl	%edx, %eax
	movq	96(%r12), %rcx
	cwtl
.L517:
	addl	%eax, %eax
	cltq
	movl	(%rcx,%rax,4), %edx
	movl	4(%rcx,%rax,4), %eax
	salq	$32, %rdx
	orq	%rdx, %rax
.L480:
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	cvtsi2sdq	%rax, %xmm0
	mulsd	.LC1(%rip), %xmm0
	movsd	%xmm0, -376(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L482
	movq	192(%r12), %rdx
	movsd	-376(%rbp), %xmm0
	movq	%rax, %rdi
	movq	%rax, -376(%rbp)
	movq	160(%r12), %rsi
	movq	(%rdx,%r15,8), %rdx
	call	_ZN6icu_6718TimeZoneTransitionC1EdRKNS_12TimeZoneRuleES3_@PLT
	movq	-376(%rbp), %rax
	movq	%rax, 168(%r12)
	jmp	.L456
	.p2align 4,,10
	.p2align 3
.L526:
	movq	80(%r12), %r8
	addl	%r10d, %r10d
	movslq	%r10d, %r10
	movl	(%r8,%r10,4), %ecx
	movl	4(%r8,%r10,4), %r8d
	salq	$32, %rcx
	orq	%r8, %rcx
	jmp	.L468
	.p2align 4,,10
	.p2align 3
.L527:
	movq	88(%r12), %r8
	movslq	(%r8,%rcx,4), %rcx
	jmp	.L468
.L473:
	movw	%di, 200(%r12)
	salq	$3, %rdi
	movl	%ecx, -384(%rbp)
	movl	%r10d, -380(%rbp)
	movl	%r9d, -376(%rbp)
	call	uprv_malloc_67@PLT
	movl	-376(%rbp), %r9d
	movl	-380(%rbp), %r10d
	testq	%rax, %rax
	movq	%rax, 192(%r12)
	movl	-384(%rbp), %ecx
	movq	%rax, %rdi
	je	.L531
	movswl	200(%r12), %eax
	testw	%ax, %ax
	jle	.L477
	subl	$1, %eax
	xorl	%esi, %esi
	movl	%ecx, -384(%rbp)
	leaq	8(,%rax,8), %rdx
	movl	%r10d, -380(%rbp)
	movl	%r9d, -376(%rbp)
	call	memset@PLT
	movl	-376(%rbp), %r9d
	movl	-380(%rbp), %r10d
	movl	-384(%rbp), %ecx
	jmp	.L477
.L498:
	movl	%eax, %ecx
	cmpw	%cx, %bx
	je	.L456
	jmp	.L521
.L528:
	movq	192(%r12), %rax
	movswq	%r13w, %r15
	movq	$0, (%rax,%r15,8)
.L518:
	movq	-408(%rbp), %rax
	movq	%r12, %rdi
	movl	$7, (%rax)
	call	_ZN6icu_6713OlsonTimeZone21deleteTransitionRulesEv
	jmp	.L461
.L530:
	movq	88(%r12), %rdx
	movswq	%ax, %rax
	movslq	(%rdx,%rax,4), %rax
	jmp	.L480
.L529:
	movq	80(%r12), %rcx
	jmp	.L517
.L523:
	movq	-408(%rbp), %rax
	movq	%r12, %rdi
	movl	$7, (%rax)
	call	_ZN6icu_6713OlsonTimeZone21deleteTransitionRulesEv
	movq	-432(%rbp), %rdi
	call	_ZN6icu_6718TimeZoneTransitionD1Ev@PLT
	jmp	.L461
.L482:
	movq	-408(%rbp), %rax
	movq	%r12, %rdi
	movq	$0, 168(%r12)
	movl	$7, (%rax)
	call	_ZN6icu_6713OlsonTimeZone21deleteTransitionRulesEv
	jmp	.L461
.L494:
	movq	-408(%rbp), %rax
	movq	%r12, %rdi
	movq	$0, 184(%r12)
	movl	$7, (%rax)
	call	_ZN6icu_6713OlsonTimeZone21deleteTransitionRulesEv
	jmp	.L461
.L520:
	call	__stack_chk_fail@PLT
.L453:
	movq	-408(%rbp), %rax
	movq	%r12, %rdi
	movq	$0, 160(%r12)
	movl	$7, (%rax)
	call	_ZN6icu_6713OlsonTimeZone21deleteTransitionRulesEv
	jmp	.L461
.L531:
	movq	-408(%rbp), %rax
	movq	%r12, %rdi
	movl	$7, (%rax)
	call	_ZN6icu_6713OlsonTimeZone21deleteTransitionRulesEv
	movq	%r14, %rdi
	call	uprv_free_67@PLT
	jmp	.L461
	.cfi_endproc
.LFE3256:
	.size	_ZN6icu_6713OlsonTimeZone19initTransitionRulesER10UErrorCode, .-_ZN6icu_6713OlsonTimeZone19initTransitionRulesER10UErrorCode
	.p2align 4
	.type	_ZN6icu_67L9initRulesEPNS_13OlsonTimeZoneER10UErrorCode, @function
_ZN6icu_67L9initRulesEPNS_13OlsonTimeZoneER10UErrorCode:
.LFB3254:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_6713OlsonTimeZone19initTransitionRulesER10UErrorCode
	.cfi_endproc
.LFE3254:
	.size	_ZN6icu_67L9initRulesEPNS_13OlsonTimeZoneER10UErrorCode, .-_ZN6icu_67L9initRulesEPNS_13OlsonTimeZoneER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713OlsonTimeZone16getTimeZoneRulesERPKNS_19InitialTimeZoneRuleEPPKNS_12TimeZoneRuleERiR10UErrorCode
	.type	_ZNK6icu_6713OlsonTimeZone16getTimeZoneRulesERPKNS_19InitialTimeZoneRuleEPPKNS_12TimeZoneRuleERiR10UErrorCode, @function
_ZNK6icu_6713OlsonTimeZone16getTimeZoneRulesERPKNS_19InitialTimeZoneRuleEPPKNS_12TimeZoneRuleERiR10UErrorCode:
.LFB3260:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r8), %r9d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r9d, %r9d
	jle	.L566
.L533:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L567
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L566:
	.cfi_restore_state
	movl	216(%rdi), %eax
	movq	%r8, %r12
	movq	%rdi, %rbx
	movq	%rsi, %r15
	movq	%rdx, %r13
	movq	%rcx, %r14
	leaq	216(%rdi), %r8
	cmpl	$2, %eax
	jne	.L568
.L536:
	movl	220(%rbx), %eax
	testl	%eax, %eax
	jle	.L537
	movl	%eax, (%r12)
	jmp	.L533
	.p2align 4,,10
	.p2align 3
.L568:
	movq	%r8, %rdi
	movq	%r8, -88(%rbp)
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L536
	movq	%rbx, %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6713OlsonTimeZone19initTransitionRulesER10UErrorCode
	movl	(%r12), %eax
	movq	-88(%rbp), %r8
	movl	%eax, 220(%rbx)
	movq	%r8, %rdi
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L537:
	movl	(%r12), %edx
	testl	%edx, %edx
	jg	.L533
	movq	160(%rbx), %rax
	movq	208(%rbx), %rdi
	movq	%rax, (%r15)
	movq	192(%rbx), %rax
	testq	%rax, %rax
	je	.L548
	movl	(%r14), %esi
	testl	%esi, %esi
	jle	.L549
	movswl	200(%rbx), %edx
	testw	%dx, %dx
	jle	.L541
	subl	$1, %edx
	xorl	%ebx, %ebx
	leaq	8(%rax,%rdx,8), %r8
	.p2align 4,,10
	.p2align 3
.L543:
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L542
	leal	1(%rbx), %ecx
	movq	%rdx, 0(%r13,%rbx,8)
	cmpl	%ecx, %esi
	jle	.L550
	movslq	%ecx, %rbx
.L542:
	addq	$8, %rax
	cmpq	%r8, %rax
	jne	.L543
	jmp	.L539
	.p2align 4,,10
	.p2align 3
.L548:
	xorl	%ebx, %ebx
.L539:
	testq	%rdi, %rdi
	je	.L544
	movl	(%r14), %esi
	cmpl	%ebx, %esi
	jle	.L544
	movslq	%ebx, %rax
	subl	%ebx, %esi
	leaq	0(%r13,%rax,8), %r13
.L546:
	movq	(%rdi), %rax
	movl	%esi, -68(%rbp)
	leaq	-68(%rbp), %rcx
	leaq	-64(%rbp), %rsi
	movq	%r12, %r8
	movq	%r13, %rdx
	call	*144(%rax)
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L533
	addl	-68(%rbp), %ebx
.L544:
	movl	%ebx, (%r14)
	jmp	.L533
	.p2align 4,,10
	.p2align 3
.L549:
	xorl	%ebx, %ebx
	jmp	.L544
	.p2align 4,,10
	.p2align 3
.L550:
	movl	%ecx, %ebx
	jmp	.L544
.L541:
	xorl	%ebx, %ebx
	testq	%rdi, %rdi
	jne	.L546
	jmp	.L544
.L567:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3260:
	.size	_ZNK6icu_6713OlsonTimeZone16getTimeZoneRulesERPKNS_19InitialTimeZoneRuleEPPKNS_12TimeZoneRuleERiR10UErrorCode, .-_ZNK6icu_6713OlsonTimeZone16getTimeZoneRulesERPKNS_19InitialTimeZoneRuleEPPKNS_12TimeZoneRuleERiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713OlsonTimeZone20checkTransitionRulesER10UErrorCode
	.type	_ZNK6icu_6713OlsonTimeZone20checkTransitionRulesER10UErrorCode, @function
_ZNK6icu_6713OlsonTimeZone20checkTransitionRulesER10UErrorCode:
.LFB3255:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jle	.L584
	ret
	.p2align 4,,10
	.p2align 3
.L584:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	216(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	movl	216(%rdi), %eax
	cmpl	$2, %eax
	jne	.L585
.L572:
	movl	220(%r12), %eax
	testl	%eax, %eax
	jle	.L569
	movl	%eax, (%rbx)
.L569:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L585:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L572
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN6icu_6713OlsonTimeZone19initTransitionRulesER10UErrorCode
	movl	(%rbx), %eax
	movq	%r13, %rdi
	movl	%eax, 220(%r12)
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
	.cfi_endproc
.LFE3255:
	.size	_ZNK6icu_6713OlsonTimeZone20checkTransitionRulesER10UErrorCode, .-_ZNK6icu_6713OlsonTimeZone20checkTransitionRulesER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713OlsonTimeZone20countTransitionRulesER10UErrorCode
	.type	_ZNK6icu_6713OlsonTimeZone20countTransitionRulesER10UErrorCode, @function
_ZNK6icu_6713OlsonTimeZone20countTransitionRulesER10UErrorCode:
.LFB3259:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	(%rsi), %edx
	testl	%edx, %edx
	jle	.L587
.L610:
	xorl	%r12d, %r12d
.L586:
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L587:
	.cfi_restore_state
	movl	216(%rdi), %eax
	movq	%rdi, %rbx
	leaq	216(%rdi), %r13
	cmpl	$2, %eax
	jne	.L611
.L589:
	movl	220(%rbx), %eax
	testl	%eax, %eax
	jle	.L590
	movl	%eax, (%rsi)
	jmp	.L610
	.p2align 4,,10
	.p2align 3
.L611:
	movq	%r13, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	movq	-40(%rbp), %rsi
	testb	%al, %al
	je	.L589
	movq	%rbx, %rdi
	call	_ZN6icu_6713OlsonTimeZone19initTransitionRulesER10UErrorCode
	movq	-40(%rbp), %rsi
	movq	%r13, %rdi
	movl	(%rsi), %eax
	movl	%eax, 220(%rbx)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
	movq	-40(%rbp), %rsi
.L590:
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L610
	movq	192(%rbx), %rax
	xorl	%r12d, %r12d
	testq	%rax, %rax
	je	.L592
	movswl	200(%rbx), %edx
	testw	%dx, %dx
	jle	.L592
	subl	$1, %edx
	leaq	8(%rax,%rdx,8), %rdx
	.p2align 4,,10
	.p2align 3
.L594:
	cmpq	$1, (%rax)
	sbbl	$-1, %r12d
	addq	$8, %rax
	cmpq	%rdx, %rax
	jne	.L594
.L592:
	movq	128(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L586
	movq	(%rdi), %rax
	call	*72(%rax)
	movl	%eax, %r8d
	movl	%r12d, %eax
	xorl	%r12d, %r12d
	testb	%r8b, %r8b
	setne	%r12b
	addq	$24, %rsp
	leal	1(%r12,%rax), %r12d
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3259:
	.size	_ZNK6icu_6713OlsonTimeZone20countTransitionRulesER10UErrorCode, .-_ZNK6icu_6713OlsonTimeZone20countTransitionRulesER10UErrorCode
	.align 2
	.p2align 4
	.type	_ZNK6icu_6713OlsonTimeZone21getPreviousTransitionEdaRNS_18TimeZoneTransitionE.part.0, @function
_ZNK6icu_6713OlsonTimeZone21getPreviousTransitionEdaRNS_18TimeZoneTransitionE.part.0:
.LFB4255:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movapd	%xmm0, %xmm1
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$200, %rsp
	movq	%rdx, -216(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 128(%rdi)
	je	.L613
	movq	184(%rdi), %rdi
	movl	%esi, %r13d
	testb	%sil, %sil
	je	.L614
	movsd	%xmm0, -224(%rbp)
	call	_ZNK6icu_6718TimeZoneTransition7getTimeEv@PLT
	movsd	-224(%rbp), %xmm1
	ucomisd	%xmm1, %xmm0
	jnp	.L672
.L650:
	movq	184(%r12), %rdi
.L614:
	movsd	%xmm1, -224(%rbp)
	call	_ZNK6icu_6718TimeZoneTransition7getTimeEv@PLT
	movsd	-224(%rbp), %xmm1
	comisd	%xmm0, %xmm1
	jbe	.L613
	movq	128(%r12), %rdi
	movq	(%rdi), %rax
	call	*72(%rax)
	testb	%al, %al
	je	.L619
	movq	208(%r12), %rdi
	movsd	-224(%rbp), %xmm1
	movsbl	%r13b, %esi
	movq	-216(%rbp), %rdx
	movq	(%rdi), %rax
	movapd	%xmm1, %xmm0
	call	*120(%rax)
.L612:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L673
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L613:
	.cfi_restore_state
	movq	192(%r12), %r8
	xorl	%eax, %eax
	testq	%r8, %r8
	je	.L612
	movzwl	72(%r12), %r10d
	movzwl	74(%r12), %r14d
	movzwl	176(%r12), %r11d
	movzwl	76(%r12), %edx
	leal	(%r10,%r14), %r13d
	leal	-1(%r13,%rdx), %edx
	cmpw	%r11w, %dx
	jl	.L612
	movl	%edx, %esi
	movsd	.LC1(%rip), %xmm2
	xorl	%r15d, %r15d
	subl	%r10d, %esi
	jmp	.L625
	.p2align 4,,10
	.p2align 3
.L620:
	cmpw	%si, %r14w
	jg	.L674
	movl	%edx, %ecx
	movq	96(%r12), %r9
	subl	%r13d, %ecx
	movswl	%cx, %ecx
	addl	%ecx, %ecx
.L671:
	movslq	%ecx, %rcx
	pxor	%xmm0, %xmm0
	movl	(%r9,%rcx,4), %eax
	movl	4(%r9,%rcx,4), %ecx
	salq	$32, %rax
	orq	%rcx, %rax
	cvtsi2sdq	%rax, %xmm0
	mulsd	%xmm2, %xmm0
	comisd	%xmm0, %xmm1
	ja	.L623
.L676:
	testb	%bl, %bl
	je	.L652
	ucomisd	%xmm0, %xmm1
	setnp	%al
	cmovne	%r15d, %eax
	testb	%al, %al
	jne	.L623
.L652:
	subl	$1, %edx
	subl	$1, %esi
	cmpw	%dx, %r11w
	jg	.L675
.L625:
	movswl	%dx, %edi
	cmpw	%dx, %r10w
	jle	.L620
	movq	80(%r12), %r9
	leal	(%rdi,%rdi), %ecx
	jmp	.L671
	.p2align 4,,10
	.p2align 3
.L674:
	movq	88(%r12), %rcx
	movswq	%si, %rax
	pxor	%xmm0, %xmm0
	movslq	(%rcx,%rax,4), %rax
	cvtsi2sdq	%rax, %xmm0
	mulsd	%xmm2, %xmm0
	comisd	%xmm0, %xmm1
	jbe	.L676
	.p2align 4,,10
	.p2align 3
.L623:
	cmpw	%dx, %r11w
	je	.L677
	movq	120(%r12), %rcx
	movswq	%dx, %rax
	movzbl	(%rcx,%rax), %esi
	movzbl	-1(%rcx,%rax), %eax
	movq	(%r8,%rsi,8), %r13
	movq	(%r8,%rax,8), %rbx
	cmpw	%dx, %r10w
	jg	.L678
	subl	%r10d, %edx
	cmpw	%dx, %r14w
	jg	.L679
	subl	%r14d, %edx
	movq	96(%r12), %rcx
	movswl	%dx, %edx
	addl	%edx, %edx
	movslq	%edx, %rdx
	movl	(%rcx,%rdx,4), %eax
	movl	4(%rcx,%rdx,4), %edx
	salq	$32, %rax
	orq	%rdx, %rax
.L627:
	pxor	%xmm0, %xmm0
	movl	$2, %esi
	movl	$2, %edi
	cvtsi2sdq	%rax, %xmm0
	leaq	-192(%rbp), %r14
	movw	%si, -184(%rbp)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%di, -120(%rbp)
	movq	%r14, %rsi
	movq	%rbx, %rdi
	leaq	-128(%rbp), %r15
	movq	%rax, -192(%rbp)
	movq	%rax, -128(%rbp)
	mulsd	%xmm2, %xmm0
	movsd	%xmm0, -224(%rbp)
	call	_ZNK6icu_6712TimeZoneRule7getNameERNS_13UnicodeStringE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZNK6icu_6712TimeZoneRule7getNameERNS_13UnicodeStringE@PLT
	movzwl	-184(%rbp), %eax
	testb	$1, %al
	je	.L629
	movzbl	-120(%rbp), %eax
	andl	$1, %eax
.L630:
	testb	%al, %al
	je	.L635
	movq	%rbx, %rdi
	call	_ZNK6icu_6712TimeZoneRule12getRawOffsetEv@PLT
	movq	%r13, %rdi
	movl	%eax, -228(%rbp)
	call	_ZNK6icu_6712TimeZoneRule12getRawOffsetEv@PLT
	cmpl	%eax, -228(%rbp)
	je	.L680
.L635:
	movq	-216(%rbp), %r12
	movsd	-224(%rbp), %xmm0
	movq	%r12, %rdi
	call	_ZN6icu_6718TimeZoneTransition7setTimeEd@PLT
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*24(%rax)
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_6718TimeZoneTransition9adoptFromEPNS_12TimeZoneRuleE@PLT
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_6718TimeZoneTransition7adoptToEPNS_12TimeZoneRuleE@PLT
	movl	$1, %eax
.L637:
	movq	%r15, %rdi
	movb	%al, -216(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movzbl	-216(%rbp), %eax
	jmp	.L612
	.p2align 4,,10
	.p2align 3
.L678:
	movq	80(%r12), %rdx
	addl	%edi, %edi
	movslq	%edi, %rdi
	movl	(%rdx,%rdi,4), %eax
	movl	4(%rdx,%rdi,4), %edx
	salq	$32, %rax
	orq	%rdx, %rax
	jmp	.L627
	.p2align 4,,10
	.p2align 3
.L672:
	jne	.L650
.L619:
	movq	184(%r12), %rsi
	movq	-216(%rbp), %rdi
	call	_ZN6icu_6718TimeZoneTransitionaSERKS0_@PLT
	movl	$1, %eax
	jmp	.L612
	.p2align 4,,10
	.p2align 3
.L675:
	xorl	%eax, %eax
	jmp	.L612
	.p2align 4,,10
	.p2align 3
.L629:
	testw	%ax, %ax
	js	.L631
	movswl	%ax, %edx
	sarl	$5, %edx
.L632:
	movzwl	-120(%rbp), %ecx
	testw	%cx, %cx
	js	.L633
	movswl	%cx, %eax
	sarl	$5, %eax
.L634:
	cmpl	%edx, %eax
	jne	.L635
	andl	$1, %ecx
	jne	.L635
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%al
	jmp	.L630
	.p2align 4,,10
	.p2align 3
.L677:
	movq	168(%r12), %rsi
	movq	-216(%rbp), %rdi
	call	_ZN6icu_6718TimeZoneTransitionaSERKS0_@PLT
	movl	$1, %eax
	jmp	.L612
	.p2align 4,,10
	.p2align 3
.L679:
	movq	88(%r12), %rax
	movswq	%dx, %rdx
	movslq	(%rax,%rdx,4), %rax
	jmp	.L627
	.p2align 4,,10
	.p2align 3
.L680:
	movq	%rbx, %rdi
	call	_ZNK6icu_6712TimeZoneRule13getDSTSavingsEv@PLT
	movq	%r13, %rdi
	movl	%eax, -228(%rbp)
	call	_ZNK6icu_6712TimeZoneRule13getDSTSavingsEv@PLT
	cmpl	%eax, -228(%rbp)
	jne	.L635
	movq	(%r12), %rax
	leaq	_ZNK6icu_6713OlsonTimeZone21getPreviousTransitionEdaRNS_18TimeZoneTransitionE(%rip), %rdx
	movq	120(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L638
	movl	$0, -196(%rbp)
	movl	216(%r12), %eax
	leaq	216(%r12), %r13
	cmpl	$2, %eax
	je	.L639
	movq	%r13, %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L639
	movq	%r12, %rdi
	leaq	-196(%rbp), %rsi
	call	_ZN6icu_6713OlsonTimeZone19initTransitionRulesER10UErrorCode
	movl	-196(%rbp), %eax
	movq	%r13, %rdi
	movl	%eax, 220(%r12)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L640:
	movl	-196(%rbp), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L637
	movq	-216(%rbp), %rdx
	movsd	-224(%rbp), %xmm0
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_6713OlsonTimeZone21getPreviousTransitionEdaRNS_18TimeZoneTransitionE.part.0
	jmp	.L637
	.p2align 4,,10
	.p2align 3
.L633:
	movl	-116(%rbp), %eax
	jmp	.L634
	.p2align 4,,10
	.p2align 3
.L631:
	movl	-180(%rbp), %edx
	jmp	.L632
.L639:
	movl	220(%r12), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L637
	jmp	.L640
.L638:
	movq	-216(%rbp), %rdx
	movsd	-224(%rbp), %xmm0
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	*%rax
	jmp	.L637
.L673:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4255:
	.size	_ZNK6icu_6713OlsonTimeZone21getPreviousTransitionEdaRNS_18TimeZoneTransitionE.part.0, .-_ZNK6icu_6713OlsonTimeZone21getPreviousTransitionEdaRNS_18TimeZoneTransitionE.part.0
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713OlsonTimeZone21getPreviousTransitionEdaRNS_18TimeZoneTransitionE
	.type	_ZNK6icu_6713OlsonTimeZone21getPreviousTransitionEdaRNS_18TimeZoneTransitionE, @function
_ZNK6icu_6713OlsonTimeZone21getPreviousTransitionEdaRNS_18TimeZoneTransitionE:
.LFB3258:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%esi, %ebx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$0, -44(%rbp)
	movl	216(%rdi), %eax
	cmpl	$2, %eax
	jne	.L694
.L682:
	movl	220(%r12), %ecx
	testl	%ecx, %ecx
	jle	.L683
.L685:
	xorl	%eax, %eax
	jmp	.L681
	.p2align 4,,10
	.p2align 3
.L694:
	leaq	216(%rdi), %r13
	movq	%rdx, -64(%rbp)
	movq	%r13, %rdi
	movsd	%xmm0, -56(%rbp)
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	movsd	-56(%rbp), %xmm0
	movq	-64(%rbp), %rdx
	testb	%al, %al
	je	.L682
	leaq	-44(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713OlsonTimeZone19initTransitionRulesER10UErrorCode
	movl	-44(%rbp), %eax
	movq	%r13, %rdi
	movl	%eax, 220(%r12)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
	movsd	-56(%rbp), %xmm0
	movq	-64(%rbp), %rdx
.L683:
	movl	-44(%rbp), %eax
	testl	%eax, %eax
	jg	.L685
	movsbl	%bl, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_6713OlsonTimeZone21getPreviousTransitionEdaRNS_18TimeZoneTransitionE.part.0
.L681:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L695
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L695:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3258:
	.size	_ZNK6icu_6713OlsonTimeZone21getPreviousTransitionEdaRNS_18TimeZoneTransitionE, .-_ZNK6icu_6713OlsonTimeZone21getPreviousTransitionEdaRNS_18TimeZoneTransitionE
	.align 2
	.p2align 4
	.type	_ZNK6icu_6713OlsonTimeZone17getNextTransitionEdaRNS_18TimeZoneTransitionE.part.0, @function
_ZNK6icu_6713OlsonTimeZone17getNextTransitionEdaRNS_18TimeZoneTransitionE.part.0:
.LFB4254:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movapd	%xmm0, %xmm1
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$200, %rsp
	movq	%rdx, -216(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 128(%rdi)
	je	.L697
	movq	184(%rdi), %rdi
	movl	%esi, %r13d
	testb	%sil, %sil
	je	.L698
	movsd	%xmm0, -224(%rbp)
	call	_ZNK6icu_6718TimeZoneTransition7getTimeEv@PLT
	movsd	-224(%rbp), %xmm1
	ucomisd	%xmm1, %xmm0
	jnp	.L764
.L734:
	movq	184(%r12), %rdi
.L698:
	movsd	%xmm1, -224(%rbp)
	call	_ZNK6icu_6718TimeZoneTransition7getTimeEv@PLT
	movsd	-224(%rbp), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L697
	movq	128(%r12), %rdi
	movq	(%rdi), %rax
	call	*72(%rax)
	movsd	-224(%rbp), %xmm1
	testb	%al, %al
	je	.L704
	movq	208(%r12), %rdi
	movq	-216(%rbp), %rdx
	movsbl	%r13b, %esi
	movapd	%xmm1, %xmm0
	movq	(%rdi), %rax
	call	*112(%rax)
.L696:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L765
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L697:
	.cfi_restore_state
	movq	192(%r12), %r8
	testq	%r8, %r8
	je	.L704
	movzwl	72(%r12), %r9d
	movzwl	74(%r12), %r14d
	movzwl	176(%r12), %r11d
	movzwl	76(%r12), %eax
	leal	(%r9,%r14), %r13d
	addl	%r13d, %eax
	leal	-1(%rax), %edx
	movw	%ax, -224(%rbp)
	cmpw	%r11w, %dx
	jl	.L705
	movl	%r9d, %esi
	movsd	.LC1(%rip), %xmm2
	xorl	%r15d, %r15d
	notl	%esi
	addl	%eax, %esi
	jmp	.L711
	.p2align 4,,10
	.p2align 3
.L706:
	cmpw	%si, %r14w
	jg	.L766
	movl	%edx, %ecx
	movq	96(%r12), %r10
	subl	%r13d, %ecx
	movswl	%cx, %ecx
	addl	%ecx, %ecx
.L761:
	movslq	%ecx, %rcx
	pxor	%xmm0, %xmm0
	movl	(%r10,%rcx,4), %eax
	movl	4(%r10,%rcx,4), %ecx
	salq	$32, %rax
	orq	%rcx, %rax
	cvtsi2sdq	%rax, %xmm0
	mulsd	%xmm2, %xmm0
	comisd	%xmm0, %xmm1
	ja	.L709
.L767:
	testb	%bl, %bl
	jne	.L736
	ucomisd	%xmm0, %xmm1
	setnp	%al
	cmovne	%r15d, %eax
	testb	%al, %al
	jne	.L709
.L736:
	subl	$1, %edx
	subl	$1, %esi
	cmpw	%dx, %r11w
	jg	.L705
.L711:
	movswl	%dx, %edi
	cmpw	%dx, %r9w
	jle	.L706
	movq	80(%r12), %r10
	leal	(%rdi,%rdi), %ecx
	jmp	.L761
	.p2align 4,,10
	.p2align 3
.L704:
	xorl	%eax, %eax
	jmp	.L696
	.p2align 4,,10
	.p2align 3
.L766:
	movq	88(%r12), %rcx
	movswq	%si, %rax
	pxor	%xmm0, %xmm0
	movslq	(%rcx,%rax,4), %rax
	cvtsi2sdq	%rax, %xmm0
	mulsd	%xmm2, %xmm0
	comisd	%xmm0, %xmm1
	jbe	.L767
	.p2align 4,,10
	.p2align 3
.L709:
	movswl	-224(%rbp), %eax
	subl	$1, %eax
	cmpl	%eax, %edi
	je	.L728
	movq	120(%r12), %rcx
	movswq	%dx, %rax
	addl	$1, %edx
	movzbl	1(%rcx,%rax), %esi
	movzbl	(%rcx,%rax), %eax
	movq	(%r8,%rsi,8), %r13
	movq	(%r8,%rax,8), %rbx
	cmpw	%dx, %r9w
	jg	.L768
	subl	%r9d, %edx
	cmpw	%dx, %r14w
	jg	.L769
	movq	96(%r12), %rcx
	subl	%r14d, %edx
.L762:
	movswl	%dx, %eax
	addl	%eax, %eax
	cltq
	movl	(%rcx,%rax,4), %edx
	movl	4(%rcx,%rax,4), %eax
	salq	$32, %rdx
	orq	%rdx, %rax
.L713:
	pxor	%xmm0, %xmm0
	movl	$2, %esi
	movl	$2, %edi
	cvtsi2sdq	%rax, %xmm0
	leaq	-192(%rbp), %r14
	movw	%si, -184(%rbp)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%di, -120(%rbp)
	movq	%r14, %rsi
	movq	%rbx, %rdi
	leaq	-128(%rbp), %r15
	movq	%rax, -192(%rbp)
	movq	%rax, -128(%rbp)
	mulsd	%xmm2, %xmm0
	movsd	%xmm0, -224(%rbp)
	call	_ZNK6icu_6712TimeZoneRule7getNameERNS_13UnicodeStringE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZNK6icu_6712TimeZoneRule7getNameERNS_13UnicodeStringE@PLT
	movzwl	-184(%rbp), %eax
	testb	$1, %al
	je	.L715
	movzbl	-120(%rbp), %eax
	andl	$1, %eax
.L716:
	testb	%al, %al
	je	.L721
	movq	%rbx, %rdi
	call	_ZNK6icu_6712TimeZoneRule12getRawOffsetEv@PLT
	movq	%r13, %rdi
	movl	%eax, -228(%rbp)
	call	_ZNK6icu_6712TimeZoneRule12getRawOffsetEv@PLT
	cmpl	%eax, -228(%rbp)
	je	.L770
.L721:
	movq	-216(%rbp), %r12
	movsd	-224(%rbp), %xmm0
	movq	%r12, %rdi
	call	_ZN6icu_6718TimeZoneTransition7setTimeEd@PLT
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*24(%rax)
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_6718TimeZoneTransition9adoptFromEPNS_12TimeZoneRuleE@PLT
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_6718TimeZoneTransition7adoptToEPNS_12TimeZoneRuleE@PLT
	movl	$1, %eax
.L723:
	movq	%r15, %rdi
	movb	%al, -216(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movzbl	-216(%rbp), %eax
	jmp	.L696
	.p2align 4,,10
	.p2align 3
.L728:
	movq	184(%r12), %rsi
	testq	%rsi, %rsi
	je	.L704
.L763:
	movq	-216(%rbp), %rdi
	call	_ZN6icu_6718TimeZoneTransitionaSERKS0_@PLT
	movl	$1, %eax
	jmp	.L696
	.p2align 4,,10
	.p2align 3
.L705:
	movswl	-224(%rbp), %eax
	movswl	%dx, %edx
	subl	$1, %eax
	cmpl	%edx, %eax
	je	.L728
	movq	168(%r12), %rsi
	jmp	.L763
	.p2align 4,,10
	.p2align 3
.L715:
	testw	%ax, %ax
	js	.L717
	movswl	%ax, %edx
	sarl	$5, %edx
.L718:
	movzwl	-120(%rbp), %eax
	testw	%ax, %ax
	js	.L719
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L720:
	testb	$1, %al
	jne	.L721
	cmpl	%edx, %ecx
	jne	.L721
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%al
	jmp	.L716
	.p2align 4,,10
	.p2align 3
.L764:
	jne	.L734
	movq	184(%r12), %rsi
	movq	-216(%rbp), %rdi
	call	_ZN6icu_6718TimeZoneTransitionaSERKS0_@PLT
	movl	$1, %eax
	jmp	.L696
	.p2align 4,,10
	.p2align 3
.L768:
	movq	80(%r12), %rcx
	jmp	.L762
	.p2align 4,,10
	.p2align 3
.L769:
	movq	88(%r12), %rax
	movswq	%dx, %rdx
	movslq	(%rax,%rdx,4), %rax
	jmp	.L713
	.p2align 4,,10
	.p2align 3
.L770:
	movq	%rbx, %rdi
	call	_ZNK6icu_6712TimeZoneRule13getDSTSavingsEv@PLT
	movq	%r13, %rdi
	movl	%eax, -228(%rbp)
	call	_ZNK6icu_6712TimeZoneRule13getDSTSavingsEv@PLT
	cmpl	%eax, -228(%rbp)
	jne	.L721
	movq	(%r12), %rax
	leaq	_ZNK6icu_6713OlsonTimeZone17getNextTransitionEdaRNS_18TimeZoneTransitionE(%rip), %rdx
	movq	112(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L724
	movl	$0, -196(%rbp)
	movl	216(%r12), %eax
	leaq	216(%r12), %r13
	cmpl	$2, %eax
	je	.L725
	movq	%r13, %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L725
	movq	%r12, %rdi
	leaq	-196(%rbp), %rsi
	call	_ZN6icu_6713OlsonTimeZone19initTransitionRulesER10UErrorCode
	movl	-196(%rbp), %eax
	movq	%r13, %rdi
	movl	%eax, 220(%r12)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L726:
	movl	-196(%rbp), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L723
	movq	-216(%rbp), %rdx
	movsd	-224(%rbp), %xmm0
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_6713OlsonTimeZone17getNextTransitionEdaRNS_18TimeZoneTransitionE.part.0
	jmp	.L723
.L717:
	movl	-180(%rbp), %edx
	jmp	.L718
.L719:
	movl	-116(%rbp), %ecx
	jmp	.L720
.L725:
	movl	220(%r12), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L723
	jmp	.L726
.L724:
	movq	-216(%rbp), %rdx
	movsd	-224(%rbp), %xmm0
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	*%rax
	jmp	.L723
.L765:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4254:
	.size	_ZNK6icu_6713OlsonTimeZone17getNextTransitionEdaRNS_18TimeZoneTransitionE.part.0, .-_ZNK6icu_6713OlsonTimeZone17getNextTransitionEdaRNS_18TimeZoneTransitionE.part.0
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713OlsonTimeZone17getNextTransitionEdaRNS_18TimeZoneTransitionE
	.type	_ZNK6icu_6713OlsonTimeZone17getNextTransitionEdaRNS_18TimeZoneTransitionE, @function
_ZNK6icu_6713OlsonTimeZone17getNextTransitionEdaRNS_18TimeZoneTransitionE:
.LFB3257:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%esi, %ebx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$0, -44(%rbp)
	movl	216(%rdi), %eax
	cmpl	$2, %eax
	jne	.L784
.L772:
	movl	220(%r12), %ecx
	testl	%ecx, %ecx
	jle	.L773
.L775:
	xorl	%eax, %eax
	jmp	.L771
	.p2align 4,,10
	.p2align 3
.L784:
	leaq	216(%rdi), %r13
	movq	%rdx, -64(%rbp)
	movq	%r13, %rdi
	movsd	%xmm0, -56(%rbp)
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	movsd	-56(%rbp), %xmm0
	movq	-64(%rbp), %rdx
	testb	%al, %al
	je	.L772
	leaq	-44(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713OlsonTimeZone19initTransitionRulesER10UErrorCode
	movl	-44(%rbp), %eax
	movq	%r13, %rdi
	movl	%eax, 220(%r12)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
	movsd	-56(%rbp), %xmm0
	movq	-64(%rbp), %rdx
.L773:
	movl	-44(%rbp), %eax
	testl	%eax, %eax
	jg	.L775
	movsbl	%bl, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_6713OlsonTimeZone17getNextTransitionEdaRNS_18TimeZoneTransitionE.part.0
.L771:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L785
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L785:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3257:
	.size	_ZNK6icu_6713OlsonTimeZone17getNextTransitionEdaRNS_18TimeZoneTransitionE, .-_ZNK6icu_6713OlsonTimeZone17getNextTransitionEdaRNS_18TimeZoneTransitionE
	.weak	_ZTSN6icu_6713OlsonTimeZoneE
	.section	.rodata._ZTSN6icu_6713OlsonTimeZoneE,"aG",@progbits,_ZTSN6icu_6713OlsonTimeZoneE,comdat
	.align 16
	.type	_ZTSN6icu_6713OlsonTimeZoneE, @object
	.size	_ZTSN6icu_6713OlsonTimeZoneE, 25
_ZTSN6icu_6713OlsonTimeZoneE:
	.string	"N6icu_6713OlsonTimeZoneE"
	.weak	_ZTIN6icu_6713OlsonTimeZoneE
	.section	.data.rel.ro._ZTIN6icu_6713OlsonTimeZoneE,"awG",@progbits,_ZTIN6icu_6713OlsonTimeZoneE,comdat
	.align 8
	.type	_ZTIN6icu_6713OlsonTimeZoneE, @object
	.size	_ZTIN6icu_6713OlsonTimeZoneE, 24
_ZTIN6icu_6713OlsonTimeZoneE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6713OlsonTimeZoneE
	.quad	_ZTIN6icu_6713BasicTimeZoneE
	.weak	_ZTVN6icu_6713OlsonTimeZoneE
	.section	.data.rel.ro._ZTVN6icu_6713OlsonTimeZoneE,"awG",@progbits,_ZTVN6icu_6713OlsonTimeZoneE,comdat
	.align 8
	.type	_ZTVN6icu_6713OlsonTimeZoneE, @object
	.size	_ZTVN6icu_6713OlsonTimeZoneE, 184
_ZTVN6icu_6713OlsonTimeZoneE:
	.quad	0
	.quad	_ZTIN6icu_6713OlsonTimeZoneE
	.quad	_ZN6icu_6713OlsonTimeZoneD1Ev
	.quad	_ZN6icu_6713OlsonTimeZoneD0Ev
	.quad	_ZNK6icu_6713OlsonTimeZone17getDynamicClassIDEv
	.quad	_ZNK6icu_6713OlsonTimeZoneeqERKNS_8TimeZoneE
	.quad	_ZNK6icu_6713OlsonTimeZone9getOffsetEhiiihiR10UErrorCode
	.quad	_ZNK6icu_6713OlsonTimeZone9getOffsetEhiiihiiR10UErrorCode
	.quad	_ZNK6icu_6713OlsonTimeZone9getOffsetEdaRiS1_R10UErrorCode
	.quad	_ZN6icu_6713OlsonTimeZone12setRawOffsetEi
	.quad	_ZNK6icu_6713OlsonTimeZone12getRawOffsetEv
	.quad	_ZNK6icu_6713OlsonTimeZone15useDaylightTimeEv
	.quad	_ZNK6icu_6713OlsonTimeZone14inDaylightTimeEdR10UErrorCode
	.quad	_ZNK6icu_6713OlsonTimeZone12hasSameRulesERKNS_8TimeZoneE
	.quad	_ZNK6icu_6713OlsonTimeZone5cloneEv
	.quad	_ZNK6icu_6713OlsonTimeZone13getDSTSavingsEv
	.quad	_ZNK6icu_6713OlsonTimeZone17getNextTransitionEdaRNS_18TimeZoneTransitionE
	.quad	_ZNK6icu_6713OlsonTimeZone21getPreviousTransitionEdaRNS_18TimeZoneTransitionE
	.quad	_ZNK6icu_6713BasicTimeZone24hasEquivalentTransitionsERKS0_ddaR10UErrorCode
	.quad	_ZNK6icu_6713OlsonTimeZone20countTransitionRulesER10UErrorCode
	.quad	_ZNK6icu_6713OlsonTimeZone16getTimeZoneRulesERPKNS_19InitialTimeZoneRuleEPPKNS_12TimeZoneRuleERiR10UErrorCode
	.quad	_ZNK6icu_6713BasicTimeZone18getSimpleRulesNearEdRPNS_19InitialTimeZoneRuleERPNS_18AnnualTimeZoneRuleES6_R10UErrorCode
	.quad	_ZNK6icu_6713OlsonTimeZone18getOffsetFromLocalEdiiRiS1_R10UErrorCode
	.local	_ZZN6icu_6713OlsonTimeZone16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6713OlsonTimeZone16getStaticClassIDEvE7classID,1,1
	.section	.rodata
	.align 8
	.type	_ZN6icu_67L5ZEROSE, @object
	.size	_ZN6icu_67L5ZEROSE, 8
_ZN6icu_67L5ZEROSE:
	.zero	8
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	1089804288
	.align 8
.LC1:
	.long	0
	.long	1083129856
	.align 8
.LC10:
	.long	0
	.long	1100257648
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
