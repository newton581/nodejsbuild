	.file	"basictz.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713BasicTimeZone18getOffsetFromLocalEdiiRiS1_R10UErrorCode
	.type	_ZNK6icu_6713BasicTimeZone18getOffsetFromLocalEdiiRiS1_R10UErrorCode, @function
_ZNK6icu_6713BasicTimeZone18getOffsetFromLocalEdiiRiS1_R10UErrorCode:
.LFB2404:
	.cfi_startproc
	endbr64
	movl	(%r9), %eax
	testl	%eax, %eax
	jg	.L1
	movl	$16, (%r9)
.L1:
	ret
	.cfi_endproc
.LFE2404:
	.size	_ZNK6icu_6713BasicTimeZone18getOffsetFromLocalEdiiRiS1_R10UErrorCode, .-_ZNK6icu_6713BasicTimeZone18getOffsetFromLocalEdiiRiS1_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713BasicTimeZoneC2Ev
	.type	_ZN6icu_6713BasicTimeZoneC2Ev, @function
_ZN6icu_6713BasicTimeZoneC2Ev:
.LFB2389:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_678TimeZoneC2Ev@PLT
	leaq	16+_ZTVN6icu_6713BasicTimeZoneE(%rip), %rax
	movq	%rax, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2389:
	.size	_ZN6icu_6713BasicTimeZoneC2Ev, .-_ZN6icu_6713BasicTimeZoneC2Ev
	.globl	_ZN6icu_6713BasicTimeZoneC1Ev
	.set	_ZN6icu_6713BasicTimeZoneC1Ev,_ZN6icu_6713BasicTimeZoneC2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713BasicTimeZoneC2ERKNS_13UnicodeStringE
	.type	_ZN6icu_6713BasicTimeZoneC2ERKNS_13UnicodeStringE, @function
_ZN6icu_6713BasicTimeZoneC2ERKNS_13UnicodeStringE:
.LFB2392:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_678TimeZoneC2ERKNS_13UnicodeStringE@PLT
	leaq	16+_ZTVN6icu_6713BasicTimeZoneE(%rip), %rax
	movq	%rax, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2392:
	.size	_ZN6icu_6713BasicTimeZoneC2ERKNS_13UnicodeStringE, .-_ZN6icu_6713BasicTimeZoneC2ERKNS_13UnicodeStringE
	.globl	_ZN6icu_6713BasicTimeZoneC1ERKNS_13UnicodeStringE
	.set	_ZN6icu_6713BasicTimeZoneC1ERKNS_13UnicodeStringE,_ZN6icu_6713BasicTimeZoneC2ERKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713BasicTimeZoneC2ERKS0_
	.type	_ZN6icu_6713BasicTimeZoneC2ERKS0_, @function
_ZN6icu_6713BasicTimeZoneC2ERKS0_:
.LFB2395:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_678TimeZoneC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6713BasicTimeZoneE(%rip), %rax
	movq	%rax, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2395:
	.size	_ZN6icu_6713BasicTimeZoneC2ERKS0_, .-_ZN6icu_6713BasicTimeZoneC2ERKS0_
	.globl	_ZN6icu_6713BasicTimeZoneC1ERKS0_
	.set	_ZN6icu_6713BasicTimeZoneC1ERKS0_,_ZN6icu_6713BasicTimeZoneC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713BasicTimeZoneD2Ev
	.type	_ZN6icu_6713BasicTimeZoneD2Ev, @function
_ZN6icu_6713BasicTimeZoneD2Ev:
.LFB2398:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6713BasicTimeZoneE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_678TimeZoneD2Ev@PLT
	.cfi_endproc
.LFE2398:
	.size	_ZN6icu_6713BasicTimeZoneD2Ev, .-_ZN6icu_6713BasicTimeZoneD2Ev
	.globl	_ZN6icu_6713BasicTimeZoneD1Ev
	.set	_ZN6icu_6713BasicTimeZoneD1Ev,_ZN6icu_6713BasicTimeZoneD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713BasicTimeZoneD0Ev
	.type	_ZN6icu_6713BasicTimeZoneD0Ev, @function
_ZN6icu_6713BasicTimeZoneD0Ev:
.LFB2400:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6713BasicTimeZoneE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_678TimeZoneD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2400:
	.size	_ZN6icu_6713BasicTimeZoneD0Ev, .-_ZN6icu_6713BasicTimeZoneD0Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713BasicTimeZone24hasEquivalentTransitionsERKS0_ddaR10UErrorCode
	.type	_ZNK6icu_6713BasicTimeZone24hasEquivalentTransitionsERKS0_ddaR10UErrorCode, @function
_ZNK6icu_6713BasicTimeZone24hasEquivalentTransitionsERKS0_ddaR10UErrorCode:
.LFB2401:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movb	%dl, -165(%rbp)
	movl	(%rcx), %r8d
	movsd	%xmm0, -160(%rbp)
	movsd	%xmm1, -152(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L13
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	movq	%rsi, %r14
	movl	%edx, %r12d
	movq	%rcx, %r13
	call	*88(%rax)
	movl	%eax, %r15d
	testb	%al, %al
	jne	.L49
	movq	(%rbx), %rax
	leaq	-144(%rbp), %rdx
	movq	%r13, %r8
	xorl	%esi, %esi
	movsd	-160(%rbp), %xmm0
	leaq	-136(%rbp), %rcx
	movq	%rbx, %rdi
	call	*48(%rax)
	movl	0(%r13), %edx
	testl	%edx, %edx
	jg	.L13
	movq	(%r14), %rax
	movsd	-160(%rbp), %xmm0
	movq	%r13, %r8
	xorl	%esi, %esi
	leaq	-132(%rbp), %rcx
	leaq	-140(%rbp), %rdx
	movq	%r14, %rdi
	call	*48(%rax)
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L13
	testb	%r12b, %r12b
	movl	-144(%rbp), %edx
	movl	-140(%rbp), %eax
	movsd	-160(%rbp), %xmm0
	je	.L18
	movl	-136(%rbp), %ecx
	movl	-132(%rbp), %esi
	addl	%ecx, %edx
	addl	%esi, %eax
	cmpl	%eax, %edx
	je	.L79
	.p2align 4,,10
	.p2align 3
.L13:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L80
	addq	$136, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	.cfi_restore_state
	movl	$1, %r15d
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L18:
	cmpl	%eax, %edx
	jne	.L13
	movl	-132(%rbp), %eax
	cmpl	%eax, -136(%rbp)
	jne	.L13
.L20:
	leaq	-128(%rbp), %r12
	leaq	-96(%rbp), %r13
	movsd	%xmm0, -160(%rbp)
	movq	%r12, %rdi
	call	_ZN6icu_6718TimeZoneTransitionC1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6718TimeZoneTransitionC1Ev@PLT
	movsd	-160(%rbp), %xmm0
	.p2align 4,,10
	.p2align 3
.L43:
	movq	(%rbx), %rax
	xorl	%esi, %esi
	movq	%r12, %rdx
	movsd	%xmm0, -160(%rbp)
	movq	%rbx, %rdi
	call	*112(%rax)
	xorl	%esi, %esi
	movsd	-160(%rbp), %xmm0
	movq	%r13, %rdx
	movb	%al, -166(%rbp)
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*112(%rax)
	cmpb	$0, -165(%rbp)
	movb	%al, -167(%rbp)
	jne	.L81
	cmpb	$0, -166(%rbp)
	jne	.L30
	xorl	%eax, %eax
	cmpb	$0, -167(%rbp)
	jne	.L82
.L33:
	xorl	$1, %eax
	movl	%eax, %r15d
.L37:
	movq	%r13, %rdi
	call	_ZN6icu_6718TimeZoneTransitionD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6718TimeZoneTransitionD1Ev@PLT
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L79:
	testl	%ecx, %ecx
	jne	.L83
	testl	%esi, %esi
	je	.L20
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L81:
	cmpb	$0, -166(%rbp)
	jne	.L25
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L84:
	movq	%r12, %rdi
	call	_ZNK6icu_6718TimeZoneTransition7getFromEv@PLT
	movq	%rax, %rdi
	call	_ZNK6icu_6712TimeZoneRule12getRawOffsetEv@PLT
	movq	%r12, %rdi
	movl	%eax, -160(%rbp)
	call	_ZNK6icu_6718TimeZoneTransition7getFromEv@PLT
	movq	%rax, %rdi
	call	_ZNK6icu_6712TimeZoneRule13getDSTSavingsEv@PLT
	movq	%r12, %rdi
	addl	-160(%rbp), %eax
	movl	%eax, -160(%rbp)
	call	_ZNK6icu_6718TimeZoneTransition5getToEv@PLT
	movq	%rax, %rdi
	call	_ZNK6icu_6712TimeZoneRule12getRawOffsetEv@PLT
	movq	%r12, %rdi
	movl	%eax, -164(%rbp)
	call	_ZNK6icu_6718TimeZoneTransition5getToEv@PLT
	movq	%rax, %rdi
	call	_ZNK6icu_6712TimeZoneRule13getDSTSavingsEv@PLT
	addl	-164(%rbp), %eax
	cmpl	%eax, -160(%rbp)
	jne	.L23
	movq	%r12, %rdi
	call	_ZNK6icu_6718TimeZoneTransition7getFromEv@PLT
	movq	%rax, %rdi
	call	_ZNK6icu_6712TimeZoneRule13getDSTSavingsEv@PLT
	testl	%eax, %eax
	je	.L23
	movq	%r12, %rdi
	call	_ZNK6icu_6718TimeZoneTransition5getToEv@PLT
	movq	%rax, %rdi
	call	_ZNK6icu_6712TimeZoneRule13getDSTSavingsEv@PLT
	testl	%eax, %eax
	je	.L22
	movq	(%rbx), %rax
	movq	%r12, %rdi
	movq	112(%rax), %rax
	movq	%rax, -160(%rbp)
	call	_ZNK6icu_6718TimeZoneTransition7getTimeEv@PLT
	movq	%r12, %rdx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	-160(%rbp), %rax
	call	*%rax
.L25:
	movq	%r12, %rdi
	call	_ZNK6icu_6718TimeZoneTransition7getTimeEv@PLT
	movsd	-152(%rbp), %xmm2
	comisd	%xmm0, %xmm2
	jnb	.L84
.L23:
	cmpb	$0, -167(%rbp)
	jne	.L29
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L85:
	movq	%r13, %rdi
	call	_ZNK6icu_6718TimeZoneTransition7getFromEv@PLT
	movq	%rax, %rdi
	call	_ZNK6icu_6712TimeZoneRule12getRawOffsetEv@PLT
	movq	%r13, %rdi
	movl	%eax, -160(%rbp)
	call	_ZNK6icu_6718TimeZoneTransition7getFromEv@PLT
	movq	%rax, %rdi
	call	_ZNK6icu_6712TimeZoneRule13getDSTSavingsEv@PLT
	movq	%r13, %rdi
	addl	-160(%rbp), %eax
	movl	%eax, -160(%rbp)
	call	_ZNK6icu_6718TimeZoneTransition5getToEv@PLT
	movq	%rax, %rdi
	call	_ZNK6icu_6712TimeZoneRule12getRawOffsetEv@PLT
	movq	%r13, %rdi
	movl	%eax, -164(%rbp)
	call	_ZNK6icu_6718TimeZoneTransition5getToEv@PLT
	movq	%rax, %rdi
	call	_ZNK6icu_6712TimeZoneRule13getDSTSavingsEv@PLT
	addl	-164(%rbp), %eax
	cmpl	%eax, -160(%rbp)
	jne	.L27
	movq	%r13, %rdi
	call	_ZNK6icu_6718TimeZoneTransition7getFromEv@PLT
	movq	%rax, %rdi
	call	_ZNK6icu_6712TimeZoneRule13getDSTSavingsEv@PLT
	testl	%eax, %eax
	je	.L27
	movq	%r13, %rdi
	call	_ZNK6icu_6718TimeZoneTransition5getToEv@PLT
	movq	%rax, %rdi
	call	_ZNK6icu_6712TimeZoneRule13getDSTSavingsEv@PLT
	testl	%eax, %eax
	je	.L27
	movq	(%r14), %rax
	movq	%r13, %rdi
	movq	112(%rax), %rax
	movq	%rax, -160(%rbp)
	call	_ZNK6icu_6718TimeZoneTransition7getTimeEv@PLT
	movq	%r13, %rdx
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	-160(%rbp), %rax
	call	*%rax
.L29:
	movq	%r13, %rdi
	call	_ZNK6icu_6718TimeZoneTransition7getTimeEv@PLT
	movsd	-152(%rbp), %xmm3
	comisd	%xmm0, %xmm3
	jnb	.L85
.L27:
	cmpb	$0, -166(%rbp)
	jne	.L44
	movq	%r13, %rdi
	call	_ZNK6icu_6718TimeZoneTransition7getTimeEv@PLT
	movsd	-152(%rbp), %xmm5
	comisd	%xmm0, %xmm5
	setb	%r15b
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L30:
	movq	%r12, %rdi
	call	_ZNK6icu_6718TimeZoneTransition7getTimeEv@PLT
	movsd	-152(%rbp), %xmm4
	comisd	%xmm0, %xmm4
	setnb	%al
	cmpb	$0, -167(%rbp)
	je	.L33
	.p2align 4,,10
	.p2align 3
.L34:
	movq	%r13, %rdi
	movb	%al, -160(%rbp)
	call	_ZNK6icu_6718TimeZoneTransition7getTimeEv@PLT
	movsd	-152(%rbp), %xmm7
	movzbl	-160(%rbp), %eax
	comisd	%xmm0, %xmm7
	jb	.L33
	testb	%al, %al
	je	.L37
	movq	%r12, %rdi
	call	_ZNK6icu_6718TimeZoneTransition7getTimeEv@PLT
	movq	%r13, %rdi
	movsd	%xmm0, -160(%rbp)
	call	_ZNK6icu_6718TimeZoneTransition7getTimeEv@PLT
	movsd	-160(%rbp), %xmm1
	ucomisd	%xmm0, %xmm1
	jp	.L37
	jne	.L37
	cmpb	$0, -165(%rbp)
	movq	%r12, %rdi
	je	.L38
	call	_ZNK6icu_6718TimeZoneTransition5getToEv@PLT
	movq	%rax, %rdi
	call	_ZNK6icu_6712TimeZoneRule12getRawOffsetEv@PLT
	movq	%r12, %rdi
	movl	%eax, -160(%rbp)
	call	_ZNK6icu_6718TimeZoneTransition5getToEv@PLT
	movq	%rax, %rdi
	call	_ZNK6icu_6712TimeZoneRule13getDSTSavingsEv@PLT
	movq	%r13, %rdi
	addl	-160(%rbp), %eax
	movl	%eax, -160(%rbp)
	call	_ZNK6icu_6718TimeZoneTransition5getToEv@PLT
	movq	%rax, %rdi
	call	_ZNK6icu_6712TimeZoneRule12getRawOffsetEv@PLT
	movq	%r13, %rdi
	movl	%eax, -164(%rbp)
	call	_ZNK6icu_6718TimeZoneTransition5getToEv@PLT
	movq	%rax, %rdi
	call	_ZNK6icu_6712TimeZoneRule13getDSTSavingsEv@PLT
	addl	-164(%rbp), %eax
	cmpl	%eax, -160(%rbp)
	jne	.L37
	movq	%r12, %rdi
	call	_ZNK6icu_6718TimeZoneTransition5getToEv@PLT
	movq	%rax, %rdi
	call	_ZNK6icu_6712TimeZoneRule13getDSTSavingsEv@PLT
	testl	%eax, %eax
	je	.L42
	movq	%r13, %rdi
	call	_ZNK6icu_6718TimeZoneTransition5getToEv@PLT
	movq	%rax, %rdi
	call	_ZNK6icu_6712TimeZoneRule13getDSTSavingsEv@PLT
	testl	%eax, %eax
	je	.L37
.L42:
	movq	%r12, %rdi
	call	_ZNK6icu_6718TimeZoneTransition5getToEv@PLT
	movq	%rax, %rdi
	call	_ZNK6icu_6712TimeZoneRule13getDSTSavingsEv@PLT
	testl	%eax, %eax
	je	.L86
.L41:
	movq	%r12, %rdi
	call	_ZNK6icu_6718TimeZoneTransition7getTimeEv@PLT
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L38:
	call	_ZNK6icu_6718TimeZoneTransition5getToEv@PLT
	movq	%rax, %rdi
	call	_ZNK6icu_6712TimeZoneRule12getRawOffsetEv@PLT
	movq	%r13, %rdi
	movl	%eax, -160(%rbp)
	call	_ZNK6icu_6718TimeZoneTransition5getToEv@PLT
	movq	%rax, %rdi
	call	_ZNK6icu_6712TimeZoneRule12getRawOffsetEv@PLT
	cmpl	%eax, -160(%rbp)
	jne	.L37
	movq	%r12, %rdi
	call	_ZNK6icu_6718TimeZoneTransition5getToEv@PLT
	movq	%rax, %rdi
	call	_ZNK6icu_6712TimeZoneRule13getDSTSavingsEv@PLT
	movq	%r13, %rdi
	movl	%eax, -160(%rbp)
	call	_ZNK6icu_6718TimeZoneTransition5getToEv@PLT
	movq	%rax, %rdi
	call	_ZNK6icu_6712TimeZoneRule13getDSTSavingsEv@PLT
	cmpl	%eax, -160(%rbp)
	je	.L41
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L86:
	movq	%r13, %rdi
	call	_ZNK6icu_6718TimeZoneTransition5getToEv@PLT
	movq	%rax, %rdi
	call	_ZNK6icu_6712TimeZoneRule13getDSTSavingsEv@PLT
	testl	%eax, %eax
	je	.L41
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L82:
	movq	%r13, %rdi
	movb	%al, -160(%rbp)
	call	_ZNK6icu_6718TimeZoneTransition7getTimeEv@PLT
	movsd	-152(%rbp), %xmm6
	movzbl	-160(%rbp), %eax
	comisd	%xmm0, %xmm6
	jnb	.L37
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L22:
	cmpb	$0, -167(%rbp)
	jne	.L29
	cmpb	$0, -166(%rbp)
	jne	.L46
	xorl	%eax, %eax
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L44:
	movq	%r12, %rdi
	call	_ZNK6icu_6718TimeZoneTransition7getTimeEv@PLT
	movsd	-152(%rbp), %xmm7
	comisd	%xmm0, %xmm7
	setnb	%al
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L83:
	testl	%esi, %esi
	jne	.L20
	jmp	.L13
.L46:
	movq	%r12, %rdi
	call	_ZNK6icu_6718TimeZoneTransition7getTimeEv@PLT
	movsd	-152(%rbp), %xmm6
	comisd	%xmm0, %xmm6
	setnb	%al
	jmp	.L33
.L80:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2401:
	.size	_ZNK6icu_6713BasicTimeZone24hasEquivalentTransitionsERKS0_ddaR10UErrorCode, .-_ZNK6icu_6713BasicTimeZone24hasEquivalentTransitionsERKS0_ddaR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713BasicTimeZone18getSimpleRulesNearEdRPNS_19InitialTimeZoneRuleERPNS_18AnnualTimeZoneRuleES6_R10UErrorCode
	.type	_ZNK6icu_6713BasicTimeZone18getSimpleRulesNearEdRPNS_19InitialTimeZoneRuleERPNS_18AnnualTimeZoneRuleES6_R10UErrorCode, @function
_ZNK6icu_6713BasicTimeZone18getSimpleRulesNearEdRPNS_19InitialTimeZoneRuleERPNS_18AnnualTimeZoneRuleES6_R10UErrorCode:
.LFB2402:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$360, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -296(%rbp)
	movl	(%r8), %r15d
	movsd	%xmm0, -280(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, (%rsi)
	movq	$0, (%rdx)
	movq	$0, (%rcx)
	testl	%r15d, %r15d
	jle	.L186
.L87:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L187
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L186:
	.cfi_restore_state
	movl	$2, %r14d
	movq	%rdi, %r12
	movq	%rsi, %rbx
	movq	%rcx, %r13
	movw	%r14w, -120(%rbp)
	leaq	-224(%rbp), %r14
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%r8, %r15
	movl	$2, %r11d
	movq	%r14, %rdi
	movq	%rax, -192(%rbp)
	movq	%rax, -128(%rbp)
	movw	%r11w, -184(%rbp)
	call	_ZN6icu_6718TimeZoneTransitionC1Ev@PLT
	movq	(%r12), %rax
	xorl	%esi, %esi
	movq	%r14, %rdx
	movsd	-280(%rbp), %xmm0
	movq	%r12, %rdi
	call	*112(%rax)
	testb	%al, %al
	jne	.L188
	movq	(%r12), %rax
	movq	%r14, %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	movsd	-280(%rbp), %xmm0
	call	*120(%rax)
	testb	%al, %al
	jne	.L189
	movq	(%r12), %rax
	leaq	-260(%rbp), %rcx
	movq	%r15, %r8
	xorl	%esi, %esi
	movsd	-280(%rbp), %xmm0
	leaq	-264(%rbp), %rdx
	movq	%r12, %rdi
	call	*48(%rax)
	leaq	-192(%rbp), %rax
	movq	%rax, -288(%rbp)
	movl	(%r15), %eax
	testl	%eax, %eax
	jg	.L190
.L125:
	movl	$80, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L191
	movq	$0, -312(%rbp)
	xorl	%r12d, %r12d
	leaq	-128(%rbp), %r15
	movq	$0, -304(%rbp)
.L131:
	movl	-260(%rbp), %ecx
	movl	-264(%rbp), %edx
	movq	%rax, %rdi
	movq	%rax, -280(%rbp)
	movq	-288(%rbp), %rsi
	call	_ZN6icu_6719InitialTimeZoneRuleC1ERKNS_13UnicodeStringEii@PLT
	movq	-280(%rbp), %rax
.L130:
	movq	%rax, (%rbx)
	testb	%r12b, %r12b
	jne	.L192
.L126:
	movq	%r14, %rdi
	call	_ZN6icu_6718TimeZoneTransitionD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-288(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L189:
	movq	%r14, %rdi
	call	_ZNK6icu_6718TimeZoneTransition5getToEv@PLT
	movq	%rax, %rdi
	leaq	-192(%rbp), %rax
	movq	%rax, %rsi
	movq	%rax, -288(%rbp)
	call	_ZNK6icu_6712TimeZoneRule7getNameERNS_13UnicodeStringE@PLT
	movq	%r14, %rdi
	call	_ZNK6icu_6718TimeZoneTransition5getToEv@PLT
	movq	%rax, %rdi
	call	_ZNK6icu_6712TimeZoneRule12getRawOffsetEv@PLT
	movq	%r14, %rdi
	movl	%eax, -264(%rbp)
	call	_ZNK6icu_6718TimeZoneTransition5getToEv@PLT
	movq	%rax, %rdi
	call	_ZNK6icu_6712TimeZoneRule13getDSTSavingsEv@PLT
	movl	%eax, -260(%rbp)
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L188:
	movq	%r14, %rdi
	call	_ZNK6icu_6718TimeZoneTransition7getFromEv@PLT
	movq	%rax, %rdi
	leaq	-192(%rbp), %rax
	movq	%rax, %rsi
	movq	%rax, -288(%rbp)
	call	_ZNK6icu_6712TimeZoneRule7getNameERNS_13UnicodeStringE@PLT
	movq	%r14, %rdi
	call	_ZNK6icu_6718TimeZoneTransition7getFromEv@PLT
	movq	%rax, %rdi
	call	_ZNK6icu_6712TimeZoneRule12getRawOffsetEv@PLT
	movq	%r14, %rdi
	movl	%eax, -264(%rbp)
	call	_ZNK6icu_6718TimeZoneTransition7getFromEv@PLT
	movq	%rax, %rdi
	call	_ZNK6icu_6712TimeZoneRule13getDSTSavingsEv@PLT
	movq	%r14, %rdi
	movl	%eax, -260(%rbp)
	call	_ZNK6icu_6718TimeZoneTransition7getTimeEv@PLT
	movq	%r14, %rdi
	movsd	%xmm0, -320(%rbp)
	call	_ZNK6icu_6718TimeZoneTransition7getFromEv@PLT
	movq	%rax, %rdi
	call	_ZNK6icu_6712TimeZoneRule13getDSTSavingsEv@PLT
	testl	%eax, %eax
	jne	.L93
	movq	%r14, %rdi
	call	_ZNK6icu_6718TimeZoneTransition5getToEv@PLT
	movq	%rax, %rdi
	call	_ZNK6icu_6712TimeZoneRule13getDSTSavingsEv@PLT
	testl	%eax, %eax
	je	.L93
.L95:
	movsd	.LC0(%rip), %xmm0
	addsd	-280(%rbp), %xmm0
	comisd	-320(%rbp), %xmm0
	jbe	.L125
	pxor	%xmm1, %xmm1
	pxor	%xmm0, %xmm0
	leaq	-236(%rbp), %r9
	cvtsi2sdl	-260(%rbp), %xmm1
	leaq	-240(%rbp), %r8
	movq	%r9, -376(%rbp)
	cvtsi2sdl	-264(%rbp), %xmm0
	addsd	-320(%rbp), %xmm0
	movq	%r8, -368(%rbp)
	leaq	-244(%rbp), %rcx
	leaq	-248(%rbp), %rdx
	leaq	-252(%rbp), %rsi
	leaq	-256(%rbp), %rdi
	movq	%rcx, -360(%rbp)
	addsd	%xmm1, %xmm0
	movq	%rdx, -352(%rbp)
	movq	%rsi, -344(%rbp)
	movq	%rdi, -336(%rbp)
	call	_ZN6icu_675Grego12timeToFieldsEdRiS1_S1_S1_S1_S1_@PLT
	movl	-248(%rbp), %edx
	movl	-252(%rbp), %esi
	movl	-256(%rbp), %edi
	call	_ZN6icu_675Grego16dayOfWeekInMonthEiii@PLT
	movl	$40, %edi
	movl	%eax, %r15d
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r11
	testq	%rax, %rax
	je	.L96
	movl	-236(%rbp), %r8d
	xorl	%r9d, %r9d
	movl	%r15d, %edx
	movq	%rax, %rdi
	movl	-244(%rbp), %ecx
	movl	-252(%rbp), %esi
	movq	%rax, -304(%rbp)
	call	_ZN6icu_6712DateTimeRuleC1EiiiiNS0_12TimeRuleTypeE@PLT
	movq	-304(%rbp), %r11
.L96:
	movq	%r14, %rdi
	movq	%r11, -328(%rbp)
	leaq	-128(%rbp), %r15
	call	_ZNK6icu_6718TimeZoneTransition5getToEv@PLT
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	_ZNK6icu_6712TimeZoneRule7getNameERNS_13UnicodeStringE@PLT
	movq	%r14, %rdi
	call	_ZNK6icu_6718TimeZoneTransition5getToEv@PLT
	movq	%rax, %rdi
	call	_ZNK6icu_6712TimeZoneRule13getDSTSavingsEv@PLT
	movl	$96, %edi
	movl	%eax, -312(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, -304(%rbp)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L97
	movl	_ZN6icu_6718AnnualTimeZoneRule8MAX_YEARE(%rip), %eax
	subq	$8, %rsp
	movq	%r15, %rsi
	movq	-328(%rbp), %r11
	movl	-256(%rbp), %r9d
	movl	-312(%rbp), %ecx
	pushq	%rax
	movl	-264(%rbp), %edx
	movq	%r11, %r8
	call	_ZN6icu_6718AnnualTimeZoneRuleC1ERKNS_13UnicodeStringEiiPNS_12DateTimeRuleEii@PLT
	popq	%r9
	popq	%r10
.L97:
	movq	%r14, %rdi
	call	_ZNK6icu_6718TimeZoneTransition5getToEv@PLT
	movq	%rax, %rdi
	call	_ZNK6icu_6712TimeZoneRule12getRawOffsetEv@PLT
	cmpl	-264(%rbp), %eax
	je	.L193
.L99:
	movq	(%r12), %rax
	movq	%r14, %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	movsd	-280(%rbp), %xmm0
	call	*120(%rax)
	testb	%al, %al
	jne	.L194
.L112:
	movq	-304(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L123
	movq	(%rdi), %rax
	call	*8(%rax)
.L123:
	movl	$80, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L195
	movq	$0, -312(%rbp)
	xorl	%r12d, %r12d
	movq	$0, -304(%rbp)
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L93:
	movq	%r14, %rdi
	call	_ZNK6icu_6718TimeZoneTransition7getFromEv@PLT
	movq	%rax, %rdi
	call	_ZNK6icu_6712TimeZoneRule13getDSTSavingsEv@PLT
	testl	%eax, %eax
	je	.L125
	movq	%r14, %rdi
	call	_ZNK6icu_6718TimeZoneTransition5getToEv@PLT
	movq	%rax, %rdi
	call	_ZNK6icu_6712TimeZoneRule13getDSTSavingsEv@PLT
	testl	%eax, %eax
	je	.L95
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L192:
	movq	-304(%rbp), %rbx
	movq	%rbx, %rdi
	call	_ZNK6icu_6712TimeZoneRule13getDSTSavingsEv@PLT
	testl	%eax, %eax
	je	.L127
	movq	%rbx, 0(%r13)
	movq	-296(%rbp), %rax
	movq	-312(%rbp), %rbx
	movq	%rbx, (%rax)
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L127:
	movq	-296(%rbp), %rax
	movq	-304(%rbp), %rbx
	movq	%rbx, (%rax)
	movq	-312(%rbp), %rax
	movq	%rax, 0(%r13)
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L190:
	movq	%r14, %rdi
	call	_ZN6icu_6718TimeZoneTransitionD1Ev@PLT
	leaq	-128(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-288(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L87
.L193:
	movq	(%r12), %rax
	xorl	%esi, %esi
	movq	%r14, %rdx
	movq	%r12, %rdi
	movsd	-320(%rbp), %xmm0
	call	*112(%rax)
	testb	%al, %al
	je	.L99
	movq	%r14, %rdi
	call	_ZNK6icu_6718TimeZoneTransition7getFromEv@PLT
	movq	%rax, %rdi
	call	_ZNK6icu_6712TimeZoneRule13getDSTSavingsEv@PLT
	testl	%eax, %eax
	jne	.L103
	movq	%r14, %rdi
	call	_ZNK6icu_6718TimeZoneTransition5getToEv@PLT
	movq	%rax, %rdi
	call	_ZNK6icu_6712TimeZoneRule13getDSTSavingsEv@PLT
	testl	%eax, %eax
	je	.L103
.L105:
	movq	%r14, %rdi
	call	_ZNK6icu_6718TimeZoneTransition7getTimeEv@PLT
	movsd	.LC0(%rip), %xmm1
	addsd	-320(%rbp), %xmm1
	comisd	%xmm0, %xmm1
	jbe	.L99
	movq	%r14, %rdi
	call	_ZNK6icu_6718TimeZoneTransition7getTimeEv@PLT
	movq	%r14, %rdi
	movsd	%xmm0, -312(%rbp)
	call	_ZNK6icu_6718TimeZoneTransition7getFromEv@PLT
	movq	%rax, %rdi
	call	_ZNK6icu_6712TimeZoneRule12getRawOffsetEv@PLT
	pxor	%xmm0, %xmm0
	movsd	-312(%rbp), %xmm7
	movq	%r14, %rdi
	cvtsi2sdl	%eax, %xmm0
	addsd	%xmm0, %xmm7
	movsd	%xmm7, -312(%rbp)
	call	_ZNK6icu_6718TimeZoneTransition7getFromEv@PLT
	movq	%rax, %rdi
	call	_ZNK6icu_6712TimeZoneRule13getDSTSavingsEv@PLT
	pxor	%xmm0, %xmm0
	movq	-376(%rbp), %r9
	movq	-368(%rbp), %r8
	cvtsi2sdl	%eax, %xmm0
	addsd	-312(%rbp), %xmm0
	movq	-360(%rbp), %rcx
	movq	-352(%rbp), %rdx
	movq	-344(%rbp), %rsi
	movq	-336(%rbp), %rdi
	call	_ZN6icu_675Grego12timeToFieldsEdRiS1_S1_S1_S1_S1_@PLT
	movl	-248(%rbp), %edx
	movl	-252(%rbp), %esi
	movl	-256(%rbp), %edi
	call	_ZN6icu_675Grego16dayOfWeekInMonthEiii@PLT
	movl	$40, %edi
	movl	%eax, -312(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r11
	testq	%rax, %rax
	je	.L106
	movl	-312(%rbp), %edx
	movl	-236(%rbp), %r8d
	xorl	%r9d, %r9d
	movq	%rax, %rdi
	movl	-244(%rbp), %ecx
	movl	-252(%rbp), %esi
	movq	%rax, -312(%rbp)
	call	_ZN6icu_6712DateTimeRuleC1EiiiiNS0_12TimeRuleTypeE@PLT
	movq	-312(%rbp), %r11
.L106:
	movq	%r14, %rdi
	movq	%r11, -392(%rbp)
	call	_ZNK6icu_6718TimeZoneTransition5getToEv@PLT
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	_ZNK6icu_6712TimeZoneRule7getNameERNS_13UnicodeStringE@PLT
	movq	%r14, %rdi
	call	_ZNK6icu_6718TimeZoneTransition5getToEv@PLT
	movq	%rax, %rdi
	call	_ZNK6icu_6712TimeZoneRule12getRawOffsetEv@PLT
	movq	%r14, %rdi
	movl	%eax, -384(%rbp)
	call	_ZNK6icu_6718TimeZoneTransition5getToEv@PLT
	movq	%rax, %rdi
	call	_ZNK6icu_6712TimeZoneRule13getDSTSavingsEv@PLT
	movl	$96, %edi
	movl	%eax, -328(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, -312(%rbp)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L107
	movl	_ZN6icu_6718AnnualTimeZoneRule8MAX_YEARE(%rip), %eax
	movq	-392(%rbp), %r11
	pushq	%rsi
	movq	%r15, %rsi
	movl	-328(%rbp), %ecx
	movl	-384(%rbp), %edx
	pushq	%rax
	movl	-256(%rbp), %eax
	movq	%r11, %r8
	leal	-1(%rax), %r9d
	call	_ZN6icu_6718AnnualTimeZoneRuleC1ERKNS_13UnicodeStringEiiPNS_12DateTimeRuleEii@PLT
	popq	%rdi
	popq	%r8
.L107:
	movq	-312(%rbp), %rax
	movq	%r14, %rdi
	movq	(%rax), %rax
	movq	80(%rax), %r9
	movq	%r9, -384(%rbp)
	call	_ZNK6icu_6718TimeZoneTransition7getFromEv@PLT
	movq	%rax, %rdi
	call	_ZNK6icu_6712TimeZoneRule13getDSTSavingsEv@PLT
	movq	%r14, %rdi
	movl	%eax, -328(%rbp)
	call	_ZNK6icu_6718TimeZoneTransition7getFromEv@PLT
	movq	%rax, %rdi
	call	_ZNK6icu_6712TimeZoneRule12getRawOffsetEv@PLT
	movl	-328(%rbp), %edx
	movsd	-280(%rbp), %xmm0
	leaq	-232(%rbp), %r8
	movq	-312(%rbp), %rdi
	movq	-384(%rbp), %r9
	movl	%eax, %esi
	movl	$1, %ecx
	call	*%r9
	testb	%al, %al
	je	.L109
	movsd	-232(%rbp), %xmm0
	comisd	-280(%rbp), %xmm0
	jbe	.L196
.L109:
	movq	-312(%rbp), %rdi
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L194:
	movq	%r14, %rdi
	call	_ZNK6icu_6718TimeZoneTransition7getFromEv@PLT
	movq	%rax, %rdi
	call	_ZNK6icu_6712TimeZoneRule13getDSTSavingsEv@PLT
	testl	%eax, %eax
	jne	.L116
	movq	%r14, %rdi
	call	_ZNK6icu_6718TimeZoneTransition5getToEv@PLT
	movq	%rax, %rdi
	call	_ZNK6icu_6712TimeZoneRule13getDSTSavingsEv@PLT
	testl	%eax, %eax
	je	.L116
.L119:
	movq	%r14, %rdi
	call	_ZNK6icu_6718TimeZoneTransition7getTimeEv@PLT
	movq	%r14, %rdi
	movsd	%xmm0, -312(%rbp)
	call	_ZNK6icu_6718TimeZoneTransition7getFromEv@PLT
	movq	%rax, %rdi
	call	_ZNK6icu_6712TimeZoneRule12getRawOffsetEv@PLT
	pxor	%xmm0, %xmm0
	movsd	-312(%rbp), %xmm5
	movq	%r14, %rdi
	cvtsi2sdl	%eax, %xmm0
	addsd	%xmm0, %xmm5
	movsd	%xmm5, -312(%rbp)
	call	_ZNK6icu_6718TimeZoneTransition7getFromEv@PLT
	movq	%rax, %rdi
	call	_ZNK6icu_6712TimeZoneRule13getDSTSavingsEv@PLT
	pxor	%xmm0, %xmm0
	movq	-376(%rbp), %r9
	movq	-368(%rbp), %r8
	cvtsi2sdl	%eax, %xmm0
	addsd	-312(%rbp), %xmm0
	movq	-360(%rbp), %rcx
	movq	-352(%rbp), %rdx
	movq	-344(%rbp), %rsi
	movq	-336(%rbp), %rdi
	call	_ZN6icu_675Grego12timeToFieldsEdRiS1_S1_S1_S1_S1_@PLT
	movl	-248(%rbp), %edx
	movl	-252(%rbp), %esi
	movl	-256(%rbp), %edi
	call	_ZN6icu_675Grego16dayOfWeekInMonthEiii@PLT
	movl	$40, %edi
	movl	%eax, -312(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movl	-312(%rbp), %edx
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L118
	movl	-236(%rbp), %r8d
	movl	-244(%rbp), %ecx
	xorl	%r9d, %r9d
	movq	%rax, %rdi
	movl	-252(%rbp), %esi
	call	_ZN6icu_6712DateTimeRuleC1EiiiiNS0_12TimeRuleTypeE@PLT
.L118:
	movq	%r14, %rdi
	call	_ZNK6icu_6718TimeZoneTransition5getToEv@PLT
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	_ZNK6icu_6712TimeZoneRule7getNameERNS_13UnicodeStringE@PLT
	movq	-304(%rbp), %rdi
	call	_ZNK6icu_6718AnnualTimeZoneRule12getStartYearEv@PLT
	movl	$96, %edi
	leal	-1(%rax), %r9d
	movl	%r9d, -328(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, -312(%rbp)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L120
	movl	_ZN6icu_6718AnnualTimeZoneRule8MAX_YEARE(%rip), %eax
	subq	$8, %rsp
	movq	%r12, %r8
	movq	%r15, %rsi
	movl	-260(%rbp), %ecx
	movl	-264(%rbp), %edx
	pushq	%rax
	movl	-328(%rbp), %r9d
	call	_ZN6icu_6718AnnualTimeZoneRuleC1ERKNS_13UnicodeStringEiiPNS_12DateTimeRuleEii@PLT
	popq	%rdx
	popq	%rcx
.L120:
	movq	-312(%rbp), %rax
	movq	%r14, %rdi
	movq	(%rax), %rax
	movq	72(%rax), %r12
	call	_ZNK6icu_6718TimeZoneTransition7getFromEv@PLT
	movq	%rax, %rdi
	call	_ZNK6icu_6712TimeZoneRule13getDSTSavingsEv@PLT
	movq	%r14, %rdi
	movl	%eax, -328(%rbp)
	call	_ZNK6icu_6718TimeZoneTransition7getFromEv@PLT
	movq	%rax, %rdi
	call	_ZNK6icu_6712TimeZoneRule12getRawOffsetEv@PLT
	xorl	%ecx, %ecx
	movl	-328(%rbp), %edx
	movsd	-280(%rbp), %xmm0
	movq	-312(%rbp), %rdi
	movl	%eax, %esi
	leaq	-232(%rbp), %r8
	call	*%r12
	testb	%al, %al
	je	.L121
	movsd	-320(%rbp), %xmm7
	comisd	-232(%rbp), %xmm7
	jb	.L110
.L121:
	movq	-312(%rbp), %rdi
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L112
.L196:
	movq	%r14, %rdi
	call	_ZNK6icu_6718TimeZoneTransition5getToEv@PLT
	movq	%rax, %rdi
	call	_ZNK6icu_6712TimeZoneRule12getRawOffsetEv@PLT
	cmpl	-264(%rbp), %eax
	jne	.L109
	movq	%r14, %rdi
	call	_ZNK6icu_6718TimeZoneTransition5getToEv@PLT
	movq	%rax, %rdi
	call	_ZNK6icu_6712TimeZoneRule13getDSTSavingsEv@PLT
	cmpl	-260(%rbp), %eax
	jne	.L109
.L110:
	movq	-304(%rbp), %r12
	movq	-288(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNK6icu_6712TimeZoneRule7getNameERNS_13UnicodeStringE@PLT
	movq	%r12, %rdi
	call	_ZNK6icu_6712TimeZoneRule12getRawOffsetEv@PLT
	movq	%r12, %rdi
	movl	%eax, -264(%rbp)
	call	_ZNK6icu_6712TimeZoneRule13getDSTSavingsEv@PLT
	testq	%r12, %r12
	movl	$80, %edi
	setne	%r12b
	movl	%eax, -260(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	jne	.L131
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L116:
	movq	%r14, %rdi
	call	_ZNK6icu_6718TimeZoneTransition7getFromEv@PLT
	movq	%rax, %rdi
	call	_ZNK6icu_6712TimeZoneRule13getDSTSavingsEv@PLT
	testl	%eax, %eax
	je	.L112
	movq	%r14, %rdi
	call	_ZNK6icu_6718TimeZoneTransition5getToEv@PLT
	movq	%rax, %rdi
	call	_ZNK6icu_6712TimeZoneRule13getDSTSavingsEv@PLT
	testl	%eax, %eax
	je	.L119
	jmp	.L112
.L103:
	movq	%r14, %rdi
	call	_ZNK6icu_6718TimeZoneTransition7getFromEv@PLT
	movq	%rax, %rdi
	call	_ZNK6icu_6712TimeZoneRule13getDSTSavingsEv@PLT
	testl	%eax, %eax
	je	.L99
	movq	%r14, %rdi
	call	_ZNK6icu_6718TimeZoneTransition5getToEv@PLT
	movq	%rax, %rdi
	call	_ZNK6icu_6712TimeZoneRule13getDSTSavingsEv@PLT
	testl	%eax, %eax
	je	.L105
	jmp	.L99
.L187:
	call	__stack_chk_fail@PLT
.L191:
	movq	$0, (%rbx)
	leaq	-128(%rbp), %r15
	jmp	.L126
.L195:
	movq	$0, (%rbx)
	jmp	.L126
	.cfi_endproc
.LFE2402:
	.size	_ZNK6icu_6713BasicTimeZone18getSimpleRulesNearEdRPNS_19InitialTimeZoneRuleERPNS_18AnnualTimeZoneRuleES6_R10UErrorCode, .-_ZNK6icu_6713BasicTimeZone18getSimpleRulesNearEdRPNS_19InitialTimeZoneRuleERPNS_18AnnualTimeZoneRuleES6_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713BasicTimeZone21getTimeZoneRulesAfterEdRPNS_19InitialTimeZoneRuleERPNS_7UVectorER10UErrorCode
	.type	_ZNK6icu_6713BasicTimeZone21getTimeZoneRulesAfterEdRPNS_19InitialTimeZoneRuleERPNS_7UVectorER10UErrorCode, @function
_ZNK6icu_6713BasicTimeZone21getTimeZoneRulesAfterEdRPNS_19InitialTimeZoneRuleERPNS_7UVectorER10UErrorCode:
.LFB2403:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$360, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -264(%rbp)
	movl	(%rcx), %r10d
	movq	%rdx, -272(%rbp)
	movsd	%xmm0, -280(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r10d, %r10d
	jle	.L353
.L197:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L354
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L353:
	.cfi_restore_state
	leaq	-192(%rbp), %rax
	movq	%rdi, %r15
	movq	%rcx, %rbx
	movq	%rax, %rdi
	movq	%rax, -296(%rbp)
	call	_ZN6icu_6718TimeZoneTransitionC1Ev@PLT
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	%rax, -128(%rbp)
	movq	(%r15), %rax
	movl	$2, %r8d
	movw	%r8w, -120(%rbp)
	call	*136(%rax)
	movl	(%rbx), %r9d
	movl	%eax, -248(%rbp)
	leaq	-128(%rbp), %rax
	movq	%rax, -288(%rbp)
	testl	%r9d, %r9d
	jle	.L355
.L209:
	movq	-288(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-296(%rbp), %rdi
	call	_ZN6icu_6718TimeZoneTransitionD1Ev@PLT
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L355:
	movl	$40, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L200
	movl	-248(%rbp), %esi
	movq	%rbx, %rdx
	movq	%rax, %rdi
	call	_ZN6icu_677UVectorC1EiR10UErrorCode@PLT
.L200:
	movl	(%rbx), %edi
	leaq	-128(%rbp), %rax
	movq	%rax, -288(%rbp)
	testl	%edi, %edi
	jg	.L209
	movslq	-248(%rbp), %rdi
	salq	$3, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L210
	movq	(%r15), %rax
	leaq	-224(%rbp), %rsi
	movq	%rbx, %r8
	movq	%r13, %rdx
	leaq	-248(%rbp), %rcx
	movq	%r15, %rdi
	call	*144(%rax)
	movl	(%rbx), %esi
	testl	%esi, %esi
	jg	.L204
	movl	-248(%rbp), %ecx
	xorl	%r14d, %r14d
	testl	%ecx, %ecx
	jg	.L205
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L356:
	addq	$1, %r14
	cmpl	%r14d, -248(%rbp)
	jle	.L208
.L205:
	movq	0(%r13,%r14,8), %rdi
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	movl	(%rbx), %edx
	testl	%edx, %edx
	jle	.L356
.L204:
	movq	%r13, %rdi
	call	uprv_free_67@PLT
	testq	%r12, %r12
	je	.L352
.L202:
	leaq	-128(%rbp), %rax
	xorl	%r14d, %r14d
	movq	$0, -312(%rbp)
	movq	$0, -304(%rbp)
	movq	%rax, -288(%rbp)
	jmp	.L255
	.p2align 4,,10
	.p2align 3
.L358:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN6icu_677UVector15orphanElementAtEi@PLT
	testq	%rax, %rax
	jne	.L357
.L255:
	movl	8(%r12), %edx
	testl	%edx, %edx
	jne	.L358
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
	cmpq	$0, -304(%rbp)
	je	.L257
.L253:
	testq	%r14, %r14
	jne	.L260
	jmp	.L258
	.p2align 4,,10
	.p2align 3
.L360:
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN6icu_677UVector15orphanElementAtEi@PLT
	testq	%rax, %rax
	jne	.L359
.L260:
	movl	8(%r14), %eax
	testl	%eax, %eax
	jne	.L360
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*8(%rax)
.L258:
	movq	-312(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L262
	movq	(%rdi), %rax
	call	*8(%rax)
.L262:
	movq	-304(%rbp), %rdi
	call	uprv_free_67@PLT
.L257:
	movq	-264(%rbp), %rax
	movq	-288(%rbp), %rdi
	movq	$0, (%rax)
	movq	-272(%rbp), %rax
	movq	$0, (%rax)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-296(%rbp), %rdi
	call	_ZN6icu_6718TimeZoneTransitionD1Ev@PLT
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L357:
	movq	(%rax), %rdx
	movq	%rax, %rdi
	call	*8(%rdx)
	jmp	.L255
	.p2align 4,,10
	.p2align 3
.L359:
	movq	(%rax), %rdx
	movq	%rax, %rdi
	call	*8(%rdx)
	jmp	.L260
	.p2align 4,,10
	.p2align 3
.L208:
	movq	%r13, %rdi
	call	uprv_free_67@PLT
	movq	(%r15), %rax
	movl	$1, %esi
	movq	%r15, %rdi
	movq	-296(%rbp), %rdx
	movsd	-280(%rbp), %xmm0
	call	*120(%rax)
	testb	%al, %al
	jne	.L361
	movq	-224(%rbp), %rdi
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	-264(%rbp), %rcx
	movq	%rax, (%rcx)
	movq	-272(%rbp), %rax
	movq	%r12, (%rax)
	leaq	-128(%rbp), %rax
	movq	%rax, -288(%rbp)
	jmp	.L209
.L361:
	movslq	-248(%rbp), %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, -304(%rbp)
	testq	%rax, %rax
	je	.L210
	movl	$40, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L211
	movq	%rbx, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_677UVectorC1ER10UErrorCode@PLT
.L211:
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L362
	testq	%r12, %r12
	je	.L363
	movq	$0, -312(%rbp)
	leaq	-128(%rbp), %rax
	movq	%rax, -288(%rbp)
	jmp	.L255
.L362:
	movq	-296(%rbp), %r13
	movq	%r13, %rdi
	call	_ZNK6icu_6718TimeZoneTransition5getToEv@PLT
	movq	%rax, %rdi
	leaq	-128(%rbp), %rax
	movq	%rax, %rsi
	movq	%rax, -288(%rbp)
	call	_ZNK6icu_6712TimeZoneRule7getNameERNS_13UnicodeStringE@PLT
	movq	%r13, %rdi
	movq	%r13, -296(%rbp)
	call	_ZNK6icu_6718TimeZoneTransition5getToEv@PLT
	movq	%rax, %rdi
	call	_ZNK6icu_6712TimeZoneRule12getRawOffsetEv@PLT
	movq	-296(%rbp), %rdi
	movl	%eax, %r13d
	call	_ZNK6icu_6718TimeZoneTransition5getToEv@PLT
	movq	%rax, %rdi
	call	_ZNK6icu_6712TimeZoneRule13getDSTSavingsEv@PLT
	movl	$80, %edi
	movl	%eax, -320(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, -312(%rbp)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L213
	movl	-320(%rbp), %ecx
	movq	-288(%rbp), %rsi
	movl	%r13d, %edx
	call	_ZN6icu_6719InitialTimeZoneRuleC1ERKNS_13UnicodeStringEii@PLT
.L213:
	leaq	-216(%rbp), %rax
	xorl	%r13d, %r13d
	movq	%rax, -328(%rbp)
	movl	-248(%rbp), %eax
	testl	%eax, %eax
	jle	.L216
	movq	%r15, -344(%rbp)
	movq	%r14, -336(%rbp)
	movq	-312(%rbp), %r14
	movq	%rbx, -352(%rbp)
	movq	%r13, %rbx
	.p2align 4,,10
	.p2align 3
.L217:
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%r14, %rdi
	movq	%rax, %r13
	movq	(%rax), %rax
	movq	72(%rax), %r15
	call	_ZNK6icu_6712TimeZoneRule13getDSTSavingsEv@PLT
	movq	%r14, %rdi
	movl	%eax, -320(%rbp)
	call	_ZNK6icu_6712TimeZoneRule12getRawOffsetEv@PLT
	xorl	%ecx, %ecx
	movl	-320(%rbp), %edx
	movq	%r13, %rdi
	movq	-328(%rbp), %r8
	movsd	-280(%rbp), %xmm0
	movl	%eax, %esi
	call	*%r15
	testb	%al, %al
	movq	-304(%rbp), %rax
	sete	(%rax,%rbx)
	addq	$1, %rbx
	cmpl	%ebx, -248(%rbp)
	jg	.L217
	movq	-336(%rbp), %r14
	movq	-344(%rbp), %r15
	movq	-352(%rbp), %rbx
.L216:
	leaq	-200(%rbp), %rax
	movsd	-280(%rbp), %xmm3
	movb	$0, -352(%rbp)
	movb	$0, -344(%rbp)
	movq	%rax, -368(%rbp)
	movq	%r14, -328(%rbp)
	movq	%rbx, -336(%rbp)
	movq	-296(%rbp), %rbx
	movsd	%xmm3, -216(%rbp)
	.p2align 4,,10
	.p2align 3
.L215:
	movzbl	-352(%rbp), %ecx
	testb	%cl, -344(%rbp)
	jne	.L338
.L330:
	movq	(%r15), %rax
	xorl	%esi, %esi
	movsd	-216(%rbp), %xmm0
	movq	%rbx, %rdx
	movq	%r15, %rdi
	call	*112(%rax)
	testb	%al, %al
	je	.L338
	movq	%rbx, %rdi
	call	_ZNK6icu_6718TimeZoneTransition7getTimeEv@PLT
	ucomisd	-216(%rbp), %xmm0
	jnp	.L364
.L219:
	movq	%rbx, %rdi
	movsd	%xmm0, -216(%rbp)
	call	_ZNK6icu_6718TimeZoneTransition5getToEv@PLT
	movq	%rax, %r13
	movl	-248(%rbp), %eax
	testl	%eax, %eax
	jle	.L340
	xorl	%r14d, %r14d
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L365:
	addl	$1, %r14d
	cmpl	%r14d, -248(%rbp)
	jle	.L340
.L224:
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*32(%rax)
	testb	%al, %al
	je	.L365
	cmpl	%r14d, -248(%rbp)
	jle	.L340
	movslq	%r14d, %r14
	addq	-304(%rbp), %r14
	cmpb	$0, (%r14)
	jne	.L330
	movq	%r14, -320(%rbp)
	testq	%r13, %r13
	je	.L227
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6721TimeArrayTimeZoneRuleE(%rip), %rdx
	leaq	_ZTIN6icu_6712TimeZoneRuleE(%rip), %rsi
	movq	%r13, %rdi
	call	__dynamic_cast@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	jne	.L366
	movq	%r13, %rdi
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6718AnnualTimeZoneRuleE(%rip), %rdx
	leaq	_ZTIN6icu_6712TimeZoneRuleE(%rip), %rsi
	call	__dynamic_cast@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L227
	movq	(%rax), %rax
	movq	%rbx, %rdi
	movq	56(%rax), %r14
	call	_ZNK6icu_6718TimeZoneTransition7getFromEv@PLT
	movq	%rax, %rdi
	call	_ZNK6icu_6712TimeZoneRule13getDSTSavingsEv@PLT
	movq	%rbx, %rdi
	movl	%eax, -360(%rbp)
	call	_ZNK6icu_6718TimeZoneTransition7getFromEv@PLT
	movq	%rax, %rdi
	call	_ZNK6icu_6712TimeZoneRule12getRawOffsetEv@PLT
	movq	-368(%rbp), %rcx
	movl	-360(%rbp), %edx
	movq	%r13, %rdi
	movl	%eax, %esi
	call	*%r14
	movq	%rbx, %rdi
	call	_ZNK6icu_6718TimeZoneTransition7getTimeEv@PLT
	ucomisd	-200(%rbp), %xmm0
	jp	.L243
	jne	.L243
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	movq	-336(%rbp), %r14
	movq	-328(%rbp), %rdi
	movq	%rax, %rsi
	movq	%r14, %rdx
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	movl	(%r14), %r9d
	testl	%r9d, %r9d
	jg	.L344
.L246:
	movq	%r13, %rdi
	call	_ZNK6icu_6718AnnualTimeZoneRule10getEndYearEv@PLT
	cmpl	_ZN6icu_6718AnnualTimeZoneRule8MAX_YEARE(%rip), %eax
	je	.L367
.L227:
	movq	-320(%rbp), %rax
	movb	$1, (%rax)
	jmp	.L215
.L364:
	jne	.L219
	.p2align 4,,10
	.p2align 3
.L340:
	movq	-336(%rbp), %rbx
	movq	-328(%rbp), %r14
	movl	$27, (%rbx)
.L221:
	testq	%r12, %r12
	jne	.L255
	jmp	.L253
	.p2align 4,,10
	.p2align 3
.L366:
	leaq	-160(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN6icu_6718TimeZoneTransitionC1Ev@PLT
	movsd	-280(%rbp), %xmm2
	movsd	%xmm2, -208(%rbp)
	movapd	%xmm2, %xmm0
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L229:
	movq	%r13, %rdi
	call	_ZNK6icu_6718TimeZoneTransition5getToEv@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*32(%rax)
	testb	%al, %al
	jne	.L230
	movq	%r13, %rdi
	call	_ZNK6icu_6718TimeZoneTransition7getTimeEv@PLT
	movsd	%xmm0, -208(%rbp)
.L231:
	movq	(%r15), %rax
	xorl	%esi, %esi
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	*112(%rax)
	testb	%al, %al
	jne	.L229
.L233:
	movq	%r13, %rdi
	call	_ZN6icu_6718TimeZoneTransitionD1Ev@PLT
	jmp	.L227
.L230:
	movq	(%r14), %rax
	movq	%rbx, %rdi
	movq	56(%rax), %r8
	movq	%r8, -376(%rbp)
	call	_ZNK6icu_6718TimeZoneTransition7getFromEv@PLT
	movq	%rax, %rdi
	call	_ZNK6icu_6712TimeZoneRule13getDSTSavingsEv@PLT
	movq	%rbx, %rdi
	movl	%eax, -360(%rbp)
	call	_ZNK6icu_6718TimeZoneTransition7getFromEv@PLT
	movq	%rax, %rdi
	call	_ZNK6icu_6712TimeZoneRule12getRawOffsetEv@PLT
	movq	-368(%rbp), %rcx
	movl	-360(%rbp), %edx
	movq	%r14, %rdi
	movq	-376(%rbp), %r8
	movl	%eax, %esi
	call	*%r8
	movsd	-200(%rbp), %xmm0
	comisd	-280(%rbp), %xmm0
	jbe	.L368
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*24(%rax)
	movq	-336(%rbp), %r14
	movq	-328(%rbp), %rdi
	movq	%rax, %rsi
	movq	%r14, %rdx
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	movl	(%r14), %eax
	testl	%eax, %eax
	jle	.L233
.L351:
	movq	-328(%rbp), %r14
.L240:
	movq	%r13, %rdi
	call	_ZN6icu_6718TimeZoneTransitionD1Ev@PLT
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L243:
	movq	%rbx, %rdi
	call	_ZNK6icu_6718TimeZoneTransition7getTimeEv@PLT
	leaq	-232(%rbp), %rcx
	leaq	-236(%rbp), %rdx
	leaq	-160(%rbp), %r9
	leaq	-228(%rbp), %r8
	leaq	-240(%rbp), %rsi
	leaq	-244(%rbp), %rdi
	call	_ZN6icu_675Grego12timeToFieldsEdRiS1_S1_S1_S1_S1_@PLT
	movq	-288(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNK6icu_6712TimeZoneRule7getNameERNS_13UnicodeStringE@PLT
	movq	%r13, %rdi
	call	_ZNK6icu_6712TimeZoneRule12getRawOffsetEv@PLT
	movq	%r13, %rdi
	movl	%eax, -388(%rbp)
	call	_ZNK6icu_6712TimeZoneRule13getDSTSavingsEv@PLT
	movq	%r13, %rdi
	movl	%eax, -384(%rbp)
	call	_ZNK6icu_6718AnnualTimeZoneRule7getRuleEv@PLT
	movq	%r13, %rdi
	movq	%rax, -376(%rbp)
	call	_ZNK6icu_6718AnnualTimeZoneRule10getEndYearEv@PLT
	movl	$96, %edi
	movl	%eax, -360(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L247
	movl	-360(%rbp), %esi
	subq	$8, %rsp
	movq	%rax, %rdi
	movq	-376(%rbp), %r8
	movl	-244(%rbp), %r9d
	movl	-384(%rbp), %ecx
	pushq	%rsi
	movl	-388(%rbp), %edx
	movq	-288(%rbp), %rsi
	call	_ZN6icu_6718AnnualTimeZoneRuleC1ERKNS_13UnicodeStringEiiRKNS_12DateTimeRuleEii@PLT
	popq	%rdi
	popq	%r8
.L247:
	movq	-336(%rbp), %rdx
	movq	-328(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	movq	-336(%rbp), %rax
	movl	(%rax), %esi
	testl	%esi, %esi
	jle	.L246
.L344:
	movq	-328(%rbp), %r14
	jmp	.L221
.L367:
	movq	%r13, %rdi
	call	_ZNK6icu_6712TimeZoneRule13getDSTSavingsEv@PLT
	movzbl	-352(%rbp), %ecx
	movl	$1, %edx
	testl	%eax, %eax
	movzbl	-344(%rbp), %eax
	cmovne	%edx, %ecx
	cmove	%edx, %eax
	movb	%cl, -352(%rbp)
	movb	%al, -344(%rbp)
	jmp	.L227
.L338:
	movq	-328(%rbp), %r14
	testq	%r12, %r12
	jne	.L251
	jmp	.L249
	.p2align 4,,10
	.p2align 3
.L370:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN6icu_677UVector15orphanElementAtEi@PLT
	testq	%rax, %rax
	jne	.L369
.L251:
	movl	8(%r12), %ecx
	testl	%ecx, %ecx
	jne	.L370
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
.L249:
	movq	-304(%rbp), %rdi
	call	uprv_free_67@PLT
	movq	-264(%rbp), %rax
	movq	-312(%rbp), %rcx
	movq	%rcx, (%rax)
	movq	-272(%rbp), %rax
	movq	%r14, (%rax)
	jmp	.L209
.L369:
	movq	(%rax), %rdx
	movq	%rax, %rdi
	call	*8(%rdx)
	jmp	.L251
.L368:
	movq	%r14, %rdi
	call	_ZNK6icu_6721TimeArrayTimeZoneRule15countStartTimesEv@PLT
	movq	%r14, %rdi
	movl	%eax, -360(%rbp)
	call	_ZNK6icu_6721TimeArrayTimeZoneRule11getTimeTypeEv@PLT
	cmpl	$0, -360(%rbp)
	movl	%eax, -388(%rbp)
	jle	.L233
	xorl	%ecx, %ecx
	leaq	-208(%rbp), %rdx
	cmpl	$1, %eax
	je	.L271
	testl	%eax, %eax
	je	.L272
	movq	%rbx, -376(%rbp)
	movl	%ecx, %ebx
	movq	%r13, -384(%rbp)
	movq	%r12, %r13
	movq	%rdx, %r12
	jmp	.L238
.L372:
	addl	$1, %ebx
	cmpl	-360(%rbp), %ebx
	je	.L371
.L238:
	movq	%r12, %rdx
	movl	%ebx, %esi
	movq	%r14, %rdi
	call	_ZNK6icu_6721TimeArrayTimeZoneRule14getStartTimeAtEiRd@PLT
	movsd	-208(%rbp), %xmm0
	comisd	-280(%rbp), %xmm0
	jbe	.L372
	movq	%r13, %r12
	movl	%ebx, %ecx
	movq	-384(%rbp), %r13
	movq	-376(%rbp), %rbx
.L236:
	movl	-360(%rbp), %eax
	movl	%ecx, -376(%rbp)
	subl	%ecx, %eax
	movl	%eax, -360(%rbp)
	testl	%eax, %eax
	jle	.L233
	movslq	%eax, %rdi
	salq	$3, %rdi
	call	uprv_malloc_67@PLT
	movl	-376(%rbp), %ecx
	testq	%rax, %rax
	je	.L373
	xorl	%r9d, %r9d
	movq	%rbx, -376(%rbp)
	movq	%r14, %rdi
	movq	%r12, %r14
	movq	%r13, -384(%rbp)
	movq	%rax, %r12
	movq	%r9, %rbx
	movl	%ecx, %r13d
.L239:
	leaq	(%r12,%rbx,8), %rdx
	leal	0(%r13,%rbx), %esi
	movq	%rdi, -400(%rbp)
	call	_ZNK6icu_6721TimeArrayTimeZoneRule14getStartTimeAtEiRd@PLT
	movq	-336(%rbp), %rax
	movq	-400(%rbp), %rdi
	cmpl	$0, (%rax)
	jg	.L374
	addq	$1, %rbx
	cmpl	%ebx, -360(%rbp)
	jg	.L239
	movq	-288(%rbp), %rsi
	movq	%r12, -400(%rbp)
	movq	%r14, %r12
	movq	%rdi, %r14
	movq	-376(%rbp), %rbx
	movq	-384(%rbp), %r13
	call	_ZNK6icu_6712TimeZoneRule7getNameERNS_13UnicodeStringE@PLT
	movq	%r14, %rdi
	call	_ZNK6icu_6712TimeZoneRule12getRawOffsetEv@PLT
	movq	%r14, %rdi
	movl	%eax, -384(%rbp)
	call	_ZNK6icu_6712TimeZoneRule13getDSTSavingsEv@PLT
	movl	$352, %edi
	movl	%eax, -376(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	-400(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L242
	movl	-388(%rbp), %eax
	pushq	%r10
	movq	%r14, %rdi
	movl	-360(%rbp), %r9d
	movl	-376(%rbp), %ecx
	movq	%r8, -360(%rbp)
	movl	-384(%rbp), %edx
	movq	-288(%rbp), %rsi
	pushq	%rax
	call	_ZN6icu_6721TimeArrayTimeZoneRuleC1ERKNS_13UnicodeStringEiiPKdiNS_12DateTimeRule12TimeRuleTypeE@PLT
	movq	-360(%rbp), %r8
	popq	%r11
	popq	%rax
.L242:
	movq	%r8, %rdi
	call	uprv_free_67@PLT
	movq	-336(%rbp), %rdx
	movq	-328(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	movq	-336(%rbp), %rax
	cmpl	$0, (%rax)
	jle	.L233
	jmp	.L351
.L354:
	call	__stack_chk_fail@PLT
.L272:
	movq	%r15, -376(%rbp)
	movq	%r12, %r15
	movq	%rbx, %r12
	movl	%ecx, %ebx
	movq	%r13, -384(%rbp)
	movq	%rdx, %r13
	jmp	.L235
.L375:
	addl	$1, %ebx
	cmpl	%ebx, -360(%rbp)
	je	.L350
.L235:
	movq	%r13, %rdx
	movl	%ebx, %esi
	movq	%r14, %rdi
	call	_ZNK6icu_6721TimeArrayTimeZoneRule14getStartTimeAtEiRd@PLT
	movq	%r12, %rdi
	call	_ZNK6icu_6718TimeZoneTransition7getFromEv@PLT
	movq	%rax, %rdi
	call	_ZNK6icu_6712TimeZoneRule13getDSTSavingsEv@PLT
	pxor	%xmm1, %xmm1
	movsd	-208(%rbp), %xmm0
	cvtsi2sdl	%eax, %xmm1
	subsd	%xmm1, %xmm0
	comisd	-280(%rbp), %xmm0
	movsd	%xmm0, -208(%rbp)
	jbe	.L375
.L343:
	movl	%ebx, %ecx
	movq	-384(%rbp), %r13
	movq	%r12, %rbx
	movq	%r15, %r12
	movq	-376(%rbp), %r15
	jmp	.L236
.L350:
	movq	%r12, %rbx
	movq	-384(%rbp), %r13
	movq	%r15, %r12
	movq	-376(%rbp), %r15
	jmp	.L233
.L271:
	movq	%r15, -376(%rbp)
	movq	%r12, %r15
	movq	%rbx, %r12
	movl	%ecx, %ebx
	movq	%r13, -384(%rbp)
	movq	%rdx, %r13
	jmp	.L234
.L376:
	addl	$1, %ebx
	cmpl	%ebx, -360(%rbp)
	je	.L350
.L234:
	movq	%r13, %rdx
	movl	%ebx, %esi
	movq	%r14, %rdi
	call	_ZNK6icu_6721TimeArrayTimeZoneRule14getStartTimeAtEiRd@PLT
	movq	%r12, %rdi
	call	_ZNK6icu_6718TimeZoneTransition7getFromEv@PLT
	movq	%rax, %rdi
	call	_ZNK6icu_6712TimeZoneRule12getRawOffsetEv@PLT
	pxor	%xmm1, %xmm1
	movsd	-208(%rbp), %xmm0
	cvtsi2sdl	%eax, %xmm1
	subsd	%xmm1, %xmm0
	comisd	-280(%rbp), %xmm0
	movsd	%xmm0, -208(%rbp)
	jbe	.L376
	jmp	.L343
.L371:
	movq	%r13, %r12
	movq	-376(%rbp), %rbx
	movq	-384(%rbp), %r13
	jmp	.L233
.L210:
	movl	$7, (%rbx)
	testq	%r12, %r12
	jne	.L202
.L352:
	leaq	-128(%rbp), %rax
	movq	%rax, -288(%rbp)
	jmp	.L257
.L363:
	testq	%r14, %r14
	je	.L377
	movq	$0, -312(%rbp)
	leaq	-128(%rbp), %rax
	movq	%rax, -288(%rbp)
	jmp	.L260
.L374:
	movq	%r12, %r8
	movq	-376(%rbp), %rax
	movq	%r14, %r12
	movq	-384(%rbp), %r13
	movq	%r8, %rdi
	movq	-328(%rbp), %r14
	movq	%rax, -296(%rbp)
	call	uprv_free_67@PLT
	jmp	.L240
.L373:
	movq	-336(%rbp), %rbx
	movq	-328(%rbp), %r14
	movl	$7, (%rbx)
	jmp	.L240
.L377:
	leaq	-128(%rbp), %rax
	movq	%rax, -288(%rbp)
	jmp	.L262
	.cfi_endproc
.LFE2403:
	.size	_ZNK6icu_6713BasicTimeZone21getTimeZoneRulesAfterEdRPNS_19InitialTimeZoneRuleERPNS_7UVectorER10UErrorCode, .-_ZNK6icu_6713BasicTimeZone21getTimeZoneRulesAfterEdRPNS_19InitialTimeZoneRuleERPNS_7UVectorER10UErrorCode
	.weak	_ZTSN6icu_6713BasicTimeZoneE
	.section	.rodata._ZTSN6icu_6713BasicTimeZoneE,"aG",@progbits,_ZTSN6icu_6713BasicTimeZoneE,comdat
	.align 16
	.type	_ZTSN6icu_6713BasicTimeZoneE, @object
	.size	_ZTSN6icu_6713BasicTimeZoneE, 25
_ZTSN6icu_6713BasicTimeZoneE:
	.string	"N6icu_6713BasicTimeZoneE"
	.weak	_ZTIN6icu_6713BasicTimeZoneE
	.section	.data.rel.ro._ZTIN6icu_6713BasicTimeZoneE,"awG",@progbits,_ZTIN6icu_6713BasicTimeZoneE,comdat
	.align 8
	.type	_ZTIN6icu_6713BasicTimeZoneE, @object
	.size	_ZTIN6icu_6713BasicTimeZoneE, 24
_ZTIN6icu_6713BasicTimeZoneE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6713BasicTimeZoneE
	.quad	_ZTIN6icu_678TimeZoneE
	.weak	_ZTVN6icu_6713BasicTimeZoneE
	.section	.data.rel.ro._ZTVN6icu_6713BasicTimeZoneE,"awG",@progbits,_ZTVN6icu_6713BasicTimeZoneE,comdat
	.align 8
	.type	_ZTVN6icu_6713BasicTimeZoneE, @object
	.size	_ZTVN6icu_6713BasicTimeZoneE, 184
_ZTVN6icu_6713BasicTimeZoneE:
	.quad	0
	.quad	_ZTIN6icu_6713BasicTimeZoneE
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	_ZNK6icu_678TimeZoneeqERKS0_
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZNK6icu_678TimeZone9getOffsetEdaRiS1_R10UErrorCode
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZNK6icu_678TimeZone12hasSameRulesERKS0_
	.quad	__cxa_pure_virtual
	.quad	_ZNK6icu_678TimeZone13getDSTSavingsEv
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZNK6icu_6713BasicTimeZone24hasEquivalentTransitionsERKS0_ddaR10UErrorCode
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZNK6icu_6713BasicTimeZone18getSimpleRulesNearEdRPNS_19InitialTimeZoneRuleERPNS_18AnnualTimeZoneRuleES6_R10UErrorCode
	.quad	_ZNK6icu_6713BasicTimeZone18getOffsetFromLocalEdiiRiS1_R10UErrorCode
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	2952790016
	.long	1109221060
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
