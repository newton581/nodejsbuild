	.file	"collationiterator.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717CollationIterator23handleGetTrailSurrogateEv
	.type	_ZN6icu_6717CollationIterator23handleGetTrailSurrogateEv, @function
_ZN6icu_6717CollationIterator23handleGetTrailSurrogateEv:
.LFB3354:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3354:
	.size	_ZN6icu_6717CollationIterator23handleGetTrailSurrogateEv, .-_ZN6icu_6717CollationIterator23handleGetTrailSurrogateEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717CollationIterator18foundNULTerminatorEv
	.type	_ZN6icu_6717CollationIterator18foundNULTerminatorEv, @function
_ZN6icu_6717CollationIterator18foundNULTerminatorEv:
.LFB3355:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3355:
	.size	_ZN6icu_6717CollationIterator18foundNULTerminatorEv, .-_ZN6icu_6717CollationIterator18foundNULTerminatorEv
	.globl	_ZNK6icu_6717CollationIterator25forbidSurrogateCodePointsEv
	.set	_ZNK6icu_6717CollationIterator25forbidSurrogateCodePointsEv,_ZN6icu_6717CollationIterator18foundNULTerminatorEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717CollationIterator22getCE32FromBuilderDataEjR10UErrorCode
	.type	_ZN6icu_6717CollationIterator22getCE32FromBuilderDataEjR10UErrorCode, @function
_ZN6icu_6717CollationIterator22getCE32FromBuilderDataEjR10UErrorCode:
.LFB3358:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L5
	movl	$5, (%rdx)
.L5:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3358:
	.size	_ZN6icu_6717CollationIterator22getCE32FromBuilderDataEjR10UErrorCode, .-_ZN6icu_6717CollationIterator22getCE32FromBuilderDataEjR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717CollationIteratoreqERKS0_
	.type	_ZNK6icu_6717CollationIteratoreqERKS0_, @function
_ZNK6icu_6717CollationIteratoreqERKS0_:
.LFB3350:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rax
	movq	-8(%rax), %rax
	movq	8(%rax), %rdi
	movq	(%rsi), %rax
	movq	-8(%rax), %rax
	movq	8(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L7
	xorl	%r13d, %r13d
	cmpb	$42, (%rdi)
	je	.L6
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L6
.L7:
	movl	24(%r12), %edx
	xorl	%r13d, %r13d
	cmpl	24(%rbx), %edx
	je	.L19
.L6:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	movl	368(%rbx), %eax
	cmpl	%eax, 368(%r12)
	jne	.L6
	movq	384(%r12), %rax
	movabsq	$1099511627775, %rcx
	xorq	384(%rbx), %rax
	andq	%rcx, %rax
	jne	.L6
	testl	%edx, %edx
	jle	.L15
	movq	32(%r12), %rsi
	movq	32(%rbx), %rcx
	leal	-1(%rdx), %edi
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L20:
	leaq	1(%rax), %rdx
	cmpq	%rax, %rdi
	je	.L15
	movq	%rdx, %rax
.L9:
	movq	(%rsi,%rax,8), %rdx
	cmpq	%rdx, (%rcx,%rax,8)
	je	.L20
	xorl	%r13d, %r13d
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L15:
	movl	$1, %r13d
	jmp	.L6
	.cfi_endproc
.LFE3350:
	.size	_ZNK6icu_6717CollationIteratoreqERKS0_, .-_ZNK6icu_6717CollationIteratoreqERKS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717CollationIterator11getDataCE32Ei
	.type	_ZNK6icu_6717CollationIterator11getDataCE32Ei, @function
_ZNK6icu_6717CollationIterator11getDataCE32Ei:
.LFB3357:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	(%rax), %rdx
	movq	16(%rdx), %rcx
	cmpl	$55295, %esi
	ja	.L22
	movl	%esi, %eax
	movq	(%rdx), %rdx
	andl	$31, %esi
	sarl	$5, %eax
	cltq
	movzwl	(%rdx,%rax,2), %eax
	leal	(%rsi,%rax,4), %eax
	cltq
	salq	$2, %rax
.L23:
	movl	(%rcx,%rax), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	cmpl	$65535, %esi
	ja	.L24
	cmpl	$56320, %esi
	movl	$320, %eax
	movq	(%rdx), %rdi
	movl	$0, %edx
	cmovl	%eax, %edx
	movl	%esi, %eax
	sarl	$5, %eax
.L29:
	addl	%edx, %eax
	andl	$31, %esi
	cltq
	movzwl	(%rdi,%rax,2), %eax
	leal	(%rsi,%rax,4), %eax
	cltq
	salq	$2, %rax
	movl	(%rcx,%rax), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	movl	$512, %eax
	cmpl	$1114111, %esi
	ja	.L23
	cmpl	44(%rdx), %esi
	jl	.L26
	movslq	48(%rdx), %rax
	salq	$2, %rax
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L26:
	movl	%esi, %eax
	movq	(%rdx), %rdi
	sarl	$11, %eax
	addl	$2080, %eax
	cltq
	movzwl	(%rdi,%rax,2), %edx
	movl	%esi, %eax
	sarl	$5, %eax
	andl	$63, %eax
	jmp	.L29
	.cfi_endproc
.LFE3357:
	.size	_ZNK6icu_6717CollationIterator11getDataCE32Ei, .-_ZNK6icu_6717CollationIterator11getDataCE32Ei
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717CollationIterator14handleNextCE32ERiR10UErrorCode
	.type	_ZN6icu_6717CollationIterator14handleNextCE32ERiR10UErrorCode, @function
_ZN6icu_6717CollationIterator14handleNextCE32ERiR10UErrorCode:
.LFB3353:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	movq	%rdx, %rsi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	call	*48(%rax)
	movl	$192, %r8d
	movl	%eax, (%r12)
	testl	%eax, %eax
	js	.L30
	movq	16(%rbx), %rdx
	movq	(%rdx), %rcx
	movq	16(%rcx), %rsi
	cmpl	$55295, %eax
	jg	.L32
	movl	%eax, %edx
	movq	(%rcx), %rcx
	andl	$31, %eax
	sarl	$5, %edx
	movslq	%edx, %rdx
	movzwl	(%rcx,%rdx,2), %edx
	leal	(%rax,%rdx,4), %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
.L33:
	movl	(%rsi,%rdx), %r8d
.L30:
	popq	%rbx
	movl	%r8d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore_state
	cmpl	$65535, %eax
	jg	.L34
	cmpl	$56320, %eax
	movl	$320, %edx
	movq	(%rcx), %rdi
	movl	$0, %ecx
	cmovl	%edx, %ecx
	movl	%eax, %edx
	sarl	$5, %edx
.L42:
	addl	%ecx, %edx
	andl	$31, %eax
	movslq	%edx, %rdx
	movzwl	(%rdi,%rdx,2), %edx
	leal	(%rax,%rdx,4), %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L34:
	movl	$512, %edx
	cmpl	$1114111, %eax
	ja	.L33
	cmpl	44(%rcx), %eax
	jl	.L36
	movslq	48(%rcx), %rdx
	salq	$2, %rdx
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L36:
	movl	%eax, %edx
	movq	(%rcx), %rdi
	sarl	$11, %edx
	addl	$2080, %edx
	movslq	%edx, %rdx
	movzwl	(%rdi,%rdx,2), %ecx
	movl	%eax, %edx
	sarl	$5, %edx
	andl	$63, %edx
	jmp	.L42
	.cfi_endproc
.LFE3353:
	.size	_ZN6icu_6717CollationIterator14handleNextCE32ERiR10UErrorCode, .-_ZN6icu_6717CollationIterator14handleNextCE32ERiR10UErrorCode
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev:
.LFB3606:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3606:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei:
.LFB3609:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	cmpl	$40, %esi
	jg	.L56
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L44
	cmpb	$0, 12(%rbx)
	jne	.L57
.L48:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L44:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L48
	.cfi_endproc
.LFE3609:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EED2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev:
.LFB3612:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L60
	ret
	.p2align 4,,10
	.p2align 3
.L60:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3612:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5EOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_:
.LFB3615:
	.cfi_startproc
	endbr64
	movzbl	12(%rsi), %eax
	movq	(%rsi), %r8
	movslq	8(%rsi), %rdx
	movb	%al, 12(%rdi)
	leaq	13(%rsi), %rax
	movq	%r8, (%rdi)
	movl	%edx, 8(%rdi)
	cmpq	%rax, %r8
	je	.L63
	movq	%rax, (%rsi)
	movl	$40, 8(%rsi)
	movb	$0, 12(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L63:
	leaq	13(%rdi), %r9
	movq	%r8, %rsi
	movq	%r9, (%rdi)
	movq	%r9, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE3615:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_:
.LFB3617:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 12(%rdi)
	movq	%rsi, %rbx
	jne	.L69
.L65:
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movzbl	12(%rbx), %eax
	movb	%al, 12(%r12)
	movq	(%rbx), %rsi
	leaq	13(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L70
	movq	%rsi, (%r12)
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L69:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L70:
	leaq	13(%r12), %rdi
	movslq	8(%rbx), %rdx
	movq	%rdi, (%r12)
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3617:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv:
.LFB3618:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE3618:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv:
.LFB3619:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3619:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv:
.LFB3620:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	addq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3620:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3621:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3621:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3622:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3622:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci:
.LFB3623:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L86
	testl	%edx, %edx
	jle	.L86
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 12(%rdi)
	jne	.L89
.L78:
	movq	%rsi, (%rbx)
	movl	%edx, 8(%rbx)
	movb	$0, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L86:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L89:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	%edx, -28(%rbp)
	movq	%rsi, -24(%rbp)
	call	uprv_free_67@PLT
	movl	-28(%rbp), %edx
	movq	-24(%rbp), %rsi
	jmp	.L78
	.cfi_endproc
.LFE3623:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii:
.LFB3624:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L93
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movl	%esi, %r13d
	movl	%edx, %r12d
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L93
	testl	%r12d, %r12d
	jg	.L100
	cmpb	$0, 12(%rbx)
	jne	.L101
.L95:
	movq	%r14, (%rbx)
	movq	%r14, %rax
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L100:
	.cfi_restore_state
	cmpl	%r12d, 8(%rbx)
	cmovle	8(%rbx), %r12d
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	je	.L95
.L101:
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L93:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3624:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, .-_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi:
.LFB3625:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 12(%rdi)
	movq	%rdi, %rbx
	je	.L103
	movq	(%rdi), %r8
.L104:
	leaq	13(%rbx), %rax
	movl	%r12d, 0(%r13)
	movq	%rax, (%rbx)
	movq	%r8, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L103:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L107
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	cmovg	%eax, %r12d
	movslq	%r12d, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L107
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L107:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3625:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, .-_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv:
.LFB3626:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L114
	ret
	.p2align 4,,10
	.p2align 3
.L114:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3626:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv:
.LFB3627:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3627:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_:
.LFB3628:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3628:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_:
.LFB3629:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3629:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5ERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_:
.LFB3631:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3631:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_:
.LFB3633:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3633:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717CollationIterator8CEBufferD2Ev
	.type	_ZN6icu_6717CollationIterator8CEBufferD2Ev, @function
_ZN6icu_6717CollationIterator8CEBufferD2Ev:
.LFB3319:
	.cfi_startproc
	endbr64
	cmpb	$0, 20(%rdi)
	jne	.L122
	ret
	.p2align 4,,10
	.p2align 3
.L122:
	movq	8(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3319:
	.size	_ZN6icu_6717CollationIterator8CEBufferD2Ev, .-_ZN6icu_6717CollationIterator8CEBufferD2Ev
	.globl	_ZN6icu_6717CollationIterator8CEBufferD1Ev
	.set	_ZN6icu_6717CollationIterator8CEBufferD1Ev,_ZN6icu_6717CollationIterator8CEBufferD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717CollationIterator8CEBuffer20ensureAppendCapacityEiR10UErrorCode
	.type	_ZN6icu_6717CollationIterator8CEBuffer20ensureAppendCapacityEiR10UErrorCode, @function
_ZN6icu_6717CollationIterator8CEBuffer20ensureAppendCapacityEiR10UErrorCode:
.LFB3321:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rdi), %r14d
	movl	16(%rdi), %ebx
	addl	%r14d, %esi
	cmpl	%ebx, %esi
	jle	.L123
	movq	%rdx, %r13
	movl	(%rdx), %edx
	movq	%rdi, %r12
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L123
	.p2align 4,,10
	.p2align 3
.L127:
	leal	0(,%rbx,4), %eax
	leal	(%rbx,%rbx), %ecx
	cmpl	$999, %ebx
	cmovg	%ecx, %eax
	movl	%eax, %ebx
	cmpl	%eax, %esi
	jg	.L127
	testl	%eax, %eax
	jle	.L128
	movslq	%eax, %rdi
	salq	$3, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L128
	movq	8(%r12), %r13
	testl	%r14d, %r14d
	jg	.L138
.L129:
	cmpb	$0, 20(%r12)
	jne	.L139
.L130:
	movq	%r15, 8(%r12)
	movl	$1, %eax
	movl	%ebx, 16(%r12)
	movb	$1, 20(%r12)
.L123:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L138:
	.cfi_restore_state
	cmpl	%r14d, 16(%r12)
	movl	%r14d, %eax
	movq	%r13, %rsi
	movq	%r15, %rdi
	cmovle	16(%r12), %eax
	cmpl	%ebx, %eax
	cmovg	%ebx, %eax
	movslq	%eax, %rdx
	salq	$3, %rdx
	call	memcpy@PLT
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L139:
	movq	%r13, %rdi
	call	uprv_free_67@PLT
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L128:
	movl	$7, 0(%r13)
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3321:
	.size	_ZN6icu_6717CollationIterator8CEBuffer20ensureAppendCapacityEiR10UErrorCode, .-_ZN6icu_6717CollationIterator8CEBuffer20ensureAppendCapacityEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717CollationIteratorC2ERKS0_
	.type	_ZN6icu_6717CollationIteratorC2ERKS0_, @function
_ZN6icu_6717CollationIteratorC2ERKS0_:
.LFB3341:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6717CollationIteratorE(%rip), %rax
	movl	$0, -44(%rbp)
	movq	%rax, (%rdi)
	movq	8(%rsi), %rax
	movq	$0, 376(%rdi)
	movq	%rax, 8(%rdi)
	movq	16(%rsi), %rax
	movb	$0, 44(%rdi)
	movq	%rax, 16(%rdi)
	leaq	48(%rdi), %rax
	movq	%rax, 32(%rdi)
	movl	368(%rsi), %eax
	movl	$0, 24(%rdi)
	movl	24(%rsi), %r13d
	movl	%eax, 368(%rdi)
	movl	384(%rsi), %eax
	movl	$40, 40(%rdi)
	movl	%eax, 384(%rdi)
	movzbl	388(%rsi), %eax
	movb	%al, 388(%rdi)
	testl	%r13d, %r13d
	jg	.L141
.L143:
	movl	$0, 368(%rbx)
.L140:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L161
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L141:
	.cfi_restore_state
	movq	%rsi, %r12
	leaq	-44(%rbp), %rdx
	leaq	24(%rdi), %rdi
	movl	%r13d, %esi
	call	_ZN6icu_6717CollationIterator8CEBuffer20ensureAppendCapacityEiR10UErrorCode
	testb	%al, %al
	je	.L143
	movq	32(%rbx), %rdx
	movq	32(%r12), %rcx
	leal	-1(%r13), %esi
	leaq	15(%rdx), %rax
	subq	%rcx, %rax
	cmpq	$30, %rax
	jbe	.L144
	cmpl	$3, %esi
	jbe	.L144
	movl	%r13d, %esi
	xorl	%eax, %eax
	shrl	%esi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L145:
	movdqu	(%rcx,%rax), %xmm0
	movups	%xmm0, (%rdx,%rax)
	addq	$16, %rax
	cmpq	%rax, %rsi
	jne	.L145
	movl	%r13d, %eax
	andl	$-2, %eax
	testb	$1, %r13b
	je	.L147
	movq	(%rcx,%rax,8), %rcx
	movq	%rcx, (%rdx,%rax,8)
.L147:
	movl	%r13d, 24(%rbx)
	jmp	.L140
	.p2align 4,,10
	.p2align 3
.L144:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L148:
	movq	(%rcx,%rax,8), %rdi
	movq	%rdi, (%rdx,%rax,8)
	movq	%rax, %rdi
	addq	$1, %rax
	cmpq	%rdi, %rsi
	jne	.L148
	jmp	.L147
.L161:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3341:
	.size	_ZN6icu_6717CollationIteratorC2ERKS0_, .-_ZN6icu_6717CollationIteratorC2ERKS0_
	.globl	_ZN6icu_6717CollationIteratorC1ERKS0_
	.set	_ZN6icu_6717CollationIteratorC1ERKS0_,_ZN6icu_6717CollationIteratorC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717CollationIteratorD2Ev
	.type	_ZN6icu_6717CollationIteratorD2Ev, @function
_ZN6icu_6717CollationIteratorD2Ev:
.LFB3347:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6717CollationIteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	376(%rdi), %r13
	movq	%rdi, %r12
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L163
	leaq	72(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	8(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L163:
	cmpb	$0, 44(%r12)
	jne	.L169
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L169:
	.cfi_restore_state
	movq	32(%r12), %rdi
	call	uprv_free_67@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE3347:
	.size	_ZN6icu_6717CollationIteratorD2Ev, .-_ZN6icu_6717CollationIteratorD2Ev
	.globl	_ZN6icu_6717CollationIteratorD1Ev
	.set	_ZN6icu_6717CollationIteratorD1Ev,_ZN6icu_6717CollationIteratorD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717CollationIteratorD0Ev
	.type	_ZN6icu_6717CollationIteratorD0Ev, @function
_ZN6icu_6717CollationIteratorD0Ev:
.LFB3349:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6717CollationIteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	376(%rdi), %r13
	movq	%rdi, %r12
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L171
	leaq	72(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	8(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L171:
	cmpb	$0, 44(%r12)
	jne	.L177
.L172:
	movq	%r12, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L177:
	.cfi_restore_state
	movq	32(%r12), %rdi
	call	uprv_free_67@PLT
	jmp	.L172
	.cfi_endproc
.LFE3349:
	.size	_ZN6icu_6717CollationIteratorD0Ev, .-_ZN6icu_6717CollationIteratorD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717CollationIterator5resetEv
	.type	_ZN6icu_6717CollationIterator5resetEv, @function
_ZN6icu_6717CollationIterator5resetEv:
.LFB3351:
	.cfi_startproc
	endbr64
	movq	376(%rdi), %rax
	movl	$0, 24(%rdi)
	movl	$0, 368(%rdi)
	testq	%rax, %rax
	je	.L178
	movl	$0, 136(%rax)
	movzwl	16(%rax), %ecx
	movl	%ecx, %edx
	andl	$31, %edx
	andl	$1, %ecx
	movl	$2, %ecx
	cmovne	%ecx, %edx
	movw	%dx, 16(%rax)
.L178:
	ret
	.cfi_endproc
.LFE3351:
	.size	_ZN6icu_6717CollationIterator5resetEv, .-_ZN6icu_6717CollationIterator5resetEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717CollationIterator17getCE32FromPrefixEPKNS_13CollationDataEjR10UErrorCode
	.type	_ZN6icu_6717CollationIterator17getCE32FromPrefixEPKNS_13CollationDataEjR10UErrorCode, @function
_ZN6icu_6717CollationIterator17getCE32FromPrefixEPKNS_13CollationDataEjR10UErrorCode:
.LFB3361:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	shrl	$13, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rsi), %rax
	leaq	(%rax,%rdx,2), %rax
	movzwl	(%rax), %r12d
	movzwl	2(%rax), %edx
	addq	$4, %rax
	movq	$0, -96(%rbp)
	movq	%rax, -88(%rbp)
	sall	$16, %r12d
	movq	%rax, -80(%rbp)
	movl	$-1, -72(%rbp)
	orl	%edx, %r12d
	xorl	%r13d, %r13d
	leaq	-96(%rbp), %r15
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L188:
	movzwl	%dx, %esi
	cmpw	$16447, %dx
	ja	.L191
	sarl	$6, %esi
	leal	-1(%rsi), %r12d
	.p2align 4,,10
	.p2align 3
.L187:
	testb	$1, %al
	je	.L186
.L193:
	movq	(%rbx), %rax
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	*56(%rax)
	movl	%eax, %esi
	testl	%eax, %eax
	js	.L186
	movq	%r15, %rdi
	addl	$1, %r13d
	call	_ZN6icu_6710UCharsTrie16nextForCodePointEi@PLT
	cmpl	$1, %eax
	jle	.L187
	movq	-80(%rbp), %rcx
	movzwl	(%rcx), %edx
	testw	%dx, %dx
	jns	.L188
	movl	%edx, %esi
	movl	%edx, %r12d
	andw	$32767, %si
	andl	$32767, %r12d
	andb	$64, %dh
	je	.L187
	movzwl	2(%rcx), %edx
	cmpw	$32767, %si
	je	.L190
	subl	$16384, %r12d
	sall	$16, %r12d
	orl	%edx, %r12d
	testb	$1, %al
	jne	.L193
	.p2align 4,,10
	.p2align 3
.L186:
	movq	(%rbx), %rax
	movq	%r14, %rdx
	movl	%r13d, %esi
	movq	%rbx, %rdi
	call	*96(%rax)
	movq	%r15, %rdi
	call	_ZN6icu_6710UCharsTrieD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L200
	addq	$56, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L191:
	.cfi_restore_state
	movzwl	2(%rcx), %r12d
	cmpl	$32703, %esi
	jg	.L192
	andl	$32704, %edx
	subl	$16448, %edx
	sall	$10, %edx
	orl	%edx, %r12d
	jmp	.L187
	.p2align 4,,10
	.p2align 3
.L192:
	movzwl	4(%rcx), %edx
	sall	$16, %r12d
	orl	%edx, %r12d
	jmp	.L187
	.p2align 4,,10
	.p2align 3
.L190:
	movzwl	4(%rcx), %r12d
	sall	$16, %edx
	orl	%edx, %r12d
	jmp	.L187
.L200:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3361:
	.size	_ZN6icu_6717CollationIterator17getCE32FromPrefixEPKNS_13CollationDataEjR10UErrorCode, .-_ZN6icu_6717CollationIterator17getCE32FromPrefixEPKNS_13CollationDataEjR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717CollationIterator20nextSkippedCodePointER10UErrorCode
	.type	_ZN6icu_6717CollationIterator20nextSkippedCodePointER10UErrorCode, @function
_ZN6icu_6717CollationIterator20nextSkippedCodePointER10UErrorCode:
.LFB3362:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	376(%rdi), %r12
	movq	%rdi, %rbx
	testq	%r12, %r12
	je	.L202
	movswl	16(%r12), %eax
	movl	136(%r12), %r8d
	testw	%ax, %ax
	js	.L203
	sarl	$5, %eax
.L204:
	cmpl	%eax, %r8d
	jge	.L202
	leaq	8(%r12), %rdi
	movl	%r8d, %esi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	xorl	%edx, %edx
	cmpl	$65535, %eax
	seta	%dl
	addl	$1, %edx
	addl	%edx, 136(%r12)
.L201:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L202:
	.cfi_restore_state
	movl	384(%rbx), %eax
	testl	%eax, %eax
	je	.L212
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*48(%rax)
	movq	376(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L209
	movswl	16(%rdx), %ecx
	shrl	$5, %ecx
	jne	.L226
.L209:
	movl	384(%rbx), %edx
	testl	%edx, %edx
	jle	.L201
	testl	%eax, %eax
	js	.L201
.L210:
	subl	$1, %edx
	movl	%edx, 384(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L203:
	.cfi_restore_state
	movl	20(%r12), %eax
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L226:
	testl	%eax, %eax
	js	.L201
	addl	$1, 136(%rdx)
	movl	384(%rbx), %edx
	testl	%edx, %edx
	jg	.L210
	jmp	.L201
.L212:
	movl	$-1, %eax
	jmp	.L201
	.cfi_endproc
.LFE3362:
	.size	_ZN6icu_6717CollationIterator20nextSkippedCodePointER10UErrorCode, .-_ZN6icu_6717CollationIterator20nextSkippedCodePointER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717CollationIterator18backwardNumSkippedEiR10UErrorCode
	.type	_ZN6icu_6717CollationIterator18backwardNumSkippedEiR10UErrorCode, @function
_ZN6icu_6717CollationIterator18backwardNumSkippedEiR10UErrorCode:
.LFB3363:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	376(%rdi), %r13
	testq	%r13, %r13
	je	.L228
	movswl	16(%r13), %esi
	movl	%esi, %eax
	sarl	$5, %esi
	jne	.L240
.L228:
	movq	(%rbx), %rax
	movl	%r12d, %esi
	movq	%r14, %rdx
	movq	%rbx, %rdi
	call	*104(%rax)
	movl	384(%rbx), %esi
	testl	%esi, %esi
	js	.L227
	addl	%r12d, %esi
	movl	%esi, 384(%rbx)
.L227:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L240:
	.cfi_restore_state
	testw	%ax, %ax
	js	.L241
.L229:
	movl	136(%r13), %r8d
	movl	%r8d, %r15d
	subl	%esi, %r15d
	testl	%r15d, %r15d
	jle	.L230
	cmpl	%r15d, %r12d
	jg	.L231
	subl	%r12d, %r8d
	movl	%r8d, 136(%r13)
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L241:
	movl	20(%r13), %esi
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L231:
	movl	%r15d, %edx
	leaq	8(%r13), %rdi
	subl	%r12d, %edx
	movl	%r15d, %r12d
	call	_ZNK6icu_6713UnicodeString11moveIndex32Eii@PLT
	movl	%eax, 136(%r13)
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L230:
	movl	%r12d, %edx
	leaq	8(%r13), %rdi
	movl	%r8d, %esi
	xorl	%r12d, %r12d
	negl	%edx
	call	_ZNK6icu_6713UnicodeString11moveIndex32Eii@PLT
	movl	%eax, 136(%r13)
	jmp	.L228
	.cfi_endproc
.LFE3363:
	.size	_ZN6icu_6717CollationIterator18backwardNumSkippedEiR10UErrorCode, .-_ZN6icu_6717CollationIterator18backwardNumSkippedEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717CollationIterator23appendNumericSegmentCEsEPKciR10UErrorCode
	.type	_ZN6icu_6717CollationIterator23appendNumericSegmentCEsEPKciR10UErrorCode, @function
_ZN6icu_6717CollationIterator23appendNumericSegmentCEsEPKciR10UErrorCode:
.LFB3367:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	addq	$24, %rdi
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	-8(%rdi), %rax
	movsbl	(%rsi), %r10d
	movslq	24(%r13), %r9
	movl	56(%rax), %r14d
	cmpl	$7, %edx
	jg	.L243
	movl	%r10d, %eax
	cmpl	$1, %edx
	jle	.L244
	movsbl	1(%rsi), %ecx
	leal	(%r10,%r10,4), %eax
	leal	(%rcx,%rax,2), %eax
	cmpl	$2, %edx
	je	.L245
	leal	(%rax,%rax,4), %ecx
	movsbl	2(%rsi), %eax
	leal	(%rax,%rcx,2), %eax
	cmpl	$3, %edx
	je	.L246
	leal	(%rax,%rax,4), %ecx
	movsbl	3(%rsi), %eax
	leal	(%rax,%rcx,2), %eax
	cmpl	$4, %edx
	je	.L246
	leal	(%rax,%rax,4), %ecx
	movsbl	4(%rsi), %eax
	leal	(%rax,%rcx,2), %eax
	cmpl	$5, %edx
	je	.L246
	leal	(%rax,%rax,4), %ecx
	movsbl	5(%rsi), %eax
	leal	(%rax,%rcx,2), %eax
	cmpl	$6, %edx
	je	.L246
	leal	(%rax,%rax,4), %ecx
	movsbl	6(%rsi), %eax
	leal	(%rax,%rcx,2), %eax
.L246:
	cmpl	$73, %eax
	jle	.L267
	leal	-74(%rax), %ecx
	cmpl	$10159, %ecx
	jle	.L266
	subl	$10234, %eax
	cmpl	$1032255, %eax
	jle	.L305
.L243:
	leal	1(%rdx), %eax
	movl	%eax, %r8d
	shrl	$31, %r8d
	addl	%eax, %r8d
	movslq	%edx, %rax
	sarl	%r8d
	subl	$-128, %r8d
	sall	$16, %r8d
	orl	%r14d, %r8d
	cmpb	$0, -1(%rsi,%rax)
	jne	.L255
	leaq	-3(%rsi,%rax), %rcx
	jmp	.L256
	.p2align 4,,10
	.p2align 3
.L306:
	subq	$2, %rcx
	subl	$2, %edx
	cmpb	$0, 2(%rcx)
	jne	.L255
.L256:
	cmpb	$0, 1(%rcx)
	je	.L306
.L255:
	movl	%edx, %r11d
	andl	$1, %r11d
	je	.L307
.L258:
	leal	11(%r10,%r10), %eax
	cmpl	%edx, %r11d
	jge	.L270
	leal	-1(%rdx), %ebx
	movslq	%r11d, %rcx
	subq	%r11, %rbx
	leaq	2(%rsi,%rcx), %rdx
	leaq	(%rsi,%rcx), %r12
	movl	$8, %ecx
	andl	$4294967294, %ebx
	addq	%rdx, %rbx
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L260:
	sall	%cl, %eax
	subl	$8, %ecx
	orl	%eax, %r8d
.L263:
	movsbl	(%r12), %eax
	addq	$2, %r12
	leal	(%rax,%rax,4), %edx
	movsbl	-1(%r12), %eax
	leal	(%rax,%rdx,2), %eax
	leal	11(%rax,%rax), %eax
	cmpq	%r12, %rbx
	je	.L259
.L264:
	testl	%ecx, %ecx
	jne	.L260
	orl	%r8d, %eax
	salq	$32, %rax
	orq	$83887360, %rax
	movq	%rax, %r8
	cmpl	$39, %r9d
	jg	.L308
.L261:
	movq	32(%r13), %rdx
	leal	1(%r9), %eax
	movl	%eax, 24(%r13)
	movq	%r8, (%rdx,%r9,8)
	movslq	%eax, %r9
.L299:
	movl	%r14d, %r8d
	movl	$16, %ecx
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L270:
	movl	$8, %ecx
	.p2align 4,,10
	.p2align 3
.L259:
	subl	$1, %eax
	sall	%cl, %eax
	orl	%eax, %r8d
	salq	$32, %r8
	movq	%r8, %rbx
	orq	$83887360, %rbx
	cmpl	$39, %r9d
	jg	.L309
.L265:
	leal	1(%r9), %eax
	movl	%eax, 24(%r13)
	movq	32(%r13), %rax
	movq	%rbx, (%rax,%r9,8)
.L242:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L308:
	.cfi_restore_state
	movq	%r15, %rdx
	movl	$1, %esi
	movq	%rdi, -56(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN6icu_6717CollationIterator8CEBuffer20ensureAppendCapacityEiR10UErrorCode
	movslq	24(%r13), %r9
	movq	-56(%rbp), %rdi
	testb	%al, %al
	movq	-64(%rbp), %r8
	jne	.L261
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L307:
	movsbl	1(%rsi), %eax
	leal	(%r10,%r10,4), %ecx
	movl	$2, %r11d
	leal	(%rax,%rcx,2), %r10d
	jmp	.L258
	.p2align 4,,10
	.p2align 3
.L309:
	movq	%r15, %rdx
	movl	$1, %esi
	call	_ZN6icu_6717CollationIterator8CEBuffer20ensureAppendCapacityEiR10UErrorCode
	testb	%al, %al
	je	.L242
	movslq	24(%r13), %r9
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L244:
	leal	-74(%r10), %ecx
	cmpl	$73, %r10d
	jle	.L267
.L266:
	movl	%ecx, %edx
	movl	$2164392969, %eax
	shrl	%edx
	imulq	%rax, %rdx
	shrq	$38, %rdx
	leal	76(%rdx), %eax
	imull	$254, %edx, %edx
	sall	$16, %eax
	subl	%edx, %ecx
	leal	2(%rcx), %edx
	sall	$8, %edx
	orl	%edx, %eax
.L304:
	orl	%eax, %r14d
	salq	$32, %r14
	orq	$83887360, %r14
	cmpl	$39, %r9d
	jg	.L302
.L254:
	leal	1(%r9), %eax
	movl	%eax, 24(%r13)
	movq	32(%r13), %rax
	movq	%r14, (%rax,%r9,8)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L305:
	.cfi_restore_state
	movslq	%eax, %rdx
	movl	%eax, %ecx
	imulq	$-2130574327, %rdx, %rdx
	sarl	$31, %ecx
	shrq	$32, %rdx
	addl	%eax, %edx
	sarl	$7, %edx
	subl	%ecx, %edx
	imull	$254, %edx, %ecx
	subl	%ecx, %eax
	movl	%edx, %ecx
	addl	$2, %eax
	sarl	$31, %ecx
	orl	%eax, %r14d
	movslq	%edx, %rax
	imulq	$-2130574327, %rax, %rax
	shrq	$32, %rax
	addl	%edx, %eax
	sarl	$7, %eax
	subl	%ecx, %eax
	imull	$254, %eax, %ecx
	subl	%ecx, %edx
	addl	$2, %edx
	sall	$8, %edx
	orl	%r14d, %edx
	leal	116(%rax), %r14d
	sall	$16, %r14d
	orl	%edx, %r14d
	salq	$32, %r14
	orq	$83887360, %r14
	cmpl	$39, %r9d
	jle	.L254
.L302:
	movq	%r15, %rdx
	movl	$1, %esi
	call	_ZN6icu_6717CollationIterator8CEBuffer20ensureAppendCapacityEiR10UErrorCode
	testb	%al, %al
	je	.L242
	movslq	24(%r13), %r9
	jmp	.L254
.L245:
	cmpl	$73, %eax
	jg	.L310
.L267:
	addl	$2, %eax
	sall	$16, %eax
	jmp	.L304
.L310:
	leal	-74(%rax), %ecx
	jmp	.L266
	.cfi_endproc
.LFE3367:
	.size	_ZN6icu_6717CollationIterator23appendNumericSegmentCEsEPKciR10UErrorCode, .-_ZN6icu_6717CollationIterator23appendNumericSegmentCEsEPKciR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717CollationIterator16appendNumericCEsEjaR10UErrorCode
	.type	_ZN6icu_6717CollationIterator16appendNumericCEsEjaR10UErrorCode, @function
_ZN6icu_6717CollationIterator16appendNumericCEsEjaR10UErrorCode:
.LFB3366:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	xorl	%edi, %edi
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-128(%rbp), %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-115(%rbp), %rax
	movl	$0, -72(%rbp)
	movq	%rax, -128(%rbp)
	movl	$40, -120(%rbp)
	movw	%di, -116(%rbp)
	testb	%dl, %dl
	je	.L313
.L312:
	shrl	$8, %esi
	movq	%r15, %rdx
	movq	%rbx, %rdi
	andl	$15, %esi
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
	movl	384(%r14), %ecx
	testl	%ecx, %ecx
	je	.L315
	movq	(%r14), %rax
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	*48(%rax)
	testl	%eax, %eax
	js	.L315
	movq	16(%r14), %rcx
	movq	(%rcx), %rdx
	movq	16(%rdx), %rsi
	cmpl	$55295, %eax
	jg	.L317
	movl	%eax, %edi
	movq	(%rdx), %rdx
	andl	$31, %eax
	sarl	$5, %edi
	movslq	%edi, %rdi
	movzwl	(%rdx,%rdi,2), %edx
.L375:
	leal	(%rax,%rdx,4), %edx
	movslq	%edx, %rdx
	movl	(%rsi,%rdx,4), %esi
	cmpl	$192, %esi
	je	.L378
.L319:
	cmpb	$-65, %sil
	jbe	.L327
	movl	%esi, %eax
	andl	$15, %eax
	cmpl	$10, %eax
	je	.L379
.L327:
	movq	(%r14), %rax
	movq	%r15, %rdx
	movl	$1, %esi
	movq	%r14, %rdi
	call	*104(%rax)
.L315:
	movl	(%r15), %edx
	testl	%edx, %edx
	jg	.L350
	movl	-72(%rbp), %eax
	movq	-128(%rbp), %rcx
	xorl	%r12d, %r12d
	movl	$254, %r13d
	leal	-1(%rax), %edx
	.p2align 4,,10
	.p2align 3
.L344:
	movslq	%r12d, %rsi
	addq	%rcx, %rsi
	cmpl	%r12d, %edx
	jle	.L347
.L380:
	cmpb	$0, (%rsi)
	jne	.L347
	addl	$1, %r12d
	movslq	%r12d, %rsi
	addq	%rcx, %rsi
	cmpl	%r12d, %edx
	jg	.L380
	.p2align 4,,10
	.p2align 3
.L347:
	subl	%r12d, %eax
	movq	%r15, %rcx
	movq	%r14, %rdi
	cmpl	$254, %eax
	cmovg	%r13d, %eax
	movl	%eax, %edx
	movl	%eax, %ebx
	call	_ZN6icu_6717CollationIterator23appendNumericSegmentCEsEPKciR10UErrorCode
	movl	(%r15), %eax
	addl	%ebx, %r12d
	testl	%eax, %eax
	jg	.L350
	movl	-72(%rbp), %eax
	cmpl	%eax, %r12d
	jge	.L350
	movq	-128(%rbp), %rcx
	leal	-1(%rax), %edx
	jmp	.L344
	.p2align 4,,10
	.p2align 3
.L382:
	movl	%eax, %edi
	movq	(%rdx), %rdx
	andl	$31, %eax
	sarl	$5, %edi
	movslq	%edi, %rdi
	movzwl	(%rdx,%rdi,2), %edx
.L377:
	leal	(%rax,%rdx,4), %edx
	movslq	%edx, %rdx
	movl	(%rsi,%rdx,4), %esi
	cmpl	$192, %esi
	je	.L381
.L333:
	cmpb	$-65, %sil
	jbe	.L341
	movl	%esi, %eax
	andl	$15, %eax
	cmpl	$10, %eax
	jne	.L341
	.p2align 4,,10
	.p2align 3
.L313:
	shrl	$8, %esi
	movq	%rbx, %rdi
	movq	%r15, %rdx
	andl	$15, %esi
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
	movq	(%r14), %rax
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	*56(%rax)
	testl	%eax, %eax
	js	.L330
	movq	16(%r14), %rcx
	movq	(%rcx), %rdx
	movq	16(%rdx), %rsi
	cmpl	$55295, %eax
	jle	.L382
	cmpl	$65535, %eax
	jle	.L383
	cmpl	$1114111, %eax
	ja	.L337
	cmpl	44(%rdx), %eax
	jge	.L384
	movq	(%rdx), %r8
	movl	%eax, %edx
	movl	%eax, %edi
	sarl	$11, %edx
	sarl	$5, %edi
	addl	$2080, %edx
	andl	$63, %edi
	movslq	%edx, %rdx
	movzwl	(%r8,%rdx,2), %edx
	addl	%edi, %edx
	movslq	%edx, %rdx
	movzwl	(%r8,%rdx,2), %edi
	movl	%eax, %edx
	andl	$31, %edx
	leal	(%rdx,%rdi,4), %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
.L339:
	movl	(%rsi,%rdx), %esi
	cmpl	$192, %esi
	jne	.L333
	movq	32(%rcx), %rdx
	movq	(%rdx), %rcx
	movq	16(%rcx), %rdx
	cmpl	44(%rcx), %eax
	jl	.L385
	movslq	48(%rcx), %rax
	salq	$2, %rax
	jmp	.L340
	.p2align 4,,10
	.p2align 3
.L350:
	cmpb	$0, -116(%rbp)
	jne	.L386
.L311:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L387
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L317:
	.cfi_restore_state
	cmpl	$65535, %eax
	jle	.L388
	cmpl	$1114111, %eax
	ja	.L323
	cmpl	44(%rdx), %eax
	jge	.L389
	movq	(%rdx), %r8
	movl	%eax, %edx
	movl	%eax, %edi
	sarl	$11, %edx
	sarl	$5, %edi
	addl	$2080, %edx
	andl	$63, %edi
	movslq	%edx, %rdx
	movzwl	(%r8,%rdx,2), %edx
	addl	%edi, %edx
	movslq	%edx, %rdx
	movzwl	(%r8,%rdx,2), %edi
	movl	%eax, %edx
	andl	$31, %edx
	leal	(%rdx,%rdi,4), %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
.L325:
	movl	(%rsi,%rdx), %esi
	cmpl	$192, %esi
	jne	.L319
	movq	32(%rcx), %rdx
	movq	(%rdx), %rcx
	movq	16(%rcx), %rdx
	cmpl	44(%rcx), %eax
	jl	.L390
	movslq	48(%rcx), %rax
	salq	$2, %rax
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L341:
	movq	(%r14), %rax
	movq	%r15, %rdx
	movl	$1, %esi
	movq	%r14, %rdi
	call	*96(%rax)
.L330:
	movq	-128(%rbp), %rax
	movslq	-72(%rbp), %rdx
	leaq	-1(%rax,%rdx), %rdx
	cmpq	%rax, %rdx
	jbe	.L315
	.p2align 4,,10
	.p2align 3
.L342:
	movzbl	(%rax), %ecx
	movzbl	(%rdx), %esi
	addq	$1, %rax
	subq	$1, %rdx
	movb	%sil, -1(%rax)
	movb	%cl, 1(%rdx)
	cmpq	%rdx, %rax
	jb	.L342
	jmp	.L315
	.p2align 4,,10
	.p2align 3
.L386:
	movq	-128(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L311
	.p2align 4,,10
	.p2align 3
.L383:
	cmpl	$56320, %eax
	movq	(%rdx), %r8
	movl	$320, %edi
	movl	$0, %edx
	cmovl	%edi, %edx
	movl	%eax, %edi
	andl	$31, %eax
	sarl	$5, %edi
	addl	%edx, %edi
	movslq	%edi, %rdi
	movzwl	(%r8,%rdi,2), %edx
	jmp	.L377
	.p2align 4,,10
	.p2align 3
.L388:
	cmpl	$56320, %eax
	movq	(%rdx), %r8
	movl	$320, %edi
	movl	$0, %edx
	cmovl	%edi, %edx
	movl	%eax, %edi
	andl	$31, %eax
	sarl	$5, %edi
	addl	%edx, %edi
	movslq	%edi, %rdi
	movzwl	(%r8,%rdi,2), %edx
	jmp	.L375
	.p2align 4,,10
	.p2align 3
.L379:
	movl	384(%r14), %eax
	testl	%eax, %eax
	jle	.L312
	subl	$1, %eax
	movl	%eax, 384(%r14)
	jmp	.L312
	.p2align 4,,10
	.p2align 3
.L381:
	movq	32(%rcx), %rdx
	movq	(%rdx), %rcx
	movq	16(%rcx), %rdx
	movq	(%rcx), %rcx
	movzwl	(%rcx,%rdi,2), %ecx
	leal	(%rax,%rcx,4), %eax
	cltq
	salq	$2, %rax
.L340:
	movl	(%rdx,%rax), %esi
	jmp	.L333
	.p2align 4,,10
	.p2align 3
.L378:
	movq	32(%rcx), %rdx
	movq	(%rdx), %rcx
	movq	16(%rcx), %rdx
	movq	(%rcx), %rcx
	movzwl	(%rcx,%rdi,2), %ecx
	leal	(%rax,%rcx,4), %eax
	cltq
	salq	$2, %rax
.L326:
	movl	(%rdx,%rax), %esi
	jmp	.L319
	.p2align 4,,10
	.p2align 3
.L337:
	movl	512(%rsi), %esi
	cmpl	$192, %esi
	jne	.L333
	movq	32(%rcx), %rax
	movq	(%rax), %rax
	movq	16(%rax), %rdx
	movl	$512, %eax
	jmp	.L340
.L323:
	movl	512(%rsi), %esi
	cmpl	$192, %esi
	jne	.L319
	movq	32(%rcx), %rax
	movq	(%rax), %rax
	movq	16(%rax), %rdx
	movl	$512, %eax
	jmp	.L326
.L384:
	movslq	48(%rdx), %rdx
	salq	$2, %rdx
	jmp	.L339
.L389:
	movslq	48(%rdx), %rdx
	salq	$2, %rdx
	jmp	.L325
.L390:
	movq	(%rcx), %rdi
	movl	%eax, %ecx
	movl	%eax, %esi
	andl	$31, %eax
	sarl	$11, %ecx
	sarl	$5, %esi
	addl	$2080, %ecx
	andl	$63, %esi
	movslq	%ecx, %rcx
	movzwl	(%rdi,%rcx,2), %ecx
	addl	%esi, %ecx
	movslq	%ecx, %rcx
	movzwl	(%rdi,%rcx,2), %ecx
	leal	(%rax,%rcx,4), %eax
	cltq
	salq	$2, %rax
	jmp	.L326
.L385:
	movq	(%rcx), %rdi
	movl	%eax, %ecx
	movl	%eax, %esi
	andl	$31, %eax
	sarl	$11, %ecx
	sarl	$5, %esi
	addl	$2080, %ecx
	andl	$63, %esi
	movslq	%ecx, %rcx
	movzwl	(%rdi,%rcx,2), %ecx
	addl	%esi, %ecx
	movslq	%ecx, %rcx
	movzwl	(%rdi,%rcx,2), %ecx
	leal	(%rax,%rcx,4), %eax
	cltq
	salq	$2, %rax
	jmp	.L340
.L387:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3366:
	.size	_ZN6icu_6717CollationIterator16appendNumericCEsEjaR10UErrorCode, .-_ZN6icu_6717CollationIterator16appendNumericCEsEjaR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717CollationIterator17appendCEsFromCE32EPKNS_13CollationDataEijaR10UErrorCode
	.type	_ZN6icu_6717CollationIterator17appendCEsFromCE32EPKNS_13CollationDataEijaR10UErrorCode, @function
_ZN6icu_6717CollationIterator17appendCEsFromCE32EPKNS_13CollationDataEijaR10UErrorCode:
.LFB3360:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r9, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%ecx, %r12d
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movb	%r8b, -56(%rbp)
	cmpb	$-65, %cl
	jbe	.L599
	movq	%rsi, %r15
	movl	%edx, %r10d
	leaq	.L398(%rip), %rbx
	movl	%r8d, %r11d
.L392:
	movl	%r12d, %eax
	andl	$15, %eax
	movslq	(%rbx,%rax,4), %rax
	addq	%rbx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L398:
	.long	.L396-.L398
	.long	.L411-.L398
	.long	.L410-.L398
	.long	.L396-.L398
	.long	.L409-.L398
	.long	.L408-.L398
	.long	.L407-.L398
	.long	.L406-.L398
	.long	.L405-.L398
	.long	.L404-.L398
	.long	.L403-.L398
	.long	.L402-.L398
	.long	.L401-.L398
	.long	.L400-.L398
	.long	.L399-.L398
	.long	.L397-.L398
	.text
	.p2align 4,,10
	.p2align 3
.L406:
	movq	0(%r13), %rax
	leaq	_ZN6icu_6717CollationIterator22getCE32FromBuilderDataEjR10UErrorCode(%rip), %rdx
	movq	120(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L424
.L396:
	movl	(%r14), %r11d
	testl	%r11d, %r11d
	jle	.L610
.L391:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L397:
	.cfi_restore_state
	movl	%r10d, %eax
	andl	$-2048, %eax
	cmpl	$55296, %eax
	jne	.L465
	movq	0(%r13), %rax
	leaq	_ZN6icu_6717CollationIterator18foundNULTerminatorEv(%rip), %rdx
	movq	88(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L611
.L465:
	movl	%r10d, %edi
	call	_ZN6icu_679Collation30unassignedPrimaryFromCodePointEi@PLT
	movslq	24(%r13), %rbx
	movl	%eax, %r12d
	leal	1(%rbx), %eax
	cmpl	$39, %ebx
	jg	.L612
.L463:
	movl	%eax, 24(%r13)
	movq	%r12, %rax
	movq	32(%r13), %rdx
	salq	$32, %rax
	orq	$83887360, %rax
	movq	%rax, (%rdx,%rbx,8)
	jmp	.L391
	.p2align 4,,10
	.p2align 3
.L411:
	salq	$32, %r12
	movslq	24(%r13), %rax
	movabsq	$-1099511627776, %rcx
	andq	%rcx, %r12
	orq	$83887360, %r12
	cmpl	$39, %eax
	jg	.L609
.L416:
	leal	1(%rax), %edx
	movl	%edx, 24(%r13)
	movq	32(%r13), %rdx
	movq	%r12, (%rdx,%rax,8)
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L410:
	.cfi_restore_state
	movslq	24(%r13), %rax
	andl	$-256, %r12d
	cmpl	$39, %eax
	jle	.L416
.L609:
	leaq	24(%r13), %rdi
	movq	%r14, %rdx
	movl	$1, %esi
	call	_ZN6icu_6717CollationIterator8CEBuffer20ensureAppendCapacityEiR10UErrorCode
	testb	%al, %al
	je	.L391
	movslq	24(%r13), %rax
	jmp	.L416
	.p2align 4,,10
	.p2align 3
.L408:
	movl	%r12d, %esi
	leaq	24(%r13), %rdi
	movq	%r14, %rdx
	movq	8(%r15), %rbx
	shrl	$8, %esi
	andl	$31, %esi
	movl	%esi, %r15d
	call	_ZN6icu_6717CollationIterator8CEBuffer20ensureAppendCapacityEiR10UErrorCode
	testb	%al, %al
	je	.L391
	movslq	24(%r13), %rax
	shrl	$13, %r12d
	movq	32(%r13), %rdi
	movabsq	$-281474976710656, %r9
	leal	1(%rax), %esi
	subq	%rax, %r12
	movslq	%esi, %rdx
	leaq	(%rbx,%r12,4), %r8
	addl	%r15d, %esi
	jmp	.L419
	.p2align 4,,10
	.p2align 3
.L417:
	subl	%r10d, %ecx
	andl	$15, %eax
	movq	%rcx, %r10
	salq	$32, %r10
	orq	$83887360, %r10
	cmpl	$1, %eax
	cmove	%r10, %rcx
.L418:
	movl	%edx, 24(%r13)
	movl	%esi, %eax
	movq	%rcx, -8(%rdi,%rdx,8)
	addq	$1, %rdx
	subl	%edx, %eax
	testl	%eax, %eax
	jle	.L391
.L419:
	movl	-4(%r8,%rdx,4), %eax
	movzbl	%al, %r10d
	movl	%eax, %ecx
	cmpb	$-65, %al
	ja	.L417
	sall	$16, %ecx
	salq	$32, %rax
	andl	$-16777216, %ecx
	andq	%r9, %rax
	sall	$8, %r10d
	orq	%rcx, %rax
	movl	%r10d, %ecx
	orq	%rax, %rcx
	jmp	.L418
	.p2align 4,,10
	.p2align 3
.L407:
	movl	%r12d, %ebx
	leaq	24(%r13), %rdi
	movq	%r14, %rdx
	movq	16(%r15), %r15
	shrl	$8, %ebx
	andl	$31, %ebx
	movl	%ebx, %esi
	call	_ZN6icu_6717CollationIterator8CEBuffer20ensureAppendCapacityEiR10UErrorCode
	testb	%al, %al
	je	.L391
	movslq	24(%r13), %rax
	movq	32(%r13), %rsi
	shrl	$13, %r12d
	leaq	16(%r15,%r12,8), %r8
	leaq	(%r15,%r12,8), %rdx
	movq	%rax, %rdi
	salq	$3, %rax
	leaq	(%rsi,%rax), %rcx
	leaq	16(%rsi,%rax), %rax
	cmpq	%r8, %rcx
	setnb	%r8b
	cmpq	%rax, %rdx
	setnb	%al
	orb	%al, %r8b
	je	.L477
	cmpl	$4, %ebx
	jle	.L477
	testl	%ebx, %ebx
	movl	$1, %r8d
	movdqu	(%rdx), %xmm2
	cmovg	%ebx, %r8d
	movups	%xmm2, (%rcx)
	movl	%r8d, %eax
	shrl	%eax
	cmpl	$1, %eax
	je	.L421
	movdqu	16(%rdx), %xmm3
	movups	%xmm3, 16(%rcx)
	cmpl	$2, %eax
	je	.L421
	movdqu	32(%rdx), %xmm4
	movups	%xmm4, 32(%rcx)
	cmpl	$3, %eax
	je	.L421
	movdqu	48(%rdx), %xmm5
	movups	%xmm5, 48(%rcx)
	cmpl	$4, %eax
	je	.L421
	movdqu	64(%rdx), %xmm6
	movups	%xmm6, 64(%rcx)
	cmpl	$5, %eax
	je	.L421
	movdqu	80(%rdx), %xmm7
	movups	%xmm7, 80(%rcx)
	cmpl	$6, %eax
	je	.L421
	movdqu	96(%rdx), %xmm7
	movups	%xmm7, 96(%rcx)
	cmpl	$7, %eax
	je	.L421
	movdqu	112(%rdx), %xmm6
	movups	%xmm6, 112(%rcx)
	cmpl	$8, %eax
	je	.L421
	movdqu	128(%rdx), %xmm5
	movups	%xmm5, 128(%rcx)
	cmpl	$9, %eax
	je	.L421
	movdqu	144(%rdx), %xmm4
	movups	%xmm4, 144(%rcx)
	cmpl	$10, %eax
	je	.L421
	movdqu	160(%rdx), %xmm7
	movups	%xmm7, 160(%rcx)
	cmpl	$11, %eax
	je	.L421
	movdqu	176(%rdx), %xmm6
	movups	%xmm6, 176(%rcx)
	cmpl	$12, %eax
	je	.L421
	movdqu	192(%rdx), %xmm3
	movups	%xmm3, 192(%rcx)
	cmpl	$13, %eax
	je	.L421
	movdqu	208(%rdx), %xmm5
	movups	%xmm5, 208(%rcx)
	cmpl	$15, %eax
	jne	.L421
	movdqu	224(%rdx), %xmm6
	movups	%xmm6, 224(%rcx)
	.p2align 4,,10
	.p2align 3
.L421:
	movl	%r8d, %ecx
	andl	$-2, %ecx
	movl	%ecx, %eax
	leaq	(%rdx,%rax,8), %rdx
	leal	(%rcx,%rdi), %eax
	cmpl	%r8d, %ecx
	je	.L423
	movq	(%rdx), %rdx
	cltq
	movq	%rdx, (%rsi,%rax,8)
.L423:
	movl	$1, %eax
	testl	%ebx, %ebx
	cmovle	%eax, %ebx
	leal	(%rdi,%rbx), %eax
	movl	%eax, 24(%r13)
	jmp	.L391
	.p2align 4,,10
	.p2align 3
.L409:
	leaq	24(%r13), %rdi
	movq	%r14, %rdx
	movl	$2, %esi
	call	_ZN6icu_6717CollationIterator8CEBuffer20ensureAppendCapacityEiR10UErrorCode
	testb	%al, %al
	je	.L391
	movq	%r12, %rax
	movl	24(%r13), %ecx
	movabsq	$-72057594037927936, %rdx
	salq	$32, %rax
	andq	%rdx, %rax
	movl	%r12d, %edx
	sall	$16, %r12d
	shrl	$8, %edx
	andl	$-16777216, %r12d
	andl	$65280, %edx
	orl	$1280, %r12d
	orq	%rdx, %rax
	movq	%r12, %xmm1
	movslq	%ecx, %rdx
	orq	$83886080, %rax
	movq	%rax, %xmm0
	movq	32(%r13), %rax
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rax,%rdx,8)
	leal	2(%rcx), %eax
	movl	%eax, 24(%r13)
	jmp	.L391
	.p2align 4,,10
	.p2align 3
.L405:
	cmpb	$0, -56(%rbp)
	movl	%r11d, -68(%rbp)
	movl	%r10d, -64(%rbp)
	je	.L431
	movq	0(%r13), %rax
	movq	%r14, %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	*104(%rax)
	movl	%r12d, %edx
	movq	%r14, %rcx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6717CollationIterator17getCE32FromPrefixEPKNS_13CollationDataEjR10UErrorCode
	movq	%r14, %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	movl	%eax, %r12d
	movq	0(%r13), %rax
	call	*96(%rax)
	movl	-64(%rbp), %r10d
	movl	-68(%rbp), %r11d
	.p2align 4,,10
	.p2align 3
.L425:
	cmpb	$-65, %r12b
	ja	.L392
.L599:
	movabsq	$-281474976710656, %rdx
	movq	%r12, %rax
	salq	$32, %rax
	andq	%rdx, %rax
	movl	%r12d, %edx
	sall	$8, %r12d
	sall	$16, %edx
	andl	$65280, %r12d
	andl	$-16777216, %edx
	orq	%rdx, %rax
	orq	%rax, %r12
	jmp	.L395
	.p2align 4,,10
	.p2align 3
.L404:
	movq	24(%r15), %rdx
	movl	%r12d, %eax
	shrl	$13, %eax
	leaq	(%rdx,%rax,2), %rdx
	movzwl	(%rdx), %r8d
	movzwl	2(%rdx), %eax
	sall	$16, %r8d
	orl	%eax, %r8d
	cmpb	$0, -56(%rbp)
	je	.L483
	cmpq	$0, 376(%r13)
	je	.L613
.L432:
	movq	%r14, %rsi
	movq	%r13, %rdi
	movl	%r11d, -72(%rbp)
	movl	%r10d, -68(%rbp)
	movl	%r8d, -64(%rbp)
	movq	%rdx, -80(%rbp)
	call	_ZN6icu_6717CollationIterator20nextSkippedCodePointER10UErrorCode
	movl	-64(%rbp), %r8d
	movl	-68(%rbp), %r10d
	testl	%eax, %eax
	movl	-72(%rbp), %r11d
	movl	%eax, %r9d
	js	.L483
	testl	$512, %r12d
	movq	-80(%rbp), %rdx
	je	.L433
	cmpl	$767, %eax
	jle	.L436
	cmpl	$65535, %eax
	jle	.L437
	sarl	$10, %eax
	subw	$10304, %ax
	movzwl	%ax, %eax
.L437:
	movl	%eax, %ecx
	leaq	_ZN6icu_6712CollationFCD9lcccIndexE(%rip), %rdi
	sarl	$5, %ecx
	movslq	%ecx, %rcx
	movzbl	(%rdi,%rcx), %ecx
	testl	%ecx, %ecx
	je	.L436
	leaq	_ZN6icu_6712CollationFCD8lcccBitsE(%rip), %rdi
	movl	(%rdi,%rcx,4), %ecx
	btl	%eax, %ecx
	jc	.L433
.L436:
	movq	%r14, %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	movl	%r11d, -72(%rbp)
	movl	%r10d, -68(%rbp)
	movl	%r8d, -64(%rbp)
	call	_ZN6icu_6717CollationIterator18backwardNumSkippedEiR10UErrorCode
	movl	-64(%rbp), %r8d
	movl	-68(%rbp), %r10d
	movl	-72(%rbp), %r11d
	movl	%r8d, %r12d
	jmp	.L425
	.p2align 4,,10
	.p2align 3
.L403:
	cmpb	$0, 388(%r13)
	jne	.L614
	movl	%r12d, %eax
	shrl	$13, %eax
	movl	%eax, %r12d
	movq	8(%r15), %rax
	movl	(%rax,%r12,4), %r12d
	jmp	.L425
	.p2align 4,,10
	.p2align 3
.L402:
	cmpb	$0, -56(%rbp)
	je	.L440
	movq	0(%r13), %rax
	leaq	_ZN6icu_6717CollationIterator18foundNULTerminatorEv(%rip), %rdi
	movq	80(%rax), %rax
	cmpq	%rdi, %rax
	jne	.L615
.L440:
	movq	8(%r15), %rax
	movl	(%rax), %r12d
	jmp	.L425
	.p2align 4,,10
	.p2align 3
.L401:
	subl	$44032, %r10d
	movq	40(%r15), %rax
	movslq	%r10d, %rdx
	movl	%r10d, %ecx
	imulq	$-1840700269, %rdx, %rdx
	sarl	$31, %ecx
	movq	%rax, -64(%rbp)
	shrq	$32, %rdx
	addl	%r10d, %edx
	sarl	$4, %edx
	movl	%edx, %eax
	subl	%ecx, %edx
	subl	%ecx, %eax
	movl	%edx, %ecx
	imull	$28, %eax, %eax
	sarl	$31, %ecx
	subl	%eax, %r10d
	movl	%r10d, %eax
	movslq	%edx, %r10
	imulq	$818089009, %r10, %r10
	sarq	$34, %r10
	subl	%ecx, %r10d
	movslq	%r10d, %rcx
	leal	(%rcx,%rcx,4), %esi
	leal	(%rcx,%rsi,4), %esi
	subl	%esi, %edx
	andl	$256, %r12d
	jne	.L616
	movl	%eax, -68(%rbp)
	movq	-64(%rbp), %rax
	movq	%r14, %r9
	movq	%r15, %rsi
	movsbl	-56(%rbp), %r12d
	movl	%edx, -72(%rbp)
	movq	%r13, %rdi
	movl	$-1, %edx
	movl	(%rax,%rcx,4), %ecx
	movl	%r11d, -80(%rbp)
	movl	%r12d, %r8d
	call	_ZN6icu_6717CollationIterator17appendCEsFromCE32EPKNS_13CollationDataEijaR10UErrorCode
	movl	-72(%rbp), %r10d
	movq	-64(%rbp), %rax
	movq	%r14, %r9
	movl	%r12d, %r8d
	movl	$-1, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	addl	$19, %r10d
	movslq	%r10d, %r10
	movl	(%rax,%r10,4), %ecx
	call	_ZN6icu_6717CollationIterator17appendCEsFromCE32EPKNS_13CollationDataEijaR10UErrorCode
	movl	-68(%rbp), %eax
	testl	%eax, %eax
	je	.L391
	movq	-64(%rbp), %rdi
	addl	$39, %eax
	movl	-80(%rbp), %r11d
	movl	$-1, %r10d
	cltq
	movl	(%rdi,%rax,4), %r12d
	jmp	.L425
	.p2align 4,,10
	.p2align 3
.L400:
	movq	0(%r13), %rax
	leaq	_ZN6icu_6717CollationIterator23handleGetTrailSurrogateEv(%rip), %rdi
	movq	72(%rax), %rax
	cmpq	%rdi, %rax
	jne	.L617
.L455:
	movl	$-1, %r12d
	jmp	.L392
	.p2align 4,,10
	.p2align 3
.L399:
	movq	16(%r15), %rax
	shrl	$13, %r12d
	movl	%r10d, %edi
	movq	(%rax,%r12,8), %rsi
	call	_ZN6icu_679Collation32getThreeBytePrimaryForOffsetDataEil@PLT
	salq	$32, %rax
	orq	$83887360, %rax
	movq	%rax, %rbx
	movslq	24(%r13), %rax
	cmpl	$39, %eax
	jg	.L618
.L461:
	leal	1(%rax), %edx
	movl	%edx, 24(%r13)
	movq	32(%r13), %rdx
	movq	%rbx, (%rdx,%rax,8)
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L610:
	.cfi_restore_state
	movl	$5, (%r14)
	jmp	.L391
	.p2align 4,,10
	.p2align 3
.L483:
	movl	%r8d, %r12d
	jmp	.L425
	.p2align 4,,10
	.p2align 3
.L431:
	movl	%r12d, %edx
	movq	%r14, %rcx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6717CollationIterator17getCE32FromPrefixEPKNS_13CollationDataEjR10UErrorCode
	movl	-64(%rbp), %r10d
	movl	-68(%rbp), %r11d
	movl	%eax, %r12d
	jmp	.L425
	.p2align 4,,10
	.p2align 3
.L613:
	movl	384(%r13), %r9d
	testl	%r9d, %r9d
	jns	.L432
	movq	0(%r13), %rax
	movl	%r11d, -72(%rbp)
	movq	%r14, %rsi
	movq	%r13, %rdi
	movl	%r10d, -68(%rbp)
	movl	%r8d, -64(%rbp)
	movq	%rdx, -80(%rbp)
	call	*48(%rax)
	movl	-64(%rbp), %r8d
	movl	-68(%rbp), %r10d
	testl	%eax, %eax
	movl	-72(%rbp), %r11d
	movl	%eax, %r9d
	js	.L483
	testl	$512, %r12d
	movq	-80(%rbp), %rdx
	je	.L433
	cmpl	$767, %eax
	jle	.L434
	cmpl	$65535, %eax
	jle	.L435
	sarl	$10, %eax
	subw	$10304, %ax
	movzwl	%ax, %eax
.L435:
	movl	%eax, %ecx
	leaq	_ZN6icu_6712CollationFCD9lcccIndexE(%rip), %rdi
	sarl	$5, %ecx
	movslq	%ecx, %rcx
	movzbl	(%rdi,%rcx), %ecx
	testl	%ecx, %ecx
	je	.L434
	leaq	_ZN6icu_6712CollationFCD8lcccBitsE(%rip), %rdi
	movl	(%rdi,%rcx,4), %ecx
	btl	%eax, %ecx
	jc	.L433
.L434:
	movq	0(%r13), %rax
	movl	%r11d, -72(%rbp)
	movq	%r14, %rdx
	movl	$1, %esi
	movl	%r10d, -68(%rbp)
	movq	%r13, %rdi
	movl	%r8d, -64(%rbp)
	call	*104(%rax)
	movl	-64(%rbp), %r8d
	movl	-68(%rbp), %r10d
	movl	-72(%rbp), %r11d
	movl	%r8d, %r12d
	jmp	.L425
	.p2align 4,,10
	.p2align 3
.L424:
	movl	%r10d, -64(%rbp)
	movl	%r12d, %esi
	movq	%r14, %rdx
	movq	%r13, %rdi
	movl	%r11d, -68(%rbp)
	call	*%rax
	movl	(%r14), %r10d
	movl	%eax, %r12d
	testl	%r10d, %r10d
	jg	.L391
	cmpl	$192, %eax
	movl	-64(%rbp), %r10d
	movl	-68(%rbp), %r11d
	jne	.L425
	movq	16(%r13), %rax
	movq	32(%rax), %r15
	movq	(%r15), %rcx
	movq	16(%rcx), %rdx
	cmpl	$55295, %r10d
	ja	.L426
	movl	%r10d, %eax
	movq	(%rcx), %rcx
	sarl	$5, %eax
	cltq
.L604:
	movzwl	(%rcx,%rax,2), %ecx
.L605:
	movl	%r10d, %eax
	andl	$31, %eax
	leal	(%rax,%rcx,4), %eax
	cltq
	salq	$2, %rax
.L460:
	movl	(%rdx,%rax), %r12d
	jmp	.L425
	.p2align 4,,10
	.p2align 3
.L617:
	movl	%r11d, -68(%rbp)
	movq	%r13, %rdi
	movl	%r10d, -64(%rbp)
	call	*%rax
	movl	-64(%rbp), %r10d
	movl	-68(%rbp), %r11d
	movzwl	%ax, %edx
	andl	$64512, %eax
	cmpl	$56320, %eax
	jne	.L455
	sall	$10, %r10d
	movl	%r12d, %eax
	leal	-56613888(%rdx,%r10), %r10d
	andl	$768, %eax
	je	.L455
	cmpl	$256, %eax
	je	.L456
	movq	(%r15), %rax
	movq	16(%rax), %rdx
	cmpl	44(%rax), %r10d
	jl	.L457
	movslq	48(%rax), %rax
	salq	$2, %rax
.L458:
	movl	(%rdx,%rax), %r12d
	cmpl	$192, %r12d
	jne	.L425
.L456:
	movq	32(%r15), %r15
	movq	(%r15), %rax
	movq	16(%rax), %rdx
	cmpl	44(%rax), %r10d
	jl	.L459
	movslq	48(%rax), %rax
	salq	$2, %rax
	jmp	.L460
	.p2align 4,,10
	.p2align 3
.L618:
	leaq	24(%r13), %rdi
	movq	%r14, %rdx
	movl	$1, %esi
	call	_ZN6icu_6717CollationIterator8CEBuffer20ensureAppendCapacityEiR10UErrorCode
	testb	%al, %al
	je	.L391
	movslq	24(%r13), %rax
	jmp	.L461
	.p2align 4,,10
	.p2align 3
.L433:
	subq	$8, %rsp
	leaq	4(%rdx), %rcx
	movq	%r15, %rsi
	movl	%r12d, %edx
	pushq	%r14
	movq	%r13, %rdi
	movl	%r11d, -68(%rbp)
	movl	%r10d, -64(%rbp)
	call	_ZN6icu_6717CollationIterator23nextCE32FromContractionEPKNS_13CollationDataEjPKDsjiR10UErrorCode
	popq	%rsi
	movl	-64(%rbp), %r10d
	cmpl	$1, %eax
	movl	-68(%rbp), %r11d
	movl	%eax, %r12d
	popq	%rdi
	jne	.L425
	jmp	.L391
	.p2align 4,,10
	.p2align 3
.L611:
	movl	%r10d, -56(%rbp)
	movq	%r13, %rdi
	call	*%rax
	movl	-56(%rbp), %r10d
	testb	%al, %al
	je	.L465
	movabsq	$-844424846244608, %r12
.L395:
	movslq	24(%r13), %rbx
	leal	1(%rbx), %eax
	cmpl	$39, %ebx
	jg	.L470
.L606:
	movq	32(%r13), %r15
.L471:
	movl	%eax, 24(%r13)
	movq	%r12, (%r15,%rbx,8)
	jmp	.L391
	.p2align 4,,10
	.p2align 3
.L426:
	cmpl	$65535, %r10d
	ja	.L428
	cmpl	$56320, %r10d
	movq	(%rcx), %rsi
	movl	$320, %eax
	movl	$0, %ecx
	cmovl	%eax, %ecx
	movl	%r10d, %eax
	sarl	$5, %eax
	addl	%ecx, %eax
	cltq
	movzwl	(%rsi,%rax,2), %ecx
	jmp	.L605
.L470:
	movl	40(%r13), %ecx
	cmpl	%eax, %ecx
	jge	.L606
	movl	(%r14), %edx
	testl	%edx, %edx
	jg	.L391
	.p2align 4,,10
	.p2align 3
.L475:
	leal	0(,%rcx,4), %edx
	leal	(%rcx,%rcx), %esi
	cmpl	$999, %ecx
	movl	%edx, %ecx
	cmovg	%esi, %ecx
	cmpl	%eax, %ecx
	jl	.L475
	movslq	%ecx, %rdi
	movl	%ecx, -56(%rbp)
	salq	$3, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L445
	movl	-56(%rbp), %ecx
	cmpl	%ebx, 40(%r13)
	movq	%rax, %rdi
	cmovle	40(%r13), %ebx
	movq	32(%r13), %r14
	cmpl	%ecx, %ebx
	movq	%r14, %rsi
	cmovg	%ecx, %ebx
	movslq	%ebx, %rdx
	salq	$3, %rdx
	call	memcpy@PLT
	cmpb	$0, 44(%r13)
	movl	-56(%rbp), %ecx
	jne	.L619
.L476:
	movslq	24(%r13), %rbx
	movq	%r15, 32(%r13)
	movl	%ecx, 40(%r13)
	movb	$1, 44(%r13)
	leal	1(%rbx), %eax
	jmp	.L471
.L477:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L420:
	movq	(%rdx,%rax,8), %rsi
	movq	%rsi, (%rcx,%rax,8)
	addq	$1, %rax
	movl	%ebx, %esi
	subl	%eax, %esi
	testl	%esi, %esi
	jg	.L420
	jmp	.L423
.L615:
	movl	%r11d, -68(%rbp)
	movq	%r13, %rdi
	movl	%r10d, -64(%rbp)
	call	*%rax
	movl	-64(%rbp), %r10d
	movl	-68(%rbp), %r11d
	testb	%al, %al
	je	.L440
	movslq	24(%r13), %rbx
	cmpl	$39, %ebx
	jg	.L620
.L441:
	movabsq	$4311744768, %rdi
	leal	1(%rbx), %eax
	movl	%eax, 24(%r13)
	movq	32(%r13), %rax
	movq	%rdi, (%rax,%rbx,8)
	jmp	.L391
.L428:
	movl	$512, %eax
	cmpl	$1114111, %r10d
	ja	.L460
	cmpl	%r10d, 44(%rcx)
	jg	.L430
	movslq	48(%rcx), %rax
	salq	$2, %rax
	jmp	.L460
.L457:
	movq	(%rax), %rcx
	movl	%r10d, %eax
	movl	%r10d, %esi
	sarl	$11, %eax
	sarl	$5, %esi
	addl	$2080, %eax
	andl	$63, %esi
	cltq
	movzwl	(%rcx,%rax,2), %eax
	addl	%esi, %eax
	cltq
	movzwl	(%rcx,%rax,2), %ecx
	movl	%r10d, %eax
	andl	$31, %eax
	leal	(%rax,%rcx,4), %eax
	cltq
	salq	$2, %rax
	jmp	.L458
.L459:
	movq	(%rax), %rcx
.L603:
	movl	%r10d, %eax
	movl	%r10d, %esi
	sarl	$11, %eax
	sarl	$5, %esi
	addl	$2080, %eax
	andl	$63, %esi
	cltq
	movzwl	(%rcx,%rax,2), %eax
	addl	%esi, %eax
	cltq
	jmp	.L604
.L430:
	movq	(%rcx), %rcx
	jmp	.L603
.L445:
	movl	$7, (%r14)
	jmp	.L391
.L614:
	leaq	-40(%rbp), %rsp
	movq	%r14, %rcx
	movl	%r12d, %esi
	movq	%r13, %rdi
	popq	%rbx
	movsbl	%r11b, %edx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6717CollationIterator16appendNumericCEsEjaR10UErrorCode
.L616:
	.cfi_restore_state
	xorl	%esi, %esi
	testl	%eax, %eax
	movl	%edx, -68(%rbp)
	leaq	24(%r13), %rdi
	setne	%sil
	movq	%r14, %rdx
	movl	%ecx, -56(%rbp)
	movl	%eax, %ebx
	addl	$2, %esi
	call	_ZN6icu_6717CollationIterator8CEBuffer20ensureAppendCapacityEiR10UErrorCode
	testb	%al, %al
	je	.L391
	movslq	-56(%rbp), %rcx
	movq	-64(%rbp), %rax
	movl	-68(%rbp), %r10d
	movl	(%rax,%rcx,4), %edx
	cmpb	$-65, %dl
	movzbl	%dl, %ecx
	jbe	.L621
	movl	%edx, %eax
	andl	$15, %edx
	subl	%ecx, %eax
	subl	$1, %edx
	jne	.L450
	salq	$32, %rax
	orq	$83887360, %rax
.L450:
	movslq	24(%r13), %rdi
	movq	32(%r13), %rcx
	movq	%rdi, %rdx
	movq	%rax, (%rcx,%rdi,8)
	leaq	0(,%rdi,8), %rsi
	leal	19(%r10), %eax
	movq	-64(%rbp), %rdi
	cltq
	movl	(%rdi,%rax,4), %edi
	movzbl	%dil, %r8d
	cmpb	$-65, %dil
	jbe	.L622
	movl	%edi, %eax
	andl	$15, %edi
	subl	%r8d, %eax
	subl	$1, %edi
	jne	.L452
	salq	$32, %rax
	orq	$83887360, %rax
.L452:
	movq	%rax, 8(%rcx,%rsi)
	leal	2(%rdx), %eax
	movl	%eax, 24(%r13)
	movl	%ebx, %eax
	testl	%ebx, %ebx
	je	.L391
	movq	-64(%rbp), %rdi
	addl	$39, %eax
	cltq
	movl	(%rdi,%rax,4), %edi
	movzbl	%dil, %r8d
	cmpb	$-65, %dil
	jbe	.L623
	movl	%edi, %eax
	andl	$15, %edi
	subl	%r8d, %eax
	subl	$1, %edi
	jne	.L454
	salq	$32, %rax
	orq	$83887360, %rax
.L454:
	addl	$3, %edx
	movl	%edx, 24(%r13)
	movq	%rax, 16(%rcx,%rsi)
	jmp	.L391
	.p2align 4,,10
	.p2align 3
.L612:
	movl	40(%r13), %r15d
	cmpl	%eax, %r15d
	jge	.L463
	movl	(%r14), %ecx
	testl	%ecx, %ecx
	jg	.L391
.L468:
	leal	0(,%r15,4), %edx
	leal	(%r15,%r15), %ecx
	cmpl	$999, %r15d
	cmovg	%ecx, %edx
	movl	%edx, %r15d
	cmpl	%edx, %eax
	jg	.L468
	movslq	%edx, %rdi
	salq	$3, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L445
	cmpl	%ebx, 40(%r13)
	cmovle	40(%r13), %ebx
	movq	%rax, %rdi
	movq	32(%r13), %r14
	cmpl	%r15d, %ebx
	cmovg	%r15d, %ebx
	movq	%r14, %rsi
	movslq	%ebx, %rdx
	salq	$3, %rdx
	call	memcpy@PLT
	cmpb	$0, 44(%r13)
	movq	%rax, %rcx
	jne	.L624
.L469:
	movslq	24(%r13), %rbx
	movq	%rcx, 32(%r13)
	movl	%r15d, 40(%r13)
	movb	$1, 44(%r13)
	leal	1(%rbx), %eax
	jmp	.L463
.L619:
	movq	%r14, %rdi
	call	uprv_free_67@PLT
	movl	-56(%rbp), %ecx
	jmp	.L476
.L621:
	movq	%rdx, %rax
	movl	$65535, %esi
	sall	$16, %edx
	salq	$32, %rax
	salq	$48, %rsi
	andl	$-16777216, %edx
	andq	%rsi, %rax
	sall	$8, %ecx
	orq	%rdx, %rax
	movl	%ecx, %edx
	orq	%rdx, %rax
	jmp	.L450
.L622:
	movabsq	$-281474976710656, %r9
	movq	%rdi, %rax
	sall	$16, %edi
	salq	$32, %rax
	andl	$-16777216, %edi
	andq	%r9, %rax
	orq	%rdi, %rax
	movl	%r8d, %edi
	sall	$8, %edi
	orq	%rdi, %rax
	jmp	.L452
.L620:
	movl	40(%r13), %r12d
	leal	1(%rbx), %eax
	cmpl	%eax, %r12d
	jge	.L441
	cmpl	$0, (%r14)
	jg	.L391
.L444:
	leal	0(,%r12,4), %edx
	leal	(%r12,%r12), %ecx
	cmpl	$999, %r12d
	cmovg	%ecx, %edx
	movl	%edx, %r12d
	cmpl	%eax, %edx
	jl	.L444
	movslq	%edx, %rdi
	salq	$3, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L445
	cmpl	%ebx, 40(%r13)
	cmovle	40(%r13), %ebx
	movq	%rax, %rdi
	movq	32(%r13), %r14
	cmpl	%r12d, %ebx
	cmovg	%r12d, %ebx
	movq	%r14, %rsi
	movslq	%ebx, %rdx
	salq	$3, %rdx
	call	memcpy@PLT
	cmpb	$0, 44(%r13)
	jne	.L625
.L446:
	movq	%r15, 32(%r13)
	movslq	24(%r13), %rbx
	movl	%r12d, 40(%r13)
	movb	$1, 44(%r13)
	jmp	.L441
.L623:
	movabsq	$-281474976710656, %r9
	movq	%rdi, %rax
	sall	$16, %edi
	salq	$32, %rax
	andl	$-16777216, %edi
	andq	%r9, %rax
	orq	%rdi, %rax
	movl	%r8d, %edi
	sall	$8, %edi
	orq	%rdi, %rax
	jmp	.L454
.L624:
	movq	%r14, %rdi
	movq	%rax, -56(%rbp)
	call	uprv_free_67@PLT
	movq	-56(%rbp), %rcx
	jmp	.L469
.L625:
	movq	%r14, %rdi
	call	uprv_free_67@PLT
	jmp	.L446
	.cfi_endproc
.LFE3360:
	.size	_ZN6icu_6717CollationIterator17appendCEsFromCE32EPKNS_13CollationDataEijaR10UErrorCode, .-_ZN6icu_6717CollationIterator17appendCEsFromCE32EPKNS_13CollationDataEijaR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717CollationIterator14nextCEFromCE32EPKNS_13CollationDataEijR10UErrorCode
	.type	_ZN6icu_6717CollationIterator14nextCEFromCE32EPKNS_13CollationDataEijR10UErrorCode, @function
_ZN6icu_6717CollationIterator14nextCEFromCE32EPKNS_13CollationDataEijR10UErrorCode:
.LFB3359:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%r8, %r9
	subl	$1, 24(%rdi)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%r8, %r12
	movl	$1, %r8d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZN6icu_6717CollationIterator17appendCEsFromCE32EPKNS_13CollationDataEijaR10UErrorCode
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L629
	movslq	368(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, 368(%rbx)
	movq	32(%rbx), %rdx
	popq	%rbx
	popq	%r12
	movq	(%rdx,%rax,8), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L629:
	.cfi_restore_state
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3359:
	.size	_ZN6icu_6717CollationIterator14nextCEFromCE32EPKNS_13CollationDataEijR10UErrorCode, .-_ZN6icu_6717CollationIterator14nextCEFromCE32EPKNS_13CollationDataEijR10UErrorCode
	.align 2
	.p2align 4
	.type	_ZN6icu_6717CollationIterator36nextCE32FromDiscontiguousContractionEPKNS_13CollationDataERNS_10UCharsTrieEjiiR10UErrorCode.part.0, @function
_ZN6icu_6717CollationIterator36nextCE32FromDiscontiguousContractionEPKNS_13CollationDataERNS_10UCharsTrieEjiiR10UErrorCode.part.0:
.LFB4480:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%r9d, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$40, %rsp
	movq	48(%rsi), %rdi
	movq	16(%rbp), %r13
	movl	%r8d, -64(%rbp)
	xorl	%r8d, %r8d
	movl	%ecx, -56(%rbp)
	movzwl	8(%rdi), %eax
	movw	%r8w, -74(%rbp)
	cmpl	%r9d, %eax
	jg	.L632
	cmpl	$65535, %r9d
	jg	.L633
	movl	%r9d, %eax
	movq	56(%rdi), %rdx
	sarl	$8, %eax
	cltq
	movzbl	(%rdx,%rax), %eax
	testb	%al, %al
	je	.L632
	movl	%r9d, %edx
	shrb	$5, %dl
	btl	%edx, %eax
	jnc	.L632
.L633:
	movl	%r14d, %esi
	call	_ZNK6icu_6715Normalizer2Impl20getFCD16FromNormDataEi@PLT
	movw	%ax, -74(%rbp)
.L632:
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6717CollationIterator20nextSkippedCodePointER10UErrorCode
	movl	%eax, %r8d
	testl	%eax, %eax
	js	.L732
	movq	48(%r12), %rdi
	movzwl	8(%rdi), %eax
	cmpl	%eax, %r8d
	jl	.L638
	cmpl	$65535, %r8d
	jg	.L637
	movl	%r8d, %eax
	movq	56(%rdi), %rdx
	sarl	$8, %eax
	cltq
	movzbl	(%rdx,%rax), %eax
	testb	%al, %al
	je	.L638
	movl	%r8d, %edx
	shrb	$5, %dl
	btl	%edx, %eax
	jnc	.L638
.L637:
	movl	%r8d, %esi
	movl	%r8d, -72(%rbp)
	call	_ZNK6icu_6715Normalizer2Impl20getFCD16FromNormDataEi@PLT
	movw	%ax, -52(%rbp)
	cmpw	$255, %ax
	jbe	.L638
	movq	376(%r15), %rax
	movl	-72(%rbp), %r8d
	testq	%rax, %rax
	je	.L639
	movswl	16(%rax), %ecx
	movq	8(%rbx), %rdx
	shrl	$5, %ecx
	je	.L640
	cmpq	%rdx, 144(%rax)
	jne	.L643
	testq	%rdx, %rdx
	je	.L643
	movq	152(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movl	160(%rax), %edx
	movl	%edx, 24(%rbx)
	jmp	.L643
	.p2align 4,,10
	.p2align 3
.L638:
	movq	%r13, %rdx
	movl	$2, %esi
	movq	%r15, %rdi
	call	_ZN6icu_6717CollationIterator18backwardNumSkippedEiR10UErrorCode
	movl	-56(%rbp), %r10d
.L631:
	addq	$40, %rsp
	movl	%r10d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L639:
	.cfi_restore_state
	movl	$168, %edi
	movl	%r8d, -72(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L644
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rdx
	movl	$2, %ecx
	movl	$2, %esi
	movq	$0, 136(%rax)
	movq	%rdx, 8(%rax)
	movl	-72(%rbp), %r8d
	movq	%rdx, 72(%rax)
	movq	8(%rbx), %rdx
	movw	%cx, 16(%rax)
	movw	%si, 80(%rax)
	movq	$0, 144(%rax)
	movq	%rax, 376(%r15)
.L640:
	movl	-64(%rbp), %ecx
	movq	%rdx, 16(%rbx)
	movl	$-1, 24(%rbx)
	addl	$1, %ecx
	cmpl	$2, %ecx
	jg	.L733
	movq	%rdx, -64(%rbp)
	movl	$-1, %ecx
.L645:
	movq	%rdx, %xmm0
	movl	%ecx, 160(%rax)
	movhps	-64(%rbp), %xmm0
	movups	%xmm0, 144(%rax)
.L643:
	movl	$0, 140(%rax)
	leaq	72(%rax), %rdi
	movl	%r8d, -80(%rbp)
	movq	%rax, -72(%rbp)
	movq	%rdi, -64(%rbp)
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	movq	-72(%rbp), %rax
	movq	-64(%rbp), %rdi
	movl	-80(%rbp), %r8d
	movswl	80(%rax), %edx
	testw	%dx, %dx
	js	.L649
	sarl	$5, %edx
.L650:
	movzbl	-74(%rbp), %eax
	movl	%r14d, %ecx
	xorl	%esi, %esi
	movl	%r8d, -72(%rbp)
	movl	$2, %r14d
	movb	%al, -64(%rbp)
	call	_ZN6icu_6713UnicodeString7replaceEiii@PLT
	movl	-56(%rbp), %r10d
	movl	-72(%rbp), %r8d
.L666:
	movzwl	-52(%rbp), %eax
	movzbl	%ah, %eax
	cmpb	-64(%rbp), %al
	jbe	.L655
	movl	%r8d, %esi
	movq	%rbx, %rdi
	movl	%r10d, -72(%rbp)
	movl	%r8d, -56(%rbp)
	call	_ZN6icu_6710UCharsTrie16nextForCodePointEi@PLT
	movl	-56(%rbp), %r8d
	movl	-72(%rbp), %r10d
	cmpl	$1, %eax
	jle	.L655
	movq	16(%rbx), %rsi
	movzwl	(%rsi), %edx
	testw	%dx, %dx
	js	.L734
	movzwl	%dx, %ecx
	cmpw	$16447, %dx
	ja	.L659
	sarl	$6, %ecx
	leal	-1(%rcx), %r11d
.L657:
	movq	376(%r15), %rdx
	movl	%r11d, %r10d
	movswl	80(%rdx), %ecx
	testw	%cx, %cx
	js	.L661
	sarl	$5, %ecx
.L662:
	andl	$1, %eax
	movl	%ecx, 140(%rdx)
	movl	%eax, %r14d
	je	.L663
	movq	8(%rbx), %rax
	movq	%rsi, 152(%rdx)
	xorl	%r14d, %r14d
	movq	%rax, 144(%rdx)
	movl	24(%rbx), %eax
	movl	%eax, 160(%rdx)
.L664:
	movq	%r13, %rsi
	movq	%r15, %rdi
	movl	%r10d, -52(%rbp)
	call	_ZN6icu_6717CollationIterator20nextSkippedCodePointER10UErrorCode
	movl	-52(%rbp), %r10d
	testl	%eax, %eax
	movl	%eax, %r8d
	js	.L663
	movq	48(%r12), %rdi
	addl	$1, %r14d
	movzwl	8(%rdi), %eax
	cmpl	%eax, %r8d
	jl	.L663
	cmpl	$65535, %r8d
	jg	.L665
	movl	%r8d, %eax
	movq	56(%rdi), %rdx
	sarl	$8, %eax
	cltq
	movzbl	(%rdx,%rax), %eax
	testb	%al, %al
	je	.L663
	movl	%r8d, %edx
	shrb	$5, %dl
	btl	%edx, %eax
	jnc	.L663
.L665:
	movl	%r8d, %esi
	movl	%r10d, -72(%rbp)
	movl	%r8d, -56(%rbp)
	call	_ZNK6icu_6715Normalizer2Impl20getFCD16FromNormDataEi@PLT
	movl	-56(%rbp), %r8d
	movl	-72(%rbp), %r10d
	cmpw	$255, %ax
	movw	%ax, -52(%rbp)
	ja	.L666
	.p2align 4,,10
	.p2align 3
.L663:
	movl	%r14d, %esi
	movq	%r13, %rdx
	movq	%r15, %rdi
	movl	%r10d, -52(%rbp)
	call	_ZN6icu_6717CollationIterator18backwardNumSkippedEiR10UErrorCode
	movq	376(%r15), %r14
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movswl	16(%r14), %ebx
	movl	136(%r14), %edx
	leaq	72(%r14), %rcx
	leaq	8(%r14), %rdi
	movl	140(%r14), %r9d
	sarl	$5, %ebx
	call	_ZN6icu_6713UnicodeString9doReplaceEiiRKS0_ii@PLT
	testl	%ebx, %ebx
	movl	-52(%rbp), %r10d
	movl	$0, 136(%r14)
	jne	.L631
	movq	376(%r15), %rax
	movswl	16(%rax), %eax
	shrl	$5, %eax
	je	.L631
	movl	$-1, %edx
	leaq	_ZNK6icu_6717CollationIterator11getDataCE32Ei(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L667:
	movq	%r12, %rsi
	movq	%r13, %r9
	movl	$1, %r8d
	movl	%r10d, %ecx
	movq	%r15, %rdi
	call	_ZN6icu_6717CollationIterator17appendCEsFromCE32EPKNS_13CollationDataEijaR10UErrorCode
	movq	376(%r15), %r12
	movzwl	16(%r12), %edx
	movl	136(%r12), %esi
	testw	%dx, %dx
	js	.L668
	movswl	%dx, %eax
	sarl	$5, %eax
.L669:
	cmpl	%eax, %esi
	jge	.L670
	leaq	8(%r12), %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movl	%eax, %edx
	movl	136(%r12), %eax
	cmpl	$65535, %edx
	ja	.L671
	addl	$1, %eax
	movl	%eax, 136(%r12)
	movq	(%r15), %rax
	movq	112(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L672
	movq	16(%r15), %r12
	movq	(%r12), %rax
	movq	16(%rax), %rcx
	cmpl	$55295, %edx
	ja	.L673
	movl	%edx, %esi
	movq	(%rax), %rax
	sarl	$5, %esi
	movslq	%esi, %rsi
	movzwl	(%rax,%rsi,2), %esi
.L728:
	movl	%edx, %eax
	andl	$31, %eax
	leal	(%rax,%rsi,4), %eax
	cltq
	salq	$2, %rax
.L674:
	movl	(%rcx,%rax), %r10d
.L677:
	cmpl	$192, %r10d
	jne	.L667
	movq	32(%r12), %r12
	movq	(%r12), %rsi
	movq	16(%rsi), %rcx
	cmpl	$55295, %edx
	ja	.L679
	movl	%edx, %eax
	movq	(%rsi), %rsi
	sarl	$5, %eax
	cltq
	movzwl	(%rsi,%rax,2), %esi
.L730:
	movl	%edx, %eax
	andl	$31, %eax
	leal	(%rax,%rsi,4), %eax
	cltq
	salq	$2, %rax
.L680:
	movl	(%rcx,%rax), %r10d
	jmp	.L667
	.p2align 4,,10
	.p2align 3
.L733:
	movq	(%r15), %rax
	movl	%r8d, -64(%rbp)
	movq	%r13, %rdx
	movl	%ecx, %esi
	movl	%ecx, -80(%rbp)
	movq	%r15, %rdi
	call	*104(%rax)
	movq	(%r15), %rax
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	*48(%rax)
	movq	%rbx, %rdi
	movl	%eax, %esi
	call	_ZN6icu_6710UCharsTrie17firstForCodePointEi@PLT
	cmpl	$3, -80(%rbp)
	movl	-64(%rbp), %r8d
	movl	$3, %edx
	je	.L647
	.p2align 4,,10
	.p2align 3
.L648:
	movq	(%r15), %rax
	movl	%r8d, -72(%rbp)
	movq	%r13, %rsi
	movq	%r15, %rdi
	movl	%edx, -64(%rbp)
	call	*48(%rax)
	movq	%rbx, %rdi
	movl	%eax, %esi
	call	_ZN6icu_6710UCharsTrie16nextForCodePointEi@PLT
	movl	-64(%rbp), %edx
	movl	-72(%rbp), %r8d
	addl	$1, %edx
	cmpl	-80(%rbp), %edx
	jne	.L648
.L647:
	movq	(%r15), %rax
	movl	%r8d, -72(%rbp)
	movq	%r13, %rdx
	movl	$2, %esi
	movq	%r15, %rdi
	call	*96(%rax)
	movq	16(%rbx), %rcx
	movq	376(%r15), %rax
	movq	8(%rbx), %rdx
	movl	-72(%rbp), %r8d
	movq	%rcx, -64(%rbp)
	movl	24(%rbx), %ecx
	jmp	.L645
	.p2align 4,,10
	.p2align 3
.L655:
	movq	376(%r15), %rax
	movl	%r8d, %esi
	movl	%r10d, -56(%rbp)
	leaq	72(%rax), %rdi
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	movq	376(%r15), %rdx
	movq	8(%rbx), %rax
	movl	-56(%rbp), %r10d
	cmpq	%rax, 144(%rdx)
	jne	.L654
	testq	%rax, %rax
	je	.L654
	movq	152(%rdx), %rax
	movq	%rax, 16(%rbx)
	movl	160(%rdx), %eax
	movl	%eax, 24(%rbx)
.L654:
	movzbl	-52(%rbp), %eax
	movb	%al, -64(%rbp)
	jmp	.L664
	.p2align 4,,10
	.p2align 3
.L671:
	addl	$2, %eax
	movl	%eax, 136(%r12)
	movq	(%r15), %rax
	movq	112(%rax), %rax
	cmpq	%rbx, %rax
	je	.L735
.L672:
	movl	%edx, -52(%rbp)
	movl	%edx, %esi
	movq	%r15, %rdi
	call	*%rax
	movq	16(%r15), %r12
	movl	-52(%rbp), %edx
	movl	%eax, %r10d
	jmp	.L677
	.p2align 4,,10
	.p2align 3
.L668:
	movl	20(%r12), %eax
	jmp	.L669
	.p2align 4,,10
	.p2align 3
.L649:
	movl	84(%rax), %edx
	jmp	.L650
	.p2align 4,,10
	.p2align 3
.L732:
	movq	%r13, %rdx
	movl	$1, %esi
	movq	%r15, %rdi
	call	_ZN6icu_6717CollationIterator18backwardNumSkippedEiR10UErrorCode
	movl	-56(%rbp), %r10d
	jmp	.L631
	.p2align 4,,10
	.p2align 3
.L673:
	cmpl	$56320, %edx
	movq	(%rax), %rdi
	movl	$0, %esi
	movl	$320, %eax
	cmovl	%eax, %esi
	movl	%edx, %eax
	sarl	$5, %eax
.L729:
	addl	%esi, %eax
	cltq
	movzwl	(%rdi,%rax,2), %esi
	jmp	.L728
	.p2align 4,,10
	.p2align 3
.L735:
	movq	16(%r15), %r12
	movq	(%r12), %rax
	movq	16(%rax), %rcx
	cmpl	$1114111, %edx
	jbe	.L685
	movl	$512, %eax
	jmp	.L674
	.p2align 4,,10
	.p2align 3
.L679:
	cmpl	$65535, %edx
	ja	.L681
	cmpl	$56320, %edx
	movl	$320, %eax
	movq	(%rsi), %rdi
	movl	$0, %esi
	cmovl	%eax, %esi
	movl	%edx, %eax
	sarl	$5, %eax
.L731:
	addl	%esi, %eax
	cltq
	movzwl	(%rdi,%rax,2), %esi
	jmp	.L730
	.p2align 4,,10
	.p2align 3
.L661:
	movl	84(%rdx), %ecx
	jmp	.L662
	.p2align 4,,10
	.p2align 3
.L734:
	movl	%edx, %ecx
	movl	%edx, %r11d
	andw	$32767, %cx
	andl	$32767, %r11d
	andb	$64, %dh
	je	.L657
	movzwl	2(%rsi), %edx
	cmpw	$32767, %cx
	je	.L658
	subl	$16384, %r11d
	sall	$16, %r11d
	orl	%edx, %r11d
	jmp	.L657
	.p2align 4,,10
	.p2align 3
.L670:
	movl	%edx, %eax
	movl	$1, %r10d
	movl	$0, 136(%r12)
	andl	$31, %eax
	andl	$1, %edx
	movl	$2, %edx
	cmovne	%edx, %eax
	movw	%ax, 16(%r12)
	jmp	.L631
	.p2align 4,,10
	.p2align 3
.L659:
	movzwl	2(%rsi), %r11d
	cmpl	$32703, %ecx
	jg	.L660
	andl	$32704, %edx
	subl	$16448, %edx
	sall	$10, %edx
	orl	%edx, %r11d
	jmp	.L657
	.p2align 4,,10
	.p2align 3
.L685:
	cmpl	44(%rax), %edx
	jl	.L676
	movslq	48(%rax), %rax
	salq	$2, %rax
	jmp	.L674
	.p2align 4,,10
	.p2align 3
.L681:
	movl	$512, %eax
	cmpl	$1114111, %edx
	ja	.L680
	cmpl	44(%rsi), %edx
	jl	.L683
	movslq	48(%rsi), %rax
	salq	$2, %rax
	jmp	.L680
	.p2align 4,,10
	.p2align 3
.L660:
	movzwl	4(%rsi), %edx
	sall	$16, %r11d
	orl	%edx, %r11d
	jmp	.L657
	.p2align 4,,10
	.p2align 3
.L676:
	movq	(%rax), %rdi
	movl	%edx, %eax
	sarl	$11, %eax
	addl	$2080, %eax
	cltq
	movzwl	(%rdi,%rax,2), %esi
	movl	%edx, %eax
	sarl	$5, %eax
	andl	$63, %eax
	jmp	.L729
	.p2align 4,,10
	.p2align 3
.L658:
	movzwl	4(%rsi), %r11d
	sall	$16, %edx
	orl	%edx, %r11d
	jmp	.L657
	.p2align 4,,10
	.p2align 3
.L683:
	movl	%edx, %eax
	movq	(%rsi), %rdi
	sarl	$11, %eax
	addl	$2080, %eax
	cltq
	movzwl	(%rdi,%rax,2), %esi
	movl	%edx, %eax
	sarl	$5, %eax
	andl	$63, %eax
	jmp	.L731
.L644:
	movq	$0, 376(%r15)
	xorl	%r10d, %r10d
	movl	$7, 0(%r13)
	jmp	.L631
	.cfi_endproc
.LFE4480:
	.size	_ZN6icu_6717CollationIterator36nextCE32FromDiscontiguousContractionEPKNS_13CollationDataERNS_10UCharsTrieEjiiR10UErrorCode.part.0, .-_ZN6icu_6717CollationIterator36nextCE32FromDiscontiguousContractionEPKNS_13CollationDataERNS_10UCharsTrieEjiiR10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717CollationIterator36nextCE32FromDiscontiguousContractionEPKNS_13CollationDataERNS_10UCharsTrieEjiiR10UErrorCode
	.type	_ZN6icu_6717CollationIterator36nextCE32FromDiscontiguousContractionEPKNS_13CollationDataERNS_10UCharsTrieEjiiR10UErrorCode, @function
_ZN6icu_6717CollationIterator36nextCE32FromDiscontiguousContractionEPKNS_13CollationDataERNS_10UCharsTrieEjiiR10UErrorCode:
.LFB3365:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	16(%rbp), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jg	.L737
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6717CollationIterator36nextCE32FromDiscontiguousContractionEPKNS_13CollationDataERNS_10UCharsTrieEjiiR10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L737:
	.cfi_restore_state
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3365:
	.size	_ZN6icu_6717CollationIterator36nextCE32FromDiscontiguousContractionEPKNS_13CollationDataERNS_10UCharsTrieEjiiR10UErrorCode, .-_ZN6icu_6717CollationIterator36nextCE32FromDiscontiguousContractionEPKNS_13CollationDataERNS_10UCharsTrieEjiiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717CollationIterator23nextCE32FromContractionEPKNS_13CollationDataEjPKDsjiR10UErrorCode
	.type	_ZN6icu_6717CollationIterator23nextCE32FromContractionEPKNS_13CollationDataEjPKDsjiR10UErrorCode, @function
_ZN6icu_6717CollationIterator23nextCE32FromContractionEPKNS_13CollationDataEjPKDsjiR10UErrorCode:
.LFB3364:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%r9d, %r12d
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -128(%rbp)
	movq	16(%rbp), %r15
	movl	%edx, -116(%rbp)
	movl	%r8d, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movq	%rcx, -88(%rbp)
	movq	%rcx, -80(%rbp)
	movl	$-1, -72(%rbp)
	movq	376(%rdi), %rax
	testq	%rax, %rax
	je	.L740
	movswl	16(%rax), %edx
	shrl	$5, %edx
	jne	.L818
.L740:
	leaq	-96(%rbp), %r14
	movl	%r12d, %esi
	movl	$1, %ebx
	movq	%r14, %rdi
	call	_ZN6icu_6710UCharsTrie17firstForCodePointEi@PLT
	movl	$1, -100(%rbp)
	jmp	.L767
	.p2align 4,,10
	.p2align 3
.L821:
	movq	-80(%rbp), %rcx
	movzwl	(%rcx), %edx
	testw	%dx, %dx
	js	.L819
	movzwl	%dx, %r8d
	cmpw	$16447, %dx
	ja	.L745
	sarl	$6, %r8d
	leal	-1(%r8), %r12d
.L743:
	andl	$1, %eax
	movl	%r12d, -104(%rbp)
	movl	%eax, -100(%rbp)
	je	.L747
	movq	376(%r13), %rdx
	testq	%rdx, %rdx
	je	.L748
	movswl	16(%rdx), %eax
	movl	136(%rdx), %esi
	testw	%ax, %ax
	js	.L749
	sarl	$5, %eax
.L750:
	cmpl	%eax, %esi
	jl	.L820
.L748:
	movl	384(%r13), %esi
	testl	%esi, %esi
	je	.L747
	movq	0(%r13), %rax
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	*48(%rax)
	movl	%eax, %r12d
	movq	376(%r13), %rax
	testq	%rax, %rax
	je	.L755
	movswl	16(%rax), %edx
	shrl	$5, %edx
	je	.L755
	testl	%r12d, %r12d
	js	.L747
	addl	$1, 136(%rax)
	movl	384(%r13), %edx
	testl	%edx, %edx
	jle	.L757
.L756:
	subl	$1, %edx
	movl	%edx, 384(%r13)
.L758:
	testq	%rax, %rax
	je	.L759
.L757:
	movswl	16(%rax), %edx
	shrl	$5, %edx
	je	.L759
	movl	-72(%rbp), %edx
	movdqu	-88(%rbp), %xmm0
	movl	%edx, 160(%rax)
	movups	%xmm0, 144(%rax)
.L759:
	movl	%r12d, %esi
	movq	%r14, %rdi
	addl	$1, %ebx
	call	_ZN6icu_6710UCharsTrie16nextForCodePointEi@PLT
.L767:
	cmpl	$1, %eax
	jg	.L821
	testl	%eax, %eax
	je	.L760
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6717CollationIterator20nextSkippedCodePointER10UErrorCode
	testl	%eax, %eax
	js	.L760
	addl	$1, -100(%rbp)
	movl	%eax, %r12d
	jmp	.L759
	.p2align 4,,10
	.p2align 3
.L755:
	movl	384(%r13), %edx
	testl	%edx, %edx
	jle	.L754
	testl	%r12d, %r12d
	jns	.L756
.L747:
	movq	%r14, %rdi
	call	_ZN6icu_6710UCharsTrieD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L822
	movl	-104(%rbp), %eax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L820:
	.cfi_restore_state
	leaq	8(%rdx), %rdi
	movq	%rdx, -112(%rbp)
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movq	-112(%rbp), %rdx
	cmpl	$65536, %eax
	movl	%eax, %r12d
	movl	$2, %eax
	cmovb	-100(%rbp), %eax
	addl	%eax, 136(%rdx)
.L754:
	testl	%r12d, %r12d
	js	.L747
	movq	376(%r13), %rax
	jmp	.L758
	.p2align 4,,10
	.p2align 3
.L819:
	movl	%edx, %esi
	movl	%edx, %r12d
	andw	$32767, %si
	andl	$32767, %r12d
	andb	$64, %dh
	je	.L743
	movzwl	2(%rcx), %edx
	cmpw	$32767, %si
	je	.L744
	subl	$16384, %r12d
	sall	$16, %r12d
	orl	%edx, %r12d
	jmp	.L743
	.p2align 4,,10
	.p2align 3
.L745:
	movzwl	2(%rcx), %r12d
	cmpl	$32703, %r8d
	jg	.L746
	andl	$32704, %edx
	subl	$16448, %edx
	sall	$10, %edx
	orl	%edx, %r12d
	jmp	.L743
	.p2align 4,,10
	.p2align 3
.L749:
	movl	20(%rdx), %eax
	jmp	.L750
	.p2align 4,,10
	.p2align 3
.L746:
	movzwl	4(%rcx), %edx
	sall	$16, %r12d
	orl	%edx, %r12d
	jmp	.L743
	.p2align 4,,10
	.p2align 3
.L744:
	movzwl	4(%rcx), %r12d
	sall	$16, %edx
	orl	%edx, %r12d
	jmp	.L743
	.p2align 4,,10
	.p2align 3
.L818:
	movdqu	-88(%rbp), %xmm1
	movl	-72(%rbp), %edx
	movups	%xmm1, 144(%rax)
	movl	%edx, 160(%rax)
	jmp	.L740
	.p2align 4,,10
	.p2align 3
.L760:
	movl	-116(%rbp), %eax
	testb	$4, %ah
	je	.L761
	testb	$1, %ah
	je	.L771
	cmpl	-100(%rbp), %ebx
	jle	.L761
.L771:
	movl	-100(%rbp), %eax
	cmpl	$1, %eax
	jne	.L823
.L763:
	movq	-128(%rbp), %rax
	movq	48(%rax), %rdi
	movzwl	8(%rdi), %eax
	cmpl	%eax, %r12d
	jl	.L766
	cmpl	$65535, %r12d
	jg	.L765
	movl	%r12d, %eax
	movq	56(%rdi), %rdx
	sarl	$8, %eax
	cltq
	movzbl	(%rdx,%rax), %eax
	testb	%al, %al
	je	.L766
	movl	%r12d, %edx
	shrb	$5, %dl
	btl	%edx, %eax
	jnc	.L766
.L765:
	movl	%r12d, %esi
	call	_ZNK6icu_6715Normalizer2Impl20getFCD16FromNormDataEi@PLT
	cmpw	$255, %ax
	jbe	.L766
	movl	(%r15), %ecx
	testl	%ecx, %ecx
	jg	.L770
	subq	$8, %rsp
	movl	-104(%rbp), %ecx
	movq	-128(%rbp), %rsi
	movq	%r14, %rdx
	pushq	%r15
	movl	%r12d, %r9d
	movl	%ebx, %r8d
	movq	%r13, %rdi
	call	_ZN6icu_6717CollationIterator36nextCE32FromDiscontiguousContractionEPKNS_13CollationDataERNS_10UCharsTrieEjiiR10UErrorCode.part.0
	movl	%eax, -104(%rbp)
	popq	%rax
	popq	%rdx
	jmp	.L747
.L766:
	movl	$1, -100(%rbp)
.L761:
	movl	-100(%rbp), %esi
	movq	%r15, %rdx
	movq	%r13, %rdi
	call	_ZN6icu_6717CollationIterator18backwardNumSkippedEiR10UErrorCode
	jmp	.L747
.L823:
	movl	%eax, %esi
	movq	%r15, %rdx
	movq	%r13, %rdi
	call	_ZN6icu_6717CollationIterator18backwardNumSkippedEiR10UErrorCode
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6717CollationIterator20nextSkippedCodePointER10UErrorCode
	movl	%eax, %r12d
	movl	-100(%rbp), %eax
	subl	$1, %eax
	subl	%eax, %ebx
	jmp	.L763
.L770:
	movl	$0, -104(%rbp)
	jmp	.L747
.L822:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3364:
	.size	_ZN6icu_6717CollationIterator23nextCE32FromContractionEPKNS_13CollationDataEjPKDsjiR10UErrorCode, .-_ZN6icu_6717CollationIterator23nextCE32FromContractionEPKNS_13CollationDataEjPKDsjiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717CollationIterator8fetchCEsER10UErrorCode
	.type	_ZN6icu_6717CollationIterator8fetchCEsER10UErrorCode, @function
_ZN6icu_6717CollationIterator8fetchCEsER10UErrorCode:
.LFB3352:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rsi), %ecx
	movl	24(%rdi), %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L854
	movslq	368(%rdi), %rax
	movq	%rdi, %rbx
	movq	%rsi, %r15
	movabsq	$4311744768, %r12
	leaq	_ZN6icu_6717CollationIterator14handleNextCE32ERiR10UErrorCode(%rip), %r13
	movabsq	$-281474976710656, %r14
	jmp	.L851
	.p2align 4,,10
	.p2align 3
.L864:
	leal	1(%rax), %ecx
	movl	%ecx, 368(%rbx)
	movq	32(%rbx), %rcx
	movq	(%rcx,%rax,8), %rcx
	movslq	%edx, %rax
.L827:
	cmpq	%r12, %rcx
	je	.L824
	movl	(%r15), %edx
.L852:
	movl	%eax, 368(%rbx)
	testl	%edx, %edx
	jg	.L824
	movl	%eax, %edx
.L851:
	cmpl	%edx, %eax
	jl	.L864
	cmpl	$39, %edx
	jle	.L828
	leaq	24(%rbx), %rdi
	movq	%r15, %rdx
	movl	$1, %esi
	call	_ZN6icu_6717CollationIterator8CEBuffer20ensureAppendCapacityEiR10UErrorCode
	testb	%al, %al
	je	.L863
	movl	24(%rbx), %edx
.L828:
	movq	(%rbx), %rax
	addl	$1, %edx
	movl	%edx, 24(%rbx)
	movq	64(%rax), %rcx
	cmpq	%r13, %rcx
	jne	.L830
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	*48(%rax)
	movl	%eax, -60(%rbp)
	testl	%eax, %eax
	js	.L841
	movq	16(%rbx), %rdx
	movq	(%rdx), %rsi
	movq	16(%rsi), %rcx
	cmpl	$55295, %eax
	jg	.L832
	movl	%eax, %edx
	movq	(%rsi), %rsi
	andl	$31, %eax
	sarl	$5, %edx
	movslq	%edx, %rdx
	movzwl	(%rsi,%rdx,2), %edx
	leal	(%rax,%rdx,4), %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
.L833:
	movl	(%rcx,%rdx), %ecx
.L837:
	movzbl	%cl, %r8d
	cmpb	$-65, %cl
	ja	.L838
	movq	%rcx, %rdx
	sall	$16, %ecx
	movslq	368(%rbx), %rdi
	movq	32(%rbx), %rsi
	movl	%ecx, %eax
	salq	$32, %rdx
	movl	%r8d, %ecx
	andq	%r14, %rdx
	andl	$-16777216, %eax
	sall	$8, %ecx
	orq	%rax, %rdx
	orq	%rcx, %rdx
	movq	%rdx, (%rsi,%rdi,8)
.L839:
	movl	(%r15), %edx
	movslq	24(%rbx), %rax
	jmp	.L852
	.p2align 4,,10
	.p2align 3
.L841:
	movslq	368(%rbx), %rax
	movabsq	$4311744768, %rdi
	leal	1(%rax), %edx
	movl	%edx, 368(%rbx)
	movq	32(%rbx), %rdx
	movq	%rdi, (%rdx,%rax,8)
.L863:
	movl	24(%rbx), %eax
.L824:
	movq	-56(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L865
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L832:
	.cfi_restore_state
	cmpl	$65535, %eax
	jg	.L834
	cmpl	$56320, %eax
	movl	$320, %edx
	movq	(%rsi), %rdi
	movl	$0, %esi
	cmovl	%edx, %esi
	movl	%eax, %edx
	sarl	$5, %edx
.L861:
	addl	%esi, %edx
	andl	$31, %eax
	movslq	%edx, %rdx
	movzwl	(%rdi,%rdx,2), %edx
	leal	(%rax,%rdx,4), %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
	jmp	.L833
	.p2align 4,,10
	.p2align 3
.L838:
	cmpl	$192, %r8d
	je	.L866
	movq	16(%rbx), %rsi
.L847:
	movslq	24(%rbx), %rax
	cmpl	$193, %r8d
	jne	.L848
	movslq	368(%rbx), %rdx
	subl	$193, %ecx
	salq	$32, %rcx
	leal	1(%rdx), %esi
	orq	$83887360, %rcx
	movl	%esi, 368(%rbx)
	movq	32(%rbx), %rsi
	movq	%rcx, (%rsi,%rdx,8)
	jmp	.L827
	.p2align 4,,10
	.p2align 3
.L848:
	subl	$1, %eax
	movl	-60(%rbp), %edx
	movq	%r15, %r9
	movl	$1, %r8d
	movl	%eax, 24(%rbx)
	movq	%rbx, %rdi
	call	_ZN6icu_6717CollationIterator17appendCEsFromCE32EPKNS_13CollationDataEijaR10UErrorCode
	movl	(%r15), %eax
	testl	%eax, %eax
	jg	.L867
	movslq	368(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, 368(%rbx)
	movq	32(%rbx), %rdx
	movq	(%rdx,%rax,8), %rcx
	movslq	24(%rbx), %rax
	jmp	.L827
	.p2align 4,,10
	.p2align 3
.L834:
	movl	$512, %edx
	cmpl	$1114111, %eax
	ja	.L833
	cmpl	44(%rsi), %eax
	jl	.L836
	movslq	48(%rsi), %rdx
	salq	$2, %rdx
	jmp	.L833
	.p2align 4,,10
	.p2align 3
.L830:
	leaq	-60(%rbp), %rsi
	movq	%r15, %rdx
	movq	%rbx, %rdi
	call	*%rcx
	movl	%eax, %ecx
	jmp	.L837
.L866:
	movl	-60(%rbp), %eax
	testl	%eax, %eax
	js	.L841
	movq	16(%rbx), %rdx
	movq	32(%rdx), %rsi
	movq	(%rsi), %rdi
	movq	16(%rdi), %rcx
	cmpl	$55295, %eax
	jle	.L868
	cmpl	$65535, %eax
	jg	.L844
	cmpl	$56320, %eax
	movq	(%rdi), %r8
	movl	$320, %edx
	movl	$0, %edi
	cmovl	%edx, %edi
	movl	%eax, %edx
	andl	$31, %eax
	sarl	$5, %edx
	addl	%edi, %edx
	movslq	%edx, %rdx
	movzwl	(%r8,%rdx,2), %edx
	leal	(%rax,%rdx,4), %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
.L843:
	movl	(%rcx,%rdx), %ecx
	movzbl	%cl, %r8d
	cmpb	$-65, %cl
	ja	.L847
	movq	%rcx, %rdx
	sall	$16, %ecx
	movslq	368(%rbx), %rdi
	movq	32(%rbx), %rsi
	salq	$32, %rdx
	andl	$-16777216, %ecx
	andq	%r14, %rdx
	orq	%rcx, %rdx
	movl	%r8d, %ecx
	sall	$8, %ecx
	movl	%ecx, %eax
	orq	%rax, %rdx
	movq	%rdx, (%rsi,%rdi,8)
	jmp	.L839
	.p2align 4,,10
	.p2align 3
.L868:
	movl	%eax, %edx
	movq	(%rdi), %rdi
	sarl	$5, %edx
	movslq	%edx, %rdx
.L862:
	movzwl	(%rdi,%rdx,2), %edx
	andl	$31, %eax
	leal	(%rax,%rdx,4), %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
	jmp	.L843
	.p2align 4,,10
	.p2align 3
.L867:
	movl	24(%rbx), %eax
	movl	%eax, 368(%rbx)
	jmp	.L824
.L836:
	movl	%eax, %edx
	movq	(%rsi), %rdi
	sarl	$11, %edx
	addl	$2080, %edx
	movslq	%edx, %rdx
	movzwl	(%rdi,%rdx,2), %esi
	movl	%eax, %edx
	sarl	$5, %edx
	andl	$63, %edx
	jmp	.L861
.L844:
	movl	$512, %edx
	cmpl	$1114111, %eax
	ja	.L843
	cmpl	44(%rdi), %eax
	jl	.L846
	movslq	48(%rdi), %rdx
	salq	$2, %rdx
	jmp	.L843
.L854:
	movl	%edx, %eax
	jmp	.L824
.L846:
	movl	%eax, %edx
	movq	(%rdi), %rdi
	movl	%eax, %r8d
	sarl	$11, %edx
	sarl	$5, %r8d
	addl	$2080, %edx
	andl	$63, %r8d
	movslq	%edx, %rdx
	movzwl	(%rdi,%rdx,2), %edx
	addl	%r8d, %edx
	movslq	%edx, %rdx
	jmp	.L862
.L865:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3352:
	.size	_ZN6icu_6717CollationIterator8fetchCEsER10UErrorCode, .-_ZN6icu_6717CollationIterator8fetchCEsER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717CollationIterator16previousCEUnsafeEiRNS_9UVector32ER10UErrorCode
	.type	_ZN6icu_6717CollationIterator16previousCEUnsafeEiRNS_9UVector32ER10UErrorCode, @function
_ZN6icu_6717CollationIterator16previousCEUnsafeEiRNS_9UVector32ER10UErrorCode:
.LFB3369:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$1, -68(%rbp)
	movq	%rdx, -80(%rbp)
	.p2align 4,,10
	.p2align 3
.L870:
	movq	(%r15), %rax
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	*56(%rax)
	movl	%eax, %r12d
	testl	%eax, %eax
	js	.L871
	movq	16(%r15), %rbx
	movl	%eax, %esi
	addl	$1, -68(%rbp)
	movzbl	388(%r15), %r14d
	movq	80(%rbx), %rdi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	jne	.L870
	testb	%r14b, %r14b
	jne	.L942
.L871:
	movl	-68(%rbp), %eax
	movq	-80(%rbp), %rbx
	movq	%r15, %rdi
	movq	%r15, %r14
	movl	$0, 368(%r15)
	movl	%eax, 384(%r15)
	movq	(%r15), %rax
	call	*40(%rax)
	movl	384(%r15), %ecx
	movl	%eax, %r12d
	testl	%ecx, %ecx
	jle	.L880
	movl	24(%r15), %eax
	leaq	_ZN6icu_6717CollationIterator14handleNextCE32ERiR10UErrorCode(%rip), %r15
	.p2align 4,,10
	.p2align 3
.L908:
	subl	$1, %ecx
	movl	%ecx, 384(%r14)
	cmpl	%eax, 368(%r14)
	jge	.L943
.L881:
	movl	%eax, 368(%r14)
	movslq	8(%rbx), %rax
	movl	%eax, %esi
	addl	$1, %esi
	js	.L902
	cmpl	12(%rbx), %esi
	jle	.L903
.L902:
	movq	%r13, %rdx
	movq	%rbx, %rdi
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	je	.L904
	movslq	8(%rbx), %rax
.L903:
	movq	24(%rbx), %rdx
	movl	%r12d, (%rdx,%rax,4)
	addl	$1, 8(%rbx)
.L904:
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*40(%rax)
	movl	8(%rbx), %edx
	movl	%eax, %r12d
	movl	24(%r14), %eax
	cmpl	%eax, %edx
	jge	.L906
	.p2align 4,,10
	.p2align 3
.L905:
	movl	%edx, %esi
	addl	$1, %esi
	js	.L909
	cmpl	12(%rbx), %esi
	jle	.L910
.L909:
	movq	%r13, %rdx
	movq	%rbx, %rdi
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	movl	8(%rbx), %edx
	testb	%al, %al
	je	.L912
.L910:
	movq	24(%rbx), %rax
	movslq	%edx, %rdx
	movl	%r12d, (%rax,%rdx,4)
	movl	8(%rbx), %eax
	leal	1(%rax), %edx
	movl	%edx, 8(%rbx)
.L912:
	movl	24(%r14), %eax
	cmpl	%edx, %eax
	jg	.L905
.L906:
	movl	384(%r14), %ecx
	testl	%ecx, %ecx
	jg	.L908
.L907:
	movl	%edx, %esi
	addl	$1, %esi
	js	.L913
	cmpl	12(%rbx), %esi
	jle	.L914
.L913:
	movq	%r13, %rdx
	movq	%rbx, %rdi
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	jne	.L944
.L915:
	movq	(%r14), %rax
	movq	%r13, %rdx
	movl	-68(%rbp), %esi
	movq	%r14, %rdi
	movl	$-1, 384(%r14)
	call	*104(%rax)
	movl	0(%r13), %edx
	movl	$1, %eax
	movl	$0, 368(%r14)
	testl	%edx, %edx
	jg	.L869
	movl	24(%r14), %eax
	movq	32(%r14), %rdx
	subl	$1, %eax
	movl	%eax, 24(%r14)
	cltq
	movq	(%rdx,%rax,8), %rax
.L869:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L945
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L944:
	.cfi_restore_state
	movl	8(%rbx), %edx
.L914:
	movq	24(%rbx), %rax
	movslq	%edx, %rdx
	movl	%r12d, (%rax,%rdx,4)
	addl	$1, 8(%rbx)
	jmp	.L915
	.p2align 4,,10
	.p2align 3
.L943:
	cmpl	$39, %eax
	jle	.L882
	leaq	24(%r14), %rdi
	movq	%r13, %rdx
	movl	$1, %esi
	call	_ZN6icu_6717CollationIterator8CEBuffer20ensureAppendCapacityEiR10UErrorCode
	testb	%al, %al
	movl	24(%r14), %eax
	je	.L881
.L882:
	addl	$1, %eax
	movl	%eax, 24(%r14)
	movq	(%r14), %rax
	movq	64(%rax), %rcx
	cmpq	%r15, %rcx
	jne	.L884
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	*48(%rax)
	movl	%eax, -60(%rbp)
	testl	%eax, %eax
	js	.L946
	movq	16(%r14), %rdx
	movq	(%rdx), %rsi
	movq	16(%rsi), %rcx
	cmpl	$55295, %eax
	jg	.L886
	movl	%eax, %edx
	movq	(%rsi), %rsi
	sarl	$5, %edx
	movslq	%edx, %rdx
.L939:
	movzwl	(%rsi,%rdx,2), %edx
	andl	$31, %eax
	leal	(%rax,%rdx,4), %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
.L887:
	movl	(%rcx,%rdx), %ecx
.L891:
	movl	24(%r14), %eax
	movzbl	%cl, %r9d
	movl	%eax, %edx
	cmpb	$-65, %cl
	ja	.L892
	movq	%rcx, %rdx
	sall	$16, %ecx
	movslq	368(%r14), %rdi
	movq	32(%r14), %rsi
	movl	%ecx, %r8d
	salq	$32, %rdx
	movl	%r9d, %ecx
	movabsq	$-281474976710656, %r11
	andq	%r11, %rdx
	andl	$-16777216, %r8d
	sall	$8, %ecx
	orq	%r8, %rdx
	orq	%rcx, %rdx
	movq	%rdx, (%rsi,%rdi,8)
	jmp	.L881
	.p2align 4,,10
	.p2align 3
.L942:
	cmpl	$1631, %r12d
	jg	.L873
	subl	$48, %r12d
	cmpl	$9, %r12d
	setbe	%al
.L874:
	testb	%al, %al
	jne	.L870
	jmp	.L871
	.p2align 4,,10
	.p2align 3
.L873:
	movq	(%rbx), %rsi
	movq	16(%rsi), %rdx
	cmpl	$55295, %r12d
	jg	.L875
	movl	%r12d, %eax
	movq	(%rsi), %rsi
	andl	$31, %r12d
	sarl	$5, %eax
	cltq
	movzwl	(%rsi,%rax,2), %eax
	leal	(%r12,%rax,4), %eax
	cltq
	salq	$2, %rax
.L876:
	movl	(%rdx,%rax), %eax
	cmpb	$-65, %al
	jbe	.L871
	andl	$15, %eax
	cmpl	$10, %eax
	sete	%al
	jmp	.L874
.L886:
	cmpl	$65535, %eax
	jg	.L888
	cmpl	$56320, %eax
	movq	(%rsi), %rdi
	movl	$320, %edx
	movl	$0, %esi
	cmovl	%edx, %esi
	movl	%eax, %edx
	andl	$31, %eax
	sarl	$5, %edx
	addl	%esi, %edx
	movslq	%edx, %rdx
	movzwl	(%rdi,%rdx,2), %edx
	leal	(%rax,%rdx,4), %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
	jmp	.L887
.L892:
	cmpl	$192, %r9d
	je	.L947
	movq	16(%r14), %rsi
.L900:
	cmpl	$193, %r9d
	jne	.L901
	leal	-193(%rcx), %edx
	movslq	368(%r14), %rdi
	movq	32(%r14), %rsi
	salq	$32, %rdx
	orq	$83887360, %rdx
	movq	%rdx, (%rsi,%rdi,8)
	jmp	.L881
.L875:
	cmpl	$65535, %r12d
	jg	.L877
	cmpl	$56320, %r12d
	movl	$320, %eax
	movq	(%rsi), %rdi
	movl	$0, %esi
	cmovl	%eax, %esi
	movl	%r12d, %eax
	sarl	$5, %eax
.L938:
	addl	%esi, %eax
	andl	$31, %r12d
	cltq
	movzwl	(%rdi,%rax,2), %eax
	leal	(%r12,%rax,4), %eax
	cltq
	salq	$2, %rax
	jmp	.L876
.L901:
	subl	$1, %edx
	movq	%r13, %r9
	movl	$1, %r8d
	movq	%r14, %rdi
	movl	%edx, 24(%r14)
	movl	-60(%rbp), %edx
	call	_ZN6icu_6717CollationIterator17appendCEsFromCE32EPKNS_13CollationDataEijaR10UErrorCode
	movl	24(%r14), %eax
	jmp	.L881
.L888:
	movl	$512, %edx
	cmpl	$1114111, %eax
	ja	.L887
	cmpl	44(%rsi), %eax
	jl	.L890
	movslq	48(%rsi), %rdx
	salq	$2, %rdx
	jmp	.L887
.L877:
	movl	$512, %eax
	cmpl	$1114111, %r12d
	ja	.L876
	cmpl	44(%rsi), %r12d
	jl	.L879
	movslq	48(%rsi), %rax
	salq	$2, %rax
	jmp	.L876
.L946:
	movl	24(%r14), %edx
.L894:
	movq	32(%r14), %rax
	movslq	368(%r14), %rcx
	movabsq	$4311744768, %rdi
	movq	%rdi, (%rax,%rcx,8)
	movl	%edx, %eax
	jmp	.L881
.L884:
	leaq	-60(%rbp), %rsi
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	*%rcx
	movl	%eax, %ecx
	jmp	.L891
.L947:
	movl	-60(%rbp), %ecx
	testl	%ecx, %ecx
	js	.L894
	movq	16(%r14), %rsi
	movq	32(%rsi), %rsi
	movq	(%rsi), %r9
	movq	16(%r9), %r8
	cmpl	$55295, %ecx
	jle	.L948
	cmpl	$65535, %ecx
	jg	.L897
	cmpl	$56320, %ecx
	movl	$320, %edi
	movl	$0, %r10d
	movq	(%r9), %r9
	cmovl	%edi, %r10d
	movl	%ecx, %edi
	sarl	$5, %edi
.L941:
	addl	%r10d, %edi
	movslq	%edi, %rdi
.L940:
	movzwl	(%r9,%rdi,2), %edi
	andl	$31, %ecx
	leal	(%rcx,%rdi,4), %edi
	movslq	%edi, %rdi
	salq	$2, %rdi
.L896:
	movl	(%r8,%rdi), %ecx
	movzbl	%cl, %r9d
	cmpb	$-65, %cl
	ja	.L900
	movq	%rcx, %rdx
	sall	$16, %ecx
	movslq	368(%r14), %rdi
	movq	32(%r14), %rsi
	salq	$32, %rdx
	andl	$-16777216, %ecx
	movabsq	$-281474976710656, %r10
	andq	%r10, %rdx
	orq	%rcx, %rdx
	movl	%r9d, %ecx
	sall	$8, %ecx
	orq	%rcx, %rdx
	movq	%rdx, (%rsi,%rdi,8)
	jmp	.L881
.L948:
	movl	%ecx, %edi
	movq	(%r9), %r9
	sarl	$5, %edi
	movslq	%edi, %rdi
	jmp	.L940
.L880:
	movl	8(%rbx), %edx
	jmp	.L907
.L890:
	movl	%eax, %edx
	movq	(%rsi), %rsi
	movl	%eax, %edi
	sarl	$11, %edx
	sarl	$5, %edi
	addl	$2080, %edx
	andl	$63, %edi
	movslq	%edx, %rdx
	movzwl	(%rsi,%rdx,2), %edx
	addl	%edi, %edx
	movslq	%edx, %rdx
	jmp	.L939
.L897:
	movl	$512, %edi
	cmpl	$1114111, %ecx
	ja	.L896
	cmpl	44(%r9), %ecx
	jl	.L899
	movslq	48(%r9), %rdi
	salq	$2, %rdi
	jmp	.L896
.L879:
	movl	%r12d, %eax
	movq	(%rsi), %rdi
	movl	%r12d, %esi
	sarl	$11, %eax
	sarl	$5, %esi
	addl	$2080, %eax
	andl	$63, %esi
	cltq
	movzwl	(%rdi,%rax,2), %eax
	jmp	.L938
.L899:
	movl	%ecx, %edi
	movq	(%r9), %r9
	movl	%ecx, %r10d
	sarl	$11, %edi
	sarl	$5, %r10d
	addl	$2080, %edi
	andl	$63, %r10d
	movslq	%edi, %rdi
	movzwl	(%r9,%rdi,2), %edi
	jmp	.L941
.L945:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3369:
	.size	_ZN6icu_6717CollationIterator16previousCEUnsafeEiRNS_9UVector32ER10UErrorCode, .-_ZN6icu_6717CollationIterator16previousCEUnsafeEiRNS_9UVector32ER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717CollationIterator10previousCEERNS_9UVector32ER10UErrorCode
	.type	_ZN6icu_6717CollationIterator10previousCEERNS_9UVector32ER10UErrorCode, @function
_ZN6icu_6717CollationIterator10previousCEERNS_9UVector32ER10UErrorCode:
.LFB3368:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	24(%rdi), %eax
	testl	%eax, %eax
	jle	.L950
	subl	$1, %eax
	movq	32(%rdi), %rdx
	movl	%eax, 24(%rdi)
	cltq
	movq	(%rdx,%rax,8), %rax
.L949:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L950:
	.cfi_restore_state
	movq	%rsi, %rdi
	movq	%rdx, %r13
	movq	%rsi, %r14
	call	_ZN6icu_679UVector3217removeAllElementsEv@PLT
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*40(%rax)
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, -52(%rbp)
	movq	(%r12), %rax
	call	*56(%rax)
	movl	%eax, %r15d
	testl	%eax, %eax
	js	.L989
	movq	16(%r12), %rbx
	movl	%r15d, %esi
	movzbl	388(%r12), %eax
	movq	80(%rbx), %rdi
	movb	%al, -53(%rbp)
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	je	.L1002
.L952:
	addq	$24, %rsp
	movq	%r13, %rcx
	movq	%r14, %rdx
	movl	%r15d, %esi
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6717CollationIterator16previousCEUnsafeEiRNS_9UVector32ER10UErrorCode
	.p2align 4,,10
	.p2align 3
.L989:
	.cfi_restore_state
	movabsq	$4311744768, %rax
	jmp	.L949
	.p2align 4,,10
	.p2align 3
.L1002:
	cmpb	$0, -53(%rbp)
	je	.L953
	cmpl	$1631, %r15d
	jg	.L954
	leal	-48(%r15), %eax
	cmpl	$9, %eax
	jbe	.L952
	movq	16(%r12), %r10
	movq	(%r10), %rax
	movq	16(%rax), %rcx
.L988:
	movl	%r15d, %edx
	movq	(%rax), %rax
	movl	%r15d, %edi
	sarl	$5, %edx
	andl	$31, %edi
	movslq	%edx, %rdx
	movzwl	(%rax,%rdx,2), %eax
	leal	(%rdi,%rax,4), %eax
	cltq
	movl	(%rcx,%rax,4), %ecx
	cmpl	$192, %ecx
	je	.L1003
.L963:
	movzbl	%cl, %edx
	cmpb	$-65, %cl
	ja	.L1004
	movabsq	$-281474976710656, %rax
	movq	%rcx, %rsi
	sall	$8, %edx
	salq	$32, %rsi
	andq	%rax, %rsi
	movl	%ecx, %eax
	sall	$16, %eax
	andl	$-16777216, %eax
	orq	%rax, %rsi
	movl	%edx, %eax
	orq	%rsi, %rax
	jmp	.L949
	.p2align 4,,10
	.p2align 3
.L954:
	movq	(%rbx), %rcx
	movq	16(%rcx), %rdx
	cmpl	$55295, %r15d
	jg	.L956
	movl	%r15d, %eax
	movq	(%rcx), %rcx
	sarl	$5, %eax
	cltq
	movzwl	(%rcx,%rax,2), %ecx
.L1000:
	movl	%r15d, %eax
	andl	$31, %eax
	leal	(%rax,%rcx,4), %eax
	cltq
	salq	$2, %rax
.L957:
	movl	(%rdx,%rax), %eax
	cmpb	$-65, %al
	jbe	.L953
	andl	$15, %eax
	cmpl	$10, %eax
	je	.L952
	.p2align 4,,10
	.p2align 3
.L953:
	movq	16(%r12), %r10
	movq	(%r10), %rax
	movq	16(%rax), %rcx
	cmpl	$55295, %r15d
	jle	.L988
	cmpl	$65535, %r15d
	jle	.L1005
	cmpl	$1114111, %r15d
	ja	.L967
	cmpl	44(%rax), %r15d
	jl	.L968
	movslq	48(%rax), %rax
	salq	$2, %rax
.L969:
	movl	(%rcx,%rax), %ecx
	cmpl	$192, %ecx
	jne	.L963
	movq	32(%r10), %r10
	movq	(%r10), %rax
	movq	16(%rax), %rcx
	cmpl	44(%rax), %r15d
	jl	.L1006
	movslq	48(%rax), %rax
	salq	$2, %rax
	.p2align 4,,10
	.p2align 3
.L970:
	movl	(%rcx,%rax), %ecx
	jmp	.L963
	.p2align 4,,10
	.p2align 3
.L1004:
	movl	%ecx, %edi
	andl	$15, %edi
	leal	-1(%rdi), %eax
	cmpl	$1, %eax
	ja	.L1007
	movl	%ecx, %eax
	subl	%edx, %eax
	movq	%rax, %rdx
	salq	$32, %rdx
	orq	$83887360, %rdx
	cmpl	$1, %edi
	cmove	%rdx, %rax
	jmp	.L949
	.p2align 4,,10
	.p2align 3
.L1005:
	cmpl	$56320, %r15d
	movq	(%rax), %r8
	movl	$0, %edx
	movl	$320, %eax
	cmovl	%eax, %edx
	movl	%r15d, %eax
	movl	%r15d, %edi
	sarl	$5, %eax
	andl	$31, %edi
	addl	%edx, %eax
	cltq
	movzwl	(%r8,%rax,2), %edx
	leal	(%rdi,%rdx,4), %edx
	movslq	%edx, %rdx
	movl	(%rcx,%rdx,4), %ecx
	cmpl	$192, %ecx
	jne	.L963
	movq	32(%r10), %r10
	movq	(%r10), %rdx
	movq	16(%rdx), %rcx
	movq	(%rdx), %rdx
	movzwl	(%rdx,%rax,2), %eax
	leal	(%rdi,%rax,4), %eax
	cltq
	salq	$2, %rax
	jmp	.L970
	.p2align 4,,10
	.p2align 3
.L1007:
	movq	%r13, %r9
	xorl	%r8d, %r8d
	movl	%r15d, %edx
	movq	%r10, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6717CollationIterator17appendCEsFromCE32EPKNS_13CollationDataEijaR10UErrorCode
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L1008
	movl	24(%r12), %edx
	cmpl	$1, %edx
	jle	.L974
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*40(%rax)
	movl	%eax, %r15d
	movslq	8(%r14), %rax
	movl	%eax, %esi
	addl	$1, %esi
	js	.L975
	cmpl	12(%r14), %esi
	jle	.L976
.L975:
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	jne	.L977
	movl	8(%r14), %eax
	.p2align 4,,10
	.p2align 3
.L983:
	movl	24(%r12), %edx
	cmpl	%eax, %edx
	jl	.L974
.L1009:
	movl	%eax, %esi
	addl	$1, %esi
	js	.L980
	cmpl	12(%r14), %esi
	jle	.L981
.L980:
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	movl	8(%r14), %eax
	je	.L983
.L981:
	movq	24(%r14), %rdx
	movl	-52(%rbp), %ebx
	cltq
	movl	%ebx, (%rdx,%rax,4)
	movl	8(%r14), %eax
	movl	24(%r12), %edx
	addl	$1, %eax
	movl	%eax, 8(%r14)
	cmpl	%eax, %edx
	jge	.L1009
.L974:
	subl	$1, %edx
	movq	32(%r12), %rax
	movl	%edx, 24(%r12)
	movslq	%edx, %rdx
	movq	(%rax,%rdx,8), %rax
	jmp	.L949
	.p2align 4,,10
	.p2align 3
.L1003:
	movq	32(%r10), %r10
	movq	(%r10), %rax
	movq	16(%rax), %rcx
	movq	(%rax), %rax
	movzwl	(%rax,%rdx,2), %eax
	leal	(%rdi,%rax,4), %eax
	cltq
	salq	$2, %rax
	jmp	.L970
	.p2align 4,,10
	.p2align 3
.L967:
	movl	512(%rcx), %ecx
	cmpl	$192, %ecx
	jne	.L963
	movq	32(%r10), %r10
	movq	(%r10), %rax
	movq	16(%rax), %rcx
	movl	$512, %eax
	jmp	.L970
	.p2align 4,,10
	.p2align 3
.L956:
	cmpl	$65535, %r15d
	jg	.L958
	cmpl	$56320, %r15d
	movl	$320, %eax
	movq	(%rcx), %rdi
	movl	$0, %ecx
	cmovl	%eax, %ecx
	movl	%r15d, %eax
	sarl	$5, %eax
.L1001:
	addl	%ecx, %eax
	cltq
	movzwl	(%rdi,%rax,2), %ecx
	jmp	.L1000
	.p2align 4,,10
	.p2align 3
.L1008:
	movl	$1, %eax
	jmp	.L949
.L968:
	movq	(%rax), %rdi
	movl	%r15d, %eax
	sarl	$11, %eax
	addl	$2080, %eax
	cltq
	movzwl	(%rdi,%rax,2), %edx
	movl	%r15d, %eax
	sarl	$5, %eax
	andl	$63, %eax
	addl	%edx, %eax
	cltq
	movzwl	(%rdi,%rax,2), %edx
	movl	%r15d, %eax
	andl	$31, %eax
	leal	(%rax,%rdx,4), %eax
	cltq
	salq	$2, %rax
	jmp	.L969
.L977:
	movslq	8(%r14), %rax
.L976:
	movq	24(%r14), %rdx
	movl	%r15d, (%rdx,%rax,4)
	movl	8(%r14), %eax
	addl	$1, %eax
	movl	%eax, 8(%r14)
	jmp	.L983
.L958:
	movl	$512, %eax
	cmpl	$1114111, %r15d
	ja	.L957
	cmpl	44(%rcx), %r15d
	jl	.L960
	movslq	48(%rcx), %rax
	salq	$2, %rax
	jmp	.L957
.L1006:
	movq	(%rax), %rdx
	movl	%r15d, %eax
	movl	%r15d, %edi
	sarl	$11, %eax
	sarl	$5, %edi
	addl	$2080, %eax
	andl	$63, %edi
	cltq
	movzwl	(%rdx,%rax,2), %eax
	addl	%edi, %eax
	cltq
	movzwl	(%rdx,%rax,2), %edx
	movl	%r15d, %eax
	andl	$31, %eax
	leal	(%rax,%rdx,4), %eax
	cltq
	salq	$2, %rax
	jmp	.L970
.L960:
	movl	%r15d, %eax
	movq	(%rcx), %rdi
	movl	%r15d, %ecx
	sarl	$11, %eax
	sarl	$5, %ecx
	addl	$2080, %eax
	andl	$63, %ecx
	cltq
	movzwl	(%rdi,%rax,2), %eax
	jmp	.L1001
	.cfi_endproc
.LFE3368:
	.size	_ZN6icu_6717CollationIterator10previousCEERNS_9UVector32ER10UErrorCode, .-_ZN6icu_6717CollationIterator10previousCEERNS_9UVector32ER10UErrorCode
	.weak	_ZTSN6icu_6717CollationIteratorE
	.section	.rodata._ZTSN6icu_6717CollationIteratorE,"aG",@progbits,_ZTSN6icu_6717CollationIteratorE,comdat
	.align 16
	.type	_ZTSN6icu_6717CollationIteratorE, @object
	.size	_ZTSN6icu_6717CollationIteratorE, 29
_ZTSN6icu_6717CollationIteratorE:
	.string	"N6icu_6717CollationIteratorE"
	.weak	_ZTIN6icu_6717CollationIteratorE
	.section	.data.rel.ro._ZTIN6icu_6717CollationIteratorE,"awG",@progbits,_ZTIN6icu_6717CollationIteratorE,comdat
	.align 8
	.type	_ZTIN6icu_6717CollationIteratorE, @object
	.size	_ZTIN6icu_6717CollationIteratorE, 24
_ZTIN6icu_6717CollationIteratorE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6717CollationIteratorE
	.quad	_ZTIN6icu_677UObjectE
	.weak	_ZTVN6icu_6717CollationIteratorE
	.section	.data.rel.ro._ZTVN6icu_6717CollationIteratorE,"awG",@progbits,_ZTVN6icu_6717CollationIteratorE,comdat
	.align 8
	.type	_ZTVN6icu_6717CollationIteratorE, @object
	.size	_ZTVN6icu_6717CollationIteratorE, 144
_ZTVN6icu_6717CollationIteratorE:
	.quad	0
	.quad	_ZTIN6icu_6717CollationIteratorE
	.quad	0
	.quad	0
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZNK6icu_6717CollationIteratoreqERKS0_
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN6icu_6717CollationIterator14handleNextCE32ERiR10UErrorCode
	.quad	_ZN6icu_6717CollationIterator23handleGetTrailSurrogateEv
	.quad	_ZN6icu_6717CollationIterator18foundNULTerminatorEv
	.quad	_ZNK6icu_6717CollationIterator25forbidSurrogateCodePointsEv
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZNK6icu_6717CollationIterator11getDataCE32Ei
	.quad	_ZN6icu_6717CollationIterator22getCE32FromBuilderDataEjR10UErrorCode
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
