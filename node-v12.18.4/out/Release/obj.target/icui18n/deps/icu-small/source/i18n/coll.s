	.file	"coll.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678Collator9safeCloneEv
	.type	_ZNK6icu_678Collator9safeCloneEv, @function
_ZNK6icu_678Collator9safeCloneEv:
.LFB3396:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*40(%rax)
	.cfi_endproc
.LFE3396:
	.size	_ZNK6icu_678Collator9safeCloneEv, .-_ZNK6icu_678Collator9safeCloneEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678Collator7compareERKNS_13UnicodeStringES3_
	.type	_ZNK6icu_678Collator7compareERKNS_13UnicodeStringES3_, @function
_ZNK6icu_678Collator7compareERKNS_13UnicodeStringES3_:
.LFB3397:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	leaq	-12(%rbp), %rcx
	movl	$0, -12(%rbp)
	call	*56(%rax)
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L6
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L6:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3397:
	.size	_ZNK6icu_678Collator7compareERKNS_13UnicodeStringES3_, .-_ZNK6icu_678Collator7compareERKNS_13UnicodeStringES3_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678Collator7compareERKNS_13UnicodeStringES3_i
	.type	_ZNK6icu_678Collator7compareERKNS_13UnicodeStringES3_i, @function
_ZNK6icu_678Collator7compareERKNS_13UnicodeStringES3_i:
.LFB3398:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movl	$0, -12(%rbp)
	leaq	-12(%rbp), %r8
	call	*72(%rax)
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L10
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L10:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3398:
	.size	_ZNK6icu_678Collator7compareERKNS_13UnicodeStringES3_i, .-_ZNK6icu_678Collator7compareERKNS_13UnicodeStringES3_i
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678Collator7compareEPKDsiS2_i
	.type	_ZNK6icu_678Collator7compareEPKDsiS2_i, @function
_ZNK6icu_678Collator7compareEPKDsiS2_i:
.LFB3399:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movl	$0, -12(%rbp)
	leaq	-12(%rbp), %r9
	call	*88(%rax)
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L14
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L14:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3399:
	.size	_ZNK6icu_678Collator7compareEPKDsiS2_i, .-_ZNK6icu_678Collator7compareEPKDsiS2_i
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678Collator7compareER13UCharIteratorS2_R10UErrorCode
	.type	_ZNK6icu_678Collator7compareER13UCharIteratorS2_R10UErrorCode, @function
_ZNK6icu_678Collator7compareER13UCharIteratorS2_R10UErrorCode:
.LFB3400:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L16
	movl	$16, (%rcx)
.L16:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3400:
	.size	_ZNK6icu_678Collator7compareER13UCharIteratorS2_R10UErrorCode, .-_ZNK6icu_678Collator7compareER13UCharIteratorS2_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Collator10setLocalesERKNS_6LocaleES3_S3_
	.type	_ZN6icu_678Collator10setLocalesERKNS_6LocaleES3_S3_, @function
_ZN6icu_678Collator10setLocalesERKNS_6LocaleES3_S3_:
.LFB3424:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3424:
	.size	_ZN6icu_678Collator10setLocalesERKNS_6LocaleES3_S3_, .-_ZN6icu_678Collator10setLocalesERKNS_6LocaleES3_S3_
	.section	.text._ZNK6icu_6730CollationLocaleListEnumeration5countER10UErrorCode,"axG",@progbits,_ZNK6icu_6730CollationLocaleListEnumeration5countER10UErrorCode,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6730CollationLocaleListEnumeration5countER10UErrorCode
	.type	_ZNK6icu_6730CollationLocaleListEnumeration5countER10UErrorCode, @function
_ZNK6icu_6730CollationLocaleListEnumeration5countER10UErrorCode:
.LFB3430:
	.cfi_startproc
	endbr64
	movl	_ZL24availableLocaleListCount(%rip), %eax
	ret
	.cfi_endproc
.LFE3430:
	.size	_ZNK6icu_6730CollationLocaleListEnumeration5countER10UErrorCode, .-_ZNK6icu_6730CollationLocaleListEnumeration5countER10UErrorCode
	.section	.text._ZN6icu_6730CollationLocaleListEnumeration5resetER10UErrorCode,"axG",@progbits,_ZN6icu_6730CollationLocaleListEnumeration5resetER10UErrorCode,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6730CollationLocaleListEnumeration5resetER10UErrorCode
	.type	_ZN6icu_6730CollationLocaleListEnumeration5resetER10UErrorCode, @function
_ZN6icu_6730CollationLocaleListEnumeration5resetER10UErrorCode:
.LFB3433:
	.cfi_startproc
	endbr64
	movl	$0, 116(%rdi)
	ret
	.cfi_endproc
.LFE3433:
	.size	_ZN6icu_6730CollationLocaleListEnumeration5resetER10UErrorCode, .-_ZN6icu_6730CollationLocaleListEnumeration5resetER10UErrorCode
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6730CollationLocaleListEnumeration17getDynamicClassIDEv
	.type	_ZNK6icu_6730CollationLocaleListEnumeration17getDynamicClassIDEv, @function
_ZNK6icu_6730CollationLocaleListEnumeration17getDynamicClassIDEv:
.LFB3439:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6730CollationLocaleListEnumeration16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3439:
	.size	_ZNK6icu_6730CollationLocaleListEnumeration17getDynamicClassIDEv, .-_ZNK6icu_6730CollationLocaleListEnumeration17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678Collator11getStrengthEv
	.type	_ZNK6icu_678Collator11getStrengthEv, @function
_ZNK6icu_678Collator11getStrengthEv:
.LFB3445:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$5, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movl	$0, -12(%rbp)
	leaq	-12(%rbp), %rdx
	call	*192(%rax)
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L24
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L24:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3445:
	.size	_ZNK6icu_678Collator11getStrengthEv, .-_ZNK6icu_678Collator11getStrengthEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Collator11setStrengthENS0_18ECollationStrengthE
	.type	_ZN6icu_678Collator11setStrengthENS0_18ECollationStrengthE, @function
_ZN6icu_678Collator11setStrengthENS0_18ECollationStrengthE:
.LFB3446:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %edx
	movl	$5, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movl	$0, -12(%rbp)
	leaq	-12(%rbp), %rcx
	call	*184(%rax)
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L28
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L28:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3446:
	.size	_ZN6icu_678Collator11setStrengthENS0_18ECollationStrengthE, .-_ZN6icu_678Collator11setStrengthENS0_18ECollationStrengthE
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Collator14setMaxVariableE15UColReorderCodeR10UErrorCode
	.type	_ZN6icu_678Collator14setMaxVariableE15UColReorderCodeR10UErrorCode, @function
_ZN6icu_678Collator14setMaxVariableE15UColReorderCodeR10UErrorCode:
.LFB3447:
	.cfi_startproc
	endbr64
	movl	(%rdx), %ecx
	movq	%rdi, %rax
	testl	%ecx, %ecx
	jle	.L31
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	movl	$16, (%rdx)
	ret
	.cfi_endproc
.LFE3447:
	.size	_ZN6icu_678Collator14setMaxVariableE15UColReorderCodeR10UErrorCode, .-_ZN6icu_678Collator14setMaxVariableE15UColReorderCodeR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678Collator14getMaxVariableEv
	.type	_ZNK6icu_678Collator14getMaxVariableEv, @function
_ZNK6icu_678Collator14getMaxVariableEv:
.LFB3448:
	.cfi_startproc
	endbr64
	movl	$4097, %eax
	ret
	.cfi_endproc
.LFE3448:
	.size	_ZNK6icu_678Collator14getMaxVariableEv, .-_ZNK6icu_678Collator14getMaxVariableEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678Collator15getReorderCodesEPiiR10UErrorCode
	.type	_ZNK6icu_678Collator15getReorderCodesEPiiR10UErrorCode, @function
_ZNK6icu_678Collator15getReorderCodesEPiiR10UErrorCode:
.LFB3449:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L34
	movl	$16, (%rcx)
.L34:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3449:
	.size	_ZNK6icu_678Collator15getReorderCodesEPiiR10UErrorCode, .-_ZNK6icu_678Collator15getReorderCodesEPiiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Collator15setReorderCodesEPKiiR10UErrorCode
	.type	_ZN6icu_678Collator15setReorderCodesEPKiiR10UErrorCode, @function
_ZN6icu_678Collator15setReorderCodesEPKiiR10UErrorCode:
.LFB3450:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jle	.L37
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	movl	$16, (%rcx)
	ret
	.cfi_endproc
.LFE3450:
	.size	_ZN6icu_678Collator15setReorderCodesEPKiiR10UErrorCode, .-_ZN6icu_678Collator15setReorderCodesEPKiiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678Collator32internalGetShortDefinitionStringEPKcPciR10UErrorCode
	.type	_ZNK6icu_678Collator32internalGetShortDefinitionStringEPKcPciR10UErrorCode, @function
_ZNK6icu_678Collator32internalGetShortDefinitionStringEPKcPciR10UErrorCode:
.LFB3452:
	.cfi_startproc
	endbr64
	movl	(%r8), %eax
	testl	%eax, %eax
	jg	.L39
	movl	$16, (%r8)
.L39:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3452:
	.size	_ZNK6icu_678Collator32internalGetShortDefinitionStringEPKcPciR10UErrorCode, .-_ZNK6icu_678Collator32internalGetShortDefinitionStringEPKcPciR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678Collator23internalNextSortKeyPartEP13UCharIteratorPjPhiR10UErrorCode
	.type	_ZNK6icu_678Collator23internalNextSortKeyPartEP13UCharIteratorPjPhiR10UErrorCode, @function
_ZNK6icu_678Collator23internalNextSortKeyPartEP13UCharIteratorPjPhiR10UErrorCode:
.LFB3454:
	.cfi_startproc
	endbr64
	movl	(%r9), %eax
	testl	%eax, %eax
	jg	.L41
	movl	$16, (%r9)
.L41:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3454:
	.size	_ZNK6icu_678Collator23internalNextSortKeyPartEP13UCharIteratorPjPhiR10UErrorCode, .-_ZNK6icu_678Collator23internalNextSortKeyPartEP13UCharIteratorPjPhiR10UErrorCode
	.p2align 4
	.type	collator_cleanup, @function
collator_cleanup:
.LFB3388:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	_ZL19availableLocaleList(%rip), %rax
	testq	%rax, %rax
	je	.L43
	movq	-8(%rax), %rdx
	leaq	0(,%rdx,8), %rbx
	subq	%rdx, %rbx
	salq	$5, %rbx
	addq	%rax, %rbx
	cmpq	%rbx, %rax
	je	.L44
	.p2align 4,,10
	.p2align 3
.L45:
	movq	-224(%rbx), %rax
	subq	$224, %rbx
	movq	%rbx, %rdi
	call	*(%rax)
	cmpq	%rbx, _ZL19availableLocaleList(%rip)
	jne	.L45
.L44:
	leaq	-8(%rbx), %rdi
	call	_ZN6icu_677UMemorydaEPv@PLT
	movq	$0, _ZL19availableLocaleList(%rip)
.L43:
	movl	$0, _ZL24availableLocaleListCount(%rip)
	movl	$1, %eax
	movl	$0, _ZL28gAvailableLocaleListInitOnce(%rip)
	mfence
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3388:
	.size	collator_cleanup, .-collator_cleanup
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678CollatoreqERKS0_
	.type	_ZNK6icu_678CollatoreqERKS0_, @function
_ZNK6icu_678CollatoreqERKS0_:
.LFB3421:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	-8(%rax), %rax
	movq	8(%rax), %rdi
	movq	(%rsi), %rax
	movq	-8(%rax), %rax
	movq	8(%rax), %rsi
	movl	$1, %eax
	cmpq	%rsi, %rdi
	je	.L56
	xorl	%eax, %eax
	cmpb	$42, (%rdi)
	je	.L56
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	strcmp@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE3421:
	.size	_ZNK6icu_678CollatoreqERKS0_, .-_ZNK6icu_678CollatoreqERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6730CollationLocaleListEnumerationD2Ev
	.type	_ZN6icu_6730CollationLocaleListEnumerationD2Ev, @function
_ZN6icu_6730CollationLocaleListEnumerationD2Ev:
.LFB3435:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6730CollationLocaleListEnumerationE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6717StringEnumerationD2Ev@PLT
	.cfi_endproc
.LFE3435:
	.size	_ZN6icu_6730CollationLocaleListEnumerationD2Ev, .-_ZN6icu_6730CollationLocaleListEnumerationD2Ev
	.globl	_ZN6icu_6730CollationLocaleListEnumerationD1Ev
	.set	_ZN6icu_6730CollationLocaleListEnumerationD1Ev,_ZN6icu_6730CollationLocaleListEnumerationD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6730CollationLocaleListEnumerationD0Ev
	.type	_ZN6icu_6730CollationLocaleListEnumerationD0Ev, @function
_ZN6icu_6730CollationLocaleListEnumerationD0Ev:
.LFB3437:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6730CollationLocaleListEnumerationE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6717StringEnumerationD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3437:
	.size	_ZN6icu_6730CollationLocaleListEnumerationD0Ev, .-_ZN6icu_6730CollationLocaleListEnumerationD0Ev
	.section	.text._ZNK6icu_6730CollationLocaleListEnumeration5cloneEv,"axG",@progbits,_ZNK6icu_6730CollationLocaleListEnumeration5cloneEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6730CollationLocaleListEnumeration5cloneEv
	.type	_ZNK6icu_6730CollationLocaleListEnumeration5cloneEv, @function
_ZNK6icu_6730CollationLocaleListEnumeration5cloneEv:
.LFB3429:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	$120, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L62
	movq	%rax, %rdi
	call	_ZN6icu_6717StringEnumerationC2Ev@PLT
	leaq	16+_ZTVN6icu_6730CollationLocaleListEnumerationE(%rip), %rax
	movl	$0, 116(%r12)
	movq	%rax, (%r12)
	movl	116(%rbx), %eax
	movl	%eax, 116(%r12)
.L62:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3429:
	.size	_ZNK6icu_6730CollationLocaleListEnumeration5cloneEv, .-_ZNK6icu_6730CollationLocaleListEnumeration5cloneEv
	.section	.text._ZN6icu_6730CollationLocaleListEnumeration4nextEPiR10UErrorCode,"axG",@progbits,_ZN6icu_6730CollationLocaleListEnumeration4nextEPiR10UErrorCode,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6730CollationLocaleListEnumeration4nextEPiR10UErrorCode
	.type	_ZN6icu_6730CollationLocaleListEnumeration4nextEPiR10UErrorCode, @function
_ZN6icu_6730CollationLocaleListEnumeration4nextEPiR10UErrorCode:
.LFB3431:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movslq	116(%rdi), %rax
	movq	%rsi, %rbx
	cmpl	_ZL24availableLocaleListCount(%rip), %eax
	jge	.L69
	leal	1(%rax), %edx
	movl	%edx, 116(%rdi)
	leaq	0(,%rax,8), %rdx
	subq	%rax, %rdx
	movq	%rdx, %rax
	salq	$5, %rax
	addq	_ZL19availableLocaleList(%rip), %rax
	movq	40(%rax), %r12
	testq	%rsi, %rsi
	je	.L68
	movq	%r12, %rdi
	call	strlen@PLT
	movl	%eax, (%rbx)
.L68:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L69:
	.cfi_restore_state
	xorl	%r12d, %r12d
	testq	%rsi, %rsi
	je	.L68
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	movl	$0, (%rsi)
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3431:
	.size	_ZN6icu_6730CollationLocaleListEnumeration4nextEPiR10UErrorCode, .-_ZN6icu_6730CollationLocaleListEnumeration4nextEPiR10UErrorCode
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678Collator14getTailoredSetER10UErrorCode
	.type	_ZNK6icu_678Collator14getTailoredSetER10UErrorCode, @function
_ZNK6icu_678Collator14getTailoredSetER10UErrorCode:
.LFB3425:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	xorl	%r12d, %r12d
	subq	$8, %rsp
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L77
	movl	$200, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L77
	movl	$1114111, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSetC1Eii@PLT
.L77:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3425:
	.size	_ZNK6icu_678Collator14getTailoredSetER10UErrorCode, .-_ZNK6icu_678Collator14getTailoredSetER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678Collator11compareUTF8ERKNS_11StringPieceES3_R10UErrorCode
	.type	_ZNK6icu_678Collator11compareUTF8ERKNS_11StringPieceES3_R10UErrorCode, @function
_ZNK6icu_678Collator11compareUTF8ERKNS_11StringPieceES3_R10UErrorCode:
.LFB3401:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$264, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rcx), %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jle	.L92
.L84:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L93
	addq	$264, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L92:
	.cfi_restore_state
	movq	%rdx, %rbx
	leaq	-288(%rbp), %r15
	movl	8(%rsi), %edx
	movq	(%rsi), %rsi
	movq	%rdi, %r14
	movq	%r15, %rdi
	movq	%rcx, %r12
	call	uiter_setUTF8_67@PLT
	leaq	-176(%rbp), %r8
	movl	8(%rbx), %edx
	movq	(%rbx), %rsi
	movq	%r8, %rdi
	movq	%r8, -296(%rbp)
	call	uiter_setUTF8_67@PLT
	movq	(%r14), %rax
	leaq	_ZNK6icu_678Collator7compareER13UCharIteratorS2_R10UErrorCode(%rip), %rdx
	movq	-296(%rbp), %r8
	movq	96(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L86
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L84
	movl	$16, (%r12)
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L86:
	movq	%r12, %rcx
	movq	%r8, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	*%rax
	movl	%eax, %r13d
	jmp	.L84
.L93:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3401:
	.size	_ZNK6icu_678Collator11compareUTF8ERKNS_11StringPieceES3_R10UErrorCode, .-_ZNK6icu_678Collator11compareUTF8ERKNS_11StringPieceES3_R10UErrorCode
	.section	.text._ZN6icu_6730CollationLocaleListEnumeration5snextER10UErrorCode,"axG",@progbits,_ZN6icu_6730CollationLocaleListEnumeration5snextER10UErrorCode,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6730CollationLocaleListEnumeration5snextER10UErrorCode
	.type	_ZN6icu_6730CollationLocaleListEnumeration5snextER10UErrorCode, @function
_ZN6icu_6730CollationLocaleListEnumeration5snextER10UErrorCode:
.LFB3432:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN6icu_6730CollationLocaleListEnumeration4nextEPiR10UErrorCode(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movl	$0, -44(%rbp)
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L95
	movslq	116(%rdi), %rax
	cmpl	_ZL24availableLocaleListCount(%rip), %eax
	jl	.L100
	xorl	%edx, %edx
	xorl	%r14d, %r14d
.L96:
	movq	%r13, %rcx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6717StringEnumeration8setCharsEPKciR10UErrorCode@PLT
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L101
	addq	$24, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L100:
	.cfi_restore_state
	leal	1(%rax), %edx
	movl	%edx, 116(%rdi)
	leaq	0(,%rax,8), %rdx
	subq	%rax, %rdx
	movq	%rdx, %rax
	salq	$5, %rax
	addq	_ZL19availableLocaleList(%rip), %rax
	movq	40(%rax), %r14
	movq	%r14, %rdi
	call	strlen@PLT
	movl	%eax, -44(%rbp)
	movl	%eax, %edx
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L95:
	movq	%r13, %rdx
	leaq	-44(%rbp), %rsi
	call	*%rax
	movl	-44(%rbp), %edx
	movq	%rax, %r14
	jmp	.L96
.L101:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3432:
	.size	_ZN6icu_6730CollationLocaleListEnumeration5snextER10UErrorCode, .-_ZN6icu_6730CollationLocaleListEnumeration5snextER10UErrorCode
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678Collator19internalCompareUTF8EPKciS2_iR10UErrorCode
	.type	_ZNK6icu_678Collator19internalCompareUTF8EPKciS2_iR10UErrorCode, @function
_ZNK6icu_678Collator19internalCompareUTF8EPKciS2_iR10UErrorCode:
.LFB3453:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$288, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	(%r9), %r10d
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%r10d, %r10d
	jg	.L102
	movq	%rdi, %r13
	movq	%rsi, %r14
	movq	%r9, %r12
	testq	%rsi, %rsi
	jne	.L116
	testl	%edx, %edx
	je	.L116
.L104:
	movl	$1, (%r12)
	xorl	%eax, %eax
.L102:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L127
	addq	$288, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L116:
	.cfi_restore_state
	testq	%rcx, %rcx
	jne	.L106
	testl	%r8d, %r8d
	jne	.L104
.L106:
	movq	0(%r13), %rax
	movq	104(%rax), %rbx
	testl	%r8d, %r8d
	js	.L128
.L107:
	movq	%rcx, -304(%rbp)
	movl	%r8d, -296(%rbp)
	testl	%edx, %edx
	js	.L129
.L108:
	leaq	_ZNK6icu_678Collator11compareUTF8ERKNS_11StringPieceES3_R10UErrorCode(%rip), %rax
	movq	%r14, -288(%rbp)
	movl	%edx, -280(%rbp)
	cmpq	%rax, %rbx
	jne	.L109
	leaq	-272(%rbp), %rbx
	movq	%r14, %rsi
	leaq	-160(%rbp), %r14
	movq	%rbx, %rdi
	call	uiter_setUTF8_67@PLT
	movl	-296(%rbp), %edx
	movq	-304(%rbp), %rsi
	movq	%r14, %rdi
	call	uiter_setUTF8_67@PLT
	movq	0(%r13), %rax
	leaq	_ZNK6icu_678Collator7compareER13UCharIteratorS2_R10UErrorCode(%rip), %rdx
	movq	96(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L110
	movl	(%r12), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L102
	movl	$16, (%r12)
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L129:
	movq	%r14, %rdi
	call	strlen@PLT
	movl	%eax, %edx
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L128:
	movq	%rcx, %rdi
	movl	%edx, -316(%rbp)
	movq	%rcx, -312(%rbp)
	call	strlen@PLT
	movl	-316(%rbp), %edx
	movq	-312(%rbp), %rcx
	movl	%eax, %r8d
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L109:
	leaq	-304(%rbp), %rdx
	leaq	-288(%rbp), %rsi
	movq	%r12, %rcx
	movq	%r13, %rdi
	call	*%rbx
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L110:
	movq	%r12, %rcx
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	*%rax
	jmp	.L102
.L127:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3453:
	.size	_ZNK6icu_678Collator19internalCompareUTF8EPKciS2_iR10UErrorCode, .-_ZNK6icu_678Collator19internalCompareUTF8EPKciS2_iR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678CollatorneERKS0_
	.type	_ZNK6icu_678CollatorneERKS0_, @function
_ZNK6icu_678CollatorneERKS0_:
.LFB3422:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %rax
	leaq	_ZNK6icu_678CollatoreqERKS0_(%rip), %rcx
	movq	24(%rax), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpq	%rcx, %rdx
	jne	.L131
	movq	-8(%rax), %rax
	movq	8(%rax), %rdi
	movq	(%rsi), %rax
	movq	-8(%rax), %rax
	movq	8(%rax), %rsi
	xorl	%eax, %eax
	cmpq	%rsi, %rdi
	je	.L130
	cmpb	$42, (%rdi)
	movl	$1, %eax
	je	.L130
	call	strcmp@PLT
	testl	%eax, %eax
	setne	%al
.L130:
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L131:
	.cfi_restore_state
	call	*%rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	testb	%al, %al
	sete	%al
	ret
	.cfi_endproc
.LFE3422:
	.size	_ZNK6icu_678CollatorneERKS0_, .-_ZNK6icu_678CollatorneERKS0_
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"colHiraganaQuaternary"
.LC1:
	.string	"variableTop"
.LC2:
	.string	"colReorder"
.LC3:
	.string	"kv"
	.text
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_125setAttributesFromKeywordsERKNS_6LocaleERNS_8CollatorER10UErrorCode, @function
_ZN6icu_6712_GLOBAL__N_125setAttributesFromKeywordsERKNS_6LocaleERNS_8CollatorER10UErrorCode:
.LFB3392:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$1880, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rdx), %ecx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jle	.L192
.L137:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L193
	addq	$1880, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L192:
	.cfi_restore_state
	movq	%rdi, %r15
	movq	%rsi, %r12
	movq	%rdx, %r14
	call	_ZNK6icu_676Locale11getBaseNameEv@PLT
	movq	40(%r15), %rdi
	movq	%rax, %rsi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L137
	leaq	-1088(%rbp), %rbx
	movq	%r14, %r8
	movl	$1024, %ecx
	movq	%r15, %rdi
	movq	%rbx, %rdx
	leaq	.LC0(%rip), %rsi
	call	_ZNK6icu_676Locale15getKeywordValueEPKcPciR10UErrorCode@PLT
	movl	(%r14), %edx
	testl	%edx, %edx
	jg	.L159
	testl	%eax, %eax
	jne	.L143
	movq	%rbx, %rdx
	movq	%r14, %r8
	movl	$1024, %ecx
	movq	%r15, %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZNK6icu_676Locale15getKeywordValueEPKcPciR10UErrorCode@PLT
	movl	(%r14), %edx
	testl	%edx, %edx
	jg	.L159
	testl	%eax, %eax
	jne	.L143
	cmpl	$-124, %edx
	jne	.L144
	movl	$0, (%r14)
.L144:
	leaq	_ZN6icu_6712_GLOBAL__N_1L14collAttributesE(%rip), %rax
	movq	%rax, -1904(%rbp)
.L148:
	movq	-1904(%rbp), %rax
	movq	%rbx, %rdx
	movq	%r14, %r8
	movq	%r15, %rdi
	movl	$1024, %ecx
	movq	(%rax), %rsi
	call	_ZNK6icu_676Locale15getKeywordValueEPKcPciR10UErrorCode@PLT
	movl	(%r14), %edx
	testl	%edx, %edx
	jg	.L159
	cmpl	$-124, %edx
	je	.L159
	testl	%eax, %eax
	jne	.L194
.L145:
	addq	$16, -1904(%rbp)
	movq	-1904(%rbp), %rax
	leaq	112+_ZN6icu_6712_GLOBAL__N_1L14collAttributesE(%rip), %rcx
	cmpq	%rax, %rcx
	jne	.L148
	movq	%rbx, %rdx
	movq	%r14, %r8
	movl	$1024, %ecx
	movq	%r15, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZNK6icu_676Locale15getKeywordValueEPKcPciR10UErrorCode@PLT
	movl	(%r14), %edx
	testl	%edx, %edx
	jg	.L159
	cmpl	$-124, %edx
	je	.L159
	testl	%eax, %eax
	je	.L149
	movq	$1, -1920(%rbp)
	movq	%rbx, %r11
	.p2align 4,,10
	.p2align 3
.L161:
	movzbl	(%r11), %ecx
	movq	%r11, %r13
	testb	%cl, %cl
	jne	.L150
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L152:
	movzbl	1(%r13), %ecx
	addq	$1, %r13
	testb	%cl, %cl
	je	.L154
.L150:
	cmpb	$45, %cl
	jne	.L152
.L154:
	movq	%r13, %rax
	movb	$0, 0(%r13)
	subq	%r11, %rax
	cmpq	$4, %rax
	je	.L195
.L151:
	xorl	%r10d, %r10d
.L158:
	leaq	_ZN6icu_6712_GLOBAL__N_1L16collReorderCodesE(%rip), %rax
	movq	%r11, %rdi
	movb	%cl, -1905(%rbp)
	movq	(%rax,%r10,8), %rsi
	movl	%r10d, -1912(%rbp)
	movq	%r10, -1904(%rbp)
	movq	%r11, -1896(%rbp)
	call	uprv_stricmp_67@PLT
	movq	-1896(%rbp), %r11
	movq	-1904(%rbp), %r10
	testl	%eax, %eax
	movzbl	-1905(%rbp), %ecx
	je	.L196
	addq	$1, %r10
	cmpq	$5, %r10
	jne	.L158
	.p2align 4,,10
	.p2align 3
.L159:
	movl	$1, (%r14)
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L143:
	movl	$16, (%r14)
	jmp	.L137
.L195:
	movq	%r11, %rsi
	movl	$4106, %edi
	movb	%cl, -1896(%rbp)
	call	u_getPropertyValueEnum_67@PLT
	movzbl	-1896(%rbp), %ecx
	testl	%eax, %eax
	js	.L159
.L156:
	movq	-1920(%rbp), %rdx
	leaq	-1888(%rbp), %rsi
	movl	%eax, -1892(%rbp,%rdx,4)
	testb	%cl, %cl
	je	.L160
	addq	$1, %rdx
	leaq	1(%r13), %r11
	movq	%rdx, -1920(%rbp)
	cmpq	$199, %rdx
	jne	.L161
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L194:
	leaq	_ZN6icu_6712_GLOBAL__N_1L19collAttributeValuesE(%rip), %rcx
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L147:
	movq	(%rcx), %rsi
	movq	%rbx, %rdi
	movq	%rcx, -1896(%rbp)
	call	uprv_stricmp_67@PLT
	movq	-1896(%rbp), %rcx
	testl	%eax, %eax
	je	.L197
	addl	$1, %r13d
	addq	$16, %rcx
	cmpl	$11, %r13d
	jne	.L147
	jmp	.L159
.L197:
	movq	-1904(%rbp), %rax
	movslq	%r13d, %rdx
	leaq	_ZN6icu_6712_GLOBAL__N_1L19collAttributeValuesE(%rip), %rdi
	movq	%r14, %rcx
	salq	$4, %rdx
	movl	8(%rax), %esi
	movq	(%r12), %rax
	movl	8(%rdi,%rdx), %edx
	movq	%r12, %rdi
	call	*184(%rax)
	jmp	.L145
.L196:
	movl	-1912(%rbp), %eax
	addl	$4096, %eax
	jmp	.L156
.L160:
	movq	(%r12), %rax
	movl	-1920(%rbp), %edx
	movq	%r14, %rcx
	movq	%r12, %rdi
	call	*168(%rax)
.L149:
	movq	%rbx, %rdx
	movq	%r14, %r8
	movl	$1024, %ecx
	movq	%r15, %rdi
	leaq	.LC3(%rip), %rsi
	call	_ZNK6icu_676Locale15getKeywordValueEPKcPciR10UErrorCode@PLT
	movl	(%r14), %edx
	testl	%edx, %edx
	jg	.L159
	cmpl	$-124, %edx
	je	.L159
	testl	%eax, %eax
	je	.L137
	xorl	%r13d, %r13d
	leaq	_ZN6icu_6712_GLOBAL__N_1L16collReorderCodesE(%rip), %r15
.L163:
	movq	(%r15,%r13,8), %rsi
	movq	%rbx, %rdi
	movl	%r13d, -1896(%rbp)
	call	uprv_stricmp_67@PLT
	testl	%eax, %eax
	je	.L198
	addq	$1, %r13
	cmpq	$5, %r13
	jne	.L163
	jmp	.L159
.L198:
	movl	-1896(%rbp), %esi
	movq	(%r12), %rax
	movq	%r14, %rdx
	movq	%r12, %rdi
	addl	$4096, %esi
	call	*200(%rax)
	cmpl	$0, (%r14)
	jg	.L159
	jmp	.L137
.L193:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3392:
	.size	_ZN6icu_6712_GLOBAL__N_125setAttributesFromKeywordsERKNS_6LocaleERNS_8CollatorER10UErrorCode, .-_ZN6icu_6712_GLOBAL__N_125setAttributesFromKeywordsERKNS_6LocaleERNS_8CollatorER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Collator14createInstanceER10UErrorCode
	.type	_ZN6icu_678Collator14createInstanceER10UErrorCode, @function
_ZN6icu_678Collator14createInstanceER10UErrorCode:
.LFB3393:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	call	_ZN6icu_676Locale10getDefaultEv@PLT
	movl	(%rbx), %esi
	testl	%esi, %esi
	jg	.L212
	cmpb	$0, 216(%rax)
	movq	%rax, %r12
	jne	.L213
	movq	%rbx, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6715CollationLoader13loadTailoringERKNS_6LocaleER10UErrorCode@PLT
	movl	(%rbx), %ecx
	movq	%rax, %r14
	testl	%ecx, %ecx
	jle	.L214
.L203:
	testq	%r14, %r14
	je	.L212
	movq	%r14, %rdi
	xorl	%r13d, %r13d
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
.L205:
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L212
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6712_GLOBAL__N_125setAttributesFromKeywordsERKNS_6LocaleERNS_8CollatorER10UErrorCode
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L199
	testq	%r13, %r13
	je	.L212
	movq	0(%r13), %rax
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	call	*8(%rax)
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L213:
	movl	$1, (%rbx)
.L212:
	xorl	%r13d, %r13d
.L199:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L214:
	.cfi_restore_state
	movl	$272, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L204
	movq	%rax, %rdi
	movq	%r14, %rsi
	call	_ZN6icu_6717RuleBasedCollatorC1EPKNS_19CollationCacheEntryE@PLT
	movq	%r14, %rdi
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	jmp	.L205
.L204:
	movl	$7, (%rbx)
	jmp	.L203
	.cfi_endproc
.LFE3393:
	.size	_ZN6icu_678Collator14createInstanceER10UErrorCode, .-_ZN6icu_678Collator14createInstanceER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Collator14createInstanceERKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_678Collator14createInstanceERKNS_6LocaleER10UErrorCode, @function
_ZN6icu_678Collator14createInstanceERKNS_6LocaleER10UErrorCode:
.LFB3394:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	(%rsi), %r8d
	testl	%r8d, %r8d
	jg	.L228
	cmpb	$0, 216(%rdi)
	movq	%rdi, %r12
	movq	%rsi, %rbx
	jne	.L229
	call	_ZN6icu_6715CollationLoader13loadTailoringERKNS_6LocaleER10UErrorCode@PLT
	movl	(%rbx), %ecx
	movq	%rax, %r14
	testl	%ecx, %ecx
	jle	.L230
.L219:
	testq	%r14, %r14
	je	.L228
	movq	%r14, %rdi
	xorl	%r13d, %r13d
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
.L221:
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L228
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6712_GLOBAL__N_125setAttributesFromKeywordsERKNS_6LocaleERNS_8CollatorER10UErrorCode
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L215
	testq	%r13, %r13
	je	.L228
	movq	0(%r13), %rax
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	call	*8(%rax)
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L229:
	movl	$1, (%rsi)
.L228:
	xorl	%r13d, %r13d
.L215:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L230:
	.cfi_restore_state
	movl	$272, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L220
	movq	%rax, %rdi
	movq	%r14, %rsi
	call	_ZN6icu_6717RuleBasedCollatorC1EPKNS_19CollationCacheEntryE@PLT
	movq	%r14, %rdi
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	jmp	.L221
.L220:
	movl	$7, (%rbx)
	jmp	.L219
	.cfi_endproc
.LFE3394:
	.size	_ZN6icu_678Collator14createInstanceERKNS_6LocaleER10UErrorCode, .-_ZN6icu_678Collator14createInstanceERKNS_6LocaleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Collator12makeInstanceERKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_678Collator12makeInstanceERKNS_6LocaleER10UErrorCode, @function
_ZN6icu_678Collator12makeInstanceERKNS_6LocaleER10UErrorCode:
.LFB3395:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_6715CollationLoader13loadTailoringERKNS_6LocaleER10UErrorCode@PLT
	movq	%rax, %r12
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L239
.L232:
	testq	%r12, %r12
	je	.L231
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
.L231:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L239:
	.cfi_restore_state
	movl	$272, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L233
	movq	%rax, %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6717RuleBasedCollatorC1EPKNS_19CollationCacheEntryE@PLT
	movq	%r12, %rdi
	movq	%r13, %r12
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L233:
	.cfi_restore_state
	movl	$7, (%rbx)
	jmp	.L232
	.cfi_endproc
.LFE3395:
	.size	_ZN6icu_678Collator12makeInstanceERKNS_6LocaleER10UErrorCode, .-_ZN6icu_678Collator12makeInstanceERKNS_6LocaleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678Collator6equalsERKNS_13UnicodeStringES3_
	.type	_ZNK6icu_678Collator6equalsERKNS_13UnicodeStringES3_, @function
_ZNK6icu_678Collator6equalsERKNS_13UnicodeStringES3_:
.LFB3402:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	leaq	-12(%rbp), %rcx
	movl	$0, -12(%rbp)
	call	*56(%rax)
	testl	%eax, %eax
	sete	%al
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L243
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L243:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3402:
	.size	_ZNK6icu_678Collator6equalsERKNS_13UnicodeStringES3_, .-_ZNK6icu_678Collator6equalsERKNS_13UnicodeStringES3_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678Collator14greaterOrEqualERKNS_13UnicodeStringES3_
	.type	_ZNK6icu_678Collator14greaterOrEqualERKNS_13UnicodeStringES3_, @function
_ZNK6icu_678Collator14greaterOrEqualERKNS_13UnicodeStringES3_:
.LFB3403:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	leaq	-12(%rbp), %rcx
	movl	$0, -12(%rbp)
	call	*56(%rax)
	cmpl	$-1, %eax
	setne	%al
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L247
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L247:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3403:
	.size	_ZNK6icu_678Collator14greaterOrEqualERKNS_13UnicodeStringES3_, .-_ZNK6icu_678Collator14greaterOrEqualERKNS_13UnicodeStringES3_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678Collator7greaterERKNS_13UnicodeStringES3_
	.type	_ZNK6icu_678Collator7greaterERKNS_13UnicodeStringES3_, @function
_ZNK6icu_678Collator7greaterERKNS_13UnicodeStringES3_:
.LFB3404:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	leaq	-12(%rbp), %rcx
	movl	$0, -12(%rbp)
	call	*56(%rax)
	cmpl	$1, %eax
	sete	%al
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L251
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L251:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3404:
	.size	_ZNK6icu_678Collator7greaterERKNS_13UnicodeStringES3_, .-_ZNK6icu_678Collator7greaterERKNS_13UnicodeStringES3_
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Collator14getDisplayNameERKNS_6LocaleES3_RNS_13UnicodeStringE
	.type	_ZN6icu_678Collator14getDisplayNameERKNS_6LocaleES3_RNS_13UnicodeStringE, @function
_ZN6icu_678Collator14getDisplayNameERKNS_6LocaleES3_RNS_13UnicodeStringE:
.LFB3406:
	.cfi_startproc
	endbr64
	jmp	_ZNK6icu_676Locale14getDisplayNameERKS0_RNS_13UnicodeStringE@PLT
	.cfi_endproc
.LFE3406:
	.size	_ZN6icu_678Collator14getDisplayNameERKNS_6LocaleES3_RNS_13UnicodeStringE, .-_ZN6icu_678Collator14getDisplayNameERKNS_6LocaleES3_RNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Collator14getDisplayNameERKNS_6LocaleERNS_13UnicodeStringE
	.type	_ZN6icu_678Collator14getDisplayNameERKNS_6LocaleERNS_13UnicodeStringE, @function
_ZN6icu_678Collator14getDisplayNameERKNS_6LocaleERNS_13UnicodeStringE:
.LFB3407:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN6icu_676Locale10getDefaultEv@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	popq	%r12
	movq	%rax, %rsi
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_676Locale14getDisplayNameERKS0_RNS_13UnicodeStringE@PLT
	.cfi_endproc
.LFE3407:
	.size	_ZN6icu_678Collator14getDisplayNameERKNS_6LocaleERNS_13UnicodeStringE, .-_ZN6icu_678Collator14getDisplayNameERKNS_6LocaleERNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_678CollatorC2Ev
	.type	_ZN6icu_678CollatorC2Ev, @function
_ZN6icu_678CollatorC2Ev:
.LFB3409:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_678CollatorE(%rip), %rax
	movq	%rax, (%rdi)
	ret
	.cfi_endproc
.LFE3409:
	.size	_ZN6icu_678CollatorC2Ev, .-_ZN6icu_678CollatorC2Ev
	.globl	_ZN6icu_678CollatorC1Ev
	.set	_ZN6icu_678CollatorC1Ev,_ZN6icu_678CollatorC2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_678CollatorC2E18UColAttributeValue18UNormalizationMode
	.type	_ZN6icu_678CollatorC2E18UColAttributeValue18UNormalizationMode, @function
_ZN6icu_678CollatorC2E18UColAttributeValue18UNormalizationMode:
.LFB3412:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_678CollatorE(%rip), %rax
	movq	%rax, (%rdi)
	ret
	.cfi_endproc
.LFE3412:
	.size	_ZN6icu_678CollatorC2E18UColAttributeValue18UNormalizationMode, .-_ZN6icu_678CollatorC2E18UColAttributeValue18UNormalizationMode
	.globl	_ZN6icu_678CollatorC1E18UColAttributeValue18UNormalizationMode
	.set	_ZN6icu_678CollatorC1E18UColAttributeValue18UNormalizationMode,_ZN6icu_678CollatorC2E18UColAttributeValue18UNormalizationMode
	.align 2
	.p2align 4
	.globl	_ZN6icu_678CollatorD2Ev
	.type	_ZN6icu_678CollatorD2Ev, @function
_ZN6icu_678CollatorD2Ev:
.LFB3415:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_678CollatorE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE3415:
	.size	_ZN6icu_678CollatorD2Ev, .-_ZN6icu_678CollatorD2Ev
	.globl	_ZN6icu_678CollatorD1Ev
	.set	_ZN6icu_678CollatorD1Ev,_ZN6icu_678CollatorD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_678CollatorD0Ev
	.type	_ZN6icu_678CollatorD0Ev, @function
_ZN6icu_678CollatorD0Ev:
.LFB3417:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_678CollatorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3417:
	.size	_ZN6icu_678CollatorD0Ev, .-_ZN6icu_678CollatorD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_678CollatorC2ERKS0_
	.type	_ZN6icu_678CollatorC2ERKS0_, @function
_ZN6icu_678CollatorC2ERKS0_:
.LFB3419:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_678CollatorE(%rip), %rax
	movq	%rax, (%rdi)
	ret
	.cfi_endproc
.LFE3419:
	.size	_ZN6icu_678CollatorC2ERKS0_, .-_ZN6icu_678CollatorC2ERKS0_
	.globl	_ZN6icu_678CollatorC1ERKS0_
	.set	_ZN6icu_678CollatorC1ERKS0_,_ZN6icu_678CollatorC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Collator8getBoundEPKhi13UColBoundModejPhiR10UErrorCode
	.type	_ZN6icu_678Collator8getBoundEPKhi13UColBoundModejPhiR10UErrorCode, @function
_ZN6icu_678Collator8getBoundEPKhi13UColBoundModejPhiR10UErrorCode:
.LFB3423:
	.cfi_startproc
	endbr64
	jmp	ucol_getBound_67@PLT
	.cfi_endproc
.LFE3423:
	.size	_ZN6icu_678Collator8getBoundEPKhi13UColBoundModejPhiR10UErrorCode, .-_ZN6icu_678Collator8getBoundEPKhi13UColBoundModejPhiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6730CollationLocaleListEnumeration16getStaticClassIDEv
	.type	_ZN6icu_6730CollationLocaleListEnumeration16getStaticClassIDEv, @function
_ZN6icu_6730CollationLocaleListEnumeration16getStaticClassIDEv:
.LFB3438:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6730CollationLocaleListEnumeration16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3438:
	.size	_ZN6icu_6730CollationLocaleListEnumeration16getStaticClassIDEv, .-_ZN6icu_6730CollationLocaleListEnumeration16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Collator11getKeywordsER10UErrorCode
	.type	_ZN6icu_678Collator11getKeywordsER10UErrorCode, @function
_ZN6icu_678Collator11getKeywordsER10UErrorCode:
.LFB3441:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	ucol_getKeywords_67@PLT
	addq	$8, %rsp
	movq	%r12, %rsi
	movq	%rax, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6718UStringEnumeration16fromUEnumerationEP12UEnumerationR10UErrorCode@PLT
	.cfi_endproc
.LFE3441:
	.size	_ZN6icu_678Collator11getKeywordsER10UErrorCode, .-_ZN6icu_678Collator11getKeywordsER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Collator16getKeywordValuesEPKcR10UErrorCode
	.type	_ZN6icu_678Collator16getKeywordValuesEPKcR10UErrorCode, @function
_ZN6icu_678Collator16getKeywordValuesEPKcR10UErrorCode:
.LFB3442:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$8, %rsp
	call	ucol_getKeywordValues_67@PLT
	addq	$8, %rsp
	movq	%r12, %rsi
	movq	%rax, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6718UStringEnumeration16fromUEnumerationEP12UEnumerationR10UErrorCode@PLT
	.cfi_endproc
.LFE3442:
	.size	_ZN6icu_678Collator16getKeywordValuesEPKcR10UErrorCode, .-_ZN6icu_678Collator16getKeywordValuesEPKcR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Collator25getKeywordValuesForLocaleEPKcRKNS_6LocaleEaR10UErrorCode
	.type	_ZN6icu_678Collator25getKeywordValuesForLocaleEPKcRKNS_6LocaleEaR10UErrorCode, @function
_ZN6icu_678Collator25getKeywordValuesForLocaleEPKcRKNS_6LocaleEaR10UErrorCode:
.LFB3443:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movsbl	%dl, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rcx, %r12
	subq	$8, %rsp
	movq	40(%rsi), %rsi
	call	ucol_getKeywordValuesForLocale_67@PLT
	addq	$8, %rsp
	movq	%r12, %rsi
	movq	%rax, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6718UStringEnumeration16fromUEnumerationEP12UEnumerationR10UErrorCode@PLT
	.cfi_endproc
.LFE3443:
	.size	_ZN6icu_678Collator25getKeywordValuesForLocaleEPKcRKNS_6LocaleEaR10UErrorCode, .-_ZN6icu_678Collator25getKeywordValuesForLocaleEPKcRKNS_6LocaleEaR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Collator23getFunctionalEquivalentEPKcRKNS_6LocaleERaR10UErrorCode
	.type	_ZN6icu_678Collator23getFunctionalEquivalentEPKcRKNS_6LocaleERaR10UErrorCode, @function
_ZN6icu_678Collator23getFunctionalEquivalentEPKcRKNS_6LocaleERaR10UErrorCode:
.LFB3444:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r11
	movq	%r8, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-208(%rbp), %r13
	movq	%rdi, %r12
	pushq	%rbx
	movq	%r13, %rdi
	.cfi_offset 3, -40
	movq	%r8, %rbx
	movq	%r11, %r8
	subq	$184, %rsp
	movq	40(%rdx), %rcx
	movq	%rsi, %rdx
	movl	$157, %esi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	ucol_getFunctionalEquivalent_67@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L270
	movb	$0, -208(%rbp)
.L270:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_676Locale14createFromNameEPKc@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L273
	addq	$184, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L273:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3444:
	.size	_ZN6icu_678Collator23getFunctionalEquivalentEPKcRKNS_6LocaleERaR10UErrorCode, .-_ZN6icu_678Collator23getFunctionalEquivalentEPKcRKNS_6LocaleERaR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Collator25getEquivalentReorderCodesEiPiiR10UErrorCode
	.type	_ZN6icu_678Collator25getEquivalentReorderCodesEiPiiR10UErrorCode, @function
_ZN6icu_678Collator25getEquivalentReorderCodesEiPiiR10UErrorCode:
.LFB3451:
	.cfi_startproc
	endbr64
	movl	(%rcx), %r8d
	testl	%r8d, %r8d
	jg	.L287
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rcx, %rbx
	testl	%edx, %edx
	js	.L277
	movl	%edi, %r14d
	movq	%rsi, %r13
	testq	%rsi, %rsi
	jne	.L278
	testl	%edx, %edx
	jle	.L278
.L277:
	movl	$1, (%rbx)
.L274:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L287:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L278:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movq	%rbx, %rdi
	call	_ZN6icu_6713CollationRoot7getDataER10UErrorCode@PLT
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L274
	movq	%rbx, %r8
	movl	%r12d, %ecx
	popq	%rbx
	.cfi_restore 3
	movq	%r13, %rdx
	popq	%r12
	.cfi_restore 12
	movl	%r14d, %esi
	popq	%r13
	.cfi_restore 13
	movq	%rax, %rdi
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_6713CollationData20getEquivalentScriptsEiPiiR10UErrorCode@PLT
	.cfi_endproc
.LFE3451:
	.size	_ZN6icu_678Collator25getEquivalentReorderCodesEiPiiR10UErrorCode, .-_ZN6icu_678Collator25getEquivalentReorderCodesEiPiiR10UErrorCode
	.section	.rodata.str1.1
.LC4:
	.string	"res_index"
.LC5:
	.string	"icudt67l-coll"
.LC6:
	.string	"InstalledLocales"
	.text
	.p2align 4
	.type	_ZN6icu_67L23initAvailableLocaleListER10UErrorCode, @function
_ZN6icu_67L23initAvailableLocaleListER10UErrorCode:
.LFB3389:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-496(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	subq	$488, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6720StackUResourceBundleC1Ev@PLT
	movq	%r12, %rdx
	leaq	.LC4(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	call	ures_openDirect_67@PLT
	movq	%r12, %rcx
	movq	%r13, %rdx
	leaq	.LC6(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r14
	call	ures_getByKey_67@PLT
	movl	(%r12), %eax
	testl	%eax, %eax
	jle	.L308
.L297:
	movq	%r14, %rdi
	call	ures_close_67@PLT
	leaq	collator_cleanup(%rip), %rsi
	movl	$27, %edi
	call	ucln_i18n_registerCleanup_67@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6720StackUResourceBundleD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L309
	addq	$488, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L308:
	.cfi_restore_state
	movq	%r13, %rdi
	call	ures_getSize_67@PLT
	movq	$-1, %rdi
	movslq	%eax, %rbx
	movabsq	$41175768021673106, %rax
	movl	%ebx, _ZL24availableLocaleListCount(%rip)
	cmpq	%rax, %rbx
	jbe	.L310
.L292:
	call	_ZN6icu_677UMemorynaEm@PLT
	testq	%rax, %rax
	je	.L293
	leaq	8(%rax), %r15
	movq	%rbx, (%rax)
	movq	%r15, -520(%rbp)
	subq	$1, %rbx
	js	.L294
	.p2align 4,,10
	.p2align 3
.L295:
	movq	%r15, %rdi
	subq	$1, %rbx
	addq	$224, %r15
	call	_ZN6icu_676LocaleC1Ev@PLT
	cmpq	$-1, %rbx
	jne	.L295
.L294:
	movq	-520(%rbp), %rax
	movq	%r13, %rdi
	xorl	%ebx, %ebx
	movq	%rax, _ZL19availableLocaleList(%rip)
	call	ures_resetIterator_67@PLT
	leaq	-504(%rbp), %rax
	movq	%rax, -520(%rbp)
	jmp	.L296
	.p2align 4,,10
	.p2align 3
.L311:
	movq	-520(%rbp), %rdx
	movq	%r12, %rcx
	xorl	%esi, %esi
	movq	%r13, %rdi
	leaq	-288(%rbp), %r15
	movq	$0, -504(%rbp)
	call	ures_getNextString_67@PLT
	movq	-504(%rbp), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	movq	_ZL19availableLocaleList(%rip), %rdi
	movq	%r15, %rsi
	addq	%rbx, %rdi
	addq	$224, %rbx
	call	_ZN6icu_676LocaleaSEOS0_@PLT
	movq	%r15, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
.L296:
	movq	%r13, %rdi
	call	ures_hasNext_67@PLT
	testb	%al, %al
	jne	.L311
	jmp	.L297
	.p2align 4,,10
	.p2align 3
.L310:
	leaq	0(,%rbx,8), %rdi
	subq	%rbx, %rdi
	salq	$5, %rdi
	addq	$8, %rdi
	jmp	.L292
.L309:
	call	__stack_chk_fail@PLT
.L293:
	movq	$0, _ZL19availableLocaleList(%rip)
	jmp	.L297
	.cfi_endproc
.LFE3389:
	.size	_ZN6icu_67L23initAvailableLocaleListER10UErrorCode, .-_ZN6icu_67L23initAvailableLocaleListER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Collator19getAvailableLocalesERi
	.type	_ZN6icu_678Collator19getAvailableLocalesERi, @function
_ZN6icu_678Collator19getAvailableLocalesERi:
.LFB3405:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	$0, -28(%rbp)
	movl	$0, (%rdi)
	movl	_ZL28gAvailableLocaleListInitOnce(%rip), %eax
	cmpl	$2, %eax
	jne	.L325
.L313:
	movl	4+_ZL28gAvailableLocaleListInitOnce(%rip), %edx
	testl	%edx, %edx
	jle	.L314
.L316:
	xorl	%eax, %eax
	jmp	.L312
	.p2align 4,,10
	.p2align 3
.L325:
	leaq	_ZL28gAvailableLocaleListInitOnce(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L313
	leaq	-28(%rbp), %rdi
	call	_ZN6icu_67L23initAvailableLocaleListER10UErrorCode
	movl	-28(%rbp), %eax
	leaq	_ZL28gAvailableLocaleListInitOnce(%rip), %rdi
	movl	%eax, 4+_ZL28gAvailableLocaleListInitOnce(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L314:
	movl	-28(%rbp), %eax
	testl	%eax, %eax
	jg	.L316
	movl	_ZL24availableLocaleListCount(%rip), %edx
	movq	_ZL19availableLocaleList(%rip), %rax
	movl	%edx, (%rbx)
.L312:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L326
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L326:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3405:
	.size	_ZN6icu_678Collator19getAvailableLocalesERi, .-_ZN6icu_678Collator19getAvailableLocalesERi
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Collator19getAvailableLocalesEv
	.type	_ZN6icu_678Collator19getAvailableLocalesEv, @function
_ZN6icu_678Collator19getAvailableLocalesEv:
.LFB3440:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	$0, -28(%rbp)
	movl	_ZL28gAvailableLocaleListInitOnce(%rip), %eax
	cmpl	$2, %eax
	jne	.L343
.L328:
	movl	4+_ZL28gAvailableLocaleListInitOnce(%rip), %edx
	testl	%edx, %edx
	jle	.L329
.L331:
	xorl	%r12d, %r12d
.L327:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L344
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L343:
	.cfi_restore_state
	leaq	_ZL28gAvailableLocaleListInitOnce(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L328
	leaq	-28(%rbp), %rdi
	call	_ZN6icu_67L23initAvailableLocaleListER10UErrorCode
	movl	-28(%rbp), %eax
	leaq	_ZL28gAvailableLocaleListInitOnce(%rip), %rdi
	movl	%eax, 4+_ZL28gAvailableLocaleListInitOnce(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L329:
	movl	-28(%rbp), %eax
	testl	%eax, %eax
	jg	.L331
	movl	$120, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L327
	movq	%rax, %rdi
	call	_ZN6icu_6717StringEnumerationC2Ev@PLT
	leaq	16+_ZTVN6icu_6730CollationLocaleListEnumerationE(%rip), %rax
	movl	$0, 116(%r12)
	movq	%rax, (%r12)
	jmp	.L327
.L344:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3440:
	.size	_ZN6icu_678Collator19getAvailableLocalesEv, .-_ZN6icu_678Collator19getAvailableLocalesEv
	.weak	_ZTSN6icu_678CollatorE
	.section	.rodata._ZTSN6icu_678CollatorE,"aG",@progbits,_ZTSN6icu_678CollatorE,comdat
	.align 16
	.type	_ZTSN6icu_678CollatorE, @object
	.size	_ZTSN6icu_678CollatorE, 19
_ZTSN6icu_678CollatorE:
	.string	"N6icu_678CollatorE"
	.weak	_ZTIN6icu_678CollatorE
	.section	.data.rel.ro._ZTIN6icu_678CollatorE,"awG",@progbits,_ZTIN6icu_678CollatorE,comdat
	.align 8
	.type	_ZTIN6icu_678CollatorE, @object
	.size	_ZTIN6icu_678CollatorE, 24
_ZTIN6icu_678CollatorE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_678CollatorE
	.quad	_ZTIN6icu_677UObjectE
	.weak	_ZTSN6icu_6730CollationLocaleListEnumerationE
	.section	.rodata._ZTSN6icu_6730CollationLocaleListEnumerationE,"aG",@progbits,_ZTSN6icu_6730CollationLocaleListEnumerationE,comdat
	.align 32
	.type	_ZTSN6icu_6730CollationLocaleListEnumerationE, @object
	.size	_ZTSN6icu_6730CollationLocaleListEnumerationE, 42
_ZTSN6icu_6730CollationLocaleListEnumerationE:
	.string	"N6icu_6730CollationLocaleListEnumerationE"
	.weak	_ZTIN6icu_6730CollationLocaleListEnumerationE
	.section	.data.rel.ro._ZTIN6icu_6730CollationLocaleListEnumerationE,"awG",@progbits,_ZTIN6icu_6730CollationLocaleListEnumerationE,comdat
	.align 8
	.type	_ZTIN6icu_6730CollationLocaleListEnumerationE, @object
	.size	_ZTIN6icu_6730CollationLocaleListEnumerationE, 24
_ZTIN6icu_6730CollationLocaleListEnumerationE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6730CollationLocaleListEnumerationE
	.quad	_ZTIN6icu_6717StringEnumerationE
	.weak	_ZTVN6icu_678CollatorE
	.section	.data.rel.ro._ZTVN6icu_678CollatorE,"awG",@progbits,_ZTVN6icu_678CollatorE,comdat
	.align 8
	.type	_ZTVN6icu_678CollatorE, @object
	.size	_ZTVN6icu_678CollatorE, 328
_ZTVN6icu_678CollatorE:
	.quad	0
	.quad	_ZTIN6icu_678CollatorE
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	_ZNK6icu_678CollatoreqERKS0_
	.quad	_ZNK6icu_678CollatorneERKS0_
	.quad	__cxa_pure_virtual
	.quad	_ZNK6icu_678Collator7compareERKNS_13UnicodeStringES3_
	.quad	__cxa_pure_virtual
	.quad	_ZNK6icu_678Collator7compareERKNS_13UnicodeStringES3_i
	.quad	__cxa_pure_virtual
	.quad	_ZNK6icu_678Collator7compareEPKDsiS2_i
	.quad	__cxa_pure_virtual
	.quad	_ZNK6icu_678Collator7compareER13UCharIteratorS2_R10UErrorCode
	.quad	_ZNK6icu_678Collator11compareUTF8ERKNS_11StringPieceES3_R10UErrorCode
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZNK6icu_678Collator11getStrengthEv
	.quad	_ZN6icu_678Collator11setStrengthENS0_18ECollationStrengthE
	.quad	_ZNK6icu_678Collator15getReorderCodesEPiiR10UErrorCode
	.quad	_ZN6icu_678Collator15setReorderCodesEPKiiR10UErrorCode
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN6icu_678Collator14setMaxVariableE15UColReorderCodeR10UErrorCode
	.quad	_ZNK6icu_678Collator14getMaxVariableEv
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZNK6icu_678Collator14getTailoredSetER10UErrorCode
	.quad	_ZNK6icu_678Collator9safeCloneEv
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN6icu_678Collator10setLocalesERKNS_6LocaleES3_S3_
	.quad	_ZNK6icu_678Collator32internalGetShortDefinitionStringEPKcPciR10UErrorCode
	.quad	_ZNK6icu_678Collator19internalCompareUTF8EPKciS2_iR10UErrorCode
	.quad	_ZNK6icu_678Collator23internalNextSortKeyPartEP13UCharIteratorPjPhiR10UErrorCode
	.weak	_ZTVN6icu_6730CollationLocaleListEnumerationE
	.section	.data.rel.ro._ZTVN6icu_6730CollationLocaleListEnumerationE,"awG",@progbits,_ZTVN6icu_6730CollationLocaleListEnumerationE,comdat
	.align 8
	.type	_ZTVN6icu_6730CollationLocaleListEnumerationE, @object
	.size	_ZTVN6icu_6730CollationLocaleListEnumerationE, 104
_ZTVN6icu_6730CollationLocaleListEnumerationE:
	.quad	0
	.quad	_ZTIN6icu_6730CollationLocaleListEnumerationE
	.quad	_ZN6icu_6730CollationLocaleListEnumerationD1Ev
	.quad	_ZN6icu_6730CollationLocaleListEnumerationD0Ev
	.quad	_ZNK6icu_6730CollationLocaleListEnumeration17getDynamicClassIDEv
	.quad	_ZNK6icu_6730CollationLocaleListEnumeration5cloneEv
	.quad	_ZNK6icu_6730CollationLocaleListEnumeration5countER10UErrorCode
	.quad	_ZN6icu_6730CollationLocaleListEnumeration4nextEPiR10UErrorCode
	.quad	_ZN6icu_6717StringEnumeration5unextEPiR10UErrorCode
	.quad	_ZN6icu_6730CollationLocaleListEnumeration5snextER10UErrorCode
	.quad	_ZN6icu_6730CollationLocaleListEnumeration5resetER10UErrorCode
	.quad	_ZNK6icu_6717StringEnumerationeqERKS0_
	.quad	_ZNK6icu_6717StringEnumerationneERKS0_
	.local	_ZZN6icu_6730CollationLocaleListEnumeration16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6730CollationLocaleListEnumeration16getStaticClassIDEvE7classID,1,1
	.section	.rodata.str1.1
.LC7:
	.string	"space"
.LC8:
	.string	"punct"
.LC9:
	.string	"symbol"
.LC10:
	.string	"currency"
.LC11:
	.string	"digit"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	_ZN6icu_6712_GLOBAL__N_1L16collReorderCodesE, @object
	.size	_ZN6icu_6712_GLOBAL__N_1L16collReorderCodesE, 40
_ZN6icu_6712_GLOBAL__N_1L16collReorderCodesE:
	.quad	.LC7
	.quad	.LC8
	.quad	.LC9
	.quad	.LC10
	.quad	.LC11
	.section	.rodata.str1.1
.LC12:
	.string	"primary"
.LC13:
	.string	"secondary"
.LC14:
	.string	"tertiary"
.LC15:
	.string	"quaternary"
.LC16:
	.string	"identical"
.LC17:
	.string	"no"
.LC18:
	.string	"yes"
.LC19:
	.string	"shifted"
.LC20:
	.string	"non-ignorable"
.LC21:
	.string	"lower"
.LC22:
	.string	"upper"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZN6icu_6712_GLOBAL__N_1L19collAttributeValuesE, @object
	.size	_ZN6icu_6712_GLOBAL__N_1L19collAttributeValuesE, 176
_ZN6icu_6712_GLOBAL__N_1L19collAttributeValuesE:
	.quad	.LC12
	.long	0
	.zero	4
	.quad	.LC13
	.long	1
	.zero	4
	.quad	.LC14
	.long	2
	.zero	4
	.quad	.LC15
	.long	3
	.zero	4
	.quad	.LC16
	.long	15
	.zero	4
	.quad	.LC17
	.long	16
	.zero	4
	.quad	.LC18
	.long	17
	.zero	4
	.quad	.LC19
	.long	20
	.zero	4
	.quad	.LC20
	.long	21
	.zero	4
	.quad	.LC21
	.long	24
	.zero	4
	.quad	.LC22
	.long	25
	.zero	4
	.section	.rodata.str1.1
.LC23:
	.string	"colStrength"
.LC24:
	.string	"colBackwards"
.LC25:
	.string	"colCaseLevel"
.LC26:
	.string	"colCaseFirst"
.LC27:
	.string	"colAlternate"
.LC28:
	.string	"colNormalization"
.LC29:
	.string	"colNumeric"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZN6icu_6712_GLOBAL__N_1L14collAttributesE, @object
	.size	_ZN6icu_6712_GLOBAL__N_1L14collAttributesE, 112
_ZN6icu_6712_GLOBAL__N_1L14collAttributesE:
	.quad	.LC23
	.long	5
	.zero	4
	.quad	.LC24
	.long	0
	.zero	4
	.quad	.LC25
	.long	3
	.zero	4
	.quad	.LC26
	.long	2
	.zero	4
	.quad	.LC27
	.long	1
	.zero	4
	.quad	.LC28
	.long	4
	.zero	4
	.quad	.LC29
	.long	7
	.zero	4
	.local	_ZL28gAvailableLocaleListInitOnce
	.comm	_ZL28gAvailableLocaleListInitOnce,8,8
	.local	_ZL24availableLocaleListCount
	.comm	_ZL24availableLocaleListCount,4,4
	.local	_ZL19availableLocaleList
	.comm	_ZL19availableLocaleList,8,8
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
