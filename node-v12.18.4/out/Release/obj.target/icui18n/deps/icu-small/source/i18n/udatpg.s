	.file	"udatpg.cpp"
	.text
	.p2align 4
	.globl	udatpg_openEmpty_67
	.type	udatpg_openEmpty_67, @function
udatpg_openEmpty_67:
.LFB2469:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_6724DateTimePatternGenerator19createEmptyInstanceER10UErrorCode@PLT
	.cfi_endproc
.LFE2469:
	.size	udatpg_openEmpty_67, .-udatpg_openEmpty_67
	.p2align 4
	.globl	udatpg_close_67
	.type	udatpg_close_67, @function
udatpg_close_67:
.LFB2470:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L3
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L3:
	ret
	.cfi_endproc
.LFE2470:
	.size	udatpg_close_67, .-udatpg_close_67
	.p2align 4
	.globl	udatpg_clone_67
	.type	udatpg_clone_67, @function
udatpg_clone_67:
.LFB2471:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L6
	jmp	_ZNK6icu_6724DateTimePatternGenerator5cloneEv@PLT
	.p2align 4,,10
	.p2align 3
.L6:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2471:
	.size	udatpg_clone_67, .-udatpg_clone_67
	.p2align 4
	.globl	udatpg_getBestPattern_67
	.type	udatpg_getBestPattern_67, @function
udatpg_getBestPattern_67:
.LFB2472:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$200, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r9), %r10d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r10d, %r10d
	jg	.L7
	movq	%rdi, %r14
	movq	%rcx, %rbx
	movl	%r8d, %r15d
	movq	%r9, %r12
	testq	%rsi, %rsi
	jne	.L9
	testl	%edx, %edx
	je	.L9
	movl	$1, (%r9)
.L7:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L19
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore_state
	leaq	-192(%rbp), %r9
	movq	%rsi, -200(%rbp)
	leaq	-200(%rbp), %r13
	movl	%edx, %esi
	movl	%edx, %ecx
	shrl	$31, %esi
	movq	%r13, %rdx
	movq	%r9, %rdi
	movq	%r9, -216(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	-216(%rbp), %r9
	leaq	-128(%rbp), %rdi
	movq	%r12, %r8
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	%rdi, -216(%rbp)
	movq	%r9, %rdx
	movq	%r9, -232(%rbp)
	call	_ZN6icu_6724DateTimePatternGenerator14getBestPatternERKNS_13UnicodeStringE28UDateTimePatternMatchOptionsR10UErrorCode@PLT
	movq	%r12, %rcx
	movl	%r15d, %edx
	movq	%r13, %rsi
	movq	-216(%rbp), %rdi
	movq	%rbx, -200(%rbp)
	movq	%rdi, -224(%rbp)
	call	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode@PLT
	movl	%eax, -216(%rbp)
	movq	-224(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-232(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-216(%rbp), %eax
	jmp	.L7
.L19:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2472:
	.size	udatpg_getBestPattern_67, .-udatpg_getBestPattern_67
	.p2align 4
	.globl	udatpg_getBestPatternWithOptions_67
	.type	udatpg_getBestPatternWithOptions_67, @function
udatpg_getBestPatternWithOptions_67:
.LFB2473:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$200, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%r12), %r10d
	testl	%r10d, %r10d
	jg	.L20
	movq	%rdi, %r14
	movl	%ecx, %r15d
	movq	%r8, %rbx
	testq	%rsi, %rsi
	jne	.L22
	testl	%edx, %edx
	je	.L22
	movl	$1, (%r12)
.L20:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L32
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	.cfi_restore_state
	leaq	-192(%rbp), %r11
	movq	%rsi, -200(%rbp)
	movl	%edx, %esi
	movl	%edx, %ecx
	leaq	-200(%rbp), %r13
	shrl	$31, %esi
	movq	%r11, %rdi
	movl	%r9d, -224(%rbp)
	movq	%r13, %rdx
	movq	%r11, -216(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	-216(%rbp), %r11
	movq	%r12, %r8
	movl	%r15d, %ecx
	movq	%r14, %rsi
	leaq	-128(%rbp), %rdi
	movq	%r11, %rdx
	movq	%r11, -232(%rbp)
	movq	%rdi, -216(%rbp)
	call	_ZN6icu_6724DateTimePatternGenerator14getBestPatternERKNS_13UnicodeStringE28UDateTimePatternMatchOptionsR10UErrorCode@PLT
	movl	-224(%rbp), %r9d
	movq	%r12, %rcx
	movq	%r13, %rsi
	movq	-216(%rbp), %rdi
	movq	%rbx, -200(%rbp)
	movl	%r9d, %edx
	movq	%rdi, -224(%rbp)
	call	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode@PLT
	movl	%eax, -216(%rbp)
	movq	-224(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-232(%rbp), %r11
	movq	%r11, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-216(%rbp), %eax
	jmp	.L20
.L32:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2473:
	.size	udatpg_getBestPatternWithOptions_67, .-udatpg_getBestPatternWithOptions_67
	.p2align 4
	.globl	udatpg_getSkeleton_67
	.type	udatpg_getSkeleton_67, @function
udatpg_getSkeleton_67:
.LFB2474:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r9), %edi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edi, %edi
	jg	.L33
	movq	%rcx, %rbx
	movl	%r8d, %r14d
	movq	%r9, %r12
	testq	%rsi, %rsi
	jne	.L35
	testl	%edx, %edx
	je	.L35
	movl	$1, (%r9)
.L33:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L45
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore_state
	movq	%rsi, -200(%rbp)
	leaq	-200(%rbp), %r13
	movl	%edx, %esi
	movl	%edx, %ecx
	leaq	-192(%rbp), %r15
	shrl	$31, %esi
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	leaq	-128(%rbp), %rdi
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%rdi, -216(%rbp)
	call	_ZN6icu_6724DateTimePatternGenerator17staticGetSkeletonERKNS_13UnicodeStringER10UErrorCode@PLT
	movq	%r12, %rcx
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	-216(%rbp), %rdi
	movq	%rbx, -200(%rbp)
	movq	%rdi, -224(%rbp)
	call	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode@PLT
	movl	%eax, -216(%rbp)
	movq	-224(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-216(%rbp), %eax
	jmp	.L33
.L45:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2474:
	.size	udatpg_getSkeleton_67, .-udatpg_getSkeleton_67
	.p2align 4
	.globl	udatpg_getBaseSkeleton_67
	.type	udatpg_getBaseSkeleton_67, @function
udatpg_getBaseSkeleton_67:
.LFB2475:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r9), %edi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edi, %edi
	jg	.L46
	movq	%rcx, %rbx
	movl	%r8d, %r14d
	movq	%r9, %r12
	testq	%rsi, %rsi
	jne	.L48
	testl	%edx, %edx
	je	.L48
	movl	$1, (%r9)
.L46:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L58
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore_state
	movq	%rsi, -200(%rbp)
	leaq	-200(%rbp), %r13
	movl	%edx, %esi
	movl	%edx, %ecx
	leaq	-192(%rbp), %r15
	shrl	$31, %esi
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	leaq	-128(%rbp), %rdi
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%rdi, -216(%rbp)
	call	_ZN6icu_6724DateTimePatternGenerator21staticGetBaseSkeletonERKNS_13UnicodeStringER10UErrorCode@PLT
	movq	%r12, %rcx
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	-216(%rbp), %rdi
	movq	%rbx, -200(%rbp)
	movq	%rdi, -224(%rbp)
	call	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode@PLT
	movl	%eax, -216(%rbp)
	movq	-224(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-216(%rbp), %eax
	jmp	.L46
.L58:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2475:
	.size	udatpg_getBaseSkeleton_67, .-udatpg_getBaseSkeleton_67
	.p2align 4
	.globl	udatpg_addPattern_67
	.type	udatpg_addPattern_67, @function
udatpg_addPattern_67:
.LFB2476:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%r8, %r10
	xorl	%r8d, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%r9d, %r12d
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 3, -56
	movq	24(%rbp), %r15
	movq	16(%rbp), %r9
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%r15), %r11d
	testl	%r11d, %r11d
	jg	.L59
	movq	%rdi, %r14
	movl	%ecx, %ebx
	testq	%rsi, %rsi
	jne	.L61
	testl	%edx, %edx
	je	.L61
	movl	$1, (%r15)
.L59:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L75
	addq	$216, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L61:
	.cfi_restore_state
	leaq	-200(%rbp), %rax
	movq	%rsi, -200(%rbp)
	movl	%edx, %esi
	movl	%edx, %ecx
	leaq	-192(%rbp), %r13
	shrl	$31, %esi
	movq	%rax, %rdx
	movq	%r9, -248(%rbp)
	movq	%r13, %rdi
	movq	%r10, -240(%rbp)
	movq	%rax, -216(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movsbl	%bl, %edx
	movq	%r15, %r8
	movq	%r13, %rsi
	leaq	-128(%rbp), %r11
	movq	%rax, -128(%rbp)
	movq	%r14, %rdi
	movl	$2, %eax
	movq	%r11, %rcx
	movq	%r11, -232(%rbp)
	movw	%ax, -120(%rbp)
	call	_ZN6icu_6724DateTimePatternGenerator10addPatternERKNS_13UnicodeStringEaRS1_R10UErrorCode@PLT
	movq	-232(%rbp), %r11
	movq	%r15, %rcx
	movl	%r12d, %edx
	movq	-240(%rbp), %r10
	movq	-216(%rbp), %rsi
	movl	%eax, -220(%rbp)
	movq	%r11, %rdi
	movq	%r11, -216(%rbp)
	movq	%r10, -200(%rbp)
	call	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode@PLT
	movq	-248(%rbp), %r9
	movq	-216(%rbp), %r11
	movl	-220(%rbp), %r8d
	testq	%r9, %r9
	je	.L62
	movl	%eax, (%r9)
.L62:
	movq	%r11, %rdi
	movl	%r8d, -216(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-216(%rbp), %r8d
	jmp	.L59
.L75:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2476:
	.size	udatpg_addPattern_67, .-udatpg_addPattern_67
	.p2align 4
	.globl	udatpg_setAppendItemFormat_67
	.type	udatpg_setAppendItemFormat_67, @function
udatpg_setAppendItemFormat_67:
.LFB2477:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%esi, %r14d
	movl	%ecx, %esi
	pushq	%r13
	shrl	$31, %esi
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-112(%rbp), %r12
	movq	%r12, %rdi
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rdx, -120(%rbp)
	leaq	-120(%rbp), %rdx
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	%r12, %rdx
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6724DateTimePatternGenerator19setAppendItemFormatE21UDateTimePatternFieldRKNS_13UnicodeStringE@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L79
	addq	$104, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L79:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2477:
	.size	udatpg_setAppendItemFormat_67, .-udatpg_setAppendItemFormat_67
	.p2align 4
	.globl	udatpg_getAppendItemFormat_67
	.type	udatpg_getAppendItemFormat_67, @function
udatpg_getAppendItemFormat_67:
.LFB2478:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdx, %rbx
	subq	$8, %rsp
	call	_ZNK6icu_6724DateTimePatternGenerator19getAppendItemFormatE21UDateTimePatternField@PLT
	testq	%rbx, %rbx
	je	.L81
	movswl	8(%rax), %edx
	testw	%dx, %dx
	js	.L82
	sarl	$5, %edx
	movl	%edx, (%rbx)
.L81:
	movzwl	8(%rax), %edx
	testb	$17, %dl
	jne	.L86
	andl	$2, %edx
	jne	.L91
	movq	24(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L91:
	.cfi_restore_state
	addq	$8, %rsp
	addq	$10, %rax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L82:
	.cfi_restore_state
	movl	12(%rax), %edx
	movl	%edx, (%rbx)
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L86:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2478:
	.size	udatpg_getAppendItemFormat_67, .-udatpg_getAppendItemFormat_67
	.p2align 4
	.globl	udatpg_setAppendItemName_67
	.type	udatpg_setAppendItemName_67, @function
udatpg_setAppendItemName_67:
.LFB2479:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%esi, %r14d
	movl	%ecx, %esi
	pushq	%r13
	shrl	$31, %esi
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-112(%rbp), %r12
	movq	%r12, %rdi
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rdx, -120(%rbp)
	leaq	-120(%rbp), %rdx
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	%r12, %rdx
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6724DateTimePatternGenerator17setAppendItemNameE21UDateTimePatternFieldRKNS_13UnicodeStringE@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L95
	addq	$104, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L95:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2479:
	.size	udatpg_setAppendItemName_67, .-udatpg_setAppendItemName_67
	.p2align 4
	.globl	udatpg_getAppendItemName_67
	.type	udatpg_getAppendItemName_67, @function
udatpg_getAppendItemName_67:
.LFB2480:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdx, %rbx
	subq	$8, %rsp
	call	_ZNK6icu_6724DateTimePatternGenerator17getAppendItemNameE21UDateTimePatternField@PLT
	testq	%rbx, %rbx
	je	.L97
	movswl	8(%rax), %edx
	testw	%dx, %dx
	js	.L98
	sarl	$5, %edx
	movl	%edx, (%rbx)
.L97:
	movzwl	8(%rax), %edx
	testb	$17, %dl
	jne	.L102
	andl	$2, %edx
	jne	.L107
	movq	24(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L107:
	.cfi_restore_state
	addq	$8, %rsp
	addq	$10, %rax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L98:
	.cfi_restore_state
	movl	12(%rax), %edx
	movl	%edx, (%rbx)
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L102:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2480:
	.size	udatpg_getAppendItemName_67, .-udatpg_getAppendItemName_67
	.p2align 4
	.globl	udatpg_getFieldDisplayName_67
	.type	udatpg_getFieldDisplayName_67, @function
udatpg_getFieldDisplayName_67:
.LFB2481:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$96, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	(%r9), %eax
	testl	%eax, %eax
	jg	.L118
	movq	%rcx, %rbx
	movl	%r8d, %r13d
	movq	%r9, %r12
	testq	%rcx, %rcx
	je	.L121
	testl	%r8d, %r8d
	js	.L111
	leaq	-112(%rbp), %r14
	movl	%edx, %ecx
	movl	%esi, %edx
	movq	%rdi, %rsi
	movq	%r14, %rdi
	call	_ZNK6icu_6724DateTimePatternGenerator19getFieldDisplayNameE21UDateTimePatternField23UDateTimePGDisplayWidth@PLT
	movq	%r12, %rcx
	leaq	-120(%rbp), %rsi
	movl	%r13d, %edx
	movq	%r14, %rdi
	movq	%rbx, -120(%rbp)
	call	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode@PLT
	movl	%eax, %r12d
.L114:
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L108:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L122
	addq	$96, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L121:
	.cfi_restore_state
	testl	%r8d, %r8d
	jne	.L111
	leaq	-112(%rbp), %r14
	movl	%edx, %ecx
	movl	%esi, %edx
	movq	%rdi, %rsi
	movq	%r14, %rdi
	call	_ZNK6icu_6724DateTimePatternGenerator19getFieldDisplayNameE21UDateTimePatternField23UDateTimePGDisplayWidth@PLT
	movswl	-104(%rbp), %r12d
	movl	%r12d, %eax
	sarl	$5, %r12d
	testw	%ax, %ax
	cmovs	-100(%rbp), %r12d
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L111:
	movl	$1, (%r12)
	movl	$-1, %r12d
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L118:
	movl	$-1, %r12d
	jmp	.L108
.L122:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2481:
	.size	udatpg_getFieldDisplayName_67, .-udatpg_getFieldDisplayName_67
	.p2align 4
	.globl	udatpg_setDateTimeFormat_67
	.type	udatpg_setDateTimeFormat_67, @function
udatpg_setDateTimeFormat_67:
.LFB2482:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-96(%rbp), %r12
	leaq	-104(%rbp), %rdx
	movq	%r12, %rdi
	subq	$96, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	%rsi, -104(%rbp)
	movl	%ecx, %esi
	shrl	$31, %esi
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6724DateTimePatternGenerator17setDateTimeFormatERKNS_13UnicodeStringE@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L126
	addq	$96, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L126:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2482:
	.size	udatpg_setDateTimeFormat_67, .-udatpg_setDateTimeFormat_67
	.p2align 4
	.globl	udatpg_getDateTimeFormat_67
	.type	udatpg_getDateTimeFormat_67, @function
udatpg_getDateTimeFormat_67:
.LFB2483:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	_ZNK6icu_6724DateTimePatternGenerator17getDateTimeFormatEv@PLT
	testq	%rbx, %rbx
	je	.L128
	movswl	8(%rax), %edx
	testw	%dx, %dx
	js	.L129
	sarl	$5, %edx
	movl	%edx, (%rbx)
.L128:
	movzwl	8(%rax), %edx
	testb	$17, %dl
	jne	.L133
	andl	$2, %edx
	jne	.L138
	movq	24(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L138:
	.cfi_restore_state
	addq	$8, %rsp
	addq	$10, %rax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L129:
	.cfi_restore_state
	movl	12(%rax), %edx
	movl	%edx, (%rbx)
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L133:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2483:
	.size	udatpg_getDateTimeFormat_67, .-udatpg_getDateTimeFormat_67
	.p2align 4
	.globl	udatpg_setDecimal_67
	.type	udatpg_setDecimal_67, @function
udatpg_setDecimal_67:
.LFB2484:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-96(%rbp), %r12
	leaq	-104(%rbp), %rdx
	movq	%r12, %rdi
	subq	$96, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	%rsi, -104(%rbp)
	movl	%ecx, %esi
	shrl	$31, %esi
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6724DateTimePatternGenerator10setDecimalERKNS_13UnicodeStringE@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L142
	addq	$96, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L142:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2484:
	.size	udatpg_setDecimal_67, .-udatpg_setDecimal_67
	.p2align 4
	.globl	udatpg_getDecimal_67
	.type	udatpg_getDecimal_67, @function
udatpg_getDecimal_67:
.LFB2485:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	_ZNK6icu_6724DateTimePatternGenerator10getDecimalEv@PLT
	testq	%rbx, %rbx
	je	.L144
	movswl	8(%rax), %edx
	testw	%dx, %dx
	js	.L145
	sarl	$5, %edx
	movl	%edx, (%rbx)
.L144:
	movzwl	8(%rax), %edx
	testb	$17, %dl
	jne	.L149
	andl	$2, %edx
	jne	.L154
	movq	24(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L154:
	.cfi_restore_state
	addq	$8, %rsp
	addq	$10, %rax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L145:
	.cfi_restore_state
	movl	12(%rax), %edx
	movl	%edx, (%rbx)
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L149:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2485:
	.size	udatpg_getDecimal_67, .-udatpg_getDecimal_67
	.p2align 4
	.globl	udatpg_replaceFieldTypes_67
	.type	udatpg_replaceFieldTypes_67, @function
udatpg_replaceFieldTypes_67:
.LFB2486:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$248, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	24(%rbp), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%r15), %r10d
	testl	%r10d, %r10d
	jg	.L155
	movq	%rdi, %r14
	movq	%rcx, %rbx
	movl	%r8d, %r12d
	movq	%r9, %r13
	testq	%rsi, %rsi
	jne	.L162
	testl	%edx, %edx
	je	.L162
.L157:
	movl	$1, (%r15)
	xorl	%eax, %eax
.L155:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L173
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L162:
	.cfi_restore_state
	testq	%rbx, %rbx
	jne	.L159
	testl	%r12d, %r12d
	jne	.L157
.L159:
	leaq	-264(%rbp), %r11
	leaq	-256(%rbp), %r10
	movq	%rsi, -264(%rbp)
	movl	%edx, %esi
	movl	%edx, %ecx
	shrl	$31, %esi
	movq	%r11, %rdx
	movq	%r10, %rdi
	movq	%r11, -280(%rbp)
	movq	%r10, -288(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	-280(%rbp), %r11
	movq	%rbx, -264(%rbp)
	movl	%r12d, %esi
	movl	%r12d, %ecx
	leaq	-192(%rbp), %rbx
	shrl	$31, %esi
	movq	%r11, %rdx
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	-288(%rbp), %r10
	movq	%r15, %r9
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	leaq	-128(%rbp), %r12
	movq	%r14, %rsi
	movq	%r10, %rdx
	movq	%r12, %rdi
	call	_ZN6icu_6724DateTimePatternGenerator17replaceFieldTypesERKNS_13UnicodeStringES3_28UDateTimePatternMatchOptionsR10UErrorCode@PLT
	movl	16(%rbp), %edx
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	-280(%rbp), %r11
	movq	%r13, -264(%rbp)
	movq	%r11, %rsi
	call	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode@PLT
	movl	%eax, -280(%rbp)
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-288(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-280(%rbp), %eax
	jmp	.L155
.L173:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2486:
	.size	udatpg_replaceFieldTypes_67, .-udatpg_replaceFieldTypes_67
	.p2align 4
	.globl	udatpg_replaceFieldTypesWithOptions_67
	.type	udatpg_replaceFieldTypesWithOptions_67, @function
udatpg_replaceFieldTypesWithOptions_67:
.LFB2487:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%r9d, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$264, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	32(%rbp), %r9
	movq	16(%rbp), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%r9), %r10d
	testl	%r10d, %r10d
	jg	.L174
	movq	%rdi, %r13
	movq	%rcx, %rbx
	movl	%r8d, %r12d
	testq	%rsi, %rsi
	jne	.L181
	testl	%edx, %edx
	je	.L181
.L176:
	movl	$1, (%r9)
	xorl	%eax, %eax
.L174:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L192
	addq	$264, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L181:
	.cfi_restore_state
	testq	%rbx, %rbx
	jne	.L178
	testl	%r12d, %r12d
	jne	.L176
.L178:
	leaq	-264(%rbp), %r11
	leaq	-256(%rbp), %r10
	movq	%rsi, -264(%rbp)
	movl	%edx, %esi
	movl	%edx, %ecx
	shrl	$31, %esi
	movq	%r11, %rdx
	movq	%r10, %rdi
	movq	%r9, -304(%rbp)
	movq	%r11, -280(%rbp)
	movq	%r10, -288(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	-280(%rbp), %r11
	movq	%rbx, -264(%rbp)
	movl	%r12d, %esi
	movl	%r12d, %ecx
	leaq	-192(%rbp), %rbx
	shrl	$31, %esi
	movq	%r11, %rdx
	movq	%rbx, %rdi
	movq	%r11, -296(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	-288(%rbp), %r10
	movl	%r14d, %r8d
	movq	%rbx, %rcx
	movq	%r13, %rsi
	movq	-304(%rbp), %r9
	leaq	-128(%rbp), %r12
	movq	%r10, %rdx
	movq	%r12, %rdi
	movq	%r9, -280(%rbp)
	call	_ZN6icu_6724DateTimePatternGenerator17replaceFieldTypesERKNS_13UnicodeStringES3_28UDateTimePatternMatchOptionsR10UErrorCode@PLT
	movq	-280(%rbp), %r9
	movl	24(%rbp), %edx
	movq	%r12, %rdi
	movq	-296(%rbp), %r11
	movq	%r15, -264(%rbp)
	movq	%r9, %rcx
	movq	%r11, %rsi
	call	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode@PLT
	movl	%eax, -280(%rbp)
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-288(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-280(%rbp), %eax
	jmp	.L174
.L192:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2487:
	.size	udatpg_replaceFieldTypesWithOptions_67, .-udatpg_replaceFieldTypesWithOptions_67
	.p2align 4
	.globl	udatpg_openSkeletons_67
	.type	udatpg_openSkeletons_67, @function
udatpg_openSkeletons_67:
.LFB2488:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$8, %rsp
	call	_ZNK6icu_6724DateTimePatternGenerator12getSkeletonsER10UErrorCode@PLT
	addq	$8, %rsp
	movq	%r12, %rsi
	movq	%rax, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	uenum_openFromStringEnumeration_67@PLT
	.cfi_endproc
.LFE2488:
	.size	udatpg_openSkeletons_67, .-udatpg_openSkeletons_67
	.p2align 4
	.globl	udatpg_openBaseSkeletons_67
	.type	udatpg_openBaseSkeletons_67, @function
udatpg_openBaseSkeletons_67:
.LFB2489:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$8, %rsp
	call	_ZNK6icu_6724DateTimePatternGenerator16getBaseSkeletonsER10UErrorCode@PLT
	addq	$8, %rsp
	movq	%r12, %rsi
	movq	%rax, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	uenum_openFromStringEnumeration_67@PLT
	.cfi_endproc
.LFE2489:
	.size	udatpg_openBaseSkeletons_67, .-udatpg_openBaseSkeletons_67
	.p2align 4
	.globl	udatpg_getPatternForSkeleton_67
	.type	udatpg_getPatternForSkeleton_67, @function
udatpg_getPatternForSkeleton_67:
.LFB2490:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	-112(%rbp), %r13
	leaq	-120(%rbp), %rdx
	pushq	%r12
	movq	%r13, %rdi
	.cfi_offset 12, -40
	movq	%rcx, %r12
	movl	%r8d, %ecx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rsi, -120(%rbp)
	movl	%r8d, %esi
	shrl	$31, %esi
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZNK6icu_6724DateTimePatternGenerator21getPatternForSkeletonERKNS_13UnicodeStringE@PLT
	testq	%r12, %r12
	je	.L198
	movswl	8(%rax), %edx
	testw	%dx, %dx
	js	.L199
	sarl	$5, %edx
.L200:
	movl	%edx, (%r12)
.L198:
	movzwl	8(%rax), %edx
	testb	$17, %dl
	jne	.L204
	andl	$2, %edx
	leaq	10(%rax), %r12
	je	.L209
.L201:
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L210
	addq	$104, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L209:
	.cfi_restore_state
	movq	24(%rax), %r12
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L199:
	movl	12(%rax), %edx
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L204:
	xorl	%r12d, %r12d
	jmp	.L201
.L210:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2490:
	.size	udatpg_getPatternForSkeleton_67, .-udatpg_getPatternForSkeleton_67
	.p2align 4
	.globl	udatpg_getDefaultHourCycle_67
	.type	udatpg_getDefaultHourCycle_67, @function
udatpg_getDefaultHourCycle_67:
.LFB2491:
	.cfi_startproc
	endbr64
	jmp	_ZNK6icu_6724DateTimePatternGenerator19getDefaultHourCycleER10UErrorCode@PLT
	.cfi_endproc
.LFE2491:
	.size	udatpg_getDefaultHourCycle_67, .-udatpg_getDefaultHourCycle_67
	.p2align 4
	.globl	udatpg_open_67
	.type	udatpg_open_67, @function
udatpg_open_67:
.LFB2468:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	subq	$240, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L217
	leaq	-256(%rbp), %r13
	movq	%rdi, %rsi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6724DateTimePatternGenerator14createInstanceERKNS_6LocaleER10UErrorCode@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN6icu_676LocaleD1Ev@PLT
.L212:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L218
	addq	$240, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L217:
	.cfi_restore_state
	movq	%rsi, %rdi
	call	_ZN6icu_6724DateTimePatternGenerator14createInstanceER10UErrorCode@PLT
	movq	%rax, %r12
	jmp	.L212
.L218:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2468:
	.size	udatpg_open_67, .-udatpg_open_67
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
