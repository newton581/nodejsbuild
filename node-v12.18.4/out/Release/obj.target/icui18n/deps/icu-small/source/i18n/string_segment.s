	.file	"string_segment.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713StringSegmentC2ERKNS_13UnicodeStringEb
	.type	_ZN6icu_6713StringSegmentC2ERKNS_13UnicodeStringEb, @function
_ZN6icu_6713StringSegmentC2ERKNS_13UnicodeStringEb:
.LFB2803:
	.cfi_startproc
	endbr64
	movswl	8(%rsi), %eax
	movq	%rsi, (%rdi)
	movl	$0, 8(%rdi)
	testw	%ax, %ax
	js	.L2
	sarl	$5, %eax
	movb	%dl, 16(%rdi)
	movl	%eax, 12(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movl	12(%rsi), %eax
	movb	%dl, 16(%rdi)
	movl	%eax, 12(%rdi)
	ret
	.cfi_endproc
.LFE2803:
	.size	_ZN6icu_6713StringSegmentC2ERKNS_13UnicodeStringEb, .-_ZN6icu_6713StringSegmentC2ERKNS_13UnicodeStringEb
	.globl	_ZN6icu_6713StringSegmentC1ERKNS_13UnicodeStringEb
	.set	_ZN6icu_6713StringSegmentC1ERKNS_13UnicodeStringEb,_ZN6icu_6713StringSegmentC2ERKNS_13UnicodeStringEb
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713StringSegment9getOffsetEv
	.type	_ZNK6icu_6713StringSegment9getOffsetEv, @function
_ZNK6icu_6713StringSegment9getOffsetEv:
.LFB2805:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE2805:
	.size	_ZNK6icu_6713StringSegment9getOffsetEv, .-_ZNK6icu_6713StringSegment9getOffsetEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713StringSegment9setOffsetEi
	.type	_ZN6icu_6713StringSegment9setOffsetEi, @function
_ZN6icu_6713StringSegment9setOffsetEi:
.LFB2806:
	.cfi_startproc
	endbr64
	movl	%esi, 8(%rdi)
	ret
	.cfi_endproc
.LFE2806:
	.size	_ZN6icu_6713StringSegment9setOffsetEi, .-_ZN6icu_6713StringSegment9setOffsetEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713StringSegment12adjustOffsetEi
	.type	_ZN6icu_6713StringSegment12adjustOffsetEi, @function
_ZN6icu_6713StringSegment12adjustOffsetEi:
.LFB2807:
	.cfi_startproc
	endbr64
	addl	%esi, 8(%rdi)
	ret
	.cfi_endproc
.LFE2807:
	.size	_ZN6icu_6713StringSegment12adjustOffsetEi, .-_ZN6icu_6713StringSegment12adjustOffsetEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713StringSegment23adjustOffsetByCodePointEv
	.type	_ZN6icu_6713StringSegment23adjustOffsetByCodePointEv, @function
_ZN6icu_6713StringSegment23adjustOffsetByCodePointEv:
.LFB2808:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	movl	8(%rbx), %esi
	movzwl	8(%rdi), %eax
	testw	%ax, %ax
	js	.L9
	movswl	%ax, %edx
	sarl	$5, %edx
.L10:
	movl	$1, %r12d
	cmpl	%esi, %edx
	jbe	.L11
	testb	$2, %al
	jne	.L20
	movq	24(%rdi), %rax
.L13:
	movslq	%esi, %rdx
	movzwl	(%rax,%rdx,2), %eax
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	je	.L21
	andl	$-2048, %eax
	xorl	%r12d, %r12d
	cmpl	$55296, %eax
	sete	%r12b
	addl	$1, %r12d
.L11:
	addl	%esi, %r12d
	movl	%r12d, 8(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	leaq	10(%rdi), %rax
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L9:
	movl	12(%rdi), %edx
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L21:
	leal	1(%rsi), %eax
	movl	$2, %r12d
	cmpl	12(%rbx), %eax
	jge	.L11
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movl	8(%rbx), %esi
	cmpl	$65535, %eax
	ja	.L11
	movl	$1, %r12d
	jmp	.L11
	.cfi_endproc
.LFE2808:
	.size	_ZN6icu_6713StringSegment23adjustOffsetByCodePointEv, .-_ZN6icu_6713StringSegment23adjustOffsetByCodePointEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713StringSegment9setLengthEi
	.type	_ZN6icu_6713StringSegment9setLengthEi, @function
_ZN6icu_6713StringSegment9setLengthEi:
.LFB2809:
	.cfi_startproc
	endbr64
	addl	8(%rdi), %esi
	movl	%esi, 12(%rdi)
	ret
	.cfi_endproc
.LFE2809:
	.size	_ZN6icu_6713StringSegment9setLengthEi, .-_ZN6icu_6713StringSegment9setLengthEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713StringSegment11resetLengthEv
	.type	_ZN6icu_6713StringSegment11resetLengthEv, @function
_ZN6icu_6713StringSegment11resetLengthEv:
.LFB2810:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movswl	8(%rdx), %eax
	testw	%ax, %ax
	js	.L24
	sarl	$5, %eax
	movl	%eax, 12(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	movl	12(%rdx), %eax
	movl	%eax, 12(%rdi)
	ret
	.cfi_endproc
.LFE2810:
	.size	_ZN6icu_6713StringSegment11resetLengthEv, .-_ZN6icu_6713StringSegment11resetLengthEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713StringSegment6lengthEv
	.type	_ZNK6icu_6713StringSegment6lengthEv, @function
_ZNK6icu_6713StringSegment6lengthEv:
.LFB2811:
	.cfi_startproc
	endbr64
	movl	12(%rdi), %eax
	subl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE2811:
	.size	_ZNK6icu_6713StringSegment6lengthEv, .-_ZNK6icu_6713StringSegment6lengthEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713StringSegment6charAtEi
	.type	_ZNK6icu_6713StringSegment6charAtEi, @function
_ZNK6icu_6713StringSegment6charAtEi:
.LFB2812:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addl	8(%rdi), %esi
	movzwl	8(%rax), %edx
	testw	%dx, %dx
	js	.L28
	movswl	%dx, %ecx
	sarl	$5, %ecx
.L29:
	movl	$-1, %r8d
	cmpl	%esi, %ecx
	jbe	.L27
	andl	$2, %edx
	jne	.L34
	movq	24(%rax), %rax
.L32:
	movslq	%esi, %rsi
	movzwl	(%rax,%rsi,2), %r8d
.L27:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	addq	$10, %rax
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L28:
	movl	12(%rax), %ecx
	jmp	.L29
	.cfi_endproc
.LFE2812:
	.size	_ZNK6icu_6713StringSegment6charAtEi, .-_ZNK6icu_6713StringSegment6charAtEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713StringSegment11codePointAtEi
	.type	_ZNK6icu_6713StringSegment11codePointAtEi, @function
_ZNK6icu_6713StringSegment11codePointAtEi:
.LFB2813:
	.cfi_startproc
	endbr64
	addl	8(%rdi), %esi
	movq	(%rdi), %rdi
	jmp	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	.cfi_endproc
.LFE2813:
	.size	_ZNK6icu_6713StringSegment11codePointAtEi, .-_ZNK6icu_6713StringSegment11codePointAtEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713StringSegment15toUnicodeStringEv
	.type	_ZNK6icu_6713StringSegment15toUnicodeStringEv, @function
_ZNK6icu_6713StringSegment15toUnicodeStringEv:
.LFB2814:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	(%rsi), %rdx
	movslq	8(%rsi), %rax
	movl	12(%rsi), %r8d
	movzwl	8(%rdx), %ecx
	subl	%eax, %r8d
	testb	$17, %cl
	jne	.L39
	andl	$2, %ecx
	jne	.L41
	movq	24(%rdx), %rdx
.L37:
	leaq	(%rdx,%rax,2), %rsi
	movq	%r12, %rdi
	movl	%r8d, %edx
	call	_ZN6icu_6713UnicodeStringC1EPKDsi@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	.cfi_restore_state
	addq	$10, %rdx
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L39:
	xorl	%edx, %edx
	jmp	.L37
	.cfi_endproc
.LFE2814:
	.size	_ZNK6icu_6713StringSegment15toUnicodeStringEv, .-_ZNK6icu_6713StringSegment15toUnicodeStringEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713StringSegment19toTempUnicodeStringEv
	.type	_ZNK6icu_6713StringSegment19toTempUnicodeStringEv, @function
_ZNK6icu_6713StringSegment19toTempUnicodeStringEv:
.LFB2815:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movslq	8(%rsi), %rdx
	movl	12(%rsi), %ecx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	subl	%edx, %ecx
	movzwl	8(%rax), %esi
	testb	$17, %sil
	jne	.L46
	andl	$2, %esi
	jne	.L48
	movq	24(%rax), %rax
.L43:
	leaq	(%rax,%rdx,2), %rax
	xorl	%esi, %esi
	leaq	-32(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, -32(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L49
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore_state
	addq	$10, %rax
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L46:
	xorl	%eax, %eax
	jmp	.L43
.L49:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2815:
	.size	_ZNK6icu_6713StringSegment19toTempUnicodeStringEv, .-_ZNK6icu_6713StringSegment19toTempUnicodeStringEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713StringSegment12getCodePointEv
	.type	_ZNK6icu_6713StringSegment12getCodePointEv, @function
_ZNK6icu_6713StringSegment12getCodePointEv:
.LFB2816:
	.cfi_startproc
	endbr64
	movq	(%rdi), %r8
	movl	8(%rdi), %esi
	movzwl	8(%r8), %eax
	testw	%ax, %ax
	js	.L51
	movswl	%ax, %edx
	sarl	$5, %edx
	cmpl	%esi, %edx
	jbe	.L58
.L63:
	testb	$2, %al
	jne	.L61
	movq	24(%r8), %rax
.L55:
	movslq	%esi, %rdx
	movzwl	(%rax,%rdx,2), %eax
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	je	.L62
	movl	%eax, %edx
	andl	$-2048, %edx
	cmpl	$55296, %edx
	movl	$-1, %edx
	cmove	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L61:
	leaq	10(%r8), %rax
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L51:
	movl	12(%r8), %edx
	cmpl	%esi, %edx
	ja	.L63
.L58:
	movl	$65535, %eax
.L50:
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	leal	1(%rsi), %edx
	movl	$-1, %eax
	cmpl	12(%rdi), %edx
	jge	.L50
	movq	%r8, %rdi
	jmp	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	.cfi_endproc
.LFE2816:
	.size	_ZNK6icu_6713StringSegment12getCodePointEv, .-_ZNK6icu_6713StringSegment12getCodePointEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713StringSegment10startsWithEi
	.type	_ZNK6icu_6713StringSegment10startsWithEi, @function
_ZNK6icu_6713StringSegment10startsWithEi:
.LFB2817:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	(%rdi), %r8
	movl	%esi, %r12d
	movzbl	16(%rdi), %r13d
	movl	8(%rdi), %esi
	movzwl	8(%r8), %eax
	testw	%ax, %ax
	js	.L65
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L66:
	movl	$65535, %edi
	cmpl	%esi, %ecx
	jbe	.L71
	testb	$2, %al
	jne	.L81
	movq	24(%r8), %rax
.L69:
	movslq	%esi, %rcx
	movzwl	(%rax,%rcx,2), %eax
	movl	%eax, %ecx
	andl	$-1024, %ecx
	cmpl	$55296, %ecx
	je	.L82
	movl	%eax, %edx
	movl	$-1, %edi
	andl	$-2048, %edx
	cmpl	$55296, %edx
	cmovne	%eax, %edi
.L71:
	cmpl	%edi, %r12d
	je	.L76
.L84:
	testb	%r13b, %r13b
	jne	.L83
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L81:
	.cfi_restore_state
	leaq	10(%r8), %rax
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L83:
	movl	$1, %esi
	call	u_foldCase_67@PLT
	movl	%r12d, %edi
	movl	$1, %esi
	movl	%eax, %r13d
	call	u_foldCase_67@PLT
	popq	%r12
	cmpl	%eax, %r13d
	sete	%r13b
	movl	%r13d, %eax
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	.cfi_restore_state
	movl	12(%r8), %ecx
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L82:
	leal	1(%rsi), %eax
	movl	$-1, %edi
	cmpl	12(%rdx), %eax
	jge	.L71
	movq	%r8, %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movl	%eax, %edi
	cmpl	%edi, %r12d
	jne	.L84
.L76:
	movl	$1, %r13d
	popq	%r12
	movl	%r13d, %eax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2817:
	.size	_ZNK6icu_6713StringSegment10startsWithEi, .-_ZNK6icu_6713StringSegment10startsWithEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713StringSegment10startsWithERKNS_13UnicodeStringE
	.type	_ZNK6icu_6713StringSegment10startsWithERKNS_13UnicodeStringE, @function
_ZNK6icu_6713StringSegment10startsWithERKNS_13UnicodeStringE:
.LFB2819:
	.cfi_startproc
	endbr64
	movswl	8(%rsi), %eax
	testb	$1, %al
	jne	.L108
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	testw	%ax, %ax
	js	.L88
	sarl	$5, %eax
	testl	%eax, %eax
	je	.L90
.L110:
	movl	12(%rbx), %edx
	movl	8(%rbx), %esi
	cmpl	%esi, %edx
	je	.L90
	movq	(%rbx), %rdi
	movzwl	8(%rdi), %eax
	testw	%ax, %ax
	js	.L91
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L92:
	movl	$65535, %r13d
	cmpl	%esi, %ecx
	jbe	.L97
	leaq	10(%rdi), %rcx
	testb	$2, %al
	jne	.L95
	movq	24(%rdi), %rcx
.L95:
	movslq	%esi, %rax
	movzwl	(%rcx,%rax,2), %r13d
	movl	%r13d, %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	je	.L109
	movl	%r13d, %eax
	andl	$-2048, %eax
	cmpl	$55296, %eax
	movl	$-1, %eax
	cmove	%eax, %r13d
.L97:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movl	%eax, %r12d
	movl	$1, %eax
	cmpl	%r13d, %r12d
	je	.L85
	cmpb	$0, 16(%rbx)
	je	.L90
	movl	%r13d, %edi
	movl	$1, %esi
	call	u_foldCase_67@PLT
	movl	%r12d, %edi
	movl	$1, %esi
	movl	%eax, %ebx
	call	u_foldCase_67@PLT
	cmpl	%eax, %ebx
	sete	%al
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L88:
	.cfi_restore_state
	movl	12(%rsi), %eax
	testl	%eax, %eax
	jne	.L110
	.p2align 4,,10
	.p2align 3
.L90:
	xorl	%eax, %eax
.L85:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L108:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L91:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movl	12(%rdi), %ecx
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L109:
	leal	1(%rsi), %eax
	movl	$-1, %r13d
	cmpl	%eax, %edx
	jle	.L97
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movl	%eax, %r13d
	jmp	.L97
	.cfi_endproc
.LFE2819:
	.size	_ZNK6icu_6713StringSegment10startsWithERKNS_13UnicodeStringE, .-_ZNK6icu_6713StringSegment10startsWithERKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713StringSegment21getCommonPrefixLengthERKNS_13UnicodeStringE
	.type	_ZN6icu_6713StringSegment21getCommonPrefixLengthERKNS_13UnicodeStringE, @function
_ZN6icu_6713StringSegment21getCommonPrefixLengthERKNS_13UnicodeStringE:
.LFB2820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	movl	%ebx, %r13d
	movl	%ebx, %r12d
	subq	$24, %rsp
	movzbl	16(%rdi), %eax
	movswl	8(%rsi), %esi
	movl	8(%rdi), %edi
	movb	%al, -49(%rbp)
	testw	%si, %si
	js	.L112
.L133:
	sarl	$5, %esi
.L113:
	movl	12(%r15), %eax
	subl	%edi, %eax
	movl	%eax, %edi
	call	uprv_min_67@PLT
	cmpl	%ebx, %eax
	jle	.L111
	movq	(%r15), %rax
	movl	8(%r15), %edi
	movzwl	8(%rax), %r8d
	leal	(%r12,%rdi), %ecx
	testw	%r8w, %r8w
	js	.L115
	movswl	%r8w, %esi
	sarl	$5, %esi
.L116:
	movl	$65535, %r9d
	cmpl	%ecx, %esi
	jbe	.L117
	andl	$2, %r8d
	je	.L118
	addq	$10, %rax
.L119:
	movslq	%ecx, %rcx
	movzwl	(%rax,%rcx,2), %r9d
.L117:
	movswl	8(%r14), %esi
	testw	%si, %si
	js	.L120
	movswl	%si, %eax
	sarl	$5, %eax
.L121:
	movl	$65535, %r8d
	cmpl	%r13d, %eax
	jbe	.L122
	leaq	10(%r14), %rax
	testb	$2, %sil
	jne	.L124
	movq	24(%r14), %rax
.L124:
	movzwl	(%rax,%rbx,2), %r8d
.L122:
	cmpl	%r8d, %r9d
	movl	%r8d, -56(%rbp)
	je	.L125
	cmpb	$0, -49(%rbp)
	je	.L111
	movl	$1, %esi
	movl	%r9d, %edi
	call	u_foldCase_67@PLT
	movl	-56(%rbp), %r8d
	movl	$1, %esi
	movl	%eax, %r13d
	movl	%r8d, %edi
	call	u_foldCase_67@PLT
	cmpl	%eax, %r13d
	jne	.L111
	movswl	8(%r14), %esi
	movl	8(%r15), %edi
.L125:
	addq	$1, %rbx
	movl	%ebx, %r13d
	movl	%ebx, %r12d
	testw	%si, %si
	jns	.L133
.L112:
	movl	12(%r14), %esi
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L111:
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L115:
	.cfi_restore_state
	movl	12(%rax), %esi
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L120:
	movl	12(%r14), %eax
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L118:
	movq	24(%rax), %rax
	jmp	.L119
	.cfi_endproc
.LFE2820:
	.size	_ZN6icu_6713StringSegment21getCommonPrefixLengthERKNS_13UnicodeStringE, .-_ZN6icu_6713StringSegment21getCommonPrefixLengthERKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713StringSegment28getCaseSensitivePrefixLengthERKNS_13UnicodeStringE
	.type	_ZN6icu_6713StringSegment28getCaseSensitivePrefixLengthERKNS_13UnicodeStringE, @function
_ZN6icu_6713StringSegment28getCaseSensitivePrefixLengthERKNS_13UnicodeStringE:
.LFB2821:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movswl	8(%rsi), %esi
	movl	8(%rdi), %ecx
.L148:
	movl	%r14d, %r12d
	movl	%r14d, %r15d
	testw	%si, %si
	js	.L135
	sarl	$5, %esi
.L136:
	movl	12(%rbx), %edi
	subl	%ecx, %edi
	call	uprv_min_67@PLT
	cmpl	%r14d, %eax
	jle	.L134
	movq	(%rbx), %rax
	movl	8(%rbx), %ecx
	movzwl	8(%rax), %esi
	leal	(%r15,%rcx), %edx
	testw	%si, %si
	js	.L138
	movswl	%si, %edi
	sarl	$5, %edi
.L139:
	movl	$-1, %r8d
	cmpl	%edx, %edi
	jbe	.L140
	andl	$2, %esi
	jne	.L153
	movq	24(%rax), %rax
.L142:
	movslq	%edx, %rdx
	movzwl	(%rax,%rdx,2), %r8d
.L140:
	movswl	8(%r13), %esi
	testw	%si, %si
	js	.L143
	movswl	%si, %eax
	sarl	$5, %eax
.L144:
	movl	$-1, %edx
	cmpl	%r12d, %eax
	jbe	.L145
	leaq	10(%r13), %rax
	testb	$2, %sil
	je	.L154
.L147:
	movzwl	(%rax,%r14,2), %edx
.L145:
	addq	$1, %r14
	cmpw	%r8w, %dx
	je	.L148
.L134:
	addq	$8, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L135:
	.cfi_restore_state
	movl	12(%r13), %esi
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L154:
	movq	24(%r13), %rax
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L153:
	addq	$10, %rax
	jmp	.L142
	.p2align 4,,10
	.p2align 3
.L143:
	movl	12(%r13), %eax
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L138:
	movl	12(%rax), %edi
	jmp	.L139
	.cfi_endproc
.LFE2821:
	.size	_ZN6icu_6713StringSegment28getCaseSensitivePrefixLengthERKNS_13UnicodeStringE, .-_ZN6icu_6713StringSegment28getCaseSensitivePrefixLengthERKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713StringSegment23getPrefixLengthInternalERKNS_13UnicodeStringEb
	.type	_ZN6icu_6713StringSegment23getPrefixLengthInternalERKNS_13UnicodeStringEb, @function
_ZN6icu_6713StringSegment23getPrefixLengthInternalERKNS_13UnicodeStringEb:
.LFB2822:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%r14d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movl	%r14d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movswl	8(%rsi), %esi
	movl	8(%rdi), %edi
	movb	%dl, -53(%rbp)
	testw	%si, %si
	js	.L156
.L177:
	sarl	$5, %esi
.L157:
	movl	12(%r15), %eax
	subl	%edi, %eax
	movl	%eax, %edi
	call	uprv_min_67@PLT
	cmpl	%r14d, %eax
	jle	.L155
	movq	(%r15), %rax
	movl	8(%r15), %edi
	movzwl	8(%rax), %esi
	leal	(%r12,%rdi), %edx
	testw	%si, %si
	js	.L159
	movswl	%si, %r8d
	sarl	$5, %r8d
.L160:
	movl	$65535, %r9d
	cmpl	%edx, %r8d
	jbe	.L161
	andl	$2, %esi
	je	.L162
	addq	$10, %rax
.L163:
	movslq	%edx, %rdx
	movzwl	(%rax,%rdx,2), %r9d
.L161:
	movswl	8(%rbx), %esi
	testw	%si, %si
	js	.L164
	movswl	%si, %eax
	sarl	$5, %eax
.L165:
	movl	$65535, %r8d
	cmpl	%r13d, %eax
	jbe	.L166
	leaq	10(%rbx), %rax
	testb	$2, %sil
	jne	.L168
	movq	24(%rbx), %rax
.L168:
	movzwl	(%rax,%r14,2), %r8d
.L166:
	cmpl	%r8d, %r9d
	movl	%r8d, -52(%rbp)
	je	.L169
	cmpb	$0, -53(%rbp)
	je	.L155
	movl	$1, %esi
	movl	%r9d, %edi
	call	u_foldCase_67@PLT
	movl	-52(%rbp), %r8d
	movl	$1, %esi
	movl	%eax, %r13d
	movl	%r8d, %edi
	call	u_foldCase_67@PLT
	cmpl	%eax, %r13d
	jne	.L155
	movswl	8(%rbx), %esi
	movl	8(%r15), %edi
.L169:
	addq	$1, %r14
	movl	%r14d, %r13d
	movl	%r14d, %r12d
	testw	%si, %si
	jns	.L177
.L156:
	movl	12(%rbx), %esi
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L155:
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L159:
	.cfi_restore_state
	movl	12(%rax), %r8d
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L164:
	movl	12(%rbx), %eax
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L162:
	movq	24(%rax), %rax
	jmp	.L163
	.cfi_endproc
.LFE2822:
	.size	_ZN6icu_6713StringSegment23getPrefixLengthInternalERKNS_13UnicodeStringEb, .-_ZN6icu_6713StringSegment23getPrefixLengthInternalERKNS_13UnicodeStringEb
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713StringSegment15codePointsEqualEiib
	.type	_ZN6icu_6713StringSegment15codePointsEqualEiib, @function
_ZN6icu_6713StringSegment15codePointsEqualEiib:
.LFB2823:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	cmpl	%esi, %edi
	je	.L183
	xorl	%eax, %eax
	testb	%dl, %dl
	jne	.L186
.L183:
	ret
	.p2align 4,,10
	.p2align 3
.L186:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%esi, %r12d
	movl	$1, %esi
	pushq	%rbx
	.cfi_offset 3, -32
	call	u_foldCase_67@PLT
	movl	%r12d, %edi
	movl	$1, %esi
	movl	%eax, %ebx
	call	u_foldCase_67@PLT
	cmpl	%eax, %ebx
	popq	%rbx
	popq	%r12
	sete	%al
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2823:
	.size	_ZN6icu_6713StringSegment15codePointsEqualEiib, .-_ZN6icu_6713StringSegment15codePointsEqualEiib
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713StringSegment10startsWithERKNS_10UnicodeSetE
	.type	_ZNK6icu_6713StringSegment10startsWithERKNS_10UnicodeSetE, @function
_ZNK6icu_6713StringSegment10startsWithERKNS_10UnicodeSetE:
.LFB2818:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$8, %rsp
	movq	(%rdi), %r8
	movl	8(%rdi), %r9d
	movzwl	8(%r8), %eax
	testw	%ax, %ax
	js	.L188
	movswl	%ax, %edx
	sarl	$5, %edx
	cmpl	%r9d, %edx
	jbe	.L198
.L208:
	testb	$2, %al
	jne	.L206
	movq	24(%r8), %rax
.L192:
	movslq	%r9d, %rdx
	movzwl	(%rax,%rdx,2), %esi
	movl	%esi, %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	je	.L207
	movl	%esi, %eax
	andl	$-2048, %eax
	cmpl	$55296, %eax
	je	.L196
.L197:
	movq	%r12, %rdi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	setne	%al
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L206:
	.cfi_restore_state
	leaq	10(%r8), %rax
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L207:
	leal	1(%r9), %eax
	cmpl	12(%rdi), %eax
	jge	.L196
	movl	%r9d, %esi
	movq	%r8, %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movl	%eax, %esi
	cmpl	$-1, %eax
	jne	.L197
.L196:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L188:
	.cfi_restore_state
	movl	12(%r8), %edx
	cmpl	%r9d, %edx
	ja	.L208
.L198:
	movl	$65535, %esi
	jmp	.L197
	.cfi_endproc
.LFE2818:
	.size	_ZNK6icu_6713StringSegment10startsWithERKNS_10UnicodeSetE, .-_ZNK6icu_6713StringSegment10startsWithERKNS_10UnicodeSetE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713StringSegmenteqERKNS_13UnicodeStringE
	.type	_ZNK6icu_6713StringSegmenteqERKNS_13UnicodeStringE, @function
_ZNK6icu_6713StringSegmenteqERKNS_13UnicodeStringE:
.LFB2824:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	subq	$104, %rsp
	.cfi_offset 12, -40
	movq	(%rdi), %rdx
	movl	12(%rdi), %ecx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movslq	8(%rdi), %rax
	movzwl	8(%rdx), %esi
	subl	%eax, %ecx
	testb	$17, %sil
	jne	.L220
	andl	$2, %esi
	jne	.L225
	movq	24(%rdx), %rdx
.L210:
	leaq	-112(%rbp), %r14
	leaq	(%rdx,%rax,2), %rax
	xorl	%esi, %esi
	leaq	-120(%rbp), %rdx
	movq	%r14, %rdi
	movq	%rax, -120(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movswl	-104(%rbp), %edx
	testb	$1, %dl
	je	.L212
	movzwl	8(%r13), %r12d
	andl	$1, %r12d
.L213:
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L226
	addq	$104, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L225:
	.cfi_restore_state
	addq	$10, %rdx
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L212:
	testw	%dx, %dx
	js	.L214
	movzwl	8(%r13), %r12d
	sarl	$5, %edx
	testw	%r12w, %r12w
	js	.L216
.L227:
	movswl	%r12w, %eax
	sarl	$5, %eax
.L217:
	cmpl	%edx, %eax
	notl	%r12d
	sete	%al
	andb	%al, %r12b
	je	.L213
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%r12b
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L214:
	movzwl	8(%r13), %r12d
	movl	-100(%rbp), %edx
	testw	%r12w, %r12w
	jns	.L227
.L216:
	movl	12(%r13), %eax
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L220:
	xorl	%edx, %edx
	jmp	.L210
.L226:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2824:
	.size	_ZNK6icu_6713StringSegmenteqERKNS_13UnicodeStringE, .-_ZNK6icu_6713StringSegmenteqERKNS_13UnicodeStringE
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
