	.file	"collationsets.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711TailoredSet7forDataEPKNS_13CollationDataER10UErrorCode
	.type	_ZN6icu_6711TailoredSet7forDataEPKNS_13CollationDataER10UErrorCode, @function
_ZN6icu_6711TailoredSet7forDataEPKNS_13CollationDataER10UErrorCode:
.LFB3273:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	(%rdx), %edx
	testl	%edx, %edx
	jg	.L1
	movl	%edx, 96(%rdi)
	movq	32(%rsi), %rdx
	movq	%rsi, %rax
	movq	%rdi, %rbx
	movq	%rsi, (%rdi)
	movq	%rdi, %rcx
	xorl	%esi, %esi
	movq	%rdx, 8(%rdi)
	movq	(%rax), %rdi
	leaq	enumTailoredRange(%rip), %rdx
	call	utrie2_enum_67@PLT
	movl	96(%rbx), %eax
	movl	%eax, (%r12)
.L1:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3273:
	.size	_ZN6icu_6711TailoredSet7forDataEPKNS_13CollationDataER10UErrorCode, .-_ZN6icu_6711TailoredSet7forDataEPKNS_13CollationDataER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725ContractionsAndExpansions6CESinkD2Ev
	.type	_ZN6icu_6725ContractionsAndExpansions6CESinkD2Ev, @function
_ZN6icu_6725ContractionsAndExpansions6CESinkD2Ev:
.LFB3284:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3284:
	.size	_ZN6icu_6725ContractionsAndExpansions6CESinkD2Ev, .-_ZN6icu_6725ContractionsAndExpansions6CESinkD2Ev
	.globl	_ZN6icu_6725ContractionsAndExpansions6CESinkD1Ev
	.set	_ZN6icu_6725ContractionsAndExpansions6CESinkD1Ev,_ZN6icu_6725ContractionsAndExpansions6CESinkD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725ContractionsAndExpansions6CESinkD0Ev
	.type	_ZN6icu_6725ContractionsAndExpansions6CESinkD0Ev, @function
_ZN6icu_6725ContractionsAndExpansions6CESinkD0Ev:
.LFB3286:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3286:
	.size	_ZN6icu_6725ContractionsAndExpansions6CESinkD0Ev, .-_ZN6icu_6725ContractionsAndExpansions6CESinkD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725ContractionsAndExpansions7forDataEPKNS_13CollationDataER10UErrorCode
	.type	_ZN6icu_6725ContractionsAndExpansions7forDataEPKNS_13CollationDataER10UErrorCode, @function
_ZN6icu_6725ContractionsAndExpansions7forDataEPKNS_13CollationDataER10UErrorCode:
.LFB3288:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L14
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpq	$0, 32(%rsi)
	movl	%eax, 760(%rdi)
	je	.L9
	movb	$-1, 33(%rdi)
.L9:
	movq	%r12, (%rbx)
	movq	(%r12), %rdi
	xorl	%esi, %esi
	movq	%rbx, %rcx
	leaq	enumCnERange(%rip), %rdx
	call	utrie2_enum_67@PLT
	cmpq	$0, 32(%r12)
	movl	760(%rbx), %eax
	je	.L17
	testl	%eax, %eax
	jle	.L12
.L17:
	movl	%eax, 0(%r13)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	leaq	40(%rbx), %rdi
	call	_ZN6icu_6710UnicodeSet6freezeEv@PLT
	movb	$1, 33(%rbx)
	movq	32(%r12), %rax
	xorl	%esi, %esi
	movq	%rbx, %rcx
	leaq	enumCnERange(%rip), %rdx
	movq	%rax, (%rbx)
	movq	(%rax), %rdi
	call	utrie2_enum_67@PLT
	movl	760(%rbx), %eax
	movl	%eax, 0(%r13)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3288:
	.size	_ZN6icu_6725ContractionsAndExpansions7forDataEPKNS_13CollationDataER10UErrorCode, .-_ZN6icu_6725ContractionsAndExpansions7forDataEPKNS_13CollationDataER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711TailoredSet9addSuffixEiRKNS_13UnicodeStringE
	.type	_ZN6icu_6711TailoredSet9addSuffixEiRKNS_13UnicodeStringE, @function
_ZN6icu_6711TailoredSet9addSuffixEiRKNS_13UnicodeStringE:
.LFB3281:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%esi, %r15d
	leaq	24(%rdi), %rsi
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-112(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	subq	$80, %rsp
	.cfi_offset 12, -48
	movq	16(%rdi), %r12
	movq	%r14, %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	%r14, %rdi
	movl	%r15d, %esi
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	movswl	8(%r13), %ecx
	movq	%rax, %rdi
	testw	%cx, %cx
	js	.L19
	sarl	$5, %ecx
.L20:
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_6710UnicodeSet3addERKNS_13UnicodeStringE@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L23
	addq	$80, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	movl	12(%r13), %ecx
	jmp	.L20
.L23:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3281:
	.size	_ZN6icu_6711TailoredSet9addSuffixEiRKNS_13UnicodeStringE, .-_ZN6icu_6711TailoredSet9addSuffixEiRKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711TailoredSet3addEi
	.type	_ZN6icu_6711TailoredSet3addEi, @function
_ZN6icu_6711TailoredSet3addEi:
.LFB3282:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movswl	32(%rdi), %eax
	shrl	$5, %eax
	jne	.L25
	cmpq	$0, 88(%rdi)
	je	.L35
.L25:
	leaq	-112(%rbp), %r12
	leaq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	movq	88(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L27
	movswl	8(%rsi), %ecx
	testw	%cx, %cx
	js	.L28
	sarl	$5, %ecx
.L29:
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
.L27:
	movq	16(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6710UnicodeSet3addERKNS_13UnicodeStringE@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L24:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L36
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	.cfi_restore_state
	movl	12(%rsi), %ecx
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L35:
	movq	16(%rdi), %rdi
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	jmp	.L24
.L36:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3282:
	.size	_ZN6icu_6711TailoredSet3addEi, .-_ZN6icu_6711TailoredSet3addEi
	.align 2
	.p2align 4
	.type	_ZN6icu_6725ContractionsAndExpansions10addStringsEiiPNS_10UnicodeSetE.part.0, @function
_ZN6icu_6725ContractionsAndExpansions10addStringsEiiPNS_10UnicodeSetE.part.0:
.LFB4293:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-128(%rbp), %r15
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	leaq	440(%rdi), %rsi
	movq	%r15, %rdi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L63:
	sarl	$5, %eax
.L42:
	movzwl	-120(%rbp), %edx
	testl	%eax, %eax
	jne	.L43
	testb	$1, %dl
	jne	.L61
.L43:
	testw	%dx, %dx
	js	.L45
	movswl	%dx, %ecx
	sarl	$5, %ecx
.L46:
	cmpl	%eax, %ecx
	jbe	.L44
	cmpl	$1023, %eax
	jg	.L47
	andl	$31, %edx
	sall	$5, %eax
	orl	%edx, %eax
	movw	%ax, -120(%rbp)
.L44:
	addl	$1, %ebx
	cmpl	%r14d, %ebx
	jg	.L62
.L48:
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	movq	504(%r12), %rsi
	testq	%rsi, %rsi
	je	.L38
	movswl	8(%rsi), %ecx
	testw	%cx, %cx
	js	.L39
	sarl	$5, %ecx
.L40:
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
.L38:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSet3addERKNS_13UnicodeStringE@PLT
	movswl	448(%r12), %eax
	testw	%ax, %ax
	jns	.L63
	movl	452(%r12), %eax
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L45:
	movl	-116(%rbp), %ecx
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L39:
	movl	12(%rsi), %ecx
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L47:
	orl	$-32, %edx
	addl	$1, %ebx
	movl	%eax, -116(%rbp)
	movw	%dx, -120(%rbp)
	cmpl	%r14d, %ebx
	jle	.L48
.L62:
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L64
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L61:
	.cfi_restore_state
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	jmp	.L44
.L64:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4293:
	.size	_ZN6icu_6725ContractionsAndExpansions10addStringsEiiPNS_10UnicodeSetE.part.0, .-_ZN6icu_6725ContractionsAndExpansions10addStringsEiiPNS_10UnicodeSetE.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725ContractionsAndExpansions10addStringsEiiPNS_10UnicodeSetE
	.type	_ZN6icu_6725ContractionsAndExpansions10addStringsEiiPNS_10UnicodeSetE, @function
_ZN6icu_6725ContractionsAndExpansions10addStringsEiiPNS_10UnicodeSetE:
.LFB3294:
	.cfi_startproc
	endbr64
	testq	%rcx, %rcx
	je	.L65
	jmp	_ZN6icu_6725ContractionsAndExpansions10addStringsEiiPNS_10UnicodeSetE.part.0
	.p2align 4,,10
	.p2align 3
.L65:
	ret
	.cfi_endproc
.LFE3294:
	.size	_ZN6icu_6725ContractionsAndExpansions10addStringsEiiPNS_10UnicodeSetE, .-_ZN6icu_6725ContractionsAndExpansions10addStringsEiiPNS_10UnicodeSetE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711TailoredSet15addContractionsEiPKDs
	.type	_ZN6icu_6711TailoredSet15addContractionsEiPKDs, @function
_ZN6icu_6711TailoredSet15addContractionsEiPKDs:
.LFB3280:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-176(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	96(%rdi), %rbx
	movq	%rbx, %rcx
	subq	$232, %rsp
	movq	%rdi, -264(%rbp)
	movq	%r13, %rdi
	movl	%esi, -268(%rbp)
	leaq	-248(%rbp), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdx, -248(%rbp)
	xorl	%edx, %edx
	call	_ZN6icu_6710UCharsTrie8IteratorC1ENS_14ConstChar16PtrEiR10UErrorCode@PLT
	addq	$24, %r15
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L74:
	sarl	$5, %ecx
.L70:
	leaq	-136(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_6710UnicodeSet3addERKNS_13UnicodeStringE@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L71:
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6710UCharsTrie8Iterator4nextER10UErrorCode@PLT
	testb	%al, %al
	je	.L68
	movq	-264(%rbp), %rax
	leaq	-240(%rbp), %r14
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	16(%rax), %r12
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movl	-268(%rbp), %esi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	movswl	-128(%rbp), %ecx
	movq	%rax, %rdi
	testw	%cx, %cx
	jns	.L74
	movl	-124(%rbp), %ecx
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L68:
	movq	%r13, %rdi
	call	_ZN6icu_6710UCharsTrie8IteratorD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L75
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L75:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3280:
	.size	_ZN6icu_6711TailoredSet15addContractionsEiPKDs, .-_ZN6icu_6711TailoredSet15addContractionsEiPKDs
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711TailoredSet9addPrefixEPKNS_13CollationDataERKNS_13UnicodeStringEij
	.type	_ZN6icu_6711TailoredSet9addPrefixEPKNS_13CollationDataERKNS_13UnicodeStringEij, @function
_ZN6icu_6711TailoredSet9addPrefixEPKNS_13CollationDataERKNS_13UnicodeStringEij:
.LFB3279:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	24(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%r8d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movq	%rdx, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movl	%ecx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r15, %rdi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movswl	32(%rbx), %edx
	testw	%dx, %dx
	js	.L77
	sarl	$5, %edx
.L78:
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeString9doReverseEii@PLT
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	_ZNK6icu_6713CollationData12getFinalCE32Ej@PLT
	cmpb	$-65, %al
	jbe	.L79
	movl	%eax, %edx
	andl	$15, %edx
	cmpl	$9, %edx
	je	.L84
.L79:
	leaq	-128(%rbp), %r13
	movq	%r15, %rsi
	movq	16(%rbx), %r14
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_6710UnicodeSet3addERKNS_13UnicodeStringE@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movzwl	32(%rbx), %edx
	movl	%edx, %eax
	andl	$31, %eax
	andl	$1, %edx
	movl	$2, %edx
	cmovne	%edx, %eax
	movw	%ax, 32(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L85
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L77:
	.cfi_restore_state
	movl	36(%rbx), %edx
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L84:
	movq	24(%r13), %rdx
	shrl	$13, %eax
	movl	%r12d, %esi
	movq	%rbx, %rdi
	leaq	4(%rdx,%rax,2), %rdx
	call	_ZN6icu_6711TailoredSet15addContractionsEiPKDs
	jmp	.L79
.L85:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3279:
	.size	_ZN6icu_6711TailoredSet9addPrefixEPKNS_13CollationDataERKNS_13UnicodeStringEij, .-_ZN6icu_6711TailoredSet9addPrefixEPKNS_13CollationDataERKNS_13UnicodeStringEij
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711TailoredSet11addPrefixesEPKNS_13CollationDataEiPKDs
	.type	_ZN6icu_6711TailoredSet11addPrefixesEPKNS_13CollationDataEiPKDs, @function
_ZN6icu_6711TailoredSet11addPrefixesEPKNS_13CollationDataEiPKDs:
.LFB3278:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	leaq	-184(%rbp), %rsi
	.cfi_offset 14, -32
	movl	%edx, %r14d
	xorl	%edx, %edx
	pushq	%r13
	.cfi_offset 13, -40
	leaq	96(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-176(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$168, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rcx, -184(%rbp)
	movq	%r13, %rcx
	call	_ZN6icu_6710UCharsTrie8IteratorC1ENS_14ConstChar16PtrEiR10UErrorCode@PLT
	leaq	-136(%rbp), %rax
	movq	%rax, -200(%rbp)
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L91:
	movl	-68(%rbp), %r8d
	movl	%r14d, %ecx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	-200(%rbp), %rdx
	call	_ZN6icu_6711TailoredSet9addPrefixEPKNS_13CollationDataERKNS_13UnicodeStringEij
.L88:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6710UCharsTrie8Iterator4nextER10UErrorCode@PLT
	testb	%al, %al
	jne	.L91
	movq	%r12, %rdi
	call	_ZN6icu_6710UCharsTrie8IteratorD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L92
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L92:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3278:
	.size	_ZN6icu_6711TailoredSet11addPrefixesEPKNS_13CollationDataEiPKDs, .-_ZN6icu_6711TailoredSet11addPrefixesEPKNS_13CollationDataEiPKDs
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711TailoredSet19compareContractionsEiPKDsS2_
	.type	_ZN6icu_6711TailoredSet19compareContractionsEiPKDsS2_, @function
_ZN6icu_6711TailoredSet19compareContractionsEiPKDsS2_:
.LFB3277:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-440(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	96(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	movq	%r13, %rcx
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$456, %rsp
	movl	%esi, -452(%rbp)
	movq	%r15, %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-304(%rbp), %rax
	movq	%rdx, -440(%rbp)
	xorl	%edx, %edx
	movq	%rax, %rdi
	movq	%rax, -472(%rbp)
	call	_ZN6icu_6710UCharsTrie8IteratorC1ENS_14ConstChar16PtrEiR10UErrorCode@PLT
	leaq	-176(%rbp), %rax
	movq	%r13, %rcx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%r12, -440(%rbp)
	movq	%rax, -480(%rbp)
	call	_ZN6icu_6710UCharsTrie8IteratorC1ENS_14ConstChar16PtrEiR10UErrorCode@PLT
	leaq	-432(%rbp), %r12
	movl	$65535, %esi
	xorl	%r14d, %r14d
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EDs@PLT
	movl	$-1, %eax
	movq	%r15, %rsi
	xorl	%edx, %edx
	movl	$1, %ecx
	movq	%r12, %rdi
	movw	%ax, -440(%rbp)
	movq	%r14, %r15
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	-136(%rbp), %rax
	movq	%r12, -464(%rbp)
	movq	%rax, -488(%rbp)
	testq	%r15, %r15
	je	.L132
.L95:
	movq	-480(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6710UCharsTrie8Iterator4nextER10UErrorCode@PLT
	testb	%al, %al
	jne	.L133
	movq	-464(%rbp), %rax
	movq	%rax, %r12
	cmpq	%rax, %r15
	je	.L116
	.p2align 4,,10
	.p2align 3
.L97:
	movzwl	8(%r12), %esi
	testw	%si, %si
	js	.L98
.L134:
	movzwl	8(%r15), %eax
	movswl	%si, %ecx
	sarl	$5, %ecx
	testw	%ax, %ax
	js	.L100
.L135:
	movswl	%ax, %edx
	sarl	$5, %edx
	testb	$1, %sil
	je	.L102
.L136:
	notl	%eax
	andl	$1, %eax
.L103:
	testl	%eax, %eax
	je	.L111
	movq	16(%rbx), %rax
	leaq	-368(%rbp), %r14
	leaq	24(%rbx), %rsi
	movq	%r14, %rdi
	movq	%rax, -496(%rbp)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movl	-452(%rbp), %esi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	movswl	8(%r12), %ecx
	movq	%rax, %rdi
	testw	%cx, %cx
	js	.L112
	sarl	$5, %ecx
.L113:
	xorl	%edx, %edx
	movq	%r12, %rsi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	-496(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN6icu_6710UnicodeSet3addERKNS_13UnicodeStringE@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	testq	%r15, %r15
	jne	.L95
.L132:
	movq	-472(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6710UCharsTrie8Iterator4nextER10UErrorCode@PLT
	movq	-464(%rbp), %r15
	testb	%al, %al
	je	.L95
	leaq	-264(%rbp), %r15
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L133:
	movq	-488(%rbp), %r12
	movzwl	8(%r12), %esi
	testw	%si, %si
	jns	.L134
	.p2align 4,,10
	.p2align 3
.L98:
	movzwl	8(%r15), %eax
	movl	12(%r12), %ecx
	testw	%ax, %ax
	jns	.L135
.L100:
	movl	12(%r15), %edx
	testb	$1, %sil
	jne	.L136
.L102:
	testl	%ecx, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	cmovle	%ecx, %r8d
	js	.L104
	movl	%ecx, %r9d
	subl	%r8d, %r9d
	cmpl	%ecx, %r9d
	cmovg	%ecx, %r9d
.L104:
	andl	$2, %esi
	leaq	10(%r12), %rcx
	jne	.L106
	movq	24(%r12), %rcx
.L106:
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	movsbl	%al, %eax
	testl	%eax, %eax
	jns	.L103
	movq	16(%rbx), %rax
	leaq	-368(%rbp), %r14
	leaq	24(%rbx), %rsi
	movq	%r14, %rdi
	movq	%rax, -496(%rbp)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movl	-452(%rbp), %esi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	movswl	8(%r15), %ecx
	movq	%rax, %rdi
	testw	%cx, %cx
	js	.L107
	sarl	$5, %ecx
.L108:
	xorl	%edx, %edx
	movq	%r15, %rsi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	-496(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN6icu_6710UnicodeSet3addERKNS_13UnicodeStringE@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-472(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6710UCharsTrie8Iterator4nextER10UErrorCode@PLT
	testb	%al, %al
	je	.L137
	leaq	-264(%rbp), %r15
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L111:
	movl	-68(%rbp), %ecx
	movl	-196(%rbp), %edx
	movq	%r15, 88(%rbx)
	movq	%rbx, %rdi
	movl	-452(%rbp), %esi
	leaq	-264(%rbp), %r15
	call	_ZN6icu_6711TailoredSet7compareEijj
	movq	$0, 88(%rbx)
	movq	-472(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6710UCharsTrie8Iterator4nextER10UErrorCode@PLT
	testb	%al, %al
	cmove	-464(%rbp), %r15
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L137:
	movq	-464(%rbp), %r15
	cmpq	%r15, %r12
	jne	.L97
	movq	%r15, %r12
.L116:
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-480(%rbp), %rdi
	call	_ZN6icu_6710UCharsTrie8IteratorD1Ev@PLT
	movq	-472(%rbp), %rdi
	call	_ZN6icu_6710UCharsTrie8IteratorD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L138
	addq	$456, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L112:
	.cfi_restore_state
	movl	12(%r12), %ecx
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L107:
	movl	12(%r15), %ecx
	jmp	.L108
.L138:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3277:
	.size	_ZN6icu_6711TailoredSet19compareContractionsEiPKDsS2_, .-_ZN6icu_6711TailoredSet19compareContractionsEiPKDsS2_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711TailoredSet7compareEijj
	.type	_ZN6icu_6711TailoredSet7compareEijj, @function
_ZN6icu_6711TailoredSet7compareEijj:
.LFB3275:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movzbl	%cl, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$200, %rsp
	movl	%ecx, -196(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	%dl, %eax
	movl	%eax, -200(%rbp)
	cmpb	$-65, %dl
	jbe	.L140
	movl	%edx, %eax
	andl	$15, %eax
	cmpl	$8, %eax
	je	.L191
	cmpl	$191, %r15d
	jbe	.L147
.L176:
	movl	-196(%rbp), %eax
	andl	$15, %eax
	cmpl	$8, %eax
	je	.L192
	cmpl	$191, -200(%rbp)
	jbe	.L173
	movl	%ebx, %eax
	andl	$15, %eax
	cmpl	$9, %eax
	je	.L193
.L175:
	movl	-196(%rbp), %eax
	andl	$15, %eax
.L173:
	cmpl	$9, %eax
	je	.L194
	cmpl	$191, -200(%rbp)
	ja	.L159
.L158:
	cmpl	$191, %r15d
	jbe	.L170
.L160:
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6711TailoredSet3addEi
.L139:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L195
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L140:
	.cfi_restore_state
	cmpl	$191, %r15d
	ja	.L176
.L156:
	cmpl	$191, -200(%rbp)
	ja	.L160
.L170:
	cmpl	%ebx, -196(%rbp)
	je	.L139
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L191:
	movq	(%rdi), %rdi
	shrl	$13, %ebx
	movq	24(%rdi), %rax
	leaq	(%rax,%rbx,2), %r14
	movzwl	(%r14), %esi
	movzwl	2(%r14), %eax
	addq	$4, %r14
	sall	$16, %esi
	orl	%eax, %esi
	call	_ZNK6icu_6713CollationData12getFinalCE32Ej@PLT
	movl	%eax, %ebx
	movzbl	%al, %eax
	movl	%eax, -200(%rbp)
	cmpl	$191, %r15d
	jbe	.L143
	movl	-196(%rbp), %eax
	andl	$15, %eax
	cmpl	$8, %eax
	je	.L196
.L143:
	movq	(%r12), %rax
	movq	%r14, -184(%rbp)
	leaq	-176(%rbp), %r14
	xorl	%edx, %edx
	leaq	-184(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, -208(%rbp)
	leaq	96(%r12), %rax
	movq	%rax, %rcx
	movq	%rax, -216(%rbp)
	call	_ZN6icu_6710UCharsTrie8IteratorC1ENS_14ConstChar16PtrEiR10UErrorCode@PLT
	leaq	-136(%rbp), %rax
	movl	%r15d, -220(%rbp)
	movl	%ebx, -224(%rbp)
	movq	%rax, %r15
	movq	-216(%rbp), %rbx
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L197:
	movl	-68(%rbp), %r8d
	movl	%r13d, %ecx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	-208(%rbp), %rsi
	call	_ZN6icu_6711TailoredSet9addPrefixEPKNS_13CollationDataERKNS_13UnicodeStringEij
.L146:
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6710UCharsTrie8Iterator4nextER10UErrorCode@PLT
	testb	%al, %al
	jne	.L197
	movq	%r14, %rdi
	movl	-220(%rbp), %r15d
	movl	-224(%rbp), %ebx
	call	_ZN6icu_6710UCharsTrie8IteratorD1Ev@PLT
.L144:
	cmpl	$191, -200(%rbp)
	jbe	.L151
.L147:
	movl	%ebx, %eax
	andl	$15, %eax
	cmpl	$9, %eax
	je	.L198
.L151:
	cmpl	$191, %r15d
	ja	.L175
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L194:
	movq	8(%r12), %rdi
	movl	-196(%rbp), %eax
	movq	24(%rdi), %rdx
	shrl	$13, %eax
	leaq	(%rdx,%rax,2), %r14
	movzwl	(%r14), %esi
	movzwl	2(%r14), %eax
	sall	$16, %esi
	orl	%eax, %esi
	call	_ZNK6icu_6713CollationData12getFinalCE32Ej@PLT
	leaq	4(%r14), %rdx
	movl	%r13d, %esi
	movq	%r12, %rdi
	movl	%eax, %r15d
	movl	%eax, -196(%rbp)
	call	_ZN6icu_6711TailoredSet15addContractionsEiPKDs
	movzbl	%r15b, %r15d
.L155:
	cmpl	$191, -200(%rbp)
	jbe	.L158
	cmpl	$191, %r15d
	jbe	.L160
.L159:
	movl	-196(%rbp), %r15d
	movl	%ebx, %r14d
	andl	$15, %r14d
	andl	$15, %r15d
	cmpl	$14, %r15d
	jne	.L161
	cmpl	$1, %r14d
	jne	.L160
	movq	8(%r12), %rdx
	movl	-196(%rbp), %eax
	movl	%r13d, %edi
	movq	16(%rdx), %rdx
	shrl	$13, %eax
	movq	(%rdx,%rax,8), %rsi
	call	_ZN6icu_679Collation32getThreeBytePrimaryForOffsetDataEil@PLT
	movl	%eax, %r8d
	movl	%ebx, %eax
	xorb	%al, %al
	cmpl	%eax, %r8d
	jne	.L160
	.p2align 4,,10
	.p2align 3
.L161:
	cmpl	%r15d, %r14d
	jne	.L160
	cmpl	$5, %r14d
	je	.L199
	cmpl	$6, %r14d
	jne	.L168
	movl	-196(%rbp), %ecx
	movl	%ebx, %edx
	shrl	$8, %edx
	movl	%ecx, %eax
	andl	$31, %edx
	shrl	$8, %eax
	andl	$31, %eax
	cmpl	%edx, %eax
	jne	.L160
	testl	%eax, %eax
	je	.L139
	leal	-1(%rax), %edi
	movq	(%r12), %rax
	shrl	$13, %ebx
	movq	8(%r12), %rdx
	movq	16(%rax), %rax
	leaq	(%rax,%rbx,8), %rsi
	movl	%ecx, %eax
	shrl	$13, %eax
	movl	%eax, %ecx
	movq	16(%rdx), %rax
	leaq	(%rax,%rcx,8), %rcx
	xorl	%eax, %eax
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L200:
	leaq	1(%rax), %rdx
	cmpq	%rax, %rdi
	je	.L139
	movq	%rdx, %rax
.L169:
	movq	(%rcx,%rax,8), %rdx
	cmpq	%rdx, (%rsi,%rax,8)
	je	.L200
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L168:
	cmpl	$12, %r14d
	jne	.L170
	leal	-44032(%r13), %eax
	movl	$2, %r14d
	movslq	%eax, %rbx
	movl	%eax, %ecx
	imulq	$-1840700269, %rbx, %rbx
	sarl	$31, %ecx
	shrq	$32, %rbx
	addl	%eax, %ebx
	sarl	$4, %ebx
	movl	%ebx, %edx
	subl	%ecx, %ebx
	movslq	%ebx, %r15
	subl	%ecx, %edx
	movl	%ebx, %ecx
	imulq	$818089009, %r15, %rsi
	sarl	$31, %ecx
	imull	$28, %edx, %edx
	sarq	$34, %rsi
	subl	%ecx, %esi
	addw	$4352, %si
	subl	%edx, %eax
	jne	.L201
.L171:
	movq	16(%r12), %rdi
	movzwl	%si, %esi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	jne	.L160
	imulq	$818089009, %r15, %r15
	movl	%ebx, %eax
	movq	16(%r12), %rdi
	sarl	$31, %eax
	sarq	$34, %r15
	subl	%eax, %r15d
	leal	(%r15,%r15,4), %eax
	leal	(%r15,%rax,4), %eax
	subl	%eax, %ebx
	leal	4449(%rbx), %esi
	movzwl	%si, %esi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	jne	.L160
	cmpl	$3, %r14d
	jne	.L139
	movzwl	-226(%rbp), %esi
	movq	16(%r12), %rdi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	jne	.L160
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L199:
	movl	-196(%rbp), %ecx
	movl	%ebx, %edx
	shrl	$8, %edx
	movl	%ecx, %eax
	andl	$31, %edx
	shrl	$8, %eax
	andl	$31, %eax
	cmpl	%edx, %eax
	jne	.L160
	testl	%eax, %eax
	je	.L139
	leal	-1(%rax), %edi
	movq	(%r12), %rax
	shrl	$13, %ebx
	movq	8(%r12), %rdx
	movq	8(%rax), %rax
	leaq	(%rax,%rbx,4), %rsi
	movl	%ecx, %eax
	shrl	$13, %eax
	movl	%eax, %ecx
	movq	8(%rdx), %rax
	leaq	(%rax,%rcx,4), %rcx
	xorl	%eax, %eax
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L202:
	leaq	1(%rax), %rdx
	cmpq	%rdi, %rax
	je	.L139
	movq	%rdx, %rax
.L167:
	movl	(%rcx,%rax,4), %ebx
	cmpl	%ebx, (%rsi,%rax,4)
	je	.L202
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L192:
	movq	8(%r12), %rdi
	movl	-196(%rbp), %eax
	leaq	96(%r12), %r15
	movq	24(%rdi), %rdx
	shrl	$13, %eax
	leaq	(%rdx,%rax,2), %r14
	movzwl	(%r14), %esi
	movzwl	2(%r14), %eax
	addq	$4, %r14
	sall	$16, %esi
	orl	%eax, %esi
	call	_ZNK6icu_6713CollationData12getFinalCE32Ej@PLT
	movq	%r14, -184(%rbp)
	leaq	-176(%rbp), %r14
	xorl	%edx, %edx
	movl	%eax, -196(%rbp)
	movq	8(%r12), %rax
	movq	%r15, %rcx
	movq	%r14, %rdi
	leaq	-184(%rbp), %rsi
	movq	%rax, -208(%rbp)
	call	_ZN6icu_6710UCharsTrie8IteratorC1ENS_14ConstChar16PtrEiR10UErrorCode@PLT
	leaq	-136(%rbp), %rax
	movl	%ebx, -216(%rbp)
	movq	%rax, %rbx
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L203:
	movl	-68(%rbp), %r8d
	movl	%r13d, %ecx
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	-208(%rbp), %rsi
	call	_ZN6icu_6711TailoredSet9addPrefixEPKNS_13CollationDataERKNS_13UnicodeStringEij
.L150:
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6710UCharsTrie8Iterator4nextER10UErrorCode@PLT
	testb	%al, %al
	jne	.L203
	movq	%r14, %rdi
	movl	-216(%rbp), %ebx
	call	_ZN6icu_6710UCharsTrie8IteratorD1Ev@PLT
	movzbl	-196(%rbp), %r15d
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L198:
	movl	$1, -200(%rbp)
	movq	(%r12), %rdi
	movl	%ebx, %eax
	shrl	$13, %eax
	movq	24(%rdi), %rdx
	leaq	(%rdx,%rax,2), %r14
	movl	%ebx, %eax
	movl	$1, %ebx
	andl	$256, %eax
	je	.L178
.L152:
	cmpl	$191, %r15d
	ja	.L177
.L153:
	leaq	4(%r14), %rdx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6711TailoredSet15addContractionsEiPKDs
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L193:
	movl	$1, -200(%rbp)
	movq	(%r12), %rdi
	movl	%ebx, %eax
	shrl	$13, %eax
	movq	24(%rdi), %rdx
	leaq	(%rdx,%rax,2), %r14
	movl	%ebx, %eax
	movl	$1, %ebx
	andl	$256, %eax
	je	.L178
.L177:
	movl	-196(%rbp), %eax
	andl	$15, %eax
	cmpl	$9, %eax
	jne	.L153
	movl	-196(%rbp), %esi
	movq	8(%r12), %rdi
	movl	$1, -196(%rbp)
	movl	$1, %r15d
	movq	24(%rdi), %rdx
	movl	%esi, %eax
	shrl	$13, %eax
	leaq	(%rdx,%rax,2), %rcx
	movl	%esi, %eax
	andl	$256, %eax
	je	.L204
.L154:
	addq	$4, %rcx
	leaq	4(%r14), %rdx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6711TailoredSet19compareContractionsEiPKDsS2_
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L178:
	movzwl	(%r14), %esi
	movzwl	2(%r14), %eax
	sall	$16, %esi
	orl	%eax, %esi
	call	_ZNK6icu_6713CollationData12getFinalCE32Ej@PLT
	movl	%eax, %ebx
	movzbl	%al, %eax
	movl	%eax, -200(%rbp)
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L201:
	leal	4519(%rax), %eax
	movl	$3, %r14d
	movw	%ax, -226(%rbp)
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L196:
	movq	8(%r12), %rdi
	movl	-196(%rbp), %eax
	movq	24(%rdi), %rdx
	shrl	$13, %eax
	leaq	(%rdx,%rax,2), %r15
	movzwl	(%r15), %esi
	movzwl	2(%r15), %eax
	sall	$16, %esi
	orl	%eax, %esi
	call	_ZNK6icu_6713CollationData12getFinalCE32Ej@PLT
	leaq	4(%r15), %rcx
	movq	%r14, %rdx
	movl	%r13d, %esi
	movq	%r12, %rdi
	movl	%eax, -196(%rbp)
	call	_ZN6icu_6711TailoredSet15comparePrefixesEiPKDsS2_
	movzbl	-196(%rbp), %r15d
	jmp	.L144
.L204:
	movzwl	(%rcx), %esi
	movzwl	2(%rcx), %eax
	movq	%rcx, -208(%rbp)
	sall	$16, %esi
	orl	%eax, %esi
	call	_ZNK6icu_6713CollationData12getFinalCE32Ej@PLT
	movq	-208(%rbp), %rcx
	movl	%eax, -196(%rbp)
	movzbl	%al, %r15d
	jmp	.L154
.L195:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3275:
	.size	_ZN6icu_6711TailoredSet7compareEijj, .-_ZN6icu_6711TailoredSet7compareEijj
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711TailoredSet10handleCE32Eiij
	.type	_ZN6icu_6711TailoredSet10handleCE32Eiij, @function
_ZN6icu_6711TailoredSet10handleCE32Eiij:
.LFB3274:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movzbl	%cl, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movl	%ecx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	%edx, -52(%rbp)
	cmpb	$-65, %cl
	ja	.L250
.L206:
	movl	%r12d, %eax
	andl	$15, %eax
	leal	-1(%rax), %edx
	cmpl	$1, %edx
	setbe	%r15b
	cmpl	$4, %eax
	sete	%al
	orl	%eax, %r15d
	cmpl	$56319, %r14d
	jg	.L229
	movl	-52(%rbp), %edx
	movl	$56319, %eax
	cmpl	$56319, %edx
	cmovle	%edx, %eax
	movl	%eax, -56(%rbp)
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L252:
	movl	%r14d, %eax
	movq	(%rcx), %rcx
	sarl	$5, %eax
	cltq
	movzwl	(%rcx,%rax,2), %ecx
.L247:
	movl	%r14d, %eax
	andl	$31, %eax
	leal	(%rax,%rcx,4), %eax
	cltq
	salq	$2, %rax
.L211:
	movl	(%rdx,%rax), %esi
	call	_ZNK6icu_6713CollationData12getFinalCE32Ej@PLT
	movl	%eax, %ecx
	cmpl	$191, %r13d
	jbe	.L214
	testb	%r15b, %r15b
	je	.L215
.L214:
	cmpb	$-65, %cl
	jbe	.L216
	movl	%ecx, %eax
	andl	$15, %eax
	leal	-1(%rax), %edx
	cmpl	$1, %edx
	jbe	.L216
	cmpl	$4, %eax
	jne	.L215
.L216:
	cmpl	%r12d, %ecx
	je	.L217
	movq	16(%rbx), %rdi
	movl	%r14d, %esi
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
.L217:
	addl	$1, %r14d
	cmpl	-56(%rbp), %r14d
	jg	.L251
.L218:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rcx
	movq	16(%rcx), %rdx
	cmpl	$55295, %r14d
	jbe	.L252
	cmpl	$65535, %r14d
	ja	.L212
	movl	%r14d, %eax
	movq	(%rcx), %rcx
	sarl	$5, %eax
	addl	$320, %eax
	cltq
	movzwl	(%rcx,%rax,2), %ecx
	jmp	.L247
	.p2align 4,,10
	.p2align 3
.L220:
	cmpl	$65535, %r14d
	jbe	.L249
	movl	$512, %eax
	cmpl	$1114111, %r14d
	ja	.L221
	cmpl	%r14d, 44(%rcx)
	jg	.L223
	movslq	48(%rcx), %rax
	salq	$2, %rax
	.p2align 4,,10
	.p2align 3
.L221:
	movl	(%rdx,%rax), %esi
	call	_ZNK6icu_6713CollationData12getFinalCE32Ej@PLT
	movl	%eax, %ecx
	cmpl	$191, %r13d
	jbe	.L224
	testb	%r15b, %r15b
	je	.L225
.L224:
	cmpb	$-65, %cl
	jbe	.L226
	movl	%ecx, %eax
	andl	$15, %eax
	leal	-1(%rax), %edx
	cmpl	$1, %edx
	jbe	.L226
	cmpl	$4, %eax
	jne	.L225
.L226:
	cmpl	%ecx, %r12d
	je	.L228
	movq	16(%rbx), %rdi
	movl	%r14d, %esi
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
.L228:
	addl	$1, %r14d
	cmpl	%r14d, -52(%rbp)
	jl	.L230
.L229:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rcx
	movq	16(%rcx), %rdx
	cmpl	$55295, %r14d
	ja	.L220
.L249:
	movl	%r14d, %eax
	movq	(%rcx), %rcx
	sarl	$5, %eax
	cltq
	movzwl	(%rcx,%rax,2), %ecx
.L248:
	movl	%r14d, %eax
	andl	$31, %eax
	leal	(%rax,%rcx,4), %eax
	cltq
	salq	$2, %rax
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L225:
	movl	%r14d, %esi
	movl	%r12d, %edx
	movq	%rbx, %rdi
	addl	$1, %r14d
	call	_ZN6icu_6711TailoredSet7compareEijj
	cmpl	%r14d, -52(%rbp)
	jge	.L229
.L230:
	movl	96(%rbx), %eax
	testl	%eax, %eax
	setle	%al
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L215:
	.cfi_restore_state
	movl	%r14d, %esi
	movl	%r12d, %edx
	movq	%rbx, %rdi
	addl	$1, %r14d
	call	_ZN6icu_6711TailoredSet7compareEijj
	cmpl	-56(%rbp), %r14d
	jle	.L218
.L251:
	cmpl	-52(%rbp), %r14d
	jle	.L229
	jmp	.L230
	.p2align 4,,10
	.p2align 3
.L212:
	movl	$512, %eax
	cmpl	$1114111, %r14d
	ja	.L211
	cmpl	44(%rcx), %r14d
	jl	.L213
	movslq	48(%rcx), %rax
	salq	$2, %rax
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L213:
	movl	%r14d, %eax
	movq	(%rcx), %rsi
	sarl	$11, %eax
	addl	$2080, %eax
	cltq
	movzwl	(%rsi,%rax,2), %ecx
	movl	%r14d, %eax
	sarl	$5, %eax
	andl	$63, %eax
	addl	%ecx, %eax
	cltq
	movzwl	(%rsi,%rax,2), %ecx
	jmp	.L247
	.p2align 4,,10
	.p2align 3
.L223:
	movl	%r14d, %eax
	movq	(%rcx), %rsi
	sarl	$11, %eax
	addl	$2080, %eax
	cltq
	movzwl	(%rsi,%rax,2), %ecx
	movl	%r14d, %eax
	sarl	$5, %eax
	andl	$63, %eax
	addl	%ecx, %eax
	cltq
	movzwl	(%rsi,%rax,2), %ecx
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L250:
	movq	(%rdi), %rdi
	movl	%ecx, %esi
	call	_ZNK6icu_6713CollationData15getIndirectCE32Ej@PLT
	movl	%eax, %r12d
	movzbl	%al, %r13d
	cmpl	$192, %eax
	jne	.L206
	jmp	.L230
	.cfi_endproc
.LFE3274:
	.size	_ZN6icu_6711TailoredSet10handleCE32Eiij, .-_ZN6icu_6711TailoredSet10handleCE32Eiij
	.p2align 4
	.type	enumTailoredRange, @function
enumTailoredRange:
.LFB3272:
	.cfi_startproc
	endbr64
	cmpl	$192, %ecx
	je	.L254
	jmp	_ZN6icu_6711TailoredSet10handleCE32Eiij
	.p2align 4,,10
	.p2align 3
.L254:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3272:
	.size	enumTailoredRange, .-enumTailoredRange
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711TailoredSet15comparePrefixesEiPKDsS2_
	.type	_ZN6icu_6711TailoredSet15comparePrefixesEiPKDsS2_, @function
_ZN6icu_6711TailoredSet15comparePrefixesEiPKDsS2_:
.LFB3276:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-304(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	96(%rdi), %r13
	pushq	%r12
	movq	%r13, %rcx
	.cfi_offset 12, -48
	movl	%esi, %r12d
	leaq	-376(%rbp), %rsi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r14, %rdi
	subq	$376, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdx, -376(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -392(%rbp)
	call	_ZN6icu_6710UCharsTrie8IteratorC1ENS_14ConstChar16PtrEiR10UErrorCode@PLT
	movq	-392(%rbp), %rsi
	leaq	-176(%rbp), %rdi
	movq	%r13, %rcx
	xorl	%edx, %edx
	movq	%r15, -376(%rbp)
	movq	%rdi, -408(%rbp)
	call	_ZN6icu_6710UCharsTrie8IteratorC1ENS_14ConstChar16PtrEiR10UErrorCode@PLT
	leaq	-368(%rbp), %r15
	movl	$65535, %esi
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringC1EDs@PLT
	xorl	%r10d, %r10d
	movq	%r15, -392(%rbp)
	leaq	-264(%rbp), %rax
	movq	%r10, %r15
	movq	%rax, -400(%rbp)
	testq	%r15, %r15
	je	.L306
.L257:
	movq	-408(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6710UCharsTrie8Iterator4nextER10UErrorCode@PLT
	testb	%al, %al
	je	.L258
.L268:
	movswl	-128(%rbp), %ecx
	movswl	8(%r15), %edx
	movl	%ecx, %esi
	movl	%edx, %eax
	sarl	$5, %ecx
	testw	%si, %si
	cmovs	-124(%rbp), %ecx
	sarl	$5, %edx
	testw	%ax, %ax
	jns	.L262
	movl	12(%r15), %edx
.L262:
	testb	$1, %sil
	jne	.L288
	testl	%ecx, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	cmovle	%ecx, %r8d
	js	.L264
	movl	%ecx, %r9d
	subl	%r8d, %r9d
	cmpl	%ecx, %r9d
	cmovg	%ecx, %r9d
.L264:
	andl	$2, %esi
	leaq	-126(%rbp), %rcx
	cmove	-112(%rbp), %rcx
	movq	%r15, %rdi
	xorl	%esi, %esi
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	movsbl	%al, %eax
	testl	%eax, %eax
	jns	.L291
	movl	-196(%rbp), %r8d
	movq	(%rbx), %rsi
	movq	%r15, %rdx
	movl	%r12d, %ecx
	movq	%rbx, %rdi
	call	_ZN6icu_6711TailoredSet9addPrefixEPKNS_13CollationDataERKNS_13UnicodeStringEij
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6710UCharsTrie8Iterator4nextER10UErrorCode@PLT
	movq	-392(%rbp), %r15
	testb	%al, %al
	cmovne	-400(%rbp), %r15
	jmp	.L268
	.p2align 4,,10
	.p2align 3
.L288:
	leaq	-136(%rbp), %rdx
.L263:
	notl	%eax
	andl	$1, %eax
.L266:
	testl	%eax, %eax
	je	.L278
	movq	8(%rbx), %rsi
	movl	-68(%rbp), %r8d
	movl	%r12d, %ecx
	movq	%rbx, %rdi
	call	_ZN6icu_6711TailoredSet9addPrefixEPKNS_13CollationDataERKNS_13UnicodeStringEij
	testq	%r15, %r15
	jne	.L257
.L306:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6710UCharsTrie8Iterator4nextER10UErrorCode@PLT
	movq	-400(%rbp), %r15
	testb	%al, %al
	cmove	-392(%rbp), %r15
	jmp	.L257
	.p2align 4,,10
	.p2align 3
.L278:
	leaq	24(%rbx), %rdi
	movq	%r15, %rsi
	movq	%rdi, -416(%rbp)
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movswl	32(%rbx), %edx
	movq	-416(%rbp), %rdi
	testw	%dx, %dx
	js	.L280
	sarl	$5, %edx
.L281:
	xorl	%esi, %esi
	call	_ZN6icu_6713UnicodeString9doReverseEii@PLT
	movl	-68(%rbp), %ecx
	movl	%r12d, %esi
	movq	%rbx, %rdi
	movl	-196(%rbp), %edx
	call	_ZN6icu_6711TailoredSet7compareEijj
	movzwl	32(%rbx), %eax
	testb	$1, %al
	je	.L282
	movl	$2, %eax
	movw	%ax, 32(%rbx)
.L305:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6710UCharsTrie8Iterator4nextER10UErrorCode@PLT
	testb	%al, %al
	je	.L283
	movq	-400(%rbp), %r15
	jmp	.L257
	.p2align 4,,10
	.p2align 3
.L291:
	leaq	-136(%rbp), %rdx
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L280:
	movl	36(%rbx), %edx
	jmp	.L281
	.p2align 4,,10
	.p2align 3
.L283:
	movq	-392(%rbp), %r15
	jmp	.L257
	.p2align 4,,10
	.p2align 3
.L282:
	andl	$31, %eax
	movw	%ax, 32(%rbx)
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L258:
	movq	-392(%rbp), %rax
	cmpq	%rax, %r15
	je	.L307
.L276:
	movzwl	-360(%rbp), %esi
	testw	%si, %si
	js	.L269
	movswl	%si, %ecx
	sarl	$5, %ecx
.L270:
	movzwl	8(%r15), %eax
	testw	%ax, %ax
	js	.L271
	movswl	%ax, %edx
	sarl	$5, %edx
.L272:
	testb	$1, %sil
	je	.L273
	movq	-392(%rbp), %rdx
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L307:
	movq	%rax, %r15
.L277:
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-408(%rbp), %rdi
	call	_ZN6icu_6710UCharsTrie8IteratorD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6710UCharsTrie8IteratorD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L308
	addq	$376, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L269:
	.cfi_restore_state
	movl	-356(%rbp), %ecx
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L273:
	testl	%ecx, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	cmovle	%ecx, %r8d
	js	.L274
	movl	%ecx, %r9d
	subl	%r8d, %r9d
	cmpl	%ecx, %r9d
	cmovg	%ecx, %r9d
.L274:
	andl	$2, %esi
	leaq	-358(%rbp), %rcx
	movq	%r15, %rdi
	cmove	-344(%rbp), %rcx
	xorl	%esi, %esi
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	movsbl	%al, %eax
	testl	%eax, %eax
	jns	.L295
	movl	-196(%rbp), %r8d
	movq	(%rbx), %rsi
	movq	%r15, %rdx
	movl	%r12d, %ecx
	movq	%rbx, %rdi
	call	_ZN6icu_6711TailoredSet9addPrefixEPKNS_13CollationDataERKNS_13UnicodeStringEij
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6710UCharsTrie8Iterator4nextER10UErrorCode@PLT
	movq	-400(%rbp), %r15
	testb	%al, %al
	jne	.L276
	movq	-392(%rbp), %r15
	jmp	.L277
	.p2align 4,,10
	.p2align 3
.L271:
	movl	12(%r15), %edx
	jmp	.L272
	.p2align 4,,10
	.p2align 3
.L295:
	movq	-392(%rbp), %rdx
	jmp	.L266
.L308:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3276:
	.size	_ZN6icu_6711TailoredSet15comparePrefixesEiPKDsS2_, .-_ZN6icu_6711TailoredSet15comparePrefixesEiPKDsS2_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725ContractionsAndExpansions13addExpansionsEii
	.type	_ZN6icu_6725ContractionsAndExpansions13addExpansionsEii, @function
_ZN6icu_6725ContractionsAndExpansions13addExpansionsEii:
.LFB3293:
	.cfi_startproc
	endbr64
	movswl	448(%rdi), %eax
	movq	16(%rdi), %rcx
	shrl	$5, %eax
	jne	.L310
	cmpq	$0, 504(%rdi)
	je	.L318
.L310:
	testq	%rcx, %rcx
	je	.L309
	jmp	_ZN6icu_6725ContractionsAndExpansions10addStringsEiiPNS_10UnicodeSetE.part.0
	.p2align 4,,10
	.p2align 3
.L309:
	ret
	.p2align 4,,10
	.p2align 3
.L318:
	testq	%rcx, %rcx
	je	.L309
	movq	%rcx, %rdi
	jmp	_ZN6icu_6710UnicodeSet3addEii@PLT
	.cfi_endproc
.LFE3293:
	.size	_ZN6icu_6725ContractionsAndExpansions13addExpansionsEii, .-_ZN6icu_6725ContractionsAndExpansions13addExpansionsEii
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725ContractionsAndExpansions10handleCE32Eiij
	.type	_ZN6icu_6725ContractionsAndExpansions10handleCE32Eiij, @function
_ZN6icu_6725ContractionsAndExpansions10handleCE32Eiij:
.LFB3290:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$488, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$-65, %cl
	jbe	.L320
	movl	%esi, %r13d
	movl	%edx, %r12d
	leaq	.L325(%rip), %rdi
.L321:
	movl	%ecx, %eax
	andl	$15, %eax
	cmpl	$13, %eax
	ja	.L319
	movslq	(%rdi,%rax,4), %rax
	addq	%rdi, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L325:
	.long	.L319-.L325
	.long	.L335-.L325
	.long	.L334-.L325
	.long	.L324-.L325
	.long	.L333-.L325
	.long	.L332-.L325
	.long	.L331-.L325
	.long	.L324-.L325
	.long	.L330-.L325
	.long	.L329-.L325
	.long	.L328-.L325
	.long	.L327-.L325
	.long	.L326-.L325
	.long	.L324-.L325
	.text
	.p2align 4,,10
	.p2align 3
.L333:
	movq	24(%r15), %rdi
	testq	%rdi, %rdi
	je	.L349
	movq	%rcx, %rax
	leaq	512(%r15), %rsi
	movabsq	$-72057594037927936, %rdx
	salq	$32, %rax
	andq	%rdx, %rax
	movl	%ecx, %edx
	sall	$16, %ecx
	shrl	$8, %edx
	andl	$-16777216, %ecx
	andl	$65280, %edx
	orl	$1280, %ecx
	orq	%rdx, %rax
	movq	%rcx, %xmm3
	movl	$2, %edx
	orq	$83886080, %rax
	movq	%rax, %xmm0
	movq	(%rdi), %rax
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 512(%r15)
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L349:
	movswl	448(%r15), %eax
	shrl	$5, %eax
	jne	.L319
	cmpq	$0, 504(%r15)
	movq	16(%r15), %rcx
	je	.L412
	testq	%rcx, %rcx
	je	.L319
	movl	%r12d, %edx
	movl	%r13d, %esi
	movq	%r15, %rdi
	call	_ZN6icu_6725ContractionsAndExpansions10addStringsEiiPNS_10UnicodeSetE.part.0
	.p2align 4,,10
	.p2align 3
.L319:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L413
	addq	$488, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L324:
	.cfi_restore_state
	movl	760(%r15), %esi
	testl	%esi, %esi
	jg	.L319
	movl	$5, 760(%r15)
	jmp	.L319
	.p2align 4,,10
	.p2align 3
.L332:
	movq	24(%r15), %rdi
	testq	%rdi, %rdi
	je	.L349
	movq	(%r15), %rdx
	movl	%ecx, %eax
	shrl	$8, %ecx
	movl	$0, %esi
	movabsq	$-281474976710656, %r11
	shrl	$13, %eax
	movq	8(%rdx), %rdx
	leaq	(%rdx,%rax,4), %r10
	movl	%ecx, %edx
	andl	$31, %edx
	leal	-1(%rdx), %r9d
	jne	.L344
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L341:
	subl	%ebx, %ecx
	andl	$15, %eax
	movq	%rcx, %rbx
	salq	$32, %rbx
	orq	$83887360, %rbx
	cmpl	$1, %eax
	cmove	%rbx, %rcx
.L342:
	movq	%rcx, 512(%r15,%rsi,8)
	leaq	1(%rsi), %rax
	cmpq	%rsi, %r9
	je	.L343
	movq	%rax, %rsi
.L344:
	movl	(%r10,%rsi,4), %eax
	movzbl	%al, %ebx
	movl	%eax, %ecx
	cmpb	$-65, %al
	ja	.L341
	sall	$16, %ecx
	salq	$32, %rax
	andl	$-16777216, %ecx
	andq	%r11, %rax
	sall	$8, %ebx
	orq	%rcx, %rax
	movl	%ebx, %ecx
	orq	%rax, %rcx
	jmp	.L342
	.p2align 4,,10
	.p2align 3
.L331:
	movq	24(%r15), %rdi
	testq	%rdi, %rdi
	je	.L349
	movq	(%r15), %rax
	movl	%ecx, %edx
	shrl	$13, %ecx
	shrl	$8, %edx
	movq	16(%rax), %rax
	andl	$31, %edx
	leaq	(%rax,%rcx,8), %rsi
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L330:
	movl	%r12d, %edx
	movl	%r13d, %esi
	movq	%r15, %rdi
	call	_ZN6icu_6725ContractionsAndExpansions14handlePrefixesEiij
	jmp	.L319
	.p2align 4,,10
	.p2align 3
.L329:
	movl	%r12d, %edx
	movl	%r13d, %esi
	movq	%r15, %rdi
	call	_ZN6icu_6725ContractionsAndExpansions18handleContractionsEiij
	jmp	.L319
	.p2align 4,,10
	.p2align 3
.L328:
	movq	(%r15), %rax
	shrl	$13, %ecx
	movq	8(%rax), %rax
	movl	(%rax,%rcx,4), %ecx
.L348:
	cmpb	$-65, %cl
	ja	.L321
	.p2align 4,,10
	.p2align 3
.L320:
	movq	24(%r15), %rdi
	testq	%rdi, %rdi
	je	.L319
	movabsq	$-281474976710656, %rsi
	movq	%rcx, %rax
	salq	$32, %rax
	andq	%rsi, %rax
	movl	%ecx, %esi
	sall	$16, %esi
	andl	$-16777216, %esi
	orq	%rsi, %rax
	movl	%ecx, %esi
	sall	$8, %esi
	andl	$65280, %esi
	orq	%rax, %rsi
	movq	(%rdi), %rax
	call	*16(%rax)
	jmp	.L319
	.p2align 4,,10
	.p2align 3
.L327:
	movq	(%r15), %rax
	movq	8(%rax), %rax
	movl	(%rax), %ecx
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L326:
	cmpq	$0, 24(%r15)
	je	.L349
	movq	(%r15), %rax
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	(%rax), %rdx
	movq	%rax, -464(%rbp)
	leaq	-432(%rbp), %rax
	movq	%rax, -448(%rbp)
	leaq	16+_ZTVN6icu_6722UTF16CollationIteratorE(%rip), %rax
	movq	%rdx, -472(%rbp)
	movl	$0, -456(%rbp)
	movl	$40, -440(%rbp)
	movb	$0, -436(%rbp)
	movl	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	movl	$-1, -96(%rbp)
	movb	$0, -92(%rbp)
	movq	%rax, -480(%rbp)
	movq	$0, -72(%rbp)
	movw	%cx, -482(%rbp)
	movups	%xmm0, -88(%rbp)
	cmpl	%r12d, %r13d
	jg	.L414
	leaq	760(%r15), %rax
	movl	%r13d, %ebx
	leaq	-480(%rbp), %r14
	movq	%rax, -504(%rbp)
	leaq	-482(%rbp), %rax
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm2
	movaps	%xmm2, -528(%rbp)
	jmp	.L353
	.p2align 4,,10
	.p2align 3
.L351:
	movq	24(%r15), %rdi
	leal	-1(%rax), %edx
	addl	$1, %ebx
	movq	-448(%rbp), %rsi
	movq	(%rdi), %rax
	call	*24(%rax)
	cmpl	%ebx, %r12d
	jl	.L352
.L353:
	movq	%r14, %rdi
	movw	%bx, -482(%rbp)
	call	_ZN6icu_6717CollationIterator5resetEv@PLT
	movdqa	-528(%rbp), %xmm1
	movq	%r14, %rdi
	movq	%r14, -72(%rbp)
	movq	-504(%rbp), %rsi
	movups	%xmm1, -88(%rbp)
	call	_ZN6icu_6717CollationIterator8fetchCEsER10UErrorCode@PLT
	movl	760(%r15), %edx
	testl	%edx, %edx
	jle	.L351
	movq	%r14, %rdi
	call	_ZN6icu_6722UTF16CollationIteratorD1Ev@PLT
	jmp	.L319
	.p2align 4,,10
	.p2align 3
.L335:
	movq	24(%r15), %rdi
	testq	%rdi, %rdi
	je	.L319
	movq	%rcx, %rsi
	movq	(%rdi), %rax
	movabsq	$-1099511627776, %rcx
	salq	$32, %rsi
	andq	%rcx, %rsi
	orq	$83887360, %rsi
	call	*16(%rax)
	jmp	.L319
	.p2align 4,,10
	.p2align 3
.L334:
	movq	24(%r15), %rdi
	testq	%rdi, %rdi
	je	.L319
	movq	(%rdi), %rax
	movl	%ecx, %esi
	andl	$-256, %esi
	call	*16(%rax)
	jmp	.L319
	.p2align 4,,10
	.p2align 3
.L343:
	movq	(%rdi), %rax
	leaq	512(%r15), %rsi
	call	*24(%rax)
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L412:
	testq	%rcx, %rcx
	je	.L319
	movl	%r12d, %edx
	movl	%r13d, %esi
	movq	%rcx, %rdi
	call	_ZN6icu_6710UnicodeSet3addEii@PLT
	jmp	.L319
.L414:
	leaq	-480(%rbp), %r14
	.p2align 4,,10
	.p2align 3
.L352:
	movq	%r14, %rdi
	call	_ZN6icu_6722UTF16CollationIteratorD1Ev@PLT
	jmp	.L349
.L413:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3290:
	.size	_ZN6icu_6725ContractionsAndExpansions10handleCE32Eiij, .-_ZN6icu_6725ContractionsAndExpansions10handleCE32Eiij
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725ContractionsAndExpansions12forCodePointEPKNS_13CollationDataEiR10UErrorCode
	.type	_ZN6icu_6725ContractionsAndExpansions12forCodePointEPKNS_13CollationDataEiR10UErrorCode, @function
_ZN6icu_6725ContractionsAndExpansions12forCodePointEPKNS_13CollationDataEiR10UErrorCode:
.LFB3289:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L434
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movl	%edx, %esi
	movl	%eax, 760(%rbx)
	movq	(%rdi), %rax
	movq	16(%rax), %rcx
	cmpl	$55295, %edx
	ja	.L417
	movq	(%rax), %rax
	sarl	$5, %edx
	movl	%esi, %r8d
	movslq	%edx, %rdx
	andl	$31, %r8d
	movzwl	(%rax,%rdx,2), %eax
	leal	(%r8,%rax,4), %eax
	cltq
	movl	(%rcx,%rax,4), %ecx
	cmpl	$192, %ecx
	je	.L437
.L419:
	movq	%rdi, (%rbx)
	movl	%esi, %edx
	movq	%rbx, %rdi
	call	_ZN6icu_6725ContractionsAndExpansions10handleCE32Eiij
	movl	760(%rbx), %eax
	movl	%eax, (%r12)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L417:
	.cfi_restore_state
	cmpl	$65535, %edx
	jbe	.L438
	cmpl	$1114111, %edx
	ja	.L423
	cmpl	44(%rax), %edx
	jge	.L439
	movq	(%rax), %r8
	movl	%edx, %eax
	sarl	$11, %eax
	addl	$2080, %eax
	cltq
	movzwl	(%r8,%rax,2), %edx
	movl	%esi, %eax
	sarl	$5, %eax
	andl	$63, %eax
	addl	%edx, %eax
	cltq
	movzwl	(%r8,%rax,2), %edx
	movl	%esi, %eax
	andl	$31, %eax
	leal	(%rax,%rdx,4), %eax
	cltq
	salq	$2, %rax
.L425:
	movl	(%rcx,%rax), %ecx
	cmpl	$192, %ecx
	jne	.L419
	movq	32(%rdi), %rdi
	movq	(%rdi), %rax
	movq	16(%rax), %rcx
	cmpl	44(%rax), %esi
	jl	.L440
	movslq	48(%rax), %rax
	salq	$2, %rax
	jmp	.L426
	.p2align 4,,10
	.p2align 3
.L434:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.p2align 4,,10
	.p2align 3
.L438:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	cmpl	$56320, %edx
	movq	(%rax), %r9
	movl	$0, %edx
	movl	%esi, %r8d
	movl	$320, %eax
	cmovl	%eax, %edx
	movl	%esi, %eax
	andl	$31, %r8d
	sarl	$5, %eax
	addl	%edx, %eax
	cltq
	movzwl	(%r9,%rax,2), %edx
	leal	(%r8,%rdx,4), %edx
	movslq	%edx, %rdx
	movl	(%rcx,%rdx,4), %ecx
	cmpl	$192, %ecx
	jne	.L419
	movq	32(%rdi), %rdi
	movq	(%rdi), %rdx
	movq	16(%rdx), %rcx
	movq	(%rdx), %rdx
	movzwl	(%rdx,%rax,2), %eax
	leal	(%r8,%rax,4), %eax
	cltq
	salq	$2, %rax
	jmp	.L426
	.p2align 4,,10
	.p2align 3
.L437:
	movq	32(%rdi), %rdi
	movq	(%rdi), %rax
	movq	16(%rax), %rcx
	movq	(%rax), %rax
	movzwl	(%rax,%rdx,2), %eax
	leal	(%r8,%rax,4), %eax
	cltq
	salq	$2, %rax
.L426:
	movl	(%rcx,%rax), %ecx
	jmp	.L419
	.p2align 4,,10
	.p2align 3
.L423:
	movl	512(%rcx), %ecx
	cmpl	$192, %ecx
	jne	.L419
	movq	32(%rdi), %rdi
	movq	(%rdi), %rax
	movq	16(%rax), %rcx
	movl	$512, %eax
	jmp	.L426
	.p2align 4,,10
	.p2align 3
.L439:
	movslq	48(%rax), %rax
	salq	$2, %rax
	jmp	.L425
	.p2align 4,,10
	.p2align 3
.L440:
	movq	(%rax), %r8
	movl	%esi, %eax
	sarl	$11, %eax
	addl	$2080, %eax
	cltq
	movzwl	(%r8,%rax,2), %edx
	movl	%esi, %eax
	sarl	$5, %eax
	andl	$63, %eax
	addl	%edx, %eax
	cltq
	movzwl	(%r8,%rax,2), %edx
	movl	%esi, %eax
	andl	$31, %eax
	leal	(%rax,%rdx,4), %eax
	cltq
	salq	$2, %rax
	jmp	.L426
	.cfi_endproc
.LFE3289:
	.size	_ZN6icu_6725ContractionsAndExpansions12forCodePointEPKNS_13CollationDataEiR10UErrorCode, .-_ZN6icu_6725ContractionsAndExpansions12forCodePointEPKNS_13CollationDataEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725ContractionsAndExpansions14handlePrefixesEiij
	.type	_ZN6icu_6725ContractionsAndExpansions14handlePrefixesEiij, @function
_ZN6icu_6725ContractionsAndExpansions14handlePrefixesEiij:
.LFB3291:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	shrl	$13, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$168, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	leaq	(%rax,%rcx,2), %r14
	movzwl	(%r14), %ecx
	movzwl	2(%r14), %eax
	sall	$16, %ecx
	orl	%eax, %ecx
	call	_ZN6icu_6725ContractionsAndExpansions10handleCE32Eiij
	cmpb	$0, 32(%rbx)
	jne	.L460
.L441:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L461
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L460:
	.cfi_restore_state
	leaq	-176(%rbp), %r15
	leaq	760(%rbx), %rcx
	addq	$4, %r14
	xorl	%edx, %edx
	leaq	-184(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rcx, -200(%rbp)
	movq	%r14, -184(%rbp)
	call	_ZN6icu_6710UCharsTrie8IteratorC1ENS_14ConstChar16PtrEiR10UErrorCode@PLT
	leaq	440(%rbx), %r14
	jmp	.L449
	.p2align 4,,10
	.p2align 3
.L462:
	sarl	$5, %edx
.L446:
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString9doReverseEii@PLT
	movq	8(%rbx), %rcx
	testq	%rcx, %rcx
	je	.L447
	movl	%r13d, %edx
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6725ContractionsAndExpansions10addStringsEiiPNS_10UnicodeSetE.part.0
.L447:
	movq	16(%rbx), %rcx
	testq	%rcx, %rcx
	je	.L448
	movl	%r13d, %edx
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6725ContractionsAndExpansions10addStringsEiiPNS_10UnicodeSetE.part.0
.L448:
	movl	-68(%rbp), %ecx
	movl	%r13d, %edx
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6725ContractionsAndExpansions10handleCE32Eiij
.L449:
	movq	-200(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6710UCharsTrie8Iterator4nextER10UErrorCode@PLT
	testb	%al, %al
	je	.L444
	leaq	-136(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movswl	448(%rbx), %edx
	testw	%dx, %dx
	jns	.L462
	movl	452(%rbx), %edx
	jmp	.L446
	.p2align 4,,10
	.p2align 3
.L444:
	movzwl	448(%rbx), %edx
	movq	%r15, %rdi
	movl	%edx, %eax
	andl	$31, %eax
	andl	$1, %edx
	movl	$2, %edx
	cmovne	%edx, %eax
	movw	%ax, 448(%rbx)
	call	_ZN6icu_6710UCharsTrie8IteratorD1Ev@PLT
	jmp	.L441
.L461:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3291:
	.size	_ZN6icu_6725ContractionsAndExpansions14handlePrefixesEiij, .-_ZN6icu_6725ContractionsAndExpansions14handlePrefixesEiij
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725ContractionsAndExpansions18handleContractionsEiij
	.type	_ZN6icu_6725ContractionsAndExpansions18handleContractionsEiij, @function
_ZN6icu_6725ContractionsAndExpansions18handleContractionsEiij:
.LFB3292:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$168, %rsp
	movq	(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	%ecx, %eax
	movq	24(%rdx), %rdx
	shrl	$13, %eax
	leaq	(%rdx,%rax,2), %r13
	andb	$1, %ch
	je	.L480
.L464:
	addq	$4, %r13
	leaq	760(%r15), %r14
	leaq	-184(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r13, -184(%rbp)
	leaq	-176(%rbp), %r13
	movq	%r14, %rcx
	movq	%r13, %rdi
	call	_ZN6icu_6710UCharsTrie8IteratorC1ENS_14ConstChar16PtrEiR10UErrorCode@PLT
	leaq	-136(%rbp), %rax
	movq	%rax, -200(%rbp)
	jmp	.L468
	.p2align 4,,10
	.p2align 3
.L481:
	movq	-200(%rbp), %rax
	movq	8(%r15), %rcx
	movq	%rax, 504(%r15)
	testq	%rcx, %rcx
	je	.L466
	movl	%r12d, %edx
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	_ZN6icu_6725ContractionsAndExpansions10addStringsEiiPNS_10UnicodeSetE.part.0
.L466:
	movswl	448(%r15), %eax
	shrl	$5, %eax
	je	.L467
	movq	16(%r15), %rcx
	testq	%rcx, %rcx
	je	.L467
	movl	%r12d, %edx
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	_ZN6icu_6725ContractionsAndExpansions10addStringsEiiPNS_10UnicodeSetE.part.0
.L467:
	movl	-68(%rbp), %ecx
	movl	%r12d, %edx
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	_ZN6icu_6725ContractionsAndExpansions10handleCE32Eiij
.L468:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6710UCharsTrie8Iterator4nextER10UErrorCode@PLT
	testb	%al, %al
	jne	.L481
	movq	$0, 504(%r15)
	movq	%r13, %rdi
	call	_ZN6icu_6710UCharsTrie8IteratorD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L482
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L480:
	.cfi_restore_state
	movzwl	0(%r13), %ecx
	movzwl	2(%r13), %eax
	movl	%r12d, %edx
	sall	$16, %ecx
	orl	%eax, %ecx
	call	_ZN6icu_6725ContractionsAndExpansions10handleCE32Eiij
	jmp	.L464
.L482:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3292:
	.size	_ZN6icu_6725ContractionsAndExpansions18handleContractionsEiij, .-_ZN6icu_6725ContractionsAndExpansions18handleContractionsEiij
	.p2align 4
	.type	enumCnERange, @function
enumCnERange:
.LFB3287:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 33(%rdi)
	je	.L488
	js	.L500
	leaq	40(%rdi), %r8
	cmpl	%edx, %esi
	je	.L501
	movq	%r8, %rdi
	movq	%r8, -56(%rbp)
	call	_ZNK6icu_6710UnicodeSet12containsNoneEii@PLT
	testb	%al, %al
	je	.L502
.L488:
	movl	%r13d, %ecx
	movl	%r14d, %edx
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6725ContractionsAndExpansions10handleCE32Eiij
.L499:
	movl	760(%rbx), %eax
	testl	%eax, %eax
	setle	%al
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L500:
	.cfi_restore_state
	cmpl	$192, %ecx
	je	.L490
	leaq	40(%rdi), %rdi
	call	_ZN6icu_6710UnicodeSet3addEii@PLT
	jmp	.L488
	.p2align 4,,10
	.p2align 3
.L501:
	movq	%r8, %rdi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	je	.L488
.L490:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L502:
	.cfi_restore_state
	leaq	240(%rbx), %r15
	movl	%r14d, %edx
	movl	%r12d, %esi
	xorl	%r12d, %r12d
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSet3setEii@PLT
	movq	-56(%rbp), %r8
	movq	%rax, %rdi
	movq	%r8, %rsi
	call	_ZN6icu_6710UnicodeSet9removeAllERKS0_@PLT
	movq	%r15, %rdi
	call	_ZNK6icu_6710UnicodeSet13getRangeCountEv@PLT
	movl	%eax, -56(%rbp)
	testl	%eax, %eax
	jle	.L499
	.p2align 4,,10
	.p2align 3
.L491:
	movl	%r12d, %esi
	movq	%r15, %rdi
	call	_ZNK6icu_6710UnicodeSet11getRangeEndEi@PLT
	movl	%r12d, %esi
	movq	%r15, %rdi
	addl	$1, %r12d
	movl	%eax, %r14d
	call	_ZNK6icu_6710UnicodeSet13getRangeStartEi@PLT
	movl	%r13d, %ecx
	movl	%r14d, %edx
	movq	%rbx, %rdi
	movl	%eax, %esi
	call	_ZN6icu_6725ContractionsAndExpansions10handleCE32Eiij
	cmpl	%r12d, -56(%rbp)
	jne	.L491
	jmp	.L499
	.cfi_endproc
.LFE3287:
	.size	enumCnERange, .-enumCnERange
	.weak	_ZTSN6icu_677UMemoryE
	.section	.rodata._ZTSN6icu_677UMemoryE,"aG",@progbits,_ZTSN6icu_677UMemoryE,comdat
	.align 16
	.type	_ZTSN6icu_677UMemoryE, @object
	.size	_ZTSN6icu_677UMemoryE, 18
_ZTSN6icu_677UMemoryE:
	.string	"N6icu_677UMemoryE"
	.weak	_ZTIN6icu_677UMemoryE
	.section	.data.rel.ro._ZTIN6icu_677UMemoryE,"awG",@progbits,_ZTIN6icu_677UMemoryE,comdat
	.align 8
	.type	_ZTIN6icu_677UMemoryE, @object
	.size	_ZTIN6icu_677UMemoryE, 16
_ZTIN6icu_677UMemoryE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN6icu_677UMemoryE
	.weak	_ZTSN6icu_6725ContractionsAndExpansions6CESinkE
	.section	.rodata._ZTSN6icu_6725ContractionsAndExpansions6CESinkE,"aG",@progbits,_ZTSN6icu_6725ContractionsAndExpansions6CESinkE,comdat
	.align 32
	.type	_ZTSN6icu_6725ContractionsAndExpansions6CESinkE, @object
	.size	_ZTSN6icu_6725ContractionsAndExpansions6CESinkE, 44
_ZTSN6icu_6725ContractionsAndExpansions6CESinkE:
	.string	"N6icu_6725ContractionsAndExpansions6CESinkE"
	.weak	_ZTIN6icu_6725ContractionsAndExpansions6CESinkE
	.section	.data.rel.ro._ZTIN6icu_6725ContractionsAndExpansions6CESinkE,"awG",@progbits,_ZTIN6icu_6725ContractionsAndExpansions6CESinkE,comdat
	.align 8
	.type	_ZTIN6icu_6725ContractionsAndExpansions6CESinkE, @object
	.size	_ZTIN6icu_6725ContractionsAndExpansions6CESinkE, 24
_ZTIN6icu_6725ContractionsAndExpansions6CESinkE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6725ContractionsAndExpansions6CESinkE
	.quad	_ZTIN6icu_677UMemoryE
	.weak	_ZTVN6icu_6725ContractionsAndExpansions6CESinkE
	.section	.data.rel.ro._ZTVN6icu_6725ContractionsAndExpansions6CESinkE,"awG",@progbits,_ZTVN6icu_6725ContractionsAndExpansions6CESinkE,comdat
	.align 8
	.type	_ZTVN6icu_6725ContractionsAndExpansions6CESinkE, @object
	.size	_ZTVN6icu_6725ContractionsAndExpansions6CESinkE, 48
_ZTVN6icu_6725ContractionsAndExpansions6CESinkE:
	.quad	0
	.quad	_ZTIN6icu_6725ContractionsAndExpansions6CESinkE
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
