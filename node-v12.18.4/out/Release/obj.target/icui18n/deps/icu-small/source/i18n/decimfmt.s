	.file	"decimfmt.cpp"
	.text
	.section	.text._ZNK6icu_6712NumberFormat9isLenientEv,"axG",@progbits,_ZNK6icu_6712NumberFormat9isLenientEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6712NumberFormat9isLenientEv
	.type	_ZNK6icu_6712NumberFormat9isLenientEv, @function
_ZNK6icu_6712NumberFormat9isLenientEv:
.LFB2604:
	.cfi_startproc
	endbr64
	movzbl	341(%rdi), %eax
	ret
	.cfi_endproc
.LFE2604:
	.size	_ZNK6icu_6712NumberFormat9isLenientEv, .-_ZNK6icu_6712NumberFormat9isLenientEv
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713DecimalFormat17getDynamicClassIDEv
	.type	_ZNK6icu_6713DecimalFormat17getDynamicClassIDEv, @function
_ZNK6icu_6713DecimalFormat17getDynamicClassIDEv:
.LFB3887:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6713DecimalFormat16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3887:
	.size	_ZNK6icu_6713DecimalFormat17getDynamicClassIDEv, .-_ZNK6icu_6713DecimalFormat17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713DecimalFormat6formatEiRNS_13UnicodeStringERNS_13FieldPositionE
	.type	_ZNK6icu_6713DecimalFormat6formatEiRNS_13UnicodeStringERNS_13FieldPositionE, @function
_ZNK6icu_6713DecimalFormat6formatEiRNS_13UnicodeStringERNS_13FieldPositionE:
.LFB3944:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movslq	%esi, %rsi
	jmp	*112(%rax)
	.cfi_endproc
.LFE3944:
	.size	_ZNK6icu_6713DecimalFormat6formatEiRNS_13UnicodeStringERNS_13FieldPositionE, .-_ZNK6icu_6713DecimalFormat6formatEiRNS_13UnicodeStringERNS_13FieldPositionE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713DecimalFormat6formatEiRNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode
	.type	_ZNK6icu_6713DecimalFormat6formatEiRNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode, @function
_ZNK6icu_6713DecimalFormat6formatEiRNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode:
.LFB3946:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movslq	%esi, %rsi
	jmp	*128(%rax)
	.cfi_endproc
.LFE3946:
	.size	_ZNK6icu_6713DecimalFormat6formatEiRNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode, .-_ZNK6icu_6713DecimalFormat6formatEiRNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713DecimalFormat23getDecimalFormatSymbolsEv
	.type	_ZNK6icu_6713DecimalFormat23getDecimalFormatSymbolsEv, @function
_ZNK6icu_6713DecimalFormat23getDecimalFormatSymbolsEv:
.LFB3958:
	.cfi_startproc
	endbr64
	movq	360(%rdi), %rax
	testq	%rax, %rax
	je	.L6
	movq	768(%rax), %rax
.L6:
	ret
	.cfi_endproc
.LFE3958:
	.size	_ZNK6icu_6713DecimalFormat23getDecimalFormatSymbolsEv, .-_ZNK6icu_6713DecimalFormat23getDecimalFormatSymbolsEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713DecimalFormat21getCurrencyPluralInfoEv
	.type	_ZNK6icu_6713DecimalFormat21getCurrencyPluralInfoEv, @function
_ZNK6icu_6713DecimalFormat21getCurrencyPluralInfoEv:
.LFB3963:
	.cfi_startproc
	endbr64
	movq	360(%rdi), %rax
	testq	%rax, %rax
	je	.L11
	movq	56(%rax), %rax
.L11:
	ret
	.cfi_endproc
.LFE3963:
	.size	_ZNK6icu_6713DecimalFormat21getCurrencyPluralInfoEv, .-_ZNK6icu_6713DecimalFormat21getCurrencyPluralInfoEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713DecimalFormat14getPadPositionEv
	.type	_ZNK6icu_6713DecimalFormat14getPadPositionEv, @function
_ZNK6icu_6713DecimalFormat14getPadPositionEv:
.LFB3990:
	.cfi_startproc
	endbr64
	movq	360(%rdi), %rax
	xorl	%r8d, %r8d
	testq	%rax, %rax
	je	.L16
	cmpb	$0, 392(%rax)
	jne	.L16
	movl	396(%rax), %r8d
.L16:
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE3990:
	.size	_ZNK6icu_6713DecimalFormat14getPadPositionEv, .-_ZNK6icu_6713DecimalFormat14getPadPositionEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713DecimalFormateqERKNS_6FormatE
	.type	_ZNK6icu_6713DecimalFormateqERKNS_6FormatE, @function
_ZNK6icu_6713DecimalFormateqERKNS_6FormatE:
.LFB3934:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6713DecimalFormatE(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%rsi, %rdi
	leaq	_ZTIN6icu_676FormatE(%rip), %rsi
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	call	__dynamic_cast@PLT
	testq	%rax, %rax
	je	.L24
	movq	360(%r12), %rdi
	testq	%rdi, %rdi
	je	.L24
	movq	360(%rax), %rsi
	movq	%rax, %rbx
	xorl	%r13d, %r13d
	testq	%rsi, %rsi
	je	.L20
	addq	$8, %rsi
	addq	$8, %rdi
	xorl	%edx, %edx
	call	_ZNK6icu_676number4impl23DecimalFormatProperties7_equalsERKS2_b@PLT
	testb	%al, %al
	jne	.L30
.L20:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%r13d, %r13d
	popq	%rbx
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_restore_state
	movq	360(%rbx), %rax
	movq	768(%rax), %rsi
	movq	360(%r12), %rax
	movq	768(%rax), %rdi
	call	_ZNK6icu_6720DecimalFormatSymbolseqERKS0_@PLT
	testb	%al, %al
	setne	%r13b
	addq	$8, %rsp
	popq	%rbx
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3934:
	.size	_ZNK6icu_6713DecimalFormateqERKNS_6FormatE, .-_ZNK6icu_6713DecimalFormateqERKNS_6FormatE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713DecimalFormat21getPadCharacterStringEv
	.type	_ZNK6icu_6713DecimalFormat21getPadCharacterStringEv, @function
_ZNK6icu_6713DecimalFormat21getPadCharacterStringEv:
.LFB3988:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	360(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L32
	testb	$1, 408(%rsi)
	je	.L33
.L32:
	leaq	_ZN6icu_676number4implL22kFallbackPaddingStringE(%rip), %rax
	leaq	-32(%rbp), %rdx
	movl	$-1, %ecx
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, -32(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
.L31:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L40
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	.cfi_restore_state
	addq	$400, %rsi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	jmp	.L31
.L40:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3988:
	.size	_ZNK6icu_6713DecimalFormat21getPadCharacterStringEv, .-_ZNK6icu_6713DecimalFormat21getPadCharacterStringEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713DecimalFormat18toLocalizedPatternERNS_13UnicodeStringE
	.type	_ZNK6icu_6713DecimalFormat18toLocalizedPatternERNS_13UnicodeStringE, @function
_ZNK6icu_6713DecimalFormat18toLocalizedPatternERNS_13UnicodeStringE:
.LFB4015:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$96, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 360(%rdi)
	je	.L46
	leaq	16+_ZTVN6icu_679ErrorCodeE(%rip), %rax
	movq	%rdi, %rbx
	movl	$0, -120(%rbp)
	leaq	-112(%rbp), %r13
	movq	%rax, -128(%rbp)
	movq	(%rdi), %rax
	leaq	-128(%rbp), %r14
	call	*544(%rax)
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	leaq	-120(%rbp), %r8
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	360(%rbx), %rax
	movl	$1, %ecx
	movq	768(%rax), %rdx
	call	_ZN6icu_676number4impl18PatternStringUtils16convertLocalizedERKNS_13UnicodeStringERKNS_20DecimalFormatSymbolsEbR10UErrorCode@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_679ErrorCodeD1Ev@PLT
.L43:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L47
	addq	$96, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	.cfi_restore_state
	movq	%rsi, %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	jmp	.L43
.L47:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4015:
	.size	_ZNK6icu_6713DecimalFormat18toLocalizedPatternERNS_13UnicodeStringE, .-_ZNK6icu_6713DecimalFormat18toLocalizedPatternERNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713DecimalFormat20getRoundingIncrementEv
	.type	_ZNK6icu_6713DecimalFormat20getRoundingIncrementEv, @function
_ZNK6icu_6713DecimalFormat20getRoundingIncrementEv:
.LFB3982:
	.cfi_startproc
	endbr64
	movq	360(%rdi), %rax
	testq	%rax, %rax
	je	.L55
	movsd	3904(%rax), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN6icu_676number4impl23DecimalFormatProperties10getDefaultEv@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movsd	736(%rax), %xmm0
	ret
	.cfi_endproc
.LFE3982:
	.size	_ZNK6icu_6713DecimalFormat20getRoundingIncrementEv, .-_ZNK6icu_6713DecimalFormat20getRoundingIncrementEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713DecimalFormat14getFormatWidthEv
	.type	_ZNK6icu_6713DecimalFormat14getFormatWidthEv, @function
_ZNK6icu_6713DecimalFormat14getFormatWidthEv:
.LFB3986:
	.cfi_startproc
	endbr64
	movq	360(%rdi), %rax
	testq	%rax, %rax
	je	.L63
	movl	76(%rax), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L63:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN6icu_676number4impl23DecimalFormatProperties10getDefaultEv@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movl	68(%rax), %eax
	ret
	.cfi_endproc
.LFE3986:
	.size	_ZNK6icu_6713DecimalFormat14getFormatWidthEv, .-_ZNK6icu_6713DecimalFormat14getFormatWidthEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713DecimalFormat20isScientificNotationEv
	.type	_ZNK6icu_6713DecimalFormat20isScientificNotationEv, @function
_ZNK6icu_6713DecimalFormat20isScientificNotationEv:
.LFB3992:
	.cfi_startproc
	endbr64
	movq	360(%rdi), %rax
	testq	%rax, %rax
	je	.L71
	cmpl	$-1, 104(%rax)
	setne	%al
	ret
	.p2align 4,,10
	.p2align 3
.L71:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN6icu_676number4impl23DecimalFormatProperties10getDefaultEv@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	cmpl	$-1, 96(%rax)
	setne	%al
	ret
	.cfi_endproc
.LFE3992:
	.size	_ZNK6icu_6713DecimalFormat20isScientificNotationEv, .-_ZNK6icu_6713DecimalFormat20isScientificNotationEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713DecimalFormat24getMinimumExponentDigitsEv
	.type	_ZNK6icu_6713DecimalFormat24getMinimumExponentDigitsEv, @function
_ZNK6icu_6713DecimalFormat24getMinimumExponentDigitsEv:
.LFB3994:
	.cfi_startproc
	endbr64
	movq	360(%rdi), %rax
	testq	%rax, %rax
	je	.L79
	movzbl	104(%rax), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN6icu_676number4impl23DecimalFormatProperties10getDefaultEv@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movzbl	96(%rax), %eax
	ret
	.cfi_endproc
.LFE3994:
	.size	_ZNK6icu_6713DecimalFormat24getMinimumExponentDigitsEv, .-_ZNK6icu_6713DecimalFormat24getMinimumExponentDigitsEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713DecimalFormat25isExponentSignAlwaysShownEv
	.type	_ZNK6icu_6713DecimalFormat25isExponentSignAlwaysShownEv, @function
_ZNK6icu_6713DecimalFormat25isExponentSignAlwaysShownEv:
.LFB3996:
	.cfi_startproc
	endbr64
	movq	360(%rdi), %rax
	testq	%rax, %rax
	je	.L87
	movzbl	74(%rax), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L87:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN6icu_676number4impl23DecimalFormatProperties10getDefaultEv@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movzbl	66(%rax), %eax
	ret
	.cfi_endproc
.LFE3996:
	.size	_ZNK6icu_6713DecimalFormat25isExponentSignAlwaysShownEv, .-_ZNK6icu_6713DecimalFormat25isExponentSignAlwaysShownEv
	.align 2
	.p2align 4
	.type	_ZN6icu_6713DecimalFormat15setupFastFormatEv.part.0, @function
_ZN6icu_6713DecimalFormat15setupFastFormatEv.part.0:
.LFB5359:
	.cfi_startproc
	movq	360(%rdi), %rax
	movzwl	208(%rax), %edx
	testb	$1, %dl
	jne	.L89
	testw	%dx, %dx
	js	.L90
	movswl	%dx, %ecx
	sarl	$5, %ecx
.L91:
	cmpl	$1, %ecx
	je	.L136
.L106:
	movb	$0, 3928(%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L136:
	andl	$2, %edx
	leaq	210(%rax), %rdx
	jne	.L94
	movq	224(%rax), %rdx
.L94:
	cmpw	$45, (%rdx)
	jne	.L106
	.p2align 4,,10
	.p2align 3
.L89:
	movswl	336(%rax), %edx
	movswl	688(%rax), %ecx
	orl	%edx, %ecx
	movswl	560(%rax), %edx
	orl	%ecx, %edx
	shrl	$5, %edx
	jne	.L106
	movzbl	84(%rax), %ecx
	movl	80(%rax), %esi
	movq	768(%rax), %rdx
	testb	%cl, %cl
	je	.L96
	testl	%esi, %esi
	jle	.L123
	cmpl	$3, %esi
	jne	.L106
.L123:
	movswl	80(%rdx), %r8d
	testw	%r8w, %r8w
	js	.L100
	sarl	$5, %r8d
.L101:
	cmpl	$1, %r8d
	jne	.L106
.L96:
	movl	3276(%rax), %r8d
	cmpl	$10, %r8d
	jg	.L106
	movl	3268(%rax), %r9d
	testl	%r9d, %r9d
	jg	.L106
	movswl	400(%rdx), %r9d
	testw	%r9w, %r9w
	js	.L104
	sarl	$5, %r9d
.L105:
	cmpl	$1, %r9d
	jne	.L106
	movl	1864(%rdx), %r10d
	cmpl	$65535, %r10d
	ja	.L106
	movb	$1, 3928(%rax)
	movl	3256(%rax), %r9d
	movw	%r10w, 3930(%rax)
	cmpl	$3, %esi
	jne	.L124
	testb	%cl, %cl
	je	.L124
	movzwl	80(%rdx), %esi
	testw	%si, %si
	js	.L110
	movswl	%si, %r10d
	sarl	$5, %r10d
.L111:
	movl	$-1, %ecx
	testl	%r10d, %r10d
	je	.L109
	andl	$2, %esi
	leaq	82(%rdx), %rcx
	jne	.L113
	movq	96(%rdx), %rcx
.L113:
	movzwl	(%rcx), %ecx
.L109:
	movw	%cx, 3932(%rax)
	movzwl	400(%rdx), %ecx
	testw	%cx, %cx
	js	.L114
	movswl	%cx, %esi
	sarl	$5, %esi
.L115:
	movl	$-1, %r10d
	testl	%esi, %esi
	je	.L116
	andl	$2, %ecx
	je	.L117
	addq	$402, %rdx
.L118:
	movzwl	(%rdx), %r10d
.L116:
	cmpl	$128, %r8d
	movl	$0, %edx
	movw	%r10w, 3934(%rax)
	cmovnb	%edx, %r8d
	cmpl	$128, %r9d
	movb	%r8b, 3936(%rax)
	movl	$127, %eax
	cmovnb	%eax, %r9d
	movq	360(%rdi), %rax
	movb	%r9b, 3937(%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L90:
	movl	212(%rax), %ecx
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L100:
	movl	84(%rdx), %r8d
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L104:
	movl	404(%rdx), %r9d
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L124:
	xorl	%ecx, %ecx
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L114:
	movl	404(%rdx), %esi
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L117:
	movq	416(%rdx), %rdx
	jmp	.L118
.L110:
	movl	84(%rdx), %r10d
	jmp	.L111
	.cfi_endproc
.LFE5359:
	.size	_ZN6icu_6713DecimalFormat15setupFastFormatEv.part.0, .-_ZN6icu_6713DecimalFormat15setupFastFormatEv.part.0
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713DecimalFormat15getRoundingModeEv
	.type	_ZNK6icu_6713DecimalFormat15getRoundingModeEv, @function
_ZNK6icu_6713DecimalFormat15getRoundingModeEv:
.LFB3984:
	.cfi_startproc
	endbr64
	movq	360(%rdi), %rax
	testq	%rax, %rax
	je	.L144
	movl	3916(%rax), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L144:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN6icu_676number4impl23DecimalFormatProperties10getDefaultEv@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movl	748(%rax), %eax
	ret
	.cfi_endproc
.LFE3984:
	.size	_ZNK6icu_6713DecimalFormat15getRoundingModeEv, .-_ZNK6icu_6713DecimalFormat15getRoundingModeEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713DecimalFormat12getAttributeE22UNumberFormatAttributeR10UErrorCode
	.type	_ZNK6icu_6713DecimalFormat12getAttributeE22UNumberFormatAttributeR10UErrorCode, @function
_ZNK6icu_6713DecimalFormat12getAttributeE22UNumberFormatAttributeR10UErrorCode:
.LFB3912:
	.cfi_startproc
	endbr64
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jg	.L201
	movq	360(%rdi), %r8
	testq	%r8, %r8
	je	.L202
	cmpl	$23, %esi
	ja	.L203
	leaq	.L158(%rip), %rcx
	movl	%esi, %esi
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	(%rcx,%rsi,4), %rax
	addq	%rcx, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L158:
	.long	.L177-.L158
	.long	.L176-.L158
	.long	.L175-.L158
	.long	.L174-.L158
	.long	.L173-.L158
	.long	.L173-.L158
	.long	.L172-.L158
	.long	.L171-.L158
	.long	.L171-.L158
	.long	.L170-.L158
	.long	.L169-.L158
	.long	.L168-.L158
	.long	.L150-.L158
	.long	.L167-.L158
	.long	.L166-.L158
	.long	.L165-.L158
	.long	.L164-.L158
	.long	.L163-.L158
	.long	.L162-.L158
	.long	.L161-.L158
	.long	.L150-.L158
	.long	.L160-.L158
	.long	.L159-.L158
	.long	.L157-.L158
	.text
	.p2align 4,,10
	.p2align 3
.L203:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	subl	$4096, %esi
	cmpl	$4, %esi
	ja	.L204
	leaq	.L152(%rip), %rdx
	movslq	(%rdx,%rsi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L152:
	.long	.L156-.L152
	.long	.L155-.L152
	.long	.L154-.L152
	.long	.L153-.L152
	.long	.L151-.L152
	.text
.L171:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	popq	%rbp
	.cfi_remember_state
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_6712NumberFormat24getMinimumFractionDigitsEv@PLT
.L173:
	.cfi_restore_state
	popq	%rbp
	.cfi_remember_state
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_6712NumberFormat23getMinimumIntegerDigitsEv@PLT
.L172:
	.cfi_restore_state
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_6712NumberFormat24getMaximumFractionDigitsEv@PLT
.L155:
	movzbl	476(%r8), %eax
	ret
.L156:
	movzbl	75(%r8), %eax
	ret
.L151:
	movzbl	764(%r8), %eax
	ret
.L157:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	cmpb	$0, 64(%r8)
	jne	.L145
	movl	68(%r8), %eax
.L145:
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L159:
	.cfi_restore_state
	movl	112(%r8), %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L160:
	.cfi_restore_state
	movl	128(%r8), %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L161:
	.cfi_restore_state
	movq	(%rdi), %rax
	leaq	_ZNK6icu_6712NumberFormat9isLenientEv(%rip), %rdx
	movq	200(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L179
	movzbl	341(%rdi), %eax
.L200:
	movsbl	%al, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L162:
	.cfi_restore_state
	movl	3260(%r8), %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L165:
	.cfi_restore_state
	movl	760(%r8), %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	movl	$0, %eax
	cmovns	760(%r8), %eax
	ret
.L166:
	.cfi_restore_state
	movq	(%rdi), %rax
	leaq	_ZNK6icu_6713DecimalFormat14getPadPositionEv(%rip), %rdx
	movq	448(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L185
	xorl	%eax, %eax
	cmpb	$0, 392(%r8)
	jne	.L145
	movl	396(%r8), %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L167:
	.cfi_restore_state
	movq	(%rdi), %rax
	leaq	_ZNK6icu_6713DecimalFormat14getFormatWidthEv(%rip), %rdx
	movq	416(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L185
	movl	76(%r8), %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L168:
	.cfi_restore_state
	movq	(%rdi), %rax
	leaq	_ZNK6icu_6713DecimalFormat15getRoundingModeEv(%rip), %rdx
	movq	272(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L185
	movl	3916(%r8), %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L169:
	.cfi_restore_state
	movl	80(%r8), %edx
	movl	$0, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	testl	%edx, %edx
	cmovns	80(%r8), %eax
	ret
.L170:
	.cfi_restore_state
	movl	124(%r8), %eax
	cmpl	$1, %eax
	jne	.L145
	movl	88(%r8), %edi
	testl	%edi, %edi
	je	.L145
	call	uprv_pow10_67@PLT
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	cvttsd2sil	%xmm0, %eax
	ret
.L174:
	.cfi_restore_state
	popq	%rbp
	.cfi_remember_state
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_6712NumberFormat23getMaximumIntegerDigitsEv@PLT
.L175:
	.cfi_restore_state
	movzbl	73(%r8), %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
.L153:
	.cfi_restore 6
	movzbl	464(%r8), %eax
	ret
.L154:
	movzbl	72(%r8), %eax
	ret
.L163:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	movl	3280(%r8), %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L164:
	.cfi_restore_state
	cmpl	$-1, 120(%r8)
	movl	$1, %eax
	jne	.L145
	xorl	%eax, %eax
	cmpl	$-1, 100(%r8)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	setne	%al
	ret
.L176:
	.cfi_restore_state
	call	_ZNK6icu_6712NumberFormat14isGroupingUsedEv@PLT
	jmp	.L200
.L177:
	movsbl	340(%rdi), %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L150:
	.cfi_restore_state
	movl	$16, (%rdx)
	movl	$-1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L204:
	.cfi_restore 6
	movl	$16, (%rdx)
.L201:
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L185:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	popq	%rbp
	.cfi_remember_state
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
.L179:
	.cfi_restore_state
	call	*%rax
	jmp	.L200
.L202:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	movl	$7, (%rdx)
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE3912:
	.size	_ZNK6icu_6713DecimalFormat12getAttributeE22UNumberFormatAttributeR10UErrorCode, .-_ZNK6icu_6713DecimalFormat12getAttributeE22UNumberFormatAttributeR10UErrorCode
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev:
.LFB4521:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE4521:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei:
.LFB4524:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	cmpl	$40, %esi
	jg	.L218
	ret
	.p2align 4,,10
	.p2align 3
.L218:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L206
	cmpb	$0, 12(%rbx)
	jne	.L219
.L210:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L206:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L219:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L210
	.cfi_endproc
.LFE4524:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EED2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev:
.LFB4527:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L222
	ret
	.p2align 4,,10
	.p2align 3
.L222:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE4527:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5EOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_:
.LFB4530:
	.cfi_startproc
	endbr64
	movzbl	12(%rsi), %eax
	movq	(%rsi), %r8
	movslq	8(%rsi), %rdx
	movb	%al, 12(%rdi)
	leaq	13(%rsi), %rax
	movq	%r8, (%rdi)
	movl	%edx, 8(%rdi)
	cmpq	%rax, %r8
	je	.L225
	movq	%rax, (%rsi)
	movl	$40, 8(%rsi)
	movb	$0, 12(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L225:
	leaq	13(%rdi), %r9
	movq	%r8, %rsi
	movq	%r9, (%rdi)
	movq	%r9, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE4530:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_:
.LFB4532:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 12(%rdi)
	movq	%rsi, %rbx
	jne	.L231
.L227:
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movzbl	12(%rbx), %eax
	movb	%al, 12(%r12)
	movq	(%rbx), %rsi
	leaq	13(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L232
	movq	%rsi, (%r12)
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L231:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L232:
	leaq	13(%r12), %rdi
	movslq	8(%rbx), %rdx
	movq	%rdi, (%r12)
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4532:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv:
.LFB4533:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE4533:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv:
.LFB4534:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE4534:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv:
.LFB4535:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	addq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE4535:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB4536:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE4536:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB4537:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE4537:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci:
.LFB4538:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L248
	testl	%edx, %edx
	jle	.L248
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 12(%rdi)
	jne	.L251
.L240:
	movq	%rsi, (%rbx)
	movl	%edx, 8(%rbx)
	movb	$0, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L248:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L251:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	%edx, -28(%rbp)
	movq	%rsi, -24(%rbp)
	call	uprv_free_67@PLT
	movl	-28(%rbp), %edx
	movq	-24(%rbp), %rsi
	jmp	.L240
	.cfi_endproc
.LFE4538:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii:
.LFB4539:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L255
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movl	%esi, %r13d
	movl	%edx, %r12d
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L255
	testl	%r12d, %r12d
	jg	.L262
	cmpb	$0, 12(%rbx)
	jne	.L263
.L257:
	movq	%r14, (%rbx)
	movq	%r14, %rax
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L262:
	.cfi_restore_state
	cmpl	%r12d, 8(%rbx)
	cmovle	8(%rbx), %r12d
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	je	.L257
.L263:
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L257
	.p2align 4,,10
	.p2align 3
.L255:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4539:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, .-_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi:
.LFB4540:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 12(%rdi)
	movq	%rdi, %rbx
	je	.L265
	movq	(%rdi), %r8
.L266:
	leaq	13(%rbx), %rax
	movl	%r12d, 0(%r13)
	movq	%rax, (%rbx)
	movq	%r8, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L265:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L269
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	cmovg	%eax, %r12d
	movslq	%r12d, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L269
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L269:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4540:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, .-_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv:
.LFB4541:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L276
	ret
	.p2align 4,,10
	.p2align 3
.L276:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE4541:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv:
.LFB4542:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE4542:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_:
.LFB4543:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4543:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_:
.LFB4544:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE4544:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5ERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_:
.LFB4546:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4546:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_:
.LFB4548:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4548:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713DecimalFormat16getStaticClassIDEv
	.type	_ZN6icu_6713DecimalFormat16getStaticClassIDEv, @function
_ZN6icu_6713DecimalFormat16getStaticClassIDEv:
.LFB3886:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6713DecimalFormat16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3886:
	.size	_ZN6icu_6713DecimalFormat16getStaticClassIDEv, .-_ZN6icu_6713DecimalFormat16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713DecimalFormat16setParseAllInputE27UNumberFormatAttributeValue
	.type	_ZN6icu_6713DecimalFormat16setParseAllInputE27UNumberFormatAttributeValue, @function
_ZN6icu_6713DecimalFormat16setParseAllInputE27UNumberFormatAttributeValue:
.LFB3910:
	.cfi_startproc
	endbr64
	movq	360(%rdi), %rax
	testq	%rax, %rax
	je	.L283
	cmpl	%esi, 480(%rax)
	je	.L283
	movl	%esi, 480(%rax)
.L283:
	ret
	.cfi_endproc
.LFE3910:
	.size	_ZN6icu_6713DecimalFormat16setParseAllInputE27UNumberFormatAttributeValue, .-_ZN6icu_6713DecimalFormat16setParseAllInputE27UNumberFormatAttributeValue
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713DecimalFormat17getPositivePrefixERNS_13UnicodeStringE
	.type	_ZNK6icu_6713DecimalFormat17getPositivePrefixERNS_13UnicodeStringE, @function
_ZNK6icu_6713DecimalFormat17getPositivePrefixERNS_13UnicodeStringE:
.LFB3968:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$24, %rsp
	movq	360(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L291
	movq	%rsi, %rcx
	addq	$776, %rdi
	leaq	-28(%rbp), %r8
	xorl	%edx, %edx
	movl	$1, %esi
	movl	$0, -28(%rbp)
	call	_ZNK6icu_676number24LocalizedNumberFormatter12getAffixImplEbbRNS_13UnicodeStringER10UErrorCode@PLT
	movl	-28(%rbp), %eax
	testl	%eax, %eax
	jg	.L291
.L290:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L294
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L291:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	jmp	.L290
.L294:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3968:
	.size	_ZNK6icu_6713DecimalFormat17getPositivePrefixERNS_13UnicodeStringE, .-_ZNK6icu_6713DecimalFormat17getPositivePrefixERNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713DecimalFormat17getNegativePrefixERNS_13UnicodeStringE
	.type	_ZNK6icu_6713DecimalFormat17getNegativePrefixERNS_13UnicodeStringE, @function
_ZNK6icu_6713DecimalFormat17getNegativePrefixERNS_13UnicodeStringE:
.LFB3970:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$24, %rsp
	movq	360(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L298
	movq	%rsi, %rcx
	addq	$776, %rdi
	leaq	-28(%rbp), %r8
	movl	$1, %edx
	movl	$1, %esi
	movl	$0, -28(%rbp)
	call	_ZNK6icu_676number24LocalizedNumberFormatter12getAffixImplEbbRNS_13UnicodeStringER10UErrorCode@PLT
	movl	-28(%rbp), %eax
	testl	%eax, %eax
	jg	.L298
.L297:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L301
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L298:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	jmp	.L297
.L301:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3970:
	.size	_ZNK6icu_6713DecimalFormat17getNegativePrefixERNS_13UnicodeStringE, .-_ZNK6icu_6713DecimalFormat17getNegativePrefixERNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713DecimalFormat17getPositiveSuffixERNS_13UnicodeStringE
	.type	_ZNK6icu_6713DecimalFormat17getPositiveSuffixERNS_13UnicodeStringE, @function
_ZNK6icu_6713DecimalFormat17getPositiveSuffixERNS_13UnicodeStringE:
.LFB3972:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$24, %rsp
	movq	360(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L305
	movq	%rsi, %rcx
	addq	$776, %rdi
	leaq	-28(%rbp), %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movl	$0, -28(%rbp)
	call	_ZNK6icu_676number24LocalizedNumberFormatter12getAffixImplEbbRNS_13UnicodeStringER10UErrorCode@PLT
	movl	-28(%rbp), %eax
	testl	%eax, %eax
	jg	.L305
.L304:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L308
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L305:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	jmp	.L304
.L308:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3972:
	.size	_ZNK6icu_6713DecimalFormat17getPositiveSuffixERNS_13UnicodeStringE, .-_ZNK6icu_6713DecimalFormat17getPositiveSuffixERNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713DecimalFormat17getNegativeSuffixERNS_13UnicodeStringE
	.type	_ZNK6icu_6713DecimalFormat17getNegativeSuffixERNS_13UnicodeStringE, @function
_ZNK6icu_6713DecimalFormat17getNegativeSuffixERNS_13UnicodeStringE:
.LFB3974:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$24, %rsp
	movq	360(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L312
	movq	%rsi, %rcx
	leaq	-28(%rbp), %r8
	movl	$1, %edx
	xorl	%esi, %esi
	addq	$776, %rdi
	movl	$0, -28(%rbp)
	call	_ZNK6icu_676number24LocalizedNumberFormatter12getAffixImplEbbRNS_13UnicodeStringER10UErrorCode@PLT
	movl	-28(%rbp), %eax
	testl	%eax, %eax
	jg	.L312
.L311:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L315
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L312:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	jmp	.L311
.L315:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3974:
	.size	_ZNK6icu_6713DecimalFormat17getNegativeSuffixERNS_13UnicodeStringE, .-_ZNK6icu_6713DecimalFormat17getNegativeSuffixERNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713DecimalFormat17isSignAlwaysShownEv
	.type	_ZNK6icu_6713DecimalFormat17isSignAlwaysShownEv, @function
_ZNK6icu_6713DecimalFormat17isSignAlwaysShownEv:
.LFB3976:
	.cfi_startproc
	endbr64
	movq	360(%rdi), %rax
	testq	%rax, %rax
	je	.L323
	movzbl	764(%rax), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L323:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN6icu_676number4impl23DecimalFormatProperties10getDefaultEv@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movzbl	756(%rax), %eax
	ret
	.cfi_endproc
.LFE3976:
	.size	_ZNK6icu_6713DecimalFormat17isSignAlwaysShownEv, .-_ZNK6icu_6713DecimalFormat17isSignAlwaysShownEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713DecimalFormat13getMultiplierEv
	.type	_ZNK6icu_6713DecimalFormat13getMultiplierEv, @function
_ZNK6icu_6713DecimalFormat13getMultiplierEv:
.LFB3978:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	360(%rdi), %rax
	leaq	8(%rax), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testq	%rax, %rax
	je	.L333
.L326:
	movl	116(%rdx), %eax
	cmpl	$1, %eax
	jne	.L324
	movl	80(%rdx), %edi
	testl	%edi, %edi
	jne	.L334
.L324:
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L334:
	.cfi_restore_state
	call	uprv_pow10_67@PLT
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	cvttsd2sil	%xmm0, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L333:
	.cfi_restore_state
	call	_ZN6icu_676number4impl23DecimalFormatProperties10getDefaultEv@PLT
	movq	%rax, %rdx
	jmp	.L326
	.cfi_endproc
.LFE3978:
	.size	_ZNK6icu_6713DecimalFormat13getMultiplierEv, .-_ZNK6icu_6713DecimalFormat13getMultiplierEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713DecimalFormat18getMultiplierScaleEv
	.type	_ZNK6icu_6713DecimalFormat18getMultiplierScaleEv, @function
_ZNK6icu_6713DecimalFormat18getMultiplierScaleEv:
.LFB3980:
	.cfi_startproc
	endbr64
	movq	360(%rdi), %rax
	testq	%rax, %rax
	je	.L342
	movl	128(%rax), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L342:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN6icu_676number4impl23DecimalFormatProperties10getDefaultEv@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movl	120(%rax), %eax
	ret
	.cfi_endproc
.LFE3980:
	.size	_ZNK6icu_6713DecimalFormat18getMultiplierScaleEv, .-_ZNK6icu_6713DecimalFormat18getMultiplierScaleEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713DecimalFormat15getGroupingSizeEv
	.type	_ZNK6icu_6713DecimalFormat15getGroupingSizeEv, @function
_ZNK6icu_6713DecimalFormat15getGroupingSizeEv:
.LFB3998:
	.cfi_startproc
	endbr64
	movq	360(%rdi), %rax
	testq	%rax, %rax
	je	.L350
	movl	80(%rax), %eax
	movl	$0, %edx
	testl	%eax, %eax
	cmovs	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L350:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN6icu_676number4impl23DecimalFormatProperties10getDefaultEv@PLT
	movl	$0, %edx
	popq	%rbp
	.cfi_def_cfa 7, 8
	movl	72(%rax), %eax
	testl	%eax, %eax
	cmovs	%edx, %eax
	ret
	.cfi_endproc
.LFE3998:
	.size	_ZNK6icu_6713DecimalFormat15getGroupingSizeEv, .-_ZNK6icu_6713DecimalFormat15getGroupingSizeEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713DecimalFormat24getSecondaryGroupingSizeEv
	.type	_ZNK6icu_6713DecimalFormat24getSecondaryGroupingSizeEv, @function
_ZNK6icu_6713DecimalFormat24getSecondaryGroupingSizeEv:
.LFB4000:
	.cfi_startproc
	endbr64
	movq	360(%rdi), %rax
	testq	%rax, %rax
	je	.L358
	movl	760(%rax), %eax
	movl	$0, %edx
	testl	%eax, %eax
	cmovs	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L358:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN6icu_676number4impl23DecimalFormatProperties10getDefaultEv@PLT
	movl	$0, %edx
	popq	%rbp
	.cfi_def_cfa 7, 8
	movl	752(%rax), %eax
	testl	%eax, %eax
	cmovs	%edx, %eax
	ret
	.cfi_endproc
.LFE4000:
	.size	_ZNK6icu_6713DecimalFormat24getSecondaryGroupingSizeEv, .-_ZNK6icu_6713DecimalFormat24getSecondaryGroupingSizeEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713DecimalFormat24getMinimumGroupingDigitsEv
	.type	_ZNK6icu_6713DecimalFormat24getMinimumGroupingDigitsEv, @function
_ZNK6icu_6713DecimalFormat24getMinimumGroupingDigitsEv:
.LFB4002:
	.cfi_startproc
	endbr64
	movq	360(%rdi), %rax
	testq	%rax, %rax
	je	.L366
	movl	112(%rax), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L366:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN6icu_676number4impl23DecimalFormatProperties10getDefaultEv@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movl	104(%rax), %eax
	ret
	.cfi_endproc
.LFE4002:
	.size	_ZNK6icu_6713DecimalFormat24getMinimumGroupingDigitsEv, .-_ZNK6icu_6713DecimalFormat24getMinimumGroupingDigitsEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713DecimalFormat29isDecimalSeparatorAlwaysShownEv
	.type	_ZNK6icu_6713DecimalFormat29isDecimalSeparatorAlwaysShownEv, @function
_ZNK6icu_6713DecimalFormat29isDecimalSeparatorAlwaysShownEv:
.LFB4004:
	.cfi_startproc
	endbr64
	movq	360(%rdi), %rax
	testq	%rax, %rax
	je	.L374
	movzbl	73(%rax), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L374:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN6icu_676number4impl23DecimalFormatProperties10getDefaultEv@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movzbl	65(%rax), %eax
	ret
	.cfi_endproc
.LFE4004:
	.size	_ZNK6icu_6713DecimalFormat29isDecimalSeparatorAlwaysShownEv, .-_ZNK6icu_6713DecimalFormat29isDecimalSeparatorAlwaysShownEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713DecimalFormat29isDecimalPatternMatchRequiredEv
	.type	_ZNK6icu_6713DecimalFormat29isDecimalPatternMatchRequiredEv, @function
_ZNK6icu_6713DecimalFormat29isDecimalPatternMatchRequiredEv:
.LFB4006:
	.cfi_startproc
	endbr64
	movq	360(%rdi), %rax
	testq	%rax, %rax
	je	.L382
	movzbl	72(%rax), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L382:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN6icu_676number4impl23DecimalFormatProperties10getDefaultEv@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movzbl	64(%rax), %eax
	ret
	.cfi_endproc
.LFE4006:
	.size	_ZNK6icu_6713DecimalFormat29isDecimalPatternMatchRequiredEv, .-_ZNK6icu_6713DecimalFormat29isDecimalPatternMatchRequiredEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713DecimalFormat17isParseNoExponentEv
	.type	_ZNK6icu_6713DecimalFormat17isParseNoExponentEv, @function
_ZNK6icu_6713DecimalFormat17isParseNoExponentEv:
.LFB4008:
	.cfi_startproc
	endbr64
	movq	360(%rdi), %rax
	testq	%rax, %rax
	je	.L390
	movzbl	476(%rax), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L390:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN6icu_676number4impl23DecimalFormatProperties10getDefaultEv@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movzbl	468(%rax), %eax
	ret
	.cfi_endproc
.LFE4008:
	.size	_ZNK6icu_6713DecimalFormat17isParseNoExponentEv, .-_ZNK6icu_6713DecimalFormat17isParseNoExponentEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713DecimalFormat20isParseCaseSensitiveEv
	.type	_ZNK6icu_6713DecimalFormat20isParseCaseSensitiveEv, @function
_ZNK6icu_6713DecimalFormat20isParseCaseSensitiveEv:
.LFB4010:
	.cfi_startproc
	endbr64
	movq	360(%rdi), %rax
	testq	%rax, %rax
	je	.L398
	movzbl	464(%rax), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L398:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN6icu_676number4impl23DecimalFormatProperties10getDefaultEv@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movzbl	456(%rax), %eax
	ret
	.cfi_endproc
.LFE4010:
	.size	_ZNK6icu_6713DecimalFormat20isParseCaseSensitiveEv, .-_ZNK6icu_6713DecimalFormat20isParseCaseSensitiveEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713DecimalFormat31isFormatFailIfMoreThanMaxDigitsEv
	.type	_ZNK6icu_6713DecimalFormat31isFormatFailIfMoreThanMaxDigitsEv, @function
_ZNK6icu_6713DecimalFormat31isFormatFailIfMoreThanMaxDigitsEv:
.LFB4012:
	.cfi_startproc
	endbr64
	movq	360(%rdi), %rax
	testq	%rax, %rax
	je	.L406
	movzbl	75(%rax), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L406:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN6icu_676number4impl23DecimalFormatProperties10getDefaultEv@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movzbl	67(%rax), %eax
	ret
	.cfi_endproc
.LFE4012:
	.size	_ZNK6icu_6713DecimalFormat31isFormatFailIfMoreThanMaxDigitsEv, .-_ZNK6icu_6713DecimalFormat31isFormatFailIfMoreThanMaxDigitsEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713DecimalFormat27getMinimumSignificantDigitsEv
	.type	_ZNK6icu_6713DecimalFormat27getMinimumSignificantDigitsEv, @function
_ZNK6icu_6713DecimalFormat27getMinimumSignificantDigitsEv:
.LFB4024:
	.cfi_startproc
	endbr64
	movq	360(%rdi), %rax
	testq	%rax, %rax
	je	.L414
	movl	3280(%rax), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L414:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN6icu_676number4impl23DecimalFormatProperties10getDefaultEv@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movl	112(%rax), %eax
	ret
	.cfi_endproc
.LFE4024:
	.size	_ZNK6icu_6713DecimalFormat27getMinimumSignificantDigitsEv, .-_ZNK6icu_6713DecimalFormat27getMinimumSignificantDigitsEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713DecimalFormat27getMaximumSignificantDigitsEv
	.type	_ZNK6icu_6713DecimalFormat27getMaximumSignificantDigitsEv, @function
_ZNK6icu_6713DecimalFormat27getMaximumSignificantDigitsEv:
.LFB4025:
	.cfi_startproc
	endbr64
	movq	360(%rdi), %rax
	testq	%rax, %rax
	je	.L422
	movl	3260(%rax), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L422:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN6icu_676number4impl23DecimalFormatProperties10getDefaultEv@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movl	92(%rax), %eax
	ret
	.cfi_endproc
.LFE4025:
	.size	_ZNK6icu_6713DecimalFormat27getMaximumSignificantDigitsEv, .-_ZNK6icu_6713DecimalFormat27getMaximumSignificantDigitsEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713DecimalFormat24areSignificantDigitsUsedEv
	.type	_ZNK6icu_6713DecimalFormat24areSignificantDigitsUsedEv, @function
_ZNK6icu_6713DecimalFormat24areSignificantDigitsUsedEv:
.LFB4028:
	.cfi_startproc
	endbr64
	movq	360(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L436
	cmpl	$-1, 120(%rdx)
	leaq	8(%rdx), %rax
	movl	$1, %r8d
	jne	.L431
	cmpl	$-1, 92(%rax)
	setne	%r8b
.L431:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L436:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN6icu_676number4impl23DecimalFormatProperties10getDefaultEv@PLT
	movl	$1, %r8d
	cmpl	$-1, 112(%rax)
	jne	.L423
	cmpl	$-1, 92(%rax)
	setne	%r8b
.L423:
	movl	%r8d, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4028:
	.size	_ZNK6icu_6713DecimalFormat24areSignificantDigitsUsedEv, .-_ZNK6icu_6713DecimalFormat24areSignificantDigitsUsedEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713DecimalFormat16getCurrencyUsageEv
	.type	_ZNK6icu_6713DecimalFormat16getCurrencyUsageEv, @function
_ZNK6icu_6713DecimalFormat16getCurrencyUsageEv:
.LFB4033:
	.cfi_startproc
	endbr64
	movq	360(%rdi), %rax
	xorl	%r8d, %r8d
	testq	%rax, %rax
	je	.L437
	cmpb	$0, 64(%rax)
	jne	.L437
	movl	68(%rax), %r8d
.L437:
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE4033:
	.size	_ZNK6icu_6713DecimalFormat16getCurrencyUsageEv, .-_ZNK6icu_6713DecimalFormat16getCurrencyUsageEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713DecimalFormat23formatToDecimalQuantityERKNS_11FormattableERNS_6number4impl15DecimalQuantityER10UErrorCode
	.type	_ZNK6icu_6713DecimalFormat23formatToDecimalQuantityERKNS_11FormattableERNS_6number4impl15DecimalQuantityER10UErrorCode, @function
_ZNK6icu_6713DecimalFormat23formatToDecimalQuantityERKNS_11FormattableERNS_6number4impl15DecimalQuantityER10UErrorCode:
.LFB4035:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$264, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L441
	cmpq	$0, 360(%rdi)
	movq	%rdi, %rbx
	movq	%rcx, %r12
	je	.L447
	leaq	-288(%rbp), %r15
	movq	%rsi, %r14
	movq	%rdx, %r13
	movzbl	_ZN6icu_67L15kUndefinedFieldE(%rip), %esi
	movq	%r15, %rdi
	call	_ZN6icu_6731FormattedValueStringBuilderImplC2ENS_22FormattedStringBuilder5FieldE@PLT
	leaq	-136(%rbp), %r8
	leaq	16+_ZTVN6icu_676number4impl20UFormattedNumberDataE(%rip), %rax
	movq	%r8, %rdi
	movq	%rax, -288(%rbp)
	movq	%r8, -296(%rbp)
	call	_ZN6icu_676number4impl15DecimalQuantityC1Ev@PLT
	movq	-296(%rbp), %r8
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r8, %rsi
	call	_ZNK6icu_6711Formattable23populateDecimalQuantityERNS_6number4impl15DecimalQuantityER10UErrorCode@PLT
	movq	360(%rbx), %rdi
	movq	%r12, %rdx
	movq	%r15, %rsi
	addq	$776, %rdi
	call	_ZNK6icu_676number24LocalizedNumberFormatter10formatImplEPNS0_4impl20UFormattedNumberDataER10UErrorCode@PLT
	movq	-296(%rbp), %r8
	movq	%r13, %rdi
	movq	%r8, %rsi
	call	_ZN6icu_676number4impl15DecimalQuantityaSEOS2_@PLT
	movq	%r15, %rdi
	call	_ZN6icu_676number4impl20UFormattedNumberDataD1Ev@PLT
.L441:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L448
	addq	$264, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L447:
	.cfi_restore_state
	movl	$7, (%rcx)
	jmp	.L441
.L448:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4035:
	.size	_ZNK6icu_6713DecimalFormat23formatToDecimalQuantityERKNS_11FormattableERNS_6number4impl15DecimalQuantityER10UErrorCode, .-_ZNK6icu_6713DecimalFormat23formatToDecimalQuantityERKNS_11FormattableERNS_6number4impl15DecimalQuantityER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713DecimalFormat17toNumberFormatterER10UErrorCode
	.type	_ZNK6icu_6713DecimalFormat17toNumberFormatterER10UErrorCode, @function
_ZNK6icu_6713DecimalFormat17toNumberFormatterER10UErrorCode:
.LFB4037:
	.cfi_startproc
	endbr64
	movl	(%rsi), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L449
	movq	360(%rdi), %rax
	testq	%rax, %rax
	je	.L453
	addq	$776, %rax
.L449:
	ret
	.p2align 4,,10
	.p2align 3
.L453:
	movl	$7, (%rsi)
	ret
	.cfi_endproc
.LFE4037:
	.size	_ZNK6icu_6713DecimalFormat17toNumberFormatterER10UErrorCode, .-_ZNK6icu_6713DecimalFormat17toNumberFormatterER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713DecimalFormat24setPropertiesFromPatternERKNS_13UnicodeStringEiR10UErrorCode
	.type	_ZN6icu_6713DecimalFormat24setPropertiesFromPatternERKNS_13UnicodeStringEiR10UErrorCode, @function
_ZN6icu_6713DecimalFormat24setPropertiesFromPatternERKNS_13UnicodeStringEiR10UErrorCode:
.LFB4046:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	movq	%rsi, %rdi
	movl	(%rcx), %esi
	testl	%esi, %esi
	jle	.L456
	ret
	.p2align 4,,10
	.p2align 3
.L456:
	movq	360(%rax), %rsi
	addq	$8, %rsi
	jmp	_ZN6icu_676number4impl13PatternParser25parseToExistingPropertiesERKNS_13UnicodeStringERNS1_23DecimalFormatPropertiesENS1_14IgnoreRoundingER10UErrorCode@PLT
	.cfi_endproc
.LFE4046:
	.size	_ZN6icu_6713DecimalFormat24setPropertiesFromPatternERKNS_13UnicodeStringEiR10UErrorCode, .-_ZN6icu_6713DecimalFormat24setPropertiesFromPatternERKNS_13UnicodeStringEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713DecimalFormat9getParserER10UErrorCode
	.type	_ZNK6icu_6713DecimalFormat9getParserER10UErrorCode, @function
_ZNK6icu_6713DecimalFormat9getParserER10UErrorCode:
.LFB4047:
	.cfi_startproc
	endbr64
	movl	(%rsi), %edx
	testl	%edx, %edx
	jg	.L467
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	360(%rdi), %rax
	movq	1232(%rax), %r13
	testq	%r13, %r13
	je	.L460
.L466:
	movq	%r13, %r8
.L457:
	addq	$8, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L460:
	.cfi_restore_state
	movq	360(%rdi), %rdi
	movq	%rsi, %rbx
	xorl	%edx, %edx
	movq	%rbx, %rcx
	movq	768(%rdi), %rsi
	addq	$8, %rdi
	call	_ZN6icu_678numparse4impl16NumberParserImpl26createParserFromPropertiesERKNS_6number4impl23DecimalFormatPropertiesERKNS_20DecimalFormatSymbolsEbR10UErrorCode@PLT
	movq	%rax, %r8
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L461
	testq	%r8, %r8
	je	.L468
	movq	360(%r12), %rdx
	movq	%r13, %rax
	lock cmpxchgq	%r8, 1232(%rdx)
	movq	%rax, %r13
	je	.L457
	movq	(%r8), %rdx
	movq	%r8, %rdi
	call	*8(%rdx)
	jmp	.L466
	.p2align 4,,10
	.p2align 3
.L461:
	addq	$8, %rsp
	xorl	%r8d, %r8d
	popq	%rbx
	movq	%r8, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L467:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	xorl	%r8d, %r8d
	movq	%r8, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L468:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movl	$7, (%rbx)
	jmp	.L457
	.cfi_endproc
.LFE4047:
	.size	_ZNK6icu_6713DecimalFormat9getParserER10UErrorCode, .-_ZNK6icu_6713DecimalFormat9getParserER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713DecimalFormat17getCurrencyParserER10UErrorCode
	.type	_ZNK6icu_6713DecimalFormat17getCurrencyParserER10UErrorCode, @function
_ZNK6icu_6713DecimalFormat17getCurrencyParserER10UErrorCode:
.LFB4048:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L481
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	360(%rdi), %rax
	movq	1240(%rax), %r13
	testq	%r13, %r13
	je	.L472
.L480:
	movq	%r13, %r8
.L469:
	addq	$8, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L472:
	.cfi_restore_state
	movq	360(%rdi), %rdi
	movq	%rsi, %rbx
	movl	$1, %edx
	movq	%rbx, %rcx
	movq	768(%rdi), %rsi
	addq	$8, %rdi
	call	_ZN6icu_678numparse4impl16NumberParserImpl26createParserFromPropertiesERKNS_6number4impl23DecimalFormatPropertiesERKNS_20DecimalFormatSymbolsEbR10UErrorCode@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L482
	movq	360(%r12), %rdx
	movq	%r13, %rax
	lock cmpxchgq	%r8, 1240(%rdx)
	movq	%rax, %r13
	je	.L469
	movq	(%r8), %rdx
	movq	%r8, %rdi
	call	*8(%rdx)
	jmp	.L480
	.p2align 4,,10
	.p2align 3
.L481:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	xorl	%r8d, %r8d
	movq	%r8, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L482:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	360(%r12), %rdx
	movl	$7, (%rbx)
	lock cmpxchgq	%rax, 1240(%rdx)
	movq	%rax, %r8
	jne	.L469
	addq	$8, %rsp
	xorl	%r8d, %r8d
	popq	%rbx
	movq	%r8, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4048:
	.size	_ZNK6icu_6713DecimalFormat17getCurrencyParserER10UErrorCode, .-_ZNK6icu_6713DecimalFormat17getCurrencyParserER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713DecimalFormat15setupFastFormatEv
	.type	_ZN6icu_6713DecimalFormat15setupFastFormatEv, @function
_ZN6icu_6713DecimalFormat15setupFastFormatEv:
.LFB4051:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	360(%rdi), %rax
	leaq	8(%rax), %rdi
	call	_ZNK6icu_676number4impl23DecimalFormatProperties29equalsDefaultExceptFastFormatEv@PLT
	testb	%al, %al
	movq	360(%rbx), %rax
	je	.L502
	movzwl	208(%rax), %edx
	testb	$1, %dl
	je	.L533
.L486:
	movswl	336(%rax), %edx
	movswl	688(%rax), %ecx
	orl	%edx, %ecx
	movswl	560(%rax), %edx
	orl	%ecx, %edx
	shrl	$5, %edx
	jne	.L502
	movzbl	84(%rax), %ecx
	movl	80(%rax), %esi
	movq	768(%rax), %rdx
	testb	%cl, %cl
	jne	.L492
.L493:
	movl	3276(%rax), %edi
	cmpl	$10, %edi
	jg	.L502
	movl	3268(%rax), %r8d
	testl	%r8d, %r8d
	jg	.L502
	movswl	400(%rdx), %r8d
	testw	%r8w, %r8w
	js	.L500
	sarl	$5, %r8d
.L501:
	cmpl	$1, %r8d
	jne	.L502
	movl	1864(%rdx), %r9d
	cmpl	$65535, %r9d
	ja	.L502
	movb	$1, 3928(%rax)
	movl	3256(%rax), %r8d
	movw	%r9w, 3930(%rax)
	cmpl	$3, %esi
	jne	.L520
	testb	%cl, %cl
	je	.L520
	movzwl	80(%rdx), %esi
	testw	%si, %si
	js	.L506
	movswl	%si, %r9d
	sarl	$5, %r9d
.L507:
	movl	$-1, %ecx
	testl	%r9d, %r9d
	je	.L505
	andl	$2, %esi
	leaq	82(%rdx), %rcx
	jne	.L509
	movq	96(%rdx), %rcx
.L509:
	movzwl	(%rcx), %ecx
.L505:
	movw	%cx, 3932(%rax)
	movzwl	400(%rdx), %ecx
	testw	%cx, %cx
	js	.L510
	movswl	%cx, %esi
	sarl	$5, %esi
.L511:
	movl	$-1, %r9d
	testl	%esi, %esi
	je	.L512
	andl	$2, %ecx
	je	.L513
	addq	$402, %rdx
.L514:
	movzwl	(%rdx), %r9d
.L512:
	cmpl	$128, %edi
	movl	$0, %edx
	movw	%r9w, 3934(%rax)
	cmovnb	%edx, %edi
	cmpl	$128, %r8d
	movb	%dil, 3936(%rax)
	movl	$127, %eax
	cmovnb	%eax, %r8d
	movq	360(%rbx), %rax
	movb	%r8b, 3937(%rax)
	jmp	.L483
	.p2align 4,,10
	.p2align 3
.L533:
	testw	%dx, %dx
	js	.L487
	movswl	%dx, %ecx
	sarl	$5, %ecx
.L488:
	cmpl	$1, %ecx
	jne	.L502
	andl	$2, %edx
	leaq	210(%rax), %rdx
	jne	.L491
	movq	224(%rax), %rdx
.L491:
	cmpw	$45, (%rdx)
	je	.L486
	.p2align 4,,10
	.p2align 3
.L502:
	movb	$0, 3928(%rax)
.L483:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L492:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L519
	cmpl	$3, %esi
	jne	.L502
.L519:
	movswl	80(%rdx), %edi
	testw	%di, %di
	js	.L496
	sarl	$5, %edi
.L497:
	cmpl	$1, %edi
	je	.L493
	jmp	.L502
	.p2align 4,,10
	.p2align 3
.L487:
	movl	212(%rax), %ecx
	jmp	.L488
	.p2align 4,,10
	.p2align 3
.L500:
	movl	404(%rdx), %r8d
	jmp	.L501
	.p2align 4,,10
	.p2align 3
.L496:
	movl	84(%rdx), %edi
	jmp	.L497
	.p2align 4,,10
	.p2align 3
.L520:
	xorl	%ecx, %ecx
	jmp	.L505
.L510:
	movl	404(%rdx), %esi
	jmp	.L511
.L513:
	movq	416(%rdx), %rdx
	jmp	.L514
.L506:
	movl	84(%rdx), %r9d
	jmp	.L507
	.cfi_endproc
.LFE4051:
	.size	_ZN6icu_6713DecimalFormat15setupFastFormatEv, .-_ZN6icu_6713DecimalFormat15setupFastFormatEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713DecimalFormat17doFastFormatInt32EibRNS_13UnicodeStringE
	.type	_ZNK6icu_6713DecimalFormat17doFastFormatInt32EibRNS_13UnicodeStringE, @function
_ZNK6icu_6713DecimalFormat17doFastFormatInt32EibRNS_13UnicodeStringE:
.LFB4054:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, -120(%rbp)
	movq	360(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testb	%dl, %dl
	jne	.L554
.L535:
	movzbl	3936(%r15), %ebx
	movl	$1, %eax
	testb	%bl, %bl
	cmovle	%eax, %ebx
	cmpb	$0, 3937(%r15)
	jle	.L542
	xorl	%r13d, %r13d
	movl	$1, %r12d
	leaq	-70(%rbp), %r14
	jmp	.L537
	.p2align 4,,10
	.p2align 3
.L539:
	movl	%eax, %r12d
	movq	%rsi, %r14
.L537:
	movl	$10, %esi
	movl	%r8d, %edi
	addl	$1, %r13d
	call	div@PLT
	leaq	-2(%r14), %rsi
	movq	%rax, %rdx
	movl	%eax, %r8d
	sarq	$32, %rdx
	addw	3930(%r15), %dx
	movw	%dx, -2(%r14)
	cmpb	%r13b, 3937(%r15)
	jle	.L553
	testl	%eax, %eax
	jne	.L538
	cmpb	%r13b, %bl
	jle	.L553
.L538:
	leal	1(%r12), %eax
	cmpb	$3, %r12b
	jne	.L539
	movzwl	3932(%r15), %edx
	testw	%dx, %dx
	je	.L539
	movw	%dx, -4(%r14)
	leaq	-4(%r14), %rsi
	movl	$1, %eax
	jmp	.L539
	.p2align 4,,10
	.p2align 3
.L553:
	leaq	-96(%rbp), %rax
	movq	%rsi, %rcx
	subq	%rax, %rcx
	movq	%rcx, %rax
	movl	$13, %ecx
	sarq	%rax
	subl	%eax, %ecx
.L536:
	movq	-120(%rbp), %rdi
	xorl	%edx, %edx
	movq	%rsi, -128(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movq	-128(%rbp), %rsi
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L555
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L554:
	.cfi_restore_state
	movzwl	3934(%r15), %eax
	movl	%esi, -128(%rbp)
	movq	%rdi, %rbx
	xorl	%edx, %edx
	movq	%rcx, %rdi
	leaq	-98(%rbp), %rsi
	movl	$1, %ecx
	movw	%ax, -98(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movl	-128(%rbp), %r8d
	movq	360(%rbx), %r15
	negl	%r8d
	jmp	.L535
.L542:
	xorl	%ecx, %ecx
	leaq	-70(%rbp), %rsi
	jmp	.L536
.L555:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4054:
	.size	_ZNK6icu_6713DecimalFormat17doFastFormatInt32EibRNS_13UnicodeStringE, .-_ZNK6icu_6713DecimalFormat17doFastFormatInt32EibRNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713DecimalFormat16fastFormatDoubleEdRNS_13UnicodeStringE
	.type	_ZNK6icu_6713DecimalFormat16fastFormatDoubleEdRNS_13UnicodeStringE, @function
_ZNK6icu_6713DecimalFormat16fastFormatDoubleEdRNS_13UnicodeStringE:
.LFB4052:
	.cfi_startproc
	endbr64
	movq	360(%rdi), %rax
	cmpb	$1, 3928(%rax)
	jne	.L562
	ucomisd	%xmm0, %xmm0
	jp	.L562
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$16, %rsp
	movsd	%xmm0, -24(%rbp)
	call	uprv_trunc_67@PLT
	movsd	-24(%rbp), %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L560
	jne	.L560
	movsd	.LC0(%rip), %xmm0
	comisd	%xmm1, %xmm0
	jnb	.L560
	comisd	.LC1(%rip), %xmm1
	jbe	.L564
.L560:
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L562:
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L564:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	cvttsd2sil	%xmm1, %esi
	movmskpd	%xmm1, %edx
	movq	%r13, %rcx
	movq	%r12, %rdi
	andl	$1, %edx
	call	_ZNK6icu_6713DecimalFormat17doFastFormatInt32EibRNS_13UnicodeStringE
	addq	$16, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4052:
	.size	_ZNK6icu_6713DecimalFormat16fastFormatDoubleEdRNS_13UnicodeStringE, .-_ZNK6icu_6713DecimalFormat16fastFormatDoubleEdRNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713DecimalFormat15fastFormatInt64ElRNS_13UnicodeStringE
	.type	_ZNK6icu_6713DecimalFormat15fastFormatInt64ElRNS_13UnicodeStringE, @function
_ZNK6icu_6713DecimalFormat15fastFormatInt64ElRNS_13UnicodeStringE:
.LFB4053:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	360(%rdi), %rax
	movzbl	3928(%rax), %r12d
	testb	%r12b, %r12b
	je	.L565
	movq	%rdx, %rcx
	movl	$4294967294, %eax
	leaq	2147483647(%rsi), %rdx
	cmpq	%rax, %rdx
	jbe	.L572
	xorl	%r12d, %r12d
.L565:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L572:
	.cfi_restore_state
	movq	%rsi, %rdx
	shrq	$63, %rdx
	call	_ZNK6icu_6713DecimalFormat17doFastFormatInt32EibRNS_13UnicodeStringE
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4053:
	.size	_ZNK6icu_6713DecimalFormat15fastFormatInt64ElRNS_13UnicodeStringE, .-_ZNK6icu_6713DecimalFormat15fastFormatInt64ElRNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713DecimalFormat9toPatternERNS_13UnicodeStringE
	.type	_ZNK6icu_6713DecimalFormat9toPatternERNS_13UnicodeStringE, @function
_ZNK6icu_6713DecimalFormat9toPatternERNS_13UnicodeStringE:
.LFB4014:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$936, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -904(%rbp)
	movq	360(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r15, %r15
	je	.L599
	leaq	16+_ZTVN6icu_679ErrorCodeE(%rip), %rax
	leaq	24(%r15), %rsi
	movl	$0, -888(%rbp)
	movq	%rax, -896(%rbp)
	movq	8(%r15), %rax
	movq	%rax, -816(%rbp)
	movzbl	16(%r15), %eax
	movb	%al, -808(%rbp)
	leaq	-800(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -928(%rbp)
	call	_ZN6icu_6712CurrencyUnitC1ERKS0_@PLT
	movq	56(%r15), %r13
	movq	$0, -768(%rbp)
	testq	%r13, %r13
	je	.L576
	movl	$40, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L577
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6718CurrencyPluralInfoC1ERKS0_@PLT
.L577:
	movq	-768(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L578
	movq	(%rdi), %rax
	call	*8(%rax)
.L578:
	movq	%rbx, -768(%rbp)
.L576:
	movq	64(%r15), %rax
	movdqu	88(%r15), %xmm1
	leaq	136(%r15), %rsi
	leaq	-496(%rbp), %rbx
	movdqu	104(%r15), %xmm2
	leaq	-272(%rbp), %r13
	leaq	-144(%rbp), %r14
	movq	%rax, -760(%rbp)
	movq	72(%r15), %rax
	movq	%rax, -752(%rbp)
	movl	80(%r15), %eax
	movl	%eax, -744(%rbp)
	movzbl	84(%r15), %eax
	movaps	%xmm1, -736(%rbp)
	movb	%al, -740(%rbp)
	movq	120(%r15), %rax
	movaps	%xmm2, -720(%rbp)
	movq	%rax, -704(%rbp)
	movl	128(%r15), %eax
	movl	%eax, -696(%rbp)
	leaq	-688(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -936(%rbp)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	leaq	-624(%rbp), %rax
	leaq	200(%r15), %rsi
	movq	%rax, %rdi
	movq	%rax, -912(%rbp)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	leaq	-560(%rbp), %rax
	leaq	264(%r15), %rsi
	movq	%rax, %rdi
	movq	%rax, -920(%rbp)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	leaq	328(%r15), %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	leaq	-424(%rbp), %r9
	movq	392(%r15), %rax
	leaq	400(%r15), %rsi
	movq	%r9, %rdi
	movq	%r9, -960(%rbp)
	movq	%rax, -432(%rbp)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movzwl	464(%r15), %eax
	leaq	-336(%rbp), %r10
	leaq	488(%r15), %rsi
	movq	%r10, %rdi
	movq	%r10, -952(%rbp)
	movw	%ax, -360(%rbp)
	movq	468(%r15), %rax
	movq	%rax, -356(%rbp)
	movzwl	476(%r15), %eax
	movw	%ax, -348(%rbp)
	movl	480(%r15), %eax
	movl	%eax, -344(%rbp)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	leaq	552(%r15), %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	leaq	-208(%rbp), %r11
	leaq	616(%r15), %rsi
	movq	%r11, %rdi
	movq	%r11, -944(%rbp)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	leaq	680(%r15), %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	752(%r15), %rax
	movsd	744(%r15), %xmm0
	leaq	-888(%rbp), %rdx
	cmpb	$0, -808(%rbp)
	movq	-944(%rbp), %r11
	movq	%rax, -72(%rbp)
	movl	760(%r15), %eax
	movq	-952(%rbp), %r10
	movq	-960(%rbp), %r9
	movsd	%xmm0, -80(%rbp)
	movl	%eax, -64(%rbp)
	movzbl	764(%r15), %eax
	movb	%al, -60(%rbp)
	jne	.L579
.L581:
	movq	-904(%rbp), %rax
	movq	360(%rax), %rax
	movl	3268(%rax), %ecx
	movsd	3904(%rax), %xmm0
	movl	%ecx, -716(%rbp)
	movl	3252(%rax), %ecx
	movsd	%xmm0, -80(%rbp)
	movl	%ecx, -732(%rbp)
.L580:
	leaq	-880(%rbp), %r8
	leaq	-816(%rbp), %rsi
	movq	%r9, -944(%rbp)
	movq	%r8, %rdi
	movq	%r10, -952(%rbp)
	movq	%r11, -960(%rbp)
	movq	%r8, -904(%rbp)
	call	_ZN6icu_676number4impl18PatternStringUtils25propertiesToPatternStringERKNS1_23DecimalFormatPropertiesER10UErrorCode@PLT
	movq	-904(%rbp), %r8
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	-904(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-960(%rbp), %r11
	movq	%r11, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-952(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-944(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-920(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-912(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-936(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-768(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L582
	movq	(%rdi), %rax
	call	*8(%rax)
.L582:
	movq	-928(%rbp), %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	leaq	-896(%rbp), %rdi
	call	_ZN6icu_679ErrorCodeD1Ev@PLT
.L575:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L600
	addq	$936, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L579:
	.cfi_restore_state
	cmpq	$0, -768(%rbp)
	jne	.L581
	cmpb	$0, -760(%rbp)
	je	.L581
	movq	%rdx, %rsi
	movq	%r13, %rdi
	movq	%r11, -968(%rbp)
	movq	%r10, -960(%rbp)
	movq	%r9, -952(%rbp)
	movq	%rdx, -944(%rbp)
	call	_ZN6icu_676number4impl10AffixUtils18hasCurrencySymbolsERKNS_13UnicodeStringER10UErrorCode@PLT
	movq	-944(%rbp), %rdx
	movq	-952(%rbp), %r9
	testb	%al, %al
	movq	-960(%rbp), %r10
	movq	-968(%rbp), %r11
	jne	.L581
	movq	%rdx, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl10AffixUtils18hasCurrencySymbolsERKNS_13UnicodeStringER10UErrorCode@PLT
	movq	-944(%rbp), %rdx
	movq	-952(%rbp), %r9
	testb	%al, %al
	movq	-960(%rbp), %r10
	movq	-968(%rbp), %r11
	jne	.L581
	movq	-912(%rbp), %rdi
	movq	%rdx, %rsi
	call	_ZN6icu_676number4impl10AffixUtils18hasCurrencySymbolsERKNS_13UnicodeStringER10UErrorCode@PLT
	movq	-944(%rbp), %rdx
	movq	-952(%rbp), %r9
	testb	%al, %al
	movq	-960(%rbp), %r10
	movq	-968(%rbp), %r11
	jne	.L581
	movq	%rdx, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_676number4impl10AffixUtils18hasCurrencySymbolsERKNS_13UnicodeStringER10UErrorCode@PLT
	movq	-944(%rbp), %rdx
	movq	-952(%rbp), %r9
	testb	%al, %al
	movq	-960(%rbp), %r10
	movq	-968(%rbp), %r11
	jne	.L581
	jmp	.L580
	.p2align 4,,10
	.p2align 3
.L599:
	movq	%rsi, %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	jmp	.L575
.L600:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4014:
	.size	_ZNK6icu_6713DecimalFormat9toPatternERNS_13UnicodeStringE, .-_ZNK6icu_6713DecimalFormat9toPatternERNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713DecimalFormatD0Ev
	.type	_ZN6icu_6713DecimalFormatD0Ev, @function
_ZN6icu_6713DecimalFormatD0Ev:
.LFB3930:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6713DecimalFormatE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rax, (%rdi)
	movq	360(%rdi), %rax
	testq	%rax, %rax
	je	.L606
	xorl	%edi, %edi
	xchgq	1232(%rax), %rdi
	testq	%rdi, %rdi
	je	.L604
	movq	(%rdi), %rax
	call	*8(%rax)
.L604:
	movq	360(%r13), %rax
	xorl	%edi, %edi
	xchgq	1240(%rax), %rdi
	testq	%rdi, %rdi
	je	.L605
	movq	(%rdi), %rax
	call	*8(%rax)
.L605:
	movq	360(%r13), %r12
	testq	%r12, %r12
	je	.L606
	leaq	3840(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	3776(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	3712(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	3648(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	3560(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	3488(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	3424(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	3360(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	3296(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	3216(%r12), %rdi
	testq	%rdi, %rdi
	je	.L607
	movq	(%rdi), %rax
	call	*8(%rax)
.L607:
	leaq	3184(%r12), %rdi
	leaq	1520(%r12), %r15
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	leaq	16+_ZTVN6icu_676number4impl31CurrencyPluralInfoAffixProviderE(%rip), %rax
	leaq	2888(%r12), %rbx
	movq	%rax, 1520(%r12)
	leaq	1256(%r12), %r14
	.p2align 4,,10
	.p2align 3
.L608:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	subq	$272, %rbx
	call	*(%rax)
	cmpq	%rbx, %r14
	jne	.L608
	movq	%r15, %rdi
	leaq	1248(%r12), %r15
	call	_ZN6icu_676number4impl20AffixPatternProviderD2Ev@PLT
	leaq	16+_ZTVN6icu_676number4impl30PropertiesAffixPatternProviderE(%rip), %rax
	leaq	1448(%r12), %rdi
	movq	%rax, 1248(%r12)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	1384(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	1320(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_676number4impl20AffixPatternProviderD2Ev@PLT
	leaq	776(%r12), %rdi
	call	_ZN6icu_676number24LocalizedNumberFormatterD1Ev@PLT
	movq	768(%r12), %rdi
	testq	%rdi, %rdi
	je	.L609
	movq	(%rdi), %rax
	call	*8(%rax)
.L609:
	leaq	680(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	616(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	552(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	488(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	400(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	328(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	264(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	200(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	136(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	56(%r12), %rdi
	testq	%rdi, %rdi
	je	.L610
	movq	(%rdi), %rax
	call	*8(%rax)
.L610:
	leaq	24(%r12), %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L606:
	movq	%r13, %rdi
	call	_ZN6icu_6712NumberFormatD2Ev@PLT
	addq	$8, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3930:
	.size	_ZN6icu_6713DecimalFormatD0Ev, .-_ZN6icu_6713DecimalFormatD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713DecimalFormatD2Ev
	.type	_ZN6icu_6713DecimalFormatD2Ev, @function
_ZN6icu_6713DecimalFormatD2Ev:
.LFB3928:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6713DecimalFormatE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rax, (%rdi)
	movq	360(%rdi), %rax
	testq	%rax, %rax
	je	.L635
	xorl	%edi, %edi
	xchgq	1232(%rax), %rdi
	testq	%rdi, %rdi
	je	.L633
	movq	(%rdi), %rax
	call	*8(%rax)
.L633:
	movq	360(%r13), %rax
	xorl	%edi, %edi
	xchgq	1240(%rax), %rdi
	testq	%rdi, %rdi
	je	.L634
	movq	(%rdi), %rax
	call	*8(%rax)
.L634:
	movq	360(%r13), %r12
	testq	%r12, %r12
	je	.L635
	leaq	3840(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	3776(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	3712(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	3648(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	3560(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	3488(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	3424(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	3360(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	3296(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	3216(%r12), %rdi
	testq	%rdi, %rdi
	je	.L636
	movq	(%rdi), %rax
	call	*8(%rax)
.L636:
	leaq	3184(%r12), %rdi
	leaq	1520(%r12), %r15
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	leaq	16+_ZTVN6icu_676number4impl31CurrencyPluralInfoAffixProviderE(%rip), %rax
	leaq	2888(%r12), %rbx
	movq	%rax, 1520(%r12)
	leaq	1256(%r12), %r14
	.p2align 4,,10
	.p2align 3
.L637:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	subq	$272, %rbx
	call	*(%rax)
	cmpq	%rbx, %r14
	jne	.L637
	movq	%r15, %rdi
	leaq	1248(%r12), %r15
	call	_ZN6icu_676number4impl20AffixPatternProviderD2Ev@PLT
	leaq	16+_ZTVN6icu_676number4impl30PropertiesAffixPatternProviderE(%rip), %rax
	leaq	1448(%r12), %rdi
	movq	%rax, 1248(%r12)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	1384(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	1320(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_676number4impl20AffixPatternProviderD2Ev@PLT
	leaq	776(%r12), %rdi
	call	_ZN6icu_676number24LocalizedNumberFormatterD1Ev@PLT
	movq	768(%r12), %rdi
	testq	%rdi, %rdi
	je	.L638
	movq	(%rdi), %rax
	call	*8(%rax)
.L638:
	leaq	680(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	616(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	552(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	488(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	400(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	328(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	264(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	200(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	136(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	56(%r12), %rdi
	testq	%rdi, %rdi
	je	.L639
	movq	(%rdi), %rax
	call	*8(%rax)
.L639:
	leaq	24(%r12), %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L635:
	addq	$8, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6712NumberFormatD2Ev@PLT
	.cfi_endproc
.LFE3928:
	.size	_ZN6icu_6713DecimalFormatD2Ev, .-_ZN6icu_6713DecimalFormatD2Ev
	.globl	_ZN6icu_6713DecimalFormatD1Ev
	.set	_ZN6icu_6713DecimalFormatD1Ev,_ZN6icu_6713DecimalFormatD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713DecimalFormatC2EPKNS_20DecimalFormatSymbolsER10UErrorCode
	.type	_ZN6icu_6713DecimalFormatC2EPKNS_20DecimalFormatSymbolsER10UErrorCode, @function
_ZN6icu_6713DecimalFormatC2EPKNS_20DecimalFormatSymbolsER10UErrorCode:
.LFB3908:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	call	_ZN6icu_6712NumberFormatC2Ev@PLT
	movl	0(%r13), %r10d
	leaq	16+_ZTVN6icu_6713DecimalFormatE(%rip), %rax
	movq	$0, 360(%r12)
	movq	%rax, (%r12)
	testl	%r10d, %r10d
	jle	.L701
.L661:
	testq	%r15, %r15
	je	.L660
	movq	(%r15), %rax
	movq	%r15, %rdi
	movq	8(%rax), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L701:
	.cfi_restore_state
	movl	$3944, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L662
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	leaq	8(%rbx), %rdi
	movq	.LC2(%rip), %xmm0
	movq	%rax, %xmm2
	leaq	16+_ZTVN6icu_676number4impl31CurrencyPluralInfoAffixProviderE(%rip), %r14
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN6icu_676number4impl23DecimalFormatPropertiesC1Ev@PLT
	leaq	792(%rbx), %rdi
	movq	$0, 768(%rbx)
	movq	$2, 780(%rbx)
	movl	$0, 788(%rbx)
	call	_ZN6icu_6711MeasureUnitC1Ev@PLT
	leaq	816(%rbx), %rdi
	call	_ZN6icu_6711MeasureUnitC1Ev@PLT
	movl	$-3, %edx
	movl	$-1, %ecx
	movabsq	$30064771077, %rax
	movw	%dx, 876(%rbx)
	pxor	%xmm1, %xmm1
	leaq	992(%rbx), %rdi
	movw	%cx, 900(%rbx)
	movq	%rax, 928(%rbx)
	movl	$0, 840(%rbx)
	movl	$4, 872(%rbx)
	movl	$-2, 888(%rbx)
	movb	$0, 908(%rbx)
	movl	$0, 912(%rbx)
	movq	$0, 920(%rbx)
	movl	$2, 936(%rbx)
	movl	$0, 944(%rbx)
	movq	$0, 952(%rbx)
	movl	$0, 960(%rbx)
	movl	$3, 984(%rbx)
	movups	%xmm1, 968(%rbx)
	call	_ZN6icu_676LocaleC1Ev@PLT
	movl	$2, %esi
	movl	$2, %edi
	movl	$2, %r8d
	movl	$2, %r9d
	movdqa	-64(%rbp), %xmm0
	movw	%si, 1264(%rbx)
	movl	$2, %r10d
	movw	%di, 1328(%rbx)
	movl	$2, %r11d
	movl	$2, %edx
	movl	$2, %ecx
	movl	$2, %esi
	movl	$2, %edi
	pxor	%xmm1, %xmm1
	movw	%r8w, 1392(%rbx)
	movw	%r9w, 1456(%rbx)
	movl	$2, %r8d
	movl	$2, %r9d
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%r10w, 1544(%rbx)
	movl	$2, %r10d
	movw	%r11w, 1608(%rbx)
	movl	$2, %r11d
	movw	%dx, 1672(%rbx)
	movl	$2, %edx
	movw	%cx, 1736(%rbx)
	movl	$2, %ecx
	movw	%si, 1816(%rbx)
	movl	$2, %esi
	movw	%di, 1880(%rbx)
	movl	$2, %edi
	movw	%r8w, 1944(%rbx)
	movl	$2, %r8d
	movw	%r9w, 2008(%rbx)
	movl	$2, %r9d
	movq	%rax, 1320(%rbx)
	movq	%rax, 1384(%rbx)
	movq	%rax, 1448(%rbx)
	movq	%rax, 1600(%rbx)
	movq	%rax, 1664(%rbx)
	movq	%rax, 1728(%rbx)
	movq	%rax, 1872(%rbx)
	movq	%rax, 1936(%rbx)
	movq	%rax, 2000(%rbx)
	movups	%xmm1, 1232(%rbx)
	movups	%xmm0, 1248(%rbx)
	movups	%xmm0, 1528(%rbx)
	movups	%xmm0, 1800(%rbx)
	movq	$0, 1216(%rbx)
	movq	$0, 1224(%rbx)
	movb	$1, 1513(%rbx)
	movq	%r14, 1520(%rbx)
	movb	$1, 1793(%rbx)
	movb	$1, 2065(%rbx)
	movups	%xmm0, 2072(%rbx)
	movw	%r10w, 2088(%rbx)
	movl	$2, %r10d
	movw	%r11w, 2152(%rbx)
	movl	$2, %r11d
	movw	%dx, 2216(%rbx)
	movl	$2, %edx
	movw	%cx, 2280(%rbx)
	movl	$2, %ecx
	movw	%si, 2360(%rbx)
	movl	$2, %esi
	movw	%di, 2424(%rbx)
	movl	$2, %edi
	movw	%r8w, 2488(%rbx)
	movl	$2, %r8d
	movw	%r9w, 2552(%rbx)
	movl	$2, %r9d
	movq	%rax, 2144(%rbx)
	movq	%rax, 2208(%rbx)
	movq	%rax, 2272(%rbx)
	movq	%rax, 2416(%rbx)
	movq	%rax, 2480(%rbx)
	movq	%rax, 2544(%rbx)
	movw	%r10w, 2632(%rbx)
	movq	%rax, 2688(%rbx)
	movw	%r11w, 2696(%rbx)
	movq	%rax, 2752(%rbx)
	movw	%dx, 2760(%rbx)
	movq	%rax, 2816(%rbx)
	movw	%cx, 2824(%rbx)
	movw	%si, 2904(%rbx)
	movq	%rax, 2960(%rbx)
	movw	%di, 2968(%rbx)
	leaq	3168(%rbx), %rdi
	movq	%rax, 3024(%rbx)
	movw	%r8w, 3032(%rbx)
	movups	%xmm0, 2344(%rbx)
	movups	%xmm0, 2616(%rbx)
	movups	%xmm0, 2888(%rbx)
	movb	$1, 2337(%rbx)
	movb	$1, 2609(%rbx)
	movb	$1, 2881(%rbx)
	movq	%rax, 3088(%rbx)
	movw	%r9w, 3096(%rbx)
	movb	$1, 3153(%rbx)
	movb	$1, 3160(%rbx)
	call	_ZN6icu_676number4impl23DecimalFormatPropertiesC1Ev@PLT
	movb	$0, 3928(%rbx)
	movq	%rbx, 360(%r12)
	testq	%r15, %r15
	je	.L702
	movl	0(%r13), %eax
	testl	%eax, %eax
	jle	.L703
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*8(%rax)
	movl	0(%r13), %eax
.L668:
	testl	%eax, %eax
	jle	.L660
.L667:
	movq	360(%r12), %r13
	testq	%r13, %r13
	je	.L679
	leaq	3840(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	3776(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	3712(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	3648(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	3560(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	3488(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	3424(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	3360(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	3296(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	3216(%r13), %rdi
	testq	%rdi, %rdi
	je	.L672
	movq	(%rdi), %rax
	call	*8(%rax)
.L672:
	leaq	3184(%r13), %rdi
	leaq	1520(%r13), %r15
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	leaq	3160(%r13), %rbx
	movq	%r14, 1520(%r13)
	leaq	1528(%r13), %r14
	.p2align 4,,10
	.p2align 3
.L673:
	movq	-272(%rbx), %rdx
	subq	$272, %rbx
	movq	%rbx, %rdi
	call	*(%rdx)
	cmpq	%r14, %rbx
	jne	.L673
	movq	%r15, %rdi
	leaq	1248(%r13), %r14
	call	_ZN6icu_676number4impl20AffixPatternProviderD2Ev@PLT
	leaq	16+_ZTVN6icu_676number4impl30PropertiesAffixPatternProviderE(%rip), %rax
	leaq	1448(%r13), %rdi
	movq	%rax, 1248(%r13)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	1384(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	1320(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	1256(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl20AffixPatternProviderD2Ev@PLT
	leaq	776(%r13), %rdi
	call	_ZN6icu_676number24LocalizedNumberFormatterD1Ev@PLT
	movq	768(%r13), %rdi
	testq	%rdi, %rdi
	je	.L674
	movq	(%rdi), %rax
	call	*8(%rax)
.L674:
	leaq	680(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	616(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	552(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	488(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	400(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	328(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	264(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	200(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	136(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	56(%r13), %rdi
	testq	%rdi, %rdi
	je	.L675
	movq	(%rdi), %rax
	call	*8(%rax)
.L675:
	leaq	24(%r13), %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L679:
	movq	$0, 360(%r12)
.L660:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L703:
	.cfi_restore_state
	movq	768(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L670
	movq	(%rdi), %rax
	call	*8(%rax)
	movl	0(%r13), %eax
.L670:
	movq	%r15, 768(%rbx)
	jmp	.L668
	.p2align 4,,10
	.p2align 3
.L702:
	movl	$2816, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L664
	movq	%rax, %rdi
	movq	%r13, %rsi
	movq	%rax, -64(%rbp)
	call	_ZN6icu_6720DecimalFormatSymbolsC1ER10UErrorCode@PLT
	movl	0(%r13), %eax
	movq	-64(%rbp), %r8
	testl	%eax, %eax
	jg	.L704
	movq	768(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L682
	movq	(%rdi), %rax
	movq	%r8, -64(%rbp)
	call	*8(%rax)
	movq	-64(%rbp), %r8
	movl	0(%r13), %eax
	movq	%r8, 768(%rbx)
	jmp	.L668
	.p2align 4,,10
	.p2align 3
.L682:
	movq	%r8, 768(%rbx)
	jmp	.L660
	.p2align 4,,10
	.p2align 3
.L704:
	movq	%r8, %rdi
	call	_ZN6icu_6720DecimalFormatSymbolsD0Ev@PLT
	movl	0(%r13), %eax
	jmp	.L668
.L662:
	movq	$0, 360(%r12)
	movl	$7, 0(%r13)
	jmp	.L661
.L664:
	cmpl	$0, 0(%r13)
	jg	.L667
	movq	768(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L681
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	$0, 768(%rbx)
.L681:
	movl	$7, 0(%r13)
	jmp	.L667
	.cfi_endproc
.LFE3908:
	.size	_ZN6icu_6713DecimalFormatC2EPKNS_20DecimalFormatSymbolsER10UErrorCode, .-_ZN6icu_6713DecimalFormatC2EPKNS_20DecimalFormatSymbolsER10UErrorCode
	.globl	_ZN6icu_6713DecimalFormatC1EPKNS_20DecimalFormatSymbolsER10UErrorCode
	.set	_ZN6icu_6713DecimalFormatC1EPKNS_20DecimalFormatSymbolsER10UErrorCode,_ZN6icu_6713DecimalFormatC2EPKNS_20DecimalFormatSymbolsER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713DecimalFormat5parseERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE
	.type	_ZNK6icu_6713DecimalFormat5parseERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE, @function
_ZNK6icu_6713DecimalFormat5parseERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE:
.LFB3953:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$264, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 360(%rdi)
	je	.L705
	movq	%rdx, %r13
	movl	8(%rcx), %edx
	movswl	8(%rsi), %eax
	movq	%rsi, %r12
	movq	%rcx, %rbx
	testl	%edx, %edx
	js	.L707
	movq	%rdi, %r15
	testw	%ax, %ax
	js	.L708
	sarl	$5, %eax
	cmpl	%edx, %eax
	jg	.L710
.L709:
	cmpl	%edx, %eax
	je	.L724
.L705:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L725
	addq	$264, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L708:
	.cfi_restore_state
	movl	12(%rsi), %eax
	cmpl	%edx, %eax
	jle	.L709
.L710:
	leaq	-272(%rbp), %r14
	leaq	16+_ZTVN6icu_679ErrorCodeE(%rip), %rax
	movl	$0, -280(%rbp)
	movq	%r14, %rdi
	movq	%rax, -288(%rbp)
	call	_ZN6icu_678numparse4impl12ParsedNumberC1Ev@PLT
	movl	-280(%rbp), %esi
	testl	%esi, %esi
	jg	.L716
	movl	8(%rbx), %eax
	movl	%eax, -292(%rbp)
	movq	360(%r15), %rax
	movq	1232(%rax), %r9
	movq	%r9, %r10
	testq	%r9, %r9
	jne	.L714
	movq	360(%r15), %rdi
	leaq	-280(%rbp), %rcx
	xorl	%edx, %edx
	movq	%r9, -304(%rbp)
	movq	768(%rdi), %rsi
	addq	$8, %rdi
	call	_ZN6icu_678numparse4impl16NumberParserImpl26createParserFromPropertiesERKNS_6number4impl23DecimalFormatPropertiesERKNS_20DecimalFormatSymbolsEbR10UErrorCode@PLT
	movl	-280(%rbp), %ecx
	movq	%rax, %r10
	testl	%ecx, %ecx
	jg	.L716
	testq	%rax, %rax
	movq	-304(%rbp), %r9
	je	.L726
	movq	360(%r15), %rdx
	movq	%r9, %rax
	lock cmpxchgq	%r10, 1232(%rdx)
	jne	.L727
.L714:
	movl	-280(%rbp), %edx
	testl	%edx, %edx
	jg	.L716
	movl	-292(%rbp), %edx
	movq	%r14, %r8
	movq	%r12, %rsi
	movq	%r10, %rdi
	leaq	-280(%rbp), %r9
	movl	$1, %ecx
	movq	%r10, -304(%rbp)
	call	_ZNK6icu_678numparse4impl16NumberParserImpl5parseERKNS_13UnicodeStringEibRNS1_12ParsedNumberER10UErrorCode@PLT
	movl	-280(%rbp), %eax
	testl	%eax, %eax
	jle	.L728
.L716:
	leaq	-128(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	-192(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	leaq	-288(%rbp), %rdi
	call	_ZN6icu_679ErrorCodeD1Ev@PLT
	jmp	.L705
	.p2align 4,,10
	.p2align 3
.L707:
	testw	%ax, %ax
	jns	.L705
	movl	12(%rsi), %eax
	jmp	.L709
	.p2align 4,,10
	.p2align 3
.L724:
	movl	%eax, 12(%rbx)
	jmp	.L705
	.p2align 4,,10
	.p2align 3
.L728:
	movq	%r14, %rdi
	call	_ZNK6icu_678numparse4impl12ParsedNumber7successEv@PLT
	movq	-304(%rbp), %r10
	testb	%al, %al
	jne	.L729
	movl	-292(%rbp), %r15d
	addl	-200(%rbp), %r15d
	movl	%r15d, 12(%rbx)
	jmp	.L716
	.p2align 4,,10
	.p2align 3
.L729:
	movl	-200(%rbp), %eax
	movq	%r10, %rdi
	movl	%eax, 8(%rbx)
	call	_ZNK6icu_678numparse4impl16NumberParserImpl13getParseFlagsEv@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movl	%eax, %edx
	call	_ZNK6icu_678numparse4impl12ParsedNumber19populateFormattableERNS_11FormattableEi@PLT
	jmp	.L716
.L726:
	movl	$7, -280(%rbp)
	jmp	.L716
.L725:
	call	__stack_chk_fail@PLT
.L727:
	movq	(%r10), %rdx
	movq	%rax, -304(%rbp)
	movq	%r10, %rdi
	call	*8(%rdx)
	movq	-304(%rbp), %rax
	movq	%rax, %r10
	jmp	.L714
	.cfi_endproc
.LFE3953:
	.size	_ZNK6icu_6713DecimalFormat5parseERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE, .-_ZNK6icu_6713DecimalFormat5parseERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE
	.align 2
	.p2align 4
	.type	_ZNK6icu_6713DecimalFormat13parseCurrencyERKNS_13UnicodeStringERNS_13ParsePositionE.part.0, @function
_ZNK6icu_6713DecimalFormat13parseCurrencyERKNS_13UnicodeStringERNS_13ParsePositionE.part.0:
.LFB5395:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-272(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$392, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_679ErrorCodeE(%rip), %rax
	movl	$0, -392(%rbp)
	movq	%rax, -400(%rbp)
	call	_ZN6icu_678numparse4impl12ParsedNumberC1Ev@PLT
	movl	-392(%rbp), %edi
	testl	%edi, %edi
	jg	.L750
	movl	8(%r12), %r15d
	movq	360(%rbx), %rax
	movq	1240(%rax), %r8
	movq	%r8, %r10
	testq	%r8, %r8
	je	.L751
.L734:
	movl	-392(%rbp), %esi
	testl	%esi, %esi
	jg	.L750
	leaq	-392(%rbp), %rbx
	movq	%r13, %r8
	movl	%r15d, %edx
	movq	%r14, %rsi
	movl	$1, %ecx
	movq	%rbx, %r9
	movq	%r10, %rdi
	movq	%r10, -424(%rbp)
	call	_ZNK6icu_678numparse4impl16NumberParserImpl5parseERKNS_13UnicodeStringEibRNS1_12ParsedNumberER10UErrorCode@PLT
	movl	-392(%rbp), %ecx
	testl	%ecx, %ecx
	jle	.L752
.L750:
	xorl	%r12d, %r12d
.L732:
	leaq	-128(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	-192(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	leaq	-400(%rbp), %rdi
	call	_ZN6icu_679ErrorCodeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L753
	addq	$392, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L751:
	.cfi_restore_state
	movq	360(%rbx), %rdi
	leaq	-392(%rbp), %rcx
	movl	$1, %edx
	movq	%r8, -424(%rbp)
	movq	768(%rdi), %rsi
	addq	$8, %rdi
	call	_ZN6icu_678numparse4impl16NumberParserImpl26createParserFromPropertiesERKNS_6number4impl23DecimalFormatPropertiesERKNS_20DecimalFormatSymbolsEbR10UErrorCode@PLT
	movq	-424(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %r10
	je	.L754
	movq	360(%rbx), %rdx
	movq	%r8, %rax
	lock cmpxchgq	%r10, 1240(%rdx)
	je	.L734
	movq	(%r10), %rdx
	movq	%rax, -424(%rbp)
	movq	%r10, %rdi
	call	*8(%rdx)
	movq	-424(%rbp), %rax
	movq	%rax, %r10
	jmp	.L734
	.p2align 4,,10
	.p2align 3
.L752:
	movq	%r13, %rdi
	call	_ZNK6icu_678numparse4impl12ParsedNumber7successEv@PLT
	testb	%al, %al
	je	.L737
	movl	-200(%rbp), %eax
	leaq	-384(%rbp), %r14
	movq	%r14, %rdi
	movl	%eax, 8(%r12)
	call	_ZN6icu_6711FormattableC1Ev@PLT
	movq	-424(%rbp), %r10
	movq	%r10, %rdi
	call	_ZNK6icu_678numparse4impl16NumberParserImpl13getParseFlagsEv@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movl	%eax, %edx
	call	_ZNK6icu_678numparse4impl12ParsedNumber19populateFormattableERNS_11FormattableEi@PLT
	leaq	-64(%rbp), %rax
	movl	$128, %edi
	movq	%rax, -408(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L738
	leaq	-408(%rbp), %rdx
	movq	%rbx, %rcx
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6714CurrencyAmountC1ERKNS_11FormattableENS_14ConstChar16PtrER10UErrorCode@PLT
.L739:
	movl	-392(%rbp), %edx
	testl	%edx, %edx
	jg	.L755
.L740:
	movq	%r14, %rdi
	call	_ZN6icu_6711FormattableD1Ev@PLT
	jmp	.L732
	.p2align 4,,10
	.p2align 3
.L754:
	movq	360(%rbx), %rdx
	movl	$7, -392(%rbp)
	lock cmpxchgq	%rax, 1240(%rdx)
	cmovne	%rax, %r10
	jmp	.L734
	.p2align 4,,10
	.p2align 3
.L737:
	addl	-200(%rbp), %r15d
	movl	%r15d, 12(%r12)
	jmp	.L750
	.p2align 4,,10
	.p2align 3
.L755:
	testq	%r12, %r12
	je	.L740
	movq	(%r12), %rax
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	*8(%rax)
	jmp	.L740
.L738:
	movl	-392(%rbp), %eax
	testl	%eax, %eax
	jg	.L739
	movl	$7, -392(%rbp)
	jmp	.L739
.L753:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5395:
	.size	_ZNK6icu_6713DecimalFormat13parseCurrencyERKNS_13UnicodeStringERNS_13ParsePositionE.part.0, .-_ZNK6icu_6713DecimalFormat13parseCurrencyERKNS_13UnicodeStringERNS_13ParsePositionE.part.0
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713DecimalFormat13parseCurrencyERKNS_13UnicodeStringERNS_13ParsePositionE
	.type	_ZNK6icu_6713DecimalFormat13parseCurrencyERKNS_13UnicodeStringERNS_13ParsePositionE, @function
_ZNK6icu_6713DecimalFormat13parseCurrencyERKNS_13UnicodeStringERNS_13ParsePositionE:
.LFB3957:
	.cfi_startproc
	endbr64
	cmpq	$0, 360(%rdi)
	je	.L756
	movl	8(%rdx), %ecx
	testl	%ecx, %ecx
	js	.L756
	movswl	8(%rsi), %eax
	testw	%ax, %ax
	js	.L758
	sarl	$5, %eax
	cmpl	%ecx, %eax
	jle	.L756
.L765:
	jmp	_ZNK6icu_6713DecimalFormat13parseCurrencyERKNS_13UnicodeStringERNS_13ParsePositionE.part.0
	.p2align 4,,10
	.p2align 3
.L758:
	movl	12(%rsi), %eax
	cmpl	%ecx, %eax
	jg	.L765
.L756:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3957:
	.size	_ZNK6icu_6713DecimalFormat13parseCurrencyERKNS_13UnicodeStringERNS_13ParsePositionE, .-_ZNK6icu_6713DecimalFormat13parseCurrencyERKNS_13UnicodeStringERNS_13ParsePositionE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713DecimalFormat23formatToDecimalQuantityEdRNS_6number4impl15DecimalQuantityER10UErrorCode
	.type	_ZNK6icu_6713DecimalFormat23formatToDecimalQuantityEdRNS_6number4impl15DecimalQuantityER10UErrorCode, @function
_ZNK6icu_6713DecimalFormat23formatToDecimalQuantityEdRNS_6number4impl15DecimalQuantityER10UErrorCode:
.LFB4034:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$40, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L766
	movq	%rsi, %r13
	movq	360(%rdi), %rsi
	movq	%rdx, %r12
	testq	%rsi, %rsi
	je	.L771
	leaq	-64(%rbp), %r14
	addq	$776, %rsi
	movq	%r14, %rdi
	call	_ZNK6icu_676number24LocalizedNumberFormatter12formatDoubleEdR10UErrorCode@PLT
	movq	%r14, %rdi
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	_ZNK6icu_676number15FormattedNumber18getDecimalQuantityERNS0_4impl15DecimalQuantityER10UErrorCode@PLT
	movq	%r14, %rdi
	call	_ZN6icu_676number15FormattedNumberD1Ev@PLT
.L766:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L772
	addq	$40, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L771:
	.cfi_restore_state
	movl	$7, (%rdx)
	jmp	.L766
.L772:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4034:
	.size	_ZNK6icu_6713DecimalFormat23formatToDecimalQuantityEdRNS_6number4impl15DecimalQuantityER10UErrorCode, .-_ZNK6icu_6713DecimalFormat23formatToDecimalQuantityEdRNS_6number4impl15DecimalQuantityER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713DecimalFormat5touchER10UErrorCode
	.type	_ZN6icu_6713DecimalFormat5touchER10UErrorCode, @function
_ZN6icu_6713DecimalFormat5touchER10UErrorCode:
.LFB4038:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$1144, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L773
	movq	360(%rdi), %rax
	movq	%rdi, %r12
	movq	%rsi, %r13
	testq	%rax, %rax
	je	.L790
	movq	768(%rax), %rsi
	leaq	-1184(%rbp), %r15
	leaq	-960(%rbp), %rbx
	movq	%r15, %rdi
	leaq	-512(%rbp), %r14
	addq	$1872, %rsi
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movq	360(%r12), %rax
	movq	%r13, %r9
	movq	%rbx, %rdi
	movq	768(%rax), %rdx
	leaq	1248(%rax), %rcx
	leaq	3168(%rax), %r8
	leaq	8(%rax), %rsi
	call	_ZN6icu_676number4impl20NumberPropertyMapper6createERKNS1_23DecimalFormatPropertiesERKNS_20DecimalFormatSymbolsERNS1_22DecimalFormatWarehouseERS3_R10UErrorCode@PLT
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZNO6icu_676number26UnlocalizedNumberFormatter6localeERKNS_6LocaleE@PLT
	movq	360(%r12), %rax
	movq	%r14, %rsi
	leaq	776(%rax), %rdi
	call	_ZN6icu_676number24LocalizedNumberFormatteraSEOS1_@PLT
	movq	%r14, %rdi
	call	_ZN6icu_676number24LocalizedNumberFormatterD1Ev@PLT
	leaq	-744(%rbp), %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	leaq	-792(%rbp), %rdi
	call	_ZN6icu_676number5ScaleD1Ev@PLT
	leaq	-824(%rbp), %rdi
	call	_ZN6icu_676number4impl14SymbolsWrapperD1Ev@PLT
	leaq	-920(%rbp), %rdi
	call	_ZN6icu_6711MeasureUnitD1Ev@PLT
	leaq	-944(%rbp), %rdi
	call	_ZN6icu_6711MeasureUnitD1Ev@PLT
	movq	360(%r12), %rax
	leaq	8(%rax), %rdi
	call	_ZNK6icu_676number4impl23DecimalFormatProperties29equalsDefaultExceptFastFormatEv@PLT
	testb	%al, %al
	je	.L791
	movq	%r12, %rdi
	call	_ZN6icu_6713DecimalFormat15setupFastFormatEv.part.0
	movq	360(%r12), %rax
.L777:
	xorl	%edi, %edi
	xchgq	1232(%rax), %rdi
	testq	%rdi, %rdi
	je	.L778
	movq	(%rdi), %rax
	call	*8(%rax)
.L778:
	movq	360(%r12), %rax
	xorl	%edi, %edi
	xchgq	1240(%rax), %rdi
	testq	%rdi, %rdi
	je	.L779
	movq	(%rdi), %rax
	call	*8(%rax)
.L779:
	movq	360(%r12), %rsi
	cmpb	$0, 3176(%rsi)
	je	.L780
	movl	$65554, 0(%r13)
.L780:
	addq	$3184, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6712CurrencyUnitC1ERKS0_@PLT
	movq	%r13, %rdx
	leaq	-492(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6712NumberFormat11setCurrencyEPKDsR10UErrorCode@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	movq	360(%r12), %rax
	movq	%r12, %rdi
	movl	3256(%rax), %esi
	call	_ZN6icu_6712NumberFormat23setMaximumIntegerDigitsEi@PLT
	movq	360(%r12), %rax
	movq	%r12, %rdi
	movl	3276(%rax), %esi
	call	_ZN6icu_6712NumberFormat23setMinimumIntegerDigitsEi@PLT
	movq	360(%r12), %rax
	movq	%r12, %rdi
	movl	3252(%rax), %esi
	call	_ZN6icu_6712NumberFormat24setMaximumFractionDigitsEi@PLT
	movq	360(%r12), %rax
	movq	%r12, %rdi
	movl	3268(%rax), %esi
	call	_ZN6icu_6712NumberFormat24setMinimumFractionDigitsEi@PLT
	movq	360(%r12), %rax
	movq	%r12, %rdi
	movzbl	84(%rax), %esi
	call	_ZN6icu_6712NumberFormat15setGroupingUsedEa@PLT
	movq	%r15, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
.L773:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L792
	addq	$1144, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L790:
	.cfi_restore_state
	movl	$7, (%rsi)
	jmp	.L773
	.p2align 4,,10
	.p2align 3
.L791:
	movq	360(%r12), %rax
	movb	$0, 3928(%rax)
	jmp	.L777
.L792:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4038:
	.size	_ZN6icu_6713DecimalFormat5touchER10UErrorCode, .-_ZN6icu_6713DecimalFormat5touchER10UErrorCode
	.align 2
	.p2align 4
	.type	_ZN6icu_6713DecimalFormatC2ER10UErrorCode.part.0, @function
_ZN6icu_6713DecimalFormatC2ER10UErrorCode.part.0:
.LFB5396:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	leaq	-288(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$328, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_676Locale10getDefaultEv@PLT
	movq	%r12, %rdi
	movq	40(%rax), %rsi
	movq	%rsi, -360(%rbp)
	call	_ZN6icu_6715NumberingSystem14createInstanceER10UErrorCode@PLT
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZNK6icu_6715NumberingSystem7getNameEv@PLT
	movq	-360(%rbp), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rbx
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	movq	%r12, %rcx
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	leaq	-352(%rbp), %rbx
	call	_ZN6icu_676number4impl5utils18getPatternForStyleERKNS_6LocaleEPKcNS1_16CldrPatternStyleER10UErrorCode@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%r15, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	movl	(%r12), %eax
	testl	%eax, %eax
	jle	.L801
.L794:
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6713DecimalFormat5touchER10UErrorCode
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	testq	%r13, %r13
	je	.L793
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*8(%rax)
.L793:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L802
	addq	$328, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L801:
	.cfi_restore_state
	movq	360(%r14), %rax
	movq	%r12, %rcx
	movl	$1, %edx
	movq	%rbx, %rdi
	leaq	8(%rax), %rsi
	call	_ZN6icu_676number4impl13PatternParser25parseToExistingPropertiesERKNS_13UnicodeStringERNS1_23DecimalFormatPropertiesENS1_14IgnoreRoundingER10UErrorCode@PLT
	jmp	.L794
.L802:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5396:
	.size	_ZN6icu_6713DecimalFormatC2ER10UErrorCode.part.0, .-_ZN6icu_6713DecimalFormatC2ER10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713DecimalFormatC2ER10UErrorCode
	.type	_ZN6icu_6713DecimalFormatC2ER10UErrorCode, @function
_ZN6icu_6713DecimalFormatC2ER10UErrorCode:
.LFB3891:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	xorl	%esi, %esi
	call	_ZN6icu_6713DecimalFormatC1EPKNS_20DecimalFormatSymbolsER10UErrorCode
	movl	(%r12), %eax
	testl	%eax, %eax
	jle	.L806
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L806:
	.cfi_restore_state
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6713DecimalFormatC2ER10UErrorCode.part.0
	.cfi_endproc
.LFE3891:
	.size	_ZN6icu_6713DecimalFormatC2ER10UErrorCode, .-_ZN6icu_6713DecimalFormatC2ER10UErrorCode
	.globl	_ZN6icu_6713DecimalFormatC1ER10UErrorCode
	.set	_ZN6icu_6713DecimalFormatC1ER10UErrorCode,_ZN6icu_6713DecimalFormatC2ER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713DecimalFormatC2ERKNS_13UnicodeStringEPNS_20DecimalFormatSymbolsER10UErrorCode
	.type	_ZN6icu_6713DecimalFormatC2ERKNS_13UnicodeStringEPNS_20DecimalFormatSymbolsER10UErrorCode, @function
_ZN6icu_6713DecimalFormatC2ERKNS_13UnicodeStringEPNS_20DecimalFormatSymbolsER10UErrorCode:
.LFB3897:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	movq	%rdx, %rsi
	pushq	%r13
	movq	%rcx, %rdx
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rcx, %r12
	subq	$8, %rsp
	call	_ZN6icu_6713DecimalFormatC1EPKNS_20DecimalFormatSymbolsER10UErrorCode
	movl	(%r12), %eax
	testl	%eax, %eax
	jle	.L810
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L810:
	.cfi_restore_state
	movq	360(%r13), %rax
	movq	%r14, %rdi
	movq	%r12, %rcx
	movl	$1, %edx
	leaq	8(%rax), %rsi
	call	_ZN6icu_676number4impl13PatternParser25parseToExistingPropertiesERKNS_13UnicodeStringERNS1_23DecimalFormatPropertiesENS1_14IgnoreRoundingER10UErrorCode@PLT
	addq	$8, %rsp
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6713DecimalFormat5touchER10UErrorCode
	.cfi_endproc
.LFE3897:
	.size	_ZN6icu_6713DecimalFormatC2ERKNS_13UnicodeStringEPNS_20DecimalFormatSymbolsER10UErrorCode, .-_ZN6icu_6713DecimalFormatC2ERKNS_13UnicodeStringEPNS_20DecimalFormatSymbolsER10UErrorCode
	.globl	_ZN6icu_6713DecimalFormatC1ERKNS_13UnicodeStringEPNS_20DecimalFormatSymbolsER10UErrorCode
	.set	_ZN6icu_6713DecimalFormatC1ERKNS_13UnicodeStringEPNS_20DecimalFormatSymbolsER10UErrorCode,_ZN6icu_6713DecimalFormatC2ERKNS_13UnicodeStringEPNS_20DecimalFormatSymbolsER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713DecimalFormatC2ERKNS_13UnicodeStringEPNS_20DecimalFormatSymbolsE18UNumberFormatStyleR10UErrorCode
	.type	_ZN6icu_6713DecimalFormatC2ERKNS_13UnicodeStringEPNS_20DecimalFormatSymbolsE18UNumberFormatStyleR10UErrorCode, @function
_ZN6icu_6713DecimalFormatC2ERKNS_13UnicodeStringEPNS_20DecimalFormatSymbolsE18UNumberFormatStyleR10UErrorCode:
.LFB3900:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movq	%rdx, %rsi
	movq	%r8, %rdx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$248, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6713DecimalFormatC1EPKNS_20DecimalFormatSymbolsER10UErrorCode
	movl	(%r12), %esi
	testl	%esi, %esi
	jg	.L811
	movq	360(%r13), %rax
	leaq	8(%rax), %rsi
	cmpl	$16, %ebx
	jbe	.L814
.L815:
	movq	%r12, %rcx
	movl	$1, %edx
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl13PatternParser25parseToExistingPropertiesERKNS_13UnicodeStringERNS1_23DecimalFormatPropertiesENS1_14IgnoreRoundingER10UErrorCode@PLT
.L817:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6713DecimalFormat5touchER10UErrorCode
.L811:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L830
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L814:
	.cfi_restore_state
	movl	$80900, %eax
	btq	%rbx, %rax
	jnc	.L815
	movq	%r12, %rcx
	movl	$2, %edx
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl13PatternParser25parseToExistingPropertiesERKNS_13UnicodeStringERNS1_23DecimalFormatPropertiesENS1_14IgnoreRoundingER10UErrorCode@PLT
	cmpl	$11, %ebx
	jne	.L817
	movq	360(%r13), %rax
	leaq	-288(%rbp), %r15
	movq	%r15, %rdi
	movq	768(%rax), %rsi
	addq	$1872, %rsi
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movl	$40, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L818
	movq	%rax, %rdi
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	_ZN6icu_6718CurrencyPluralInfoC1ERKNS_6LocaleER10UErrorCode@PLT
	movq	%r15, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	movl	(%r12), %ecx
	testl	%ecx, %ecx
	jg	.L831
.L820:
	movq	360(%r13), %rbx
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L822
	movq	(%rdi), %rax
	call	*8(%rax)
.L822:
	movq	%r14, 56(%rbx)
	jmp	.L817
.L818:
	movl	(%r12), %eax
	testl	%eax, %eax
	jle	.L832
.L821:
	movq	%r15, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	movl	(%r12), %edx
	testl	%edx, %edx
	jle	.L820
	jmp	.L811
	.p2align 4,,10
	.p2align 3
.L831:
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*8(%rax)
	jmp	.L811
	.p2align 4,,10
	.p2align 3
.L832:
	movl	$7, (%r12)
	jmp	.L821
.L830:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3900:
	.size	_ZN6icu_6713DecimalFormatC2ERKNS_13UnicodeStringEPNS_20DecimalFormatSymbolsE18UNumberFormatStyleR10UErrorCode, .-_ZN6icu_6713DecimalFormatC2ERKNS_13UnicodeStringEPNS_20DecimalFormatSymbolsE18UNumberFormatStyleR10UErrorCode
	.globl	_ZN6icu_6713DecimalFormatC1ERKNS_13UnicodeStringEPNS_20DecimalFormatSymbolsE18UNumberFormatStyleR10UErrorCode
	.set	_ZN6icu_6713DecimalFormatC1ERKNS_13UnicodeStringEPNS_20DecimalFormatSymbolsE18UNumberFormatStyleR10UErrorCode,_ZN6icu_6713DecimalFormatC2ERKNS_13UnicodeStringEPNS_20DecimalFormatSymbolsE18UNumberFormatStyleR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713DecimalFormatC2ERKNS_13UnicodeStringERKNS_20DecimalFormatSymbolsER10UErrorCode
	.type	_ZN6icu_6713DecimalFormatC2ERKNS_13UnicodeStringERKNS_20DecimalFormatSymbolsER10UErrorCode, @function
_ZN6icu_6713DecimalFormatC2ERKNS_13UnicodeStringERKNS_20DecimalFormatSymbolsER10UErrorCode:
.LFB3920:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	movq	%rcx, %rdx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	xorl	%esi, %esi
	subq	$24, %rsp
	call	_ZN6icu_6713DecimalFormatC1EPKNS_20DecimalFormatSymbolsER10UErrorCode
	movl	(%r12), %ecx
	testl	%ecx, %ecx
	jle	.L870
.L833:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L870:
	.cfi_restore_state
	movl	$2816, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L836
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKS0_@PLT
	movq	360(%r13), %rsi
	movl	(%r12), %edx
	movq	%rsi, %r14
	testl	%edx, %edx
	jg	.L837
	movq	768(%rsi), %rdi
	testq	%rdi, %rdi
	je	.L845
	movq	(%rdi), %rax
	movq	%rsi, -56(%rbp)
	call	*8(%rax)
	movl	(%r12), %eax
	movq	-56(%rbp), %rsi
	movq	%r15, 768(%rsi)
	testl	%eax, %eax
	jle	.L871
.L846:
	addq	$24, %rsp
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6713DecimalFormat5touchER10UErrorCode
	.p2align 4,,10
	.p2align 3
.L845:
	.cfi_restore_state
	movq	%r15, 768(%rsi)
.L849:
	addq	$8, %rsi
	movq	%r12, %rcx
	movl	$1, %edx
	movq	%rbx, %rdi
	call	_ZN6icu_676number4impl13PatternParser25parseToExistingPropertiesERKNS_13UnicodeStringERNS1_23DecimalFormatPropertiesENS1_14IgnoreRoundingER10UErrorCode@PLT
	jmp	.L846
.L836:
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L869
	movl	$7, (%r12)
.L869:
	movq	360(%r13), %r14
.L837:
	testq	%r14, %r14
	je	.L839
	leaq	3840(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	3776(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	3712(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	3648(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	3560(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	3488(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	3424(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	3360(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	3296(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	3216(%r14), %rdi
	testq	%rdi, %rdi
	je	.L840
	movq	(%rdi), %rax
	call	*8(%rax)
.L840:
	leaq	3184(%r14), %rdi
	leaq	3160(%r14), %rbx
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	leaq	1520(%r14), %rax
	movq	%rax, -64(%rbp)
	leaq	16+_ZTVN6icu_676number4impl31CurrencyPluralInfoAffixProviderE(%rip), %rax
	movq	%rax, 1520(%r14)
	leaq	1528(%r14), %rax
	movq	%rax, -56(%rbp)
	.p2align 4,,10
	.p2align 3
.L841:
	movq	-272(%rbx), %rdx
	subq	$272, %rbx
	movq	%rbx, %rdi
	call	*(%rdx)
	cmpq	%rbx, -56(%rbp)
	jne	.L841
	movq	-64(%rbp), %rdi
	leaq	1248(%r14), %rbx
	call	_ZN6icu_676number4impl20AffixPatternProviderD2Ev@PLT
	leaq	16+_ZTVN6icu_676number4impl30PropertiesAffixPatternProviderE(%rip), %rax
	leaq	1448(%r14), %rdi
	movq	%rax, 1248(%r14)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	1384(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	1320(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	1256(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_676number4impl20AffixPatternProviderD2Ev@PLT
	leaq	776(%r14), %rdi
	call	_ZN6icu_676number24LocalizedNumberFormatterD1Ev@PLT
	movq	768(%r14), %rdi
	testq	%rdi, %rdi
	je	.L842
	movq	(%rdi), %rax
	call	*8(%rax)
.L842:
	leaq	680(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	616(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	552(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	488(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	400(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	328(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	264(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	200(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	136(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	56(%r14), %rdi
	testq	%rdi, %rdi
	je	.L843
	movq	(%rdi), %rax
	call	*8(%rax)
.L843:
	leaq	24(%r14), %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L839:
	movq	$0, 360(%r13)
	movl	$7, (%r12)
	testq	%r15, %r15
	je	.L833
	movq	(%r15), %rax
	movq	%r15, %rdi
	movq	8(%rax), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
.L871:
	.cfi_restore_state
	movq	360(%r13), %rsi
	jmp	.L849
	.cfi_endproc
.LFE3920:
	.size	_ZN6icu_6713DecimalFormatC2ERKNS_13UnicodeStringERKNS_20DecimalFormatSymbolsER10UErrorCode, .-_ZN6icu_6713DecimalFormatC2ERKNS_13UnicodeStringERKNS_20DecimalFormatSymbolsER10UErrorCode
	.globl	_ZN6icu_6713DecimalFormatC1ERKNS_13UnicodeStringERKNS_20DecimalFormatSymbolsER10UErrorCode
	.set	_ZN6icu_6713DecimalFormatC1ERKNS_13UnicodeStringERKNS_20DecimalFormatSymbolsER10UErrorCode,_ZN6icu_6713DecimalFormatC2ERKNS_13UnicodeStringERKNS_20DecimalFormatSymbolsER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713DecimalFormatC2ERKS0_
	.type	_ZN6icu_6713DecimalFormatC2ERKS0_, @function
_ZN6icu_6713DecimalFormatC2ERKS0_:
.LFB3923:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6712NumberFormatC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6713DecimalFormatE(%rip), %rax
	cmpq	$0, 360(%r14)
	movq	$0, 360(%r13)
	movq	%rax, 0(%r13)
	je	.L872
	movl	$3944, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L874
	movq	360(%r14), %r12
	leaq	24(%rbx), %rdi
	movq	8(%r12), %rax
	leaq	24(%r12), %rsi
	movq	%rax, 8(%rbx)
	movzbl	16(%r12), %eax
	movb	%al, 16(%rbx)
	call	_ZN6icu_6712CurrencyUnitC1ERKS0_@PLT
	movq	$0, 56(%rbx)
	movq	56(%r12), %rsi
	testq	%rsi, %rsi
	movq	%rsi, -80(%rbp)
	je	.L875
	movl	$40, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L876
	movq	-80(%rbp), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6718CurrencyPluralInfoC1ERKS0_@PLT
.L876:
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L877
	movq	(%rdi), %rax
	call	*8(%rax)
.L877:
	movq	%r15, 56(%rbx)
.L875:
	movq	64(%r12), %rdx
	movl	80(%r12), %ecx
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %r15
	leaq	136(%rbx), %rdi
	movq	72(%r12), %rsi
	movdqu	88(%r12), %xmm3
	movq	%r15, %xmm2
	movq	%rdx, 64(%rbx)
	movzbl	84(%r12), %edx
	movl	%ecx, 80(%rbx)
	movq	120(%r12), %rcx
	movb	%dl, 84(%rbx)
	movdqu	104(%r12), %xmm4
	movl	128(%r12), %edx
	movq	%rcx, 120(%rbx)
	movq	.LC2(%rip), %xmm0
	movq	%rsi, 72(%rbx)
	leaq	136(%r12), %rsi
	movl	%edx, 128(%rbx)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm3, 88(%rbx)
	movups	%xmm4, 104(%rbx)
	movaps	%xmm0, -80(%rbp)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	leaq	200(%r12), %rsi
	leaq	200(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	leaq	264(%r12), %rsi
	leaq	264(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	leaq	328(%r12), %rsi
	leaq	328(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	392(%r12), %rdx
	leaq	400(%r12), %rsi
	leaq	400(%rbx), %rdi
	movq	%rdx, 392(%rbx)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	leaq	488(%r12), %rsi
	movzwl	464(%r12), %edx
	leaq	488(%rbx), %rdi
	movw	%dx, 464(%rbx)
	movq	468(%r12), %rdx
	movq	%rdx, 468(%rbx)
	movzwl	476(%r12), %edx
	movw	%dx, 476(%rbx)
	movl	480(%r12), %edx
	movl	%edx, 480(%rbx)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	leaq	552(%r12), %rsi
	leaq	552(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	leaq	616(%r12), %rsi
	leaq	616(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	leaq	680(%r12), %rsi
	leaq	680(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	752(%r12), %rdx
	movsd	744(%r12), %xmm1
	leaq	792(%rbx), %rdi
	movq	%rdx, 752(%rbx)
	movl	760(%r12), %edx
	movsd	%xmm1, 744(%rbx)
	movl	%edx, 760(%rbx)
	movzbl	764(%r12), %edx
	leaq	16+_ZTVN6icu_676number4impl31CurrencyPluralInfoAffixProviderE(%rip), %r12
	movq	$0, 768(%rbx)
	movb	%dl, 764(%rbx)
	movq	$2, 780(%rbx)
	movl	$0, 788(%rbx)
	call	_ZN6icu_6711MeasureUnitC1Ev@PLT
	leaq	816(%rbx), %rdi
	call	_ZN6icu_6711MeasureUnitC1Ev@PLT
	movl	$-3, %edx
	movl	$-1, %ecx
	movabsq	$30064771077, %rax
	movw	%dx, 876(%rbx)
	pxor	%xmm1, %xmm1
	leaq	992(%rbx), %rdi
	movw	%cx, 900(%rbx)
	movq	%rax, 928(%rbx)
	movl	$0, 840(%rbx)
	movl	$4, 872(%rbx)
	movl	$-2, 888(%rbx)
	movb	$0, 908(%rbx)
	movl	$0, 912(%rbx)
	movq	$0, 920(%rbx)
	movl	$2, 936(%rbx)
	movl	$0, 944(%rbx)
	movq	$0, 952(%rbx)
	movl	$0, 960(%rbx)
	movl	$3, 984(%rbx)
	movups	%xmm1, 968(%rbx)
	call	_ZN6icu_676LocaleC1Ev@PLT
	movl	$2, %eax
	movdqa	-80(%rbp), %xmm0
	pxor	%xmm1, %xmm1
	movl	$2, %esi
	movl	$2, %edi
	movw	%ax, 1672(%rbx)
	movl	$2, %eax
	movw	%ax, 1736(%rbx)
	movl	$2, %eax
	movl	$2, %r8d
	movl	$2, %r9d
	movl	$2, %r10d
	movl	$2, %r11d
	movw	%ax, 1816(%rbx)
	movl	$2, %eax
	movw	%ax, 1880(%rbx)
	movl	$2, %eax
	movl	$2, %edx
	movl	$2, %ecx
	movw	%ax, 1944(%rbx)
	movl	$2, %eax
	movups	%xmm1, 1232(%rbx)
	movups	%xmm0, 1248(%rbx)
	movups	%xmm0, 1528(%rbx)
	movups	%xmm0, 1800(%rbx)
	movw	%si, 1264(%rbx)
	movl	$2, %esi
	movq	%r15, 1320(%rbx)
	movw	%di, 1328(%rbx)
	movl	$2, %edi
	movq	%r15, 1384(%rbx)
	movw	%r8w, 1392(%rbx)
	movl	$2, %r8d
	movq	%r15, 1448(%rbx)
	movw	%r9w, 1456(%rbx)
	movl	$2, %r9d
	movw	%r10w, 1544(%rbx)
	movl	$2, %r10d
	movq	%r15, 1600(%rbx)
	movw	%r11w, 1608(%rbx)
	movl	$2, %r11d
	movq	%r15, 1664(%rbx)
	movq	%r15, 1728(%rbx)
	movq	%r15, 1872(%rbx)
	movq	%r15, 1936(%rbx)
	movq	%r15, 2000(%rbx)
	movw	%dx, 2008(%rbx)
	movl	$2, %edx
	movq	$0, 1216(%rbx)
	movq	$0, 1224(%rbx)
	movb	$1, 1513(%rbx)
	movq	%r12, 1520(%rbx)
	movb	$1, 1793(%rbx)
	movb	$1, 2065(%rbx)
	movups	%xmm0, 2072(%rbx)
	movw	%ax, 2552(%rbx)
	movl	$2, %eax
	movw	%ax, 2632(%rbx)
	movl	$2, %eax
	movw	%ax, 2696(%rbx)
	movl	$2, %eax
	movw	%cx, 2088(%rbx)
	movl	$2, %ecx
	movw	%si, 2152(%rbx)
	movl	$2, %esi
	movw	%di, 2216(%rbx)
	movl	$2, %edi
	movw	%ax, 2760(%rbx)
	movl	$2, %eax
	movw	%r8w, 2280(%rbx)
	movw	%r9w, 2360(%rbx)
	movw	%r10w, 2424(%rbx)
	movw	%r11w, 2488(%rbx)
	movw	%ax, 2824(%rbx)
	movw	%dx, 2904(%rbx)
	movw	%cx, 2968(%rbx)
	movw	%si, 3032(%rbx)
	movups	%xmm0, 2344(%rbx)
	movups	%xmm0, 2616(%rbx)
	movups	%xmm0, 2888(%rbx)
	movq	%r15, 2144(%rbx)
	movq	%r15, 2208(%rbx)
	movq	%r15, 2272(%rbx)
	movq	%r15, 2416(%rbx)
	movq	%r15, 2480(%rbx)
	movq	%r15, 2544(%rbx)
	movq	%r15, 2688(%rbx)
	movq	%r15, 2752(%rbx)
	movq	%r15, 2816(%rbx)
	movq	%r15, 2960(%rbx)
	movq	%r15, 3024(%rbx)
	movb	$1, 2337(%rbx)
	movb	$1, 2609(%rbx)
	movb	$1, 2881(%rbx)
	movq	%r15, 3088(%rbx)
	movw	%di, 3096(%rbx)
	leaq	3168(%rbx), %rdi
	movb	$1, 3153(%rbx)
	movb	$1, 3160(%rbx)
	call	_ZN6icu_676number4impl23DecimalFormatPropertiesC1Ev@PLT
	movb	$0, 3928(%rbx)
	movq	360(%r14), %rax
	movl	$2816, %edi
	movq	%rbx, 360(%r13)
	movl	$0, -60(%rbp)
	movq	768(%rax), %r15
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L878
	movq	%rax, %rdi
	movq	%r15, %rsi
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKS0_@PLT
	movl	-60(%rbp), %eax
	testl	%eax, %eax
	jg	.L921
	movq	768(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L922
	movq	(%rdi), %rax
	call	*8(%rax)
	movl	-60(%rbp), %eax
	movq	%r14, 768(%rbx)
	testl	%eax, %eax
	jg	.L881
	.p2align 4,,10
	.p2align 3
.L882:
	leaq	-60(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6713DecimalFormat5touchER10UErrorCode
.L872:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L923
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L921:
	.cfi_restore_state
	movq	%r14, %rdi
	call	_ZN6icu_6720DecimalFormatSymbolsD0Ev@PLT
	movl	-60(%rbp), %eax
	testl	%eax, %eax
	jle	.L882
.L881:
	movq	360(%r13), %r14
	testq	%r14, %r14
	je	.L874
	leaq	3840(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	3776(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	3712(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	3648(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	3560(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	3488(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	3424(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	3360(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	3296(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	3216(%r14), %rdi
	testq	%rdi, %rdi
	je	.L883
	movq	(%rdi), %rax
	call	*8(%rax)
.L883:
	leaq	3184(%r14), %rdi
	leaq	1520(%r14), %r15
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	leaq	3160(%r14), %rbx
	movq	%r12, 1520(%r14)
	leaq	1528(%r14), %r12
	.p2align 4,,10
	.p2align 3
.L884:
	movq	-272(%rbx), %rdx
	subq	$272, %rbx
	movq	%rbx, %rdi
	call	*(%rdx)
	cmpq	%r12, %rbx
	jne	.L884
	movq	%r15, %rdi
	leaq	1248(%r14), %r12
	call	_ZN6icu_676number4impl20AffixPatternProviderD2Ev@PLT
	leaq	16+_ZTVN6icu_676number4impl30PropertiesAffixPatternProviderE(%rip), %rax
	leaq	1448(%r14), %rdi
	movq	%rax, 1248(%r14)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	1384(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	1320(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	1256(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl20AffixPatternProviderD2Ev@PLT
	leaq	776(%r14), %rdi
	call	_ZN6icu_676number24LocalizedNumberFormatterD1Ev@PLT
	movq	768(%r14), %rdi
	testq	%rdi, %rdi
	je	.L885
	movq	(%rdi), %rax
	call	*8(%rax)
.L885:
	leaq	680(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	616(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	552(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	488(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	400(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	328(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	264(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	200(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	136(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	56(%r14), %rdi
	testq	%rdi, %rdi
	je	.L886
	movq	(%rdi), %rax
	call	*8(%rax)
.L886:
	leaq	24(%r14), %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L874:
	movq	$0, 360(%r13)
	jmp	.L872
	.p2align 4,,10
	.p2align 3
.L922:
	movq	%r14, 768(%rbx)
	jmp	.L882
.L923:
	call	__stack_chk_fail@PLT
.L878:
	cmpl	$0, -60(%rbp)
	jg	.L881
	movq	768(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L891
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	$0, 768(%rbx)
.L891:
	movl	$7, -60(%rbp)
	jmp	.L881
	.cfi_endproc
.LFE3923:
	.size	_ZN6icu_6713DecimalFormatC2ERKS0_, .-_ZN6icu_6713DecimalFormatC2ERKS0_
	.globl	_ZN6icu_6713DecimalFormatC1ERKS0_
	.set	_ZN6icu_6713DecimalFormatC1ERKS0_,_ZN6icu_6713DecimalFormatC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713DecimalFormat5cloneEv
	.type	_ZNK6icu_6713DecimalFormat5cloneEv, @function
_ZNK6icu_6713DecimalFormat5cloneEv:
.LFB3931:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	cmpq	$0, 360(%rdi)
	je	.L937
	movq	%rdi, %r13
	movl	$368, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L937
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6713DecimalFormatC1ERKS0_
	cmpq	$0, 360(%r12)
	je	.L938
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L938:
	.cfi_restore_state
	movq	(%r12), %rdx
	leaq	_ZN6icu_6713DecimalFormatD0Ev(%rip), %rax
	cmpq	%rax, 8(%rdx)
	jne	.L939
	movq	%r12, %rdi
	call	_ZN6icu_6713DecimalFormatD1Ev
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L937:
	xorl	%r12d, %r12d
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L939:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	_ZN6icu_6713DecimalFormatD0Ev
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3931:
	.size	_ZNK6icu_6713DecimalFormat5cloneEv, .-_ZNK6icu_6713DecimalFormat5cloneEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713DecimalFormataSERKS0_
	.type	_ZN6icu_6713DecimalFormataSERKS0_, @function
_ZN6icu_6713DecimalFormataSERKS0_:
.LFB3925:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	%rsi, %rdi
	je	.L942
	movq	360(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L942
	movq	360(%rsi), %r12
	movq	%rsi, %r14
	testq	%r12, %r12
	je	.L942
	movzbl	8(%r12), %eax
	movb	%al, 8(%rbx)
	testb	%al, %al
	jne	.L944
	movl	12(%r12), %eax
	movl	%eax, 12(%rbx)
.L944:
	movzbl	16(%r12), %eax
	movb	%al, 16(%rbx)
	testb	%al, %al
	je	.L1000
.L945:
	movq	56(%r12), %rsi
	testq	%rsi, %rsi
	movq	%rsi, -72(%rbp)
	je	.L946
	movl	$40, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L947
	movq	-72(%rbp), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6718CurrencyPluralInfoC1ERKS0_@PLT
.L947:
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L948
	movq	(%rdi), %rax
	call	*8(%rax)
.L948:
	movq	%r15, 56(%rbx)
.L946:
	movzbl	64(%r12), %eax
	movb	%al, 64(%rbx)
	testb	%al, %al
	jne	.L949
	movl	68(%r12), %eax
	movl	%eax, 68(%rbx)
.L949:
	movzbl	72(%r12), %eax
	leaq	136(%r12), %rsi
	leaq	136(%rbx), %rdi
	movb	%al, 72(%rbx)
	movzbl	73(%r12), %eax
	movb	%al, 73(%rbx)
	movzbl	74(%r12), %eax
	movb	%al, 74(%rbx)
	movzbl	75(%r12), %eax
	movb	%al, 75(%rbx)
	movl	76(%r12), %eax
	movl	%eax, 76(%rbx)
	movl	80(%r12), %eax
	movl	%eax, 80(%rbx)
	movzbl	84(%r12), %eax
	movdqu	104(%r12), %xmm0
	movdqu	88(%r12), %xmm1
	movb	%al, 84(%rbx)
	movups	%xmm1, 88(%rbx)
	movups	%xmm0, 104(%rbx)
	movl	120(%r12), %eax
	movl	%eax, 120(%rbx)
	movl	124(%r12), %eax
	movl	%eax, 124(%rbx)
	movl	128(%r12), %eax
	movl	%eax, 128(%rbx)
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	leaq	200(%r12), %rsi
	leaq	200(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	leaq	264(%r12), %rsi
	leaq	264(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	leaq	328(%r12), %rsi
	leaq	328(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movzbl	392(%r12), %eax
	movb	%al, 392(%rbx)
	testb	%al, %al
	jne	.L950
	movl	396(%r12), %eax
	movl	%eax, 396(%rbx)
.L950:
	leaq	400(%r12), %rsi
	leaq	400(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movzbl	464(%r12), %eax
	movb	%al, 464(%rbx)
	movzbl	465(%r12), %eax
	movb	%al, 465(%rbx)
	movzbl	468(%r12), %eax
	movb	%al, 468(%rbx)
	testb	%al, %al
	jne	.L951
	movl	472(%r12), %eax
	movl	%eax, 472(%rbx)
.L951:
	movzbl	476(%r12), %eax
	leaq	488(%r12), %rsi
	leaq	488(%rbx), %rdi
	movb	%al, 476(%rbx)
	movzbl	477(%r12), %eax
	movb	%al, 477(%rbx)
	movl	480(%r12), %eax
	movl	%eax, 480(%rbx)
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	leaq	552(%r12), %rsi
	leaq	552(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	leaq	616(%r12), %rsi
	leaq	616(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	leaq	680(%r12), %rsi
	leaq	680(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movsd	744(%r12), %xmm0
	movzbl	752(%r12), %eax
	movsd	%xmm0, 744(%rbx)
	movb	%al, 752(%rbx)
	testb	%al, %al
	jne	.L952
	movl	756(%r12), %eax
	movl	%eax, 756(%rbx)
.L952:
	movl	760(%r12), %eax
	movl	%eax, 760(%rbx)
	movzbl	764(%r12), %eax
	movb	%al, 764(%rbx)
	movq	360(%r13), %rax
	leaq	3168(%rax), %rdi
	call	_ZN6icu_676number4impl23DecimalFormatProperties5clearEv@PLT
	movq	360(%r14), %rax
	movl	$2816, %edi
	movl	$0, -60(%rbp)
	movq	768(%rax), %r12
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L953
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKS0_@PLT
	movl	-60(%rbp), %edx
	movq	360(%r13), %r12
	testl	%edx, %edx
	jg	.L954
	movq	768(%r12), %rdi
	testq	%rdi, %rdi
	je	.L962
	movq	(%rdi), %rax
	call	*8(%rax)
.L962:
	movq	%r14, 768(%r12)
	leaq	-60(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6713DecimalFormat5touchER10UErrorCode
.L942:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1001
	addq	$40, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L953:
	.cfi_restore_state
	movl	-60(%rbp), %eax
	testl	%eax, %eax
	jg	.L999
	movl	$7, -60(%rbp)
.L999:
	movq	360(%r13), %r12
.L954:
	testq	%r12, %r12
	je	.L956
	leaq	3840(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	3776(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	3712(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	3648(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	3560(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	3488(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	3424(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	3360(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	3296(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	3216(%r12), %rdi
	testq	%rdi, %rdi
	je	.L957
	movq	(%rdi), %rax
	call	*8(%rax)
.L957:
	leaq	3184(%r12), %rdi
	leaq	2888(%r12), %rbx
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	leaq	1520(%r12), %rax
	leaq	1256(%r12), %r15
	movq	%rax, -72(%rbp)
	leaq	16+_ZTVN6icu_676number4impl31CurrencyPluralInfoAffixProviderE(%rip), %rax
	movq	%rax, 1520(%r12)
	.p2align 4,,10
	.p2align 3
.L958:
	movq	(%rbx), %rdx
	movq	%rbx, %rdi
	subq	$272, %rbx
	call	*(%rdx)
	cmpq	%rbx, %r15
	jne	.L958
	movq	-72(%rbp), %rdi
	leaq	1248(%r12), %rbx
	call	_ZN6icu_676number4impl20AffixPatternProviderD2Ev@PLT
	leaq	16+_ZTVN6icu_676number4impl30PropertiesAffixPatternProviderE(%rip), %rax
	leaq	1448(%r12), %rdi
	movq	%rax, 1248(%r12)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	1384(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	1320(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_676number4impl20AffixPatternProviderD2Ev@PLT
	leaq	776(%r12), %rdi
	call	_ZN6icu_676number24LocalizedNumberFormatterD1Ev@PLT
	movq	768(%r12), %rdi
	testq	%rdi, %rdi
	je	.L959
	movq	(%rdi), %rax
	call	*8(%rax)
.L959:
	leaq	680(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	616(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	552(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	488(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	400(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	328(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	264(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	200(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	136(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	56(%r12), %rdi
	testq	%rdi, %rdi
	je	.L960
	movq	(%rdi), %rax
	call	*8(%rax)
.L960:
	leaq	24(%r12), %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L956:
	movq	$0, 360(%r13)
	testq	%r14, %r14
	je	.L942
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*8(%rax)
	jmp	.L942
	.p2align 4,,10
	.p2align 3
.L1000:
	leaq	24(%r12), %rsi
	leaq	24(%rbx), %rdi
	call	_ZN6icu_6712CurrencyUnitaSERKS0_@PLT
	jmp	.L945
.L1001:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3925:
	.size	_ZN6icu_6713DecimalFormataSERKS0_, .-_ZN6icu_6713DecimalFormataSERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713DecimalFormat12applyPatternERKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6713DecimalFormat12applyPatternERKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6713DecimalFormat12applyPatternERKNS_13UnicodeStringER10UErrorCode:
.LFB4017:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L1006
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	.cfi_offset 12, -32
	movq	360(%r13), %rsi
	movq	%rdx, %r12
	testq	%rsi, %rsi
	je	.L1009
	addq	$8, %rsi
	movq	%rdx, %rcx
	xorl	%edx, %edx
	call	_ZN6icu_676number4impl13PatternParser25parseToExistingPropertiesERKNS_13UnicodeStringERNS1_23DecimalFormatPropertiesENS1_14IgnoreRoundingER10UErrorCode@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6713DecimalFormat5touchER10UErrorCode
	.p2align 4,,10
	.p2align 3
.L1006:
	ret
	.p2align 4,,10
	.p2align 3
.L1009:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movl	$7, (%rdx)
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4017:
	.size	_ZN6icu_6713DecimalFormat12applyPatternERKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6713DecimalFormat12applyPatternERKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713DecimalFormat16setCurrencyUsageE14UCurrencyUsageP10UErrorCode
	.type	_ZN6icu_6713DecimalFormat16setCurrencyUsageE14UCurrencyUsageP10UErrorCode, @function
_ZN6icu_6713DecimalFormat16setCurrencyUsageE14UCurrencyUsageP10UErrorCode:
.LFB4032:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	movq	%rdx, %rsi
	movl	(%rdx), %edx
	testl	%edx, %edx
	jg	.L1010
	movq	360(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L1014
	cmpb	$0, 64(%rdx)
	jne	.L1013
	cmpl	68(%rdx), %eax
	je	.L1010
.L1013:
	movl	%eax, 68(%rdx)
	movb	$0, 64(%rdx)
	jmp	_ZN6icu_6713DecimalFormat5touchER10UErrorCode
	.p2align 4,,10
	.p2align 3
.L1010:
	ret
	.p2align 4,,10
	.p2align 3
.L1014:
	movl	$7, (%rsi)
	ret
	.cfi_endproc
.LFE4032:
	.size	_ZN6icu_6713DecimalFormat16setCurrencyUsageE14UCurrencyUsageP10UErrorCode, .-_ZN6icu_6713DecimalFormat16setCurrencyUsageE14UCurrencyUsageP10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713DecimalFormatC2ERKNS_13UnicodeStringEPNS_20DecimalFormatSymbolsER11UParseErrorR10UErrorCode
	.type	_ZN6icu_6713DecimalFormatC2ERKNS_13UnicodeStringEPNS_20DecimalFormatSymbolsER11UParseErrorR10UErrorCode, @function
_ZN6icu_6713DecimalFormatC2ERKNS_13UnicodeStringEPNS_20DecimalFormatSymbolsER11UParseErrorR10UErrorCode:
.LFB3917:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	movq	%rdx, %rsi
	pushq	%r13
	movq	%r8, %rdx
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%r8, %r12
	subq	$8, %rsp
	call	_ZN6icu_6713DecimalFormatC1EPKNS_20DecimalFormatSymbolsER10UErrorCode
	movl	(%r12), %eax
	testl	%eax, %eax
	jle	.L1018
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1018:
	.cfi_restore_state
	movq	360(%r13), %rax
	movq	%r14, %rdi
	movq	%r12, %rcx
	movl	$1, %edx
	leaq	8(%rax), %rsi
	call	_ZN6icu_676number4impl13PatternParser25parseToExistingPropertiesERKNS_13UnicodeStringERNS1_23DecimalFormatPropertiesENS1_14IgnoreRoundingER10UErrorCode@PLT
	addq	$8, %rsp
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6713DecimalFormat5touchER10UErrorCode
	.cfi_endproc
.LFE3917:
	.size	_ZN6icu_6713DecimalFormatC2ERKNS_13UnicodeStringEPNS_20DecimalFormatSymbolsER11UParseErrorR10UErrorCode, .-_ZN6icu_6713DecimalFormatC2ERKNS_13UnicodeStringEPNS_20DecimalFormatSymbolsER11UParseErrorR10UErrorCode
	.globl	_ZN6icu_6713DecimalFormatC1ERKNS_13UnicodeStringEPNS_20DecimalFormatSymbolsER11UParseErrorR10UErrorCode
	.set	_ZN6icu_6713DecimalFormatC1ERKNS_13UnicodeStringEPNS_20DecimalFormatSymbolsER11UParseErrorR10UErrorCode,_ZN6icu_6713DecimalFormatC2ERKNS_13UnicodeStringEPNS_20DecimalFormatSymbolsER11UParseErrorR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713DecimalFormatC2ERKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6713DecimalFormatC2ERKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6713DecimalFormatC2ERKNS_13UnicodeStringER10UErrorCode:
.LFB3894:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	xorl	%esi, %esi
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	subq	$8, %rsp
	call	_ZN6icu_6713DecimalFormatC1EPKNS_20DecimalFormatSymbolsER10UErrorCode
	movl	(%r12), %eax
	testl	%eax, %eax
	jle	.L1022
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1022:
	.cfi_restore_state
	movq	360(%r13), %rax
	movq	%r14, %rdi
	movq	%r12, %rcx
	movl	$1, %edx
	leaq	8(%rax), %rsi
	call	_ZN6icu_676number4impl13PatternParser25parseToExistingPropertiesERKNS_13UnicodeStringERNS1_23DecimalFormatPropertiesENS1_14IgnoreRoundingER10UErrorCode@PLT
	addq	$8, %rsp
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6713DecimalFormat5touchER10UErrorCode
	.cfi_endproc
.LFE3894:
	.size	_ZN6icu_6713DecimalFormatC2ERKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6713DecimalFormatC2ERKNS_13UnicodeStringER10UErrorCode
	.globl	_ZN6icu_6713DecimalFormatC1ERKNS_13UnicodeStringER10UErrorCode
	.set	_ZN6icu_6713DecimalFormatC1ERKNS_13UnicodeStringER10UErrorCode,_ZN6icu_6713DecimalFormatC2ERKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713DecimalFormat12applyPatternERKNS_13UnicodeStringER11UParseErrorR10UErrorCode
	.type	_ZN6icu_6713DecimalFormat12applyPatternERKNS_13UnicodeStringER11UParseErrorR10UErrorCode, @function
_ZN6icu_6713DecimalFormat12applyPatternERKNS_13UnicodeStringER11UParseErrorR10UErrorCode:
.LFB4016:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN6icu_6713DecimalFormat12applyPatternERKNS_13UnicodeStringER10UErrorCode(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%rsi, %rdi
	movq	(%r12), %rax
	movq	568(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1024
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L1023
	movq	360(%r12), %rsi
	testq	%rsi, %rsi
	je	.L1028
	addq	$8, %rsi
	xorl	%edx, %edx
	call	_ZN6icu_676number4impl13PatternParser25parseToExistingPropertiesERKNS_13UnicodeStringERNS1_23DecimalFormatPropertiesENS1_14IgnoreRoundingER10UErrorCode@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6713DecimalFormat5touchER10UErrorCode
	.p2align 4,,10
	.p2align 3
.L1028:
	.cfi_restore_state
	movl	$7, (%rcx)
.L1023:
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1024:
	.cfi_restore_state
	movq	%r12, %rdi
	movq	%rcx, %rdx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE4016:
	.size	_ZN6icu_6713DecimalFormat12applyPatternERKNS_13UnicodeStringER11UParseErrorR10UErrorCode, .-_ZN6icu_6713DecimalFormat12applyPatternERKNS_13UnicodeStringER11UParseErrorR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713DecimalFormat21applyLocalizedPatternERKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6713DecimalFormat21applyLocalizedPatternERKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6713DecimalFormat21applyLocalizedPatternERKNS_13UnicodeStringER10UErrorCode:
.LFB4019:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$88, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movl	(%rdx), %ecx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1029
	movq	360(%rdi), %rax
	movq	%rdi, %r13
	movq	%rdx, %r12
	testq	%rax, %rax
	je	.L1038
	movq	768(%rax), %rdx
	leaq	-112(%rbp), %r14
	movq	%r12, %r8
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl18PatternStringUtils16convertLocalizedERKNS_13UnicodeStringERKNS_20DecimalFormatSymbolsEbR10UErrorCode@PLT
	movq	0(%r13), %rax
	leaq	_ZN6icu_6713DecimalFormat12applyPatternERKNS_13UnicodeStringER10UErrorCode(%rip), %rdx
	movq	568(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1032
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L1033
	movq	360(%r13), %rsi
	testq	%rsi, %rsi
	je	.L1039
	addq	$8, %rsi
	movq	%r14, %rdi
	movq	%r12, %rcx
	xorl	%edx, %edx
	call	_ZN6icu_676number4impl13PatternParser25parseToExistingPropertiesERKNS_13UnicodeStringERNS1_23DecimalFormatPropertiesENS1_14IgnoreRoundingER10UErrorCode@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6713DecimalFormat5touchER10UErrorCode
.L1033:
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L1029:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1040
	addq	$88, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1038:
	.cfi_restore_state
	movl	$7, (%rdx)
	jmp	.L1029
	.p2align 4,,10
	.p2align 3
.L1032:
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1033
	.p2align 4,,10
	.p2align 3
.L1039:
	movl	$7, (%r12)
	jmp	.L1033
.L1040:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4019:
	.size	_ZN6icu_6713DecimalFormat21applyLocalizedPatternERKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6713DecimalFormat21applyLocalizedPatternERKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713DecimalFormat21applyLocalizedPatternERKNS_13UnicodeStringER11UParseErrorR10UErrorCode
	.type	_ZN6icu_6713DecimalFormat21applyLocalizedPatternERKNS_13UnicodeStringER11UParseErrorR10UErrorCode, @function
_ZN6icu_6713DecimalFormat21applyLocalizedPatternERKNS_13UnicodeStringER11UParseErrorR10UErrorCode:
.LFB4018:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN6icu_6713DecimalFormat21applyLocalizedPatternERKNS_13UnicodeStringER10UErrorCode(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	584(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1042
	movl	(%rcx), %edx
	testl	%edx, %edx
	jg	.L1041
	movq	360(%rdi), %rax
	testq	%rax, %rax
	je	.L1051
	movq	768(%rax), %rdx
	leaq	-112(%rbp), %r14
	movq	%rcx, %r8
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl18PatternStringUtils16convertLocalizedERKNS_13UnicodeStringERKNS_20DecimalFormatSymbolsEbR10UErrorCode@PLT
	movq	(%r12), %rax
	leaq	_ZN6icu_6713DecimalFormat12applyPatternERKNS_13UnicodeStringER10UErrorCode(%rip), %rdx
	movq	568(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1045
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L1046
	movq	360(%r12), %rsi
	testq	%rsi, %rsi
	je	.L1052
	addq	$8, %rsi
	movq	%r14, %rdi
	movq	%r13, %rcx
	xorl	%edx, %edx
	call	_ZN6icu_676number4impl13PatternParser25parseToExistingPropertiesERKNS_13UnicodeStringERNS1_23DecimalFormatPropertiesENS1_14IgnoreRoundingER10UErrorCode@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713DecimalFormat5touchER10UErrorCode
.L1046:
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L1041:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1053
	addq	$88, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1042:
	.cfi_restore_state
	movq	%rcx, %rdx
	call	*%rax
	jmp	.L1041
	.p2align 4,,10
	.p2align 3
.L1051:
	movl	$7, (%rcx)
	jmp	.L1041
	.p2align 4,,10
	.p2align 3
.L1045:
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1046
	.p2align 4,,10
	.p2align 3
.L1052:
	movl	$7, 0(%r13)
	jmp	.L1046
.L1053:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4018:
	.size	_ZN6icu_6713DecimalFormat21applyLocalizedPatternERKNS_13UnicodeStringER11UParseErrorR10UErrorCode, .-_ZN6icu_6713DecimalFormat21applyLocalizedPatternERKNS_13UnicodeStringER11UParseErrorR10UErrorCode
	.align 2
	.p2align 4
	.type	_ZN6icu_6713DecimalFormat5touchER10UErrorCode.constprop.0, @function
_ZN6icu_6713DecimalFormat5touchER10UErrorCode.constprop.0:
.LFB5422:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$1144, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	360(%rdi), %rax
	testq	%rax, %rax
	je	.L1070
	movq	768(%rax), %rsi
	leaq	-1184(%rbp), %r15
	movq	%rdi, %r12
	leaq	-960(%rbp), %rbx
	movq	%r15, %rdi
	leaq	-512(%rbp), %r14
	addq	$1872, %rsi
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movq	360(%r12), %rax
	movq	%r13, %r9
	movq	%rbx, %rdi
	movq	768(%rax), %rdx
	leaq	1248(%rax), %rcx
	leaq	3168(%rax), %r8
	leaq	8(%rax), %rsi
	call	_ZN6icu_676number4impl20NumberPropertyMapper6createERKNS1_23DecimalFormatPropertiesERKNS_20DecimalFormatSymbolsERNS1_22DecimalFormatWarehouseERS3_R10UErrorCode@PLT
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZNO6icu_676number26UnlocalizedNumberFormatter6localeERKNS_6LocaleE@PLT
	movq	360(%r12), %rax
	movq	%r14, %rsi
	leaq	776(%rax), %rdi
	call	_ZN6icu_676number24LocalizedNumberFormatteraSEOS1_@PLT
	movq	%r14, %rdi
	call	_ZN6icu_676number24LocalizedNumberFormatterD1Ev@PLT
	leaq	-744(%rbp), %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	leaq	-792(%rbp), %rdi
	call	_ZN6icu_676number5ScaleD1Ev@PLT
	leaq	-824(%rbp), %rdi
	call	_ZN6icu_676number4impl14SymbolsWrapperD1Ev@PLT
	leaq	-920(%rbp), %rdi
	call	_ZN6icu_6711MeasureUnitD1Ev@PLT
	leaq	-944(%rbp), %rdi
	call	_ZN6icu_6711MeasureUnitD1Ev@PLT
	movq	360(%r12), %rax
	leaq	8(%rax), %rdi
	call	_ZNK6icu_676number4impl23DecimalFormatProperties29equalsDefaultExceptFastFormatEv@PLT
	testb	%al, %al
	je	.L1071
	movq	%r12, %rdi
	call	_ZN6icu_6713DecimalFormat15setupFastFormatEv.part.0
	movq	360(%r12), %rax
.L1058:
	xorl	%edi, %edi
	xchgq	1232(%rax), %rdi
	testq	%rdi, %rdi
	je	.L1059
	movq	(%rdi), %rax
	call	*8(%rax)
.L1059:
	movq	360(%r12), %rax
	xorl	%edi, %edi
	xchgq	1240(%rax), %rdi
	testq	%rdi, %rdi
	je	.L1060
	movq	(%rdi), %rax
	call	*8(%rax)
.L1060:
	movq	360(%r12), %rsi
	cmpb	$0, 3176(%rsi)
	je	.L1061
	movl	$65554, 0(%r13)
.L1061:
	addq	$3184, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6712CurrencyUnitC1ERKS0_@PLT
	movq	%r13, %rdx
	leaq	-492(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6712NumberFormat11setCurrencyEPKDsR10UErrorCode@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	movq	360(%r12), %rax
	movq	%r12, %rdi
	movl	3256(%rax), %esi
	call	_ZN6icu_6712NumberFormat23setMaximumIntegerDigitsEi@PLT
	movq	360(%r12), %rax
	movq	%r12, %rdi
	movl	3276(%rax), %esi
	call	_ZN6icu_6712NumberFormat23setMinimumIntegerDigitsEi@PLT
	movq	360(%r12), %rax
	movq	%r12, %rdi
	movl	3252(%rax), %esi
	call	_ZN6icu_6712NumberFormat24setMaximumFractionDigitsEi@PLT
	movq	360(%r12), %rax
	movq	%r12, %rdi
	movl	3268(%rax), %esi
	call	_ZN6icu_6712NumberFormat24setMinimumFractionDigitsEi@PLT
	movq	360(%r12), %rax
	movq	%r12, %rdi
	movzbl	84(%rax), %esi
	call	_ZN6icu_6712NumberFormat15setGroupingUsedEa@PLT
	movq	%r15, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	jmp	.L1054
	.p2align 4,,10
	.p2align 3
.L1070:
	movl	$7, (%rsi)
.L1054:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1072
	addq	$1144, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1071:
	.cfi_restore_state
	movq	360(%r12), %rax
	movb	$0, 3928(%rax)
	jmp	.L1058
.L1072:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5422:
	.size	_ZN6icu_6713DecimalFormat5touchER10UErrorCode.constprop.0, .-_ZN6icu_6713DecimalFormat5touchER10UErrorCode.constprop.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713DecimalFormat30setDecimalPatternMatchRequiredEa
	.type	_ZN6icu_6713DecimalFormat30setDecimalPatternMatchRequiredEa, @function
_ZN6icu_6713DecimalFormat30setDecimalPatternMatchRequiredEa:
.LFB4007:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	360(%rdi), %rax
	testq	%rax, %rax
	je	.L1073
	movzbl	72(%rax), %ecx
	movsbl	%sil, %edx
	cmpl	%ecx, %edx
	je	.L1073
	testb	%sil, %sil
	leaq	-12(%rbp), %rsi
	movl	$0, -12(%rbp)
	setne	72(%rax)
	call	_ZN6icu_6713DecimalFormat5touchER10UErrorCode.constprop.0
.L1073:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1080
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1080:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4007:
	.size	_ZN6icu_6713DecimalFormat30setDecimalPatternMatchRequiredEa, .-_ZN6icu_6713DecimalFormat30setDecimalPatternMatchRequiredEa
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713DecimalFormat24setSecondaryGroupingSizeEi
	.type	_ZN6icu_6713DecimalFormat24setSecondaryGroupingSizeEi, @function
_ZN6icu_6713DecimalFormat24setSecondaryGroupingSizeEi:
.LFB4001:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	360(%rdi), %rax
	testq	%rax, %rax
	je	.L1081
	cmpl	%esi, 760(%rax)
	je	.L1081
	movl	%esi, 760(%rax)
	leaq	-12(%rbp), %rsi
	movl	$0, -12(%rbp)
	call	_ZN6icu_6713DecimalFormat5touchER10UErrorCode.constprop.0
.L1081:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1088
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1088:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4001:
	.size	_ZN6icu_6713DecimalFormat24setSecondaryGroupingSizeEi, .-_ZN6icu_6713DecimalFormat24setSecondaryGroupingSizeEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713DecimalFormat14setPadPositionENS0_12EPadPositionE
	.type	_ZN6icu_6713DecimalFormat14setPadPositionENS0_12EPadPositionE, @function
_ZN6icu_6713DecimalFormat14setPadPositionENS0_12EPadPositionE:
.LFB3991:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	360(%rdi), %rax
	testq	%rax, %rax
	je	.L1089
	cmpb	$0, 392(%rax)
	jne	.L1091
	cmpl	396(%rax), %esi
	je	.L1089
.L1091:
	movl	%esi, 396(%rax)
	leaq	-12(%rbp), %rsi
	movb	$0, 392(%rax)
	movl	$0, -12(%rbp)
	call	_ZN6icu_6713DecimalFormat5touchER10UErrorCode.constprop.0
.L1089:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1097
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1097:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3991:
	.size	_ZN6icu_6713DecimalFormat14setPadPositionENS0_12EPadPositionE, .-_ZN6icu_6713DecimalFormat14setPadPositionENS0_12EPadPositionE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713DecimalFormat14setFormatWidthEi
	.type	_ZN6icu_6713DecimalFormat14setFormatWidthEi, @function
_ZN6icu_6713DecimalFormat14setFormatWidthEi:
.LFB3987:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	360(%rdi), %rax
	testq	%rax, %rax
	je	.L1098
	cmpl	%esi, 76(%rax)
	je	.L1098
	movl	%esi, 76(%rax)
	leaq	-12(%rbp), %rsi
	movl	$0, -12(%rbp)
	call	_ZN6icu_6713DecimalFormat5touchER10UErrorCode.constprop.0
.L1098:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1105
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1105:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3987:
	.size	_ZN6icu_6713DecimalFormat14setFormatWidthEi, .-_ZN6icu_6713DecimalFormat14setFormatWidthEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713DecimalFormat15setRoundingModeENS_12NumberFormat13ERoundingModeE
	.type	_ZN6icu_6713DecimalFormat15setRoundingModeENS_12NumberFormat13ERoundingModeE, @function
_ZN6icu_6713DecimalFormat15setRoundingModeENS_12NumberFormat13ERoundingModeE:
.LFB3985:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	360(%rdi), %rax
	testq	%rax, %rax
	je	.L1106
	cmpb	$0, 752(%rax)
	movq	%rdi, %r12
	movl	%esi, %ebx
	jne	.L1108
	cmpl	756(%rax), %esi
	je	.L1106
.L1108:
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6712NumberFormat23setMaximumIntegerDigitsEi@PLT
	movq	360(%r12), %rax
	leaq	-28(%rbp), %rsi
	movq	%r12, %rdi
	movl	$0, -28(%rbp)
	movl	%ebx, 756(%rax)
	movb	$0, 752(%rax)
	call	_ZN6icu_6713DecimalFormat5touchER10UErrorCode.constprop.0
.L1106:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1114
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1114:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3985:
	.size	_ZN6icu_6713DecimalFormat15setRoundingModeENS_12NumberFormat13ERoundingModeE, .-_ZN6icu_6713DecimalFormat15setRoundingModeENS_12NumberFormat13ERoundingModeE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713DecimalFormat15setGroupingSizeEi
	.type	_ZN6icu_6713DecimalFormat15setGroupingSizeEi, @function
_ZN6icu_6713DecimalFormat15setGroupingSizeEi:
.LFB3999:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	360(%rdi), %rax
	testq	%rax, %rax
	je	.L1115
	cmpl	%esi, 80(%rax)
	je	.L1115
	movl	%esi, 80(%rax)
	leaq	-12(%rbp), %rsi
	movl	$0, -12(%rbp)
	call	_ZN6icu_6713DecimalFormat5touchER10UErrorCode.constprop.0
.L1115:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1122
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1122:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3999:
	.size	_ZN6icu_6713DecimalFormat15setGroupingSizeEi, .-_ZN6icu_6713DecimalFormat15setGroupingSizeEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713DecimalFormat13setMultiplierEi
	.type	_ZN6icu_6713DecimalFormat13setMultiplierEi, @function
_ZN6icu_6713DecimalFormat13setMultiplierEi:
.LFB3979:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	360(%rdi), %r9
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	testq	%r9, %r9
	je	.L1123
	testl	%esi, %esi
	jne	.L1138
.L1125:
	movl	%esi, 88(%r9)
	movl	$1, 124(%r9)
.L1128:
	leaq	-12(%rbp), %rsi
	movl	$0, -12(%rbp)
	call	_ZN6icu_6713DecimalFormat5touchER10UErrorCode.constprop.0
.L1123:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1139
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1138:
	.cfi_restore_state
	movl	%esi, %edx
	xorl	%r8d, %r8d
	jmp	.L1127
	.p2align 4,,10
	.p2align 3
.L1140:
	movl	%eax, %edx
.L1127:
	cmpl	$1, %edx
	je	.L1130
	movslq	%edx, %rax
	movl	%edx, %ecx
	addl	$1, %r8d
	imulq	$1717986919, %rax, %rax
	sarl	$31, %ecx
	sarq	$34, %rax
	subl	%ecx, %eax
	leal	(%rax,%rax,4), %ecx
	addl	%ecx, %ecx
	cmpl	%edx, %ecx
	je	.L1140
	movl	$0, 88(%r9)
	movl	%esi, 124(%r9)
	jmp	.L1128
.L1130:
	movl	%r8d, %esi
	jmp	.L1125
.L1139:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3979:
	.size	_ZN6icu_6713DecimalFormat13setMultiplierEi, .-_ZN6icu_6713DecimalFormat13setMultiplierEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713DecimalFormat24setMaximumFractionDigitsEi
	.type	_ZN6icu_6713DecimalFormat24setMaximumFractionDigitsEi, @function
_ZN6icu_6713DecimalFormat24setMaximumFractionDigitsEi:
.LFB4022:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	360(%rdi), %rax
	testq	%rax, %rax
	je	.L1141
	cmpl	%esi, 92(%rax)
	je	.L1141
	movl	$999, %edx
	cmpl	$999, %esi
	cmovg	%edx, %esi
	movl	108(%rax), %edx
	testl	%edx, %edx
	js	.L1143
	cmpl	%edx, %esi
	jge	.L1143
	movl	%esi, 108(%rax)
.L1143:
	movl	%esi, 92(%rax)
	leaq	-12(%rbp), %rsi
	movl	$0, -12(%rbp)
	call	_ZN6icu_6713DecimalFormat5touchER10UErrorCode.constprop.0
.L1141:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1152
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1152:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4022:
	.size	_ZN6icu_6713DecimalFormat24setMaximumFractionDigitsEi, .-_ZN6icu_6713DecimalFormat24setMaximumFractionDigitsEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713DecimalFormat24setMinimumFractionDigitsEi
	.type	_ZN6icu_6713DecimalFormat24setMinimumFractionDigitsEi, @function
_ZN6icu_6713DecimalFormat24setMinimumFractionDigitsEi:
.LFB4023:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	360(%rdi), %rax
	testq	%rax, %rax
	je	.L1153
	cmpl	%esi, 108(%rax)
	je	.L1153
	movl	92(%rax), %edx
	testl	%edx, %edx
	js	.L1155
	cmpl	%edx, %esi
	jle	.L1155
	movl	%esi, 92(%rax)
.L1155:
	movl	%esi, 108(%rax)
	leaq	-12(%rbp), %rsi
	movl	$0, -12(%rbp)
	call	_ZN6icu_6713DecimalFormat5touchER10UErrorCode.constprop.0
.L1153:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1164
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1164:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4023:
	.size	_ZN6icu_6713DecimalFormat24setMinimumFractionDigitsEi, .-_ZN6icu_6713DecimalFormat24setMinimumFractionDigitsEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713DecimalFormat23setMaximumIntegerDigitsEi
	.type	_ZN6icu_6713DecimalFormat23setMaximumIntegerDigitsEi, @function
_ZN6icu_6713DecimalFormat23setMaximumIntegerDigitsEi:
.LFB4020:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	360(%rdi), %rax
	testq	%rax, %rax
	je	.L1165
	cmpl	%esi, 96(%rax)
	je	.L1165
	movl	116(%rax), %edx
	testl	%edx, %edx
	js	.L1167
	cmpl	%edx, %esi
	jge	.L1167
	movl	%esi, 116(%rax)
.L1167:
	movl	%esi, 96(%rax)
	leaq	-12(%rbp), %rsi
	movl	$0, -12(%rbp)
	call	_ZN6icu_6713DecimalFormat5touchER10UErrorCode.constprop.0
.L1165:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1176
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1176:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4020:
	.size	_ZN6icu_6713DecimalFormat23setMaximumIntegerDigitsEi, .-_ZN6icu_6713DecimalFormat23setMaximumIntegerDigitsEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713DecimalFormat23setMinimumIntegerDigitsEi
	.type	_ZN6icu_6713DecimalFormat23setMinimumIntegerDigitsEi, @function
_ZN6icu_6713DecimalFormat23setMinimumIntegerDigitsEi:
.LFB4021:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	360(%rdi), %rax
	testq	%rax, %rax
	je	.L1177
	cmpl	%esi, 116(%rax)
	je	.L1177
	movl	96(%rax), %edx
	testl	%edx, %edx
	js	.L1179
	cmpl	%edx, %esi
	jle	.L1179
	movl	%esi, 96(%rax)
.L1179:
	movl	%esi, 116(%rax)
	leaq	-12(%rbp), %rsi
	movl	$0, -12(%rbp)
	call	_ZN6icu_6713DecimalFormat5touchER10UErrorCode.constprop.0
.L1177:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1188
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1188:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4021:
	.size	_ZN6icu_6713DecimalFormat23setMinimumIntegerDigitsEi, .-_ZN6icu_6713DecimalFormat23setMinimumIntegerDigitsEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713DecimalFormat30setDecimalSeparatorAlwaysShownEa
	.type	_ZN6icu_6713DecimalFormat30setDecimalSeparatorAlwaysShownEa, @function
_ZN6icu_6713DecimalFormat30setDecimalSeparatorAlwaysShownEa:
.LFB4005:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	360(%rdi), %rax
	testq	%rax, %rax
	je	.L1189
	movzbl	73(%rax), %ecx
	movsbl	%sil, %edx
	cmpl	%ecx, %edx
	je	.L1189
	testb	%sil, %sil
	leaq	-12(%rbp), %rsi
	movl	$0, -12(%rbp)
	setne	73(%rax)
	call	_ZN6icu_6713DecimalFormat5touchER10UErrorCode.constprop.0
.L1189:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1196
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1196:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4005:
	.size	_ZN6icu_6713DecimalFormat30setDecimalSeparatorAlwaysShownEa, .-_ZN6icu_6713DecimalFormat30setDecimalSeparatorAlwaysShownEa
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713DecimalFormat15setGroupingUsedEa
	.type	_ZN6icu_6713DecimalFormat15setGroupingUsedEa, @function
_ZN6icu_6713DecimalFormat15setGroupingUsedEa:
.LFB3913:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	360(%rdi), %rax
	testq	%rax, %rax
	je	.L1197
	movzbl	84(%rax), %eax
	movl	%esi, %ebx
	movsbl	%sil, %esi
	cmpl	%eax, %esi
	je	.L1197
	movq	%rdi, %r12
	call	_ZN6icu_6712NumberFormat15setGroupingUsedEa@PLT
	testb	%bl, %bl
	leaq	-28(%rbp), %rsi
	movq	%r12, %rdi
	movq	360(%r12), %rax
	movl	$0, -28(%rbp)
	setne	84(%rax)
	call	_ZN6icu_6713DecimalFormat5touchER10UErrorCode.constprop.0
.L1197:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1204
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1204:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3913:
	.size	_ZN6icu_6713DecimalFormat15setGroupingUsedEa, .-_ZN6icu_6713DecimalFormat15setGroupingUsedEa
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713DecimalFormat19setParseIntegerOnlyEa
	.type	_ZN6icu_6713DecimalFormat19setParseIntegerOnlyEa, @function
_ZN6icu_6713DecimalFormat19setParseIntegerOnlyEa:
.LFB3914:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	360(%rdi), %rax
	testq	%rax, %rax
	je	.L1205
	movzbl	465(%rax), %eax
	movl	%esi, %ebx
	movsbl	%sil, %esi
	cmpl	%eax, %esi
	je	.L1205
	movq	%rdi, %r12
	call	_ZN6icu_6712NumberFormat19setParseIntegerOnlyEa@PLT
	testb	%bl, %bl
	leaq	-28(%rbp), %rsi
	movq	%r12, %rdi
	movq	360(%r12), %rax
	movl	$0, -28(%rbp)
	setne	465(%rax)
	call	_ZN6icu_6713DecimalFormat5touchER10UErrorCode.constprop.0
.L1205:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1212
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1212:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3914:
	.size	_ZN6icu_6713DecimalFormat19setParseIntegerOnlyEa, .-_ZN6icu_6713DecimalFormat19setParseIntegerOnlyEa
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713DecimalFormat10setLenientEa
	.type	_ZN6icu_6713DecimalFormat10setLenientEa, @function
_ZN6icu_6713DecimalFormat10setLenientEa:
.LFB3915:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	360(%rdi), %rax
	testq	%rax, %rax
	je	.L1213
	xorl	%ebx, %ebx
	testb	%sil, %sil
	movq	%rdi, %r12
	sete	%bl
	cmpb	$0, 468(%rax)
	jne	.L1215
	cmpl	472(%rax), %ebx
	je	.L1213
.L1215:
	movsbl	%sil, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6712NumberFormat10setLenientEa@PLT
	movq	360(%r12), %rax
	leaq	-28(%rbp), %rsi
	movq	%r12, %rdi
	movl	$0, -28(%rbp)
	movl	%ebx, 472(%rax)
	movb	$0, 468(%rax)
	call	_ZN6icu_6713DecimalFormat5touchER10UErrorCode.constprop.0
.L1213:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1221
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1221:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3915:
	.size	_ZN6icu_6713DecimalFormat10setLenientEa, .-_ZN6icu_6713DecimalFormat10setLenientEa
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713DecimalFormat24setSignificantDigitsUsedEa
	.type	_ZN6icu_6713DecimalFormat24setSignificantDigitsUsedEa, @function
_ZN6icu_6713DecimalFormat24setSignificantDigitsUsedEa:
.LFB4029:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	360(%rdi), %rax
	testq	%rax, %rax
	je	.L1222
	movl	120(%rax), %edx
	testb	%sil, %sil
	jne	.L1232
	cmpl	$-1, %edx
	je	.L1233
	movl	$-1, %ecx
	movl	$-1, %edx
.L1225:
	movl	%ecx, 120(%rax)
	leaq	-12(%rbp), %rsi
	movl	%edx, 100(%rax)
	movl	$0, -12(%rbp)
	call	_ZN6icu_6713DecimalFormat5touchER10UErrorCode.constprop.0
.L1222:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1234
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1232:
	.cfi_restore_state
	cmpl	$-1, %edx
	jne	.L1222
	cmpl	$-1, 100(%rax)
	jne	.L1222
	movl	$1, %ecx
	movl	$6, %edx
	jmp	.L1225
	.p2align 4,,10
	.p2align 3
.L1233:
	cmpl	$-1, 100(%rax)
	movl	$-1, %ecx
	jne	.L1225
	jmp	.L1222
.L1234:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4029:
	.size	_ZN6icu_6713DecimalFormat24setSignificantDigitsUsedEa, .-_ZN6icu_6713DecimalFormat24setSignificantDigitsUsedEa
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713DecimalFormat27setMaximumSignificantDigitsEi
	.type	_ZN6icu_6713DecimalFormat27setMaximumSignificantDigitsEi, @function
_ZN6icu_6713DecimalFormat27setMaximumSignificantDigitsEi:
.LFB4027:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	360(%rdi), %rax
	testq	%rax, %rax
	je	.L1235
	cmpl	%esi, 100(%rax)
	je	.L1235
	movl	120(%rax), %edx
	testl	%edx, %edx
	js	.L1237
	cmpl	%edx, %esi
	jge	.L1237
	movl	%esi, 120(%rax)
.L1237:
	movl	%esi, 100(%rax)
	leaq	-12(%rbp), %rsi
	movl	$0, -12(%rbp)
	call	_ZN6icu_6713DecimalFormat5touchER10UErrorCode.constprop.0
.L1235:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1246
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1246:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4027:
	.size	_ZN6icu_6713DecimalFormat27setMaximumSignificantDigitsEi, .-_ZN6icu_6713DecimalFormat27setMaximumSignificantDigitsEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713DecimalFormat27setMinimumSignificantDigitsEi
	.type	_ZN6icu_6713DecimalFormat27setMinimumSignificantDigitsEi, @function
_ZN6icu_6713DecimalFormat27setMinimumSignificantDigitsEi:
.LFB4026:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	360(%rdi), %rax
	testq	%rax, %rax
	je	.L1247
	cmpl	%esi, 120(%rax)
	je	.L1247
	movl	100(%rax), %edx
	testl	%edx, %edx
	js	.L1249
	cmpl	%edx, %esi
	jle	.L1249
	movl	%esi, 100(%rax)
.L1249:
	movl	%esi, 120(%rax)
	leaq	-12(%rbp), %rsi
	movl	$0, -12(%rbp)
	call	_ZN6icu_6713DecimalFormat5touchER10UErrorCode.constprop.0
.L1247:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1258
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1258:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4026:
	.size	_ZN6icu_6713DecimalFormat27setMinimumSignificantDigitsEi, .-_ZN6icu_6713DecimalFormat27setMinimumSignificantDigitsEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713DecimalFormat18setMultiplierScaleEi
	.type	_ZN6icu_6713DecimalFormat18setMultiplierScaleEi, @function
_ZN6icu_6713DecimalFormat18setMultiplierScaleEi:
.LFB3981:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	360(%rdi), %rax
	testq	%rax, %rax
	je	.L1259
	cmpl	%esi, 128(%rax)
	je	.L1259
	movl	%esi, 128(%rax)
	leaq	-12(%rbp), %rsi
	movl	$0, -12(%rbp)
	call	_ZN6icu_6713DecimalFormat5touchER10UErrorCode.constprop.0
.L1259:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1266
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1266:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3981:
	.size	_ZN6icu_6713DecimalFormat18setMultiplierScaleEi, .-_ZN6icu_6713DecimalFormat18setMultiplierScaleEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713DecimalFormat18setParseNoExponentEa
	.type	_ZN6icu_6713DecimalFormat18setParseNoExponentEa, @function
_ZN6icu_6713DecimalFormat18setParseNoExponentEa:
.LFB4009:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	360(%rdi), %rax
	testq	%rax, %rax
	je	.L1267
	movzbl	476(%rax), %ecx
	movsbl	%sil, %edx
	cmpl	%ecx, %edx
	je	.L1267
	testb	%sil, %sil
	leaq	-12(%rbp), %rsi
	movl	$0, -12(%rbp)
	setne	476(%rax)
	call	_ZN6icu_6713DecimalFormat5touchER10UErrorCode.constprop.0
.L1267:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1274
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1274:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4009:
	.size	_ZN6icu_6713DecimalFormat18setParseNoExponentEa, .-_ZN6icu_6713DecimalFormat18setParseNoExponentEa
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713DecimalFormat24setMinimumGroupingDigitsEi
	.type	_ZN6icu_6713DecimalFormat24setMinimumGroupingDigitsEi, @function
_ZN6icu_6713DecimalFormat24setMinimumGroupingDigitsEi:
.LFB4003:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	360(%rdi), %rax
	testq	%rax, %rax
	je	.L1275
	cmpl	%esi, 112(%rax)
	je	.L1275
	movl	%esi, 112(%rax)
	leaq	-12(%rbp), %rsi
	movl	$0, -12(%rbp)
	call	_ZN6icu_6713DecimalFormat5touchER10UErrorCode.constprop.0
.L1275:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1282
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1282:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4003:
	.size	_ZN6icu_6713DecimalFormat24setMinimumGroupingDigitsEi, .-_ZN6icu_6713DecimalFormat24setMinimumGroupingDigitsEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713DecimalFormat21setParseCaseSensitiveEa
	.type	_ZN6icu_6713DecimalFormat21setParseCaseSensitiveEa, @function
_ZN6icu_6713DecimalFormat21setParseCaseSensitiveEa:
.LFB4011:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	360(%rdi), %rax
	testq	%rax, %rax
	je	.L1283
	movzbl	464(%rax), %ecx
	movsbl	%sil, %edx
	cmpl	%ecx, %edx
	je	.L1283
	testb	%sil, %sil
	leaq	-12(%rbp), %rsi
	movl	$0, -12(%rbp)
	setne	464(%rax)
	call	_ZN6icu_6713DecimalFormat5touchER10UErrorCode.constprop.0
.L1283:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1290
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1290:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4011:
	.size	_ZN6icu_6713DecimalFormat21setParseCaseSensitiveEa, .-_ZN6icu_6713DecimalFormat21setParseCaseSensitiveEa
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713DecimalFormat18setSignAlwaysShownEa
	.type	_ZN6icu_6713DecimalFormat18setSignAlwaysShownEa, @function
_ZN6icu_6713DecimalFormat18setSignAlwaysShownEa:
.LFB3977:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	360(%rdi), %rax
	testq	%rax, %rax
	je	.L1291
	movzbl	764(%rax), %ecx
	movsbl	%sil, %edx
	cmpl	%ecx, %edx
	je	.L1291
	testb	%sil, %sil
	leaq	-12(%rbp), %rsi
	movl	$0, -12(%rbp)
	setne	764(%rax)
	call	_ZN6icu_6713DecimalFormat5touchER10UErrorCode.constprop.0
.L1291:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1298
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1298:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3977:
	.size	_ZN6icu_6713DecimalFormat18setSignAlwaysShownEa, .-_ZN6icu_6713DecimalFormat18setSignAlwaysShownEa
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713DecimalFormat32setFormatFailIfMoreThanMaxDigitsEa
	.type	_ZN6icu_6713DecimalFormat32setFormatFailIfMoreThanMaxDigitsEa, @function
_ZN6icu_6713DecimalFormat32setFormatFailIfMoreThanMaxDigitsEa:
.LFB4013:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	360(%rdi), %rax
	testq	%rax, %rax
	je	.L1299
	movzbl	75(%rax), %ecx
	movsbl	%sil, %edx
	cmpl	%ecx, %edx
	je	.L1299
	testb	%sil, %sil
	leaq	-12(%rbp), %rsi
	movl	$0, -12(%rbp)
	setne	75(%rax)
	call	_ZN6icu_6713DecimalFormat5touchER10UErrorCode.constprop.0
.L1299:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1306
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1306:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4013:
	.size	_ZN6icu_6713DecimalFormat32setFormatFailIfMoreThanMaxDigitsEa, .-_ZN6icu_6713DecimalFormat32setFormatFailIfMoreThanMaxDigitsEa
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713DecimalFormat25adoptDecimalFormatSymbolsEPNS_20DecimalFormatSymbolsE
	.type	_ZN6icu_6713DecimalFormat25adoptDecimalFormatSymbolsEPNS_20DecimalFormatSymbolsE, @function
_ZN6icu_6713DecimalFormat25adoptDecimalFormatSymbolsEPNS_20DecimalFormatSymbolsE:
.LFB3959:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L1307
	movq	360(%rdi), %rbx
	movq	%rdi, %r13
	movq	%rsi, %r12
	testq	%rbx, %rbx
	je	.L1319
	movq	768(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1310
	movq	(%rdi), %rax
	call	*8(%rax)
.L1310:
	movq	%r12, 768(%rbx)
	leaq	-44(%rbp), %rsi
	movq	%r13, %rdi
	movl	$0, -44(%rbp)
	call	_ZN6icu_6713DecimalFormat5touchER10UErrorCode.constprop.0
.L1307:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1320
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1319:
	.cfi_restore_state
	movq	(%rsi), %rax
	movq	%rsi, %rdi
	call	*8(%rax)
	jmp	.L1307
.L1320:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3959:
	.size	_ZN6icu_6713DecimalFormat25adoptDecimalFormatSymbolsEPNS_20DecimalFormatSymbolsE, .-_ZN6icu_6713DecimalFormat25adoptDecimalFormatSymbolsEPNS_20DecimalFormatSymbolsE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713DecimalFormat23setDecimalFormatSymbolsERKNS_20DecimalFormatSymbolsE
	.type	_ZN6icu_6713DecimalFormat23setDecimalFormatSymbolsERKNS_20DecimalFormatSymbolsE, @function
_ZN6icu_6713DecimalFormat23setDecimalFormatSymbolsERKNS_20DecimalFormatSymbolsE:
.LFB3962:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 360(%rdi)
	je	.L1321
	movq	%rdi, %r12
	movl	$2816, %edi
	movq	%rsi, %r14
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1323
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKS0_@PLT
	movq	360(%r12), %r14
	movq	768(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1325
	movq	(%rdi), %rax
	call	*8(%rax)
.L1325:
	movq	%r13, 768(%r14)
	leaq	-60(%rbp), %rsi
	movq	%r12, %rdi
	movl	$0, -60(%rbp)
	call	_ZN6icu_6713DecimalFormat5touchER10UErrorCode.constprop.0
.L1321:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1344
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1323:
	.cfi_restore_state
	movq	360(%r12), %r13
	testq	%r13, %r13
	je	.L1331
	leaq	3840(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	3776(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	3712(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	3648(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	3560(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	3488(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	3424(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	3360(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	3296(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	3216(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1326
	movq	(%rdi), %rax
	call	*8(%rax)
.L1326:
	leaq	3184(%r13), %rdi
	leaq	1520(%r13), %r15
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	leaq	16+_ZTVN6icu_676number4impl31CurrencyPluralInfoAffixProviderE(%rip), %rax
	leaq	3160(%r13), %r14
	movq	%rax, 1520(%r13)
	leaq	1528(%r13), %rbx
	.p2align 4,,10
	.p2align 3
.L1327:
	movq	-272(%r14), %rax
	subq	$272, %r14
	movq	%r14, %rdi
	call	*(%rax)
	cmpq	%r14, %rbx
	jne	.L1327
	movq	%r15, %rdi
	leaq	1248(%r13), %r14
	call	_ZN6icu_676number4impl20AffixPatternProviderD2Ev@PLT
	leaq	16+_ZTVN6icu_676number4impl30PropertiesAffixPatternProviderE(%rip), %rax
	leaq	1448(%r13), %rdi
	movq	%rax, 1248(%r13)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	1384(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	1320(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	1256(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl20AffixPatternProviderD2Ev@PLT
	leaq	776(%r13), %rdi
	call	_ZN6icu_676number24LocalizedNumberFormatterD1Ev@PLT
	movq	768(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1328
	movq	(%rdi), %rax
	call	*8(%rax)
.L1328:
	leaq	680(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	616(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	552(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	488(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	400(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	328(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	264(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	200(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	136(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	56(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1329
	movq	(%rdi), %rax
	call	*8(%rax)
.L1329:
	leaq	24(%r13), %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L1331:
	movq	$0, 360(%r12)
	jmp	.L1321
.L1344:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3962:
	.size	_ZN6icu_6713DecimalFormat23setDecimalFormatSymbolsERKNS_20DecimalFormatSymbolsE, .-_ZN6icu_6713DecimalFormat23setDecimalFormatSymbolsERKNS_20DecimalFormatSymbolsE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713DecimalFormat23adoptCurrencyPluralInfoEPNS_18CurrencyPluralInfoE
	.type	_ZN6icu_6713DecimalFormat23adoptCurrencyPluralInfoEPNS_18CurrencyPluralInfoE, @function
_ZN6icu_6713DecimalFormat23adoptCurrencyPluralInfoEPNS_18CurrencyPluralInfoE:
.LFB3964:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	360(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.L1357
	movq	%rdi, %r12
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1348
	movq	(%rdi), %rax
	call	*8(%rax)
.L1348:
	movq	%r13, 56(%rbx)
	leaq	-44(%rbp), %rsi
	movq	%r12, %rdi
	movl	$0, -44(%rbp)
	call	_ZN6icu_6713DecimalFormat5touchER10UErrorCode.constprop.0
.L1345:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1358
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1357:
	.cfi_restore_state
	testq	%rsi, %rsi
	je	.L1345
	movq	(%rsi), %rax
	movq	%rsi, %rdi
	call	*8(%rax)
	jmp	.L1345
.L1358:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3964:
	.size	_ZN6icu_6713DecimalFormat23adoptCurrencyPluralInfoEPNS_18CurrencyPluralInfoE, .-_ZN6icu_6713DecimalFormat23adoptCurrencyPluralInfoEPNS_18CurrencyPluralInfoE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713DecimalFormat21setCurrencyPluralInfoERKNS_18CurrencyPluralInfoE
	.type	_ZN6icu_6713DecimalFormat21setCurrencyPluralInfoERKNS_18CurrencyPluralInfoE, @function
_ZN6icu_6713DecimalFormat21setCurrencyPluralInfoERKNS_18CurrencyPluralInfoE:
.LFB3967:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	360(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.L1359
	movq	%rdi, %r12
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1372
	call	_ZN6icu_6718CurrencyPluralInfoaSERKS0_@PLT
.L1363:
	leaq	-44(%rbp), %rsi
	movq	%r12, %rdi
	movl	$0, -44(%rbp)
	call	_ZN6icu_6713DecimalFormat5touchER10UErrorCode.constprop.0
.L1359:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1373
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1372:
	.cfi_restore_state
	movq	%rsi, %rdi
	call	_ZNK6icu_6718CurrencyPluralInfo5cloneEv@PLT
	movq	56(%rbx), %rdi
	movq	%rax, %r13
	testq	%rdi, %rdi
	je	.L1362
	movq	(%rdi), %rax
	call	*8(%rax)
.L1362:
	movq	%r13, 56(%rbx)
	jmp	.L1363
.L1373:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3967:
	.size	_ZN6icu_6713DecimalFormat21setCurrencyPluralInfoERKNS_18CurrencyPluralInfoE, .-_ZN6icu_6713DecimalFormat21setCurrencyPluralInfoERKNS_18CurrencyPluralInfoE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713DecimalFormat17setPositivePrefixERKNS_13UnicodeStringE
	.type	_ZN6icu_6713DecimalFormat17setPositivePrefixERKNS_13UnicodeStringE, @function
_ZN6icu_6713DecimalFormat17setPositivePrefixERKNS_13UnicodeStringE:
.LFB3969:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$16, %rsp
	movq	360(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L1374
	movswl	496(%rdi), %eax
	movswl	8(%rsi), %edx
	movq	%rsi, %r13
	movl	%eax, %ecx
	andl	$1, %ecx
	testb	$1, %dl
	jne	.L1377
	testw	%dx, %dx
	js	.L1378
	sarl	$5, %edx
.L1379:
	testw	%ax, %ax
	js	.L1380
	sarl	$5, %eax
.L1381:
	testb	%cl, %cl
	jne	.L1382
	cmpl	%edx, %eax
	je	.L1392
.L1382:
	addq	$488, %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	leaq	-28(%rbp), %rsi
	movq	%r12, %rdi
	movl	$0, -28(%rbp)
	call	_ZN6icu_6713DecimalFormat5touchER10UErrorCode.constprop.0
.L1374:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1393
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1392:
	.cfi_restore_state
	leaq	488(%rdi), %rsi
	movq	%r13, %rdi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%cl
	.p2align 4,,10
	.p2align 3
.L1377:
	testb	%cl, %cl
	jne	.L1374
	movq	360(%r12), %rdi
	jmp	.L1382
	.p2align 4,,10
	.p2align 3
.L1380:
	movl	500(%rdi), %eax
	jmp	.L1381
	.p2align 4,,10
	.p2align 3
.L1378:
	movl	12(%rsi), %edx
	jmp	.L1379
.L1393:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3969:
	.size	_ZN6icu_6713DecimalFormat17setPositivePrefixERKNS_13UnicodeStringE, .-_ZN6icu_6713DecimalFormat17setPositivePrefixERKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713DecimalFormat17setNegativePrefixERKNS_13UnicodeStringE
	.type	_ZN6icu_6713DecimalFormat17setNegativePrefixERKNS_13UnicodeStringE, @function
_ZN6icu_6713DecimalFormat17setNegativePrefixERKNS_13UnicodeStringE:
.LFB3971:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$16, %rsp
	movq	360(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L1394
	movswl	144(%rdi), %eax
	movswl	8(%rsi), %edx
	movq	%rsi, %r13
	movl	%eax, %ecx
	andl	$1, %ecx
	testb	$1, %dl
	jne	.L1397
	testw	%dx, %dx
	js	.L1398
	sarl	$5, %edx
.L1399:
	testw	%ax, %ax
	js	.L1400
	sarl	$5, %eax
.L1401:
	testb	%cl, %cl
	jne	.L1402
	cmpl	%edx, %eax
	je	.L1412
.L1402:
	addq	$136, %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	leaq	-28(%rbp), %rsi
	movq	%r12, %rdi
	movl	$0, -28(%rbp)
	call	_ZN6icu_6713DecimalFormat5touchER10UErrorCode.constprop.0
.L1394:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1413
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1412:
	.cfi_restore_state
	leaq	136(%rdi), %rsi
	movq	%r13, %rdi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%cl
	.p2align 4,,10
	.p2align 3
.L1397:
	testb	%cl, %cl
	jne	.L1394
	movq	360(%r12), %rdi
	jmp	.L1402
	.p2align 4,,10
	.p2align 3
.L1400:
	movl	148(%rdi), %eax
	jmp	.L1401
	.p2align 4,,10
	.p2align 3
.L1398:
	movl	12(%rsi), %edx
	jmp	.L1399
.L1413:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3971:
	.size	_ZN6icu_6713DecimalFormat17setNegativePrefixERKNS_13UnicodeStringE, .-_ZN6icu_6713DecimalFormat17setNegativePrefixERKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713DecimalFormat17setPositiveSuffixERKNS_13UnicodeStringE
	.type	_ZN6icu_6713DecimalFormat17setPositiveSuffixERKNS_13UnicodeStringE, @function
_ZN6icu_6713DecimalFormat17setPositiveSuffixERKNS_13UnicodeStringE:
.LFB3973:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$16, %rsp
	movq	360(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L1414
	movswl	624(%rdi), %eax
	movswl	8(%rsi), %edx
	movq	%rsi, %r13
	movl	%eax, %ecx
	andl	$1, %ecx
	testb	$1, %dl
	jne	.L1417
	testw	%dx, %dx
	js	.L1418
	sarl	$5, %edx
.L1419:
	testw	%ax, %ax
	js	.L1420
	sarl	$5, %eax
.L1421:
	testb	%cl, %cl
	jne	.L1422
	cmpl	%edx, %eax
	je	.L1432
.L1422:
	addq	$616, %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	leaq	-28(%rbp), %rsi
	movq	%r12, %rdi
	movl	$0, -28(%rbp)
	call	_ZN6icu_6713DecimalFormat5touchER10UErrorCode.constprop.0
.L1414:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1433
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1432:
	.cfi_restore_state
	leaq	616(%rdi), %rsi
	movq	%r13, %rdi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%cl
	.p2align 4,,10
	.p2align 3
.L1417:
	testb	%cl, %cl
	jne	.L1414
	movq	360(%r12), %rdi
	jmp	.L1422
	.p2align 4,,10
	.p2align 3
.L1420:
	movl	628(%rdi), %eax
	jmp	.L1421
	.p2align 4,,10
	.p2align 3
.L1418:
	movl	12(%rsi), %edx
	jmp	.L1419
.L1433:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3973:
	.size	_ZN6icu_6713DecimalFormat17setPositiveSuffixERKNS_13UnicodeStringE, .-_ZN6icu_6713DecimalFormat17setPositiveSuffixERKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713DecimalFormat17setNegativeSuffixERKNS_13UnicodeStringE
	.type	_ZN6icu_6713DecimalFormat17setNegativeSuffixERKNS_13UnicodeStringE, @function
_ZN6icu_6713DecimalFormat17setNegativeSuffixERKNS_13UnicodeStringE:
.LFB3975:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$16, %rsp
	movq	360(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L1434
	movswl	272(%rdi), %eax
	movswl	8(%rsi), %edx
	movq	%rsi, %r13
	movl	%eax, %ecx
	andl	$1, %ecx
	testb	$1, %dl
	jne	.L1437
	testw	%dx, %dx
	js	.L1438
	sarl	$5, %edx
.L1439:
	testw	%ax, %ax
	js	.L1440
	sarl	$5, %eax
.L1441:
	testb	%cl, %cl
	jne	.L1442
	cmpl	%edx, %eax
	je	.L1452
.L1442:
	addq	$264, %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	leaq	-28(%rbp), %rsi
	movq	%r12, %rdi
	movl	$0, -28(%rbp)
	call	_ZN6icu_6713DecimalFormat5touchER10UErrorCode.constprop.0
.L1434:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1453
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1452:
	.cfi_restore_state
	leaq	264(%rdi), %rsi
	movq	%r13, %rdi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%cl
	.p2align 4,,10
	.p2align 3
.L1437:
	testb	%cl, %cl
	jne	.L1434
	movq	360(%r12), %rdi
	jmp	.L1442
	.p2align 4,,10
	.p2align 3
.L1440:
	movl	276(%rdi), %eax
	jmp	.L1441
	.p2align 4,,10
	.p2align 3
.L1438:
	movl	12(%rsi), %edx
	jmp	.L1439
.L1453:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3975:
	.size	_ZN6icu_6713DecimalFormat17setNegativeSuffixERKNS_13UnicodeStringE, .-_ZN6icu_6713DecimalFormat17setNegativeSuffixERKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713DecimalFormat20setRoundingIncrementEd
	.type	_ZN6icu_6713DecimalFormat20setRoundingIncrementEd, @function
_ZN6icu_6713DecimalFormat20setRoundingIncrementEd:
.LFB3983:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	360(%rdi), %rax
	testq	%rax, %rax
	je	.L1454
	ucomisd	744(%rax), %xmm0
	jp	.L1458
	jne	.L1458
.L1454:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1463
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1458:
	.cfi_restore_state
	movsd	%xmm0, 744(%rax)
	leaq	-12(%rbp), %rsi
	movl	$0, -12(%rbp)
	call	_ZN6icu_6713DecimalFormat5touchER10UErrorCode.constprop.0
	jmp	.L1454
.L1463:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3983:
	.size	_ZN6icu_6713DecimalFormat20setRoundingIncrementEd, .-_ZN6icu_6713DecimalFormat20setRoundingIncrementEd
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713DecimalFormat15setPadCharacterERKNS_13UnicodeStringE
	.type	_ZN6icu_6713DecimalFormat15setPadCharacterERKNS_13UnicodeStringE, @function
_ZN6icu_6713DecimalFormat15setPadCharacterERKNS_13UnicodeStringE:
.LFB3989:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%rsi, %rdi
	subq	$112, %rsp
	movq	360(%r12), %rsi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L1464
	movswl	408(%rsi), %ecx
	movswl	8(%rdi), %eax
	movl	%ecx, %r8d
	andl	$1, %r8d
	testb	$1, %al
	jne	.L1467
	testw	%ax, %ax
	js	.L1468
	movswl	%ax, %edx
	sarl	$5, %edx
.L1469:
	testw	%cx, %cx
	js	.L1470
	sarl	$5, %ecx
.L1471:
	cmpl	%edx, %ecx
	jne	.L1472
	testb	%r8b, %r8b
	je	.L1486
.L1472:
	testw	%ax, %ax
	js	.L1474
.L1488:
	sarl	$5, %eax
	testl	%eax, %eax
	jle	.L1476
.L1489:
	xorl	%esi, %esi
	leaq	-96(%rbp), %r13
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movq	%r13, %rdi
	movl	%eax, %esi
	call	_ZN6icu_6713UnicodeStringC1Ei@PLT
	movq	360(%r12), %rax
	movq	%r13, %rsi
	leaq	400(%rax), %rdi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L1477:
	leaq	-100(%rbp), %rsi
	movq	%r12, %rdi
	movl	$0, -100(%rbp)
	call	_ZN6icu_6713DecimalFormat5touchER10UErrorCode.constprop.0
.L1464:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1487
	addq	$112, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1486:
	.cfi_restore_state
	addq	$400, %rsi
	movq	%rdi, -120(%rbp)
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	movq	-120(%rbp), %rdi
	testb	%al, %al
	setne	%r8b
	.p2align 4,,10
	.p2align 3
.L1467:
	testb	%r8b, %r8b
	jne	.L1464
	movswl	8(%rdi), %eax
	testw	%ax, %ax
	jns	.L1488
.L1474:
	movl	12(%rdi), %eax
	testl	%eax, %eax
	jg	.L1489
.L1476:
	movq	360(%r12), %rax
	leaq	400(%rax), %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	jmp	.L1477
	.p2align 4,,10
	.p2align 3
.L1470:
	movl	412(%rsi), %ecx
	jmp	.L1471
	.p2align 4,,10
	.p2align 3
.L1468:
	movl	12(%rdi), %edx
	jmp	.L1469
.L1487:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3989:
	.size	_ZN6icu_6713DecimalFormat15setPadCharacterERKNS_13UnicodeStringE, .-_ZN6icu_6713DecimalFormat15setPadCharacterERKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713DecimalFormat21setScientificNotationEa
	.type	_ZN6icu_6713DecimalFormat21setScientificNotationEa, @function
_ZN6icu_6713DecimalFormat21setScientificNotationEa:
.LFB3993:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	360(%rdi), %rax
	testq	%rax, %rax
	je	.L1490
	movl	104(%rax), %edx
	testb	%sil, %sil
	jne	.L1492
	cmpl	$-1, %edx
	je	.L1490
	movl	$-1, 104(%rax)
.L1494:
	leaq	-12(%rbp), %rsi
	movl	$0, -12(%rbp)
	call	_ZN6icu_6713DecimalFormat5touchER10UErrorCode.constprop.0
.L1490:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1511
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1492:
	.cfi_restore_state
	cmpl	$1, %edx
	je	.L1490
	movl	$1, 104(%rax)
	jmp	.L1494
.L1511:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3993:
	.size	_ZN6icu_6713DecimalFormat21setScientificNotationEa, .-_ZN6icu_6713DecimalFormat21setScientificNotationEa
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713DecimalFormat24setMinimumExponentDigitsEa
	.type	_ZN6icu_6713DecimalFormat24setMinimumExponentDigitsEa, @function
_ZN6icu_6713DecimalFormat24setMinimumExponentDigitsEa:
.LFB3995:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	360(%rdi), %rax
	testq	%rax, %rax
	je	.L1512
	movsbl	%sil, %esi
	cmpl	104(%rax), %esi
	je	.L1512
	movl	%esi, 104(%rax)
	leaq	-12(%rbp), %rsi
	movl	$0, -12(%rbp)
	call	_ZN6icu_6713DecimalFormat5touchER10UErrorCode.constprop.0
.L1512:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1519
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1519:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3995:
	.size	_ZN6icu_6713DecimalFormat24setMinimumExponentDigitsEa, .-_ZN6icu_6713DecimalFormat24setMinimumExponentDigitsEa
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713DecimalFormat26setExponentSignAlwaysShownEa
	.type	_ZN6icu_6713DecimalFormat26setExponentSignAlwaysShownEa, @function
_ZN6icu_6713DecimalFormat26setExponentSignAlwaysShownEa:
.LFB3997:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	360(%rdi), %rax
	testq	%rax, %rax
	je	.L1520
	movzbl	74(%rax), %ecx
	movsbl	%sil, %edx
	cmpl	%ecx, %edx
	je	.L1520
	testb	%sil, %sil
	leaq	-12(%rbp), %rsi
	movl	$0, -12(%rbp)
	setne	74(%rax)
	call	_ZN6icu_6713DecimalFormat5touchER10UErrorCode.constprop.0
.L1520:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1527
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1527:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3997:
	.size	_ZN6icu_6713DecimalFormat26setExponentSignAlwaysShownEa, .-_ZN6icu_6713DecimalFormat26setExponentSignAlwaysShownEa
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713DecimalFormat12touchNoErrorEv
	.type	_ZN6icu_6713DecimalFormat12touchNoErrorEv, @function
_ZN6icu_6713DecimalFormat12touchNoErrorEv:
.LFB4045:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-12(%rbp), %rsi
	movl	$0, -12(%rbp)
	call	_ZN6icu_6713DecimalFormat5touchER10UErrorCode.constprop.0
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1531
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1531:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4045:
	.size	_ZN6icu_6713DecimalFormat12touchNoErrorEv, .-_ZN6icu_6713DecimalFormat12touchNoErrorEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713DecimalFormat12setAttributeE22UNumberFormatAttributeiR10UErrorCode
	.type	_ZN6icu_6713DecimalFormat12setAttributeE22UNumberFormatAttributeiR10UErrorCode, @function
_ZN6icu_6713DecimalFormat12setAttributeE22UNumberFormatAttributeiR10UErrorCode:
.LFB3911:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L1534
	movq	360(%rdi), %r8
	testq	%r8, %r8
	je	.L1674
	cmpl	$23, %esi
	ja	.L1675
	leaq	.L1546(%rip), %rdi
	movl	%esi, %esi
	movslq	(%rdi,%rsi,4), %rax
	addq	%rdi, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L1546:
	.long	.L1568-.L1546
	.long	.L1567-.L1546
	.long	.L1566-.L1546
	.long	.L1565-.L1546
	.long	.L1564-.L1546
	.long	.L1563-.L1546
	.long	.L1562-.L1546
	.long	.L1561-.L1546
	.long	.L1560-.L1546
	.long	.L1559-.L1546
	.long	.L1558-.L1546
	.long	.L1557-.L1546
	.long	.L1538-.L1546
	.long	.L1556-.L1546
	.long	.L1555-.L1546
	.long	.L1554-.L1546
	.long	.L1553-.L1546
	.long	.L1552-.L1546
	.long	.L1551-.L1546
	.long	.L1550-.L1546
	.long	.L1549-.L1546
	.long	.L1548-.L1546
	.long	.L1547-.L1546
	.long	.L1545-.L1546
	.text
.L1564:
	movq	(%r12), %rax
	leaq	_ZN6icu_6713DecimalFormat23setMinimumIntegerDigitsEi(%rip), %rcx
	movq	224(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1610
	cmpl	116(%r8), %edx
	je	.L1534
	movl	96(%r8), %eax
	testl	%eax, %eax
	js	.L1579
	cmpl	%eax, %edx
	jle	.L1579
	movl	%edx, 96(%r8)
.L1579:
	movl	%edx, 116(%r8)
	leaq	-28(%rbp), %rsi
	movq	%r12, %rdi
	movl	$0, -28(%rbp)
	call	_ZN6icu_6713DecimalFormat5touchER10UErrorCode.constprop.0
	.p2align 4,,10
	.p2align 3
.L1534:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1676
	addq	$32, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1675:
	.cfi_restore_state
	subl	$4096, %esi
	cmpl	$4, %esi
	ja	.L1538
	leaq	.L1540(%rip), %rcx
	movslq	(%rcx,%rsi,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L1540:
	.long	.L1544-.L1540
	.long	.L1543-.L1540
	.long	.L1542-.L1540
	.long	.L1541-.L1540
	.long	.L1539-.L1540
	.text
	.p2align 4,,10
	.p2align 3
.L1674:
	movl	$7, (%rcx)
	jmp	.L1534
.L1541:
	movzbl	464(%r8), %ecx
	movsbl	%dl, %eax
	cmpl	%ecx, %eax
	je	.L1534
	testb	%dl, %dl
	leaq	-28(%rbp), %rsi
	movq	%r12, %rdi
	setne	464(%r8)
	movl	$0, -28(%rbp)
	call	_ZN6icu_6713DecimalFormat5touchER10UErrorCode.constprop.0
	jmp	.L1534
.L1538:
	movl	$16, (%rcx)
	jmp	.L1534
.L1568:
	movq	(%r12), %rax
	testl	%edx, %edx
	leaq	_ZN6icu_6713DecimalFormat19setParseIntegerOnlyEa(%rip), %rdx
	setne	%sil
	setne	%bl
	movq	184(%rax), %rax
	movzbl	%sil, %esi
	cmpq	%rdx, %rax
	jne	.L1662
	movzbl	465(%r8), %eax
	cmpl	%eax, %esi
	je	.L1534
	movq	%r12, %rdi
	call	_ZN6icu_6712NumberFormat19setParseIntegerOnlyEa@PLT
	movq	360(%r12), %rax
	leaq	-28(%rbp), %rsi
	movq	%r12, %rdi
	movb	%bl, 465(%rax)
	movl	$0, -28(%rbp)
	call	_ZN6icu_6713DecimalFormat5touchER10UErrorCode.constprop.0
	jmp	.L1534
.L1567:
	movq	(%r12), %rax
	testl	%edx, %edx
	leaq	_ZN6icu_6713DecimalFormat15setGroupingUsedEa(%rip), %rdx
	setne	%sil
	setne	%bl
	movq	208(%rax), %rax
	movzbl	%sil, %esi
	cmpq	%rdx, %rax
	jne	.L1662
	movzbl	84(%r8), %eax
	cmpl	%eax, %esi
	je	.L1534
	movq	%r12, %rdi
	call	_ZN6icu_6712NumberFormat15setGroupingUsedEa@PLT
	movq	360(%r12), %rax
	leaq	-28(%rbp), %rsi
	movq	%r12, %rdi
	movb	%bl, 84(%rax)
	movl	$0, -28(%rbp)
	call	_ZN6icu_6713DecimalFormat5touchER10UErrorCode.constprop.0
	jmp	.L1534
.L1566:
	movq	(%r12), %rax
	testl	%edx, %edx
	leaq	_ZN6icu_6713DecimalFormat30setDecimalSeparatorAlwaysShownEa(%rip), %rdx
	movq	528(%rax), %rcx
	setne	%al
	cmpq	%rdx, %rcx
	jne	.L1575
	cmpb	73(%r8), %al
	je	.L1534
	movb	%al, 73(%r8)
	leaq	-28(%rbp), %rsi
	movq	%r12, %rdi
	movl	$0, -28(%rbp)
	call	_ZN6icu_6713DecimalFormat5touchER10UErrorCode.constprop.0
	jmp	.L1534
.L1565:
	movq	(%r12), %rax
	leaq	_ZN6icu_6713DecimalFormat23setMaximumIntegerDigitsEi(%rip), %rcx
	movq	216(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1610
.L1672:
	cmpl	96(%r8), %edx
	je	.L1534
	movl	116(%r8), %eax
	testl	%eax, %eax
	js	.L1585
	cmpl	%eax, %edx
	jge	.L1585
	movl	%edx, 116(%r8)
.L1585:
	movl	%edx, 96(%r8)
	leaq	-28(%rbp), %rsi
	movq	%r12, %rdi
	movl	$0, -28(%rbp)
	call	_ZN6icu_6713DecimalFormat5touchER10UErrorCode.constprop.0
	jmp	.L1534
.L1563:
	movq	(%r12), %rcx
	leaq	_ZN6icu_6713DecimalFormat23setMinimumIntegerDigitsEi(%rip), %rsi
	movq	224(%rcx), %rax
	cmpq	%rsi, %rax
	jne	.L1580
	cmpl	116(%r8), %edx
	je	.L1581
	movl	96(%r8), %eax
	testl	%eax, %eax
	js	.L1582
	cmpl	%eax, %edx
	jle	.L1582
	movl	%edx, 96(%r8)
.L1582:
	movl	%edx, 116(%r8)
	leaq	-28(%rbp), %rsi
	movq	%r12, %rdi
	movl	%edx, -36(%rbp)
	movl	$0, -28(%rbp)
	call	_ZN6icu_6713DecimalFormat5touchER10UErrorCode.constprop.0
	movl	-36(%rbp), %edx
.L1583:
	movq	(%r12), %rax
	leaq	_ZN6icu_6713DecimalFormat23setMaximumIntegerDigitsEi(%rip), %rcx
	movq	216(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1610
	movq	360(%r12), %r8
	testq	%r8, %r8
	jne	.L1672
	jmp	.L1534
.L1562:
	movq	(%r12), %rax
	leaq	_ZN6icu_6713DecimalFormat24setMaximumFractionDigitsEi(%rip), %rcx
	movq	232(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1610
.L1615:
	cmpl	92(%r8), %edx
	je	.L1534
	movl	$999, %eax
	cmpl	$999, %edx
	cmovg	%eax, %edx
	movl	108(%r8), %eax
	testl	%eax, %eax
	js	.L1595
	cmpl	%eax, %edx
	jge	.L1595
	movl	%edx, 108(%r8)
.L1595:
	movl	%edx, 92(%r8)
	leaq	-28(%rbp), %rsi
	movq	%r12, %rdi
	movl	$0, -28(%rbp)
	call	_ZN6icu_6713DecimalFormat5touchER10UErrorCode.constprop.0
	jmp	.L1534
.L1561:
	movq	(%r12), %rax
	leaq	_ZN6icu_6713DecimalFormat24setMinimumFractionDigitsEi(%rip), %rcx
	movq	240(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1610
	cmpl	108(%r8), %edx
	je	.L1534
	movl	92(%r8), %eax
	testl	%eax, %eax
	js	.L1589
	cmpl	%eax, %edx
	jle	.L1589
	movl	%edx, 92(%r8)
.L1589:
	movl	%edx, 108(%r8)
	leaq	-28(%rbp), %rsi
	movq	%r12, %rdi
	movl	$0, -28(%rbp)
	call	_ZN6icu_6713DecimalFormat5touchER10UErrorCode.constprop.0
	jmp	.L1534
.L1560:
	movq	(%r12), %rcx
	leaq	_ZN6icu_6713DecimalFormat24setMinimumFractionDigitsEi(%rip), %rsi
	movq	240(%rcx), %rax
	cmpq	%rsi, %rax
	jne	.L1590
	cmpl	108(%r8), %edx
	je	.L1591
	movl	92(%r8), %eax
	testl	%eax, %eax
	js	.L1592
	cmpl	%eax, %edx
	jle	.L1592
	movl	%edx, 92(%r8)
.L1592:
	movl	%edx, 108(%r8)
	leaq	-28(%rbp), %rsi
	movq	%r12, %rdi
	movl	%edx, -36(%rbp)
	movl	$0, -28(%rbp)
	call	_ZN6icu_6713DecimalFormat5touchER10UErrorCode.constprop.0
	movl	-36(%rbp), %edx
.L1593:
	movq	(%r12), %rax
	leaq	_ZN6icu_6713DecimalFormat24setMaximumFractionDigitsEi(%rip), %rcx
	movq	232(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1610
	movq	360(%r12), %r8
	testq	%r8, %r8
	jne	.L1615
	jmp	.L1534
.L1559:
	movq	(%r12), %rax
	leaq	_ZN6icu_6713DecimalFormat13setMultiplierEi(%rip), %rcx
	movq	392(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1610
	testl	%edx, %edx
	je	.L1601
	movl	%edx, %ecx
	xorl	%edi, %edi
	jmp	.L1603
	.p2align 4,,10
	.p2align 3
.L1677:
	movl	%eax, %ecx
.L1603:
	cmpl	$1, %ecx
	je	.L1618
	movslq	%ecx, %rax
	movl	%ecx, %esi
	addl	$1, %edi
	imulq	$1717986919, %rax, %rax
	sarl	$31, %esi
	sarq	$34, %rax
	subl	%esi, %eax
	leal	(%rax,%rax,4), %esi
	addl	%esi, %esi
	cmpl	%esi, %ecx
	je	.L1677
	movl	$0, 88(%r8)
	movl	%edx, 124(%r8)
	jmp	.L1661
.L1558:
	movq	(%r12), %rax
	leaq	_ZN6icu_6713DecimalFormat15setGroupingSizeEi(%rip), %rcx
	movq	512(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1610
	cmpl	80(%r8), %edx
	je	.L1534
	movl	%edx, 80(%r8)
	leaq	-28(%rbp), %rsi
	movq	%r12, %rdi
	movl	$0, -28(%rbp)
	call	_ZN6icu_6713DecimalFormat5touchER10UErrorCode.constprop.0
	jmp	.L1534
.L1557:
	movq	(%r12), %rax
	leaq	_ZN6icu_6713DecimalFormat15setRoundingModeENS_12NumberFormat13ERoundingModeE(%rip), %rcx
	movq	280(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1610
	cmpb	$0, 752(%r8)
	jne	.L1606
	cmpl	756(%r8), %edx
	je	.L1534
.L1606:
	movl	%edx, %esi
	movq	%r12, %rdi
	movl	%edx, -36(%rbp)
	call	_ZN6icu_6712NumberFormat23setMaximumIntegerDigitsEi@PLT
	movl	-36(%rbp), %edx
	leaq	-28(%rbp), %rsi
	movq	%r12, %rdi
	movq	360(%r12), %rax
	movl	%edx, 756(%rax)
	movb	$0, 752(%rax)
	movl	$0, -28(%rbp)
	call	_ZN6icu_6713DecimalFormat5touchER10UErrorCode.constprop.0
	jmp	.L1534
.L1556:
	movq	(%r12), %rax
	leaq	_ZN6icu_6713DecimalFormat14setFormatWidthEi(%rip), %rcx
	movq	424(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1610
	cmpl	76(%r8), %edx
	je	.L1534
	movl	%edx, 76(%r8)
	leaq	-28(%rbp), %rsi
	movq	%r12, %rdi
	movl	$0, -28(%rbp)
	call	_ZN6icu_6713DecimalFormat5touchER10UErrorCode.constprop.0
	jmp	.L1534
.L1555:
	movq	(%r12), %rax
	leaq	_ZN6icu_6713DecimalFormat14setPadPositionENS0_12EPadPositionE(%rip), %rcx
	movq	456(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1610
	cmpb	$0, 392(%r8)
	jne	.L1609
	cmpl	396(%r8), %edx
	je	.L1534
.L1609:
	movl	%edx, 396(%r8)
	leaq	-28(%rbp), %rsi
	movq	%r12, %rdi
	movb	$0, 392(%r8)
	movl	$0, -28(%rbp)
	call	_ZN6icu_6713DecimalFormat5touchER10UErrorCode.constprop.0
	jmp	.L1534
.L1554:
	movq	(%r12), %rax
	leaq	_ZN6icu_6713DecimalFormat24setSecondaryGroupingSizeEi(%rip), %rcx
	movq	520(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1610
	cmpl	760(%r8), %edx
	je	.L1534
	movl	%edx, 760(%r8)
	leaq	-28(%rbp), %rsi
	movq	%r12, %rdi
	movl	$0, -28(%rbp)
	call	_ZN6icu_6713DecimalFormat5touchER10UErrorCode.constprop.0
	jmp	.L1534
.L1553:
	movl	120(%r8), %eax
	testl	%edx, %edx
	je	.L1596
	cmpl	$-1, %eax
	jne	.L1534
	cmpl	$-1, 100(%r8)
	jne	.L1534
	movl	$1, %edx
	movl	$6, %eax
.L1597:
	movl	%edx, 120(%r8)
	leaq	-28(%rbp), %rsi
	movq	%r12, %rdi
	movl	%eax, 100(%r8)
	movl	$0, -28(%rbp)
	call	_ZN6icu_6713DecimalFormat5touchER10UErrorCode.constprop.0
	jmp	.L1534
.L1550:
	movq	(%r12), %rax
	xorl	%esi, %esi
	leaq	_ZN6icu_6713DecimalFormat10setLenientEa(%rip), %rcx
	testl	%edx, %edx
	setne	%sil
	movq	192(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1662
	xorl	%ebx, %ebx
	testl	%edx, %edx
	sete	%bl
	cmpb	$0, 468(%r8)
	jne	.L1571
	cmpl	472(%r8), %ebx
	je	.L1534
.L1571:
	movq	%r12, %rdi
	call	_ZN6icu_6712NumberFormat10setLenientEa@PLT
	movq	360(%r12), %rax
	leaq	-28(%rbp), %rsi
	movq	%r12, %rdi
	movl	%ebx, 472(%rax)
	movb	$0, 468(%rax)
	movl	$0, -28(%rbp)
	call	_ZN6icu_6713DecimalFormat5touchER10UErrorCode.constprop.0
	jmp	.L1534
.L1539:
	movzbl	764(%r8), %ecx
	movsbl	%dl, %eax
	cmpl	%ecx, %eax
	je	.L1534
	testb	%dl, %dl
	leaq	-28(%rbp), %rsi
	movq	%r12, %rdi
	setne	764(%r8)
	movl	$0, -28(%rbp)
	call	_ZN6icu_6713DecimalFormat5touchER10UErrorCode.constprop.0
	jmp	.L1534
.L1544:
	movzbl	75(%r8), %ecx
	movsbl	%dl, %eax
	cmpl	%ecx, %eax
	je	.L1534
	testb	%dl, %dl
	leaq	-28(%rbp), %rsi
	movq	%r12, %rdi
	setne	75(%r8)
	movl	$0, -28(%rbp)
	call	_ZN6icu_6713DecimalFormat5touchER10UErrorCode.constprop.0
	jmp	.L1534
.L1543:
	movzbl	476(%r8), %ecx
	movsbl	%dl, %eax
	cmpl	%ecx, %eax
	je	.L1534
	testb	%dl, %dl
	leaq	-28(%rbp), %rsi
	movq	%r12, %rdi
	setne	476(%r8)
	movl	$0, -28(%rbp)
	call	_ZN6icu_6713DecimalFormat5touchER10UErrorCode.constprop.0
	jmp	.L1534
.L1542:
	movq	(%r12), %rax
	leaq	_ZN6icu_6713DecimalFormat30setDecimalPatternMatchRequiredEa(%rip), %rcx
	movsbl	%dl, %esi
	movq	536(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1662
	movzbl	72(%r8), %eax
	cmpl	%eax, %esi
	je	.L1534
	testb	%dl, %dl
	leaq	-28(%rbp), %rsi
	movq	%r12, %rdi
	setne	72(%r8)
	movl	$0, -28(%rbp)
	call	_ZN6icu_6713DecimalFormat5touchER10UErrorCode.constprop.0
	jmp	.L1534
.L1549:
	cmpl	480(%r8), %edx
	je	.L1534
	movl	%edx, 480(%r8)
	jmp	.L1534
.L1545:
	cmpb	$0, 64(%r8)
	jne	.L1612
	cmpl	68(%r8), %edx
	je	.L1534
.L1612:
	movl	%edx, 68(%r8)
	movq	%rcx, %rsi
	movq	%r12, %rdi
	movb	$0, 64(%r8)
	call	_ZN6icu_6713DecimalFormat5touchER10UErrorCode
	jmp	.L1534
.L1552:
	cmpl	120(%r8), %edx
	je	.L1534
	movl	100(%r8), %eax
	testl	%eax, %eax
	js	.L1599
	cmpl	%eax, %edx
	jle	.L1599
	movl	%edx, 100(%r8)
.L1599:
	movl	%edx, 120(%r8)
	leaq	-28(%rbp), %rsi
	movq	%r12, %rdi
	movl	$0, -28(%rbp)
	call	_ZN6icu_6713DecimalFormat5touchER10UErrorCode.constprop.0
	jmp	.L1534
.L1551:
	cmpl	100(%r8), %edx
	je	.L1534
	movl	120(%r8), %eax
	testl	%eax, %eax
	js	.L1598
	cmpl	%eax, %edx
	jge	.L1598
	movl	%edx, 120(%r8)
.L1598:
	movl	%edx, 100(%r8)
	leaq	-28(%rbp), %rsi
	movq	%r12, %rdi
	movl	$0, -28(%rbp)
	call	_ZN6icu_6713DecimalFormat5touchER10UErrorCode.constprop.0
	jmp	.L1534
.L1548:
	cmpl	128(%r8), %edx
	je	.L1534
	movl	%edx, 128(%r8)
.L1661:
	leaq	-28(%rbp), %rsi
	movq	%r12, %rdi
	movl	$0, -28(%rbp)
	call	_ZN6icu_6713DecimalFormat5touchER10UErrorCode.constprop.0
	jmp	.L1534
.L1547:
	cmpl	112(%r8), %edx
	je	.L1534
	movl	%edx, 112(%r8)
	leaq	-28(%rbp), %rsi
	movq	%r12, %rdi
	movl	$0, -28(%rbp)
	call	_ZN6icu_6713DecimalFormat5touchER10UErrorCode.constprop.0
	jmp	.L1534
.L1591:
	movq	232(%rcx), %rax
	leaq	_ZN6icu_6713DecimalFormat24setMaximumFractionDigitsEi(%rip), %rcx
	cmpq	%rcx, %rax
	je	.L1615
	.p2align 4,,10
	.p2align 3
.L1610:
	movl	%edx, %esi
.L1662:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1534
.L1618:
	movl	%edi, %edx
.L1601:
	movl	%edx, 88(%r8)
	movl	$1, 124(%r8)
	jmp	.L1661
.L1596:
	cmpl	$-1, %eax
	je	.L1678
	movl	$-1, %edx
	movl	$-1, %eax
	jmp	.L1597
.L1580:
	movl	%edx, -36(%rbp)
	movl	%edx, %esi
	movq	%r12, %rdi
	call	*%rax
	movl	-36(%rbp), %edx
	jmp	.L1583
.L1575:
	movzbl	%al, %esi
	movq	%r12, %rdi
	call	*%rcx
	jmp	.L1534
.L1590:
	movl	%edx, -36(%rbp)
	movl	%edx, %esi
	movq	%r12, %rdi
	call	*%rax
	movl	-36(%rbp), %edx
	jmp	.L1593
.L1678:
	cmpl	$-1, 100(%r8)
	movl	$-1, %edx
	jne	.L1597
	jmp	.L1534
.L1581:
	movq	216(%rcx), %rax
	leaq	_ZN6icu_6713DecimalFormat23setMaximumIntegerDigitsEi(%rip), %rcx
	cmpq	%rcx, %rax
	jne	.L1610
	cmpl	96(%r8), %edx
	jne	.L1585
	jmp	.L1534
.L1676:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3911:
	.size	_ZN6icu_6713DecimalFormat12setAttributeE22UNumberFormatAttributeiR10UErrorCode, .-_ZN6icu_6713DecimalFormat12setAttributeE22UNumberFormatAttributeiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713DecimalFormat19fieldPositionHelperERKNS_6number4impl20UFormattedNumberDataERNS_13FieldPositionEiR10UErrorCode
	.type	_ZN6icu_6713DecimalFormat19fieldPositionHelperERKNS_6number4impl20UFormattedNumberDataERNS_13FieldPositionEiR10UErrorCode, @function
_ZN6icu_6713DecimalFormat19fieldPositionHelperERKNS_6number4impl20UFormattedNumberDataERNS_13FieldPositionEiR10UErrorCode:
.LFB4049:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$56, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	(%rcx), %eax
	testl	%eax, %eax
	jle	.L1691
.L1679:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1692
	addq	$56, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1691:
	.cfi_restore_state
	movq	$0, 12(%rsi)
	movl	%edx, %r13d
	movq	%rcx, %rdx
	movq	%rsi, %r12
	call	_ZNK6icu_6731FormattedValueStringBuilderImpl17nextFieldPositionERNS_13FieldPositionER10UErrorCode@PLT
	testl	%r13d, %r13d
	je	.L1679
	testb	%al, %al
	je	.L1679
	leaq	-80(%rbp), %r14
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6724FieldPositionOnlyHandlerC1ERNS_13FieldPositionE@PLT
	movq	%r14, %rdi
	movl	%r13d, %esi
	call	_ZN6icu_6724FieldPositionOnlyHandler9shiftLastEi@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6724FieldPositionOnlyHandlerD1Ev@PLT
	jmp	.L1679
.L1692:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4049:
	.size	_ZN6icu_6713DecimalFormat19fieldPositionHelperERKNS_6number4impl20UFormattedNumberDataERNS_13FieldPositionEiR10UErrorCode, .-_ZN6icu_6713DecimalFormat19fieldPositionHelperERKNS_6number4impl20UFormattedNumberDataERNS_13FieldPositionEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713DecimalFormat6formatERKNS_6number4impl15DecimalQuantityERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.type	_ZNK6icu_6713DecimalFormat6formatERKNS_6number4impl15DecimalQuantityERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode, @function
_ZNK6icu_6713DecimalFormat6formatERKNS_6number4impl15DecimalQuantityERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode:
.LFB3952:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$296, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r8), %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L1694
	cmpq	$0, 360(%rdi)
	movq	%rdi, %r13
	movq	%r8, %r12
	je	.L1709
	movq	%rsi, %r15
	leaq	-288(%rbp), %rbx
	movzbl	_ZN6icu_67L15kUndefinedFieldE(%rip), %esi
	movq	%rcx, -336(%rbp)
	movq	%rbx, %rdi
	call	_ZN6icu_6731FormattedValueStringBuilderImplC2ENS_22FormattedStringBuilder5FieldE@PLT
	leaq	16+_ZTVN6icu_676number4impl20UFormattedNumberDataE(%rip), %rax
	leaq	-136(%rbp), %rdi
	movq	%rax, -288(%rbp)
	movq	%rdi, -328(%rbp)
	call	_ZN6icu_676number4impl15DecimalQuantityC1Ev@PLT
	movq	-328(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN6icu_676number4impl15DecimalQuantityaSERKS2_@PLT
	movq	360(%r13), %rdi
	movq	%r12, %rdx
	movq	%rbx, %rsi
	addq	$776, %rdi
	call	_ZNK6icu_676number24LocalizedNumberFormatter10formatImplEPNS0_4impl20UFormattedNumberDataER10UErrorCode@PLT
	movzwl	8(%r14), %eax
	movq	-336(%rbp), %r9
	testw	%ax, %ax
	js	.L1696
	movswl	%ax, %r13d
	movl	(%r12), %eax
	leaq	-320(%rbp), %r15
	sarl	$5, %r13d
	testl	%eax, %eax
	jle	.L1710
.L1699:
	leaq	16+_ZTVN6icu_6723UnicodeStringAppendableE(%rip), %rax
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%rax, -320(%rbp)
	movq	%r14, -312(%rbp)
	call	_ZNK6icu_6731FormattedValueStringBuilderImpl8appendToERNS_10AppendableER10UErrorCode@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6723UnicodeStringAppendableD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_676number4impl20UFormattedNumberDataD1Ev@PLT
.L1694:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1711
	addq	$296, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1709:
	.cfi_restore_state
	movl	$7, (%r8)
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	jmp	.L1694
	.p2align 4,,10
	.p2align 3
.L1696:
	movl	(%r12), %eax
	movl	12(%r14), %r13d
	leaq	-320(%rbp), %r15
	testl	%eax, %eax
	jg	.L1699
.L1710:
	movq	%r12, %rdx
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r9, -328(%rbp)
	movq	$0, 12(%r9)
	leaq	-320(%rbp), %r15
	call	_ZNK6icu_6731FormattedValueStringBuilderImpl17nextFieldPositionERNS_13FieldPositionER10UErrorCode@PLT
	testb	%al, %al
	je	.L1699
	testl	%r13d, %r13d
	je	.L1699
	movq	-328(%rbp), %r9
	movq	%r15, %rdi
	movq	%r9, %rsi
	call	_ZN6icu_6724FieldPositionOnlyHandlerC1ERNS_13FieldPositionE@PLT
	movq	%r15, %rdi
	movl	%r13d, %esi
	call	_ZN6icu_6724FieldPositionOnlyHandler9shiftLastEi@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6724FieldPositionOnlyHandlerD1Ev@PLT
	jmp	.L1699
.L1711:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3952:
	.size	_ZNK6icu_6713DecimalFormat6formatERKNS_6number4impl15DecimalQuantityERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode, .-_ZNK6icu_6713DecimalFormat6formatERKNS_6number4impl15DecimalQuantityERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713DecimalFormat6formatEdRNS_13UnicodeStringERNS_13FieldPositionE
	.type	_ZNK6icu_6713DecimalFormat6formatEdRNS_13UnicodeStringERNS_13FieldPositionE, @function
_ZNK6icu_6713DecimalFormat6formatEdRNS_13UnicodeStringERNS_13FieldPositionE:
.LFB3935:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$312, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	360(%rdi), %rax
	testq	%rax, %rax
	je	.L1729
	cmpl	$-1, 8(%rdx)
	movq	%rdi, %r13
	movapd	%xmm0, %xmm1
	movq	%rdx, %r14
	je	.L1730
.L1715:
	movzbl	_ZN6icu_67L15kUndefinedFieldE(%rip), %esi
	leaq	-288(%rbp), %r15
	leaq	-136(%rbp), %rbx
	movsd	%xmm1, -344(%rbp)
	movl	$0, -324(%rbp)
	movq	%r15, %rdi
	call	_ZN6icu_6731FormattedValueStringBuilderImplC2ENS_22FormattedStringBuilder5FieldE@PLT
	leaq	16+_ZTVN6icu_676number4impl20UFormattedNumberDataE(%rip), %rax
	movq	%rbx, %rdi
	movq	%rax, -288(%rbp)
	call	_ZN6icu_676number4impl15DecimalQuantityC1Ev@PLT
	movsd	-344(%rbp), %xmm1
	movq	%rbx, %rdi
	leaq	-324(%rbp), %rbx
	movapd	%xmm1, %xmm0
	call	_ZN6icu_676number4impl15DecimalQuantity11setToDoubleEd@PLT
	movq	360(%r13), %rdi
	movq	%rbx, %rdx
	movq	%r15, %rsi
	addq	$776, %rdi
	call	_ZNK6icu_676number24LocalizedNumberFormatter10formatImplEPNS0_4impl20UFormattedNumberDataER10UErrorCode@PLT
	movzwl	8(%r12), %eax
	testw	%ax, %ax
	js	.L1716
	movswl	%ax, %r13d
	movl	%r13d, %r8d
	sarl	$5, %r8d
.L1717:
	movl	-324(%rbp), %eax
	leaq	-320(%rbp), %r13
	testl	%eax, %eax
	jle	.L1731
.L1719:
	leaq	16+_ZTVN6icu_6723UnicodeStringAppendableE(%rip), %rax
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, -320(%rbp)
	movq	%r12, -312(%rbp)
	call	_ZNK6icu_6731FormattedValueStringBuilderImpl8appendToERNS_10AppendableER10UErrorCode@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6723UnicodeStringAppendableD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_676number4impl20UFormattedNumberDataD1Ev@PLT
.L1714:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1732
	addq	$312, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1731:
	.cfi_restore_state
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	movl	%r8d, -344(%rbp)
	movq	$0, 12(%r14)
	leaq	-320(%rbp), %r13
	call	_ZNK6icu_6731FormattedValueStringBuilderImpl17nextFieldPositionERNS_13FieldPositionER10UErrorCode@PLT
	testb	%al, %al
	je	.L1719
	movl	-344(%rbp), %r8d
	testl	%r8d, %r8d
	je	.L1719
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6724FieldPositionOnlyHandlerC1ERNS_13FieldPositionE@PLT
	movl	-344(%rbp), %r8d
	movq	%r13, %rdi
	movl	%r8d, %esi
	call	_ZN6icu_6724FieldPositionOnlyHandler9shiftLastEi@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6724FieldPositionOnlyHandlerD1Ev@PLT
	jmp	.L1719
	.p2align 4,,10
	.p2align 3
.L1716:
	movl	12(%r12), %r8d
	jmp	.L1717
	.p2align 4,,10
	.p2align 3
.L1730:
	cmpb	$1, 3928(%rax)
	jne	.L1715
	ucomisd	%xmm0, %xmm0
	jp	.L1715
	movsd	%xmm0, -344(%rbp)
	call	uprv_trunc_67@PLT
	movsd	-344(%rbp), %xmm1
	ucomisd	%xmm0, %xmm1
	jp	.L1715
	jne	.L1715
	movsd	.LC0(%rip), %xmm0
	comisd	%xmm1, %xmm0
	jnb	.L1715
	comisd	.LC1(%rip), %xmm1
	ja	.L1715
	cvttsd2sil	%xmm1, %esi
	movmskpd	%xmm1, %edx
	movq	%r12, %rcx
	movq	%r13, %rdi
	andl	$1, %edx
	call	_ZNK6icu_6713DecimalFormat17doFastFormatInt32EibRNS_13UnicodeStringE
	jmp	.L1714
	.p2align 4,,10
	.p2align 3
.L1729:
	movq	%rsi, %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	jmp	.L1714
.L1732:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3935:
	.size	_ZNK6icu_6713DecimalFormat6formatEdRNS_13UnicodeStringERNS_13FieldPositionE, .-_ZNK6icu_6713DecimalFormat6formatEdRNS_13UnicodeStringERNS_13FieldPositionE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713DecimalFormat6formatElRNS_13UnicodeStringERNS_13FieldPositionE
	.type	_ZNK6icu_6713DecimalFormat6formatElRNS_13UnicodeStringERNS_13FieldPositionE, @function
_ZNK6icu_6713DecimalFormat6formatElRNS_13UnicodeStringERNS_13FieldPositionE:
.LFB3947:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$312, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	360(%rdi), %rax
	testq	%rax, %rax
	je	.L1750
	cmpl	$-1, 8(%rcx)
	movq	%rdi, %r13
	movq	%rsi, %rbx
	movq	%rcx, %r14
	jne	.L1736
	cmpb	$0, 3928(%rax)
	je	.L1736
	leaq	2147483647(%rsi), %rdx
	movl	$4294967294, %eax
	cmpq	%rax, %rdx
	jbe	.L1751
.L1736:
	movzbl	_ZN6icu_67L15kUndefinedFieldE(%rip), %esi
	leaq	-288(%rbp), %r15
	movl	$0, -324(%rbp)
	movq	%r15, %rdi
	call	_ZN6icu_6731FormattedValueStringBuilderImplC2ENS_22FormattedStringBuilder5FieldE@PLT
	leaq	16+_ZTVN6icu_676number4impl20UFormattedNumberDataE(%rip), %rax
	leaq	-136(%rbp), %rdi
	movq	%rax, -288(%rbp)
	movq	%rdi, -344(%rbp)
	call	_ZN6icu_676number4impl15DecimalQuantityC1Ev@PLT
	movq	-344(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN6icu_676number4impl15DecimalQuantity9setToLongEl@PLT
	movq	360(%r13), %rdi
	leaq	-324(%rbp), %rdx
	movq	%r15, %rsi
	movq	%rdx, -344(%rbp)
	addq	$776, %rdi
	call	_ZNK6icu_676number24LocalizedNumberFormatter10formatImplEPNS0_4impl20UFormattedNumberDataER10UErrorCode@PLT
	movzwl	8(%r12), %eax
	movq	-344(%rbp), %rdx
	testw	%ax, %ax
	js	.L1737
	movswl	%ax, %r13d
	sarl	$5, %r13d
.L1738:
	movl	-324(%rbp), %eax
	leaq	-320(%rbp), %rbx
	testl	%eax, %eax
	jle	.L1752
.L1740:
	leaq	16+_ZTVN6icu_6723UnicodeStringAppendableE(%rip), %rax
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	%r12, -312(%rbp)
	movq	%rax, -320(%rbp)
	call	_ZNK6icu_6731FormattedValueStringBuilderImpl8appendToERNS_10AppendableER10UErrorCode@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6723UnicodeStringAppendableD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_676number4impl20UFormattedNumberDataD1Ev@PLT
.L1735:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1753
	addq	$312, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1737:
	.cfi_restore_state
	movl	12(%r12), %r13d
	jmp	.L1738
	.p2align 4,,10
	.p2align 3
.L1752:
	movq	$0, 12(%r14)
	movq	%r14, %rsi
	movq	%r15, %rdi
	leaq	-320(%rbp), %rbx
	movq	%rdx, -344(%rbp)
	call	_ZNK6icu_6731FormattedValueStringBuilderImpl17nextFieldPositionERNS_13FieldPositionER10UErrorCode@PLT
	movq	-344(%rbp), %rdx
	testb	%al, %al
	je	.L1740
	testl	%r13d, %r13d
	je	.L1740
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6724FieldPositionOnlyHandlerC1ERNS_13FieldPositionE@PLT
	movl	%r13d, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6724FieldPositionOnlyHandler9shiftLastEi@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6724FieldPositionOnlyHandlerD1Ev@PLT
	movq	-344(%rbp), %rdx
	jmp	.L1740
	.p2align 4,,10
	.p2align 3
.L1751:
	movq	%rsi, %rdx
	movq	%r12, %rcx
	shrq	$63, %rdx
	call	_ZNK6icu_6713DecimalFormat17doFastFormatInt32EibRNS_13UnicodeStringE
	jmp	.L1735
	.p2align 4,,10
	.p2align 3
.L1750:
	movq	%rdx, %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	jmp	.L1735
.L1753:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3947:
	.size	_ZNK6icu_6713DecimalFormat6formatElRNS_13UnicodeStringERNS_13FieldPositionE, .-_ZNK6icu_6713DecimalFormat6formatElRNS_13UnicodeStringERNS_13FieldPositionE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713DecimalFormat6formatEiRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.type	_ZNK6icu_6713DecimalFormat6formatEiRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode, @function
_ZNK6icu_6713DecimalFormat6formatEiRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode:
.LFB3945:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	leaq	_ZNK6icu_6713DecimalFormat6formatElRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode(%rip), %rcx
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$296, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	120(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1755
	movl	(%r8), %edx
	testl	%edx, %edx
	jg	.L1756
	movq	360(%rdi), %rax
	testq	%rax, %rax
	je	.L1772
	cmpl	$-1, 8(%r15)
	jne	.L1758
	cmpb	$0, 3928(%rax)
	je	.L1758
	leaq	2147483647(%r9), %rdx
	movl	$4294967294, %eax
	cmpq	%rax, %rdx
	ja	.L1758
	movq	%r9, %rdx
	movq	%r12, %rcx
	shrq	$63, %rdx
	call	_ZNK6icu_6713DecimalFormat17doFastFormatInt32EibRNS_13UnicodeStringE
	.p2align 4,,10
	.p2align 3
.L1756:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1773
	addq	$296, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1758:
	.cfi_restore_state
	movzbl	_ZN6icu_67L15kUndefinedFieldE(%rip), %esi
	leaq	-288(%rbp), %rbx
	movq	%r9, -336(%rbp)
	movq	%rbx, %rdi
	call	_ZN6icu_6731FormattedValueStringBuilderImplC2ENS_22FormattedStringBuilder5FieldE@PLT
	leaq	16+_ZTVN6icu_676number4impl20UFormattedNumberDataE(%rip), %rax
	leaq	-136(%rbp), %rdi
	movq	%rax, -288(%rbp)
	movq	%rdi, -328(%rbp)
	call	_ZN6icu_676number4impl15DecimalQuantityC1Ev@PLT
	movq	-336(%rbp), %r9
	movq	-328(%rbp), %rdi
	movq	%r9, %rsi
	call	_ZN6icu_676number4impl15DecimalQuantity9setToLongEl@PLT
	movq	360(%r14), %rdi
	movq	%r13, %rdx
	movq	%rbx, %rsi
	addq	$776, %rdi
	call	_ZNK6icu_676number24LocalizedNumberFormatter10formatImplEPNS0_4impl20UFormattedNumberDataER10UErrorCode@PLT
	movzwl	8(%r12), %eax
	testw	%ax, %ax
	js	.L1759
	movswl	%ax, %r14d
	movl	0(%r13), %eax
	movl	%r14d, %r8d
	leaq	-320(%rbp), %r14
	sarl	$5, %r8d
	testl	%eax, %eax
	jle	.L1774
.L1762:
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	%r12, -312(%rbp)
	leaq	16+_ZTVN6icu_6723UnicodeStringAppendableE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNK6icu_6731FormattedValueStringBuilderImpl8appendToERNS_10AppendableER10UErrorCode@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6723UnicodeStringAppendableD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_676number4impl20UFormattedNumberDataD1Ev@PLT
	jmp	.L1756
	.p2align 4,,10
	.p2align 3
.L1755:
	movq	%r15, %rcx
	movq	%r9, %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L1756
	.p2align 4,,10
	.p2align 3
.L1759:
	movl	0(%r13), %eax
	movl	12(%r12), %r8d
	leaq	-320(%rbp), %r14
	testl	%eax, %eax
	jg	.L1762
.L1774:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movl	%r8d, -328(%rbp)
	movq	$0, 12(%r15)
	leaq	-320(%rbp), %r14
	call	_ZNK6icu_6731FormattedValueStringBuilderImpl17nextFieldPositionERNS_13FieldPositionER10UErrorCode@PLT
	testb	%al, %al
	je	.L1762
	movl	-328(%rbp), %r8d
	testl	%r8d, %r8d
	je	.L1762
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6724FieldPositionOnlyHandlerC1ERNS_13FieldPositionE@PLT
	movl	-328(%rbp), %r8d
	movq	%r14, %rdi
	movl	%r8d, %esi
	call	_ZN6icu_6724FieldPositionOnlyHandler9shiftLastEi@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6724FieldPositionOnlyHandlerD1Ev@PLT
	jmp	.L1762
	.p2align 4,,10
	.p2align 3
.L1772:
	movl	$7, (%r8)
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	jmp	.L1756
.L1773:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3945:
	.size	_ZNK6icu_6713DecimalFormat6formatEiRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode, .-_ZNK6icu_6713DecimalFormat6formatEiRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713DecimalFormat6formatEdRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.type	_ZNK6icu_6713DecimalFormat6formatEdRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode, @function
_ZNK6icu_6713DecimalFormat6formatEdRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode:
.LFB3942:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$296, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rcx), %esi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%esi, %esi
	jg	.L1776
	movq	360(%rdi), %rax
	movq	%rdi, %r13
	movq	%rcx, %r12
	testq	%rax, %rax
	je	.L1792
	cmpl	$-1, 8(%rdx)
	movapd	%xmm0, %xmm1
	movq	%rdx, %r15
	je	.L1793
.L1778:
	movzbl	_ZN6icu_67L15kUndefinedFieldE(%rip), %esi
	leaq	-288(%rbp), %rbx
	movsd	%xmm1, -336(%rbp)
	movq	%rbx, %rdi
	call	_ZN6icu_6731FormattedValueStringBuilderImplC2ENS_22FormattedStringBuilder5FieldE@PLT
	leaq	16+_ZTVN6icu_676number4impl20UFormattedNumberDataE(%rip), %rax
	leaq	-136(%rbp), %rdi
	movq	%rax, -288(%rbp)
	movq	%rdi, -328(%rbp)
	call	_ZN6icu_676number4impl15DecimalQuantityC1Ev@PLT
	movsd	-336(%rbp), %xmm1
	movq	-328(%rbp), %rdi
	movapd	%xmm1, %xmm0
	call	_ZN6icu_676number4impl15DecimalQuantity11setToDoubleEd@PLT
	movq	360(%r13), %rdi
	movq	%r12, %rdx
	movq	%rbx, %rsi
	addq	$776, %rdi
	call	_ZNK6icu_676number24LocalizedNumberFormatter10formatImplEPNS0_4impl20UFormattedNumberDataER10UErrorCode@PLT
	movzwl	8(%r14), %eax
	testw	%ax, %ax
	js	.L1779
	movswl	%ax, %r13d
	movl	(%r12), %eax
	movl	%r13d, %r8d
	leaq	-320(%rbp), %r13
	sarl	$5, %r8d
	testl	%eax, %eax
	jle	.L1794
.L1782:
	leaq	16+_ZTVN6icu_6723UnicodeStringAppendableE(%rip), %rax
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%rax, -320(%rbp)
	movq	%r14, -312(%rbp)
	call	_ZNK6icu_6731FormattedValueStringBuilderImpl8appendToERNS_10AppendableER10UErrorCode@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6723UnicodeStringAppendableD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_676number4impl20UFormattedNumberDataD1Ev@PLT
.L1776:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1795
	addq	$296, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1793:
	.cfi_restore_state
	cmpb	$1, 3928(%rax)
	jne	.L1778
	ucomisd	%xmm0, %xmm0
	jp	.L1778
	movsd	%xmm0, -328(%rbp)
	call	uprv_trunc_67@PLT
	movsd	-328(%rbp), %xmm1
	ucomisd	%xmm0, %xmm1
	jp	.L1778
	jne	.L1778
	movsd	.LC0(%rip), %xmm0
	comisd	%xmm1, %xmm0
	jnb	.L1778
	comisd	.LC1(%rip), %xmm1
	ja	.L1778
	cvttsd2sil	%xmm1, %esi
	movmskpd	%xmm1, %edx
	movq	%r14, %rcx
	movq	%r13, %rdi
	andl	$1, %edx
	call	_ZNK6icu_6713DecimalFormat17doFastFormatInt32EibRNS_13UnicodeStringE
	jmp	.L1776
	.p2align 4,,10
	.p2align 3
.L1779:
	movl	(%r12), %eax
	movl	12(%r14), %r8d
	leaq	-320(%rbp), %r13
	testl	%eax, %eax
	jg	.L1782
.L1794:
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movl	%r8d, -328(%rbp)
	movq	$0, 12(%r15)
	leaq	-320(%rbp), %r13
	call	_ZNK6icu_6731FormattedValueStringBuilderImpl17nextFieldPositionERNS_13FieldPositionER10UErrorCode@PLT
	testb	%al, %al
	je	.L1782
	movl	-328(%rbp), %r8d
	testl	%r8d, %r8d
	je	.L1782
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6724FieldPositionOnlyHandlerC1ERNS_13FieldPositionE@PLT
	movl	-328(%rbp), %r8d
	movq	%r13, %rdi
	movl	%r8d, %esi
	call	_ZN6icu_6724FieldPositionOnlyHandler9shiftLastEi@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6724FieldPositionOnlyHandlerD1Ev@PLT
	jmp	.L1782
	.p2align 4,,10
	.p2align 3
.L1792:
	movl	$7, (%rcx)
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	jmp	.L1776
.L1795:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3942:
	.size	_ZNK6icu_6713DecimalFormat6formatEdRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode, .-_ZNK6icu_6713DecimalFormat6formatEdRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713DecimalFormat6formatElRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.type	_ZNK6icu_6713DecimalFormat6formatElRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode, @function
_ZNK6icu_6713DecimalFormat6formatElRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode:
.LFB3948:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$296, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r8), %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L1797
	movq	360(%rdi), %rax
	movq	%rdi, %r13
	movq	%r8, %r12
	testq	%rax, %rax
	je	.L1813
	cmpl	$-1, 8(%rcx)
	movq	%rsi, %rbx
	movq	%rcx, %r9
	jne	.L1799
	cmpb	$0, 3928(%rax)
	je	.L1799
	leaq	2147483647(%rsi), %rdx
	movl	$4294967294, %eax
	cmpq	%rax, %rdx
	ja	.L1799
	movq	%rsi, %rdx
	movq	%r14, %rcx
	shrq	$63, %rdx
	call	_ZNK6icu_6713DecimalFormat17doFastFormatInt32EibRNS_13UnicodeStringE
	.p2align 4,,10
	.p2align 3
.L1797:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1814
	addq	$296, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1799:
	.cfi_restore_state
	movzbl	_ZN6icu_67L15kUndefinedFieldE(%rip), %esi
	leaq	-288(%rbp), %r15
	movq	%r9, -336(%rbp)
	movq	%r15, %rdi
	call	_ZN6icu_6731FormattedValueStringBuilderImplC2ENS_22FormattedStringBuilder5FieldE@PLT
	leaq	16+_ZTVN6icu_676number4impl20UFormattedNumberDataE(%rip), %rax
	leaq	-136(%rbp), %rdi
	movq	%rax, -288(%rbp)
	movq	%rdi, -328(%rbp)
	call	_ZN6icu_676number4impl15DecimalQuantityC1Ev@PLT
	movq	-328(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN6icu_676number4impl15DecimalQuantity9setToLongEl@PLT
	movq	360(%r13), %rdi
	movq	%r12, %rdx
	movq	%r15, %rsi
	addq	$776, %rdi
	call	_ZNK6icu_676number24LocalizedNumberFormatter10formatImplEPNS0_4impl20UFormattedNumberDataER10UErrorCode@PLT
	movzwl	8(%r14), %eax
	movq	-336(%rbp), %r9
	testw	%ax, %ax
	js	.L1800
	movswl	%ax, %r13d
	movl	(%r12), %eax
	leaq	-320(%rbp), %rbx
	sarl	$5, %r13d
	testl	%eax, %eax
	jle	.L1815
.L1803:
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	%r14, -312(%rbp)
	leaq	16+_ZTVN6icu_6723UnicodeStringAppendableE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNK6icu_6731FormattedValueStringBuilderImpl8appendToERNS_10AppendableER10UErrorCode@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6723UnicodeStringAppendableD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_676number4impl20UFormattedNumberDataD1Ev@PLT
	jmp	.L1797
	.p2align 4,,10
	.p2align 3
.L1800:
	movl	(%r12), %eax
	movl	12(%r14), %r13d
	leaq	-320(%rbp), %rbx
	testl	%eax, %eax
	jg	.L1803
.L1815:
	movq	%r12, %rdx
	movq	%r9, %rsi
	movq	%r15, %rdi
	movq	%r9, -328(%rbp)
	movq	$0, 12(%r9)
	leaq	-320(%rbp), %rbx
	call	_ZNK6icu_6731FormattedValueStringBuilderImpl17nextFieldPositionERNS_13FieldPositionER10UErrorCode@PLT
	testb	%al, %al
	je	.L1803
	testl	%r13d, %r13d
	je	.L1803
	movq	-328(%rbp), %r9
	movq	%rbx, %rdi
	movq	%r9, %rsi
	call	_ZN6icu_6724FieldPositionOnlyHandlerC1ERNS_13FieldPositionE@PLT
	movq	%rbx, %rdi
	movl	%r13d, %esi
	call	_ZN6icu_6724FieldPositionOnlyHandler9shiftLastEi@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6724FieldPositionOnlyHandlerD1Ev@PLT
	jmp	.L1803
	.p2align 4,,10
	.p2align 3
.L1813:
	movl	$7, (%r8)
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	jmp	.L1797
.L1814:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3948:
	.size	_ZNK6icu_6713DecimalFormat6formatElRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode, .-_ZNK6icu_6713DecimalFormat6formatElRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713DecimalFormat6formatENS_11StringPieceERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode
	.type	_ZNK6icu_6713DecimalFormat6formatENS_11StringPieceERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode, @function
_ZNK6icu_6713DecimalFormat6formatENS_11StringPieceERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode:
.LFB3950:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$328, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r9), %ecx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1817
	cmpq	$0, 360(%rdi)
	movq	%rdi, %r13
	movq	%r9, %r12
	je	.L1830
	movq	%rsi, -352(%rbp)
	leaq	-288(%rbp), %r15
	movzbl	_ZN6icu_67L15kUndefinedFieldE(%rip), %esi
	movq	%rdx, %rbx
	movq	%r15, %rdi
	movq	%r8, -360(%rbp)
	call	_ZN6icu_6731FormattedValueStringBuilderImplC2ENS_22FormattedStringBuilder5FieldE@PLT
	leaq	16+_ZTVN6icu_676number4impl20UFormattedNumberDataE(%rip), %rax
	leaq	-136(%rbp), %rdi
	movq	%rax, -288(%rbp)
	movq	%rdi, -344(%rbp)
	call	_ZN6icu_676number4impl15DecimalQuantityC1Ev@PLT
	movq	-352(%rbp), %r10
	movq	%r12, %rcx
	movq	%rbx, %rdx
	movq	-344(%rbp), %rdi
	movq	%r10, %rsi
	call	_ZN6icu_676number4impl15DecimalQuantity14setToDecNumberENS_11StringPieceER10UErrorCode@PLT
	movq	360(%r13), %rdi
	movq	%r12, %rdx
	movq	%r15, %rsi
	addq	$776, %rdi
	call	_ZNK6icu_676number24LocalizedNumberFormatter10formatImplEPNS0_4impl20UFormattedNumberDataER10UErrorCode@PLT
	movzwl	8(%r14), %eax
	movq	-360(%rbp), %r8
	testw	%ax, %ax
	js	.L1819
	movswl	%ax, %r13d
	sarl	$5, %r13d
.L1820:
	movl	(%r12), %eax
	leaq	-336(%rbp), %rbx
	testl	%eax, %eax
	jg	.L1821
	testq	%r8, %r8
	jne	.L1831
.L1821:
	leaq	16+_ZTVN6icu_6723UnicodeStringAppendableE(%rip), %rax
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	%rax, -336(%rbp)
	movq	%r14, -328(%rbp)
	call	_ZNK6icu_6731FormattedValueStringBuilderImpl8appendToERNS_10AppendableER10UErrorCode@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6723UnicodeStringAppendableD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_676number4impl20UFormattedNumberDataD1Ev@PLT
.L1817:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1832
	addq	$328, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1830:
	.cfi_restore_state
	movl	$7, (%r9)
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	jmp	.L1817
	.p2align 4,,10
	.p2align 3
.L1831:
	movq	%r8, %rsi
	movq	%r12, %rdx
	movq	%rbx, %rdi
	call	_ZN6icu_6728FieldPositionIteratorHandlerC1EPNS_21FieldPositionIteratorER10UErrorCode@PLT
	movl	%r13d, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6720FieldPositionHandler8setShiftEi@PLT
	movq	%r15, %rdi
	movq	%r12, %rdx
	movq	%rbx, %rsi
	call	_ZNK6icu_6731FormattedValueStringBuilderImpl20getAllFieldPositionsERNS_28FieldPositionIteratorHandlerER10UErrorCode@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6728FieldPositionIteratorHandlerD1Ev@PLT
	jmp	.L1821
	.p2align 4,,10
	.p2align 3
.L1819:
	movl	12(%r14), %r13d
	jmp	.L1820
.L1832:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3950:
	.size	_ZNK6icu_6713DecimalFormat6formatENS_11StringPieceERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode, .-_ZNK6icu_6713DecimalFormat6formatENS_11StringPieceERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713DecimalFormat27fieldPositionIteratorHelperERKNS_6number4impl20UFormattedNumberDataEPNS_21FieldPositionIteratorEiR10UErrorCode
	.type	_ZN6icu_6713DecimalFormat27fieldPositionIteratorHelperERKNS_6number4impl20UFormattedNumberDataEPNS_21FieldPositionIteratorEiR10UErrorCode, @function
_ZN6icu_6713DecimalFormat27fieldPositionIteratorHelperERKNS_6number4impl20UFormattedNumberDataEPNS_21FieldPositionIteratorEiR10UErrorCode:
.LFB4050:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$48, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L1833
	testq	%rsi, %rsi
	jne	.L1843
.L1833:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1844
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1843:
	.cfi_restore_state
	leaq	-80(%rbp), %r15
	movq	%rdi, %r13
	movl	%edx, %r14d
	movq	%rcx, %rdx
	movq	%r15, %rdi
	movq	%rcx, %r12
	call	_ZN6icu_6728FieldPositionIteratorHandlerC1EPNS_21FieldPositionIteratorER10UErrorCode@PLT
	movl	%r14d, %esi
	movq	%r15, %rdi
	call	_ZN6icu_6720FieldPositionHandler8setShiftEi@PLT
	movq	%r13, %rdi
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	_ZNK6icu_6731FormattedValueStringBuilderImpl20getAllFieldPositionsERNS_28FieldPositionIteratorHandlerER10UErrorCode@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6728FieldPositionIteratorHandlerD1Ev@PLT
	jmp	.L1833
.L1844:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4050:
	.size	_ZN6icu_6713DecimalFormat27fieldPositionIteratorHelperERKNS_6number4impl20UFormattedNumberDataEPNS_21FieldPositionIteratorEiR10UErrorCode, .-_ZN6icu_6713DecimalFormat27fieldPositionIteratorHelperERKNS_6number4impl20UFormattedNumberDataEPNS_21FieldPositionIteratorEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713DecimalFormat6formatERKNS_6number4impl15DecimalQuantityERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode
	.type	_ZNK6icu_6713DecimalFormat6formatERKNS_6number4impl15DecimalQuantityERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode, @function
_ZNK6icu_6713DecimalFormat6formatERKNS_6number4impl15DecimalQuantityERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode:
.LFB3951:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$312, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r8), %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L1846
	cmpq	$0, 360(%rdi)
	movq	%rdi, %r13
	movq	%r8, %r12
	je	.L1859
	movq	%rsi, %r15
	leaq	-288(%rbp), %rbx
	movzbl	_ZN6icu_67L15kUndefinedFieldE(%rip), %esi
	movq	%rcx, -352(%rbp)
	movq	%rbx, %rdi
	call	_ZN6icu_6731FormattedValueStringBuilderImplC2ENS_22FormattedStringBuilder5FieldE@PLT
	leaq	16+_ZTVN6icu_676number4impl20UFormattedNumberDataE(%rip), %rax
	leaq	-136(%rbp), %rdi
	movq	%rax, -288(%rbp)
	movq	%rdi, -344(%rbp)
	call	_ZN6icu_676number4impl15DecimalQuantityC1Ev@PLT
	movq	-344(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN6icu_676number4impl15DecimalQuantityaSERKS2_@PLT
	movq	360(%r13), %rdi
	movq	%r12, %rdx
	movq	%rbx, %rsi
	addq	$776, %rdi
	call	_ZNK6icu_676number24LocalizedNumberFormatter10formatImplEPNS0_4impl20UFormattedNumberDataER10UErrorCode@PLT
	movzwl	8(%r14), %eax
	movq	-352(%rbp), %r9
	testw	%ax, %ax
	js	.L1848
	movswl	%ax, %r13d
	sarl	$5, %r13d
.L1849:
	movl	(%r12), %eax
	leaq	-336(%rbp), %r15
	testl	%eax, %eax
	jg	.L1850
	testq	%r9, %r9
	jne	.L1860
.L1850:
	leaq	16+_ZTVN6icu_6723UnicodeStringAppendableE(%rip), %rax
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%rax, -336(%rbp)
	movq	%r14, -328(%rbp)
	call	_ZNK6icu_6731FormattedValueStringBuilderImpl8appendToERNS_10AppendableER10UErrorCode@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6723UnicodeStringAppendableD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_676number4impl20UFormattedNumberDataD1Ev@PLT
.L1846:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1861
	addq	$312, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1859:
	.cfi_restore_state
	movl	$7, (%r8)
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	jmp	.L1846
	.p2align 4,,10
	.p2align 3
.L1860:
	movq	%r9, %rsi
	movq	%r12, %rdx
	movq	%r15, %rdi
	call	_ZN6icu_6728FieldPositionIteratorHandlerC1EPNS_21FieldPositionIteratorER10UErrorCode@PLT
	movl	%r13d, %esi
	movq	%r15, %rdi
	call	_ZN6icu_6720FieldPositionHandler8setShiftEi@PLT
	movq	%rbx, %rdi
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	_ZNK6icu_6731FormattedValueStringBuilderImpl20getAllFieldPositionsERNS_28FieldPositionIteratorHandlerER10UErrorCode@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6728FieldPositionIteratorHandlerD1Ev@PLT
	jmp	.L1850
	.p2align 4,,10
	.p2align 3
.L1848:
	movl	12(%r14), %r13d
	jmp	.L1849
.L1861:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3951:
	.size	_ZNK6icu_6713DecimalFormat6formatERKNS_6number4impl15DecimalQuantityERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode, .-_ZNK6icu_6713DecimalFormat6formatERKNS_6number4impl15DecimalQuantityERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713DecimalFormat6formatEdRNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode
	.type	_ZNK6icu_6713DecimalFormat6formatEdRNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode, @function
_ZNK6icu_6713DecimalFormat6formatEdRNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode:
.LFB3943:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$312, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rcx), %esi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%esi, %esi
	jg	.L1863
	movq	360(%rdi), %rax
	movq	%rdi, %r13
	movq	%rcx, %r12
	testq	%rax, %rax
	je	.L1877
	movapd	%xmm0, %xmm1
	movq	%rdx, %r15
	testq	%rdx, %rdx
	je	.L1878
.L1865:
	movzbl	_ZN6icu_67L15kUndefinedFieldE(%rip), %esi
	leaq	-288(%rbp), %rbx
	movsd	%xmm1, -352(%rbp)
	movq	%rbx, %rdi
	call	_ZN6icu_6731FormattedValueStringBuilderImplC2ENS_22FormattedStringBuilder5FieldE@PLT
	leaq	16+_ZTVN6icu_676number4impl20UFormattedNumberDataE(%rip), %rax
	leaq	-136(%rbp), %rdi
	movq	%rax, -288(%rbp)
	movq	%rdi, -344(%rbp)
	call	_ZN6icu_676number4impl15DecimalQuantityC1Ev@PLT
	movsd	-352(%rbp), %xmm1
	movq	-344(%rbp), %rdi
	movapd	%xmm1, %xmm0
	call	_ZN6icu_676number4impl15DecimalQuantity11setToDoubleEd@PLT
	movq	360(%r13), %rdi
	movq	%r12, %rdx
	movq	%rbx, %rsi
	addq	$776, %rdi
	call	_ZNK6icu_676number24LocalizedNumberFormatter10formatImplEPNS0_4impl20UFormattedNumberDataER10UErrorCode@PLT
	movzwl	8(%r14), %eax
	testw	%ax, %ax
	js	.L1866
	movswl	%ax, %r13d
	movl	%r13d, %r8d
	sarl	$5, %r8d
.L1867:
	movl	(%r12), %eax
	leaq	-336(%rbp), %r13
	testl	%eax, %eax
	jg	.L1868
	testq	%r15, %r15
	jne	.L1879
.L1868:
	leaq	16+_ZTVN6icu_6723UnicodeStringAppendableE(%rip), %rax
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%rax, -336(%rbp)
	movq	%r14, -328(%rbp)
	call	_ZNK6icu_6731FormattedValueStringBuilderImpl8appendToERNS_10AppendableER10UErrorCode@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6723UnicodeStringAppendableD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_676number4impl20UFormattedNumberDataD1Ev@PLT
.L1863:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1880
	addq	$312, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1878:
	.cfi_restore_state
	cmpb	$1, 3928(%rax)
	jne	.L1865
	ucomisd	%xmm0, %xmm0
	jp	.L1865
	movsd	%xmm0, -344(%rbp)
	call	uprv_trunc_67@PLT
	movsd	-344(%rbp), %xmm1
	ucomisd	%xmm0, %xmm1
	jp	.L1865
	jne	.L1865
	movsd	.LC0(%rip), %xmm0
	comisd	%xmm1, %xmm0
	jnb	.L1865
	comisd	.LC1(%rip), %xmm1
	ja	.L1865
	cvttsd2sil	%xmm1, %esi
	movmskpd	%xmm1, %edx
	movq	%r14, %rcx
	movq	%r13, %rdi
	andl	$1, %edx
	call	_ZNK6icu_6713DecimalFormat17doFastFormatInt32EibRNS_13UnicodeStringE
	jmp	.L1863
	.p2align 4,,10
	.p2align 3
.L1879:
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	movl	%r8d, -344(%rbp)
	call	_ZN6icu_6728FieldPositionIteratorHandlerC1EPNS_21FieldPositionIteratorER10UErrorCode@PLT
	movl	-344(%rbp), %r8d
	movq	%r13, %rdi
	movl	%r8d, %esi
	call	_ZN6icu_6720FieldPositionHandler8setShiftEi@PLT
	movq	%rbx, %rdi
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	_ZNK6icu_6731FormattedValueStringBuilderImpl20getAllFieldPositionsERNS_28FieldPositionIteratorHandlerER10UErrorCode@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6728FieldPositionIteratorHandlerD1Ev@PLT
	jmp	.L1868
	.p2align 4,,10
	.p2align 3
.L1866:
	movl	12(%r14), %r8d
	jmp	.L1867
	.p2align 4,,10
	.p2align 3
.L1877:
	movl	$7, (%rcx)
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	jmp	.L1863
.L1880:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3943:
	.size	_ZNK6icu_6713DecimalFormat6formatEdRNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode, .-_ZNK6icu_6713DecimalFormat6formatEdRNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713DecimalFormat6formatElRNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode
	.type	_ZNK6icu_6713DecimalFormat6formatElRNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode, @function
_ZNK6icu_6713DecimalFormat6formatElRNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode:
.LFB3949:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$312, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r8), %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L1882
	movq	360(%rdi), %rax
	movq	%rdi, %r13
	movq	%r8, %r12
	testq	%rax, %rax
	je	.L1896
	movq	%rsi, %rbx
	movq	%rcx, %r9
	testq	%rcx, %rcx
	je	.L1897
.L1884:
	movzbl	_ZN6icu_67L15kUndefinedFieldE(%rip), %esi
	leaq	-288(%rbp), %r15
	movq	%r9, -352(%rbp)
	movq	%r15, %rdi
	call	_ZN6icu_6731FormattedValueStringBuilderImplC2ENS_22FormattedStringBuilder5FieldE@PLT
	leaq	16+_ZTVN6icu_676number4impl20UFormattedNumberDataE(%rip), %rax
	leaq	-136(%rbp), %rdi
	movq	%rax, -288(%rbp)
	movq	%rdi, -344(%rbp)
	call	_ZN6icu_676number4impl15DecimalQuantityC1Ev@PLT
	movq	-344(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN6icu_676number4impl15DecimalQuantity9setToLongEl@PLT
	movq	360(%r13), %rdi
	movq	%r12, %rdx
	movq	%r15, %rsi
	addq	$776, %rdi
	call	_ZNK6icu_676number24LocalizedNumberFormatter10formatImplEPNS0_4impl20UFormattedNumberDataER10UErrorCode@PLT
	movzwl	8(%r14), %eax
	movq	-352(%rbp), %r9
	testw	%ax, %ax
	js	.L1885
	movswl	%ax, %r13d
	sarl	$5, %r13d
.L1886:
	movl	(%r12), %eax
	leaq	-336(%rbp), %rbx
	testl	%eax, %eax
	jg	.L1887
	testq	%r9, %r9
	jne	.L1898
.L1887:
	leaq	16+_ZTVN6icu_6723UnicodeStringAppendableE(%rip), %rax
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	%rax, -336(%rbp)
	movq	%r14, -328(%rbp)
	call	_ZNK6icu_6731FormattedValueStringBuilderImpl8appendToERNS_10AppendableER10UErrorCode@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6723UnicodeStringAppendableD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_676number4impl20UFormattedNumberDataD1Ev@PLT
.L1882:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1899
	addq	$312, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1897:
	.cfi_restore_state
	cmpb	$0, 3928(%rax)
	je	.L1884
	leaq	2147483647(%rsi), %rdx
	movl	$4294967294, %eax
	cmpq	%rax, %rdx
	ja	.L1884
	movq	%rsi, %rdx
	movq	%r14, %rcx
	shrq	$63, %rdx
	call	_ZNK6icu_6713DecimalFormat17doFastFormatInt32EibRNS_13UnicodeStringE
	jmp	.L1882
	.p2align 4,,10
	.p2align 3
.L1885:
	movl	12(%r14), %r13d
	jmp	.L1886
	.p2align 4,,10
	.p2align 3
.L1898:
	movq	%r9, %rsi
	movq	%r12, %rdx
	movq	%rbx, %rdi
	call	_ZN6icu_6728FieldPositionIteratorHandlerC1EPNS_21FieldPositionIteratorER10UErrorCode@PLT
	movl	%r13d, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6720FieldPositionHandler8setShiftEi@PLT
	movq	%r15, %rdi
	movq	%r12, %rdx
	movq	%rbx, %rsi
	call	_ZNK6icu_6731FormattedValueStringBuilderImpl20getAllFieldPositionsERNS_28FieldPositionIteratorHandlerER10UErrorCode@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6728FieldPositionIteratorHandlerD1Ev@PLT
	jmp	.L1887
	.p2align 4,,10
	.p2align 3
.L1896:
	movl	$7, (%r8)
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	jmp	.L1882
.L1899:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3949:
	.size	_ZNK6icu_6713DecimalFormat6formatElRNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode, .-_ZNK6icu_6713DecimalFormat6formatElRNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713DecimalFormat11setCurrencyEPKDsR10UErrorCode
	.type	_ZN6icu_6713DecimalFormat11setCurrencyEPKDsR10UErrorCode, @function
_ZN6icu_6713DecimalFormat11setCurrencyEPKDsR10UErrorCode:
.LFB4030:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rdx), %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L1900
	cmpq	$0, 360(%rdi)
	movq	%rdi, %r14
	movq	%rdx, %r12
	je	.L1923
	leaq	-128(%rbp), %r15
	movq	%rsi, -136(%rbp)
	movq	%rsi, %r13
	leaq	-136(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6712CurrencyUnitC1ENS_14ConstChar16PtrER10UErrorCode@PLT
	movl	(%r12), %esi
	testl	%esi, %esi
	jg	.L1903
	movq	360(%r14), %rsi
	cmpb	$0, 16(%rsi)
	je	.L1924
.L1905:
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6712NumberFormat11setCurrencyEPKDsR10UErrorCode@PLT
	movq	360(%r14), %r13
	movq	%r15, %rsi
	leaq	24(%r13), %rdi
	call	_ZN6icu_6712CurrencyUnitaSERKS0_@PLT
	movb	$0, 16(%r13)
	movl	$2816, %edi
	movq	360(%r14), %rax
	movq	768(%rax), %rbx
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1907
	movq	%rax, %rdi
	movq	%rbx, %rsi
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKS0_@PLT
	leaq	-108(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN6icu_6720DecimalFormatSymbols11setCurrencyEPKDsR10UErrorCode@PLT
	movl	(%r12), %ecx
	testl	%ecx, %ecx
	jle	.L1925
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*8(%rax)
.L1911:
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6713DecimalFormat5touchER10UErrorCode
	movq	%r15, %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	.p2align 4,,10
	.p2align 3
.L1900:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1926
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1924:
	.cfi_restore_state
	leaq	-96(%rbp), %rbx
	addq	$24, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6712CurrencyUnitC1ERKS0_@PLT
	movq	%rbx, %rdi
	movq	%r15, %rsi
	call	_ZNK6icu_6711MeasureUniteqERKNS_7UObjectE@PLT
	movq	%rbx, %rdi
	testb	%al, %al
	je	.L1906
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	.p2align 4,,10
	.p2align 3
.L1903:
	movq	%r15, %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	jmp	.L1900
	.p2align 4,,10
	.p2align 3
.L1923:
	movl	$7, (%rdx)
	jmp	.L1900
	.p2align 4,,10
	.p2align 3
.L1906:
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	jmp	.L1905
	.p2align 4,,10
	.p2align 3
.L1925:
	movq	360(%r14), %rbx
	movq	768(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1922
	movq	(%rdi), %rax
	call	*8(%rax)
.L1922:
	movq	%r13, 768(%rbx)
	jmp	.L1911
.L1907:
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L1910
	movl	$7, (%r12)
.L1910:
	movq	%r12, %rdx
	leaq	-108(%rbp), %rsi
	xorl	%edi, %edi
	call	_ZN6icu_6720DecimalFormatSymbols11setCurrencyEPKDsR10UErrorCode@PLT
	movl	(%r12), %edx
	testl	%edx, %edx
	jg	.L1911
	movq	360(%r14), %rbx
	movq	768(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1917
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	$0, 768(%rbx)
.L1917:
	movl	$7, (%r12)
	jmp	.L1911
.L1926:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4030:
	.size	_ZN6icu_6713DecimalFormat11setCurrencyEPKDsR10UErrorCode, .-_ZN6icu_6713DecimalFormat11setCurrencyEPKDsR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713DecimalFormat11setCurrencyEPKDs
	.type	_ZN6icu_6713DecimalFormat11setCurrencyEPKDs, @function
_ZN6icu_6713DecimalFormat11setCurrencyEPKDs:
.LFB4031:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN6icu_6713DecimalFormat11setCurrencyEPKDsR10UErrorCode(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_679ErrorCodeE(%rip), %rax
	movl	$0, -136(%rbp)
	movq	%rax, -144(%rbp)
	movq	(%rdi), %rax
	movq	248(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1928
	cmpq	$0, 360(%rdi)
	je	.L1952
	leaq	-136(%rbp), %r15
	leaq	-128(%rbp), %r14
	movq	%rsi, -152(%rbp)
	movq	%r15, %rdx
	leaq	-152(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6712CurrencyUnitC1ENS_14ConstChar16PtrER10UErrorCode@PLT
	movl	-136(%rbp), %esi
	testl	%esi, %esi
	jg	.L1931
	movq	360(%r12), %rsi
	cmpb	$0, 16(%rsi)
	je	.L1953
.L1933:
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6712NumberFormat11setCurrencyEPKDsR10UErrorCode@PLT
	movq	360(%r12), %r13
	movq	%r14, %rsi
	leaq	24(%r13), %rdi
	call	_ZN6icu_6712CurrencyUnitaSERKS0_@PLT
	movb	$0, 16(%r13)
	movl	$2816, %edi
	movq	360(%r12), %rax
	movq	768(%rax), %rbx
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1935
	movq	%rax, %rdi
	movq	%rbx, %rsi
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKS0_@PLT
	leaq	-108(%rbp), %rsi
	movq	%r15, %rdx
	movq	%r13, %rdi
	call	_ZN6icu_6720DecimalFormatSymbols11setCurrencyEPKDsR10UErrorCode@PLT
	movl	-136(%rbp), %ecx
	testl	%ecx, %ecx
	jle	.L1954
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*8(%rax)
.L1939:
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN6icu_6713DecimalFormat5touchER10UErrorCode
	movq	%r14, %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	jmp	.L1940
	.p2align 4,,10
	.p2align 3
.L1953:
	leaq	-96(%rbp), %rbx
	addq	$24, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6712CurrencyUnitC1ERKS0_@PLT
	movq	%rbx, %rdi
	movq	%r14, %rsi
	call	_ZNK6icu_6711MeasureUniteqERKNS_7UObjectE@PLT
	movq	%rbx, %rdi
	testb	%al, %al
	je	.L1955
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	.p2align 4,,10
	.p2align 3
.L1931:
	movq	%r14, %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
.L1940:
	leaq	-144(%rbp), %rdi
	call	_ZN6icu_679ErrorCodeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1956
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1928:
	.cfi_restore_state
	leaq	-136(%rbp), %rdx
	call	*%rax
	jmp	.L1940
	.p2align 4,,10
	.p2align 3
.L1952:
	movl	$7, -136(%rbp)
	jmp	.L1940
	.p2align 4,,10
	.p2align 3
.L1955:
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	jmp	.L1933
	.p2align 4,,10
	.p2align 3
.L1954:
	movq	360(%r12), %rbx
	movq	768(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1951
	movq	(%rdi), %rax
	call	*8(%rax)
.L1951:
	movq	%r13, 768(%rbx)
	jmp	.L1939
.L1935:
	movl	-136(%rbp), %eax
	testl	%eax, %eax
	jg	.L1938
	movl	$7, -136(%rbp)
.L1938:
	movq	%r15, %rdx
	leaq	-108(%rbp), %rsi
	xorl	%edi, %edi
	call	_ZN6icu_6720DecimalFormatSymbols11setCurrencyEPKDsR10UErrorCode@PLT
	movl	-136(%rbp), %edx
	testl	%edx, %edx
	jg	.L1939
	movq	360(%r12), %rbx
	movq	768(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1945
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	$0, 768(%rbx)
.L1945:
	movl	$7, -136(%rbp)
	jmp	.L1939
.L1956:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4031:
	.size	_ZN6icu_6713DecimalFormat11setCurrencyEPKDs, .-_ZN6icu_6713DecimalFormat11setCurrencyEPKDs
	.weak	_ZTSN6icu_6713DecimalFormatE
	.section	.rodata._ZTSN6icu_6713DecimalFormatE,"aG",@progbits,_ZTSN6icu_6713DecimalFormatE,comdat
	.align 16
	.type	_ZTSN6icu_6713DecimalFormatE, @object
	.size	_ZTSN6icu_6713DecimalFormatE, 25
_ZTSN6icu_6713DecimalFormatE:
	.string	"N6icu_6713DecimalFormatE"
	.weak	_ZTIN6icu_6713DecimalFormatE
	.section	.data.rel.ro._ZTIN6icu_6713DecimalFormatE,"awG",@progbits,_ZTIN6icu_6713DecimalFormatE,comdat
	.align 8
	.type	_ZTIN6icu_6713DecimalFormatE, @object
	.size	_ZTIN6icu_6713DecimalFormatE, 24
_ZTIN6icu_6713DecimalFormatE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6713DecimalFormatE
	.quad	_ZTIN6icu_6712NumberFormatE
	.weak	_ZTVN6icu_6713DecimalFormatE
	.section	.data.rel.ro._ZTVN6icu_6713DecimalFormatE,"awG",@progbits,_ZTVN6icu_6713DecimalFormatE,comdat
	.align 8
	.type	_ZTVN6icu_6713DecimalFormatE, @object
	.size	_ZTVN6icu_6713DecimalFormatE, 616
_ZTVN6icu_6713DecimalFormatE:
	.quad	0
	.quad	_ZTIN6icu_6713DecimalFormatE
	.quad	_ZN6icu_6713DecimalFormatD1Ev
	.quad	_ZN6icu_6713DecimalFormatD0Ev
	.quad	_ZNK6icu_6713DecimalFormat17getDynamicClassIDEv
	.quad	_ZNK6icu_6713DecimalFormateqERKNS_6FormatE
	.quad	_ZNK6icu_6713DecimalFormat5cloneEv
	.quad	_ZNK6icu_6712NumberFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.quad	_ZNK6icu_6712NumberFormat6formatERKNS_11FormattableERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode
	.quad	_ZNK6icu_6712NumberFormat11parseObjectERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE
	.quad	_ZNK6icu_6713DecimalFormat6formatEdRNS_13UnicodeStringERNS_13FieldPositionE
	.quad	_ZNK6icu_6713DecimalFormat6formatEdRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.quad	_ZNK6icu_6713DecimalFormat6formatEdRNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode
	.quad	_ZNK6icu_6713DecimalFormat6formatEiRNS_13UnicodeStringERNS_13FieldPositionE
	.quad	_ZNK6icu_6713DecimalFormat6formatEiRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.quad	_ZNK6icu_6713DecimalFormat6formatEiRNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode
	.quad	_ZNK6icu_6713DecimalFormat6formatElRNS_13UnicodeStringERNS_13FieldPositionE
	.quad	_ZNK6icu_6713DecimalFormat6formatElRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.quad	_ZNK6icu_6713DecimalFormat6formatElRNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode
	.quad	_ZNK6icu_6713DecimalFormat6formatENS_11StringPieceERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode
	.quad	_ZNK6icu_6713DecimalFormat6formatERKNS_6number4impl15DecimalQuantityERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode
	.quad	_ZNK6icu_6713DecimalFormat6formatERKNS_6number4impl15DecimalQuantityERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.quad	_ZNK6icu_6713DecimalFormat5parseERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE
	.quad	_ZNK6icu_6712NumberFormat5parseERKNS_13UnicodeStringERNS_11FormattableER10UErrorCode
	.quad	_ZNK6icu_6713DecimalFormat13parseCurrencyERKNS_13UnicodeStringERNS_13ParsePositionE
	.quad	_ZN6icu_6713DecimalFormat19setParseIntegerOnlyEa
	.quad	_ZN6icu_6713DecimalFormat10setLenientEa
	.quad	_ZNK6icu_6712NumberFormat9isLenientEv
	.quad	_ZN6icu_6713DecimalFormat15setGroupingUsedEa
	.quad	_ZN6icu_6713DecimalFormat23setMaximumIntegerDigitsEi
	.quad	_ZN6icu_6713DecimalFormat23setMinimumIntegerDigitsEi
	.quad	_ZN6icu_6713DecimalFormat24setMaximumFractionDigitsEi
	.quad	_ZN6icu_6713DecimalFormat24setMinimumFractionDigitsEi
	.quad	_ZN6icu_6713DecimalFormat11setCurrencyEPKDsR10UErrorCode
	.quad	_ZN6icu_6712NumberFormat10setContextE15UDisplayContextR10UErrorCode
	.quad	_ZNK6icu_6712NumberFormat10getContextE19UDisplayContextTypeR10UErrorCode
	.quad	_ZNK6icu_6713DecimalFormat15getRoundingModeEv
	.quad	_ZN6icu_6713DecimalFormat15setRoundingModeENS_12NumberFormat13ERoundingModeE
	.quad	_ZNK6icu_6712NumberFormat20getEffectiveCurrencyEPDsR10UErrorCode
	.quad	_ZN6icu_6713DecimalFormat12setAttributeE22UNumberFormatAttributeiR10UErrorCode
	.quad	_ZNK6icu_6713DecimalFormat12getAttributeE22UNumberFormatAttributeR10UErrorCode
	.quad	_ZNK6icu_6713DecimalFormat23getDecimalFormatSymbolsEv
	.quad	_ZN6icu_6713DecimalFormat25adoptDecimalFormatSymbolsEPNS_20DecimalFormatSymbolsE
	.quad	_ZN6icu_6713DecimalFormat23setDecimalFormatSymbolsERKNS_20DecimalFormatSymbolsE
	.quad	_ZNK6icu_6713DecimalFormat21getCurrencyPluralInfoEv
	.quad	_ZN6icu_6713DecimalFormat23adoptCurrencyPluralInfoEPNS_18CurrencyPluralInfoE
	.quad	_ZN6icu_6713DecimalFormat21setCurrencyPluralInfoERKNS_18CurrencyPluralInfoE
	.quad	_ZN6icu_6713DecimalFormat17setPositivePrefixERKNS_13UnicodeStringE
	.quad	_ZN6icu_6713DecimalFormat17setNegativePrefixERKNS_13UnicodeStringE
	.quad	_ZN6icu_6713DecimalFormat17setPositiveSuffixERKNS_13UnicodeStringE
	.quad	_ZN6icu_6713DecimalFormat17setNegativeSuffixERKNS_13UnicodeStringE
	.quad	_ZN6icu_6713DecimalFormat13setMultiplierEi
	.quad	_ZNK6icu_6713DecimalFormat20getRoundingIncrementEv
	.quad	_ZN6icu_6713DecimalFormat20setRoundingIncrementEd
	.quad	_ZNK6icu_6713DecimalFormat14getFormatWidthEv
	.quad	_ZN6icu_6713DecimalFormat14setFormatWidthEi
	.quad	_ZNK6icu_6713DecimalFormat21getPadCharacterStringEv
	.quad	_ZN6icu_6713DecimalFormat15setPadCharacterERKNS_13UnicodeStringE
	.quad	_ZNK6icu_6713DecimalFormat14getPadPositionEv
	.quad	_ZN6icu_6713DecimalFormat14setPadPositionENS0_12EPadPositionE
	.quad	_ZNK6icu_6713DecimalFormat20isScientificNotationEv
	.quad	_ZN6icu_6713DecimalFormat21setScientificNotationEa
	.quad	_ZNK6icu_6713DecimalFormat24getMinimumExponentDigitsEv
	.quad	_ZN6icu_6713DecimalFormat24setMinimumExponentDigitsEa
	.quad	_ZNK6icu_6713DecimalFormat25isExponentSignAlwaysShownEv
	.quad	_ZN6icu_6713DecimalFormat26setExponentSignAlwaysShownEa
	.quad	_ZN6icu_6713DecimalFormat15setGroupingSizeEi
	.quad	_ZN6icu_6713DecimalFormat24setSecondaryGroupingSizeEi
	.quad	_ZN6icu_6713DecimalFormat30setDecimalSeparatorAlwaysShownEa
	.quad	_ZN6icu_6713DecimalFormat30setDecimalPatternMatchRequiredEa
	.quad	_ZNK6icu_6713DecimalFormat9toPatternERNS_13UnicodeStringE
	.quad	_ZNK6icu_6713DecimalFormat18toLocalizedPatternERNS_13UnicodeStringE
	.quad	_ZN6icu_6713DecimalFormat12applyPatternERKNS_13UnicodeStringER11UParseErrorR10UErrorCode
	.quad	_ZN6icu_6713DecimalFormat12applyPatternERKNS_13UnicodeStringER10UErrorCode
	.quad	_ZN6icu_6713DecimalFormat21applyLocalizedPatternERKNS_13UnicodeStringER11UParseErrorR10UErrorCode
	.quad	_ZN6icu_6713DecimalFormat21applyLocalizedPatternERKNS_13UnicodeStringER10UErrorCode
	.quad	_ZN6icu_6713DecimalFormat11setCurrencyEPKDs
	.local	_ZZN6icu_6713DecimalFormat16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6713DecimalFormat16getStaticClassIDEvE7classID,1,1
	.section	.rodata
	.align 2
	.type	_ZN6icu_676number4implL22kFallbackPaddingStringE, @object
	.size	_ZN6icu_676number4implL22kFallbackPaddingStringE, 4
_ZN6icu_676number4implL22kFallbackPaddingStringE:
	.string	" "
	.string	""
	.string	""
	.type	_ZN6icu_67L15kUndefinedFieldE, @object
	.size	_ZN6icu_67L15kUndefinedFieldE, 1
_ZN6icu_67L15kUndefinedFieldE:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	-1042284544
	.align 8
.LC1:
	.long	4290772992
	.long	1105199103
	.section	.data.rel.ro,"aw"
	.align 8
.LC2:
	.quad	_ZTVN6icu_676number4impl30PropertiesAffixPatternProviderE+16
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
