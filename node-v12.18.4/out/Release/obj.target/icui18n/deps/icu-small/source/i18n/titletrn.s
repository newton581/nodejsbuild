	.file	"titletrn.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6723TitlecaseTransliterator17getDynamicClassIDEv
	.type	_ZNK6icu_6723TitlecaseTransliterator17getDynamicClassIDEv, @function
_ZNK6icu_6723TitlecaseTransliterator17getDynamicClassIDEv:
.LFB3057:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6723TitlecaseTransliterator16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3057:
	.size	_ZNK6icu_6723TitlecaseTransliterator17getDynamicClassIDEv, .-_ZNK6icu_6723TitlecaseTransliterator17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6723TitlecaseTransliteratorD2Ev
	.type	_ZN6icu_6723TitlecaseTransliteratorD2Ev, @function
_ZN6icu_6723TitlecaseTransliteratorD2Ev:
.LFB3062:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6723TitlecaseTransliteratorE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6721CaseMapTransliteratorD2Ev@PLT
	.cfi_endproc
.LFE3062:
	.size	_ZN6icu_6723TitlecaseTransliteratorD2Ev, .-_ZN6icu_6723TitlecaseTransliteratorD2Ev
	.globl	_ZN6icu_6723TitlecaseTransliteratorD1Ev
	.set	_ZN6icu_6723TitlecaseTransliteratorD1Ev,_ZN6icu_6723TitlecaseTransliteratorD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6723TitlecaseTransliteratorD0Ev
	.type	_ZN6icu_6723TitlecaseTransliteratorD0Ev, @function
_ZN6icu_6723TitlecaseTransliteratorD0Ev:
.LFB3064:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6723TitlecaseTransliteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6721CaseMapTransliteratorD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3064:
	.size	_ZN6icu_6723TitlecaseTransliteratorD0Ev, .-_ZN6icu_6723TitlecaseTransliteratorD0Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6723TitlecaseTransliterator5cloneEv
	.type	_ZNK6icu_6723TitlecaseTransliterator5cloneEv, @function
_ZNK6icu_6723TitlecaseTransliterator5cloneEv:
.LFB3068:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movl	$96, %edi
	pushq	%r12
	.cfi_offset 12, -32
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L6
	movq	%rax, %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6721CaseMapTransliteratorC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6723TitlecaseTransliteratorE(%rip), %rax
	movq	%rax, (%r12)
.L6:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3068:
	.size	_ZNK6icu_6723TitlecaseTransliterator5cloneEv, .-_ZNK6icu_6723TitlecaseTransliterator5cloneEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6723TitlecaseTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositiona
	.type	_ZNK6icu_6723TitlecaseTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositiona, @function
_ZNK6icu_6723TitlecaseTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositiona:
.LFB3069:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movb	%cl, -182(%rbp)
	movl	8(%rdx), %r9d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	12(%rdx), %r9d
	jge	.L12
	movq	%rsi, %r14
	movq	%rdx, %r12
	leal	-1(%r9), %r13d
	cmpl	(%rdx), %r13d
	jl	.L16
	.p2align 4,,10
	.p2align 3
.L14:
	movq	(%r14), %rax
	movq	%r14, %rdi
	movl	%r13d, %esi
	call	*80(%rax)
	movl	%eax, %edi
	movl	%eax, %ebx
	call	ucase_getTypeOrIgnorable_67@PLT
	testl	%eax, %eax
	jg	.L37
	je	.L54
	cmpl	$65535, %ebx
	ja	.L17
	subl	$1, %r13d
	cmpl	%r13d, (%r12)
	jle	.L14
.L54:
	movl	8(%r12), %r9d
.L16:
	movb	$1, -181(%rbp)
.L15:
	movl	(%r12), %eax
	pxor	%xmm0, %xmm0
	cmpl	12(%r12), %r9d
	movl	%r9d, %r15d
	movups	%xmm0, -152(%rbp)
	movl	%eax, -152(%rbp)
	movl	4(%r12), %eax
	movq	$0, -136(%rbp)
	movl	%eax, -144(%rbp)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, -128(%rbp)
	movl	$2, %eax
	movw	%ax, -120(%rbp)
	leaq	-128(%rbp), %rax
	movq	%r14, -160(%rbp)
	movq	%rax, -192(%rbp)
	jge	.L19
	leaq	-168(%rbp), %rax
	movq	%rax, -200(%rbp)
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L56:
	cmpb	$0, -181(%rbp)
	movq	utrans_rep_caseContextIterator_67@GOTPCREL(%rip), %rsi
	leaq	-176(%rbp), %rcx
	leaq	-160(%rbp), %rdx
	movl	-180(%rbp), %edi
	movl	$1, %r8d
	je	.L23
	call	ucase_toFullTitle_67@PLT
	movl	%eax, %ecx
.L24:
	testl	%r13d, %r13d
	sete	-181(%rbp)
	cmpb	$0, -131(%rbp)
	je	.L25
	cmpb	$0, -182(%rbp)
	jne	.L55
.L25:
	testl	%ecx, %ecx
	js	.L22
	cmpl	$31, %ecx
	jg	.L28
	movq	-176(%rbp), %rax
	movq	-200(%rbp), %rdx
	xorl	%esi, %esi
	movl	%ecx, -180(%rbp)
	movq	-192(%rbp), %rdi
	movq	%rax, -168(%rbp)
	call	_ZN6icu_6713UnicodeString5setToEaNS_14ConstChar16PtrEi@PLT
	movl	-180(%rbp), %ecx
	subl	%ebx, %ecx
	movl	%ecx, %ebx
.L29:
	movq	(%r14), %rax
	movq	-192(%rbp), %rcx
	movl	%r15d, %edx
	movq	%r14, %rdi
	movl	-140(%rbp), %esi
	call	*32(%rax)
	testl	%ebx, %ebx
	je	.L22
	movl	4(%r12), %eax
	addl	%ebx, %r15d
	addl	%ebx, %eax
	addl	12(%r12), %ebx
	movl	%ebx, %ecx
	movl	%eax, 4(%r12)
	movl	%eax, -144(%rbp)
	movl	%ebx, 12(%r12)
	cmpl	%ecx, %r15d
	jge	.L19
	.p2align 4,,10
	.p2align 3
.L20:
	movq	(%r14), %rax
	movl	%r15d, %esi
	movl	%r15d, -140(%rbp)
	movq	%r14, %rdi
	xorl	%ebx, %ebx
	call	*80(%rax)
	cmpl	$65535, %eax
	movl	%eax, %edi
	movl	%eax, -180(%rbp)
	seta	%bl
	addl	$1, %ebx
	addl	%ebx, %r15d
	movl	%r15d, -136(%rbp)
	call	ucase_getTypeOrIgnorable_67@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	jns	.L56
.L22:
	movl	12(%r12), %ecx
	cmpl	%ecx, %r15d
	jl	.L20
.L19:
	movl	%r15d, 8(%r12)
	movq	-192(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L12:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L57
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	subl	$2, %r13d
	cmpl	%r13d, (%r12)
	jle	.L14
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L23:
	call	ucase_toFullLower_67@PLT
	movl	%eax, %ecx
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L28:
	movq	-192(%rbp), %rdi
	movl	%ecx, -180(%rbp)
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	movswl	-120(%rbp), %edx
	movl	-180(%rbp), %ecx
	testw	%dx, %dx
	js	.L30
	sarl	$5, %edx
.L31:
	movq	-192(%rbp), %rdi
	xorl	%esi, %esi
	call	_ZN6icu_6713UnicodeString7replaceEiii@PLT
	movswl	-120(%rbp), %ecx
	testw	%cx, %cx
	js	.L32
	sarl	$5, %ecx
.L33:
	subl	%ebx, %ecx
	movl	%ecx, %ebx
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L30:
	movl	-116(%rbp), %edx
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L32:
	movl	-116(%rbp), %ecx
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L37:
	movb	$0, -181(%rbp)
	movl	8(%r12), %r9d
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L55:
	movl	-140(%rbp), %eax
	movq	-192(%rbp), %rdi
	movl	%eax, 8(%r12)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L12
.L57:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3069:
	.size	_ZNK6icu_6723TitlecaseTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositiona, .-_ZNK6icu_6723TitlecaseTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositiona
	.align 2
	.p2align 4
	.globl	_ZN6icu_6723TitlecaseTransliterator16getStaticClassIDEv
	.type	_ZN6icu_6723TitlecaseTransliterator16getStaticClassIDEv, @function
_ZN6icu_6723TitlecaseTransliterator16getStaticClassIDEv:
.LFB3056:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6723TitlecaseTransliterator16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3056:
	.size	_ZN6icu_6723TitlecaseTransliterator16getStaticClassIDEv, .-_ZN6icu_6723TitlecaseTransliterator16getStaticClassIDEv
	.section	.rodata.str2.2,"aMS",@progbits,2
	.align 2
.LC0:
	.string	"A"
	.string	"n"
	.string	"y"
	.string	"-"
	.string	"T"
	.string	"i"
	.string	"t"
	.string	"l"
	.string	"e"
	.string	""
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6723TitlecaseTransliteratorC2Ev
	.type	_ZN6icu_6723TitlecaseTransliteratorC2Ev, @function
_ZN6icu_6723TitlecaseTransliteratorC2Ev:
.LFB3059:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$9, %ecx
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-96(%rbp), %r13
	movq	%rdi, %r12
	leaq	-104(%rbp), %rdx
	movq	%r13, %rdi
	subq	$96, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	.LC0(%rip), %rax
	movq	%rax, -104(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6721CaseMapTransliteratorC2ERKNS_13UnicodeStringEPFiiPFiPvaES4_PPKDsiE@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	16+_ZTVN6icu_6723TitlecaseTransliteratorE(%rip), %rax
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, (%r12)
	call	_ZN6icu_6714Transliterator23setMaximumContextLengthEi@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L62
	addq	$96, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L62:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3059:
	.size	_ZN6icu_6723TitlecaseTransliteratorC2Ev, .-_ZN6icu_6723TitlecaseTransliteratorC2Ev
	.globl	_ZN6icu_6723TitlecaseTransliteratorC1Ev
	.set	_ZN6icu_6723TitlecaseTransliteratorC1Ev,_ZN6icu_6723TitlecaseTransliteratorC2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6723TitlecaseTransliteratorC2ERKS0_
	.type	_ZN6icu_6723TitlecaseTransliteratorC2ERKS0_, @function
_ZN6icu_6723TitlecaseTransliteratorC2ERKS0_:
.LFB3066:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_6721CaseMapTransliteratorC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6723TitlecaseTransliteratorE(%rip), %rax
	movq	%rax, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3066:
	.size	_ZN6icu_6723TitlecaseTransliteratorC2ERKS0_, .-_ZN6icu_6723TitlecaseTransliteratorC2ERKS0_
	.globl	_ZN6icu_6723TitlecaseTransliteratorC1ERKS0_
	.set	_ZN6icu_6723TitlecaseTransliteratorC1ERKS0_,_ZN6icu_6723TitlecaseTransliteratorC2ERKS0_
	.weak	_ZTSN6icu_6723TitlecaseTransliteratorE
	.section	.rodata._ZTSN6icu_6723TitlecaseTransliteratorE,"aG",@progbits,_ZTSN6icu_6723TitlecaseTransliteratorE,comdat
	.align 32
	.type	_ZTSN6icu_6723TitlecaseTransliteratorE, @object
	.size	_ZTSN6icu_6723TitlecaseTransliteratorE, 35
_ZTSN6icu_6723TitlecaseTransliteratorE:
	.string	"N6icu_6723TitlecaseTransliteratorE"
	.weak	_ZTIN6icu_6723TitlecaseTransliteratorE
	.section	.data.rel.ro._ZTIN6icu_6723TitlecaseTransliteratorE,"awG",@progbits,_ZTIN6icu_6723TitlecaseTransliteratorE,comdat
	.align 8
	.type	_ZTIN6icu_6723TitlecaseTransliteratorE, @object
	.size	_ZTIN6icu_6723TitlecaseTransliteratorE, 24
_ZTIN6icu_6723TitlecaseTransliteratorE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6723TitlecaseTransliteratorE
	.quad	_ZTIN6icu_6721CaseMapTransliteratorE
	.weak	_ZTVN6icu_6723TitlecaseTransliteratorE
	.section	.data.rel.ro._ZTVN6icu_6723TitlecaseTransliteratorE,"awG",@progbits,_ZTVN6icu_6723TitlecaseTransliteratorE,comdat
	.align 8
	.type	_ZTVN6icu_6723TitlecaseTransliteratorE, @object
	.size	_ZTVN6icu_6723TitlecaseTransliteratorE, 152
_ZTVN6icu_6723TitlecaseTransliteratorE:
	.quad	0
	.quad	_ZTIN6icu_6723TitlecaseTransliteratorE
	.quad	_ZN6icu_6723TitlecaseTransliteratorD1Ev
	.quad	_ZN6icu_6723TitlecaseTransliteratorD0Ev
	.quad	_ZNK6icu_6723TitlecaseTransliterator17getDynamicClassIDEv
	.quad	_ZNK6icu_6723TitlecaseTransliterator5cloneEv
	.quad	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableEii
	.quad	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableE
	.quad	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableER14UTransPositionRKNS_13UnicodeStringER10UErrorCode
	.quad	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableER14UTransPositioniR10UErrorCode
	.quad	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableER14UTransPositionR10UErrorCode
	.quad	_ZNK6icu_6714Transliterator21finishTransliterationERNS_11ReplaceableER14UTransPosition
	.quad	_ZNK6icu_6723TitlecaseTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositiona
	.quad	_ZNK6icu_6714Transliterator21filteredTransliterateERNS_11ReplaceableER14UTransPositiona
	.quad	_ZNK6icu_6714Transliterator21filteredTransliterateERNS_11ReplaceableER14UTransPositionaa
	.quad	_ZNK6icu_6714Transliterator5getIDEv
	.quad	_ZNK6icu_6714Transliterator7toRulesERNS_13UnicodeStringEa
	.quad	_ZNK6icu_6714Transliterator18handleGetSourceSetERNS_10UnicodeSetE
	.quad	_ZNK6icu_6714Transliterator12getTargetSetERNS_10UnicodeSetE
	.local	_ZZN6icu_6723TitlecaseTransliterator16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6723TitlecaseTransliterator16getStaticClassIDEvE7classID,1,1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
