	.file	"coptccal.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714CopticCalendar17getDynamicClassIDEv
	.type	_ZNK6icu_6714CopticCalendar17getDynamicClassIDEv, @function
_ZNK6icu_6714CopticCalendar17getDynamicClassIDEv:
.LFB2971:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6714CopticCalendar16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2971:
	.size	_ZNK6icu_6714CopticCalendar17getDynamicClassIDEv, .-_ZNK6icu_6714CopticCalendar17getDynamicClassIDEv
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"coptic"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714CopticCalendar7getTypeEv
	.type	_ZNK6icu_6714CopticCalendar7getTypeEv, @function
_ZNK6icu_6714CopticCalendar7getTypeEv:
.LFB2983:
	.cfi_startproc
	endbr64
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE2983:
	.size	_ZNK6icu_6714CopticCalendar7getTypeEv, .-_ZNK6icu_6714CopticCalendar7getTypeEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714CopticCalendar16getJDEpochOffsetEv
	.type	_ZNK6icu_6714CopticCalendar16getJDEpochOffsetEv, @function
_ZNK6icu_6714CopticCalendar16getJDEpochOffsetEv:
.LFB2989:
	.cfi_startproc
	endbr64
	movl	$1824665, %eax
	ret
	.cfi_endproc
.LFE2989:
	.size	_ZNK6icu_6714CopticCalendar16getJDEpochOffsetEv, .-_ZNK6icu_6714CopticCalendar16getJDEpochOffsetEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714CopticCalendarD2Ev
	.type	_ZN6icu_6714CopticCalendarD2Ev, @function
_ZN6icu_6714CopticCalendarD2Ev:
.LFB2979:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6714CopticCalendarE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6710CECalendarD2Ev@PLT
	.cfi_endproc
.LFE2979:
	.size	_ZN6icu_6714CopticCalendarD2Ev, .-_ZN6icu_6714CopticCalendarD2Ev
	.globl	_ZN6icu_6714CopticCalendarD1Ev
	.set	_ZN6icu_6714CopticCalendarD1Ev,_ZN6icu_6714CopticCalendarD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714CopticCalendarD0Ev
	.type	_ZN6icu_6714CopticCalendarD0Ev, @function
_ZN6icu_6714CopticCalendarD0Ev:
.LFB2981:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6714CopticCalendarE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6710CECalendarD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2981:
	.size	_ZN6icu_6714CopticCalendarD0Ev, .-_ZN6icu_6714CopticCalendarD0Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714CopticCalendar5cloneEv
	.type	_ZNK6icu_6714CopticCalendar5cloneEv, @function
_ZNK6icu_6714CopticCalendar5cloneEv:
.LFB2982:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movl	$616, %edi
	pushq	%r12
	.cfi_offset 12, -32
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L8
	movq	%rax, %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6710CECalendarC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6714CopticCalendarE(%rip), %rax
	movq	%rax, (%r12)
.L8:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2982:
	.size	_ZNK6icu_6714CopticCalendar5cloneEv, .-_ZNK6icu_6714CopticCalendar5cloneEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714CopticCalendar21handleGetExtendedYearEv
	.type	_ZN6icu_6714CopticCalendar21handleGetExtendedYearEv, @function
_ZN6icu_6714CopticCalendar21handleGetExtendedYearEv:
.LFB2984:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	movl	$19, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZNK6icu_678Calendar10newerFieldE19UCalendarDateFieldsS1_@PLT
	cmpl	$19, %eax
	je	.L22
	movl	128(%rbx), %eax
	movl	132(%rbx), %edx
	testl	%eax, %eax
	jle	.L17
	movl	12(%rbx), %eax
	testl	%eax, %eax
	je	.L23
.L17:
	movl	$1, %eax
	testl	%edx, %edx
	jle	.L14
	movl	16(%rbx), %eax
.L14:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	.cfi_restore_state
	movl	204(%rbx), %edx
	movl	$1, %eax
	testl	%edx, %edx
	jle	.L14
	movl	88(%rbx), %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	.cfi_restore_state
	testl	%edx, %edx
	jle	.L14
	movl	$1, %eax
	subl	16(%rbx), %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2984:
	.size	_ZN6icu_6714CopticCalendar21handleGetExtendedYearEv, .-_ZN6icu_6714CopticCalendar21handleGetExtendedYearEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714CopticCalendar19handleComputeFieldsEiR10UErrorCode
	.type	_ZN6icu_6714CopticCalendar19handleComputeFieldsEiR10UErrorCode, @function
_ZN6icu_6714CopticCalendar19handleComputeFieldsEiR10UErrorCode:
.LFB2985:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZNK6icu_6714CopticCalendar16getJDEpochOffsetEv(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%esi, %r12d
	movl	$1824665, %esi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	400(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L32
.L25:
	leaq	-32(%rbp), %rcx
	leaq	-36(%rbp), %rdx
	movl	%r12d, %edi
	leaq	-28(%rbp), %r8
	call	_ZN6icu_6710CECalendar6jdToCEEiiRiS1_S1_@PLT
	movl	-36(%rbp), %eax
	movl	$1, %edx
	movl	%eax, %ecx
	testl	%eax, %eax
	jg	.L26
	movl	%edx, %ecx
	xorl	%edx, %edx
	subl	%eax, %ecx
.L26:
	movl	%eax, 88(%rbx)
	movl	-32(%rbp), %eax
	movl	$257, %esi
	movl	%ecx, 16(%rbx)
	movl	$257, %ecx
	movl	%eax, 20(%rbx)
	imull	$30, %eax, %eax
	movw	%cx, 104(%rbx)
	movl	-28(%rbp), %ecx
	movl	%edx, 12(%rbx)
	movabsq	$4294967297, %rdx
	addl	%ecx, %eax
	movb	$1, 123(%rbx)
	movl	$1, 204(%rbx)
	movq	%rdx, 128(%rbx)
	movl	$1, 136(%rbx)
	movb	$1, 106(%rbx)
	movl	%ecx, 32(%rbx)
	movl	%eax, 36(%rbx)
	movq	%rdx, 148(%rbx)
	movw	%si, 109(%rbx)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L33
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore_state
	call	*%rax
	movl	%eax, %esi
	jmp	.L25
.L33:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2985:
	.size	_ZN6icu_6714CopticCalendar19handleComputeFieldsEiR10UErrorCode, .-_ZN6icu_6714CopticCalendar19handleComputeFieldsEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714CopticCalendar16getStaticClassIDEv
	.type	_ZN6icu_6714CopticCalendar16getStaticClassIDEv, @function
_ZN6icu_6714CopticCalendar16getStaticClassIDEv:
.LFB2970:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6714CopticCalendar16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2970:
	.size	_ZN6icu_6714CopticCalendar16getStaticClassIDEv, .-_ZN6icu_6714CopticCalendar16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714CopticCalendarC2ERKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_6714CopticCalendarC2ERKNS_6LocaleER10UErrorCode, @function
_ZN6icu_6714CopticCalendarC2ERKNS_6LocaleER10UErrorCode:
.LFB2973:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_6710CECalendarC2ERKNS_6LocaleER10UErrorCode@PLT
	leaq	16+_ZTVN6icu_6714CopticCalendarE(%rip), %rax
	movq	%rax, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2973:
	.size	_ZN6icu_6714CopticCalendarC2ERKNS_6LocaleER10UErrorCode, .-_ZN6icu_6714CopticCalendarC2ERKNS_6LocaleER10UErrorCode
	.globl	_ZN6icu_6714CopticCalendarC1ERKNS_6LocaleER10UErrorCode
	.set	_ZN6icu_6714CopticCalendarC1ERKNS_6LocaleER10UErrorCode,_ZN6icu_6714CopticCalendarC2ERKNS_6LocaleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714CopticCalendarC2ERKS0_
	.type	_ZN6icu_6714CopticCalendarC2ERKS0_, @function
_ZN6icu_6714CopticCalendarC2ERKS0_:
.LFB2976:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_6710CECalendarC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6714CopticCalendarE(%rip), %rax
	movq	%rax, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2976:
	.size	_ZN6icu_6714CopticCalendarC2ERKS0_, .-_ZN6icu_6714CopticCalendarC2ERKS0_
	.globl	_ZN6icu_6714CopticCalendarC1ERKS0_
	.set	_ZN6icu_6714CopticCalendarC1ERKS0_,_ZN6icu_6714CopticCalendarC2ERKS0_
	.section	.rodata.str1.1
.LC1:
	.string	"@calendar=coptic"
	.text
	.p2align 4
	.type	_ZN6icu_67L30initializeSystemDefaultCenturyEv, @function
_ZN6icu_67L30initializeSystemDefaultCenturyEv:
.LFB2986:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	leaq	.LC1(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-880(%rbp), %r13
	leaq	-884(%rbp), %r14
	pushq	%r12
	movq	%r13, %rdi
	.cfi_offset 12, -40
	leaq	-656(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -48
	leaq	16+_ZTVN6icu_6714CopticCalendarE(%rip), %rbx
	subq	$864, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$0, -884(%rbp)
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6710CECalendarC2ERKNS_6LocaleER10UErrorCode@PLT
	movq	%r13, %rdi
	movq	%rbx, -656(%rbp)
	call	_ZN6icu_676LocaleD1Ev@PLT
	movl	-884(%rbp), %eax
	testl	%eax, %eax
	jle	.L43
.L40:
	movq	%r12, %rdi
	movq	%rbx, -656(%rbp)
	call	_ZN6icu_6710CECalendarD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L44
	addq	$864, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	.cfi_restore_state
	call	_ZN6icu_678Calendar6getNowEv@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_678Calendar15setTimeInMillisEdR10UErrorCode@PLT
	movq	%r14, %rcx
	movl	$-80, %edx
	movq	%r12, %rdi
	movl	$1, %esi
	call	_ZN6icu_678Calendar3addE19UCalendarDateFieldsiR10UErrorCode@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZNK6icu_678Calendar15getTimeInMillisER10UErrorCode@PLT
	movq	%r14, %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	movsd	%xmm0, _ZN6icu_67L26gSystemDefaultCenturyStartE(%rip)
	call	_ZNK6icu_678Calendar3getE19UCalendarDateFieldsR10UErrorCode@PLT
	movl	%eax, _ZN6icu_67L30gSystemDefaultCenturyStartYearE(%rip)
	jmp	.L40
.L44:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2986:
	.size	_ZN6icu_67L30initializeSystemDefaultCenturyEv, .-_ZN6icu_67L30initializeSystemDefaultCenturyEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714CopticCalendar19defaultCenturyStartEv
	.type	_ZNK6icu_6714CopticCalendar19defaultCenturyStartEv, @function
_ZNK6icu_6714CopticCalendar19defaultCenturyStartEv:
.LFB2987:
	.cfi_startproc
	endbr64
	movl	_ZN6icu_67L25gSystemDefaultCenturyInitE(%rip), %eax
	cmpl	$2, %eax
	je	.L53
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN6icu_67L25gSystemDefaultCenturyInitE(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L47
	call	_ZN6icu_67L30initializeSystemDefaultCenturyEv
	leaq	_ZN6icu_67L25gSystemDefaultCenturyInitE(%rip), %rdi
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L47:
	movsd	_ZN6icu_67L26gSystemDefaultCenturyStartE(%rip), %xmm0
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	.cfi_restore 6
	movsd	_ZN6icu_67L26gSystemDefaultCenturyStartE(%rip), %xmm0
	ret
	.cfi_endproc
.LFE2987:
	.size	_ZNK6icu_6714CopticCalendar19defaultCenturyStartEv, .-_ZNK6icu_6714CopticCalendar19defaultCenturyStartEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714CopticCalendar23defaultCenturyStartYearEv
	.type	_ZNK6icu_6714CopticCalendar23defaultCenturyStartYearEv, @function
_ZNK6icu_6714CopticCalendar23defaultCenturyStartYearEv:
.LFB2988:
	.cfi_startproc
	endbr64
	movl	_ZN6icu_67L25gSystemDefaultCenturyInitE(%rip), %eax
	cmpl	$2, %eax
	je	.L64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN6icu_67L25gSystemDefaultCenturyInitE(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L58
	call	_ZN6icu_67L30initializeSystemDefaultCenturyEv
	leaq	_ZN6icu_67L25gSystemDefaultCenturyInitE(%rip), %rdi
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L58:
	movl	_ZN6icu_67L30gSystemDefaultCenturyStartYearE(%rip), %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L64:
	.cfi_restore 6
	movl	_ZN6icu_67L30gSystemDefaultCenturyStartYearE(%rip), %eax
	ret
	.cfi_endproc
.LFE2988:
	.size	_ZNK6icu_6714CopticCalendar23defaultCenturyStartYearEv, .-_ZNK6icu_6714CopticCalendar23defaultCenturyStartYearEv
	.weak	_ZTSN6icu_6714CopticCalendarE
	.section	.rodata._ZTSN6icu_6714CopticCalendarE,"aG",@progbits,_ZTSN6icu_6714CopticCalendarE,comdat
	.align 16
	.type	_ZTSN6icu_6714CopticCalendarE, @object
	.size	_ZTSN6icu_6714CopticCalendarE, 26
_ZTSN6icu_6714CopticCalendarE:
	.string	"N6icu_6714CopticCalendarE"
	.weak	_ZTIN6icu_6714CopticCalendarE
	.section	.data.rel.ro._ZTIN6icu_6714CopticCalendarE,"awG",@progbits,_ZTIN6icu_6714CopticCalendarE,comdat
	.align 8
	.type	_ZTIN6icu_6714CopticCalendarE, @object
	.size	_ZTIN6icu_6714CopticCalendarE, 24
_ZTIN6icu_6714CopticCalendarE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6714CopticCalendarE
	.quad	_ZTIN6icu_6710CECalendarE
	.weak	_ZTVN6icu_6714CopticCalendarE
	.section	.data.rel.ro._ZTVN6icu_6714CopticCalendarE,"awG",@progbits,_ZTVN6icu_6714CopticCalendarE,comdat
	.align 8
	.type	_ZTVN6icu_6714CopticCalendarE, @object
	.size	_ZTVN6icu_6714CopticCalendarE, 424
_ZTVN6icu_6714CopticCalendarE:
	.quad	0
	.quad	_ZTIN6icu_6714CopticCalendarE
	.quad	_ZN6icu_6714CopticCalendarD1Ev
	.quad	_ZN6icu_6714CopticCalendarD0Ev
	.quad	_ZNK6icu_6714CopticCalendar17getDynamicClassIDEv
	.quad	_ZNK6icu_6714CopticCalendar5cloneEv
	.quad	_ZNK6icu_678CalendareqERKS0_
	.quad	_ZNK6icu_678Calendar14isEquivalentToERKS0_
	.quad	_ZN6icu_678Calendar3addENS0_11EDateFieldsEiR10UErrorCode
	.quad	_ZN6icu_678Calendar3addE19UCalendarDateFieldsiR10UErrorCode
	.quad	_ZN6icu_678Calendar4rollENS0_11EDateFieldsEiR10UErrorCode
	.quad	_ZN6icu_678Calendar4rollE19UCalendarDateFieldsiR10UErrorCode
	.quad	_ZN6icu_678Calendar15fieldDifferenceEdNS0_11EDateFieldsER10UErrorCode
	.quad	_ZN6icu_678Calendar15fieldDifferenceEd19UCalendarDateFieldsR10UErrorCode
	.quad	_ZNK6icu_6710CECalendar14inDaylightTimeER10UErrorCode
	.quad	_ZNK6icu_678Calendar10getMinimumENS0_11EDateFieldsE
	.quad	_ZNK6icu_678Calendar10getMinimumE19UCalendarDateFields
	.quad	_ZNK6icu_678Calendar10getMaximumENS0_11EDateFieldsE
	.quad	_ZNK6icu_678Calendar10getMaximumE19UCalendarDateFields
	.quad	_ZNK6icu_678Calendar18getGreatestMinimumENS0_11EDateFieldsE
	.quad	_ZNK6icu_678Calendar18getGreatestMinimumE19UCalendarDateFields
	.quad	_ZNK6icu_678Calendar15getLeastMaximumENS0_11EDateFieldsE
	.quad	_ZNK6icu_678Calendar15getLeastMaximumE19UCalendarDateFields
	.quad	_ZNK6icu_678Calendar16getActualMinimumE19UCalendarDateFieldsR10UErrorCode
	.quad	_ZNK6icu_678Calendar16getActualMaximumE19UCalendarDateFieldsR10UErrorCode
	.quad	_ZNK6icu_6714CopticCalendar7getTypeEv
	.quad	_ZNK6icu_678Calendar16getDayOfWeekTypeE19UCalendarDaysOfWeekR10UErrorCode
	.quad	_ZNK6icu_678Calendar20getWeekendTransitionE19UCalendarDaysOfWeekR10UErrorCode
	.quad	_ZNK6icu_678Calendar9isWeekendEdR10UErrorCode
	.quad	_ZNK6icu_678Calendar9isWeekendEv
	.quad	_ZN6icu_678Calendar11computeTimeER10UErrorCode
	.quad	_ZN6icu_678Calendar13computeFieldsER10UErrorCode
	.quad	_ZN6icu_678Calendar16prepareGetActualE19UCalendarDateFieldsaR10UErrorCode
	.quad	_ZNK6icu_6710CECalendar14handleGetLimitE19UCalendarDateFieldsNS_8Calendar10ELimitTypeE
	.quad	_ZNK6icu_678Calendar8getLimitE19UCalendarDateFieldsNS0_10ELimitTypeE
	.quad	_ZNK6icu_6710CECalendar23handleComputeMonthStartEiia
	.quad	_ZNK6icu_678Calendar20handleGetMonthLengthEii
	.quad	_ZNK6icu_678Calendar19handleGetYearLengthEi
	.quad	_ZN6icu_6714CopticCalendar21handleGetExtendedYearEv
	.quad	_ZN6icu_678Calendar22handleComputeJulianDayE19UCalendarDateFields
	.quad	_ZN6icu_678Calendar35handleGetExtendedYearFromWeekFieldsEii
	.quad	_ZN6icu_678Calendar13validateFieldE19UCalendarDateFieldsR10UErrorCode
	.quad	_ZNK6icu_678Calendar23getFieldResolutionTableEv
	.quad	_ZN6icu_6714CopticCalendar19handleComputeFieldsEiR10UErrorCode
	.quad	_ZN6icu_678Calendar21getDefaultMonthInYearEi
	.quad	_ZN6icu_678Calendar20getDefaultDayInMonthEii
	.quad	_ZN6icu_678Calendar8pinFieldE19UCalendarDateFieldsR10UErrorCode
	.quad	_ZNK6icu_6710CECalendar18haveDefaultCenturyEv
	.quad	_ZNK6icu_6714CopticCalendar19defaultCenturyStartEv
	.quad	_ZNK6icu_6714CopticCalendar23defaultCenturyStartYearEv
	.quad	_ZNK6icu_678Calendar14getRelatedYearER10UErrorCode
	.quad	_ZN6icu_678Calendar14setRelatedYearEi
	.quad	_ZNK6icu_6714CopticCalendar16getJDEpochOffsetEv
	.local	_ZN6icu_67L25gSystemDefaultCenturyInitE
	.comm	_ZN6icu_67L25gSystemDefaultCenturyInitE,8,8
	.data
	.align 4
	.type	_ZN6icu_67L30gSystemDefaultCenturyStartYearE, @object
	.size	_ZN6icu_67L30gSystemDefaultCenturyStartYearE, 4
_ZN6icu_67L30gSystemDefaultCenturyStartYearE:
	.long	-1
	.align 8
	.type	_ZN6icu_67L26gSystemDefaultCenturyStartE, @object
	.size	_ZN6icu_67L26gSystemDefaultCenturyStartE, 8
_ZN6icu_67L26gSystemDefaultCenturyStartE:
	.long	0
	.long	1048576
	.local	_ZZN6icu_6714CopticCalendar16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6714CopticCalendar16getStaticClassIDEvE7classID,1,1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
