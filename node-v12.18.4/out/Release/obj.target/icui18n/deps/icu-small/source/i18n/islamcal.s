	.file	"islamcal.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715IslamicCalendar14handleGetLimitE19UCalendarDateFieldsNS_8Calendar10ELimitTypeE
	.type	_ZNK6icu_6715IslamicCalendar14handleGetLimitE19UCalendarDateFieldsNS_8Calendar10ELimitTypeE, @function
_ZNK6icu_6715IslamicCalendar14handleGetLimitE19UCalendarDateFieldsNS_8Calendar10ELimitTypeE:
.LFB3115:
	.cfi_startproc
	endbr64
	movslq	%esi, %rsi
	movslq	%edx, %rdx
	leaq	_ZN6icu_67L6LIMITSE(%rip), %rax
	leaq	(%rdx,%rsi,4), %rdx
	movl	(%rax,%rdx,4), %eax
	ret
	.cfi_endproc
.LFE3115:
	.size	_ZNK6icu_6715IslamicCalendar14handleGetLimitE19UCalendarDateFieldsNS_8Calendar10ELimitTypeE, .-_ZNK6icu_6715IslamicCalendar14handleGetLimitE19UCalendarDateFieldsNS_8Calendar10ELimitTypeE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715IslamicCalendar18haveDefaultCenturyEv
	.type	_ZNK6icu_6715IslamicCalendar18haveDefaultCenturyEv, @function
_ZNK6icu_6715IslamicCalendar18haveDefaultCenturyEv:
.LFB3133:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3133:
	.size	_ZNK6icu_6715IslamicCalendar18haveDefaultCenturyEv, .-_ZNK6icu_6715IslamicCalendar18haveDefaultCenturyEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715IslamicCalendar17getDynamicClassIDEv
	.type	_ZNK6icu_6715IslamicCalendar17getDynamicClassIDEv, @function
_ZNK6icu_6715IslamicCalendar17getDynamicClassIDEv:
.LFB3138:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6715IslamicCalendar16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3138:
	.size	_ZNK6icu_6715IslamicCalendar17getDynamicClassIDEv, .-_ZNK6icu_6715IslamicCalendar17getDynamicClassIDEv
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"islamic-civil"
.LC1:
	.string	"islamic-umalqura"
.LC2:
	.string	"islamic"
.LC3:
	.string	"islamic-tbla"
	.section	.text.unlikely,"ax",@progbits
	.align 2
.LCOLDB4:
	.text
.LHOTB4:
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715IslamicCalendar7getTypeEv
	.type	_ZNK6icu_6715IslamicCalendar7getTypeEv, @function
_ZNK6icu_6715IslamicCalendar7getTypeEv:
.LFB3101:
	.cfi_startproc
	endbr64
	movl	612(%rdi), %eax
	cmpl	$2, %eax
	je	.L8
	ja	.L7
	testl	%eax, %eax
	leaq	.LC2(%rip), %rdx
	leaq	.LC0(%rip), %rax
	cmove	%rdx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpl	$3, %eax
	jne	.L11
	leaq	.LC3(%rip), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_restore 6
	leaq	.LC1(%rip), %rax
	ret
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZNK6icu_6715IslamicCalendar7getTypeEv.cold, @function
_ZNK6icu_6715IslamicCalendar7getTypeEv.cold:
.LFSB3101:
.L11:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	call	abort@PLT
	.cfi_endproc
.LFE3101:
	.text
	.size	_ZNK6icu_6715IslamicCalendar7getTypeEv, .-_ZNK6icu_6715IslamicCalendar7getTypeEv
	.section	.text.unlikely
	.size	_ZNK6icu_6715IslamicCalendar7getTypeEv.cold, .-_ZNK6icu_6715IslamicCalendar7getTypeEv.cold
.LCOLDE4:
	.text
.LHOTE4:
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715IslamicCalendar5cloneEv
	.type	_ZNK6icu_6715IslamicCalendar5cloneEv, @function
_ZNK6icu_6715IslamicCalendar5cloneEv:
.LFB3102:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	$616, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L15
	movq	%rax, %rdi
	movq	%rbx, %rsi
	call	_ZN6icu_678CalendarC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6715IslamicCalendarE(%rip), %rax
	movq	%rax, (%r12)
	movl	612(%rbx), %eax
	movl	%eax, 612(%r12)
.L15:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3102:
	.size	_ZNK6icu_6715IslamicCalendar5cloneEv, .-_ZNK6icu_6715IslamicCalendar5cloneEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715IslamicCalendarD2Ev
	.type	_ZN6icu_6715IslamicCalendarD2Ev, @function
_ZN6icu_6715IslamicCalendarD2Ev:
.LFB3110:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6715IslamicCalendarE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_678CalendarD2Ev@PLT
	.cfi_endproc
.LFE3110:
	.size	_ZN6icu_6715IslamicCalendarD2Ev, .-_ZN6icu_6715IslamicCalendarD2Ev
	.globl	_ZN6icu_6715IslamicCalendarD1Ev
	.set	_ZN6icu_6715IslamicCalendarD1Ev,_ZN6icu_6715IslamicCalendarD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715IslamicCalendarD0Ev
	.type	_ZN6icu_6715IslamicCalendarD0Ev, @function
_ZN6icu_6715IslamicCalendarD0Ev:
.LFB3112:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6715IslamicCalendarE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_678CalendarD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3112:
	.size	_ZN6icu_6715IslamicCalendarD0Ev, .-_ZN6icu_6715IslamicCalendarD0Ev
	.p2align 4
	.type	calendar_islamic_cleanup, @function
calendar_islamic_cleanup:
.LFB3099:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	_ZL11gMonthCache(%rip), %rdi
	testq	%rdi, %rdi
	je	.L25
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	$0, _ZL11gMonthCache(%rip)
.L25:
	movq	_ZL21gIslamicCalendarAstro(%rip), %r12
	testq	%r12, %r12
	je	.L26
	movq	%r12, %rdi
	call	_ZN6icu_6718CalendarAstronomerD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	movq	$0, _ZL21gIslamicCalendarAstro(%rip)
.L26:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3099:
	.size	calendar_islamic_cleanup, .-calendar_islamic_cleanup
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715IslamicCalendar14inDaylightTimeER10UErrorCode
	.type	_ZNK6icu_6715IslamicCalendar14inDaylightTimeER10UErrorCode, @function
_ZNK6icu_6715IslamicCalendar14inDaylightTimeER10UErrorCode:
.LFB3132:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	testl	%ecx, %ecx
	jle	.L35
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_ZNK6icu_678Calendar11getTimeZoneEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*72(%rax)
	testb	%al, %al
	jne	.L46
	xorl	%eax, %eax
.L34:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	.cfi_restore_state
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_678Calendar8completeER10UErrorCode@PLT
	movl	(%rbx), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L34
	movl	76(%r12), %eax
	testl	%eax, %eax
	setne	%al
	jmp	.L34
	.cfi_endproc
.LFE3132:
	.size	_ZNK6icu_6715IslamicCalendar14inDaylightTimeER10UErrorCode, .-_ZNK6icu_6715IslamicCalendar14inDaylightTimeER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715IslamicCalendar21handleGetExtendedYearEv
	.type	_ZN6icu_6715IslamicCalendar21handleGetExtendedYearEv, @function
_ZN6icu_6715IslamicCalendar21handleGetExtendedYearEv:
.LFB3130:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	movl	$19, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZNK6icu_678Calendar10newerFieldE19UCalendarDateFieldsS1_@PLT
	cmpl	$19, %eax
	movl	$1, %eax
	je	.L53
	movl	132(%rbx), %edx
	testl	%edx, %edx
	jle	.L47
	movl	16(%rbx), %eax
.L47:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	.cfi_restore_state
	movl	204(%rbx), %ecx
	testl	%ecx, %ecx
	jle	.L47
	movl	88(%rbx), %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3130:
	.size	_ZN6icu_6715IslamicCalendar21handleGetExtendedYearEv, .-_ZN6icu_6715IslamicCalendar21handleGetExtendedYearEv
	.p2align 4
	.globl	_ZN6icu_6723getUmalqura_MonthLengthEii
	.type	_ZN6icu_6723getUmalqura_MonthLengthEii, @function
_ZN6icu_6723getUmalqura_MonthLengthEii:
.LFB3100:
	.cfi_startproc
	endbr64
	movl	$11, %ecx
	movslq	%edi, %rdi
	movl	$1, %eax
	subl	%esi, %ecx
	leaq	_ZN6icu_67L20UMALQURA_MONTHLENGTHE(%rip), %rdx
	sall	%cl, %eax
	testl	%eax, (%rdx,%rdi,4)
	setne	%al
	movzbl	%al, %eax
	addl	$29, %eax
	ret
	.cfi_endproc
.LFE3100:
	.size	_ZN6icu_6723getUmalqura_MonthLengthEii, .-_ZN6icu_6723getUmalqura_MonthLengthEii
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715IslamicCalendarC2ERKNS_6LocaleER10UErrorCodeNS0_16ECalculationTypeE
	.type	_ZN6icu_6715IslamicCalendarC2ERKNS_6LocaleER10UErrorCodeNS0_16ECalculationTypeE, @function
_ZN6icu_6715IslamicCalendarC2ERKNS_6LocaleER10UErrorCodeNS0_16ECalculationTypeE:
.LFB3104:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%ecx, %ebx
	call	_ZN6icu_678TimeZone13createDefaultEv@PLT
	movq	%r12, %rdi
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%rax, %rsi
	call	_ZN6icu_678CalendarC2EPNS_8TimeZoneERKNS_6LocaleER10UErrorCode@PLT
	leaq	16+_ZTVN6icu_6715IslamicCalendarE(%rip), %rax
	movl	%ebx, 612(%r12)
	movq	%rax, (%r12)
	call	_ZN6icu_678Calendar6getNowEv@PLT
	popq	%rbx
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_678Calendar15setTimeInMillisEdR10UErrorCode@PLT
	.cfi_endproc
.LFE3104:
	.size	_ZN6icu_6715IslamicCalendarC2ERKNS_6LocaleER10UErrorCodeNS0_16ECalculationTypeE, .-_ZN6icu_6715IslamicCalendarC2ERKNS_6LocaleER10UErrorCodeNS0_16ECalculationTypeE
	.globl	_ZN6icu_6715IslamicCalendarC1ERKNS_6LocaleER10UErrorCodeNS0_16ECalculationTypeE
	.set	_ZN6icu_6715IslamicCalendarC1ERKNS_6LocaleER10UErrorCodeNS0_16ECalculationTypeE,_ZN6icu_6715IslamicCalendarC2ERKNS_6LocaleER10UErrorCodeNS0_16ECalculationTypeE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715IslamicCalendarC2ERKS0_
	.type	_ZN6icu_6715IslamicCalendarC2ERKS0_, @function
_ZN6icu_6715IslamicCalendarC2ERKS0_:
.LFB3107:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZN6icu_678CalendarC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6715IslamicCalendarE(%rip), %rax
	movq	%rax, (%rbx)
	movl	612(%r12), %eax
	movl	%eax, 612(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3107:
	.size	_ZN6icu_6715IslamicCalendarC2ERKS0_, .-_ZN6icu_6715IslamicCalendarC2ERKS0_
	.globl	_ZN6icu_6715IslamicCalendarC1ERKS0_
	.set	_ZN6icu_6715IslamicCalendarC1ERKS0_,_ZN6icu_6715IslamicCalendarC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715IslamicCalendar18setCalculationTypeENS0_16ECalculationTypeER10UErrorCode
	.type	_ZN6icu_6715IslamicCalendar18setCalculationTypeENS0_16ECalculationTypeER10UErrorCode, @function
_ZN6icu_6715IslamicCalendar18setCalculationTypeENS0_16ECalculationTypeER10UErrorCode:
.LFB3113:
	.cfi_startproc
	endbr64
	cmpl	%esi, 612(%rdi)
	jne	.L66
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%esi, %ebx
	movq	%rdx, %rsi
	subq	$24, %rsp
	call	_ZNK6icu_678Calendar15getTimeInMillisER10UErrorCode@PLT
	movl	%ebx, 612(%r12)
	movq	%r12, %rdi
	movsd	%xmm0, -40(%rbp)
	call	_ZN6icu_678Calendar5clearEv@PLT
	movsd	-40(%rbp), %xmm0
	movq	%r13, %rsi
	movq	%r12, %rdi
	addq	$24, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_678Calendar15setTimeInMillisEdR10UErrorCode@PLT
	.cfi_endproc
.LFE3113:
	.size	_ZN6icu_6715IslamicCalendar18setCalculationTypeENS0_16ECalculationTypeER10UErrorCode, .-_ZN6icu_6715IslamicCalendar18setCalculationTypeENS0_16ECalculationTypeER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715IslamicCalendar7isCivilEv
	.type	_ZN6icu_6715IslamicCalendar7isCivilEv, @function
_ZN6icu_6715IslamicCalendar7isCivilEv:
.LFB3114:
	.cfi_startproc
	endbr64
	cmpl	$1, 612(%rdi)
	sete	%al
	ret
	.cfi_endproc
.LFE3114:
	.size	_ZN6icu_6715IslamicCalendar7isCivilEv, .-_ZN6icu_6715IslamicCalendar7isCivilEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715IslamicCalendar13civilLeapYearEi
	.type	_ZN6icu_6715IslamicCalendar13civilLeapYearEi, @function
_ZN6icu_6715IslamicCalendar13civilLeapYearEi:
.LFB3116:
	.cfi_startproc
	endbr64
	leal	(%rdi,%rdi,4), %eax
	leal	14(%rdi,%rax,2), %edx
	movslq	%edx, %rax
	movl	%edx, %ecx
	imulq	$-2004318071, %rax, %rax
	sarl	$31, %ecx
	shrq	$32, %rax
	addl	%edx, %eax
	sarl	$4, %eax
	subl	%ecx, %eax
	imull	$30, %eax, %eax
	subl	%eax, %edx
	cmpl	$10, %edx
	setle	%al
	ret
	.cfi_endproc
.LFE3116:
	.size	_ZN6icu_6715IslamicCalendar13civilLeapYearEi, .-_ZN6icu_6715IslamicCalendar13civilLeapYearEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715IslamicCalendar7moonAgeEdR10UErrorCode
	.type	_ZN6icu_6715IslamicCalendar7moonAgeEdR10UErrorCode, @function
_ZN6icu_6715IslamicCalendar7moonAgeEdR10UErrorCode:
.LFB3120:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	leaq	_ZZN6icu_6715IslamicCalendar7moonAgeEdR10UErrorCodeE9astroLock(%rip), %rdi
	subq	$16, %rsp
	movsd	%xmm0, -24(%rbp)
	call	umtx_lock_67@PLT
	movq	_ZL21gIslamicCalendarAstro(%rip), %rdi
	testq	%rdi, %rdi
	je	.L76
.L70:
	movsd	-24(%rbp), %xmm0
	call	_ZN6icu_6718CalendarAstronomer7setTimeEd@PLT
	movq	_ZL21gIslamicCalendarAstro(%rip), %rdi
	call	_ZN6icu_6718CalendarAstronomer10getMoonAgeEv@PLT
	leaq	_ZZN6icu_6715IslamicCalendar7moonAgeEdR10UErrorCodeE9astroLock(%rip), %rdi
	movsd	%xmm0, -24(%rbp)
	call	umtx_unlock_67@PLT
	movsd	.LC6(%rip), %xmm1
	movsd	-24(%rbp), %xmm0
	mulsd	%xmm1, %xmm0
	divsd	_ZN6icu_6718CalendarAstronomer2PIE(%rip), %xmm0
	comisd	%xmm1, %xmm0
	jbe	.L69
	subsd	.LC7(%rip), %xmm0
.L69:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L76:
	.cfi_restore_state
	movl	$136, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L71
	movq	%rax, %rdi
	call	_ZN6icu_6718CalendarAstronomerC1Ev@PLT
	movl	$8, %edi
	leaq	calendar_islamic_cleanup(%rip), %rsi
	movq	%r12, _ZL21gIslamicCalendarAstro(%rip)
	call	ucln_i18n_registerCleanup_67@PLT
	movq	_ZL21gIslamicCalendarAstro(%rip), %rdi
	jmp	.L70
.L71:
	movq	$0, _ZL21gIslamicCalendarAstro(%rip)
	pxor	%xmm0, %xmm0
	movl	$7, (%rbx)
	jmp	.L69
	.cfi_endproc
.LFE3120:
	.size	_ZN6icu_6715IslamicCalendar7moonAgeEdR10UErrorCode, .-_ZN6icu_6715IslamicCalendar7moonAgeEdR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715IslamicCalendar14trueMonthStartEi
	.type	_ZNK6icu_6715IslamicCalendar14trueMonthStartEi, @function
_ZNK6icu_6715IslamicCalendar14trueMonthStartEi:
.LFB3119:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZL11gMonthCache(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-28(%rbp), %r13
	movl	%esi, %r12d
	movq	%r13, %rdx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	$0, -28(%rbp)
	call	_ZN6icu_6713CalendarCache3getEPPS0_iR10UErrorCode@PLT
	testl	%eax, %eax
	je	.L93
.L78:
	movl	-28(%rbp), %edx
	testl	%edx, %edx
	jg	.L85
.L77:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L94
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L93:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%r12d, %xmm0
	mulsd	_ZN6icu_6718CalendarAstronomer13SYNODIC_MONTHE(%rip), %xmm0
	call	uprv_floor_67@PLT
	movsd	.LC8(%rip), %xmm1
	movq	%r13, %rdi
	mulsd	%xmm0, %xmm1
	subsd	.LC9(%rip), %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -40(%rbp)
	call	_ZN6icu_6715IslamicCalendar7moonAgeEdR10UErrorCode
	movl	-28(%rbp), %edi
	testl	%edi, %edi
	jle	.L95
.L85:
	xorl	%eax, %eax
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L95:
	pxor	%xmm4, %xmm4
	movsd	-40(%rbp), %xmm1
	comisd	%xmm4, %xmm0
	jnb	.L82
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L96:
	pxor	%xmm2, %xmm2
	movsd	-40(%rbp), %xmm1
	comisd	%xmm2, %xmm0
	jb	.L83
.L82:
	subsd	.LC8(%rip), %xmm1
	movq	%r13, %rdi
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -40(%rbp)
	call	_ZN6icu_6715IslamicCalendar7moonAgeEdR10UErrorCode
	movl	-28(%rbp), %esi
	testl	%esi, %esi
	jle	.L96
	xorl	%eax, %eax
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L97:
	pxor	%xmm3, %xmm3
	movsd	-40(%rbp), %xmm1
	comisd	%xmm0, %xmm3
	jbe	.L83
.L80:
	addsd	.LC8(%rip), %xmm1
	movq	%r13, %rdi
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -40(%rbp)
	call	_ZN6icu_6715IslamicCalendar7moonAgeEdR10UErrorCode
	movl	-28(%rbp), %ecx
	testl	%ecx, %ecx
	jle	.L97
	xorl	%eax, %eax
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L83:
	movsd	.LC9(%rip), %xmm0
	addsd	%xmm1, %xmm0
	divsd	.LC8(%rip), %xmm0
	call	uprv_floor_67@PLT
	movq	%r13, %rcx
	movl	%r12d, %esi
	leaq	_ZL11gMonthCache(%rip), %rdi
	cvttsd2sil	%xmm0, %eax
	addl	$1, %eax
	movl	%eax, %edx
	movl	%eax, -40(%rbp)
	call	_ZN6icu_6713CalendarCache3putEPPS0_iiR10UErrorCode@PLT
	movl	-40(%rbp), %eax
	jmp	.L78
.L94:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3119:
	.size	_ZNK6icu_6715IslamicCalendar14trueMonthStartEi, .-_ZNK6icu_6715IslamicCalendar14trueMonthStartEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715IslamicCalendar9yearStartEi
	.type	_ZNK6icu_6715IslamicCalendar9yearStartEi, @function
_ZNK6icu_6715IslamicCalendar9yearStartEi:
.LFB3117:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movl	%esi, %ebx
	subq	$8, %rsp
	movl	612(%rdi), %eax
	movl	%eax, %edx
	andl	$-3, %edx
	cmpl	$1, %edx
	je	.L99
	cmpl	$2, %eax
	je	.L109
	leal	-1300(%rsi), %edx
	testl	%eax, %eax
	jne	.L101
	addq	$8, %rsp
	leal	-3(%rsi,%rsi,2), %esi
	popq	%rbx
	sall	$2, %esi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_6715IslamicCalendar14trueMonthStartEi
	.p2align 4,,10
	.p2align 3
.L109:
	.cfi_restore_state
	leal	-1300(%rsi), %edx
	cmpl	$300, %edx
	jbe	.L101
.L99:
	leal	(%rbx,%rbx,4), %eax
	movl	$30, %esi
	leal	3(%rbx,%rax,2), %edi
	call	_ZN6icu_679ClockMath11floorDivideEii@PLT
	addq	$8, %rsp
	movl	%eax, %r8d
	leal	-1(%rbx), %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	imull	$354, %eax, %eax
	addl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L101:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	leaq	_ZN6icu_67L26umAlQuraYrStartEstimateFixE(%rip), %rcx
	cvtsi2sdl	%edx, %xmm0
	mulsd	.LC10(%rip), %xmm0
	movslq	%edx, %rdx
	addsd	.LC11(%rip), %xmm0
	addsd	.LC12(%rip), %xmm0
	movsbl	(%rcx,%rdx), %ebx
	addq	$8, %rsp
	cvttsd2sil	%xmm0, %eax
	addl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3117:
	.size	_ZNK6icu_6715IslamicCalendar9yearStartEi, .-_ZNK6icu_6715IslamicCalendar9yearStartEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715IslamicCalendar20handleGetMonthLengthEii
	.type	_ZNK6icu_6715IslamicCalendar20handleGetMonthLengthEii, @function
_ZNK6icu_6715IslamicCalendar20handleGetMonthLengthEii:
.LFB3127:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	612(%rdi), %eax
	movl	%eax, %ecx
	andl	$-3, %ecx
	cmpl	$1, %ecx
	je	.L111
	cmpl	$2, %eax
	je	.L121
	movq	%rdi, %r12
	leal	-1300(%rsi), %edi
	testl	%eax, %eax
	je	.L122
.L113:
	movl	$11, %ecx
	movl	$1, %eax
	movslq	%edi, %rdi
	xorl	%r8d, %r8d
	subl	%edx, %ecx
	leaq	_ZN6icu_67L20UMALQURA_MONTHLENGTHE(%rip), %rdx
	sall	%cl, %eax
	testl	%eax, (%rdx,%rdi,4)
	setne	%r8b
	addl	$29, %r8d
.L110:
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L121:
	.cfi_restore_state
	leal	-1300(%rsi), %edi
	cmpl	$300, %edi
	jbe	.L113
	.p2align 4,,10
	.p2align 3
.L111:
	leal	1(%rdx), %eax
	movl	%eax, %ecx
	shrl	$31, %ecx
	addl	%ecx, %eax
	andl	$1, %eax
	subl	%ecx, %eax
	leal	29(%rax), %r8d
	cmpl	$11, %edx
	jne	.L110
	leal	(%rsi,%rsi,4), %edx
	addl	$30, %eax
	leal	14(%rsi,%rdx,2), %ecx
	movslq	%ecx, %rdx
	movl	%ecx, %esi
	imulq	$-2004318071, %rdx, %rdx
	sarl	$31, %esi
	shrq	$32, %rdx
	addl	%ecx, %edx
	sarl	$4, %edx
	subl	%esi, %edx
	imull	$30, %edx, %edx
	subl	%edx, %ecx
	cmpl	$11, %ecx
	cmovl	%eax, %r8d
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	movl	%r8d, %eax
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L122:
	.cfi_restore_state
	leal	-3(%rsi,%rsi,2), %eax
	movq	%r12, %rdi
	leal	(%rdx,%rax,4), %r13d
	leal	1(%r13), %esi
	call	_ZNK6icu_6715IslamicCalendar14trueMonthStartEi
	movl	%r13d, %esi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZNK6icu_6715IslamicCalendar14trueMonthStartEi
	addq	$8, %rsp
	subl	%eax, %ebx
	movl	%ebx, %r8d
	popq	%rbx
	popq	%r12
	movl	%r8d, %eax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3127:
	.size	_ZNK6icu_6715IslamicCalendar20handleGetMonthLengthEii, .-_ZNK6icu_6715IslamicCalendar20handleGetMonthLengthEii
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715IslamicCalendar19handleGetYearLengthEi
	.type	_ZNK6icu_6715IslamicCalendar19handleGetYearLengthEi, @function
_ZNK6icu_6715IslamicCalendar19handleGetYearLengthEi:
.LFB3128:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%esi, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	612(%rdi), %eax
	movl	%eax, %edx
	andl	$-3, %edx
	cmpl	$1, %edx
	je	.L124
	movq	%rdi, %r14
	cmpl	$2, %eax
	je	.L162
	testl	%eax, %eax
	jne	.L163
	leal	-3(%rsi,%rsi,2), %r13d
	sall	$2, %r13d
	leal	12(%r13), %esi
	call	_ZNK6icu_6715IslamicCalendar14trueMonthStartEi
	movl	%r13d, %esi
	movq	%r14, %rdi
	movl	%eax, %r12d
	call	_ZNK6icu_6715IslamicCalendar14trueMonthStartEi
	subl	%eax, %r12d
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L124:
	leal	(%r15,%r15,4), %eax
	xorl	%r12d, %r12d
	leal	14(%r15,%rax,2), %edx
	movslq	%edx, %rax
	movl	%edx, %ecx
	imulq	$-2004318071, %rax, %rax
	sarl	$31, %ecx
	shrq	$32, %rax
	addl	%edx, %eax
	sarl	$4, %eax
	subl	%ecx, %eax
	imull	$30, %eax, %eax
	subl	%eax, %edx
	cmpl	$11, %edx
	setl	%r12b
	addl	$354, %r12d
.L123:
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L162:
	.cfi_restore_state
	leal	-1300(%rsi), %eax
	movl	%eax, -52(%rbp)
	cmpl	$300, %eax
	ja	.L124
	leal	-3(%rsi,%rsi,2), %ebx
	leal	0(,%rbx,4), %eax
	movl	%eax, -56(%rbp)
.L130:
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
	leaq	_ZNK6icu_6715IslamicCalendar20handleGetMonthLengthEii(%rip), %rbx
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L133:
	testl	%eax, %eax
	je	.L164
	movslq	-52(%rbp), %rdx
	movl	$12, %ecx
	movl	$1, %eax
	leaq	_ZN6icu_67L20UMALQURA_MONTHLENGTHE(%rip), %rdi
	subl	%r13d, %ecx
	sall	%cl, %eax
	testl	%eax, (%rdi,%rdx,4)
	setne	%al
	movzbl	%al, %eax
	addl	$29, %eax
.L132:
	addl	%eax, %r12d
	cmpl	$12, %r13d
	je	.L123
.L136:
	movq	(%r14), %rax
	movl	%r13d, %edx
	addl	$1, %r13d
	movq	272(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L165
	movl	612(%r14), %eax
	movl	%eax, %ecx
	andl	$-3, %ecx
	cmpl	$1, %ecx
	jne	.L133
	movl	%r13d, %ecx
	andl	$1, %ecx
	leal	29(%rcx), %eax
	cmpl	$11, %edx
	jne	.L132
.L134:
	leal	(%r15,%r15,4), %edx
	addl	$30, %ecx
	leal	14(%r15,%rdx,2), %esi
	movslq	%esi, %rdx
	movl	%esi, %edi
	imulq	$-2004318071, %rdx, %rdx
	sarl	$31, %edi
	shrq	$32, %rdx
	addl	%esi, %edx
	sarl	$4, %edx
	subl	%edi, %edx
	imull	$30, %edx, %edx
	subl	%edx, %esi
	cmpl	$11, %esi
	cmovl	%ecx, %eax
	addl	%eax, %r12d
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L165:
	movl	%r15d, %esi
	movq	%r14, %rdi
	call	*%rax
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L164:
	movl	-56(%rbp), %eax
	movq	%r14, %rdi
	leal	0(%r13,%rax), %esi
	call	_ZNK6icu_6715IslamicCalendar14trueMonthStartEi
	movl	-56(%rbp), %edi
	movl	%eax, -60(%rbp)
	leal	-1(%r13,%rdi), %esi
	movq	%r14, %rdi
	call	_ZNK6icu_6715IslamicCalendar14trueMonthStartEi
	movl	-60(%rbp), %esi
	subl	%eax, %esi
	movl	%esi, %eax
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L163:
	leal	-3(%rsi,%rsi,2), %eax
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
	sall	$2, %eax
	leaq	_ZNK6icu_6715IslamicCalendar20handleGetMonthLengthEii(%rip), %rbx
	movl	%eax, -60(%rbp)
	movl	%eax, -56(%rbp)
	leal	-1300(%rsi), %eax
	movl	%eax, -52(%rbp)
	cmpl	$300, %eax
	ja	.L129
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L167:
	cmpl	$2, %eax
	je	.L138
	testl	%eax, %eax
	je	.L166
	movslq	-52(%rbp), %rdx
	movl	$12, %ecx
	movl	$1, %eax
	leaq	_ZN6icu_67L20UMALQURA_MONTHLENGTHE(%rip), %rdi
	subl	%r13d, %ecx
	sall	%cl, %eax
	testl	%eax, (%rdi,%rdx,4)
	setne	%al
	movzbl	%al, %eax
	addl	$29, %eax
.L140:
	addl	%eax, %r12d
	cmpl	$12, %r13d
	je	.L123
.L129:
	movq	(%r14), %rax
	movl	%r13d, %edx
	addl	$1, %r13d
	movq	272(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L137
	movl	612(%r14), %eax
	movl	%eax, %ecx
	andl	$-3, %ecx
	cmpl	$1, %ecx
	jne	.L167
.L138:
	movl	%r13d, %ecx
	andl	$1, %ecx
	leal	29(%rcx), %eax
	cmpl	$11, %edx
	jne	.L140
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L137:
	movl	%r15d, %esi
	movq	%r14, %rdi
	call	*%rax
	jmp	.L140
	.p2align 4,,10
	.p2align 3
.L166:
	movl	-60(%rbp), %eax
	movq	%r14, %rdi
	leal	(%rax,%r13), %esi
	call	_ZNK6icu_6715IslamicCalendar14trueMonthStartEi
	movl	-60(%rbp), %edi
	movl	%eax, -56(%rbp)
	leal	-1(%r13,%rdi), %esi
	movq	%r14, %rdi
	call	_ZNK6icu_6715IslamicCalendar14trueMonthStartEi
	movl	-56(%rbp), %esi
	subl	%eax, %esi
	movl	%esi, %eax
	jmp	.L140
	.cfi_endproc
.LFE3128:
	.size	_ZNK6icu_6715IslamicCalendar19handleGetYearLengthEi, .-_ZNK6icu_6715IslamicCalendar19handleGetYearLengthEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715IslamicCalendar10monthStartEii
	.type	_ZNK6icu_6715IslamicCalendar10monthStartEii, @function
_ZNK6icu_6715IslamicCalendar10monthStartEii:
.LFB3118:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	612(%rdi), %eax
	movl	%eax, %edx
	andl	$-3, %edx
	cmpl	$1, %edx
	je	.L214
	movq	%rdi, %r14
	testl	%eax, %eax
	jne	.L171
	addq	$40, %rsp
	leal	-3(%rsi,%rsi,2), %eax
	popq	%rbx
	leal	(%r8,%rax,4), %esi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_6715IslamicCalendar14trueMonthStartEi
	.p2align 4,,10
	.p2align 3
.L171:
	.cfi_restore_state
	movl	%r8d, -52(%rbp)
	call	_ZNK6icu_6715IslamicCalendar9yearStartEi
	movl	-52(%rbp), %r8d
	movl	%eax, %r12d
	testl	%r8d, %r8d
	jle	.L168
	leal	-3(%r13,%r13,2), %eax
	leal	-1300(%r13), %edi
	xorl	%r15d, %r15d
	sall	$2, %eax
	movl	%edi, -52(%rbp)
	leaq	_ZNK6icu_6715IslamicCalendar20handleGetMonthLengthEii(%rip), %r9
	movl	%eax, -56(%rbp)
	leal	0(%r13,%r13,4), %eax
	leal	14(%r13,%rax,2), %edx
	movslq	%edx, %rax
	movl	%edx, %ecx
	imulq	$-2004318071, %rax, %rax
	sarl	$31, %ecx
	shrq	$32, %rax
	addl	%edx, %eax
	sarl	$4, %eax
	subl	%ecx, %eax
	imull	$30, %eax, %eax
	subl	%eax, %edx
	cmpl	$300, %edi
	jbe	.L215
	cmpl	$10, %edx
	jle	.L184
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L187:
	cmpl	$2, %eax
	je	.L188
	testl	%eax, %eax
	je	.L216
	movslq	-52(%rbp), %rdx
	movl	$12, %ecx
	movl	$1, %eax
	xorl	%ebx, %ebx
	subl	%r15d, %ecx
	leaq	_ZN6icu_67L20UMALQURA_MONTHLENGTHE(%rip), %rdi
	sall	%cl, %eax
	testl	%eax, (%rdi,%rdx,4)
	setne	%bl
	addl	$29, %ebx
.L186:
	addl	%ebx, %r12d
	cmpl	%r15d, %r8d
	je	.L168
.L191:
	movq	(%r14), %rax
	movl	%r15d, %edx
	addl	$1, %r15d
	movq	272(%rax), %rax
	cmpq	%r9, %rax
	jne	.L217
	movl	612(%r14), %eax
	movl	%eax, %edx
	andl	$-3, %edx
	cmpl	$1, %edx
	jne	.L187
.L188:
	movl	%r15d, %ebx
	andl	$1, %ebx
	addl	$29, %ebx
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L214:
	pxor	%xmm0, %xmm0
	leal	-1(%r13), %r12d
	cvtsi2sdl	%r8d, %xmm0
	mulsd	.LC13(%rip), %xmm0
	imull	$354, %r12d, %r12d
	call	uprv_ceil_67@PLT
	movl	$30, %esi
	cvttsd2sil	%xmm0, %eax
	addl	%eax, %r12d
	leal	0(%r13,%r13,4), %eax
	leal	3(%r13,%rax,2), %edi
	call	_ZN6icu_679ClockMath11floorDivideEii@PLT
	addl	%eax, %r12d
.L168:
	addq	$40, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L219:
	.cfi_restore_state
	cmpl	$2, %eax
	je	.L193
	testl	%eax, %eax
	je	.L218
	movl	$12, %ecx
	movslq	-52(%rbp), %rdx
	movl	$1, %eax
	xorl	%ebx, %ebx
	subl	%r15d, %ecx
	sall	%cl, %eax
	leaq	_ZN6icu_67L20UMALQURA_MONTHLENGTHE(%rip), %rcx
	testl	%eax, (%rcx,%rdx,4)
	setne	%bl
	addl	$29, %ebx
.L195:
	addl	%ebx, %r12d
	cmpl	%r15d, %r8d
	je	.L168
.L184:
	movq	(%r14), %rax
	movl	%r15d, %edx
	addl	$1, %r15d
	movq	272(%rax), %rax
	cmpq	%r9, %rax
	jne	.L192
	movl	612(%r14), %eax
	movl	%eax, %ecx
	andl	$-3, %ecx
	cmpl	$1, %ecx
	jne	.L219
.L193:
	movl	%r15d, %ebx
	movl	$30, %eax
	andl	$1, %ebx
	addl	$29, %ebx
	cmpl	$11, %edx
	cmove	%eax, %ebx
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L215:
	cmpl	$10, %edx
	jle	.L173
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L221:
	testl	%eax, %eax
	je	.L220
	movslq	-52(%rbp), %rdx
	movl	$12, %ecx
	movl	$1, %eax
	xorl	%ebx, %ebx
	subl	%r15d, %ecx
	leaq	_ZN6icu_67L20UMALQURA_MONTHLENGTHE(%rip), %rsi
	sall	%cl, %eax
	testl	%eax, (%rsi,%rdx,4)
	setne	%bl
	addl	$29, %ebx
.L177:
	addl	%ebx, %r12d
	cmpl	%r15d, %r8d
	je	.L168
.L179:
	movq	(%r14), %rax
	movl	%r15d, %edx
	addl	$1, %r15d
	movq	272(%rax), %rax
	cmpq	%r9, %rax
	jne	.L174
	movl	612(%r14), %eax
	movl	%eax, %edx
	andl	$-3, %edx
	cmpl	$1, %edx
	jne	.L221
	movl	%r15d, %ebx
	andl	$1, %ebx
	addl	$29, %ebx
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L182:
	testl	%eax, %eax
	je	.L222
	movslq	-52(%rbp), %rdx
	movl	$12, %ecx
	movl	$1, %eax
	xorl	%ebx, %ebx
	subl	%r15d, %ecx
	leaq	_ZN6icu_67L20UMALQURA_MONTHLENGTHE(%rip), %rsi
	sall	%cl, %eax
	testl	%eax, (%rsi,%rdx,4)
	setne	%bl
	addl	$29, %ebx
.L181:
	addl	%ebx, %r12d
	cmpl	%r15d, %r8d
	je	.L168
.L173:
	movq	(%r14), %rax
	movl	%r15d, %edx
	addl	$1, %r15d
	movq	272(%rax), %rax
	cmpq	%r9, %rax
	jne	.L223
	movl	612(%r14), %eax
	movl	%eax, %ecx
	andl	$-3, %ecx
	cmpl	$1, %ecx
	jne	.L182
	movl	%r15d, %ebx
	movl	$30, %eax
	andl	$1, %ebx
	addl	$29, %ebx
	cmpl	$11, %edx
	cmove	%eax, %ebx
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L223:
	movq	%r9, -72(%rbp)
	movl	%r13d, %esi
	movq	%r14, %rdi
	movl	%r8d, -60(%rbp)
	call	*%rax
	movl	-60(%rbp), %r8d
	movq	-72(%rbp), %r9
	movl	%eax, %ebx
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L174:
	movq	%r9, -72(%rbp)
	movl	%r13d, %esi
	movq	%r14, %rdi
	movl	%r8d, -60(%rbp)
	call	*%rax
	movq	-72(%rbp), %r9
	movl	-60(%rbp), %r8d
	movl	%eax, %ebx
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L217:
	movq	%r9, -72(%rbp)
	movl	%r13d, %esi
	movq	%r14, %rdi
	movl	%r8d, -60(%rbp)
	call	*%rax
	movl	-60(%rbp), %r8d
	movq	-72(%rbp), %r9
	movl	%eax, %ebx
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L192:
	movq	%r9, -72(%rbp)
	movl	%r13d, %esi
	movq	%r14, %rdi
	movl	%r8d, -60(%rbp)
	call	*%rax
	movq	-72(%rbp), %r9
	movl	-60(%rbp), %r8d
	movl	%eax, %ebx
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L216:
	movl	-56(%rbp), %eax
	movq	%r14, %rdi
	movq	%r9, -72(%rbp)
	movl	%r8d, -60(%rbp)
	leal	(%rax,%r15), %esi
	call	_ZNK6icu_6715IslamicCalendar14trueMonthStartEi
	movq	%r14, %rdi
	movl	%eax, %ebx
	movl	-56(%rbp), %eax
	leal	-1(%r15,%rax), %esi
	call	_ZNK6icu_6715IslamicCalendar14trueMonthStartEi
	movq	-72(%rbp), %r9
	movl	-60(%rbp), %r8d
	subl	%eax, %ebx
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L218:
	movl	-56(%rbp), %eax
	movq	%r14, %rdi
	movq	%r9, -72(%rbp)
	movl	%r8d, -60(%rbp)
	leal	(%rax,%r15), %esi
	call	_ZNK6icu_6715IslamicCalendar14trueMonthStartEi
	movq	%r14, %rdi
	movl	%eax, %ebx
	movl	-56(%rbp), %eax
	leal	-1(%r15,%rax), %esi
	call	_ZNK6icu_6715IslamicCalendar14trueMonthStartEi
	movl	-60(%rbp), %r8d
	movq	-72(%rbp), %r9
	subl	%eax, %ebx
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L220:
	movl	-56(%rbp), %eax
	movq	%r14, %rdi
	movq	%r9, -72(%rbp)
	movl	%r8d, -60(%rbp)
	leal	(%rax,%r15), %esi
	call	_ZNK6icu_6715IslamicCalendar14trueMonthStartEi
	movq	%r14, %rdi
	movl	%eax, %ebx
	movl	-56(%rbp), %eax
	leal	-1(%r15,%rax), %esi
	call	_ZNK6icu_6715IslamicCalendar14trueMonthStartEi
	movl	-60(%rbp), %r8d
	movq	-72(%rbp), %r9
	subl	%eax, %ebx
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L222:
	movl	-56(%rbp), %eax
	movq	%r14, %rdi
	movq	%r9, -72(%rbp)
	movl	%r8d, -60(%rbp)
	leal	(%rax,%r15), %esi
	call	_ZNK6icu_6715IslamicCalendar14trueMonthStartEi
	movq	%r14, %rdi
	movl	%eax, %ebx
	movl	-56(%rbp), %eax
	leal	-1(%r15,%rax), %esi
	call	_ZNK6icu_6715IslamicCalendar14trueMonthStartEi
	movq	-72(%rbp), %r9
	movl	-60(%rbp), %r8d
	subl	%eax, %ebx
	jmp	.L181
	.cfi_endproc
.LFE3118:
	.size	_ZNK6icu_6715IslamicCalendar10monthStartEii, .-_ZNK6icu_6715IslamicCalendar10monthStartEii
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715IslamicCalendar23handleComputeMonthStartEiia
	.type	_ZNK6icu_6715IslamicCalendar23handleComputeMonthStartEiia, @function
_ZNK6icu_6715IslamicCalendar23handleComputeMonthStartEiia:
.LFB3129:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$40, %rsp
	cmpl	$11, %edx
	jle	.L225
	movslq	%edx, %rax
	sarl	$31, %edx
	imulq	$715827883, %rax, %rax
	sarq	$33, %rax
	subl	%edx, %eax
	addl	%eax, %r13d
	leal	(%rax,%rax,2), %eax
	sall	$2, %eax
	subl	%eax, %ebx
.L226:
	movl	612(%r12), %eax
	movl	%eax, %edx
	andl	$-3, %edx
	cmpl	$1, %edx
	je	.L258
	testl	%eax, %eax
	jne	.L229
	leal	-3(%r13,%r13,2), %eax
	movq	%r12, %rdi
	leal	(%rbx,%rax,4), %esi
	call	_ZNK6icu_6715IslamicCalendar14trueMonthStartEi
	movl	%eax, %r15d
.L228:
	xorl	%eax, %eax
	cmpl	$3, 612(%r12)
	setne	%al
	addq	$40, %rsp
	popq	%rbx
	leal	1948438(%rax,%r15), %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L225:
	.cfi_restore_state
	testl	%edx, %edx
	jns	.L226
	addl	$1, %ebx
	movslq	%ebx, %rax
	movl	%ebx, %edx
	imulq	$715827883, %rax, %rax
	sarl	$31, %edx
	sarq	$33, %rax
	subl	%edx, %eax
	leal	-1(%rsi,%rax), %r13d
	leal	(%rax,%rax,2), %eax
	sall	$2, %eax
	subl	%eax, %ebx
	addl	$11, %ebx
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L229:
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_6715IslamicCalendar9yearStartEi
	movl	%eax, %r15d
	testl	%ebx, %ebx
	je	.L228
	leal	-3(%r13,%r13,2), %eax
	xorl	%r14d, %r14d
	leaq	_ZNK6icu_6715IslamicCalendar20handleGetMonthLengthEii(%rip), %r8
	sall	$2, %eax
	movl	%eax, -56(%rbp)
	leal	-1300(%r13), %eax
	movl	%eax, -52(%rbp)
	cmpl	$300, %eax
	ja	.L230
	.p2align 4,,10
	.p2align 3
.L236:
	movq	(%r12), %rax
	movl	%r14d, %edx
	addl	$1, %r14d
	movq	272(%rax), %rax
	cmpq	%r8, %rax
	jne	.L259
	movl	612(%r12), %eax
	movl	%eax, %edx
	andl	$-3, %edx
	cmpl	$1, %edx
	je	.L260
	testl	%eax, %eax
	je	.L261
	movl	$12, %ecx
	movslq	-52(%rbp), %rax
	movl	$1, %edx
	subl	%r14d, %ecx
	sall	%cl, %edx
	leaq	_ZN6icu_67L20UMALQURA_MONTHLENGTHE(%rip), %rcx
	testl	%edx, (%rcx,%rax,4)
	setne	%al
	movzbl	%al, %eax
	addl	$29, %eax
.L232:
	addl	%eax, %r15d
	cmpl	%r14d, %ebx
	jne	.L236
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L238:
	movl	%r14d, %eax
	andl	$1, %eax
	addl	$29, %eax
.L240:
	addl	%eax, %r15d
	cmpl	%r14d, %ebx
	je	.L228
.L230:
	movq	(%r12), %rax
	movl	%r14d, %edx
	addl	$1, %r14d
	movq	272(%rax), %rax
	cmpq	%r8, %rax
	jne	.L237
	movl	612(%r12), %eax
	movl	%eax, %edx
	andl	$-3, %edx
	cmpl	$1, %edx
	je	.L238
	cmpl	$2, %eax
	je	.L238
	testl	%eax, %eax
	je	.L262
	movl	$12, %ecx
	movslq	-52(%rbp), %rdx
	movl	$1, %eax
	subl	%r14d, %ecx
	sall	%cl, %eax
	leaq	_ZN6icu_67L20UMALQURA_MONTHLENGTHE(%rip), %rcx
	testl	%eax, (%rcx,%rdx,4)
	setne	%al
	movzbl	%al, %eax
	addl	$29, %eax
	jmp	.L240
	.p2align 4,,10
	.p2align 3
.L258:
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%ebx, %xmm0
	mulsd	.LC13(%rip), %xmm0
	call	uprv_ceil_67@PLT
	leal	-1(%r13), %r8d
	movl	$30, %esi
	cvttsd2sil	%xmm0, %eax
	imull	$354, %r8d, %r8d
	leal	(%r8,%rax), %r15d
	leal	0(%r13,%r13,4), %eax
	leal	3(%r13,%rax,2), %edi
	call	_ZN6icu_679ClockMath11floorDivideEii@PLT
	addl	%eax, %r15d
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L260:
	movl	%r14d, %eax
	andl	$1, %eax
	addl	$29, %eax
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L262:
	movl	-56(%rbp), %eax
	movq	%r12, %rdi
	movq	%r8, -72(%rbp)
	leal	(%rax,%r14), %esi
	call	_ZNK6icu_6715IslamicCalendar14trueMonthStartEi
	movl	-56(%rbp), %ecx
	movq	%r12, %rdi
	movl	%eax, -64(%rbp)
	leal	-1(%r14,%rcx), %esi
	call	_ZNK6icu_6715IslamicCalendar14trueMonthStartEi
	movl	-64(%rbp), %ecx
	movq	-72(%rbp), %r8
	subl	%eax, %ecx
	movl	%ecx, %eax
	jmp	.L240
	.p2align 4,,10
	.p2align 3
.L261:
	movl	-56(%rbp), %eax
	movq	%r12, %rdi
	movq	%r8, -72(%rbp)
	leal	(%rax,%r14), %esi
	call	_ZNK6icu_6715IslamicCalendar14trueMonthStartEi
	movl	-56(%rbp), %ecx
	movq	%r12, %rdi
	movl	%eax, -64(%rbp)
	leal	-1(%r14,%rcx), %esi
	call	_ZNK6icu_6715IslamicCalendar14trueMonthStartEi
	movl	-64(%rbp), %ecx
	movq	-72(%rbp), %r8
	subl	%eax, %ecx
	movl	%ecx, %eax
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L237:
	movq	%r8, -64(%rbp)
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	*%rax
	movq	-64(%rbp), %r8
	jmp	.L240
	.p2align 4,,10
	.p2align 3
.L259:
	movq	%r8, -64(%rbp)
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	*%rax
	movq	-64(%rbp), %r8
	jmp	.L232
	.cfi_endproc
.LFE3129:
	.size	_ZNK6icu_6715IslamicCalendar23handleComputeMonthStartEiia, .-_ZNK6icu_6715IslamicCalendar23handleComputeMonthStartEiia
	.section	.text.unlikely
	.align 2
.LCOLDB15:
	.text
.LHOTB15:
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715IslamicCalendar19handleComputeFieldsEiR10UErrorCode
	.type	_ZN6icu_6715IslamicCalendar19handleComputeFieldsEiR10UErrorCode, @function
_ZN6icu_6715IslamicCalendar19handleComputeFieldsEiR10UErrorCode:
.LFB3131:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leal	-1948440(%rsi), %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	612(%rdi), %eax
	movl	%eax, %edx
	andl	$-3, %edx
	cmpl	$1, %edx
	jne	.L264
	subl	$1948439, %r12d
	cmpl	$3, %eax
	movl	$10631, %esi
	cmove	%r12d, %r13d
	movslq	%r13d, %rdx
	movq	%rdx, %rax
	salq	$4, %rax
	subq	%rdx, %rax
	leaq	10646(%rax,%rax), %rdi
	call	_ZN6icu_679ClockMath11floorDivideEll@PLT
	movq	%rbx, %rdi
	movl	%eax, %esi
	movq	%rax, %r15
	movl	%eax, %r14d
	call	_ZNK6icu_6715IslamicCalendar9yearStartEi
	pxor	%xmm0, %xmm0
	movl	%eax, %r8d
	leal	-29(%r13), %eax
	subl	%r8d, %eax
	cvtsi2sdl	%eax, %xmm0
	divsd	.LC13(%rip), %xmm0
	call	uprv_ceil_67@PLT
	movl	$11, %eax
	movl	%r15d, %esi
	movq	%rbx, %rdi
	cvttsd2sil	%xmm0, %r12d
	cmpl	$11, %r12d
	cmovg	%eax, %r12d
	movl	%r12d, %edx
	call	_ZNK6icu_6715IslamicCalendar10monthStartEii
.L266:
	movl	%r12d, %edx
	movl	%r14d, %esi
	movq	%rbx, %rdi
	movl	%r13d, %r15d
	call	_ZNK6icu_6715IslamicCalendar10monthStartEii
	subl	%eax, %r15d
	movl	612(%rbx), %eax
	addl	$1, %r15d
	movl	%eax, %edx
	andl	$-3, %edx
	cmpl	$1, %edx
	je	.L296
	testl	%eax, %eax
	jne	.L284
	leal	-3(%r14,%r14,2), %esi
	movq	%rbx, %rdi
	sall	$2, %esi
	call	_ZNK6icu_6715IslamicCalendar14trueMonthStartEi
.L283:
	subl	%eax, %r13d
	movl	$257, %ecx
	movl	$257, %esi
	movabsq	$4294967297, %rdx
	addl	$1, %r13d
	movl	$0, 12(%rbx)
	movl	%r14d, 16(%rbx)
	movl	%r14d, 88(%rbx)
	movl	$1, 204(%rbx)
	movb	$1, 123(%rbx)
	movl	%r12d, 20(%rbx)
	movq	%rdx, 128(%rbx)
	movl	$1, 136(%rbx)
	movw	%cx, 104(%rbx)
	movb	$1, 106(%rbx)
	movl	%r15d, 32(%rbx)
	movl	%r13d, 36(%rbx)
	movq	%rdx, 148(%rbx)
	movw	%si, 109(%rbx)
.L263:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L284:
	.cfi_restore_state
	movl	%r14d, %esi
	movq	%rbx, %rdi
	call	_ZNK6icu_6715IslamicCalendar9yearStartEi
	jmp	.L283
	.p2align 4,,10
	.p2align 3
.L264:
	testl	%eax, %eax
	je	.L297
	cmpl	$2, %eax
	jne	.L274
	cmpl	$460321, %r13d
	jg	.L285
	imull	$30, %r13d, %eax
	pxor	%xmm0, %xmm0
	subl	$1948469, %r12d
	addl	$10646, %eax
	cvtsi2sdl	%eax, %xmm0
	divsd	.LC14(%rip), %xmm0
	call	uprv_floor_67@PLT
	movq	%rbx, %rdi
	cvttsd2sil	%xmm0, %r14d
	movl	%r14d, %esi
	call	_ZNK6icu_6715IslamicCalendar9yearStartEi
	pxor	%xmm0, %xmm0
	subl	%eax, %r12d
	cvtsi2sdl	%r12d, %xmm0
	divsd	.LC13(%rip), %xmm0
	call	uprv_ceil_67@PLT
	movl	$11, %eax
	movl	%r14d, %esi
	movq	%rbx, %rdi
	cvttsd2sil	%xmm0, %r12d
	cmpl	$11, %r12d
	cmovg	%eax, %r12d
	movl	%r12d, %edx
	call	_ZNK6icu_6715IslamicCalendar10monthStartEii
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L296:
	pxor	%xmm0, %xmm0
	call	uprv_ceil_67@PLT
	leal	-1(%r14), %eax
	movl	$30, %esi
	imull	$354, %eax, %edx
	cvttsd2sil	%xmm0, %eax
	addl	%edx, %eax
	movl	%eax, -56(%rbp)
	leal	(%r14,%r14,4), %eax
	leal	3(%r14,%rax,2), %edi
	call	_ZN6icu_679ClockMath11floorDivideEii@PLT
	addl	-56(%rbp), %eax
	jmp	.L283
	.p2align 4,,10
	.p2align 3
.L297:
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%r13d, %xmm0
	divsd	_ZN6icu_6718CalendarAstronomer13SYNODIC_MONTHE(%rip), %xmm0
	call	uprv_floor_67@PLT
	cvttsd2sil	%xmm0, %r12d
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%r12d, %xmm0
	mulsd	_ZN6icu_6718CalendarAstronomer13SYNODIC_MONTHE(%rip), %xmm0
	call	uprv_floor_67@PLT
	movq	%r14, %rdi
	movsd	%xmm0, -56(%rbp)
	movsd	224(%rbx), %xmm0
	call	_ZN6icu_6715IslamicCalendar7moonAgeEdR10UErrorCode
	movl	(%r14), %edi
	testl	%edi, %edi
	jg	.L298
	cvttsd2sil	-56(%rbp), %eax
	movl	%r13d, %ecx
	subl	%eax, %ecx
	cmpl	$24, %ecx
	jle	.L273
	xorl	%eax, %eax
	comisd	.LC5(%rip), %xmm0
	seta	%al
	addl	%eax, %r12d
	jmp	.L273
	.p2align 4,,10
	.p2align 3
.L299:
	subl	$1, %r12d
.L273:
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	_ZNK6icu_6715IslamicCalendar14trueMonthStartEi
	cmpl	%eax, %r13d
	jl	.L299
	movslq	%r12d, %rax
	movl	%r12d, %edx
	imulq	$715827883, %rax, %rax
	sarl	$31, %edx
	sarq	$33, %rax
	subl	%edx, %eax
	leal	1(%rax), %r14d
	leal	(%rax,%rax,2), %eax
	sall	$2, %eax
	subl	%eax, %r12d
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L285:
	movl	$1299, %r14d
	.p2align 4,,10
	.p2align 3
.L275:
	movl	%r14d, %r12d
	addl	$1, %r14d
	movq	%rbx, %rdi
	movl	%r13d, %r15d
	movl	%r14d, %esi
	call	_ZNK6icu_6715IslamicCalendar9yearStartEi
	movl	%r14d, %esi
	movq	%rbx, %rdi
	subl	%eax, %r15d
	addl	$1, %r15d
	movslq	%r15d, %rax
	movq	%rax, -56(%rbp)
	movq	(%rbx), %rax
	call	*280(%rax)
	cmpl	%eax, %r15d
	je	.L287
	movq	(%rbx), %rax
	movl	%r14d, %esi
	movq	%rbx, %rdi
	call	*280(%rax)
	cmpl	%eax, %r15d
	jl	.L300
	testl	%r15d, %r15d
	jg	.L275
	xorl	%r12d, %r12d
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L298:
	movl	$7, (%r14)
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L287:
	movl	$11, %r12d
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L300:
	movq	(%rbx), %rax
	leaq	_ZNK6icu_6715IslamicCalendar20handleGetMonthLengthEii(%rip), %rdx
	movq	272(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L277
	movl	612(%rbx), %edx
	movl	$30, %eax
	movl	%edx, %esi
	andl	$-3, %esi
	cmpl	$1, %esi
	je	.L278
	leal	-1299(%r12), %esi
	cmpl	$2, %edx
	je	.L301
	testl	%edx, %edx
	je	.L302
.L280:
	leaq	_ZN6icu_67L20UMALQURA_MONTHLENGTHE(%rip), %rax
	movslq	%esi, %rsi
	testb	$8, 1(%rax,%rsi,4)
	setne	%al
	movzbl	%al, %eax
	addq	$29, %rax
.L278:
	xorl	%r12d, %r12d
	cmpq	%rax, -56(%rbp)
	jle	.L266
	.p2align 4,,10
	.p2align 3
.L281:
	subq	%rax, -56(%rbp)
	movq	(%rbx), %rax
	addl	$1, %r12d
	movl	%r14d, %esi
	movq	-56(%rbp), %r15
	movl	%r12d, %edx
	movq	%rbx, %rdi
	call	*272(%rax)
	cltq
	cmpq	%r15, %rax
	jl	.L281
	jmp	.L266
.L277:
	xorl	%edx, %edx
	movl	%r14d, %esi
	movq	%rbx, %rdi
	call	*%rax
	cltq
	jmp	.L278
.L301:
	cmpl	$300, %esi
	ja	.L278
	jmp	.L280
.L302:
	leal	(%r12,%r12,2), %r15d
	movq	%rbx, %rdi
	sall	$2, %r15d
	leal	1(%r15), %esi
	call	_ZNK6icu_6715IslamicCalendar14trueMonthStartEi
	movl	%r15d, %esi
	movq	%rbx, %rdi
	movl	%eax, %r12d
	call	_ZNK6icu_6715IslamicCalendar14trueMonthStartEi
	subl	%eax, %r12d
	movslq	%r12d, %rax
	jmp	.L278
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_6715IslamicCalendar19handleComputeFieldsEiR10UErrorCode.cold, @function
_ZN6icu_6715IslamicCalendar19handleComputeFieldsEiR10UErrorCode.cold:
.LFSB3131:
.L274:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	call	abort@PLT
	.cfi_endproc
.LFE3131:
	.text
	.size	_ZN6icu_6715IslamicCalendar19handleComputeFieldsEiR10UErrorCode, .-_ZN6icu_6715IslamicCalendar19handleComputeFieldsEiR10UErrorCode
	.section	.text.unlikely
	.size	_ZN6icu_6715IslamicCalendar19handleComputeFieldsEiR10UErrorCode.cold, .-_ZN6icu_6715IslamicCalendar19handleComputeFieldsEiR10UErrorCode.cold
.LCOLDE15:
	.text
.LHOTE15:
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715IslamicCalendar16getStaticClassIDEv
	.type	_ZN6icu_6715IslamicCalendar16getStaticClassIDEv, @function
_ZN6icu_6715IslamicCalendar16getStaticClassIDEv:
.LFB3137:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6715IslamicCalendar16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3137:
	.size	_ZN6icu_6715IslamicCalendar16getStaticClassIDEv, .-_ZN6icu_6715IslamicCalendar16getStaticClassIDEv
	.section	.rodata.str1.1
.LC16:
	.string	"@calendar=islamic-civil"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715IslamicCalendar30initializeSystemDefaultCenturyEv
	.type	_ZN6icu_6715IslamicCalendar30initializeSystemDefaultCenturyEv, @function
_ZN6icu_6715IslamicCalendar30initializeSystemDefaultCenturyEv:
.LFB3136:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	leaq	.LC16(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-880(%rbp), %r14
	leaq	-884(%rbp), %r13
	pushq	%r12
	movq	%r14, %rdi
	.cfi_offset 12, -40
	leaq	-656(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -48
	leaq	16+_ZTVN6icu_6715IslamicCalendarE(%rip), %rbx
	subq	$864, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$0, -884(%rbp)
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	call	_ZN6icu_678TimeZone13createDefaultEv@PLT
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_678CalendarC2EPNS_8TimeZoneERKNS_6LocaleER10UErrorCode@PLT
	movq	%rbx, -656(%rbp)
	movl	$1, -44(%rbp)
	call	_ZN6icu_678Calendar6getNowEv@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_678Calendar15setTimeInMillisEdR10UErrorCode@PLT
	movq	%r14, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	movl	-884(%rbp), %eax
	testl	%eax, %eax
	jle	.L308
.L305:
	movq	%r12, %rdi
	movq	%rbx, -656(%rbp)
	call	_ZN6icu_678CalendarD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L309
	addq	$864, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L308:
	.cfi_restore_state
	call	_ZN6icu_678Calendar6getNowEv@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_678Calendar15setTimeInMillisEdR10UErrorCode@PLT
	movq	%r13, %rcx
	movl	$-80, %edx
	movq	%r12, %rdi
	movl	$1, %esi
	call	_ZN6icu_678Calendar3addE19UCalendarDateFieldsiR10UErrorCode@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK6icu_678Calendar15getTimeInMillisER10UErrorCode@PLT
	movq	%r13, %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	movsd	%xmm0, _ZN6icu_67L26gSystemDefaultCenturyStartE(%rip)
	call	_ZNK6icu_678Calendar3getE19UCalendarDateFieldsR10UErrorCode@PLT
	movl	%eax, _ZN6icu_67L30gSystemDefaultCenturyStartYearE(%rip)
	jmp	.L305
.L309:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3136:
	.size	_ZN6icu_6715IslamicCalendar30initializeSystemDefaultCenturyEv, .-_ZN6icu_6715IslamicCalendar30initializeSystemDefaultCenturyEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715IslamicCalendar19defaultCenturyStartEv
	.type	_ZNK6icu_6715IslamicCalendar19defaultCenturyStartEv, @function
_ZNK6icu_6715IslamicCalendar19defaultCenturyStartEv:
.LFB3134:
	.cfi_startproc
	endbr64
	movl	_ZN6icu_67L25gSystemDefaultCenturyInitE(%rip), %eax
	cmpl	$2, %eax
	je	.L318
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN6icu_67L25gSystemDefaultCenturyInitE(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L312
	call	_ZN6icu_6715IslamicCalendar30initializeSystemDefaultCenturyEv
	leaq	_ZN6icu_67L25gSystemDefaultCenturyInitE(%rip), %rdi
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L312:
	movsd	_ZN6icu_67L26gSystemDefaultCenturyStartE(%rip), %xmm0
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L318:
	.cfi_restore 6
	movsd	_ZN6icu_67L26gSystemDefaultCenturyStartE(%rip), %xmm0
	ret
	.cfi_endproc
.LFE3134:
	.size	_ZNK6icu_6715IslamicCalendar19defaultCenturyStartEv, .-_ZNK6icu_6715IslamicCalendar19defaultCenturyStartEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715IslamicCalendar23defaultCenturyStartYearEv
	.type	_ZNK6icu_6715IslamicCalendar23defaultCenturyStartYearEv, @function
_ZNK6icu_6715IslamicCalendar23defaultCenturyStartYearEv:
.LFB3135:
	.cfi_startproc
	endbr64
	movl	_ZN6icu_67L25gSystemDefaultCenturyInitE(%rip), %eax
	cmpl	$2, %eax
	je	.L329
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN6icu_67L25gSystemDefaultCenturyInitE(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L323
	call	_ZN6icu_6715IslamicCalendar30initializeSystemDefaultCenturyEv
	leaq	_ZN6icu_67L25gSystemDefaultCenturyInitE(%rip), %rdi
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L323:
	movl	_ZN6icu_67L30gSystemDefaultCenturyStartYearE(%rip), %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L329:
	.cfi_restore 6
	movl	_ZN6icu_67L30gSystemDefaultCenturyStartYearE(%rip), %eax
	ret
	.cfi_endproc
.LFE3135:
	.size	_ZNK6icu_6715IslamicCalendar23defaultCenturyStartYearEv, .-_ZNK6icu_6715IslamicCalendar23defaultCenturyStartYearEv
	.weak	_ZTSN6icu_6715IslamicCalendarE
	.section	.rodata._ZTSN6icu_6715IslamicCalendarE,"aG",@progbits,_ZTSN6icu_6715IslamicCalendarE,comdat
	.align 16
	.type	_ZTSN6icu_6715IslamicCalendarE, @object
	.size	_ZTSN6icu_6715IslamicCalendarE, 27
_ZTSN6icu_6715IslamicCalendarE:
	.string	"N6icu_6715IslamicCalendarE"
	.weak	_ZTIN6icu_6715IslamicCalendarE
	.section	.data.rel.ro._ZTIN6icu_6715IslamicCalendarE,"awG",@progbits,_ZTIN6icu_6715IslamicCalendarE,comdat
	.align 8
	.type	_ZTIN6icu_6715IslamicCalendarE, @object
	.size	_ZTIN6icu_6715IslamicCalendarE, 24
_ZTIN6icu_6715IslamicCalendarE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6715IslamicCalendarE
	.quad	_ZTIN6icu_678CalendarE
	.weak	_ZTVN6icu_6715IslamicCalendarE
	.section	.data.rel.ro._ZTVN6icu_6715IslamicCalendarE,"awG",@progbits,_ZTVN6icu_6715IslamicCalendarE,comdat
	.align 8
	.type	_ZTVN6icu_6715IslamicCalendarE, @object
	.size	_ZTVN6icu_6715IslamicCalendarE, 416
_ZTVN6icu_6715IslamicCalendarE:
	.quad	0
	.quad	_ZTIN6icu_6715IslamicCalendarE
	.quad	_ZN6icu_6715IslamicCalendarD1Ev
	.quad	_ZN6icu_6715IslamicCalendarD0Ev
	.quad	_ZNK6icu_6715IslamicCalendar17getDynamicClassIDEv
	.quad	_ZNK6icu_6715IslamicCalendar5cloneEv
	.quad	_ZNK6icu_678CalendareqERKS0_
	.quad	_ZNK6icu_678Calendar14isEquivalentToERKS0_
	.quad	_ZN6icu_678Calendar3addENS0_11EDateFieldsEiR10UErrorCode
	.quad	_ZN6icu_678Calendar3addE19UCalendarDateFieldsiR10UErrorCode
	.quad	_ZN6icu_678Calendar4rollENS0_11EDateFieldsEiR10UErrorCode
	.quad	_ZN6icu_678Calendar4rollE19UCalendarDateFieldsiR10UErrorCode
	.quad	_ZN6icu_678Calendar15fieldDifferenceEdNS0_11EDateFieldsER10UErrorCode
	.quad	_ZN6icu_678Calendar15fieldDifferenceEd19UCalendarDateFieldsR10UErrorCode
	.quad	_ZNK6icu_6715IslamicCalendar14inDaylightTimeER10UErrorCode
	.quad	_ZNK6icu_678Calendar10getMinimumENS0_11EDateFieldsE
	.quad	_ZNK6icu_678Calendar10getMinimumE19UCalendarDateFields
	.quad	_ZNK6icu_678Calendar10getMaximumENS0_11EDateFieldsE
	.quad	_ZNK6icu_678Calendar10getMaximumE19UCalendarDateFields
	.quad	_ZNK6icu_678Calendar18getGreatestMinimumENS0_11EDateFieldsE
	.quad	_ZNK6icu_678Calendar18getGreatestMinimumE19UCalendarDateFields
	.quad	_ZNK6icu_678Calendar15getLeastMaximumENS0_11EDateFieldsE
	.quad	_ZNK6icu_678Calendar15getLeastMaximumE19UCalendarDateFields
	.quad	_ZNK6icu_678Calendar16getActualMinimumE19UCalendarDateFieldsR10UErrorCode
	.quad	_ZNK6icu_678Calendar16getActualMaximumE19UCalendarDateFieldsR10UErrorCode
	.quad	_ZNK6icu_6715IslamicCalendar7getTypeEv
	.quad	_ZNK6icu_678Calendar16getDayOfWeekTypeE19UCalendarDaysOfWeekR10UErrorCode
	.quad	_ZNK6icu_678Calendar20getWeekendTransitionE19UCalendarDaysOfWeekR10UErrorCode
	.quad	_ZNK6icu_678Calendar9isWeekendEdR10UErrorCode
	.quad	_ZNK6icu_678Calendar9isWeekendEv
	.quad	_ZN6icu_678Calendar11computeTimeER10UErrorCode
	.quad	_ZN6icu_678Calendar13computeFieldsER10UErrorCode
	.quad	_ZN6icu_678Calendar16prepareGetActualE19UCalendarDateFieldsaR10UErrorCode
	.quad	_ZNK6icu_6715IslamicCalendar14handleGetLimitE19UCalendarDateFieldsNS_8Calendar10ELimitTypeE
	.quad	_ZNK6icu_678Calendar8getLimitE19UCalendarDateFieldsNS0_10ELimitTypeE
	.quad	_ZNK6icu_6715IslamicCalendar23handleComputeMonthStartEiia
	.quad	_ZNK6icu_6715IslamicCalendar20handleGetMonthLengthEii
	.quad	_ZNK6icu_6715IslamicCalendar19handleGetYearLengthEi
	.quad	_ZN6icu_6715IslamicCalendar21handleGetExtendedYearEv
	.quad	_ZN6icu_678Calendar22handleComputeJulianDayE19UCalendarDateFields
	.quad	_ZN6icu_678Calendar35handleGetExtendedYearFromWeekFieldsEii
	.quad	_ZN6icu_678Calendar13validateFieldE19UCalendarDateFieldsR10UErrorCode
	.quad	_ZNK6icu_678Calendar23getFieldResolutionTableEv
	.quad	_ZN6icu_6715IslamicCalendar19handleComputeFieldsEiR10UErrorCode
	.quad	_ZN6icu_678Calendar21getDefaultMonthInYearEi
	.quad	_ZN6icu_678Calendar20getDefaultDayInMonthEii
	.quad	_ZN6icu_678Calendar8pinFieldE19UCalendarDateFieldsR10UErrorCode
	.quad	_ZNK6icu_6715IslamicCalendar18haveDefaultCenturyEv
	.quad	_ZNK6icu_6715IslamicCalendar19defaultCenturyStartEv
	.quad	_ZNK6icu_6715IslamicCalendar23defaultCenturyStartYearEv
	.quad	_ZNK6icu_678Calendar14getRelatedYearER10UErrorCode
	.quad	_ZN6icu_678Calendar14setRelatedYearEi
	.local	_ZZN6icu_6715IslamicCalendar16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6715IslamicCalendar16getStaticClassIDEvE7classID,1,1
	.local	_ZN6icu_67L25gSystemDefaultCenturyInitE
	.comm	_ZN6icu_67L25gSystemDefaultCenturyInitE,8,8
	.data
	.align 4
	.type	_ZN6icu_67L30gSystemDefaultCenturyStartYearE, @object
	.size	_ZN6icu_67L30gSystemDefaultCenturyStartYearE, 4
_ZN6icu_67L30gSystemDefaultCenturyStartYearE:
	.long	-1
	.align 8
	.type	_ZN6icu_67L26gSystemDefaultCenturyStartE, @object
	.size	_ZN6icu_67L26gSystemDefaultCenturyStartE, 8
_ZN6icu_67L26gSystemDefaultCenturyStartE:
	.long	0
	.long	1048576
	.local	_ZZN6icu_6715IslamicCalendar7moonAgeEdR10UErrorCodeE9astroLock
	.comm	_ZZN6icu_6715IslamicCalendar7moonAgeEdR10UErrorCodeE9astroLock,56,32
	.section	.rodata
	.align 32
	.type	_ZN6icu_67L26umAlQuraYrStartEstimateFixE, @object
	.size	_ZN6icu_67L26umAlQuraYrStartEstimateFixE, 301
_ZN6icu_67L26umAlQuraYrStartEstimateFixE:
	.string	""
	.string	""
	.string	"\377"
	.string	"\377"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\377"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\377"
	.string	"\001"
	.string	"\001\001"
	.string	""
	.string	""
	.string	""
	.string	"\001"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\001"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\001"
	.string	""
	.string	"\377\377"
	.string	""
	.string	""
	.string	"\001"
	.string	""
	.string	"\377"
	.string	""
	.string	""
	.string	"\001\001"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\377"
	.string	""
	.string	""
	.string	"\001\001"
	.string	""
	.string	"\377"
	.string	"\001"
	.string	"\001\001"
	.string	""
	.string	"\377"
	.string	"\001"
	.string	""
	.string	""
	.string	"\377"
	.string	"\001"
	.string	"\001"
	.string	""
	.string	""
	.string	"\377"
	.string	""
	.string	""
	.string	""
	.string	"\377\377"
	.string	"\377"
	.string	"\001"
	.string	""
	.string	""
	.string	"\377"
	.string	""
	.string	""
	.string	"\001"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\001"
	.string	""
	.string	"\377\377"
	.string	""
	.string	""
	.string	"\001"
	.string	""
	.string	"\377\377"
	.string	"\377"
	.string	""
	.string	"\377\377"
	.string	"\377"
	.string	"\377"
	.string	""
	.string	"\377\377"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\377"
	.string	"\001"
	.string	"\001\001"
	.string	""
	.string	"\377"
	.string	"\001"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\001"
	.string	"\001"
	.string	""
	.string	""
	.string	"\377"
	.string	"\001"
	.string	""
	.string	"\377\377"
	.string	""
	.string	""
	.string	"\001"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\001"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\001"
	.string	""
	.string	"\377"
	.string	""
	.string	""
	.string	"\001\001"
	.string	""
	.string	"\377"
	.string	"\001"
	.string	"\001\001"
	.string	""
	.string	""
	.string	""
	.string	"\001"
	.string	""
	.string	""
	.string	"\377"
	.string	""
	.string	""
	.string	"\001"
	.string	""
	.string	""
	.string	"\377"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\377"
	.string	"\377"
	.string	"\001"
	.string	""
	.string	""
	.string	"\377"
	.string	"\001"
	.string	"\001"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\001"
	.string	""
	.string	"\377"
	.string	""
	.string	""
	.string	""
	.string	"\001"
	.string	""
	.string	""
	.string	"\377"
	.string	""
	.string	""
	.string	""
	.string	"\377\377"
	.string	"\377"
	.string	"\001"
	.string	""
	.string	"\377\377"
	.string	""
	.string	"\001\001"
	.string	""
	.string	"\377"
	.string	""
	.string	""
	.string	""
	.string	"\001"
	.string	""
	.string	""
	.string	""
	.ascii	"\001"
	.align 32
	.type	_ZN6icu_67L6LIMITSE, @object
	.size	_ZN6icu_67L6LIMITSE, 368
_ZN6icu_67L6LIMITSE:
	.zero	16
	.long	1
	.long	1
	.long	5000000
	.long	5000000
	.long	0
	.long	0
	.long	11
	.long	11
	.long	1
	.long	1
	.long	50
	.long	51
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	1
	.long	1
	.long	29
	.long	31
	.long	1
	.long	1
	.long	354
	.long	355
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	5
	.long	5
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	1
	.long	1
	.long	5000000
	.long	5000000
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	1
	.long	1
	.long	5000000
	.long	5000000
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.align 32
	.type	_ZN6icu_67L20UMALQURA_MONTHLENGTHE, @object
	.size	_ZN6icu_67L20UMALQURA_MONTHLENGTHE, 1204
_ZN6icu_67L20UMALQURA_MONTHLENGTHE:
	.long	2730
	.long	3412
	.long	3785
	.long	1748
	.long	1770
	.long	876
	.long	2733
	.long	1365
	.long	1705
	.long	1938
	.long	2985
	.long	1492
	.long	2778
	.long	1372
	.long	3373
	.long	1685
	.long	1866
	.long	2900
	.long	2922
	.long	1453
	.long	1198
	.long	2639
	.long	1303
	.long	1675
	.long	1701
	.long	2773
	.long	726
	.long	2395
	.long	1181
	.long	2637
	.long	3366
	.long	3477
	.long	1452
	.long	2486
	.long	698
	.long	2651
	.long	1323
	.long	2709
	.long	1738
	.long	2793
	.long	756
	.long	2422
	.long	694
	.long	2390
	.long	2762
	.long	2980
	.long	3026
	.long	1497
	.long	732
	.long	2413
	.long	1357
	.long	2725
	.long	2898
	.long	2981
	.long	1460
	.long	2486
	.long	1367
	.long	663
	.long	1355
	.long	1699
	.long	1874
	.long	2917
	.long	1386
	.long	2731
	.long	1323
	.long	3221
	.long	3402
	.long	3493
	.long	1482
	.long	2774
	.long	2391
	.long	1195
	.long	2379
	.long	2725
	.long	2898
	.long	2922
	.long	1397
	.long	630
	.long	2231
	.long	1115
	.long	1365
	.long	1449
	.long	1460
	.long	2522
	.long	1245
	.long	622
	.long	2358
	.long	2730
	.long	3412
	.long	3506
	.long	1493
	.long	730
	.long	2395
	.long	1195
	.long	2645
	.long	2889
	.long	2916
	.long	2929
	.long	1460
	.long	2741
	.long	2645
	.long	3365
	.long	3730
	.long	3785
	.long	1748
	.long	2793
	.long	2411
	.long	1195
	.long	2707
	.long	3401
	.long	3492
	.long	3506
	.long	2745
	.long	1210
	.long	2651
	.long	1323
	.long	2709
	.long	2858
	.long	2901
	.long	1372
	.long	1213
	.long	573
	.long	2333
	.long	2709
	.long	2890
	.long	2906
	.long	1389
	.long	694
	.long	2363
	.long	1179
	.long	1621
	.long	1705
	.long	1876
	.long	2922
	.long	1388
	.long	2733
	.long	1365
	.long	2857
	.long	2962
	.long	2985
	.long	1492
	.long	2778
	.long	1370
	.long	2731
	.long	1429
	.long	1865
	.long	1892
	.long	2986
	.long	1461
	.long	694
	.long	2646
	.long	3661
	.long	2853
	.long	2898
	.long	2922
	.long	1453
	.long	686
	.long	2351
	.long	1175
	.long	1611
	.long	1701
	.long	1708
	.long	2774
	.long	1373
	.long	1181
	.long	2637
	.long	3350
	.long	3477
	.long	1450
	.long	1461
	.long	730
	.long	2395
	.long	1197
	.long	1429
	.long	1738
	.long	1764
	.long	2794
	.long	1269
	.long	694
	.long	2390
	.long	2730
	.long	2900
	.long	3026
	.long	1497
	.long	746
	.long	2413
	.long	1197
	.long	2709
	.long	2890
	.long	2981
	.long	1458
	.long	2485
	.long	1238
	.long	2711
	.long	1351
	.long	1683
	.long	1865
	.long	2901
	.long	1386
	.long	2667
	.long	1323
	.long	2699
	.long	3398
	.long	3491
	.long	1482
	.long	2774
	.long	1243
	.long	619
	.long	2379
	.long	2725
	.long	2898
	.long	2921
	.long	1397
	.long	374
	.long	2231
	.long	603
	.long	1323
	.long	1381
	.long	1460
	.long	2522
	.long	1261
	.long	365
	.long	2230
	.long	2726
	.long	3410
	.long	3497
	.long	1492
	.long	2778
	.long	2395
	.long	1195
	.long	1619
	.long	1833
	.long	1890
	.long	2985
	.long	1458
	.long	2741
	.long	1365
	.long	2853
	.long	3474
	.long	3785
	.long	1746
	.long	2793
	.long	1387
	.long	1195
	.long	2645
	.long	3369
	.long	3412
	.long	3498
	.long	2485
	.long	1210
	.long	2619
	.long	1179
	.long	2637
	.long	2730
	.long	2773
	.long	730
	.long	2397
	.long	1118
	.long	2606
	.long	3226
	.long	3413
	.long	1714
	.long	1721
	.long	1210
	.long	2653
	.long	1325
	.long	2709
	.long	2898
	.long	2984
	.long	2996
	.long	1465
	.long	730
	.long	2394
	.long	2890
	.long	3492
	.long	3793
	.long	1768
	.long	2922
	.long	1389
	.long	1333
	.long	1685
	.long	3402
	.long	3496
	.long	3540
	.long	1754
	.long	1371
	.long	669
	.long	1579
	.long	2837
	.long	2890
	.long	2965
	.long	1450
	.long	2734
	.long	2350
	.long	3215
	.long	1319
	.long	1685
	.long	1706
	.long	2774
	.long	1373
	.long	669
	.local	_ZL21gIslamicCalendarAstro
	.comm	_ZL21gIslamicCalendarAstro,8,8
	.local	_ZL11gMonthCache
	.comm	_ZL11gMonthCache,8,8
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC5:
	.long	0
	.long	0
	.align 8
.LC6:
	.long	0
	.long	1080459264
	.align 8
.LC7:
	.long	0
	.long	1081507840
	.align 8
.LC8:
	.long	0
	.long	1100257648
	.align 8
.LC9:
	.long	215482368
	.long	1120097834
	.align 8
.LC10:
	.long	219902326
	.long	1081484768
	.align 8
.LC11:
	.long	858993459
	.long	1092360328
	.align 8
.LC12:
	.long	0
	.long	1071644672
	.align 8
.LC13:
	.long	0
	.long	1077772288
	.align 8
.LC14:
	.long	0
	.long	1086636928
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
