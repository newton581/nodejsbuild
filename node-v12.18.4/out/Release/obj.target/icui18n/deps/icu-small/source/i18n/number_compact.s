	.file	"number_compact.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl11CompactData13getMultiplierEi
	.type	_ZNK6icu_676number4impl11CompactData13getMultiplierEi, @function
_ZNK6icu_676number4impl11CompactData13getMultiplierEi:
.LFB3435:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testl	%esi, %esi
	js	.L1
	movsbl	792(%rdi), %eax
	cmpl	%esi, %eax
	cmovle	%eax, %esi
	movslq	%esi, %rsi
	movsbl	776(%rdi,%rsi), %eax
.L1:
	ret
	.cfi_endproc
.LFE3435:
	.size	_ZNK6icu_676number4impl11CompactData13getMultiplierEi, .-_ZNK6icu_676number4impl11CompactData13getMultiplierEi
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"/patternsShort"
.LC1:
	.string	"/patternsLong"
.LC2:
	.string	"/decimalFormat"
.LC3:
	.string	"/currencyFormat"
.LC4:
	.string	"NumberElements/"
	.text
	.p2align 4
	.type	_ZN12_GLOBAL__N_120getResourceBundleKeyEPKc19UNumberCompactStyleN6icu_676number4impl11CompactTypeERNS3_10CharStringER10UErrorCode, @function
_ZN12_GLOBAL__N_120getResourceBundleKeyEPKc19UNumberCompactStyleN6icu_676number4impl11CompactTypeERNS3_10CharStringER10UErrorCode:
.LFB3425:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%esi, %r15d
	leaq	.LC4(%rip), %rsi
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$56, %rsp
	movq	%rdi, -88(%rbp)
	movq	%r14, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rcx), %rax
	movl	$0, 56(%rcx)
	movb	$0, (%rax)
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movl	-72(%rbp), %edx
	movq	-80(%rbp), %rsi
	movq	%r13, %rcx
	movq	%r12, %rdi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movq	-88(%rbp), %r9
	movq	%r14, %rdi
	movq	%r9, %rsi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movl	-72(%rbp), %edx
	movq	-80(%rbp), %rsi
	movq	%r13, %rcx
	movq	%r12, %rdi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	testl	%r15d, %r15d
	leaq	.LC1(%rip), %rax
	movq	%r14, %rdi
	leaq	.LC0(%rip), %rsi
	cmovne	%rax, %rsi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movl	-72(%rbp), %edx
	movq	-80(%rbp), %rsi
	movq	%r13, %rcx
	movq	%r12, %rdi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	testl	%ebx, %ebx
	leaq	.LC3(%rip), %rax
	movq	%r14, %rdi
	leaq	.LC2(%rip), %rsi
	cmovne	%rax, %rsi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movl	-72(%rbp), %edx
	movq	-80(%rbp), %rsi
	movq	%r13, %rcx
	movq	%r12, %rdi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L12
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L12:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3425:
	.size	_ZN12_GLOBAL__N_120getResourceBundleKeyEPKc19UNumberCompactStyleN6icu_676number4impl11CompactTypeERNS3_10CharStringER10UErrorCode, .-_ZN12_GLOBAL__N_120getResourceBundleKeyEPKc19UNumberCompactStyleN6icu_676number4impl11CompactTypeERNS3_10CharStringER10UErrorCode
	.section	.text._ZN6icu_676number4impl11CompactData15CompactDataSinkD2Ev,"axG",@progbits,_ZN6icu_676number4impl11CompactData15CompactDataSinkD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_676number4impl11CompactData15CompactDataSinkD2Ev
	.type	_ZN6icu_676number4impl11CompactData15CompactDataSinkD2Ev, @function
_ZN6icu_676number4impl11CompactData15CompactDataSinkD2Ev:
.LFB4653:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_676number4impl11CompactData15CompactDataSinkE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6712ResourceSinkD2Ev@PLT
	.cfi_endproc
.LFE4653:
	.size	_ZN6icu_676number4impl11CompactData15CompactDataSinkD2Ev, .-_ZN6icu_676number4impl11CompactData15CompactDataSinkD2Ev
	.weak	_ZN6icu_676number4impl11CompactData15CompactDataSinkD1Ev
	.set	_ZN6icu_676number4impl11CompactData15CompactDataSinkD1Ev,_ZN6icu_676number4impl11CompactData15CompactDataSinkD2Ev
	.section	.text._ZN6icu_676number4impl11CompactData15CompactDataSinkD0Ev,"axG",@progbits,_ZN6icu_676number4impl11CompactData15CompactDataSinkD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_676number4impl11CompactData15CompactDataSinkD0Ev
	.type	_ZN6icu_676number4impl11CompactData15CompactDataSinkD0Ev, @function
_ZN6icu_676number4impl11CompactData15CompactDataSinkD0Ev:
.LFB4655:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_676number4impl11CompactData15CompactDataSinkE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6712ResourceSinkD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE4655:
	.size	_ZN6icu_676number4impl11CompactData15CompactDataSinkD0Ev, .-_ZN6icu_676number4impl11CompactData15CompactDataSinkD0Ev
	.section	.rodata.str2.2,"aMS",@progbits,2
	.align 2
.LC5:
	.string	"<"
	.string	"U"
	.string	"S"
	.string	"E"
	.string	" "
	.string	"F"
	.string	"A"
	.string	"L"
	.string	"L"
	.string	"B"
	.string	"A"
	.string	"C"
	.string	"K"
	.string	">"
	.string	""
	.string	""
	.align 2
.LC6:
	.string	"0"
	.string	""
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl11CompactData15CompactDataSink3putEPKcRNS_13ResourceValueEaR10UErrorCode
	.type	_ZN6icu_676number4impl11CompactData15CompactDataSink3putEPKcRNS_13ResourceValueEaR10UErrorCode, @function
_ZN6icu_676number4impl11CompactData15CompactDataSink3putEPKcRNS_13ResourceValueEaR10UErrorCode:
.LFB3438:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -168(%rbp)
	movq	%r12, %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-144(%rbp), %rax
	movq	%rax, -208(%rbp)
	movq	%rax, %rdi
	movq	(%rdx), %rax
	movq	%r8, %rdx
	call	*88(%rax)
	movl	(%r15), %esi
	testl	%esi, %esi
	jg	.L16
	leaq	-168(%rbp), %rax
	movl	$0, -212(%rbp)
	movq	%rax, -184(%rbp)
.L33:
	movq	-184(%rbp), %rdx
	movl	-212(%rbp), %esi
	movq	%r12, %rcx
	movq	-208(%rbp), %rdi
	call	_ZNK6icu_6713ResourceTable14getKeyAndValueEiRPKcRNS_13ResourceValueE@PLT
	testb	%al, %al
	je	.L16
	movq	-168(%rbp), %rdi
	leaq	-96(%rbp), %r13
	call	strlen@PLT
	movq	%r15, %rdx
	movq	%r13, %rdi
	leal	-1(%rax), %esi
	movb	%al, -190(%rbp)
	movq	8(%r14), %rax
	movb	%sil, -213(%rbp)
	movsbl	%sil, %ebx
	movsbq	%sil, %rsi
	movzbl	776(%rax,%rsi), %eax
	movq	%rsi, -224(%rbp)
	movq	%r12, %rsi
	movb	%al, -189(%rbp)
	movq	(%r12), %rax
	call	*88(%rax)
	movl	(%r15), %ecx
	testl	%ecx, %ecx
	jg	.L16
	leal	(%rbx,%rbx,2), %eax
	xorl	%ebx, %ebx
	addl	%eax, %eax
	movl	%eax, -188(%rbp)
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L22:
	addl	$1, %ebx
.L30:
	movq	-184(%rbp), %rdx
	movq	%r12, %rcx
	movl	%ebx, %esi
	movq	%r13, %rdi
	call	_ZNK6icu_6713ResourceTable14getKeyAndValueEiRPKcRNS_13ResourceValueE@PLT
	testb	%al, %al
	je	.L19
	movq	-168(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN6icu_6714StandardPlural15indexFromStringEPKcR10UErrorCode@PLT
	movl	(%r15), %edx
	testl	%edx, %edx
	jg	.L16
	addl	-188(%rbp), %eax
	movq	8(%r14), %rdx
	movslq	%eax, %rcx
	cmpq	$0, 8(%rdx,%rcx,8)
	movq	%rcx, -176(%rbp)
	jne	.L22
	movq	(%r12), %rax
	movq	%r15, %rdx
	leaq	-148(%rbp), %rsi
	movq	%r12, %rdi
	call	*32(%rax)
	movq	%rax, %rdx
	movl	(%r15), %eax
	testl	%eax, %eax
	jg	.L16
	movq	%rdx, %rdi
	leaq	.LC6(%rip), %rsi
	movq	%rdx, -200(%rbp)
	call	u_strcmp_67@PLT
	movq	-200(%rbp), %rdx
	movq	-176(%rbp), %rcx
	testl	%eax, %eax
	jne	.L23
	movl	$0, -148(%rbp)
	leaq	.LC5(%rip), %rdx
.L23:
	movq	8(%r14), %rax
	cmpb	$0, -189(%rbp)
	movq	%rdx, 8(%rax,%rcx,8)
	jne	.L22
	movl	-148(%rbp), %ecx
	testl	%ecx, %ecx
	jle	.L22
	movq	%rdx, %rax
	leal	-1(%rcx), %edx
	leaq	2(%rax,%rdx,2), %rcx
	xorl	%edx, %edx
.L29:
	cmpw	$48, (%rax)
	je	.L28
	testl	%edx, %edx
	jne	.L27
	addq	$2, %rax
	cmpq	%rax, %rcx
	je	.L22
	cmpw	$48, (%rax)
	je	.L28
.L26:
	addq	$2, %rax
	cmpq	%rax, %rcx
	jne	.L29
	testl	%edx, %edx
	je	.L22
.L27:
	movl	%edx, %eax
	subb	-190(%rbp), %al
	movb	%al, -189(%rbp)
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L28:
	addl	$1, %edx
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L16:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L45
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	movq	8(%r14), %rax
	movq	-224(%rbp), %rbx
	cmpb	$0, 776(%rax,%rbx)
	jne	.L31
	movzbl	-189(%rbp), %edi
	movb	%dil, 776(%rax,%rbx)
	movq	8(%r14), %rax
	movzbl	-213(%rbp), %ebx
	cmpb	%bl, 792(%rax)
	jge	.L32
	movb	%bl, 792(%rax)
	movq	8(%r14), %rax
.L32:
	movb	$0, 793(%rax)
.L31:
	addl	$1, -212(%rbp)
	jmp	.L33
.L45:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3438:
	.size	_ZN6icu_676number4impl11CompactData15CompactDataSink3putEPKcRNS_13ResourceValueEaR10UErrorCode, .-_ZN6icu_676number4impl11CompactData15CompactDataSink3putEPKcRNS_13ResourceValueEaR10UErrorCode
	.section	.text._ZN6icu_676number4impl11CompactDataD2Ev,"axG",@progbits,_ZN6icu_676number4impl11CompactDataD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_676number4impl11CompactDataD2Ev
	.type	_ZN6icu_676number4impl11CompactDataD2Ev, @function
_ZN6icu_676number4impl11CompactDataD2Ev:
.LFB4657:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_676number4impl11CompactDataE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_676number4impl18MultiplierProducerD2Ev@PLT
	.cfi_endproc
.LFE4657:
	.size	_ZN6icu_676number4impl11CompactDataD2Ev, .-_ZN6icu_676number4impl11CompactDataD2Ev
	.weak	_ZN6icu_676number4impl11CompactDataD1Ev
	.set	_ZN6icu_676number4impl11CompactDataD1Ev,_ZN6icu_676number4impl11CompactDataD2Ev
	.section	.text._ZN6icu_676number4impl11CompactDataD0Ev,"axG",@progbits,_ZN6icu_676number4impl11CompactDataD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_676number4impl11CompactDataD0Ev
	.type	_ZN6icu_676number4impl11CompactDataD0Ev, @function
_ZN6icu_676number4impl11CompactDataD0Ev:
.LFB4659:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_676number4impl11CompactDataE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_676number4impl18MultiplierProducerD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$800, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE4659:
	.size	_ZN6icu_676number4impl11CompactDataD0Ev, .-_ZN6icu_676number4impl11CompactDataD0Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl14CompactHandler15processQuantityERNS1_15DecimalQuantityERNS1_10MicroPropsER10UErrorCode
	.type	_ZNK6icu_676number4impl14CompactHandler15processQuantityERNS1_15DecimalQuantityERNS1_10MicroPropsER10UErrorCode, @function
_ZNK6icu_676number4impl14CompactHandler15processQuantityERNS1_15DecimalQuantityERNS1_10MicroPropsER10UErrorCode:
.LFB3450:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$168, %rsp
	movq	16(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	call	*16(%rax)
	movl	(%r14), %edx
	testl	%edx, %edx
	jle	.L79
.L49:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L80
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZNK6icu_676number4impl15DecimalQuantity9isZeroishEv@PLT
	leaq	8(%r12), %rdi
	testb	%al, %al
	jne	.L81
	movq	%r14, %rcx
	leaq	240(%rbx), %rdx
	movq	%r13, %rsi
	call	_ZN6icu_676number4impl12RoundingImpl24chooseMultiplierAndApplyERNS1_15DecimalQuantityERKNS1_18MultiplierProducerER10UErrorCode@PLT
	movq	%r13, %rdi
	movl	%eax, %r15d
	call	_ZNK6icu_676number4impl15DecimalQuantity9isZeroishEv@PLT
	xorl	%ecx, %ecx
	testb	%al, %al
	je	.L82
.L54:
	movq	8(%rbx), %rsi
	subl	%r15d, %ecx
	negl	%r15d
	movl	%r15d, -196(%rbp)
	testq	%rsi, %rsi
	je	.L55
	leaq	-128(%rbp), %r15
	movq	%r13, %rdx
	movl	%ecx, -184(%rbp)
	movq	%r15, %rdi
	call	_ZNK6icu_6711PluralRules6selectERKNS_13IFixedDecimalE@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6714StandardPlural25indexOrNegativeFromStringERKNS_13UnicodeStringE@PLT
	movl	-184(%rbp), %ecx
	testl	%eax, %eax
	js	.L68
	movl	%eax, -184(%rbp)
.L56:
	movq	%r15, %rdi
	movl	%eax, -200(%rbp)
	movl	%ecx, -192(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-192(%rbp), %ecx
	movl	-200(%rbp), %eax
	testl	%ecx, %ecx
	js	.L57
.L63:
	movsbl	1032(%rbx), %edx
	cmpl	%ecx, %edx
	cmovle	%edx, %ecx
	leal	(%rcx,%rcx,2), %edx
	addl	%edx, %edx
	addl	%edx, %eax
	cltq
	movq	248(%rbx,%rax,8), %r15
	testq	%r15, %r15
	sete	%al
	cmpl	$5, -184(%rbp)
	je	.L58
	testb	%al, %al
	je	.L58
	addl	$5, %edx
	movslq	%edx, %rdx
	movq	248(%rbx,%rdx,8), %r15
	testq	%r15, %r15
	sete	%al
.L58:
	leaq	.LC5(%rip), %rdx
	cmpq	%rdx, %r15
	je	.L57
	testb	%al, %al
	jne	.L57
	cmpb	$0, 1488(%rbx)
	je	.L59
	movl	232(%rbx), %eax
	testl	%eax, %eax
	jle	.L57
	xorl	%edx, %edx
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L60:
	addq	$1, %rdx
	cmpl	%edx, 232(%rbx)
	jle	.L57
.L61:
	movq	%rdx, %r8
	movq	%r15, %rdi
	movq	%rdx, -192(%rbp)
	salq	$4, %r8
	addq	24(%rbx), %r8
	movq	8(%r8), %rsi
	movq	%r8, -184(%rbp)
	call	u_strcmp_67@PLT
	movq	-184(%rbp), %r8
	movq	-192(%rbp), %rdx
	testl	%eax, %eax
	jne	.L60
	movq	(%r8), %rdi
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	_ZNK6icu_676number4impl24ImmutablePatternModifier13applyToMicrosERNS1_10MicroPropsERKNS1_15DecimalQuantityER10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L57:
	movl	-196(%rbp), %esi
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantity14adjustExponentEi@PLT
	leaq	-176(%rbp), %rdi
	call	_ZN6icu_676number4impl12RoundingImpl11passThroughEv@PLT
	movl	-144(%rbp), %eax
	movdqa	-176(%rbp), %xmm0
	movdqa	-160(%rbp), %xmm1
	movl	%eax, 40(%r12)
	movzbl	-140(%rbp), %eax
	movups	%xmm0, 8(%r12)
	movb	%al, 44(%r12)
	movups	%xmm1, 24(%r12)
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L81:
	movq	%r13, %rsi
	movq	%r14, %rdx
	call	_ZNK6icu_676number4impl12RoundingImpl5applyERNS1_15DecimalQuantityER10UErrorCode@PLT
	movq	8(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L83
	leaq	-128(%rbp), %r15
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZNK6icu_6711PluralRules6selectERKNS_13IFixedDecimalE@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6714StandardPlural25indexOrNegativeFromStringERKNS_13UnicodeStringE@PLT
	testl	%eax, %eax
	js	.L84
	movq	%r15, %rdi
	movl	%eax, -184(%rbp)
	movl	%eax, -192(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-192(%rbp), %eax
	xorl	%ecx, %ecx
	movl	$0, -196(%rbp)
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L59:
	leaq	1040(%rbx), %r8
	leaq	-128(%rbp), %rdi
	movq	%r15, %rsi
	movq	%r8, -192(%rbp)
	movq	%rdi, -184(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	-192(%rbp), %r8
	movq	-184(%rbp), %rdi
	movq	%r14, %rdx
	movq	%r8, %rsi
	call	_ZN6icu_676number4impl13PatternParser18parseToPatternInfoERKNS_13UnicodeStringERNS1_17ParsedPatternInfoER10UErrorCode@PLT
	movq	-184(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-192(%rbp), %r8
	movl	$44, %edx
	movq	1480(%rbx), %rdi
	movq	%r8, %rsi
	call	_ZN6icu_676number4impl22MutablePatternModifier14setPatternInfoEPKNS1_20AffixPatternProviderENS_22FormattedStringBuilder5FieldE@PLT
	movq	1480(%rbx), %r14
	movq	%r13, %rdi
	call	_ZNK6icu_676number4impl15DecimalQuantity6signumEv@PLT
	movl	$6, %edx
	movl	%eax, %esi
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl22MutablePatternModifier19setNumberPropertiesENS1_6SignumENS_14StandardPlural4FormE@PLT
	movq	1480(%rbx), %rax
	leaq	8(%rax), %rdx
	testq	%rax, %rax
	cmovne	%rdx, %rax
	movq	%rax, 120(%r12)
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L82:
	movq	%r13, %rdi
	call	_ZNK6icu_676number4impl15DecimalQuantity12getMagnitudeEv@PLT
	movl	%eax, %ecx
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L68:
	movl	$5, -184(%rbp)
	movl	$5, %eax
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L84:
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movsbl	1032(%rbx), %eax
	movl	$0, %edx
	movl	$0, -196(%rbp)
	testl	%eax, %eax
	cmovg	%edx, %eax
	leal	(%rax,%rax,2), %eax
	leal	5(%rax,%rax), %eax
	cltq
	movq	248(%rbx,%rax,8), %r15
	testq	%r15, %r15
	sete	%al
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L83:
	movl	$0, -196(%rbp)
	xorl	%ecx, %ecx
.L52:
	movsbl	1032(%rbx), %eax
	cmpl	%ecx, %eax
	cmovle	%eax, %ecx
	leal	(%rcx,%rcx,2), %eax
	leal	5(%rax,%rax), %eax
	cltq
	movq	248(%rbx,%rax,8), %r15
	testq	%r15, %r15
	sete	%al
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L55:
	testl	%ecx, %ecx
	jns	.L52
	jmp	.L57
.L80:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3450:
	.size	_ZNK6icu_676number4impl14CompactHandler15processQuantityERNS1_15DecimalQuantityERNS1_10MicroPropsER10UErrorCode, .-_ZNK6icu_676number4impl14CompactHandler15processQuantityERNS1_15DecimalQuantityERNS1_10MicroPropsER10UErrorCode
	.section	.text._ZN6icu_676number4impl24ImmutablePatternModifierD0Ev,"axG",@progbits,_ZN6icu_676number4impl24ImmutablePatternModifierD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_676number4impl24ImmutablePatternModifierD0Ev
	.type	_ZN6icu_676number4impl24ImmutablePatternModifierD0Ev, @function
_ZN6icu_676number4impl24ImmutablePatternModifierD0Ev:
.LFB4671:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_676number4impl24ImmutablePatternModifierE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L86
	movq	(%rdi), %rax
	call	*8(%rax)
.L86:
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE4671:
	.size	_ZN6icu_676number4impl24ImmutablePatternModifierD0Ev, .-_ZN6icu_676number4impl24ImmutablePatternModifierD0Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev:
.LFB3733:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3733:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei:
.LFB3736:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	cmpl	$40, %esi
	jg	.L104
	ret
	.p2align 4,,10
	.p2align 3
.L104:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L92
	cmpb	$0, 12(%rbx)
	jne	.L105
.L96:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L92:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L105:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L96
	.cfi_endproc
.LFE3736:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EED2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev:
.LFB3739:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L108
	ret
	.p2align 4,,10
	.p2align 3
.L108:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3739:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5EOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_:
.LFB3742:
	.cfi_startproc
	endbr64
	movzbl	12(%rsi), %eax
	movq	(%rsi), %r8
	movslq	8(%rsi), %rdx
	movb	%al, 12(%rdi)
	leaq	13(%rsi), %rax
	movq	%r8, (%rdi)
	movl	%edx, 8(%rdi)
	cmpq	%rax, %r8
	je	.L111
	movq	%rax, (%rsi)
	movl	$40, 8(%rsi)
	movb	$0, 12(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L111:
	leaq	13(%rdi), %r9
	movq	%r8, %rsi
	movq	%r9, (%rdi)
	movq	%r9, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE3742:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_:
.LFB3744:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 12(%rdi)
	movq	%rsi, %rbx
	jne	.L117
.L113:
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movzbl	12(%rbx), %eax
	movb	%al, 12(%r12)
	movq	(%rbx), %rsi
	leaq	13(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L118
	movq	%rsi, (%r12)
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L117:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L118:
	leaq	13(%r12), %rdi
	movslq	8(%rbx), %rdx
	movq	%rdi, (%r12)
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3744:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv:
.LFB3745:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE3745:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv:
.LFB3746:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3746:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv:
.LFB3747:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	addq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3747:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3748:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3748:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3749:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3749:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci:
.LFB3750:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L134
	testl	%edx, %edx
	jle	.L134
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 12(%rdi)
	jne	.L137
.L126:
	movq	%rsi, (%rbx)
	movl	%edx, 8(%rbx)
	movb	$0, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L134:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L137:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	%edx, -28(%rbp)
	movq	%rsi, -24(%rbp)
	call	uprv_free_67@PLT
	movl	-28(%rbp), %edx
	movq	-24(%rbp), %rsi
	jmp	.L126
	.cfi_endproc
.LFE3750:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii:
.LFB3751:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L141
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movl	%esi, %r13d
	movl	%edx, %r12d
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L141
	testl	%r12d, %r12d
	jg	.L148
	cmpb	$0, 12(%rbx)
	jne	.L149
.L143:
	movq	%r14, (%rbx)
	movq	%r14, %rax
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L148:
	.cfi_restore_state
	cmpl	%r12d, 8(%rbx)
	cmovle	8(%rbx), %r12d
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	je	.L143
.L149:
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L141:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3751:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, .-_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi:
.LFB3752:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 12(%rdi)
	movq	%rdi, %rbx
	je	.L151
	movq	(%rdi), %r8
.L152:
	leaq	13(%rbx), %rax
	movl	%r12d, 0(%r13)
	movq	%rax, (%rbx)
	movq	%r8, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L151:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L155
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	cmovg	%eax, %r12d
	movslq	%r12d, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L155
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L155:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3752:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, .-_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv:
.LFB3753:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L162
	ret
	.p2align 4,,10
	.p2align 3
.L162:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3753:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv:
.LFB3754:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3754:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_:
.LFB3755:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3755:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_:
.LFB3756:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3756:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5ERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_:
.LFB3758:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3758:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_:
.LFB3760:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3760:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl11CompactDataC2Ev
	.type	_ZN6icu_676number4impl11CompactDataC2Ev, @function
_ZN6icu_676number4impl11CompactDataC2Ev:
.LFB3432:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_676number4impl11CompactDataE(%rip), %rax
	movq	$0, 8(%rdi)
	movq	%rdi, %rdx
	leaq	16(%rdi), %rdi
	movq	%rax, -16(%rdi)
	movl	%edx, %eax
	pxor	%xmm0, %xmm0
	movq	$0, 752(%rdi)
	andq	$-8, %rdi
	subl	%edi, %eax
	leal	776(%rax), %ecx
	xorl	%eax, %eax
	shrl	$3, %ecx
	rep stosq
	movl	$256, %eax
	movups	%xmm0, 776(%rdx)
	movw	%ax, 792(%rdx)
	ret
	.cfi_endproc
.LFE3432:
	.size	_ZN6icu_676number4impl11CompactDataC2Ev, .-_ZN6icu_676number4impl11CompactDataC2Ev
	.globl	_ZN6icu_676number4impl11CompactDataC1Ev
	.set	_ZN6icu_676number4impl11CompactDataC1Ev,_ZN6icu_676number4impl11CompactDataC2Ev
	.section	.rodata.str1.1
.LC7:
	.string	"latn"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl11CompactData8populateERKNS_6LocaleEPKc19UNumberCompactStyleNS1_11CompactTypeER10UErrorCode
	.type	_ZN6icu_676number4impl11CompactData8populateERKNS_6LocaleEPKc19UNumberCompactStyleNS1_11CompactTypeER10UErrorCode, @function
_ZN6icu_676number4impl11CompactData8populateERKNS_6LocaleEPKc19UNumberCompactStyleNS1_11CompactTypeER10UErrorCode:
.LFB3434:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	16+_ZTVN6icu_676number4impl11CompactData15CompactDataSinkE(%rip), %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%r9, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$168, %rsp
	movq	%rdx, -168(%rbp)
	movq	40(%rsi), %rsi
	movq	%r9, %rdx
	movl	%ecx, -172(%rbp)
	movl	%r8d, -176(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -136(%rbp)
	xorl	%edi, %edi
	movq	%r15, -144(%rbp)
	call	ures_open_67@PLT
	movl	0(%r13), %esi
	movq	%rax, %r14
	testl	%esi, %esi
	jle	.L170
	testq	%rax, %rax
	je	.L202
	movq	%rax, %rdi
	call	ures_close_67@PLT
.L202:
	leaq	-144(%rbp), %r12
.L172:
	movq	%r12, %rdi
	movq	%r15, -144(%rbp)
	call	_ZN6icu_6712ResourceSinkD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L203
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L170:
	.cfi_restore_state
	movl	$5, %ecx
	leaq	-115(%rbp), %rdx
	leaq	-128(%rbp), %r9
	movq	%r13, %r8
	movq	-168(%rbp), %rsi
	leaq	.LC7(%rip), %rdi
	movq	%rdx, -128(%rbp)
	leaq	-144(%rbp), %r12
	movl	-176(%rbp), %edx
	movq	%r9, -192(%rbp)
	repz cmpsb
	movl	-172(%rbp), %esi
	movq	-168(%rbp), %rdi
	movl	$0, -72(%rbp)
	movl	$40, -120(%rbp)
	seta	%al
	sbbb	$0, %al
	xorl	%ecx, %ecx
	movsbl	%al, %eax
	movw	%cx, -116(%rbp)
	movq	%r9, %rcx
	movl	%eax, -196(%rbp)
	call	_ZN12_GLOBAL__N_120getResourceBundleKeyEPKc19UNumberCompactStyleN6icu_676number4impl11CompactTypeERNS3_10CharStringER10UErrorCode
	movq	-128(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r14, %rdi
	leaq	-148(%rbp), %rax
	movl	$0, -148(%rbp)
	movq	%rax, %rcx
	movq	%rax, -184(%rbp)
	call	ures_getAllItemsWithFallback_67@PLT
	movzbl	793(%rbx), %edx
	testb	%dl, %dl
	je	.L180
	movl	-196(%rbp), %eax
	movq	-192(%rbp), %r9
	testl	%eax, %eax
	jne	.L204
.L173:
	testb	%dl, %dl
	je	.L180
	movl	-172(%rbp), %edx
	testl	%edx, %edx
	jne	.L205
.L176:
	movl	$5, 0(%r13)
.L180:
	cmpb	$0, -116(%rbp)
	jne	.L206
.L178:
	testq	%r14, %r14
	je	.L172
	movq	%r14, %rdi
	call	ures_close_67@PLT
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L204:
	movl	-176(%rbp), %edx
	movl	-172(%rbp), %esi
	movq	%r9, %rcx
	movq	%r13, %r8
	leaq	.LC7(%rip), %rdi
	call	_ZN12_GLOBAL__N_120getResourceBundleKeyEPKc19UNumberCompactStyleN6icu_676number4impl11CompactTypeERNS3_10CharStringER10UErrorCode
	movq	-128(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	-184(%rbp), %rcx
	movl	$0, -148(%rbp)
	call	ures_getAllItemsWithFallback_67@PLT
	movzbl	793(%rbx), %edx
	movl	-196(%rbp), %eax
	movq	-192(%rbp), %r9
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L205:
	movl	-176(%rbp), %edx
	movq	%r9, %rcx
	xorl	%esi, %esi
	movq	%r13, %r8
	movq	-168(%rbp), %rdi
	movl	%eax, -172(%rbp)
	movq	%r9, -192(%rbp)
	call	_ZN12_GLOBAL__N_120getResourceBundleKeyEPKc19UNumberCompactStyleN6icu_676number4impl11CompactTypeERNS3_10CharStringER10UErrorCode
	movq	-128(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	-184(%rbp), %rcx
	movl	$0, -148(%rbp)
	call	ures_getAllItemsWithFallback_67@PLT
	cmpb	$0, 793(%rbx)
	je	.L180
	movl	-172(%rbp), %eax
	testl	%eax, %eax
	je	.L176
	movq	-192(%rbp), %r9
	movl	-176(%rbp), %edx
	xorl	%esi, %esi
	movq	%r13, %r8
	leaq	.LC7(%rip), %rdi
	movq	%r9, %rcx
	call	_ZN12_GLOBAL__N_120getResourceBundleKeyEPKc19UNumberCompactStyleN6icu_676number4impl11CompactTypeERNS3_10CharStringER10UErrorCode
	movq	-128(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	-184(%rbp), %rcx
	movl	$0, -148(%rbp)
	call	ures_getAllItemsWithFallback_67@PLT
	cmpb	$0, 793(%rbx)
	je	.L180
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L206:
	movq	-128(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L178
.L203:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3434:
	.size	_ZN6icu_676number4impl11CompactData8populateERKNS_6LocaleEPKc19UNumberCompactStyleNS1_11CompactTypeER10UErrorCode, .-_ZN6icu_676number4impl11CompactData8populateERKNS_6LocaleEPKc19UNumberCompactStyleNS1_11CompactTypeER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl11CompactData10getPatternEiNS_14StandardPlural4FormE
	.type	_ZNK6icu_676number4impl11CompactData10getPatternEiNS_14StandardPlural4FormE, @function
_ZNK6icu_676number4impl11CompactData10getPatternEiNS_14StandardPlural4FormE:
.LFB3436:
	.cfi_startproc
	endbr64
	testl	%esi, %esi
	js	.L210
	movsbl	792(%rdi), %eax
	cmpl	%esi, %eax
	cmovle	%eax, %esi
	leal	(%rsi,%rsi,2), %ecx
	addl	%ecx, %ecx
	leal	(%rdx,%rcx), %eax
	cltq
	movq	8(%rdi,%rax,8), %rax
	testq	%rax, %rax
	jne	.L209
	cmpl	$5, %edx
	je	.L209
	addl	$5, %ecx
	movslq	%ecx, %rcx
	movq	8(%rdi,%rcx,8), %rax
.L209:
	leaq	.LC5(%rip), %rdx
	cmpq	%rdx, %rax
	movl	$0, %edx
	cmove	%rdx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L210:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3436:
	.size	_ZNK6icu_676number4impl11CompactData10getPatternEiNS_14StandardPlural4FormE, .-_ZNK6icu_676number4impl11CompactData10getPatternEiNS_14StandardPlural4FormE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl11CompactData17getUniquePatternsERNS_7UVectorER10UErrorCode
	.type	_ZNK6icu_676number4impl11CompactData17getUniquePatternsERNS_7UVectorER10UErrorCode, @function
_ZNK6icu_676number4impl11CompactData17getUniquePatternsERNS_7UVectorER10UErrorCode:
.LFB3437:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	776(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	.LC5(%rip), %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	8(%rdi), %rbx
	subq	$24, %rsp
	movq	%rdx, -64(%rbp)
	movq	%rax, -56(%rbp)
	.p2align 4,,10
	.p2align 3
.L224:
	movq	(%rbx), %r12
	cmpq	%r15, %r12
	je	.L220
	testq	%r12, %r12
	je	.L220
	movl	8(%r13), %r14d
	subl	$1, %r14d
	jns	.L223
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L231:
	subl	$1, %r14d
	cmpl	$-1, %r14d
	je	.L221
.L223:
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	u_strcmp_67@PLT
	testl	%eax, %eax
	jne	.L231
.L220:
	addq	$8, %rbx
	cmpq	%rbx, -56(%rbp)
	jne	.L224
.L232:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L221:
	.cfi_restore_state
	movq	-64(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	addq	$8, %rbx
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	cmpq	%rbx, -56(%rbp)
	jne	.L224
	jmp	.L232
	.cfi_endproc
.LFE3437:
	.size	_ZNK6icu_676number4impl11CompactData17getUniquePatternsERNS_7UVectorER10UErrorCode, .-_ZNK6icu_676number4impl11CompactData17getUniquePatternsERNS_7UVectorER10UErrorCode
	.section	.text._ZN6icu_676number4impl24ImmutablePatternModifierD2Ev,"axG",@progbits,_ZN6icu_676number4impl24ImmutablePatternModifierD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_676number4impl24ImmutablePatternModifierD2Ev
	.type	_ZN6icu_676number4impl24ImmutablePatternModifierD2Ev, @function
_ZN6icu_676number4impl24ImmutablePatternModifierD2Ev:
.LFB4669:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_676number4impl24ImmutablePatternModifierE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L234
	movq	(%rdi), %rax
	call	*8(%rax)
.L234:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	.cfi_endproc
.LFE4669:
	.size	_ZN6icu_676number4impl24ImmutablePatternModifierD2Ev, .-_ZN6icu_676number4impl24ImmutablePatternModifierD2Ev
	.weak	_ZN6icu_676number4impl24ImmutablePatternModifierD1Ev
	.set	_ZN6icu_676number4impl24ImmutablePatternModifierD1Ev,_ZN6icu_676number4impl24ImmutablePatternModifierD2Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl14CompactHandlerD2Ev
	.type	_ZN6icu_676number4impl14CompactHandlerD2Ev, @function
_ZN6icu_676number4impl14CompactHandlerD2Ev:
.LFB3446:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_676number4impl14CompactHandlerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	16+_ZTVN6icu_676number4impl24ImmutablePatternModifierE(%rip), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	_ZN6icu_676number4impl24ImmutablePatternModifierD0Ev(%rip), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$8, %rsp
	movl	232(%rdi), %edx
	movq	%rax, (%rdi)
	testl	%edx, %edx
	jg	.L240
	jmp	.L246
	.p2align 4,,10
	.p2align 3
.L257:
	movq	8(%r12), %rdi
	movq	%r14, (%r12)
	testq	%rdi, %rdi
	je	.L245
	movq	(%rdi), %rax
	call	*8(%rax)
.L245:
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	movl	232(%r15), %edx
.L243:
	addq	$1, %rbx
	cmpl	%ebx, %edx
	jle	.L246
.L240:
	movq	%rbx, %rax
	salq	$4, %rax
	addq	24(%r15), %rax
	movq	(%rax), %r12
	testq	%r12, %r12
	je	.L243
	movq	(%r12), %rax
	movq	8(%rax), %rax
	cmpq	%r13, %rax
	je	.L257
	movq	%r12, %rdi
	addq	$1, %rbx
	call	*%rax
	movl	232(%r15), %edx
	cmpl	%ebx, %edx
	jg	.L240
	.p2align 4,,10
	.p2align 3
.L246:
	leaq	16+_ZTVN6icu_676number4impl17ParsedPatternInfoE(%rip), %rax
	leaq	1336(%r15), %rdi
	movq	%rax, 1040(%r15)
	leaq	1040(%r15), %r12
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	leaq	1168(%r15), %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	leaq	1048(%r15), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl20AffixPatternProviderD2Ev@PLT
	leaq	16+_ZTVN6icu_676number4impl11CompactDataE(%rip), %rax
	leaq	240(%r15), %rdi
	movq	%rax, 240(%r15)
	call	_ZN6icu_676number4impl18MultiplierProducerD2Ev@PLT
	cmpb	$0, 36(%r15)
	jne	.L258
.L242:
	addq	$8, %rsp
	movq	%r15, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L258:
	.cfi_restore_state
	movq	24(%r15), %rdi
	call	uprv_free_67@PLT
	jmp	.L242
	.cfi_endproc
.LFE3446:
	.size	_ZN6icu_676number4impl14CompactHandlerD2Ev, .-_ZN6icu_676number4impl14CompactHandlerD2Ev
	.globl	_ZN6icu_676number4impl14CompactHandlerD1Ev
	.set	_ZN6icu_676number4impl14CompactHandlerD1Ev,_ZN6icu_676number4impl14CompactHandlerD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl14CompactHandlerD0Ev
	.type	_ZN6icu_676number4impl14CompactHandlerD0Ev, @function
_ZN6icu_676number4impl14CompactHandlerD0Ev:
.LFB3448:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN6icu_676number4impl14CompactHandlerD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3448:
	.size	_ZN6icu_676number4impl14CompactHandlerD0Ev, .-_ZN6icu_676number4impl14CompactHandlerD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl14CompactHandler22precomputeAllModifiersERNS1_22MutablePatternModifierER10UErrorCode
	.type	_ZN6icu_676number4impl14CompactHandler22precomputeAllModifiersERNS1_22MutablePatternModifierER10UErrorCode, @function
_ZN6icu_676number4impl14CompactHandler22precomputeAllModifiersERNS1_22MutablePatternModifierER10UErrorCode:
.LFB3449:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$696, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -688(%rbp)
	movl	(%rdx), %r9d
	movq	%rsi, -696(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r9d, %r9d
	jle	.L292
.L261:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L293
	addq	$696, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L292:
	.cfi_restore_state
	leaq	-672(%rbp), %r13
	movq	%rdx, %rbx
	movl	$12, %esi
	movq	%r13, %rdi
	call	_ZN6icu_677UVectorC1EiR10UErrorCode@PLT
	movl	(%rbx), %r8d
	testl	%r8d, %r8d
	jle	.L294
.L277:
	movq	%r13, %rdi
	call	_ZN6icu_677UVectorD1Ev@PLT
	jmp	.L261
	.p2align 4,,10
	.p2align 3
.L294:
	movq	-688(%rbp), %rax
	leaq	248(%rax), %r12
	addq	$1016, %rax
	movq	%rax, -680(%rbp)
	.p2align 4,,10
	.p2align 3
.L270:
	movq	(%r12), %r14
	leaq	.LC5(%rip), %rax
	cmpq	%rax, %r14
	je	.L266
	testq	%r14, %r14
	je	.L266
	movl	-664(%rbp), %r15d
	subl	$1, %r15d
	jns	.L267
	jmp	.L269
	.p2align 4,,10
	.p2align 3
.L295:
	subl	$1, %r15d
	cmpl	$-1, %r15d
	je	.L269
.L267:
	movl	%r15d, %esi
	movq	%r13, %rdi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	u_strcmp_67@PLT
	testl	%eax, %eax
	jne	.L295
.L266:
	addq	$8, %r12
	cmpq	%r12, -680(%rbp)
	jne	.L270
.L299:
	movl	(%rbx), %edi
	testl	%edi, %edi
	jg	.L277
	movl	-664(%rbp), %r12d
	movq	-688(%rbp), %r15
	movl	%r12d, 232(%r15)
	cmpl	32(%r15), %r12d
	jle	.L271
	testl	%r12d, %r12d
	jle	.L277
	movslq	%r12d, %rdi
	salq	$4, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L273
	cmpb	$0, 36(%r15)
	jne	.L296
.L274:
	movq	-688(%rbp), %rax
	movq	%r14, 24(%rax)
	movl	%r12d, 32(%rax)
	movb	$1, 36(%rax)
.L273:
	movl	(%rbx), %esi
	testl	%esi, %esi
	jg	.L277
	movq	-688(%rbp), %rax
	movl	232(%rax), %r12d
	.p2align 4,,10
	.p2align 3
.L271:
	testl	%r12d, %r12d
	jle	.L277
	leaq	-624(%rbp), %rax
	movq	%r13, -736(%rbp)
	leaq	-496(%rbp), %r15
	movq	%rax, -712(%rbp)
	leaq	-368(%rbp), %rax
	leaq	-560(%rbp), %r14
	movq	%rax, -720(%rbp)
	leaq	-200(%rbp), %rax
	movq	%rax, -728(%rbp)
	leaq	-488(%rbp), %rax
	movq	$0, -680(%rbp)
	movq	%rax, -704(%rbp)
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L298:
	movq	-696(%rbp), %rdi
	movl	$44, %edx
	movq	%r15, %rsi
	call	_ZN6icu_676number4impl22MutablePatternModifier14setPatternInfoEPKNS1_20AffixPatternProviderENS_22FormattedStringBuilder5FieldE@PLT
	movq	-696(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN6icu_676number4impl22MutablePatternModifier15createImmutableER10UErrorCode@PLT
	movq	%rax, 0(%r13)
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L275
	movq	%r12, 8(%r13)
	movq	-728(%rbp), %rdi
	leaq	16+_ZTVN6icu_676number4impl17ParsedPatternInfoE(%rip), %rdx
	movq	%rdx, -496(%rbp)
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	movq	-720(%rbp), %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	movq	-704(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_676number4impl20AffixPatternProviderD2Ev@PLT
	movq	-712(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-688(%rbp), %rcx
	addq	$1, -680(%rbp)
	movq	-680(%rbp), %rax
	cmpl	%eax, 232(%rcx)
	jle	.L297
.L276:
	movq	-680(%rbp), %r13
	movq	-736(%rbp), %rdi
	movl	%r13d, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	-712(%rbp), %rdi
	movq	%rax, %rsi
	movq	%rax, %r12
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	-688(%rbp), %rcx
	movq	.LC8(%rip), %xmm0
	movl	$2, %edx
	movq	-720(%rbp), %rdi
	movq	%r13, -680(%rbp)
	salq	$4, %r13
	movhps	.LC9(%rip), %xmm0
	addq	24(%rcx), %r13
	movabsq	$281474976645120, %rcx
	movw	%dx, -480(%rbp)
	movaps	%xmm0, -496(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rcx, -424(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movb	$0, -384(%rbp)
	movl	$0, -380(%rbp)
	movb	$0, -376(%rbp)
	movl	$0, -372(%rbp)
	call	_ZN6icu_676number4impl15DecimalQuantityC1Ev@PLT
	movq	-728(%rbp), %rdi
	pxor	%xmm0, %xmm0
	movabsq	$281474976645120, %rcx
	movq	%rcx, -256(%rbp)
	movups	%xmm0, -280(%rbp)
	movups	%xmm0, -248(%rbp)
	movups	%xmm0, -232(%rbp)
	movb	$0, -296(%rbp)
	movq	$0, -292(%rbp)
	movb	$0, -284(%rbp)
	movq	$0, -264(%rbp)
	movb	$0, -216(%rbp)
	movl	$0, -212(%rbp)
	movb	$0, -208(%rbp)
	movl	$0, -204(%rbp)
	call	_ZN6icu_676number4impl15DecimalQuantityC1Ev@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	-704(%rbp), %rcx
	movaps	%xmm0, -112(%rbp)
	movb	$0, -128(%rbp)
	movq	%rcx, -88(%rbp)
	movq	$0, -124(%rbp)
	movb	$0, -116(%rbp)
	movq	$0, -96(%rbp)
	movl	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	movb	$0, -64(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl13PatternParser18parseToPatternInfoERKNS_13UnicodeStringERNS1_17ParsedPatternInfoER10UErrorCode@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jle	.L298
.L275:
	movq	-728(%rbp), %rdi
	leaq	16+_ZTVN6icu_676number4impl17ParsedPatternInfoE(%rip), %rax
	movq	-736(%rbp), %r13
	movq	%rax, -496(%rbp)
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	movq	-720(%rbp), %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	movq	-704(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_676number4impl20AffixPatternProviderD2Ev@PLT
	movq	-712(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L277
	.p2align 4,,10
	.p2align 3
.L269:
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	addq	$8, %r12
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	cmpq	%r12, -680(%rbp)
	jne	.L270
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L297:
	movq	-736(%rbp), %r13
	jmp	.L277
.L296:
	movq	-688(%rbp), %rax
	movq	24(%rax), %rdi
	call	uprv_free_67@PLT
	jmp	.L274
.L293:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3449:
	.size	_ZN6icu_676number4impl14CompactHandler22precomputeAllModifiersERNS1_22MutablePatternModifierER10UErrorCode, .-_ZN6icu_676number4impl14CompactHandler22precomputeAllModifiersERNS1_22MutablePatternModifierER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl14CompactHandlerC2E19UNumberCompactStyleRKNS_6LocaleEPKcNS1_11CompactTypeEPKNS_11PluralRulesEPNS1_22MutablePatternModifierEbPKNS1_19MicroPropsGeneratorER10UErrorCode
	.type	_ZN6icu_676number4impl14CompactHandlerC2E19UNumberCompactStyleRKNS_6LocaleEPKcNS1_11CompactTypeEPKNS_11PluralRulesEPNS1_22MutablePatternModifierEbPKNS1_19MicroPropsGeneratorER10UErrorCode, @function
_ZN6icu_676number4impl14CompactHandlerC2E19UNumberCompactStyleRKNS_6LocaleEPKcNS1_11CompactTypeEPKNS_11PluralRulesEPNS1_22MutablePatternModifierEbPKNS1_19MicroPropsGeneratorER10UErrorCode:
.LFB3443:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_676number4impl14CompactHandlerE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	240(%rdi), %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	movl	$2, %edx
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movl	24(%rbp), %ebx
	movq	16(%rbp), %r10
	movq	%rax, (%rdi)
	movq	32(%rbp), %rax
	movq	%r9, 8(%rdi)
	movl	$12, 32(%rdi)
	movq	40(%rbp), %r14
	movq	%rax, 16(%rdi)
	leaq	40(%rdi), %rax
	addq	$256, %rdi
	movq	%rax, -232(%rdi)
	leaq	16+_ZTVN6icu_676number4impl11CompactDataE(%rip), %rax
	movq	%r10, -56(%rbp)
	movl	%r8d, -60(%rbp)
	movl	%esi, -64(%rbp)
	leaq	16+_ZTVN6icu_676number4impl17ParsedPatternInfoE(%rip), %rsi
	movq	%rcx, -72(%rbp)
	movq	%rax, -16(%rdi)
	movl	%r12d, %eax
	movb	$0, -220(%rdi)
	movl	$0, -24(%rdi)
	movq	$0, -8(%rdi)
	movq	$0, 752(%rdi)
	andq	$-8, %rdi
	subl	%edi, %eax
	leal	1016(%rax), %ecx
	xorl	%eax, %eax
	shrl	$3, %ecx
	rep stosq
	movl	$256, %eax
	leaq	1168(%r12), %rdi
	movups	%xmm0, 1016(%r12)
	movw	%ax, 1032(%r12)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rsi, %xmm0
	movw	%dx, 1056(%r12)
	movq	%rax, %xmm1
	movabsq	$281474976645120, %rax
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, 1112(%r12)
	movups	%xmm0, 1040(%r12)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 1120(%r12)
	movups	%xmm0, 1136(%r12)
	movb	$0, 1152(%r12)
	movl	$0, 1156(%r12)
	movb	$0, 1160(%r12)
	movl	$0, 1164(%r12)
	call	_ZN6icu_676number4impl15DecimalQuantityC1Ev@PLT
	pxor	%xmm0, %xmm0
	movabsq	$281474976645120, %rax
	movb	$0, 1240(%r12)
	movq	%rax, 1280(%r12)
	leaq	1336(%r12), %rdi
	movq	$0, 1244(%r12)
	movb	$0, 1252(%r12)
	movq	$0, 1272(%r12)
	movb	$0, 1320(%r12)
	movl	$0, 1324(%r12)
	movb	$0, 1328(%r12)
	movl	$0, 1332(%r12)
	movups	%xmm0, 1256(%r12)
	movups	%xmm0, 1288(%r12)
	movups	%xmm0, 1304(%r12)
	call	_ZN6icu_676number4impl15DecimalQuantityC1Ev@PLT
	movl	-64(%rbp), %r11d
	pxor	%xmm0, %xmm0
	movq	%r14, %r9
	leaq	1048(%r12), %rax
	movl	-60(%rbp), %r8d
	movq	-72(%rbp), %rdx
	movb	$0, 1408(%r12)
	movb	$0, 1420(%r12)
	movl	%r11d, %ecx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	$0, 1412(%r12)
	movq	$0, 1440(%r12)
	movq	%rax, 1448(%r12)
	movl	$0, 1456(%r12)
	movq	$0, 1464(%r12)
	movb	$0, 1472(%r12)
	movb	%bl, 1488(%r12)
	movups	%xmm0, 1424(%r12)
	call	_ZN6icu_676number4impl11CompactData8populateERKNS_6LocaleEPKc19UNumberCompactStyleNS1_11CompactTypeER10UErrorCode
	testb	%bl, %bl
	movq	-56(%rbp), %r10
	jne	.L303
	movq	%r10, 1480(%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L303:
	.cfi_restore_state
	addq	$40, %rsp
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%r10, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676number4impl14CompactHandler22precomputeAllModifiersERNS1_22MutablePatternModifierER10UErrorCode
	.cfi_endproc
.LFE3443:
	.size	_ZN6icu_676number4impl14CompactHandlerC2E19UNumberCompactStyleRKNS_6LocaleEPKcNS1_11CompactTypeEPKNS_11PluralRulesEPNS1_22MutablePatternModifierEbPKNS1_19MicroPropsGeneratorER10UErrorCode, .-_ZN6icu_676number4impl14CompactHandlerC2E19UNumberCompactStyleRKNS_6LocaleEPKcNS1_11CompactTypeEPKNS_11PluralRulesEPNS1_22MutablePatternModifierEbPKNS1_19MicroPropsGeneratorER10UErrorCode
	.globl	_ZN6icu_676number4impl14CompactHandlerC1E19UNumberCompactStyleRKNS_6LocaleEPKcNS1_11CompactTypeEPKNS_11PluralRulesEPNS1_22MutablePatternModifierEbPKNS1_19MicroPropsGeneratorER10UErrorCode
	.set	_ZN6icu_676number4impl14CompactHandlerC1E19UNumberCompactStyleRKNS_6LocaleEPKcNS1_11CompactTypeEPKNS_11PluralRulesEPNS1_22MutablePatternModifierEbPKNS1_19MicroPropsGeneratorER10UErrorCode,_ZN6icu_676number4impl14CompactHandlerC2E19UNumberCompactStyleRKNS_6LocaleEPKcNS1_11CompactTypeEPKNS_11PluralRulesEPNS1_22MutablePatternModifierEbPKNS1_19MicroPropsGeneratorER10UErrorCode
	.weak	_ZTSN6icu_677UMemoryE
	.section	.rodata._ZTSN6icu_677UMemoryE,"aG",@progbits,_ZTSN6icu_677UMemoryE,comdat
	.align 16
	.type	_ZTSN6icu_677UMemoryE, @object
	.size	_ZTSN6icu_677UMemoryE, 18
_ZTSN6icu_677UMemoryE:
	.string	"N6icu_677UMemoryE"
	.weak	_ZTIN6icu_677UMemoryE
	.section	.data.rel.ro._ZTIN6icu_677UMemoryE,"awG",@progbits,_ZTIN6icu_677UMemoryE,comdat
	.align 8
	.type	_ZTIN6icu_677UMemoryE, @object
	.size	_ZTIN6icu_677UMemoryE, 16
_ZTIN6icu_677UMemoryE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN6icu_677UMemoryE
	.weak	_ZTSN6icu_676number4impl11CompactData15CompactDataSinkE
	.section	.rodata._ZTSN6icu_676number4impl11CompactData15CompactDataSinkE,"aG",@progbits,_ZTSN6icu_676number4impl11CompactData15CompactDataSinkE,comdat
	.align 32
	.type	_ZTSN6icu_676number4impl11CompactData15CompactDataSinkE, @object
	.size	_ZTSN6icu_676number4impl11CompactData15CompactDataSinkE, 52
_ZTSN6icu_676number4impl11CompactData15CompactDataSinkE:
	.string	"N6icu_676number4impl11CompactData15CompactDataSinkE"
	.weak	_ZTIN6icu_676number4impl11CompactData15CompactDataSinkE
	.section	.data.rel.ro._ZTIN6icu_676number4impl11CompactData15CompactDataSinkE,"awG",@progbits,_ZTIN6icu_676number4impl11CompactData15CompactDataSinkE,comdat
	.align 8
	.type	_ZTIN6icu_676number4impl11CompactData15CompactDataSinkE, @object
	.size	_ZTIN6icu_676number4impl11CompactData15CompactDataSinkE, 24
_ZTIN6icu_676number4impl11CompactData15CompactDataSinkE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_676number4impl11CompactData15CompactDataSinkE
	.quad	_ZTIN6icu_6712ResourceSinkE
	.weak	_ZTSN6icu_676number4impl11CompactDataE
	.section	.rodata._ZTSN6icu_676number4impl11CompactDataE,"aG",@progbits,_ZTSN6icu_676number4impl11CompactDataE,comdat
	.align 32
	.type	_ZTSN6icu_676number4impl11CompactDataE, @object
	.size	_ZTSN6icu_676number4impl11CompactDataE, 35
_ZTSN6icu_676number4impl11CompactDataE:
	.string	"N6icu_676number4impl11CompactDataE"
	.weak	_ZTIN6icu_676number4impl11CompactDataE
	.section	.data.rel.ro._ZTIN6icu_676number4impl11CompactDataE,"awG",@progbits,_ZTIN6icu_676number4impl11CompactDataE,comdat
	.align 8
	.type	_ZTIN6icu_676number4impl11CompactDataE, @object
	.size	_ZTIN6icu_676number4impl11CompactDataE, 24
_ZTIN6icu_676number4impl11CompactDataE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_676number4impl11CompactDataE
	.quad	_ZTIN6icu_676number4impl18MultiplierProducerE
	.weak	_ZTSN6icu_676number4impl14CompactHandlerE
	.section	.rodata._ZTSN6icu_676number4impl14CompactHandlerE,"aG",@progbits,_ZTSN6icu_676number4impl14CompactHandlerE,comdat
	.align 32
	.type	_ZTSN6icu_676number4impl14CompactHandlerE, @object
	.size	_ZTSN6icu_676number4impl14CompactHandlerE, 38
_ZTSN6icu_676number4impl14CompactHandlerE:
	.string	"N6icu_676number4impl14CompactHandlerE"
	.weak	_ZTIN6icu_676number4impl14CompactHandlerE
	.section	.data.rel.ro._ZTIN6icu_676number4impl14CompactHandlerE,"awG",@progbits,_ZTIN6icu_676number4impl14CompactHandlerE,comdat
	.align 8
	.type	_ZTIN6icu_676number4impl14CompactHandlerE, @object
	.size	_ZTIN6icu_676number4impl14CompactHandlerE, 56
_ZTIN6icu_676number4impl14CompactHandlerE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN6icu_676number4impl14CompactHandlerE
	.long	0
	.long	2
	.quad	_ZTIN6icu_676number4impl19MicroPropsGeneratorE
	.quad	2
	.quad	_ZTIN6icu_677UMemoryE
	.quad	2
	.weak	_ZTVN6icu_676number4impl11CompactDataE
	.section	.data.rel.ro.local._ZTVN6icu_676number4impl11CompactDataE,"awG",@progbits,_ZTVN6icu_676number4impl11CompactDataE,comdat
	.align 8
	.type	_ZTVN6icu_676number4impl11CompactDataE, @object
	.size	_ZTVN6icu_676number4impl11CompactDataE, 40
_ZTVN6icu_676number4impl11CompactDataE:
	.quad	0
	.quad	_ZTIN6icu_676number4impl11CompactDataE
	.quad	_ZN6icu_676number4impl11CompactDataD1Ev
	.quad	_ZN6icu_676number4impl11CompactDataD0Ev
	.quad	_ZNK6icu_676number4impl11CompactData13getMultiplierEi
	.weak	_ZTVN6icu_676number4impl11CompactData15CompactDataSinkE
	.section	.data.rel.ro._ZTVN6icu_676number4impl11CompactData15CompactDataSinkE,"awG",@progbits,_ZTVN6icu_676number4impl11CompactData15CompactDataSinkE,comdat
	.align 8
	.type	_ZTVN6icu_676number4impl11CompactData15CompactDataSinkE, @object
	.size	_ZTVN6icu_676number4impl11CompactData15CompactDataSinkE, 48
_ZTVN6icu_676number4impl11CompactData15CompactDataSinkE:
	.quad	0
	.quad	_ZTIN6icu_676number4impl11CompactData15CompactDataSinkE
	.quad	_ZN6icu_676number4impl11CompactData15CompactDataSinkD1Ev
	.quad	_ZN6icu_676number4impl11CompactData15CompactDataSinkD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZN6icu_676number4impl11CompactData15CompactDataSink3putEPKcRNS_13ResourceValueEaR10UErrorCode
	.weak	_ZTVN6icu_676number4impl14CompactHandlerE
	.section	.data.rel.ro.local._ZTVN6icu_676number4impl14CompactHandlerE,"awG",@progbits,_ZTVN6icu_676number4impl14CompactHandlerE,comdat
	.align 8
	.type	_ZTVN6icu_676number4impl14CompactHandlerE, @object
	.size	_ZTVN6icu_676number4impl14CompactHandlerE, 40
_ZTVN6icu_676number4impl14CompactHandlerE:
	.quad	0
	.quad	_ZTIN6icu_676number4impl14CompactHandlerE
	.quad	_ZN6icu_676number4impl14CompactHandlerD1Ev
	.quad	_ZN6icu_676number4impl14CompactHandlerD0Ev
	.quad	_ZNK6icu_676number4impl14CompactHandler15processQuantityERNS1_15DecimalQuantityERNS1_10MicroPropsER10UErrorCode
	.section	.data.rel.ro,"aw"
	.align 8
.LC8:
	.quad	_ZTVN6icu_676number4impl17ParsedPatternInfoE+16
	.align 8
.LC9:
	.quad	_ZTVN6icu_6713UnicodeStringE+16
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
