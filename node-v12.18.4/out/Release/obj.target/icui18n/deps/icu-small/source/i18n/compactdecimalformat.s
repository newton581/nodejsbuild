	.file	"compactdecimalformat.cpp"
	.text
	.section	.text._ZNK6icu_6712NumberFormat9isLenientEv,"axG",@progbits,_ZNK6icu_6712NumberFormat9isLenientEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6712NumberFormat9isLenientEv
	.type	_ZNK6icu_6712NumberFormat9isLenientEv, @function
_ZNK6icu_6712NumberFormat9isLenientEv:
.LFB2365:
	.cfi_startproc
	endbr64
	movzbl	341(%rdi), %eax
	ret
	.cfi_endproc
.LFE2365:
	.size	_ZNK6icu_6712NumberFormat9isLenientEv, .-_ZNK6icu_6712NumberFormat9isLenientEv
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6720CompactDecimalFormat17getDynamicClassIDEv
	.type	_ZNK6icu_6720CompactDecimalFormat17getDynamicClassIDEv, @function
_ZNK6icu_6720CompactDecimalFormat17getDynamicClassIDEv:
.LFB3150:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6720CompactDecimalFormat16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3150:
	.size	_ZNK6icu_6720CompactDecimalFormat17getDynamicClassIDEv, .-_ZNK6icu_6720CompactDecimalFormat17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6720CompactDecimalFormat5parseERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE
	.type	_ZNK6icu_6720CompactDecimalFormat5parseERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE, @function
_ZNK6icu_6720CompactDecimalFormat5parseERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE:
.LFB3164:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3164:
	.size	_ZNK6icu_6720CompactDecimalFormat5parseERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE, .-_ZNK6icu_6720CompactDecimalFormat5parseERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6720CompactDecimalFormat5parseERKNS_13UnicodeStringERNS_11FormattableER10UErrorCode
	.type	_ZNK6icu_6720CompactDecimalFormat5parseERKNS_13UnicodeStringERNS_11FormattableER10UErrorCode, @function
_ZNK6icu_6720CompactDecimalFormat5parseERKNS_13UnicodeStringERNS_11FormattableER10UErrorCode:
.LFB3165:
	.cfi_startproc
	endbr64
	movl	$16, (%rcx)
	ret
	.cfi_endproc
.LFE3165:
	.size	_ZNK6icu_6720CompactDecimalFormat5parseERKNS_13UnicodeStringERNS_11FormattableER10UErrorCode, .-_ZNK6icu_6720CompactDecimalFormat5parseERKNS_13UnicodeStringERNS_11FormattableER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6720CompactDecimalFormat13parseCurrencyERKNS_13UnicodeStringERNS_13ParsePositionE
	.type	_ZNK6icu_6720CompactDecimalFormat13parseCurrencyERKNS_13UnicodeStringERNS_13ParsePositionE, @function
_ZNK6icu_6720CompactDecimalFormat13parseCurrencyERKNS_13UnicodeStringERNS_13ParsePositionE:
.LFB3166:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3166:
	.size	_ZNK6icu_6720CompactDecimalFormat13parseCurrencyERKNS_13UnicodeStringERNS_13ParsePositionE, .-_ZNK6icu_6720CompactDecimalFormat13parseCurrencyERKNS_13UnicodeStringERNS_13ParsePositionE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6720CompactDecimalFormat5cloneEv
	.type	_ZNK6icu_6720CompactDecimalFormat5cloneEv, @function
_ZNK6icu_6720CompactDecimalFormat5cloneEv:
.LFB3163:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movl	$368, %edi
	pushq	%r12
	.cfi_offset 12, -32
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L7
	movq	%rax, %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6713DecimalFormatC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6720CompactDecimalFormatE(%rip), %rax
	movq	%rax, (%r12)
.L7:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3163:
	.size	_ZNK6icu_6720CompactDecimalFormat5cloneEv, .-_ZNK6icu_6720CompactDecimalFormat5cloneEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720CompactDecimalFormatD2Ev
	.type	_ZN6icu_6720CompactDecimalFormatD2Ev, @function
_ZN6icu_6720CompactDecimalFormatD2Ev:
.LFB3159:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6720CompactDecimalFormatE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6713DecimalFormatD2Ev@PLT
	.cfi_endproc
.LFE3159:
	.size	_ZN6icu_6720CompactDecimalFormatD2Ev, .-_ZN6icu_6720CompactDecimalFormatD2Ev
	.globl	_ZN6icu_6720CompactDecimalFormatD1Ev
	.set	_ZN6icu_6720CompactDecimalFormatD1Ev,_ZN6icu_6720CompactDecimalFormatD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720CompactDecimalFormatD0Ev
	.type	_ZN6icu_6720CompactDecimalFormatD0Ev, @function
_ZN6icu_6720CompactDecimalFormatD0Ev:
.LFB3161:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6720CompactDecimalFormatE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6713DecimalFormatD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3161:
	.size	_ZN6icu_6720CompactDecimalFormatD0Ev, .-_ZN6icu_6720CompactDecimalFormatD0Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev:
.LFB3660:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3660:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei:
.LFB3663:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	cmpl	$40, %esi
	jg	.L29
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L17
	cmpb	$0, 12(%rbx)
	jne	.L30
.L21:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L17:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L21
	.cfi_endproc
.LFE3663:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EED2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev:
.LFB3666:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L33
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3666:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5EOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_:
.LFB3669:
	.cfi_startproc
	endbr64
	movzbl	12(%rsi), %eax
	movq	(%rsi), %r8
	movslq	8(%rsi), %rdx
	movb	%al, 12(%rdi)
	leaq	13(%rsi), %rax
	movq	%r8, (%rdi)
	movl	%edx, 8(%rdi)
	cmpq	%rax, %r8
	je	.L36
	movq	%rax, (%rsi)
	movl	$40, 8(%rsi)
	movb	$0, 12(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	leaq	13(%rdi), %r9
	movq	%r8, %rsi
	movq	%r9, (%rdi)
	movq	%r9, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE3669:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_:
.LFB3671:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 12(%rdi)
	movq	%rsi, %rbx
	jne	.L42
.L38:
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movzbl	12(%rbx), %eax
	movb	%al, 12(%r12)
	movq	(%rbx), %rsi
	leaq	13(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L43
	movq	%rsi, (%r12)
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L43:
	leaq	13(%r12), %rdi
	movslq	8(%rbx), %rdx
	movq	%rdi, (%r12)
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3671:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv:
.LFB3672:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE3672:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv:
.LFB3673:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3673:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv:
.LFB3674:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	addq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3674:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3675:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3675:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3676:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3676:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci:
.LFB3677:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L59
	testl	%edx, %edx
	jle	.L59
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 12(%rdi)
	jne	.L62
.L51:
	movq	%rsi, (%rbx)
	movl	%edx, 8(%rbx)
	movb	$0, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	%edx, -28(%rbp)
	movq	%rsi, -24(%rbp)
	call	uprv_free_67@PLT
	movl	-28(%rbp), %edx
	movq	-24(%rbp), %rsi
	jmp	.L51
	.cfi_endproc
.LFE3677:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii:
.LFB3678:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L66
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movl	%esi, %r13d
	movl	%edx, %r12d
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L66
	testl	%r12d, %r12d
	jg	.L73
	cmpb	$0, 12(%rbx)
	jne	.L74
.L68:
	movq	%r14, (%rbx)
	movq	%r14, %rax
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	.cfi_restore_state
	cmpl	%r12d, 8(%rbx)
	cmovle	8(%rbx), %r12d
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	je	.L68
.L74:
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L66:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3678:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, .-_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi:
.LFB3679:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 12(%rdi)
	movq	%rdi, %rbx
	je	.L76
	movq	(%rdi), %r8
.L77:
	leaq	13(%rbx), %rax
	movl	%r12d, 0(%r13)
	movq	%rax, (%rbx)
	movq	%r8, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L76:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L80
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	cmovg	%eax, %r12d
	movslq	%r12d, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L80
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L80:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3679:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, .-_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv:
.LFB3680:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L87
	ret
	.p2align 4,,10
	.p2align 3
.L87:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3680:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv:
.LFB3681:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3681:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_:
.LFB3682:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3682:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_:
.LFB3683:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3683:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5ERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_:
.LFB3685:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3685:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_:
.LFB3687:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3687:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720CompactDecimalFormat16getStaticClassIDEv
	.type	_ZN6icu_6720CompactDecimalFormat16getStaticClassIDEv, @function
_ZN6icu_6720CompactDecimalFormat16getStaticClassIDEv:
.LFB3149:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6720CompactDecimalFormat16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3149:
	.size	_ZN6icu_6720CompactDecimalFormat16getStaticClassIDEv, .-_ZN6icu_6720CompactDecimalFormat16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720CompactDecimalFormat14createInstanceERKNS_6LocaleE19UNumberCompactStyleR10UErrorCode
	.type	_ZN6icu_6720CompactDecimalFormat14createInstanceERKNS_6LocaleE19UNumberCompactStyleR10UErrorCode, @function
_ZN6icu_6720CompactDecimalFormat14createInstanceERKNS_6LocaleE19UNumberCompactStyleR10UErrorCode:
.LFB3151:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movl	$368, %edi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L94
	movl	$2816, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L97
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKNS_6LocaleER10UErrorCode@PLT
.L97:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713DecimalFormatC2EPKNS_20DecimalFormatSymbolsER10UErrorCode@PLT
	leaq	16+_ZTVN6icu_6720CompactDecimalFormatE(%rip), %rax
	movq	%rax, (%r12)
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L94
	movq	360(%r12), %rax
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%ebx, 12(%rax)
	movb	$0, 8(%rax)
	movl	$-2, 80(%rax)
	movl	$2, 112(%rax)
	call	_ZN6icu_6713DecimalFormat5touchER10UErrorCode@PLT
.L94:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3151:
	.size	_ZN6icu_6720CompactDecimalFormat14createInstanceERKNS_6LocaleE19UNumberCompactStyleR10UErrorCode, .-_ZN6icu_6720CompactDecimalFormat14createInstanceERKNS_6LocaleE19UNumberCompactStyleR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720CompactDecimalFormatC2ERKNS_6LocaleE19UNumberCompactStyleR10UErrorCode
	.type	_ZN6icu_6720CompactDecimalFormatC2ERKNS_6LocaleE19UNumberCompactStyleR10UErrorCode, @function
_ZN6icu_6720CompactDecimalFormatC2ERKNS_6LocaleE19UNumberCompactStyleR10UErrorCode:
.LFB3153:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movl	$2816, %edi
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L104
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKNS_6LocaleER10UErrorCode@PLT
.L104:
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713DecimalFormatC2EPKNS_20DecimalFormatSymbolsER10UErrorCode@PLT
	leaq	16+_ZTVN6icu_6720CompactDecimalFormatE(%rip), %rax
	movq	%rax, (%r14)
	movl	(%r12), %eax
	testl	%eax, %eax
	jle	.L110
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L110:
	.cfi_restore_state
	movq	360(%r14), %rax
	movq	%r12, %rsi
	movq	%r14, %rdi
	movl	%ebx, 12(%rax)
	movb	$0, 8(%rax)
	movl	$-2, 80(%rax)
	movl	$2, 112(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6713DecimalFormat5touchER10UErrorCode@PLT
	.cfi_endproc
.LFE3153:
	.size	_ZN6icu_6720CompactDecimalFormatC2ERKNS_6LocaleE19UNumberCompactStyleR10UErrorCode, .-_ZN6icu_6720CompactDecimalFormatC2ERKNS_6LocaleE19UNumberCompactStyleR10UErrorCode
	.globl	_ZN6icu_6720CompactDecimalFormatC1ERKNS_6LocaleE19UNumberCompactStyleR10UErrorCode
	.set	_ZN6icu_6720CompactDecimalFormatC1ERKNS_6LocaleE19UNumberCompactStyleR10UErrorCode,_ZN6icu_6720CompactDecimalFormatC2ERKNS_6LocaleE19UNumberCompactStyleR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720CompactDecimalFormatC2ERKS0_
	.type	_ZN6icu_6720CompactDecimalFormatC2ERKS0_, @function
_ZN6icu_6720CompactDecimalFormatC2ERKS0_:
.LFB3156:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_6713DecimalFormatC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6720CompactDecimalFormatE(%rip), %rax
	movq	%rax, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3156:
	.size	_ZN6icu_6720CompactDecimalFormatC2ERKS0_, .-_ZN6icu_6720CompactDecimalFormatC2ERKS0_
	.globl	_ZN6icu_6720CompactDecimalFormatC1ERKS0_
	.set	_ZN6icu_6720CompactDecimalFormatC1ERKS0_,_ZN6icu_6720CompactDecimalFormatC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720CompactDecimalFormataSERKS0_
	.type	_ZN6icu_6720CompactDecimalFormataSERKS0_, @function
_ZN6icu_6720CompactDecimalFormataSERKS0_:
.LFB3162:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN6icu_6713DecimalFormataSERKS0_@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3162:
	.size	_ZN6icu_6720CompactDecimalFormataSERKS0_, .-_ZN6icu_6720CompactDecimalFormataSERKS0_
	.weak	_ZTSN6icu_6720CompactDecimalFormatE
	.section	.rodata._ZTSN6icu_6720CompactDecimalFormatE,"aG",@progbits,_ZTSN6icu_6720CompactDecimalFormatE,comdat
	.align 32
	.type	_ZTSN6icu_6720CompactDecimalFormatE, @object
	.size	_ZTSN6icu_6720CompactDecimalFormatE, 32
_ZTSN6icu_6720CompactDecimalFormatE:
	.string	"N6icu_6720CompactDecimalFormatE"
	.weak	_ZTIN6icu_6720CompactDecimalFormatE
	.section	.data.rel.ro._ZTIN6icu_6720CompactDecimalFormatE,"awG",@progbits,_ZTIN6icu_6720CompactDecimalFormatE,comdat
	.align 8
	.type	_ZTIN6icu_6720CompactDecimalFormatE, @object
	.size	_ZTIN6icu_6720CompactDecimalFormatE, 24
_ZTIN6icu_6720CompactDecimalFormatE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6720CompactDecimalFormatE
	.quad	_ZTIN6icu_6713DecimalFormatE
	.weak	_ZTVN6icu_6720CompactDecimalFormatE
	.section	.data.rel.ro._ZTVN6icu_6720CompactDecimalFormatE,"awG",@progbits,_ZTVN6icu_6720CompactDecimalFormatE,comdat
	.align 8
	.type	_ZTVN6icu_6720CompactDecimalFormatE, @object
	.size	_ZTVN6icu_6720CompactDecimalFormatE, 616
_ZTVN6icu_6720CompactDecimalFormatE:
	.quad	0
	.quad	_ZTIN6icu_6720CompactDecimalFormatE
	.quad	_ZN6icu_6720CompactDecimalFormatD1Ev
	.quad	_ZN6icu_6720CompactDecimalFormatD0Ev
	.quad	_ZNK6icu_6720CompactDecimalFormat17getDynamicClassIDEv
	.quad	_ZNK6icu_6713DecimalFormateqERKNS_6FormatE
	.quad	_ZNK6icu_6720CompactDecimalFormat5cloneEv
	.quad	_ZNK6icu_6712NumberFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.quad	_ZNK6icu_6712NumberFormat6formatERKNS_11FormattableERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode
	.quad	_ZNK6icu_6712NumberFormat11parseObjectERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE
	.quad	_ZNK6icu_6713DecimalFormat6formatEdRNS_13UnicodeStringERNS_13FieldPositionE
	.quad	_ZNK6icu_6713DecimalFormat6formatEdRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.quad	_ZNK6icu_6713DecimalFormat6formatEdRNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode
	.quad	_ZNK6icu_6713DecimalFormat6formatEiRNS_13UnicodeStringERNS_13FieldPositionE
	.quad	_ZNK6icu_6713DecimalFormat6formatEiRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.quad	_ZNK6icu_6713DecimalFormat6formatEiRNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode
	.quad	_ZNK6icu_6713DecimalFormat6formatElRNS_13UnicodeStringERNS_13FieldPositionE
	.quad	_ZNK6icu_6713DecimalFormat6formatElRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.quad	_ZNK6icu_6713DecimalFormat6formatElRNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode
	.quad	_ZNK6icu_6713DecimalFormat6formatENS_11StringPieceERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode
	.quad	_ZNK6icu_6713DecimalFormat6formatERKNS_6number4impl15DecimalQuantityERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode
	.quad	_ZNK6icu_6713DecimalFormat6formatERKNS_6number4impl15DecimalQuantityERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.quad	_ZNK6icu_6720CompactDecimalFormat5parseERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE
	.quad	_ZNK6icu_6720CompactDecimalFormat5parseERKNS_13UnicodeStringERNS_11FormattableER10UErrorCode
	.quad	_ZNK6icu_6720CompactDecimalFormat13parseCurrencyERKNS_13UnicodeStringERNS_13ParsePositionE
	.quad	_ZN6icu_6713DecimalFormat19setParseIntegerOnlyEa
	.quad	_ZN6icu_6713DecimalFormat10setLenientEa
	.quad	_ZNK6icu_6712NumberFormat9isLenientEv
	.quad	_ZN6icu_6713DecimalFormat15setGroupingUsedEa
	.quad	_ZN6icu_6713DecimalFormat23setMaximumIntegerDigitsEi
	.quad	_ZN6icu_6713DecimalFormat23setMinimumIntegerDigitsEi
	.quad	_ZN6icu_6713DecimalFormat24setMaximumFractionDigitsEi
	.quad	_ZN6icu_6713DecimalFormat24setMinimumFractionDigitsEi
	.quad	_ZN6icu_6713DecimalFormat11setCurrencyEPKDsR10UErrorCode
	.quad	_ZN6icu_6712NumberFormat10setContextE15UDisplayContextR10UErrorCode
	.quad	_ZNK6icu_6712NumberFormat10getContextE19UDisplayContextTypeR10UErrorCode
	.quad	_ZNK6icu_6713DecimalFormat15getRoundingModeEv
	.quad	_ZN6icu_6713DecimalFormat15setRoundingModeENS_12NumberFormat13ERoundingModeE
	.quad	_ZNK6icu_6712NumberFormat20getEffectiveCurrencyEPDsR10UErrorCode
	.quad	_ZN6icu_6713DecimalFormat12setAttributeE22UNumberFormatAttributeiR10UErrorCode
	.quad	_ZNK6icu_6713DecimalFormat12getAttributeE22UNumberFormatAttributeR10UErrorCode
	.quad	_ZNK6icu_6713DecimalFormat23getDecimalFormatSymbolsEv
	.quad	_ZN6icu_6713DecimalFormat25adoptDecimalFormatSymbolsEPNS_20DecimalFormatSymbolsE
	.quad	_ZN6icu_6713DecimalFormat23setDecimalFormatSymbolsERKNS_20DecimalFormatSymbolsE
	.quad	_ZNK6icu_6713DecimalFormat21getCurrencyPluralInfoEv
	.quad	_ZN6icu_6713DecimalFormat23adoptCurrencyPluralInfoEPNS_18CurrencyPluralInfoE
	.quad	_ZN6icu_6713DecimalFormat21setCurrencyPluralInfoERKNS_18CurrencyPluralInfoE
	.quad	_ZN6icu_6713DecimalFormat17setPositivePrefixERKNS_13UnicodeStringE
	.quad	_ZN6icu_6713DecimalFormat17setNegativePrefixERKNS_13UnicodeStringE
	.quad	_ZN6icu_6713DecimalFormat17setPositiveSuffixERKNS_13UnicodeStringE
	.quad	_ZN6icu_6713DecimalFormat17setNegativeSuffixERKNS_13UnicodeStringE
	.quad	_ZN6icu_6713DecimalFormat13setMultiplierEi
	.quad	_ZNK6icu_6713DecimalFormat20getRoundingIncrementEv
	.quad	_ZN6icu_6713DecimalFormat20setRoundingIncrementEd
	.quad	_ZNK6icu_6713DecimalFormat14getFormatWidthEv
	.quad	_ZN6icu_6713DecimalFormat14setFormatWidthEi
	.quad	_ZNK6icu_6713DecimalFormat21getPadCharacterStringEv
	.quad	_ZN6icu_6713DecimalFormat15setPadCharacterERKNS_13UnicodeStringE
	.quad	_ZNK6icu_6713DecimalFormat14getPadPositionEv
	.quad	_ZN6icu_6713DecimalFormat14setPadPositionENS0_12EPadPositionE
	.quad	_ZNK6icu_6713DecimalFormat20isScientificNotationEv
	.quad	_ZN6icu_6713DecimalFormat21setScientificNotationEa
	.quad	_ZNK6icu_6713DecimalFormat24getMinimumExponentDigitsEv
	.quad	_ZN6icu_6713DecimalFormat24setMinimumExponentDigitsEa
	.quad	_ZNK6icu_6713DecimalFormat25isExponentSignAlwaysShownEv
	.quad	_ZN6icu_6713DecimalFormat26setExponentSignAlwaysShownEa
	.quad	_ZN6icu_6713DecimalFormat15setGroupingSizeEi
	.quad	_ZN6icu_6713DecimalFormat24setSecondaryGroupingSizeEi
	.quad	_ZN6icu_6713DecimalFormat30setDecimalSeparatorAlwaysShownEa
	.quad	_ZN6icu_6713DecimalFormat30setDecimalPatternMatchRequiredEa
	.quad	_ZNK6icu_6713DecimalFormat9toPatternERNS_13UnicodeStringE
	.quad	_ZNK6icu_6713DecimalFormat18toLocalizedPatternERNS_13UnicodeStringE
	.quad	_ZN6icu_6713DecimalFormat12applyPatternERKNS_13UnicodeStringER11UParseErrorR10UErrorCode
	.quad	_ZN6icu_6713DecimalFormat12applyPatternERKNS_13UnicodeStringER10UErrorCode
	.quad	_ZN6icu_6713DecimalFormat21applyLocalizedPatternERKNS_13UnicodeStringER11UParseErrorR10UErrorCode
	.quad	_ZN6icu_6713DecimalFormat21applyLocalizedPatternERKNS_13UnicodeStringER10UErrorCode
	.quad	_ZN6icu_6713DecimalFormat11setCurrencyEPKDs
	.local	_ZZN6icu_6720CompactDecimalFormat16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6720CompactDecimalFormat16getStaticClassIDEvE7classID,1,1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
