	.file	"number_skeletons.cpp"
	.text
	.p2align 4
	.type	_ZN12_GLOBAL__N_122cleanupNumberSkeletonsEv, @function
_ZN12_GLOBAL__N_122cleanupNumberSkeletonsEv:
.LFB3941:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	_ZN12_GLOBAL__N_119kSerializedStemTrieE(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	uprv_free_67@PLT
	movl	$1, %eax
	movq	$0, _ZN12_GLOBAL__N_119kSerializedStemTrieE(%rip)
	movl	$0, _ZN12_GLOBAL__N_124gNumberSkeletonsInitOnceE(%rip)
	mfence
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3941:
	.size	_ZN12_GLOBAL__N_122cleanupNumberSkeletonsEv, .-_ZN12_GLOBAL__N_122cleanupNumberSkeletonsEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev:
.LFB4262:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE4262:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei:
.LFB4265:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	cmpl	$40, %esi
	jg	.L17
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L5
	cmpb	$0, 12(%rbx)
	jne	.L18
.L9:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L5:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L9
	.cfi_endproc
.LFE4265:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EED2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev:
.LFB4268:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L21
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE4268:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5EOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_:
.LFB4271:
	.cfi_startproc
	endbr64
	movzbl	12(%rsi), %eax
	movq	(%rsi), %r8
	movslq	8(%rsi), %rdx
	movb	%al, 12(%rdi)
	leaq	13(%rsi), %rax
	movq	%r8, (%rdi)
	movl	%edx, 8(%rdi)
	cmpq	%rax, %r8
	je	.L24
	movq	%rax, (%rsi)
	movl	$40, 8(%rsi)
	movb	$0, 12(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	leaq	13(%rdi), %r9
	movq	%r8, %rsi
	movq	%r9, (%rdi)
	movq	%r9, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE4271:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_:
.LFB4273:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 12(%rdi)
	movq	%rsi, %rbx
	jne	.L30
.L26:
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movzbl	12(%rbx), %eax
	movb	%al, 12(%r12)
	movq	(%rbx), %rsi
	leaq	13(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L31
	movq	%rsi, (%r12)
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L31:
	leaq	13(%r12), %rdi
	movslq	8(%rbx), %rdx
	movq	%rdi, (%r12)
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4273:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv:
.LFB4274:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE4274:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv:
.LFB4275:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE4275:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv:
.LFB4276:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	addq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE4276:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB4277:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE4277:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB4278:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE4278:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci:
.LFB4279:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L47
	testl	%edx, %edx
	jle	.L47
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 12(%rdi)
	jne	.L50
.L39:
	movq	%rsi, (%rbx)
	movl	%edx, 8(%rbx)
	movb	$0, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	%edx, -28(%rbp)
	movq	%rsi, -24(%rbp)
	call	uprv_free_67@PLT
	movl	-28(%rbp), %edx
	movq	-24(%rbp), %rsi
	jmp	.L39
	.cfi_endproc
.LFE4279:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii:
.LFB4280:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L54
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movl	%esi, %r13d
	movl	%edx, %r12d
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L54
	testl	%r12d, %r12d
	jg	.L61
	cmpb	$0, 12(%rbx)
	jne	.L62
.L56:
	movq	%r14, (%rbx)
	movq	%r14, %rax
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L61:
	.cfi_restore_state
	cmpl	%r12d, 8(%rbx)
	cmovle	8(%rbx), %r12d
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	je	.L56
.L62:
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L54:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4280:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, .-_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi:
.LFB4281:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 12(%rdi)
	movq	%rdi, %rbx
	je	.L64
	movq	(%rdi), %r8
.L65:
	leaq	13(%rbx), %rax
	movl	%r12d, 0(%r13)
	movq	%rax, (%rbx)
	movq	%r8, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L64:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L68
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	cmovg	%eax, %r12d
	movslq	%r12d, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L68
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L68:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4281:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, .-_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv:
.LFB4282:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L75
	ret
	.p2align 4,,10
	.p2align 3
.L75:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE4282:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv:
.LFB4283:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE4283:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_:
.LFB4284:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4284:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_:
.LFB4285:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE4285:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5ERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_:
.LFB4287:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4287:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_:
.LFB4289:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4289:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.section	.text.unlikely,"ax",@progbits
.LCOLDB0:
	.text
.LHOTB0:
	.p2align 4
	.globl	_ZN6icu_676number4impl14stem_to_object8notationENS1_8skeleton8StemEnumE
	.type	_ZN6icu_676number4impl14stem_to_object8notationENS1_8skeleton8StemEnumE, @function
_ZN6icu_676number4impl14stem_to_object8notationENS1_8skeleton8StemEnumE:
.LFB3944:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	cmpl	$4, %edi
	ja	.L82
	leaq	.L84(%rip), %rdx
	movl	%edi, %edi
	movslq	(%rdx,%rdi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L84:
	.long	.L88-.L84
	.long	.L87-.L84
	.long	.L86-.L84
	.long	.L85-.L84
	.long	.L83-.L84
	.text
	.p2align 4,,10
	.p2align 3
.L85:
	call	_ZN6icu_676number8Notation11engineeringEv@PLT
	movq	%rax, -12(%rbp)
	movq	-12(%rbp), %rax
	movl	%edx, -4(%rbp)
	movl	-4(%rbp), %edx
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L83:
	.cfi_restore_state
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676number8Notation6simpleEv@PLT
	.p2align 4,,10
	.p2align 3
.L88:
	.cfi_restore_state
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676number8Notation12compactShortEv@PLT
	.p2align 4,,10
	.p2align 3
.L87:
	.cfi_restore_state
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676number8Notation11compactLongEv@PLT
	.p2align 4,,10
	.p2align 3
.L86:
	.cfi_restore_state
	call	_ZN6icu_676number8Notation10scientificEv@PLT
	movq	%rax, -12(%rbp)
	movq	-12(%rbp), %rax
	movl	%edx, -4(%rbp)
	movl	-4(%rbp), %edx
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_676number4impl14stem_to_object8notationENS1_8skeleton8StemEnumE.cold, @function
_ZN6icu_676number4impl14stem_to_object8notationENS1_8skeleton8StemEnumE.cold:
.LFSB3944:
.L82:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	call	abort@PLT
	.cfi_endproc
.LFE3944:
	.text
	.size	_ZN6icu_676number4impl14stem_to_object8notationENS1_8skeleton8StemEnumE, .-_ZN6icu_676number4impl14stem_to_object8notationENS1_8skeleton8StemEnumE
	.section	.text.unlikely
	.size	_ZN6icu_676number4impl14stem_to_object8notationENS1_8skeleton8StemEnumE.cold, .-_ZN6icu_676number4impl14stem_to_object8notationENS1_8skeleton8StemEnumE.cold
.LCOLDE0:
	.text
.LHOTE0:
	.section	.text.unlikely
.LCOLDB1:
	.text
.LHOTB1:
	.p2align 4
	.globl	_ZN6icu_676number4impl14stem_to_object9precisionENS1_8skeleton8StemEnumE
	.type	_ZN6icu_676number4impl14stem_to_object9precisionENS1_8skeleton8StemEnumE, @function
_ZN6icu_676number4impl14stem_to_object9precisionENS1_8skeleton8StemEnumE:
.LFB3946:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpl	$11, %esi
	je	.L92
	ja	.L93
	cmpl	$9, %esi
	jne	.L101
	leaq	-64(%rbp), %rdi
	call	_ZN6icu_676number9Precision7integerEv@PLT
	movq	-48(%rbp), %rax
	movdqa	-64(%rbp), %xmm0
	movq	%rax, 16(%r12)
	movl	-40(%rbp), %eax
	movups	%xmm0, (%r12)
	movl	%eax, 24(%r12)
.L91:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L102
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L101:
	.cfi_restore_state
	cmpl	$10, %esi
	jne	.L103
	call	_ZN6icu_676number9Precision9unlimitedEv@PLT
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L93:
	cmpl	$12, %esi
	jne	.L104
	leaq	-64(%rbp), %rdi
	movl	$1, %esi
	call	_ZN6icu_676number9Precision8currencyE14UCurrencyUsage@PLT
	movq	-48(%rbp), %rax
	movdqa	-64(%rbp), %xmm2
	movq	%rax, 16(%r12)
	movl	-40(%rbp), %eax
	movups	%xmm2, (%r12)
	movl	%eax, 24(%r12)
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L92:
	leaq	-64(%rbp), %rdi
	xorl	%esi, %esi
	call	_ZN6icu_676number9Precision8currencyE14UCurrencyUsage@PLT
	movq	-48(%rbp), %rax
	movdqa	-64(%rbp), %xmm1
	movq	%rax, 16(%r12)
	movl	-40(%rbp), %eax
	movups	%xmm1, (%r12)
	movl	%eax, 24(%r12)
	jmp	.L91
.L102:
	call	__stack_chk_fail@PLT
.L104:
	jmp	.L96
.L103:
	jmp	.L96
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_676number4impl14stem_to_object9precisionENS1_8skeleton8StemEnumE.cold, @function
_ZN6icu_676number4impl14stem_to_object9precisionENS1_8skeleton8StemEnumE.cold:
.LFSB3946:
.L96:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	call	abort@PLT
	.cfi_endproc
.LFE3946:
	.text
	.size	_ZN6icu_676number4impl14stem_to_object9precisionENS1_8skeleton8StemEnumE, .-_ZN6icu_676number4impl14stem_to_object9precisionENS1_8skeleton8StemEnumE
	.section	.text.unlikely
	.size	_ZN6icu_676number4impl14stem_to_object9precisionENS1_8skeleton8StemEnumE.cold, .-_ZN6icu_676number4impl14stem_to_object9precisionENS1_8skeleton8StemEnumE.cold
.LCOLDE1:
	.text
.LHOTE1:
	.section	.text.unlikely
.LCOLDB2:
	.text
.LHOTB2:
	.p2align 4
	.globl	_ZN6icu_676number4impl14stem_to_object12roundingModeENS1_8skeleton8StemEnumE
	.type	_ZN6icu_676number4impl14stem_to_object12roundingModeENS1_8skeleton8StemEnumE, @function
_ZN6icu_676number4impl14stem_to_object12roundingModeENS1_8skeleton8StemEnumE:
.LFB3947:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leal	-13(%rdi), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpl	$7, %eax
	ja	.L106
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_676number4impl14stem_to_object12roundingModeENS1_8skeleton8StemEnumE.cold, @function
_ZN6icu_676number4impl14stem_to_object12roundingModeENS1_8skeleton8StemEnumE.cold:
.LFSB3947:
.L106:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	call	abort@PLT
	.cfi_endproc
.LFE3947:
	.text
	.size	_ZN6icu_676number4impl14stem_to_object12roundingModeENS1_8skeleton8StemEnumE, .-_ZN6icu_676number4impl14stem_to_object12roundingModeENS1_8skeleton8StemEnumE
	.section	.text.unlikely
	.size	_ZN6icu_676number4impl14stem_to_object12roundingModeENS1_8skeleton8StemEnumE.cold, .-_ZN6icu_676number4impl14stem_to_object12roundingModeENS1_8skeleton8StemEnumE.cold
.LCOLDE2:
	.text
.LHOTE2:
	.p2align 4
	.globl	_ZN6icu_676number4impl14stem_to_object16groupingStrategyENS1_8skeleton8StemEnumE
	.type	_ZN6icu_676number4impl14stem_to_object16groupingStrategyENS1_8skeleton8StemEnumE, @function
_ZN6icu_676number4impl14stem_to_object16groupingStrategyENS1_8skeleton8StemEnumE:
.LFB3948:
	.cfi_startproc
	endbr64
	leal	-21(%rdi), %eax
	movl	$5, %edx
	cmpl	$5, %eax
	cmovnb	%edx, %eax
	ret
	.cfi_endproc
.LFE3948:
	.size	_ZN6icu_676number4impl14stem_to_object16groupingStrategyENS1_8skeleton8StemEnumE, .-_ZN6icu_676number4impl14stem_to_object16groupingStrategyENS1_8skeleton8StemEnumE
	.p2align 4
	.globl	_ZN6icu_676number4impl14stem_to_object9unitWidthENS1_8skeleton8StemEnumE
	.type	_ZN6icu_676number4impl14stem_to_object9unitWidthENS1_8skeleton8StemEnumE, @function
_ZN6icu_676number4impl14stem_to_object9unitWidthENS1_8skeleton8StemEnumE:
.LFB3949:
	.cfi_startproc
	endbr64
	leal	-27(%rdi), %eax
	movl	$5, %edx
	cmpl	$5, %eax
	cmovnb	%edx, %eax
	ret
	.cfi_endproc
.LFE3949:
	.size	_ZN6icu_676number4impl14stem_to_object9unitWidthENS1_8skeleton8StemEnumE, .-_ZN6icu_676number4impl14stem_to_object9unitWidthENS1_8skeleton8StemEnumE
	.p2align 4
	.globl	_ZN6icu_676number4impl14stem_to_object11signDisplayENS1_8skeleton8StemEnumE
	.type	_ZN6icu_676number4impl14stem_to_object11signDisplayENS1_8skeleton8StemEnumE, @function
_ZN6icu_676number4impl14stem_to_object11signDisplayENS1_8skeleton8StemEnumE:
.LFB3950:
	.cfi_startproc
	endbr64
	leal	-32(%rdi), %eax
	movl	$7, %edx
	cmpl	$7, %eax
	cmovnb	%edx, %eax
	ret
	.cfi_endproc
.LFE3950:
	.size	_ZN6icu_676number4impl14stem_to_object11signDisplayENS1_8skeleton8StemEnumE, .-_ZN6icu_676number4impl14stem_to_object11signDisplayENS1_8skeleton8StemEnumE
	.p2align 4
	.globl	_ZN6icu_676number4impl14stem_to_object23decimalSeparatorDisplayENS1_8skeleton8StemEnumE
	.type	_ZN6icu_676number4impl14stem_to_object23decimalSeparatorDisplayENS1_8skeleton8StemEnumE, @function
_ZN6icu_676number4impl14stem_to_object23decimalSeparatorDisplayENS1_8skeleton8StemEnumE:
.LFB3951:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpl	$39, %edi
	je	.L118
	xorl	%eax, %eax
	cmpl	$40, %edi
	setne	%al
	addl	$1, %eax
.L118:
	ret
	.cfi_endproc
.LFE3951:
	.size	_ZN6icu_676number4impl14stem_to_object23decimalSeparatorDisplayENS1_8skeleton8StemEnumE, .-_ZN6icu_676number4impl14stem_to_object23decimalSeparatorDisplayENS1_8skeleton8StemEnumE
	.section	.rodata.str2.8,"aMS",@progbits,2
	.align 8
.LC3:
	.string	"r"
	.string	"o"
	.string	"u"
	.string	"n"
	.string	"d"
	.string	"i"
	.string	"n"
	.string	"g"
	.string	"-"
	.string	"m"
	.string	"o"
	.string	"d"
	.string	"e"
	.string	"-"
	.string	"c"
	.string	"e"
	.string	"i"
	.string	"l"
	.string	"i"
	.string	"n"
	.string	"g"
	.string	""
	.string	""
	.align 8
.LC4:
	.string	"r"
	.string	"o"
	.string	"u"
	.string	"n"
	.string	"d"
	.string	"i"
	.string	"n"
	.string	"g"
	.string	"-"
	.string	"m"
	.string	"o"
	.string	"d"
	.string	"e"
	.string	"-"
	.string	"f"
	.string	"l"
	.string	"o"
	.string	"o"
	.string	"r"
	.string	""
	.string	""
	.align 8
.LC5:
	.string	"r"
	.string	"o"
	.string	"u"
	.string	"n"
	.string	"d"
	.string	"i"
	.string	"n"
	.string	"g"
	.string	"-"
	.string	"m"
	.string	"o"
	.string	"d"
	.string	"e"
	.string	"-"
	.string	"d"
	.string	"o"
	.string	"w"
	.string	"n"
	.string	""
	.string	""
	.align 8
.LC6:
	.string	"r"
	.string	"o"
	.string	"u"
	.string	"n"
	.string	"d"
	.string	"i"
	.string	"n"
	.string	"g"
	.string	"-"
	.string	"m"
	.string	"o"
	.string	"d"
	.string	"e"
	.string	"-"
	.string	"u"
	.string	"p"
	.string	""
	.string	""
	.align 8
.LC7:
	.string	"r"
	.string	"o"
	.string	"u"
	.string	"n"
	.string	"d"
	.string	"i"
	.string	"n"
	.string	"g"
	.string	"-"
	.string	"m"
	.string	"o"
	.string	"d"
	.string	"e"
	.string	"-"
	.string	"h"
	.string	"a"
	.string	"l"
	.string	"f"
	.string	"-"
	.string	"e"
	.string	"v"
	.string	"e"
	.string	"n"
	.string	""
	.string	""
	.align 8
.LC8:
	.string	"r"
	.string	"o"
	.string	"u"
	.string	"n"
	.string	"d"
	.string	"i"
	.string	"n"
	.string	"g"
	.string	"-"
	.string	"m"
	.string	"o"
	.string	"d"
	.string	"e"
	.string	"-"
	.string	"h"
	.string	"a"
	.string	"l"
	.string	"f"
	.string	"-"
	.string	"d"
	.string	"o"
	.string	"w"
	.string	"n"
	.string	""
	.string	""
	.align 8
.LC9:
	.string	"r"
	.string	"o"
	.string	"u"
	.string	"n"
	.string	"d"
	.string	"i"
	.string	"n"
	.string	"g"
	.string	"-"
	.string	"m"
	.string	"o"
	.string	"d"
	.string	"e"
	.string	"-"
	.string	"h"
	.string	"a"
	.string	"l"
	.string	"f"
	.string	"-"
	.string	"u"
	.string	"p"
	.string	""
	.string	""
	.align 8
.LC10:
	.string	"r"
	.string	"o"
	.string	"u"
	.string	"n"
	.string	"d"
	.string	"i"
	.string	"n"
	.string	"g"
	.string	"-"
	.string	"m"
	.string	"o"
	.string	"d"
	.string	"e"
	.string	"-"
	.string	"u"
	.string	"n"
	.string	"n"
	.string	"e"
	.string	"c"
	.string	"e"
	.string	"s"
	.string	"s"
	.string	"a"
	.string	"r"
	.string	"y"
	.string	""
	.string	""
	.section	.text.unlikely
.LCOLDB11:
	.text
.LHOTB11:
	.p2align 4
	.globl	_ZN6icu_676number4impl19enum_to_stem_string12roundingModeE25UNumberFormatRoundingModeRNS_13UnicodeStringE
	.type	_ZN6icu_676number4impl19enum_to_stem_string12roundingModeE25UNumberFormatRoundingModeRNS_13UnicodeStringE, @function
_ZN6icu_676number4impl19enum_to_stem_string12roundingModeE25UNumberFormatRoundingModeRNS_13UnicodeStringE:
.LFB3952:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpl	$7, %edi
	ja	.L124
	leaq	.L126(%rip), %rdx
	movl	%edi, %edi
	movq	%rsi, %r8
	movl	$-1, %ecx
	movslq	(%rdx,%rdi,4), %rax
	addq	%rdx, %rax
	xorl	%edx, %edx
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L126:
	.long	.L133-.L126
	.long	.L132-.L126
	.long	.L131-.L126
	.long	.L130-.L126
	.long	.L129-.L126
	.long	.L128-.L126
	.long	.L127-.L126
	.long	.L125-.L126
	.text
	.p2align 4,,10
	.p2align 3
.L127:
	leaq	.LC9(%rip), %rsi
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC9(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L125:
	.cfi_restore_state
	leaq	.LC10(%rip), %rsi
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC10(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L133:
	.cfi_restore_state
	leaq	.LC3(%rip), %rsi
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC3(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L132:
	.cfi_restore_state
	leaq	.LC4(%rip), %rsi
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC4(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L131:
	.cfi_restore_state
	leaq	.LC5(%rip), %rsi
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC5(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L130:
	.cfi_restore_state
	leaq	.LC6(%rip), %rsi
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC6(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L129:
	.cfi_restore_state
	leaq	.LC7(%rip), %rsi
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC7(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L128:
	.cfi_restore_state
	leaq	.LC8(%rip), %rsi
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC8(%rip), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_676number4impl19enum_to_stem_string12roundingModeE25UNumberFormatRoundingModeRNS_13UnicodeStringE.cold, @function
_ZN6icu_676number4impl19enum_to_stem_string12roundingModeE25UNumberFormatRoundingModeRNS_13UnicodeStringE.cold:
.LFSB3952:
.L124:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	call	abort@PLT
	.cfi_endproc
.LFE3952:
	.text
	.size	_ZN6icu_676number4impl19enum_to_stem_string12roundingModeE25UNumberFormatRoundingModeRNS_13UnicodeStringE, .-_ZN6icu_676number4impl19enum_to_stem_string12roundingModeE25UNumberFormatRoundingModeRNS_13UnicodeStringE
	.section	.text.unlikely
	.size	_ZN6icu_676number4impl19enum_to_stem_string12roundingModeE25UNumberFormatRoundingModeRNS_13UnicodeStringE.cold, .-_ZN6icu_676number4impl19enum_to_stem_string12roundingModeE25UNumberFormatRoundingModeRNS_13UnicodeStringE.cold
.LCOLDE11:
	.text
.LHOTE11:
	.section	.rodata.str2.2,"aMS",@progbits,2
	.align 2
.LC12:
	.string	"g"
	.string	"r"
	.string	"o"
	.string	"u"
	.string	"p"
	.string	"-"
	.string	"o"
	.string	"f"
	.string	"f"
	.string	""
	.string	""
	.align 2
.LC13:
	.string	"g"
	.string	"r"
	.string	"o"
	.string	"u"
	.string	"p"
	.string	"-"
	.string	"m"
	.string	"i"
	.string	"n"
	.string	"2"
	.string	""
	.string	""
	.align 2
.LC14:
	.string	"g"
	.string	"r"
	.string	"o"
	.string	"u"
	.string	"p"
	.string	"-"
	.string	"a"
	.string	"u"
	.string	"t"
	.string	"o"
	.string	""
	.string	""
	.section	.rodata.str2.8
	.align 8
.LC15:
	.string	"g"
	.string	"r"
	.string	"o"
	.string	"u"
	.string	"p"
	.string	"-"
	.string	"o"
	.string	"n"
	.string	"-"
	.string	"a"
	.string	"l"
	.string	"i"
	.string	"g"
	.string	"n"
	.string	"e"
	.string	"d"
	.string	""
	.string	""
	.align 8
.LC16:
	.string	"g"
	.string	"r"
	.string	"o"
	.string	"u"
	.string	"p"
	.string	"-"
	.string	"t"
	.string	"h"
	.string	"o"
	.string	"u"
	.string	"s"
	.string	"a"
	.string	"n"
	.string	"d"
	.string	"s"
	.string	""
	.string	""
	.section	.text.unlikely
.LCOLDB17:
	.text
.LHOTB17:
	.p2align 4
	.globl	_ZN6icu_676number4impl19enum_to_stem_string16groupingStrategyE23UNumberGroupingStrategyRNS_13UnicodeStringE
	.type	_ZN6icu_676number4impl19enum_to_stem_string16groupingStrategyE23UNumberGroupingStrategyRNS_13UnicodeStringE, @function
_ZN6icu_676number4impl19enum_to_stem_string16groupingStrategyE23UNumberGroupingStrategyRNS_13UnicodeStringE:
.LFB3953:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpl	$4, %edi
	ja	.L137
	leaq	.L139(%rip), %rdx
	movl	%edi, %edi
	movq	%rsi, %r8
	movl	$-1, %ecx
	movslq	(%rdx,%rdi,4), %rax
	addq	%rdx, %rax
	xorl	%edx, %edx
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L139:
	.long	.L143-.L139
	.long	.L142-.L139
	.long	.L141-.L139
	.long	.L140-.L139
	.long	.L138-.L139
	.text
	.p2align 4,,10
	.p2align 3
.L140:
	leaq	.LC15(%rip), %rsi
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC15(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L138:
	.cfi_restore_state
	leaq	.LC16(%rip), %rsi
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC16(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L143:
	.cfi_restore_state
	leaq	.LC12(%rip), %rsi
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC12(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L142:
	.cfi_restore_state
	leaq	.LC13(%rip), %rsi
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC13(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L141:
	.cfi_restore_state
	leaq	.LC14(%rip), %rsi
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC14(%rip), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_676number4impl19enum_to_stem_string16groupingStrategyE23UNumberGroupingStrategyRNS_13UnicodeStringE.cold, @function
_ZN6icu_676number4impl19enum_to_stem_string16groupingStrategyE23UNumberGroupingStrategyRNS_13UnicodeStringE.cold:
.LFSB3953:
.L137:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	call	abort@PLT
	.cfi_endproc
.LFE3953:
	.text
	.size	_ZN6icu_676number4impl19enum_to_stem_string16groupingStrategyE23UNumberGroupingStrategyRNS_13UnicodeStringE, .-_ZN6icu_676number4impl19enum_to_stem_string16groupingStrategyE23UNumberGroupingStrategyRNS_13UnicodeStringE
	.section	.text.unlikely
	.size	_ZN6icu_676number4impl19enum_to_stem_string16groupingStrategyE23UNumberGroupingStrategyRNS_13UnicodeStringE.cold, .-_ZN6icu_676number4impl19enum_to_stem_string16groupingStrategyE23UNumberGroupingStrategyRNS_13UnicodeStringE.cold
.LCOLDE17:
	.text
.LHOTE17:
	.section	.rodata.str2.8
	.align 8
.LC18:
	.string	"u"
	.string	"n"
	.string	"i"
	.string	"t"
	.string	"-"
	.string	"w"
	.string	"i"
	.string	"d"
	.string	"t"
	.string	"h"
	.string	"-"
	.string	"n"
	.string	"a"
	.string	"r"
	.string	"r"
	.string	"o"
	.string	"w"
	.string	""
	.string	""
	.align 8
.LC19:
	.string	"u"
	.string	"n"
	.string	"i"
	.string	"t"
	.string	"-"
	.string	"w"
	.string	"i"
	.string	"d"
	.string	"t"
	.string	"h"
	.string	"-"
	.string	"s"
	.string	"h"
	.string	"o"
	.string	"r"
	.string	"t"
	.string	""
	.string	""
	.align 8
.LC20:
	.string	"u"
	.string	"n"
	.string	"i"
	.string	"t"
	.string	"-"
	.string	"w"
	.string	"i"
	.string	"d"
	.string	"t"
	.string	"h"
	.string	"-"
	.string	"f"
	.string	"u"
	.string	"l"
	.string	"l"
	.string	"-"
	.string	"n"
	.string	"a"
	.string	"m"
	.string	"e"
	.string	""
	.string	""
	.align 8
.LC21:
	.string	"u"
	.string	"n"
	.string	"i"
	.string	"t"
	.string	"-"
	.string	"w"
	.string	"i"
	.string	"d"
	.string	"t"
	.string	"h"
	.string	"-"
	.string	"i"
	.string	"s"
	.string	"o"
	.string	"-"
	.string	"c"
	.string	"o"
	.string	"d"
	.string	"e"
	.string	""
	.string	""
	.align 8
.LC22:
	.string	"u"
	.string	"n"
	.string	"i"
	.string	"t"
	.string	"-"
	.string	"w"
	.string	"i"
	.string	"d"
	.string	"t"
	.string	"h"
	.string	"-"
	.string	"h"
	.string	"i"
	.string	"d"
	.string	"d"
	.string	"e"
	.string	"n"
	.string	""
	.string	""
	.section	.text.unlikely
.LCOLDB23:
	.text
.LHOTB23:
	.p2align 4
	.globl	_ZN6icu_676number4impl19enum_to_stem_string9unitWidthE16UNumberUnitWidthRNS_13UnicodeStringE
	.type	_ZN6icu_676number4impl19enum_to_stem_string9unitWidthE16UNumberUnitWidthRNS_13UnicodeStringE, @function
_ZN6icu_676number4impl19enum_to_stem_string9unitWidthE16UNumberUnitWidthRNS_13UnicodeStringE:
.LFB3954:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpl	$4, %edi
	ja	.L147
	leaq	.L149(%rip), %rdx
	movl	%edi, %edi
	movq	%rsi, %r8
	movl	$-1, %ecx
	movslq	(%rdx,%rdi,4), %rax
	addq	%rdx, %rax
	xorl	%edx, %edx
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L149:
	.long	.L153-.L149
	.long	.L152-.L149
	.long	.L151-.L149
	.long	.L150-.L149
	.long	.L148-.L149
	.text
	.p2align 4,,10
	.p2align 3
.L150:
	leaq	.LC21(%rip), %rsi
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC21(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L148:
	.cfi_restore_state
	leaq	.LC22(%rip), %rsi
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC22(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L153:
	.cfi_restore_state
	leaq	.LC18(%rip), %rsi
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC18(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L152:
	.cfi_restore_state
	leaq	.LC19(%rip), %rsi
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC19(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L151:
	.cfi_restore_state
	leaq	.LC20(%rip), %rsi
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC20(%rip), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_676number4impl19enum_to_stem_string9unitWidthE16UNumberUnitWidthRNS_13UnicodeStringE.cold, @function
_ZN6icu_676number4impl19enum_to_stem_string9unitWidthE16UNumberUnitWidthRNS_13UnicodeStringE.cold:
.LFSB3954:
.L147:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	call	abort@PLT
	.cfi_endproc
.LFE3954:
	.text
	.size	_ZN6icu_676number4impl19enum_to_stem_string9unitWidthE16UNumberUnitWidthRNS_13UnicodeStringE, .-_ZN6icu_676number4impl19enum_to_stem_string9unitWidthE16UNumberUnitWidthRNS_13UnicodeStringE
	.section	.text.unlikely
	.size	_ZN6icu_676number4impl19enum_to_stem_string9unitWidthE16UNumberUnitWidthRNS_13UnicodeStringE.cold, .-_ZN6icu_676number4impl19enum_to_stem_string9unitWidthE16UNumberUnitWidthRNS_13UnicodeStringE.cold
.LCOLDE23:
	.text
.LHOTE23:
	.section	.rodata.str2.2
	.align 2
.LC24:
	.string	"s"
	.string	"i"
	.string	"g"
	.string	"n"
	.string	"-"
	.string	"a"
	.string	"u"
	.string	"t"
	.string	"o"
	.string	""
	.string	""
	.align 2
.LC25:
	.string	"s"
	.string	"i"
	.string	"g"
	.string	"n"
	.string	"-"
	.string	"a"
	.string	"l"
	.string	"w"
	.string	"a"
	.string	"y"
	.string	"s"
	.string	""
	.string	""
	.align 2
.LC26:
	.string	"s"
	.string	"i"
	.string	"g"
	.string	"n"
	.string	"-"
	.string	"n"
	.string	"e"
	.string	"v"
	.string	"e"
	.string	"r"
	.string	""
	.string	""
	.section	.rodata.str2.8
	.align 8
.LC27:
	.string	"s"
	.string	"i"
	.string	"g"
	.string	"n"
	.string	"-"
	.string	"a"
	.string	"c"
	.string	"c"
	.string	"o"
	.string	"u"
	.string	"n"
	.string	"t"
	.string	"i"
	.string	"n"
	.string	"g"
	.string	""
	.string	""
	.align 8
.LC28:
	.string	"s"
	.string	"i"
	.string	"g"
	.string	"n"
	.string	"-"
	.string	"a"
	.string	"c"
	.string	"c"
	.string	"o"
	.string	"u"
	.string	"n"
	.string	"t"
	.string	"i"
	.string	"n"
	.string	"g"
	.string	"-"
	.string	"a"
	.string	"l"
	.string	"w"
	.string	"a"
	.string	"y"
	.string	"s"
	.string	""
	.string	""
	.align 8
.LC29:
	.string	"s"
	.string	"i"
	.string	"g"
	.string	"n"
	.string	"-"
	.string	"e"
	.string	"x"
	.string	"c"
	.string	"e"
	.string	"p"
	.string	"t"
	.string	"-"
	.string	"z"
	.string	"e"
	.string	"r"
	.string	"o"
	.string	""
	.string	""
	.align 8
.LC30:
	.string	"s"
	.string	"i"
	.string	"g"
	.string	"n"
	.string	"-"
	.string	"a"
	.string	"c"
	.string	"c"
	.string	"o"
	.string	"u"
	.string	"n"
	.string	"t"
	.string	"i"
	.string	"n"
	.string	"g"
	.string	"-"
	.string	"e"
	.string	"x"
	.string	"c"
	.string	"e"
	.string	"p"
	.string	"t"
	.string	"-"
	.string	"z"
	.string	"e"
	.string	"r"
	.string	"o"
	.string	""
	.string	""
	.section	.text.unlikely
.LCOLDB31:
	.text
.LHOTB31:
	.p2align 4
	.globl	_ZN6icu_676number4impl19enum_to_stem_string11signDisplayE18UNumberSignDisplayRNS_13UnicodeStringE
	.type	_ZN6icu_676number4impl19enum_to_stem_string11signDisplayE18UNumberSignDisplayRNS_13UnicodeStringE, @function
_ZN6icu_676number4impl19enum_to_stem_string11signDisplayE18UNumberSignDisplayRNS_13UnicodeStringE:
.LFB3955:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpl	$6, %edi
	ja	.L157
	leaq	.L159(%rip), %rdx
	movl	%edi, %edi
	movq	%rsi, %r8
	movl	$-1, %ecx
	movslq	(%rdx,%rdi,4), %rax
	addq	%rdx, %rax
	xorl	%edx, %edx
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L159:
	.long	.L165-.L159
	.long	.L164-.L159
	.long	.L163-.L159
	.long	.L162-.L159
	.long	.L161-.L159
	.long	.L160-.L159
	.long	.L158-.L159
	.text
	.p2align 4,,10
	.p2align 3
.L160:
	leaq	.LC29(%rip), %rsi
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC29(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L158:
	.cfi_restore_state
	leaq	.LC30(%rip), %rsi
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC30(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L165:
	.cfi_restore_state
	leaq	.LC24(%rip), %rsi
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC24(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L164:
	.cfi_restore_state
	leaq	.LC25(%rip), %rsi
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC25(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L163:
	.cfi_restore_state
	leaq	.LC26(%rip), %rsi
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC26(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L162:
	.cfi_restore_state
	leaq	.LC27(%rip), %rsi
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC27(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L161:
	.cfi_restore_state
	leaq	.LC28(%rip), %rsi
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC28(%rip), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_676number4impl19enum_to_stem_string11signDisplayE18UNumberSignDisplayRNS_13UnicodeStringE.cold, @function
_ZN6icu_676number4impl19enum_to_stem_string11signDisplayE18UNumberSignDisplayRNS_13UnicodeStringE.cold:
.LFSB3955:
.L157:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	call	abort@PLT
	.cfi_endproc
.LFE3955:
	.text
	.size	_ZN6icu_676number4impl19enum_to_stem_string11signDisplayE18UNumberSignDisplayRNS_13UnicodeStringE, .-_ZN6icu_676number4impl19enum_to_stem_string11signDisplayE18UNumberSignDisplayRNS_13UnicodeStringE
	.section	.text.unlikely
	.size	_ZN6icu_676number4impl19enum_to_stem_string11signDisplayE18UNumberSignDisplayRNS_13UnicodeStringE.cold, .-_ZN6icu_676number4impl19enum_to_stem_string11signDisplayE18UNumberSignDisplayRNS_13UnicodeStringE.cold
.LCOLDE31:
	.text
.LHOTE31:
	.section	.rodata.str2.2
	.align 2
.LC32:
	.string	"d"
	.string	"e"
	.string	"c"
	.string	"i"
	.string	"m"
	.string	"a"
	.string	"l"
	.string	"-"
	.string	"a"
	.string	"u"
	.string	"t"
	.string	"o"
	.string	""
	.string	""
	.align 2
.LC33:
	.string	"d"
	.string	"e"
	.string	"c"
	.string	"i"
	.string	"m"
	.string	"a"
	.string	"l"
	.string	"-"
	.string	"a"
	.string	"l"
	.string	"w"
	.string	"a"
	.string	"y"
	.string	"s"
	.string	""
	.string	""
	.section	.text.unlikely
.LCOLDB34:
	.text
.LHOTB34:
	.p2align 4
	.globl	_ZN6icu_676number4impl19enum_to_stem_string23decimalSeparatorDisplayE30UNumberDecimalSeparatorDisplayRNS_13UnicodeStringE
	.type	_ZN6icu_676number4impl19enum_to_stem_string23decimalSeparatorDisplayE30UNumberDecimalSeparatorDisplayRNS_13UnicodeStringE, @function
_ZN6icu_676number4impl19enum_to_stem_string23decimalSeparatorDisplayE30UNumberDecimalSeparatorDisplayRNS_13UnicodeStringE:
.LFB3956:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testl	%edi, %edi
	jne	.L175
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC32(%rip), %rsi
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC32(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L175:
	.cfi_restore_state
	cmpl	$1, %edi
	jne	.L176
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC33(%rip), %rsi
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC33(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L176:
	.cfi_restore_state
	jmp	.L173
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_676number4impl19enum_to_stem_string23decimalSeparatorDisplayE30UNumberDecimalSeparatorDisplayRNS_13UnicodeStringE.cold, @function
_ZN6icu_676number4impl19enum_to_stem_string23decimalSeparatorDisplayE30UNumberDecimalSeparatorDisplayRNS_13UnicodeStringE.cold:
.LFSB3956:
.L173:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	call	abort@PLT
	.cfi_endproc
.LFE3956:
	.text
	.size	_ZN6icu_676number4impl19enum_to_stem_string23decimalSeparatorDisplayE30UNumberDecimalSeparatorDisplayRNS_13UnicodeStringE, .-_ZN6icu_676number4impl19enum_to_stem_string23decimalSeparatorDisplayE30UNumberDecimalSeparatorDisplayRNS_13UnicodeStringE
	.section	.text.unlikely
	.size	_ZN6icu_676number4impl19enum_to_stem_string23decimalSeparatorDisplayE30UNumberDecimalSeparatorDisplayRNS_13UnicodeStringE.cold, .-_ZN6icu_676number4impl19enum_to_stem_string23decimalSeparatorDisplayE30UNumberDecimalSeparatorDisplayRNS_13UnicodeStringE.cold
.LCOLDE34:
	.text
.LHOTE34:
	.p2align 4
	.globl	_ZN6icu_676number4impl17blueprint_helpers24parseExponentWidthOptionERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode
	.type	_ZN6icu_676number4impl17blueprint_helpers24parseExponentWidthOptionERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode, @function
_ZN6icu_676number4impl17blueprint_helpers24parseExponentWidthOptionERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode:
.LFB3987:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	xorl	%esi, %esi
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	$1, %ebx
	subq	$16, %rsp
	call	_ZNK6icu_6713StringSegment6charAtEi@PLT
	subl	$42, %eax
	cmpw	$1, %ax
	jbe	.L178
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L186:
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_6713StringSegment6charAtEi@PLT
	cmpw	$101, %ax
	jne	.L180
	addl	$1, %ebx
.L178:
	movq	%r12, %rdi
	leal	-1(%rbx), %r13d
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	cmpl	%ebx, %eax
	jg	.L186
.L180:
	movq	%r12, %rdi
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	cmpl	%eax, %ebx
	jge	.L187
.L181:
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L187:
	.cfi_restore_state
	leaq	4(%r14), %rdi
	movl	%r13d, %esi
	call	_ZNK6icu_676number18ScientificNotation21withMinExponentDigitsEi@PLT
	movq	%rax, 4(%r14)
	movl	$1, %eax
	movl	%edx, 12(%r14)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3987:
	.size	_ZN6icu_676number4impl17blueprint_helpers24parseExponentWidthOptionERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode, .-_ZN6icu_676number4impl17blueprint_helpers24parseExponentWidthOptionERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode
	.p2align 4
	.globl	_ZN6icu_676number4impl17blueprint_helpers27generateExponentWidthOptionEiRNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_676number4impl17blueprint_helpers27generateExponentWidthOptionEiRNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_676number4impl17blueprint_helpers27generateExponentWidthOptionEiRNS_13UnicodeStringER10UErrorCode:
.LFB3988:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movl	$1, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	leaq	-42(%rbp), %rsi
	.cfi_offset 12, -32
	movl	%edi, %r12d
	movq	%r13, %rdi
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$42, %eax
	movw	%ax, -42(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	testl	%r12d, %r12d
	jle	.L188
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L190:
	movl	$101, %esi
	movq	%r13, %rdi
	addl	$1, %ebx
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	cmpl	%ebx, %r12d
	jne	.L190
.L188:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L194
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L194:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3988:
	.size	_ZN6icu_676number4impl17blueprint_helpers27generateExponentWidthOptionEiRNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_676number4impl17blueprint_helpers27generateExponentWidthOptionEiRNS_13UnicodeStringER10UErrorCode
	.p2align 4
	.globl	_ZN6icu_676number4impl17blueprint_helpers23parseExponentSignOptionERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode
	.type	_ZN6icu_676number4impl17blueprint_helpers23parseExponentSignOptionERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode, @function
_ZN6icu_676number4impl17blueprint_helpers23parseExponentSignOptionERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode:
.LFB3989:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	_ZN12_GLOBAL__N_119kSerializedStemTrieE(%rip), %rax
	movq	$0, -160(%rbp)
	movl	$-1, -136(%rbp)
	movq	%rax, -152(%rbp)
	movq	%rax, -144(%rbp)
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	leaq	-128(%rbp), %r15
	movq	%r12, %rsi
	movq	%r15, %rdi
	movl	%eax, %r14d
	call	_ZNK6icu_6713StringSegment19toTempUnicodeStringEv@PLT
	movzwl	-120(%rbp), %edx
	testb	$17, %dl
	jne	.L204
	leaq	-118(%rbp), %rax
	andl	$2, %edx
	cmove	-104(%rbp), %rax
.L196:
	leaq	-160(%rbp), %r12
	leaq	-176(%rbp), %rsi
	movl	%r14d, %edx
	movq	%rax, -176(%rbp)
	movq	%r12, %rdi
	call	_ZN6icu_6710UCharsTrie4nextENS_14ConstChar16PtrEi@PLT
	movl	%eax, %ebx
	movq	%r15, %rdi
	subl	$2, %ebx
	xorl	%r14d, %r14d
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpl	$1, %ebx
	ja	.L197
	movq	-144(%rbp), %rdx
	movzwl	(%rdx), %eax
	testw	%ax, %ax
	js	.L212
	movzwl	%ax, %ecx
	cmpw	$16447, %ax
	jbe	.L213
	movzwl	2(%rdx), %esi
	cmpl	$32703, %ecx
	jg	.L202
	andl	$32704, %eax
	subl	$16448, %eax
	sall	$10, %eax
	orl	%eax, %esi
	.p2align 4,,10
	.p2align 3
.L199:
	subl	$32, %esi
	xorl	%r14d, %r14d
	cmpl	$6, %esi
	ja	.L197
	leaq	4(%r13), %rdi
	movl	$1, %r14d
	call	_ZNK6icu_676number18ScientificNotation23withExponentSignDisplayE18UNumberSignDisplay@PLT
	movq	%rax, -176(%rbp)
	movl	%edx, -168(%rbp)
	movq	%rax, 4(%r13)
	movl	%edx, 12(%r13)
.L197:
	movq	%r12, %rdi
	call	_ZN6icu_6710UCharsTrieD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L214
	addq	$136, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L212:
	.cfi_restore_state
	movl	%eax, %ecx
	movl	%eax, %esi
	andw	$32767, %cx
	andl	$32767, %esi
	testb	$64, %ah
	je	.L199
	movzwl	2(%rdx), %eax
	cmpw	$32767, %cx
	je	.L200
	subl	$16384, %esi
	sall	$16, %esi
	orl	%eax, %esi
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L213:
	sarl	$6, %ecx
	leal	-1(%rcx), %esi
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L204:
	xorl	%eax, %eax
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L202:
	movzwl	4(%rdx), %eax
	sall	$16, %esi
	orl	%eax, %esi
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L200:
	movzwl	4(%rdx), %esi
	sall	$16, %eax
	orl	%eax, %esi
	jmp	.L199
.L214:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3989:
	.size	_ZN6icu_676number4impl17blueprint_helpers23parseExponentSignOptionERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode, .-_ZN6icu_676number4impl17blueprint_helpers23parseExponentSignOptionERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode
	.p2align 4
	.globl	_ZN6icu_676number4impl17blueprint_helpers22generateCurrencyOptionERKNS_12CurrencyUnitERNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_676number4impl17blueprint_helpers22generateCurrencyOptionERKNS_12CurrencyUnitERNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_676number4impl17blueprint_helpers22generateCurrencyOptionERKNS_12CurrencyUnitERNS_13UnicodeStringER10UErrorCode:
.LFB3991:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	leaq	20(%rdi), %rbx
	movq	%r8, %rdi
	movq	%rbx, %rsi
	subq	$8, %rsp
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3991:
	.size	_ZN6icu_676number4impl17blueprint_helpers22generateCurrencyOptionERKNS_12CurrencyUnitERNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_676number4impl17blueprint_helpers22generateCurrencyOptionERKNS_12CurrencyUnitERNS_13UnicodeStringER10UErrorCode
	.p2align 4
	.globl	_ZN6icu_676number4impl17blueprint_helpers22parseMeasureUnitOptionERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode
	.type	_ZN6icu_676number4impl17blueprint_helpers22parseMeasureUnitOptionERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode, @function
_ZN6icu_676number4impl17blueprint_helpers22parseMeasureUnitOptionERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode:
.LFB3992:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-976(%rbp), %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$984, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -1016(%rbp)
	movq	%rdi, %rsi
	movq	%r14, %rdi
	movq	%rdx, -1000(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK6icu_6713StringSegment19toTempUnicodeStringEv@PLT
	movswl	-968(%rbp), %edx
	movq	-952(%rbp), %r9
	leaq	-966(%rbp), %r8
	movq	%r8, %rcx
	movl	-964(%rbp), %edi
	movl	%edx, %esi
	sarl	$5, %edx
	movl	%esi, %r10d
	andw	$2, %r10w
	cmove	%r9, %rcx
	xorl	%eax, %eax
	testw	%si, %si
	jns	.L218
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L271:
	jbe	.L223
	cmpw	$45, (%rcx,%rax,2)
	je	.L225
.L223:
	addq	$1, %rax
.L218:
	movl	%eax, %r12d
	cmpl	%eax, %edx
	jg	.L271
	cmpl	%r12d, %edx
	je	.L272
.L225:
	xorl	%edi, %edi
	andl	$17, %esi
	leaq	-899(%rbp), %rax
	movl	$0, -856(%rbp)
	movq	%rax, -912(%rbp)
	movl	$40, -904(%rbp)
	movw	%di, -900(%rbp)
	movl	$0, -988(%rbp)
	jne	.L258
	testw	%r10w, %r10w
	cmovne	%r8, %r9
.L227:
	leaq	-784(%rbp), %rbx
	movl	%r12d, %ecx
	xorl	%esi, %esi
	movq	%r8, -1008(%rbp)
	leaq	-984(%rbp), %r15
	movq	%rbx, %rdi
	leaq	-988(%rbp), %r13
	movq	%r9, -984(%rbp)
	movq	%r15, %rdx
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	leaq	-912(%rbp), %rdi
	movq	%r13, %rdx
	movq	%rbx, %rsi
	call	_ZN6icu_6710CharString20appendInvariantCharsERKNS_13UnicodeStringER10UErrorCode@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-988(%rbp), %eax
	movq	-1008(%rbp), %r8
	cmpl	$26, %eax
	je	.L273
	testl	%eax, %eax
	jg	.L274
	leaq	-835(%rbp), %rax
	xorl	%ecx, %ecx
	movl	$0, -792(%rbp)
	movq	%rax, -848(%rbp)
	movzwl	-968(%rbp), %eax
	movl	$40, -840(%rbp)
	movw	%cx, -836(%rbp)
	movl	$0, -988(%rbp)
	testb	$17, %al
	jne	.L260
	testb	$2, %al
	cmove	-952(%rbp), %r8
.L231:
	movslq	%r12d, %rdx
	leaq	2(%r8,%rdx,2), %rdx
	movq	%rdx, -984(%rbp)
	testw	%ax, %ax
	js	.L232
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L233:
	addl	$1, %r12d
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	subl	%r12d, %ecx
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	leaq	-848(%rbp), %rdi
	movq	%r13, %rdx
	movq	%rbx, %rsi
	call	_ZN6icu_6710CharString20appendInvariantCharsERKNS_13UnicodeStringER10UErrorCode@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-988(%rbp), %edx
	cmpl	$26, %edx
	je	.L275
	leaq	-64(%rbp), %r12
	movq	%rbx, %r13
	testl	%edx, %edx
	jg	.L276
	.p2align 4,,10
	.p2align 3
.L239:
	movq	%r13, %rdi
	addq	$24, %r13
	call	_ZN6icu_6711MeasureUnitC1Ev@PLT
	cmpq	%r12, %r13
	jne	.L239
	movq	-912(%rbp), %rdi
	movl	$30, %edx
	movq	%r15, %rcx
	movq	%rbx, %rsi
	movl	$0, -984(%rbp)
	call	_ZN6icu_6711MeasureUnit12getAvailableEPKcPS0_iR10UErrorCode@PLT
	movl	-984(%rbp), %edx
	testl	%edx, %edx
	jg	.L277
	testl	%eax, %eax
	jle	.L246
	subl	$1, %eax
	movq	%rbx, %r15
	leaq	(%rax,%rax,2), %rax
	leaq	24(%rbx,%rax,8), %r12
	jmp	.L247
	.p2align 4,,10
	.p2align 3
.L245:
	addq	$24, %r15
	cmpq	%r15, %r12
	je	.L246
.L247:
	movq	%r15, %rdi
	call	_ZNK6icu_6711MeasureUnit10getSubtypeEv@PLT
	movq	-848(%rbp), %rdi
	movq	%rax, %rsi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L245
	movq	-1016(%rbp), %rdi
	movq	%r15, %rsi
	addq	$16, %rdi
	call	_ZN6icu_6711MeasureUnitaSERKS0_@PLT
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L278:
	jbe	.L220
	cmpw	$45, (%rcx,%rax,2)
	je	.L225
.L220:
	addq	$1, %rax
.L222:
	movl	%eax, %r12d
	cmpl	%eax, %edi
	jg	.L278
	movl	%edi, %edx
	cmpl	%r12d, %edx
	jne	.L225
	jmp	.L272
	.p2align 4,,10
	.p2align 3
.L274:
	movq	-1000(%rbp), %rbx
	movl	%eax, (%rbx)
.L236:
	cmpb	$0, -900(%rbp)
	jne	.L279
.L226:
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L280
	addq	$984, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L272:
	.cfi_restore_state
	movq	-1000(%rbp), %rax
	movl	$65811, (%rax)
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L279:
	movq	-912(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L276:
	movq	-1000(%rbp), %rax
	movl	%edx, (%rax)
.L240:
	cmpb	$0, -836(%rbp)
	je	.L236
	movq	-848(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L236
	.p2align 4,,10
	.p2align 3
.L273:
	movq	-1000(%rbp), %rax
	movl	$65811, (%rax)
	jmp	.L236
	.p2align 4,,10
	.p2align 3
.L232:
	movl	-964(%rbp), %ecx
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L258:
	xorl	%r9d, %r9d
	jmp	.L227
.L275:
	movq	-1000(%rbp), %rax
	movl	$65811, (%rax)
	jmp	.L240
.L277:
	movq	-1000(%rbp), %rax
	movl	$5, (%rax)
.L242:
	leaq	-88(%rbp), %r12
	.p2align 4,,10
	.p2align 3
.L249:
	movq	(%r12), %rax
	movq	%r12, %r13
	movq	%r12, %rdi
	subq	$24, %r12
	call	*(%rax)
	cmpq	%r13, %rbx
	jne	.L249
	jmp	.L240
.L260:
	xorl	%r8d, %r8d
	jmp	.L231
.L246:
	movq	-1000(%rbp), %rax
	leaq	-88(%rbp), %r12
	movl	$65811, (%rax)
	.p2align 4,,10
	.p2align 3
.L244:
	movq	(%r12), %rax
	movq	%r12, %r13
	movq	%r12, %rdi
	subq	$24, %r12
	call	*(%rax)
	cmpq	%r13, %rbx
	jne	.L244
	jmp	.L240
.L280:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3992:
	.size	_ZN6icu_676number4impl17blueprint_helpers22parseMeasureUnitOptionERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode, .-_ZN6icu_676number4impl17blueprint_helpers22parseMeasureUnitOptionERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode
	.p2align 4
	.globl	_ZN6icu_676number4impl17blueprint_helpers25generateMeasureUnitOptionERKNS_11MeasureUnitERNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_676number4impl17blueprint_helpers25generateMeasureUnitOptionERKNS_11MeasureUnitERNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_676number4impl17blueprint_helpers25generateMeasureUnitOptionERKNS_11MeasureUnitERNS_13UnicodeStringER10UErrorCode:
.LFB3993:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-112(%rbp), %r12
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK6icu_6711MeasureUnit7getTypeEv@PLT
	xorl	%ecx, %ecx
	movl	$-1, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_6713UnicodeStringC1EPKciNS0_10EInvariantE@PLT
	movswl	-104(%rbp), %ecx
	testw	%cx, %cx
	js	.L282
	sarl	$5, %ecx
.L283:
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	-114(%rbp), %rsi
	movl	$1, %ecx
	xorl	%edx, %edx
	movl	$45, %eax
	movq	%r13, %rdi
	movw	%ax, -114(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movq	%r14, %rdi
	call	_ZNK6icu_6711MeasureUnit10getSubtypeEv@PLT
	xorl	%ecx, %ecx
	movl	$-1, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_6713UnicodeStringC1EPKciNS0_10EInvariantE@PLT
	movswl	-104(%rbp), %ecx
	testw	%cx, %cx
	js	.L284
	sarl	$5, %ecx
.L285:
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L288
	addq	$104, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L282:
	.cfi_restore_state
	movl	-100(%rbp), %ecx
	jmp	.L283
	.p2align 4,,10
	.p2align 3
.L284:
	movl	-100(%rbp), %ecx
	jmp	.L285
.L288:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3993:
	.size	_ZN6icu_676number4impl17blueprint_helpers25generateMeasureUnitOptionERKNS_11MeasureUnitERNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_676number4impl17blueprint_helpers25generateMeasureUnitOptionERKNS_11MeasureUnitERNS_13UnicodeStringER10UErrorCode
	.p2align 4
	.globl	_ZN6icu_676number4impl17blueprint_helpers17parseFractionStemERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode
	.type	_ZN6icu_676number4impl17blueprint_helpers17parseFractionStemERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode, @function
_ZN6icu_676number4impl17blueprint_helpers17parseFractionStemERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode:
.LFB3996:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	$1, %ebx
	subq	$72, %rsp
	movq	%rdx, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	jmp	.L291
	.p2align 4,,10
	.p2align 3
.L306:
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	_ZNK6icu_6713StringSegment6charAtEi@PLT
	cmpw	$48, %ax
	jne	.L290
	addl	$1, %ebx
.L291:
	leal	-1(%rbx), %r12d
	movq	%r15, %rdi
	movl	%r12d, %r13d
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	cmpl	%ebx, %eax
	jg	.L306
.L290:
	movq	%r15, %rdi
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	cmpl	%ebx, %eax
	jg	.L307
.L292:
	movq	%r15, %rdi
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	cmpl	%eax, %ebx
	jge	.L308
.L294:
	movq	-104(%rbp), %rax
	movl	$65811, (%rax)
.L289:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L309
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L307:
	.cfi_restore_state
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	_ZNK6icu_6713StringSegment6charAtEi@PLT
	subl	$42, %eax
	cmpw	$1, %ax
	ja	.L293
	movq	%r15, %rdi
	addl	$1, %ebx
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	cmpl	%eax, %ebx
	jl	.L294
	leaq	-96(%rbp), %rdi
	testl	%r12d, %r12d
	jne	.L297
	call	_ZN6icu_676number9Precision9unlimitedEv@PLT
	movq	-80(%rbp), %rax
	movdqa	-96(%rbp), %xmm1
	movq	%rax, 80(%r14)
	movl	-72(%rbp), %eax
	movups	%xmm1, 64(%r14)
	movl	%eax, 88(%r14)
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L310:
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	_ZNK6icu_6713StringSegment6charAtEi@PLT
	cmpw	$35, %ax
	jne	.L292
	addl	$1, %ebx
	leal	-1(%rbx), %r12d
.L293:
	movq	%r15, %rdi
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	cmpl	%ebx, %eax
	jg	.L310
	movq	%r15, %rdi
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	cmpl	%eax, %ebx
	jl	.L294
	.p2align 4,,10
	.p2align 3
.L308:
	leaq	-96(%rbp), %rdi
	movl	%r12d, %edx
	movl	%r13d, %esi
	call	_ZN6icu_676number9Precision14minMaxFractionEii@PLT
	movq	-80(%rbp), %rax
	movdqa	-96(%rbp), %xmm0
	movq	%rax, 80(%r14)
	movl	-72(%rbp), %eax
	movups	%xmm0, 64(%r14)
	movl	%eax, 88(%r14)
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L297:
	movl	%r12d, %esi
	call	_ZN6icu_676number9Precision11minFractionEi@PLT
	movq	-80(%rbp), %rax
	movdqa	-96(%rbp), %xmm2
	movq	%rax, 80(%r14)
	movl	-72(%rbp), %eax
	movups	%xmm2, 64(%r14)
	movl	%eax, 88(%r14)
	jmp	.L289
.L309:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3996:
	.size	_ZN6icu_676number4impl17blueprint_helpers17parseFractionStemERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode, .-_ZN6icu_676number4impl17blueprint_helpers17parseFractionStemERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode
	.section	.rodata.str2.8
	.align 8
.LC35:
	.string	"p"
	.string	"r"
	.string	"e"
	.string	"c"
	.string	"i"
	.string	"s"
	.string	"i"
	.string	"o"
	.string	"n"
	.string	"-"
	.string	"i"
	.string	"n"
	.string	"t"
	.string	"e"
	.string	"g"
	.string	"e"
	.string	"r"
	.string	""
	.string	""
	.text
	.p2align 4
	.globl	_ZN6icu_676number4impl17blueprint_helpers20generateFractionStemEiiRNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_676number4impl17blueprint_helpers20generateFractionStemEiiRNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_676number4impl17blueprint_helpers20generateFractionStemEiiRNS_13UnicodeStringER10UErrorCode:
.LFB3997:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	%edi, %eax
	orl	%esi, %eax
	je	.L323
	movl	$46, %edx
	leaq	-58(%rbp), %r15
	movl	%edi, %r13d
	movl	%esi, %r14d
	movw	%dx, -58(%rbp)
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	testl	%r13d, %r13d
	jle	.L314
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L315:
	movl	$48, %esi
	movq	%r12, %rdi
	addl	$1, %ebx
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	cmpl	%ebx, %r13d
	jne	.L315
.L314:
	cmpl	$-1, %r14d
	je	.L324
	subl	%r13d, %r14d
	testl	%r14d, %r14d
	jle	.L311
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L318:
	movl	$35, %esi
	movq	%r12, %rdi
	addl	$1, %ebx
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	cmpl	%ebx, %r14d
	jne	.L318
.L311:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L325
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L323:
	.cfi_restore_state
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC35(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC35(%rip), %rax
	jmp	.L311
	.p2align 4,,10
	.p2align 3
.L324:
	movl	$42, %eax
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movw	%ax, -58(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	jmp	.L311
.L325:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3997:
	.size	_ZN6icu_676number4impl17blueprint_helpers20generateFractionStemEiiRNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_676number4impl17blueprint_helpers20generateFractionStemEiiRNS_13UnicodeStringER10UErrorCode
	.p2align 4
	.globl	_ZN6icu_676number4impl17blueprint_helpers15parseDigitsStemERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode
	.type	_ZN6icu_676number4impl17blueprint_helpers15parseDigitsStemERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode, @function
_ZN6icu_676number4impl17blueprint_helpers15parseDigitsStemERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode:
.LFB3998:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	jmp	.L328
	.p2align 4,,10
	.p2align 3
.L343:
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	_ZNK6icu_6713StringSegment6charAtEi@PLT
	cmpw	$64, %ax
	jne	.L327
	addl	$1, %r12d
.L328:
	movq	%r13, %rdi
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	cmpl	%r12d, %eax
	jg	.L343
.L327:
	movq	%r13, %rdi
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	cmpl	%r12d, %eax
	jg	.L344
	movq	%r13, %rdi
	movl	%r12d, %r15d
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	cmpl	%eax, %r12d
	jl	.L331
.L335:
	leaq	-96(%rbp), %rdi
	movl	%r15d, %edx
	movl	%r12d, %esi
	call	_ZN6icu_676number9Precision23minMaxSignificantDigitsEii@PLT
	movq	-80(%rbp), %rax
	movdqa	-96(%rbp), %xmm0
	movq	%rax, 80(%rbx)
	movl	-72(%rbp), %eax
	movups	%xmm0, 64(%rbx)
	movl	%eax, 88(%rbx)
.L326:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L345
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L331:
	.cfi_restore_state
	movl	$65811, (%r14)
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L344:
	movl	%r12d, %esi
	movq	%r13, %rdi
	movl	%r12d, %r15d
	call	_ZNK6icu_6713StringSegment6charAtEi@PLT
	subl	$42, %eax
	cmpw	$1, %ax
	ja	.L330
	movq	%r13, %rdi
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	leal	1(%r12), %edx
	cmpl	%eax, %edx
	jl	.L331
.L334:
	leaq	-96(%rbp), %rdi
	movl	%r12d, %esi
	call	_ZN6icu_676number9Precision20minSignificantDigitsEi@PLT
	movq	-80(%rbp), %rax
	movdqa	-96(%rbp), %xmm1
	movq	%rax, 80(%rbx)
	movl	-72(%rbp), %eax
	movups	%xmm1, 64(%rbx)
	movl	%eax, 88(%rbx)
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L346:
	movl	%r15d, %esi
	movq	%r13, %rdi
	call	_ZNK6icu_6713StringSegment6charAtEi@PLT
	cmpw	$35, %ax
	jne	.L333
	addl	$1, %r15d
.L330:
	movq	%r13, %rdi
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	cmpl	%r15d, %eax
	jg	.L346
.L333:
	movq	%r13, %rdi
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	cmpl	%r15d, %eax
	jg	.L331
	cmpl	$-1, %r15d
	je	.L334
	jmp	.L335
.L345:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3998:
	.size	_ZN6icu_676number4impl17blueprint_helpers15parseDigitsStemERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode, .-_ZN6icu_676number4impl17blueprint_helpers15parseDigitsStemERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode
	.p2align 4
	.globl	_ZN6icu_676number4impl17blueprint_helpers18generateDigitsStemEiiRNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_676number4impl17blueprint_helpers18generateDigitsStemEiiRNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_676number4impl17blueprint_helpers18generateDigitsStemEiiRNS_13UnicodeStringER10UErrorCode:
.LFB3999:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%esi, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movl	%edi, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%edi, %edi
	jle	.L348
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L349:
	movl	$64, %esi
	movq	%r12, %rdi
	addl	$1, %ebx
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	cmpl	%ebx, %r13d
	jne	.L349
.L348:
	cmpl	$-1, %r14d
	je	.L358
	subl	%r13d, %r14d
	testl	%r14d, %r14d
	jle	.L347
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L353:
	movl	$35, %esi
	movq	%r12, %rdi
	addl	$1, %ebx
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	cmpl	%ebx, %r14d
	jne	.L353
.L347:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L359
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L358:
	.cfi_restore_state
	movl	$42, %eax
	leaq	-42(%rbp), %rsi
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	movw	%ax, -42(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	jmp	.L347
.L359:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3999:
	.size	_ZN6icu_676number4impl17blueprint_helpers18generateDigitsStemEiiRNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_676number4impl17blueprint_helpers18generateDigitsStemEiiRNS_13UnicodeStringER10UErrorCode
	.p2align 4
	.globl	_ZN6icu_676number4impl17blueprint_helpers19parseScientificStemERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode
	.type	_ZN6icu_676number4impl17blueprint_helpers19parseScientificStemERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode, @function
_ZN6icu_676number4impl17blueprint_helpers19parseScientificStemERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode:
.LFB4000:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	cmpl	$1, %eax
	je	.L361
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZNK6icu_6713StringSegment6charAtEi@PLT
	cmpw	$69, %ax
	je	.L381
	movb	$0, -101(%rbp)
	movl	$2, %r12d
	movl	$1, %r15d
	movl	$3, -108(%rbp)
.L362:
	movl	%r15d, %esi
	movq	%rbx, %rdi
	call	_ZNK6icu_6713StringSegment6charAtEi@PLT
	movl	$0, -100(%rbp)
	cmpw	$43, %ax
	je	.L382
.L363:
	xorl	%r12d, %r12d
	jmp	.L366
	.p2align 4,,10
	.p2align 3
.L383:
	movl	%r15d, %esi
	movq	%rbx, %rdi
	call	_ZNK6icu_6713StringSegment6charAtEi@PLT
	cmpw	$48, %ax
	jne	.L361
	addl	$1, %r12d
	addl	$1, %r15d
.L366:
	movq	%rbx, %rdi
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	cmpl	%r15d, %eax
	jg	.L383
	cmpb	$0, -101(%rbp)
	jne	.L384
	call	_ZN6icu_676number8Notation10scientificEv@PLT
	movq	%rax, -92(%rbp)
	movl	%edx, -84(%rbp)
.L368:
	movl	-100(%rbp), %esi
	leaq	-92(%rbp), %rdi
	call	_ZNK6icu_676number18ScientificNotation23withExponentSignDisplayE18UNumberSignDisplay@PLT
	leaq	-80(%rbp), %rdi
	movl	%r12d, %esi
	movq	%rax, -80(%rbp)
	movl	%edx, -72(%rbp)
	call	_ZNK6icu_676number18ScientificNotation21withMinExponentDigitsEi@PLT
	movq	%rax, 4(%r14)
	movl	%edx, 12(%r14)
	jmp	.L360
	.p2align 4,,10
	.p2align 3
.L382:
	movq	%rbx, %rdi
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	cmpl	%r12d, %eax
	je	.L361
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	_ZNK6icu_6713StringSegment6charAtEi@PLT
	cmpw	$33, %ax
	je	.L373
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	_ZNK6icu_6713StringSegment6charAtEi@PLT
	cmpw	$63, %ax
	je	.L385
	.p2align 4,,10
	.p2align 3
.L361:
	movl	$65811, 0(%r13)
.L360:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L386
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L381:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	cmpl	$2, %eax
	je	.L361
	movb	$1, -101(%rbp)
	movl	$3, %r12d
	movl	$2, %r15d
	movl	$4, -108(%rbp)
	jmp	.L362
	.p2align 4,,10
	.p2align 3
.L384:
	call	_ZN6icu_676number8Notation11engineeringEv@PLT
	movq	%rax, -92(%rbp)
	movl	%edx, -84(%rbp)
	jmp	.L368
.L385:
	movl	$5, -100(%rbp)
.L364:
	movq	%rbx, %rdi
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	movl	-108(%rbp), %ecx
	cmpl	%ecx, %eax
	je	.L361
	movl	%ecx, %r15d
	jmp	.L363
.L373:
	movl	$1, -100(%rbp)
	jmp	.L364
.L386:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4000:
	.size	_ZN6icu_676number4impl17blueprint_helpers19parseScientificStemERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode, .-_ZN6icu_676number4impl17blueprint_helpers19parseScientificStemERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode
	.p2align 4
	.globl	_ZN6icu_676number4impl17blueprint_helpers16parseIntegerStemERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode
	.type	_ZN6icu_676number4impl17blueprint_helpers16parseIntegerStemERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode, @function
_ZN6icu_676number4impl17blueprint_helpers16parseIntegerStemERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode:
.LFB4001:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	$1, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$16, %rsp
	jmp	.L390
	.p2align 4,,10
	.p2align 3
.L395:
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	_ZNK6icu_6713StringSegment6charAtEi@PLT
	cmpw	$48, %ax
	jne	.L394
	addl	$1, %r12d
.L390:
	movq	%r13, %rdi
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	cmpl	%r12d, %eax
	jg	.L395
.L388:
	movq	%r13, %rdi
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	cmpl	%r12d, %eax
	jle	.L391
	movl	$65811, (%r14)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L391:
	.cfi_restore_state
	movl	%r12d, %edi
	call	_ZN6icu_676number12IntegerWidth10zeroFillToEi@PLT
	movq	%rax, 124(%rbx)
	movb	%dl, 132(%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L394:
	.cfi_restore_state
	subl	$1, %r12d
	jmp	.L388
	.cfi_endproc
.LFE4001:
	.size	_ZN6icu_676number4impl17blueprint_helpers16parseIntegerStemERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode, .-_ZN6icu_676number4impl17blueprint_helpers16parseIntegerStemERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode
	.p2align 4
	.globl	_ZN6icu_676number4impl17blueprint_helpers18parseFracSigOptionERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode
	.type	_ZN6icu_676number4impl17blueprint_helpers18parseFracSigOptionERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode, @function
_ZN6icu_676number4impl17blueprint_helpers18parseFracSigOptionERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode:
.LFB4002:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	xorl	%esi, %esi
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK6icu_6713StringSegment6charAtEi@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	cmpw	$64, %r8w
	jne	.L396
	xorl	%r12d, %r12d
	jmp	.L399
	.p2align 4,,10
	.p2align 3
.L414:
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	_ZNK6icu_6713StringSegment6charAtEi@PLT
	cmpw	$64, %ax
	jne	.L398
	addl	$1, %r12d
.L399:
	movq	%r13, %rdi
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	cmpl	%r12d, %eax
	jg	.L414
.L398:
	movq	%r13, %rdi
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	cmpl	%eax, %r12d
	jl	.L415
.L400:
	movl	$65811, (%r14)
	xorl	%eax, %eax
.L396:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L416
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L415:
	.cfi_restore_state
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	_ZNK6icu_6713StringSegment6charAtEi@PLT
	subl	$42, %eax
	cmpw	$1, %ax
	jbe	.L417
	movl	%r12d, %r15d
	cmpl	$1, %r12d
	jle	.L403
	jmp	.L400
	.p2align 4,,10
	.p2align 3
.L418:
	movl	%r15d, %esi
	movq	%r13, %rdi
	call	_ZNK6icu_6713StringSegment6charAtEi@PLT
	cmpw	$35, %ax
	jne	.L404
	addl	$1, %r15d
.L403:
	movq	%r13, %rdi
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	cmpl	%r15d, %eax
	jg	.L418
.L404:
	movq	%r13, %rdi
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	cmpl	%eax, %r15d
	jl	.L400
	leaq	64(%rbx), %rsi
	cmpl	$-1, %r15d
	je	.L405
	leaq	-96(%rbp), %rdi
	movl	%r15d, %edx
	call	_ZNK6icu_676number17FractionPrecision13withMaxDigitsEi@PLT
	movdqa	-96(%rbp), %xmm0
	movups	%xmm0, 64(%rbx)
.L413:
	movq	-80(%rbp), %rax
	movq	%rax, 80(%rbx)
	movl	-72(%rbp), %eax
	movl	%eax, 88(%rbx)
	movl	$1, %eax
	jmp	.L396
	.p2align 4,,10
	.p2align 3
.L417:
	movq	%r13, %rdi
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	leaq	64(%rbx), %rsi
	movl	%eax, %r8d
	leal	1(%r12), %eax
	cmpl	%eax, %r8d
	jg	.L400
.L405:
	leaq	-96(%rbp), %rdi
	movl	%r12d, %edx
	call	_ZNK6icu_676number17FractionPrecision13withMinDigitsEi@PLT
	movdqa	-96(%rbp), %xmm1
	movups	%xmm1, 64(%rbx)
	jmp	.L413
.L416:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4002:
	.size	_ZN6icu_676number4impl17blueprint_helpers18parseFracSigOptionERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode, .-_ZN6icu_676number4impl17blueprint_helpers18parseFracSigOptionERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode
	.p2align 4
	.globl	_ZN6icu_676number4impl17blueprint_helpers23parseIntegerWidthOptionERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode
	.type	_ZN6icu_676number4impl17blueprint_helpers23parseIntegerWidthOptionERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode, @function
_ZN6icu_676number4impl17blueprint_helpers23parseIntegerWidthOptionERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode:
.LFB4005:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	xorl	%esi, %esi
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK6icu_6713StringSegment6charAtEi@PLT
	subl	$42, %eax
	cmpw	$2, %ax
	sbbl	%r12d, %r12d
	movl	%r12d, %ebx
	andl	$1, %ebx
	jmp	.L424
	.p2align 4,,10
	.p2align 3
.L443:
	cmpl	$-1, %r12d
	je	.L422
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	_ZNK6icu_6713StringSegment6charAtEi@PLT
	cmpw	$35, %ax
	jne	.L423
	addl	$1, %r12d
	addl	$1, %ebx
.L424:
	movq	%r15, %rdi
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	cmpl	%ebx, %eax
	jg	.L443
	movq	%r15, %rdi
	xorl	%r13d, %r13d
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	cmpl	%ebx, %eax
	jg	.L432
.L425:
	cmpl	$-1, %r12d
	je	.L427
.L446:
	addl	%r13d, %r12d
.L433:
	movq	%r15, %rdi
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	cmpl	%eax, %ebx
	jl	.L430
	movl	%r13d, %edi
	call	_ZN6icu_676number12IntegerWidth10zeroFillToEi@PLT
	leaq	-80(%rbp), %rdi
	movl	%r12d, %esi
	movq	%rax, -80(%rbp)
	movl	%edx, -72(%rbp)
	call	_ZN6icu_676number12IntegerWidth10truncateAtEi@PLT
	movq	%rax, 124(%r14)
	movb	%dl, 132(%r14)
.L419:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L444
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L423:
	.cfi_restore_state
	movq	%r15, %rdi
	xorl	%r13d, %r13d
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	cmpl	%eax, %ebx
	jge	.L433
.L432:
	xorl	%r13d, %r13d
	jmp	.L426
	.p2align 4,,10
	.p2align 3
.L445:
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	_ZNK6icu_6713StringSegment6charAtEi@PLT
	cmpw	$48, %ax
	jne	.L425
	addl	$1, %r13d
	addl	$1, %ebx
.L426:
	movq	%r15, %rdi
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	cmpl	%ebx, %eax
	jg	.L445
	cmpl	$-1, %r12d
	jne	.L446
	.p2align 4,,10
	.p2align 3
.L427:
	movq	%r15, %rdi
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	cmpl	%eax, %ebx
	jl	.L430
	movl	%r13d, %edi
	call	_ZN6icu_676number12IntegerWidth10zeroFillToEi@PLT
	movq	%rax, 124(%r14)
	movb	%dl, 132(%r14)
	jmp	.L419
	.p2align 4,,10
	.p2align 3
.L430:
	movq	-88(%rbp), %rax
	movl	$65811, (%rax)
	jmp	.L419
	.p2align 4,,10
	.p2align 3
.L422:
	movq	%r15, %rdi
	xorl	%r13d, %r13d
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	cmpl	%eax, %ebx
	jge	.L427
	jmp	.L432
.L444:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4005:
	.size	_ZN6icu_676number4impl17blueprint_helpers23parseIntegerWidthOptionERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode, .-_ZN6icu_676number4impl17blueprint_helpers23parseIntegerWidthOptionERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode
	.p2align 4
	.globl	_ZN6icu_676number4impl17blueprint_helpers26generateIntegerWidthOptionEiiRNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_676number4impl17blueprint_helpers26generateIntegerWidthOptionEiiRNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_676number4impl17blueprint_helpers26generateIntegerWidthOptionEiiRNS_13UnicodeStringER10UErrorCode:
.LFB4006:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%edi, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$-1, %esi
	je	.L459
	movl	%esi, %r14d
	xorl	%ebx, %ebx
	subl	%edi, %r14d
	testl	%r14d, %r14d
	jle	.L452
	.p2align 4,,10
	.p2align 3
.L451:
	movl	$35, %esi
	movq	%r12, %rdi
	addl	$1, %ebx
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	cmpl	%ebx, %r14d
	jne	.L451
.L452:
	testl	%r13d, %r13d
	jle	.L447
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L450:
	movl	$48, %esi
	movq	%r12, %rdi
	addl	$1, %ebx
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	cmpl	%ebx, %r13d
	jne	.L450
.L447:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L460
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L459:
	.cfi_restore_state
	movl	$42, %eax
	leaq	-42(%rbp), %rsi
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	movw	%ax, -42(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	jmp	.L452
.L460:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4006:
	.size	_ZN6icu_676number4impl17blueprint_helpers26generateIntegerWidthOptionEiiRNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_676number4impl17blueprint_helpers26generateIntegerWidthOptionEiiRNS_13UnicodeStringER10UErrorCode
	.p2align 4
	.globl	_ZN6icu_676number4impl17blueprint_helpers26parseNumberingSystemOptionERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode
	.type	_ZN6icu_676number4impl17blueprint_helpers26parseNumberingSystemOptionERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode, @function
_ZN6icu_676number4impl17blueprint_helpers26parseNumberingSystemOptionERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode:
.LFB4007:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-176(%rbp), %r14
	movq	%rsi, %r13
	movq	%rdi, %rsi
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r14, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	subq	$224, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	-227(%rbp), %rax
	movw	%cx, -228(%rbp)
	movq	%rax, -240(%rbp)
	movl	$0, -184(%rbp)
	movl	$40, -232(%rbp)
	movl	$0, -252(%rbp)
	call	_ZNK6icu_6713StringSegment19toTempUnicodeStringEv@PLT
	movzwl	-168(%rbp), %edx
	testb	$17, %dl
	jne	.L474
	leaq	-166(%rbp), %rax
	andl	$2, %edx
	cmove	-152(%rbp), %rax
.L462:
	movq	%r12, %rdi
	movq	%rax, -248(%rbp)
	leaq	-112(%rbp), %r12
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	leaq	-248(%rbp), %rdx
	xorl	%esi, %esi
	movq	%r12, %rdi
	movl	%eax, %ecx
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	leaq	-252(%rbp), %rdx
	leaq	-240(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6710CharString20appendInvariantCharsERKNS_13UnicodeStringER10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-252(%rbp), %eax
	cmpl	$26, %eax
	je	.L466
	testl	%eax, %eax
	jle	.L465
	cmpb	$0, -228(%rbp)
	movl	%eax, (%rbx)
	jne	.L480
.L461:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L481
	addq	$224, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L465:
	.cfi_restore_state
	movq	-240(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN6icu_6715NumberingSystem20createInstanceByNameEPKcR10UErrorCode@PLT
	testq	%rax, %rax
	je	.L466
	movl	(%rbx), %edx
	testl	%edx, %edx
	jle	.L469
.L466:
	cmpb	$0, -228(%rbp)
	movl	$65811, (%rbx)
	je	.L461
.L480:
	movq	-240(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L461
	.p2align 4,,10
	.p2align 3
.L469:
	leaq	136(%r13), %rdi
	movq	%rax, %rsi
	call	_ZN6icu_676number4impl14SymbolsWrapper5setToEPKNS_15NumberingSystemE@PLT
	cmpb	$0, -228(%rbp)
	je	.L461
	jmp	.L480
	.p2align 4,,10
	.p2align 3
.L474:
	xorl	%eax, %eax
	jmp	.L462
.L481:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4007:
	.size	_ZN6icu_676number4impl17blueprint_helpers26parseNumberingSystemOptionERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode, .-_ZN6icu_676number4impl17blueprint_helpers26parseNumberingSystemOptionERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode
	.p2align 4
	.globl	_ZN6icu_676number4impl17blueprint_helpers29generateNumberingSystemOptionERKNS_15NumberingSystemERNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_676number4impl17blueprint_helpers29generateNumberingSystemOptionERKNS_15NumberingSystemERNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_676number4impl17blueprint_helpers29generateNumberingSystemOptionERKNS_15NumberingSystemERNS_13UnicodeStringER10UErrorCode:
.LFB4008:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-96(%rbp), %r12
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK6icu_6715NumberingSystem7getNameEv@PLT
	xorl	%ecx, %ecx
	movl	$-1, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_6713UnicodeStringC1EPKciNS0_10EInvariantE@PLT
	movswl	-88(%rbp), %ecx
	testw	%cx, %cx
	js	.L483
	sarl	$5, %ecx
.L484:
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L487
	addq	$80, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L483:
	.cfi_restore_state
	movl	-84(%rbp), %ecx
	jmp	.L484
.L487:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4008:
	.size	_ZN6icu_676number4impl17blueprint_helpers29generateNumberingSystemOptionERKNS_15NumberingSystemERNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_676number4impl17blueprint_helpers29generateNumberingSystemOptionERKNS_15NumberingSystemERNS_13UnicodeStringER10UErrorCode
	.p2align 4
	.globl	_ZN6icu_676number4impl17blueprint_helpers16parseScaleOptionERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode
	.type	_ZN6icu_676number4impl17blueprint_helpers16parseScaleOptionERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode, @function
_ZN6icu_676number4impl17blueprint_helpers16parseScaleOptionERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode:
.LFB4009:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-192(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	xorl	%esi, %esi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$264, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movw	%si, -244(%rbp)
	leaq	-243(%rbp), %rax
	movq	%rdi, %rsi
	movq	%r15, %rdi
	movq	%rax, -256(%rbp)
	movl	$0, -200(%rbp)
	movl	$40, -248(%rbp)
	movl	$0, -292(%rbp)
	call	_ZNK6icu_6713StringSegment19toTempUnicodeStringEv@PLT
	movzwl	-184(%rbp), %edx
	testb	$17, %dl
	jne	.L507
	leaq	-182(%rbp), %rax
	andl	$2, %edx
	cmove	-168(%rbp), %rax
.L489:
	movq	%r12, %rdi
	movq	%rax, -288(%rbp)
	leaq	-128(%rbp), %r12
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	leaq	-288(%rbp), %r14
	xorl	%esi, %esi
	movq	%r12, %rdi
	movl	%eax, %ecx
	movq	%r14, %rdx
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	leaq	-292(%rbp), %rdx
	leaq	-256(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6710CharString20appendInvariantCharsERKNS_13UnicodeStringER10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-292(%rbp), %eax
	cmpl	$26, %eax
	je	.L514
	testl	%eax, %eax
	jle	.L492
	movl	%eax, (%rbx)
.L495:
	cmpb	$0, -244(%rbp)
	jne	.L513
.L488:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L515
	addq	$264, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L492:
	.cfi_restore_state
	movl	$96, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L509
	movq	%rax, %rdi
	call	_ZN6icu_676number4impl6DecNumC1Ev@PLT
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jle	.L516
.L496:
	cmpb	$0, 12(%r12)
	jne	.L517
.L501:
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	cmpb	$0, -244(%rbp)
	je	.L488
	.p2align 4,,10
	.p2align 3
.L513:
	movq	-256(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L488
	.p2align 4,,10
	.p2align 3
.L514:
	movl	$65811, (%rbx)
	jmp	.L495
	.p2align 4,,10
	.p2align 3
.L516:
	movl	-200(%rbp), %edx
	movq	-256(%rbp), %rsi
	movq	%rbx, %rcx
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl6DecNum5setToENS_11StringPieceER10UErrorCode@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L518
	movq	%r12, %rdx
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN6icu_676number5ScaleC1EiPNS0_4impl6DecNumE@PLT
	leaq	168(%r13), %rdi
	movq	%r14, %rsi
	call	_ZN6icu_676number5ScaleaSEOS1_@PLT
	movq	%r14, %rdi
	call	_ZN6icu_676number5ScaleD1Ev@PLT
	cmpb	$0, -244(%rbp)
	je	.L488
	jmp	.L513
	.p2align 4,,10
	.p2align 3
.L507:
	xorl	%eax, %eax
	jmp	.L489
.L509:
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L495
	movl	$7, (%rbx)
	jmp	.L495
	.p2align 4,,10
	.p2align 3
.L517:
	movq	(%r12), %rdi
	call	uprv_free_67@PLT
	jmp	.L501
	.p2align 4,,10
	.p2align 3
.L518:
	movl	$65811, (%rbx)
	jmp	.L496
.L515:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4009:
	.size	_ZN6icu_676number4impl17blueprint_helpers16parseScaleOptionERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode, .-_ZN6icu_676number4impl17blueprint_helpers16parseScaleOptionERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode
	.section	.rodata.str2.2
	.align 2
.LC36:
	.string	"c"
	.string	"o"
	.string	"m"
	.string	"p"
	.string	"a"
	.string	"c"
	.string	"t"
	.string	"-"
	.string	"l"
	.string	"o"
	.string	"n"
	.string	"g"
	.string	""
	.string	""
	.align 2
.LC37:
	.string	"c"
	.string	"o"
	.string	"m"
	.string	"p"
	.string	"a"
	.string	"c"
	.string	"t"
	.string	"-"
	.string	"s"
	.string	"h"
	.string	"o"
	.string	"r"
	.string	"t"
	.string	""
	.string	""
	.align 2
.LC38:
	.string	"e"
	.string	"n"
	.string	"g"
	.string	"i"
	.string	"n"
	.string	"e"
	.string	"e"
	.string	"r"
	.string	"i"
	.string	"n"
	.string	"g"
	.string	""
	.string	""
	.align 2
.LC39:
	.string	"s"
	.string	"c"
	.string	"i"
	.string	"e"
	.string	"n"
	.string	"t"
	.string	"i"
	.string	"f"
	.string	"i"
	.string	"c"
	.string	""
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl16GeneratorHelpers8notationERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_676number4impl16GeneratorHelpers8notationERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_676number4impl16GeneratorHelpers8notationERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode:
.LFB4011:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	4(%rdi), %eax
	cmpl	$1, %eax
	je	.L537
	testl	%eax, %eax
	jne	.L531
	xorl	%edx, %edx
	cmpb	$3, 8(%rdi)
	movl	$-1, %ecx
	je	.L538
	leaq	.LC39(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC39(%rip), %rax
.L526:
	cmpw	$1, 10(%rbx)
	jle	.L532
	movl	$47, %ecx
	leaq	-58(%rbp), %r13
	xorl	%edx, %edx
	movq	%r12, %rdi
	movw	%cx, -58(%rbp)
	movq	%r13, %rsi
	movl	$1, %ecx
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movl	$42, %esi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movswl	10(%rbx), %r15d
	movw	%si, -58(%rbp)
	movl	$1, %ecx
	movq	%r13, %rsi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	testl	%r15d, %r15d
	jle	.L529
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L530:
	movl	$101, %esi
	movq	%r12, %rdi
	addl	$1, %r13d
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	cmpl	%r13d, %r15d
	jne	.L530
.L529:
	movl	(%r14), %edx
	testl	%edx, %edx
	jle	.L532
.L531:
	xorl	%eax, %eax
.L519:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L539
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L532:
	.cfi_restore_state
	movl	12(%rbx), %edi
	testl	%edi, %edi
	jne	.L528
.L536:
	movl	$1, %eax
	jmp	.L519
	.p2align 4,,10
	.p2align 3
.L537:
	movl	8(%rdi), %eax
	cmpl	$1, %eax
	je	.L540
	testl	%eax, %eax
	je	.L541
	movl	$16, (%rdx)
	xorl	%eax, %eax
	jmp	.L519
	.p2align 4,,10
	.p2align 3
.L528:
	leaq	-58(%rbp), %rsi
	movq	%r12, %rdi
	movl	$47, %eax
	xorl	%edx, %edx
	movl	$1, %ecx
	movw	%ax, -58(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movl	12(%rbx), %edi
	movq	%r12, %rsi
	call	_ZN6icu_676number4impl19enum_to_stem_string11signDisplayE18UNumberSignDisplayRNS_13UnicodeStringE
	jmp	.L536
	.p2align 4,,10
	.p2align 3
.L538:
	leaq	.LC38(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC38(%rip), %rax
	jmp	.L526
	.p2align 4,,10
	.p2align 3
.L541:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC37(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC37(%rip), %rax
	movl	$1, %eax
	jmp	.L519
	.p2align 4,,10
	.p2align 3
.L540:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC36(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC36(%rip), %rax
	movl	$1, %eax
	jmp	.L519
.L539:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4011:
	.size	_ZN6icu_676number4impl16GeneratorHelpers8notationERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_676number4impl16GeneratorHelpers8notationERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC40:
	.string	"none"
.LC41:
	.string	"percent"
.LC42:
	.string	"permille"
.LC43:
	.string	"currency"
	.section	.rodata.str2.8
	.align 8
.LC44:
	.string	"p"
	.string	"e"
	.string	"r"
	.string	"-"
	.string	"m"
	.string	"e"
	.string	"a"
	.string	"s"
	.string	"u"
	.string	"r"
	.string	"e"
	.string	"-"
	.string	"u"
	.string	"n"
	.string	"i"
	.string	"t"
	.string	"/"
	.string	""
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl16GeneratorHelpers7perUnitERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_676number4impl16GeneratorHelpers7perUnitERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_676number4impl16GeneratorHelpers7perUnitERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode:
.LFB4013:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	40(%rdi), %r12
	movq	%r12, %rdi
	subq	$8, %rsp
	call	_ZNK6icu_6711MeasureUnit7getTypeEv@PLT
	movl	$5, %ecx
	leaq	.LC40(%rip), %rsi
	movq	%rax, %rdi
	repz cmpsb
	movq	%r12, %rdi
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L543
	call	_ZNK6icu_6711MeasureUnit10getSubtypeEv@PLT
	movl	$8, %ecx
	leaq	.LC41(%rip), %rsi
	movq	%rax, %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L557
.L545:
	movl	$16, 0(%r13)
.L555:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L543:
	.cfi_restore_state
	call	_ZNK6icu_6711MeasureUnit7getTypeEv@PLT
	movl	$9, %ecx
	leaq	.LC43(%rip), %rsi
	movq	%rax, %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L545
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC44(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC44(%rip), %rax
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl17blueprint_helpers25generateMeasureUnitOptionERKNS_11MeasureUnitERNS_13UnicodeStringER10UErrorCode
	addq	$8, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L557:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK6icu_6711MeasureUnit10getSubtypeEv@PLT
	movl	$9, %ecx
	leaq	.LC42(%rip), %rsi
	movq	%rax, %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L555
	movl	$16, 0(%r13)
	jmp	.L555
	.cfi_endproc
.LFE4013:
	.size	_ZN6icu_676number4impl16GeneratorHelpers7perUnitERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_676number4impl16GeneratorHelpers7perUnitERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode
	.section	.text.unlikely
	.align 2
.LCOLDB45:
	.text
.LHOTB45:
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl16GeneratorHelpers12roundingModeERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_676number4impl16GeneratorHelpers12roundingModeERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_676number4impl16GeneratorHelpers12roundingModeERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode:
.LFB4015:
	.cfi_startproc
	endbr64
	movl	96(%rdi), %eax
	cmpl	$4, %eax
	je	.L569
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpl	$7, %eax
	ja	.L560
	leaq	.L562(%rip), %rdx
	movq	%rsi, %rdi
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L562:
	.long	.L568-.L562
	.long	.L567-.L562
	.long	.L566-.L562
	.long	.L565-.L562
	.long	.L560-.L562
	.long	.L564-.L562
	.long	.L563-.L562
	.long	.L561-.L562
	.text
	.p2align 4,,10
	.p2align 3
.L569:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	xorl	%eax, %eax
	ret
.L561:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC10(%rip), %rsi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC10(%rip), %rax
	movl	$1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L568:
	.cfi_restore_state
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC3(%rip), %rsi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC3(%rip), %rax
	movl	$1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L567:
	.cfi_restore_state
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC4(%rip), %rsi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC4(%rip), %rax
	movl	$1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L566:
	.cfi_restore_state
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC5(%rip), %rsi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC5(%rip), %rax
	movl	$1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L565:
	.cfi_restore_state
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC6(%rip), %rsi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC6(%rip), %rax
	movl	$1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L564:
	.cfi_restore_state
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC8(%rip), %rsi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC8(%rip), %rax
	movl	$1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L563:
	.cfi_restore_state
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC9(%rip), %rsi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC9(%rip), %rax
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_676number4impl16GeneratorHelpers12roundingModeERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode.cold, @function
_ZN6icu_676number4impl16GeneratorHelpers12roundingModeERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode.cold:
.LFSB4015:
.L560:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	call	abort@PLT
	.cfi_endproc
.LFE4015:
	.text
	.size	_ZN6icu_676number4impl16GeneratorHelpers12roundingModeERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_676number4impl16GeneratorHelpers12roundingModeERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode
	.section	.text.unlikely
	.size	_ZN6icu_676number4impl16GeneratorHelpers12roundingModeERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode.cold, .-_ZN6icu_676number4impl16GeneratorHelpers12roundingModeERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode.cold
.LCOLDE45:
	.text
.LHOTE45:
	.section	.text.unlikely
	.align 2
.LCOLDB46:
	.text
.LHOTB46:
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl16GeneratorHelpers8groupingERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_676number4impl16GeneratorHelpers8groupingERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_676number4impl16GeneratorHelpers8groupingERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode:
.LFB4016:
	.cfi_startproc
	endbr64
	cmpw	$-3, 100(%rdi)
	je	.L583
	movl	108(%rdi), %eax
	cmpl	$5, %eax
	je	.L592
	xorl	%r9d, %r9d
	cmpl	$2, %eax
	jne	.L593
	movl	%r9d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L593:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpl	$3, %eax
	je	.L577
	ja	.L578
	testl	%eax, %eax
	je	.L579
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC13(%rip), %rsi
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC13(%rip), %rax
	movl	$1, %r9d
.L574:
	movl	%r9d, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L578:
	.cfi_restore_state
	cmpl	$4, %eax
	jne	.L594
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC16(%rip), %rsi
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC16(%rip), %rax
	movl	$1, %r9d
	jmp	.L574
	.p2align 4,,10
	.p2align 3
.L583:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	xorl	%r9d, %r9d
	movl	%r9d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L579:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC12(%rip), %rsi
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC12(%rip), %rax
	movl	$1, %r9d
	jmp	.L574
	.p2align 4,,10
	.p2align 3
.L592:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	xorl	%r9d, %r9d
	movl	$16, (%rdx)
	movl	%r9d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L577:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC15(%rip), %rsi
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC15(%rip), %rax
	movl	$1, %r9d
	jmp	.L574
.L594:
	jmp	.L586
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_676number4impl16GeneratorHelpers8groupingERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode.cold, @function
_ZN6icu_676number4impl16GeneratorHelpers8groupingERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode.cold:
.LFSB4016:
.L586:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	call	abort@PLT
	.cfi_endproc
.LFE4016:
	.text
	.size	_ZN6icu_676number4impl16GeneratorHelpers8groupingERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_676number4impl16GeneratorHelpers8groupingERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode
	.section	.text.unlikely
	.size	_ZN6icu_676number4impl16GeneratorHelpers8groupingERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode.cold, .-_ZN6icu_676number4impl16GeneratorHelpers8groupingERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode.cold
.LCOLDE46:
	.text
.LHOTE46:
	.section	.rodata.str2.2
	.align 2
.LC47:
	.string	"i"
	.string	"n"
	.string	"t"
	.string	"e"
	.string	"g"
	.string	"e"
	.string	"r"
	.string	"-"
	.string	"w"
	.string	"i"
	.string	"d"
	.string	"t"
	.string	"h"
	.string	"/"
	.string	""
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl16GeneratorHelpers12integerWidthERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_676number4impl16GeneratorHelpers12integerWidthERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_676number4impl16GeneratorHelpers12integerWidthERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode:
.LFB4017:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 132(%rdi)
	jne	.L596
	cmpw	$-1, 124(%rdi)
	movq	%rdi, %rbx
	je	.L596
	movl	$1, %edi
	movq	%rsi, %r12
	call	_ZN6icu_676number12IntegerWidth10zeroFillToEi@PLT
	leaq	-76(%rbp), %rsi
	leaq	124(%rbx), %rdi
	movq	%rax, -52(%rbp)
	movl	%edx, -44(%rbp)
	movq	%rax, -64(%rbp)
	movl	%edx, -56(%rbp)
	movq	%rax, -76(%rbp)
	movl	%edx, -68(%rbp)
	call	_ZNK6icu_676number12IntegerWidtheqERKS1_@PLT
	testb	%al, %al
	jne	.L596
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC47(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC47(%rip), %rax
	movswl	126(%rbx), %r13d
	movswl	124(%rbx), %r14d
	cmpl	$-1, %r13d
	je	.L610
	subl	%r14d, %r13d
	xorl	%ebx, %ebx
	testl	%r13d, %r13d
	jle	.L601
	.p2align 4,,10
	.p2align 3
.L600:
	movl	$35, %esi
	movq	%r12, %rdi
	addl	$1, %ebx
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	cmpl	%ebx, %r13d
	jne	.L600
.L601:
	xorl	%ebx, %ebx
	testl	%r14d, %r14d
	jle	.L603
	.p2align 4,,10
	.p2align 3
.L598:
	movl	$48, %esi
	movq	%r12, %rdi
	addl	$1, %ebx
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	cmpl	%ebx, %r14d
	jne	.L598
.L603:
	movl	$1, %eax
	jmp	.L595
	.p2align 4,,10
	.p2align 3
.L596:
	xorl	%eax, %eax
.L595:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L611
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L610:
	.cfi_restore_state
	movl	$42, %eax
	leaq	-64(%rbp), %rsi
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	movw	%ax, -64(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	jmp	.L601
.L611:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4017:
	.size	_ZN6icu_676number4impl16GeneratorHelpers12integerWidthERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_676number4impl16GeneratorHelpers12integerWidthERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode
	.section	.rodata.str1.1
.LC48:
	.string	"latn"
	.section	.rodata.str2.2
	.align 2
.LC49:
	.string	"l"
	.string	"a"
	.string	"t"
	.string	"i"
	.string	"n"
	.string	""
	.string	""
	.section	.rodata.str2.8
	.align 8
.LC50:
	.string	"n"
	.string	"u"
	.string	"m"
	.string	"b"
	.string	"e"
	.string	"r"
	.string	"i"
	.string	"n"
	.string	"g"
	.string	"-"
	.string	"s"
	.string	"y"
	.string	"s"
	.string	"t"
	.string	"e"
	.string	"m"
	.string	"/"
	.string	""
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl16GeneratorHelpers7symbolsERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_676number4impl16GeneratorHelpers7symbolsERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_676number4impl16GeneratorHelpers7symbolsERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode:
.LFB4018:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	leaq	136(%rdi), %r12
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK6icu_676number4impl14SymbolsWrapper17isNumberingSystemEv@PLT
	movq	%r12, %rdi
	movl	%eax, %r13d
	testb	%al, %al
	jne	.L623
	call	_ZNK6icu_676number4impl14SymbolsWrapper22isDecimalFormatSymbolsEv@PLT
	testb	%al, %al
	jne	.L624
.L612:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L625
	addq	$80, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L624:
	.cfi_restore_state
	movl	$16, (%rbx)
	jmp	.L612
	.p2align 4,,10
	.p2align 3
.L623:
	call	_ZNK6icu_676number4impl14SymbolsWrapper18getNumberingSystemEv@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZNK6icu_6715NumberingSystem7getNameEv@PLT
	movl	$5, %ecx
	leaq	.LC48(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	movl	$-1, %ecx
	seta	%al
	sbbb	$0, %al
	xorl	%edx, %edx
	testb	%al, %al
	je	.L626
	leaq	.LC50(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC50(%rip), %rax
	movq	%r12, %rdi
	leaq	-112(%rbp), %r12
	call	_ZNK6icu_6715NumberingSystem7getNameEv@PLT
	xorl	%ecx, %ecx
	movl	$-1, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_6713UnicodeStringC1EPKciNS0_10EInvariantE@PLT
	movswl	-104(%rbp), %ecx
	testw	%cx, %cx
	js	.L616
	sarl	$5, %ecx
.L617:
	movq	%r14, %rdi
	xorl	%edx, %edx
	movq	%r12, %rsi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L612
	.p2align 4,,10
	.p2align 3
.L626:
	leaq	.LC49(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC49(%rip), %rax
	jmp	.L612
	.p2align 4,,10
	.p2align 3
.L616:
	movl	-100(%rbp), %ecx
	jmp	.L617
.L625:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4018:
	.size	_ZN6icu_676number4impl16GeneratorHelpers7symbolsERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_676number4impl16GeneratorHelpers7symbolsERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode
	.section	.text.unlikely
	.align 2
.LCOLDB51:
	.text
.LHOTB51:
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl16GeneratorHelpers9unitWidthERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_676number4impl16GeneratorHelpers9unitWidthERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_676number4impl16GeneratorHelpers9unitWidthERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode:
.LFB4019:
	.cfi_startproc
	endbr64
	movl	152(%rdi), %eax
	xorl	%r8d, %r8d
	movl	%eax, %edx
	andl	$-5, %edx
	cmpl	$1, %edx
	jne	.L642
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L642:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpl	$4, %eax
	ja	.L629
	leaq	.L631(%rip), %rdx
	movq	%rsi, %rdi
	movl	$-1, %ecx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	xorl	%edx, %edx
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L631:
	.long	.L635-.L631
	.long	.L634-.L631
	.long	.L633-.L631
	.long	.L632-.L631
	.long	.L630-.L631
	.text
.L632:
	leaq	.LC21(%rip), %rsi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC21(%rip), %rax
	movl	$1, %r8d
.L627:
	movl	%r8d, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L630:
	.cfi_restore_state
	leaq	.LC22(%rip), %rsi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC22(%rip), %rax
	movl	$1, %r8d
	jmp	.L627
.L635:
	leaq	.LC18(%rip), %rsi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC18(%rip), %rax
	movl	$1, %r8d
	jmp	.L627
.L634:
	leaq	.LC19(%rip), %rsi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC19(%rip), %rax
	movl	$1, %r8d
	jmp	.L627
.L633:
	leaq	.LC20(%rip), %rsi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC20(%rip), %rax
	movl	$1, %r8d
	jmp	.L627
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_676number4impl16GeneratorHelpers9unitWidthERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode.cold, @function
_ZN6icu_676number4impl16GeneratorHelpers9unitWidthERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode.cold:
.LFSB4019:
.L629:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	call	abort@PLT
	.cfi_endproc
.LFE4019:
	.text
	.size	_ZN6icu_676number4impl16GeneratorHelpers9unitWidthERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_676number4impl16GeneratorHelpers9unitWidthERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode
	.section	.text.unlikely
	.size	_ZN6icu_676number4impl16GeneratorHelpers9unitWidthERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode.cold, .-_ZN6icu_676number4impl16GeneratorHelpers9unitWidthERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode.cold
.LCOLDE51:
	.text
.LHOTE51:
	.section	.text.unlikely
	.align 2
.LCOLDB52:
	.text
.LHOTB52:
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl16GeneratorHelpers4signERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_676number4impl16GeneratorHelpers4signERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_676number4impl16GeneratorHelpers4signERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode:
.LFB4020:
	.cfi_startproc
	endbr64
	movl	156(%rdi), %eax
	testl	%eax, %eax
	je	.L654
	cmpl	$7, %eax
	je	.L654
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpl	$6, %eax
	ja	.L645
	leaq	.L647(%rip), %rdx
	movq	%rsi, %rdi
	movl	$-1, %ecx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	xorl	%edx, %edx
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L647:
	.long	.L653-.L647
	.long	.L652-.L647
	.long	.L651-.L647
	.long	.L650-.L647
	.long	.L649-.L647
	.long	.L648-.L647
	.long	.L646-.L647
	.text
	.p2align 4,,10
	.p2align 3
.L654:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	xorl	%eax, %eax
	ret
.L648:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	leaq	.LC29(%rip), %rsi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC29(%rip), %rax
	movl	$1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L646:
	.cfi_restore_state
	leaq	.LC30(%rip), %rsi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC30(%rip), %rax
	movl	$1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L653:
	.cfi_restore_state
	leaq	.LC24(%rip), %rsi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC24(%rip), %rax
	movl	$1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L652:
	.cfi_restore_state
	leaq	.LC25(%rip), %rsi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC25(%rip), %rax
	movl	$1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L651:
	.cfi_restore_state
	leaq	.LC26(%rip), %rsi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC26(%rip), %rax
	movl	$1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L650:
	.cfi_restore_state
	leaq	.LC27(%rip), %rsi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC27(%rip), %rax
	movl	$1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L649:
	.cfi_restore_state
	leaq	.LC28(%rip), %rsi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC28(%rip), %rax
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_676number4impl16GeneratorHelpers4signERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode.cold, @function
_ZN6icu_676number4impl16GeneratorHelpers4signERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode.cold:
.LFSB4020:
.L645:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	call	abort@PLT
	.cfi_endproc
.LFE4020:
	.text
	.size	_ZN6icu_676number4impl16GeneratorHelpers4signERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_676number4impl16GeneratorHelpers4signERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode
	.section	.text.unlikely
	.size	_ZN6icu_676number4impl16GeneratorHelpers4signERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode.cold, .-_ZN6icu_676number4impl16GeneratorHelpers4signERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode.cold
.LCOLDE52:
	.text
.LHOTE52:
	.section	.text.unlikely
	.align 2
.LCOLDB53:
	.text
.LHOTB53:
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl16GeneratorHelpers7decimalERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_676number4impl16GeneratorHelpers7decimalERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_676number4impl16GeneratorHelpers7decimalERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode:
.LFB4021:
	.cfi_startproc
	endbr64
	movl	160(%rdi), %eax
	testl	$-3, %eax
	jne	.L669
	xorl	%r8d, %r8d
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L669:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpl	$1, %eax
	jne	.L661
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC33(%rip), %rsi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC33(%rip), %rax
	movl	$1, %r8d
	popq	%rbp
	.cfi_def_cfa 7, 8
	movl	%r8d, %eax
	ret
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_676number4impl16GeneratorHelpers7decimalERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode.cold, @function
_ZN6icu_676number4impl16GeneratorHelpers7decimalERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode.cold:
.LFSB4021:
.L661:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	call	abort@PLT
	.cfi_endproc
.LFE4021:
	.text
	.size	_ZN6icu_676number4impl16GeneratorHelpers7decimalERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_676number4impl16GeneratorHelpers7decimalERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode
	.section	.text.unlikely
	.size	_ZN6icu_676number4impl16GeneratorHelpers7decimalERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode.cold, .-_ZN6icu_676number4impl16GeneratorHelpers7decimalERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode.cold
.LCOLDE53:
	.text
.LHOTE53:
	.section	.text._ZNK6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE11copyErrorToER10UErrorCode,"axG",@progbits,_ZNK6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE11copyErrorToER10UErrorCode,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE11copyErrorToER10UErrorCode
	.type	_ZNK6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE11copyErrorToER10UErrorCode, @function
_ZNK6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE11copyErrorToER10UErrorCode:
.LFB4626:
	.cfi_startproc
	endbr64
	movl	(%rsi), %edx
	movl	$1, %eax
	testl	%edx, %edx
	jle	.L683
.L670:
	ret
	.p2align 4,,10
	.p2align 3
.L683:
	cmpl	$3, 4(%rdi)
	je	.L672
	cmpl	$9, 64(%rdi)
	je	.L684
	cmpl	$-3, 112(%rdi)
	je	.L685
	cmpb	$0, 132(%rdi)
	jne	.L686
	movl	136(%rdi), %ecx
	leal	-1(%rcx), %edx
	cmpl	$1, %edx
	jbe	.L687
.L678:
	movl	184(%rdi), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	je	.L670
	movl	%edx, (%rsi)
	setg	%al
	ret
	.p2align 4,,10
	.p2align 3
.L685:
	movl	116(%rdi), %eax
	testl	%eax, %eax
	movl	%eax, (%rsi)
	setg	%al
	ret
	.p2align 4,,10
	.p2align 3
.L687:
	cmpq	$0, 144(%rdi)
	jne	.L678
	movl	$7, (%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L672:
	movl	8(%rdi), %eax
	testl	%eax, %eax
	movl	%eax, (%rsi)
	setg	%al
	ret
	.p2align 4,,10
	.p2align 3
.L686:
	movl	124(%rdi), %eax
	testl	%eax, %eax
	movl	%eax, (%rsi)
	setg	%al
	ret
	.p2align 4,,10
	.p2align 3
.L684:
	movl	72(%rdi), %eax
	testl	%eax, %eax
	movl	%eax, (%rsi)
	setg	%al
	ret
	.cfi_endproc
.LFE4626:
	.size	_ZNK6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE11copyErrorToER10UErrorCode, .-_ZNK6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE11copyErrorToER10UErrorCode
	.section	.text._ZNK6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE11copyErrorToER10UErrorCode,"axG",@progbits,_ZNK6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE11copyErrorToER10UErrorCode,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE11copyErrorToER10UErrorCode
	.type	_ZNK6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE11copyErrorToER10UErrorCode, @function
_ZNK6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE11copyErrorToER10UErrorCode:
.LFB4628:
	.cfi_startproc
	endbr64
	movl	(%rsi), %edx
	movl	$1, %eax
	testl	%edx, %edx
	jle	.L701
.L688:
	ret
	.p2align 4,,10
	.p2align 3
.L701:
	cmpl	$3, 4(%rdi)
	je	.L690
	cmpl	$9, 64(%rdi)
	je	.L702
	cmpl	$-3, 112(%rdi)
	je	.L703
	cmpb	$0, 132(%rdi)
	jne	.L704
	movl	136(%rdi), %ecx
	leal	-1(%rcx), %edx
	cmpl	$1, %edx
	jbe	.L705
.L696:
	movl	184(%rdi), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	je	.L688
	movl	%edx, (%rsi)
	setg	%al
	ret
	.p2align 4,,10
	.p2align 3
.L703:
	movl	116(%rdi), %eax
	testl	%eax, %eax
	movl	%eax, (%rsi)
	setg	%al
	ret
	.p2align 4,,10
	.p2align 3
.L705:
	cmpq	$0, 144(%rdi)
	jne	.L696
	movl	$7, (%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L690:
	movl	8(%rdi), %eax
	testl	%eax, %eax
	movl	%eax, (%rsi)
	setg	%al
	ret
	.p2align 4,,10
	.p2align 3
.L704:
	movl	124(%rdi), %eax
	testl	%eax, %eax
	movl	%eax, (%rsi)
	setg	%al
	ret
	.p2align 4,,10
	.p2align 3
.L702:
	movl	72(%rdi), %eax
	testl	%eax, %eax
	movl	%eax, (%rsi)
	setg	%al
	ret
	.cfi_endproc
.LFE4628:
	.size	_ZNK6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE11copyErrorToER10UErrorCode, .-_ZNK6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE11copyErrorToER10UErrorCode
	.section	.text.unlikely
.LCOLDB54:
	.text
.LHOTB54:
	.p2align 4
	.globl	_ZN6icu_676number4impl14stem_to_object4unitENS1_8skeleton8StemEnumE
	.type	_ZN6icu_676number4impl14stem_to_object4unitENS1_8skeleton8StemEnumE, @function
_ZN6icu_676number4impl14stem_to_object4unitENS1_8skeleton8StemEnumE:
.LFB3945:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpl	$6, %esi
	je	.L707
	cmpl	$7, %esi
	jne	.L714
	leaq	-48(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN6icu_676NoUnit8permilleEv@PLT
.L713:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6711MeasureUnitC1EOS0_@PLT
	movq	%r13, %rdi
	call	_ZN6icu_676NoUnitD1Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L715
	addq	$32, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L714:
	.cfi_restore_state
	cmpl	$5, %esi
	jne	.L709
	leaq	-48(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN6icu_676NoUnit4baseEv@PLT
	jmp	.L713
	.p2align 4,,10
	.p2align 3
.L707:
	leaq	-48(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN6icu_676NoUnit7percentEv@PLT
	jmp	.L713
.L715:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_676number4impl14stem_to_object4unitENS1_8skeleton8StemEnumE.cold, @function
_ZN6icu_676number4impl14stem_to_object4unitENS1_8skeleton8StemEnumE.cold:
.LFSB3945:
.L709:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	call	abort@PLT
	.cfi_endproc
.LFE3945:
	.text
	.size	_ZN6icu_676number4impl14stem_to_object4unitENS1_8skeleton8StemEnumE, .-_ZN6icu_676number4impl14stem_to_object4unitENS1_8skeleton8StemEnumE
	.section	.text.unlikely
	.size	_ZN6icu_676number4impl14stem_to_object4unitENS1_8skeleton8StemEnumE.cold, .-_ZN6icu_676number4impl14stem_to_object4unitENS1_8skeleton8StemEnumE.cold
.LCOLDE54:
	.text
.LHOTE54:
	.section	.rodata.str2.8
	.align 8
.LC55:
	.string	"n"
	.string	"o"
	.string	"t"
	.string	"a"
	.string	"t"
	.string	"i"
	.string	"o"
	.string	"n"
	.string	"-"
	.string	"s"
	.string	"i"
	.string	"m"
	.string	"p"
	.string	"l"
	.string	"e"
	.string	""
	.string	""
	.section	.rodata.str2.2
	.align 2
.LC56:
	.string	"b"
	.string	"a"
	.string	"s"
	.string	"e"
	.string	"-"
	.string	"u"
	.string	"n"
	.string	"i"
	.string	"t"
	.string	""
	.string	""
	.align 2
.LC57:
	.string	"p"
	.string	"e"
	.string	"r"
	.string	"c"
	.string	"e"
	.string	"n"
	.string	"t"
	.string	""
	.string	""
	.align 2
.LC58:
	.string	"p"
	.string	"e"
	.string	"r"
	.string	"m"
	.string	"i"
	.string	"l"
	.string	"l"
	.string	"e"
	.string	""
	.string	""
	.section	.rodata.str2.8
	.align 8
.LC59:
	.string	"p"
	.string	"r"
	.string	"e"
	.string	"c"
	.string	"i"
	.string	"s"
	.string	"i"
	.string	"o"
	.string	"n"
	.string	"-"
	.string	"u"
	.string	"n"
	.string	"l"
	.string	"i"
	.string	"m"
	.string	"i"
	.string	"t"
	.string	"e"
	.string	"d"
	.string	""
	.string	""
	.align 8
.LC60:
	.string	"p"
	.string	"r"
	.string	"e"
	.string	"c"
	.string	"i"
	.string	"s"
	.string	"i"
	.string	"o"
	.string	"n"
	.string	"-"
	.string	"c"
	.string	"u"
	.string	"r"
	.string	"r"
	.string	"e"
	.string	"n"
	.string	"c"
	.string	"y"
	.string	"-"
	.string	"s"
	.string	"t"
	.string	"a"
	.string	"n"
	.string	"d"
	.string	"a"
	.string	"r"
	.string	"d"
	.string	""
	.string	""
	.align 8
.LC61:
	.string	"p"
	.string	"r"
	.string	"e"
	.string	"c"
	.string	"i"
	.string	"s"
	.string	"i"
	.string	"o"
	.string	"n"
	.string	"-"
	.string	"c"
	.string	"u"
	.string	"r"
	.string	"r"
	.string	"e"
	.string	"n"
	.string	"c"
	.string	"y"
	.string	"-"
	.string	"c"
	.string	"a"
	.string	"s"
	.string	"h"
	.string	""
	.string	""
	.align 8
.LC62:
	.string	"p"
	.string	"r"
	.string	"e"
	.string	"c"
	.string	"i"
	.string	"s"
	.string	"i"
	.string	"o"
	.string	"n"
	.string	"-"
	.string	"i"
	.string	"n"
	.string	"c"
	.string	"r"
	.string	"e"
	.string	"m"
	.string	"e"
	.string	"n"
	.string	"t"
	.string	""
	.string	""
	.section	.rodata.str2.2
	.align 2
.LC63:
	.string	"m"
	.string	"e"
	.string	"a"
	.string	"s"
	.string	"u"
	.string	"r"
	.string	"e"
	.string	"-"
	.string	"u"
	.string	"n"
	.string	"i"
	.string	"t"
	.string	""
	.string	""
	.section	.rodata.str2.8
	.align 8
.LC64:
	.string	"p"
	.string	"e"
	.string	"r"
	.string	"-"
	.string	"m"
	.string	"e"
	.string	"a"
	.string	"s"
	.string	"u"
	.string	"r"
	.string	"e"
	.string	"-"
	.string	"u"
	.string	"n"
	.string	"i"
	.string	"t"
	.string	""
	.string	""
	.section	.rodata.str2.2
	.align 2
.LC65:
	.string	"u"
	.string	"n"
	.string	"i"
	.string	"t"
	.string	""
	.string	""
	.align 2
.LC66:
	.string	"c"
	.string	"u"
	.string	"r"
	.string	"r"
	.string	"e"
	.string	"n"
	.string	"c"
	.string	"y"
	.string	""
	.string	""
	.align 2
.LC67:
	.string	"i"
	.string	"n"
	.string	"t"
	.string	"e"
	.string	"g"
	.string	"e"
	.string	"r"
	.string	"-"
	.string	"w"
	.string	"i"
	.string	"d"
	.string	"t"
	.string	"h"
	.string	""
	.string	""
	.section	.rodata.str2.8
	.align 8
.LC68:
	.string	"n"
	.string	"u"
	.string	"m"
	.string	"b"
	.string	"e"
	.string	"r"
	.string	"i"
	.string	"n"
	.string	"g"
	.string	"-"
	.string	"s"
	.string	"y"
	.string	"s"
	.string	"t"
	.string	"e"
	.string	"m"
	.string	""
	.string	""
	.section	.rodata.str2.2
	.align 2
.LC69:
	.string	"s"
	.string	"c"
	.string	"a"
	.string	"l"
	.string	"e"
	.string	""
	.string	""
	.align 2
.LC70:
	.string	"K"
	.string	""
	.string	""
	.align 2
.LC71:
	.string	"K"
	.string	"K"
	.string	""
	.string	""
	.align 2
.LC72:
	.string	"%"
	.string	""
	.string	""
	.align 2
.LC73:
	.string	"%"
	.string	"x"
	.string	"1"
	.string	"0"
	.string	"0"
	.string	""
	.string	""
	.align 2
.LC74:
	.string	","
	.string	"_"
	.string	""
	.string	""
	.align 2
.LC75:
	.string	","
	.string	"?"
	.string	""
	.string	""
	.align 2
.LC76:
	.string	","
	.string	"!"
	.string	""
	.string	""
	.align 2
.LC77:
	.string	"+"
	.string	"!"
	.string	""
	.string	""
	.align 2
.LC78:
	.string	"+"
	.string	"_"
	.string	""
	.string	""
	.align 2
.LC79:
	.string	"("
	.string	")"
	.string	""
	.string	""
	.align 2
.LC80:
	.string	"("
	.string	")"
	.string	"!"
	.string	""
	.string	""
	.align 2
.LC81:
	.string	"+"
	.string	"?"
	.string	""
	.string	""
	.align 2
.LC82:
	.string	"("
	.string	")"
	.string	"?"
	.string	""
	.string	""
	.text
	.p2align 4
	.type	_ZN12_GLOBAL__N_119initNumberSkeletonsER10UErrorCode, @function
_ZN12_GLOBAL__N_119initNumberSkeletonsER10UErrorCode:
.LFB3942:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN12_GLOBAL__N_122cleanupNumberSkeletonsEv(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-160(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movl	$1, %edi
	subq	$192, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	ucln_i18n_registerCleanup_67@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6717UCharsTrieBuilderC1ER10UErrorCode@PLT
	movl	(%rbx), %r8d
	testl	%r8d, %r8d
	jle	.L728
.L719:
	movq	%r13, %rdi
	call	_ZN6icu_6717UCharsTrieBuilderD1Ev@PLT
.L716:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L729
	addq	$192, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L728:
	.cfi_restore_state
	leaq	-224(%rbp), %r12
	leaq	.LC37(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	xorl	%edx, %edx
	movq	%rbx, %rcx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC36(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%rbx, %rcx
	movl	$1, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC39(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%rbx, %rcx
	movl	$2, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC38(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%rbx, %rcx
	movl	$3, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC55(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%rbx, %rcx
	movl	$4, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC56(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%rbx, %rcx
	movl	$5, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC57(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%rbx, %rcx
	movl	$6, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC58(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%rbx, %rcx
	movl	$7, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC35(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%rbx, %rcx
	movl	$9, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC59(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%rbx, %rcx
	movl	$10, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC60(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%rbx, %rcx
	movl	$11, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC61(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%rbx, %rcx
	movl	$12, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC3(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%rbx, %rcx
	movl	$13, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC4(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%rbx, %rcx
	movl	$14, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC5(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%rbx, %rcx
	movl	$15, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC6(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%rbx, %rcx
	movl	$16, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC7(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%rbx, %rcx
	movl	$17, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC8(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%rbx, %rcx
	movl	$18, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC9(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%rbx, %rcx
	movl	$19, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC10(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%rbx, %rcx
	movl	$20, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC12(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%rbx, %rcx
	movl	$21, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC13(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%rbx, %rcx
	movl	$22, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC14(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%rbx, %rcx
	movl	$23, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC15(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%rbx, %rcx
	movl	$24, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC16(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%rbx, %rcx
	movl	$25, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC49(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%rbx, %rcx
	movl	$26, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC18(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%rbx, %rcx
	movl	$27, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC19(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%rbx, %rcx
	movl	$28, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC20(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%rbx, %rcx
	movl	$29, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC21(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%rbx, %rcx
	movl	$30, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC22(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%rbx, %rcx
	movl	$31, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC24(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%rbx, %rcx
	movl	$32, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC25(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%rbx, %rcx
	movl	$33, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC26(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%rbx, %rcx
	movl	$34, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC27(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%rbx, %rcx
	movl	$35, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC28(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%rbx, %rcx
	movl	$36, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC29(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%rbx, %rcx
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC30(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%rbx, %rcx
	movl	$38, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC32(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%rbx, %rcx
	movl	$39, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC33(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%rbx, %rcx
	movl	$40, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	(%rbx), %edi
	testl	%edi, %edi
	jg	.L719
	leaq	.LC62(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%rbx, %rcx
	movl	$41, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC63(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%rbx, %rcx
	movl	$42, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC64(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%rbx, %rcx
	movl	$43, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC65(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%rbx, %rcx
	movl	$44, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC66(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%rbx, %rcx
	movl	$45, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC67(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%rbx, %rcx
	movl	$46, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC68(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%rbx, %rcx
	movl	$47, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC69(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%r12, %rsi
	movq	%rbx, %rcx
	movl	$48, %edx
	movq	%r13, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	(%rbx), %esi
	testl	%esi, %esi
	jg	.L719
	leaq	.LC70(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	xorl	%edx, %edx
	movq	%rbx, %rcx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC71(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%rbx, %rcx
	movl	$1, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC72(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%rbx, %rcx
	movl	$6, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC73(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%rbx, %rcx
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC74(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%rbx, %rcx
	movl	$21, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC75(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%rbx, %rcx
	movl	$22, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC76(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%rbx, %rcx
	movl	$24, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC77(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%rbx, %rcx
	movl	$33, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC78(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%rbx, %rcx
	movl	$34, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC79(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%rbx, %rcx
	movl	$35, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC80(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%rbx, %rcx
	movl	$36, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC81(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%rbx, %rcx
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	.LC82(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%rbx, %rcx
	movl	$38, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L719
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%r12, %rdx
	movq	%rbx, %rcx
	xorl	%esi, %esi
	movq	%rax, -224(%rbp)
	movq	%r13, %rdi
	movl	$2, %eax
	movw	%ax, -216(%rbp)
	call	_ZN6icu_6717UCharsTrieBuilder18buildUnicodeStringE22UStringTrieBuildOptionRNS_13UnicodeStringER10UErrorCode@PLT
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L730
	movzwl	-216(%rbp), %eax
	testw	%ax, %ax
	js	.L720
	movswl	%ax, %r14d
	sarl	$5, %r14d
.L721:
	movslq	%r14d, %r14
	addq	%r14, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, _ZN12_GLOBAL__N_119kSerializedStemTrieE(%rip)
	movq	%rax, %rdi
	movzwl	-216(%rbp), %eax
	testb	$17, %al
	jne	.L725
	leaq	-214(%rbp), %rsi
	testb	$2, %al
	cmove	-200(%rbp), %rsi
.L722:
	movq	%r14, %rdx
	call	memcpy@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6717UCharsTrieBuilderD1Ev@PLT
	jmp	.L716
.L720:
	movl	-212(%rbp), %r14d
	jmp	.L721
.L730:
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L719
.L725:
	xorl	%esi, %esi
	jmp	.L722
.L729:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3942:
	.size	_ZN12_GLOBAL__N_119initNumberSkeletonsER10UErrorCode, .-_ZN12_GLOBAL__N_119initNumberSkeletonsER10UErrorCode
	.section	.text.unlikely
.LCOLDB83:
	.text
.LHOTB83:
	.p2align 4
	.globl	_ZN6icu_676number4impl8skeleton9parseStemERKNS_13StringSegmentERKNS_10UCharsTrieERNS1_14SeenMacroPropsERNS1_10MacroPropsER10UErrorCode
	.type	_ZN6icu_676number4impl8skeleton9parseStemERKNS_13StringSegmentERKNS_10UCharsTrieERNS1_14SeenMacroPropsERNS1_10MacroPropsER10UErrorCode, @function
_ZN6icu_676number4impl8skeleton9parseStemERKNS_13StringSegmentERKNS_10UCharsTrieERNS1_14SeenMacroPropsERNS1_10MacroPropsER10UErrorCode:
.LFB3984:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	xorl	%esi, %esi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK6icu_6713StringSegment6charAtEi@PLT
	cmpw	$64, %ax
	je	.L732
	ja	.L733
	cmpw	$46, %ax
	je	.L734
	cmpw	$48, %ax
	jne	.L736
	cmpb	$0, 7(%rbx)
	je	.L790
	.p2align 4,,10
	.p2align 3
.L740:
	movl	$65811, (%r12)
	xorl	%eax, %eax
.L731:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L791
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L733:
	.cfi_restore_state
	cmpw	$69, %ax
	jne	.L736
	cmpb	$0, (%rbx)
	jne	.L740
	movb	$1, (%rbx)
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_676number4impl17blueprint_helpers19parseScientificStemERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode
	xorl	%eax, %eax
	jmp	.L731
	.p2align 4,,10
	.p2align 3
.L736:
	movq	%r14, %rdi
	call	_ZNK6icu_6710UCharsTrie7currentEv@PLT
	subl	$2, %eax
	cmpl	$1, %eax
	ja	.L740
	movq	16(%r14), %rdx
	movzwl	(%rdx), %eax
	testw	%ax, %ax
	js	.L792
	movzwl	%ax, %ecx
	cmpw	$16447, %ax
	jbe	.L793
	movzwl	2(%rdx), %r14d
	cmpl	$32703, %ecx
	jg	.L748
	andl	$32704, %eax
	subl	$16448, %eax
	sall	$10, %eax
	orl	%eax, %r14d
	.p2align 4,,10
	.p2align 3
.L745:
	cmpl	$48, %r14d
	ja	.L749
	leaq	.L751(%rip), %rcx
	movl	%r14d, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L751:
	.long	.L768-.L751
	.long	.L768-.L751
	.long	.L768-.L751
	.long	.L768-.L751
	.long	.L768-.L751
	.long	.L767-.L751
	.long	.L767-.L751
	.long	.L767-.L751
	.long	.L766-.L751
	.long	.L765-.L751
	.long	.L765-.L751
	.long	.L765-.L751
	.long	.L765-.L751
	.long	.L764-.L751
	.long	.L764-.L751
	.long	.L764-.L751
	.long	.L764-.L751
	.long	.L764-.L751
	.long	.L764-.L751
	.long	.L764-.L751
	.long	.L764-.L751
	.long	.L763-.L751
	.long	.L763-.L751
	.long	.L763-.L751
	.long	.L763-.L751
	.long	.L763-.L751
	.long	.L762-.L751
	.long	.L761-.L751
	.long	.L761-.L751
	.long	.L761-.L751
	.long	.L761-.L751
	.long	.L761-.L751
	.long	.L760-.L751
	.long	.L760-.L751
	.long	.L760-.L751
	.long	.L760-.L751
	.long	.L760-.L751
	.long	.L760-.L751
	.long	.L760-.L751
	.long	.L759-.L751
	.long	.L759-.L751
	.long	.L758-.L751
	.long	.L757-.L751
	.long	.L756-.L751
	.long	.L755-.L751
	.long	.L754-.L751
	.long	.L753-.L751
	.long	.L752-.L751
	.long	.L750-.L751
	.text
	.p2align 4,,10
	.p2align 3
.L734:
	cmpb	$0, 3(%rbx)
	jne	.L740
	movb	$1, 3(%rbx)
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_676number4impl17blueprint_helpers17parseFractionStemERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode
	movl	$2, %eax
	jmp	.L731
	.p2align 4,,10
	.p2align 3
.L732:
	cmpb	$0, 3(%rbx)
	jne	.L740
	movb	$1, 3(%rbx)
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_676number4impl17blueprint_helpers15parseDigitsStemERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode
	xorl	%eax, %eax
	jmp	.L731
	.p2align 4,,10
	.p2align 3
.L790:
	movb	$1, 7(%rbx)
	movl	$1, %r14d
	jmp	.L743
	.p2align 4,,10
	.p2align 3
.L795:
	movl	%r14d, %esi
	movq	%r15, %rdi
	call	_ZNK6icu_6713StringSegment6charAtEi@PLT
	cmpw	$48, %ax
	jne	.L794
	addl	$1, %r14d
.L743:
	movq	%r15, %rdi
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	cmpl	%r14d, %eax
	jg	.L795
.L741:
	movq	%r15, %rdi
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	cmpl	%r14d, %eax
	jg	.L740
	movl	%r14d, %edi
	call	_ZN6icu_676number12IntegerWidth10zeroFillToEi@PLT
	movq	%rax, -96(%rbp)
	movq	%rax, 124(%r13)
	xorl	%eax, %eax
	movl	%edx, -88(%rbp)
	movb	%dl, 132(%r13)
	jmp	.L731
	.p2align 4,,10
	.p2align 3
.L793:
	sarl	$6, %ecx
	leal	-1(%rcx), %r14d
	jmp	.L745
	.p2align 4,,10
	.p2align 3
.L764:
	cmpb	$0, 4(%rbx)
	jne	.L740
	subl	$13, %r14d
	movb	$1, 4(%rbx)
	cmpl	$7, %r14d
	ja	.L749
	movl	%r14d, 96(%r13)
	xorl	%eax, %eax
	jmp	.L731
	.p2align 4,,10
	.p2align 3
.L760:
	cmpb	$0, 10(%rbx)
	jne	.L740
	subl	$32, %r14d
	movl	$7, %eax
	movb	$1, 10(%rbx)
	cmpl	$7, %r14d
	cmovnb	%eax, %r14d
	xorl	%eax, %eax
	movl	%r14d, 156(%r13)
	jmp	.L731
	.p2align 4,,10
	.p2align 3
.L761:
	cmpb	$0, 9(%rbx)
	jne	.L740
	subl	$27, %r14d
	movl	$5, %eax
	movb	$1, 9(%rbx)
	cmpl	$5, %r14d
	cmovnb	%eax, %r14d
	xorl	%eax, %eax
	movl	%r14d, 152(%r13)
	jmp	.L731
	.p2align 4,,10
	.p2align 3
.L763:
	cmpb	$0, 5(%rbx)
	jne	.L740
	leal	-21(%r14), %edi
	movl	$5, %eax
	movb	$1, 5(%rbx)
	cmpl	$5, %edi
	cmovnb	%eax, %edi
	call	_ZN6icu_676number4impl7Grouper11forStrategyE23UNumberGroupingStrategy@PLT
	movq	%rax, 100(%r13)
	xorl	%eax, %eax
	movl	%edx, 108(%r13)
	jmp	.L731
	.p2align 4,,10
	.p2align 3
.L768:
	cmpb	$0, (%rbx)
	jne	.L740
	movb	$1, (%rbx)
	cmpl	$3, %r14d
	je	.L769
	cmpl	$4, %r14d
	je	.L770
	cmpl	$1, %r14d
	je	.L771
	cmpl	$2, %r14d
	jne	.L796
	call	_ZN6icu_676number8Notation10scientificEv@PLT
	movq	%rax, -128(%rbp)
	movq	%rax, -140(%rbp)
	movl	%edx, -120(%rbp)
	movl	%edx, -132(%rbp)
	movq	%rax, %rdx
.L773:
	movl	-132(%rbp), %eax
	subl	$2, %r14d
	movq	%rdx, -96(%rbp)
	movq	%rdx, 4(%r13)
	movl	%eax, -88(%rbp)
	movl	%eax, 12(%r13)
	xorl	%eax, %eax
	cmpl	$1, %r14d
	setbe	%al
	jmp	.L731
.L765:
	cmpb	$0, 3(%rbx)
	jne	.L740
	movb	$1, 3(%rbx)
	leaq	-96(%rbp), %rdi
	movl	%r14d, %esi
	call	_ZN6icu_676number4impl14stem_to_object9precisionENS1_8skeleton8StemEnumE
	movq	-80(%rbp), %rax
	movdqa	-96(%rbp), %xmm0
	movq	%rax, 80(%r13)
	movl	-72(%rbp), %eax
	movups	%xmm0, 64(%r13)
	movl	%eax, 88(%r13)
	xorl	%eax, %eax
	cmpl	$9, %r14d
	sete	%al
	addl	%eax, %eax
	jmp	.L731
.L767:
	cmpb	$0, 1(%rbx)
	jne	.L740
	movb	$1, 1(%rbx)
	cmpl	$6, %r14d
	je	.L774
	cmpl	$7, %r14d
	leaq	-96(%rbp), %r14
	movq	%r14, %rdi
	je	.L775
	call	_ZN6icu_676NoUnit4baseEv@PLT
.L789:
	leaq	-128(%rbp), %r12
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6711MeasureUnitC1EOS0_@PLT
	movq	%r14, %rdi
	call	_ZN6icu_676NoUnitD1Ev@PLT
	leaq	16(%r13), %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6711MeasureUnitaSEOS0_@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6711MeasureUnitD1Ev@PLT
	xorl	%eax, %eax
	jmp	.L731
.L759:
	cmpb	$0, 11(%rbx)
	jne	.L740
	xorl	%eax, %eax
	cmpl	$40, %r14d
	movb	$1, 11(%rbx)
	sete	%al
	movl	%eax, 160(%r13)
	xorl	%eax, %eax
	jmp	.L731
.L758:
	cmpb	$0, 3(%rbx)
	jne	.L740
	movb	$1, 3(%rbx)
	movl	$3, %eax
	jmp	.L731
.L766:
	cmpb	$0, 12(%rbx)
	jne	.L740
	cmpb	$0, 1(%rbx)
	movb	$1, 12(%rbx)
	jne	.L740
	movb	$1, 1(%rbx)
	leaq	-96(%rbp), %r12
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZN6icu_676number5Scale10powerOfTenEi@PLT
	movq	%r12, %rsi
	leaq	168(%r13), %rdi
	call	_ZN6icu_676number5ScaleaSEOS1_@PLT
	movq	%r12, %rdi
	call	_ZN6icu_676number5ScaleD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_676NoUnit7percentEv@PLT
	leaq	16(%r13), %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6711MeasureUnitaSEOS0_@PLT
	movq	%r12, %rdi
	call	_ZN6icu_676NoUnitD1Ev@PLT
	xorl	%eax, %eax
	jmp	.L731
.L762:
	cmpb	$0, 8(%rbx)
	jne	.L740
	movb	$1, 8(%rbx)
	movq	%r12, %rsi
	leaq	.LC48(%rip), %rdi
	call	_ZN6icu_6715NumberingSystem20createInstanceByNameEPKcR10UErrorCode@PLT
	leaq	136(%r13), %rdi
	movq	%rax, %rsi
	call	_ZN6icu_676number4impl14SymbolsWrapper5setToEPKNS_15NumberingSystemE@PLT
	xorl	%eax, %eax
	jmp	.L731
.L750:
	cmpb	$0, 12(%rbx)
	jne	.L740
	movb	$1, 12(%rbx)
	movl	$10, %eax
	jmp	.L731
.L752:
	cmpb	$0, 8(%rbx)
	jne	.L740
	movb	$1, 8(%rbx)
	movl	$9, %eax
	jmp	.L731
.L753:
	cmpb	$0, 7(%rbx)
	jne	.L740
	movb	$1, 7(%rbx)
	movl	$8, %eax
	jmp	.L731
.L754:
	cmpb	$0, 1(%rbx)
	jne	.L740
	movb	$1, 1(%rbx)
	movl	$7, %eax
	jmp	.L731
.L755:
	cmpb	$0, 1(%rbx)
	jne	.L740
	cmpb	$0, 2(%rbx)
	movb	$1, 1(%rbx)
	jne	.L740
	movb	$1, 2(%rbx)
	movl	$6, %eax
	jmp	.L731
.L756:
	cmpb	$0, 2(%rbx)
	jne	.L740
	movb	$1, 2(%rbx)
	movl	$5, %eax
	jmp	.L731
.L757:
	cmpb	$0, 1(%rbx)
	jne	.L740
	movb	$1, 1(%rbx)
	movl	$4, %eax
	jmp	.L731
	.p2align 4,,10
	.p2align 3
.L794:
	subl	$1, %r14d
	jmp	.L741
	.p2align 4,,10
	.p2align 3
.L792:
	movl	%eax, %ecx
	movl	%eax, %r14d
	andw	$32767, %cx
	andl	$32767, %r14d
	testb	$64, %ah
	je	.L745
	movzwl	2(%rdx), %eax
	cmpw	$32767, %cx
	je	.L746
	subl	$16384, %r14d
	sall	$16, %r14d
	orl	%eax, %r14d
	jmp	.L745
	.p2align 4,,10
	.p2align 3
.L748:
	movzwl	4(%rdx), %eax
	sall	$16, %r14d
	orl	%eax, %r14d
	jmp	.L745
	.p2align 4,,10
	.p2align 3
.L746:
	movzwl	4(%rdx), %r14d
	sall	$16, %eax
	orl	%eax, %r14d
	jmp	.L745
.L775:
	call	_ZN6icu_676NoUnit8permilleEv@PLT
	jmp	.L789
.L774:
	leaq	-96(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN6icu_676NoUnit7percentEv@PLT
	jmp	.L789
.L796:
	call	_ZN6icu_676number8Notation12compactShortEv@PLT
	movl	%edx, -132(%rbp)
	movq	%rax, %rdx
	movq	%rax, -140(%rbp)
	jmp	.L773
.L771:
	call	_ZN6icu_676number8Notation11compactLongEv@PLT
	movl	%edx, -132(%rbp)
	movq	%rax, %rdx
	movq	%rax, -140(%rbp)
	jmp	.L773
.L770:
	call	_ZN6icu_676number8Notation6simpleEv@PLT
	movl	%edx, -132(%rbp)
	movq	%rax, %rdx
	movq	%rax, -140(%rbp)
	jmp	.L773
.L769:
	call	_ZN6icu_676number8Notation11engineeringEv@PLT
	movl	%edx, -132(%rbp)
	movq	%rax, %rdx
	movq	%rax, -140(%rbp)
	jmp	.L773
.L791:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_676number4impl8skeleton9parseStemERKNS_13StringSegmentERKNS_10UCharsTrieERNS1_14SeenMacroPropsERNS1_10MacroPropsER10UErrorCode.cold, @function
_ZN6icu_676number4impl8skeleton9parseStemERKNS_13StringSegmentERKNS_10UCharsTrieERNS1_14SeenMacroPropsERNS1_10MacroPropsER10UErrorCode.cold:
.LFSB3984:
.L749:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	call	abort@PLT
	.cfi_endproc
.LFE3984:
	.text
	.size	_ZN6icu_676number4impl8skeleton9parseStemERKNS_13StringSegmentERKNS_10UCharsTrieERNS1_14SeenMacroPropsERNS1_10MacroPropsER10UErrorCode, .-_ZN6icu_676number4impl8skeleton9parseStemERKNS_13StringSegmentERKNS_10UCharsTrieERNS1_14SeenMacroPropsERNS1_10MacroPropsER10UErrorCode
	.section	.text.unlikely
	.size	_ZN6icu_676number4impl8skeleton9parseStemERKNS_13StringSegmentERKNS_10UCharsTrieERNS1_14SeenMacroPropsERNS1_10MacroPropsER10UErrorCode.cold, .-_ZN6icu_676number4impl8skeleton9parseStemERKNS_13StringSegmentERKNS_10UCharsTrieERNS1_14SeenMacroPropsERNS1_10MacroPropsER10UErrorCode.cold
.LCOLDE83:
	.text
.LHOTE83:
	.p2align 4
	.globl	_ZN6icu_676number4impl17blueprint_helpers25parseMeasurePerUnitOptionERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode
	.type	_ZN6icu_676number4impl17blueprint_helpers25parseMeasurePerUnitOptionERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode, @function
_ZN6icu_676number4impl17blueprint_helpers25parseMeasurePerUnitOptionERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode:
.LFB3994:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	16(%rsi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r14, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movq	%r15, %rsi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl17blueprint_helpers22parseMeasureUnitOptionERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L802
	leaq	40(%rbx), %rdi
	movq	%r15, %rsi
	call	_ZN6icu_6711MeasureUnitaSERKS0_@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6711MeasureUnitaSERKS0_@PLT
.L802:
	movq	%r14, %rdi
	call	_ZN6icu_6711MeasureUnitD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L803
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L803:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3994:
	.size	_ZN6icu_676number4impl17blueprint_helpers25parseMeasurePerUnitOptionERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode, .-_ZN6icu_676number4impl17blueprint_helpers25parseMeasurePerUnitOptionERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode
	.p2align 4
	.globl	_ZN6icu_676number4impl17blueprint_helpers25parseIdentifierUnitOptionERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode
	.type	_ZN6icu_676number4impl17blueprint_helpers25parseIdentifierUnitOptionERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode, @function
_ZN6icu_676number4impl17blueprint_helpers25parseIdentifierUnitOptionERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode:
.LFB3995:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-288(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$408, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -440(%rbp)
	movq	%rdi, %rsi
	movq	%r14, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-339(%rbp), %rax
	movw	%cx, -340(%rbp)
	movq	%rax, -352(%rbp)
	movl	$0, -296(%rbp)
	movl	$40, -344(%rbp)
	movl	$0, -416(%rbp)
	call	_ZNK6icu_6713StringSegment19toTempUnicodeStringEv@PLT
	movzwl	-280(%rbp), %edx
	testb	$17, %dl
	jne	.L837
	leaq	-278(%rbp), %rax
	andl	$2, %edx
	cmove	-264(%rbp), %rax
.L805:
	movq	%r12, %rdi
	leaq	-224(%rbp), %r15
	movq	%rax, -384(%rbp)
	leaq	-384(%rbp), %r12
	leaq	-416(%rbp), %rbx
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	movq	%r12, %rdx
	xorl	%esi, %esi
	movq	%r15, %rdi
	movl	%eax, %ecx
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	leaq	-352(%rbp), %rdi
	movq	%rbx, %rdx
	movq	%r15, %rsi
	call	_ZN6icu_6710CharString20appendInvariantCharsERKNS_13UnicodeStringER10UErrorCode@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-416(%rbp), %eax
	cmpl	$26, %eax
	je	.L845
	testl	%eax, %eax
	jle	.L808
	movl	%eax, 0(%r13)
.L811:
	cmpb	$0, -340(%rbp)
	jne	.L843
.L804:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L846
	addq	$408, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L808:
	.cfi_restore_state
	movl	-296(%rbp), %edx
	movq	-352(%rbp), %rsi
	movq	%r15, %rdi
	leaq	16+_ZTVN6icu_679ErrorCodeE(%rip), %rax
	leaq	-424(%rbp), %rcx
	movq	%rax, -432(%rbp)
	movl	$0, -424(%rbp)
	call	_ZN6icu_6715MeasureUnitImpl13forIdentifierENS_11StringPieceER10UErrorCode@PLT
	movl	-424(%rbp), %edx
	testl	%edx, %edx
	jg	.L847
	movl	-216(%rbp), %eax
	testl	%eax, %eax
	jle	.L819
	movq	-440(%rbp), %rax
	xorl	%r14d, %r14d
	addq	$40, %rax
	movq	%rax, -448(%rbp)
	jmp	.L823
	.p2align 4,,10
	.p2align 3
.L849:
	movq	-440(%rbp), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	16(%rax), %r15
	call	_ZNK6icu_6714SingleUnitImpl5buildER10UErrorCode@PLT
	movq	%r13, %rcx
	movq	%rbx, %rdx
.L842:
	movq	%r15, %rsi
	movq	%r12, %rdi
	addq	$1, %r14
	call	_ZNK6icu_6711MeasureUnit7productERKS0_R10UErrorCode@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6711MeasureUnitaSEOS0_@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6711MeasureUnitD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6711MeasureUnitD1Ev@PLT
	movl	-216(%rbp), %eax
	cmpl	%r14d, %eax
	jle	.L848
.L823:
	movq	-208(%rbp), %rax
	movq	(%rax,%r14,8), %rsi
	movl	8(%rsi), %eax
	testl	%eax, %eax
	jg	.L849
	negl	%eax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	movl	%eax, 8(%rsi)
	call	_ZNK6icu_6714SingleUnitImpl5buildER10UErrorCode@PLT
	movq	-448(%rbp), %r15
	movq	%r13, %rcx
	movq	%rbx, %rdx
	jmp	.L842
	.p2align 4,,10
	.p2align 3
.L819:
	cmpb	$0, -116(%rbp)
	movq	-208(%rbp), %r8
	jne	.L834
	.p2align 4,,10
	.p2align 3
.L825:
	cmpb	$0, -196(%rbp)
	je	.L830
	movq	%r8, %rdi
	call	uprv_free_67@PLT
.L830:
	leaq	-432(%rbp), %rdi
	call	_ZN6icu_679ErrorCodeD1Ev@PLT
	cmpb	$0, -340(%rbp)
	je	.L804
.L843:
	movq	-352(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L804
	.p2align 4,,10
	.p2align 3
.L845:
	movl	$65811, 0(%r13)
	jmp	.L811
	.p2align 4,,10
	.p2align 3
.L848:
	cmpb	$0, -116(%rbp)
	jne	.L834
.L824:
	movq	-208(%rbp), %r8
	testl	%eax, %eax
	jle	.L825
	xorl	%ebx, %ebx
.L829:
	movq	(%r8,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.L826
.L850:
	call	_ZN6icu_677UMemorydlEPv@PLT
	movl	-216(%rbp), %eax
	addq	$1, %rbx
	movq	-208(%rbp), %r8
	cmpl	%ebx, %eax
	jle	.L825
	movq	(%r8,%rbx,8), %rdi
	testq	%rdi, %rdi
	jne	.L850
.L826:
	addq	$1, %rbx
	cmpl	%ebx, %eax
	jg	.L829
	jmp	.L825
	.p2align 4,,10
	.p2align 3
.L834:
	movq	-128(%rbp), %rdi
	call	uprv_free_67@PLT
	movl	-216(%rbp), %eax
	jmp	.L824
	.p2align 4,,10
	.p2align 3
.L837:
	xorl	%eax, %eax
	jmp	.L805
	.p2align 4,,10
	.p2align 3
.L847:
	cmpb	$0, -116(%rbp)
	movl	$65811, 0(%r13)
	jne	.L851
.L812:
	movl	-216(%rbp), %eax
	movq	-208(%rbp), %r8
	testl	%eax, %eax
	jle	.L813
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L817:
	movq	(%r8,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.L814
	call	_ZN6icu_677UMemorydlEPv@PLT
	movl	-216(%rbp), %eax
	addq	$1, %rbx
	movq	-208(%rbp), %r8
	cmpl	%ebx, %eax
	jg	.L817
.L813:
	cmpb	$0, -196(%rbp)
	jne	.L852
.L818:
	leaq	-432(%rbp), %rdi
	call	_ZN6icu_679ErrorCodeD1Ev@PLT
	jmp	.L811
	.p2align 4,,10
	.p2align 3
.L814:
	addq	$1, %rbx
	cmpl	%ebx, %eax
	jg	.L817
	jmp	.L813
	.p2align 4,,10
	.p2align 3
.L851:
	movq	-128(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L812
	.p2align 4,,10
	.p2align 3
.L852:
	movq	%r8, %rdi
	call	uprv_free_67@PLT
	jmp	.L818
.L846:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3995:
	.size	_ZN6icu_676number4impl17blueprint_helpers25parseIdentifierUnitOptionERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode, .-_ZN6icu_676number4impl17blueprint_helpers25parseIdentifierUnitOptionERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode
	.p2align 4
	.globl	_ZN6icu_676number4impl17blueprint_helpers19parseCurrencyOptionERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode
	.type	_ZN6icu_676number4impl17blueprint_helpers19parseCurrencyOptionERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode, @function
_ZN6icu_676number4impl17blueprint_helpers19parseCurrencyOptionERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode:
.LFB3990:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	subq	$96, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	cmpl	$3, %eax
	je	.L854
	movl	$65811, (%rbx)
.L853:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L863
	addq	$96, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L854:
	.cfi_restore_state
	leaq	-112(%rbp), %r14
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZNK6icu_6713StringSegment19toTempUnicodeStringEv@PLT
	movzwl	-104(%rbp), %eax
	testb	$17, %al
	jne	.L860
	leaq	-102(%rbp), %r12
	testb	$2, %al
	cmove	-88(%rbp), %r12
.L856:
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	-124(%rbp), %rdx
	leaq	-120(%rbp), %rsi
	movq	%r14, %rdi
	movl	$0, -124(%rbp)
	movq	%r12, -120(%rbp)
	call	_ZN6icu_6712CurrencyUnitC1ENS_14ConstChar16PtrER10UErrorCode@PLT
	movl	-124(%rbp), %eax
	testl	%eax, %eax
	jg	.L864
	leaq	16(%r13), %rdi
	movq	%r14, %rsi
	call	_ZN6icu_6711MeasureUnitaSERKS0_@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	jmp	.L853
	.p2align 4,,10
	.p2align 3
.L864:
	movl	$65811, (%rbx)
	movq	%r14, %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	jmp	.L853
	.p2align 4,,10
	.p2align 3
.L860:
	xorl	%r12d, %r12d
	jmp	.L856
.L863:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3990:
	.size	_ZN6icu_676number4impl17blueprint_helpers19parseCurrencyOptionERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode, .-_ZN6icu_676number4impl17blueprint_helpers19parseCurrencyOptionERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode
	.section	.rodata.str2.2
	.align 2
.LC84:
	.string	"c"
	.string	"u"
	.string	"r"
	.string	"r"
	.string	"e"
	.string	"n"
	.string	"c"
	.string	"y"
	.string	"/"
	.string	""
	.string	""
	.align 2
.LC85:
	.string	"m"
	.string	"e"
	.string	"a"
	.string	"s"
	.string	"u"
	.string	"r"
	.string	"e"
	.string	"-"
	.string	"u"
	.string	"n"
	.string	"i"
	.string	"t"
	.string	"/"
	.string	""
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl16GeneratorHelpers4unitERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_676number4impl16GeneratorHelpers4unitERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_676number4impl16GeneratorHelpers4unitERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode:
.LFB4012:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	16(%rdi), %r12
	movq	%r12, %rdi
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK6icu_6711MeasureUnit7getTypeEv@PLT
	movl	$9, %ecx
	leaq	.LC43(%rip), %rsi
	movq	%rax, %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L866
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC84(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC84(%rip), %rax
	leaq	-80(%rbp), %r15
	movq	%r12, %rsi
	movq	%r13, %rdx
	xorl	%r12d, %r12d
	movq	%r15, %rdi
	call	_ZN6icu_6712CurrencyUnitC1ERKNS_11MeasureUnitER10UErrorCode@PLT
	movl	0(%r13), %eax
	testl	%eax, %eax
	jle	.L875
.L867:
	movq	%r15, %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
.L865:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L876
	addq	$48, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L866:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK6icu_6711MeasureUnit7getTypeEv@PLT
	movl	$5, %ecx
	leaq	.LC40(%rip), %rsi
	movq	%rax, %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L869
	movq	%r12, %rdi
	call	_ZNK6icu_6711MeasureUnit10getSubtypeEv@PLT
	movl	$8, %ecx
	leaq	.LC41(%rip), %rsi
	movq	%rax, %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L877
	movq	%r12, %rdi
	call	_ZNK6icu_6711MeasureUnit10getSubtypeEv@PLT
	movl	$9, %ecx
	leaq	.LC42(%rip), %rsi
	movq	%rax, %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	xorl	%r12d, %r12d
	testb	%al, %al
	jne	.L865
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC58(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC58(%rip), %rax
	movl	$1, %r12d
	jmp	.L865
	.p2align 4,,10
	.p2align 3
.L875:
	leaq	-60(%rbp), %r12
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movl	$1, %r12d
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L869:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC85(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC85(%rip), %rax
	movq	%r12, %rdi
	movq	%r13, %rdx
	movq	%r14, %rsi
	movl	$1, %r12d
	call	_ZN6icu_676number4impl17blueprint_helpers25generateMeasureUnitOptionERKNS_11MeasureUnitERNS_13UnicodeStringER10UErrorCode
	jmp	.L865
	.p2align 4,,10
	.p2align 3
.L877:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC57(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC57(%rip), %rax
	movl	$1, %r12d
	jmp	.L865
.L876:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4012:
	.size	_ZN6icu_676number4impl16GeneratorHelpers4unitERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_676number4impl16GeneratorHelpers4unitERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode
	.p2align 4
	.globl	_ZN6icu_676number4impl17blueprint_helpers20parseIncrementOptionERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode
	.type	_ZN6icu_676number4impl17blueprint_helpers20parseIncrementOptionERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode, @function
_ZN6icu_676number4impl17blueprint_helpers20parseIncrementOptionERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode:
.LFB4003:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-192(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$392, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -432(%rbp)
	movq	%rdi, %rsi
	movq	%r13, %rdi
	movq	%rdx, -424(%rbp)
	xorl	%edx, %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-243(%rbp), %rax
	movw	%dx, -244(%rbp)
	movq	%rax, -256(%rbp)
	movl	$0, -200(%rbp)
	movl	$40, -248(%rbp)
	movl	$0, -368(%rbp)
	call	_ZNK6icu_6713StringSegment19toTempUnicodeStringEv@PLT
	movzwl	-184(%rbp), %edx
	testb	$17, %dl
	jne	.L894
	leaq	-182(%rbp), %rax
	andl	$2, %edx
	cmove	-168(%rbp), %rax
.L879:
	movq	%r12, %rdi
	leaq	-128(%rbp), %rbx
	movq	%rax, -336(%rbp)
	leaq	-336(%rbp), %r14
	leaq	-368(%rbp), %r15
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movl	%eax, %ecx
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	leaq	-256(%rbp), %rdi
	movq	%r15, %rdx
	movq	%rbx, %rsi
	call	_ZN6icu_6710CharString20appendInvariantCharsERKNS_13UnicodeStringER10UErrorCode@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-368(%rbp), %eax
	cmpl	$26, %eax
	je	.L902
	testl	%eax, %eax
	jle	.L882
	movq	-424(%rbp), %rcx
	movl	%eax, (%rcx)
.L885:
	cmpb	$0, -244(%rbp)
	jne	.L901
.L878:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L903
	addq	$392, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L882:
	.cfi_restore_state
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityC1Ev@PLT
	movl	-200(%rbp), %edx
	movq	-256(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-404(%rbp), %rcx
	movl	$0, -404(%rbp)
	call	_ZN6icu_676number4impl15DecimalQuantity14setToDecNumberENS_11StringPieceER10UErrorCode@PLT
	movl	-404(%rbp), %eax
	testl	%eax, %eax
	jg	.L904
	movq	%r14, %rdi
	xorl	%ebx, %ebx
	call	_ZNK6icu_676number4impl15DecimalQuantity8toDoubleEv@PLT
	movsd	%xmm0, -424(%rbp)
	jmp	.L887
	.p2align 4,,10
	.p2align 3
.L905:
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_6713StringSegment6charAtEi@PLT
	cmpw	$46, %ax
	je	.L886
	addl	$1, %ebx
.L887:
	movq	%r12, %rdi
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	cmpl	%ebx, %eax
	jg	.L905
.L886:
	movq	%r12, %rdi
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	cmpl	%ebx, %eax
	je	.L906
	movq	%r12, %rdi
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	leaq	-400(%rbp), %rsi
	movsd	-424(%rbp), %xmm0
	movl	%eax, %r12d
	movq	%rsi, %rdi
	movq	%rsi, -424(%rbp)
	call	_ZN6icu_676number9Precision9incrementEd@PLT
	subl	%ebx, %r12d
	movq	-424(%rbp), %rsi
	movq	%r15, %rdi
	leal	-1(%r12), %edx
	call	_ZNK6icu_676number18IncrementPrecision15withMinFractionEi@PLT
	movq	-432(%rbp), %rcx
	movdqa	-368(%rbp), %xmm1
	movups	%xmm1, 64(%rcx)
	movq	-352(%rbp), %rax
	movq	%rax, 80(%rcx)
	movl	-344(%rbp), %eax
	movl	%eax, 88(%rcx)
.L889:
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	cmpb	$0, -244(%rbp)
	je	.L878
	.p2align 4,,10
	.p2align 3
.L901:
	movq	-256(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L878
	.p2align 4,,10
	.p2align 3
.L902:
	movq	-424(%rbp), %rax
	movl	$65811, (%rax)
	jmp	.L885
	.p2align 4,,10
	.p2align 3
.L904:
	movq	-424(%rbp), %rax
	movq	%r14, %rdi
	movl	$65811, (%rax)
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	jmp	.L885
	.p2align 4,,10
	.p2align 3
.L894:
	xorl	%eax, %eax
	jmp	.L879
	.p2align 4,,10
	.p2align 3
.L906:
	movsd	-424(%rbp), %xmm0
	movq	%r15, %rdi
	call	_ZN6icu_676number9Precision9incrementEd@PLT
	movq	-432(%rbp), %rcx
	movdqa	-368(%rbp), %xmm2
	movups	%xmm2, 64(%rcx)
	movq	-352(%rbp), %rax
	movq	%rax, 80(%rcx)
	movl	-344(%rbp), %eax
	movl	%eax, 88(%rcx)
	jmp	.L889
.L903:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4003:
	.size	_ZN6icu_676number4impl17blueprint_helpers20parseIncrementOptionERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode, .-_ZN6icu_676number4impl17blueprint_helpers20parseIncrementOptionERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode
	.p2align 4
	.globl	_ZN6icu_676number4impl8skeleton11parseOptionENS2_10ParseStateERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode
	.type	_ZN6icu_676number4impl8skeleton11parseOptionENS2_10ParseStateERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode, @function
_ZN6icu_676number4impl8skeleton11parseOptionENS2_10ParseStateERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode:
.LFB3985:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leal	-3(%rdi), %eax
	cmpl	$7, %eax
	ja	.L908
	leaq	.L910(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L910:
	.long	.L917-.L910
	.long	.L916-.L910
	.long	.L915-.L910
	.long	.L914-.L910
	.long	.L913-.L910
	.long	.L912-.L910
	.long	.L911-.L910
	.long	.L909-.L910
	.text
	.p2align 4,,10
	.p2align 3
.L911:
	movq	%rcx, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl17blueprint_helpers26parseNumberingSystemOptionERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L907:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L934
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L909:
	.cfi_restore_state
	movq	%rcx, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl17blueprint_helpers16parseScaleOptionERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode
	xorl	%eax, %eax
	jmp	.L907
	.p2align 4,,10
	.p2align 3
.L917:
	movq	%rcx, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl17blueprint_helpers20parseIncrementOptionERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode
	xorl	%eax, %eax
	jmp	.L907
	.p2align 4,,10
	.p2align 3
.L916:
	movq	%rcx, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl17blueprint_helpers22parseMeasureUnitOptionERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode
	xorl	%eax, %eax
	jmp	.L907
	.p2align 4,,10
	.p2align 3
.L915:
	leaq	16(%r12), %rbx
	leaq	-80(%rbp), %r15
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6711MeasureUnitC1ERKS0_@PLT
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl17blueprint_helpers22parseMeasureUnitOptionERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode
	movl	(%r14), %ecx
	testl	%ecx, %ecx
	jg	.L932
	leaq	40(%r12), %rdi
	movq	%rbx, %rsi
	call	_ZN6icu_6711MeasureUnitaSERKS0_@PLT
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6711MeasureUnitaSERKS0_@PLT
.L932:
	movq	%r15, %rdi
	call	_ZN6icu_6711MeasureUnitD1Ev@PLT
.L933:
	xorl	%eax, %eax
	jmp	.L907
	.p2align 4,,10
	.p2align 3
.L914:
	movq	%rcx, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl17blueprint_helpers25parseIdentifierUnitOptionERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode
	xorl	%eax, %eax
	jmp	.L907
	.p2align 4,,10
	.p2align 3
.L913:
	movq	%rcx, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl17blueprint_helpers19parseCurrencyOptionERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode
	xorl	%eax, %eax
	jmp	.L907
	.p2align 4,,10
	.p2align 3
.L912:
	movq	%rcx, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl17blueprint_helpers23parseIntegerWidthOptionERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode
	xorl	%eax, %eax
	jmp	.L907
.L908:
	cmpl	$1, %edi
	je	.L935
	cmpl	$2, %edi
	je	.L936
.L926:
	movl	$65811, (%r14)
	xorl	%eax, %eax
	jmp	.L907
	.p2align 4,,10
	.p2align 3
.L935:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZNK6icu_6713StringSegment6charAtEi@PLT
	subl	$42, %eax
	cmpw	$1, %ax
	ja	.L921
	movl	$1, %ebx
	jmp	.L923
	.p2align 4,,10
	.p2align 3
.L937:
	movl	%ebx, %esi
	movq	%r13, %rdi
	call	_ZNK6icu_6713StringSegment6charAtEi@PLT
	cmpw	$101, %ax
	jne	.L922
	addl	$1, %ebx
.L923:
	movq	%r13, %rdi
	leal	-1(%rbx), %r15d
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	cmpl	%ebx, %eax
	jg	.L937
.L922:
	movq	%r13, %rdi
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	cmpl	%eax, %ebx
	jl	.L921
	leaq	4(%r12), %rdi
	movl	%r15d, %esi
	call	_ZNK6icu_676number18ScientificNotation21withMinExponentDigitsEi@PLT
	movq	%rax, -80(%rbp)
	movq	%rax, 4(%r12)
	movl	$1, %eax
	movl	%edx, -72(%rbp)
	movl	%edx, 12(%r12)
	jmp	.L907
	.p2align 4,,10
	.p2align 3
.L936:
	movq	%rcx, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl17blueprint_helpers18parseFracSigOptionERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode
	testb	%al, %al
	jne	.L933
.L930:
	movl	(%r14), %eax
	testl	%eax, %eax
	jle	.L926
	xorl	%eax, %eax
	jmp	.L907
	.p2align 4,,10
	.p2align 3
.L921:
	movl	(%r14), %edx
	testl	%edx, %edx
	jg	.L933
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl17blueprint_helpers23parseExponentSignOptionERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode
	movl	%eax, %r8d
	movl	$1, %eax
	testb	%r8b, %r8b
	je	.L930
	jmp	.L907
.L934:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3985:
	.size	_ZN6icu_676number4impl8skeleton11parseOptionENS2_10ParseStateERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode, .-_ZN6icu_676number4impl8skeleton11parseOptionENS2_10ParseStateERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode
	.p2align 4
	.globl	_ZN6icu_676number4impl8skeleton13parseSkeletonERKNS_13UnicodeStringERiR10UErrorCode
	.type	_ZN6icu_676number4impl8skeleton13parseSkeletonERKNS_13UnicodeStringERiR10UErrorCode, @function
_ZN6icu_676number4impl8skeleton13parseSkeletonERKNS_13UnicodeStringERiR10UErrorCode:
.LFB3980:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	leaq	-192(%rbp), %r15
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-128(%rbp), %rbx
	movq	%rbx, %rdi
	subq	$216, %rsp
	movq	%rdx, -240(%rbp)
	movq	%rcx, -216(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rbx, -248(%rbp)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	leaq	-160(%rbp), %rax
	movl	$32, %edx
	movq	%rbx, %rdi
	movl	$1, %ecx
	movq	%rax, %rsi
	movw	%dx, -160(%rbp)
	xorl	%edx, %edx
	movq	%rax, -232(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	16(%r14), %rdi
	movq	$2, 4(%r14)
	movl	$0, 12(%r14)
	movq	$0, -205(%rbp)
	movl	$0, -197(%rbp)
	movb	$0, -193(%rbp)
	call	_ZN6icu_6711MeasureUnitC1Ev@PLT
	leaq	40(%r14), %rdi
	call	_ZN6icu_6711MeasureUnitC1Ev@PLT
	movl	$-3, %ecx
	movabsq	$30064771077, %rax
	movl	$-1, %esi
	movw	%cx, 100(%r14)
	pxor	%xmm0, %xmm0
	leaq	216(%r14), %rdi
	movq	%rax, 152(%r14)
	movw	%si, 124(%r14)
	movl	$0, 64(%r14)
	movl	$4, 96(%r14)
	movl	$-2, 112(%r14)
	movb	$0, 132(%r14)
	movl	$0, 136(%r14)
	movq	$0, 144(%r14)
	movl	$2, 160(%r14)
	movl	$0, 168(%r14)
	movq	$0, 176(%r14)
	movl	$0, 184(%r14)
	movl	$3, 208(%r14)
	movups	%xmm0, 192(%r14)
	call	_ZN6icu_676LocaleC1Ev@PLT
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6713StringSegmentC1ERKNS_13UnicodeStringEb@PLT
	movq	_ZN12_GLOBAL__N_119kSerializedStemTrieE(%rip), %rax
	movq	$0, -160(%rbp)
	movl	$-1, -136(%rbp)
	movq	%rax, -152(%rbp)
	movq	%rax, -144(%rbp)
	xorl	%r12d, %r12d
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L939:
	movq	%r15, %rdi
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	cmpl	%r12d, %eax
	jle	.L940
	movl	%r12d, %esi
	movq	%r15, %rdi
	call	_ZNK6icu_6713StringSegment11codePointAtEi@PLT
	movl	%eax, %edi
	movl	%eax, %ebx
	call	_ZN6icu_6712PatternProps12isWhiteSpaceEi@PLT
	movl	%eax, %r10d
	testb	%al, %al
	jne	.L941
	cmpl	$47, %ebx
	je	.L941
	xorl	%eax, %eax
	cmpl	$65535, %ebx
	seta	%al
	leal	1(%r12,%rax), %r12d
	testl	%r13d, %r13d
	jne	.L939
	movq	-232(%rbp), %rdi
	movl	%ebx, %esi
	call	_ZN6icu_6710UCharsTrie16nextForCodePointEi@PLT
	jmp	.L939
	.p2align 4,,10
	.p2align 3
.L941:
	testl	%r12d, %r12d
	jne	.L983
	testl	%r13d, %r13d
	jne	.L982
	cmpl	$47, %ebx
	je	.L982
.L955:
	xorl	%r13d, %r13d
.L952:
	xorl	%esi, %esi
	cmpl	$65535, %ebx
	movq	%r15, %rdi
	seta	%sil
	xorl	%r12d, %r12d
	addl	$1, %esi
	call	_ZN6icu_6713StringSegment12adjustOffsetEi@PLT
	jmp	.L939
	.p2align 4,,10
	.p2align 3
.L983:
	movl	%r12d, %esi
	movq	%r15, %rdi
	movb	%r10b, -217(%rbp)
	call	_ZN6icu_6713StringSegment9setLengthEi@PLT
	testl	%r13d, %r13d
	movzbl	-217(%rbp), %r10d
	jne	.L945
	movq	-216(%rbp), %r8
	movq	-232(%rbp), %rsi
	movq	%r14, %rcx
	movq	%r15, %rdi
	leaq	-205(%rbp), %rdx
	call	_ZN6icu_676number4impl8skeleton9parseStemERKNS_13StringSegmentERKNS_10UCharsTrieERNS1_14SeenMacroPropsERNS1_10MacroPropsER10UErrorCode
	movzbl	-217(%rbp), %r10d
	movl	$-1, -136(%rbp)
	movl	%eax, %r13d
	movq	-152(%rbp), %rax
	movq	%rax, -144(%rbp)
.L946:
	movq	%r15, %rdi
	movb	%r10b, -217(%rbp)
	call	_ZN6icu_6713StringSegment11resetLengthEv@PLT
	movq	-216(%rbp), %rax
	movzbl	-217(%rbp), %r10d
	movl	(%rax), %eax
	testl	%eax, %eax
	jg	.L981
	movl	%r12d, %esi
	movq	%r15, %rdi
	movb	%r10b, -217(%rbp)
	call	_ZN6icu_6713StringSegment12adjustOffsetEi@PLT
	testl	%r13d, %r13d
	movzbl	-217(%rbp), %r10d
	jne	.L977
	cmpl	$47, %ebx
	je	.L982
	testl	%r13d, %r13d
	je	.L952
.L977:
	testb	%r10b, %r10b
	je	.L952
	subl	$3, %r13d
	cmpl	$7, %r13d
	ja	.L955
	.p2align 4,,10
	.p2align 3
.L982:
	movq	-216(%rbp), %rax
	movl	$65811, (%rax)
.L981:
	movq	%r15, %rdi
	call	_ZNK6icu_6713StringSegment9getOffsetEv@PLT
	movq	-240(%rbp), %rcx
	movl	%eax, (%rcx)
.L940:
	movq	-232(%rbp), %rdi
	call	_ZN6icu_6710UCharsTrieD1Ev@PLT
	movq	-248(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L984
	addq	$216, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L945:
	.cfi_restore_state
	movq	-216(%rbp), %rcx
	movl	%r13d, %edi
	movq	%r14, %rdx
	movq	%r15, %rsi
	movb	%r10b, -217(%rbp)
	call	_ZN6icu_676number4impl8skeleton11parseOptionENS2_10ParseStateERKNS_13StringSegmentERNS1_10MacroPropsER10UErrorCode
	movzbl	-217(%rbp), %r10d
	movl	%eax, %r13d
	jmp	.L946
.L984:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3980:
	.size	_ZN6icu_676number4impl8skeleton13parseSkeletonERKNS_13UnicodeStringERiR10UErrorCode, .-_ZN6icu_676number4impl8skeleton13parseSkeletonERKNS_13UnicodeStringERiR10UErrorCode
	.p2align 4
	.globl	_ZN6icu_676number4impl8skeleton6createERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode
	.type	_ZN6icu_676number4impl8skeleton6createERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode, @function
_ZN6icu_676number4impl8skeleton6createERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode:
.LFB3957:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$952, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L986
	movabsq	$-4294967296, %rax
	xorl	%r13d, %r13d
	movq	%rax, (%rdx)
	xorl	%eax, %eax
	movw	%r13w, 8(%rdx)
	movw	%ax, 40(%rdx)
.L986:
	movl	(%rbx), %r11d
	testl	%r11d, %r11d
	jle	.L1008
.L987:
	xorl	%eax, %eax
	movl	$56, %ecx
	movq	%r12, %rdi
	rep stosq
	leaq	16(%r12), %rdi
	movq	$2, 4(%r12)
	call	_ZN6icu_6711MeasureUnitC1Ev@PLT
	leaq	40(%r12), %rdi
	call	_ZN6icu_6711MeasureUnitC1Ev@PLT
	movl	$-3, %eax
	movl	$-1, %edx
	movl	$0, 64(%r12)
	movw	%ax, 100(%r12)
	pxor	%xmm0, %xmm0
	movabsq	$30064771077, %rax
	leaq	216(%r12), %rdi
	movl	$4, 96(%r12)
	movl	$-2, 112(%r12)
	movb	$0, 132(%r12)
	movw	%dx, 124(%r12)
	movl	$0, 136(%r12)
	movq	$0, 144(%r12)
	movq	%rax, 152(%r12)
	movl	$2, 160(%r12)
	movl	$0, 168(%r12)
	movq	$0, 176(%r12)
	movl	$0, 184(%r12)
	movl	$3, 208(%r12)
	movups	%xmm0, 192(%r12)
	call	_ZN6icu_676LocaleC1Ev@PLT
.L985:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1009
	addq	$952, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1008:
	.cfi_restore_state
	movl	_ZN12_GLOBAL__N_124gNumberSkeletonsInitOnceE(%rip), %eax
	cmpl	$2, %eax
	jne	.L1010
.L988:
	movl	4+_ZN12_GLOBAL__N_124gNumberSkeletonsInitOnceE(%rip), %eax
	testl	%eax, %eax
	jle	.L989
	movl	%eax, (%rbx)
	jmp	.L987
	.p2align 4,,10
	.p2align 3
.L1010:
	leaq	_ZN12_GLOBAL__N_124gNumberSkeletonsInitOnceE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L988
	movq	%rbx, %rdi
	call	_ZN12_GLOBAL__N_119initNumberSkeletonsER10UErrorCode
	movl	(%rbx), %eax
	leaq	_ZN12_GLOBAL__N_124gNumberSkeletonsInitOnceE(%rip), %rdi
	movl	%eax, 4+_ZN12_GLOBAL__N_124gNumberSkeletonsInitOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L989:
	movl	(%rbx), %r10d
	testl	%r10d, %r10d
	jg	.L987
	leaq	-960(%rbp), %r13
	leaq	-964(%rbp), %rdx
	movq	%rbx, %rcx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl8skeleton13parseSkeletonERKNS_13UnicodeStringERiR10UErrorCode
	movl	(%rbx), %r9d
	testl	%r9d, %r9d
	jle	.L1011
	leaq	40(%r12), %rax
	leaq	216(%r12), %r13
	movq	%rax, -984(%rbp)
	testq	%r14, %r14
	je	.L1012
	movl	-964(%rbp), %esi
	xorl	%edi, %edi
	movl	%esi, 4(%r14)
	subl	$15, %esi
	call	uprv_max_67@PLT
	movswl	8(%r15), %edi
	movl	%eax, %ebx
	movl	-964(%rbp), %eax
	leal	15(%rax), %esi
	testw	%di, %di
	js	.L993
	sarl	$5, %edi
.L994:
	call	uprv_min_67@PLT
	movl	-964(%rbp), %edx
	xorl	%r8d, %r8d
	movl	%ebx, %esi
	movl	%eax, -984(%rbp)
	leaq	8(%r14), %rax
	movq	%r15, %rdi
	movq	%rax, %rcx
	subl	%ebx, %edx
	movq	%rax, -992(%rbp)
	call	_ZNK6icu_6713UnicodeString9doExtractEiiPDsi@PLT
	movq	-992(%rbp), %rax
	movl	-964(%rbp), %esi
	xorl	%edi, %edi
	xorl	%r8d, %r8d
	movl	%esi, %eax
	subl	%ebx, %eax
	movl	-984(%rbp), %ebx
	cltq
	movw	%di, 8(%r14,%rax,2)
	movl	%ebx, %edx
	leaq	40(%r14), %rax
	movq	%r15, %rdi
	movq	%rax, %rcx
	subl	%esi, %edx
	movq	%rax, -984(%rbp)
	call	_ZNK6icu_6713UnicodeString9doExtractEiiPDsi@PLT
	movq	-984(%rbp), %rax
	subl	-964(%rbp), %ebx
	xorl	%r8d, %r8d
	movl	$56, %ecx
	xorl	%eax, %eax
	movslq	%ebx, %rbx
	movq	%r12, %rdi
	movw	%r8w, 40(%r14,%rbx,2)
.L1007:
	rep stosq
	leaq	16(%r12), %rdi
	movq	$2, 4(%r12)
	call	_ZN6icu_6711MeasureUnitC1Ev@PLT
	leaq	40(%r12), %rdi
	call	_ZN6icu_6711MeasureUnitC1Ev@PLT
	movl	$-3, %ecx
	movl	$-1, %esi
	movq	%r13, %rdi
	movabsq	$30064771077, %rax
	movw	%cx, 100(%r12)
	pxor	%xmm0, %xmm0
	movl	$0, 64(%r12)
	movl	$4, 96(%r12)
	movl	$-2, 112(%r12)
	movb	$0, 132(%r12)
	movw	%si, 124(%r12)
	movl	$0, 136(%r12)
	movq	$0, 144(%r12)
	movq	%rax, 152(%r12)
	movl	$2, 160(%r12)
	movl	$0, 168(%r12)
	movq	$0, 176(%r12)
	movl	$0, 184(%r12)
	movl	$3, 208(%r12)
	movups	%xmm0, 192(%r12)
	call	_ZN6icu_676LocaleC1Ev@PLT
.L991:
	leaq	-744(%rbp), %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	leaq	-792(%rbp), %rdi
	call	_ZN6icu_676number5ScaleD1Ev@PLT
	leaq	-824(%rbp), %rdi
	call	_ZN6icu_676number4impl14SymbolsWrapperD1Ev@PLT
	leaq	-920(%rbp), %rdi
	call	_ZN6icu_6711MeasureUnitD1Ev@PLT
	leaq	-944(%rbp), %rdi
	call	_ZN6icu_6711MeasureUnitD1Ev@PLT
	jmp	.L985
	.p2align 4,,10
	.p2align 3
.L993:
	movl	12(%r15), %edi
	jmp	.L994
	.p2align 4,,10
	.p2align 3
.L1011:
	leaq	-512(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN6icu_676number15NumberFormatter4withEv@PLT
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZNO6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE6macrosERKNS0_4impl10MacroPropsE@PLT
	leaq	-296(%rbp), %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	leaq	-344(%rbp), %rdi
	call	_ZN6icu_676number5ScaleD1Ev@PLT
	leaq	-376(%rbp), %rdi
	call	_ZN6icu_676number4impl14SymbolsWrapperD1Ev@PLT
	leaq	-472(%rbp), %rdi
	call	_ZN6icu_6711MeasureUnitD1Ev@PLT
	leaq	-496(%rbp), %rdi
	call	_ZN6icu_6711MeasureUnitD1Ev@PLT
	jmp	.L991
	.p2align 4,,10
	.p2align 3
.L1012:
	movl	$56, %ecx
	movq	%r12, %rdi
	movq	%r14, %rax
	jmp	.L1007
.L1009:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3957:
	.size	_ZN6icu_676number4impl8skeleton6createERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode, .-_ZN6icu_676number4impl8skeleton6createERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number15NumberFormatter11forSkeletonERKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_676number15NumberFormatter11forSkeletonERKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_676number15NumberFormatter11forSkeletonERKNS_13UnicodeStringER10UErrorCode:
.LFB4024:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rcx
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_676number4impl8skeleton6createERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1016
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1016:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4024:
	.size	_ZN6icu_676number15NumberFormatter11forSkeletonERKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_676number15NumberFormatter11forSkeletonERKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number15NumberFormatter11forSkeletonERKNS_13UnicodeStringER11UParseErrorR10UErrorCode
	.type	_ZN6icu_676number15NumberFormatter11forSkeletonERKNS_13UnicodeStringER11UParseErrorR10UErrorCode, @function
_ZN6icu_676number15NumberFormatter11forSkeletonERKNS_13UnicodeStringER11UParseErrorR10UErrorCode:
.LFB4025:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_676number4impl8skeleton6createERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1020
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1020:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4025:
	.size	_ZN6icu_676number15NumberFormatter11forSkeletonERKNS_13UnicodeStringER11UParseErrorR10UErrorCode, .-_ZN6icu_676number15NumberFormatter11forSkeletonERKNS_13UnicodeStringER11UParseErrorR10UErrorCode
	.p2align 4
	.globl	_ZN6icu_676number4impl17blueprint_helpers23generateIncrementOptionEdiRNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_676number4impl17blueprint_helpers23generateIncrementOptionEdiRNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_676number4impl17blueprint_helpers23generateIncrementOptionEdiRNS_13UnicodeStringER10UErrorCode:
.LFB4004:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-208(%rbp), %r14
	leaq	-128(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edi, %r12d
	movq	%r14, %rdi
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 3, -56
	movsd	%xmm0, -216(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_676number4impl15DecimalQuantityC1Ev@PLT
	movsd	-216(%rbp), %xmm0
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantity11setToDoubleEd@PLT
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantity15roundToInfinityEv@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZNK6icu_676number4impl15DecimalQuantity13toPlainStringEv@PLT
	movswl	-120(%rbp), %ecx
	testw	%cx, %cx
	js	.L1022
	sarl	$5, %ecx
.L1023:
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	testl	%r12d, %r12d
	jle	.L1024
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L1025:
	movl	$48, %esi
	movq	%r13, %rdi
	addl	$1, %ebx
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	cmpl	%ebx, %r12d
	jne	.L1025
.L1024:
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1029
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1022:
	.cfi_restore_state
	movl	-116(%rbp), %ecx
	jmp	.L1023
.L1029:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4004:
	.size	_ZN6icu_676number4impl17blueprint_helpers23generateIncrementOptionEdiRNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_676number4impl17blueprint_helpers23generateIncrementOptionEdiRNS_13UnicodeStringER10UErrorCode
	.section	.rodata.str2.8
	.align 8
.LC86:
	.string	"p"
	.string	"r"
	.string	"e"
	.string	"c"
	.string	"i"
	.string	"s"
	.string	"i"
	.string	"o"
	.string	"n"
	.string	"-"
	.string	"i"
	.string	"n"
	.string	"c"
	.string	"r"
	.string	"e"
	.string	"m"
	.string	"e"
	.string	"n"
	.string	"t"
	.string	"/"
	.string	""
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl16GeneratorHelpers9precisionERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_676number4impl16GeneratorHelpers9precisionERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_676number4impl16GeneratorHelpers9precisionERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode:
.LFB4014:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	64(%rdi), %eax
	cmpl	$1, %eax
	je	.L1056
	movq	%rdi, %rbx
	movq	%rdx, %r13
	cmpl	$2, %eax
	je	.L1057
	cmpl	$3, %eax
	je	.L1058
	cmpl	$4, %eax
	je	.L1059
	leal	-5(%rax), %edx
	cmpl	$2, %edx
	jbe	.L1060
	xorl	%r8d, %r8d
	cmpl	$8, %eax
	jne	.L1030
	movl	72(%rdi), %eax
	movl	$-1, %ecx
	xorl	%edx, %edx
	testl	%eax, %eax
	jne	.L1047
	leaq	.LC60(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC60(%rip), %rax
	movl	$1, %r8d
	.p2align 4,,10
	.p2align 3
.L1030:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1061
	addq	$16, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1056:
	.cfi_restore_state
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC59(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC59(%rip), %rax
	movl	$1, %r8d
	jmp	.L1030
	.p2align 4,,10
	.p2align 3
.L1059:
	movswl	74(%rdi), %esi
	movswl	72(%rdi), %edi
	movq	%rdx, %rcx
	movq	%r12, %rdx
	leaq	-42(%rbp), %r14
	call	_ZN6icu_676number4impl17blueprint_helpers20generateFractionStemEiiRNS_13UnicodeStringER10UErrorCode
	movl	$47, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movw	%cx, -42(%rbp)
	movq	%r12, %rdi
	movl	$1, %ecx
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movswl	76(%rbx), %r13d
	cmpw	$-1, %r13w
	je	.L1062
	testl	%r13d, %r13d
	jle	.L1044
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L1045:
	movl	$64, %esi
	movq	%r12, %rdi
	addl	$1, %ebx
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	cmpl	%ebx, %r13d
	jne	.L1045
.L1044:
	movl	$42, %edx
	movl	$1, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movw	%dx, -42(%rbp)
	xorl	%edx, %edx
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movl	$1, %r8d
	jmp	.L1030
	.p2align 4,,10
	.p2align 3
.L1057:
	movswl	74(%rdi), %esi
	movswl	72(%rdi), %edi
	movq	%rdx, %rcx
	movq	%r12, %rdx
	call	_ZN6icu_676number4impl17blueprint_helpers20generateFractionStemEiiRNS_13UnicodeStringER10UErrorCode
	movl	$1, %r8d
	jmp	.L1030
	.p2align 4,,10
	.p2align 3
.L1060:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC86(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC86(%rip), %rax
	movswl	80(%rbx), %edi
	movswl	82(%rbx), %eax
	movq	%r13, %rdx
	movq	%r12, %rsi
	movsd	72(%rbx), %xmm0
	subl	%eax, %edi
	call	_ZN6icu_676number4impl17blueprint_helpers23generateIncrementOptionEdiRNS_13UnicodeStringER10UErrorCode
	movl	$1, %r8d
	jmp	.L1030
	.p2align 4,,10
	.p2align 3
.L1058:
	movswl	76(%rdi), %r14d
	movswl	78(%rdi), %r13d
	testl	%r14d, %r14d
	jle	.L1035
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L1036:
	movl	$64, %esi
	movq	%r12, %rdi
	addl	$1, %ebx
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	cmpl	%ebx, %r14d
	jne	.L1036
.L1035:
	cmpl	$-1, %r13d
	je	.L1063
	subl	%r14d, %r13d
	xorl	%ebx, %ebx
	testl	%r13d, %r13d
	jle	.L1055
	.p2align 4,,10
	.p2align 3
.L1038:
	movl	$35, %esi
	movq	%r12, %rdi
	addl	$1, %ebx
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	cmpl	%ebx, %r13d
	jne	.L1038
.L1055:
	movl	$1, %r8d
	jmp	.L1030
	.p2align 4,,10
	.p2align 3
.L1047:
	leaq	.LC61(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC61(%rip), %rax
	jmp	.L1055
	.p2align 4,,10
	.p2align 3
.L1063:
	movl	$42, %esi
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	movw	%si, -42(%rbp)
	leaq	-42(%rbp), %rsi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movl	$1, %r8d
	jmp	.L1030
	.p2align 4,,10
	.p2align 3
.L1062:
	movswl	78(%rbx), %r13d
	movl	$64, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	cmpl	$-1, %r13d
	je	.L1044
	cmpl	$1, %r13d
	jle	.L1055
	subl	$1, %r13d
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L1043:
	movl	$35, %esi
	movq	%r12, %rdi
	addl	$1, %ebx
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	cmpl	%r13d, %ebx
	jne	.L1043
	jmp	.L1055
.L1061:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4014:
	.size	_ZN6icu_676number4impl16GeneratorHelpers9precisionERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_676number4impl16GeneratorHelpers9precisionERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode
	.p2align 4
	.globl	_ZN6icu_676number4impl17blueprint_helpers19generateScaleOptionEiPKNS1_6DecNumERNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_676number4impl17blueprint_helpers19generateScaleOptionEiPKNS1_6DecNumERNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_676number4impl17blueprint_helpers19generateScaleOptionEiPKNS1_6DecNumERNS_13UnicodeStringER10UErrorCode:
.LFB4010:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edi, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-208(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$168, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_676number4impl15DecimalQuantityC1Ev@PLT
	testq	%r13, %r13
	je	.L1065
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantity11setToDecNumERKNS1_6DecNumER10UErrorCode@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L1072
.L1066:
	movl	%r15d, %esi
	movq	%r12, %rdi
	leaq	-128(%rbp), %r13
	call	_ZN6icu_676number4impl15DecimalQuantity15adjustMagnitudeEi@PLT
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantity15roundToInfinityEv@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK6icu_676number4impl15DecimalQuantity13toPlainStringEv@PLT
	movswl	-120(%rbp), %ecx
	testw	%cx, %cx
	js	.L1068
	sarl	$5, %ecx
.L1069:
	movq	%r14, %rdi
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L1072:
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1073
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1068:
	.cfi_restore_state
	movl	-116(%rbp), %ecx
	jmp	.L1069
	.p2align 4,,10
	.p2align 3
.L1065:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantity8setToIntEi@PLT
	jmp	.L1066
.L1073:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4010:
	.size	_ZN6icu_676number4impl17blueprint_helpers19generateScaleOptionEiPKNS1_6DecNumERNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_676number4impl17blueprint_helpers19generateScaleOptionEiPKNS1_6DecNumERNS_13UnicodeStringER10UErrorCode
	.section	.rodata.str2.2
	.align 2
.LC87:
	.string	"s"
	.string	"c"
	.string	"a"
	.string	"l"
	.string	"e"
	.string	"/"
	.string	""
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl16GeneratorHelpers5scaleERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_676number4impl16GeneratorHelpers5scaleERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_676number4impl16GeneratorHelpers5scaleERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode:
.LFB4022:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	168(%rdi), %eax
	testl	%eax, %eax
	jne	.L1075
	xorl	%eax, %eax
	cmpq	$0, 176(%rdi)
	je	.L1074
.L1075:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC87(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC87(%rip), %rax
	movq	176(%rbx), %rsi
	movl	168(%rbx), %edi
	movq	%r13, %rcx
	movq	%r12, %rdx
	call	_ZN6icu_676number4impl17blueprint_helpers19generateScaleOptionEiPKNS1_6DecNumERNS_13UnicodeStringER10UErrorCode
	movl	$1, %eax
.L1074:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4022:
	.size	_ZN6icu_676number4impl16GeneratorHelpers5scaleERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_676number4impl16GeneratorHelpers5scaleERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.type	_ZN6icu_676number4impl16GeneratorHelpers16generateSkeletonERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode.part.0, @function
_ZN6icu_676number4impl16GeneratorHelpers16generateSkeletonERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode.part.0:
.LFB5460:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_676number4impl16GeneratorHelpers4unitERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode
	testb	%al, %al
	jne	.L1136
.L1080:
	movl	(%rbx), %esi
	testl	%esi, %esi
	jle	.L1137
.L1079:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1138
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1137:
	.cfi_restore_state
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl16GeneratorHelpers7perUnitERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode
	testb	%al, %al
	jne	.L1139
.L1083:
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L1079
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl16GeneratorHelpers9precisionERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode
	testb	%al, %al
	jne	.L1140
.L1085:
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L1079
	movl	96(%r13), %edi
	cmpl	$4, %edi
	je	.L1089
	movq	%r12, %rsi
	call	_ZN6icu_676number4impl19enum_to_stem_string12roundingModeE25UNumberFormatRoundingModeRNS_13UnicodeStringE
	movl	$32, %eax
	leaq	-42(%rbp), %rsi
	xorl	%edx, %edx
	movl	$1, %ecx
	movq	%r12, %rdi
	movw	%ax, -42(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L1079
.L1089:
	cmpw	$-3, 100(%r13)
	je	.L1087
	movl	108(%r13), %edi
	cmpl	$5, %edi
	je	.L1102
	cmpl	$2, %edi
	je	.L1087
	movq	%r12, %rsi
	call	_ZN6icu_676number4impl19enum_to_stem_string16groupingStrategyE23UNumberGroupingStrategyRNS_13UnicodeStringE
	movl	$32, %r11d
	xorl	%edx, %edx
	movq	%r12, %rdi
	leaq	-42(%rbp), %rsi
	movl	$1, %ecx
	movw	%r11w, -42(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L1079
	.p2align 4,,10
	.p2align 3
.L1087:
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl16GeneratorHelpers12integerWidthERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode
	testb	%al, %al
	jne	.L1141
.L1091:
	movl	(%rbx), %r9d
	testl	%r9d, %r9d
	jg	.L1079
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl16GeneratorHelpers7symbolsERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode
	testb	%al, %al
	jne	.L1142
.L1093:
	movl	(%rbx), %edi
	testl	%edi, %edi
	jg	.L1079
	movl	152(%r13), %edi
	movl	%edi, %eax
	andl	$-5, %eax
	cmpl	$1, %eax
	je	.L1097
	movq	%r12, %rsi
	call	_ZN6icu_676number4impl19enum_to_stem_string9unitWidthE16UNumberUnitWidthRNS_13UnicodeStringE
	movl	$32, %ecx
	leaq	-42(%rbp), %rsi
	xorl	%edx, %edx
	movw	%cx, -42(%rbp)
	movq	%r12, %rdi
	movl	$1, %ecx
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movl	(%rbx), %esi
	testl	%esi, %esi
	jg	.L1079
.L1097:
	movl	156(%r13), %edi
	testl	%edi, %edi
	je	.L1095
	cmpl	$7, %edi
	je	.L1095
	movq	%r12, %rsi
	call	_ZN6icu_676number4impl19enum_to_stem_string11signDisplayE18UNumberSignDisplayRNS_13UnicodeStringE
	movl	$32, %eax
	xorl	%edx, %edx
	leaq	-42(%rbp), %rsi
	movl	$1, %ecx
	movq	%r12, %rdi
	movw	%ax, -42(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L1079
.L1095:
	movl	160(%r13), %edi
	testl	$-3, %edi
	jne	.L1143
.L1098:
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl16GeneratorHelpers5scaleERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode
	testb	%al, %al
	jne	.L1144
.L1101:
	cmpl	$0, (%rbx)
	jg	.L1079
	cmpl	$-2, 112(%r13)
	jne	.L1102
	cmpq	$0, 192(%r13)
	je	.L1145
.L1102:
	movl	$16, (%rbx)
	jmp	.L1079
	.p2align 4,,10
	.p2align 3
.L1136:
	movl	$32, %edi
	leaq	-42(%rbp), %rsi
	movl	$1, %ecx
	xorl	%edx, %edx
	movw	%di, -42(%rbp)
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	jmp	.L1080
	.p2align 4,,10
	.p2align 3
.L1139:
	movl	$32, %ecx
	leaq	-42(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movw	%cx, -42(%rbp)
	movl	$1, %ecx
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	jmp	.L1083
	.p2align 4,,10
	.p2align 3
.L1140:
	movl	$32, %eax
	leaq	-42(%rbp), %rsi
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	movw	%ax, -42(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	jmp	.L1085
.L1141:
	movl	$32, %r10d
	leaq	-42(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	$1, %ecx
	movw	%r10w, -42(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	jmp	.L1091
.L1142:
	movl	$32, %r8d
	leaq	-42(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	$1, %ecx
	movw	%r8w, -42(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	jmp	.L1093
.L1143:
	movq	%r12, %rsi
	call	_ZN6icu_676number4impl19enum_to_stem_string23decimalSeparatorDisplayE30UNumberDecimalSeparatorDisplayRNS_13UnicodeStringE
	xorl	%edx, %edx
	leaq	-42(%rbp), %rsi
	movl	$1, %ecx
	movq	%r12, %rdi
	movw	$32, -42(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	cmpl	$0, (%rbx)
	jg	.L1079
	jmp	.L1098
.L1138:
	call	__stack_chk_fail@PLT
.L1144:
	leaq	-42(%rbp), %rsi
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	movw	$32, -42(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	jmp	.L1101
.L1145:
	cmpq	$0, 200(%r13)
	jne	.L1102
	movzwl	8(%r12), %eax
	testw	%ax, %ax
	js	.L1103
	movswl	%ax, %edx
	sarl	$5, %edx
.L1104:
	testl	%edx, %edx
	jle	.L1079
	leal	-1(%rdx), %ecx
	cmpl	$1, %edx
	jne	.L1105
	testb	$1, %al
	jne	.L1146
.L1105:
	cmpl	%ecx, %edx
	jbe	.L1079
	cmpl	$1023, %ecx
	jg	.L1106
	andl	$31, %eax
	sall	$5, %ecx
	orl	%ecx, %eax
	movw	%ax, 8(%r12)
	jmp	.L1079
.L1103:
	movl	12(%r12), %edx
	jmp	.L1104
.L1146:
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	jmp	.L1079
.L1106:
	orl	$-32, %eax
	movl	%ecx, 12(%r12)
	movw	%ax, 8(%r12)
	jmp	.L1079
	.cfi_endproc
.LFE5460:
	.size	_ZN6icu_676number4impl16GeneratorHelpers16generateSkeletonERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode.part.0, .-_ZN6icu_676number4impl16GeneratorHelpers16generateSkeletonERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl16GeneratorHelpers16generateSkeletonERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_676number4impl16GeneratorHelpers16generateSkeletonERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_676number4impl16GeneratorHelpers16generateSkeletonERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode:
.LFB3986:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rdx), %r10d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r10d, %r10d
	jle	.L1166
.L1147:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1167
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1166:
	.cfi_restore_state
	movl	4(%rdi), %eax
	movq	%rdi, %r14
	movq	%rsi, %r12
	movq	%rdx, %r13
	cmpl	$1, %eax
	je	.L1168
	testl	%eax, %eax
	jne	.L1162
	xorl	%edx, %edx
	cmpb	$3, 8(%rdi)
	movl	$-1, %ecx
	je	.L1169
	leaq	.LC39(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC39(%rip), %rax
.L1156:
	cmpw	$1, 10(%r14)
	jle	.L1170
	movl	$47, %edi
	leaq	-58(%rbp), %r15
	xorl	%edx, %edx
	movl	$1, %ecx
	movw	%di, -58(%rbp)
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movswl	10(%r14), %eax
	xorl	%edx, %edx
	movq	%r15, %rsi
	movl	$42, %r8d
	movl	$1, %ecx
	movq	%r12, %rdi
	movl	%eax, %ebx
	movl	%eax, -68(%rbp)
	movw	%r8w, -58(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	testl	%ebx, %ebx
	jle	.L1159
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L1160:
	movl	$101, %esi
	movq	%r12, %rdi
	addl	$1, %ebx
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	cmpl	%ebx, -68(%rbp)
	jne	.L1160
.L1159:
	movl	0(%r13), %esi
	testl	%esi, %esi
	jg	.L1147
.L1161:
	movl	12(%r14), %r9d
	testl	%r9d, %r9d
	jne	.L1171
.L1151:
	movl	$32, %eax
	xorl	%edx, %edx
	movl	$1, %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movw	%ax, -58(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movl	0(%r13), %edx
	testl	%edx, %edx
	jg	.L1147
.L1162:
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl16GeneratorHelpers16generateSkeletonERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode.part.0
	jmp	.L1147
	.p2align 4,,10
	.p2align 3
.L1168:
	movl	8(%rdi), %eax
	cmpl	$1, %eax
	je	.L1172
	testl	%eax, %eax
	je	.L1173
	movl	$16, (%rdx)
	jmp	.L1147
	.p2align 4,,10
	.p2align 3
.L1170:
	leaq	-58(%rbp), %r15
	jmp	.L1161
	.p2align 4,,10
	.p2align 3
.L1169:
	leaq	.LC38(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC38(%rip), %rax
	jmp	.L1156
	.p2align 4,,10
	.p2align 3
.L1173:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC37(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC37(%rip), %rax
	leaq	-58(%rbp), %r15
	jmp	.L1151
	.p2align 4,,10
	.p2align 3
.L1171:
	movl	$47, %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	xorl	%edx, %edx
	movw	%cx, -58(%rbp)
	movl	$1, %ecx
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movl	12(%r14), %edi
	movq	%r12, %rsi
	call	_ZN6icu_676number4impl19enum_to_stem_string11signDisplayE18UNumberSignDisplayRNS_13UnicodeStringE
	jmp	.L1151
	.p2align 4,,10
	.p2align 3
.L1172:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC36(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC36(%rip), %rax
	leaq	-58(%rbp), %r15
	jmp	.L1151
.L1167:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3986:
	.size	_ZN6icu_676number4impl16GeneratorHelpers16generateSkeletonERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_676number4impl16GeneratorHelpers16generateSkeletonERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode
	.section	.text._ZNK6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE10toSkeletonER10UErrorCode,"axG",@progbits,_ZNK6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE10toSkeletonER10UErrorCode,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE10toSkeletonER10UErrorCode
	.type	_ZNK6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE10toSkeletonER10UErrorCode, @function
_ZNK6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE10toSkeletonER10UErrorCode:
.LFB4625:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$16, %rsp
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jle	.L1175
.L1180:
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %edx
	movq	%r12, %rdi
	movq	%rax, (%r12)
	movw	%dx, 8(%r12)
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1175:
	.cfi_restore_state
	cmpl	$3, 4(%rsi)
	movq	%rsi, %r13
	je	.L1177
	cmpl	$9, 64(%rsi)
	je	.L1193
	cmpl	$-3, 112(%rsi)
	je	.L1194
	cmpb	$0, 132(%rsi)
	jne	.L1195
	movl	136(%rsi), %eax
	subl	$1, %eax
	cmpl	$1, %eax
	jbe	.L1196
.L1183:
	movl	184(%r13), %eax
	testl	%eax, %eax
	jne	.L1184
	movl	_ZN12_GLOBAL__N_124gNumberSkeletonsInitOnceE(%rip), %eax
	cmpl	$2, %eax
	je	.L1186
	leaq	_ZN12_GLOBAL__N_124gNumberSkeletonsInitOnceE(%rip), %rdi
	movq	%rdx, -24(%rbp)
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	movq	-24(%rbp), %rdx
	testb	%al, %al
	jne	.L1197
	.p2align 4,,10
	.p2align 3
.L1186:
	movl	4+_ZN12_GLOBAL__N_124gNumberSkeletonsInitOnceE(%rip), %eax
	testl	%eax, %eax
	jle	.L1187
	movl	%eax, (%rdx)
.L1187:
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, (%r12)
	movl	$2, %eax
	movw	%ax, 8(%r12)
	call	_ZN6icu_676number4impl16GeneratorHelpers16generateSkeletonERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1195:
	.cfi_restore_state
	movl	124(%rsi), %eax
	movl	%eax, (%rdx)
	jmp	.L1180
	.p2align 4,,10
	.p2align 3
.L1196:
	cmpq	$0, 144(%rsi)
	jne	.L1183
	movl	$7, (%rdx)
	jmp	.L1180
	.p2align 4,,10
	.p2align 3
.L1177:
	movl	8(%rsi), %eax
	movl	%eax, (%rdx)
	jmp	.L1180
	.p2align 4,,10
	.p2align 3
.L1193:
	movl	72(%rsi), %eax
	movl	%eax, (%rdx)
	jmp	.L1180
	.p2align 4,,10
	.p2align 3
.L1194:
	movl	116(%rsi), %eax
	movl	%eax, (%rdx)
	jmp	.L1180
	.p2align 4,,10
	.p2align 3
.L1197:
	movq	%rdx, %rdi
	call	_ZN12_GLOBAL__N_119initNumberSkeletonsER10UErrorCode
	movq	-24(%rbp), %rdx
	leaq	_ZN12_GLOBAL__N_124gNumberSkeletonsInitOnceE(%rip), %rdi
	movl	(%rdx), %eax
	movl	%eax, 4+_ZN12_GLOBAL__N_124gNumberSkeletonsInitOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
	movq	-24(%rbp), %rdx
	jmp	.L1187
	.p2align 4,,10
	.p2align 3
.L1184:
	movl	%eax, (%rdx)
	jmp	.L1180
	.cfi_endproc
.LFE4625:
	.size	_ZNK6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE10toSkeletonER10UErrorCode, .-_ZNK6icu_676number23NumberFormatterSettingsINS0_26UnlocalizedNumberFormatterEE10toSkeletonER10UErrorCode
	.section	.text._ZNK6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE10toSkeletonER10UErrorCode,"axG",@progbits,_ZNK6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE10toSkeletonER10UErrorCode,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE10toSkeletonER10UErrorCode
	.type	_ZNK6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE10toSkeletonER10UErrorCode, @function
_ZNK6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE10toSkeletonER10UErrorCode:
.LFB4627:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$16, %rsp
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jle	.L1199
.L1204:
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %edx
	movq	%r12, %rdi
	movq	%rax, (%r12)
	movw	%dx, 8(%r12)
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1199:
	.cfi_restore_state
	cmpl	$3, 4(%rsi)
	movq	%rsi, %r13
	je	.L1201
	cmpl	$9, 64(%rsi)
	je	.L1217
	cmpl	$-3, 112(%rsi)
	je	.L1218
	cmpb	$0, 132(%rsi)
	jne	.L1219
	movl	136(%rsi), %eax
	subl	$1, %eax
	cmpl	$1, %eax
	jbe	.L1220
.L1207:
	movl	184(%r13), %eax
	testl	%eax, %eax
	jne	.L1208
	movl	_ZN12_GLOBAL__N_124gNumberSkeletonsInitOnceE(%rip), %eax
	cmpl	$2, %eax
	je	.L1210
	leaq	_ZN12_GLOBAL__N_124gNumberSkeletonsInitOnceE(%rip), %rdi
	movq	%rdx, -24(%rbp)
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	movq	-24(%rbp), %rdx
	testb	%al, %al
	jne	.L1221
	.p2align 4,,10
	.p2align 3
.L1210:
	movl	4+_ZN12_GLOBAL__N_124gNumberSkeletonsInitOnceE(%rip), %eax
	testl	%eax, %eax
	jle	.L1211
	movl	%eax, (%rdx)
.L1211:
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, (%r12)
	movl	$2, %eax
	movw	%ax, 8(%r12)
	call	_ZN6icu_676number4impl16GeneratorHelpers16generateSkeletonERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1219:
	.cfi_restore_state
	movl	124(%rsi), %eax
	movl	%eax, (%rdx)
	jmp	.L1204
	.p2align 4,,10
	.p2align 3
.L1220:
	cmpq	$0, 144(%rsi)
	jne	.L1207
	movl	$7, (%rdx)
	jmp	.L1204
	.p2align 4,,10
	.p2align 3
.L1201:
	movl	8(%rsi), %eax
	movl	%eax, (%rdx)
	jmp	.L1204
	.p2align 4,,10
	.p2align 3
.L1217:
	movl	72(%rsi), %eax
	movl	%eax, (%rdx)
	jmp	.L1204
	.p2align 4,,10
	.p2align 3
.L1218:
	movl	116(%rsi), %eax
	movl	%eax, (%rdx)
	jmp	.L1204
	.p2align 4,,10
	.p2align 3
.L1221:
	movq	%rdx, %rdi
	call	_ZN12_GLOBAL__N_119initNumberSkeletonsER10UErrorCode
	movq	-24(%rbp), %rdx
	leaq	_ZN12_GLOBAL__N_124gNumberSkeletonsInitOnceE(%rip), %rdi
	movl	(%rdx), %eax
	movl	%eax, 4+_ZN12_GLOBAL__N_124gNumberSkeletonsInitOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
	movq	-24(%rbp), %rdx
	jmp	.L1211
	.p2align 4,,10
	.p2align 3
.L1208:
	movl	%eax, (%rdx)
	jmp	.L1204
	.cfi_endproc
.LFE4627:
	.size	_ZNK6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE10toSkeletonER10UErrorCode, .-_ZNK6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE10toSkeletonER10UErrorCode
	.text
	.p2align 4
	.globl	_ZN6icu_676number4impl8skeleton8generateERKNS1_10MacroPropsER10UErrorCode
	.type	_ZN6icu_676number4impl8skeleton8generateERKNS1_10MacroPropsER10UErrorCode, @function
_ZN6icu_676number4impl8skeleton8generateERKNS1_10MacroPropsER10UErrorCode:
.LFB3979:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$24, %rsp
	movl	(%rdx), %edi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%edi, %edi
	jle	.L1242
.L1223:
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, (%r12)
	movl	$2, %eax
	movw	%ax, 8(%r12)
.L1222:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1243
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1242:
	.cfi_restore_state
	movl	_ZN12_GLOBAL__N_124gNumberSkeletonsInitOnceE(%rip), %eax
	movq	%rsi, %r14
	movq	%rdx, %r13
	cmpl	$2, %eax
	jne	.L1244
.L1224:
	movl	4+_ZN12_GLOBAL__N_124gNumberSkeletonsInitOnceE(%rip), %eax
	testl	%eax, %eax
	jle	.L1245
	movl	%eax, 0(%r13)
	jmp	.L1223
	.p2align 4,,10
	.p2align 3
.L1245:
	movl	0(%r13), %eax
.L1225:
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rcx
	movl	$2, %esi
	movq	%rcx, (%r12)
	movw	%si, 8(%r12)
	testl	%eax, %eax
	jg	.L1222
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl16GeneratorHelpers8notationERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode
	testb	%al, %al
	jne	.L1246
.L1229:
	movl	0(%r13), %edx
	testl	%edx, %edx
	jg	.L1222
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl16GeneratorHelpers16generateSkeletonERKNS1_10MacroPropsERNS_13UnicodeStringER10UErrorCode.part.0
	jmp	.L1222
	.p2align 4,,10
	.p2align 3
.L1244:
	leaq	_ZN12_GLOBAL__N_124gNumberSkeletonsInitOnceE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L1224
	movq	%r13, %rdi
	call	_ZN12_GLOBAL__N_119initNumberSkeletonsER10UErrorCode
	movl	0(%r13), %eax
	leaq	_ZN12_GLOBAL__N_124gNumberSkeletonsInitOnceE(%rip), %rdi
	movl	%eax, 4+_ZN12_GLOBAL__N_124gNumberSkeletonsInitOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
	movl	0(%r13), %eax
	jmp	.L1225
	.p2align 4,,10
	.p2align 3
.L1246:
	movl	$32, %ecx
	leaq	-42(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movw	%cx, -42(%rbp)
	movl	$1, %ecx
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	jmp	.L1229
.L1243:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3979:
	.size	_ZN6icu_676number4impl8skeleton8generateERKNS1_10MacroPropsER10UErrorCode, .-_ZN6icu_676number4impl8skeleton8generateERKNS1_10MacroPropsER10UErrorCode
	.local	_ZN12_GLOBAL__N_119kSerializedStemTrieE
	.comm	_ZN12_GLOBAL__N_119kSerializedStemTrieE,8,8
	.local	_ZN12_GLOBAL__N_124gNumberSkeletonsInitOnceE
	.comm	_ZN12_GLOBAL__N_124gNumberSkeletonsInitOnceE,8,8
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
