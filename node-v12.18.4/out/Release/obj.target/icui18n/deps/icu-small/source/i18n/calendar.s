	.file	"calendar.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Calendar19handleComputeFieldsEiR10UErrorCode
	.type	_ZN6icu_678Calendar19handleComputeFieldsEiR10UErrorCode, @function
_ZN6icu_678Calendar19handleComputeFieldsEiR10UErrorCode:
.LFB3283:
	.cfi_startproc
	endbr64
	movl	284(%rdi), %eax
	movl	$257, %edx
	movb	$1, 106(%rdi)
	movw	%dx, 109(%rdi)
	movl	$1, %edx
	movl	%eax, 20(%rdi)
	movl	292(%rdi), %eax
	movl	$1, 136(%rdi)
	movl	%eax, 32(%rdi)
	movl	288(%rdi), %eax
	movl	$1, 204(%rdi)
	movl	%eax, 36(%rdi)
	movabsq	$4294967297, %rax
	movq	%rax, 148(%rdi)
	movl	280(%rdi), %eax
	movb	$1, 123(%rdi)
	movl	%eax, 88(%rdi)
	testl	%eax, %eax
	jg	.L2
	subl	%eax, %edx
	movl	%edx, %eax
	xorl	%edx, %edx
.L2:
	movl	%eax, 16(%rdi)
	movabsq	$4294967297, %rax
	movq	%rax, 128(%rdi)
	movl	$257, %eax
	movl	%edx, 12(%rdi)
	movw	%ax, 104(%rdi)
	ret
	.cfi_endproc
.LFE3283:
	.size	_ZN6icu_678Calendar19handleComputeFieldsEiR10UErrorCode, .-_ZN6icu_678Calendar19handleComputeFieldsEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678Calendar23getFieldResolutionTableEv
	.type	_ZNK6icu_678Calendar23getFieldResolutionTableEv, @function
_ZNK6icu_678Calendar23getFieldResolutionTableEv:
.LFB3323:
	.cfi_startproc
	endbr64
	leaq	_ZN6icu_678Calendar15kDatePrecedenceE(%rip), %rax
	ret
	.cfi_endproc
.LFE3323:
	.size	_ZNK6icu_678Calendar23getFieldResolutionTableEv, .-_ZNK6icu_678Calendar23getFieldResolutionTableEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Calendar21getDefaultMonthInYearEi
	.type	_ZN6icu_678Calendar21getDefaultMonthInYearEi, @function
_ZN6icu_678Calendar21getDefaultMonthInYearEi:
.LFB3332:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3332:
	.size	_ZN6icu_678Calendar21getDefaultMonthInYearEi, .-_ZN6icu_678Calendar21getDefaultMonthInYearEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Calendar20getDefaultDayInMonthEii
	.type	_ZN6icu_678Calendar20getDefaultDayInMonthEii, @function
_ZN6icu_678Calendar20getDefaultDayInMonthEii:
.LFB3333:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3333:
	.size	_ZN6icu_678Calendar20getDefaultDayInMonthEii, .-_ZN6icu_678Calendar20getDefaultDayInMonthEii
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678Calendar20handleGetMonthLengthEii
	.type	_ZNK6icu_678Calendar20handleGetMonthLengthEii, @function
_ZNK6icu_678Calendar20handleGetMonthLengthEii:
.LFB3336:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%edx, %r14d
	leal	1(%rdx), %edx
	pushq	%r13
	.cfi_offset 13, -32
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rdi), %rax
	call	*264(%rax)
	movl	%r14d, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	movl	%eax, %ebx
	movq	(%r12), %rax
	movl	$1, %ecx
	call	*264(%rax)
	subl	%eax, %ebx
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3336:
	.size	_ZNK6icu_678Calendar20handleGetMonthLengthEii, .-_ZNK6icu_678Calendar20handleGetMonthLengthEii
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678Calendar19handleGetYearLengthEi
	.type	_ZNK6icu_678Calendar19handleGetYearLengthEi, @function
_ZNK6icu_678Calendar19handleGetYearLengthEi:
.LFB3337:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	leal	1(%rsi), %esi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	(%rdi), %rax
	call	*264(%rax)
	movl	%r13d, %esi
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	movl	%eax, %ebx
	movq	(%r12), %rax
	xorl	%edx, %edx
	call	*264(%rax)
	addq	$8, %rsp
	subl	%eax, %ebx
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3337:
	.size	_ZNK6icu_678Calendar19handleGetYearLengthEi, .-_ZNK6icu_678Calendar19handleGetYearLengthEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714SharedCalendarD2Ev
	.type	_ZN6icu_6714SharedCalendarD2Ev, @function
_ZN6icu_6714SharedCalendarD2Ev:
.LFB3219:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6714SharedCalendarE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L13
	movq	(%rdi), %rax
	call	*8(%rax)
.L13:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6712SharedObjectD2Ev@PLT
	.cfi_endproc
.LFE3219:
	.size	_ZN6icu_6714SharedCalendarD2Ev, .-_ZN6icu_6714SharedCalendarD2Ev
	.globl	_ZN6icu_6714SharedCalendarD1Ev
	.set	_ZN6icu_6714SharedCalendarD1Ev,_ZN6icu_6714SharedCalendarD2Ev
	.section	.text._ZNK6icu_678CacheKeyINS_14SharedCalendarEE16writeDescriptionEPci,"axG",@progbits,_ZNK6icu_678CacheKeyINS_14SharedCalendarEE16writeDescriptionEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_678CacheKeyINS_14SharedCalendarEE16writeDescriptionEPci
	.type	_ZNK6icu_678CacheKeyINS_14SharedCalendarEE16writeDescriptionEPci, @function
_ZNK6icu_678CacheKeyINS_14SharedCalendarEE16writeDescriptionEPci:
.LFB4317:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	leaq	_ZTSN6icu_6714SharedCalendarE(%rip), %rsi
	movq	%r8, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movslq	%edx, %rbx
	movq	%rbx, %rdx
	subq	$8, %rsp
	call	strncpy@PLT
	movb	$0, -1(%rax,%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4317:
	.size	_ZNK6icu_678CacheKeyINS_14SharedCalendarEE16writeDescriptionEPci, .-_ZNK6icu_678CacheKeyINS_14SharedCalendarEE16writeDescriptionEPci
	.section	.text._ZNK6icu_6714LocaleCacheKeyINS_14SharedCalendarEE16writeDescriptionEPci,"axG",@progbits,_ZNK6icu_6714LocaleCacheKeyINS_14SharedCalendarEE16writeDescriptionEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6714LocaleCacheKeyINS_14SharedCalendarEE16writeDescriptionEPci
	.type	_ZNK6icu_6714LocaleCacheKeyINS_14SharedCalendarEE16writeDescriptionEPci, @function
_ZNK6icu_6714LocaleCacheKeyINS_14SharedCalendarEE16writeDescriptionEPci:
.LFB4314:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movslq	%edx, %rbx
	movq	%rbx, %rdx
	subq	$8, %rsp
	movq	56(%rdi), %rsi
	movq	%r8, %rdi
	call	strncpy@PLT
	movb	$0, -1(%rax,%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4314:
	.size	_ZNK6icu_6714LocaleCacheKeyINS_14SharedCalendarEE16writeDescriptionEPci, .-_ZNK6icu_6714LocaleCacheKeyINS_14SharedCalendarEE16writeDescriptionEPci
	.section	.text._ZNK6icu_6714LocaleCacheKeyINS_14SharedCalendarEE5cloneEv,"axG",@progbits,_ZNK6icu_6714LocaleCacheKeyINS_14SharedCalendarEE5cloneEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6714LocaleCacheKeyINS_14SharedCalendarEE5cloneEv
	.type	_ZNK6icu_6714LocaleCacheKeyINS_14SharedCalendarEE5cloneEv, @function
_ZNK6icu_6714LocaleCacheKeyINS_14SharedCalendarEE5cloneEv:
.LFB4312:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	$240, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L22
	movl	8(%rbx), %eax
	movb	$0, 12(%r12)
	leaq	16(%rbx), %rsi
	leaq	16(%r12), %rdi
	movl	%eax, 8(%r12)
	leaq	16+_ZTVN6icu_6714LocaleCacheKeyINS_14SharedCalendarEEE(%rip), %rax
	movq	%rax, (%r12)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
.L22:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4312:
	.size	_ZNK6icu_6714LocaleCacheKeyINS_14SharedCalendarEE5cloneEv, .-_ZNK6icu_6714LocaleCacheKeyINS_14SharedCalendarEE5cloneEv
	.section	.text._ZNK6icu_678CacheKeyINS_14SharedCalendarEEeqERKNS_12CacheKeyBaseE,"axG",@progbits,_ZNK6icu_678CacheKeyINS_14SharedCalendarEEeqERKNS_12CacheKeyBaseE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_678CacheKeyINS_14SharedCalendarEEeqERKNS_12CacheKeyBaseE
	.type	_ZNK6icu_678CacheKeyINS_14SharedCalendarEEeqERKNS_12CacheKeyBaseE, @function
_ZNK6icu_678CacheKeyINS_14SharedCalendarEEeqERKNS_12CacheKeyBaseE:
.LFB4316:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	-8(%rax), %rax
	movq	8(%rax), %rdi
	movq	(%rsi), %rax
	movq	-8(%rax), %rax
	movq	8(%rax), %rsi
	movl	$1, %eax
	cmpq	%rsi, %rdi
	je	.L33
	xorl	%eax, %eax
	cmpb	$42, (%rdi)
	je	.L33
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	strcmp@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE4316:
	.size	_ZNK6icu_678CacheKeyINS_14SharedCalendarEEeqERKNS_12CacheKeyBaseE, .-_ZNK6icu_678CacheKeyINS_14SharedCalendarEEeqERKNS_12CacheKeyBaseE
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678Calendar14isEquivalentToERKS0_
	.type	_ZNK6icu_678Calendar14isEquivalentToERKS0_, @function
_ZNK6icu_678Calendar14isEquivalentToERKS0_:
.LFB3253:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rax
	movq	-8(%rax), %rax
	movq	8(%rax), %rdi
	movq	(%rsi), %rax
	movq	-8(%rax), %rax
	movq	8(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L37
	xorl	%r12d, %r12d
	cmpb	$42, (%rdi)
	je	.L36
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L36
.L37:
	movzbl	232(%rbx), %eax
	xorl	%r12d, %r12d
	cmpb	%al, 232(%r13)
	je	.L47
.L36:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	.cfi_restore_state
	movq	248(%rbx), %rax
	cmpq	%rax, 248(%r13)
	jne	.L36
	movq	256(%r13), %rax
	movabsq	$1099511627775, %rdx
	xorq	256(%rbx), %rax
	testq	%rdx, %rax
	jne	.L36
	movq	264(%rbx), %rax
	cmpq	%rax, 264(%r13)
	jne	.L36
	movq	272(%rbx), %rax
	cmpq	%rax, 272(%r13)
	jne	.L36
	movq	240(%r13), %rdi
	movq	240(%rbx), %rsi
	movq	(%rdi), %rax
	call	*24(%rax)
	testb	%al, %al
	setne	%r12b
	jmp	.L36
	.cfi_endproc
.LFE3253:
	.size	_ZNK6icu_678Calendar14isEquivalentToERKS0_, .-_ZNK6icu_678Calendar14isEquivalentToERKS0_
	.section	.text._ZNK6icu_678CacheKeyINS_14SharedCalendarEE8hashCodeEv,"axG",@progbits,_ZNK6icu_678CacheKeyINS_14SharedCalendarEE8hashCodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_678CacheKeyINS_14SharedCalendarEE8hashCodeEv
	.type	_ZNK6icu_678CacheKeyINS_14SharedCalendarEE8hashCodeEv, @function
_ZNK6icu_678CacheKeyINS_14SharedCalendarEE8hashCodeEv:
.LFB4315:
	.cfi_startproc
	endbr64
	movl	$25, %esi
	leaq	_ZTSN6icu_6714SharedCalendarE(%rip), %rdi
	jmp	ustr_hashCharsN_67@PLT
	.cfi_endproc
.LFE4315:
	.size	_ZNK6icu_678CacheKeyINS_14SharedCalendarEE8hashCodeEv, .-_ZNK6icu_678CacheKeyINS_14SharedCalendarEE8hashCodeEv
	.section	.text._ZNK6icu_6714LocaleCacheKeyINS_14SharedCalendarEE8hashCodeEv,"axG",@progbits,_ZNK6icu_6714LocaleCacheKeyINS_14SharedCalendarEE8hashCodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6714LocaleCacheKeyINS_14SharedCalendarEE8hashCodeEv
	.type	_ZNK6icu_6714LocaleCacheKeyINS_14SharedCalendarEE8hashCodeEv, @function
_ZNK6icu_6714LocaleCacheKeyINS_14SharedCalendarEE8hashCodeEv:
.LFB4311:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$25, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	_ZTSN6icu_6714SharedCalendarE(%rip), %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	call	ustr_hashCharsN_67@PLT
	leaq	16(%r12), %rdi
	movl	%eax, %ebx
	call	_ZNK6icu_676Locale8hashCodeEv@PLT
	movl	%eax, %r8d
	leal	(%rbx,%rbx,8), %eax
	leal	(%rbx,%rax,4), %eax
	popq	%rbx
	popq	%r12
	addl	%r8d, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4311:
	.size	_ZNK6icu_6714LocaleCacheKeyINS_14SharedCalendarEE8hashCodeEv, .-_ZNK6icu_6714LocaleCacheKeyINS_14SharedCalendarEE8hashCodeEv
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678Calendar16getDayOfWeekTypeE19UCalendarDaysOfWeekR10UErrorCode
	.type	_ZNK6icu_678Calendar16getDayOfWeekTypeE19UCalendarDaysOfWeekR10UErrorCode, @function
_ZNK6icu_678Calendar16getDayOfWeekTypeE19UCalendarDaysOfWeekR10UErrorCode:
.LFB3305:
	.cfi_startproc
	endbr64
	movl	(%rdx), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L51
	leal	-1(%rsi), %ecx
	cmpl	$6, %ecx
	ja	.L69
	movl	264(%rdi), %edx
	movl	272(%rdi), %ecx
	cmpl	%ecx, %edx
	je	.L70
	cmpl	%esi, %edx
	setg	%r9b
	cmpl	%esi, %ecx
	setl	%r8b
	cmpl	%ecx, %edx
	jl	.L71
	testb	%r8b, %r8b
	je	.L56
	testb	%r9b, %r9b
	jne	.L51
.L56:
	cmpl	%edx, %esi
	je	.L58
	movl	$1, %eax
	cmpl	%ecx, %esi
	je	.L72
.L51:
	ret
	.p2align 4,,10
	.p2align 3
.L69:
	movl	$1, (%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L71:
	testb	%r8b, %r8b
	jne	.L62
	testb	%r9b, %r9b
	je	.L56
.L62:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L70:
	cmpl	%edx, %esi
	jne	.L51
.L58:
	movl	268(%rdi), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	setne	%al
	addl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L72:
	xorl	%eax, %eax
	cmpl	$86399999, 276(%rdi)
	setle	%al
	leal	1(%rax,%rax), %eax
	ret
	.cfi_endproc
.LFE3305:
	.size	_ZNK6icu_678Calendar16getDayOfWeekTypeE19UCalendarDaysOfWeekR10UErrorCode, .-_ZNK6icu_678Calendar16getDayOfWeekTypeE19UCalendarDaysOfWeekR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678Calendar20getWeekendTransitionE19UCalendarDaysOfWeekR10UErrorCode
	.type	_ZNK6icu_678Calendar20getWeekendTransitionE19UCalendarDaysOfWeekR10UErrorCode, @function
_ZNK6icu_678Calendar20getWeekendTransitionE19UCalendarDaysOfWeekR10UErrorCode:
.LFB3306:
	.cfi_startproc
	endbr64
	movl	(%rdx), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L73
	cmpl	264(%rdi), %esi
	je	.L78
	cmpl	272(%rdi), %esi
	je	.L79
	movl	$1, (%rdx)
.L73:
	ret
	.p2align 4,,10
	.p2align 3
.L78:
	movl	268(%rdi), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	movl	276(%rdi), %eax
	ret
	.cfi_endproc
.LFE3306:
	.size	_ZNK6icu_678Calendar20getWeekendTransitionE19UCalendarDaysOfWeekR10UErrorCode, .-_ZNK6icu_678Calendar20getWeekendTransitionE19UCalendarDaysOfWeekR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678Calendar8getLimitE19UCalendarDateFieldsNS0_10ELimitTypeE
	.type	_ZNK6icu_678Calendar8getLimitE19UCalendarDateFieldsNS0_10ELimitTypeE, @function
_ZNK6icu_678Calendar8getLimitE19UCalendarDateFieldsNS0_10ELimitTypeE:
.LFB3318:
	.cfi_startproc
	endbr64
	leal	-4(%rsi), %eax
	cmpl	$18, %eax
	ja	.L81
	leaq	.L83(%rip), %rcx
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L83:
	.long	.L84-.L83
	.long	.L81-.L83
	.long	.L81-.L83
	.long	.L82-.L83
	.long	.L81-.L83
	.long	.L82-.L83
	.long	.L82-.L83
	.long	.L82-.L83
	.long	.L82-.L83
	.long	.L82-.L83
	.long	.L82-.L83
	.long	.L82-.L83
	.long	.L82-.L83
	.long	.L81-.L83
	.long	.L82-.L83
	.long	.L81-.L83
	.long	.L82-.L83
	.long	.L82-.L83
	.long	.L82-.L83
	.text
	.p2align 4,,10
	.p2align 3
.L82:
	movslq	%esi, %rsi
	movslq	%edx, %rdx
	leaq	_ZN6icu_67L15kCalendarLimitsE(%rip), %rax
	leaq	(%rdx,%rsi,4), %rdx
	movl	(%rax,%rdx,4), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L81:
	movq	(%rdi), %rax
	jmp	*248(%rax)
	.p2align 4,,10
	.p2align 3
.L84:
	testl	%edx, %edx
	je	.L94
	movl	$1, %eax
	cmpl	$1, %edx
	je	.L91
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$5, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rax
	movl	%edx, -20(%rbp)
	movzbl	260(%rdi), %ebx
	call	*248(%rax)
	movl	-20(%rbp), %edx
	movl	$7, %ecx
	subl	%ebx, %ecx
	cmpl	$2, %edx
	je	.L95
	leal	6(%rcx,%rax), %edx
	movslq	%edx, %rax
	imulq	$-1840700269, %rax, %rax
	shrq	$32, %rax
	addl	%edx, %eax
	sarl	$31, %edx
	sarl	$2, %eax
	subl	%edx, %eax
.L80:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	.cfi_restore 3
	.cfi_restore 6
	xorl	%eax, %eax
	cmpb	$1, 260(%rdi)
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L91:
	ret
	.p2align 4,,10
	.p2align 3
.L95:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	addl	%eax, %ecx
	movslq	%ecx, %rax
	imulq	$-1840700269, %rax, %rax
	shrq	$32, %rax
	addl	%ecx, %eax
	sarl	$31, %ecx
	sarl	$2, %eax
	subl	%ecx, %eax
	jmp	.L80
	.cfi_endproc
.LFE3318:
	.size	_ZNK6icu_678Calendar8getLimitE19UCalendarDateFieldsNS0_10ELimitTypeE, .-_ZNK6icu_678Calendar8getLimitE19UCalendarDateFieldsNS0_10ELimitTypeE
	.section	.text._ZNK6icu_6714LocaleCacheKeyINS_14SharedCalendarEEeqERKNS_12CacheKeyBaseE,"axG",@progbits,_ZNK6icu_6714LocaleCacheKeyINS_14SharedCalendarEEeqERKNS_12CacheKeyBaseE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6714LocaleCacheKeyINS_14SharedCalendarEEeqERKNS_12CacheKeyBaseE
	.type	_ZNK6icu_6714LocaleCacheKeyINS_14SharedCalendarEEeqERKNS_12CacheKeyBaseE, @function
_ZNK6icu_6714LocaleCacheKeyINS_14SharedCalendarEEeqERKNS_12CacheKeyBaseE:
.LFB4313:
	.cfi_startproc
	endbr64
	cmpq	%rsi, %rdi
	je	.L99
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	movq	-8(%rax), %rax
	movq	8(%rax), %rdi
	movq	(%rsi), %rax
	movq	-8(%rax), %rax
	movq	8(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L98
	cmpb	$42, (%rdi)
	je	.L101
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L101
.L98:
	leaq	16(%r12), %rsi
	leaq	16(%rbx), %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_676LocaleeqERKS0_@PLT
	.p2align 4,,10
	.p2align 3
.L101:
	.cfi_restore_state
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L99:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE4313:
	.size	_ZNK6icu_6714LocaleCacheKeyINS_14SharedCalendarEEeqERKNS_12CacheKeyBaseE, .-_ZNK6icu_6714LocaleCacheKeyINS_14SharedCalendarEEeqERKNS_12CacheKeyBaseE
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714SharedCalendarD0Ev
	.type	_ZN6icu_6714SharedCalendarD0Ev, @function
_ZN6icu_6714SharedCalendarD0Ev:
.LFB3221:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6714SharedCalendarE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L107
	movq	(%rdi), %rax
	call	*8(%rax)
.L107:
	movq	%r12, %rdi
	call	_ZN6icu_6712SharedObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3221:
	.size	_ZN6icu_6714SharedCalendarD0Ev, .-_ZN6icu_6714SharedCalendarD0Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678Calendar18getGreatestMinimumE19UCalendarDateFields
	.type	_ZNK6icu_678Calendar18getGreatestMinimumE19UCalendarDateFields, @function
_ZNK6icu_678Calendar18getGreatestMinimumE19UCalendarDateFields:
.LFB3314:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rcx
	leaq	_ZNK6icu_678Calendar8getLimitE19UCalendarDateFieldsNS0_10ELimitTypeE(%rip), %rdx
	movq	256(%rcx), %rax
	cmpq	%rdx, %rax
	jne	.L113
	leal	-4(%rsi), %eax
	cmpl	$18, %eax
	ja	.L114
	leaq	.L116(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L116:
	.long	.L118-.L116
	.long	.L114-.L116
	.long	.L114-.L116
	.long	.L115-.L116
	.long	.L114-.L116
	.long	.L115-.L116
	.long	.L115-.L116
	.long	.L115-.L116
	.long	.L115-.L116
	.long	.L115-.L116
	.long	.L115-.L116
	.long	.L115-.L116
	.long	.L115-.L116
	.long	.L114-.L116
	.long	.L115-.L116
	.long	.L114-.L116
	.long	.L115-.L116
	.long	.L115-.L116
	.long	.L115-.L116
	.text
.L118:
	movl	$1, %eax
	ret
.L115:
	movslq	%esi, %rsi
	leaq	_ZN6icu_67L15kCalendarLimitsE(%rip), %rax
	salq	$4, %rsi
	movl	4(%rax,%rsi), %eax
	ret
.L114:
	movq	248(%rcx), %rax
	movl	$1, %edx
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L113:
	movl	$1, %edx
	jmp	*%rax
	.cfi_endproc
.LFE3314:
	.size	_ZNK6icu_678Calendar18getGreatestMinimumE19UCalendarDateFields, .-_ZNK6icu_678Calendar18getGreatestMinimumE19UCalendarDateFields
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678Calendar18getGreatestMinimumENS0_11EDateFieldsE
	.type	_ZNK6icu_678Calendar18getGreatestMinimumENS0_11EDateFieldsE, @function
_ZNK6icu_678Calendar18getGreatestMinimumENS0_11EDateFieldsE:
.LFB3313:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rcx
	leaq	_ZNK6icu_678Calendar8getLimitE19UCalendarDateFieldsNS0_10ELimitTypeE(%rip), %rdx
	movq	256(%rcx), %rax
	cmpq	%rdx, %rax
	jne	.L120
	leal	-4(%rsi), %eax
	cmpl	$18, %eax
	ja	.L121
	leaq	.L123(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L123:
	.long	.L125-.L123
	.long	.L121-.L123
	.long	.L121-.L123
	.long	.L122-.L123
	.long	.L121-.L123
	.long	.L122-.L123
	.long	.L122-.L123
	.long	.L122-.L123
	.long	.L122-.L123
	.long	.L122-.L123
	.long	.L122-.L123
	.long	.L122-.L123
	.long	.L122-.L123
	.long	.L121-.L123
	.long	.L122-.L123
	.long	.L121-.L123
	.long	.L122-.L123
	.long	.L122-.L123
	.long	.L122-.L123
	.text
.L125:
	movl	$1, %eax
	ret
.L122:
	movslq	%esi, %rsi
	leaq	_ZN6icu_67L15kCalendarLimitsE(%rip), %rax
	salq	$4, %rsi
	movl	4(%rax,%rsi), %eax
	ret
.L121:
	movq	248(%rcx), %rax
	movl	$1, %edx
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L120:
	movl	$1, %edx
	jmp	*%rax
	.cfi_endproc
.LFE3313:
	.size	_ZNK6icu_678Calendar18getGreatestMinimumENS0_11EDateFieldsE, .-_ZNK6icu_678Calendar18getGreatestMinimumENS0_11EDateFieldsE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678CalendareqERKS0_
	.type	_ZNK6icu_678CalendareqERKS0_, @function
_ZNK6icu_678CalendareqERKS0_:
.LFB3252:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZNK6icu_678Calendar14isEquivalentToERKS0_(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movl	$0, -28(%rbp)
	movq	40(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L127
	movq	-8(%rax), %rax
	movq	8(%rax), %rdi
	movq	(%rsi), %rax
	movq	-8(%rax), %rax
	movq	8(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L128
	cmpb	$42, (%rdi)
	je	.L129
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L128
	.p2align 4,,10
	.p2align 3
.L129:
	xorl	%eax, %eax
.L126:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L148
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L128:
	.cfi_restore_state
	movzbl	232(%r12), %eax
	cmpb	%al, 232(%rbx)
	jne	.L129
	movq	248(%r12), %rax
	cmpq	%rax, 248(%rbx)
	jne	.L129
	movq	256(%rbx), %rax
	movabsq	$1099511627775, %rdx
	xorq	256(%r12), %rax
	testq	%rdx, %rax
	jne	.L129
	movq	264(%r12), %rax
	cmpq	%rax, 264(%rbx)
	jne	.L129
	movq	272(%r12), %rax
	cmpq	%rax, 272(%rbx)
	jne	.L129
	movq	240(%rbx), %rdi
	movq	240(%r12), %rsi
	movq	(%rdi), %rax
	call	*24(%rax)
	testb	%al, %al
	setne	%al
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L127:
	call	*%rdx
.L130:
	testb	%al, %al
	je	.L129
	movl	-28(%rbp), %eax
	testl	%eax, %eax
	jg	.L141
	cmpb	$0, 8(%rbx)
	je	.L149
.L134:
	cmpb	$0, 8(%r12)
	movsd	224(%rbx), %xmm1
	jne	.L143
	movq	(%r12), %rax
	movsd	%xmm1, -40(%rbp)
	leaq	-28(%rbp), %rsi
	movq	%r12, %rdi
	call	*224(%rax)
	movl	-28(%rbp), %eax
	movsd	-40(%rbp), %xmm1
	pxor	%xmm0, %xmm0
	testl	%eax, %eax
	jg	.L137
	cmpb	$0, 232(%r12)
	je	.L150
.L138:
	movb	$0, 9(%r12)
.L139:
	movb	$1, 8(%r12)
	movb	$0, 11(%r12)
.L143:
	movsd	224(%r12), %xmm0
.L137:
	ucomisd	%xmm1, %xmm0
	jp	.L129
	movl	-28(%rbp), %eax
	jne	.L129
.L141:
	testl	%eax, %eax
	setle	%al
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L149:
	movq	(%rbx), %rax
	leaq	-28(%rbp), %rsi
	movq	%rbx, %rdi
	call	*224(%rax)
	movl	-28(%rbp), %eax
	testl	%eax, %eax
	jg	.L141
	cmpb	$0, 232(%rbx)
	je	.L151
.L135:
	movb	$0, 9(%rbx)
.L136:
	movb	$1, 8(%rbx)
	movb	$0, 11(%rbx)
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L151:
	cmpb	$0, 10(%rbx)
	jne	.L136
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L150:
	cmpb	$0, 10(%r12)
	jne	.L139
	jmp	.L138
.L148:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3252:
	.size	_ZNK6icu_678CalendareqERKS0_, .-_ZNK6icu_678CalendareqERKS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678Calendar10getMinimumENS0_11EDateFieldsE
	.type	_ZNK6icu_678Calendar10getMinimumENS0_11EDateFieldsE, @function
_ZNK6icu_678Calendar10getMinimumENS0_11EDateFieldsE:
.LFB3309:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rcx
	leaq	_ZNK6icu_678Calendar8getLimitE19UCalendarDateFieldsNS0_10ELimitTypeE(%rip), %rdx
	movq	256(%rcx), %rax
	cmpq	%rdx, %rax
	jne	.L153
	leal	-4(%rsi), %eax
	cmpl	$18, %eax
	ja	.L154
	leaq	.L156(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L156:
	.long	.L157-.L156
	.long	.L154-.L156
	.long	.L154-.L156
	.long	.L155-.L156
	.long	.L154-.L156
	.long	.L155-.L156
	.long	.L155-.L156
	.long	.L155-.L156
	.long	.L155-.L156
	.long	.L155-.L156
	.long	.L155-.L156
	.long	.L155-.L156
	.long	.L155-.L156
	.long	.L154-.L156
	.long	.L155-.L156
	.long	.L154-.L156
	.long	.L155-.L156
	.long	.L155-.L156
	.long	.L155-.L156
	.text
.L155:
	movslq	%esi, %rsi
	leaq	_ZN6icu_67L15kCalendarLimitsE(%rip), %rax
	salq	$4, %rsi
	movl	(%rax,%rsi), %eax
	ret
.L157:
	xorl	%eax, %eax
	cmpb	$1, 260(%rdi)
	sete	%al
	ret
.L154:
	movq	248(%rcx), %rax
	xorl	%edx, %edx
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L153:
	xorl	%edx, %edx
	jmp	*%rax
	.cfi_endproc
.LFE3309:
	.size	_ZNK6icu_678Calendar10getMinimumENS0_11EDateFieldsE, .-_ZNK6icu_678Calendar10getMinimumENS0_11EDateFieldsE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678Calendar10getMinimumE19UCalendarDateFields
	.type	_ZNK6icu_678Calendar10getMinimumE19UCalendarDateFields, @function
_ZNK6icu_678Calendar10getMinimumE19UCalendarDateFields:
.LFB3310:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rcx
	leaq	_ZNK6icu_678Calendar8getLimitE19UCalendarDateFieldsNS0_10ELimitTypeE(%rip), %rdx
	movq	256(%rcx), %rax
	cmpq	%rdx, %rax
	jne	.L160
	leal	-4(%rsi), %eax
	cmpl	$18, %eax
	ja	.L161
	leaq	.L163(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L163:
	.long	.L164-.L163
	.long	.L161-.L163
	.long	.L161-.L163
	.long	.L162-.L163
	.long	.L161-.L163
	.long	.L162-.L163
	.long	.L162-.L163
	.long	.L162-.L163
	.long	.L162-.L163
	.long	.L162-.L163
	.long	.L162-.L163
	.long	.L162-.L163
	.long	.L162-.L163
	.long	.L161-.L163
	.long	.L162-.L163
	.long	.L161-.L163
	.long	.L162-.L163
	.long	.L162-.L163
	.long	.L162-.L163
	.text
.L162:
	movslq	%esi, %rsi
	leaq	_ZN6icu_67L15kCalendarLimitsE(%rip), %rax
	salq	$4, %rsi
	movl	(%rax,%rsi), %eax
	ret
.L164:
	xorl	%eax, %eax
	cmpb	$1, 260(%rdi)
	sete	%al
	ret
.L161:
	movq	248(%rcx), %rax
	xorl	%edx, %edx
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L160:
	xorl	%edx, %edx
	jmp	*%rax
	.cfi_endproc
.LFE3310:
	.size	_ZNK6icu_678Calendar10getMinimumE19UCalendarDateFields, .-_ZNK6icu_678Calendar10getMinimumE19UCalendarDateFields
	.align 2
	.p2align 4
	.type	_ZN6icu_678Calendar13computeFieldsER10UErrorCode.part.0, @function
_ZN6icu_678Calendar13computeFieldsER10UErrorCode.part.0:
.LFB4347:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	leaq	-68(%rbp), %rdx
	leaq	-64(%rbp), %rcx
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movsd	224(%rdi), %xmm1
	movq	240(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -88(%rbp)
	call	*48(%rax)
	movl	-64(%rbp), %eax
	pxor	%xmm0, %xmm0
	addl	-68(%rbp), %eax
	cvtsi2sdl	%eax, %xmm0
	movsd	-88(%rbp), %xmm1
	xorl	%eax, %eax
	movl	$524391, %edx
	addsd	%xmm1, %xmm0
	movsd	%xmm0, -88(%rbp)
	.p2align 4,,10
	.p2align 3
.L170:
	testb	$1, %dl
	je	.L167
	movb	$0, 104(%rbx,%rax)
	shrl	%edx
	movl	$0, 128(%rbx,%rax,4)
	addq	$1, %rax
	cmpq	$23, %rax
	jne	.L170
.L168:
	movsd	-88(%rbp), %xmm0
	divsd	.LC1(%rip), %xmm0
	call	uprv_floor_67@PLT
	pxor	%xmm2, %xmm2
	movb	$1, 124(%rbx)
	leaq	292(%rbx), %rdx
	cvttsd2sil	%xmm0, %eax
	leaq	-60(%rbp), %rcx
	movl	$1, 208(%rbx)
	leaq	284(%rbx), %rsi
	leaq	280(%rbx), %rdi
	leaq	288(%rbx), %r8
	cvtsi2sdl	%eax, %xmm2
	leal	2440588(%rax), %r13d
	movl	%r13d, 92(%rbx)
	movapd	%xmm2, %xmm0
	movsd	%xmm2, -96(%rbp)
	call	_ZN6icu_675Grego11dayToFieldsEdRiS1_S1_S1_S1_@PLT
	pxor	%xmm0, %xmm0
	movsd	.LC2(%rip), %xmm1
	cvtsi2sdl	%r13d, %xmm0
	addsd	.LC3(%rip), %xmm0
	call	uprv_fmod_67@PLT
	movb	$1, 111(%rbx)
	movsd	-96(%rbp), %xmm2
	cvttsd2sil	%xmm0, %edx
	movb	$1, 122(%rbx)
	movl	$1, 156(%rbx)
	movl	$1, 200(%rbx)
	movl	%edx, %eax
	sarb	$7, %al
	andl	$7, %eax
	leal	1(%rax,%rdx), %eax
	movzbl	%al, %eax
	movl	%eax, 40(%rbx)
	subl	256(%rbx), %eax
	leal	1(%rax), %edx
	addl	$8, %eax
	testl	%edx, %edx
	cmovle	%eax, %edx
	movq	(%rbx), %rax
	movq	328(%rax), %rax
	movl	%edx, 84(%rbx)
	leaq	_ZN6icu_678Calendar19handleComputeFieldsEiR10UErrorCode(%rip), %rdx
	cmpq	%rdx, %rax
	jne	.L173
	movl	284(%rbx), %eax
	movl	$257, %ecx
	movb	$1, 106(%rbx)
	movl	$1, %edx
	movl	$1, 136(%rbx)
	movl	%eax, 20(%rbx)
	movl	292(%rbx), %eax
	movw	%cx, 109(%rbx)
	movl	%eax, 32(%rbx)
	movl	288(%rbx), %eax
	movl	$1, 204(%rbx)
	movl	%eax, 36(%rbx)
	movabsq	$4294967297, %rax
	movq	%rax, 148(%rbx)
	movl	280(%rbx), %eax
	movb	$1, 123(%rbx)
	movl	%eax, 88(%rbx)
	testl	%eax, %eax
	jle	.L199
.L174:
	movl	%edx, 12(%rbx)
	movl	$257, %edx
	movl	%eax, 16(%rbx)
	movabsq	$4294967297, %rax
	movq	%rax, 128(%rbx)
	movw	%dx, 104(%rbx)
.L175:
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L176
	movl	40(%rbx), %r13d
	movl	36(%rbx), %r14d
	movl	$7001, %edx
	movl	256(%rbx), %r9d
	movq	(%rbx), %r10
	movl	%r13d, %eax
	movl	88(%rbx), %r8d
	subl	%r14d, %eax
	subl	%r9d, %edx
	addl	%eax, %edx
	movslq	%edx, %rax
	movl	%edx, %ecx
	imulq	$-1840700269, %rax, %rax
	sarl	$31, %ecx
	shrq	$32, %rax
	addl	%edx, %eax
	sarl	$2, %eax
	subl	%ecx, %eax
	leal	0(,%rax,8), %ecx
	subl	%eax, %ecx
	subl	%ecx, %edx
	movl	%edx, %eax
	leal	-1(%r14,%rdx), %edx
	movslq	%edx, %r12
	imulq	$-1840700269, %r12, %r12
	shrq	$32, %r12
	addl	%edx, %r12d
	sarl	$31, %edx
	sarl	$2, %r12d
	subl	%edx, %r12d
	movl	$7, %edx
	subl	%eax, %edx
	movl	%edx, %eax
	movzbl	260(%rbx), %edx
	cmpl	%edx, %eax
	movq	280(%r10), %rdx
	setge	%al
	movzbl	%al, %eax
	addl	%eax, %r12d
	jne	.L178
	leaq	_ZNK6icu_678Calendar19handleGetYearLengthEi(%rip), %rax
	leal	-1(%r8), %r15d
	movsd	%xmm2, -96(%rbp)
	cmpq	%rax, %rdx
	jne	.L179
	movl	%r8d, %esi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	*264(%r10)
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	%r15d, %esi
	movl	%eax, %r12d
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*264(%rax)
	movsd	-96(%rbp), %xmm2
	subl	%eax, %r12d
	movl	%r12d, %eax
.L180:
	movl	%r13d, %edx
	subl	256(%rbx), %edx
	addl	%eax, %r14d
	movl	%r15d, %r8d
	movl	%edx, %ecx
	subl	%r14d, %ecx
	addl	$1, %ecx
	movslq	%ecx, %rax
	movl	%ecx, %esi
	imulq	$-1840700269, %rax, %rax
	sarl	$31, %esi
	shrq	$32, %rax
	addl	%ecx, %eax
	sarl	$2, %eax
	subl	%esi, %eax
	leal	0(,%rax,8), %esi
	subl	%eax, %esi
	subl	%esi, %ecx
	movl	$7, %esi
	movl	%ecx, %eax
	leal	7(%rcx), %ecx
	cmovs	%ecx, %eax
	leal	-1(%r14,%rax), %ecx
	subl	%eax, %esi
	movslq	%ecx, %r12
	imulq	$-1840700269, %r12, %r12
	shrq	$32, %r12
	addl	%ecx, %r12d
	sarl	$31, %ecx
	sarl	$2, %r12d
	subl	%ecx, %r12d
	movzbl	260(%rbx), %ecx
	cmpl	%ecx, %esi
	setge	%al
	movzbl	%al, %eax
	addl	%eax, %r12d
.L182:
	movl	32(%rbx), %esi
	movl	%r12d, 24(%rbx)
	movl	%r8d, 80(%rbx)
	subl	%esi, %edx
	addl	$1, %edx
	movslq	%edx, %rax
	movl	%edx, %edi
	imulq	$-1840700269, %rax, %rax
	sarl	$31, %edi
	shrq	$32, %rax
	addl	%edx, %eax
	sarl	$2, %eax
	subl	%edi, %eax
	leal	0(,%rax,8), %edi
	subl	%eax, %edi
	subl	%edi, %edx
	movl	%edx, %eax
	leal	7(%rdx), %edx
	cmovs	%edx, %eax
	leal	-1(%rsi,%rax), %edi
	movslq	%edi, %rdx
	imulq	$-1840700269, %rdx, %rdx
	shrq	$32, %rdx
	addl	%edi, %edx
	sarl	$31, %edi
	sarl	$2, %edx
	subl	%edi, %edx
	movl	$7, %edi
	subl	%eax, %edi
	cmpl	%ecx, %edi
	setge	%al
	movzbl	%al, %eax
	addl	%eax, %edx
	movl	%edx, 28(%rbx)
	leal	-1(%rsi), %edx
	movslq	%edx, %rax
	imulq	$-1840700269, %rax, %rax
	shrq	$32, %rax
	addl	%edx, %eax
	sarl	$31, %edx
	sarl	$2, %eax
	subl	%edx, %eax
	addl	$1, %eax
	movl	%eax, 44(%rbx)
.L176:
	mulsd	.LC1(%rip), %xmm2
	movsd	-88(%rbp), %xmm0
	subsd	%xmm2, %xmm0
	cvttsd2sil	%xmm0, %edx
	movslq	%edx, %rax
	movl	%edx, %ecx
	movl	%edx, 96(%rbx)
	imulq	$274877907, %rax, %rax
	sarl	$31, %ecx
	sarq	$38, %rax
	subl	%ecx, %eax
	imull	$1000, %eax, %ecx
	subl	%ecx, %edx
	movl	%eax, %ecx
	movl	%edx, 68(%rbx)
	movslq	%eax, %rdx
	sarl	$31, %ecx
	imulq	$-2004318071, %rdx, %rdx
	shrq	$32, %rdx
	addl	%eax, %edx
	sarl	$5, %edx
	subl	%ecx, %edx
	imull	$60, %edx, %ecx
	subl	%ecx, %eax
	movl	%edx, %ecx
	movl	%eax, 64(%rbx)
	movslq	%edx, %rax
	sarl	$31, %ecx
	imulq	$-2004318071, %rax, %rax
	shrq	$32, %rax
	addl	%edx, %eax
	sarl	$5, %eax
	subl	%ecx, %eax
	imull	$60, %eax, %ecx
	movl	%eax, 56(%rbx)
	subl	%ecx, %edx
	movl	%eax, %ecx
	movl	%edx, 60(%rbx)
	movslq	%eax, %rdx
	sarl	$31, %ecx
	imulq	$715827883, %rdx, %rdx
	sarq	$33, %rdx
	subl	%ecx, %edx
	movl	%edx, 48(%rbx)
	leal	(%rdx,%rdx,2), %edx
	sall	$2, %edx
	subl	%edx, %eax
	movl	%eax, 52(%rbx)
	movl	-68(%rbp), %eax
	movl	%eax, 72(%rbx)
	movl	-64(%rbp), %eax
	movl	%eax, 76(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L200
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L167:
	.cfi_restore_state
	movb	$1, 104(%rbx,%rax)
	shrl	%edx
	movl	$1, 128(%rbx,%rax,4)
	addq	$1, %rax
	cmpq	$23, %rax
	jne	.L170
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L199:
	subl	%eax, %edx
	movl	%edx, %eax
	xorl	%edx, %edx
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L178:
	leaq	_ZNK6icu_678Calendar19handleGetYearLengthEi(%rip), %rax
	movl	%r9d, -108(%rbp)
	movsd	%xmm2, -104(%rbp)
	cmpq	%rax, %rdx
	jne	.L183
	leal	1(%r8), %esi
	movl	%r8d, -96(%rbp)
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	*264(%r10)
	movl	-96(%rbp), %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	%eax, %r15d
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movl	%r8d, %esi
	call	*264(%rax)
	movl	-96(%rbp), %r8d
	movsd	-104(%rbp), %xmm2
	movl	-108(%rbp), %r9d
	subl	%eax, %r15d
.L184:
	leal	-5(%r15), %eax
	movl	%r13d, %edx
	movzbl	260(%rbx), %ecx
	subl	256(%rbx), %edx
	cmpl	%eax, %r14d
	jl	.L182
	subl	%r9d, %r13d
	leal	7(%r13), %esi
	movslq	%esi, %rax
	movl	%esi, %edi
	imulq	$-1840700269, %rax, %rax
	sarl	$31, %edi
	shrq	$32, %rax
	addl	%esi, %eax
	sarl	$2, %eax
	subl	%edi, %eax
	leal	0(,%rax,8), %edi
	subl	%eax, %edi
	subl	%edi, %esi
	leal	(%rsi,%r15), %edi
	movl	%esi, %eax
	subl	%r14d, %edi
	movslq	%edi, %rsi
	movl	%edi, %r9d
	imulq	$-1840700269, %rsi, %rsi
	sarl	$31, %r9d
	shrq	$32, %rsi
	addl	%edi, %esi
	sarl	$2, %esi
	subl	%r9d, %esi
	leal	0(,%rsi,8), %r9d
	subl	%esi, %r9d
	subl	%r9d, %edi
	movl	%edi, %esi
	leal	7(%rdi), %edi
	cmovs	%edi, %esi
	movl	$6, %edi
	subl	%esi, %edi
	cmpl	%ecx, %edi
	jl	.L182
	addl	$7, %r14d
	subl	%eax, %r14d
	cmpl	%r14d, %r15d
	jge	.L182
	addl	$1, %r8d
	movl	$1, %r12d
	jmp	.L182
.L173:
	movsd	%xmm2, -96(%rbp)
	movl	92(%rbx), %esi
	movq	%r12, %rdx
	movq	%rbx, %rdi
	call	*%rax
	movsd	-96(%rbp), %xmm2
	jmp	.L175
.L183:
	movl	%r8d, -96(%rbp)
	movl	%r8d, %esi
	movq	%rbx, %rdi
	call	*%rdx
	movl	-108(%rbp), %r9d
	movsd	-104(%rbp), %xmm2
	movl	-96(%rbp), %r8d
	movl	%eax, %r15d
	jmp	.L184
.L179:
	movl	%r15d, %esi
	movq	%rbx, %rdi
	call	*%rdx
	movsd	-96(%rbp), %xmm2
	jmp	.L180
.L200:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4347:
	.size	_ZN6icu_678Calendar13computeFieldsER10UErrorCode.part.0, .-_ZN6icu_678Calendar13computeFieldsER10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Calendar13computeFieldsER10UErrorCode
	.type	_ZN6icu_678Calendar13computeFieldsER10UErrorCode, @function
_ZN6icu_678Calendar13computeFieldsER10UErrorCode:
.LFB3277:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L201
	jmp	_ZN6icu_678Calendar13computeFieldsER10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L201:
	ret
	.cfi_endproc
.LFE3277:
	.size	_ZN6icu_678Calendar13computeFieldsER10UErrorCode, .-_ZN6icu_678Calendar13computeFieldsER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678Calendar15getLeastMaximumE19UCalendarDateFields
	.type	_ZNK6icu_678Calendar15getLeastMaximumE19UCalendarDateFields, @function
_ZNK6icu_678Calendar15getLeastMaximumE19UCalendarDateFields:
.LFB3316:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rcx
	leaq	_ZNK6icu_678Calendar8getLimitE19UCalendarDateFieldsNS0_10ELimitTypeE(%rip), %rdx
	movq	256(%rcx), %rax
	cmpq	%rdx, %rax
	jne	.L204
	leal	-4(%rsi), %eax
	cmpl	$18, %eax
	ja	.L205
	leaq	.L207(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L207:
	.long	.L208-.L207
	.long	.L205-.L207
	.long	.L205-.L207
	.long	.L206-.L207
	.long	.L205-.L207
	.long	.L206-.L207
	.long	.L206-.L207
	.long	.L206-.L207
	.long	.L206-.L207
	.long	.L206-.L207
	.long	.L206-.L207
	.long	.L206-.L207
	.long	.L206-.L207
	.long	.L205-.L207
	.long	.L206-.L207
	.long	.L205-.L207
	.long	.L206-.L207
	.long	.L206-.L207
	.long	.L206-.L207
	.text
.L206:
	movslq	%esi, %rsi
	leaq	_ZN6icu_67L15kCalendarLimitsE(%rip), %rax
	salq	$4, %rsi
	movl	8(%rax,%rsi), %eax
	ret
.L208:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %edx
	movl	$5, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movzbl	260(%rdi), %ebx
	call	*248(%rcx)
	addq	$8, %rsp
	subl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leal	7(%rax), %edx
	movslq	%edx, %rax
	imulq	$-1840700269, %rax, %rax
	shrq	$32, %rax
	addl	%edx, %eax
	sarl	$31, %edx
	sarl	$2, %eax
	subl	%edx, %eax
	ret
.L205:
	.cfi_restore 3
	.cfi_restore 6
	movq	248(%rcx), %rax
	movl	$2, %edx
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L204:
	movl	$2, %edx
	jmp	*%rax
	.cfi_endproc
.LFE3316:
	.size	_ZNK6icu_678Calendar15getLeastMaximumE19UCalendarDateFields, .-_ZNK6icu_678Calendar15getLeastMaximumE19UCalendarDateFields
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678Calendar15getLeastMaximumENS0_11EDateFieldsE
	.type	_ZNK6icu_678Calendar15getLeastMaximumENS0_11EDateFieldsE, @function
_ZNK6icu_678Calendar15getLeastMaximumENS0_11EDateFieldsE:
.LFB3315:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rcx
	leaq	_ZNK6icu_678Calendar8getLimitE19UCalendarDateFieldsNS0_10ELimitTypeE(%rip), %rdx
	movq	256(%rcx), %rax
	cmpq	%rdx, %rax
	jne	.L214
	leal	-4(%rsi), %eax
	cmpl	$18, %eax
	ja	.L215
	leaq	.L217(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L217:
	.long	.L218-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L216-.L217
	.long	.L215-.L217
	.long	.L216-.L217
	.long	.L216-.L217
	.long	.L216-.L217
	.long	.L216-.L217
	.long	.L216-.L217
	.long	.L216-.L217
	.long	.L216-.L217
	.long	.L216-.L217
	.long	.L215-.L217
	.long	.L216-.L217
	.long	.L215-.L217
	.long	.L216-.L217
	.long	.L216-.L217
	.long	.L216-.L217
	.text
.L216:
	movslq	%esi, %rsi
	leaq	_ZN6icu_67L15kCalendarLimitsE(%rip), %rax
	salq	$4, %rsi
	movl	8(%rax,%rsi), %eax
	ret
.L218:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %edx
	movl	$5, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movzbl	260(%rdi), %ebx
	call	*248(%rcx)
	addq	$8, %rsp
	subl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leal	7(%rax), %edx
	movslq	%edx, %rax
	imulq	$-1840700269, %rax, %rax
	shrq	$32, %rax
	addl	%edx, %eax
	sarl	$31, %edx
	sarl	$2, %eax
	subl	%edx, %eax
	ret
.L215:
	.cfi_restore 3
	.cfi_restore 6
	movq	248(%rcx), %rax
	movl	$2, %edx
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L214:
	movl	$2, %edx
	jmp	*%rax
	.cfi_endproc
.LFE3315:
	.size	_ZNK6icu_678Calendar15getLeastMaximumENS0_11EDateFieldsE, .-_ZNK6icu_678Calendar15getLeastMaximumENS0_11EDateFieldsE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678Calendar10getMaximumE19UCalendarDateFields
	.type	_ZNK6icu_678Calendar10getMaximumE19UCalendarDateFields, @function
_ZNK6icu_678Calendar10getMaximumE19UCalendarDateFields:
.LFB3312:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rcx
	leaq	_ZNK6icu_678Calendar8getLimitE19UCalendarDateFieldsNS0_10ELimitTypeE(%rip), %rdx
	movq	256(%rcx), %rax
	cmpq	%rdx, %rax
	jne	.L224
	leal	-4(%rsi), %eax
	cmpl	$18, %eax
	ja	.L225
	leaq	.L227(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L227:
	.long	.L228-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L226-.L227
	.long	.L225-.L227
	.long	.L226-.L227
	.long	.L226-.L227
	.long	.L226-.L227
	.long	.L226-.L227
	.long	.L226-.L227
	.long	.L226-.L227
	.long	.L226-.L227
	.long	.L226-.L227
	.long	.L225-.L227
	.long	.L226-.L227
	.long	.L225-.L227
	.long	.L226-.L227
	.long	.L226-.L227
	.long	.L226-.L227
	.text
.L226:
	movslq	%esi, %rsi
	leaq	_ZN6icu_67L15kCalendarLimitsE(%rip), %rax
	salq	$4, %rsi
	movl	12(%rax,%rsi), %eax
	ret
.L228:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3, %edx
	movl	$5, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movzbl	260(%rdi), %ebx
	call	*248(%rcx)
	addq	$8, %rsp
	subl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leal	13(%rax), %edx
	movslq	%edx, %rax
	imulq	$-1840700269, %rax, %rax
	shrq	$32, %rax
	addl	%edx, %eax
	sarl	$31, %edx
	sarl	$2, %eax
	subl	%edx, %eax
	ret
.L225:
	.cfi_restore 3
	.cfi_restore 6
	movq	248(%rcx), %rax
	movl	$3, %edx
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L224:
	movl	$3, %edx
	jmp	*%rax
	.cfi_endproc
.LFE3312:
	.size	_ZNK6icu_678Calendar10getMaximumE19UCalendarDateFields, .-_ZNK6icu_678Calendar10getMaximumE19UCalendarDateFields
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678Calendar10getMaximumENS0_11EDateFieldsE
	.type	_ZNK6icu_678Calendar10getMaximumENS0_11EDateFieldsE, @function
_ZNK6icu_678Calendar10getMaximumENS0_11EDateFieldsE:
.LFB3311:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rcx
	leaq	_ZNK6icu_678Calendar8getLimitE19UCalendarDateFieldsNS0_10ELimitTypeE(%rip), %rdx
	movq	256(%rcx), %rax
	cmpq	%rdx, %rax
	jne	.L234
	leal	-4(%rsi), %eax
	cmpl	$18, %eax
	ja	.L235
	leaq	.L237(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L237:
	.long	.L238-.L237
	.long	.L235-.L237
	.long	.L235-.L237
	.long	.L236-.L237
	.long	.L235-.L237
	.long	.L236-.L237
	.long	.L236-.L237
	.long	.L236-.L237
	.long	.L236-.L237
	.long	.L236-.L237
	.long	.L236-.L237
	.long	.L236-.L237
	.long	.L236-.L237
	.long	.L235-.L237
	.long	.L236-.L237
	.long	.L235-.L237
	.long	.L236-.L237
	.long	.L236-.L237
	.long	.L236-.L237
	.text
.L236:
	movslq	%esi, %rsi
	leaq	_ZN6icu_67L15kCalendarLimitsE(%rip), %rax
	salq	$4, %rsi
	movl	12(%rax,%rsi), %eax
	ret
.L238:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3, %edx
	movl	$5, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movzbl	260(%rdi), %ebx
	call	*248(%rcx)
	addq	$8, %rsp
	subl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leal	13(%rax), %edx
	movslq	%edx, %rax
	imulq	$-1840700269, %rax, %rax
	shrq	$32, %rax
	addl	%edx, %eax
	sarl	$31, %edx
	sarl	$2, %eax
	subl	%edx, %eax
	ret
.L235:
	.cfi_restore 3
	.cfi_restore 6
	movq	248(%rcx), %rax
	movl	$3, %edx
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L234:
	movl	$3, %edx
	jmp	*%rax
	.cfi_endproc
.LFE3311:
	.size	_ZNK6icu_678Calendar10getMaximumENS0_11EDateFieldsE, .-_ZNK6icu_678Calendar10getMaximumENS0_11EDateFieldsE
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC4:
	.string	"gregorian"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Calendar14setRelatedYearEi
	.type	_ZN6icu_678Calendar14setRelatedYearEi, @function
_ZN6icu_678Calendar14setRelatedYearEi:
.LFB3270:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	_ZL9gCalTypes(%rip), %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$40, %rsp
	movl	%esi, -68(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	call	*184(%rax)
	leaq	.LC4(%rip), %rsi
	movq	%rax, %r13
	jmp	.L246
	.p2align 4,,10
	.p2align 3
.L283:
	addq	$1, %rbx
	movq	(%r14,%rbx,8), %rsi
	testq	%rsi, %rsi
	je	.L245
.L246:
	movq	%r13, %rdi
	movl	%ebx, %r12d
	call	uprv_stricmp_67@PLT
	testl	%eax, %eax
	jne	.L283
	subl	$4, %r12d
	cmpl	$13, %r12d
	ja	.L245
	leaq	.L248(%rip), %rdx
	movslq	(%rdx,%r12,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L248:
	.long	.L256-.L248
	.long	.L247-.L248
	.long	.L247-.L248
	.long	.L255-.L248
	.long	.L254-.L248
	.long	.L253-.L248
	.long	.L252-.L248
	.long	.L251-.L248
	.long	.L250-.L248
	.long	.L245-.L248
	.long	.L249-.L248
	.long	.L247-.L248
	.long	.L247-.L248
	.long	.L247-.L248
	.text
	.p2align 4,,10
	.p2align 3
.L247:
	movl	-68(%rbp), %eax
	cmpl	$1976, %eax
	jg	.L284
	leal	-1976(%rax), %edx
	movl	%eax, %ecx
	movl	$4228890877, %esi
	movslq	%edx, %rax
	sarl	$31, %edx
	imulq	$2114445439, %rax, %rax
	sarq	$37, %rax
	subl	%edx, %eax
	movl	$1976, %edx
	subl	%ecx, %edx
	movl	%edx, %ecx
	imulq	%rsi, %rcx
	shrq	$38, %rcx
	movl	%ecx, %esi
	sall	$6, %esi
	addl	%esi, %ecx
	subl	%ecx, %edx
	cmpl	$32, %edx
	setle	%dl
	movzbl	%dl, %edx
	leal	-2(%rdx,%rax,2), %eax
.L258:
	movl	-68(%rbp), %ecx
	leal	-579(%rax,%rcx), %eax
	movl	%eax, -68(%rbp)
	.p2align 4,,10
	.p2align 3
.L245:
	cmpb	$0, 11(%r15)
	je	.L259
	movq	(%r15), %rax
	leaq	_ZN6icu_678Calendar13computeFieldsER10UErrorCode(%rip), %rdx
	leaq	-60(%rbp), %rsi
	movq	%r15, %rdi
	movl	$0, -60(%rbp)
	movq	232(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L260
	call	_ZN6icu_678Calendar13computeFieldsER10UErrorCode.part.0
.L259:
	movl	-68(%rbp), %eax
	movl	%eax, 88(%r15)
	movl	220(%r15), %eax
	cmpl	$10000, %eax
	je	.L262
	leal	1(%rax), %edx
.L263:
	movl	%eax, 204(%r15)
	xorl	%eax, %eax
	movl	%edx, 220(%r15)
	movb	$1, 123(%r15)
	movb	$0, 11(%r15)
	movw	%ax, 8(%r15)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L285
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L262:
	.cfi_restore_state
	movl	$1, 220(%r15)
	movl	$1, %edi
	.p2align 4,,10
	.p2align 3
.L268:
	movl	$10000, %ecx
	xorl	%eax, %eax
	movl	$-1, %esi
	.p2align 4,,10
	.p2align 3
.L266:
	movl	128(%r15,%rax,4), %edx
	cmpl	%edi, %edx
	jle	.L264
	cmpl	%ecx, %edx
	cmovl	%edx, %ecx
	cmovl	%eax, %esi
.L264:
	addq	$1, %rax
	cmpq	$23, %rax
	jne	.L266
	leal	1(%rdi), %eax
	leal	2(%rdi), %edx
	cmpl	$-1, %esi
	je	.L263
	movslq	%esi, %rsi
	movl	%eax, 220(%r15)
	movl	%eax, 128(%r15,%rsi,4)
	cmpl	$24, %eax
	je	.L286
	movl	%eax, %edi
	jmp	.L268
	.p2align 4,,10
	.p2align 3
.L260:
	call	*%rax
	jmp	.L259
.L252:
	subl	$284, -68(%rbp)
	jmp	.L245
.L253:
	subl	$79, -68(%rbp)
	jmp	.L245
.L251:
	subl	$8, -68(%rbp)
	jmp	.L245
.L250:
	addl	$5492, -68(%rbp)
	jmp	.L245
.L249:
	addl	$2333, -68(%rbp)
	jmp	.L245
.L256:
	subl	$622, -68(%rbp)
	jmp	.L245
.L255:
	addl	$3760, -68(%rbp)
	jmp	.L245
.L254:
	addl	$2637, -68(%rbp)
	jmp	.L245
.L284:
	subl	$1977, %eax
	movl	$4228890877, %ecx
	movq	%rax, %rdx
	imulq	%rcx, %rax
	shrq	$38, %rax
	movl	%eax, %ecx
	sall	$6, %ecx
	addl	%eax, %ecx
	subl	%ecx, %edx
	cmpl	$31, %edx
	setg	%dl
	movzbl	%dl, %edx
	leal	(%rdx,%rax,2), %eax
	jmp	.L258
.L286:
	movl	%edx, %eax
	leal	3(%rdi), %edx
	jmp	.L263
.L285:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3270:
	.size	_ZN6icu_678Calendar14setRelatedYearEi, .-_ZN6icu_678Calendar14setRelatedYearEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678Calendar9isWeekendEdR10UErrorCode
	.type	_ZNK6icu_678Calendar9isWeekendEdR10UErrorCode, @function
_ZNK6icu_678Calendar9isWeekendEdR10UErrorCode:
.LFB3307:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	xorl	%r13d, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	(%rsi), %r8d
	movsd	%xmm0, -40(%rbp)
	testl	%r8d, %r8d
	jg	.L287
	movq	(%rdi), %rax
	movq	%rsi, %rbx
	call	*24(%rax)
	movsd	-40(%rbp), %xmm0
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L304
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L290
	comisd	.LC5(%rip), %xmm0
	jbe	.L302
	cmpb	$0, 232(%r12)
	movq	.LC5(%rip), %rax
	movq	%rax, %xmm0
	je	.L295
.L293:
	leaq	20(%r12), %rdi
	movl	%r12d, %edx
	xorl	%eax, %eax
	movl	$16777217, 8(%r12)
	movq	$0, 12(%r12)
	andq	$-8, %rdi
	movq	$0, 119(%r12)
	subl	%edi, %edx
	movsd	%xmm0, 224(%r12)
	leal	127(%rdx), %ecx
	movl	%r12d, %edx
	shrl	$3, %ecx
	rep stosq
	leaq	136(%r12), %rdi
	movq	$0, 128(%r12)
	movq	$0, 212(%r12)
	andq	$-8, %rdi
	subl	%edi, %edx
	leal	220(%rdx), %ecx
	shrl	$3, %ecx
	rep stosq
	movl	(%rbx), %edx
	movq	(%r12), %rax
	testl	%edx, %edx
	jg	.L299
	movq	%r12, %rdi
	call	*216(%rax)
	movq	%r12, %rdi
	movl	%eax, %r13d
	movq	(%r12), %rax
	call	*8(%rax)
.L287:
	addq	$24, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L302:
	.cfi_restore_state
	movsd	.LC6(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jbe	.L293
	cmpb	$0, 232(%rax)
	jne	.L305
.L295:
	movl	$1, (%rbx)
.L290:
	movq	(%r12), %rax
	xorl	%r13d, %r13d
.L296:
	movq	%r12, %rdi
	call	*8(%rax)
	addq	$24, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L304:
	.cfi_restore_state
	movl	$7, (%rbx)
	addq	$24, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L305:
	.cfi_restore_state
	movapd	%xmm1, %xmm0
	jmp	.L293
.L299:
	xorl	%r13d, %r13d
	jmp	.L296
	.cfi_endproc
.LFE3307:
	.size	_ZNK6icu_678Calendar9isWeekendEdR10UErrorCode, .-_ZNK6icu_678Calendar9isWeekendEdR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678Calendar9isWeekendEv
	.type	_ZNK6icu_678Calendar9isWeekendEv, @function
_ZNK6icu_678Calendar9isWeekendEv:
.LFB3308:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 8(%rdi)
	movq	(%rdi), %rax
	movl	$0, -60(%rbp)
	je	.L357
	cmpb	$0, 9(%rdi)
	jne	.L311
	movq	232(%rax), %rax
	leaq	_ZN6icu_678Calendar13computeFieldsER10UErrorCode(%rip), %rdx
	leaq	-60(%rbp), %r14
	cmpq	%rdx, %rax
	jne	.L312
.L330:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_678Calendar13computeFieldsER10UErrorCode.part.0
.L313:
	movl	-60(%rbp), %r9d
	movq	(%r12), %rax
	testl	%r9d, %r9d
	jg	.L314
	movl	$257, %r8d
	movw	%r8w, 9(%r12)
.L311:
	movq	192(%rax), %rcx
	leaq	_ZNK6icu_678Calendar16getDayOfWeekTypeE19UCalendarDaysOfWeekR10UErrorCode(%rip), %rdx
	movl	40(%r12), %r13d
	cmpq	%rdx, %rcx
	jne	.L358
	leal	-1(%r13), %edx
	cmpl	$6, %edx
	ja	.L339
	movl	264(%r12), %edx
	movl	272(%r12), %ecx
	cmpl	%ecx, %edx
	je	.L359
	cmpl	%ecx, %r13d
	setg	%dil
	cmpl	%edx, %r13d
	setl	%sil
	cmpl	%ecx, %edx
	jge	.L321
	testb	%sil, %sil
	jne	.L339
.L355:
	testb	%dil, %dil
	jne	.L339
.L322:
	movl	$1, %r8d
	cmpl	%r13d, %edx
	je	.L360
	cmpl	%r13d, %ecx
	jne	.L306
	cmpl	$86399999, 276(%r12)
	jg	.L306
	movq	200(%rax), %rax
	movl	96(%r12), %r15d
	movl	$3, %ebx
	leaq	_ZNK6icu_678Calendar20getWeekendTransitionE19UCalendarDaysOfWeekR10UErrorCode(%rip), %rdx
	cmpq	%rdx, %rax
	jne	.L356
.L327:
	cmpl	%r13d, 272(%r12)
	jne	.L339
	movl	276(%r12), %eax
.L332:
	cmpl	%r15d, %eax
	setg	%r8b
	cmpl	$2, %ebx
	jne	.L306
	cmpl	%r15d, %eax
	setle	%r8b
	jmp	.L306
	.p2align 4,,10
	.p2align 3
.L359:
	cmpl	%r13d, %edx
	jne	.L339
	movl	268(%r12), %edi
	movl	$1, %r8d
	testl	%edi, %edi
	je	.L306
.L320:
	movq	200(%rax), %rax
	movl	96(%r12), %r15d
	movl	$2, %ebx
	leaq	_ZNK6icu_678Calendar20getWeekendTransitionE19UCalendarDaysOfWeekR10UErrorCode(%rip), %rdx
	cmpq	%rdx, %rax
	je	.L331
.L356:
	leaq	-60(%rbp), %r14
.L326:
	movq	%r14, %rdx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	*%rax
	movl	-60(%rbp), %edx
	testl	%edx, %edx
	jle	.L332
	.p2align 4,,10
	.p2align 3
.L339:
	xorl	%r8d, %r8d
.L306:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L361
	addq	$24, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L357:
	.cfi_restore_state
	leaq	-60(%rbp), %r14
	movq	%r14, %rsi
	call	*224(%rax)
	movl	-60(%rbp), %r11d
	testl	%r11d, %r11d
	jg	.L308
	cmpb	$0, 232(%r12)
	jne	.L309
	cmpb	$0, 10(%r12)
	jne	.L362
.L309:
	movl	$1, %r10d
	movb	$0, 11(%r12)
	movq	(%r12), %rax
	movw	%r10w, 8(%r12)
.L310:
	movq	232(%rax), %rax
	leaq	_ZN6icu_678Calendar13computeFieldsER10UErrorCode(%rip), %rdx
	cmpq	%rdx, %rax
	je	.L330
	.p2align 4,,10
	.p2align 3
.L312:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	*%rax
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L308:
	movq	(%r12), %rax
.L314:
	movq	192(%rax), %rcx
	leaq	_ZNK6icu_678Calendar16getDayOfWeekTypeE19UCalendarDaysOfWeekR10UErrorCode(%rip), %rax
	xorl	%r13d, %r13d
	cmpq	%rax, %rcx
	je	.L339
.L315:
	movq	%r14, %rdx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	*%rcx
	movl	-60(%rbp), %ecx
	movl	%eax, %ebx
	testl	%ecx, %ecx
	jg	.L339
	movl	$1, %r8d
	cmpl	$1, %eax
	je	.L306
	leal	-2(%rax), %eax
	cmpl	$1, %eax
	ja	.L339
	movq	(%r12), %rax
	leaq	_ZNK6icu_678Calendar20getWeekendTransitionE19UCalendarDaysOfWeekR10UErrorCode(%rip), %rdx
	movl	96(%r12), %r15d
	movq	200(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L326
	cmpl	%r13d, 264(%r12)
	jne	.L327
.L331:
	movl	268(%r12), %eax
	jmp	.L332
	.p2align 4,,10
	.p2align 3
.L321:
	testb	%sil, %sil
	jne	.L355
	jmp	.L322
	.p2align 4,,10
	.p2align 3
.L362:
	cmpb	$0, 9(%r12)
	movq	(%r12), %rax
	movb	$1, 8(%r12)
	movb	$0, 11(%r12)
	je	.L310
	jmp	.L311
	.p2align 4,,10
	.p2align 3
.L360:
	movl	268(%r12), %esi
	testl	%esi, %esi
	je	.L306
	jmp	.L320
.L361:
	call	__stack_chk_fail@PLT
.L358:
	leaq	-60(%rbp), %r14
	jmp	.L315
	.cfi_endproc
.LFE3308:
	.size	_ZNK6icu_678Calendar9isWeekendEv, .-_ZNK6icu_678Calendar9isWeekendEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Calendar13validateFieldE19UCalendarDateFieldsR10UErrorCode
	.type	_ZN6icu_678Calendar13validateFieldE19UCalendarDateFieldsR10UErrorCode, @function
_ZN6icu_678Calendar13validateFieldE19UCalendarDateFieldsR10UErrorCode:
.LFB3321:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movslq	%esi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpl	$6, %r12d
	je	.L364
	cmpl	$8, %r12d
	je	.L365
	movq	(%rdi), %rax
	cmpl	$5, %r12d
	je	.L392
	movq	128(%rax), %rdx
	leaq	_ZNK6icu_678Calendar10getMaximumE19UCalendarDateFields(%rip), %rcx
	cmpq	%rcx, %rdx
	jne	.L379
	movl	$3, %edx
	movl	%r12d, %esi
	call	*256(%rax)
	movl	%eax, %r14d
.L380:
	movq	(%rbx), %rcx
	leaq	_ZNK6icu_678Calendar10getMinimumE19UCalendarDateFields(%rip), %rdx
	movq	112(%rcx), %rax
	cmpq	%rdx, %rax
	jne	.L381
	movq	256(%rcx), %rax
	leaq	_ZNK6icu_678Calendar8getLimitE19UCalendarDateFieldsNS0_10ELimitTypeE(%rip), %rdx
	cmpq	%rdx, %rax
	jne	.L382
	leal	-4(%r12), %eax
	cmpl	$18, %eax
	ja	.L383
	leaq	.L385(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L385:
	.long	.L386-.L385
	.long	.L383-.L385
	.long	.L383-.L385
	.long	.L384-.L385
	.long	.L383-.L385
	.long	.L384-.L385
	.long	.L384-.L385
	.long	.L384-.L385
	.long	.L384-.L385
	.long	.L384-.L385
	.long	.L384-.L385
	.long	.L384-.L385
	.long	.L384-.L385
	.long	.L383-.L385
	.long	.L384-.L385
	.long	.L383-.L385
	.long	.L384-.L385
	.long	.L384-.L385
	.long	.L384-.L385
	.text
	.p2align 4,,10
	.p2align 3
.L364:
	movq	(%rdi), %rax
	call	*288(%rax)
	leaq	_ZNK6icu_678Calendar19handleGetYearLengthEi(%rip), %rcx
	movl	%eax, %r14d
	movq	(%rbx), %rax
	movq	280(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L371
	leal	1(%r14), %esi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	*264(%rax)
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	%r14d, %esi
	movl	%eax, %r12d
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*264(%rax)
	subl	%eax, %r12d
	movl	%r12d, %eax
.L372:
	movl	36(%rbx), %edx
	testl	%edx, %edx
	jle	.L369
.L390:
	cmpl	%edx, %eax
	jl	.L369
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L365:
	.cfi_restore_state
	movl	44(%rdi), %eax
	testl	%eax, %eax
	jne	.L393
.L369:
	movl	$1, 0(%r13)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L392:
	.cfi_restore_state
	call	*288(%rax)
	leaq	_ZNK6icu_678Calendar20handleGetMonthLengthEii(%rip), %rdx
	movl	20(%rbx), %r15d
	movl	%eax, %r12d
	movq	(%rbx), %rax
	movq	272(%rax), %rcx
	cmpq	%rdx, %rcx
	jne	.L367
	leal	1(%r15), %edx
	movl	$1, %ecx
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	*264(%rax)
	movl	%r15d, %edx
	movl	%r12d, %esi
	movq	%rbx, %rdi
	movl	%eax, %r14d
	movq	(%rbx), %rax
	movl	$1, %ecx
	call	*264(%rax)
	subl	%eax, %r14d
	movl	%r14d, %eax
.L368:
	movl	32(%rbx), %edx
	testl	%edx, %edx
	jg	.L390
	jmp	.L369
.L384:
	movq	%r12, %rax
	leaq	_ZN6icu_67L15kCalendarLimitsE(%rip), %rdx
	salq	$4, %rax
	movl	(%rdx,%rax), %eax
.L387:
	movl	12(%rbx,%r12,4), %edx
	cmpl	%edx, %eax
	jg	.L369
	cmpl	%edx, %r14d
	jl	.L369
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L386:
	.cfi_restore_state
	xorl	%eax, %eax
	cmpb	$1, 260(%rbx)
	sete	%al
	jmp	.L387
.L383:
	xorl	%edx, %edx
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	*248(%rcx)
	jmp	.L387
	.p2align 4,,10
	.p2align 3
.L393:
	movq	(%rdi), %rax
	leaq	_ZNK6icu_678Calendar10getMaximumE19UCalendarDateFields(%rip), %rcx
	movq	128(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L374
	movl	$3, %edx
	movl	$8, %esi
	call	*256(%rax)
	movl	%eax, %r12d
.L375:
	movq	(%rbx), %rax
	leaq	_ZNK6icu_678Calendar10getMinimumE19UCalendarDateFields(%rip), %rcx
	movq	112(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L376
	movq	256(%rax), %rcx
	leaq	_ZNK6icu_678Calendar8getLimitE19UCalendarDateFieldsNS0_10ELimitTypeE(%rip), %rdx
	movl	$8, %esi
	movq	%rbx, %rdi
	cmpq	%rdx, %rcx
	movl	$0, %edx
	jne	.L377
	call	*248(%rax)
.L378:
	movl	44(%rbx), %edx
	cmpl	%edx, %eax
	jg	.L369
	cmpl	%edx, %r12d
	jl	.L369
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L379:
	.cfi_restore_state
	movl	%r12d, %esi
	call	*%rdx
	movl	%eax, %r14d
	jmp	.L380
	.p2align 4,,10
	.p2align 3
.L381:
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L387
	.p2align 4,,10
	.p2align 3
.L367:
	movl	%r15d, %edx
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	*%rcx
	jmp	.L368
	.p2align 4,,10
	.p2align 3
.L371:
	movl	%r14d, %esi
	movq	%rbx, %rdi
	call	*%rdx
	jmp	.L372
	.p2align 4,,10
	.p2align 3
.L382:
	xorl	%edx, %edx
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L387
	.p2align 4,,10
	.p2align 3
.L376:
	movl	$8, %esi
	movq	%rbx, %rdi
	call	*%rdx
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L374:
	movl	$8, %esi
	call	*%rdx
	movl	%eax, %r12d
	jmp	.L375
	.p2align 4,,10
	.p2align 3
.L377:
	call	*%rcx
	jmp	.L378
	.cfi_endproc
.LFE3321:
	.size	_ZN6icu_678Calendar13validateFieldE19UCalendarDateFieldsR10UErrorCode, .-_ZN6icu_678Calendar13validateFieldE19UCalendarDateFieldsR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_678CalendarD2Ev
	.type	_ZN6icu_678CalendarD2Ev, @function
_ZN6icu_678CalendarD2Ev:
.LFB3236:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_678CalendarE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	240(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L395
	movq	(%rdi), %rax
	call	*8(%rax)
.L395:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE3236:
	.size	_ZN6icu_678CalendarD2Ev, .-_ZN6icu_678CalendarD2Ev
	.globl	_ZN6icu_678CalendarD1Ev
	.set	_ZN6icu_678CalendarD1Ev,_ZN6icu_678CalendarD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_678CalendarD0Ev
	.type	_ZN6icu_678CalendarD0Ev, @function
_ZN6icu_678CalendarD0Ev:
.LFB3238:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_678CalendarE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	240(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L401
	movq	(%rdi), %rax
	call	*8(%rax)
.L401:
	movq	%r12, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3238:
	.size	_ZN6icu_678CalendarD0Ev, .-_ZN6icu_678CalendarD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_678CalendarC2ERKS0_
	.type	_ZN6icu_678CalendarC2ERKS0_, @function
_ZN6icu_678CalendarC2ERKS0_:
.LFB3240:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_678CalendarE(%rip), %rax
	movq	$0, 240(%rdi)
	movq	%rax, (%rdi)
	cmpq	%rsi, %rdi
	je	.L413
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movdqu	12(%rsi), %xmm1
	movq	%rdi, %rbx
	movups	%xmm1, 12(%rdi)
	movdqu	28(%rsi), %xmm2
	movups	%xmm2, 28(%rdi)
	movdqu	44(%rsi), %xmm3
	movups	%xmm3, 44(%rdi)
	movdqu	60(%rsi), %xmm4
	movups	%xmm4, 60(%rdi)
	movdqu	76(%rsi), %xmm5
	movups	%xmm5, 76(%rdi)
	movq	92(%rsi), %rcx
	movq	%rcx, 92(%rdi)
	movl	100(%rsi), %edx
	movl	%edx, 100(%rdi)
	movdqu	104(%rsi), %xmm6
	movups	%xmm6, 104(%rdi)
	movl	120(%rsi), %ecx
	movl	%ecx, 120(%rdi)
	movzwl	124(%rsi), %ecx
	movw	%cx, 124(%rdi)
	movzbl	126(%rsi), %edx
	movb	%dl, 126(%rdi)
	movdqu	128(%rsi), %xmm7
	movups	%xmm7, 128(%rdi)
	movdqu	144(%rsi), %xmm0
	movups	%xmm0, 144(%rdi)
	movdqu	160(%rsi), %xmm1
	movups	%xmm1, 160(%rdi)
	movdqu	176(%rsi), %xmm2
	movups	%xmm2, 176(%rdi)
	movdqu	192(%rsi), %xmm3
	movups	%xmm3, 192(%rdi)
	movq	208(%rsi), %rcx
	movq	%rcx, 208(%rdi)
	movl	216(%rsi), %edx
	movl	%edx, 216(%rdi)
	movl	8(%rsi), %eax
	movsd	224(%rsi), %xmm0
	movl	%eax, 8(%rdi)
	movzbl	232(%rsi), %eax
	movsd	%xmm0, 224(%rdi)
	movb	%al, 232(%rdi)
	movq	248(%rsi), %rax
	movq	%rax, 248(%rdi)
	movq	240(%rsi), %rdi
	testq	%rdi, %rdi
	je	.L408
	movq	(%rdi), %rax
	call	*96(%rax)
	movq	%rax, 240(%rbx)
.L408:
	movl	256(%r12), %eax
	leaq	296(%r12), %rsi
	movl	$157, %edx
	leaq	296(%rbx), %rdi
	movl	%eax, 256(%rbx)
	movzbl	260(%r12), %eax
	movb	%al, 260(%rbx)
	movq	264(%r12), %rax
	movq	%rax, 264(%rbx)
	movq	272(%r12), %rax
	movq	%rax, 272(%rbx)
	movl	220(%r12), %eax
	movl	%eax, 220(%rbx)
	call	strncpy@PLT
	leaq	453(%r12), %rsi
	movl	$157, %edx
	leaq	453(%rbx), %rdi
	call	strncpy@PLT
	movb	$0, 452(%rbx)
	movb	$0, 609(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L413:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE3240:
	.size	_ZN6icu_678CalendarC2ERKS0_, .-_ZN6icu_678CalendarC2ERKS0_
	.globl	_ZN6icu_678CalendarC1ERKS0_
	.set	_ZN6icu_678CalendarC1ERKS0_,_ZN6icu_678CalendarC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_678CalendaraSERKS0_
	.type	_ZN6icu_678CalendaraSERKS0_, @function
_ZN6icu_678CalendaraSERKS0_:
.LFB3242:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpq	%rsi, %rdi
	je	.L417
	movdqu	12(%rsi), %xmm1
	movq	%rsi, %rbx
	movups	%xmm1, 12(%rdi)
	movdqu	28(%rsi), %xmm2
	movups	%xmm2, 28(%rdi)
	movdqu	44(%rsi), %xmm3
	movups	%xmm3, 44(%rdi)
	movdqu	60(%rsi), %xmm4
	movups	%xmm4, 60(%rdi)
	movdqu	76(%rsi), %xmm5
	movups	%xmm5, 76(%rdi)
	movq	92(%rsi), %rcx
	movq	%rcx, 92(%rdi)
	movl	100(%rsi), %edx
	movl	%edx, 100(%rdi)
	movdqu	104(%rsi), %xmm6
	movups	%xmm6, 104(%rdi)
	movl	120(%rsi), %ecx
	movl	%ecx, 120(%rdi)
	movzwl	124(%rsi), %ecx
	movw	%cx, 124(%rdi)
	movzbl	126(%rsi), %edx
	movb	%dl, 126(%rdi)
	movdqu	128(%rsi), %xmm7
	movups	%xmm7, 128(%rdi)
	movdqu	144(%rsi), %xmm0
	movups	%xmm0, 144(%rdi)
	movdqu	160(%rsi), %xmm1
	movups	%xmm1, 160(%rdi)
	movdqu	176(%rsi), %xmm2
	movups	%xmm2, 176(%rdi)
	movdqu	192(%rsi), %xmm3
	movups	%xmm3, 192(%rdi)
	movq	208(%rsi), %rcx
	movq	%rcx, 208(%rdi)
	movl	216(%rsi), %edx
	movl	%edx, 216(%rdi)
	movsd	224(%rsi), %xmm0
	movsd	%xmm0, 224(%rdi)
	movzbl	8(%rsi), %eax
	movb	%al, 8(%rdi)
	movzbl	10(%rsi), %eax
	movb	%al, 10(%rdi)
	movzbl	9(%rsi), %eax
	movb	%al, 9(%rdi)
	movzbl	11(%rsi), %eax
	movb	%al, 11(%rdi)
	movzbl	232(%rsi), %eax
	movb	%al, 232(%rdi)
	movl	248(%rsi), %eax
	movl	%eax, 248(%rdi)
	movl	252(%rsi), %eax
	movl	%eax, 252(%rdi)
	movq	240(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L418
	movq	(%rdi), %rax
	call	*8(%rax)
.L418:
	movq	$0, 240(%r12)
	movq	240(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L419
	movq	(%rdi), %rax
	call	*96(%rax)
	movq	%rax, 240(%r12)
.L419:
	movl	256(%rbx), %eax
	leaq	296(%rbx), %rsi
	movl	$157, %edx
	leaq	296(%r12), %rdi
	movl	%eax, 256(%r12)
	movzbl	260(%rbx), %eax
	movb	%al, 260(%r12)
	movl	264(%rbx), %eax
	movl	%eax, 264(%r12)
	movl	268(%rbx), %eax
	movl	%eax, 268(%r12)
	movl	272(%rbx), %eax
	movl	%eax, 272(%r12)
	movl	276(%rbx), %eax
	movl	%eax, 276(%r12)
	movl	220(%rbx), %eax
	movl	%eax, 220(%r12)
	call	strncpy@PLT
	leaq	453(%rbx), %rsi
	movl	$157, %edx
	leaq	453(%r12), %rdi
	call	strncpy@PLT
	movb	$0, 452(%r12)
	movb	$0, 609(%r12)
.L417:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3242:
	.size	_ZN6icu_678CalendaraSERKS0_, .-_ZN6icu_678CalendaraSERKS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678Calendar6equalsERKS0_R10UErrorCode
	.type	_ZNK6icu_678Calendar6equalsERKS0_R10UErrorCode, @function
_ZNK6icu_678Calendar6equalsERKS0_R10UErrorCode:
.LFB3254:
	.cfi_startproc
	endbr64
	cmpq	%rsi, %rdi
	je	.L442
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 12, -32
	movl	(%rdx), %r8d
	testl	%r8d, %r8d
	jg	.L428
	cmpb	$0, 8(%rdi)
	movq	%rsi, %r12
	je	.L445
.L430:
	cmpb	$0, 8(%r12)
	movsd	224(%rdi), %xmm1
	jne	.L440
	movq	(%r12), %rax
	movsd	%xmm1, -24(%rbp)
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*224(%rax)
	movl	0(%r13), %edx
	movsd	-24(%rbp), %xmm1
	testl	%edx, %edx
	jg	.L437
	cmpb	$0, 232(%r12)
	je	.L446
.L435:
	movb	$0, 9(%r12)
.L436:
	movb	$1, 8(%r12)
	movb	$0, 11(%r12)
	movl	0(%r13), %eax
	testl	%eax, %eax
	jle	.L440
.L437:
	pxor	%xmm0, %xmm0
	jmp	.L434
	.p2align 4,,10
	.p2align 3
.L445:
	movq	(%rdi), %rax
	movq	%rdx, %rsi
	movq	%rdi, -24(%rbp)
	call	*224(%rax)
	movl	0(%r13), %esi
	testl	%esi, %esi
	jg	.L428
	movq	-24(%rbp), %rdi
	cmpb	$0, 232(%rdi)
	je	.L447
.L431:
	movb	$0, 9(%rdi)
.L432:
	movb	$1, 8(%rdi)
	movb	$0, 11(%rdi)
	movl	0(%r13), %ecx
	testl	%ecx, %ecx
	jle	.L430
.L428:
	addq	$16, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L440:
	.cfi_restore_state
	movsd	224(%r12), %xmm0
.L434:
	ucomisd	%xmm1, %xmm0
	movl	$0, %edx
	setnp	%al
	cmovne	%edx, %eax
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L442:
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L447:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	cmpb	$0, 10(%rdi)
	jne	.L432
	jmp	.L431
	.p2align 4,,10
	.p2align 3
.L446:
	cmpb	$0, 10(%r12)
	jne	.L436
	jmp	.L435
	.cfi_endproc
.LFE3254:
	.size	_ZNK6icu_678Calendar6equalsERKS0_R10UErrorCode, .-_ZNK6icu_678Calendar6equalsERKS0_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678Calendar6beforeERKS0_R10UErrorCode
	.type	_ZNK6icu_678Calendar6beforeERKS0_R10UErrorCode, @function
_ZNK6icu_678Calendar6beforeERKS0_R10UErrorCode:
.LFB3255:
	.cfi_startproc
	endbr64
	cmpq	%rsi, %rdi
	je	.L463
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 12, -32
	movl	(%rdx), %r8d
	testl	%r8d, %r8d
	jg	.L449
	cmpb	$0, 8(%rdi)
	movq	%rsi, %r12
	je	.L466
.L451:
	cmpb	$0, 8(%r12)
	movsd	224(%rdi), %xmm1
	jne	.L461
	movq	(%r12), %rax
	movsd	%xmm1, -24(%rbp)
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*224(%rax)
	movl	0(%r13), %edx
	movsd	-24(%rbp), %xmm1
	testl	%edx, %edx
	jg	.L458
	cmpb	$0, 232(%r12)
	je	.L467
.L456:
	movb	$0, 9(%r12)
.L457:
	movb	$1, 8(%r12)
	movb	$0, 11(%r12)
	movl	0(%r13), %eax
	testl	%eax, %eax
	jle	.L461
.L458:
	pxor	%xmm0, %xmm0
	jmp	.L455
	.p2align 4,,10
	.p2align 3
.L466:
	movq	(%rdi), %rax
	movq	%rdx, %rsi
	movq	%rdi, -24(%rbp)
	call	*224(%rax)
	movl	0(%r13), %esi
	testl	%esi, %esi
	jg	.L449
	movq	-24(%rbp), %rdi
	cmpb	$0, 232(%rdi)
	je	.L468
.L452:
	movb	$0, 9(%rdi)
.L453:
	movb	$1, 8(%rdi)
	movb	$0, 11(%rdi)
	movl	0(%r13), %ecx
	testl	%ecx, %ecx
	jle	.L451
.L449:
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L461:
	.cfi_restore_state
	movsd	224(%r12), %xmm0
.L455:
	comisd	%xmm1, %xmm0
	seta	%al
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L463:
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L468:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	cmpb	$0, 10(%rdi)
	jne	.L453
	jmp	.L452
	.p2align 4,,10
	.p2align 3
.L467:
	cmpb	$0, 10(%r12)
	jne	.L457
	jmp	.L456
	.cfi_endproc
.LFE3255:
	.size	_ZNK6icu_678Calendar6beforeERKS0_R10UErrorCode, .-_ZNK6icu_678Calendar6beforeERKS0_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678Calendar5afterERKS0_R10UErrorCode
	.type	_ZNK6icu_678Calendar5afterERKS0_R10UErrorCode, @function
_ZNK6icu_678Calendar5afterERKS0_R10UErrorCode:
.LFB3256:
	.cfi_startproc
	endbr64
	cmpq	%rsi, %rdi
	je	.L484
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 12, -32
	movl	(%rdx), %r8d
	testl	%r8d, %r8d
	jg	.L470
	cmpb	$0, 8(%rdi)
	movq	%rsi, %r12
	je	.L487
.L472:
	cmpb	$0, 8(%r12)
	movsd	224(%rdi), %xmm1
	jne	.L482
	movq	(%r12), %rax
	movsd	%xmm1, -24(%rbp)
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*224(%rax)
	movl	0(%r13), %edx
	movsd	-24(%rbp), %xmm1
	testl	%edx, %edx
	jg	.L479
	cmpb	$0, 232(%r12)
	je	.L488
.L477:
	movb	$0, 9(%r12)
.L478:
	movb	$1, 8(%r12)
	movb	$0, 11(%r12)
	movl	0(%r13), %eax
	testl	%eax, %eax
	jle	.L482
.L479:
	pxor	%xmm0, %xmm0
	jmp	.L476
	.p2align 4,,10
	.p2align 3
.L487:
	movq	(%rdi), %rax
	movq	%rdx, %rsi
	movq	%rdi, -24(%rbp)
	call	*224(%rax)
	movl	0(%r13), %esi
	testl	%esi, %esi
	jg	.L470
	movq	-24(%rbp), %rdi
	cmpb	$0, 232(%rdi)
	je	.L489
.L473:
	movb	$0, 9(%rdi)
.L474:
	movb	$1, 8(%rdi)
	movb	$0, 11(%rdi)
	movl	0(%r13), %ecx
	testl	%ecx, %ecx
	jle	.L472
.L470:
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L482:
	.cfi_restore_state
	movsd	224(%r12), %xmm0
.L476:
	comisd	%xmm0, %xmm1
	seta	%al
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L484:
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L489:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	cmpb	$0, 10(%rdi)
	jne	.L474
	jmp	.L473
	.p2align 4,,10
	.p2align 3
.L488:
	cmpb	$0, 10(%r12)
	jne	.L478
	jmp	.L477
	.cfi_endproc
.LFE3256:
	.size	_ZNK6icu_678Calendar5afterERKS0_R10UErrorCode, .-_ZNK6icu_678Calendar5afterERKS0_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Calendar19getAvailableLocalesERi
	.type	_ZN6icu_678Calendar19getAvailableLocalesERi, @function
_ZN6icu_678Calendar19getAvailableLocalesERi:
.LFB3257:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_676Locale19getAvailableLocalesERi@PLT
	.cfi_endproc
.LFE3257:
	.size	_ZN6icu_678Calendar19getAvailableLocalesERi, .-_ZN6icu_678Calendar19getAvailableLocalesERi
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Calendar25getKeywordValuesForLocaleEPKcRKNS_6LocaleEaR10UErrorCode
	.type	_ZN6icu_678Calendar25getKeywordValuesForLocaleEPKcRKNS_6LocaleEaR10UErrorCode, @function
_ZN6icu_678Calendar25getKeywordValuesForLocaleEPKcRKNS_6LocaleEaR10UErrorCode:
.LFB3258:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movsbl	%dl, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rcx, %rbx
	subq	$8, %rsp
	movq	40(%rsi), %rsi
	call	ucal_getKeywordValuesForLocale_67@PLT
	movq	%rax, %r13
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L496
	movl	$128, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L494
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6718UStringEnumerationC1EP12UEnumeration@PLT
.L491:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L496:
	.cfi_restore_state
	movq	%r13, %rdi
	xorl	%r12d, %r12d
	call	uenum_close_67@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L494:
	.cfi_restore_state
	movl	$7, (%rbx)
	jmp	.L491
	.cfi_endproc
.LFE3258:
	.size	_ZN6icu_678Calendar25getKeywordValuesForLocaleEPKcRKNS_6LocaleEaR10UErrorCode, .-_ZN6icu_678Calendar25getKeywordValuesForLocaleEPKcRKNS_6LocaleEaR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Calendar6getNowEv
	.type	_ZN6icu_678Calendar6getNowEv, @function
_ZN6icu_678Calendar6getNowEv:
.LFB3259:
	.cfi_startproc
	endbr64
	jmp	uprv_getUTCtime_67@PLT
	.cfi_endproc
.LFE3259:
	.size	_ZN6icu_678Calendar6getNowEv, .-_ZN6icu_678Calendar6getNowEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678Calendar15getTimeInMillisER10UErrorCode
	.type	_ZNK6icu_678Calendar15getTimeInMillisER10UErrorCode, @function
_ZNK6icu_678Calendar15getTimeInMillisER10UErrorCode:
.LFB3260:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	testl	%ecx, %ecx
	jg	.L509
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	cmpb	$0, 8(%rdi)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	je	.L501
.L505:
	movsd	224(%rbx), %xmm0
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L509:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	pxor	%xmm0, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L501:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movq	(%rdi), %rax
	movq	%rsi, %r12
	call	*224(%rax)
	movl	(%r12), %edx
	testl	%edx, %edx
	jg	.L502
	cmpb	$0, 232(%rbx)
	jne	.L503
	cmpb	$0, 10(%rbx)
	je	.L503
.L504:
	movb	$1, 8(%rbx)
	movb	$0, 11(%rbx)
	movl	(%r12), %eax
	testl	%eax, %eax
	jle	.L505
.L502:
	popq	%rbx
	pxor	%xmm0, %xmm0
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L503:
	.cfi_restore_state
	movb	$0, 9(%rbx)
	jmp	.L504
	.cfi_endproc
.LFE3260:
	.size	_ZNK6icu_678Calendar15getTimeInMillisER10UErrorCode, .-_ZNK6icu_678Calendar15getTimeInMillisER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Calendar15setTimeInMillisEdR10UErrorCode
	.type	_ZN6icu_678Calendar15setTimeInMillisEdR10UErrorCode, @function
_ZN6icu_678Calendar15setTimeInMillisEdR10UErrorCode:
.LFB3261:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	movq	%rdi, %rdx
	testl	%eax, %eax
	jg	.L510
	comisd	.LC5(%rip), %xmm0
	jbe	.L521
	movq	.LC5(%rip), %rax
	cmpb	$0, 232(%rdi)
	movq	%rax, %xmm0
	je	.L517
.L514:
	leaq	20(%rdx), %rdi
	movl	%edx, %ecx
	xorl	%eax, %eax
	movl	$16777217, 8(%rdx)
	andq	$-8, %rdi
	movq	$0, 12(%rdx)
	subl	%edi, %ecx
	movq	$0, 119(%rdx)
	addl	$127, %ecx
	movsd	%xmm0, 224(%rdx)
	shrl	$3, %ecx
	rep stosq
	leaq	136(%rdx), %rdi
	movq	$0, 128(%rdx)
	movq	$0, 212(%rdx)
	andq	$-8, %rdi
	subl	%edi, %edx
	leal	220(%rdx), %ecx
	shrl	$3, %ecx
	rep stosq
.L510:
	ret
	.p2align 4,,10
	.p2align 3
.L521:
	movsd	.LC6(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jbe	.L514
	cmpb	$0, 232(%rdi)
	je	.L517
	movapd	%xmm1, %xmm0
	jmp	.L514
	.p2align 4,,10
	.p2align 3
.L517:
	movl	$1, (%rsi)
	ret
	.cfi_endproc
.LFE3261:
	.size	_ZN6icu_678Calendar15setTimeInMillisEdR10UErrorCode, .-_ZN6icu_678Calendar15setTimeInMillisEdR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678Calendar3getE19UCalendarDateFieldsR10UErrorCode
	.type	_ZNK6icu_678Calendar3getE19UCalendarDateFieldsR10UErrorCode, @function
_ZNK6icu_678Calendar3getE19UCalendarDateFieldsR10UErrorCode:
.LFB3262:
	.cfi_startproc
	endbr64
	movl	(%rdx), %r8d
	testl	%r8d, %r8d
	jle	.L537
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L537:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	subq	$16, %rsp
	cmpb	$0, 8(%rdi)
	je	.L538
.L524:
	cmpb	$0, 9(%rdi)
	jne	.L532
	movq	(%rdi), %rax
	leaq	_ZN6icu_678Calendar13computeFieldsER10UErrorCode(%rip), %rdx
	movq	%rdi, -24(%rbp)
	movq	%r12, %rsi
	movq	232(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L527
	call	_ZN6icu_678Calendar13computeFieldsER10UErrorCode.part.0
	movq	-24(%rbp), %rdi
.L528:
	movl	(%r12), %ecx
	testl	%ecx, %ecx
	jg	.L529
	movl	$257, %eax
	movw	%ax, 9(%rdi)
	movl	(%r12), %edx
	testl	%edx, %edx
	jg	.L529
.L532:
	movslq	%r13d, %rsi
	movl	12(%rdi,%rsi,4), %eax
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L538:
	.cfi_restore_state
	movq	(%rdi), %rax
	movq	%rdi, -24(%rbp)
	movq	%rdx, %rsi
	call	*224(%rax)
	movl	(%r12), %edi
	testl	%edi, %edi
	jg	.L529
	movq	-24(%rbp), %rdi
	cmpb	$0, 232(%rdi)
	jne	.L525
	cmpb	$0, 10(%rdi)
	jne	.L526
.L525:
	movb	$0, 9(%rdi)
.L526:
	movb	$1, 8(%rdi)
	movb	$0, 11(%rdi)
	movl	(%r12), %esi
	testl	%esi, %esi
	jle	.L524
.L529:
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L527:
	.cfi_restore_state
	call	*%rax
	movq	-24(%rbp), %rdi
	jmp	.L528
	.cfi_endproc
.LFE3262:
	.size	_ZNK6icu_678Calendar3getE19UCalendarDateFieldsR10UErrorCode, .-_ZNK6icu_678Calendar3getE19UCalendarDateFieldsR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Calendar3setE19UCalendarDateFieldsi
	.type	_ZN6icu_678Calendar3setE19UCalendarDateFieldsi, @function
_ZN6icu_678Calendar3setE19UCalendarDateFieldsi:
.LFB3263:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 11(%rdi)
	je	.L540
	movq	(%rdi), %rax
	leaq	_ZN6icu_678Calendar13computeFieldsER10UErrorCode(%rip), %rdx
	movl	$0, -44(%rbp)
	leaq	-44(%rbp), %rsi
	movq	232(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L541
	call	_ZN6icu_678Calendar13computeFieldsER10UErrorCode.part.0
.L540:
	movslq	%r12d, %rsi
	leaq	(%rbx,%rsi,4), %r9
	movl	%r13d, 12(%r9)
	movl	220(%rbx), %eax
	cmpl	$10000, %eax
	je	.L543
	leal	1(%rax), %edi
.L544:
	movl	%edi, 220(%rbx)
	movl	%eax, 128(%r9)
	xorl	%eax, %eax
	movb	$1, 104(%rbx,%rsi)
	movb	$0, 11(%rbx)
	movw	%ax, 8(%rbx)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L561
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L543:
	.cfi_restore_state
	movl	$1, 220(%rbx)
	movl	$1, %ecx
	.p2align 4,,10
	.p2align 3
.L549:
	movl	$10000, %edi
	xorl	%eax, %eax
	movl	$-1, %edx
	.p2align 4,,10
	.p2align 3
.L547:
	movl	128(%rbx,%rax,4), %r8d
	cmpl	%ecx, %r8d
	jle	.L545
	cmpl	%edi, %r8d
	cmovl	%r8d, %edi
	cmovl	%eax, %edx
.L545:
	addq	$1, %rax
	cmpq	$23, %rax
	jne	.L547
	leal	1(%rcx), %eax
	leal	2(%rcx), %edi
	cmpl	$-1, %edx
	je	.L544
	movslq	%edx, %rdx
	movl	%eax, 220(%rbx)
	movl	%eax, 128(%rbx,%rdx,4)
	cmpl	$24, %eax
	je	.L562
	movl	%eax, %ecx
	jmp	.L549
	.p2align 4,,10
	.p2align 3
.L541:
	call	*%rax
	jmp	.L540
	.p2align 4,,10
	.p2align 3
.L562:
	movl	%edi, %eax
	leal	3(%rcx), %edi
	jmp	.L544
.L561:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3263:
	.size	_ZN6icu_678Calendar3setE19UCalendarDateFieldsi, .-_ZN6icu_678Calendar3setE19UCalendarDateFieldsi
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Calendar3setEiii
	.type	_ZN6icu_678Calendar3setEiii, @function
_ZN6icu_678Calendar3setEiii:
.LFB3264:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movl	%ecx, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 11(%rdi)
	je	.L564
	movq	(%rdi), %rax
	leaq	_ZN6icu_678Calendar13computeFieldsER10UErrorCode(%rip), %rdx
	movl	$0, -44(%rbp)
	leaq	-44(%rbp), %rsi
	movq	232(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L565
	call	_ZN6icu_678Calendar13computeFieldsER10UErrorCode.part.0
.L564:
	movl	220(%rbx), %eax
	movl	%r13d, 16(%rbx)
	cmpl	$10000, %eax
	je	.L567
	xorl	%edx, %edx
	movl	%eax, 132(%rbx)
	leal	1(%rax), %ecx
	movb	$1, 105(%rbx)
	movb	$0, 11(%rbx)
	movw	%dx, 8(%rbx)
	movl	%r14d, 20(%rbx)
	cmpl	$9999, %eax
	je	.L617
.L569:
	movl	%ecx, 136(%rbx)
	leal	1(%rcx), %eax
	movb	$1, 106(%rbx)
	movl	%r12d, 32(%rbx)
	cmpl	$9999, %ecx
	je	.L618
.L576:
	leal	1(%rax), %esi
.L582:
	movl	%esi, 220(%rbx)
	movl	%eax, 148(%rbx)
	movb	$1, 109(%rbx)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L619
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L567:
	.cfi_restore_state
	movl	$1, 220(%rbx)
	movl	$1, %edi
	.p2align 4,,10
	.p2align 3
.L574:
	movl	$10000, %edx
	xorl	%esi, %esi
	movl	$-1, %eax
	.p2align 4,,10
	.p2align 3
.L572:
	movl	128(%rbx,%rsi,4), %ecx
	cmpl	%edi, %ecx
	jle	.L570
	cmpl	%edx, %ecx
	cmovl	%ecx, %edx
	cmovl	%esi, %eax
.L570:
	addq	$1, %rsi
	cmpq	$23, %rsi
	jne	.L572
	leal	1(%rdi), %esi
	leal	2(%rdi), %ecx
	cmpl	$-1, %eax
	je	.L573
	cltq
	movl	%esi, 220(%rbx)
	movl	%esi, 128(%rbx,%rax,4)
	cmpl	$24, %esi
	je	.L620
	movl	%esi, %edi
	jmp	.L574
	.p2align 4,,10
	.p2align 3
.L618:
	movl	$1, 220(%rbx)
	movl	$1, %ecx
	.p2align 4,,10
	.p2align 3
.L587:
	movl	$10000, %esi
	xorl	%eax, %eax
	movl	$-1, %edx
	.p2align 4,,10
	.p2align 3
.L585:
	movl	128(%rbx,%rax,4), %edi
	cmpl	%edi, %ecx
	jge	.L583
	cmpl	%esi, %edi
	cmovl	%edi, %esi
	cmovl	%eax, %edx
.L583:
	addq	$1, %rax
	cmpq	$23, %rax
	jne	.L585
	leal	1(%rcx), %eax
	leal	2(%rcx), %esi
	cmpl	$-1, %edx
	je	.L582
	movslq	%edx, %rdx
	movl	%eax, 220(%rbx)
	movl	%eax, 128(%rbx,%rdx,4)
	cmpl	$24, %eax
	je	.L621
	movl	%eax, %ecx
	jmp	.L587
	.p2align 4,,10
	.p2align 3
.L617:
	movl	$1, 220(%rbx)
	movl	$1, %ecx
	.p2align 4,,10
	.p2align 3
.L581:
	movl	$10000, %esi
	xorl	%eax, %eax
	movl	$-1, %edx
	.p2align 4,,10
	.p2align 3
.L579:
	movl	128(%rbx,%rax,4), %edi
	cmpl	%ecx, %edi
	jle	.L577
	cmpl	%esi, %edi
	cmovl	%edi, %esi
	cmovl	%eax, %edx
.L577:
	addq	$1, %rax
	cmpq	$23, %rax
	jne	.L579
	leal	1(%rcx), %esi
	leal	2(%rcx), %eax
	cmpl	$-1, %edx
	je	.L580
	movslq	%edx, %rdx
	movl	%esi, 220(%rbx)
	movl	%esi, 128(%rbx,%rdx,4)
	cmpl	$24, %esi
	je	.L622
	movl	%esi, %ecx
	jmp	.L581
	.p2align 4,,10
	.p2align 3
.L620:
	movl	%ecx, %esi
	leal	3(%rdi), %ecx
.L573:
	xorl	%eax, %eax
	movl	%esi, 132(%rbx)
	movb	$1, 105(%rbx)
	movb	$0, 11(%rbx)
	movw	%ax, 8(%rbx)
	movl	%r14d, 20(%rbx)
	jmp	.L569
	.p2align 4,,10
	.p2align 3
.L622:
	movl	%eax, %esi
	leal	3(%rcx), %eax
.L580:
	movl	%esi, 136(%rbx)
	movb	$1, 106(%rbx)
	movl	%r12d, 32(%rbx)
	jmp	.L576
	.p2align 4,,10
	.p2align 3
.L565:
	call	*%rax
	jmp	.L564
	.p2align 4,,10
	.p2align 3
.L621:
	movl	%esi, %eax
	leal	3(%rcx), %esi
	jmp	.L582
.L619:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3264:
	.size	_ZN6icu_678Calendar3setEiii, .-_ZN6icu_678Calendar3setEiii
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Calendar3setEiiiii
	.type	_ZN6icu_678Calendar3setEiiiii, @function
_ZN6icu_678Calendar3setEiiiii:
.LFB3265:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%r8d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movl	%r9d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rdi
	movq	%rdi, -56(%rbp)
	xorl	%edi, %edi
	cmpb	$0, 11(%rbx)
	je	.L624
	movq	(%rbx), %rdx
	leaq	_ZN6icu_678Calendar13computeFieldsER10UErrorCode(%rip), %rcx
	movl	%esi, -68(%rbp)
	movq	%rbx, %rdi
	movl	$0, -60(%rbp)
	leaq	-60(%rbp), %rsi
	movq	232(%rdx), %rdx
	cmpq	%rcx, %rdx
	jne	.L625
	call	_ZN6icu_678Calendar13computeFieldsER10UErrorCode.part.0
	movl	-68(%rbp), %eax
.L624:
	movl	%eax, 16(%rbx)
	movl	220(%rbx), %eax
	cmpl	$10000, %eax
	je	.L627
	xorl	%ecx, %ecx
	movl	%eax, 132(%rbx)
	leal	1(%rax), %edx
	movb	$1, 105(%rbx)
	movb	$0, 11(%rbx)
	movw	%cx, 8(%rbx)
	movl	%r15d, 20(%rbx)
	cmpl	$9999, %eax
	je	.L709
.L629:
	movl	%edx, 136(%rbx)
	leal	1(%rdx), %esi
	movb	$1, 106(%rbx)
	movl	%r14d, 32(%rbx)
	cmpl	$9999, %edx
	je	.L710
.L636:
	movl	%esi, 148(%rbx)
	leal	1(%rsi), %ecx
	movb	$1, 109(%rbx)
	movl	%r13d, 56(%rbx)
	cmpl	$9999, %esi
	je	.L711
.L643:
	movl	%ecx, 172(%rbx)
	leal	1(%rcx), %eax
	movb	$1, 115(%rbx)
	movl	%r12d, 60(%rbx)
	cmpl	$9999, %ecx
	je	.L712
.L650:
	leal	1(%rax), %esi
.L656:
	movl	%esi, 220(%rbx)
	movl	%eax, 176(%rbx)
	movb	$1, 116(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L713
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L712:
	.cfi_restore_state
	movl	$1, 220(%rbx)
	movl	$1, %ecx
	.p2align 4,,10
	.p2align 3
.L661:
	movl	$10000, %esi
	xorl	%eax, %eax
	movl	$-1, %edx
	.p2align 4,,10
	.p2align 3
.L659:
	movl	128(%rbx,%rax,4), %edi
	cmpl	%edi, %ecx
	jge	.L657
	cmpl	%esi, %edi
	cmovl	%edi, %esi
	cmovl	%eax, %edx
.L657:
	addq	$1, %rax
	cmpq	$23, %rax
	jne	.L659
	leal	1(%rcx), %eax
	leal	2(%rcx), %esi
	cmpl	$-1, %edx
	je	.L656
	movslq	%edx, %rdx
	movl	%eax, 220(%rbx)
	movl	%eax, 128(%rbx,%rdx,4)
	cmpl	$24, %eax
	je	.L714
	movl	%eax, %ecx
	jmp	.L661
	.p2align 4,,10
	.p2align 3
.L711:
	movl	$1, 220(%rbx)
	movl	$1, %esi
	.p2align 4,,10
	.p2align 3
.L655:
	movl	$10000, %ecx
	xorl	%eax, %eax
	movl	$-1, %edx
	.p2align 4,,10
	.p2align 3
.L653:
	movl	128(%rbx,%rax,4), %edi
	cmpl	%esi, %edi
	jle	.L651
	cmpl	%ecx, %edi
	cmovl	%edi, %ecx
	cmovl	%eax, %edx
.L651:
	addq	$1, %rax
	cmpq	$23, %rax
	jne	.L653
	leal	1(%rsi), %ecx
	leal	2(%rsi), %eax
	cmpl	$-1, %edx
	je	.L654
	movslq	%edx, %rdx
	movl	%ecx, 220(%rbx)
	movl	%ecx, 128(%rbx,%rdx,4)
	cmpl	$24, %ecx
	je	.L715
	movl	%ecx, %esi
	jmp	.L655
	.p2align 4,,10
	.p2align 3
.L627:
	movl	$1, 220(%rbx)
	movl	$1, %edi
	.p2align 4,,10
	.p2align 3
.L634:
	movl	$10000, %edx
	xorl	%esi, %esi
	movl	$-1, %eax
	.p2align 4,,10
	.p2align 3
.L632:
	movl	128(%rbx,%rsi,4), %ecx
	cmpl	%edx, %ecx
	jge	.L630
	cmpl	%edi, %ecx
	cmovg	%ecx, %edx
	cmovg	%esi, %eax
.L630:
	addq	$1, %rsi
	cmpq	$23, %rsi
	jne	.L632
	leal	1(%rdi), %ecx
	leal	2(%rdi), %edx
	cmpl	$-1, %eax
	je	.L633
	cltq
	movl	%ecx, 220(%rbx)
	movl	%ecx, 128(%rbx,%rax,4)
	cmpl	$24, %ecx
	je	.L716
	movl	%ecx, %edi
	jmp	.L634
	.p2align 4,,10
	.p2align 3
.L710:
	movl	$1, 220(%rbx)
	movl	$1, %esi
	.p2align 4,,10
	.p2align 3
.L648:
	movl	$10000, %ecx
	xorl	%edx, %edx
	movl	$-1, %eax
	.p2align 4,,10
	.p2align 3
.L646:
	movl	128(%rbx,%rdx,4), %edi
	cmpl	%edi, %esi
	jge	.L644
	cmpl	%ecx, %edi
	cmovl	%edi, %ecx
	cmovl	%edx, %eax
.L644:
	addq	$1, %rdx
	cmpq	$23, %rdx
	jne	.L646
	leal	1(%rsi), %edx
	leal	2(%rsi), %ecx
	cmpl	$-1, %eax
	je	.L647
	cltq
	movl	%edx, 220(%rbx)
	movl	%edx, 128(%rbx,%rax,4)
	cmpl	$24, %edx
	je	.L717
	movl	%edx, %esi
	jmp	.L648
	.p2align 4,,10
	.p2align 3
.L715:
	movl	%eax, %ecx
	leal	3(%rsi), %eax
.L654:
	movl	%ecx, 172(%rbx)
	movb	$1, 115(%rbx)
	movl	%r12d, 60(%rbx)
	jmp	.L650
	.p2align 4,,10
	.p2align 3
.L709:
	movl	$1, 220(%rbx)
	movl	$1, %ecx
	.p2align 4,,10
	.p2align 3
.L641:
	movl	$10000, %esi
	xorl	%edx, %edx
	movl	$-1, %eax
	.p2align 4,,10
	.p2align 3
.L639:
	movl	128(%rbx,%rdx,4), %edi
	cmpl	%ecx, %edi
	jle	.L637
	cmpl	%esi, %edi
	cmovl	%edi, %esi
	cmovl	%edx, %eax
.L637:
	addq	$1, %rdx
	cmpq	$23, %rdx
	jne	.L639
	leal	1(%rcx), %edx
	leal	2(%rcx), %esi
	cmpl	$-1, %eax
	je	.L640
	cltq
	movl	%edx, 220(%rbx)
	movl	%edx, 128(%rbx,%rax,4)
	cmpl	$24, %edx
	je	.L718
	movl	%edx, %ecx
	jmp	.L641
	.p2align 4,,10
	.p2align 3
.L716:
	movl	%edx, %ecx
	leal	3(%rdi), %edx
.L633:
	xorl	%eax, %eax
	movl	%ecx, 132(%rbx)
	movb	$1, 105(%rbx)
	movb	$0, 11(%rbx)
	movw	%ax, 8(%rbx)
	movl	%r15d, 20(%rbx)
	jmp	.L629
	.p2align 4,,10
	.p2align 3
.L718:
	movl	%esi, %edx
	leal	3(%rcx), %esi
.L640:
	movl	%edx, 136(%rbx)
	movb	$1, 106(%rbx)
	movl	%r14d, 32(%rbx)
	jmp	.L636
	.p2align 4,,10
	.p2align 3
.L717:
	movl	%ecx, %edx
	leal	3(%rsi), %ecx
.L647:
	movl	%edx, 148(%rbx)
	movb	$1, 109(%rbx)
	movl	%r13d, 56(%rbx)
	jmp	.L643
	.p2align 4,,10
	.p2align 3
.L625:
	call	*%rdx
	movl	-68(%rbp), %eax
	jmp	.L624
	.p2align 4,,10
	.p2align 3
.L714:
	movl	%esi, %eax
	leal	3(%rcx), %esi
	jmp	.L656
.L713:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3265:
	.size	_ZN6icu_678Calendar3setEiiiii, .-_ZN6icu_678Calendar3setEiiiii
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Calendar3setEiiiiii
	.type	_ZN6icu_678Calendar3setEiiiiii, @function
_ZN6icu_678Calendar3setEiiiiii:
.LFB3266:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%r8d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movl	%r9d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rdi
	movq	%rdi, -56(%rbp)
	xorl	%edi, %edi
	cmpb	$0, 11(%rbx)
	je	.L720
	movq	(%rbx), %rdx
	leaq	_ZN6icu_678Calendar13computeFieldsER10UErrorCode(%rip), %rcx
	movl	%esi, -68(%rbp)
	movq	%rbx, %rdi
	movl	$0, -60(%rbp)
	leaq	-60(%rbp), %rsi
	movq	232(%rdx), %rdx
	cmpq	%rcx, %rdx
	jne	.L721
	call	_ZN6icu_678Calendar13computeFieldsER10UErrorCode.part.0
	movl	-68(%rbp), %eax
.L720:
	movl	%eax, 16(%rbx)
	movl	220(%rbx), %eax
	cmpl	$10000, %eax
	je	.L723
	xorl	%ecx, %ecx
	movl	%eax, 132(%rbx)
	leal	1(%rax), %edx
	movb	$1, 105(%rbx)
	movb	$0, 11(%rbx)
	movw	%cx, 8(%rbx)
	movl	%r15d, 20(%rbx)
	cmpl	$9999, %eax
	je	.L821
.L725:
	movl	%edx, 136(%rbx)
	leal	1(%rdx), %ecx
	movb	$1, 106(%rbx)
	movl	%r14d, 32(%rbx)
	cmpl	$9999, %edx
	je	.L822
.L732:
	movl	%ecx, 148(%rbx)
	leal	1(%rcx), %esi
	movb	$1, 109(%rbx)
	movl	%r13d, 56(%rbx)
	cmpl	$9999, %ecx
	je	.L823
.L739:
	movl	%esi, 172(%rbx)
	leal	1(%rsi), %ecx
	movb	$1, 115(%rbx)
	movl	%r12d, 60(%rbx)
	cmpl	$9999, %esi
	je	.L824
.L746:
	movl	16(%rbp), %edi
	movl	%ecx, 176(%rbx)
	leal	1(%rcx), %eax
	movb	$1, 116(%rbx)
	movl	%edi, 64(%rbx)
	cmpl	$9999, %ecx
	je	.L825
.L753:
	leal	1(%rax), %esi
.L759:
	movl	%esi, 220(%rbx)
	movl	%eax, 180(%rbx)
	movb	$1, 117(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L826
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L723:
	.cfi_restore_state
	movl	$1, 220(%rbx)
	movl	$1, %edi
	.p2align 4,,10
	.p2align 3
.L730:
	movl	$10000, %edx
	xorl	%esi, %esi
	movl	$-1, %eax
	.p2align 4,,10
	.p2align 3
.L728:
	movl	128(%rbx,%rsi,4), %ecx
	cmpl	%ecx, %edx
	jle	.L726
	cmpl	%edi, %ecx
	cmovg	%ecx, %edx
	cmovg	%esi, %eax
.L726:
	addq	$1, %rsi
	cmpq	$23, %rsi
	jne	.L728
	leal	1(%rdi), %ecx
	leal	2(%rdi), %edx
	cmpl	$-1, %eax
	je	.L729
	cltq
	movl	%ecx, 220(%rbx)
	movl	%ecx, 128(%rbx,%rax,4)
	cmpl	$24, %ecx
	je	.L827
	movl	%ecx, %edi
	jmp	.L730
	.p2align 4,,10
	.p2align 3
.L822:
	movl	$1, 220(%rbx)
	movl	$1, %ecx
	.p2align 4,,10
	.p2align 3
.L744:
	movl	$10000, %esi
	xorl	%edx, %edx
	movl	$-1, %eax
	.p2align 4,,10
	.p2align 3
.L742:
	movl	128(%rbx,%rdx,4), %edi
	cmpl	%ecx, %edi
	jle	.L740
	cmpl	%esi, %edi
	cmovl	%edi, %esi
	cmovl	%edx, %eax
.L740:
	addq	$1, %rdx
	cmpq	$23, %rdx
	jne	.L742
	leal	1(%rcx), %edx
	leal	2(%rcx), %esi
	cmpl	$-1, %eax
	je	.L743
	cltq
	movl	%edx, 220(%rbx)
	movl	%edx, 128(%rbx,%rax,4)
	cmpl	$24, %edx
	je	.L828
	movl	%edx, %ecx
	jmp	.L744
	.p2align 4,,10
	.p2align 3
.L821:
	movl	$1, 220(%rbx)
	movl	$1, %esi
	.p2align 4,,10
	.p2align 3
.L737:
	movl	$10000, %ecx
	xorl	%edx, %edx
	movl	$-1, %eax
	.p2align 4,,10
	.p2align 3
.L735:
	movl	128(%rbx,%rdx,4), %edi
	cmpl	%esi, %edi
	jle	.L733
	cmpl	%ecx, %edi
	cmovl	%edi, %ecx
	cmovl	%edx, %eax
.L733:
	addq	$1, %rdx
	cmpq	$23, %rdx
	jne	.L735
	leal	1(%rsi), %edx
	leal	2(%rsi), %ecx
	cmpl	$-1, %eax
	je	.L736
	cltq
	movl	%edx, 220(%rbx)
	movl	%edx, 128(%rbx,%rax,4)
	cmpl	$24, %edx
	je	.L829
	movl	%edx, %esi
	jmp	.L737
	.p2align 4,,10
	.p2align 3
.L827:
	movl	%edx, %ecx
	leal	3(%rdi), %edx
.L729:
	xorl	%eax, %eax
	movl	%ecx, 132(%rbx)
	movb	$1, 105(%rbx)
	movb	$0, 11(%rbx)
	movw	%ax, 8(%rbx)
	movl	%r15d, 20(%rbx)
	jmp	.L725
	.p2align 4,,10
	.p2align 3
.L828:
	movl	%esi, %edx
	leal	3(%rcx), %esi
.L743:
	movl	%edx, 148(%rbx)
	movb	$1, 109(%rbx)
	movl	%r13d, 56(%rbx)
	jmp	.L739
	.p2align 4,,10
	.p2align 3
.L829:
	movl	%ecx, %edx
	leal	3(%rsi), %ecx
.L736:
	movl	%edx, 136(%rbx)
	movb	$1, 106(%rbx)
	movl	%r14d, 32(%rbx)
	jmp	.L732
	.p2align 4,,10
	.p2align 3
.L825:
	movl	$1, 220(%rbx)
	movl	$1, %ecx
	.p2align 4,,10
	.p2align 3
.L764:
	movl	$10000, %esi
	xorl	%eax, %eax
	movl	$-1, %edx
	.p2align 4,,10
	.p2align 3
.L762:
	movl	128(%rbx,%rax,4), %edi
	cmpl	%edi, %ecx
	jge	.L760
	cmpl	%esi, %edi
	cmovl	%edi, %esi
	cmovl	%eax, %edx
.L760:
	addq	$1, %rax
	cmpq	$23, %rax
	jne	.L762
	leal	1(%rcx), %eax
	leal	2(%rcx), %esi
	cmpl	$-1, %edx
	je	.L759
	movslq	%edx, %rdx
	movl	%eax, 220(%rbx)
	movl	%eax, 128(%rbx,%rdx,4)
	cmpl	$24, %eax
	je	.L830
	movl	%eax, %ecx
	jmp	.L764
	.p2align 4,,10
	.p2align 3
.L824:
	movl	$1, 220(%rbx)
	movl	$1, %esi
	.p2align 4,,10
	.p2align 3
.L758:
	movl	$10000, %ecx
	xorl	%eax, %eax
	movl	$-1, %edx
	.p2align 4,,10
	.p2align 3
.L756:
	movl	128(%rbx,%rax,4), %edi
	cmpl	%esi, %edi
	jle	.L754
	cmpl	%ecx, %edi
	cmovl	%edi, %ecx
	cmovl	%eax, %edx
.L754:
	addq	$1, %rax
	cmpq	$23, %rax
	jne	.L756
	leal	1(%rsi), %ecx
	leal	2(%rsi), %eax
	cmpl	$-1, %edx
	je	.L757
	movslq	%edx, %rdx
	movl	%ecx, 220(%rbx)
	movl	%ecx, 128(%rbx,%rdx,4)
	cmpl	$24, %ecx
	je	.L831
	movl	%ecx, %esi
	jmp	.L758
	.p2align 4,,10
	.p2align 3
.L823:
	movl	$1, 220(%rbx)
	movl	$1, %esi
	.p2align 4,,10
	.p2align 3
.L751:
	movl	$10000, %ecx
	xorl	%edx, %edx
	movl	$-1, %eax
	.p2align 4,,10
	.p2align 3
.L749:
	movl	128(%rbx,%rdx,4), %edi
	cmpl	%edi, %esi
	jge	.L747
	cmpl	%ecx, %edi
	cmovl	%edi, %ecx
	cmovl	%edx, %eax
.L747:
	addq	$1, %rdx
	cmpq	$23, %rdx
	jne	.L749
	leal	1(%rsi), %edx
	leal	2(%rsi), %ecx
	cmpl	$-1, %eax
	je	.L750
	cltq
	movl	%edx, 220(%rbx)
	movl	%edx, 128(%rbx,%rax,4)
	cmpl	$24, %edx
	je	.L832
	movl	%edx, %esi
	jmp	.L751
	.p2align 4,,10
	.p2align 3
.L831:
	movl	%eax, %ecx
	leal	3(%rsi), %eax
.L757:
	movl	16(%rbp), %edi
	movl	%ecx, 176(%rbx)
	movb	$1, 116(%rbx)
	movl	%edi, 64(%rbx)
	jmp	.L753
	.p2align 4,,10
	.p2align 3
.L832:
	movl	%ecx, %edx
	leal	3(%rsi), %ecx
.L750:
	movl	%edx, 172(%rbx)
	movb	$1, 115(%rbx)
	movl	%r12d, 60(%rbx)
	jmp	.L746
	.p2align 4,,10
	.p2align 3
.L721:
	call	*%rdx
	movl	-68(%rbp), %eax
	jmp	.L720
	.p2align 4,,10
	.p2align 3
.L830:
	movl	%esi, %eax
	leal	3(%rcx), %esi
	jmp	.L759
.L826:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3266:
	.size	_ZN6icu_678Calendar3setEiiiiii, .-_ZN6icu_678Calendar3setEiiiiii
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Calendar5clearEv
	.type	_ZN6icu_678Calendar5clearEv, @function
_ZN6icu_678Calendar5clearEv:
.LFB3271:
	.cfi_startproc
	endbr64
	movq	$0, 12(%rdi)
	movq	%rdi, %rdx
	leaq	20(%rdi), %rdi
	xorl	%eax, %eax
	movq	$0, 99(%rdi)
	movl	%edx, %ecx
	andq	$-8, %rdi
	subl	%edi, %ecx
	addl	$127, %ecx
	shrl	$3, %ecx
	rep stosq
	leaq	136(%rdx), %rdi
	movl	%edx, %ecx
	movq	$0, 128(%rdx)
	movq	$0, 212(%rdx)
	andq	$-8, %rdi
	subl	%edi, %ecx
	addl	$220, %ecx
	shrl	$3, %ecx
	rep stosq
	movl	$0, 8(%rdx)
	ret
	.cfi_endproc
.LFE3271:
	.size	_ZN6icu_678Calendar5clearEv, .-_ZN6icu_678Calendar5clearEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Calendar5clearE19UCalendarDateFields
	.type	_ZN6icu_678Calendar5clearE19UCalendarDateFields, @function
_ZN6icu_678Calendar5clearE19UCalendarDateFields:
.LFB3272:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 11(%rdi)
	je	.L835
	movq	(%rdi), %rax
	leaq	_ZN6icu_678Calendar13computeFieldsER10UErrorCode(%rip), %rdx
	movl	$0, -28(%rbp)
	leaq	-28(%rbp), %rsi
	movq	232(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L836
	call	_ZN6icu_678Calendar13computeFieldsER10UErrorCode.part.0
.L835:
	movslq	%r12d, %rsi
	leaq	(%rbx,%rsi,4), %rax
	movl	$0, 12(%rax)
	movl	$0, 128(%rax)
	movb	$0, 104(%rbx,%rsi)
	movl	$0, 8(%rbx)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L840
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L836:
	.cfi_restore_state
	call	*%rax
	jmp	.L835
.L840:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3272:
	.size	_ZN6icu_678Calendar5clearE19UCalendarDateFields, .-_ZN6icu_678Calendar5clearE19UCalendarDateFields
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678Calendar5isSetE19UCalendarDateFields
	.type	_ZNK6icu_678Calendar5isSetE19UCalendarDateFields, @function
_ZNK6icu_678Calendar5isSetE19UCalendarDateFields:
.LFB3273:
	.cfi_startproc
	endbr64
	cmpb	$0, 11(%rdi)
	movl	$1, %eax
	jne	.L841
	movslq	%esi, %rsi
	movl	128(%rdi,%rsi,4), %eax
	testl	%eax, %eax
	setne	%al
.L841:
	ret
	.cfi_endproc
.LFE3273:
	.size	_ZNK6icu_678Calendar5isSetE19UCalendarDateFields, .-_ZNK6icu_678Calendar5isSetE19UCalendarDateFields
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678Calendar11newestStampE19UCalendarDateFieldsS1_i
	.type	_ZNK6icu_678Calendar11newestStampE19UCalendarDateFieldsS1_i, @function
_ZNK6icu_678Calendar11newestStampE19UCalendarDateFieldsS1_i:
.LFB3274:
	.cfi_startproc
	endbr64
	movl	%esi, %r8d
	cmpl	%edx, %esi
	jg	.L849
	movl	%edx, %eax
	leal	1(%rdx), %r9d
	subl	%esi, %eax
	subl	%esi, %r9d
	cmpl	$2, %eax
	jbe	.L850
	movslq	%esi, %rax
	movd	%ecx, %xmm3
	movl	%r9d, %r8d
	leaq	128(%rdi,%rax,4), %rax
	pshufd	$0, %xmm3, %xmm2
	shrl	$2, %r8d
	movdqu	(%rax), %xmm0
	movdqa	%xmm0, %xmm1
	pcmpgtd	%xmm2, %xmm1
	pand	%xmm1, %xmm0
	pandn	%xmm2, %xmm1
	por	%xmm1, %xmm0
	cmpl	$1, %r8d
	je	.L847
	movdqu	16(%rax), %xmm1
	movdqa	%xmm1, %xmm2
	pcmpgtd	%xmm0, %xmm2
	pand	%xmm2, %xmm1
	pandn	%xmm0, %xmm2
	movdqa	%xmm2, %xmm0
	por	%xmm1, %xmm0
	cmpl	$2, %r8d
	je	.L847
	movdqu	32(%rax), %xmm1
	movdqa	%xmm1, %xmm2
	pcmpgtd	%xmm0, %xmm2
	pand	%xmm2, %xmm1
	pandn	%xmm0, %xmm2
	movdqa	%xmm2, %xmm0
	por	%xmm1, %xmm0
	cmpl	$3, %r8d
	je	.L847
	movdqu	48(%rax), %xmm1
	movdqa	%xmm1, %xmm2
	pcmpgtd	%xmm0, %xmm2
	pand	%xmm2, %xmm1
	pandn	%xmm0, %xmm2
	movdqa	%xmm2, %xmm0
	por	%xmm1, %xmm0
	cmpl	$4, %r8d
	je	.L847
	movdqu	64(%rax), %xmm1
	movdqa	%xmm1, %xmm2
	pcmpgtd	%xmm0, %xmm2
	pand	%xmm2, %xmm1
	pandn	%xmm0, %xmm2
	movdqa	%xmm2, %xmm0
	por	%xmm1, %xmm0
.L847:
	movdqa	%xmm0, %xmm2
	movl	%r9d, %ecx
	psrldq	$8, %xmm2
	andl	$-4, %ecx
	movdqa	%xmm2, %xmm1
	leal	(%rsi,%rcx), %r8d
	pcmpgtd	%xmm0, %xmm1
	pand	%xmm1, %xmm2
	pandn	%xmm0, %xmm1
	movdqa	%xmm1, %xmm0
	por	%xmm2, %xmm0
	movdqa	%xmm0, %xmm2
	psrldq	$4, %xmm2
	movdqa	%xmm2, %xmm1
	pcmpgtd	%xmm0, %xmm1
	pand	%xmm1, %xmm2
	pandn	%xmm0, %xmm1
	por	%xmm2, %xmm1
	movd	%xmm1, %eax
	cmpl	%ecx, %r9d
	je	.L863
.L846:
	movslq	%r8d, %rcx
	movl	128(%rdi,%rcx,4), %ecx
	cmpl	%ecx, %eax
	cmovl	%ecx, %eax
	leal	1(%r8), %ecx
	cmpl	%edx, %ecx
	jg	.L844
	movslq	%ecx, %rcx
	movl	128(%rdi,%rcx,4), %ecx
	cmpl	%ecx, %eax
	cmovl	%ecx, %eax
	leal	2(%r8), %ecx
	cmpl	%ecx, %edx
	jl	.L844
	movslq	%ecx, %rcx
	movl	128(%rdi,%rcx,4), %edx
	cmpl	%edx, %eax
	cmovl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L849:
	movl	%ecx, %eax
.L844:
	ret
	.p2align 4,,10
	.p2align 3
.L863:
	ret
.L850:
	movl	%ecx, %eax
	jmp	.L846
	.cfi_endproc
.LFE3274:
	.size	_ZNK6icu_678Calendar11newestStampE19UCalendarDateFieldsS1_i, .-_ZNK6icu_678Calendar11newestStampE19UCalendarDateFieldsS1_i
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Calendar8completeER10UErrorCode
	.type	_ZN6icu_678Calendar8completeER10UErrorCode, @function
_ZN6icu_678Calendar8completeER10UErrorCode:
.LFB3275:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 8(%rdi)
	movq	%rdi, %rbx
	je	.L865
.L872:
	cmpb	$0, 9(%rbx)
	jne	.L864
	movq	(%rbx), %rax
	leaq	_ZN6icu_678Calendar13computeFieldsER10UErrorCode(%rip), %rdx
	movq	232(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L873
	movl	(%r12), %ecx
	testl	%ecx, %ecx
	jg	.L864
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_678Calendar13computeFieldsER10UErrorCode.part.0
.L874:
	movl	(%r12), %edx
	testl	%edx, %edx
	jg	.L864
	movl	$257, %eax
	movw	%ax, 9(%rbx)
.L864:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L865:
	.cfi_restore_state
	movq	(%rdi), %rax
	call	*224(%rax)
	movl	(%r12), %edi
	testl	%edi, %edi
	jg	.L864
	cmpb	$0, 232(%rbx)
	jne	.L870
	cmpb	$0, 10(%rbx)
	jne	.L871
.L870:
	movb	$0, 9(%rbx)
.L871:
	movb	$1, 8(%rbx)
	movb	$0, 11(%rbx)
	movl	(%r12), %esi
	testl	%esi, %esi
	jle	.L872
	jmp	.L864
	.p2align 4,,10
	.p2align 3
.L873:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L874
	.cfi_endproc
.LFE3275:
	.size	_ZN6icu_678Calendar8completeER10UErrorCode, .-_ZN6icu_678Calendar8completeER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678Calendar14getRelatedYearER10UErrorCode
	.type	_ZNK6icu_678Calendar14getRelatedYearER10UErrorCode, @function
_ZNK6icu_678Calendar14getRelatedYearER10UErrorCode:
.LFB3268:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rsi), %edx
	testl	%edx, %edx
	jle	.L877
.L879:
	xorl	%r14d, %r14d
.L876:
	addq	$8, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L877:
	.cfi_restore_state
	movq	%rsi, %rbx
	movq	%rdi, %r12
	call	_ZN6icu_678Calendar8completeER10UErrorCode
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L879
	movq	(%r12), %rax
	movl	88(%r12), %r14d
	movq	%r12, %rdi
	xorl	%ebx, %ebx
	leaq	_ZL9gCalTypes(%rip), %r15
	call	*184(%rax)
	leaq	.LC4(%rip), %rsi
	movq	%rax, %r13
	jmp	.L881
	.p2align 4,,10
	.p2align 3
.L898:
	addq	$1, %rbx
	movq	(%r15,%rbx,8), %rsi
	testq	%rsi, %rsi
	je	.L876
.L881:
	movq	%r13, %rdi
	movl	%ebx, %r12d
	call	uprv_stricmp_67@PLT
	testl	%eax, %eax
	jne	.L898
	subl	$4, %r12d
	cmpl	$13, %r12d
	ja	.L876
	leaq	.L883(%rip), %rdx
	movslq	(%rdx,%r12,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L883:
	.long	.L891-.L883
	.long	.L882-.L883
	.long	.L882-.L883
	.long	.L890-.L883
	.long	.L889-.L883
	.long	.L888-.L883
	.long	.L887-.L883
	.long	.L886-.L883
	.long	.L885-.L883
	.long	.L876-.L883
	.long	.L884-.L883
	.long	.L882-.L883
	.long	.L882-.L883
	.long	.L882-.L883
	.text
	.p2align 4,,10
	.p2align 3
.L882:
	cmpl	$1396, %r14d
	jle	.L892
	leal	-1397(%r14), %eax
	movq	%rax, %rdx
	imulq	$128207979, %rax, %rax
	shrq	$33, %rax
	imull	$67, %eax, %ecx
	subl	%ecx, %edx
	cmpl	$32, %edx
	setg	%dl
	movzbl	%dl, %edx
	leal	(%rdx,%rax,2), %eax
.L893:
	addl	$579, %r14d
	subl	%eax, %r14d
	jmp	.L876
.L891:
	addl	$622, %r14d
	jmp	.L876
.L887:
	addl	$284, %r14d
	jmp	.L876
.L888:
	addl	$79, %r14d
	jmp	.L876
.L889:
	subl	$2637, %r14d
	jmp	.L876
.L890:
	subl	$3760, %r14d
	jmp	.L876
.L885:
	subl	$5492, %r14d
	jmp	.L876
.L886:
	addl	$8, %r14d
	jmp	.L876
.L884:
	subl	$2333, %r14d
	jmp	.L876
.L892:
	leal	-1396(%r14), %edx
	movl	$1396, %ecx
	movslq	%edx, %rax
	subl	%r14d, %ecx
	sarl	$31, %edx
	imulq	$128207979, %rax, %rax
	sarq	$33, %rax
	subl	%edx, %eax
	movl	%ecx, %edx
	imulq	$128207979, %rdx, %rdx
	shrq	$33, %rdx
	imull	$67, %edx, %edx
	subl	%edx, %ecx
	xorl	%edx, %edx
	cmpl	$33, %ecx
	setle	%dl
	leal	-2(%rdx,%rax,2), %eax
	jmp	.L893
	.cfi_endproc
.LFE3268:
	.size	_ZNK6icu_678Calendar14getRelatedYearER10UErrorCode, .-_ZNK6icu_678Calendar14getRelatedYearER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678Calendar16getActualMinimumE19UCalendarDateFieldsR10UErrorCode
	.type	_ZNK6icu_678Calendar16getActualMinimumE19UCalendarDateFieldsR10UErrorCode, @function
_ZNK6icu_678Calendar16getActualMinimumE19UCalendarDateFieldsR10UErrorCode:
.LFB3319:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	leaq	_ZNK6icu_678Calendar18getGreatestMinimumE19UCalendarDateFields(%rip), %rdx
	subq	$24, %rsp
	movq	(%rdi), %r8
	movq	144(%r8), %rax
	cmpq	%rdx, %rax
	jne	.L900
	movq	256(%r8), %rax
	leaq	_ZNK6icu_678Calendar8getLimitE19UCalendarDateFieldsNS0_10ELimitTypeE(%rip), %rdx
	cmpq	%rdx, %rax
	jne	.L901
	leal	-4(%rsi), %eax
	cmpl	$18, %eax
	ja	.L902
	leaq	.L904(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L904:
	.long	.L920-.L904
	.long	.L902-.L904
	.long	.L902-.L904
	.long	.L903-.L904
	.long	.L902-.L904
	.long	.L903-.L904
	.long	.L903-.L904
	.long	.L903-.L904
	.long	.L903-.L904
	.long	.L903-.L904
	.long	.L903-.L904
	.long	.L903-.L904
	.long	.L903-.L904
	.long	.L902-.L904
	.long	.L903-.L904
	.long	.L902-.L904
	.long	.L903-.L904
	.long	.L903-.L904
	.long	.L903-.L904
	.text
.L920:
	movl	$1, %r13d
.L905:
	movq	112(%r8), %rax
	leaq	_ZNK6icu_678Calendar10getMinimumE19UCalendarDateFields(%rip), %rdx
	cmpq	%rdx, %rax
	jne	.L906
.L924:
	movq	256(%r8), %rax
	leaq	_ZNK6icu_678Calendar8getLimitE19UCalendarDateFieldsNS0_10ELimitTypeE(%rip), %rdx
	cmpq	%rdx, %rax
	jne	.L907
	leal	-4(%r14), %eax
	cmpl	$18, %eax
	ja	.L908
	leaq	.L910(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L910:
	.long	.L911-.L910
	.long	.L908-.L910
	.long	.L908-.L910
	.long	.L909-.L910
	.long	.L908-.L910
	.long	.L909-.L910
	.long	.L909-.L910
	.long	.L909-.L910
	.long	.L909-.L910
	.long	.L909-.L910
	.long	.L909-.L910
	.long	.L909-.L910
	.long	.L909-.L910
	.long	.L908-.L910
	.long	.L909-.L910
	.long	.L908-.L910
	.long	.L909-.L910
	.long	.L909-.L910
	.long	.L909-.L910
	.text
.L903:
	movslq	%esi, %rax
	leaq	_ZN6icu_67L15kCalendarLimitsE(%rip), %rdx
	salq	$4, %rax
	movl	4(%rdx,%rax), %r13d
	movq	112(%r8), %rax
	leaq	_ZNK6icu_678Calendar10getMinimumE19UCalendarDateFields(%rip), %rdx
	cmpq	%rdx, %rax
	je	.L924
.L906:
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	*%rax
	movl	%eax, -52(%rbp)
	cmpl	-52(%rbp), %r13d
	jne	.L925
.L899:
	addq	$24, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L902:
	.cfi_restore_state
	movl	$1, %edx
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	*248(%r8)
	movq	(%r12), %r8
	movl	%eax, %r13d
	jmp	.L905
.L909:
	movslq	%r14d, %rax
	leaq	_ZN6icu_67L15kCalendarLimitsE(%rip), %rdx
	salq	$4, %rax
	movl	(%rdx,%rax), %eax
	movl	%eax, -52(%rbp)
.L912:
	cmpl	-52(%rbp), %r13d
	je	.L899
.L925:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L926
	movb	$1, 232(%rax)
	movslq	%r14d, %rax
	movl	%r13d, %r12d
	movq	%rax, -64(%rbp)
	.p2align 4,,10
	.p2align 3
.L919:
	movl	%r12d, %edx
	movl	%r14d, %esi
	movq	%r15, %rdi
	call	_ZN6icu_678Calendar3setE19UCalendarDateFieldsi
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jle	.L915
.L917:
	xorl	%eax, %eax
	cmpl	%eax, %r12d
	jne	.L918
.L927:
	leal	-1(%r12), %edx
	movl	%r12d, %r13d
	cmpl	%edx, -52(%rbp)
	jg	.L918
	movl	%edx, %r12d
	jmp	.L919
.L911:
	xorl	%eax, %eax
	cmpb	$1, 260(%r12)
	sete	%al
	movl	%eax, -52(%rbp)
	jmp	.L912
.L908:
	xorl	%edx, %edx
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	*248(%r8)
	movl	%eax, -52(%rbp)
	jmp	.L912
	.p2align 4,,10
	.p2align 3
.L915:
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_678Calendar8completeER10UErrorCode
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L917
	movq	-64(%rbp), %rax
	movl	12(%r15,%rax,4), %eax
	cmpl	%eax, %r12d
	je	.L927
	.p2align 4,,10
	.p2align 3
.L918:
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*8(%rax)
	movl	(%rbx), %eax
	testl	%eax, %eax
	movl	$0, %eax
	cmovg	%eax, %r13d
	jmp	.L899
	.p2align 4,,10
	.p2align 3
.L900:
	call	*%rax
	movq	(%r12), %r8
	movl	%eax, %r13d
	jmp	.L905
	.p2align 4,,10
	.p2align 3
.L907:
	xorl	%edx, %edx
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	*%rax
	movl	%eax, -52(%rbp)
	jmp	.L912
	.p2align 4,,10
	.p2align 3
.L901:
	movl	$1, %edx
	call	*%rax
	movq	(%r12), %r8
	movl	%eax, %r13d
	jmp	.L905
	.p2align 4,,10
	.p2align 3
.L926:
	movl	$7, (%rbx)
	xorl	%r13d, %r13d
	jmp	.L899
	.cfi_endproc
.LFE3319:
	.size	_ZNK6icu_678Calendar16getActualMinimumE19UCalendarDateFieldsR10UErrorCode, .-_ZNK6icu_678Calendar16getActualMinimumE19UCalendarDateFieldsR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Calendar16prepareGetActualE19UCalendarDateFieldsaR10UErrorCode
	.type	_ZN6icu_678Calendar16prepareGetActualE19UCalendarDateFieldsaR10UErrorCode, @function
_ZN6icu_678Calendar16prepareGetActualE19UCalendarDateFieldsaR10UErrorCode:
.LFB3339:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rcx, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 11(%rdi)
	je	.L929
	movq	(%rdi), %rax
	leaq	_ZN6icu_678Calendar13computeFieldsER10UErrorCode(%rip), %rdx
	movl	$0, -44(%rbp)
	leaq	-44(%rbp), %rsi
	movq	232(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L930
	call	_ZN6icu_678Calendar13computeFieldsER10UErrorCode.part.0
.L929:
	movl	220(%r12), %eax
	movl	$0, 96(%r12)
	cmpl	$10000, %eax
	je	.L932
	leal	1(%rax), %esi
.L933:
	xorl	%r9d, %r9d
	movl	%esi, 220(%r12)
	movl	%eax, 212(%r12)
	movb	$1, 125(%r12)
	movb	$0, 11(%r12)
	movw	%r9w, 8(%r12)
	cmpl	$19, %r13d
	ja	.L1090
	leaq	.L941(%rip), %rcx
	movl	%r13d, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L941:
	.long	.L1090-.L941
	.long	.L940-.L941
	.long	.L945-.L941
	.long	.L944-.L941
	.long	.L944-.L941
	.long	.L1090-.L941
	.long	.L1090-.L941
	.long	.L1090-.L941
	.long	.L943-.L941
	.long	.L1090-.L941
	.long	.L1090-.L941
	.long	.L1090-.L941
	.long	.L1090-.L941
	.long	.L1090-.L941
	.long	.L1090-.L941
	.long	.L1090-.L941
	.long	.L1090-.L941
	.long	.L942-.L941
	.long	.L1090-.L941
	.long	.L940-.L941
	.text
	.p2align 4,,10
	.p2align 3
.L944:
	movl	256(%r12), %eax
	testb	%r14b, %r14b
	je	.L1007
	leal	6(%rax), %edx
	movslq	%edx, %rax
	movl	%edx, %ecx
	imulq	$-1840700269, %rax, %rax
	sarl	$31, %ecx
	shrq	$32, %rax
	addl	%edx, %eax
	sarl	$2, %eax
	subl	%ecx, %eax
	leal	0(,%rax,8), %ecx
	subl	%eax, %ecx
	subl	%ecx, %edx
	movl	%edx, %eax
	leal	7(%rdx), %edx
	testl	%eax, %eax
	cmovle	%edx, %eax
.L1007:
	movl	%eax, 40(%r12)
	cmpl	$10000, %esi
	je	.L1008
	leal	1(%rsi), %eax
.L1009:
	movl	%eax, 220(%r12)
	movl	%esi, 156(%r12)
	movb	$1, 111(%r12)
.L1090:
	movq	(%r12), %rcx
	leaq	_ZNK6icu_678Calendar18getGreatestMinimumE19UCalendarDateFields(%rip), %rbx
.L947:
	movq	144(%rcx), %rax
	cmpq	%rbx, %rax
	jne	.L1015
	movq	256(%rcx), %rax
	leaq	_ZNK6icu_678Calendar8getLimitE19UCalendarDateFieldsNS0_10ELimitTypeE(%rip), %rdx
	cmpq	%rdx, %rax
	jne	.L1016
	leal	-4(%r13), %eax
	cmpl	$18, %eax
	ja	.L1017
	leaq	.L1019(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L1019:
	.long	.L1036-.L1019
	.long	.L1017-.L1019
	.long	.L1017-.L1019
	.long	.L1018-.L1019
	.long	.L1017-.L1019
	.long	.L1018-.L1019
	.long	.L1018-.L1019
	.long	.L1018-.L1019
	.long	.L1018-.L1019
	.long	.L1018-.L1019
	.long	.L1018-.L1019
	.long	.L1018-.L1019
	.long	.L1018-.L1019
	.long	.L1017-.L1019
	.long	.L1018-.L1019
	.long	.L1017-.L1019
	.long	.L1018-.L1019
	.long	.L1018-.L1019
	.long	.L1018-.L1019
	.text
.L1036:
	movl	$1, %edx
.L1020:
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN6icu_678Calendar3setE19UCalendarDateFieldsi
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1091
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1017:
	.cfi_restore_state
	movl	$1, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	*248(%rcx)
	movl	%eax, %edx
	jmp	.L1020
.L1018:
	movslq	%r13d, %rax
	leaq	_ZN6icu_67L15kCalendarLimitsE(%rip), %rdx
	salq	$4, %rax
	movl	4(%rdx,%rax), %edx
	jmp	.L1020
	.p2align 4,,10
	.p2align 3
.L932:
	movl	$1, 220(%r12)
	movl	$1, %r8d
	.p2align 4,,10
	.p2align 3
.L938:
	movl	$10000, %edx
	xorl	%eax, %eax
	movl	$-1, %edi
	.p2align 4,,10
	.p2align 3
.L936:
	movl	128(%r12,%rax,4), %ecx
	cmpl	%edx, %ecx
	jge	.L934
	cmpl	%r8d, %ecx
	cmovg	%ecx, %edx
	cmovg	%eax, %edi
.L934:
	addq	$1, %rax
	cmpq	$23, %rax
	jne	.L936
	leal	1(%r8), %eax
	leal	2(%r8), %esi
	cmpl	$-1, %edi
	je	.L933
	movslq	%edi, %rdi
	movl	%eax, 220(%r12)
	movl	%eax, 128(%r12,%rdi,4)
	cmpl	$24, %eax
	je	.L1092
	movl	%eax, %r8d
	jmp	.L938
	.p2align 4,,10
	.p2align 3
.L1015:
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	*%rax
	movl	%eax, %edx
	jmp	.L1020
	.p2align 4,,10
	.p2align 3
.L940:
	movq	(%r12), %rax
	leaq	_ZNK6icu_678Calendar18getGreatestMinimumE19UCalendarDateFields(%rip), %rbx
	movq	144(%rax), %rdx
	cmpq	%rbx, %rdx
	jne	.L948
	movq	256(%rax), %rcx
	leaq	_ZNK6icu_678Calendar8getLimitE19UCalendarDateFieldsNS0_10ELimitTypeE(%rip), %rdx
	movl	$6, %esi
	movq	%r12, %rdi
	cmpq	%rdx, %rcx
	movl	$1, %edx
	jne	.L949
	call	*248(%rax)
	movl	%eax, %r14d
.L950:
	cmpb	$0, 11(%r12)
	movq	(%r12), %rcx
	je	.L951
	movq	232(%rcx), %rax
	leaq	_ZN6icu_678Calendar13computeFieldsER10UErrorCode(%rip), %rdx
	leaq	-44(%rbp), %rsi
	movq	%r12, %rdi
	movl	$0, -44(%rbp)
	cmpq	%rdx, %rax
	jne	.L952
	call	_ZN6icu_678Calendar13computeFieldsER10UErrorCode.part.0
.L953:
	movq	(%r12), %rcx
.L951:
	movl	220(%r12), %eax
	movl	%r14d, 36(%r12)
	cmpl	$10000, %eax
	je	.L954
	leal	1(%rax), %edx
.L955:
	xorl	%r8d, %r8d
	movl	%edx, 220(%r12)
	movl	%eax, 152(%r12)
	movb	$1, 110(%r12)
	movb	$0, 11(%r12)
	movw	%r8w, 8(%r12)
	jmp	.L947
	.p2align 4,,10
	.p2align 3
.L930:
	call	*%rax
	jmp	.L929
	.p2align 4,,10
	.p2align 3
.L943:
	movl	$1, 32(%r12)
	cmpl	$10000, %esi
	je	.L987
	leal	1(%rsi), %eax
.L988:
	movb	$1, 109(%r12)
	movl	(%rbx), %ecx
	movl	%eax, 220(%r12)
	movl	%esi, 148(%r12)
	testl	%ecx, %ecx
	jle	.L994
	movq	(%r12), %rcx
	xorl	%ebx, %ebx
.L995:
	movl	220(%r12), %eax
	movl	%ebx, 40(%r12)
	cmpl	$10000, %eax
	je	.L1000
	leal	1(%rax), %edx
.L1001:
	movl	%eax, 156(%r12)
	xorl	%eax, %eax
	leaq	_ZNK6icu_678Calendar18getGreatestMinimumE19UCalendarDateFields(%rip), %rbx
	movl	%edx, 220(%r12)
	movb	$1, 111(%r12)
	movb	$0, 11(%r12)
	movw	%ax, 8(%r12)
	jmp	.L947
	.p2align 4,,10
	.p2align 3
.L942:
	movq	(%r12), %rax
	leaq	_ZNK6icu_678Calendar18getGreatestMinimumE19UCalendarDateFields(%rip), %rbx
	movq	144(%rax), %rdx
	cmpq	%rbx, %rdx
	jne	.L961
	movq	256(%rax), %rcx
	leaq	_ZNK6icu_678Calendar8getLimitE19UCalendarDateFieldsNS0_10ELimitTypeE(%rip), %rdx
	movl	$3, %esi
	movq	%r12, %rdi
	cmpq	%rdx, %rcx
	movl	$1, %edx
	jne	.L962
	call	*248(%rax)
	movl	%eax, %r14d
.L963:
	cmpb	$0, 11(%r12)
	movq	(%r12), %r8
	je	.L964
	movq	232(%r8), %rax
	leaq	_ZN6icu_678Calendar13computeFieldsER10UErrorCode(%rip), %rdx
	leaq	-44(%rbp), %rsi
	movq	%r12, %rdi
	movl	$0, -44(%rbp)
	cmpq	%rdx, %rax
	jne	.L965
	call	_ZN6icu_678Calendar13computeFieldsER10UErrorCode.part.0
.L966:
	movq	(%r12), %r8
.L964:
	movl	220(%r12), %eax
	movl	%r14d, 24(%r12)
	cmpl	$10000, %eax
	je	.L967
	leal	1(%rax), %edx
.L968:
	xorl	%edi, %edi
	movl	%edx, 220(%r12)
	movl	%eax, 140(%r12)
	movb	$1, 107(%r12)
	movb	$0, 11(%r12)
	movw	%di, 8(%r12)
.L946:
	movq	144(%r8), %rax
	cmpq	%rbx, %rax
	jne	.L974
	movq	256(%r8), %rax
	leaq	_ZNK6icu_678Calendar8getLimitE19UCalendarDateFieldsNS0_10ELimitTypeE(%rip), %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	cmpq	%rdx, %rax
	movl	$1, %edx
	jne	.L975
	call	*248(%r8)
	movl	%eax, %r14d
.L976:
	cmpb	$0, 11(%r12)
	movq	(%r12), %rcx
	je	.L977
	movq	232(%rcx), %rax
	leaq	_ZN6icu_678Calendar13computeFieldsER10UErrorCode(%rip), %rdx
	leaq	-44(%rbp), %rsi
	movq	%r12, %rdi
	movl	$0, -44(%rbp)
	cmpq	%rdx, %rax
	jne	.L978
	call	_ZN6icu_678Calendar13computeFieldsER10UErrorCode.part.0
.L979:
	movq	(%r12), %rcx
.L977:
	movl	220(%r12), %eax
	movl	%r14d, 32(%r12)
	cmpl	$10000, %eax
	je	.L980
	leal	1(%rax), %esi
.L981:
	movl	%esi, 220(%r12)
	xorl	%esi, %esi
	movl	%eax, 148(%r12)
	movb	$1, 109(%r12)
	movb	$0, 11(%r12)
	movw	%si, 8(%r12)
	jmp	.L947
	.p2align 4,,10
	.p2align 3
.L945:
	movq	(%r12), %r8
	leaq	_ZNK6icu_678Calendar18getGreatestMinimumE19UCalendarDateFields(%rip), %rbx
	jmp	.L946
	.p2align 4,,10
	.p2align 3
.L1016:
	movl	$1, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	*%rax
	movl	%eax, %edx
	jmp	.L1020
	.p2align 4,,10
	.p2align 3
.L1092:
	movl	%esi, %eax
	leal	3(%r8), %esi
	jmp	.L933
	.p2align 4,,10
	.p2align 3
.L994:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_678Calendar8completeER10UErrorCode
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L1093
	movl	40(%r12), %ebx
	movzbl	11(%r12), %eax
.L997:
	movq	(%r12), %rcx
	testb	%al, %al
	je	.L995
	movq	232(%rcx), %rax
	leaq	_ZN6icu_678Calendar13computeFieldsER10UErrorCode(%rip), %rdx
	leaq	-44(%rbp), %rsi
	movq	%r12, %rdi
	movl	$0, -44(%rbp)
	cmpq	%rdx, %rax
	jne	.L998
	call	_ZN6icu_678Calendar13computeFieldsER10UErrorCode.part.0
.L999:
	movq	(%r12), %rcx
	jmp	.L995
	.p2align 4,,10
	.p2align 3
.L980:
	movl	$1, 220(%r12)
	movl	$1, %r8d
	.p2align 4,,10
	.p2align 3
.L986:
	movl	$10000, %edi
	xorl	%eax, %eax
	movl	$-1, %edx
	.p2align 4,,10
	.p2align 3
.L984:
	movl	128(%r12,%rax,4), %esi
	cmpl	%r8d, %esi
	jle	.L982
	cmpl	%edi, %esi
	cmovl	%esi, %edi
	cmovl	%eax, %edx
.L982:
	addq	$1, %rax
	cmpq	$23, %rax
	jne	.L984
	leal	1(%r8), %eax
	leal	2(%r8), %esi
	cmpl	$-1, %edx
	je	.L981
	movslq	%edx, %rdx
	movl	%eax, 220(%r12)
	movl	%eax, 128(%r12,%rdx,4)
	cmpl	$24, %eax
	je	.L1094
	movl	%eax, %r8d
	jmp	.L986
	.p2align 4,,10
	.p2align 3
.L974:
	movl	$5, %esi
	movq	%r12, %rdi
	call	*%rax
	movl	%eax, %r14d
	jmp	.L976
	.p2align 4,,10
	.p2align 3
.L975:
	call	*%rax
	movl	%eax, %r14d
	jmp	.L976
	.p2align 4,,10
	.p2align 3
.L954:
	movl	$1, 220(%r12)
	movl	$1, %r8d
	.p2align 4,,10
	.p2align 3
.L960:
	movl	$10000, %edi
	xorl	%eax, %eax
	movl	$-1, %esi
	.p2align 4,,10
	.p2align 3
.L958:
	movl	128(%r12,%rax,4), %edx
	cmpl	%r8d, %edx
	jle	.L956
	cmpl	%edi, %edx
	cmovl	%edx, %edi
	cmovl	%eax, %esi
.L956:
	addq	$1, %rax
	cmpq	$23, %rax
	jne	.L958
	leal	1(%r8), %eax
	leal	2(%r8), %edx
	cmpl	$-1, %esi
	je	.L955
	movslq	%esi, %rsi
	movl	%eax, 220(%r12)
	movl	%eax, 128(%r12,%rsi,4)
	cmpl	$24, %eax
	je	.L1095
	movl	%eax, %r8d
	jmp	.L960
	.p2align 4,,10
	.p2align 3
.L967:
	movl	$1, 220(%r12)
	movl	$1, %edi
	.p2align 4,,10
	.p2align 3
.L973:
	movl	$10000, %esi
	xorl	%eax, %eax
	movl	$-1, %ecx
	.p2align 4,,10
	.p2align 3
.L971:
	movl	128(%r12,%rax,4), %edx
	cmpl	%edi, %edx
	jle	.L969
	cmpl	%esi, %edx
	cmovl	%edx, %esi
	cmovl	%eax, %ecx
.L969:
	addq	$1, %rax
	cmpq	$23, %rax
	jne	.L971
	leal	1(%rdi), %eax
	leal	2(%rdi), %edx
	cmpl	$-1, %ecx
	je	.L968
	movslq	%ecx, %rcx
	movl	%eax, 220(%r12)
	movl	%eax, 128(%r12,%rcx,4)
	cmpl	$24, %eax
	je	.L1096
	movl	%eax, %edi
	jmp	.L973
	.p2align 4,,10
	.p2align 3
.L1000:
	movl	$1, 220(%r12)
	movl	$1, %r8d
	.p2align 4,,10
	.p2align 3
.L1006:
	movl	$10000, %edi
	xorl	%eax, %eax
	movl	$-1, %esi
	.p2align 4,,10
	.p2align 3
.L1004:
	movl	128(%r12,%rax,4), %edx
	cmpl	%edx, %r8d
	jge	.L1002
	cmpl	%edi, %edx
	cmovl	%edx, %edi
	cmovl	%eax, %esi
.L1002:
	addq	$1, %rax
	cmpq	$23, %rax
	jne	.L1004
	leal	1(%r8), %eax
	leal	2(%r8), %edx
	cmpl	$-1, %esi
	je	.L1001
	movslq	%esi, %rsi
	movl	%eax, 220(%r12)
	movl	%eax, 128(%r12,%rsi,4)
	cmpl	$24, %eax
	je	.L1097
	movl	%eax, %r8d
	jmp	.L1006
	.p2align 4,,10
	.p2align 3
.L987:
	movl	$1, 220(%r12)
	movl	$1, %edi
	.p2align 4,,10
	.p2align 3
.L993:
	movl	$10000, %esi
	xorl	%eax, %eax
	movl	$-1, %ecx
	.p2align 4,,10
	.p2align 3
.L991:
	movl	128(%r12,%rax,4), %edx
	cmpl	%edx, %edi
	jge	.L989
	cmpl	%esi, %edx
	cmovl	%edx, %esi
	cmovl	%eax, %ecx
.L989:
	addq	$1, %rax
	cmpq	$23, %rax
	jne	.L991
	leal	1(%rdi), %esi
	leal	2(%rdi), %eax
	cmpl	$-1, %ecx
	je	.L988
	movslq	%ecx, %rcx
	movl	%esi, 220(%r12)
	movl	%esi, 128(%r12,%rcx,4)
	cmpl	$24, %esi
	je	.L1098
	movl	%esi, %edi
	jmp	.L993
	.p2align 4,,10
	.p2align 3
.L1008:
	movl	$1, 220(%r12)
	movl	$1, %edi
	.p2align 4,,10
	.p2align 3
.L1014:
	movl	$10000, %esi
	xorl	%eax, %eax
	movl	$-1, %ecx
	.p2align 4,,10
	.p2align 3
.L1012:
	movl	128(%r12,%rax,4), %edx
	cmpl	%edi, %edx
	jle	.L1010
	cmpl	%esi, %edx
	cmovl	%edx, %esi
	cmovl	%eax, %ecx
.L1010:
	addq	$1, %rax
	cmpq	$23, %rax
	jne	.L1012
	leal	1(%rdi), %esi
	leal	2(%rdi), %eax
	cmpl	$-1, %ecx
	je	.L1009
	movslq	%ecx, %rcx
	movl	%esi, 220(%r12)
	movl	%esi, 128(%r12,%rcx,4)
	cmpl	$24, %esi
	je	.L1099
	movl	%esi, %edi
	jmp	.L1014
	.p2align 4,,10
	.p2align 3
.L948:
	movl	$6, %esi
	movq	%r12, %rdi
	call	*%rdx
	movl	%eax, %r14d
	jmp	.L950
	.p2align 4,,10
	.p2align 3
.L978:
	call	*%rax
	jmp	.L979
	.p2align 4,,10
	.p2align 3
.L961:
	movl	$3, %esi
	movq	%r12, %rdi
	call	*%rdx
	movl	%eax, %r14d
	jmp	.L963
	.p2align 4,,10
	.p2align 3
.L1094:
	movl	%esi, %eax
	leal	3(%r8), %esi
	jmp	.L981
	.p2align 4,,10
	.p2align 3
.L949:
	call	*%rcx
	movl	%eax, %r14d
	jmp	.L950
	.p2align 4,,10
	.p2align 3
.L962:
	call	*%rcx
	movl	%eax, %r14d
	jmp	.L963
	.p2align 4,,10
	.p2align 3
.L965:
	call	*%rax
	jmp	.L966
	.p2align 4,,10
	.p2align 3
.L952:
	call	*%rax
	jmp	.L953
	.p2align 4,,10
	.p2align 3
.L998:
	call	*%rax
	jmp	.L999
	.p2align 4,,10
	.p2align 3
.L1096:
	movl	%edx, %eax
	leal	3(%rdi), %edx
	jmp	.L968
	.p2align 4,,10
	.p2align 3
.L1099:
	movl	%eax, %esi
	leal	3(%rdi), %eax
	jmp	.L1009
	.p2align 4,,10
	.p2align 3
.L1095:
	movl	%edx, %eax
	leal	3(%r8), %edx
	jmp	.L955
	.p2align 4,,10
	.p2align 3
.L1097:
	movl	%edx, %eax
	leal	3(%r8), %edx
	jmp	.L1001
	.p2align 4,,10
	.p2align 3
.L1098:
	movl	%eax, %esi
	leal	3(%rdi), %eax
	jmp	.L988
.L1091:
	call	__stack_chk_fail@PLT
.L1093:
	movzbl	11(%r12), %eax
	xorl	%ebx, %ebx
	jmp	.L997
	.cfi_endproc
.LFE3339:
	.size	_ZN6icu_678Calendar16prepareGetActualE19UCalendarDateFieldsaR10UErrorCode, .-_ZN6icu_678Calendar16prepareGetActualE19UCalendarDateFieldsaR10UErrorCode
	.section	.rodata.str1.1
.LC7:
	.string	"roc"
.LC8:
	.string	"coptic"
	.text
	.align 2
	.p2align 4
	.type	_ZN6icu_678Calendar4rollE19UCalendarDateFieldsiR10UErrorCode.part.0, @function
_ZN6icu_678Calendar4rollE19UCalendarDateFieldsiR10UErrorCode.part.0:
.LFB4354:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$21, %esi
	ja	.L1101
	leaq	.L1103(%rip), %rcx
	movl	%edx, %ebx
	movl	%esi, %edx
	movq	%rdi, %r14
	movslq	(%rcx,%rdx,4), %rax
	movl	%esi, %r12d
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L1103:
	.long	.L1102-.L1103
	.long	.L1107-.L1103
	.long	.L1113-.L1103
	.long	.L1112-.L1103
	.long	.L1111-.L1103
	.long	.L1102-.L1103
	.long	.L1110-.L1103
	.long	.L1106-.L1103
	.long	.L1109-.L1103
	.long	.L1102-.L1103
	.long	.L1108-.L1103
	.long	.L1108-.L1103
	.long	.L1102-.L1103
	.long	.L1102-.L1103
	.long	.L1102-.L1103
	.long	.L1101-.L1103
	.long	.L1101-.L1103
	.long	.L1107-.L1103
	.long	.L1106-.L1103
	.long	.L1105-.L1103
	.long	.L1104-.L1103
	.long	.L1102-.L1103
	.text
	.p2align 4,,10
	.p2align 3
.L1102:
	movq	(%rdi), %rax
	movq	%r13, %rdx
	call	*168(%rax)
	movq	%r13, %rdx
	movl	%r12d, %esi
	movq	%r14, %rdi
	movl	%eax, %r15d
	movq	(%r14), %rax
	call	*176(%rax)
	movl	%r12d, %esi
	movq	%r14, %rdi
	subl	%r15d, %eax
	leal	1(%rax), %ecx
	movslq	%r12d, %rax
	addl	12(%r14,%rax,4), %ebx
	movl	%ebx, %eax
	subl	%r15d, %eax
	cltd
	idivl	%ecx
	addl	%edx, %ecx
	testl	%edx, %edx
	cmovns	%edx, %ecx
	leal	(%r15,%rcx), %edx
	call	_ZN6icu_678Calendar3setE19UCalendarDateFieldsi
	.p2align 4,,10
	.p2align 3
.L1100:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1651
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1107:
	.cfi_restore_state
	movq	%r13, %rsi
	call	_ZN6icu_678Calendar8completeER10UErrorCode
	movl	0(%r13), %r15d
	testl	%r15d, %r15d
	jle	.L1652
.L1186:
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*184(%rax)
	movl	$10, %ecx
	leaq	.LC4(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L1188
	movl	$4, %ecx
	leaq	.LC7(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L1188
	movq	%rax, %rsi
	movl	$7, %ecx
	leaq	.LC8(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L1188
	xorl	%edx, %edx
	jmp	.L1189
	.p2align 4,,10
	.p2align 3
.L1106:
	pxor	%xmm0, %xmm0
	movsd	.LC1(%rip), %xmm2
	movslq	%esi, %rax
	movl	$1, %edx
	cvtsi2sdl	%ebx, %xmm0
	movl	12(%rdi,%rax,4), %eax
	mulsd	%xmm2, %xmm0
	cmpl	$7, %esi
	jne	.L1418
	movl	256(%rdi), %edx
.L1418:
	movsd	224(%r14), %xmm3
	subl	%edx, %eax
	pxor	%xmm1, %xmm1
	leal	7(%rax), %edx
	cmovs	%edx, %eax
	addsd	%xmm3, %xmm0
	cvtsi2sdl	%eax, %xmm1
	movq	.LC9(%rip), %rax
	mulsd	%xmm2, %xmm1
	movapd	%xmm3, %xmm2
	subsd	%xmm1, %xmm2
	movq	%rax, %xmm1
	subsd	%xmm2, %xmm0
	movsd	%xmm2, -72(%rbp)
	call	uprv_fmod_67@PLT
	pxor	%xmm1, %xmm1
	movsd	-72(%rbp), %xmm2
	comisd	%xmm0, %xmm1
	ja	.L1653
.L1426:
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L1100
	addsd	%xmm2, %xmm0
	comisd	.LC5(%rip), %xmm0
	jbe	.L1631
.L1640:
	movq	.LC5(%rip), %rax
	cmpb	$0, 232(%r14)
	movq	%rax, %xmm0
	je	.L1101
.L1430:
	leaq	20(%r14), %rdi
	movl	%r14d, %edx
	xorl	%eax, %eax
	movl	%r14d, %r8d
	andq	$-8, %rdi
	movl	$16777217, 8(%r14)
	subl	%edi, %edx
	movq	$0, 12(%r14)
	leal	127(%rdx), %ecx
	movq	$0, 119(%r14)
	movsd	%xmm0, 224(%r14)
	shrl	$3, %ecx
	rep stosq
	leaq	136(%r14), %rdi
	movq	$0, 128(%r14)
	movq	$0, 212(%r14)
	andq	$-8, %rdi
	subl	%edi, %r8d
	leal	220(%r8), %ecx
	shrl	$3, %ecx
	rep stosq
	jmp	.L1100
	.p2align 4,,10
	.p2align 3
.L1108:
	cmpb	$0, 8(%rdi)
	movq	(%rdi), %rax
	je	.L1654
.L1116:
	movl	0(%r13), %ecx
	pxor	%xmm1, %xmm1
	testl	%ecx, %ecx
	jg	.L1118
	movsd	224(%r14), %xmm1
.L1118:
	movslq	%r12d, %rdx
	movsd	%xmm1, -72(%rbp)
	movl	%r12d, %esi
	movq	%r14, %rdi
	movl	12(%r14,%rdx,4), %r15d
	call	*128(%rax)
	leal	1(%rax), %ecx
	leal	(%rbx,%r15), %eax
	cltd
	idivl	%ecx
	addl	%edx, %ecx
	testl	%edx, %edx
	cmovns	%edx, %ecx
	movl	0(%r13), %edx
	testl	%edx, %edx
	jg	.L1100
	subl	%r15d, %ecx
	pxor	%xmm0, %xmm0
	movsd	-72(%rbp), %xmm1
	imull	$3600000, %ecx, %ecx
	cvtsi2sdl	%ecx, %xmm0
	addsd	%xmm1, %xmm0
	comisd	.LC5(%rip), %xmm0
	ja	.L1640
.L1631:
	movsd	.LC6(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jbe	.L1430
	cmpb	$0, 232(%r14)
	je	.L1101
	movapd	%xmm1, %xmm0
	jmp	.L1430
	.p2align 4,,10
	.p2align 3
.L1101:
	movl	$1, 0(%r13)
	jmp	.L1100
	.p2align 4,,10
	.p2align 3
.L1111:
	movl	40(%rdi), %r12d
	subl	256(%rdi), %r12d
	leal	7(%r12), %eax
	cmovs	%eax, %r12d
	movl	%r12d, %edx
	subl	32(%rdi), %edx
	addl	$1, %edx
	movslq	%edx, %rax
	movl	%edx, %ecx
	imulq	$-1840700269, %rax, %rax
	sarl	$31, %ecx
	shrq	$32, %rax
	addl	%edx, %eax
	sarl	$2, %eax
	subl	%ecx, %eax
	leal	0(,%rax,8), %ecx
	subl	%eax, %ecx
	subl	%ecx, %edx
	movzbl	260(%rdi), %ecx
	movl	%edx, %eax
	leal	7(%rdx), %edx
	cmovs	%edx, %eax
	movl	$7, %edx
	subl	%eax, %edx
	cmpl	%ecx, %edx
	jl	.L1655
	movl	$1, %edx
	subl	%eax, %edx
	movl	%edx, %r15d
.L1385:
	movq	(%r14), %rax
	movq	%r13, %rdx
	movl	$5, %esi
	movq	%r14, %rdi
	movl	$1, %r13d
	call	*176(%rax)
	movl	32(%r14), %edx
	movl	%eax, %esi
	subl	%edx, %eax
	addl	%eax, %r12d
	movslq	%r12d, %rax
	movl	%r12d, %ecx
	imulq	$-1840700269, %rax, %rax
	sarl	$31, %ecx
	shrq	$32, %rax
	addl	%r12d, %eax
	sarl	$2, %eax
	subl	%ecx, %eax
	leal	0(,%rax,8), %ecx
	subl	%eax, %ecx
	movl	%ecx, %eax
	movl	%esi, %ecx
	subl	%r12d, %ecx
	leal	7(%rcx,%rax), %ecx
	leal	(%rdx,%rbx,8), %eax
	subl	%ebx, %eax
	subl	%r15d, %ecx
	subl	%r15d, %eax
	cltd
	idivl	%ecx
	addl	%edx, %ecx
	testl	%edx, %edx
	cmovns	%edx, %ecx
	addl	%r15d, %ecx
	testl	%ecx, %ecx
	cmovle	%r13d, %ecx
	cmpl	%ecx, %esi
	cmovle	%esi, %ecx
	cmpb	$0, 11(%r14)
	movl	%ecx, %r13d
	je	.L1387
	movq	(%r14), %rax
	leaq	_ZN6icu_678Calendar13computeFieldsER10UErrorCode(%rip), %rdx
	leaq	-60(%rbp), %rsi
	movq	%r14, %rdi
	movl	$0, -60(%rbp)
	movq	232(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1388
	call	_ZN6icu_678Calendar13computeFieldsER10UErrorCode.part.0
.L1387:
	movl	220(%r14), %eax
	movl	%r13d, 32(%r14)
	cmpl	$10000, %eax
	je	.L1390
.L1638:
	leal	1(%rax), %edx
.L1391:
	xorl	%ecx, %ecx
	movl	%edx, 220(%r14)
	movl	%eax, 148(%r14)
	movb	$1, 109(%r14)
	movb	$0, 11(%r14)
	movw	%cx, 8(%r14)
	jmp	.L1100
	.p2align 4,,10
	.p2align 3
.L1113:
	movq	(%rdi), %rax
	movq	%r13, %rdx
	movl	$2, %esi
	call	*176(%rax)
	movq	(%r14), %r10
	leal	1(%rax), %ecx
	movl	20(%r14), %eax
	addl	%ebx, %eax
	cltd
	idivl	%ecx
	addl	%edx, %ecx
	movl	%edx, %ebx
	testl	%edx, %edx
	cmovs	%ecx, %ebx
	cmpb	$0, 11(%r14)
	jne	.L1656
.L1130:
	movl	220(%r14), %eax
	movl	%ebx, 20(%r14)
	cmpl	$10000, %eax
	je	.L1133
	leal	1(%rax), %edx
.L1134:
	movl	%eax, 136(%r14)
	xorl	%eax, %eax
	movl	$5, %esi
	movq	%r14, %rdi
	movb	$1, 106(%r14)
	movb	$0, 11(%r14)
	movw	%ax, 8(%r14)
	movq	352(%r10), %rax
	movl	%edx, 220(%r14)
	leaq	_ZN6icu_678Calendar8pinFieldE19UCalendarDateFieldsR10UErrorCode(%rip), %rdx
	cmpq	%rdx, %rax
	movq	%r13, %rdx
	jne	.L1336
	call	*176(%r10)
	leaq	_ZNK6icu_678Calendar16getActualMinimumE19UCalendarDateFieldsR10UErrorCode(%rip), %rdx
	movl	%eax, -80(%rbp)
	movq	(%r14), %rax
	movq	168(%rax), %rcx
	cmpq	%rdx, %rcx
	jne	.L1141
	movq	144(%rax), %rdx
	leaq	_ZNK6icu_678Calendar18getGreatestMinimumE19UCalendarDateFields(%rip), %rcx
	cmpq	%rcx, %rdx
	jne	.L1142
	movq	256(%rax), %rcx
	leaq	_ZNK6icu_678Calendar8getLimitE19UCalendarDateFieldsNS0_10ELimitTypeE(%rip), %rdx
	movl	$5, %esi
	movq	%r14, %rdi
	cmpq	%rdx, %rcx
	movl	$1, %edx
	jne	.L1143
	call	*248(%rax)
	movl	%eax, %ebx
.L1144:
	movq	(%r14), %rax
	leaq	_ZNK6icu_678Calendar10getMinimumE19UCalendarDateFields(%rip), %rcx
	movq	112(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L1145
	movq	256(%rax), %rcx
	leaq	_ZNK6icu_678Calendar8getLimitE19UCalendarDateFieldsNS0_10ELimitTypeE(%rip), %rdx
	movl	$5, %esi
	movq	%r14, %rdi
	cmpq	%rdx, %rcx
	movl	$0, %edx
	jne	.L1146
	call	*248(%rax)
	movl	%eax, -72(%rbp)
.L1147:
	cmpl	-72(%rbp), %ebx
	je	.L1148
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*24(%rax)
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1657
	movb	$1, 232(%rax)
	movl	%ebx, %r12d
	.p2align 4,,10
	.p2align 3
.L1164:
	cmpb	$0, 11(%r15)
	je	.L1150
	movq	(%r15), %rax
	leaq	_ZN6icu_678Calendar13computeFieldsER10UErrorCode(%rip), %rdi
	movl	$0, -60(%rbp)
	leaq	-60(%rbp), %rsi
	movq	232(%rax), %rax
	cmpq	%rdi, %rax
	movq	%r15, %rdi
	jne	.L1151
	call	_ZN6icu_678Calendar13computeFieldsER10UErrorCode.part.0
.L1150:
	movl	220(%r15), %eax
	movl	%r12d, 32(%r15)
	cmpl	$10000, %eax
	je	.L1153
	leal	1(%rax), %esi
.L1154:
	movl	%eax, 148(%r15)
	xorl	%eax, %eax
	movb	$1, 109(%r15)
	movb	$0, 11(%r15)
	movw	%ax, 8(%r15)
	movl	0(%r13), %eax
	movl	%esi, 220(%r15)
	testl	%eax, %eax
	jle	.L1160
.L1162:
	xorl	%eax, %eax
.L1161:
	cmpl	%eax, %r12d
	jne	.L1163
	leal	-1(%r12), %eax
	movl	%r12d, %ebx
	cmpl	%eax, -72(%rbp)
	jg	.L1163
	movl	%eax, %r12d
	jmp	.L1164
	.p2align 4,,10
	.p2align 3
.L1110:
	movl	36(%rdi), %eax
	pxor	%xmm0, %xmm0
	movq	%r13, %rdx
	movl	$6, %esi
	movsd	224(%rdi), %xmm1
	subl	$1, %eax
	cvtsi2sdl	%eax, %xmm0
	mulsd	.LC1(%rip), %xmm0
	movq	(%rdi), %rax
	subsd	%xmm0, %xmm1
	movsd	%xmm1, -72(%rbp)
	call	*176(%rax)
	pxor	%xmm1, %xmm1
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm1
	mulsd	.LC1(%rip), %xmm1
	cvtsi2sdl	%ebx, %xmm0
	mulsd	.LC1(%rip), %xmm0
	addsd	224(%r14), %xmm0
	subsd	-72(%rbp), %xmm0
	movsd	%xmm1, -80(%rbp)
	call	uprv_fmod_67@PLT
	pxor	%xmm2, %xmm2
	comisd	%xmm0, %xmm2
	ja	.L1658
.L1412:
	movl	0(%r13), %edx
	testl	%edx, %edx
	jg	.L1100
	addsd	-72(%rbp), %xmm0
	comisd	.LC5(%rip), %xmm0
	jbe	.L1631
	jmp	.L1640
	.p2align 4,,10
	.p2align 3
.L1112:
	movl	40(%rdi), %r12d
	subl	256(%rdi), %r12d
	leal	7(%r12), %eax
	cmovs	%eax, %r12d
	movl	%r12d, %edx
	subl	36(%rdi), %edx
	addl	$1, %edx
	movslq	%edx, %rax
	movl	%edx, %ecx
	imulq	$-1840700269, %rax, %rax
	sarl	$31, %ecx
	shrq	$32, %rax
	addl	%edx, %eax
	sarl	$2, %eax
	subl	%ecx, %eax
	leal	0(,%rax,8), %ecx
	subl	%eax, %ecx
	subl	%ecx, %edx
	movzbl	260(%rdi), %ecx
	movl	%edx, %eax
	leal	7(%rdx), %edx
	cmovs	%edx, %eax
	movl	$7, %edx
	subl	%eax, %edx
	cmpl	%ecx, %edx
	jl	.L1659
	movl	$1, %edx
	subl	%eax, %edx
	movl	%edx, %r15d
.L1400:
	movq	(%r14), %rax
	movq	%r13, %rdx
	movl	$6, %esi
	movq	%r14, %rdi
	movl	$1, %r13d
	call	*176(%rax)
	movl	36(%r14), %edx
	movl	%eax, %esi
	subl	%edx, %eax
	addl	%eax, %r12d
	movslq	%r12d, %rax
	movl	%r12d, %ecx
	imulq	$-1840700269, %rax, %rax
	sarl	$31, %ecx
	shrq	$32, %rax
	addl	%r12d, %eax
	sarl	$2, %eax
	subl	%ecx, %eax
	leal	0(,%rax,8), %ecx
	subl	%eax, %ecx
	movl	%ecx, %eax
	movl	%esi, %ecx
	subl	%r12d, %ecx
	leal	7(%rcx,%rax), %ecx
	leal	(%rdx,%rbx,8), %eax
	subl	%ebx, %eax
	subl	%r15d, %ecx
	subl	%r15d, %eax
	cltd
	idivl	%ecx
	addl	%edx, %ecx
	testl	%edx, %edx
	cmovns	%edx, %ecx
	addl	%r15d, %ecx
	testl	%ecx, %ecx
	cmovle	%r13d, %ecx
	cmpl	%ecx, %esi
	cmovle	%esi, %ecx
	cmpb	$0, 11(%r14)
	movl	%ecx, %r13d
	je	.L1402
	movq	(%r14), %rax
	leaq	_ZN6icu_678Calendar13computeFieldsER10UErrorCode(%rip), %rdx
	leaq	-60(%rbp), %rsi
	movq	%r14, %rdi
	movl	$0, -60(%rbp)
	movq	232(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1403
	call	_ZN6icu_678Calendar13computeFieldsER10UErrorCode.part.0
.L1402:
	movl	220(%r14), %eax
	movl	%r13d, 36(%r14)
	cmpl	$10000, %eax
	je	.L1405
	leal	1(%rax), %edx
.L1406:
	movl	%edx, 220(%r14)
	movl	%eax, 152(%r14)
	movb	$1, 110(%r14)
	movl	$0, 20(%r14)
	movl	$0, 136(%r14)
	movb	$0, 106(%r14)
	movl	$0, 8(%r14)
	jmp	.L1100
	.p2align 4,,10
	.p2align 3
.L1104:
	addl	92(%rdi), %ebx
	movl	$20, %esi
	movl	%ebx, %edx
	call	_ZN6icu_678Calendar3setE19UCalendarDateFieldsi
	jmp	.L1100
	.p2align 4,,10
	.p2align 3
.L1109:
	movl	32(%rdi), %eax
	movq	%r13, %rdx
	movl	$5, %esi
	subl	$1, %eax
	movslq	%eax, %r12
	imulq	$-1840700269, %r12, %r12
	shrq	$32, %r12
	addl	%eax, %r12d
	sarl	$31, %eax
	sarl	$2, %r12d
	subl	%eax, %r12d
	movq	(%rdi), %rax
	call	*176(%rax)
	pxor	%xmm1, %xmm1
	subl	32(%r14), %eax
	movsd	.LC9(%rip), %xmm3
	cvtsi2sdl	%r12d, %xmm1
	movl	%eax, %edx
	cltq
	movsd	224(%r14), %xmm4
	imulq	$-1840700269, %rax, %rax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%ebx, %xmm0
	movapd	%xmm4, %xmm2
	mulsd	%xmm3, %xmm1
	shrq	$32, %rax
	addl	%edx, %eax
	sarl	$31, %edx
	mulsd	%xmm3, %xmm0
	sarl	$2, %eax
	subl	%edx, %eax
	leal	1(%r12,%rax), %eax
	subsd	%xmm1, %xmm2
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
	addsd	%xmm4, %xmm0
	movsd	%xmm2, -72(%rbp)
	mulsd	%xmm3, %xmm1
	subsd	%xmm2, %xmm0
	movsd	%xmm1, -80(%rbp)
	call	uprv_fmod_67@PLT
	pxor	%xmm3, %xmm3
	movsd	-72(%rbp), %xmm2
	comisd	%xmm0, %xmm3
	jbe	.L1426
	movsd	-80(%rbp), %xmm1
	addsd	%xmm1, %xmm0
	jmp	.L1426
	.p2align 4,,10
	.p2align 3
.L1105:
	addl	88(%rdi), %ebx
	movl	$19, %esi
	movl	%ebx, %edx
	leaq	_ZN6icu_678Calendar8pinFieldE19UCalendarDateFieldsR10UErrorCode(%rip), %rbx
	call	_ZN6icu_678Calendar3setE19UCalendarDateFieldsi
	movq	(%r14), %rax
	movq	%rbx, -80(%rbp)
	movq	%r13, %rdx
	movl	$2, %esi
	movq	%r14, %rdi
	movq	352(%rax), %rcx
	cmpq	%rbx, %rcx
	jne	.L1288
	call	*176(%rax)
	leaq	_ZNK6icu_678Calendar16getActualMinimumE19UCalendarDateFieldsR10UErrorCode(%rip), %rdx
	movl	%eax, -84(%rbp)
	movq	(%r14), %rax
	movq	168(%rax), %rcx
	cmpq	%rdx, %rcx
	jne	.L1289
	movq	144(%rax), %rdx
	leaq	_ZNK6icu_678Calendar18getGreatestMinimumE19UCalendarDateFields(%rip), %rcx
	cmpq	%rcx, %rdx
	jne	.L1290
	movq	256(%rax), %rcx
	leaq	_ZNK6icu_678Calendar8getLimitE19UCalendarDateFieldsNS0_10ELimitTypeE(%rip), %rdx
	movl	$2, %esi
	movq	%r14, %rdi
	cmpq	%rdx, %rcx
	movl	$1, %edx
	jne	.L1291
	call	*248(%rax)
	movl	%eax, -72(%rbp)
.L1292:
	movq	(%r14), %rax
	leaq	_ZNK6icu_678Calendar10getMinimumE19UCalendarDateFields(%rip), %rcx
	movq	112(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L1293
	movq	256(%rax), %rcx
	leaq	_ZNK6icu_678Calendar8getLimitE19UCalendarDateFieldsNS0_10ELimitTypeE(%rip), %rdx
	movl	$2, %esi
	movq	%r14, %rdi
	cmpq	%rdx, %rcx
	movl	$0, %edx
	jne	.L1294
	call	*248(%rax)
	movl	%eax, %ebx
.L1295:
	movq	(%r14), %r10
	cmpl	%ebx, -72(%rbp)
	je	.L1296
	movq	%r14, %rdi
	call	*24(%r10)
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1660
	movb	$1, 232(%rax)
	movl	-72(%rbp), %r12d
	.p2align 4,,10
	.p2align 3
.L1312:
	cmpb	$0, 11(%r15)
	je	.L1298
	movq	(%r15), %rax
	leaq	_ZN6icu_678Calendar13computeFieldsER10UErrorCode(%rip), %rdi
	movl	$0, -60(%rbp)
	leaq	-60(%rbp), %rsi
	movq	232(%rax), %rax
	cmpq	%rdi, %rax
	movq	%r15, %rdi
	jne	.L1299
	call	_ZN6icu_678Calendar13computeFieldsER10UErrorCode.part.0
.L1298:
	movl	220(%r15), %eax
	movl	%r12d, 20(%r15)
	cmpl	$10000, %eax
	je	.L1301
	leal	1(%rax), %r10d
.L1302:
	movl	%eax, 136(%r15)
	xorl	%eax, %eax
	movb	$1, 106(%r15)
	movb	$0, 11(%r15)
	movw	%ax, 8(%r15)
	movl	0(%r13), %eax
	movl	%r10d, 220(%r15)
	testl	%eax, %eax
	jle	.L1308
.L1310:
	xorl	%eax, %eax
.L1309:
	cmpl	%eax, %r12d
	jne	.L1311
	leal	-1(%r12), %eax
	movl	%r12d, -72(%rbp)
	cmpl	%eax, %ebx
	jg	.L1311
	movl	%eax, %r12d
	jmp	.L1312
	.p2align 4,,10
	.p2align 3
.L1188:
	movslq	%r12d, %rax
	movl	12(%r14,%rax,4), %eax
	subl	%ebx, %eax
	movl	%eax, %r15d
	testl	%eax, %eax
	jle	.L1661
	movq	(%r14), %rax
	movq	%r13, %rdx
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	*176(%rax)
	movl	%eax, %ecx
	cmpl	$32767, %eax
	jle	.L1193
.L1192:
	testl	%r15d, %r15d
	movl	$1, %eax
	cmovle	%eax, %r15d
.L1191:
	movl	%r15d, %edx
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	_ZN6icu_678Calendar3setE19UCalendarDateFieldsi
	movq	(%r14), %rax
	movq	%r13, %rdx
	movl	$2, %esi
	leaq	_ZN6icu_678Calendar8pinFieldE19UCalendarDateFieldsR10UErrorCode(%rip), %rbx
	movq	%r14, %rdi
	movq	352(%rax), %rcx
	movq	%rbx, -80(%rbp)
	cmpq	%rbx, %rcx
	jne	.L1194
	call	*176(%rax)
	leaq	_ZNK6icu_678Calendar16getActualMinimumE19UCalendarDateFieldsR10UErrorCode(%rip), %rdx
	movl	%eax, -84(%rbp)
	movq	(%r14), %rax
	movq	168(%rax), %rcx
	cmpq	%rdx, %rcx
	jne	.L1195
	movq	144(%rax), %rdx
	leaq	_ZNK6icu_678Calendar18getGreatestMinimumE19UCalendarDateFields(%rip), %rcx
	cmpq	%rcx, %rdx
	jne	.L1196
	movq	256(%rax), %rcx
	leaq	_ZNK6icu_678Calendar8getLimitE19UCalendarDateFieldsNS0_10ELimitTypeE(%rip), %rdx
	movl	$2, %esi
	movq	%r14, %rdi
	cmpq	%rdx, %rcx
	movl	$1, %edx
	jne	.L1197
	call	*248(%rax)
	movl	%eax, -72(%rbp)
.L1198:
	movq	(%r14), %rax
	leaq	_ZNK6icu_678Calendar10getMinimumE19UCalendarDateFields(%rip), %rcx
	movq	112(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L1199
	movq	256(%rax), %rcx
	leaq	_ZNK6icu_678Calendar8getLimitE19UCalendarDateFieldsNS0_10ELimitTypeE(%rip), %rdx
	movl	$2, %esi
	movq	%r14, %rdi
	cmpq	%rdx, %rcx
	movl	$0, %edx
	jne	.L1200
	call	*248(%rax)
	movl	%eax, %ebx
.L1201:
	movq	(%r14), %r10
	cmpl	%ebx, -72(%rbp)
	je	.L1202
	movq	%r14, %rdi
	call	*24(%r10)
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1662
	movb	$1, 232(%rax)
	movl	-72(%rbp), %r12d
	.p2align 4,,10
	.p2align 3
.L1218:
	cmpb	$0, 11(%r15)
	je	.L1204
	movq	(%r15), %rax
	leaq	_ZN6icu_678Calendar13computeFieldsER10UErrorCode(%rip), %rdi
	movl	$0, -60(%rbp)
	leaq	-60(%rbp), %rsi
	movq	232(%rax), %rax
	cmpq	%rdi, %rax
	movq	%r15, %rdi
	jne	.L1205
	call	_ZN6icu_678Calendar13computeFieldsER10UErrorCode.part.0
.L1204:
	movl	220(%r15), %eax
	movl	%r12d, 20(%r15)
	cmpl	$10000, %eax
	je	.L1207
	leal	1(%rax), %r10d
.L1208:
	xorl	%r9d, %r9d
	movl	%r10d, 220(%r15)
	movb	$1, 106(%r15)
	movb	$0, 11(%r15)
	movw	%r9w, 8(%r15)
	movl	0(%r13), %r10d
	movl	%eax, 136(%r15)
	testl	%r10d, %r10d
	jle	.L1214
.L1216:
	xorl	%eax, %eax
.L1215:
	cmpl	%eax, %r12d
	jne	.L1217
	leal	-1(%r12), %eax
	movl	%r12d, -72(%rbp)
	cmpl	%eax, %ebx
	jg	.L1217
	movl	%eax, %r12d
	jmp	.L1218
	.p2align 4,,10
	.p2align 3
.L1160:
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_678Calendar8completeER10UErrorCode
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L1162
	movl	32(%r15), %eax
	jmp	.L1161
	.p2align 4,,10
	.p2align 3
.L1308:
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_678Calendar8completeER10UErrorCode
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L1310
	movl	20(%r15), %eax
	jmp	.L1309
	.p2align 4,,10
	.p2align 3
.L1656:
	movq	232(%r10), %rax
	leaq	_ZN6icu_678Calendar13computeFieldsER10UErrorCode(%rip), %rdx
	leaq	-60(%rbp), %rsi
	movq	%r14, %rdi
	movl	$0, -60(%rbp)
	cmpq	%rdx, %rax
	jne	.L1131
	call	_ZN6icu_678Calendar13computeFieldsER10UErrorCode.part.0
.L1132:
	movq	(%r14), %r10
	jmp	.L1130
	.p2align 4,,10
	.p2align 3
.L1655:
	movl	$8, %edx
	subl	%eax, %edx
	movl	%edx, %r15d
	jmp	.L1385
	.p2align 4,,10
	.p2align 3
.L1659:
	movl	$8, %edx
	subl	%eax, %edx
	movl	%edx, %r15d
	jmp	.L1400
	.p2align 4,,10
	.p2align 3
.L1214:
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_678Calendar8completeER10UErrorCode
	movl	0(%r13), %r8d
	testl	%r8d, %r8d
	jg	.L1216
	movl	20(%r15), %eax
	jmp	.L1215
	.p2align 4,,10
	.p2align 3
.L1658:
	movsd	-80(%rbp), %xmm1
	addsd	%xmm1, %xmm0
	jmp	.L1412
	.p2align 4,,10
	.p2align 3
.L1653:
	addsd	.LC9(%rip), %xmm0
	jmp	.L1426
	.p2align 4,,10
	.p2align 3
.L1652:
	movl	12(%r14), %r11d
	testl	%r11d, %r11d
	je	.L1186
	setg	%dl
.L1189:
	movslq	%r12d, %rax
	addl	12(%r14,%rax,4), %ebx
	movl	%ebx, %r15d
	testl	%ebx, %ebx
	jg	.L1432
	testb	%dl, %dl
	je	.L1191
.L1432:
	movq	(%r14), %rax
	movq	%r13, %rdx
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	*176(%rax)
	movl	%eax, %ecx
	cmpl	$32767, %eax
	jg	.L1192
	testl	%r15d, %r15d
	jg	.L1193
	movl	%r15d, %eax
	negl	%eax
	cltd
	idivl	%ecx
	subl	%edx, %ecx
	movl	%ecx, %r15d
	jmp	.L1191
	.p2align 4,,10
	.p2align 3
.L1661:
	movl	$1, %r15d
	jmp	.L1191
	.p2align 4,,10
	.p2align 3
.L1142:
	movl	$5, %esi
	movq	%r14, %rdi
	call	*%rdx
	movl	%eax, %ebx
	jmp	.L1144
	.p2align 4,,10
	.p2align 3
.L1290:
	movl	$2, %esi
	movq	%r14, %rdi
	call	*%rdx
	movl	%eax, -72(%rbp)
	jmp	.L1292
.L1657:
	movl	$7, 0(%r13)
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L1148:
	movl	32(%r14), %eax
	cmpl	%eax, -80(%rbp)
	jl	.L1663
	cmpl	%eax, %ebx
	jle	.L1100
	cmpb	$0, 11(%r14)
	je	.L1176
	movq	(%r14), %rax
	leaq	_ZN6icu_678Calendar13computeFieldsER10UErrorCode(%rip), %rdx
	leaq	-60(%rbp), %rsi
	movq	%r14, %rdi
	movl	$0, -60(%rbp)
	movq	232(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1177
	call	_ZN6icu_678Calendar13computeFieldsER10UErrorCode.part.0
.L1176:
	movl	220(%r14), %eax
	movl	%ebx, 32(%r14)
	cmpl	$10000, %eax
	jne	.L1638
	movl	$1, 220(%r14)
	movl	$1, %edi
	.p2align 4,,10
	.p2align 3
.L1185:
	movl	$10000, %ecx
	xorl	%eax, %eax
	movl	$-1, %esi
	.p2align 4,,10
	.p2align 3
.L1183:
	movl	128(%r14,%rax,4), %edx
	cmpl	%edi, %edx
	jle	.L1181
	cmpl	%ecx, %edx
	cmovl	%edx, %ecx
	cmovl	%eax, %esi
.L1181:
	addq	$1, %rax
	cmpq	$23, %rax
	jne	.L1183
	leal	1(%rdi), %eax
	leal	2(%rdi), %edx
	cmpl	$-1, %esi
	je	.L1391
	movslq	%esi, %rsi
	movl	%eax, 220(%r14)
	movl	%eax, 128(%r14,%rsi,4)
	cmpl	$24, %eax
	je	.L1637
	movl	%eax, %edi
	jmp	.L1185
	.p2align 4,,10
	.p2align 3
.L1311:
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*8(%rax)
	movl	0(%r13), %ebx
	movq	(%r14), %r10
	testl	%ebx, %ebx
	jg	.L1313
	movl	-72(%rbp), %ebx
.L1296:
	movl	20(%r14), %eax
	cmpl	%eax, -84(%rbp)
	jl	.L1664
	cmpl	%eax, %ebx
	jg	.L1665
.L1325:
	movq	352(%r10), %rax
	movq	%r13, %rdx
	movl	$5, %esi
	movq	%r14, %rdi
	cmpq	-80(%rbp), %rax
	jne	.L1336
	call	*176(%r10)
	leaq	_ZNK6icu_678Calendar16getActualMinimumE19UCalendarDateFieldsR10UErrorCode(%rip), %rdx
	movl	%eax, -80(%rbp)
	movq	(%r14), %rax
	movq	168(%rax), %rcx
	cmpq	%rdx, %rcx
	jne	.L1337
	movq	144(%rax), %rdx
	leaq	_ZNK6icu_678Calendar18getGreatestMinimumE19UCalendarDateFields(%rip), %rcx
	cmpq	%rcx, %rdx
	jne	.L1338
	movq	256(%rax), %rcx
	leaq	_ZNK6icu_678Calendar8getLimitE19UCalendarDateFieldsNS0_10ELimitTypeE(%rip), %rdx
	movl	$5, %esi
	movq	%r14, %rdi
	cmpq	%rdx, %rcx
	movl	$1, %edx
	jne	.L1339
	call	*248(%rax)
	movl	%eax, %ebx
.L1340:
	movq	(%r14), %rax
	leaq	_ZNK6icu_678Calendar10getMinimumE19UCalendarDateFields(%rip), %rcx
	movq	112(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L1341
	movq	256(%rax), %rcx
	leaq	_ZNK6icu_678Calendar8getLimitE19UCalendarDateFieldsNS0_10ELimitTypeE(%rip), %rdx
	movl	$5, %esi
	movq	%r14, %rdi
	cmpq	%rdx, %rcx
	movl	$0, %edx
	jne	.L1342
	call	*248(%rax)
	movl	%eax, -72(%rbp)
.L1343:
	cmpl	-72(%rbp), %ebx
	je	.L1344
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*24(%rax)
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1666
	movb	$1, 232(%rax)
	movl	%ebx, %r12d
	.p2align 4,,10
	.p2align 3
.L1360:
	cmpb	$0, 11(%r15)
	je	.L1346
	movq	(%r15), %rax
	leaq	_ZN6icu_678Calendar13computeFieldsER10UErrorCode(%rip), %rdi
	movl	$0, -60(%rbp)
	leaq	-60(%rbp), %rsi
	movq	232(%rax), %rax
	cmpq	%rdi, %rax
	movq	%r15, %rdi
	jne	.L1347
	call	_ZN6icu_678Calendar13computeFieldsER10UErrorCode.part.0
.L1346:
	movl	220(%r15), %eax
	movl	%r12d, 32(%r15)
	cmpl	$10000, %eax
	je	.L1349
	leal	1(%rax), %esi
.L1350:
	xorl	%r8d, %r8d
	movb	$1, 109(%r15)
	movb	$0, 11(%r15)
	movw	%r8w, 8(%r15)
	movl	0(%r13), %r9d
	movl	%esi, 220(%r15)
	movl	%eax, 148(%r15)
	testl	%r9d, %r9d
	jle	.L1356
.L1358:
	xorl	%eax, %eax
.L1357:
	cmpl	%eax, %r12d
	jne	.L1359
	leal	-1(%r12), %eax
	movl	%r12d, %ebx
	cmpl	%eax, -72(%rbp)
	jg	.L1359
	movl	%eax, %r12d
	jmp	.L1360
	.p2align 4,,10
	.p2align 3
.L1356:
	movq	%r15, %rdi
	movq	%r13, %rsi
	call	_ZN6icu_678Calendar8completeER10UErrorCode
	movl	0(%r13), %edi
	testl	%edi, %edi
	jg	.L1358
	movl	32(%r15), %eax
	jmp	.L1357
.L1666:
	movl	$7, 0(%r13)
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L1344:
	movl	32(%r14), %eax
	cmpl	%eax, -80(%rbp)
	jl	.L1667
	cmpl	%eax, %ebx
	jle	.L1100
	cmpb	$0, 11(%r14)
	je	.L1372
	movq	(%r14), %rax
	leaq	_ZN6icu_678Calendar13computeFieldsER10UErrorCode(%rip), %rdx
	leaq	-60(%rbp), %rsi
	movq	%r14, %rdi
	movl	$0, -60(%rbp)
	movq	232(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1373
	call	_ZN6icu_678Calendar13computeFieldsER10UErrorCode.part.0
.L1372:
	movl	220(%r14), %eax
	movl	%ebx, 32(%r14)
	cmpl	$10000, %eax
	jne	.L1638
	movl	$1, 220(%r14)
	movl	$1, %edi
	.p2align 4,,10
	.p2align 3
.L1381:
	movl	$10000, %ecx
	xorl	%eax, %eax
	movl	$-1, %esi
	.p2align 4,,10
	.p2align 3
.L1379:
	movl	128(%r14,%rax,4), %edx
	cmpl	%edi, %edx
	jle	.L1377
	cmpl	%ecx, %edx
	cmovl	%edx, %ecx
	cmovl	%eax, %esi
.L1377:
	addq	$1, %rax
	cmpq	$23, %rax
	jne	.L1379
	leal	1(%rdi), %eax
	leal	2(%rdi), %edx
	cmpl	$-1, %esi
	je	.L1391
	movslq	%esi, %rsi
	movl	%eax, 220(%r14)
	movl	%eax, 128(%r14,%rsi,4)
	cmpl	$24, %eax
	je	.L1637
	movl	%eax, %edi
	jmp	.L1381
	.p2align 4,,10
	.p2align 3
.L1217:
	movq	(%r15), %rax
	movq	%r15, %rdi
	xorl	%ebx, %ebx
	call	*8(%rax)
	movl	0(%r13), %edi
	movq	(%r14), %r10
	testl	%edi, %edi
	jg	.L1202
	movl	-72(%rbp), %ebx
.L1202:
	movl	20(%r14), %eax
	cmpl	%eax, -84(%rbp)
	jl	.L1668
	cmpl	%eax, %ebx
	jg	.L1669
.L1231:
	movq	352(%r10), %rax
	movq	%r13, %rdx
	movl	$5, %esi
	movq	%r14, %rdi
	cmpq	-80(%rbp), %rax
	jne	.L1336
	call	*176(%r10)
	leaq	_ZNK6icu_678Calendar16getActualMinimumE19UCalendarDateFieldsR10UErrorCode(%rip), %rdx
	movl	%eax, -80(%rbp)
	movq	(%r14), %rax
	movq	168(%rax), %rcx
	cmpq	%rdx, %rcx
	jne	.L1243
	movq	144(%rax), %rdx
	leaq	_ZNK6icu_678Calendar18getGreatestMinimumE19UCalendarDateFields(%rip), %rcx
	cmpq	%rcx, %rdx
	jne	.L1244
	movq	256(%rax), %rcx
	leaq	_ZNK6icu_678Calendar8getLimitE19UCalendarDateFieldsNS0_10ELimitTypeE(%rip), %rdx
	movl	$5, %esi
	movq	%r14, %rdi
	cmpq	%rdx, %rcx
	movl	$1, %edx
	jne	.L1245
	call	*248(%rax)
	movl	%eax, %ebx
.L1246:
	movq	(%r14), %rax
	leaq	_ZNK6icu_678Calendar10getMinimumE19UCalendarDateFields(%rip), %rcx
	movq	112(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L1247
	movq	256(%rax), %rcx
	leaq	_ZNK6icu_678Calendar8getLimitE19UCalendarDateFieldsNS0_10ELimitTypeE(%rip), %rdx
	movl	$5, %esi
	movq	%r14, %rdi
	cmpq	%rdx, %rcx
	movl	$0, %edx
	jne	.L1248
	call	*248(%rax)
	movl	%eax, -72(%rbp)
.L1249:
	cmpl	-72(%rbp), %ebx
	je	.L1250
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*24(%rax)
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1670
	movb	$1, 232(%rax)
	movl	%ebx, %r12d
	.p2align 4,,10
	.p2align 3
.L1266:
	cmpb	$0, 11(%r15)
	je	.L1252
	movq	(%r15), %rax
	leaq	_ZN6icu_678Calendar13computeFieldsER10UErrorCode(%rip), %rdi
	movl	$0, -60(%rbp)
	leaq	-60(%rbp), %rsi
	movq	232(%rax), %rax
	cmpq	%rdi, %rax
	movq	%r15, %rdi
	jne	.L1253
	call	_ZN6icu_678Calendar13computeFieldsER10UErrorCode.part.0
.L1252:
	movl	220(%r15), %eax
	movl	%r12d, 32(%r15)
	cmpl	$10000, %eax
	je	.L1255
	leal	1(%rax), %esi
.L1256:
	xorl	%edx, %edx
	movb	$1, 109(%r15)
	movb	$0, 11(%r15)
	movw	%dx, 8(%r15)
	movl	0(%r13), %ecx
	movl	%esi, 220(%r15)
	movl	%eax, 148(%r15)
	testl	%ecx, %ecx
	jle	.L1262
.L1264:
	xorl	%eax, %eax
.L1263:
	cmpl	%eax, %r12d
	jne	.L1265
	leal	-1(%r12), %eax
	movl	%r12d, %ebx
	cmpl	%eax, -72(%rbp)
	jg	.L1265
	movl	%eax, %r12d
	jmp	.L1266
	.p2align 4,,10
	.p2align 3
.L1262:
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_678Calendar8completeER10UErrorCode
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L1264
	movl	32(%r15), %eax
	jmp	.L1263
.L1670:
	movl	$7, 0(%r13)
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L1250:
	movl	32(%r14), %eax
	cmpl	%eax, -80(%rbp)
	jl	.L1671
	cmpl	%eax, %ebx
	jle	.L1100
	cmpb	$0, 11(%r14)
	je	.L1278
	movq	(%r14), %rax
	leaq	_ZN6icu_678Calendar13computeFieldsER10UErrorCode(%rip), %rdx
	leaq	-60(%rbp), %rsi
	movq	%r14, %rdi
	movl	$0, -60(%rbp)
	movq	232(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1279
	call	_ZN6icu_678Calendar13computeFieldsER10UErrorCode.part.0
.L1278:
	movl	220(%r14), %eax
	movl	%ebx, 32(%r14)
	cmpl	$10000, %eax
	jne	.L1638
	movl	$1, 220(%r14)
	movl	$1, %edi
	.p2align 4,,10
	.p2align 3
.L1287:
	movl	$10000, %ecx
	xorl	%eax, %eax
	movl	$-1, %esi
	.p2align 4,,10
	.p2align 3
.L1285:
	movl	128(%r14,%rax,4), %edx
	cmpl	%edx, %ecx
	jle	.L1283
	cmpl	%edx, %edi
	cmovl	%edx, %ecx
	cmovl	%eax, %esi
.L1283:
	addq	$1, %rax
	cmpq	$23, %rax
	jne	.L1285
	leal	1(%rdi), %eax
	leal	2(%rdi), %edx
	cmpl	$-1, %esi
	je	.L1391
	movslq	%esi, %rsi
	movl	%eax, 220(%r14)
	movl	%eax, 128(%r14,%rsi,4)
	cmpl	$24, %eax
	je	.L1637
	movl	%eax, %edi
	jmp	.L1287
	.p2align 4,,10
	.p2align 3
.L1255:
	movl	$1, 220(%r15)
	movl	$1, %edi
	.p2align 4,,10
	.p2align 3
.L1261:
	movl	$10000, %r10d
	xorl	%eax, %eax
	movl	$-1, %edx
	.p2align 4,,10
	.p2align 3
.L1259:
	movl	128(%r15,%rax,4), %esi
	cmpl	%esi, %edi
	jge	.L1257
	cmpl	%r10d, %esi
	cmovl	%esi, %r10d
	cmovl	%eax, %edx
.L1257:
	addq	$1, %rax
	cmpq	$23, %rax
	jne	.L1259
	leal	1(%rdi), %eax
	leal	2(%rdi), %esi
	cmpl	$-1, %edx
	je	.L1256
	movslq	%edx, %rdx
	movl	%eax, 220(%r15)
	movl	%eax, 128(%r15,%rdx,4)
	cmpl	$24, %eax
	je	.L1672
	movl	%eax, %edi
	jmp	.L1261
	.p2align 4,,10
	.p2align 3
.L1349:
	movl	$1, 220(%r15)
	movl	$1, %edi
	.p2align 4,,10
	.p2align 3
.L1355:
	movl	$10000, %r10d
	xorl	%eax, %eax
	movl	$-1, %edx
	.p2align 4,,10
	.p2align 3
.L1353:
	movl	128(%r15,%rax,4), %esi
	cmpl	%edi, %esi
	jle	.L1351
	cmpl	%r10d, %esi
	cmovl	%esi, %r10d
	cmovl	%eax, %edx
.L1351:
	addq	$1, %rax
	cmpq	$23, %rax
	jne	.L1353
	leal	1(%rdi), %eax
	leal	2(%rdi), %esi
	cmpl	$-1, %edx
	je	.L1350
	movslq	%edx, %rdx
	movl	%eax, 220(%r15)
	movl	%eax, 128(%r15,%rdx,4)
	cmpl	$24, %eax
	je	.L1673
	movl	%eax, %edi
	jmp	.L1355
	.p2align 4,,10
	.p2align 3
.L1207:
	movl	$1, 220(%r15)
	movl	$1, %esi
	.p2align 4,,10
	.p2align 3
.L1213:
	movl	$10000, %r11d
	xorl	%eax, %eax
	movl	$-1, %edx
	.p2align 4,,10
	.p2align 3
.L1211:
	movl	128(%r15,%rax,4), %r10d
	cmpl	%r10d, %esi
	jge	.L1209
	cmpl	%r11d, %r10d
	cmovl	%r10d, %r11d
	cmovl	%eax, %edx
.L1209:
	addq	$1, %rax
	cmpq	$23, %rax
	jne	.L1211
	leal	1(%rsi), %eax
	leal	2(%rsi), %r10d
	cmpl	$-1, %edx
	je	.L1208
	movslq	%edx, %rdx
	movl	%eax, 220(%r15)
	movl	%eax, 128(%r15,%rdx,4)
	cmpl	$24, %eax
	je	.L1674
	movl	%eax, %esi
	jmp	.L1213
	.p2align 4,,10
	.p2align 3
.L1153:
	movl	$1, 220(%r15)
	movl	$1, %edi
	.p2align 4,,10
	.p2align 3
.L1159:
	movl	$10000, %r10d
	xorl	%eax, %eax
	movl	$-1, %edx
	.p2align 4,,10
	.p2align 3
.L1157:
	movl	128(%r15,%rax,4), %esi
	cmpl	%edi, %esi
	jle	.L1155
	cmpl	%r10d, %esi
	cmovl	%esi, %r10d
	cmovl	%eax, %edx
.L1155:
	addq	$1, %rax
	cmpq	$23, %rax
	jne	.L1157
	leal	1(%rdi), %eax
	leal	2(%rdi), %esi
	cmpl	$-1, %edx
	je	.L1154
	movslq	%edx, %rdx
	movl	%eax, 220(%r15)
	movl	%eax, 128(%r15,%rdx,4)
	cmpl	$24, %eax
	je	.L1675
	movl	%eax, %edi
	jmp	.L1159
	.p2align 4,,10
	.p2align 3
.L1301:
	movl	$1, 220(%r15)
	movl	$1, %esi
	.p2align 4,,10
	.p2align 3
.L1307:
	movl	$10000, %r11d
	xorl	%eax, %eax
	movl	$-1, %edx
	.p2align 4,,10
	.p2align 3
.L1305:
	movl	128(%r15,%rax,4), %r10d
	cmpl	%esi, %r10d
	jle	.L1303
	cmpl	%r11d, %r10d
	cmovl	%r10d, %r11d
	cmovl	%eax, %edx
.L1303:
	addq	$1, %rax
	cmpq	$23, %rax
	jne	.L1305
	leal	1(%rsi), %eax
	leal	2(%rsi), %r10d
	cmpl	$-1, %edx
	je	.L1302
	movslq	%edx, %rdx
	movl	%eax, 220(%r15)
	movl	%eax, 128(%r15,%rdx,4)
	cmpl	$24, %eax
	je	.L1676
	movl	%eax, %esi
	jmp	.L1307
	.p2align 4,,10
	.p2align 3
.L1336:
	call	*%rax
	jmp	.L1100
	.p2align 4,,10
	.p2align 3
.L1265:
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*8(%rax)
	movl	0(%r13), %eax
	testl	%eax, %eax
	movl	$0, %eax
	cmovg	%eax, %ebx
	jmp	.L1250
	.p2align 4,,10
	.p2align 3
.L1359:
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*8(%rax)
	movl	0(%r13), %esi
	movl	$0, %eax
	testl	%esi, %esi
	cmovg	%eax, %ebx
	jmp	.L1344
	.p2align 4,,10
	.p2align 3
.L1163:
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*8(%rax)
	movl	0(%r13), %eax
	testl	%eax, %eax
	movl	$0, %eax
	cmovg	%eax, %ebx
	jmp	.L1148
	.p2align 4,,10
	.p2align 3
.L1253:
	call	*%rax
	jmp	.L1252
	.p2align 4,,10
	.p2align 3
.L1347:
	call	*%rax
	jmp	.L1346
	.p2align 4,,10
	.p2align 3
.L1299:
	call	*%rax
	jmp	.L1298
	.p2align 4,,10
	.p2align 3
.L1205:
	call	*%rax
	jmp	.L1204
	.p2align 4,,10
	.p2align 3
.L1151:
	call	*%rax
	jmp	.L1150
	.p2align 4,,10
	.p2align 3
.L1672:
	movl	%esi, %eax
	leal	3(%rdi), %esi
	jmp	.L1256
	.p2align 4,,10
	.p2align 3
.L1676:
	movl	%r10d, %eax
	leal	3(%rsi), %r10d
	jmp	.L1302
	.p2align 4,,10
	.p2align 3
.L1674:
	movl	%r10d, %eax
	leal	3(%rsi), %r10d
	jmp	.L1208
	.p2align 4,,10
	.p2align 3
.L1675:
	movl	%esi, %eax
	leal	3(%rdi), %esi
	jmp	.L1154
	.p2align 4,,10
	.p2align 3
.L1673:
	movl	%esi, %eax
	leal	3(%rdi), %esi
	jmp	.L1350
	.p2align 4,,10
	.p2align 3
.L1654:
	movq	%r13, %rsi
	call	*224(%rax)
	movl	0(%r13), %esi
	testl	%esi, %esi
	jg	.L1677
	cmpb	$0, 232(%r14)
	jne	.L1119
	cmpb	$0, 10(%r14)
	jne	.L1120
.L1119:
	movb	$0, 9(%r14)
.L1120:
	movb	$1, 8(%r14)
	movq	(%r14), %rax
	movb	$0, 11(%r14)
	jmp	.L1116
	.p2align 4,,10
	.p2align 3
.L1667:
	cmpb	$0, 11(%r14)
	je	.L1362
	movq	(%r14), %rax
	leaq	_ZN6icu_678Calendar13computeFieldsER10UErrorCode(%rip), %rdx
	leaq	-60(%rbp), %rsi
	movq	%r14, %rdi
	movl	$0, -60(%rbp)
	movq	232(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1363
	call	_ZN6icu_678Calendar13computeFieldsER10UErrorCode.part.0
.L1362:
	movl	-80(%rbp), %eax
	movl	%eax, 32(%r14)
	movl	220(%r14), %eax
	cmpl	$10000, %eax
	jne	.L1638
	movl	$1, 220(%r14)
	movl	$1, %edi
	.p2align 4,,10
	.p2align 3
.L1371:
	movl	$10000, %ecx
	xorl	%eax, %eax
	movl	$-1, %esi
	.p2align 4,,10
	.p2align 3
.L1369:
	movl	128(%r14,%rax,4), %edx
	cmpl	%edx, %edi
	jge	.L1367
	cmpl	%ecx, %edx
	cmovl	%edx, %ecx
	cmovl	%eax, %esi
.L1367:
	addq	$1, %rax
	cmpq	$23, %rax
	jne	.L1369
	leal	1(%rdi), %eax
	leal	2(%rdi), %edx
	cmpl	$-1, %esi
	je	.L1391
	movslq	%esi, %rsi
	movl	%eax, 220(%r14)
	movl	%eax, 128(%r14,%rsi,4)
	cmpl	$24, %eax
	je	.L1637
	movl	%eax, %edi
	jmp	.L1371
	.p2align 4,,10
	.p2align 3
.L1663:
	cmpb	$0, 11(%r14)
	je	.L1166
	movq	(%r14), %rax
	leaq	_ZN6icu_678Calendar13computeFieldsER10UErrorCode(%rip), %rdx
	leaq	-60(%rbp), %rsi
	movq	%r14, %rdi
	movl	$0, -60(%rbp)
	movq	232(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1167
	call	_ZN6icu_678Calendar13computeFieldsER10UErrorCode.part.0
.L1166:
	movl	-80(%rbp), %eax
	movl	%eax, 32(%r14)
	movl	220(%r14), %eax
	cmpl	$10000, %eax
	jne	.L1638
	movl	$1, 220(%r14)
	movl	$1, %edi
	.p2align 4,,10
	.p2align 3
.L1175:
	movl	$10000, %ecx
	xorl	%eax, %eax
	movl	$-1, %esi
	.p2align 4,,10
	.p2align 3
.L1173:
	movl	128(%r14,%rax,4), %edx
	cmpl	%edi, %edx
	jle	.L1171
	cmpl	%ecx, %edx
	cmovl	%edx, %ecx
	cmovl	%eax, %esi
.L1171:
	addq	$1, %rax
	cmpq	$23, %rax
	jne	.L1173
	leal	1(%rdi), %eax
	leal	2(%rdi), %edx
	cmpl	$-1, %esi
	je	.L1391
	movslq	%esi, %rsi
	movl	%eax, 220(%r14)
	movl	%eax, 128(%r14,%rsi,4)
	cmpl	$24, %eax
	je	.L1637
	movl	%eax, %edi
	jmp	.L1175
	.p2align 4,,10
	.p2align 3
.L1668:
	cmpb	$0, 11(%r14)
	je	.L1221
	movq	232(%r10), %rax
	leaq	_ZN6icu_678Calendar13computeFieldsER10UErrorCode(%rip), %rdx
	leaq	-60(%rbp), %rsi
	movq	%r14, %rdi
	movl	$0, -60(%rbp)
	cmpq	%rdx, %rax
	jne	.L1222
	call	_ZN6icu_678Calendar13computeFieldsER10UErrorCode.part.0
.L1223:
	movq	(%r14), %r10
.L1221:
	movl	-84(%rbp), %eax
	movl	%eax, 20(%r14)
	movl	220(%r14), %eax
	cmpl	$10000, %eax
	je	.L1224
.L1634:
	leal	1(%rax), %edx
.L1236:
	xorl	%esi, %esi
	movl	%edx, 220(%r14)
	movl	%eax, 136(%r14)
	movb	$1, 106(%r14)
	movb	$0, 11(%r14)
	movw	%si, 8(%r14)
	jmp	.L1231
	.p2align 4,,10
	.p2align 3
.L1664:
	cmpb	$0, 11(%r14)
	je	.L1315
	movq	232(%r10), %rax
	leaq	_ZN6icu_678Calendar13computeFieldsER10UErrorCode(%rip), %rdx
	leaq	-60(%rbp), %rsi
	movq	%r14, %rdi
	movl	$0, -60(%rbp)
	cmpq	%rdx, %rax
	jne	.L1316
	call	_ZN6icu_678Calendar13computeFieldsER10UErrorCode.part.0
.L1317:
	movq	(%r14), %r10
.L1315:
	movl	-84(%rbp), %eax
	movl	%eax, 20(%r14)
	movl	220(%r14), %eax
	cmpl	$10000, %eax
	je	.L1318
.L1636:
	leal	1(%rax), %edx
.L1330:
	xorl	%r11d, %r11d
	movl	%edx, 220(%r14)
	movl	%eax, 136(%r14)
	movb	$1, 106(%r14)
	movb	$0, 11(%r14)
	movw	%r11w, 8(%r14)
	jmp	.L1325
	.p2align 4,,10
	.p2align 3
.L1671:
	cmpb	$0, 11(%r14)
	je	.L1268
	movq	(%r14), %rax
	leaq	_ZN6icu_678Calendar13computeFieldsER10UErrorCode(%rip), %rdx
	leaq	-60(%rbp), %rsi
	movq	%r14, %rdi
	movl	$0, -60(%rbp)
	movq	232(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1269
	call	_ZN6icu_678Calendar13computeFieldsER10UErrorCode.part.0
.L1268:
	movl	-80(%rbp), %eax
	movl	%eax, 32(%r14)
	movl	220(%r14), %eax
	cmpl	$10000, %eax
	jne	.L1638
	movl	$1, 220(%r14)
	movl	$1, %edi
	.p2align 4,,10
	.p2align 3
.L1277:
	movl	$10000, %ecx
	xorl	%eax, %eax
	movl	$-1, %esi
	.p2align 4,,10
	.p2align 3
.L1275:
	movl	128(%r14,%rax,4), %edx
	cmpl	%edx, %edi
	jge	.L1273
	cmpl	%ecx, %edx
	cmovl	%edx, %ecx
	cmovl	%eax, %esi
.L1273:
	addq	$1, %rax
	cmpq	$23, %rax
	jne	.L1275
	leal	1(%rdi), %eax
	leal	2(%rdi), %edx
	cmpl	$-1, %esi
	je	.L1391
	movslq	%esi, %rsi
	movl	%eax, 220(%r14)
	movl	%eax, 128(%r14,%rsi,4)
	cmpl	$24, %eax
	je	.L1637
	movl	%eax, %edi
	jmp	.L1277
	.p2align 4,,10
	.p2align 3
.L1637:
	movl	%edx, %eax
	leal	3(%rdi), %edx
	jmp	.L1391
	.p2align 4,,10
	.p2align 3
.L1390:
	movl	$1, 220(%r14)
	movl	$1, %edi
	.p2align 4,,10
	.p2align 3
.L1396:
	movl	$10000, %ecx
	xorl	%eax, %eax
	movl	$-1, %esi
	.p2align 4,,10
	.p2align 3
.L1394:
	movl	128(%r14,%rax,4), %edx
	cmpl	%edx, %edi
	jge	.L1392
	cmpl	%ecx, %edx
	cmovl	%edx, %ecx
	cmovl	%eax, %esi
.L1392:
	addq	$1, %rax
	cmpq	$23, %rax
	jne	.L1394
	leal	1(%rdi), %eax
	leal	2(%rdi), %edx
	cmpl	$-1, %esi
	je	.L1391
	movslq	%esi, %rsi
	movl	%eax, 220(%r14)
	movl	%eax, 128(%r14,%rsi,4)
	cmpl	$24, %eax
	je	.L1637
	movl	%eax, %edi
	jmp	.L1396
	.p2align 4,,10
	.p2align 3
.L1133:
	movl	$1, 220(%r14)
	movl	$1, %edi
	.p2align 4,,10
	.p2align 3
.L1139:
	movl	$10000, %ecx
	xorl	%eax, %eax
	movl	$-1, %esi
	.p2align 4,,10
	.p2align 3
.L1137:
	movl	128(%r14,%rax,4), %edx
	cmpl	%edi, %edx
	jle	.L1135
	cmpl	%ecx, %edx
	cmovl	%edx, %ecx
	cmovl	%eax, %esi
.L1135:
	addq	$1, %rax
	cmpq	$23, %rax
	jne	.L1137
	leal	1(%rdi), %eax
	leal	2(%rdi), %edx
	cmpl	$-1, %esi
	je	.L1134
	movslq	%esi, %rsi
	movl	%eax, 220(%r14)
	movl	%eax, 128(%r14,%rsi,4)
	cmpl	$24, %eax
	je	.L1678
	movl	%eax, %edi
	jmp	.L1139
	.p2align 4,,10
	.p2align 3
.L1405:
	movl	$1, 220(%r14)
	movl	$1, %edi
	.p2align 4,,10
	.p2align 3
.L1411:
	movl	$10000, %ecx
	xorl	%eax, %eax
	movl	$-1, %esi
	.p2align 4,,10
	.p2align 3
.L1409:
	movl	128(%r14,%rax,4), %edx
	cmpl	%edi, %edx
	jle	.L1407
	cmpl	%ecx, %edx
	cmovl	%edx, %ecx
	cmovl	%eax, %esi
.L1407:
	addq	$1, %rax
	cmpq	$23, %rax
	jne	.L1409
	leal	1(%rdi), %eax
	leal	2(%rdi), %edx
	cmpl	$-1, %esi
	je	.L1406
	movslq	%esi, %rsi
	movl	%eax, 220(%r14)
	movl	%eax, 128(%r14,%rsi,4)
	cmpl	$24, %eax
	je	.L1679
	movl	%eax, %edi
	jmp	.L1411
	.p2align 4,,10
	.p2align 3
.L1194:
	call	*%rcx
	movq	(%r14), %r10
	jmp	.L1231
	.p2align 4,,10
	.p2align 3
.L1288:
	call	*%rcx
	movq	(%r14), %r10
	jmp	.L1325
	.p2align 4,,10
	.p2align 3
.L1669:
	cmpb	$0, 11(%r14)
	je	.L1232
	movq	232(%r10), %rax
	leaq	_ZN6icu_678Calendar13computeFieldsER10UErrorCode(%rip), %rdx
	leaq	-60(%rbp), %rsi
	movq	%r14, %rdi
	movl	$0, -60(%rbp)
	cmpq	%rdx, %rax
	jne	.L1233
	call	_ZN6icu_678Calendar13computeFieldsER10UErrorCode.part.0
.L1234:
	movq	(%r14), %r10
.L1232:
	movl	220(%r14), %eax
	movl	%ebx, 20(%r14)
	cmpl	$10000, %eax
	jne	.L1634
	movl	$1, 220(%r14)
	movl	$1, %edi
	.p2align 4,,10
	.p2align 3
.L1241:
	movl	$10000, %ecx
	xorl	%eax, %eax
	movl	$-1, %esi
	.p2align 4,,10
	.p2align 3
.L1239:
	movl	128(%r14,%rax,4), %edx
	cmpl	%edx, %edi
	jge	.L1237
	cmpl	%ecx, %edx
	cmovl	%edx, %ecx
	cmovl	%eax, %esi
.L1237:
	addq	$1, %rax
	cmpq	$23, %rax
	jne	.L1239
	leal	1(%rdi), %eax
	leal	2(%rdi), %edx
	cmpl	$-1, %esi
	je	.L1236
	movslq	%esi, %rsi
	movl	%eax, 220(%r14)
	movl	%eax, 128(%r14,%rsi,4)
	cmpl	$24, %eax
	je	.L1633
	movl	%eax, %edi
	jmp	.L1241
	.p2align 4,,10
	.p2align 3
.L1313:
	xorl	%ebx, %ebx
	jmp	.L1296
	.p2align 4,,10
	.p2align 3
.L1665:
	cmpb	$0, 11(%r14)
	je	.L1326
	movq	232(%r10), %rax
	leaq	_ZN6icu_678Calendar13computeFieldsER10UErrorCode(%rip), %rdx
	leaq	-60(%rbp), %rsi
	movq	%r14, %rdi
	movl	$0, -60(%rbp)
	cmpq	%rdx, %rax
	jne	.L1327
	call	_ZN6icu_678Calendar13computeFieldsER10UErrorCode.part.0
.L1328:
	movq	(%r14), %r10
.L1326:
	movl	220(%r14), %eax
	movl	%ebx, 20(%r14)
	cmpl	$10000, %eax
	jne	.L1636
	movl	$1, 220(%r14)
	movl	$1, %edi
	.p2align 4,,10
	.p2align 3
.L1335:
	movl	$10000, %ecx
	xorl	%eax, %eax
	movl	$-1, %esi
	.p2align 4,,10
	.p2align 3
.L1333:
	movl	128(%r14,%rax,4), %edx
	cmpl	%edi, %edx
	jle	.L1331
	cmpl	%ecx, %edx
	cmovl	%edx, %ecx
	cmovl	%eax, %esi
.L1331:
	addq	$1, %rax
	cmpq	$23, %rax
	jne	.L1333
	leal	1(%rdi), %eax
	leal	2(%rdi), %edx
	cmpl	$-1, %esi
	je	.L1330
	movslq	%esi, %rsi
	movl	%eax, 220(%r14)
	movl	%eax, 128(%r14,%rsi,4)
	cmpl	$24, %eax
	je	.L1635
	movl	%eax, %edi
	jmp	.L1335
	.p2align 4,,10
	.p2align 3
.L1289:
	movq	%r13, %rdx
	movl	$2, %esi
	movq	%r14, %rdi
	call	*%rcx
	movq	(%r14), %r10
	movl	%eax, %ebx
	jmp	.L1296
	.p2align 4,,10
	.p2align 3
.L1337:
	movq	%r13, %rdx
	movl	$5, %esi
	movq	%r14, %rdi
	call	*%rcx
	movl	%eax, %ebx
	jmp	.L1344
	.p2align 4,,10
	.p2align 3
.L1195:
	movq	%r13, %rdx
	movl	$2, %esi
	movq	%r14, %rdi
	call	*%rcx
	movq	(%r14), %r10
	movl	%eax, %ebx
	jmp	.L1202
	.p2align 4,,10
	.p2align 3
.L1141:
	movq	%r13, %rdx
	movl	$5, %esi
	movq	%r14, %rdi
	call	*%rcx
	movl	%eax, %ebx
	jmp	.L1148
	.p2align 4,,10
	.p2align 3
.L1243:
	movq	%r13, %rdx
	movl	$5, %esi
	movq	%r14, %rdi
	call	*%rcx
	movl	%eax, %ebx
	jmp	.L1250
	.p2align 4,,10
	.p2align 3
.L1193:
	cmpl	%r15d, %ecx
	jge	.L1191
	leal	-1(%r15), %eax
	cltd
	idivl	%ecx
	leal	1(%rdx), %r15d
	jmp	.L1191
	.p2align 4,,10
	.p2align 3
.L1677:
	movq	(%r14), %rax
	pxor	%xmm1, %xmm1
	jmp	.L1118
	.p2align 4,,10
	.p2align 3
.L1199:
	movl	$2, %esi
	movq	%r14, %rdi
	call	*%rdx
	movl	%eax, %ebx
	jmp	.L1201
	.p2align 4,,10
	.p2align 3
.L1196:
	movl	$2, %esi
	movq	%r14, %rdi
	call	*%rdx
	movl	%eax, -72(%rbp)
	jmp	.L1198
	.p2align 4,,10
	.p2align 3
.L1145:
	movl	$5, %esi
	movq	%r14, %rdi
	call	*%rdx
	movl	%eax, -72(%rbp)
	jmp	.L1147
	.p2align 4,,10
	.p2align 3
.L1338:
	movl	$5, %esi
	movq	%r14, %rdi
	call	*%rdx
	movl	%eax, %ebx
	jmp	.L1340
	.p2align 4,,10
	.p2align 3
.L1341:
	movl	$5, %esi
	movq	%r14, %rdi
	call	*%rdx
	movl	%eax, -72(%rbp)
	jmp	.L1343
	.p2align 4,,10
	.p2align 3
.L1244:
	movl	$5, %esi
	movq	%r14, %rdi
	call	*%rdx
	movl	%eax, %ebx
	jmp	.L1246
	.p2align 4,,10
	.p2align 3
.L1293:
	movl	$2, %esi
	movq	%r14, %rdi
	call	*%rdx
	movl	%eax, %ebx
	jmp	.L1295
	.p2align 4,,10
	.p2align 3
.L1247:
	movl	$5, %esi
	movq	%r14, %rdi
	call	*%rdx
	movl	%eax, -72(%rbp)
	jmp	.L1249
.L1245:
	call	*%rcx
	movl	%eax, %ebx
	jmp	.L1246
.L1248:
	call	*%rcx
	movl	%eax, -72(%rbp)
	jmp	.L1249
.L1143:
	call	*%rcx
	movl	%eax, %ebx
	jmp	.L1144
.L1291:
	call	*%rcx
	movl	%eax, -72(%rbp)
	jmp	.L1292
.L1339:
	call	*%rcx
	movl	%eax, %ebx
	jmp	.L1340
.L1200:
	call	*%rcx
	movl	%eax, %ebx
	jmp	.L1201
.L1342:
	call	*%rcx
	movl	%eax, -72(%rbp)
	jmp	.L1343
.L1197:
	call	*%rcx
	movl	%eax, -72(%rbp)
	jmp	.L1198
.L1146:
	call	*%rcx
	movl	%eax, -72(%rbp)
	jmp	.L1147
.L1294:
	call	*%rcx
	movl	%eax, %ebx
	jmp	.L1295
.L1131:
	call	*%rax
	jmp	.L1132
.L1388:
	call	*%rax
	jmp	.L1387
.L1403:
	call	*%rax
	jmp	.L1402
.L1662:
	movl	$7, 0(%r13)
	movq	(%r14), %r10
	xorl	%ebx, %ebx
	jmp	.L1202
.L1660:
	movl	$7, 0(%r13)
	movq	(%r14), %r10
	xorl	%ebx, %ebx
	jmp	.L1296
.L1678:
	movl	%edx, %eax
	leal	3(%rdi), %edx
	jmp	.L1134
.L1679:
	movl	%edx, %eax
	leal	3(%rdi), %edx
	jmp	.L1406
.L1224:
	movl	$1, 220(%r14)
	movl	$1, %edi
	.p2align 4,,10
	.p2align 3
.L1230:
	movl	$10000, %ecx
	xorl	%eax, %eax
	movl	$-1, %esi
	.p2align 4,,10
	.p2align 3
.L1228:
	movl	128(%r14,%rax,4), %edx
	cmpl	%edx, %edi
	jge	.L1226
	cmpl	%ecx, %edx
	cmovl	%edx, %ecx
	cmovl	%eax, %esi
.L1226:
	addq	$1, %rax
	cmpq	$23, %rax
	jne	.L1228
	leal	1(%rdi), %eax
	leal	2(%rdi), %edx
	cmpl	$-1, %esi
	je	.L1236
	movslq	%esi, %rsi
	movl	%eax, 220(%r14)
	movl	%eax, 128(%r14,%rsi,4)
	cmpl	$24, %eax
	je	.L1633
	movl	%eax, %edi
	jmp	.L1230
.L1318:
	movl	$1, 220(%r14)
	movl	$1, %edi
	.p2align 4,,10
	.p2align 3
.L1324:
	movl	$10000, %ecx
	xorl	%eax, %eax
	movl	$-1, %esi
	.p2align 4,,10
	.p2align 3
.L1322:
	movl	128(%r14,%rax,4), %edx
	cmpl	%edi, %edx
	jle	.L1320
	cmpl	%ecx, %edx
	cmovl	%edx, %ecx
	cmovl	%eax, %esi
.L1320:
	addq	$1, %rax
	cmpq	$23, %rax
	jne	.L1322
	leal	1(%rdi), %eax
	leal	2(%rdi), %edx
	cmpl	$-1, %esi
	je	.L1330
	movslq	%esi, %rsi
	movl	%eax, 220(%r14)
	movl	%eax, 128(%r14,%rsi,4)
	cmpl	$24, %eax
	je	.L1635
	movl	%eax, %edi
	jmp	.L1324
.L1633:
	movl	%edx, %eax
	leal	3(%rdi), %edx
	jmp	.L1236
.L1635:
	movl	%edx, %eax
	leal	3(%rdi), %edx
	jmp	.L1330
.L1167:
	call	*%rax
	jmp	.L1166
.L1363:
	call	*%rax
	jmp	.L1362
.L1269:
	call	*%rax
	jmp	.L1268
.L1222:
	call	*%rax
	jmp	.L1223
.L1316:
	call	*%rax
	jmp	.L1317
.L1373:
	call	*%rax
	jmp	.L1372
.L1233:
	call	*%rax
	jmp	.L1234
.L1279:
	call	*%rax
	jmp	.L1278
.L1177:
	call	*%rax
	jmp	.L1176
.L1327:
	call	*%rax
	jmp	.L1328
.L1651:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4354:
	.size	_ZN6icu_678Calendar4rollE19UCalendarDateFieldsiR10UErrorCode.part.0, .-_ZN6icu_678Calendar4rollE19UCalendarDateFieldsiR10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Calendar4rollE19UCalendarDateFieldsiR10UErrorCode
	.type	_ZN6icu_678Calendar4rollE19UCalendarDateFieldsiR10UErrorCode, @function
_ZN6icu_678Calendar4rollE19UCalendarDateFieldsiR10UErrorCode:
.LFB3285:
	.cfi_startproc
	endbr64
	testl	%edx, %edx
	jne	.L1701
	ret
	.p2align 4,,10
	.p2align 3
.L1701:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%esi, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	cmpb	$0, 8(%rdi)
	movq	%rdi, %r12
	je	.L1702
	cmpb	$0, 9(%rdi)
	jne	.L1687
	movq	(%rdi), %rax
	leaq	_ZN6icu_678Calendar13computeFieldsER10UErrorCode(%rip), %rdx
	movq	232(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1688
	movl	(%rcx), %esi
	testl	%esi, %esi
	jg	.L1680
.L1692:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_678Calendar13computeFieldsER10UErrorCode.part.0
.L1689:
	movl	0(%r13), %ecx
	testl	%ecx, %ecx
	jg	.L1680
	movl	$257, %edx
	movw	%dx, 9(%r12)
.L1687:
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L1680
.L1691:
	movq	%r13, %rcx
	movl	%r14d, %edx
	movl	%r15d, %esi
	movq	%r12, %rdi
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_678Calendar4rollE19UCalendarDateFieldsiR10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L1680:
	.cfi_restore_state
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1702:
	.cfi_restore_state
	movq	(%rdi), %rax
	movq	%rcx, %rsi
	call	*224(%rax)
	movl	0(%r13), %r8d
	testl	%r8d, %r8d
	jg	.L1680
	cmpb	$0, 232(%r12)
	je	.L1703
.L1684:
	movb	$0, 9(%r12)
.L1685:
	movb	$1, 8(%r12)
	movb	$0, 11(%r12)
	movl	0(%r13), %edi
	testl	%edi, %edi
	jg	.L1680
	cmpb	$0, 9(%r12)
	jne	.L1691
	movq	(%r12), %rax
	leaq	_ZN6icu_678Calendar13computeFieldsER10UErrorCode(%rip), %rdx
	movq	232(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1692
.L1688:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1689
	.p2align 4,,10
	.p2align 3
.L1703:
	cmpb	$0, 10(%r12)
	jne	.L1685
	jmp	.L1684
	.cfi_endproc
.LFE3285:
	.size	_ZN6icu_678Calendar4rollE19UCalendarDateFieldsiR10UErrorCode, .-_ZN6icu_678Calendar4rollE19UCalendarDateFieldsiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Calendar4rollENS0_11EDateFieldsEiR10UErrorCode
	.type	_ZN6icu_678Calendar4rollENS0_11EDateFieldsEiR10UErrorCode, @function
_ZN6icu_678Calendar4rollENS0_11EDateFieldsEiR10UErrorCode:
.LFB3284:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%esi, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	leaq	_ZN6icu_678Calendar4rollE19UCalendarDateFieldsiR10UErrorCode(%rip), %rdx
	subq	$16, %rsp
	movq	(%rdi), %rax
	movq	72(%rax), %r8
	cmpq	%rdx, %r8
	jne	.L1705
	testl	%r14d, %r14d
	jne	.L1723
.L1704:
	addq	$16, %rsp
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1723:
	.cfi_restore_state
	cmpb	$0, 8(%rdi)
	je	.L1724
	cmpb	$0, 9(%rdi)
	jne	.L1712
	movq	232(%rax), %rax
	leaq	_ZN6icu_678Calendar13computeFieldsER10UErrorCode(%rip), %rdx
	cmpq	%rdx, %rax
	jne	.L1713
	movl	(%rcx), %r8d
	testl	%r8d, %r8d
	jg	.L1704
.L1717:
	movq	%rcx, %rsi
	movq	%rcx, -32(%rbp)
	movq	%rdi, -24(%rbp)
	call	_ZN6icu_678Calendar13computeFieldsER10UErrorCode.part.0
	movq	-24(%rbp), %rdi
	movq	-32(%rbp), %rcx
.L1714:
	movl	(%rcx), %esi
	testl	%esi, %esi
	jg	.L1704
	movl	$257, %edx
	movw	%dx, 9(%rdi)
.L1712:
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L1704
.L1716:
	addq	$16, %rsp
	movl	%r14d, %edx
	movl	%r15d, %esi
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_678Calendar4rollE19UCalendarDateFieldsiR10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L1705:
	.cfi_restore_state
	addq	$16, %rsp
	movl	%r14d, %edx
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%r8
	.p2align 4,,10
	.p2align 3
.L1724:
	.cfi_restore_state
	movq	%rcx, -24(%rbp)
	movq	%rcx, %rsi
	movq	%rdi, -32(%rbp)
	call	*224(%rax)
	movq	-24(%rbp), %rcx
	movl	(%rcx), %r10d
	testl	%r10d, %r10d
	jg	.L1704
	movq	-32(%rbp), %rdi
	cmpb	$0, 232(%rdi)
	je	.L1725
.L1709:
	movb	$0, 9(%rdi)
.L1710:
	movb	$1, 8(%rdi)
	movb	$0, 11(%rdi)
	movl	(%rcx), %r9d
	testl	%r9d, %r9d
	jg	.L1704
	cmpb	$0, 9(%rdi)
	jne	.L1716
	movq	(%rdi), %rax
	leaq	_ZN6icu_678Calendar13computeFieldsER10UErrorCode(%rip), %rdx
	movq	232(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1717
.L1713:
	movq	%rcx, -32(%rbp)
	movq	%rcx, %rsi
	movq	%rdi, -24(%rbp)
	call	*%rax
	movq	-32(%rbp), %rcx
	movq	-24(%rbp), %rdi
	jmp	.L1714
	.p2align 4,,10
	.p2align 3
.L1725:
	cmpb	$0, 10(%rdi)
	jne	.L1710
	jmp	.L1709
	.cfi_endproc
.LFE3284:
	.size	_ZN6icu_678Calendar4rollENS0_11EDateFieldsEiR10UErrorCode, .-_ZN6icu_678Calendar4rollENS0_11EDateFieldsEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Calendar20julianDayToDayOfWeekEd
	.type	_ZN6icu_678Calendar20julianDayToDayOfWeekEd, @function
_ZN6icu_678Calendar20julianDayToDayOfWeekEd:
.LFB3278:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addsd	.LC3(%rip), %xmm0
	movsd	.LC2(%rip), %xmm1
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	uprv_fmod_67@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	cvttsd2sil	%xmm0, %edx
	movl	%edx, %eax
	sarb	$7, %al
	andl	$7, %eax
	leal	1(%rax,%rdx), %eax
	ret
	.cfi_endproc
.LFE3278:
	.size	_ZN6icu_678Calendar20julianDayToDayOfWeekEd, .-_ZN6icu_678Calendar20julianDayToDayOfWeekEd
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Calendar28computeGregorianAndDOWFieldsEiR10UErrorCode
	.type	_ZN6icu_678Calendar28computeGregorianAndDOWFieldsEiR10UErrorCode, @function
_ZN6icu_678Calendar28computeGregorianAndDOWFieldsEiR10UErrorCode:
.LFB3279:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	leaq	292(%rdi), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%esi, %r12d
	leaq	284(%rdi), %rsi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	leaq	-28(%rbp), %rcx
	addq	$280, %rdi
	leaq	288(%rbx), %r8
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leal	-2440588(%r12), %eax
	cvtsi2sdl	%eax, %xmm0
	call	_ZN6icu_675Grego11dayToFieldsEdRiS1_S1_S1_S1_@PLT
	pxor	%xmm0, %xmm0
	movsd	.LC2(%rip), %xmm1
	cvtsi2sdl	%r12d, %xmm0
	addsd	.LC3(%rip), %xmm0
	call	uprv_fmod_67@PLT
	movb	$1, 111(%rbx)
	cvttsd2sil	%xmm0, %edx
	movb	$1, 122(%rbx)
	movl	$1, 156(%rbx)
	movl	$1, 200(%rbx)
	movl	%edx, %eax
	sarb	$7, %al
	andl	$7, %eax
	leal	1(%rax,%rdx), %eax
	movzbl	%al, %eax
	movl	%eax, 40(%rbx)
	subl	256(%rbx), %eax
	leal	1(%rax), %edx
	addl	$8, %eax
	testl	%edx, %edx
	cmovle	%eax, %edx
	movl	%edx, 84(%rbx)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1736
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1736:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3279:
	.size	_ZN6icu_678Calendar28computeGregorianAndDOWFieldsEiR10UErrorCode, .-_ZN6icu_678Calendar28computeGregorianAndDOWFieldsEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Calendar22computeGregorianFieldsEiR10UErrorCode
	.type	_ZN6icu_678Calendar22computeGregorianFieldsEiR10UErrorCode, @function
_ZN6icu_678Calendar22computeGregorianFieldsEiR10UErrorCode:
.LFB3280:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	subl	$2440588, %esi
	pxor	%xmm0, %xmm0
	leaq	284(%rdi), %r10
	cvtsi2sdl	%esi, %xmm0
	leaq	280(%rdi), %r9
	movq	%r10, %rsi
	leaq	292(%rdi), %rdx
	leaq	288(%rdi), %r8
	movq	%r9, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-12(%rbp), %rcx
	call	_ZN6icu_675Grego11dayToFieldsEdRiS1_S1_S1_S1_@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1740
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1740:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3280:
	.size	_ZN6icu_678Calendar22computeGregorianFieldsEiR10UErrorCode, .-_ZN6icu_678Calendar22computeGregorianFieldsEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Calendar17computeWeekFieldsER10UErrorCode
	.type	_ZN6icu_678Calendar17computeWeekFieldsER10UErrorCode, @function
_ZN6icu_678Calendar17computeWeekFieldsER10UErrorCode:
.LFB3281:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L1756
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	40(%rdi), %r14d
	movl	36(%rdi), %r15d
	movl	256(%rdi), %r9d
	movq	(%rdi), %r10
	movl	%r14d, %eax
	movl	88(%rdi), %r8d
	subl	%r15d, %eax
	subl	%r9d, %eax
	leal	7001(%rax), %edx
	movslq	%edx, %rax
	movl	%edx, %ecx
	imulq	$-1840700269, %rax, %rax
	sarl	$31, %ecx
	shrq	$32, %rax
	addl	%edx, %eax
	sarl	$2, %eax
	subl	%ecx, %eax
	leal	0(,%rax,8), %ecx
	subl	%eax, %ecx
	subl	%ecx, %edx
	movl	%edx, %eax
	leal	-1(%r15,%rdx), %edx
	movslq	%edx, %rbx
	imulq	$-1840700269, %rbx, %rbx
	shrq	$32, %rbx
	addl	%edx, %ebx
	sarl	$31, %edx
	sarl	$2, %ebx
	subl	%edx, %ebx
	movl	$7, %edx
	subl	%eax, %edx
	movl	%edx, %eax
	movzbl	260(%rdi), %edx
	cmpl	%edx, %eax
	movq	280(%r10), %rdx
	setge	%al
	movzbl	%al, %eax
	addl	%eax, %ebx
	jne	.L1744
	leaq	_ZNK6icu_678Calendar19handleGetYearLengthEi(%rip), %rax
	leal	-1(%r8), %r13d
	cmpq	%rax, %rdx
	jne	.L1745
	movl	%r8d, %esi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	*264(%r10)
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	%r13d, %esi
	movl	%eax, %ebx
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*264(%rax)
	subl	%eax, %ebx
	movl	%ebx, %eax
.L1746:
	movl	%r14d, %edi
	subl	256(%r12), %edi
	addl	%eax, %r15d
	movl	%r13d, %r8d
	movl	%edi, %eax
	subl	%r15d, %eax
	addl	$1, %eax
	movslq	%eax, %rdx
	movl	%eax, %ecx
	imulq	$-1840700269, %rdx, %rdx
	sarl	$31, %ecx
	shrq	$32, %rdx
	addl	%eax, %edx
	sarl	$2, %edx
	subl	%ecx, %edx
	leal	0(,%rdx,8), %ecx
	subl	%edx, %ecx
	subl	%ecx, %eax
	movzbl	260(%r12), %ecx
	movl	%eax, %edx
	leal	7(%rax), %eax
	cmovs	%eax, %edx
	leal	-1(%r15,%rdx), %eax
	movslq	%eax, %rbx
	imulq	$-1840700269, %rbx, %rbx
	shrq	$32, %rbx
	addl	%eax, %ebx
	sarl	$31, %eax
	sarl	$2, %ebx
	subl	%eax, %ebx
	movl	$7, %eax
	subl	%edx, %eax
	cmpl	%ecx, %eax
	setge	%al
	movzbl	%al, %eax
	addl	%eax, %ebx
.L1748:
	movl	32(%r12), %esi
	movl	%ebx, 24(%r12)
	movl	%r8d, 80(%r12)
	subl	%esi, %edi
	leal	1(%rdi), %edx
	movslq	%edx, %rax
	movl	%edx, %edi
	imulq	$-1840700269, %rax, %rax
	sarl	$31, %edi
	shrq	$32, %rax
	addl	%edx, %eax
	sarl	$2, %eax
	subl	%edi, %eax
	leal	0(,%rax,8), %edi
	subl	%eax, %edi
	subl	%edi, %edx
	movl	%edx, %eax
	leal	7(%rdx), %edx
	cmovs	%edx, %eax
	leal	-1(%rsi,%rax), %edi
	movslq	%edi, %rdx
	imulq	$-1840700269, %rdx, %rdx
	shrq	$32, %rdx
	addl	%edi, %edx
	sarl	$31, %edi
	sarl	$2, %edx
	subl	%edi, %edx
	movl	$7, %edi
	subl	%eax, %edi
	cmpl	%ecx, %edi
	setge	%al
	movzbl	%al, %eax
	addl	%eax, %edx
	movl	%edx, 28(%r12)
	leal	-1(%rsi), %edx
	movslq	%edx, %rax
	imulq	$-1840700269, %rax, %rax
	shrq	$32, %rax
	addl	%edx, %eax
	sarl	$31, %edx
	sarl	$2, %eax
	subl	%edx, %eax
	addl	$1, %eax
	movl	%eax, 44(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1744:
	.cfi_restore_state
	leaq	_ZNK6icu_678Calendar19handleGetYearLengthEi(%rip), %rax
	movl	%r9d, -56(%rbp)
	cmpq	%rax, %rdx
	jne	.L1749
	leal	1(%r8), %esi
	movl	%r8d, -52(%rbp)
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	*264(%r10)
	movl	-52(%rbp), %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	%eax, %r13d
	movq	(%r12), %rax
	movq	%r12, %rdi
	movl	%r8d, %esi
	call	*264(%rax)
	movl	-52(%rbp), %r8d
	movl	-56(%rbp), %r9d
	subl	%eax, %r13d
.L1750:
	movzbl	260(%r12), %ecx
	leal	-5(%r13), %eax
	movl	%r14d, %edi
	subl	256(%r12), %edi
	cmpl	%eax, %r15d
	jl	.L1748
	subl	%r9d, %r14d
	addl	$7, %r14d
	movslq	%r14d, %rax
	movl	%r14d, %edx
	imulq	$-1840700269, %rax, %rax
	sarl	$31, %edx
	shrq	$32, %rax
	addl	%r14d, %eax
	sarl	$2, %eax
	subl	%edx, %eax
	leal	0(,%rax,8), %edx
	subl	%eax, %edx
	subl	%edx, %r14d
	leal	(%r14,%r13), %esi
	subl	%r15d, %esi
	movslq	%esi, %rdx
	movl	%esi, %r9d
	imulq	$-1840700269, %rdx, %rdx
	sarl	$31, %r9d
	shrq	$32, %rdx
	addl	%esi, %edx
	sarl	$2, %edx
	subl	%r9d, %edx
	leal	0(,%rdx,8), %r9d
	subl	%edx, %r9d
	subl	%r9d, %esi
	movl	%esi, %edx
	leal	7(%rsi), %esi
	cmovs	%esi, %edx
	movl	$6, %esi
	subl	%edx, %esi
	cmpl	%ecx, %esi
	jl	.L1748
	addl	$7, %r15d
	subl	%r14d, %r15d
	cmpl	%r15d, %r13d
	jge	.L1748
	addl	$1, %r8d
	movl	$1, %ebx
	jmp	.L1748
	.p2align 4,,10
	.p2align 3
.L1756:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L1749:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	%r8d, -52(%rbp)
	movl	%r8d, %esi
	call	*%rdx
	movl	-56(%rbp), %r9d
	movl	-52(%rbp), %r8d
	movl	%eax, %r13d
	jmp	.L1750
	.p2align 4,,10
	.p2align 3
.L1745:
	movl	%r13d, %esi
	call	*%rdx
	jmp	.L1746
	.cfi_endproc
.LFE3281:
	.size	_ZN6icu_678Calendar17computeWeekFieldsER10UErrorCode, .-_ZN6icu_678Calendar17computeWeekFieldsER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Calendar10weekNumberEiii
	.type	_ZN6icu_678Calendar10weekNumberEiii, @function
_ZN6icu_678Calendar10weekNumberEiii:
.LFB3282:
	.cfi_startproc
	endbr64
	subl	256(%rdi), %ecx
	subl	%edx, %ecx
	addl	$1, %ecx
	movslq	%ecx, %rdx
	movl	%ecx, %eax
	imulq	$-1840700269, %rdx, %rdx
	sarl	$31, %eax
	shrq	$32, %rdx
	addl	%ecx, %edx
	sarl	$2, %edx
	subl	%eax, %edx
	leal	0(,%rdx,8), %eax
	subl	%edx, %eax
	subl	%eax, %ecx
	leal	7(%rcx), %eax
	movl	%ecx, %edx
	cmovs	%eax, %edx
	leal	-1(%rdx,%rsi), %ecx
	movslq	%ecx, %rax
	imulq	$-1840700269, %rax, %rax
	shrq	$32, %rax
	addl	%ecx, %eax
	sarl	$31, %ecx
	sarl	$2, %eax
	subl	%ecx, %eax
	movl	$7, %ecx
	subl	%edx, %ecx
	movl	%ecx, %edx
	movzbl	260(%rdi), %ecx
	cmpl	%ecx, %edx
	setge	%dl
	movzbl	%dl, %edx
	addl	%edx, %eax
	ret
	.cfi_endproc
.LFE3282:
	.size	_ZN6icu_678Calendar10weekNumberEiii, .-_ZN6icu_678Calendar10weekNumberEiii
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Calendar13adoptTimeZoneEPNS_8TimeZoneE
	.type	_ZN6icu_678Calendar13adoptTimeZoneEPNS_8TimeZoneE, @function
_ZN6icu_678Calendar13adoptTimeZoneEPNS_8TimeZoneE:
.LFB3290:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L1772
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	240(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1764
	movq	(%rdi), %rax
	call	*8(%rax)
.L1764:
	movq	%r12, 240(%rbx)
	movb	$0, 9(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1772:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE3290:
	.size	_ZN6icu_678Calendar13adoptTimeZoneEPNS_8TimeZoneE, .-_ZN6icu_678Calendar13adoptTimeZoneEPNS_8TimeZoneE
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Calendar11setTimeZoneERKNS_8TimeZoneE
	.type	_ZN6icu_678Calendar11setTimeZoneERKNS_8TimeZoneE, @function
_ZN6icu_678Calendar11setTimeZoneERKNS_8TimeZoneE:
.LFB3291:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rsi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	call	*96(%rax)
	testq	%rax, %rax
	je	.L1775
	movq	240(%rbx), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L1777
	movq	(%rdi), %rax
	call	*8(%rax)
.L1777:
	movq	%r12, 240(%rbx)
	movb	$0, 9(%rbx)
.L1775:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3291:
	.size	_ZN6icu_678Calendar11setTimeZoneERKNS_8TimeZoneE, .-_ZN6icu_678Calendar11setTimeZoneERKNS_8TimeZoneE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678Calendar11getTimeZoneEv
	.type	_ZNK6icu_678Calendar11getTimeZoneEv, @function
_ZNK6icu_678Calendar11getTimeZoneEv:
.LFB3292:
	.cfi_startproc
	endbr64
	movq	240(%rdi), %rax
	ret
	.cfi_endproc
.LFE3292:
	.size	_ZNK6icu_678Calendar11getTimeZoneEv, .-_ZNK6icu_678Calendar11getTimeZoneEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Calendar14orphanTimeZoneEv
	.type	_ZN6icu_678Calendar14orphanTimeZoneEv, @function
_ZN6icu_678Calendar14orphanTimeZoneEv:
.LFB3293:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_678TimeZone13createDefaultEv@PLT
	testq	%rax, %rax
	je	.L1788
	movq	240(%rbx), %r8
	movq	%rax, 240(%rbx)
.L1786:
	addq	$8, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1788:
	.cfi_restore_state
	xorl	%r8d, %r8d
	jmp	.L1786
	.cfi_endproc
.LFE3293:
	.size	_ZN6icu_678Calendar14orphanTimeZoneEv, .-_ZN6icu_678Calendar14orphanTimeZoneEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Calendar10setLenientEa
	.type	_ZN6icu_678Calendar10setLenientEa, @function
_ZN6icu_678Calendar10setLenientEa:
.LFB3294:
	.cfi_startproc
	endbr64
	movb	%sil, 232(%rdi)
	ret
	.cfi_endproc
.LFE3294:
	.size	_ZN6icu_678Calendar10setLenientEa, .-_ZN6icu_678Calendar10setLenientEa
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678Calendar9isLenientEv
	.type	_ZNK6icu_678Calendar9isLenientEv, @function
_ZNK6icu_678Calendar9isLenientEv:
.LFB3295:
	.cfi_startproc
	endbr64
	movzbl	232(%rdi), %eax
	ret
	.cfi_endproc
.LFE3295:
	.size	_ZNK6icu_678Calendar9isLenientEv, .-_ZNK6icu_678Calendar9isLenientEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Calendar25setRepeatedWallTimeOptionE23UCalendarWallTimeOption
	.type	_ZN6icu_678Calendar25setRepeatedWallTimeOptionE23UCalendarWallTimeOption, @function
_ZN6icu_678Calendar25setRepeatedWallTimeOptionE23UCalendarWallTimeOption:
.LFB3296:
	.cfi_startproc
	endbr64
	cmpl	$1, %esi
	jbe	.L1794
	ret
	.p2align 4,,10
	.p2align 3
.L1794:
	movl	%esi, 248(%rdi)
	ret
	.cfi_endproc
.LFE3296:
	.size	_ZN6icu_678Calendar25setRepeatedWallTimeOptionE23UCalendarWallTimeOption, .-_ZN6icu_678Calendar25setRepeatedWallTimeOptionE23UCalendarWallTimeOption
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678Calendar25getRepeatedWallTimeOptionEv
	.type	_ZNK6icu_678Calendar25getRepeatedWallTimeOptionEv, @function
_ZNK6icu_678Calendar25getRepeatedWallTimeOptionEv:
.LFB3297:
	.cfi_startproc
	endbr64
	movl	248(%rdi), %eax
	ret
	.cfi_endproc
.LFE3297:
	.size	_ZNK6icu_678Calendar25getRepeatedWallTimeOptionEv, .-_ZNK6icu_678Calendar25getRepeatedWallTimeOptionEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Calendar24setSkippedWallTimeOptionE23UCalendarWallTimeOption
	.type	_ZN6icu_678Calendar24setSkippedWallTimeOptionE23UCalendarWallTimeOption, @function
_ZN6icu_678Calendar24setSkippedWallTimeOptionE23UCalendarWallTimeOption:
.LFB3298:
	.cfi_startproc
	endbr64
	movl	%esi, 252(%rdi)
	ret
	.cfi_endproc
.LFE3298:
	.size	_ZN6icu_678Calendar24setSkippedWallTimeOptionE23UCalendarWallTimeOption, .-_ZN6icu_678Calendar24setSkippedWallTimeOptionE23UCalendarWallTimeOption
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678Calendar24getSkippedWallTimeOptionEv
	.type	_ZNK6icu_678Calendar24getSkippedWallTimeOptionEv, @function
_ZNK6icu_678Calendar24getSkippedWallTimeOptionEv:
.LFB3299:
	.cfi_startproc
	endbr64
	movl	252(%rdi), %eax
	ret
	.cfi_endproc
.LFE3299:
	.size	_ZNK6icu_678Calendar24getSkippedWallTimeOptionEv, .-_ZNK6icu_678Calendar24getSkippedWallTimeOptionEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Calendar17setFirstDayOfWeekE19UCalendarDaysOfWeek
	.type	_ZN6icu_678Calendar17setFirstDayOfWeekE19UCalendarDaysOfWeek, @function
_ZN6icu_678Calendar17setFirstDayOfWeekE19UCalendarDaysOfWeek:
.LFB3300:
	.cfi_startproc
	endbr64
	cmpl	%esi, 256(%rdi)
	je	.L1798
	leal	-1(%rsi), %eax
	cmpl	$6, %eax
	ja	.L1798
	movl	%esi, 256(%rdi)
	movb	$0, 9(%rdi)
.L1798:
	ret
	.cfi_endproc
.LFE3300:
	.size	_ZN6icu_678Calendar17setFirstDayOfWeekE19UCalendarDaysOfWeek, .-_ZN6icu_678Calendar17setFirstDayOfWeekE19UCalendarDaysOfWeek
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678Calendar17getFirstDayOfWeekEv
	.type	_ZNK6icu_678Calendar17getFirstDayOfWeekEv, @function
_ZNK6icu_678Calendar17getFirstDayOfWeekEv:
.LFB3301:
	.cfi_startproc
	endbr64
	movl	256(%rdi), %eax
	ret
	.cfi_endproc
.LFE3301:
	.size	_ZNK6icu_678Calendar17getFirstDayOfWeekEv, .-_ZNK6icu_678Calendar17getFirstDayOfWeekEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678Calendar17getFirstDayOfWeekER10UErrorCode
	.type	_ZNK6icu_678Calendar17getFirstDayOfWeekER10UErrorCode, @function
_ZNK6icu_678Calendar17getFirstDayOfWeekER10UErrorCode:
.LFB3302:
	.cfi_startproc
	endbr64
	movl	256(%rdi), %eax
	ret
	.cfi_endproc
.LFE3302:
	.size	_ZNK6icu_678Calendar17getFirstDayOfWeekER10UErrorCode, .-_ZNK6icu_678Calendar17getFirstDayOfWeekER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Calendar25setMinimalDaysInFirstWeekEh
	.type	_ZN6icu_678Calendar25setMinimalDaysInFirstWeekEh, @function
_ZN6icu_678Calendar25setMinimalDaysInFirstWeekEh:
.LFB3303:
	.cfi_startproc
	endbr64
	cmpb	$7, %sil
	movl	$7, %eax
	cmova	%eax, %esi
	movl	$1, %eax
	testb	%sil, %sil
	cmove	%eax, %esi
	cmpb	%sil, 260(%rdi)
	je	.L1802
	movb	%sil, 260(%rdi)
	movb	$0, 9(%rdi)
.L1802:
	ret
	.cfi_endproc
.LFE3303:
	.size	_ZN6icu_678Calendar25setMinimalDaysInFirstWeekEh, .-_ZN6icu_678Calendar25setMinimalDaysInFirstWeekEh
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678Calendar25getMinimalDaysInFirstWeekEv
	.type	_ZNK6icu_678Calendar25getMinimalDaysInFirstWeekEv, @function
_ZNK6icu_678Calendar25getMinimalDaysInFirstWeekEv:
.LFB3304:
	.cfi_startproc
	endbr64
	movzbl	260(%rdi), %eax
	ret
	.cfi_endproc
.LFE3304:
	.size	_ZNK6icu_678Calendar25getMinimalDaysInFirstWeekEv, .-_ZNK6icu_678Calendar25getMinimalDaysInFirstWeekEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678Calendar16getActualMinimumENS0_11EDateFieldsER10UErrorCode
	.type	_ZNK6icu_678Calendar16getActualMinimumENS0_11EDateFieldsER10UErrorCode, @function
_ZNK6icu_678Calendar16getActualMinimumENS0_11EDateFieldsER10UErrorCode:
.LFB3317:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*168(%rax)
	.cfi_endproc
.LFE3317:
	.size	_ZNK6icu_678Calendar16getActualMinimumENS0_11EDateFieldsER10UErrorCode, .-_ZNK6icu_678Calendar16getActualMinimumENS0_11EDateFieldsER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Calendar14validateFieldsER10UErrorCode
	.type	_ZN6icu_678Calendar14validateFieldsER10UErrorCode, @function
_ZN6icu_678Calendar14validateFieldsER10UErrorCode:
.LFB3320:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L1839
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	.p2align 4,,10
	.p2align 3
.L1832:
	cmpl	$1, 128(%r15,%r14,4)
	movl	%r14d, %r13d
	movl	%r14d, %ebx
	jle	.L1808
	movq	(%r15), %rax
	leaq	_ZN6icu_678Calendar13validateFieldE19UCalendarDateFieldsR10UErrorCode(%rip), %rdi
	movq	312(%rax), %rcx
	cmpq	%rdi, %rcx
	jne	.L1809
	cmpq	$6, %r14
	je	.L1810
	cmpl	$8, %r14d
	je	.L1811
	cmpl	$5, %r14d
	je	.L1845
	movq	128(%rax), %rdx
	leaq	_ZNK6icu_678Calendar10getMaximumE19UCalendarDateFields(%rip), %rsi
	cmpq	%rsi, %rdx
	jne	.L1823
	movl	$3, %edx
	movl	%r14d, %esi
	movq	%r15, %rdi
	call	*256(%rax)
	movl	%eax, %ecx
.L1824:
	movq	(%r15), %r8
	leaq	_ZNK6icu_678Calendar10getMinimumE19UCalendarDateFields(%rip), %rsi
	movq	112(%r8), %rax
	cmpq	%rsi, %rax
	jne	.L1825
	movq	256(%r8), %rax
	leaq	_ZNK6icu_678Calendar8getLimitE19UCalendarDateFieldsNS0_10ELimitTypeE(%rip), %rdx
	cmpq	%rdx, %rax
	jne	.L1826
	leal	-4(%rbx), %eax
	cmpl	$18, %eax
	ja	.L1827
	leaq	.L1829(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L1829:
	.long	.L1830-.L1829
	.long	.L1827-.L1829
	.long	.L1827-.L1829
	.long	.L1828-.L1829
	.long	.L1827-.L1829
	.long	.L1828-.L1829
	.long	.L1828-.L1829
	.long	.L1828-.L1829
	.long	.L1828-.L1829
	.long	.L1828-.L1829
	.long	.L1828-.L1829
	.long	.L1828-.L1829
	.long	.L1828-.L1829
	.long	.L1827-.L1829
	.long	.L1828-.L1829
	.long	.L1827-.L1829
	.long	.L1828-.L1829
	.long	.L1828-.L1829
	.long	.L1828-.L1829
	.text
	.p2align 4,,10
	.p2align 3
.L1845:
	movq	%r15, %rdi
	call	*288(%rax)
	leaq	_ZNK6icu_678Calendar20handleGetMonthLengthEii(%rip), %rdx
	movl	20(%r15), %r8d
	movl	%eax, %esi
	movq	(%r15), %rax
	movq	272(%rax), %rcx
	cmpq	%rdx, %rcx
	jne	.L1813
	leal	1(%r8), %edx
	movl	%r8d, -56(%rbp)
	movl	$1, %ecx
	movq	%r15, %rdi
	movl	%esi, -52(%rbp)
	call	*264(%rax)
	movl	-56(%rbp), %r8d
	movl	-52(%rbp), %esi
	movq	%r15, %rdi
	movl	%eax, %ebx
	movq	(%r15), %rax
	movl	$1, %ecx
	movl	%r8d, %edx
	call	*264(%rax)
	subl	%eax, %ebx
.L1814:
	movl	32(%r15), %eax
	testl	%eax, %eax
	jle	.L1815
.L1844:
	cmpl	%eax, %ebx
	jl	.L1815
.L1842:
	movl	(%r12), %eax
.L1808:
	addq	$1, %r14
	testl	%eax, %eax
	jg	.L1806
	cmpl	$22, %r13d
	jne	.L1832
	jmp	.L1806
	.p2align 4,,10
	.p2align 3
.L1811:
	movl	44(%r15), %edx
	testl	%edx, %edx
	je	.L1815
	movq	128(%rax), %rdx
	leaq	_ZNK6icu_678Calendar10getMaximumE19UCalendarDateFields(%rip), %rcx
	cmpq	%rcx, %rdx
	jne	.L1818
	movl	$3, %edx
	movl	$8, %esi
	movq	%r15, %rdi
	call	*256(%rax)
	movl	%eax, %ebx
.L1819:
	movq	(%r15), %rax
	leaq	_ZNK6icu_678Calendar10getMinimumE19UCalendarDateFields(%rip), %rsi
	movq	112(%rax), %rdx
	cmpq	%rsi, %rdx
	jne	.L1820
	movq	256(%rax), %rcx
	leaq	_ZNK6icu_678Calendar8getLimitE19UCalendarDateFieldsNS0_10ELimitTypeE(%rip), %rdx
	movl	$8, %esi
	movq	%r15, %rdi
	cmpq	%rdx, %rcx
	movl	$0, %edx
	jne	.L1821
	call	*248(%rax)
.L1822:
	movl	44(%r15), %edx
	cmpl	%edx, %eax
	jg	.L1815
	cmpl	%edx, %ebx
	jge	.L1842
	.p2align 4,,10
	.p2align 3
.L1815:
	movl	$1, (%r12)
.L1806:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1810:
	.cfi_restore_state
	movq	%r15, %rdi
	call	*288(%rax)
	leaq	_ZNK6icu_678Calendar19handleGetYearLengthEi(%rip), %rsi
	movl	%eax, %r8d
	movq	(%r15), %rax
	movq	280(%rax), %rdx
	cmpq	%rsi, %rdx
	jne	.L1816
	leal	1(%r8), %esi
	movl	%r8d, -52(%rbp)
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	*264(%rax)
	movl	-52(%rbp), %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	%eax, %ebx
	movq	(%r15), %rax
	movq	%r15, %rdi
	movl	%r8d, %esi
	call	*264(%rax)
	subl	%eax, %ebx
.L1817:
	movl	36(%r15), %eax
	testl	%eax, %eax
	jg	.L1844
	jmp	.L1815
	.p2align 4,,10
	.p2align 3
.L1809:
	movq	%r12, %rdx
	movl	%r14d, %esi
	movq	%r15, %rdi
	call	*%rcx
	jmp	.L1842
.L1828:
	movq	%r14, %rax
	leaq	_ZN6icu_67L15kCalendarLimitsE(%rip), %rdx
	salq	$4, %rax
	movl	(%rdx,%rax), %eax
.L1831:
	movl	12(%r15,%r14,4), %edx
	cmpl	%edx, %eax
	jg	.L1815
	cmpl	%edx, %ecx
	jge	.L1842
	jmp	.L1815
.L1830:
	xorl	%eax, %eax
	cmpb	$1, 260(%r15)
	sete	%al
	jmp	.L1831
.L1827:
	movl	%ecx, -52(%rbp)
	xorl	%edx, %edx
	movl	%r13d, %esi
	movq	%r15, %rdi
	call	*248(%r8)
	movl	-52(%rbp), %ecx
	jmp	.L1831
	.p2align 4,,10
	.p2align 3
.L1825:
	movl	%ecx, -52(%rbp)
	movl	%r13d, %esi
	movq	%r15, %rdi
	call	*%rax
	movl	-52(%rbp), %ecx
	jmp	.L1831
	.p2align 4,,10
	.p2align 3
.L1823:
	movl	%r14d, %esi
	movq	%r15, %rdi
	call	*%rdx
	movl	%eax, %ecx
	jmp	.L1824
	.p2align 4,,10
	.p2align 3
.L1816:
	movl	%r8d, %esi
	movq	%r15, %rdi
	call	*%rdx
	movl	%eax, %ebx
	jmp	.L1817
	.p2align 4,,10
	.p2align 3
.L1813:
	movl	%r8d, %edx
	movq	%r15, %rdi
	call	*%rcx
	movl	%eax, %ebx
	jmp	.L1814
	.p2align 4,,10
	.p2align 3
.L1821:
	call	*%rcx
	jmp	.L1822
	.p2align 4,,10
	.p2align 3
.L1826:
	movl	%ecx, -52(%rbp)
	xorl	%edx, %edx
	movl	%r13d, %esi
	movq	%r15, %rdi
	call	*%rax
	movl	-52(%rbp), %ecx
	jmp	.L1831
	.p2align 4,,10
	.p2align 3
.L1818:
	movl	$8, %esi
	movq	%r15, %rdi
	call	*%rdx
	movl	%eax, %ebx
	jmp	.L1819
	.p2align 4,,10
	.p2align 3
.L1820:
	movl	$8, %esi
	movq	%r15, %rdi
	call	*%rdx
	jmp	.L1822
.L1839:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.cfi_endproc
.LFE3320:
	.size	_ZN6icu_678Calendar14validateFieldsER10UErrorCode, .-_ZN6icu_678Calendar14validateFieldsER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Calendar13validateFieldE19UCalendarDateFieldsiiR10UErrorCode
	.type	_ZN6icu_678Calendar13validateFieldE19UCalendarDateFieldsiiR10UErrorCode, @function
_ZN6icu_678Calendar13validateFieldE19UCalendarDateFieldsiiR10UErrorCode:
.LFB3322:
	.cfi_startproc
	endbr64
	movslq	%esi, %rsi
	movl	12(%rdi,%rsi,4), %eax
	cmpl	%edx, %eax
	jl	.L1849
	cmpl	%ecx, %eax
	jg	.L1849
	ret
	.p2align 4,,10
	.p2align 3
.L1849:
	movl	$1, (%r8)
	ret
	.cfi_endproc
.LFE3322:
	.size	_ZN6icu_678Calendar13validateFieldE19UCalendarDateFieldsiiR10UErrorCode, .-_ZN6icu_678Calendar13validateFieldE19UCalendarDateFieldsiiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678Calendar10newerFieldE19UCalendarDateFieldsS1_
	.type	_ZNK6icu_678Calendar10newerFieldE19UCalendarDateFieldsS1_, @function
_ZNK6icu_678Calendar10newerFieldE19UCalendarDateFieldsS1_:
.LFB3324:
	.cfi_startproc
	endbr64
	movslq	%esi, %rcx
	movslq	%edx, %rsi
	movq	%rcx, %rax
	movl	128(%rdi,%rcx,4), %ecx
	cmpl	%ecx, 128(%rdi,%rsi,4)
	cmovg	%edx, %eax
	ret
	.cfi_endproc
.LFE3324:
	.size	_ZNK6icu_678Calendar10newerFieldE19UCalendarDateFieldsS1_, .-_ZNK6icu_678Calendar10newerFieldE19UCalendarDateFieldsS1_
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Calendar13resolveFieldsEPA12_A8_Ki
	.type	_ZN6icu_678Calendar13resolveFieldsEPA12_A8_Ki, @function
_ZN6icu_678Calendar13resolveFieldsEPA12_A8_Ki:
.LFB3325:
	.cfi_startproc
	endbr64
	cmpl	$-1, (%rsi)
	je	.L1865
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r9
	movq	%rsi, %rcx
	xorl	%r11d, %r11d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	.p2align 4,,10
	.p2align 3
.L1863:
	movl	(%rcx), %r10d
	cmpl	$-1, %r10d
	je	.L1866
	xorl	%edi, %edi
	xorl	%ebx, %ebx
	movl	$23, %r12d
	.p2align 4,,10
	.p2align 3
.L1862:
	xorl	%edx, %edx
	cmpl	$31, %r10d
	movslq	%edi, %r8
	setg	%al
	leaq	0(,%r8,8), %rsi
	setg	%dl
	movzbl	%al, %eax
	addq	%rsi, %rax
	movslq	(%rcx,%rax,4), %rax
	cmpl	$-1, %eax
	je	.L1858
	movl	128(%r9,%rax,4), %eax
	testl	%eax, %eax
	je	.L1858
	leal	1(%rdx), %r13d
	cmovs	%r11d, %eax
	movslq	%r13d, %r13
	addq	%rsi, %r13
	movslq	(%rcx,%r13,4), %r13
	cmpl	$-1, %r13d
	je	.L1859
	movl	128(%r9,%r13,4), %r13d
	testl	%r13d, %r13d
	je	.L1858
	cmpl	%r13d, %eax
	cmovl	%r13d, %eax
	leal	2(%rdx), %r13d
	movslq	%r13d, %r13
	addq	%rsi, %r13
	movslq	(%rcx,%r13,4), %r13
	cmpl	$-1, %r13d
	je	.L1859
	movl	128(%r9,%r13,4), %r13d
	testl	%r13d, %r13d
	je	.L1858
	cmpl	%r13d, %eax
	cmovl	%r13d, %eax
	leal	3(%rdx), %r13d
	movslq	%r13d, %r13
	addq	%rsi, %r13
	movslq	(%rcx,%r13,4), %r13
	cmpl	$-1, %r13d
	je	.L1859
	movl	128(%r9,%r13,4), %r13d
	testl	%r13d, %r13d
	je	.L1858
	cmpl	%r13d, %eax
	cmovl	%r13d, %eax
	leal	4(%rdx), %r13d
	movslq	%r13d, %r13
	addq	%r13, %rsi
	movslq	(%rcx,%rsi,4), %rsi
	cmpl	$-1, %esi
	je	.L1859
	movl	128(%r9,%rsi,4), %esi
	testl	%esi, %esi
	je	.L1858
	cmpl	%esi, %eax
	cmovl	%esi, %eax
	leal	5(%rdx), %esi
	salq	$3, %r8
	movslq	%esi, %rsi
	addq	%r8, %rsi
	movslq	(%rcx,%rsi,4), %rsi
	cmpl	$-1, %esi
	je	.L1859
	movl	128(%r9,%rsi,4), %esi
	testl	%esi, %esi
	je	.L1858
	cmpl	%esi, %eax
	cmovl	%esi, %eax
	leal	6(%rdx), %esi
	movslq	%esi, %rsi
	addq	%r8, %rsi
	movslq	(%rcx,%rsi,4), %rsi
	cmpl	$-1, %esi
	je	.L1859
	movl	128(%r9,%rsi,4), %esi
	testl	%esi, %esi
	je	.L1858
	cmpl	%esi, %eax
	cmovl	%esi, %eax
	addl	$7, %edx
	movslq	%edx, %rdx
	addq	%rdx, %r8
	movslq	(%rcx,%r8,4), %rdx
	cmpl	$-1, %edx
	je	.L1859
	movl	128(%r9,%rdx,4), %edx
	testl	%edx, %edx
	je	.L1858
	cmpl	%edx, %eax
	cmovl	%edx, %eax
.L1859:
	cmpl	%eax, %ebx
	jge	.L1858
	cmpl	$31, %r10d
	jle	.L1868
	andl	$31, %r10d
	cmpl	$5, %r10d
	je	.L1928
.L1868:
	movl	%eax, %ebx
	movl	%r10d, %r12d
.L1858:
	addl	$1, %edi
	movslq	%edi, %rax
	salq	$5, %rax
	movl	(%rcx,%rax), %r10d
	cmpl	$-1, %r10d
	jne	.L1862
	cmpl	$23, %r12d
	setne	%al
.L1857:
	addq	$384, %rcx
	cmpl	$-1, (%rcx)
	je	.L1870
	testb	%al, %al
	je	.L1863
.L1870:
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1928:
	.cfi_restore_state
	movl	148(%r9), %edx
	cmpl	%edx, 144(%r9)
	jge	.L1929
.L1860:
	addl	$1, %edi
	movslq	%edi, %rdx
	salq	$5, %rdx
	movl	(%rcx,%rdx), %r10d
	cmpl	$-1, %r10d
	je	.L1869
	movl	%eax, %ebx
	movl	$5, %r12d
	jmp	.L1862
.L1929:
	cmpl	$5, %r12d
	jne	.L1858
	jmp	.L1860
	.p2align 4,,10
	.p2align 3
.L1866:
	xorl	%eax, %eax
	movl	$23, %r12d
	jmp	.L1857
.L1869:
	popq	%rbx
	movl	$5, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
.L1865:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	movl	$23, %eax
	ret
	.cfi_endproc
.LFE3325:
	.size	_ZN6icu_678Calendar13resolveFieldsEPA12_A8_Ki, .-_ZN6icu_678Calendar13resolveFieldsEPA12_A8_Ki
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Calendar35handleGetExtendedYearFromWeekFieldsEii
	.type	_ZN6icu_678Calendar35handleGetExtendedYearFromWeekFieldsEii, @function
_ZN6icu_678Calendar35handleGetExtendedYearFromWeekFieldsEii:
.LFB3335:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	leaq	_ZN6icu_678Calendar15kDatePrecedenceE(%rip), %rsi
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	%edx, -60(%rbp)
	call	_ZN6icu_678Calendar13resolveFieldsEPA12_A8_Ki
	leaq	_ZN6icu_678Calendar14kDOWPrecedenceE(%rip), %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN6icu_678Calendar13resolveFieldsEPA12_A8_Ki
	movl	256(%r13), %r10d
	cmpl	$7, %eax
	je	.L1931
	xorl	%r8d, %r8d
	cmpl	$18, %eax
	jne	.L1933
	movl	84(%r13), %eax
	subl	$1, %eax
.L1964:
	movslq	%eax, %r8
	cltd
	imulq	$-1840700269, %r8, %r8
	shrq	$32, %r8
	addl	%eax, %r8d
	sarl	$2, %r8d
	subl	%edx, %r8d
	leal	0(,%r8,8), %edx
	subl	%r8d, %edx
	subl	%edx, %eax
	movl	%eax, %r8d
	leal	7(%rax), %eax
	testl	%r8d, %r8d
	cmovs	%eax, %r8d
.L1933:
	movq	0(%r13), %rax
	movl	%r10d, -56(%rbp)
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	%r8d, -52(%rbp)
	movl	%r12d, %esi
	movq	%r13, %rdi
	leal	1(%r12), %r15d
	call	*264(%rax)
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movl	%r15d, %esi
	movl	%eax, %r14d
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*264(%rax)
	pxor	%xmm0, %xmm0
	movsd	.LC2(%rip), %xmm1
	movl	%eax, -64(%rbp)
	leal	1(%r14), %eax
	cvtsi2sdl	%eax, %xmm0
	addsd	.LC3(%rip), %xmm0
	call	uprv_fmod_67@PLT
	movl	-56(%rbp), %r10d
	movl	-52(%rbp), %r8d
	cvttsd2sil	%xmm0, %edx
	movl	%edx, %eax
	sarb	$7, %al
	andl	$7, %eax
	leal	1(%rax,%rdx), %eax
	movzbl	%al, %eax
	subl	%r10d, %eax
	leal	7(%rax), %edx
	cmovs	%edx, %eax
	cmpl	$3, %ebx
	je	.L1937
	cmpl	$5, %ebx
	je	.L1938
.L1958:
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1938:
	.cfi_restore_state
	movl	20(%r13), %edx
	testl	%edx, %edx
	jne	.L1948
	movq	0(%r13), %rax
	leaq	_ZNK6icu_678Calendar15getLeastMaximumE19UCalendarDateFields(%rip), %rcx
	movq	160(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L1949
	movq	256(%rax), %rcx
	leaq	_ZNK6icu_678Calendar8getLimitE19UCalendarDateFieldsNS0_10ELimitTypeE(%rip), %rdx
	movl	$3, %esi
	movq	%r13, %rdi
	cmpq	%rdx, %rcx
	movl	$2, %edx
	jne	.L1950
	call	*248(%rax)
.L1951:
	movl	-60(%rbp), %ecx
	cmpl	%ecx, %eax
	jle	.L1955
	cmpl	$1, %ecx
	jne	.L1958
	movl	20(%r13), %eax
	testl	%eax, %eax
	je	.L1958
	.p2align 4,,10
	.p2align 3
.L1941:
	subl	$1, %r12d
	jmp	.L1958
	.p2align 4,,10
	.p2align 3
.L1937:
	movl	$7, %ebx
	movzbl	260(%r13), %ecx
	subl	%eax, %ebx
	cmpl	$1, -60(%rbp)
	je	.L1965
	movq	0(%r13), %rax
	leaq	_ZNK6icu_678Calendar15getLeastMaximumE19UCalendarDateFields(%rip), %rsi
	movq	160(%rax), %rdx
	cmpq	%rsi, %rdx
	jne	.L1943
	movq	256(%rax), %r10
	leaq	_ZNK6icu_678Calendar8getLimitE19UCalendarDateFieldsNS0_10ELimitTypeE(%rip), %rdx
	movl	%ecx, -52(%rbp)
	movq	%r13, %rdi
	movl	%r8d, -56(%rbp)
	movl	$3, %esi
	cmpq	%rdx, %r10
	movl	$2, %edx
	jne	.L1944
	call	*248(%rax)
	movl	-52(%rbp), %ecx
	movl	-56(%rbp), %r8d
.L1945:
	movl	-60(%rbp), %edi
	cmpl	%edi, %eax
	jg	.L1958
	movl	%edi, %r13d
	addl	%ebx, %r14d
	subl	$1, %r13d
	leal	0(,%r13,8), %eax
	subl	%r13d, %eax
	addl	%eax, %r14d
	addl	%r14d, %r8d
	cmpl	%ecx, %ebx
	leal	-7(%r8), %eax
	cmovge	%eax, %r8d
	addl	$1, %r8d
	cmpl	-64(%rbp), %r8d
	cmovge	%r15d, %r12d
	jmp	.L1958
	.p2align 4,,10
	.p2align 3
.L1931:
	movl	40(%r13), %eax
	subl	%r10d, %eax
	jmp	.L1964
	.p2align 4,,10
	.p2align 3
.L1948:
	cmpl	$1, -60(%rbp)
	jne	.L1958
	jmp	.L1941
	.p2align 4,,10
	.p2align 3
.L1965:
	cmpl	%r8d, %eax
	jle	.L1958
	cmpl	%ecx, %ebx
	jge	.L1941
	jmp	.L1958
	.p2align 4,,10
	.p2align 3
.L1955:
	movl	%r15d, %r12d
	jmp	.L1958
	.p2align 4,,10
	.p2align 3
.L1943:
	movl	%r8d, -56(%rbp)
	movl	$3, %esi
	movq	%r13, %rdi
	movl	%ecx, -52(%rbp)
	call	*%rdx
	movl	-56(%rbp), %r8d
	movl	-52(%rbp), %ecx
	jmp	.L1945
	.p2align 4,,10
	.p2align 3
.L1944:
	call	*%r10
	movl	-52(%rbp), %ecx
	movl	-56(%rbp), %r8d
	jmp	.L1945
	.p2align 4,,10
	.p2align 3
.L1949:
	movl	$3, %esi
	movq	%r13, %rdi
	call	*%rdx
	jmp	.L1951
	.p2align 4,,10
	.p2align 3
.L1950:
	call	*%rcx
	jmp	.L1951
	.cfi_endproc
.LFE3335:
	.size	_ZN6icu_678Calendar35handleGetExtendedYearFromWeekFieldsEii, .-_ZN6icu_678Calendar35handleGetExtendedYearFromWeekFieldsEii
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Calendar22handleComputeJulianDayE19UCalendarDateFields
	.type	_ZN6icu_678Calendar22handleComputeJulianDayE19UCalendarDateFields, @function
_ZN6icu_678Calendar22handleComputeJulianDayE19UCalendarDateFields:
.LFB3331:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movslq	%esi, %rbx
	leal	-4(%rbx), %eax
	subq	$40, %rsp
	cmpl	$1, %eax
	setbe	%r12b
	cmpl	$8, %ebx
	sete	%al
	orl	%eax, %r12d
	cmpl	$3, %ebx
	je	.L2021
.L1967:
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*288(%rax)
	movl	%eax, %r14d
.L1968:
	cmpb	$0, 11(%r15)
	movq	(%r15), %rax
	movl	%r14d, 88(%r15)
	movl	$1, 204(%r15)
	movb	$1, 123(%r15)
	jne	.L1969
	movl	136(%r15), %r13d
	testl	%r13d, %r13d
	je	.L2022
.L1969:
	movl	20(%r15), %r13d
.L1970:
	movq	264(%rax), %rax
	testb	%r12b, %r12b
	je	.L1971
	movl	$1, %ecx
	movl	%r13d, %edx
	movl	%r14d, %esi
	movq	%r15, %rdi
	call	*%rax
	movl	%eax, %r12d
	cmpl	$5, %ebx
	je	.L2023
	cmpl	$6, %ebx
	jne	.L1976
.L2027:
	movl	36(%r15), %eax
	addq	$40, %rsp
	popq	%rbx
	addl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1976:
	.cfi_restore_state
	leal	1(%r12), %eax
	pxor	%xmm0, %xmm0
	movl	256(%r15), %r13d
	cvtsi2sdl	%eax, %xmm0
	movq	.LC2(%rip), %rax
	addsd	.LC3(%rip), %xmm0
	movl	%r13d, -52(%rbp)
	movq	%rax, %xmm1
	call	uprv_fmod_67@PLT
	leaq	_ZN6icu_678Calendar14kDOWPrecedenceE(%rip), %rsi
	movq	%r15, %rdi
	cvttsd2sil	%xmm0, %eax
	movl	%eax, %r8d
	sarb	$7, %r8b
	andl	$7, %r8d
	leal	1(%r8,%rax), %r8d
	movzbl	%r8b, %r8d
	subl	%r13d, %r8d
	movl	%r8d, %r13d
	leal	7(%r8), %eax
	cmovs	%eax, %r13d
	call	_ZN6icu_678Calendar13resolveFieldsEPA12_A8_Ki
	cmpl	$7, %eax
	je	.L2024
	xorl	%r9d, %r9d
	cmpl	$18, %eax
	jne	.L1980
	movl	84(%r15), %eax
	subl	$1, %eax
.L1979:
	movslq	%eax, %r9
	cltd
	imulq	$-1840700269, %r9, %r9
	shrq	$32, %r9
	addl	%eax, %r9d
	sarl	$2, %r9d
	subl	%edx, %r9d
	leal	0(,%r9,8), %edx
	subl	%r9d, %edx
	subl	%edx, %eax
	movl	%eax, %r9d
	leal	7(%rax), %eax
	cmovs	%eax, %r9d
.L1980:
	movl	%r9d, %eax
	subl	%r13d, %eax
	leal	1(%rax), %r11d
	cmpl	$8, %ebx
	je	.L2025
	cmpl	$3, %ebx
	je	.L1989
.L2020:
	movl	$7, %edx
	movzbl	260(%r15), %ecx
	subl	%r13d, %edx
.L1990:
	cmpl	%ecx, %edx
	leal	7(%r11), %eax
	movl	12(%r15,%rbx,4), %edx
	cmovl	%eax, %r11d
	subl	$1, %edx
	leal	(%r11,%rdx,8), %r11d
	subl	%edx, %r11d
.L1985:
	addq	$40, %rsp
	leal	(%r12,%r11), %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2022:
	.cfi_restore_state
	movq	336(%rax), %rdx
	leaq	_ZN6icu_678Calendar21getDefaultMonthInYearEi(%rip), %rcx
	cmpq	%rcx, %rdx
	je	.L1970
	movl	%r14d, %esi
	movq	%r15, %rdi
	call	*%rdx
	movl	%eax, %r13d
	movq	(%r15), %rax
	jmp	.L1970
	.p2align 4,,10
	.p2align 3
.L2023:
	cmpb	$0, 11(%r15)
	movl	148(%r15), %edx
	je	.L2026
.L1973:
	movl	$1, %eax
	testl	%edx, %edx
	jle	.L1974
	movl	32(%r15), %eax
.L1974:
	addq	$40, %rsp
	addl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1971:
	.cfi_restore_state
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	%r14d, %esi
	movq	%r15, %rdi
	call	*%rax
	movl	%eax, %r12d
	cmpl	$6, %ebx
	je	.L2027
	jmp	.L1976
	.p2align 4,,10
	.p2align 3
.L2021:
	movl	132(%rdi), %eax
	cmpl	%eax, 196(%rdi)
	jl	.L1967
	movl	80(%rdi), %r14d
	jmp	.L1968
	.p2align 4,,10
	.p2align 3
.L2026:
	testl	%edx, %edx
	jne	.L1973
	movq	(%r15), %rax
	leaq	_ZN6icu_678Calendar20getDefaultDayInMonthEii(%rip), %rdx
	movq	344(%rax), %rcx
	movl	$1, %eax
	cmpq	%rdx, %rcx
	je	.L1974
	movl	%r13d, %edx
	movl	%r14d, %esi
	movq	%r15, %rdi
	call	*%rcx
	jmp	.L1974
	.p2align 4,,10
	.p2align 3
.L1989:
	cmpb	$0, 11(%r15)
	jne	.L1991
	movl	196(%r15), %eax
	testl	%eax, %eax
	jne	.L1991
.L1992:
	movl	24(%r15), %eax
	xorl	%edx, %edx
	movl	%r9d, -72(%rbp)
	xorl	%ecx, %ecx
	movl	%r11d, -68(%rbp)
	leal	1(%r14), %esi
	movq	%r15, %rdi
	movl	%eax, -56(%rbp)
	movq	(%r15), %rax
	call	*264(%rax)
	pxor	%xmm2, %xmm2
	movq	.LC2(%rip), %rsi
	movl	%eax, -76(%rbp)
	leal	1(%rax), %eax
	cvtsi2sdl	%eax, %xmm2
	addsd	.LC3(%rip), %xmm2
	movq	%rsi, %xmm1
	movapd	%xmm2, %xmm0
	movsd	%xmm2, -64(%rbp)
	call	uprv_fmod_67@PLT
	movsd	-64(%rbp), %xmm2
	movl	-68(%rbp), %r11d
	cvttsd2sil	%xmm0, %edx
	movl	-72(%rbp), %r9d
	movl	%edx, %eax
	sarb	$7, %al
	andl	$7, %eax
	leal	1(%rax,%rdx), %eax
	movzbl	%al, %eax
	subl	-52(%rbp), %eax
	leal	7(%rax), %edx
	cmovs	%edx, %eax
	cmpl	$1, -56(%rbp)
	je	.L2028
	movq	(%r15), %rax
	leaq	_ZNK6icu_678Calendar15getLeastMaximumE19UCalendarDateFields(%rip), %rcx
	movq	160(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L2001
	movq	256(%rax), %rcx
	movl	%r9d, -68(%rbp)
	movl	$3, %esi
	movq	%r15, %rdi
	leaq	_ZNK6icu_678Calendar8getLimitE19UCalendarDateFieldsNS0_10ELimitTypeE(%rip), %rdx
	movl	%r11d, -64(%rbp)
	cmpq	%rdx, %rcx
	movl	$2, %edx
	jne	.L2002
	call	*248(%rax)
	movl	-64(%rbp), %r11d
	movl	-68(%rbp), %r9d
.L2003:
	movl	$7, %r8d
	movl	-56(%rbp), %esi
	movl	%r9d, -64(%rbp)
	movl	%r8d, %edx
	movzbl	260(%r15), %ecx
	subl	%r13d, %edx
	cmpl	%esi, %eax
	jg	.L1990
	cmpl	%ecx, %edx
	leal	7(%r11), %eax
	cmovge	%r11d, %eax
	subl	$1, %esi
	leal	(%rax,%rsi,8), %eax
	subl	%esi, %eax
	addl	%r12d, %eax
	cmpl	-76(%rbp), %eax
	jle	.L1990
	movq	(%r15), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	leal	-1(%r14), %esi
	movq	%r15, %rdi
	call	*264(%rax)
	pxor	%xmm0, %xmm0
	movl	%eax, %r12d
	leal	1(%rax), %eax
	cvtsi2sdl	%eax, %xmm0
	movq	.LC2(%rip), %rax
	addsd	.LC3(%rip), %xmm0
	movq	%rax, %xmm1
	call	uprv_fmod_67@PLT
	movl	-64(%rbp), %r9d
	movl	$7, %r8d
	movzbl	260(%r15), %ecx
	cvttsd2sil	%xmm0, %edx
	movl	%edx, %eax
	sarb	$7, %al
	andl	$7, %eax
	leal	1(%rax,%rdx), %eax
	movzbl	%al, %eax
	subl	-52(%rbp), %eax
	leal	7(%rax), %edx
	cmovs	%edx, %eax
	movl	%r8d, %edx
	subl	%eax, %r9d
	subl	%eax, %edx
	leal	1(%r9), %r11d
	jmp	.L1990
	.p2align 4,,10
	.p2align 3
.L1991:
	leaq	_ZN6icu_678Calendar15kYearPrecedenceE(%rip), %rsi
	movq	%r15, %rdi
	movl	%r9d, -64(%rbp)
	movl	%r11d, -56(%rbp)
	call	_ZN6icu_678Calendar13resolveFieldsEPA12_A8_Ki
	movl	-56(%rbp), %r11d
	movl	-64(%rbp), %r9d
	cmpl	$17, %eax
	je	.L2020
	cmpl	$1, 196(%r15)
	jne	.L1992
	jmp	.L2020
	.p2align 4,,10
	.p2align 3
.L2025:
	movl	160(%r15), %ecx
	leal	7(%r11), %eax
	testl	%r11d, %r11d
	cmovle	%eax, %r11d
	testl	%ecx, %ecx
	jle	.L1985
	movl	44(%r15), %r13d
	testl	%r13d, %r13d
	js	.L1984
	subl	$1, %r13d
	leal	(%r11,%r13,8), %r11d
	subl	%r13d, %r11d
	jmp	.L1985
	.p2align 4,,10
	.p2align 3
.L2024:
	movl	40(%r15), %eax
	subl	256(%r15), %eax
	jmp	.L1979
	.p2align 4,,10
	.p2align 3
.L1984:
	movl	136(%r15), %edx
	xorl	%r8d, %r8d
	testl	%edx, %edx
	jle	.L1986
	movl	20(%r15), %r8d
.L1986:
	movq	(%r15), %rax
	leaq	_ZNK6icu_678Calendar20handleGetMonthLengthEii(%rip), %rdx
	movq	272(%rax), %rcx
	cmpq	%rdx, %rcx
	jne	.L1987
	movl	%r11d, -56(%rbp)
	leal	1(%r8), %edx
	movl	$1, %ecx
	movl	%r14d, %esi
	movl	%r8d, -52(%rbp)
	movq	%r15, %rdi
	call	*264(%rax)
	movl	-52(%rbp), %r8d
	movl	%r14d, %esi
	movq	%r15, %rdi
	movl	%eax, %ebx
	movq	(%r15), %rax
	movl	$1, %ecx
	movl	%r8d, %edx
	call	*264(%rax)
	movl	-56(%rbp), %r11d
	subl	%eax, %ebx
.L1988:
	subl	%r11d, %ebx
	movslq	%ebx, %rax
	imulq	$-1840700269, %rax, %rax
	shrq	$32, %rax
	addl	%ebx, %eax
	sarl	$31, %ebx
	sarl	$2, %eax
	subl	%ebx, %eax
	leal	1(%r13,%rax), %edx
	leal	(%r11,%rdx,8), %r11d
	subl	%edx, %r11d
	jmp	.L1985
	.p2align 4,,10
	.p2align 3
.L2028:
	movl	$7, %edx
	movzbl	260(%r15), %ecx
	subl	%r13d, %edx
	testl	%eax, %eax
	jle	.L1990
	movl	$7, %r14d
	movl	%r14d, %edi
	subl	%eax, %edi
	cmpl	%ecx, %edi
	jge	.L1998
	movl	%r14d, %edx
	subl	%r13d, %edx
	jmp	.L1990
.L2001:
	movl	%r9d, -68(%rbp)
	movl	$3, %esi
	movq	%r15, %rdi
	movl	%r11d, -64(%rbp)
	call	*%rdx
	movl	-68(%rbp), %r9d
	movl	-64(%rbp), %r11d
	jmp	.L2003
.L1987:
	movl	%r11d, -52(%rbp)
	movl	%r8d, %edx
	movl	%r14d, %esi
	movq	%r15, %rdi
	call	*%rcx
	movl	-52(%rbp), %r11d
	movl	%eax, %ebx
	jmp	.L1988
.L2002:
	call	*%rcx
	movl	-64(%rbp), %r11d
	movl	-68(%rbp), %r9d
	jmp	.L2003
.L1998:
	movq	.LC2(%rip), %rax
	movapd	%xmm2, %xmm0
	movl	%r9d, -64(%rbp)
	movq	%rax, %xmm1
	call	uprv_fmod_67@PLT
	movl	-56(%rbp), %edx
	movl	-64(%rbp), %r9d
	cvttsd2sil	%xmm0, %ecx
	movl	-76(%rbp), %r12d
	movl	%ecx, %eax
	sarb	$7, %al
	andl	$7, %eax
	leal	1(%rax,%rcx), %eax
	movzbl	%al, %eax
	subl	-52(%rbp), %eax
	leal	7(%rax), %ecx
	cmovs	%ecx, %eax
	movzbl	260(%r15), %ecx
	subl	%eax, %edx
	leal	(%rdx,%r9), %r11d
	movl	%r14d, %edx
	subl	%eax, %edx
	jmp	.L1990
	.cfi_endproc
.LFE3331:
	.size	_ZN6icu_678Calendar22handleComputeJulianDayE19UCalendarDateFields, .-_ZN6icu_678Calendar22handleComputeJulianDayE19UCalendarDateFields
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Calendar18computeMillisInDayEv
	.type	_ZN6icu_678Calendar18computeMillisInDayEv, @function
_ZN6icu_678Calendar18computeMillisInDayEv:
.LFB3328:
	.cfi_startproc
	endbr64
	movl	172(%rdi), %edx
	movl	164(%rdi), %eax
	cmpl	%eax, 168(%rdi)
	cmovge	168(%rdi), %eax
	cmpl	%eax, %edx
	movl	%eax, %ecx
	cmovge	%edx, %ecx
	testl	%ecx, %ecx
	je	.L2032
	cmpl	%eax, %edx
	jge	.L2033
	pxor	%xmm1, %xmm1
	movl	48(%rdi), %eax
	movsd	.LC10(%rip), %xmm2
	cvtsi2sdl	52(%rdi), %xmm1
	addsd	.LC0(%rip), %xmm1
	leal	(%rax,%rax,2), %eax
	sall	$2, %eax
	movapd	%xmm1, %xmm0
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
	addsd	%xmm0, %xmm1
	mulsd	%xmm2, %xmm1
	jmp	.L2030
	.p2align 4,,10
	.p2align 3
.L2032:
	movsd	.LC10(%rip), %xmm2
	pxor	%xmm1, %xmm1
.L2030:
	pxor	%xmm0, %xmm0
	cvtsi2sdl	60(%rdi), %xmm0
	addsd	%xmm1, %xmm0
	movapd	%xmm0, %xmm1
	pxor	%xmm0, %xmm0
	mulsd	%xmm2, %xmm1
	cvtsi2sdl	64(%rdi), %xmm0
	addsd	%xmm1, %xmm0
	mulsd	.LC11(%rip), %xmm0
	pxor	%xmm1, %xmm1
	cvtsi2sdl	68(%rdi), %xmm1
	addsd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L2033:
	pxor	%xmm1, %xmm1
	movsd	.LC10(%rip), %xmm2
	cvtsi2sdl	56(%rdi), %xmm1
	addsd	.LC0(%rip), %xmm1
	mulsd	%xmm2, %xmm1
	jmp	.L2030
	.cfi_endproc
.LFE3328:
	.size	_ZN6icu_678Calendar18computeMillisInDayEv, .-_ZN6icu_678Calendar18computeMillisInDayEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Calendar17computeZoneOffsetEddR10UErrorCode
	.type	_ZN6icu_678Calendar17computeZoneOffsetEddR10UErrorCode, @function
_ZN6icu_678Calendar17computeZoneOffsetEddR10UErrorCode:
.LFB3329:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addsd	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	240(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movsd	%xmm0, -88(%rbp)
	testq	%r12, %r12
	je	.L2035
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6713OlsonTimeZoneE(%rip), %rdx
	leaq	_ZTIN6icu_678TimeZoneE(%rip), %rsi
	movq	%r12, %rdi
	call	__dynamic_cast@PLT
	testq	%rax, %rax
	je	.L2051
.L2036:
	xorl	%edx, %edx
	cmpl	$1, 248(%rbx)
	movq	(%r12), %rax
	movq	%r13, %r9
	setne	%dl
	xorl	%esi, %esi
	cmpl	$1, 252(%rbx)
	movq	%r12, %rdi
	sete	%sil
	movsd	-88(%rbp), %xmm0
	leaq	-72(%rbp), %rcx
	leaq	-68(%rbp), %r8
	leal	4(,%rdx,8), %edx
	leal	4(,%rsi,8), %esi
	call	*160(%rax)
	movl	-68(%rbp), %eax
	addl	-72(%rbp), %eax
.L2034:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L2052
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2051:
	.cfi_restore_state
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6714SimpleTimeZoneE(%rip), %rdx
	leaq	_ZTIN6icu_678TimeZoneE(%rip), %rsi
	movq	%r12, %rdi
	call	__dynamic_cast@PLT
	testq	%rax, %rax
	jne	.L2036
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6717RuleBasedTimeZoneE(%rip), %rdx
	leaq	_ZTIN6icu_678TimeZoneE(%rip), %rsi
	movq	%r12, %rdi
	call	__dynamic_cast@PLT
	testq	%rax, %rax
	jne	.L2036
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_679VTimeZoneE(%rip), %rdx
	leaq	_ZTIN6icu_678TimeZoneE(%rip), %rsi
	movq	%r12, %rdi
	call	__dynamic_cast@PLT
	testq	%rax, %rax
	jne	.L2036
	.p2align 4,,10
	.p2align 3
.L2035:
	movq	(%r12), %rax
	leaq	-68(%rbp), %r15
	leaq	-72(%rbp), %r14
	movq	%r13, %r8
	movsd	-88(%rbp), %xmm0
	movq	%r15, %rcx
	movq	%r14, %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	call	*48(%rax)
	cmpl	$1, 248(%rbx)
	je	.L2039
	movl	-72(%rbp), %eax
	addl	-68(%rbp), %eax
.L2040:
	cmpl	$1, 252(%rbx)
	jne	.L2034
	pxor	%xmm0, %xmm0
	movsd	-88(%rbp), %xmm3
	movq	%r13, %r8
	movq	%r15, %rcx
	cvtsi2sdl	%eax, %xmm0
	movq	(%r12), %rax
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%r12, %rdi
	subsd	%xmm0, %xmm3
	movapd	%xmm3, %xmm0
	call	*48(%rax)
	movl	-68(%rbp), %eax
	addl	-72(%rbp), %eax
	jmp	.L2034
	.p2align 4,,10
	.p2align 3
.L2039:
	movl	-68(%rbp), %eax
	pxor	%xmm0, %xmm0
	addl	-72(%rbp), %eax
	leaq	-64(%rbp), %rdx
	cvtsi2sdl	%eax, %xmm0
	movsd	-88(%rbp), %xmm2
	movq	(%r12), %rax
	movq	%r13, %r8
	leaq	-60(%rbp), %rcx
	xorl	%esi, %esi
	movq	%r12, %rdi
	subsd	%xmm0, %xmm2
	movapd	%xmm2, %xmm0
	subsd	.LC12(%rip), %xmm0
	call	*48(%rax)
	movl	-68(%rbp), %eax
	addl	-72(%rbp), %eax
	movl	%eax, %edx
	subl	-60(%rbp), %edx
	subl	-64(%rbp), %edx
	jns	.L2040
	movq	(%r12), %rax
	pxor	%xmm0, %xmm0
	movq	%r13, %r8
	movq	%r15, %rcx
	cvtsi2sdl	%edx, %xmm0
	movl	$1, %esi
	movq	%r14, %rdx
	movq	%r12, %rdi
	addsd	-88(%rbp), %xmm0
	call	*48(%rax)
	movl	-68(%rbp), %eax
	addl	-72(%rbp), %eax
	jmp	.L2034
.L2052:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3329:
	.size	_ZN6icu_678Calendar17computeZoneOffsetEddR10UErrorCode, .-_ZN6icu_678Calendar17computeZoneOffsetEddR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Calendar16computeJulianDayEv
	.type	_ZN6icu_678Calendar16computeJulianDayEv, @function
_ZN6icu_678Calendar16computeJulianDayEv:
.LFB3330:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	208(%rdi), %esi
	cmpl	$1, %esi
	jle	.L2054
	pxor	%xmm0, %xmm0
	movdqu	128(%rdi), %xmm1
	movdqu	144(%rdi), %xmm2
	movdqu	128(%rdi), %xmm3
	movl	200(%rdi), %ecx
	pcmpgtd	%xmm0, %xmm1
	movdqa	%xmm2, %xmm0
	movl	160(%rdi), %eax
	cmpl	%eax, 196(%rdi)
	cmovge	196(%rdi), %eax
	pand	%xmm3, %xmm1
	cmpl	%ecx, %eax
	pcmpgtd	%xmm1, %xmm0
	cmovl	%ecx, %eax
	movl	204(%rdi), %ecx
	cmpl	%ecx, %eax
	pand	%xmm0, %xmm2
	pandn	%xmm1, %xmm0
	cmovl	%ecx, %eax
	por	%xmm0, %xmm2
	movdqa	%xmm2, %xmm1
	psrldq	$8, %xmm1
	movdqa	%xmm1, %xmm0
	pcmpgtd	%xmm2, %xmm0
	pand	%xmm0, %xmm1
	pandn	%xmm2, %xmm0
	por	%xmm0, %xmm1
	movdqa	%xmm1, %xmm2
	psrldq	$4, %xmm2
	movdqa	%xmm2, %xmm0
	pcmpgtd	%xmm1, %xmm0
	pand	%xmm0, %xmm2
	pandn	%xmm1, %xmm0
	por	%xmm2, %xmm0
	movd	%xmm0, %edx
	cmpl	%edx, %eax
	cmovl	%edx, %eax
	cmpl	%eax, %esi
	jl	.L2054
	popq	%rbx
	movl	92(%rdi), %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2054:
	.cfi_restore_state
	movq	(%r12), %rbx
	leaq	_ZNK6icu_678Calendar23getFieldResolutionTableEv(%rip), %rdx
	leaq	_ZN6icu_678Calendar15kDatePrecedenceE(%rip), %rsi
	movq	320(%rbx), %rax
	cmpq	%rdx, %rax
	jne	.L2061
.L2056:
	movq	%r12, %rdi
	call	_ZN6icu_678Calendar13resolveFieldsEPA12_A8_Ki
	movq	%r12, %rdi
	movl	%eax, %esi
	cmpl	$23, %eax
	movl	$5, %eax
	cmove	%eax, %esi
	movq	296(%rbx), %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L2061:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	movq	(%r12), %rbx
	movq	%rax, %rsi
	jmp	.L2056
	.cfi_endproc
.LFE3330:
	.size	_ZN6icu_678Calendar16computeJulianDayEv, .-_ZN6icu_678Calendar16computeJulianDayEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Calendar11getLocalDOWEv
	.type	_ZN6icu_678Calendar11getLocalDOWEv, @function
_ZN6icu_678Calendar11getLocalDOWEv:
.LFB3334:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN6icu_678Calendar14kDOWPrecedenceE(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_678Calendar13resolveFieldsEPA12_A8_Ki
	cmpl	$7, %eax
	je	.L2063
	cmpl	$18, %eax
	jne	.L2069
	movl	84(%rbx), %edx
	subl	$1, %edx
.L2068:
	movslq	%edx, %rax
	movl	%edx, %ecx
	imulq	$-1840700269, %rax, %rax
	sarl	$31, %ecx
	shrq	$32, %rax
	addl	%edx, %eax
	sarl	$2, %eax
	subl	%ecx, %eax
	leal	0(,%rax,8), %ecx
	subl	%eax, %ecx
	movl	%edx, %eax
	subl	%ecx, %eax
	testl	%eax, %eax
	leal	7(%rax), %edx
	cmovs	%edx, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2069:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2063:
	.cfi_restore_state
	movl	40(%rbx), %edx
	subl	256(%rbx), %edx
	jmp	.L2068
	.cfi_endproc
.LFE3334:
	.size	_ZN6icu_678Calendar11getLocalDOWEv, .-_ZN6icu_678Calendar11getLocalDOWEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Calendar10updateTimeER10UErrorCode
	.type	_ZN6icu_678Calendar10updateTimeER10UErrorCode, @function
_ZN6icu_678Calendar10updateTimeER10UErrorCode:
.LFB3342:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	*224(%rax)
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L2070
	cmpb	$0, 232(%rbx)
	je	.L2075
.L2072:
	movb	$0, 9(%rbx)
.L2073:
	movb	$1, 8(%rbx)
	movb	$0, 11(%rbx)
.L2070:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2075:
	.cfi_restore_state
	cmpb	$0, 10(%rbx)
	jne	.L2073
	jmp	.L2072
	.cfi_endproc
.LFE3342:
	.size	_ZN6icu_678Calendar10updateTimeER10UErrorCode, .-_ZN6icu_678Calendar10updateTimeER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678Calendar9getLocaleE18ULocDataLocaleTypeR10UErrorCode
	.type	_ZNK6icu_678Calendar9getLocaleE18ULocDataLocaleTypeR10UErrorCode, @function
_ZNK6icu_678Calendar9getLocaleE18ULocDataLocaleTypeR10UErrorCode:
.LFB3343:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	296(%rsi), %rax
	addq	$453, %rsi
	movq	%rax, %xmm0
	movq	%rsi, %xmm1
	leaq	-48(%rbp), %rsi
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_ZNK6icu_6711LocaleBased9getLocaleE18ULocDataLocaleTypeR10UErrorCode@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2079
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2079:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3343:
	.size	_ZNK6icu_678Calendar9getLocaleE18ULocDataLocaleTypeR10UErrorCode, .-_ZNK6icu_678Calendar9getLocaleE18ULocDataLocaleTypeR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678Calendar11getLocaleIDE18ULocDataLocaleTypeR10UErrorCode
	.type	_ZNK6icu_678Calendar11getLocaleIDE18ULocDataLocaleTypeR10UErrorCode, @function
_ZNK6icu_678Calendar11getLocaleIDE18ULocDataLocaleTypeR10UErrorCode:
.LFB3344:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	296(%rdi), %rax
	addq	$453, %rdi
	movq	%rdi, %xmm1
	movq	%rax, %xmm0
	leaq	-32(%rbp), %rdi
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -32(%rbp)
	call	_ZNK6icu_6711LocaleBased11getLocaleIDE18ULocDataLocaleTypeR10UErrorCode@PLT
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L2083
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2083:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3344:
	.size	_ZNK6icu_678Calendar11getLocaleIDE18ULocDataLocaleTypeR10UErrorCode, .-_ZNK6icu_678Calendar11getLocaleIDE18ULocDataLocaleTypeR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Calendar16recalculateStampEv
	.type	_ZN6icu_678Calendar16recalculateStampEv, @function
_ZN6icu_678Calendar16recalculateStampEv:
.LFB3345:
	.cfi_startproc
	endbr64
	movl	$1, 220(%rdi)
	movq	%rdi, %r8
	movl	$1, %edi
	.p2align 4,,10
	.p2align 3
.L2089:
	xorl	%eax, %eax
	movl	$10000, %ecx
	movl	$-1, %esi
	.p2align 4,,10
	.p2align 3
.L2087:
	movl	128(%r8,%rax,4), %edx
	cmpl	%edi, %edx
	jle	.L2085
	cmpl	%ecx, %edx
	cmovl	%edx, %ecx
	cmovl	%eax, %esi
.L2085:
	addq	$1, %rax
	cmpq	$23, %rax
	jne	.L2087
	addl	$1, %edi
	cmpl	$-1, %esi
	je	.L2088
	movslq	%esi, %rsi
	movl	%edi, 220(%r8)
	movl	%edi, 128(%r8,%rsi,4)
	cmpl	$24, %edi
	jne	.L2089
	movl	$25, %edi
.L2088:
	movl	%edi, 220(%r8)
	ret
	.cfi_endproc
.LFE3345:
	.size	_ZN6icu_678Calendar16recalculateStampEv, .-_ZN6icu_678Calendar16recalculateStampEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Calendar11internalSetENS0_11EDateFieldsEi
	.type	_ZN6icu_678Calendar11internalSetENS0_11EDateFieldsEi, @function
_ZN6icu_678Calendar11internalSetENS0_11EDateFieldsEi:
.LFB3346:
	.cfi_startproc
	endbr64
	movslq	%esi, %rsi
	leaq	(%rdi,%rsi,4), %rax
	movl	%edx, 12(%rax)
	movl	$1, 128(%rax)
	movb	$1, 104(%rdi,%rsi)
	ret
	.cfi_endproc
.LFE3346:
	.size	_ZN6icu_678Calendar11internalSetENS0_11EDateFieldsEi, .-_ZN6icu_678Calendar11internalSetENS0_11EDateFieldsEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678Calendar16getBasicTimeZoneEv
	.type	_ZNK6icu_678Calendar16getBasicTimeZoneEv, @function
_ZNK6icu_678Calendar16getBasicTimeZoneEv:
.LFB3347:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	240(%rdi), %r12
	testq	%r12, %r12
	je	.L2100
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6713OlsonTimeZoneE(%rip), %rdx
	leaq	_ZTIN6icu_678TimeZoneE(%rip), %rsi
	movq	%r12, %rdi
	call	__dynamic_cast@PLT
	testq	%rax, %rax
	je	.L2107
.L2100:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2107:
	.cfi_restore_state
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6714SimpleTimeZoneE(%rip), %rdx
	leaq	_ZTIN6icu_678TimeZoneE(%rip), %rsi
	movq	%r12, %rdi
	call	__dynamic_cast@PLT
	testq	%rax, %rax
	jne	.L2100
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6717RuleBasedTimeZoneE(%rip), %rdx
	leaq	_ZTIN6icu_678TimeZoneE(%rip), %rsi
	movq	%r12, %rdi
	call	__dynamic_cast@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	jne	.L2100
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZTIN6icu_679VTimeZoneE(%rip), %rdx
	leaq	_ZTIN6icu_678TimeZoneE(%rip), %rsi
	call	__dynamic_cast@PLT
	testq	%rax, %rax
	cmove	%rbx, %r12
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3347:
	.size	_ZNK6icu_678Calendar16getBasicTimeZoneEv, .-_ZNK6icu_678Calendar16getBasicTimeZoneEv
	.section	.rodata.str1.1
.LC13:
	.string	"calendar"
.LC14:
	.string	"supplementalData"
.LC15:
	.string	"calendarPreferenceData"
.LC16:
	.string	"001"
	.text
	.align 2
	.p2align 4
	.type	_ZN6icu_678Calendar12makeInstanceERKNS_6LocaleER10UErrorCode.part.0, @function
_ZN6icu_678Calendar12makeInstanceERKNS_6LocaleER10UErrorCode.part.0:
.LFB4364:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-592(%rbp), %r14
	leaq	-320(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-600(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r14, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$600, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_676LocaleC1Ev@PLT
	movq	40(%r12), %rdi
	movq	%r13, %rcx
	movq	%r15, %rsi
	movl	$0, -600(%rbp)
	movl	$255, %edx
	call	uloc_canonicalize_67@PLT
	movl	-600(%rbp), %ecx
	testl	%ecx, %ecx
	jle	.L2109
.L2113:
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L2216
.L2111:
	movl	$656, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L2155
	movq	%rax, %rdi
	movq	%rbx, %rdx
	movq	%r12, %rsi
	call	_ZN6icu_6717GregorianCalendarC1ERKNS_6LocaleER10UErrorCode@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L2182
	jmp	.L2211
.L2130:
	movl	$616, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L2155
	movl	$2, %ecx
.L2229:
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6715IslamicCalendarC1ERKNS_6LocaleER10UErrorCodeNS0_16ECalculationTypeE@PLT
	movl	(%rbx), %r10d
	testl	%r10d, %r10d
	jle	.L2182
	movq	%r13, %rdi
	call	_ZN6icu_6715IslamicCalendarD0Ev@PLT
	movl	(%rbx), %eax
.L2146:
	testl	%eax, %eax
	jg	.L2216
	movl	$5, (%rbx)
.L2216:
	xorl	%r13d, %r13d
.L2182:
	movq	%r14, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2237
	addq	$600, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2109:
	.cfi_restore_state
	cltq
	movq	%r13, %r8
	movl	$31, %ecx
	movq	%r15, %rdi
	movb	$0, -320(%rbp,%rax)
	leaq	-352(%rbp), %rax
	leaq	.LC13(%rip), %rsi
	movq	%rax, %rdx
	movq	%rax, -632(%rbp)
	call	uloc_getKeywordValue_67@PLT
	movl	-600(%rbp), %edx
	testl	%edx, %edx
	jg	.L2116
	cltq
	xorl	%ecx, %ecx
	leaq	.LC4(%rip), %rsi
	movb	$0, -352(%rbp,%rax)
	jmp	.L2117
	.p2align 4,,10
	.p2align 3
.L2238:
	movq	-624(%rbp), %rcx
	leaq	_ZL9gCalTypes(%rip), %rax
	addq	$1, %rcx
	movq	(%rax,%rcx,8), %rsi
	testq	%rsi, %rsi
	je	.L2116
.L2117:
	movq	-632(%rbp), %rdi
	movq	%rcx, -624(%rbp)
	movl	%ecx, -616(%rbp)
	call	uprv_stricmp_67@PLT
	movl	-616(%rbp), %edx
	testl	%eax, %eax
	jne	.L2238
.L2115:
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L2216
	cmpl	$17, %edx
	ja	.L2126
	leaq	.L2128(%rip), %rcx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L2128:
	.long	.L2111-.L2128
	.long	.L2143-.L2128
	.long	.L2142-.L2128
	.long	.L2141-.L2128
	.long	.L2140-.L2128
	.long	.L2139-.L2128
	.long	.L2127-.L2128
	.long	.L2138-.L2128
	.long	.L2137-.L2128
	.long	.L2136-.L2128
	.long	.L2135-.L2128
	.long	.L2134-.L2128
	.long	.L2133-.L2128
	.long	.L2132-.L2128
	.long	.L2131-.L2128
	.long	.L2130-.L2128
	.long	.L2129-.L2128
	.long	.L2127-.L2128
	.text
	.p2align 4,,10
	.p2align 3
.L2116:
	movq	%r13, %r8
	movl	$4, %ecx
	movl	$1, %esi
	movq	%r15, %rdi
	leaq	-356(%rbp), %r9
	movl	$0, -600(%rbp)
	movq	%r9, %rdx
	movq	%r9, -616(%rbp)
	call	ulocimp_getRegionForSupplementalData_67@PLT
	movl	-600(%rbp), %eax
	movq	-616(%rbp), %r9
	testl	%eax, %eax
	jg	.L2113
	movq	%r13, %rdx
	leaq	.LC14(%rip), %rsi
	xorl	%edi, %edi
	movq	%r9, -616(%rbp)
	call	ures_openDirect_67@PLT
	movq	%r13, %rcx
	leaq	.LC15(%rip), %rsi
	movq	%rax, %rdx
	movq	%rax, %rdi
	movq	%rax, %r15
	call	ures_getByKey_67@PLT
	movq	%r13, %rcx
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	-616(%rbp), %r9
	movq	%r9, %rsi
	call	ures_getByKey_67@PLT
	movq	%rax, %r8
	movl	-600(%rbp), %eax
	testq	%r15, %r15
	je	.L2118
	cmpl	$2, %eax
	je	.L2239
.L2118:
	movb	$0, -352(%rbp)
	testq	%r8, %r8
	je	.L2120
	testl	%eax, %eax
	jle	.L2240
.L2120:
	movq	%r8, %rdi
	call	ures_close_67@PLT
	movq	%r15, %rdi
	call	ures_close_67@PLT
	jmp	.L2113
.L2131:
	movl	$624, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L2155
	movq	%rax, %rdi
	movq	%rbx, %rdx
	movq	%r12, %rsi
	call	_ZN6icu_6713DangiCalendarC1ERKNS_6LocaleER10UErrorCode@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L2182
	movq	%r13, %rdi
	call	_ZN6icu_6713DangiCalendarD0Ev@PLT
	movl	(%rbx), %eax
	jmp	.L2146
.L2132:
	movl	$656, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L2155
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6717GregorianCalendarC1ERKNS_6LocaleER10UErrorCode@PLT
	movl	(%rbx), %edx
	testl	%edx, %edx
	jle	.L2241
.L2211:
	movq	%r13, %rdi
	call	_ZN6icu_6717GregorianCalendarD0Ev@PLT
	movl	(%rbx), %eax
	jmp	.L2146
.L2133:
	movl	$616, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L2155
	movl	$1, %ecx
.L2235:
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6716EthiopicCalendarC1ERKNS_6LocaleER10UErrorCodeNS0_8EEraTypeE@PLT
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jle	.L2182
	movq	%r13, %rdi
	call	_ZN6icu_6716EthiopicCalendarD0Ev@PLT
	movl	(%rbx), %eax
	jmp	.L2146
.L2134:
	movl	$616, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L2155
	xorl	%ecx, %ecx
	jmp	.L2235
.L2135:
	movl	$616, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L2155
	movq	%r12, %rsi
	movq	%rbx, %rdx
	movq	%rax, %rdi
	call	_ZN6icu_6714CopticCalendarC1ERKNS_6LocaleER10UErrorCode@PLT
	movl	(%rbx), %esi
	testl	%esi, %esi
	jle	.L2182
	movq	%r13, %rdi
	call	_ZN6icu_6714CopticCalendarD0Ev@PLT
	movl	(%rbx), %eax
	jmp	.L2146
.L2136:
	movl	$616, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L2155
	movq	%rax, %rdi
	movq	%rbx, %rdx
	movq	%r12, %rsi
	call	_ZN6icu_6714IndianCalendarC1ERKNS_6LocaleER10UErrorCode@PLT
	movl	(%rbx), %edi
	testl	%edi, %edi
	jle	.L2182
	movq	%r13, %rdi
	call	_ZN6icu_6714IndianCalendarD0Ev@PLT
	movl	(%rbx), %eax
	jmp	.L2146
.L2137:
	movl	$624, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L2155
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6715ChineseCalendarC1ERKNS_6LocaleER10UErrorCode@PLT
	movl	(%rbx), %r8d
	testl	%r8d, %r8d
	jle	.L2182
	movq	%r13, %rdi
	call	_ZN6icu_6715ChineseCalendarD0Ev@PLT
	movl	(%rbx), %eax
	jmp	.L2146
.L2138:
	movl	$616, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L2155
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6714HebrewCalendarC1ERKNS_6LocaleER10UErrorCode@PLT
	movl	(%rbx), %r9d
	testl	%r9d, %r9d
	jle	.L2182
	movq	%r13, %rdi
	call	_ZN6icu_6714HebrewCalendarD0Ev@PLT
	movl	(%rbx), %eax
	jmp	.L2146
.L2127:
	movl	$616, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L2155
	xorl	%ecx, %ecx
	jmp	.L2229
.L2139:
	movl	$616, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L2155
	movl	$1, %ecx
	jmp	.L2229
.L2140:
	movl	$616, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L2155
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6715PersianCalendarC1ERKNS_6LocaleER10UErrorCode@PLT
	movl	(%rbx), %r11d
	testl	%r11d, %r11d
	jle	.L2182
	movq	%r13, %rdi
	call	_ZN6icu_6715PersianCalendarD0Ev@PLT
	movl	(%rbx), %eax
	jmp	.L2146
.L2141:
	movl	$656, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L2155
	movq	%r12, %rsi
	movq	%rbx, %rdx
	movq	%rax, %rdi
	call	_ZN6icu_6714TaiwanCalendarC1ERKNS_6LocaleER10UErrorCode@PLT
	movl	(%rbx), %r12d
	testl	%r12d, %r12d
	jle	.L2182
	movq	%r13, %rdi
	call	_ZN6icu_6714TaiwanCalendarD0Ev@PLT
	movl	(%rbx), %eax
	jmp	.L2146
.L2142:
	movl	$656, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L2155
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6716BuddhistCalendarC1ERKNS_6LocaleER10UErrorCode@PLT
	movl	(%rbx), %r15d
	testl	%r15d, %r15d
	jle	.L2182
	movq	%r13, %rdi
	call	_ZN6icu_6716BuddhistCalendarD0Ev@PLT
	movl	(%rbx), %eax
	jmp	.L2146
.L2143:
	movl	$656, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L2155
	movq	%rax, %rdi
	movq	%rbx, %rdx
	movq	%r12, %rsi
	call	_ZN6icu_6716JapaneseCalendarC1ERKNS_6LocaleER10UErrorCode@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L2182
	movq	%r13, %rdi
	call	_ZN6icu_6716JapaneseCalendarD0Ev@PLT
	movl	(%rbx), %eax
	jmp	.L2146
.L2129:
	movl	$616, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L2155
	movl	$3, %ecx
	jmp	.L2229
	.p2align 4,,10
	.p2align 3
.L2240:
	leaq	-596(%rbp), %rdx
	xorl	%esi, %esi
	movq	%r8, %rdi
	movq	%r13, %rcx
	movq	%r8, -616(%rbp)
	movl	$0, -596(%rbp)
	call	ures_getStringByIndex_67@PLT
	movl	-596(%rbp), %edx
	movq	-616(%rbp), %r8
	cmpl	$31, %edx
	jg	.L2120
	movq	-632(%rbp), %rsi
	movq	%rax, %rdi
	xorl	%r13d, %r13d
	call	u_UCharsToChars_67@PLT
	movslq	-596(%rbp), %rax
	movq	-616(%rbp), %r8
	leaq	.LC4(%rip), %rsi
	movb	$0, -352(%rbp,%rax)
	jmp	.L2123
	.p2align 4,,10
	.p2align 3
.L2242:
	addq	$1, %r13
	leaq	_ZL9gCalTypes(%rip), %rax
	movq	(%rax,%r13,8), %rsi
	testq	%rsi, %rsi
	je	.L2120
.L2123:
	movq	-632(%rbp), %rdi
	movq	%r8, -624(%rbp)
	movl	%r13d, -616(%rbp)
	call	uprv_stricmp_67@PLT
	movl	-616(%rbp), %edx
	movq	-624(%rbp), %r8
	testl	%eax, %eax
	jne	.L2242
	movq	%r8, %rdi
	movl	%edx, -616(%rbp)
	call	ures_close_67@PLT
	movq	%r15, %rdi
	call	ures_close_67@PLT
	movl	-616(%rbp), %edx
	jmp	.L2115
	.p2align 4,,10
	.p2align 3
.L2239:
	movq	%r13, %rcx
	xorl	%edx, %edx
	leaq	.LC16(%rip), %rsi
	movq	%r15, %rdi
	movl	$0, -600(%rbp)
	call	ures_getByKey_67@PLT
	movq	%rax, %r8
	movl	-600(%rbp), %eax
	jmp	.L2118
.L2126:
	movl	$16, (%rbx)
	jmp	.L2216
	.p2align 4,,10
	.p2align 3
.L2241:
	cmpl	$2, 256(%r13)
	je	.L2180
	movl	$2, 256(%r13)
	movb	$0, 9(%r13)
.L2180:
	cmpb	$4, 260(%r13)
	je	.L2223
	movb	$4, 260(%r13)
	movb	$0, 9(%r13)
.L2223:
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L2182
	jmp	.L2216
.L2237:
	call	__stack_chk_fail@PLT
.L2155:
	cmpl	$0, (%rbx)
	jg	.L2216
	movl	$7, (%rbx)
	jmp	.L2216
	.cfi_endproc
.LFE4364:
	.size	_ZN6icu_678Calendar12makeInstanceERKNS_6LocaleER10UErrorCode.part.0, .-_ZN6icu_678Calendar12makeInstanceERKNS_6LocaleER10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Calendar12makeInstanceERKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_678Calendar12makeInstanceERKNS_6LocaleER10UErrorCode, @function
_ZN6icu_678Calendar12makeInstanceERKNS_6LocaleER10UErrorCode:
.LFB3246:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L2244
	jmp	_ZN6icu_678Calendar12makeInstanceERKNS_6LocaleER10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L2244:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3246:
	.size	_ZN6icu_678Calendar12makeInstanceERKNS_6LocaleER10UErrorCode, .-_ZN6icu_678Calendar12makeInstanceERKNS_6LocaleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714LocaleCacheKeyINS_14SharedCalendarEE12createObjectEPKvR10UErrorCode
	.type	_ZNK6icu_6714LocaleCacheKeyINS_14SharedCalendarEE12createObjectEPKvR10UErrorCode, @function
_ZNK6icu_6714LocaleCacheKeyINS_14SharedCalendarEE12createObjectEPKvR10UErrorCode:
.LFB3222:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jg	.L2248
	addq	$16, %rdi
	movq	%rdx, %rsi
	movq	%rdx, %rbx
	call	_ZN6icu_678Calendar12makeInstanceERKNS_6LocaleER10UErrorCode.part.0
	movq	%rax, %r13
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L2248
	movl	$32, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L2249
	movq	$0, 8(%rax)
	movq	%r12, %rdi
	movq	$0, 16(%rax)
	leaq	16+_ZTVN6icu_6714SharedCalendarE(%rip), %rax
	movq	%rax, (%r12)
	movq	%r13, 24(%r12)
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
.L2245:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2248:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%r12d, %r12d
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2249:
	.cfi_restore_state
	testq	%r13, %r13
	je	.L2251
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*8(%rax)
.L2251:
	movl	$7, (%rbx)
	jmp	.L2245
	.cfi_endproc
.LFE3222:
	.size	_ZNK6icu_6714LocaleCacheKeyINS_14SharedCalendarEE12createObjectEPKvR10UErrorCode, .-_ZNK6icu_6714LocaleCacheKeyINS_14SharedCalendarEE12createObjectEPKvR10UErrorCode
	.section	.text._ZN6icu_6714LocaleCacheKeyINS_14SharedCalendarEED2Ev,"axG",@progbits,_ZN6icu_6714LocaleCacheKeyINS_14SharedCalendarEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6714LocaleCacheKeyINS_14SharedCalendarEED2Ev
	.type	_ZN6icu_6714LocaleCacheKeyINS_14SharedCalendarEED2Ev, @function
_ZN6icu_6714LocaleCacheKeyINS_14SharedCalendarEED2Ev:
.LFB3945:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6714LocaleCacheKeyINS_14SharedCalendarEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	16(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -16(%rdi)
	call	_ZN6icu_676LocaleD1Ev@PLT
	leaq	16+_ZTVN6icu_678CacheKeyINS_14SharedCalendarEEE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6712CacheKeyBaseD2Ev@PLT
	.cfi_endproc
.LFE3945:
	.size	_ZN6icu_6714LocaleCacheKeyINS_14SharedCalendarEED2Ev, .-_ZN6icu_6714LocaleCacheKeyINS_14SharedCalendarEED2Ev
	.weak	_ZN6icu_6714LocaleCacheKeyINS_14SharedCalendarEED1Ev
	.set	_ZN6icu_6714LocaleCacheKeyINS_14SharedCalendarEED1Ev,_ZN6icu_6714LocaleCacheKeyINS_14SharedCalendarEED2Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Calendar14createInstanceEPNS_8TimeZoneERKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_678Calendar14createInstanceEPNS_8TimeZoneERKNS_6LocaleER10UErrorCode, @function
_ZN6icu_678Calendar14createInstanceEPNS_8TimeZoneERKNS_6LocaleER10UErrorCode:
.LFB3247:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rdx, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$296, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6712UnifiedCache11getInstanceER10UErrorCode@PLT
	movl	(%rbx), %r10d
	testl	%r10d, %r10d
	jle	.L2293
.L2265:
	testq	%r13, %r13
	je	.L2277
	movq	0(%r13), %rax
	movq	%r13, %rdi
	xorl	%r12d, %r12d
	call	*8(%rax)
.L2255:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2294
	addq	$296, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2293:
	.cfi_restore_state
	leaq	-288(%rbp), %r9
	movq	%r12, %rsi
	xorl	%r14d, %r14d
	movq	%rax, -336(%rbp)
	movq	%r9, %rdi
	leaq	16+_ZTVN6icu_6714LocaleCacheKeyINS_14SharedCalendarEEE(%rip), %rax
	movq	%r9, -328(%rbp)
	leaq	-304(%rbp), %r15
	movq	%rax, -304(%rbp)
	movl	$0, -296(%rbp)
	movb	$0, -292(%rbp)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movl	(%rbx), %r9d
	movq	-336(%rbp), %r10
	leaq	16+_ZTVN6icu_6714LocaleCacheKeyINS_14SharedCalendarEEE(%rip), %rax
	testl	%r9d, %r9d
	movq	-328(%rbp), %r9
	jle	.L2295
.L2257:
	movq	%r9, %rdi
	movq	%rax, -304(%rbp)
	call	_ZN6icu_676LocaleD1Ev@PLT
	leaq	16+_ZTVN6icu_678CacheKeyINS_14SharedCalendarEEE(%rip), %rax
	movq	%r15, %rdi
	movq	%rax, -304(%rbp)
	call	_ZN6icu_6712CacheKeyBaseD2Ev@PLT
	movl	(%rbx), %edi
	testl	%edi, %edi
	jg	.L2265
	movq	24(%r14), %rdi
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	testq	%r12, %r12
	je	.L2296
	testq	%r13, %r13
	je	.L2266
	movq	240(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2267
	movq	(%rdi), %rax
	call	*8(%rax)
.L2267:
	movq	%r13, 240(%r12)
	movb	$0, 9(%r12)
.L2266:
	call	uprv_getUTCtime_67@PLT
	movl	(%rbx), %esi
	testl	%esi, %esi
	jg	.L2255
	comisd	.LC5(%rip), %xmm0
	jbe	.L2289
	cmpb	$0, 232(%r12)
	movq	.LC5(%rip), %rax
	movq	%rax, %xmm0
	je	.L2291
.L2271:
	leaq	20(%r12), %rdi
	movl	%r12d, %edx
	xorl	%eax, %eax
	movl	$16777217, 8(%r12)
	movq	$0, 12(%r12)
	andq	$-8, %rdi
	movq	$0, 119(%r12)
	subl	%edi, %edx
	movsd	%xmm0, 224(%r12)
	leal	127(%rdx), %ecx
	movl	%r12d, %edx
	shrl	$3, %ecx
	rep stosq
	leaq	136(%r12), %rdi
	movq	$0, 128(%r12)
	movq	$0, 212(%r12)
	andq	$-8, %rdi
	subl	%edi, %edx
	leal	220(%rdx), %ecx
	shrl	$3, %ecx
	rep stosq
	jmp	.L2255
	.p2align 4,,10
	.p2align 3
.L2289:
	movsd	.LC6(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jbe	.L2271
	cmpb	$0, 232(%r12)
	movapd	%xmm1, %xmm0
	jne	.L2271
.L2291:
	movl	$1, (%rbx)
	jmp	.L2255
	.p2align 4,,10
	.p2align 3
.L2295:
	leaq	-312(%rbp), %rdx
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	movq	%r10, %rdi
	leaq	-316(%rbp), %r8
	movl	$0, -316(%rbp)
	movq	$0, -312(%rbp)
	call	_ZNK6icu_6712UnifiedCache4_getERKNS_12CacheKeyBaseERPKNS_12SharedObjectEPKvR10UErrorCode@PLT
	movl	-316(%rbp), %edx
	movq	-312(%rbp), %r14
	leaq	16+_ZTVN6icu_6714LocaleCacheKeyINS_14SharedCalendarEEE(%rip), %rax
	movq	-328(%rbp), %r9
	testl	%edx, %edx
	jle	.L2297
	testq	%r14, %r14
	je	.L2261
	movq	%r14, %r12
	xorl	%r14d, %r14d
.L2260:
	movq	%r12, %rdi
	movq	%r9, -328(%rbp)
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	movl	(%rbx), %r8d
	movl	-316(%rbp), %edx
	leaq	16+_ZTVN6icu_6714LocaleCacheKeyINS_14SharedCalendarEEE(%rip), %rax
	movq	-328(%rbp), %r9
	testl	%r8d, %r8d
	jne	.L2262
.L2261:
	movl	%edx, (%rbx)
	jmp	.L2257
	.p2align 4,,10
	.p2align 3
.L2296:
	movl	$7, (%rbx)
	jmp	.L2265
	.p2align 4,,10
	.p2align 3
.L2262:
	testl	%edx, %edx
	jle	.L2257
	movl	%edx, (%rbx)
	jmp	.L2257
	.p2align 4,,10
	.p2align 3
.L2297:
	testq	%r14, %r14
	je	.L2259
	movq	%r14, %rdi
	movq	%r14, %r12
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
	movq	-328(%rbp), %r9
	jmp	.L2260
	.p2align 4,,10
	.p2align 3
.L2259:
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jne	.L2257
	movl	%edx, (%rbx)
	jmp	.L2257
.L2294:
	call	__stack_chk_fail@PLT
.L2277:
	xorl	%r12d, %r12d
	jmp	.L2255
	.cfi_endproc
.LFE3247:
	.size	_ZN6icu_678Calendar14createInstanceEPNS_8TimeZoneERKNS_6LocaleER10UErrorCode, .-_ZN6icu_678Calendar14createInstanceEPNS_8TimeZoneERKNS_6LocaleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Calendar14createInstanceER10UErrorCode
	.type	_ZN6icu_678Calendar14createInstanceER10UErrorCode, @function
_ZN6icu_678Calendar14createInstanceER10UErrorCode:
.LFB3243:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	call	_ZN6icu_676Locale10getDefaultEv@PLT
	movq	%rax, %r12
	call	_ZN6icu_678TimeZone13createDefaultEv@PLT
	movq	%r13, %rdx
	movq	%r12, %rsi
	popq	%r12
	movq	%rax, %rdi
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_678Calendar14createInstanceEPNS_8TimeZoneERKNS_6LocaleER10UErrorCode
	.cfi_endproc
.LFE3243:
	.size	_ZN6icu_678Calendar14createInstanceER10UErrorCode, .-_ZN6icu_678Calendar14createInstanceER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Calendar14createInstanceERKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_678Calendar14createInstanceERKNS_6LocaleER10UErrorCode, @function
_ZN6icu_678Calendar14createInstanceERKNS_6LocaleER10UErrorCode:
.LFB3245:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN6icu_678TimeZone13createDefaultEv@PLT
	movq	%r13, %rdx
	movq	%r12, %rsi
	popq	%r12
	movq	%rax, %rdi
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_678Calendar14createInstanceEPNS_8TimeZoneERKNS_6LocaleER10UErrorCode
	.cfi_endproc
.LFE3245:
	.size	_ZN6icu_678Calendar14createInstanceERKNS_6LocaleER10UErrorCode, .-_ZN6icu_678Calendar14createInstanceERKNS_6LocaleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Calendar14createInstanceERKNS_8TimeZoneERKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_678Calendar14createInstanceERKNS_8TimeZoneERKNS_6LocaleER10UErrorCode, @function
_ZN6icu_678Calendar14createInstanceERKNS_8TimeZoneERKNS_6LocaleER10UErrorCode:
.LFB3250:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$8, %rsp
	call	_ZN6icu_678TimeZone13createDefaultEv@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdx
	movq	%rax, %rdi
	call	_ZN6icu_678Calendar14createInstanceEPNS_8TimeZoneERKNS_6LocaleER10UErrorCode
	movq	%rax, %r12
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L2302
	testq	%r12, %r12
	jne	.L2318
.L2302:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2318:
	.cfi_restore_state
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*96(%rax)
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L2302
	movq	240(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2307
	movq	(%rdi), %rax
	call	*8(%rax)
.L2307:
	movq	%rbx, 240(%r12)
	movq	%r12, %rax
	movb	$0, 9(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3250:
	.size	_ZN6icu_678Calendar14createInstanceERKNS_8TimeZoneERKNS_6LocaleER10UErrorCode, .-_ZN6icu_678Calendar14createInstanceERKNS_8TimeZoneERKNS_6LocaleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Calendar14createInstanceERKNS_8TimeZoneER10UErrorCode
	.type	_ZN6icu_678Calendar14createInstanceERKNS_8TimeZoneER10UErrorCode, @function
_ZN6icu_678Calendar14createInstanceERKNS_8TimeZoneER10UErrorCode:
.LFB3244:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_676Locale10getDefaultEv@PLT
	movq	%rax, %r12
	call	_ZN6icu_678TimeZone13createDefaultEv@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdx
	movq	%rax, %rdi
	call	_ZN6icu_678Calendar14createInstanceEPNS_8TimeZoneERKNS_6LocaleER10UErrorCode
	movq	%rax, %r12
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L2319
	testq	%r12, %r12
	jne	.L2335
.L2319:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2335:
	.cfi_restore_state
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*96(%rax)
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L2319
	movq	240(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2324
	movq	(%rdi), %rax
	call	*8(%rax)
.L2324:
	movq	%rbx, 240(%r12)
	movq	%r12, %rax
	movb	$0, 9(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3244:
	.size	_ZN6icu_678Calendar14createInstanceERKNS_8TimeZoneER10UErrorCode, .-_ZN6icu_678Calendar14createInstanceERKNS_8TimeZoneER10UErrorCode
	.section	.text._ZN6icu_6714LocaleCacheKeyINS_14SharedCalendarEED0Ev,"axG",@progbits,_ZN6icu_6714LocaleCacheKeyINS_14SharedCalendarEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6714LocaleCacheKeyINS_14SharedCalendarEED0Ev
	.type	_ZN6icu_6714LocaleCacheKeyINS_14SharedCalendarEED0Ev, @function
_ZN6icu_6714LocaleCacheKeyINS_14SharedCalendarEED0Ev:
.LFB3947:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6714LocaleCacheKeyINS_14SharedCalendarEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	16(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -16(%rdi)
	call	_ZN6icu_676LocaleD1Ev@PLT
	leaq	16+_ZTVN6icu_678CacheKeyINS_14SharedCalendarEEE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	call	_ZN6icu_6712CacheKeyBaseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3947:
	.size	_ZN6icu_6714LocaleCacheKeyINS_14SharedCalendarEED0Ev, .-_ZN6icu_6714LocaleCacheKeyINS_14SharedCalendarEED0Ev
	.section	.rodata.str1.1
.LC17:
	.string	"weekData"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Calendar11setWeekDataERKNS_6LocaleEPKcR10UErrorCode
	.type	_ZN6icu_678Calendar11setWeekDataERKNS_6LocaleEPKcR10UErrorCode, @function
_ZN6icu_678Calendar11setWeekDataERKNS_6LocaleEPKcR10UErrorCode:
.LFB3341:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$984, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -1000(%rbp)
	movl	(%rcx), %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jle	.L2388
.L2338:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2389
	addq	$984, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2388:
	.cfi_restore_state
	movb	$1, 260(%rdi)
	movq	%rdi, %r12
	movq	%rsi, %r13
	movq	%rcx, %rbx
	movabsq	$371085174374400001, %rax
	leaq	-960(%rbp), %r14
	movl	$1, 256(%rdi)
	movq	%rax, 272(%rdi)
	movq	$7, 264(%rdi)
	movq	%r14, %rdi
	movq	%r14, -1016(%rbp)
	movl	$0, -980(%rbp)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	leaq	-980(%rbp), %r8
	movq	%r14, %rdi
	leaq	-736(%rbp), %r14
	movq	%r8, %rsi
	movq	%r8, -1008(%rbp)
	call	_ZN6icu_676Locale15minimizeSubtagsER10UErrorCode@PLT
	movq	%r14, %rdi
	call	_ZN6icu_676LocaleC1Ev@PLT
	cmpb	$0, 26(%r13)
	movq	-1008(%rbp), %r8
	je	.L2340
	cmpb	$0, 20(%r13)
	je	.L2341
	cmpb	$0, -940(%rbp)
	je	.L2340
.L2341:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	jmp	.L2342
	.p2align 4,,10
	.p2align 3
.L2340:
	leaq	-512(%rbp), %r15
	movq	%r13, %rsi
	movq	%r8, -1008(%rbp)
	movl	$0, -980(%rbp)
	movq	%r15, %rdi
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movq	-1008(%rbp), %r8
	movq	%r15, %rdi
	movq	%r8, %rsi
	call	_ZN6icu_676Locale16addLikelySubtagsER10UErrorCode@PLT
	leaq	-288(%rbp), %r10
	leaq	-486(%rbp), %rdx
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	%r10, %rdi
	leaq	-504(%rbp), %rsi
	movq	%r10, -1008(%rbp)
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	movq	-1008(%rbp), %r10
	movq	%r14, %rdi
	movq	%r10, %rsi
	call	_ZN6icu_676LocaleaSEOS0_@PLT
	movq	-1008(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
.L2342:
	movq	%r14, %rdi
	call	_ZNK6icu_676Locale11getBaseNameEv@PLT
	xorl	%edi, %edi
	movq	%rbx, %rdx
	movq	%rax, %rsi
	call	ures_open_67@PLT
	movq	%rbx, %rcx
	leaq	_ZN6icu_67L9gCalendarE(%rip), %rsi
	movq	%rax, %rdx
	movq	%rax, %rdi
	movq	%rax, %r15
	call	ures_getByKey_67@PLT
	cmpq	$0, -1000(%rbp)
	je	.L2345
	movq	-1000(%rbp), %rax
	cmpb	$0, (%rax)
	jne	.L2390
.L2345:
	xorl	%r10d, %r10d
.L2344:
	movl	$0, (%rbx)
	movq	%r10, %rdx
	movq	%rbx, %rcx
	movq	%r15, %rdi
	leaq	_ZN6icu_67L10gGregorianE(%rip), %rsi
	call	ures_getByKeyWithFallback_67@PLT
	movq	%rbx, %rcx
	leaq	_ZN6icu_67L11gMonthNamesE(%rip), %rsi
	movq	%rax, %rdx
	movq	%rax, %rdi
	movq	%rax, -1000(%rbp)
	call	ures_getByKeyWithFallback_67@PLT
	movl	(%rbx), %eax
	movq	-1000(%rbp), %r10
	testl	%eax, %eax
	jle	.L2391
.L2347:
	movl	$-128, (%rbx)
	testq	%r10, %r10
	je	.L2358
.L2386:
	movq	%r10, %rdi
	call	ures_close_67@PLT
.L2358:
	testq	%r15, %r15
	je	.L2359
	movq	%r15, %rdi
	call	ures_close_67@PLT
.L2359:
	movq	%r14, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	movq	-1016(%rbp), %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	jmp	.L2338
	.p2align 4,,10
	.p2align 3
.L2390:
	leaq	_ZN6icu_67L10gGregorianE(%rip), %rsi
	movq	%rax, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L2345
	movq	-1000(%rbp), %rsi
	movq	%rbx, %rcx
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	ures_getByKeyWithFallback_67@PLT
	movq	%rbx, %rcx
	leaq	_ZN6icu_67L11gMonthNamesE(%rip), %rsi
	movq	%rax, %rdx
	movq	%rax, %rdi
	movq	%rax, -1000(%rbp)
	call	ures_getByKeyWithFallback_67@PLT
	movq	-1000(%rbp), %r10
	testq	%r10, %r10
	je	.L2345
	movl	(%rbx), %eax
	cmpl	$2, %eax
	je	.L2344
	testl	%eax, %eax
	jg	.L2347
.L2391:
	leaq	296(%r12), %rax
	xorl	%esi, %esi
	movq	%r10, %rdi
	movq	%rbx, %rdx
	movq	%rax, %xmm0
	leaq	453(%r12), %rax
	movq	%r10, -1008(%rbp)
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -976(%rbp)
	call	ures_getLocaleByType_67@PLT
	movq	-1008(%rbp), %r10
	movq	%rbx, %rdx
	movl	$1, %esi
	movq	%rax, -1000(%rbp)
	movq	%r10, %rdi
	movq	%r10, -1024(%rbp)
	call	ures_getLocaleByType_67@PLT
	movq	-1000(%rbp), %r8
	leaq	-976(%rbp), %r9
	movq	%r9, %rdi
	movq	%rax, %rsi
	movq	%r9, -1008(%rbp)
	movq	%r8, %rdx
	call	_ZN6icu_6711LocaleBased12setLocaleIDsEPKcS2_@PLT
	leaq	-60(%rbp), %r11
	movq	40(%r13), %rdi
	movq	%rbx, %r8
	movq	%r11, %rdx
	movl	$4, %ecx
	movl	$1, %esi
	movq	%r11, -1000(%rbp)
	call	ulocimp_getRegionForSupplementalData_67@PLT
	xorl	%edi, %edi
	movq	%rbx, %rdx
	leaq	.LC14(%rip), %rsi
	call	ures_openDirect_67@PLT
	movq	%rbx, %rcx
	leaq	.LC17(%rip), %rsi
	movq	%rax, %rdx
	movq	%rax, %rdi
	movq	%rax, %r13
	call	ures_getByKey_67@PLT
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%rbx, %rcx
	movq	-1000(%rbp), %r11
	movq	%r11, %rsi
	call	ures_getByKey_67@PLT
	testq	%r13, %r13
	movq	-1008(%rbp), %r9
	movq	-1024(%rbp), %r10
	movq	%rax, %rdi
	movl	(%rbx), %eax
	je	.L2350
	cmpl	$2, %eax
	je	.L2392
.L2350:
	testl	%eax, %eax
	jle	.L2353
	movl	$-128, (%rbx)
.L2354:
	movq	%r10, -1000(%rbp)
	call	ures_close_67@PLT
	movq	%r13, %rdi
	call	ures_close_67@PLT
	movq	-1000(%rbp), %r10
	testq	%r10, %r10
	jne	.L2386
	jmp	.L2358
	.p2align 4,,10
	.p2align 3
.L2353:
	movq	%rbx, %rdx
	movq	%r9, %rsi
	movq	%r10, -1008(%rbp)
	movq	%rdi, -1000(%rbp)
	call	ures_getIntVector_67@PLT
	movl	(%rbx), %edx
	movq	-1000(%rbp), %rdi
	movq	-1008(%rbp), %r10
	testl	%edx, %edx
	jle	.L2393
.L2355:
	movl	$3, (%rbx)
	jmp	.L2354
	.p2align 4,,10
	.p2align 3
.L2392:
	movl	$0, (%rbx)
	movq	%r13, %rdi
	movq	%rbx, %rcx
	xorl	%edx, %edx
	leaq	.LC16(%rip), %rsi
	movq	%r9, -1008(%rbp)
	movq	%r10, -1000(%rbp)
	call	ures_getByKey_67@PLT
	movq	-1008(%rbp), %r9
	movq	-1000(%rbp), %r10
	movq	%rax, %rdi
	movl	(%rbx), %eax
	jmp	.L2350
	.p2align 4,,10
	.p2align 3
.L2393:
	cmpl	$6, -976(%rbp)
	jne	.L2355
	movl	(%rax), %edx
	leal	-1(%rdx), %ecx
	cmpl	$6, %ecx
	ja	.L2355
	movl	4(%rax), %esi
	leal	-1(%rsi), %ecx
	cmpl	$6, %ecx
	ja	.L2355
	movl	8(%rax), %ecx
	subl	$1, %ecx
	cmpl	$6, %ecx
	ja	.L2355
	movl	16(%rax), %ecx
	subl	$1, %ecx
	cmpl	$6, %ecx
	ja	.L2355
	movb	%sil, 260(%r12)
	movl	%edx, 256(%r12)
	movl	8(%rax), %edx
	movl	%edx, 264(%r12)
	movl	12(%rax), %edx
	movl	%edx, 268(%r12)
	movl	16(%rax), %edx
	movl	20(%rax), %eax
	movl	%edx, 272(%r12)
	movl	%eax, 276(%r12)
	jmp	.L2354
.L2389:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3341:
	.size	_ZN6icu_678Calendar11setWeekDataERKNS_6LocaleEPKcR10UErrorCode, .-_ZN6icu_678Calendar11setWeekDataERKNS_6LocaleEPKcR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_678CalendarC2ER10UErrorCode
	.type	_ZN6icu_678CalendarC2ER10UErrorCode, @function
_ZN6icu_678CalendarC2ER10UErrorCode:
.LFB3227:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_678CalendarE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	addq	$20, %rdi
	movq	%rax, -20(%rdi)
	movl	%r12d, %edx
	xorl	%eax, %eax
	movq	$0, -8(%rdi)
	movq	$0, 99(%rdi)
	movl	$0, -12(%rdi)
	movl	$2, 200(%rdi)
	movq	$0x000000000, 204(%rdi)
	movb	$1, 212(%rdi)
	movq	$0, 220(%rdi)
	movq	$0, 228(%rdi)
	movb	$0, 276(%rdi)
	movb	$0, 433(%rdi)
	andq	$-8, %rdi
	subl	%edi, %edx
	leal	127(%rdx), %ecx
	movl	%r12d, %edx
	shrl	$3, %ecx
	rep stosq
	leaq	136(%r12), %rdi
	movq	$0, 128(%r12)
	movq	$0, 212(%r12)
	andq	$-8, %rdi
	subl	%edi, %edx
	leal	220(%rdx), %ecx
	shrl	$3, %ecx
	rep stosq
	movl	(%rsi), %eax
	testl	%eax, %eax
	jle	.L2398
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2398:
	.cfi_restore_state
	movq	%rsi, %r13
	call	_ZN6icu_678TimeZone13createDefaultEv@PLT
	movq	%rax, 240(%r12)
	testq	%rax, %rax
	je	.L2399
.L2396:
	call	_ZN6icu_676Locale10getDefaultEv@PLT
	movq	%r13, %rcx
	movq	%r12, %rdi
	xorl	%edx, %edx
	popq	%r12
	movq	%rax, %rsi
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_678Calendar11setWeekDataERKNS_6LocaleEPKcR10UErrorCode
.L2399:
	.cfi_restore_state
	movl	$7, 0(%r13)
	jmp	.L2396
	.cfi_endproc
.LFE3227:
	.size	_ZN6icu_678CalendarC2ER10UErrorCode, .-_ZN6icu_678CalendarC2ER10UErrorCode
	.globl	_ZN6icu_678CalendarC1ER10UErrorCode
	.set	_ZN6icu_678CalendarC1ER10UErrorCode,_ZN6icu_678CalendarC2ER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_678CalendarC2EPNS_8TimeZoneERKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_678CalendarC2EPNS_8TimeZoneERKNS_6LocaleER10UErrorCode, @function
_ZN6icu_678CalendarC2EPNS_8TimeZoneERKNS_6LocaleER10UErrorCode:
.LFB3230:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_678CalendarE(%rip), %rax
	movq	%rsi, %r9
	movq	%rdi, %r8
	movq	%rdx, %rsi
	movq	%rax, (%rdi)
	movl	(%rcx), %eax
	movq	%rcx, %r10
	movl	$0, 8(%rdi)
	movl	$2, 220(%rdi)
	movq	$0x000000000, 224(%rdi)
	movb	$1, 232(%rdi)
	movq	$0, 240(%rdi)
	movq	$0, 248(%rdi)
	movb	$0, 296(%rdi)
	movb	$0, 453(%rdi)
	testl	%eax, %eax
	jg	.L2407
	testq	%r9, %r9
	je	.L2408
	movq	$0, 12(%rdi)
	leaq	20(%rdi), %rdi
	movl	%r8d, %edx
	xorl	%eax, %eax
	movq	$0, 99(%rdi)
	andq	$-8, %rdi
	subl	%edi, %edx
	leal	127(%rdx), %ecx
	movl	%r8d, %edx
	shrl	$3, %ecx
	rep stosq
	leaq	136(%r8), %rdi
	movq	$0, 128(%r8)
	movq	$0, 212(%r8)
	andq	$-8, %rdi
	subl	%edi, %edx
	leal	220(%rdx), %ecx
	xorl	%edx, %edx
	shrl	$3, %ecx
	rep stosq
	movq	%r9, 240(%r8)
	movq	%r10, %rcx
	movq	%r8, %rdi
	jmp	_ZN6icu_678Calendar11setWeekDataERKNS_6LocaleEPKcR10UErrorCode
	.p2align 4,,10
	.p2align 3
.L2407:
	testq	%r9, %r9
	je	.L2400
	movq	(%r9), %rax
	movq	%r9, %rdi
	movq	8(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L2400:
	ret
.L2408:
	movl	$1, (%rcx)
	ret
	.cfi_endproc
.LFE3230:
	.size	_ZN6icu_678CalendarC2EPNS_8TimeZoneERKNS_6LocaleER10UErrorCode, .-_ZN6icu_678CalendarC2EPNS_8TimeZoneERKNS_6LocaleER10UErrorCode
	.globl	_ZN6icu_678CalendarC1EPNS_8TimeZoneERKNS_6LocaleER10UErrorCode
	.set	_ZN6icu_678CalendarC1EPNS_8TimeZoneERKNS_6LocaleER10UErrorCode,_ZN6icu_678CalendarC2EPNS_8TimeZoneERKNS_6LocaleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_678CalendarC2ERKNS_8TimeZoneERKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_678CalendarC2ERKNS_8TimeZoneERKNS_6LocaleER10UErrorCode, @function
_ZN6icu_678CalendarC2ERKNS_8TimeZoneERKNS_6LocaleER10UErrorCode:
.LFB3233:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_678CalendarE(%rip), %rax
	movl	$0, 8(%rdi)
	movq	%rax, (%rdi)
	movl	(%rcx), %eax
	movl	$2, 220(%rdi)
	movq	$0x000000000, 224(%rdi)
	movb	$1, 232(%rdi)
	movq	$0, 240(%rdi)
	movq	$0, 248(%rdi)
	movb	$0, 296(%rdi)
	movb	$0, 453(%rdi)
	testl	%eax, %eax
	jg	.L2409
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	leaq	20(%rdi), %rdi
	movl	%r12d, %edx
	subq	$8, %rsp
	movq	$0, -8(%rdi)
	movq	$0, 99(%rdi)
	andq	$-8, %rdi
	subl	%edi, %edx
	leal	127(%rdx), %ecx
	movl	%r12d, %edx
	shrl	$3, %ecx
	rep stosq
	leaq	136(%r12), %rdi
	movq	$0, 128(%r12)
	movq	$0, 212(%r12)
	andq	$-8, %rdi
	subl	%edi, %edx
	leal	220(%rdx), %ecx
	shrl	$3, %ecx
	rep stosq
	movq	%rsi, %rdi
	movq	(%rsi), %rax
	call	*96(%rax)
	movq	%rax, 240(%r12)
	testq	%rax, %rax
	je	.L2415
.L2411:
	addq	$8, %rsp
	movq	%r13, %rcx
	movq	%r14, %rsi
	movq	%r12, %rdi
	xorl	%edx, %edx
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_678Calendar11setWeekDataERKNS_6LocaleEPKcR10UErrorCode
	.p2align 4,,10
	.p2align 3
.L2409:
	ret
	.p2align 4,,10
	.p2align 3
.L2415:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movl	$7, 0(%r13)
	jmp	.L2411
	.cfi_endproc
.LFE3233:
	.size	_ZN6icu_678CalendarC2ERKNS_8TimeZoneERKNS_6LocaleER10UErrorCode, .-_ZN6icu_678CalendarC2ERKNS_8TimeZoneERKNS_6LocaleER10UErrorCode
	.globl	_ZN6icu_678CalendarC1ERKNS_8TimeZoneERKNS_6LocaleER10UErrorCode
	.set	_ZN6icu_678CalendarC1ERKNS_8TimeZoneERKNS_6LocaleER10UErrorCode,_ZN6icu_678CalendarC2ERKNS_8TimeZoneERKNS_6LocaleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Calendar25getCalendarTypeFromLocaleERKNS_6LocaleEPciR10UErrorCode
	.type	_ZN6icu_678Calendar25getCalendarTypeFromLocaleERKNS_6LocaleEPciR10UErrorCode, @function
_ZN6icu_678Calendar25getCalendarTypeFromLocaleERKNS_6LocaleEPciR10UErrorCode:
.LFB3251:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movq	%rcx, %rdi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movslq	%edx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$296, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6712UnifiedCache11getInstanceER10UErrorCode@PLT
	movl	(%rbx), %r8d
	testl	%r8d, %r8d
	jle	.L2434
.L2416:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2435
	addq	$296, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2434:
	.cfi_restore_state
	leaq	-288(%rbp), %r10
	leaq	16+_ZTVN6icu_6714LocaleCacheKeyINS_14SharedCalendarEEE(%rip), %r9
	movq	%r14, %rsi
	movq	%rax, -336(%rbp)
	movq	%r10, %rdi
	xorl	%r14d, %r14d
	movq	%r9, -304(%rbp)
	leaq	-304(%rbp), %r15
	movq	%r10, -328(%rbp)
	movl	$0, -296(%rbp)
	movb	$0, -292(%rbp)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movl	(%rbx), %edi
	movq	-328(%rbp), %r10
	leaq	16+_ZTVN6icu_6714LocaleCacheKeyINS_14SharedCalendarEEE(%rip), %r9
	movq	-336(%rbp), %rax
	testl	%edi, %edi
	jle	.L2436
.L2419:
	movq	%r10, %rdi
	movq	%r9, -304(%rbp)
	call	_ZN6icu_676LocaleD1Ev@PLT
	leaq	16+_ZTVN6icu_678CacheKeyINS_14SharedCalendarEEE(%rip), %rax
	movq	%r15, %rdi
	movq	%rax, -304(%rbp)
	call	_ZN6icu_6712CacheKeyBaseD2Ev@PLT
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L2416
	movq	24(%r14), %rdi
	movq	(%rdi), %rax
	call	*184(%rax)
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	strncpy@PLT
	movq	%r14, %rdi
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	cmpb	$0, -1(%r13,%r12)
	je	.L2416
	movl	$15, (%rbx)
	jmp	.L2416
	.p2align 4,,10
	.p2align 3
.L2436:
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	leaq	-312(%rbp), %rdx
	movq	%r15, %rsi
	leaq	-316(%rbp), %r8
	movl	$0, -316(%rbp)
	movq	$0, -312(%rbp)
	call	_ZNK6icu_6712UnifiedCache4_getERKNS_12CacheKeyBaseERPKNS_12SharedObjectEPKvR10UErrorCode@PLT
	movl	-316(%rbp), %eax
	movq	-312(%rbp), %r14
	leaq	16+_ZTVN6icu_6714LocaleCacheKeyINS_14SharedCalendarEEE(%rip), %r9
	movq	-328(%rbp), %r10
	testl	%eax, %eax
	jle	.L2437
	testq	%r14, %r14
	je	.L2423
	movq	%r14, %r8
	xorl	%r14d, %r14d
.L2422:
	movq	%r8, %rdi
	movq	%r10, -328(%rbp)
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	movl	(%rbx), %esi
	movl	-316(%rbp), %eax
	leaq	16+_ZTVN6icu_6714LocaleCacheKeyINS_14SharedCalendarEEE(%rip), %r9
	movq	-328(%rbp), %r10
	testl	%esi, %esi
	je	.L2423
	testl	%eax, %eax
	jle	.L2419
	.p2align 4,,10
	.p2align 3
.L2423:
	movl	%eax, (%rbx)
	jmp	.L2419
	.p2align 4,,10
	.p2align 3
.L2437:
	testq	%r14, %r14
	je	.L2421
	movq	%r14, %rdi
	movq	%r10, -336(%rbp)
	movq	%r14, -328(%rbp)
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
	movq	-328(%rbp), %r8
	movq	-336(%rbp), %r10
	jmp	.L2422
	.p2align 4,,10
	.p2align 3
.L2421:
	movl	(%rbx), %edx
	testl	%edx, %edx
	jne	.L2419
	movl	%eax, (%rbx)
	jmp	.L2419
.L2435:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3251:
	.size	_ZN6icu_678Calendar25getCalendarTypeFromLocaleERKNS_6LocaleEPciR10UErrorCode, .-_ZN6icu_678Calendar25getCalendarTypeFromLocaleERKNS_6LocaleEPciR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678Calendar34getImmediatePreviousZoneTransitionEdPdR10UErrorCode
	.type	_ZNK6icu_678Calendar34getImmediatePreviousZoneTransitionEdPdR10UErrorCode, @function
_ZNK6icu_678Calendar34getImmediatePreviousZoneTransitionEdPdR10UErrorCode:
.LFB3327:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	subq	$64, %rsp
	movq	240(%rdi), %r12
	movsd	%xmm0, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%r12, %r12
	je	.L2439
	movq	%rsi, %r13
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6713OlsonTimeZoneE(%rip), %rdx
	movq	%r12, %rdi
	leaq	_ZTIN6icu_678TimeZoneE(%rip), %rsi
	call	__dynamic_cast@PLT
	testq	%rax, %rax
	je	.L2450
.L2440:
	leaq	-80(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN6icu_6718TimeZoneTransitionC1Ev@PLT
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	%r14, %rdx
	movsd	-88(%rbp), %xmm0
	movl	$1, %esi
	call	*120(%rax)
	movl	%eax, %r12d
	testb	%al, %al
	jne	.L2451
	movl	$5, (%rbx)
	movq	%r14, %rdi
	call	_ZN6icu_6718TimeZoneTransitionD1Ev@PLT
.L2438:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2452
	addq	$64, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2450:
	.cfi_restore_state
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6714SimpleTimeZoneE(%rip), %rdx
	leaq	_ZTIN6icu_678TimeZoneE(%rip), %rsi
	movq	%r12, %rdi
	call	__dynamic_cast@PLT
	testq	%rax, %rax
	jne	.L2440
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6717RuleBasedTimeZoneE(%rip), %rdx
	leaq	_ZTIN6icu_678TimeZoneE(%rip), %rsi
	movq	%r12, %rdi
	call	__dynamic_cast@PLT
	testq	%rax, %rax
	jne	.L2440
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_679VTimeZoneE(%rip), %rdx
	leaq	_ZTIN6icu_678TimeZoneE(%rip), %rsi
	movq	%r12, %rdi
	call	__dynamic_cast@PLT
	testq	%rax, %rax
	jne	.L2440
	.p2align 4,,10
	.p2align 3
.L2439:
	movl	$16, (%rbx)
	xorl	%r12d, %r12d
	jmp	.L2438
	.p2align 4,,10
	.p2align 3
.L2451:
	movq	%r14, %rdi
	movl	$1, %r12d
	call	_ZNK6icu_6718TimeZoneTransition7getTimeEv@PLT
	movq	%r14, %rdi
	movsd	%xmm0, 0(%r13)
	call	_ZN6icu_6718TimeZoneTransitionD1Ev@PLT
	jmp	.L2438
.L2452:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3327:
	.size	_ZNK6icu_678Calendar34getImmediatePreviousZoneTransitionEdPdR10UErrorCode, .-_ZNK6icu_678Calendar34getImmediatePreviousZoneTransitionEdPdR10UErrorCode
	.align 2
	.p2align 4
	.type	_ZN6icu_678Calendar3addE19UCalendarDateFieldsiR10UErrorCode.part.0, @function
_ZN6icu_678Calendar3addE19UCalendarDateFieldsiR10UErrorCode.part.0:
.LFB4368:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$21, %esi
	ja	.L2609
	pxor	%xmm0, %xmm0
	movl	%edx, %r15d
	movq	%rdi, %r14
	movl	%esi, %r12d
	cvtsi2sdl	%edx, %xmm0
	leaq	.L2456(%rip), %rcx
	movl	%esi, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L2456:
	.long	.L2465-.L2456
	.long	.L2459-.L2456
	.long	.L2458-.L2456
	.long	.L2464-.L2456
	.long	.L2464-.L2456
	.long	.L2457-.L2456
	.long	.L2457-.L2456
	.long	.L2457-.L2456
	.long	.L2464-.L2456
	.long	.L2463-.L2456
	.long	.L2462-.L2456
	.long	.L2462-.L2456
	.long	.L2461-.L2456
	.long	.L2460-.L2456
	.long	.L2455-.L2456
	.long	.L2609-.L2456
	.long	.L2609-.L2456
	.long	.L2459-.L2456
	.long	.L2457-.L2456
	.long	.L2458-.L2456
	.long	.L2457-.L2456
	.long	.L2455-.L2456
	.text
	.p2align 4,,10
	.p2align 3
.L2461:
	mulsd	.LC20(%rip), %xmm0
.L2455:
	movl	0(%r13), %r11d
	testl	%r11d, %r11d
	jg	.L2453
	xorl	%ebx, %ebx
	xorl	%r12d, %r12d
	xorl	%r15d, %r15d
	cmpb	$0, 8(%r14)
	je	.L2576
.L2582:
	addsd	224(%r14), %xmm0
	comisd	.LC5(%rip), %xmm0
	ja	.L2577
	movsd	.LC6(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jbe	.L2583
	cmpb	$0, 232(%r14)
	movapd	%xmm1, %xmm0
	je	.L2729
.L2583:
	leaq	20(%r14), %rdi
	movl	%r14d, %edx
	xorl	%eax, %eax
	movl	$16777217, 8(%r14)
	andq	$-8, %rdi
	movq	$0, 12(%r14)
	subl	%edi, %edx
	movq	$0, 119(%r14)
	movsd	%xmm0, 224(%r14)
	leal	127(%rdx), %ecx
	movl	%r14d, %edx
	shrl	$3, %ecx
	rep stosq
	leaq	136(%r14), %rdi
	movq	$0, 128(%r14)
	movq	$0, 212(%r14)
	andq	$-8, %rdi
	subl	%edi, %edx
	leal	220(%rdx), %ecx
	shrl	$3, %ecx
	rep stosq
.L2623:
	testb	%r15b, %r15b
	je	.L2453
	movl	0(%r13), %eax
	testl	%eax, %eax
	jle	.L2624
.L2586:
	testl	%r12d, %r12d
	je	.L2453
	movsd	224(%r14), %xmm2
	movsd	%xmm2, -72(%rbp)
.L2620:
	xorl	%r15d, %r15d
	jmp	.L2588
.L2744:
	movl	$7, 0(%r13)
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L2477:
	movl	12(%r14), %eax
	cmpl	%eax, -76(%rbp)
	jl	.L2735
	cmpl	%eax, %ebx
	jle	.L2453
	cmpb	$0, 11(%r14)
	je	.L2507
	movq	(%r14), %rax
	leaq	_ZN6icu_678Calendar13computeFieldsER10UErrorCode(%rip), %rdx
	leaq	-64(%rbp), %rsi
	movq	%r14, %rdi
	movl	$0, -64(%rbp)
	movq	232(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2508
	call	_ZN6icu_678Calendar13computeFieldsER10UErrorCode.part.0
.L2507:
	movl	220(%r14), %eax
	movl	%ebx, 12(%r14)
	cmpl	$10000, %eax
	je	.L2510
.L2732:
	leal	1(%rax), %edx
.L2511:
	xorl	%r11d, %r11d
	movl	%edx, 220(%r14)
	movl	%eax, 128(%r14)
	movb	$1, 104(%r14)
	movb	$0, 11(%r14)
	movw	%r11w, 8(%r14)
	.p2align 4,,10
	.p2align 3
.L2453:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2736
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2457:
	.cfi_restore_state
	mulsd	.LC1(%rip), %xmm0
.L2571:
	movl	0(%r13), %eax
	testl	%eax, %eax
	jle	.L2737
.L2572:
	xorl	%ebx, %ebx
.L2574:
	movl	0(%r13), %eax
	xorl	%r12d, %r12d
	testl	%eax, %eax
	jg	.L2453
.L2624:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_678Calendar8completeER10UErrorCode
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L2586
	cmpl	%r12d, 96(%r14)
	je	.L2453
	movsd	224(%r14), %xmm3
	movq	%r13, %rsi
	movq	%r14, %rdi
	movsd	%xmm3, -72(%rbp)
	call	_ZN6icu_678Calendar8completeER10UErrorCode
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L2620
	movq	%r13, %rsi
	movq	%r14, %rdi
	movl	76(%r14), %r15d
	call	_ZN6icu_678Calendar8completeER10UErrorCode
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L2588
	addl	72(%r14), %r15d
.L2588:
	cmpl	%ebx, %r15d
	je	.L2453
	movl	%ebx, %edx
	subl	%r15d, %edx
	js	.L2589
	movslq	%edx, %rbx
	movl	%edx, %ecx
	imulq	$833999931, %rbx, %rbx
	sarl	$31, %ecx
	sarq	$56, %rbx
	subl	%ecx, %ebx
	imull	$86400000, %ebx, %ebx
	subl	%ebx, %edx
	movl	%edx, %ebx
.L2590:
	testl	%ebx, %ebx
	jne	.L2738
	movl	252(%r14), %eax
	cmpl	$1, %eax
	je	.L2453
	cmpl	$2, %eax
	jne	.L2453
.L2614:
	movsd	-72(%rbp), %xmm0
	movq	%r13, %rdx
	leaq	-64(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZNK6icu_678Calendar34getImmediatePreviousZoneTransitionEdPdR10UErrorCode
	movl	0(%r13), %edx
	testl	%edx, %edx
	jg	.L2453
	testb	%al, %al
	je	.L2453
	movsd	-64(%rbp), %xmm0
	comisd	.LC5(%rip), %xmm0
	jbe	.L2727
	movq	.LC5(%rip), %rax
	cmpb	$0, 232(%r14)
	movq	%rax, %xmm0
	je	.L2609
.L2617:
	movsd	%xmm0, 224(%r14)
.L2730:
	leaq	20(%r14), %rdi
	movl	%r14d, %edx
	xorl	%eax, %eax
	movl	%r14d, %r8d
	andq	$-8, %rdi
	movl	$16777217, 8(%r14)
	subl	%edi, %edx
	movq	$0, 12(%r14)
	leal	127(%rdx), %ecx
	movq	$0, 119(%r14)
	shrl	$3, %ecx
	rep stosq
	leaq	136(%r14), %rdi
	movq	$0, 128(%r14)
	movq	$0, 212(%r14)
	andq	$-8, %rdi
	subl	%edi, %r8d
	leal	220(%r8), %ecx
	shrl	$3, %ecx
	rep stosq
	jmp	.L2453
	.p2align 4,,10
	.p2align 3
.L2577:
	movq	.LC5(%rip), %rax
	cmpb	$0, 232(%r14)
	movq	%rax, %xmm0
	jne	.L2583
.L2729:
	movl	$1, 0(%r13)
	testb	%r15b, %r15b
	je	.L2453
	movl	0(%r13), %eax
	jmp	.L2586
	.p2align 4,,10
	.p2align 3
.L2464:
	mulsd	.LC9(%rip), %xmm0
	jmp	.L2571
	.p2align 4,,10
	.p2align 3
.L2459:
	movl	0(%r13), %r10d
	testl	%r10d, %r10d
	jle	.L2517
.L2520:
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*184(%rax)
	movl	$10, %ecx
	leaq	.LC4(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	jne	.L2739
.L2518:
	negl	%r15d
.L2458:
	movzbl	232(%r14), %eax
	movb	$1, 232(%r14)
	movl	0(%r13), %edi
	movb	%al, -76(%rbp)
	testl	%edi, %edi
	jle	.L2740
.L2522:
	movl	%r15d, %edx
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	_ZN6icu_678Calendar3setE19UCalendarDateFieldsi
	movq	(%r14), %rax
	movl	$5, %esi
	movq	%r14, %rdi
	leaq	_ZN6icu_678Calendar8pinFieldE19UCalendarDateFieldsR10UErrorCode(%rip), %rdx
	movq	352(%rax), %rcx
	cmpq	%rdx, %rcx
	movq	%r13, %rdx
	jne	.L2523
	call	*176(%rax)
	leaq	_ZNK6icu_678Calendar16getActualMinimumE19UCalendarDateFieldsR10UErrorCode(%rip), %rdx
	movl	%eax, -80(%rbp)
	movq	(%r14), %rax
	movq	168(%rax), %rcx
	cmpq	%rdx, %rcx
	jne	.L2524
	movq	144(%rax), %rdx
	leaq	_ZNK6icu_678Calendar18getGreatestMinimumE19UCalendarDateFields(%rip), %rcx
	cmpq	%rcx, %rdx
	jne	.L2525
	movq	256(%rax), %rcx
	leaq	_ZNK6icu_678Calendar8getLimitE19UCalendarDateFieldsNS0_10ELimitTypeE(%rip), %rdx
	movl	$5, %esi
	movq	%r14, %rdi
	cmpq	%rdx, %rcx
	movl	$1, %edx
	jne	.L2526
	call	*248(%rax)
	movl	%eax, %ebx
.L2527:
	movq	(%r14), %rax
	leaq	_ZNK6icu_678Calendar10getMinimumE19UCalendarDateFields(%rip), %rcx
	movq	112(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L2528
	movq	256(%rax), %rcx
	leaq	_ZNK6icu_678Calendar8getLimitE19UCalendarDateFieldsNS0_10ELimitTypeE(%rip), %rdx
	movl	$5, %esi
	movq	%r14, %rdi
	cmpq	%rdx, %rcx
	movl	$0, %edx
	jne	.L2529
	call	*248(%rax)
	movl	%eax, -72(%rbp)
.L2530:
	cmpl	-72(%rbp), %ebx
	je	.L2531
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*24(%rax)
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L2741
	movb	$1, 232(%rax)
	movl	%ebx, %r12d
	.p2align 4,,10
	.p2align 3
.L2547:
	cmpb	$0, 11(%r15)
	je	.L2533
	movq	(%r15), %rax
	leaq	_ZN6icu_678Calendar13computeFieldsER10UErrorCode(%rip), %rdi
	movl	$0, -64(%rbp)
	leaq	-64(%rbp), %rsi
	movq	232(%rax), %rax
	cmpq	%rdi, %rax
	movq	%r15, %rdi
	jne	.L2534
	call	_ZN6icu_678Calendar13computeFieldsER10UErrorCode.part.0
.L2533:
	movl	220(%r15), %eax
	movl	%r12d, 32(%r15)
	cmpl	$10000, %eax
	je	.L2536
	leal	1(%rax), %esi
.L2537:
	xorl	%edx, %edx
	movb	$1, 109(%r15)
	movb	$0, 11(%r15)
	movw	%dx, 8(%r15)
	movl	0(%r13), %ecx
	movl	%esi, 220(%r15)
	movl	%eax, 148(%r15)
	testl	%ecx, %ecx
	jle	.L2543
.L2545:
	xorl	%eax, %eax
.L2544:
	cmpl	%eax, %r12d
	jne	.L2546
	leal	-1(%r12), %eax
	movl	%r12d, %ebx
	cmpl	%eax, -72(%rbp)
	jg	.L2546
	movl	%eax, %r12d
	jmp	.L2547
	.p2align 4,,10
	.p2align 3
.L2543:
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_678Calendar8completeER10UErrorCode
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L2545
	movl	32(%r15), %eax
	jmp	.L2544
	.p2align 4,,10
	.p2align 3
.L2462:
	mulsd	.LC19(%rip), %xmm0
	jmp	.L2455
	.p2align 4,,10
	.p2align 3
.L2737:
	movq	%r13, %rsi
	movq	%r14, %rdi
	movsd	%xmm0, -72(%rbp)
	call	_ZN6icu_678Calendar8completeER10UErrorCode
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L2572
	movq	%r13, %rsi
	movq	%r14, %rdi
	movl	76(%r14), %ebx
	call	_ZN6icu_678Calendar8completeER10UErrorCode
	movl	0(%r13), %r15d
	testl	%r15d, %r15d
	jg	.L2574
	movq	%r13, %rsi
	movq	%r14, %rdi
	addl	72(%r14), %ebx
	call	_ZN6icu_678Calendar8completeER10UErrorCode
	movl	0(%r13), %r12d
	testl	%r12d, %r12d
	jg	.L2574
	cmpb	$0, 8(%r14)
	movl	96(%r14), %r12d
	movl	$1, %r15d
	movsd	-72(%rbp), %xmm0
	jne	.L2582
.L2576:
	movq	(%r14), %rax
	movsd	%xmm0, -72(%rbp)
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	*224(%rax)
	movl	0(%r13), %r10d
	testl	%r10d, %r10d
	jg	.L2623
	cmpb	$0, 232(%r14)
	movsd	-72(%rbp), %xmm0
	je	.L2742
.L2580:
	movb	$0, 9(%r14)
.L2581:
	movb	$1, 8(%r14)
	movb	$0, 11(%r14)
	movl	0(%r13), %r9d
	testl	%r9d, %r9d
	jg	.L2623
	jmp	.L2582
	.p2align 4,,10
	.p2align 3
.L2609:
	movl	$1, 0(%r13)
	jmp	.L2453
	.p2align 4,,10
	.p2align 3
.L2465:
	movl	0(%r13), %eax
	testl	%eax, %eax
	jle	.L2743
.L2467:
	xorl	%esi, %esi
	movl	%r15d, %edx
	movq	%r14, %rdi
	call	_ZN6icu_678Calendar3setE19UCalendarDateFieldsi
	movq	(%r14), %rax
	movl	$0, %esi
	movq	%r14, %rdi
	leaq	_ZN6icu_678Calendar8pinFieldE19UCalendarDateFieldsR10UErrorCode(%rip), %rdx
	movq	352(%rax), %rcx
	cmpq	%rdx, %rcx
	movq	%r13, %rdx
	jne	.L2469
	call	*176(%rax)
	leaq	_ZNK6icu_678Calendar16getActualMinimumE19UCalendarDateFieldsR10UErrorCode(%rip), %rdx
	movl	%eax, -76(%rbp)
	movq	(%r14), %rax
	movq	168(%rax), %rcx
	cmpq	%rdx, %rcx
	jne	.L2470
	movq	144(%rax), %rdx
	leaq	_ZNK6icu_678Calendar18getGreatestMinimumE19UCalendarDateFields(%rip), %rcx
	cmpq	%rcx, %rdx
	jne	.L2471
	movq	256(%rax), %rcx
	leaq	_ZNK6icu_678Calendar8getLimitE19UCalendarDateFieldsNS0_10ELimitTypeE(%rip), %rdx
	movl	$0, %esi
	movq	%r14, %rdi
	cmpq	%rdx, %rcx
	movl	$1, %edx
	jne	.L2472
	call	*248(%rax)
	movl	%eax, %ebx
.L2473:
	movq	(%r14), %rax
	leaq	_ZNK6icu_678Calendar10getMinimumE19UCalendarDateFields(%rip), %rcx
	movq	112(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L2474
	movq	256(%rax), %rcx
	leaq	_ZNK6icu_678Calendar8getLimitE19UCalendarDateFieldsNS0_10ELimitTypeE(%rip), %rdx
	movl	$0, %esi
	movq	%r14, %rdi
	cmpq	%rdx, %rcx
	movl	$0, %edx
	jne	.L2475
	call	*248(%rax)
	movl	%eax, -72(%rbp)
.L2476:
	cmpl	-72(%rbp), %ebx
	je	.L2477
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*24(%rax)
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L2744
	movb	$1, 232(%rax)
	movl	%ebx, %r12d
	.p2align 4,,10
	.p2align 3
.L2493:
	cmpb	$0, 11(%r15)
	je	.L2479
	movq	(%r15), %rax
	leaq	_ZN6icu_678Calendar13computeFieldsER10UErrorCode(%rip), %rdi
	movl	$0, -64(%rbp)
	leaq	-64(%rbp), %rsi
	movq	232(%rax), %rax
	cmpq	%rdi, %rax
	movq	%r15, %rdi
	jne	.L2480
	call	_ZN6icu_678Calendar13computeFieldsER10UErrorCode.part.0
.L2479:
	movl	220(%r15), %eax
	movl	%r12d, 12(%r15)
	cmpl	$10000, %eax
	je	.L2482
	leal	1(%rax), %esi
.L2483:
	movl	%eax, 128(%r15)
	xorl	%eax, %eax
	movb	$1, 104(%r15)
	movb	$0, 11(%r15)
	movw	%ax, 8(%r15)
	movl	0(%r13), %eax
	movl	%esi, 220(%r15)
	testl	%eax, %eax
	jle	.L2489
.L2491:
	xorl	%eax, %eax
.L2490:
	cmpl	%eax, %r12d
	jne	.L2492
	leal	-1(%r12), %eax
	movl	%r12d, %ebx
	cmpl	%eax, -72(%rbp)
	jg	.L2492
	movl	%eax, %r12d
	jmp	.L2493
	.p2align 4,,10
	.p2align 3
.L2463:
	mulsd	.LC18(%rip), %xmm0
	jmp	.L2571
	.p2align 4,,10
	.p2align 3
.L2460:
	mulsd	.LC11(%rip), %xmm0
	jmp	.L2455
	.p2align 4,,10
	.p2align 3
.L2741:
	movl	$7, 0(%r13)
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L2531:
	movl	32(%r14), %eax
	cmpl	%eax, -80(%rbp)
	jl	.L2745
	cmpl	%eax, %ebx
	jg	.L2746
.L2559:
	cmpb	$0, -76(%rbp)
	jne	.L2453
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_678Calendar8completeER10UErrorCode
	movb	$0, 232(%r14)
	jmp	.L2453
	.p2align 4,,10
	.p2align 3
.L2536:
	movl	$1, 220(%r15)
	movl	$1, %r10d
	.p2align 4,,10
	.p2align 3
.L2542:
	movl	$10000, %esi
	xorl	%eax, %eax
	movl	$-1, %edx
	.p2align 4,,10
	.p2align 3
.L2540:
	movl	128(%r15,%rax,4), %r11d
	cmpl	%r10d, %r11d
	jle	.L2538
	cmpl	%esi, %r11d
	cmovl	%r11d, %esi
	cmovl	%eax, %edx
.L2538:
	addq	$1, %rax
	cmpq	$23, %rax
	jne	.L2540
	leal	1(%r10), %eax
	leal	2(%r10), %esi
	cmpl	$-1, %edx
	je	.L2537
	movslq	%edx, %rdx
	movl	%eax, 220(%r15)
	movl	%eax, 128(%r15,%rdx,4)
	cmpl	$24, %eax
	je	.L2747
	movl	%eax, %r10d
	jmp	.L2542
	.p2align 4,,10
	.p2align 3
.L2546:
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*8(%rax)
	movl	0(%r13), %eax
	testl	%eax, %eax
	movl	$0, %eax
	cmovg	%eax, %ebx
	jmp	.L2531
	.p2align 4,,10
	.p2align 3
.L2740:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_678Calendar8completeER10UErrorCode
	movl	0(%r13), %esi
	testl	%esi, %esi
	jg	.L2522
	movslq	%r12d, %rax
	addl	12(%r14,%rax,4), %r15d
	jmp	.L2522
	.p2align 4,,10
	.p2align 3
.L2534:
	call	*%rax
	jmp	.L2533
	.p2align 4,,10
	.p2align 3
.L2747:
	movl	%esi, %eax
	leal	3(%r10), %esi
	jmp	.L2537
	.p2align 4,,10
	.p2align 3
.L2589:
	movl	%r15d, %ecx
	subl	%ebx, %ecx
	movslq	%ecx, %rbx
	movl	%ecx, %edx
	imulq	$833999931, %rbx, %rbx
	sarl	$31, %edx
	sarq	$56, %rbx
	subl	%edx, %ebx
	imull	$86400000, %ebx, %ebx
	subl	%ecx, %ebx
	jmp	.L2590
	.p2align 4,,10
	.p2align 3
.L2489:
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_678Calendar8completeER10UErrorCode
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L2491
	movl	12(%r15), %eax
	jmp	.L2490
	.p2align 4,,10
	.p2align 3
.L2739:
	movl	$4, %ecx
	leaq	.LC7(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L2518
	movq	%rax, %rsi
	movl	$7, %ecx
	leaq	.LC8(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L2458
	jmp	.L2518
	.p2align 4,,10
	.p2align 3
.L2742:
	cmpb	$0, 10(%r14)
	jne	.L2581
	jmp	.L2580
	.p2align 4,,10
	.p2align 3
.L2471:
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	*%rdx
	movl	%eax, %ebx
	jmp	.L2473
	.p2align 4,,10
	.p2align 3
.L2482:
	movl	$1, 220(%r15)
	movl	$1, %edi
	.p2align 4,,10
	.p2align 3
.L2488:
	movl	$10000, %r10d
	xorl	%eax, %eax
	movl	$-1, %edx
	.p2align 4,,10
	.p2align 3
.L2486:
	movl	128(%r15,%rax,4), %esi
	cmpl	%esi, %edi
	jge	.L2484
	cmpl	%r10d, %esi
	cmovl	%esi, %r10d
	cmovl	%eax, %edx
.L2484:
	addq	$1, %rax
	cmpq	$23, %rax
	jne	.L2486
	leal	1(%rdi), %eax
	leal	2(%rdi), %esi
	cmpl	$-1, %edx
	je	.L2483
	movslq	%edx, %rdx
	movl	%eax, 220(%r15)
	movl	%eax, 128(%r15,%rdx,4)
	cmpl	$24, %eax
	je	.L2748
	movl	%eax, %edi
	jmp	.L2488
	.p2align 4,,10
	.p2align 3
.L2745:
	cmpb	$0, 11(%r14)
	je	.L2549
	movq	(%r14), %rax
	leaq	_ZN6icu_678Calendar13computeFieldsER10UErrorCode(%rip), %rdx
	leaq	-64(%rbp), %rsi
	movq	%r14, %rdi
	movl	$0, -64(%rbp)
	movq	232(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2550
	call	_ZN6icu_678Calendar13computeFieldsER10UErrorCode.part.0
.L2549:
	movl	-80(%rbp), %eax
	movl	%eax, 32(%r14)
	movl	220(%r14), %eax
	cmpl	$10000, %eax
	je	.L2552
.L2734:
	leal	1(%rax), %edx
.L2564:
	movl	%eax, 148(%r14)
	xorl	%eax, %eax
	movl	%edx, 220(%r14)
	movb	$1, 109(%r14)
	movb	$0, 11(%r14)
	movw	%ax, 8(%r14)
	jmp	.L2559
	.p2align 4,,10
	.p2align 3
.L2738:
	testl	%eax, %eax
	jg	.L2598
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%ebx, %xmm0
	addsd	-72(%rbp), %xmm0
	comisd	.LC5(%rip), %xmm0
	jbe	.L2724
	movq	.LC5(%rip), %rax
	cmpb	$0, 232(%r14)
	movq	%rax, %xmm0
	je	.L2597
.L2595:
	leaq	20(%r14), %rdi
	movl	%r14d, %edx
	xorl	%eax, %eax
	movl	$16777217, 8(%r14)
	andq	$-8, %rdi
	movq	$0, 12(%r14)
	subl	%edi, %edx
	movq	$0, 119(%r14)
	movsd	%xmm0, 224(%r14)
	leal	127(%rdx), %ecx
	movl	%r14d, %edx
	shrl	$3, %ecx
	rep stosq
	leaq	136(%r14), %rdi
	movq	$0, 128(%r14)
	movq	$0, 212(%r14)
	andq	$-8, %rdi
	subl	%edi, %edx
	leal	220(%rdx), %ecx
	shrl	$3, %ecx
	rep stosq
	movl	0(%r13), %r8d
	testl	%r8d, %r8d
	jg	.L2598
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN6icu_678Calendar8completeER10UErrorCode
	movl	0(%r13), %edi
	testl	%edi, %edi
	jg	.L2598
	movl	96(%r14), %eax
.L2622:
	cmpl	%r12d, %eax
	je	.L2453
	movl	252(%r14), %eax
	cmpl	$1, %eax
	je	.L2599
	cmpl	$2, %eax
	je	.L2600
	testl	%eax, %eax
	jne	.L2453
	testl	%ebx, %ebx
	jns	.L2453
	movl	0(%r13), %ecx
	testl	%ecx, %ecx
	jg	.L2453
	movsd	-72(%rbp), %xmm7
	comisd	.LC5(%rip), %xmm7
	jbe	.L2726
	movq	.LC5(%rip), %rax
	cmpb	$0, 232(%r14)
	movq	%rax, -72(%rbp)
	je	.L2609
.L2612:
	movsd	-72(%rbp), %xmm6
	movsd	%xmm6, 224(%r14)
	jmp	.L2730
	.p2align 4,,10
	.p2align 3
.L2492:
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*8(%rax)
	movl	0(%r13), %r12d
	movl	$0, %eax
	testl	%r12d, %r12d
	cmovg	%eax, %ebx
	jmp	.L2477
	.p2align 4,,10
	.p2align 3
.L2517:
	movq	%r13, %rsi
	call	_ZN6icu_678Calendar8completeER10UErrorCode
	movl	0(%r13), %r9d
	testl	%r9d, %r9d
	jg	.L2520
	movl	12(%r14), %r8d
	testl	%r8d, %r8d
	je	.L2520
	jmp	.L2458
	.p2align 4,,10
	.p2align 3
.L2743:
	movq	%r13, %rsi
	call	_ZN6icu_678Calendar8completeER10UErrorCode
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L2467
	addl	12(%r14), %r15d
	jmp	.L2467
	.p2align 4,,10
	.p2align 3
.L2523:
	call	*%rcx
	jmp	.L2559
	.p2align 4,,10
	.p2align 3
.L2480:
	call	*%rax
	jmp	.L2479
	.p2align 4,,10
	.p2align 3
.L2746:
	cmpb	$0, 11(%r14)
	je	.L2560
	movq	(%r14), %rax
	leaq	_ZN6icu_678Calendar13computeFieldsER10UErrorCode(%rip), %rdx
	leaq	-64(%rbp), %rsi
	movq	%r14, %rdi
	movl	$0, -64(%rbp)
	movq	232(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2561
	call	_ZN6icu_678Calendar13computeFieldsER10UErrorCode.part.0
.L2560:
	movl	220(%r14), %eax
	movl	%ebx, 32(%r14)
	cmpl	$10000, %eax
	jne	.L2734
	movl	$1, 220(%r14)
	movl	$1, %edi
	.p2align 4,,10
	.p2align 3
.L2569:
	movl	$10000, %ecx
	xorl	%eax, %eax
	movl	$-1, %esi
	.p2align 4,,10
	.p2align 3
.L2567:
	movl	128(%r14,%rax,4), %edx
	cmpl	%edi, %edx
	jle	.L2565
	cmpl	%ecx, %edx
	cmovl	%edx, %ecx
	cmovl	%eax, %esi
.L2565:
	addq	$1, %rax
	cmpq	$23, %rax
	jne	.L2567
	leal	1(%rdi), %eax
	leal	2(%rdi), %edx
	cmpl	$-1, %esi
	je	.L2564
	movslq	%esi, %rsi
	movl	%eax, 220(%r14)
	movl	%eax, 128(%r14,%rsi,4)
	cmpl	$24, %eax
	je	.L2733
	movl	%eax, %edi
	jmp	.L2569
	.p2align 4,,10
	.p2align 3
.L2748:
	movl	%esi, %eax
	leal	3(%rdi), %esi
	jmp	.L2483
	.p2align 4,,10
	.p2align 3
.L2524:
	movq	%r13, %rdx
	movl	$5, %esi
	movq	%r14, %rdi
	call	*%rcx
	movl	%eax, %ebx
	jmp	.L2531
.L2597:
	movl	$1, 0(%r13)
	.p2align 4,,10
	.p2align 3
.L2598:
	xorl	%eax, %eax
	jmp	.L2622
	.p2align 4,,10
	.p2align 3
.L2735:
	cmpb	$0, 11(%r14)
	je	.L2495
	movq	(%r14), %rax
	leaq	_ZN6icu_678Calendar13computeFieldsER10UErrorCode(%rip), %rdx
	leaq	-64(%rbp), %rsi
	movq	%r14, %rdi
	movl	$0, -64(%rbp)
	movq	232(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2496
	call	_ZN6icu_678Calendar13computeFieldsER10UErrorCode.part.0
.L2495:
	movl	-76(%rbp), %eax
	movl	%eax, 12(%r14)
	movl	220(%r14), %eax
	cmpl	$10000, %eax
	jne	.L2732
	movl	$1, 220(%r14)
	movl	$1, %edi
	.p2align 4,,10
	.p2align 3
.L2504:
	movl	$10000, %ecx
	xorl	%eax, %eax
	movl	$-1, %esi
	.p2align 4,,10
	.p2align 3
.L2502:
	movl	128(%r14,%rax,4), %edx
	cmpl	%edx, %ecx
	jle	.L2500
	cmpl	%edx, %edi
	cmovl	%edx, %ecx
	cmovl	%eax, %esi
.L2500:
	addq	$1, %rax
	cmpq	$23, %rax
	jne	.L2502
	leal	1(%rdi), %eax
	leal	2(%rdi), %edx
	cmpl	$-1, %esi
	je	.L2511
	movslq	%esi, %rsi
	movl	%eax, 220(%r14)
	movl	%eax, 128(%r14,%rsi,4)
	cmpl	$24, %eax
	je	.L2731
	movl	%eax, %edi
	jmp	.L2504
	.p2align 4,,10
	.p2align 3
.L2525:
	movl	$5, %esi
	movq	%r14, %rdi
	call	*%rdx
	movl	%eax, %ebx
	jmp	.L2527
	.p2align 4,,10
	.p2align 3
.L2528:
	movl	$5, %esi
	movq	%r14, %rdi
	call	*%rdx
	movl	%eax, -72(%rbp)
	jmp	.L2530
	.p2align 4,,10
	.p2align 3
.L2529:
	call	*%rcx
	movl	%eax, -72(%rbp)
	jmp	.L2530
	.p2align 4,,10
	.p2align 3
.L2526:
	call	*%rcx
	movl	%eax, %ebx
	jmp	.L2527
	.p2align 4,,10
	.p2align 3
.L2469:
	call	*%rcx
	jmp	.L2453
.L2600:
	testl	%ebx, %ebx
	jle	.L2614
	movsd	224(%r14), %xmm5
	movsd	%xmm5, -72(%rbp)
	jmp	.L2614
	.p2align 4,,10
	.p2align 3
.L2470:
	movq	%r13, %rdx
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	*%rcx
	movl	%eax, %ebx
	jmp	.L2477
	.p2align 4,,10
	.p2align 3
.L2724:
	movsd	.LC6(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jbe	.L2595
	cmpb	$0, 232(%r14)
	je	.L2597
	movapd	%xmm1, %xmm0
	jmp	.L2595
	.p2align 4,,10
	.p2align 3
.L2474:
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	*%rdx
	movl	%eax, -72(%rbp)
	jmp	.L2476
.L2599:
	testl	%ebx, %ebx
	jle	.L2453
	movl	0(%r13), %esi
	testl	%esi, %esi
	jg	.L2453
	movsd	-72(%rbp), %xmm6
	comisd	.LC5(%rip), %xmm6
	jbe	.L2725
	movq	.LC5(%rip), %rax
	cmpb	$0, 232(%r14)
	movq	%rax, -72(%rbp)
	je	.L2609
.L2607:
	movsd	-72(%rbp), %xmm7
	movsd	%xmm7, 224(%r14)
	jmp	.L2730
.L2552:
	movl	$1, 220(%r14)
	movl	$1, %edi
	.p2align 4,,10
	.p2align 3
.L2558:
	movl	$10000, %ecx
	xorl	%eax, %eax
	movl	$-1, %esi
	.p2align 4,,10
	.p2align 3
.L2556:
	movl	128(%r14,%rax,4), %edx
	cmpl	%edx, %edi
	jge	.L2554
	cmpl	%ecx, %edx
	cmovl	%edx, %ecx
	cmovl	%eax, %esi
.L2554:
	addq	$1, %rax
	cmpq	$23, %rax
	jne	.L2556
	leal	1(%rdi), %eax
	leal	2(%rdi), %edx
	cmpl	$-1, %esi
	je	.L2564
	movslq	%esi, %rsi
	movl	%eax, 220(%r14)
	movl	%eax, 128(%r14,%rsi,4)
	cmpl	$24, %eax
	je	.L2733
	movl	%eax, %edi
	jmp	.L2558
.L2475:
	call	*%rcx
	movl	%eax, -72(%rbp)
	jmp	.L2476
.L2472:
	call	*%rcx
	movl	%eax, %ebx
	jmp	.L2473
.L2733:
	movl	%edx, %eax
	leal	3(%rdi), %edx
	jmp	.L2564
.L2550:
	call	*%rax
	jmp	.L2549
.L2731:
	movl	%edx, %eax
	leal	3(%rdi), %edx
	jmp	.L2511
.L2510:
	movl	$1, 220(%r14)
	movl	$1, %edi
	.p2align 4,,10
	.p2align 3
.L2516:
	movl	$10000, %ecx
	xorl	%eax, %eax
	movl	$-1, %esi
	.p2align 4,,10
	.p2align 3
.L2514:
	movl	128(%r14,%rax,4), %edx
	cmpl	%edx, %ecx
	jle	.L2512
	cmpl	%edx, %edi
	cmovl	%edx, %ecx
	cmovl	%eax, %esi
.L2512:
	addq	$1, %rax
	cmpq	$23, %rax
	jne	.L2514
	leal	1(%rdi), %eax
	leal	2(%rdi), %edx
	cmpl	$-1, %esi
	je	.L2511
	movslq	%esi, %rsi
	movl	%eax, 220(%r14)
	movl	%eax, 128(%r14,%rsi,4)
	cmpl	$24, %eax
	je	.L2731
	movl	%eax, %edi
	jmp	.L2516
.L2561:
	call	*%rax
	jmp	.L2560
.L2725:
	movsd	.LC6(%rip), %xmm0
	comisd	-72(%rbp), %xmm0
	jbe	.L2607
	cmpb	$0, 232(%r14)
	je	.L2609
	movsd	%xmm0, -72(%rbp)
	jmp	.L2607
.L2496:
	call	*%rax
	jmp	.L2495
.L2726:
	movsd	.LC6(%rip), %xmm0
	comisd	-72(%rbp), %xmm0
	jbe	.L2612
	cmpb	$0, 232(%r14)
	je	.L2609
	movsd	%xmm0, -72(%rbp)
	jmp	.L2612
.L2508:
	call	*%rax
	jmp	.L2507
.L2727:
	movsd	.LC6(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jbe	.L2617
	cmpb	$0, 232(%r14)
	je	.L2609
	movapd	%xmm1, %xmm0
	jmp	.L2617
.L2736:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4368:
	.size	_ZN6icu_678Calendar3addE19UCalendarDateFieldsiR10UErrorCode.part.0, .-_ZN6icu_678Calendar3addE19UCalendarDateFieldsiR10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Calendar3addE19UCalendarDateFieldsiR10UErrorCode
	.type	_ZN6icu_678Calendar3addE19UCalendarDateFieldsiR10UErrorCode, @function
_ZN6icu_678Calendar3addE19UCalendarDateFieldsiR10UErrorCode:
.LFB3287:
	.cfi_startproc
	endbr64
	testl	%edx, %edx
	je	.L2749
	jmp	_ZN6icu_678Calendar3addE19UCalendarDateFieldsiR10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L2749:
	ret
	.cfi_endproc
.LFE3287:
	.size	_ZN6icu_678Calendar3addE19UCalendarDateFieldsiR10UErrorCode, .-_ZN6icu_678Calendar3addE19UCalendarDateFieldsiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Calendar3addENS0_11EDateFieldsEiR10UErrorCode
	.type	_ZN6icu_678Calendar3addENS0_11EDateFieldsEiR10UErrorCode, @function
_ZN6icu_678Calendar3addENS0_11EDateFieldsEiR10UErrorCode:
.LFB3286:
	.cfi_startproc
	endbr64
	testl	%edx, %edx
	je	.L2751
	jmp	_ZN6icu_678Calendar3addE19UCalendarDateFieldsiR10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L2751:
	ret
	.cfi_endproc
.LFE3286:
	.size	_ZN6icu_678Calendar3addENS0_11EDateFieldsEiR10UErrorCode, .-_ZN6icu_678Calendar3addENS0_11EDateFieldsEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678Calendar15getActualHelperE19UCalendarDateFieldsiiR10UErrorCode
	.type	_ZNK6icu_678Calendar15getActualHelperE19UCalendarDateFieldsiiR10UErrorCode, @function
_ZNK6icu_678Calendar15getActualHelperE19UCalendarDateFieldsiiR10UErrorCode:
.LFB3340:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%ecx, -52(%rbp)
	movl	%edx, -64(%rbp)
	cmpl	%ecx, %edx
	je	.L2753
	movq	%r8, %rbx
	setge	%al
	setl	%r13b
	movl	(%r8), %r8d
	movzbl	%al, %eax
	movzbl	%r13b, %r13d
	setl	-65(%rbp)
	movl	%eax, -56(%rbp)
	leal	-1(%r13,%r13), %r13d
	testl	%r8d, %r8d
	jg	.L2753
	movq	(%rdi), %rax
	movl	%esi, %r12d
	call	*24(%rax)
	movl	-64(%rbp), %r9d
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L2774
	movq	%rax, %rdi
	movq	%rbx, %rsi
	movl	%r9d, -64(%rbp)
	call	_ZN6icu_678Calendar8completeER10UErrorCode
	movq	(%r14), %rax
	movl	-56(%rbp), %edx
	movq	%rbx, %rcx
	movl	%r12d, %esi
	movq	%r14, %rdi
	movb	$1, 232(%r14)
	call	*240(%rax)
	movl	-64(%rbp), %r9d
	movl	%r12d, %esi
	movq	%r14, %rdi
	movl	%r9d, %edx
	call	_ZN6icu_678Calendar3setE19UCalendarDateFieldsi
	movl	(%rbx), %esi
	movl	-64(%rbp), %r9d
	testl	%esi, %esi
	jle	.L2775
.L2764:
	movl	%r9d, %r15d
.L2760:
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*8(%rax)
.L2753:
	addq	$40, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2774:
	.cfi_restore_state
	movl	$7, (%rbx)
	jmp	.L2753
	.p2align 4,,10
	.p2align 3
.L2775:
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_678Calendar8completeER10UErrorCode
	movl	(%rbx), %ecx
	movl	-64(%rbp), %r9d
	testl	%ecx, %ecx
	jg	.L2764
	movslq	%r12d, %rax
	movq	%rax, -64(%rbp)
	cmpl	12(%r14,%rax,4), %r9d
	je	.L2759
	cmpl	$4, %r12d
	je	.L2759
	cmpb	$0, -65(%rbp)
	jne	.L2764
.L2759:
	movl	%r9d, %r15d
	jmp	.L2763
	.p2align 4,,10
	.p2align 3
.L2777:
	call	_ZN6icu_678Calendar3addE19UCalendarDateFieldsiR10UErrorCode.part.0
.L2762:
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L2760
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_678Calendar8completeER10UErrorCode
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L2760
	movq	-64(%rbp), %rdx
	leal	(%r15,%r13), %eax
	cmpl	12(%r14,%rdx,4), %eax
	jne	.L2760
	cmpl	%eax, -52(%rbp)
	je	.L2776
	movl	%eax, %r15d
.L2763:
	movq	(%r14), %rax
	leaq	_ZN6icu_678Calendar3addE19UCalendarDateFieldsiR10UErrorCode(%rip), %rdx
	movq	%rbx, %rcx
	movl	%r12d, %esi
	movq	%r14, %rdi
	movq	56(%rax), %rax
	cmpq	%rdx, %rax
	movl	%r13d, %edx
	je	.L2777
	call	*%rax
	jmp	.L2762
	.p2align 4,,10
	.p2align 3
.L2776:
	movl	-52(%rbp), %r15d
	jmp	.L2760
	.cfi_endproc
.LFE3340:
	.size	_ZNK6icu_678Calendar15getActualHelperE19UCalendarDateFieldsiiR10UErrorCode, .-_ZNK6icu_678Calendar15getActualHelperE19UCalendarDateFieldsiiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678Calendar16getActualMaximumE19UCalendarDateFieldsR10UErrorCode
	.type	_ZNK6icu_678Calendar16getActualMaximumE19UCalendarDateFieldsR10UErrorCode, @function
_ZNK6icu_678Calendar16getActualMaximumE19UCalendarDateFieldsR10UErrorCode:
.LFB3338:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leal	-5(%rsi), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$24, %rsp
	cmpl	$16, %eax
	ja	.L2779
	leaq	.L2781(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L2781:
	.long	.L2783-.L2781
	.long	.L2782-.L2781
	.long	.L2780-.L2781
	.long	.L2779-.L2781
	.long	.L2780-.L2781
	.long	.L2780-.L2781
	.long	.L2780-.L2781
	.long	.L2780-.L2781
	.long	.L2780-.L2781
	.long	.L2780-.L2781
	.long	.L2780-.L2781
	.long	.L2780-.L2781
	.long	.L2779-.L2781
	.long	.L2780-.L2781
	.long	.L2779-.L2781
	.long	.L2780-.L2781
	.long	.L2780-.L2781
	.text
	.p2align 4,,10
	.p2align 3
.L2780:
	movq	(%rdi), %rax
	leaq	_ZNK6icu_678Calendar10getMaximumE19UCalendarDateFields(%rip), %rcx
	movq	128(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L2796
	movq	256(%rax), %rax
	addq	$24, %rsp
	movl	$3, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L2779:
	.cfi_restore_state
	movq	(%r12), %rax
	leaq	_ZNK6icu_678Calendar10getMaximumE19UCalendarDateFields(%rip), %rcx
	movq	128(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L2797
	movl	$3, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	*256(%rax)
	movl	%eax, %r14d
.L2798:
	movq	(%r12), %rax
	leaq	_ZNK6icu_678Calendar15getLeastMaximumE19UCalendarDateFields(%rip), %rcx
	movq	160(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L2799
	movq	256(%rax), %rcx
	leaq	_ZNK6icu_678Calendar8getLimitE19UCalendarDateFieldsNS0_10ELimitTypeE(%rip), %rdx
	cmpq	%rdx, %rcx
	jne	.L2800
	cmpl	$7, %r13d
	ja	.L2801
	cmpl	$4, %r13d
	jne	.L2803
	movl	$2, %edx
	movl	$5, %esi
	movq	%r12, %rdi
	movzbl	260(%r12), %r15d
	call	*248(%rax)
	subl	%r15d, %eax
	addl	$7, %eax
	movslq	%eax, %rdx
	imulq	$-1840700269, %rdx, %rdx
	shrq	$32, %rdx
	addl	%eax, %edx
	sarl	$31, %eax
	sarl	$2, %edx
	subl	%eax, %edx
.L2806:
	addq	$24, %rsp
	movq	%rbx, %r8
	movl	%r14d, %ecx
	movl	%r13d, %esi
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_678Calendar15getActualHelperE19UCalendarDateFieldsiiR10UErrorCode
	.p2align 4,,10
	.p2align 3
.L2782:
	.cfi_restore_state
	movl	(%rbx), %ecx
	xorl	%r13d, %r13d
	testl	%ecx, %ecx
	jg	.L2778
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L2790
	movb	$1, 232(%rax)
	movq	(%rax), %rax
	xorl	%edx, %edx
	movq	%rbx, %rcx
	movl	$6, %esi
	movq	%r15, %rdi
	call	*240(%rax)
	movq	(%r12), %rax
	movl	(%rbx), %edx
	movq	280(%rax), %r13
	testl	%edx, %edx
	jle	.L2791
.L2793:
	xorl	%r14d, %r14d
.L2792:
	leaq	_ZNK6icu_678Calendar19handleGetYearLengthEi(%rip), %rax
	cmpq	%rax, %r13
	jne	.L2794
	movq	(%r12), %rax
	leal	1(%r14), %esi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	*264(%rax)
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	%r14d, %esi
	movl	%eax, %r13d
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*264(%rax)
	subl	%eax, %r13d
.L2795:
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*8(%rax)
.L2778:
	addq	$24, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2783:
	.cfi_restore_state
	movl	(%rbx), %r9d
	xorl	%r13d, %r13d
	testl	%r9d, %r9d
	jg	.L2778
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2790
	movb	$1, 232(%rax)
	movq	(%rax), %rax
	movq	%rbx, %rcx
	xorl	%edx, %edx
	movl	$5, %esi
	movq	%r14, %rdi
	call	*240(%rax)
	movq	(%r12), %rax
	movl	(%rbx), %r8d
	movq	272(%rax), %r13
	testl	%r8d, %r8d
	jle	.L2814
.L2786:
	xorl	%r8d, %r8d
	xorl	%r15d, %r15d
.L2787:
	leaq	_ZNK6icu_678Calendar20handleGetMonthLengthEii(%rip), %rax
	cmpq	%rax, %r13
	jne	.L2788
	movq	(%r12), %rax
	leal	1(%r8), %edx
	movl	%r8d, -52(%rbp)
	movl	%r15d, %esi
	movl	$1, %ecx
	movq	%r12, %rdi
	call	*264(%rax)
	movl	-52(%rbp), %r8d
	movl	%r15d, %esi
	movq	%r12, %rdi
	movl	%eax, %r13d
	movq	(%r12), %rax
	movl	$1, %ecx
	movl	%r8d, %edx
	call	*264(%rax)
	subl	%eax, %r13d
.L2789:
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*8(%rax)
	jmp	.L2778
	.p2align 4,,10
	.p2align 3
.L2801:
	cmpl	$16, %r13d
	ja	.L2804
	cmpl	$8, %r13d
	jne	.L2805
.L2803:
	movl	$2, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	*248(%rax)
	movl	%eax, %edx
	jmp	.L2806
	.p2align 4,,10
	.p2align 3
.L2804:
	leal	-20(%r13), %edx
	cmpl	$2, %edx
	ja	.L2803
.L2805:
	movslq	%r13d, %rax
	leaq	_ZN6icu_67L15kCalendarLimitsE(%rip), %rdx
	salq	$4, %rax
	movl	8(%rdx,%rax), %edx
	jmp	.L2806
	.p2align 4,,10
	.p2align 3
.L2790:
	movl	$7, (%rbx)
	xorl	%r13d, %r13d
	jmp	.L2778
	.p2align 4,,10
	.p2align 3
.L2796:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rdx
	.p2align 4,,10
	.p2align 3
.L2799:
	.cfi_restore_state
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	*%rdx
	movl	%eax, %edx
	jmp	.L2806
	.p2align 4,,10
	.p2align 3
.L2797:
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	*%rdx
	movl	%eax, %r14d
	jmp	.L2798
	.p2align 4,,10
	.p2align 3
.L2800:
	movl	$2, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	*%rcx
	movl	%eax, %edx
	jmp	.L2806
	.p2align 4,,10
	.p2align 3
.L2791:
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_678Calendar8completeER10UErrorCode
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L2793
	movl	88(%r15), %r14d
	jmp	.L2792
	.p2align 4,,10
	.p2align 3
.L2814:
	movq	%r14, %rdi
	movq	%rbx, %rsi
	call	_ZN6icu_678Calendar8completeER10UErrorCode
	movl	(%rbx), %edi
	testl	%edi, %edi
	jg	.L2786
	movl	20(%r14), %r8d
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movl	%r8d, -52(%rbp)
	call	_ZN6icu_678Calendar8completeER10UErrorCode
	movl	(%rbx), %esi
	movl	-52(%rbp), %r8d
	testl	%esi, %esi
	jg	.L2808
	movl	88(%r14), %r15d
	jmp	.L2787
	.p2align 4,,10
	.p2align 3
.L2794:
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	*%r13
	movl	%eax, %r13d
	jmp	.L2795
	.p2align 4,,10
	.p2align 3
.L2788:
	movl	%r8d, %edx
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	*%r13
	movl	%eax, %r13d
	jmp	.L2789
.L2808:
	xorl	%r15d, %r15d
	jmp	.L2787
	.cfi_endproc
.LFE3338:
	.size	_ZNK6icu_678Calendar16getActualMaximumE19UCalendarDateFieldsR10UErrorCode, .-_ZNK6icu_678Calendar16getActualMaximumE19UCalendarDateFieldsR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Calendar8pinFieldE19UCalendarDateFieldsR10UErrorCode
	.type	_ZN6icu_678Calendar8pinFieldE19UCalendarDateFieldsR10UErrorCode, @function
_ZN6icu_678Calendar8pinFieldE19UCalendarDateFieldsR10UErrorCode:
.LFB3276:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	leaq	_ZNK6icu_678Calendar16getActualMaximumE19UCalendarDateFieldsR10UErrorCode(%rip), %rdx
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rcx
	movq	176(%rcx), %rax
	cmpq	%rdx, %rax
	jne	.L2816
	leal	-5(%rsi), %eax
	cmpl	$16, %eax
	ja	.L2817
	leaq	.L2819(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L2819:
	.long	.L2821-.L2819
	.long	.L2820-.L2819
	.long	.L2818-.L2819
	.long	.L2817-.L2819
	.long	.L2818-.L2819
	.long	.L2818-.L2819
	.long	.L2818-.L2819
	.long	.L2818-.L2819
	.long	.L2818-.L2819
	.long	.L2818-.L2819
	.long	.L2818-.L2819
	.long	.L2818-.L2819
	.long	.L2817-.L2819
	.long	.L2818-.L2819
	.long	.L2817-.L2819
	.long	.L2818-.L2819
	.long	.L2818-.L2819
	.text
.L2818:
	movq	128(%rcx), %rax
	leaq	_ZNK6icu_678Calendar10getMaximumE19UCalendarDateFields(%rip), %rdx
	cmpq	%rdx, %rax
	jne	.L2834
	movl	$3, %edx
	movslq	%r13d, %rbx
	call	*256(%rcx)
	movq	(%r12), %rcx
	movl	%eax, %r15d
	jmp	.L2822
.L2820:
	movl	(%r14), %esi
	testl	%esi, %esi
	jg	.L2849
	call	*24(%rcx)
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L2851
	movb	$1, 232(%rax)
	movq	(%rax), %rax
	xorl	%edx, %edx
	movq	%r14, %rcx
	movl	$6, %esi
	movq	%rbx, %rdi
	call	*240(%rax)
	movq	(%r12), %rax
	movl	(%r14), %edx
	movq	280(%rax), %r15
	testl	%edx, %edx
	jle	.L2829
.L2831:
	xorl	%r8d, %r8d
.L2830:
	leaq	_ZNK6icu_678Calendar19handleGetYearLengthEi(%rip), %rax
	cmpq	%rax, %r15
	jne	.L2832
	movq	(%r12), %rax
	leal	1(%r8), %esi
	movl	%r8d, -52(%rbp)
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	*264(%rax)
	movl	-52(%rbp), %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	%eax, %r15d
	movq	(%r12), %rax
	movq	%r12, %rdi
	movl	%r8d, %esi
	call	*264(%rax)
	subl	%eax, %r15d
	jmp	.L2833
.L2821:
	movl	(%r14), %r10d
	testl	%r10d, %r10d
	jg	.L2849
	call	*24(%rcx)
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L2851
	movb	$1, 232(%rax)
	movq	(%rax), %rax
	movq	%r14, %rcx
	xorl	%edx, %edx
	movl	$5, %esi
	movq	%rbx, %rdi
	call	*240(%rax)
	movq	(%r12), %rax
	movl	(%r14), %r9d
	movq	272(%rax), %r15
	testl	%r9d, %r9d
	jle	.L2853
.L2824:
	xorl	%r8d, %r8d
	xorl	%esi, %esi
.L2825:
	leaq	_ZNK6icu_678Calendar20handleGetMonthLengthEii(%rip), %rax
	cmpq	%rax, %r15
	jne	.L2826
	movq	(%r12), %rax
	leal	1(%r8), %edx
	movl	%r8d, -56(%rbp)
	movq	%r12, %rdi
	movl	%esi, -52(%rbp)
	movl	$1, %ecx
	call	*264(%rax)
	movl	-56(%rbp), %r8d
	movl	-52(%rbp), %esi
	movq	%r12, %rdi
	movl	%eax, %r15d
	movq	(%r12), %rax
	movl	$1, %ecx
	movl	%r8d, %edx
	call	*264(%rax)
	subl	%eax, %r15d
.L2833:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movslq	%r13d, %rbx
	call	*8(%rax)
	movq	(%r12), %rcx
	jmp	.L2822
.L2817:
	movq	128(%rcx), %rax
	leaq	_ZNK6icu_678Calendar10getMaximumE19UCalendarDateFields(%rip), %rdx
	cmpq	%rdx, %rax
	jne	.L2835
	movl	$3, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	*256(%rcx)
	movl	%eax, %r15d
.L2836:
	movq	(%r12), %rax
	leaq	_ZNK6icu_678Calendar15getLeastMaximumE19UCalendarDateFields(%rip), %rcx
	movq	160(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L2837
	movq	256(%rax), %rcx
	leaq	_ZNK6icu_678Calendar8getLimitE19UCalendarDateFieldsNS0_10ELimitTypeE(%rip), %rdx
	cmpq	%rdx, %rcx
	jne	.L2838
	cmpl	$7, %r13d
	ja	.L2839
	cmpl	$4, %r13d
	jne	.L2841
	movl	$2, %edx
	movl	$5, %esi
	movq	%r12, %rdi
	movzbl	260(%r12), %ebx
	call	*248(%rax)
	subl	%ebx, %eax
	movl	$4, %ebx
	addl	$7, %eax
	movslq	%eax, %rdx
	imulq	$-1840700269, %rdx, %rdx
	shrq	$32, %rdx
	addl	%eax, %edx
	sarl	$31, %eax
	sarl	$2, %edx
	subl	%eax, %edx
.L2844:
	movl	%r15d, %ecx
	movq	%r14, %r8
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_678Calendar15getActualHelperE19UCalendarDateFieldsiiR10UErrorCode
	movq	(%r12), %rcx
	movl	%eax, %r15d
	jmp	.L2822
	.p2align 4,,10
	.p2align 3
.L2816:
	movq	%r14, %rdx
	movslq	%r13d, %rbx
	call	*%rax
	movq	(%r12), %rcx
	movl	%eax, %r15d
.L2822:
	movq	%r14, %rdx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	*168(%rcx)
	movl	12(%r12,%rbx,4), %edx
	cmpl	%r15d, %edx
	jg	.L2854
	cmpl	%eax, %edx
	jl	.L2855
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2854:
	.cfi_restore_state
	movl	%r15d, %edx
.L2852:
	addq	$24, %rsp
	movl	%r13d, %esi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_678Calendar3setE19UCalendarDateFieldsi
	.p2align 4,,10
	.p2align 3
.L2855:
	.cfi_restore_state
	movl	%eax, %edx
	jmp	.L2852
	.p2align 4,,10
	.p2align 3
.L2849:
	xorl	%r15d, %r15d
	movslq	%r13d, %rbx
	jmp	.L2822
	.p2align 4,,10
	.p2align 3
.L2839:
	cmpl	$16, %r13d
	ja	.L2842
	cmpl	$8, %r13d
	jne	.L2843
.L2841:
	movl	$2, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	movslq	%r13d, %rbx
	call	*248(%rax)
	movl	%eax, %edx
	jmp	.L2844
	.p2align 4,,10
	.p2align 3
.L2842:
	leal	-20(%r13), %edx
	cmpl	$2, %edx
	ja	.L2841
.L2843:
	movslq	%r13d, %rbx
	leaq	_ZN6icu_67L15kCalendarLimitsE(%rip), %rdx
	movq	%rbx, %rax
	salq	$4, %rax
	movl	8(%rdx,%rax), %edx
	jmp	.L2844
	.p2align 4,,10
	.p2align 3
.L2834:
	call	*%rax
	movslq	%r13d, %rbx
	movq	(%r12), %rcx
	movl	%eax, %r15d
	jmp	.L2822
	.p2align 4,,10
	.p2align 3
.L2835:
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	*%rax
	movl	%eax, %r15d
	jmp	.L2836
	.p2align 4,,10
	.p2align 3
.L2851:
	movl	$7, (%r14)
	movq	(%r12), %rcx
	xorl	%r15d, %r15d
	movslq	%r13d, %rbx
	jmp	.L2822
	.p2align 4,,10
	.p2align 3
.L2837:
	movl	%r13d, %esi
	movq	%r12, %rdi
	movslq	%r13d, %rbx
	call	*%rdx
	movl	%eax, %edx
	jmp	.L2844
	.p2align 4,,10
	.p2align 3
.L2853:
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_678Calendar8completeER10UErrorCode
	movl	(%r14), %r8d
	testl	%r8d, %r8d
	jg	.L2824
	movl	20(%rbx), %r8d
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movl	%r8d, -52(%rbp)
	call	_ZN6icu_678Calendar8completeER10UErrorCode
	movl	(%r14), %edi
	movl	-52(%rbp), %r8d
	testl	%edi, %edi
	jg	.L2848
	movl	88(%rbx), %esi
	jmp	.L2825
	.p2align 4,,10
	.p2align 3
.L2829:
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_678Calendar8completeER10UErrorCode
	movl	(%r14), %eax
	testl	%eax, %eax
	jg	.L2831
	movl	88(%rbx), %r8d
	jmp	.L2830
	.p2align 4,,10
	.p2align 3
.L2838:
	movl	$2, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	movslq	%r13d, %rbx
	call	*%rcx
	movl	%eax, %edx
	jmp	.L2844
	.p2align 4,,10
	.p2align 3
.L2832:
	movl	%r8d, %esi
	movq	%r12, %rdi
	call	*%r15
	movl	%eax, %r15d
	jmp	.L2833
	.p2align 4,,10
	.p2align 3
.L2826:
	movl	%r8d, %edx
	movq	%r12, %rdi
	call	*%r15
	movl	%eax, %r15d
	jmp	.L2833
.L2848:
	xorl	%esi, %esi
	jmp	.L2825
	.cfi_endproc
.LFE3276:
	.size	_ZN6icu_678Calendar8pinFieldE19UCalendarDateFieldsR10UErrorCode, .-_ZN6icu_678Calendar8pinFieldE19UCalendarDateFieldsR10UErrorCode
	.align 2
	.p2align 4
	.type	_ZN6icu_678Calendar15fieldDifferenceEd19UCalendarDateFieldsR10UErrorCode.part.0, @function
_ZN6icu_678Calendar15fieldDifferenceEd19UCalendarDateFieldsR10UErrorCode.part.0:
.LFB4351:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	cmpb	$0, 8(%rdi)
	movq	(%rdi), %rdx
	je	.L3046
.L2857:
	movl	0(%r13), %r10d
	testl	%r10d, %r10d
	jg	.L2861
	movsd	224(%r14), %xmm5
	comisd	%xmm5, %xmm0
	movsd	%xmm5, -56(%rbp)
	ja	.L3047
	comisd	%xmm0, %xmm5
	jbe	.L3048
	movsd	.LC5(%rip), %xmm6
	leaq	_ZN6icu_678Calendar3addE19UCalendarDateFieldsiR10UErrorCode(%rip), %rax
	movl	$-1, %ebx
	movl	$0, -76(%rbp)
	movq	%rax, -72(%rbp)
	leaq	12(%r14), %rax
	movl	$32, %r15d
	movsd	%xmm6, -64(%rbp)
	movsd	-56(%rbp), %xmm6
	movsd	-64(%rbp), %xmm7
	movq	%rax, -96(%rbp)
	movl	%r12d, %eax
	movl	%ebx, %r12d
	comisd	%xmm7, %xmm6
	movl	%eax, %ebx
	jbe	.L3026
	.p2align 4,,10
	.p2align 3
.L3058:
	cmpb	$0, 232(%r14)
	movapd	%xmm7, %xmm1
	je	.L2915
.L2912:
	movq	-96(%rbp), %rax
	leaq	20(%r14), %rdi
	movl	%r14d, %esi
	movl	$16777217, 8(%r14)
	movsd	%xmm1, 224(%r14)
	andq	$-8, %rdi
	movq	$0, (%rax)
	subq	%rdi, %rax
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	$0, 119(%r14)
	addl	$115, %ecx
	shrl	$3, %ecx
	rep stosq
	leaq	136(%r14), %rdi
	xorl	%eax, %eax
	movq	$0, 128(%r14)
	movq	$0, 212(%r14)
	andq	$-8, %rdi
	subl	%edi, %esi
	leal	220(%rsi), %ecx
	shrl	$3, %ecx
	rep stosq
.L2913:
	movq	56(%rdx), %rax
	cmpq	-72(%rbp), %rax
	jne	.L2916
	testl	%r12d, %r12d
	je	.L2917
	movq	%r13, %rcx
	movl	%r12d, %edx
	movl	%ebx, %esi
	movq	%r14, %rdi
	movsd	%xmm0, -88(%rbp)
	call	_ZN6icu_678Calendar3addE19UCalendarDateFieldsiR10UErrorCode.part.0
	movsd	-88(%rbp), %xmm0
.L2917:
	movl	0(%r13), %r9d
	testl	%r9d, %r9d
	jg	.L2921
	cmpb	$0, 8(%r14)
	je	.L2920
.L2924:
	movsd	224(%r14), %xmm1
	ucomisd	%xmm1, %xmm0
	jnp	.L3049
.L2999:
	comisd	%xmm1, %xmm0
	movq	(%r14), %rdx
	ja	.L3050
	leal	(%r12,%r12), %eax
	subl	$1, %r15d
	jne	.L2929
	movl	$1, 0(%r13)
	movl	%ebx, %eax
	movl	%r12d, %ebx
	movl	%eax, %r12d
.L2952:
	movq	56(%rdx), %rax
	cmpq	-72(%rbp), %rax
	jne	.L2957
	testl	%ebx, %ebx
	je	.L2958
	movq	%r13, %rcx
	movl	%ebx, %edx
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	_ZN6icu_678Calendar3addE19UCalendarDateFieldsiR10UErrorCode.part.0
.L2958:
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L2959
.L2856:
	addq	$72, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3047:
	.cfi_restore_state
	leaq	_ZN6icu_678Calendar3addE19UCalendarDateFieldsiR10UErrorCode(%rip), %rax
	movl	$0, -76(%rbp)
	movl	$1, %ebx
	movsd	.LC5(%rip), %xmm6
	movq	%rax, -72(%rbp)
	leaq	12(%r14), %r15
	movsd	%xmm6, -64(%rbp)
	jmp	.L2862
	.p2align 4,,10
	.p2align 3
.L2872:
	cmpb	$0, 8(%r14)
	je	.L2874
.L2878:
	movsd	224(%r14), %xmm1
	ucomisd	%xmm1, %xmm0
	jnp	.L3051
.L2992:
	comisd	%xmm0, %xmm1
	ja	.L3052
	cmpl	$2147483647, %ebx
	je	.L2885
	movl	%ebx, %eax
	movl	$2147483647, %edx
	movl	0(%r13), %esi
	movl	%ebx, -76(%rbp)
	addl	%eax, %eax
	cmovs	%edx, %eax
	movq	(%r14), %rdx
	movq	%rdx, %rcx
	testl	%esi, %esi
	jg	.L3053
	movl	%eax, %ebx
.L2862:
	movsd	-56(%rbp), %xmm5
	movsd	-64(%rbp), %xmm3
	comisd	%xmm3, %xmm5
	jbe	.L3018
	cmpb	$0, 232(%r14)
	movapd	%xmm3, %xmm1
	je	.L2870
.L2867:
	leaq	20(%r14), %rdi
	movq	%r15, %rcx
	xorl	%eax, %eax
	movl	%r14d, %esi
	andq	$-8, %rdi
	movl	$16777217, 8(%r14)
	movsd	%xmm1, 224(%r14)
	subq	%rdi, %rcx
	addl	$115, %ecx
	movq	$0, (%r15)
	shrl	$3, %ecx
	movq	$0, 119(%r14)
	rep stosq
	leaq	136(%r14), %rdi
	xorl	%eax, %eax
	movq	$0, 128(%r14)
	movq	$0, 212(%r14)
	andq	$-8, %rdi
	subl	%edi, %esi
	leal	220(%rsi), %ecx
	shrl	$3, %ecx
	rep stosq
.L2868:
	movq	56(%rdx), %rax
	cmpq	-72(%rbp), %rax
	jne	.L3054
	movq	%r13, %rcx
	movl	%ebx, %edx
	movl	%r12d, %esi
	movq	%r14, %rdi
	movsd	%xmm0, -88(%rbp)
	call	_ZN6icu_678Calendar3addE19UCalendarDateFieldsiR10UErrorCode.part.0
	movsd	-88(%rbp), %xmm0
.L2871:
	movl	0(%r13), %r9d
	testl	%r9d, %r9d
	jle	.L2872
.L2875:
	pxor	%xmm1, %xmm1
.L3055:
	ucomisd	%xmm1, %xmm0
	jp	.L2992
.L3051:
	je	.L2856
	jmp	.L2992
	.p2align 4,,10
	.p2align 3
.L2885:
	movl	$1, 0(%r13)
	movq	(%r14), %rcx
.L2864:
	movq	%rcx, %rdx
.L3043:
	movl	-76(%rbp), %ebx
	jmp	.L2952
	.p2align 4,,10
	.p2align 3
.L3018:
	movsd	.LC6(%rip), %xmm4
	comisd	-56(%rbp), %xmm4
	jbe	.L3019
	cmpb	$0, 232(%r14)
	je	.L2870
	movapd	%xmm4, %xmm1
	jmp	.L2867
	.p2align 4,,10
	.p2align 3
.L3054:
	movsd	%xmm0, -88(%rbp)
	movq	%r13, %rcx
	movl	%ebx, %edx
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	*%rax
	movsd	-88(%rbp), %xmm0
	jmp	.L2871
	.p2align 4,,10
	.p2align 3
.L2874:
	movq	(%r14), %rax
	movsd	%xmm0, -88(%rbp)
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	*224(%rax)
	movl	0(%r13), %r8d
	movsd	-88(%rbp), %xmm0
	testl	%r8d, %r8d
	jg	.L2875
	cmpb	$0, 232(%r14)
	jne	.L2876
	cmpb	$0, 10(%r14)
	jne	.L2877
.L2876:
	movb	$0, 9(%r14)
.L2877:
	movb	$1, 8(%r14)
	movb	$0, 11(%r14)
	movl	0(%r13), %edi
	testl	%edi, %edi
	jle	.L2878
	pxor	%xmm1, %xmm1
	jmp	.L3055
	.p2align 4,,10
	.p2align 3
.L2870:
	movl	$1, 0(%r13)
	jmp	.L2868
	.p2align 4,,10
	.p2align 3
.L3019:
	movapd	%xmm5, %xmm1
	jmp	.L2867
	.p2align 4,,10
	.p2align 3
.L3046:
	movsd	%xmm0, -56(%rbp)
	movq	%r13, %rsi
	call	*224(%rdx)
	movl	0(%r13), %r11d
	movsd	-56(%rbp), %xmm0
	testl	%r11d, %r11d
	jle	.L3056
	movq	(%r14), %rdx
.L2861:
	pxor	%xmm1, %xmm1
	comisd	%xmm1, %xmm0
	ja	.L2906
	comisd	%xmm0, %xmm1
	jbe	.L3057
.L2906:
	movq	56(%rdx), %rax
	leaq	_ZN6icu_678Calendar3addE19UCalendarDateFieldsiR10UErrorCode(%rip), %rdx
	xorl	%ebx, %ebx
	cmpq	%rdx, %rax
	je	.L2958
	.p2align 4,,10
	.p2align 3
.L2957:
	movq	%r13, %rcx
	movl	%ebx, %edx
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	*%rax
	movl	0(%r13), %eax
	testl	%eax, %eax
	jle	.L2856
.L2959:
	xorl	%ebx, %ebx
	jmp	.L2856
	.p2align 4,,10
	.p2align 3
.L3056:
	cmpb	$0, 232(%r14)
	jne	.L2859
	cmpb	$0, 10(%r14)
	jne	.L2860
.L2859:
	movb	$0, 9(%r14)
.L2860:
	movb	$1, 8(%r14)
	movq	(%r14), %rdx
	movb	$0, 11(%r14)
	jmp	.L2857
	.p2align 4,,10
	.p2align 3
.L3048:
	leaq	_ZN6icu_678Calendar3addE19UCalendarDateFieldsiR10UErrorCode(%rip), %rax
	movsd	.LC5(%rip), %xmm4
	xorl	%ebx, %ebx
	movq	%rax, -72(%rbp)
	movsd	%xmm4, -64(%rbp)
.L2965:
	movsd	-56(%rbp), %xmm6
	movsd	-64(%rbp), %xmm7
	comisd	%xmm7, %xmm6
	jbe	.L3033
	cmpb	$0, 232(%r14)
	movsd	%xmm7, -56(%rbp)
	je	.L3042
.L2955:
	leaq	20(%r14), %rdi
	movl	%r14d, %ecx
	movsd	-56(%rbp), %xmm7
	xorl	%eax, %eax
	andq	$-8, %rdi
	movl	$16777217, 8(%r14)
	subl	%edi, %ecx
	movq	$0, 12(%r14)
	addl	$127, %ecx
	movq	$0, 119(%r14)
	movsd	%xmm7, 224(%r14)
	shrl	$3, %ecx
	rep stosq
	leaq	136(%r14), %rdi
	movl	%r14d, %ecx
	movq	$0, 128(%r14)
	movq	$0, 212(%r14)
	andq	$-8, %rdi
	subl	%edi, %ecx
	addl	$220, %ecx
	shrl	$3, %ecx
	rep stosq
	jmp	.L2952
	.p2align 4,,10
	.p2align 3
.L3057:
	movq	56(%rdx), %rax
	leaq	_ZN6icu_678Calendar3addE19UCalendarDateFieldsiR10UErrorCode(%rip), %rdx
	xorl	%ebx, %ebx
	cmpq	%rdx, %rax
	jne	.L2957
	xorl	%ebx, %ebx
	jmp	.L2856
	.p2align 4,,10
	.p2align 3
.L2929:
	movl	0(%r13), %r10d
	movl	%r12d, -76(%rbp)
	testl	%r10d, %r10d
	jg	.L3039
	movsd	-56(%rbp), %xmm6
	movsd	-64(%rbp), %xmm7
	movl	%eax, %r12d
	comisd	%xmm7, %xmm6
	ja	.L3058
.L3026:
	movsd	.LC6(%rip), %xmm6
	comisd	-56(%rbp), %xmm6
	jbe	.L3027
	cmpb	$0, 232(%r14)
	je	.L2915
	movapd	%xmm6, %xmm1
	jmp	.L2912
	.p2align 4,,10
	.p2align 3
.L2920:
	movq	(%r14), %rax
	movsd	%xmm0, -88(%rbp)
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	*224(%rax)
	movl	0(%r13), %r8d
	movsd	-88(%rbp), %xmm0
	testl	%r8d, %r8d
	jg	.L2921
	cmpb	$0, 232(%r14)
	jne	.L2922
	cmpb	$0, 10(%r14)
	jne	.L2923
.L2922:
	movb	$0, 9(%r14)
.L2923:
	movb	$1, 8(%r14)
	movb	$0, 11(%r14)
	movl	0(%r13), %edi
	testl	%edi, %edi
	jle	.L2924
.L2921:
	pxor	%xmm1, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L2999
.L3049:
	jne	.L2999
	movl	%r12d, %ebx
	jmp	.L2856
	.p2align 4,,10
	.p2align 3
.L2916:
	movsd	%xmm0, -88(%rbp)
	movq	%r13, %rcx
	movl	%r12d, %edx
	movl	%ebx, %esi
	movq	%r14, %rdi
	call	*%rax
	movsd	-88(%rbp), %xmm0
	jmp	.L2917
	.p2align 4,,10
	.p2align 3
.L2915:
	movl	$1, 0(%r13)
	jmp	.L2913
	.p2align 4,,10
	.p2align 3
.L3027:
	movsd	-56(%rbp), %xmm1
	jmp	.L2912
	.p2align 4,,10
	.p2align 3
.L3033:
	movsd	.LC6(%rip), %xmm0
	comisd	-56(%rbp), %xmm0
	jbe	.L2955
	cmpb	$0, 232(%r14)
	movsd	%xmm0, -56(%rbp)
	jne	.L2955
.L3042:
	movl	$1, 0(%r13)
	jmp	.L2952
	.p2align 4,,10
	.p2align 3
.L3052:
	movl	%ebx, %r15d
	subl	-76(%rbp), %r15d
	movl	0(%r13), %ecx
	movq	(%r14), %rdx
	cmpl	$1, %r15d
	jle	.L2931
	leaq	12(%r14), %rax
	movq	%rax, -104(%rbp)
	.p2align 4,,10
	.p2align 3
.L2883:
	testl	%ecx, %ecx
	jg	.L3043
	movsd	-56(%rbp), %xmm4
	movsd	-64(%rbp), %xmm2
	comisd	%xmm2, %xmm4
	jbe	.L3023
.L3062:
	cmpb	$0, 232(%r14)
	movapd	%xmm2, %xmm1
	je	.L2894
.L2891:
	movq	-104(%rbp), %rax
	leaq	20(%r14), %rdi
	movl	%r14d, %esi
	movl	$16777217, 8(%r14)
	movsd	%xmm1, 224(%r14)
	andq	$-8, %rdi
	movq	$0, (%rax)
	subq	%rdi, %rax
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	$0, 119(%r14)
	addl	$115, %ecx
	shrl	$3, %ecx
	rep stosq
	leaq	136(%r14), %rdi
	xorl	%eax, %eax
	movq	$0, 128(%r14)
	movq	$0, 212(%r14)
	andq	$-8, %rdi
	subl	%edi, %esi
	leal	220(%rsi), %ecx
	shrl	$3, %ecx
	rep stosq
.L2892:
	movl	-76(%rbp), %eax
	sarl	%r15d
	movq	56(%rdx), %r8
	addl	%r15d, %eax
	cmpq	-72(%rbp), %r8
	jne	.L3059
	movl	%eax, %edx
	movq	%r13, %rcx
	movl	%r12d, %esi
	movq	%r14, %rdi
	movl	%eax, -88(%rbp)
	movsd	%xmm0, -96(%rbp)
	call	_ZN6icu_678Calendar3addE19UCalendarDateFieldsiR10UErrorCode.part.0
	movl	-88(%rbp), %eax
	movsd	-96(%rbp), %xmm0
.L2895:
	movl	0(%r13), %edx
	testl	%edx, %edx
	jg	.L2899
	cmpb	$0, 8(%r14)
	je	.L2898
.L2902:
	movsd	224(%r14), %xmm1
	ucomisd	%xmm1, %xmm0
	jnp	.L3060
.L2996:
	comisd	%xmm0, %xmm1
	ja	.L2904
	movl	%ebx, %r15d
	movl	0(%r13), %ecx
	movq	(%r14), %rdx
	subl	%eax, %r15d
	cmpl	$1, %r15d
	jle	.L3061
	movl	%eax, -76(%rbp)
	testl	%ecx, %ecx
	jg	.L3043
	movsd	-56(%rbp), %xmm4
	movsd	-64(%rbp), %xmm2
	comisd	%xmm2, %xmm4
	ja	.L3062
.L3023:
	movsd	.LC6(%rip), %xmm4
	comisd	-56(%rbp), %xmm4
	jbe	.L3024
	cmpb	$0, 232(%r14)
	je	.L2894
	movapd	%xmm4, %xmm1
	jmp	.L2891
	.p2align 4,,10
	.p2align 3
.L2898:
	movq	(%r14), %rdx
	movl	%eax, -96(%rbp)
	movq	%r13, %rsi
	movq	%r14, %rdi
	movsd	%xmm0, -88(%rbp)
	call	*224(%rdx)
	movl	0(%r13), %eax
	movsd	-88(%rbp), %xmm0
	testl	%eax, %eax
	movl	-96(%rbp), %eax
	jg	.L2899
	cmpb	$0, 232(%r14)
	jne	.L2900
	cmpb	$0, 10(%r14)
	jne	.L2901
.L2900:
	movb	$0, 9(%r14)
.L2901:
	movb	$1, 8(%r14)
	movb	$0, 11(%r14)
	movl	0(%r13), %r11d
	testl	%r11d, %r11d
	jle	.L2902
.L2899:
	pxor	%xmm1, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L2996
.L3060:
	jne	.L2996
	movl	%eax, %ebx
	jmp	.L2856
	.p2align 4,,10
	.p2align 3
.L2904:
	movl	0(%r13), %ecx
	movq	(%r14), %rdx
	cmpl	$1, %r15d
	je	.L2931
	movl	%eax, %ebx
	jmp	.L2883
	.p2align 4,,10
	.p2align 3
.L3059:
	movl	%eax, -88(%rbp)
	movl	%eax, %edx
	movq	%r13, %rcx
	movl	%r12d, %esi
	movsd	%xmm0, -96(%rbp)
	movq	%r14, %rdi
	call	*%r8
	movsd	-96(%rbp), %xmm0
	movl	-88(%rbp), %eax
	jmp	.L2895
	.p2align 4,,10
	.p2align 3
.L2894:
	movl	$1, 0(%r13)
	jmp	.L2892
	.p2align 4,,10
	.p2align 3
.L3024:
	movsd	-56(%rbp), %xmm1
	jmp	.L2891
.L2981:
	movl	%r15d, -76(%rbp)
	movl	%r9d, %r12d
.L2931:
	movl	-76(%rbp), %ebx
.L2884:
	testl	%ecx, %ecx
	jle	.L2965
	jmp	.L2952
	.p2align 4,,10
	.p2align 3
.L3050:
	movl	%ebx, %eax
	movl	%r12d, %ebx
	movl	0(%r13), %ecx
	movl	%eax, %r12d
	movl	-76(%rbp), %eax
	subl	%ebx, %eax
	cmpl	$1, %eax
	jle	.L2931
	leaq	12(%r14), %rax
	movl	%r12d, %r9d
	movq	%rax, -104(%rbp)
	.p2align 4,,10
	.p2align 3
.L2928:
	testl	%ecx, %ecx
	jg	.L3063
.L2932:
	movl	-76(%rbp), %eax
	movl	%ebx, %r12d
	movsd	-56(%rbp), %xmm5
	movsd	-64(%rbp), %xmm6
	subl	%eax, %r12d
	movl	%r12d, %r15d
	shrl	$31, %r15d
	addl	%r12d, %r15d
	sarl	%r15d
	addl	%eax, %r15d
	comisd	%xmm6, %xmm5
	jbe	.L3030
	cmpb	$0, 232(%r14)
	movapd	%xmm6, %xmm1
	je	.L2939
.L2936:
	movq	-104(%rbp), %rax
	leaq	20(%r14), %rdi
	movl	%r14d, %esi
	movl	$16777217, 8(%r14)
	movsd	%xmm1, 224(%r14)
	andq	$-8, %rdi
	movq	$0, (%rax)
	subq	%rdi, %rax
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	$0, 119(%r14)
	addl	$115, %ecx
	shrl	$3, %ecx
	rep stosq
	leaq	136(%r14), %rdi
	xorl	%eax, %eax
	movq	$0, 128(%r14)
	movq	$0, 212(%r14)
	andq	$-8, %rdi
	subl	%edi, %esi
	leal	220(%rsi), %ecx
	shrl	$3, %ecx
	rep stosq
.L2937:
	movq	56(%rdx), %rax
	cmpq	-72(%rbp), %rax
	jne	.L2940
	testl	%r15d, %r15d
	je	.L2941
	movl	%r9d, %esi
	movq	%r13, %rcx
	movl	%r15d, %edx
	movq	%r14, %rdi
	movl	%r9d, -88(%rbp)
	movsd	%xmm0, -96(%rbp)
	call	_ZN6icu_678Calendar3addE19UCalendarDateFieldsiR10UErrorCode.part.0
	movl	-88(%rbp), %r9d
	movsd	-96(%rbp), %xmm0
.L2941:
	movl	0(%r13), %esi
	testl	%esi, %esi
	jg	.L2945
	cmpb	$0, 8(%r14)
	je	.L2944
.L2948:
	movsd	224(%r14), %xmm1
.L2943:
	ucomisd	%xmm1, %xmm0
	jnp	.L3064
.L3003:
	comisd	%xmm1, %xmm0
	jbe	.L3065
	movl	0(%r13), %ecx
	movq	(%r14), %rdx
	cmpl	$-3, %r12d
	jge	.L3066
	movl	%r15d, %ebx
	testl	%ecx, %ecx
	jle	.L2932
.L3063:
	movl	-76(%rbp), %ebx
	movl	%r9d, %r12d
	jmp	.L2952
	.p2align 4,,10
	.p2align 3
.L2944:
	movq	(%r14), %rax
	movl	%r9d, -96(%rbp)
	movq	%r13, %rsi
	movq	%r14, %rdi
	movsd	%xmm0, -88(%rbp)
	call	*224(%rax)
	movl	0(%r13), %ecx
	movsd	-88(%rbp), %xmm0
	movl	-96(%rbp), %r9d
	testl	%ecx, %ecx
	jg	.L2945
	cmpb	$0, 232(%r14)
	jne	.L2946
	cmpb	$0, 10(%r14)
	jne	.L2947
.L2946:
	movb	$0, 9(%r14)
.L2947:
	movb	$1, 8(%r14)
	movb	$0, 11(%r14)
	movl	0(%r13), %edx
	testl	%edx, %edx
	jle	.L2948
.L2945:
	pxor	%xmm1, %xmm1
	jmp	.L2943
	.p2align 4,,10
	.p2align 3
.L3065:
	movl	%r15d, %eax
	movl	0(%r13), %ecx
	movq	(%r14), %rdx
	subl	%ebx, %eax
	cmpl	$1, %eax
	jle	.L2981
	movl	%r15d, -76(%rbp)
	jmp	.L2928
	.p2align 4,,10
	.p2align 3
.L3030:
	movsd	.LC6(%rip), %xmm6
	comisd	-56(%rbp), %xmm6
	jbe	.L3031
	cmpb	$0, 232(%r14)
	je	.L2939
	movapd	%xmm6, %xmm1
	jmp	.L2936
	.p2align 4,,10
	.p2align 3
.L2940:
	movl	%r9d, -88(%rbp)
	movl	%r9d, %esi
	movq	%r13, %rcx
	movl	%r15d, %edx
	movsd	%xmm0, -96(%rbp)
	movq	%r14, %rdi
	call	*%rax
	movsd	-96(%rbp), %xmm0
	movl	-88(%rbp), %r9d
	jmp	.L2941
	.p2align 4,,10
	.p2align 3
.L2939:
	movl	$1, 0(%r13)
	jmp	.L2937
	.p2align 4,,10
	.p2align 3
.L3031:
	movapd	%xmm5, %xmm1
	jmp	.L2936
	.p2align 4,,10
	.p2align 3
.L3064:
	jne	.L3003
	movl	%r15d, %ebx
	jmp	.L2856
	.p2align 4,,10
	.p2align 3
.L3061:
	movl	%eax, %ebx
	jmp	.L2884
.L3066:
	movl	%r9d, %r12d
	jmp	.L2931
.L3053:
	movl	%ebx, -76(%rbp)
	jmp	.L2864
.L3039:
	movl	%ebx, %eax
	movl	%r12d, %ebx
	movl	%eax, %r12d
	jmp	.L2952
	.cfi_endproc
.LFE4351:
	.size	_ZN6icu_678Calendar15fieldDifferenceEd19UCalendarDateFieldsR10UErrorCode.part.0, .-_ZN6icu_678Calendar15fieldDifferenceEd19UCalendarDateFieldsR10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Calendar15fieldDifferenceEd19UCalendarDateFieldsR10UErrorCode
	.type	_ZN6icu_678Calendar15fieldDifferenceEd19UCalendarDateFieldsR10UErrorCode, @function
_ZN6icu_678Calendar15fieldDifferenceEd19UCalendarDateFieldsR10UErrorCode:
.LFB3289:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jle	.L3069
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L3069:
	jmp	_ZN6icu_678Calendar15fieldDifferenceEd19UCalendarDateFieldsR10UErrorCode.part.0
	.cfi_endproc
.LFE3289:
	.size	_ZN6icu_678Calendar15fieldDifferenceEd19UCalendarDateFieldsR10UErrorCode, .-_ZN6icu_678Calendar15fieldDifferenceEd19UCalendarDateFieldsR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Calendar15fieldDifferenceEdNS0_11EDateFieldsER10UErrorCode
	.type	_ZN6icu_678Calendar15fieldDifferenceEdNS0_11EDateFieldsER10UErrorCode, @function
_ZN6icu_678Calendar15fieldDifferenceEdNS0_11EDateFieldsER10UErrorCode:
.LFB3288:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	leaq	_ZN6icu_678Calendar15fieldDifferenceEd19UCalendarDateFieldsR10UErrorCode(%rip), %rcx
	movq	88(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3071
	movl	(%rdx), %eax
	testl	%eax, %eax
	jle	.L3073
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L3073:
	jmp	_ZN6icu_678Calendar15fieldDifferenceEd19UCalendarDateFieldsR10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L3071:
	jmp	*%rax
	.cfi_endproc
.LFE3288:
	.size	_ZN6icu_678Calendar15fieldDifferenceEdNS0_11EDateFieldsER10UErrorCode, .-_ZN6icu_678Calendar15fieldDifferenceEdNS0_11EDateFieldsER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_678Calendar11computeTimeER10UErrorCode
	.type	_ZN6icu_678Calendar11computeTimeER10UErrorCode, @function
_ZN6icu_678Calendar11computeTimeER10UErrorCode:
.LFB3326:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 232(%rdi)
	jne	.L3075
	movl	(%rsi), %edx
	xorl	%r12d, %r12d
	testl	%edx, %edx
	jle	.L3076
	jmp	.L3074
	.p2align 4,,10
	.p2align 3
.L3078:
	addq	$1, %r12
	cmpq	$23, %r12
	je	.L3075
.L3076:
	cmpl	$1, 128(%rbx,%r12,4)
	jle	.L3078
	movq	(%rbx), %rax
	movq	%r13, %rdx
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	*312(%rax)
	movl	0(%r13), %eax
	testl	%eax, %eax
	jle	.L3078
.L3074:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3107
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3075:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN6icu_678Calendar16computeJulianDayEv
	movl	212(%rbx), %ecx
	pxor	%xmm0, %xmm0
	subl	$2440588, %eax
	cvtsi2sdl	%eax, %xmm0
	mulsd	.LC1(%rip), %xmm0
	cmpl	$1, %ecx
	jle	.L3080
	movdqu	164(%rbx), %xmm4
	pxor	%xmm1, %xmm1
	movdqu	164(%rbx), %xmm5
	movl	184(%rbx), %edx
	cmpl	%edx, 180(%rbx)
	pcmpgtd	%xmm1, %xmm4
	cmovge	180(%rbx), %edx
	pand	%xmm5, %xmm4
	movdqa	%xmm4, %xmm3
	psrldq	$8, %xmm3
	movdqa	%xmm3, %xmm2
	pcmpgtd	%xmm4, %xmm2
	pand	%xmm2, %xmm3
	pandn	%xmm4, %xmm2
	por	%xmm2, %xmm3
	movdqa	%xmm3, %xmm1
	psrldq	$4, %xmm1
	movdqa	%xmm1, %xmm2
	pcmpgtd	%xmm3, %xmm2
	pand	%xmm2, %xmm1
	pandn	%xmm3, %xmm2
	por	%xmm1, %xmm2
	movd	%xmm2, %eax
	cmpl	%eax, %edx
	cmovge	%edx, %eax
	cmpl	%eax, %ecx
	jl	.L3080
	pxor	%xmm1, %xmm1
	cmpl	$1, 188(%rbx)
	cvtsi2sdl	96(%rbx), %xmm1
	jle	.L3108
.L3084:
	addsd	%xmm1, %xmm0
	movl	72(%rbx), %eax
	pxor	%xmm1, %xmm1
	addl	76(%rbx), %eax
	cvtsi2sdl	%eax, %xmm1
	movl	0(%r13), %edx
	subsd	%xmm1, %xmm0
.L3086:
	testl	%edx, %edx
	jg	.L3074
	movsd	%xmm0, 224(%rbx)
	jmp	.L3074
	.p2align 4,,10
	.p2align 3
.L3080:
	movl	172(%rbx), %edx
	movl	164(%rbx), %eax
	cmpl	%eax, 168(%rbx)
	cmovge	168(%rbx), %eax
	cmpl	%eax, %edx
	movl	%eax, %ecx
	cmovge	%edx, %ecx
	testl	%ecx, %ecx
	jne	.L3109
	movsd	.LC10(%rip), %xmm4
	pxor	%xmm1, %xmm1
.L3082:
	pxor	%xmm2, %xmm2
	cmpl	$1, 188(%rbx)
	cvtsi2sdl	60(%rbx), %xmm2
	addsd	%xmm1, %xmm2
	pxor	%xmm1, %xmm1
	cvtsi2sdl	64(%rbx), %xmm1
	mulsd	%xmm4, %xmm2
	addsd	%xmm2, %xmm1
	mulsd	.LC11(%rip), %xmm1
	movapd	%xmm1, %xmm2
	pxor	%xmm1, %xmm1
	cvtsi2sdl	68(%rbx), %xmm1
	addsd	%xmm2, %xmm1
	jg	.L3084
.L3108:
	cmpl	$1, 192(%rbx)
	jg	.L3084
	cmpb	$0, 232(%rbx)
	je	.L3087
	cmpl	$2, 252(%rbx)
	jne	.L3110
.L3087:
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movsd	%xmm1, -80(%rbp)
	movsd	%xmm0, -72(%rbp)
	call	_ZN6icu_678Calendar17computeZoneOffsetEddR10UErrorCode
	movsd	-72(%rbp), %xmm0
	leaq	-56(%rbp), %rdx
	xorl	%esi, %esi
	movsd	-80(%rbp), %xmm1
	movl	%eax, %r12d
	leaq	-52(%rbp), %rcx
	movq	%r13, %r8
	movq	240(%rbx), %rdi
	addsd	%xmm1, %xmm0
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
	movq	(%rdi), %rax
	subsd	%xmm1, %xmm0
	movsd	%xmm0, -72(%rbp)
	call	*48(%rax)
	movl	0(%r13), %edx
	testl	%edx, %edx
	jle	.L3111
.L3094:
	pxor	%xmm0, %xmm0
	jmp	.L3086
	.p2align 4,,10
	.p2align 3
.L3109:
	cmpl	%eax, %edx
	jge	.L3112
	movl	48(%rbx), %eax
	pxor	%xmm2, %xmm2
	pxor	%xmm1, %xmm1
	movsd	.LC10(%rip), %xmm4
	cvtsi2sdl	52(%rbx), %xmm1
	addsd	.LC0(%rip), %xmm1
	leal	(%rax,%rax,2), %eax
	sall	$2, %eax
	cvtsi2sdl	%eax, %xmm2
	addsd	%xmm1, %xmm2
	movapd	%xmm2, %xmm1
	mulsd	%xmm4, %xmm1
	jmp	.L3082
	.p2align 4,,10
	.p2align 3
.L3112:
	pxor	%xmm1, %xmm1
	pxor	%xmm3, %xmm3
	movsd	.LC10(%rip), %xmm4
	cvtsi2sdl	56(%rbx), %xmm1
	addsd	%xmm1, %xmm3
	mulsd	%xmm4, %xmm3
	movapd	%xmm3, %xmm1
	jmp	.L3082
.L3110:
	movapd	%xmm1, %xmm2
	movq	%r13, %rsi
	movq	%rbx, %rdi
	addsd	%xmm0, %xmm2
	movsd	%xmm2, -72(%rbp)
	call	_ZN6icu_678Calendar17computeZoneOffsetEddR10UErrorCode
	pxor	%xmm0, %xmm0
	movsd	-72(%rbp), %xmm2
	movl	0(%r13), %edx
	cvtsi2sdl	%eax, %xmm0
	subsd	%xmm0, %xmm2
	movapd	%xmm2, %xmm0
	jmp	.L3086
.L3111:
	movl	-52(%rbp), %eax
	addl	-56(%rbp), %eax
	cmpl	%r12d, %eax
	movsd	-72(%rbp), %xmm0
	je	.L3086
	cmpb	$0, 232(%rbx)
	jne	.L3089
	movl	$1, 0(%r13)
	jmp	.L3074
.L3089:
	movq	%r13, %rdx
	leaq	-48(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZNK6icu_678Calendar34getImmediatePreviousZoneTransitionEdPdR10UErrorCode
	movl	0(%r13), %edx
	testb	%al, %al
	je	.L3094
	movsd	-48(%rbp), %xmm0
	testl	%edx, %edx
	jg	.L3094
	jmp	.L3086
.L3107:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3326:
	.size	_ZN6icu_678Calendar11computeTimeER10UErrorCode, .-_ZN6icu_678Calendar11computeTimeER10UErrorCode
	.weak	_ZTSN6icu_678CalendarE
	.section	.rodata._ZTSN6icu_678CalendarE,"aG",@progbits,_ZTSN6icu_678CalendarE,comdat
	.align 16
	.type	_ZTSN6icu_678CalendarE, @object
	.size	_ZTSN6icu_678CalendarE, 19
_ZTSN6icu_678CalendarE:
	.string	"N6icu_678CalendarE"
	.weak	_ZTIN6icu_678CalendarE
	.section	.data.rel.ro._ZTIN6icu_678CalendarE,"awG",@progbits,_ZTIN6icu_678CalendarE,comdat
	.align 8
	.type	_ZTIN6icu_678CalendarE, @object
	.size	_ZTIN6icu_678CalendarE, 24
_ZTIN6icu_678CalendarE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_678CalendarE
	.quad	_ZTIN6icu_677UObjectE
	.weak	_ZTSN6icu_6714SharedCalendarE
	.section	.rodata._ZTSN6icu_6714SharedCalendarE,"aG",@progbits,_ZTSN6icu_6714SharedCalendarE,comdat
	.align 16
	.type	_ZTSN6icu_6714SharedCalendarE, @object
	.size	_ZTSN6icu_6714SharedCalendarE, 26
_ZTSN6icu_6714SharedCalendarE:
	.string	"N6icu_6714SharedCalendarE"
	.weak	_ZTIN6icu_6714SharedCalendarE
	.section	.data.rel.ro._ZTIN6icu_6714SharedCalendarE,"awG",@progbits,_ZTIN6icu_6714SharedCalendarE,comdat
	.align 8
	.type	_ZTIN6icu_6714SharedCalendarE, @object
	.size	_ZTIN6icu_6714SharedCalendarE, 24
_ZTIN6icu_6714SharedCalendarE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6714SharedCalendarE
	.quad	_ZTIN6icu_6712SharedObjectE
	.weak	_ZTSN6icu_678CacheKeyINS_14SharedCalendarEEE
	.section	.rodata._ZTSN6icu_678CacheKeyINS_14SharedCalendarEEE,"aG",@progbits,_ZTSN6icu_678CacheKeyINS_14SharedCalendarEEE,comdat
	.align 32
	.type	_ZTSN6icu_678CacheKeyINS_14SharedCalendarEEE, @object
	.size	_ZTSN6icu_678CacheKeyINS_14SharedCalendarEEE, 41
_ZTSN6icu_678CacheKeyINS_14SharedCalendarEEE:
	.string	"N6icu_678CacheKeyINS_14SharedCalendarEEE"
	.weak	_ZTIN6icu_678CacheKeyINS_14SharedCalendarEEE
	.section	.data.rel.ro._ZTIN6icu_678CacheKeyINS_14SharedCalendarEEE,"awG",@progbits,_ZTIN6icu_678CacheKeyINS_14SharedCalendarEEE,comdat
	.align 8
	.type	_ZTIN6icu_678CacheKeyINS_14SharedCalendarEEE, @object
	.size	_ZTIN6icu_678CacheKeyINS_14SharedCalendarEEE, 24
_ZTIN6icu_678CacheKeyINS_14SharedCalendarEEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_678CacheKeyINS_14SharedCalendarEEE
	.quad	_ZTIN6icu_6712CacheKeyBaseE
	.weak	_ZTSN6icu_6714LocaleCacheKeyINS_14SharedCalendarEEE
	.section	.rodata._ZTSN6icu_6714LocaleCacheKeyINS_14SharedCalendarEEE,"aG",@progbits,_ZTSN6icu_6714LocaleCacheKeyINS_14SharedCalendarEEE,comdat
	.align 32
	.type	_ZTSN6icu_6714LocaleCacheKeyINS_14SharedCalendarEEE, @object
	.size	_ZTSN6icu_6714LocaleCacheKeyINS_14SharedCalendarEEE, 48
_ZTSN6icu_6714LocaleCacheKeyINS_14SharedCalendarEEE:
	.string	"N6icu_6714LocaleCacheKeyINS_14SharedCalendarEEE"
	.weak	_ZTIN6icu_6714LocaleCacheKeyINS_14SharedCalendarEEE
	.section	.data.rel.ro._ZTIN6icu_6714LocaleCacheKeyINS_14SharedCalendarEEE,"awG",@progbits,_ZTIN6icu_6714LocaleCacheKeyINS_14SharedCalendarEEE,comdat
	.align 8
	.type	_ZTIN6icu_6714LocaleCacheKeyINS_14SharedCalendarEEE, @object
	.size	_ZTIN6icu_6714LocaleCacheKeyINS_14SharedCalendarEEE, 24
_ZTIN6icu_6714LocaleCacheKeyINS_14SharedCalendarEEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6714LocaleCacheKeyINS_14SharedCalendarEEE
	.quad	_ZTIN6icu_678CacheKeyINS_14SharedCalendarEEE
	.weak	_ZTVN6icu_6714SharedCalendarE
	.section	.data.rel.ro._ZTVN6icu_6714SharedCalendarE,"awG",@progbits,_ZTVN6icu_6714SharedCalendarE,comdat
	.align 8
	.type	_ZTVN6icu_6714SharedCalendarE, @object
	.size	_ZTVN6icu_6714SharedCalendarE, 40
_ZTVN6icu_6714SharedCalendarE:
	.quad	0
	.quad	_ZTIN6icu_6714SharedCalendarE
	.quad	_ZN6icu_6714SharedCalendarD1Ev
	.quad	_ZN6icu_6714SharedCalendarD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.weak	_ZTVN6icu_678CacheKeyINS_14SharedCalendarEEE
	.section	.data.rel.ro._ZTVN6icu_678CacheKeyINS_14SharedCalendarEEE,"awG",@progbits,_ZTVN6icu_678CacheKeyINS_14SharedCalendarEEE,comdat
	.align 8
	.type	_ZTVN6icu_678CacheKeyINS_14SharedCalendarEEE, @object
	.size	_ZTVN6icu_678CacheKeyINS_14SharedCalendarEEE, 80
_ZTVN6icu_678CacheKeyINS_14SharedCalendarEEE:
	.quad	0
	.quad	_ZTIN6icu_678CacheKeyINS_14SharedCalendarEEE
	.quad	0
	.quad	0
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZNK6icu_678CacheKeyINS_14SharedCalendarEE8hashCodeEv
	.quad	__cxa_pure_virtual
	.quad	_ZNK6icu_678CacheKeyINS_14SharedCalendarEEeqERKNS_12CacheKeyBaseE
	.quad	__cxa_pure_virtual
	.quad	_ZNK6icu_678CacheKeyINS_14SharedCalendarEE16writeDescriptionEPci
	.weak	_ZTVN6icu_6714LocaleCacheKeyINS_14SharedCalendarEEE
	.section	.data.rel.ro._ZTVN6icu_6714LocaleCacheKeyINS_14SharedCalendarEEE,"awG",@progbits,_ZTVN6icu_6714LocaleCacheKeyINS_14SharedCalendarEEE,comdat
	.align 8
	.type	_ZTVN6icu_6714LocaleCacheKeyINS_14SharedCalendarEEE, @object
	.size	_ZTVN6icu_6714LocaleCacheKeyINS_14SharedCalendarEEE, 80
_ZTVN6icu_6714LocaleCacheKeyINS_14SharedCalendarEEE:
	.quad	0
	.quad	_ZTIN6icu_6714LocaleCacheKeyINS_14SharedCalendarEEE
	.quad	_ZN6icu_6714LocaleCacheKeyINS_14SharedCalendarEED1Ev
	.quad	_ZN6icu_6714LocaleCacheKeyINS_14SharedCalendarEED0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZNK6icu_6714LocaleCacheKeyINS_14SharedCalendarEE8hashCodeEv
	.quad	_ZNK6icu_6714LocaleCacheKeyINS_14SharedCalendarEE5cloneEv
	.quad	_ZNK6icu_6714LocaleCacheKeyINS_14SharedCalendarEEeqERKNS_12CacheKeyBaseE
	.quad	_ZNK6icu_6714LocaleCacheKeyINS_14SharedCalendarEE12createObjectEPKvR10UErrorCode
	.quad	_ZNK6icu_6714LocaleCacheKeyINS_14SharedCalendarEE16writeDescriptionEPci
	.weak	_ZTVN6icu_678CalendarE
	.section	.data.rel.ro._ZTVN6icu_678CalendarE,"awG",@progbits,_ZTVN6icu_678CalendarE,comdat
	.align 8
	.type	_ZTVN6icu_678CalendarE, @object
	.size	_ZTVN6icu_678CalendarE, 416
_ZTVN6icu_678CalendarE:
	.quad	0
	.quad	_ZTIN6icu_678CalendarE
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZNK6icu_678CalendareqERKS0_
	.quad	_ZNK6icu_678Calendar14isEquivalentToERKS0_
	.quad	_ZN6icu_678Calendar3addENS0_11EDateFieldsEiR10UErrorCode
	.quad	_ZN6icu_678Calendar3addE19UCalendarDateFieldsiR10UErrorCode
	.quad	_ZN6icu_678Calendar4rollENS0_11EDateFieldsEiR10UErrorCode
	.quad	_ZN6icu_678Calendar4rollE19UCalendarDateFieldsiR10UErrorCode
	.quad	_ZN6icu_678Calendar15fieldDifferenceEdNS0_11EDateFieldsER10UErrorCode
	.quad	_ZN6icu_678Calendar15fieldDifferenceEd19UCalendarDateFieldsR10UErrorCode
	.quad	__cxa_pure_virtual
	.quad	_ZNK6icu_678Calendar10getMinimumENS0_11EDateFieldsE
	.quad	_ZNK6icu_678Calendar10getMinimumE19UCalendarDateFields
	.quad	_ZNK6icu_678Calendar10getMaximumENS0_11EDateFieldsE
	.quad	_ZNK6icu_678Calendar10getMaximumE19UCalendarDateFields
	.quad	_ZNK6icu_678Calendar18getGreatestMinimumENS0_11EDateFieldsE
	.quad	_ZNK6icu_678Calendar18getGreatestMinimumE19UCalendarDateFields
	.quad	_ZNK6icu_678Calendar15getLeastMaximumENS0_11EDateFieldsE
	.quad	_ZNK6icu_678Calendar15getLeastMaximumE19UCalendarDateFields
	.quad	_ZNK6icu_678Calendar16getActualMinimumE19UCalendarDateFieldsR10UErrorCode
	.quad	_ZNK6icu_678Calendar16getActualMaximumE19UCalendarDateFieldsR10UErrorCode
	.quad	__cxa_pure_virtual
	.quad	_ZNK6icu_678Calendar16getDayOfWeekTypeE19UCalendarDaysOfWeekR10UErrorCode
	.quad	_ZNK6icu_678Calendar20getWeekendTransitionE19UCalendarDaysOfWeekR10UErrorCode
	.quad	_ZNK6icu_678Calendar9isWeekendEdR10UErrorCode
	.quad	_ZNK6icu_678Calendar9isWeekendEv
	.quad	_ZN6icu_678Calendar11computeTimeER10UErrorCode
	.quad	_ZN6icu_678Calendar13computeFieldsER10UErrorCode
	.quad	_ZN6icu_678Calendar16prepareGetActualE19UCalendarDateFieldsaR10UErrorCode
	.quad	__cxa_pure_virtual
	.quad	_ZNK6icu_678Calendar8getLimitE19UCalendarDateFieldsNS0_10ELimitTypeE
	.quad	__cxa_pure_virtual
	.quad	_ZNK6icu_678Calendar20handleGetMonthLengthEii
	.quad	_ZNK6icu_678Calendar19handleGetYearLengthEi
	.quad	__cxa_pure_virtual
	.quad	_ZN6icu_678Calendar22handleComputeJulianDayE19UCalendarDateFields
	.quad	_ZN6icu_678Calendar35handleGetExtendedYearFromWeekFieldsEii
	.quad	_ZN6icu_678Calendar13validateFieldE19UCalendarDateFieldsR10UErrorCode
	.quad	_ZNK6icu_678Calendar23getFieldResolutionTableEv
	.quad	_ZN6icu_678Calendar19handleComputeFieldsEiR10UErrorCode
	.quad	_ZN6icu_678Calendar21getDefaultMonthInYearEi
	.quad	_ZN6icu_678Calendar20getDefaultDayInMonthEii
	.quad	_ZN6icu_678Calendar8pinFieldE19UCalendarDateFieldsR10UErrorCode
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZNK6icu_678Calendar14getRelatedYearER10UErrorCode
	.quad	_ZN6icu_678Calendar14setRelatedYearEi
	.globl	_ZN6icu_678Calendar15kYearPrecedenceE
	.section	.rodata
	.align 32
	.type	_ZN6icu_678Calendar15kYearPrecedenceE, @object
	.size	_ZN6icu_678Calendar15kYearPrecedenceE, 768
_ZN6icu_678Calendar15kYearPrecedenceE:
	.long	1
	.long	-1
	.zero	24
	.long	19
	.long	-1
	.zero	24
	.long	17
	.long	3
	.long	-1
	.zero	20
	.long	-1
	.zero	28
	.zero	256
	.long	-1
	.zero	28
	.zero	352
	.globl	_ZN6icu_678Calendar14kDOWPrecedenceE
	.align 32
	.type	_ZN6icu_678Calendar14kDOWPrecedenceE, @object
	.size	_ZN6icu_678Calendar14kDOWPrecedenceE, 768
_ZN6icu_678Calendar14kDOWPrecedenceE:
	.long	7
	.long	-1
	.long	-1
	.zero	20
	.long	18
	.long	-1
	.long	-1
	.zero	20
	.long	-1
	.zero	28
	.zero	288
	.long	-1
	.zero	28
	.zero	352
	.globl	_ZN6icu_678Calendar15kDatePrecedenceE
	.align 32
	.type	_ZN6icu_678Calendar15kDatePrecedenceE, @object
	.size	_ZN6icu_678Calendar15kDatePrecedenceE, 1152
_ZN6icu_678Calendar15kDatePrecedenceE:
	.long	5
	.long	-1
	.zero	24
	.long	3
	.long	7
	.long	-1
	.zero	20
	.long	4
	.long	7
	.long	-1
	.zero	20
	.long	8
	.long	7
	.long	-1
	.zero	20
	.long	3
	.long	18
	.long	-1
	.zero	20
	.long	4
	.long	18
	.long	-1
	.zero	20
	.long	8
	.long	18
	.long	-1
	.zero	20
	.long	6
	.long	-1
	.zero	24
	.long	37
	.long	1
	.long	-1
	.zero	20
	.long	35
	.long	17
	.long	-1
	.zero	20
	.long	-1
	.zero	28
	.zero	32
	.long	3
	.long	-1
	.zero	24
	.long	4
	.long	-1
	.zero	24
	.long	8
	.long	-1
	.zero	24
	.long	40
	.long	7
	.long	-1
	.zero	20
	.long	40
	.long	18
	.long	-1
	.zero	20
	.long	-1
	.zero	28
	.zero	192
	.long	-1
	.zero	28
	.zero	352
	.align 8
	.type	_ZN6icu_67L10gGregorianE, @object
	.size	_ZN6icu_67L10gGregorianE, 10
_ZN6icu_67L10gGregorianE:
	.string	"gregorian"
	.align 8
	.type	_ZN6icu_67L11gMonthNamesE, @object
	.size	_ZN6icu_67L11gMonthNamesE, 11
_ZN6icu_67L11gMonthNamesE:
	.string	"monthNames"
	.align 8
	.type	_ZN6icu_67L9gCalendarE, @object
	.size	_ZN6icu_67L9gCalendarE, 9
_ZN6icu_67L9gCalendarE:
	.string	"calendar"
	.align 32
	.type	_ZN6icu_67L15kCalendarLimitsE, @object
	.size	_ZN6icu_67L15kCalendarLimitsE, 368
_ZN6icu_67L15kCalendarLimitsE:
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	1
	.long	1
	.long	7
	.long	7
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	0
	.long	0
	.long	1
	.long	1
	.long	0
	.long	0
	.long	11
	.long	11
	.long	0
	.long	0
	.long	23
	.long	23
	.long	0
	.long	0
	.long	59
	.long	59
	.long	0
	.long	0
	.long	59
	.long	59
	.long	0
	.long	0
	.long	999
	.long	999
	.long	-43200000
	.long	-43200000
	.long	43200000
	.long	54000000
	.long	0
	.long	0
	.long	3600000
	.long	3600000
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	1
	.long	1
	.long	7
	.long	7
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-2130706432
	.long	-2130706432
	.long	2130706432
	.long	2130706432
	.long	0
	.long	0
	.long	86399999
	.long	86399999
	.long	0
	.long	0
	.long	1
	.long	1
	.section	.rodata.str1.1
.LC21:
	.string	"japanese"
.LC22:
	.string	"buddhist"
.LC23:
	.string	"persian"
.LC24:
	.string	"islamic-civil"
.LC25:
	.string	"islamic"
.LC26:
	.string	"hebrew"
.LC27:
	.string	"chinese"
.LC28:
	.string	"indian"
.LC29:
	.string	"ethiopic"
.LC30:
	.string	"ethiopic-amete-alem"
.LC31:
	.string	"iso8601"
.LC32:
	.string	"dangi"
.LC33:
	.string	"islamic-umalqura"
.LC34:
	.string	"islamic-tbla"
.LC35:
	.string	"islamic-rgsa"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	_ZL9gCalTypes, @object
	.size	_ZL9gCalTypes, 152
_ZL9gCalTypes:
	.quad	.LC4
	.quad	.LC21
	.quad	.LC22
	.quad	.LC7
	.quad	.LC23
	.quad	.LC24
	.quad	.LC25
	.quad	.LC26
	.quad	.LC27
	.quad	.LC28
	.quad	.LC8
	.quad	.LC29
	.quad	.LC30
	.quad	.LC31
	.quad	.LC32
	.quad	.LC33
	.quad	.LC34
	.quad	.LC35
	.quad	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	0
	.align 8
.LC1:
	.long	0
	.long	1100257648
	.align 8
.LC2:
	.long	0
	.long	1075576832
	.align 8
.LC3:
	.long	0
	.long	1072693248
	.align 8
.LC5:
	.long	3724070272
	.long	1132751422
	.align 8
.LC6:
	.long	1644638848
	.long	-1014729157
	.align 8
.LC9:
	.long	0
	.long	1103234626
	.align 8
.LC10:
	.long	0
	.long	1078853632
	.align 8
.LC11:
	.long	0
	.long	1083129856
	.align 8
.LC12:
	.long	0
	.long	1098160496
	.align 8
.LC18:
	.long	0
	.long	1099209072
	.align 8
.LC19:
	.long	0
	.long	1095464768
	.align 8
.LC20:
	.long	0
	.long	1089293312
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
