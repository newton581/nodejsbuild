	.file	"rbt_pars.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_679ParseDataD2Ev
	.type	_ZN6icu_679ParseDataD2Ev, @function
_ZN6icu_679ParseDataD2Ev:
.LFB2494:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_679ParseDataE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6711SymbolTableD2Ev@PLT
	.cfi_endproc
.LFE2494:
	.size	_ZN6icu_679ParseDataD2Ev, .-_ZN6icu_679ParseDataD2Ev
	.globl	_ZN6icu_679ParseDataD1Ev
	.set	_ZN6icu_679ParseDataD1Ev,_ZN6icu_679ParseDataD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_679ParseDataD0Ev
	.type	_ZN6icu_679ParseDataD0Ev, @function
_ZN6icu_679ParseDataD0Ev:
.LFB2496:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_679ParseDataE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6711SymbolTableD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2496:
	.size	_ZN6icu_679ParseDataD0Ev, .-_ZN6icu_679ParseDataD0Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_679ParseData6lookupERKNS_13UnicodeStringE
	.type	_ZNK6icu_679ParseData6lookupERKNS_13UnicodeStringE, @function
_ZNK6icu_679ParseData6lookupERKNS_13UnicodeStringE:
.LFB2497:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	movq	(%rax), %rdi
	jmp	uhash_get_67@PLT
	.cfi_endproc
.LFE2497:
	.size	_ZNK6icu_679ParseData6lookupERKNS_13UnicodeStringE, .-_ZNK6icu_679ParseData6lookupERKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_679ParseData13lookupMatcherEi
	.type	_ZNK6icu_679ParseData13lookupMatcherEi, @function
_ZNK6icu_679ParseData13lookupMatcherEi:
.LFB2498:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movzwl	1162(%rax), %eax
	subl	%eax, %esi
	js	.L6
	movq	16(%rdi), %rdi
	cmpl	8(%rdi), %esi
	jge	.L6
	jmp	_ZNK6icu_677UVector9elementAtEi@PLT
	.p2align 4,,10
	.p2align 3
.L6:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2498:
	.size	_ZNK6icu_679ParseData13lookupMatcherEi, .-_ZNK6icu_679ParseData13lookupMatcherEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_679ParseData14parseReferenceERKNS_13UnicodeStringERNS_13ParsePositionEi
	.type	_ZNK6icu_679ParseData14parseReferenceERKNS_13UnicodeStringERNS_13ParsePositionEi, @function
_ZNK6icu_679ParseData14parseReferenceERKNS_13UnicodeStringERNS_13ParsePositionEi:
.LFB2499:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movslq	8(%rcx), %rsi
	movq	%rax, (%rdi)
	movl	$2, %eax
	movq	%rdi, -64(%rbp)
	movq	%rcx, -72(%rbp)
	movw	%ax, 8(%rdi)
	cmpl	%esi, %r8d
	jle	.L10
	movq	%rdx, %r12
	movl	%r8d, %r14d
	movq	%rsi, %r13
	movl	%esi, %ebx
	leaq	(%rsi,%rsi), %r15
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L33:
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L14:
	movl	$65535, %edi
	cmpl	%ebx, %ecx
	jbe	.L15
	testb	$2, %al
	je	.L16
	leaq	10(%r12), %rax
.L17:
	movzwl	(%rax,%r15), %edi
.L15:
	cmpl	%ebx, %r13d
	je	.L18
.L22:
	call	u_isIDPart_67@PLT
	testb	%al, %al
	je	.L32
	addl	$1, %ebx
	addq	$2, %r15
	cmpl	%ebx, %r14d
	je	.L23
.L24:
	movzwl	8(%r12), %eax
	testw	%ax, %ax
	jns	.L33
	movl	12(%r12), %ecx
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L16:
	movq	24(%r12), %rax
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L18:
	movl	%edi, -52(%rbp)
	call	u_isIDStart_67@PLT
	movl	-52(%rbp), %edi
	testb	%al, %al
	jne	.L22
.L10:
	movq	-64(%rbp), %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore_state
	cmpl	%ebx, %r13d
	je	.L10
	movl	%ebx, %r14d
.L23:
	movq	-72(%rbp), %rax
	movq	-64(%rbp), %rcx
	movl	%r14d, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	movl	%r14d, 8(%rax)
	movq	(%r12), %rax
	call	*24(%rax)
	jmp	.L10
	.cfi_endproc
.LFE2499:
	.size	_ZNK6icu_679ParseData14parseReferenceERKNS_13UnicodeStringERNS_13ParsePositionEi, .-_ZNK6icu_679ParseData14parseReferenceERKNS_13UnicodeStringERNS_13ParsePositionEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_679ParseDataC2EPKNS_23TransliterationRuleDataEPKNS_7UVectorEPKNS_9HashtableE
	.type	_ZN6icu_679ParseDataC2EPKNS_23TransliterationRuleDataEPKNS_7UVectorEPKNS_9HashtableE, @function
_ZN6icu_679ParseDataC2EPKNS_23TransliterationRuleDataEPKNS_7UVectorEPKNS_9HashtableE:
.LFB2491:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_679ParseDataE(%rip), %rax
	movq	%rsi, 8(%rdi)
	movq	%rax, (%rdi)
	movq	%rdx, 16(%rdi)
	movq	%rcx, 24(%rdi)
	ret
	.cfi_endproc
.LFE2491:
	.size	_ZN6icu_679ParseDataC2EPKNS_23TransliterationRuleDataEPKNS_7UVectorEPKNS_9HashtableE, .-_ZN6icu_679ParseDataC2EPKNS_23TransliterationRuleDataEPKNS_7UVectorEPKNS_9HashtableE
	.globl	_ZN6icu_679ParseDataC1EPKNS_23TransliterationRuleDataEPKNS_7UVectorEPKNS_9HashtableE
	.set	_ZN6icu_679ParseDataC1EPKNS_23TransliterationRuleDataEPKNS_7UVectorEPKNS_9HashtableE,_ZN6icu_679ParseDataC2EPKNS_23TransliterationRuleDataEPKNS_7UVectorEPKNS_9HashtableE
	.align 2
	.p2align 4
	.globl	_ZN6icu_679ParseData9isMatcherEi
	.type	_ZN6icu_679ParseData9isMatcherEi, @function
_ZN6icu_679ParseData9isMatcherEi:
.LFB2500:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movl	$1, %r8d
	movzwl	1162(%rax), %eax
	subl	%eax, %esi
	js	.L43
	movq	16(%rdi), %rdi
	cmpl	8(%rdi), %esi
	jl	.L46
.L43:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	xorl	%r8d, %r8d
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L35
	movq	(%rax), %rax
	call	*32(%rax)
	testq	%rax, %rax
	setne	%r8b
.L35:
	movl	%r8d, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2500:
	.size	_ZN6icu_679ParseData9isMatcherEi, .-_ZN6icu_679ParseData9isMatcherEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_679ParseData10isReplacerEi
	.type	_ZN6icu_679ParseData10isReplacerEi, @function
_ZN6icu_679ParseData10isReplacerEi:
.LFB2501:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movl	$1, %r8d
	movzwl	1162(%rax), %eax
	subl	%eax, %esi
	js	.L55
	movq	16(%rdi), %rdi
	cmpl	8(%rdi), %esi
	jl	.L58
.L55:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L58:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	xorl	%r8d, %r8d
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L47
	movq	(%rax), %rax
	call	*40(%rax)
	testq	%rax, %rax
	setne	%r8b
.L47:
	movl	%r8d, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2501:
	.size	_ZN6icu_679ParseData10isReplacerEi, .-_ZN6icu_679ParseData10isReplacerEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_678RuleHalfC2ERNS_20TransliteratorParserE
	.type	_ZN6icu_678RuleHalfC2ERNS_20TransliteratorParserE, @function
_ZN6icu_678RuleHalfC2ERNS_20TransliteratorParserE:
.LFB2504:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movdqa	.LC0(%rip), %xmm0
	xorl	%edx, %edx
	movq	%rsi, 104(%rdi)
	movq	%rax, 8(%rdi)
	movl	$2, %eax
	movw	%ax, 16(%rdi)
	movl	$0, 88(%rdi)
	movw	%dx, 92(%rdi)
	movl	$1, 96(%rdi)
	movups	%xmm0, 72(%rdi)
	ret
	.cfi_endproc
.LFE2504:
	.size	_ZN6icu_678RuleHalfC2ERNS_20TransliteratorParserE, .-_ZN6icu_678RuleHalfC2ERNS_20TransliteratorParserE
	.globl	_ZN6icu_678RuleHalfC1ERNS_20TransliteratorParserE
	.set	_ZN6icu_678RuleHalfC1ERNS_20TransliteratorParserE,_ZN6icu_678RuleHalfC2ERNS_20TransliteratorParserE
	.align 2
	.p2align 4
	.globl	_ZN6icu_678RuleHalfD2Ev
	.type	_ZN6icu_678RuleHalfD2Ev, @function
_ZN6icu_678RuleHalfD2Ev:
.LFB2507:
	.cfi_startproc
	endbr64
	addq	$8, %rdi
	jmp	_ZN6icu_6713UnicodeStringD1Ev@PLT
	.cfi_endproc
.LFE2507:
	.size	_ZN6icu_678RuleHalfD2Ev, .-_ZN6icu_678RuleHalfD2Ev
	.globl	_ZN6icu_678RuleHalfD1Ev
	.set	_ZN6icu_678RuleHalfD1Ev,_ZN6icu_678RuleHalfD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_678RuleHalf13removeContextEv
	.type	_ZN6icu_678RuleHalf13removeContextEv, @function
_ZN6icu_678RuleHalf13removeContextEv:
.LFB2514:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	80(%rdi), %esi
	testl	%esi, %esi
	jns	.L67
.L62:
	movl	76(%rbx), %edx
	testl	%edx, %edx
	js	.L65
	leaq	8(%rbx), %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
.L65:
	xorl	%eax, %eax
	movq	$-1, 76(%rbx)
	movw	%ax, 92(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	.cfi_restore_state
	jne	.L63
	movzwl	16(%rdi), %edx
	movl	$2, %eax
	movl	%edx, %ecx
	andl	$31, %ecx
	andl	$1, %edx
	cmove	%ecx, %eax
	movw	%ax, 16(%rdi)
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L63:
	leaq	8(%rdi), %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$2147483647, %edx
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	jmp	.L62
	.cfi_endproc
.LFE2514:
	.size	_ZN6icu_678RuleHalf13removeContextEv, .-_ZN6icu_678RuleHalf13removeContextEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_678RuleHalf13isValidOutputERNS_20TransliteratorParserE
	.type	_ZN6icu_678RuleHalf13isValidOutputERNS_20TransliteratorParserE, @function
_ZN6icu_678RuleHalf13isValidOutputERNS_20TransliteratorParserE:
.LFB2515:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	xorl	%r14d, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	leaq	8(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	.p2align 4,,10
	.p2align 3
.L69:
	movswl	16(%rbx), %eax
	testw	%ax, %ax
	js	.L70
	sarl	$5, %eax
	cmpl	%eax, %r14d
	jge	.L77
.L80:
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	xorl	%edx, %edx
	movq	184(%r12), %rcx
	cmpl	$65535, %eax
	seta	%dl
	leal	1(%r14,%rdx), %r14d
	movq	8(%rcx), %rdx
	movzwl	1162(%rdx), %edx
	subl	%edx, %eax
	js	.L69
	movq	16(%rcx), %rdi
	cmpl	8(%rdi), %eax
	jge	.L69
	movl	%eax, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L76
	movq	(%rax), %rax
	call	*40(%rax)
	testq	%rax, %rax
	jne	.L69
.L76:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L70:
	.cfi_restore_state
	movl	20(%rbx), %eax
	cmpl	%eax, %r14d
	jl	.L80
.L77:
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2515:
	.size	_ZN6icu_678RuleHalf13isValidOutputERNS_20TransliteratorParserE, .-_ZN6icu_678RuleHalf13isValidOutputERNS_20TransliteratorParserE
	.align 2
	.p2align 4
	.globl	_ZN6icu_678RuleHalf12isValidInputERNS_20TransliteratorParserE
	.type	_ZN6icu_678RuleHalf12isValidInputERNS_20TransliteratorParserE, @function
_ZN6icu_678RuleHalf12isValidInputERNS_20TransliteratorParserE:
.LFB2516:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	xorl	%r14d, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	leaq	8(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	.p2align 4,,10
	.p2align 3
.L82:
	movswl	16(%rbx), %eax
	testw	%ax, %ax
	js	.L83
	sarl	$5, %eax
	cmpl	%eax, %r14d
	jge	.L90
.L93:
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	xorl	%edx, %edx
	movq	184(%r12), %rcx
	cmpl	$65535, %eax
	seta	%dl
	leal	1(%r14,%rdx), %r14d
	movq	8(%rcx), %rdx
	movzwl	1162(%rdx), %edx
	subl	%edx, %eax
	js	.L82
	movq	16(%rcx), %rdi
	cmpl	8(%rdi), %eax
	jge	.L82
	movl	%eax, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L89
	movq	(%rax), %rax
	call	*32(%rax)
	testq	%rax, %rax
	jne	.L82
.L89:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L83:
	.cfi_restore_state
	movl	20(%rbx), %eax
	cmpl	%eax, %r14d
	jl	.L93
.L90:
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2516:
	.size	_ZN6icu_678RuleHalf12isValidInputERNS_20TransliteratorParserE, .-_ZN6icu_678RuleHalf12isValidInputERNS_20TransliteratorParserE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720TransliteratorParserC2ER10UErrorCode
	.type	_ZN6icu_6720TransliteratorParserC2ER10UErrorCode, @function
_ZN6icu_6720TransliteratorParserC2ER10UErrorCode:
.LFB2518:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$8, %rdi
	leaq	48(%rbx), %r14
	leaq	240(%rbx), %r15
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_677UVectorC1ER10UErrorCode@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_677UVectorC1ER10UErrorCode@PLT
	leaq	192(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN6icu_677UVectorC1ER10UErrorCode@PLT
	xorl	%ecx, %ecx
	leaq	-60(%rbp), %r8
	movq	%r15, %rdi
	movq	uhash_compareUnicodeString_67@GOTPCREL(%rip), %rdx
	movq	uhash_hashUnicodeString_67@GOTPCREL(%rip), %rsi
	movq	$0, 232(%rbx)
	movl	$0, -60(%rbp)
	call	uhash_init_67@PLT
	movl	-60(%rbp), %ecx
	testl	%ecx, %ecx
	jle	.L98
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %r13
.L95:
	movl	$2, %eax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %r15
	leaq	384(%rbx), %rdi
	movq	%r12, %rsi
	movw	%ax, 328(%rbx)
	movq	%r15, 320(%rbx)
	call	_ZN6icu_677UVectorC1ER10UErrorCode@PLT
	movl	$2, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movw	%dx, 440(%rbx)
	movq	%r15, 432(%rbx)
	call	_ZN6icu_677UVector10setDeleterEPFvPvE@PLT
	pxor	%xmm0, %xmm0
	movq	232(%rbx), %rdi
	movq	%r13, %rsi
	movq	$0, 184(%rbx)
	movups	%xmm0, 88(%rbx)
	call	uhash_setValueDeleter_67@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L100
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L98:
	.cfi_restore_state
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %r13
	movq	%r15, 232(%rbx)
	movq	%r15, %rdi
	movq	%r13, %rsi
	call	uhash_setKeyDeleter_67@PLT
	jmp	.L95
.L100:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2518:
	.size	_ZN6icu_6720TransliteratorParserC2ER10UErrorCode, .-_ZN6icu_6720TransliteratorParserC2ER10UErrorCode
	.globl	_ZN6icu_6720TransliteratorParserC1ER10UErrorCode
	.set	_ZN6icu_6720TransliteratorParserC1ER10UErrorCode,_ZN6icu_6720TransliteratorParserC2ER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720TransliteratorParser20orphanCompoundFilterEv
	.type	_ZN6icu_6720TransliteratorParser20orphanCompoundFilterEv, @function
_ZN6icu_6720TransliteratorParser20orphanCompoundFilterEv:
.LFB2524:
	.cfi_startproc
	endbr64
	movq	88(%rdi), %rax
	movq	$0, 88(%rdi)
	ret
	.cfi_endproc
.LFE2524:
	.size	_ZN6icu_6720TransliteratorParser20orphanCompoundFilterEv, .-_ZN6icu_6720TransliteratorParser20orphanCompoundFilterEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720TransliteratorParser16setVariableRangeEiiR10UErrorCode
	.type	_ZN6icu_6720TransliteratorParser16setVariableRangeEiiR10UErrorCode, @function
_ZN6icu_6720TransliteratorParser16setVariableRangeEiiR10UErrorCode:
.LFB2526:
	.cfi_startproc
	endbr64
	cmpl	%edx, %esi
	movl	%esi, %eax
	setg	%r8b
	shrl	$31, %eax
	orb	%al, %r8b
	jne	.L106
	cmpl	$65535, %edx
	jg	.L106
	movq	96(%rdi), %rax
	movw	%si, 1162(%rax)
	movl	16(%rdi), %eax
	testl	%eax, %eax
	jne	.L102
	addl	$1, %edx
	movw	%si, 424(%rdi)
	movw	%dx, 426(%rdi)
.L102:
	ret
	.p2align 4,,10
	.p2align 3
.L106:
	movl	$65562, (%rcx)
	ret
	.cfi_endproc
.LFE2526:
	.size	_ZN6icu_6720TransliteratorParser16setVariableRangeEiiR10UErrorCode, .-_ZN6icu_6720TransliteratorParser16setVariableRangeEiiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6720TransliteratorParser18checkVariableRangeEi
	.type	_ZNK6icu_6720TransliteratorParser18checkVariableRangeEi, @function
_ZNK6icu_6720TransliteratorParser18checkVariableRangeEi:
.LFB2527:
	.cfi_startproc
	endbr64
	movq	96(%rdi), %rax
	movzwl	1162(%rax), %edx
	movl	$1, %eax
	cmpl	%esi, %edx
	jg	.L107
	movzwl	426(%rdi), %eax
	cmpl	%esi, %eax
	setle	%al
.L107:
	ret
	.cfi_endproc
.LFE2527:
	.size	_ZNK6icu_6720TransliteratorParser18checkVariableRangeEi, .-_ZNK6icu_6720TransliteratorParser18checkVariableRangeEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720TransliteratorParser19pragmaMaximumBackupEi
	.type	_ZN6icu_6720TransliteratorParser19pragmaMaximumBackupEi, @function
_ZN6icu_6720TransliteratorParser19pragmaMaximumBackupEi:
.LFB2528:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2528:
	.size	_ZN6icu_6720TransliteratorParser19pragmaMaximumBackupEi, .-_ZN6icu_6720TransliteratorParser19pragmaMaximumBackupEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720TransliteratorParser20pragmaNormalizeRulesE18UNormalizationMode
	.type	_ZN6icu_6720TransliteratorParser20pragmaNormalizeRulesE18UNormalizationMode, @function
_ZN6icu_6720TransliteratorParser20pragmaNormalizeRulesE18UNormalizationMode:
.LFB2529:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2529:
	.size	_ZN6icu_6720TransliteratorParser20pragmaNormalizeRulesE18UNormalizationMode, .-_ZN6icu_6720TransliteratorParser20pragmaNormalizeRulesE18UNormalizationMode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720TransliteratorParser15resemblesPragmaERKNS_13UnicodeStringEii
	.type	_ZN6icu_6720TransliteratorParser15resemblesPragmaERKNS_13UnicodeStringEii, @function
_ZN6icu_6720TransliteratorParser15resemblesPragmaERKNS_13UnicodeStringEii:
.LFB2530:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$4, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	leaq	-136(%rbp), %rdx
	.cfi_offset 14, -32
	movl	%esi, %r14d
	movl	$1, %esi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-128(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	_ZN6icu_67L10PRAGMA_USEE(%rip), %rax
	movq	%rax, -136(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movl	%r15d, %edx
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6711ICU_Utility12parsePatternERKNS_13UnicodeStringEiiS3_Pi@PLT
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	notl	%ebx
	shrl	$31, %ebx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L115
	addq	$104, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L115:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2530:
	.size	_ZN6icu_6720TransliteratorParser15resemblesPragmaERKNS_13UnicodeStringEii, .-_ZN6icu_6720TransliteratorParser15resemblesPragmaERKNS_13UnicodeStringEii
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720TransliteratorParser11parsePragmaERKNS_13UnicodeStringEiiR10UErrorCode
	.type	_ZN6icu_6720TransliteratorParser11parsePragmaERKNS_13UnicodeStringEiiR10UErrorCode, @function
_ZN6icu_6720TransliteratorParser11parsePragmaERKNS_13UnicodeStringEiiR10UErrorCode:
.LFB2531:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	movl	$-1, %ecx
	pushq	%r14
	leaq	-144(%rbp), %r10
	.cfi_offset 14, -32
	leal	4(%rdx), %r14d
	pushq	%r13
	movq	%r10, %rdx
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movl	$1, %esi
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-128(%rbp), %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -168(%rbp)
	movq	%r12, %rdi
	movq	%r8, -176(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	_ZN6icu_67L21PRAGMA_VARIABLE_RANGEE(%rip), %rax
	movq	%r10, -160(%rbp)
	movq	%rax, -144(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	%r12, %rcx
	movl	%r15d, %edx
	movl	%r14d, %esi
	leaq	-136(%rbp), %r8
	movq	%r13, %rdi
	movq	%r8, -152(%rbp)
	call	_ZN6icu_6711ICU_Utility12parsePatternERKNS_13UnicodeStringEiiS3_Pi@PLT
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	testl	%ebx, %ebx
	movq	-152(%rbp), %r8
	movq	-160(%rbp), %r10
	js	.L117
	movl	-136(%rbp), %edx
	movl	-132(%rbp), %ecx
	movl	%edx, %esi
	cmpl	%edx, %ecx
	setl	%dil
	shrl	$31, %esi
	orb	%sil, %dil
	jne	.L122
	cmpl	$65535, %ecx
	movq	-168(%rbp), %r9
	jg	.L122
	movq	96(%r9), %rsi
	movl	16(%r9), %eax
	movw	%dx, 1162(%rsi)
	testl	%eax, %eax
	jne	.L116
	addl	$1, %ecx
	movw	%dx, 424(%r9)
	movw	%cx, 426(%r9)
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L122:
	movq	-176(%rbp), %rax
	movl	$65562, (%rax)
.L116:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L124
	addq	$136, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L117:
	.cfi_restore_state
	movq	%r10, %rdx
	movq	%r12, %rdi
	movl	$-1, %ecx
	movl	$1, %esi
	leaq	_ZN6icu_67L21PRAGMA_MAXIMUM_BACKUPE(%rip), %rax
	movq	%r10, -152(%rbp)
	movq	%r8, -160(%rbp)
	movq	%rax, -144(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	%r12, %rcx
	movl	%r15d, %edx
	movl	%r14d, %esi
	movq	-160(%rbp), %r8
	movq	%r13, %rdi
	call	_ZN6icu_6711ICU_Utility12parsePatternERKNS_13UnicodeStringEiiS3_Pi@PLT
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	testl	%ebx, %ebx
	movq	-152(%rbp), %r10
	jns	.L116
	movq	%r10, %rdx
	movq	%r12, %rdi
	movl	$-1, %ecx
	movl	$1, %esi
	leaq	_ZN6icu_67L16PRAGMA_NFD_RULESE(%rip), %rax
	movq	%rax, -144(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movl	%r15d, %edx
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6711ICU_Utility12parsePatternERKNS_13UnicodeStringEiiS3_Pi@PLT
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	testl	%ebx, %ebx
	movq	-152(%rbp), %r10
	jns	.L116
	movq	%r10, %rdx
	movq	%r12, %rdi
	movl	$-1, %ecx
	movl	$1, %esi
	leaq	_ZN6icu_67L16PRAGMA_NFC_RULESE(%rip), %rax
	movq	%rax, -144(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movl	%r15d, %edx
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6711ICU_Utility12parsePatternERKNS_13UnicodeStringEiiS3_Pi@PLT
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	$-1, %eax
	testl	%ebx, %ebx
	cmovs	%eax, %ebx
	jmp	.L116
.L124:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2531:
	.size	_ZN6icu_6720TransliteratorParser11parsePragmaERKNS_13UnicodeStringEiiR10UErrorCode, .-_ZN6icu_6720TransliteratorParser11parsePragmaERKNS_13UnicodeStringEiiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720TransliteratorParser11syntaxErrorE10UErrorCodeRKNS_13UnicodeStringEiRS1_
	.type	_ZN6icu_6720TransliteratorParser11syntaxErrorE10UErrorCodeRKNS_13UnicodeStringEiRS1_, @function
_ZN6icu_6720TransliteratorParser11syntaxErrorE10UErrorCodeRKNS_13UnicodeStringEiRS1_:
.LFB2533:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%esi, %r15d
	xorl	%esi, %esi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%ecx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	%ecx, 112(%rdi)
	movl	$0, 108(%rdi)
	leal	-15(%rcx), %edi
	call	uprv_max_67@PLT
	movl	%r12d, %edx
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	movl	%eax, %esi
	leaq	116(%rbx), %rax
	subl	%esi, %edx
	movq	%rax, %rcx
	movq	%rax, -64(%rbp)
	movl	%edx, -52(%rbp)
	call	_ZNK6icu_6713UnicodeString9doExtractEiiPDsi@PLT
	movq	-64(%rbp), %rax
	movslq	-52(%rbp), %rdx
	xorl	%ecx, %ecx
	movw	%cx, 116(%rbx,%rdx,2)
	movswl	8(%r13), %esi
	testw	%si, %si
	js	.L126
	sarl	$5, %esi
.L127:
	leal	15(%r12), %edi
	call	uprv_min_67@PLT
	xorl	%r8d, %r8d
	movl	%r12d, %esi
	movq	%r13, %rdi
	subl	%r12d, %eax
	leaq	148(%rbx), %rcx
	movl	%eax, %edx
	movq	%rcx, -64(%rbp)
	movl	%eax, -52(%rbp)
	call	_ZNK6icu_6713UnicodeString9doExtractEiiPDsi@PLT
	movq	-64(%rbp), %rcx
	movslq	-52(%rbp), %rax
	xorl	%edx, %edx
	movw	%dx, 148(%rbx,%rax,2)
	movl	%r12d, %eax
	movl	%r15d, (%r14)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L126:
	.cfi_restore_state
	movl	12(%r13), %esi
	jmp	.L127
	.cfi_endproc
.LFE2533:
	.size	_ZN6icu_6720TransliteratorParser11syntaxErrorE10UErrorCodeRKNS_13UnicodeStringEiRS1_, .-_ZN6icu_6720TransliteratorParser11syntaxErrorE10UErrorCodeRKNS_13UnicodeStringEiRS1_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720TransliteratorParser18generateStandInForEPNS_14UnicodeFunctorER10UErrorCode
	.type	_ZN6icu_6720TransliteratorParser18generateStandInForEPNS_14UnicodeFunctorER10UErrorCode, @function
_ZN6icu_6720TransliteratorParser18generateStandInForEPNS_14UnicodeFunctorER10UErrorCode:
.LFB2535:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	192(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$8, %rsp
	movl	200(%rdi), %eax
	testl	%eax, %eax
	jg	.L135
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L133:
	addl	$1, %ebx
	cmpl	200(%r12), %ebx
	jge	.L136
.L135:
	movl	%ebx, %esi
	movq	%r14, %rdi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	cmpq	%rax, %r13
	jne	.L133
	movq	96(%r12), %rax
	movzwl	1162(%rax), %ecx
	addq	$8, %rsp
	addl	%ebx, %ecx
	popq	%rbx
	popq	%r12
	movl	%ecx, %eax
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L136:
	.cfi_restore_state
	movzwl	426(%r12), %eax
	cmpw	%ax, 424(%r12)
	jb	.L146
	testq	%r13, %r13
	je	.L137
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*8(%rax)
.L137:
	movl	$65565, (%r15)
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L146:
	.cfi_restore_state
	movq	%r15, %rdx
	leaq	192(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	movzwl	424(%r12), %eax
	leal	1(%rax), %edx
	movw	%dx, 424(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2535:
	.size	_ZN6icu_6720TransliteratorParser18generateStandInForEPNS_14UnicodeFunctorER10UErrorCode, .-_ZN6icu_6720TransliteratorParser18generateStandInForEPNS_14UnicodeFunctorER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720TransliteratorParser17getSegmentStandinEiR10UErrorCode
	.type	_ZN6icu_6720TransliteratorParser17getSegmentStandinEiR10UErrorCode, @function
_ZN6icu_6720TransliteratorParser17getSegmentStandinEiR10UErrorCode:
.LFB2536:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-58(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	96(%rdi), %rax
	movzwl	1162(%rax), %r12d
	subl	$1, %r12d
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L160:
	movswl	%dx, %eax
	sarl	$5, %eax
	cmpl	%eax, %ebx
	jle	.L150
.L149:
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movw	%r12w, -58(%rbp)
	leaq	320(%r15), %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
.L151:
	movzwl	328(%r15), %edx
	testw	%dx, %dx
	jns	.L160
	movl	332(%r15), %eax
	cmpl	%eax, %ebx
	jg	.L149
.L150:
	subl	$1, %ebx
	movl	$-1, %r13d
	cmpl	%ebx, %eax
	jbe	.L152
	andl	$2, %edx
	leaq	330(%r15), %rax
	je	.L161
.L154:
	movslq	%ebx, %rdx
	movzwl	(%rax,%rdx,2), %r13d
.L152:
	cmpw	%r13w, %r12w
	je	.L162
.L147:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L163
	addq	$24, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L161:
	.cfi_restore_state
	movq	344(%r15), %rax
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L162:
	movzwl	424(%r15), %r13d
	cmpw	426(%r15), %r13w
	jb	.L156
	movl	$65565, (%r14)
	xorl	%r13d, %r13d
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L156:
	leal	1(%r13), %eax
	leaq	192(%r15), %rdi
	movq	%r14, %rdx
	xorl	%esi, %esi
	movw	%ax, 424(%r15)
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	movzwl	%r13w, %edx
	leaq	320(%r15), %rdi
	movl	%ebx, %esi
	call	_ZN6icu_6713UnicodeString9setCharAtEiDs@PLT
	jmp	.L147
.L163:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2536:
	.size	_ZN6icu_6720TransliteratorParser17getSegmentStandinEiR10UErrorCode, .-_ZN6icu_6720TransliteratorParser17getSegmentStandinEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720TransliteratorParser16setSegmentObjectEiPNS_13StringMatcherER10UErrorCode
	.type	_ZN6icu_6720TransliteratorParser16setSegmentObjectEiPNS_13StringMatcherER10UErrorCode, @function
_ZN6icu_6720TransliteratorParser16setSegmentObjectEiPNS_13StringMatcherER10UErrorCode:
.LFB2537:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	384(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$24, %rsp
	movq	%rdx, -64(%rbp)
	cmpl	392(%rdi), %esi
	jg	.L171
.L165:
	movl	%ebx, %esi
	movq	%r13, %rdi
	movq	%r15, %rdx
	subl	$1, %ebx
	call	_ZN6icu_6720TransliteratorParser17getSegmentStandinEiR10UErrorCode
	movl	%ebx, %esi
	movq	%r14, %rdi
	movzwl	%ax, %r12d
	movq	96(%r13), %rax
	movzwl	1162(%rax), %eax
	movw	%ax, -50(%rbp)
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	testq	%rax, %rax
	je	.L166
.L168:
	movl	$65568, (%r15)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L171:
	.cfi_restore_state
	movq	%rcx, %rdx
	movq	%r14, %rdi
	call	_ZN6icu_677UVector7setSizeEiR10UErrorCode@PLT
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L166:
	movzwl	-50(%rbp), %eax
	addq	$192, %r13
	movq	%r13, %rdi
	subl	%eax, %r12d
	movl	%r12d, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	testq	%rax, %rax
	jne	.L168
	movl	%ebx, %edx
	movq	-64(%rbp), %rbx
	movq	%r14, %rdi
	movq	%rbx, %rsi
	call	_ZN6icu_677UVector12setElementAtEPvi@PLT
	addq	$24, %rsp
	movl	%r12d, %edx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UVector12setElementAtEPvi@PLT
	.cfi_endproc
.LFE2537:
	.size	_ZN6icu_6720TransliteratorParser16setSegmentObjectEiPNS_13StringMatcherER10UErrorCode, .-_ZN6icu_6720TransliteratorParser16setSegmentObjectEiPNS_13StringMatcherER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720TransliteratorParser13getDotStandInER10UErrorCode
	.type	_ZN6icu_6720TransliteratorParser13getDotStandInER10UErrorCode, @function
_ZN6icu_6720TransliteratorParser13getDotStandInER10UErrorCode:
.LFB2538:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzwl	496(%rdi), %eax
	cmpw	$-1, %ax
	je	.L192
.L172:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L193
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L192:
	.cfi_restore_state
	leaq	-128(%rbp), %r15
	movl	$-1, %ecx
	movq	%rdi, %r12
	movq	%rsi, %r13
	leaq	_ZL7DOT_SET(%rip), %rax
	leaq	-136(%rbp), %rdx
	movl	$1, %esi
	movq	%r15, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movl	$200, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L174
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSetC1ERKNS_13UnicodeStringER10UErrorCode@PLT
.L174:
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	testq	%r14, %r14
	je	.L194
	movl	200(%r12), %eax
	leaq	192(%r12), %r15
	xorl	%ebx, %ebx
	testl	%eax, %eax
	jg	.L182
	.p2align 4,,10
	.p2align 3
.L183:
	movzwl	426(%r12), %eax
	cmpw	%ax, 424(%r12)
	jb	.L195
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*8(%rax)
	movl	$65565, 0(%r13)
	xorl	%eax, %eax
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L180:
	addl	$1, %ebx
	cmpl	%ebx, 200(%r12)
	jle	.L183
.L182:
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	cmpq	%rax, %r14
	jne	.L180
	movq	96(%r12), %rax
	movzwl	1162(%rax), %edx
	addl	%ebx, %edx
	movl	%edx, %eax
.L181:
	movw	%ax, 496(%r12)
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L195:
	movq	%r13, %rdx
	leaq	192(%r12), %rdi
	movq	%r14, %rsi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	movzwl	424(%r12), %eax
	leal	1(%rax), %edx
	movw	%dx, 424(%r12)
	jmp	.L181
.L193:
	call	__stack_chk_fail@PLT
.L194:
	movl	$7, 0(%r13)
	xorl	%eax, %eax
	jmp	.L172
	.cfi_endproc
.LFE2538:
	.size	_ZN6icu_6720TransliteratorParser13getDotStandInER10UErrorCode, .-_ZN6icu_6720TransliteratorParser13getDotStandInER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720TransliteratorParser17appendVariableDefERKNS_13UnicodeStringERS1_R10UErrorCode
	.type	_ZN6icu_6720TransliteratorParser17appendVariableDefERKNS_13UnicodeStringERS1_R10UErrorCode, @function
_ZN6icu_6720TransliteratorParser17appendVariableDefERKNS_13UnicodeStringERS1_R10UErrorCode:
.LFB2539:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	232(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	uhash_get_67@PLT
	testq	%rax, %rax
	je	.L207
	movswl	8(%rax), %ecx
	movq	%rax, %rsi
	testw	%cx, %cx
	js	.L203
	sarl	$5, %ecx
.L204:
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
.L196:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L208
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L203:
	.cfi_restore_state
	movl	12(%rax), %ecx
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L207:
	movswl	440(%rbx), %eax
	testw	%ax, %ax
	js	.L198
	sarl	$5, %eax
	testl	%eax, %eax
	jne	.L200
.L209:
	leaq	432(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movzwl	426(%rbx), %eax
	cmpw	%ax, 424(%rbx)
	jnb	.L200
	subl	$1, %eax
	leaq	-42(%rbp), %rsi
	movl	$1, %ecx
	xorl	%edx, %edx
	movw	%ax, 426(%rbx)
	movq	%r12, %rdi
	movw	%ax, -42(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L198:
	movl	444(%rbx), %eax
	testl	%eax, %eax
	je	.L209
.L200:
	movl	$1, 0(%r13)
	jmp	.L196
.L208:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2539:
	.size	_ZN6icu_6720TransliteratorParser17appendVariableDefERKNS_13UnicodeStringERS1_R10UErrorCode, .-_ZN6icu_6720TransliteratorParser17appendVariableDefERKNS_13UnicodeStringERS1_R10UErrorCode
	.p2align 4
	.globl	utrans_stripRules_67
	.type	utrans_stripRules_67, @function
utrans_stripRules_67:
.LFB2540:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movslq	%esi, %r13
	addl	%esi, %esi
	pushq	%r12
	addq	%r13, %r13
	.cfi_offset 12, -48
	movq	%r14, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$152, %rsp
	movq	%rcx, -168(%rbp)
	leaq	(%rdi,%r13), %rcx
	movq	%r14, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	(%rdx,%r13), %rax
	movslq	%esi, %rdx
	xorl	%esi, %esi
	movq	%rcx, -160(%rbp)
	movq	%rax, -152(%rbp)
	call	memset@PLT
	movq	-160(%rbp), %rcx
	cmpq	%rcx, %rbx
	jnb	.L246
	movabsq	$4294976512, %r13
	xorl	%r15d, %r15d
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L214:
	testb	%r15b, %r15b
	jne	.L216
	cmpl	$35, %edx
	je	.L266
	cmpl	$92, %edx
	jne	.L216
	cmpq	%rbx, %rcx
	jbe	.L216
	movzwl	(%rbx), %eax
	cmpl	$13, %eax
	je	.L252
	cmpl	$10, %eax
	je	.L252
	cmpl	$117, %eax
	je	.L272
.L229:
	cmpl	$39, %eax
	movl	$92, %esi
	sete	%r15b
	.p2align 4,,10
	.p2align 3
.L243:
	movl	$2, %eax
.L241:
	movw	%si, (%r12)
	addq	%rax, %r12
.L228:
	cmpq	%rcx, %rbx
	jnb	.L267
.L212:
	movzwl	(%rbx), %edx
	movl	$2, %eax
	movl	%edx, %esi
	andl	$-1024, %esi
	cmpl	$55296, %esi
	jne	.L213
	movzwl	2(%rbx), %esi
	sall	$10, %edx
	movl	$4, %eax
	leal	-56613888(%rdx,%rsi), %edx
.L213:
	addq	%rax, %rbx
	cmpl	$39, %edx
	jne	.L214
	xorl	$1, %r15d
	movl	$39, %esi
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L219:
	cmpw	$32, -2(%r12)
	jne	.L222
	subq	$2, %r12
.L266:
	cmpq	%r12, %r14
	jb	.L219
	cmpq	%rcx, %rbx
	je	.L267
	.p2align 4,,10
	.p2align 3
.L220:
	movzwl	(%rbx), %eax
	addq	$2, %rbx
	cmpl	$13, %eax
	je	.L271
	cmpl	$10, %eax
	je	.L271
.L222:
	cmpq	%rcx, %rbx
	jne	.L220
.L267:
	movq	%r12, %rax
	subq	%r14, %rax
	sarq	%rax
.L211:
	cmpq	-152(%rbp), %r12
	jb	.L273
.L210:
	movq	-56(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L274
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L275:
	.cfi_restore_state
	btq	%rax, %r13
	jnc	.L250
	addq	$2, %rbx
.L271:
	cmpq	%rbx, %rcx
	jbe	.L267
.L239:
	movzwl	(%rbx), %eax
	cmpw	$32, %ax
	jbe	.L275
.L250:
	xorl	%r15d, %r15d
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L216:
	cmpl	$13, %edx
	sete	%al
	cmpl	$10, %edx
	sete	%sil
	orl	%esi, %eax
	testb	%al, %al
	jne	.L271
.L238:
	cmpl	$65535, %edx
	jbe	.L244
	movl	%edx, %esi
	andw	$1023, %dx
	movl	$4, %eax
	sarl	$10, %esi
	orw	$-9216, %dx
	movw	%dx, 2(%r12)
	subw	$10304, %si
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L252:
	addq	$2, %rbx
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L272:
	leaq	10(%rbx), %r11
	cmpq	%rcx, %r11
	jnb	.L229
	leaq	-128(%rbp), %rax
	movq	%rbx, %rsi
	movl	$5, %edx
	movq	%rcx, -184(%rbp)
	movq	%rax, %rdi
	movq	%r11, -176(%rbp)
	movl	$0, -132(%rbp)
	movq	%rax, -160(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EPKDsi@PLT
	movq	-160(%rbp), %rdi
	leaq	-132(%rbp), %rsi
	call	_ZNK6icu_6713UnicodeString10unescapeAtERi@PLT
	cmpl	$-1, %eax
	je	.L230
	movl	-132(%rbp), %ecx
	movq	-176(%rbp), %r11
	testl	%ecx, %ecx
	movq	-184(%rbp), %rcx
	je	.L230
	movl	%eax, %edi
	movq	%rcx, -192(%rbp)
	movq	%r11, -184(%rbp)
	movl	%eax, -176(%rbp)
	call	_ZN6icu_6712PatternProps12isWhiteSpaceEi@PLT
	movl	-176(%rbp), %edx
	movq	-184(%rbp), %r11
	testb	%al, %al
	movq	-192(%rbp), %rcx
	je	.L233
.L269:
	movq	-160(%rbp), %rdi
	movq	%rcx, -176(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-176(%rbp), %rcx
	movl	$92, %edx
.L244:
	movl	%edx, %esi
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L273:
	xorl	%edx, %edx
	movw	%dx, (%r12)
	jmp	.L210
.L233:
	movl	%edx, %edi
	movq	%rcx, -192(%rbp)
	movq	%r11, -184(%rbp)
	movl	%edx, -176(%rbp)
	call	u_iscntrl_67@PLT
	movl	-176(%rbp), %edx
	movq	-184(%rbp), %r11
	testb	%al, %al
	movq	-192(%rbp), %rcx
	jne	.L269
	movl	%edx, %edi
	movq	%rcx, -192(%rbp)
	movq	%r11, -184(%rbp)
	movl	%edx, -176(%rbp)
	call	u_ispunct_67@PLT
	movl	-176(%rbp), %edx
	movq	-184(%rbp), %r11
	testb	%al, %al
	movq	-192(%rbp), %rcx
	jne	.L269
	cmpl	$13, %edx
	movq	-160(%rbp), %rdi
	movq	%rcx, -192(%rbp)
	sete	%bl
	cmpl	$10, %edx
	movq	%r11, -184(%rbp)
	sete	%al
	movl	%edx, -176(%rbp)
	orl	%eax, %ebx
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	testb	%bl, %bl
	movq	-184(%rbp), %r11
	movq	-192(%rbp), %rcx
	jne	.L248
	movl	-176(%rbp), %edx
	xorl	%r15d, %r15d
	movq	%r11, %rbx
	jmp	.L238
.L246:
	xorl	%eax, %eax
	jmp	.L211
.L230:
	movq	-168(%rbp), %rax
	movq	-160(%rbp), %rdi
	movl	$9, (%rax)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	xorl	%eax, %eax
	jmp	.L210
.L248:
	movq	%r11, %rbx
	jmp	.L239
.L274:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2540:
	.size	utrans_stripRules_67, .-utrans_stripRules_67
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720TransliteratorParserD2Ev
	.type	_ZN6icu_6720TransliteratorParserD2Ev, @function
_ZN6icu_6720TransliteratorParserD2Ev:
.LFB2521:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	8(%rdi), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	jmp	.L278
	.p2align 4,,10
	.p2align 3
.L302:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN6icu_677UVector15orphanElementAtEi@PLT
	testq	%rax, %rax
	jne	.L301
.L278:
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jne	.L302
	movq	88(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L280
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
.L280:
	movq	184(%rbx), %r12
	testq	%r12, %r12
	je	.L281
	movq	(%r12), %rax
	leaq	_ZN6icu_679ParseDataD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L282
	leaq	16+_ZTVN6icu_679ParseDataE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	call	_ZN6icu_6711SymbolTableD2Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L281:
	leaq	192(%rbx), %r12
	jmp	.L285
	.p2align 4,,10
	.p2align 3
.L304:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN6icu_677UVector15orphanElementAtEi@PLT
	testq	%rax, %rax
	jne	.L303
.L285:
	movl	200(%rbx), %eax
	testl	%eax, %eax
	jne	.L304
	leaq	432(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	384(%rbx), %rdi
	call	_ZN6icu_677UVectorD1Ev@PLT
	leaq	320(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	232(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L286
	call	uhash_close_67@PLT
.L286:
	movq	%r12, %rdi
	call	_ZN6icu_677UVectorD1Ev@PLT
	leaq	48(%rbx), %rdi
	call	_ZN6icu_677UVectorD1Ev@PLT
	addq	$24, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UVectorD1Ev@PLT
	.p2align 4,,10
	.p2align 3
.L301:
	.cfi_restore_state
	movq	%rax, %rdi
	movq	%rax, -40(%rbp)
	call	_ZN6icu_6723TransliterationRuleDataD1Ev@PLT
	movq	-40(%rbp), %rax
	movq	%rax, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	jmp	.L278
	.p2align 4,,10
	.p2align 3
.L303:
	movq	(%rax), %rdx
	movq	%rax, %rdi
	call	*8(%rdx)
	jmp	.L285
.L282:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L281
	.cfi_endproc
.LFE2521:
	.size	_ZN6icu_6720TransliteratorParserD2Ev, .-_ZN6icu_6720TransliteratorParserD2Ev
	.globl	_ZN6icu_6720TransliteratorParserD1Ev
	.set	_ZN6icu_6720TransliteratorParserD1Ev,_ZN6icu_6720TransliteratorParserD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720TransliteratorParser8parseSetERKNS_13UnicodeStringERNS_13ParsePositionER10UErrorCode
	.type	_ZN6icu_6720TransliteratorParser8parseSetERKNS_13UnicodeStringERNS_13ParsePositionER10UErrorCode, @function
_ZN6icu_6720TransliteratorParser8parseSetERKNS_13UnicodeStringERNS_13ParsePositionER10UErrorCode:
.LFB2534:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movl	$200, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L306
	movq	184(%rbx), %r8
	movq	%rax, %r12
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%r14, %r9
	movl	$1, %ecx
	xorl	%r13d, %r13d
	call	_ZN6icu_6710UnicodeSetC1ERKNS_13UnicodeStringERNS_13ParsePositionEjPKNS_11SymbolTableER10UErrorCode@PLT
	movq	%r12, %rdi
	leaq	192(%rbx), %r15
	call	_ZN6icu_6710UnicodeSet7compactEv@PLT
	movl	200(%rbx), %eax
	testl	%eax, %eax
	jg	.L312
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L310:
	addl	$1, %r13d
	cmpl	%r13d, 200(%rbx)
	jle	.L313
.L312:
	movl	%r13d, %esi
	movq	%r15, %rdi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	cmpq	%rax, %r12
	jne	.L310
	movq	96(%rbx), %rax
	movzwl	1162(%rax), %ecx
	addl	%r13d, %ecx
	movl	%ecx, %eax
.L305:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L313:
	.cfi_restore_state
	movzwl	426(%rbx), %eax
	cmpw	%ax, 424(%rbx)
	jb	.L318
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
	movl	$65565, (%r14)
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L318:
	.cfi_restore_state
	movq	%r14, %rdx
	leaq	192(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	movzwl	424(%rbx), %eax
	leal	1(%rax), %edx
	movw	%dx, 424(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L306:
	.cfi_restore_state
	movl	$7, (%r14)
	xorl	%eax, %eax
	jmp	.L305
	.cfi_endproc
.LFE2534:
	.size	_ZN6icu_6720TransliteratorParser8parseSetERKNS_13UnicodeStringERNS_13ParsePositionER10UErrorCode, .-_ZN6icu_6720TransliteratorParser8parseSetERKNS_13UnicodeStringERNS_13ParsePositionER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_678RuleHalf12parseSectionERKNS_13UnicodeStringEiiRS1_S3_aR10UErrorCode
	.type	_ZN6icu_678RuleHalf12parseSectionERKNS_13UnicodeStringEiiRS1_S3_aR10UErrorCode, @function
_ZN6icu_678RuleHalf12parseSectionERKNS_13UnicodeStringEiiRS1_S3_aR10UErrorCode:
.LFB2510:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6713ParsePositionE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	$2, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$312, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -240(%rbp)
	movl	16(%rbp), %edi
	movq	24(%rbp), %rsi
	movl	%edx, -228(%rbp)
	movl	%ecx, -232(%rbp)
	movq	%r8, -256(%rbp)
	movq	%r9, -288(%rbp)
	movl	%edi, -308(%rbp)
	movq	%rsi, -264(%rbp)
	movb	%dil, -301(%rbp)
	movq	%fs:40, %rdi
	movq	%rdi, -56(%rbp)
	xorl	%edi, %edi
	movq	%rax, -208(%rbp)
	movabsq	$-4294967296, %rax
	movq	%rax, -200(%rbp)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, -192(%rbp)
	movswl	8(%r8), %eax
	movl	%edx, -244(%rbp)
	movw	%r15w, -184(%rbp)
	testw	%ax, %ax
	js	.L320
	sarl	$5, %eax
	movl	%eax, -320(%rbp)
.L321:
	leaq	-208(%rbp), %rsi
	leaq	10(%r12), %rbx
	xorl	%r15d, %r15d
	movl	$-1, -316(%rbp)
	leaq	-192(%rbp), %rdi
	movl	-244(%rbp), %eax
	movl	$-1, -312(%rbp)
	movl	$-1, -300(%rbp)
	movl	$-1, -248(%rbp)
	movq	%rsi, -280(%rbp)
	movq	%rdi, -296(%rbp)
	movq	%rbx, -272(%rbp)
.L330:
	cmpl	%eax, -232(%rbp)
	jle	.L323
.L598:
	andl	$1, %r15d
	jne	.L323
	leal	1(%rax), %edx
	movl	%edx, -228(%rbp)
	movzwl	8(%r12), %edx
	testw	%dx, %dx
	js	.L324
	movswl	%dx, %ecx
	sarl	$5, %ecx
	cmpl	%eax, %ecx
	jbe	.L526
.L606:
	andl	$2, %edx
	je	.L327
	movq	-272(%rbp), %rdx
.L328:
	cltq
	movzwl	(%rdx,%rax,2), %r13d
	movl	%r13d, %ebx
.L326:
	movl	%r13d, %edi
	call	_ZN6icu_6712PatternProps12isWhiteSpaceEi@PLT
	testb	%al, %al
	je	.L329
	movl	-228(%rbp), %eax
	xorl	%r15d, %r15d
	cmpl	%eax, -232(%rbp)
	jg	.L598
.L323:
	movl	%eax, -244(%rbp)
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L329:
	movl	%r13d, %esi
	leaq	_ZL11HALF_ENDERS(%rip), %rdi
	call	u_strchr_67@PLT
	testq	%rax, %rax
	jne	.L599
	movq	-240(%rbp), %rax
	cmpb	$0, 93(%rax)
	jne	.L600
	movl	-228(%rbp), %eax
	movq	%r12, %rdi
	leal	-1(%rax), %esi
	call	_ZN6icu_6710UnicodeSet16resemblesPatternERKNS_13UnicodeStringEi@PLT
	movl	%eax, %r15d
	testb	%al, %al
	jne	.L601
	cmpw	$92, %bx
	je	.L602
	cmpw	$39, %bx
	je	.L603
	movq	-240(%rbp), %rax
	movq	104(%rax), %r14
	movq	96(%r14), %rax
	movzwl	1162(%rax), %eax
	cmpl	%r13d, %eax
	jle	.L604
.L397:
	movq	-288(%rbp), %rax
	movswl	8(%rax), %ecx
	testw	%cx, %cx
	js	.L400
	sarl	$5, %ecx
	xorl	%edx, %edx
	movq	%rax, %rdi
	movl	%r13d, %esi
	call	_ZNK6icu_6713UnicodeString9doIndexOfEDsii@PLT
	testl	%eax, %eax
	jns	.L605
.L402:
	cmpw	$125, %bx
	ja	.L405
	cmpw	$35, %bx
	jbe	.L406
	leal	-36(%rbx), %eax
	cmpw	$89, %ax
	ja	.L406
	leaq	.L408(%rip), %rdx
	movzwl	%ax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L408:
	.long	.L418-.L408
	.long	.L406-.L408
	.long	.L417-.L408
	.long	.L406-.L408
	.long	.L416-.L408
	.long	.L415-.L408
	.long	.L413-.L408
	.long	.L413-.L408
	.long	.L406-.L408
	.long	.L406-.L408
	.long	.L414-.L408
	.long	.L406-.L408
	.long	.L406-.L408
	.long	.L406-.L408
	.long	.L406-.L408
	.long	.L406-.L408
	.long	.L406-.L408
	.long	.L406-.L408
	.long	.L406-.L408
	.long	.L406-.L408
	.long	.L406-.L408
	.long	.L406-.L408
	.long	.L406-.L408
	.long	.L406-.L408
	.long	.L406-.L408
	.long	.L406-.L408
	.long	.L406-.L408
	.long	.L413-.L408
	.long	.L412-.L408
	.long	.L406-.L408
	.long	.L406-.L408
	.long	.L406-.L408
	.long	.L406-.L408
	.long	.L406-.L408
	.long	.L406-.L408
	.long	.L406-.L408
	.long	.L406-.L408
	.long	.L406-.L408
	.long	.L406-.L408
	.long	.L406-.L408
	.long	.L406-.L408
	.long	.L406-.L408
	.long	.L406-.L408
	.long	.L406-.L408
	.long	.L406-.L408
	.long	.L406-.L408
	.long	.L406-.L408
	.long	.L406-.L408
	.long	.L406-.L408
	.long	.L406-.L408
	.long	.L406-.L408
	.long	.L406-.L408
	.long	.L406-.L408
	.long	.L406-.L408
	.long	.L406-.L408
	.long	.L406-.L408
	.long	.L406-.L408
	.long	.L406-.L408
	.long	.L411-.L408
	.long	.L406-.L408
	.long	.L406-.L408
	.long	.L406-.L408
	.long	.L406-.L408
	.long	.L406-.L408
	.long	.L406-.L408
	.long	.L406-.L408
	.long	.L406-.L408
	.long	.L406-.L408
	.long	.L406-.L408
	.long	.L406-.L408
	.long	.L406-.L408
	.long	.L406-.L408
	.long	.L406-.L408
	.long	.L406-.L408
	.long	.L406-.L408
	.long	.L406-.L408
	.long	.L406-.L408
	.long	.L406-.L408
	.long	.L406-.L408
	.long	.L406-.L408
	.long	.L406-.L408
	.long	.L406-.L408
	.long	.L406-.L408
	.long	.L406-.L408
	.long	.L406-.L408
	.long	.L406-.L408
	.long	.L406-.L408
	.long	.L410-.L408
	.long	.L409-.L408
	.long	.L407-.L408
	.text
	.p2align 4,,10
	.p2align 3
.L324:
	movl	12(%r12), %ecx
	cmpl	%eax, %ecx
	ja	.L606
.L526:
	movl	$65535, %r13d
	movl	$-1, %ebx
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L327:
	movq	24(%r12), %rdx
	jmp	.L328
	.p2align 4,,10
	.p2align 3
.L601:
	movl	-228(%rbp), %eax
	movl	$200, %edi
	subl	$1, %eax
	movl	%eax, -200(%rbp)
	movq	-240(%rbp), %rax
	movq	104(%rax), %rbx
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L340
	movq	184(%rbx), %r8
	movq	%rax, %rdi
	movq	%r12, %rsi
	xorl	%r15d, %r15d
	movq	-264(%rbp), %r9
	movq	-280(%rbp), %rdx
	movl	$1, %ecx
	leaq	192(%rbx), %r14
	call	_ZN6icu_6710UnicodeSetC1ERKNS_13UnicodeStringERNS_13ParsePositionEjPKNS_11SymbolTableER10UErrorCode@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSet7compactEv@PLT
	movl	200(%rbx), %r8d
	testl	%r8d, %r8d
	jg	.L346
	jmp	.L347
	.p2align 4,,10
	.p2align 3
.L344:
	addl	$1, %r15d
	cmpl	%r15d, 200(%rbx)
	jle	.L347
.L346:
	movl	%r15d, %esi
	movq	%r14, %rdi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	cmpq	%rax, %r13
	jne	.L344
	movq	96(%rbx), %rax
	addw	1162(%rax), %r15w
.L345:
	movq	-256(%rbp), %rdi
	leaq	-216(%rbp), %rsi
	movl	$1, %ecx
	xorl	%edx, %edx
	movw	%r15w, -216(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movq	-264(%rbp), %rax
	movl	(%rax), %edi
	testl	%edi, %edi
	jle	.L348
	movq	-240(%rbp), %rax
	movl	-244(%rbp), %r15d
	xorl	%esi, %esi
	movq	104(%rax), %rbx
	leal	-15(%r15), %edi
	movl	%r15d, 112(%rbx)
	leaq	116(%rbx), %r14
	movl	$0, 108(%rbx)
	call	uprv_max_67@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	%r12, %rdi
	subl	%eax, %r15d
	movl	%eax, %esi
	movslq	%r15d, %r13
	movl	%r13d, %edx
	call	_ZNK6icu_6713UnicodeString9doExtractEiiPDsi@PLT
	xorl	%esi, %esi
	movw	%si, 116(%rbx,%r13,2)
	movzwl	8(%r12), %eax
	testw	%ax, %ax
	js	.L349
	movswl	%ax, %esi
	sarl	$5, %esi
.L350:
	movl	-244(%rbp), %r15d
	leaq	148(%rbx), %r14
	leal	15(%r15), %edi
	call	uprv_min_67@PLT
	movl	%r15d, %esi
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	subl	%r15d, %eax
	movq	%r12, %rdi
	movslq	%eax, %r13
	movl	%r13d, %edx
	call	_ZNK6icu_6713UnicodeString9doExtractEiiPDsi@PLT
	movq	-264(%rbp), %rax
	xorl	%ecx, %ecx
	movw	%cx, 148(%rbx,%r13,2)
	movl	$65538, (%rax)
.L335:
	movq	-296(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-280(%rbp), %rdi
	call	_ZN6icu_6713ParsePositionD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L607
	movl	-244(%rbp), %eax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L604:
	.cfi_restore_state
	movzwl	426(%r14), %eax
	cmpl	%r13d, %eax
	jle	.L397
	movl	-244(%rbp), %ebx
	movl	$0, 108(%r14)
	xorl	%esi, %esi
	leaq	116(%r14), %r13
	movl	%ebx, 112(%r14)
	leal	-15(%rbx), %edi
	call	uprv_max_67@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	%r12, %rdi
	subl	%eax, %ebx
	movl	%eax, %esi
	movl	%ebx, %edx
	call	_ZNK6icu_6713UnicodeString9doExtractEiiPDsi@PLT
	xorl	%r11d, %r11d
	movslq	%ebx, %rbx
	movw	%r11w, 116(%r14,%rbx,2)
	movzwl	8(%r12), %eax
	testw	%ax, %ax
	js	.L398
	movswl	%ax, %esi
	sarl	$5, %esi
.L399:
	movl	-244(%rbp), %r15d
	leaq	148(%r14), %r13
	leal	15(%r15), %edi
	call	uprv_min_67@PLT
	movl	%r15d, %esi
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	subl	%r15d, %eax
	movq	%r12, %rdi
	movslq	%eax, %rbx
	movl	%ebx, %edx
	call	_ZNK6icu_6713UnicodeString9doExtractEiiPDsi@PLT
	movq	-264(%rbp), %rax
	xorl	%r10d, %r10d
	movw	%r10w, 148(%r14,%rbx,2)
	movl	$65566, (%rax)
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L400:
	movl	12(%rax), %ecx
	xorl	%edx, %edx
	movq	%rax, %rdi
	movl	%r13d, %esi
	call	_ZNK6icu_6713UnicodeString9doIndexOfEDsii@PLT
	testl	%eax, %eax
	js	.L402
.L605:
	movq	-240(%rbp), %rax
	movl	-244(%rbp), %r14d
	xorl	%esi, %esi
	movq	104(%rax), %r13
	leal	-15(%r14), %edi
	movl	%r14d, 112(%r13)
	movl	$0, 108(%r13)
	call	uprv_max_67@PLT
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movl	%eax, %esi
	leaq	116(%r13), %rax
	subl	%esi, %r14d
	movq	%rax, %rcx
	movq	%rax, -328(%rbp)
	movl	%r14d, %edx
	call	_ZNK6icu_6713UnicodeString9doExtractEiiPDsi@PLT
	movq	-328(%rbp), %rax
	xorl	%r9d, %r9d
	movslq	%r14d, %r14
	movw	%r9w, 116(%r13,%r14,2)
	movzwl	8(%r12), %eax
	testw	%ax, %ax
	js	.L403
	movswl	%ax, %esi
	sarl	$5, %esi
.L404:
	movl	-244(%rbp), %r14d
	leal	15(%r14), %edi
	call	uprv_min_67@PLT
	movl	%r14d, %esi
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	subl	%r14d, %eax
	leaq	148(%r13), %rcx
	movslq	%eax, %r14
	movq	%rcx, -328(%rbp)
	movl	%r14d, %edx
	call	_ZNK6icu_6713UnicodeString9doExtractEiiPDsi@PLT
	movq	-328(%rbp), %rcx
	movq	-264(%rbp), %rax
	xorl	%r8d, %r8d
	movw	%r8w, 148(%r13,%r14,2)
	movl	$65567, (%rax)
	jmp	.L402
	.p2align 4,,10
	.p2align 3
.L405:
	cmpw	$8710, %bx
	je	.L417
.L406:
	leal	-33(%rbx), %eax
	cmpw	$93, %ax
	ja	.L522
	movl	%ebx, %eax
	andl	$-33, %eax
	subl	$65, %eax
	cmpw	$25, %ax
	jbe	.L522
	leal	-48(%rbx), %eax
	cmpw	$9, %ax
	jbe	.L522
	movq	-240(%rbp), %rax
	movq	-264(%rbp), %r8
	movq	%r12, %rdx
	movl	$65555, %esi
	movl	-244(%rbp), %ecx
	movq	104(%rax), %rdi
	call	_ZN6icu_6720TransliteratorParser11syntaxErrorE10UErrorCodeRKNS_13UnicodeStringEiRS1_
	movl	%eax, -244(%rbp)
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L602:
	movl	-232(%rbp), %eax
	cmpl	%eax, -228(%rbp)
	je	.L608
	leaq	-228(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNK6icu_6713UnicodeString10unescapeAtERi@PLT
	movl	%eax, %esi
	movq	-240(%rbp), %rax
	movq	104(%rax), %rbx
	cmpl	$-1, %esi
	je	.L609
	movq	96(%rbx), %rax
	movzwl	1162(%rax), %eax
	cmpl	%eax, %esi
	jl	.L359
	movzwl	426(%rbx), %eax
	cmpl	%eax, %esi
	jge	.L359
.L390:
	movl	-244(%rbp), %r15d
	movl	$0, 108(%rbx)
	xorl	%esi, %esi
	leaq	116(%rbx), %r14
	movl	%r15d, 112(%rbx)
	leal	-15(%r15), %edi
	call	uprv_max_67@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	%r12, %rdi
	subl	%eax, %r15d
	movl	%eax, %esi
	movslq	%r15d, %r13
	movl	%r13d, %edx
	call	_ZNK6icu_6713UnicodeString9doExtractEiiPDsi@PLT
	xorl	%r14d, %r14d
	movw	%r14w, 116(%rbx,%r13,2)
	movzwl	8(%r12), %eax
	testw	%ax, %ax
	js	.L394
	movswl	%ax, %esi
	sarl	$5, %esi
.L395:
	movl	-244(%rbp), %r15d
	leaq	148(%rbx), %r14
	leal	15(%r15), %edi
	call	uprv_min_67@PLT
	movl	%r15d, %esi
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	subl	%r15d, %eax
	movq	%r12, %rdi
	movslq	%eax, %r13
	movl	%r13d, %edx
	call	_ZNK6icu_6713UnicodeString9doExtractEiiPDsi@PLT
	movq	-264(%rbp), %rax
	xorl	%r12d, %r12d
	movw	%r12w, 148(%rbx,%r13,2)
	movl	$65566, (%rax)
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L359:
	movq	-256(%rbp), %rdi
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	movl	-228(%rbp), %eax
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L320:
	movl	12(%r8), %eax
	movl	%eax, -320(%rbp)
	jmp	.L321
	.p2align 4,,10
	.p2align 3
.L347:
	movzwl	426(%rbx), %eax
	cmpw	%ax, 424(%rbx)
	jb	.L610
	movq	0(%r13), %rax
	movq	%r13, %rdi
	xorl	%r15d, %r15d
	call	*8(%rax)
	movq	-264(%rbp), %rax
	movl	$65565, (%rax)
	jmp	.L345
	.p2align 4,,10
	.p2align 3
.L348:
	movl	-200(%rbp), %eax
	xorl	%r15d, %r15d
	movl	%eax, -228(%rbp)
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L610:
	movq	-264(%rbp), %rdx
	leaq	192(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	movzwl	424(%rbx), %r15d
	leal	1(%r15), %eax
	movw	%ax, 424(%rbx)
	jmp	.L345
	.p2align 4,,10
	.p2align 3
.L403:
	movl	12(%r12), %esi
	jmp	.L404
	.p2align 4,,10
	.p2align 3
.L603:
	movl	-228(%rbp), %ecx
	movzwl	8(%r12), %eax
	xorl	%edx, %edx
	testl	%ecx, %ecx
	js	.L363
	testw	%ax, %ax
	js	.L364
	movswl	%ax, %edx
	sarl	$5, %edx
.L365:
	cmpl	%edx, %ecx
	cmovle	%ecx, %edx
.L363:
	testw	%ax, %ax
	js	.L366
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L367:
	subl	%edx, %ecx
	movl	$39, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_6713UnicodeString9doIndexOfEDsii@PLT
	movl	%eax, %ebx
	cmpl	%eax, -228(%rbp)
	je	.L611
	movq	-256(%rbp), %rax
	movswl	8(%rax), %eax
	testw	%ax, %ax
	js	.L369
	sarl	$5, %eax
	movl	%eax, -248(%rbp)
.L370:
	testl	%ebx, %ebx
	js	.L523
	movq	-256(%rbp), %r14
	movq	-296(%rbp), %r13
.L371:
	movzwl	-184(%rbp), %eax
	testb	$1, %al
	je	.L374
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
.L375:
	movq	(%r12), %rax
	movl	-228(%rbp), %esi
	movq	%r13, %rcx
	movl	%ebx, %edx
	movq	%r12, %rdi
	call	*24(%rax)
	movzwl	-184(%rbp), %eax
	testw	%ax, %ax
	js	.L378
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L379:
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	leal	1(%rbx), %eax
	movl	%eax, -228(%rbp)
	cmpl	-232(%rbp), %eax
	jge	.L380
	movzwl	8(%r12), %edx
	testw	%dx, %dx
	js	.L381
	movswl	%dx, %ecx
	sarl	$5, %ecx
.L382:
	cmpl	%eax, %ecx
	jbe	.L380
	andl	$2, %edx
	je	.L383
	movq	-272(%rbp), %rsi
	movslq	%eax, %rdx
	cmpw	$39, (%rsi,%rdx,2)
	jne	.L380
.L524:
	leal	2(%rbx), %edx
	movl	$39, %esi
	movq	%r12, %rdi
	cmpl	%ecx, %edx
	cmovg	%ecx, %edx
	subl	%edx, %ecx
	call	_ZNK6icu_6713UnicodeString9doIndexOfEDsii@PLT
	movl	%eax, %ebx
	testl	%eax, %eax
	jns	.L371
.L523:
	movq	-240(%rbp), %rax
	movl	-244(%rbp), %r15d
	xorl	%esi, %esi
	movq	104(%rax), %rbx
	leal	-15(%r15), %edi
	movl	%r15d, 112(%rbx)
	leaq	116(%rbx), %r14
	movl	$0, 108(%rbx)
	call	uprv_max_67@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	%r12, %rdi
	subl	%eax, %r15d
	movl	%eax, %esi
	movslq	%r15d, %r13
	movl	%r13d, %edx
	call	_ZNK6icu_6713UnicodeString9doExtractEiiPDsi@PLT
	xorl	%eax, %eax
	movw	%ax, 116(%rbx,%r13,2)
	movswl	8(%r12), %eax
	testw	%ax, %ax
	js	.L372
	sarl	$5, %eax
	movl	%eax, %esi
.L373:
	movl	-244(%rbp), %r15d
	leaq	148(%rbx), %r14
	leal	15(%r15), %edi
	call	uprv_min_67@PLT
	movl	%r15d, %esi
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	subl	%r15d, %eax
	movq	%r12, %rdi
	movslq	%eax, %r13
	movl	%r13d, %edx
	call	_ZNK6icu_6713UnicodeString9doExtractEiiPDsi@PLT
	movq	-264(%rbp), %rax
	xorl	%r15d, %r15d
	movw	%r15w, 148(%rbx,%r13,2)
	movl	$65556, (%rax)
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L374:
	testw	%ax, %ax
	js	.L376
	movswl	%ax, %edx
	sarl	$5, %edx
.L377:
	testl	%edx, %edx
	je	.L375
	andl	$31, %eax
	movw	%ax, -184(%rbp)
	jmp	.L375
	.p2align 4,,10
	.p2align 3
.L378:
	movl	-180(%rbp), %ecx
	jmp	.L379
	.p2align 4,,10
	.p2align 3
.L381:
	movl	12(%r12), %ecx
	jmp	.L382
	.p2align 4,,10
	.p2align 3
.L383:
	movq	24(%r12), %rsi
	movslq	%eax, %rdx
	cmpw	$39, (%rsi,%rdx,2)
	je	.L524
.L380:
	movq	-256(%rbp), %rcx
	movzwl	8(%rcx), %edx
	testw	%dx, %dx
	js	.L385
	movswl	%dx, %ecx
	sarl	$5, %ecx
	movl	%ecx, -300(%rbp)
	movl	%ecx, %edi
.L386:
	movl	-248(%rbp), %ecx
	cmpl	%edi, %ecx
	jge	.L330
	movq	-240(%rbp), %rbx
	andl	$2, %edx
	movq	104(%rbx), %rbx
	jne	.L387
	movslq	%ecx, %rcx
	movq	-256(%rbp), %r9
	movq	%rcx, %rdx
	addq	%rcx, %rcx
.L392:
	cmpl	%edx, %edi
	jbe	.L389
	movq	24(%r9), %rsi
	movq	96(%rbx), %r8
	movzwl	(%rsi,%rcx), %esi
	cmpw	%si, 1162(%r8)
	jbe	.L391
.L389:
	addl	$1, %edx
	addq	$2, %rcx
	cmpl	%edx, %edi
	jne	.L392
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L349:
	movl	12(%r12), %esi
	jmp	.L350
	.p2align 4,,10
	.p2align 3
.L376:
	movl	-180(%rbp), %edx
	jmp	.L377
	.p2align 4,,10
	.p2align 3
.L366:
	movl	12(%r12), %ecx
	jmp	.L367
	.p2align 4,,10
	.p2align 3
.L398:
	movl	12(%r12), %esi
	jmp	.L399
	.p2align 4,,10
	.p2align 3
.L394:
	movl	12(%r12), %esi
	jmp	.L395
	.p2align 4,,10
	.p2align 3
.L369:
	movq	-256(%rbp), %rax
	movl	12(%rax), %eax
	movl	%eax, -248(%rbp)
	jmp	.L370
.L407:
	movq	-240(%rbp), %rax
	movl	80(%rax), %esi
	testl	%esi, %esi
	jns	.L612
	movq	-256(%rbp), %rax
	movswl	8(%rax), %eax
	testw	%ax, %ax
	js	.L501
	sarl	$5, %eax
.L502:
	movq	-240(%rbp), %rdi
	movl	%eax, 80(%rdi)
	movl	-228(%rbp), %eax
	jmp	.L330
.L418:
	movslq	-228(%rbp), %rax
	cmpl	-232(%rbp), %eax
	je	.L613
	movzwl	8(%r12), %edx
	testw	%dx, %dx
	js	.L460
	movswl	%dx, %ecx
	sarl	$5, %ecx
.L461:
	movl	$65535, %edi
	cmpl	%eax, %ecx
	jbe	.L462
	andl	$2, %edx
	je	.L463
	movq	-272(%rbp), %rdx
.L464:
	movzwl	(%rdx,%rax,2), %edi
.L462:
	movl	$10, %esi
	call	u_digit_67@PLT
	subl	$1, %eax
	cmpl	$8, %eax
	jbe	.L614
	movl	-228(%rbp), %eax
	movl	-232(%rbp), %r8d
	movq	%r12, %rdx
	movq	-280(%rbp), %rcx
	movl	%eax, -200(%rbp)
	movq	-240(%rbp), %rax
	movq	104(%rax), %rax
	movq	184(%rax), %rsi
	leaq	-128(%rbp), %rax
	movq	%rax, -328(%rbp)
	movq	%rax, %rdi
	movq	(%rsi), %rax
	call	*32(%rax)
	movswl	-120(%rbp), %eax
	testw	%ax, %ax
	js	.L467
	sarl	$5, %eax
.L468:
	testl	%eax, %eax
	je	.L615
	movl	-200(%rbp), %eax
	movl	%eax, -228(%rbp)
	movq	-256(%rbp), %rax
	movswl	8(%rax), %eax
	testw	%ax, %ax
	js	.L470
	sarl	$5, %eax
	movl	%eax, -312(%rbp)
.L471:
	movq	-240(%rbp), %rax
	movq	-256(%rbp), %rbx
	movq	-264(%rbp), %rcx
	movq	-328(%rbp), %rsi
	movq	104(%rax), %rdi
	movq	%rbx, %rdx
	call	_ZN6icu_6720TransliteratorParser17appendVariableDefERKNS_13UnicodeStringERS1_R10UErrorCode
	movswl	8(%rbx), %eax
	testw	%ax, %ax
	js	.L472
	sarl	$5, %eax
	movl	%eax, -316(%rbp)
.L473:
	movq	-328(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-228(%rbp), %eax
	jmp	.L330
.L417:
	movl	-228(%rbp), %eax
	leaq	-220(%rbp), %r14
	movq	%r12, %rdi
	movq	%r14, %rsi
	movl	%eax, -220(%rbp)
	call	_ZN6icu_6722TransliteratorIDParser13parseFilterIDERKNS_13UnicodeStringERi@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L435
	movl	$40, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6711ICU_Utility9parseCharERKNS_13UnicodeStringERiDs@PLT
	testb	%al, %al
	je	.L435
	movq	%r13, %rdi
	call	_ZN6icu_6722TransliteratorIDParser8SingleID14createInstanceEv@PLT
	leaq	136(%r13), %rdi
	movq	%rax, %r14
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	72(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	8(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	testq	%r14, %r14
	je	.L616
	movq	-256(%rbp), %rax
	movzwl	8(%rax), %eax
	testw	%ax, %ax
	js	.L441
	movswl	%ax, %r13d
	sarl	$5, %r13d
.L442:
	leaq	_ZL12ILLEGAL_FUNC(%rip), %rax
	movl	$-1, %ecx
	movl	$1, %esi
	movq	%rax, -216(%rbp)
	leaq	-128(%rbp), %rax
	leaq	-216(%rbp), %rdx
	movq	%rax, %rdi
	movq	%rax, %rbx
	movq	%rdx, -336(%rbp)
	movq	%rax, -328(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	pushq	-264(%rbp)
	movq	%rbx, %r9
	movq	-256(%rbp), %r8
	movl	-232(%rbp), %ecx
	pushq	$1
	movq	%r12, %rsi
	movl	-220(%rbp), %edx
	movq	-240(%rbp), %rdi
	call	_ZN6icu_678RuleHalf12parseSectionERKNS_13UnicodeStringEiiRS1_S3_aR10UErrorCode
	movq	%rbx, %rdi
	movl	%eax, -228(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-256(%rbp), %rcx
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %r10d
	popq	%r11
	movq	%rax, -128(%rbp)
	popq	%rbx
	movq	(%rcx), %rax
	movw	%r10w, -120(%rbp)
	movq	24(%rax), %r8
	movzwl	8(%rcx), %eax
	testw	%ax, %ax
	js	.L443
	movswl	%ax, %edx
	movq	%rcx, %rdi
	sarl	$5, %edx
.L444:
	movq	-328(%rbp), %rcx
	movl	%r13d, %esi
	call	*%r8
	movl	$96, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L445
	movq	-240(%rbp), %rax
	movq	-328(%rbp), %rsi
	movq	%rbx, %rdi
	movq	104(%rax), %rax
	movq	96(%rax), %rdx
	call	_ZN6icu_6714StringReplacerC1ERKNS_13UnicodeStringEPKNS_23TransliterationRuleDataE@PLT
.L445:
	movl	$32, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, -344(%rbp)
	testq	%rax, %rax
	je	.L446
	movq	%rax, %rdi
	movq	%rbx, %rdx
	movq	%r14, %rsi
	call	_ZN6icu_6716FunctionReplacerC1EPNS_14TransliteratorEPNS_14UnicodeFunctorE@PLT
	movq	-256(%rbp), %rax
	movzwl	8(%rax), %eax
	testl	%r13d, %r13d
	jne	.L447
	testb	$1, %al
	jne	.L617
.L447:
	testw	%ax, %ax
	js	.L449
	movswl	%ax, %edx
	sarl	$5, %edx
.L450:
	cmpl	%r13d, %edx
	jbe	.L448
	cmpl	$1023, %r13d
	jg	.L451
	movq	-256(%rbp), %rdi
	andl	$31, %eax
	sall	$5, %r13d
	orl	%r13d, %eax
	movw	%ax, 8(%rdi)
.L448:
	movq	-240(%rbp), %rax
	xorl	%ebx, %ebx
	movq	104(%rax), %r13
	movl	200(%r13), %r9d
	leaq	192(%r13), %r14
	testl	%r9d, %r9d
	jle	.L458
	movq	%r12, -352(%rbp)
	movl	%ebx, %r12d
	movq	%r14, %rbx
	movq	-344(%rbp), %r14
	jmp	.L457
	.p2align 4,,10
	.p2align 3
.L455:
	addl	$1, %r12d
	cmpl	%r12d, 200(%r13)
	jle	.L618
.L457:
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	cmpq	%rax, %r14
	jne	.L455
	movq	96(%r13), %rax
	movl	%r12d, %ebx
	movq	-352(%rbp), %r12
	movzwl	1162(%rax), %edx
	addl	%ebx, %edx
.L456:
	movq	-336(%rbp), %rsi
	movq	-256(%rbp), %rdi
	movl	$1, %ecx
	movw	%dx, -216(%rbp)
	xorl	%edx, %edx
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movq	-328(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-228(%rbp), %eax
	jmp	.L330
.L416:
	movq	-256(%rbp), %rax
	movzwl	8(%rax), %eax
	testw	%ax, %ax
	js	.L425
	movswl	%ax, %ebx
	sarl	$5, %ebx
.L426:
	movq	-240(%rbp), %rdi
	leaq	-128(%rbp), %r14
	movl	$-1, %ecx
	movl	$1, %esi
	movl	96(%rdi), %r13d
	leal	1(%r13), %eax
	movl	%eax, 96(%rdi)
	leaq	_ZL11ILLEGAL_SEG(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -216(%rbp)
	leaq	-216(%rbp), %rax
	movq	%rax, %rdx
	movq	%rax, -336(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	pushq	-264(%rbp)
	movq	%r14, %r9
	movq	-256(%rbp), %r8
	movl	-232(%rbp), %ecx
	pushq	$1
	movq	%r12, %rsi
	movl	-228(%rbp), %edx
	movq	-240(%rbp), %rdi
	call	_ZN6icu_678RuleHalf12parseSectionERKNS_13UnicodeStringEiiRS1_S3_aR10UErrorCode
	movq	%r14, %rdi
	movl	%eax, -228(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-256(%rbp), %rax
	popq	%rdx
	popq	%rcx
	movzwl	8(%rax), %eax
	testw	%ax, %ax
	js	.L427
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L428:
	movl	$112, %edi
	movl	%ecx, -328(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	movq	-240(%rbp), %rax
	testq	%r14, %r14
	je	.L482
	movq	104(%rax), %rax
	movl	-328(%rbp), %ecx
	movl	%r13d, %r8d
	movl	%ebx, %edx
	movq	-256(%rbp), %rsi
	movq	%r14, %rdi
	movq	96(%rax), %r9
	call	_ZN6icu_6713StringMatcherC1ERKNS_13UnicodeStringEiiiRKNS_23TransliterationRuleDataE@PLT
	movq	-240(%rbp), %rax
	movq	%r14, %rdx
	movl	%r13d, %esi
	movq	-264(%rbp), %rcx
	movq	104(%rax), %rdi
	call	_ZN6icu_6720TransliteratorParser16setSegmentObjectEiPNS_13StringMatcherER10UErrorCode
	movq	-256(%rbp), %rax
	movzwl	8(%rax), %eax
	testl	%ebx, %ebx
	jne	.L430
	testb	$1, %al
	jne	.L619
.L430:
	testw	%ax, %ax
	js	.L432
	movswl	%ax, %edx
	sarl	$5, %edx
.L433:
	cmpl	%ebx, %edx
	jbe	.L431
	cmpl	$1023, %ebx
	jg	.L434
	movq	-256(%rbp), %rdi
	andl	$31, %eax
	sall	$5, %ebx
	orl	%ebx, %eax
	movw	%ax, 8(%rdi)
.L431:
	movq	-240(%rbp), %rax
	movq	-264(%rbp), %rdx
	movl	%r13d, %esi
	movq	104(%rax), %rdi
	call	_ZN6icu_6720TransliteratorParser17getSegmentStandinEiR10UErrorCode
	movq	-336(%rbp), %rsi
	movq	-256(%rbp), %rdi
	xorl	%edx, %edx
	movl	$1, %ecx
	movw	%ax, -216(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movl	-228(%rbp), %eax
	jmp	.L330
.L413:
	movq	-256(%rbp), %rax
	cmpb	$0, -301(%rbp)
	movzwl	8(%rax), %eax
	je	.L474
	testw	%ax, %ax
	js	.L475
	movswl	%ax, %ecx
	sarl	$5, %ecx
	cmpl	%ecx, -320(%rbp)
	je	.L476
.L480:
	movl	-248(%rbp), %r13d
	cmpl	-300(%rbp), %ecx
	je	.L481
	leal	-1(%rcx), %r13d
	cmpl	-316(%rbp), %ecx
	cmove	-312(%rbp), %r13d
.L481:
	movl	$112, %edi
	movl	%ecx, -328(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	movq	-240(%rbp), %rax
	testq	%r14, %r14
	je	.L482
	movq	104(%rax), %rax
	movl	-328(%rbp), %ecx
	xorl	%r8d, %r8d
	movl	%r13d, %edx
	movq	-256(%rbp), %rsi
	movq	%r14, %rdi
	movq	96(%rax), %r9
	call	_ZN6icu_6713StringMatcherC1ERKNS_13UnicodeStringEiiiRKNS_23TransliterationRuleDataE@PLT
	cmpw	$43, %bx
	je	.L531
	xorl	%edx, %edx
	movl	$2147483647, %ecx
	cmpw	$63, %bx
	movl	$1, %eax
	cmovne	%ecx, %eax
	movl	%eax, %ebx
.L483:
	movl	$32, %edi
	movl	%edx, -336(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, -328(%rbp)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L484
	movl	-336(%rbp), %edx
	movl	%ebx, %ecx
	movq	%r14, %rsi
	call	_ZN6icu_6710QuantifierC1EPNS_14UnicodeFunctorEjj@PLT
	movq	-256(%rbp), %rax
	movzwl	8(%rax), %eax
	testl	%r13d, %r13d
	jne	.L485
	testb	$1, %al
	jne	.L620
.L485:
	testw	%ax, %ax
	js	.L487
	movswl	%ax, %edx
	sarl	$5, %edx
.L488:
	cmpl	%r13d, %edx
	jbe	.L486
	cmpl	$1023, %r13d
	jg	.L489
	movq	-256(%rbp), %rdi
	andl	$31, %eax
	sall	$5, %r13d
	orl	%r13d, %eax
	movw	%ax, 8(%rdi)
.L486:
	movq	-240(%rbp), %rax
	xorl	%ebx, %ebx
	movq	104(%rax), %r13
	movl	200(%r13), %r8d
	leaq	192(%r13), %r14
	testl	%r8d, %r8d
	jle	.L496
	movq	%r12, -336(%rbp)
	movl	%ebx, %r12d
	movq	%r14, %rbx
	movq	-328(%rbp), %r14
	jmp	.L495
	.p2align 4,,10
	.p2align 3
.L493:
	addl	$1, %r12d
	cmpl	%r12d, 200(%r13)
	jle	.L621
.L495:
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	cmpq	%rax, %r14
	jne	.L493
	movq	96(%r13), %rax
	movl	%r12d, %ebx
	movq	-336(%rbp), %r12
	movzwl	1162(%rax), %edx
	addl	%ebx, %edx
.L494:
	movw	%dx, -216(%rbp)
	jmp	.L595
.L414:
	movq	-240(%rbp), %rax
	movq	-264(%rbp), %rsi
	movq	104(%rax), %rdi
	call	_ZN6icu_6720TransliteratorParser13getDotStandInER10UErrorCode
.L594:
	movw	%ax, -216(%rbp)
.L595:
	movq	-256(%rbp), %rdi
	leaq	-216(%rbp), %rsi
	movl	$1, %ecx
	xorl	%edx, %edx
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movl	-228(%rbp), %eax
	jmp	.L330
.L412:
	movq	-240(%rbp), %rax
	movl	84(%rax), %eax
	testl	%eax, %eax
	js	.L622
	je	.L510
	movq	-256(%rbp), %rcx
	movswl	8(%rcx), %edx
	testw	%dx, %dx
	js	.L511
	sarl	$5, %edx
.L512:
	movq	-240(%rbp), %rcx
	cmpl	%edx, 88(%rcx)
	jne	.L518
	movl	72(%rcx), %edx
	testl	%edx, %edx
	jns	.L518
	addl	$1, %eax
	movl	%eax, 84(%rcx)
	movl	-228(%rbp), %eax
	jmp	.L330
.L411:
	movq	-256(%rbp), %rax
	movswl	8(%rax), %eax
	testw	%ax, %ax
	js	.L419
	sarl	$5, %eax
.L420:
	testl	%eax, %eax
	jne	.L421
	movq	-240(%rbp), %rax
	movzbl	92(%rax), %r15d
	testb	%r15b, %r15b
	jne	.L421
	movb	$1, 92(%rax)
	movl	-228(%rbp), %eax
	jmp	.L330
.L410:
	movq	-240(%rbp), %rax
	movl	76(%rax), %edi
	testl	%edi, %edi
	jns	.L623
	movq	-256(%rbp), %rax
	movswl	8(%rax), %eax
	testw	%ax, %ax
	js	.L498
	sarl	$5, %eax
.L499:
	movq	-240(%rbp), %rcx
	movl	%eax, 76(%rcx)
	movl	-228(%rbp), %eax
	jmp	.L330
.L409:
	movq	-240(%rbp), %rax
	movl	72(%rax), %ecx
	testl	%ecx, %ecx
	jns	.L624
	movq	-256(%rbp), %rax
	movswl	8(%rax), %eax
	testw	%ax, %ax
	js	.L504
	sarl	$5, %eax
.L505:
	movq	-240(%rbp), %rcx
	movl	%eax, 72(%rcx)
	movl	-228(%rbp), %eax
	jmp	.L330
.L415:
	movl	-228(%rbp), %eax
	movl	$1, %r15d
	jmp	.L330
.L387:
	movslq	-248(%rbp), %rdx
	movq	-256(%rbp), %rsi
	movl	%edi, %ecx
.L396:
	cmpl	%edx, %ecx
	jbe	.L393
	movq	96(%rbx), %rdi
	movzwl	10(%rsi,%rdx,2), %r8d
	movzwl	10(%rsi,%rdx,2), %r10d
	cmpw	%r10w, 1162(%rdi)
	jbe	.L625
.L393:
	addq	$1, %rdx
	cmpl	%edx, %ecx
	jg	.L396
	jmp	.L330
.L522:
	movw	%bx, -216(%rbp)
	jmp	.L595
	.p2align 4,,10
	.p2align 3
.L385:
	movl	12(%rcx), %edi
	movl	%edi, -300(%rbp)
	jmp	.L386
	.p2align 4,,10
	.p2align 3
.L611:
	movq	-256(%rbp), %rdi
	movl	$39, %eax
	leaq	-216(%rbp), %rsi
	xorl	%edx, %edx
	movl	$1, %ecx
	movw	%ax, -216(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movl	-228(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -228(%rbp)
	jmp	.L330
.L364:
	movl	12(%r12), %edx
	jmp	.L365
.L599:
	cmpb	$0, -308(%rbp)
	jne	.L332
	movl	-228(%rbp), %eax
	jmp	.L323
.L600:
	movq	104(%rax), %rbx
	movl	-244(%rbp), %r15d
	xorl	%esi, %esi
	movl	%r15d, 112(%rbx)
	leal	-15(%r15), %edi
	leaq	116(%rbx), %r14
	movl	$0, 108(%rbx)
	call	uprv_max_67@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	%r12, %rdi
	subl	%eax, %r15d
	movl	%eax, %esi
	movslq	%r15d, %r13
	movl	%r13d, %edx
	call	_ZNK6icu_6713UnicodeString9doExtractEiiPDsi@PLT
	xorl	%r10d, %r10d
	movw	%r10w, 116(%rbx,%r13,2)
	movzwl	8(%r12), %eax
	testw	%ax, %ax
	js	.L337
	movswl	%ax, %esi
	sarl	$5, %esi
.L338:
	movl	-244(%rbp), %r15d
	leaq	148(%rbx), %r14
	leal	15(%r15), %edi
	call	uprv_min_67@PLT
	movl	%r15d, %esi
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	subl	%r15d, %eax
	movq	%r12, %rdi
	movslq	%eax, %r13
	movl	%r13d, %edx
	call	_ZNK6icu_6713UnicodeString9doExtractEiiPDsi@PLT
	movq	-264(%rbp), %rax
	xorl	%r9d, %r9d
	movw	%r9w, 148(%rbx,%r13,2)
	movl	$65542, (%rax)
	jmp	.L335
.L625:
	movzwl	426(%rbx), %edi
	cmpl	%edi, %r8d
	jl	.L390
	jmp	.L393
.L391:
	cmpw	%si, 426(%rbx)
	ja	.L390
	jmp	.L389
.L621:
	movq	-336(%rbp), %r12
.L496:
	movzwl	426(%r13), %eax
	cmpw	%ax, 424(%r13)
	jb	.L626
	movq	-328(%rbp), %rdi
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	-264(%rbp), %rax
	xorl	%edx, %edx
	movl	$65565, (%rax)
	jmp	.L494
.L474:
	movswl	%ax, %ecx
	sarl	$5, %ecx
	testw	%ax, %ax
	jns	.L480
	movq	-256(%rbp), %rax
	movl	12(%rax), %ecx
	jmp	.L480
.L618:
	movq	-352(%rbp), %r12
.L458:
	movzwl	426(%r13), %eax
	cmpw	%ax, 424(%r13)
	jb	.L627
	movq	-344(%rbp), %rdi
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	-264(%rbp), %rax
	xorl	%edx, %edx
	movl	$65565, (%rax)
	jmp	.L456
.L427:
	movq	-256(%rbp), %rax
	movl	12(%rax), %ecx
	jmp	.L428
.L425:
	movq	-256(%rbp), %rax
	movl	12(%rax), %ebx
	jmp	.L426
.L419:
	movq	-256(%rbp), %rax
	movl	12(%rax), %eax
	jmp	.L420
.L622:
	movq	-256(%rbp), %rdi
	movswl	8(%rdi), %edx
	testw	%dx, %dx
	js	.L507
	sarl	$5, %edx
.L508:
	testl	%edx, %edx
	jg	.L518
	movq	-240(%rbp), %rcx
	subl	$1, %eax
	movl	%eax, 84(%rcx)
	movl	-228(%rbp), %eax
	jmp	.L330
.L498:
	movq	-256(%rbp), %rax
	movl	12(%rax), %eax
	jmp	.L499
.L504:
	movq	-256(%rbp), %rax
	movl	12(%rax), %eax
	jmp	.L505
.L501:
	movq	-256(%rbp), %rax
	movl	12(%rax), %eax
	jmp	.L502
.L443:
	movl	12(%rcx), %edx
	movq	%rcx, %rdi
	jmp	.L444
.L441:
	movq	-256(%rbp), %rax
	movl	12(%rax), %r13d
	jmp	.L442
.L337:
	movl	12(%r12), %esi
	jmp	.L338
.L487:
	movq	-256(%rbp), %rcx
	movl	12(%rcx), %edx
	jmp	.L488
.L432:
	movq	-256(%rbp), %rdi
	movl	12(%rdi), %edx
	jmp	.L433
.L449:
	movq	-256(%rbp), %rdi
	movl	12(%rdi), %edx
	jmp	.L450
.L613:
	movq	-240(%rbp), %rax
	movb	$1, 93(%rax)
	movl	-232(%rbp), %eax
	jmp	.L330
.L531:
	movl	$1, %edx
	movl	$2147483647, %ebx
	jmp	.L483
.L332:
	movq	-240(%rbp), %rax
	movl	-244(%rbp), %r15d
	xorl	%esi, %esi
	movq	104(%rax), %rbx
	leal	-15(%r15), %edi
	movl	%r15d, 112(%rbx)
	leaq	116(%rbx), %r14
	movl	$0, 108(%rbx)
	call	uprv_max_67@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	%r12, %rdi
	subl	%eax, %r15d
	movl	%eax, %esi
	movslq	%r15d, %r13
	movl	%r13d, %edx
	call	_ZNK6icu_6713UnicodeString9doExtractEiiPDsi@PLT
	xorl	%r14d, %r14d
	movw	%r14w, 116(%rbx,%r13,2)
	movswl	8(%r12), %eax
	testw	%ax, %ax
	js	.L333
	sarl	$5, %eax
	movl	%eax, %esi
.L334:
	movl	-244(%rbp), %r15d
	leaq	148(%rbx), %r14
	leal	15(%r15), %edi
	call	uprv_min_67@PLT
	movl	%r15d, %esi
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	subl	%r15d, %eax
	movq	%r12, %rdi
	movslq	%eax, %r13
	movl	%r13d, %edx
	call	_ZNK6icu_6713UnicodeString9doExtractEiiPDsi@PLT
	movq	-264(%rbp), %rax
	xorl	%r11d, %r11d
	movw	%r11w, 148(%rbx,%r13,2)
	movl	$65563, (%rax)
	jmp	.L335
.L510:
	movq	-240(%rbp), %rax
	movl	72(%rax), %eax
	testl	%eax, %eax
	jne	.L514
	movq	-256(%rbp), %rax
	movswl	8(%rax), %eax
	testw	%ax, %ax
	js	.L515
	sarl	$5, %eax
.L516:
	testl	%eax, %eax
	je	.L517
.L518:
	movq	-240(%rbp), %rax
	movq	-264(%rbp), %r8
	movq	%r12, %rdx
	movl	$65545, %esi
	movl	-244(%rbp), %ecx
	movq	104(%rax), %rdi
	call	_ZN6icu_6720TransliteratorParser11syntaxErrorE10UErrorCodeRKNS_13UnicodeStringEiRS1_
	movl	%eax, -244(%rbp)
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L460:
	movl	12(%r12), %ecx
	jmp	.L461
.L463:
	movq	24(%r12), %rdx
	jmp	.L464
.L626:
	movq	-264(%rbp), %rdx
	movq	-328(%rbp), %rsi
	leaq	192(%r13), %rdi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	movzwl	424(%r13), %edx
	leal	1(%rdx), %eax
	movw	%ax, 424(%r13)
	jmp	.L494
.L627:
	movq	-264(%rbp), %rdx
	movq	-344(%rbp), %rsi
	leaq	192(%r13), %rdi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	movzwl	424(%r13), %edx
	leal	1(%rdx), %eax
	movw	%ax, 424(%r13)
	jmp	.L456
.L489:
	movq	-256(%rbp), %rcx
	orl	$-32, %eax
	movw	%ax, 8(%rcx)
	movl	%r13d, 12(%rcx)
	jmp	.L486
.L434:
	movq	-256(%rbp), %rcx
	orl	$-32, %eax
	movw	%ax, 8(%rcx)
	movl	%ebx, 12(%rcx)
	jmp	.L431
.L514:
	jns	.L518
	movq	-256(%rbp), %rax
	movswl	8(%rax), %eax
	testw	%ax, %ax
	js	.L520
	sarl	$5, %eax
.L521:
	movq	-240(%rbp), %rcx
	movl	%eax, 88(%rcx)
	movl	-228(%rbp), %eax
	movl	$1, 84(%rcx)
	jmp	.L330
.L614:
	leaq	-228(%rbp), %rsi
	movq	%r12, %rdi
	movl	$10, %edx
	call	_ZN6icu_6711ICU_Utility11parseNumberERKNS_13UnicodeStringERia@PLT
	movl	%eax, %esi
	movq	-240(%rbp), %rax
	movq	104(%rax), %rdi
	testl	%esi, %esi
	js	.L628
	movq	-264(%rbp), %rdx
	call	_ZN6icu_6720TransliteratorParser17getSegmentStandinEiR10UErrorCode
	jmp	.L594
.L608:
	movq	-240(%rbp), %rax
	movl	-244(%rbp), %r15d
	xorl	%esi, %esi
	movq	104(%rax), %rbx
	leal	-15(%r15), %edi
	movl	%r15d, 112(%rbx)
	leaq	116(%rbx), %r14
	movl	$0, 108(%rbx)
	call	uprv_max_67@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	%r12, %rdi
	subl	%eax, %r15d
	movl	%eax, %esi
	movslq	%r15d, %r13
	movl	%r13d, %edx
	call	_ZNK6icu_6713UnicodeString9doExtractEiiPDsi@PLT
	xorl	%edx, %edx
	movw	%dx, 116(%rbx,%r13,2)
	movswl	8(%r12), %eax
	testw	%ax, %ax
	js	.L354
	sarl	$5, %eax
	movl	%eax, %esi
.L355:
	movl	-244(%rbp), %r15d
	leaq	148(%rbx), %r14
	leal	15(%r15), %edi
	call	uprv_min_67@PLT
	movl	%r15d, %esi
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	subl	%r15d, %eax
	movq	%r12, %rdi
	movslq	%eax, %r13
	movl	%r13d, %edx
	call	_ZNK6icu_6713UnicodeString9doExtractEiiPDsi@PLT
	xorl	%eax, %eax
	movw	%ax, 148(%rbx,%r13,2)
	movq	-264(%rbp), %rax
	movl	$65552, (%rax)
	jmp	.L335
.L609:
	movl	-244(%rbp), %r15d
	movl	$0, 108(%rbx)
	xorl	%esi, %esi
	leaq	116(%rbx), %r14
	movl	%r15d, 112(%rbx)
	leal	-15(%r15), %edi
	call	uprv_max_67@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	%r12, %rdi
	subl	%eax, %r15d
	movl	%eax, %esi
	movslq	%r15d, %r13
	movl	%r13d, %edx
	call	_ZNK6icu_6713UnicodeString9doExtractEiiPDsi@PLT
	xorl	%eax, %eax
	movw	%ax, 116(%rbx,%r13,2)
	movswl	8(%r12), %eax
	testw	%ax, %ax
	js	.L357
	sarl	$5, %eax
	movl	%eax, %esi
.L358:
	movl	-244(%rbp), %r15d
	leaq	148(%rbx), %r14
	leal	15(%r15), %edi
	call	uprv_min_67@PLT
	movl	%r15d, %esi
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	subl	%r15d, %eax
	movq	%r12, %rdi
	movslq	%eax, %r13
	movl	%r13d, %edx
	call	_ZNK6icu_6713UnicodeString9doExtractEiiPDsi@PLT
	xorl	%eax, %eax
	movw	%ax, 148(%rbx,%r13,2)
	movq	-264(%rbp), %rax
	movl	$65540, (%rax)
	jmp	.L335
.L451:
	movq	-256(%rbp), %rdi
	orl	$-32, %eax
	movw	%ax, 8(%rdi)
	movl	%r13d, 12(%rdi)
	jmp	.L448
.L475:
	movq	-256(%rbp), %rax
	movl	12(%rax), %ecx
	cmpl	%ecx, -320(%rbp)
	jne	.L480
.L476:
	movq	-240(%rbp), %rax
	movq	-264(%rbp), %r8
	movq	%r12, %rdx
	movl	$65546, %esi
	movl	-244(%rbp), %ecx
	movq	104(%rax), %rdi
	call	_ZN6icu_6720TransliteratorParser11syntaxErrorE10UErrorCodeRKNS_13UnicodeStringEiRS1_
	movl	%eax, -244(%rbp)
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L467:
	movl	-116(%rbp), %eax
	jmp	.L468
.L507:
	movl	12(%rdi), %edx
	jmp	.L508
.L615:
	movq	-240(%rbp), %rax
	movq	-328(%rbp), %rdi
	movb	$1, 93(%rax)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-228(%rbp), %eax
	jmp	.L330
.L333:
	movl	12(%r12), %esi
	jmp	.L334
.L470:
	movq	-256(%rbp), %rax
	movl	12(%rax), %eax
	movl	%eax, -312(%rbp)
	jmp	.L471
.L511:
	movl	12(%rcx), %edx
	jmp	.L512
.L372:
	movl	12(%r12), %esi
	jmp	.L373
.L472:
	movq	-256(%rbp), %rax
	movl	12(%rax), %eax
	movl	%eax, -316(%rbp)
	jmp	.L473
.L620:
	movq	-256(%rbp), %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	jmp	.L486
.L619:
	movq	-256(%rbp), %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	jmp	.L431
.L617:
	movq	-256(%rbp), %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	jmp	.L448
.L520:
	movq	-256(%rbp), %rax
	movl	12(%rax), %eax
	jmp	.L521
.L354:
	movl	12(%r12), %esi
	jmp	.L355
.L357:
	movl	12(%r12), %esi
	jmp	.L358
.L517:
	movq	-240(%rbp), %rax
	movl	$-1, 84(%rax)
	movl	-228(%rbp), %eax
	jmp	.L330
.L515:
	movq	-256(%rbp), %rax
	movl	12(%rax), %eax
	jmp	.L516
.L435:
	movq	-240(%rbp), %rax
	movl	-244(%rbp), %r15d
	xorl	%esi, %esi
	movq	104(%rax), %rbx
	leal	-15(%r15), %edi
	movl	%r15d, 112(%rbx)
	leaq	116(%rbx), %r14
	movl	$0, 108(%rbx)
	call	uprv_max_67@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	%r12, %rdi
	subl	%eax, %r15d
	movl	%eax, %esi
	movslq	%r15d, %r13
	movl	%r13d, %edx
	call	_ZNK6icu_6713UnicodeString9doExtractEiiPDsi@PLT
	xorl	%r14d, %r14d
	movw	%r14w, 116(%rbx,%r13,2)
	movswl	8(%r12), %eax
	testw	%ax, %ax
	js	.L438
	sarl	$5, %eax
	movl	%eax, %esi
.L439:
	movl	-244(%rbp), %r15d
	leaq	148(%rbx), %r14
	leal	15(%r15), %edi
	call	uprv_min_67@PLT
	movl	%r15d, %esi
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	subl	%r15d, %eax
	movq	%r12, %rdi
	movslq	%eax, %r13
	movl	%r13d, %edx
	call	_ZNK6icu_6713UnicodeString9doExtractEiiPDsi@PLT
	movq	-264(%rbp), %rax
	xorl	%r12d, %r12d
	movw	%r12w, 148(%rbx,%r13,2)
	movl	$65570, (%rax)
	jmp	.L335
.L421:
	movq	-240(%rbp), %rax
	movl	-244(%rbp), %r15d
	xorl	%esi, %esi
	movq	104(%rax), %rbx
	leal	-15(%r15), %edi
	movl	%r15d, 112(%rbx)
	leaq	116(%rbx), %r14
	movl	$0, 108(%rbx)
	call	uprv_max_67@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	%r12, %rdi
	subl	%eax, %r15d
	movl	%eax, %esi
	movslq	%r15d, %r13
	movl	%r13d, %edx
	call	_ZNK6icu_6713UnicodeString9doExtractEiiPDsi@PLT
	xorl	%edi, %edi
	movw	%di, 116(%rbx,%r13,2)
	movswl	8(%r12), %eax
	testw	%ax, %ax
	js	.L423
	sarl	$5, %eax
	movl	%eax, %esi
.L424:
	movl	-244(%rbp), %r15d
	leaq	148(%rbx), %r14
	leal	15(%r15), %edi
	call	uprv_min_67@PLT
	movl	%r15d, %esi
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	subl	%r15d, %eax
	movq	%r12, %rdi
	movslq	%eax, %r13
	movl	%r13d, %edx
	call	_ZNK6icu_6713UnicodeString9doExtractEiiPDsi@PLT
	movq	-264(%rbp), %rax
	xorl	%esi, %esi
	movw	%si, 148(%rbx,%r13,2)
	movl	$65544, (%rax)
	jmp	.L335
.L616:
	movq	-240(%rbp), %rax
	movq	-264(%rbp), %r8
	movq	%r12, %rdx
	movl	$65570, %esi
	movl	-244(%rbp), %ecx
	movq	104(%rax), %rdi
	call	_ZN6icu_6720TransliteratorParser11syntaxErrorE10UErrorCodeRKNS_13UnicodeStringEiRS1_
	movl	%eax, -244(%rbp)
	jmp	.L335
.L423:
	movl	12(%r12), %esi
	jmp	.L424
.L623:
	movq	104(%rax), %rdi
	movq	-264(%rbp), %r8
	movq	%r12, %rdx
	movl	$65549, %esi
	movl	-244(%rbp), %ecx
	call	_ZN6icu_6720TransliteratorParser11syntaxErrorE10UErrorCodeRKNS_13UnicodeStringEiRS1_
	movl	%eax, -244(%rbp)
	jmp	.L335
.L624:
	movq	104(%rax), %rdi
	movq	-264(%rbp), %r8
	movq	%r12, %rdx
	movl	$65550, %esi
	movl	-244(%rbp), %ecx
	call	_ZN6icu_6720TransliteratorParser11syntaxErrorE10UErrorCodeRKNS_13UnicodeStringEiRS1_
	movl	%eax, -244(%rbp)
	jmp	.L335
.L612:
	movq	104(%rax), %rdi
	movq	-264(%rbp), %r8
	movq	%r12, %rdx
	movl	$65551, %esi
	movl	-244(%rbp), %ecx
	call	_ZN6icu_6720TransliteratorParser11syntaxErrorE10UErrorCodeRKNS_13UnicodeStringEiRS1_
	movl	%eax, -244(%rbp)
	jmp	.L335
.L438:
	movl	12(%r12), %esi
	jmp	.L439
.L607:
	call	__stack_chk_fail@PLT
.L340:
	movq	-264(%rbp), %rax
	xorl	%r15d, %r15d
	movl	$7, (%rax)
	jmp	.L345
.L446:
	movq	-240(%rbp), %rax
	movq	-264(%rbp), %r8
	movq	%r12, %rdx
	movl	$7, %esi
	movl	-244(%rbp), %ecx
	movq	104(%rax), %rdi
	call	_ZN6icu_6720TransliteratorParser11syntaxErrorE10UErrorCodeRKNS_13UnicodeStringEiRS1_
	movq	-328(%rbp), %rdi
	movl	%eax, -244(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L335
.L484:
	movq	-240(%rbp), %rax
.L482:
	movq	104(%rax), %rdi
	movq	-264(%rbp), %r8
	movq	%r12, %rdx
	movl	$7, %esi
	movl	-244(%rbp), %ecx
	call	_ZN6icu_6720TransliteratorParser11syntaxErrorE10UErrorCodeRKNS_13UnicodeStringEiRS1_
	movl	%eax, -244(%rbp)
	jmp	.L335
.L628:
	movq	-264(%rbp), %r8
	movl	-244(%rbp), %ecx
	movq	%r12, %rdx
	movl	$65553, %esi
	call	_ZN6icu_6720TransliteratorParser11syntaxErrorE10UErrorCodeRKNS_13UnicodeStringEiRS1_
	movl	%eax, -244(%rbp)
	jmp	.L335
	.cfi_endproc
.LFE2510:
	.size	_ZN6icu_678RuleHalf12parseSectionERKNS_13UnicodeStringEiiRS1_S3_aR10UErrorCode, .-_ZN6icu_678RuleHalf12parseSectionERKNS_13UnicodeStringEiiRS1_S3_aR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_678RuleHalf5parseERKNS_13UnicodeStringEiiR10UErrorCode
	.type	_ZN6icu_678RuleHalf5parseERKNS_13UnicodeStringEiiR10UErrorCode, @function
_ZN6icu_678RuleHalf5parseERKNS_13UnicodeStringEiiR10UErrorCode:
.LFB2509:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	%r8, -152(%rbp)
	leaq	8(%rdi), %r8
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzwl	16(%rdi), %eax
	testb	$1, %al
	je	.L630
	movq	%r8, %rdi
	movq	%r8, -160(%rbp)
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	movq	-160(%rbp), %r8
.L631:
	leaq	-128(%rbp), %r14
	movl	$-1, %ecx
	movl	$1, %esi
	movq	%r8, -160(%rbp)
	leaq	_ZL11ILLEGAL_TOP(%rip), %rax
	leaq	-136(%rbp), %rdx
	movq	%r14, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movl	%r15d, %ecx
	movq	%r14, %r9
	movl	%r12d, %edx
	pushq	-152(%rbp)
	movq	-160(%rbp), %r8
	movq	%r13, %rsi
	movq	%rbx, %rdi
	pushq	$0
	call	_ZN6icu_678RuleHalf12parseSectionERKNS_13UnicodeStringEiiRS1_S3_aR10UErrorCode
	movq	%r14, %rdi
	movl	%eax, %r15d
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	84(%rbx), %edi
	popq	%rcx
	popq	%rsi
	testl	%edi, %edi
	jle	.L629
	movl	88(%rbx), %eax
	cmpl	%eax, 72(%rbx)
	jne	.L642
.L629:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L643
	leaq	-40(%rbp), %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L630:
	.cfi_restore_state
	testw	%ax, %ax
	js	.L632
	movswl	%ax, %edx
	sarl	$5, %edx
.L633:
	testl	%edx, %edx
	je	.L631
	andl	$31, %eax
	movw	%ax, 16(%rbx)
	jmp	.L631
	.p2align 4,,10
	.p2align 3
.L642:
	movq	104(%rbx), %rbx
	leal	-15(%r12), %edi
	xorl	%esi, %esi
	movl	%r12d, %r15d
	movl	%r12d, 112(%rbx)
	movl	$0, 108(%rbx)
	call	uprv_max_67@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	movl	%eax, %esi
	leaq	116(%rbx), %rax
	subl	%esi, %r15d
	movq	%rax, %rcx
	movq	%rax, -160(%rbp)
	movl	%r15d, %edx
	call	_ZNK6icu_6713UnicodeString9doExtractEiiPDsi@PLT
	movq	-160(%rbp), %rax
	xorl	%edx, %edx
	movslq	%r15d, %r15
	movw	%dx, 116(%rbx,%r15,2)
	movswl	8(%r13), %esi
	testw	%si, %si
	js	.L635
	sarl	$5, %esi
.L636:
	leal	15(%r12), %edi
	call	uprv_min_67@PLT
	xorl	%r8d, %r8d
	movl	%r12d, %esi
	movq	%r13, %rdi
	subl	%r12d, %eax
	leaq	148(%rbx), %rcx
	movslq	%eax, %r15
	movq	%rcx, -160(%rbp)
	movl	%r15d, %edx
	call	_ZNK6icu_6713UnicodeString9doExtractEiiPDsi@PLT
	movq	-160(%rbp), %rcx
	xorl	%eax, %eax
	movw	%ax, 148(%rbx,%r15,2)
	movq	-152(%rbp), %rax
	movl	%r12d, %r15d
	movl	$65545, (%rax)
	jmp	.L629
	.p2align 4,,10
	.p2align 3
.L632:
	movl	20(%rdi), %edx
	jmp	.L633
	.p2align 4,,10
	.p2align 3
.L635:
	movl	12(%r13), %esi
	jmp	.L636
.L643:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2509:
	.size	_ZN6icu_678RuleHalf5parseERKNS_13UnicodeStringEiiR10UErrorCode, .-_ZN6icu_678RuleHalf5parseERKNS_13UnicodeStringEiiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720TransliteratorParser9parseRuleERKNS_13UnicodeStringEiiR10UErrorCode
	.type	_ZN6icu_6720TransliteratorParser9parseRuleERKNS_13UnicodeStringEiiR10UErrorCode, @function
_ZN6icu_6720TransliteratorParser9parseRuleERKNS_13UnicodeStringEiiR10UErrorCode:
.LFB2532:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$312, %rsp
	movq	%r8, -296(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzwl	328(%rdi), %eax
	testb	$1, %al
	je	.L645
	leaq	320(%rdi), %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
.L646:
	leaq	384(%r14), %rax
	movq	%rax, %rdi
	movq	%rax, -312(%rbp)
	call	_ZN6icu_677UVector17removeAllElementsEv@PLT
	movl	$2, %edx
	xorl	%ecx, %ecx
	xorl	%edi, %edi
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %esi
	movw	%di, -84(%rbp)
	movdqa	.LC0(%rip), %xmm0
	movw	%dx, -272(%rbp)
	movzwl	440(%r14), %edx
	movq	%rax, -280(%rbp)
	movq	-296(%rbp), %r8
	movq	%rax, -168(%rbp)
	movl	%edx, %eax
	andl	$31, %eax
	andl	$1, %edx
	movl	$2, %edx
	movw	%cx, -196(%rbp)
	cmovne	%edx, %eax
	movl	%ebx, %ecx
	movl	%r12d, %edx
	movq	%r14, -72(%rbp)
	movw	%si, -160(%rbp)
	movq	%r13, %rsi
	movw	%ax, 440(%r14)
	leaq	-288(%rbp), %rax
	movq	%rax, %rdi
	movups	%xmm0, -104(%rbp)
	movq	%r14, -184(%rbp)
	movl	$0, -200(%rbp)
	movl	$1, -192(%rbp)
	movl	$0, -88(%rbp)
	movl	$1, -80(%rbp)
	movq	%rax, -320(%rbp)
	movups	%xmm0, -216(%rbp)
	call	_ZN6icu_678RuleHalf5parseERKNS_13UnicodeStringEiiR10UErrorCode
	movl	%eax, %r15d
	movq	-296(%rbp), %rax
	movl	(%rax), %r8d
	testl	%r8d, %r8d
	jg	.L777
	cmpl	%r15d, %ebx
	je	.L652
	movzwl	8(%r13), %ecx
	leal	-1(%r15), %eax
	testw	%cx, %cx
	js	.L653
	movswl	%cx, %esi
	sarl	$5, %esi
.L654:
	cmpl	%esi, %eax
	jnb	.L655
	andl	$2, %ecx
	leaq	10(%r13), %rcx
	je	.L781
.L657:
	cltq
	leaq	_ZL10gOPERATORS(%rip), %rdi
	leaq	(%rax,%rax), %rsi
	movq	%rsi, -328(%rbp)
	movzwl	(%rcx,%rax,2), %esi
	movw	%si, -304(%rbp)
	call	u_strchr_67@PLT
	testq	%rax, %rax
	je	.L652
	cmpw	$60, -304(%rbp)
	jne	.L660
	cmpl	%r15d, %ebx
	jle	.L660
	movzwl	8(%r13), %eax
	testw	%ax, %ax
	js	.L663
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L664:
	movl	$60, %r10d
	movw	%r10w, -304(%rbp)
	cmpl	%r15d, %ecx
	jbe	.L665
	testb	$2, %al
	je	.L666
	leaq	10(%r13), %rax
.L667:
	movq	-328(%rbp), %rcx
	cmpw	$62, 2(%rax,%rcx)
	je	.L668
	movl	$60, %r9d
	movw	%r9w, -304(%rbp)
	jmp	.L665
	.p2align 4,,10
	.p2align 3
.L655:
	movl	$65535, %esi
	leaq	_ZL10gOPERATORS(%rip), %rdi
	call	u_strchr_67@PLT
	testq	%rax, %rax
	jne	.L782
.L652:
	movl	%r12d, 112(%r14)
	leal	-15(%r12), %edi
	xorl	%esi, %esi
	movl	%r12d, %ebx
	movl	$0, 108(%r14)
	leaq	116(%r14), %r15
	call	uprv_max_67@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%r13, %rdi
	subl	%eax, %ebx
	movl	%eax, %esi
	movl	%ebx, %edx
	call	_ZNK6icu_6713UnicodeString9doExtractEiiPDsi@PLT
	xorl	%r15d, %r15d
	movslq	%ebx, %rbx
	movw	%r15w, 116(%r14,%rbx,2)
	movswl	8(%r13), %esi
	testw	%si, %si
	js	.L661
	sarl	$5, %esi
.L662:
	leal	15(%r12), %edi
	leaq	148(%r14), %r15
	call	uprv_min_67@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	%r12d, %esi
	subl	%r12d, %eax
	movq	%r13, %rdi
	movslq	%eax, %rbx
	movl	%ebx, %edx
	call	_ZNK6icu_6713UnicodeString9doExtractEiiPDsi@PLT
	movq	-296(%rbp), %rax
	xorl	%r11d, %r11d
	movw	%r11w, 148(%r14,%rbx,2)
	movl	$65547, (%rax)
.L777:
	movl	%r12d, %r15d
	leaq	-168(%rbp), %r8
.L651:
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	-280(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L783
	leaq	-40(%rbp), %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L645:
	.cfi_restore_state
	testw	%ax, %ax
	js	.L647
	movswl	%ax, %edx
	sarl	$5, %edx
.L648:
	testl	%edx, %edx
	je	.L646
	andl	$31, %eax
	movw	%ax, 328(%r14)
	jmp	.L646
	.p2align 4,,10
	.p2align 3
.L647:
	movl	332(%rdi), %edx
	jmp	.L648
	.p2align 4,,10
	.p2align 3
.L660:
	movzwl	-304(%rbp), %ecx
	cmpw	$8594, %cx
	je	.L741
	cmpw	$8596, %cx
	jne	.L784
	movl	$126, %esi
	movw	%si, -304(%rbp)
	jmp	.L665
	.p2align 4,,10
	.p2align 3
.L784:
	cmpw	$8592, %cx
	movl	$60, %eax
	cmovne	%ecx, %eax
	movw	%ax, -304(%rbp)
.L665:
	leaq	-176(%rbp), %rax
	movl	%ebx, %ecx
	movl	%r15d, %edx
	movq	%r13, %rsi
	movq	-296(%rbp), %r8
	movq	%rax, %rdi
	movq	%rax, -328(%rbp)
	call	_ZN6icu_678RuleHalf5parseERKNS_13UnicodeStringEiiR10UErrorCode
	movl	%eax, %r15d
	movq	-296(%rbp), %rax
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	jg	.L777
	cmpl	%r15d, %ebx
	jg	.L785
.L670:
	cmpw	$61, -304(%rbp)
	movswl	440(%r14), %r9d
	je	.L786
	testw	%r9w, %r9w
	js	.L696
	movl	%r9d, %ebx
	sarl	$5, %ebx
.L697:
	testl	%ebx, %ebx
	jne	.L787
	movzwl	328(%r14), %edx
	testw	%dx, %dx
	js	.L701
	movswl	%dx, %eax
	sarl	$5, %eax
.L702:
	cmpl	%eax, 392(%r14)
	jl	.L788
.L703:
	xorl	%r11d, %r11d
	leal	-15(%r12), %eax
	movl	%r15d, -352(%rbp)
	movq	%r14, %r15
	movl	%eax, -344(%rbp)
	movq	%r11, %r14
	movl	%ebx, -348(%rbp)
	jmp	.L714
	.p2align 4,,10
	.p2align 3
.L790:
	movswl	%dx, %eax
	sarl	$5, %eax
.L707:
	cmpl	%r14d, %eax
	jle	.L708
	jbe	.L709
	leaq	330(%r15), %rax
	testb	$2, %dl
	jne	.L711
	movq	344(%r15), %rax
.L711:
	cmpw	$0, (%rax,%r14,2)
	je	.L789
.L709:
	addq	$1, %r14
.L714:
	testw	%dx, %dx
	jns	.L790
	movl	332(%r15), %eax
	jmp	.L707
	.p2align 4,,10
	.p2align 3
.L781:
	movq	24(%r13), %rcx
	jmp	.L657
	.p2align 4,,10
	.p2align 3
.L661:
	movl	12(%r13), %esi
	jmp	.L662
	.p2align 4,,10
	.p2align 3
.L785:
	movzwl	8(%r13), %edx
	leal	-1(%r15), %eax
	testw	%dx, %dx
	js	.L671
	movswl	%dx, %ecx
	sarl	$5, %ecx
.L672:
	cmpl	%eax, %ecx
	jbe	.L673
	andl	$2, %edx
	leaq	10(%r13), %rdx
	je	.L791
.L675:
	cltq
	cmpw	$59, (%rdx,%rax,2)
	je	.L670
.L673:
	movl	%r12d, 112(%r14)
	leal	-15(%r12), %edi
	xorl	%esi, %esi
	movl	%r12d, %ebx
	movl	$0, 108(%r14)
	leaq	116(%r14), %r15
	call	uprv_max_67@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%r13, %rdi
	subl	%eax, %ebx
	movl	%eax, %esi
	movl	%ebx, %edx
	call	_ZNK6icu_6713UnicodeString9doExtractEiiPDsi@PLT
	xorl	%edx, %edx
	movslq	%ebx, %rbx
	movw	%dx, 116(%r14,%rbx,2)
	movzwl	8(%r13), %eax
	testw	%ax, %ax
	js	.L676
	movswl	%ax, %esi
	sarl	$5, %esi
.L677:
	leal	15(%r12), %edi
	leaq	148(%r14), %r15
	call	uprv_min_67@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	%r12d, %esi
	subl	%r12d, %eax
	movq	%r13, %rdi
	movslq	%eax, %rbx
	movl	%ebx, %edx
	call	_ZNK6icu_6713UnicodeString9doExtractEiiPDsi@PLT
	xorl	%eax, %eax
	movl	%r12d, %r15d
	leaq	-168(%rbp), %r8
	movw	%ax, 148(%r14,%rbx,2)
	movq	-296(%rbp), %rax
	movl	$65555, (%rax)
	jmp	.L651
	.p2align 4,,10
	.p2align 3
.L741:
	movl	$62, %edi
	movw	%di, -304(%rbp)
	jmp	.L665
	.p2align 4,,10
	.p2align 3
.L653:
	movl	12(%r13), %esi
	jmp	.L654
	.p2align 4,,10
	.p2align 3
.L663:
	movl	12(%r13), %ecx
	jmp	.L664
	.p2align 4,,10
	.p2align 3
.L666:
	movq	24(%r13), %rax
	jmp	.L667
	.p2align 4,,10
	.p2align 3
.L668:
	movl	$126, %r8d
	addl	$1, %r15d
	movw	%r8w, -304(%rbp)
	jmp	.L665
	.p2align 4,,10
	.p2align 3
.L791:
	movq	24(%r13), %rdx
	jmp	.L675
	.p2align 4,,10
	.p2align 3
.L676:
	movl	12(%r13), %esi
	jmp	.L677
	.p2align 4,,10
	.p2align 3
.L701:
	movl	332(%r14), %eax
	jmp	.L702
	.p2align 4,,10
	.p2align 3
.L789:
	movl	%r12d, 112(%r15)
	movl	-344(%rbp), %edi
	xorl	%esi, %esi
	movl	%r12d, %ebx
	movl	$0, 108(%r15)
	call	uprv_max_67@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	movl	%eax, %esi
	leaq	116(%r15), %rax
	subl	%esi, %ebx
	movq	%rax, %rcx
	movq	%rax, -336(%rbp)
	movl	%ebx, %edx
	call	_ZNK6icu_6713UnicodeString9doExtractEiiPDsi@PLT
	movq	-336(%rbp), %rax
	xorl	%r8d, %r8d
	movslq	%ebx, %rdx
	movw	%r8w, 116(%r15,%rdx,2)
	movswl	8(%r13), %esi
	testw	%si, %si
	js	.L712
	sarl	$5, %esi
.L713:
	leal	15(%r12), %edi
	call	uprv_min_67@PLT
	xorl	%r8d, %r8d
	movl	%r12d, %esi
	movq	%r13, %rdi
	subl	%r12d, %eax
	leaq	148(%r15), %rcx
	movl	%eax, %edx
	movq	%rcx, -336(%rbp)
	movl	%eax, %ebx
	call	_ZNK6icu_6713UnicodeString9doExtractEiiPDsi@PLT
	movq	-336(%rbp), %rcx
	xorl	%edi, %edi
	movslq	%ebx, %rax
	movw	%di, 148(%r15,%rax,2)
	movq	-296(%rbp), %rax
	movzwl	328(%r15), %edx
	movl	$65568, (%rax)
	jmp	.L709
	.p2align 4,,10
	.p2align 3
.L708:
	movq	%r15, %r14
	leal	-15(%r12), %eax
	movl	-348(%rbp), %ebx
	movl	-352(%rbp), %r15d
	movl	392(%r14), %esi
	movl	%eax, -348(%rbp)
	testl	%esi, %esi
	jle	.L721
	movl	%r15d, -352(%rbp)
	movl	%ebx, %r15d
	movq	-312(%rbp), %rbx
	jmp	.L715
	.p2align 4,,10
	.p2align 3
.L718:
	addl	$1, %r15d
	cmpl	392(%r14), %r15d
	jge	.L792
.L715:
	movl	%r15d, %esi
	movq	%rbx, %rdi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	testq	%rax, %rax
	jne	.L718
	movl	%r12d, 112(%r14)
	movl	-348(%rbp), %edi
	xorl	%esi, %esi
	movl	$0, 108(%r14)
	call	uprv_max_67@PLT
	movl	%r12d, %edx
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	movl	%eax, %esi
	leaq	116(%r14), %rax
	subl	%esi, %edx
	movq	%rax, %rcx
	movq	%rax, -344(%rbp)
	movl	%edx, -336(%rbp)
	call	_ZNK6icu_6713UnicodeString9doExtractEiiPDsi@PLT
	movq	-344(%rbp), %rax
	movslq	-336(%rbp), %rdx
	xorl	%ecx, %ecx
	movw	%cx, 116(%r14,%rdx,2)
	movzwl	8(%r13), %eax
	testw	%ax, %ax
	js	.L719
	movswl	%ax, %esi
	sarl	$5, %esi
.L720:
	leal	15(%r12), %edi
	call	uprv_min_67@PLT
	xorl	%r8d, %r8d
	movl	%r12d, %esi
	movq	%r13, %rdi
	subl	%r12d, %eax
	leaq	148(%r14), %rcx
	movl	%eax, %edx
	movq	%rcx, -344(%rbp)
	movl	%eax, -336(%rbp)
	call	_ZNK6icu_6713UnicodeString9doExtractEiiPDsi@PLT
	movq	-344(%rbp), %rcx
	movslq	-336(%rbp), %rax
	xorl	%edx, %edx
	movw	%dx, 148(%r14,%rax,2)
	movq	-296(%rbp), %rax
	movl	$65568, (%rax)
	jmp	.L718
	.p2align 4,,10
	.p2align 3
.L792:
	movl	-352(%rbp), %r15d
.L721:
	cmpw	$126, -304(%rbp)
	movl	104(%r14), %eax
	je	.L793
	testl	%eax, %eax
	leaq	-168(%rbp), %r8
	sete	%cl
	cmpw	$62, -304(%rbp)
	sete	%dl
	cmpb	%dl, %cl
	jne	.L651
	movq	-320(%rbp), %rcx
	movq	-328(%rbp), %rbx
	cmpl	$1, %eax
	movq	%rcx, %rdx
	cmovne	%rbx, %rdx
	cmovne	%rcx, %rbx
.L737:
	movl	76(%rbx), %eax
	testl	%eax, %eax
	jns	.L723
	movl	$0, 76(%rbx)
.L723:
	movl	80(%rbx), %r11d
	testl	%r11d, %r11d
	js	.L794
.L724:
	movl	76(%rdx), %r10d
	testl	%r10d, %r10d
	jns	.L727
	movl	80(%rdx), %r9d
	testl	%r9d, %r9d
	js	.L795
.L727:
	movl	%r12d, 112(%r14)
	leal	-15(%r12), %edi
	xorl	%esi, %esi
	movl	%r12d, %ebx
	movl	$0, 108(%r14)
	leaq	116(%r14), %r15
	call	uprv_max_67@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%r13, %rdi
	subl	%eax, %ebx
	movl	%eax, %esi
	movl	%ebx, %edx
	call	_ZNK6icu_6713UnicodeString9doExtractEiiPDsi@PLT
	xorl	%ecx, %ecx
	movslq	%ebx, %rbx
	movw	%cx, 116(%r14,%rbx,2)
	movzwl	8(%r13), %eax
	testw	%ax, %ax
	js	.L731
	movswl	%ax, %esi
	sarl	$5, %esi
.L732:
	leal	15(%r12), %edi
	leaq	148(%r14), %r15
	call	uprv_min_67@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	%r12d, %esi
	subl	%r12d, %eax
	movq	%r13, %rdi
	movslq	%eax, %rbx
	movl	%ebx, %edx
	call	_ZNK6icu_6713UnicodeString9doExtractEiiPDsi@PLT
	movq	-296(%rbp), %rax
	xorl	%edx, %edx
	movl	%r12d, %r15d
	leaq	-168(%rbp), %r8
	movw	%dx, 148(%r14,%rbx,2)
	movl	$65537, (%rax)
	jmp	.L651
	.p2align 4,,10
	.p2align 3
.L712:
	movl	12(%r13), %esi
	jmp	.L713
	.p2align 4,,10
	.p2align 3
.L786:
	testw	%r9w, %r9w
	js	.L679
	sarl	$5, %r9d
.L680:
	testl	%r9d, %r9d
	je	.L796
	movzwl	-272(%rbp), %edx
	testw	%dx, %dx
	js	.L684
	movswl	%dx, %eax
	sarl	$5, %eax
.L685:
	cmpl	$1, %eax
	jne	.L688
	leaq	-270(%rbp), %rax
	andl	$2, %edx
	cmove	-256(%rbp), %rax
	movzwl	(%rax), %eax
	cmpw	%ax, 426(%r14)
	je	.L797
.L688:
	movl	%r12d, 112(%r14)
	leal	-15(%r12), %edi
	xorl	%esi, %esi
	movl	%r12d, %ebx
	movl	$0, 108(%r14)
	leaq	116(%r14), %r15
	call	uprv_max_67@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%r13, %rdi
	subl	%eax, %ebx
	movl	%eax, %esi
	movl	%ebx, %edx
	call	_ZNK6icu_6713UnicodeString9doExtractEiiPDsi@PLT
	xorl	%eax, %eax
	movslq	%ebx, %rbx
	movw	%ax, 116(%r14,%rbx,2)
	movzwl	8(%r13), %eax
	testw	%ax, %ax
	js	.L692
	movswl	%ax, %esi
	sarl	$5, %esi
.L693:
	leal	15(%r12), %edi
	leaq	148(%r14), %r15
	call	uprv_min_67@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	%r12d, %esi
	subl	%r12d, %eax
	movq	%r13, %rdi
	movslq	%eax, %rbx
	movl	%ebx, %edx
	call	_ZNK6icu_6713UnicodeString9doExtractEiiPDsi@PLT
	xorl	%eax, %eax
	movl	%r12d, %r15d
	leaq	-168(%rbp), %r8
	movw	%ax, 148(%r14,%rbx,2)
	movq	-296(%rbp), %rax
	movl	$65541, (%rax)
	jmp	.L651
	.p2align 4,,10
	.p2align 3
.L671:
	movl	12(%r13), %ecx
	jmp	.L672
	.p2align 4,,10
	.p2align 3
.L696:
	movl	444(%r14), %ebx
	jmp	.L697
	.p2align 4,,10
	.p2align 3
.L719:
	movl	12(%r13), %esi
	jmp	.L720
	.p2align 4,,10
	.p2align 3
.L787:
	movl	%r12d, 112(%r14)
	leal	-15(%r12), %edi
	xorl	%esi, %esi
	movl	%r12d, %ebx
	movl	$0, 108(%r14)
	leaq	116(%r14), %r15
	call	uprv_max_67@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%r13, %rdi
	subl	%eax, %ebx
	movl	%eax, %esi
	movl	%ebx, %edx
	call	_ZNK6icu_6713UnicodeString9doExtractEiiPDsi@PLT
	xorl	%r15d, %r15d
	movslq	%ebx, %rbx
	movw	%r15w, 116(%r14,%rbx,2)
	movzwl	8(%r13), %eax
	testw	%ax, %ax
	js	.L699
	movswl	%ax, %esi
	sarl	$5, %esi
.L700:
	leal	15(%r12), %edi
	leaq	148(%r14), %r15
	call	uprv_min_67@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	%r12d, %esi
	subl	%r12d, %eax
	movq	%r13, %rdi
	movslq	%eax, %rbx
	movl	%ebx, %edx
	call	_ZNK6icu_6713UnicodeString9doExtractEiiPDsi@PLT
	movq	-296(%rbp), %rax
	xorl	%r11d, %r11d
	movl	%r12d, %r15d
	leaq	-168(%rbp), %r8
	movw	%r11w, 148(%r14,%rbx,2)
	movl	$65554, (%rax)
	jmp	.L651
	.p2align 4,,10
	.p2align 3
.L793:
	movq	-328(%rbp), %rcx
	movq	-320(%rbp), %rbx
	cmpl	$1, %eax
	movq	%rcx, %rdx
	cmove	%rbx, %rdx
	cmove	%rcx, %rbx
	movq	%rdx, %rdi
	movq	%rdx, -304(%rbp)
	call	_ZN6icu_678RuleHalf13removeContextEv
	movl	$-1, 72(%rbx)
	movq	-304(%rbp), %rdx
	movl	$0, 84(%rbx)
	jmp	.L737
	.p2align 4,,10
	.p2align 3
.L788:
	movl	%r12d, 112(%r14)
	leal	-15(%r12), %edi
	xorl	%esi, %esi
	movl	$0, 108(%r14)
	call	uprv_max_67@PLT
	movl	%r12d, %edx
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	movl	%eax, %esi
	leaq	116(%r14), %rax
	subl	%esi, %edx
	movq	%rax, %rcx
	movq	%rax, -344(%rbp)
	movl	%edx, -336(%rbp)
	call	_ZNK6icu_6713UnicodeString9doExtractEiiPDsi@PLT
	movq	-344(%rbp), %rax
	movslq	-336(%rbp), %rax
	xorl	%r10d, %r10d
	movw	%r10w, 116(%r14,%rax,2)
	movzwl	8(%r13), %eax
	testw	%ax, %ax
	js	.L704
	movswl	%ax, %esi
	sarl	$5, %esi
.L705:
	leal	15(%r12), %edi
	call	uprv_min_67@PLT
	xorl	%r8d, %r8d
	movl	%r12d, %esi
	movq	%r13, %rdi
	subl	%r12d, %eax
	leaq	148(%r14), %rcx
	movl	%eax, %edx
	movq	%rcx, -344(%rbp)
	movl	%eax, -336(%rbp)
	call	_ZNK6icu_6713UnicodeString9doExtractEiiPDsi@PLT
	movq	-344(%rbp), %rcx
	movslq	-336(%rbp), %rax
	xorl	%r9d, %r9d
	movw	%r9w, 148(%r14,%rax,2)
	movq	-296(%rbp), %rax
	movzwl	328(%r14), %edx
	movl	$65553, (%rax)
	jmp	.L703
	.p2align 4,,10
	.p2align 3
.L679:
	movl	444(%r14), %r9d
	jmp	.L680
	.p2align 4,,10
	.p2align 3
.L731:
	movl	12(%r13), %esi
	jmp	.L732
	.p2align 4,,10
	.p2align 3
.L794:
	movswl	16(%rbx), %eax
	testw	%ax, %ax
	js	.L725
	sarl	$5, %eax
.L726:
	movl	%eax, 80(%rbx)
	jmp	.L724
	.p2align 4,,10
	.p2align 3
.L796:
	movl	%r12d, 112(%r14)
	leal	-15(%r12), %edi
	xorl	%esi, %esi
	movl	%r12d, %ebx
	movl	$0, 108(%r14)
	leaq	116(%r14), %r15
	call	uprv_max_67@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%r13, %rdi
	subl	%eax, %ebx
	movl	%eax, %esi
	movl	%ebx, %edx
	call	_ZNK6icu_6713UnicodeString9doExtractEiiPDsi@PLT
	xorl	%eax, %eax
	movslq	%ebx, %rbx
	movw	%ax, 116(%r14,%rbx,2)
	movzwl	8(%r13), %eax
	testw	%ax, %ax
	js	.L682
	movswl	%ax, %esi
	sarl	$5, %esi
.L683:
	leal	15(%r12), %edi
	leaq	148(%r14), %r15
	call	uprv_min_67@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	%r12d, %esi
	subl	%r12d, %eax
	movq	%r13, %rdi
	movslq	%eax, %rbx
	movl	%ebx, %edx
	call	_ZNK6icu_6713UnicodeString9doExtractEiiPDsi@PLT
	xorl	%eax, %eax
	movl	%r12d, %r15d
	leaq	-168(%rbp), %r8
	movw	%ax, 148(%r14,%rbx,2)
	movq	-296(%rbp), %rax
	movl	$65536, (%rax)
	jmp	.L651
	.p2align 4,,10
	.p2align 3
.L684:
	movl	-268(%rbp), %eax
	jmp	.L685
	.p2align 4,,10
	.p2align 3
.L692:
	movl	12(%r13), %esi
	jmp	.L693
	.p2align 4,,10
	.p2align 3
.L699:
	movl	12(%r13), %esi
	jmp	.L700
.L795:
	movl	72(%rbx), %r8d
	testl	%r8d, %r8d
	jns	.L727
	movl	84(%rdx), %edi
	testl	%edi, %edi
	je	.L728
	movl	72(%rdx), %esi
	testl	%esi, %esi
	js	.L727
.L728:
	cmpw	$0, 92(%rdx)
	jne	.L727
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	%rdx, -304(%rbp)
	call	_ZN6icu_678RuleHalf12isValidInputERNS_20TransliteratorParserE
	movq	-304(%rbp), %rdx
	testb	%al, %al
	je	.L727
	movq	%r14, %rsi
	movq	%rdx, %rdi
	call	_ZN6icu_678RuleHalf13isValidOutputERNS_20TransliteratorParserE
	testb	%al, %al
	je	.L727
	movl	80(%rbx), %eax
	cmpl	%eax, 76(%rbx)
	jg	.L727
	movslq	392(%r14), %rcx
	xorl	%r8d, %r8d
	movq	-304(%rbp), %rdx
	testl	%ecx, %ecx
	jle	.L730
	leaq	0(,%rcx,8), %rdi
	movq	%rdx, -304(%rbp)
	call	uprv_malloc_67@PLT
	movq	-304(%rbp), %rdx
	testq	%rax, %rax
	je	.L778
	movq	-312(%rbp), %rdi
	movq	%rax, %rsi
	movq	%rdx, -320(%rbp)
	movq	%rax, -304(%rbp)
	call	_ZNK6icu_677UVector7toArrayEPPv@PLT
	movl	392(%r14), %ecx
	movq	-320(%rbp), %rdx
	movq	-304(%rbp), %r8
.L730:
	movl	$136, %edi
	movq	%r8, -312(%rbp)
	movl	%ecx, -320(%rbp)
	movq	%rdx, -304(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	-312(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %r11
	je	.L734
	movl	80(%rbx), %r13d
	movl	76(%rbx), %r12d
	movq	%r11, %rdi
	leaq	8(%rbx), %rsi
	movq	-304(%rbp), %rdx
	movl	-320(%rbp), %ecx
	movq	%r11, -304(%rbp)
	movl	72(%rdx), %r9d
	pushq	%rax
	pushq	-296(%rbp)
	pushq	96(%r14)
	movsbl	93(%rbx), %eax
	pushq	%rax
	movsbl	92(%rbx), %eax
	pushq	%rax
	pushq	%rcx
	movl	84(%rdx), %eax
	movl	%r13d, %ecx
	pushq	%r8
	leaq	8(%rdx), %r8
	movl	%r12d, %edx
	pushq	%rax
	call	_ZN6icu_6719TransliterationRuleC1ERKNS_13UnicodeStringEiiS3_iiPPNS_14UnicodeFunctorEiaaPKNS_23TransliterationRuleDataER10UErrorCode@PLT
	movq	96(%r14), %rdi
	movq	-304(%rbp), %r11
	addq	$64, %rsp
	movq	-296(%rbp), %rdx
	addq	$8, %rdi
	movq	%r11, %rsi
	call	_ZN6icu_6722TransliterationRuleSet7addRuleEPNS_19TransliterationRuleER10UErrorCode@PLT
	leaq	-168(%rbp), %r8
	jmp	.L651
	.p2align 4,,10
	.p2align 3
.L704:
	movl	12(%r13), %esi
	jmp	.L705
.L725:
	movl	20(%rbx), %eax
	jmp	.L726
.L682:
	movl	12(%r13), %esi
	jmp	.L683
.L797:
	movzwl	-196(%rbp), %eax
	orw	-84(%rbp), %ax
	jne	.L688
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L778
	leaq	-168(%rbp), %r8
	movq	%rax, %rdi
	movq	%r8, %rsi
	movq	%r8, -304(%rbp)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	-304(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L695
	leaq	432(%r14), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	-304(%rbp), %r8
.L695:
	movq	232(%r14), %rdi
	movq	-296(%rbp), %rcx
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r8, -304(%rbp)
	call	uhash_put_67@PLT
	movq	-304(%rbp), %r8
	addw	$1, 426(%r14)
	jmp	.L651
.L783:
	call	__stack_chk_fail@PLT
.L782:
	movw	$-1, -304(%rbp)
	jmp	.L665
.L734:
	movq	%r8, %rdi
	call	uprv_free_67@PLT
.L778:
	movq	-296(%rbp), %r8
	movl	%r12d, %ecx
	movq	%r13, %rdx
	movq	%r14, %rdi
	movl	$7, %esi
	call	_ZN6icu_6720TransliteratorParser11syntaxErrorE10UErrorCodeRKNS_13UnicodeStringEiRS1_
	leaq	-168(%rbp), %r8
	movl	%eax, %r15d
	jmp	.L651
	.cfi_endproc
.LFE2532:
	.size	_ZN6icu_6720TransliteratorParser9parseRuleERKNS_13UnicodeStringEiiR10UErrorCode, .-_ZN6icu_6720TransliteratorParser9parseRuleERKNS_13UnicodeStringEiiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720TransliteratorParser10parseRulesERKNS_13UnicodeStringE15UTransDirectionR10UErrorCode
	.type	_ZN6icu_6720TransliteratorParser10parseRulesERKNS_13UnicodeStringE15UTransDirectionR10UErrorCode, @function
_ZN6icu_6720TransliteratorParser10parseRulesERKNS_13UnicodeStringE15UTransDirectionR10UErrorCode:
.LFB2525:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	leaq	8(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$312, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	116(%rdi), %rax
	movups	%xmm0, 116(%rdi)
	movq	%rax, -320(%rbp)
	movq	$-1, 108(%rdi)
	movups	%xmm0, 132(%rdi)
	movups	%xmm0, 148(%rdi)
	movups	%xmm0, 164(%rdi)
	jmp	.L800
	.p2align 4,,10
	.p2align 3
.L984:
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN6icu_677UVector15orphanElementAtEi@PLT
	testq	%rax, %rax
	jne	.L983
.L800:
	movl	16(%rbx), %edi
	testl	%edi, %edi
	jne	.L984
	movl	0(%r13), %esi
	testl	%esi, %esi
	jle	.L985
.L798:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L986
	addq	$312, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L983:
	.cfi_restore_state
	movq	%rax, %rdi
	movq	%rax, -280(%rbp)
	call	_ZN6icu_6723TransliterationRuleDataD1Ev@PLT
	movq	-280(%rbp), %rax
	movq	%rax, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	jmp	.L800
	.p2align 4,,10
	.p2align 3
.L985:
	leaq	48(%rbx), %rax
	movq	%rax, %rdi
	movq	%rax, -328(%rbp)
	call	_ZN6icu_677UVector17removeAllElementsEv@PLT
	movq	88(%rbx), %rdi
	movl	%r15d, 104(%rbx)
	movq	$0, 96(%rbx)
	testq	%rdi, %rdi
	je	.L803
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
.L803:
	movq	$0, 88(%rbx)
	leaq	192(%rbx), %r14
	jmp	.L805
	.p2align 4,,10
	.p2align 3
.L988:
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN6icu_677UVector15orphanElementAtEi@PLT
	testq	%rax, %rax
	jne	.L987
.L805:
	movl	200(%rbx), %eax
	testl	%eax, %eax
	jne	.L988
	movq	232(%rbx), %rdi
	movl	%eax, -344(%rbp)
	call	uhash_removeAll_67@PLT
	movl	$32, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L807
	leaq	192(%rbx), %rdx
	leaq	16+_ZTVN6icu_679ParseDataE(%rip), %rcx
	movq	$0, 8(%rax)
	movq	%rdx, 16(%rax)
	movq	%rdx, -336(%rbp)
	leaq	232(%rbx), %rdx
	movq	%rcx, (%rax)
	movl	$2, %ecx
	movq	%rdx, 24(%rax)
	movl	$2, %edx
	movq	%rax, 184(%rbx)
	movl	$-1, %eax
	movw	%ax, 496(%rbx)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, -256(%rbp)
	movq	%rax, -192(%rbp)
	movswl	8(%r12), %eax
	movw	%dx, -248(%rbp)
	movw	%cx, -184(%rbp)
	testw	%ax, %ax
	js	.L808
	sarl	$5, %eax
.L809:
	movq	$0, 88(%rbx)
	movl	%eax, %r15d
	testl	%eax, %eax
	jle	.L909
	leaq	-192(%rbp), %rcx
	movl	0(%r13), %eax
	xorl	%r14d, %r14d
	movl	$0, -300(%rbp)
	movl	$-1, -340(%rbp)
	movb	$1, -296(%rbp)
	movq	%rcx, -312(%rbp)
	jmp	.L811
	.p2align 4,,10
	.p2align 3
.L993:
	movswl	%ax, %ecx
	sarl	$5, %ecx
	cmpl	%r14d, %ecx
	jbe	.L989
.L815:
	leaq	10(%r12), %rcx
	testb	$2, %al
	jne	.L820
	movq	24(%r12), %rcx
.L820:
	movslq	%r14d, %rax
	movl	%edx, -280(%rbp)
	movzwl	(%rcx,%rax,2), %edi
	movl	%edi, -288(%rbp)
	call	_ZN6icu_6712PatternProps12isWhiteSpaceEi@PLT
	movl	-280(%rbp), %edx
	testb	%al, %al
	jne	.L816
	movl	-288(%rbp), %ecx
	cmpw	$35, %cx
	je	.L990
	cmpw	$59, %cx
	je	.L816
	leal	2(%r14), %r10d
	addl	$1, -300(%rbp)
	cmpl	%r15d, %r10d
	jl	.L991
.L818:
	cmpb	$0, -296(%rbp)
	je	.L831
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L982
	movq	-312(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rax, -280(%rbp)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movl	104(%rbx), %esi
	movq	-280(%rbp), %r8
	testl	%esi, %esi
	jne	.L868
	movq	-328(%rbp), %rdi
	movq	%r13, %rdx
	movq	%r8, %rsi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
.L869:
	movzwl	-184(%rbp), %edx
	movl	$1168, %edi
	movl	%edx, %eax
	andl	$31, %eax
	andl	$1, %edx
	movl	$2, %edx
	cmovne	%edx, %eax
	movw	%ax, -184(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L871
	movq	%rax, %rdi
	movq	%r13, %rsi
	movq	%rax, -280(%rbp)
	call	_ZN6icu_6723TransliterationRuleDataC1ER10UErrorCode@PLT
	movq	-280(%rbp), %rax
	movq	184(%rbx), %rdx
	movl	16(%rbx), %ecx
	movq	%rax, 96(%rbx)
	movq	%rax, 8(%rdx)
	movl	$-4096, %edx
	movw	%dx, 1162(%rax)
	testl	%ecx, %ecx
	jne	.L831
	movl	$-117379072, 424(%rbx)
.L831:
	leaq	-128(%rbp), %r9
	movl	$4, %ecx
	movl	$1, %esi
	leaq	_ZN6icu_67L10PRAGMA_USEE(%rip), %rax
	leaq	-264(%rbp), %rdx
	movq	%r9, %rdi
	movq	%r9, -280(%rbp)
	movq	%rax, -264(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	xorl	%r8d, %r8d
	movl	%r15d, %edx
	movl	%r14d, %esi
	movq	-280(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rcx
	movq	%r9, -288(%rbp)
	call	_ZN6icu_6711ICU_Utility12parsePatternERKNS_13UnicodeStringEiiS3_Pi@PLT
	movq	-288(%rbp), %r9
	movl	%eax, -280(%rbp)
	movq	%r9, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-280(%rbp), %eax
	movq	%r13, %r8
	movl	%r15d, %ecx
	movl	%r14d, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	testl	%eax, %eax
	js	.L872
	call	_ZN6icu_6720TransliteratorParser11parsePragmaERKNS_13UnicodeStringEiiR10UErrorCode
	movl	%eax, %edx
	testl	%eax, %eax
	js	.L992
	movb	$0, -296(%rbp)
	.p2align 4,,10
	.p2align 3
.L816:
	cmpl	%r15d, %edx
	jge	.L812
	movl	0(%r13), %eax
	movl	%edx, %r14d
.L811:
	testl	%eax, %eax
	jg	.L812
	movzwl	8(%r12), %eax
	leal	1(%r14), %edx
	testw	%ax, %ax
	jns	.L993
	movl	12(%r12), %ecx
	cmpl	%r14d, %ecx
	ja	.L815
.L989:
	movl	$65535, %edi
	movl	%edx, -280(%rbp)
	call	_ZN6icu_6712PatternProps12isWhiteSpaceEi@PLT
	movl	-280(%rbp), %edx
	testb	%al, %al
	jne	.L816
	leal	2(%r14), %r10d
	addl	$1, -300(%rbp)
	cmpl	%r15d, %r10d
	jge	.L818
.L991:
	xorl	%r8d, %r8d
	movl	$2, %r9d
	movl	%r14d, %esi
	movq	%r12, %rdi
	leaq	_ZL8ID_TOKEN(%rip), %rcx
	movl	$2, %edx
	movl	%r10d, -280(%rbp)
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	movl	-280(%rbp), %r10d
	testb	%al, %al
	jne	.L818
	movzwl	8(%r12), %eax
	testw	%ax, %ax
	js	.L832
	movswl	%ax, %edx
	sarl	$5, %edx
.L833:
	cmpl	%r10d, %edx
	jbe	.L911
	testb	$2, %al
	jne	.L994
	movq	24(%r12), %rax
.L836:
	movslq	%r10d, %rdx
	movzwl	(%rax,%rdx,2), %eax
.L834:
	leaq	2(%rdx,%rdx), %r14
	movq	%rbx, -280(%rbp)
	movq	%r12, %rbx
	movq	%r14, %r12
	movl	%r10d, %r14d
	jmp	.L837
	.p2align 4,,10
	.p2align 3
.L995:
	movswl	%dx, %ecx
	sarl	$5, %ecx
.L843:
	movl	$-1, %eax
	cmpl	%r14d, %ecx
	jbe	.L844
	andl	$2, %edx
	leaq	10(%rbx), %rax
	jne	.L846
	movq	24(%rbx), %rax
.L846:
	movzwl	(%rax,%r12), %eax
.L844:
	addq	$2, %r12
.L837:
	movzwl	%ax, %edi
	call	_ZN6icu_6712PatternProps12isWhiteSpaceEi@PLT
	testb	%al, %al
	je	.L915
	cmpl	%r15d, %r14d
	jge	.L915
	movzwl	8(%rbx), %edx
	addl	$1, %r14d
	testw	%dx, %dx
	jns	.L995
	movl	12(%rbx), %ecx
	jmp	.L843
	.p2align 4,,10
	.p2align 3
.L872:
	call	_ZN6icu_6720TransliteratorParser9parseRuleERKNS_13UnicodeStringEiiR10UErrorCode
	movb	$0, -296(%rbp)
	movl	%eax, %edx
	jmp	.L816
	.p2align 4,,10
	.p2align 3
.L990:
	movzwl	8(%r12), %eax
	xorl	%r8d, %r8d
	testl	%edx, %edx
	js	.L822
	testw	%ax, %ax
	js	.L823
	movswl	%ax, %r8d
	sarl	$5, %r8d
.L824:
	cmpl	%r8d, %edx
	cmovle	%edx, %r8d
.L822:
	testw	%ax, %ax
	js	.L825
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L826:
	subl	%r8d, %ecx
	movl	%r8d, %edx
	movl	$10, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_6713UnicodeString9doIndexOfEDsii@PLT
	addl	$1, %eax
	movl	%eax, %edx
	jne	.L816
.L812:
	cmpb	$0, -296(%rbp)
	je	.L875
	movswl	-184(%rbp), %eax
	testw	%ax, %ax
	js	.L996
.L810:
	sarl	$5, %eax
.L877:
	testl	%eax, %eax
	jg	.L997
.L878:
	movl	0(%r13), %esi
	testl	%esi, %esi
	jg	.L887
	movl	16(%rbx), %eax
	movl	%eax, -304(%rbp)
	testl	%eax, %eax
	jle	.L883
	leaq	8(%rbx), %rax
	movq	%r13, -288(%rbp)
	leaq	-264(%rbp), %r14
	movq	%rax, -320(%rbp)
	movl	$0, -296(%rbp)
.L892:
	movl	-296(%rbp), %esi
	movq	-320(%rbp), %rdi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%rax, %r12
	movslq	200(%rbx), %rax
	movl	%eax, 1164(%r12)
	testl	%eax, %eax
	jne	.L884
	movq	$0, 1152(%r12)
.L885:
	movq	1064(%r12), %rdi
	call	uhash_removeAll_67@PLT
	movl	$-1, -264(%rbp)
	jmp	.L980
	.p2align 4,,10
	.p2align 3
.L890:
	movq	8(%rax), %rdi
	movq	(%rdi), %rax
	call	*56(%rax)
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L976
	movq	16(%r15), %rsi
	movl	$64, %edi
	movq	%rsi, -280(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L894
	movq	-280(%rbp), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	1064(%r12), %rdi
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	-288(%rbp), %rcx
	call	uhash_put_67@PLT
.L980:
	movq	232(%rbx), %rdi
	movq	%r14, %rsi
	call	uhash_nextElement_67@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	jne	.L890
	addl	$1, -296(%rbp)
	movl	-296(%rbp), %eax
	cmpl	-304(%rbp), %eax
	jne	.L892
	movq	-336(%rbp), %rdi
	movq	-288(%rbp), %r13
	call	_ZN6icu_677UVector17removeAllElementsEv@PLT
	cmpq	$0, 88(%rbx)
	movq	-320(%rbp), %r15
	je	.L907
.L906:
	movl	104(%rbx), %eax
	testl	%eax, %eax
	jne	.L897
	cmpl	$1, -340(%rbp)
	je	.L899
.L898:
	movl	$65558, 0(%r13)
.L899:
	movl	-304(%rbp), %eax
	testl	%eax, %eax
	jle	.L903
	leaq	8(%rbx), %rax
	movq	%rax, -320(%rbp)
	movq	%rax, %r15
.L907:
	movq	%rbx, -280(%rbp)
	leaq	108(%rbx), %r12
	movl	-304(%rbp), %r14d
	movl	-344(%rbp), %ebx
.L904:
	movl	%ebx, %esi
	movq	%r15, %rdi
	addl	$1, %ebx
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%r13, %rdx
	movq	%r12, %rsi
	leaq	8(%rax), %rdi
	call	_ZN6icu_6722TransliterationRuleSet6freezeER11UParseErrorR10UErrorCode@PLT
	cmpl	%r14d, %ebx
	jl	.L904
	movq	-280(%rbp), %rbx
.L903:
	cmpl	$1, 56(%rbx)
	jne	.L887
	movq	-328(%rbp), %rdi
	xorl	%esi, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movswl	8(%rax), %eax
	shrl	$5, %eax
	jne	.L887
	movq	-328(%rbp), %rdi
	xorl	%esi, %esi
	call	_ZN6icu_677UVector15removeElementAtEi@PLT
	jmp	.L887
	.p2align 4,,10
	.p2align 3
.L987:
	movq	(%rax), %rdx
	movq	%rax, %rdi
	call	*8(%rdx)
	jmp	.L805
	.p2align 4,,10
	.p2align 3
.L868:
	movq	-328(%rbp), %rdi
	movq	%r13, %rcx
	xorl	%edx, %edx
	movq	%r8, %rsi
	call	_ZN6icu_677UVector15insertElementAtEPviR10UErrorCode@PLT
	jmp	.L869
.L871:
	movq	$0, 96(%rbx)
	.p2align 4,,10
	.p2align 3
.L982:
	movl	$7, 0(%r13)
.L887:
	movq	-312(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	-256(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L798
.L992:
	movl	%r14d, 112(%rbx)
	leal	-15(%r14), %edi
	xorl	%esi, %esi
	movl	%r14d, %r15d
	movl	$0, 108(%rbx)
	call	uprv_max_67@PLT
	movq	-320(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	subl	%eax, %r15d
	movl	%eax, %esi
	movl	%r15d, %edx
	call	_ZNK6icu_6713UnicodeString9doExtractEiiPDsi@PLT
	xorl	%r11d, %r11d
	movslq	%r15d, %r15
	movw	%r11w, 116(%rbx,%r15,2)
	movzwl	8(%r12), %eax
	testw	%ax, %ax
	js	.L873
	movswl	%ax, %esi
	sarl	$5, %esi
.L874:
	leal	15(%r14), %edi
	call	uprv_min_67@PLT
	xorl	%r8d, %r8d
	movl	%r14d, %esi
	movq	%r12, %rdi
	subl	%r14d, %eax
	leaq	148(%rbx), %rcx
	movslq	%eax, %r15
	movq	%rcx, -280(%rbp)
	movl	%r15d, %edx
	call	_ZNK6icu_6713UnicodeString9doExtractEiiPDsi@PLT
	movq	-280(%rbp), %rcx
	xorl	%r10d, %r10d
	movw	%r10w, 148(%rbx,%r15,2)
	movl	$65562, 0(%r13)
.L875:
	movq	96(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L878
	movl	104(%rbx), %r8d
	leaq	8(%rbx), %rdi
	testl	%r8d, %r8d
	jne	.L880
	movq	%r13, %rdx
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	jmp	.L878
.L823:
	movl	12(%r12), %r8d
	jmp	.L824
	.p2align 4,,10
	.p2align 3
.L976:
	movq	-288(%rbp), %r13
	jmp	.L982
.L808:
	movl	12(%r12), %eax
	jmp	.L809
.L825:
	movl	12(%r12), %ecx
	jmp	.L826
.L884:
	leaq	0(,%rax,8), %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, 1152(%r12)
	testq	%rax, %rax
	je	.L976
	movl	-296(%rbp), %edx
	movl	1164(%r12), %ecx
	testl	%edx, %edx
	sete	1160(%r12)
	testl	%ecx, %ecx
	jle	.L885
	movq	%rbx, -280(%rbp)
	xorl	%r15d, %r15d
	movq	-336(%rbp), %rbx
	jmp	.L889
	.p2align 4,,10
	.p2align 3
.L998:
	movq	1152(%r12), %rax
.L889:
	movl	%r15d, %esi
	movq	%rbx, %rdi
	leaq	(%rax,%r15,8), %r13
	addq	$1, %r15
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%rax, 0(%r13)
	cmpl	%r15d, 1164(%r12)
	jg	.L998
	movq	-280(%rbp), %rbx
	jmp	.L885
.L997:
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L982
	movq	-312(%rbp), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movl	104(%rbx), %r9d
	testl	%r9d, %r9d
	jne	.L879
	movq	-328(%rbp), %rdi
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	jmp	.L878
.L996:
	movl	-180(%rbp), %eax
	jmp	.L877
.L873:
	movl	12(%r12), %esi
	jmp	.L874
.L880:
	movq	%r13, %rcx
	xorl	%edx, %edx
	call	_ZN6icu_677UVector15insertElementAtEPviR10UErrorCode@PLT
	jmp	.L878
.L994:
	leaq	10(%r12), %rax
	jmp	.L836
.L879:
	movq	-328(%rbp), %rdi
	movq	%r13, %rcx
	xorl	%edx, %edx
	movq	%r12, %rsi
	call	_ZN6icu_677UVector15insertElementAtEPviR10UErrorCode@PLT
	jmp	.L878
.L915:
	movq	%rbx, %r12
	movq	-280(%rbp), %rbx
	cmpb	$0, -296(%rbp)
	movl	%r14d, %r10d
	movl	%r14d, -268(%rbp)
	movl	104(%rbx), %edx
	jne	.L841
	movq	96(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L841
	movl	%r14d, -280(%rbp)
	leaq	8(%rbx), %rdi
	testl	%edx, %edx
	jne	.L847
	movq	%r13, %rdx
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	movl	-280(%rbp), %r10d
.L848:
	movq	$0, 96(%rbx)
	movl	104(%rbx), %edx
.L841:
	leaq	-268(%rbp), %rsi
	movq	%r13, %rcx
	movq	%r12, %rdi
	movl	%r10d, -288(%rbp)
	movq	%rsi, -280(%rbp)
	call	_ZN6icu_6722TransliteratorIDParser13parseSingleIDERKNS_13UnicodeStringERiiR10UErrorCode@PLT
	movl	-288(%rbp), %r10d
	cmpl	%r10d, -268(%rbp)
	movq	-280(%rbp), %rsi
	movq	%rax, %r14
	je	.L852
	movl	$59, %edx
	movq	%r12, %rdi
	movl	%r10d, -288(%rbp)
	movq	%rsi, -280(%rbp)
	call	_ZN6icu_6711ICU_Utility9parseCharERKNS_13UnicodeStringERiDs@PLT
	movq	-280(%rbp), %rsi
	movl	-288(%rbp), %r10d
	testb	%al, %al
	je	.L852
	leaq	8(%r14), %rax
	movq	%rax, -280(%rbp)
	movl	104(%rbx), %eax
	testl	%eax, %eax
	jne	.L853
	movswl	16(%r14), %eax
	testw	%ax, %ax
	js	.L854
	sarl	$5, %eax
	movl	%eax, %ecx
.L855:
	movq	-312(%rbp), %rdi
	xorl	%edx, %edx
	leaq	8(%r14), %rsi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	leaq	-264(%rbp), %rsi
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%rax, %rdi
	movl	$59, %eax
	movw	%ax, -264(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
.L856:
	leaq	136(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	72(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-280(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L866:
	movb	$1, -296(%rbp)
	movl	-268(%rbp), %edx
	jmp	.L816
.L852:
	movl	104(%rbx), %edx
	xorl	%r8d, %r8d
	leaq	-264(%rbp), %rcx
	movq	%r12, %rdi
	movl	%r10d, -288(%rbp)
	movq	%rsi, -280(%rbp)
	movl	$-1, -264(%rbp)
	call	_ZN6icu_6722TransliteratorIDParser17parseGlobalFilterERKNS_13UnicodeStringERiiS4_PS1_@PLT
	movq	-280(%rbp), %rsi
	movl	-288(%rbp), %r10d
	testq	%rax, %rax
	je	.L999
	movl	$59, %edx
	movq	%r12, %rdi
	movq	%rax, -280(%rbp)
	movl	%r10d, -288(%rbp)
	call	_ZN6icu_6711ICU_Utility9parseCharERKNS_13UnicodeStringERiDs@PLT
	movq	-280(%rbp), %r9
	testb	%al, %al
	je	.L859
	movl	104(%rbx), %r11d
	movl	-264(%rbp), %eax
	movl	-288(%rbp), %r10d
	testl	%r11d, %r11d
	sete	%dl
	testl	%eax, %eax
	sete	%al
	cmpb	%al, %dl
	je	.L1000
.L859:
	movq	%r9, %rdi
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
.L863:
	testq	%r14, %r14
	je	.L866
	leaq	8(%r14), %rax
	movq	%rax, -280(%rbp)
	jmp	.L856
.L832:
	movl	12(%r12), %edx
	jmp	.L833
.L911:
	movl	$-1, %eax
	movslq	%r10d, %rdx
	jmp	.L834
.L999:
	movl	%r10d, 112(%rbx)
	leal	-15(%r10), %edi
	xorl	%esi, %esi
	movl	$0, 108(%rbx)
	movl	%r10d, -280(%rbp)
	call	uprv_max_67@PLT
	movl	-280(%rbp), %r10d
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movl	%eax, %esi
	movq	-320(%rbp), %rcx
	movl	%r10d, %eax
	movl	%r10d, -288(%rbp)
	subl	%esi, %eax
	movl	%eax, %edx
	movl	%eax, -280(%rbp)
	call	_ZNK6icu_6713UnicodeString9doExtractEiiPDsi@PLT
	movslq	-280(%rbp), %rax
	xorl	%r8d, %r8d
	movl	-288(%rbp), %r10d
	movw	%r8w, 116(%rbx,%rax,2)
	movswl	8(%r12), %eax
	testw	%ax, %ax
	js	.L864
	sarl	$5, %eax
	movl	%eax, %esi
.L865:
	leal	15(%r10), %edi
	movl	%r10d, -280(%rbp)
	call	uprv_min_67@PLT
	movl	-280(%rbp), %r10d
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	leaq	148(%rbx), %rcx
	subl	%r10d, %eax
	movl	%r10d, %esi
	movq	%rcx, -288(%rbp)
	movl	%eax, %edx
	movl	%eax, -280(%rbp)
	call	_ZNK6icu_6713UnicodeString9doExtractEiiPDsi@PLT
	movq	-288(%rbp), %rcx
	movslq	-280(%rbp), %rax
	xorl	%edi, %edi
	movw	%di, 148(%rbx,%rax,2)
	movl	$65569, 0(%r13)
	jmp	.L863
.L847:
	movq	%r13, %rcx
	xorl	%edx, %edx
	call	_ZN6icu_677UVector15insertElementAtEPviR10UErrorCode@PLT
	movl	-280(%rbp), %r10d
	jmp	.L848
.L1000:
	cmpq	$0, 88(%rbx)
	je	.L860
	movl	%r10d, 112(%rbx)
	leal	-15(%r10), %edi
	xorl	%esi, %esi
	movl	$0, 108(%rbx)
	movq	%r9, -296(%rbp)
	movl	%r10d, -280(%rbp)
	call	uprv_max_67@PLT
	movl	-280(%rbp), %r10d
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movl	%eax, %esi
	movq	-320(%rbp), %rcx
	movl	%r10d, %eax
	movl	%r10d, -288(%rbp)
	subl	%esi, %eax
	movl	%eax, %edx
	movl	%eax, -280(%rbp)
	call	_ZNK6icu_6713UnicodeString9doExtractEiiPDsi@PLT
	movslq	-280(%rbp), %rax
	xorl	%r10d, %r10d
	movq	-296(%rbp), %r9
	movw	%r10w, 116(%rbx,%rax,2)
	movswl	8(%r12), %eax
	movl	-288(%rbp), %r10d
	testw	%ax, %ax
	js	.L861
	sarl	$5, %eax
	movl	%eax, %esi
.L862:
	leal	15(%r10), %edi
	movq	%r9, -296(%rbp)
	movl	%r10d, -280(%rbp)
	call	uprv_min_67@PLT
	movl	-280(%rbp), %r10d
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	leaq	148(%rbx), %rcx
	subl	%r10d, %eax
	movl	%r10d, %esi
	movq	%rcx, -288(%rbp)
	movl	%eax, %edx
	movl	%eax, -280(%rbp)
	call	_ZNK6icu_6713UnicodeString9doExtractEiiPDsi@PLT
	movq	-288(%rbp), %rcx
	movslq	-280(%rbp), %rax
	xorl	%r9d, %r9d
	movw	%r9w, 148(%rbx,%rax,2)
	movq	-296(%rbp), %r9
	movl	$65559, 0(%r13)
	movq	%r9, %rdi
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
	jmp	.L863
.L853:
	movq	-312(%rbp), %rdi
	movl	$59, %eax
	xorl	%edx, %edx
	xorl	%esi, %esi
	leaq	-264(%rbp), %rcx
	movl	$1, %r9d
	xorl	%r8d, %r8d
	movw	%ax, -264(%rbp)
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	movswl	16(%r14), %eax
	testw	%ax, %ax
	js	.L857
	sarl	$5, %eax
	movl	%eax, %r9d
.L858:
	movq	-312(%rbp), %rdi
	xorl	%r8d, %r8d
	leaq	8(%r14), %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZN6icu_6713UnicodeString9doReplaceEiiRKS0_ii@PLT
	jmp	.L856
.L854:
	movl	20(%r14), %ecx
	jmp	.L855
.L909:
	leaq	-192(%rbp), %rdx
	movl	$2, %eax
	movl	$0, -300(%rbp)
	movl	$-1, -340(%rbp)
	movq	%rdx, -312(%rbp)
	jmp	.L810
.L883:
	movq	-336(%rbp), %rdi
	call	_ZN6icu_677UVector17removeAllElementsEv@PLT
	cmpq	$0, 88(%rbx)
	jne	.L906
	jmp	.L903
.L864:
	movl	12(%r12), %esi
	jmp	.L865
.L897:
	movl	-340(%rbp), %ecx
	cmpl	%ecx, -300(%rbp)
	je	.L899
	cmpl	$1, %eax
	jne	.L899
	jmp	.L898
.L860:
	movl	-300(%rbp), %eax
	movq	%r9, 88(%rbx)
	movl	%eax, -340(%rbp)
	jmp	.L863
.L857:
	movl	20(%r14), %r9d
	jmp	.L858
.L894:
	movq	1064(%r12), %rdi
	movq	-288(%rbp), %rcx
	movq	%r13, %rdx
	xorl	%esi, %esi
	call	uhash_put_67@PLT
	jmp	.L980
.L861:
	movl	12(%r12), %esi
	jmp	.L862
.L986:
	call	__stack_chk_fail@PLT
.L807:
	movq	$0, 184(%rbx)
	movl	$7, 0(%r13)
	jmp	.L798
	.cfi_endproc
.LFE2525:
	.size	_ZN6icu_6720TransliteratorParser10parseRulesERKNS_13UnicodeStringE15UTransDirectionR10UErrorCode, .-_ZN6icu_6720TransliteratorParser10parseRulesERKNS_13UnicodeStringE15UTransDirectionR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720TransliteratorParser5parseERKNS_13UnicodeStringE15UTransDirectionR11UParseErrorR10UErrorCode
	.type	_ZN6icu_6720TransliteratorParser5parseERKNS_13UnicodeStringE15UTransDirectionR11UParseErrorR10UErrorCode, @function
_ZN6icu_6720TransliteratorParser5parseERKNS_13UnicodeStringE15UTransDirectionR11UParseErrorR10UErrorCode:
.LFB2523:
	.cfi_startproc
	endbr64
	movl	(%r8), %eax
	testl	%eax, %eax
	jle	.L1007
	ret
	.p2align 4,,10
	.p2align 3
.L1007:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rcx, %r12
	movq	%r8, %rcx
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZN6icu_6720TransliteratorParser10parseRulesERKNS_13UnicodeStringE15UTransDirectionR10UErrorCode
	movdqu	108(%rbx), %xmm0
	movups	%xmm0, (%r12)
	movdqu	124(%rbx), %xmm1
	movups	%xmm1, 16(%r12)
	movdqu	140(%rbx), %xmm2
	movups	%xmm2, 32(%r12)
	movdqu	156(%rbx), %xmm3
	movups	%xmm3, 48(%r12)
	movq	172(%rbx), %rax
	movq	%rax, 64(%r12)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2523:
	.size	_ZN6icu_6720TransliteratorParser5parseERKNS_13UnicodeStringE15UTransDirectionR11UParseErrorR10UErrorCode, .-_ZN6icu_6720TransliteratorParser5parseERKNS_13UnicodeStringE15UTransDirectionR11UParseErrorR10UErrorCode
	.weak	_ZTSN6icu_677UMemoryE
	.section	.rodata._ZTSN6icu_677UMemoryE,"aG",@progbits,_ZTSN6icu_677UMemoryE,comdat
	.align 16
	.type	_ZTSN6icu_677UMemoryE, @object
	.size	_ZTSN6icu_677UMemoryE, 18
_ZTSN6icu_677UMemoryE:
	.string	"N6icu_677UMemoryE"
	.weak	_ZTIN6icu_677UMemoryE
	.section	.data.rel.ro._ZTIN6icu_677UMemoryE,"awG",@progbits,_ZTIN6icu_677UMemoryE,comdat
	.align 8
	.type	_ZTIN6icu_677UMemoryE, @object
	.size	_ZTIN6icu_677UMemoryE, 16
_ZTIN6icu_677UMemoryE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN6icu_677UMemoryE
	.weak	_ZTSN6icu_679ParseDataE
	.section	.rodata._ZTSN6icu_679ParseDataE,"aG",@progbits,_ZTSN6icu_679ParseDataE,comdat
	.align 16
	.type	_ZTSN6icu_679ParseDataE, @object
	.size	_ZTSN6icu_679ParseDataE, 20
_ZTSN6icu_679ParseDataE:
	.string	"N6icu_679ParseDataE"
	.weak	_ZTIN6icu_679ParseDataE
	.section	.data.rel.ro._ZTIN6icu_679ParseDataE,"awG",@progbits,_ZTIN6icu_679ParseDataE,comdat
	.align 8
	.type	_ZTIN6icu_679ParseDataE, @object
	.size	_ZTIN6icu_679ParseDataE, 56
_ZTIN6icu_679ParseDataE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN6icu_679ParseDataE
	.long	0
	.long	2
	.quad	_ZTIN6icu_677UMemoryE
	.quad	2
	.quad	_ZTIN6icu_6711SymbolTableE
	.quad	2
	.weak	_ZTVN6icu_679ParseDataE
	.section	.data.rel.ro.local._ZTVN6icu_679ParseDataE,"awG",@progbits,_ZTVN6icu_679ParseDataE,comdat
	.align 8
	.type	_ZTVN6icu_679ParseDataE, @object
	.size	_ZTVN6icu_679ParseDataE, 56
_ZTVN6icu_679ParseDataE:
	.quad	0
	.quad	_ZTIN6icu_679ParseDataE
	.quad	_ZN6icu_679ParseDataD1Ev
	.quad	_ZN6icu_679ParseDataD0Ev
	.quad	_ZNK6icu_679ParseData6lookupERKNS_13UnicodeStringE
	.quad	_ZNK6icu_679ParseData13lookupMatcherEi
	.quad	_ZNK6icu_679ParseData14parseReferenceERKNS_13UnicodeStringERNS_13ParsePositionEi
	.section	.rodata
	.align 16
	.type	_ZN6icu_67L16PRAGMA_NFC_RULESE, @object
	.size	_ZN6icu_67L16PRAGMA_NFC_RULESE, 26
_ZN6icu_67L16PRAGMA_NFC_RULESE:
	.value	126
	.value	110
	.value	102
	.value	99
	.value	32
	.value	114
	.value	117
	.value	108
	.value	101
	.value	115
	.value	126
	.value	59
	.value	0
	.align 16
	.type	_ZN6icu_67L16PRAGMA_NFD_RULESE, @object
	.size	_ZN6icu_67L16PRAGMA_NFD_RULESE, 26
_ZN6icu_67L16PRAGMA_NFD_RULESE:
	.value	126
	.value	110
	.value	102
	.value	100
	.value	32
	.value	114
	.value	117
	.value	108
	.value	101
	.value	115
	.value	126
	.value	59
	.value	0
	.align 32
	.type	_ZN6icu_67L21PRAGMA_MAXIMUM_BACKUPE, @object
	.size	_ZN6icu_67L21PRAGMA_MAXIMUM_BACKUPE, 40
_ZN6icu_67L21PRAGMA_MAXIMUM_BACKUPE:
	.value	126
	.value	109
	.value	97
	.value	120
	.value	105
	.value	109
	.value	117
	.value	109
	.value	32
	.value	98
	.value	97
	.value	99
	.value	107
	.value	117
	.value	112
	.value	32
	.value	35
	.value	126
	.value	59
	.value	0
	.align 32
	.type	_ZN6icu_67L21PRAGMA_VARIABLE_RANGEE, @object
	.size	_ZN6icu_67L21PRAGMA_VARIABLE_RANGEE, 44
_ZN6icu_67L21PRAGMA_VARIABLE_RANGEE:
	.value	126
	.value	118
	.value	97
	.value	114
	.value	105
	.value	97
	.value	98
	.value	108
	.value	101
	.value	32
	.value	114
	.value	97
	.value	110
	.value	103
	.value	101
	.value	32
	.value	35
	.value	32
	.value	35
	.value	126
	.value	59
	.value	0
	.align 8
	.type	_ZN6icu_67L10PRAGMA_USEE, @object
	.size	_ZN6icu_67L10PRAGMA_USEE, 10
_ZN6icu_67L10PRAGMA_USEE:
	.value	117
	.value	115
	.value	101
	.value	32
	.value	0
	.align 2
	.type	_ZL8ID_TOKEN, @object
	.size	_ZL8ID_TOKEN, 4
_ZL8ID_TOKEN:
	.value	58
	.value	58
	.align 16
	.type	_ZL11HALF_ENDERS, @object
	.size	_ZL11HALF_ENDERS, 16
_ZL11HALF_ENDERS:
	.value	61
	.value	62
	.value	60
	.value	8594
	.value	8592
	.value	8596
	.value	59
	.value	0
	.align 8
	.type	_ZL10gOPERATORS, @object
	.size	_ZL10gOPERATORS, 14
_ZL10gOPERATORS:
	.value	61
	.value	62
	.value	60
	.value	8594
	.value	8592
	.value	8596
	.value	0
	.align 16
	.type	_ZL12ILLEGAL_FUNC, @object
	.size	_ZL12ILLEGAL_FUNC, 22
_ZL12ILLEGAL_FUNC:
	.value	94
	.value	40
	.value	46
	.value	42
	.value	43
	.value	63
	.value	123
	.value	125
	.value	124
	.value	64
	.value	0
	.align 8
	.type	_ZL11ILLEGAL_SEG, @object
	.size	_ZL11ILLEGAL_SEG, 10
_ZL11ILLEGAL_SEG:
	.value	123
	.value	125
	.value	124
	.value	64
	.value	0
	.align 2
	.type	_ZL11ILLEGAL_TOP, @object
	.size	_ZL11ILLEGAL_TOP, 4
_ZL11ILLEGAL_TOP:
	.value	41
	.value	0
	.align 32
	.type	_ZL7DOT_SET, @object
	.size	_ZL7DOT_SET, 42
_ZL7DOT_SET:
	.value	91
	.value	94
	.value	91
	.value	58
	.value	90
	.value	112
	.value	58
	.value	93
	.value	91
	.value	58
	.value	90
	.value	108
	.value	58
	.value	93
	.value	92
	.value	114
	.value	92
	.value	110
	.value	36
	.value	93
	.value	0
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	-1
	.long	-1
	.long	-1
	.long	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
