	.file	"collationweights.cpp"
	.text
	.p2align 4
	.type	_ZN6icu_67L13compareRangesEPKvS1_S1_, @function
_ZN6icu_67L13compareRangesEPKvS1_S1_:
.LFB2098:
	.cfi_startproc
	endbr64
	movl	(%rdx), %ecx
	movl	$-1, %eax
	cmpl	%ecx, (%rsi)
	jb	.L1
	seta	%al
	movzbl	%al, %eax
.L1:
	ret
	.cfi_endproc
.LFE2098:
	.size	_ZN6icu_67L13compareRangesEPKvS1_S1_, .-_ZN6icu_67L13compareRangesEPKvS1_S1_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716CollationWeightsC2Ev
	.type	_ZN6icu_6716CollationWeightsC2Ev, @function
_ZN6icu_6716CollationWeightsC2Ev:
.LFB2090:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movl	$0, (%rdi)
	movq	$0, 156(%rdi)
	movq	$0, 36(%rdi)
	movups	%xmm0, 4(%rdi)
	movups	%xmm0, 20(%rdi)
	ret
	.cfi_endproc
.LFE2090:
	.size	_ZN6icu_6716CollationWeightsC2Ev, .-_ZN6icu_6716CollationWeightsC2Ev
	.globl	_ZN6icu_6716CollationWeightsC1Ev
	.set	_ZN6icu_6716CollationWeightsC1Ev,_ZN6icu_6716CollationWeightsC2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716CollationWeights14initForPrimaryEa
	.type	_ZN6icu_6716CollationWeights14initForPrimaryEa, @function
_ZN6icu_6716CollationWeights14initForPrimaryEa:
.LFB2092:
	.cfi_startproc
	endbr64
	cmpb	$1, %sil
	movl	$1, (%rdi)
	sbbl	%edx, %edx
	movl	$3, 8(%rdi)
	notl	%edx
	movl	$255, 28(%rdi)
	addl	$255, %edx
	cmpb	$1, %sil
	sbbl	%eax, %eax
	movl	%edx, 32(%rdi)
	andl	$-2, %eax
	addl	$4, %eax
	movl	%eax, 12(%rdi)
	movabsq	$8589934594, %rax
	movq	%rax, 16(%rdi)
	movabsq	$1095216660735, %rax
	movq	%rax, 36(%rdi)
	ret
	.cfi_endproc
.LFE2092:
	.size	_ZN6icu_6716CollationWeights14initForPrimaryEa, .-_ZN6icu_6716CollationWeights14initForPrimaryEa
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716CollationWeights16initForSecondaryEv
	.type	_ZN6icu_6716CollationWeights16initForSecondaryEv, @function
_ZN6icu_6716CollationWeights16initForSecondaryEv:
.LFB2093:
	.cfi_startproc
	endbr64
	movdqa	.LC0(%rip), %xmm0
	movl	$3, (%rdi)
	movups	%xmm0, 8(%rdi)
	movdqa	.LC1(%rip), %xmm0
	movups	%xmm0, 28(%rdi)
	ret
	.cfi_endproc
.LFE2093:
	.size	_ZN6icu_6716CollationWeights16initForSecondaryEv, .-_ZN6icu_6716CollationWeights16initForSecondaryEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716CollationWeights15initForTertiaryEv
	.type	_ZN6icu_6716CollationWeights15initForTertiaryEv, @function
_ZN6icu_6716CollationWeights15initForTertiaryEv:
.LFB2094:
	.cfi_startproc
	endbr64
	movdqa	.LC0(%rip), %xmm0
	movl	$3, (%rdi)
	movups	%xmm0, 8(%rdi)
	movdqa	.LC2(%rip), %xmm0
	movups	%xmm0, 28(%rdi)
	ret
	.cfi_endproc
.LFE2094:
	.size	_ZN6icu_6716CollationWeights15initForTertiaryEv, .-_ZN6icu_6716CollationWeights15initForTertiaryEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6716CollationWeights9incWeightEji
	.type	_ZNK6icu_6716CollationWeights9incWeightEji, @function
_ZNK6icu_6716CollationWeights9incWeightEji:
.LFB2095:
	.cfi_startproc
	endbr64
	movl	$4, %r9d
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %r8d
	movslq	%edx, %rax
	movl	%r9d, %ecx
	subl	%edx, %ecx
	sall	$3, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	shrl	%cl, %r8d
	pushq	%r13
	movzbl	%r8b, %r8d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	cmpl	%r8d, 24(%rdi,%rax,4)
	ja	.L20
	movl	$-1, %r11d
	leaq	(%rdi,%rax,4), %r13
	xorl	%r12d, %r12d
	movl	$32, %ebx
	leal	0(,%rdx,8), %ecx
	movl	%r11d, %eax
	movl	$-256, %r10d
	shrl	%cl, %eax
	cmpl	$32, %ecx
	cmovge	%r12d, %eax
	movl	%eax, %r8d
	movl	%ebx, %eax
	subl	%ecx, %eax
	movl	%eax, %ecx
	movl	%r10d, %eax
	sall	%cl, %eax
	orl	%r8d, %eax
	andl	%eax, %esi
	movl	4(%r13), %eax
	sall	%cl, %eax
	movl	%r9d, %ecx
	orl	%eax, %esi
	leal	-1(%rdx), %eax
	subl	%eax, %ecx
	movl	%esi, %r8d
	sall	$3, %ecx
	shrl	%cl, %r8d
	movzbl	%r8b, %r8d
	cmpl	20(%r13), %r8d
	jb	.L28
	movslq	%eax, %rcx
	movl	%r11d, %r8d
	leaq	(%rdi,%rcx,4), %r13
	leal	0(,%rax,8), %ecx
	movl	%ebx, %eax
	shrl	%cl, %r8d
	cmpl	$32, %ecx
	cmovge	%r12d, %r8d
	subl	%ecx, %eax
	movl	%eax, %ecx
	movl	%r10d, %eax
	sall	%cl, %eax
	orl	%r8d, %eax
	andl	%eax, %esi
	movl	4(%r13), %eax
	sall	%cl, %eax
	movl	%r9d, %ecx
	orl	%eax, %esi
	leal	-2(%rdx), %eax
	subl	%eax, %ecx
	movl	%esi, %r8d
	sall	$3, %ecx
	shrl	%cl, %r8d
	movzbl	%r8b, %r8d
	cmpl	%r8d, 20(%r13)
	ja	.L28
	movslq	%eax, %rcx
	movl	%r11d, %r8d
	leaq	(%rdi,%rcx,4), %r13
	leal	0(,%rax,8), %ecx
	movl	%ebx, %eax
	shrl	%cl, %r8d
	cmpl	$32, %ecx
	cmovge	%r12d, %r8d
	subl	%ecx, %eax
	movl	%eax, %ecx
	movl	%r10d, %eax
	sall	%cl, %eax
	orl	%r8d, %eax
	andl	%eax, %esi
	movl	4(%r13), %eax
	sall	%cl, %eax
	movl	%r9d, %ecx
	orl	%eax, %esi
	leal	-3(%rdx), %eax
	subl	%eax, %ecx
	movl	%esi, %r8d
	sall	$3, %ecx
	shrl	%cl, %r8d
	movzbl	%r8b, %r8d
	cmpl	20(%r13), %r8d
	jb	.L28
	movslq	%eax, %rcx
	movl	%r11d, %r8d
	leal	-4(%rdx), %r13d
	leaq	(%rdi,%rcx,4), %r14
	leal	0(,%rax,8), %ecx
	movl	%ebx, %eax
	shrl	%cl, %r8d
	cmpl	$32, %ecx
	cmovge	%r12d, %r8d
	subl	%ecx, %eax
	movl	%eax, %ecx
	movl	%r10d, %eax
	sall	%cl, %eax
	orl	%r8d, %eax
	andl	%eax, %esi
	movl	4(%r14), %eax
	sall	%cl, %eax
	movl	%r9d, %ecx
	orl	%eax, %esi
	subl	%r13d, %ecx
	sall	$3, %ecx
	movl	%esi, %r8d
	shrl	%cl, %r8d
	movzbl	%r8b, %r8d
	cmpl	20(%r14), %r8d
	jb	.L33
	leal	0(,%r13,8), %ecx
	movslq	%r13d, %rax
	shrl	%cl, %r11d
	cmpl	$32, %ecx
	movl	4(%rdi,%rax,4), %eax
	cmovge	%r12d, %r11d
	subl	%ecx, %ebx
	subl	$5, %edx
	movl	%ebx, %ecx
	subl	%edx, %r9d
	sall	%cl, %r10d
	sall	%cl, %eax
	leal	0(,%r9,8), %ecx
	orl	%r11d, %r10d
	andl	%r10d, %esi
	orl	%eax, %esi
	movl	%esi, %r8d
	shrl	%cl, %r8d
	movzbl	%r8b, %r8d
.L20:
	movl	$-1, %edi
	movl	$32, %r9d
	leal	1(%r8), %eax
	popq	%rbx
	leal	0(,%rdx,8), %ecx
	movl	$0, %edx
	popq	%r12
	popq	%r13
	shrl	%cl, %edi
	cmpl	$32, %ecx
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	cmovge	%edx, %edi
	subl	%ecx, %r9d
	movl	$-256, %edx
	movl	%r9d, %ecx
	sall	%cl, %edx
	sall	%cl, %eax
	orl	%edi, %edx
	andl	%edx, %esi
	orl	%esi, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	.cfi_restore_state
	movl	%eax, %edx
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L33:
	movl	%r13d, %edx
	jmp	.L20
	.cfi_endproc
.LFE2095:
	.size	_ZNK6icu_6716CollationWeights9incWeightEji, .-_ZNK6icu_6716CollationWeights9incWeightEji
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6716CollationWeights17incWeightByOffsetEjii
	.type	_ZNK6icu_6716CollationWeights17incWeightByOffsetEjii, @function
_ZNK6icu_6716CollationWeights17incWeightByOffsetEjii:
.LFB2096:
	.cfi_startproc
	endbr64
	movl	$4, %r10d
	movslq	%edx, %rdx
	movl	%ecx, %r11d
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%r10d, %ecx
	movl	%esi, %eax
	movl	%esi, %r9d
	movq	%rdx, %r8
	subl	%edx, %ecx
	sall	$3, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	shrl	%cl, %eax
	pushq	%r14
	movzbl	%al, %eax
	pushq	%r13
	pushq	%r12
	addl	%r11d, %eax
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	24(%rdi,%rdx,4), %ecx
	cmpl	%ecx, %eax
	jbe	.L43
	leaq	(%rdi,%rdx,4), %rbx
	addl	$1, %ecx
	movl	$-1, %r12d
	xorl	%r14d, %r14d
	movl	4(%rbx), %esi
	movl	$32, %r13d
	movl	%r13d, %r11d
	subl	%esi, %eax
	subl	%esi, %ecx
	cltd
	idivl	%ecx
	leal	0(,%r8,8), %ecx
	addl	%esi, %edx
	movl	%r12d, %esi
	shrl	%cl, %esi
	cmpl	$32, %ecx
	cmovge	%r14d, %esi
	subl	%ecx, %r11d
	movl	%r11d, %ecx
	movl	$-256, %r11d
	movl	%r11d, %r15d
	sall	%cl, %edx
	sall	%cl, %r15d
	movl	%r10d, %ecx
	orl	%r15d, %esi
	leal	-1(%r8), %r15d
	andl	%r9d, %esi
	subl	%r15d, %ecx
	orl	%edx, %esi
	sall	$3, %ecx
	movl	%esi, %edx
	movl	%esi, %r9d
	movl	20(%rbx), %esi
	shrl	%cl, %edx
	movzbl	%dl, %edx
	addl	%edx, %eax
	cmpl	%esi, %eax
	jbe	.L51
	movslq	%r15d, %rdx
	addl	$1, %esi
	leaq	(%rdi,%rdx,4), %rbx
	movl	4(%rbx), %ecx
	subl	%ecx, %eax
	subl	%ecx, %esi
	cltd
	idivl	%esi
	movl	%r12d, %esi
	addl	%ecx, %edx
	leal	0(,%r15,8), %ecx
	movl	%r13d, %r15d
	shrl	%cl, %esi
	cmpl	$32, %ecx
	cmovge	%r14d, %esi
	subl	%ecx, %r15d
	movl	%r15d, %ecx
	movl	%r11d, %r15d
	sall	%cl, %r15d
	sall	%cl, %edx
	movl	%r10d, %ecx
	orl	%r15d, %esi
	leal	-2(%r8), %r15d
	andl	%r9d, %esi
	subl	%r15d, %ecx
	orl	%edx, %esi
	sall	$3, %ecx
	movl	%esi, %edx
	movl	%esi, %r9d
	movl	20(%rbx), %esi
	shrl	%cl, %edx
	movzbl	%dl, %edx
	addl	%edx, %eax
	cmpl	%eax, %esi
	jnb	.L51
	movslq	%r15d, %rdx
	addl	$1, %esi
	leaq	(%rdi,%rdx,4), %rbx
	movl	4(%rbx), %ecx
	subl	%ecx, %eax
	subl	%ecx, %esi
	cltd
	idivl	%esi
	movl	%r12d, %esi
	addl	%ecx, %edx
	leal	0(,%r15,8), %ecx
	movl	%r13d, %r15d
	shrl	%cl, %esi
	cmpl	$32, %ecx
	cmovge	%r14d, %esi
	subl	%ecx, %r15d
	movl	%r15d, %ecx
	movl	%r11d, %r15d
	sall	%cl, %r15d
	sall	%cl, %edx
	movl	%r10d, %ecx
	orl	%r15d, %esi
	leal	-3(%r8), %r15d
	andl	%r9d, %esi
	subl	%r15d, %ecx
	orl	%edx, %esi
	sall	$3, %ecx
	movl	%esi, %edx
	movl	%esi, %r9d
	movl	20(%rbx), %esi
	shrl	%cl, %edx
	movzbl	%dl, %edx
	addl	%edx, %eax
	cmpl	%esi, %eax
	jbe	.L51
	movslq	%r15d, %rdx
	addl	$1, %esi
	leaq	(%rdi,%rdx,4), %rbx
	movl	4(%rbx), %ecx
	subl	%ecx, %eax
	subl	%ecx, %esi
	cltd
	idivl	%esi
	movl	%r12d, %esi
	addl	%ecx, %edx
	leal	0(,%r15,8), %ecx
	movl	%r13d, %r15d
	shrl	%cl, %esi
	cmpl	$32, %ecx
	cmovge	%r14d, %esi
	subl	%ecx, %r15d
	movl	%r15d, %ecx
	movl	%r11d, %r15d
	sall	%cl, %r15d
	sall	%cl, %edx
	movl	%r10d, %ecx
	orl	%r15d, %esi
	leal	-4(%r8), %r15d
	andl	%r9d, %esi
	subl	%r15d, %ecx
	orl	%edx, %esi
	sall	$3, %ecx
	movl	%esi, %edx
	movl	%esi, %r9d
	shrl	%cl, %edx
	movl	20(%rbx), %ecx
	movzbl	%dl, %edx
	addl	%edx, %eax
	cmpl	%ecx, %eax
	jbe	.L51
	movslq	%r15d, %rdx
	addl	$1, %ecx
	movl	4(%rdi,%rdx,4), %esi
	subl	%esi, %eax
	subl	%esi, %ecx
	cltd
	idivl	%ecx
	leal	0(,%r15,8), %ecx
	shrl	%cl, %r12d
	addl	%esi, %edx
	cmpl	$32, %ecx
	movl	%r11d, %esi
	cmovl	%r12d, %r14d
	subl	%ecx, %r13d
	subl	$5, %r8d
	movl	%r13d, %ecx
	subl	%r8d, %r10d
	sall	%cl, %esi
	sall	%cl, %edx
	leal	0(,%r10,8), %ecx
	orl	%r14d, %esi
	andl	%r9d, %esi
	orl	%edx, %esi
	movl	%esi, %edx
	movl	%esi, %r9d
	shrl	%cl, %edx
	movzbl	%dl, %edx
	addl	%edx, %eax
.L43:
	movl	$-1, %esi
	movl	$0, %edi
	movl	$-256, %edx
	popq	%rbx
	leal	0(,%r8,8), %ecx
	popq	%r12
	popq	%r13
	shrl	%cl, %esi
	cmpl	$32, %ecx
	popq	%r14
	popq	%r15
	cmovl	%esi, %edi
	movl	$32, %esi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	subl	%ecx, %esi
	movl	%esi, %ecx
	sall	%cl, %edx
	sall	%cl, %eax
	movl	%edx, %esi
	orl	%edi, %esi
	andl	%r9d, %esi
	orl	%esi, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L51:
	.cfi_restore_state
	movl	%r15d, %r8d
	jmp	.L43
	.cfi_endproc
.LFE2096:
	.size	_ZNK6icu_6716CollationWeights17incWeightByOffsetEjii, .-_ZNK6icu_6716CollationWeights17incWeightByOffsetEjii
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6716CollationWeights13lengthenRangeERNS0_11WeightRangeE
	.type	_ZNK6icu_6716CollationWeights13lengthenRangeERNS0_11WeightRangeE, @function
_ZNK6icu_6716CollationWeights13lengthenRangeERNS0_11WeightRangeE:
.LFB2097:
	.cfi_startproc
	endbr64
	movslq	8(%rsi), %rax
	movq	%rdi, %r8
	movl	$4, %ecx
	movl	$-256, %edx
	leal	1(%rax), %edi
	leaq	(%r8,%rax,4), %r9
	movl	(%rsi), %eax
	movl	8(%r9), %r8d
	subl	%edi, %ecx
	sall	$3, %ecx
	sall	%cl, %edx
	movl	%r8d, %r10d
	andl	%edx, %eax
	sall	%cl, %r10d
	andl	4(%rsi), %edx
	orl	%r10d, %eax
	movl	%eax, (%rsi)
	movl	28(%r9), %eax
	movl	%edi, 8(%rsi)
	movl	%eax, %r11d
	addl	$1, %eax
	subl	%r8d, %eax
	imull	12(%rsi), %eax
	sall	%cl, %r11d
	orl	%r11d, %edx
	movl	%edx, 4(%rsi)
	movl	%eax, 12(%rsi)
	ret
	.cfi_endproc
.LFE2097:
	.size	_ZNK6icu_6716CollationWeights13lengthenRangeERNS0_11WeightRangeE, .-_ZNK6icu_6716CollationWeights13lengthenRangeERNS0_11WeightRangeE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716CollationWeights15getWeightRangesEjj
	.type	_ZN6icu_6716CollationWeights15getWeightRangesEjj, @function
_ZN6icu_6716CollationWeights15getWeightRangesEjj:
.LFB2099:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	$16777215, %esi
	je	.L110
	testw	%si, %si
	je	.L111
	movzbl	%sil, %eax
	cmpl	$1, %eax
	sbbl	%ecx, %ecx
	andb	$1, %cl
	subl	$1, %ecx
	cmpl	$1, %eax
	sbbl	%r10d, %r10d
	addl	$4, %r10d
.L58:
	testl	$16777215, %edx
	je	.L59
	movl	$2, %eax
	testw	%dx, %dx
	je	.L60
	xorl	%eax, %eax
	testb	%dl, %dl
	setne	%al
	addl	$3, %eax
.L60:
	xorl	%r9d, %r9d
	cmpl	%edx, %esi
	jnb	.L57
	cmpl	%r10d, %eax
	jle	.L62
	andl	%edx, %ecx
	cmpl	%ecx, %esi
	je	.L57
.L62:
	movl	(%rdi), %r8d
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -224(%rbp)
	movaps	%xmm0, -208(%rbp)
	movaps	%xmm0, -192(%rbp)
	movaps	%xmm0, -176(%rbp)
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm0, -240(%rbp)
	movaps	%xmm0, -144(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	cmpl	%r8d, %r10d
	jle	.L63
	movl	$4, %ecx
	movl	%esi, %r11d
	movslq	%r10d, %r9
	subl	%r10d, %ecx
	movl	24(%rdi,%r9,4), %ebx
	sall	$3, %ecx
	shrl	%cl, %r11d
	movzbl	%r11b, %r11d
	cmpl	%ebx, %r11d
	jnb	.L64
	movl	$1, %r12d
	salq	$4, %r9
	movl	%ebx, %r14d
	subl	%r11d, %ebx
	salq	%cl, %r12
	sall	%cl, %r14d
	movl	%r10d, -216(%rbp,%r9)
	addl	%esi, %r12d
	movl	%ebx, -212(%rbp,%r9)
	movl	%r12d, -224(%rbp,%r9)
	movl	$-256, %r12d
	sall	%cl, %r12d
	andl	%esi, %r12d
	orl	%r14d, %r12d
	movl	%r12d, -220(%rbp,%r9)
.L64:
	leal	-1(%r10), %r11d
	movl	$4, %ecx
	movl	$-1, %r9d
	subl	%r11d, %ecx
	sall	$3, %ecx
	sall	%cl, %r9d
	andl	%r9d, %esi
	cmpl	%r11d, %r8d
	jge	.L63
	movl	%esi, %ebx
	movslq	%r11d, %r9
	shrl	%cl, %ebx
	movl	24(%rdi,%r9,4), %r12d
	movzbl	%bl, %ebx
	cmpl	%r12d, %ebx
	jb	.L144
.L66:
	leal	-2(%r10), %r11d
	movl	$4, %ecx
	movl	$-1, %r9d
	subl	%r11d, %ecx
	sall	$3, %ecx
	sall	%cl, %r9d
	andl	%r9d, %esi
	cmpl	%r11d, %r8d
	jge	.L63
	movl	%esi, %ebx
	movslq	%r11d, %r9
	shrl	%cl, %ebx
	movl	24(%rdi,%r9,4), %r12d
	movzbl	%bl, %ebx
	cmpl	%r12d, %ebx
	jb	.L145
.L67:
	leal	-3(%r10), %r11d
	movl	$4, %ecx
	movl	$-1, %r9d
	subl	%r11d, %ecx
	sall	$3, %ecx
	sall	%cl, %r9d
	andl	%r9d, %esi
	cmpl	%r11d, %r8d
	jge	.L63
	movl	%esi, %ebx
	movslq	%r11d, %r9
	shrl	%cl, %ebx
	movl	24(%rdi,%r9,4), %r12d
	movzbl	%bl, %ebx
	cmpl	%r12d, %ebx
	jb	.L146
.L68:
	leal	-4(%r10), %r11d
	movl	$4, %ecx
	movl	$-1, %r9d
	subl	%r11d, %ecx
	sall	$3, %ecx
	sall	%cl, %r9d
	andl	%r9d, %esi
	cmpl	%r11d, %r8d
	jge	.L63
	movl	%esi, %ebx
	movslq	%r11d, %r9
	shrl	%cl, %ebx
	movl	24(%rdi,%r9,4), %r12d
	movzbl	%bl, %ebx
	cmpl	%ebx, %r12d
	ja	.L147
.L69:
	movl	$9, %ecx
	movl	$-1, %r9d
	subl	%r10d, %ecx
	sall	$3, %ecx
	sall	%cl, %r9d
	andl	%r9d, %esi
.L63:
	movl	$4, %r9d
	movl	$1, %ebx
	movl	%r9d, %r10d
	movq	%rbx, %r11
	subl	%r8d, %r10d
	sall	$3, %r10d
	movl	%r10d, %ecx
	salq	%cl, %r11
	movl	$-1, %ecx
	cmpl	$-16777217, %esi
	leal	(%rsi,%r11), %r12d
	movl	%ecx, %esi
	cmovbe	%r12d, %esi
	movl	%esi, -240(%rbp)
	cmpl	%eax, %r8d
	jge	.L71
	subl	%eax, %r9d
	movl	%edx, %r12d
	movslq	%eax, %r13
	leal	0(,%r9,8), %ecx
	movl	4(%rdi,%r13,4), %r14d
	shrl	%cl, %r12d
	movzbl	%r12b, %r12d
	cmpl	%r14d, %r12d
	jbe	.L72
	movl	$-256, %r9d
	movl	%r14d, %r15d
	salq	%cl, %rbx
	subl	%r14d, %r12d
	sall	%cl, %r9d
	sall	%cl, %r15d
	movl	%edx, %ecx
	andl	%edx, %r9d
	salq	$4, %r13
	subl	%ebx, %ecx
	orl	%r15d, %r9d
	movl	%ecx, -140(%rbp,%r13)
	movl	%r9d, -144(%rbp,%r13)
	movl	%eax, -136(%rbp,%r13)
	movl	%r12d, -132(%rbp,%r13)
.L72:
	leal	-1(%rax), %ebx
	movl	$4, %ecx
	movl	$-1, %r9d
	subl	%ebx, %ecx
	sall	$3, %ecx
	sall	%cl, %r9d
	andl	%r9d, %edx
	cmpl	%ebx, %r8d
	jge	.L73
	movl	%edx, %r12d
	movslq	%ebx, %r13
	shrl	%cl, %r12d
	movl	4(%rdi,%r13,4), %r14d
	movzbl	%r12b, %r12d
	cmpl	%r14d, %r12d
	ja	.L148
.L74:
	leal	-2(%rax), %ebx
	movl	$4, %ecx
	movl	$-1, %r9d
	subl	%ebx, %ecx
	sall	$3, %ecx
	sall	%cl, %r9d
	andl	%r9d, %edx
	cmpl	%ebx, %r8d
	jge	.L73
	movl	%edx, %r12d
	movslq	%ebx, %r13
	shrl	%cl, %r12d
	movl	4(%rdi,%r13,4), %r14d
	movzbl	%r12b, %r12d
	cmpl	%r14d, %r12d
	ja	.L149
.L75:
	leal	-3(%rax), %ebx
	movl	$4, %ecx
	movl	$-1, %r9d
	subl	%ebx, %ecx
	sall	$3, %ecx
	sall	%cl, %r9d
	andl	%r9d, %edx
	cmpl	%ebx, %r8d
	jge	.L73
	movl	%edx, %r12d
	movslq	%ebx, %r13
	shrl	%cl, %r12d
	movl	4(%rdi,%r13,4), %r14d
	movzbl	%r12b, %r12d
	cmpl	%r14d, %r12d
	ja	.L150
.L76:
	leal	-4(%rax), %ebx
	movl	$4, %ecx
	movl	$-1, %r9d
	subl	%ebx, %ecx
	sall	$3, %ecx
	sall	%cl, %r9d
	andl	%r9d, %edx
	cmpl	%ebx, %r8d
	jge	.L73
	movl	%edx, %r12d
	movslq	%ebx, %r13
	shrl	%cl, %r12d
	movl	4(%rdi,%r13,4), %r14d
	movzbl	%r12b, %r12d
	cmpl	%r12d, %r14d
	jb	.L151
.L77:
	movl	$9, %ecx
	subl	%eax, %ecx
	movl	$-1, %eax
	sall	$3, %ecx
	sall	%cl, %eax
	andl	%eax, %edx
.L73:
	subl	%r11d, %edx
	movl	%r8d, -232(%rbp)
	movl	%edx, -236(%rbp)
	cmpl	%esi, %edx
	jb	.L81
	movl	$0, 160(%rdi)
	subl	%esi, %edx
	movl	%r10d, %ecx
	shrl	%cl, %edx
	leal	1(%rdx), %eax
	movl	%eax, -228(%rbp)
	testl	%eax, %eax
	jle	.L152
.L79:
	movdqa	-240(%rbp), %xmm1
	movl	$1, %r9d
	movl	$1, %eax
	movl	$1, 160(%rdi)
	movups	%xmm1, 44(%rdi)
.L80:
	addl	$1, %r8d
	cmpl	$4, %r8d
	jg	.L57
.L107:
	movslq	%r8d, %rdx
	movq	%rdx, %rcx
	salq	$4, %rcx
	movl	-132(%rbp,%rcx), %r15d
	testl	%r15d, %r15d
	jle	.L95
	movdqa	-144(%rbp,%rcx), %xmm2
	salq	$4, %rax
	movups	%xmm2, 44(%rdi,%rax)
	movl	160(%rdi), %eax
	addl	$1, %eax
	movl	%eax, 160(%rdi)
.L95:
	salq	$4, %rdx
	movl	-212(%rbp,%rdx), %r14d
	testl	%r14d, %r14d
	jle	.L96
	movdqa	-224(%rbp,%rdx), %xmm3
	cltq
	salq	$4, %rax
	movups	%xmm3, 44(%rdi,%rax)
	movl	160(%rdi), %eax
	addl	$1, %eax
	movl	%eax, 160(%rdi)
.L96:
	leal	1(%r8), %edx
	cmpl	$4, %r8d
	je	.L97
	movslq	%edx, %rdx
	movq	%rdx, %rcx
	salq	$4, %rcx
	movl	-132(%rbp,%rcx), %r13d
	testl	%r13d, %r13d
	jle	.L98
	movdqa	-144(%rbp,%rcx), %xmm4
	cltq
	salq	$4, %rax
	movups	%xmm4, 44(%rdi,%rax)
	movl	160(%rdi), %eax
	addl	$1, %eax
	movl	%eax, 160(%rdi)
.L98:
	salq	$4, %rdx
	movl	-212(%rbp,%rdx), %r12d
	testl	%r12d, %r12d
	jle	.L99
	movdqa	-224(%rbp,%rdx), %xmm5
	cltq
	salq	$4, %rax
	movups	%xmm5, 44(%rdi,%rax)
	movl	160(%rdi), %eax
	addl	$1, %eax
	movl	%eax, 160(%rdi)
.L99:
	leal	2(%r8), %edx
	cmpl	$3, %r8d
	je	.L97
	movslq	%edx, %rdx
	movq	%rdx, %rcx
	salq	$4, %rcx
	movl	-132(%rbp,%rcx), %ebx
	testl	%ebx, %ebx
	jg	.L153
.L100:
	salq	$4, %rdx
	movl	-212(%rbp,%rdx), %r11d
	testl	%r11d, %r11d
	jg	.L154
.L101:
	leal	3(%r8), %edx
	cmpl	$2, %r8d
	je	.L97
	movslq	%edx, %rdx
	movq	%rdx, %rcx
	salq	$4, %rcx
	movl	-132(%rbp,%rcx), %r10d
	testl	%r10d, %r10d
	jg	.L155
.L102:
	salq	$4, %rdx
	movl	-212(%rbp,%rdx), %r9d
	testl	%r9d, %r9d
	jg	.L156
.L103:
	leal	4(%r8), %edx
	cmpl	$1, %r8d
	je	.L97
	movslq	%edx, %rdx
	movq	%rdx, %rcx
	salq	$4, %rcx
	movl	-132(%rbp,%rcx), %esi
	testl	%esi, %esi
	jg	.L157
.L104:
	salq	$4, %rdx
	movl	-212(%rbp,%rdx), %ecx
	testl	%ecx, %ecx
	jg	.L158
.L97:
	testl	%eax, %eax
	setg	%r9b
.L57:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L159
	addq	$216, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore_state
	xorl	%r9d, %r9d
	movl	$1, %eax
	cmpl	%edx, %esi
	jb	.L62
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L110:
	movl	$-16777216, %ecx
	movl	$1, %r10d
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L71:
	subl	%r11d, %edx
	movl	%r8d, -232(%rbp)
	movl	%edx, -236(%rbp)
	cmpl	%edx, %esi
	jbe	.L160
	cmpl	$3, %r8d
	jg	.L92
	.p2align 4,,10
	.p2align 3
.L81:
	xorl	%eax, %eax
	movl	$4, %esi
	leaq	-224(%rbp), %r12
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L137:
	movl	-68(%rbp,%rax,4), %r14d
	testl	%r14d, %r14d
	jle	.L142
	movl	-80(%rbp,%rax,4), %ebx
	movl	68(%r12,%rax,4), %edx
	movl	%ebx, -248(%rbp)
	cmpl	%ebx, %edx
	ja	.L161
	je	.L142
	movl	%esi, %r11d
	movl	%edx, %ebx
	negl	%r11d
	sall	$3, %r11d
	leal	32(%r11), %r9d
	movl	%r9d, %ecx
	shrl	%cl, %ebx
	movzbl	%bl, %ecx
	cmpl	40(%rdi,%rax), %ecx
	jb	.L162
	leal	0(,%rsi,8), %r15d
	movl	$-1, %ebx
	movl	%r15d, %ecx
	movl	%r15d, -244(%rbp)
	shrl	%cl, %ebx
	cmpl	$32, %r15d
	movl	$-256, %r15d
	movl	%ebx, %ecx
	movl	$0, %ebx
	cmovne	%ecx, %ebx
	movl	%r9d, %ecx
	sall	%cl, %r15d
	movl	%r15d, %ecx
	leal	40(%r11), %r15d
	orl	%ebx, %ecx
	movl	20(%rdi,%rax), %ebx
	andl	%ecx, %edx
	movl	%r9d, %ecx
	leal	-1(%rsi), %r9d
	sall	%cl, %ebx
	movl	%r15d, %ecx
	orl	%ebx, %edx
	movl	%r9d, %ebx
	movl	%edx, %r13d
	shrl	%cl, %r13d
	movzbl	%r13b, %ecx
	cmpl	36(%rdi,%rax), %ecx
	jb	.L89
	movl	-244(%rbp), %ebx
	movl	$-256, %r13d
	leal	-8(%rbx), %ecx
	movl	$-1, %ebx
	shrl	%cl, %ebx
	movl	%r15d, %ecx
	leal	48(%r11), %r15d
	sall	%cl, %r13d
	orl	%r13d, %ebx
	andl	%ebx, %edx
	movl	16(%rdi,%rax), %ebx
	sall	%cl, %ebx
	movl	%r15d, %ecx
	orl	%ebx, %edx
	leal	-2(%rsi), %ebx
	movl	%edx, %r13d
	shrl	%cl, %r13d
	movzbl	%r13b, %ecx
	cmpl	32(%rdi,%rax), %ecx
	jb	.L89
	movl	-244(%rbp), %ebx
	movl	$-256, %r13d
	leal	-16(%rbx), %ecx
	movl	$-1, %ebx
	shrl	%cl, %ebx
	movl	%r15d, %ecx
	leal	56(%r11), %r15d
	sall	%cl, %r13d
	orl	%r13d, %ebx
	andl	%ebx, %edx
	movl	12(%rdi,%rax), %ebx
	sall	%cl, %ebx
	movl	%r15d, %ecx
	orl	%ebx, %edx
	leal	-3(%rsi), %ebx
	movl	%edx, %r13d
	shrl	%cl, %r13d
	movzbl	%r13b, %ecx
	cmpl	28(%rdi,%rax), %ecx
	jb	.L89
	movl	-244(%rbp), %ebx
	movl	$-256, %r13d
	leal	-24(%rbx), %ecx
	movl	$-1, %ebx
	shrl	%cl, %ebx
	movl	%r15d, %ecx
	leal	64(%r11), %r15d
	sall	%cl, %r13d
	orl	%r13d, %ebx
	andl	%ebx, %edx
	movl	8(%rdi,%rax), %ebx
	sall	%cl, %ebx
	movl	%r15d, %ecx
	orl	%ebx, %edx
	leal	-4(%rsi), %ebx
	movl	%edx, %r13d
	shrl	%cl, %r13d
	movzbl	%r13b, %ecx
	cmpl	24(%rdi,%rax), %ecx
	jb	.L89
	movl	-244(%rbp), %ecx
	movl	$-1, %ebx
	movl	$-256, %r13d
	subl	$32, %ecx
	shrl	%cl, %ebx
	movl	%r15d, %ecx
	sall	%cl, %r13d
	movl	%r13d, %ecx
	orl	%ebx, %ecx
	movl	4(%rdi,%rax), %ebx
	andl	%ecx, %edx
	movl	%r15d, %ecx
	sall	%cl, %ebx
	leal	72(%r11), %ecx
	orl	%ebx, %edx
	leal	-5(%rsi), %ebx
	movl	%edx, %r15d
	shrl	%cl, %r15d
	movzbl	%r15b, %ecx
.L89:
	leal	1(%rcx), %r11d
	leal	0(,%rbx,8), %ecx
.L91:
	movl	$-1, %r13d
	shrl	%cl, %r13d
.L86:
	movl	$32, %ebx
	subl	%ecx, %ebx
	movl	%ebx, %ecx
	movl	$-256, %ebx
	sall	%cl, %ebx
	sall	%cl, %r11d
	orl	%r13d, %ebx
	andl	%ebx, %edx
	orl	%r11d, %edx
	cmpl	%edx, -248(%rbp)
	je	.L163
	movl	%r9d, %esi
	subq	$4, %rax
	cmpl	%r8d, %r9d
	jle	.L92
.L93:
	movl	76(%r12,%rax,4), %r10d
	testl	%r10d, %r10d
	jg	.L137
.L142:
	leal	-1(%rsi), %r9d
	subq	$4, %rax
	movl	%r9d, %esi
	cmpl	%r8d, %r9d
	jg	.L93
.L92:
	movl	$0, 160(%rdi)
.L143:
	xorl	%r9d, %r9d
	xorl	%eax, %eax
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L162:
	leal	1(%rcx), %r11d
	leal	0(,%rsi,8), %ecx
	cmpl	$32, %ecx
	jne	.L164
	xorl	%r13d, %r13d
	leal	-1(%rsi), %r9d
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L148:
	movl	$-256, %r9d
	movl	%r14d, %r15d
	salq	$4, %r13
	sall	%cl, %r9d
	sall	%cl, %r15d
	movl	%ebx, -136(%rbp,%r13)
	andl	%edx, %r9d
	orl	%r15d, %r9d
	movl	%r9d, -144(%rbp,%r13)
	movl	$1, %r9d
	salq	%cl, %r9
	movl	%edx, %ecx
	subl	%r9d, %ecx
	movl	%ecx, -140(%rbp,%r13)
	movl	%r12d, %ecx
	subl	%r14d, %ecx
	movl	%ecx, -132(%rbp,%r13)
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L144:
	movl	$1, %r13d
	salq	$4, %r9
	movl	%r12d, %r14d
	subl	%ebx, %r12d
	salq	%cl, %r13
	sall	%cl, %r14d
	movl	%r11d, -216(%rbp,%r9)
	addl	%esi, %r13d
	movl	%r12d, -212(%rbp,%r9)
	movl	%r13d, -224(%rbp,%r9)
	movl	$-256, %r13d
	sall	%cl, %r13d
	andl	%esi, %r13d
	orl	%r14d, %r13d
	movl	%r13d, -220(%rbp,%r9)
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L145:
	movl	$1, %r13d
	salq	$4, %r9
	movl	%r12d, %r14d
	subl	%ebx, %r12d
	salq	%cl, %r13
	sall	%cl, %r14d
	movl	%r11d, -216(%rbp,%r9)
	addl	%esi, %r13d
	movl	%r12d, -212(%rbp,%r9)
	movl	%r13d, -224(%rbp,%r9)
	movl	$-256, %r13d
	sall	%cl, %r13d
	andl	%esi, %r13d
	orl	%r14d, %r13d
	movl	%r13d, -220(%rbp,%r9)
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L149:
	movl	$-256, %r9d
	movl	%r14d, %r15d
	salq	$4, %r13
	sall	%cl, %r9d
	sall	%cl, %r15d
	movl	%ebx, -136(%rbp,%r13)
	andl	%edx, %r9d
	orl	%r15d, %r9d
	movl	%r9d, -144(%rbp,%r13)
	movl	$1, %r9d
	salq	%cl, %r9
	movl	%edx, %ecx
	subl	%r9d, %ecx
	movl	%ecx, -140(%rbp,%r13)
	movl	%r12d, %ecx
	subl	%r14d, %ecx
	movl	%ecx, -132(%rbp,%r13)
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L150:
	movl	$-256, %r9d
	movl	%r14d, %r15d
	salq	$4, %r13
	sall	%cl, %r9d
	sall	%cl, %r15d
	movl	%ebx, -136(%rbp,%r13)
	andl	%edx, %r9d
	orl	%r15d, %r9d
	movl	%r9d, -144(%rbp,%r13)
	movl	$1, %r9d
	salq	%cl, %r9
	movl	%edx, %ecx
	subl	%r9d, %ecx
	movl	%ecx, -140(%rbp,%r13)
	movl	%r12d, %ecx
	subl	%r14d, %ecx
	movl	%ecx, -132(%rbp,%r13)
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L146:
	movl	$1, %r13d
	salq	$4, %r9
	movl	%r12d, %r14d
	subl	%ebx, %r12d
	salq	%cl, %r13
	sall	%cl, %r14d
	movl	%r11d, -216(%rbp,%r9)
	addl	%esi, %r13d
	movl	%r12d, -212(%rbp,%r9)
	movl	%r13d, -224(%rbp,%r9)
	movl	$-256, %r13d
	sall	%cl, %r13d
	andl	%esi, %r13d
	orl	%r14d, %r13d
	movl	%r13d, -220(%rbp,%r9)
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L154:
	movdqa	-224(%rbp,%rdx), %xmm7
	cltq
	salq	$4, %rax
	movups	%xmm7, 44(%rdi,%rax)
	movl	160(%rdi), %eax
	addl	$1, %eax
	movl	%eax, 160(%rdi)
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L153:
	movdqa	-144(%rbp,%rcx), %xmm6
	cltq
	salq	$4, %rax
	movups	%xmm6, 44(%rdi,%rax)
	movl	160(%rdi), %eax
	addl	$1, %eax
	movl	%eax, 160(%rdi)
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L147:
	movl	$1, %r13d
	salq	$4, %r9
	movl	%r12d, %r14d
	subl	%ebx, %r12d
	salq	%cl, %r13
	sall	%cl, %r14d
	movl	%r11d, -216(%rbp,%r9)
	addl	%esi, %r13d
	movl	%r12d, -212(%rbp,%r9)
	movl	%r13d, -224(%rbp,%r9)
	movl	$-256, %r13d
	sall	%cl, %r13d
	andl	%esi, %r13d
	orl	%r14d, %r13d
	movl	%r13d, -220(%rbp,%r9)
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L151:
	movl	$-256, %r9d
	movl	%r14d, %r15d
	salq	$4, %r13
	sall	%cl, %r9d
	sall	%cl, %r15d
	movl	%ebx, -136(%rbp,%r13)
	andl	%edx, %r9d
	orl	%r15d, %r9d
	movl	%r9d, -144(%rbp,%r13)
	movl	$1, %r9d
	salq	%cl, %r9
	movl	%edx, %ecx
	subl	%r9d, %ecx
	movl	%ecx, -140(%rbp,%r13)
	movl	%r12d, %ecx
	subl	%r14d, %ecx
	movl	%ecx, -132(%rbp,%r13)
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L156:
	movdqa	-224(%rbp,%rdx), %xmm7
	cltq
	salq	$4, %rax
	movups	%xmm7, 44(%rdi,%rax)
	movl	160(%rdi), %eax
	addl	$1, %eax
	movl	%eax, 160(%rdi)
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L155:
	movdqa	-144(%rbp,%rcx), %xmm6
	cltq
	salq	$4, %rax
	movups	%xmm6, 44(%rdi,%rax)
	movl	160(%rdi), %eax
	addl	$1, %eax
	movl	%eax, 160(%rdi)
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L158:
	movdqa	-224(%rbp,%rdx), %xmm5
	cltq
	salq	$4, %rax
	movups	%xmm5, 44(%rdi,%rax)
	movl	160(%rdi), %eax
	addl	$1, %eax
	movl	%eax, 160(%rdi)
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L157:
	movdqa	-144(%rbp,%rcx), %xmm4
	cltq
	salq	$4, %rax
	movups	%xmm4, 44(%rdi,%rax)
	movl	160(%rdi), %eax
	addl	$1, %eax
	movl	%eax, 160(%rdi)
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L152:
	addl	$1, %r8d
	xorl	%eax, %eax
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L111:
	movl	$-65536, %ecx
	movl	$2, %r10d
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L161:
	movslq	%esi, %rdx
	movl	$4, %ecx
	movq	%rdx, %r9
	subl	%esi, %ecx
	salq	$4, %r9
	sall	$3, %ecx
	movl	-140(%rbp,%r9), %eax
	movl	-224(%rbp,%r9), %ebx
	movl	%eax, -220(%rbp,%r9)
	shrl	%cl, %ebx
	shrl	%cl, %eax
	movzbl	%al, %eax
	movzbl	%bl, %ecx
	subl	%ecx, %eax
	addl	$1, %eax
	movl	%eax, -212(%rbp,%r9)
	leal	-1(%rsi), %r9d
.L84:
	movq	%rdx, %rax
	salq	$4, %rax
	movl	$0, -132(%rbp,%rax)
	cmpl	%r8d, %r9d
	jle	.L92
	movslq	%r9d, %r9
	leal	-2(%rsi), %eax
	salq	$4, %r9
	movl	$0, -132(%rbp,%r9)
	movl	$0, -212(%rbp,%r9)
	cmpl	%r8d, %eax
	jle	.L92
	cltq
	salq	$4, %rax
	movl	$0, -132(%rbp,%rax)
	movl	$0, -212(%rbp,%rax)
	leal	-3(%rsi), %eax
	cmpl	%r8d, %eax
	jle	.L92
	cltq
	subl	$4, %esi
	salq	$4, %rax
	movl	$0, -132(%rbp,%rax)
	movl	$0, -212(%rbp,%rax)
	cmpl	%r8d, %esi
	jle	.L92
	movslq	%esi, %rsi
	salq	$4, %rsi
	movl	$0, -132(%rbp,%rsi)
	movl	$0, -212(%rbp,%rsi)
	jmp	.L92
.L160:
	movl	$0, 160(%rdi)
	subl	%esi, %edx
	movl	%r10d, %ecx
	shrl	%cl, %edx
	leal	1(%rdx), %eax
	movl	%eax, -228(%rbp)
	testl	%eax, %eax
	jg	.L79
	jmp	.L143
.L163:
	movslq	%esi, %rdx
	addl	%r14d, %r10d
	movq	%rdx, %rax
	salq	$4, %rax
	movl	-140(%rbp,%rax), %ecx
	movl	%r10d, -212(%rbp,%rax)
	movl	%ecx, -220(%rbp,%rax)
	jmp	.L84
.L159:
	call	__stack_chk_fail@PLT
.L164:
	leal	-1(%rsi), %r9d
	jmp	.L91
	.cfi_endproc
.LFE2099:
	.size	_ZN6icu_6716CollationWeights15getWeightRangesEjj, .-_ZN6icu_6716CollationWeights15getWeightRangesEjj
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716CollationWeights25allocWeightsInShortRangesEii
	.type	_ZN6icu_6716CollationWeights25allocWeightsInShortRangesEii, @function
_ZN6icu_6716CollationWeights25allocWeightsInShortRangesEii:
.LFB2100:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movl	160(%rdi), %ecx
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jle	.L192
	movl	52(%rdi), %r9d
	leal	1(%rdx), %r8d
	cmpl	%r9d, %r8d
	jl	.L192
	movl	56(%rdi), %eax
	cmpl	%eax, %esi
	jle	.L167
	subl	%eax, %esi
	cmpl	$1, %ecx
	je	.L192
	movl	68(%rdi), %eax
	cmpl	%eax, %r8d
	jl	.L192
	movl	72(%rdi), %r9d
	cmpl	%r9d, %esi
	jle	.L178
	subl	%r9d, %esi
	cmpl	$2, %ecx
	je	.L192
	movl	84(%rdi), %eax
	cmpl	%eax, %r8d
	jl	.L192
	movl	88(%rdi), %r9d
	cmpl	%r9d, %esi
	jle	.L181
	subl	%r9d, %esi
	cmpl	$3, %ecx
	je	.L192
	movl	100(%rdi), %eax
	cmpl	%eax, %r8d
	jl	.L192
	movl	104(%rdi), %r9d
	cmpl	%r9d, %esi
	jle	.L184
	subl	%r9d, %esi
	cmpl	$4, %ecx
	je	.L192
	movl	116(%rdi), %eax
	cmpl	%eax, %r8d
	jl	.L192
	movl	120(%rdi), %r9d
	cmpl	%r9d, %esi
	jle	.L187
	subl	%r9d, %esi
	cmpl	$5, %ecx
	je	.L192
	movl	132(%rdi), %eax
	cmpl	%eax, %r8d
	jl	.L192
	movl	136(%rdi), %r9d
	cmpl	%r9d, %esi
	jle	.L190
	subl	%r9d, %esi
	cmpl	$6, %ecx
	je	.L192
	movl	148(%rdi), %eax
	cmpl	%eax, %r8d
	jl	.L192
	cmpl	%esi, 152(%rdi)
	jge	.L193
.L192:
	xorl	%eax, %eax
.L165:
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L198
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L193:
	.cfi_restore_state
	movl	$6, %ecx
	movl	$7, %r10d
	.p2align 4,,10
	.p2align 3
.L168:
	cmpl	%eax, %edx
	jge	.L196
	salq	$4, %rcx
	movl	%esi, 56(%rcx,%rdi)
.L196:
	subq	$8, %rsp
	leaq	-12(%rbp), %rax
	movl	$16, %edx
	xorl	%r9d, %r9d
	movl	%r10d, 160(%rdi)
	xorl	%r8d, %r8d
	addq	$44, %rdi
	leaq	_ZN6icu_67L13compareRangesEPKvS1_S1_(%rip), %rcx
	pushq	%rax
	movl	%r10d, %esi
	movl	$0, -12(%rbp)
	call	uprv_sortArray_67@PLT
	popq	%rax
	movl	$1, %eax
	popq	%rdx
	jmp	.L165
.L167:
	cmpl	%r9d, %edx
	jge	.L197
	movl	%esi, 56(%rdi)
.L197:
	movl	$1, 160(%rdi)
	movl	$1, %eax
	jmp	.L165
.L178:
	movl	$1, %ecx
	movl	$2, %r10d
	jmp	.L168
.L181:
	movl	$2, %ecx
	movl	$3, %r10d
	jmp	.L168
.L184:
	movl	$3, %ecx
	movl	$4, %r10d
	jmp	.L168
.L187:
	movl	$4, %ecx
	movl	$5, %r10d
	jmp	.L168
.L190:
	movl	$5, %ecx
	movl	$6, %r10d
	jmp	.L168
.L198:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2100:
	.size	_ZN6icu_6716CollationWeights25allocWeightsInShortRangesEii, .-_ZN6icu_6716CollationWeights25allocWeightsInShortRangesEii
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716CollationWeights29allocWeightsInMinLengthRangesEii
	.type	_ZN6icu_6716CollationWeights29allocWeightsInMinLengthRangesEii, @function
_ZN6icu_6716CollationWeights29allocWeightsInMinLengthRangesEii:
.LFB2101:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leal	1(%rdx), %eax
	movl	%edx, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%eax, -48(%rbp)
	movslq	%edx, %rax
	leaq	(%rdi,%rax,4), %r11
	movl	8(%r11), %eax
	movl	28(%r11), %ebx
	movl	160(%rdi), %ecx
	movl	%ebx, -56(%rbp)
	subl	%eax, %ebx
	movl	%eax, -52(%rbp)
	leal	1(%rbx), %r15d
	testl	%ecx, %ecx
	jle	.L200
	cmpl	52(%rdi), %edx
	jne	.L200
	movl	56(%rdi), %r12d
	cmpl	$1, %ecx
	je	.L205
	cmpl	68(%rdi), %edx
	jne	.L205
	addl	72(%rdi), %r12d
	cmpl	$2, %ecx
	je	.L208
	cmpl	84(%rdi), %edx
	jne	.L208
	addl	88(%rdi), %r12d
	cmpl	$3, %ecx
	je	.L298
	cmpl	100(%rdi), %edx
	jne	.L211
	movl	104(%rdi), %eax
	addl	%r12d, %eax
	movl	%eax, %r12d
	cmpl	$4, %ecx
	je	.L299
	cmpl	116(%rdi), %edx
	jne	.L213
	addl	120(%rdi), %eax
	movl	%eax, %r12d
	cmpl	$5, %ecx
	je	.L298
	cmpl	132(%rdi), %edx
	jne	.L215
	addl	136(%rdi), %r12d
	cmpl	$6, %ecx
	je	.L298
	cmpl	%edx, 148(%rdi)
	jne	.L217
	addl	152(%rdi), %r12d
	xorl	%r9d, %r9d
	movl	%r12d, %eax
	imull	%r15d, %eax
	cmpl	%eax, %esi
	jg	.L199
	movl	%esi, %eax
	movl	44(%rdi), %r9d
	movl	48(%rdi), %r10d
	movl	$7, %ecx
	subl	%r12d, %eax
.L210:
	cmpl	%r9d, 60(%rdi)
	cmovbe	60(%rdi), %r9d
	cmpl	%r10d, 64(%rdi)
	cmovnb	64(%rdi), %r10d
	cmpl	%r9d, 76(%rdi)
	cmovbe	76(%rdi), %r9d
	cmpl	%r10d, 80(%rdi)
	cmovnb	80(%rdi), %r10d
	cmpl	$3, %ecx
	je	.L204
	movl	92(%rdi), %edx
	cmpl	%edx, %r9d
	cmova	%edx, %r9d
	movl	96(%rdi), %edx
	cmpl	%edx, %r10d
	cmovb	%edx, %r10d
	cmpl	$4, %ecx
	je	.L204
	movl	108(%rdi), %edx
	cmpl	%edx, %r9d
	cmova	%edx, %r9d
	movl	112(%rdi), %edx
	cmpl	%edx, %r10d
	cmovb	%edx, %r10d
	cmpl	$5, %ecx
	je	.L204
	movl	124(%rdi), %edx
	cmpl	%edx, %r9d
	cmova	%edx, %r9d
	movl	128(%rdi), %edx
	cmpl	%edx, %r10d
	cmovb	%edx, %r10d
	cmpl	$6, %ecx
	je	.L204
	movl	140(%rdi), %edx
	cmpl	%edx, %r9d
	cmova	%edx, %r9d
	movl	144(%rdi), %edx
	cmpl	%edx, %r10d
	cmovb	%edx, %r10d
.L204:
	cltd
	idivl	%ebx
	movl	%r12d, %ebx
	movl	%eax, -44(%rbp)
	subl	%eax, %ebx
	testl	%eax, %eax
	je	.L219
	movl	%eax, %edx
	imull	%r15d, %edx
	addl	%ebx, %edx
	cmpl	%esi, %edx
	jl	.L219
	movl	%r9d, 44(%rdi)
	testl	%ebx, %ebx
	jne	.L221
.L300:
	movslq	52(%rdi), %rax
	movl	$4, %ecx
	movl	$-256, %edx
	leal	1(%rax), %esi
	leaq	(%rdi,%rax,4), %rax
	movl	8(%rax), %r8d
	subl	%esi, %ecx
	sall	$3, %ecx
	sall	%cl, %edx
	movl	%r8d, %r11d
	andl	%edx, %r9d
	sall	%cl, %r11d
	andl	%edx, %r10d
	orl	%r11d, %r9d
	movl	%r9d, 44(%rdi)
	movl	28(%rax), %eax
	movl	$1, %r9d
	movl	%esi, 52(%rdi)
	movl	$1, 160(%rdi)
	movl	%eax, %edx
	addl	$1, %eax
	subl	%r8d, %eax
	sall	%cl, %edx
	imull	%eax, %r12d
	orl	%edx, %r10d
	movl	%r10d, 48(%rdi)
	movl	%r12d, 56(%rdi)
.L199:
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L219:
	.cfi_restore_state
	addl	$1, %eax
	subl	$1, %ebx
	movl	%r9d, 44(%rdi)
	movl	%eax, -44(%rbp)
	testl	%ebx, %ebx
	je	.L300
.L221:
	movl	$4, %r12d
	movl	%r9d, %esi
	movl	%r12d, %eax
	subl	%r8d, %eax
	sall	$3, %eax
	movl	%eax, %ecx
	movl	%eax, -64(%rbp)
	shrl	%cl, %esi
	movzbl	%sil, %eax
	movl	24(%r11), %esi
	leal	-1(%rbx,%rax), %eax
	movl	%esi, -60(%rbp)
	cmpl	%eax, %esi
	jnb	.L301
	movl	4(%r11), %ecx
	addl	$1, %esi
	movl	$-1, %r13d
	movl	$32, %r14d
	subl	%ecx, %eax
	subl	%ecx, %esi
	cltd
	idivl	%esi
	movl	$0, %esi
	addl	%ecx, %edx
	leal	0(,%r8,8), %ecx
	shrl	%cl, %r13d
	cmpl	$32, %ecx
	cmovge	%esi, %r13d
	subl	%ecx, %r14d
	movl	%r14d, %ecx
	movl	$-256, %r14d
	sall	%cl, %r14d
	sall	%cl, %edx
	movl	%r12d, %ecx
	movl	%r14d, %esi
	movl	20(%r11), %r14d
	orl	%r13d, %esi
	andl	%esi, %r9d
	leal	-1(%r8), %esi
	orl	%edx, %r9d
	subl	%esi, %ecx
	sall	$3, %ecx
	movl	%r9d, %edx
	shrl	%cl, %edx
	movzbl	%dl, %edx
	addl	%edx, %eax
	cmpl	%r14d, %eax
	jbe	.L232
	movslq	%esi, %rdx
	addl	$1, %r14d
	leaq	(%rdi,%rdx,4), %r13
	movl	4(%r13), %ecx
	subl	%ecx, %eax
	subl	%ecx, %r14d
	cltd
	idivl	%r14d
	movl	$-1, %r14d
	addl	%ecx, %edx
	leal	0(,%rsi,8), %ecx
	movl	$0, %esi
	shrl	%cl, %r14d
	cmpl	$32, %ecx
	cmovge	%esi, %r14d
	movl	$32, %esi
	subl	%ecx, %esi
	movl	%esi, %ecx
	movl	$-256, %esi
	sall	%cl, %esi
	sall	%cl, %edx
	movl	%r12d, %ecx
	orl	%r14d, %esi
	movl	20(%r13), %r14d
	andl	%esi, %r9d
	leal	-2(%r8), %esi
	orl	%edx, %r9d
	subl	%esi, %ecx
	sall	$3, %ecx
	movl	%r9d, %edx
	shrl	%cl, %edx
	movzbl	%dl, %edx
	addl	%edx, %eax
	cmpl	%r14d, %eax
	jbe	.L232
	movslq	%esi, %rdx
	addl	$1, %r14d
	leaq	(%rdi,%rdx,4), %r13
	movl	4(%r13), %ecx
	subl	%ecx, %eax
	subl	%ecx, %r14d
	cltd
	idivl	%r14d
	movl	$-1, %r14d
	addl	%ecx, %edx
	leal	0(,%rsi,8), %ecx
	movl	$0, %esi
	shrl	%cl, %r14d
	cmpl	$32, %ecx
	cmovge	%esi, %r14d
	movl	$32, %esi
	subl	%ecx, %esi
	movl	%esi, %ecx
	movl	$-256, %esi
	sall	%cl, %esi
	sall	%cl, %edx
	movl	%r12d, %ecx
	orl	%r14d, %esi
	movl	20(%r13), %r14d
	andl	%esi, %r9d
	leal	-3(%r8), %esi
	orl	%edx, %r9d
	subl	%esi, %ecx
	sall	$3, %ecx
	movl	%r9d, %edx
	shrl	%cl, %edx
	movzbl	%dl, %edx
	addl	%edx, %eax
	cmpl	%r14d, %eax
	jbe	.L232
	movslq	%esi, %rdx
	addl	$1, %r14d
	leaq	(%rdi,%rdx,4), %r13
	movl	4(%r13), %ecx
	movl	20(%r13), %r13d
	subl	%ecx, %eax
	subl	%ecx, %r14d
	cltd
	idivl	%r14d
	movl	$-1, %r14d
	addl	%ecx, %edx
	leal	0(,%rsi,8), %ecx
	movl	$0, %esi
	shrl	%cl, %r14d
	cmpl	$32, %ecx
	cmovge	%esi, %r14d
	movl	$32, %esi
	subl	%ecx, %esi
	movl	%esi, %ecx
	movl	$-256, %esi
	sall	%cl, %esi
	sall	%cl, %edx
	movl	%r12d, %ecx
	orl	%r14d, %esi
	andl	%esi, %r9d
	leal	-4(%r8), %esi
	orl	%edx, %r9d
	subl	%esi, %ecx
	sall	$3, %ecx
	movl	%r9d, %edx
	shrl	%cl, %edx
	movzbl	%dl, %edx
	addl	%edx, %eax
	cmpl	%r13d, %eax
	jbe	.L232
	movslq	%esi, %rdx
	addl	$1, %r13d
	movl	$-1, %r14d
	movl	4(%rdi,%rdx,4), %ecx
	subl	%ecx, %eax
	subl	%ecx, %r13d
	cltd
	idivl	%r13d
	leal	(%rdx,%rcx), %r13d
	leal	0(,%rsi,8), %ecx
	movl	$0, %esi
	movl	$-256, %edx
	shrl	%cl, %r14d
	cmpl	$32, %ecx
	cmovge	%esi, %r14d
	movl	$32, %esi
	subl	%ecx, %esi
	movl	%esi, %ecx
	leal	-5(%r8), %esi
	sall	%cl, %edx
	sall	%cl, %r13d
	subl	%esi, %r12d
	orl	%r14d, %edx
	leal	0(,%r12,8), %ecx
	andl	%edx, %r9d
	orl	%r13d, %r9d
	movl	%r9d, %edx
	shrl	%cl, %edx
	movzbl	%dl, %edx
	addl	%edx, %eax
.L232:
	movl	$-1, %r12d
	leal	0(,%rsi,8), %ecx
	xorl	%r14d, %r14d
	movl	%ebx, 56(%rdi)
	movl	%r12d, %edx
	movl	$32, %r13d
	shrl	%cl, %edx
	movl	%r13d, %esi
	cmpl	$32, %ecx
	cmovge	%r14d, %edx
	subl	%ecx, %esi
	movl	%esi, %ecx
	movl	$-256, %esi
	movl	%edx, -68(%rbp)
	movl	%esi, %edx
	sall	%cl, %eax
	sall	%cl, %edx
	orl	-68(%rbp), %edx
	movzbl	-64(%rbp), %ecx
	andl	%edx, %r9d
	orl	%r9d, %eax
	movl	%eax, %ebx
	movl	%eax, 48(%rdi)
	shrl	%cl, %ebx
	movzbl	%bl, %ecx
	cmpl	%ecx, -60(%rbp)
	ja	.L224
	leal	0(,%r8,8), %ecx
	movl	%r12d, %r9d
	movl	%r13d, %ebx
	movl	%esi, %edx
	shrl	%cl, %r9d
	cmpl	$32, %ecx
	cmovge	%r14d, %r9d
	subl	%ecx, %ebx
	movl	%ebx, %ecx
	movl	$4, %ebx
	sall	%cl, %edx
	orl	%r9d, %edx
	movl	4(%r11), %r9d
	andl	%edx, %eax
	leal	-1(%r8), %edx
	sall	%cl, %r9d
	movl	%ebx, %ecx
	orl	%r9d, %eax
	subl	%edx, %ecx
	sall	$3, %ecx
	movl	%eax, %r9d
	shrl	%cl, %r9d
	movzbl	%r9b, %ecx
	cmpl	20(%r11), %ecx
	jb	.L270
	movslq	%edx, %rcx
	movl	%r12d, %r9d
	leaq	(%rdi,%rcx,4), %r11
	leal	0(,%rdx,8), %ecx
	movl	%r13d, %edx
	shrl	%cl, %r9d
	cmpl	$32, %ecx
	cmovge	%r14d, %r9d
	subl	%ecx, %edx
	movl	%edx, %ecx
	movl	%esi, %edx
	sall	%cl, %edx
	orl	%r9d, %edx
	movl	4(%r11), %r9d
	andl	%edx, %eax
	leal	-2(%r8), %edx
	sall	%cl, %r9d
	movl	%ebx, %ecx
	orl	%r9d, %eax
	subl	%edx, %ecx
	sall	$3, %ecx
	movl	%eax, %r9d
	shrl	%cl, %r9d
	movzbl	%r9b, %ecx
	cmpl	20(%r11), %ecx
	jb	.L270
	movslq	%edx, %rcx
	movl	%r12d, %r9d
	leaq	(%rdi,%rcx,4), %r11
	leal	0(,%rdx,8), %ecx
	movl	%r13d, %edx
	shrl	%cl, %r9d
	cmpl	$32, %ecx
	cmovge	%r14d, %r9d
	subl	%ecx, %edx
	movl	%edx, %ecx
	movl	%esi, %edx
	sall	%cl, %edx
	orl	%r9d, %edx
	movl	4(%r11), %r9d
	andl	%edx, %eax
	leal	-3(%r8), %edx
	sall	%cl, %r9d
	movl	%ebx, %ecx
	orl	%r9d, %eax
	subl	%edx, %ecx
	sall	$3, %ecx
	movl	%eax, %r9d
	shrl	%cl, %r9d
	movzbl	%r9b, %ecx
	cmpl	%ecx, 20(%r11)
	ja	.L270
	movslq	%edx, %rcx
	movl	%r12d, %r9d
	leaq	(%rdi,%rcx,4), %r11
	leal	0(,%rdx,8), %ecx
	movl	%r13d, %edx
	shrl	%cl, %r9d
	cmpl	$32, %ecx
	cmovge	%r14d, %r9d
	subl	%ecx, %edx
	movl	%edx, %ecx
	movl	%esi, %edx
	sall	%cl, %edx
	orl	%r9d, %edx
	movl	4(%r11), %r9d
	andl	%edx, %eax
	leal	-4(%r8), %edx
	sall	%cl, %r9d
	movl	%ebx, %ecx
	orl	%r9d, %eax
	subl	%edx, %ecx
	sall	$3, %ecx
	movl	%eax, %r9d
	shrl	%cl, %r9d
	movzbl	%r9b, %ecx
	cmpl	%ecx, 20(%r11)
	ja	.L270
	movslq	%edx, %rcx
	movl	4(%rdi,%rcx,4), %r11d
	leal	0(,%rdx,8), %ecx
	shrl	%cl, %r12d
	cmpl	$32, %ecx
	cmovl	%r12d, %r14d
	subl	%ecx, %r13d
	subl	$5, %r8d
	movl	%r13d, %ecx
	subl	%r8d, %ebx
	sall	%cl, %esi
	sall	%cl, %r11d
	leal	0(,%rbx,8), %ecx
	orl	%esi, %r14d
	andl	%r14d, %eax
	orl	%r11d, %eax
	movl	%eax, %ebx
	shrl	%cl, %ebx
	movzbl	%bl, %ecx
.L224:
	movl	-48(%rbp), %r14d
	leal	1(%rcx), %esi
	movl	$-1, %r9d
	leal	0(,%r8,8), %ecx
	shrl	%cl, %r9d
	movl	$0, %edx
	cmpl	$32, %ecx
	movl	$4, %r8d
	cmovge	%edx, %r9d
	subl	%r14d, %r8d
	movl	$-256, %edx
	movl	$32, %ebx
	sall	$3, %r8d
	subl	%ecx, %ebx
	movl	%edx, %r11d
	movl	%r14d, 68(%rdi)
	movl	$2, 160(%rdi)
	movl	%r8d, %ecx
	sall	%cl, %r11d
	movl	%ebx, %ecx
	sall	%cl, %edx
	sall	%cl, %esi
	movl	%r8d, %ecx
	andl	%r11d, %r10d
	orl	%edx, %r9d
	andl	%r9d, %eax
	movl	$1, %r9d
	orl	%eax, %esi
	movl	-52(%rbp), %eax
	andl	%r11d, %esi
	movl	-56(%rbp), %r11d
	sall	%cl, %eax
	orl	%eax, %esi
	movl	-44(%rbp), %eax
	sall	%cl, %r11d
	orl	%r11d, %r10d
	movl	%esi, 60(%rdi)
	imull	%r15d, %eax
	movl	%r10d, 64(%rdi)
	movl	%eax, 72(%rdi)
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L270:
	.cfi_restore_state
	movl	%edx, %r8d
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L298:
	movl	%r15d, %eax
	xorl	%r9d, %r9d
	imull	%r12d, %eax
	cmpl	%eax, %esi
	jg	.L199
	movl	%esi, %eax
	movl	44(%rdi), %r9d
	movl	48(%rdi), %r10d
	subl	%r12d, %eax
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L200:
	xorl	%r9d, %r9d
	testl	%esi, %esi
	jg	.L199
	movl	44(%rdi), %r9d
	movl	48(%rdi), %r10d
	movl	%esi, %eax
	xorl	%r12d, %r12d
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L205:
	movl	%r15d, %eax
	xorl	%r9d, %r9d
	imull	%r12d, %eax
	cmpl	%eax, %esi
	jg	.L199
	movl	%esi, %eax
	movl	44(%rdi), %r9d
	movl	48(%rdi), %r10d
	subl	%r12d, %eax
	jmp	.L204
.L208:
	movl	%r15d, %eax
	xorl	%r9d, %r9d
	imull	%r12d, %eax
	cmpl	%eax, %esi
	jg	.L199
	movl	%esi, %eax
	movl	44(%rdi), %r9d
	movl	48(%rdi), %r10d
	subl	%r12d, %eax
	cmpl	%r9d, 60(%rdi)
	cmovbe	60(%rdi), %r9d
	cmpl	%r10d, 64(%rdi)
	cmovnb	64(%rdi), %r10d
	jmp	.L204
.L301:
	movl	%r8d, %esi
	jmp	.L232
.L211:
	movl	%r15d, %eax
	xorl	%r9d, %r9d
	imull	%r12d, %eax
	cmpl	%eax, %esi
	jg	.L199
	movl	%esi, %eax
	movl	44(%rdi), %r9d
	movl	48(%rdi), %r10d
	movl	$3, %ecx
	subl	%r12d, %eax
	jmp	.L210
.L299:
	movl	%r15d, %edx
	xorl	%r9d, %r9d
	imull	%eax, %edx
	cmpl	%edx, %esi
	jg	.L199
	movl	%esi, %edx
	movl	44(%rdi), %r9d
	movl	48(%rdi), %r10d
	subl	%eax, %edx
	movl	%edx, %eax
	jmp	.L210
.L213:
	movl	%r15d, %edx
	xorl	%r9d, %r9d
	imull	%eax, %edx
	cmpl	%edx, %esi
	jg	.L199
	movl	%esi, %edx
	movl	44(%rdi), %r9d
	movl	48(%rdi), %r10d
	movl	$4, %ecx
	subl	%eax, %edx
	movl	%edx, %eax
	jmp	.L210
.L215:
	movl	%r15d, %eax
	xorl	%r9d, %r9d
	imull	%r12d, %eax
	cmpl	%eax, %esi
	jg	.L199
	movl	%esi, %eax
	movl	44(%rdi), %r9d
	movl	48(%rdi), %r10d
	movl	$5, %ecx
	subl	%r12d, %eax
	jmp	.L210
.L217:
	movl	%r15d, %eax
	xorl	%r9d, %r9d
	imull	%r12d, %eax
	cmpl	%eax, %esi
	jg	.L199
	movl	%esi, %eax
	movl	44(%rdi), %r9d
	movl	48(%rdi), %r10d
	movl	$6, %ecx
	subl	%r12d, %eax
	jmp	.L210
	.cfi_endproc
.LFE2101:
	.size	_ZN6icu_6716CollationWeights29allocWeightsInMinLengthRangesEii, .-_ZN6icu_6716CollationWeights29allocWeightsInMinLengthRangesEii
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716CollationWeights12allocWeightsEjji
	.type	_ZN6icu_6716CollationWeights12allocWeightsEjji, @function
_ZN6icu_6716CollationWeights12allocWeightsEjji:
.LFB2102:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6716CollationWeights15getWeightRangesEjj
	testb	%al, %al
	je	.L315
	movl	160(%rbx), %edx
	movl	52(%rbx), %r8d
	movl	$4, %r15d
.L305:
	movl	%r8d, %r12d
	testl	%edx, %edx
	jle	.L306
	movl	56(%rbx), %ecx
	leal	1(%r8), %eax
	cmpl	%ecx, %r13d
	jle	.L307
.L386:
	movl	%r13d, %esi
	subl	%ecx, %esi
	movl	%esi, %ecx
	cmpl	$1, %edx
	je	.L306
	movl	68(%rbx), %esi
	cmpl	%esi, %eax
	jl	.L306
	movl	72(%rbx), %edi
	cmpl	%edi, %ecx
	jle	.L321
	subl	%edi, %ecx
	cmpl	$2, %edx
	je	.L306
	movl	84(%rbx), %esi
	cmpl	%esi, %eax
	jl	.L306
	movl	88(%rbx), %edi
	cmpl	%edi, %ecx
	jle	.L322
	subl	%edi, %ecx
	cmpl	$3, %edx
	je	.L306
	movl	100(%rbx), %esi
	cmpl	%esi, %eax
	jl	.L306
	movl	104(%rbx), %edi
	cmpl	%edi, %ecx
	jle	.L323
	subl	%edi, %ecx
	cmpl	$4, %edx
	je	.L306
	movl	116(%rbx), %esi
	cmpl	%esi, %eax
	jl	.L306
	movl	120(%rbx), %edi
	cmpl	%edi, %ecx
	jle	.L324
	subl	%edi, %ecx
	cmpl	$5, %edx
	je	.L306
	movl	132(%rbx), %esi
	cmpl	%esi, %eax
	jl	.L306
	movl	136(%rbx), %edi
	cmpl	%edi, %ecx
	jle	.L325
	subl	%edi, %ecx
	cmpl	$6, %edx
	je	.L306
	movl	148(%rbx), %esi
	cmpl	%eax, %esi
	jg	.L306
	cmpl	%ecx, 152(%rbx)
	jge	.L385
.L306:
	cmpl	$4, %r8d
	je	.L315
	movl	%r8d, %edx
	movl	%r13d, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6716CollationWeights29allocWeightsInMinLengthRangesEii
	testb	%al, %al
	jne	.L309
	movl	160(%rbx), %edx
	testl	%edx, %edx
	jle	.L382
	movl	52(%rbx), %r8d
	cmpl	%r8d, %r12d
	jne	.L305
	movslq	%r12d, %r9
	leal	1(%r12), %eax
	movl	%r15d, %ecx
	movl	44(%rbx), %edi
	leaq	(%rbx,%r9,4), %r8
	subl	%eax, %ecx
	movl	$-256, %esi
	movl	8(%r8), %r10d
	sall	$3, %ecx
	sall	%cl, %esi
	movl	%r10d, %r11d
	andl	%esi, %edi
	sall	%cl, %r11d
	orl	%r11d, %edi
	movl	48(%rbx), %r11d
	movl	%edi, 44(%rbx)
	movl	28(%r8), %edi
	andl	%esi, %r11d
	movl	%eax, 52(%rbx)
	movl	%edi, %r14d
	addl	$1, %edi
	subl	%r10d, %edi
	imull	56(%rbx), %edi
	sall	%cl, %r14d
	orl	%r14d, %r11d
	movl	%r11d, 48(%rbx)
	movl	%edi, 56(%rbx)
	cmpl	$1, %edx
	je	.L383
	cmpl	68(%rbx), %r12d
	jne	.L383
	movl	8(%r8), %r10d
	movl	60(%rbx), %edi
	movl	%r10d, %r11d
	andl	%esi, %edi
	sall	%cl, %r11d
	orl	%r11d, %edi
	movl	64(%rbx), %r11d
	movl	%edi, 60(%rbx)
	movl	28(%r8), %edi
	andl	%esi, %r11d
	movl	%eax, 68(%rbx)
	movl	%edi, %r14d
	addl	$1, %edi
	subl	%r10d, %edi
	imull	72(%rbx), %edi
	sall	%cl, %r14d
	orl	%r14d, %r11d
	movl	%r11d, 64(%rbx)
	movl	%edi, 72(%rbx)
	cmpl	$2, %edx
	je	.L383
	cmpl	84(%rbx), %r12d
	jne	.L383
	movl	8(%r8), %edi
	movl	76(%rbx), %r10d
	movl	%edi, %r11d
	andl	%esi, %r10d
	andl	80(%rbx), %esi
	sall	%cl, %r11d
	orl	%r11d, %r10d
	movl	%r10d, 76(%rbx)
	movl	28(%r8), %r8d
	movl	%eax, 84(%rbx)
	movl	%r8d, %r14d
	addl	$1, %r8d
	subl	%edi, %r8d
	imull	88(%rbx), %r8d
	sall	%cl, %r14d
	orl	%r14d, %esi
	movl	%esi, 80(%rbx)
	movl	%r8d, 88(%rbx)
	cmpl	$3, %edx
	je	.L383
	movslq	100(%rbx), %rcx
	cmpl	%ecx, %r12d
	jne	.L382
	leal	1(%rcx), %r10d
	leaq	(%rbx,%rcx,4), %r14
	movl	%r15d, %ecx
	movl	92(%rbx), %r8d
	movl	8(%r14), %r11d
	subl	%r10d, %ecx
	movl	$-256, %edi
	sall	$3, %ecx
	sall	%cl, %edi
	movl	%r11d, %esi
	andl	%edi, %r8d
	sall	%cl, %esi
	andl	96(%rbx), %edi
	orl	%esi, %r8d
	movl	%r8d, 92(%rbx)
	movl	28(%r14), %r8d
	movl	%r10d, 100(%rbx)
	movl	%r8d, %esi
	addl	$1, %r8d
	subl	%r11d, %r8d
	imull	104(%rbx), %r8d
	sall	%cl, %esi
	orl	%esi, %edi
	movl	%edi, 96(%rbx)
	movl	%r8d, 104(%rbx)
	cmpl	$4, %edx
	je	.L382
	cmpl	116(%rbx), %r12d
	jne	.L382
	leaq	(%rbx,%r9,4), %r8
	movl	%r15d, %ecx
	movl	108(%rbx), %edi
	movl	$-256, %esi
	movl	8(%r8), %r9d
	subl	%eax, %ecx
	sall	$3, %ecx
	sall	%cl, %esi
	movl	%r9d, %r10d
	sall	%cl, %r10d
	andl	%esi, %edi
	orl	%r10d, %edi
	movl	112(%rbx), %r10d
	movl	%edi, 108(%rbx)
	movl	28(%r8), %edi
	andl	%esi, %r10d
	movl	%eax, 116(%rbx)
	movl	%edi, %r11d
	addl	$1, %edi
	subl	%r9d, %edi
	imull	120(%rbx), %edi
	sall	%cl, %r11d
	orl	%r11d, %r10d
	movl	%r10d, 112(%rbx)
	movl	%edi, 120(%rbx)
	cmpl	$5, %edx
	je	.L382
	cmpl	132(%rbx), %r12d
	jne	.L382
	movl	8(%r8), %r10d
	movl	124(%rbx), %edi
	movl	%r10d, %r9d
	andl	%esi, %edi
	sall	%cl, %r9d
	orl	%r9d, %edi
	movl	128(%rbx), %r9d
	movl	%edi, 124(%rbx)
	movl	28(%r8), %edi
	andl	%esi, %r9d
	movl	%eax, 132(%rbx)
	movl	%edi, %r11d
	addl	$1, %edi
	subl	%r10d, %edi
	imull	136(%rbx), %edi
	sall	%cl, %r11d
	orl	%r11d, %r9d
	movl	%r9d, 128(%rbx)
	movl	%edi, 136(%rbx)
	cmpl	$6, %edx
	je	.L382
	cmpl	%r12d, 148(%rbx)
	jne	.L382
	movl	8(%r8), %r9d
	movl	140(%rbx), %edi
	movl	%r9d, %r10d
	andl	%esi, %edi
	andl	144(%rbx), %esi
	sall	%cl, %r10d
	orl	%r10d, %edi
	movl	%edi, 140(%rbx)
	movl	28(%r8), %edi
	movl	%eax, 148(%rbx)
	movl	%edi, %r14d
	addl	$1, %edi
	subl	%r9d, %edi
	imull	152(%rbx), %edi
	sall	%cl, %r14d
	orl	%r14d, %esi
	movl	%esi, 144(%rbx)
	movl	%edi, 152(%rbx)
.L382:
	movl	52(%rbx), %r8d
	movl	%r8d, %r12d
	testl	%edx, %edx
	jle	.L306
	movl	56(%rbx), %ecx
	leal	1(%r8), %eax
	cmpl	%ecx, %r13d
	jg	.L386
.L307:
	movl	$1, 160(%rbx)
.L309:
	movl	$0, 156(%rbx)
	movl	$1, %eax
	jmp	.L302
	.p2align 4,,10
	.p2align 3
.L383:
	movl	%eax, %r8d
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L315:
	xorl	%eax, %eax
.L302:
	movq	-56(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L387
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L321:
	.cfi_restore_state
	movl	$2, %r10d
	movl	$1, %eax
.L308:
	cmpl	%esi, %r8d
	jge	.L384
	salq	$4, %rax
	movl	%ecx, 56(%rax,%rbx)
.L384:
	subq	$8, %rsp
	leaq	-60(%rbp), %rax
	movl	$16, %edx
	xorl	%r9d, %r9d
	pushq	%rax
	leaq	44(%rbx), %rdi
	xorl	%r8d, %r8d
	leaq	_ZN6icu_67L13compareRangesEPKvS1_S1_(%rip), %rcx
	movl	%r10d, 160(%rbx)
	movl	%r10d, %esi
	movl	$0, -60(%rbp)
	call	uprv_sortArray_67@PLT
	popq	%rax
	movl	$1, %eax
	popq	%rdx
	movl	$0, 156(%rbx)
	jmp	.L302
.L322:
	movl	$3, %r10d
	movl	$2, %eax
	jmp	.L308
.L323:
	movl	$4, %r10d
	movl	$3, %eax
	jmp	.L308
.L324:
	movl	$5, %r10d
	movl	$4, %eax
	jmp	.L308
.L325:
	movl	$6, %r10d
	movl	$5, %eax
	jmp	.L308
.L385:
	movl	$7, %r10d
	movl	$6, %eax
	jmp	.L308
.L387:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2102:
	.size	_ZN6icu_6716CollationWeights12allocWeightsEjji, .-_ZN6icu_6716CollationWeights12allocWeightsEjji
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716CollationWeights10nextWeightEv
	.type	_ZN6icu_6716CollationWeights10nextWeightEv, @function
_ZN6icu_6716CollationWeights10nextWeightEv:
.LFB2103:
	.cfi_startproc
	endbr64
	movl	156(%rdi), %eax
	movl	$-1, %r8d
	cmpl	160(%rdi), %eax
	jge	.L412
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%eax, %rdx
	salq	$4, %rdx
	leaq	(%rdi,%rdx), %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	44(%r9), %r8d
	subl	$1, 56(%r9)
	je	.L415
	movl	52(%r9), %edx
	movl	$4, %esi
	movl	%r8d, %eax
	movl	%esi, %ecx
	subl	%edx, %ecx
	sall	$3, %ecx
	shrl	%cl, %eax
	movslq	%edx, %rcx
	movzbl	%al, %eax
	cmpl	24(%rdi,%rcx,4), %eax
	jb	.L416
	movl	$-1, %r12d
	leaq	(%rdi,%rcx,4), %r15
	movl	$32, %r13d
	xorl	%r14d, %r14d
	leal	0(,%rdx,8), %ecx
	movl	%r12d, %r10d
	leal	-1(%rdx), %r11d
	movl	%r13d, %ebx
	shrl	%cl, %r10d
	cmpl	$32, %ecx
	movl	%r10d, %eax
	cmovge	%r14d, %eax
	subl	%ecx, %ebx
	movl	%ebx, %ecx
	movl	$-256, %ebx
	movl	%ebx, %r10d
	sall	%cl, %r10d
	orl	%eax, %r10d
	movl	4(%r15), %eax
	andl	%r8d, %r10d
	sall	%cl, %eax
	movl	%esi, %ecx
	orl	%eax, %r10d
	subl	%r11d, %ecx
	sall	$3, %ecx
	movl	%r10d, %eax
	shrl	%cl, %eax
	movzbl	%al, %eax
	cmpl	20(%r15), %eax
	jb	.L408
	movslq	%r11d, %rax
	leal	0(,%r11,8), %ecx
	movl	%r13d, %r11d
	leaq	(%rdi,%rax,4), %r15
	movl	%r12d, %eax
	shrl	%cl, %eax
	cmpl	$32, %ecx
	cmovge	%r14d, %eax
	subl	%ecx, %r11d
	movl	%r11d, %ecx
	movl	%ebx, %r11d
	sall	%cl, %r11d
	orl	%r11d, %eax
	leal	-2(%rdx), %r11d
	andl	%eax, %r10d
	movl	4(%r15), %eax
	sall	%cl, %eax
	movl	%esi, %ecx
	orl	%eax, %r10d
	subl	%r11d, %ecx
	sall	$3, %ecx
	movl	%r10d, %eax
	shrl	%cl, %eax
	movzbl	%al, %eax
	cmpl	20(%r15), %eax
	jb	.L408
	movslq	%r11d, %rax
	leal	0(,%r11,8), %ecx
	movl	%r13d, %r11d
	leaq	(%rdi,%rax,4), %r15
	movl	%r12d, %eax
	shrl	%cl, %eax
	cmpl	$32, %ecx
	cmovge	%r14d, %eax
	subl	%ecx, %r11d
	movl	%r11d, %ecx
	movl	%ebx, %r11d
	sall	%cl, %r11d
	orl	%r11d, %eax
	leal	-3(%rdx), %r11d
	andl	%eax, %r10d
	movl	4(%r15), %eax
	sall	%cl, %eax
	movl	%esi, %ecx
	orl	%eax, %r10d
	subl	%r11d, %ecx
	sall	$3, %ecx
	movl	%r10d, %eax
	shrl	%cl, %eax
	movzbl	%al, %eax
	cmpl	20(%r15), %eax
	jb	.L408
	movslq	%r11d, %rax
	leal	0(,%r11,8), %ecx
	movl	%r13d, %r11d
	leaq	(%rdi,%rax,4), %r15
	movl	%r12d, %eax
	shrl	%cl, %eax
	cmpl	$32, %ecx
	cmovge	%r14d, %eax
	subl	%ecx, %r11d
	movl	%r11d, %ecx
	movl	%ebx, %r11d
	sall	%cl, %r11d
	orl	%r11d, %eax
	leal	-4(%rdx), %r11d
	andl	%eax, %r10d
	movl	4(%r15), %eax
	sall	%cl, %eax
	movl	%esi, %ecx
	orl	%eax, %r10d
	subl	%r11d, %ecx
	sall	$3, %ecx
	movl	%r10d, %eax
	shrl	%cl, %eax
	movzbl	%al, %eax
	cmpl	20(%r15), %eax
	jb	.L408
	leal	0(,%r11,8), %ecx
	movslq	%r11d, %rax
	shrl	%cl, %r12d
	cmpl	$32, %ecx
	movl	4(%rdi,%rax,4), %eax
	cmovl	%r12d, %r14d
	subl	%ecx, %r13d
	subl	$5, %edx
	movl	%r13d, %ecx
	subl	%edx, %esi
	sall	%cl, %ebx
	sall	%cl, %eax
	leal	0(,%rsi,8), %ecx
	orl	%r14d, %ebx
	andl	%ebx, %r10d
	orl	%eax, %r10d
	movl	%r10d, %eax
	shrl	%cl, %eax
	movzbl	%al, %eax
.L399:
	leal	0(,%rdx,8), %ecx
	movl	$-1, %edi
	addl	$1, %eax
	movl	$0, %edx
	shrl	%cl, %edi
	movl	$32, %esi
	cmpl	$32, %ecx
	cmovge	%edx, %edi
	subl	%ecx, %esi
	movl	%esi, %ecx
	movl	$-256, %esi
	sall	%cl, %esi
	sall	%cl, %eax
	movl	%esi, %edx
	orl	%edi, %edx
	andl	%r10d, %edx
	orl	%eax, %edx
	movl	%r8d, %eax
	movl	%edx, 44(%r9)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L415:
	.cfi_restore_state
	addl	$1, %eax
	movl	%eax, 156(%rdi)
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L412:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L408:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	%r11d, %edx
	jmp	.L399
	.p2align 4,,10
	.p2align 3
.L416:
	movl	%r8d, %r10d
	jmp	.L399
	.cfi_endproc
.LFE2103:
	.size	_ZN6icu_6716CollationWeights10nextWeightEv, .-_ZN6icu_6716CollationWeights10nextWeightEv
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	0
	.long	0
	.long	2
	.long	2
	.align 16
.LC1:
	.long	0
	.long	0
	.long	255
	.long	255
	.align 16
.LC2:
	.long	0
	.long	0
	.long	63
	.long	63
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
