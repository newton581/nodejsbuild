	.file	"tmutamt.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714TimeUnitAmount17getDynamicClassIDEv
	.type	_ZNK6icu_6714TimeUnitAmount17getDynamicClassIDEv, @function
_ZNK6icu_6714TimeUnitAmount17getDynamicClassIDEv:
.LFB2238:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6714TimeUnitAmount16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2238:
	.size	_ZNK6icu_6714TimeUnitAmount17getDynamicClassIDEv, .-_ZNK6icu_6714TimeUnitAmount17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714TimeUnitAmounteqERKNS_7UObjectE
	.type	_ZNK6icu_6714TimeUnitAmounteqERKNS_7UObjectE, @function
_ZNK6icu_6714TimeUnitAmounteqERKNS_7UObjectE:
.LFB2249:
	.cfi_startproc
	endbr64
	jmp	_ZNK6icu_677MeasureeqERKNS_7UObjectE@PLT
	.cfi_endproc
.LFE2249:
	.size	_ZNK6icu_6714TimeUnitAmounteqERKNS_7UObjectE, .-_ZNK6icu_6714TimeUnitAmounteqERKNS_7UObjectE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714TimeUnitAmount5cloneEv
	.type	_ZNK6icu_6714TimeUnitAmount5cloneEv, @function
_ZNK6icu_6714TimeUnitAmount5cloneEv:
.LFB2250:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movl	$128, %edi
	pushq	%r12
	.cfi_offset 12, -32
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L4
	movq	%rax, %rdi
	movq	%r13, %rsi
	call	_ZN6icu_677MeasureC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6714TimeUnitAmountE(%rip), %rax
	movq	%rax, (%r12)
.L4:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2250:
	.size	_ZNK6icu_6714TimeUnitAmount5cloneEv, .-_ZNK6icu_6714TimeUnitAmount5cloneEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714TimeUnitAmountD2Ev
	.type	_ZN6icu_6714TimeUnitAmountD2Ev, @function
_ZN6icu_6714TimeUnitAmountD2Ev:
.LFB2252:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6714TimeUnitAmountE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_677MeasureD2Ev@PLT
	.cfi_endproc
.LFE2252:
	.size	_ZN6icu_6714TimeUnitAmountD2Ev, .-_ZN6icu_6714TimeUnitAmountD2Ev
	.globl	_ZN6icu_6714TimeUnitAmountD1Ev
	.set	_ZN6icu_6714TimeUnitAmountD1Ev,_ZN6icu_6714TimeUnitAmountD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714TimeUnitAmountD0Ev
	.type	_ZN6icu_6714TimeUnitAmountD0Ev, @function
_ZN6icu_6714TimeUnitAmountD0Ev:
.LFB2254:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6714TimeUnitAmountE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_677MeasureD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2254:
	.size	_ZN6icu_6714TimeUnitAmountD0Ev, .-_ZN6icu_6714TimeUnitAmountD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714TimeUnitAmount16getStaticClassIDEv
	.type	_ZN6icu_6714TimeUnitAmount16getStaticClassIDEv, @function
_ZN6icu_6714TimeUnitAmount16getStaticClassIDEv:
.LFB2237:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6714TimeUnitAmount16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2237:
	.size	_ZN6icu_6714TimeUnitAmount16getStaticClassIDEv, .-_ZN6icu_6714TimeUnitAmount16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714TimeUnitAmountC2ERKNS_11FormattableENS_8TimeUnit15UTimeUnitFieldsER10UErrorCode
	.type	_ZN6icu_6714TimeUnitAmountC2ERKNS_11FormattableENS_8TimeUnit15UTimeUnitFieldsER10UErrorCode, @function
_ZN6icu_6714TimeUnitAmountC2ERKNS_11FormattableENS_8TimeUnit15UTimeUnitFieldsER10UErrorCode:
.LFB2240:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	movq	%rcx, %rsi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movl	%edx, %edi
	subq	$8, %rsp
	call	_ZN6icu_678TimeUnit14createInstanceENS0_15UTimeUnitFieldsER10UErrorCode@PLT
	movq	%r12, %rcx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%rax, %rdx
	call	_ZN6icu_677MeasureC2ERKNS_11FormattableEPNS_11MeasureUnitER10UErrorCode@PLT
	leaq	16+_ZTVN6icu_6714TimeUnitAmountE(%rip), %rax
	movq	%rax, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2240:
	.size	_ZN6icu_6714TimeUnitAmountC2ERKNS_11FormattableENS_8TimeUnit15UTimeUnitFieldsER10UErrorCode, .-_ZN6icu_6714TimeUnitAmountC2ERKNS_11FormattableENS_8TimeUnit15UTimeUnitFieldsER10UErrorCode
	.globl	_ZN6icu_6714TimeUnitAmountC1ERKNS_11FormattableENS_8TimeUnit15UTimeUnitFieldsER10UErrorCode
	.set	_ZN6icu_6714TimeUnitAmountC1ERKNS_11FormattableENS_8TimeUnit15UTimeUnitFieldsER10UErrorCode,_ZN6icu_6714TimeUnitAmountC2ERKNS_11FormattableENS_8TimeUnit15UTimeUnitFieldsER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714TimeUnitAmountC2ERKS0_
	.type	_ZN6icu_6714TimeUnitAmountC2ERKS0_, @function
_ZN6icu_6714TimeUnitAmountC2ERKS0_:
.LFB2246:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_677MeasureC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6714TimeUnitAmountE(%rip), %rax
	movq	%rax, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2246:
	.size	_ZN6icu_6714TimeUnitAmountC2ERKS0_, .-_ZN6icu_6714TimeUnitAmountC2ERKS0_
	.globl	_ZN6icu_6714TimeUnitAmountC1ERKS0_
	.set	_ZN6icu_6714TimeUnitAmountC1ERKS0_,_ZN6icu_6714TimeUnitAmountC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714TimeUnitAmountaSERKS0_
	.type	_ZN6icu_6714TimeUnitAmountaSERKS0_, @function
_ZN6icu_6714TimeUnitAmountaSERKS0_:
.LFB2248:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN6icu_677MeasureaSERKS0_@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2248:
	.size	_ZN6icu_6714TimeUnitAmountaSERKS0_, .-_ZN6icu_6714TimeUnitAmountaSERKS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714TimeUnitAmount11getTimeUnitEv
	.type	_ZNK6icu_6714TimeUnitAmount11getTimeUnitEv, @function
_ZNK6icu_6714TimeUnitAmount11getTimeUnitEv:
.LFB2255:
	.cfi_startproc
	endbr64
	movq	120(%rdi), %rax
	ret
	.cfi_endproc
.LFE2255:
	.size	_ZNK6icu_6714TimeUnitAmount11getTimeUnitEv, .-_ZNK6icu_6714TimeUnitAmount11getTimeUnitEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714TimeUnitAmount16getTimeUnitFieldEv
	.type	_ZNK6icu_6714TimeUnitAmount16getTimeUnitFieldEv, @function
_ZNK6icu_6714TimeUnitAmount16getTimeUnitFieldEv:
.LFB2256:
	.cfi_startproc
	endbr64
	movq	120(%rdi), %rdi
	jmp	_ZNK6icu_678TimeUnit16getTimeUnitFieldEv@PLT
	.cfi_endproc
.LFE2256:
	.size	_ZNK6icu_6714TimeUnitAmount16getTimeUnitFieldEv, .-_ZNK6icu_6714TimeUnitAmount16getTimeUnitFieldEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714TimeUnitAmountC2EdNS_8TimeUnit15UTimeUnitFieldsER10UErrorCode
	.type	_ZN6icu_6714TimeUnitAmountC2EdNS_8TimeUnit15UTimeUnitFieldsER10UErrorCode, @function
_ZN6icu_6714TimeUnitAmountC2EdNS_8TimeUnit15UTimeUnitFieldsER10UErrorCode:
.LFB2243:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-160(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movl	%esi, %edi
	movq	%rdx, %rsi
	subq	$144, %rsp
	movsd	%xmm0, -168(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_678TimeUnit14createInstanceENS0_15UTimeUnitFieldsER10UErrorCode@PLT
	movsd	-168(%rbp), %xmm0
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN6icu_6711FormattableC1Ed@PLT
	movq	%r12, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_677MeasureC2ERKNS_11FormattableEPNS_11MeasureUnitER10UErrorCode@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6711FormattableD1Ev@PLT
	leaq	16+_ZTVN6icu_6714TimeUnitAmountE(%rip), %rax
	movq	%rax, (%rbx)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L25
	addq	$144, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L25:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2243:
	.size	_ZN6icu_6714TimeUnitAmountC2EdNS_8TimeUnit15UTimeUnitFieldsER10UErrorCode, .-_ZN6icu_6714TimeUnitAmountC2EdNS_8TimeUnit15UTimeUnitFieldsER10UErrorCode
	.globl	_ZN6icu_6714TimeUnitAmountC1EdNS_8TimeUnit15UTimeUnitFieldsER10UErrorCode
	.set	_ZN6icu_6714TimeUnitAmountC1EdNS_8TimeUnit15UTimeUnitFieldsER10UErrorCode,_ZN6icu_6714TimeUnitAmountC2EdNS_8TimeUnit15UTimeUnitFieldsER10UErrorCode
	.weak	_ZTSN6icu_6714TimeUnitAmountE
	.section	.rodata._ZTSN6icu_6714TimeUnitAmountE,"aG",@progbits,_ZTSN6icu_6714TimeUnitAmountE,comdat
	.align 16
	.type	_ZTSN6icu_6714TimeUnitAmountE, @object
	.size	_ZTSN6icu_6714TimeUnitAmountE, 26
_ZTSN6icu_6714TimeUnitAmountE:
	.string	"N6icu_6714TimeUnitAmountE"
	.weak	_ZTIN6icu_6714TimeUnitAmountE
	.section	.data.rel.ro._ZTIN6icu_6714TimeUnitAmountE,"awG",@progbits,_ZTIN6icu_6714TimeUnitAmountE,comdat
	.align 8
	.type	_ZTIN6icu_6714TimeUnitAmountE, @object
	.size	_ZTIN6icu_6714TimeUnitAmountE, 24
_ZTIN6icu_6714TimeUnitAmountE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6714TimeUnitAmountE
	.quad	_ZTIN6icu_677MeasureE
	.weak	_ZTVN6icu_6714TimeUnitAmountE
	.section	.data.rel.ro.local._ZTVN6icu_6714TimeUnitAmountE,"awG",@progbits,_ZTVN6icu_6714TimeUnitAmountE,comdat
	.align 8
	.type	_ZTVN6icu_6714TimeUnitAmountE, @object
	.size	_ZTVN6icu_6714TimeUnitAmountE, 56
_ZTVN6icu_6714TimeUnitAmountE:
	.quad	0
	.quad	_ZTIN6icu_6714TimeUnitAmountE
	.quad	_ZN6icu_6714TimeUnitAmountD1Ev
	.quad	_ZN6icu_6714TimeUnitAmountD0Ev
	.quad	_ZNK6icu_6714TimeUnitAmount17getDynamicClassIDEv
	.quad	_ZNK6icu_6714TimeUnitAmount5cloneEv
	.quad	_ZNK6icu_6714TimeUnitAmounteqERKNS_7UObjectE
	.local	_ZZN6icu_6714TimeUnitAmount16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6714TimeUnitAmount16getStaticClassIDEvE7classID,1,1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
