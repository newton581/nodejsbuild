	.file	"number_formatimpl.cpp"
	.text
	.section	.text._ZNK6icu_676number4impl13EmptyModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode,"axG",@progbits,_ZNK6icu_676number4impl13EmptyModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_676number4impl13EmptyModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode
	.type	_ZNK6icu_676number4impl13EmptyModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode, @function
_ZNK6icu_676number4impl13EmptyModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode:
.LFB3386:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3386:
	.size	_ZNK6icu_676number4impl13EmptyModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode, .-_ZNK6icu_676number4impl13EmptyModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode
	.section	.text._ZNK6icu_676number4impl13EmptyModifier15getPrefixLengthEv,"axG",@progbits,_ZNK6icu_676number4impl13EmptyModifier15getPrefixLengthEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_676number4impl13EmptyModifier15getPrefixLengthEv
	.type	_ZNK6icu_676number4impl13EmptyModifier15getPrefixLengthEv, @function
_ZNK6icu_676number4impl13EmptyModifier15getPrefixLengthEv:
.LFB3387:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3387:
	.size	_ZNK6icu_676number4impl13EmptyModifier15getPrefixLengthEv, .-_ZNK6icu_676number4impl13EmptyModifier15getPrefixLengthEv
	.section	.text._ZNK6icu_676number4impl13EmptyModifier17getCodePointCountEv,"axG",@progbits,_ZNK6icu_676number4impl13EmptyModifier17getCodePointCountEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_676number4impl13EmptyModifier17getCodePointCountEv
	.type	_ZNK6icu_676number4impl13EmptyModifier17getCodePointCountEv, @function
_ZNK6icu_676number4impl13EmptyModifier17getCodePointCountEv:
.LFB3388:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3388:
	.size	_ZNK6icu_676number4impl13EmptyModifier17getCodePointCountEv, .-_ZNK6icu_676number4impl13EmptyModifier17getCodePointCountEv
	.section	.text._ZNK6icu_676number4impl13EmptyModifier8isStrongEv,"axG",@progbits,_ZNK6icu_676number4impl13EmptyModifier8isStrongEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_676number4impl13EmptyModifier8isStrongEv
	.type	_ZNK6icu_676number4impl13EmptyModifier8isStrongEv, @function
_ZNK6icu_676number4impl13EmptyModifier8isStrongEv:
.LFB3389:
	.cfi_startproc
	endbr64
	movzbl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE3389:
	.size	_ZNK6icu_676number4impl13EmptyModifier8isStrongEv, .-_ZNK6icu_676number4impl13EmptyModifier8isStrongEv
	.section	.text._ZNK6icu_676number4impl13EmptyModifier13containsFieldENS_22FormattedStringBuilder5FieldE,"axG",@progbits,_ZNK6icu_676number4impl13EmptyModifier13containsFieldENS_22FormattedStringBuilder5FieldE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_676number4impl13EmptyModifier13containsFieldENS_22FormattedStringBuilder5FieldE
	.type	_ZNK6icu_676number4impl13EmptyModifier13containsFieldENS_22FormattedStringBuilder5FieldE, @function
_ZNK6icu_676number4impl13EmptyModifier13containsFieldENS_22FormattedStringBuilder5FieldE:
.LFB3390:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3390:
	.size	_ZNK6icu_676number4impl13EmptyModifier13containsFieldENS_22FormattedStringBuilder5FieldE, .-_ZNK6icu_676number4impl13EmptyModifier13containsFieldENS_22FormattedStringBuilder5FieldE
	.section	.text._ZNK6icu_676number4impl13EmptyModifier13getParametersERNS1_8Modifier10ParametersE,"axG",@progbits,_ZNK6icu_676number4impl13EmptyModifier13getParametersERNS1_8Modifier10ParametersE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_676number4impl13EmptyModifier13getParametersERNS1_8Modifier10ParametersE
	.type	_ZNK6icu_676number4impl13EmptyModifier13getParametersERNS1_8Modifier10ParametersE, @function
_ZNK6icu_676number4impl13EmptyModifier13getParametersERNS1_8Modifier10ParametersE:
.LFB3391:
	.cfi_startproc
	endbr64
	movq	$0, (%rsi)
	ret
	.cfi_endproc
.LFE3391:
	.size	_ZNK6icu_676number4impl13EmptyModifier13getParametersERNS1_8Modifier10ParametersE, .-_ZNK6icu_676number4impl13EmptyModifier13getParametersERNS1_8Modifier10ParametersE
	.section	.text._ZNK6icu_676number4impl13EmptyModifier22semanticallyEquivalentERKNS1_8ModifierE,"axG",@progbits,_ZNK6icu_676number4impl13EmptyModifier22semanticallyEquivalentERKNS1_8ModifierE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_676number4impl13EmptyModifier22semanticallyEquivalentERKNS1_8ModifierE
	.type	_ZNK6icu_676number4impl13EmptyModifier22semanticallyEquivalentERKNS1_8ModifierE, @function
_ZNK6icu_676number4impl13EmptyModifier22semanticallyEquivalentERKNS1_8ModifierE:
.LFB3392:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rsi), %rax
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	*32(%rax)
	popq	%rbp
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	sete	%al
	ret
	.cfi_endproc
.LFE3392:
	.size	_ZNK6icu_676number4impl13EmptyModifier22semanticallyEquivalentERKNS1_8ModifierE, .-_ZNK6icu_676number4impl13EmptyModifier22semanticallyEquivalentERKNS1_8ModifierE
	.section	.text._ZN6icu_676number4impl24ImmutablePatternModifierD0Ev,"axG",@progbits,_ZN6icu_676number4impl24ImmutablePatternModifierD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_676number4impl24ImmutablePatternModifierD0Ev
	.type	_ZN6icu_676number4impl24ImmutablePatternModifierD0Ev, @function
_ZN6icu_676number4impl24ImmutablePatternModifierD0Ev:
.LFB4045:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_676number4impl24ImmutablePatternModifierE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L11
	movq	(%rdi), %rax
	call	*8(%rax)
.L11:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE4045:
	.size	_ZN6icu_676number4impl24ImmutablePatternModifierD0Ev, .-_ZN6icu_676number4impl24ImmutablePatternModifierD0Ev
	.section	.text._ZN6icu_676number4impl13EmptyModifierD2Ev,"axG",@progbits,_ZN6icu_676number4impl13EmptyModifierD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_676number4impl13EmptyModifierD2Ev
	.type	_ZN6icu_676number4impl13EmptyModifierD2Ev, @function
_ZN6icu_676number4impl13EmptyModifierD2Ev:
.LFB4850:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_676number4impl13EmptyModifierE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	.cfi_endproc
.LFE4850:
	.size	_ZN6icu_676number4impl13EmptyModifierD2Ev, .-_ZN6icu_676number4impl13EmptyModifierD2Ev
	.weak	_ZN6icu_676number4impl13EmptyModifierD1Ev
	.set	_ZN6icu_676number4impl13EmptyModifierD1Ev,_ZN6icu_676number4impl13EmptyModifierD2Ev
	.section	.text._ZN6icu_676number4impl13EmptyModifierD0Ev,"axG",@progbits,_ZN6icu_676number4impl13EmptyModifierD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_676number4impl13EmptyModifierD0Ev
	.type	_ZN6icu_676number4impl13EmptyModifierD0Ev, @function
_ZN6icu_676number4impl13EmptyModifierD0Ev:
.LFB4852:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_676number4impl13EmptyModifierE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE4852:
	.size	_ZN6icu_676number4impl13EmptyModifierD0Ev, .-_ZN6icu_676number4impl13EmptyModifierD0Ev
	.section	.text._ZNK6icu_676number4impl10MicroProps15processQuantityERNS1_15DecimalQuantityERS2_R10UErrorCode,"axG",@progbits,_ZNK6icu_676number4impl10MicroProps15processQuantityERNS1_15DecimalQuantityERS2_R10UErrorCode,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_676number4impl10MicroProps15processQuantityERNS1_15DecimalQuantityERS2_R10UErrorCode
	.type	_ZNK6icu_676number4impl10MicroProps15processQuantityERNS1_15DecimalQuantityERS2_R10UErrorCode, @function
_ZNK6icu_676number4impl10MicroProps15processQuantityERNS1_15DecimalQuantityERS2_R10UErrorCode:
.LFB3426:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	cmpq	%rdx, %rdi
	je	.L23
	movdqu	8(%rdi), %xmm0
	movq	%rdx, %r12
	leaq	200(%rdi), %rsi
	movups	%xmm0, 8(%rdx)
	movdqu	24(%rdi), %xmm1
	movups	%xmm1, 24(%rdx)
	movl	40(%rdi), %eax
	movl	%eax, 40(%rdx)
	movzbl	44(%rdi), %eax
	movb	%al, 44(%rdx)
	movq	48(%rdi), %rax
	movq	%rax, 48(%rdx)
	movl	56(%rdi), %eax
	movl	%eax, 56(%rdx)
	movq	60(%rdi), %rax
	movq	%rax, 60(%rdx)
	movl	68(%rdi), %eax
	movl	%eax, 68(%rdx)
	movq	72(%rdi), %rax
	movq	%rax, 72(%rdx)
	movzbl	80(%rdi), %eax
	movb	%al, 80(%rdx)
	movl	84(%rdi), %eax
	movl	%eax, 84(%rdx)
	movl	88(%rdi), %eax
	movl	%eax, 88(%rdx)
	movzbl	92(%rdi), %eax
	movb	%al, 92(%rdx)
	movzbl	93(%rdi), %eax
	movb	%al, 93(%rdx)
	movzbl	94(%rdi), %eax
	movb	%al, 94(%rdx)
	movzbl	95(%rdi), %eax
	movb	%al, 95(%rdx)
	movzbl	96(%rdi), %eax
	movb	%al, 96(%rdx)
	movzbl	97(%rdi), %eax
	movb	%al, 97(%rdx)
	movzbl	98(%rdi), %eax
	movb	%al, 98(%rdx)
	movzbl	99(%rdi), %eax
	movb	%al, 99(%rdx)
	movzbl	100(%rdi), %eax
	movb	%al, 100(%rdx)
	movzbl	101(%rdi), %eax
	movb	%al, 101(%rdx)
	movq	104(%rdi), %rax
	movq	%rax, 104(%rdx)
	movq	112(%rdi), %rax
	movq	%rax, 112(%rdx)
	movq	120(%rdi), %rax
	movq	%rax, 120(%rdx)
	movq	128(%rdi), %rax
	movq	%rax, 128(%rdx)
	movq	152(%rdi), %rax
	movl	144(%rdi), %edx
	movq	%rax, 152(%r12)
	movzbl	168(%rdi), %eax
	movl	%edx, 144(%r12)
	movb	%al, 168(%r12)
	movzbl	184(%rdi), %eax
	leaq	200(%r12), %rdi
	movb	%al, 184(%r12)
	call	_ZN6icu_676number5ScaleaSERKS1_@PLT
	movq	224(%rbx), %rax
	movq	%rax, 224(%r12)
	movzbl	232(%rbx), %eax
	movb	%al, 232(%r12)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	.cfi_restore_state
	movb	$1, 232(%rdi)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3426:
	.size	_ZNK6icu_676number4impl10MicroProps15processQuantityERNS1_15DecimalQuantityERS2_R10UErrorCode, .-_ZNK6icu_676number4impl10MicroProps15processQuantityERNS1_15DecimalQuantityERS2_R10UErrorCode
	.section	.text._ZN6icu_676number4impl10MicroPropsD2Ev,"axG",@progbits,_ZN6icu_676number4impl10MicroPropsD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_676number4impl10MicroPropsD2Ev
	.type	_ZN6icu_676number4impl10MicroPropsD2Ev, @function
_ZN6icu_676number4impl10MicroPropsD2Ev:
.LFB4846:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_676number4impl10MicroPropsE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	16+_ZTVN6icu_676number4impl13EmptyModifierE(%rip), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	leaq	200(%rdi), %rdi
	movq	%rax, -200(%rdi)
	leaq	16+_ZTVN6icu_676number4impl23MultiplierFormatHandlerE(%rip), %rax
	movq	%rax, -8(%rdi)
	call	_ZN6icu_676number5ScaleD1Ev@PLT
	movq	%r12, 176(%rbx)
	leaq	176(%rbx), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%r12, 160(%rbx)
	leaq	160(%rbx), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	leaq	16+_ZTVN6icu_676number4impl18ScientificModifierE(%rip), %rax
	leaq	136(%rbx), %rdi
	movq	%rax, 136(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	.cfi_endproc
.LFE4846:
	.size	_ZN6icu_676number4impl10MicroPropsD2Ev, .-_ZN6icu_676number4impl10MicroPropsD2Ev
	.weak	_ZN6icu_676number4impl10MicroPropsD1Ev
	.set	_ZN6icu_676number4impl10MicroPropsD1Ev,_ZN6icu_676number4impl10MicroPropsD2Ev
	.section	.text._ZN6icu_676number4impl10MicroPropsD0Ev,"axG",@progbits,_ZN6icu_676number4impl10MicroPropsD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_676number4impl10MicroPropsD0Ev
	.type	_ZN6icu_676number4impl10MicroPropsD0Ev, @function
_ZN6icu_676number4impl10MicroPropsD0Ev:
.LFB4848:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_676number4impl10MicroPropsE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	200(%rdi), %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	leaq	16+_ZTVN6icu_676number4impl13EmptyModifierE(%rip), %rbx
	movq	%rax, -200(%rdi)
	leaq	16+_ZTVN6icu_676number4impl23MultiplierFormatHandlerE(%rip), %rax
	movq	%rax, -8(%rdi)
	call	_ZN6icu_676number5ScaleD1Ev@PLT
	movq	%rbx, 176(%r12)
	leaq	176(%r12), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%rbx, 160(%r12)
	leaq	160(%r12), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	leaq	16+_ZTVN6icu_676number4impl18ScientificModifierE(%rip), %rax
	leaq	136(%r12), %rdi
	movq	%rax, 136(%r12)
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	popq	%rbx
	movq	%r12, %rdi
	movl	$240, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE4848:
	.size	_ZN6icu_676number4impl10MicroPropsD0Ev, .-_ZN6icu_676number4impl10MicroPropsD0Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev:
.LFB3793:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3793:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei:
.LFB3796:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	cmpl	$40, %esi
	jg	.L41
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L29
	cmpb	$0, 12(%rbx)
	jne	.L42
.L33:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L29:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L33
	.cfi_endproc
.LFE3796:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EED2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev:
.LFB3799:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L45
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3799:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5EOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_:
.LFB3802:
	.cfi_startproc
	endbr64
	movzbl	12(%rsi), %eax
	movq	(%rsi), %r8
	movslq	8(%rsi), %rdx
	movb	%al, 12(%rdi)
	leaq	13(%rsi), %rax
	movq	%r8, (%rdi)
	movl	%edx, 8(%rdi)
	cmpq	%rax, %r8
	je	.L48
	movq	%rax, (%rsi)
	movl	$40, 8(%rsi)
	movb	$0, 12(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	leaq	13(%rdi), %r9
	movq	%r8, %rsi
	movq	%r9, (%rdi)
	movq	%r9, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE3802:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_:
.LFB3804:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 12(%rdi)
	movq	%rsi, %rbx
	jne	.L54
.L50:
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movzbl	12(%rbx), %eax
	movb	%al, 12(%r12)
	movq	(%rbx), %rsi
	leaq	13(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L55
	movq	%rsi, (%r12)
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L55:
	leaq	13(%r12), %rdi
	movslq	8(%rbx), %rdx
	movq	%rdi, (%r12)
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3804:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv:
.LFB3805:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE3805:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv:
.LFB3806:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3806:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv:
.LFB3807:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	addq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3807:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3808:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3808:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3809:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3809:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci:
.LFB3810:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L71
	testl	%edx, %edx
	jle	.L71
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 12(%rdi)
	jne	.L74
.L63:
	movq	%rsi, (%rbx)
	movl	%edx, 8(%rbx)
	movb	$0, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L71:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L74:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	%edx, -28(%rbp)
	movq	%rsi, -24(%rbp)
	call	uprv_free_67@PLT
	movl	-28(%rbp), %edx
	movq	-24(%rbp), %rsi
	jmp	.L63
	.cfi_endproc
.LFE3810:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii:
.LFB3811:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L78
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movl	%esi, %r13d
	movl	%edx, %r12d
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L78
	testl	%r12d, %r12d
	jg	.L85
	cmpb	$0, 12(%rbx)
	jne	.L86
.L80:
	movq	%r14, (%rbx)
	movq	%r14, %rax
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L85:
	.cfi_restore_state
	cmpl	%r12d, 8(%rbx)
	cmovle	8(%rbx), %r12d
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	je	.L80
.L86:
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L78:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3811:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, .-_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi:
.LFB3812:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 12(%rdi)
	movq	%rdi, %rbx
	je	.L88
	movq	(%rdi), %r8
.L89:
	leaq	13(%rbx), %rax
	movl	%r12d, 0(%r13)
	movq	%rax, (%rbx)
	movq	%r8, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L88:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L92
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	cmovg	%eax, %r12d
	movslq	%r12d, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L92
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L92:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3812:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, .-_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv:
.LFB3813:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L99
	ret
	.p2align 4,,10
	.p2align 3
.L99:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3813:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv:
.LFB3814:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3814:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_:
.LFB3815:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3815:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_:
.LFB3816:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3816:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5ERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_:
.LFB3818:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3818:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_:
.LFB3820:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3820:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev
	.type	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev, @function
_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev:
.LFB3436:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3436:
	.size	_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev, .-_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev
	.globl	_ZN6icu_676number4impl19MicroPropsGeneratorD1Ev
	.set	_ZN6icu_676number4impl19MicroPropsGeneratorD1Ev,_ZN6icu_676number4impl19MicroPropsGeneratorD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl19MicroPropsGeneratorD0Ev
	.type	_ZN6icu_676number4impl19MicroPropsGeneratorD0Ev, @function
_ZN6icu_676number4impl19MicroPropsGeneratorD0Ev:
.LFB3438:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE3438:
	.size	_ZN6icu_676number4impl19MicroPropsGeneratorD0Ev, .-_ZN6icu_676number4impl19MicroPropsGeneratorD0Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl19NumberFormatterImpl10preProcessERNS1_15DecimalQuantityERNS1_10MicroPropsER10UErrorCode
	.type	_ZNK6icu_676number4impl19NumberFormatterImpl10preProcessERNS1_15DecimalQuantityERNS1_10MicroPropsER10UErrorCode, @function
_ZNK6icu_676number4impl19NumberFormatterImpl10preProcessERNS1_15DecimalQuantityERNS1_10MicroPropsER10UErrorCode:
.LFB3469:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L111
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rcx, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L114
	movq	(%rdi), %rax
	movq	%rdx, %rbx
	movq	%rsi, %r13
	call	*16(%rax)
	addq	$8, %rsp
	leaq	72(%rbx), %rdi
	movq	%r12, %rdx
	popq	%rbx
	.cfi_restore 3
	movq	%r13, %rsi
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_676number12IntegerWidth5applyERNS0_4impl15DecimalQuantityER10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L111:
	ret
	.p2align 4,,10
	.p2align 3
.L114:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movl	$5, (%rcx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3469:
	.size	_ZNK6icu_676number4impl19NumberFormatterImpl10preProcessERNS1_15DecimalQuantityERNS1_10MicroPropsER10UErrorCode, .-_ZNK6icu_676number4impl19NumberFormatterImpl10preProcessERNS1_15DecimalQuantityERNS1_10MicroPropsER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl19NumberFormatterImpl16preProcessUnsafeERNS1_15DecimalQuantityER10UErrorCode
	.type	_ZN6icu_676number4impl19NumberFormatterImpl16preProcessUnsafeERNS1_15DecimalQuantityER10UErrorCode, @function
_ZN6icu_676number4impl19NumberFormatterImpl16preProcessUnsafeERNS1_15DecimalQuantityER10UErrorCode:
.LFB3470:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	8(%rdi), %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L115
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	movq	%rdx, %r12
	testq	%rdi, %rdi
	je	.L119
	movq	(%rdi), %rax
	movq	%rsi, %r13
	movq	%rdx, %rcx
	movq	%r14, %rdx
	call	*16(%rax)
	leaq	80(%rbx), %rdi
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	_ZNK6icu_676number12IntegerWidth5applyERNS0_4impl15DecimalQuantityER10UErrorCode@PLT
.L115:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L119:
	.cfi_restore_state
	movl	$5, (%rdx)
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3470:
	.size	_ZN6icu_676number4impl19NumberFormatterImpl16preProcessUnsafeERNS1_15DecimalQuantityER10UErrorCode, .-_ZN6icu_676number4impl19NumberFormatterImpl16preProcessUnsafeERNS1_15DecimalQuantityER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl19NumberFormatterImpl15getPrefixSuffixENS1_6SignumENS_14StandardPlural4FormERNS_22FormattedStringBuilderER10UErrorCode
	.type	_ZNK6icu_676number4impl19NumberFormatterImpl15getPrefixSuffixENS1_6SignumENS_14StandardPlural4FormERNS_22FormattedStringBuilderER10UErrorCode, @function
_ZNK6icu_676number4impl19NumberFormatterImpl15getPrefixSuffixENS1_6SignumENS_14StandardPlural4FormERNS_22FormattedStringBuilderER10UErrorCode:
.LFB3471:
	.cfi_startproc
	endbr64
	movl	(%r8), %r9d
	testl	%r9d, %r9d
	jle	.L128
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L128:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%r8, %rbx
	subq	$8, %rsp
	movq	288(%rdi), %rdi
	call	_ZNK6icu_676number4impl24ImmutablePatternModifier11getModifierENS1_6SignumENS_14StandardPlural4FormE@PLT
	movq	%rbx, %r8
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %r12
	movq	(%rax), %rax
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*16(%rax)
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L122
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L122:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3471:
	.size	_ZNK6icu_676number4impl19NumberFormatterImpl15getPrefixSuffixENS1_6SignumENS_14StandardPlural4FormERNS_22FormattedStringBuilderER10UErrorCode, .-_ZNK6icu_676number4impl19NumberFormatterImpl15getPrefixSuffixENS1_6SignumENS_14StandardPlural4FormERNS_22FormattedStringBuilderER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl19NumberFormatterImpl21getPrefixSuffixUnsafeENS1_6SignumENS_14StandardPlural4FormERNS_22FormattedStringBuilderER10UErrorCode
	.type	_ZN6icu_676number4impl19NumberFormatterImpl21getPrefixSuffixUnsafeENS1_6SignumENS_14StandardPlural4FormERNS_22FormattedStringBuilderER10UErrorCode, @function
_ZN6icu_676number4impl19NumberFormatterImpl21getPrefixSuffixUnsafeENS1_6SignumENS_14StandardPlural4FormERNS_22FormattedStringBuilderER10UErrorCode:
.LFB3472:
	.cfi_startproc
	endbr64
	movl	(%r8), %r9d
	testl	%r9d, %r9d
	jle	.L137
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L137:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%r8, %rbx
	subq	$8, %rsp
	movq	280(%rdi), %rdi
	call	_ZN6icu_676number4impl22MutablePatternModifier19setNumberPropertiesENS1_6SignumENS_14StandardPlural4FormE@PLT
	movq	%rbx, %r8
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	280(%r12), %rdi
	movq	%r13, %rsi
	movq	(%rdi), %rax
	call	*24(%rax)
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L131
	movq	280(%r12), %rdi
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L131:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3472:
	.size	_ZN6icu_676number4impl19NumberFormatterImpl21getPrefixSuffixUnsafeENS1_6SignumENS_14StandardPlural4FormERNS_22FormattedStringBuilderER10UErrorCode, .-_ZN6icu_676number4impl19NumberFormatterImpl21getPrefixSuffixUnsafeENS1_6SignumENS_14StandardPlural4FormERNS_22FormattedStringBuilderER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl19NumberFormatterImpl18resolvePluralRulesEPKNS_11PluralRulesERKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_676number4impl19NumberFormatterImpl18resolvePluralRulesEPKNS_11PluralRulesERKNS_6LocaleER10UErrorCode, @function
_ZN6icu_676number4impl19NumberFormatterImpl18resolvePluralRulesEPKNS_11PluralRulesERKNS_6LocaleER10UErrorCode:
.LFB3483:
	.cfi_startproc
	endbr64
	movq	%rsi, %rax
	testq	%rsi, %rsi
	je	.L149
	ret
	.p2align 4,,10
	.p2align 3
.L149:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	256(%rdi), %rax
	testq	%rax, %rax
	je	.L150
.L139:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L150:
	.cfi_restore_state
	movq	%rdx, %rdi
	movq	%rcx, %rsi
	call	_ZN6icu_6711PluralRules9forLocaleERKNS_6LocaleER10UErrorCode@PLT
	movq	256(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L141
	movq	(%rdi), %rdx
	movq	%rax, -24(%rbp)
	call	*8(%rdx)
	movq	-24(%rbp), %rax
.L141:
	movq	%rax, 256(%rbx)
	jmp	.L139
	.cfi_endproc
.LFE3483:
	.size	_ZN6icu_676number4impl19NumberFormatterImpl18resolvePluralRulesEPKNS_11PluralRulesERKNS_6LocaleER10UErrorCode, .-_ZN6icu_676number4impl19NumberFormatterImpl18resolvePluralRulesEPKNS_11PluralRulesERKNS_6LocaleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl19NumberFormatterImpl12writeAffixesERKNS1_10MicroPropsERNS_22FormattedStringBuilderEiiR10UErrorCode
	.type	_ZN6icu_676number4impl19NumberFormatterImpl12writeAffixesERKNS1_10MicroPropsERNS_22FormattedStringBuilderEiiR10UErrorCode, @function
_ZN6icu_676number4impl19NumberFormatterImpl12writeAffixesERKNS1_10MicroPropsERNS_22FormattedStringBuilderEiiR10UErrorCode:
.LFB3484:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	128(%rdi), %rdi
	movq	%r8, -56(%rbp)
	movq	(%rdi), %rax
	call	*16(%rax)
	movl	60(%r12), %esi
	movq	-56(%rbp), %r8
	movl	%eax, %ebx
	testl	%esi, %esi
	jg	.L155
	movq	120(%r12), %rdi
	leal	0(%r13,%rax), %ecx
	movl	%r15d, %edx
	movq	%r14, %rsi
	movq	%r8, -56(%rbp)
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	112(%r12), %rdi
	movq	-56(%rbp), %r8
	movl	%r15d, %edx
	addl	%eax, %ebx
	movq	%r14, %rsi
	movq	(%rdi), %rax
	leal	0(%r13,%rbx), %ecx
	call	*16(%rax)
	leaq	-40(%rbp), %rsp
	addl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L155:
	.cfi_restore_state
	subq	$8, %rsp
	movq	112(%r12), %rdx
	movq	120(%r12), %rsi
	movq	%r14, %rcx
	pushq	%r8
	leaq	60(%r12), %rdi
	leal	0(%r13,%rax), %r9d
	movl	%r15d, %r8d
	call	_ZNK6icu_676number4impl6Padder11padAndApplyERKNS1_8ModifierES5_RNS_22FormattedStringBuilderEiiR10UErrorCode@PLT
	popq	%rdx
	popq	%rcx
	leaq	-40(%rbp), %rsp
	addl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3484:
	.size	_ZN6icu_676number4impl19NumberFormatterImpl12writeAffixesERKNS1_10MicroPropsERNS_22FormattedStringBuilderEiiR10UErrorCode, .-_ZN6icu_676number4impl19NumberFormatterImpl12writeAffixesERKNS1_10MicroPropsERNS_22FormattedStringBuilderEiiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl19NumberFormatterImpl18writeIntegerDigitsERKNS1_10MicroPropsERNS1_15DecimalQuantityERNS_22FormattedStringBuilderEiR10UErrorCode
	.type	_ZN6icu_676number4impl19NumberFormatterImpl18writeIntegerDigitsERKNS1_10MicroPropsERNS1_15DecimalQuantityERNS_22FormattedStringBuilderEiR10UErrorCode, @function
_ZN6icu_676number4impl19NumberFormatterImpl18writeIntegerDigitsERKNS1_10MicroPropsERNS1_15DecimalQuantityERNS_22FormattedStringBuilderEiR10UErrorCode:
.LFB3486:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$136, %rsp
	movq	%rdi, -136(%rbp)
	movq	%rsi, %rdi
	movq	%rdx, -144(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK6icu_676number4impl15DecimalQuantity24getUpperDisplayMagnitudeEv@PLT
	testl	%eax, %eax
	js	.L167
	addl	$1, %eax
	addq	$48, %rbx
	xorl	%r12d, %r12d
	movl	%eax, -156(%rbp)
	leaq	-128(%rbp), %rax
	movq	%rbx, -168(%rbp)
	xorl	%ebx, %ebx
	movq	%rax, -152(%rbp)
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L173:
	movq	-144(%rbp), %rdi
	addl	%esi, %edx
	movq	%r15, %r8
	movl	%r14d, %esi
	movl	$32, %ecx
	addl	$1, %ebx
	call	_ZN6icu_6722FormattedStringBuilder15insertCodePointEiiNS0_5FieldER10UErrorCode@PLT
	addl	%eax, %r12d
	cmpl	%ebx, -156(%rbp)
	je	.L156
.L165:
	movq	-168(%rbp), %rdi
	movq	%r13, %rdx
	movl	%ebx, %esi
	call	_ZNK6icu_676number4impl7Grouper15groupAtPositionEiRKNS1_15DecimalQuantityE@PLT
	testb	%al, %al
	je	.L158
	movq	-136(%rbp), %rax
	cmpb	$0, 92(%rax)
	movq	104(%rax), %rsi
	je	.L159
	movq	-152(%rbp), %rdi
	addq	$1096, %rsi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
.L160:
	movq	-144(%rbp), %rdi
	movq	%r15, %r8
	movl	$38, %ecx
	movl	%r14d, %esi
	movq	-152(%rbp), %rdx
	call	_ZN6icu_6722FormattedStringBuilder6insertEiRKNS_13UnicodeStringENS0_5FieldER10UErrorCode@PLT
	movq	-152(%rbp), %rdi
	addl	%eax, %r12d
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L158:
	movl	%ebx, %esi
	movq	%r13, %rdi
	call	_ZNK6icu_676number4impl15DecimalQuantity8getDigitEi@PLT
	movq	-136(%rbp), %rcx
	movsbl	%al, %edx
	movq	104(%rcx), %rcx
	movl	1864(%rcx), %esi
	cmpl	$-1, %esi
	jne	.L173
	subl	$1, %eax
	cmpb	$8, %al
	jbe	.L163
	leaq	264(%rcx), %rdx
.L164:
	movq	-144(%rbp), %rdi
	movq	%r15, %r8
	movl	%r14d, %esi
	addl	$1, %ebx
	movl	$32, %ecx
	call	_ZN6icu_6722FormattedStringBuilder6insertEiRKNS_13UnicodeStringENS0_5FieldER10UErrorCode@PLT
	addl	%eax, %r12d
	cmpl	%ebx, -156(%rbp)
	jne	.L165
.L156:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L174
	addq	$136, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L159:
	.cfi_restore_state
	movq	-152(%rbp), %rdi
	addq	$72, %rsi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L163:
	addl	$17, %edx
	movslq	%edx, %rdx
	salq	$6, %rdx
	leaq	8(%rcx,%rdx), %rdx
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L167:
	xorl	%r12d, %r12d
	jmp	.L156
.L174:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3486:
	.size	_ZN6icu_676number4impl19NumberFormatterImpl18writeIntegerDigitsERKNS1_10MicroPropsERNS1_15DecimalQuantityERNS_22FormattedStringBuilderEiR10UErrorCode, .-_ZN6icu_676number4impl19NumberFormatterImpl18writeIntegerDigitsERKNS1_10MicroPropsERNS1_15DecimalQuantityERNS_22FormattedStringBuilderEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl19NumberFormatterImpl19writeFractionDigitsERKNS1_10MicroPropsERNS1_15DecimalQuantityERNS_22FormattedStringBuilderEiR10UErrorCode
	.type	_ZN6icu_676number4impl19NumberFormatterImpl19writeFractionDigitsERKNS1_10MicroPropsERNS1_15DecimalQuantityERNS_22FormattedStringBuilderEiR10UErrorCode, @function
_ZN6icu_676number4impl19NumberFormatterImpl19writeFractionDigitsERKNS1_10MicroPropsERNS1_15DecimalQuantityERNS_22FormattedStringBuilderEiR10UErrorCode:
.LFB3487:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -64(%rbp)
	movq	%rsi, %rdi
	movl	%ecx, -56(%rbp)
	call	_ZNK6icu_676number4impl15DecimalQuantity24getLowerDisplayMagnitudeEv@PLT
	movl	%eax, %ebx
	negl	%ebx
	movl	%ebx, -52(%rbp)
	testl	%eax, %eax
	jns	.L182
	xorl	%ebx, %ebx
	xorl	%r12d, %r12d
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L185:
	addl	%edi, %edx
	movq	%r15, %r8
	movl	$33, %ecx
	movq	%r14, %rdi
	call	_ZN6icu_6722FormattedStringBuilder15insertCodePointEiiNS0_5FieldER10UErrorCode@PLT
	addl	$1, %ebx
	addl	%eax, %r12d
	cmpl	%ebx, -52(%rbp)
	jle	.L175
.L181:
	movl	%ebx, %esi
	movq	%r13, %rdi
	notl	%esi
	call	_ZNK6icu_676number4impl15DecimalQuantity8getDigitEi@PLT
	movq	-64(%rbp), %rcx
	movl	-56(%rbp), %edi
	movsbl	%al, %edx
	movq	104(%rcx), %rcx
	leal	(%rdi,%r12), %esi
	movl	1864(%rcx), %edi
	cmpl	$-1, %edi
	jne	.L185
	subl	$1, %eax
	cmpb	$8, %al
	jbe	.L179
	leaq	264(%rcx), %rdx
.L180:
	movq	%r15, %r8
	movl	$33, %ecx
	movq	%r14, %rdi
	addl	$1, %ebx
	call	_ZN6icu_6722FormattedStringBuilder6insertEiRKNS_13UnicodeStringENS0_5FieldER10UErrorCode@PLT
	addl	%eax, %r12d
	cmpl	%ebx, -52(%rbp)
	jg	.L181
.L175:
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L179:
	.cfi_restore_state
	addl	$17, %edx
	movslq	%edx, %rdx
	salq	$6, %rdx
	leaq	8(%rcx,%rdx), %rdx
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L182:
	xorl	%r12d, %r12d
	jmp	.L175
	.cfi_endproc
.LFE3487:
	.size	_ZN6icu_676number4impl19NumberFormatterImpl19writeFractionDigitsERKNS1_10MicroPropsERNS1_15DecimalQuantityERNS_22FormattedStringBuilderEiR10UErrorCode, .-_ZN6icu_676number4impl19NumberFormatterImpl19writeFractionDigitsERKNS1_10MicroPropsERNS1_15DecimalQuantityERNS_22FormattedStringBuilderEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl19NumberFormatterImpl11writeNumberERKNS1_10MicroPropsERNS1_15DecimalQuantityERNS_22FormattedStringBuilderEiR10UErrorCode
	.type	_ZN6icu_676number4impl19NumberFormatterImpl11writeNumberERKNS1_10MicroPropsERNS1_15DecimalQuantityERNS_22FormattedStringBuilderEiR10UErrorCode, @function
_ZN6icu_676number4impl19NumberFormatterImpl11writeNumberERKNS1_10MicroPropsERNS1_15DecimalQuantityERNS_22FormattedStringBuilderEiR10UErrorCode:
.LFB3485:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	call	*32(%rax)
	testb	%al, %al
	jne	.L200
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*24(%rax)
	testb	%al, %al
	je	.L189
	movq	104(%r12), %rsi
	addq	$968, %rsi
.L199:
	leaq	-128(%rbp), %r12
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	%r13, %rdi
	movq	%r15, %r8
	movl	$32, %ecx
	movq	%r12, %rdx
	movl	%r14d, %esi
	call	_ZN6icu_6722FormattedStringBuilder6insertEiRKNS_13UnicodeStringENS0_5FieldER10UErrorCode@PLT
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L186:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L201
	addq	$104, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L189:
	.cfi_restore_state
	movq	%r12, %rdi
	movq	%r15, %r8
	movl	%r14d, %ecx
	movq	%r13, %rdx
	movq	%rbx, %rsi
	call	_ZN6icu_676number4impl19NumberFormatterImpl18writeIntegerDigitsERKNS1_10MicroPropsERNS1_15DecimalQuantityERNS_22FormattedStringBuilderEiR10UErrorCode
	movq	%rbx, %rdi
	movl	%eax, -132(%rbp)
	call	_ZNK6icu_676number4impl15DecimalQuantity24getLowerDisplayMagnitudeEv@PLT
	testl	%eax, %eax
	js	.L190
	cmpl	$1, 88(%r12)
	je	.L190
.L191:
	movl	-132(%rbp), %eax
	movq	%rbx, %rsi
	movq	%r15, %r8
	movq	%r13, %rdx
	movq	%r12, %rdi
	leal	(%r14,%rax), %ecx
	call	_ZN6icu_676number4impl19NumberFormatterImpl19writeFractionDigitsERKNS1_10MicroPropsERNS1_15DecimalQuantityERNS_22FormattedStringBuilderEiR10UErrorCode
	movl	-132(%rbp), %ebx
	addl	%eax, %ebx
	jne	.L186
	movq	104(%r12), %rdx
	movl	1864(%rdx), %r9d
	cmpl	$-1, %r9d
	je	.L194
	movq	%r15, %r8
	movl	$32, %ecx
	movl	%r9d, %edx
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6722FormattedStringBuilder15insertCodePointEiiNS0_5FieldER10UErrorCode@PLT
	movl	%eax, %ebx
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L190:
	cmpb	$0, 92(%r12)
	movq	104(%r12), %rsi
	jne	.L202
	addq	$8, %rsi
.L198:
	leaq	-128(%rbp), %r10
	movq	%r10, %rdi
	movq	%r10, -144(%rbp)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	%r13, %rdi
	movq	%r15, %r8
	movl	$34, %ecx
	movq	-144(%rbp), %r10
	movl	-132(%rbp), %eax
	movq	%r10, %rdx
	leal	(%r14,%rax), %esi
	movq	%r10, -144(%rbp)
	call	_ZN6icu_6722FormattedStringBuilder6insertEiRKNS_13UnicodeStringENS0_5FieldER10UErrorCode@PLT
	movq	-144(%rbp), %r10
	addl	%eax, -132(%rbp)
	movq	%r10, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L200:
	movq	104(%r12), %rsi
	addq	$904, %rsi
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L202:
	addq	$648, %rsi
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L194:
	addq	$264, %rdx
	movq	%r15, %r8
	movl	%r14d, %esi
	movq	%r13, %rdi
	movl	$32, %ecx
	call	_ZN6icu_6722FormattedStringBuilder6insertEiRKNS_13UnicodeStringENS0_5FieldER10UErrorCode@PLT
	movl	%eax, %ebx
	jmp	.L186
.L201:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3485:
	.size	_ZN6icu_676number4impl19NumberFormatterImpl11writeNumberERKNS1_10MicroPropsERNS1_15DecimalQuantityERNS_22FormattedStringBuilderEiR10UErrorCode, .-_ZN6icu_676number4impl19NumberFormatterImpl11writeNumberERKNS1_10MicroPropsERNS1_15DecimalQuantityERNS_22FormattedStringBuilderEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl19NumberFormatterImpl6formatERNS1_15DecimalQuantityERNS_22FormattedStringBuilderER10UErrorCode
	.type	_ZNK6icu_676number4impl19NumberFormatterImpl6formatERNS1_15DecimalQuantityERNS_22FormattedStringBuilderER10UErrorCode, @function
_ZNK6icu_676number4impl19NumberFormatterImpl6formatERNS1_15DecimalQuantityERNS_22FormattedStringBuilderER10UErrorCode:
.LFB3453:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_676number4impl10MicroPropsE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-168(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	16+_ZTVN6icu_676number4impl13EmptyModifierE(%rip), %rbx
	subq	$280, %rsp
	movq	%rdi, -320(%rbp)
	movq	%r14, %rdi
	movq	%rdx, -312(%rbp)
	movl	$-3, %edx
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	movl	$-1, %ecx
	movq	%rax, -304(%rbp)
	movl	$0, -296(%rbp)
	movb	$1, -260(%rbp)
	movw	%dx, -256(%rbp)
	movl	$-2, -244(%rbp)
	movb	$0, -224(%rbp)
	movw	%cx, -232(%rbp)
	movq	$0, -184(%rbp)
	call	_ZN6icu_676number4impl18ScientificModifierC1Ev@PLT
	movl	0(%r13), %esi
	movq	%rbx, -128(%rbp)
	leaq	16+_ZTVN6icu_676number4impl23MultiplierFormatHandlerE(%rip), %r9
	movq	%rbx, -144(%rbp)
	movb	$0, -136(%rbp)
	movb	$1, -120(%rbp)
	movq	%r9, -112(%rbp)
	movl	$0, -104(%rbp)
	movq	$0, -96(%rbp)
	movl	$0, -88(%rbp)
	movb	$0, -72(%rbp)
	testl	%esi, %esi
	jg	.L204
	movq	-320(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L210
	movq	(%rdi), %rax
	leaq	-304(%rbp), %r15
	movq	%r13, %rcx
	movq	%r12, %rsi
	movq	%r15, %rdx
	call	*16(%rax)
	leaq	-232(%rbp), %rdi
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	_ZNK6icu_676number12IntegerWidth5applyERNS0_4impl15DecimalQuantityER10UErrorCode@PLT
	movl	0(%r13), %eax
	leaq	16+_ZTVN6icu_676number4impl23MultiplierFormatHandlerE(%rip), %r9
	testl	%eax, %eax
	jg	.L204
	movq	-312(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r13, %r8
	xorl	%ecx, %ecx
	movq	%r15, %rdi
	call	_ZN6icu_676number4impl19NumberFormatterImpl11writeNumberERKNS1_10MicroPropsERNS1_15DecimalQuantityERNS_22FormattedStringBuilderEiR10UErrorCode
	movq	%r13, %r8
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	-312(%rbp), %rsi
	movl	%eax, %ecx
	movl	%eax, %r12d
	call	_ZN6icu_676number4impl19NumberFormatterImpl12writeAffixesERKNS1_10MicroPropsERNS_22FormattedStringBuilderEiiR10UErrorCode
	leaq	16+_ZTVN6icu_676number4impl23MultiplierFormatHandlerE(%rip), %r9
	addl	%eax, %r12d
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L210:
	movl	$5, 0(%r13)
	.p2align 4,,10
	.p2align 3
.L204:
	xorl	%r12d, %r12d
.L206:
	leaq	16+_ZTVN6icu_676number4impl10MicroPropsE(%rip), %rax
	leaq	-104(%rbp), %rdi
	movq	%r9, -112(%rbp)
	movq	%rax, -304(%rbp)
	call	_ZN6icu_676number5ScaleD1Ev@PLT
	leaq	-128(%rbp), %rdi
	movq	%rbx, -128(%rbp)
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	leaq	-144(%rbp), %rdi
	movq	%rbx, -144(%rbp)
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	leaq	16+_ZTVN6icu_676number4impl18ScientificModifierE(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -168(%rbp)
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L211
	addq	$280, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L211:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3453:
	.size	_ZNK6icu_676number4impl19NumberFormatterImpl6formatERNS1_15DecimalQuantityERNS_22FormattedStringBuilderER10UErrorCode, .-_ZNK6icu_676number4impl19NumberFormatterImpl6formatERNS1_15DecimalQuantityERNS_22FormattedStringBuilderER10UErrorCode
	.section	.text._ZN6icu_676number4impl24ImmutablePatternModifierD2Ev,"axG",@progbits,_ZN6icu_676number4impl24ImmutablePatternModifierD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_676number4impl24ImmutablePatternModifierD2Ev
	.type	_ZN6icu_676number4impl24ImmutablePatternModifierD2Ev, @function
_ZN6icu_676number4impl24ImmutablePatternModifierD2Ev:
.LFB4043:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_676number4impl24ImmutablePatternModifierE(%rip), %rax
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L212
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L212:
	ret
	.cfi_endproc
.LFE4043:
	.size	_ZN6icu_676number4impl24ImmutablePatternModifierD2Ev, .-_ZN6icu_676number4impl24ImmutablePatternModifierD2Ev
	.weak	_ZN6icu_676number4impl24ImmutablePatternModifierD1Ev
	.set	_ZN6icu_676number4impl24ImmutablePatternModifierD1Ev,_ZN6icu_676number4impl24ImmutablePatternModifierD2Ev
	.section	.text._ZN6icu_676number4impl22MutablePatternModifierD2Ev,"axG",@progbits,_ZN6icu_676number4impl22MutablePatternModifierD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_676number4impl22MutablePatternModifierD2Ev
	.type	_ZN6icu_676number4impl22MutablePatternModifierD2Ev, @function
_ZN6icu_676number4impl22MutablePatternModifierD2Ev:
.LFB4036:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	120+_ZTVN6icu_676number4impl22MutablePatternModifierE(%rip), %rax
	leaq	-104(%rax), %rdx
	movq	%rax, %xmm1
	addq	$88, %rax
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	addq	$328, %rdi
	subq	$8, %rsp
	movq	%rax, -312(%rdi)
	movups	%xmm0, -328(%rdi)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	240(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	176(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, 124(%rbx)
	jne	.L217
.L215:
	leaq	80(%rbx), %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	leaq	16(%rbx), %rdi
	call	_ZN6icu_676number4impl14SymbolProviderD2Ev@PLT
	addq	$8, %rsp
	leaq	8(%rbx), %rdi
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L217:
	.cfi_restore_state
	movq	112(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L215
	.cfi_endproc
.LFE4036:
	.size	_ZN6icu_676number4impl22MutablePatternModifierD2Ev, .-_ZN6icu_676number4impl22MutablePatternModifierD2Ev
	.weak	_ZN6icu_676number4impl22MutablePatternModifierD1Ev
	.set	_ZN6icu_676number4impl22MutablePatternModifierD1Ev,_ZN6icu_676number4impl22MutablePatternModifierD2Ev
	.section	.text._ZN6icu_676number4impl22MutablePatternModifierD0Ev,"axG",@progbits,_ZN6icu_676number4impl22MutablePatternModifierD5Ev,comdat
	.p2align 4
	.weak	_ZThn8_N6icu_676number4impl22MutablePatternModifierD0Ev
	.type	_ZThn8_N6icu_676number4impl22MutablePatternModifierD0Ev, @function
_ZThn8_N6icu_676number4impl22MutablePatternModifierD0Ev:
.LFB4914:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	120+_ZTVN6icu_676number4impl22MutablePatternModifierE(%rip), %rax
	leaq	-104(%rax), %rdx
	movq	%rax, %xmm1
	addq	$88, %rax
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 13, -24
	leaq	-8(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	addq	$320, %rdi
	movq	%rax, -312(%rdi)
	movups	%xmm0, -328(%rdi)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	232(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	168(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, 116(%r12)
	jne	.L221
.L219:
	leaq	72(%r12), %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	leaq	8(%r12), %rdi
	call	_ZN6icu_676number4impl14SymbolProviderD2Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	popq	%r12
	movq	%r13, %rdi
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L221:
	.cfi_restore_state
	movq	104(%r12), %rdi
	call	uprv_free_67@PLT
	jmp	.L219
	.cfi_endproc
.LFE4914:
	.size	_ZThn8_N6icu_676number4impl22MutablePatternModifierD0Ev, .-_ZThn8_N6icu_676number4impl22MutablePatternModifierD0Ev
	.p2align 4
	.weak	_ZThn16_N6icu_676number4impl22MutablePatternModifierD0Ev
	.type	_ZThn16_N6icu_676number4impl22MutablePatternModifierD0Ev, @function
_ZThn16_N6icu_676number4impl22MutablePatternModifierD0Ev:
.LFB4913:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	120+_ZTVN6icu_676number4impl22MutablePatternModifierE(%rip), %rax
	leaq	-104(%rax), %rdx
	movq	%rax, %xmm1
	addq	$88, %rax
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 12, -24
	leaq	-16(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	addq	$312, %rdi
	movq	%rax, -312(%rdi)
	movups	%xmm0, -328(%rdi)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	224(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	160(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, 108(%rbx)
	jne	.L225
.L223:
	leaq	64(%rbx), %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_676number4impl14SymbolProviderD2Ev@PLT
	leaq	-8(%rbx), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L225:
	.cfi_restore_state
	movq	96(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L223
	.cfi_endproc
.LFE4913:
	.size	_ZThn16_N6icu_676number4impl22MutablePatternModifierD0Ev, .-_ZThn16_N6icu_676number4impl22MutablePatternModifierD0Ev
	.align 2
	.p2align 4
	.weak	_ZN6icu_676number4impl22MutablePatternModifierD0Ev
	.type	_ZN6icu_676number4impl22MutablePatternModifierD0Ev, @function
_ZN6icu_676number4impl22MutablePatternModifierD0Ev:
.LFB4038:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	120+_ZTVN6icu_676number4impl22MutablePatternModifierE(%rip), %rax
	leaq	-104(%rax), %rdx
	movq	%rax, %xmm1
	addq	$88, %rax
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 12, -24
	movq	%rdi, %r12
	addq	$328, %rdi
	subq	$8, %rsp
	movq	%rax, -312(%rdi)
	movups	%xmm0, -328(%rdi)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	240(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	176(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, 124(%r12)
	jne	.L229
.L227:
	leaq	80(%r12), %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	leaq	16(%r12), %rdi
	call	_ZN6icu_676number4impl14SymbolProviderD2Ev@PLT
	leaq	8(%r12), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L229:
	.cfi_restore_state
	movq	112(%r12), %rdi
	call	uprv_free_67@PLT
	jmp	.L227
	.cfi_endproc
.LFE4038:
	.size	_ZN6icu_676number4impl22MutablePatternModifierD0Ev, .-_ZN6icu_676number4impl22MutablePatternModifierD0Ev
	.section	.text._ZN6icu_676number4impl22MutablePatternModifierD2Ev,"axG",@progbits,_ZN6icu_676number4impl22MutablePatternModifierD5Ev,comdat
	.p2align 4
	.weak	_ZThn16_N6icu_676number4impl22MutablePatternModifierD1Ev
	.type	_ZThn16_N6icu_676number4impl22MutablePatternModifierD1Ev, @function
_ZThn16_N6icu_676number4impl22MutablePatternModifierD1Ev:
.LFB4915:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	120+_ZTVN6icu_676number4impl22MutablePatternModifierE(%rip), %rax
	leaq	-104(%rax), %rdx
	movq	%rax, %xmm1
	addq	$88, %rax
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	addq	$312, %rdi
	subq	$8, %rsp
	movq	%rax, -312(%rdi)
	movups	%xmm0, -328(%rdi)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	224(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	160(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, 108(%rbx)
	jne	.L233
.L231:
	leaq	64(%rbx), %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_676number4impl14SymbolProviderD2Ev@PLT
	addq	$8, %rsp
	leaq	-8(%rbx), %rdi
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L233:
	.cfi_restore_state
	movq	96(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L231
	.cfi_endproc
.LFE4915:
	.size	_ZThn16_N6icu_676number4impl22MutablePatternModifierD1Ev, .-_ZThn16_N6icu_676number4impl22MutablePatternModifierD1Ev
	.p2align 4
	.weak	_ZThn8_N6icu_676number4impl22MutablePatternModifierD1Ev
	.type	_ZThn8_N6icu_676number4impl22MutablePatternModifierD1Ev, @function
_ZThn8_N6icu_676number4impl22MutablePatternModifierD1Ev:
.LFB4916:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	120+_ZTVN6icu_676number4impl22MutablePatternModifierE(%rip), %rax
	leaq	-104(%rax), %rdx
	movq	%rax, %xmm1
	addq	$88, %rax
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 12, -24
	movq	%rdi, %r12
	addq	$320, %rdi
	subq	$8, %rsp
	movq	%rax, -312(%rdi)
	movups	%xmm0, -328(%rdi)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	232(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	168(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, 116(%r12)
	jne	.L237
.L235:
	leaq	72(%r12), %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	leaq	8(%r12), %rdi
	call	_ZN6icu_676number4impl14SymbolProviderD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L237:
	.cfi_restore_state
	movq	104(%r12), %rdi
	call	uprv_free_67@PLT
	jmp	.L235
	.cfi_endproc
.LFE4916:
	.size	_ZThn8_N6icu_676number4impl22MutablePatternModifierD1Ev, .-_ZThn8_N6icu_676number4impl22MutablePatternModifierD1Ev
	.section	.text._ZN6icu_676number4impl17ParsedPatternInfoD2Ev,"axG",@progbits,_ZN6icu_676number4impl17ParsedPatternInfoD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_676number4impl17ParsedPatternInfoD2Ev
	.type	_ZN6icu_676number4impl17ParsedPatternInfoD2Ev, @function
_ZN6icu_676number4impl17ParsedPatternInfoD2Ev:
.LFB4022:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_676number4impl17ParsedPatternInfoE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	296(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -296(%rdi)
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	leaq	128(%r12), %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	leaq	8(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676number4impl20AffixPatternProviderD2Ev@PLT
	.cfi_endproc
.LFE4022:
	.size	_ZN6icu_676number4impl17ParsedPatternInfoD2Ev, .-_ZN6icu_676number4impl17ParsedPatternInfoD2Ev
	.weak	_ZN6icu_676number4impl17ParsedPatternInfoD1Ev
	.set	_ZN6icu_676number4impl17ParsedPatternInfoD1Ev,_ZN6icu_676number4impl17ParsedPatternInfoD2Ev
	.section	.text._ZN6icu_676number4impl17ParsedPatternInfoD0Ev,"axG",@progbits,_ZN6icu_676number4impl17ParsedPatternInfoD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_676number4impl17ParsedPatternInfoD0Ev
	.type	_ZN6icu_676number4impl17ParsedPatternInfoD0Ev, @function
_ZN6icu_676number4impl17ParsedPatternInfoD0Ev:
.LFB4024:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_676number4impl17ParsedPatternInfoE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	296(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -296(%rdi)
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	leaq	128(%r12), %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	leaq	8(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl20AffixPatternProviderD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE4024:
	.size	_ZN6icu_676number4impl17ParsedPatternInfoD0Ev, .-_ZN6icu_676number4impl17ParsedPatternInfoD0Ev
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"latn"
.LC1:
	.string	"currency"
.LC2:
	.string	"none"
.LC3:
	.string	"percent"
.LC4:
	.string	"permille"
	.section	.rodata.str2.2,"aMS",@progbits,2
	.align 2
.LC5:
	.string	""
	.string	""
	.text
	.align 2
	.p2align 4
	.type	_ZN6icu_676number4impl19NumberFormatterImpl22macrosToMicroGeneratorERKNS1_10MacroPropsEbR10UErrorCode.part.0, @function
_ZN6icu_676number4impl19NumberFormatterImpl22macrosToMicroGeneratorERKNS1_10MacroPropsEbR10UErrorCode.part.0:
.LFB4912:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$344, %rsp
	movl	%edx, -340(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$3, 4(%rsi)
	je	.L243
	cmpl	$9, 64(%rsi)
	je	.L412
	cmpl	$-3, 112(%rsi)
	je	.L413
	movzbl	132(%rsi), %eax
	movb	%al, -328(%rbp)
	testb	%al, %al
	jne	.L414
	movl	136(%rsi), %eax
	movq	%rdi, %r12
	subl	$1, %eax
	cmpl	$1, %eax
	jbe	.L415
.L249:
	movl	184(%rbx), %eax
	testl	%eax, %eax
	jne	.L250
	leaq	16(%rbx), %r15
	movq	%r15, %rdi
	movq	%r15, -376(%rbp)
	call	_ZNK6icu_6711MeasureUnit7getTypeEv@PLT
	movl	$9, %ecx
	leaq	.LC1(%rip), %rsi
	movq	%rax, %rdi
	repz cmpsb
	movq	%r15, %rdi
	seta	%al
	sbbb	$0, %al
	movsbl	%al, %eax
	testl	%eax, %eax
	movl	%eax, -296(%rbp)
	sete	-341(%rbp)
	call	_ZNK6icu_6711MeasureUnit7getTypeEv@PLT
	movl	$5, %ecx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	repz cmpsb
	movq	%r15, %rdi
	seta	%r14b
	sbbb	$0, %r14b
	call	_ZNK6icu_6711MeasureUnit10getSubtypeEv@PLT
	movl	$8, %ecx
	leaq	.LC3(%rip), %rsi
	movsbl	%r14b, %r14d
	movq	%rax, %rdi
	repz cmpsb
	movq	%r15, %rdi
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	sete	-352(%rbp)
	call	_ZNK6icu_6711MeasureUnit10getSubtypeEv@PLT
	movl	$9, %ecx
	leaq	.LC4(%rip), %rsi
	movl	156(%rbx), %edx
	movq	%rax, %rdi
	repz cmpsb
	leaq	.LC5(%rip), %rcx
	movl	%edx, -380(%rbp)
	leaq	-208(%rbp), %rsi
	movq	%rcx, -208(%rbp)
	leaq	-160(%rbp), %rcx
	movq	%r13, %rdx
	movq	%rcx, %rdi
	movq	%rsi, -360(%rbp)
	movq	%rcx, -304(%rbp)
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	sete	-336(%rbp)
	call	_ZN6icu_6712CurrencyUnitC1ENS_14ConstChar16PtrER10UErrorCode@PLT
	movl	-296(%rbp), %r9d
	testl	%r9d, %r9d
	je	.L416
	movl	$1, -288(%rbp)
	movl	152(%rbx), %eax
	cmpl	$5, %eax
	je	.L256
	movl	%eax, -288(%rbp)
	testl	%r14d, %r14d
	je	.L254
	movb	$1, -328(%rbp)
	cmpl	$2, %eax
	je	.L254
.L331:
	movzbl	-336(%rbp), %eax
	orb	-352(%rbp), %al
	xorl	$1, %eax
	movb	%al, -328(%rbp)
.L254:
	leaq	136(%rbx), %r15
	movq	%r15, %rdi
	call	_ZNK6icu_676number4impl14SymbolsWrapper17isNumberingSystemEv@PLT
	testb	%al, %al
	je	.L257
	movq	%r15, %rdi
	call	_ZNK6icu_676number4impl14SymbolsWrapper18getNumberingSystemEv@PLT
	movq	$0, -320(%rbp)
	movq	%rax, %r14
.L258:
	movl	0(%r13), %r8d
	leaq	.LC0(%rip), %rax
	movq	%rax, -312(%rbp)
	testl	%r8d, %r8d
	jle	.L417
.L259:
	movq	-312(%rbp), %rsi
	leaq	101(%r12), %rdi
	movl	$8, %edx
	call	strncpy@PLT
	movb	$0, 109(%r12)
	movq	%r15, %rdi
	call	_ZNK6icu_676number4impl14SymbolsWrapper22isDecimalFormatSymbolsEv@PLT
	testb	%al, %al
	je	.L260
	movq	%r15, %rdi
	call	_ZNK6icu_676number4impl14SymbolsWrapper23getDecimalFormatSymbolsEv@PLT
	movq	%rax, 112(%r12)
.L261:
	movl	-296(%rbp), %edx
	testl	%edx, %edx
	jne	.L271
	movq	112(%r12), %rax
	movq	2416(%rax), %rax
	movq	%rax, -368(%rbp)
	testq	%rax, %rax
	je	.L271
.L272:
	movl	$440, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L274
	movq	.LC6(%rip), %xmm0
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movb	$0, 112(%r14)
	movabsq	$281474976645120, %r15
	movq	%rax, %xmm1
	movl	$2, %eax
	movq	%r15, 72(%r14)
	leaq	128(%r14), %rdi
	punpcklqdq	%xmm1, %xmm0
	movw	%ax, 16(%r14)
	movl	$0, 116(%r14)
	movb	$0, 120(%r14)
	movl	$0, 124(%r14)
	movups	%xmm0, (%r14)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 80(%r14)
	movups	%xmm0, 96(%r14)
	call	_ZN6icu_676number4impl15DecimalQuantityC1Ev@PLT
	movq	%r15, 240(%r14)
	pxor	%xmm0, %xmm0
	leaq	296(%r14), %rdi
	movb	$0, 200(%r14)
	movq	$0, 204(%r14)
	movb	$0, 212(%r14)
	movq	$0, 232(%r14)
	movb	$0, 280(%r14)
	movl	$0, 284(%r14)
	movb	$0, 288(%r14)
	movl	$0, 292(%r14)
	movups	%xmm0, 216(%r14)
	movups	%xmm0, 248(%r14)
	movups	%xmm0, 264(%r14)
	call	_ZN6icu_676number4impl15DecimalQuantityC1Ev@PLT
	pxor	%xmm0, %xmm0
	movq	$0, 372(%r14)
	movq	264(%r12), %r15
	leaq	8(%r14), %rax
	movb	$0, 368(%r14)
	movb	$0, 380(%r14)
	movq	$0, 400(%r14)
	movq	%rax, 408(%r14)
	movl	$0, 416(%r14)
	movq	$0, 424(%r14)
	movb	$0, 432(%r14)
	movups	%xmm0, 384(%r14)
	testq	%r15, %r15
	je	.L275
	movq	(%r15), %rax
	leaq	_ZN6icu_676number4impl17ParsedPatternInfoD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L276
	leaq	16+_ZTVN6icu_676number4impl17ParsedPatternInfoE(%rip), %rax
	leaq	296(%r15), %rdi
	movq	%rax, (%r15)
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	leaq	128(%r15), %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	leaq	8(%r15), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_676number4impl20AffixPatternProviderD2Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L275:
	movq	%r14, 264(%r12)
	leaq	-128(%rbp), %r15
	movq	-368(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_676number4impl13PatternParser18parseToPatternInfoERKNS_13UnicodeStringERNS1_17ParsedPatternInfoER10UErrorCode@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L410
	movl	168(%rbx), %eax
	leaq	8(%r12), %rdx
	testl	%eax, %eax
	je	.L418
.L277:
	leaq	200(%r12), %rax
	leaq	168(%rbx), %rsi
	movq	%rax, %rdi
	movq	%rax, -352(%rbp)
	call	_ZN6icu_676number4impl23MultiplierFormatHandler11setAndChainERKNS0_5ScaleEPKNS1_19MicroPropsGeneratorE@PLT
.L278:
	movl	$0, -272(%rbp)
	movl	64(%rbx), %eax
	testl	%eax, %eax
	je	.L279
	movq	80(%rbx), %rax
	movdqu	64(%rbx), %xmm4
	movq	%rax, -256(%rbp)
	movl	88(%rbx), %eax
	movaps	%xmm4, -272(%rbp)
	movl	%eax, -248(%rbp)
.L280:
	movl	96(%rbx), %edx
	movq	-304(%rbp), %rcx
	movq	%r13, %r8
	leaq	-272(%rbp), %rsi
	movq	-360(%rbp), %rdi
	cmpl	$4, %edx
	cmove	-248(%rbp), %edx
	call	_ZN6icu_676number4impl12RoundingImplC1ERKNS0_9PrecisionE25UNumberFormatRoundingModeRKNS_12CurrencyUnitER10UErrorCode@PLT
	movl	-176(%rbp), %eax
	movdqa	-208(%rbp), %xmm2
	movdqa	-192(%rbp), %xmm3
	movl	%eax, 48(%r12)
	movzbl	-172(%rbp), %eax
	movups	%xmm2, 16(%r12)
	movb	%al, 52(%r12)
	movups	%xmm3, 32(%r12)
	movl	0(%r13), %r14d
	testl	%r14d, %r14d
	jg	.L410
	cmpw	$-3, 100(%rbx)
	je	.L284
	movq	100(%rbx), %rax
	movq	%rax, 56(%r12)
	movl	108(%rbx), %eax
	movl	%eax, 64(%r12)
.L285:
	leaq	216(%rbx), %rax
	movq	264(%r12), %rsi
	leaq	56(%r12), %rdi
	movq	%rax, %rdx
	movq	%rax, -360(%rbp)
	call	_ZN6icu_676number4impl7Grouper13setLocaleDataERKNS1_17ParsedPatternInfoERKNS_6LocaleE@PLT
	cmpl	$-2, 112(%rbx)
	je	.L287
	movq	112(%rbx), %rax
	movq	%rax, 68(%r12)
	movl	120(%rbx), %eax
	movl	%eax, 76(%r12)
.L288:
	cmpw	$-1, 124(%rbx)
	jne	.L344
	cmpb	$1, 132(%rbx)
	jne	.L289
.L344:
	movq	124(%rbx), %rax
	movq	%rax, 80(%r12)
	movzbl	132(%rbx), %eax
	movb	%al, 88(%r12)
.L291:
	movl	156(%rbx), %eax
	xorl	%edx, %edx
	movl	4(%rbx), %r11d
	cmpl	$7, %eax
	cmove	%edx, %eax
	movl	%eax, 92(%r12)
	movl	160(%rbx), %eax
	cmpl	$2, %eax
	cmove	%edx, %eax
	movl	%eax, 96(%r12)
	movzbl	-341(%rbp), %eax
	movb	%al, 100(%r12)
	testl	%r11d, %r11d
	je	.L419
	leaq	184(%r12), %rax
	movq	%rax, 136(%r12)
.L296:
	movl	$392, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L274
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN6icu_676number4impl22MutablePatternModifierC1Eb@PLT
	movq	280(%r12), %r15
	testq	%r15, %r15
	je	.L297
	movq	(%r15), %rax
	leaq	_ZN6icu_676number4impl22MutablePatternModifierD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L298
	leaq	120+_ZTVN6icu_676number4impl22MutablePatternModifierE(%rip), %rax
	leaq	328(%r15), %rdi
	leaq	-104(%rax), %rdx
	movq	%rax, %xmm5
	addq	$88, %rax
	movq	%rdx, %xmm0
	movq	%rax, 16(%r15)
	punpcklqdq	%xmm5, %xmm0
	movups	%xmm0, (%r15)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	240(%r15), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	176(%r15), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, 124(%r15)
	jne	.L420
.L299:
	leaq	80(%r15), %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	leaq	16(%r15), %rdi
	call	_ZN6icu_676number4impl14SymbolProviderD2Ev@PLT
	leaq	8(%r15), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L297:
	movq	192(%rbx), %rsi
	movq	%r14, 280(%r12)
	testq	%rsi, %rsi
	je	.L421
.L300:
	movzbl	_ZN6icu_67L15kUndefinedFieldE(%rip), %edx
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl22MutablePatternModifier14setPatternInfoEPKNS1_20AffixPatternProviderENS_22FormattedStringBuilder5FieldE@PLT
	movzbl	-336(%rbp), %edx
	movl	92(%r12), %esi
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl22MutablePatternModifier20setPatternAttributesE18UNumberSignDisplayb@PLT
	movq	%r14, %rdi
	call	_ZNK6icu_676number4impl22MutablePatternModifier12needsPluralsEv@PLT
	testb	%al, %al
	je	.L301
	movq	200(%rbx), %r8
	testq	%r8, %r8
	je	.L422
.L302:
	movq	112(%r12), %rsi
	movl	-288(%rbp), %ecx
	movq	%r13, %r9
	movq	%r14, %rdi
	movq	-304(%rbp), %rdx
	call	_ZN6icu_676number4impl22MutablePatternModifier10setSymbolsEPKNS_20DecimalFormatSymbolsERKNS_12CurrencyUnitE16UNumberUnitWidthPKNS_11PluralRulesER10UErrorCode@PLT
.L305:
	cmpb	$0, -340(%rbp)
	jne	.L423
.L306:
	movl	0(%r13), %r10d
	testl	%r10d, %r10d
	jg	.L410
	cmpb	$0, -328(%rbp)
	jne	.L424
	movl	-296(%rbp), %edi
	testl	%edi, %edi
	jne	.L316
	cmpl	$2, -288(%rbp)
	je	.L425
.L316:
	leaq	168(%r12), %rax
	movq	%rax, 120(%r12)
.L321:
	cmpl	$1, 4(%rbx)
	je	.L426
.L322:
	cmpb	$0, -340(%rbp)
	je	.L328
	movq	288(%r12), %rdi
	movq	-352(%rbp), %rsi
	call	_ZN6icu_676number4impl24ImmutablePatternModifier10addToChainEPKNS1_19MicroPropsGeneratorE@PLT
	movq	288(%r12), %r14
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L271:
	xorl	%edx, %edx
	cmpb	$0, -328(%rbp)
	jne	.L273
	cmpb	$0, -336(%rbp)
	jne	.L336
	cmpb	$0, -352(%rbp)
	jne	.L336
	movl	-296(%rbp), %eax
	testl	%eax, %eax
	jne	.L273
	cmpl	$2, -288(%rbp)
	movl	%eax, %edx
	je	.L273
	movl	-380(%rbp), %ecx
	leal	-3(%rcx), %eax
	cmpl	$1, %eax
	jbe	.L339
	movl	$1, %edx
	cmpl	$6, %ecx
	jne	.L273
.L339:
	movl	$2, %edx
	.p2align 4,,10
	.p2align 3
.L273:
	movq	-312(%rbp), %rsi
	leaq	216(%rbx), %rdi
	movq	%r13, %rcx
	call	_ZN6icu_676number4impl5utils18getPatternForStyleERKNS_6LocaleEPKcNS1_16CldrPatternStyleER10UErrorCode@PLT
	movq	%rax, -368(%rbp)
	movl	0(%r13), %eax
	testl	%eax, %eax
	jle	.L272
.L410:
	xorl	%r14d, %r14d
.L264:
	movq	-320(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L329
	movq	(%rdi), %rax
	call	*8(%rax)
.L329:
	movq	-304(%rbp), %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
.L242:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L427
	leaq	-40(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L257:
	.cfi_restore_state
	leaq	216(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6715NumberingSystem14createInstanceERKNS_6LocaleER10UErrorCode@PLT
	movq	%rax, -320(%rbp)
	movq	%rax, %r14
	jmp	.L258
	.p2align 4,,10
	.p2align 3
.L260:
	movl	$2816, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L262
	movq	%rax, %rdi
	leaq	216(%rbx), %rsi
	movq	%r13, %rcx
	movq	%r14, %rdx
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKNS_6LocaleERKNS_15NumberingSystemER10UErrorCode@PLT
	movl	0(%r13), %edi
	testl	%edi, %edi
	jg	.L269
	movl	-296(%rbp), %esi
	testl	%esi, %esi
	je	.L266
.L270:
	movq	248(%r12), %rdi
	movq	%r15, 112(%r12)
	testq	%rdi, %rdi
	je	.L268
	movq	(%rdi), %rax
	call	*8(%rax)
.L268:
	movq	%r15, 248(%r12)
	jmp	.L261
	.p2align 4,,10
	.p2align 3
.L416:
	leaq	-128(%rbp), %r14
	movq	-376(%rbp), %rsi
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN6icu_6712CurrencyUnitC1ERKNS_11MeasureUnitER10UErrorCode@PLT
	movq	-304(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN6icu_6712CurrencyUnitaSERKS0_@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	movl	152(%rbx), %eax
	movl	$1, %edx
	cmpl	$5, %eax
	cmove	%edx, %eax
	movl	%eax, -288(%rbp)
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L415:
	movq	144(%rsi), %r14
	testq	%r14, %r14
	jne	.L249
	movl	$7, (%rcx)
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L243:
	movl	8(%rsi), %eax
	xorl	%r14d, %r14d
	movl	%eax, (%rcx)
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L413:
	movl	116(%rsi), %eax
	xorl	%r14d, %r14d
	movl	%eax, (%rcx)
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L418:
	cmpq	$0, 176(%rbx)
	movq	%rdx, -352(%rbp)
	jne	.L277
	jmp	.L278
	.p2align 4,,10
	.p2align 3
.L250:
	movl	%eax, 0(%r13)
	xorl	%r14d, %r14d
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L266:
	leaq	-140(%rbp), %rsi
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN6icu_6720DecimalFormatSymbols11setCurrencyEPKDsR10UErrorCode@PLT
	movl	0(%r13), %ecx
	testl	%ecx, %ecx
	jle	.L270
	.p2align 4,,10
	.p2align 3
.L269:
	movq	(%r15), %rax
	movq	%r15, %rdi
	xorl	%r14d, %r14d
	call	*8(%rax)
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L417:
	movq	%r14, %rdi
	call	_ZNK6icu_6715NumberingSystem7getNameEv@PLT
	movq	%rax, -312(%rbp)
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L414:
	movl	124(%rsi), %eax
	xorl	%r14d, %r14d
	movl	%eax, (%rcx)
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L412:
	movl	72(%rsi), %eax
	xorl	%r14d, %r14d
	movl	%eax, (%rcx)
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L256:
	testl	%r14d, %r14d
	jne	.L331
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L279:
	cmpl	$1, 4(%rbx)
	je	.L428
	movl	-296(%rbp), %r15d
	testl	%r15d, %r15d
	jne	.L282
	movq	-360(%rbp), %rdi
	xorl	%esi, %esi
	call	_ZN6icu_676number9Precision8currencyE14UCurrencyUsage@PLT
.L409:
	movq	-192(%rbp), %rax
	movdqa	-208(%rbp), %xmm6
	movq	%rax, -256(%rbp)
	movl	-184(%rbp), %eax
	movaps	%xmm6, -272(%rbp)
	movl	%eax, -248(%rbp)
	jmp	.L280
.L262:
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L410
.L274:
	movl	$7, 0(%r13)
	jmp	.L410
	.p2align 4,,10
	.p2align 3
.L336:
	movl	$3, %edx
	jmp	.L273
	.p2align 4,,10
	.p2align 3
.L276:
	movq	%r15, %rdi
	call	*%rax
	jmp	.L275
	.p2align 4,,10
	.p2align 3
.L282:
	movq	-360(%rbp), %rdi
	movl	$6, %esi
	call	_ZN6icu_676number9Precision11maxFractionEi@PLT
	movq	-192(%rbp), %rax
	movdqa	-208(%rbp), %xmm7
	movq	%rax, -256(%rbp)
	movl	-184(%rbp), %eax
	movaps	%xmm7, -272(%rbp)
	movl	%eax, -248(%rbp)
	jmp	.L280
	.p2align 4,,10
	.p2align 3
.L422:
	movq	256(%r12), %r8
	testq	%r8, %r8
	jne	.L302
	movq	-360(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6711PluralRules9forLocaleERKNS_6LocaleER10UErrorCode@PLT
	movq	256(%r12), %rdi
	movq	%rax, %r8
	testq	%rdi, %rdi
	je	.L304
	movq	%rax, -336(%rbp)
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	-336(%rbp), %r8
.L304:
	movq	%r8, 256(%r12)
	jmp	.L302
	.p2align 4,,10
	.p2align 3
.L424:
	movq	200(%rbx), %r8
	testq	%r8, %r8
	je	.L429
.L311:
	subq	$8, %rsp
	movq	-352(%rbp), %r9
	movq	-360(%rbp), %rdi
	leaq	-288(%rbp), %rcx
	pushq	%r13
	movq	-376(%rbp), %rsi
	leaq	40(%rbx), %rdx
	call	_ZN6icu_676number4impl15LongNameHandler14forMeasureUnitERKNS_6LocaleERKNS_11MeasureUnitES8_RK16UNumberUnitWidthPKNS_11PluralRulesEPKNS1_19MicroPropsGeneratorER10UErrorCode@PLT
	movq	296(%r12), %rdi
	popq	%r8
	movq	%rax, -352(%rbp)
	popq	%r9
	testq	%rdi, %rdi
	je	.L320
.L411:
	movq	(%rdi), %rax
	call	*8(%rax)
.L320:
	movq	-352(%rbp), %rax
	movl	0(%r13), %esi
	movq	%rax, 296(%r12)
	testl	%esi, %esi
	jg	.L410
	jmp	.L321
	.p2align 4,,10
	.p2align 3
.L420:
	movq	112(%r15), %rdi
	call	uprv_free_67@PLT
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L284:
	cmpl	$1, 4(%rbx)
	je	.L430
	movl	$2, %edi
	call	_ZN6icu_676number4impl7Grouper11forStrategyE23UNumberGroupingStrategy@PLT
	movq	%rax, 56(%r12)
	movl	%edx, 64(%r12)
	jmp	.L285
	.p2align 4,,10
	.p2align 3
.L301:
	movq	112(%r12), %rsi
	movq	%r13, %r9
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movl	-288(%rbp), %ecx
	movq	-304(%rbp), %rdx
	call	_ZN6icu_676number4impl22MutablePatternModifier10setSymbolsEPKNS_20DecimalFormatSymbolsERKNS_12CurrencyUnitE16UNumberUnitWidthPKNS_11PluralRulesER10UErrorCode@PLT
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L419:
	movl	$40, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L274
	movq	112(%r12), %rdx
	movq	-352(%rbp), %rcx
	movq	%rax, %rdi
	leaq	4(%rbx), %rsi
	call	_ZN6icu_676number4impl17ScientificHandlerC1EPKNS0_8NotationEPKNS_20DecimalFormatSymbolsEPKNS1_19MicroPropsGeneratorE@PLT
	movq	272(%r12), %rdi
	testq	%rdi, %rdi
	je	.L295
	movq	(%rdi), %rax
	call	*8(%rax)
.L295:
	movq	%r15, 272(%r12)
	movq	%r15, -352(%rbp)
	jmp	.L296
	.p2align 4,,10
	.p2align 3
.L423:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl22MutablePatternModifier15createImmutableER10UErrorCode@PLT
	movq	288(%r12), %r8
	movq	%rax, %r15
	testq	%r8, %r8
	je	.L307
	movq	(%r8), %rax
	leaq	_ZN6icu_676number4impl24ImmutablePatternModifierD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L308
	movq	8(%r8), %rdi
	leaq	16+_ZTVN6icu_676number4impl24ImmutablePatternModifierE(%rip), %rax
	movq	%rax, (%r8)
	testq	%rdi, %rdi
	je	.L309
	movq	(%rdi), %rax
	movq	%r8, -336(%rbp)
	call	*8(%rax)
	movq	-336(%rbp), %r8
.L309:
	movq	%r8, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L307:
	movq	%r15, 288(%r12)
	jmp	.L306
	.p2align 4,,10
	.p2align 3
.L421:
	movq	264(%r12), %rsi
	jmp	.L300
	.p2align 4,,10
	.p2align 3
.L287:
	call	_ZN6icu_676number4impl6Padder4noneEv@PLT
	movq	%rax, 68(%r12)
	movl	%edx, 76(%r12)
	jmp	.L288
	.p2align 4,,10
	.p2align 3
.L428:
	leaq	-240(%rbp), %r15
	movq	%r15, %rdi
	call	_ZN6icu_676number9Precision7integerEv@PLT
	movq	-360(%rbp), %rdi
	movl	$2, %edx
	movq	%r15, %rsi
	call	_ZNK6icu_676number17FractionPrecision13withMinDigitsEi@PLT
	jmp	.L409
	.p2align 4,,10
	.p2align 3
.L289:
	movl	$1, %edi
	call	_ZN6icu_676number12IntegerWidth10zeroFillToEi@PLT
	movq	%rax, -284(%rbp)
	movl	%edx, -276(%rbp)
	movq	%rax, -240(%rbp)
	movl	%edx, -232(%rbp)
	movq	%rax, -208(%rbp)
	movl	%edx, -200(%rbp)
	movq	%rax, 80(%r12)
	movb	%dl, 88(%r12)
	jmp	.L291
	.p2align 4,,10
	.p2align 3
.L298:
	movq	%r15, %rdi
	call	*%rax
	jmp	.L297
	.p2align 4,,10
	.p2align 3
.L429:
	movq	256(%r12), %r8
	testq	%r8, %r8
	jne	.L311
	movq	-360(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6711PluralRules9forLocaleERKNS_6LocaleER10UErrorCode@PLT
	movq	256(%r12), %rdi
	movq	%rax, %r8
	testq	%rdi, %rdi
	je	.L313
	movq	%rax, -328(%rbp)
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	-328(%rbp), %r8
.L313:
	movq	%r8, 256(%r12)
	jmp	.L311
	.p2align 4,,10
	.p2align 3
.L430:
	movl	$1, %edi
	call	_ZN6icu_676number4impl7Grouper11forStrategyE23UNumberGroupingStrategy@PLT
	movq	%rax, 56(%r12)
	movl	%edx, 64(%r12)
	jmp	.L285
	.p2align 4,,10
	.p2align 3
.L426:
	movl	-296(%rbp), %ecx
	xorl	%r8d, %r8d
	testl	%ecx, %ecx
	jne	.L323
	xorl	%r8d, %r8d
	cmpl	$2, -288(%rbp)
	setne	%r8b
.L323:
	movq	200(%rbx), %r9
	testq	%r9, %r9
	je	.L431
.L324:
	movl	$1496, %edi
	movl	%r8d, -328(%rbp)
	movq	%r9, -296(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L274
	movzbl	-340(%rbp), %eax
	movl	8(%rbx), %esi
	pushq	%r13
	movq	%r15, %rdi
	pushq	-352(%rbp)
	movq	-296(%rbp), %r9
	movl	-328(%rbp), %r8d
	pushq	%rax
	movq	-312(%rbp), %rcx
	movq	-360(%rbp), %rdx
	pushq	%r14
	call	_ZN6icu_676number4impl14CompactHandlerC1E19UNumberCompactStyleRKNS_6LocaleEPKcNS1_11CompactTypeEPKNS_11PluralRulesEPNS1_22MutablePatternModifierEbPKNS1_19MicroPropsGeneratorER10UErrorCode@PLT
	movq	304(%r12), %rdi
	addq	$32, %rsp
	testq	%rdi, %rdi
	je	.L327
	movq	(%rdi), %rax
	call	*8(%rax)
.L327:
	movl	0(%r13), %edx
	movq	%r15, 304(%r12)
	testl	%edx, %edx
	jg	.L410
	movq	%r15, -352(%rbp)
	jmp	.L322
	.p2align 4,,10
	.p2align 3
.L328:
	movq	-352(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl22MutablePatternModifier10addToChainEPKNS1_19MicroPropsGeneratorE@PLT
	jmp	.L264
.L431:
	movq	256(%r12), %r9
	testq	%r9, %r9
	jne	.L324
	movq	-360(%rbp), %rdi
	movq	%r13, %rsi
	movl	%r8d, -296(%rbp)
	call	_ZN6icu_6711PluralRules9forLocaleERKNS_6LocaleER10UErrorCode@PLT
	movq	256(%r12), %rdi
	movl	-296(%rbp), %r8d
	movq	%rax, %r9
	testq	%rdi, %rdi
	je	.L326
	movq	%rax, -296(%rbp)
	movq	(%rdi), %rax
	movl	%r8d, -328(%rbp)
	call	*8(%rax)
	movl	-328(%rbp), %r8d
	movq	-296(%rbp), %r9
.L326:
	movq	%r9, 256(%r12)
	jmp	.L324
.L425:
	movq	200(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L432
.L317:
	movq	-360(%rbp), %rdi
	movq	-352(%rbp), %rcx
	movq	%r13, %r8
	movq	-304(%rbp), %rsi
	call	_ZN6icu_676number4impl15LongNameHandler20forCurrencyLongNamesERKNS_6LocaleERKNS_12CurrencyUnitEPKNS_11PluralRulesEPKNS1_19MicroPropsGeneratorER10UErrorCode@PLT
	movq	296(%r12), %rdi
	movq	%rax, -352(%rbp)
	testq	%rdi, %rdi
	jne	.L411
	jmp	.L320
.L308:
	movq	%r8, %rdi
	call	*%rax
	jmp	.L307
.L432:
	movq	256(%r12), %rdx
	testq	%rdx, %rdx
	jne	.L317
	movq	-360(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6711PluralRules9forLocaleERKNS_6LocaleER10UErrorCode@PLT
	movq	256(%r12), %rdi
	movq	%rax, %rdx
	testq	%rdi, %rdi
	je	.L319
	movq	%rax, -328(%rbp)
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	-328(%rbp), %rdx
.L319:
	movq	%rdx, 256(%r12)
	jmp	.L317
.L427:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4912:
	.size	_ZN6icu_676number4impl19NumberFormatterImpl22macrosToMicroGeneratorERKNS1_10MacroPropsEbR10UErrorCode.part.0, .-_ZN6icu_676number4impl19NumberFormatterImpl22macrosToMicroGeneratorERKNS1_10MacroPropsEbR10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl19NumberFormatterImpl22macrosToMicroGeneratorERKNS1_10MacroPropsEbR10UErrorCode
	.type	_ZN6icu_676number4impl19NumberFormatterImpl22macrosToMicroGeneratorERKNS1_10MacroPropsEbR10UErrorCode, @function
_ZN6icu_676number4impl19NumberFormatterImpl22macrosToMicroGeneratorERKNS1_10MacroPropsEbR10UErrorCode:
.LFB3482:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L434
	movzbl	%dl, %edx
	jmp	_ZN6icu_676number4impl19NumberFormatterImpl22macrosToMicroGeneratorERKNS1_10MacroPropsEbR10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L434:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3482:
	.size	_ZN6icu_676number4impl19NumberFormatterImpl22macrosToMicroGeneratorERKNS1_10MacroPropsEbR10UErrorCode, .-_ZN6icu_676number4impl19NumberFormatterImpl22macrosToMicroGeneratorERKNS1_10MacroPropsEbR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl19NumberFormatterImplC2ERKNS1_10MacroPropsEbR10UErrorCode
	.type	_ZN6icu_676number4impl19NumberFormatterImplC2ERKNS1_10MacroPropsEbR10UErrorCode, @function
_ZN6icu_676number4impl19NumberFormatterImplC2ERKNS1_10MacroPropsEbR10UErrorCode:
.LFB3480:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_676number4impl10MicroPropsE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%edx, %r12d
	movl	$-1, %edx
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	addq	$144, %rdi
	movq	%rax, -136(%rdi)
	movl	$-3, %eax
	movw	%ax, -88(%rdi)
	movw	%dx, -64(%rdi)
	movq	$0, -144(%rdi)
	movl	$0, -128(%rdi)
	movb	$1, -92(%rdi)
	movl	$-2, -76(%rdi)
	movb	$0, -56(%rdi)
	movq	$0, -16(%rdi)
	call	_ZN6icu_676number4impl18ScientificModifierC1Ev@PLT
	leaq	16+_ZTVN6icu_676number4impl13EmptyModifierE(%rip), %rax
	pxor	%xmm0, %xmm0
	movb	$0, 176(%rbx)
	movq	%rax, 168(%rbx)
	leaq	320(%rbx), %rdi
	movq	%rax, 184(%rbx)
	leaq	16+_ZTVN6icu_676number4impl23MultiplierFormatHandlerE(%rip), %rax
	movq	%rax, 200(%rbx)
	movb	$1, 192(%rbx)
	movl	$0, 208(%rbx)
	movq	$0, 216(%rbx)
	movl	$0, 224(%rbx)
	movb	$0, 240(%rbx)
	movups	%xmm0, 248(%rbx)
	movups	%xmm0, 264(%rbx)
	movups	%xmm0, 280(%rbx)
	movups	%xmm0, 296(%rbx)
	call	_ZN6icu_6712CurrencyUnitC1Ev@PLT
	xorl	%ecx, %ecx
	movl	$2, %esi
	leaq	365(%rbx), %rax
	movw	%cx, 364(%rbx)
	movl	0(%r13), %r8d
	movl	$2, %edi
	movq	%rax, 352(%rbx)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, 416(%rbx)
	movq	%rax, 480(%rbx)
	xorl	%eax, %eax
	movl	$0, 408(%rbx)
	movl	$40, 360(%rbx)
	movw	%si, 424(%rbx)
	movw	%di, 488(%rbx)
	testl	%r8d, %r8d
	jg	.L436
	movzbl	%r12b, %edx
	movq	%r13, %rcx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_676number4impl19NumberFormatterImpl22macrosToMicroGeneratorERKNS1_10MacroPropsEbR10UErrorCode.part.0
.L436:
	movq	%rax, (%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3480:
	.size	_ZN6icu_676number4impl19NumberFormatterImplC2ERKNS1_10MacroPropsEbR10UErrorCode, .-_ZN6icu_676number4impl19NumberFormatterImplC2ERKNS1_10MacroPropsEbR10UErrorCode
	.globl	_ZN6icu_676number4impl19NumberFormatterImplC1ERKNS1_10MacroPropsEbR10UErrorCode
	.set	_ZN6icu_676number4impl19NumberFormatterImplC1ERKNS1_10MacroPropsEbR10UErrorCode,_ZN6icu_676number4impl19NumberFormatterImplC2ERKNS1_10MacroPropsEbR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl19NumberFormatterImplC2ERKNS1_10MacroPropsER10UErrorCode
	.type	_ZN6icu_676number4impl19NumberFormatterImplC2ERKNS1_10MacroPropsER10UErrorCode, @function
_ZN6icu_676number4impl19NumberFormatterImplC2ERKNS1_10MacroPropsER10UErrorCode:
.LFB3449:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_676number4impl10MicroPropsE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	movl	$-1, %edx
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	addq	$144, %rdi
	subq	$8, %rsp
	movq	%rax, -136(%rdi)
	movl	$-3, %eax
	movw	%ax, -88(%rdi)
	movw	%dx, -64(%rdi)
	movq	$0, -144(%rdi)
	movl	$0, -128(%rdi)
	movb	$1, -92(%rdi)
	movl	$-2, -76(%rdi)
	movb	$0, -56(%rdi)
	movq	$0, -16(%rdi)
	call	_ZN6icu_676number4impl18ScientificModifierC1Ev@PLT
	leaq	16+_ZTVN6icu_676number4impl13EmptyModifierE(%rip), %rax
	pxor	%xmm0, %xmm0
	movb	$0, 176(%rbx)
	movq	%rax, 168(%rbx)
	leaq	320(%rbx), %rdi
	movq	%rax, 184(%rbx)
	leaq	16+_ZTVN6icu_676number4impl23MultiplierFormatHandlerE(%rip), %rax
	movq	%rax, 200(%rbx)
	movb	$1, 192(%rbx)
	movl	$0, 208(%rbx)
	movq	$0, 216(%rbx)
	movl	$0, 224(%rbx)
	movb	$0, 240(%rbx)
	movups	%xmm0, 248(%rbx)
	movups	%xmm0, 264(%rbx)
	movups	%xmm0, 280(%rbx)
	movups	%xmm0, 296(%rbx)
	call	_ZN6icu_6712CurrencyUnitC1Ev@PLT
	xorl	%ecx, %ecx
	movl	$2, %esi
	leaq	365(%rbx), %rax
	movw	%cx, 364(%rbx)
	movl	(%r12), %r8d
	movl	$2, %edi
	movq	%rax, 352(%rbx)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, 416(%rbx)
	movq	%rax, 480(%rbx)
	xorl	%eax, %eax
	movl	$0, 408(%rbx)
	movl	$40, 360(%rbx)
	movw	%si, 424(%rbx)
	movw	%di, 488(%rbx)
	testl	%r8d, %r8d
	jg	.L441
	movq	%r12, %rcx
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_676number4impl19NumberFormatterImpl22macrosToMicroGeneratorERKNS1_10MacroPropsEbR10UErrorCode.part.0
.L441:
	movq	%rax, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3449:
	.size	_ZN6icu_676number4impl19NumberFormatterImplC2ERKNS1_10MacroPropsER10UErrorCode, .-_ZN6icu_676number4impl19NumberFormatterImplC2ERKNS1_10MacroPropsER10UErrorCode
	.globl	_ZN6icu_676number4impl19NumberFormatterImplC1ERKNS1_10MacroPropsER10UErrorCode
	.set	_ZN6icu_676number4impl19NumberFormatterImplC1ERKNS1_10MacroPropsER10UErrorCode,_ZN6icu_676number4impl19NumberFormatterImplC2ERKNS1_10MacroPropsER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl19NumberFormatterImpl12formatStaticERKNS1_10MacroPropsERNS1_15DecimalQuantityERNS_22FormattedStringBuilderER10UErrorCode
	.type	_ZN6icu_676number4impl19NumberFormatterImpl12formatStaticERKNS1_10MacroPropsERNS1_15DecimalQuantityERNS_22FormattedStringBuilderER10UErrorCode, @function
_ZN6icu_676number4impl19NumberFormatterImpl12formatStaticERKNS1_10MacroPropsERNS1_15DecimalQuantityERNS_22FormattedStringBuilderER10UErrorCode:
.LFB3451:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	movl	$-3, %ecx
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-464(%rbp), %r14
	leaq	-608(%rbp), %r10
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-288(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movl	$-1, %esi
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	16+_ZTVN6icu_676number4impl13EmptyModifierE(%rip), %rbx
	subq	$600, %rsp
	movq	%rdi, -624(%rbp)
	movq	%r14, %rdi
	movq	%rdx, -632(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_676number4impl10MicroPropsE(%rip), %rax
	movq	%r10, -616(%rbp)
	movq	%rax, -600(%rbp)
	movw	%cx, -552(%rbp)
	movw	%si, -528(%rbp)
	movq	$0, -608(%rbp)
	movl	$0, -592(%rbp)
	movb	$1, -556(%rbp)
	movl	$-2, -540(%rbp)
	movb	$0, -520(%rbp)
	movq	$0, -480(%rbp)
	call	_ZN6icu_676number4impl18ScientificModifierC1Ev@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rdi
	leaq	16+_ZTVN6icu_676number4impl23MultiplierFormatHandlerE(%rip), %rax
	movq	%rax, -408(%rbp)
	movq	%rbx, -440(%rbp)
	movb	$0, -432(%rbp)
	movq	%rbx, -424(%rbp)
	movb	$1, -416(%rbp)
	movl	$0, -400(%rbp)
	movq	$0, -392(%rbp)
	movl	$0, -384(%rbp)
	movb	$0, -368(%rbp)
	movups	%xmm0, -360(%rbp)
	movups	%xmm0, -344(%rbp)
	movups	%xmm0, -328(%rbp)
	movups	%xmm0, -312(%rbp)
	call	_ZN6icu_6712CurrencyUnitC1Ev@PLT
	movl	(%r15), %r10d
	xorl	%edi, %edi
	movl	$2, %r9d
	leaq	-243(%rbp), %rax
	movl	$2, %r8d
	movw	%r9w, -120(%rbp)
	movq	-624(%rbp), %r9
	movq	%rax, -256(%rbp)
	testl	%r10d, %r10d
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	-616(%rbp), %r10
	movl	$0, -200(%rbp)
	movl	$40, -248(%rbp)
	movw	%di, -244(%rbp)
	movq	%rax, -192(%rbp)
	movw	%r8w, -184(%rbp)
	movq	%rax, -128(%rbp)
	jg	.L494
	xorl	%edx, %edx
	movq	%r10, %rdi
	movq	%r15, %rcx
	movq	%r9, %rsi
	call	_ZN6icu_676number4impl19NumberFormatterImpl22macrosToMicroGeneratorERKNS1_10MacroPropsEbR10UErrorCode.part.0
	movl	(%r15), %edx
	movq	%rax, -608(%rbp)
	movq	%rax, %rdi
	testl	%edx, %edx
	jg	.L447
	testq	%rax, %rax
	je	.L495
	movq	(%rax), %rax
	leaq	-600(%rbp), %r9
	movq	%r15, %rcx
	movq	%r12, %rsi
	movq	%r9, %rdx
	movq	%r9, -616(%rbp)
	call	*16(%rax)
	leaq	-528(%rbp), %rdi
	movq	%r15, %rdx
	movq	%r12, %rsi
	call	_ZNK6icu_676number12IntegerWidth5applyERNS0_4impl15DecimalQuantityER10UErrorCode@PLT
	movl	(%r15), %eax
	testl	%eax, %eax
	jg	.L447
	movq	-616(%rbp), %r9
	movq	%r12, %rsi
	movq	%r15, %r8
	xorl	%ecx, %ecx
	movq	-632(%rbp), %rdx
	movq	%r9, %rdi
	call	_ZN6icu_676number4impl19NumberFormatterImpl11writeNumberERKNS1_10MicroPropsERNS1_15DecimalQuantityERNS_22FormattedStringBuilderEiR10UErrorCode
	movq	-616(%rbp), %r9
	movq	-632(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r15, %r8
	movl	%eax, %ecx
	movl	%eax, %r12d
	movq	%r9, %rdi
	call	_ZN6icu_676number4impl19NumberFormatterImpl12writeAffixesERKNS1_10MicroPropsERNS_22FormattedStringBuilderEiiR10UErrorCode
	addl	%eax, %r12d
	jmp	.L463
	.p2align 4,,10
	.p2align 3
.L494:
	movq	$0, -608(%rbp)
.L447:
	xorl	%r12d, %r12d
.L463:
	leaq	-128(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	-192(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, -244(%rbp)
	jne	.L496
.L449:
	movq	%r13, %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	movq	-304(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L450
	movq	(%rdi), %rax
	call	*8(%rax)
.L450:
	movq	-312(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L451
	movq	(%rdi), %rax
	call	*8(%rax)
.L451:
	movq	-320(%rbp), %r13
	testq	%r13, %r13
	je	.L452
	movq	0(%r13), %rax
	leaq	_ZN6icu_676number4impl24ImmutablePatternModifierD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L453
	movq	8(%r13), %rdi
	leaq	16+_ZTVN6icu_676number4impl24ImmutablePatternModifierE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%rdi, %rdi
	je	.L454
	movq	(%rdi), %rax
	call	*8(%rax)
.L454:
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L452:
	movq	-328(%rbp), %r13
	testq	%r13, %r13
	je	.L455
	movq	0(%r13), %rax
	leaq	_ZN6icu_676number4impl22MutablePatternModifierD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L456
	leaq	120+_ZTVN6icu_676number4impl22MutablePatternModifierE(%rip), %rax
	leaq	328(%r13), %rdi
	leaq	-104(%rax), %rcx
	movq	%rax, %xmm1
	addq	$88, %rax
	movq	%rcx, %xmm0
	movq	%rax, 16(%r13)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 0(%r13)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	240(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	176(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, 124(%r13)
	jne	.L497
.L457:
	leaq	80(%r13), %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	leaq	16(%r13), %rdi
	call	_ZN6icu_676number4impl14SymbolProviderD2Ev@PLT
	leaq	8(%r13), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L455:
	movq	-336(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L458
	movq	(%rdi), %rax
	call	*8(%rax)
.L458:
	movq	-344(%rbp), %r13
	testq	%r13, %r13
	je	.L459
	movq	0(%r13), %rax
	leaq	_ZN6icu_676number4impl17ParsedPatternInfoD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L460
	leaq	16+_ZTVN6icu_676number4impl17ParsedPatternInfoE(%rip), %rax
	leaq	296(%r13), %rdi
	movq	%rax, 0(%r13)
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	leaq	128(%r13), %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	leaq	8(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl20AffixPatternProviderD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L459:
	movq	-352(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L461
	movq	(%rdi), %rax
	call	*8(%rax)
.L461:
	movq	-360(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L462
	movq	(%rdi), %rax
	call	*8(%rax)
.L462:
	leaq	16+_ZTVN6icu_676number4impl10MicroPropsE(%rip), %rax
	leaq	-400(%rbp), %rdi
	movq	%rax, -600(%rbp)
	leaq	16+_ZTVN6icu_676number4impl23MultiplierFormatHandlerE(%rip), %rax
	movq	%rax, -408(%rbp)
	call	_ZN6icu_676number5ScaleD1Ev@PLT
	leaq	-424(%rbp), %rdi
	movq	%rbx, -424(%rbp)
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	leaq	-440(%rbp), %rdi
	movq	%rbx, -440(%rbp)
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	leaq	16+_ZTVN6icu_676number4impl18ScientificModifierE(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -464(%rbp)
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L498
	addq	$600, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L496:
	.cfi_restore_state
	movq	-256(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L449
	.p2align 4,,10
	.p2align 3
.L497:
	movq	112(%r13), %rdi
	call	uprv_free_67@PLT
	jmp	.L457
	.p2align 4,,10
	.p2align 3
.L460:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L459
	.p2align 4,,10
	.p2align 3
.L453:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L452
	.p2align 4,,10
	.p2align 3
.L456:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L455
	.p2align 4,,10
	.p2align 3
.L495:
	movl	$5, (%r15)
	jmp	.L447
.L498:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3451:
	.size	_ZN6icu_676number4impl19NumberFormatterImpl12formatStaticERKNS1_10MacroPropsERNS1_15DecimalQuantityERNS_22FormattedStringBuilderER10UErrorCode, .-_ZN6icu_676number4impl19NumberFormatterImpl12formatStaticERKNS1_10MacroPropsERNS1_15DecimalQuantityERNS_22FormattedStringBuilderER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl19NumberFormatterImpl21getPrefixSuffixStaticERKNS1_10MacroPropsENS1_6SignumENS_14StandardPlural4FormERNS_22FormattedStringBuilderER10UErrorCode
	.type	_ZN6icu_676number4impl19NumberFormatterImpl21getPrefixSuffixStaticERKNS1_10MacroPropsENS1_6SignumENS_14StandardPlural4FormERNS_22FormattedStringBuilderER10UErrorCode, @function
_ZN6icu_676number4impl19NumberFormatterImpl21getPrefixSuffixStaticERKNS1_10MacroPropsENS1_6SignumENS_14StandardPlural4FormERNS_22FormattedStringBuilderER10UErrorCode:
.LFB3452:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	16+_ZTVN6icu_676number4impl10MicroPropsE(%rip), %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-464(%rbp), %r14
	leaq	-608(%rbp), %r10
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-288(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	16+_ZTVN6icu_676number4impl13EmptyModifierE(%rip), %rbx
	subq	$600, %rsp
	movq	%rdi, -624(%rbp)
	movq	%r14, %rdi
	movl	%esi, -628(%rbp)
	movl	$-1, %esi
	movl	%edx, -632(%rbp)
	movq	%rcx, -640(%rbp)
	movl	$-3, %ecx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r10, -616(%rbp)
	movw	%cx, -552(%rbp)
	movw	%si, -528(%rbp)
	movq	$0, -608(%rbp)
	movq	%r15, -600(%rbp)
	movl	$0, -592(%rbp)
	movb	$1, -556(%rbp)
	movl	$-2, -540(%rbp)
	movb	$0, -520(%rbp)
	movq	$0, -480(%rbp)
	call	_ZN6icu_676number4impl18ScientificModifierC1Ev@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rdi
	leaq	16+_ZTVN6icu_676number4impl23MultiplierFormatHandlerE(%rip), %rax
	movq	%rax, -408(%rbp)
	movq	%rbx, -440(%rbp)
	movb	$0, -432(%rbp)
	movq	%rbx, -424(%rbp)
	movb	$1, -416(%rbp)
	movl	$0, -400(%rbp)
	movq	$0, -392(%rbp)
	movl	$0, -384(%rbp)
	movb	$0, -368(%rbp)
	movups	%xmm0, -360(%rbp)
	movups	%xmm0, -344(%rbp)
	movups	%xmm0, -328(%rbp)
	movups	%xmm0, -312(%rbp)
	call	_ZN6icu_6712CurrencyUnitC1Ev@PLT
	leaq	-243(%rbp), %rax
	movl	(%r12), %r10d
	xorl	%edi, %edi
	movq	%rax, -256(%rbp)
	movl	$2, %r8d
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %r9d
	movl	$0, -200(%rbp)
	movl	$40, -248(%rbp)
	movw	%di, -244(%rbp)
	movq	%rax, -192(%rbp)
	movw	%r8w, -184(%rbp)
	movq	%rax, -128(%rbp)
	movw	%r9w, -120(%rbp)
	testl	%r10d, %r10d
	jg	.L500
	movq	-624(%rbp), %r9
	movq	-616(%rbp), %r10
	xorl	%edx, %edx
	movq	%r12, %rcx
	movq	%r9, %rsi
	movq	%r10, %rdi
	call	_ZN6icu_676number4impl19NumberFormatterImpl22macrosToMicroGeneratorERKNS1_10MacroPropsEbR10UErrorCode.part.0
	movl	(%r12), %edx
	movq	%rax, -608(%rbp)
	testl	%edx, %edx
	jg	.L503
	movl	-632(%rbp), %edx
	movl	-628(%rbp), %esi
	movq	-328(%rbp), %rdi
	call	_ZN6icu_676number4impl22MutablePatternModifier19setNumberPropertiesENS1_6SignumENS_14StandardPlural4FormE@PLT
	movq	-328(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	-640(%rbp), %rsi
	movq	%r12, %r8
	movq	(%rdi), %rax
	call	*24(%rax)
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L503
	movq	-328(%rbp), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movl	%eax, %r12d
	jmp	.L502
	.p2align 4,,10
	.p2align 3
.L500:
	movq	$0, -608(%rbp)
.L503:
	xorl	%r12d, %r12d
.L502:
	leaq	-128(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	-192(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, -244(%rbp)
	jne	.L548
.L504:
	movq	%r13, %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	movq	-304(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L505
	movq	(%rdi), %rax
	call	*8(%rax)
.L505:
	movq	-312(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L506
	movq	(%rdi), %rax
	call	*8(%rax)
.L506:
	movq	-320(%rbp), %r13
	testq	%r13, %r13
	je	.L507
	movq	0(%r13), %rax
	leaq	_ZN6icu_676number4impl24ImmutablePatternModifierD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L508
	movq	8(%r13), %rdi
	leaq	16+_ZTVN6icu_676number4impl24ImmutablePatternModifierE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%rdi, %rdi
	je	.L509
	movq	(%rdi), %rax
	call	*8(%rax)
.L509:
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L507:
	movq	-328(%rbp), %r13
	testq	%r13, %r13
	je	.L510
	movq	0(%r13), %rax
	leaq	_ZN6icu_676number4impl22MutablePatternModifierD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L511
	leaq	120+_ZTVN6icu_676number4impl22MutablePatternModifierE(%rip), %rax
	leaq	328(%r13), %rdi
	leaq	-104(%rax), %rcx
	movq	%rax, %xmm1
	addq	$88, %rax
	movq	%rcx, %xmm0
	movq	%rax, 16(%r13)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 0(%r13)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	240(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	176(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, 124(%r13)
	jne	.L549
.L512:
	leaq	80(%r13), %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	leaq	16(%r13), %rdi
	call	_ZN6icu_676number4impl14SymbolProviderD2Ev@PLT
	leaq	8(%r13), %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L510:
	movq	-336(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L513
	movq	(%rdi), %rax
	call	*8(%rax)
.L513:
	movq	-344(%rbp), %r13
	testq	%r13, %r13
	je	.L514
	movq	0(%r13), %rax
	leaq	_ZN6icu_676number4impl17ParsedPatternInfoD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L515
	leaq	16+_ZTVN6icu_676number4impl17ParsedPatternInfoE(%rip), %rax
	leaq	296(%r13), %rdi
	movq	%rax, 0(%r13)
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	leaq	128(%r13), %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	leaq	8(%r13), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl20AffixPatternProviderD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L514:
	movq	-352(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L516
	movq	(%rdi), %rax
	call	*8(%rax)
.L516:
	movq	-360(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L517
	movq	(%rdi), %rax
	call	*8(%rax)
.L517:
	leaq	16+_ZTVN6icu_676number4impl23MultiplierFormatHandlerE(%rip), %rax
	leaq	-400(%rbp), %rdi
	movq	%r15, -600(%rbp)
	movq	%rax, -408(%rbp)
	call	_ZN6icu_676number5ScaleD1Ev@PLT
	leaq	-424(%rbp), %rdi
	movq	%rbx, -424(%rbp)
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	leaq	-440(%rbp), %rdi
	movq	%rbx, -440(%rbp)
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	leaq	16+_ZTVN6icu_676number4impl18ScientificModifierE(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -464(%rbp)
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L550
	addq	$600, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L548:
	.cfi_restore_state
	movq	-256(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L504
	.p2align 4,,10
	.p2align 3
.L549:
	movq	112(%r13), %rdi
	call	uprv_free_67@PLT
	jmp	.L512
	.p2align 4,,10
	.p2align 3
.L515:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L514
	.p2align 4,,10
	.p2align 3
.L511:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L510
	.p2align 4,,10
	.p2align 3
.L508:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L507
.L550:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3452:
	.size	_ZN6icu_676number4impl19NumberFormatterImpl21getPrefixSuffixStaticERKNS1_10MacroPropsENS1_6SignumENS_14StandardPlural4FormERNS_22FormattedStringBuilderER10UErrorCode, .-_ZN6icu_676number4impl19NumberFormatterImpl21getPrefixSuffixStaticERKNS1_10MacroPropsENS1_6SignumENS_14StandardPlural4FormERNS_22FormattedStringBuilderER10UErrorCode
	.weak	_ZTSN6icu_677UMemoryE
	.section	.rodata._ZTSN6icu_677UMemoryE,"aG",@progbits,_ZTSN6icu_677UMemoryE,comdat
	.align 16
	.type	_ZTSN6icu_677UMemoryE, @object
	.size	_ZTSN6icu_677UMemoryE, 18
_ZTSN6icu_677UMemoryE:
	.string	"N6icu_677UMemoryE"
	.weak	_ZTIN6icu_677UMemoryE
	.section	.data.rel.ro._ZTIN6icu_677UMemoryE,"awG",@progbits,_ZTIN6icu_677UMemoryE,comdat
	.align 8
	.type	_ZTIN6icu_677UMemoryE, @object
	.size	_ZTIN6icu_677UMemoryE, 16
_ZTIN6icu_677UMemoryE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN6icu_677UMemoryE
	.weak	_ZTSN6icu_676number4impl19MicroPropsGeneratorE
	.section	.rodata._ZTSN6icu_676number4impl19MicroPropsGeneratorE,"aG",@progbits,_ZTSN6icu_676number4impl19MicroPropsGeneratorE,comdat
	.align 32
	.type	_ZTSN6icu_676number4impl19MicroPropsGeneratorE, @object
	.size	_ZTSN6icu_676number4impl19MicroPropsGeneratorE, 43
_ZTSN6icu_676number4impl19MicroPropsGeneratorE:
	.string	"N6icu_676number4impl19MicroPropsGeneratorE"
	.weak	_ZTIN6icu_676number4impl19MicroPropsGeneratorE
	.section	.data.rel.ro._ZTIN6icu_676number4impl19MicroPropsGeneratorE,"awG",@progbits,_ZTIN6icu_676number4impl19MicroPropsGeneratorE,comdat
	.align 8
	.type	_ZTIN6icu_676number4impl19MicroPropsGeneratorE, @object
	.size	_ZTIN6icu_676number4impl19MicroPropsGeneratorE, 16
_ZTIN6icu_676number4impl19MicroPropsGeneratorE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN6icu_676number4impl19MicroPropsGeneratorE
	.weak	_ZTSN6icu_676number4impl13EmptyModifierE
	.section	.rodata._ZTSN6icu_676number4impl13EmptyModifierE,"aG",@progbits,_ZTSN6icu_676number4impl13EmptyModifierE,comdat
	.align 32
	.type	_ZTSN6icu_676number4impl13EmptyModifierE, @object
	.size	_ZTSN6icu_676number4impl13EmptyModifierE, 37
_ZTSN6icu_676number4impl13EmptyModifierE:
	.string	"N6icu_676number4impl13EmptyModifierE"
	.weak	_ZTIN6icu_676number4impl13EmptyModifierE
	.section	.data.rel.ro._ZTIN6icu_676number4impl13EmptyModifierE,"awG",@progbits,_ZTIN6icu_676number4impl13EmptyModifierE,comdat
	.align 8
	.type	_ZTIN6icu_676number4impl13EmptyModifierE, @object
	.size	_ZTIN6icu_676number4impl13EmptyModifierE, 56
_ZTIN6icu_676number4impl13EmptyModifierE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN6icu_676number4impl13EmptyModifierE
	.long	0
	.long	2
	.quad	_ZTIN6icu_676number4impl8ModifierE
	.quad	2
	.quad	_ZTIN6icu_677UMemoryE
	.quad	2
	.weak	_ZTSN6icu_676number4impl10MicroPropsE
	.section	.rodata._ZTSN6icu_676number4impl10MicroPropsE,"aG",@progbits,_ZTSN6icu_676number4impl10MicroPropsE,comdat
	.align 32
	.type	_ZTSN6icu_676number4impl10MicroPropsE, @object
	.size	_ZTSN6icu_676number4impl10MicroPropsE, 34
_ZTSN6icu_676number4impl10MicroPropsE:
	.string	"N6icu_676number4impl10MicroPropsE"
	.weak	_ZTIN6icu_676number4impl10MicroPropsE
	.section	.data.rel.ro._ZTIN6icu_676number4impl10MicroPropsE,"awG",@progbits,_ZTIN6icu_676number4impl10MicroPropsE,comdat
	.align 8
	.type	_ZTIN6icu_676number4impl10MicroPropsE, @object
	.size	_ZTIN6icu_676number4impl10MicroPropsE, 24
_ZTIN6icu_676number4impl10MicroPropsE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_676number4impl10MicroPropsE
	.quad	_ZTIN6icu_676number4impl19MicroPropsGeneratorE
	.weak	_ZTVN6icu_676number4impl13EmptyModifierE
	.section	.data.rel.ro.local._ZTVN6icu_676number4impl13EmptyModifierE,"awG",@progbits,_ZTVN6icu_676number4impl13EmptyModifierE,comdat
	.align 8
	.type	_ZTVN6icu_676number4impl13EmptyModifierE, @object
	.size	_ZTVN6icu_676number4impl13EmptyModifierE, 88
_ZTVN6icu_676number4impl13EmptyModifierE:
	.quad	0
	.quad	_ZTIN6icu_676number4impl13EmptyModifierE
	.quad	_ZN6icu_676number4impl13EmptyModifierD1Ev
	.quad	_ZN6icu_676number4impl13EmptyModifierD0Ev
	.quad	_ZNK6icu_676number4impl13EmptyModifier5applyERNS_22FormattedStringBuilderEiiR10UErrorCode
	.quad	_ZNK6icu_676number4impl13EmptyModifier15getPrefixLengthEv
	.quad	_ZNK6icu_676number4impl13EmptyModifier17getCodePointCountEv
	.quad	_ZNK6icu_676number4impl13EmptyModifier8isStrongEv
	.quad	_ZNK6icu_676number4impl13EmptyModifier13containsFieldENS_22FormattedStringBuilder5FieldE
	.quad	_ZNK6icu_676number4impl13EmptyModifier13getParametersERNS1_8Modifier10ParametersE
	.quad	_ZNK6icu_676number4impl13EmptyModifier22semanticallyEquivalentERKNS1_8ModifierE
	.weak	_ZTVN6icu_676number4impl10MicroPropsE
	.section	.data.rel.ro.local._ZTVN6icu_676number4impl10MicroPropsE,"awG",@progbits,_ZTVN6icu_676number4impl10MicroPropsE,comdat
	.align 8
	.type	_ZTVN6icu_676number4impl10MicroPropsE, @object
	.size	_ZTVN6icu_676number4impl10MicroPropsE, 40
_ZTVN6icu_676number4impl10MicroPropsE:
	.quad	0
	.quad	_ZTIN6icu_676number4impl10MicroPropsE
	.quad	_ZN6icu_676number4impl10MicroPropsD1Ev
	.quad	_ZN6icu_676number4impl10MicroPropsD0Ev
	.quad	_ZNK6icu_676number4impl10MicroProps15processQuantityERNS1_15DecimalQuantityERS2_R10UErrorCode
	.weak	_ZTVN6icu_676number4impl19MicroPropsGeneratorE
	.section	.data.rel.ro._ZTVN6icu_676number4impl19MicroPropsGeneratorE,"awG",@progbits,_ZTVN6icu_676number4impl19MicroPropsGeneratorE,comdat
	.align 8
	.type	_ZTVN6icu_676number4impl19MicroPropsGeneratorE, @object
	.size	_ZTVN6icu_676number4impl19MicroPropsGeneratorE, 40
_ZTVN6icu_676number4impl19MicroPropsGeneratorE:
	.quad	0
	.quad	_ZTIN6icu_676number4impl19MicroPropsGeneratorE
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.section	.rodata
	.type	_ZN6icu_67L15kUndefinedFieldE, @object
	.size	_ZN6icu_67L15kUndefinedFieldE, 1
_ZN6icu_67L15kUndefinedFieldE:
	.zero	1
	.section	.data.rel.ro,"aw"
	.align 8
.LC6:
	.quad	_ZTVN6icu_676number4impl17ParsedPatternInfoE+16
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
