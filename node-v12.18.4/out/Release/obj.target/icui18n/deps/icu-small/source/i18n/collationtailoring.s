	.file	"collationtailoring.cpp"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CollationTailoringC2EPKNS_17CollationSettingsE
	.type	_ZN6icu_6718CollationTailoringC2EPKNS_17CollationSettingsE, @function
_ZN6icu_6718CollationTailoringC2EPKNS_17CollationSettingsE:
.LFB3322:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6718CollationTailoringE(%rip), %rax
	pxor	%xmm0, %xmm0
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	40(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	addq	$104, %rdi
	subq	$8, %rsp
	movq	%rax, -104(%rdi)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, -64(%rdi)
	movl	$2, %eax
	movq	%rsi, -72(%rdi)
	leaq	.LC0(%rip), %rsi
	movq	$0, -96(%rdi)
	movw	%ax, -56(%rdi)
	movups	%xmm0, -88(%rdi)
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	pxor	%xmm0, %xmm0
	movq	$0, 384(%rbx)
	movups	%xmm0, 336(%rbx)
	movups	%xmm0, 352(%rbx)
	movups	%xmm0, 368(%rbx)
	testq	%r12, %r12
	je	.L2
	movq	32(%rbx), %rdi
.L3:
	testq	%rdi, %rdi
	je	.L5
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
.L5:
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString19getTerminatedBufferEv@PLT
	movl	$0, 328(%rbx)
	movl	$0, 392(%rbx)
	mfence
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	movl	$856, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L4
	leaq	16+_ZTVN6icu_6717CollationSettingsE(%rip), %rax
	movq	$0, 8(%rdi)
	movq	%rax, (%rdi)
	movq	$0, 16(%rdi)
	movq	$8208, 24(%rdi)
	movq	$0, 32(%rdi)
	movl	$0, 40(%rdi)
	movq	$0, 48(%rdi)
	movl	$0, 56(%rdi)
	movq	$0, 64(%rdi)
	movq	$0, 72(%rdi)
	movl	$-1, 80(%rdi)
.L4:
	movq	%rdi, 32(%rbx)
	jmp	.L3
	.cfi_endproc
.LFE3322:
	.size	_ZN6icu_6718CollationTailoringC2EPKNS_17CollationSettingsE, .-_ZN6icu_6718CollationTailoringC2EPKNS_17CollationSettingsE
	.globl	_ZN6icu_6718CollationTailoringC1EPKNS_17CollationSettingsE
	.set	_ZN6icu_6718CollationTailoringC1EPKNS_17CollationSettingsE,_ZN6icu_6718CollationTailoringC2EPKNS_17CollationSettingsE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CollationTailoring15ensureOwnedDataER10UErrorCode
	.type	_ZN6icu_6718CollationTailoring15ensureOwnedDataER10UErrorCode, @function
_ZN6icu_6718CollationTailoring15ensureOwnedDataER10UErrorCode:
.LFB3328:
	.cfi_startproc
	endbr64
	movl	(%rsi), %edx
	testl	%edx, %edx
	jg	.L24
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$16, %rsp
	movq	336(%rdi), %rax
	testq	%rax, %rax
	je	.L25
.L17:
	movq	%rax, 24(%r12)
	addq	$16, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	%rsi, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN6icu_6718Normalizer2Factory10getNFCImplER10UErrorCode@PLT
	movq	-24(%rbp), %rsi
	movq	%rax, %r13
	movl	(%rsi), %eax
	testl	%eax, %eax
	jle	.L26
	xorl	%eax, %eax
.L27:
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	movl	$144, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	-24(%rbp), %rsi
	testq	%rax, %rax
	je	.L19
	pxor	%xmm0, %xmm0
	movq	%r13, 48(%rax)
	movq	$301989888, 56(%rax)
	movq	$0, 64(%rax)
	movq	$0, 88(%rax)
	movq	$0, 96(%rax)
	movl	$0, 120(%rax)
	movq	$0, 128(%rax)
	movl	$0, 136(%rax)
	movups	%xmm0, (%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 72(%rax)
	movups	%xmm0, 104(%rax)
	movq	%rax, 336(%r12)
	jmp	.L17
.L19:
	movq	$0, 336(%r12)
	xorl	%eax, %eax
	movl	$7, (%rsi)
	jmp	.L27
	.cfi_endproc
.LFE3328:
	.size	_ZN6icu_6718CollationTailoring15ensureOwnedDataER10UErrorCode, .-_ZN6icu_6718CollationTailoring15ensureOwnedDataER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CollationTailoring15makeBaseVersionEPKhPh
	.type	_ZN6icu_6718CollationTailoring15makeBaseVersionEPKhPh, @function
_ZN6icu_6718CollationTailoring15makeBaseVersionEPKhPh:
.LFB3329:
	.cfi_startproc
	endbr64
	movb	$9, (%rsi)
	movzbl	(%rdi), %eax
	movzbl	1(%rdi), %edx
	leal	(%rdx,%rax,8), %eax
	movb	%al, 1(%rsi)
	movzbl	2(%rdi), %eax
	movb	$0, 3(%rsi)
	sall	$6, %eax
	movb	%al, 2(%rsi)
	ret
	.cfi_endproc
.LFE3329:
	.size	_ZN6icu_6718CollationTailoring15makeBaseVersionEPKhPh, .-_ZN6icu_6718CollationTailoring15makeBaseVersionEPKhPh
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CollationTailoring10setVersionEPKhS2_
	.type	_ZN6icu_6718CollationTailoring10setVersionEPKhS2_, @function
_ZN6icu_6718CollationTailoring10setVersionEPKhS2_:
.LFB3330:
	.cfi_startproc
	endbr64
	movb	$9, 328(%rdi)
	movzbl	1(%rsi), %eax
	movb	%al, 329(%rdi)
	movzbl	(%rdx), %ecx
	movl	%ecx, %eax
	shrb	$6, %al
	addl	%ecx, %eax
	movzbl	2(%rsi), %ecx
	andl	$63, %eax
	andl	$-64, %ecx
	orl	%ecx, %eax
	movb	%al, 330(%rdi)
	movzbl	3(%rdx), %eax
	movzbl	1(%rdx), %ecx
	movl	%eax, %esi
	shrb	$4, %al
	sall	$4, %esi
	leal	(%rsi,%rcx,8), %esi
	addb	2(%rdx), %sil
	shrb	$5, %cl
	movl	%esi, %edx
	addl	%edx, %ecx
	addl	%ecx, %eax
	movb	%al, 331(%rdi)
	ret
	.cfi_endproc
.LFE3330:
	.size	_ZN6icu_6718CollationTailoring10setVersionEPKhS2_, .-_ZN6icu_6718CollationTailoring10setVersionEPKhS2_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6718CollationTailoring13getUCAVersionEv
	.type	_ZNK6icu_6718CollationTailoring13getUCAVersionEv, @function
_ZNK6icu_6718CollationTailoring13getUCAVersionEv:
.LFB3331:
	.cfi_startproc
	endbr64
	movzbl	329(%rdi), %eax
	movzbl	330(%rdi), %edx
	sall	$4, %eax
	sarl	$6, %edx
	orl	%edx, %eax
	ret
	.cfi_endproc
.LFE3331:
	.size	_ZNK6icu_6718CollationTailoring13getUCAVersionEv, .-_ZNK6icu_6718CollationTailoring13getUCAVersionEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CollationTailoringD2Ev
	.type	_ZN6icu_6718CollationTailoringD2Ev, @function
_ZN6icu_6718CollationTailoringD2Ev:
.LFB3325:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6718CollationTailoringE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	32(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L32
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	movq	$0, 32(%r12)
.L32:
	movq	336(%r12), %rdi
	testq	%rdi, %rdi
	je	.L33
	call	_ZN6icu_677UMemorydlEPv@PLT
.L33:
	movq	344(%r12), %rdi
	testq	%rdi, %rdi
	je	.L34
	movq	(%rdi), %rax
	call	*8(%rax)
.L34:
	movq	352(%r12), %rdi
	call	udata_close_67@PLT
	movq	360(%r12), %rdi
	call	ures_close_67@PLT
	movq	368(%r12), %rdi
	call	utrie2_close_67@PLT
	movq	376(%r12), %rdi
	testq	%rdi, %rdi
	je	.L35
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
.L35:
	movq	384(%r12), %rdi
	call	uhash_close_67@PLT
	leaq	104(%r12), %rdi
	movl	$0, 392(%r12)
	mfence
	call	_ZN6icu_676LocaleD1Ev@PLT
	leaq	40(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6712SharedObjectD2Ev@PLT
	.cfi_endproc
.LFE3325:
	.size	_ZN6icu_6718CollationTailoringD2Ev, .-_ZN6icu_6718CollationTailoringD2Ev
	.globl	_ZN6icu_6718CollationTailoringD1Ev
	.set	_ZN6icu_6718CollationTailoringD1Ev,_ZN6icu_6718CollationTailoringD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CollationTailoringD0Ev
	.type	_ZN6icu_6718CollationTailoringD0Ev, @function
_ZN6icu_6718CollationTailoringD0Ev:
.LFB3327:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN6icu_6718CollationTailoringD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3327:
	.size	_ZN6icu_6718CollationTailoringD0Ev, .-_ZN6icu_6718CollationTailoringD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719CollationCacheEntryD2Ev
	.type	_ZN6icu_6719CollationCacheEntryD2Ev, @function
_ZN6icu_6719CollationCacheEntryD2Ev:
.LFB3333:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6719CollationCacheEntryE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	248(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L52
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	movq	$0, 248(%r12)
.L52:
	leaq	24(%r12), %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6712SharedObjectD2Ev@PLT
	.cfi_endproc
.LFE3333:
	.size	_ZN6icu_6719CollationCacheEntryD2Ev, .-_ZN6icu_6719CollationCacheEntryD2Ev
	.globl	_ZN6icu_6719CollationCacheEntryD1Ev
	.set	_ZN6icu_6719CollationCacheEntryD1Ev,_ZN6icu_6719CollationCacheEntryD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719CollationCacheEntryD0Ev
	.type	_ZN6icu_6719CollationCacheEntryD0Ev, @function
_ZN6icu_6719CollationCacheEntryD0Ev:
.LFB3335:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6719CollationCacheEntryE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	248(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L58
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	movq	$0, 248(%r12)
.L58:
	leaq	24(%r12), %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6712SharedObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3335:
	.size	_ZN6icu_6719CollationCacheEntryD0Ev, .-_ZN6icu_6719CollationCacheEntryD0Ev
	.weak	_ZTSN6icu_6718CollationTailoringE
	.section	.rodata._ZTSN6icu_6718CollationTailoringE,"aG",@progbits,_ZTSN6icu_6718CollationTailoringE,comdat
	.align 16
	.type	_ZTSN6icu_6718CollationTailoringE, @object
	.size	_ZTSN6icu_6718CollationTailoringE, 30
_ZTSN6icu_6718CollationTailoringE:
	.string	"N6icu_6718CollationTailoringE"
	.weak	_ZTIN6icu_6718CollationTailoringE
	.section	.data.rel.ro._ZTIN6icu_6718CollationTailoringE,"awG",@progbits,_ZTIN6icu_6718CollationTailoringE,comdat
	.align 8
	.type	_ZTIN6icu_6718CollationTailoringE, @object
	.size	_ZTIN6icu_6718CollationTailoringE, 24
_ZTIN6icu_6718CollationTailoringE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6718CollationTailoringE
	.quad	_ZTIN6icu_6712SharedObjectE
	.weak	_ZTSN6icu_6719CollationCacheEntryE
	.section	.rodata._ZTSN6icu_6719CollationCacheEntryE,"aG",@progbits,_ZTSN6icu_6719CollationCacheEntryE,comdat
	.align 16
	.type	_ZTSN6icu_6719CollationCacheEntryE, @object
	.size	_ZTSN6icu_6719CollationCacheEntryE, 31
_ZTSN6icu_6719CollationCacheEntryE:
	.string	"N6icu_6719CollationCacheEntryE"
	.weak	_ZTIN6icu_6719CollationCacheEntryE
	.section	.data.rel.ro._ZTIN6icu_6719CollationCacheEntryE,"awG",@progbits,_ZTIN6icu_6719CollationCacheEntryE,comdat
	.align 8
	.type	_ZTIN6icu_6719CollationCacheEntryE, @object
	.size	_ZTIN6icu_6719CollationCacheEntryE, 24
_ZTIN6icu_6719CollationCacheEntryE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6719CollationCacheEntryE
	.quad	_ZTIN6icu_6712SharedObjectE
	.weak	_ZTVN6icu_6718CollationTailoringE
	.section	.data.rel.ro._ZTVN6icu_6718CollationTailoringE,"awG",@progbits,_ZTVN6icu_6718CollationTailoringE,comdat
	.align 8
	.type	_ZTVN6icu_6718CollationTailoringE, @object
	.size	_ZTVN6icu_6718CollationTailoringE, 40
_ZTVN6icu_6718CollationTailoringE:
	.quad	0
	.quad	_ZTIN6icu_6718CollationTailoringE
	.quad	_ZN6icu_6718CollationTailoringD1Ev
	.quad	_ZN6icu_6718CollationTailoringD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.weak	_ZTVN6icu_6719CollationCacheEntryE
	.section	.data.rel.ro._ZTVN6icu_6719CollationCacheEntryE,"awG",@progbits,_ZTVN6icu_6719CollationCacheEntryE,comdat
	.align 8
	.type	_ZTVN6icu_6719CollationCacheEntryE, @object
	.size	_ZTVN6icu_6719CollationCacheEntryE, 40
_ZTVN6icu_6719CollationCacheEntryE:
	.quad	0
	.quad	_ZTIN6icu_6719CollationCacheEntryE
	.quad	_ZN6icu_6719CollationCacheEntryD1Ev
	.quad	_ZN6icu_6719CollationCacheEntryD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
