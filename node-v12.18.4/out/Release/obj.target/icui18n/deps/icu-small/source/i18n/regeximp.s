	.file	"regeximp.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724CaseFoldingUTextIteratorC2ER5UText
	.type	_ZN6icu_6724CaseFoldingUTextIteratorC2ER5UText, @function
_ZN6icu_6724CaseFoldingUTextIteratorC2ER5UText:
.LFB3036:
	.cfi_startproc
	endbr64
	movq	%rsi, (%rdi)
	movq	$0, 8(%rdi)
	movl	$0, 16(%rdi)
	ret
	.cfi_endproc
.LFE3036:
	.size	_ZN6icu_6724CaseFoldingUTextIteratorC2ER5UText, .-_ZN6icu_6724CaseFoldingUTextIteratorC2ER5UText
	.globl	_ZN6icu_6724CaseFoldingUTextIteratorC1ER5UText
	.set	_ZN6icu_6724CaseFoldingUTextIteratorC1ER5UText,_ZN6icu_6724CaseFoldingUTextIteratorC2ER5UText
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724CaseFoldingUTextIteratorD2Ev
	.type	_ZN6icu_6724CaseFoldingUTextIteratorD2Ev, @function
_ZN6icu_6724CaseFoldingUTextIteratorD2Ev:
.LFB3039:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3039:
	.size	_ZN6icu_6724CaseFoldingUTextIteratorD2Ev, .-_ZN6icu_6724CaseFoldingUTextIteratorD2Ev
	.globl	_ZN6icu_6724CaseFoldingUTextIteratorD1Ev
	.set	_ZN6icu_6724CaseFoldingUTextIteratorD1Ev,_ZN6icu_6724CaseFoldingUTextIteratorD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724CaseFoldingUTextIterator4nextEv
	.type	_ZN6icu_6724CaseFoldingUTextIterator4nextEv, @function
_ZN6icu_6724CaseFoldingUTextIterator4nextEv:
.LFB3041:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rcx
	testq	%rcx, %rcx
	je	.L5
	movslq	20(%rdi), %rax
	movl	16(%rbx), %edx
	movq	%rax, %rdi
	leal	1(%rax), %esi
	leaq	(%rcx,%rax,2), %rax
.L6:
	movzwl	(%rax), %eax
	movl	%esi, 20(%rbx)
	movl	%eax, %r8d
	andl	$-1024, %r8d
	cmpl	$55296, %r8d
	je	.L18
.L12:
	cmpl	%esi, %edx
	jle	.L13
.L4:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	.cfi_restore_state
	movq	$0, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	.cfi_restore_state
	cmpl	%edx, %esi
	je	.L13
	movslq	%esi, %r8
	movzwl	(%rcx,%r8,2), %ecx
	movl	%ecx, %r8d
	andl	$-1024, %r8d
	cmpl	$56320, %r8d
	jne	.L12
	leal	2(%rdi), %esi
	sall	$10, %eax
	movl	%esi, 20(%rbx)
	leal	-56613888(%rcx,%rax), %eax
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L5:
	movq	(%rdi), %rdi
	movl	40(%rdi), %edx
	cmpl	44(%rdi), %edx
	jge	.L7
	movq	48(%rdi), %rcx
	movslq	%edx, %rax
	movzwl	(%rcx,%rax,2), %eax
	cmpw	$-10241, %ax
	jbe	.L19
.L7:
	call	utext_next32_67@PLT
	cmpl	$-1, %eax
	je	.L4
.L8:
	xorl	%edx, %edx
	leaq	8(%rbx), %rsi
	movl	%eax, %edi
	call	ucase_toFullFolding_67@PLT
	movl	%eax, 16(%rbx)
	movl	%eax, %edx
	cmpl	$30, %eax
	ja	.L20
	movq	8(%rbx), %rcx
	movl	$1, %esi
	xorl	%edi, %edi
	movq	%rcx, %rax
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L19:
	addl	$1, %edx
	movl	%edx, 40(%rdi)
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L20:
	testl	%eax, %eax
	jns	.L11
	notl	%edx
	movl	%edx, 16(%rbx)
.L11:
	movq	$0, 8(%rbx)
	addq	$8, %rsp
	movl	%edx, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3041:
	.size	_ZN6icu_6724CaseFoldingUTextIterator4nextEv, .-_ZN6icu_6724CaseFoldingUTextIterator4nextEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724CaseFoldingUTextIterator11inExpansionEv
	.type	_ZN6icu_6724CaseFoldingUTextIterator11inExpansionEv, @function
_ZN6icu_6724CaseFoldingUTextIterator11inExpansionEv:
.LFB3042:
	.cfi_startproc
	endbr64
	cmpq	$0, 8(%rdi)
	setne	%al
	ret
	.cfi_endproc
.LFE3042:
	.size	_ZN6icu_6724CaseFoldingUTextIterator11inExpansionEv, .-_ZN6icu_6724CaseFoldingUTextIterator11inExpansionEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724CaseFoldingUCharIteratorC2EPKDsll
	.type	_ZN6icu_6724CaseFoldingUCharIteratorC2EPKDsll, @function
_ZN6icu_6724CaseFoldingUCharIteratorC2EPKDsll:
.LFB3044:
	.cfi_startproc
	endbr64
	movq	%rdx, %xmm0
	movq	%rcx, %xmm1
	movq	%rsi, (%rdi)
	punpcklqdq	%xmm1, %xmm0
	movq	$0, 24(%rdi)
	movl	$0, 32(%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE3044:
	.size	_ZN6icu_6724CaseFoldingUCharIteratorC2EPKDsll, .-_ZN6icu_6724CaseFoldingUCharIteratorC2EPKDsll
	.globl	_ZN6icu_6724CaseFoldingUCharIteratorC1EPKDsll
	.set	_ZN6icu_6724CaseFoldingUCharIteratorC1EPKDsll,_ZN6icu_6724CaseFoldingUCharIteratorC2EPKDsll
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724CaseFoldingUCharIteratorD2Ev
	.type	_ZN6icu_6724CaseFoldingUCharIteratorD2Ev, @function
_ZN6icu_6724CaseFoldingUCharIteratorD2Ev:
.LFB3047:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3047:
	.size	_ZN6icu_6724CaseFoldingUCharIteratorD2Ev, .-_ZN6icu_6724CaseFoldingUCharIteratorD2Ev
	.globl	_ZN6icu_6724CaseFoldingUCharIteratorD1Ev
	.set	_ZN6icu_6724CaseFoldingUCharIteratorD1Ev,_ZN6icu_6724CaseFoldingUCharIteratorD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724CaseFoldingUCharIterator4nextEv
	.type	_ZN6icu_6724CaseFoldingUCharIterator4nextEv, @function
_ZN6icu_6724CaseFoldingUCharIterator4nextEv:
.LFB3049:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	24(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L25
	movslq	36(%rdi), %rax
	movq	%rax, %rsi
	leal	1(%rax), %ecx
	leaq	(%rdx,%rax,2), %rdi
	movl	32(%rbx), %eax
.L26:
	movzwl	(%rdi), %r8d
	movl	%ecx, 36(%rbx)
	movl	%r8d, %edi
	andl	$-1024, %edi
	cmpl	$55296, %edi
	je	.L41
.L31:
	cmpl	%ecx, %eax
	jle	.L32
.L24:
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore_state
	movq	$0, 24(%rbx)
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	.cfi_restore_state
	cmpl	%eax, %ecx
	je	.L32
	movslq	%ecx, %rdi
	movzwl	(%rdx,%rdi,2), %edx
	movl	%edx, %edi
	andl	$-1024, %edi
	cmpl	$56320, %edi
	jne	.L31
	leal	2(%rsi), %ecx
	sall	$10, %r8d
	movl	%ecx, 36(%rbx)
	leal	-56613888(%rdx,%r8), %r8d
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L25:
	movq	8(%rdi), %rax
	movq	16(%rdi), %rdx
	cmpq	%rdx, %rax
	jge	.L33
	movq	(%rdi), %rsi
	leaq	1(%rax), %rcx
	leaq	(%rax,%rax), %r9
	movq	%rcx, 8(%rdi)
	movzwl	(%rsi,%rax,2), %edi
	movl	%edi, %r8d
	andl	$-1024, %r8d
	cmpl	$55296, %r8d
	jne	.L28
	cmpq	%rcx, %rdx
	je	.L28
	movzwl	2(%rsi,%r9), %edx
	movl	%edx, %ecx
	andl	$-1024, %ecx
	cmpl	$56320, %ecx
	je	.L42
	.p2align 4,,10
	.p2align 3
.L28:
	leaq	24(%rbx), %rsi
	xorl	%edx, %edx
	call	ucase_toFullFolding_67@PLT
	movl	%eax, 32(%rbx)
	cmpl	$30, %eax
	ja	.L43
	movq	24(%rbx), %rdx
	movl	$1, %ecx
	xorl	%esi, %esi
	movq	%rdx, %rdi
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L43:
	testl	%eax, %eax
	jns	.L30
	notl	%eax
	movl	%eax, 32(%rbx)
.L30:
	movq	$0, 24(%rbx)
	movl	%eax, %r8d
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	.cfi_restore_state
	addq	$2, %rax
	sall	$10, %edi
	movq	%rax, 8(%rbx)
	leal	-56613888(%rdx,%rdi), %edi
	jmp	.L28
.L33:
	movl	$-1, %r8d
	jmp	.L24
	.cfi_endproc
.LFE3049:
	.size	_ZN6icu_6724CaseFoldingUCharIterator4nextEv, .-_ZN6icu_6724CaseFoldingUCharIterator4nextEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724CaseFoldingUCharIterator11inExpansionEv
	.type	_ZN6icu_6724CaseFoldingUCharIterator11inExpansionEv, @function
_ZN6icu_6724CaseFoldingUCharIterator11inExpansionEv:
.LFB3050:
	.cfi_startproc
	endbr64
	cmpq	$0, 24(%rdi)
	setne	%al
	ret
	.cfi_endproc
.LFE3050:
	.size	_ZN6icu_6724CaseFoldingUCharIterator11inExpansionEv, .-_ZN6icu_6724CaseFoldingUCharIterator11inExpansionEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724CaseFoldingUCharIterator8getIndexEv
	.type	_ZN6icu_6724CaseFoldingUCharIterator8getIndexEv, @function
_ZN6icu_6724CaseFoldingUCharIterator8getIndexEv:
.LFB3051:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE3051:
	.size	_ZN6icu_6724CaseFoldingUCharIterator8getIndexEv, .-_ZN6icu_6724CaseFoldingUCharIterator8getIndexEv
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
