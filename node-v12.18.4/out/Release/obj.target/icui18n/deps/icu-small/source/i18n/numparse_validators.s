	.file	"numparse_validators.cpp"
	.text
	.section	.text._ZNK6icu_678numparse4impl18NumberParseMatcher10isFlexibleEv,"axG",@progbits,_ZNK6icu_678numparse4impl18NumberParseMatcher10isFlexibleEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_678numparse4impl18NumberParseMatcher10isFlexibleEv
	.type	_ZNK6icu_678numparse4impl18NumberParseMatcher10isFlexibleEv, @function
_ZNK6icu_678numparse4impl18NumberParseMatcher10isFlexibleEv:
.LFB2799:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2799:
	.size	_ZNK6icu_678numparse4impl18NumberParseMatcher10isFlexibleEv, .-_ZNK6icu_678numparse4impl18NumberParseMatcher10isFlexibleEv
	.section	.text._ZNK6icu_678numparse4impl17ValidationMatcher5matchERNS_13StringSegmentERNS1_12ParsedNumberER10UErrorCode,"axG",@progbits,_ZNK6icu_678numparse4impl17ValidationMatcher5matchERNS_13StringSegmentERNS1_12ParsedNumberER10UErrorCode,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_678numparse4impl17ValidationMatcher5matchERNS_13StringSegmentERNS1_12ParsedNumberER10UErrorCode
	.type	_ZNK6icu_678numparse4impl17ValidationMatcher5matchERNS_13StringSegmentERNS1_12ParsedNumberER10UErrorCode, @function
_ZNK6icu_678numparse4impl17ValidationMatcher5matchERNS_13StringSegmentERNS1_12ParsedNumberER10UErrorCode:
.LFB2801:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2801:
	.size	_ZNK6icu_678numparse4impl17ValidationMatcher5matchERNS_13StringSegmentERNS1_12ParsedNumberER10UErrorCode, .-_ZNK6icu_678numparse4impl17ValidationMatcher5matchERNS_13StringSegmentERNS1_12ParsedNumberER10UErrorCode
	.section	.text._ZNK6icu_678numparse4impl17ValidationMatcher9smokeTestERKNS_13StringSegmentE,"axG",@progbits,_ZNK6icu_678numparse4impl17ValidationMatcher9smokeTestERKNS_13StringSegmentE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_678numparse4impl17ValidationMatcher9smokeTestERKNS_13StringSegmentE
	.type	_ZNK6icu_678numparse4impl17ValidationMatcher9smokeTestERKNS_13StringSegmentE, @function
_ZNK6icu_678numparse4impl17ValidationMatcher9smokeTestERKNS_13StringSegmentE:
.LFB2802:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2802:
	.size	_ZNK6icu_678numparse4impl17ValidationMatcher9smokeTestERKNS_13StringSegmentE, .-_ZNK6icu_678numparse4impl17ValidationMatcher9smokeTestERKNS_13StringSegmentE
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678numparse4impl21RequireAffixValidator11postProcessERNS1_12ParsedNumberE
	.type	_ZNK6icu_678numparse4impl21RequireAffixValidator11postProcessERNS1_12ParsedNumberE, @function
_ZNK6icu_678numparse4impl21RequireAffixValidator11postProcessERNS1_12ParsedNumberE:
.LFB2803:
	.cfi_startproc
	endbr64
	testb	$1, 88(%rsi)
	jne	.L6
	testb	$1, 152(%rsi)
	je	.L5
.L6:
	orl	$256, 76(%rsi)
.L5:
	ret
	.cfi_endproc
.LFE2803:
	.size	_ZNK6icu_678numparse4impl21RequireAffixValidator11postProcessERNS1_12ParsedNumberE, .-_ZNK6icu_678numparse4impl21RequireAffixValidator11postProcessERNS1_12ParsedNumberE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678numparse4impl24RequireCurrencyValidator11postProcessERNS1_12ParsedNumberE
	.type	_ZNK6icu_678numparse4impl24RequireCurrencyValidator11postProcessERNS1_12ParsedNumberE, @function
_ZNK6icu_678numparse4impl24RequireCurrencyValidator11postProcessERNS1_12ParsedNumberE:
.LFB2805:
	.cfi_startproc
	endbr64
	cmpw	$0, 208(%rsi)
	jne	.L11
	orl	$256, 76(%rsi)
.L11:
	ret
	.cfi_endproc
.LFE2805:
	.size	_ZNK6icu_678numparse4impl24RequireCurrencyValidator11postProcessERNS1_12ParsedNumberE, .-_ZNK6icu_678numparse4impl24RequireCurrencyValidator11postProcessERNS1_12ParsedNumberE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678numparse4impl32RequireDecimalSeparatorValidator11postProcessERNS1_12ParsedNumberE
	.type	_ZNK6icu_678numparse4impl32RequireDecimalSeparatorValidator11postProcessERNS1_12ParsedNumberE, @function
_ZNK6icu_678numparse4impl32RequireDecimalSeparatorValidator11postProcessERNS1_12ParsedNumberE:
.LFB2820:
	.cfi_startproc
	endbr64
	movl	76(%rsi), %edx
	movl	%edx, %eax
	shrl	$5, %eax
	andl	$1, %eax
	cmpb	%al, 8(%rdi)
	je	.L13
	orb	$1, %dh
	movl	%edx, 76(%rsi)
.L13:
	ret
	.cfi_endproc
.LFE2820:
	.size	_ZNK6icu_678numparse4impl32RequireDecimalSeparatorValidator11postProcessERNS1_12ParsedNumberE, .-_ZNK6icu_678numparse4impl32RequireDecimalSeparatorValidator11postProcessERNS1_12ParsedNumberE
	.section	.rodata.str2.2,"aMS",@progbits,2
	.align 2
.LC0:
	.string	"<"
	.string	"R"
	.string	"e"
	.string	"q"
	.string	"A"
	.string	"f"
	.string	"f"
	.string	"i"
	.string	"x"
	.string	">"
	.string	""
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678numparse4impl21RequireAffixValidator8toStringEv
	.type	_ZNK6icu_678numparse4impl21RequireAffixValidator8toStringEv, @function
_ZNK6icu_678numparse4impl21RequireAffixValidator8toStringEv:
.LFB2804:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2804:
	.size	_ZNK6icu_678numparse4impl21RequireAffixValidator8toStringEv, .-_ZNK6icu_678numparse4impl21RequireAffixValidator8toStringEv
	.section	.rodata.str2.2
	.align 2
.LC1:
	.string	"<"
	.string	"R"
	.string	"e"
	.string	"q"
	.string	"C"
	.string	"u"
	.string	"r"
	.string	"r"
	.string	"e"
	.string	"n"
	.string	"c"
	.string	"y"
	.string	">"
	.string	""
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678numparse4impl24RequireCurrencyValidator8toStringEv
	.type	_ZNK6icu_678numparse4impl24RequireCurrencyValidator8toStringEv, @function
_ZNK6icu_678numparse4impl24RequireCurrencyValidator8toStringEv:
.LFB2806:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC1(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2806:
	.size	_ZNK6icu_678numparse4impl24RequireCurrencyValidator8toStringEv, .-_ZNK6icu_678numparse4impl24RequireCurrencyValidator8toStringEv
	.section	.rodata.str2.2
	.align 2
.LC2:
	.string	"<"
	.string	"R"
	.string	"e"
	.string	"q"
	.string	"D"
	.string	"e"
	.string	"c"
	.string	"i"
	.string	"m"
	.string	"a"
	.string	"l"
	.string	">"
	.string	""
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678numparse4impl32RequireDecimalSeparatorValidator8toStringEv
	.type	_ZNK6icu_678numparse4impl32RequireDecimalSeparatorValidator8toStringEv, @function
_ZNK6icu_678numparse4impl32RequireDecimalSeparatorValidator8toStringEv:
.LFB2821:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC2(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2821:
	.size	_ZNK6icu_678numparse4impl32RequireDecimalSeparatorValidator8toStringEv, .-_ZNK6icu_678numparse4impl32RequireDecimalSeparatorValidator8toStringEv
	.section	.rodata.str2.2
	.align 2
.LC3:
	.string	"<"
	.string	"R"
	.string	"e"
	.string	"q"
	.string	"N"
	.string	"u"
	.string	"m"
	.string	"b"
	.string	"e"
	.string	"r"
	.string	">"
	.string	""
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678numparse4impl22RequireNumberValidator8toStringEv
	.type	_ZNK6icu_678numparse4impl22RequireNumberValidator8toStringEv, @function
_ZNK6icu_678numparse4impl22RequireNumberValidator8toStringEv:
.LFB2823:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC3(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2823:
	.size	_ZNK6icu_678numparse4impl22RequireNumberValidator8toStringEv, .-_ZNK6icu_678numparse4impl22RequireNumberValidator8toStringEv
	.section	.rodata.str2.2
	.align 2
.LC4:
	.string	"<"
	.string	"S"
	.string	"c"
	.string	"a"
	.string	"l"
	.string	"e"
	.string	">"
	.string	""
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678numparse4impl22MultiplierParseHandler8toStringEv
	.type	_ZNK6icu_678numparse4impl22MultiplierParseHandler8toStringEv, @function
_ZNK6icu_678numparse4impl22MultiplierParseHandler8toStringEv:
.LFB2829:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC4(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2829:
	.size	_ZNK6icu_678numparse4impl22MultiplierParseHandler8toStringEv, .-_ZNK6icu_678numparse4impl22MultiplierParseHandler8toStringEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678numparse4impl22RequireNumberValidator11postProcessERNS1_12ParsedNumberE
	.type	_ZNK6icu_678numparse4impl22RequireNumberValidator11postProcessERNS1_12ParsedNumberE, @function
_ZNK6icu_678numparse4impl22RequireNumberValidator11postProcessERNS1_12ParsedNumberE:
.LFB2822:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	_ZNK6icu_678numparse4impl12ParsedNumber10seenNumberEv@PLT
	testb	%al, %al
	jne	.L25
	orl	$256, 76(%rbx)
.L25:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2822:
	.size	_ZNK6icu_678numparse4impl22RequireNumberValidator11postProcessERNS1_12ParsedNumberE, .-_ZNK6icu_678numparse4impl22RequireNumberValidator11postProcessERNS1_12ParsedNumberE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678numparse4impl22MultiplierParseHandler11postProcessERNS1_12ParsedNumberE
	.type	_ZNK6icu_678numparse4impl22MultiplierParseHandler11postProcessERNS1_12ParsedNumberE, @function
_ZNK6icu_678numparse4impl22MultiplierParseHandler11postProcessERNS1_12ParsedNumberE:
.LFB2828:
	.cfi_startproc
	endbr64
	cmpb	$0, 8(%rsi)
	je	.L30
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	addq	$8, %rdi
	jmp	_ZNK6icu_676number5Scale17applyReciprocalToERNS0_4impl15DecimalQuantityE@PLT
	.cfi_endproc
.LFE2828:
	.size	_ZNK6icu_678numparse4impl22MultiplierParseHandler11postProcessERNS1_12ParsedNumberE, .-_ZNK6icu_678numparse4impl22MultiplierParseHandler11postProcessERNS1_12ParsedNumberE
	.section	.text._ZN6icu_678numparse4impl22MultiplierParseHandlerD2Ev,"axG",@progbits,_ZN6icu_678numparse4impl22MultiplierParseHandlerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_678numparse4impl22MultiplierParseHandlerD2Ev
	.type	_ZN6icu_678numparse4impl22MultiplierParseHandlerD2Ev, @function
_ZN6icu_678numparse4impl22MultiplierParseHandlerD2Ev:
.LFB3914:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_678numparse4impl22MultiplierParseHandlerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	8(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -8(%rdi)
	call	_ZN6icu_676number5ScaleD1Ev@PLT
	leaq	16+_ZTVN6icu_678numparse4impl17ValidationMatcherE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_678numparse4impl18NumberParseMatcherD2Ev@PLT
	.cfi_endproc
.LFE3914:
	.size	_ZN6icu_678numparse4impl22MultiplierParseHandlerD2Ev, .-_ZN6icu_678numparse4impl22MultiplierParseHandlerD2Ev
	.weak	_ZN6icu_678numparse4impl22MultiplierParseHandlerD1Ev
	.set	_ZN6icu_678numparse4impl22MultiplierParseHandlerD1Ev,_ZN6icu_678numparse4impl22MultiplierParseHandlerD2Ev
	.section	.text._ZN6icu_678numparse4impl22MultiplierParseHandlerD0Ev,"axG",@progbits,_ZN6icu_678numparse4impl22MultiplierParseHandlerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_678numparse4impl22MultiplierParseHandlerD0Ev
	.type	_ZN6icu_678numparse4impl22MultiplierParseHandlerD0Ev, @function
_ZN6icu_678numparse4impl22MultiplierParseHandlerD0Ev:
.LFB3916:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_678numparse4impl22MultiplierParseHandlerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	8(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -8(%rdi)
	call	_ZN6icu_676number5ScaleD1Ev@PLT
	leaq	16+_ZTVN6icu_678numparse4impl17ValidationMatcherE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	call	_ZN6icu_678numparse4impl18NumberParseMatcherD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3916:
	.size	_ZN6icu_678numparse4impl22MultiplierParseHandlerD0Ev, .-_ZN6icu_678numparse4impl22MultiplierParseHandlerD0Ev
	.section	.text._ZN6icu_678numparse4impl22RequireNumberValidatorD2Ev,"axG",@progbits,_ZN6icu_678numparse4impl22RequireNumberValidatorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_678numparse4impl22RequireNumberValidatorD2Ev
	.type	_ZN6icu_678numparse4impl22RequireNumberValidatorD2Ev, @function
_ZN6icu_678numparse4impl22RequireNumberValidatorD2Ev:
.LFB3918:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_678numparse4impl17ValidationMatcherE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_678numparse4impl18NumberParseMatcherD2Ev@PLT
	.cfi_endproc
.LFE3918:
	.size	_ZN6icu_678numparse4impl22RequireNumberValidatorD2Ev, .-_ZN6icu_678numparse4impl22RequireNumberValidatorD2Ev
	.weak	_ZN6icu_678numparse4impl22RequireNumberValidatorD1Ev
	.set	_ZN6icu_678numparse4impl22RequireNumberValidatorD1Ev,_ZN6icu_678numparse4impl22RequireNumberValidatorD2Ev
	.section	.text._ZN6icu_678numparse4impl22RequireNumberValidatorD0Ev,"axG",@progbits,_ZN6icu_678numparse4impl22RequireNumberValidatorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_678numparse4impl22RequireNumberValidatorD0Ev
	.type	_ZN6icu_678numparse4impl22RequireNumberValidatorD0Ev, @function
_ZN6icu_678numparse4impl22RequireNumberValidatorD0Ev:
.LFB3920:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_678numparse4impl17ValidationMatcherE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_678numparse4impl18NumberParseMatcherD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3920:
	.size	_ZN6icu_678numparse4impl22RequireNumberValidatorD0Ev, .-_ZN6icu_678numparse4impl22RequireNumberValidatorD0Ev
	.section	.text._ZN6icu_678numparse4impl32RequireDecimalSeparatorValidatorD2Ev,"axG",@progbits,_ZN6icu_678numparse4impl32RequireDecimalSeparatorValidatorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_678numparse4impl32RequireDecimalSeparatorValidatorD2Ev
	.type	_ZN6icu_678numparse4impl32RequireDecimalSeparatorValidatorD2Ev, @function
_ZN6icu_678numparse4impl32RequireDecimalSeparatorValidatorD2Ev:
.LFB3922:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_678numparse4impl17ValidationMatcherE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_678numparse4impl18NumberParseMatcherD2Ev@PLT
	.cfi_endproc
.LFE3922:
	.size	_ZN6icu_678numparse4impl32RequireDecimalSeparatorValidatorD2Ev, .-_ZN6icu_678numparse4impl32RequireDecimalSeparatorValidatorD2Ev
	.weak	_ZN6icu_678numparse4impl32RequireDecimalSeparatorValidatorD1Ev
	.set	_ZN6icu_678numparse4impl32RequireDecimalSeparatorValidatorD1Ev,_ZN6icu_678numparse4impl32RequireDecimalSeparatorValidatorD2Ev
	.section	.text._ZN6icu_678numparse4impl32RequireDecimalSeparatorValidatorD0Ev,"axG",@progbits,_ZN6icu_678numparse4impl32RequireDecimalSeparatorValidatorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_678numparse4impl32RequireDecimalSeparatorValidatorD0Ev
	.type	_ZN6icu_678numparse4impl32RequireDecimalSeparatorValidatorD0Ev, @function
_ZN6icu_678numparse4impl32RequireDecimalSeparatorValidatorD0Ev:
.LFB3924:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_678numparse4impl17ValidationMatcherE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_678numparse4impl18NumberParseMatcherD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3924:
	.size	_ZN6icu_678numparse4impl32RequireDecimalSeparatorValidatorD0Ev, .-_ZN6icu_678numparse4impl32RequireDecimalSeparatorValidatorD0Ev
	.section	.text._ZN6icu_678numparse4impl24RequireCurrencyValidatorD2Ev,"axG",@progbits,_ZN6icu_678numparse4impl24RequireCurrencyValidatorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_678numparse4impl24RequireCurrencyValidatorD2Ev
	.type	_ZN6icu_678numparse4impl24RequireCurrencyValidatorD2Ev, @function
_ZN6icu_678numparse4impl24RequireCurrencyValidatorD2Ev:
.LFB3926:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_678numparse4impl17ValidationMatcherE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_678numparse4impl18NumberParseMatcherD2Ev@PLT
	.cfi_endproc
.LFE3926:
	.size	_ZN6icu_678numparse4impl24RequireCurrencyValidatorD2Ev, .-_ZN6icu_678numparse4impl24RequireCurrencyValidatorD2Ev
	.weak	_ZN6icu_678numparse4impl24RequireCurrencyValidatorD1Ev
	.set	_ZN6icu_678numparse4impl24RequireCurrencyValidatorD1Ev,_ZN6icu_678numparse4impl24RequireCurrencyValidatorD2Ev
	.section	.text._ZN6icu_678numparse4impl24RequireCurrencyValidatorD0Ev,"axG",@progbits,_ZN6icu_678numparse4impl24RequireCurrencyValidatorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_678numparse4impl24RequireCurrencyValidatorD0Ev
	.type	_ZN6icu_678numparse4impl24RequireCurrencyValidatorD0Ev, @function
_ZN6icu_678numparse4impl24RequireCurrencyValidatorD0Ev:
.LFB3928:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_678numparse4impl17ValidationMatcherE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_678numparse4impl18NumberParseMatcherD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3928:
	.size	_ZN6icu_678numparse4impl24RequireCurrencyValidatorD0Ev, .-_ZN6icu_678numparse4impl24RequireCurrencyValidatorD0Ev
	.section	.text._ZN6icu_678numparse4impl21RequireAffixValidatorD2Ev,"axG",@progbits,_ZN6icu_678numparse4impl21RequireAffixValidatorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_678numparse4impl21RequireAffixValidatorD2Ev
	.type	_ZN6icu_678numparse4impl21RequireAffixValidatorD2Ev, @function
_ZN6icu_678numparse4impl21RequireAffixValidatorD2Ev:
.LFB3930:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_678numparse4impl17ValidationMatcherE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_678numparse4impl18NumberParseMatcherD2Ev@PLT
	.cfi_endproc
.LFE3930:
	.size	_ZN6icu_678numparse4impl21RequireAffixValidatorD2Ev, .-_ZN6icu_678numparse4impl21RequireAffixValidatorD2Ev
	.weak	_ZN6icu_678numparse4impl21RequireAffixValidatorD1Ev
	.set	_ZN6icu_678numparse4impl21RequireAffixValidatorD1Ev,_ZN6icu_678numparse4impl21RequireAffixValidatorD2Ev
	.section	.text._ZN6icu_678numparse4impl21RequireAffixValidatorD0Ev,"axG",@progbits,_ZN6icu_678numparse4impl21RequireAffixValidatorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_678numparse4impl21RequireAffixValidatorD0Ev
	.type	_ZN6icu_678numparse4impl21RequireAffixValidatorD0Ev, @function
_ZN6icu_678numparse4impl21RequireAffixValidatorD0Ev:
.LFB3932:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_678numparse4impl17ValidationMatcherE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_678numparse4impl18NumberParseMatcherD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3932:
	.size	_ZN6icu_678numparse4impl21RequireAffixValidatorD0Ev, .-_ZN6icu_678numparse4impl21RequireAffixValidatorD0Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_678numparse4impl32RequireDecimalSeparatorValidatorC2Eb
	.type	_ZN6icu_678numparse4impl32RequireDecimalSeparatorValidatorC2Eb, @function
_ZN6icu_678numparse4impl32RequireDecimalSeparatorValidatorC2Eb:
.LFB2818:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_678numparse4impl32RequireDecimalSeparatorValidatorE(%rip), %rax
	movb	%sil, 8(%rdi)
	movq	%rax, (%rdi)
	ret
	.cfi_endproc
.LFE2818:
	.size	_ZN6icu_678numparse4impl32RequireDecimalSeparatorValidatorC2Eb, .-_ZN6icu_678numparse4impl32RequireDecimalSeparatorValidatorC2Eb
	.globl	_ZN6icu_678numparse4impl32RequireDecimalSeparatorValidatorC1Eb
	.set	_ZN6icu_678numparse4impl32RequireDecimalSeparatorValidatorC1Eb,_ZN6icu_678numparse4impl32RequireDecimalSeparatorValidatorC2Eb
	.align 2
	.p2align 4
	.globl	_ZN6icu_678numparse4impl22MultiplierParseHandlerC2ENS_6number5ScaleE
	.type	_ZN6icu_678numparse4impl22MultiplierParseHandlerC2ENS_6number5ScaleE, @function
_ZN6icu_678numparse4impl22MultiplierParseHandlerC2ENS_6number5ScaleE:
.LFB2826:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_678numparse4impl22MultiplierParseHandlerE(%rip), %rax
	addq	$8, %rdi
	movq	%rax, -8(%rdi)
	jmp	_ZN6icu_676number5ScaleC1EOS1_@PLT
	.cfi_endproc
.LFE2826:
	.size	_ZN6icu_678numparse4impl22MultiplierParseHandlerC2ENS_6number5ScaleE, .-_ZN6icu_678numparse4impl22MultiplierParseHandlerC2ENS_6number5ScaleE
	.globl	_ZN6icu_678numparse4impl22MultiplierParseHandlerC1ENS_6number5ScaleE
	.set	_ZN6icu_678numparse4impl22MultiplierParseHandlerC1ENS_6number5ScaleE,_ZN6icu_678numparse4impl22MultiplierParseHandlerC2ENS_6number5ScaleE
	.weak	_ZTSN6icu_677UMemoryE
	.section	.rodata._ZTSN6icu_677UMemoryE,"aG",@progbits,_ZTSN6icu_677UMemoryE,comdat
	.align 16
	.type	_ZTSN6icu_677UMemoryE, @object
	.size	_ZTSN6icu_677UMemoryE, 18
_ZTSN6icu_677UMemoryE:
	.string	"N6icu_677UMemoryE"
	.weak	_ZTIN6icu_677UMemoryE
	.section	.data.rel.ro._ZTIN6icu_677UMemoryE,"awG",@progbits,_ZTIN6icu_677UMemoryE,comdat
	.align 8
	.type	_ZTIN6icu_677UMemoryE, @object
	.size	_ZTIN6icu_677UMemoryE, 16
_ZTIN6icu_677UMemoryE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN6icu_677UMemoryE
	.weak	_ZTSN6icu_678numparse4impl17ValidationMatcherE
	.section	.rodata._ZTSN6icu_678numparse4impl17ValidationMatcherE,"aG",@progbits,_ZTSN6icu_678numparse4impl17ValidationMatcherE,comdat
	.align 32
	.type	_ZTSN6icu_678numparse4impl17ValidationMatcherE, @object
	.size	_ZTSN6icu_678numparse4impl17ValidationMatcherE, 43
_ZTSN6icu_678numparse4impl17ValidationMatcherE:
	.string	"N6icu_678numparse4impl17ValidationMatcherE"
	.weak	_ZTIN6icu_678numparse4impl17ValidationMatcherE
	.section	.data.rel.ro._ZTIN6icu_678numparse4impl17ValidationMatcherE,"awG",@progbits,_ZTIN6icu_678numparse4impl17ValidationMatcherE,comdat
	.align 8
	.type	_ZTIN6icu_678numparse4impl17ValidationMatcherE, @object
	.size	_ZTIN6icu_678numparse4impl17ValidationMatcherE, 24
_ZTIN6icu_678numparse4impl17ValidationMatcherE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_678numparse4impl17ValidationMatcherE
	.quad	_ZTIN6icu_678numparse4impl18NumberParseMatcherE
	.weak	_ZTSN6icu_678numparse4impl21RequireAffixValidatorE
	.section	.rodata._ZTSN6icu_678numparse4impl21RequireAffixValidatorE,"aG",@progbits,_ZTSN6icu_678numparse4impl21RequireAffixValidatorE,comdat
	.align 32
	.type	_ZTSN6icu_678numparse4impl21RequireAffixValidatorE, @object
	.size	_ZTSN6icu_678numparse4impl21RequireAffixValidatorE, 47
_ZTSN6icu_678numparse4impl21RequireAffixValidatorE:
	.string	"N6icu_678numparse4impl21RequireAffixValidatorE"
	.weak	_ZTIN6icu_678numparse4impl21RequireAffixValidatorE
	.section	.data.rel.ro._ZTIN6icu_678numparse4impl21RequireAffixValidatorE,"awG",@progbits,_ZTIN6icu_678numparse4impl21RequireAffixValidatorE,comdat
	.align 8
	.type	_ZTIN6icu_678numparse4impl21RequireAffixValidatorE, @object
	.size	_ZTIN6icu_678numparse4impl21RequireAffixValidatorE, 56
_ZTIN6icu_678numparse4impl21RequireAffixValidatorE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN6icu_678numparse4impl21RequireAffixValidatorE
	.long	0
	.long	2
	.quad	_ZTIN6icu_678numparse4impl17ValidationMatcherE
	.quad	2
	.quad	_ZTIN6icu_677UMemoryE
	.quad	2
	.weak	_ZTSN6icu_678numparse4impl24RequireCurrencyValidatorE
	.section	.rodata._ZTSN6icu_678numparse4impl24RequireCurrencyValidatorE,"aG",@progbits,_ZTSN6icu_678numparse4impl24RequireCurrencyValidatorE,comdat
	.align 32
	.type	_ZTSN6icu_678numparse4impl24RequireCurrencyValidatorE, @object
	.size	_ZTSN6icu_678numparse4impl24RequireCurrencyValidatorE, 50
_ZTSN6icu_678numparse4impl24RequireCurrencyValidatorE:
	.string	"N6icu_678numparse4impl24RequireCurrencyValidatorE"
	.weak	_ZTIN6icu_678numparse4impl24RequireCurrencyValidatorE
	.section	.data.rel.ro._ZTIN6icu_678numparse4impl24RequireCurrencyValidatorE,"awG",@progbits,_ZTIN6icu_678numparse4impl24RequireCurrencyValidatorE,comdat
	.align 8
	.type	_ZTIN6icu_678numparse4impl24RequireCurrencyValidatorE, @object
	.size	_ZTIN6icu_678numparse4impl24RequireCurrencyValidatorE, 56
_ZTIN6icu_678numparse4impl24RequireCurrencyValidatorE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN6icu_678numparse4impl24RequireCurrencyValidatorE
	.long	0
	.long	2
	.quad	_ZTIN6icu_678numparse4impl17ValidationMatcherE
	.quad	2
	.quad	_ZTIN6icu_677UMemoryE
	.quad	2
	.weak	_ZTSN6icu_678numparse4impl32RequireDecimalSeparatorValidatorE
	.section	.rodata._ZTSN6icu_678numparse4impl32RequireDecimalSeparatorValidatorE,"aG",@progbits,_ZTSN6icu_678numparse4impl32RequireDecimalSeparatorValidatorE,comdat
	.align 32
	.type	_ZTSN6icu_678numparse4impl32RequireDecimalSeparatorValidatorE, @object
	.size	_ZTSN6icu_678numparse4impl32RequireDecimalSeparatorValidatorE, 58
_ZTSN6icu_678numparse4impl32RequireDecimalSeparatorValidatorE:
	.string	"N6icu_678numparse4impl32RequireDecimalSeparatorValidatorE"
	.weak	_ZTIN6icu_678numparse4impl32RequireDecimalSeparatorValidatorE
	.section	.data.rel.ro._ZTIN6icu_678numparse4impl32RequireDecimalSeparatorValidatorE,"awG",@progbits,_ZTIN6icu_678numparse4impl32RequireDecimalSeparatorValidatorE,comdat
	.align 8
	.type	_ZTIN6icu_678numparse4impl32RequireDecimalSeparatorValidatorE, @object
	.size	_ZTIN6icu_678numparse4impl32RequireDecimalSeparatorValidatorE, 56
_ZTIN6icu_678numparse4impl32RequireDecimalSeparatorValidatorE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN6icu_678numparse4impl32RequireDecimalSeparatorValidatorE
	.long	0
	.long	2
	.quad	_ZTIN6icu_678numparse4impl17ValidationMatcherE
	.quad	2
	.quad	_ZTIN6icu_677UMemoryE
	.quad	2
	.weak	_ZTSN6icu_678numparse4impl22RequireNumberValidatorE
	.section	.rodata._ZTSN6icu_678numparse4impl22RequireNumberValidatorE,"aG",@progbits,_ZTSN6icu_678numparse4impl22RequireNumberValidatorE,comdat
	.align 32
	.type	_ZTSN6icu_678numparse4impl22RequireNumberValidatorE, @object
	.size	_ZTSN6icu_678numparse4impl22RequireNumberValidatorE, 48
_ZTSN6icu_678numparse4impl22RequireNumberValidatorE:
	.string	"N6icu_678numparse4impl22RequireNumberValidatorE"
	.weak	_ZTIN6icu_678numparse4impl22RequireNumberValidatorE
	.section	.data.rel.ro._ZTIN6icu_678numparse4impl22RequireNumberValidatorE,"awG",@progbits,_ZTIN6icu_678numparse4impl22RequireNumberValidatorE,comdat
	.align 8
	.type	_ZTIN6icu_678numparse4impl22RequireNumberValidatorE, @object
	.size	_ZTIN6icu_678numparse4impl22RequireNumberValidatorE, 56
_ZTIN6icu_678numparse4impl22RequireNumberValidatorE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN6icu_678numparse4impl22RequireNumberValidatorE
	.long	0
	.long	2
	.quad	_ZTIN6icu_678numparse4impl17ValidationMatcherE
	.quad	2
	.quad	_ZTIN6icu_677UMemoryE
	.quad	2
	.weak	_ZTSN6icu_678numparse4impl22MultiplierParseHandlerE
	.section	.rodata._ZTSN6icu_678numparse4impl22MultiplierParseHandlerE,"aG",@progbits,_ZTSN6icu_678numparse4impl22MultiplierParseHandlerE,comdat
	.align 32
	.type	_ZTSN6icu_678numparse4impl22MultiplierParseHandlerE, @object
	.size	_ZTSN6icu_678numparse4impl22MultiplierParseHandlerE, 48
_ZTSN6icu_678numparse4impl22MultiplierParseHandlerE:
	.string	"N6icu_678numparse4impl22MultiplierParseHandlerE"
	.weak	_ZTIN6icu_678numparse4impl22MultiplierParseHandlerE
	.section	.data.rel.ro._ZTIN6icu_678numparse4impl22MultiplierParseHandlerE,"awG",@progbits,_ZTIN6icu_678numparse4impl22MultiplierParseHandlerE,comdat
	.align 8
	.type	_ZTIN6icu_678numparse4impl22MultiplierParseHandlerE, @object
	.size	_ZTIN6icu_678numparse4impl22MultiplierParseHandlerE, 56
_ZTIN6icu_678numparse4impl22MultiplierParseHandlerE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN6icu_678numparse4impl22MultiplierParseHandlerE
	.long	0
	.long	2
	.quad	_ZTIN6icu_678numparse4impl17ValidationMatcherE
	.quad	2
	.quad	_ZTIN6icu_677UMemoryE
	.quad	2
	.weak	_ZTVN6icu_678numparse4impl17ValidationMatcherE
	.section	.data.rel.ro._ZTVN6icu_678numparse4impl17ValidationMatcherE,"awG",@progbits,_ZTVN6icu_678numparse4impl17ValidationMatcherE,comdat
	.align 8
	.type	_ZTVN6icu_678numparse4impl17ValidationMatcherE, @object
	.size	_ZTVN6icu_678numparse4impl17ValidationMatcherE, 72
_ZTVN6icu_678numparse4impl17ValidationMatcherE:
	.quad	0
	.quad	_ZTIN6icu_678numparse4impl17ValidationMatcherE
	.quad	0
	.quad	0
	.quad	_ZNK6icu_678numparse4impl18NumberParseMatcher10isFlexibleEv
	.quad	_ZNK6icu_678numparse4impl17ValidationMatcher5matchERNS_13StringSegmentERNS1_12ParsedNumberER10UErrorCode
	.quad	_ZNK6icu_678numparse4impl17ValidationMatcher9smokeTestERKNS_13StringSegmentE
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.weak	_ZTVN6icu_678numparse4impl21RequireAffixValidatorE
	.section	.data.rel.ro.local._ZTVN6icu_678numparse4impl21RequireAffixValidatorE,"awG",@progbits,_ZTVN6icu_678numparse4impl21RequireAffixValidatorE,comdat
	.align 8
	.type	_ZTVN6icu_678numparse4impl21RequireAffixValidatorE, @object
	.size	_ZTVN6icu_678numparse4impl21RequireAffixValidatorE, 72
_ZTVN6icu_678numparse4impl21RequireAffixValidatorE:
	.quad	0
	.quad	_ZTIN6icu_678numparse4impl21RequireAffixValidatorE
	.quad	_ZN6icu_678numparse4impl21RequireAffixValidatorD1Ev
	.quad	_ZN6icu_678numparse4impl21RequireAffixValidatorD0Ev
	.quad	_ZNK6icu_678numparse4impl18NumberParseMatcher10isFlexibleEv
	.quad	_ZNK6icu_678numparse4impl17ValidationMatcher5matchERNS_13StringSegmentERNS1_12ParsedNumberER10UErrorCode
	.quad	_ZNK6icu_678numparse4impl17ValidationMatcher9smokeTestERKNS_13StringSegmentE
	.quad	_ZNK6icu_678numparse4impl21RequireAffixValidator11postProcessERNS1_12ParsedNumberE
	.quad	_ZNK6icu_678numparse4impl21RequireAffixValidator8toStringEv
	.weak	_ZTVN6icu_678numparse4impl24RequireCurrencyValidatorE
	.section	.data.rel.ro.local._ZTVN6icu_678numparse4impl24RequireCurrencyValidatorE,"awG",@progbits,_ZTVN6icu_678numparse4impl24RequireCurrencyValidatorE,comdat
	.align 8
	.type	_ZTVN6icu_678numparse4impl24RequireCurrencyValidatorE, @object
	.size	_ZTVN6icu_678numparse4impl24RequireCurrencyValidatorE, 72
_ZTVN6icu_678numparse4impl24RequireCurrencyValidatorE:
	.quad	0
	.quad	_ZTIN6icu_678numparse4impl24RequireCurrencyValidatorE
	.quad	_ZN6icu_678numparse4impl24RequireCurrencyValidatorD1Ev
	.quad	_ZN6icu_678numparse4impl24RequireCurrencyValidatorD0Ev
	.quad	_ZNK6icu_678numparse4impl18NumberParseMatcher10isFlexibleEv
	.quad	_ZNK6icu_678numparse4impl17ValidationMatcher5matchERNS_13StringSegmentERNS1_12ParsedNumberER10UErrorCode
	.quad	_ZNK6icu_678numparse4impl17ValidationMatcher9smokeTestERKNS_13StringSegmentE
	.quad	_ZNK6icu_678numparse4impl24RequireCurrencyValidator11postProcessERNS1_12ParsedNumberE
	.quad	_ZNK6icu_678numparse4impl24RequireCurrencyValidator8toStringEv
	.weak	_ZTVN6icu_678numparse4impl32RequireDecimalSeparatorValidatorE
	.section	.data.rel.ro.local._ZTVN6icu_678numparse4impl32RequireDecimalSeparatorValidatorE,"awG",@progbits,_ZTVN6icu_678numparse4impl32RequireDecimalSeparatorValidatorE,comdat
	.align 8
	.type	_ZTVN6icu_678numparse4impl32RequireDecimalSeparatorValidatorE, @object
	.size	_ZTVN6icu_678numparse4impl32RequireDecimalSeparatorValidatorE, 72
_ZTVN6icu_678numparse4impl32RequireDecimalSeparatorValidatorE:
	.quad	0
	.quad	_ZTIN6icu_678numparse4impl32RequireDecimalSeparatorValidatorE
	.quad	_ZN6icu_678numparse4impl32RequireDecimalSeparatorValidatorD1Ev
	.quad	_ZN6icu_678numparse4impl32RequireDecimalSeparatorValidatorD0Ev
	.quad	_ZNK6icu_678numparse4impl18NumberParseMatcher10isFlexibleEv
	.quad	_ZNK6icu_678numparse4impl17ValidationMatcher5matchERNS_13StringSegmentERNS1_12ParsedNumberER10UErrorCode
	.quad	_ZNK6icu_678numparse4impl17ValidationMatcher9smokeTestERKNS_13StringSegmentE
	.quad	_ZNK6icu_678numparse4impl32RequireDecimalSeparatorValidator11postProcessERNS1_12ParsedNumberE
	.quad	_ZNK6icu_678numparse4impl32RequireDecimalSeparatorValidator8toStringEv
	.weak	_ZTVN6icu_678numparse4impl22RequireNumberValidatorE
	.section	.data.rel.ro.local._ZTVN6icu_678numparse4impl22RequireNumberValidatorE,"awG",@progbits,_ZTVN6icu_678numparse4impl22RequireNumberValidatorE,comdat
	.align 8
	.type	_ZTVN6icu_678numparse4impl22RequireNumberValidatorE, @object
	.size	_ZTVN6icu_678numparse4impl22RequireNumberValidatorE, 72
_ZTVN6icu_678numparse4impl22RequireNumberValidatorE:
	.quad	0
	.quad	_ZTIN6icu_678numparse4impl22RequireNumberValidatorE
	.quad	_ZN6icu_678numparse4impl22RequireNumberValidatorD1Ev
	.quad	_ZN6icu_678numparse4impl22RequireNumberValidatorD0Ev
	.quad	_ZNK6icu_678numparse4impl18NumberParseMatcher10isFlexibleEv
	.quad	_ZNK6icu_678numparse4impl17ValidationMatcher5matchERNS_13StringSegmentERNS1_12ParsedNumberER10UErrorCode
	.quad	_ZNK6icu_678numparse4impl17ValidationMatcher9smokeTestERKNS_13StringSegmentE
	.quad	_ZNK6icu_678numparse4impl22RequireNumberValidator11postProcessERNS1_12ParsedNumberE
	.quad	_ZNK6icu_678numparse4impl22RequireNumberValidator8toStringEv
	.weak	_ZTVN6icu_678numparse4impl22MultiplierParseHandlerE
	.section	.data.rel.ro.local._ZTVN6icu_678numparse4impl22MultiplierParseHandlerE,"awG",@progbits,_ZTVN6icu_678numparse4impl22MultiplierParseHandlerE,comdat
	.align 8
	.type	_ZTVN6icu_678numparse4impl22MultiplierParseHandlerE, @object
	.size	_ZTVN6icu_678numparse4impl22MultiplierParseHandlerE, 72
_ZTVN6icu_678numparse4impl22MultiplierParseHandlerE:
	.quad	0
	.quad	_ZTIN6icu_678numparse4impl22MultiplierParseHandlerE
	.quad	_ZN6icu_678numparse4impl22MultiplierParseHandlerD1Ev
	.quad	_ZN6icu_678numparse4impl22MultiplierParseHandlerD0Ev
	.quad	_ZNK6icu_678numparse4impl18NumberParseMatcher10isFlexibleEv
	.quad	_ZNK6icu_678numparse4impl17ValidationMatcher5matchERNS_13StringSegmentERNS1_12ParsedNumberER10UErrorCode
	.quad	_ZNK6icu_678numparse4impl17ValidationMatcher9smokeTestERKNS_13StringSegmentE
	.quad	_ZNK6icu_678numparse4impl22MultiplierParseHandler11postProcessERNS1_12ParsedNumberE
	.quad	_ZNK6icu_678numparse4impl22MultiplierParseHandler8toStringEv
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
