	.file	"csdetect.cpp"
	.text
	.p2align 4
	.type	enumReset, @function
enumReset:
.LFB2915:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movl	$0, (%rax)
	ret
	.cfi_endproc
.LFE2915:
	.size	enumReset, .-enumReset
	.p2align 4
	.type	enumClose, @function
enumClose:
.LFB2912:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L4
	call	uprv_free_67@PLT
.L4:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE2912:
	.size	enumClose, .-enumClose
	.p2align 4
	.type	csdet_cleanup, @function
csdet_cleanup:
.LFB2771:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	_ZL14fCSRecognizers(%rip), %rdi
	testq	%rdi, %rdi
	je	.L10
	movl	_ZL19fCSRecognizers_size(%rip), %edx
	testl	%edx, %edx
	jle	.L11
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L14:
	leaq	0(,%rbx,8), %r13
	leaq	(%rdi,%r13), %rax
	movq	(%rax), %r12
	testq	%r12, %r12
	je	.L12
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L13
	movq	(%rdi), %rax
	call	*32(%rax)
.L13:
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	movq	_ZL14fCSRecognizers(%rip), %rdi
	movl	_ZL19fCSRecognizers_size(%rip), %edx
	leaq	(%rdi,%r13), %rax
.L12:
	addq	$1, %rbx
	movq	$0, (%rax)
	cmpl	%ebx, %edx
	jg	.L14
.L11:
	call	uprv_free_67@PLT
	movq	$0, _ZL14fCSRecognizers(%rip)
	movl	$0, _ZL19fCSRecognizers_size(%rip)
.L10:
	movl	$1, %eax
	movl	$0, _ZL22gCSRecognizersInitOnce(%rip)
	mfence
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2771:
	.size	csdet_cleanup, .-csdet_cleanup
	.p2align 4
	.type	initRecognizers, @function
initRecognizers:
.LFB2773:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	csdet_cleanup(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	$29, %edi
	subq	$240, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	ucln_i18n_registerCleanup_67@PLT
	movl	$8, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L27
	leaq	16+_ZTVN6icu_6717CharsetRecog_UTF8E(%rip), %rax
	movq	%rax, (%r12)
.L27:
	movl	$16, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L28
	movq	%r12, (%rax)
	movb	$1, 8(%rax)
.L28:
	movl	$8, %edi
	movq	%rax, -256(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L29
	leaq	16+_ZTVN6icu_6722CharsetRecog_UTF_16_BEE(%rip), %rax
	movq	%rax, (%r12)
.L29:
	movl	$16, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L30
	movq	%r12, (%rax)
	movb	$1, 8(%rax)
.L30:
	movl	$8, %edi
	movq	%rax, -248(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L31
	leaq	16+_ZTVN6icu_6722CharsetRecog_UTF_16_LEE(%rip), %rax
	movq	%rax, (%r12)
.L31:
	movl	$16, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L32
	movq	%r12, (%rax)
	movb	$1, 8(%rax)
.L32:
	movl	$8, %edi
	movq	%rax, -240(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L33
	leaq	16+_ZTVN6icu_6722CharsetRecog_UTF_32_BEE(%rip), %rax
	movq	%rax, (%r12)
.L33:
	movl	$16, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L34
	movq	%r12, (%rax)
	movb	$1, 8(%rax)
.L34:
	movl	$8, %edi
	movq	%rax, -232(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L35
	leaq	16+_ZTVN6icu_6722CharsetRecog_UTF_32_LEE(%rip), %rax
	movq	%rax, (%r12)
.L35:
	movl	$16, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L36
	movq	%r12, (%rax)
	movb	$1, 8(%rax)
.L36:
	movl	$8, %edi
	movq	%rax, -224(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L37
	movq	$0, (%rax)
	movq	%rax, %rdi
	call	_ZN6icu_6717CharsetRecog_sbcsC2Ev@PLT
	leaq	16+_ZTVN6icu_6719CharsetRecog_8859_1E(%rip), %rax
	movq	%rax, (%r12)
.L37:
	movl	$16, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L38
	movq	%r12, (%rax)
	movb	$1, 8(%rax)
.L38:
	movl	$8, %edi
	movq	%rax, -216(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L39
	movq	$0, (%rax)
	movq	%rax, %rdi
	call	_ZN6icu_6717CharsetRecog_sbcsC2Ev@PLT
	leaq	16+_ZTVN6icu_6719CharsetRecog_8859_2E(%rip), %rax
	movq	%rax, (%r12)
.L39:
	movl	$16, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L40
	movq	%r12, (%rax)
	movb	$1, 8(%rax)
.L40:
	movl	$8, %edi
	movq	%rax, -208(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L41
	movq	$0, (%rax)
	movq	%rax, %rdi
	call	_ZN6icu_6717CharsetRecog_sbcsC2Ev@PLT
	leaq	16+_ZTVN6icu_6722CharsetRecog_8859_5_ruE(%rip), %rax
	movq	%rax, (%r12)
.L41:
	movl	$16, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L42
	movq	%r12, (%rax)
	movb	$1, 8(%rax)
.L42:
	movl	$8, %edi
	movq	%rax, -200(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L43
	movq	$0, (%rax)
	movq	%rax, %rdi
	call	_ZN6icu_6717CharsetRecog_sbcsC2Ev@PLT
	leaq	16+_ZTVN6icu_6722CharsetRecog_8859_6_arE(%rip), %rax
	movq	%rax, (%r12)
.L43:
	movl	$16, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L44
	movq	%r12, (%rax)
	movb	$1, 8(%rax)
.L44:
	movl	$8, %edi
	movq	%rax, -192(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L45
	movq	$0, (%rax)
	movq	%rax, %rdi
	call	_ZN6icu_6717CharsetRecog_sbcsC2Ev@PLT
	leaq	16+_ZTVN6icu_6722CharsetRecog_8859_7_elE(%rip), %rax
	movq	%rax, (%r12)
.L45:
	movl	$16, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L46
	movq	%r12, (%rax)
	movb	$1, 8(%rax)
.L46:
	movl	$8, %edi
	movq	%rax, -184(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L47
	movq	$0, (%rax)
	movq	%rax, %rdi
	call	_ZN6icu_6717CharsetRecog_sbcsC2Ev@PLT
	leaq	16+_ZTVN6icu_6724CharsetRecog_8859_8_I_heE(%rip), %rax
	movq	%rax, (%r12)
.L47:
	movl	$16, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L48
	movq	%r12, (%rax)
	movb	$1, 8(%rax)
.L48:
	movl	$8, %edi
	movq	%rax, -176(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L49
	movq	$0, (%rax)
	movq	%rax, %rdi
	call	_ZN6icu_6717CharsetRecog_sbcsC2Ev@PLT
	leaq	16+_ZTVN6icu_6722CharsetRecog_8859_8_heE(%rip), %rax
	movq	%rax, (%r12)
.L49:
	movl	$16, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L50
	movq	%r12, (%rax)
	movb	$1, 8(%rax)
.L50:
	movl	$8, %edi
	movq	%rax, -168(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L51
	movq	$0, (%rax)
	movq	%rax, %rdi
	call	_ZN6icu_6717CharsetRecog_sbcsC2Ev@PLT
	leaq	16+_ZTVN6icu_6725CharsetRecog_windows_1251E(%rip), %rax
	movq	%rax, (%r12)
.L51:
	movl	$16, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L52
	movq	%r12, (%rax)
	movb	$1, 8(%rax)
.L52:
	movl	$8, %edi
	movq	%rax, -160(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L53
	movq	$0, (%rax)
	movq	%rax, %rdi
	call	_ZN6icu_6717CharsetRecog_sbcsC2Ev@PLT
	leaq	16+_ZTVN6icu_6725CharsetRecog_windows_1256E(%rip), %rax
	movq	%rax, (%r12)
.L53:
	movl	$16, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L54
	movq	%r12, (%rax)
	movb	$1, 8(%rax)
.L54:
	movl	$8, %edi
	movq	%rax, -152(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L55
	movq	$0, (%rax)
	movq	%rax, %rdi
	call	_ZN6icu_6717CharsetRecog_sbcsC2Ev@PLT
	leaq	16+_ZTVN6icu_6719CharsetRecog_KOI8_RE(%rip), %rax
	movq	%rax, (%r12)
.L55:
	movl	$16, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L56
	movq	%r12, (%rax)
	movb	$1, 8(%rax)
.L56:
	movl	$8, %edi
	movq	%rax, -144(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L57
	movq	$0, (%rax)
	movq	%rax, %rdi
	call	_ZN6icu_6717CharsetRecog_sbcsC2Ev@PLT
	leaq	16+_ZTVN6icu_6722CharsetRecog_8859_9_trE(%rip), %rax
	movq	%rax, (%r12)
.L57:
	movl	$16, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L58
	movq	%r12, (%rax)
	movb	$1, 8(%rax)
.L58:
	movl	$8, %edi
	movq	%rax, -136(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L59
	leaq	16+_ZTVN6icu_6717CharsetRecog_sjisE(%rip), %rax
	movq	%rax, (%r12)
.L59:
	movl	$16, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L60
	movq	%r12, (%rax)
	movb	$1, 8(%rax)
.L60:
	movl	$8, %edi
	movq	%rax, -128(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L61
	leaq	16+_ZTVN6icu_6721CharsetRecog_gb_18030E(%rip), %rax
	movq	%rax, (%r12)
.L61:
	movl	$16, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L62
	movq	%r12, (%rax)
	movb	$1, 8(%rax)
.L62:
	movl	$8, %edi
	movq	%rax, -120(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L63
	leaq	16+_ZTVN6icu_6719CharsetRecog_euc_jpE(%rip), %rax
	movq	%rax, (%r12)
.L63:
	movl	$16, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L64
	movq	%r12, (%rax)
	movb	$1, 8(%rax)
.L64:
	movl	$8, %edi
	movq	%rax, -112(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L65
	leaq	16+_ZTVN6icu_6719CharsetRecog_euc_krE(%rip), %rax
	movq	%rax, (%r12)
.L65:
	movl	$16, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L66
	movq	%r12, (%rax)
	movb	$1, 8(%rax)
.L66:
	movl	$8, %edi
	movq	%rax, -104(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L67
	leaq	16+_ZTVN6icu_6717CharsetRecog_big5E(%rip), %rax
	movq	%rax, (%r12)
.L67:
	movl	$16, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L68
	movq	%r12, (%rax)
	movb	$1, 8(%rax)
.L68:
	movl	$8, %edi
	movq	%rax, -96(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L69
	leaq	16+_ZTVN6icu_6719CharsetRecog_2022JPE(%rip), %rax
	movq	%rax, (%r12)
.L69:
	movl	$16, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L70
	movq	%r12, (%rax)
	movb	$1, 8(%rax)
.L70:
	movl	$8, %edi
	movq	%rax, -88(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L71
	leaq	16+_ZTVN6icu_6719CharsetRecog_2022KRE(%rip), %rax
	movq	%rax, (%r12)
.L71:
	movl	$16, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L72
	movq	%r12, (%rax)
	movb	$1, 8(%rax)
.L72:
	movl	$8, %edi
	movq	%rax, -80(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L73
	leaq	16+_ZTVN6icu_6719CharsetRecog_2022CNE(%rip), %rax
	movq	%rax, (%r12)
.L73:
	movl	$16, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L74
	movq	%r12, (%rax)
	movb	$1, 8(%rax)
.L74:
	movl	$8, %edi
	movq	%rax, -72(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L75
	movq	$0, (%rax)
	movq	%rax, %rdi
	call	_ZN6icu_6717CharsetRecog_sbcsC2Ev@PLT
	leaq	16+_ZTVN6icu_6726CharsetRecog_IBM424_he_rtlE(%rip), %rax
	movq	%rax, (%r12)
.L75:
	movl	$16, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L76
	movq	%r12, (%rax)
	movb	$0, 8(%rax)
.L76:
	movl	$8, %edi
	movq	%rax, -64(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L77
	movq	$0, (%rax)
	movq	%rax, %rdi
	call	_ZN6icu_6717CharsetRecog_sbcsC2Ev@PLT
	leaq	16+_ZTVN6icu_6726CharsetRecog_IBM424_he_ltrE(%rip), %rax
	movq	%rax, (%r12)
.L77:
	movl	$16, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L78
	movq	%r12, (%rax)
	movb	$0, 8(%rax)
.L78:
	movl	$8, %edi
	movq	%rax, -56(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L79
	movq	$0, (%rax)
	movq	%rax, %rdi
	call	_ZN6icu_6717CharsetRecog_sbcsC2Ev@PLT
	leaq	16+_ZTVN6icu_6726CharsetRecog_IBM420_ar_rtlE(%rip), %rax
	movq	%rax, (%r12)
.L79:
	movl	$16, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L80
	movq	%r12, (%rax)
	movb	$0, 8(%rax)
.L80:
	movl	$8, %edi
	movq	%rax, -48(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L81
	movq	$0, (%rax)
	movq	%rax, %rdi
	call	_ZN6icu_6717CharsetRecog_sbcsC2Ev@PLT
	leaq	16+_ZTVN6icu_6726CharsetRecog_IBM420_ar_ltrE(%rip), %rax
	movq	%rax, (%r12)
.L81:
	movl	$16, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L82
	movq	%r12, (%rax)
	movb	$0, 8(%rax)
.L82:
	movl	$224, %edi
	movq	%rax, -40(%rbp)
	call	uprv_malloc_67@PLT
	movq	%rax, _ZL14fCSRecognizers(%rip)
	testq	%rax, %rax
	je	.L258
	movdqa	-256(%rbp), %xmm0
	movdqa	-240(%rbp), %xmm1
	movl	$28, _ZL19fCSRecognizers_size(%rip)
	leaq	-32(%rbp), %rdx
	movdqa	-224(%rbp), %xmm2
	movdqa	-208(%rbp), %xmm3
	movdqa	-192(%rbp), %xmm4
	movdqa	-176(%rbp), %xmm5
	movups	%xmm0, (%rax)
	movups	%xmm1, 16(%rax)
	movdqa	-160(%rbp), %xmm6
	movdqa	-144(%rbp), %xmm7
	movups	%xmm2, 32(%rax)
	movdqa	-128(%rbp), %xmm0
	movdqa	-112(%rbp), %xmm1
	movups	%xmm3, 48(%rax)
	movdqa	-96(%rbp), %xmm2
	movdqa	-80(%rbp), %xmm3
	movups	%xmm4, 64(%rax)
	movdqa	-64(%rbp), %xmm4
	movups	%xmm5, 80(%rax)
	movdqa	-48(%rbp), %xmm5
	movups	%xmm6, 96(%rax)
	movups	%xmm7, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm1, 144(%rax)
	movups	%xmm2, 160(%rax)
	movups	%xmm3, 176(%rax)
	movups	%xmm4, 192(%rax)
	movups	%xmm5, 208(%rax)
	leaq	-256(%rbp), %rax
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L85:
	addq	$8, %rax
	cmpq	%rdx, %rax
	je	.L26
.L86:
	cmpq	$0, (%rax)
	jne	.L85
	addq	$8, %rax
	movl	$7, (%rbx)
	cmpq	%rdx, %rax
	jne	.L86
.L26:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L259
	addq	$240, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L258:
	.cfi_restore_state
	movl	$7, (%rbx)
	jmp	.L26
.L259:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2773:
	.size	initRecognizers, .-initRecognizers
	.p2align 4
	.type	charsetMatchComparator, @function
charsetMatchComparator:
.LFB2772:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdx), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	call	_ZNK6icu_6712CharsetMatch13getConfidenceEv@PLT
	movq	(%r12), %rdi
	movl	%eax, %ebx
	call	_ZNK6icu_6712CharsetMatch13getConfidenceEv@PLT
	subl	%eax, %ebx
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2772:
	.size	charsetMatchComparator, .-charsetMatchComparator
	.p2align 4
	.type	enumNext, @function
enumNext:
.LFB2914:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	_ZL19fCSRecognizers_size(%rip), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	8(%rdi), %rcx
	movq	%rsi, %rbx
	movslq	(%rcx), %rdx
	cmpl	%eax, %edx
	jge	.L263
	cmpb	$0, 4(%rcx)
	movq	%rdi, %r13
	jne	.L293
	movq	8(%rcx), %r14
	testq	%r14, %r14
	je	.L291
	.p2align 4,,10
	.p2align 3
.L267:
	movslq	%edx, %rdx
	cmpb	$0, (%r14,%rdx)
	je	.L294
	movq	_ZL14fCSRecognizers(%rip), %rax
	movq	(%rax,%rdx,8), %rax
	movq	(%rax), %rdi
	movq	(%rdi), %rax
	call	*(%rax)
	movq	8(%r13), %rcx
	movq	%rax, %r12
	movl	(%rcx), %eax
	leal	1(%rax), %edx
	movl	%edx, (%rcx)
	testq	%r12, %r12
	jne	.L272
	movl	_ZL19fCSRecognizers_size(%rip), %eax
	cmpl	%eax, %edx
	jl	.L267
	.p2align 4,,10
	.p2align 3
.L263:
	xorl	%r12d, %r12d
	testq	%rbx, %rbx
	je	.L262
.L278:
	xorl	%r12d, %r12d
	xorl	%eax, %eax
.L277:
	movl	%eax, (%rbx)
.L262:
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L293:
	.cfi_restore_state
	movq	_ZL14fCSRecognizers(%rip), %rax
	movq	(%rax,%rdx,8), %rax
	movq	(%rax), %rdi
	movq	(%rdi), %rax
	call	*(%rax)
	movq	%rax, %r12
	movq	8(%r13), %rax
	addl	$1, (%rax)
	testq	%rbx, %rbx
	je	.L262
	testq	%r12, %r12
	je	.L278
	movq	%r12, %rdi
	call	strlen@PLT
	jmp	.L277
	.p2align 4,,10
	.p2align 3
.L291:
	movq	_ZL14fCSRecognizers(%rip), %rsi
.L280:
	movslq	%edx, %rdx
	movq	(%rsi,%rdx,8), %rdx
	cmpb	$0, 8(%rdx)
	je	.L295
	movq	(%rdx), %rdi
	movq	(%rdi), %rax
	call	*(%rax)
	movq	8(%r13), %rcx
	movq	%rax, %r12
	movl	(%rcx), %eax
	leal	1(%rax), %edx
	movl	%edx, (%rcx)
	testq	%r12, %r12
	jne	.L272
	movl	_ZL19fCSRecognizers_size(%rip), %eax
	cmpl	%eax, %edx
	jl	.L291
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L294:
	movl	(%rcx), %esi
	leal	1(%rsi), %edx
	movl	%edx, (%rcx)
	cmpl	%eax, %edx
	jl	.L267
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L295:
	movl	(%rcx), %edi
	leal	1(%rdi), %edx
	movl	%edx, (%rcx)
	cmpl	%eax, %edx
	jl	.L280
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L272:
	testq	%rbx, %rbx
	je	.L262
	movq	%r12, %rdi
	call	strlen@PLT
	jmp	.L277
	.cfi_endproc
.LFE2914:
	.size	enumNext, .-enumNext
	.p2align 4
	.type	enumCount, @function
enumCount:
.LFB2913:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movl	_ZL19fCSRecognizers_size(%rip), %r8d
	cmpb	$0, 4(%rax)
	jne	.L296
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	je	.L298
	testl	%r8d, %r8d
	jle	.L338
	leal	-1(%r8), %eax
	cmpl	$14, %eax
	jbe	.L322
	movl	%r8d, %edx
	pxor	%xmm1, %xmm1
	pxor	%xmm5, %xmm5
	movq	%rcx, %rax
	shrl	$4, %edx
	movdqa	.LC0(%rip), %xmm6
	pxor	%xmm4, %xmm4
	salq	$4, %rdx
	addq	%rcx, %rdx
	.p2align 4,,10
	.p2align 3
.L301:
	movdqu	(%rax), %xmm0
	movdqa	%xmm5, %xmm3
	addq	$16, %rax
	pcmpeqb	%xmm5, %xmm0
	pandn	%xmm6, %xmm0
	pcmpgtb	%xmm0, %xmm3
	movdqa	%xmm0, %xmm2
	punpcklbw	%xmm3, %xmm2
	punpckhbw	%xmm3, %xmm0
	movdqa	%xmm4, %xmm3
	pcmpgtw	%xmm2, %xmm3
	movdqa	%xmm2, %xmm7
	punpcklwd	%xmm3, %xmm7
	punpckhwd	%xmm3, %xmm2
	movdqa	%xmm0, %xmm3
	paddd	%xmm7, %xmm1
	paddd	%xmm2, %xmm1
	movdqa	%xmm4, %xmm2
	pcmpgtw	%xmm0, %xmm2
	punpcklwd	%xmm2, %xmm3
	punpckhwd	%xmm2, %xmm0
	paddd	%xmm3, %xmm1
	paddd	%xmm0, %xmm1
	cmpq	%rdx, %rax
	jne	.L301
	movdqa	%xmm1, %xmm0
	movl	%r8d, %edx
	psrldq	$8, %xmm0
	andl	$-16, %edx
	paddd	%xmm1, %xmm0
	movdqa	%xmm0, %xmm1
	psrldq	$4, %xmm1
	paddd	%xmm1, %xmm0
	movd	%xmm0, %eax
	cmpl	%edx, %r8d
	je	.L337
.L299:
	movslq	%edx, %rsi
	cmpb	$1, (%rcx,%rsi)
	leal	1(%rdx), %esi
	sbbl	$-1, %eax
	cmpl	%r8d, %esi
	jge	.L337
	movslq	%esi, %rsi
	cmpb	$1, (%rcx,%rsi)
	leal	2(%rdx), %esi
	sbbl	$-1, %eax
	cmpl	%esi, %r8d
	jle	.L337
	movslq	%esi, %rsi
	cmpb	$1, (%rcx,%rsi)
	leal	3(%rdx), %esi
	sbbl	$-1, %eax
	cmpl	%esi, %r8d
	jle	.L337
	movslq	%esi, %rsi
	cmpb	$1, (%rcx,%rsi)
	leal	4(%rdx), %esi
	sbbl	$-1, %eax
	cmpl	%esi, %r8d
	jle	.L337
	movslq	%esi, %rsi
	cmpb	$1, (%rcx,%rsi)
	leal	5(%rdx), %esi
	sbbl	$-1, %eax
	cmpl	%esi, %r8d
	jle	.L337
	movslq	%esi, %rsi
	cmpb	$1, (%rcx,%rsi)
	leal	6(%rdx), %esi
	sbbl	$-1, %eax
	cmpl	%esi, %r8d
	jle	.L337
	movslq	%esi, %rsi
	cmpb	$1, (%rcx,%rsi)
	leal	7(%rdx), %esi
	sbbl	$-1, %eax
	cmpl	%esi, %r8d
	jle	.L337
	movslq	%esi, %rsi
	cmpb	$1, (%rcx,%rsi)
	leal	8(%rdx), %esi
	sbbl	$-1, %eax
	cmpl	%esi, %r8d
	jle	.L337
	movslq	%esi, %rsi
	cmpb	$1, (%rcx,%rsi)
	leal	9(%rdx), %esi
	sbbl	$-1, %eax
	cmpl	%esi, %r8d
	jle	.L337
	movslq	%esi, %rsi
	cmpb	$1, (%rcx,%rsi)
	leal	10(%rdx), %esi
	sbbl	$-1, %eax
	cmpl	%esi, %r8d
	jle	.L337
	movslq	%esi, %rsi
	cmpb	$1, (%rcx,%rsi)
	leal	11(%rdx), %esi
	sbbl	$-1, %eax
	cmpl	%esi, %r8d
	jle	.L337
	movslq	%esi, %rsi
	cmpb	$1, (%rcx,%rsi)
	leal	12(%rdx), %esi
	sbbl	$-1, %eax
	cmpl	%esi, %r8d
	jle	.L337
	movslq	%esi, %rsi
	cmpb	$1, (%rcx,%rsi)
	leal	13(%rdx), %esi
	sbbl	$-1, %eax
	cmpl	%esi, %r8d
	jle	.L337
	movslq	%esi, %rsi
	cmpb	$1, (%rcx,%rsi)
	sbbl	$-1, %eax
	addl	$14, %edx
	cmpl	%edx, %r8d
	jle	.L337
	movslq	%edx, %rdx
	leal	1(%rax), %r8d
	cmpb	$0, (%rcx,%rdx)
	cmove	%eax, %r8d
.L296:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L337:
	movl	%eax, %r8d
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L298:
	testl	%r8d, %r8d
	jle	.L338
	movq	_ZL14fCSRecognizers(%rip), %rdx
	leal	-1(%r8), %ecx
	xorl	%r8d, %r8d
	leaq	8(%rdx), %rax
	leaq	(%rax,%rcx,8), %rcx
	jmp	.L320
	.p2align 4,,10
	.p2align 3
.L341:
	addq	$8, %rax
.L320:
	movq	(%rdx), %rdx
	cmpb	$1, 8(%rdx)
	movq	%rax, %rdx
	sbbl	$-1, %r8d
	cmpq	%rcx, %rax
	jne	.L341
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L338:
	xorl	%r8d, %r8d
	jmp	.L296
.L322:
	xorl	%edx, %edx
	xorl	%eax, %eax
	jmp	.L299
	.cfi_endproc
.LFE2913:
	.size	enumCount, .-enumCount
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715CharsetDetector14setRecognizersER10UErrorCode
	.type	_ZN6icu_6715CharsetDetector14setRecognizersER10UErrorCode, @function
_ZN6icu_6715CharsetDetector14setRecognizersER10UErrorCode:
.LFB2897:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	testl	%eax, %eax
	jle	.L357
	ret
	.p2align 4,,10
	.p2align 3
.L357:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	_ZL22gCSRecognizersInitOnce(%rip), %eax
	cmpl	$2, %eax
	jne	.L358
.L345:
	movl	4+_ZL22gCSRecognizersInitOnce(%rip), %eax
	testl	%eax, %eax
	jle	.L342
	movl	%eax, (%rbx)
.L342:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L358:
	.cfi_restore_state
	leaq	_ZL22gCSRecognizersInitOnce(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L345
	movq	%rbx, %rdi
	call	initRecognizers
	movl	(%rbx), %eax
	leaq	_ZL22gCSRecognizersInitOnce(%rip), %rdi
	movl	%eax, 4+_ZL22gCSRecognizersInitOnce(%rip)
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
	.cfi_endproc
.LFE2897:
	.size	_ZN6icu_6715CharsetDetector14setRecognizersER10UErrorCode, .-_ZN6icu_6715CharsetDetector14setRecognizersER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715CharsetDetectorC2ER10UErrorCode
	.type	_ZN6icu_6715CharsetDetectorC2ER10UErrorCode, @function
_ZN6icu_6715CharsetDetectorC2ER10UErrorCode:
.LFB2899:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	movl	$56, %edi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L360
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_679InputTextC1ER10UErrorCode@PLT
.L360:
	movl	(%r14), %esi
	xorl	%ecx, %ecx
	movq	%rbx, 0(%r13)
	movq	$0, 8(%r13)
	movl	$0, 16(%r13)
	movw	%cx, 20(%r13)
	movq	$0, 24(%r13)
	testl	%esi, %esi
	jle	.L380
.L359:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L380:
	.cfi_restore_state
	movl	_ZL22gCSRecognizersInitOnce(%rip), %eax
	cmpl	$2, %eax
	je	.L363
	leaq	_ZL22gCSRecognizersInitOnce(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	jne	.L381
.L363:
	movl	4+_ZL22gCSRecognizersInitOnce(%rip), %eax
	testl	%eax, %eax
	jle	.L364
	movl	%eax, (%r14)
	jmp	.L359
	.p2align 4,,10
	.p2align 3
.L381:
	movq	%r14, %rdi
	call	initRecognizers
	movl	(%r14), %eax
	leaq	_ZL22gCSRecognizersInitOnce(%rip), %rdi
	movl	%eax, 4+_ZL22gCSRecognizersInitOnce(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L364:
	movl	(%r14), %edx
	testl	%edx, %edx
	jg	.L359
	movslq	_ZL19fCSRecognizers_size(%rip), %rdi
	salq	$3, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, 8(%r13)
	testq	%rax, %rax
	je	.L379
	movl	_ZL19fCSRecognizers_size(%rip), %eax
	testl	%eax, %eax
	jle	.L359
	xorl	%r12d, %r12d
	jmp	.L368
	.p2align 4,,10
	.p2align 3
.L382:
	movq	%rbx, %rdi
	call	_ZN6icu_6712CharsetMatchC1Ev@PLT
	movq	8(%r13), %rax
	movq	%rbx, (%rax,%r12,8)
	addq	$1, %r12
	cmpl	%r12d, _ZL19fCSRecognizers_size(%rip)
	jle	.L359
.L368:
	movl	$32, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rbx
	leaq	0(,%r12,8), %rax
	testq	%rbx, %rbx
	jne	.L382
	movq	8(%r13), %rdx
	movq	$0, (%rdx,%rax)
.L379:
	movl	$7, (%r14)
	jmp	.L359
	.cfi_endproc
.LFE2899:
	.size	_ZN6icu_6715CharsetDetectorC2ER10UErrorCode, .-_ZN6icu_6715CharsetDetectorC2ER10UErrorCode
	.globl	_ZN6icu_6715CharsetDetectorC1ER10UErrorCode
	.set	_ZN6icu_6715CharsetDetectorC1ER10UErrorCode,_ZN6icu_6715CharsetDetectorC2ER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715CharsetDetectorD2Ev
	.type	_ZN6icu_6715CharsetDetectorD2Ev, @function
_ZN6icu_6715CharsetDetectorD2Ev:
.LFB2902:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	(%rdi), %r13
	testq	%r13, %r13
	je	.L384
	movq	%r13, %rdi
	call	_ZN6icu_679InputTextD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L384:
	movl	_ZL19fCSRecognizers_size(%rip), %eax
	movq	8(%r12), %r8
	testl	%eax, %eax
	jle	.L385
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L389:
	movq	(%r8,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.L386
	call	_ZN6icu_677UMemorydlEPv@PLT
	movl	_ZL19fCSRecognizers_size(%rip), %eax
	addq	$1, %rbx
	movq	8(%r12), %r8
	cmpl	%ebx, %eax
	jg	.L389
.L385:
	movq	%r8, %rdi
	call	uprv_free_67@PLT
	movq	24(%r12), %rdi
	testq	%rdi, %rdi
	je	.L383
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uprv_free_67@PLT
	.p2align 4,,10
	.p2align 3
.L386:
	.cfi_restore_state
	addq	$1, %rbx
	cmpl	%ebx, %eax
	jg	.L389
	jmp	.L385
	.p2align 4,,10
	.p2align 3
.L383:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2902:
	.size	_ZN6icu_6715CharsetDetectorD2Ev, .-_ZN6icu_6715CharsetDetectorD2Ev
	.globl	_ZN6icu_6715CharsetDetectorD1Ev
	.set	_ZN6icu_6715CharsetDetectorD1Ev,_ZN6icu_6715CharsetDetectorD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715CharsetDetector7setTextEPKci
	.type	_ZN6icu_6715CharsetDetector7setTextEPKci, @function
_ZN6icu_6715CharsetDetector7setTextEPKci:
.LFB2904:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	call	_ZN6icu_679InputText7setTextEPKci@PLT
	movb	$1, 21(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2904:
	.size	_ZN6icu_6715CharsetDetector7setTextEPKci, .-_ZN6icu_6715CharsetDetector7setTextEPKci
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715CharsetDetector16setStripTagsFlagEa
	.type	_ZN6icu_6715CharsetDetector16setStripTagsFlagEa, @function
_ZN6icu_6715CharsetDetector16setStripTagsFlagEa:
.LFB2905:
	.cfi_startproc
	endbr64
	movzbl	20(%rdi), %eax
	movb	$1, 21(%rdi)
	movb	%sil, 20(%rdi)
	ret
	.cfi_endproc
.LFE2905:
	.size	_ZN6icu_6715CharsetDetector16setStripTagsFlagEa, .-_ZN6icu_6715CharsetDetector16setStripTagsFlagEa
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715CharsetDetector16getStripTagsFlagEv
	.type	_ZNK6icu_6715CharsetDetector16getStripTagsFlagEv, @function
_ZNK6icu_6715CharsetDetector16getStripTagsFlagEv:
.LFB2906:
	.cfi_startproc
	endbr64
	movzbl	20(%rdi), %eax
	ret
	.cfi_endproc
.LFE2906:
	.size	_ZNK6icu_6715CharsetDetector16getStripTagsFlagEv, .-_ZNK6icu_6715CharsetDetector16getStripTagsFlagEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715CharsetDetector19setDeclaredEncodingEPKci
	.type	_ZNK6icu_6715CharsetDetector19setDeclaredEncodingEPKci, @function
_ZNK6icu_6715CharsetDetector19setDeclaredEncodingEPKci:
.LFB2907:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	jmp	_ZN6icu_679InputText19setDeclaredEncodingEPKci@PLT
	.cfi_endproc
.LFE2907:
	.size	_ZNK6icu_6715CharsetDetector19setDeclaredEncodingEPKci, .-_ZNK6icu_6715CharsetDetector19setDeclaredEncodingEPKci
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715CharsetDetector18getDetectableCountEv
	.type	_ZN6icu_6715CharsetDetector18getDetectableCountEv, @function
_ZN6icu_6715CharsetDetector18getDetectableCountEv:
.LFB2908:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movl	$0, -12(%rbp)
	movl	_ZL22gCSRecognizersInitOnce(%rip), %eax
	cmpl	$2, %eax
	jne	.L411
.L402:
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	movl	_ZL19fCSRecognizers_size(%rip), %eax
	jne	.L412
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L411:
	.cfi_restore_state
	leaq	_ZL22gCSRecognizersInitOnce(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L402
	leaq	-12(%rbp), %rdi
	call	initRecognizers
	movl	-12(%rbp), %eax
	leaq	_ZL22gCSRecognizersInitOnce(%rip), %rdi
	movl	%eax, 4+_ZL22gCSRecognizersInitOnce(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
	jmp	.L402
.L412:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2908:
	.size	_ZN6icu_6715CharsetDetector18getDetectableCountEv, .-_ZN6icu_6715CharsetDetector18getDetectableCountEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715CharsetDetector6detectER10UErrorCode
	.type	_ZN6icu_6715CharsetDetector6detectER10UErrorCode, @function
_ZN6icu_6715CharsetDetector6detectER10UErrorCode:
.LFB2909:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	call	_ZNK6icu_679InputText5isSetEv@PLT
	testb	%al, %al
	je	.L427
	cmpb	$0, 21(%rbx)
	jne	.L416
	movl	16(%rbx), %esi
.L417:
	testl	%esi, %esi
	jle	.L424
	movq	8(%rbx), %rax
	movq	(%rax), %rax
.L413:
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L416:
	.cfi_restore_state
	movsbl	20(%rbx), %esi
	movq	(%rbx), %rdi
	xorl	%r12d, %r12d
	call	_ZN6icu_679InputText10MungeInputEa@PLT
	movl	_ZL19fCSRecognizers_size(%rip), %ecx
	movl	$0, 16(%rbx)
	xorl	%esi, %esi
	testl	%ecx, %ecx
	jle	.L428
	.p2align 4,,10
	.p2align 3
.L418:
	movq	_ZL14fCSRecognizers(%rip), %rax
	movslq	%esi, %rsi
	movq	(%rax,%r12,8), %rax
	movq	(%rax), %rdi
	movq	8(%rbx), %rax
	movq	(%rax,%rsi,8), %rdx
	movq	(%rdi), %rax
	movq	(%rbx), %rsi
	call	*16(%rax)
	testb	%al, %al
	jne	.L419
	addq	$1, %r12
	cmpl	%r12d, _ZL19fCSRecognizers_size(%rip)
	movl	16(%rbx), %esi
	jg	.L418
	cmpl	$1, %esi
	jg	.L429
.L422:
	movb	$0, 21(%rbx)
	jmp	.L417
	.p2align 4,,10
	.p2align 3
.L419:
	movl	16(%rbx), %eax
	addq	$1, %r12
	cmpl	%r12d, _ZL19fCSRecognizers_size(%rip)
	leal	1(%rax), %esi
	movl	%esi, 16(%rbx)
	jg	.L418
	cmpl	$1, %esi
	jle	.L422
.L429:
	subq	$8, %rsp
	movq	8(%rbx), %rdi
	movl	$8, %edx
	xorl	%r8d, %r8d
	pushq	%r13
	movl	$1, %r9d
	leaq	charsetMatchComparator(%rip), %rcx
	call	uprv_sortArray_67@PLT
	popq	%rax
	movl	16(%rbx), %esi
	movb	$0, 21(%rbx)
	popq	%rdx
	jmp	.L417
	.p2align 4,,10
	.p2align 3
.L427:
	movl	$2, 0(%r13)
	leaq	-24(%rbp), %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L428:
	.cfi_restore_state
	movb	$0, 21(%rbx)
	xorl	%eax, %eax
	jmp	.L413
.L424:
	xorl	%eax, %eax
	jmp	.L413
	.cfi_endproc
.LFE2909:
	.size	_ZN6icu_6715CharsetDetector6detectER10UErrorCode, .-_ZN6icu_6715CharsetDetector6detectER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715CharsetDetector9detectAllERiR10UErrorCode
	.type	_ZN6icu_6715CharsetDetector9detectAllERiR10UErrorCode, @function
_ZN6icu_6715CharsetDetector9detectAllERiR10UErrorCode:
.LFB2910:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	call	_ZNK6icu_679InputText5isSetEv@PLT
	testb	%al, %al
	je	.L443
	cmpb	$0, 21(%rbx)
	jne	.L433
	movl	16(%rbx), %esi
	movq	8(%rbx), %rax
.L434:
	movl	%esi, (%r12)
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L433:
	.cfi_restore_state
	movsbl	20(%rbx), %esi
	movq	(%rbx), %rdi
	xorl	%r14d, %r14d
	call	_ZN6icu_679InputText10MungeInputEa@PLT
	movl	_ZL19fCSRecognizers_size(%rip), %edi
	movl	$0, 16(%rbx)
	xorl	%esi, %esi
	testl	%edi, %edi
	jle	.L444
	.p2align 4,,10
	.p2align 3
.L435:
	movq	_ZL14fCSRecognizers(%rip), %rax
	movslq	%esi, %rsi
	movq	(%rax,%r14,8), %rax
	movq	(%rax), %rdi
	movq	8(%rbx), %rax
	movq	(%rax,%rsi,8), %rdx
	movq	(%rdi), %rax
	movq	(%rbx), %rsi
	call	*16(%rax)
	testb	%al, %al
	jne	.L437
	addq	$1, %r14
	cmpl	%r14d, _ZL19fCSRecognizers_size(%rip)
	movl	16(%rbx), %esi
	jg	.L435
	movq	8(%rbx), %rax
	cmpl	$1, %esi
	jg	.L445
.L436:
	movb	$0, 21(%rbx)
	jmp	.L434
	.p2align 4,,10
	.p2align 3
.L437:
	movl	16(%rbx), %eax
	addq	$1, %r14
	cmpl	%r14d, _ZL19fCSRecognizers_size(%rip)
	leal	1(%rax), %esi
	movl	%esi, 16(%rbx)
	jg	.L435
	movq	8(%rbx), %rax
	cmpl	$1, %esi
	jle	.L436
.L445:
	subq	$8, %rsp
	movl	$8, %edx
	movq	%rax, %rdi
	xorl	%r8d, %r8d
	pushq	%r13
	leaq	charsetMatchComparator(%rip), %rcx
	movl	$1, %r9d
	call	uprv_sortArray_67@PLT
	popq	%rdx
	movl	16(%rbx), %esi
	movb	$0, 21(%rbx)
	movq	8(%rbx), %rax
	popq	%rcx
	jmp	.L434
	.p2align 4,,10
	.p2align 3
.L443:
	movl	$2, 0(%r13)
	leaq	-32(%rbp), %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L444:
	.cfi_restore_state
	movq	8(%rbx), %rax
	jmp	.L436
	.cfi_endproc
.LFE2910:
	.size	_ZN6icu_6715CharsetDetector9detectAllERiR10UErrorCode, .-_ZN6icu_6715CharsetDetector9detectAllERiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715CharsetDetector20setDetectableCharsetEPKcaR10UErrorCode
	.type	_ZN6icu_6715CharsetDetector20setDetectableCharsetEPKcaR10UErrorCode, @function
_ZN6icu_6715CharsetDetector20setDetectableCharsetEPKcaR10UErrorCode:
.LFB2911:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, -52(%rbp)
	movl	(%rcx), %edx
	testl	%edx, %edx
	jg	.L446
	movl	_ZL19fCSRecognizers_size(%rip), %eax
	movq	%rcx, %r13
	testl	%eax, %eax
	jle	.L449
	movq	%rdi, %r14
	movq	%rsi, %r12
	xorl	%ebx, %ebx
	jmp	.L454
	.p2align 4,,10
	.p2align 3
.L450:
	addq	$1, %rbx
	cmpl	%ebx, _ZL19fCSRecognizers_size(%rip)
	jle	.L449
.L454:
	movq	_ZL14fCSRecognizers(%rip), %rax
	movq	(%rax,%rbx,8), %r15
	movq	(%r15), %rdi
	movq	(%rdi), %rax
	call	*(%rax)
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L450
	movq	24(%r14), %rcx
	movzbl	-52(%rbp), %eax
	cmpb	%al, 8(%r15)
	je	.L453
	testq	%rcx, %rcx
	je	.L469
.L456:
	movzbl	-52(%rbp), %eax
	movb	%al, (%rcx,%rbx)
.L446:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L449:
	.cfi_restore_state
	movl	$1, 0(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L469:
	.cfi_restore_state
	movslq	_ZL19fCSRecognizers_size(%rip), %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, 24(%r14)
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L470
	movl	_ZL19fCSRecognizers_size(%rip), %edx
	testl	%edx, %edx
	jle	.L456
	movq	_ZL14fCSRecognizers(%rip), %rdi
	movq	(%rdi), %rcx
	movzbl	8(%rcx), %ecx
	movb	%cl, (%rax)
	cmpl	$1, %edx
	je	.L458
	leal	-2(%rdx), %esi
	movl	$1, %eax
	addq	$2, %rsi
	.p2align 4,,10
	.p2align 3
.L459:
	movq	(%rdi,%rax,8), %rdx
	movzbl	8(%rdx), %ecx
	movq	24(%r14), %rdx
	movb	%cl, (%rdx,%rax)
	addq	$1, %rax
	cmpq	%rax, %rsi
	jne	.L459
.L458:
	movq	24(%r14), %rcx
	.p2align 4,,10
	.p2align 3
.L453:
	testq	%rcx, %rcx
	jne	.L456
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L470:
	.cfi_restore_state
	movl	$7, 0(%r13)
	jmp	.L446
	.cfi_endproc
.LFE2911:
	.size	_ZN6icu_6715CharsetDetector20setDetectableCharsetEPKcaR10UErrorCode, .-_ZN6icu_6715CharsetDetector20setDetectableCharsetEPKcaR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715CharsetDetector24getAllDetectableCharsetsER10UErrorCode
	.type	_ZN6icu_6715CharsetDetector24getAllDetectableCharsetsER10UErrorCode, @function
_ZN6icu_6715CharsetDetector24getAllDetectableCharsetsER10UErrorCode:
.LFB2916:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movl	(%rdi), %edx
	testl	%edx, %edx
	jle	.L485
.L472:
	xorl	%r12d, %r12d
.L471:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L485:
	.cfi_restore_state
	movl	_ZL22gCSRecognizersInitOnce(%rip), %eax
	movq	%rdi, %rbx
	cmpl	$2, %eax
	jne	.L486
.L473:
	movl	4+_ZL22gCSRecognizersInitOnce(%rip), %eax
	testl	%eax, %eax
	jle	.L474
	movl	%eax, (%rbx)
	jmp	.L472
	.p2align 4,,10
	.p2align 3
.L486:
	leaq	_ZL22gCSRecognizersInitOnce(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L473
	movq	%rbx, %rdi
	call	initRecognizers
	movl	(%rbx), %eax
	leaq	_ZL22gCSRecognizersInitOnce(%rip), %rdi
	movl	%eax, 4+_ZL22gCSRecognizersInitOnce(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L474:
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L472
	movl	$56, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L487
	movdqa	_ZL17gCSDetEnumeration(%rip), %xmm1
	movdqa	16+_ZL17gCSDetEnumeration(%rip), %xmm2
	movl	$16, %edi
	movdqa	32+_ZL17gCSDetEnumeration(%rip), %xmm3
	movups	%xmm1, (%rax)
	movups	%xmm2, 16(%rax)
	movups	%xmm3, 32(%rax)
	movq	48+_ZL17gCSDetEnumeration(%rip), %rax
	movq	%rax, 48(%r12)
	call	uprv_malloc_67@PLT
	movq	%rax, 8(%r12)
	testq	%rax, %rax
	je	.L488
	pxor	%xmm0, %xmm0
	movups	%xmm0, (%rax)
	movb	$1, 4(%rax)
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L488:
	.cfi_restore_state
	movl	$7, (%rbx)
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	uprv_free_67@PLT
	jmp	.L471
.L487:
	movl	$7, (%rbx)
	jmp	.L471
	.cfi_endproc
.LFE2916:
	.size	_ZN6icu_6715CharsetDetector24getAllDetectableCharsetsER10UErrorCode, .-_ZN6icu_6715CharsetDetector24getAllDetectableCharsetsER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715CharsetDetector21getDetectableCharsetsER10UErrorCode
	.type	_ZNK6icu_6715CharsetDetector21getDetectableCharsetsER10UErrorCode, @function
_ZNK6icu_6715CharsetDetector21getDetectableCharsetsER10UErrorCode:
.LFB2917:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L489
	movq	%rdi, %r13
	movl	$56, %edi
	movq	%rsi, %rbx
	call	uprv_malloc_67@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L495
	movdqa	_ZL17gCSDetEnumeration(%rip), %xmm1
	movdqa	16+_ZL17gCSDetEnumeration(%rip), %xmm2
	movl	$16, %edi
	movdqa	32+_ZL17gCSDetEnumeration(%rip), %xmm3
	movups	%xmm1, (%rax)
	movups	%xmm2, 16(%rax)
	movups	%xmm3, 32(%rax)
	movq	48+_ZL17gCSDetEnumeration(%rip), %rax
	movq	%rax, 48(%r12)
	call	uprv_malloc_67@PLT
	movq	%rax, 8(%r12)
	testq	%rax, %rax
	je	.L496
	movq	24(%r13), %rdx
	pxor	%xmm0, %xmm0
	movups	%xmm0, (%rax)
	movq	%rdx, 8(%rax)
.L489:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L495:
	.cfi_restore_state
	movl	$7, (%rbx)
	jmp	.L489
.L496:
	movl	$7, (%rbx)
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	uprv_free_67@PLT
	jmp	.L489
	.cfi_endproc
.LFE2917:
	.size	_ZNK6icu_6715CharsetDetector21getDetectableCharsetsER10UErrorCode, .-_ZNK6icu_6715CharsetDetector21getDetectableCharsetsER10UErrorCode
	.section	.data.rel.ro,"aw"
	.align 32
	.type	_ZL17gCSDetEnumeration, @object
	.size	_ZL17gCSDetEnumeration, 56
_ZL17gCSDetEnumeration:
	.quad	0
	.quad	0
	.quad	enumClose
	.quad	enumCount
	.quad	uenum_unextDefault_67
	.quad	enumNext
	.quad	enumReset
	.local	_ZL19fCSRecognizers_size
	.comm	_ZL19fCSRecognizers_size,4,4
	.local	_ZL22gCSRecognizersInitOnce
	.comm	_ZL22gCSRecognizersInitOnce,8,8
	.local	_ZL14fCSRecognizers
	.comm	_ZL14fCSRecognizers,8,8
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
