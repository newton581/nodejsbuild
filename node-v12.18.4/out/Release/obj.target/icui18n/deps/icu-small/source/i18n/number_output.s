	.file	"number_output.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number15FormattedNumber8toStringER10UErrorCode
	.type	_ZNK6icu_676number15FormattedNumber8toStringER10UErrorCode, @function
_ZNK6icu_676number15FormattedNumber8toStringER10UErrorCode:
.LFB2857:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movl	(%rdx), %ecx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L7
	movq	8(%rsi), %r8
	testq	%r8, %r8
	je	.L9
	movq	(%r8), %rax
	movq	%r8, %rsi
	call	*16(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L9:
	movl	16(%rsi), %eax
	movl	%eax, (%rdx)
	.p2align 4,,10
	.p2align 3
.L7:
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	movl	$2, %eax
	movw	%ax, 8(%r12)
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
.L1:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L10
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L10:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2857:
	.size	_ZNK6icu_676number15FormattedNumber8toStringER10UErrorCode, .-_ZNK6icu_676number15FormattedNumber8toStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number15FormattedNumber12toTempStringER10UErrorCode
	.type	_ZNK6icu_676number15FormattedNumber12toTempStringER10UErrorCode, @function
_ZNK6icu_676number15FormattedNumber12toTempStringER10UErrorCode:
.LFB2858:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movl	(%rdx), %ecx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L17
	movq	8(%rsi), %r8
	testq	%r8, %r8
	je	.L18
	movq	(%r8), %rax
	movq	%r8, %rsi
	call	*24(%rax)
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L18:
	movl	16(%rsi), %eax
	movl	%eax, (%rdx)
	.p2align 4,,10
	.p2align 3
.L17:
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	movl	$2, %eax
	movw	%ax, 8(%r12)
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
.L11:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L19
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L19:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2858:
	.size	_ZNK6icu_676number15FormattedNumber12toTempStringER10UErrorCode, .-_ZNK6icu_676number15FormattedNumber12toTempStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number15FormattedNumber8appendToERNS_10AppendableER10UErrorCode
	.type	_ZNK6icu_676number15FormattedNumber8appendToERNS_10AppendableER10UErrorCode, @function
_ZNK6icu_676number15FormattedNumber8appendToERNS_10AppendableER10UErrorCode:
.LFB2859:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L20
	movq	8(%rdi), %r8
	testq	%r8, %r8
	je	.L24
	movq	(%r8), %rax
	movq	%r8, %rdi
	movq	32(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L24:
	movl	16(%rdi), %eax
	movl	%eax, (%rdx)
.L20:
	movq	%rsi, %rax
	ret
	.cfi_endproc
.LFE2859:
	.size	_ZNK6icu_676number15FormattedNumber8appendToERNS_10AppendableER10UErrorCode, .-_ZNK6icu_676number15FormattedNumber8appendToERNS_10AppendableER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number15FormattedNumber12nextPositionERNS_24ConstrainedFieldPositionER10UErrorCode
	.type	_ZNK6icu_676number15FormattedNumber12nextPositionERNS_24ConstrainedFieldPositionER10UErrorCode, @function
_ZNK6icu_676number15FormattedNumber12nextPositionERNS_24ConstrainedFieldPositionER10UErrorCode:
.LFB2860:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L25
	movq	8(%rdi), %r8
	testq	%r8, %r8
	je	.L29
	movq	(%r8), %rax
	movq	%r8, %rdi
	movq	40(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L29:
	movl	16(%rdi), %eax
	movl	%eax, (%rdx)
.L25:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2860:
	.size	_ZNK6icu_676number15FormattedNumber12nextPositionERNS_24ConstrainedFieldPositionER10UErrorCode, .-_ZNK6icu_676number15FormattedNumber12nextPositionERNS_24ConstrainedFieldPositionER10UErrorCode
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev:
.LFB3337:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3337:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei:
.LFB3340:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	cmpl	$40, %esi
	jg	.L43
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L31
	cmpb	$0, 12(%rbx)
	jne	.L44
.L35:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L31:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L35
	.cfi_endproc
.LFE3340:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EED2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev:
.LFB3343:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L47
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3343:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5EOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_:
.LFB3346:
	.cfi_startproc
	endbr64
	movzbl	12(%rsi), %eax
	movq	(%rsi), %r8
	movslq	8(%rsi), %rdx
	movb	%al, 12(%rdi)
	leaq	13(%rsi), %rax
	movq	%r8, (%rdi)
	movl	%edx, 8(%rdi)
	cmpq	%rax, %r8
	je	.L50
	movq	%rax, (%rsi)
	movl	$40, 8(%rsi)
	movb	$0, 12(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	leaq	13(%rdi), %r9
	movq	%r8, %rsi
	movq	%r9, (%rdi)
	movq	%r9, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE3346:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_:
.LFB3348:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 12(%rdi)
	movq	%rsi, %rbx
	jne	.L56
.L52:
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movzbl	12(%rbx), %eax
	movb	%al, 12(%r12)
	movq	(%rbx), %rsi
	leaq	13(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L57
	movq	%rsi, (%r12)
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L57:
	leaq	13(%r12), %rdi
	movslq	8(%rbx), %rdx
	movq	%rdi, (%r12)
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3348:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv:
.LFB3349:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE3349:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv:
.LFB3350:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3350:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv:
.LFB3351:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	addq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3351:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3352:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3352:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3353:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3353:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci:
.LFB3354:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L73
	testl	%edx, %edx
	jle	.L73
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 12(%rdi)
	jne	.L76
.L65:
	movq	%rsi, (%rbx)
	movl	%edx, 8(%rbx)
	movb	$0, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L76:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	%edx, -28(%rbp)
	movq	%rsi, -24(%rbp)
	call	uprv_free_67@PLT
	movl	-28(%rbp), %edx
	movq	-24(%rbp), %rsi
	jmp	.L65
	.cfi_endproc
.LFE3354:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii:
.LFB3355:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L80
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movl	%esi, %r13d
	movl	%edx, %r12d
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L80
	testl	%r12d, %r12d
	jg	.L87
	cmpb	$0, 12(%rbx)
	jne	.L88
.L82:
	movq	%r14, (%rbx)
	movq	%r14, %rax
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L87:
	.cfi_restore_state
	cmpl	%r12d, 8(%rbx)
	cmovle	8(%rbx), %r12d
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	je	.L82
.L88:
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L80:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3355:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, .-_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi:
.LFB3356:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 12(%rdi)
	movq	%rdi, %rbx
	je	.L90
	movq	(%rdi), %r8
.L91:
	leaq	13(%rbx), %rax
	movl	%r12d, 0(%r13)
	movq	%rax, (%rbx)
	movq	%r8, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L90:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L94
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	cmovg	%eax, %r12d
	movslq	%r12d, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L94
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L94:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3356:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, .-_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv:
.LFB3357:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L101
	ret
	.p2align 4,,10
	.p2align 3
.L101:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3357:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv:
.LFB3358:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3358:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_:
.LFB3359:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3359:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_:
.LFB3360:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3360:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5ERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_:
.LFB3362:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3362:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_:
.LFB3364:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3364:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number15FormattedNumberC2EOS1_
	.type	_ZN6icu_676number15FormattedNumberC2EOS1_, @function
_ZN6icu_676number15FormattedNumberC2EOS1_:
.LFB2850:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_676number15FormattedNumberE(%rip), %rax
	movq	%rax, (%rdi)
	movq	8(%rsi), %rax
	movq	$0, 8(%rsi)
	movq	%rax, 8(%rdi)
	movl	16(%rsi), %eax
	movl	$27, 16(%rsi)
	movl	%eax, 16(%rdi)
	ret
	.cfi_endproc
.LFE2850:
	.size	_ZN6icu_676number15FormattedNumberC2EOS1_, .-_ZN6icu_676number15FormattedNumberC2EOS1_
	.globl	_ZN6icu_676number15FormattedNumberC1EOS1_
	.set	_ZN6icu_676number15FormattedNumberC1EOS1_,_ZN6icu_676number15FormattedNumberC2EOS1_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number15FormattedNumber15toDecimalNumberERNS_8ByteSinkER10UErrorCode
	.type	_ZNK6icu_676number15FormattedNumber15toDecimalNumberERNS_8ByteSinkER10UErrorCode, @function
_ZNK6icu_676number15FormattedNumber15toDecimalNumberERNS_8ByteSinkER10UErrorCode:
.LFB2861:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L108
	cmpq	$0, 8(%rdi)
	movq	%rdi, %rbx
	movq	%rdx, %r12
	je	.L114
	leaq	-144(%rbp), %r14
	movq	%rsi, %r13
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl6DecNumC1Ev@PLT
	movq	8(%rbx), %rdi
	movq	%r12, %rdx
	movq	%r14, %rsi
	addq	$152, %rdi
	call	_ZNK6icu_676number4impl15DecimalQuantity8toDecNumERNS1_6DecNumER10UErrorCode@PLT
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZNK6icu_676number4impl6DecNum8toStringERNS_8ByteSinkER10UErrorCode@PLT
	cmpb	$0, -132(%rbp)
	jne	.L115
.L108:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L116
	addq	$112, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L114:
	.cfi_restore_state
	movl	16(%rdi), %eax
	movl	%eax, (%rdx)
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L115:
	movq	-144(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L108
.L116:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2861:
	.size	_ZNK6icu_676number15FormattedNumber15toDecimalNumberERNS_8ByteSinkER10UErrorCode, .-_ZNK6icu_676number15FormattedNumber15toDecimalNumberERNS_8ByteSinkER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number15FormattedNumber24getAllFieldPositionsImplERNS_28FieldPositionIteratorHandlerER10UErrorCode
	.type	_ZNK6icu_676number15FormattedNumber24getAllFieldPositionsImplERNS_28FieldPositionIteratorHandlerER10UErrorCode, @function
_ZNK6icu_676number15FormattedNumber24getAllFieldPositionsImplERNS_28FieldPositionIteratorHandlerER10UErrorCode:
.LFB2865:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L117
	movq	8(%rdi), %r8
	testq	%r8, %r8
	je	.L120
	movq	%r8, %rdi
	jmp	_ZNK6icu_6731FormattedValueStringBuilderImpl20getAllFieldPositionsERNS_28FieldPositionIteratorHandlerER10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L117:
	ret
	.p2align 4,,10
	.p2align 3
.L120:
	movl	16(%rdi), %eax
	movl	%eax, (%rdx)
	ret
	.cfi_endproc
.LFE2865:
	.size	_ZNK6icu_676number15FormattedNumber24getAllFieldPositionsImplERNS_28FieldPositionIteratorHandlerER10UErrorCode, .-_ZNK6icu_676number15FormattedNumber24getAllFieldPositionsImplERNS_28FieldPositionIteratorHandlerER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number15FormattedNumber18getDecimalQuantityERNS0_4impl15DecimalQuantityER10UErrorCode
	.type	_ZNK6icu_676number15FormattedNumber18getDecimalQuantityERNS0_4impl15DecimalQuantityER10UErrorCode, @function
_ZNK6icu_676number15FormattedNumber18getDecimalQuantityERNS0_4impl15DecimalQuantityER10UErrorCode:
.LFB2866:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	movq	%rsi, %r8
	testl	%eax, %eax
	jg	.L121
	movq	8(%rdi), %rsi
	testq	%rsi, %rsi
	je	.L124
	addq	$152, %rsi
	movq	%r8, %rdi
	jmp	_ZN6icu_676number4impl15DecimalQuantityaSERKS2_@PLT
	.p2align 4,,10
	.p2align 3
.L121:
	ret
	.p2align 4,,10
	.p2align 3
.L124:
	movl	16(%rdi), %eax
	movl	%eax, (%rdx)
	ret
	.cfi_endproc
.LFE2866:
	.size	_ZNK6icu_676number15FormattedNumber18getDecimalQuantityERNS0_4impl15DecimalQuantityER10UErrorCode, .-_ZNK6icu_676number15FormattedNumber18getDecimalQuantityERNS0_4impl15DecimalQuantityER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl20UFormattedNumberDataD2Ev
	.type	_ZN6icu_676number4impl20UFormattedNumberDataD2Ev, @function
_ZN6icu_676number4impl20UFormattedNumberDataD2Ev:
.LFB2868:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_676number4impl20UFormattedNumberDataE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	152(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -152(%rdi)
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6731FormattedValueStringBuilderImplD2Ev@PLT
	.cfi_endproc
.LFE2868:
	.size	_ZN6icu_676number4impl20UFormattedNumberDataD2Ev, .-_ZN6icu_676number4impl20UFormattedNumberDataD2Ev
	.globl	_ZN6icu_676number4impl20UFormattedNumberDataD1Ev
	.set	_ZN6icu_676number4impl20UFormattedNumberDataD1Ev,_ZN6icu_676number4impl20UFormattedNumberDataD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl20UFormattedNumberDataD0Ev
	.type	_ZN6icu_676number4impl20UFormattedNumberDataD0Ev, @function
_ZN6icu_676number4impl20UFormattedNumberDataD0Ev:
.LFB2870:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_676number4impl20UFormattedNumberDataE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	152(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -152(%rdi)
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6731FormattedValueStringBuilderImplD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2870:
	.size	_ZN6icu_676number4impl20UFormattedNumberDataD0Ev, .-_ZN6icu_676number4impl20UFormattedNumberDataD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number15FormattedNumberD2Ev
	.type	_ZN6icu_676number15FormattedNumberD2Ev, @function
_ZN6icu_676number15FormattedNumberD2Ev:
.LFB2853:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_676number15FormattedNumberE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	8(%rdi), %r13
	movq	%rdi, %r12
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L130
	movq	0(%r13), %rax
	leaq	_ZN6icu_676number4impl20UFormattedNumberDataD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L131
	leaq	16+_ZTVN6icu_676number4impl20UFormattedNumberDataE(%rip), %rax
	leaq	152(%r13), %rdi
	movq	%rax, 0(%r13)
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6731FormattedValueStringBuilderImplD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L130:
	movq	$0, 8(%r12)
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6714FormattedValueD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L131:
	.cfi_restore_state
	movq	%r13, %rdi
	call	*%rax
	jmp	.L130
	.cfi_endproc
.LFE2853:
	.size	_ZN6icu_676number15FormattedNumberD2Ev, .-_ZN6icu_676number15FormattedNumberD2Ev
	.globl	_ZN6icu_676number15FormattedNumberD1Ev
	.set	_ZN6icu_676number15FormattedNumberD1Ev,_ZN6icu_676number15FormattedNumberD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number15FormattedNumberaSEOS1_
	.type	_ZN6icu_676number15FormattedNumberaSEOS1_, @function
_ZN6icu_676number15FormattedNumberaSEOS1_:
.LFB2856:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %r13
	testq	%r13, %r13
	je	.L137
	movq	0(%r13), %rax
	leaq	_ZN6icu_676number4impl20UFormattedNumberDataD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L138
	leaq	16+_ZTVN6icu_676number4impl20UFormattedNumberDataE(%rip), %rax
	leaq	152(%r13), %rdi
	movq	%rax, 0(%r13)
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6731FormattedValueStringBuilderImplD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L137:
	movq	8(%rbx), %rax
	movq	%rax, 8(%r12)
	movl	16(%rbx), %eax
	movq	$0, 8(%rbx)
	movl	%eax, 16(%r12)
	movq	%r12, %rax
	movl	$27, 16(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L138:
	.cfi_restore_state
	movq	%r13, %rdi
	call	*%rax
	jmp	.L137
	.cfi_endproc
.LFE2856:
	.size	_ZN6icu_676number15FormattedNumberaSEOS1_, .-_ZN6icu_676number15FormattedNumberaSEOS1_
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number15FormattedNumberD0Ev
	.type	_ZN6icu_676number15FormattedNumberD0Ev, @function
_ZN6icu_676number15FormattedNumberD0Ev:
.LFB2855:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_676number15FormattedNumberE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	8(%rdi), %r13
	movq	%rdi, %r12
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L144
	movq	0(%r13), %rax
	leaq	_ZN6icu_676number4impl20UFormattedNumberDataD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L145
	leaq	16+_ZTVN6icu_676number4impl20UFormattedNumberDataE(%rip), %rax
	leaq	152(%r13), %rdi
	movq	%rax, 0(%r13)
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6731FormattedValueStringBuilderImplD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L144:
	movq	$0, 8(%r12)
	movq	%r12, %rdi
	call	_ZN6icu_6714FormattedValueD2Ev@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L145:
	.cfi_restore_state
	movq	%r13, %rdi
	call	*%rax
	jmp	.L144
	.cfi_endproc
.LFE2855:
	.size	_ZN6icu_676number15FormattedNumberD0Ev, .-_ZN6icu_676number15FormattedNumberD0Ev
	.weak	_ZTSN6icu_677UMemoryE
	.section	.rodata._ZTSN6icu_677UMemoryE,"aG",@progbits,_ZTSN6icu_677UMemoryE,comdat
	.align 16
	.type	_ZTSN6icu_677UMemoryE, @object
	.size	_ZTSN6icu_677UMemoryE, 18
_ZTSN6icu_677UMemoryE:
	.string	"N6icu_677UMemoryE"
	.weak	_ZTIN6icu_677UMemoryE
	.section	.data.rel.ro._ZTIN6icu_677UMemoryE,"awG",@progbits,_ZTIN6icu_677UMemoryE,comdat
	.align 8
	.type	_ZTIN6icu_677UMemoryE, @object
	.size	_ZTIN6icu_677UMemoryE, 16
_ZTIN6icu_677UMemoryE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN6icu_677UMemoryE
	.weak	_ZTSN6icu_676number15FormattedNumberE
	.section	.rodata._ZTSN6icu_676number15FormattedNumberE,"aG",@progbits,_ZTSN6icu_676number15FormattedNumberE,comdat
	.align 32
	.type	_ZTSN6icu_676number15FormattedNumberE, @object
	.size	_ZTSN6icu_676number15FormattedNumberE, 34
_ZTSN6icu_676number15FormattedNumberE:
	.string	"N6icu_676number15FormattedNumberE"
	.weak	_ZTIN6icu_676number15FormattedNumberE
	.section	.data.rel.ro._ZTIN6icu_676number15FormattedNumberE,"awG",@progbits,_ZTIN6icu_676number15FormattedNumberE,comdat
	.align 8
	.type	_ZTIN6icu_676number15FormattedNumberE, @object
	.size	_ZTIN6icu_676number15FormattedNumberE, 56
_ZTIN6icu_676number15FormattedNumberE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN6icu_676number15FormattedNumberE
	.long	0
	.long	2
	.quad	_ZTIN6icu_677UMemoryE
	.quad	2
	.quad	_ZTIN6icu_6714FormattedValueE
	.quad	2
	.weak	_ZTSN6icu_676number4impl20UFormattedNumberDataE
	.section	.rodata._ZTSN6icu_676number4impl20UFormattedNumberDataE,"aG",@progbits,_ZTSN6icu_676number4impl20UFormattedNumberDataE,comdat
	.align 32
	.type	_ZTSN6icu_676number4impl20UFormattedNumberDataE, @object
	.size	_ZTSN6icu_676number4impl20UFormattedNumberDataE, 44
_ZTSN6icu_676number4impl20UFormattedNumberDataE:
	.string	"N6icu_676number4impl20UFormattedNumberDataE"
	.weak	_ZTIN6icu_676number4impl20UFormattedNumberDataE
	.section	.data.rel.ro._ZTIN6icu_676number4impl20UFormattedNumberDataE,"awG",@progbits,_ZTIN6icu_676number4impl20UFormattedNumberDataE,comdat
	.align 8
	.type	_ZTIN6icu_676number4impl20UFormattedNumberDataE, @object
	.size	_ZTIN6icu_676number4impl20UFormattedNumberDataE, 24
_ZTIN6icu_676number4impl20UFormattedNumberDataE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_676number4impl20UFormattedNumberDataE
	.quad	_ZTIN6icu_6731FormattedValueStringBuilderImplE
	.weak	_ZTVN6icu_676number15FormattedNumberE
	.section	.data.rel.ro.local._ZTVN6icu_676number15FormattedNumberE,"awG",@progbits,_ZTVN6icu_676number15FormattedNumberE,comdat
	.align 8
	.type	_ZTVN6icu_676number15FormattedNumberE, @object
	.size	_ZTVN6icu_676number15FormattedNumberE, 64
_ZTVN6icu_676number15FormattedNumberE:
	.quad	0
	.quad	_ZTIN6icu_676number15FormattedNumberE
	.quad	_ZN6icu_676number15FormattedNumberD1Ev
	.quad	_ZN6icu_676number15FormattedNumberD0Ev
	.quad	_ZNK6icu_676number15FormattedNumber8toStringER10UErrorCode
	.quad	_ZNK6icu_676number15FormattedNumber12toTempStringER10UErrorCode
	.quad	_ZNK6icu_676number15FormattedNumber8appendToERNS_10AppendableER10UErrorCode
	.quad	_ZNK6icu_676number15FormattedNumber12nextPositionERNS_24ConstrainedFieldPositionER10UErrorCode
	.weak	_ZTVN6icu_676number4impl20UFormattedNumberDataE
	.section	.data.rel.ro._ZTVN6icu_676number4impl20UFormattedNumberDataE,"awG",@progbits,_ZTVN6icu_676number4impl20UFormattedNumberDataE,comdat
	.align 8
	.type	_ZTVN6icu_676number4impl20UFormattedNumberDataE, @object
	.size	_ZTVN6icu_676number4impl20UFormattedNumberDataE, 64
_ZTVN6icu_676number4impl20UFormattedNumberDataE:
	.quad	0
	.quad	_ZTIN6icu_676number4impl20UFormattedNumberDataE
	.quad	_ZN6icu_676number4impl20UFormattedNumberDataD1Ev
	.quad	_ZN6icu_676number4impl20UFormattedNumberDataD0Ev
	.quad	_ZNK6icu_6731FormattedValueStringBuilderImpl8toStringER10UErrorCode
	.quad	_ZNK6icu_6731FormattedValueStringBuilderImpl12toTempStringER10UErrorCode
	.quad	_ZNK6icu_6731FormattedValueStringBuilderImpl8appendToERNS_10AppendableER10UErrorCode
	.quad	_ZNK6icu_6731FormattedValueStringBuilderImpl12nextPositionERNS_24ConstrainedFieldPositionER10UErrorCode
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
