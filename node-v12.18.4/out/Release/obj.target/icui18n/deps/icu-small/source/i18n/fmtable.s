	.file	"fmtable.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6711Formattable17getDynamicClassIDEv
	.type	_ZNK6icu_6711Formattable17getDynamicClassIDEv, @function
_ZNK6icu_6711Formattable17getDynamicClassIDEv:
.LFB3051:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6711Formattable16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3051:
	.size	_ZNK6icu_6711Formattable17getDynamicClassIDEv, .-_ZNK6icu_6711Formattable17getDynamicClassIDEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev:
.LFB3396:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3396:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei:
.LFB3399:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	cmpl	$40, %esi
	jg	.L16
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L4
	cmpb	$0, 12(%rbx)
	jne	.L17
.L8:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L4:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L8
	.cfi_endproc
.LFE3399:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EED2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev:
.LFB3402:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L20
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3402:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5EOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_:
.LFB3405:
	.cfi_startproc
	endbr64
	movzbl	12(%rsi), %eax
	movq	(%rsi), %r8
	movslq	8(%rsi), %rdx
	movb	%al, 12(%rdi)
	leaq	13(%rsi), %rax
	movq	%r8, (%rdi)
	movl	%edx, 8(%rdi)
	cmpq	%rax, %r8
	je	.L23
	movq	%rax, (%rsi)
	movl	$40, 8(%rsi)
	movb	$0, 12(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	leaq	13(%rdi), %r9
	movq	%r8, %rsi
	movq	%r9, (%rdi)
	movq	%r9, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE3405:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_:
.LFB3407:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 12(%rdi)
	movq	%rsi, %rbx
	jne	.L29
.L25:
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movzbl	12(%rbx), %eax
	movb	%al, 12(%r12)
	movq	(%rbx), %rsi
	leaq	13(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L30
	movq	%rsi, (%r12)
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L30:
	leaq	13(%r12), %rdi
	movslq	8(%rbx), %rdx
	movq	%rdi, (%r12)
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3407:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv:
.LFB3408:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE3408:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv:
.LFB3409:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3409:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv:
.LFB3410:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	addq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3410:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3411:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3411:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3412:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3412:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci:
.LFB3413:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L46
	testl	%edx, %edx
	jle	.L46
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 12(%rdi)
	jne	.L49
.L38:
	movq	%rsi, (%rbx)
	movl	%edx, 8(%rbx)
	movb	$0, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	%edx, -28(%rbp)
	movq	%rsi, -24(%rbp)
	call	uprv_free_67@PLT
	movl	-28(%rbp), %edx
	movq	-24(%rbp), %rsi
	jmp	.L38
	.cfi_endproc
.LFE3413:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii:
.LFB3414:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L53
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movl	%esi, %r13d
	movl	%edx, %r12d
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L53
	testl	%r12d, %r12d
	jg	.L60
	cmpb	$0, 12(%rbx)
	jne	.L61
.L55:
	movq	%r14, (%rbx)
	movq	%r14, %rax
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L60:
	.cfi_restore_state
	cmpl	%r12d, 8(%rbx)
	cmovle	8(%rbx), %r12d
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	je	.L55
.L61:
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L53:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3414:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, .-_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi:
.LFB3415:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 12(%rdi)
	movq	%rdi, %rbx
	je	.L63
	movq	(%rdi), %r8
.L64:
	leaq	13(%rbx), %rax
	movl	%r12d, 0(%r13)
	movq	%rax, (%rbx)
	movq	%r8, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L63:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L67
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	cmovg	%eax, %r12d
	movslq	%r12d, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L67
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L67:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3415:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, .-_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv:
.LFB3416:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L74
	ret
	.p2align 4,,10
	.p2align 3
.L74:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3416:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv:
.LFB3417:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3417:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_:
.LFB3418:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3418:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_:
.LFB3419:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3419:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5ERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_:
.LFB3421:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3421:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_:
.LFB3423:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3423:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711Formattable16getStaticClassIDEv
	.type	_ZN6icu_6711Formattable16getStaticClassIDEv, @function
_ZN6icu_6711Formattable16getStaticClassIDEv:
.LFB3050:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6711Formattable16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3050:
	.size	_ZN6icu_6711Formattable16getStaticClassIDEv, .-_ZN6icu_6711Formattable16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711Formattable4initEv
	.type	_ZN6icu_6711Formattable4initEv, @function
_ZN6icu_6711Formattable4initEv:
.LFB3057:
	.cfi_startproc
	endbr64
	movq	$0, 8(%rdi)
	pxor	%xmm0, %xmm0
	addq	$48, %rdi
	movl	$2, -8(%rdi)
	movups	%xmm0, -24(%rdi)
	jmp	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	.cfi_endproc
.LFE3057:
	.size	_ZN6icu_6711Formattable4initEv, .-_ZN6icu_6711Formattable4initEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711FormattableC2Ev
	.type	_ZN6icu_6711FormattableC2Ev, @function
_ZN6icu_6711FormattableC2Ev:
.LFB3059:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711FormattableE(%rip), %rax
	movq	$0, 8(%rdi)
	pxor	%xmm0, %xmm0
	addq	$48, %rdi
	movq	%rax, -48(%rdi)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, (%rdi)
	movl	$2, %eax
	movw	%ax, 8(%rdi)
	movl	$2, -8(%rdi)
	movups	%xmm0, -24(%rdi)
	jmp	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	.cfi_endproc
.LFE3059:
	.size	_ZN6icu_6711FormattableC2Ev, .-_ZN6icu_6711FormattableC2Ev
	.globl	_ZN6icu_6711FormattableC1Ev
	.set	_ZN6icu_6711FormattableC1Ev,_ZN6icu_6711FormattableC2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711FormattableC2EdNS0_6ISDATEE
	.type	_ZN6icu_6711FormattableC2EdNS0_6ISDATEE, @function
_ZN6icu_6711FormattableC2EdNS0_6ISDATEE:
.LFB3062:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6711FormattableE(%rip), %rax
	pxor	%xmm1, %xmm1
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	48(%rdi), %rdi
	subq	$24, %rsp
	movq	%rax, -48(%rdi)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, (%rdi)
	movl	$2, %eax
	movw	%ax, 8(%rdi)
	movq	$0, -40(%rdi)
	movl	$2, -8(%rdi)
	movups	%xmm1, -24(%rdi)
	movsd	%xmm0, -24(%rbp)
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	movsd	-24(%rbp), %xmm0
	movl	$0, 40(%rbx)
	movsd	%xmm0, 8(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3062:
	.size	_ZN6icu_6711FormattableC2EdNS0_6ISDATEE, .-_ZN6icu_6711FormattableC2EdNS0_6ISDATEE
	.globl	_ZN6icu_6711FormattableC1EdNS0_6ISDATEE
	.set	_ZN6icu_6711FormattableC1EdNS0_6ISDATEE,_ZN6icu_6711FormattableC2EdNS0_6ISDATEE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711FormattableC2Ed
	.type	_ZN6icu_6711FormattableC2Ed, @function
_ZN6icu_6711FormattableC2Ed:
.LFB3065:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6711FormattableE(%rip), %rax
	pxor	%xmm1, %xmm1
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	48(%rdi), %rdi
	subq	$24, %rsp
	movq	%rax, -48(%rdi)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, (%rdi)
	movl	$2, %eax
	movw	%ax, 8(%rdi)
	movq	$0, -40(%rdi)
	movl	$2, -8(%rdi)
	movups	%xmm1, -24(%rdi)
	movsd	%xmm0, -24(%rbp)
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	movsd	-24(%rbp), %xmm0
	movl	$1, 40(%rbx)
	movsd	%xmm0, 8(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3065:
	.size	_ZN6icu_6711FormattableC2Ed, .-_ZN6icu_6711FormattableC2Ed
	.globl	_ZN6icu_6711FormattableC1Ed
	.set	_ZN6icu_6711FormattableC1Ed,_ZN6icu_6711FormattableC2Ed
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711FormattableC2Ei
	.type	_ZN6icu_6711FormattableC2Ei, @function
_ZN6icu_6711FormattableC2Ei:
.LFB3068:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6711FormattableE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movslq	%esi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	leaq	48(%rdi), %rdi
	movq	%rax, -48(%rdi)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, (%rdi)
	movl	$2, %eax
	movw	%ax, 8(%rdi)
	movq	$0, -40(%rdi)
	movl	$2, -8(%rdi)
	movups	%xmm0, -24(%rdi)
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	movq	%r12, 8(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3068:
	.size	_ZN6icu_6711FormattableC2Ei, .-_ZN6icu_6711FormattableC2Ei
	.globl	_ZN6icu_6711FormattableC1Ei
	.set	_ZN6icu_6711FormattableC1Ei,_ZN6icu_6711FormattableC2Ei
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711FormattableC2El
	.type	_ZN6icu_6711FormattableC2El, @function
_ZN6icu_6711FormattableC2El:
.LFB3071:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6711FormattableE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	leaq	48(%rdi), %rdi
	movq	%rax, -48(%rdi)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, (%rdi)
	movl	$2, %eax
	movw	%ax, 8(%rdi)
	movq	$0, -40(%rdi)
	movl	$2, -8(%rdi)
	movups	%xmm0, -24(%rdi)
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	movq	%r12, 8(%rbx)
	movl	$5, 40(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3071:
	.size	_ZN6icu_6711FormattableC2El, .-_ZN6icu_6711FormattableC2El
	.globl	_ZN6icu_6711FormattableC1El
	.set	_ZN6icu_6711FormattableC1El,_ZN6icu_6711FormattableC2El
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711FormattableC2ERKNS_13UnicodeStringE
	.type	_ZN6icu_6711FormattableC2ERKNS_13UnicodeStringE, @function
_ZN6icu_6711FormattableC2ERKNS_13UnicodeStringE:
.LFB3077:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6711FormattableE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	addq	$48, %rdi
	subq	$8, %rsp
	movq	%rax, -48(%rdi)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, (%rdi)
	movl	$2, %eax
	movw	%ax, 8(%rdi)
	movq	$0, -40(%rdi)
	movl	$2, -8(%rdi)
	movups	%xmm0, -24(%rdi)
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	movl	$3, 40(%rbx)
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L92
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
.L92:
	movq	%r12, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3077:
	.size	_ZN6icu_6711FormattableC2ERKNS_13UnicodeStringE, .-_ZN6icu_6711FormattableC2ERKNS_13UnicodeStringE
	.globl	_ZN6icu_6711FormattableC1ERKNS_13UnicodeStringE
	.set	_ZN6icu_6711FormattableC1ERKNS_13UnicodeStringE,_ZN6icu_6711FormattableC2ERKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711FormattableC2EPNS_13UnicodeStringE
	.type	_ZN6icu_6711FormattableC2EPNS_13UnicodeStringE, @function
_ZN6icu_6711FormattableC2EPNS_13UnicodeStringE:
.LFB3080:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6711FormattableE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	leaq	48(%rdi), %rdi
	movq	%rax, -48(%rdi)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, (%rdi)
	movl	$2, %eax
	movw	%ax, 8(%rdi)
	movq	$0, -40(%rdi)
	movl	$2, -8(%rdi)
	movups	%xmm0, -24(%rdi)
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	movq	%r12, 8(%rbx)
	movl	$3, 40(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3080:
	.size	_ZN6icu_6711FormattableC2EPNS_13UnicodeStringE, .-_ZN6icu_6711FormattableC2EPNS_13UnicodeStringE
	.globl	_ZN6icu_6711FormattableC1EPNS_13UnicodeStringE
	.set	_ZN6icu_6711FormattableC1EPNS_13UnicodeStringE,_ZN6icu_6711FormattableC2EPNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711FormattableC2EPNS_7UObjectE
	.type	_ZN6icu_6711FormattableC2EPNS_7UObjectE, @function
_ZN6icu_6711FormattableC2EPNS_7UObjectE:
.LFB3083:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6711FormattableE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	leaq	48(%rdi), %rdi
	movq	%rax, -48(%rdi)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, (%rdi)
	movl	$2, %eax
	movw	%ax, 8(%rdi)
	movq	$0, -40(%rdi)
	movl	$2, -8(%rdi)
	movups	%xmm0, -24(%rdi)
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	movq	%r12, 8(%rbx)
	movl	$6, 40(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3083:
	.size	_ZN6icu_6711FormattableC2EPNS_7UObjectE, .-_ZN6icu_6711FormattableC2EPNS_7UObjectE
	.globl	_ZN6icu_6711FormattableC1EPNS_7UObjectE
	.set	_ZN6icu_6711FormattableC1EPNS_7UObjectE,_ZN6icu_6711FormattableC2EPNS_7UObjectE
	.section	.text.unlikely,"ax",@progbits
	.align 2
.LCOLDB0:
	.text
.LHOTB0:
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6711FormattableeqERKS0_
	.type	_ZNK6icu_6711FormattableeqERKS0_, @function
_ZNK6icu_6711FormattableeqERKS0_:
.LFB3092:
	.cfi_startproc
	endbr64
	cmpq	%rsi, %rdi
	je	.L139
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movl	40(%rdi), %eax
	cmpl	40(%rsi), %eax
	je	.L104
.L117:
	xorl	%eax, %eax
.L101:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L104:
	.cfi_restore_state
	cmpl	$6, %eax
	ja	.L120
	leaq	.L106(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L106:
	.long	.L111-.L106
	.long	.L110-.L106
	.long	.L107-.L106
	.long	.L109-.L106
	.long	.L108-.L106
	.long	.L107-.L106
	.long	.L105-.L106
	.text
.L108:
	movl	16(%rdi), %eax
	cmpl	16(%rsi), %eax
	jne	.L117
	testl	%eax, %eax
	jle	.L118
	xorl	%r13d, %r13d
	xorl	%r14d, %r14d
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L140:
	addl	$1, %r14d
	addq	$112, %r13
	cmpl	%r14d, 16(%rbx)
	jle	.L118
.L119:
	movq	8(%r12), %rsi
	movq	8(%rbx), %rdi
	addq	%r13, %rsi
	addq	%r13, %rdi
	call	_ZNK6icu_6711FormattableeqERKS0_
	testb	%al, %al
	jne	.L140
	jmp	.L117
.L105:
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L117
	movq	8(%rsi), %rsi
	testq	%rsi, %rsi
	je	.L117
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_677MeasureeqERKNS_7UObjectE@PLT
.L111:
	.cfi_restore_state
	movsd	8(%rdi), %xmm0
	ucomisd	8(%rsi), %xmm0
	movl	$0, %edx
	setnp	%al
	cmovne	%edx, %eax
	jmp	.L101
.L110:
	movsd	8(%rdi), %xmm0
	ucomisd	8(%rsi), %xmm0
	movl	$0, %edx
	setnp	%al
	cmovne	%edx, %eax
	jmp	.L101
.L107:
	movq	8(%rsi), %rax
	cmpq	%rax, 8(%rdi)
	sete	%al
	jmp	.L101
.L109:
	movq	8(%rsi), %rsi
	movq	8(%rdi), %rdi
	movswl	8(%rsi), %ecx
	movswl	8(%rdi), %edx
	movl	%ecx, %eax
	movl	%edx, %r8d
	andl	$1, %eax
	andl	$1, %r8d
	jne	.L101
	testw	%dx, %dx
	js	.L112
	sarl	$5, %edx
.L113:
	testw	%cx, %cx
	js	.L114
	sarl	$5, %ecx
.L115:
	cmpl	%edx, %ecx
	jne	.L116
	testb	$1, %al
	je	.L141
.L116:
	movl	%r8d, %eax
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L118:
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L139:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L114:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movl	12(%rsi), %ecx
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L112:
	movl	12(%rdi), %edx
	jmp	.L113
.L141:
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%r8b
	jmp	.L116
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZNK6icu_6711FormattableeqERKS0_.cold, @function
_ZNK6icu_6711FormattableeqERKS0_.cold:
.LFSB3092:
.L120:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movl	$1, %eax
	jmp	.L101
	.cfi_endproc
.LFE3092:
	.text
	.size	_ZNK6icu_6711FormattableeqERKS0_, .-_ZNK6icu_6711FormattableeqERKS0_
	.section	.text.unlikely
	.size	_ZNK6icu_6711FormattableeqERKS0_.cold, .-_ZNK6icu_6711FormattableeqERKS0_.cold
.LCOLDE0:
	.text
.LHOTE0:
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711Formattable7disposeEv
	.type	_ZN6icu_6711Formattable7disposeEv, @function
_ZN6icu_6711Formattable7disposeEv:
.LFB3097:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	40(%rdi), %eax
	cmpl	$4, %eax
	je	.L143
	cmpl	$6, %eax
	je	.L144
	cmpl	$3, %eax
	je	.L144
.L145:
	movl	$2, 40(%r12)
	movq	24(%r12), %r13
	movq	$0, 8(%r12)
	testq	%r13, %r13
	je	.L151
	cmpb	$0, 12(%r13)
	jne	.L172
.L152:
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L151:
	movq	$0, 24(%r12)
	movq	32(%r12), %rdi
	testq	%rdi, %rdi
	je	.L153
	movq	(%rdi), %rax
	call	*8(%rax)
.L153:
	movq	$0, 32(%r12)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L172:
	.cfi_restore_state
	movq	0(%r13), %rdi
	call	uprv_free_67@PLT
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L144:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L145
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L145
	.p2align 4,,10
	.p2align 3
.L143:
	movq	8(%rdi), %rax
	testq	%rax, %rax
	je	.L145
	movq	-8(%rax), %rdx
	leaq	0(,%rdx,8), %rbx
	subq	%rdx, %rbx
	salq	$4, %rbx
	addq	%rax, %rbx
	cmpq	%rbx, %rax
	je	.L146
	leaq	_ZN6icu_6711FormattableD2Ev(%rip), %r13
	leaq	16+_ZTVN6icu_6711FormattableE(%rip), %r14
	.p2align 4,,10
	.p2align 3
.L147:
	movq	-112(%rbx), %rax
	subq	$112, %rbx
	movq	(%rax), %rax
	cmpq	%r13, %rax
	jne	.L148
	movq	%r14, (%rbx)
	movq	%rbx, %rdi
	call	_ZN6icu_6711Formattable7disposeEv
	leaq	48(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	cmpq	%rbx, 8(%r12)
	jne	.L147
.L146:
	leaq	-8(%rbx), %rdi
	call	_ZN6icu_677UMemorydaEPv@PLT
	jmp	.L145
	.p2align 4,,10
	.p2align 3
.L148:
	movq	%rbx, %rdi
	call	*%rax
	cmpq	%rbx, 8(%r12)
	jne	.L147
	jmp	.L146
	.cfi_endproc
.LFE3097:
	.size	_ZN6icu_6711Formattable7disposeEv, .-_ZN6icu_6711Formattable7disposeEv
	.align 2
	.p2align 4
	.type	_ZN6icu_6711FormattableaSERKS0_.part.0, @function
_ZN6icu_6711FormattableaSERKS0_.part.0:
.LFB4276:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6711Formattable7disposeEv
	movl	40(%rbx), %eax
	movl	%eax, 40(%r12)
	cmpl	$6, %eax
	ja	.L174
	leaq	.L176(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L176:
	.long	.L181-.L176
	.long	.L181-.L176
	.long	.L177-.L176
	.long	.L179-.L176
	.long	.L178-.L176
	.long	.L177-.L176
	.long	.L175-.L176
	.text
	.p2align 4,,10
	.p2align 3
.L179:
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L182
	movq	8(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
.L182:
	movq	%r13, 8(%r12)
	.p2align 4,,10
	.p2align 3
.L174:
	cmpq	$0, 32(%rbx)
	movl	$0, -44(%rbp)
	je	.L183
	movl	$72, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L184
	movq	32(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityC1ERKS2_@PLT
.L184:
	movq	%r13, 32(%r12)
.L183:
	cmpq	$0, 24(%rbx)
	je	.L173
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L187
	leaq	13(%r13), %rdx
	movq	24(%rbx), %rax
	leaq	-44(%rbp), %rcx
	movq	%r13, %rdi
	movq	%rdx, 0(%r13)
	xorl	%edx, %edx
	movw	%dx, 12(%r13)
	movq	(%rax), %rsi
	movl	$40, 8(%r13)
	movl	56(%rax), %edx
	movl	$0, 56(%r13)
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movl	-44(%rbp), %ecx
	movq	%r13, 24(%r12)
	testl	%ecx, %ecx
	jle	.L173
	cmpb	$0, 12(%r13)
	jne	.L199
.L189:
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L190:
	movq	$0, 24(%r12)
.L173:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L200
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L181:
	.cfi_restore_state
	movsd	8(%rbx), %xmm0
	movsd	%xmm0, 8(%r12)
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L177:
	movq	8(%rbx), %rax
	movq	%rax, 8(%r12)
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L175:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	%rax, 8(%r12)
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L178:
	movl	16(%rbx), %eax
	movq	8(%rbx), %rdi
	movl	%eax, 16(%r12)
	movl	16(%rbx), %esi
	call	_ZN6icu_67L15createArrayCopyEPKNS_11FormattableEi
	movq	%rax, 8(%r12)
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L199:
	movq	0(%r13), %rdi
	call	uprv_free_67@PLT
	jmp	.L189
.L200:
	call	__stack_chk_fail@PLT
.L187:
	movq	$0, 24(%r12)
	cmpl	$0, -44(%rbp)
	jg	.L190
	jmp	.L173
	.cfi_endproc
.LFE4276:
	.size	_ZN6icu_6711FormattableaSERKS0_.part.0, .-_ZN6icu_6711FormattableaSERKS0_.part.0
	.p2align 4
	.type	_ZN6icu_67L15createArrayCopyEPKNS_11FormattableEi, @function
_ZN6icu_67L15createArrayCopyEPKNS_11FormattableEi:
.LFB3055:
	.cfi_startproc
	movabsq	$82351536043346212, %rax
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -72(%rbp)
	movq	$-1, %rdi
	movl	%edx, -52(%rbp)
	cmpq	%rax, %rdx
	ja	.L202
	leaq	0(,%rdx,8), %rdi
	subq	%rdx, %rdi
	salq	$4, %rdi
	addq	$8, %rdi
.L202:
	movq	%rdx, -64(%rbp)
	call	_ZN6icu_677UMemorynaEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L201
	movq	-64(%rbp), %rdx
	addq	$8, %r12
	leaq	16+_ZTVN6icu_6711FormattableE(%rip), %r14
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %r13
	movq	%r12, %r15
	movq	%rdx, (%rax)
	subq	$1, %rdx
	movq	%rdx, %rbx
	js	.L206
	.p2align 4,,10
	.p2align 3
.L207:
	movl	$2, %eax
	movq	%r14, (%r15)
	pxor	%xmm0, %xmm0
	leaq	48(%r15), %rdi
	movq	%r13, 48(%r15)
	subq	$1, %rbx
	addq	$112, %r15
	movw	%ax, -56(%r15)
	movq	$0, -104(%r15)
	movl	$2, -72(%r15)
	movups	%xmm0, -88(%r15)
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	cmpq	$-1, %rbx
	jne	.L207
.L206:
	movl	-52(%rbp), %edx
	testl	%edx, %edx
	jle	.L201
	movl	-52(%rbp), %eax
	movq	-72(%rbp), %rbx
	movq	%r12, %r13
	leal	-1(%rax), %edx
	leaq	0(,%rdx,8), %rax
	subq	%rdx, %rax
	salq	$4, %rax
	leaq	112(%rbx,%rax), %r14
	.p2align 4,,10
	.p2align 3
.L209:
	cmpq	%r13, %rbx
	je	.L208
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6711FormattableaSERKS0_.part.0
.L208:
	addq	$112, %rbx
	addq	$112, %r13
	cmpq	%r14, %rbx
	jne	.L209
.L201:
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3055:
	.size	_ZN6icu_67L15createArrayCopyEPKNS_11FormattableEi, .-_ZN6icu_67L15createArrayCopyEPKNS_11FormattableEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711FormattableC2EPKS0_i
	.type	_ZN6icu_6711FormattableC2EPKS0_i, @function
_ZN6icu_6711FormattableC2EPKS0_i:
.LFB3086:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6711FormattableE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	leaq	48(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -48(%rdi)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, (%rdi)
	movl	$2, %eax
	movw	%ax, 8(%rdi)
	movq	$0, -40(%rdi)
	movl	$2, -8(%rdi)
	movups	%xmm0, -24(%rdi)
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	movl	$4, 40(%rbx)
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	_ZN6icu_67L15createArrayCopyEPKNS_11FormattableEi
	movl	%r12d, 16(%rbx)
	movq	%rax, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3086:
	.size	_ZN6icu_6711FormattableC2EPKS0_i, .-_ZN6icu_6711FormattableC2EPKS0_i
	.globl	_ZN6icu_6711FormattableC1EPKS0_i
	.set	_ZN6icu_6711FormattableC1EPKS0_i,_ZN6icu_6711FormattableC2EPKS0_i
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711FormattableC2ERKS0_
	.type	_ZN6icu_6711FormattableC2ERKS0_, @function
_ZN6icu_6711FormattableC2ERKS0_:
.LFB3089:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6711FormattableE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	addq	$48, %rdi
	movq	%rax, -48(%rdi)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, (%rdi)
	movl	$2, %eax
	movw	%ax, 8(%rdi)
	movq	$0, -40(%rdi)
	movl	$2, -8(%rdi)
	movups	%xmm0, -24(%rdi)
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	cmpq	%r13, %r12
	je	.L224
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6711FormattableaSERKS0_.part.0
	.p2align 4,,10
	.p2align 3
.L224:
	.cfi_restore_state
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3089:
	.size	_ZN6icu_6711FormattableC2ERKS0_, .-_ZN6icu_6711FormattableC2ERKS0_
	.globl	_ZN6icu_6711FormattableC1ERKS0_
	.set	_ZN6icu_6711FormattableC1ERKS0_,_ZN6icu_6711FormattableC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711FormattableD2Ev
	.type	_ZN6icu_6711FormattableD2Ev, @function
_ZN6icu_6711FormattableD2Ev:
.LFB3094:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6711FormattableE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6711Formattable7disposeEv
	leaq	48(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE3094:
	.size	_ZN6icu_6711FormattableD2Ev, .-_ZN6icu_6711FormattableD2Ev
	.globl	_ZN6icu_6711FormattableD1Ev
	.set	_ZN6icu_6711FormattableD1Ev,_ZN6icu_6711FormattableD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711FormattableD0Ev
	.type	_ZN6icu_6711FormattableD0Ev, @function
_ZN6icu_6711FormattableD0Ev:
.LFB3096:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6711FormattableE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6711Formattable7disposeEv
	leaq	48(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3096:
	.size	_ZN6icu_6711FormattableD0Ev, .-_ZN6icu_6711FormattableD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711FormattableC2ENS_11StringPieceER10UErrorCode
	.type	_ZN6icu_6711FormattableC2ENS_11StringPieceER10UErrorCode, @function
_ZN6icu_6711FormattableC2ENS_11StringPieceER10UErrorCode:
.LFB3074:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6711FormattableE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$48, %rdi
	subq	$8, %rsp
	movq	%rax, -48(%rdi)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, (%rdi)
	movl	$2, %eax
	movw	%ax, 8(%rdi)
	movq	$0, -40(%rdi)
	movl	$2, -8(%rdi)
	movups	%xmm0, -24(%rdi)
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	movl	(%r12), %edx
	testl	%edx, %edx
	jle	.L248
.L241:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L248:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN6icu_6711Formattable7disposeEv
	movl	$72, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L234
	movq	%rax, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityC1Ev@PLT
	movq	%r15, %rdi
	movq	%r12, %rcx
	movq	%r14, %rsi
	movq	%r13, %rdx
	call	_ZN6icu_676number4impl15DecimalQuantity14setToDecNumberENS_11StringPieceER10UErrorCode@PLT
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L247
	movq	(%rdi), %rax
	call	*8(%rax)
.L247:
	movq	%r15, 32(%rbx)
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZNK6icu_676number4impl15DecimalQuantity10fitsInLongEb@PLT
	testb	%al, %al
	je	.L237
	movq	32(%rbx), %rdi
	xorl	%esi, %esi
	call	_ZNK6icu_676number4impl15DecimalQuantity6toLongEb@PLT
	movl	$2147483648, %edx
	movq	%rax, 8(%rbx)
	addq	%rdx, %rax
	addq	$2147483647, %rdx
	cmpq	%rax, %rdx
	sbbl	%eax, %eax
	andl	$3, %eax
	addl	$2, %eax
	movl	%eax, 40(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L237:
	.cfi_restore_state
	movl	$1, 40(%rbx)
	movq	32(%rbx), %rdi
	call	_ZNK6icu_676number4impl15DecimalQuantity8toDoubleEv@PLT
	movsd	%xmm0, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L234:
	.cfi_restore_state
	xorl	%edi, %edi
	movq	%r12, %rcx
	movq	%r14, %rsi
	movq	%r13, %rdx
	call	_ZN6icu_676number4impl15DecimalQuantity14setToDecNumberENS_11StringPieceER10UErrorCode@PLT
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L241
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	$0, 32(%rbx)
	jmp	.L241
	.cfi_endproc
.LFE3074:
	.size	_ZN6icu_6711FormattableC2ENS_11StringPieceER10UErrorCode, .-_ZN6icu_6711FormattableC2ENS_11StringPieceER10UErrorCode
	.globl	_ZN6icu_6711FormattableC1ENS_11StringPieceER10UErrorCode
	.set	_ZN6icu_6711FormattableC1ENS_11StringPieceER10UErrorCode,_ZN6icu_6711FormattableC2ENS_11StringPieceER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711FormattableaSERKS0_
	.type	_ZN6icu_6711FormattableaSERKS0_, @function
_ZN6icu_6711FormattableaSERKS0_:
.LFB3091:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpq	%rsi, %rdi
	je	.L250
	movq	%rsi, %rbx
	call	_ZN6icu_6711Formattable7disposeEv
	movl	40(%rbx), %eax
	movl	%eax, 40(%r12)
	cmpl	$6, %eax
	ja	.L251
	leaq	.L253(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L253:
	.long	.L258-.L253
	.long	.L258-.L253
	.long	.L254-.L253
	.long	.L256-.L253
	.long	.L255-.L253
	.long	.L254-.L253
	.long	.L252-.L253
	.text
	.p2align 4,,10
	.p2align 3
.L256:
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L259
	movq	8(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
.L259:
	movq	%r13, 8(%r12)
.L251:
	cmpq	$0, 32(%rbx)
	movl	$0, -44(%rbp)
	je	.L260
.L279:
	movl	$72, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L261
	movq	32(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityC1ERKS2_@PLT
.L261:
	movq	%r13, 32(%r12)
.L260:
	cmpq	$0, 24(%rbx)
	je	.L250
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L264
	leaq	13(%r13), %rdx
	movq	24(%rbx), %rax
	leaq	-44(%rbp), %rcx
	movq	%r13, %rdi
	movq	%rdx, 0(%r13)
	xorl	%edx, %edx
	movw	%dx, 12(%r13)
	movq	(%rax), %rsi
	movl	$40, 8(%r13)
	movl	56(%rax), %edx
	movl	$0, 56(%r13)
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movl	-44(%rbp), %ecx
	movq	%r13, 24(%r12)
	testl	%ecx, %ecx
	jle	.L250
	cmpb	$0, 12(%r13)
	jne	.L277
.L266:
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L267:
	movq	$0, 24(%r12)
	.p2align 4,,10
	.p2align 3
.L250:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L278
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L258:
	.cfi_restore_state
	movsd	8(%rbx), %xmm0
	cmpq	$0, 32(%rbx)
	movl	$0, -44(%rbp)
	movsd	%xmm0, 8(%r12)
	jne	.L279
	jmp	.L260
	.p2align 4,,10
	.p2align 3
.L254:
	movq	8(%rbx), %rax
	movq	%rax, 8(%r12)
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L255:
	movl	16(%rbx), %eax
	movq	8(%rbx), %rdi
	movl	%eax, 16(%r12)
	movl	16(%rbx), %esi
	call	_ZN6icu_67L15createArrayCopyEPKNS_11FormattableEi
	movq	%rax, 8(%r12)
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L252:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	%rax, 8(%r12)
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L277:
	movq	0(%r13), %rdi
	call	uprv_free_67@PLT
	jmp	.L266
.L278:
	call	__stack_chk_fail@PLT
.L264:
	movq	$0, 24(%r12)
	cmpl	$0, -44(%rbp)
	jg	.L267
	jmp	.L250
	.cfi_endproc
.LFE3091:
	.size	_ZN6icu_6711FormattableaSERKS0_, .-_ZN6icu_6711FormattableaSERKS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6711Formattable5cloneEv
	.type	_ZNK6icu_6711Formattable5cloneEv, @function
_ZNK6icu_6711Formattable5cloneEv:
.LFB3098:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movl	$112, %edi
	pushq	%r12
	.cfi_offset 12, -32
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L280
	leaq	16+_ZTVN6icu_6711FormattableE(%rip), %rax
	pxor	%xmm0, %xmm0
	leaq	48(%r12), %rdi
	movq	$0, 8(%r12)
	movq	%rax, (%r12)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, 48(%r12)
	movl	$2, %eax
	movw	%ax, 56(%r12)
	movl	$2, 40(%r12)
	movups	%xmm0, 24(%r12)
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	cmpq	%r13, %r12
	je	.L280
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6711FormattableaSERKS0_.part.0
.L280:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3098:
	.size	_ZNK6icu_6711Formattable5cloneEv, .-_ZNK6icu_6711Formattable5cloneEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6711Formattable7getTypeEv
	.type	_ZNK6icu_6711Formattable7getTypeEv, @function
_ZNK6icu_6711Formattable7getTypeEv:
.LFB3099:
	.cfi_startproc
	endbr64
	movl	40(%rdi), %eax
	ret
	.cfi_endproc
.LFE3099:
	.size	_ZNK6icu_6711Formattable7getTypeEv, .-_ZNK6icu_6711Formattable7getTypeEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6711Formattable9isNumericEv
	.type	_ZNK6icu_6711Formattable9isNumericEv, @function
_ZNK6icu_6711Formattable9isNumericEv:
.LFB3100:
	.cfi_startproc
	endbr64
	movl	40(%rdi), %eax
	cmpl	$2, %eax
	ja	.L287
	testl	%eax, %eax
	setne	%al
	ret
	.p2align 4,,10
	.p2align 3
.L287:
	cmpl	$5, %eax
	sete	%al
	ret
	.cfi_endproc
.LFE3100:
	.size	_ZNK6icu_6711Formattable9isNumericEv, .-_ZNK6icu_6711Formattable9isNumericEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6711Formattable7getLongER10UErrorCode
	.type	_ZNK6icu_6711Formattable7getLongER10UErrorCode, @function
_ZNK6icu_6711Formattable7getLongER10UErrorCode:
.LFB3101:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L303
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	_ZTIN6icu_677MeasureE(%rip), %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	_ZTIN6icu_677UObjectE(%rip), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	40(%rdi), %eax
	cmpl	$5, %eax
	je	.L291
.L312:
	ja	.L292
	cmpl	$1, %eax
	je	.L293
	cmpl	$2, %eax
	jne	.L295
	movl	8(%rdi), %eax
.L289:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L292:
	.cfi_restore_state
	cmpl	$6, %eax
	jne	.L295
	movq	8(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L311
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	__dynamic_cast@PLT
	testq	%rax, %rax
	je	.L295
	movl	48(%rbx), %eax
	leaq	8(%rbx), %rdi
	cmpl	$5, %eax
	jne	.L312
	.p2align 4,,10
	.p2align 3
.L291:
	movq	8(%rdi), %rax
	cmpq	$2147483647, %rax
	jg	.L299
	cmpq	$-2147483648, %rax
	jge	.L289
.L300:
	movl	$3, (%r12)
	movl	$-2147483648, %eax
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L295:
	movl	$3, (%r12)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L293:
	.cfi_restore_state
	movsd	8(%rdi), %xmm0
	comisd	.LC1(%rip), %xmm0
	ja	.L299
	movsd	.LC2(%rip), %xmm1
	comisd	%xmm0, %xmm1
	ja	.L300
	popq	%rbx
	cvttsd2sil	%xmm0, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L299:
	.cfi_restore_state
	movl	$3, (%r12)
	movl	$2147483647, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L311:
	.cfi_restore_state
	movl	$7, (%r12)
	xorl	%eax, %eax
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L303:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3101:
	.size	_ZNK6icu_6711Formattable7getLongER10UErrorCode, .-_ZNK6icu_6711Formattable7getLongER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6711Formattable8getInt64ER10UErrorCode
	.type	_ZNK6icu_6711Formattable8getInt64ER10UErrorCode, @function
_ZNK6icu_6711Formattable8getInt64ER10UErrorCode:
.LFB3102:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L329
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	_ZTIN6icu_677MeasureE(%rip), %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	_ZTIN6icu_677UObjectE(%rip), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
.L328:
	movl	40(%rbx), %eax
	cmpl	$5, %eax
	je	.L315
	ja	.L316
	cmpl	$1, %eax
	jne	.L352
	movsd	8(%rbx), %xmm0
	comisd	.LC3(%rip), %xmm0
	ja	.L353
	movsd	.LC4(%rip), %xmm1
	comisd	%xmm0, %xmm1
	ja	.L354
	movapd	%xmm0, %xmm1
	andpd	.LC5(%rip), %xmm1
	comisd	.LC6(%rip), %xmm1
	jbe	.L324
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L324
	movl	$1, %esi
	call	_ZNK6icu_676number4impl15DecimalQuantity10fitsInLongEb@PLT
	testb	%al, %al
	je	.L326
	movq	32(%rbx), %rdi
	xorl	%esi, %esi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_676number4impl15DecimalQuantity6toLongEb@PLT
	.p2align 4,,10
	.p2align 3
.L316:
	.cfi_restore_state
	cmpl	$6, %eax
	jne	.L318
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L355
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	__dynamic_cast@PLT
	testq	%rax, %rax
	je	.L318
	addq	$8, %rbx
	jmp	.L328
	.p2align 4,,10
	.p2align 3
.L352:
	cmpl	$2, %eax
	jne	.L318
.L315:
	movq	8(%rbx), %rax
.L313:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L318:
	.cfi_restore_state
	movl	$3, (%r12)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L324:
	.cfi_restore_state
	popq	%rbx
	cvttsd2siq	%xmm0, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L353:
	.cfi_restore_state
	movl	$3, (%r12)
	movabsq	$9223372036854775807, %rax
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L326:
	movl	$3, (%r12)
	movq	32(%rbx), %rdi
	call	_ZNK6icu_676number4impl15DecimalQuantity10isNegativeEv@PLT
	movabsq	$9223372036854775807, %rdx
	testb	%al, %al
	movabsq	$-9223372036854775808, %rax
	cmove	%rdx, %rax
	jmp	.L313
.L355:
	movl	$7, (%r12)
	xorl	%eax, %eax
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L329:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	xorl	%eax, %eax
	ret
.L354:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movl	$3, (%r12)
	movabsq	$-9223372036854775808, %rax
	jmp	.L313
	.cfi_endproc
.LFE3102:
	.size	_ZNK6icu_6711Formattable8getInt64ER10UErrorCode, .-_ZNK6icu_6711Formattable8getInt64ER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6711Formattable9getDoubleER10UErrorCode
	.type	_ZNK6icu_6711Formattable9getDoubleER10UErrorCode, @function
_ZNK6icu_6711Formattable9getDoubleER10UErrorCode:
.LFB3103:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L365
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	_ZTIN6icu_677MeasureE(%rip), %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	_ZTIN6icu_677UObjectE(%rip), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
.L364:
	movl	40(%rdi), %eax
	cmpl	$5, %eax
	je	.L358
	ja	.L359
	cmpl	$1, %eax
	jne	.L379
	movsd	8(%rdi), %xmm0
.L356:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L379:
	.cfi_restore_state
	cmpl	$2, %eax
	jne	.L361
.L358:
	pxor	%xmm0, %xmm0
	popq	%rbx
	popq	%r12
	cvtsi2sdq	8(%rdi), %xmm0
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L359:
	.cfi_restore_state
	cmpl	$6, %eax
	jne	.L361
	movq	8(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L380
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	__dynamic_cast@PLT
	testq	%rax, %rax
	je	.L361
	leaq	8(%rbx), %rdi
	jmp	.L364
	.p2align 4,,10
	.p2align 3
.L361:
	movl	$3, (%r12)
	pxor	%xmm0, %xmm0
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L380:
	.cfi_restore_state
	movl	$7, (%r12)
	pxor	%xmm0, %xmm0
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L365:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	pxor	%xmm0, %xmm0
	ret
	.cfi_endproc
.LFE3103:
	.size	_ZNK6icu_6711Formattable9getDoubleER10UErrorCode, .-_ZNK6icu_6711Formattable9getDoubleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6711Formattable9getObjectEv
	.type	_ZNK6icu_6711Formattable9getObjectEv, @function
_ZNK6icu_6711Formattable9getObjectEv:
.LFB3104:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpl	$6, 40(%rdi)
	jne	.L381
	movq	8(%rdi), %rax
.L381:
	ret
	.cfi_endproc
.LFE3104:
	.size	_ZNK6icu_6711Formattable9getObjectEv, .-_ZNK6icu_6711Formattable9getObjectEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711Formattable9setDoubleEd
	.type	_ZN6icu_6711Formattable9setDoubleEd, @function
_ZN6icu_6711Formattable9setDoubleEd:
.LFB3105:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	movsd	%xmm0, -24(%rbp)
	call	_ZN6icu_6711Formattable7disposeEv
	movsd	-24(%rbp), %xmm0
	movl	$1, 40(%rbx)
	movsd	%xmm0, 8(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3105:
	.size	_ZN6icu_6711Formattable9setDoubleEd, .-_ZN6icu_6711Formattable9setDoubleEd
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711Formattable7setLongEi
	.type	_ZN6icu_6711Formattable7setLongEi, @function
_ZN6icu_6711Formattable7setLongEi:
.LFB3106:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movslq	%esi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZN6icu_6711Formattable7disposeEv
	movq	%r12, 8(%rbx)
	movl	$2, 40(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3106:
	.size	_ZN6icu_6711Formattable7setLongEi, .-_ZN6icu_6711Formattable7setLongEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711Formattable8setInt64El
	.type	_ZN6icu_6711Formattable8setInt64El, @function
_ZN6icu_6711Formattable8setInt64El:
.LFB3107:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZN6icu_6711Formattable7disposeEv
	movq	%r12, 8(%rbx)
	movl	$5, 40(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3107:
	.size	_ZN6icu_6711Formattable8setInt64El, .-_ZN6icu_6711Formattable8setInt64El
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711Formattable7setDateEd
	.type	_ZN6icu_6711Formattable7setDateEd, @function
_ZN6icu_6711Formattable7setDateEd:
.LFB3108:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	movsd	%xmm0, -24(%rbp)
	call	_ZN6icu_6711Formattable7disposeEv
	movsd	-24(%rbp), %xmm0
	movl	$0, 40(%rbx)
	movsd	%xmm0, 8(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3108:
	.size	_ZN6icu_6711Formattable7setDateEd, .-_ZN6icu_6711Formattable7setDateEd
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711Formattable9setStringERKNS_13UnicodeStringE
	.type	_ZN6icu_6711Formattable9setStringERKNS_13UnicodeStringE, @function
_ZN6icu_6711Formattable9setStringERKNS_13UnicodeStringE:
.LFB3109:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_6711Formattable7disposeEv
	movl	$3, 40(%rbx)
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L393
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
.L393:
	movq	%r12, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3109:
	.size	_ZN6icu_6711Formattable9setStringERKNS_13UnicodeStringE, .-_ZN6icu_6711Formattable9setStringERKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711Formattable8setArrayEPKS0_i
	.type	_ZN6icu_6711Formattable8setArrayEPKS0_i, @function
_ZN6icu_6711Formattable8setArrayEPKS0_i:
.LFB3110:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_6711Formattable7disposeEv
	movl	$4, 40(%rbx)
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	_ZN6icu_67L15createArrayCopyEPKNS_11FormattableEi
	movl	%r12d, 16(%rbx)
	movq	%rax, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3110:
	.size	_ZN6icu_6711Formattable8setArrayEPKS0_i, .-_ZN6icu_6711Formattable8setArrayEPKS0_i
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711Formattable11adoptStringEPNS_13UnicodeStringE
	.type	_ZN6icu_6711Formattable11adoptStringEPNS_13UnicodeStringE, @function
_ZN6icu_6711Formattable11adoptStringEPNS_13UnicodeStringE:
.LFB3111:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZN6icu_6711Formattable7disposeEv
	movq	%r12, 8(%rbx)
	movl	$3, 40(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3111:
	.size	_ZN6icu_6711Formattable11adoptStringEPNS_13UnicodeStringE, .-_ZN6icu_6711Formattable11adoptStringEPNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711Formattable10adoptArrayEPS0_i
	.type	_ZN6icu_6711Formattable10adoptArrayEPS0_i, @function
_ZN6icu_6711Formattable10adoptArrayEPS0_i:
.LFB3112:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_6711Formattable7disposeEv
	movq	%r13, 8(%rbx)
	movl	%r12d, 16(%rbx)
	movl	$4, 40(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3112:
	.size	_ZN6icu_6711Formattable10adoptArrayEPS0_i, .-_ZN6icu_6711Formattable10adoptArrayEPS0_i
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711Formattable11adoptObjectEPNS_7UObjectE
	.type	_ZN6icu_6711Formattable11adoptObjectEPNS_7UObjectE, @function
_ZN6icu_6711Formattable11adoptObjectEPNS_7UObjectE:
.LFB3113:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZN6icu_6711Formattable7disposeEv
	movq	%r12, 8(%rbx)
	movl	$6, 40(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3113:
	.size	_ZN6icu_6711Formattable11adoptObjectEPNS_7UObjectE, .-_ZN6icu_6711Formattable11adoptObjectEPNS_7UObjectE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6711Formattable9getStringERNS_13UnicodeStringER10UErrorCode
	.type	_ZNK6icu_6711Formattable9getStringERNS_13UnicodeStringER10UErrorCode, @function
_ZNK6icu_6711Formattable9getStringERNS_13UnicodeStringER10UErrorCode:
.LFB3114:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$8, %rsp
	cmpl	$3, 40(%rdi)
	je	.L407
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jle	.L412
.L408:
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
.L409:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L412:
	.cfi_restore_state
	movl	$3, (%rdx)
	jmp	.L408
	.p2align 4,,10
	.p2align 3
.L407:
	movq	8(%rdi), %rsi
	testq	%rsi, %rsi
	je	.L413
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L413:
	.cfi_restore_state
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L409
	movl	$7, (%rdx)
	jmp	.L409
	.cfi_endproc
.LFE3114:
	.size	_ZNK6icu_6711Formattable9getStringERNS_13UnicodeStringER10UErrorCode, .-_ZNK6icu_6711Formattable9getStringERNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6711Formattable9getStringER10UErrorCode
	.type	_ZNK6icu_6711Formattable9getStringER10UErrorCode, @function
_ZNK6icu_6711Formattable9getStringER10UErrorCode:
.LFB3115:
	.cfi_startproc
	endbr64
	cmpl	$3, 40(%rdi)
	je	.L415
	movl	(%rsi), %edx
	testl	%edx, %edx
	jle	.L418
.L416:
	leaq	48(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L418:
	movl	$3, (%rsi)
	leaq	48(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L415:
	movq	8(%rdi), %rax
	testq	%rax, %rax
	je	.L419
	ret
	.p2align 4,,10
	.p2align 3
.L419:
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L416
	movl	$7, (%rsi)
	jmp	.L416
	.cfi_endproc
.LFE3115:
	.size	_ZNK6icu_6711Formattable9getStringER10UErrorCode, .-_ZNK6icu_6711Formattable9getStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711Formattable9getStringER10UErrorCode
	.type	_ZN6icu_6711Formattable9getStringER10UErrorCode, @function
_ZN6icu_6711Formattable9getStringER10UErrorCode:
.LFB3116:
	.cfi_startproc
	endbr64
	cmpl	$3, 40(%rdi)
	je	.L421
	movl	(%rsi), %edx
	testl	%edx, %edx
	jle	.L424
.L422:
	leaq	48(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L424:
	movl	$3, (%rsi)
	leaq	48(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L421:
	movq	8(%rdi), %rax
	testq	%rax, %rax
	je	.L425
	ret
	.p2align 4,,10
	.p2align 3
.L425:
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L422
	movl	$7, (%rsi)
	jmp	.L422
	.cfi_endproc
.LFE3116:
	.size	_ZN6icu_6711Formattable9getStringER10UErrorCode, .-_ZN6icu_6711Formattable9getStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6711Formattable8getArrayERiR10UErrorCode
	.type	_ZNK6icu_6711Formattable8getArrayERiR10UErrorCode, @function
_ZNK6icu_6711Formattable8getArrayERiR10UErrorCode:
.LFB3117:
	.cfi_startproc
	endbr64
	cmpl	$4, 40(%rdi)
	jne	.L430
	movl	16(%rdi), %eax
	movl	%eax, (%rsi)
	movq	8(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L430:
	movl	(%rdx), %eax
	testl	%eax, %eax
	jle	.L431
	movl	$0, (%rsi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L431:
	movl	$3, (%rdx)
	xorl	%eax, %eax
	movl	$0, (%rsi)
	ret
	.cfi_endproc
.LFE3117:
	.size	_ZNK6icu_6711Formattable8getArrayERiR10UErrorCode, .-_ZNK6icu_6711Formattable8getArrayERiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6711Formattable8getBogusEv
	.type	_ZNK6icu_6711Formattable8getBogusEv, @function
_ZNK6icu_6711Formattable8getBogusEv:
.LFB3118:
	.cfi_startproc
	endbr64
	leaq	48(%rdi), %rax
	ret
	.cfi_endproc
.LFE3118:
	.size	_ZNK6icu_6711Formattable8getBogusEv, .-_ZNK6icu_6711Formattable8getBogusEv
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC8:
	.string	"Infinity"
.LC9:
	.string	"NaN"
.LC10:
	.string	"0"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711Formattable21internalGetCharStringER10UErrorCode
	.type	_ZN6icu_6711Formattable21internalGetCharStringER10UErrorCode, @function
_ZN6icu_6711Formattable21internalGetCharStringER10UErrorCode:
.LFB3120:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$96, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	24(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%r12, %r12
	je	.L461
.L433:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L462
	addq	$96, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L461:
	.cfi_restore_state
	movq	32(%rdi), %r12
	movq	%rdi, %rbx
	movq	%rsi, %r13
	testq	%r12, %r12
	je	.L463
.L435:
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L446
	movl	$0, 56(%r12)
	xorl	%edx, %edx
	leaq	13(%rax), %rax
	movw	%dx, 12(%r12)
	movq	32(%rbx), %rdi
	movq	%rax, (%r12)
	movl	$40, 8(%r12)
	movq	(%rdi), %rax
	movq	%r12, 24(%rbx)
	call	*32(%rax)
	testb	%al, %al
	jne	.L464
	movq	32(%rbx), %rdi
	movq	(%rdi), %rax
	call	*24(%rax)
	testb	%al, %al
	je	.L448
	movq	24(%rbx), %r12
	leaq	-128(%rbp), %rdi
	leaq	.LC9(%rip), %rsi
.L460:
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movl	-120(%rbp), %edx
	movq	-128(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r13, %rcx
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movq	24(%rbx), %r12
	jmp	.L433
	.p2align 4,,10
	.p2align 3
.L464:
	movq	24(%rbx), %r12
	leaq	-128(%rbp), %rdi
	leaq	.LC8(%rip), %rsi
	jmp	.L460
	.p2align 4,,10
	.p2align 3
.L463:
	movl	$72, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L436
	movq	%rax, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityC1Ev@PLT
	movl	0(%r13), %esi
	testl	%esi, %esi
	jle	.L437
.L453:
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*8(%rax)
	jmp	.L433
	.p2align 4,,10
	.p2align 3
.L448:
	movq	32(%rbx), %rdi
	call	_ZNK6icu_676number4impl15DecimalQuantity9isZeroishEv@PLT
	testb	%al, %al
	jne	.L465
	movl	40(%rbx), %eax
	movq	32(%rbx), %rsi
	cmpl	$2, %eax
	je	.L450
	cmpl	$5, %eax
	je	.L450
	movq	%rsi, %rdi
	call	_ZNK6icu_676number4impl15DecimalQuantity12getMagnitudeEv@PLT
	cmpl	$-2147483648, %eax
	je	.L451
	movq	32(%rbx), %rdi
	call	_ZNK6icu_676number4impl15DecimalQuantity12getMagnitudeEv@PLT
	cltd
	xorl	%edx, %eax
	subl	%edx, %eax
	cmpl	$4, %eax
	jg	.L451
	movq	32(%rbx), %rsi
.L450:
	leaq	-112(%rbp), %r12
	movq	24(%rbx), %r14
	movq	%r12, %rdi
	call	_ZNK6icu_676number4impl15DecimalQuantity13toPlainStringEv@PLT
.L459:
	movq	%r12, %rsi
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN6icu_6710CharString20appendInvariantCharsERKNS_13UnicodeStringER10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	24(%rbx), %r12
	jmp	.L433
	.p2align 4,,10
	.p2align 3
.L437:
	movq	32(%rbx), %rsi
	testq	%rsi, %rsi
	jne	.L466
	movl	40(%rbx), %eax
	cmpl	$2, %eax
	je	.L442
	cmpl	$5, %eax
	je	.L443
	cmpl	$1, %eax
	je	.L467
	movl	$27, 0(%r13)
	jmp	.L453
.L436:
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L439
	movl	$7, 0(%r13)
.L439:
	xorl	%r12d, %r12d
	jmp	.L433
	.p2align 4,,10
	.p2align 3
.L465:
	movq	24(%rbx), %rdi
	movq	%r13, %rcx
	movl	$-1, %edx
	leaq	.LC10(%rip), %rsi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movq	24(%rbx), %r12
	jmp	.L433
	.p2align 4,,10
	.p2align 3
.L451:
	leaq	-112(%rbp), %r12
	movq	32(%rbx), %rsi
	movq	24(%rbx), %r14
	movq	%r12, %rdi
	call	_ZNK6icu_676number4impl15DecimalQuantity18toScientificStringEv@PLT
	jmp	.L459
	.p2align 4,,10
	.p2align 3
.L466:
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityaSERKS2_@PLT
.L441:
	movl	0(%r13), %ecx
	testl	%ecx, %ecx
	jg	.L453
	movq	%r14, 32(%rbx)
	jmp	.L435
.L442:
	movl	8(%rbx), %esi
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantity8setToIntEi@PLT
	jmp	.L441
.L467:
	movsd	8(%rbx), %xmm0
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantity11setToDoubleEd@PLT
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantity15roundToInfinityEv@PLT
	jmp	.L441
.L443:
	movq	8(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantity9setToLongEl@PLT
	jmp	.L441
.L462:
	call	__stack_chk_fail@PLT
.L446:
	movq	$0, 24(%rbx)
	movl	$7, 0(%r13)
	jmp	.L433
	.cfi_endproc
.LFE3120:
	.size	_ZN6icu_6711Formattable21internalGetCharStringER10UErrorCode, .-_ZN6icu_6711Formattable21internalGetCharStringER10UErrorCode
	.section	.rodata.str1.1
.LC11:
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711Formattable16getDecimalNumberER10UErrorCode
	.type	_ZN6icu_6711Formattable16getDecimalNumberER10UErrorCode, @function
_ZN6icu_6711Formattable16getDecimalNumberER10UErrorCode:
.LFB3119:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L475
	movq	24(%rdi), %rax
	testq	%rax, %rax
	je	.L471
.L472:
	movq	(%rax), %r8
	movl	56(%rax), %edx
.L470:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L476
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	%r8, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L471:
	.cfi_restore_state
	call	_ZN6icu_6711Formattable21internalGetCharStringER10UErrorCode
	testq	%rax, %rax
	jne	.L472
	.p2align 4,,10
	.p2align 3
.L475:
	leaq	-32(%rbp), %rdi
	leaq	.LC11(%rip), %rsi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movq	-32(%rbp), %r8
	movq	-24(%rbp), %rdx
	jmp	.L470
.L476:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3119:
	.size	_ZN6icu_6711Formattable16getDecimalNumberER10UErrorCode, .-_ZN6icu_6711Formattable16getDecimalNumberER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6711Formattable23populateDecimalQuantityERNS_6number4impl15DecimalQuantityER10UErrorCode
	.type	_ZNK6icu_6711Formattable23populateDecimalQuantityERNS_6number4impl15DecimalQuantityER10UErrorCode, @function
_ZNK6icu_6711Formattable23populateDecimalQuantityERNS_6number4impl15DecimalQuantityER10UErrorCode:
.LFB3121:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$8, %rsp
	movq	32(%rdi), %rsi
	testq	%rsi, %rsi
	jne	.L483
	movl	40(%rdi), %eax
	cmpl	$2, %eax
	je	.L479
	cmpl	$5, %eax
	je	.L480
	cmpl	$1, %eax
	je	.L484
	movl	$27, (%rdx)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L483:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676number4impl15DecimalQuantityaSERKS2_@PLT
	.p2align 4,,10
	.p2align 3
.L484:
	.cfi_restore_state
	movsd	8(%rdi), %xmm0
	movq	%r12, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantity11setToDoubleEd@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676number4impl15DecimalQuantity15roundToInfinityEv@PLT
	.p2align 4,,10
	.p2align 3
.L480:
	.cfi_restore_state
	movq	8(%rdi), %rsi
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676number4impl15DecimalQuantity9setToLongEl@PLT
	.p2align 4,,10
	.p2align 3
.L479:
	.cfi_restore_state
	movq	8(%rdi), %rsi
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676number4impl15DecimalQuantity8setToIntEi@PLT
	.cfi_endproc
.LFE3121:
	.size	_ZNK6icu_6711Formattable23populateDecimalQuantityERNS_6number4impl15DecimalQuantityER10UErrorCode, .-_ZNK6icu_6711Formattable23populateDecimalQuantityERNS_6number4impl15DecimalQuantityER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711Formattable20adoptDecimalQuantityEPNS_6number4impl15DecimalQuantityE
	.type	_ZN6icu_6711Formattable20adoptDecimalQuantityEPNS_6number4impl15DecimalQuantityE, @function
_ZN6icu_6711Formattable20adoptDecimalQuantityEPNS_6number4impl15DecimalQuantityE:
.LFB3122:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	32(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L486
	movq	(%rdi), %rax
	call	*8(%rax)
.L486:
	movq	%r12, 32(%rbx)
	testq	%r12, %r12
	je	.L485
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_676number4impl15DecimalQuantity10fitsInLongEb@PLT
	testb	%al, %al
	je	.L488
	movq	32(%rbx), %rdi
	xorl	%esi, %esi
	call	_ZNK6icu_676number4impl15DecimalQuantity6toLongEb@PLT
	movl	$2147483648, %edx
	movq	%rax, 8(%rbx)
	addq	%rdx, %rax
	addq	$2147483647, %rdx
	cmpq	%rax, %rdx
	sbbl	%eax, %eax
	andl	$3, %eax
	addl	$2, %eax
	movl	%eax, 40(%rbx)
.L485:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L488:
	.cfi_restore_state
	movl	$1, 40(%rbx)
	movq	32(%rbx), %rdi
	call	_ZNK6icu_676number4impl15DecimalQuantity8toDoubleEv@PLT
	movsd	%xmm0, 8(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3122:
	.size	_ZN6icu_6711Formattable20adoptDecimalQuantityEPNS_6number4impl15DecimalQuantityE, .-_ZN6icu_6711Formattable20adoptDecimalQuantityEPNS_6number4impl15DecimalQuantityE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711Formattable16setDecimalNumberENS_11StringPieceER10UErrorCode
	.type	_ZN6icu_6711Formattable16setDecimalNumberENS_11StringPieceER10UErrorCode, @function
_ZN6icu_6711Formattable16setDecimalNumberENS_11StringPieceER10UErrorCode:
.LFB3123:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jle	.L516
	ret
	.p2align 4,,10
	.p2align 3
.L516:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_6711Formattable7disposeEv
	movl	$72, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L500
	movq	%rax, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityC1Ev@PLT
	movq	%r15, %rdi
	movq	%r12, %rcx
	movq	%r14, %rsi
	movq	%r13, %rdx
	call	_ZN6icu_676number4impl15DecimalQuantity14setToDecNumberENS_11StringPieceER10UErrorCode@PLT
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L515
	movq	(%rdi), %rax
	call	*8(%rax)
.L515:
	movq	%r15, 32(%rbx)
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZNK6icu_676number4impl15DecimalQuantity10fitsInLongEb@PLT
	testb	%al, %al
	jne	.L517
	movl	$1, 40(%rbx)
	movq	32(%rbx), %rdi
	call	_ZNK6icu_676number4impl15DecimalQuantity8toDoubleEv@PLT
	movsd	%xmm0, 8(%rbx)
.L497:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L517:
	.cfi_restore_state
	movq	32(%rbx), %rdi
	xorl	%esi, %esi
	call	_ZNK6icu_676number4impl15DecimalQuantity6toLongEb@PLT
	movl	$2147483648, %edx
	movq	%rax, 8(%rbx)
	addq	%rdx, %rax
	addq	$2147483647, %rdx
	cmpq	%rax, %rdx
	sbbl	%eax, %eax
	andl	$3, %eax
	addl	$2, %eax
	movl	%eax, 40(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L500:
	.cfi_restore_state
	xorl	%edi, %edi
	movq	%r12, %rcx
	movq	%r14, %rsi
	movq	%r13, %rdx
	call	_ZN6icu_676number4impl15DecimalQuantity14setToDecNumberENS_11StringPieceER10UErrorCode@PLT
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L497
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	$0, 32(%rbx)
	jmp	.L497
	.cfi_endproc
.LFE3123:
	.size	_ZN6icu_6711Formattable16setDecimalNumberENS_11StringPieceER10UErrorCode, .-_ZN6icu_6711Formattable16setDecimalNumberENS_11StringPieceER10UErrorCode
	.p2align 4
	.globl	ufmt_open_67
	.type	ufmt_open_67, @function
ufmt_open_67:
.LFB3124:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movl	(%rdi), %edx
	testl	%edx, %edx
	jg	.L518
	movq	%rdi, %rbx
	movl	$112, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L520
	leaq	16+_ZTVN6icu_6711FormattableE(%rip), %rax
	pxor	%xmm0, %xmm0
	leaq	48(%r12), %rdi
	movq	$0, 8(%r12)
	movq	%rax, (%r12)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, 48(%r12)
	movl	$2, %eax
	movw	%ax, 56(%r12)
	movl	$2, 40(%r12)
	movups	%xmm0, 24(%r12)
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
.L518:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L520:
	.cfi_restore_state
	movl	$7, (%rbx)
	jmp	.L518
	.cfi_endproc
.LFE3124:
	.size	ufmt_open_67, .-ufmt_open_67
	.p2align 4
	.globl	ufmt_close_67
	.type	ufmt_close_67, @function
ufmt_close_67:
.LFB3125:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L523
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN6icu_6711FormattableD0Ev(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L525
	leaq	16+_ZTVN6icu_6711FormattableE(%rip), %rax
	movq	%rax, (%rdi)
	call	_ZN6icu_6711Formattable7disposeEv
	leaq	48(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L523:
	ret
	.p2align 4,,10
	.p2align 3
.L525:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	addq	$8, %rsp
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE3125:
	.size	ufmt_close_67, .-ufmt_close_67
	.p2align 4
	.globl	ufmt_getType_67
	.type	ufmt_getType_67, @function
ufmt_getType_67:
.LFB3126:
	.cfi_startproc
	endbr64
	movl	(%rsi), %edx
	movl	$7, %eax
	testl	%edx, %edx
	jg	.L529
	movl	40(%rdi), %eax
.L529:
	ret
	.cfi_endproc
.LFE3126:
	.size	ufmt_getType_67, .-ufmt_getType_67
	.p2align 4
	.globl	ufmt_isNumeric_67
	.type	ufmt_isNumeric_67, @function
ufmt_isNumeric_67:
.LFB3127:
	.cfi_startproc
	endbr64
	movl	40(%rdi), %eax
	cmpl	$2, %eax
	ja	.L533
	testl	%eax, %eax
	setne	%al
	ret
	.p2align 4,,10
	.p2align 3
.L533:
	cmpl	$5, %eax
	sete	%al
	ret
	.cfi_endproc
.LFE3127:
	.size	ufmt_isNumeric_67, .-ufmt_isNumeric_67
	.p2align 4
	.globl	ufmt_getDate_67
	.type	ufmt_getDate_67, @function
ufmt_getDate_67:
.LFB3128:
	.cfi_startproc
	endbr64
	movl	40(%rdi), %edx
	testl	%edx, %edx
	jne	.L536
	movsd	8(%rdi), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L536:
	movl	(%rsi), %eax
	pxor	%xmm0, %xmm0
	testl	%eax, %eax
	jle	.L539
	ret
	.p2align 4,,10
	.p2align 3
.L539:
	movl	$3, (%rsi)
	ret
	.cfi_endproc
.LFE3128:
	.size	ufmt_getDate_67, .-ufmt_getDate_67
	.p2align 4
	.globl	ufmt_getDouble_67
	.type	ufmt_getDouble_67, @function
ufmt_getDouble_67:
.LFB3129:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L549
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	_ZTIN6icu_677MeasureE(%rip), %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	_ZTIN6icu_677UObjectE(%rip), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
.L548:
	movl	40(%rdi), %eax
	cmpl	$5, %eax
	je	.L542
	ja	.L543
	cmpl	$1, %eax
	jne	.L563
	movsd	8(%rdi), %xmm0
.L540:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L563:
	.cfi_restore_state
	cmpl	$2, %eax
	jne	.L545
.L542:
	pxor	%xmm0, %xmm0
	popq	%rbx
	popq	%r12
	cvtsi2sdq	8(%rdi), %xmm0
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L543:
	.cfi_restore_state
	cmpl	$6, %eax
	jne	.L545
	movq	8(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L564
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	__dynamic_cast@PLT
	testq	%rax, %rax
	je	.L545
	leaq	8(%rbx), %rdi
	jmp	.L548
	.p2align 4,,10
	.p2align 3
.L545:
	movl	$3, (%r12)
	pxor	%xmm0, %xmm0
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L564:
	.cfi_restore_state
	movl	$7, (%r12)
	pxor	%xmm0, %xmm0
	jmp	.L540
	.p2align 4,,10
	.p2align 3
.L549:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	pxor	%xmm0, %xmm0
	ret
	.cfi_endproc
.LFE3129:
	.size	ufmt_getDouble_67, .-ufmt_getDouble_67
	.p2align 4
	.globl	ufmt_getLong_67
	.type	ufmt_getLong_67, @function
ufmt_getLong_67:
.LFB3130:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L579
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	_ZTIN6icu_677MeasureE(%rip), %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	_ZTIN6icu_677UObjectE(%rip), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	40(%rdi), %eax
	cmpl	$5, %eax
	je	.L567
.L588:
	ja	.L568
	cmpl	$1, %eax
	je	.L569
	cmpl	$2, %eax
	jne	.L571
	movl	8(%rdi), %eax
.L565:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L568:
	.cfi_restore_state
	cmpl	$6, %eax
	jne	.L571
	movq	8(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L587
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	__dynamic_cast@PLT
	testq	%rax, %rax
	je	.L571
	movl	48(%rbx), %eax
	leaq	8(%rbx), %rdi
	cmpl	$5, %eax
	jne	.L588
	.p2align 4,,10
	.p2align 3
.L567:
	movq	8(%rdi), %rax
	cmpq	$2147483647, %rax
	jg	.L575
	cmpq	$-2147483648, %rax
	jge	.L565
.L576:
	movl	$3, (%r12)
	movl	$-2147483648, %eax
	jmp	.L565
	.p2align 4,,10
	.p2align 3
.L571:
	movl	$3, (%r12)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L569:
	.cfi_restore_state
	movsd	8(%rdi), %xmm0
	comisd	.LC1(%rip), %xmm0
	ja	.L575
	movsd	.LC2(%rip), %xmm1
	comisd	%xmm0, %xmm1
	ja	.L576
	popq	%rbx
	cvttsd2sil	%xmm0, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L575:
	.cfi_restore_state
	movl	$3, (%r12)
	movl	$2147483647, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L587:
	.cfi_restore_state
	movl	$7, (%r12)
	xorl	%eax, %eax
	jmp	.L565
	.p2align 4,,10
	.p2align 3
.L579:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3130:
	.size	ufmt_getLong_67, .-ufmt_getLong_67
	.p2align 4
	.globl	ufmt_getObject_67
	.type	ufmt_getObject_67, @function
ufmt_getObject_67:
.LFB3131:
	.cfi_startproc
	endbr64
	cmpl	$6, 40(%rdi)
	jne	.L590
	movq	8(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L590:
	movl	(%rsi), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jle	.L593
	ret
	.p2align 4,,10
	.p2align 3
.L593:
	movl	$3, (%rsi)
	ret
	.cfi_endproc
.LFE3131:
	.size	ufmt_getObject_67, .-ufmt_getObject_67
	.p2align 4
	.globl	ufmt_getUChars_67
	.type	ufmt_getUChars_67, @function
ufmt_getUChars_67:
.LFB3132:
	.cfi_startproc
	endbr64
	cmpl	$3, 40(%rdi)
	movl	(%rdx), %eax
	jne	.L609
	movq	8(%rdi), %r8
	testq	%rsi, %rsi
	setne	%cl
	testq	%r8, %r8
	je	.L597
	testl	%eax, %eax
	jg	.L600
	testb	%cl, %cl
	je	.L600
	movswl	8(%r8), %eax
	testw	%ax, %ax
	js	.L602
	sarl	$5, %eax
	movl	%eax, (%rsi)
.L600:
	movq	%r8, %rdi
	jmp	_ZN6icu_6713UnicodeString19getTerminatedBufferEv@PLT
	.p2align 4,,10
	.p2align 3
.L609:
	testl	%eax, %eax
	jg	.L594
	movl	$3, (%rdx)
.L594:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L597:
	testl	%eax, %eax
	jg	.L608
	movl	$7, (%rdx)
.L608:
	leaq	48(%rdi), %r8
	movq	%r8, %rdi
	jmp	_ZN6icu_6713UnicodeString19getTerminatedBufferEv@PLT
	.p2align 4,,10
	.p2align 3
.L602:
	movl	12(%r8), %eax
	movl	%eax, (%rsi)
	jmp	.L600
	.cfi_endproc
.LFE3132:
	.size	ufmt_getUChars_67, .-ufmt_getUChars_67
	.p2align 4
	.globl	ufmt_getArrayLength_67
	.type	ufmt_getArrayLength_67, @function
ufmt_getArrayLength_67:
.LFB3133:
	.cfi_startproc
	endbr64
	cmpl	$4, 40(%rdi)
	jne	.L614
	movl	16(%rdi), %eax
.L610:
	ret
	.p2align 4,,10
	.p2align 3
.L614:
	movl	(%rsi), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L610
	movl	$3, (%rsi)
	ret
	.cfi_endproc
.LFE3133:
	.size	ufmt_getArrayLength_67, .-ufmt_getArrayLength_67
	.p2align 4
	.globl	ufmt_getArrayItemByIndex_67
	.type	ufmt_getArrayItemByIndex_67, @function
ufmt_getArrayItemByIndex_67:
.LFB3134:
	.cfi_startproc
	endbr64
	cmpl	$4, 40(%rdi)
	movl	(%rdx), %eax
	jne	.L622
	movl	16(%rdi), %ecx
	testl	%eax, %eax
	jg	.L617
	cmpl	%ecx, %esi
	jge	.L621
	testl	%esi, %esi
	js	.L621
	movslq	%esi, %rsi
	leaq	0(,%rsi,8), %rax
	subq	%rsi, %rax
	salq	$4, %rax
	addq	8(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L622:
	testl	%eax, %eax
	jle	.L623
.L617:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L621:
	movl	$8, (%rdx)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L623:
	movl	$3, (%rdx)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3134:
	.size	ufmt_getArrayItemByIndex_67, .-ufmt_getArrayItemByIndex_67
	.p2align 4
	.globl	ufmt_getDecNumChars_67
	.type	ufmt_getDecNumChars_67, @function
ufmt_getDecNumChars_67:
.LFB3135:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rdx), %r9d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r9d, %r9d
	jle	.L663
.L625:
	leaq	.LC11(%rip), %rax
.L624:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L664
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L663:
	.cfi_restore_state
	movq	24(%rdi), %rax
	movq	%rdi, %r12
	movq	%rsi, %r13
	testq	%rax, %rax
	je	.L665
.L626:
	testq	%r13, %r13
	je	.L646
	movl	56(%rax), %edx
	movl	%edx, 0(%r13)
.L646:
	movq	(%rax), %rax
	jmp	.L624
	.p2align 4,,10
	.p2align 3
.L665:
	cmpq	$0, 32(%rdi)
	movq	%rdx, %rbx
	je	.L666
.L627:
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L639
	xorl	%ecx, %ecx
	leaq	13(%rax), %rdx
	movl	$0, 56(%rax)
	movw	%cx, 12(%rax)
	movq	32(%r12), %rdi
	movq	%rdx, (%rax)
	movl	$40, 8(%rax)
	movq	%rax, 24(%r12)
	movq	(%rdi), %rax
	call	*32(%rax)
	testb	%al, %al
	jne	.L667
	movq	32(%r12), %rdi
	movq	(%rdi), %rax
	call	*24(%rax)
	testb	%al, %al
	jne	.L668
	movq	32(%r12), %rdi
	call	_ZNK6icu_676number4impl15DecimalQuantity9isZeroishEv@PLT
	testb	%al, %al
	jne	.L669
	movl	40(%r12), %eax
	movq	32(%r12), %rsi
	cmpl	$2, %eax
	je	.L644
	cmpl	$5, %eax
	je	.L644
	movq	%rsi, %rdi
	call	_ZNK6icu_676number4impl15DecimalQuantity12getMagnitudeEv@PLT
	cmpl	$-2147483648, %eax
	je	.L645
	movq	32(%r12), %rdi
	call	_ZNK6icu_676number4impl15DecimalQuantity12getMagnitudeEv@PLT
	cltd
	xorl	%edx, %eax
	subl	%edx, %eax
	cmpl	$4, %eax
	jg	.L645
	movq	32(%r12), %rsi
.L644:
	leaq	-128(%rbp), %r14
	movq	24(%r12), %r15
	movq	%r14, %rdi
	call	_ZNK6icu_676number4impl15DecimalQuantity13toPlainStringEv@PLT
.L661:
	movq	%r15, %rdi
	movq	%rbx, %rdx
	movq	%r14, %rsi
	call	_ZN6icu_6710CharString20appendInvariantCharsERKNS_13UnicodeStringER10UErrorCode@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L641:
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L625
	movq	24(%r12), %rax
	testq	%rax, %rax
	jne	.L626
.L662:
	movl	$7, (%rbx)
	leaq	.LC11(%rip), %rax
	jmp	.L624
	.p2align 4,,10
	.p2align 3
.L668:
	movq	24(%r12), %r14
	leaq	-144(%rbp), %rdi
	leaq	.LC9(%rip), %rsi
.L660:
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movl	-136(%rbp), %edx
	movq	%rbx, %rcx
	movq	%r14, %rdi
	movq	-144(%rbp), %rsi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	jmp	.L641
	.p2align 4,,10
	.p2align 3
.L666:
	movl	$72, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L628
	movq	%rax, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityC1Ev@PLT
	movl	(%rbx), %r8d
	testl	%r8d, %r8d
	jle	.L629
.L648:
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*8(%rax)
	movl	(%rbx), %edi
	testl	%edi, %edi
	jle	.L662
	jmp	.L625
	.p2align 4,,10
	.p2align 3
.L667:
	movq	24(%r12), %r14
	leaq	-144(%rbp), %rdi
	leaq	.LC8(%rip), %rsi
	jmp	.L660
	.p2align 4,,10
	.p2align 3
.L629:
	movq	32(%r12), %rsi
	testq	%rsi, %rsi
	jne	.L670
	movl	40(%r12), %eax
	cmpl	$2, %eax
	je	.L634
	cmpl	$5, %eax
	je	.L635
	cmpl	$1, %eax
	je	.L671
	movl	$27, (%rbx)
	jmp	.L648
.L628:
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L625
	movl	$7, (%rbx)
	jmp	.L625
	.p2align 4,,10
	.p2align 3
.L669:
	movq	24(%r12), %rdi
	movq	%rbx, %rcx
	movl	$-1, %edx
	leaq	.LC10(%rip), %rsi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	jmp	.L641
.L645:
	leaq	-128(%rbp), %r14
	movq	32(%r12), %rsi
	movq	24(%r12), %r15
	movq	%r14, %rdi
	call	_ZNK6icu_676number4impl15DecimalQuantity18toScientificStringEv@PLT
	jmp	.L661
.L670:
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityaSERKS2_@PLT
.L633:
	movl	(%rbx), %esi
	testl	%esi, %esi
	jg	.L648
	movq	%r14, 32(%r12)
	jmp	.L627
.L634:
	movl	8(%r12), %esi
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantity8setToIntEi@PLT
	jmp	.L633
.L671:
	movsd	8(%r12), %xmm0
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantity11setToDoubleEd@PLT
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantity15roundToInfinityEv@PLT
	jmp	.L633
.L635:
	movq	8(%r12), %rsi
	movq	%r14, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantity9setToLongEl@PLT
	jmp	.L633
.L664:
	call	__stack_chk_fail@PLT
.L639:
	movq	$0, 24(%r12)
	jmp	.L662
	.cfi_endproc
.LFE3135:
	.size	ufmt_getDecNumChars_67, .-ufmt_getDecNumChars_67
	.p2align 4
	.globl	ufmt_getInt64_67
	.type	ufmt_getInt64_67, @function
ufmt_getInt64_67:
.LFB3136:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L688
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	_ZTIN6icu_677MeasureE(%rip), %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	_ZTIN6icu_677UObjectE(%rip), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
.L687:
	movl	40(%rbx), %eax
	cmpl	$5, %eax
	je	.L674
	ja	.L675
	cmpl	$1, %eax
	jne	.L711
	movsd	8(%rbx), %xmm0
	comisd	.LC3(%rip), %xmm0
	ja	.L712
	movsd	.LC4(%rip), %xmm1
	comisd	%xmm0, %xmm1
	ja	.L713
	movapd	%xmm0, %xmm1
	andpd	.LC5(%rip), %xmm1
	comisd	.LC6(%rip), %xmm1
	jbe	.L683
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L683
	movl	$1, %esi
	call	_ZNK6icu_676number4impl15DecimalQuantity10fitsInLongEb@PLT
	testb	%al, %al
	je	.L685
	movq	32(%rbx), %rdi
	xorl	%esi, %esi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_676number4impl15DecimalQuantity6toLongEb@PLT
	.p2align 4,,10
	.p2align 3
.L675:
	.cfi_restore_state
	cmpl	$6, %eax
	jne	.L677
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L714
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	__dynamic_cast@PLT
	testq	%rax, %rax
	je	.L677
	addq	$8, %rbx
	jmp	.L687
	.p2align 4,,10
	.p2align 3
.L711:
	cmpl	$2, %eax
	jne	.L677
.L674:
	movq	8(%rbx), %rax
.L672:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L677:
	.cfi_restore_state
	movl	$3, (%r12)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L683:
	.cfi_restore_state
	popq	%rbx
	cvttsd2siq	%xmm0, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L712:
	.cfi_restore_state
	movl	$3, (%r12)
	movabsq	$9223372036854775807, %rax
	jmp	.L672
	.p2align 4,,10
	.p2align 3
.L685:
	movl	$3, (%r12)
	movq	32(%rbx), %rdi
	call	_ZNK6icu_676number4impl15DecimalQuantity10isNegativeEv@PLT
	movabsq	$9223372036854775807, %rdx
	testb	%al, %al
	movabsq	$-9223372036854775808, %rax
	cmove	%rdx, %rax
	jmp	.L672
.L714:
	movl	$7, (%r12)
	xorl	%eax, %eax
	jmp	.L672
	.p2align 4,,10
	.p2align 3
.L688:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	xorl	%eax, %eax
	ret
.L713:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movl	$3, (%r12)
	movabsq	$-9223372036854775808, %rax
	jmp	.L672
	.cfi_endproc
.LFE3136:
	.size	ufmt_getInt64_67, .-ufmt_getInt64_67
	.weak	_ZTSN6icu_6711FormattableE
	.section	.rodata._ZTSN6icu_6711FormattableE,"aG",@progbits,_ZTSN6icu_6711FormattableE,comdat
	.align 16
	.type	_ZTSN6icu_6711FormattableE, @object
	.size	_ZTSN6icu_6711FormattableE, 23
_ZTSN6icu_6711FormattableE:
	.string	"N6icu_6711FormattableE"
	.weak	_ZTIN6icu_6711FormattableE
	.section	.data.rel.ro._ZTIN6icu_6711FormattableE,"awG",@progbits,_ZTIN6icu_6711FormattableE,comdat
	.align 8
	.type	_ZTIN6icu_6711FormattableE, @object
	.size	_ZTIN6icu_6711FormattableE, 24
_ZTIN6icu_6711FormattableE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6711FormattableE
	.quad	_ZTIN6icu_677UObjectE
	.weak	_ZTVN6icu_6711FormattableE
	.section	.data.rel.ro.local._ZTVN6icu_6711FormattableE,"awG",@progbits,_ZTVN6icu_6711FormattableE,comdat
	.align 8
	.type	_ZTVN6icu_6711FormattableE, @object
	.size	_ZTVN6icu_6711FormattableE, 40
_ZTVN6icu_6711FormattableE:
	.quad	0
	.quad	_ZTIN6icu_6711FormattableE
	.quad	_ZN6icu_6711FormattableD1Ev
	.quad	_ZN6icu_6711FormattableD0Ev
	.quad	_ZNK6icu_6711Formattable17getDynamicClassIDEv
	.local	_ZZN6icu_6711Formattable16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6711Formattable16getStaticClassIDEvE7classID,1,1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	4290772992
	.long	1105199103
	.align 8
.LC2:
	.long	0
	.long	-1042284544
	.align 8
.LC3:
	.long	0
	.long	1138753536
	.align 8
.LC4:
	.long	0
	.long	-1008730112
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC5:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC6:
	.long	0
	.long	1128267776
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
