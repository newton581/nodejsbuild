	.file	"alphaindex.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715AlphabeticIndex14getRecordCountER10UErrorCode
	.type	_ZN6icu_6715AlphabeticIndex14getRecordCountER10UErrorCode, @function
_ZN6icu_6715AlphabeticIndex14getRecordCountER10UErrorCode:
.LFB2546:
	.cfi_startproc
	endbr64
	movl	(%rsi), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L1
	movq	8(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L1
	movl	8(%rdx), %eax
.L1:
	ret
	.cfi_endproc
.LFE2546:
	.size	_ZN6icu_6715AlphabeticIndex14getRecordCountER10UErrorCode, .-_ZN6icu_6715AlphabeticIndex14getRecordCountER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715AlphabeticIndexeqERKS0_
	.type	_ZNK6icu_6715AlphabeticIndexeqERKS0_, @function
_ZNK6icu_6715AlphabeticIndexeqERKS0_:
.LFB2557:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2557:
	.size	_ZNK6icu_6715AlphabeticIndexeqERKS0_, .-_ZNK6icu_6715AlphabeticIndexeqERKS0_
	.globl	_ZNK6icu_6715AlphabeticIndexneERKS0_
	.set	_ZNK6icu_6715AlphabeticIndexneERKS0_,_ZNK6icu_6715AlphabeticIndexeqERKS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715AlphabeticIndex11getCollatorEv
	.type	_ZNK6icu_6715AlphabeticIndex11getCollatorEv, @function
_ZNK6icu_6715AlphabeticIndex11getCollatorEv:
.LFB2559:
	.cfi_startproc
	endbr64
	movq	56(%rdi), %rax
	ret
	.cfi_endproc
.LFE2559:
	.size	_ZNK6icu_6715AlphabeticIndex11getCollatorEv, .-_ZNK6icu_6715AlphabeticIndex11getCollatorEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715AlphabeticIndex14getInflowLabelEv
	.type	_ZNK6icu_6715AlphabeticIndex14getInflowLabelEv, @function
_ZNK6icu_6715AlphabeticIndex14getInflowLabelEv:
.LFB2560:
	.cfi_startproc
	endbr64
	leaq	80(%rdi), %rax
	ret
	.cfi_endproc
.LFE2560:
	.size	_ZNK6icu_6715AlphabeticIndex14getInflowLabelEv, .-_ZNK6icu_6715AlphabeticIndex14getInflowLabelEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715AlphabeticIndex16getOverflowLabelEv
	.type	_ZNK6icu_6715AlphabeticIndex16getOverflowLabelEv, @function
_ZNK6icu_6715AlphabeticIndex16getOverflowLabelEv:
.LFB2561:
	.cfi_startproc
	endbr64
	leaq	144(%rdi), %rax
	ret
	.cfi_endproc
.LFE2561:
	.size	_ZNK6icu_6715AlphabeticIndex16getOverflowLabelEv, .-_ZNK6icu_6715AlphabeticIndex16getOverflowLabelEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715AlphabeticIndex17getUnderflowLabelEv
	.type	_ZNK6icu_6715AlphabeticIndex17getUnderflowLabelEv, @function
_ZNK6icu_6715AlphabeticIndex17getUnderflowLabelEv:
.LFB2562:
	.cfi_startproc
	endbr64
	leaq	208(%rdi), %rax
	ret
	.cfi_endproc
.LFE2562:
	.size	_ZNK6icu_6715AlphabeticIndex17getUnderflowLabelEv, .-_ZNK6icu_6715AlphabeticIndex17getUnderflowLabelEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715AlphabeticIndex16getMaxLabelCountEv
	.type	_ZNK6icu_6715AlphabeticIndex16getMaxLabelCountEv, @function
_ZNK6icu_6715AlphabeticIndex16getMaxLabelCountEv:
.LFB2566:
	.cfi_startproc
	endbr64
	movl	32(%rdi), %eax
	ret
	.cfi_endproc
.LFE2566:
	.size	_ZNK6icu_6715AlphabeticIndex16getMaxLabelCountEv, .-_ZNK6icu_6715AlphabeticIndex16getMaxLabelCountEv
	.p2align 4
	.type	_ZN6icu_67L18collatorComparatorEPKvS1_S1_, @function
_ZN6icu_67L18collatorComparatorEPKvS1_S1_:
.LFB2569:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	(%rsi), %rsi
	movq	(%rdx), %rdx
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	cmpq	%rdx, %rsi
	je	.L15
	testq	%rsi, %rsi
	je	.L16
	testq	%rdx, %rdx
	je	.L17
	movq	(%rdi), %rax
	movl	$0, -12(%rbp)
	leaq	-12(%rbp), %rcx
	call	*56(%rax)
.L12:
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L19
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	.cfi_restore_state
	xorl	%eax, %eax
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L16:
	movl	$1, %eax
	jmp	.L12
.L17:
	movl	$-1, %eax
	jmp	.L12
.L19:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2569:
	.size	_ZN6icu_67L18collatorComparatorEPKvS1_S1_, .-_ZN6icu_67L18collatorComparatorEPKvS1_S1_
	.p2align 4
	.type	_ZN6icu_67L15recordCompareFnEPKvS1_S1_, @function
_ZN6icu_67L15recordCompareFnEPKvS1_S1_:
.LFB2570:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	(%rsi), %rsi
	movq	(%rdx), %rdx
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	leaq	-12(%rbp), %rcx
	movl	$0, -12(%rbp)
	addq	$8, %rdx
	addq	$8, %rsi
	call	*56(%rax)
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L23
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L23:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2570:
	.size	_ZN6icu_67L15recordCompareFnEPKvS1_S1_, .-_ZN6icu_67L15recordCompareFnEPKvS1_S1_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715AlphabeticIndex14getBucketIndexEv
	.type	_ZNK6icu_6715AlphabeticIndex14getBucketIndexEv, @function
_ZNK6icu_6715AlphabeticIndex14getBucketIndexEv:
.LFB2582:
	.cfi_startproc
	endbr64
	movl	16(%rdi), %eax
	ret
	.cfi_endproc
.LFE2582:
	.size	_ZNK6icu_6715AlphabeticIndex14getBucketIndexEv, .-_ZNK6icu_6715AlphabeticIndex14getBucketIndexEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715AlphabeticIndex14getBucketLabelEv
	.type	_ZNK6icu_6715AlphabeticIndex14getBucketLabelEv, @function
_ZNK6icu_6715AlphabeticIndex14getBucketLabelEv:
.LFB2584:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rdx
	addq	$336, %rdi
	leaq	8(%rdx), %rax
	testq	%rdx, %rdx
	cmove	%rdi, %rax
	ret
	.cfi_endproc
.LFE2584:
	.size	_ZNK6icu_6715AlphabeticIndex14getBucketLabelEv, .-_ZNK6icu_6715AlphabeticIndex14getBucketLabelEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715AlphabeticIndex18getBucketLabelTypeEv
	.type	_ZNK6icu_6715AlphabeticIndex18getBucketLabelTypeEv, @function
_ZNK6icu_6715AlphabeticIndex18getBucketLabelTypeEv:
.LFB2585:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rdx
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L28
	movl	136(%rdx), %eax
.L28:
	ret
	.cfi_endproc
.LFE2585:
	.size	_ZNK6icu_6715AlphabeticIndex18getBucketLabelTypeEv, .-_ZNK6icu_6715AlphabeticIndex18getBucketLabelTypeEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715AlphabeticIndex20getBucketRecordCountEv
	.type	_ZNK6icu_6715AlphabeticIndex20getBucketRecordCountEv, @function
_ZNK6icu_6715AlphabeticIndex20getBucketRecordCountEv:
.LFB2586:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	testq	%rax, %rax
	je	.L35
	movq	160(%rax), %rax
	testq	%rax, %rax
	je	.L35
	movl	8(%rax), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2586:
	.size	_ZNK6icu_6715AlphabeticIndex20getBucketRecordCountEv, .-_ZNK6icu_6715AlphabeticIndex20getBucketRecordCountEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715AlphabeticIndex19resetBucketIteratorER10UErrorCode
	.type	_ZN6icu_6715AlphabeticIndex19resetBucketIteratorER10UErrorCode, @function
_ZN6icu_6715AlphabeticIndex19resetBucketIteratorER10UErrorCode:
.LFB2587:
	.cfi_startproc
	endbr64
	movl	(%rsi), %edx
	movq	%rdi, %rax
	testl	%edx, %edx
	jg	.L37
	movl	$-1, 16(%rdi)
	movq	$0, 24(%rdi)
.L37:
	ret
	.cfi_endproc
.LFE2587:
	.size	_ZN6icu_6715AlphabeticIndex19resetBucketIteratorER10UErrorCode, .-_ZN6icu_6715AlphabeticIndex19resetBucketIteratorER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715AlphabeticIndex19resetRecordIteratorEv
	.type	_ZN6icu_6715AlphabeticIndex19resetRecordIteratorEv, @function
_ZN6icu_6715AlphabeticIndex19resetRecordIteratorEv:
.LFB2591:
	.cfi_startproc
	endbr64
	movl	$-1, 20(%rdi)
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE2591:
	.size	_ZN6icu_6715AlphabeticIndex19resetRecordIteratorEv, .-_ZN6icu_6715AlphabeticIndex19resetRecordIteratorEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710BucketListD2Ev
	.type	_ZN6icu_6710BucketListD2Ev, @function
_ZN6icu_6710BucketListD2Ev:
.LFB2518:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6710BucketListE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L40
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	8(%r12), %rdi
.L40:
	movq	16(%r12), %r8
	cmpq	%rdi, %r8
	je	.L41
	testq	%r8, %r8
	je	.L41
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
.L41:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE2518:
	.size	_ZN6icu_6710BucketListD2Ev, .-_ZN6icu_6710BucketListD2Ev
	.globl	_ZN6icu_6710BucketListD1Ev
	.set	_ZN6icu_6710BucketListD1Ev,_ZN6icu_6710BucketListD2Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715AlphabeticIndex13getRecordNameEv
	.type	_ZNK6icu_6715AlphabeticIndex13getRecordNameEv, @function
_ZNK6icu_6715AlphabeticIndex13getRecordNameEv:
.LFB2589:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rdx
	leaq	336(%rdi), %rax
	testq	%rdx, %rdx
	je	.L61
	movq	160(%rdx), %r8
	testq	%r8, %r8
	je	.L61
	movl	20(%rdi), %esi
	testl	%esi, %esi
	js	.L61
	cmpl	8(%r8), %esi
	jl	.L64
.L61:
	ret
	.p2align 4,,10
	.p2align 3
.L64:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%r8, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	addq	$8, %rax
	ret
	.cfi_endproc
.LFE2589:
	.size	_ZNK6icu_6715AlphabeticIndex13getRecordNameEv, .-_ZNK6icu_6715AlphabeticIndex13getRecordNameEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715AlphabeticIndex13getRecordDataEv
	.type	_ZNK6icu_6715AlphabeticIndex13getRecordDataEv, @function
_ZNK6icu_6715AlphabeticIndex13getRecordDataEv:
.LFB2590:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	testq	%rax, %rax
	je	.L76
	movq	160(%rax), %rax
	testq	%rax, %rax
	je	.L76
	movl	20(%rdi), %esi
	testl	%esi, %esi
	js	.L68
	cmpl	8(%rax), %esi
	jge	.L68
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rax, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	72(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L76:
	ret
	.cfi_endproc
.LFE2590:
	.size	_ZNK6icu_6715AlphabeticIndex13getRecordDataEv, .-_ZNK6icu_6715AlphabeticIndex13getRecordDataEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715AlphabeticIndex6BucketD2Ev
	.type	_ZN6icu_6715AlphabeticIndex6BucketD2Ev, @function
_ZN6icu_6715AlphabeticIndex6BucketD2Ev:
.LFB2596:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6715AlphabeticIndex6BucketE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	160(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L80
	movq	(%rdi), %rax
	call	*8(%rax)
.L80:
	leaq	72(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	8(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE2596:
	.size	_ZN6icu_6715AlphabeticIndex6BucketD2Ev, .-_ZN6icu_6715AlphabeticIndex6BucketD2Ev
	.globl	_ZN6icu_6715AlphabeticIndex6BucketD1Ev
	.set	_ZN6icu_6715AlphabeticIndex6BucketD1Ev,_ZN6icu_6715AlphabeticIndex6BucketD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715AlphabeticIndex10nextRecordER10UErrorCode
	.type	_ZN6icu_6715AlphabeticIndex10nextRecordER10UErrorCode, @function
_ZN6icu_6715AlphabeticIndex10nextRecordER10UErrorCode:
.LFB2588:
	.cfi_startproc
	endbr64
	movl	(%rsi), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L85
	movq	24(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L92
	cmpq	$0, 72(%rdi)
	je	.L93
	movq	160(%rdx), %rcx
	testq	%rcx, %rcx
	je	.L85
	movl	20(%rdi), %eax
	leal	1(%rax), %edx
	movl	$1, %eax
	movl	%edx, 20(%rdi)
	movl	8(%rcx), %ecx
	cmpl	%ecx, %edx
	jge	.L94
.L85:
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	movl	%ecx, 20(%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L92:
	movl	$27, (%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L93:
	movl	$25, (%rsi)
	ret
	.cfi_endproc
.LFE2588:
	.size	_ZN6icu_6715AlphabeticIndex10nextRecordER10UErrorCode, .-_ZN6icu_6715AlphabeticIndex10nextRecordER10UErrorCode
	.p2align 4
	.type	_ZN6icu_67L23alphaIndex_deleteRecordEPv, @function
_ZN6icu_67L23alphaIndex_deleteRecordEPv:
.LFB2506:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L95
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	8(%rdi), %rdi
	subq	$8, %rsp
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L95:
	ret
	.cfi_endproc
.LFE2506:
	.size	_ZN6icu_67L23alphaIndex_deleteRecordEPv, .-_ZN6icu_67L23alphaIndex_deleteRecordEPv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715AlphabeticIndex6BucketD0Ev
	.type	_ZN6icu_6715AlphabeticIndex6BucketD0Ev, @function
_ZN6icu_6715AlphabeticIndex6BucketD0Ev:
.LFB2598:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6715AlphabeticIndex6BucketE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	160(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L101
	movq	(%rdi), %rax
	call	*8(%rax)
.L101:
	leaq	72(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	8(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2598:
	.size	_ZN6icu_6715AlphabeticIndex6BucketD0Ev, .-_ZN6icu_6715AlphabeticIndex6BucketD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710BucketListD0Ev
	.type	_ZN6icu_6710BucketListD0Ev, @function
_ZN6icu_6710BucketListD0Ev:
.LFB2520:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6710BucketListE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L107
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	8(%r12), %rdi
.L107:
	movq	16(%r12), %r8
	cmpq	%rdi, %r8
	je	.L108
	testq	%r8, %r8
	je	.L108
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
.L108:
	movq	%r12, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2520:
	.size	_ZN6icu_6710BucketListD0Ev, .-_ZN6icu_6710BucketListD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715AlphabeticIndex14ImmutableIndexD0Ev
	.type	_ZN6icu_6715AlphabeticIndex14ImmutableIndexD0Ev, @function
_ZN6icu_6715AlphabeticIndex14ImmutableIndexD0Ev:
.LFB2524:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6715AlphabeticIndex14ImmutableIndexE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	8(%rdi), %r13
	movq	%rdi, %r12
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L120
	movq	0(%r13), %rax
	leaq	_ZN6icu_6710BucketListD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L121
	movq	8(%r13), %rdi
	leaq	16+_ZTVN6icu_6710BucketListE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%rdi, %rdi
	je	.L122
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	8(%r13), %rdi
.L122:
	movq	16(%r13), %r8
	testq	%r8, %r8
	je	.L123
	cmpq	%rdi, %r8
	je	.L123
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
.L123:
	movq	%r13, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L120:
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L124
	movq	(%rdi), %rax
	call	*8(%rax)
.L124:
	movq	%r12, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L121:
	.cfi_restore_state
	movq	%r13, %rdi
	call	*%rax
	jmp	.L120
	.cfi_endproc
.LFE2524:
	.size	_ZN6icu_6715AlphabeticIndex14ImmutableIndexD0Ev, .-_ZN6icu_6715AlphabeticIndex14ImmutableIndexD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715AlphabeticIndex16setOverflowLabelERKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6715AlphabeticIndex16setOverflowLabelERKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6715AlphabeticIndex16setOverflowLabelERKNS_13UnicodeStringER10UErrorCode:
.LFB2564:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	addq	$144, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	72(%r12), %r13
	testq	%r13, %r13
	je	.L142
	movq	0(%r13), %rax
	leaq	_ZN6icu_6710BucketListD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L143
	movq	8(%r13), %rdi
	leaq	16+_ZTVN6icu_6710BucketListE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%rdi, %rdi
	je	.L144
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	8(%r13), %rdi
.L144:
	movq	16(%r13), %r8
	cmpq	%rdi, %r8
	je	.L145
	testq	%r8, %r8
	je	.L145
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
.L145:
	movq	%r13, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L146:
	movq	$0, 72(%r12)
	movl	$-1, 16(%r12)
	movq	$0, 24(%r12)
.L142:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L143:
	.cfi_restore_state
	movq	%r13, %rdi
	call	*%rax
	jmp	.L146
	.cfi_endproc
.LFE2564:
	.size	_ZN6icu_6715AlphabeticIndex16setOverflowLabelERKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6715AlphabeticIndex16setOverflowLabelERKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715AlphabeticIndex17setUnderflowLabelERKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6715AlphabeticIndex17setUnderflowLabelERKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6715AlphabeticIndex17setUnderflowLabelERKNS_13UnicodeStringER10UErrorCode:
.LFB2565:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	addq	$208, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	72(%r12), %r13
	testq	%r13, %r13
	je	.L161
	movq	0(%r13), %rax
	leaq	_ZN6icu_6710BucketListD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L162
	movq	8(%r13), %rdi
	leaq	16+_ZTVN6icu_6710BucketListE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%rdi, %rdi
	je	.L163
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	8(%r13), %rdi
.L163:
	movq	16(%r13), %r8
	cmpq	%rdi, %r8
	je	.L164
	testq	%r8, %r8
	je	.L164
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
.L164:
	movq	%r13, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L165:
	movq	$0, 72(%r12)
	movl	$-1, 16(%r12)
	movq	$0, 24(%r12)
.L161:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L162:
	.cfi_restore_state
	movq	%r13, %rdi
	call	*%rax
	jmp	.L165
	.cfi_endproc
.LFE2565:
	.size	_ZN6icu_6715AlphabeticIndex17setUnderflowLabelERKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6715AlphabeticIndex17setUnderflowLabelERKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715AlphabeticIndex14setInflowLabelERKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6715AlphabeticIndex14setInflowLabelERKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6715AlphabeticIndex14setInflowLabelERKNS_13UnicodeStringER10UErrorCode:
.LFB2563:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	addq	$80, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	72(%r12), %r13
	testq	%r13, %r13
	je	.L180
	movq	0(%r13), %rax
	leaq	_ZN6icu_6710BucketListD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L181
	movq	8(%r13), %rdi
	leaq	16+_ZTVN6icu_6710BucketListE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%rdi, %rdi
	je	.L182
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	8(%r13), %rdi
.L182:
	movq	16(%r13), %r8
	cmpq	%rdi, %r8
	je	.L183
	testq	%r8, %r8
	je	.L183
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
.L183:
	movq	%r13, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L184:
	movq	$0, 72(%r12)
	movl	$-1, 16(%r12)
	movq	$0, 24(%r12)
.L180:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L181:
	.cfi_restore_state
	movq	%r13, %rdi
	call	*%rax
	jmp	.L184
	.cfi_endproc
.LFE2563:
	.size	_ZN6icu_6715AlphabeticIndex14setInflowLabelERKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6715AlphabeticIndex14setInflowLabelERKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715AlphabeticIndex14ImmutableIndexD2Ev
	.type	_ZN6icu_6715AlphabeticIndex14ImmutableIndexD2Ev, @function
_ZN6icu_6715AlphabeticIndex14ImmutableIndexD2Ev:
.LFB2522:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6715AlphabeticIndex14ImmutableIndexE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	8(%rdi), %r13
	movq	%rdi, %r12
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L199
	movq	0(%r13), %rax
	leaq	_ZN6icu_6710BucketListD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L200
	movq	8(%r13), %rdi
	leaq	16+_ZTVN6icu_6710BucketListE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%rdi, %rdi
	je	.L201
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	8(%r13), %rdi
.L201:
	movq	16(%r13), %r8
	testq	%r8, %r8
	je	.L202
	cmpq	%rdi, %r8
	je	.L202
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
.L202:
	movq	%r13, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L199:
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L203
	movq	(%rdi), %rax
	call	*8(%rax)
.L203:
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L200:
	.cfi_restore_state
	movq	%r13, %rdi
	call	*%rax
	jmp	.L199
	.cfi_endproc
.LFE2522:
	.size	_ZN6icu_6715AlphabeticIndex14ImmutableIndexD2Ev, .-_ZN6icu_6715AlphabeticIndex14ImmutableIndexD2Ev
	.globl	_ZN6icu_6715AlphabeticIndex14ImmutableIndexD1Ev
	.set	_ZN6icu_6715AlphabeticIndex14ImmutableIndexD1Ev,_ZN6icu_6715AlphabeticIndex14ImmutableIndexD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715AlphabeticIndex16setMaxLabelCountEiR10UErrorCode
	.type	_ZN6icu_6715AlphabeticIndex16setMaxLabelCountEiR10UErrorCode, @function
_ZN6icu_6715AlphabeticIndex16setMaxLabelCountEiR10UErrorCode:
.LFB2567:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	(%rdx), %eax
	movq	%rdi, %r12
	testl	%eax, %eax
	jg	.L222
	testl	%esi, %esi
	jle	.L242
	movq	72(%rdi), %r13
	movl	%esi, 32(%rdi)
	testq	%r13, %r13
	je	.L222
	movq	0(%r13), %rax
	leaq	_ZN6icu_6710BucketListD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L225
	movq	8(%r13), %rdi
	leaq	16+_ZTVN6icu_6710BucketListE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%rdi, %rdi
	je	.L226
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	8(%r13), %rdi
.L226:
	movq	16(%r13), %r8
	cmpq	%rdi, %r8
	je	.L227
	testq	%r8, %r8
	je	.L227
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
.L227:
	movq	%r13, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L228:
	movq	$0, 72(%r12)
	movl	$-1, 16(%r12)
	movq	$0, 24(%r12)
.L222:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L242:
	.cfi_restore_state
	movl	$1, (%rdx)
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L225:
	.cfi_restore_state
	movq	%r13, %rdi
	call	*%rax
	jmp	.L228
	.cfi_endproc
.LFE2567:
	.size	_ZN6icu_6715AlphabeticIndex16setMaxLabelCountEiR10UErrorCode, .-_ZN6icu_6715AlphabeticIndex16setMaxLabelCountEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715AlphabeticIndex9addRecordERKNS_13UnicodeStringEPKvR10UErrorCode
	.type	_ZN6icu_6715AlphabeticIndex9addRecordERKNS_13UnicodeStringEPKvR10UErrorCode, @function
_ZN6icu_6715AlphabeticIndex9addRecordERKNS_13UnicodeStringEPKvR10UErrorCode:
.LFB2579:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L258
	cmpq	$0, 8(%rdi)
	movq	%rsi, %r15
	movq	%rdx, %rbx
	movq	%rcx, %r13
	je	.L269
.L246:
	movl	$80, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L268
	leaq	8(%rax), %rdi
	movq	%r15, %rsi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	%rbx, 72(%r14)
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	8(%r12), %rdi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	movq	72(%r12), %r13
	testq	%r13, %r13
	je	.L258
	movq	0(%r13), %rax
	leaq	_ZN6icu_6710BucketListD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L250
	movq	8(%r13), %rdi
	leaq	16+_ZTVN6icu_6710BucketListE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%rdi, %rdi
	je	.L251
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	8(%r13), %rdi
.L251:
	movq	16(%r13), %r8
	testq	%r8, %r8
	je	.L252
	cmpq	%rdi, %r8
	je	.L252
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
.L252:
	movq	%r13, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L253:
	movq	$0, 72(%r12)
	movl	$-1, 16(%r12)
	movq	$0, 24(%r12)
.L258:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L269:
	.cfi_restore_state
	movl	$40, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L247
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_677UVectorC1ER10UErrorCode@PLT
	movq	%r14, 8(%r12)
	leaq	_ZN6icu_67L23alphaIndex_deleteRecordEPv(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN6icu_677UVector10setDeleterEPFvPvE@PLT
	jmp	.L246
	.p2align 4,,10
	.p2align 3
.L250:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L253
.L247:
	movq	$0, 8(%r12)
.L268:
	movl	$7, 0(%r13)
	jmp	.L258
	.cfi_endproc
.LFE2579:
	.size	_ZN6icu_6715AlphabeticIndex9addRecordERKNS_13UnicodeStringEPKvR10UErrorCode, .-_ZN6icu_6715AlphabeticIndex9addRecordERKNS_13UnicodeStringEPKvR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715AlphabeticIndex12clearRecordsER10UErrorCode
	.type	_ZN6icu_6715AlphabeticIndex12clearRecordsER10UErrorCode, @function
_ZN6icu_6715AlphabeticIndex12clearRecordsER10UErrorCode:
.LFB2580:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	movq	%rdi, %rax
	testl	%ecx, %ecx
	jle	.L297
.L294:
	ret
	.p2align 4,,10
	.p2align 3
.L297:
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L294
	movl	8(%rdi), %edx
	testl	%edx, %edx
	je	.L294
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 12, -24
	movq	%rax, -24(%rbp)
	call	_ZN6icu_677UVector17removeAllElementsEv@PLT
	movq	-24(%rbp), %rax
	movq	72(%rax), %r12
	testq	%r12, %r12
	je	.L272
	movq	(%r12), %rdx
	leaq	_ZN6icu_6710BucketListD0Ev(%rip), %rcx
	movq	8(%rdx), %rdx
	cmpq	%rcx, %rdx
	jne	.L274
	movq	8(%r12), %rdi
	leaq	16+_ZTVN6icu_6710BucketListE(%rip), %rcx
	movq	%rcx, (%r12)
	testq	%rdi, %rdi
	je	.L275
	movq	(%rdi), %rdx
	call	*8(%rdx)
	movq	8(%r12), %rdi
	movq	-24(%rbp), %rax
.L275:
	movq	16(%r12), %r8
	cmpq	%rdi, %r8
	je	.L276
	testq	%r8, %r8
	je	.L276
	movq	(%r8), %rdx
	movq	%rax, -24(%rbp)
	movq	%r8, %rdi
	call	*8(%rdx)
	movq	-24(%rbp), %rax
.L276:
	movq	%r12, %rdi
	movq	%rax, -24(%rbp)
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	movq	-24(%rbp), %rax
.L277:
	movq	$0, 72(%rax)
	movl	$-1, 16(%rax)
	movq	$0, 24(%rax)
.L272:
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L274:
	.cfi_restore_state
	movq	%rax, -24(%rbp)
	movq	%r12, %rdi
	call	*%rdx
	movq	-24(%rbp), %rax
	jmp	.L277
	.cfi_endproc
.LFE2580:
	.size	_ZN6icu_6715AlphabeticIndex12clearRecordsER10UErrorCode, .-_ZN6icu_6715AlphabeticIndex12clearRecordsER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715AlphabeticIndex14ImmutableIndex14getBucketCountEv
	.type	_ZNK6icu_6715AlphabeticIndex14ImmutableIndex14getBucketCountEv, @function
_ZNK6icu_6715AlphabeticIndex14ImmutableIndex14getBucketCountEv:
.LFB2525:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movq	16(%rax), %rax
	movl	8(%rax), %eax
	ret
	.cfi_endproc
.LFE2525:
	.size	_ZNK6icu_6715AlphabeticIndex14ImmutableIndex14getBucketCountEv, .-_ZNK6icu_6715AlphabeticIndex14ImmutableIndex14getBucketCountEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715AlphabeticIndex14ImmutableIndex14getBucketIndexERKNS_13UnicodeStringER10UErrorCode
	.type	_ZNK6icu_6715AlphabeticIndex14ImmutableIndex14getBucketIndexERKNS_13UnicodeStringER10UErrorCode, @function
_ZNK6icu_6715AlphabeticIndex14ImmutableIndex14getBucketIndexERKNS_13UnicodeStringER10UErrorCode:
.LFB2526:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r8
	movq	16(%rdi), %r13
	movq	%rsi, -56(%rbp)
	movq	8(%r8), %rdi
	movl	8(%rdi), %ebx
	cmpl	$1, %ebx
	jle	.L307
	movq	%rdx, %r14
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L301:
	leal	(%r15,%rbx), %r12d
	movq	%r8, -64(%rbp)
	sarl	%r12d
	movl	%r12d, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	-56(%rbp), %rsi
	movq	%r14, %rcx
	movq	%r13, %rdi
	leaq	72(%rax), %rdx
	movq	0(%r13), %rax
	call	*56(%rax)
	movq	-64(%rbp), %r8
	testl	%eax, %eax
	js	.L312
	leal	1(%r12), %eax
	movq	8(%r8), %rdi
	cmpl	%eax, %ebx
	jle	.L300
	movl	%r12d, %r15d
	jmp	.L301
	.p2align 4,,10
	.p2align 3
.L312:
	leal	1(%r15), %eax
	movq	8(%r8), %rdi
	cmpl	%eax, %r12d
	jle	.L308
	movl	%r12d, %ebx
	jmp	.L301
	.p2align 4,,10
	.p2align 3
.L307:
	xorl	%r12d, %r12d
.L300:
	movl	%r12d, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	144(%rax), %rdx
	testq	%rdx, %rdx
	cmove	%rax, %rdx
	movl	152(%rdx), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L308:
	.cfi_restore_state
	movl	%r15d, %r12d
	jmp	.L300
	.cfi_endproc
.LFE2526:
	.size	_ZNK6icu_6715AlphabeticIndex14ImmutableIndex14getBucketIndexERKNS_13UnicodeStringER10UErrorCode, .-_ZNK6icu_6715AlphabeticIndex14ImmutableIndex14getBucketIndexERKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715AlphabeticIndex14ImmutableIndex9getBucketEi
	.type	_ZNK6icu_6715AlphabeticIndex14ImmutableIndex9getBucketEi, @function
_ZNK6icu_6715AlphabeticIndex14ImmutableIndex9getBucketEi:
.LFB2527:
	.cfi_startproc
	endbr64
	testl	%esi, %esi
	js	.L313
	movq	8(%rdi), %rax
	movq	16(%rax), %rdi
	cmpl	8(%rdi), %esi
	jge	.L313
	jmp	_ZNK6icu_677UVector9elementAtEi@PLT
	.p2align 4,,10
	.p2align 3
.L313:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2527:
	.size	_ZNK6icu_6715AlphabeticIndex14ImmutableIndex9getBucketEi, .-_ZNK6icu_6715AlphabeticIndex14ImmutableIndex9getBucketEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715AlphabeticIndex12clearBucketsEv
	.type	_ZN6icu_6715AlphabeticIndex12clearBucketsEv, @function
_ZN6icu_6715AlphabeticIndex12clearBucketsEv:
.LFB2552:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	72(%rdi), %r12
	testq	%r12, %r12
	je	.L317
	movq	(%r12), %rax
	leaq	_ZN6icu_6710BucketListD0Ev(%rip), %rdx
	movq	%rdi, %rbx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L319
	movq	8(%r12), %rdi
	leaq	16+_ZTVN6icu_6710BucketListE(%rip), %rax
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L320
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	8(%r12), %rdi
.L320:
	movq	16(%r12), %r8
	cmpq	%rdi, %r8
	je	.L321
	testq	%r8, %r8
	je	.L321
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
.L321:
	movq	%r12, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L322:
	movq	$0, 72(%rbx)
	movl	$-1, 16(%rbx)
	movq	$0, 24(%rbx)
.L317:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L319:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	jmp	.L322
	.cfi_endproc
.LFE2552:
	.size	_ZN6icu_6715AlphabeticIndex12clearBucketsEv, .-_ZN6icu_6715AlphabeticIndex12clearBucketsEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715AlphabeticIndex27internalResetBucketIteratorEv
	.type	_ZN6icu_6715AlphabeticIndex27internalResetBucketIteratorEv, @function
_ZN6icu_6715AlphabeticIndex27internalResetBucketIteratorEv:
.LFB2553:
	.cfi_startproc
	endbr64
	movl	$-1, 16(%rdi)
	movq	$0, 24(%rdi)
	ret
	.cfi_endproc
.LFE2553:
	.size	_ZN6icu_6715AlphabeticIndex27internalResetBucketIteratorEv, .-_ZN6icu_6715AlphabeticIndex27internalResetBucketIteratorEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715AlphabeticIndex9separatedERKNS_13UnicodeStringE
	.type	_ZN6icu_6715AlphabeticIndex9separatedERKNS_13UnicodeStringE, @function
_ZN6icu_6715AlphabeticIndex9separatedERKNS_13UnicodeStringE:
.LFB2556:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%dx, 8(%rdi)
	movq	%rax, (%rdi)
	movswl	8(%rsi), %eax
	testw	%ax, %ax
	js	.L338
	sarl	$5, %eax
.L339:
	testl	%eax, %eax
	je	.L337
	xorl	%r12d, %r12d
	leaq	-42(%rbp), %r14
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L349:
	sarl	$5, %eax
	cmpl	%eax, %r12d
	jge	.L337
.L342:
	movl	$847, %eax
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movw	%ax, -42(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
.L343:
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movq	%r13, %rdi
	movl	%eax, %esi
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	movl	%r12d, %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	call	_ZNK6icu_6713UnicodeString11moveIndex32Eii@PLT
	movl	%eax, %r12d
	movswl	8(%rbx), %eax
	testw	%ax, %ax
	jns	.L349
	cmpl	12(%rbx), %r12d
	jl	.L342
.L337:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L350
	addq	$16, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L338:
	.cfi_restore_state
	movl	12(%rsi), %eax
	jmp	.L339
.L350:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2556:
	.size	_ZN6icu_6715AlphabeticIndex9separatedERKNS_13UnicodeStringE, .-_ZN6icu_6715AlphabeticIndex9separatedERKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715AlphabeticIndex6RecordC2ERKNS_13UnicodeStringEPKv
	.type	_ZN6icu_6715AlphabeticIndex6RecordC2ERKNS_13UnicodeStringEPKv, @function
_ZN6icu_6715AlphabeticIndex6RecordC2ERKNS_13UnicodeStringEPKv:
.LFB2574:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	leaq	8(%rdi), %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	%r12, 72(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2574:
	.size	_ZN6icu_6715AlphabeticIndex6RecordC2ERKNS_13UnicodeStringEPKv, .-_ZN6icu_6715AlphabeticIndex6RecordC2ERKNS_13UnicodeStringEPKv
	.globl	_ZN6icu_6715AlphabeticIndex6RecordC1ERKNS_13UnicodeStringEPKv
	.set	_ZN6icu_6715AlphabeticIndex6RecordC1ERKNS_13UnicodeStringEPKv,_ZN6icu_6715AlphabeticIndex6RecordC2ERKNS_13UnicodeStringEPKv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715AlphabeticIndex6RecordD2Ev
	.type	_ZN6icu_6715AlphabeticIndex6RecordD2Ev, @function
_ZN6icu_6715AlphabeticIndex6RecordD2Ev:
.LFB2577:
	.cfi_startproc
	endbr64
	addq	$8, %rdi
	jmp	_ZN6icu_6713UnicodeStringD1Ev@PLT
	.cfi_endproc
.LFE2577:
	.size	_ZN6icu_6715AlphabeticIndex6RecordD2Ev, .-_ZN6icu_6715AlphabeticIndex6RecordD2Ev
	.globl	_ZN6icu_6715AlphabeticIndex6RecordD1Ev
	.set	_ZN6icu_6715AlphabeticIndex6RecordD1Ev,_ZN6icu_6715AlphabeticIndex6RecordD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715AlphabeticIndex6BucketC2ERKNS_13UnicodeStringES4_25UAlphabeticIndexLabelType
	.type	_ZN6icu_6715AlphabeticIndex6BucketC2ERKNS_13UnicodeStringES4_25UAlphabeticIndexLabelType, @function
_ZN6icu_6715AlphabeticIndex6BucketC2ERKNS_13UnicodeStringES4_25UAlphabeticIndexLabelType:
.LFB2593:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6715AlphabeticIndex6BucketE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movl	%ecx, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	leaq	8(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -8(%rdi)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	leaq	72(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movl	%r12d, 136(%rbx)
	movq	$0, 144(%rbx)
	movl	$-1, 152(%rbx)
	movq	$0, 160(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2593:
	.size	_ZN6icu_6715AlphabeticIndex6BucketC2ERKNS_13UnicodeStringES4_25UAlphabeticIndexLabelType, .-_ZN6icu_6715AlphabeticIndex6BucketC2ERKNS_13UnicodeStringES4_25UAlphabeticIndexLabelType
	.globl	_ZN6icu_6715AlphabeticIndex6BucketC1ERKNS_13UnicodeStringES4_25UAlphabeticIndexLabelType
	.set	_ZN6icu_6715AlphabeticIndex6BucketC1ERKNS_13UnicodeStringES4_25UAlphabeticIndexLabelType,_ZN6icu_6715AlphabeticIndex6BucketC2ERKNS_13UnicodeStringES4_25UAlphabeticIndexLabelType
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715AlphabeticIndexD2Ev
	.type	_ZN6icu_6715AlphabeticIndexD2Ev, @function
_ZN6icu_6715AlphabeticIndexD2Ev:
.LFB2535:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6715AlphabeticIndexE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%rax, (%rdi)
	movq	56(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L357
	movq	(%rdi), %rax
	call	*8(%rax)
.L357:
	movq	64(%r12), %rdi
	testq	%rdi, %rdi
	je	.L358
	movq	(%rdi), %rax
	call	*8(%rax)
.L358:
	movq	48(%r12), %rdi
	testq	%rdi, %rdi
	je	.L359
	movq	(%rdi), %rax
	call	*8(%rax)
.L359:
	movq	72(%r12), %r13
	testq	%r13, %r13
	je	.L360
	movq	0(%r13), %rax
	leaq	_ZN6icu_6710BucketListD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L361
	movq	8(%r13), %rdi
	leaq	16+_ZTVN6icu_6710BucketListE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%rdi, %rdi
	je	.L362
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	8(%r13), %rdi
.L362:
	movq	16(%r13), %r8
	cmpq	%rdi, %r8
	je	.L363
	testq	%r8, %r8
	je	.L363
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
.L363:
	movq	%r13, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L360:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L364
	movq	(%rdi), %rax
	call	*8(%rax)
.L364:
	movq	40(%r12), %rdi
	testq	%rdi, %rdi
	je	.L365
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
.L365:
	leaq	336(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	272(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	208(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	144(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	80(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L361:
	.cfi_restore_state
	movq	%r13, %rdi
	call	*%rax
	jmp	.L360
	.cfi_endproc
.LFE2535:
	.size	_ZN6icu_6715AlphabeticIndexD2Ev, .-_ZN6icu_6715AlphabeticIndexD2Ev
	.globl	_ZN6icu_6715AlphabeticIndexD1Ev
	.set	_ZN6icu_6715AlphabeticIndexD1Ev,_ZN6icu_6715AlphabeticIndexD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715AlphabeticIndexD0Ev
	.type	_ZN6icu_6715AlphabeticIndexD0Ev, @function
_ZN6icu_6715AlphabeticIndexD0Ev:
.LFB2537:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN6icu_6715AlphabeticIndexD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2537:
	.size	_ZN6icu_6715AlphabeticIndexD0Ev, .-_ZN6icu_6715AlphabeticIndexD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715AlphabeticIndex9addLabelsERKNS_10UnicodeSetER10UErrorCode
	.type	_ZN6icu_6715AlphabeticIndex9addLabelsERKNS_10UnicodeSetER10UErrorCode, @function
_ZN6icu_6715AlphabeticIndex9addLabelsERKNS_10UnicodeSetER10UErrorCode:
.LFB2538:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	(%rdx), %eax
	movq	%rdi, %r12
	testl	%eax, %eax
	jle	.L417
.L398:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L417:
	.cfi_restore_state
	movq	40(%rdi), %rdi
	call	_ZN6icu_6710UnicodeSet6addAllERKS0_@PLT
	movq	72(%r12), %r13
	testq	%r13, %r13
	je	.L398
	movq	0(%r13), %rax
	leaq	_ZN6icu_6710BucketListD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L400
	movq	8(%r13), %rdi
	leaq	16+_ZTVN6icu_6710BucketListE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%rdi, %rdi
	je	.L401
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	8(%r13), %rdi
.L401:
	movq	16(%r13), %r8
	cmpq	%rdi, %r8
	je	.L402
	testq	%r8, %r8
	je	.L402
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
.L402:
	movq	%r13, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L403:
	movq	$0, 72(%r12)
	movq	%r12, %rax
	movl	$-1, 16(%r12)
	movq	$0, 24(%r12)
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L400:
	.cfi_restore_state
	movq	%r13, %rdi
	call	*%rax
	jmp	.L403
	.cfi_endproc
.LFE2538:
	.size	_ZN6icu_6715AlphabeticIndex9addLabelsERKNS_10UnicodeSetER10UErrorCode, .-_ZN6icu_6715AlphabeticIndex9addLabelsERKNS_10UnicodeSetER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715AlphabeticIndex10initLabelsERNS_7UVectorER10UErrorCode
	.type	_ZNK6icu_6715AlphabeticIndex10initLabelsERNS_7UVectorER10UErrorCode, @function
_ZNK6icu_6715AlphabeticIndex10initLabelsERNS_7UVectorER10UErrorCode:
.LFB2547:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$328, %rsp
	movq	%rdi, -280(%rbp)
	movq	%rdx, %rdi
	movq	%rsi, -304(%rbp)
	movq	%rdx, -288(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6711Normalizer215getNFKDInstanceER10UErrorCode@PLT
	movl	(%rbx), %r11d
	movq	%rax, -344(%rbp)
	testl	%r11d, %r11d
	jle	.L528
.L418:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L529
	addq	$328, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L528:
	.cfi_restore_state
	movq	-280(%rbp), %rbx
	xorl	%esi, %esi
	movq	48(%rbx), %rdi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	48(%rbx), %rdi
	movq	%rax, -320(%rbp)
	movl	8(%rdi), %eax
	leal	-1(%rax), %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	40(%rbx), %rsi
	leaq	-256(%rbp), %rbx
	movq	%rbx, %rdi
	movq	%rax, -336(%rbp)
	movq	%rbx, %r14
	call	_ZN6icu_6718UnicodeSetIteratorC1ERKNS_10UnicodeSetE@PLT
	leaq	-260(%rbp), %rax
	movq	%rax, -312(%rbp)
	.p2align 4,,10
	.p2align 3
.L479:
	leaq	-128(%rbp), %rax
	movq	%rax, -328(%rbp)
.L420:
	movq	%r14, %rdi
	call	_ZN6icu_6718UnicodeSetIterator4nextEv@PLT
	testb	%al, %al
	je	.L421
.L534:
	movq	%r14, %rdi
	call	_ZN6icu_6718UnicodeSetIterator9getStringEv@PLT
	movswl	8(%rax), %r13d
	movq	%rax, %r12
	testw	%r13w, %r13w
	js	.L422
	sarl	$5, %r13d
.L423:
	movl	$1, %ecx
	movl	%r13d, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_6713UnicodeString17hasMoreChar32ThanEiii@PLT
	movl	%eax, %ebx
	testb	%al, %al
	je	.L424
	movzwl	8(%r12), %edx
	leal	-1(%r13), %r15d
	testw	%dx, %dx
	js	.L425
	movswl	%dx, %eax
	sarl	$5, %eax
	cmpl	%r15d, %eax
	jbe	.L490
.L537:
	andl	$2, %edx
	leaq	10(%r12), %rcx
	jne	.L428
	movq	24(%r12), %rcx
.L428:
	movslq	%r15d, %rdx
	movl	$1, %ebx
	cmpw	$42, (%rcx,%rdx,2)
	leaq	(%rdx,%rdx), %rsi
	je	.L530
.L424:
	movq	-280(%rbp), %rax
	movq	-288(%rbp), %rcx
	movq	%r12, %rsi
	movq	-320(%rbp), %rdx
	movq	64(%rax), %rdi
	movq	(%rdi), %rax
	call	*56(%rax)
	testl	%eax, %eax
	js	.L420
	movq	-280(%rbp), %rax
	movq	-288(%rbp), %rcx
	movq	%r12, %rsi
	movq	-336(%rbp), %rdx
	movq	64(%rax), %rdi
	movq	(%rdi), %rax
	call	*56(%rax)
	testl	%eax, %eax
	jns	.L420
	movq	-280(%rbp), %rax
	movq	64(%rax), %r13
	testb	%bl, %bl
	jne	.L434
.L526:
	xorl	%ebx, %ebx
.L435:
	movq	-304(%rbp), %rax
	movl	8(%rax), %eax
	movl	%eax, -296(%rbp)
	testl	%eax, %eax
	je	.L444
	movq	%rbx, -352(%rbp)
	xorl	%r9d, %r9d
	movq	%r13, %r15
	movq	%r14, -360(%rbp)
	movl	%r9d, %r13d
	.p2align 4,,10
	.p2align 3
.L448:
	movl	-296(%rbp), %eax
	movq	-304(%rbp), %rdi
	addl	%r13d, %eax
	movl	%eax, %r8d
	shrl	$31, %r8d
	addl	%eax, %r8d
	sarl	%r8d
	movl	%r8d, %esi
	movl	%r8d, %r14d
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	-312(%rbp), %rcx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movl	$0, -260(%rbp)
	movq	%rax, %rdx
	movq	(%r15), %rax
	call	*56(%rax)
	testl	%eax, %eax
	je	.L522
	js	.L531
	cmpl	%r14d, %r13d
	je	.L532
	movl	%r14d, %r13d
	jmp	.L448
	.p2align 4,,10
	.p2align 3
.L434:
	movq	0(%r13), %rax
	movl	$2, %r10d
	movq	56(%rax), %rbx
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%r10w, -120(%rbp)
	movq	%rax, -128(%rbp)
	movswl	8(%r12), %eax
	testw	%ax, %ax
	js	.L436
	sarl	$5, %eax
.L437:
	testl	%eax, %eax
	je	.L438
	xorl	%r15d, %r15d
	movq	%rbx, -296(%rbp)
	movq	-328(%rbp), %rbx
	movq	%r13, -352(%rbp)
	movl	%r15d, %r13d
	movq	-312(%rbp), %r15
	jmp	.L441
	.p2align 4,,10
	.p2align 3
.L533:
	sarl	$5, %eax
	cmpl	%eax, %r13d
	jge	.L521
.L440:
	movl	$847, %r9d
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movw	%r9w, -260(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
.L441:
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movq	%rbx, %rdi
	movl	%eax, %esi
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	movl	%r13d, %esi
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZNK6icu_6713UnicodeString11moveIndex32Eii@PLT
	movl	%eax, %r13d
	movswl	8(%r12), %eax
	testw	%ax, %ax
	jns	.L533
	cmpl	12(%r12), %r13d
	jl	.L440
.L521:
	movq	-296(%rbp), %rbx
	movq	-352(%rbp), %r13
.L438:
	movq	-328(%rbp), %r15
	movq	-288(%rbp), %rcx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%r15, %rdx
	call	*%rbx
	testl	%eax, %eax
	jne	.L442
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6718UnicodeSetIterator4nextEv@PLT
	testb	%al, %al
	jne	.L534
	.p2align 4,,10
	.p2align 3
.L421:
	movq	-288(%rbp), %rax
	movq	%r14, %rbx
	movl	(%rax), %eax
	testl	%eax, %eax
	jg	.L480
	movq	-304(%rbp), %rax
	movl	8(%rax), %esi
	movq	-280(%rbp), %rax
	movl	32(%rax), %edi
	leal	-1(%rsi), %r13d
	cmpl	%r13d, %edi
	jge	.L480
	movq	-304(%rbp), %r15
	xorl	%r12d, %r12d
	movl	$-1, %ecx
	xorl	%r14d, %r14d
	testl	%esi, %esi
	jle	.L480
	.p2align 4,,10
	.p2align 3
.L482:
	addl	$1, %r14d
	movl	%edi, %eax
	imull	%r14d, %eax
	cltd
	idivl	%r13d
	cmpl	%eax, %ecx
	je	.L535
	addl	$1, %r12d
	cmpl	%r12d, %esi
	jle	.L480
	movl	%eax, %ecx
	jmp	.L482
	.p2align 4,,10
	.p2align 3
.L531:
	cmpl	%r14d, %r13d
	je	.L536
	movl	%r14d, -296(%rbp)
	jmp	.L448
	.p2align 4,,10
	.p2align 3
.L422:
	movl	12(%rax), %r13d
	jmp	.L423
	.p2align 4,,10
	.p2align 3
.L425:
	movl	12(%r12), %eax
	cmpl	%r15d, %eax
	ja	.L537
.L490:
	movl	$1, %ebx
	jmp	.L424
	.p2align 4,,10
	.p2align 3
.L530:
	subl	$2, %r13d
	cmpl	%r13d, %eax
	jbe	.L429
	cmpw	$42, -2(%rcx,%rsi)
	je	.L424
.L429:
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L430
	xorl	%edx, %edx
	movq	%rax, %rdi
	movl	%r15d, %ecx
	movq	%r12, %rsi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_ii@PLT
	movq	-280(%rbp), %rax
	movq	-288(%rbp), %rcx
	movq	%rbx, %rsi
	movq	-320(%rbp), %rdx
	movq	64(%rax), %rdi
	movq	(%rdi), %rax
	call	*56(%rax)
	testl	%eax, %eax
	jns	.L538
	.p2align 4,,10
	.p2align 3
.L431:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*8(%rax)
	jmp	.L479
	.p2align 4,,10
	.p2align 3
.L522:
	movl	%r14d, %r15d
	movq	-352(%rbp), %rbx
	movq	-360(%rbp), %r14
.L445:
	testl	%r15d, %r15d
	jns	.L449
	notl	%r15d
	movl	%r15d, -296(%rbp)
.L444:
	movq	-288(%rbp), %rax
	movl	(%rax), %r8d
	testl	%r8d, %r8d
	jg	.L450
	testq	%rbx, %rbx
	je	.L539
	movl	-296(%rbp), %edx
	movq	-304(%rbp), %rdi
	movq	%rax, %rcx
	movq	%rbx, %rsi
	call	_ZN6icu_677UVector15insertElementAtEPviR10UErrorCode@PLT
	jmp	.L420
	.p2align 4,,10
	.p2align 3
.L442:
	movq	-328(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-280(%rbp), %rax
	movq	64(%rax), %r13
	jmp	.L526
.L430:
	movq	-288(%rbp), %rax
	movq	%r14, %rbx
	movl	$7, (%rax)
	.p2align 4,,10
	.p2align 3
.L480:
	movq	%rbx, %rdi
	call	_ZN6icu_6718UnicodeSetIteratorD1Ev@PLT
	jmp	.L418
	.p2align 4,,10
	.p2align 3
.L449:
	movq	-304(%rbp), %rdi
	movl	%r15d, %esi
	leaq	-192(%rbp), %r13
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	-344(%rbp), %rdi
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rax, -296(%rbp)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rcx
	movq	(%rdi), %rax
	movq	%rcx, -192(%rbp)
	movl	$2, %ecx
	movw	%cx, -184(%rbp)
	movq	-312(%rbp), %rcx
	movl	$0, -260(%rbp)
	call	*24(%rax)
	movq	-344(%rbp), %rdi
	movl	$2, %esi
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rdx
	movq	%rdx, -128(%rbp)
	movq	-312(%rbp), %rcx
	movq	(%rdi), %rax
	movq	-328(%rbp), %rdx
	movw	%si, -120(%rbp)
	movq	-296(%rbp), %rsi
	call	*24(%rax)
	movl	-260(%rbp), %edi
	testl	%edi, %edi
	jle	.L455
	movq	-328(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L454:
	testq	%rbx, %rbx
	jne	.L431
	jmp	.L420
	.p2align 4,,10
	.p2align 3
.L436:
	movl	12(%r12), %eax
	jmp	.L437
	.p2align 4,,10
	.p2align 3
.L532:
	movl	%r14d, %r15d
	movq	-352(%rbp), %rbx
	movq	-360(%rbp), %r14
	leal	1(%r15), %r10d
	notl	%r10d
	movl	%r10d, %r15d
	jmp	.L445
	.p2align 4,,10
	.p2align 3
.L535:
	movl	%r12d, %esi
	movq	%r15, %rdi
	movl	%eax, -288(%rbp)
	call	_ZN6icu_677UVector15removeElementAtEi@PLT
	movl	8(%r15), %esi
	cmpl	%r12d, %esi
	jle	.L480
	movq	-280(%rbp), %rax
	movl	32(%rax), %edi
	movl	-288(%rbp), %eax
	movl	%eax, %ecx
	jmp	.L482
	.p2align 4,,10
	.p2align 3
.L536:
	movl	%r14d, %r15d
	movq	-352(%rbp), %rbx
	movq	-360(%rbp), %r14
	notl	%r15d
	jmp	.L445
	.p2align 4,,10
	.p2align 3
.L455:
	movl	$2147483647, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZNK6icu_6713UnicodeString11countChar32Eii@PLT
	movq	-328(%rbp), %rdi
	movl	$2147483647, %edx
	xorl	%esi, %esi
	movl	%eax, -352(%rbp)
	call	_ZNK6icu_6713UnicodeString11countChar32Eii@PLT
	movl	-352(%rbp), %r9d
	subl	%eax, %r9d
	jne	.L527
	movzwl	-120(%rbp), %eax
	testw	%ax, %ax
	js	.L458
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L459:
	movzwl	-184(%rbp), %edx
	testw	%dx, %dx
	js	.L460
	movswl	%dx, %r11d
	sarl	$5, %r11d
.L461:
	testb	$1, %al
	je	.L462
	movl	%edx, %eax
	notl	%eax
	andl	$1, %eax
.L463:
	movsbl	%al, %r9d
	testl	%r9d, %r9d
	jne	.L527
	movq	-296(%rbp), %rax
	movzwl	8(%rax), %ecx
	testw	%cx, %cx
	js	.L467
	movswl	%cx, %esi
	sarl	$5, %esi
.L468:
	movzwl	8(%r12), %eax
	testw	%ax, %ax
	js	.L469
	movswl	%ax, %edx
	sarl	$5, %edx
.L470:
	testb	$1, %cl
	je	.L471
	notl	%eax
	andl	$1, %eax
.L472:
	shrb	$7, %al
	movb	%al, -296(%rbp)
	jmp	.L457
	.p2align 4,,10
	.p2align 3
.L450:
	movl	-296(%rbp), %edx
	movq	-304(%rbp), %rdi
	movq	%rax, %rcx
	xorl	%esi, %esi
	call	_ZN6icu_677UVector15insertElementAtEPviR10UErrorCode@PLT
	jmp	.L454
.L527:
	shrl	$31, %r9d
	movl	%r9d, -296(%rbp)
.L457:
	movq	-328(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, -296(%rbp)
	je	.L454
	movq	-288(%rbp), %rax
	movl	(%rax), %edx
	testl	%edx, %edx
	jg	.L476
	testq	%rbx, %rbx
	je	.L540
	movq	-304(%rbp), %rdi
	movl	%r15d, %edx
	movq	%rbx, %rsi
	call	_ZN6icu_677UVector12setElementAtEPvi@PLT
	jmp	.L420
.L539:
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L452
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	-288(%rbp), %rcx
	movl	-296(%rbp), %edx
	movq	%r13, %rsi
	movq	-304(%rbp), %rdi
	call	_ZN6icu_677UVector15insertElementAtEPviR10UErrorCode@PLT
	jmp	.L420
.L458:
	movl	-116(%rbp), %ecx
	jmp	.L459
.L476:
	movq	-304(%rbp), %rdi
	movl	%r15d, %edx
	xorl	%esi, %esi
	call	_ZN6icu_677UVector12setElementAtEPvi@PLT
	jmp	.L454
.L462:
	testl	%ecx, %ecx
	movl	$0, %r8d
	cmovle	%ecx, %r8d
	js	.L464
	movl	%ecx, %edx
	subl	%r8d, %edx
	cmpl	%ecx, %edx
	cmovle	%edx, %ecx
	movl	%ecx, %r9d
.L464:
	testb	$2, %al
	leaq	-118(%rbp), %rcx
	cmove	-104(%rbp), %rcx
	movl	%r11d, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZNK6icu_6713UnicodeString23doCompareCodePointOrderEiiPKDsii@PLT
	jmp	.L463
.L460:
	movl	-180(%rbp), %r11d
	jmp	.L461
.L540:
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L478
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	-304(%rbp), %rdi
	movl	%r15d, %edx
	movq	%r13, %rsi
	call	_ZN6icu_677UVector12setElementAtEPvi@PLT
	jmp	.L420
.L471:
	testl	%esi, %esi
	movl	$0, %r8d
	cmovle	%esi, %r8d
	js	.L473
	movl	%esi, %eax
	subl	%r8d, %eax
	cmpl	%esi, %eax
	cmovle	%eax, %esi
	movl	%esi, %r9d
.L473:
	andl	$2, %ecx
	je	.L474
	movq	-296(%rbp), %rcx
	addq	$10, %rcx
.L475:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_6713UnicodeString23doCompareCodePointOrderEiiPKDsii@PLT
	jmp	.L472
.L469:
	movl	12(%r12), %edx
	jmp	.L470
.L467:
	movl	12(%rax), %esi
	jmp	.L468
.L474:
	movq	-296(%rbp), %rax
	movq	24(%rax), %rcx
	jmp	.L475
.L452:
	movq	-288(%rbp), %rax
	movl	-296(%rbp), %edx
	xorl	%esi, %esi
	movq	-304(%rbp), %rdi
	movl	$7, (%rax)
	movq	%rax, %rcx
	call	_ZN6icu_677UVector15insertElementAtEPviR10UErrorCode@PLT
	jmp	.L420
.L538:
	movq	-280(%rbp), %rax
	movq	-288(%rbp), %rcx
	movq	%rbx, %rsi
	movq	-336(%rbp), %rdx
	movq	64(%rax), %rdi
	movq	(%rdi), %rax
	call	*56(%rax)
	testl	%eax, %eax
	jns	.L431
	movq	-280(%rbp), %rax
	movq	%rbx, %r12
	movq	64(%rax), %r13
	jmp	.L435
.L478:
	movq	-288(%rbp), %rax
	movq	-304(%rbp), %rdi
	movl	%r15d, %edx
	xorl	%esi, %esi
	movl	$7, (%rax)
	call	_ZN6icu_677UVector12setElementAtEPvi@PLT
	jmp	.L420
.L529:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2547:
	.size	_ZNK6icu_6715AlphabeticIndex10initLabelsERNS_7UVectorER10UErrorCode, .-_ZNK6icu_6715AlphabeticIndex10initLabelsERNS_7UVectorER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715AlphabeticIndex16createBucketListER10UErrorCode
	.type	_ZNK6icu_6715AlphabeticIndex16createBucketListER10UErrorCode, @function
_ZNK6icu_6715AlphabeticIndex16createBucketListER10UErrorCode:
.LFB2550:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-656(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r14, %rdi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$776, %rsp
	movq	%rsi, -792(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r14, -752(%rbp)
	call	_ZN6icu_677UVectorC1ER10UErrorCode@PLT
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %r12
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZN6icu_677UVector10setDeleterEPFvPvE@PLT
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZNK6icu_6715AlphabeticIndex10initLabelsERNS_7UVectorER10UErrorCode
	movl	(%rbx), %r11d
	testl	%r11d, %r11d
	jg	.L542
	leaq	-688(%rbp), %rdi
	movq	%rbx, %rsi
	movq	%rdi, -768(%rbp)
	call	_ZN6icu_679UVector64C1ER10UErrorCode@PLT
	movq	64(%r13), %rdi
	movq	%rbx, %rdx
	movl	$1, %esi
	movq	(%rdi), %rax
	call	*192(%rax)
	movl	$0, -780(%rbp)
	cmpl	$20, %eax
	je	.L697
.L543:
	xorl	%eax, %eax
	leaq	-480(%rbp), %rdi
	movl	$26, %ecx
	movq	%rdi, -808(%rbp)
	rep stosq
	leaq	-272(%rbp), %rdi
	movl	$26, %ecx
	movq	%rdi, -800(%rbp)
	rep stosq
	movl	$40, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, -728(%rbp)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L544
	movq	-792(%rbp), %rbx
	xorl	%r15d, %r15d
	movq	%rbx, %rsi
	call	_ZN6icu_677UVectorC1ER10UErrorCode@PLT
	movl	(%rbx), %r10d
	testl	%r10d, %r10d
	jle	.L698
.L545:
	movq	-728(%rbp), %rdi
	movq	(%rdi), %rax
	call	*8(%rax)
.L547:
	movq	-768(%rbp), %rdi
	call	_ZN6icu_679UVector64D1Ev@PLT
.L542:
	movq	-752(%rbp), %rdi
	call	_ZN6icu_677UVectorD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L699
	addq	$776, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L698:
	.cfi_restore_state
	movq	-728(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN6icu_677UVector10setDeleterEPFvPvE@PLT
	movq	0(%r13), %rax
	leaq	_ZNK6icu_6715AlphabeticIndex17getUnderflowLabelEv(%rip), %rdx
	movq	80(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L548
	leaq	208(%r13), %r14
.L549:
	movl	$168, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L550
	leaq	16+_ZTVN6icu_6715AlphabeticIndex6BucketE(%rip), %rax
	movq	%r14, %rsi
	leaq	8(%r12), %rdi
	movq	%rax, (%r12)
	leaq	336(%r13), %rbx
	movq	%rbx, -816(%rbp)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	leaq	72(%r12), %rdi
	movq	%rbx, %rsi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	-792(%rbp), %r14
	movq	-728(%rbp), %rdi
	movq	%r12, %rsi
	movl	$1, 136(%r12)
	movq	$0, 144(%r12)
	movq	%r14, %rdx
	movl	$-1, 152(%r12)
	movq	$0, 160(%r12)
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	movl	(%r14), %r9d
	testl	%r9d, %r9d
	jg	.L551
	movl	-648(%rbp), %r8d
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %edi
	movq	%rax, -608(%rbp)
	movw	%di, -600(%rbp)
	testl	%r8d, %r8d
	jle	.L648
	leaq	-608(%rbp), %rax
	movq	%rbx, -744(%rbp)
	movl	$0, -716(%rbp)
	movl	$-1, -720(%rbp)
	movb	$0, -782(%rbp)
	movb	$0, -783(%rbp)
	movq	%rax, -736(%rbp)
	.p2align 4,,10
	.p2align 3
.L607:
	movl	-716(%rbp), %esi
	movq	-752(%rbp), %rdi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	64(%r13), %rdi
	movq	-744(%rbp), %rdx
	movq	%r14, %rcx
	movq	%rax, %r12
	movq	(%rdi), %rax
	movq	%r12, %rsi
	call	*56(%rax)
	testl	%eax, %eax
	js	.L553
	movq	%r13, %rbx
	movb	$0, -712(%rbp)
	movl	-720(%rbp), %r13d
	jmp	.L554
	.p2align 4,,10
	.p2align 3
.L649:
	movb	$1, -712(%rbp)
.L554:
	movq	48(%rbx), %rdi
	addl	$1, %r13d
	movl	%r13d, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	64(%rbx), %rdi
	movq	%r14, %rcx
	movq	%r12, %rsi
	movq	%rax, %r15
	movq	(%rdi), %rax
	movq	%r15, %rdx
	call	*56(%rax)
	testl	%eax, %eax
	jns	.L649
	cmpb	$0, -712(%rbp)
	movl	%r13d, -720(%rbp)
	movq	%rbx, %r13
	je	.L651
	movq	-728(%rbp), %rax
	cmpl	$1, 8(%rax)
	jle	.L651
	movq	(%rbx), %rax
	leaq	_ZNK6icu_6715AlphabeticIndex14getInflowLabelEv(%rip), %rcx
	movq	48(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L555
	leaq	80(%rbx), %rsi
.L556:
	movl	$168, %edi
	movq	%rsi, -760(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r10
	testq	%rax, %rax
	je	.L557
	leaq	16+_ZTVN6icu_6715AlphabeticIndex6BucketE(%rip), %rax
	movq	-760(%rbp), %rsi
	leaq	8(%r10), %rdi
	movq	%r10, -712(%rbp)
	movq	%rax, (%r10)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	-712(%rbp), %r10
	movq	-744(%rbp), %rsi
	leaq	72(%r10), %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	-712(%rbp), %r10
	movq	-728(%rbp), %rdi
	movq	%r14, %rdx
	movl	$2, 136(%r10)
	movq	%r10, %rsi
	movq	$0, 144(%r10)
	movl	$-1, 152(%r10)
	movq	$0, 160(%r10)
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	movq	%r15, -744(%rbp)
	.p2align 4,,10
	.p2align 3
.L553:
	movl	$1, %r9d
	xorl	%r8d, %r8d
	movl	$1, %edx
	xorl	%esi, %esi
	leaq	_ZN6icu_6712_GLOBAL__N_1L4BASEE(%rip), %rcx
	movq	%r12, %rdi
	leaq	_ZN6icu_6712_GLOBAL__N_1L4BASEE(%rip), %rbx
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	movq	%r12, %r15
	testb	%al, %al
	jne	.L558
	movzwl	8(%r12), %eax
	testw	%ax, %ax
	js	.L559
	movswl	%ax, %edx
	sarl	$5, %edx
.L560:
	cmpl	$1, %edx
	jbe	.L561
	testb	$2, %al
	je	.L562
	leaq	10(%r12), %rax
.L563:
	movzwl	2(%rax), %r15d
	leal	-10241(%r15), %eax
	cmpw	$254, %ax
	ja	.L561
	leal	-10240(%r15), %eax
	movq	-736(%rbp), %rdi
	movq	%rax, %r15
	movq	%rax, %rcx
	movq	%rax, %rbx
	movl	$3435973837, %eax
	imulq	%r15, %rax
	shrq	$35, %rax
	leal	(%rax,%rax,4), %eax
	addl	%eax, %eax
	subl	%eax, %ecx
	movl	%ecx, %eax
	addl	$48, %eax
	movw	%ax, -690(%rbp)
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	movzwl	-600(%rbp), %eax
	testw	%ax, %ax
	js	.L564
	movswl	%ax, %edx
	sarl	$5, %edx
.L565:
	leaq	-690(%rbp), %r10
	movq	-736(%rbp), %rdi
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	%r10, %rcx
	movl	$1, %r9d
	movq	%r10, -712(%rbp)
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	cmpl	$9, %r15d
	movq	-712(%rbp), %r10
	jg	.L700
.L567:
	movl	$21123, %esi
	movq	-736(%rbp), %rdi
	movl	$1, %ecx
	xorl	%edx, %edx
	movw	%si, -690(%rbp)
	movq	%r10, %rsi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movq	%rax, %r15
.L558:
	movl	$168, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r10
	testq	%rax, %rax
	je	.L557
	leaq	16+_ZTVN6icu_6715AlphabeticIndex6BucketE(%rip), %rax
	leaq	8(%r10), %rdi
	movq	%r15, %rsi
	movq	%r10, -712(%rbp)
	movq	%rax, (%r10)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	-712(%rbp), %r10
	movq	%r12, %rsi
	leaq	72(%r10), %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	-712(%rbp), %r10
	movq	-728(%rbp), %rdi
	movq	%r14, %rdx
	movl	$0, 136(%r10)
	movq	%r10, %rsi
	movq	$0, 144(%r10)
	movl	$-1, 152(%r10)
	movq	$0, 160(%r10)
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	movzwl	8(%r12), %edx
	movq	-712(%rbp), %r10
	testw	%dx, %dx
	js	.L574
	movswl	%dx, %eax
	sarl	$5, %eax
	cmpl	$1, %eax
	je	.L701
.L576:
	cmpl	$2, %eax
	je	.L702
.L580:
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movl	$1, %r9d
	movl	$1, %edx
	leaq	_ZN6icu_6712_GLOBAL__N_1L4BASEE(%rip), %rcx
	movq	%r12, %rdi
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	testb	%al, %al
	jne	.L703
.L591:
	leaq	_ZN6icu_6712_GLOBAL__N_1L4BASEE(%rip), %rax
.L696:
.L600:
	addl	$1, -716(%rbp)
	movl	-716(%rbp), %eax
	cmpl	-648(%rbp), %eax
	jl	.L607
	movq	-792(%rbp), %rax
	movl	(%rax), %r14d
	testl	%r14d, %r14d
	jg	.L654
.L552:
	movq	-728(%rbp), %rax
	cmpl	$1, 8(%rax)
	je	.L704
	movq	0(%r13), %rax
	leaq	_ZNK6icu_6715AlphabeticIndex16getOverflowLabelEv(%rip), %rdx
	movq	64(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L613
	addq	$144, %r13
.L614:
	movl	$168, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L615
	leaq	16+_ZTVN6icu_6715AlphabeticIndex6BucketE(%rip), %rax
	leaq	8(%r15), %rdi
	movq	%r13, %rsi
	movq	%rax, (%r15)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	-744(%rbp), %rsi
	leaq	72(%r15), %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	-792(%rbp), %rdx
	movq	-728(%rbp), %rdi
	movq	%r15, %rsi
	movl	$3, 136(%r15)
	movq	$0, 144(%r15)
	movl	$-1, 152(%r15)
	movq	$0, 160(%r15)
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	movzbl	-782(%rbp), %r10d
	testb	%r10b, %r10b
	je	.L616
	movzbl	-783(%rbp), %r9d
	movq	-800(%rbp), %rdi
	xorl	%eax, %eax
	xorl	%esi, %esi
	movq	-808(%rbp), %r8
	jmp	.L619
	.p2align 4,,10
	.p2align 3
.L617:
	movq	%rdx, %rsi
	testq	%rcx, %rcx
	je	.L618
.L639:
	movq	%rdx, 144(%rcx)
	movq	%rdx, %rsi
	movl	%r10d, %r9d
.L618:
	addq	$8, %rax
	cmpq	$208, %rax
	je	.L705
.L619:
	movq	(%r8,%rax), %rdx
	movq	(%rdi,%rax), %rcx
	testq	%rdx, %rdx
	jne	.L617
	testq	%rsi, %rsi
	je	.L618
	testq	%rcx, %rcx
	je	.L618
	movq	%rsi, %rdx
	jmp	.L639
	.p2align 4,,10
	.p2align 3
.L697:
	movq	64(%r13), %rdi
	movq	-792(%rbp), %rsi
	movq	(%rdi), %rax
	call	*240(%rax)
	movl	%eax, -780(%rbp)
	jmp	.L543
.L550:
	movq	-792(%rbp), %rax
	movl	$7, (%rax)
.L551:
	xorl	%r15d, %r15d
	jmp	.L545
.L705:
	movb	%r9b, -783(%rbp)
.L616:
	movq	-792(%rbp), %rax
	movl	(%rax), %ebx
	testl	%ebx, %ebx
	jg	.L654
	cmpb	$0, -783(%rbp)
	jne	.L620
	movl	$24, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L615
	movq	-728(%rbp), %r13
	leaq	16+_ZTVN6icu_6710BucketListE(%rip), %rax
	movq	%rax, (%r15)
	movl	8(%r13), %r11d
	movq	%r13, 8(%r15)
	movq	%r13, 16(%r15)
	testl	%r11d, %r11d
	jle	.L622
	xorl	%ebx, %ebx
.L623:
	movl	%ebx, %r12d
	movq	%r13, %rdi
	addl	$1, %ebx
	movl	%r12d, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movl	%r12d, 152(%rax)
	cmpl	%ebx, 8(%r13)
	jg	.L623
.L622:
	movq	-736(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L547
.L654:
	xorl	%r15d, %r15d
.L608:
	movq	-736(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpq	$0, -728(%rbp)
	jne	.L545
	jmp	.L547
	.p2align 4,,10
	.p2align 3
.L574:
	movl	12(%r12), %eax
	cmpl	$1, %eax
	jne	.L576
.L701:
	andl	$2, %edx
	leaq	10(%r12), %rax
	jne	.L578
	movq	24(%r12), %rax
.L578:
	movzwl	(%rax), %eax
	leal	-65(%rax), %edx
	cmpw	$25, %dx
	ja	.L580
	subl	$65, %eax
	cltq
	movq	%r10, -480(%rbp,%rax,8)
	jmp	.L580
	.p2align 4,,10
	.p2align 3
.L651:
	movq	%r15, -744(%rbp)
	jmp	.L553
	.p2align 4,,10
	.p2align 3
.L703:
	movq	-768(%rbp), %rbx
	movq	64(%r13), %r15
	movq	%rbx, %rdi
	call	_ZN6icu_679UVector6417removeAllElementsEv@PLT
	movq	%r14, %rcx
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZNK6icu_6717RuleBasedCollator14internalGetCEsERKNS_13UnicodeStringERNS_9UVector64ER10UErrorCode@PLT
	movl	(%r14), %ecx
	testl	%ecx, %ecx
	jg	.L591
	movl	-680(%rbp), %edx
	testl	%edx, %edx
	jle	.L591
	movq	-664(%rbp), %rax
	subl	$1, %edx
	movl	-780(%rbp), %ecx
	xorl	%esi, %esi
	leaq	8(%rax,%rdx,8), %rdx
	jmp	.L594
	.p2align 4,,10
	.p2align 3
.L706:
	movl	$1, %esi
.L592:
	addq	$8, %rax
	cmpq	%rax, %rdx
	je	.L591
.L594:
	cmpl	%ecx, 4(%rax)
	jbe	.L592
	testb	%sil, %sil
	je	.L706
	movswl	8(%r12), %eax
	movb	%sil, -781(%rbp)
	movl	%eax, %edx
	sarl	$5, %eax
	testw	%dx, %dx
	jns	.L595
	movl	12(%r12), %eax
.L595:
	leal	-1(%rax), %ecx
	testl	%eax, %eax
	je	.L591
	andl	$2, %edx
	leaq	10(%r12), %rax
	jne	.L598
	movq	24(%r12), %rax
.L598:
	movslq	%ecx, %rcx
	cmpw	$-1, (%rax,%rcx,2)
	leaq	_ZN6icu_6712_GLOBAL__N_1L4BASEE(%rip), %rax
	je	.L696
	movq	-728(%rbp), %rax
	movq	%r12, -776(%rbp)
	movl	8(%rax), %ecx
	leal	-2(%rcx), %r15d
	movl	%ecx, -712(%rbp)
	movl	%r15d, %ebx
	movq	%rax, %r15
	jmp	.L605
	.p2align 4,,10
	.p2align 3
.L601:
	subl	$1, %ebx
.L605:
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movl	136(%rax), %edx
	testl	%edx, %edx
	jne	.L600
	cmpq	$0, 144(%rax)
	jne	.L601
	movq	-768(%rbp), %r12
	movq	64(%r13), %r9
	movq	%rax, -712(%rbp)
	movq	%r12, %rdi
	movq	%r9, -760(%rbp)
	call	_ZN6icu_679UVector6417removeAllElementsEv@PLT
	movq	-712(%rbp), %rax
	movq	%r14, %rcx
	movq	%r12, %rdx
	movq	-760(%rbp), %r9
	leaq	72(%rax), %rsi
	movq	%r9, %rdi
	call	_ZNK6icu_6717RuleBasedCollator14internalGetCEsERKNS_13UnicodeStringERNS_9UVector64ER10UErrorCode@PLT
	movl	(%r14), %eax
	testl	%eax, %eax
	movq	-712(%rbp), %rax
	jg	.L689
	movl	-680(%rbp), %ecx
	testl	%ecx, %ecx
	jle	.L689
	movq	-664(%rbp), %rdx
	subl	$1, %ecx
	movl	-780(%rbp), %r12d
	movzbl	-781(%rbp), %edi
	leaq	8(%rdx,%rcx,8), %rsi
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L604:
	cmpl	%r12d, 4(%rdx)
	jbe	.L603
	testb	%cl, %cl
	jne	.L601
	movl	%edi, %ecx
.L603:
	addq	$8, %rdx
	cmpq	%rsi, %rdx
	jne	.L604
.L689:
	movq	-776(%rbp), %r12
	leaq	-544(%rbp), %r8
	movq	%rax, -776(%rbp)
	movl	$-1, %r15d
	movq	%r8, %rdi
	movq	%r8, -712(%rbp)
	movq	%r12, %rsi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	-712(%rbp), %r8
	xorl	%edx, %edx
	leaq	-690(%rbp), %rsi
	movl	$1, %ecx
	movw	%r15w, -690(%rbp)
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movl	$168, %edi
	movq	%rax, %r12
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	-712(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %r10
	je	.L606
	leaq	16+_ZTVN6icu_6715AlphabeticIndex6BucketE(%rip), %rax
	movq	-816(%rbp), %rsi
	leaq	8(%r10), %rdi
	movq	%r8, -760(%rbp)
	movq	%rax, (%r10)
	movq	%r10, -712(%rbp)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	-712(%rbp), %r10
	movq	%r12, %rsi
	leaq	72(%r10), %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	-712(%rbp), %r10
	movq	-760(%rbp), %r8
	movl	$0, 136(%r10)
	movq	%r8, %rdi
	movq	$0, 144(%r10)
	movl	$-1, 152(%r10)
	movq	$0, 160(%r10)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-712(%rbp), %r10
	movq	-776(%rbp), %r9
	movq	%r14, %rdx
	movq	-728(%rbp), %rdi
	movq	%r9, 144(%r10)
	movq	%r10, %rsi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	movzbl	-781(%rbp), %eax
	movb	%al, -783(%rbp)
	jmp	.L600
	.p2align 4,,10
	.p2align 3
.L561:
	movq	-736(%rbp), %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	movswl	8(%r12), %r9d
	testw	%r9w, %r9w
	js	.L569
	sarl	$5, %r9d
	movl	$1, %r8d
	testl	%r9d, %r9d
	cmovle	%r9d, %r8d
.L570:
	movzwl	-600(%rbp), %eax
	subl	%r8d, %r9d
	testw	%ax, %ax
	js	.L571
	movswl	%ax, %edx
	sarl	$5, %edx
.L572:
	movq	-736(%rbp), %rdi
	movq	%r12, %rcx
	xorl	%esi, %esi
	call	_ZN6icu_6713UnicodeString9doReplaceEiiRKS0_ii@PLT
	movq	%rax, %r15
	jmp	.L558
	.p2align 4,,10
	.p2align 3
.L559:
	movl	12(%r12), %edx
	jmp	.L560
	.p2align 4,,10
	.p2align 3
.L562:
	movq	24(%r12), %rax
	jmp	.L563
	.p2align 4,,10
	.p2align 3
.L702:
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movl	$1, %r9d
	movl	$1, %edx
	leaq	_ZN6icu_6712_GLOBAL__N_1L4BASEE(%rip), %rcx
	movq	%r12, %rdi
	movq	%r10, -712(%rbp)
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	testb	%al, %al
	jne	.L585
	movzwl	8(%r12), %eax
	movq	-712(%rbp), %r10
	testw	%ax, %ax
	js	.L583
	movswl	%ax, %edx
	sarl	$5, %edx
.L584:
	cmpl	$1, %edx
	jbe	.L585
	testb	$2, %al
	je	.L586
	leaq	10(%r12), %rax
.L587:
	movzwl	2(%rax), %eax
	cmpw	$64, %ax
	ja	.L707
.L585:
	leaq	_ZN6icu_6712_GLOBAL__N_1L4BASEE(%rip), %rax
	jmp	.L580
.L707:
	leaq	_ZN6icu_6712_GLOBAL__N_1L4BASEE(%rip), %rcx
	cmpw	$90, %ax
	ja	.L580
	subl	$65, %eax
	movb	$1, -782(%rbp)
	cltq
	movq	%r10, -272(%rbp,%rax,8)
	jmp	.L580
	.p2align 4,,10
	.p2align 3
.L564:
	movl	-596(%rbp), %edx
	jmp	.L565
	.p2align 4,,10
	.p2align 3
.L571:
	movl	-596(%rbp), %edx
	jmp	.L572
	.p2align 4,,10
	.p2align 3
.L569:
	movl	12(%r12), %r9d
	movl	$1, %r8d
	testl	%r9d, %r9d
	cmovle	%r9d, %r8d
	jmp	.L570
	.p2align 4,,10
	.p2align 3
.L700:
	movl	$3435973837, %eax
	movl	$3435973837, %ecx
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	imulq	%rbx, %rax
	movq	-736(%rbp), %rdi
	movl	$1, %r9d
	shrq	$35, %rax
	movl	%eax, %r11d
	imulq	%rcx, %r11
	movq	%r10, %rcx
	shrq	$35, %r11
	leal	(%r11,%r11,4), %edx
	movl	%r11d, -760(%rbp)
	addl	%edx, %edx
	subl	%edx, %eax
	xorl	%edx, %edx
	addl	$48, %eax
	movw	%ax, -690(%rbp)
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	cmpl	$99, %r15d
	movq	-712(%rbp), %r10
	movl	-760(%rbp), %r11d
	jle	.L567
	movq	-736(%rbp), %rdi
	addl	$48, %r11d
	movq	%r10, %rcx
	xorl	%edx, %edx
	movl	$1, %r9d
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movw	%r11w, -690(%rbp)
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	movq	-712(%rbp), %r10
	jmp	.L567
.L583:
	movl	12(%r12), %edx
	jmp	.L584
.L544:
	movq	-792(%rbp), %rax
	xorl	%r15d, %r15d
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	jg	.L547
	movl	$7, (%rax)
	xorl	%r15d, %r15d
	jmp	.L547
.L586:
	movq	24(%r12), %rax
	jmp	.L587
.L555:
	movq	%rbx, %rdi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L556
.L548:
	movq	%r13, %rdi
	call	*%rax
	movq	%rax, %r14
	jmp	.L549
.L620:
	movq	-728(%rbp), %rdi
	movl	8(%rdi), %r12d
	leal	-1(%r12), %esi
	subl	$2, %r12d
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	-728(%rbp), %r13
	movq	%rax, %rbx
	testl	%r12d, %r12d
	jg	.L624
	jmp	.L625
	.p2align 4,,10
	.p2align 3
.L626:
	subl	$1, %r12d
	je	.L625
.L624:
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	cmpq	$0, 144(%rax)
	jne	.L626
	cmpl	$2, 136(%rax)
	je	.L708
.L656:
	movq	%rax, %rbx
	jmp	.L626
.L708:
	movl	136(%rbx), %r10d
	testl	%r10d, %r10d
	je	.L656
	movq	%rbx, 144(%rax)
	jmp	.L626
.L625:
	movl	$40, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L627
	movq	-792(%rbp), %rbx
	movq	%rax, %rdi
	movq	%rbx, %rsi
	call	_ZN6icu_677UVectorC1ER10UErrorCode@PLT
	movl	(%rbx), %r9d
	testl	%r9d, %r9d
	jle	.L628
.L643:
	movq	(%r12), %rax
	movq	%r12, %rdi
	xorl	%r15d, %r15d
	call	*8(%rax)
	jmp	.L608
.L704:
	movl	$24, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L615
	movq	-728(%rbp), %r13
	leaq	16+_ZTVN6icu_6710BucketListE(%rip), %rax
	movq	%rax, (%r15)
	movl	8(%r13), %r12d
	movq	%r13, 8(%r15)
	movq	%r13, 16(%r15)
	testl	%r12d, %r12d
	jle	.L622
	xorl	%ebx, %ebx
.L612:
	movl	%ebx, %r12d
	movq	%r13, %rdi
	addl	$1, %ebx
	movl	%r12d, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movl	%r12d, 152(%rax)
	cmpl	%ebx, 8(%r13)
	jg	.L612
	jmp	.L622
.L613:
	movq	%r13, %rdi
	call	*%rax
	movq	%rax, %r13
	jmp	.L614
.L628:
	movq	-728(%rbp), %rax
	xorl	%ebx, %ebx
	movl	8(%rax), %edx
	movq	%rax, %r13
	testl	%edx, %edx
	jle	.L641
.L632:
	movl	%ebx, %esi
	movq	%r13, %rdi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	cmpq	$0, 144(%rax)
	je	.L709
	addl	$1, %ebx
	cmpl	8(%r13), %ebx
	jl	.L632
.L631:
	movq	-792(%rbp), %rax
	movl	(%rax), %r8d
	testl	%r8d, %r8d
	jg	.L643
.L641:
	movl	$24, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L634
	leaq	16+_ZTVN6icu_6710BucketListE(%rip), %rax
	movl	8(%r12), %edi
	movq	%r12, 16(%r15)
	xorl	%ebx, %ebx
	movq	%rax, (%r15)
	movq	-728(%rbp), %rax
	movq	%rax, 8(%r15)
	testl	%edi, %edi
	jle	.L622
.L635:
	movl	%ebx, %r13d
	movq	%r12, %rdi
	addl	$1, %ebx
	movl	%r13d, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movl	%r13d, 152(%rax)
	cmpl	%ebx, 8(%r12)
	jg	.L635
	movq	$0, -728(%rbp)
	jmp	.L608
.L709:
	movq	-792(%rbp), %rdx
	movq	%rax, %rsi
	movq	%r12, %rdi
	addl	$1, %ebx
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	cmpl	8(%r13), %ebx
	jl	.L632
	jmp	.L631
.L627:
	movq	-792(%rbp), %rax
	movl	(%rax), %esi
	testl	%esi, %esi
	jg	.L654
	movl	$7, (%rax)
	xorl	%r15d, %r15d
	jmp	.L608
.L648:
	movq	-816(%rbp), %rax
	movb	$0, -782(%rbp)
	movb	$0, -783(%rbp)
	movq	%rax, -744(%rbp)
	leaq	-608(%rbp), %rax
	movq	%rax, -736(%rbp)
	jmp	.L552
.L557:
	movq	-792(%rbp), %rax
	movq	%r10, %r15
	movl	$7, (%rax)
	jmp	.L608
.L634:
	movq	-792(%rbp), %rax
	movl	$7, (%rax)
	jmp	.L643
.L615:
	movq	-792(%rbp), %rax
	movl	$7, (%rax)
	jmp	.L608
.L606:
	movq	%r8, %rdi
	movq	%rax, %r15
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-792(%rbp), %rax
	movl	$7, (%rax)
	jmp	.L608
.L699:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2550:
	.size	_ZNK6icu_6715AlphabeticIndex16createBucketListER10UErrorCode, .-_ZNK6icu_6715AlphabeticIndex16createBucketListER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715AlphabeticIndex19buildImmutableIndexER10UErrorCode
	.type	_ZN6icu_6715AlphabeticIndex19buildImmutableIndexER10UErrorCode, @function
_ZN6icu_6715AlphabeticIndex19buildImmutableIndexER10UErrorCode:
.LFB2540:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L739
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	_ZNK6icu_6715AlphabeticIndex16createBucketListER10UErrorCode
	movq	64(%r13), %rdi
	movq	%rax, %r12
	movq	(%rdi), %rax
	call	*40(%rax)
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L722
	testq	%r12, %r12
	je	.L722
	movl	$24, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L717
	leaq	16+_ZTVN6icu_6715AlphabeticIndex14ImmutableIndexE(%rip), %rcx
	movq	%r12, 8(%rax)
	movq	%rcx, (%rax)
	movq	%r13, 16(%rax)
.L710:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L722:
	.cfi_restore_state
	movl	$7, (%rbx)
	testq	%r13, %r13
	je	.L716
.L715:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*8(%rax)
.L716:
	testq	%r12, %r12
	je	.L738
	movq	(%r12), %rax
	leaq	_ZN6icu_6710BucketListD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L719
	movq	8(%r12), %rdi
	leaq	16+_ZTVN6icu_6710BucketListE(%rip), %rax
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L720
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	8(%r12), %rdi
.L720:
	movq	16(%r12), %r8
	testq	%r8, %r8
	je	.L721
	cmpq	%rdi, %r8
	je	.L721
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
.L721:
	movq	%r12, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L738:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L739:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L719:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	%r12, %rdi
	call	*%rax
	xorl	%eax, %eax
	jmp	.L710
.L717:
	movl	$7, (%rbx)
	jmp	.L715
	.cfi_endproc
.LFE2540:
	.size	_ZN6icu_6715AlphabeticIndex19buildImmutableIndexER10UErrorCode, .-_ZN6icu_6715AlphabeticIndex19buildImmutableIndexER10UErrorCode
	.align 2
	.p2align 4
	.type	_ZN6icu_6715AlphabeticIndex11initBucketsER10UErrorCode.part.0, @function
_ZN6icu_6715AlphabeticIndex11initBucketsER10UErrorCode.part.0:
.LFB3592:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	call	_ZNK6icu_6715AlphabeticIndex16createBucketListER10UErrorCode
	movl	(%rbx), %ecx
	movq	%rax, 72(%r14)
	testl	%ecx, %ecx
	jg	.L740
	movq	8(%r14), %rdi
	testq	%rdi, %rdi
	je	.L740
	movl	8(%rdi), %edx
	testl	%edx, %edx
	je	.L740
	movq	56(%r14), %rdx
	movq	%rbx, %rcx
	leaq	_ZN6icu_67L15recordCompareFnEPKvS1_S1_(%rip), %rsi
	call	_ZN6icu_677UVector19sortWithUComparatorEPFiPKvS2_S2_ES2_R10UErrorCode@PLT
	movq	72(%r14), %rax
	xorl	%esi, %esi
	movq	8(%rax), %rdi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%rax, -64(%rbp)
	movq	72(%r14), %rax
	movq	8(%rax), %rdi
	cmpl	$1, 8(%rdi)
	jg	.L764
	movq	$0, -56(%rbp)
	xorl	%r13d, %r13d
	movl	$1, %r12d
.L744:
	movq	8(%r14), %rdi
	movl	8(%rdi), %eax
	testl	%eax, %eax
	jle	.L740
	movl	$0, -76(%rbp)
	jmp	.L752
	.p2align 4,,10
	.p2align 3
.L754:
	movq	-56(%rbp), %rax
	xorl	%r13d, %r13d
	movq	%rax, -64(%rbp)
.L745:
	movq	-64(%rbp), %rax
	movq	144(%rax), %r15
	testq	%r15, %r15
	cmove	%rax, %r15
	movq	160(%r15), %rdi
	testq	%rdi, %rdi
	je	.L765
.L750:
	movq	-88(%rbp), %rsi
	movq	%rbx, %rdx
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	movq	8(%r14), %rdi
	addl	$1, -76(%rbp)
	movl	-76(%rbp), %eax
	cmpl	%eax, 8(%rdi)
	jle	.L740
.L752:
	movl	-76(%rbp), %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%rax, -88(%rbp)
	testq	%r13, %r13
	je	.L745
	leaq	8(%rax), %r15
.L749:
	movq	64(%r14), %rdi
	movq	%rbx, %rcx
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	(%rdi), %rax
	call	*56(%rax)
	testl	%eax, %eax
	js	.L745
	movq	72(%r14), %rax
	movq	8(%rax), %rdi
	cmpl	%r12d, 8(%rdi)
	jle	.L754
	leal	1(%r12), %eax
	movl	%r12d, %esi
	movl	%eax, -72(%rbp)
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	-56(%rbp), %rcx
	movl	-72(%rbp), %r12d
	movq	%rax, -56(%rbp)
	leaq	72(%rax), %r13
	movq	%rcx, -64(%rbp)
	jmp	.L749
.L751:
	movq	$0, 160(%r15)
	movl	$7, (%rbx)
.L740:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L764:
	.cfi_restore_state
	movl	$1, %esi
	movl	$2, %r12d
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%rax, -56(%rbp)
	leaq	72(%rax), %r13
	jmp	.L744
	.p2align 4,,10
	.p2align 3
.L765:
	movl	$40, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L751
	movq	%rbx, %rsi
	movq	%rax, -72(%rbp)
	call	_ZN6icu_677UVectorC1ER10UErrorCode@PLT
	movq	-72(%rbp), %rdi
	movq	%rdi, 160(%r15)
	jmp	.L750
	.cfi_endproc
.LFE3592:
	.size	_ZN6icu_6715AlphabeticIndex11initBucketsER10UErrorCode.part.0, .-_ZN6icu_6715AlphabeticIndex11initBucketsER10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715AlphabeticIndex11initBucketsER10UErrorCode
	.type	_ZN6icu_6715AlphabeticIndex11initBucketsER10UErrorCode, @function
_ZN6icu_6715AlphabeticIndex11initBucketsER10UErrorCode:
.LFB2551:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L766
	cmpq	$0, 72(%rdi)
	je	.L768
.L766:
	ret
	.p2align 4,,10
	.p2align 3
.L768:
	jmp	_ZN6icu_6715AlphabeticIndex11initBucketsER10UErrorCode.part.0
	.cfi_endproc
.LFE2551:
	.size	_ZN6icu_6715AlphabeticIndex11initBucketsER10UErrorCode, .-_ZN6icu_6715AlphabeticIndex11initBucketsER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715AlphabeticIndex14getBucketCountER10UErrorCode
	.type	_ZN6icu_6715AlphabeticIndex14getBucketCountER10UErrorCode, @function
_ZN6icu_6715AlphabeticIndex14getBucketCountER10UErrorCode:
.LFB2545:
	.cfi_startproc
	endbr64
	movl	(%rsi), %edx
	testl	%edx, %edx
	jg	.L780
	movq	72(%rdi), %rax
	testq	%rax, %rax
	je	.L781
	movq	16(%rax), %rax
	movl	8(%rax), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L780:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L781:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rsi, -16(%rbp)
	movq	%rdi, -8(%rbp)
	call	_ZN6icu_6715AlphabeticIndex11initBucketsER10UErrorCode.part.0
	movq	-16(%rbp), %rsi
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L773
	movq	-8(%rbp), %rdi
	movq	72(%rdi), %rax
	movq	16(%rax), %rax
	movl	8(%rax), %eax
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L773:
	.cfi_restore_state
	leave
	.cfi_def_cfa 7, 8
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2545:
	.size	_ZN6icu_6715AlphabeticIndex14getBucketCountER10UErrorCode, .-_ZN6icu_6715AlphabeticIndex14getBucketCountER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715AlphabeticIndex14getBucketIndexERKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6715AlphabeticIndex14getBucketIndexERKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6715AlphabeticIndex14getBucketIndexERKNS_13UnicodeStringER10UErrorCode:
.LFB2581:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rdx), %ecx
	movq	%rsi, -56(%rbp)
	testl	%ecx, %ecx
	jg	.L786
	movq	72(%rdi), %r15
	movq	%rdi, %r12
	movq	%rdx, %rbx
	testq	%r15, %r15
	je	.L800
.L785:
	movq	8(%r15), %rdi
	movq	64(%r12), %r13
	movl	8(%rdi), %r14d
	cmpl	$1, %r14d
	jle	.L794
	xorl	%r8d, %r8d
	.p2align 4,,10
	.p2align 3
.L788:
	leal	(%r8,%r14), %r12d
	movl	%r8d, -60(%rbp)
	sarl	%r12d
	movl	%r12d, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	-56(%rbp), %rsi
	movq	%rbx, %rcx
	movq	%r13, %rdi
	leaq	72(%rax), %rdx
	movq	0(%r13), %rax
	call	*56(%rax)
	testl	%eax, %eax
	js	.L801
	leal	1(%r12), %eax
	movq	8(%r15), %rdi
	cmpl	%r14d, %eax
	jge	.L787
	movl	%r12d, %r8d
	jmp	.L788
	.p2align 4,,10
	.p2align 3
.L800:
	movq	%rdx, %rsi
	call	_ZN6icu_6715AlphabeticIndex11initBucketsER10UErrorCode.part.0
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L802
.L786:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L801:
	.cfi_restore_state
	movl	-60(%rbp), %r8d
	movq	8(%r15), %rdi
	leal	1(%r8), %eax
	cmpl	%eax, %r12d
	jle	.L795
	movl	%r12d, %r14d
	jmp	.L788
	.p2align 4,,10
	.p2align 3
.L794:
	xorl	%r12d, %r12d
.L787:
	movl	%r12d, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	144(%rax), %rdx
	testq	%rdx, %rdx
	cmove	%rax, %rdx
	movl	152(%rdx), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L795:
	.cfi_restore_state
	movl	%r8d, %r12d
	jmp	.L787
.L802:
	movq	72(%r12), %r15
	jmp	.L785
	.cfi_endproc
.LFE2581:
	.size	_ZN6icu_6715AlphabeticIndex14getBucketIndexERKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6715AlphabeticIndex14getBucketIndexERKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715AlphabeticIndex10nextBucketER10UErrorCode
	.type	_ZN6icu_6715AlphabeticIndex10nextBucketER10UErrorCode, @function
_ZN6icu_6715AlphabeticIndex10nextBucketER10UErrorCode:
.LFB2583:
	.cfi_startproc
	endbr64
	movl	(%rsi), %edx
	testl	%edx, %edx
	jg	.L815
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	72(%rdi), %rax
	testq	%rax, %rax
	je	.L816
.L806:
	movl	16(%r12), %ecx
	movq	16(%rax), %rdi
	leal	1(%rcx), %esi
	movl	%esi, 16(%r12)
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	jl	.L809
	movl	%eax, 16(%r12)
.L814:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L815:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L816:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	cmpq	$0, 24(%rdi)
	movq	%rsi, %rbx
	je	.L807
	movl	$25, (%rsi)
.L803:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L809:
	.cfi_restore_state
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	leaq	_ZN6icu_6715AlphabeticIndex19resetRecordIteratorEv(%rip), %rdx
	movq	%rax, 24(%r12)
	movq	(%r12), %rax
	movq	224(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L810
	movl	$-1, 20(%r12)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L807:
	.cfi_restore_state
	call	_ZN6icu_6715AlphabeticIndex11initBucketsER10UErrorCode.part.0
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L814
	movq	72(%r12), %rax
	jmp	.L806
	.p2align 4,,10
	.p2align 3
.L810:
	movq	%r12, %rdi
	call	*%rax
	movl	$1, %eax
	jmp	.L803
	.cfi_endproc
.LFE2583:
	.size	_ZN6icu_6715AlphabeticIndex10nextBucketER10UErrorCode, .-_ZN6icu_6715AlphabeticIndex10nextBucketER10UErrorCode
	.section	.rodata.str2.8,"aMS",@progbits,2
	.align 8
.LC0:
	.string	"["
	.string	""
	.string	"\022\b\022\020\022\030\022 \022(\0220\0228\022@\022H\022P\022X\022`\022h\022p\022x\022\200\022\210\022\220\022\230\022\240\022\250\022\260\022\270\022\300\022\310\022\320\022\330\022\340\022\350\022\360\022\370\022"
	.string	"\023\b\023\020\023\030\023 \023(\0230\0238\023@\023H\023P\023X\023]"
	.string	""
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715AlphabeticIndex17addIndexExemplarsERKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_6715AlphabeticIndex17addIndexExemplarsERKNS_6LocaleER10UErrorCode, @function
_ZN6icu_6715AlphabeticIndex17addIndexExemplarsERKNS_6LocaleER10UErrorCode:
.LFB2554:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$568, %rsp
	movq	%rdi, -600(%rbp)
	movq	40(%rsi), %rdi
	movq	%rdx, %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	ulocdata_open_67@PLT
	movl	0(%r13), %esi
	movq	%rax, %r12
	testl	%esi, %esi
	jle	.L838
.L818:
	testq	%r12, %r12
	je	.L817
.L837:
	movq	%r12, %rdi
	call	ulocdata_close_67@PLT
.L817:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L839
	addq	$568, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L838:
	.cfi_restore_state
	leaq	-464(%rbp), %r15
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movl	$2, %ecx
	movq	%r13, %r8
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	ulocdata_getExemplarSet_67@PLT
	movl	0(%r13), %ecx
	testl	%ecx, %ecx
	jle	.L840
	movl	$0, 0(%r13)
	xorl	%edx, %edx
	movq	%r13, %r8
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	ulocdata_getExemplarSet_67@PLT
	movl	0(%r13), %edx
	testl	%edx, %edx
	jle	.L841
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	jmp	.L818
	.p2align 4,,10
	.p2align 3
.L841:
	movl	$122, %edx
	movl	$97, %esi
	movq	%r15, %rdi
	call	_ZNK6icu_6710UnicodeSet12containsNoneEii@PLT
	testb	%al, %al
	je	.L823
	movq	%r15, %rdi
	call	_ZNK6icu_6710UnicodeSet7isEmptyEv@PLT
	testb	%al, %al
	jne	.L823
.L822:
	movl	$55203, %edx
	movl	$44032, %esi
	movq	%r15, %rdi
	call	_ZNK6icu_6710UnicodeSet12containsNoneEii@PLT
	testb	%al, %al
	je	.L842
.L824:
	movl	$4991, %edx
	movl	$4608, %esi
	movq	%r15, %rdi
	call	_ZNK6icu_6710UnicodeSet12containsNoneEii@PLT
	leaq	-256(%rbp), %r14
	testb	%al, %al
	je	.L843
.L825:
	leaq	-592(%rbp), %r13
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6718UnicodeSetIteratorC1ERKNS_10UnicodeSetE@PLT
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, -256(%rbp)
	movl	$2, %eax
	movw	%ax, -248(%rbp)
	jmp	.L827
	.p2align 4,,10
	.p2align 3
.L844:
	movq	%r13, %rdi
	call	_ZN6icu_6718UnicodeSetIterator9getStringEv@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString7toUpperERKNS_6LocaleE@PLT
	movq	-600(%rbp), %rax
	movq	%r14, %rsi
	movq	40(%rax), %rdi
	call	_ZN6icu_6710UnicodeSet3addERKNS_13UnicodeStringE@PLT
.L827:
	movq	%r13, %rdi
	call	_ZN6icu_6718UnicodeSetIterator4nextEv@PLT
	testb	%al, %al
	jne	.L844
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6718UnicodeSetIteratorD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	testq	%r12, %r12
	jne	.L837
	jmp	.L817
	.p2align 4,,10
	.p2align 3
.L823:
	movl	$122, %edx
	movl	$97, %esi
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSet3addEii@PLT
	jmp	.L822
	.p2align 4,,10
	.p2align 3
.L840:
	movq	-600(%rbp), %rax
	movq	%r15, %rsi
	movq	40(%rax), %rdi
	call	_ZN6icu_6710UnicodeSet6addAllERKS0_@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	jmp	.L818
	.p2align 4,,10
	.p2align 3
.L842:
	movl	$55203, %edx
	movq	%r15, %rdi
	movl	$44032, %esi
	call	_ZN6icu_6710UnicodeSet6removeEii@PLT
	movl	$44032, %esi
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	movl	$45208, %esi
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	movl	$45796, %esi
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	movl	$46972, %esi
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	movl	$47560, %esi
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	movl	$48148, %esi
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	movl	$49324, %esi
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	movl	$50500, %esi
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	movl	$51088, %esi
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	movl	$52264, %esi
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	movl	$52852, %esi
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	movl	$53440, %esi
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	movl	$54028, %esi
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	movl	$54616, %esi
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	jmp	.L824
	.p2align 4,,10
	.p2align 3
.L843:
	leaq	-528(%rbp), %r8
	leaq	.LC0(%rip), %rsi
	movq	%r8, %rdi
	movq	%r8, -608(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	-608(%rbp), %r8
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%r8, %rsi
	call	_ZN6icu_6710UnicodeSetC1ERKNS_13UnicodeStringER10UErrorCode@PLT
	movq	-608(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6710UnicodeSet9retainAllERKS0_@PLT
	movl	$4991, %edx
	movq	%r15, %rdi
	movl	$4608, %esi
	call	_ZN6icu_6710UnicodeSet6removeEii@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSet6addAllERKS0_@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	jmp	.L825
.L839:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2554:
	.size	_ZN6icu_6715AlphabeticIndex17addIndexExemplarsERKNS_6LocaleER10UErrorCode, .-_ZN6icu_6715AlphabeticIndex17addIndexExemplarsERKNS_6LocaleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715AlphabeticIndex9addLabelsERKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_6715AlphabeticIndex9addLabelsERKNS_6LocaleER10UErrorCode, @function
_ZN6icu_6715AlphabeticIndex9addLabelsERKNS_6LocaleER10UErrorCode:
.LFB2539:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN6icu_6715AlphabeticIndex17addIndexExemplarsERKNS_6LocaleER10UErrorCode
	movq	72(%r12), %r13
	testq	%r13, %r13
	je	.L846
	movq	0(%r13), %rax
	leaq	_ZN6icu_6710BucketListD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L847
	movq	8(%r13), %rdi
	leaq	16+_ZTVN6icu_6710BucketListE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%rdi, %rdi
	je	.L848
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	8(%r13), %rdi
.L848:
	movq	16(%r13), %r8
	cmpq	%rdi, %r8
	je	.L849
	testq	%r8, %r8
	je	.L849
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
.L849:
	movq	%r13, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L850:
	movq	$0, 72(%r12)
	movl	$-1, 16(%r12)
	movq	$0, 24(%r12)
.L846:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L847:
	.cfi_restore_state
	movq	%r13, %rdi
	call	*%rax
	jmp	.L850
	.cfi_endproc
.LFE2539:
	.size	_ZN6icu_6715AlphabeticIndex9addLabelsERKNS_6LocaleER10UErrorCode, .-_ZN6icu_6715AlphabeticIndex9addLabelsERKNS_6LocaleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715AlphabeticIndex25addChineseIndexCharactersER10UErrorCode
	.type	_ZN6icu_6715AlphabeticIndex25addChineseIndexCharactersER10UErrorCode, @function
_ZN6icu_6715AlphabeticIndex25addChineseIndexCharactersER10UErrorCode:
.LFB2555:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-240(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$280, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movq	64(%rbx), %rdi
	movq	%r13, %rcx
	movq	%r12, %rdx
	movl	$64976, %esi
	call	_ZNK6icu_6717RuleBasedCollator23internalAddContractionsEiRNS_10UnicodeSetER10UErrorCode@PLT
	movl	0(%r13), %eax
	testl	%eax, %eax
	jle	.L865
.L867:
	xorl	%r13d, %r13d
.L866:
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L880
	addq	$280, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L865:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK6icu_6710UnicodeSet7isEmptyEv@PLT
	testb	%al, %al
	jne	.L867
	movq	40(%rbx), %rdi
	movq	%r12, %rsi
	leaq	-304(%rbp), %r13
	call	_ZN6icu_6710UnicodeSet6addAllERKS0_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6718UnicodeSetIteratorC1ERKNS_10UnicodeSetE@PLT
	.p2align 4,,10
	.p2align 3
.L877:
	movq	%r13, %rdi
	call	_ZN6icu_6718UnicodeSetIterator4nextEv@PLT
	testb	%al, %al
	je	.L868
	movq	%r13, %rdi
	call	_ZN6icu_6718UnicodeSetIterator9getStringEv@PLT
	movzwl	8(%rax), %ecx
	testw	%cx, %cx
	js	.L869
	movswl	%cx, %edx
	sarl	$5, %edx
.L870:
	leal	-1(%rdx), %esi
	testl	%edx, %edx
	je	.L877
	andl	$2, %ecx
	je	.L872
	addq	$10, %rax
.L873:
	movslq	%esi, %rsi
	movzwl	(%rax,%rsi,2), %eax
	subl	$65, %eax
	cmpw	$25, %ax
	ja	.L877
	movq	40(%rbx), %rdi
	movl	$90, %edx
	movl	$65, %esi
	call	_ZN6icu_6710UnicodeSet3addEii@PLT
.L868:
	movq	%r13, %rdi
	movl	$1, %r13d
	call	_ZN6icu_6718UnicodeSetIteratorD1Ev@PLT
	jmp	.L866
	.p2align 4,,10
	.p2align 3
.L869:
	movl	12(%rax), %edx
	jmp	.L870
	.p2align 4,,10
	.p2align 3
.L872:
	movq	24(%rax), %rax
	jmp	.L873
.L880:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2555:
	.size	_ZN6icu_6715AlphabeticIndex25addChineseIndexCharactersER10UErrorCode, .-_ZN6icu_6715AlphabeticIndex25addChineseIndexCharactersER10UErrorCode
	.align 2
	.p2align 4
	.type	_ZN6icu_6715AlphabeticIndex20firstStringsInScriptER10UErrorCode.part.0, @function
_ZN6icu_6715AlphabeticIndex20firstStringsInScriptER10UErrorCode.part.0:
.LFB3595:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movl	$40, %edi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$296, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L882
	movq	%rbx, %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	xorl	%r14d, %r14d
	call	_ZN6icu_677UVectorC1ER10UErrorCode@PLT
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jle	.L905
.L883:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
.L881:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L906
	addq	$296, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L882:
	.cfi_restore_state
	movl	(%rbx), %edx
	xorl	%r14d, %r14d
	testl	%edx, %edx
	jg	.L881
	movl	$7, (%rbx)
	xorl	%r14d, %r14d
	jmp	.L881
	.p2align 4,,10
	.p2align 3
.L905:
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %rsi
	movq	%r12, %rdi
	leaq	-256(%rbp), %r15
	call	_ZN6icu_677UVector10setDeleterEPFvPvE@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movq	64(%r13), %rdi
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movl	$64977, %esi
	call	_ZNK6icu_6717RuleBasedCollator23internalAddContractionsEiRNS_10UnicodeSetER10UErrorCode@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L907
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	jmp	.L883
	.p2align 4,,10
	.p2align 3
.L907:
	movq	%r15, %rdi
	call	_ZNK6icu_6710UnicodeSet7isEmptyEv@PLT
	testb	%al, %al
	je	.L886
	movl	$16, (%rbx)
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	jmp	.L883
	.p2align 4,,10
	.p2align 3
.L886:
	leaq	-320(%rbp), %r14
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6718UnicodeSetIteratorC1ERKNS_10UnicodeSetE@PLT
	.p2align 4,,10
	.p2align 3
.L889:
	movq	%r14, %rdi
	call	_ZN6icu_6718UnicodeSetIterator4nextEv@PLT
	testb	%al, %al
	je	.L897
.L908:
	movq	%r14, %rdi
	call	_ZN6icu_6718UnicodeSetIterator9getStringEv@PLT
	movl	$1, %esi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movl	%eax, %edi
	call	u_charType_67@PLT
	movl	$1, %edx
	movl	%eax, %ecx
	sall	%cl, %edx
	andl	$63, %edx
	je	.L889
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L890
	movq	%rax, %rdi
	movq	%r13, %rsi
	movq	%rax, -328(%rbp)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	-328(%rbp), %r8
	movq	%r12, %rdi
	movq	%rbx, %rdx
	movq	%r8, %rsi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6718UnicodeSetIterator4nextEv@PLT
	testb	%al, %al
	jne	.L908
.L897:
	xorl	%ebx, %ebx
.L888:
	movq	%r14, %rdi
	movq	%r12, %r14
	call	_ZN6icu_6718UnicodeSetIteratorD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	testq	%rbx, %rbx
	je	.L881
	movq	%rbx, %r12
	jmp	.L883
.L906:
	call	__stack_chk_fail@PLT
.L890:
	movl	$7, (%rbx)
	movq	%r12, %rbx
	xorl	%r12d, %r12d
	jmp	.L888
	.cfi_endproc
.LFE3595:
	.size	_ZN6icu_6715AlphabeticIndex20firstStringsInScriptER10UErrorCode.part.0, .-_ZN6icu_6715AlphabeticIndex20firstStringsInScriptER10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715AlphabeticIndex20firstStringsInScriptER10UErrorCode
	.type	_ZN6icu_6715AlphabeticIndex20firstStringsInScriptER10UErrorCode, @function
_ZN6icu_6715AlphabeticIndex20firstStringsInScriptER10UErrorCode:
.LFB2571:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L910
	jmp	_ZN6icu_6715AlphabeticIndex20firstStringsInScriptER10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L910:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2571:
	.size	_ZN6icu_6715AlphabeticIndex20firstStringsInScriptER10UErrorCode, .-_ZN6icu_6715AlphabeticIndex20firstStringsInScriptER10UErrorCode
	.align 2
	.p2align 4
	.type	_ZN6icu_6715AlphabeticIndex4initEPKNS_6LocaleER10UErrorCode.part.0, @function
_ZN6icu_6715AlphabeticIndex4initEPKNS_6LocaleER10UErrorCode.part.0:
.LFB3596:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$8230, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	80(%rdi), %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r15, %rdi
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movw	%r8w, -58(%rbp)
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	movswl	88(%r12), %edx
	testw	%dx, %dx
	js	.L912
	sarl	$5, %edx
.L913:
	leaq	-58(%rbp), %rcx
	movl	$1, %r9d
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	leaq	144(%r12), %rdi
	movq	%r15, %rsi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	leaq	208(%r12), %rdi
	movq	%r15, %rsi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	56(%r12), %rdi
	testq	%rdi, %rdi
	je	.L936
.L914:
	movq	(%rdi), %rax
	call	*40(%rax)
	movq	%rax, 64(%r12)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L919
	movq	(%rax), %rax
	movl	$5, %esi
	movq	%r13, %rcx
	xorl	%edx, %edx
	call	*184(%rax)
	movl	0(%r13), %esi
	testl	%esi, %esi
	jg	.L920
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6715AlphabeticIndex20firstStringsInScriptER10UErrorCode.part.0
	movq	%rax, %rdi
	movl	0(%r13), %eax
	movq	%rdi, 48(%r12)
	testl	%eax, %eax
	jg	.L911
	movq	64(%r12), %rdx
	movq	%r13, %rcx
	leaq	_ZN6icu_67L18collatorComparatorEPKvS1_S1_(%rip), %rsi
	call	_ZN6icu_677UVector19sortWithUComparatorEPFiPKvS2_S2_ES2_R10UErrorCode@PLT
	movl	0(%r13), %ecx
	testl	%ecx, %ecx
	jg	.L911
	leaq	336(%r12), %rbx
	jmp	.L924
	.p2align 4,,10
	.p2align 3
.L922:
	movq	64(%r12), %r15
	xorl	%esi, %esi
	movq	(%r15), %rax
	movq	56(%rax), %r14
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%r13, %rcx
	movq	%rbx, %rdx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	*%r14
	testl	%eax, %eax
	jne	.L923
	movq	48(%r12), %rdi
	xorl	%esi, %esi
	call	_ZN6icu_677UVector15removeElementAtEi@PLT
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L911
.L924:
	movq	48(%r12), %rdi
	movl	8(%rdi), %edx
	testl	%edx, %edx
	jne	.L922
	movl	$1, 0(%r13)
	jmp	.L911
	.p2align 4,,10
	.p2align 3
.L920:
	movq	$0, 48(%r12)
.L911:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L937
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L912:
	.cfi_restore_state
	movl	92(%r12), %edx
	jmp	.L913
	.p2align 4,,10
	.p2align 3
.L919:
	movl	$7, 0(%r13)
	jmp	.L911
	.p2align 4,,10
	.p2align 3
.L936:
	movq	-72(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN6icu_678Collator14createInstanceERKNS_6LocaleER10UErrorCode@PLT
	movl	0(%r13), %edi
	movq	%rax, %r15
	testl	%edi, %edi
	jg	.L938
	testq	%rax, %rax
	je	.L919
	movq	%rax, %rdi
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6717RuleBasedCollatorE(%rip), %rdx
	leaq	_ZTIN6icu_678CollatorE(%rip), %rsi
	call	__dynamic_cast@PLT
	movq	%rax, 56(%r12)
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L914
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*8(%rax)
	movl	$16, 0(%r13)
	jmp	.L911
	.p2align 4,,10
	.p2align 3
.L923:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6715AlphabeticIndex25addChineseIndexCharactersER10UErrorCode
	movq	-72(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L911
	testb	%al, %al
	jne	.L911
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6715AlphabeticIndex17addIndexExemplarsERKNS_6LocaleER10UErrorCode
	jmp	.L911
	.p2align 4,,10
	.p2align 3
.L938:
	testq	%rax, %rax
	je	.L911
	movq	(%rax), %rax
	movq	%r15, %rdi
	call	*8(%rax)
	jmp	.L911
.L937:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3596:
	.size	_ZN6icu_6715AlphabeticIndex4initEPKNS_6LocaleER10UErrorCode.part.0, .-_ZN6icu_6715AlphabeticIndex4initEPKNS_6LocaleER10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715AlphabeticIndex4initEPKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_6715AlphabeticIndex4initEPKNS_6LocaleER10UErrorCode, @function
_ZN6icu_6715AlphabeticIndex4initEPKNS_6LocaleER10UErrorCode:
.LFB2568:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L944
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	testq	%rsi, %rsi
	je	.L947
.L941:
	movl	$200, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L942
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movq	%rbx, 40(%r13)
	movq	%r12, %rdx
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	movq	%r14, %rsi
	popq	%r12
	.cfi_restore 12
	movq	%r13, %rdi
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6715AlphabeticIndex4initEPKNS_6LocaleER10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L947:
	.cfi_restore_state
	cmpq	$0, 56(%rdi)
	jne	.L941
	movl	$1, (%rdx)
.L939:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L944:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	ret
.L942:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movq	$0, 40(%r13)
	movl	$7, (%r12)
	jmp	.L939
	.cfi_endproc
.LFE2568:
	.size	_ZN6icu_6715AlphabeticIndex4initEPKNS_6LocaleER10UErrorCode, .-_ZN6icu_6715AlphabeticIndex4initEPKNS_6LocaleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715AlphabeticIndexC2EPNS_17RuleBasedCollatorER10UErrorCode
	.type	_ZN6icu_6715AlphabeticIndexC2EPNS_17RuleBasedCollatorER10UErrorCode, @function
_ZN6icu_6715AlphabeticIndexC2EPNS_17RuleBasedCollatorER10UErrorCode:
.LFB2532:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6715AlphabeticIndexE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%rsi, 56(%rdi)
	movl	$2, %ecx
	movq	%rax, (%rdi)
	movl	$4294967295, %eax
	movl	$2, %r8d
	movl	$2, %r9d
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %r10d
	movl	$2, %r11d
	movq	%rax, 80(%rdi)
	movq	%rax, 144(%rdi)
	movq	%rax, 208(%rdi)
	movq	%rax, 272(%rdi)
	movq	%rax, 336(%rdi)
	movl	(%rdx), %eax
	movq	$0, 8(%rdi)
	movq	$0, 24(%rdi)
	movl	$99, 32(%rdi)
	movq	$0, 64(%rdi)
	movq	$0, 72(%rdi)
	movw	%cx, 88(%rdi)
	movw	%r8w, 152(%rdi)
	movw	%r9w, 216(%rdi)
	movw	%r10w, 280(%rdi)
	movw	%r11w, 344(%rdi)
	movups	%xmm0, 40(%rdi)
	testl	%eax, %eax
	jg	.L953
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	testq	%rsi, %rsi
	je	.L956
	movq	%rdi, %r12
	movl	$200, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L951
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movq	%rbx, 40(%r12)
	addq	$8, %rsp
	xorl	%esi, %esi
	popq	%rbx
	.cfi_restore 3
	movq	%r13, %rdx
	movq	%r12, %rdi
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6715AlphabeticIndex4initEPKNS_6LocaleER10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L953:
	ret
	.p2align 4,,10
	.p2align 3
.L956:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movl	$1, (%rdx)
.L948:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L951:
	.cfi_restore_state
	movq	$0, 40(%r12)
	movl	$7, 0(%r13)
	jmp	.L948
	.cfi_endproc
.LFE2532:
	.size	_ZN6icu_6715AlphabeticIndexC2EPNS_17RuleBasedCollatorER10UErrorCode, .-_ZN6icu_6715AlphabeticIndexC2EPNS_17RuleBasedCollatorER10UErrorCode
	.globl	_ZN6icu_6715AlphabeticIndexC1EPNS_17RuleBasedCollatorER10UErrorCode
	.set	_ZN6icu_6715AlphabeticIndexC1EPNS_17RuleBasedCollatorER10UErrorCode,_ZN6icu_6715AlphabeticIndexC2EPNS_17RuleBasedCollatorER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715AlphabeticIndexC2ERKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_6715AlphabeticIndexC2ERKNS_6LocaleER10UErrorCode, @function
_ZN6icu_6715AlphabeticIndexC2ERKNS_6LocaleER10UErrorCode:
.LFB2529:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6715AlphabeticIndexE(%rip), %rax
	pxor	%xmm0, %xmm0
	movl	$2, %ecx
	movl	$2, %r8d
	movq	%rax, (%rdi)
	movl	$4294967295, %eax
	movl	$2, %r9d
	movl	$2, %r10d
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %r11d
	movq	%rax, 80(%rdi)
	movq	%rax, 144(%rdi)
	movq	%rax, 208(%rdi)
	movq	%rax, 272(%rdi)
	movq	%rax, 336(%rdi)
	movl	(%rdx), %eax
	movq	$0, 8(%rdi)
	movq	$0, 24(%rdi)
	movl	$99, 32(%rdi)
	movq	$0, 72(%rdi)
	movw	%cx, 88(%rdi)
	movw	%r8w, 152(%rdi)
	movw	%r9w, 216(%rdi)
	movw	%r10w, 280(%rdi)
	movw	%r11w, 344(%rdi)
	movups	%xmm0, 40(%rdi)
	movups	%xmm0, 56(%rdi)
	testl	%eax, %eax
	jg	.L961
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movl	$200, %edi
	pushq	%rbx
	.cfi_offset 3, -48
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L959
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movq	%rbx, 40(%r12)
	movq	%r13, %rdx
	popq	%rbx
	.cfi_restore 3
	movq	%r14, %rsi
	movq	%r12, %rdi
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6715AlphabeticIndex4initEPKNS_6LocaleER10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L961:
	ret
.L959:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movq	$0, 40(%r12)
	movl	$7, 0(%r13)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2529:
	.size	_ZN6icu_6715AlphabeticIndexC2ERKNS_6LocaleER10UErrorCode, .-_ZN6icu_6715AlphabeticIndexC2ERKNS_6LocaleER10UErrorCode
	.globl	_ZN6icu_6715AlphabeticIndexC1ERKNS_6LocaleER10UErrorCode
	.set	_ZN6icu_6715AlphabeticIndexC1ERKNS_6LocaleER10UErrorCode,_ZN6icu_6715AlphabeticIndexC2ERKNS_6LocaleER10UErrorCode
	.weak	_ZTSN6icu_6715AlphabeticIndex6BucketE
	.section	.rodata._ZTSN6icu_6715AlphabeticIndex6BucketE,"aG",@progbits,_ZTSN6icu_6715AlphabeticIndex6BucketE,comdat
	.align 32
	.type	_ZTSN6icu_6715AlphabeticIndex6BucketE, @object
	.size	_ZTSN6icu_6715AlphabeticIndex6BucketE, 34
_ZTSN6icu_6715AlphabeticIndex6BucketE:
	.string	"N6icu_6715AlphabeticIndex6BucketE"
	.weak	_ZTIN6icu_6715AlphabeticIndex6BucketE
	.section	.data.rel.ro._ZTIN6icu_6715AlphabeticIndex6BucketE,"awG",@progbits,_ZTIN6icu_6715AlphabeticIndex6BucketE,comdat
	.align 8
	.type	_ZTIN6icu_6715AlphabeticIndex6BucketE, @object
	.size	_ZTIN6icu_6715AlphabeticIndex6BucketE, 24
_ZTIN6icu_6715AlphabeticIndex6BucketE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6715AlphabeticIndex6BucketE
	.quad	_ZTIN6icu_677UObjectE
	.weak	_ZTSN6icu_6715AlphabeticIndex14ImmutableIndexE
	.section	.rodata._ZTSN6icu_6715AlphabeticIndex14ImmutableIndexE,"aG",@progbits,_ZTSN6icu_6715AlphabeticIndex14ImmutableIndexE,comdat
	.align 32
	.type	_ZTSN6icu_6715AlphabeticIndex14ImmutableIndexE, @object
	.size	_ZTSN6icu_6715AlphabeticIndex14ImmutableIndexE, 43
_ZTSN6icu_6715AlphabeticIndex14ImmutableIndexE:
	.string	"N6icu_6715AlphabeticIndex14ImmutableIndexE"
	.weak	_ZTIN6icu_6715AlphabeticIndex14ImmutableIndexE
	.section	.data.rel.ro._ZTIN6icu_6715AlphabeticIndex14ImmutableIndexE,"awG",@progbits,_ZTIN6icu_6715AlphabeticIndex14ImmutableIndexE,comdat
	.align 8
	.type	_ZTIN6icu_6715AlphabeticIndex14ImmutableIndexE, @object
	.size	_ZTIN6icu_6715AlphabeticIndex14ImmutableIndexE, 24
_ZTIN6icu_6715AlphabeticIndex14ImmutableIndexE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6715AlphabeticIndex14ImmutableIndexE
	.quad	_ZTIN6icu_677UObjectE
	.weak	_ZTSN6icu_6715AlphabeticIndexE
	.section	.rodata._ZTSN6icu_6715AlphabeticIndexE,"aG",@progbits,_ZTSN6icu_6715AlphabeticIndexE,comdat
	.align 16
	.type	_ZTSN6icu_6715AlphabeticIndexE, @object
	.size	_ZTSN6icu_6715AlphabeticIndexE, 27
_ZTSN6icu_6715AlphabeticIndexE:
	.string	"N6icu_6715AlphabeticIndexE"
	.weak	_ZTIN6icu_6715AlphabeticIndexE
	.section	.data.rel.ro._ZTIN6icu_6715AlphabeticIndexE,"awG",@progbits,_ZTIN6icu_6715AlphabeticIndexE,comdat
	.align 8
	.type	_ZTIN6icu_6715AlphabeticIndexE, @object
	.size	_ZTIN6icu_6715AlphabeticIndexE, 24
_ZTIN6icu_6715AlphabeticIndexE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6715AlphabeticIndexE
	.quad	_ZTIN6icu_677UObjectE
	.weak	_ZTSN6icu_6710BucketListE
	.section	.rodata._ZTSN6icu_6710BucketListE,"aG",@progbits,_ZTSN6icu_6710BucketListE,comdat
	.align 16
	.type	_ZTSN6icu_6710BucketListE, @object
	.size	_ZTSN6icu_6710BucketListE, 22
_ZTSN6icu_6710BucketListE:
	.string	"N6icu_6710BucketListE"
	.weak	_ZTIN6icu_6710BucketListE
	.section	.data.rel.ro._ZTIN6icu_6710BucketListE,"awG",@progbits,_ZTIN6icu_6710BucketListE,comdat
	.align 8
	.type	_ZTIN6icu_6710BucketListE, @object
	.size	_ZTIN6icu_6710BucketListE, 24
_ZTIN6icu_6710BucketListE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6710BucketListE
	.quad	_ZTIN6icu_677UObjectE
	.weak	_ZTVN6icu_6710BucketListE
	.section	.data.rel.ro._ZTVN6icu_6710BucketListE,"awG",@progbits,_ZTVN6icu_6710BucketListE,comdat
	.align 8
	.type	_ZTVN6icu_6710BucketListE, @object
	.size	_ZTVN6icu_6710BucketListE, 40
_ZTVN6icu_6710BucketListE:
	.quad	0
	.quad	_ZTIN6icu_6710BucketListE
	.quad	_ZN6icu_6710BucketListD1Ev
	.quad	_ZN6icu_6710BucketListD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.weak	_ZTVN6icu_6715AlphabeticIndex14ImmutableIndexE
	.section	.data.rel.ro._ZTVN6icu_6715AlphabeticIndex14ImmutableIndexE,"awG",@progbits,_ZTVN6icu_6715AlphabeticIndex14ImmutableIndexE,comdat
	.align 8
	.type	_ZTVN6icu_6715AlphabeticIndex14ImmutableIndexE, @object
	.size	_ZTVN6icu_6715AlphabeticIndex14ImmutableIndexE, 40
_ZTVN6icu_6715AlphabeticIndex14ImmutableIndexE:
	.quad	0
	.quad	_ZTIN6icu_6715AlphabeticIndex14ImmutableIndexE
	.quad	_ZN6icu_6715AlphabeticIndex14ImmutableIndexD1Ev
	.quad	_ZN6icu_6715AlphabeticIndex14ImmutableIndexD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.weak	_ZTVN6icu_6715AlphabeticIndexE
	.section	.data.rel.ro._ZTVN6icu_6715AlphabeticIndexE,"awG",@progbits,_ZTVN6icu_6715AlphabeticIndexE,comdat
	.align 8
	.type	_ZTVN6icu_6715AlphabeticIndexE, @object
	.size	_ZTVN6icu_6715AlphabeticIndexE, 264
_ZTVN6icu_6715AlphabeticIndexE:
	.quad	0
	.quad	_ZTIN6icu_6715AlphabeticIndexE
	.quad	_ZN6icu_6715AlphabeticIndexD1Ev
	.quad	_ZN6icu_6715AlphabeticIndexD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZN6icu_6715AlphabeticIndex9addLabelsERKNS_10UnicodeSetER10UErrorCode
	.quad	_ZN6icu_6715AlphabeticIndex9addLabelsERKNS_6LocaleER10UErrorCode
	.quad	_ZNK6icu_6715AlphabeticIndex11getCollatorEv
	.quad	_ZNK6icu_6715AlphabeticIndex14getInflowLabelEv
	.quad	_ZN6icu_6715AlphabeticIndex14setInflowLabelERKNS_13UnicodeStringER10UErrorCode
	.quad	_ZNK6icu_6715AlphabeticIndex16getOverflowLabelEv
	.quad	_ZN6icu_6715AlphabeticIndex16setOverflowLabelERKNS_13UnicodeStringER10UErrorCode
	.quad	_ZNK6icu_6715AlphabeticIndex17getUnderflowLabelEv
	.quad	_ZN6icu_6715AlphabeticIndex17setUnderflowLabelERKNS_13UnicodeStringER10UErrorCode
	.quad	_ZNK6icu_6715AlphabeticIndex16getMaxLabelCountEv
	.quad	_ZN6icu_6715AlphabeticIndex16setMaxLabelCountEiR10UErrorCode
	.quad	_ZN6icu_6715AlphabeticIndex9addRecordERKNS_13UnicodeStringEPKvR10UErrorCode
	.quad	_ZN6icu_6715AlphabeticIndex12clearRecordsER10UErrorCode
	.quad	_ZN6icu_6715AlphabeticIndex14getBucketCountER10UErrorCode
	.quad	_ZN6icu_6715AlphabeticIndex14getRecordCountER10UErrorCode
	.quad	_ZN6icu_6715AlphabeticIndex14getBucketIndexERKNS_13UnicodeStringER10UErrorCode
	.quad	_ZNK6icu_6715AlphabeticIndex14getBucketIndexEv
	.quad	_ZN6icu_6715AlphabeticIndex10nextBucketER10UErrorCode
	.quad	_ZNK6icu_6715AlphabeticIndex14getBucketLabelEv
	.quad	_ZNK6icu_6715AlphabeticIndex18getBucketLabelTypeEv
	.quad	_ZNK6icu_6715AlphabeticIndex20getBucketRecordCountEv
	.quad	_ZN6icu_6715AlphabeticIndex19resetBucketIteratorER10UErrorCode
	.quad	_ZN6icu_6715AlphabeticIndex10nextRecordER10UErrorCode
	.quad	_ZNK6icu_6715AlphabeticIndex13getRecordNameEv
	.quad	_ZNK6icu_6715AlphabeticIndex13getRecordDataEv
	.quad	_ZN6icu_6715AlphabeticIndex19resetRecordIteratorEv
	.quad	_ZNK6icu_6715AlphabeticIndexeqERKS0_
	.quad	_ZNK6icu_6715AlphabeticIndexneERKS0_
	.weak	_ZTVN6icu_6715AlphabeticIndex6BucketE
	.section	.data.rel.ro._ZTVN6icu_6715AlphabeticIndex6BucketE,"awG",@progbits,_ZTVN6icu_6715AlphabeticIndex6BucketE,comdat
	.align 8
	.type	_ZTVN6icu_6715AlphabeticIndex6BucketE, @object
	.size	_ZTVN6icu_6715AlphabeticIndex6BucketE, 40
_ZTVN6icu_6715AlphabeticIndex6BucketE:
	.quad	0
	.quad	_ZTIN6icu_6715AlphabeticIndex6BucketE
	.quad	_ZN6icu_6715AlphabeticIndex6BucketD1Ev
	.quad	_ZN6icu_6715AlphabeticIndex6BucketD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.section	.rodata
	.align 2
	.type	_ZN6icu_6712_GLOBAL__N_1L4BASEE, @object
	.size	_ZN6icu_6712_GLOBAL__N_1L4BASEE, 2
_ZN6icu_6712_GLOBAL__N_1L4BASEE:
	.value	-560
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
