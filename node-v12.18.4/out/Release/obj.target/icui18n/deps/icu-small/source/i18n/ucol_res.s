	.file	"ucol_res.cpp"
	.text
	.p2align 4
	.type	ucol_res_cleanup, @function
ucol_res_cleanup:
.LFB3308:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	_ZN6icu_6712_GLOBAL__N_1L10rootBundleE(%rip), %rdi
	movq	$0, _ZN6icu_6712_GLOBAL__N_1L9rootRulesE(%rip)
	movl	$0, _ZN6icu_6712_GLOBAL__N_1L15rootRulesLengthE(%rip)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ures_close_67@PLT
	movl	$1, %eax
	movq	$0, _ZN6icu_6712_GLOBAL__N_1L10rootBundleE(%rip)
	movl	$0, _ZN6icu_6712_GLOBAL__N_1L16gInitOnceUcolResE(%rip)
	mfence
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3308:
	.size	ucol_res_cleanup, .-ucol_res_cleanup
	.section	.text._ZNK6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEE5cloneEv,"axG",@progbits,_ZNK6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEE5cloneEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEE5cloneEv
	.type	_ZNK6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEE5cloneEv, @function
_ZNK6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEE5cloneEv:
.LFB4510:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	$240, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L4
	movl	8(%rbx), %eax
	movb	$0, 12(%r12)
	leaq	16(%rbx), %rsi
	leaq	16(%r12), %rdi
	movl	%eax, 8(%r12)
	leaq	16+_ZTVN6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEEE(%rip), %rax
	movq	%rax, (%r12)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
.L4:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4510:
	.size	_ZNK6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEE5cloneEv, .-_ZNK6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEE5cloneEv
	.section	.text._ZNK6icu_678CacheKeyINS_19CollationCacheEntryEE16writeDescriptionEPci,"axG",@progbits,_ZNK6icu_678CacheKeyINS_19CollationCacheEntryEE16writeDescriptionEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_678CacheKeyINS_19CollationCacheEntryEE16writeDescriptionEPci
	.type	_ZNK6icu_678CacheKeyINS_19CollationCacheEntryEE16writeDescriptionEPci, @function
_ZNK6icu_678CacheKeyINS_19CollationCacheEntryEE16writeDescriptionEPci:
.LFB4515:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	xorl	%eax, %eax
	movq	%r8, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movslq	%edx, %rbx
	movq	%rbx, %rdx
	subq	$8, %rsp
	movq	8+_ZTIN6icu_6719CollationCacheEntryE(%rip), %rsi
	cmpb	$42, (%rsi)
	sete	%al
	addq	%rax, %rsi
	call	strncpy@PLT
	movb	$0, -1(%rax,%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4515:
	.size	_ZNK6icu_678CacheKeyINS_19CollationCacheEntryEE16writeDescriptionEPci, .-_ZNK6icu_678CacheKeyINS_19CollationCacheEntryEE16writeDescriptionEPci
	.section	.text._ZNK6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEE16writeDescriptionEPci,"axG",@progbits,_ZNK6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEE16writeDescriptionEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEE16writeDescriptionEPci
	.type	_ZNK6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEE16writeDescriptionEPci, @function
_ZNK6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEE16writeDescriptionEPci:
.LFB4512:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movslq	%edx, %rbx
	movq	%rbx, %rdx
	subq	$8, %rsp
	movq	56(%rdi), %rsi
	movq	%r8, %rdi
	call	strncpy@PLT
	movb	$0, -1(%rax,%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4512:
	.size	_ZNK6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEE16writeDescriptionEPci, .-_ZNK6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEE16writeDescriptionEPci
	.section	.text._ZNK6icu_678CacheKeyINS_19CollationCacheEntryEEeqERKNS_12CacheKeyBaseE,"axG",@progbits,_ZNK6icu_678CacheKeyINS_19CollationCacheEntryEEeqERKNS_12CacheKeyBaseE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_678CacheKeyINS_19CollationCacheEntryEEeqERKNS_12CacheKeyBaseE
	.type	_ZNK6icu_678CacheKeyINS_19CollationCacheEntryEEeqERKNS_12CacheKeyBaseE, @function
_ZNK6icu_678CacheKeyINS_19CollationCacheEntryEEeqERKNS_12CacheKeyBaseE:
.LFB4514:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	-8(%rax), %rax
	movq	8(%rax), %rdi
	movq	(%rsi), %rax
	movq	-8(%rax), %rax
	movq	8(%rax), %rsi
	movl	$1, %eax
	cmpq	%rsi, %rdi
	je	.L20
	xorl	%eax, %eax
	cmpb	$42, (%rdi)
	je	.L20
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	strcmp@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE4514:
	.size	_ZNK6icu_678CacheKeyINS_19CollationCacheEntryEEeqERKNS_12CacheKeyBaseE, .-_ZNK6icu_678CacheKeyINS_19CollationCacheEntryEEeqERKNS_12CacheKeyBaseE
	.section	.text._ZNK6icu_678CacheKeyINS_19CollationCacheEntryEE8hashCodeEv,"axG",@progbits,_ZNK6icu_678CacheKeyINS_19CollationCacheEntryEE8hashCodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_678CacheKeyINS_19CollationCacheEntryEE8hashCodeEv
	.type	_ZNK6icu_678CacheKeyINS_19CollationCacheEntryEE8hashCodeEv, @function
_ZNK6icu_678CacheKeyINS_19CollationCacheEntryEE8hashCodeEv:
.LFB4513:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	8+_ZTIN6icu_6719CollationCacheEntryE(%rip), %r12
	cmpb	$42, (%r12)
	sete	%al
	addq	%rax, %r12
	movq	%r12, %rdi
	call	strlen@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	ustr_hashCharsN_67@PLT
	.cfi_endproc
.LFE4513:
	.size	_ZNK6icu_678CacheKeyINS_19CollationCacheEntryEE8hashCodeEv, .-_ZNK6icu_678CacheKeyINS_19CollationCacheEntryEE8hashCodeEv
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"default"
.LC1:
	.string	"private-"
	.text
	.align 2
	.p2align 4
	.type	_ZN12_GLOBAL__N_112KeywordsSink3putEPKcRN6icu_6713ResourceValueEaR10UErrorCode, @function
_ZN12_GLOBAL__N_112KeywordsSink3putEPKcRN6icu_6713ResourceValueEaR10UErrorCode:
.LFB3339:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$264, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -272(%rbp)
	movl	(%r8), %edi
	movq	%rsi, -264(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edi, %edi
	jg	.L26
	movq	(%rdx), %rax
	movq	%rdx, %r15
	leaq	-240(%rbp), %r13
	movq	%r8, %r12
	movq	%r8, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	xorl	%ebx, %ebx
	call	*88(%rax)
	leaq	-192(%rbp), %rax
	leaq	-264(%rbp), %r14
	movq	%rax, -288(%rbp)
	leaq	-179(%rbp), %rax
	movq	%rax, -296(%rbp)
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L47:
	movq	-272(%rbp), %rax
	cmpb	$0, 16(%rax)
	jne	.L31
	movq	-264(%rbp), %rsi
	movl	$8, %ecx
	leaq	.LC0(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L46
	.p2align 4,,10
	.p2align 3
.L31:
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L26
.L48:
	addl	$1, %ebx
.L37:
	movq	%r15, %rcx
	movq	%r14, %rdx
	movl	%ebx, %esi
	movq	%r13, %rdi
	call	_ZNK6icu_6713ResourceTable14getKeyAndValueEiRPKcRNS_13ResourceValueE@PLT
	testb	%al, %al
	je	.L26
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*24(%rax)
	testl	%eax, %eax
	je	.L47
	cmpl	$2, %eax
	jne	.L31
	movq	-264(%rbp), %r9
	movl	$8, %ecx
	leaq	.LC1(%rip), %rdi
	movq	%r9, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L31
	movq	%r9, %rdi
	movq	%r9, -280(%rbp)
	call	strlen@PLT
	movq	-280(%rbp), %r9
	movq	%rax, %rdx
	movq	-272(%rbp), %rax
	movq	%r9, %rsi
	movq	8(%rax), %rdi
	call	ulist_containsString_67@PLT
	testb	%al, %al
	jne	.L31
	movq	-272(%rbp), %rax
	movq	-264(%rbp), %rsi
	movq	%r12, %rcx
	xorl	%edx, %edx
	movq	8(%rax), %rdi
	call	ulist_addItemEndList_67@PLT
	movl	(%r12), %eax
	testl	%eax, %eax
	jle	.L48
	.p2align 4,,10
	.p2align 3
.L26:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L49
	addq	$264, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	.cfi_restore_state
	movq	-296(%rbp), %rax
	xorl	%ecx, %ecx
	movq	%r12, %rdx
	movq	%r15, %rdi
	movw	%cx, -180(%rbp)
	leaq	-252(%rbp), %rsi
	movq	%rax, -192(%rbp)
	movq	(%r15), %rax
	movl	$0, -136(%rbp)
	movl	$40, -184(%rbp)
	movl	$0, -252(%rbp)
	call	*32(%rax)
	leaq	-128(%rbp), %r9
	movl	-252(%rbp), %ecx
	leaq	-248(%rbp), %rdx
	movl	$1, %esi
	movq	%r9, %rdi
	movq	%rax, -248(%rbp)
	movq	%r9, -280(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	-280(%rbp), %r9
	movq	-288(%rbp), %rdi
	movq	%r12, %rdx
	movq	%r9, %rsi
	call	_ZN6icu_6710CharString20appendInvariantCharsERKNS_13UnicodeStringER10UErrorCode@PLT
	movq	-280(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	(%r12), %esi
	testl	%esi, %esi
	jle	.L50
.L33:
	cmpb	$0, -180(%rbp)
	je	.L31
	movq	-192(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L50:
	movl	-136(%rbp), %edx
	testl	%edx, %edx
	je	.L33
	movq	-192(%rbp), %rdi
	call	uprv_strdup_67@PLT
	testq	%rax, %rax
	je	.L51
	movq	%rax, -280(%rbp)
	movq	-272(%rbp), %rax
	movq	-192(%rbp), %rsi
	movq	8(%rax), %rdi
	call	ulist_removeString_67@PLT
	movq	-272(%rbp), %rax
	movq	%r12, %rcx
	movq	-280(%rbp), %r9
	movl	$1, %edx
	movq	8(%rax), %rdi
	movq	%r9, %rsi
	call	ulist_addItemBeginList_67@PLT
	movq	-272(%rbp), %rax
	movb	$1, 16(%rax)
	jmp	.L33
.L51:
	cmpb	$0, -180(%rbp)
	movl	$7, (%r12)
	je	.L26
	movq	-192(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L26
.L49:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3339:
	.size	_ZN12_GLOBAL__N_112KeywordsSink3putEPKcRN6icu_6713ResourceValueEaR10UErrorCode, .-_ZN12_GLOBAL__N_112KeywordsSink3putEPKcRN6icu_6713ResourceValueEaR10UErrorCode
	.align 2
	.p2align 4
	.type	_ZN12_GLOBAL__N_112KeywordsSinkD2Ev, @function
_ZN12_GLOBAL__N_112KeywordsSinkD2Ev:
.LFB3341:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12_GLOBAL__N_112KeywordsSinkE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	call	ulist_deleteList_67@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6712ResourceSinkD2Ev@PLT
	.cfi_endproc
.LFE3341:
	.size	_ZN12_GLOBAL__N_112KeywordsSinkD2Ev, .-_ZN12_GLOBAL__N_112KeywordsSinkD2Ev
	.set	_ZN12_GLOBAL__N_112KeywordsSinkD1Ev,_ZN12_GLOBAL__N_112KeywordsSinkD2Ev
	.align 2
	.p2align 4
	.type	_ZN12_GLOBAL__N_112KeywordsSinkD0Ev, @function
_ZN12_GLOBAL__N_112KeywordsSinkD0Ev:
.LFB3343:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12_GLOBAL__N_112KeywordsSinkE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	call	ulist_deleteList_67@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6712ResourceSinkD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3343:
	.size	_ZN12_GLOBAL__N_112KeywordsSinkD0Ev, .-_ZN12_GLOBAL__N_112KeywordsSinkD0Ev
	.section	.text._ZNK6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEE8hashCodeEv,"axG",@progbits,_ZNK6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEE8hashCodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEE8hashCodeEv
	.type	_ZNK6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEE8hashCodeEv, @function
_ZNK6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEE8hashCodeEv:
.LFB4509:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	8+_ZTIN6icu_6719CollationCacheEntryE(%rip), %r13
	cmpb	$42, 0(%r13)
	sete	%al
	addq	%rax, %r13
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	ustr_hashCharsN_67@PLT
	leaq	16(%r12), %rdi
	movl	%eax, %ebx
	call	_ZNK6icu_676Locale8hashCodeEv@PLT
	addq	$8, %rsp
	movl	%eax, %r8d
	leal	(%rbx,%rbx,8), %eax
	leal	(%rbx,%rax,4), %eax
	popq	%rbx
	popq	%r12
	addl	%r8d, %eax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4509:
	.size	_ZNK6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEE8hashCodeEv, .-_ZNK6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEE8hashCodeEv
	.section	.text._ZNK6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEEeqERKNS_12CacheKeyBaseE,"axG",@progbits,_ZNK6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEEeqERKNS_12CacheKeyBaseE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEEeqERKNS_12CacheKeyBaseE
	.type	_ZNK6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEEeqERKNS_12CacheKeyBaseE, @function
_ZNK6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEEeqERKNS_12CacheKeyBaseE:
.LFB4511:
	.cfi_startproc
	endbr64
	cmpq	%rsi, %rdi
	je	.L62
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	movq	-8(%rax), %rax
	movq	8(%rax), %rdi
	movq	(%rsi), %rax
	movq	-8(%rax), %rax
	movq	8(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L61
	cmpb	$42, (%rdi)
	je	.L64
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L64
.L61:
	leaq	16(%r12), %rsi
	leaq	16(%rbx), %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_676LocaleeqERKS0_@PLT
	.p2align 4,,10
	.p2align 3
.L64:
	.cfi_restore_state
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE4511:
	.size	_ZNK6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEEeqERKNS_12CacheKeyBaseE, .-_ZNK6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEEeqERKNS_12CacheKeyBaseE
	.text
	.align 2
	.p2align 4
	.type	_ZNK6icu_6715CollationLoader22makeCacheEntryFromRootERKNS_6LocaleER10UErrorCode.part.0, @function
_ZNK6icu_6715CollationLoader22makeCacheEntryFromRootERKNS_6LocaleER10UErrorCode.part.0:
.LFB4538:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	8(%rdi), %rdi
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
	movl	0(%r13), %eax
	movq	8(%r12), %r14
	testl	%eax, %eax
	jle	.L77
.L69:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L77:
	.cfi_restore_state
	addq	$16, %r12
	leaq	24(%r14), %rsi
	movq	%r12, %rdi
	call	_ZNK6icu_676LocaleeqERKS0_@PLT
	testb	%al, %al
	jne	.L69
	movl	$256, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L71
	movq	248(%r14), %r13
	leaq	24(%rbx), %rdi
	movq	%r12, %rsi
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	leaq	16+_ZTVN6icu_6719CollationCacheEntryE(%rip), %rax
	movq	%rax, (%rbx)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movq	%r13, 248(%rbx)
	testq	%r13, %r13
	je	.L72
	movq	%r13, %rdi
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
.L72:
	movq	%rbx, %rdi
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
	movq	%r14, %rdi
	movq	%rbx, %r14
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L71:
	.cfi_restore_state
	movl	$7, 0(%r13)
	movq	%r14, %rdi
	xorl	%r14d, %r14d
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	jmp	.L69
	.cfi_endproc
.LFE4538:
	.size	_ZNK6icu_6715CollationLoader22makeCacheEntryFromRootERKNS_6LocaleER10UErrorCode.part.0, .-_ZNK6icu_6715CollationLoader22makeCacheEntryFromRootERKNS_6LocaleER10UErrorCode.part.0
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev:
.LFB3697:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3697:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei:
.LFB3700:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	cmpl	$40, %esi
	jg	.L91
	ret
	.p2align 4,,10
	.p2align 3
.L91:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L79
	cmpb	$0, 12(%rbx)
	jne	.L92
.L83:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L79:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L92:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L83
	.cfi_endproc
.LFE3700:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EED2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev:
.LFB3703:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L95
	ret
	.p2align 4,,10
	.p2align 3
.L95:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3703:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5EOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_:
.LFB3706:
	.cfi_startproc
	endbr64
	movzbl	12(%rsi), %eax
	movq	(%rsi), %r8
	movslq	8(%rsi), %rdx
	movb	%al, 12(%rdi)
	leaq	13(%rsi), %rax
	movq	%r8, (%rdi)
	movl	%edx, 8(%rdi)
	cmpq	%rax, %r8
	je	.L98
	movq	%rax, (%rsi)
	movl	$40, 8(%rsi)
	movb	$0, 12(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L98:
	leaq	13(%rdi), %r9
	movq	%r8, %rsi
	movq	%r9, (%rdi)
	movq	%r9, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE3706:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_:
.LFB3708:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 12(%rdi)
	movq	%rsi, %rbx
	jne	.L104
.L100:
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movzbl	12(%rbx), %eax
	movb	%al, 12(%r12)
	movq	(%rbx), %rsi
	leaq	13(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L105
	movq	%rsi, (%r12)
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L104:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L105:
	leaq	13(%r12), %rdi
	movslq	8(%rbx), %rdx
	movq	%rdi, (%r12)
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3708:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv:
.LFB3709:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE3709:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv:
.LFB3710:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3710:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv:
.LFB3711:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	addq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3711:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3712:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3712:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3713:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3713:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci:
.LFB3714:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L121
	testl	%edx, %edx
	jle	.L121
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 12(%rdi)
	jne	.L124
.L113:
	movq	%rsi, (%rbx)
	movl	%edx, 8(%rbx)
	movb	$0, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L121:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L124:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	%edx, -28(%rbp)
	movq	%rsi, -24(%rbp)
	call	uprv_free_67@PLT
	movl	-28(%rbp), %edx
	movq	-24(%rbp), %rsi
	jmp	.L113
	.cfi_endproc
.LFE3714:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii:
.LFB3715:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L128
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movl	%esi, %r13d
	movl	%edx, %r12d
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L128
	testl	%r12d, %r12d
	jg	.L135
	cmpb	$0, 12(%rbx)
	jne	.L136
.L130:
	movq	%r14, (%rbx)
	movq	%r14, %rax
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L135:
	.cfi_restore_state
	cmpl	%r12d, 8(%rbx)
	cmovle	8(%rbx), %r12d
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	je	.L130
.L136:
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L128:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3715:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, .-_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi:
.LFB3716:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 12(%rdi)
	movq	%rdi, %rbx
	je	.L138
	movq	(%rdi), %r8
.L139:
	leaq	13(%rbx), %rax
	movl	%r12d, 0(%r13)
	movq	%rax, (%rbx)
	movq	%r8, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L138:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L142
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	cmovg	%eax, %r12d
	movslq	%r12d, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L142
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L142:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3716:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, .-_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv:
.LFB3717:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L149
	ret
	.p2align 4,,10
	.p2align 3
.L149:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3717:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv:
.LFB3718:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3718:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_:
.LFB3719:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3719:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_:
.LFB3720:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3720:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5ERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_:
.LFB3722:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3722:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_:
.LFB3724:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3724:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.section	.rodata.str1.1
.LC2:
	.string	"root"
.LC3:
	.string	"icudt67l-coll"
.LC4:
	.string	"UCARules"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715CollationLoader13loadRootRulesER10UErrorCode
	.type	_ZN6icu_6715CollationLoader13loadRootRulesER10UErrorCode, @function
_ZN6icu_6715CollationLoader13loadRootRulesER10UErrorCode:
.LFB3309:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	testl	%ecx, %ecx
	jle	.L164
	ret
	.p2align 4,,10
	.p2align 3
.L164:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rdx
	leaq	.LC2(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	.LC3(%rip), %rdi
	subq	$8, %rsp
	call	ures_open_67@PLT
	movl	(%rbx), %edx
	movq	%rax, _ZN6icu_6712_GLOBAL__N_1L10rootBundleE(%rip)
	testl	%edx, %edx
	jle	.L165
.L155:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L165:
	.cfi_restore_state
	movq	%rbx, %rcx
	leaq	_ZN6icu_6712_GLOBAL__N_1L15rootRulesLengthE(%rip), %rdx
	leaq	.LC4(%rip), %rsi
	movq	%rax, %rdi
	call	ures_getStringByKey_67@PLT
	movq	%rax, _ZN6icu_6712_GLOBAL__N_1L9rootRulesE(%rip)
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L166
	addq	$8, %rsp
	leaq	ucol_res_cleanup(%rip), %rsi
	movl	$28, %edi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	ucln_i18n_registerCleanup_67@PLT
	.p2align 4,,10
	.p2align 3
.L166:
	.cfi_restore_state
	movq	_ZN6icu_6712_GLOBAL__N_1L10rootBundleE(%rip), %rdi
	call	ures_close_67@PLT
	movq	$0, _ZN6icu_6712_GLOBAL__N_1L10rootBundleE(%rip)
	jmp	.L155
	.cfi_endproc
.LFE3309:
	.size	_ZN6icu_6715CollationLoader13loadRootRulesER10UErrorCode, .-_ZN6icu_6715CollationLoader13loadRootRulesER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715CollationLoader15appendRootRulesERNS_13UnicodeStringE
	.type	_ZN6icu_6715CollationLoader15appendRootRulesERNS_13UnicodeStringE, @function
_ZN6icu_6715CollationLoader15appendRootRulesERNS_13UnicodeStringE:
.LFB3310:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$0, -44(%rbp)
	movl	_ZN6icu_6712_GLOBAL__N_1L16gInitOnceUcolResE(%rip), %eax
	cmpl	$2, %eax
	jne	.L182
.L168:
	movl	4+_ZN6icu_6712_GLOBAL__N_1L16gInitOnceUcolResE(%rip), %edx
	testl	%edx, %edx
	jle	.L171
.L167:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L183
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L182:
	.cfi_restore_state
	leaq	_ZN6icu_6712_GLOBAL__N_1L16gInitOnceUcolResE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L168
	movl	-44(%rbp), %edx
	testl	%edx, %edx
	jle	.L184
.L169:
	leaq	_ZN6icu_6712_GLOBAL__N_1L16gInitOnceUcolResE(%rip), %rdi
	movl	%edx, 4+_ZN6icu_6712_GLOBAL__N_1L16gInitOnceUcolResE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L171:
	movl	-44(%rbp), %eax
	testl	%eax, %eax
	jg	.L167
	movq	_ZN6icu_6712_GLOBAL__N_1L9rootRulesE(%rip), %rbx
	movl	_ZN6icu_6712_GLOBAL__N_1L15rootRulesLengthE(%rip), %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L184:
	leaq	-44(%rbp), %r13
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdx
	leaq	.LC3(%rip), %rdi
	call	ures_open_67@PLT
	movl	-44(%rbp), %edx
	movq	%rax, _ZN6icu_6712_GLOBAL__N_1L10rootBundleE(%rip)
	testl	%edx, %edx
	jg	.L169
	movq	%r13, %rcx
	leaq	_ZN6icu_6712_GLOBAL__N_1L15rootRulesLengthE(%rip), %rdx
	leaq	.LC4(%rip), %rsi
	movq	%rax, %rdi
	call	ures_getStringByKey_67@PLT
	movl	-44(%rbp), %ecx
	movq	%rax, _ZN6icu_6712_GLOBAL__N_1L9rootRulesE(%rip)
	testl	%ecx, %ecx
	jg	.L185
	leaq	ucol_res_cleanup(%rip), %rsi
	movl	$28, %edi
	call	ucln_i18n_registerCleanup_67@PLT
	movl	-44(%rbp), %edx
	jmp	.L169
.L185:
	movq	_ZN6icu_6712_GLOBAL__N_1L10rootBundleE(%rip), %rdi
	call	ures_close_67@PLT
	movl	-44(%rbp), %edx
	movq	$0, _ZN6icu_6712_GLOBAL__N_1L10rootBundleE(%rip)
	jmp	.L169
.L183:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3310:
	.size	_ZN6icu_6715CollationLoader15appendRootRulesERNS_13UnicodeStringE, .-_ZN6icu_6715CollationLoader15appendRootRulesERNS_13UnicodeStringE
	.section	.rodata.str1.1
.LC5:
	.string	"collations"
.LC6:
	.string	"Sequence"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715CollationLoader9loadRulesEPKcS2_RNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6715CollationLoader9loadRulesEPKcS2_RNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6715CollationLoader9loadRulesEPKcS2_RNS_13UnicodeStringER10UErrorCode:
.LFB3311:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rcx), %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L186
	movq	%rdi, %r13
	movq	%rsi, %rdi
	movq	%rsi, %r12
	movq	%rdx, %r14
	movq	%rcx, %rbx
	call	strlen@PLT
	cmpl	$15, %eax
	jle	.L188
	movl	$1, (%rbx)
.L186:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L227
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L188:
	.cfi_restore_state
	leaq	-80(%rbp), %r15
	addl	$1, %eax
	movl	$16, %ecx
	movq	%r12, %rsi
	movslq	%eax, %rdx
	movq	%r15, %rdi
	call	__memcpy_chk@PLT
	movq	%r15, %rdi
	call	T_CString_toLowerCase_67@PLT
	movq	%r13, %rsi
	movq	%rbx, %rdx
	leaq	.LC3(%rip), %rdi
	call	ures_open_67@PLT
	movq	%rbx, %rcx
	xorl	%edx, %edx
	leaq	.LC5(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	ures_getByKey_67@PLT
	movq	%r15, %rsi
	movq	%rbx, %rcx
	xorl	%edx, %edx
	movq	%rax, %rdi
	movq	%rax, %r13
	call	ures_getByKeyWithFallback_67@PLT
	leaq	-84(%rbp), %rdx
	movq	%rbx, %rcx
	leaq	.LC6(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	ures_getStringByKey_67@PLT
	movl	(%rbx), %edx
	testl	%edx, %edx
	jle	.L190
.L195:
	testq	%r15, %r15
	je	.L196
	movq	%r15, %rdi
	call	ures_close_67@PLT
.L196:
	testq	%r13, %r13
	je	.L197
	movq	%r13, %rdi
	call	ures_close_67@PLT
.L197:
	testq	%r12, %r12
	je	.L186
	movq	%r12, %rdi
	call	ures_close_67@PLT
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L190:
	movl	-84(%rbp), %r9d
	movq	%r14, %rdi
	movq	%rax, -112(%rbp)
	movl	%r9d, -100(%rbp)
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	movswl	8(%r14), %edx
	movl	-100(%rbp), %r9d
	movq	-112(%rbp), %rax
	testw	%dx, %dx
	js	.L193
	sarl	$5, %edx
.L194:
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	%rax, %rcx
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	testb	$1, 8(%r14)
	je	.L195
	movl	$7, (%rbx)
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L193:
	movl	12(%r14), %edx
	jmp	.L194
.L227:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3311:
	.size	_ZN6icu_6715CollationLoader9loadRulesEPKcS2_RNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6715CollationLoader9loadRulesEPKcS2_RNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715CollationLoader22makeCacheEntryFromRootERKNS_6LocaleER10UErrorCode
	.type	_ZNK6icu_6715CollationLoader22makeCacheEntryFromRootERKNS_6LocaleER10UErrorCode, @function
_ZNK6icu_6715CollationLoader22makeCacheEntryFromRootERKNS_6LocaleER10UErrorCode:
.LFB3328:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	(%rdx), %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	xorl	%r14d, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%ecx, %ecx
	jg	.L228
	movq	%rdi, %r12
	movq	8(%rdi), %rdi
	movq	%rdx, %r13
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
	movl	0(%r13), %eax
	movq	8(%r12), %r14
	testl	%eax, %eax
	jle	.L237
.L228:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L237:
	.cfi_restore_state
	addq	$16, %r12
	leaq	24(%r14), %rsi
	movq	%r12, %rdi
	call	_ZNK6icu_676LocaleeqERKS0_@PLT
	testb	%al, %al
	jne	.L228
	movl	$256, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L230
	movq	248(%r14), %r13
	leaq	24(%rbx), %rdi
	movq	%r12, %rsi
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	leaq	16+_ZTVN6icu_6719CollationCacheEntryE(%rip), %rax
	movq	%rax, (%rbx)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movq	%r13, 248(%rbx)
	testq	%r13, %r13
	je	.L231
	movq	%r13, %rdi
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
.L231:
	movq	%rbx, %rdi
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
	movq	%r14, %rdi
	movq	%rbx, %r14
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L230:
	.cfi_restore_state
	movl	$7, 0(%r13)
	movq	%r14, %rdi
	xorl	%r14d, %r14d
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	jmp	.L228
	.cfi_endproc
.LFE3328:
	.size	_ZNK6icu_6715CollationLoader22makeCacheEntryFromRootERKNS_6LocaleER10UErrorCode, .-_ZNK6icu_6715CollationLoader22makeCacheEntryFromRootERKNS_6LocaleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715CollationLoader14makeCacheEntryERKNS_6LocaleEPKNS_19CollationCacheEntryER10UErrorCode
	.type	_ZN6icu_6715CollationLoader14makeCacheEntryERKNS_6LocaleEPKNS_19CollationCacheEntryER10UErrorCode, @function
_ZN6icu_6715CollationLoader14makeCacheEntryERKNS_6LocaleEPKNS_19CollationCacheEntryER10UErrorCode:
.LFB3329:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movl	(%rdx), %eax
	testl	%eax, %eax
	jle	.L247
.L239:
	movq	%r12, %r13
.L238:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L247:
	.cfi_restore_state
	leaq	24(%rsi), %rsi
	movq	%rdi, %r14
	movq	%rdx, %rbx
	call	_ZNK6icu_676LocaleeqERKS0_@PLT
	testb	%al, %al
	jne	.L239
	movl	$256, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L240
	movq	248(%r12), %r15
	leaq	24(%r13), %rdi
	movq	%r14, %rsi
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	leaq	16+_ZTVN6icu_6719CollationCacheEntryE(%rip), %rax
	movq	%rax, 0(%r13)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movq	%r15, 248(%r13)
	testq	%r15, %r15
	je	.L241
	movq	%r15, %rdi
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
.L241:
	movq	%r13, %rdi
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
	movq	%r12, %rdi
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	jmp	.L238
.L240:
	movl	$7, (%rbx)
	movq	%r12, %rdi
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	jmp	.L238
	.cfi_endproc
.LFE3329:
	.size	_ZN6icu_6715CollationLoader14makeCacheEntryERKNS_6LocaleEPKNS_19CollationCacheEntryER10UErrorCode, .-_ZN6icu_6715CollationLoader14makeCacheEntryERKNS_6LocaleEPKNS_19CollationCacheEntryER10UErrorCode
	.p2align 4
	.globl	ucol_getAvailable_67
	.type	ucol_getAvailable_67, @function
ucol_getAvailable_67:
.LFB3332:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movslq	%edi, %rbx
	leaq	-28(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	$0, -28(%rbp)
	call	_ZN6icu_678Collator19getAvailableLocalesERi@PLT
	testq	%rax, %rax
	je	.L248
	cmpl	%ebx, -28(%rbp)
	jle	.L251
	leaq	0(,%rbx,8), %rdx
	subq	%rbx, %rdx
	salq	$5, %rdx
	movq	40(%rax,%rdx), %rax
.L248:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L256
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L251:
	.cfi_restore_state
	xorl	%eax, %eax
	jmp	.L248
.L256:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3332:
	.size	ucol_getAvailable_67, .-ucol_getAvailable_67
	.p2align 4
	.globl	ucol_countAvailable_67
	.type	ucol_countAvailable_67, @function
ucol_countAvailable_67:
.LFB3333:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-12(%rbp), %rdi
	movl	$0, -12(%rbp)
	call	_ZN6icu_678Collator19getAvailableLocalesERi@PLT
	movl	-12(%rbp), %eax
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L260
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L260:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3333:
	.size	ucol_countAvailable_67, .-ucol_countAvailable_67
	.p2align 4
	.globl	ucol_getKeywords_67
	.type	ucol_getKeywords_67, @function
ucol_getKeywords_67:
.LFB3334:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	movq	%rdi, %rdx
	testl	%eax, %eax
	jle	.L263
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L263:
	movl	$1, %esi
	leaq	_ZL8KEYWORDS(%rip), %rdi
	jmp	uenum_openCharStringsEnumeration_67@PLT
	.cfi_endproc
.LFE3334:
	.size	ucol_getKeywords_67, .-ucol_getKeywords_67
	.section	.rodata.str1.1
.LC7:
	.string	"collation"
	.text
	.p2align 4
	.globl	ucol_getKeywordValues_67
	.type	ucol_getKeywordValues_67, @function
ucol_getKeywordValues_67:
.LFB3335:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	movq	%rdi, %rax
	movq	%rsi, %rdx
	testl	%ecx, %ecx
	jg	.L264
	testq	%rdi, %rdi
	je	.L266
	movq	%rax, %rsi
	movl	$10, %ecx
	leaq	.LC7(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L266
	leaq	_ZL13RESOURCE_NAME(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	jmp	ures_getKeywordValues_67@PLT
	.p2align 4,,10
	.p2align 3
.L266:
	movl	$1, (%rdx)
.L264:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3335:
	.size	ucol_getKeywordValues_67, .-ucol_getKeywordValues_67
	.p2align 4
	.globl	ucol_getKeywordValuesForLocale_67
	.type	ucol_getKeywordValuesForLocale_67, @function
ucol_getKeywordValuesForLocale_67:
.LFB3344:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %rdx
	leaq	.LC3(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	16+_ZTVN12_GLOBAL__N_112KeywordsSinkE(%rip), %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	ures_open_67@PLT
	movq	%rbx, %rdi
	movq	%r15, -80(%rbp)
	movq	%rax, %r13
	call	ulist_createEmptyList_67@PLT
	movq	%rbx, %rcx
	movq	%r14, %rdx
	movq	%r13, %rdi
	leaq	_ZL13RESOURCE_NAME(%rip), %rsi
	movq	%rax, -72(%rbp)
	movb	$0, -64(%rbp)
	call	ures_getAllItemsWithFallback_67@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L282
	movl	$56, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L283
	movdqa	_ZL20defaultKeywordValues(%rip), %xmm0
	movdqa	16+_ZL20defaultKeywordValues(%rip), %xmm1
	movdqa	32+_ZL20defaultKeywordValues(%rip), %xmm2
	movq	-72(%rbp), %rdi
	movups	%xmm0, (%rax)
	movups	%xmm1, 16(%rax)
	movups	%xmm2, 32(%rax)
	movq	48+_ZL20defaultKeywordValues(%rip), %rax
	movq	%rax, 48(%r12)
	call	ulist_resetList_67@PLT
	movq	-72(%rbp), %rax
	xorl	%edi, %edi
	movq	$0, -72(%rbp)
	movq	%rax, 8(%r12)
.L274:
	movq	%r15, -80(%rbp)
	call	ulist_deleteList_67@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6712ResourceSinkD2Ev@PLT
	testq	%r13, %r13
	je	.L272
	movq	%r13, %rdi
	call	ures_close_67@PLT
.L272:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L284
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L282:
	.cfi_restore_state
	movq	-72(%rbp), %rdi
	xorl	%r12d, %r12d
	jmp	.L274
.L284:
	call	__stack_chk_fail@PLT
.L283:
	movl	$7, (%rbx)
	movq	-72(%rbp), %rdi
	jmp	.L274
	.cfi_endproc
.LFE3344:
	.size	ucol_getKeywordValuesForLocale_67, .-ucol_getKeywordValuesForLocale_67
	.p2align 4
	.globl	ucol_getFunctionalEquivalent_67
	.type	ucol_getFunctionalEquivalent_67, @function
ucol_getFunctionalEquivalent_67:
.LFB3345:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	%r9
	movq	%rcx, %r9
	leaq	.LC5(%rip), %rcx
	pushq	$1
	pushq	%r8
	movq	%rdx, %r8
	leaq	.LC3(%rip), %rdx
	call	ures_getFunctionalEquivalent_67@PLT
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3345:
	.size	ucol_getFunctionalEquivalent_67, .-ucol_getFunctionalEquivalent_67
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715CollationLoaderC2EPKNS_19CollationCacheEntryERKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_6715CollationLoaderC2EPKNS_19CollationCacheEntryERKNS_6LocaleER10UErrorCode, @function
_ZN6icu_6715CollationLoaderC2EPKNS_19CollationCacheEntryERKNS_6LocaleER10UErrorCode:
.LFB3315:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%rcx, %rdi
	subq	$264, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6712UnifiedCache11getInstanceER10UErrorCode@PLT
	movq	%r12, %xmm1
	leaq	24(%r12), %rsi
	leaq	16(%rbx), %rdi
	movq	%rax, %xmm0
	leaq	240(%rbx), %r12
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movl	0(%r13), %ecx
	pxor	%xmm0, %xmm0
	movl	$0, 496(%rbx)
	movb	$0, 500(%rbx)
	movq	$0, 520(%rbx)
	movb	$0, 464(%rbx)
	movb	$0, 480(%rbx)
	movups	%xmm0, 504(%rbx)
	testl	%ecx, %ecx
	jle	.L301
.L287:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L302
	addq	$264, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L301:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK6icu_676Locale11getBaseNameEv@PLT
	movq	280(%rbx), %rdi
	movq	%rax, %rsi
	movq	%rax, %r15
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L287
	leaq	-288(%rbp), %r9
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r9, %rdi
	movq	%r9, -296(%rbp)
	leaq	464(%rbx), %r15
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	movq	-296(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	call	_ZN6icu_676LocaleaSEOS0_@PLT
	movq	-296(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	movq	%r15, %rdx
	movq	%r13, %r8
	movl	$15, %ecx
	leaq	.LC7(%rip), %rsi
	movq	%r14, %rdi
	call	_ZNK6icu_676Locale15getKeywordValueEPKcPciR10UErrorCode@PLT
	movl	0(%r13), %edx
	testl	%edx, %edx
	jg	.L303
	movslq	%eax, %rdx
	movb	$0, 464(%rbx,%rdx)
	testl	%eax, %eax
	je	.L287
	leaq	.LC0(%rip), %rsi
	movq	%r15, %rdi
	call	uprv_stricmp_67@PLT
	testl	%eax, %eax
	jne	.L292
	movb	$0, 464(%rbx)
	jmp	.L287
	.p2align 4,,10
	.p2align 3
.L303:
	movl	$1, 0(%r13)
	jmp	.L287
.L292:
	movq	%r15, %rdi
	call	T_CString_toLowerCase_67@PLT
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	leaq	.LC7(%rip), %rsi
	call	_ZN6icu_676Locale15setKeywordValueEPKcS2_R10UErrorCode@PLT
	jmp	.L287
.L302:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3315:
	.size	_ZN6icu_6715CollationLoaderC2EPKNS_19CollationCacheEntryERKNS_6LocaleER10UErrorCode, .-_ZN6icu_6715CollationLoaderC2EPKNS_19CollationCacheEntryERKNS_6LocaleER10UErrorCode
	.globl	_ZN6icu_6715CollationLoaderC1EPKNS_19CollationCacheEntryERKNS_6LocaleER10UErrorCode
	.set	_ZN6icu_6715CollationLoaderC1EPKNS_19CollationCacheEntryERKNS_6LocaleER10UErrorCode,_ZN6icu_6715CollationLoaderC2EPKNS_19CollationCacheEntryERKNS_6LocaleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715CollationLoaderD2Ev
	.type	_ZN6icu_6715CollationLoaderD2Ev, @function
_ZN6icu_6715CollationLoaderD2Ev:
.LFB3318:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	520(%rdi), %rdi
	call	ures_close_67@PLT
	movq	512(%rbx), %rdi
	call	ures_close_67@PLT
	movq	504(%rbx), %rdi
	call	ures_close_67@PLT
	leaq	240(%rbx), %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	addq	$8, %rsp
	leaq	16(%rbx), %rdi
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676LocaleD1Ev@PLT
	.cfi_endproc
.LFE3318:
	.size	_ZN6icu_6715CollationLoaderD2Ev, .-_ZN6icu_6715CollationLoaderD2Ev
	.globl	_ZN6icu_6715CollationLoaderD1Ev
	.set	_ZN6icu_6715CollationLoaderD1Ev,_ZN6icu_6715CollationLoaderD2Ev
	.section	.rodata.str1.1
.LC8:
	.string	"%%CollationBin"
.LC9:
	.string	"collations/default"
	.text
	.align 2
	.p2align 4
	.type	_ZN6icu_6715CollationLoader12loadFromDataER10UErrorCode.part.0, @function
_ZN6icu_6715CollationLoader12loadFromDataER10UErrorCode.part.0:
.LFB4548:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movl	$400, %edi
	subq	$552, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L307
	movq	%rax, %r12
	movq	8(%rbx), %rax
	movq	%r12, %rdi
	movq	248(%rax), %rax
	movq	32(%rax), %rsi
	call	_ZN6icu_6718CollationTailoringC1EPKNS_17CollationSettingsE@PLT
	cmpq	$0, 32(%r12)
	je	.L308
	movq	520(%rbx), %rdi
	movq	%r13, %rcx
	xorl	%edx, %edx
	leaq	.LC8(%rip), %rsi
	call	ures_getByKey_67@PLT
	leaq	-532(%rbp), %rsi
	movq	%r13, %rdx
	movq	%rax, %rdi
	movq	%rax, %r14
	call	ures_getBinary_67@PLT
	movl	-532(%rbp), %edx
	movq	%r13, %r8
	movq	%r12, %rcx
	movq	%rax, %rsi
	movq	8(%rbx), %rax
	movq	248(%rax), %rdi
	call	_ZN6icu_6719CollationDataReader4readEPKNS_18CollationTailoringEPKhiRS1_R10UErrorCode@PLT
	movl	0(%r13), %edi
	testl	%edi, %edi
	jle	.L349
.L317:
	testq	%r14, %r14
	je	.L310
	movq	%r14, %rdi
	call	ures_close_67@PLT
.L310:
	movq	(%r12), %rax
	movq	%r12, %rdi
	xorl	%r15d, %r15d
	call	*8(%rax)
.L306:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L350
	addq	$552, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L349:
	.cfi_restore_state
	leaq	-524(%rbp), %rax
	movq	520(%rbx), %rdi
	leaq	.LC6(%rip), %rsi
	movl	$0, -528(%rbp)
	leaq	-528(%rbp), %rcx
	movq	%rax, %rdx
	movq	%rax, -592(%rbp)
	call	ures_getStringByKey_67@PLT
	movl	-528(%rbp), %esi
	testl	%esi, %esi
	jle	.L351
.L312:
	leaq	240(%rbx), %r15
	movq	%r15, %rdi
	call	_ZNK6icu_676Locale11getBaseNameEv@PLT
	leaq	16(%rbx), %rcx
	movq	%rcx, %rdi
	movq	%rcx, -584(%rbp)
	movq	%rax, -552(%rbp)
	call	_ZNK6icu_676Locale11getBaseNameEv@PLT
	leaq	-288(%rbp), %r9
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r9, %rdi
	xorl	%r8d, %r8d
	movq	%r9, -568(%rbp)
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	movq	-552(%rbp), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	leaq	-512(%rbp), %rdi
	movq	%rdi, -560(%rbp)
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	movq	-568(%rbp), %r9
	movq	-560(%rbp), %rdi
	movq	%r9, %rsi
	movq	%r9, -576(%rbp)
	movq	%rdi, -568(%rbp)
	call	_ZNK6icu_676LocaleeqERKS0_@PLT
	movq	-568(%rbp), %rdi
	movb	%al, -560(%rbp)
	call	_ZN6icu_676LocaleD1Ev@PLT
	movq	-576(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	movzbl	-560(%rbp), %eax
	leaq	480(%rbx), %r9
	testb	%al, %al
	je	.L352
.L314:
	leaq	104(%r12), %r8
	movq	%r15, %rsi
	movq	%r9, -560(%rbp)
	movq	%r8, %rdi
	movq	%r8, -552(%rbp)
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	movq	-560(%rbp), %r9
	leaq	464(%rbx), %rdx
	movq	%rdx, %rdi
	movq	%rdx, -560(%rbp)
	movq	%r9, %rsi
	call	strcmp@PLT
	movq	-552(%rbp), %r8
	testl	%eax, %eax
	je	.L323
	movq	-560(%rbp), %rdx
	movq	%r13, %rcx
	leaq	.LC7(%rip), %rsi
	movq	%r8, %rdi
	call	_ZN6icu_676Locale15setKeywordValueEPKcS2_R10UErrorCode@PLT
.L324:
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L317
	cmpb	$0, 500(%rbx)
	je	.L325
	movl	$-127, 0(%r13)
.L325:
	movq	504(%rbx), %rax
	movl	$256, %edi
	movq	%rax, 360(%r12)
	movq	$0, 504(%rbx)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L326
	movq	$0, 8(%rax)
	movq	-584(%rbp), %rsi
	leaq	24(%r15), %rdi
	movq	$0, 16(%rax)
	leaq	16+_ZTVN6icu_6719CollationCacheEntryE(%rip), %rax
	movq	%rax, (%r15)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movq	%r12, 248(%r15)
	movq	%r12, %rdi
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
	movq	%r15, %rdi
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
	testq	%r14, %r14
	je	.L306
	movq	%r14, %rdi
	call	ures_close_67@PLT
	jmp	.L306
	.p2align 4,,10
	.p2align 3
.L352:
	movq	-552(%rbp), %rsi
	movq	%r13, %rdx
	leaq	.LC3(%rip), %rdi
	call	ures_open_67@PLT
	movl	0(%r13), %ecx
	testl	%ecx, %ecx
	jle	.L315
	testq	%rax, %rax
	je	.L317
	movq	%rax, %rdi
	call	ures_close_67@PLT
	jmp	.L317
	.p2align 4,,10
	.p2align 3
.L308:
	movl	$7, 0(%r13)
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L323:
	movq	%r15, %rdi
	movq	%r8, -552(%rbp)
	call	_ZNK6icu_676Locale11getBaseNameEv@PLT
	movq	280(%rbx), %rdi
	movq	%rax, %rsi
	call	strcmp@PLT
	movq	-552(%rbp), %r8
	testl	%eax, %eax
	je	.L324
	movq	%r13, %rcx
	xorl	%edx, %edx
	leaq	.LC7(%rip), %rsi
	movq	%r8, %rdi
	call	_ZN6icu_676Locale15setKeywordValueEPKcS2_R10UErrorCode@PLT
	jmp	.L324
	.p2align 4,,10
	.p2align 3
.L351:
	movl	-524(%rbp), %ecx
	leaq	40(%r12), %rdi
	leaq	-520(%rbp), %rdx
	movl	$1, %esi
	movq	%rax, -520(%rbp)
	call	_ZN6icu_6713UnicodeString5setToEaNS_14ConstChar16PtrEi@PLT
	jmp	.L312
	.p2align 4,,10
	.p2align 3
.L315:
	movq	-592(%rbp), %rcx
	xorl	%edx, %edx
	leaq	.LC9(%rip), %rsi
	movq	%rax, %rdi
	movl	$0, -524(%rbp)
	movq	%rax, -560(%rbp)
	call	ures_getByKeyWithFallback_67@PLT
	movq	-592(%rbp), %rdx
	leaq	-520(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rax, -552(%rbp)
	call	ures_getString_67@PLT
	movl	-524(%rbp), %edx
	movq	-552(%rbp), %r10
	leaq	480(%rbx), %r9
	movq	-560(%rbp), %r8
	testl	%edx, %edx
	jg	.L318
	movl	-520(%rbp), %edx
	cmpl	$15, %edx
	jle	.L319
.L318:
	movabsq	$7237954635114312819, %rax
	movq	%rax, 480(%rbx)
	movb	$0, 8(%r9)
.L320:
	testq	%r10, %r10
	je	.L321
	movq	%r10, %rdi
	movq	%r9, -560(%rbp)
	movq	%r8, -552(%rbp)
	call	ures_close_67@PLT
	movq	-560(%rbp), %r9
	movq	-552(%rbp), %r8
.L321:
	testq	%r8, %r8
	je	.L314
	movq	%r8, %rdi
	movq	%r9, -552(%rbp)
	call	ures_close_67@PLT
	movq	-552(%rbp), %r9
	jmp	.L314
	.p2align 4,,10
	.p2align 3
.L319:
	movq	%r9, %rsi
	addl	$1, %edx
	movq	%rax, %rdi
	movq	%r10, -568(%rbp)
	movq	%r8, -560(%rbp)
	movq	%r9, -552(%rbp)
	call	u_UCharsToChars_67@PLT
	movq	-568(%rbp), %r10
	movq	-560(%rbp), %r8
	movq	-552(%rbp), %r9
	jmp	.L320
.L350:
	call	__stack_chk_fail@PLT
.L307:
	movl	$7, 0(%r13)
	xorl	%r15d, %r15d
	jmp	.L306
.L326:
	movl	$7, 0(%r13)
	jmp	.L317
	.cfi_endproc
.LFE4548:
	.size	_ZN6icu_6715CollationLoader12loadFromDataER10UErrorCode.part.0, .-_ZN6icu_6715CollationLoader12loadFromDataER10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715CollationLoader12loadFromDataER10UErrorCode
	.type	_ZN6icu_6715CollationLoader12loadFromDataER10UErrorCode, @function
_ZN6icu_6715CollationLoader12loadFromDataER10UErrorCode:
.LFB3324:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L354
	jmp	_ZN6icu_6715CollationLoader12loadFromDataER10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L354:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3324:
	.size	_ZN6icu_6715CollationLoader12loadFromDataER10UErrorCode, .-_ZN6icu_6715CollationLoader12loadFromDataER10UErrorCode
	.section	.text._ZN6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEED2Ev,"axG",@progbits,_ZN6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEED2Ev
	.type	_ZN6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEED2Ev, @function
_ZN6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEED2Ev:
.LFB3861:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	16(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -16(%rdi)
	call	_ZN6icu_676LocaleD1Ev@PLT
	leaq	16+_ZTVN6icu_678CacheKeyINS_19CollationCacheEntryEEE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6712CacheKeyBaseD2Ev@PLT
	.cfi_endproc
.LFE3861:
	.size	_ZN6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEED2Ev, .-_ZN6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEED2Ev
	.weak	_ZN6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEED1Ev
	.set	_ZN6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEED1Ev,_ZN6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEED2Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715CollationLoader13getCacheEntryER10UErrorCode
	.type	_ZN6icu_6715CollationLoader13getCacheEntryER10UErrorCode, @function
_ZN6icu_6715CollationLoader13getCacheEntryER10UErrorCode:
.LFB3327:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-288(%rbp), %r14
	leaq	-304(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	16+_ZTVN6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEEE(%rip), %rbx
	subq	$296, %rsp
	movq	%rsi, -328(%rbp)
	leaq	240(%rdi), %rsi
	movq	%r14, %rdi
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	movb	$0, -292(%rbp)
	movl	$0, -296(%rbp)
	movq	%rbx, -304(%rbp)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movq	-328(%rbp), %rax
	movl	(%rax), %esi
	testl	%esi, %esi
	jle	.L372
.L358:
	movq	%r14, %rdi
	movq	%rbx, -304(%rbp)
	call	_ZN6icu_676LocaleD1Ev@PLT
	leaq	16+_ZTVN6icu_678CacheKeyINS_19CollationCacheEntryEEE(%rip), %rax
	movq	%r15, %rdi
	movq	%rax, -304(%rbp)
	call	_ZN6icu_6712CacheKeyBaseD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L373
	addq	$296, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L372:
	.cfi_restore_state
	movq	(%r12), %rdi
	leaq	-312(%rbp), %rdx
	movq	%r12, %rcx
	movq	%r15, %rsi
	leaq	-316(%rbp), %r8
	movl	$0, -316(%rbp)
	movq	$0, -312(%rbp)
	call	_ZNK6icu_6712UnifiedCache4_getERKNS_12CacheKeyBaseERPKNS_12SharedObjectEPKvR10UErrorCode@PLT
	movl	-316(%rbp), %eax
	movq	-312(%rbp), %r13
	testl	%eax, %eax
	jle	.L374
	testq	%r13, %r13
	jne	.L375
.L362:
	movq	-328(%rbp), %rdx
	movl	%eax, (%rdx)
	jmp	.L358
	.p2align 4,,10
	.p2align 3
.L375:
	movq	%r13, %r12
	xorl	%r13d, %r13d
.L361:
	movq	%r12, %rdi
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	movq	-328(%rbp), %rax
	movl	(%rax), %ecx
	movl	-316(%rbp), %eax
	testl	%ecx, %ecx
	je	.L362
	testl	%eax, %eax
	jle	.L358
	jmp	.L362
	.p2align 4,,10
	.p2align 3
.L374:
	testq	%r13, %r13
	je	.L360
	movq	%r13, %rdi
	movq	%r13, %r12
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
	jmp	.L361
	.p2align 4,,10
	.p2align 3
.L360:
	movq	-328(%rbp), %rcx
	movl	(%rcx), %edx
	testl	%edx, %edx
	je	.L362
	jmp	.L358
.L373:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3327:
	.size	_ZN6icu_6715CollationLoader13getCacheEntryER10UErrorCode, .-_ZN6icu_6715CollationLoader13getCacheEntryER10UErrorCode
	.section	.text._ZN6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEED0Ev,"axG",@progbits,_ZN6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEED0Ev
	.type	_ZN6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEED0Ev, @function
_ZN6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEED0Ev:
.LFB3863:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	16(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -16(%rdi)
	call	_ZN6icu_676LocaleD1Ev@PLT
	leaq	16+_ZTVN6icu_678CacheKeyINS_19CollationCacheEntryEEE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	call	_ZN6icu_6712CacheKeyBaseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3863:
	.size	_ZN6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEED0Ev, .-_ZN6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEED0Ev
	.section	.rodata.str1.1
.LC10:
	.string	"search"
.LC11:
	.string	"standard"
	.text
	.align 2
	.p2align 4
	.type	_ZN6icu_6715CollationLoader18loadFromCollationsER10UErrorCode.part.0, @function
_ZN6icu_6715CollationLoader18loadFromCollationsER10UErrorCode.part.0:
.LFB4549:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	464(%rdi), %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movq	%r15, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$504, %rsp
	.cfi_offset 3, -56
	movq	512(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	ures_getByKeyWithFallback_67@PLT
	movq	%rax, %r14
	movl	0(%r13), %eax
	cmpl	$2, %eax
	je	.L417
	testl	%eax, %eax
	jle	.L418
.L384:
	xorl	%r12d, %r12d
.L385:
	testq	%r14, %r14
	je	.L378
	movq	%r14, %rdi
	call	ures_close_67@PLT
.L378:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L419
	addq	$504, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L418:
	.cfi_restore_state
	movq	%r14, 520(%r12)
	movq	%r14, %rdi
	movq	%r13, %rdx
	xorl	%esi, %esi
	call	ures_getLocaleByType_67@PLT
	movl	0(%r13), %edi
	movq	%rax, %rbx
	testl	%edi, %edi
	jle	.L386
.L416:
	xorl	%r12d, %r12d
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L417:
	movq	%r15, %rdi
	call	strlen@PLT
	movl	$-127, 0(%r13)
	movb	$1, 500(%r12)
	movq	%rax, %r8
	movl	496(%r12), %eax
	cmpl	$6, %r8d
	jle	.L380
	testb	$1, %al
	je	.L420
.L380:
	testb	$2, %al
	je	.L421
	testb	$4, %al
	je	.L422
	movl	0(%r13), %r8d
	testl	%r8d, %r8d
	jg	.L384
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZNK6icu_6715CollationLoader22makeCacheEntryFromRootERKNS_6LocaleER10UErrorCode.part.0
	movq	%rax, %r12
	jmp	.L385
	.p2align 4,,10
	.p2align 3
.L386:
	leaq	16(%r12), %rax
	leaq	-288(%rbp), %r14
	movq	%rax, %rdi
	movq	%rax, -536(%rbp)
	call	_ZNK6icu_676Locale11getBaseNameEv@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	leaq	-512(%rbp), %rdi
	movq	%rbx, %rsi
	movq	%rdi, -520(%rbp)
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	movq	-520(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZNK6icu_676LocaleeqERKS0_@PLT
	movq	-520(%rbp), %rdi
	movb	%al, -521(%rbp)
	call	_ZN6icu_676LocaleD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	leaq	480(%r12), %rsi
	movq	%r15, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L388
.L392:
	cmpb	$0, (%rbx)
	jne	.L423
.L389:
	movabsq	$7237954635114312819, %rax
	cmpq	%rax, 464(%r12)
	je	.L424
.L393:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	leaq	240(%r12), %r8
	movq	%r14, %rsi
	movq	%r8, %rdi
	movq	%r8, -520(%rbp)
	call	_ZN6icu_676LocaleaSEOS0_@PLT
	movq	%r14, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	cmpb	$0, -521(%rbp)
	movq	-520(%rbp), %r8
	je	.L425
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L416
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6715CollationLoader12loadFromDataER10UErrorCode.part.0
	movq	%rax, %r12
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L421:
	orl	$2, %eax
	leaq	480(%r12), %rsi
	movl	$16, %edx
	movq	%r15, %rdi
	movl	%eax, 496(%r12)
	call	__strcpy_chk@PLT
.L381:
	leaq	240(%r12), %rdi
	movq	%r13, %rcx
	movq	%r15, %rdx
	leaq	.LC7(%rip), %rsi
	call	_ZN6icu_676Locale15setKeywordValueEPKcS2_R10UErrorCode@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6715CollationLoader13getCacheEntryER10UErrorCode
	movq	%rax, %r12
	jmp	.L385
	.p2align 4,,10
	.p2align 3
.L420:
	movl	$6, %ecx
	leaq	.LC10(%rip), %rdi
	movq	%r15, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	jne	.L380
	movb	$0, 470(%r12)
	orl	$1, %eax
	movl	%eax, 496(%r12)
	jmp	.L381
	.p2align 4,,10
	.p2align 3
.L422:
	orl	$4, %eax
	movl	%eax, 496(%r12)
	movabsq	$7237954635114312819, %rax
	movq	%rax, 464(%r12)
	movb	$0, 8(%r15)
	jmp	.L381
	.p2align 4,,10
	.p2align 3
.L423:
	movl	$5, %ecx
	leaq	.LC2(%rip), %rdi
	movq	%rbx, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L393
	jmp	.L389
	.p2align 4,,10
	.p2align 3
.L424:
	cmpb	$0, 8(%r15)
	jne	.L393
	cmpb	$0, 500(%r12)
	je	.L396
	movl	$-127, 0(%r13)
.L397:
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZNK6icu_6715CollationLoader22makeCacheEntryFromRootERKNS_6LocaleER10UErrorCode.part.0
	movq	%rax, %r12
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L425:
	movq	%r15, %rdx
	movq	%r8, %rdi
	leaq	.LC7(%rip), %rsi
	movq	%r13, %rcx
	call	_ZN6icu_676Locale15setKeywordValueEPKcS2_R10UErrorCode@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6715CollationLoader13getCacheEntryER10UErrorCode
	movl	0(%r13), %edx
	movq	%rax, %r12
	testl	%edx, %edx
	jg	.L378
	movq	-536(%rbp), %r15
	leaq	24(%rax), %rsi
	movq	%r15, %rdi
	call	_ZNK6icu_676LocaleeqERKS0_@PLT
	testb	%al, %al
	jne	.L378
	movl	$256, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L399
	movq	248(%r12), %r13
	leaq	24(%rbx), %rdi
	movq	%r15, %rsi
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	leaq	16+_ZTVN6icu_6719CollationCacheEntryE(%rip), %rax
	movq	%rax, (%rbx)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movq	%r13, 248(%rbx)
	testq	%r13, %r13
	je	.L400
	movq	%r13, %rdi
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
.L400:
	movq	%rbx, %rdi
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
	movq	%r12, %rdi
	movq	%rbx, %r12
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L388:
	movq	-536(%rbp), %rdi
	leaq	.LC7(%rip), %rsi
	movq	%r13, %rcx
	movq	%r15, %rdx
	call	_ZN6icu_676Locale15setKeywordValueEPKcS2_R10UErrorCode@PLT
	movl	0(%r13), %esi
	testl	%esi, %esi
	jle	.L392
	jmp	.L416
.L396:
	movl	0(%r13), %ecx
	testl	%ecx, %ecx
	jg	.L416
	jmp	.L397
.L419:
	call	__stack_chk_fail@PLT
.L399:
	movl	$7, 0(%r13)
	movq	%r12, %rdi
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	jmp	.L416
	.cfi_endproc
.LFE4549:
	.size	_ZN6icu_6715CollationLoader18loadFromCollationsER10UErrorCode.part.0, .-_ZN6icu_6715CollationLoader18loadFromCollationsER10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715CollationLoader18loadFromCollationsER10UErrorCode
	.type	_ZN6icu_6715CollationLoader18loadFromCollationsER10UErrorCode, @function
_ZN6icu_6715CollationLoader18loadFromCollationsER10UErrorCode:
.LFB3323:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L427
	jmp	_ZN6icu_6715CollationLoader18loadFromCollationsER10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L427:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3323:
	.size	_ZN6icu_6715CollationLoader18loadFromCollationsER10UErrorCode, .-_ZN6icu_6715CollationLoader18loadFromCollationsER10UErrorCode
	.align 2
	.p2align 4
	.type	_ZN6icu_6715CollationLoader14loadFromBundleER10UErrorCode.part.0, @function
_ZN6icu_6715CollationLoader14loadFromBundleER10UErrorCode.part.0:
.LFB4550:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	leaq	.LC5(%rip), %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$16, %rsp
	movq	504(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	ures_getByKey_67@PLT
	movq	%rax, 512(%r12)
	movq	%rax, %rdi
	movl	0(%r13), %eax
	cmpl	$2, %eax
	je	.L457
	testl	%eax, %eax
	jg	.L456
	leaq	-48(%rbp), %r15
	xorl	%edx, %edx
	leaq	.LC0(%rip), %rsi
	movl	$0, -48(%rbp)
	movq	%r15, %rcx
	call	ures_getByKeyWithFallback_67@PLT
	movq	%r15, %rdx
	leaq	-44(%rbp), %rsi
	leaq	480(%r12), %r15
	movq	%rax, %rdi
	movq	%rax, %r14
	call	ures_getString_67@PLT
	movl	-48(%rbp), %edx
	testl	%edx, %edx
	jg	.L434
	movl	-44(%rbp), %edx
	leal	-1(%rdx), %ecx
	cmpl	$14, %ecx
	jbe	.L458
.L434:
	movabsq	$7237954635114312819, %rax
	movq	%rax, 480(%r12)
	movb	$0, 8(%r15)
.L435:
	testq	%r14, %r14
	je	.L436
	movq	%r14, %rdi
	call	ures_close_67@PLT
.L436:
	cmpb	$0, 464(%r12)
	leaq	464(%r12), %r14
	je	.L459
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L460
	cmpl	$1918985587, 464(%r12)
	je	.L461
.L443:
	movabsq	$7237954635114312819, %rax
	cmpq	%rax, 464(%r12)
	je	.L462
.L446:
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L456
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6715CollationLoader18loadFromCollationsER10UErrorCode.part.0
	movq	%rax, %r14
	jmp	.L428
.L431:
	movl	$7, 0(%r13)
	movq	%r14, %rdi
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	.p2align 4,,10
	.p2align 3
.L456:
	xorl	%r14d, %r14d
.L428:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L463
	addq	$16, %rsp
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L457:
	.cfi_restore_state
	movl	$-127, 0(%r13)
	movq	8(%r12), %rdi
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
	movl	0(%r13), %ecx
	movq	8(%r12), %r14
	testl	%ecx, %ecx
	jg	.L428
	addq	$16, %r12
	leaq	24(%r14), %rsi
	movq	%r12, %rdi
	call	_ZNK6icu_676LocaleeqERKS0_@PLT
	testb	%al, %al
	jne	.L428
	movl	$256, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L431
	movq	248(%r14), %r13
	leaq	24(%r15), %rdi
	movq	%r12, %rsi
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	leaq	16+_ZTVN6icu_6719CollationCacheEntryE(%rip), %rax
	movq	%rax, (%r15)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movq	%r13, 248(%r15)
	testq	%r13, %r13
	je	.L432
	movq	%r13, %rdi
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
.L432:
	movq	%r15, %rdi
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
	movq	%r14, %rdi
	movq	%r15, %r14
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	jmp	.L428
	.p2align 4,,10
	.p2align 3
.L460:
	orl	$2, 496(%r12)
	cmpl	$1918985587, 464(%r12)
	jne	.L443
.L461:
	cmpw	$26723, 4(%r14)
	jne	.L443
	cmpb	$0, 6(%r14)
	jne	.L443
	orl	$1, 496(%r12)
	jmp	.L443
	.p2align 4,,10
	.p2align 3
.L459:
	movl	$16, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	__strcpy_chk@PLT
	movl	496(%r12), %r8d
	movl	$7, %ecx
	movq	%r14, %rsi
	leaq	.LC10(%rip), %rdi
	movl	%r8d, %eax
	orl	$2, %eax
	movl	%eax, 496(%r12)
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	jne	.L438
	movl	%r8d, %eax
	orl	$3, %eax
	movl	%eax, 496(%r12)
.L438:
	movl	$9, %ecx
	leaq	.LC11(%rip), %rdi
	movq	%r14, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	jne	.L439
	orl	$4, %eax
	movl	%eax, 496(%r12)
.L439:
	movq	%r14, %rdx
	leaq	240(%r12), %rdi
	movq	%r13, %rcx
	leaq	.LC7(%rip), %rsi
	call	_ZN6icu_676Locale15setKeywordValueEPKcS2_R10UErrorCode@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6715CollationLoader13getCacheEntryER10UErrorCode
	movq	%rax, %r14
	jmp	.L428
	.p2align 4,,10
	.p2align 3
.L462:
	cmpb	$0, 8(%r14)
	jne	.L446
	orl	$4, 496(%r12)
	jmp	.L446
	.p2align 4,,10
	.p2align 3
.L458:
	addl	$1, %edx
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	u_UCharsToChars_67@PLT
	jmp	.L435
.L463:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4550:
	.size	_ZN6icu_6715CollationLoader14loadFromBundleER10UErrorCode.part.0, .-_ZN6icu_6715CollationLoader14loadFromBundleER10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715CollationLoader14loadFromBundleER10UErrorCode
	.type	_ZN6icu_6715CollationLoader14loadFromBundleER10UErrorCode, @function
_ZN6icu_6715CollationLoader14loadFromBundleER10UErrorCode:
.LFB3322:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L465
	jmp	_ZN6icu_6715CollationLoader14loadFromBundleER10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L465:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3322:
	.size	_ZN6icu_6715CollationLoader14loadFromBundleER10UErrorCode, .-_ZN6icu_6715CollationLoader14loadFromBundleER10UErrorCode
	.align 2
	.p2align 4
	.type	_ZN6icu_6715CollationLoader14loadFromLocaleER10UErrorCode.part.0, @function
_ZN6icu_6715CollationLoader14loadFromLocaleER10UErrorCode.part.0:
.LFB4551:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	240(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r14, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$472, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK6icu_676Locale11getBaseNameEv@PLT
	movq	%r12, %rdx
	leaq	.LC3(%rip), %rdi
	movq	%rax, %rsi
	call	ures_openNoDefault_67@PLT
	cmpl	$2, (%r12)
	movq	%rax, 504(%r13)
	je	.L476
	leaq	-512(%rbp), %r15
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movq	504(%r13), %rdi
	movq	%r12, %rdx
	xorl	%esi, %esi
	call	ures_getLocaleByType_67@PLT
	movl	(%r12), %edx
	testl	%edx, %edx
	jle	.L469
.L473:
	xorl	%r12d, %r12d
.L470:
	movq	%r15, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
.L466:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L477
	addq	$472, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L469:
	.cfi_restore_state
	leaq	-288(%rbp), %rbx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	leaq	16(%r13), %rdi
	movq	%rbx, %rsi
	call	_ZN6icu_676LocaleaSEOS0_@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	cmpb	$0, 464(%r13)
	jne	.L478
.L471:
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZNK6icu_676LocaleeqERKS0_@PLT
	testb	%al, %al
	je	.L479
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L473
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6715CollationLoader14loadFromBundleER10UErrorCode.part.0
	movq	%rax, %r12
	jmp	.L470
	.p2align 4,,10
	.p2align 3
.L476:
	movl	$-127, (%r12)
	movq	8(%r13), %rdi
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
	movq	8(%r13), %r12
	jmp	.L466
	.p2align 4,,10
	.p2align 3
.L479:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6715CollationLoader13getCacheEntryER10UErrorCode
	movq	%rax, %r12
	jmp	.L470
	.p2align 4,,10
	.p2align 3
.L478:
	leaq	464(%r13), %rdx
	movq	%r12, %rcx
	leaq	.LC7(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN6icu_676Locale15setKeywordValueEPKcS2_R10UErrorCode@PLT
	jmp	.L471
.L477:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4551:
	.size	_ZN6icu_6715CollationLoader14loadFromLocaleER10UErrorCode.part.0, .-_ZN6icu_6715CollationLoader14loadFromLocaleER10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715CollationLoader14loadFromLocaleER10UErrorCode
	.type	_ZN6icu_6715CollationLoader14loadFromLocaleER10UErrorCode, @function
_ZN6icu_6715CollationLoader14loadFromLocaleER10UErrorCode:
.LFB3321:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L481
	jmp	_ZN6icu_6715CollationLoader14loadFromLocaleER10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L481:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3321:
	.size	_ZN6icu_6715CollationLoader14loadFromLocaleER10UErrorCode, .-_ZN6icu_6715CollationLoader14loadFromLocaleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715CollationLoader16createCacheEntryER10UErrorCode
	.type	_ZN6icu_6715CollationLoader16createCacheEntryER10UErrorCode, @function
_ZN6icu_6715CollationLoader16createCacheEntryER10UErrorCode:
.LFB3320:
	.cfi_startproc
	endbr64
	cmpq	$0, 504(%rdi)
	movl	(%rsi), %eax
	je	.L488
	cmpq	$0, 512(%rdi)
	je	.L489
	cmpq	$0, 520(%rdi)
	je	.L490
	testl	%eax, %eax
	jg	.L482
	jmp	_ZN6icu_6715CollationLoader12loadFromDataER10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L489:
	testl	%eax, %eax
	jg	.L482
	jmp	_ZN6icu_6715CollationLoader14loadFromBundleER10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L488:
	testl	%eax, %eax
	jle	.L491
.L482:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L491:
	jmp	_ZN6icu_6715CollationLoader14loadFromLocaleER10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L490:
	testl	%eax, %eax
	jg	.L482
	jmp	_ZN6icu_6715CollationLoader18loadFromCollationsER10UErrorCode.part.0
	.cfi_endproc
.LFE3320:
	.size	_ZN6icu_6715CollationLoader16createCacheEntryER10UErrorCode, .-_ZN6icu_6715CollationLoader16createCacheEntryER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEE12createObjectEPKvR10UErrorCode
	.type	_ZNK6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEE12createObjectEPKvR10UErrorCode, @function
_ZNK6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEE12createObjectEPKvR10UErrorCode:
.LFB3312:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdi
	movl	(%rdx), %eax
	movq	%rdx, %rsi
	cmpq	$0, 504(%rdi)
	je	.L498
	cmpq	$0, 512(%rdi)
	je	.L499
	cmpq	$0, 520(%rdi)
	je	.L500
	testl	%eax, %eax
	jg	.L492
	jmp	_ZN6icu_6715CollationLoader12loadFromDataER10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L499:
	testl	%eax, %eax
	jg	.L492
	jmp	_ZN6icu_6715CollationLoader14loadFromBundleER10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L498:
	testl	%eax, %eax
	jle	.L501
.L492:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L501:
	jmp	_ZN6icu_6715CollationLoader14loadFromLocaleER10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L500:
	testl	%eax, %eax
	jg	.L492
	jmp	_ZN6icu_6715CollationLoader18loadFromCollationsER10UErrorCode.part.0
	.cfi_endproc
.LFE3312:
	.size	_ZNK6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEE12createObjectEPKvR10UErrorCode, .-_ZNK6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEE12createObjectEPKvR10UErrorCode
	.p2align 4
	.globl	ucol_open_67
	.type	ucol_open_67, @function
ucol_open_67:
.LFB3330:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-272(%rbp), %r13
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	movq	%rdi, %rsi
	movq	%r13, %rdi
	subq	$248, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_678Collator14createInstanceERKNS_6LocaleER10UErrorCode@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN6icu_676LocaleD1Ev@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	movl	$0, %eax
	cmovg	%rax, %r12
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L507
	addq	$248, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L507:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3330:
	.size	ucol_open_67, .-ucol_open_67
	.p2align 4
	.globl	ucol_getDisplayName_67
	.type	ucol_getDisplayName_67, @function
ucol_getDisplayName_67:
.LFB3331:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$568, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r8), %r9d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r9d, %r9d
	jg	.L513
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rdi, %r10
	movq	%rsi, %r15
	movq	%rdx, %rbx
	movq	%rax, -576(%rbp)
	movl	$2, %eax
	movl	%ecx, %r12d
	movq	%r8, %r9
	movw	%ax, -568(%rbp)
	testq	%rdx, %rdx
	jne	.L514
	leaq	-576(%rbp), %r13
	testl	%ecx, %ecx
	jne	.L514
.L510:
	leaq	-288(%rbp), %r14
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%r9, -608(%rbp)
	leaq	-512(%rbp), %r15
	movq	%r10, -600(%rbp)
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	movq	-600(%rbp), %r10
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movq	%r10, %rsi
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_678Collator14getDisplayNameERKNS_6LocaleES3_RNS_13UnicodeStringE@PLT
	movq	%r15, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	movq	-608(%rbp), %r9
	movl	%r12d, %edx
	movq	%r13, %rdi
	leaq	-584(%rbp), %rsi
	movq	%rbx, -584(%rbp)
	movq	%r9, %rcx
	call	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode@PLT
	movl	%eax, %r12d
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L508:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L519
	addq	$568, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L514:
	.cfi_restore_state
	leaq	-576(%rbp), %r13
	movl	%r12d, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%r9, -608(%rbp)
	movq	%r10, -600(%rbp)
	call	_ZN6icu_6713UnicodeString5setToEPDsii@PLT
	movq	-608(%rbp), %r9
	movq	-600(%rbp), %r10
	jmp	.L510
	.p2align 4,,10
	.p2align 3
.L513:
	movl	$-1, %r12d
	jmp	.L508
.L519:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3331:
	.size	ucol_getDisplayName_67, .-ucol_getDisplayName_67
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715CollationLoader13loadTailoringERKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_6715CollationLoader13loadTailoringERKNS_6LocaleER10UErrorCode, @function
_ZN6icu_6715CollationLoader13loadTailoringERKNS_6LocaleER10UErrorCode:
.LFB3313:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movq	%rsi, %rdi
	pushq	%r13
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	subq	$544, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6713CollationRoot17getRootCacheEntryER10UErrorCode@PLT
	movl	(%r12), %edx
	testl	%edx, %edx
	jg	.L520
	movq	40(%r14), %rsi
	movq	%rax, %r13
	cmpb	$0, (%rsi)
	je	.L522
	movl	$5, %ecx
	leaq	.LC2(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L523
.L522:
	movq	%r13, %rdi
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
.L520:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L527
	addq	$544, %rsp
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L523:
	.cfi_restore_state
	leaq	-576(%rbp), %r15
	movq	%r12, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movl	$0, (%r12)
	movq	%r15, %rdi
	call	_ZN6icu_6715CollationLoaderC1EPKNS_19CollationCacheEntryERKNS_6LocaleER10UErrorCode
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6715CollationLoader13getCacheEntryER10UErrorCode
	movq	-56(%rbp), %rdi
	movq	%rax, %r13
	call	ures_close_67@PLT
	movq	-64(%rbp), %rdi
	call	ures_close_67@PLT
	movq	-72(%rbp), %rdi
	call	ures_close_67@PLT
	leaq	-336(%rbp), %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	leaq	-560(%rbp), %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	jmp	.L520
.L527:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3313:
	.size	_ZN6icu_6715CollationLoader13loadTailoringERKNS_6LocaleER10UErrorCode, .-_ZN6icu_6715CollationLoader13loadTailoringERKNS_6LocaleER10UErrorCode
	.weak	_ZTSN6icu_678CacheKeyINS_19CollationCacheEntryEEE
	.section	.rodata._ZTSN6icu_678CacheKeyINS_19CollationCacheEntryEEE,"aG",@progbits,_ZTSN6icu_678CacheKeyINS_19CollationCacheEntryEEE,comdat
	.align 32
	.type	_ZTSN6icu_678CacheKeyINS_19CollationCacheEntryEEE, @object
	.size	_ZTSN6icu_678CacheKeyINS_19CollationCacheEntryEEE, 46
_ZTSN6icu_678CacheKeyINS_19CollationCacheEntryEEE:
	.string	"N6icu_678CacheKeyINS_19CollationCacheEntryEEE"
	.weak	_ZTIN6icu_678CacheKeyINS_19CollationCacheEntryEEE
	.section	.data.rel.ro._ZTIN6icu_678CacheKeyINS_19CollationCacheEntryEEE,"awG",@progbits,_ZTIN6icu_678CacheKeyINS_19CollationCacheEntryEEE,comdat
	.align 8
	.type	_ZTIN6icu_678CacheKeyINS_19CollationCacheEntryEEE, @object
	.size	_ZTIN6icu_678CacheKeyINS_19CollationCacheEntryEEE, 24
_ZTIN6icu_678CacheKeyINS_19CollationCacheEntryEEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_678CacheKeyINS_19CollationCacheEntryEEE
	.quad	_ZTIN6icu_6712CacheKeyBaseE
	.weak	_ZTSN6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEEE
	.section	.rodata._ZTSN6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEEE,"aG",@progbits,_ZTSN6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEEE,comdat
	.align 32
	.type	_ZTSN6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEEE, @object
	.size	_ZTSN6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEEE, 53
_ZTSN6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEEE:
	.string	"N6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEEE"
	.weak	_ZTIN6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEEE
	.section	.data.rel.ro._ZTIN6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEEE,"awG",@progbits,_ZTIN6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEEE,comdat
	.align 8
	.type	_ZTIN6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEEE, @object
	.size	_ZTIN6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEEE, 24
_ZTIN6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEEE
	.quad	_ZTIN6icu_678CacheKeyINS_19CollationCacheEntryEEE
	.section	.data.rel.ro,"aw"
	.align 8
	.type	_ZTIN12_GLOBAL__N_112KeywordsSinkE, @object
	.size	_ZTIN12_GLOBAL__N_112KeywordsSinkE, 24
_ZTIN12_GLOBAL__N_112KeywordsSinkE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN12_GLOBAL__N_112KeywordsSinkE
	.quad	_ZTIN6icu_6712ResourceSinkE
	.section	.rodata
	.align 32
	.type	_ZTSN12_GLOBAL__N_112KeywordsSinkE, @object
	.size	_ZTSN12_GLOBAL__N_112KeywordsSinkE, 32
_ZTSN12_GLOBAL__N_112KeywordsSinkE:
	.string	"*N12_GLOBAL__N_112KeywordsSinkE"
	.weak	_ZTVN6icu_678CacheKeyINS_19CollationCacheEntryEEE
	.section	.data.rel.ro._ZTVN6icu_678CacheKeyINS_19CollationCacheEntryEEE,"awG",@progbits,_ZTVN6icu_678CacheKeyINS_19CollationCacheEntryEEE,comdat
	.align 8
	.type	_ZTVN6icu_678CacheKeyINS_19CollationCacheEntryEEE, @object
	.size	_ZTVN6icu_678CacheKeyINS_19CollationCacheEntryEEE, 80
_ZTVN6icu_678CacheKeyINS_19CollationCacheEntryEEE:
	.quad	0
	.quad	_ZTIN6icu_678CacheKeyINS_19CollationCacheEntryEEE
	.quad	0
	.quad	0
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZNK6icu_678CacheKeyINS_19CollationCacheEntryEE8hashCodeEv
	.quad	__cxa_pure_virtual
	.quad	_ZNK6icu_678CacheKeyINS_19CollationCacheEntryEEeqERKNS_12CacheKeyBaseE
	.quad	__cxa_pure_virtual
	.quad	_ZNK6icu_678CacheKeyINS_19CollationCacheEntryEE16writeDescriptionEPci
	.weak	_ZTVN6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEEE
	.section	.data.rel.ro._ZTVN6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEEE,"awG",@progbits,_ZTVN6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEEE,comdat
	.align 8
	.type	_ZTVN6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEEE, @object
	.size	_ZTVN6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEEE, 80
_ZTVN6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEEE:
	.quad	0
	.quad	_ZTIN6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEEE
	.quad	_ZN6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEED1Ev
	.quad	_ZN6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEED0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZNK6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEE8hashCodeEv
	.quad	_ZNK6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEE5cloneEv
	.quad	_ZNK6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEEeqERKNS_12CacheKeyBaseE
	.quad	_ZNK6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEE12createObjectEPKvR10UErrorCode
	.quad	_ZNK6icu_6714LocaleCacheKeyINS_19CollationCacheEntryEE16writeDescriptionEPci
	.section	.data.rel.ro
	.align 8
	.type	_ZTVN12_GLOBAL__N_112KeywordsSinkE, @object
	.size	_ZTVN12_GLOBAL__N_112KeywordsSinkE, 48
_ZTVN12_GLOBAL__N_112KeywordsSinkE:
	.quad	0
	.quad	_ZTIN12_GLOBAL__N_112KeywordsSinkE
	.quad	_ZN12_GLOBAL__N_112KeywordsSinkD1Ev
	.quad	_ZN12_GLOBAL__N_112KeywordsSinkD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZN12_GLOBAL__N_112KeywordsSink3putEPKcRN6icu_6713ResourceValueEaR10UErrorCode
	.align 32
	.type	_ZL20defaultKeywordValues, @object
	.size	_ZL20defaultKeywordValues, 56
_ZL20defaultKeywordValues:
	.quad	0
	.quad	0
	.quad	ulist_close_keyword_values_iterator_67
	.quad	ulist_count_keyword_values_67
	.quad	uenum_unextDefault_67
	.quad	ulist_next_keyword_value_67
	.quad	ulist_reset_keyword_values_iterator_67
	.section	.data.rel.ro.local,"aw"
	.align 8
	.type	_ZL8KEYWORDS, @object
	.size	_ZL8KEYWORDS, 8
_ZL8KEYWORDS:
	.quad	.LC7
	.section	.rodata
	.align 8
	.type	_ZL13RESOURCE_NAME, @object
	.size	_ZL13RESOURCE_NAME, 11
_ZL13RESOURCE_NAME:
	.string	"collations"
	.local	_ZN6icu_6712_GLOBAL__N_1L16gInitOnceUcolResE
	.comm	_ZN6icu_6712_GLOBAL__N_1L16gInitOnceUcolResE,8,8
	.local	_ZN6icu_6712_GLOBAL__N_1L10rootBundleE
	.comm	_ZN6icu_6712_GLOBAL__N_1L10rootBundleE,8,8
	.local	_ZN6icu_6712_GLOBAL__N_1L15rootRulesLengthE
	.comm	_ZN6icu_6712_GLOBAL__N_1L15rootRulesLengthE,4,4
	.local	_ZN6icu_6712_GLOBAL__N_1L9rootRulesE
	.comm	_ZN6icu_6712_GLOBAL__N_1L9rootRulesE,8,8
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
