	.file	"datefmt.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEE12createObjectEPKvR10UErrorCode
	.type	_ZNK6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEE12createObjectEPKvR10UErrorCode, @function
_ZNK6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEE12createObjectEPKvR10UErrorCode:
.LFB3322:
	.cfi_startproc
	endbr64
	movl	$16, (%rdx)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3322:
	.size	_ZNK6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEE12createObjectEPKvR10UErrorCode, .-_ZNK6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEE12createObjectEPKvR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710DateFormat6formatERNS_8CalendarERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode
	.type	_ZNK6icu_6710DateFormat6formatERNS_8CalendarERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode, @function
_ZNK6icu_6710DateFormat6formatERNS_8CalendarERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode:
.LFB3353:
	.cfi_startproc
	endbr64
	movq	%rdx, %rax
	movl	(%r8), %edx
	testl	%edx, %edx
	jle	.L5
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movl	$16, (%r8)
	ret
	.cfi_endproc
.LFE3353:
	.size	_ZNK6icu_6710DateFormat6formatERNS_8CalendarERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode, .-_ZNK6icu_6710DateFormat6formatERNS_8CalendarERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710DateFormat13adoptCalendarEPNS_8CalendarE
	.type	_ZN6icu_6710DateFormat13adoptCalendarEPNS_8CalendarE, @function
_ZN6icu_6710DateFormat13adoptCalendarEPNS_8CalendarE:
.LFB3372:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	328(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L7
	movq	(%rdi), %rax
	call	*8(%rax)
.L7:
	movq	%r12, 328(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3372:
	.size	_ZN6icu_6710DateFormat13adoptCalendarEPNS_8CalendarE, .-_ZN6icu_6710DateFormat13adoptCalendarEPNS_8CalendarE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710DateFormat11setCalendarERKNS_8CalendarE
	.type	_ZN6icu_6710DateFormat11setCalendarERKNS_8CalendarE, @function
_ZN6icu_6710DateFormat11setCalendarERKNS_8CalendarE:
.LFB3373:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	subq	$8, %rsp
	movq	(%rsi), %rax
	call	*24(%rax)
	testq	%rax, %rax
	je	.L12
	movq	%rax, %rsi
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	136(%rax), %rax
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L12:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3373:
	.size	_ZN6icu_6710DateFormat11setCalendarERKNS_8CalendarE, .-_ZN6icu_6710DateFormat11setCalendarERKNS_8CalendarE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710DateFormat11getCalendarEv
	.type	_ZNK6icu_6710DateFormat11getCalendarEv, @function
_ZNK6icu_6710DateFormat11getCalendarEv:
.LFB3374:
	.cfi_startproc
	endbr64
	movq	328(%rdi), %rax
	ret
	.cfi_endproc
.LFE3374:
	.size	_ZNK6icu_6710DateFormat11getCalendarEv, .-_ZNK6icu_6710DateFormat11getCalendarEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710DateFormat17adoptNumberFormatEPNS_12NumberFormatE
	.type	_ZN6icu_6710DateFormat17adoptNumberFormatEPNS_12NumberFormatE, @function
_ZN6icu_6710DateFormat17adoptNumberFormatEPNS_12NumberFormatE:
.LFB3375:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	336(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L17
	movq	(%rdi), %rax
	call	*8(%rax)
.L17:
	movq	(%r12), %rax
	movq	%r12, 336(%rbx)
	movq	%r12, %rdi
	movl	$1, %esi
	call	*184(%rax)
	movq	(%r12), %rax
	popq	%rbx
	movq	%r12, %rdi
	xorl	%esi, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	208(%rax), %rax
	jmp	*%rax
	.cfi_endproc
.LFE3375:
	.size	_ZN6icu_6710DateFormat17adoptNumberFormatEPNS_12NumberFormatE, .-_ZN6icu_6710DateFormat17adoptNumberFormatEPNS_12NumberFormatE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710DateFormat15setNumberFormatERKNS_12NumberFormatE
	.type	_ZN6icu_6710DateFormat15setNumberFormatERKNS_12NumberFormatE, @function
_ZN6icu_6710DateFormat15setNumberFormatERKNS_12NumberFormatE:
.LFB3376:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	subq	$8, %rsp
	movq	(%rsi), %rax
	call	*32(%rax)
	testq	%rax, %rax
	je	.L22
	movq	%rax, %rsi
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	160(%rax), %rax
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L22:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3376:
	.size	_ZN6icu_6710DateFormat15setNumberFormatERKNS_12NumberFormatE, .-_ZN6icu_6710DateFormat15setNumberFormatERKNS_12NumberFormatE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710DateFormat15getNumberFormatEv
	.type	_ZNK6icu_6710DateFormat15getNumberFormatEv, @function
_ZNK6icu_6710DateFormat15getNumberFormatEv:
.LFB3377:
	.cfi_startproc
	endbr64
	movq	336(%rdi), %rax
	ret
	.cfi_endproc
.LFE3377:
	.size	_ZNK6icu_6710DateFormat15getNumberFormatEv, .-_ZNK6icu_6710DateFormat15getNumberFormatEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710DateFormat10setContextE15UDisplayContextR10UErrorCode
	.type	_ZN6icu_6710DateFormat10setContextE15UDisplayContextR10UErrorCode, @function
_ZN6icu_6710DateFormat10setContextE15UDisplayContextR10UErrorCode:
.LFB3385:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L26
	movl	%esi, %eax
	shrl	$8, %eax
	cmpl	$1, %eax
	je	.L29
	movl	$1, (%rdx)
.L26:
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	movl	%esi, 348(%rdi)
	ret
	.cfi_endproc
.LFE3385:
	.size	_ZN6icu_6710DateFormat10setContextE15UDisplayContextR10UErrorCode, .-_ZN6icu_6710DateFormat10setContextE15UDisplayContextR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710DateFormat10getContextE19UDisplayContextTypeR10UErrorCode
	.type	_ZNK6icu_6710DateFormat10getContextE19UDisplayContextTypeR10UErrorCode, @function
_ZNK6icu_6710DateFormat10getContextE19UDisplayContextTypeR10UErrorCode:
.LFB3386:
	.cfi_startproc
	endbr64
	movl	(%rdx), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L30
	cmpl	$1, %esi
	je	.L32
	movl	$1, (%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	movl	348(%rdi), %eax
.L30:
	ret
	.cfi_endproc
.LFE3386:
	.size	_ZNK6icu_6710DateFormat10getContextE19UDisplayContextTypeR10UErrorCode, .-_ZNK6icu_6710DateFormat10getContextE19UDisplayContextTypeR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710DateFormat19getBooleanAttributeE27UDateFormatBooleanAttributeR10UErrorCode
	.type	_ZNK6icu_6710DateFormat19getBooleanAttributeE27UDateFormatBooleanAttributeR10UErrorCode, @function
_ZNK6icu_6710DateFormat19getBooleanAttributeE27UDateFormatBooleanAttributeR10UErrorCode:
.LFB3388:
	.cfi_startproc
	endbr64
	movl	%esi, %ecx
	movl	$1, %eax
	sall	%cl, %eax
	testl	%eax, 344(%rdi)
	setne	%al
	ret
	.cfi_endproc
.LFE3388:
	.size	_ZNK6icu_6710DateFormat19getBooleanAttributeE27UDateFormatBooleanAttributeR10UErrorCode, .-_ZNK6icu_6710DateFormat19getBooleanAttributeE27UDateFormatBooleanAttributeR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718DateFmtBestPatternD2Ev
	.type	_ZN6icu_6718DateFmtBestPatternD2Ev, @function
_ZN6icu_6718DateFmtBestPatternD2Ev:
.LFB3319:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6718DateFmtBestPatternE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	24(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -24(%rdi)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6712SharedObjectD2Ev@PLT
	.cfi_endproc
.LFE3319:
	.size	_ZN6icu_6718DateFmtBestPatternD2Ev, .-_ZN6icu_6718DateFmtBestPatternD2Ev
	.globl	_ZN6icu_6718DateFmtBestPatternD1Ev
	.set	_ZN6icu_6718DateFmtBestPatternD1Ev,_ZN6icu_6718DateFmtBestPatternD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718DateFmtBestPatternD0Ev
	.type	_ZN6icu_6718DateFmtBestPatternD0Ev, @function
_ZN6icu_6718DateFmtBestPatternD0Ev:
.LFB3321:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6718DateFmtBestPatternE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	24(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -24(%rdi)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6712SharedObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3321:
	.size	_ZN6icu_6718DateFmtBestPatternD0Ev, .-_ZN6icu_6718DateFmtBestPatternD0Ev
	.section	.text._ZNK6icu_678CacheKeyINS_18DateFmtBestPatternEE16writeDescriptionEPci,"axG",@progbits,_ZNK6icu_678CacheKeyINS_18DateFmtBestPatternEE16writeDescriptionEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_678CacheKeyINS_18DateFmtBestPatternEE16writeDescriptionEPci
	.type	_ZNK6icu_678CacheKeyINS_18DateFmtBestPatternEE16writeDescriptionEPci, @function
_ZNK6icu_678CacheKeyINS_18DateFmtBestPatternEE16writeDescriptionEPci:
.LFB4521:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	leaq	_ZTSN6icu_6718DateFmtBestPatternE(%rip), %rsi
	movq	%r8, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movslq	%edx, %rbx
	movq	%rbx, %rdx
	subq	$8, %rsp
	call	strncpy@PLT
	movb	$0, -1(%rax,%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4521:
	.size	_ZNK6icu_678CacheKeyINS_18DateFmtBestPatternEE16writeDescriptionEPci, .-_ZNK6icu_678CacheKeyINS_18DateFmtBestPatternEE16writeDescriptionEPci
	.section	.text._ZNK6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEE16writeDescriptionEPci,"axG",@progbits,_ZNK6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEE16writeDescriptionEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEE16writeDescriptionEPci
	.type	_ZNK6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEE16writeDescriptionEPci, @function
_ZNK6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEE16writeDescriptionEPci:
.LFB4519:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movslq	%edx, %rbx
	movq	%rbx, %rdx
	subq	$8, %rsp
	movq	56(%rdi), %rsi
	movq	%r8, %rdi
	call	strncpy@PLT
	movb	$0, -1(%rax,%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4519:
	.size	_ZNK6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEE16writeDescriptionEPci, .-_ZNK6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEE16writeDescriptionEPci
	.section	.text._ZNK6icu_678CacheKeyINS_18DateFmtBestPatternEEeqERKNS_12CacheKeyBaseE,"axG",@progbits,_ZNK6icu_678CacheKeyINS_18DateFmtBestPatternEEeqERKNS_12CacheKeyBaseE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_678CacheKeyINS_18DateFmtBestPatternEEeqERKNS_12CacheKeyBaseE
	.type	_ZNK6icu_678CacheKeyINS_18DateFmtBestPatternEEeqERKNS_12CacheKeyBaseE, @function
_ZNK6icu_678CacheKeyINS_18DateFmtBestPatternEEeqERKNS_12CacheKeyBaseE:
.LFB4075:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	-8(%rax), %rax
	movq	8(%rax), %rdi
	movq	(%rsi), %rax
	movq	-8(%rax), %rax
	movq	8(%rax), %rsi
	movl	$1, %eax
	cmpq	%rsi, %rdi
	je	.L48
	xorl	%eax, %eax
	cmpb	$42, (%rdi)
	je	.L48
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	strcmp@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE4075:
	.size	_ZNK6icu_678CacheKeyINS_18DateFmtBestPatternEEeqERKNS_12CacheKeyBaseE, .-_ZNK6icu_678CacheKeyINS_18DateFmtBestPatternEEeqERKNS_12CacheKeyBaseE
	.section	.text._ZNK6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEE5cloneEv,"axG",@progbits,_ZNK6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEE5cloneEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEE5cloneEv
	.type	_ZNK6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEE5cloneEv, @function
_ZNK6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEE5cloneEv:
.LFB4520:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	$240, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L51
	movl	8(%rbx), %eax
	movb	$0, 12(%r12)
	leaq	16(%rbx), %rsi
	leaq	16(%r12), %rdi
	movl	%eax, 8(%r12)
	leaq	16+_ZTVN6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEEE(%rip), %rax
	movq	%rax, (%r12)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
.L51:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4520:
	.size	_ZNK6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEE5cloneEv, .-_ZNK6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEE5cloneEv
	.section	.text._ZNK6icu_678CacheKeyINS_18DateFmtBestPatternEE8hashCodeEv,"axG",@progbits,_ZNK6icu_678CacheKeyINS_18DateFmtBestPatternEE8hashCodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_678CacheKeyINS_18DateFmtBestPatternEE8hashCodeEv
	.type	_ZNK6icu_678CacheKeyINS_18DateFmtBestPatternEE8hashCodeEv, @function
_ZNK6icu_678CacheKeyINS_18DateFmtBestPatternEE8hashCodeEv:
.LFB4074:
	.cfi_startproc
	endbr64
	movl	$29, %esi
	leaq	_ZTSN6icu_6718DateFmtBestPatternE(%rip), %rdi
	jmp	ustr_hashCharsN_67@PLT
	.cfi_endproc
.LFE4074:
	.size	_ZNK6icu_678CacheKeyINS_18DateFmtBestPatternEE8hashCodeEv, .-_ZNK6icu_678CacheKeyINS_18DateFmtBestPatternEE8hashCodeEv
	.section	.text._ZNK6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEE8hashCodeEv,"axG",@progbits,_ZNK6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEE8hashCodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEE8hashCodeEv
	.type	_ZNK6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEE8hashCodeEv, @function
_ZNK6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEE8hashCodeEv:
.LFB3849:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$29, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	_ZTSN6icu_6718DateFmtBestPatternE(%rip), %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	call	ustr_hashCharsN_67@PLT
	leaq	16(%r12), %rdi
	movl	%eax, %ebx
	call	_ZNK6icu_676Locale8hashCodeEv@PLT
	movl	%eax, %r8d
	leal	(%rbx,%rbx,8), %eax
	leal	(%rbx,%rax,4), %eax
	popq	%rbx
	popq	%r12
	addl	%r8d, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3849:
	.size	_ZNK6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEE8hashCodeEv, .-_ZNK6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEE8hashCodeEv
	.section	.text._ZNK6icu_6721DateFmtBestPatternKey5cloneEv,"axG",@progbits,_ZNK6icu_6721DateFmtBestPatternKey5cloneEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6721DateFmtBestPatternKey5cloneEv
	.type	_ZNK6icu_6721DateFmtBestPatternKey5cloneEv, @function
_ZNK6icu_6721DateFmtBestPatternKey5cloneEv:
.LFB3331:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	$304, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L60
	movl	8(%rbx), %eax
	movb	$0, 12(%r12)
	leaq	16(%rbx), %rsi
	leaq	16(%r12), %rdi
	movl	%eax, 8(%r12)
	leaq	16+_ZTVN6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEEE(%rip), %rax
	movq	%rax, (%r12)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	leaq	16+_ZTVN6icu_6721DateFmtBestPatternKeyE(%rip), %rax
	leaq	240(%rbx), %rsi
	movq	%rax, (%r12)
	leaq	240(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
.L60:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3331:
	.size	_ZNK6icu_6721DateFmtBestPatternKey5cloneEv, .-_ZNK6icu_6721DateFmtBestPatternKey5cloneEv
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710DateFormat13adoptTimeZoneEPNS_8TimeZoneE
	.type	_ZN6icu_6710DateFormat13adoptTimeZoneEPNS_8TimeZoneE, @function
_ZN6icu_6710DateFormat13adoptTimeZoneEPNS_8TimeZoneE:
.LFB3378:
	.cfi_startproc
	endbr64
	movq	328(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L66
	jmp	_ZN6icu_678Calendar13adoptTimeZoneEPNS_8TimeZoneE@PLT
	.p2align 4,,10
	.p2align 3
.L66:
	ret
	.cfi_endproc
.LFE3378:
	.size	_ZN6icu_6710DateFormat13adoptTimeZoneEPNS_8TimeZoneE, .-_ZN6icu_6710DateFormat13adoptTimeZoneEPNS_8TimeZoneE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710DateFormat11setTimeZoneERKNS_8TimeZoneE
	.type	_ZN6icu_6710DateFormat11setTimeZoneERKNS_8TimeZoneE, @function
_ZN6icu_6710DateFormat11setTimeZoneERKNS_8TimeZoneE:
.LFB3379:
	.cfi_startproc
	endbr64
	movq	328(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L68
	jmp	_ZN6icu_678Calendar11setTimeZoneERKNS_8TimeZoneE@PLT
	.p2align 4,,10
	.p2align 3
.L68:
	ret
	.cfi_endproc
.LFE3379:
	.size	_ZN6icu_6710DateFormat11setTimeZoneERKNS_8TimeZoneE, .-_ZN6icu_6710DateFormat11setTimeZoneERKNS_8TimeZoneE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710DateFormat11getTimeZoneEv
	.type	_ZNK6icu_6710DateFormat11getTimeZoneEv, @function
_ZNK6icu_6710DateFormat11getTimeZoneEv:
.LFB3380:
	.cfi_startproc
	endbr64
	movq	328(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L71
	jmp	_ZNK6icu_678Calendar11getTimeZoneEv@PLT
	.p2align 4,,10
	.p2align 3
.L71:
	jmp	_ZN6icu_678TimeZone13createDefaultEv@PLT
	.cfi_endproc
.LFE3380:
	.size	_ZNK6icu_6710DateFormat11getTimeZoneEv, .-_ZNK6icu_6710DateFormat11getTimeZoneEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710DateFormat18setCalendarLenientEa
	.type	_ZN6icu_6710DateFormat18setCalendarLenientEa, @function
_ZN6icu_6710DateFormat18setCalendarLenientEa:
.LFB3383:
	.cfi_startproc
	endbr64
	movq	328(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L72
	movsbl	%sil, %esi
	jmp	_ZN6icu_678Calendar10setLenientEa@PLT
	.p2align 4,,10
	.p2align 3
.L72:
	ret
	.cfi_endproc
.LFE3383:
	.size	_ZN6icu_6710DateFormat18setCalendarLenientEa, .-_ZN6icu_6710DateFormat18setCalendarLenientEa
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710DateFormat17isCalendarLenientEv
	.type	_ZNK6icu_6710DateFormat17isCalendarLenientEv, @function
_ZNK6icu_6710DateFormat17isCalendarLenientEv:
.LFB3384:
	.cfi_startproc
	endbr64
	movq	328(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L75
	jmp	_ZNK6icu_678Calendar9isLenientEv@PLT
	.p2align 4,,10
	.p2align 3
.L75:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3384:
	.size	_ZNK6icu_6710DateFormat17isCalendarLenientEv, .-_ZNK6icu_6710DateFormat17isCalendarLenientEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710DateFormat19setBooleanAttributeE27UDateFormatBooleanAttributeaR10UErrorCode
	.type	_ZN6icu_6710DateFormat19setBooleanAttributeE27UDateFormatBooleanAttributeaR10UErrorCode, @function
_ZN6icu_6710DateFormat19setBooleanAttributeE27UDateFormatBooleanAttributeaR10UErrorCode:
.LFB3387:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	movsbl	%dl, %edi
	cmpl	$1, %edi
	jbe	.L77
	movl	$1, (%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L77:
	movl	%esi, %ecx
	movl	$1, %r8d
	movl	344(%rax), %edi
	sall	%cl, %r8d
	movl	%r8d, %ecx
	notl	%ecx
	andl	%edi, %ecx
	orl	%r8d, %edi
	testb	%dl, %dl
	cmovne	%edi, %ecx
	movl	%ecx, 344(%rax)
	ret
	.cfi_endproc
.LFE3387:
	.size	_ZN6icu_6710DateFormat19setBooleanAttributeE27UDateFormatBooleanAttributeaR10UErrorCode, .-_ZN6icu_6710DateFormat19setBooleanAttributeE27UDateFormatBooleanAttributeaR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710DateFormat9isLenientEv
	.type	_ZNK6icu_6710DateFormat9isLenientEv, @function
_ZNK6icu_6710DateFormat9isLenientEv:
.LFB3382:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -32
	movq	328(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L94
	call	_ZNK6icu_678Calendar9isLenientEv@PLT
	movl	$0, -28(%rbp)
	testb	%al, %al
	je	.L86
.L82:
	movq	(%r12), %rax
	leaq	_ZNK6icu_6710DateFormat19getBooleanAttributeE27UDateFormatBooleanAttributeR10UErrorCode(%rip), %rbx
	movq	224(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L84
	movzbl	344(%r12), %eax
	andl	$1, %eax
.L85:
	testb	%al, %al
	jne	.L95
.L86:
	xorl	%eax, %eax
.L80:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L96
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L95:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	224(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L87
	movl	344(%r12), %eax
	shrl	%eax
	andl	$1, %eax
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L94:
	movl	$0, -28(%rbp)
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L84:
	leaq	-28(%rbp), %rdx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	*%rax
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L87:
	leaq	-28(%rbp), %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	call	*%rax
	testb	%al, %al
	setne	%al
	jmp	.L80
.L96:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3382:
	.size	_ZNK6icu_6710DateFormat9isLenientEv, .-_ZNK6icu_6710DateFormat9isLenientEv
	.section	.text._ZNK6icu_6721DateFmtBestPatternKey8hashCodeEv,"axG",@progbits,_ZNK6icu_6721DateFmtBestPatternKey8hashCodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6721DateFmtBestPatternKey8hashCodeEv
	.type	_ZNK6icu_6721DateFmtBestPatternKey8hashCodeEv, @function
_ZNK6icu_6721DateFmtBestPatternKey8hashCodeEv:
.LFB3329:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$29, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	leaq	_ZTSN6icu_6718DateFmtBestPatternE(%rip), %rdi
	subq	$8, %rsp
	call	ustr_hashCharsN_67@PLT
	leaq	16(%rbx), %rdi
	movl	%eax, %r12d
	call	_ZNK6icu_676Locale8hashCodeEv@PLT
	leaq	240(%rbx), %rdi
	movl	%eax, %r13d
	call	_ZNK6icu_6713UnicodeString10doHashCodeEv@PLT
	addq	$8, %rsp
	movl	%eax, %r8d
	leal	(%r12,%r12,8), %eax
	popq	%rbx
	leal	(%r12,%rax,4), %eax
	popq	%r12
	addl	%r13d, %eax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	leal	(%rax,%rax,8), %edx
	leal	(%rax,%rdx,4), %eax
	addl	%r8d, %eax
	ret
	.cfi_endproc
.LFE3329:
	.size	_ZNK6icu_6721DateFmtBestPatternKey8hashCodeEv, .-_ZNK6icu_6721DateFmtBestPatternKey8hashCodeEv
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710DateFormat10setLenientEa
	.type	_ZN6icu_6710DateFormat10setLenientEa, @function
_ZN6icu_6710DateFormat10setLenientEa:
.LFB3381:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movsbl	%sil, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$24, %rsp
	movq	328(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L100
	movl	%r13d, %esi
	call	_ZN6icu_678Calendar10setLenientEa@PLT
.L100:
	movq	(%r12), %rax
	leaq	_ZN6icu_6710DateFormat19setBooleanAttributeE27UDateFormatBooleanAttributeaR10UErrorCode(%rip), %r14
	movl	$0, -60(%rbp)
	movq	216(%rax), %rax
	cmpq	%r14, %rax
	jne	.L101
	cmpl	$1, %r13d
	jbe	.L102
	movl	$1, -60(%rbp)
.L99:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L113
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L102:
	.cfi_restore_state
	movl	344(%r12), %eax
	xorl	%edx, %edx
	andl	$-2, %eax
	testb	%bl, %bl
	setne	%dl
	orl	%edx, %eax
	movl	%eax, 344(%r12)
.L105:
	movl	%eax, %edx
	orl	$2, %eax
	andl	$-3, %edx
	testb	%bl, %bl
	cmovne	%eax, %edx
	movl	%edx, 344(%r12)
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L101:
	leaq	-60(%rbp), %r15
	movl	%r13d, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%r15, %rcx
	call	*%rax
	movq	(%r12), %rax
	movq	216(%rax), %rax
	cmpq	%r14, %rax
	jne	.L103
	cmpl	$1, %r13d
	ja	.L99
	movl	344(%r12), %eax
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L103:
	movq	%r15, %rcx
	movl	%r13d, %edx
	movl	$1, %esi
	movq	%r12, %rdi
	call	*%rax
	jmp	.L99
.L113:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3381:
	.size	_ZN6icu_6710DateFormat10setLenientEa, .-_ZN6icu_6710DateFormat10setLenientEa
	.section	.text._ZNK6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEEeqERKNS_12CacheKeyBaseE,"axG",@progbits,_ZNK6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEEeqERKNS_12CacheKeyBaseE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEEeqERKNS_12CacheKeyBaseE
	.type	_ZNK6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEEeqERKNS_12CacheKeyBaseE, @function
_ZNK6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEEeqERKNS_12CacheKeyBaseE:
.LFB3850:
	.cfi_startproc
	endbr64
	cmpq	%rsi, %rdi
	je	.L117
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	movq	-8(%rax), %rax
	movq	8(%rax), %rdi
	movq	(%rsi), %rax
	movq	-8(%rax), %rax
	movq	8(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L116
	cmpb	$42, (%rdi)
	je	.L119
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L119
.L116:
	leaq	16(%r12), %rsi
	leaq	16(%rbx), %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_676LocaleeqERKS0_@PLT
	.p2align 4,,10
	.p2align 3
.L119:
	.cfi_restore_state
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L117:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3850:
	.size	_ZNK6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEEeqERKNS_12CacheKeyBaseE, .-_ZNK6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEEeqERKNS_12CacheKeyBaseE
	.section	.text._ZNK6icu_6721DateFmtBestPatternKeyeqERKNS_12CacheKeyBaseE,"axG",@progbits,_ZNK6icu_6721DateFmtBestPatternKeyeqERKNS_12CacheKeyBaseE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6721DateFmtBestPatternKeyeqERKNS_12CacheKeyBaseE
	.type	_ZNK6icu_6721DateFmtBestPatternKeyeqERKNS_12CacheKeyBaseE, @function
_ZNK6icu_6721DateFmtBestPatternKeyeqERKNS_12CacheKeyBaseE:
.LFB3330:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	cmpq	%rsi, %rdi
	je	.L145
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	movq	%rsi, %rbx
	movq	-8(%rax), %rax
	movq	8(%rax), %rdi
	movq	(%rsi), %rax
	movq	-8(%rax), %rax
	movq	8(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L126
	cmpb	$42, (%rdi)
	je	.L128
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L128
.L126:
	leaq	16(%rbx), %rsi
	leaq	16(%r12), %rdi
	call	_ZNK6icu_676LocaleeqERKS0_@PLT
	testb	%al, %al
	je	.L128
	movswl	248(%r12), %ecx
	movswl	248(%rbx), %edx
	movl	%ecx, %eax
	movl	%edx, %esi
	andl	$1, %eax
	andl	$1, %esi
	jne	.L124
	testw	%dx, %dx
	js	.L129
	sarl	$5, %edx
.L130:
	testw	%cx, %cx
	js	.L131
	sarl	$5, %ecx
.L132:
	testb	$1, %al
	jne	.L133
	cmpl	%edx, %ecx
	je	.L148
.L133:
	movl	%esi, %eax
.L124:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L128:
	.cfi_restore_state
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L145:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.p2align 4,,10
	.p2align 3
.L131:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movl	252(%r12), %ecx
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L129:
	movl	252(%rbx), %edx
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L148:
	leaq	240(%r12), %rsi
	leaq	240(%rbx), %rdi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%sil
	movl	%esi, %eax
	jmp	.L124
	.cfi_endproc
.LFE3330:
	.size	_ZNK6icu_6721DateFmtBestPatternKeyeqERKNS_12CacheKeyBaseE, .-_ZNK6icu_6721DateFmtBestPatternKeyeqERKNS_12CacheKeyBaseE
	.section	.text._ZNK6icu_6721DateFmtBestPatternKey12createObjectEPKvR10UErrorCode,"axG",@progbits,_ZNK6icu_6721DateFmtBestPatternKey12createObjectEPKvR10UErrorCode,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6721DateFmtBestPatternKey12createObjectEPKvR10UErrorCode
	.type	_ZNK6icu_6721DateFmtBestPatternKey12createObjectEPKvR10UErrorCode, @function
_ZNK6icu_6721DateFmtBestPatternKey12createObjectEPKvR10UErrorCode:
.LFB3332:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	addq	$16, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6724DateTimePatternGenerator14createInstanceERKNS_6LocaleER10UErrorCode@PLT
	movl	(%rbx), %esi
	movq	%rax, %r13
	testl	%esi, %esi
	jg	.L167
	leaq	-128(%rbp), %r14
	leaq	240(%r12), %rdx
	movq	%rbx, %rcx
	movq	%rax, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6724DateTimePatternGenerator14getBestPatternERKNS_13UnicodeStringER10UErrorCode@PLT
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L152
	movq	$0, 8(%rax)
	leaq	24(%r12), %r15
	movq	%r14, %rsi
	movq	$0, 16(%rax)
	leaq	16+_ZTVN6icu_6718DateFmtBestPatternE(%rip), %rax
	movq	%r15, %rdi
	movq	%rax, (%r12)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	(%rbx), %ecx
	leaq	16+_ZTVN6icu_6718DateFmtBestPatternE(%rip), %rax
	testl	%ecx, %ecx
	jle	.L154
	movq	(%r12), %rdx
	leaq	_ZN6icu_6718DateFmtBestPatternD0Ev(%rip), %rcx
	movq	8(%rdx), %rdx
	cmpq	%rcx, %rdx
	jne	.L168
	movq	%rax, (%r12)
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6712SharedObjectD2Ev@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	_ZN6icu_677UMemorydlEPv@PLT
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L168:
	movq	%r12, %rdi
	call	*%rdx
	.p2align 4,,10
	.p2align 3
.L167:
	xorl	%r12d, %r12d
.L151:
	testq	%r13, %r13
	je	.L149
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*8(%rax)
.L149:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L169
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L152:
	.cfi_restore_state
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L155
	movl	$7, (%rbx)
.L155:
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L167
.L154:
	movq	%r12, %rdi
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
	jmp	.L151
.L169:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3332:
	.size	_ZNK6icu_6721DateFmtBestPatternKey12createObjectEPKvR10UErrorCode, .-_ZNK6icu_6721DateFmtBestPatternKey12createObjectEPKvR10UErrorCode
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710DateFormat6formatERKNS_11FormattableERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode
	.type	_ZNK6icu_6710DateFormat6formatERKNS_11FormattableERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode, @function
_ZNK6icu_6710DateFormat6formatERKNS_11FormattableERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode:
.LFB3352:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	(%r8), %edx
	testl	%edx, %edx
	jle	.L187
.L172:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L187:
	.cfi_restore_state
	movq	%rdi, %r13
	movq	%rsi, %rdi
	movq	%rsi, %r15
	movq	%rcx, %r14
	movq	%r8, %rbx
	call	_ZNK6icu_6711Formattable7getTypeEv@PLT
	cmpl	$1, %eax
	ja	.L188
	movsd	8(%r15), %xmm0
.L176:
	movq	328(%r13), %rdi
	movsd	%xmm0, -56(%rbp)
	testq	%rdi, %rdi
	je	.L172
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L172
	movsd	-56(%rbp), %xmm0
	movq	%rax, %rdi
	movq	%rbx, %rsi
	call	_ZN6icu_678Calendar15setTimeInMillisEdR10UErrorCode@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L178
	movq	0(%r13), %rax
	movq	%rbx, %r8
	movq	%r14, %rcx
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	*72(%rax)
.L178:
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*8(%rax)
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L188:
	cmpl	$2, %eax
	jne	.L189
	pxor	%xmm0, %xmm0
	cvtsi2sdl	8(%r15), %xmm0
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L189:
	movl	$1, (%rbx)
	jmp	.L172
	.cfi_endproc
.LFE3352:
	.size	_ZNK6icu_6710DateFormat6formatERKNS_11FormattableERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode, .-_ZNK6icu_6710DateFormat6formatERKNS_11FormattableERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710DateFormat11parseObjectERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE
	.type	_ZNK6icu_6710DateFormat11parseObjectERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE, @function
_ZNK6icu_6710DateFormat11parseObjectERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE:
.LFB3359:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	328(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L193
	movq	(%rdi), %rax
	movq	%rsi, %r14
	movq	%rcx, %rbx
	call	*24(%rax)
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L193
	movl	8(%rbx), %eax
	movq	%r15, %rdi
	movl	%eax, -72(%rbp)
	call	_ZN6icu_678Calendar5clearEv@PLT
	movq	(%r12), %rax
	movq	%r14, %rsi
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	*88(%rax)
	movl	-72(%rbp), %r14d
	pxor	%xmm0, %xmm0
	cmpl	8(%rbx), %r14d
	jne	.L202
.L194:
	movq	(%r15), %rax
	movsd	%xmm0, -72(%rbp)
	movq	%r15, %rdi
	call	*8(%rax)
	movsd	-72(%rbp), %xmm0
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L193:
	pxor	%xmm0, %xmm0
.L192:
	movq	%r13, %rdi
	call	_ZN6icu_6711Formattable7setDateEd@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L203
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L202:
	.cfi_restore_state
	leaq	-60(%rbp), %rsi
	movq	%r15, %rdi
	movl	$0, -60(%rbp)
	call	_ZNK6icu_678Calendar15getTimeInMillisER10UErrorCode@PLT
	movl	-60(%rbp), %eax
	testl	%eax, %eax
	jle	.L194
	movl	%r14d, 8(%rbx)
	pxor	%xmm0, %xmm0
	movl	%r14d, 12(%rbx)
	jmp	.L194
.L203:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3359:
	.size	_ZNK6icu_6710DateFormat11parseObjectERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE, .-_ZNK6icu_6710DateFormat11parseObjectERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710DateFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.type	_ZNK6icu_6710DateFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode, @function
_ZNK6icu_6710DateFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode:
.LFB3351:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movl	(%r8), %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	jle	.L222
.L206:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L223
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L222:
	.cfi_restore_state
	movq	%rdi, %r13
	movq	%rsi, %rdi
	movq	%rsi, %r15
	movq	%rcx, %r14
	movq	%r8, %rbx
	call	_ZNK6icu_6711Formattable7getTypeEv@PLT
	cmpl	$1, %eax
	ja	.L224
	movsd	8(%r15), %xmm0
.L210:
	movq	328(%r13), %rdi
	movsd	%xmm0, -72(%rbp)
	testq	%rdi, %rdi
	je	.L206
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L206
	movsd	-72(%rbp), %xmm0
	movq	%rax, %rdi
	leaq	-60(%rbp), %rsi
	movl	$0, -60(%rbp)
	call	_ZN6icu_678Calendar15setTimeInMillisEdR10UErrorCode@PLT
	movl	-60(%rbp), %eax
	testl	%eax, %eax
	jg	.L212
	movq	0(%r13), %rax
	movq	%r14, %rcx
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	*64(%rax)
.L212:
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*8(%rax)
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L224:
	cmpl	$2, %eax
	jne	.L225
	pxor	%xmm0, %xmm0
	cvtsi2sdl	8(%r15), %xmm0
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L225:
	movl	$1, (%rbx)
	jmp	.L206
.L223:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3351:
	.size	_ZNK6icu_6710DateFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode, .-_ZNK6icu_6710DateFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710DateFormat5parseERKNS_13UnicodeStringER10UErrorCode
	.type	_ZNK6icu_6710DateFormat5parseERKNS_13UnicodeStringER10UErrorCode, @function
_ZNK6icu_6710DateFormat5parseERKNS_13UnicodeStringER10UErrorCode:
.LFB3358:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rdx), %ecx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jle	.L239
.L226:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L240
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L239:
	.cfi_restore_state
	leaq	16+_ZTVN6icu_6713ParsePositionE(%rip), %rax
	movq	%rdi, %r13
	movq	%rdx, %rbx
	movq	328(%rdi), %rdi
	movq	%rax, -80(%rbp)
	movabsq	$-4294967296, %rax
	movq	%rax, -72(%rbp)
	testq	%rdi, %rdi
	je	.L235
	movq	(%rdi), %rax
	movsd	%xmm0, -104(%rbp)
	movq	%rsi, %r12
	leaq	-80(%rbp), %r15
	call	*24(%rax)
	movsd	-104(%rbp), %xmm0
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L229
	movl	-72(%rbp), %eax
	movq	%r14, %rdi
	movsd	%xmm0, -112(%rbp)
	movl	%eax, -104(%rbp)
	call	_ZN6icu_678Calendar5clearEv@PLT
	movq	0(%r13), %rax
	movq	%r12, %rsi
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	*88(%rax)
	movl	-104(%rbp), %r12d
	cmpl	-72(%rbp), %r12d
	movsd	-112(%rbp), %xmm0
	je	.L230
	leaq	-84(%rbp), %rsi
	movq	%r14, %rdi
	movl	$0, -84(%rbp)
	call	_ZNK6icu_678Calendar15getTimeInMillisER10UErrorCode@PLT
	movl	-84(%rbp), %edx
	testl	%edx, %edx
	jle	.L230
	movl	%r12d, -72(%rbp)
	pxor	%xmm0, %xmm0
	movl	%r12d, -68(%rbp)
.L230:
	movq	(%r14), %rax
	movsd	%xmm0, -104(%rbp)
	movq	%r14, %rdi
	call	*8(%rax)
	movsd	-104(%rbp), %xmm0
.L229:
	movl	-72(%rbp), %eax
	testl	%eax, %eax
	je	.L228
.L232:
	movq	%r15, %rdi
	movsd	%xmm0, -104(%rbp)
	call	_ZN6icu_6713ParsePositionD1Ev@PLT
	movsd	-104(%rbp), %xmm0
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L235:
	leaq	-80(%rbp), %r15
.L228:
	movl	$1, (%rbx)
	jmp	.L232
.L240:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3358:
	.size	_ZNK6icu_6710DateFormat5parseERKNS_13UnicodeStringER10UErrorCode, .-_ZNK6icu_6710DateFormat5parseERKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710DateFormatC2Ev
	.type	_ZN6icu_6710DateFormatC2Ev, @function
_ZN6icu_6710DateFormatC2Ev:
.LFB3340:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_676FormatC2Ev@PLT
	leaq	16+_ZTVN6icu_6710DateFormatE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%rax, (%rbx)
	movabsq	$1099511627776, %rax
	movq	%rax, 344(%rbx)
	movups	%xmm0, 328(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3340:
	.size	_ZN6icu_6710DateFormatC2Ev, .-_ZN6icu_6710DateFormatC2Ev
	.globl	_ZN6icu_6710DateFormatC1Ev
	.set	_ZN6icu_6710DateFormatC1Ev,_ZN6icu_6710DateFormatC2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710DateFormatC2ERKS0_
	.type	_ZN6icu_6710DateFormatC2ERKS0_, @function
_ZN6icu_6710DateFormatC2ERKS0_:
.LFB3343:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZN6icu_676FormatC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6710DateFormatE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%rax, (%rbx)
	movabsq	$1099511627776, %rax
	movq	%rax, 344(%rbx)
	movups	%xmm0, 328(%rbx)
	cmpq	%r12, %rbx
	je	.L243
	movq	328(%r12), %rdi
	testq	%rdi, %rdi
	je	.L245
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	%rax, 328(%rbx)
.L245:
	movq	336(%r12), %rdi
	testq	%rdi, %rdi
	je	.L246
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%rax, 336(%rbx)
.L247:
	movq	344(%r12), %rax
	movq	%rax, 344(%rbx)
.L243:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L246:
	.cfi_restore_state
	movq	$0, 336(%rbx)
	jmp	.L247
	.cfi_endproc
.LFE3343:
	.size	_ZN6icu_6710DateFormatC2ERKS0_, .-_ZN6icu_6710DateFormatC2ERKS0_
	.globl	_ZN6icu_6710DateFormatC1ERKS0_
	.set	_ZN6icu_6710DateFormatC1ERKS0_,_ZN6icu_6710DateFormatC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710DateFormataSERKS0_
	.type	_ZN6icu_6710DateFormataSERKS0_, @function
_ZN6icu_6710DateFormataSERKS0_:
.LFB3345:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpq	%rsi, %rdi
	je	.L253
	movq	328(%rdi), %rdi
	movq	%rsi, %rbx
	testq	%rdi, %rdi
	je	.L254
	movq	(%rdi), %rax
	call	*8(%rax)
.L254:
	movq	336(%r12), %rdi
	testq	%rdi, %rdi
	je	.L255
	movq	(%rdi), %rax
	call	*8(%rax)
.L255:
	movq	328(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L256
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	336(%rbx), %rdi
	movq	%rax, 328(%r12)
	testq	%rdi, %rdi
	je	.L258
.L267:
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%rax, 336(%r12)
.L259:
	movl	344(%rbx), %eax
	movl	%eax, 344(%r12)
	movl	348(%rbx), %eax
	movl	%eax, 348(%r12)
.L253:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L256:
	.cfi_restore_state
	movq	336(%rbx), %rdi
	movq	$0, 328(%r12)
	testq	%rdi, %rdi
	jne	.L267
.L258:
	movq	$0, 336(%r12)
	jmp	.L259
	.cfi_endproc
.LFE3345:
	.size	_ZN6icu_6710DateFormataSERKS0_, .-_ZN6icu_6710DateFormataSERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710DateFormatD2Ev
	.type	_ZN6icu_6710DateFormatD2Ev, @function
_ZN6icu_6710DateFormatD2Ev:
.LFB3347:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6710DateFormatE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	328(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L269
	movq	(%rdi), %rax
	call	*8(%rax)
.L269:
	movq	336(%r12), %rdi
	testq	%rdi, %rdi
	je	.L270
	movq	(%rdi), %rax
	call	*8(%rax)
.L270:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676FormatD2Ev@PLT
	.cfi_endproc
.LFE3347:
	.size	_ZN6icu_6710DateFormatD2Ev, .-_ZN6icu_6710DateFormatD2Ev
	.globl	_ZN6icu_6710DateFormatD1Ev
	.set	_ZN6icu_6710DateFormatD1Ev,_ZN6icu_6710DateFormatD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710DateFormatD0Ev
	.type	_ZN6icu_6710DateFormatD0Ev, @function
_ZN6icu_6710DateFormatD0Ev:
.LFB3349:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6710DateFormatE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	328(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L279
	movq	(%rdi), %rax
	call	*8(%rax)
.L279:
	movq	336(%r12), %rdi
	testq	%rdi, %rdi
	je	.L280
	movq	(%rdi), %rax
	call	*8(%rax)
.L280:
	movq	%r12, %rdi
	call	_ZN6icu_676FormatD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3349:
	.size	_ZN6icu_6710DateFormatD0Ev, .-_ZN6icu_6710DateFormatD0Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710DateFormat6formatEdRNS_13UnicodeStringERNS_13FieldPositionE
	.type	_ZNK6icu_6710DateFormat6formatEdRNS_13UnicodeStringERNS_13FieldPositionE, @function
_ZNK6icu_6710DateFormat6formatEdRNS_13UnicodeStringERNS_13FieldPositionE:
.LFB3354:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$32, %rsp
	movq	328(%rdi), %rdi
	movsd	%xmm0, -56(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L290
	movq	(%rdi), %rax
	movq	%rdx, %r15
	call	*24(%rax)
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L290
	movsd	-56(%rbp), %xmm0
	movq	%rax, %rdi
	leaq	-44(%rbp), %rsi
	movl	$0, -44(%rbp)
	call	_ZN6icu_678Calendar15setTimeInMillisEdR10UErrorCode@PLT
	movl	-44(%rbp), %eax
	testl	%eax, %eax
	jg	.L292
	movq	(%r12), %rax
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	*64(%rax)
.L292:
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*8(%rax)
.L290:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L298
	addq	$32, %rsp
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L298:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3354:
	.size	_ZNK6icu_6710DateFormat6formatEdRNS_13UnicodeStringERNS_13FieldPositionE, .-_ZNK6icu_6710DateFormat6formatEdRNS_13UnicodeStringERNS_13FieldPositionE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710DateFormat6formatEdRNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode
	.type	_ZNK6icu_6710DateFormat6formatEdRNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode, @function
_ZNK6icu_6710DateFormat6formatEdRNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode:
.LFB3355:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	328(%rdi), %rdi
	movsd	%xmm0, -56(%rbp)
	testq	%rdi, %rdi
	je	.L301
	movq	(%rdi), %rax
	movq	%rdx, %r15
	movq	%rcx, %rbx
	call	*24(%rax)
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L301
	movsd	-56(%rbp), %xmm0
	movq	%rax, %rdi
	movq	%rbx, %rsi
	call	_ZN6icu_678Calendar15setTimeInMillisEdR10UErrorCode@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L303
	movq	(%r12), %rax
	movq	%rbx, %r8
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	*72(%rax)
.L303:
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*8(%rax)
.L301:
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3355:
	.size	_ZNK6icu_6710DateFormat6formatEdRNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode, .-_ZNK6icu_6710DateFormat6formatEdRNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710DateFormat6formatEdRNS_13UnicodeStringE
	.type	_ZNK6icu_6710DateFormat6formatEdRNS_13UnicodeStringE, @function
_ZNK6icu_6710DateFormat6formatEdRNS_13UnicodeStringE:
.LFB3356:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-64(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$64, %rsp
	movq	328(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6713FieldPositionE(%rip), %rax
	movl	$0, -48(%rbp)
	movq	%rax, -64(%rbp)
	movl	$4294967295, %eax
	movq	%rax, -56(%rbp)
	testq	%rdi, %rdi
	je	.L310
	movq	(%rdi), %rax
	movsd	%xmm0, -88(%rbp)
	leaq	-64(%rbp), %r15
	call	*24(%rax)
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L310
	movsd	-88(%rbp), %xmm0
	movq	%rax, %rdi
	leaq	-68(%rbp), %rsi
	movl	$0, -68(%rbp)
	call	_ZN6icu_678Calendar15setTimeInMillisEdR10UErrorCode@PLT
	movl	-68(%rbp), %eax
	testl	%eax, %eax
	jg	.L312
	movq	(%r12), %rax
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	*64(%rax)
.L312:
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*8(%rax)
.L310:
	movq	%r15, %rdi
	call	_ZN6icu_6713FieldPositionD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L319
	addq	$64, %rsp
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L319:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3356:
	.size	_ZNK6icu_6710DateFormat6formatEdRNS_13UnicodeStringE, .-_ZNK6icu_6710DateFormat6formatEdRNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710DateFormat5parseERKNS_13UnicodeStringERNS_13ParsePositionE
	.type	_ZNK6icu_6710DateFormat5parseERKNS_13UnicodeStringERNS_13ParsePositionE, @function
_ZNK6icu_6710DateFormat5parseERKNS_13UnicodeStringERNS_13ParsePositionE:
.LFB3357:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	328(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L323
	movq	(%rdi), %rax
	movq	%rsi, %r14
	movq	%rdx, %rbx
	call	*24(%rax)
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L323
	movq	%rax, %rdi
	movl	8(%rbx), %r15d
	call	_ZN6icu_678Calendar5clearEv@PLT
	movq	(%r12), %rax
	movq	%rbx, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	*88(%rax)
	pxor	%xmm0, %xmm0
	cmpl	8(%rbx), %r15d
	jne	.L332
.L324:
	movq	0(%r13), %rax
	movsd	%xmm0, -72(%rbp)
	movq	%r13, %rdi
	call	*8(%rax)
	movsd	-72(%rbp), %xmm0
	jmp	.L320
	.p2align 4,,10
	.p2align 3
.L323:
	pxor	%xmm0, %xmm0
.L320:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L333
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L332:
	.cfi_restore_state
	leaq	-60(%rbp), %rsi
	movq	%r13, %rdi
	movl	$0, -60(%rbp)
	call	_ZNK6icu_678Calendar15getTimeInMillisER10UErrorCode@PLT
	movl	-60(%rbp), %eax
	testl	%eax, %eax
	jle	.L324
	movl	%r15d, 8(%rbx)
	pxor	%xmm0, %xmm0
	movl	%r15d, 12(%rbx)
	jmp	.L324
.L333:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3357:
	.size	_ZNK6icu_6710DateFormat5parseERKNS_13UnicodeStringERNS_13ParsePositionE, .-_ZNK6icu_6710DateFormat5parseERKNS_13UnicodeStringERNS_13ParsePositionE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710DateFormat18createTimeInstanceENS0_6EStyleERKNS_6LocaleE
	.type	_ZN6icu_6710DateFormat18createTimeInstanceENS0_6EStyleERKNS_6LocaleE, @function
_ZN6icu_6710DateFormat18createTimeInstanceENS0_6EStyleERKNS_6LocaleE:
.LFB3360:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%edi, %r14d
	movl	$864, %edi
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 12, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$0, -44(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L335
	movl	$-1, %edx
	leaq	-44(%rbp), %r8
	movq	%r13, %rcx
	movl	%r14d, %esi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN6icu_6716SimpleDateFormatC1ENS_10DateFormat6EStyleES2_RKNS_6LocaleER10UErrorCode@PLT
	movl	-44(%rbp), %edx
	testl	%edx, %edx
	jle	.L334
	movq	%r12, %rdi
	call	_ZN6icu_6716SimpleDateFormatD0Ev@PLT
.L339:
	movl	$864, %edi
	movl	$0, -44(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L342
	movq	%rax, %rdi
	leaq	-44(%rbp), %rdx
	movq	%r13, %rsi
	call	_ZN6icu_6716SimpleDateFormatC1ERKNS_6LocaleER10UErrorCode@PLT
	movl	-44(%rbp), %eax
	testl	%eax, %eax
	jle	.L334
	movq	%r12, %rdi
	call	_ZN6icu_6716SimpleDateFormatD0Ev@PLT
.L342:
	xorl	%r12d, %r12d
.L334:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L343
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L335:
	.cfi_restore_state
	cmpl	$0, -44(%rbp)
	jg	.L339
	jmp	.L342
.L343:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3360:
	.size	_ZN6icu_6710DateFormat18createTimeInstanceENS0_6EStyleERKNS_6LocaleE, .-_ZN6icu_6710DateFormat18createTimeInstanceENS0_6EStyleERKNS_6LocaleE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710DateFormat14createInstanceEv
	.type	_ZN6icu_6710DateFormat14createInstanceEv, @function
_ZN6icu_6710DateFormat14createInstanceEv:
.LFB3363:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_676Locale10getDefaultEv@PLT
	movl	$864, %edi
	movl	$0, -28(%rbp)
	movq	%rax, %r13
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L345
	movl	$7, %edx
	leaq	-28(%rbp), %r8
	movq	%r13, %rcx
	movq	%rax, %rdi
	movl	$3, %esi
	movq	%rax, %r12
	call	_ZN6icu_6716SimpleDateFormatC1ENS_10DateFormat6EStyleES2_RKNS_6LocaleER10UErrorCode@PLT
	movl	-28(%rbp), %edx
	testl	%edx, %edx
	jle	.L344
	movq	%r12, %rdi
	call	_ZN6icu_6716SimpleDateFormatD0Ev@PLT
.L349:
	movl	$864, %edi
	movl	$0, -28(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L352
	movq	%rax, %rdi
	leaq	-28(%rbp), %rdx
	movq	%r13, %rsi
	call	_ZN6icu_6716SimpleDateFormatC1ERKNS_6LocaleER10UErrorCode@PLT
	movl	-28(%rbp), %eax
	testl	%eax, %eax
	jle	.L344
	movq	%r12, %rdi
	call	_ZN6icu_6716SimpleDateFormatD0Ev@PLT
.L352:
	xorl	%r12d, %r12d
.L344:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L353
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L345:
	.cfi_restore_state
	cmpl	$0, -28(%rbp)
	jg	.L349
	jmp	.L352
.L353:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3363:
	.size	_ZN6icu_6710DateFormat14createInstanceEv, .-_ZN6icu_6710DateFormat14createInstanceEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710DateFormat6createENS0_6EStyleES1_RKNS_6LocaleE
	.type	_ZN6icu_6710DateFormat6createENS0_6EStyleES1_RKNS_6LocaleE, @function
_ZN6icu_6710DateFormat6createENS0_6EStyleES1_RKNS_6LocaleE:
.LFB3370:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%esi, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edi, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -60(%rbp)
	cmpl	$-1, %esi
	je	.L355
	leal	-4(%rsi), %ebx
	testb	$-128, %bl
	jne	.L372
.L355:
	movl	$864, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L358
	movl	%r15d, %edx
	leaq	-60(%rbp), %r8
	movq	%r13, %rcx
	movl	%r14d, %esi
	movq	%rax, %rdi
	call	_ZN6icu_6716SimpleDateFormatC1ENS_10DateFormat6EStyleES2_RKNS_6LocaleER10UErrorCode@PLT
	movl	-60(%rbp), %edx
	testl	%edx, %edx
	jle	.L354
	movq	%r12, %rdi
	call	_ZN6icu_6716SimpleDateFormatD0Ev@PLT
.L361:
	movl	$864, %edi
	movl	$0, -60(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L371
	movq	%rax, %rdi
	leaq	-60(%rbp), %rdx
	movq	%r13, %rsi
	call	_ZN6icu_6716SimpleDateFormatC1ERKNS_6LocaleER10UErrorCode@PLT
	movl	-60(%rbp), %eax
	testl	%eax, %eax
	jle	.L354
	movq	%r12, %rdi
	call	_ZN6icu_6716SimpleDateFormatD0Ev@PLT
.L371:
	xorl	%r12d, %r12d
.L354:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L373
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L372:
	.cfi_restore_state
	movl	$760, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L356
	movq	%r13, %rcx
	leaq	-60(%rbp), %r8
	movl	%ebx, %edx
	movl	%r14d, %esi
	movq	%rax, %rdi
	call	_ZN6icu_6718RelativeDateFormatC1E16UDateFormatStyleS1_RKNS_6LocaleER10UErrorCode@PLT
	movl	-60(%rbp), %ecx
	testl	%ecx, %ecx
	jle	.L354
	movq	%r12, %rdi
	call	_ZN6icu_6718RelativeDateFormatD0Ev@PLT
.L362:
	movl	$0, -60(%rbp)
	jmp	.L355
.L358:
	cmpl	$0, -60(%rbp)
	jg	.L361
	jmp	.L371
.L373:
	call	__stack_chk_fail@PLT
.L356:
	cmpl	$0, -60(%rbp)
	jg	.L362
	jmp	.L371
	.cfi_endproc
.LFE3370:
	.size	_ZN6icu_6710DateFormat6createENS0_6EStyleES1_RKNS_6LocaleE, .-_ZN6icu_6710DateFormat6createENS0_6EStyleES1_RKNS_6LocaleE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710DateFormat22createDateTimeInstanceENS0_6EStyleES1_RKNS_6LocaleE
	.type	_ZN6icu_6710DateFormat22createDateTimeInstanceENS0_6EStyleES1_RKNS_6LocaleE, @function
_ZN6icu_6710DateFormat22createDateTimeInstanceENS0_6EStyleES1_RKNS_6LocaleE:
.LFB3362:
	.cfi_startproc
	endbr64
	movl	%edi, %r8d
	movl	%esi, %edi
	leal	4(%r8), %eax
	cmpl	$-1, %r8d
	cmovne	%eax, %r8d
	movl	%r8d, %esi
	jmp	_ZN6icu_6710DateFormat6createENS0_6EStyleES1_RKNS_6LocaleE
	.cfi_endproc
.LFE3362:
	.size	_ZN6icu_6710DateFormat22createDateTimeInstanceENS0_6EStyleES1_RKNS_6LocaleE, .-_ZN6icu_6710DateFormat22createDateTimeInstanceENS0_6EStyleES1_RKNS_6LocaleE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710DateFormat18createDateInstanceENS0_6EStyleERKNS_6LocaleE
	.type	_ZN6icu_6710DateFormat18createDateInstanceENS0_6EStyleERKNS_6LocaleE, @function
_ZN6icu_6710DateFormat18createDateInstanceENS0_6EStyleERKNS_6LocaleE:
.LFB3361:
	.cfi_startproc
	endbr64
	movl	%edi, %r8d
	leal	4(%rdi), %eax
	cmpl	$-1, %edi
	movq	%rsi, %rdx
	cmovne	%eax, %r8d
	movl	$-1, %edi
	movl	%r8d, %esi
	jmp	_ZN6icu_6710DateFormat6createENS0_6EStyleES1_RKNS_6LocaleE
	.cfi_endproc
.LFE3361:
	.size	_ZN6icu_6710DateFormat18createDateInstanceENS0_6EStyleERKNS_6LocaleE, .-_ZN6icu_6710DateFormat18createDateInstanceENS0_6EStyleERKNS_6LocaleE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710DateFormat19getAvailableLocalesERi
	.type	_ZN6icu_6710DateFormat19getAvailableLocalesERi, @function
_ZN6icu_6710DateFormat19getAvailableLocalesERi:
.LFB3371:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_676Locale19getAvailableLocalesERi@PLT
	.cfi_endproc
.LFE3371:
	.size	_ZN6icu_6710DateFormat19getAvailableLocalesERi, .-_ZN6icu_6710DateFormat19getAvailableLocalesERi
	.section	.text._ZN6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEED2Ev,"axG",@progbits,_ZN6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEED2Ev
	.type	_ZN6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEED2Ev, @function
_ZN6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEED2Ev:
.LFB3840:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	16(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -16(%rdi)
	call	_ZN6icu_676LocaleD1Ev@PLT
	leaq	16+_ZTVN6icu_678CacheKeyINS_18DateFmtBestPatternEEE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6712CacheKeyBaseD2Ev@PLT
	.cfi_endproc
.LFE3840:
	.size	_ZN6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEED2Ev, .-_ZN6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEED2Ev
	.weak	_ZN6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEED1Ev
	.set	_ZN6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEED1Ev,_ZN6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEED2Ev
	.section	.text._ZN6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEED0Ev,"axG",@progbits,_ZN6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEED0Ev
	.type	_ZN6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEED0Ev, @function
_ZN6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEED0Ev:
.LFB3842:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	16(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -16(%rdi)
	call	_ZN6icu_676LocaleD1Ev@PLT
	leaq	16+_ZTVN6icu_678CacheKeyINS_18DateFmtBestPatternEEE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	call	_ZN6icu_6712CacheKeyBaseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3842:
	.size	_ZN6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEED0Ev, .-_ZN6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEED0Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721DateFmtBestPatternKeyD2Ev
	.type	_ZN6icu_6721DateFmtBestPatternKeyD2Ev, @function
_ZN6icu_6721DateFmtBestPatternKeyD2Ev:
.LFB3336:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6721DateFmtBestPatternKeyE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	240(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -240(%rdi)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	16+_ZTVN6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEEE(%rip), %rax
	leaq	16(%r12), %rdi
	movq	%rax, (%r12)
	call	_ZN6icu_676LocaleD1Ev@PLT
	leaq	16+_ZTVN6icu_678CacheKeyINS_18DateFmtBestPatternEEE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6712CacheKeyBaseD2Ev@PLT
	.cfi_endproc
.LFE3336:
	.size	_ZN6icu_6721DateFmtBestPatternKeyD2Ev, .-_ZN6icu_6721DateFmtBestPatternKeyD2Ev
	.globl	_ZN6icu_6721DateFmtBestPatternKeyD1Ev
	.set	_ZN6icu_6721DateFmtBestPatternKeyD1Ev,_ZN6icu_6721DateFmtBestPatternKeyD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721DateFmtBestPatternKeyD0Ev
	.type	_ZN6icu_6721DateFmtBestPatternKeyD0Ev, @function
_ZN6icu_6721DateFmtBestPatternKeyD0Ev:
.LFB3338:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6721DateFmtBestPatternKeyE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	240(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -240(%rdi)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	16+_ZTVN6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEEE(%rip), %rax
	leaq	16(%r12), %rdi
	movq	%rax, (%r12)
	call	_ZN6icu_676LocaleD1Ev@PLT
	leaq	16+_ZTVN6icu_678CacheKeyINS_18DateFmtBestPatternEEE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	call	_ZN6icu_6712CacheKeyBaseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3338:
	.size	_ZN6icu_6721DateFmtBestPatternKeyD0Ev, .-_ZN6icu_6721DateFmtBestPatternKeyD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710DateFormat14getBestPatternERKNS_6LocaleERKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6710DateFormat14getBestPatternERKNS_6LocaleERKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6710DateFormat14getBestPatternERKNS_6LocaleERKNS_13UnicodeStringER10UErrorCode:
.LFB3364:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	movq	%rcx, %rdi
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$424, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6712UnifiedCache11getInstanceER10UErrorCode@PLT
	movl	(%rbx), %esi
	testl	%esi, %esi
	jle	.L392
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %ecx
	movq	%rax, (%r15)
	movw	%cx, 8(%r15)
.L391:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L411
	addq	$424, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L392:
	.cfi_restore_state
	movq	%rax, -464(%rbp)
	movq	%r13, %rsi
	leaq	16+_ZTVN6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEEE(%rip), %rax
	leaq	-352(%rbp), %r14
	movq	%rax, -368(%rbp)
	movq	%r14, %rdi
	leaq	-368(%rbp), %rax
	leaq	-128(%rbp), %r13
	movq	%rax, -456(%rbp)
	movl	$0, -360(%rbp)
	movb	$0, -356(%rbp)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	leaq	16+_ZTVN6icu_6721DateFmtBestPatternKeyE(%rip), %rax
	movq	%rax, -368(%rbp)
	call	_ZN6icu_6724DateTimePatternGenerator17staticGetSkeletonERKNS_13UnicodeStringER10UErrorCode@PLT
	movl	(%rbx), %edx
	movq	-464(%rbp), %rax
	testl	%edx, %edx
	jle	.L394
.L402:
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, (%r15)
	movl	$2, %eax
	movw	%ax, 8(%r15)
.L395:
	leaq	16+_ZTVN6icu_6721DateFmtBestPatternKeyE(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, -368(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	16+_ZTVN6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEEE(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -368(%rbp)
	call	_ZN6icu_676LocaleD1Ev@PLT
	movq	-456(%rbp), %rdi
	leaq	16+_ZTVN6icu_678CacheKeyINS_18DateFmtBestPatternEEE(%rip), %rax
	movq	%rax, -368(%rbp)
	call	_ZN6icu_6712CacheKeyBaseD2Ev@PLT
	jmp	.L391
	.p2align 4,,10
	.p2align 3
.L394:
	movq	-456(%rbp), %rsi
	movq	%rax, %rdi
	leaq	-440(%rbp), %rdx
	xorl	%ecx, %ecx
	leaq	-444(%rbp), %r8
	movl	$0, -444(%rbp)
	movq	$0, -440(%rbp)
	call	_ZNK6icu_6712UnifiedCache4_getERKNS_12CacheKeyBaseERPKNS_12SharedObjectEPKvR10UErrorCode@PLT
	movl	-444(%rbp), %eax
	movq	-440(%rbp), %r12
	testl	%eax, %eax
	jle	.L412
	testq	%r12, %r12
	jne	.L413
.L399:
	movl	%eax, (%rbx)
.L401:
	testl	%eax, %eax
	jg	.L402
	leaq	-432(%rbp), %rbx
	leaq	24(%r12), %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	%r12, %rdi
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	movq	%r15, %rdi
	movq	%rbx, %rsi
	call	_ZN6icu_6713UnicodeStringC1EOS0_@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L395
	.p2align 4,,10
	.p2align 3
.L413:
	movq	%r12, %r8
	xorl	%r12d, %r12d
.L398:
	movq	%r8, %rdi
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jne	.L400
	movl	-444(%rbp), %eax
	jmp	.L399
	.p2align 4,,10
	.p2align 3
.L400:
	movl	-444(%rbp), %edx
	testl	%edx, %edx
	jle	.L401
	movl	%edx, %eax
	jmp	.L399
	.p2align 4,,10
	.p2align 3
.L412:
	testq	%r12, %r12
	je	.L397
	movq	%r12, %rdi
	movq	%r12, -464(%rbp)
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
	movq	-464(%rbp), %r8
	jmp	.L398
.L397:
	movl	(%rbx), %edx
	testl	%edx, %edx
	je	.L399
	movl	%edx, %eax
	jmp	.L401
.L411:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3364:
	.size	_ZN6icu_6710DateFormat14getBestPatternERKNS_6LocaleERKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6710DateFormat14getBestPatternERKNS_6LocaleERKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.type	_ZN6icu_6710DateFormat25createInstanceForSkeletonERKNS_13UnicodeStringERKNS_6LocaleER10UErrorCode.part.0, @function
_ZN6icu_6710DateFormat25createInstanceForSkeletonERKNS_13UnicodeStringERKNS_6LocaleER10UErrorCode.part.0:
.LFB4556:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rdx, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$504, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6712UnifiedCache11getInstanceER10UErrorCode@PLT
	movl	(%rbx), %r8d
	testl	%r8d, %r8d
	jle	.L415
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %edi
	leaq	-496(%rbp), %r14
	movq	%rax, -496(%rbp)
	movw	%di, -488(%rbp)
.L416:
	movl	$864, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L426
	movq	%r12, %rdx
	movq	%rax, %rdi
	movq	%rbx, %rcx
	movq	%r14, %rsi
	call	_ZN6icu_6716SimpleDateFormatC1ERKNS_13UnicodeStringERKNS_6LocaleER10UErrorCode@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	(%rbx), %edx
	testl	%edx, %edx
	jle	.L414
	movq	0(%r13), %rax
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	call	*8(%rax)
.L414:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L440
	addq	$504, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L426:
	.cfi_restore_state
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L441
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L414
	.p2align 4,,10
	.p2align 3
.L415:
	movq	%rax, %r14
	movq	%r12, %rsi
	leaq	16+_ZTVN6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEEE(%rip), %rax
	movl	$0, -360(%rbp)
	movq	%rax, -368(%rbp)
	leaq	-368(%rbp), %rax
	leaq	-128(%rbp), %r15
	movq	%rax, -520(%rbp)
	leaq	-352(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -528(%rbp)
	movb	$0, -356(%rbp)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movq	%r13, %rsi
	movq	%rbx, %rdx
	movq	%r15, %rdi
	leaq	16+_ZTVN6icu_6721DateFmtBestPatternKeyE(%rip), %rax
	movq	%rax, -368(%rbp)
	call	_ZN6icu_6724DateTimePatternGenerator17staticGetSkeletonERKNS_13UnicodeStringER10UErrorCode@PLT
	movl	(%rbx), %esi
	testl	%esi, %esi
	jle	.L417
.L425:
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %ecx
	leaq	-496(%rbp), %r14
	movq	%rax, -496(%rbp)
	movw	%cx, -488(%rbp)
.L418:
	leaq	16+_ZTVN6icu_6721DateFmtBestPatternKeyE(%rip), %rax
	movq	%r15, %rdi
	movq	%rax, -368(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-528(%rbp), %rdi
	leaq	16+_ZTVN6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEEE(%rip), %rax
	movq	%rax, -368(%rbp)
	call	_ZN6icu_676LocaleD1Ev@PLT
	movq	-520(%rbp), %rdi
	leaq	16+_ZTVN6icu_678CacheKeyINS_18DateFmtBestPatternEEE(%rip), %rax
	movq	%rax, -368(%rbp)
	call	_ZN6icu_6712CacheKeyBaseD2Ev@PLT
	jmp	.L416
	.p2align 4,,10
	.p2align 3
.L441:
	movl	$7, (%rbx)
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L414
	.p2align 4,,10
	.p2align 3
.L417:
	movq	-520(%rbp), %rsi
	leaq	-504(%rbp), %rdx
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	leaq	-508(%rbp), %r8
	movl	$0, -508(%rbp)
	movq	$0, -504(%rbp)
	call	_ZNK6icu_6712UnifiedCache4_getERKNS_12CacheKeyBaseERPKNS_12SharedObjectEPKvR10UErrorCode@PLT
	movl	-508(%rbp), %eax
	movq	-504(%rbp), %r13
	testl	%eax, %eax
	jle	.L442
	testq	%r13, %r13
	jne	.L443
.L422:
	movl	%eax, (%rbx)
.L424:
	testl	%eax, %eax
	jg	.L425
	leaq	-432(%rbp), %r10
	leaq	24(%r13), %rsi
	movq	%r10, %rdi
	movq	%r10, -536(%rbp)
	leaq	-496(%rbp), %r14
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	%r13, %rdi
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	movq	-536(%rbp), %r10
	movq	%r14, %rdi
	movq	%r10, %rsi
	call	_ZN6icu_6713UnicodeStringC1EOS0_@PLT
	movq	-536(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L418
	.p2align 4,,10
	.p2align 3
.L443:
	movq	%r13, %r14
	xorl	%r13d, %r13d
.L421:
	movq	%r14, %rdi
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jne	.L423
	movl	-508(%rbp), %eax
	jmp	.L422
	.p2align 4,,10
	.p2align 3
.L423:
	movl	-508(%rbp), %edx
	testl	%edx, %edx
	jle	.L424
	movl	%edx, %eax
	jmp	.L422
	.p2align 4,,10
	.p2align 3
.L442:
	testq	%r13, %r13
	je	.L420
	movq	%r13, %rdi
	movq	%r13, %r14
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
	jmp	.L421
.L420:
	movl	(%rbx), %edx
	testl	%edx, %edx
	je	.L422
	movl	%edx, %eax
	jmp	.L424
.L440:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4556:
	.size	_ZN6icu_6710DateFormat25createInstanceForSkeletonERKNS_13UnicodeStringERKNS_6LocaleER10UErrorCode.part.0, .-_ZN6icu_6710DateFormat25createInstanceForSkeletonERKNS_13UnicodeStringERKNS_6LocaleER10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710DateFormat25createInstanceForSkeletonERKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6710DateFormat25createInstanceForSkeletonERKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6710DateFormat25createInstanceForSkeletonERKNS_13UnicodeStringER10UErrorCode:
.LFB3369:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	call	_ZN6icu_676Locale10getDefaultEv@PLT
	movl	(%r12), %edx
	testl	%edx, %edx
	jg	.L445
	movq	%r12, %rdx
	movq	%r13, %rdi
	popq	%r12
	movq	%rax, %rsi
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6710DateFormat25createInstanceForSkeletonERKNS_13UnicodeStringERKNS_6LocaleER10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L445:
	.cfi_restore_state
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3369:
	.size	_ZN6icu_6710DateFormat25createInstanceForSkeletonERKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6710DateFormat25createInstanceForSkeletonERKNS_13UnicodeStringER10UErrorCode
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"calendar"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710DateFormat25createInstanceForSkeletonEPNS_8CalendarERKNS_13UnicodeStringERKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_6710DateFormat25createInstanceForSkeletonEPNS_8CalendarERKNS_13UnicodeStringERKNS_6LocaleER10UErrorCode, @function
_ZN6icu_6710DateFormat25createInstanceForSkeletonEPNS_8CalendarERKNS_13UnicodeStringERKNS_6LocaleER10UErrorCode:
.LFB3365:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$256, %rsp
	.cfi_offset 3, -48
	movl	(%rcx), %edi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%edi, %edi
	jg	.L448
	movq	%rcx, %rbx
	testq	%r12, %r12
	je	.L456
	leaq	-272(%rbp), %r13
	movq	%rsi, %r14
	movq	%rdx, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*184(%rax)
	movq	%rbx, %rcx
	leaq	.LC1(%rip), %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN6icu_676Locale15setKeywordValueEPKcS2_R10UErrorCode@PLT
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jle	.L457
.L451:
	movq	%r13, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
.L452:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
	xorl	%eax, %eax
.L447:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L458
	addq	$256, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L457:
	.cfi_restore_state
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6710DateFormat25createInstanceForSkeletonERKNS_13UnicodeStringERKNS_6LocaleER10UErrorCode.part.0
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L451
	movq	(%rax), %rdx
	movq	%rax, %rdi
	movq	%rax, -280(%rbp)
	movq	%r12, %rsi
	call	*136(%rdx)
	movq	%r13, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	movq	-280(%rbp), %rax
	jmp	.L447
	.p2align 4,,10
	.p2align 3
.L448:
	testq	%r12, %r12
	jne	.L452
	xorl	%eax, %eax
	jmp	.L447
	.p2align 4,,10
	.p2align 3
.L456:
	movl	$1, (%rcx)
	jmp	.L447
.L458:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3365:
	.size	_ZN6icu_6710DateFormat25createInstanceForSkeletonEPNS_8CalendarERKNS_13UnicodeStringERKNS_6LocaleER10UErrorCode, .-_ZN6icu_6710DateFormat25createInstanceForSkeletonEPNS_8CalendarERKNS_13UnicodeStringERKNS_6LocaleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710DateFormat25createInstanceForSkeletonERKNS_13UnicodeStringERKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_6710DateFormat25createInstanceForSkeletonERKNS_13UnicodeStringERKNS_6LocaleER10UErrorCode, @function
_ZN6icu_6710DateFormat25createInstanceForSkeletonERKNS_13UnicodeStringERKNS_6LocaleER10UErrorCode:
.LFB3368:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$488, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rdx), %r9d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r9d, %r9d
	jg	.L459
	movq	%rdx, %rbx
	movq	%rdi, %r13
	movq	%rdx, %rdi
	movq	%rsi, %r12
	call	_ZN6icu_6712UnifiedCache11getInstanceER10UErrorCode@PLT
	movl	(%rbx), %r8d
	testl	%r8d, %r8d
	jle	.L461
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %edi
	leaq	-496(%rbp), %r13
	movq	%rax, -496(%rbp)
	movw	%di, -488(%rbp)
.L462:
	movl	$864, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L472
	movq	%r12, %rdx
	movq	%rax, %rdi
	movq	%rbx, %rcx
	movq	%r13, %rsi
	call	_ZN6icu_6716SimpleDateFormatC1ERKNS_13UnicodeStringERKNS_6LocaleER10UErrorCode@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	(%rbx), %edx
	testl	%edx, %edx
	jle	.L459
	movq	(%r14), %rax
	movq	%r14, %rdi
	xorl	%r14d, %r14d
	call	*8(%rax)
.L459:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L486
	addq	$488, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L461:
	.cfi_restore_state
	movq	%rax, -528(%rbp)
	leaq	-352(%rbp), %r14
	movq	%r12, %rsi
	leaq	-128(%rbp), %r15
	leaq	16+_ZTVN6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEEE(%rip), %rax
	movq	%r14, %rdi
	movl	$0, -360(%rbp)
	movq	%rax, -368(%rbp)
	leaq	-368(%rbp), %rax
	movq	%rax, -520(%rbp)
	movb	$0, -356(%rbp)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movq	%r13, %rsi
	movq	%rbx, %rdx
	movq	%r15, %rdi
	leaq	16+_ZTVN6icu_6721DateFmtBestPatternKeyE(%rip), %rax
	movq	%rax, -368(%rbp)
	call	_ZN6icu_6724DateTimePatternGenerator17staticGetSkeletonERKNS_13UnicodeStringER10UErrorCode@PLT
	movl	(%rbx), %esi
	movq	-528(%rbp), %rax
	testl	%esi, %esi
	jle	.L463
.L471:
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %ecx
	leaq	-496(%rbp), %r13
	movq	%rax, -496(%rbp)
	movw	%cx, -488(%rbp)
.L464:
	leaq	16+_ZTVN6icu_6721DateFmtBestPatternKeyE(%rip), %rax
	movq	%r15, %rdi
	movq	%rax, -368(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	16+_ZTVN6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEEE(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -368(%rbp)
	call	_ZN6icu_676LocaleD1Ev@PLT
	movq	-520(%rbp), %rdi
	leaq	16+_ZTVN6icu_678CacheKeyINS_18DateFmtBestPatternEEE(%rip), %rax
	movq	%rax, -368(%rbp)
	call	_ZN6icu_6712CacheKeyBaseD2Ev@PLT
	jmp	.L462
.L472:
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L474
	movl	$7, (%rbx)
.L474:
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L459
	.p2align 4,,10
	.p2align 3
.L463:
	movq	-520(%rbp), %rsi
	movq	%rax, %rdi
	leaq	-504(%rbp), %rdx
	xorl	%ecx, %ecx
	leaq	-508(%rbp), %r8
	movl	$0, -508(%rbp)
	movq	$0, -504(%rbp)
	call	_ZNK6icu_6712UnifiedCache4_getERKNS_12CacheKeyBaseERPKNS_12SharedObjectEPKvR10UErrorCode@PLT
	movl	-508(%rbp), %eax
	movq	-504(%rbp), %r13
	testl	%eax, %eax
	jle	.L487
	testq	%r13, %r13
	je	.L468
	movq	%r13, %r8
	xorl	%r13d, %r13d
.L467:
	movq	%r8, %rdi
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jne	.L469
	movl	-508(%rbp), %eax
.L468:
	movl	%eax, (%rbx)
.L470:
	testl	%eax, %eax
	jg	.L471
	leaq	-432(%rbp), %r8
	leaq	24(%r13), %rsi
	movq	%r8, %rdi
	movq	%r8, -528(%rbp)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	%r13, %rdi
	leaq	-496(%rbp), %r13
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	movq	-528(%rbp), %r8
	movq	%r13, %rdi
	movq	%r8, %rsi
	call	_ZN6icu_6713UnicodeStringC1EOS0_@PLT
	movq	-528(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L464
	.p2align 4,,10
	.p2align 3
.L487:
	testq	%r13, %r13
	je	.L466
	movq	%r13, %rdi
	movq	%r13, -528(%rbp)
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
	movq	-528(%rbp), %r8
	jmp	.L467
	.p2align 4,,10
	.p2align 3
.L469:
	movl	-508(%rbp), %edx
	testl	%edx, %edx
	jle	.L470
	movl	%edx, %eax
	jmp	.L468
.L466:
	movl	(%rbx), %edx
	testl	%edx, %edx
	je	.L468
	movl	%edx, %eax
	jmp	.L470
.L486:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3368:
	.size	_ZN6icu_6710DateFormat25createInstanceForSkeletonERKNS_13UnicodeStringERKNS_6LocaleER10UErrorCode, .-_ZN6icu_6710DateFormat25createInstanceForSkeletonERKNS_13UnicodeStringERKNS_6LocaleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710DateFormateqERKNS_6FormatE
	.type	_ZNK6icu_6710DateFormateqERKNS_6FormatE, @function
_ZNK6icu_6710DateFormateqERKNS_6FormatE:
.LFB3350:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	cmpq	%rdi, %rsi
	je	.L506
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZNK6icu_676FormateqERKS0_@PLT
	testb	%al, %al
	jne	.L490
.L491:
	xorl	%eax, %eax
.L488:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L490:
	.cfi_restore_state
	movq	328(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L491
	movq	(%rdi), %rax
	movq	328(%r12), %rsi
	call	*40(%rax)
	testb	%al, %al
	je	.L491
	movq	336(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L491
	movq	(%rdi), %rax
	movq	336(%r12), %rsi
	call	*24(%rax)
	testb	%al, %al
	je	.L491
	movl	348(%r12), %eax
	cmpl	%eax, 348(%rbx)
	sete	%al
	jmp	.L488
	.p2align 4,,10
	.p2align 3
.L506:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE3350:
	.size	_ZNK6icu_6710DateFormateqERKNS_6FormatE, .-_ZNK6icu_6710DateFormateqERKNS_6FormatE
	.weak	_ZTSN6icu_6710DateFormatE
	.section	.rodata._ZTSN6icu_6710DateFormatE,"aG",@progbits,_ZTSN6icu_6710DateFormatE,comdat
	.align 16
	.type	_ZTSN6icu_6710DateFormatE, @object
	.size	_ZTSN6icu_6710DateFormatE, 22
_ZTSN6icu_6710DateFormatE:
	.string	"N6icu_6710DateFormatE"
	.weak	_ZTIN6icu_6710DateFormatE
	.section	.data.rel.ro._ZTIN6icu_6710DateFormatE,"awG",@progbits,_ZTIN6icu_6710DateFormatE,comdat
	.align 8
	.type	_ZTIN6icu_6710DateFormatE, @object
	.size	_ZTIN6icu_6710DateFormatE, 24
_ZTIN6icu_6710DateFormatE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6710DateFormatE
	.quad	_ZTIN6icu_676FormatE
	.weak	_ZTSN6icu_6718DateFmtBestPatternE
	.section	.rodata._ZTSN6icu_6718DateFmtBestPatternE,"aG",@progbits,_ZTSN6icu_6718DateFmtBestPatternE,comdat
	.align 16
	.type	_ZTSN6icu_6718DateFmtBestPatternE, @object
	.size	_ZTSN6icu_6718DateFmtBestPatternE, 30
_ZTSN6icu_6718DateFmtBestPatternE:
	.string	"N6icu_6718DateFmtBestPatternE"
	.weak	_ZTIN6icu_6718DateFmtBestPatternE
	.section	.data.rel.ro._ZTIN6icu_6718DateFmtBestPatternE,"awG",@progbits,_ZTIN6icu_6718DateFmtBestPatternE,comdat
	.align 8
	.type	_ZTIN6icu_6718DateFmtBestPatternE, @object
	.size	_ZTIN6icu_6718DateFmtBestPatternE, 24
_ZTIN6icu_6718DateFmtBestPatternE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6718DateFmtBestPatternE
	.quad	_ZTIN6icu_6712SharedObjectE
	.weak	_ZTSN6icu_678CacheKeyINS_18DateFmtBestPatternEEE
	.section	.rodata._ZTSN6icu_678CacheKeyINS_18DateFmtBestPatternEEE,"aG",@progbits,_ZTSN6icu_678CacheKeyINS_18DateFmtBestPatternEEE,comdat
	.align 32
	.type	_ZTSN6icu_678CacheKeyINS_18DateFmtBestPatternEEE, @object
	.size	_ZTSN6icu_678CacheKeyINS_18DateFmtBestPatternEEE, 45
_ZTSN6icu_678CacheKeyINS_18DateFmtBestPatternEEE:
	.string	"N6icu_678CacheKeyINS_18DateFmtBestPatternEEE"
	.weak	_ZTIN6icu_678CacheKeyINS_18DateFmtBestPatternEEE
	.section	.data.rel.ro._ZTIN6icu_678CacheKeyINS_18DateFmtBestPatternEEE,"awG",@progbits,_ZTIN6icu_678CacheKeyINS_18DateFmtBestPatternEEE,comdat
	.align 8
	.type	_ZTIN6icu_678CacheKeyINS_18DateFmtBestPatternEEE, @object
	.size	_ZTIN6icu_678CacheKeyINS_18DateFmtBestPatternEEE, 24
_ZTIN6icu_678CacheKeyINS_18DateFmtBestPatternEEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_678CacheKeyINS_18DateFmtBestPatternEEE
	.quad	_ZTIN6icu_6712CacheKeyBaseE
	.weak	_ZTSN6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEEE
	.section	.rodata._ZTSN6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEEE,"aG",@progbits,_ZTSN6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEEE,comdat
	.align 32
	.type	_ZTSN6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEEE, @object
	.size	_ZTSN6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEEE, 52
_ZTSN6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEEE:
	.string	"N6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEEE"
	.weak	_ZTIN6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEEE
	.section	.data.rel.ro._ZTIN6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEEE,"awG",@progbits,_ZTIN6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEEE,comdat
	.align 8
	.type	_ZTIN6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEEE, @object
	.size	_ZTIN6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEEE, 24
_ZTIN6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEEE
	.quad	_ZTIN6icu_678CacheKeyINS_18DateFmtBestPatternEEE
	.weak	_ZTSN6icu_6721DateFmtBestPatternKeyE
	.section	.rodata._ZTSN6icu_6721DateFmtBestPatternKeyE,"aG",@progbits,_ZTSN6icu_6721DateFmtBestPatternKeyE,comdat
	.align 32
	.type	_ZTSN6icu_6721DateFmtBestPatternKeyE, @object
	.size	_ZTSN6icu_6721DateFmtBestPatternKeyE, 33
_ZTSN6icu_6721DateFmtBestPatternKeyE:
	.string	"N6icu_6721DateFmtBestPatternKeyE"
	.weak	_ZTIN6icu_6721DateFmtBestPatternKeyE
	.section	.data.rel.ro._ZTIN6icu_6721DateFmtBestPatternKeyE,"awG",@progbits,_ZTIN6icu_6721DateFmtBestPatternKeyE,comdat
	.align 8
	.type	_ZTIN6icu_6721DateFmtBestPatternKeyE, @object
	.size	_ZTIN6icu_6721DateFmtBestPatternKeyE, 24
_ZTIN6icu_6721DateFmtBestPatternKeyE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6721DateFmtBestPatternKeyE
	.quad	_ZTIN6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEEE
	.weak	_ZTVN6icu_6718DateFmtBestPatternE
	.section	.data.rel.ro._ZTVN6icu_6718DateFmtBestPatternE,"awG",@progbits,_ZTVN6icu_6718DateFmtBestPatternE,comdat
	.align 8
	.type	_ZTVN6icu_6718DateFmtBestPatternE, @object
	.size	_ZTVN6icu_6718DateFmtBestPatternE, 40
_ZTVN6icu_6718DateFmtBestPatternE:
	.quad	0
	.quad	_ZTIN6icu_6718DateFmtBestPatternE
	.quad	_ZN6icu_6718DateFmtBestPatternD1Ev
	.quad	_ZN6icu_6718DateFmtBestPatternD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.weak	_ZTVN6icu_678CacheKeyINS_18DateFmtBestPatternEEE
	.section	.data.rel.ro._ZTVN6icu_678CacheKeyINS_18DateFmtBestPatternEEE,"awG",@progbits,_ZTVN6icu_678CacheKeyINS_18DateFmtBestPatternEEE,comdat
	.align 8
	.type	_ZTVN6icu_678CacheKeyINS_18DateFmtBestPatternEEE, @object
	.size	_ZTVN6icu_678CacheKeyINS_18DateFmtBestPatternEEE, 80
_ZTVN6icu_678CacheKeyINS_18DateFmtBestPatternEEE:
	.quad	0
	.quad	_ZTIN6icu_678CacheKeyINS_18DateFmtBestPatternEEE
	.quad	0
	.quad	0
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZNK6icu_678CacheKeyINS_18DateFmtBestPatternEE8hashCodeEv
	.quad	__cxa_pure_virtual
	.quad	_ZNK6icu_678CacheKeyINS_18DateFmtBestPatternEEeqERKNS_12CacheKeyBaseE
	.quad	__cxa_pure_virtual
	.quad	_ZNK6icu_678CacheKeyINS_18DateFmtBestPatternEE16writeDescriptionEPci
	.weak	_ZTVN6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEEE
	.section	.data.rel.ro._ZTVN6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEEE,"awG",@progbits,_ZTVN6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEEE,comdat
	.align 8
	.type	_ZTVN6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEEE, @object
	.size	_ZTVN6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEEE, 80
_ZTVN6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEEE:
	.quad	0
	.quad	_ZTIN6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEEE
	.quad	_ZN6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEED1Ev
	.quad	_ZN6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEED0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZNK6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEE8hashCodeEv
	.quad	_ZNK6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEE5cloneEv
	.quad	_ZNK6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEEeqERKNS_12CacheKeyBaseE
	.quad	_ZNK6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEE12createObjectEPKvR10UErrorCode
	.quad	_ZNK6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEE16writeDescriptionEPci
	.weak	_ZTVN6icu_6721DateFmtBestPatternKeyE
	.section	.data.rel.ro._ZTVN6icu_6721DateFmtBestPatternKeyE,"awG",@progbits,_ZTVN6icu_6721DateFmtBestPatternKeyE,comdat
	.align 8
	.type	_ZTVN6icu_6721DateFmtBestPatternKeyE, @object
	.size	_ZTVN6icu_6721DateFmtBestPatternKeyE, 80
_ZTVN6icu_6721DateFmtBestPatternKeyE:
	.quad	0
	.quad	_ZTIN6icu_6721DateFmtBestPatternKeyE
	.quad	_ZN6icu_6721DateFmtBestPatternKeyD1Ev
	.quad	_ZN6icu_6721DateFmtBestPatternKeyD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZNK6icu_6721DateFmtBestPatternKey8hashCodeEv
	.quad	_ZNK6icu_6721DateFmtBestPatternKey5cloneEv
	.quad	_ZNK6icu_6721DateFmtBestPatternKeyeqERKNS_12CacheKeyBaseE
	.quad	_ZNK6icu_6721DateFmtBestPatternKey12createObjectEPKvR10UErrorCode
	.quad	_ZNK6icu_6714LocaleCacheKeyINS_18DateFmtBestPatternEE16writeDescriptionEPci
	.weak	_ZTVN6icu_6710DateFormatE
	.section	.data.rel.ro._ZTVN6icu_6710DateFormatE,"awG",@progbits,_ZTVN6icu_6710DateFormatE,comdat
	.align 8
	.type	_ZTVN6icu_6710DateFormatE, @object
	.size	_ZTVN6icu_6710DateFormatE, 248
_ZTVN6icu_6710DateFormatE:
	.quad	0
	.quad	_ZTIN6icu_6710DateFormatE
	.quad	0
	.quad	0
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZNK6icu_6710DateFormateqERKNS_6FormatE
	.quad	__cxa_pure_virtual
	.quad	_ZNK6icu_6710DateFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.quad	_ZNK6icu_6710DateFormat6formatERKNS_11FormattableERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode
	.quad	_ZNK6icu_6710DateFormat11parseObjectERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE
	.quad	__cxa_pure_virtual
	.quad	_ZNK6icu_6710DateFormat6formatERNS_8CalendarERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode
	.quad	_ZNK6icu_6710DateFormat5parseERKNS_13UnicodeStringER10UErrorCode
	.quad	__cxa_pure_virtual
	.quad	_ZNK6icu_6710DateFormat9isLenientEv
	.quad	_ZN6icu_6710DateFormat10setLenientEa
	.quad	_ZNK6icu_6710DateFormat17isCalendarLenientEv
	.quad	_ZN6icu_6710DateFormat18setCalendarLenientEa
	.quad	_ZNK6icu_6710DateFormat11getCalendarEv
	.quad	_ZN6icu_6710DateFormat13adoptCalendarEPNS_8CalendarE
	.quad	_ZN6icu_6710DateFormat11setCalendarERKNS_8CalendarE
	.quad	_ZNK6icu_6710DateFormat15getNumberFormatEv
	.quad	_ZN6icu_6710DateFormat17adoptNumberFormatEPNS_12NumberFormatE
	.quad	_ZN6icu_6710DateFormat15setNumberFormatERKNS_12NumberFormatE
	.quad	_ZNK6icu_6710DateFormat11getTimeZoneEv
	.quad	_ZN6icu_6710DateFormat13adoptTimeZoneEPNS_8TimeZoneE
	.quad	_ZN6icu_6710DateFormat11setTimeZoneERKNS_8TimeZoneE
	.quad	_ZN6icu_6710DateFormat10setContextE15UDisplayContextR10UErrorCode
	.quad	_ZNK6icu_6710DateFormat10getContextE19UDisplayContextTypeR10UErrorCode
	.quad	_ZN6icu_6710DateFormat19setBooleanAttributeE27UDateFormatBooleanAttributeaR10UErrorCode
	.quad	_ZNK6icu_6710DateFormat19getBooleanAttributeE27UDateFormatBooleanAttributeR10UErrorCode
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
