	.file	"ucoleitr.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_679RCEBufferC2Ev
	.type	_ZN6icu_679RCEBufferC2Ev, @function
_ZN6icu_679RCEBufferC2Ev:
.LFB2472:
	.cfi_startproc
	endbr64
	movabsq	$68719476736, %rax
	movq	%rdi, 192(%rdi)
	movq	%rax, 200(%rdi)
	ret
	.cfi_endproc
.LFE2472:
	.size	_ZN6icu_679RCEBufferC2Ev, .-_ZN6icu_679RCEBufferC2Ev
	.globl	_ZN6icu_679RCEBufferC1Ev
	.set	_ZN6icu_679RCEBufferC1Ev,_ZN6icu_679RCEBufferC2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_679RCEBufferD2Ev
	.type	_ZN6icu_679RCEBufferD2Ev, @function
_ZN6icu_679RCEBufferD2Ev:
.LFB2475:
	.cfi_startproc
	endbr64
	movq	192(%rdi), %r8
	cmpq	%rdi, %r8
	je	.L3
	movq	%r8, %rdi
	jmp	uprv_free_67@PLT
	.p2align 4,,10
	.p2align 3
.L3:
	ret
	.cfi_endproc
.LFE2475:
	.size	_ZN6icu_679RCEBufferD2Ev, .-_ZN6icu_679RCEBufferD2Ev
	.globl	_ZN6icu_679RCEBufferD1Ev
	.set	_ZN6icu_679RCEBufferD1Ev,_ZN6icu_679RCEBufferD2Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_679RCEBuffer7isEmptyEv
	.type	_ZNK6icu_679RCEBuffer7isEmptyEv, @function
_ZNK6icu_679RCEBuffer7isEmptyEv:
.LFB2477:
	.cfi_startproc
	endbr64
	movl	200(%rdi), %eax
	testl	%eax, %eax
	setle	%al
	ret
	.cfi_endproc
.LFE2477:
	.size	_ZNK6icu_679RCEBuffer7isEmptyEv, .-_ZNK6icu_679RCEBuffer7isEmptyEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_679RCEBuffer3putEjiiR10UErrorCode
	.type	_ZN6icu_679RCEBuffer3putEjiiR10UErrorCode, @function
_ZN6icu_679RCEBuffer3putEjiiR10UErrorCode:
.LFB2478:
	.cfi_startproc
	endbr64
	movl	(%r8), %eax
	testl	%eax, %eax
	jg	.L13
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%esi, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	200(%rdi), %eax
	movl	204(%rdi), %edx
	cmpl	%edx, %eax
	jge	.L8
	movq	192(%rdi), %rcx
.L9:
	movslq	%eax, %rdx
	addl	$1, %eax
	leaq	(%rdx,%rdx,2), %rdx
	leaq	(%rcx,%rdx,4), %rdx
	movl	%r15d, (%rdx)
	movl	%r14d, 4(%rdx)
	movl	%r13d, 8(%rdx)
	movl	%eax, 200(%rbx)
.L6:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	addl	$8, %edx
	movq	%r8, %r12
	movslq	%edx, %rdx
	leaq	(%rdx,%rdx,2), %rdi
	salq	$2, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L16
	movslq	204(%rbx), %rax
	movq	192(%rbx), %r8
	movq	%rcx, %rdi
	leaq	(%rax,%rax,2), %rdx
	movq	%r8, %rsi
	movq	%r8, -56(%rbp)
	movq	%rax, %r12
	salq	$2, %rdx
	call	memcpy@PLT
	movq	-56(%rbp), %r8
	movq	%rax, %rcx
	cmpq	%rbx, %r8
	je	.L11
	movq	%r8, %rdi
	movq	%rax, -56(%rbp)
	call	uprv_free_67@PLT
	movl	204(%rbx), %r12d
	movq	-56(%rbp), %rcx
.L11:
	addl	$8, %r12d
	movq	%rcx, 192(%rbx)
	movl	200(%rbx), %eax
	movl	%r12d, 204(%rbx)
	jmp	.L9
.L16:
	movl	$7, (%r12)
	jmp	.L6
	.cfi_endproc
.LFE2478:
	.size	_ZN6icu_679RCEBuffer3putEjiiR10UErrorCode, .-_ZN6icu_679RCEBuffer3putEjiiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_679RCEBuffer3getEv
	.type	_ZN6icu_679RCEBuffer3getEv, @function
_ZN6icu_679RCEBuffer3getEv:
.LFB2479:
	.cfi_startproc
	endbr64
	movl	200(%rdi), %eax
	xorl	%r8d, %r8d
	testl	%eax, %eax
	jle	.L17
	subl	$1, %eax
	movl	%eax, 200(%rdi)
	cltq
	leaq	(%rax,%rax,2), %r8
	movq	192(%rdi), %rax
	leaq	(%rax,%r8,4), %r8
.L17:
	movq	%r8, %rax
	ret
	.cfi_endproc
.LFE2479:
	.size	_ZN6icu_679RCEBuffer3getEv, .-_ZN6icu_679RCEBuffer3getEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_679PCEBufferC2Ev
	.type	_ZN6icu_679PCEBufferC2Ev, @function
_ZN6icu_679PCEBufferC2Ev:
.LFB2481:
	.cfi_startproc
	endbr64
	movabsq	$68719476736, %rax
	movq	%rdi, 256(%rdi)
	movq	%rax, 264(%rdi)
	ret
	.cfi_endproc
.LFE2481:
	.size	_ZN6icu_679PCEBufferC2Ev, .-_ZN6icu_679PCEBufferC2Ev
	.globl	_ZN6icu_679PCEBufferC1Ev
	.set	_ZN6icu_679PCEBufferC1Ev,_ZN6icu_679PCEBufferC2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_679PCEBufferD2Ev
	.type	_ZN6icu_679PCEBufferD2Ev, @function
_ZN6icu_679PCEBufferD2Ev:
.LFB2484:
	.cfi_startproc
	endbr64
	movq	256(%rdi), %r8
	cmpq	%rdi, %r8
	je	.L21
	movq	%r8, %rdi
	jmp	uprv_free_67@PLT
	.p2align 4,,10
	.p2align 3
.L21:
	ret
	.cfi_endproc
.LFE2484:
	.size	_ZN6icu_679PCEBufferD2Ev, .-_ZN6icu_679PCEBufferD2Ev
	.globl	_ZN6icu_679PCEBufferD1Ev
	.set	_ZN6icu_679PCEBufferD1Ev,_ZN6icu_679PCEBufferD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_679PCEBuffer5resetEv
	.type	_ZN6icu_679PCEBuffer5resetEv, @function
_ZN6icu_679PCEBuffer5resetEv:
.LFB2486:
	.cfi_startproc
	endbr64
	movl	$0, 264(%rdi)
	ret
	.cfi_endproc
.LFE2486:
	.size	_ZN6icu_679PCEBuffer5resetEv, .-_ZN6icu_679PCEBuffer5resetEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_679PCEBuffer7isEmptyEv
	.type	_ZNK6icu_679PCEBuffer7isEmptyEv, @function
_ZNK6icu_679PCEBuffer7isEmptyEv:
.LFB2487:
	.cfi_startproc
	endbr64
	movl	264(%rdi), %eax
	testl	%eax, %eax
	setle	%al
	ret
	.cfi_endproc
.LFE2487:
	.size	_ZNK6icu_679PCEBuffer7isEmptyEv, .-_ZNK6icu_679PCEBuffer7isEmptyEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_679PCEBuffer3putEmiiR10UErrorCode
	.type	_ZN6icu_679PCEBuffer3putEmiiR10UErrorCode, @function
_ZN6icu_679PCEBuffer3putEmiiR10UErrorCode:
.LFB2488:
	.cfi_startproc
	endbr64
	movl	(%r8), %eax
	testl	%eax, %eax
	jg	.L32
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	264(%rdi), %edx
	movl	268(%rdi), %eax
	cmpl	%eax, %edx
	jge	.L27
	movq	256(%rdi), %r12
.L28:
	movslq	%edx, %rax
	addl	$1, %edx
	salq	$4, %rax
	addq	%r12, %rax
	movq	%r15, (%rax)
	movl	%r14d, 8(%rax)
	movl	%ecx, 12(%rax)
	movl	%edx, 264(%rbx)
.L25:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	leal	8(%rax), %edi
	movl	%ecx, -56(%rbp)
	movq	%r8, %r13
	movslq	%edi, %rdi
	salq	$4, %rdi
	call	uprv_malloc_67@PLT
	movl	-56(%rbp), %ecx
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L35
	movq	256(%rbx), %r8
	movslq	268(%rbx), %rdx
	movq	%rax, %rdi
	movl	%ecx, -60(%rbp)
	movq	%r8, %rsi
	movq	%rdx, %r13
	salq	$4, %rdx
	movq	%r8, -56(%rbp)
	call	memcpy@PLT
	movq	-56(%rbp), %r8
	movl	-60(%rbp), %ecx
	cmpq	%rbx, %r8
	je	.L30
	movq	%r8, %rdi
	movl	%ecx, -56(%rbp)
	call	uprv_free_67@PLT
	movl	268(%rbx), %r13d
	movl	-56(%rbp), %ecx
.L30:
	addl	$8, %r13d
	movq	%r12, 256(%rbx)
	movl	264(%rbx), %edx
	movl	%r13d, 268(%rbx)
	jmp	.L28
.L35:
	movl	$7, 0(%r13)
	jmp	.L25
	.cfi_endproc
.LFE2488:
	.size	_ZN6icu_679PCEBuffer3putEmiiR10UErrorCode, .-_ZN6icu_679PCEBuffer3putEmiiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_679PCEBuffer3getEv
	.type	_ZN6icu_679PCEBuffer3getEv, @function
_ZN6icu_679PCEBuffer3getEv:
.LFB2489:
	.cfi_startproc
	endbr64
	movl	264(%rdi), %eax
	xorl	%r8d, %r8d
	testl	%eax, %eax
	jle	.L36
	subl	$1, %eax
	movl	%eax, 264(%rdi)
	cltq
	salq	$4, %rax
	addq	256(%rdi), %rax
	movq	%rax, %r8
.L36:
	movq	%r8, %rax
	ret
	.cfi_endproc
.LFE2489:
	.size	_ZN6icu_679PCEBuffer3getEv, .-_ZN6icu_679PCEBuffer3getEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UCollationPCEC2EP18UCollationElements
	.type	_ZN6icu_6713UCollationPCEC2EP18UCollationElements, @function
_ZN6icu_6713UCollationPCEC2EP18UCollationElements:
.LFB2491:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-44(%rbp), %r13
	pushq	%rbx
	movq	%r13, %rdx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	16(%rsi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rsi, 272(%rdi)
	movl	$5, %esi
	movabsq	$68719476736, %rax
	movq	%rax, 264(%rdi)
	movq	(%r12), %rax
	movq	%rdi, 256(%rdi)
	movq	%r12, %rdi
	movl	$0, -44(%rbp)
	call	*192(%rax)
	movq	%r13, %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	movl	%eax, 280(%rbx)
	movq	(%r12), %rax
	call	*192(%rax)
	movb	$0, 285(%rbx)
	movq	%r13, %rsi
	movq	%r12, %rdi
	cmpl	$20, %eax
	sete	284(%rbx)
	movq	(%r12), %rax
	call	*240(%rax)
	movl	%eax, 288(%rbx)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L42
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L42:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2491:
	.size	_ZN6icu_6713UCollationPCEC2EP18UCollationElements, .-_ZN6icu_6713UCollationPCEC2EP18UCollationElements
	.globl	_ZN6icu_6713UCollationPCEC1EP18UCollationElements
	.set	_ZN6icu_6713UCollationPCEC1EP18UCollationElements,_ZN6icu_6713UCollationPCEC2EP18UCollationElements
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UCollationPCEC2EPNS_24CollationElementIteratorE
	.type	_ZN6icu_6713UCollationPCEC2EPNS_24CollationElementIteratorE, @function
_ZN6icu_6713UCollationPCEC2EPNS_24CollationElementIteratorE:
.LFB2494:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-44(%rbp), %r13
	pushq	%rbx
	movq	%r13, %rdx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	16(%rsi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rsi, 272(%rdi)
	movl	$5, %esi
	movabsq	$68719476736, %rax
	movq	%rax, 264(%rdi)
	movq	(%r12), %rax
	movq	%rdi, 256(%rdi)
	movq	%r12, %rdi
	movl	$0, -44(%rbp)
	call	*192(%rax)
	movq	%r13, %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	movl	%eax, 280(%rbx)
	movq	(%r12), %rax
	call	*192(%rax)
	movb	$0, 285(%rbx)
	movq	%r13, %rsi
	movq	%r12, %rdi
	cmpl	$20, %eax
	sete	284(%rbx)
	movq	(%r12), %rax
	call	*240(%rax)
	movl	%eax, 288(%rbx)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L46
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L46:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2494:
	.size	_ZN6icu_6713UCollationPCEC2EPNS_24CollationElementIteratorE, .-_ZN6icu_6713UCollationPCEC2EPNS_24CollationElementIteratorE
	.globl	_ZN6icu_6713UCollationPCEC1EPNS_24CollationElementIteratorE
	.set	_ZN6icu_6713UCollationPCEC1EPNS_24CollationElementIteratorE,_ZN6icu_6713UCollationPCEC2EPNS_24CollationElementIteratorE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UCollationPCE4initEP18UCollationElements
	.type	_ZN6icu_6713UCollationPCE4initEP18UCollationElements, @function
_ZN6icu_6713UCollationPCE4initEP18UCollationElements:
.LFB2496:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-44(%rbp), %r13
	pushq	%rbx
	movq	%r13, %rdx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	16(%rsi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rsi, 272(%rdi)
	movl	$5, %esi
	movq	(%r12), %rax
	movq	%r12, %rdi
	movl	$0, -44(%rbp)
	call	*192(%rax)
	movq	%r13, %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	movl	%eax, 280(%rbx)
	movq	(%r12), %rax
	call	*192(%rax)
	movb	$0, 285(%rbx)
	movq	%r13, %rsi
	movq	%r12, %rdi
	cmpl	$20, %eax
	sete	284(%rbx)
	movq	(%r12), %rax
	call	*240(%rax)
	movl	%eax, 288(%rbx)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L50
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L50:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2496:
	.size	_ZN6icu_6713UCollationPCE4initEP18UCollationElements, .-_ZN6icu_6713UCollationPCE4initEP18UCollationElements
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UCollationPCE4initEPNS_24CollationElementIteratorE
	.type	_ZN6icu_6713UCollationPCE4initEPNS_24CollationElementIteratorE, @function
_ZN6icu_6713UCollationPCE4initEPNS_24CollationElementIteratorE:
.LFB2497:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-44(%rbp), %r13
	pushq	%rbx
	movq	%r13, %rdx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	16(%rsi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rsi, 272(%rdi)
	movl	$5, %esi
	movq	(%r12), %rax
	movq	%r12, %rdi
	movl	$0, -44(%rbp)
	call	*192(%rax)
	movq	%r13, %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	movl	%eax, 280(%rbx)
	movq	(%r12), %rax
	call	*192(%rax)
	movb	$0, 285(%rbx)
	movq	%r13, %rsi
	movq	%r12, %rdi
	cmpl	$20, %eax
	sete	284(%rbx)
	movq	(%r12), %rax
	call	*240(%rax)
	movl	%eax, 288(%rbx)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L54
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L54:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2497:
	.size	_ZN6icu_6713UCollationPCE4initEPNS_24CollationElementIteratorE, .-_ZN6icu_6713UCollationPCE4initEPNS_24CollationElementIteratorE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UCollationPCE4initERKNS_8CollatorE
	.type	_ZN6icu_6713UCollationPCE4initERKNS_8CollatorE, @function
_ZN6icu_6713UCollationPCE4initERKNS_8CollatorE:
.LFB2498:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-44(%rbp), %r13
	movq	%rsi, %r12
	pushq	%rbx
	movq	%r13, %rdx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movl	$0, -44(%rbp)
	movl	$5, %esi
	call	*192(%rax)
	movq	%r13, %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	movl	%eax, 280(%rbx)
	movq	(%r12), %rax
	call	*192(%rax)
	movb	$0, 285(%rbx)
	movq	%r13, %rsi
	movq	%r12, %rdi
	cmpl	$20, %eax
	sete	284(%rbx)
	movq	(%r12), %rax
	call	*240(%rax)
	movl	%eax, 288(%rbx)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L58
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L58:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2498:
	.size	_ZN6icu_6713UCollationPCE4initERKNS_8CollatorE, .-_ZN6icu_6713UCollationPCE4initERKNS_8CollatorE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UCollationPCED2Ev
	.type	_ZN6icu_6713UCollationPCED2Ev, @function
_ZN6icu_6713UCollationPCED2Ev:
.LFB2500:
	.cfi_startproc
	endbr64
	movq	256(%rdi), %r8
	cmpq	%rdi, %r8
	je	.L59
	movq	%r8, %rdi
	jmp	uprv_free_67@PLT
	.p2align 4,,10
	.p2align 3
.L59:
	ret
	.cfi_endproc
.LFE2500:
	.size	_ZN6icu_6713UCollationPCED2Ev, .-_ZN6icu_6713UCollationPCED2Ev
	.globl	_ZN6icu_6713UCollationPCED1Ev
	.set	_ZN6icu_6713UCollationPCED1Ev,_ZN6icu_6713UCollationPCED2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UCollationPCE9processCEEj
	.type	_ZN6icu_6713UCollationPCE9processCEEj, @function
_ZN6icu_6713UCollationPCE9processCEEj:
.LFB2502:
	.cfi_startproc
	endbr64
	movl	%esi, %edx
	movl	280(%rdi), %esi
	testl	%esi, %esi
	je	.L74
	movl	$0, %ecx
	movzbl	%dl, %eax
	cmpl	$1, %esi
	cmove	%rcx, %rax
	movzbl	%dh, %ecx
.L62:
	movl	%edx, %r9d
	shrl	$16, %r9d
	cmpb	$0, 284(%rdi)
	movl	%r9d, %r8d
	je	.L65
	cmpl	%edx, 288(%rdi)
	jbe	.L65
	testl	%r9d, %r9d
	je	.L83
	cmpl	$2, %esi
	movl	$0, %eax
	movb	$1, 285(%rdi)
	cmovle	%rax, %r8
	movq	%r8, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	cmpb	$0, 285(%rdi)
	je	.L70
	testl	%r9d, %r9d
	je	.L78
.L70:
	cmpl	$3, %esi
	movl	$0, %edx
	movl	$65535, %esi
	movb	$0, 285(%rdi)
	cmovge	%rsi, %rdx
	salq	$32, %rcx
	salq	$16, %rax
	salq	$48, %r8
	orq	%rcx, %rax
	orq	%r8, %rax
	orq	%rdx, %rax
	movq	%rax, %r8
	movq	%r8, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L83:
	cmpb	$0, 285(%rdi)
	je	.L70
.L78:
	xorl	%r8d, %r8d
	movq	%r8, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L74:
	xorl	%eax, %eax
	xorl	%ecx, %ecx
	jmp	.L62
	.cfi_endproc
.LFE2502:
	.size	_ZN6icu_6713UCollationPCE9processCEEj, .-_ZN6icu_6713UCollationPCE9processCEEj
	.p2align 4
	.globl	ucol_reset_67
	.type	ucol_reset_67, @function
ucol_reset_67:
.LFB2505:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_6724CollationElementIterator5resetEv@PLT
	.cfi_endproc
.LFE2505:
	.size	ucol_reset_67, .-ucol_reset_67
	.p2align 4
	.globl	ucol_next_67
	.type	ucol_next_67, @function
ucol_next_67:
.LFB2506:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L86
	jmp	_ZN6icu_6724CollationElementIterator4nextER10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L86:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE2506:
	.size	ucol_next_67, .-ucol_next_67
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UCollationPCE13nextProcessedEPiS1_P10UErrorCode
	.type	_ZN6icu_6713UCollationPCE13nextProcessedEPiS1_P10UErrorCode, @function
_ZN6icu_6713UCollationPCE13nextProcessedEPiS1_P10UErrorCode:
.LFB2507:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movabsq	$9223372036854775807, %rcx
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	0(%r13), %eax
	movq	%rsi, -56(%rbp)
	movq	%rdx, -64(%rbp)
	testl	%eax, %eax
	jg	.L87
	movl	$0, 264(%rdi)
	movq	%rdi, %r14
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L89:
	movq	272(%r14), %rdi
	call	_ZNK6icu_6724CollationElementIterator9getOffsetEv@PLT
	movq	272(%r14), %rdi
	movq	%r13, %rsi
	movl	%eax, %ebx
	call	_ZN6icu_6724CollationElementIterator4nextER10UErrorCode@PLT
	movq	272(%r14), %rdi
	movl	%eax, %r15d
	call	_ZNK6icu_6724CollationElementIterator9getOffsetEv@PLT
	cmpl	$-1, %r15d
	je	.L103
	movl	280(%r14), %edi
	testl	%edi, %edi
	je	.L104
	movzbl	%r15b, %esi
	cmpl	$1, %edi
	movl	%r15d, %edx
	cmove	%r12, %rsi
	movzbl	%dh, %ecx
.L91:
	movl	%r15d, %r10d
	shrl	$16, %r10d
	cmpb	$0, 284(%r14)
	movl	%r10d, %r9d
	je	.L93
	cmpl	288(%r14), %r15d
	jnb	.L93
	testl	%r10d, %r10d
	jne	.L94
	cmpb	$0, 285(%r14)
	jne	.L89
	.p2align 4,,10
	.p2align 3
.L98:
	cmpl	$3, %edi
	movl	$65535, %edx
	movb	$0, 285(%r14)
	cmovl	%r12, %rdx
	salq	$32, %rcx
	salq	$16, %rsi
	orq	%rsi, %rcx
	salq	$48, %r9
	orq	%r9, %rcx
	orq	%rdx, %rcx
	je	.L89
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L93:
	cmpb	$0, 285(%r14)
	je	.L98
	testl	%r10d, %r10d
	jne	.L98
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L104:
	xorl	%esi, %esi
	xorl	%ecx, %ecx
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L94:
	cmpl	$2, %edi
	jg	.L99
	movb	$1, 285(%r14)
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L99:
	movb	$1, 285(%r14)
	movq	%r9, %rcx
.L90:
	movq	-56(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L101
	movl	%ebx, (%rdx)
.L101:
	movq	-64(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L87
	movl	%eax, (%rbx)
.L87:
	addq	$24, %rsp
	movq	%rcx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L103:
	.cfi_restore_state
	movabsq	$9223372036854775807, %rcx
	jmp	.L90
	.cfi_endproc
.LFE2507:
	.size	_ZN6icu_6713UCollationPCE13nextProcessedEPiS1_P10UErrorCode, .-_ZN6icu_6713UCollationPCE13nextProcessedEPiS1_P10UErrorCode
	.p2align 4
	.globl	ucol_previous_67
	.type	ucol_previous_67, @function
ucol_previous_67:
.LFB2508:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L122
	jmp	_ZN6icu_6724CollationElementIterator8previousER10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L122:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE2508:
	.size	ucol_previous_67, .-ucol_previous_67
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UCollationPCE17previousProcessedEPiS1_P10UErrorCode
	.type	_ZN6icu_6713UCollationPCE17previousProcessedEPiS1_P10UErrorCode, @function
_ZN6icu_6713UCollationPCE17previousProcessedEPiS1_P10UErrorCode:
.LFB2509:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$280, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -312(%rbp)
	movl	(%rcx), %edx
	movq	%rsi, -304(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L199
	movl	264(%rdi), %eax
	movq	%rdi, %rbx
	testl	%eax, %eax
	jg	.L131
	leaq	-272(%rbp), %rax
	movq	%rcx, %r12
	movq	%rax, -280(%rbp)
	.p2align 4,,10
	.p2align 3
.L160:
	movq	-280(%rbp), %rax
	movq	%rax, -80(%rbp)
	movabsq	$68719476736, %rax
	movq	%rax, -72(%rbp)
	.p2align 4,,10
	.p2align 3
.L186:
	movq	272(%rbx), %rdi
	call	_ZNK6icu_6724CollationElementIterator9getOffsetEv@PLT
	movq	272(%rbx), %rdi
	movq	%r12, %rsi
	movl	%eax, %r14d
	call	_ZN6icu_6724CollationElementIterator8previousER10UErrorCode@PLT
	movq	272(%rbx), %rdi
	movl	%eax, %r13d
	call	_ZNK6icu_6724CollationElementIterator9getOffsetEv@PLT
	movl	%eax, %r15d
	cmpl	$-1, %r13d
	je	.L200
	movl	(%r12), %ecx
	testl	%ecx, %ecx
	jg	.L140
	movl	-72(%rbp), %eax
	movl	-68(%rbp), %edx
	cmpl	%edx, %eax
	jge	.L133
	movq	-80(%rbp), %r8
	movslq	%eax, %rdx
	addl	$1, %eax
	leaq	(%rdx,%rdx,2), %rdx
	leaq	(%r8,%rdx,4), %rdx
	movl	%r13d, (%rdx)
	movl	%r15d, 4(%rdx)
	movl	%r14d, 8(%rdx)
	movl	%eax, -72(%rbp)
.L134:
	testl	$-65536, %r13d
	je	.L186
	andl	$192, %r13d
	cmpl	$192, %r13d
	je	.L186
	movq	%r8, %r15
.L139:
	movl	$65535, %r14d
.L137:
	testl	%ecx, %ecx
	jg	.L140
	movslq	-72(%rbp), %rax
	movq	%rax, %rsi
	leaq	(%rax,%rax,2), %rax
	leaq	-12(%r15,%rax,4), %r8
	jmp	.L141
	.p2align 4,,10
	.p2align 3
.L201:
	cmpl	288(%rbx), %edx
	jnb	.L146
	testl	%r9d, %r9d
	jne	.L147
	cmpb	$0, 285(%rbx)
	jne	.L149
	.p2align 4,,10
	.p2align 3
.L151:
	cmpl	$3, %edi
	movl	$0, %edx
	movb	$0, 285(%rbx)
	cmovge	%r14, %rdx
	salq	$32, %rax
	salq	$16, %r10
	salq	$48, %r13
	orq	%r10, %rax
	orq	%r13, %rax
	orq	%rdx, %rax
	movq	%rax, %r13
	jne	.L153
.L149:
	subq	$12, %r8
.L141:
	testl	%esi, %esi
	jle	.L142
	movl	280(%rbx), %edi
	subl	$1, %esi
	movl	%esi, -72(%rbp)
	movl	(%r8), %edx
	testl	%edi, %edi
	je	.L170
	movzbl	%dl, %eax
	cmpl	$1, %edi
	movl	$0, %r10d
	cmovne	%rax, %r10
	movzbl	%dh, %eax
.L143:
	movl	%edx, %r9d
	shrl	$16, %r9d
	cmpb	$0, 284(%rbx)
	movl	%r9d, %r13d
	jne	.L201
.L146:
	cmpb	$0, 285(%rbx)
	je	.L151
	testl	%r9d, %r9d
	jne	.L151
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L170:
	xorl	%r10d, %r10d
	xorl	%eax, %eax
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L147:
	movb	$1, 285(%rbx)
	cmpl	$2, %edi
	jle	.L149
.L153:
	movl	264(%rbx), %eax
	movl	268(%rbx), %edx
	movq	4(%r8), %r9
	cmpl	%edx, %eax
	jge	.L155
	movq	256(%rbx), %r8
.L156:
	movslq	%eax, %rdx
	addl	$1, %eax
	salq	$4, %rdx
	addq	%rdx, %r8
	movq	%r13, (%r8)
	movq	%r9, 8(%r8)
	movl	%eax, 264(%rbx)
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L133:
	addl	$8, %edx
	movslq	%edx, %rdx
	leaq	(%rdx,%rdx,2), %rdi
	salq	$2, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L198
	movslq	-68(%rbp), %rax
	movq	-80(%rbp), %r9
	movq	%r8, %rdi
	leaq	(%rax,%rax,2), %rdx
	movq	%r9, %rsi
	movq	%r9, -288(%rbp)
	salq	$2, %rdx
	movl	%eax, -296(%rbp)
	call	memcpy@PLT
	movq	-288(%rbp), %r9
	cmpq	-280(%rbp), %r9
	movl	-296(%rbp), %ecx
	movq	%rax, %r8
	je	.L136
	movq	%r9, %rdi
	movq	%rax, -288(%rbp)
	call	uprv_free_67@PLT
	movl	-68(%rbp), %ecx
	movq	-288(%rbp), %r8
.L136:
	movslq	-72(%rbp), %rdx
	leal	8(%rcx), %eax
	movl	(%r12), %ecx
	movq	%r8, -80(%rbp)
	movl	%eax, -68(%rbp)
	movq	%rdx, %rax
	leaq	(%rdx,%rdx,2), %rdx
	leaq	(%r8,%rdx,4), %rdx
	addl	$1, %eax
	movl	%r13d, (%rdx)
	movl	%r15d, 4(%rdx)
	movl	%r14d, 8(%rdx)
	movl	%eax, -72(%rbp)
	testl	%ecx, %ecx
	jle	.L134
	.p2align 4,,10
	.p2align 3
.L140:
	movq	-80(%rbp), %rdi
	cmpq	-280(%rbp), %rdi
	je	.L199
	call	uprv_free_67@PLT
	.p2align 4,,10
	.p2align 3
.L199:
	movabsq	$9223372036854775807, %rax
.L123:
	movq	-56(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L202
	addq	$280, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L142:
	.cfi_restore_state
	cmpq	-280(%rbp), %r15
	je	.L203
	movq	%r15, %rdi
	call	uprv_free_67@PLT
	movl	264(%rbx), %eax
	testl	%eax, %eax
	jle	.L160
.L131:
	subl	$1, %eax
	movq	-304(%rbp), %rsi
	movl	%eax, 264(%rbx)
	cltq
	salq	$4, %rax
	addq	256(%rbx), %rax
	testq	%rsi, %rsi
	je	.L163
	movl	8(%rax), %edx
	movl	%edx, (%rsi)
.L163:
	movq	-312(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L164
	movl	12(%rax), %edx
	movl	%edx, (%rsi)
.L164:
	movq	(%rax), %rax
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L203:
	movl	264(%rbx), %eax
	testl	%eax, %eax
	jle	.L160
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L200:
	movl	-72(%rbp), %eax
	movq	-80(%rbp), %r15
	testl	%eax, %eax
	jle	.L204
	movl	(%r12), %ecx
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L155:
	leal	8(%rdx), %edi
	movq	%r9, -288(%rbp)
	movslq	%edi, %rdi
	salq	$4, %rdi
	call	uprv_malloc_67@PLT
	movq	-288(%rbp), %r9
	testq	%rax, %rax
	je	.L198
	movq	256(%rbx), %r10
	movslq	268(%rbx), %rdx
	movq	%rax, %rdi
	movq	%r9, -296(%rbp)
	movq	%r10, %rsi
	movq	%rdx, %r15
	salq	$4, %rdx
	movq	%r10, -288(%rbp)
	call	memcpy@PLT
	movq	-288(%rbp), %r10
	movq	-296(%rbp), %r9
	movq	%rax, %r8
	cmpq	%r10, %rbx
	je	.L158
	movq	%r10, %rdi
	movq	%r9, -288(%rbp)
	movq	%rax, -296(%rbp)
	call	uprv_free_67@PLT
	movl	268(%rbx), %r15d
	movq	-296(%rbp), %r8
	movq	-288(%rbp), %r9
.L158:
	addl	$8, %r15d
	movq	%r8, 256(%rbx)
	movl	264(%rbx), %eax
	movl	%r15d, 268(%rbx)
	movl	(%r12), %ecx
	movq	-80(%rbp), %r15
	jmp	.L156
.L204:
	cmpq	-280(%rbp), %r15
	je	.L129
	movq	%r15, %rdi
	call	uprv_free_67@PLT
.L129:
	movl	264(%rbx), %eax
	testl	%eax, %eax
	jg	.L131
	movq	-304(%rbp), %rax
	testq	%rax, %rax
	je	.L161
	movl	$-1, (%rax)
.L161:
	movq	-312(%rbp), %rax
	testq	%rax, %rax
	je	.L199
	movl	$-1, (%rax)
	jmp	.L199
.L198:
	movl	$7, (%r12)
	jmp	.L140
.L202:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2509:
	.size	_ZN6icu_6713UCollationPCE17previousProcessedEPiS1_P10UErrorCode, .-_ZN6icu_6713UCollationPCE17previousProcessedEPiS1_P10UErrorCode
	.p2align 4
	.globl	ucol_getMaxExpansion_67
	.type	ucol_getMaxExpansion_67, @function
ucol_getMaxExpansion_67:
.LFB2510:
	.cfi_startproc
	endbr64
	jmp	_ZNK6icu_6724CollationElementIterator15getMaxExpansionEi@PLT
	.cfi_endproc
.LFE2510:
	.size	ucol_getMaxExpansion_67, .-ucol_getMaxExpansion_67
	.p2align 4
	.globl	ucol_getOffset_67
	.type	ucol_getOffset_67, @function
ucol_getOffset_67:
.LFB2512:
	.cfi_startproc
	endbr64
	jmp	_ZNK6icu_6724CollationElementIterator9getOffsetEv@PLT
	.cfi_endproc
.LFE2512:
	.size	ucol_getOffset_67, .-ucol_getOffset_67
	.p2align 4
	.globl	ucol_setOffset_67
	.type	ucol_setOffset_67, @function
ucol_setOffset_67:
.LFB2513:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jle	.L209
	ret
	.p2align 4,,10
	.p2align 3
.L209:
	jmp	_ZN6icu_6724CollationElementIterator9setOffsetEiR10UErrorCode@PLT
	.cfi_endproc
.LFE2513:
	.size	ucol_setOffset_67, .-ucol_setOffset_67
	.p2align 4
	.globl	ucol_primaryOrder_67
	.type	ucol_primaryOrder_67, @function
ucol_primaryOrder_67:
.LFB2514:
	.cfi_startproc
	endbr64
	movl	%edi, %eax
	shrl	$16, %eax
	ret
	.cfi_endproc
.LFE2514:
	.size	ucol_primaryOrder_67, .-ucol_primaryOrder_67
	.p2align 4
	.globl	ucol_secondaryOrder_67
	.type	ucol_secondaryOrder_67, @function
ucol_secondaryOrder_67:
.LFB2515:
	.cfi_startproc
	endbr64
	movl	%edi, %eax
	movzbl	%ah, %eax
	ret
	.cfi_endproc
.LFE2515:
	.size	ucol_secondaryOrder_67, .-ucol_secondaryOrder_67
	.p2align 4
	.globl	ucol_tertiaryOrder_67
	.type	ucol_tertiaryOrder_67, @function
ucol_tertiaryOrder_67:
.LFB2516:
	.cfi_startproc
	endbr64
	movzbl	%dil, %eax
	ret
	.cfi_endproc
.LFE2516:
	.size	ucol_tertiaryOrder_67, .-ucol_tertiaryOrder_67
	.p2align 4
	.globl	ucol_openElements_67
	.type	ucol_openElements_67, @function
ucol_openElements_67:
.LFB2503:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L213
	movq	%rcx, %r14
	testq	%rdi, %rdi
	je	.L215
	movq	%rsi, %rbx
	movl	%edx, %r13d
	testq	%rsi, %rsi
	jne	.L216
	testl	%edx, %edx
	jne	.L215
.L216:
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6717RuleBasedCollatorE(%rip), %rdx
	leaq	_ZTIN6icu_678CollatorE(%rip), %rsi
	call	__dynamic_cast@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L231
	movl	%r13d, %esi
	leaq	-128(%rbp), %r15
	movl	%r13d, %ecx
	movq	%rbx, -136(%rbp)
	leaq	-136(%rbp), %rdx
	shrl	$31, %esi
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	*312(%rax)
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L232
.L218:
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L213:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L233
	addq	$104, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L215:
	.cfi_restore_state
	movl	$1, (%r14)
	xorl	%r12d, %r12d
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L231:
	movl	$16, (%r14)
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L232:
	movl	$7, (%r14)
	jmp	.L218
.L233:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2503:
	.size	ucol_openElements_67, .-ucol_openElements_67
	.p2align 4
	.globl	ucol_setText_67
	.type	ucol_setText_67, @function
ucol_setText_67:
.LFB2511:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$104, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L234
	movq	%rdi, %r13
	movq	%rcx, %r12
	testq	%rsi, %rsi
	jne	.L236
	testl	%edx, %edx
	je	.L236
	movl	$1, (%rcx)
.L234:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L245
	addq	$104, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L236:
	.cfi_restore_state
	movq	%rsi, -120(%rbp)
	leaq	-120(%rbp), %r8
	movl	%edx, %esi
	leaq	-112(%rbp), %r14
	movl	%edx, %ecx
	shrl	$31, %esi
	movq	%r8, %rdx
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	%r13, %rdi
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	_ZN6icu_6724CollationElementIterator7setTextERKNS_13UnicodeStringER10UErrorCode@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L234
.L245:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2511:
	.size	ucol_setText_67, .-ucol_setText_67
	.p2align 4
	.globl	ucol_closeElements_67
	.type	ucol_closeElements_67, @function
ucol_closeElements_67:
.LFB2504:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L246
	jmp	_ZN6icu_6724CollationElementIteratorD0Ev@PLT
	.p2align 4,,10
	.p2align 3
.L246:
	ret
	.cfi_endproc
.LFE2504:
	.size	ucol_closeElements_67, .-ucol_closeElements_67
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
