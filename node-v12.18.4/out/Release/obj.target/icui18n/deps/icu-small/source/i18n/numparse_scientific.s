	.file	"numparse_scientific.cpp"
	.text
	.section	.text._ZNK6icu_678numparse4impl18NumberParseMatcher10isFlexibleEv,"axG",@progbits,_ZNK6icu_678numparse4impl18NumberParseMatcher10isFlexibleEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_678numparse4impl18NumberParseMatcher10isFlexibleEv
	.type	_ZNK6icu_678numparse4impl18NumberParseMatcher10isFlexibleEv, @function
_ZNK6icu_678numparse4impl18NumberParseMatcher10isFlexibleEv:
.LFB2799:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2799:
	.size	_ZNK6icu_678numparse4impl18NumberParseMatcher10isFlexibleEv, .-_ZNK6icu_678numparse4impl18NumberParseMatcher10isFlexibleEv
	.section	.text._ZNK6icu_678numparse4impl18NumberParseMatcher11postProcessERNS1_12ParsedNumberE,"axG",@progbits,_ZNK6icu_678numparse4impl18NumberParseMatcher11postProcessERNS1_12ParsedNumberE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_678numparse4impl18NumberParseMatcher11postProcessERNS1_12ParsedNumberE
	.type	_ZNK6icu_678numparse4impl18NumberParseMatcher11postProcessERNS1_12ParsedNumberE, @function
_ZNK6icu_678numparse4impl18NumberParseMatcher11postProcessERNS1_12ParsedNumberE:
.LFB2800:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2800:
	.size	_ZNK6icu_678numparse4impl18NumberParseMatcher11postProcessERNS1_12ParsedNumberE, .-_ZNK6icu_678numparse4impl18NumberParseMatcher11postProcessERNS1_12ParsedNumberE
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678numparse4impl17ScientificMatcher9smokeTestERKNS_13StringSegmentE
	.type	_ZNK6icu_678numparse4impl17ScientificMatcher9smokeTestERKNS_13StringSegmentE, @function
_ZNK6icu_678numparse4impl17ScientificMatcher9smokeTestERKNS_13StringSegmentE:
.LFB2810:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	movq	%rsi, %rdi
	leaq	8(%r8), %rsi
	jmp	_ZNK6icu_6713StringSegment10startsWithERKNS_13UnicodeStringE@PLT
	.cfi_endproc
.LFE2810:
	.size	_ZNK6icu_678numparse4impl17ScientificMatcher9smokeTestERKNS_13StringSegmentE, .-_ZNK6icu_678numparse4impl17ScientificMatcher9smokeTestERKNS_13StringSegmentE
	.section	.rodata.str2.2,"aMS",@progbits,2
	.align 2
.LC0:
	.string	"<"
	.string	"S"
	.string	"c"
	.string	"i"
	.string	"e"
	.string	"n"
	.string	"t"
	.string	"i"
	.string	"f"
	.string	"i"
	.string	"c"
	.string	">"
	.string	""
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678numparse4impl17ScientificMatcher8toStringEv
	.type	_ZNK6icu_678numparse4impl17ScientificMatcher8toStringEv, @function
_ZNK6icu_678numparse4impl17ScientificMatcher8toStringEv:
.LFB2811:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2811:
	.size	_ZNK6icu_678numparse4impl17ScientificMatcher8toStringEv, .-_ZNK6icu_678numparse4impl17ScientificMatcher8toStringEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678numparse4impl17ScientificMatcher5matchERNS_13StringSegmentERNS1_12ParsedNumberER10UErrorCode
	.type	_ZNK6icu_678numparse4impl17ScientificMatcher5matchERNS_13StringSegmentERNS1_12ParsedNumberER10UErrorCode, @function
_ZNK6icu_678numparse4impl17ScientificMatcher5matchERNS_13StringSegmentERNS1_12ParsedNumberER10UErrorCode:
.LFB2809:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rdx, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$40, %rsp
	call	_ZNK6icu_678numparse4impl12ParsedNumber10seenNumberEv@PLT
	movl	%eax, %r15d
	testb	%al, %al
	je	.L7
	testb	$8, 76(%rbx)
	je	.L34
	xorl	%r15d, %r15d
.L7:
	addq	$40, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK6icu_6713StringSegment9getOffsetEv@PLT
	leaq	8(%r13), %rsi
	movq	%r12, %rdi
	movl	%eax, -52(%rbp)
	call	_ZN6icu_6713StringSegment21getCommonPrefixLengthERKNS_13UnicodeStringE@PLT
	movl	%eax, %esi
	movswl	16(%r13), %eax
	testw	%ax, %ax
	js	.L9
	sarl	$5, %eax
	cmpl	%eax, %esi
	je	.L35
.L11:
	movq	%r12, %rdi
	movl	%esi, -52(%rbp)
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	movl	-52(%rbp), %esi
	cmpl	%eax, %esi
	sete	%r15b
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L9:
	movl	20(%r13), %eax
	cmpl	%eax, %esi
	jne	.L11
.L35:
	movq	%r12, %rdi
	movl	%esi, -64(%rbp)
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	movl	-64(%rbp), %esi
	cmpl	%eax, %esi
	je	.L7
	movq	%r12, %rdi
	call	_ZN6icu_6713StringSegment12adjustOffsetEi@PLT
	movq	%r14, %rcx
	movq	%rbx, %rdx
	movq	%r12, %rsi
	leaq	272(%r13), %rax
	movq	%rax, %rdi
	movq	%rax, -64(%rbp)
	call	_ZNK6icu_678numparse4impl13SymbolMatcher5matchERNS_13StringSegmentERNS1_12ParsedNumberER10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	testl	%eax, %eax
	je	.L25
	movl	$11, %edi
	call	_ZN6icu_677unisets3getENS0_3KeyE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZNK6icu_6713StringSegment10startsWithERKNS_10UnicodeSetE@PLT
	testb	%al, %al
	je	.L13
	movq	%r12, %rdi
	call	_ZN6icu_6713StringSegment23adjustOffsetByCodePointEv@PLT
	movl	$-1, -56(%rbp)
.L14:
	movq	%r12, %rdi
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	testl	%eax, %eax
	je	.L25
	movq	-64(%rbp), %rdi
	movq	%r14, %rcx
	movq	%rbx, %rdx
	movq	%r12, %rsi
	call	_ZNK6icu_678numparse4impl13SymbolMatcher5matchERNS_13StringSegmentERNS1_12ParsedNumberER10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	testl	%eax, %eax
	je	.L25
	movzbl	8(%rbx), %r9d
	movb	$0, 8(%rbx)
	movq	%r12, %rdi
	movb	%r9b, -72(%rbp)
	call	_ZNK6icu_6713StringSegment9getOffsetEv@PLT
	movl	-56(%rbp), %ecx
	leaq	72(%r13), %rdi
	movq	%r14, %r8
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movl	%eax, -64(%rbp)
	call	_ZNK6icu_678numparse4impl14DecimalMatcher5matchERNS_13StringSegmentERNS1_12ParsedNumberEaR10UErrorCode@PLT
	movzbl	-72(%rbp), %r9d
	movq	%r12, %rdi
	movl	%eax, %r15d
	movb	%r9b, 8(%rbx)
	call	_ZNK6icu_6713StringSegment9getOffsetEv@PLT
	cmpl	%eax, -64(%rbp)
	je	.L25
	orl	$8, 76(%rbx)
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L25:
	movl	-52(%rbp), %esi
	movq	%r12, %rdi
	call	_ZN6icu_6713StringSegment9setOffsetEi@PLT
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L13:
	movl	$12, %edi
	call	_ZN6icu_677unisets3getENS0_3KeyE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZNK6icu_6713StringSegment10startsWithERKNS_10UnicodeSetE@PLT
	testb	%al, %al
	je	.L15
	movq	%r12, %rdi
	call	_ZN6icu_6713StringSegment23adjustOffsetByCodePointEv@PLT
	movl	$1, -56(%rbp)
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L15:
	leaq	352(%r13), %rsi
	movq	%r12, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZNK6icu_6713StringSegment10startsWithERKNS_13UnicodeStringE@PLT
	testb	%al, %al
	movb	%al, -56(%rbp)
	je	.L16
	movq	-72(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713StringSegment21getCommonPrefixLengthERKNS_13UnicodeStringE@PLT
	movzbl	-56(%rbp), %edx
	movl	%eax, %esi
	movswl	360(%r13), %eax
	testw	%ax, %ax
	js	.L17
	sarl	$5, %eax
.L18:
	movb	%dl, -56(%rbp)
	cmpl	%eax, %esi
	jne	.L33
	movq	%r12, %rdi
	call	_ZN6icu_6713StringSegment12adjustOffsetEi@PLT
	movl	$-1, -56(%rbp)
	jmp	.L14
.L16:
	leaq	416(%r13), %rsi
	movq	%r12, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZNK6icu_6713StringSegment10startsWithERKNS_13UnicodeStringE@PLT
	movl	$1, -56(%rbp)
	movq	-72(%rbp), %rsi
	testb	%al, %al
	je	.L14
	movq	%r12, %rdi
	movb	%al, -56(%rbp)
	call	_ZN6icu_6713StringSegment21getCommonPrefixLengthERKNS_13UnicodeStringE@PLT
	movzbl	-56(%rbp), %edx
	movl	%eax, %esi
	movswl	424(%r13), %eax
	testw	%ax, %ax
	js	.L20
	sarl	$5, %eax
.L21:
	movb	%dl, -56(%rbp)
	cmpl	%eax, %esi
	jne	.L33
	movq	%r12, %rdi
	call	_ZN6icu_6713StringSegment12adjustOffsetEi@PLT
	movl	$1, -56(%rbp)
	jmp	.L14
.L33:
	movl	-52(%rbp), %esi
	movq	%r12, %rdi
	call	_ZN6icu_6713StringSegment9setOffsetEi@PLT
	movzbl	-56(%rbp), %edx
	movl	%edx, %r15d
	jmp	.L7
.L17:
	movl	364(%r13), %eax
	jmp	.L18
.L20:
	movl	428(%r13), %eax
	jmp	.L21
	.cfi_endproc
.LFE2809:
	.size	_ZNK6icu_678numparse4impl17ScientificMatcher5matchERNS_13StringSegmentERNS1_12ParsedNumberER10UErrorCode, .-_ZNK6icu_678numparse4impl17ScientificMatcher5matchERNS_13StringSegmentERNS1_12ParsedNumberER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_678numparse4impl17ScientificMatcherC2ERKNS_20DecimalFormatSymbolsERKNS_6number4impl7GrouperE
	.type	_ZN6icu_678numparse4impl17ScientificMatcherC2ERKNS_20DecimalFormatSymbolsERKNS_6number4impl7GrouperE, @function
_ZN6icu_678numparse4impl17ScientificMatcherC2ERKNS_20DecimalFormatSymbolsERKNS_6number4impl7GrouperE:
.LFB2807:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_678numparse4impl17ScientificMatcherE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	addq	$712, %rsi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$8, %rdi
	leaq	392(%r12), %r14
	leaq	352(%rbx), %r15
	subq	$8, %rsp
	movq	%rax, -8(%rdi)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	%r13, %rdx
	movl	$48, %ecx
	movq	%r12, %rsi
	leaq	72(%rbx), %rdi
	leaq	416(%rbx), %r13
	call	_ZN6icu_678numparse4impl14DecimalMatcherC1ERKNS_20DecimalFormatSymbolsERKNS_6number4impl7GrouperEi@PLT
	movl	$32768, %esi
	leaq	272(%rbx), %rdi
	call	_ZN6icu_678numparse4impl17IgnorablesMatcherC1Ei@PLT
	movl	$2, %edx
	movl	$2, %ecx
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, 352(%rbx)
	movl	$11, %edi
	movw	%dx, 360(%rbx)
	movq	%rax, 416(%rbx)
	movw	%cx, 424(%rbx)
	call	_ZN6icu_677unisets3getENS0_3KeyE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZNK6icu_6710UnicodeSet8containsERKNS_13UnicodeStringE@PLT
	testb	%al, %al
	je	.L37
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
.L38:
	movl	$12, %edi
	addq	$456, %r12
	call	_ZN6icu_677unisets3getENS0_3KeyE@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZNK6icu_6710UnicodeSet8containsERKNS_13UnicodeStringE@PLT
	testb	%al, %al
	je	.L39
	addq	$8, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	.p2align 4,,10
	.p2align 3
.L37:
	.cfi_restore_state
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L39:
	addq	$8, %rsp
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	.cfi_endproc
.LFE2807:
	.size	_ZN6icu_678numparse4impl17ScientificMatcherC2ERKNS_20DecimalFormatSymbolsERKNS_6number4impl7GrouperE, .-_ZN6icu_678numparse4impl17ScientificMatcherC2ERKNS_20DecimalFormatSymbolsERKNS_6number4impl7GrouperE
	.globl	_ZN6icu_678numparse4impl17ScientificMatcherC1ERKNS_20DecimalFormatSymbolsERKNS_6number4impl7GrouperE
	.set	_ZN6icu_678numparse4impl17ScientificMatcherC1ERKNS_20DecimalFormatSymbolsERKNS_6number4impl7GrouperE,_ZN6icu_678numparse4impl17ScientificMatcherC2ERKNS_20DecimalFormatSymbolsERKNS_6number4impl7GrouperE
	.section	.text._ZN6icu_678numparse4impl17ScientificMatcherD0Ev,"axG",@progbits,_ZN6icu_678numparse4impl17ScientificMatcherD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_678numparse4impl17ScientificMatcherD0Ev
	.type	_ZN6icu_678numparse4impl17ScientificMatcherD0Ev, @function
_ZN6icu_678numparse4impl17ScientificMatcherD0Ev:
.LFB3898:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_678numparse4impl17ScientificMatcherE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	addq	$416, %rdi
	pushq	%rbx
	leaq	272(%r12), %r13
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	%rax, -416(%rdi)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	352(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	16+_ZTVN6icu_678numparse4impl13SymbolMatcherE(%rip), %rax
	leaq	280(%r12), %rdi
	movq	%rax, 272(%r12)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	leaq	72(%r12), %r13
	call	_ZN6icu_678numparse4impl18NumberParseMatcherD2Ev@PLT
	leaq	16+_ZTVN6icu_678numparse4impl14DecimalMatcherE(%rip), %rax
	movq	%rax, 72(%r12)
	movq	264(%r12), %rax
	testq	%rax, %rax
	je	.L42
	movq	-8(%rax), %rbx
	salq	$6, %rbx
	addq	%rax, %rbx
	cmpq	%rbx, %rax
	je	.L43
	.p2align 4,,10
	.p2align 3
.L44:
	movq	-64(%rbx), %rax
	subq	$64, %rbx
	movq	%rbx, %rdi
	call	*(%rax)
	cmpq	%rbx, 264(%r12)
	jne	.L44
.L43:
	leaq	-8(%rbx), %rdi
	call	_ZN6icu_677UMemorydaEPv@PLT
.L42:
	movq	256(%r12), %rdi
	testq	%rdi, %rdi
	je	.L45
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
.L45:
	movq	248(%r12), %rdi
	testq	%rdi, %rdi
	je	.L46
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
.L46:
	leaq	152(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	88(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_678numparse4impl18NumberParseMatcherD2Ev@PLT
	leaq	8(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_678numparse4impl18NumberParseMatcherD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3898:
	.size	_ZN6icu_678numparse4impl17ScientificMatcherD0Ev, .-_ZN6icu_678numparse4impl17ScientificMatcherD0Ev
	.section	.text._ZN6icu_678numparse4impl17ScientificMatcherD2Ev,"axG",@progbits,_ZN6icu_678numparse4impl17ScientificMatcherD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_678numparse4impl17ScientificMatcherD2Ev
	.type	_ZN6icu_678numparse4impl17ScientificMatcherD2Ev, @function
_ZN6icu_678numparse4impl17ScientificMatcherD2Ev:
.LFB3896:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_678numparse4impl17ScientificMatcherE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	addq	$416, %rdi
	pushq	%rbx
	leaq	272(%r12), %r13
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	%rax, -416(%rdi)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	352(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	16+_ZTVN6icu_678numparse4impl13SymbolMatcherE(%rip), %rax
	leaq	280(%r12), %rdi
	movq	%rax, 272(%r12)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	leaq	72(%r12), %r13
	call	_ZN6icu_678numparse4impl18NumberParseMatcherD2Ev@PLT
	leaq	16+_ZTVN6icu_678numparse4impl14DecimalMatcherE(%rip), %rax
	movq	%rax, 72(%r12)
	movq	264(%r12), %rax
	testq	%rax, %rax
	je	.L59
	movq	-8(%rax), %rbx
	salq	$6, %rbx
	addq	%rax, %rbx
	cmpq	%rbx, %rax
	je	.L60
	.p2align 4,,10
	.p2align 3
.L61:
	movq	-64(%rbx), %rax
	subq	$64, %rbx
	movq	%rbx, %rdi
	call	*(%rax)
	cmpq	%rbx, 264(%r12)
	jne	.L61
.L60:
	leaq	-8(%rbx), %rdi
	call	_ZN6icu_677UMemorydaEPv@PLT
.L59:
	movq	256(%r12), %rdi
	testq	%rdi, %rdi
	je	.L62
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
.L62:
	movq	248(%r12), %rdi
	testq	%rdi, %rdi
	je	.L63
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
.L63:
	leaq	152(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	88(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_678numparse4impl18NumberParseMatcherD2Ev@PLT
	leaq	8(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_678numparse4impl18NumberParseMatcherD2Ev@PLT
	.cfi_endproc
.LFE3896:
	.size	_ZN6icu_678numparse4impl17ScientificMatcherD2Ev, .-_ZN6icu_678numparse4impl17ScientificMatcherD2Ev
	.weak	_ZN6icu_678numparse4impl17ScientificMatcherD1Ev
	.set	_ZN6icu_678numparse4impl17ScientificMatcherD1Ev,_ZN6icu_678numparse4impl17ScientificMatcherD2Ev
	.weak	_ZTSN6icu_677UMemoryE
	.section	.rodata._ZTSN6icu_677UMemoryE,"aG",@progbits,_ZTSN6icu_677UMemoryE,comdat
	.align 16
	.type	_ZTSN6icu_677UMemoryE, @object
	.size	_ZTSN6icu_677UMemoryE, 18
_ZTSN6icu_677UMemoryE:
	.string	"N6icu_677UMemoryE"
	.weak	_ZTIN6icu_677UMemoryE
	.section	.data.rel.ro._ZTIN6icu_677UMemoryE,"awG",@progbits,_ZTIN6icu_677UMemoryE,comdat
	.align 8
	.type	_ZTIN6icu_677UMemoryE, @object
	.size	_ZTIN6icu_677UMemoryE, 16
_ZTIN6icu_677UMemoryE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN6icu_677UMemoryE
	.weak	_ZTSN6icu_678numparse4impl17ScientificMatcherE
	.section	.rodata._ZTSN6icu_678numparse4impl17ScientificMatcherE,"aG",@progbits,_ZTSN6icu_678numparse4impl17ScientificMatcherE,comdat
	.align 32
	.type	_ZTSN6icu_678numparse4impl17ScientificMatcherE, @object
	.size	_ZTSN6icu_678numparse4impl17ScientificMatcherE, 43
_ZTSN6icu_678numparse4impl17ScientificMatcherE:
	.string	"N6icu_678numparse4impl17ScientificMatcherE"
	.weak	_ZTIN6icu_678numparse4impl17ScientificMatcherE
	.section	.data.rel.ro._ZTIN6icu_678numparse4impl17ScientificMatcherE,"awG",@progbits,_ZTIN6icu_678numparse4impl17ScientificMatcherE,comdat
	.align 8
	.type	_ZTIN6icu_678numparse4impl17ScientificMatcherE, @object
	.size	_ZTIN6icu_678numparse4impl17ScientificMatcherE, 56
_ZTIN6icu_678numparse4impl17ScientificMatcherE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN6icu_678numparse4impl17ScientificMatcherE
	.long	0
	.long	2
	.quad	_ZTIN6icu_678numparse4impl18NumberParseMatcherE
	.quad	2
	.quad	_ZTIN6icu_677UMemoryE
	.quad	2
	.weak	_ZTVN6icu_678numparse4impl17ScientificMatcherE
	.section	.data.rel.ro.local._ZTVN6icu_678numparse4impl17ScientificMatcherE,"awG",@progbits,_ZTVN6icu_678numparse4impl17ScientificMatcherE,comdat
	.align 8
	.type	_ZTVN6icu_678numparse4impl17ScientificMatcherE, @object
	.size	_ZTVN6icu_678numparse4impl17ScientificMatcherE, 72
_ZTVN6icu_678numparse4impl17ScientificMatcherE:
	.quad	0
	.quad	_ZTIN6icu_678numparse4impl17ScientificMatcherE
	.quad	_ZN6icu_678numparse4impl17ScientificMatcherD1Ev
	.quad	_ZN6icu_678numparse4impl17ScientificMatcherD0Ev
	.quad	_ZNK6icu_678numparse4impl18NumberParseMatcher10isFlexibleEv
	.quad	_ZNK6icu_678numparse4impl17ScientificMatcher5matchERNS_13StringSegmentERNS1_12ParsedNumberER10UErrorCode
	.quad	_ZNK6icu_678numparse4impl17ScientificMatcher9smokeTestERKNS_13StringSegmentE
	.quad	_ZNK6icu_678numparse4impl18NumberParseMatcher11postProcessERNS1_12ParsedNumberE
	.quad	_ZNK6icu_678numparse4impl17ScientificMatcher8toStringEv
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
