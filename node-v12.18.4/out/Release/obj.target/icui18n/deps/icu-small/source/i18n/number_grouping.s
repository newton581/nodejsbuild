	.file	"number_grouping.cpp"
	.text
	.section	.text.unlikely,"ax",@progbits
	.align 2
.LCOLDB0:
	.text
.LHOTB0:
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl7Grouper11forStrategyE23UNumberGroupingStrategy
	.type	_ZN6icu_676number4impl7Grouper11forStrategyE23UNumberGroupingStrategy, @function
_ZN6icu_676number4impl7Grouper11forStrategyE23UNumberGroupingStrategy:
.LFB2911:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	cmpl	$4, %edi
	ja	.L2
	leaq	.L4(%rip), %rdx
	movl	%edi, %edi
	movslq	(%rdx,%rdi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L4:
	.long	.L8-.L4
	.long	.L7-.L4
	.long	.L6-.L4
	.long	.L5-.L4
	.long	.L3-.L4
	.text
	.p2align 4,,10
	.p2align 3
.L5:
	movl	$1, %edx
	movl	$-196612, -12(%rbp)
	movw	%dx, -8(%rbp)
	movq	-12(%rbp), %rax
	movl	$3, -4(%rbp)
	movl	-4(%rbp), %edx
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	.cfi_restore_state
	movl	$1, %eax
	movl	$196611, -12(%rbp)
	movw	%ax, -8(%rbp)
	movq	-12(%rbp), %rax
	movl	$4, -4(%rbp)
	movl	-4(%rbp), %edx
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_restore_state
	movl	$-2, %edi
	movl	$-1, -12(%rbp)
	movw	%di, -8(%rbp)
	movq	-12(%rbp), %rax
	movl	$0, -4(%rbp)
	movl	-4(%rbp), %edx
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
	movl	$-3, %ecx
	movl	$-65538, -12(%rbp)
	movw	%cx, -8(%rbp)
	movq	-12(%rbp), %rax
	movl	$1, -4(%rbp)
	movl	-4(%rbp), %edx
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	movl	$-2, %esi
	movl	$-65538, -12(%rbp)
	movw	%si, -8(%rbp)
	movq	-12(%rbp), %rax
	movl	$2, -4(%rbp)
	movl	-4(%rbp), %edx
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_676number4impl7Grouper11forStrategyE23UNumberGroupingStrategy.cold, @function
_ZN6icu_676number4impl7Grouper11forStrategyE23UNumberGroupingStrategy.cold:
.LFSB2911:
.L2:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	call	abort@PLT
	.cfi_endproc
.LFE2911:
	.text
	.size	_ZN6icu_676number4impl7Grouper11forStrategyE23UNumberGroupingStrategy, .-_ZN6icu_676number4impl7Grouper11forStrategyE23UNumberGroupingStrategy
	.section	.text.unlikely
	.size	_ZN6icu_676number4impl7Grouper11forStrategyE23UNumberGroupingStrategy.cold, .-_ZN6icu_676number4impl7Grouper11forStrategyE23UNumberGroupingStrategy.cold
.LCOLDE0:
	.text
.LHOTE0:
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl7Grouper13forPropertiesERKNS1_23DecimalFormatPropertiesE
	.type	_ZN6icu_676number4impl7Grouper13forPropertiesERKNS1_23DecimalFormatPropertiesE, @function
_ZN6icu_676number4impl7Grouper13forPropertiesERKNS1_23DecimalFormatPropertiesE:
.LFB2912:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpb	$0, 76(%rdi)
	je	.L19
	movl	72(%rdi), %eax
	movl	752(%rdi), %ecx
	movl	104(%rdi), %edi
	movl	%eax, %esi
	movl	%ecx, %edx
	testw	%ax, %ax
	jle	.L20
	testw	%cx, %cx
	cmovle	%eax, %edx
.L16:
	movw	%dx, -10(%rbp)
	movw	%si, -12(%rbp)
	movw	%di, -8(%rbp)
	movq	-12(%rbp), %rax
	movl	$5, -4(%rbp)
	movl	-4(%rbp), %edx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	testw	%cx, %cx
	cmovle	%eax, %edx
	cmovg	%ecx, %esi
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L19:
	movl	$-2, %eax
	movl	$-1, -12(%rbp)
	movw	%ax, -8(%rbp)
	movq	-12(%rbp), %rax
	movl	$0, -4(%rbp)
	movl	-4(%rbp), %edx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2912:
	.size	_ZN6icu_676number4impl7Grouper13forPropertiesERKNS1_23DecimalFormatPropertiesE, .-_ZN6icu_676number4impl7Grouper13forPropertiesERKNS1_23DecimalFormatPropertiesE
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"NumberElements/minimumGroupingDigits"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl7Grouper13setLocaleDataERKNS1_17ParsedPatternInfoERKNS_6LocaleE
	.type	_ZN6icu_676number4impl7Grouper13setLocaleDataERKNS1_17ParsedPatternInfoERKNS_6LocaleE, @function
_ZN6icu_676number4impl7Grouper13setLocaleDataERKNS1_17ParsedPatternInfoERKNS_6LocaleE:
.LFB2913:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movzwl	(%rdi), %edi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpw	$-2, %di
	je	.L22
	cmpw	$-4, 2(%rbx)
	jne	.L21
	movq	72(%rsi), %rcx
	movq	%rcx, %rsi
	movq	%rcx, %rax
	shrq	$16, %rsi
	shrq	$32, %rax
	movl	%esi, %r13d
	cmpw	$-1, %si
	je	.L24
.L33:
	movl	%ecx, %r12d
.L25:
	cmpw	$-1, %ax
	movzwl	4(%rbx), %eax
	cmove	%r12d, %r13d
	cmpw	$-2, %ax
	je	.L48
	cmpw	$-3, %ax
	je	.L49
.L30:
	movw	%r12w, (%rbx)
	movw	%r13w, 2(%rbx)
.L21:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L50
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	.cfi_restore_state
	movq	72(%rsi), %rcx
	movl	$-1, %r12d
	movq	%rcx, %rsi
	movq	%rcx, %rax
	shrq	$16, %rsi
	shrq	$32, %rax
	movl	%esi, %r13d
	cmpw	$-1, %si
	jne	.L33
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L48:
	movq	40(%rdx), %rsi
	leaq	-64(%rbp), %r15
	xorl	%edi, %edi
	movl	$0, -64(%rbp)
	movq	%r15, %rdx
	call	ures_open_67@PLT
	movq	%r15, %rcx
	leaq	-60(%rbp), %rdx
	leaq	.LC1(%rip), %rsi
	movq	%rax, %rdi
	movl	$0, -60(%rbp)
	movq	%rax, %r14
	movl	$1, %r15d
	call	ures_getStringByKeyWithFallback_67@PLT
	movl	-64(%rbp), %ecx
	testl	%ecx, %ecx
	jg	.L28
	cmpl	$1, -60(%rbp)
	je	.L51
.L28:
	testq	%r14, %r14
	je	.L29
	movq	%r14, %rdi
	call	ures_close_67@PLT
.L29:
	movw	%r15w, 4(%rbx)
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L24:
	xorl	%r12d, %r12d
	cmpw	$-4, %di
	sete	%r12b
	leal	-1(,%r12,4), %r12d
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L49:
	movq	40(%rdx), %rsi
	leaq	-64(%rbp), %r15
	xorl	%edi, %edi
	movl	$0, -64(%rbp)
	movq	%r15, %rdx
	call	ures_open_67@PLT
	leaq	-60(%rbp), %rdx
	movq	%r15, %rcx
	leaq	.LC1(%rip), %rsi
	movq	%rax, %rdi
	movl	$0, -60(%rbp)
	movq	%rax, %r14
	movl	$1, %r15d
	call	ures_getStringByKeyWithFallback_67@PLT
	movl	-64(%rbp), %edx
	testl	%edx, %edx
	jg	.L31
	cmpl	$1, -60(%rbp)
	je	.L52
.L31:
	testq	%r14, %r14
	je	.L32
	movq	%r14, %rdi
	call	ures_close_67@PLT
.L32:
	movl	%r15d, %esi
	movl	$2, %edi
	call	uprv_max_67@PLT
	movw	%ax, 4(%rbx)
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L51:
	movzwl	(%rax), %r15d
	subl	$48, %r15d
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L52:
	movzwl	(%rax), %r15d
	subl	$48, %r15d
	movswl	%r15w, %r15d
	jmp	.L31
.L50:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2913:
	.size	_ZN6icu_676number4impl7Grouper13setLocaleDataERKNS1_17ParsedPatternInfoERKNS_6LocaleE, .-_ZN6icu_676number4impl7Grouper13setLocaleDataERKNS1_17ParsedPatternInfoERKNS_6LocaleE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl7Grouper15groupAtPositionEiRKNS1_15DecimalQuantityE
	.type	_ZNK6icu_676number4impl7Grouper15groupAtPositionEiRKNS1_15DecimalQuantityE, @function
_ZNK6icu_676number4impl7Grouper15groupAtPositionEiRKNS1_15DecimalQuantityE:
.LFB2914:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movq	%rdx, %rdi
	subq	$8, %rsp
	movswl	(%rbx), %edx
	leal	1(%rdx), %ecx
	cmpw	$1, %cx
	jbe	.L53
	movl	%esi, %eax
	subl	%edx, %eax
	js	.L53
	movswl	2(%rbx), %ecx
	cltd
	idivl	%ecx
	testl	%edx, %edx
	je	.L59
.L53:
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore_state
	call	_ZNK6icu_676number4impl15DecimalQuantity24getUpperDisplayMagnitudeEv@PLT
	movswl	(%rbx), %edx
	subl	%edx, %eax
	movswl	4(%rbx), %edx
	addl	$1, %eax
	cmpl	%edx, %eax
	setge	%r8b
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2914:
	.size	_ZNK6icu_676number4impl7Grouper15groupAtPositionEiRKNS1_15DecimalQuantityE, .-_ZNK6icu_676number4impl7Grouper15groupAtPositionEiRKNS1_15DecimalQuantityE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl7Grouper10getPrimaryEv
	.type	_ZNK6icu_676number4impl7Grouper10getPrimaryEv, @function
_ZNK6icu_676number4impl7Grouper10getPrimaryEv:
.LFB2915:
	.cfi_startproc
	endbr64
	movzwl	(%rdi), %eax
	ret
	.cfi_endproc
.LFE2915:
	.size	_ZNK6icu_676number4impl7Grouper10getPrimaryEv, .-_ZNK6icu_676number4impl7Grouper10getPrimaryEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl7Grouper12getSecondaryEv
	.type	_ZNK6icu_676number4impl7Grouper12getSecondaryEv, @function
_ZNK6icu_676number4impl7Grouper12getSecondaryEv:
.LFB2916:
	.cfi_startproc
	endbr64
	movzwl	2(%rdi), %eax
	ret
	.cfi_endproc
.LFE2916:
	.size	_ZNK6icu_676number4impl7Grouper12getSecondaryEv, .-_ZNK6icu_676number4impl7Grouper12getSecondaryEv
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
