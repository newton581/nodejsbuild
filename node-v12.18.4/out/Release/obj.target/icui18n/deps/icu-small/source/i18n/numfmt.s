	.file	"numfmt.cpp"
	.text
	.section	.text._ZNK6icu_6712NumberFormat9isLenientEv,"axG",@progbits,_ZNK6icu_6712NumberFormat9isLenientEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6712NumberFormat9isLenientEv
	.type	_ZNK6icu_6712NumberFormat9isLenientEv, @function
_ZNK6icu_6712NumberFormat9isLenientEv:
.LFB2355:
	.cfi_startproc
	endbr64
	movzbl	341(%rdi), %eax
	ret
	.cfi_endproc
.LFE2355:
	.size	_ZNK6icu_6712NumberFormat9isLenientEv, .-_ZNK6icu_6712NumberFormat9isLenientEv
	.text
	.p2align 4
	.type	deleteNumberingSystem, @function
deleteNumberingSystem:
.LFB3996:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L3
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L3:
	ret
	.cfi_endproc
.LFE3996:
	.size	deleteNumberingSystem, .-deleteNumberingSystem
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712NumberFormat6formatEdRNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode
	.type	_ZNK6icu_6712NumberFormat6formatEdRNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode, @function
_ZNK6icu_6712NumberFormat6formatEdRNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode:
.LFB4015:
	.cfi_startproc
	endbr64
	movl	(%rcx), %edx
	movq	%rsi, %rax
	testl	%edx, %edx
	jle	.L7
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	movl	$16, (%rcx)
	ret
	.cfi_endproc
.LFE4015:
	.size	_ZNK6icu_6712NumberFormat6formatEdRNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode, .-_ZNK6icu_6712NumberFormat6formatEdRNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712NumberFormat6formatEiRNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode
	.type	_ZNK6icu_6712NumberFormat6formatEiRNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode, @function
_ZNK6icu_6712NumberFormat6formatEiRNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode:
.LFB4016:
	.cfi_startproc
	endbr64
	movq	%rdx, %rax
	movl	(%r8), %edx
	testl	%edx, %edx
	jle	.L10
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	movl	$16, (%r8)
	ret
	.cfi_endproc
.LFE4016:
	.size	_ZNK6icu_6712NumberFormat6formatEiRNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode, .-_ZNK6icu_6712NumberFormat6formatEiRNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712NumberFormat6formatElRNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode
	.type	_ZNK6icu_6712NumberFormat6formatElRNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode, @function
_ZNK6icu_6712NumberFormat6formatElRNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode:
.LFB4017:
	.cfi_startproc
	endbr64
	movq	%rdx, %rax
	movl	(%r8), %edx
	testl	%edx, %edx
	jle	.L13
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	movl	$16, (%r8)
	ret
	.cfi_endproc
.LFE4017:
	.size	_ZNK6icu_6712NumberFormat6formatElRNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode, .-_ZNK6icu_6712NumberFormat6formatElRNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712NumberFormat6formatElRNS_13UnicodeStringERNS_13FieldPositionE
	.type	_ZNK6icu_6712NumberFormat6formatElRNS_13UnicodeStringERNS_13FieldPositionE, @function
_ZNK6icu_6712NumberFormat6formatElRNS_13UnicodeStringERNS_13FieldPositionE:
.LFB4037:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*88(%rax)
	.cfi_endproc
.LFE4037:
	.size	_ZNK6icu_6712NumberFormat6formatElRNS_13UnicodeStringERNS_13FieldPositionE, .-_ZNK6icu_6712NumberFormat6formatElRNS_13UnicodeStringERNS_13FieldPositionE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712NumberFormat11parseObjectERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE
	.type	_ZNK6icu_6712NumberFormat11parseObjectERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE, @function
_ZNK6icu_6712NumberFormat11parseObjectERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE:
.LFB4038:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*160(%rax)
	.cfi_endproc
.LFE4038:
	.size	_ZNK6icu_6712NumberFormat11parseObjectERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE, .-_ZNK6icu_6712NumberFormat11parseObjectERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712NumberFormat19setParseIntegerOnlyEa
	.type	_ZN6icu_6712NumberFormat19setParseIntegerOnlyEa, @function
_ZN6icu_6712NumberFormat19setParseIntegerOnlyEa:
.LFB4044:
	.cfi_startproc
	endbr64
	movb	%sil, 340(%rdi)
	ret
	.cfi_endproc
.LFE4044:
	.size	_ZN6icu_6712NumberFormat19setParseIntegerOnlyEa, .-_ZN6icu_6712NumberFormat19setParseIntegerOnlyEa
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712NumberFormat10setLenientEa
	.type	_ZN6icu_6712NumberFormat10setLenientEa, @function
_ZN6icu_6712NumberFormat10setLenientEa:
.LFB4045:
	.cfi_startproc
	endbr64
	movb	%sil, 341(%rdi)
	ret
	.cfi_endproc
.LFE4045:
	.size	_ZN6icu_6712NumberFormat10setLenientEa, .-_ZN6icu_6712NumberFormat10setLenientEa
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712NumberFormat15setGroupingUsedEa
	.type	_ZN6icu_6712NumberFormat15setGroupingUsedEa, @function
_ZN6icu_6712NumberFormat15setGroupingUsedEa:
.LFB4058:
	.cfi_startproc
	endbr64
	movb	%sil, 322(%rdi)
	ret
	.cfi_endproc
.LFE4058:
	.size	_ZN6icu_6712NumberFormat15setGroupingUsedEa, .-_ZN6icu_6712NumberFormat15setGroupingUsedEa
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712NumberFormat10setContextE15UDisplayContextR10UErrorCode
	.type	_ZN6icu_6712NumberFormat10setContextE15UDisplayContextR10UErrorCode, @function
_ZN6icu_6712NumberFormat10setContextE15UDisplayContextR10UErrorCode:
.LFB4070:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L19
	movl	%esi, %eax
	shrl	$8, %eax
	cmpl	$1, %eax
	je	.L22
	movl	$1, (%rdx)
.L19:
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	movl	%esi, 352(%rdi)
	ret
	.cfi_endproc
.LFE4070:
	.size	_ZN6icu_6712NumberFormat10setContextE15UDisplayContextR10UErrorCode, .-_ZN6icu_6712NumberFormat10setContextE15UDisplayContextR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712NumberFormat10getContextE19UDisplayContextTypeR10UErrorCode
	.type	_ZNK6icu_6712NumberFormat10getContextE19UDisplayContextTypeR10UErrorCode, @function
_ZNK6icu_6712NumberFormat10getContextE19UDisplayContextTypeR10UErrorCode:
.LFB4071:
	.cfi_startproc
	endbr64
	movl	(%rdx), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L23
	cmpl	$1, %esi
	je	.L25
	movl	$1, (%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	movl	352(%rdi), %eax
.L23:
	ret
	.cfi_endproc
.LFE4071:
	.size	_ZNK6icu_6712NumberFormat10getContextE19UDisplayContextTypeR10UErrorCode, .-_ZNK6icu_6712NumberFormat10getContextE19UDisplayContextTypeR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712NumberFormat15getRoundingModeEv
	.type	_ZNK6icu_6712NumberFormat15getRoundingModeEv, @function
_ZNK6icu_6712NumberFormat15getRoundingModeEv:
.LFB4086:
	.cfi_startproc
	endbr64
	movl	$7, %eax
	ret
	.cfi_endproc
.LFE4086:
	.size	_ZNK6icu_6712NumberFormat15getRoundingModeEv, .-_ZNK6icu_6712NumberFormat15getRoundingModeEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712NumberFormat15setRoundingModeENS0_13ERoundingModeE
	.type	_ZN6icu_6712NumberFormat15setRoundingModeENS0_13ERoundingModeE, @function
_ZN6icu_6712NumberFormat15setRoundingModeENS0_13ERoundingModeE:
.LFB4087:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4087:
	.size	_ZN6icu_6712NumberFormat15setRoundingModeENS0_13ERoundingModeE, .-_ZN6icu_6712NumberFormat15setRoundingModeENS0_13ERoundingModeE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718SharedNumberFormatD2Ev
	.type	_ZN6icu_6718SharedNumberFormatD2Ev, @function
_ZN6icu_6718SharedNumberFormatD2Ev:
.LFB4007:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6718SharedNumberFormatE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L30
	movq	(%rdi), %rax
	call	*8(%rax)
.L30:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6712SharedObjectD2Ev@PLT
	.cfi_endproc
.LFE4007:
	.size	_ZN6icu_6718SharedNumberFormatD2Ev, .-_ZN6icu_6718SharedNumberFormatD2Ev
	.globl	_ZN6icu_6718SharedNumberFormatD1Ev
	.set	_ZN6icu_6718SharedNumberFormatD1Ev,_ZN6icu_6718SharedNumberFormatD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712NumberFormat23setMaximumIntegerDigitsEi
	.type	_ZN6icu_6712NumberFormat23setMaximumIntegerDigitsEi, @function
_ZN6icu_6712NumberFormat23setMaximumIntegerDigitsEi:
.LFB4060:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	%esi, %edi
	movl	$2000000000, %esi
	subq	$8, %rsp
	call	uprv_min_67@PLT
	xorl	%edi, %edi
	movl	%eax, %esi
	call	uprv_max_67@PLT
	movl	%eax, 324(%rbx)
	cmpl	328(%rbx), %eax
	jge	.L35
	movl	%eax, 328(%rbx)
.L35:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4060:
	.size	_ZN6icu_6712NumberFormat23setMaximumIntegerDigitsEi, .-_ZN6icu_6712NumberFormat23setMaximumIntegerDigitsEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712NumberFormat23setMinimumIntegerDigitsEi
	.type	_ZN6icu_6712NumberFormat23setMinimumIntegerDigitsEi, @function
_ZN6icu_6712NumberFormat23setMinimumIntegerDigitsEi:
.LFB4062:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	%esi, %edi
	movl	$127, %esi
	subq	$8, %rsp
	call	uprv_min_67@PLT
	xorl	%edi, %edi
	movl	%eax, %esi
	call	uprv_max_67@PLT
	movl	%eax, 328(%rbx)
	cmpl	324(%rbx), %eax
	jle	.L38
	movl	%eax, 324(%rbx)
.L38:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4062:
	.size	_ZN6icu_6712NumberFormat23setMinimumIntegerDigitsEi, .-_ZN6icu_6712NumberFormat23setMinimumIntegerDigitsEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712NumberFormat24setMaximumFractionDigitsEi
	.type	_ZN6icu_6712NumberFormat24setMaximumFractionDigitsEi, @function
_ZN6icu_6712NumberFormat24setMaximumFractionDigitsEi:
.LFB4064:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	%esi, %edi
	movl	$2000000000, %esi
	subq	$8, %rsp
	call	uprv_min_67@PLT
	xorl	%edi, %edi
	movl	%eax, %esi
	call	uprv_max_67@PLT
	movl	%eax, 332(%rbx)
	cmpl	336(%rbx), %eax
	jge	.L41
	movl	%eax, 336(%rbx)
.L41:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4064:
	.size	_ZN6icu_6712NumberFormat24setMaximumFractionDigitsEi, .-_ZN6icu_6712NumberFormat24setMaximumFractionDigitsEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712NumberFormat24setMinimumFractionDigitsEi
	.type	_ZN6icu_6712NumberFormat24setMinimumFractionDigitsEi, @function
_ZN6icu_6712NumberFormat24setMinimumFractionDigitsEi:
.LFB4066:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	%esi, %edi
	movl	$127, %esi
	subq	$8, %rsp
	call	uprv_min_67@PLT
	xorl	%edi, %edi
	movl	%eax, %esi
	call	uprv_max_67@PLT
	movl	%eax, 336(%rbx)
	cmpl	332(%rbx), %eax
	jle	.L44
	movl	%eax, 332(%rbx)
.L44:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4066:
	.size	_ZN6icu_6712NumberFormat24setMinimumFractionDigitsEi, .-_ZN6icu_6712NumberFormat24setMinimumFractionDigitsEi
	.section	.text._ZNK6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEE5cloneEv,"axG",@progbits,_ZNK6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEE5cloneEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEE5cloneEv
	.type	_ZNK6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEE5cloneEv, @function
_ZNK6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEE5cloneEv:
.LFB5695:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	$240, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L47
	movl	8(%rbx), %eax
	movb	$0, 12(%r12)
	leaq	16(%rbx), %rsi
	leaq	16(%r12), %rdi
	movl	%eax, 8(%r12)
	leaq	16+_ZTVN6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEEE(%rip), %rax
	movq	%rax, (%r12)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
.L47:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5695:
	.size	_ZNK6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEE5cloneEv, .-_ZNK6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEE5cloneEv
	.section	.text._ZNK6icu_678CacheKeyINS_18SharedNumberFormatEE16writeDescriptionEPci,"axG",@progbits,_ZNK6icu_678CacheKeyINS_18SharedNumberFormatEE16writeDescriptionEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_678CacheKeyINS_18SharedNumberFormatEE16writeDescriptionEPci
	.type	_ZNK6icu_678CacheKeyINS_18SharedNumberFormatEE16writeDescriptionEPci, @function
_ZNK6icu_678CacheKeyINS_18SharedNumberFormatEE16writeDescriptionEPci:
.LFB5700:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	leaq	_ZTSN6icu_6718SharedNumberFormatE(%rip), %rsi
	movq	%r8, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movslq	%edx, %rbx
	movq	%rbx, %rdx
	subq	$8, %rsp
	call	strncpy@PLT
	movb	$0, -1(%rax,%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5700:
	.size	_ZNK6icu_678CacheKeyINS_18SharedNumberFormatEE16writeDescriptionEPci, .-_ZNK6icu_678CacheKeyINS_18SharedNumberFormatEE16writeDescriptionEPci
	.section	.text._ZNK6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEE16writeDescriptionEPci,"axG",@progbits,_ZNK6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEE16writeDescriptionEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEE16writeDescriptionEPci
	.type	_ZNK6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEE16writeDescriptionEPci, @function
_ZNK6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEE16writeDescriptionEPci:
.LFB5697:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movslq	%edx, %rbx
	movq	%rbx, %rdx
	subq	$8, %rsp
	movq	56(%rdi), %rsi
	movq	%r8, %rdi
	call	strncpy@PLT
	movb	$0, -1(%rax,%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5697:
	.size	_ZNK6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEE16writeDescriptionEPci, .-_ZNK6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEE16writeDescriptionEPci
	.section	.text._ZNK6icu_678CacheKeyINS_18SharedNumberFormatEEeqERKNS_12CacheKeyBaseE,"axG",@progbits,_ZNK6icu_678CacheKeyINS_18SharedNumberFormatEEeqERKNS_12CacheKeyBaseE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_678CacheKeyINS_18SharedNumberFormatEEeqERKNS_12CacheKeyBaseE
	.type	_ZNK6icu_678CacheKeyINS_18SharedNumberFormatEEeqERKNS_12CacheKeyBaseE, @function
_ZNK6icu_678CacheKeyINS_18SharedNumberFormatEEeqERKNS_12CacheKeyBaseE:
.LFB5699:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	-8(%rax), %rax
	movq	8(%rax), %rdi
	movq	(%rsi), %rax
	movq	-8(%rax), %rax
	movq	8(%rax), %rsi
	movl	$1, %eax
	cmpq	%rsi, %rdi
	je	.L62
	xorl	%eax, %eax
	cmpb	$42, (%rdi)
	je	.L62
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	strcmp@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE5699:
	.size	_ZNK6icu_678CacheKeyINS_18SharedNumberFormatEEeqERKNS_12CacheKeyBaseE, .-_ZNK6icu_678CacheKeyINS_18SharedNumberFormatEEeqERKNS_12CacheKeyBaseE
	.section	.text._ZNK6icu_678CacheKeyINS_18SharedNumberFormatEE8hashCodeEv,"axG",@progbits,_ZNK6icu_678CacheKeyINS_18SharedNumberFormatEE8hashCodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_678CacheKeyINS_18SharedNumberFormatEE8hashCodeEv
	.type	_ZNK6icu_678CacheKeyINS_18SharedNumberFormatEE8hashCodeEv, @function
_ZNK6icu_678CacheKeyINS_18SharedNumberFormatEE8hashCodeEv:
.LFB5698:
	.cfi_startproc
	endbr64
	movl	$29, %esi
	leaq	_ZTSN6icu_6718SharedNumberFormatE(%rip), %rdi
	jmp	ustr_hashCharsN_67@PLT
	.cfi_endproc
.LFE5698:
	.size	_ZNK6icu_678CacheKeyINS_18SharedNumberFormatEE8hashCodeEv, .-_ZNK6icu_678CacheKeyINS_18SharedNumberFormatEE8hashCodeEv
	.section	.text._ZNK6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEE8hashCodeEv,"axG",@progbits,_ZNK6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEE8hashCodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEE8hashCodeEv
	.type	_ZNK6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEE8hashCodeEv, @function
_ZNK6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEE8hashCodeEv:
.LFB5694:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$29, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	_ZTSN6icu_6718SharedNumberFormatE(%rip), %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	call	ustr_hashCharsN_67@PLT
	leaq	16(%r12), %rdi
	movl	%eax, %ebx
	call	_ZNK6icu_676Locale8hashCodeEv@PLT
	movl	%eax, %r8d
	leal	(%rbx,%rbx,8), %eax
	leal	(%rbx,%rax,4), %eax
	popq	%rbx
	popq	%r12
	addl	%r8d, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5694:
	.size	_ZNK6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEE8hashCodeEv, .-_ZNK6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEE8hashCodeEv
	.text
	.p2align 4
	.type	numfmt_cleanup, @function
numfmt_cleanup:
.LFB3997:
	.cfi_startproc
	endbr64
	movl	$0, _ZL16gNSCacheInitOnce(%rip)
	mfence
	movq	_ZL21NumberingSystem_cache(%rip), %rdi
	testq	%rdi, %rdi
	je	.L74
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	uhash_close_67@PLT
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	$0, _ZL21NumberingSystem_cache(%rip)
	ret
	.p2align 4,,10
	.p2align 3
.L74:
	.cfi_restore 6
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3997:
	.size	numfmt_cleanup, .-numfmt_cleanup
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712NumberFormat6formatEdRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.type	_ZNK6icu_6712NumberFormat6formatEdRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode, @function
_ZNK6icu_6712NumberFormat6formatEdRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode:
.LFB4018:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L78
	movq	(%rdi), %rax
	jmp	*64(%rax)
	.p2align 4,,10
	.p2align 3
.L78:
	movq	%rsi, %rax
	ret
	.cfi_endproc
.LFE4018:
	.size	_ZNK6icu_6712NumberFormat6formatEdRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode, .-_ZNK6icu_6712NumberFormat6formatEdRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712NumberFormat6formatEiRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.type	_ZNK6icu_6712NumberFormat6formatEiRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode, @function
_ZNK6icu_6712NumberFormat6formatEiRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode:
.LFB4019:
	.cfi_startproc
	endbr64
	movl	(%r8), %eax
	testl	%eax, %eax
	jg	.L80
	movq	(%rdi), %rax
	jmp	*88(%rax)
	.p2align 4,,10
	.p2align 3
.L80:
	movq	%rdx, %rax
	ret
	.cfi_endproc
.LFE4019:
	.size	_ZNK6icu_6712NumberFormat6formatEiRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode, .-_ZNK6icu_6712NumberFormat6formatEiRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712NumberFormat6formatElRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.type	_ZNK6icu_6712NumberFormat6formatElRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode, @function
_ZNK6icu_6712NumberFormat6formatElRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode:
.LFB4020:
	.cfi_startproc
	endbr64
	movl	(%r8), %eax
	testl	%eax, %eax
	jg	.L82
	movq	(%rdi), %rax
	jmp	*112(%rax)
	.p2align 4,,10
	.p2align 3
.L82:
	movq	%rdx, %rax
	ret
	.cfi_endproc
.LFE4020:
	.size	_ZNK6icu_6712NumberFormat6formatElRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode, .-_ZNK6icu_6712NumberFormat6formatElRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712NumberFormat11setCurrencyEPKDsR10UErrorCode
	.type	_ZN6icu_6712NumberFormat11setCurrencyEPKDsR10UErrorCode, @function
_ZN6icu_6712NumberFormat11setCurrencyEPKDsR10UErrorCode:
.LFB4067:
	.cfi_startproc
	endbr64
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jg	.L87
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	testq	%rsi, %rsi
	je	.L85
	movl	$3, %edx
	leaq	342(%rdi), %rdi
	call	u_strncpy_67@PLT
	xorl	%edx, %edx
	movw	%dx, 348(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L85:
	.cfi_restore_state
	xorl	%eax, %eax
	movw	%ax, 342(%rdi)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L87:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE4067:
	.size	_ZN6icu_6712NumberFormat11setCurrencyEPKDsR10UErrorCode, .-_ZN6icu_6712NumberFormat11setCurrencyEPKDsR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712NumberFormat6formatERKNS_6number4impl15DecimalQuantityERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode
	.type	_ZNK6icu_6712NumberFormat6formatERKNS_6number4impl15DecimalQuantityERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode, @function
_ZNK6icu_6712NumberFormat6formatERKNS_6number4impl15DecimalQuantityERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode:
.LFB4031:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	(%r8), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	testl	%eax, %eax
	jg	.L91
	movq	%rdi, %r15
	movq	%rsi, %rdi
	movq	%rcx, %r14
	movq	%r8, %r13
	call	_ZNK6icu_676number4impl15DecimalQuantity8toDoubleEv@PLT
	movq	(%r15), %rax
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	*80(%rax)
.L91:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4031:
	.size	_ZNK6icu_6712NumberFormat6formatERKNS_6number4impl15DecimalQuantityERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode, .-_ZNK6icu_6712NumberFormat6formatERKNS_6number4impl15DecimalQuantityERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712NumberFormat6formatERKNS_6number4impl15DecimalQuantityERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.type	_ZNK6icu_6712NumberFormat6formatERKNS_6number4impl15DecimalQuantityERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode, @function
_ZNK6icu_6712NumberFormat6formatERKNS_6number4impl15DecimalQuantityERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode:
.LFB4032:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	(%r8), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	testl	%eax, %eax
	jg	.L94
	movq	%rdi, %r15
	movq	%rsi, %rdi
	movq	%rcx, %r14
	movq	%r8, %r13
	call	_ZNK6icu_676number4impl15DecimalQuantity8toDoubleEv@PLT
	movq	(%r15), %rax
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	*72(%rax)
.L94:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4032:
	.size	_ZNK6icu_6712NumberFormat6formatERKNS_6number4impl15DecimalQuantityERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode, .-_ZNK6icu_6712NumberFormat6formatERKNS_6number4impl15DecimalQuantityERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712NumberFormat5parseERKNS_13UnicodeStringERNS_11FormattableER10UErrorCode
	.type	_ZNK6icu_6712NumberFormat5parseERKNS_13UnicodeStringERNS_11FormattableER10UErrorCode, @function
_ZNK6icu_6712NumberFormat5parseERKNS_13UnicodeStringERNS_11FormattableER10UErrorCode:
.LFB4042:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movl	(%rcx), %r8d
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L96
	leaq	16+_ZTVN6icu_6713ParsePositionE(%rip), %rax
	leaq	-48(%rbp), %r12
	movq	%rcx, %rbx
	movq	%rax, -48(%rbp)
	movq	%r12, %rcx
	movabsq	$-4294967296, %rax
	movq	%rax, -40(%rbp)
	movq	(%rdi), %rax
	call	*160(%rax)
	movl	-40(%rbp), %eax
	testl	%eax, %eax
	je	.L101
.L98:
	movq	%r12, %rdi
	call	_ZN6icu_6713ParsePositionD1Ev@PLT
.L96:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L102
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L101:
	.cfi_restore_state
	movl	$3, (%rbx)
	jmp	.L98
.L102:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4042:
	.size	_ZNK6icu_6712NumberFormat5parseERKNS_13UnicodeStringERNS_11FormattableER10UErrorCode, .-_ZNK6icu_6712NumberFormat5parseERKNS_13UnicodeStringERNS_11FormattableER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712NumberFormat20getEffectiveCurrencyEPDsR10UErrorCode
	.type	_ZNK6icu_6712NumberFormat20getEffectiveCurrencyEPDsR10UErrorCode, @function
_ZNK6icu_6712NumberFormat20getEffectiveCurrencyEPDsR10UErrorCode:
.LFB4069:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	cmpw	$0, 342(%rdi)
	movq	%rsi, %r12
	je	.L104
	leaq	342(%rdi), %rsi
	movl	$3, %edx
	movq	%r12, %rdi
	call	u_strncpy_67@PLT
	xorl	%eax, %eax
	movw	%ax, 6(%r12)
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L104:
	.cfi_restore_state
	movl	$1, %esi
	movq	%rdx, %r13
	call	_ZNK6icu_676Format11getLocaleIDE18ULocDataLocaleTypeR10UErrorCode@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L109
.L106:
	movq	%r13, %rcx
	movq	%r12, %rsi
	movl	$4, %edx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	ucurr_forLocale_67@PLT
	.p2align 4,,10
	.p2align 3
.L109:
	.cfi_restore_state
	call	uloc_getDefault_67@PLT
	movq	%rax, %rdi
	jmp	.L106
	.cfi_endproc
.LFE4069:
	.size	_ZNK6icu_6712NumberFormat20getEffectiveCurrencyEPDsR10UErrorCode, .-_ZNK6icu_6712NumberFormat20getEffectiveCurrencyEPDsR10UErrorCode
	.section	.text._ZNK6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEEeqERKNS_12CacheKeyBaseE,"axG",@progbits,_ZNK6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEEeqERKNS_12CacheKeyBaseE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEEeqERKNS_12CacheKeyBaseE
	.type	_ZNK6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEEeqERKNS_12CacheKeyBaseE, @function
_ZNK6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEEeqERKNS_12CacheKeyBaseE:
.LFB5696:
	.cfi_startproc
	endbr64
	cmpq	%rsi, %rdi
	je	.L113
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	movq	-8(%rax), %rax
	movq	8(%rax), %rdi
	movq	(%rsi), %rax
	movq	-8(%rax), %rax
	movq	8(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L112
	cmpb	$42, (%rdi)
	je	.L115
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L115
.L112:
	leaq	16(%r12), %rsi
	leaq	16(%rbx), %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_676LocaleeqERKS0_@PLT
	.p2align 4,,10
	.p2align 3
.L115:
	.cfi_restore_state
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L113:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE5696:
	.size	_ZNK6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEEeqERKNS_12CacheKeyBaseE, .-_ZNK6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEEeqERKNS_12CacheKeyBaseE
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718SharedNumberFormatD0Ev
	.type	_ZN6icu_6718SharedNumberFormatD0Ev, @function
_ZN6icu_6718SharedNumberFormatD0Ev:
.LFB4009:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6718SharedNumberFormatE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L121
	movq	(%rdi), %rax
	call	*8(%rax)
.L121:
	movq	%r12, %rdi
	call	_ZN6icu_6712SharedObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE4009:
	.size	_ZN6icu_6718SharedNumberFormatD0Ev, .-_ZN6icu_6718SharedNumberFormatD0Ev
	.align 2
	.p2align 4
	.type	_ZNK6icu_6712NumberFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode.part.0, @function
_ZNK6icu_6712NumberFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode.part.0:
.LFB5753:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rsi, -80(%rbp)
	movb	$0, -64(%rbp)
	call	_ZNK6icu_6711Formattable9getObjectEv@PLT
	testq	%rax, %rax
	je	.L127
	movq	%rax, %rdi
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6714CurrencyAmountE(%rip), %rdx
	leaq	_ZTIN6icu_677UObjectE(%rip), %rsi
	call	__dynamic_cast@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L127
	movq	120(%rax), %rax
	leaq	-72(%rbp), %r8
	addq	$8, %r13
	movq	%r8, %rdi
	movq	%r8, -88(%rbp)
	leaq	20(%rax), %rsi
	call	u_strcpy_67@PLT
	movq	%r13, -80(%rbp)
	movq	-88(%rbp), %r8
	movb	$1, -64(%rbp)
.L129:
	leaq	342(%rbx), %rsi
	movq	%r8, %rdi
	movq	%r8, -88(%rbp)
	call	u_strcmp_67@PLT
	testl	%eax, %eax
	je	.L130
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*32(%rax)
	movq	-88(%rbp), %r8
	movq	%r14, %rdx
	movq	%rax, %rbx
	movq	(%rax), %rax
	movq	%r8, %rsi
	movq	%rbx, %rdi
	call	*248(%rax)
	movq	(%rbx), %rdx
	leaq	_ZNK6icu_6712NumberFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode(%rip), %rcx
	movq	40(%rdx), %rax
	cmpq	%rcx, %rax
	jne	.L131
	movl	(%r14), %eax
	testl	%eax, %eax
	jle	.L154
.L132:
	movq	%rbx, %rdi
	call	*8(%rdx)
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L127:
	xorl	%edx, %edx
	cmpb	$0, -64(%rbp)
	movq	-80(%rbp), %r13
	leaq	-72(%rbp), %r8
	movw	%dx, -72(%rbp)
	jne	.L129
.L130:
	movq	%r13, %rdi
	call	_ZNK6icu_6711Formattable9isNumericEv@PLT
	testb	%al, %al
	je	.L134
	movq	32(%r13), %rsi
	testq	%rsi, %rsi
	je	.L134
	movq	(%rbx), %rax
	movq	%r14, %r8
	movq	%r15, %rcx
	movq	%r12, %rdx
	movq	%rbx, %rdi
	call	*152(%rax)
.L126:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L155
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L134:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZNK6icu_6711Formattable7getTypeEv@PLT
	cmpl	$2, %eax
	je	.L136
	cmpl	$5, %eax
	je	.L137
	cmpl	$1, %eax
	je	.L156
	movl	$3, (%r14)
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L154:
	movq	%r12, %rdx
	movq	%r14, %r8
	movq	%r15, %rcx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZNK6icu_6712NumberFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode.part.0
	movq	(%rbx), %rdx
	movq	%rax, %r12
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L156:
	movq	(%rbx), %rax
	movsd	8(%r13), %xmm0
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	*72(%rax)
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L137:
	movq	(%rbx), %rax
	movq	8(%r13), %rsi
	movq	%r14, %r8
	movq	%r15, %rcx
	movq	%r12, %rdx
	movq	%rbx, %rdi
	call	*120(%rax)
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L136:
	movq	(%rbx), %rax
	movq	8(%r13), %rsi
	movq	%r14, %r8
	movq	%r15, %rcx
	movq	%r12, %rdx
	movq	%rbx, %rdi
	call	*96(%rax)
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L131:
	movq	%r12, %rdx
	movq	%r14, %r8
	movq	%r15, %rcx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	*%rax
	movq	(%rbx), %rdx
	movq	%rax, %r12
	jmp	.L132
.L155:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5753:
	.size	_ZNK6icu_6712NumberFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode.part.0, .-_ZNK6icu_6712NumberFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712NumberFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.type	_ZNK6icu_6712NumberFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode, @function
_ZNK6icu_6712NumberFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode:
.LFB4033:
	.cfi_startproc
	endbr64
	movl	(%r8), %eax
	testl	%eax, %eax
	jle	.L159
	movq	%rdx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L159:
	jmp	_ZNK6icu_6712NumberFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode.part.0
	.cfi_endproc
.LFE4033:
	.size	_ZNK6icu_6712NumberFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode, .-_ZNK6icu_6712NumberFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.align 2
	.p2align 4
	.type	_ZNK6icu_6712NumberFormat6formatERKNS_11FormattableERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode.part.0, @function
_ZNK6icu_6712NumberFormat6formatERKNS_11FormattableERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode.part.0:
.LFB5754:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rsi, -80(%rbp)
	movb	$0, -64(%rbp)
	call	_ZNK6icu_6711Formattable9getObjectEv@PLT
	testq	%rax, %rax
	je	.L161
	movq	%rax, %rdi
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6714CurrencyAmountE(%rip), %rdx
	leaq	_ZTIN6icu_677UObjectE(%rip), %rsi
	call	__dynamic_cast@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L161
	movq	120(%rax), %rax
	leaq	-72(%rbp), %r8
	addq	$8, %r13
	movq	%r8, %rdi
	movq	%r8, -88(%rbp)
	leaq	20(%rax), %rsi
	call	u_strcpy_67@PLT
	movq	%r13, -80(%rbp)
	movq	-88(%rbp), %r8
	movb	$1, -64(%rbp)
.L163:
	leaq	342(%rbx), %rsi
	movq	%r8, %rdi
	movq	%r8, -88(%rbp)
	call	u_strcmp_67@PLT
	testl	%eax, %eax
	je	.L164
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*32(%rax)
	movq	-88(%rbp), %r8
	movq	%r14, %rdx
	movq	%rax, %rbx
	movq	(%rax), %rax
	movq	%r8, %rsi
	movq	%rbx, %rdi
	call	*248(%rax)
	movq	(%rbx), %rdx
	leaq	_ZNK6icu_6712NumberFormat6formatERKNS_11FormattableERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode(%rip), %rcx
	movq	48(%rdx), %rax
	cmpq	%rcx, %rax
	jne	.L165
	movl	(%r14), %eax
	testl	%eax, %eax
	jle	.L188
.L166:
	movq	%rbx, %rdi
	call	*8(%rdx)
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L161:
	xorl	%edx, %edx
	cmpb	$0, -64(%rbp)
	movq	-80(%rbp), %r13
	leaq	-72(%rbp), %r8
	movw	%dx, -72(%rbp)
	jne	.L163
.L164:
	movq	%r13, %rdi
	call	_ZNK6icu_6711Formattable9isNumericEv@PLT
	testb	%al, %al
	je	.L168
	movq	32(%r13), %rsi
	testq	%rsi, %rsi
	je	.L168
	movq	(%rbx), %rax
	movq	%r14, %r8
	movq	%r15, %rcx
	movq	%r12, %rdx
	movq	%rbx, %rdi
	call	*144(%rax)
.L160:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L189
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L168:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZNK6icu_6711Formattable7getTypeEv@PLT
	cmpl	$2, %eax
	je	.L170
	cmpl	$5, %eax
	je	.L171
	cmpl	$1, %eax
	je	.L190
	movl	$3, (%r14)
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L188:
	movq	%r12, %rdx
	movq	%r14, %r8
	movq	%r15, %rcx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZNK6icu_6712NumberFormat6formatERKNS_11FormattableERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode.part.0
	movq	(%rbx), %rdx
	movq	%rax, %r12
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L190:
	movq	(%rbx), %rax
	movsd	8(%r13), %xmm0
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	*80(%rax)
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L171:
	movq	(%rbx), %rax
	movq	8(%r13), %rsi
	movq	%r14, %r8
	movq	%r15, %rcx
	movq	%r12, %rdx
	movq	%rbx, %rdi
	call	*128(%rax)
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L170:
	movq	(%rbx), %rax
	movq	8(%r13), %rsi
	movq	%r14, %r8
	movq	%r15, %rcx
	movq	%r12, %rdx
	movq	%rbx, %rdi
	call	*104(%rax)
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L165:
	movq	%r12, %rdx
	movq	%r14, %r8
	movq	%r15, %rcx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	*%rax
	movq	(%rbx), %rdx
	movq	%rax, %r12
	jmp	.L166
.L189:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5754:
	.size	_ZNK6icu_6712NumberFormat6formatERKNS_11FormattableERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode.part.0, .-_ZNK6icu_6712NumberFormat6formatERKNS_11FormattableERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712NumberFormat6formatERKNS_11FormattableERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode
	.type	_ZNK6icu_6712NumberFormat6formatERKNS_11FormattableERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode, @function
_ZNK6icu_6712NumberFormat6formatERKNS_11FormattableERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode:
.LFB4036:
	.cfi_startproc
	endbr64
	movl	(%r8), %eax
	testl	%eax, %eax
	jle	.L193
	movq	%rdx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L193:
	jmp	_ZNK6icu_6712NumberFormat6formatERKNS_11FormattableERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode.part.0
	.cfi_endproc
.LFE4036:
	.size	_ZNK6icu_6712NumberFormat6formatERKNS_11FormattableERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode, .-_ZNK6icu_6712NumberFormat6formatERKNS_11FormattableERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev:
.LFB4462:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE4462:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei:
.LFB4465:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	cmpl	$40, %esi
	jg	.L207
	ret
	.p2align 4,,10
	.p2align 3
.L207:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L195
	cmpb	$0, 12(%rbx)
	jne	.L208
.L199:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L195:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L208:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L199
	.cfi_endproc
.LFE4465:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EED2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev:
.LFB4468:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L211
	ret
	.p2align 4,,10
	.p2align 3
.L211:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE4468:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5EOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_:
.LFB4471:
	.cfi_startproc
	endbr64
	movzbl	12(%rsi), %eax
	movq	(%rsi), %r8
	movslq	8(%rsi), %rdx
	movb	%al, 12(%rdi)
	leaq	13(%rsi), %rax
	movq	%r8, (%rdi)
	movl	%edx, 8(%rdi)
	cmpq	%rax, %r8
	je	.L214
	movq	%rax, (%rsi)
	movl	$40, 8(%rsi)
	movb	$0, 12(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L214:
	leaq	13(%rdi), %r9
	movq	%r8, %rsi
	movq	%r9, (%rdi)
	movq	%r9, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE4471:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_:
.LFB4473:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 12(%rdi)
	movq	%rsi, %rbx
	jne	.L220
.L216:
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movzbl	12(%rbx), %eax
	movb	%al, 12(%r12)
	movq	(%rbx), %rsi
	leaq	13(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L221
	movq	%rsi, (%r12)
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L220:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L221:
	leaq	13(%r12), %rdi
	movslq	8(%rbx), %rdx
	movq	%rdi, (%r12)
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4473:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv:
.LFB4474:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE4474:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv:
.LFB4475:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE4475:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv:
.LFB4476:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	addq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE4476:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB4477:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE4477:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB4478:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE4478:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci:
.LFB4479:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L237
	testl	%edx, %edx
	jle	.L237
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 12(%rdi)
	jne	.L240
.L229:
	movq	%rsi, (%rbx)
	movl	%edx, 8(%rbx)
	movb	$0, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L237:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L240:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	%edx, -28(%rbp)
	movq	%rsi, -24(%rbp)
	call	uprv_free_67@PLT
	movl	-28(%rbp), %edx
	movq	-24(%rbp), %rsi
	jmp	.L229
	.cfi_endproc
.LFE4479:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii:
.LFB4480:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L244
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movl	%esi, %r13d
	movl	%edx, %r12d
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L244
	testl	%r12d, %r12d
	jg	.L251
	cmpb	$0, 12(%rbx)
	jne	.L252
.L246:
	movq	%r14, (%rbx)
	movq	%r14, %rax
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L251:
	.cfi_restore_state
	cmpl	%r12d, 8(%rbx)
	cmovle	8(%rbx), %r12d
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	je	.L246
.L252:
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L246
	.p2align 4,,10
	.p2align 3
.L244:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4480:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, .-_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi:
.LFB4481:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 12(%rdi)
	movq	%rdi, %rbx
	je	.L254
	movq	(%rdi), %r8
.L255:
	leaq	13(%rbx), %rax
	movl	%r12d, 0(%r13)
	movq	%rax, (%rbx)
	movq	%r8, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L254:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L258
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	cmovg	%eax, %r12d
	movslq	%r12d, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L258
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L255
	.p2align 4,,10
	.p2align 3
.L258:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4481:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, .-_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv:
.LFB4482:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L265
	ret
	.p2align 4,,10
	.p2align 3
.L265:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE4482:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv:
.LFB4483:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE4483:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_:
.LFB4484:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4484:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_:
.LFB4485:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE4485:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5ERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_:
.LFB4487:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4487:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_:
.LFB4489:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4489:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712NumberFormat16getStaticClassIDEv
	.type	_ZN6icu_6712NumberFormat16getStaticClassIDEv, @function
_ZN6icu_6712NumberFormat16getStaticClassIDEv:
.LFB3998:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6712NumberFormat16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3998:
	.size	_ZN6icu_6712NumberFormat16getStaticClassIDEv, .-_ZN6icu_6712NumberFormat16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712NumberFormatC2Ev
	.type	_ZN6icu_6712NumberFormatC2Ev, @function
_ZN6icu_6712NumberFormatC2Ev:
.LFB4000:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_676FormatC2Ev@PLT
	movdqa	.LC0(%rip), %xmm0
	leaq	16+_ZTVN6icu_6712NumberFormatE(%rip), %rax
	movb	$1, 322(%rbx)
	movq	%rax, (%rbx)
	movl	$256, 352(%rbx)
	movl	$0, 340(%rbx)
	movups	%xmm0, 324(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4000:
	.size	_ZN6icu_6712NumberFormatC2Ev, .-_ZN6icu_6712NumberFormatC2Ev
	.globl	_ZN6icu_6712NumberFormatC1Ev
	.set	_ZN6icu_6712NumberFormatC1Ev,_ZN6icu_6712NumberFormatC2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712NumberFormatD2Ev
	.type	_ZN6icu_6712NumberFormatD2Ev, @function
_ZN6icu_6712NumberFormatD2Ev:
.LFB4003:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6712NumberFormatE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_676FormatD2Ev@PLT
	.cfi_endproc
.LFE4003:
	.size	_ZN6icu_6712NumberFormatD2Ev, .-_ZN6icu_6712NumberFormatD2Ev
	.globl	_ZN6icu_6712NumberFormatD1Ev
	.set	_ZN6icu_6712NumberFormatD1Ev,_ZN6icu_6712NumberFormatD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712NumberFormatD0Ev
	.type	_ZN6icu_6712NumberFormatD0Ev, @function
_ZN6icu_6712NumberFormatD0Ev:
.LFB4005:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6712NumberFormatE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_676FormatD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE4005:
	.size	_ZN6icu_6712NumberFormatD0Ev, .-_ZN6icu_6712NumberFormatD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712NumberFormatC2ERKS0_
	.type	_ZN6icu_6712NumberFormatC2ERKS0_, @function
_ZN6icu_6712NumberFormatC2ERKS0_:
.LFB4011:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZN6icu_676FormatC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6712NumberFormatE(%rip), %rax
	movq	%rax, (%rbx)
	cmpq	%r12, %rbx
	je	.L277
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_676FormataSERKS0_@PLT
	leaq	342(%r12), %rsi
	movl	$3, %edx
	movzbl	322(%r12), %eax
	movdqu	324(%r12), %xmm0
	leaq	342(%rbx), %rdi
	movb	%al, 322(%rbx)
	movzbl	340(%r12), %eax
	movups	%xmm0, 324(%rbx)
	movb	%al, 340(%rbx)
	call	u_strncpy_67@PLT
	xorl	%eax, %eax
	movw	%ax, 348(%rbx)
	movzbl	341(%r12), %eax
	movb	%al, 341(%rbx)
	movl	352(%r12), %eax
	movl	%eax, 352(%rbx)
.L277:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4011:
	.size	_ZN6icu_6712NumberFormatC2ERKS0_, .-_ZN6icu_6712NumberFormatC2ERKS0_
	.globl	_ZN6icu_6712NumberFormatC1ERKS0_
	.set	_ZN6icu_6712NumberFormatC1ERKS0_,_ZN6icu_6712NumberFormatC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712NumberFormataSERKS0_
	.type	_ZN6icu_6712NumberFormataSERKS0_, @function
_ZN6icu_6712NumberFormataSERKS0_:
.LFB4013:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpq	%rsi, %rdi
	je	.L281
	movq	%rsi, %rbx
	call	_ZN6icu_676FormataSERKS0_@PLT
	movzbl	322(%rbx), %eax
	movl	$3, %edx
	leaq	342(%rbx), %rsi
	leaq	342(%r12), %rdi
	movb	%al, 322(%r12)
	movl	324(%rbx), %eax
	movl	%eax, 324(%r12)
	movl	328(%rbx), %eax
	movl	%eax, 328(%r12)
	movl	332(%rbx), %eax
	movl	%eax, 332(%r12)
	movl	336(%rbx), %eax
	movl	%eax, 336(%r12)
	movzbl	340(%rbx), %eax
	movb	%al, 340(%r12)
	call	u_strncpy_67@PLT
	xorl	%eax, %eax
	movw	%ax, 348(%r12)
	movzbl	341(%rbx), %eax
	movb	%al, 341(%r12)
	movl	352(%rbx), %eax
	movl	%eax, 352(%r12)
.L281:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4013:
	.size	_ZN6icu_6712NumberFormataSERKS0_, .-_ZN6icu_6712NumberFormataSERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712ArgExtractorC2ERKNS_12NumberFormatERKNS_11FormattableER10UErrorCode
	.type	_ZN6icu_6712ArgExtractorC2ERKNS_12NumberFormatERKNS_11FormattableER10UErrorCode, @function
_ZN6icu_6712ArgExtractorC2ERKNS_12NumberFormatERKNS_11FormattableER10UErrorCode:
.LFB4026:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rdx, %rdi
	movq	%rdx, (%rbx)
	movb	$0, 16(%rbx)
	call	_ZNK6icu_6711Formattable9getObjectEv@PLT
	testq	%rax, %rax
	je	.L284
	movq	%rax, %rdi
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6714CurrencyAmountE(%rip), %rdx
	leaq	_ZTIN6icu_677UObjectE(%rip), %rsi
	call	__dynamic_cast@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L284
	movq	120(%rax), %rax
	leaq	8(%rbx), %rdi
	addq	$8, %r12
	leaq	20(%rax), %rsi
	call	u_strcpy_67@PLT
	movq	%r12, (%rbx)
	movb	$1, 16(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L284:
	.cfi_restore_state
	xorl	%eax, %eax
	movw	%ax, 8(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4026:
	.size	_ZN6icu_6712ArgExtractorC2ERKNS_12NumberFormatERKNS_11FormattableER10UErrorCode, .-_ZN6icu_6712ArgExtractorC2ERKNS_12NumberFormatERKNS_11FormattableER10UErrorCode
	.globl	_ZN6icu_6712ArgExtractorC1ERKNS_12NumberFormatERKNS_11FormattableER10UErrorCode
	.set	_ZN6icu_6712ArgExtractorC1ERKNS_12NumberFormatERKNS_11FormattableER10UErrorCode,_ZN6icu_6712ArgExtractorC2ERKNS_12NumberFormatERKNS_11FormattableER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712ArgExtractorD2Ev
	.type	_ZN6icu_6712ArgExtractorD2Ev, @function
_ZN6icu_6712ArgExtractorD2Ev:
.LFB4029:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4029:
	.size	_ZN6icu_6712ArgExtractorD2Ev, .-_ZN6icu_6712ArgExtractorD2Ev
	.globl	_ZN6icu_6712ArgExtractorD1Ev
	.set	_ZN6icu_6712ArgExtractorD1Ev,_ZN6icu_6712ArgExtractorD2Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712NumberFormat6formatEdRNS_13UnicodeStringE
	.type	_ZNK6icu_6712NumberFormat6formatEdRNS_13UnicodeStringE, @function
_ZNK6icu_6712NumberFormat6formatEdRNS_13UnicodeStringE:
.LFB4039:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%r13, %rdx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6713FieldPositionE(%rip), %rax
	movl	$0, -32(%rbp)
	movq	%rax, -48(%rbp)
	movl	$4294967295, %eax
	movq	%rax, -40(%rbp)
	movq	(%rdi), %rax
	call	*64(%rax)
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN6icu_6713FieldPositionD1Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L295
	addq	$32, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L295:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4039:
	.size	_ZNK6icu_6712NumberFormat6formatEdRNS_13UnicodeStringE, .-_ZNK6icu_6712NumberFormat6formatEdRNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712NumberFormat6formatEiRNS_13UnicodeStringE
	.type	_ZNK6icu_6712NumberFormat6formatEiRNS_13UnicodeStringE, @function
_ZNK6icu_6712NumberFormat6formatEiRNS_13UnicodeStringE:
.LFB4040:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%r13, %rcx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6713FieldPositionE(%rip), %rax
	movl	$0, -32(%rbp)
	movq	%rax, -48(%rbp)
	movl	$4294967295, %eax
	movq	%rax, -40(%rbp)
	movq	(%rdi), %rax
	call	*88(%rax)
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN6icu_6713FieldPositionD1Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L299
	addq	$32, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L299:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4040:
	.size	_ZNK6icu_6712NumberFormat6formatEiRNS_13UnicodeStringE, .-_ZNK6icu_6712NumberFormat6formatEiRNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712NumberFormat6formatElRNS_13UnicodeStringE
	.type	_ZNK6icu_6712NumberFormat6formatElRNS_13UnicodeStringE, @function
_ZNK6icu_6712NumberFormat6formatElRNS_13UnicodeStringE:
.LFB4041:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%r13, %rcx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6713FieldPositionE(%rip), %rax
	movl	$0, -32(%rbp)
	movq	%rax, -48(%rbp)
	movl	$4294967295, %eax
	movq	%rax, -40(%rbp)
	movq	(%rdi), %rax
	call	*112(%rax)
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN6icu_6713FieldPositionD1Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L303
	addq	$32, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L303:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4041:
	.size	_ZNK6icu_6712NumberFormat6formatElRNS_13UnicodeStringE, .-_ZNK6icu_6712NumberFormat6formatElRNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712NumberFormat19getAvailableLocalesERi
	.type	_ZN6icu_6712NumberFormat19getAvailableLocalesERi, @function
_ZN6icu_6712NumberFormat19getAvailableLocalesERi:
.LFB4054:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_676Locale19getAvailableLocalesERi@PLT
	.cfi_endproc
.LFE4054:
	.size	_ZN6icu_6712NumberFormat19getAvailableLocalesERi, .-_ZN6icu_6712NumberFormat19getAvailableLocalesERi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712NumberFormat14isGroupingUsedEv
	.type	_ZNK6icu_6712NumberFormat14isGroupingUsedEv, @function
_ZNK6icu_6712NumberFormat14isGroupingUsedEv:
.LFB4057:
	.cfi_startproc
	endbr64
	movzbl	322(%rdi), %eax
	ret
	.cfi_endproc
.LFE4057:
	.size	_ZNK6icu_6712NumberFormat14isGroupingUsedEv, .-_ZNK6icu_6712NumberFormat14isGroupingUsedEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712NumberFormat23getMaximumIntegerDigitsEv
	.type	_ZNK6icu_6712NumberFormat23getMaximumIntegerDigitsEv, @function
_ZNK6icu_6712NumberFormat23getMaximumIntegerDigitsEv:
.LFB4059:
	.cfi_startproc
	endbr64
	movl	324(%rdi), %eax
	ret
	.cfi_endproc
.LFE4059:
	.size	_ZNK6icu_6712NumberFormat23getMaximumIntegerDigitsEv, .-_ZNK6icu_6712NumberFormat23getMaximumIntegerDigitsEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712NumberFormat23getMinimumIntegerDigitsEv
	.type	_ZNK6icu_6712NumberFormat23getMinimumIntegerDigitsEv, @function
_ZNK6icu_6712NumberFormat23getMinimumIntegerDigitsEv:
.LFB4061:
	.cfi_startproc
	endbr64
	movl	328(%rdi), %eax
	ret
	.cfi_endproc
.LFE4061:
	.size	_ZNK6icu_6712NumberFormat23getMinimumIntegerDigitsEv, .-_ZNK6icu_6712NumberFormat23getMinimumIntegerDigitsEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712NumberFormat24getMaximumFractionDigitsEv
	.type	_ZNK6icu_6712NumberFormat24getMaximumFractionDigitsEv, @function
_ZNK6icu_6712NumberFormat24getMaximumFractionDigitsEv:
.LFB4063:
	.cfi_startproc
	endbr64
	movl	332(%rdi), %eax
	ret
	.cfi_endproc
.LFE4063:
	.size	_ZNK6icu_6712NumberFormat24getMaximumFractionDigitsEv, .-_ZNK6icu_6712NumberFormat24getMaximumFractionDigitsEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712NumberFormat24getMinimumFractionDigitsEv
	.type	_ZNK6icu_6712NumberFormat24getMinimumFractionDigitsEv, @function
_ZNK6icu_6712NumberFormat24getMinimumFractionDigitsEv:
.LFB4065:
	.cfi_startproc
	endbr64
	movl	336(%rdi), %eax
	ret
	.cfi_endproc
.LFE4065:
	.size	_ZNK6icu_6712NumberFormat24getMinimumFractionDigitsEv, .-_ZNK6icu_6712NumberFormat24getMinimumFractionDigitsEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712NumberFormat11getCurrencyEv
	.type	_ZNK6icu_6712NumberFormat11getCurrencyEv, @function
_ZNK6icu_6712NumberFormat11getCurrencyEv:
.LFB4068:
	.cfi_startproc
	endbr64
	leaq	342(%rdi), %rax
	ret
	.cfi_endproc
.LFE4068:
	.size	_ZNK6icu_6712NumberFormat11getCurrencyEv, .-_ZNK6icu_6712NumberFormat11getCurrencyEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712NumberFormat16isStyleSupportedE18UNumberFormatStyle
	.type	_ZN6icu_6712NumberFormat16isStyleSupportedE18UNumberFormatStyle, @function
_ZN6icu_6712NumberFormat16isStyleSupportedE18UNumberFormatStyle:
.LFB4075:
	.cfi_startproc
	endbr64
	movslq	%edi, %rdi
	leaq	_ZL25gLastResortNumberPatterns(%rip), %rax
	cmpq	$0, (%rax,%rdi,8)
	setne	%al
	ret
	.cfi_endproc
.LFE4075:
	.size	_ZN6icu_6712NumberFormat16isStyleSupportedE18UNumberFormatStyle, .-_ZN6icu_6712NumberFormat16isStyleSupportedE18UNumberFormatStyle
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712NumberFormateqERKNS_6FormatE
	.type	_ZNK6icu_6712NumberFormateqERKNS_6FormatE, @function
_ZNK6icu_6712NumberFormateqERKNS_6FormatE:
.LFB4014:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	cmpq	%rsi, %rdi
	je	.L318
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_ZNK6icu_676FormateqERKS0_@PLT
	testb	%al, %al
	jne	.L314
.L315:
	xorl	%eax, %eax
.L312:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L314:
	.cfi_restore_state
	movl	324(%rbx), %eax
	cmpl	%eax, 324(%r12)
	jne	.L315
	movq	328(%rbx), %rax
	cmpq	%rax, 328(%r12)
	jne	.L315
	movl	336(%rbx), %eax
	cmpl	%eax, 336(%r12)
	jne	.L315
	movzbl	322(%rbx), %eax
	cmpb	%al, 322(%r12)
	jne	.L315
	movzbl	340(%rbx), %eax
	cmpb	%al, 340(%r12)
	jne	.L315
	leaq	342(%rbx), %rsi
	leaq	342(%r12), %rdi
	call	u_strcmp_67@PLT
	testl	%eax, %eax
	jne	.L315
	movzbl	341(%rbx), %eax
	cmpb	%al, 341(%r12)
	jne	.L315
	movl	352(%rbx), %eax
	cmpl	%eax, 352(%r12)
	sete	%al
	jmp	.L312
	.p2align 4,,10
	.p2align 3
.L318:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE4014:
	.size	_ZNK6icu_6712NumberFormateqERKNS_6FormatE, .-_ZNK6icu_6712NumberFormateqERKNS_6FormatE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712NumberFormat6formatENS_11StringPieceERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode
	.type	_ZNK6icu_6712NumberFormat6formatENS_11StringPieceERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode, @function
_ZNK6icu_6712NumberFormat6formatENS_11StringPieceERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode:
.LFB4021:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-176(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$152, %rsp
	movq	%rsi, -192(%rbp)
	movq	%rdx, -184(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6711FormattableC1Ev@PLT
	movq	-184(%rbp), %rdx
	movq	%rbx, %rcx
	movq	%r12, %rdi
	movq	-192(%rbp), %rsi
	call	_ZN6icu_6711Formattable16setDecimalNumberENS_11StringPieceER10UErrorCode@PLT
	movq	(%r15), %rax
	leaq	_ZNK6icu_6712NumberFormat6formatERKNS_11FormattableERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L322
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L326
.L323:
	movq	%r12, %rdi
	call	_ZN6icu_6711FormattableD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L327
	addq	$152, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L326:
	.cfi_restore_state
	movq	%rbx, %r8
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZNK6icu_6712NumberFormat6formatERKNS_11FormattableERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode.part.0
	jmp	.L323
	.p2align 4,,10
	.p2align 3
.L322:
	movq	%rbx, %r8
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	*%rax
	jmp	.L323
.L327:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4021:
	.size	_ZNK6icu_6712NumberFormat6formatENS_11StringPieceERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode, .-_ZNK6icu_6712NumberFormat6formatENS_11StringPieceERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712NumberFormat13parseCurrencyERKNS_13UnicodeStringERNS_13ParsePositionE
	.type	_ZNK6icu_6712NumberFormat13parseCurrencyERKNS_13UnicodeStringERNS_13ParsePositionE, @function
_ZNK6icu_6712NumberFormat13parseCurrencyERKNS_13UnicodeStringERNS_13ParsePositionE:
.LFB4043:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-176(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$184, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6711FormattableC1Ev@PLT
	movq	(%r12), %rax
	movl	8(%rbx), %r15d
	movq	%rbx, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	*160(%rax)
	cmpl	8(%rbx), %r15d
	je	.L341
	movq	(%r12), %rax
	leaq	_ZNK6icu_6712NumberFormat20getEffectiveCurrencyEPDsR10UErrorCode(%rip), %rdx
	movl	$0, -196(%rbp)
	movq	288(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L330
	cmpw	$0, 342(%r12)
	jne	.L347
	leaq	-196(%rbp), %rcx
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rcx, %rdx
	movq	%rcx, -216(%rbp)
	call	_ZNK6icu_676Format11getLocaleIDE18ULocDataLocaleTypeR10UErrorCode@PLT
	movq	-216(%rbp), %rcx
	testq	%rax, %rax
	movq	%rax, %rdi
	je	.L348
.L333:
	leaq	-184(%rbp), %r14
	movl	$4, %edx
	movq	%r14, %rsi
	call	ucurr_forLocale_67@PLT
.L332:
	movl	-196(%rbp), %ecx
	testl	%ecx, %ecx
	jg	.L341
	movl	$128, %edi
	movq	%r14, -192(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L335
	leaq	-196(%rbp), %rcx
	leaq	-192(%rbp), %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6714CurrencyAmountC1ERKNS_11FormattableENS_14ConstChar16PtrER10UErrorCode@PLT
.L336:
	movl	-196(%rbp), %edx
	testl	%edx, %edx
	jle	.L329
	movl	%r15d, 8(%rbx)
	testq	%r12, %r12
	je	.L341
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L341:
	xorl	%r12d, %r12d
.L329:
	movq	%r13, %rdi
	call	_ZN6icu_6711FormattableD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L349
	addq	$184, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L347:
	.cfi_restore_state
	leaq	-184(%rbp), %r14
	leaq	342(%r12), %rsi
	movl	$3, %edx
	movq	%r14, %rdi
	call	u_strncpy_67@PLT
	xorl	%esi, %esi
	movw	%si, -178(%rbp)
	jmp	.L332
	.p2align 4,,10
	.p2align 3
.L330:
	leaq	-184(%rbp), %r14
	leaq	-196(%rbp), %rdx
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	*%rax
	jmp	.L332
.L335:
	movl	-196(%rbp), %eax
	testl	%eax, %eax
	jg	.L336
	movl	$7, -196(%rbp)
	jmp	.L336
	.p2align 4,,10
	.p2align 3
.L348:
	call	uloc_getDefault_67@PLT
	movq	-216(%rbp), %rcx
	movq	%rax, %rdi
	jmp	.L333
.L349:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4043:
	.size	_ZNK6icu_6712NumberFormat13parseCurrencyERKNS_13UnicodeStringERNS_13ParsePositionE, .-_ZNK6icu_6712NumberFormat13parseCurrencyERKNS_13UnicodeStringERNS_13ParsePositionE
	.section	.text._ZN6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEED2Ev,"axG",@progbits,_ZN6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEED2Ev
	.type	_ZN6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEED2Ev, @function
_ZN6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEED2Ev:
.LFB5036:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	16(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -16(%rdi)
	call	_ZN6icu_676LocaleD1Ev@PLT
	leaq	16+_ZTVN6icu_678CacheKeyINS_18SharedNumberFormatEEE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6712CacheKeyBaseD2Ev@PLT
	.cfi_endproc
.LFE5036:
	.size	_ZN6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEED2Ev, .-_ZN6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEED2Ev
	.weak	_ZN6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEED1Ev
	.set	_ZN6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEED1Ev,_ZN6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEED2Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712NumberFormat20createSharedInstanceERKNS_6LocaleE18UNumberFormatStyleR10UErrorCode
	.type	_ZN6icu_6712NumberFormat20createSharedInstanceERKNS_6LocaleE18UNumberFormatStyleR10UErrorCode, @function
_ZN6icu_6712NumberFormat20createSharedInstanceERKNS_6LocaleE18UNumberFormatStyleR10UErrorCode:
.LFB4074:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$296, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rdx), %r9d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r9d, %r9d
	jg	.L370
	movq	%rdx, %rbx
	cmpl	$1, %esi
	jne	.L371
	movq	%rdi, %r12
	movq	%rdx, %rdi
	call	_ZN6icu_6712UnifiedCache11getInstanceER10UErrorCode@PLT
	movl	(%rbx), %r8d
	movq	%rax, %r15
	testl	%r8d, %r8d
	jle	.L372
.L370:
	xorl	%eax, %eax
.L352:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L373
	addq	$296, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L371:
	.cfi_restore_state
	movl	$16, (%rdx)
	jmp	.L370
	.p2align 4,,10
	.p2align 3
.L372:
	leaq	-288(%rbp), %r13
	movq	%r12, %rsi
	leaq	16+_ZTVN6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEEE(%rip), %rax
	movl	$0, -296(%rbp)
	movq	%r13, %rdi
	movq	%rax, -304(%rbp)
	leaq	-304(%rbp), %r14
	movb	$0, -292(%rbp)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movl	(%rbx), %edi
	xorl	%eax, %eax
	testl	%edi, %edi
	jle	.L374
.L357:
	movq	%rax, -328(%rbp)
	movq	%r13, %rdi
	leaq	16+_ZTVN6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEEE(%rip), %rax
	movq	%rax, -304(%rbp)
	call	_ZN6icu_676LocaleD1Ev@PLT
	leaq	16+_ZTVN6icu_678CacheKeyINS_18SharedNumberFormatEEE(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -304(%rbp)
	call	_ZN6icu_6712CacheKeyBaseD2Ev@PLT
	movq	-328(%rbp), %rax
	jmp	.L352
	.p2align 4,,10
	.p2align 3
.L374:
	leaq	-312(%rbp), %rdx
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	%r15, %rdi
	leaq	-316(%rbp), %r8
	movl	$0, -316(%rbp)
	movq	$0, -312(%rbp)
	call	_ZNK6icu_6712UnifiedCache4_getERKNS_12CacheKeyBaseERPKNS_12SharedObjectEPKvR10UErrorCode@PLT
	movl	-316(%rbp), %edx
	movq	-312(%rbp), %rax
	testl	%edx, %edx
	jle	.L375
	testq	%rax, %rax
	je	.L361
	movq	%rax, %r12
	xorl	%eax, %eax
.L360:
	movq	%r12, %rdi
	movq	%rax, -328(%rbp)
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	movl	(%rbx), %esi
	movl	-316(%rbp), %edx
	movq	-328(%rbp), %rax
	testl	%esi, %esi
	je	.L361
	testl	%edx, %edx
	jle	.L357
	.p2align 4,,10
	.p2align 3
.L361:
	movl	%edx, (%rbx)
	jmp	.L357
	.p2align 4,,10
	.p2align 3
.L375:
	testq	%rax, %rax
	je	.L359
	movq	%rax, %rdi
	movq	%rax, -328(%rbp)
	movq	%rax, %r12
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
	movq	-328(%rbp), %rax
	jmp	.L360
.L359:
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jne	.L357
	movl	%edx, (%rbx)
	jmp	.L357
.L373:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4074:
	.size	_ZN6icu_6712NumberFormat20createSharedInstanceERKNS_6LocaleE18UNumberFormatStyleR10UErrorCode, .-_ZN6icu_6712NumberFormat20createSharedInstanceERKNS_6LocaleE18UNumberFormatStyleR10UErrorCode
	.section	.text._ZN6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEED0Ev,"axG",@progbits,_ZN6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEED0Ev
	.type	_ZN6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEED0Ev, @function
_ZN6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEED0Ev:
.LFB5038:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	16(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -16(%rdi)
	call	_ZN6icu_676LocaleD1Ev@PLT
	leaq	16+_ZTVN6icu_678CacheKeyINS_18SharedNumberFormatEEE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	call	_ZN6icu_6712CacheKeyBaseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE5038:
	.size	_ZN6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEED0Ev, .-_ZN6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEED0Ev
	.section	.rodata.str2.2,"aMS",@progbits,2
	.align 2
.LC1:
	.string	"S"
	.string	"p"
	.string	"e"
	.string	"l"
	.string	"l"
	.string	"o"
	.string	"u"
	.string	"t"
	.string	"R"
	.string	"u"
	.string	"l"
	.string	"e"
	.string	"s"
	.string	""
	.string	""
	.text
	.align 2
	.p2align 4
	.type	_ZN6icu_6712NumberFormat12makeInstanceERKNS_6LocaleE18UNumberFormatStyleaR10UErrorCode.part.0, @function
_ZN6icu_6712NumberFormat12makeInstanceERKNS_6LocaleE18UNumberFormatStyleaR10UErrorCode.part.0:
.LFB5757:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$872, %rsp
	movl	%esi, -864(%rbp)
	movl	%edx, -856(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movslq	%esi, %rax
	movq	%rax, %rcx
	movq	%rax, -872(%rbp)
	leaq	_ZL25gLastResortNumberPatterns(%rip), %rax
	movq	(%rax,%rcx,8), %r15
	testq	%r15, %r15
	je	.L484
	movl	_ZL16gNSCacheInitOnce(%rip), %eax
	movq	%rdi, %r12
	cmpl	$2, %eax
	je	.L382
	leaq	_ZL16gNSCacheInitOnce(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	jne	.L485
.L382:
	movq	_ZL21NumberingSystem_cache(%rip), %r15
	testq	%r15, %r15
	je	.L386
.L490:
	movq	%r12, %rdi
	call	_ZNK6icu_676Locale8hashCodeEv@PLT
	leaq	_ZZN6icu_6712NumberFormat12makeInstanceERKNS_6LocaleE18UNumberFormatStyleaR10UErrorCodeE12nscacheMutex(%rip), %rdi
	movl	%eax, %r14d
	call	umtx_lock_67@PLT
	movq	_ZL21NumberingSystem_cache(%rip), %rdi
	movl	%r14d, %esi
	call	uhash_iget_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L486
.L387:
	leaq	_ZZN6icu_6712NumberFormat12makeInstanceERKNS_6LocaleE18UNumberFormatStyleaR10UErrorCodeE12nscacheMutex(%rip), %rdi
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
	call	umtx_unlock_67@PLT
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L378
.L388:
	cmpb	$0, -856(%rbp)
	je	.L390
	movq	%r13, %rdi
	call	_ZNK6icu_6715NumberingSystem13isAlgorithmicEv@PLT
	testb	%al, %al
	jne	.L487
.L390:
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rbx, %rdx
	xorl	%edi, %edi
	xorl	%r15d, %r15d
	movq	40(%r12), %rsi
	movq	%rax, -832(%rbp)
	movl	$2, %eax
	movw	%ax, -824(%rbp)
	call	ures_open_67@PLT
	leaq	-832(%rbp), %r11
	movq	%rax, -856(%rbp)
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L488
.L391:
	movq	-856(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L483
	movq	%r11, -856(%rbp)
	call	ures_close_67@PLT
	movq	-856(%rbp), %r11
.L483:
	movq	%r11, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L389:
	testq	%r14, %r14
	je	.L378
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*8(%rax)
.L378:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L489
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L485:
	.cfi_restore_state
	leaq	numfmt_cleanup(%rip), %rsi
	movl	$22, %edi
	call	ucln_i18n_registerCleanup_67@PLT
	movq	uhash_compareLong_67@GOTPCREL(%rip), %rsi
	movq	uhash_hashLong_67@GOTPCREL(%rip), %rdi
	xorl	%edx, %edx
	leaq	-840(%rbp), %rcx
	movl	$0, -840(%rbp)
	call	uhash_open_67@PLT
	movl	-840(%rbp), %esi
	movq	%rax, _ZL21NumberingSystem_cache(%rip)
	testl	%esi, %esi
	jle	.L384
	movq	$0, _ZL21NumberingSystem_cache(%rip)
.L385:
	leaq	_ZL16gNSCacheInitOnce(%rip), %rdi
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
	movq	_ZL21NumberingSystem_cache(%rip), %r15
	testq	%r15, %r15
	jne	.L490
.L386:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6715NumberingSystem14createInstanceERKNS_6LocaleER10UErrorCode@PLT
	movl	(%rbx), %edx
	movq	%rax, %r13
	movq	%rax, %r14
	testl	%edx, %edx
	jle	.L388
	jmp	.L389
	.p2align 4,,10
	.p2align 3
.L384:
	leaq	deleteNumberingSystem(%rip), %rsi
	movq	%rax, %rdi
	call	uhash_setValueDeleter_67@PLT
	jmp	.L385
	.p2align 4,,10
	.p2align 3
.L486:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6715NumberingSystem14createInstanceERKNS_6LocaleER10UErrorCode@PLT
	movq	_ZL21NumberingSystem_cache(%rip), %rdi
	movq	%rbx, %rcx
	movl	%r14d, %esi
	movq	%rax, %rdx
	movq	%rax, %r13
	call	uhash_iput_67@PLT
	jmp	.L387
	.p2align 4,,10
	.p2align 3
.L487:
	movl	$16, (%rbx)
	xorl	%r15d, %r15d
	jmp	.L389
	.p2align 4,,10
	.p2align 3
.L488:
	movl	$2816, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L392
	movq	%rax, %rdi
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%rax, -880(%rbp)
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKNS_6LocaleER10UErrorCode@PLT
	movl	(%rbx), %eax
	movq	-880(%rbp), %r10
	testl	%eax, %eax
	jle	.L449
	movq	%r10, %rdi
	call	_ZN6icu_6720DecimalFormatSymbolsD0Ev@PLT
	movl	(%rbx), %eax
	xorl	%r10d, %r10d
	testl	%eax, %eax
	jle	.L449
.L444:
	cmpq	$0, -856(%rbp)
	je	.L491
	xorl	%r10d, %r10d
	xorl	%r15d, %r15d
	leaq	-832(%rbp), %r11
.L450:
	movq	-856(%rbp), %rdi
	movq	%r11, -872(%rbp)
	movq	%r10, -864(%rbp)
	call	ures_close_67@PLT
	movq	-872(%rbp), %r11
	movq	-864(%rbp), %r10
.L442:
	movq	%r11, %rdi
	movq	%r10, -856(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-856(%rbp), %r10
	testq	%r10, %r10
	je	.L389
	movq	(%r10), %rax
	movq	%r10, %rdi
	call	*8(%rax)
	jmp	.L389
	.p2align 4,,10
	.p2align 3
.L484:
	movl	$16, (%rbx)
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L449:
	movq	-872(%rbp), %rdx
	leaq	_ZL17gFormatCldrStyles(%rip), %rax
	movq	%r13, %rdi
	movq	%r10, -880(%rbp)
	movl	(%rax,%rdx,4), %r15d
	call	_ZNK6icu_6715NumberingSystem7getNameEv@PLT
	movq	%rbx, %rcx
	movq	%r12, %rdi
	movl	%r15d, %edx
	movq	%rax, %rsi
	leaq	-288(%rbp), %r15
	call	_ZN6icu_676number4impl5utils18getPatternForStyleERKNS_6LocaleEPKcNS1_16CldrPatternStyleER10UErrorCode@PLT
	movl	$-1, %ecx
	movq	%r15, %rdi
	movl	$1, %esi
	movq	%rax, -840(%rbp)
	leaq	-840(%rbp), %rax
	movq	%rax, %rdx
	movq	%rax, -888(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	leaq	-832(%rbp), %r11
	movq	%r15, %rsi
	movq	%r11, %rdi
	movq	%r11, -872(%rbp)
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	(%rbx), %eax
	movq	-872(%rbp), %r11
	movq	-880(%rbp), %r10
	testl	%eax, %eax
	jg	.L456
	movl	-864(%rbp), %edx
	cmpl	$16, %edx
	ja	.L395
	movl	$77828, %eax
	btq	%rdx, %rax
	jc	.L396
	cmpl	$10, %edx
	jne	.L395
.L396:
	movq	2416(%r10), %rcx
	testq	%rcx, %rcx
	je	.L401
	movq	%rcx, %rdi
	movq	%r10, -904(%rbp)
	movq	%rcx, -896(%rbp)
	movq	%r11, -872(%rbp)
	call	u_strlen_67@PLT
	movq	-872(%rbp), %r11
	movl	%eax, -880(%rbp)
	movq	%r11, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	movswl	-824(%rbp), %edx
	movq	-872(%rbp), %r11
	movl	-880(%rbp), %r9d
	movq	-896(%rbp), %rcx
	testw	%dx, %dx
	movq	-904(%rbp), %r10
	js	.L402
	sarl	$5, %edx
.L403:
	movq	%r11, %rdi
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	%r10, -880(%rbp)
	movq	%r11, -872(%rbp)
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	movq	-880(%rbp), %r10
	movq	-872(%rbp), %r11
.L401:
	movq	%r13, %rdi
	movq	%r11, -880(%rbp)
	movq	%r10, -872(%rbp)
	call	_ZNK6icu_6715NumberingSystem13isAlgorithmicEv@PLT
	movq	-872(%rbp), %r10
	movq	-880(%rbp), %r11
	testb	%al, %al
	jne	.L397
	cmpl	$10, -864(%rbp)
	je	.L492
.L398:
	movl	$368, %edi
	movq	%r11, -880(%rbp)
	movq	%r10, -872(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	-872(%rbp), %r10
	movq	-880(%rbp), %r11
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L439
	movl	-864(%rbp), %ecx
	movq	%r11, %rsi
	movq	%rbx, %r8
	movq	%r10, %rdx
	movq	%rax, %rdi
	movq	%r11, -872(%rbp)
	call	_ZN6icu_6713DecimalFormatC1ERKNS_13UnicodeStringEPNS_20DecimalFormatSymbolsE18UNumberFormatStyleR10UErrorCode@PLT
	movl	(%rbx), %esi
	movq	-872(%rbp), %r11
	testl	%esi, %esi
	jg	.L441
	cmpl	$13, -864(%rbp)
	je	.L493
.L448:
	movq	-856(%rbp), %r13
	xorl	%esi, %esi
	movq	%rbx, %rdx
	movq	%r11, -864(%rbp)
	movq	%r13, %rdi
	call	ures_getLocaleByType_67@PLT
	movq	%rbx, %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	movq	%rax, %r12
	call	ures_getLocaleByType_67@PLT
	movq	%r12, %rdx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_676Format12setLocaleIDsEPKcS2_@PLT
	movl	(%rbx), %edx
	movq	-864(%rbp), %r11
	testl	%edx, %edx
	jle	.L391
	xorl	%r10d, %r10d
.L431:
	movq	(%r15), %rax
	movq	%r11, -872(%rbp)
	movq	%r15, %rdi
	xorl	%r15d, %r15d
	movq	%r10, -864(%rbp)
	call	*8(%rax)
	movq	-864(%rbp), %r10
	movq	-872(%rbp), %r11
.L394:
	cmpq	$0, -856(%rbp)
	je	.L442
	jmp	.L450
	.p2align 4,,10
	.p2align 3
.L456:
	xorl	%r15d, %r15d
	jmp	.L394
	.p2align 4,,10
	.p2align 3
.L395:
	movq	%r13, %rdi
	movq	%r11, -880(%rbp)
	movq	%r10, -872(%rbp)
	call	_ZNK6icu_6715NumberingSystem13isAlgorithmicEv@PLT
	movq	-872(%rbp), %r10
	movq	-880(%rbp), %r11
	testb	%al, %al
	je	.L398
.L397:
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%r11, -880(%rbp)
	movl	$2, %r11d
	movq	%rax, -768(%rbp)
	movq	%rax, -704(%rbp)
	movq	%rax, -640(%rbp)
	movl	$2, %eax
	movw	%ax, -632(%rbp)
	leaq	-512(%rbp), %rax
	movq	%rax, %rdi
	movq	%r10, -864(%rbp)
	movl	$2, %r10d
	movw	%r10w, -760(%rbp)
	movw	%r11w, -696(%rbp)
	movq	%rax, -872(%rbp)
	call	_ZN6icu_676LocaleC1Ev@PLT
	movq	0(%r13), %rax
	movq	%r13, %rsi
	movq	%r15, %rdi
	leaq	-768(%rbp), %r13
	call	*24(%rax)
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString8copyFromERKS0_a@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movswl	-760(%rbp), %ecx
	movl	$47, %esi
	movq	%r13, %rdi
	movl	%ecx, %eax
	sarl	$5, %ecx
	testw	%ax, %ax
	cmovs	-756(%rbp), %ecx
	xorl	%edx, %edx
	call	_ZNK6icu_6713UnicodeString9doIndexOfEDsii@PLT
	movq	-864(%rbp), %r10
	movq	-880(%rbp), %r11
	movl	%eax, %r8d
	movzwl	-760(%rbp), %eax
	movswl	%ax, %ecx
	sarl	$5, %ecx
	testw	%ax, %ax
	jns	.L407
	movl	-756(%rbp), %ecx
.L407:
	xorl	%edx, %edx
	movl	$47, %esi
	movq	%r13, %rdi
	movq	%r11, -904(%rbp)
	movl	%r8d, -896(%rbp)
	movq	%r10, -864(%rbp)
	call	_ZNK6icu_6713UnicodeString13doLastIndexOfEDsii@PLT
	movl	-896(%rbp), %r8d
	movq	-864(%rbp), %r10
	movl	%eax, -880(%rbp)
	movq	-904(%rbp), %r11
	cmpl	%eax, %r8d
	jge	.L408
	xorl	%r9d, %r9d
	movl	%r8d, %ecx
	leaq	-563(%rbp), %rax
	xorl	%edx, %edx
	leaq	-576(%rbp), %r12
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%r11, -912(%rbp)
	movq	%r10, -904(%rbp)
	movw	%r9w, -564(%rbp)
	movl	%r8d, -864(%rbp)
	movq	%rax, -576(%rbp)
	movl	$0, -520(%rbp)
	movl	$40, -568(%rbp)
	call	_ZNK6icu_6713UnicodeString13tempSubStringEii@PLT
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6710CharString20appendInvariantCharsERKNS_13UnicodeStringER10UErrorCode@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-864(%rbp), %r8d
	movl	-880(%rbp), %r9d
	leaq	-704(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -864(%rbp)
	subl	%r8d, %r9d
	leal	1(%r8), %r12d
	subl	$1, %r9d
	movl	%r9d, -896(%rbp)
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	movzwl	-696(%rbp), %eax
	movl	-896(%rbp), %r9d
	movq	-904(%rbp), %r10
	movq	-912(%rbp), %r11
	testw	%ax, %ax
	js	.L409
	movswl	%ax, %edx
	sarl	$5, %edx
.L410:
	movq	-864(%rbp), %rdi
	movl	%r12d, %r8d
	xorl	%esi, %esi
	movq	%r13, %rcx
	movq	%r11, -904(%rbp)
	leaq	-640(%rbp), %r12
	movq	%r10, -896(%rbp)
	call	_ZN6icu_6713UnicodeString9doReplaceEiiRKS0_ii@PLT
	movl	-880(%rbp), %eax
	movq	%r12, %rdi
	addl	$1, %eax
	movl	%eax, -880(%rbp)
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	movl	-880(%rbp), %eax
	xorl	%r8d, %r8d
	movzwl	-760(%rbp), %edx
	movq	-896(%rbp), %r10
	movq	-904(%rbp), %r11
	testl	%eax, %eax
	js	.L412
	testw	%dx, %dx
	js	.L413
	movswl	%dx, %r8d
	sarl	$5, %r8d
.L414:
	cmpl	%r8d, %eax
	cmovle	%eax, %r8d
.L412:
	testw	%dx, %dx
	js	.L415
	movswl	%dx, %r9d
	sarl	$5, %r9d
.L416:
	movzwl	-632(%rbp), %eax
	subl	%r8d, %r9d
	testw	%ax, %ax
	js	.L417
	movswl	%ax, %edx
	sarl	$5, %edx
.L418:
	movq	%r13, %rcx
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%r11, -896(%rbp)
	movq	%r10, -880(%rbp)
	call	_ZN6icu_6713UnicodeString9doReplaceEiiRKS0_ii@PLT
	movq	-576(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN6icu_676Locale14createFromNameEPKc@PLT
	movq	-872(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN6icu_676LocaleaSEOS0_@PLT
	movq	%r15, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	movq	-888(%rbp), %rdx
	movl	$-1, %ecx
	movq	%r15, %rdi
	leaq	.LC1(%rip), %rax
	movl	$1, %esi
	movq	%rax, -840(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movzwl	-280(%rbp), %edx
	movq	-880(%rbp), %r10
	movq	-896(%rbp), %r11
	testw	%dx, %dx
	js	.L419
	movswl	%dx, %ecx
	sarl	$5, %ecx
.L420:
	movzwl	-696(%rbp), %eax
	testw	%ax, %ax
	js	.L421
	movswl	%ax, %esi
	sarl	$5, %esi
.L422:
	testb	$1, %dl
	je	.L423
	notl	%eax
	andl	$1, %eax
.L424:
	cmpb	$1, %al
	movq	%r15, %rdi
	movq	%r11, -896(%rbp)
	sbbl	%esi, %esi
	movq	%r10, -888(%rbp)
	notl	%esi
	andl	$3, %esi
	movl	%esi, -880(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, -564(%rbp)
	movl	-880(%rbp), %esi
	movq	-888(%rbp), %r10
	movq	-896(%rbp), %r11
	jne	.L494
.L429:
	movl	$752, %edi
	movq	%r11, -888(%rbp)
	movq	%r10, -880(%rbp)
	movl	%esi, -896(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	-880(%rbp), %r10
	movq	-888(%rbp), %r11
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L430
	movq	-872(%rbp), %rdx
	movl	-896(%rbp), %esi
	movq	%rbx, %rcx
	movq	%rax, %rdi
	call	_ZN6icu_6721RuleBasedNumberFormatC1ENS_15URBNFRuleSetTagERKNS_6LocaleER10UErrorCode@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdx
	movq	%r15, %rdi
	call	_ZN6icu_6721RuleBasedNumberFormat17setDefaultRuleSetERKNS_13UnicodeStringER10UErrorCode@PLT
	movq	-872(%rbp), %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-864(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-856(%rbp), %r13
	xorl	%esi, %esi
	movq	%rbx, %rdx
	movq	%r13, %rdi
	call	ures_getLocaleByType_67@PLT
	movq	%rbx, %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	movq	%rax, %r12
	call	ures_getLocaleByType_67@PLT
	movq	%r15, %rdi
	movq	%r12, %rdx
	movq	%rax, %rsi
	call	_ZN6icu_676Format12setLocaleIDsEPKcS2_@PLT
	movl	(%rbx), %edi
	movq	-880(%rbp), %r10
	movq	-888(%rbp), %r11
	testl	%edi, %edi
	jle	.L394
	jmp	.L431
.L493:
	movq	%rbx, %rdx
	movl	$1, %esi
	movq	%r15, %rdi
	movq	%r11, -864(%rbp)
	call	_ZN6icu_6713DecimalFormat16setCurrencyUsageE14UCurrencyUsageP10UErrorCode@PLT
	movl	(%rbx), %ecx
	movq	-864(%rbp), %r11
	testl	%ecx, %ecx
	jle	.L448
.L441:
	movq	(%r15), %rax
	movq	%r11, -864(%rbp)
	movq	%r15, %rdi
	xorl	%r15d, %r15d
	call	*8(%rax)
	movq	-864(%rbp), %r11
	jmp	.L391
.L402:
	movl	-820(%rbp), %edx
	jmp	.L403
.L408:
	movq	-872(%rbp), %rdi
	movq	%r12, %rsi
	leaq	-640(%rbp), %r12
	movq	%r11, -888(%rbp)
	movq	%r10, -880(%rbp)
	call	_ZN6icu_676LocaleaSERKS0_@PLT
	movq	%r13, %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString8copyFromERKS0_a@PLT
	leaq	-704(%rbp), %rax
	movl	$3, %esi
	movq	-888(%rbp), %r11
	movq	%rax, -864(%rbp)
	movq	-880(%rbp), %r10
	jmp	.L429
.L492:
	movq	-888(%rbp), %rdx
	leaq	_ZL19gDoubleCurrencySign(%rip), %rax
	movl	$2, %ecx
	movq	%r15, %rdi
	movl	$1, %esi
	movq	%r11, -896(%rbp)
	movq	%r10, -880(%rbp)
	movq	%rax, -840(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movl	$1, %ecx
	movl	$1, %esi
	leaq	_ZL19gSingleCurrencySign(%rip), %rax
	movq	%rax, -848(%rbp)
	leaq	-512(%rbp), %rax
	leaq	-848(%rbp), %rdx
	movq	%rax, %rdi
	movq	%rax, -872(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movzwl	-280(%rbp), %eax
	movq	-880(%rbp), %r10
	movq	-896(%rbp), %r11
	testw	%ax, %ax
	js	.L433
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L434:
	movzwl	-504(%rbp), %eax
	testw	%ax, %ax
	js	.L435
	movswl	%ax, %r9d
	sarl	$5, %r9d
.L436:
	movzwl	-824(%rbp), %eax
	testw	%ax, %ax
	js	.L437
	movswl	%ax, %edx
	sarl	$5, %edx
.L438:
	subq	$8, %rsp
	movq	-872(%rbp), %r13
	movq	%r11, %rdi
	xorl	%esi, %esi
	pushq	%rcx
	xorl	%r8d, %r8d
	pushq	$0
	movq	%r13, %rcx
	pushq	%r15
	movq	%r10, -880(%rbp)
	movq	%r11, -872(%rbp)
	call	_ZN6icu_6713UnicodeString14findAndReplaceEiiRKS0_iiS2_ii@PLT
	addq	$32, %rsp
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-880(%rbp), %r10
	movq	-872(%rbp), %r11
	jmp	.L398
.L423:
	testl	%ecx, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	cmovle	%ecx, %r8d
	js	.L425
	movl	%ecx, %r9d
	subl	%r8d, %r9d
	cmpl	%ecx, %r9d
	cmovg	%ecx, %r9d
.L425:
	andl	$2, %edx
	movq	-864(%rbp), %rdi
	movl	%esi, %edx
	movq	%r11, -888(%rbp)
	leaq	-278(%rbp), %rcx
	cmove	-264(%rbp), %rcx
	xorl	%esi, %esi
	movq	%r10, -880(%rbp)
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	movq	-888(%rbp), %r11
	movq	-880(%rbp), %r10
	jmp	.L424
.L409:
	movl	-692(%rbp), %edx
	jmp	.L410
.L421:
	movl	-692(%rbp), %esi
	jmp	.L422
.L419:
	movl	-276(%rbp), %ecx
	jmp	.L420
.L417:
	movl	-628(%rbp), %edx
	jmp	.L418
.L415:
	movl	-756(%rbp), %r9d
	jmp	.L416
.L437:
	movl	-820(%rbp), %edx
	jmp	.L438
.L435:
	movl	-500(%rbp), %r9d
	jmp	.L436
.L433:
	movl	-276(%rbp), %ecx
	jmp	.L434
.L494:
	movq	-576(%rbp), %rdi
	movl	%esi, -888(%rbp)
	movq	%r10, -880(%rbp)
	call	uprv_free_67@PLT
	movq	-880(%rbp), %r10
	movl	-888(%rbp), %esi
	movq	-896(%rbp), %r11
	jmp	.L429
.L413:
	movl	-756(%rbp), %r8d
	jmp	.L414
.L489:
	call	__stack_chk_fail@PLT
.L392:
	cmpl	$0, (%rbx)
	jg	.L444
	movl	$7, (%rbx)
	jmp	.L444
.L491:
	leaq	-832(%rbp), %rdi
	xorl	%r15d, %r15d
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L389
.L439:
	movl	$7, (%rbx)
	jmp	.L394
.L430:
	movl	$7, (%rbx)
	movq	-872(%rbp), %rdi
	movq	%r11, -888(%rbp)
	movq	%r10, -880(%rbp)
	call	_ZN6icu_676LocaleD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-864(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-880(%rbp), %r10
	movq	-888(%rbp), %r11
	jmp	.L394
	.cfi_endproc
.LFE5757:
	.size	_ZN6icu_6712NumberFormat12makeInstanceERKNS_6LocaleE18UNumberFormatStyleaR10UErrorCode.part.0, .-_ZN6icu_6712NumberFormat12makeInstanceERKNS_6LocaleE18UNumberFormatStyleaR10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712NumberFormat12makeInstanceERKNS_6LocaleE18UNumberFormatStyleaR10UErrorCode
	.type	_ZN6icu_6712NumberFormat12makeInstanceERKNS_6LocaleE18UNumberFormatStyleaR10UErrorCode, @function
_ZN6icu_6712NumberFormat12makeInstanceERKNS_6LocaleE18UNumberFormatStyleaR10UErrorCode:
.LFB4077:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L495
	cmpl	$16, %esi
	ja	.L499
	movsbl	%dl, %edx
	jmp	_ZN6icu_6712NumberFormat12makeInstanceERKNS_6LocaleE18UNumberFormatStyleaR10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L499:
	movl	$1, (%rcx)
.L495:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4077:
	.size	_ZN6icu_6712NumberFormat12makeInstanceERKNS_6LocaleE18UNumberFormatStyleaR10UErrorCode, .-_ZN6icu_6712NumberFormat12makeInstanceERKNS_6LocaleE18UNumberFormatStyleaR10UErrorCode
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC2:
	.string	"cf"
.LC3:
	.string	"account"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712NumberFormat22internalCreateInstanceERKNS_6LocaleE18UNumberFormatStyleR10UErrorCode
	.type	_ZN6icu_6712NumberFormat22internalCreateInstanceERKNS_6LocaleE18UNumberFormatStyleR10UErrorCode, @function
_ZN6icu_6712NumberFormat22internalCreateInstanceERKNS_6LocaleE18UNumberFormatStyleR10UErrorCode:
.LFB4055:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$2, %esi
	je	.L516
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L515
	cmpl	$16, %esi
	ja	.L517
.L506:
	movq	%r13, %rcx
	xorl	%edx, %edx
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6712NumberFormat12makeInstanceERKNS_6LocaleE18UNumberFormatStyleaR10UErrorCode.part.0
.L500:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L518
	addq	$64, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L517:
	.cfi_restore_state
	movl	$1, (%rdx)
.L515:
	xorl	%eax, %eax
	jmp	.L500
	.p2align 4,,10
	.p2align 3
.L516:
	leaq	-80(%rbp), %r15
	pxor	%xmm0, %xmm0
	leaq	-84(%rbp), %r8
	movl	$32, %ecx
	movq	%r15, %rdx
	leaq	.LC2(%rip), %rsi
	movl	$0, -84(%rbp)
	movaps	%xmm0, -80(%rbp)
	movaps	%xmm0, -64(%rbp)
	call	_ZNK6icu_676Locale15getKeywordValueEPKcPciR10UErrorCode@PLT
	movl	-84(%rbp), %edx
	testl	%edx, %edx
	jg	.L510
	testl	%eax, %eax
	jg	.L502
.L510:
	movl	0(%r13), %eax
.L504:
	testl	%eax, %eax
	jle	.L506
	xorl	%eax, %eax
	jmp	.L500
	.p2align 4,,10
	.p2align 3
.L502:
	movl	$8, %ecx
	leaq	.LC3(%rip), %rdi
	movq	%r15, %rsi
	movl	0(%r13), %eax
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	jne	.L504
	movl	$12, %r12d
	testl	%eax, %eax
	jle	.L506
	xorl	%eax, %eax
	jmp	.L500
.L518:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4055:
	.size	_ZN6icu_6712NumberFormat22internalCreateInstanceERKNS_6LocaleE18UNumberFormatStyleR10UErrorCode, .-_ZN6icu_6712NumberFormat22internalCreateInstanceERKNS_6LocaleE18UNumberFormatStyleR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712NumberFormat12makeInstanceERKNS_6LocaleE18UNumberFormatStyleR10UErrorCode
	.type	_ZN6icu_6712NumberFormat12makeInstanceERKNS_6LocaleE18UNumberFormatStyleR10UErrorCode, @function
_ZN6icu_6712NumberFormat12makeInstanceERKNS_6LocaleE18UNumberFormatStyleR10UErrorCode:
.LFB4076:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	movq	%rdx, %rcx
	testl	%eax, %eax
	jg	.L519
	cmpl	$16, %esi
	ja	.L523
	xorl	%edx, %edx
	jmp	_ZN6icu_6712NumberFormat12makeInstanceERKNS_6LocaleE18UNumberFormatStyleaR10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L523:
	movl	$1, (%rdx)
.L519:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4076:
	.size	_ZN6icu_6712NumberFormat12makeInstanceERKNS_6LocaleE18UNumberFormatStyleR10UErrorCode, .-_ZN6icu_6712NumberFormat12makeInstanceERKNS_6LocaleE18UNumberFormatStyleR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712NumberFormat24createScientificInstanceERKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_6712NumberFormat24createScientificInstanceERKNS_6LocaleER10UErrorCode, @function
_ZN6icu_6712NumberFormat24createScientificInstanceERKNS_6LocaleER10UErrorCode:
.LFB4053:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	movq	%rsi, %rcx
	testl	%eax, %eax
	jg	.L525
	xorl	%edx, %edx
	movl	$4, %esi
	jmp	_ZN6icu_6712NumberFormat12makeInstanceERKNS_6LocaleE18UNumberFormatStyleaR10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L525:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4053:
	.size	_ZN6icu_6712NumberFormat24createScientificInstanceERKNS_6LocaleER10UErrorCode, .-_ZN6icu_6712NumberFormat24createScientificInstanceERKNS_6LocaleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712NumberFormat21createPercentInstanceERKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_6712NumberFormat21createPercentInstanceERKNS_6LocaleER10UErrorCode, @function
_ZN6icu_6712NumberFormat21createPercentInstanceERKNS_6LocaleER10UErrorCode:
.LFB4051:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	movq	%rsi, %rcx
	testl	%eax, %eax
	jg	.L527
	xorl	%edx, %edx
	movl	$3, %esi
	jmp	_ZN6icu_6712NumberFormat12makeInstanceERKNS_6LocaleE18UNumberFormatStyleaR10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L527:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4051:
	.size	_ZN6icu_6712NumberFormat21createPercentInstanceERKNS_6LocaleER10UErrorCode, .-_ZN6icu_6712NumberFormat21createPercentInstanceERKNS_6LocaleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712NumberFormat21createPercentInstanceER10UErrorCode
	.type	_ZN6icu_6712NumberFormat21createPercentInstanceER10UErrorCode, @function
_ZN6icu_6712NumberFormat21createPercentInstanceER10UErrorCode:
.LFB4050:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN6icu_676Locale10getDefaultEv@PLT
	movl	(%r12), %edx
	testl	%edx, %edx
	jg	.L529
	addq	$8, %rsp
	movq	%r12, %rcx
	movq	%rax, %rdi
	xorl	%edx, %edx
	popq	%r12
	movl	$3, %esi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6712NumberFormat12makeInstanceERKNS_6LocaleE18UNumberFormatStyleaR10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L529:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4050:
	.size	_ZN6icu_6712NumberFormat21createPercentInstanceER10UErrorCode, .-_ZN6icu_6712NumberFormat21createPercentInstanceER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712NumberFormat24createScientificInstanceER10UErrorCode
	.type	_ZN6icu_6712NumberFormat24createScientificInstanceER10UErrorCode, @function
_ZN6icu_6712NumberFormat24createScientificInstanceER10UErrorCode:
.LFB4052:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN6icu_676Locale10getDefaultEv@PLT
	movl	(%r12), %edx
	testl	%edx, %edx
	jg	.L532
	addq	$8, %rsp
	movq	%r12, %rcx
	movq	%rax, %rdi
	xorl	%edx, %edx
	popq	%r12
	movl	$4, %esi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6712NumberFormat12makeInstanceERKNS_6LocaleE18UNumberFormatStyleaR10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L532:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4052:
	.size	_ZN6icu_6712NumberFormat24createScientificInstanceER10UErrorCode, .-_ZN6icu_6712NumberFormat24createScientificInstanceER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712NumberFormat22createCurrencyInstanceERKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_6712NumberFormat22createCurrencyInstanceERKNS_6LocaleER10UErrorCode, @function
_ZN6icu_6712NumberFormat22createCurrencyInstanceERKNS_6LocaleER10UErrorCode:
.LFB4049:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movl	$32, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-80(%rbp), %r14
	movq	%rsi, %r13
	leaq	-84(%rbp), %r8
	pushq	%r12
	movq	%r14, %rdx
	leaq	.LC2(%rip), %rsi
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$0, -84(%rbp)
	movaps	%xmm0, -80(%rbp)
	movaps	%xmm0, -64(%rbp)
	call	_ZNK6icu_676Locale15getKeywordValueEPKcPciR10UErrorCode@PLT
	movl	-84(%rbp), %ecx
	testl	%ecx, %ecx
	jg	.L538
	testl	%eax, %eax
	jg	.L548
.L538:
	movl	$2, %esi
.L535:
	movl	0(%r13), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L534
	movq	%r13, %rcx
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN6icu_6712NumberFormat12makeInstanceERKNS_6LocaleE18UNumberFormatStyleaR10UErrorCode.part.0
.L534:
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L549
	addq	$72, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L548:
	.cfi_restore_state
	movq	%r14, %rsi
	movl	$8, %ecx
	leaq	.LC3(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	movsbl	%al, %eax
	cmpl	$1, %eax
	sbbl	%esi, %esi
	andl	$10, %esi
	addl	$2, %esi
	jmp	.L535
.L549:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4049:
	.size	_ZN6icu_6712NumberFormat22createCurrencyInstanceERKNS_6LocaleER10UErrorCode, .-_ZN6icu_6712NumberFormat22createCurrencyInstanceERKNS_6LocaleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712NumberFormat22createCurrencyInstanceER10UErrorCode
	.type	_ZN6icu_6712NumberFormat22createCurrencyInstanceER10UErrorCode, @function
_ZN6icu_6712NumberFormat22createCurrencyInstanceER10UErrorCode:
.LFB4048:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-80(%rbp), %r14
	movq	%rdi, %r13
	pushq	%r12
	subq	$72, %rsp
	.cfi_offset 12, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_676Locale10getDefaultEv@PLT
	pxor	%xmm0, %xmm0
	leaq	-84(%rbp), %r8
	movq	%r14, %rdx
	movl	$32, %ecx
	movq	%rax, %rdi
	movq	%rax, %r12
	movaps	%xmm0, -80(%rbp)
	leaq	.LC2(%rip), %rsi
	movl	$0, -84(%rbp)
	movaps	%xmm0, -64(%rbp)
	call	_ZNK6icu_676Locale15getKeywordValueEPKcPciR10UErrorCode@PLT
	movl	-84(%rbp), %ecx
	testl	%ecx, %ecx
	jg	.L554
	testl	%eax, %eax
	jg	.L564
.L554:
	movl	$2, %esi
.L551:
	movl	0(%r13), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L550
	movq	%r13, %rcx
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN6icu_6712NumberFormat12makeInstanceERKNS_6LocaleE18UNumberFormatStyleaR10UErrorCode.part.0
.L550:
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L565
	addq	$72, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L564:
	.cfi_restore_state
	movq	%r14, %rsi
	movl	$8, %ecx
	leaq	.LC3(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	movsbl	%al, %eax
	cmpl	$1, %eax
	sbbl	%esi, %esi
	andl	$10, %esi
	addl	$2, %esi
	jmp	.L551
.L565:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4048:
	.size	_ZN6icu_6712NumberFormat22createCurrencyInstanceER10UErrorCode, .-_ZN6icu_6712NumberFormat22createCurrencyInstanceER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEE12createObjectEPKvR10UErrorCode
	.type	_ZNK6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEE12createObjectEPKvR10UErrorCode, @function
_ZNK6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEE12createObjectEPKvR10UErrorCode:
.LFB4073:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-272(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	xorl	%edx, %edx
	subq	$248, %rsp
	movq	56(%rdi), %rsi
	movq	%r12, %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L567
	movq	%rbx, %rcx
	xorl	%edx, %edx
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6712NumberFormat12makeInstanceERKNS_6LocaleE18UNumberFormatStyleaR10UErrorCode.part.0
	movq	%rax, %r13
.L567:
	movq	%r12, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L572
	movl	$32, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L570
	movq	$0, 8(%rax)
	movq	%r12, %rdi
	movq	$0, 16(%rax)
	leaq	16+_ZTVN6icu_6718SharedNumberFormatE(%rip), %rax
	movq	%rax, (%r12)
	movq	%r13, 24(%r12)
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
.L566:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L577
	addq	$248, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L572:
	.cfi_restore_state
	xorl	%r12d, %r12d
	jmp	.L566
.L577:
	call	__stack_chk_fail@PLT
.L570:
	movl	$7, (%rbx)
	testq	%r13, %r13
	je	.L572
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*8(%rax)
	jmp	.L566
	.cfi_endproc
.LFE4073:
	.size	_ZNK6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEE12createObjectEPKvR10UErrorCode, .-_ZNK6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEE12createObjectEPKvR10UErrorCode
	.align 2
	.p2align 4
	.type	_ZN6icu_6712NumberFormat14createInstanceERKNS_6LocaleE18UNumberFormatStyleR10UErrorCode.part.0, @function
_ZN6icu_6712NumberFormat14createInstanceERKNS_6LocaleE18UNumberFormatStyleR10UErrorCode.part.0:
.LFB5759:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$296, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rsi), %r9d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r9d, %r9d
	jg	.L587
	movq	%rsi, %rbx
	movq	%rdi, %r13
	movq	%rsi, %rdi
	call	_ZN6icu_6712UnifiedCache11getInstanceER10UErrorCode@PLT
	movl	(%rbx), %r8d
	movq	%rax, %r12
	testl	%r8d, %r8d
	jle	.L596
.L587:
	xorl	%r12d, %r12d
.L578:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L597
	addq	$296, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L596:
	.cfi_restore_state
	leaq	-288(%rbp), %r9
	movq	%r13, %rsi
	xorl	%r13d, %r13d
	movl	$0, -296(%rbp)
	movq	%r9, %rdi
	leaq	16+_ZTVN6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEEE(%rip), %r15
	movq	%r9, -328(%rbp)
	leaq	-304(%rbp), %r14
	movb	$0, -292(%rbp)
	movq	%r15, -304(%rbp)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movl	(%rbx), %edi
	movq	-328(%rbp), %r9
	testl	%edi, %edi
	jle	.L598
.L580:
	movq	%r9, %rdi
	movq	%r15, -304(%rbp)
	call	_ZN6icu_676LocaleD1Ev@PLT
	leaq	16+_ZTVN6icu_678CacheKeyINS_18SharedNumberFormatEEE(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -304(%rbp)
	call	_ZN6icu_6712CacheKeyBaseD2Ev@PLT
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L587
	movq	24(%r13), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	testq	%r12, %r12
	jne	.L578
	movl	$7, (%rbx)
	jmp	.L578
	.p2align 4,,10
	.p2align 3
.L598:
	xorl	%ecx, %ecx
	leaq	-312(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	-316(%rbp), %r8
	movl	$0, -316(%rbp)
	movq	$0, -312(%rbp)
	call	_ZNK6icu_6712UnifiedCache4_getERKNS_12CacheKeyBaseERPKNS_12SharedObjectEPKvR10UErrorCode@PLT
	movl	-316(%rbp), %eax
	movq	-312(%rbp), %r13
	movq	-328(%rbp), %r9
	testl	%eax, %eax
	jle	.L599
	testq	%r13, %r13
	jne	.L600
.L584:
	movl	%eax, (%rbx)
	jmp	.L580
	.p2align 4,,10
	.p2align 3
.L600:
	movq	%r13, %r12
	xorl	%r13d, %r13d
.L583:
	movq	%r12, %rdi
	movq	%r9, -328(%rbp)
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	movl	(%rbx), %esi
	movl	-316(%rbp), %eax
	movq	-328(%rbp), %r9
	testl	%esi, %esi
	je	.L584
	testl	%eax, %eax
	jle	.L580
	movl	%eax, (%rbx)
	jmp	.L580
	.p2align 4,,10
	.p2align 3
.L599:
	testq	%r13, %r13
	je	.L582
	movq	%r13, %rdi
	movq	%r13, %r12
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
	movq	-328(%rbp), %r9
	jmp	.L583
.L582:
	movl	(%rbx), %edx
	testl	%edx, %edx
	jne	.L580
	movl	%eax, (%rbx)
	jmp	.L580
.L597:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5759:
	.size	_ZN6icu_6712NumberFormat14createInstanceERKNS_6LocaleE18UNumberFormatStyleR10UErrorCode.part.0, .-_ZN6icu_6712NumberFormat14createInstanceERKNS_6LocaleE18UNumberFormatStyleR10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712NumberFormat14createInstanceERKNS_6LocaleE18UNumberFormatStyleR10UErrorCode
	.type	_ZN6icu_6712NumberFormat14createInstanceERKNS_6LocaleE18UNumberFormatStyleR10UErrorCode, @function
_ZN6icu_6712NumberFormat14createInstanceERKNS_6LocaleE18UNumberFormatStyleR10UErrorCode:
.LFB4056:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	subq	$64, %rsp
	.cfi_offset 12, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$1, %esi
	jne	.L621
	movq	%rdx, %rsi
	call	_ZN6icu_6712NumberFormat14createInstanceERKNS_6LocaleE18UNumberFormatStyleR10UErrorCode.part.0
.L601:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L622
	addq	$64, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L621:
	.cfi_restore_state
	movl	%esi, %r12d
	cmpl	$2, %esi
	je	.L623
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L620
	cmpl	$16, %esi
	ja	.L624
.L606:
	movq	%r13, %rcx
	xorl	%edx, %edx
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6712NumberFormat12makeInstanceERKNS_6LocaleE18UNumberFormatStyleaR10UErrorCode.part.0
	jmp	.L601
	.p2align 4,,10
	.p2align 3
.L624:
	movl	$1, (%rdx)
.L620:
	xorl	%eax, %eax
	jmp	.L601
	.p2align 4,,10
	.p2align 3
.L623:
	leaq	-80(%rbp), %r15
	pxor	%xmm0, %xmm0
	leaq	-84(%rbp), %r8
	movl	$32, %ecx
	movq	%r15, %rdx
	leaq	.LC2(%rip), %rsi
	movl	$0, -84(%rbp)
	movaps	%xmm0, -80(%rbp)
	movaps	%xmm0, -64(%rbp)
	call	_ZNK6icu_676Locale15getKeywordValueEPKcPciR10UErrorCode@PLT
	movl	-84(%rbp), %edx
	testl	%edx, %edx
	jg	.L604
	testl	%eax, %eax
	jg	.L625
.L604:
	movl	0(%r13), %eax
.L619:
	testl	%eax, %eax
	jle	.L606
	xorl	%eax, %eax
	jmp	.L601
	.p2align 4,,10
	.p2align 3
.L625:
	movl	$8, %ecx
	leaq	.LC3(%rip), %rdi
	movq	%r15, %rsi
	movl	0(%r13), %eax
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	jne	.L619
	movl	$12, %r12d
	jmp	.L619
.L622:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4056:
	.size	_ZN6icu_6712NumberFormat14createInstanceERKNS_6LocaleE18UNumberFormatStyleR10UErrorCode, .-_ZN6icu_6712NumberFormat14createInstanceERKNS_6LocaleE18UNumberFormatStyleR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712NumberFormat14createInstanceER10UErrorCode
	.type	_ZN6icu_6712NumberFormat14createInstanceER10UErrorCode, @function
_ZN6icu_6712NumberFormat14createInstanceER10UErrorCode:
.LFB4046:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN6icu_676Locale10getDefaultEv@PLT
	addq	$8, %rsp
	movq	%r12, %rsi
	movq	%rax, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6712NumberFormat14createInstanceERKNS_6LocaleE18UNumberFormatStyleR10UErrorCode.part.0
	.cfi_endproc
.LFE4046:
	.size	_ZN6icu_6712NumberFormat14createInstanceER10UErrorCode, .-_ZN6icu_6712NumberFormat14createInstanceER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712NumberFormat14createInstanceERKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_6712NumberFormat14createInstanceERKNS_6LocaleER10UErrorCode, @function
_ZN6icu_6712NumberFormat14createInstanceERKNS_6LocaleER10UErrorCode:
.LFB4047:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_6712NumberFormat14createInstanceERKNS_6LocaleE18UNumberFormatStyleR10UErrorCode.part.0
	.cfi_endproc
.LFE4047:
	.size	_ZN6icu_6712NumberFormat14createInstanceERKNS_6LocaleER10UErrorCode, .-_ZN6icu_6712NumberFormat14createInstanceERKNS_6LocaleER10UErrorCode
	.weak	_ZTSN6icu_6712NumberFormatE
	.section	.rodata._ZTSN6icu_6712NumberFormatE,"aG",@progbits,_ZTSN6icu_6712NumberFormatE,comdat
	.align 16
	.type	_ZTSN6icu_6712NumberFormatE, @object
	.size	_ZTSN6icu_6712NumberFormatE, 24
_ZTSN6icu_6712NumberFormatE:
	.string	"N6icu_6712NumberFormatE"
	.weak	_ZTIN6icu_6712NumberFormatE
	.section	.data.rel.ro._ZTIN6icu_6712NumberFormatE,"awG",@progbits,_ZTIN6icu_6712NumberFormatE,comdat
	.align 8
	.type	_ZTIN6icu_6712NumberFormatE, @object
	.size	_ZTIN6icu_6712NumberFormatE, 24
_ZTIN6icu_6712NumberFormatE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6712NumberFormatE
	.quad	_ZTIN6icu_676FormatE
	.weak	_ZTSN6icu_6718SharedNumberFormatE
	.section	.rodata._ZTSN6icu_6718SharedNumberFormatE,"aG",@progbits,_ZTSN6icu_6718SharedNumberFormatE,comdat
	.align 16
	.type	_ZTSN6icu_6718SharedNumberFormatE, @object
	.size	_ZTSN6icu_6718SharedNumberFormatE, 30
_ZTSN6icu_6718SharedNumberFormatE:
	.string	"N6icu_6718SharedNumberFormatE"
	.weak	_ZTIN6icu_6718SharedNumberFormatE
	.section	.data.rel.ro._ZTIN6icu_6718SharedNumberFormatE,"awG",@progbits,_ZTIN6icu_6718SharedNumberFormatE,comdat
	.align 8
	.type	_ZTIN6icu_6718SharedNumberFormatE, @object
	.size	_ZTIN6icu_6718SharedNumberFormatE, 24
_ZTIN6icu_6718SharedNumberFormatE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6718SharedNumberFormatE
	.quad	_ZTIN6icu_6712SharedObjectE
	.weak	_ZTSN6icu_678CacheKeyINS_18SharedNumberFormatEEE
	.section	.rodata._ZTSN6icu_678CacheKeyINS_18SharedNumberFormatEEE,"aG",@progbits,_ZTSN6icu_678CacheKeyINS_18SharedNumberFormatEEE,comdat
	.align 32
	.type	_ZTSN6icu_678CacheKeyINS_18SharedNumberFormatEEE, @object
	.size	_ZTSN6icu_678CacheKeyINS_18SharedNumberFormatEEE, 45
_ZTSN6icu_678CacheKeyINS_18SharedNumberFormatEEE:
	.string	"N6icu_678CacheKeyINS_18SharedNumberFormatEEE"
	.weak	_ZTIN6icu_678CacheKeyINS_18SharedNumberFormatEEE
	.section	.data.rel.ro._ZTIN6icu_678CacheKeyINS_18SharedNumberFormatEEE,"awG",@progbits,_ZTIN6icu_678CacheKeyINS_18SharedNumberFormatEEE,comdat
	.align 8
	.type	_ZTIN6icu_678CacheKeyINS_18SharedNumberFormatEEE, @object
	.size	_ZTIN6icu_678CacheKeyINS_18SharedNumberFormatEEE, 24
_ZTIN6icu_678CacheKeyINS_18SharedNumberFormatEEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_678CacheKeyINS_18SharedNumberFormatEEE
	.quad	_ZTIN6icu_6712CacheKeyBaseE
	.weak	_ZTSN6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEEE
	.section	.rodata._ZTSN6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEEE,"aG",@progbits,_ZTSN6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEEE,comdat
	.align 32
	.type	_ZTSN6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEEE, @object
	.size	_ZTSN6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEEE, 52
_ZTSN6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEEE:
	.string	"N6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEEE"
	.weak	_ZTIN6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEEE
	.section	.data.rel.ro._ZTIN6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEEE,"awG",@progbits,_ZTIN6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEEE,comdat
	.align 8
	.type	_ZTIN6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEEE, @object
	.size	_ZTIN6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEEE, 24
_ZTIN6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEEE
	.quad	_ZTIN6icu_678CacheKeyINS_18SharedNumberFormatEEE
	.weak	_ZTVN6icu_6712NumberFormatE
	.section	.data.rel.ro._ZTVN6icu_6712NumberFormatE,"awG",@progbits,_ZTVN6icu_6712NumberFormatE,comdat
	.align 8
	.type	_ZTVN6icu_6712NumberFormatE, @object
	.size	_ZTVN6icu_6712NumberFormatE, 312
_ZTVN6icu_6712NumberFormatE:
	.quad	0
	.quad	_ZTIN6icu_6712NumberFormatE
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	_ZNK6icu_6712NumberFormateqERKNS_6FormatE
	.quad	__cxa_pure_virtual
	.quad	_ZNK6icu_6712NumberFormat6formatERKNS_11FormattableERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.quad	_ZNK6icu_6712NumberFormat6formatERKNS_11FormattableERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode
	.quad	_ZNK6icu_6712NumberFormat11parseObjectERKNS_13UnicodeStringERNS_11FormattableERNS_13ParsePositionE
	.quad	__cxa_pure_virtual
	.quad	_ZNK6icu_6712NumberFormat6formatEdRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.quad	_ZNK6icu_6712NumberFormat6formatEdRNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode
	.quad	__cxa_pure_virtual
	.quad	_ZNK6icu_6712NumberFormat6formatEiRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.quad	_ZNK6icu_6712NumberFormat6formatEiRNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode
	.quad	_ZNK6icu_6712NumberFormat6formatElRNS_13UnicodeStringERNS_13FieldPositionE
	.quad	_ZNK6icu_6712NumberFormat6formatElRNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.quad	_ZNK6icu_6712NumberFormat6formatElRNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode
	.quad	_ZNK6icu_6712NumberFormat6formatENS_11StringPieceERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode
	.quad	_ZNK6icu_6712NumberFormat6formatERKNS_6number4impl15DecimalQuantityERNS_13UnicodeStringEPNS_21FieldPositionIteratorER10UErrorCode
	.quad	_ZNK6icu_6712NumberFormat6formatERKNS_6number4impl15DecimalQuantityERNS_13UnicodeStringERNS_13FieldPositionER10UErrorCode
	.quad	__cxa_pure_virtual
	.quad	_ZNK6icu_6712NumberFormat5parseERKNS_13UnicodeStringERNS_11FormattableER10UErrorCode
	.quad	_ZNK6icu_6712NumberFormat13parseCurrencyERKNS_13UnicodeStringERNS_13ParsePositionE
	.quad	_ZN6icu_6712NumberFormat19setParseIntegerOnlyEa
	.quad	_ZN6icu_6712NumberFormat10setLenientEa
	.quad	_ZNK6icu_6712NumberFormat9isLenientEv
	.quad	_ZN6icu_6712NumberFormat15setGroupingUsedEa
	.quad	_ZN6icu_6712NumberFormat23setMaximumIntegerDigitsEi
	.quad	_ZN6icu_6712NumberFormat23setMinimumIntegerDigitsEi
	.quad	_ZN6icu_6712NumberFormat24setMaximumFractionDigitsEi
	.quad	_ZN6icu_6712NumberFormat24setMinimumFractionDigitsEi
	.quad	_ZN6icu_6712NumberFormat11setCurrencyEPKDsR10UErrorCode
	.quad	_ZN6icu_6712NumberFormat10setContextE15UDisplayContextR10UErrorCode
	.quad	_ZNK6icu_6712NumberFormat10getContextE19UDisplayContextTypeR10UErrorCode
	.quad	_ZNK6icu_6712NumberFormat15getRoundingModeEv
	.quad	_ZN6icu_6712NumberFormat15setRoundingModeENS0_13ERoundingModeE
	.quad	_ZNK6icu_6712NumberFormat20getEffectiveCurrencyEPDsR10UErrorCode
	.weak	_ZTVN6icu_6718SharedNumberFormatE
	.section	.data.rel.ro._ZTVN6icu_6718SharedNumberFormatE,"awG",@progbits,_ZTVN6icu_6718SharedNumberFormatE,comdat
	.align 8
	.type	_ZTVN6icu_6718SharedNumberFormatE, @object
	.size	_ZTVN6icu_6718SharedNumberFormatE, 40
_ZTVN6icu_6718SharedNumberFormatE:
	.quad	0
	.quad	_ZTIN6icu_6718SharedNumberFormatE
	.quad	_ZN6icu_6718SharedNumberFormatD1Ev
	.quad	_ZN6icu_6718SharedNumberFormatD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.weak	_ZTVN6icu_678CacheKeyINS_18SharedNumberFormatEEE
	.section	.data.rel.ro._ZTVN6icu_678CacheKeyINS_18SharedNumberFormatEEE,"awG",@progbits,_ZTVN6icu_678CacheKeyINS_18SharedNumberFormatEEE,comdat
	.align 8
	.type	_ZTVN6icu_678CacheKeyINS_18SharedNumberFormatEEE, @object
	.size	_ZTVN6icu_678CacheKeyINS_18SharedNumberFormatEEE, 80
_ZTVN6icu_678CacheKeyINS_18SharedNumberFormatEEE:
	.quad	0
	.quad	_ZTIN6icu_678CacheKeyINS_18SharedNumberFormatEEE
	.quad	0
	.quad	0
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZNK6icu_678CacheKeyINS_18SharedNumberFormatEE8hashCodeEv
	.quad	__cxa_pure_virtual
	.quad	_ZNK6icu_678CacheKeyINS_18SharedNumberFormatEEeqERKNS_12CacheKeyBaseE
	.quad	__cxa_pure_virtual
	.quad	_ZNK6icu_678CacheKeyINS_18SharedNumberFormatEE16writeDescriptionEPci
	.weak	_ZTVN6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEEE
	.section	.data.rel.ro._ZTVN6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEEE,"awG",@progbits,_ZTVN6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEEE,comdat
	.align 8
	.type	_ZTVN6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEEE, @object
	.size	_ZTVN6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEEE, 80
_ZTVN6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEEE:
	.quad	0
	.quad	_ZTIN6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEEE
	.quad	_ZN6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEED1Ev
	.quad	_ZN6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEED0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZNK6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEE8hashCodeEv
	.quad	_ZNK6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEE5cloneEv
	.quad	_ZNK6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEEeqERKNS_12CacheKeyBaseE
	.quad	_ZNK6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEE12createObjectEPKvR10UErrorCode
	.quad	_ZNK6icu_6714LocaleCacheKeyINS_18SharedNumberFormatEE16writeDescriptionEPci
	.local	_ZZN6icu_6712NumberFormat12makeInstanceERKNS_6LocaleE18UNumberFormatStyleaR10UErrorCodeE12nscacheMutex
	.comm	_ZZN6icu_6712NumberFormat12makeInstanceERKNS_6LocaleE18UNumberFormatStyleaR10UErrorCodeE12nscacheMutex,56,32
	.local	_ZZN6icu_6712NumberFormat16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6712NumberFormat16getStaticClassIDEvE7classID,1,1
	.local	_ZL16gNSCacheInitOnce
	.comm	_ZL16gNSCacheInitOnce,8,8
	.local	_ZL21NumberingSystem_cache
	.comm	_ZL21NumberingSystem_cache,8,8
	.section	.rodata
	.align 32
	.type	_ZL17gFormatCldrStyles, @object
	.size	_ZL17gFormatCldrStyles, 68
_ZL17gFormatCldrStyles:
	.long	5
	.long	0
	.long	1
	.long	3
	.long	4
	.long	5
	.long	5
	.long	5
	.long	5
	.long	5
	.long	1
	.long	1
	.long	2
	.long	1
	.long	5
	.long	5
	.long	1
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	_ZL25gLastResortNumberPatterns, @object
	.size	_ZL25gLastResortNumberPatterns, 136
_ZL25gLastResortNumberPatterns:
	.quad	0
	.quad	_ZL21gLastResortDecimalPat
	.quad	_ZL22gLastResortCurrencyPat
	.quad	_ZL21gLastResortPercentPat
	.quad	_ZL24gLastResortScientificPat
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZL25gLastResortIsoCurrencyPat
	.quad	_ZL28gLastResortPluralCurrencyPat
	.quad	_ZL32gLastResortAccountingCurrencyPat
	.quad	_ZL22gLastResortCurrencyPat
	.quad	0
	.quad	0
	.quad	_ZL22gLastResortCurrencyPat
	.globl	_ZN6icu_6712NumberFormat24gDefaultMinIntegerDigitsE
	.section	.rodata
	.align 4
	.type	_ZN6icu_6712NumberFormat24gDefaultMinIntegerDigitsE, @object
	.size	_ZN6icu_6712NumberFormat24gDefaultMinIntegerDigitsE, 4
_ZN6icu_6712NumberFormat24gDefaultMinIntegerDigitsE:
	.long	127
	.globl	_ZN6icu_6712NumberFormat24gDefaultMaxIntegerDigitsE
	.align 4
	.type	_ZN6icu_6712NumberFormat24gDefaultMaxIntegerDigitsE, @object
	.size	_ZN6icu_6712NumberFormat24gDefaultMaxIntegerDigitsE, 4
_ZN6icu_6712NumberFormat24gDefaultMaxIntegerDigitsE:
	.long	2000000000
	.align 2
	.type	_ZL19gDoubleCurrencySign, @object
	.size	_ZL19gDoubleCurrencySign, 6
_ZL19gDoubleCurrencySign:
	.value	164
	.value	164
	.value	0
	.align 2
	.type	_ZL19gSingleCurrencySign, @object
	.size	_ZL19gSingleCurrencySign, 4
_ZL19gSingleCurrencySign:
	.value	164
	.value	0
	.align 16
	.type	_ZL32gLastResortAccountingCurrencyPat, @object
	.size	_ZL32gLastResortAccountingCurrencyPat, 22
_ZL32gLastResortAccountingCurrencyPat:
	.value	164
	.value	160
	.value	35
	.value	44
	.value	35
	.value	35
	.value	48
	.value	46
	.value	48
	.value	48
	.value	0
	.align 16
	.type	_ZL28gLastResortPluralCurrencyPat, @object
	.size	_ZL28gLastResortPluralCurrencyPat, 28
_ZL28gLastResortPluralCurrencyPat:
	.value	35
	.value	44
	.value	35
	.value	35
	.value	48
	.value	46
	.value	35
	.value	35
	.value	35
	.value	32
	.value	164
	.value	164
	.value	164
	.value	0
	.align 16
	.type	_ZL25gLastResortIsoCurrencyPat, @object
	.size	_ZL25gLastResortIsoCurrencyPat, 24
_ZL25gLastResortIsoCurrencyPat:
	.value	164
	.value	164
	.value	160
	.value	35
	.value	44
	.value	35
	.value	35
	.value	48
	.value	46
	.value	48
	.value	48
	.value	0
	.align 8
	.type	_ZL24gLastResortScientificPat, @object
	.size	_ZL24gLastResortScientificPat, 8
_ZL24gLastResortScientificPat:
	.value	35
	.value	69
	.value	48
	.value	0
	.align 8
	.type	_ZL21gLastResortPercentPat, @object
	.size	_ZL21gLastResortPercentPat, 14
_ZL21gLastResortPercentPat:
	.value	35
	.value	44
	.value	35
	.value	35
	.value	48
	.value	37
	.value	0
	.align 16
	.type	_ZL22gLastResortCurrencyPat, @object
	.size	_ZL22gLastResortCurrencyPat, 22
_ZL22gLastResortCurrencyPat:
	.value	164
	.value	160
	.value	35
	.value	44
	.value	35
	.value	35
	.value	48
	.value	46
	.value	48
	.value	48
	.value	0
	.align 16
	.type	_ZL21gLastResortDecimalPat, @object
	.size	_ZL21gLastResortDecimalPat, 20
_ZL21gLastResortDecimalPat:
	.value	35
	.value	44
	.value	35
	.value	35
	.value	48
	.value	46
	.value	35
	.value	35
	.value	35
	.value	0
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	2000000000
	.long	1
	.long	3
	.long	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
