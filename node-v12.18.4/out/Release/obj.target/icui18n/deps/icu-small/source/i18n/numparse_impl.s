	.file	"numparse_impl.cpp"
	.text
	.section	.text._ZNK6icu_678numparse4impl18NumberParseMatcher10isFlexibleEv,"axG",@progbits,_ZNK6icu_678numparse4impl18NumberParseMatcher10isFlexibleEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_678numparse4impl18NumberParseMatcher10isFlexibleEv
	.type	_ZNK6icu_678numparse4impl18NumberParseMatcher10isFlexibleEv, @function
_ZNK6icu_678numparse4impl18NumberParseMatcher10isFlexibleEv:
.LFB2835:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2835:
	.size	_ZNK6icu_678numparse4impl18NumberParseMatcher10isFlexibleEv, .-_ZNK6icu_678numparse4impl18NumberParseMatcher10isFlexibleEv
	.section	.text._ZNK6icu_678numparse4impl18NumberParseMatcher11postProcessERNS1_12ParsedNumberE,"axG",@progbits,_ZNK6icu_678numparse4impl18NumberParseMatcher11postProcessERNS1_12ParsedNumberE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_678numparse4impl18NumberParseMatcher11postProcessERNS1_12ParsedNumberE
	.type	_ZNK6icu_678numparse4impl18NumberParseMatcher11postProcessERNS1_12ParsedNumberE, @function
_ZNK6icu_678numparse4impl18NumberParseMatcher11postProcessERNS1_12ParsedNumberE:
.LFB2836:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2836:
	.size	_ZNK6icu_678numparse4impl18NumberParseMatcher11postProcessERNS1_12ParsedNumberE, .-_ZNK6icu_678numparse4impl18NumberParseMatcher11postProcessERNS1_12ParsedNumberE
	.section	.text._ZN6icu_678numparse4impl19AffixPatternMatcherD2Ev,"axG",@progbits,_ZN6icu_678numparse4impl19AffixPatternMatcherD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_678numparse4impl19AffixPatternMatcherD2Ev
	.type	_ZN6icu_678numparse4impl19AffixPatternMatcherD2Ev, @function
_ZN6icu_678numparse4impl19AffixPatternMatcherD2Ev:
.LFB4582:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_678numparse4impl19AffixPatternMatcherE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpb	$0, 68(%rdi)
	movq	%rax, (%rdi)
	jne	.L8
	leaq	16+_ZTVN6icu_678numparse4impl18ArraySeriesMatcherE(%rip), %rax
	cmpb	$0, 20(%rbx)
	movq	%rax, (%rbx)
	jne	.L9
.L4:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_restore_state
	movq	56(%rdi), %rdi
	call	uprv_free_67@PLT
	leaq	16+_ZTVN6icu_678numparse4impl18ArraySeriesMatcherE(%rip), %rax
	cmpb	$0, 20(%rbx)
	movq	%rax, (%rbx)
	je	.L4
.L9:
	movq	8(%rbx), %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE4582:
	.size	_ZN6icu_678numparse4impl19AffixPatternMatcherD2Ev, .-_ZN6icu_678numparse4impl19AffixPatternMatcherD2Ev
	.weak	_ZN6icu_678numparse4impl19AffixPatternMatcherD1Ev
	.set	_ZN6icu_678numparse4impl19AffixPatternMatcherD1Ev,_ZN6icu_678numparse4impl19AffixPatternMatcherD2Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_678numparse4impl16NumberParserImpl10addMatcherERNS1_18NumberParseMatcherE
	.type	_ZN6icu_678numparse4impl16NumberParserImpl10addMatcherERNS1_18NumberParseMatcherE, @function
_ZN6icu_678numparse4impl16NumberParserImpl10addMatcherERNS1_18NumberParseMatcherE:
.LFB3315:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	12(%rdi), %r12d
	cmpl	24(%rdi), %r12d
	jl	.L19
	leal	(%r12,%r12), %r15d
	testl	%r15d, %r15d
	jle	.L19
	movslq	%r15d, %rdi
	salq	$3, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L20
	movq	16(%rbx), %r8
	testl	%r12d, %r12d
	jg	.L21
	cmpb	$0, 28(%rbx)
	jne	.L22
.L16:
	movq	%r14, 16(%rbx)
	movl	12(%rbx), %r12d
	movl	%r15d, 24(%rbx)
	movb	$1, 28(%rbx)
	jmp	.L12
.L20:
	movl	12(%rbx), %r12d
	.p2align 4,,10
	.p2align 3
.L19:
	movq	16(%rbx), %r14
.L12:
	movslq	%r12d, %rax
	addl	$1, %r12d
	movq	%r13, (%r14,%rax,8)
	movl	%r12d, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	.cfi_restore_state
	cmpl	%r12d, 24(%rbx)
	cmovle	24(%rbx), %r12d
	movq	%r8, %rsi
	movq	%rax, %rdi
	movq	%r8, -56(%rbp)
	cmpl	%r12d, %r15d
	movl	%r12d, %edx
	cmovle	%r15d, %edx
	movslq	%edx, %rdx
	salq	$3, %rdx
	call	memcpy@PLT
	cmpb	$0, 28(%rbx)
	movq	-56(%rbp), %r8
	je	.L16
.L22:
	movq	%r8, %rdi
	call	uprv_free_67@PLT
	jmp	.L16
	.cfi_endproc
.LFE3315:
	.size	_ZN6icu_678numparse4impl16NumberParserImpl10addMatcherERNS1_18NumberParseMatcherE, .-_ZN6icu_678numparse4impl16NumberParserImpl10addMatcherERNS1_18NumberParseMatcherE
	.section	.text._ZN6icu_678numparse4impl19AffixPatternMatcherD0Ev,"axG",@progbits,_ZN6icu_678numparse4impl19AffixPatternMatcherD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_678numparse4impl19AffixPatternMatcherD0Ev
	.type	_ZN6icu_678numparse4impl19AffixPatternMatcherD0Ev, @function
_ZN6icu_678numparse4impl19AffixPatternMatcherD0Ev:
.LFB4584:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_678numparse4impl19AffixPatternMatcherE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	cmpb	$0, 68(%rdi)
	movq	%rax, (%rdi)
	jne	.L27
.L24:
	leaq	16+_ZTVN6icu_678numparse4impl18ArraySeriesMatcherE(%rip), %rax
	cmpb	$0, 20(%r12)
	movq	%rax, (%r12)
	jne	.L28
.L25:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$80, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L27:
	.cfi_restore_state
	movq	56(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L28:
	movq	8(%r12), %rdi
	call	uprv_free_67@PLT
	jmp	.L25
	.cfi_endproc
.LFE4584:
	.size	_ZN6icu_678numparse4impl19AffixPatternMatcherD0Ev, .-_ZN6icu_678numparse4impl19AffixPatternMatcherD0Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev:
.LFB3805:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3805:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei:
.LFB3808:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	cmpl	$40, %esi
	jg	.L42
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L30
	cmpb	$0, 12(%rbx)
	jne	.L43
.L34:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L30:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L34
	.cfi_endproc
.LFE3808:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EED2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev:
.LFB3811:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L46
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3811:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5EOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_:
.LFB3814:
	.cfi_startproc
	endbr64
	movzbl	12(%rsi), %eax
	movq	(%rsi), %r8
	movslq	8(%rsi), %rdx
	movb	%al, 12(%rdi)
	leaq	13(%rsi), %rax
	movq	%r8, (%rdi)
	movl	%edx, 8(%rdi)
	cmpq	%rax, %r8
	je	.L49
	movq	%rax, (%rsi)
	movl	$40, 8(%rsi)
	movb	$0, 12(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	leaq	13(%rdi), %r9
	movq	%r8, %rsi
	movq	%r9, (%rdi)
	movq	%r9, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE3814:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_:
.LFB3816:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 12(%rdi)
	movq	%rsi, %rbx
	jne	.L55
.L51:
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movzbl	12(%rbx), %eax
	movb	%al, 12(%r12)
	movq	(%rbx), %rsi
	leaq	13(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L56
	movq	%rsi, (%r12)
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L56:
	leaq	13(%r12), %rdi
	movslq	8(%rbx), %rdx
	movq	%rdi, (%r12)
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3816:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv:
.LFB3817:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE3817:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv:
.LFB3818:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3818:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv:
.LFB3819:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	addq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3819:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3820:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3820:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3821:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3821:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci:
.LFB3822:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L72
	testl	%edx, %edx
	jle	.L72
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 12(%rdi)
	jne	.L75
.L64:
	movq	%rsi, (%rbx)
	movl	%edx, 8(%rbx)
	movb	$0, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L72:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L75:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	%edx, -28(%rbp)
	movq	%rsi, -24(%rbp)
	call	uprv_free_67@PLT
	movl	-28(%rbp), %edx
	movq	-24(%rbp), %rsi
	jmp	.L64
	.cfi_endproc
.LFE3822:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii:
.LFB3823:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L79
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movl	%esi, %r13d
	movl	%edx, %r12d
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L79
	testl	%r12d, %r12d
	jg	.L86
	cmpb	$0, 12(%rbx)
	jne	.L87
.L81:
	movq	%r14, (%rbx)
	movq	%r14, %rax
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L86:
	.cfi_restore_state
	cmpl	%r12d, 8(%rbx)
	cmovle	8(%rbx), %r12d
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	je	.L81
.L87:
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L79:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3823:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, .-_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi:
.LFB3824:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 12(%rdi)
	movq	%rdi, %rbx
	je	.L89
	movq	(%rdi), %r8
.L90:
	leaq	13(%rbx), %rax
	movl	%r12d, 0(%r13)
	movq	%rax, (%rbx)
	movq	%r8, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L89:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L93
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	cmovg	%eax, %r12d
	movslq	%r12d, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L93
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L93:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3824:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, .-_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv:
.LFB3825:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L100
	ret
	.p2align 4,,10
	.p2align 3
.L100:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3825:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv:
.LFB3826:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3826:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_:
.LFB3827:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3827:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_:
.LFB3828:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3828:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5ERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_:
.LFB3830:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3830:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_:
.LFB3832:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3832:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_678numparse4impl18NumberParseMatcherD2Ev
	.type	_ZN6icu_678numparse4impl18NumberParseMatcherD2Ev, @function
_ZN6icu_678numparse4impl18NumberParseMatcherD2Ev:
.LFB3150:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3150:
	.size	_ZN6icu_678numparse4impl18NumberParseMatcherD2Ev, .-_ZN6icu_678numparse4impl18NumberParseMatcherD2Ev
	.globl	_ZN6icu_678numparse4impl18NumberParseMatcherD1Ev
	.set	_ZN6icu_678numparse4impl18NumberParseMatcherD1Ev,_ZN6icu_678numparse4impl18NumberParseMatcherD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_678numparse4impl18NumberParseMatcherD0Ev
	.type	_ZN6icu_678numparse4impl18NumberParseMatcherD0Ev, @function
_ZN6icu_678numparse4impl18NumberParseMatcherD0Ev:
.LFB3152:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE3152:
	.size	_ZN6icu_678numparse4impl18NumberParseMatcherD0Ev, .-_ZN6icu_678numparse4impl18NumberParseMatcherD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_678numparse4impl16NumberParserImplC2Ei
	.type	_ZN6icu_678numparse4impl16NumberParserImplC2Ei, @function
_ZN6icu_678numparse4impl16NumberParserImplC2Ei:
.LFB3309:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %edx
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	leaq	16+_ZTVN6icu_678numparse4impl15InfinityMatcherE(%rip), %rcx
	movq	%rax, %xmm0
	leaq	16+_ZTVN6icu_678numparse4impl15PlusSignMatcherE(%rip), %rax
	movl	$2, %r8d
	movl	$2, %r9d
	movq	%rax, %xmm3
	leaq	16+_ZTVN6icu_678numparse4impl15PermilleMatcherE(%rip), %rax
	movl	$2, %r10d
	movl	$2, %r11d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rax, %xmm1
	pushq	%r15
	punpcklqdq	%xmm0, %xmm3
	pushq	%r14
	punpcklqdq	%xmm0, %xmm1
	leaq	16+_ZTVN6icu_678numparse4impl14PercentMatcherE(%rip), %rax
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	$2, %r14d
	pushq	%r13
	movq	%rax, %xmm2
	leaq	16+_ZTVN6icu_678numparse4impl16MinusSignMatcherE(%rip), %rax
	.cfi_offset 13, -40
	movl	$2, %r13d
	pushq	%r12
	movq	%rax, %xmm4
	punpcklqdq	%xmm0, %xmm2
	leaq	16+_ZTVN6icu_678numparse4impl16NumberParserImplE(%rip), %rax
	pushq	%rbx
	punpcklqdq	%xmm0, %xmm4
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movl	$2, %r12d
	movl	$2, %r15d
	subq	$72, %rsp
	movq	%rax, (%rdi)
	leaq	32(%rdi), %rax
	movq	%rax, 16(%rdi)
	movl	$2, %eax
	movw	%ax, 136(%rdi)
	leaq	16+_ZTVN6icu_678numparse4impl17IgnorablesMatcherE(%rip), %rax
	movq	%rax, %xmm5
	movw	%dx, 216(%rdi)
	leaq	16+_ZTVN6icu_678numparse4impl14PaddingMatcherE(%rip), %rdx
	punpcklqdq	%xmm0, %xmm5
	movl	%esi, 8(%rdi)
	movl	$2, %esi
	movups	%xmm5, 120(%rdi)
	movq	%rcx, %xmm5
	movl	$2, %ecx
	movw	%cx, 296(%rdi)
	punpcklqdq	%xmm0, %xmm5
	leaq	16+_ZTVN6icu_678numparse4impl10NanMatcherE(%rip), %rcx
	movups	%xmm5, 200(%rdi)
	movq	%rcx, %xmm5
	movl	$2, %ecx
	punpcklqdq	%xmm0, %xmm5
	movw	%si, 384(%rdi)
	leaq	16+_ZTVN6icu_678numparse4impl17ScientificMatcherE(%rip), %rsi
	movups	%xmm5, 368(%rdi)
	movq	%rdx, %xmm5
	leaq	16+_ZTVN6icu_678numparse4impl14DecimalMatcherE(%rip), %rdx
	movl	$0, 12(%rdi)
	punpcklqdq	%xmm0, %xmm5
	movl	$10, 24(%rdi)
	movb	$0, 28(%rdi)
	movb	$0, 112(%rdi)
	movups	%xmm4, 280(%rdi)
	movl	$2, %edi
	movw	%di, 464(%rbx)
	movl	$2, %edi
	movw	%r8w, 544(%rbx)
	movl	$2, %r8d
	movw	%r9w, 624(%rbx)
	movl	$2, %r9d
	movw	%r10w, 704(%rbx)
	movl	$2, %r10d
	movq	%rdx, 776(%rbx)
	movups	%xmm5, 448(%rbx)
	pxor	%xmm5, %xmm5
	movups	%xmm2, 528(%rbx)
	movups	%xmm1, 608(%rbx)
	movups	%xmm3, 688(%rbx)
	movq	%xmm0, 792(%rbx)
	movw	%r11w, 800(%rbx)
	movl	$2, %r11d
	movq	%rsi, 976(%rbx)
	movl	$2, %esi
	movw	%r13w, 992(%rbx)
	leaq	16+_ZTVN6icu_678numparse4impl23CombinedCurrencyMatcherE(%rip), %r13
	movq	%rdx, 1048(%rbx)
	movl	$2, %edx
	movq	%rax, 1248(%rbx)
	movl	$2, %eax
	movw	%r12w, 864(%rbx)
	movl	$2, %r12d
	movw	%r14w, 1072(%rbx)
	movl	$2, %r14d
	movw	%r15w, 1136(%rbx)
	movl	$2, %r15d
	movq	$0, 968(%rbx)
	movq	$0, 1240(%rbx)
	movw	%dx, 1264(%rbx)
	movw	%cx, 1336(%rbx)
	movw	%si, 1400(%rbx)
	movq	%r13, 1456(%rbx)
	movw	%di, 1480(%rbx)
	movw	%r8w, 1544(%rbx)
	movw	%r9w, 1616(%rbx)
	movw	%r10w, 1680(%rbx)
	movw	%r11w, 1744(%rbx)
	movq	%xmm0, 856(%rbx)
	movups	%xmm5, 952(%rbx)
	movq	%xmm0, 984(%rbx)
	movq	%xmm0, 1064(%rbx)
	movq	%xmm0, 1128(%rbx)
	movups	%xmm5, 1224(%rbx)
	movq	%xmm0, 1256(%rbx)
	movq	%xmm0, 1328(%rbx)
	movq	%xmm0, 1392(%rbx)
	movq	%xmm0, 1472(%rbx)
	movq	%xmm0, 1536(%rbx)
	movq	%xmm0, 1608(%rbx)
	movq	%xmm0, 1672(%rbx)
	movq	%xmm0, 1736(%rbx)
	movq	%xmm0, 1800(%rbx)
	movw	%ax, 2000(%rbx)
	movl	$2, %eax
	movw	%ax, 2064(%rbx)
	leaq	2133(%rbx), %rax
	movq	%rax, 2120(%rbx)
	xorl	%eax, %eax
	movw	%ax, 2132(%rbx)
	leaq	16+_ZTVN6icu_678numparse4impl12AffixMatcherE(%rip), %rax
	movl	$0, 2176(%rbx)
	movl	$40, 2128(%rbx)
	movq	%rax, 2184(%rbx)
	movq	%rax, 2216(%rbx)
	movq	%rax, 2248(%rbx)
	movq	%rax, 2280(%rbx)
	movq	%rax, 2312(%rbx)
	movq	%rax, 2344(%rbx)
	movq	%rax, 2376(%rbx)
	movq	%rax, 2408(%rbx)
	movq	%rax, 2440(%rbx)
	movw	%r12w, 1808(%rbx)
	leaq	2472(%rbx), %r12
	movw	%r14w, 1872(%rbx)
	leaq	16+_ZTVN6icu_678numparse4impl19AffixPatternMatcherE(%rip), %r14
	movw	%r15w, 1936(%rbx)
	leaq	2952(%rbx), %r15
	movq	%xmm0, 1864(%rbx)
	movq	%xmm0, 1928(%rbx)
	movq	%xmm0, 1992(%rbx)
	movq	%xmm0, 2056(%rbx)
	.p2align 4,,10
	.p2align 3
.L109:
	movq	%r12, %rdi
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm2, -80(%rbp)
	movaps	%xmm1, -64(%rbp)
	call	_ZN6icu_678numparse4impl18ArraySeriesMatcherC2Ev@PLT
	leaq	70(%r12), %rax
	movq	%r14, (%r12)
	addq	$80, %r12
	movdqa	-64(%rbp), %xmm1
	movdqa	-80(%rbp), %xmm2
	movq	%rax, -24(%r12)
	xorl	%eax, %eax
	movdqa	-96(%rbp), %xmm4
	movdqa	-112(%rbp), %xmm3
	movw	%ax, -10(%r12)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$4, -16(%r12)
	movq	%rax, %xmm0
	movb	$0, -12(%r12)
	cmpq	%r12, %r15
	jne	.L109
	movl	$2, %eax
	movl	$2, %ecx
	movl	$2, %edi
	movq	%r13, 3304(%rbx)
	movl	$2, %r12d
	movw	%ax, 2984(%rbx)
	movl	$2, %eax
	movl	$2, %r13d
	movl	$2, %r14d
	movl	$2, %r15d
	movl	$2, %edx
	movw	%ax, 3912(%rbx)
	leaq	3981(%rbx), %rax
	movl	$2, %esi
	movl	$2, %r8d
	movl	$2, %r9d
	movq	%rax, 3968(%rbx)
	movl	$2, %r10d
	xorl	%eax, %eax
	movl	$2, %r11d
	movw	%cx, 3160(%rbx)
	leaq	16+_ZTVN6icu_678numparse4impl21RequireAffixValidatorE(%rip), %rcx
	movw	%di, 3328(%rbx)
	leaq	16+_ZTVN6icu_678numparse4impl22RequireNumberValidatorE(%rip), %rdi
	movw	%r12w, 3656(%rbx)
	movw	%r13w, 3720(%rbx)
	movw	%r14w, 3784(%rbx)
	movw	%r15w, 3848(%rbx)
	movq	%xmm0, 3320(%rbx)
	movq	%xmm0, 3384(%rbx)
	movq	%xmm0, 3456(%rbx)
	movq	%xmm0, 3520(%rbx)
	movq	%xmm0, 3584(%rbx)
	movq	%xmm0, 3648(%rbx)
	movq	%xmm0, 3712(%rbx)
	movq	%xmm0, 3776(%rbx)
	movq	%xmm0, 3840(%rbx)
	movq	%xmm0, 3904(%rbx)
	movq	%rcx, %xmm0
	movw	%dx, 3072(%rbx)
	movw	%si, 3240(%rbx)
	movw	%r8w, 3392(%rbx)
	movw	%r9w, 3464(%rbx)
	movw	%r10w, 3528(%rbx)
	movw	%r11w, 3592(%rbx)
	movl	$0, 4024(%rbx)
	movl	$40, 3976(%rbx)
	movups	%xmm4, 2968(%rbx)
	movups	%xmm3, 3056(%rbx)
	movups	%xmm2, 3144(%rbx)
	movups	%xmm1, 3224(%rbx)
	movw	%ax, 3980(%rbx)
	leaq	4056(%rbx), %rax
	movq	%rax, 4040(%rbx)
	leaq	16+_ZTVN6icu_678numparse4impl24RequireCurrencyValidatorE(%rip), %rax
	movq	%rax, %xmm6
	leaq	16+_ZTVN6icu_678numparse4impl32RequireDecimalSeparatorValidatorE(%rip), %rax
	movl	$0, 4032(%rbx)
	movq	%rax, 4136(%rbx)
	punpcklqdq	%xmm6, %xmm0
	leaq	16+_ZTVN6icu_678numparse4impl22MultiplierParseHandlerE(%rip), %rax
	movups	%xmm0, 4120(%rbx)
	movq	%rax, %xmm7
	movq	%rdi, %xmm0
	punpcklqdq	%xmm7, %xmm0
	movl	$8, 4048(%rbx)
	movb	$0, 4052(%rbx)
	movl	$0, 4168(%rbx)
	movq	$0, 4176(%rbx)
	movl	$0, 4184(%rbx)
	movups	%xmm0, 4152(%rbx)
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3309:
	.size	_ZN6icu_678numparse4impl16NumberParserImplC2Ei, .-_ZN6icu_678numparse4impl16NumberParserImplC2Ei
	.globl	_ZN6icu_678numparse4impl16NumberParserImplC1Ei
	.set	_ZN6icu_678numparse4impl16NumberParserImplC1Ei,_ZN6icu_678numparse4impl16NumberParserImplC2Ei
	.align 2
	.p2align 4
	.globl	_ZN6icu_678numparse4impl16NumberParserImpl6freezeEv
	.type	_ZN6icu_678numparse4impl16NumberParserImpl6freezeEv, @function
_ZN6icu_678numparse4impl16NumberParserImpl6freezeEv:
.LFB3316:
	.cfi_startproc
	endbr64
	movb	$1, 112(%rdi)
	ret
	.cfi_endproc
.LFE3316:
	.size	_ZN6icu_678numparse4impl16NumberParserImpl6freezeEv, .-_ZN6icu_678numparse4impl16NumberParserImpl6freezeEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678numparse4impl16NumberParserImpl13getParseFlagsEv
	.type	_ZNK6icu_678numparse4impl16NumberParserImpl13getParseFlagsEv, @function
_ZNK6icu_678numparse4impl16NumberParserImpl13getParseFlagsEv:
.LFB3317:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE3317:
	.size	_ZNK6icu_678numparse4impl16NumberParserImpl13getParseFlagsEv, .-_ZNK6icu_678numparse4impl16NumberParserImpl13getParseFlagsEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678numparse4impl16NumberParserImpl11parseGreedyERNS_13StringSegmentERNS1_12ParsedNumberER10UErrorCode
	.type	_ZNK6icu_678numparse4impl16NumberParserImpl11parseGreedyERNS_13StringSegmentERNS1_12ParsedNumberER10UErrorCode, @function
_ZNK6icu_678numparse4impl16NumberParserImpl11parseGreedyERNS_13StringSegmentERNS1_12ParsedNumberER10UErrorCode:
.LFB3320:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -64(%rbp)
	movl	12(%rdi), %edx
	testl	%edx, %edx
	jle	.L114
	movq	%rdi, %r13
	movq	%rsi, %r12
	movq	%rcx, %r14
	xorl	%ebx, %ebx
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L124:
	addl	$1, %ebx
	cmpl	%ebx, 12(%r13)
	jle	.L114
.L116:
	movq	%r12, %rdi
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	testl	%eax, %eax
	je	.L114
	movq	16(%r13), %rdx
	movslq	%ebx, %rax
	movq	%r12, %rsi
	movq	(%rdx,%rax,8), %r15
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*32(%rax)
	testb	%al, %al
	je	.L124
	movq	%r12, %rdi
	call	_ZNK6icu_6713StringSegment9getOffsetEv@PLT
	movq	-64(%rbp), %rdx
	movq	%r14, %rcx
	movq	%r12, %rsi
	movl	%eax, -52(%rbp)
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*24(%rax)
	movl	(%r14), %eax
	testl	%eax, %eax
	jg	.L114
	movq	%r12, %rdi
	addl	$1, %ebx
	call	_ZNK6icu_6713StringSegment9getOffsetEv@PLT
	cmpl	-52(%rbp), %eax
	movl	$0, %eax
	cmovne	%eax, %ebx
	cmpl	%ebx, 12(%r13)
	jg	.L116
.L114:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3320:
	.size	_ZNK6icu_678numparse4impl16NumberParserImpl11parseGreedyERNS_13StringSegmentERNS1_12ParsedNumberER10UErrorCode, .-_ZNK6icu_678numparse4impl16NumberParserImpl11parseGreedyERNS_13StringSegmentERNS1_12ParsedNumberER10UErrorCode
	.section	.rodata.str2.8,"aMS",@progbits,2
	.align 8
.LC0:
	.string	"<"
	.string	"N"
	.string	"u"
	.string	"m"
	.string	"b"
	.string	"e"
	.string	"r"
	.string	"P"
	.string	"a"
	.string	"r"
	.string	"s"
	.string	"e"
	.string	"r"
	.string	"I"
	.string	"m"
	.string	"p"
	.string	"l"
	.string	" "
	.string	"m"
	.string	"a"
	.string	"t"
	.string	"c"
	.string	"h"
	.string	"e"
	.string	"r"
	.string	"s"
	.string	":"
	.string	"["
	.string	""
	.string	""
	.section	.rodata.str2.2,"aMS",@progbits,2
	.align 2
.LC1:
	.string	" "
	.string	"]"
	.string	">"
	.string	""
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678numparse4impl16NumberParserImpl8toStringEv
	.type	_ZNK6icu_678numparse4impl16NumberParserImpl8toStringEv, @function
_ZNK6icu_678numparse4impl16NumberParserImpl8toStringEv:
.LFB3329:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	leaq	.LC0(%rip), %rsi
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movl	12(%r13), %edx
	testl	%edx, %edx
	jle	.L126
	xorl	%ebx, %ebx
	leaq	-130(%rbp), %r14
	leaq	-128(%rbp), %r12
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L134:
	sarl	$5, %ecx
.L133:
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	addq	$1, %rbx
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpl	%ebx, 12(%r13)
	jle	.L126
.L129:
	movl	$1, %ecx
	movl	$32, %eax
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	movw	%ax, -130(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movq	16(%r13), %rax
	movq	%r12, %rdi
	movq	(%rax,%rbx,8), %rsi
	movq	(%rsi), %rax
	call	*48(%rax)
	movswl	-120(%rbp), %ecx
	testw	%cx, %cx
	jns	.L134
	movl	-116(%rbp), %ecx
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L126:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC1(%rip), %rax
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L135
	addq	$104, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L135:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3329:
	.size	_ZNK6icu_678numparse4impl16NumberParserImpl8toStringEv, .-_ZNK6icu_678numparse4impl16NumberParserImpl8toStringEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_678numparse4impl16NumberParserImplD2Ev
	.type	_ZN6icu_678numparse4impl16NumberParserImplD2Ev, @function
_ZN6icu_678numparse4impl16NumberParserImplD2Ev:
.LFB3312:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_678numparse4impl16NumberParserImplE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$4168, %rdi
	subq	$8, %rsp
	movq	%rax, -4168(%rdi)
	leaq	16+_ZTVN6icu_678numparse4impl22MultiplierParseHandlerE(%rip), %rax
	movq	%rax, -8(%rdi)
	movl	$0, -4156(%rdi)
	call	_ZN6icu_676number5ScaleD1Ev@PLT
	movl	4032(%rbx), %eax
	movq	4040(%rbx), %r8
	testl	%eax, %eax
	jle	.L139
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L141:
	movq	(%r8,%r12,8), %rdi
	testq	%rdi, %rdi
	je	.L138
	movq	(%rdi), %rax
	addq	$1, %r12
	call	*8(%rax)
	movl	4032(%rbx), %eax
	movq	4040(%rbx), %r8
	cmpl	%r12d, %eax
	jg	.L141
.L139:
	cmpb	$0, 4052(%rbx)
	jne	.L185
.L142:
	leaq	16+_ZTVN6icu_678numparse4impl23CombinedCurrencyMatcherE(%rip), %r14
	cmpb	$0, 3980(%rbx)
	movq	%r14, 3304(%rbx)
	jne	.L186
.L143:
	leaq	3904(%rbx), %rdi
	leaq	3776(%rbx), %r12
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	3840(%rbx), %rdi
	leaq	3392(%rbx), %r13
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	.p2align 4,,10
	.p2align 3
.L144:
	movq	(%r12), %rax
	movq	%r12, %rdi
	subq	$64, %r12
	call	*(%rax)
	cmpq	%r13, %r12
	jne	.L144
	leaq	3384(%rbx), %rdi
	leaq	16+_ZTVN6icu_678numparse4impl13SymbolMatcherE(%rip), %r12
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	3320(%rbx), %rdi
	leaq	2872(%rbx), %r13
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, 3224(%rbx)
	leaq	3232(%rbx), %rdi
	leaq	2392(%rbx), %r15
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, 3144(%rbx)
	leaq	3152(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, 3056(%rbx)
	leaq	3064(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, 2968(%rbx)
	leaq	2976(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	.p2align 4,,10
	.p2align 3
.L145:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	subq	$80, %r13
	call	*(%rax)
	cmpq	%r13, %r15
	jne	.L145
	leaq	2440(%rbx), %r13
	leaq	2152(%rbx), %r15
	.p2align 4,,10
	.p2align 3
.L146:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	subq	$32, %r13
	call	*(%rax)
	cmpq	%r13, %r15
	jne	.L146
	cmpb	$0, 2132(%rbx)
	movq	%r14, 1456(%rbx)
	jne	.L187
.L147:
	leaq	2056(%rbx), %rdi
	leaq	1928(%rbx), %r13
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	1992(%rbx), %rdi
	leaq	1544(%rbx), %r14
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	.p2align 4,,10
	.p2align 3
.L148:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	subq	$64, %r13
	call	*(%rax)
	cmpq	%r14, %r13
	jne	.L148
	leaq	1536(%rbx), %rdi
	leaq	16+_ZTVN6icu_678numparse4impl14DecimalMatcherE(%rip), %r14
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	1472(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	16+_ZTVN6icu_678numparse4impl17ScientificMatcherE(%rip), %rax
	leaq	1392(%rbx), %rdi
	movq	%rax, 976(%rbx)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	1328(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, 1248(%rbx)
	leaq	1256(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	1240(%rbx), %rax
	movq	%r14, 1048(%rbx)
	testq	%rax, %rax
	je	.L149
	movq	-8(%rax), %r13
	salq	$6, %r13
	addq	%rax, %r13
	cmpq	%r13, %rax
	je	.L150
	.p2align 4,,10
	.p2align 3
.L151:
	movq	-64(%r13), %rax
	subq	$64, %r13
	movq	%r13, %rdi
	call	*(%rax)
	cmpq	%r13, 1240(%rbx)
	jne	.L151
.L150:
	leaq	-8(%r13), %rdi
	call	_ZN6icu_677UMemorydaEPv@PLT
.L149:
	movq	1232(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L152
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
.L152:
	movq	1224(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L153
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
.L153:
	leaq	1128(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	1064(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	984(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	968(%rbx), %rax
	movq	%r14, 776(%rbx)
	testq	%rax, %rax
	je	.L154
	movq	-8(%rax), %r13
	salq	$6, %r13
	addq	%rax, %r13
	cmpq	%r13, %rax
	je	.L155
	.p2align 4,,10
	.p2align 3
.L156:
	movq	-64(%r13), %rax
	subq	$64, %r13
	movq	%r13, %rdi
	call	*(%rax)
	cmpq	%r13, 968(%rbx)
	jne	.L156
.L155:
	leaq	-8(%r13), %rdi
	call	_ZN6icu_677UMemorydaEPv@PLT
.L154:
	movq	960(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L157
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
.L157:
	movq	952(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L158
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
.L158:
	leaq	856(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	792(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, 688(%rbx)
	leaq	696(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, 608(%rbx)
	leaq	616(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, 528(%rbx)
	leaq	536(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, 448(%rbx)
	leaq	456(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, 368(%rbx)
	leaq	376(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, 280(%rbx)
	leaq	288(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, 200(%rbx)
	leaq	208(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, 120(%rbx)
	leaq	128(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, 28(%rbx)
	jne	.L188
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L138:
	.cfi_restore_state
	addq	$1, %r12
	cmpl	%r12d, %eax
	jg	.L141
	cmpb	$0, 4052(%rbx)
	je	.L142
.L185:
	movq	%r8, %rdi
	call	uprv_free_67@PLT
	jmp	.L142
	.p2align 4,,10
	.p2align 3
.L188:
	movq	16(%rbx), %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uprv_free_67@PLT
	.p2align 4,,10
	.p2align 3
.L187:
	.cfi_restore_state
	movq	2120(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L186:
	movq	3968(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L143
	.cfi_endproc
.LFE3312:
	.size	_ZN6icu_678numparse4impl16NumberParserImplD2Ev, .-_ZN6icu_678numparse4impl16NumberParserImplD2Ev
	.globl	_ZN6icu_678numparse4impl16NumberParserImplD1Ev
	.set	_ZN6icu_678numparse4impl16NumberParserImplD1Ev,_ZN6icu_678numparse4impl16NumberParserImplD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_678numparse4impl16NumberParserImplD0Ev
	.type	_ZN6icu_678numparse4impl16NumberParserImplD0Ev, @function
_ZN6icu_678numparse4impl16NumberParserImplD0Ev:
.LFB3314:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN6icu_678numparse4impl16NumberParserImplD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3314:
	.size	_ZN6icu_678numparse4impl16NumberParserImplD0Ev, .-_ZN6icu_678numparse4impl16NumberParserImplD0Ev
	.section	.rodata.str2.2
	.align 2
.LC2:
	.string	"I"
	.string	"U"
	.string	"$"
	.string	""
	.string	""
	.align 2
.LC3:
	.string	"I"
	.string	"C"
	.string	"U"
	.string	""
	.string	""
	.align 2
.LC4:
	.string	"@"
	.string	""
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_678numparse4impl16NumberParserImpl18createSimpleParserERKNS_6LocaleERKNS_13UnicodeStringEiR10UErrorCode
	.type	_ZN6icu_678numparse4impl16NumberParserImpl18createSimpleParserERKNS_6LocaleERKNS_13UnicodeStringEiR10UErrorCode, @function
_ZN6icu_678numparse4impl16NumberParserImpl18createSimpleParserERKNS_6LocaleERKNS_13UnicodeStringEiR10UErrorCode:
.LFB3153:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$3784, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -7712(%rbp)
	movl	$4192, %edi
	movq	%rsi, -7720(%rbp)
	movl	%edx, -7700(%rbp)
	movq	%rcx, -7696(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L313
	movl	$2, %ecx
	movl	$2, %esi
	movl	$2, %edi
	movl	$2, %r8d
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%cx, 136(%r15)
	leaq	16+_ZTVN6icu_678numparse4impl15InfinityMatcherE(%rip), %rcx
	movl	$2, %r9d
	movq	%rax, %xmm0
	leaq	16+_ZTVN6icu_678numparse4impl15PlusSignMatcherE(%rip), %rax
	leaq	16+_ZTVN6icu_678numparse4impl17ScientificMatcherE(%rip), %rdx
	movl	$2, %r10d
	movq	%rax, %xmm3
	movl	$2, %ebx
	leaq	16+_ZTVN6icu_678numparse4impl15PermilleMatcherE(%rip), %rax
	movl	$2, %r11d
	movq	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm3
	leaq	16+_ZTVN6icu_678numparse4impl14PercentMatcherE(%rip), %rax
	movl	$2, %r12d
	movq	%rax, %xmm2
	leaq	16+_ZTVN6icu_678numparse4impl16MinusSignMatcherE(%rip), %rax
	punpcklqdq	%xmm0, %xmm1
	movl	$2, %r13d
	movq	%rax, %xmm4
	leaq	16+_ZTVN6icu_678numparse4impl16NumberParserImplE(%rip), %rax
	punpcklqdq	%xmm0, %xmm2
	movw	%si, 216(%r15)
	movq	%rax, (%r15)
	movl	-7700(%rbp), %eax
	punpcklqdq	%xmm0, %xmm4
	movl	$2, %r14d
	movw	%di, 296(%r15)
	movl	$2, %esi
	movl	$2, %edi
	movl	%eax, 8(%r15)
	leaq	32(%r15), %rax
	movq	%rax, 16(%r15)
	leaq	16+_ZTVN6icu_678numparse4impl17IgnorablesMatcherE(%rip), %rax
	movq	%rax, %xmm5
	movw	%r8w, 384(%r15)
	movl	$2, %r8d
	punpcklqdq	%xmm0, %xmm5
	movw	%r9w, 464(%r15)
	movl	$2, %r9d
	movups	%xmm5, 120(%r15)
	movq	%rcx, %xmm5
	leaq	16+_ZTVN6icu_678numparse4impl10NanMatcherE(%rip), %rcx
	punpcklqdq	%xmm0, %xmm5
	movw	%r10w, 544(%r15)
	movl	$2, %r10d
	movups	%xmm5, 200(%r15)
	movq	%rcx, %xmm5
	leaq	16+_ZTVN6icu_678numparse4impl14PaddingMatcherE(%rip), %rcx
	punpcklqdq	%xmm0, %xmm5
	movw	%r11w, 624(%r15)
	movl	$2, %r11d
	movups	%xmm5, 368(%r15)
	movq	%rcx, %xmm5
	leaq	16+_ZTVN6icu_678numparse4impl14DecimalMatcherE(%rip), %rcx
	punpcklqdq	%xmm0, %xmm5
	movw	%bx, 704(%r15)
	movl	$2, %ebx
	movups	%xmm5, 448(%r15)
	pxor	%xmm5, %xmm5
	movq	%rcx, -7800(%rbp)
	movq	%rcx, 776(%r15)
	movw	%r12w, 800(%r15)
	movl	$2, %r12d
	movw	%r13w, 864(%r15)
	movl	$2, %r13d
	movl	$0, 12(%r15)
	movl	$10, 24(%r15)
	movb	$0, 28(%r15)
	movb	$0, 112(%r15)
	movq	$0, 968(%r15)
	movups	%xmm4, 280(%r15)
	movups	%xmm2, 528(%r15)
	movups	%xmm1, 608(%r15)
	movups	%xmm3, 688(%r15)
	movq	%xmm0, 792(%r15)
	movq	%xmm0, 856(%r15)
	movups	%xmm5, 952(%r15)
	movq	%rdx, -7888(%rbp)
	movq	%rax, 1248(%r15)
	leaq	16+_ZTVN6icu_678numparse4impl23CombinedCurrencyMatcherE(%rip), %rax
	movq	%rax, -7728(%rbp)
	movq	%rax, 1456(%r15)
	leaq	1608(%r15), %rax
	movq	%rdx, 976(%r15)
	movl	$2, %edx
	movq	%rcx, 1048(%r15)
	movl	$2, %ecx
	movq	%rax, -7808(%rbp)
	movl	$2, %eax
	movw	%r14w, 992(%r15)
	movl	$2, %r14d
	movw	%bx, 1680(%r15)
	leaq	2184(%r15), %rbx
	movw	%r12w, 1744(%r15)
	leaq	2472(%r15), %r12
	movw	%r13w, 1808(%r15)
	leaq	16+_ZTVN6icu_678numparse4impl19AffixPatternMatcherE(%rip), %r13
	movw	%dx, 1072(%r15)
	movw	%cx, 1136(%r15)
	movq	$0, 1240(%r15)
	movw	%si, 1264(%r15)
	movw	%di, 1336(%r15)
	movw	%r8w, 1400(%r15)
	movw	%r9w, 1480(%r15)
	movw	%r10w, 1544(%r15)
	movw	%r11w, 1616(%r15)
	movq	%xmm0, 984(%r15)
	movq	%xmm0, 1064(%r15)
	movq	%xmm0, 1128(%r15)
	movups	%xmm5, 1224(%r15)
	movq	%xmm0, 1256(%r15)
	movq	%xmm0, 1328(%r15)
	movq	%xmm0, 1392(%r15)
	movq	%xmm0, 1472(%r15)
	movq	%xmm0, 1536(%r15)
	movq	%xmm0, 1608(%r15)
	movq	%xmm0, 1672(%r15)
	movq	%xmm0, 1736(%r15)
	movq	%xmm0, 1800(%r15)
	movq	%xmm0, 1864(%r15)
	movw	%ax, 1936(%r15)
	movl	$2, %eax
	movw	%ax, 2000(%r15)
	movl	$2, %eax
	movw	%ax, 2064(%r15)
	leaq	2133(%r15), %rax
	movq	%rax, 2120(%r15)
	xorl	%eax, %eax
	movw	%ax, 2132(%r15)
	leaq	16+_ZTVN6icu_678numparse4impl12AffixMatcherE(%rip), %rax
	movq	%rax, 2184(%r15)
	movq	%rax, 2216(%r15)
	movq	%rax, 2248(%r15)
	movq	%rax, 2280(%r15)
	movq	%rax, 2312(%r15)
	movq	%rax, 2344(%r15)
	movq	%rax, 2376(%r15)
	movq	%rax, 2408(%r15)
	movq	%rax, 2440(%r15)
	leaq	2952(%r15), %rax
	movl	$0, 2176(%r15)
	movl	$40, 2128(%r15)
	movq	%rax, -7688(%rbp)
	movw	%r14w, 1872(%r15)
	movq	%r12, %r14
	movq	%xmm0, 1928(%r15)
	movq	%xmm0, 1992(%r15)
	movq	%xmm0, 2056(%r15)
	movaps	%xmm4, -7744(%rbp)
	movaps	%xmm2, -7760(%rbp)
	movaps	%xmm3, -7776(%rbp)
	movaps	%xmm1, -7792(%rbp)
	movq	%xmm0, -7824(%rbp)
	.p2align 4,,10
	.p2align 3
.L194:
	movq	%r14, %rdi
	call	_ZN6icu_678numparse4impl18ArraySeriesMatcherC2Ev@PLT
	leaq	70(%r14), %rax
	xorl	%edx, %edx
	movq	%r13, (%r14)
	movq	%rax, 56(%r14)
	addq	$80, %r14
	movl	$4, -16(%r14)
	movb	$0, -12(%r14)
	movw	%dx, -10(%r14)
	cmpq	%r14, -7688(%rbp)
	jne	.L194
	movq	-7728(%rbp), %rax
	movdqa	-7760(%rbp), %xmm2
	movl	$2, %ecx
	movl	$2, %edx
	movq	-7824(%rbp), %xmm0
	movdqa	-7776(%rbp), %xmm3
	movl	$2, %esi
	movl	$2, %edi
	movq	%rax, 3304(%r15)
	leaq	3456(%r15), %rax
	movdqa	-7792(%rbp), %xmm1
	movl	$2, %r8d
	movq	%rax, -7832(%rbp)
	movl	$2, %eax
	movdqa	-7744(%rbp), %xmm4
	movl	$2, %r9d
	movw	%ax, 3720(%r15)
	movl	$2, %eax
	movl	$2, %r10d
	movl	$2, %r11d
	movw	%ax, 3784(%r15)
	movl	$2, %r13d
	movl	$2, %eax
	movl	$2, %r14d
	movw	%cx, 3072(%r15)
	leaq	16+_ZTVN6icu_678numparse4impl21RequireAffixValidatorE(%rip), %rcx
	movups	%xmm3, 3056(%r15)
	movups	%xmm2, 3144(%r15)
	movups	%xmm1, 3224(%r15)
	movq	%rcx, %xmm1
	leaq	16+_ZTVN6icu_678numparse4impl22RequireNumberValidatorE(%rip), %rcx
	movw	%dx, 2984(%r15)
	movw	%si, 3160(%r15)
	movw	%di, 3240(%r15)
	movw	%r8w, 3328(%r15)
	movw	%r9w, 3392(%r15)
	movw	%r10w, 3464(%r15)
	movw	%r11w, 3528(%r15)
	movw	%r13w, 3592(%r15)
	movw	%r14w, 3656(%r15)
	movups	%xmm4, 2968(%r15)
	movq	%xmm0, 3320(%r15)
	movq	%xmm0, 3384(%r15)
	movq	%xmm0, 3456(%r15)
	movq	%xmm0, 3520(%r15)
	movq	%xmm0, 3584(%r15)
	movq	%xmm0, 3648(%r15)
	movq	%xmm0, 3712(%r15)
	movq	%xmm0, 3776(%r15)
	movq	%xmm0, 3840(%r15)
	movw	%ax, 3848(%r15)
	movl	$2, %eax
	movw	%ax, 3912(%r15)
	leaq	3981(%r15), %rax
	movq	%rax, 3968(%r15)
	xorl	%eax, %eax
	movw	%ax, 3980(%r15)
	leaq	4056(%r15), %rax
	movq	%rax, 4040(%r15)
	leaq	16+_ZTVN6icu_678numparse4impl24RequireCurrencyValidatorE(%rip), %rax
	movq	%rax, %xmm2
	leaq	16+_ZTVN6icu_678numparse4impl32RequireDecimalSeparatorValidatorE(%rip), %rax
	movl	$0, 4024(%r15)
	punpcklqdq	%xmm2, %xmm1
	movq	%rax, 4136(%r15)
	leaq	16+_ZTVN6icu_678numparse4impl22MultiplierParseHandlerE(%rip), %rax
	movups	%xmm1, 4120(%r15)
	movq	%rax, %xmm3
	movq	%rcx, %xmm1
	movl	$40, 3976(%r15)
	punpcklqdq	%xmm3, %xmm1
	movl	$0, 4032(%r15)
	movl	$8, 4048(%r15)
	movb	$0, 4052(%r15)
	movl	$0, 4168(%r15)
	movq	$0, 4176(%r15)
	movl	$0, 4184(%r15)
	movq	%xmm0, 3904(%r15)
	movups	%xmm1, 4152(%r15)
.L193:
	leaq	-5696(%rbp), %rcx
	movq	-7696(%rbp), %r14
	leaq	-7552(%rbp), %rax
	movq	.LC5(%rip), %xmm6
	movq	%rcx, %xmm4
	movq	-7712(%rbp), %rsi
	movq	%rax, %xmm1
	movq	%rcx, %rdi
	punpcklqdq	%xmm4, %xmm1
	punpcklqdq	%xmm0, %xmm6
	movq	%r14, %rdx
	movq	%rcx, -7688(%rbp)
	movaps	%xmm1, -7824(%rbp)
	leaq	-2872(%rbp), %r13
	movaps	%xmm6, -7776(%rbp)
	movq	%rax, -7792(%rbp)
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKNS_6LocaleER10UErrorCode@PLT
	leaq	-2880(%rbp), %rax
	movl	-7700(%rbp), %esi
	movq	%rax, %rdi
	movq	%rax, -7760(%rbp)
	call	_ZN6icu_678numparse4impl17IgnorablesMatcherC1Ei@PLT
	leaq	120(%r15), %rcx
	movq	%r13, %rsi
	leaq	128(%r15), %rdi
	movq	%rcx, -7744(%rbp)
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	-2808(%rbp), %rax
	leaq	16+_ZTVN6icu_678numparse4impl13SymbolMatcherE(%rip), %rdx
	movq	%r13, %rdi
	leaq	-6864(%rbp), %r13
	movq	%rax, 192(%r15)
	movq	%rdx, -2880(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-7712(%rbp), %rsi
	movq	%r14, %rdx
	movq	-7760(%rbp), %rdi
	movq	%r14, -7696(%rbp)
	leaq	-7664(%rbp), %r14
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKNS_6LocaleER10UErrorCode@PLT
	movq	%r13, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%r13, %rsi
	leaq	-2360(%rbp), %rdi
	movb	$1, -72(%rbp)
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	leaq	.LC3(%rip), %rsi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%r13, %rsi
	leaq	-2296(%rbp), %rdi
	movb	$1, -71(%rbp)
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-7696(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	leaq	.LC3(%rip), %r10
	movq	%r10, -7664(%rbp)
	call	_ZN6icu_6712CurrencyUnitC1ENS_14ConstChar16PtrER10UErrorCode@PLT
	movq	-7696(%rbp), %r8
	movq	%r13, %rsi
	movq	-7760(%rbp), %rcx
	movq	-7712(%rbp), %rdx
	movq	-7792(%rbp), %rdi
	call	_ZN6icu_676number4impl15CurrencySymbolsC1ENS_12CurrencyUnitERKNS_6LocaleERKNS_20DecimalFormatSymbolsER10UErrorCode@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	movl	$2, %eax
	movdqa	-7776(%rbp), %xmm0
	leaq	-7184(%rbp), %rdi
	movb	$0, -7200(%rbp)
	movw	%ax, -7296(%rbp)
	movabsq	$281474976645120, %rax
	movq	%rax, -7240(%rbp)
	leaq	-7312(%rbp), %rax
	movaps	%xmm0, -7312(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rax, -7776(%rbp)
	movq	%rdi, -7864(%rbp)
	movaps	%xmm0, -7232(%rbp)
	movaps	%xmm0, -7216(%rbp)
	movl	$0, -7196(%rbp)
	movb	$0, -7192(%rbp)
	movl	$0, -7188(%rbp)
	call	_ZN6icu_676number4impl15DecimalQuantityC1Ev@PLT
	pxor	%xmm0, %xmm0
	movabsq	$281474976645120, %rax
	leaq	-7016(%rbp), %rdi
	movq	%rax, -7072(%rbp)
	movq	%rdi, -7896(%rbp)
	movups	%xmm0, -7096(%rbp)
	movups	%xmm0, -7064(%rbp)
	movups	%xmm0, -7048(%rbp)
	movb	$0, -7112(%rbp)
	movq	$0, -7108(%rbp)
	movb	$0, -7100(%rbp)
	movq	$0, -7080(%rbp)
	movb	$0, -7032(%rbp)
	movl	$0, -7028(%rbp)
	movb	$0, -7024(%rbp)
	movl	$0, -7020(%rbp)
	call	_ZN6icu_676number4impl15DecimalQuantityC1Ev@PLT
	leaq	-7304(%rbp), %rdi
	movq	-7696(%rbp), %rdx
	movq	-7776(%rbp), %rsi
	movq	%rdi, -7848(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdi, -6904(%rbp)
	movq	-7720(%rbp), %rdi
	movaps	%xmm0, -6928(%rbp)
	movb	$0, -6944(%rbp)
	movq	$0, -6940(%rbp)
	movb	$0, -6932(%rbp)
	movq	$0, -6912(%rbp)
	movl	$0, -6896(%rbp)
	movq	$0, -6888(%rbp)
	movb	$0, -6880(%rbp)
	call	_ZN6icu_676number4impl13PatternParser18parseToPatternInfoERKNS_13UnicodeStringERNS1_17ParsedPatternInfoER10UErrorCode@PLT
	movdqa	-7824(%rbp), %xmm1
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	-7744(%rbp), %xmm0
	movl	-7700(%rbp), %eax
	leaq	-5984(%rbp), %r14
	movaps	%xmm1, -7664(%rbp)
	movhps	-7712(%rbp), %xmm0
	movl	%eax, -7632(%rbp)
	movaps	%xmm0, -7648(%rbp)
	call	_ZN6icu_678numparse4impl26AffixTokenMatcherWarehouseC1EPKNS1_26AffixTokenMatcherSetupDataE@PLT
	leaq	2960(%r15), %rax
	leaq	2976(%r15), %rdi
	movq	%rax, -7824(%rbp)
	movq	-6864(%rbp), %rax
	movq	%rax, 2960(%r15)
	leaq	-6848(%rbp), %rax
	movq	%rax, %rsi
	movq	%rax, -7720(%rbp)
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	-6784(%rbp), %rax
	leaq	3064(%r15), %rdi
	movq	%rax, 3040(%r15)
	movzbl	-6776(%rbp), %eax
	movb	%al, 3048(%r15)
	leaq	-6760(%rbp), %rax
	movq	%rax, %rsi
	movq	%rax, -7856(%rbp)
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	-6696(%rbp), %rax
	leaq	3152(%r15), %rdi
	movq	%rax, 3128(%r15)
	movzbl	-6688(%rbp), %eax
	movb	%al, 3136(%r15)
	leaq	-6672(%rbp), %rax
	movq	%rax, %rsi
	movq	%rax, -7872(%rbp)
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	-6608(%rbp), %rax
	leaq	3232(%r15), %rdi
	movq	%rax, 3216(%r15)
	leaq	-6592(%rbp), %rax
	movq	%rax, %rsi
	movq	%rax, -7880(%rbp)
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	-6528(%rbp), %rax
	leaq	3320(%r15), %rdi
	movq	%rax, 3296(%r15)
	movq	-6512(%rbp), %rax
	movq	%rax, 3312(%r15)
	leaq	-6504(%rbp), %rax
	movq	%rax, %rsi
	movq	%rax, -7904(%rbp)
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	leaq	-6440(%rbp), %rax
	leaq	3384(%r15), %rdi
	movq	%rax, %rsi
	movq	%rax, -7840(%rbp)
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	-7832(%rbp), %rdx
	movzbl	-6376(%rbp), %eax
	leaq	-6368(%rbp), %rcx
	movq	%r15, -7832(%rbp)
	movb	%al, 3448(%r15)
	movq	%r12, %r15
	movq	%rdx, %r12
	movq	%r13, -7912(%rbp)
	movq	%rbx, %r13
	movq	%rcx, %rbx
	.p2align 4,,10
	.p2align 3
.L195:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	addq	$64, %rbx
	addq	$64, %r12
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	cmpq	%r14, %rbx
	jne	.L195
	movq	%r15, %r12
	movq	-7832(%rbp), %r15
	movq	%r14, %rsi
	movq	%r13, %rbx
	movq	-7912(%rbp), %r13
	leaq	3840(%r15), %rdi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	leaq	-5920(%rbp), %r11
	leaq	3904(%r15), %rdi
	movq	%r11, %rsi
	movq	%r11, -7832(%rbp)
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	leaq	-5856(%rbp), %rsi
	leaq	3968(%r15), %rdi
	call	_ZN6icu_6710CharStringaSEOS0_@PLT
	movl	-5792(%rbp), %eax
	cmpb	$0, 4052(%r15)
	movq	-7832(%rbp), %r11
	movl	%eax, 4032(%r15)
	jne	.L314
.L196:
	movslq	-5776(%rbp), %rdx
	leaq	-5768(%rbp), %rax
	movl	%edx, 4048(%r15)
	movzbl	-5772(%rbp), %ecx
	movb	%cl, 4052(%r15)
	movq	-5784(%rbp), %r8
	cmpq	%rax, %r8
	je	.L315
	movq	%r8, 4040(%r15)
.L199:
	movq	-7728(%rbp), %rax
	cmpb	$0, -5844(%rbp)
	movq	%rax, -6520(%rbp)
	jne	.L316
.L200:
	movq	%r11, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	leaq	-6432(%rbp), %r14
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	-6048(%rbp), %rax
	movq	%r14, -7832(%rbp)
	movq	%rax, %r14
	movq	%r13, %rax
	movq	%r12, %r13
	movq	%rbx, %r12
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L201:
	movq	(%r14), %rax
	movq	%r14, %rdi
	subq	$64, %r14
	call	*(%rax)
	cmpq	-7832(%rbp), %r14
	jne	.L201
	movq	-7840(%rbp), %rdi
	movq	%rbx, %rax
	movq	%r12, %rbx
	movq	%r13, %r12
	movq	%rax, %r13
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-7904(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-7880(%rbp), %rdi
	leaq	16+_ZTVN6icu_678numparse4impl13SymbolMatcherE(%rip), %rax
	movq	%rax, %r14
	movq	%rax, -6600(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-7872(%rbp), %rdi
	movq	%r14, -6680(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-7856(%rbp), %rdi
	movq	%r14, -6768(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-7720(%rbp), %rdi
	movq	%r14, -6856(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-7824(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN6icu_678numparse4impl21AffixMatcherWarehouseC1EPNS1_26AffixTokenMatcherWarehouseE@PLT
	movq	-6856(%rbp), %rax
	movq	%rax, 8(%rbx)
	movq	-6848(%rbp), %rax
	movq	%rax, 16(%rbx)
	movl	-6840(%rbp), %eax
	movdqu	-6824(%rbp), %xmm7
	movl	%eax, 24(%rbx)
	movl	-6808(%rbp), %eax
	movups	%xmm7, 40(%rbx)
	movdqu	-6792(%rbp), %xmm7
	movl	%eax, 56(%rbx)
	movl	-6776(%rbp), %eax
	movups	%xmm7, 72(%rbx)
	movdqu	-6760(%rbp), %xmm7
	movl	%eax, 88(%rbx)
	movl	-6744(%rbp), %eax
	movups	%xmm7, 104(%rbx)
	movdqu	-6728(%rbp), %xmm6
	movl	%eax, 120(%rbx)
	movl	-6712(%rbp), %eax
	movups	%xmm6, 136(%rbx)
	movdqu	-6696(%rbp), %xmm7
	movl	%eax, 152(%rbx)
	movl	-6680(%rbp), %eax
	movups	%xmm7, 168(%rbx)
	movdqu	-6664(%rbp), %xmm2
	movl	%eax, 184(%rbx)
	movl	-6648(%rbp), %eax
	movups	%xmm2, 200(%rbx)
	movdqu	-6632(%rbp), %xmm3
	movl	%eax, 216(%rbx)
	movups	%xmm3, 232(%rbx)
	movl	-6616(%rbp), %eax
	movdqu	-6600(%rbp), %xmm4
	movl	%eax, 248(%rbx)
	movl	-6584(%rbp), %eax
	movups	%xmm4, 264(%rbx)
	movl	%eax, 280(%rbx)
	cmpb	$0, 20(%r12)
	jne	.L317
.L202:
	movslq	-6560(%rbp), %rdx
	movl	%edx, 16(%r12)
	movzbl	-6556(%rbp), %eax
	movb	%al, 20(%r12)
	movq	-6568(%rbp), %rsi
	leaq	-6552(%rbp), %rax
	cmpq	%rax, %rsi
	je	.L203
	movq	%rsi, 8(%r12)
	movq	%rax, -6568(%rbp)
	movl	$3, -6560(%rbp)
	movb	$0, -6556(%rbp)
.L204:
	movl	-6528(%rbp), %eax
	cmpb	$0, 68(%r12)
	movl	%eax, 48(%r12)
	jne	.L318
.L205:
	movslq	-6512(%rbp), %rdx
	movl	%edx, 64(%r12)
	movzbl	-6508(%rbp), %eax
	movb	%al, 68(%r12)
	movq	-6520(%rbp), %rsi
	leaq	-6506(%rbp), %rax
	cmpq	%rax, %rsi
	je	.L206
	movq	%rsi, 56(%r12)
	movq	%rax, -6520(%rbp)
	movl	$4, -6512(%rbp)
	movb	$0, -6508(%rbp)
.L207:
	cmpb	$0, 100(%r12)
	jne	.L319
.L208:
	movslq	-6480(%rbp), %rdx
	movl	%edx, 96(%r12)
	movzbl	-6476(%rbp), %eax
	movb	%al, 100(%r12)
	movq	-6488(%rbp), %rsi
	leaq	-6472(%rbp), %rax
	cmpq	%rax, %rsi
	je	.L209
	movq	%rsi, 88(%r12)
	movq	%rax, -6488(%rbp)
	movl	$3, -6480(%rbp)
	movb	$0, -6476(%rbp)
.L210:
	cmpb	$0, 148(%r12)
	movl	-6448(%rbp), %eax
	movl	%eax, 128(%r12)
	jne	.L320
.L211:
	movslq	-6432(%rbp), %rdx
	movl	%edx, 144(%r12)
	movzbl	-6428(%rbp), %eax
	movb	%al, 148(%r12)
	movq	-6440(%rbp), %rsi
	leaq	-6426(%rbp), %rax
	cmpq	%rax, %rsi
	je	.L212
	movq	%rsi, 136(%r12)
	movq	%rax, -6440(%rbp)
	movl	$4, -6432(%rbp)
	movb	$0, -6428(%rbp)
.L213:
	cmpb	$0, 180(%r12)
	jne	.L321
.L214:
	movslq	-6400(%rbp), %rdx
	leaq	-6392(%rbp), %rcx
	movq	%rcx, -7824(%rbp)
	movl	%edx, 176(%r12)
	movzbl	-6396(%rbp), %eax
	movb	%al, 180(%r12)
	movq	-6408(%rbp), %rax
	cmpq	%rcx, %rax
	je	.L215
	movq	%rax, 168(%r12)
	movq	%rcx, -6408(%rbp)
	movl	$3, -6400(%rbp)
	movb	$0, -6396(%rbp)
.L216:
	cmpb	$0, 228(%r12)
	movl	-6368(%rbp), %eax
	movl	%eax, 208(%r12)
	jne	.L322
.L217:
	movslq	-6352(%rbp), %rdx
	movl	%edx, 224(%r12)
	movzbl	-6348(%rbp), %eax
	movb	%al, 228(%r12)
	movq	-6360(%rbp), %rsi
	leaq	-6346(%rbp), %rax
	cmpq	%rax, %rsi
	je	.L218
	movq	%rsi, 216(%r12)
	movq	%rax, -6360(%rbp)
	movl	$4, -6352(%rbp)
	movb	$0, -6348(%rbp)
.L219:
	cmpb	$0, 260(%r12)
	jne	.L323
.L220:
	movslq	-6320(%rbp), %rdx
	movl	%edx, 256(%r12)
	movzbl	-6316(%rbp), %eax
	movb	%al, 260(%r12)
	movq	-6328(%rbp), %rsi
	leaq	-6312(%rbp), %rax
	cmpq	%rax, %rsi
	je	.L221
	movq	%rsi, 248(%r12)
	movq	%rax, -6328(%rbp)
	movl	$3, -6320(%rbp)
	movb	$0, -6316(%rbp)
.L222:
	cmpb	$0, 308(%r12)
	movl	-6288(%rbp), %eax
	movl	%eax, 288(%r12)
	jne	.L324
.L223:
	movslq	-6272(%rbp), %rdx
	movl	%edx, 304(%r12)
	movzbl	-6268(%rbp), %eax
	movb	%al, 308(%r12)
	movq	-6280(%rbp), %rsi
	leaq	-6266(%rbp), %rax
	cmpq	%rax, %rsi
	je	.L224
	movq	%rsi, 296(%r12)
	movq	%rax, -6280(%rbp)
	movl	$4, -6272(%rbp)
	movb	$0, -6268(%rbp)
.L225:
	cmpb	$0, 340(%r12)
	jne	.L325
.L226:
	movslq	-6240(%rbp), %rdx
	movl	%edx, 336(%r12)
	movzbl	-6236(%rbp), %eax
	movb	%al, 340(%r12)
	movq	-6248(%rbp), %rsi
	leaq	-6232(%rbp), %rax
	cmpq	%rax, %rsi
	je	.L227
	movq	%rsi, 328(%r12)
	movq	%rax, -6248(%rbp)
	movl	$3, -6240(%rbp)
	movb	$0, -6236(%rbp)
.L228:
	cmpb	$0, 388(%r12)
	movl	-6208(%rbp), %eax
	movl	%eax, 368(%r12)
	jne	.L326
.L229:
	movslq	-6192(%rbp), %rdx
	movl	%edx, 384(%r12)
	movzbl	-6188(%rbp), %eax
	movb	%al, 388(%r12)
	movq	-6200(%rbp), %rsi
	leaq	-6186(%rbp), %rax
	cmpq	%rax, %rsi
	je	.L230
	movq	%rsi, 376(%r12)
	movq	%rax, -6200(%rbp)
	movl	$4, -6192(%rbp)
	movb	$0, -6188(%rbp)
.L231:
	cmpb	$0, 420(%r12)
	jne	.L327
.L232:
	movslq	-6160(%rbp), %rdx
	movl	%edx, 416(%r12)
	movzbl	-6156(%rbp), %eax
	movb	%al, 420(%r12)
	movq	-6168(%rbp), %rsi
	leaq	-6152(%rbp), %rax
	cmpq	%rax, %rsi
	je	.L328
	movq	%rsi, 408(%r12)
	movq	%rax, -6168(%rbp)
	movl	$3, -6160(%rbp)
	movb	$0, -6156(%rbp)
.L234:
	cmpb	$0, 468(%r12)
	movl	-6128(%rbp), %eax
	movl	%eax, 448(%r12)
	jne	.L329
.L235:
	movslq	-6112(%rbp), %rdx
	movl	%edx, 464(%r12)
	movzbl	-6108(%rbp), %eax
	movb	%al, 468(%r12)
	movq	-6120(%rbp), %rsi
	leaq	-6106(%rbp), %rax
	cmpq	%rax, %rsi
	je	.L330
	movq	%rsi, 456(%r12)
	movq	%rax, -6120(%rbp)
	movl	$4, -6112(%rbp)
	movb	$0, -6108(%rbp)
.L237:
	movq	-6096(%rbp), %rax
	leaq	-6176(%rbp), %r12
	leaq	-6656(%rbp), %r14
	movq	%rax, 2952(%r15)
	.p2align 4,,10
	.p2align 3
.L238:
	movq	(%r12), %rax
	movq	%r12, %rdi
	subq	$80, %r12
	call	*(%rax)
	cmpq	%r12, %r14
	jne	.L238
	leaq	-6608(%rbp), %r12
	.p2align 4,,10
	.p2align 3
.L239:
	movq	(%r12), %rax
	movq	%r12, %r14
	movq	%r12, %rdi
	subq	$32, %r12
	call	*(%rax)
	cmpq	%r14, %r13
	jne	.L239
	movq	-7776(%rbp), %r12
	movq	%r15, %rdx
	movq	%rbx, %rdi
	movq	-7696(%rbp), %r9
	movl	-7700(%rbp), %r8d
	movq	-7744(%rbp), %rcx
	leaq	-7676(%rbp), %r14
	movq	%r12, %rsi
	call	_ZN6icu_678numparse4impl21AffixMatcherWarehouse19createAffixMatchersERKNS_6number4impl20AffixPatternProviderERNS1_24MutableMatcherCollectionERKNS1_17IgnorablesMatcherEiR10UErrorCode@PLT
	movl	$2, %edi
	call	_ZN6icu_676number4impl7Grouper11forStrategyE23UNumberGroupingStrategy@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%r14, -7832(%rbp)
	movl	%edx, -7668(%rbp)
	movq	-7712(%rbp), %rdx
	leaq	776(%r15), %r12
	movq	%rax, -7676(%rbp)
	call	_ZN6icu_676number4impl7Grouper13setLocaleDataERKNS1_17ParsedPatternInfoERKNS_6LocaleE@PLT
	movq	(%r15), %rax
	movq	-7744(%rbp), %rsi
	movq	%r15, %rdi
	call	*16(%rax)
	movq	(%r15), %rax
	movl	-7700(%rbp), %ecx
	movq	%r14, %rdx
	movq	-7688(%rbp), %rsi
	movq	%r13, %rdi
	movq	16(%rax), %rbx
	call	_ZN6icu_678numparse4impl14DecimalMatcherC1ERKNS_20DecimalFormatSymbolsERKNS_6number4impl7GrouperEi@PLT
	movzbl	-6854(%rbp), %eax
	movzwl	-6856(%rbp), %edx
	leaq	792(%r15), %rdi
	movq	-7720(%rbp), %rsi
	movb	%al, 786(%r15)
	movl	-6852(%rbp), %eax
	movw	%dx, 784(%r15)
	movl	%eax, 788(%r15)
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	leaq	-6784(%rbp), %rax
	leaq	856(%r15), %rdi
	movq	%rax, %rsi
	movq	%rax, -7712(%rbp)
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movdqa	-6704(%rbp), %xmm0
	movdqa	-6720(%rbp), %xmm6
	movq	952(%r15), %rdi
	movups	%xmm6, 920(%r15)
	movups	%xmm0, 936(%r15)
	testq	%rdi, %rdi
	je	.L240
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
.L240:
	movq	-6688(%rbp), %rax
	movq	%rax, 952(%r15)
	movq	$0, -6688(%rbp)
	movq	960(%r15), %rdi
	testq	%rdi, %rdi
	je	.L241
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
.L241:
	movq	-6680(%rbp), %rax
	movq	%rax, 960(%r15)
	movq	968(%r15), %rax
	movq	$0, -6680(%rbp)
	testq	%rax, %rax
	je	.L242
	movq	-8(%rax), %r14
	salq	$6, %r14
	addq	%rax, %r14
	cmpq	%r14, %rax
	je	.L243
	.p2align 4,,10
	.p2align 3
.L244:
	movq	-64(%r14), %rax
	subq	$64, %r14
	movq	%r14, %rdi
	call	*(%rax)
	cmpq	%r14, 968(%r15)
	jne	.L244
.L243:
	leaq	-8(%r14), %rdi
	call	_ZN6icu_677UMemorydaEPv@PLT
.L242:
	movq	-6672(%rbp), %rax
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rax, 968(%r15)
	movq	$0, -6672(%rbp)
	call	*%rbx
	movq	-7800(%rbp), %rax
	movq	%rax, -6864(%rbp)
	movq	-6672(%rbp), %rax
	testq	%rax, %rax
	je	.L245
	movq	-8(%rax), %rbx
	salq	$6, %rbx
	addq	%rax, %rbx
	cmpq	%rbx, %rax
	je	.L246
	.p2align 4,,10
	.p2align 3
.L247:
	movq	-64(%rbx), %rax
	subq	$64, %rbx
	movq	%rbx, %rdi
	call	*(%rax)
	cmpq	%rbx, -6672(%rbp)
	jne	.L247
.L246:
	leaq	-8(%rbx), %rdi
	call	_ZN6icu_677UMemorydaEPv@PLT
.L245:
	movq	-6680(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L248
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
.L248:
	movq	-6688(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L249
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
.L249:
	movq	-7712(%rbp), %rdi
	leaq	-6856(%rbp), %r12
	leaq	280(%r15), %r14
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-7720(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	(%r15), %rax
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	-7688(%rbp), %rsi
	movq	16(%rax), %rbx
	call	_ZN6icu_678numparse4impl16MinusSignMatcherC1ERKNS_20DecimalFormatSymbolsEb@PLT
	leaq	288(%r15), %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	-6792(%rbp), %rax
	movq	%r14, %rsi
	movq	%r15, %rdi
	leaq	688(%r15), %r14
	movq	%rax, 352(%r15)
	movzbl	-6784(%rbp), %eax
	movb	%al, 360(%r15)
	call	*%rbx
	leaq	16+_ZTVN6icu_678numparse4impl13SymbolMatcherE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, -6864(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	(%r15), %rax
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	-7688(%rbp), %rsi
	movq	16(%rax), %rbx
	call	_ZN6icu_678numparse4impl15PlusSignMatcherC1ERKNS_20DecimalFormatSymbolsEb@PLT
	leaq	696(%r15), %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	-6792(%rbp), %rax
	movq	%r14, %rsi
	movq	%r15, %rdi
	leaq	528(%r15), %r14
	movq	%rax, 760(%r15)
	movzbl	-6784(%rbp), %eax
	movb	%al, 768(%r15)
	call	*%rbx
	leaq	16+_ZTVN6icu_678numparse4impl13SymbolMatcherE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, -6864(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	(%r15), %rax
	movq	-7688(%rbp), %rsi
	movq	%r13, %rdi
	movq	16(%rax), %rbx
	call	_ZN6icu_678numparse4impl14PercentMatcherC1ERKNS_20DecimalFormatSymbolsE@PLT
	leaq	536(%r15), %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	-6792(%rbp), %rax
	movq	%r14, %rsi
	movq	%r15, %rdi
	leaq	608(%r15), %r14
	movq	%rax, 600(%r15)
	call	*%rbx
	leaq	16+_ZTVN6icu_678numparse4impl13SymbolMatcherE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, -6864(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	(%r15), %rax
	movq	-7688(%rbp), %rsi
	movq	%r13, %rdi
	movq	16(%rax), %rbx
	call	_ZN6icu_678numparse4impl15PermilleMatcherC1ERKNS_20DecimalFormatSymbolsE@PLT
	leaq	616(%r15), %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	-6792(%rbp), %rax
	movq	%r14, %rsi
	movq	%r15, %rdi
	leaq	368(%r15), %r14
	movq	%rax, 680(%r15)
	call	*%rbx
	leaq	16+_ZTVN6icu_678numparse4impl13SymbolMatcherE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, -6864(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	(%r15), %rax
	movq	-7688(%rbp), %rsi
	movq	%r13, %rdi
	movq	16(%rax), %rbx
	call	_ZN6icu_678numparse4impl10NanMatcherC1ERKNS_20DecimalFormatSymbolsE@PLT
	leaq	376(%r15), %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	-6792(%rbp), %rax
	movq	%r14, %rsi
	movq	%r15, %rdi
	leaq	200(%r15), %r14
	movq	%rax, 440(%r15)
	call	*%rbx
	leaq	16+_ZTVN6icu_678numparse4impl13SymbolMatcherE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, -6864(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	(%r15), %rax
	movq	-7688(%rbp), %rsi
	movq	%r13, %rdi
	movq	16(%rax), %rbx
	call	_ZN6icu_678numparse4impl15InfinityMatcherC1ERKNS_20DecimalFormatSymbolsE@PLT
	leaq	208(%r15), %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	-6792(%rbp), %rax
	movq	%r14, %rsi
	movq	%r15, %rdi
	leaq	-7616(%rbp), %r14
	movq	%rax, 272(%r15)
	call	*%rbx
	leaq	16+_ZTVN6icu_678numparse4impl13SymbolMatcherE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, -6864(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	(%r15), %rax
	movq	%r14, %rdi
	leaq	.LC4(%rip), %rsi
	movq	16(%rax), %rbx
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_678numparse4impl14PaddingMatcherC1ERKNS_13UnicodeStringE@PLT
	leaq	448(%r15), %r8
	leaq	456(%r15), %rdi
	movq	%r12, %rsi
	movq	%r8, -7744(%rbp)
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	-7744(%rbp), %r8
	movq	-6792(%rbp), %rax
	movq	%r15, %rdi
	movq	%r8, %rsi
	movq	%rax, 520(%r15)
	call	*%rbx
	leaq	16+_ZTVN6icu_678numparse4impl13SymbolMatcherE(%rip), %rax
	movq	%r12, %rdi
	leaq	-6776(%rbp), %rbx
	movq	%rax, -6864(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	leaq	976(%r15), %r14
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	(%r15), %rax
	movq	-7832(%rbp), %rdx
	movq	%r13, %rdi
	movq	-7688(%rbp), %rsi
	movq	16(%rax), %rax
	movq	%rax, -7744(%rbp)
	call	_ZN6icu_678numparse4impl17ScientificMatcherC1ERKNS_20DecimalFormatSymbolsERKNS_6number4impl7GrouperE@PLT
	leaq	984(%r15), %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movzbl	-6782(%rbp), %eax
	movzwl	-6784(%rbp), %edx
	movq	%rbx, %rsi
	leaq	1064(%r15), %rdi
	movb	%al, 1058(%r15)
	movl	-6780(%rbp), %eax
	movw	%dx, 1056(%r15)
	movl	%eax, 1060(%r15)
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	leaq	-6712(%rbp), %rax
	leaq	1128(%r15), %rdi
	movq	%rax, %rsi
	movq	%rax, -7832(%rbp)
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movdqu	-6632(%rbp), %xmm0
	movdqu	-6648(%rbp), %xmm7
	movq	1224(%r15), %rdi
	movups	%xmm7, 1192(%r15)
	movups	%xmm0, 1208(%r15)
	testq	%rdi, %rdi
	je	.L250
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
.L250:
	movq	-6616(%rbp), %rax
	movq	%rax, 1224(%r15)
	movq	$0, -6616(%rbp)
	movq	1232(%r15), %rdi
	testq	%rdi, %rdi
	je	.L251
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
.L251:
	movq	-6608(%rbp), %rax
	movq	%rax, 1232(%r15)
	movq	1240(%r15), %rax
	movq	$0, -6608(%rbp)
	testq	%rax, %rax
	je	.L252
	movq	-8(%rax), %rdi
	salq	$6, %rdi
	addq	%rax, %rdi
	cmpq	%rdi, %rax
	je	.L253
	movq	%r12, -7840(%rbp)
	movq	%r15, %r12
	movq	%rbx, %r15
	movq	%rdi, %rbx
	.p2align 4,,10
	.p2align 3
.L254:
	movq	-64(%rbx), %rax
	subq	$64, %rbx
	movq	%rbx, %rdi
	call	*(%rax)
	cmpq	%rbx, 1240(%r12)
	jne	.L254
	movq	%rbx, %rdi
	movq	%r15, %rbx
	movq	%r12, %r15
	movq	-7840(%rbp), %r12
.L253:
	subq	$8, %rdi
	call	_ZN6icu_677UMemorydaEPv@PLT
.L252:
	movq	-6600(%rbp), %rax
	leaq	-6584(%rbp), %r8
	leaq	1256(%r15), %rdi
	movq	%r8, %rsi
	movq	%r8, -7872(%rbp)
	movq	%rax, 1240(%r15)
	movq	$0, -6600(%rbp)
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	-6520(%rbp), %rax
	leaq	-6512(%rbp), %r9
	leaq	1328(%r15), %rdi
	movq	%r9, %rsi
	movq	%r9, -7856(%rbp)
	movq	%rax, 1320(%r15)
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	leaq	-6448(%rbp), %r11
	leaq	1392(%r15), %rdi
	movq	%r11, %rsi
	movq	%r11, -7840(%rbp)
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	%r14, %rsi
	movq	-7744(%rbp), %rax
	movq	%r15, %rdi
	call	*%rax
	movq	-7840(%rbp), %r11
	movq	-7888(%rbp), %rax
	movq	%r11, %rdi
	movq	%rax, -6864(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-7856(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-7872(%rbp), %r8
	leaq	16+_ZTVN6icu_678numparse4impl13SymbolMatcherE(%rip), %rax
	movq	%rax, -6592(%rbp)
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-7800(%rbp), %rax
	movq	%rax, -6792(%rbp)
	movq	-6600(%rbp), %rax
	testq	%rax, %rax
	je	.L255
	movq	-8(%rax), %r14
	salq	$6, %r14
	addq	%rax, %r14
	cmpq	%r14, %rax
	je	.L256
	.p2align 4,,10
	.p2align 3
.L257:
	movq	-64(%r14), %rax
	subq	$64, %r14
	movq	%r14, %rdi
	call	*(%rax)
	cmpq	%r14, -6600(%rbp)
	jne	.L257
.L256:
	leaq	-8(%r14), %rdi
	call	_ZN6icu_677UMemorydaEPv@PLT
.L255:
	movq	-6608(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L258
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
.L258:
	movq	-6616(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L259
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
.L259:
	movq	-7832(%rbp), %rdi
	leaq	1456(%r15), %r14
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	leaq	-6328(%rbp), %r12
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	(%r15), %rax
	movq	%r13, %rdi
	movq	-7688(%rbp), %rdx
	movq	-7696(%rbp), %r8
	movl	-7700(%rbp), %ecx
	leaq	-6712(%rbp), %r13
	movq	16(%rax), %rax
	movq	-7792(%rbp), %rsi
	movq	%rax, -7744(%rbp)
	call	_ZN6icu_678numparse4impl23CombinedCurrencyMatcherC1ERKNS_6number4impl15CurrencySymbolsERKNS_20DecimalFormatSymbolsEiR10UErrorCode@PLT
	movq	-6856(%rbp), %rax
	movq	-7720(%rbp), %rsi
	leaq	1472(%r15), %rdi
	movq	%rax, 1464(%r15)
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	-7712(%rbp), %rsi
	leaq	1536(%r15), %rdi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movzbl	-6720(%rbp), %eax
	movq	-7808(%rbp), %rdx
	movq	%rbx, -7696(%rbp)
	movb	%al, 1600(%r15)
	movq	%rdx, %rbx
	.p2align 4,,10
	.p2align 3
.L260:
	movq	%r13, %rsi
	movq	%rbx, %rdi
	addq	$64, %r13
	addq	$64, %rbx
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	cmpq	%r12, %r13
	jne	.L260
	leaq	1992(%r15), %rdi
	movq	%r12, %rsi
	leaq	-6264(%rbp), %r13
	movq	-7696(%rbp), %rbx
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	leaq	2056(%r15), %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	leaq	-6200(%rbp), %rsi
	leaq	2120(%r15), %rdi
	call	_ZN6icu_6710CharStringaSEOS0_@PLT
	movq	-7744(%rbp), %rax
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	*%rax
	movq	-7728(%rbp), %rax
	cmpb	$0, -6188(%rbp)
	movq	%rax, -6864(%rbp)
	jne	.L331
.L261:
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-7824(%rbp), %r12
	.p2align 4,,10
	.p2align 3
.L262:
	movq	(%r12), %rax
	movq	%r12, %rdi
	subq	$64, %r12
	call	*(%rax)
	cmpq	%r12, %rbx
	jne	.L262
	movq	-7712(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-7720(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	(%r15), %rax
	leaq	4152(%r15), %rsi
	movq	%r15, %rdi
	call	*16(%rax)
	movb	$1, 112(%r15)
	movq	-7896(%rbp), %rdi
	leaq	16+_ZTVN6icu_676number4impl17ParsedPatternInfoE(%rip), %rax
	movq	%rax, -7312(%rbp)
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	movq	-7864(%rbp), %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	movq	-7848(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-7776(%rbp), %rdi
	call	_ZN6icu_676number4impl20AffixPatternProviderD2Ev@PLT
	leaq	-7384(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	-7448(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, -7500(%rbp)
	jne	.L332
.L263:
	leaq	-7544(%rbp), %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	movq	-7760(%rbp), %rdi
	call	_ZN6icu_6720DecimalFormatSymbolsD1Ev@PLT
	movq	-7688(%rbp), %rdi
	call	_ZN6icu_6720DecimalFormatSymbolsD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L333
	addq	$7880, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L314:
	.cfi_restore_state
	movq	4040(%r15), %rdi
	call	uprv_free_67@PLT
	movq	-7832(%rbp), %r11
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L332:
	movq	-7512(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L331:
	movq	-6200(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L261
	.p2align 4,,10
	.p2align 3
.L329:
	movq	456(%r12), %rdi
	call	uprv_free_67@PLT
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L327:
	movq	408(%r12), %rdi
	call	uprv_free_67@PLT
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L326:
	movq	376(%r12), %rdi
	call	uprv_free_67@PLT
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L325:
	movq	328(%r12), %rdi
	call	uprv_free_67@PLT
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L324:
	movq	296(%r12), %rdi
	call	uprv_free_67@PLT
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L323:
	movq	248(%r12), %rdi
	call	uprv_free_67@PLT
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L322:
	movq	216(%r12), %rdi
	call	uprv_free_67@PLT
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L321:
	movq	168(%r12), %rdi
	call	uprv_free_67@PLT
	jmp	.L214
	.p2align 4,,10
	.p2align 3
.L320:
	movq	136(%r12), %rdi
	call	uprv_free_67@PLT
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L319:
	movq	88(%r12), %rdi
	call	uprv_free_67@PLT
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L318:
	movq	56(%r12), %rdi
	call	uprv_free_67@PLT
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L317:
	movq	8(%r12), %rdi
	call	uprv_free_67@PLT
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L316:
	movq	-5856(%rbp), %rdi
	movq	%r11, -7832(%rbp)
	call	uprv_free_67@PLT
	movq	-7832(%rbp), %r11
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L315:
	leaq	4056(%r15), %rdi
	salq	$3, %rdx
	movq	%r8, %rsi
	movq	%r11, -7920(%rbp)
	movq	%rdi, 4040(%r15)
	movb	%cl, -7912(%rbp)
	movq	%r8, -7832(%rbp)
	call	memcpy@PLT
	movzbl	-7912(%rbp), %ecx
	movl	$0, -5792(%rbp)
	movq	-7832(%rbp), %r8
	movq	-7920(%rbp), %r11
	testb	%cl, %cl
	je	.L199
	movq	%r8, %rdi
	movq	%r11, -7832(%rbp)
	call	uprv_free_67@PLT
	movq	-7832(%rbp), %r11
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L221:
	leaq	264(%r12), %rdi
	salq	$3, %rdx
	movq	%rdi, 248(%r12)
	call	memcpy@PLT
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L224:
	leaq	310(%r12), %rdi
	addq	%rdx, %rdx
	movq	%rdi, 296(%r12)
	call	memcpy@PLT
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L227:
	leaq	344(%r12), %rdi
	salq	$3, %rdx
	movq	%rdi, 328(%r12)
	call	memcpy@PLT
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L230:
	leaq	390(%r12), %rdi
	addq	%rdx, %rdx
	movq	%rdi, 376(%r12)
	call	memcpy@PLT
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L328:
	leaq	424(%r12), %rdi
	salq	$3, %rdx
	movq	%rdi, 408(%r12)
	call	memcpy@PLT
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L330:
	leaq	470(%r12), %rdi
	addq	%rdx, %rdx
	movq	%rdi, 456(%r12)
	call	memcpy@PLT
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L209:
	leaq	104(%r12), %rdi
	salq	$3, %rdx
	movq	%rdi, 88(%r12)
	call	memcpy@PLT
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L212:
	leaq	150(%r12), %rdi
	addq	%rdx, %rdx
	movq	%rdi, 136(%r12)
	call	memcpy@PLT
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L215:
	leaq	184(%r12), %rdi
	movq	%rcx, %rsi
	salq	$3, %rdx
	movq	%rdi, 168(%r12)
	call	memcpy@PLT
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L218:
	leaq	230(%r12), %rdi
	addq	%rdx, %rdx
	movq	%rdi, 216(%r12)
	call	memcpy@PLT
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L203:
	leaq	24(%r12), %rdi
	salq	$3, %rdx
	movq	%rdi, 8(%r12)
	call	memcpy@PLT
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L206:
	leaq	70(%r12), %rdi
	addq	%rdx, %rdx
	movq	%rdi, 56(%r12)
	call	memcpy@PLT
	jmp	.L207
.L333:
	call	__stack_chk_fail@PLT
.L313:
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2184, %ebx
	movl	$2472, %r12d
	movq	$1608, -7808(%rbp)
	movq	$3456, -7832(%rbp)
	movq	%rax, %xmm0
	leaq	16+_ZTVN6icu_678numparse4impl14DecimalMatcherE(%rip), %rax
	movq	%rax, -7800(%rbp)
	leaq	16+_ZTVN6icu_678numparse4impl17ScientificMatcherE(%rip), %rax
	movq	%rax, -7888(%rbp)
	leaq	16+_ZTVN6icu_678numparse4impl23CombinedCurrencyMatcherE(%rip), %rax
	movq	%rax, -7728(%rbp)
	jmp	.L193
	.cfi_endproc
.LFE3153:
	.size	_ZN6icu_678numparse4impl16NumberParserImpl18createSimpleParserERKNS_6LocaleERKNS_13UnicodeStringEiR10UErrorCode, .-_ZN6icu_678numparse4impl16NumberParserImpl18createSimpleParserERKNS_6LocaleERKNS_13UnicodeStringEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678numparse4impl16NumberParserImpl21parseLongestRecursiveERNS_13StringSegmentERNS1_12ParsedNumberEiR10UErrorCode
	.type	_ZNK6icu_678numparse4impl16NumberParserImpl21parseLongestRecursiveERNS_13StringSegmentERNS1_12ParsedNumberEiR10UErrorCode, @function
_ZNK6icu_678numparse4impl16NumberParserImpl21parseLongestRecursiveERNS_13StringSegmentERNS1_12ParsedNumberEiR10UErrorCode:
.LFB3321:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%ecx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$552, %rsp
	movq	%rdi, -512(%rbp)
	movq	%rsi, %rdi
	movq	%rdx, -568(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	testl	%eax, %eax
	je	.L334
	testl	%r12d, %r12d
	jne	.L360
.L334:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L361
	addq	$552, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L360:
	.cfi_restore_state
	leaq	-496(%rbp), %rdi
	movq	%r15, %rsi
	movq	%rdi, -544(%rbp)
	call	_ZN6icu_676number4impl15DecimalQuantityC1ERKS2_@PLT
	movq	72(%r15), %rax
	leaq	80(%r15), %rsi
	movq	%rsi, -584(%rbp)
	movq	%rax, -424(%rbp)
	leaq	-416(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -560(%rbp)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	leaq	-352(%rbp), %rax
	leaq	144(%r15), %rsi
	movq	%rax, %rdi
	movq	%rsi, -592(%rbp)
	movq	%rax, -552(%rbp)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	208(%r15), %rax
	leaq	-272(%rbp), %r15
	movq	%r15, %rdi
	movq	%rax, -288(%rbp)
	call	_ZN6icu_678numparse4impl12ParsedNumberC1Ev@PLT
	movq	%rbx, %rdi
	call	_ZNK6icu_6713StringSegment9getOffsetEv@PLT
	movl	12(%r13), %ecx
	movl	%eax, -516(%rbp)
	testl	%ecx, %ecx
	jle	.L362
	leaq	-192(%rbp), %rax
	movq	$0, -504(%rbp)
	movq	%rax, -536(%rbp)
	leaq	-128(%rbp), %rax
	movq	%rax, -528(%rbp)
	leal	1(%r12), %eax
	movl	%eax, -572(%rbp)
	jmp	.L344
	.p2align 4,,10
	.p2align 3
.L337:
	movq	-512(%rbp), %rcx
	addq	$1, -504(%rbp)
	movq	-504(%rbp), %rax
	cmpl	%eax, 12(%rcx)
	jle	.L339
.L344:
	movq	-512(%rbp), %rax
	movq	-504(%rbp), %rcx
	movq	%rbx, %rsi
	movq	16(%rax), %rax
	movq	(%rax,%rcx,8), %r13
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*32(%rax)
	testb	%al, %al
	je	.L337
	xorl	%r12d, %r12d
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L341:
	movl	-516(%rbp), %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6713StringSegment9setOffsetEi@PLT
	cmpb	$0, -517(%rbp)
	je	.L337
.L343:
	movq	%rbx, %rdi
	call	_ZNK6icu_6713StringSegment6lengthEv@PLT
	cmpl	%r12d, %eax
	jle	.L337
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	_ZNK6icu_6713StringSegment11codePointAtEi@PLT
	movq	-544(%rbp), %rsi
	movq	%r15, %rdi
	cmpl	$65535, %eax
	seta	%al
	movzbl	%al, %eax
	leal	1(%r12,%rax), %r12d
	call	_ZN6icu_676number4impl15DecimalQuantityaSERKS2_@PLT
	movq	-424(%rbp), %rax
	movq	-560(%rbp), %rsi
	movq	-536(%rbp), %rdi
	movq	%rax, -200(%rbp)
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	-552(%rbp), %rsi
	movq	-528(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	-288(%rbp), %rax
	movl	%r12d, %esi
	movq	%rbx, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN6icu_6713StringSegment9setLengthEi@PLT
	movq	0(%r13), %rax
	movq	%r15, %rdx
	movq	%r14, %rcx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	*24(%rax)
	movq	%rbx, %rdi
	movb	%al, -517(%rbp)
	call	_ZN6icu_6713StringSegment11resetLengthEv@PLT
	movl	(%r14), %edx
	testl	%edx, %edx
	jg	.L339
	movq	%rbx, %rdi
	call	_ZNK6icu_6713StringSegment9getOffsetEv@PLT
	subl	-516(%rbp), %eax
	cmpl	%r12d, %eax
	jne	.L341
	movl	-572(%rbp), %ecx
	movq	%r14, %r8
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	-512(%rbp), %rdi
	call	_ZNK6icu_678numparse4impl16NumberParserImpl21parseLongestRecursiveERNS_13StringSegmentERNS1_12ParsedNumberEiR10UErrorCode
	movl	(%r14), %eax
	testl	%eax, %eax
	jg	.L339
	movq	-568(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN6icu_678numparse4impl12ParsedNumber12isBetterThanERKS2_@PLT
	testb	%al, %al
	je	.L341
	movq	-568(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN6icu_676number4impl15DecimalQuantityaSERKS2_@PLT
	movq	-568(%rbp), %rdx
	movq	-200(%rbp), %rax
	movq	-536(%rbp), %rsi
	movq	-584(%rbp), %rdi
	movq	%rax, 72(%rdx)
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	-528(%rbp), %rsi
	movq	-592(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	-64(%rbp), %rax
	movq	-568(%rbp), %rdx
	movq	%rax, 208(%rdx)
	jmp	.L341
.L362:
	leaq	-192(%rbp), %rax
	movq	%rax, -536(%rbp)
	leaq	-128(%rbp), %rax
	movq	%rax, -528(%rbp)
	.p2align 4,,10
	.p2align 3
.L339:
	movq	-528(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-536(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	movq	-552(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-560(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-544(%rbp), %rdi
	call	_ZN6icu_676number4impl15DecimalQuantityD1Ev@PLT
	jmp	.L334
.L361:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3321:
	.size	_ZNK6icu_678numparse4impl16NumberParserImpl21parseLongestRecursiveERNS_13StringSegmentERNS1_12ParsedNumberEiR10UErrorCode, .-_ZNK6icu_678numparse4impl16NumberParserImpl21parseLongestRecursiveERNS_13StringSegmentERNS1_12ParsedNumberEiR10UErrorCode
	.align 2
	.p2align 4
	.type	_ZNK6icu_678numparse4impl16NumberParserImpl5parseERKNS_13UnicodeStringEibRNS1_12ParsedNumberER10UErrorCode.part.0, @function
_ZNK6icu_678numparse4impl16NumberParserImpl5parseERKNS_13UnicodeStringEibRNS1_12ParsedNumberER10UErrorCode.part.0:
.LFB4701:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	leaq	-80(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$56, %rsp
	movq	%r9, -88(%rbp)
	movl	8(%rdi), %edx
	movq	%r15, %rdi
	andl	$1, %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6713StringSegmentC1ERKNS_13UnicodeStringEb@PLT
	movl	%r14d, %esi
	movq	%r15, %rdi
	call	_ZN6icu_6713StringSegment12adjustOffsetEi@PLT
	testb	%bl, %bl
	movq	-88(%rbp), %r8
	jne	.L372
	testb	$64, 9(%r12)
	jne	.L373
	movl	$-100, %ecx
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZNK6icu_678numparse4impl16NumberParserImpl21parseLongestRecursiveERNS_13StringSegmentERNS1_12ParsedNumberEiR10UErrorCode
.L368:
	movl	12(%r12), %eax
	testl	%eax, %eax
	jle	.L365
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L366:
	movq	16(%r12), %rax
	movq	%r13, %rsi
	movq	(%rax,%rbx,8), %rdi
	addq	$1, %rbx
	movq	(%rdi), %rax
	call	*40(%rax)
	cmpl	%ebx, 12(%r12)
	jg	.L366
.L365:
	movq	%r13, %rdi
	call	_ZN6icu_678numparse4impl12ParsedNumber11postProcessEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L374
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L373:
	.cfi_restore_state
	movl	$1, %ecx
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZNK6icu_678numparse4impl16NumberParserImpl21parseLongestRecursiveERNS_13StringSegmentERNS1_12ParsedNumberEiR10UErrorCode
	jmp	.L368
	.p2align 4,,10
	.p2align 3
.L372:
	movq	%r8, %rcx
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZNK6icu_678numparse4impl16NumberParserImpl11parseGreedyERNS_13StringSegmentERNS1_12ParsedNumberER10UErrorCode
	jmp	.L368
.L374:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4701:
	.size	_ZNK6icu_678numparse4impl16NumberParserImpl5parseERKNS_13UnicodeStringEibRNS1_12ParsedNumberER10UErrorCode.part.0, .-_ZNK6icu_678numparse4impl16NumberParserImpl5parseERKNS_13UnicodeStringEibRNS1_12ParsedNumberER10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678numparse4impl16NumberParserImpl5parseERKNS_13UnicodeStringEibRNS1_12ParsedNumberER10UErrorCode
	.type	_ZNK6icu_678numparse4impl16NumberParserImpl5parseERKNS_13UnicodeStringEibRNS1_12ParsedNumberER10UErrorCode, @function
_ZNK6icu_678numparse4impl16NumberParserImpl5parseERKNS_13UnicodeStringEibRNS1_12ParsedNumberER10UErrorCode:
.LFB3319:
	.cfi_startproc
	endbr64
	movl	(%r9), %eax
	testl	%eax, %eax
	jle	.L377
	ret
	.p2align 4,,10
	.p2align 3
.L377:
	movzbl	%cl, %ecx
	jmp	_ZNK6icu_678numparse4impl16NumberParserImpl5parseERKNS_13UnicodeStringEibRNS1_12ParsedNumberER10UErrorCode.part.0
	.cfi_endproc
.LFE3319:
	.size	_ZNK6icu_678numparse4impl16NumberParserImpl5parseERKNS_13UnicodeStringEibRNS1_12ParsedNumberER10UErrorCode, .-_ZNK6icu_678numparse4impl16NumberParserImpl5parseERKNS_13UnicodeStringEibRNS1_12ParsedNumberER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_678numparse4impl16NumberParserImpl5parseERKNS_13UnicodeStringEbRNS1_12ParsedNumberER10UErrorCode
	.type	_ZNK6icu_678numparse4impl16NumberParserImpl5parseERKNS_13UnicodeStringEbRNS1_12ParsedNumberER10UErrorCode, @function
_ZNK6icu_678numparse4impl16NumberParserImpl5parseERKNS_13UnicodeStringEbRNS1_12ParsedNumberER10UErrorCode:
.LFB3318:
	.cfi_startproc
	endbr64
	movq	%rcx, %rax
	movl	(%r8), %ecx
	testl	%ecx, %ecx
	jle	.L380
	ret
	.p2align 4,,10
	.p2align 3
.L380:
	movzbl	%dl, %ecx
	movq	%r8, %r9
	xorl	%edx, %edx
	movq	%rax, %r8
	jmp	_ZNK6icu_678numparse4impl16NumberParserImpl5parseERKNS_13UnicodeStringEibRNS1_12ParsedNumberER10UErrorCode.part.0
	.cfi_endproc
.LFE3318:
	.size	_ZNK6icu_678numparse4impl16NumberParserImpl5parseERKNS_13UnicodeStringEbRNS1_12ParsedNumberER10UErrorCode, .-_ZNK6icu_678numparse4impl16NumberParserImpl5parseERKNS_13UnicodeStringEbRNS1_12ParsedNumberER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_678numparse4impl16NumberParserImpl26createParserFromPropertiesERKNS_6number4impl23DecimalFormatPropertiesERKNS_20DecimalFormatSymbolsEbR10UErrorCode
	.type	_ZN6icu_678numparse4impl16NumberParserImpl26createParserFromPropertiesERKNS_6number4impl23DecimalFormatPropertiesERKNS_20DecimalFormatSymbolsEbR10UErrorCode, @function
_ZN6icu_678numparse4impl16NumberParserImpl26createParserFromPropertiesERKNS_6number4impl23DecimalFormatPropertiesERKNS_20DecimalFormatSymbolsEbR10UErrorCode:
.LFB3205:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	$2, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movl	$2, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	$2, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movl	$2, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$4088, %rsp
	movq	%rdi, -3880(%rbp)
	movq	.LC6(%rip), %xmm1
	movq	.LC7(%rip), %xmm2
	movq	%rsi, -3912(%rbp)
	addq	$1872, %rsi
	movl	%edx, -3956(%rbp)
	movq	%rcx, -3888(%rbp)
	movhps	.LC6(%rip), %xmm2
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movaps	%xmm2, -3904(%rbp)
	movq	%rax, %xmm0
	leaq	-3616(%rbp), %rax
	punpcklqdq	%xmm0, %xmm1
	movq	%rax, %rdi
	movq	%rax, -3944(%rbp)
	movaps	%xmm1, -3936(%rbp)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movl	$2, %r9d
	movl	$2, %edx
	movdqa	-3936(%rbp), %xmm1
	movdqa	-3904(%rbp), %xmm2
	movl	$2, %r10d
	movl	$2, %r11d
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%r9w, -1968(%rbp)
	movl	$2, %ecx
	movl	$2, %esi
	movl	$2, %edi
	movw	%r10w, -1904(%rbp)
	movl	$2, %r8d
	movl	$2, %r9d
	movl	$2, %r10d
	movw	%r11w, -1840(%rbp)
	movl	$2, %r11d
	movw	%r12w, -1776(%rbp)
	movl	$2, %r12d
	movw	%r13w, -1688(%rbp)
	movl	$2, %r13d
	movw	%r14w, -1624(%rbp)
	movl	$2, %r14d
	movw	%r15w, -1560(%rbp)
	movl	$2, %r15d
	movw	%dx, -1496(%rbp)
	movl	$2, %edx
	movw	%cx, -1416(%rbp)
	movl	$2, %ecx
	movw	%si, -1352(%rbp)
	movl	$2, %esi
	movw	%di, -1288(%rbp)
	movl	$2, %edi
	movw	%r8w, -1224(%rbp)
	movl	$2, %r8d
	movw	%r9w, -1144(%rbp)
	movl	$2, %r9d
	movq	%rax, -1912(%rbp)
	movq	%rax, -1848(%rbp)
	movq	%rax, -1784(%rbp)
	movb	$1, -1719(%rbp)
	movq	%rax, -1696(%rbp)
	movq	%rax, -1632(%rbp)
	movq	%rax, -1568(%rbp)
	movq	%rax, -1504(%rbp)
	movb	$1, -1439(%rbp)
	movq	%rax, -1360(%rbp)
	movq	%rax, -1296(%rbp)
	movq	%rax, -1232(%rbp)
	movb	$1, -1167(%rbp)
	movaps	%xmm1, -1984(%rbp)
	movaps	%xmm2, -1712(%rbp)
	movups	%xmm1, -1432(%rbp)
	movups	%xmm1, -1160(%rbp)
	movq	%rax, -1088(%rbp)
	movw	%r10w, -1080(%rbp)
	movl	$2, %r10d
	movw	%r11w, -1016(%rbp)
	movl	$2, %r11d
	movw	%r12w, -952(%rbp)
	movl	$2, %r12d
	movw	%si, -536(%rbp)
	movq	%rax, -1024(%rbp)
	movq	%rax, -960(%rbp)
	movb	$1, -895(%rbp)
	movw	%r13w, -872(%rbp)
	movq	%rax, -816(%rbp)
	movw	%r14w, -808(%rbp)
	movq	%rax, -752(%rbp)
	movw	%r15w, -744(%rbp)
	movq	%rax, -688(%rbp)
	movw	%dx, -680(%rbp)
	movb	$1, -623(%rbp)
	movw	%cx, -600(%rbp)
	movq	%rax, -544(%rbp)
	movq	%rax, -480(%rbp)
	movw	%di, -472(%rbp)
	movq	%rax, -416(%rbp)
	movw	%r8w, -408(%rbp)
	movb	$1, -351(%rbp)
	movw	%r9w, -328(%rbp)
	movq	%rax, -272(%rbp)
	movw	%r10w, -264(%rbp)
	movq	%rax, -208(%rbp)
	movw	%r11w, -200(%rbp)
	movq	%rax, -144(%rbp)
	movw	%r12w, -136(%rbp)
	movups	%xmm1, -888(%rbp)
	movups	%xmm1, -616(%rbp)
	movups	%xmm1, -344(%rbp)
	movb	$1, -79(%rbp)
	movq	48(%rbx), %rsi
	movb	$1, -72(%rbp)
	testq	%rsi, %rsi
	je	.L571
	leaq	-1712(%rbp), %rax
	movq	-3888(%rbp), %rcx
	movq	-3880(%rbp), %rdx
	movq	%rax, %rdi
	movq	%rax, -3936(%rbp)
	call	_ZN6icu_676number4impl31CurrencyPluralInfoAffixProvider5setToERKNS_18CurrencyPluralInfoERKNS1_23DecimalFormatPropertiesER10UErrorCode@PLT
	leaq	-1984(%rbp), %rax
	movq	%rax, -3920(%rbp)
.L383:
	movq	-3888(%rbp), %rbx
	xorl	%r14d, %r14d
	movl	(%rbx), %r8d
	testl	%r8d, %r8d
	jg	.L384
	movq	-3944(%rbp), %r12
	movq	-3880(%rbp), %r13
	movq	%rbx, %rcx
	leaq	-3712(%rbp), %r14
	leaq	-3152(%rbp), %r15
	movq	%r14, %rdi
	movq	%r14, -4000(%rbp)
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	_ZN6icu_676number4impl15resolveCurrencyERKNS1_23DecimalFormatPropertiesERKNS_6LocaleER10UErrorCode@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%r15, -3904(%rbp)
	call	_ZN6icu_6712CurrencyUnitC1ERKS0_@PLT
	movq	%rbx, %r8
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	-3912(%rbp), %rcx
	leaq	-3392(%rbp), %rdi
	movq	%rdi, -4008(%rbp)
	call	_ZN6icu_676number4impl15CurrencySymbolsC1ENS_12CurrencyUnitERKNS_6LocaleERKNS_20DecimalFormatSymbolsER10UErrorCode@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	cmpb	$0, 460(%r13)
	movl	$1, -3960(%rbp)
	je	.L572
.L385:
	movq	-3880(%rbp), %rbx
	xorl	%r14d, %r14d
	movq	%rbx, %rdi
	call	_ZN6icu_676number4impl7Grouper13forPropertiesERKNS1_23DecimalFormatPropertiesE@PLT
	movq	%rax, -3868(%rbp)
	movq	-3888(%rbp), %rax
	movl	%edx, -3860(%rbp)
	movl	(%rax), %edi
	testl	%edi, %edi
	jg	.L386
	movzbl	456(%rbx), %ecx
	movq	%rbx, %rax
	movl	%ecx, %ebx
	movb	%cl, -3952(%rbp)
	xorl	$1, %ebx
	cmpb	$0, 457(%rax)
	movzbl	%bl, %ebx
	je	.L387
	orl	$16, %ebx
.L387:
	movq	-3880(%rbp), %rax
	cmpb	$0, 756(%rax)
	je	.L388
	orb	$4, %bh
.L388:
	movl	%ebx, %eax
	orb	$-128, %bl
	orl	$33548, %eax
	cmpl	$1, -3960(%rbp)
	cmove	%eax, %ebx
	leaq	-3868(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -4040(%rbp)
	call	_ZNK6icu_676number4impl7Grouper10getPrimaryEv@PLT
	movl	%eax, %r8d
	movl	%ebx, %eax
	orl	$32, %eax
	testw	%r8w, %r8w
	cmovle	%eax, %ebx
	movl	%ebx, %r15d
	orl	$2, %r15d
	cmpb	$0, -3956(%rbp)
	movl	%r15d, -3952(%rbp)
	je	.L573
.L495:
	movl	$4192, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, %xmm0
	je	.L574
	leaq	16+_ZTVN6icu_678numparse4impl15PlusSignMatcherE(%rip), %rax
	leaq	16+_ZTVN6icu_678numparse4impl15InfinityMatcherE(%rip), %rcx
	movl	$2, %r8d
	movl	$2, %r9d
	movq	%rax, %xmm3
	movl	$2, %r10d
	movl	$2, %r11d
	movl	$2, %ebx
	leaq	16+_ZTVN6icu_678numparse4impl15PermilleMatcherE(%rip), %rax
	punpcklqdq	%xmm0, %xmm3
	movl	$2, %r12d
	movl	$2, %r13d
	movq	%rax, %xmm4
	leaq	16+_ZTVN6icu_678numparse4impl14PercentMatcherE(%rip), %rax
	movl	$2, %r15d
	movl	$2, %edx
	movq	%rax, %xmm5
	leaq	16+_ZTVN6icu_678numparse4impl16MinusSignMatcherE(%rip), %rax
	punpcklqdq	%xmm0, %xmm4
	movw	%r8w, 136(%r14)
	movq	%rax, %xmm6
	leaq	16+_ZTVN6icu_678numparse4impl16NumberParserImplE(%rip), %rax
	punpcklqdq	%xmm0, %xmm5
	movw	%r9w, 216(%r14)
	movq	%rax, (%r14)
	movl	-3952(%rbp), %eax
	punpcklqdq	%xmm0, %xmm6
	movl	$2, %esi
	movw	%r10w, 296(%r14)
	movl	$2, %edi
	movl	$2, %r8d
	movl	$2, %r9d
	movl	%eax, 8(%r14)
	leaq	32(%r14), %rax
	movl	$2, %r10d
	movq	%rax, 16(%r14)
	leaq	16+_ZTVN6icu_678numparse4impl17IgnorablesMatcherE(%rip), %rax
	movq	%rax, %xmm1
	movw	%r11w, 384(%r14)
	movl	$2, %r11d
	punpcklqdq	%xmm0, %xmm1
	movw	%bx, 464(%r14)
	movl	$2, %ebx
	movups	%xmm1, 120(%r14)
	movq	%rcx, %xmm1
	leaq	16+_ZTVN6icu_678numparse4impl10NanMatcherE(%rip), %rcx
	punpcklqdq	%xmm0, %xmm1
	movw	%r12w, 544(%r14)
	movl	$2, %r12d
	movups	%xmm1, 200(%r14)
	movq	%rcx, %xmm1
	leaq	16+_ZTVN6icu_678numparse4impl14PaddingMatcherE(%rip), %rcx
	punpcklqdq	%xmm0, %xmm1
	movw	%r13w, 624(%r14)
	movl	$2, %r13d
	movups	%xmm1, 368(%r14)
	movq	%rcx, %xmm1
	leaq	16+_ZTVN6icu_678numparse4impl14DecimalMatcherE(%rip), %rcx
	punpcklqdq	%xmm0, %xmm1
	movw	%r15w, 704(%r14)
	movl	$2, %r15d
	movq	%rcx, -4016(%rbp)
	movq	%rcx, 776(%r14)
	movw	%dx, 800(%r14)
	leaq	16+_ZTVN6icu_678numparse4impl17ScientificMatcherE(%rip), %rdx
	movups	%xmm1, 448(%r14)
	pxor	%xmm1, %xmm1
	movl	$0, 12(%r14)
	movl	$10, 24(%r14)
	movb	$0, 28(%r14)
	movb	$0, 112(%r14)
	movaps	%xmm3, -4064(%rbp)
	movaps	%xmm4, -4080(%rbp)
	movaps	%xmm5, -3984(%rbp)
	movaps	%xmm6, -4032(%rbp)
	movups	%xmm6, 280(%r14)
	movups	%xmm5, 528(%r14)
	movups	%xmm4, 608(%r14)
	movups	%xmm3, 688(%r14)
	movq	%xmm0, 792(%r14)
	movq	%xmm0, 856(%r14)
	movq	%rax, 1248(%r14)
	leaq	16+_ZTVN6icu_678numparse4impl23CombinedCurrencyMatcherE(%rip), %rax
	movq	%rax, -3992(%rbp)
	movq	%rax, 1456(%r14)
	movl	$2, %eax
	movw	%ax, 1680(%r14)
	movl	$2, %eax
	movw	%ax, 1744(%r14)
	movl	$2, %eax
	movw	%si, 864(%r14)
	xorl	%esi, %esi
	movq	%rdx, 976(%r14)
	movl	$2, %edx
	movq	%rcx, 1048(%r14)
	movl	$2, %ecx
	movw	%bx, 1400(%r14)
	leaq	2472(%r14), %rbx
	movw	%r12w, 1480(%r14)
	leaq	16+_ZTVN6icu_678numparse4impl19AffixPatternMatcherE(%rip), %r12
	movw	%r13w, 1544(%r14)
	movq	%rbx, %r13
	movw	%r15w, 1616(%r14)
	leaq	2184(%r14), %r15
	movq	$0, 968(%r14)
	movw	%di, 992(%r14)
	movw	%r8w, 1072(%r14)
	movw	%r9w, 1136(%r14)
	movq	$0, 1240(%r14)
	movw	%r10w, 1264(%r14)
	movw	%r11w, 1336(%r14)
	movups	%xmm1, 952(%r14)
	movq	%xmm0, 984(%r14)
	movq	%xmm0, 1064(%r14)
	movq	%xmm0, 1128(%r14)
	movups	%xmm1, 1224(%r14)
	movq	%xmm0, 1256(%r14)
	movq	%xmm0, 1328(%r14)
	movq	%xmm0, 1392(%r14)
	movq	%xmm0, 1472(%r14)
	movq	%xmm0, 1536(%r14)
	movq	%xmm0, 1608(%r14)
	movq	%xmm0, 1672(%r14)
	movq	%xmm0, 1736(%r14)
	movq	%xmm0, 1800(%r14)
	movw	%ax, 1808(%r14)
	movl	$2, %eax
	movw	%ax, 1872(%r14)
	movl	$2, %eax
	movw	%ax, 1936(%r14)
	leaq	2133(%r14), %rax
	movq	%rax, 2120(%r14)
	leaq	16+_ZTVN6icu_678numparse4impl12AffixMatcherE(%rip), %rax
	movq	%rax, 2184(%r14)
	movq	%rax, 2216(%r14)
	movq	%rax, 2248(%r14)
	movq	%rax, 2280(%r14)
	movq	%rax, 2312(%r14)
	movq	%rax, 2344(%r14)
	movq	%rax, 2376(%r14)
	movq	%rax, 2408(%r14)
	movq	%rax, 2440(%r14)
	leaq	2952(%r14), %rax
	movw	%dx, 2000(%r14)
	movw	%cx, 2064(%r14)
	movl	$0, 2176(%r14)
	movl	$40, 2128(%r14)
	movw	%si, 2132(%r14)
	movq	%rax, -3968(%rbp)
	movq	%xmm0, 1864(%r14)
	movq	%xmm0, 1928(%r14)
	movq	%xmm0, 1992(%r14)
	movq	%xmm0, 2056(%r14)
	.p2align 4,,10
	.p2align 3
.L397:
	movq	%r13, %rdi
	call	_ZN6icu_678numparse4impl18ArraySeriesMatcherC2Ev@PLT
	leaq	70(%r13), %rdx
	xorl	%edi, %edi
	movq	%r12, 0(%r13)
	movq	%rdx, 56(%r13)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	addq	$80, %r13
	movl	$4, -16(%r13)
	movq	%rax, %xmm0
	movb	$0, -12(%r13)
	movw	%di, -10(%r13)
	cmpq	%r13, -3968(%rbp)
	jne	.L397
	movl	$2, %esi
	movl	$2, %edi
	movl	$2, %edx
	movdqa	-4032(%rbp), %xmm7
	movq	-3992(%rbp), %rax
	movl	$2, %r8d
	movl	$2, %r9d
	movl	$2, %r10d
	movups	%xmm7, 2968(%r14)
	movdqa	-4064(%rbp), %xmm7
	movl	$2, %r11d
	movl	$2, %r13d
	movq	%rax, 3304(%r14)
	movl	$2, %eax
	movl	$2, %ecx
	leaq	3456(%r14), %r12
	movw	%ax, 3528(%r14)
	movl	$2, %eax
	movw	%ax, 3592(%r14)
	movl	$2, %eax
	movups	%xmm7, 3056(%r14)
	movdqa	-3984(%rbp), %xmm7
	movw	%ax, 3656(%r14)
	movl	$2, %eax
	movw	%ax, 3720(%r14)
	movl	$2, %eax
	movups	%xmm7, 3144(%r14)
	movdqa	-4080(%rbp), %xmm7
	movw	%ax, 3784(%r14)
	leaq	3981(%r14), %rax
	movw	%si, 2984(%r14)
	xorl	%esi, %esi
	movups	%xmm7, 3224(%r14)
	movq	%xmm0, 3320(%r14)
	movq	%xmm0, 3384(%r14)
	movq	%xmm0, 3456(%r14)
	movq	%xmm0, 3520(%r14)
	movq	%xmm0, 3584(%r14)
	movq	%xmm0, 3648(%r14)
	movq	%xmm0, 3712(%r14)
	movq	%xmm0, 3776(%r14)
	movq	%xmm0, 3840(%r14)
	movw	%di, 3072(%r14)
	movw	%r8w, 3160(%r14)
	movw	%r9w, 3240(%r14)
	movw	%r10w, 3328(%r14)
	movw	%r11w, 3392(%r14)
	movw	%r13w, 3464(%r14)
	movw	%dx, 3848(%r14)
	movq	%xmm0, 3904(%r14)
	movq	%rax, 3968(%r14)
	leaq	4056(%r14), %rax
	movw	%cx, 3912(%r14)
	leaq	16+_ZTVN6icu_678numparse4impl21RequireAffixValidatorE(%rip), %rcx
	movq	%rax, 4040(%r14)
	movq	%rcx, %xmm0
	leaq	16+_ZTVN6icu_678numparse4impl24RequireCurrencyValidatorE(%rip), %rax
	leaq	16+_ZTVN6icu_678numparse4impl22RequireNumberValidatorE(%rip), %rcx
	movq	%rax, %xmm7
	leaq	16+_ZTVN6icu_678numparse4impl32RequireDecimalSeparatorValidatorE(%rip), %rax
	movl	$0, 4024(%r14)
	punpcklqdq	%xmm7, %xmm0
	movq	%rax, 4136(%r14)
	leaq	16+_ZTVN6icu_678numparse4impl22MultiplierParseHandlerE(%rip), %rax
	movups	%xmm0, 4120(%r14)
	movq	%rax, %xmm3
	movq	%rcx, %xmm0
	movl	$40, 3976(%r14)
	punpcklqdq	%xmm3, %xmm0
	movw	%si, 3980(%r14)
	movl	$0, 4032(%r14)
	movl	$8, 4048(%r14)
	movb	$0, 4052(%r14)
	movl	$0, 4168(%r14)
	movq	$0, 4176(%r14)
	movl	$0, 4184(%r14)
	movups	%xmm0, 4152(%r14)
.L396:
	movl	-3952(%rbp), %esi
	movq	-3904(%rbp), %rdi
	leaq	-3144(%rbp), %r13
	call	_ZN6icu_678numparse4impl17IgnorablesMatcherC1Ei@PLT
	movq	%r13, %rsi
	leaq	120(%r14), %rax
	leaq	128(%r14), %rdi
	movq	%rax, -4032(%rbp)
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	-3080(%rbp), %rax
	leaq	16+_ZTVN6icu_678numparse4impl13SymbolMatcherE(%rip), %rcx
	movq	%r13, %rdi
	movq	%rax, 192(%r14)
	movq	%rcx, -3152(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-4008(%rbp), %rcx
	leaq	120(%r14), %rax
	movq	-3904(%rbp), %rdi
	movq	%rax, -3744(%rbp)
	movl	-3952(%rbp), %eax
	leaq	-3760(%rbp), %rsi
	movq	%rcx, -3760(%rbp)
	movq	-3912(%rbp), %rcx
	movl	%eax, -3728(%rbp)
	movq	%rcx, -3752(%rbp)
	movq	-3944(%rbp), %rcx
	movq	%rcx, -3736(%rbp)
	call	_ZN6icu_678numparse4impl26AffixTokenMatcherWarehouseC1EPKNS1_26AffixTokenMatcherSetupDataE@PLT
	leaq	2960(%r14), %rax
	leaq	2976(%r14), %rdi
	movq	%rax, -4064(%rbp)
	movq	-3152(%rbp), %rax
	movq	%rax, 2960(%r14)
	leaq	-3136(%rbp), %rax
	movq	%rax, %rsi
	movq	%rax, -3968(%rbp)
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	-3072(%rbp), %rax
	leaq	3064(%r14), %rdi
	movq	%rax, 3040(%r14)
	movzbl	-3064(%rbp), %eax
	movb	%al, 3048(%r14)
	leaq	-3048(%rbp), %rax
	movq	%rax, %rsi
	movq	%rax, -4080(%rbp)
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	-2984(%rbp), %rax
	leaq	3152(%r14), %rdi
	movq	%rax, 3128(%r14)
	movzbl	-2976(%rbp), %eax
	movb	%al, 3136(%r14)
	leaq	-2960(%rbp), %rax
	movq	%rax, %rsi
	movq	%rax, -4048(%rbp)
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	-2896(%rbp), %rax
	leaq	3232(%r14), %rdi
	movq	%rax, 3216(%r14)
	leaq	-2880(%rbp), %rax
	movq	%rax, %rsi
	movq	%rax, -4088(%rbp)
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	-2816(%rbp), %rax
	leaq	3320(%r14), %rdi
	movq	%rax, 3296(%r14)
	movq	-2800(%rbp), %rax
	movq	%rax, 3312(%r14)
	leaq	-2792(%rbp), %rax
	movq	%rax, %rsi
	movq	%rax, -4096(%rbp)
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	leaq	-2728(%rbp), %rax
	leaq	3384(%r14), %rdi
	movq	%rax, %rsi
	movq	%rax, -4104(%rbp)
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movzbl	-2664(%rbp), %eax
	movq	%r12, %rdi
	leaq	-2656(%rbp), %rdx
	movq	%rbx, -3984(%rbp)
	leaq	-2272(%rbp), %r12
	movq	%rdx, %rbx
	movb	%al, 3448(%r14)
	movq	%r13, -4112(%rbp)
	movq	%rdi, %r13
	.p2align 4,,10
	.p2align 3
.L398:
	movq	%rbx, %rsi
	movq	%r13, %rdi
	addq	$64, %rbx
	addq	$64, %r13
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	cmpq	%r12, %rbx
	jne	.L398
	leaq	3840(%r14), %rdi
	movq	%r12, %rsi
	movq	-3984(%rbp), %rbx
	movq	-4112(%rbp), %r13
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	leaq	-2208(%rbp), %r9
	leaq	3904(%r14), %rdi
	movq	%r9, %rsi
	movq	%r9, -3984(%rbp)
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	leaq	-2144(%rbp), %rsi
	leaq	3968(%r14), %rdi
	call	_ZN6icu_6710CharStringaSEOS0_@PLT
	movl	-2080(%rbp), %eax
	cmpb	$0, 4052(%r14)
	movq	-3984(%rbp), %r9
	movl	%eax, 4032(%r14)
	jne	.L575
.L399:
	movslq	-2064(%rbp), %rax
	leaq	-2056(%rbp), %rdx
	movl	%eax, 4048(%r14)
	movzbl	-2060(%rbp), %r11d
	movb	%r11b, 4052(%r14)
	movq	-2072(%rbp), %r8
	cmpq	%rdx, %r8
	je	.L576
	movq	%r8, 4040(%r14)
.L402:
	movq	-3992(%rbp), %rax
	cmpb	$0, -2132(%rbp)
	movq	%rax, -2808(%rbp)
	jne	.L577
.L403:
	movq	%r9, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	leaq	-2336(%rbp), %r12
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	-2720(%rbp), %rax
	movq	%rax, -3984(%rbp)
	.p2align 4,,10
	.p2align 3
.L404:
	movq	(%r12), %rdx
	movq	%r12, %rdi
	subq	$64, %r12
	call	*(%rdx)
	cmpq	%r12, -3984(%rbp)
	jne	.L404
	movq	-4104(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-4096(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-4088(%rbp), %rdi
	leaq	16+_ZTVN6icu_678numparse4impl13SymbolMatcherE(%rip), %rax
	movq	%rax, %r12
	movq	%rax, -2888(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-4048(%rbp), %rdi
	movq	%r12, -2968(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-4080(%rbp), %rdi
	movq	%r12, -3056(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-3968(%rbp), %rdi
	movq	%r12, -3144(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-4064(%rbp), %rsi
	movq	-3904(%rbp), %rdi
	call	_ZN6icu_678numparse4impl21AffixMatcherWarehouseC1EPNS1_26AffixTokenMatcherWarehouseE@PLT
	movq	-3144(%rbp), %rax
	movq	%rax, 8(%r15)
	movq	-3136(%rbp), %rax
	movq	%rax, 16(%r15)
	movl	-3128(%rbp), %eax
	movdqu	-3112(%rbp), %xmm4
	movl	%eax, 24(%r15)
	movl	-3096(%rbp), %eax
	movups	%xmm4, 40(%r15)
	movdqu	-3080(%rbp), %xmm5
	movl	%eax, 56(%r15)
	movl	-3064(%rbp), %eax
	movups	%xmm5, 72(%r15)
	movdqu	-3048(%rbp), %xmm6
	movl	%eax, 88(%r15)
	movl	-3032(%rbp), %eax
	movups	%xmm6, 104(%r15)
	movdqu	-3016(%rbp), %xmm7
	movl	%eax, 120(%r15)
	movl	-3000(%rbp), %eax
	movups	%xmm7, 136(%r15)
	movdqu	-2984(%rbp), %xmm3
	movl	%eax, 152(%r15)
	movl	-2968(%rbp), %eax
	movups	%xmm3, 168(%r15)
	movdqu	-2952(%rbp), %xmm4
	movl	%eax, 184(%r15)
	movl	-2936(%rbp), %eax
	movups	%xmm4, 200(%r15)
	movdqu	-2920(%rbp), %xmm5
	movl	%eax, 216(%r15)
	movups	%xmm5, 232(%r15)
	movl	-2904(%rbp), %eax
	movdqu	-2888(%rbp), %xmm6
	movl	%eax, 248(%r15)
	movl	-2872(%rbp), %eax
	movups	%xmm6, 264(%r15)
	movl	%eax, 280(%r15)
	cmpb	$0, 20(%rbx)
	jne	.L578
.L405:
	movslq	-2848(%rbp), %rax
	movl	%eax, 16(%rbx)
	movzbl	-2844(%rbp), %edx
	movb	%dl, 20(%rbx)
	movq	-2856(%rbp), %rsi
	leaq	-2840(%rbp), %rdx
	cmpq	%rdx, %rsi
	je	.L406
	movq	%rsi, 8(%rbx)
	movq	%rdx, -2856(%rbp)
	movl	$3, -2848(%rbp)
	movb	$0, -2844(%rbp)
.L407:
	movl	-2816(%rbp), %eax
	cmpb	$0, 68(%rbx)
	movl	%eax, 48(%rbx)
	jne	.L579
.L408:
	movslq	-2800(%rbp), %rax
	movl	%eax, 64(%rbx)
	movzbl	-2796(%rbp), %edx
	movb	%dl, 68(%rbx)
	movq	-2808(%rbp), %rsi
	leaq	-2794(%rbp), %rdx
	cmpq	%rdx, %rsi
	je	.L409
	movq	%rsi, 56(%rbx)
	movq	%rdx, -2808(%rbp)
	movl	$4, -2800(%rbp)
	movb	$0, -2796(%rbp)
.L410:
	cmpb	$0, 100(%rbx)
	jne	.L580
.L411:
	movslq	-2768(%rbp), %rax
	movl	%eax, 96(%rbx)
	movzbl	-2764(%rbp), %edx
	movb	%dl, 100(%rbx)
	movq	-2776(%rbp), %rsi
	leaq	-2760(%rbp), %rdx
	cmpq	%rdx, %rsi
	je	.L412
	movq	%rsi, 88(%rbx)
	movq	%rdx, -2776(%rbp)
	movl	$3, -2768(%rbp)
	movb	$0, -2764(%rbp)
.L413:
	movl	-2736(%rbp), %eax
	cmpb	$0, 148(%rbx)
	movl	%eax, 128(%rbx)
	jne	.L581
.L414:
	movslq	-2720(%rbp), %rax
	movl	%eax, 144(%rbx)
	movzbl	-2716(%rbp), %edx
	movb	%dl, 148(%rbx)
	movq	-2728(%rbp), %rsi
	leaq	-2714(%rbp), %rdx
	cmpq	%rdx, %rsi
	je	.L415
	movq	%rsi, 136(%rbx)
	movq	%rdx, -2728(%rbp)
	movl	$4, -2720(%rbp)
	movb	$0, -2716(%rbp)
.L416:
	cmpb	$0, 180(%rbx)
	jne	.L582
.L417:
	movslq	-2688(%rbp), %rax
	leaq	-2680(%rbp), %rcx
	movq	%rcx, -4064(%rbp)
	movl	%eax, 176(%rbx)
	movzbl	-2684(%rbp), %edx
	movb	%dl, 180(%rbx)
	movq	-2696(%rbp), %rdx
	cmpq	%rcx, %rdx
	je	.L418
	movq	%rdx, 168(%rbx)
	movq	%rcx, -2696(%rbp)
	movl	$3, -2688(%rbp)
	movb	$0, -2684(%rbp)
.L419:
	movl	-2656(%rbp), %eax
	cmpb	$0, 228(%rbx)
	movl	%eax, 208(%rbx)
	jne	.L583
.L420:
	movslq	-2640(%rbp), %rax
	movl	%eax, 224(%rbx)
	movzbl	-2636(%rbp), %edx
	movb	%dl, 228(%rbx)
	movq	-2648(%rbp), %rsi
	leaq	-2634(%rbp), %rdx
	cmpq	%rdx, %rsi
	je	.L421
	movq	%rsi, 216(%rbx)
	movq	%rdx, -2648(%rbp)
	movl	$4, -2640(%rbp)
	movb	$0, -2636(%rbp)
.L422:
	cmpb	$0, 260(%rbx)
	jne	.L584
.L423:
	movslq	-2608(%rbp), %rax
	movl	%eax, 256(%rbx)
	movzbl	-2604(%rbp), %edx
	movb	%dl, 260(%rbx)
	movq	-2616(%rbp), %rsi
	leaq	-2600(%rbp), %rdx
	cmpq	%rdx, %rsi
	je	.L424
	movq	%rsi, 248(%rbx)
	movq	%rdx, -2616(%rbp)
	movl	$3, -2608(%rbp)
	movb	$0, -2604(%rbp)
.L425:
	movl	-2576(%rbp), %eax
	cmpb	$0, 308(%rbx)
	movl	%eax, 288(%rbx)
	jne	.L585
.L426:
	movslq	-2560(%rbp), %rax
	movl	%eax, 304(%rbx)
	movzbl	-2556(%rbp), %edx
	movb	%dl, 308(%rbx)
	movq	-2568(%rbp), %rsi
	leaq	-2554(%rbp), %rdx
	cmpq	%rdx, %rsi
	je	.L427
	movq	%rsi, 296(%rbx)
	movq	%rdx, -2568(%rbp)
	movl	$4, -2560(%rbp)
	movb	$0, -2556(%rbp)
.L428:
	cmpb	$0, 340(%rbx)
	jne	.L586
.L429:
	movslq	-2528(%rbp), %rax
	movl	%eax, 336(%rbx)
	movzbl	-2524(%rbp), %edx
	movb	%dl, 340(%rbx)
	movq	-2536(%rbp), %rsi
	leaq	-2520(%rbp), %rdx
	cmpq	%rdx, %rsi
	je	.L430
	movq	%rsi, 328(%rbx)
	movq	%rdx, -2536(%rbp)
	movl	$3, -2528(%rbp)
	movb	$0, -2524(%rbp)
.L431:
	movl	-2496(%rbp), %eax
	cmpb	$0, 388(%rbx)
	movl	%eax, 368(%rbx)
	jne	.L587
.L432:
	movslq	-2480(%rbp), %rax
	movl	%eax, 384(%rbx)
	movzbl	-2476(%rbp), %edx
	movb	%dl, 388(%rbx)
	movq	-2488(%rbp), %rsi
	leaq	-2474(%rbp), %rdx
	cmpq	%rdx, %rsi
	je	.L433
	movq	%rsi, 376(%rbx)
	movq	%rdx, -2488(%rbp)
	movl	$4, -2480(%rbp)
	movb	$0, -2476(%rbp)
.L434:
	cmpb	$0, 420(%rbx)
	jne	.L588
.L435:
	movslq	-2448(%rbp), %rax
	movl	%eax, 416(%rbx)
	movzbl	-2444(%rbp), %edx
	movb	%dl, 420(%rbx)
	movq	-2456(%rbp), %rsi
	leaq	-2440(%rbp), %rdx
	cmpq	%rdx, %rsi
	je	.L589
	movq	%rsi, 408(%rbx)
	movq	%rdx, -2456(%rbp)
	movl	$3, -2448(%rbp)
	movb	$0, -2444(%rbp)
.L437:
	movl	-2416(%rbp), %eax
	cmpb	$0, 468(%rbx)
	movl	%eax, 448(%rbx)
	jne	.L590
.L438:
	movslq	-2400(%rbp), %rax
	movl	%eax, 464(%rbx)
	movzbl	-2396(%rbp), %edx
	movb	%dl, 468(%rbx)
	movq	-2408(%rbp), %rsi
	leaq	-2394(%rbp), %rdx
	cmpq	%rdx, %rsi
	je	.L591
	movq	%rsi, 456(%rbx)
	movq	%rdx, -2408(%rbp)
	movl	$4, -2400(%rbp)
	movb	$0, -2396(%rbp)
.L440:
	movq	-2384(%rbp), %rax
	leaq	-2464(%rbp), %r12
	leaq	-2944(%rbp), %rbx
	movq	%rax, 2952(%r14)
	.p2align 4,,10
	.p2align 3
.L441:
	movq	(%r12), %rax
	movq	%r12, %rdi
	subq	$80, %r12
	call	*(%rax)
	cmpq	%rbx, %r12
	jne	.L441
	leaq	-2896(%rbp), %r12
	.p2align 4,,10
	.p2align 3
.L442:
	movq	(%r12), %rdx
	movq	%r12, %rdi
	movq	%r12, %rbx
	subq	$32, %r12
	call	*(%rdx)
	cmpq	-3904(%rbp), %rbx
	jne	.L442
	cmpb	$0, -72(%rbp)
	movq	-3920(%rbp), %rsi
	movq	%r14, %rdx
	movq	%r15, %rdi
	cmove	-3936(%rbp), %rsi
	movq	-3888(%rbp), %r9
	movl	-3952(%rbp), %r8d
	movq	-4032(%rbp), %rcx
	call	_ZN6icu_678numparse4impl21AffixMatcherWarehouse19createAffixMatchersERKNS_6number4impl20AffixPatternProviderERNS1_24MutableMatcherCollectionERKNS1_17IgnorablesMatcherEiR10UErrorCode@PLT
	cmpb	$0, -3956(%rbp)
	jne	.L447
	cmpb	$0, -72(%rbp)
	movq	-3920(%rbp), %rdi
	cmove	-3936(%rbp), %rdi
	movq	(%rdi), %rax
	call	*40(%rax)
	testb	%al, %al
	je	.L592
.L447:
	movq	(%r14), %rax
	movq	-3888(%rbp), %r8
	leaq	1608(%r14), %r15
	leaq	-3000(%rbp), %rbx
	movl	-3952(%rbp), %ecx
	movq	-3912(%rbp), %rdx
	leaq	-2616(%rbp), %r12
	movq	16(%rax), %rax
	movq	-4008(%rbp), %rsi
	movq	-3904(%rbp), %rdi
	movq	%rax, -4080(%rbp)
	call	_ZN6icu_678numparse4impl23CombinedCurrencyMatcherC1ERKNS_6number4impl15CurrencySymbolsERKNS_20DecimalFormatSymbolsEiR10UErrorCode@PLT
	leaq	1456(%r14), %rax
	movq	-3968(%rbp), %rsi
	leaq	1472(%r14), %rdi
	movq	%rax, -4008(%rbp)
	movq	-3144(%rbp), %rax
	movq	%rax, 1464(%r14)
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	leaq	-3072(%rbp), %rax
	leaq	1536(%r14), %rdi
	movq	%rax, %rsi
	movq	%rax, -3984(%rbp)
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movzbl	-3008(%rbp), %eax
	movb	%al, 1600(%r14)
	.p2align 4,,10
	.p2align 3
.L445:
	movq	%rbx, %rsi
	movq	%r15, %rdi
	addq	$64, %rbx
	addq	$64, %r15
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	cmpq	%r12, %rbx
	jne	.L445
	leaq	1992(%r14), %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	leaq	-2552(%rbp), %r8
	leaq	2056(%r14), %rdi
	movq	%r8, %rsi
	movq	%r8, -4048(%rbp)
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	leaq	-2488(%rbp), %rsi
	leaq	2120(%r14), %rdi
	call	_ZN6icu_6710CharStringaSEOS0_@PLT
	movq	-4080(%rbp), %rax
	movq	-4008(%rbp), %rsi
	movq	%r14, %rdi
	call	*%rax
	movq	-3992(%rbp), %rax
	cmpb	$0, -2476(%rbp)
	movq	-4048(%rbp), %r8
	movq	%rax, -3152(%rbp)
	jne	.L593
.L449:
	movq	%r8, %rdi
	leaq	-3064(%rbp), %rbx
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-4064(%rbp), %r15
	.p2align 4,,10
	.p2align 3
.L450:
	movq	(%r15), %rax
	movq	%r15, %rdi
	subq	$64, %r15
	call	*(%rax)
	cmpq	%r15, %rbx
	jne	.L450
	movq	-3984(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-3968(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L448:
	cmpl	$1, -3960(%rbp)
	je	.L451
	cmpb	$0, -72(%rbp)
	movq	-3920(%rbp), %rdi
	movl	$-3, %esi
	cmove	-3936(%rbp), %rdi
	movq	-3888(%rbp), %rdx
	movq	(%rdi), %rax
	call	*72(%rax)
	testb	%al, %al
	jne	.L594
.L453:
	cmpb	$0, -72(%rbp)
	movq	-3936(%rbp), %rdi
	movl	$-4, %esi
	cmovne	-3920(%rbp), %rdi
	movq	-3888(%rbp), %rdx
	movq	(%rdi), %rax
	call	*72(%rax)
	testb	%al, %al
	jne	.L595
.L454:
	movq	(%r14), %rax
	movq	-3912(%rbp), %rsi
	xorl	%edx, %edx
	leaq	688(%r14), %r12
	movq	-3904(%rbp), %rdi
	leaq	16+_ZTVN6icu_678numparse4impl13SymbolMatcherE(%rip), %r15
	movq	16(%rax), %rbx
	call	_ZN6icu_678numparse4impl15PlusSignMatcherC1ERKNS_20DecimalFormatSymbolsEb@PLT
	leaq	696(%r14), %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	-3080(%rbp), %rax
	movq	%r12, %rsi
	movq	%r14, %rdi
	leaq	280(%r14), %r12
	movq	%rax, 760(%r14)
	movzbl	-3072(%rbp), %eax
	movb	%al, 768(%r14)
	call	*%rbx
	movq	%r13, %rdi
	movq	%r15, -3152(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	(%r14), %rax
	movq	-3912(%rbp), %rsi
	xorl	%edx, %edx
	movq	-3904(%rbp), %rdi
	movq	16(%rax), %rbx
	call	_ZN6icu_678numparse4impl16MinusSignMatcherC1ERKNS_20DecimalFormatSymbolsEb@PLT
	leaq	288(%r14), %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	-3080(%rbp), %rax
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rax, 352(%r14)
	movzbl	-3072(%rbp), %eax
	movb	%al, 360(%r14)
	call	*%rbx
	movq	%r13, %rdi
	movq	%r15, -3152(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L451:
	movq	(%r14), %rax
	movq	-3912(%rbp), %rsi
	leaq	368(%r14), %r12
	leaq	16+_ZTVN6icu_678numparse4impl13SymbolMatcherE(%rip), %r15
	movq	-3904(%rbp), %rdi
	movq	16(%rax), %rbx
	call	_ZN6icu_678numparse4impl10NanMatcherC1ERKNS_20DecimalFormatSymbolsE@PLT
	leaq	376(%r14), %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	-3080(%rbp), %rax
	movq	%r12, %rsi
	movq	%r14, %rdi
	leaq	200(%r14), %r12
	movq	%rax, 440(%r14)
	call	*%rbx
	movq	%r13, %rdi
	movq	%r15, -3152(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	(%r14), %rax
	movq	-3912(%rbp), %rsi
	movq	-3904(%rbp), %rdi
	movq	16(%rax), %rbx
	call	_ZN6icu_678numparse4impl15InfinityMatcherC1ERKNS_20DecimalFormatSymbolsE@PLT
	leaq	208(%r14), %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	-3080(%rbp), %rax
	movq	%r12, %rsi
	movq	%r14, %rdi
	leaq	-3680(%rbp), %r12
	movq	%rax, 272(%r14)
	call	*%rbx
	movq	%r13, %rdi
	movq	%r15, -3152(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-3880(%rbp), %rax
	movq	%r12, %rdi
	leaq	392(%rax), %rsi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	testb	$1, -3672(%rbp)
	je	.L596
.L456:
	movq	(%r14), %rax
	movq	-4032(%rbp), %rsi
	movq	%r14, %rdi
	leaq	776(%r14), %rbx
	call	*16(%rax)
	movq	(%r14), %rax
	movl	-3952(%rbp), %ecx
	movq	-4040(%rbp), %rdx
	movq	-3912(%rbp), %rsi
	movq	16(%rax), %rax
	movq	-3904(%rbp), %rdi
	movq	%rax, -3888(%rbp)
	call	_ZN6icu_678numparse4impl14DecimalMatcherC1ERKNS_20DecimalFormatSymbolsERKNS_6number4impl7GrouperEi@PLT
	movzbl	-3142(%rbp), %eax
	movzwl	-3144(%rbp), %edx
	leaq	792(%r14), %rdi
	movq	-3968(%rbp), %rsi
	movb	%al, 786(%r14)
	movl	-3140(%rbp), %eax
	movw	%dx, 784(%r14)
	movl	%eax, 788(%r14)
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	-3984(%rbp), %rsi
	leaq	856(%r14), %rdi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movdqa	-2992(%rbp), %xmm0
	movdqa	-3008(%rbp), %xmm7
	movq	952(%r14), %rdi
	movups	%xmm7, 920(%r14)
	movups	%xmm0, 936(%r14)
	testq	%rdi, %rdi
	je	.L458
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
.L458:
	movq	-2976(%rbp), %rax
	movq	%rax, 952(%r14)
	movq	$0, -2976(%rbp)
	movq	960(%r14), %rdi
	testq	%rdi, %rdi
	je	.L459
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
.L459:
	movq	-2968(%rbp), %rax
	movq	%rax, 960(%r14)
	movq	968(%r14), %rax
	movq	$0, -2968(%rbp)
	testq	%rax, %rax
	je	.L460
	movq	-8(%rax), %r15
	salq	$6, %r15
	addq	%rax, %r15
	cmpq	%r15, %rax
	je	.L461
	.p2align 4,,10
	.p2align 3
.L462:
	movq	-64(%r15), %rax
	subq	$64, %r15
	movq	%r15, %rdi
	call	*(%rax)
	cmpq	%r15, 968(%r14)
	jne	.L462
.L461:
	leaq	-8(%r15), %rdi
	call	_ZN6icu_677UMemorydaEPv@PLT
.L460:
	movq	-2960(%rbp), %rax
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	%rax, 968(%r14)
	movq	-3888(%rbp), %rax
	movq	$0, -2960(%rbp)
	call	*%rax
	movq	-4016(%rbp), %rax
	movq	%rax, -3152(%rbp)
	movq	-2960(%rbp), %rax
	testq	%rax, %rax
	je	.L463
	movq	-8(%rax), %rbx
	salq	$6, %rbx
	addq	%rax, %rbx
	cmpq	%rbx, %rax
	je	.L464
	.p2align 4,,10
	.p2align 3
.L465:
	movq	-64(%rbx), %rax
	subq	$64, %rbx
	movq	%rbx, %rdi
	call	*(%rax)
	cmpq	%rbx, -2960(%rbp)
	jne	.L465
.L464:
	leaq	-8(%rbx), %rdi
	call	_ZN6icu_677UMemorydaEPv@PLT
.L463:
	movq	-2968(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L466
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
.L466:
	movq	-2976(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L467
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
.L467:
	movq	-3984(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-3968(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-3880(%rbp), %rax
	cmpb	$0, 468(%rax)
	je	.L468
	movl	96(%rax), %ecx
	testl	%ecx, %ecx
	jle	.L469
.L468:
	movq	(%r14), %rax
	movq	-4040(%rbp), %rdx
	leaq	-3064(%rbp), %r15
	movq	-3912(%rbp), %rsi
	movq	-3904(%rbp), %rdi
	movq	16(%rax), %rax
	movq	%rax, -3888(%rbp)
	call	_ZN6icu_678numparse4impl17ScientificMatcherC1ERKNS_20DecimalFormatSymbolsERKNS_6number4impl7GrouperE@PLT
	leaq	976(%r14), %rax
	leaq	984(%r14), %rdi
	movq	%r13, %rsi
	movq	%rax, -3904(%rbp)
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movzbl	-3070(%rbp), %eax
	movzwl	-3072(%rbp), %edx
	movq	%r15, %rsi
	leaq	1064(%r14), %rdi
	movb	%al, 1058(%r14)
	movl	-3068(%rbp), %eax
	movw	%dx, 1056(%r14)
	movl	%eax, 1060(%r14)
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	leaq	-3000(%rbp), %rax
	leaq	1128(%r14), %rdi
	movq	%rax, %rsi
	movq	%rax, -3912(%rbp)
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movdqu	-2920(%rbp), %xmm0
	movdqu	-2936(%rbp), %xmm3
	movq	1224(%r14), %rdi
	movups	%xmm3, 1192(%r14)
	movups	%xmm0, 1208(%r14)
	testq	%rdi, %rdi
	je	.L470
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
.L470:
	movq	-2904(%rbp), %rax
	movq	%rax, 1224(%r14)
	movq	$0, -2904(%rbp)
	movq	1232(%r14), %rdi
	testq	%rdi, %rdi
	je	.L471
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
.L471:
	movq	-2896(%rbp), %rax
	movq	%rax, 1232(%r14)
	movq	1240(%r14), %rax
	movq	$0, -2896(%rbp)
	testq	%rax, %rax
	je	.L472
	movq	-8(%rax), %rdi
	salq	$6, %rdi
	leaq	(%rax,%rdi), %rbx
	cmpq	%rbx, %rax
	je	.L473
	.p2align 4,,10
	.p2align 3
.L474:
	movq	-64(%rbx), %rax
	subq	$64, %rbx
	movq	%rbx, %rdi
	call	*(%rax)
	cmpq	%rbx, 1240(%r14)
	jne	.L474
.L473:
	leaq	-8(%rbx), %rdi
	call	_ZN6icu_677UMemorydaEPv@PLT
.L472:
	movq	-2888(%rbp), %rax
	leaq	-2872(%rbp), %r8
	leaq	1256(%r14), %rdi
	movq	%r8, %rsi
	movq	%r8, -3984(%rbp)
	movq	%rax, 1240(%r14)
	movq	$0, -2888(%rbp)
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	-2808(%rbp), %rax
	leaq	-2800(%rbp), %r9
	leaq	1328(%r14), %rdi
	movq	%r9, %rsi
	movq	%r9, -3968(%rbp)
	movq	%rax, 1320(%r14)
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	leaq	-2736(%rbp), %r11
	leaq	1392(%r14), %rdi
	movq	%r11, %rsi
	movq	%r11, -3952(%rbp)
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	-3904(%rbp), %rsi
	movq	-3888(%rbp), %rax
	movq	%r14, %rdi
	call	*%rax
	movq	-3952(%rbp), %r11
	leaq	16+_ZTVN6icu_678numparse4impl17ScientificMatcherE(%rip), %rax
	movq	%rax, -3152(%rbp)
	movq	%r11, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-3968(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-3984(%rbp), %r8
	leaq	16+_ZTVN6icu_678numparse4impl13SymbolMatcherE(%rip), %rax
	movq	%rax, -2880(%rbp)
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-4016(%rbp), %rax
	movq	%rax, -3080(%rbp)
	movq	-2888(%rbp), %rax
	testq	%rax, %rax
	je	.L475
	movq	-8(%rax), %rbx
	salq	$6, %rbx
	addq	%rax, %rbx
	cmpq	%rbx, %rax
	je	.L476
	.p2align 4,,10
	.p2align 3
.L477:
	movq	-64(%rbx), %rax
	subq	$64, %rbx
	movq	%rbx, %rdi
	call	*(%rax)
	cmpq	%rbx, -2888(%rbp)
	jne	.L477
.L476:
	leaq	-8(%rbx), %rdi
	call	_ZN6icu_677UMemorydaEPv@PLT
.L475:
	movq	-2896(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L478
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
.L478:
	movq	-2904(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L479
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
.L479:
	movq	-3912(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L469:
	movq	(%r14), %rax
	leaq	4152(%r14), %rsi
	movq	%r14, %rdi
	call	*16(%rax)
	cmpl	$1, -3960(%rbp)
	jne	.L480
	movq	(%r14), %rax
	leaq	4120(%r14), %rsi
	movq	%r14, %rdi
	call	*16(%rax)
.L480:
	cmpb	$0, -3956(%rbp)
	je	.L481
	movq	(%r14), %rax
	leaq	4128(%r14), %rsi
	movq	%r14, %rdi
	call	*16(%rax)
.L481:
	movq	-3880(%rbp), %rax
	cmpb	$0, 64(%rax)
	je	.L482
	cmpb	$0, 65(%rax)
	movl	$1, %esi
	jne	.L483
	movl	84(%rax), %edx
	xorl	%esi, %esi
	testl	%edx, %edx
	setne	%sil
.L483:
	movq	(%r14), %rax
	leaq	-3792(%rbp), %rdi
	movq	16(%rax), %rbx
	call	_ZN6icu_678numparse4impl32RequireDecimalSeparatorValidatorC1Eb@PLT
	movzbl	-3784(%rbp), %eax
	leaq	4136(%r14), %rsi
	movq	%r14, %rdi
	movb	%al, 4144(%r14)
	call	*%rbx
.L482:
	movq	-3880(%rbp), %rcx
	leaq	-3856(%rbp), %r13
	movl	120(%rcx), %esi
	movl	116(%rcx), %eax
	addl	80(%rcx), %esi
	je	.L484
	cmpl	$1, %eax
	jne	.L597
.L484:
	testl	%esi, %esi
	jne	.L598
	cmpl	$1, %eax
	je	.L487
	pxor	%xmm0, %xmm0
	movq	%r13, %rdi
	cvtsi2sdl	%eax, %xmm0
	call	_ZN6icu_676number5Scale8byDoubleEd@PLT
.L485:
	movl	-3856(%rbp), %eax
	testl	%eax, %eax
	jne	.L488
	cmpq	$0, -3848(%rbp)
	je	.L489
.L488:
	movq	(%r14), %rax
	leaq	-3824(%rbp), %rbx
	movq	%r13, %rsi
	leaq	-3784(%rbp), %r15
	movq	%rbx, %rdi
	movq	16(%rax), %rdx
	movq	%rdx, -3904(%rbp)
	call	_ZN6icu_676number5ScaleC1ERKS1_@PLT
	leaq	-3792(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN6icu_678numparse4impl22MultiplierParseHandlerC1ENS_6number5ScaleE@PLT
	leaq	4160(%r14), %r8
	leaq	4168(%r14), %rdi
	movq	%r15, %rsi
	movq	%r8, -3880(%rbp)
	call	_ZN6icu_676number5ScaleaSEOS1_@PLT
	movq	-3880(%rbp), %r8
	movq	-3904(%rbp), %rdx
	movq	%r14, %rdi
	movq	%r8, %rsi
	call	*%rdx
	leaq	16+_ZTVN6icu_678numparse4impl22MultiplierParseHandlerE(%rip), %rax
	movq	%r15, %rdi
	movq	%rax, -3792(%rbp)
	call	_ZN6icu_676number5ScaleD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_676number5ScaleD1Ev@PLT
.L489:
	movb	$1, 112(%r14)
	movq	%r13, %rdi
	call	_ZN6icu_676number5ScaleD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L386:
	leaq	-3224(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	-3288(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, -3340(%rbp)
	jne	.L599
.L490:
	leaq	-3384(%rbp), %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
	movq	-4000(%rbp), %rdi
	call	_ZN6icu_6712CurrencyUnitD1Ev@PLT
.L384:
	leaq	16+_ZTVN6icu_676number4impl31CurrencyPluralInfoAffixProviderE(%rip), %rax
	leaq	-344(%rbp), %rbx
	movq	%rax, -1712(%rbp)
	leaq	-1976(%rbp), %r12
	.p2align 4,,10
	.p2align 3
.L491:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	subq	$272, %rbx
	call	*(%rax)
	cmpq	%rbx, %r12
	jne	.L491
	movq	-3936(%rbp), %rdi
	call	_ZN6icu_676number4impl20AffixPatternProviderD2Ev@PLT
	leaq	16+_ZTVN6icu_676number4impl30PropertiesAffixPatternProviderE(%rip), %rax
	leaq	-1784(%rbp), %rdi
	movq	%rax, -1984(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	-1848(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	-1912(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-3920(%rbp), %rdi
	call	_ZN6icu_676number4impl20AffixPatternProviderD2Ev@PLT
	movq	-3944(%rbp), %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L600
	addq	$4088, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L572:
	.cfi_restore_state
	movl	464(%r13), %eax
	movl	%eax, -3960(%rbp)
	jmp	.L385
	.p2align 4,,10
	.p2align 3
.L599:
	movq	-3352(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L490
	.p2align 4,,10
	.p2align 3
.L571:
	leaq	-1984(%rbp), %rax
	movq	-3888(%rbp), %rdx
	movq	-3880(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rax, -3920(%rbp)
	call	_ZN6icu_676number4impl30PropertiesAffixPatternProvider5setToERKNS1_23DecimalFormatPropertiesER10UErrorCode@PLT
	leaq	-1712(%rbp), %rax
	movb	$1, -72(%rbp)
	movq	%rax, -3936(%rbp)
	jmp	.L383
	.p2align 4,,10
	.p2align 3
.L573:
	cmpb	$0, -72(%rbp)
	movq	-3920(%rbp), %rdi
	cmove	-3936(%rbp), %rdi
	movq	(%rdi), %rax
	call	*40(%rax)
	testb	%al, %al
	cmovne	%r15d, %ebx
	orb	$32, %bh
	movl	%ebx, -3952(%rbp)
	jmp	.L495
	.p2align 4,,10
	.p2align 3
.L592:
	leaq	-3072(%rbp), %rax
	movq	%rax, -3984(%rbp)
	jmp	.L448
	.p2align 4,,10
	.p2align 3
.L597:
	pxor	%xmm0, %xmm0
	movq	%r13, %rdi
	cvtsi2sdl	%eax, %xmm0
	call	_ZN6icu_676number5Scale21byDoubleAndPowerOfTenEdi@PLT
	jmp	.L485
	.p2align 4,,10
	.p2align 3
.L598:
	movq	%r13, %rdi
	call	_ZN6icu_676number5Scale10powerOfTenEi@PLT
	jmp	.L485
	.p2align 4,,10
	.p2align 3
.L593:
	movq	-2488(%rbp), %rdi
	movq	%r8, -3992(%rbp)
	call	uprv_free_67@PLT
	movq	-3992(%rbp), %r8
	jmp	.L449
	.p2align 4,,10
	.p2align 3
.L594:
	movq	(%r14), %rax
	movq	-3912(%rbp), %rsi
	leaq	528(%r14), %r12
	movq	-3904(%rbp), %rdi
	movq	16(%rax), %rbx
	call	_ZN6icu_678numparse4impl14PercentMatcherC1ERKNS_20DecimalFormatSymbolsE@PLT
	leaq	536(%r14), %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	-3080(%rbp), %rax
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rax, 600(%r14)
	call	*%rbx
	leaq	16+_ZTVN6icu_678numparse4impl13SymbolMatcherE(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, -3152(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L453
	.p2align 4,,10
	.p2align 3
.L595:
	movq	(%r14), %rax
	movq	-3912(%rbp), %rsi
	leaq	608(%r14), %r12
	movq	-3904(%rbp), %rdi
	movq	16(%rax), %rbx
	call	_ZN6icu_678numparse4impl15PermilleMatcherC1ERKNS_20DecimalFormatSymbolsE@PLT
	leaq	616(%r14), %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	-3080(%rbp), %rax
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rax, 680(%r14)
	call	*%rbx
	leaq	16+_ZTVN6icu_678numparse4impl13SymbolMatcherE(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, -3152(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L454
	.p2align 4,,10
	.p2align 3
.L596:
	movq	-4032(%rbp), %rdi
	call	_ZNK6icu_678numparse4impl13SymbolMatcher6getSetEv@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZNK6icu_6710UnicodeSet8containsERKNS_13UnicodeStringE@PLT
	testb	%al, %al
	jne	.L456
	movq	(%r14), %rax
	movq	-3904(%rbp), %rdi
	movq	%r12, %rsi
	leaq	448(%r14), %r15
	movq	16(%rax), %rbx
	call	_ZN6icu_678numparse4impl14PaddingMatcherC1ERKNS_13UnicodeStringE@PLT
	leaq	456(%r14), %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	-3080(%rbp), %rax
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%rax, 520(%r14)
	call	*%rbx
	leaq	16+_ZTVN6icu_678numparse4impl13SymbolMatcherE(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, -3152(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L456
	.p2align 4,,10
	.p2align 3
.L578:
	movq	8(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L577:
	movq	-2144(%rbp), %rdi
	movq	%r9, -3984(%rbp)
	call	uprv_free_67@PLT
	movq	-3984(%rbp), %r9
	jmp	.L403
	.p2align 4,,10
	.p2align 3
.L575:
	movq	4040(%r14), %rdi
	call	uprv_free_67@PLT
	movq	-3984(%rbp), %r9
	jmp	.L399
	.p2align 4,,10
	.p2align 3
.L582:
	movq	168(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L417
	.p2align 4,,10
	.p2align 3
.L581:
	movq	136(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L414
	.p2align 4,,10
	.p2align 3
.L580:
	movq	88(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L411
	.p2align 4,,10
	.p2align 3
.L579:
	movq	56(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L408
	.p2align 4,,10
	.p2align 3
.L590:
	movq	456(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L438
	.p2align 4,,10
	.p2align 3
.L588:
	movq	408(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L435
	.p2align 4,,10
	.p2align 3
.L587:
	movq	376(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L432
	.p2align 4,,10
	.p2align 3
.L586:
	movq	328(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L429
	.p2align 4,,10
	.p2align 3
.L585:
	movq	296(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L426
	.p2align 4,,10
	.p2align 3
.L584:
	movq	248(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L423
	.p2align 4,,10
	.p2align 3
.L583:
	movq	216(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L420
	.p2align 4,,10
	.p2align 3
.L421:
	leaq	230(%rbx), %rdi
	leaq	(%rax,%rax), %rdx
	movq	%rdi, 216(%rbx)
	call	memcpy@PLT
	jmp	.L422
	.p2align 4,,10
	.p2align 3
.L418:
	leaq	184(%rbx), %rdi
	movq	%rcx, %rsi
	leaq	0(,%rax,8), %rdx
	movq	%rdi, 168(%rbx)
	call	memcpy@PLT
	jmp	.L419
	.p2align 4,,10
	.p2align 3
.L427:
	leaq	310(%rbx), %rdi
	leaq	(%rax,%rax), %rdx
	movq	%rdi, 296(%rbx)
	call	memcpy@PLT
	jmp	.L428
	.p2align 4,,10
	.p2align 3
.L424:
	leaq	264(%rbx), %rdi
	leaq	0(,%rax,8), %rdx
	movq	%rdi, 248(%rbx)
	call	memcpy@PLT
	jmp	.L425
	.p2align 4,,10
	.p2align 3
.L576:
	leaq	4056(%r14), %rdi
	movq	%r8, %rsi
	leaq	0(,%rax,8), %rdx
	movq	%r9, -4120(%rbp)
	movq	%rdi, 4040(%r14)
	movb	%r11b, -4112(%rbp)
	movq	%r8, -3984(%rbp)
	call	memcpy@PLT
	movzbl	-4112(%rbp), %r11d
	movl	$0, -2080(%rbp)
	movq	-3984(%rbp), %r8
	movq	-4120(%rbp), %r9
	testb	%r11b, %r11b
	je	.L402
	movq	%r8, %rdi
	movq	%r9, -3984(%rbp)
	call	uprv_free_67@PLT
	movq	-3984(%rbp), %r9
	jmp	.L402
	.p2align 4,,10
	.p2align 3
.L415:
	leaq	150(%rbx), %rdi
	leaq	(%rax,%rax), %rdx
	movq	%rdi, 136(%rbx)
	call	memcpy@PLT
	jmp	.L416
	.p2align 4,,10
	.p2align 3
.L433:
	leaq	390(%rbx), %rdi
	leaq	(%rax,%rax), %rdx
	movq	%rdi, 376(%rbx)
	call	memcpy@PLT
	jmp	.L434
	.p2align 4,,10
	.p2align 3
.L430:
	leaq	344(%rbx), %rdi
	leaq	0(,%rax,8), %rdx
	movq	%rdi, 328(%rbx)
	call	memcpy@PLT
	jmp	.L431
	.p2align 4,,10
	.p2align 3
.L412:
	leaq	104(%rbx), %rdi
	leaq	0(,%rax,8), %rdx
	movq	%rdi, 88(%rbx)
	call	memcpy@PLT
	jmp	.L413
	.p2align 4,,10
	.p2align 3
.L409:
	leaq	70(%rbx), %rdi
	leaq	(%rax,%rax), %rdx
	movq	%rdi, 56(%rbx)
	call	memcpy@PLT
	jmp	.L410
	.p2align 4,,10
	.p2align 3
.L406:
	leaq	24(%rbx), %rdi
	leaq	0(,%rax,8), %rdx
	movq	%rdi, 8(%rbx)
	call	memcpy@PLT
	jmp	.L407
	.p2align 4,,10
	.p2align 3
.L589:
	leaq	424(%rbx), %rdi
	leaq	0(,%rax,8), %rdx
	movq	%rdi, 408(%rbx)
	call	memcpy@PLT
	jmp	.L437
	.p2align 4,,10
	.p2align 3
.L591:
	leaq	470(%rbx), %rdi
	leaq	(%rax,%rax), %rdx
	movq	%rdi, 456(%rbx)
	call	memcpy@PLT
	jmp	.L440
	.p2align 4,,10
	.p2align 3
.L487:
	movq	%r13, %rdi
	call	_ZN6icu_676number5Scale4noneEv@PLT
	jmp	.L485
.L600:
	call	__stack_chk_fail@PLT
.L574:
	leaq	16+_ZTVN6icu_678numparse4impl14DecimalMatcherE(%rip), %rax
	movl	$3456, %r12d
	movl	$2184, %r15d
	movl	$2472, %ebx
	movq	%rax, -4016(%rbp)
	leaq	16+_ZTVN6icu_678numparse4impl23CombinedCurrencyMatcherE(%rip), %rax
	movq	%rax, -3992(%rbp)
	jmp	.L396
	.cfi_endproc
.LFE3205:
	.size	_ZN6icu_678numparse4impl16NumberParserImpl26createParserFromPropertiesERKNS_6number4impl23DecimalFormatPropertiesERKNS_20DecimalFormatSymbolsEbR10UErrorCode, .-_ZN6icu_678numparse4impl16NumberParserImpl26createParserFromPropertiesERKNS_6number4impl23DecimalFormatPropertiesERKNS_20DecimalFormatSymbolsEbR10UErrorCode
	.weak	_ZTSN6icu_677UMemoryE
	.section	.rodata._ZTSN6icu_677UMemoryE,"aG",@progbits,_ZTSN6icu_677UMemoryE,comdat
	.align 16
	.type	_ZTSN6icu_677UMemoryE, @object
	.size	_ZTSN6icu_677UMemoryE, 18
_ZTSN6icu_677UMemoryE:
	.string	"N6icu_677UMemoryE"
	.weak	_ZTIN6icu_677UMemoryE
	.section	.data.rel.ro._ZTIN6icu_677UMemoryE,"awG",@progbits,_ZTIN6icu_677UMemoryE,comdat
	.align 8
	.type	_ZTIN6icu_677UMemoryE, @object
	.size	_ZTIN6icu_677UMemoryE, 16
_ZTIN6icu_677UMemoryE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN6icu_677UMemoryE
	.weak	_ZTSN6icu_678numparse4impl18NumberParseMatcherE
	.section	.rodata._ZTSN6icu_678numparse4impl18NumberParseMatcherE,"aG",@progbits,_ZTSN6icu_678numparse4impl18NumberParseMatcherE,comdat
	.align 32
	.type	_ZTSN6icu_678numparse4impl18NumberParseMatcherE, @object
	.size	_ZTSN6icu_678numparse4impl18NumberParseMatcherE, 44
_ZTSN6icu_678numparse4impl18NumberParseMatcherE:
	.string	"N6icu_678numparse4impl18NumberParseMatcherE"
	.weak	_ZTIN6icu_678numparse4impl18NumberParseMatcherE
	.section	.data.rel.ro._ZTIN6icu_678numparse4impl18NumberParseMatcherE,"awG",@progbits,_ZTIN6icu_678numparse4impl18NumberParseMatcherE,comdat
	.align 8
	.type	_ZTIN6icu_678numparse4impl18NumberParseMatcherE, @object
	.size	_ZTIN6icu_678numparse4impl18NumberParseMatcherE, 16
_ZTIN6icu_678numparse4impl18NumberParseMatcherE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN6icu_678numparse4impl18NumberParseMatcherE
	.weak	_ZTSN6icu_678numparse4impl24MutableMatcherCollectionE
	.section	.rodata._ZTSN6icu_678numparse4impl24MutableMatcherCollectionE,"aG",@progbits,_ZTSN6icu_678numparse4impl24MutableMatcherCollectionE,comdat
	.align 32
	.type	_ZTSN6icu_678numparse4impl24MutableMatcherCollectionE, @object
	.size	_ZTSN6icu_678numparse4impl24MutableMatcherCollectionE, 50
_ZTSN6icu_678numparse4impl24MutableMatcherCollectionE:
	.string	"N6icu_678numparse4impl24MutableMatcherCollectionE"
	.weak	_ZTIN6icu_678numparse4impl24MutableMatcherCollectionE
	.section	.data.rel.ro._ZTIN6icu_678numparse4impl24MutableMatcherCollectionE,"awG",@progbits,_ZTIN6icu_678numparse4impl24MutableMatcherCollectionE,comdat
	.align 8
	.type	_ZTIN6icu_678numparse4impl24MutableMatcherCollectionE, @object
	.size	_ZTIN6icu_678numparse4impl24MutableMatcherCollectionE, 16
_ZTIN6icu_678numparse4impl24MutableMatcherCollectionE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN6icu_678numparse4impl24MutableMatcherCollectionE
	.weak	_ZTSN6icu_678numparse4impl19AffixPatternMatcherE
	.section	.rodata._ZTSN6icu_678numparse4impl19AffixPatternMatcherE,"aG",@progbits,_ZTSN6icu_678numparse4impl19AffixPatternMatcherE,comdat
	.align 32
	.type	_ZTSN6icu_678numparse4impl19AffixPatternMatcherE, @object
	.size	_ZTSN6icu_678numparse4impl19AffixPatternMatcherE, 45
_ZTSN6icu_678numparse4impl19AffixPatternMatcherE:
	.string	"N6icu_678numparse4impl19AffixPatternMatcherE"
	.weak	_ZTIN6icu_678numparse4impl19AffixPatternMatcherE
	.section	.data.rel.ro._ZTIN6icu_678numparse4impl19AffixPatternMatcherE,"awG",@progbits,_ZTIN6icu_678numparse4impl19AffixPatternMatcherE,comdat
	.align 8
	.type	_ZTIN6icu_678numparse4impl19AffixPatternMatcherE, @object
	.size	_ZTIN6icu_678numparse4impl19AffixPatternMatcherE, 24
_ZTIN6icu_678numparse4impl19AffixPatternMatcherE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_678numparse4impl19AffixPatternMatcherE
	.quad	_ZTIN6icu_678numparse4impl18ArraySeriesMatcherE
	.weak	_ZTSN6icu_678numparse4impl16NumberParserImplE
	.section	.rodata._ZTSN6icu_678numparse4impl16NumberParserImplE,"aG",@progbits,_ZTSN6icu_678numparse4impl16NumberParserImplE,comdat
	.align 32
	.type	_ZTSN6icu_678numparse4impl16NumberParserImplE, @object
	.size	_ZTSN6icu_678numparse4impl16NumberParserImplE, 42
_ZTSN6icu_678numparse4impl16NumberParserImplE:
	.string	"N6icu_678numparse4impl16NumberParserImplE"
	.weak	_ZTIN6icu_678numparse4impl16NumberParserImplE
	.section	.data.rel.ro._ZTIN6icu_678numparse4impl16NumberParserImplE,"awG",@progbits,_ZTIN6icu_678numparse4impl16NumberParserImplE,comdat
	.align 8
	.type	_ZTIN6icu_678numparse4impl16NumberParserImplE, @object
	.size	_ZTIN6icu_678numparse4impl16NumberParserImplE, 56
_ZTIN6icu_678numparse4impl16NumberParserImplE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN6icu_678numparse4impl16NumberParserImplE
	.long	0
	.long	2
	.quad	_ZTIN6icu_678numparse4impl24MutableMatcherCollectionE
	.quad	2
	.quad	_ZTIN6icu_677UMemoryE
	.quad	2
	.weak	_ZTVN6icu_678numparse4impl19AffixPatternMatcherE
	.section	.data.rel.ro._ZTVN6icu_678numparse4impl19AffixPatternMatcherE,"awG",@progbits,_ZTVN6icu_678numparse4impl19AffixPatternMatcherE,comdat
	.align 8
	.type	_ZTVN6icu_678numparse4impl19AffixPatternMatcherE, @object
	.size	_ZTVN6icu_678numparse4impl19AffixPatternMatcherE, 96
_ZTVN6icu_678numparse4impl19AffixPatternMatcherE:
	.quad	0
	.quad	_ZTIN6icu_678numparse4impl19AffixPatternMatcherE
	.quad	_ZN6icu_678numparse4impl19AffixPatternMatcherD1Ev
	.quad	_ZN6icu_678numparse4impl19AffixPatternMatcherD0Ev
	.quad	_ZNK6icu_678numparse4impl18NumberParseMatcher10isFlexibleEv
	.quad	_ZNK6icu_678numparse4impl13SeriesMatcher5matchERNS_13StringSegmentERNS1_12ParsedNumberER10UErrorCode
	.quad	_ZNK6icu_678numparse4impl13SeriesMatcher9smokeTestERKNS_13StringSegmentE
	.quad	_ZNK6icu_678numparse4impl13SeriesMatcher11postProcessERNS1_12ParsedNumberE
	.quad	_ZNK6icu_678numparse4impl18ArraySeriesMatcher8toStringEv
	.quad	_ZNK6icu_678numparse4impl18ArraySeriesMatcher5beginEv
	.quad	_ZNK6icu_678numparse4impl18ArraySeriesMatcher3endEv
	.quad	_ZNK6icu_678numparse4impl18ArraySeriesMatcher6lengthEv
	.weak	_ZTVN6icu_678numparse4impl18NumberParseMatcherE
	.section	.data.rel.ro._ZTVN6icu_678numparse4impl18NumberParseMatcherE,"awG",@progbits,_ZTVN6icu_678numparse4impl18NumberParseMatcherE,comdat
	.align 8
	.type	_ZTVN6icu_678numparse4impl18NumberParseMatcherE, @object
	.size	_ZTVN6icu_678numparse4impl18NumberParseMatcherE, 72
_ZTVN6icu_678numparse4impl18NumberParseMatcherE:
	.quad	0
	.quad	_ZTIN6icu_678numparse4impl18NumberParseMatcherE
	.quad	0
	.quad	0
	.quad	_ZNK6icu_678numparse4impl18NumberParseMatcher10isFlexibleEv
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZNK6icu_678numparse4impl18NumberParseMatcher11postProcessERNS1_12ParsedNumberE
	.quad	__cxa_pure_virtual
	.weak	_ZTVN6icu_678numparse4impl16NumberParserImplE
	.section	.data.rel.ro.local._ZTVN6icu_678numparse4impl16NumberParserImplE,"awG",@progbits,_ZTVN6icu_678numparse4impl16NumberParserImplE,comdat
	.align 8
	.type	_ZTVN6icu_678numparse4impl16NumberParserImplE, @object
	.size	_ZTVN6icu_678numparse4impl16NumberParserImplE, 40
_ZTVN6icu_678numparse4impl16NumberParserImplE:
	.quad	0
	.quad	_ZTIN6icu_678numparse4impl16NumberParserImplE
	.quad	_ZN6icu_678numparse4impl16NumberParserImplD1Ev
	.quad	_ZN6icu_678numparse4impl16NumberParserImplD0Ev
	.quad	_ZN6icu_678numparse4impl16NumberParserImpl10addMatcherERNS1_18NumberParseMatcherE
	.section	.data.rel.ro,"aw"
	.align 8
.LC5:
	.quad	_ZTVN6icu_676number4impl17ParsedPatternInfoE+16
	.align 8
.LC6:
	.quad	_ZTVN6icu_676number4impl30PropertiesAffixPatternProviderE+16
	.align 8
.LC7:
	.quad	_ZTVN6icu_676number4impl31CurrencyPluralInfoAffixProviderE+16
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
