	.file	"nultrans.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6718NullTransliterator17getDynamicClassIDEv
	.type	_ZNK6icu_6718NullTransliterator17getDynamicClassIDEv, @function
_ZNK6icu_6718NullTransliterator17getDynamicClassIDEv:
.LFB2261:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6718NullTransliterator16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2261:
	.size	_ZNK6icu_6718NullTransliterator17getDynamicClassIDEv, .-_ZNK6icu_6718NullTransliterator17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6718NullTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositiona
	.type	_ZNK6icu_6718NullTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositiona, @function
_ZNK6icu_6718NullTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositiona:
.LFB2270:
	.cfi_startproc
	endbr64
	movl	12(%rdx), %eax
	movl	%eax, 8(%rdx)
	ret
	.cfi_endproc
.LFE2270:
	.size	_ZNK6icu_6718NullTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositiona, .-_ZNK6icu_6718NullTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositiona
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718NullTransliteratorD2Ev
	.type	_ZN6icu_6718NullTransliteratorD2Ev, @function
_ZN6icu_6718NullTransliteratorD2Ev:
.LFB2266:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6718NullTransliteratorE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6714TransliteratorD2Ev@PLT
	.cfi_endproc
.LFE2266:
	.size	_ZN6icu_6718NullTransliteratorD2Ev, .-_ZN6icu_6718NullTransliteratorD2Ev
	.globl	_ZN6icu_6718NullTransliteratorD1Ev
	.set	_ZN6icu_6718NullTransliteratorD1Ev,_ZN6icu_6718NullTransliteratorD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718NullTransliteratorD0Ev
	.type	_ZN6icu_6718NullTransliteratorD0Ev, @function
_ZN6icu_6718NullTransliteratorD0Ev:
.LFB2268:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6718NullTransliteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6714TransliteratorD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2268:
	.size	_ZN6icu_6718NullTransliteratorD0Ev, .-_ZN6icu_6718NullTransliteratorD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718NullTransliterator16getStaticClassIDEv
	.type	_ZN6icu_6718NullTransliterator16getStaticClassIDEv, @function
_ZN6icu_6718NullTransliterator16getStaticClassIDEv:
.LFB2260:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6718NullTransliterator16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2260:
	.size	_ZN6icu_6718NullTransliterator16getStaticClassIDEv, .-_ZN6icu_6718NullTransliterator16getStaticClassIDEv
	.section	.rodata.str2.2,"aMS",@progbits,2
	.align 2
.LC0:
	.string	"A"
	.string	"n"
	.string	"y"
	.string	"-"
	.string	"N"
	.string	"u"
	.string	"l"
	.string	"l"
	.string	""
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718NullTransliteratorC2Ev
	.type	_ZN6icu_6718NullTransliteratorC2Ev, @function
_ZN6icu_6718NullTransliteratorC2Ev:
.LFB2263:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$-1, %ecx
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	leaq	-96(%rbp), %r12
	movq	%rdi, %rbx
	leaq	-104(%rbp), %rdx
	movq	%r12, %rdi
	subq	$96, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	.LC0(%rip), %rax
	movq	%rax, -104(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6714TransliteratorC2ERKNS_13UnicodeStringEPNS_13UnicodeFilterE@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	16+_ZTVN6icu_6718NullTransliteratorE(%rip), %rax
	movq	%rax, (%rbx)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L11
	addq	$96, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L11:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2263:
	.size	_ZN6icu_6718NullTransliteratorC2Ev, .-_ZN6icu_6718NullTransliteratorC2Ev
	.globl	_ZN6icu_6718NullTransliteratorC1Ev
	.set	_ZN6icu_6718NullTransliteratorC1Ev,_ZN6icu_6718NullTransliteratorC2Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6718NullTransliterator5cloneEv
	.type	_ZNK6icu_6718NullTransliterator5cloneEv, @function
_ZNK6icu_6718NullTransliterator5cloneEv:
.LFB2269:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$88, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	subq	$96, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L12
	leaq	-96(%rbp), %r13
	leaq	.LC0(%rip), %rax
	movl	$-1, %ecx
	movl	$1, %esi
	leaq	-104(%rbp), %rdx
	movq	%r13, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6714TransliteratorC2ERKNS_13UnicodeStringEPNS_13UnicodeFilterE@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	16+_ZTVN6icu_6718NullTransliteratorE(%rip), %rax
	movq	%rax, (%r12)
.L12:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L19
	addq	$96, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L19:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2269:
	.size	_ZNK6icu_6718NullTransliterator5cloneEv, .-_ZNK6icu_6718NullTransliterator5cloneEv
	.weak	_ZTSN6icu_6718NullTransliteratorE
	.section	.rodata._ZTSN6icu_6718NullTransliteratorE,"aG",@progbits,_ZTSN6icu_6718NullTransliteratorE,comdat
	.align 16
	.type	_ZTSN6icu_6718NullTransliteratorE, @object
	.size	_ZTSN6icu_6718NullTransliteratorE, 30
_ZTSN6icu_6718NullTransliteratorE:
	.string	"N6icu_6718NullTransliteratorE"
	.weak	_ZTIN6icu_6718NullTransliteratorE
	.section	.data.rel.ro._ZTIN6icu_6718NullTransliteratorE,"awG",@progbits,_ZTIN6icu_6718NullTransliteratorE,comdat
	.align 8
	.type	_ZTIN6icu_6718NullTransliteratorE, @object
	.size	_ZTIN6icu_6718NullTransliteratorE, 24
_ZTIN6icu_6718NullTransliteratorE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6718NullTransliteratorE
	.quad	_ZTIN6icu_6714TransliteratorE
	.weak	_ZTVN6icu_6718NullTransliteratorE
	.section	.data.rel.ro._ZTVN6icu_6718NullTransliteratorE,"awG",@progbits,_ZTVN6icu_6718NullTransliteratorE,comdat
	.align 8
	.type	_ZTVN6icu_6718NullTransliteratorE, @object
	.size	_ZTVN6icu_6718NullTransliteratorE, 152
_ZTVN6icu_6718NullTransliteratorE:
	.quad	0
	.quad	_ZTIN6icu_6718NullTransliteratorE
	.quad	_ZN6icu_6718NullTransliteratorD1Ev
	.quad	_ZN6icu_6718NullTransliteratorD0Ev
	.quad	_ZNK6icu_6718NullTransliterator17getDynamicClassIDEv
	.quad	_ZNK6icu_6718NullTransliterator5cloneEv
	.quad	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableEii
	.quad	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableE
	.quad	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableER14UTransPositionRKNS_13UnicodeStringER10UErrorCode
	.quad	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableER14UTransPositioniR10UErrorCode
	.quad	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableER14UTransPositionR10UErrorCode
	.quad	_ZNK6icu_6714Transliterator21finishTransliterationERNS_11ReplaceableER14UTransPosition
	.quad	_ZNK6icu_6718NullTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositiona
	.quad	_ZNK6icu_6714Transliterator21filteredTransliterateERNS_11ReplaceableER14UTransPositiona
	.quad	_ZNK6icu_6714Transliterator21filteredTransliterateERNS_11ReplaceableER14UTransPositionaa
	.quad	_ZNK6icu_6714Transliterator5getIDEv
	.quad	_ZNK6icu_6714Transliterator7toRulesERNS_13UnicodeStringEa
	.quad	_ZNK6icu_6714Transliterator18handleGetSourceSetERNS_10UnicodeSetE
	.quad	_ZNK6icu_6714Transliterator12getTargetSetERNS_10UnicodeSetE
	.local	_ZZN6icu_6718NullTransliterator16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6718NullTransliterator16getStaticClassIDEvE7classID,1,1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
