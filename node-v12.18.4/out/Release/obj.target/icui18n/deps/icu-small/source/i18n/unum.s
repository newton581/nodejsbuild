	.file	"unum.cpp"
	.text
	.p2align 4
	.type	_ZL8parseResRN6icu_6711FormattableEPKPvPKDsiPiP10UErrorCode.part.0, @function
_ZL8parseResRN6icu_6711FormattableEPKPvPKDsiPiP10UErrorCode.part.0:
.LFB3440:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-144(%rbp), %r15
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-128(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	xorl	%esi, %esi
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$120, %rsp
	movq	%r9, -152(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$-1, %ecx
	movq	%rdx, -144(%rbp)
	movq	%r15, %rdx
	sete	%sil
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	leaq	16+_ZTVN6icu_6713ParsePositionE(%rip), %rax
	movq	%rax, -144(%rbp)
	movabsq	$-4294967296, %rax
	movq	%rax, -136(%rbp)
	testq	%rbx, %rbx
	je	.L2
	movl	(%rbx), %eax
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, -136(%rbp)
	movq	(%r12), %rax
	call	*160(%rax)
	movl	-132(%rbp), %eax
	cmpl	$-1, %eax
	je	.L3
	movq	-152(%rbp), %rdx
	movl	$9, (%rdx)
	movl	%eax, (%rbx)
.L4:
	movq	%r15, %rdi
	call	_ZN6icu_6713ParsePositionD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L10
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	.cfi_restore_state
	movl	-136(%rbp), %eax
	movl	%eax, (%rbx)
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L2:
	movq	(%r12), %rax
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*160(%rax)
	cmpl	$-1, -132(%rbp)
	je	.L4
	movq	-152(%rbp), %rax
	movl	$9, (%rax)
	jmp	.L4
.L10:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3440:
	.size	_ZL8parseResRN6icu_6711FormattableEPKPvPKDsiPiP10UErrorCode.part.0, .-_ZL8parseResRN6icu_6711FormattableEPKPvPKDsiPiP10UErrorCode.part.0
	.p2align 4
	.globl	unum_close_67
	.type	unum_close_67, @function
unum_close_67:
.LFB2549:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L11
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L11:
	ret
	.cfi_endproc
.LFE2549:
	.size	unum_close_67, .-unum_close_67
	.section	.text.unlikely,"ax",@progbits
.LCOLDB0:
	.text
.LHOTB0:
	.p2align 4
	.globl	unum_clone_67
	.type	unum_clone_67, @function
unum_clone_67:
.LFB2550:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L18
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	testq	%rdi, %rdi
	je	.L15
	movq	%rsi, %rbx
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6713DecimalFormatE(%rip), %rdx
	leaq	_ZTIN6icu_6712NumberFormatE(%rip), %rsi
	call	__dynamic_cast@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L16
.L23:
	movq	(%rdi), %rax
	call	*32(%rax)
	testq	%rax, %rax
	je	.L24
.L13:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6721RuleBasedNumberFormatE(%rip), %rdx
	leaq	_ZTIN6icu_6712NumberFormatE(%rip), %rsi
	call	__dynamic_cast@PLT
	movq	%rax, %rdi
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L18:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movl	$7, (%rbx)
	jmp	.L13
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	unum_clone_67.cold, @function
unum_clone_67.cold:
.LFSB2550:
.L15:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movq	0, %rax
	ud2
	.cfi_endproc
.LFE2550:
	.text
	.size	unum_clone_67, .-unum_clone_67
	.section	.text.unlikely
	.size	unum_clone_67.cold, .-unum_clone_67.cold
.LCOLDE0:
	.text
.LHOTE0:
	.p2align 4
	.globl	unum_format_67
	.type	unum_format_67, @function
unum_format_67:
.LFB2551:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r9), %r10d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r10d, %r10d
	jg	.L32
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rdi, %r15
	movq	%rdx, %r10
	movl	%ecx, %r14d
	movq	%rax, -128(%rbp)
	movl	$2, %eax
	movq	%r8, %rbx
	movq	%r9, %r12
	movw	%ax, -120(%rbp)
	movslq	%esi, %r11
	testq	%rdx, %rdx
	jne	.L33
	leaq	-128(%rbp), %r13
	testl	%ecx, %ecx
	jne	.L33
.L27:
	leaq	16+_ZTVN6icu_6713FieldPositionE(%rip), %rax
	movq	%r10, -192(%rbp)
	movq	%rax, -160(%rbp)
	movl	$4294967295, %eax
	movq	%rax, -152(%rbp)
	movq	(%r15), %rax
	movl	$0, -144(%rbp)
	movq	120(%rax), %rax
	testq	%rbx, %rbx
	je	.L29
	movl	(%rbx), %edx
	leaq	-160(%rbp), %r9
	movq	%r12, %r8
	movq	%r11, %rsi
	movq	%r9, -184(%rbp)
	movq	%r9, %rcx
	movq	%r15, %rdi
	movl	%edx, -152(%rbp)
	movq	%r13, %rdx
	call	*%rax
	movq	-148(%rbp), %rax
	movq	-192(%rbp), %r10
	movq	-184(%rbp), %r9
	movq	%rax, 4(%rbx)
.L30:
	movq	%r12, %rcx
	leaq	-168(%rbp), %rsi
	movl	%r14d, %edx
	movq	%r13, %rdi
	movq	%r9, -184(%rbp)
	movq	%r10, -168(%rbp)
	call	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode@PLT
	movl	%eax, %r12d
	movq	-184(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN6icu_6713FieldPositionD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L25:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L38
	addq	$152, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	.cfi_restore_state
	leaq	-128(%rbp), %r13
	movq	%r10, %rsi
	movl	%r14d, %ecx
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%r11, -192(%rbp)
	movq	%r10, -184(%rbp)
	call	_ZN6icu_6713UnicodeString5setToEPDsii@PLT
	movq	-192(%rbp), %r11
	movq	-184(%rbp), %r10
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L29:
	leaq	-160(%rbp), %r9
	movq	%r12, %r8
	movq	%r13, %rdx
	movq	%r11, %rsi
	movq	%r9, -184(%rbp)
	movq	%r9, %rcx
	movq	%r15, %rdi
	call	*%rax
	movq	-184(%rbp), %r9
	movq	-192(%rbp), %r10
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L32:
	movl	$-1, %r12d
	jmp	.L25
.L38:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2551:
	.size	unum_format_67, .-unum_format_67
	.p2align 4
	.globl	unum_formatInt64_67
	.type	unum_formatInt64_67, @function
unum_formatInt64_67:
.LFB2552:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r9), %r10d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r10d, %r10d
	jg	.L46
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rdi, %r15
	movq	%rsi, %r10
	movq	%rdx, %r11
	movq	%rax, -128(%rbp)
	movl	$2, %eax
	movl	%ecx, %r14d
	movq	%r8, %rbx
	movw	%ax, -120(%rbp)
	movq	%r9, %r12
	testq	%rdx, %rdx
	jne	.L47
	leaq	-128(%rbp), %r13
	testl	%ecx, %ecx
	jne	.L47
.L41:
	leaq	16+_ZTVN6icu_6713FieldPositionE(%rip), %rax
	movq	%r11, -192(%rbp)
	movq	%rax, -160(%rbp)
	movl	$4294967295, %eax
	movq	%rax, -152(%rbp)
	movq	(%r15), %rax
	movl	$0, -144(%rbp)
	movq	120(%rax), %rax
	testq	%rbx, %rbx
	je	.L43
	movl	(%rbx), %edx
	leaq	-160(%rbp), %r9
	movq	%r12, %r8
	movq	%r10, %rsi
	movq	%r9, -184(%rbp)
	movq	%r9, %rcx
	movq	%r15, %rdi
	movl	%edx, -152(%rbp)
	movq	%r13, %rdx
	call	*%rax
	movq	-148(%rbp), %rax
	movq	-192(%rbp), %r11
	movq	-184(%rbp), %r9
	movq	%rax, 4(%rbx)
.L44:
	movq	%r12, %rcx
	leaq	-168(%rbp), %rsi
	movl	%r14d, %edx
	movq	%r13, %rdi
	movq	%r9, -184(%rbp)
	movq	%r11, -168(%rbp)
	call	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode@PLT
	movl	%eax, %r12d
	movq	-184(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN6icu_6713FieldPositionD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L39:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L52
	addq	$152, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	.cfi_restore_state
	leaq	-128(%rbp), %r13
	movq	%r11, %rsi
	movl	%r14d, %ecx
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%r10, -192(%rbp)
	movq	%r11, -184(%rbp)
	call	_ZN6icu_6713UnicodeString5setToEPDsii@PLT
	movq	-192(%rbp), %r10
	movq	-184(%rbp), %r11
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L43:
	leaq	-160(%rbp), %r9
	movq	%r12, %r8
	movq	%r13, %rdx
	movq	%r10, %rsi
	movq	%r9, -184(%rbp)
	movq	%r9, %rcx
	movq	%r15, %rdi
	call	*%rax
	movq	-184(%rbp), %r9
	movq	-192(%rbp), %r11
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L46:
	movl	$-1, %r12d
	jmp	.L39
.L52:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2552:
	.size	unum_formatInt64_67, .-unum_formatInt64_67
	.p2align 4
	.globl	unum_formatDouble_67
	.type	unum_formatDouble_67, @function
unum_formatDouble_67:
.LFB2553:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r8), %r9d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r9d, %r9d
	jg	.L60
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rdi, %r15
	movq	%rsi, %r10
	movl	%edx, %r14d
	movq	%rax, -128(%rbp)
	movl	$2, %eax
	movq	%rcx, %rbx
	movq	%r8, %r12
	movw	%ax, -120(%rbp)
	testq	%rsi, %rsi
	jne	.L61
	leaq	-128(%rbp), %r13
	testl	%edx, %edx
	jne	.L61
.L55:
	leaq	16+_ZTVN6icu_6713FieldPositionE(%rip), %rax
	movq	%r10, -192(%rbp)
	movq	%rax, -160(%rbp)
	movl	$4294967295, %eax
	movq	%rax, -152(%rbp)
	movq	(%r15), %rax
	movl	$0, -144(%rbp)
	movq	72(%rax), %rax
	testq	%rbx, %rbx
	je	.L57
	movl	(%rbx), %edx
	leaq	-160(%rbp), %r9
	movq	%r12, %rcx
	movq	%r13, %rsi
	movq	%r9, -184(%rbp)
	movq	%r15, %rdi
	movl	%edx, -152(%rbp)
	movq	%r9, %rdx
	call	*%rax
	movq	-148(%rbp), %rax
	movq	-192(%rbp), %r10
	movq	-184(%rbp), %r9
	movq	%rax, 4(%rbx)
.L58:
	movq	%r12, %rcx
	leaq	-168(%rbp), %rsi
	movl	%r14d, %edx
	movq	%r13, %rdi
	movq	%r9, -184(%rbp)
	movq	%r10, -168(%rbp)
	call	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode@PLT
	movl	%eax, %r12d
	movq	-184(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN6icu_6713FieldPositionD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L53:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L66
	addq	$152, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L61:
	.cfi_restore_state
	leaq	-128(%rbp), %r13
	movq	%r10, %rsi
	movl	%r14d, %ecx
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%r10, -184(%rbp)
	movsd	%xmm0, -192(%rbp)
	call	_ZN6icu_6713UnicodeString5setToEPDsii@PLT
	movsd	-192(%rbp), %xmm0
	movq	-184(%rbp), %r10
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L57:
	leaq	-160(%rbp), %r9
	movq	%r12, %rcx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%r9, -184(%rbp)
	movq	%r9, %rdx
	call	*%rax
	movq	-184(%rbp), %r9
	movq	-192(%rbp), %r10
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L60:
	movl	$-1, %r12d
	jmp	.L53
.L66:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2553:
	.size	unum_formatDouble_67, .-unum_formatDouble_67
	.p2align 4
	.globl	unum_formatDoubleForFields_67
	.type	unum_formatDoubleForFields_67, @function
unum_formatDoubleForFields_67:
.LFB2554:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r8), %r9d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r9d, %r9d
	jg	.L75
	movq	%rdi, %r14
	movq	%rsi, %rbx
	movl	%edx, %r13d
	movq	%rcx, %r9
	movq	%r8, %r12
	testq	%rsi, %rsi
	je	.L78
	testl	%edx, %edx
	js	.L70
	movl	$2, %edx
	leaq	-128(%rbp), %r15
	movq	%rcx, -160(%rbp)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%dx, -120(%rbp)
	movl	%r13d, %ecx
	xorl	%edx, %edx
	movq	%r15, %rdi
	movsd	%xmm0, -152(%rbp)
	movq	%rax, -128(%rbp)
	call	_ZN6icu_6713UnicodeString5setToEPDsii@PLT
	movsd	-152(%rbp), %xmm0
	movq	-160(%rbp), %r9
.L73:
	movq	(%r14), %rax
	movq	%r9, %rdx
	movq	%r12, %rcx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	*80(%rax)
	movq	%r12, %rcx
	leaq	-136(%rbp), %rsi
	movl	%r13d, %edx
	movq	%r15, %rdi
	movq	%rbx, -136(%rbp)
	call	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode@PLT
	movl	%eax, %r12d
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L67:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L79
	addq	$120, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L78:
	.cfi_restore_state
	testl	%edx, %edx
	jne	.L70
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	leaq	-128(%rbp), %r15
	movq	%rax, -128(%rbp)
	movl	$2, %eax
	movw	%ax, -120(%rbp)
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L75:
	movl	$-1, %r12d
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L70:
	movl	$1, (%r12)
	movl	$-1, %r12d
	jmp	.L67
.L79:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2554:
	.size	unum_formatDoubleForFields_67, .-unum_formatDoubleForFields_67
	.p2align 4
	.globl	unum_parseDoubleCurrency_67
	.type	unum_parseDoubleCurrency_67, @function
unum_parseDoubleCurrency_67:
.LFB2562:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r9), %r10d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	xorl	%eax, %eax
	movw	%ax, (%r8)
	testl	%r10d, %r10d
	jle	.L96
.L80:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L97
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L96:
	.cfi_restore_state
	movq	%rsi, -144(%rbp)
	leaq	-128(%rbp), %r15
	xorl	%esi, %esi
	cmpl	$-1, %edx
	leaq	-144(%rbp), %r13
	movq	%rdi, %r14
	movq	%rcx, %rbx
	movq	%r15, %rdi
	movl	%edx, %ecx
	sete	%sil
	movq	%r13, %rdx
	movq	%r8, -152(%rbp)
	movq	%r9, %r12
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	leaq	16+_ZTVN6icu_6713ParsePositionE(%rip), %rax
	testq	%rbx, %rbx
	movq	-152(%rbp), %r8
	movq	%rax, -144(%rbp)
	movabsq	$-4294967296, %rax
	movq	%rax, -136(%rbp)
	je	.L82
	movl	(%rbx), %eax
	movq	%r14, %rdi
	movl	$9, (%r12)
	movq	%r13, %rdx
	movq	%r15, %rsi
	movl	%eax, -136(%rbp)
	movq	(%r14), %rax
	call	*176(%rax)
	movq	-152(%rbp), %r8
	movq	%rax, %r14
	movl	-132(%rbp), %eax
	cmpl	$-1, %eax
	je	.L83
	movl	%eax, (%rbx)
.L84:
	pxor	%xmm0, %xmm0
	testq	%r14, %r14
	je	.L86
.L85:
	movq	(%r14), %rax
	movsd	%xmm0, -152(%rbp)
	movq	%r14, %rdi
	call	*8(%rax)
	movsd	-152(%rbp), %xmm0
.L86:
	movq	%r13, %rdi
	movsd	%xmm0, -152(%rbp)
	call	_ZN6icu_6713ParsePositionD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movsd	-152(%rbp), %xmm0
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L83:
	movl	-136(%rbp), %eax
	movl	%eax, (%rbx)
.L88:
	testl	%eax, %eax
	jle	.L84
	movq	120(%r14), %rax
	movl	$0, (%r12)
	movq	%r8, %rdi
	leaq	20(%rax), %rsi
	call	u_strcpy_67@PLT
	leaq	8(%r14), %rdi
	movq	%r12, %rsi
	call	_ZNK6icu_6711Formattable9getDoubleER10UErrorCode@PLT
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L82:
	movq	(%r14), %rax
	movq	%r8, -152(%rbp)
	movq	%r14, %rdi
	movq	%r13, %rdx
	movl	$9, (%r12)
	movq	%r15, %rsi
	call	*176(%rax)
	cmpl	$-1, -132(%rbp)
	movq	-152(%rbp), %r8
	movq	%rax, %r14
	jne	.L84
	movl	-136(%rbp), %eax
	jmp	.L88
.L97:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2562:
	.size	unum_parseDoubleCurrency_67, .-unum_parseDoubleCurrency_67
	.p2align 4
	.globl	unum_getAvailable_67
	.type	unum_getAvailable_67, @function
unum_getAvailable_67:
.LFB2565:
	.cfi_startproc
	endbr64
	jmp	uloc_getAvailable_67@PLT
	.cfi_endproc
.LFE2565:
	.size	unum_getAvailable_67, .-unum_getAvailable_67
	.p2align 4
	.globl	unum_countAvailable_67
	.type	unum_countAvailable_67, @function
unum_countAvailable_67:
.LFB2566:
	.cfi_startproc
	endbr64
	jmp	uloc_countAvailable_67@PLT
	.cfi_endproc
.LFE2566:
	.size	unum_countAvailable_67, .-unum_countAvailable_67
	.p2align 4
	.globl	unum_getAttribute_67
	.type	unum_getAttribute_67, @function
unum_getAttribute_67:
.LFB2567:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpl	$19, %esi
	je	.L112
	movl	%esi, %r12d
	cmpl	$3, %esi
	je	.L113
	leal	-4(%rsi), %eax
	cmpl	$1, %eax
	jbe	.L114
	cmpl	$6, %esi
	je	.L115
	leal	-7(%rsi), %eax
	cmpl	$1, %eax
	jbe	.L116
	cmpl	$11, %esi
	je	.L117
	testq	%rdi, %rdi
	je	.L110
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6713DecimalFormatE(%rip), %rdx
	leaq	_ZTIN6icu_6712NumberFormatE(%rip), %rsi
	call	__dynamic_cast@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L110
	movq	(%rax), %rax
	movl	$0, -28(%rbp)
	leaq	-28(%rbp), %rdx
	movl	%r12d, %esi
	call	*304(%rax)
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L112:
	movq	(%rdi), %rax
	call	*200(%rax)
	movsbl	%al, %eax
.L100:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L118
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L114:
	.cfi_restore_state
	call	_ZNK6icu_6712NumberFormat23getMinimumIntegerDigitsEv@PLT
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L117:
	movq	(%rdi), %rax
	call	*272(%rax)
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L113:
	call	_ZNK6icu_6712NumberFormat23getMaximumIntegerDigitsEv@PLT
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L116:
	call	_ZNK6icu_6712NumberFormat24getMinimumFractionDigitsEv@PLT
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L115:
	call	_ZNK6icu_6712NumberFormat24getMaximumFractionDigitsEv@PLT
	jmp	.L100
.L110:
	movl	$-1, %eax
	jmp	.L100
.L118:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2567:
	.size	unum_getAttribute_67, .-unum_getAttribute_67
	.p2align 4
	.globl	unum_setAttribute_67
	.type	unum_setAttribute_67, @function
unum_setAttribute_67:
.LFB2568:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edx, %r13d
	pushq	%r12
	subq	$32, %rsp
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpl	$19, %esi
	je	.L139
	movl	%esi, %r12d
	cmpl	$3, %esi
	je	.L137
	cmpl	$4, %esi
	je	.L140
	cmpl	$5, %esi
	je	.L141
	cmpl	$6, %esi
	je	.L138
	cmpl	$7, %esi
	je	.L142
	cmpl	$8, %esi
	je	.L143
	cmpl	$11, %esi
	je	.L144
	testq	%rdi, %rdi
	je	.L119
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6713DecimalFormatE(%rip), %rdx
	leaq	_ZTIN6icu_6712NumberFormatE(%rip), %rsi
	call	__dynamic_cast@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L119
	movq	(%rax), %rax
	movl	$0, -28(%rbp)
	movl	%r13d, %edx
	movl	%r12d, %esi
	leaq	-28(%rbp), %rcx
	call	*296(%rax)
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L141:
	movq	(%rdi), %rax
	movq	%rdi, -40(%rbp)
	movl	%edx, %esi
	call	*224(%rax)
	movq	-40(%rbp), %rdi
.L137:
	movq	(%rdi), %rax
	movl	%r13d, %esi
	call	*216(%rax)
.L119:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L145
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L139:
	.cfi_restore_state
	movq	(%rdi), %rax
	xorl	%esi, %esi
	testl	%edx, %edx
	setne	%sil
	call	*192(%rax)
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L140:
	movq	(%rdi), %rax
	movl	%edx, %esi
	call	*224(%rax)
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L143:
	movq	(%rdi), %rax
	movq	%rdi, -40(%rbp)
	movl	%edx, %esi
	call	*240(%rax)
	movq	-40(%rbp), %rdi
.L138:
	movq	(%rdi), %rax
	movl	%r13d, %esi
	call	*232(%rax)
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L142:
	movq	(%rdi), %rax
	movl	%edx, %esi
	call	*240(%rax)
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L144:
	movq	(%rdi), %rax
	movl	%edx, %esi
	call	*280(%rax)
	jmp	.L119
.L145:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2568:
	.size	unum_setAttribute_67, .-unum_setAttribute_67
	.p2align 4
	.globl	unum_getDoubleAttribute_67
	.type	unum_getDoubleAttribute_67, @function
unum_getDoubleAttribute_67:
.LFB2569:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L148
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6713DecimalFormatE(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movl	%esi, %ebx
	leaq	_ZTIN6icu_6712NumberFormatE(%rip), %rsi
	subq	$8, %rsp
	call	__dynamic_cast@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L149
	cmpl	$12, %ebx
	jne	.L149
	movq	(%rax), %rax
	movq	400(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L148:
	movsd	.LC2(%rip), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L149:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movsd	.LC2(%rip), %xmm0
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2569:
	.size	unum_getDoubleAttribute_67, .-unum_getDoubleAttribute_67
	.p2align 4
	.globl	unum_setDoubleAttribute_67
	.type	unum_setDoubleAttribute_67, @function
unum_setDoubleAttribute_67:
.LFB2570:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movsd	%xmm0, -24(%rbp)
	testq	%rdi, %rdi
	je	.L160
	movl	%esi, %ebx
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6713DecimalFormatE(%rip), %rdx
	leaq	_ZTIN6icu_6712NumberFormatE(%rip), %rsi
	call	__dynamic_cast@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L160
	cmpl	$12, %ebx
	jne	.L160
	movq	(%rax), %rax
	movsd	-24(%rbp), %xmm0
	movq	408(%rax), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L160:
	.cfi_restore_state
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2570:
	.size	unum_setDoubleAttribute_67, .-unum_setDoubleAttribute_67
	.p2align 4
	.globl	unum_getTextAttribute_67
	.type	unum_getTextAttribute_67, @function
unum_getTextAttribute_67:
.LFB2571:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$200, %rsp
	movq	%r8, -232(%rbp)
	movl	(%r8), %r8d
	movq	%rdx, -216(%rbp)
	movl	%ecx, -220(%rbp)
	movq	%fs:40, %rsi
	movq	%rsi, -56(%rbp)
	xorl	%esi, %esi
	testl	%r8d, %r8d
	jg	.L195
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %esi
	movq	%rdi, %r14
	movq	%rax, -192(%rbp)
	movw	%si, -184(%rbp)
	testq	%rdx, %rdx
	jne	.L196
	leaq	-192(%rbp), %r12
	testl	%ecx, %ecx
	jne	.L196
.L174:
	testq	%r14, %r14
	je	.L176
	movq	%r14, %rdi
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6713DecimalFormatE(%rip), %rdx
	leaq	_ZTIN6icu_6712NumberFormatE(%rip), %rsi
	call	__dynamic_cast@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L177
	cmpl	$5, %ebx
	ja	.L189
	leaq	.L180(%rip), %rcx
	movslq	(%rcx,%rbx,4), %rdx
	addq	%rcx, %rdx
	notrack jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L180:
	.long	.L185-.L180
	.long	.L184-.L180
	.long	.L183-.L180
	.long	.L182-.L180
	.long	.L181-.L180
	.long	.L179-.L180
	.text
	.p2align 4,,10
	.p2align 3
.L177:
	movq	%r14, %rdi
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6721RuleBasedNumberFormatE(%rip), %rdx
	leaq	_ZTIN6icu_6712NumberFormatE(%rip), %rsi
	call	__dynamic_cast@PLT
	movq	%rax, %r14
.L176:
	cmpl	$6, %ebx
	je	.L210
	cmpl	$7, %ebx
	jne	.L189
	movq	(%r14), %rax
	movq	%r14, %rdi
	xorl	%r13d, %r13d
	leaq	-128(%rbp), %rbx
	leaq	-200(%rbp), %r15
	call	*304(%rax)
	movl	%eax, -224(%rbp)
	testl	%eax, %eax
	jg	.L193
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L211:
	sarl	$5, %ecx
.L208:
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	addl	$1, %r13d
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	$59, %eax
	xorl	%edx, %edx
	movq	%r15, %rsi
	movl	$1, %ecx
	movq	%r12, %rdi
	movw	%ax, -200(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	cmpl	%r13d, -224(%rbp)
	je	.L186
.L193:
	movq	(%r14), %rax
	movl	%r13d, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	*312(%rax)
	movswl	-120(%rbp), %ecx
	testw	%cx, %cx
	jns	.L211
	movl	-116(%rbp), %ecx
	jmp	.L208
.L189:
	movq	-232(%rbp), %rax
	movl	$-1, %r13d
	movl	$16, (%rax)
.L187:
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L172:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L212
	addq	$200, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L196:
	.cfi_restore_state
	leaq	-192(%rbp), %r12
	movl	-220(%rbp), %ecx
	movq	-216(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString5setToEPDsii@PLT
	jmp	.L174
.L181:
	movq	(%rax), %rax
	leaq	-128(%rbp), %r15
	movq	%rdi, %rsi
	movq	%r15, %rdi
	call	*432(%rax)
.L206:
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L209:
	leaq	-200(%rbp), %r15
.L186:
	movq	-216(%rbp), %rax
	movq	-232(%rbp), %rcx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	-220(%rbp), %edx
	movq	%rax, -200(%rbp)
	call	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode@PLT
	movl	%eax, %r13d
	jmp	.L187
.L179:
	call	_ZNK6icu_6712NumberFormat11getCurrencyEv@PLT
	leaq	-128(%rbp), %r15
	movq	%rax, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	jmp	.L206
.L185:
	movq	%r12, %rsi
	leaq	-200(%rbp), %r15
	call	_ZNK6icu_6713DecimalFormat17getPositivePrefixERNS_13UnicodeStringE@PLT
	jmp	.L186
.L184:
	movq	%r12, %rsi
	leaq	-200(%rbp), %r15
	call	_ZNK6icu_6713DecimalFormat17getPositiveSuffixERNS_13UnicodeStringE@PLT
	jmp	.L186
.L183:
	movq	%r12, %rsi
	leaq	-200(%rbp), %r15
	call	_ZNK6icu_6713DecimalFormat17getNegativePrefixERNS_13UnicodeStringE@PLT
	jmp	.L186
.L182:
	movq	%r12, %rsi
	leaq	-200(%rbp), %r15
	call	_ZNK6icu_6713DecimalFormat17getNegativeSuffixERNS_13UnicodeStringE@PLT
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L210:
	movq	(%r14), %rax
	leaq	-128(%rbp), %r15
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	*384(%rax)
	jmp	.L206
.L195:
	movl	$-1, %r13d
	jmp	.L172
.L212:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2571:
	.size	unum_getTextAttribute_67, .-unum_getTextAttribute_67
	.p2align 4
	.globl	unum_setTextAttribute_67
	.type	unum_setTextAttribute_67, @function
unum_setTextAttribute_67:
.LFB2572:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%r8), %eax
	testl	%eax, %eax
	jle	.L233
.L213:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L234
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L233:
	.cfi_restore_state
	leaq	-128(%rbp), %r14
	movq	%rdi, %r12
	movl	%esi, %ebx
	movq	%rdx, %rsi
	movq	%r14, %rdi
	movl	%ecx, %edx
	movq	%r8, %r13
	call	_ZN6icu_6713UnicodeStringC1EPKDsi@PLT
	testq	%r12, %r12
	je	.L216
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6713DecimalFormatE(%rip), %rdx
	leaq	_ZTIN6icu_6712NumberFormatE(%rip), %rsi
	movq	%r12, %rdi
	call	__dynamic_cast@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L217
	cmpl	$5, %ebx
	ja	.L227
	leaq	.L220(%rip), %rdx
	movslq	(%rdx,%rbx,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L220:
	.long	.L225-.L220
	.long	.L224-.L220
	.long	.L223-.L220
	.long	.L222-.L220
	.long	.L221-.L220
	.long	.L219-.L220
	.text
	.p2align 4,,10
	.p2align 3
.L217:
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6721RuleBasedNumberFormatE(%rip), %rdx
	leaq	_ZTIN6icu_6712NumberFormatE(%rip), %rsi
	call	__dynamic_cast@PLT
	movq	%rax, %r12
.L216:
	cmpl	$6, %ebx
	je	.L235
.L227:
	movl	$16, 0(%r13)
.L226:
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L235:
	movq	(%r12), %rax
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	*376(%rax)
	jmp	.L226
.L221:
	movq	(%r15), %rax
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	*440(%rax)
	jmp	.L226
.L219:
	movq	(%r15), %rax
	movq	%r14, %rdi
	movq	248(%rax), %rbx
	call	_ZN6icu_6713UnicodeString19getTerminatedBufferEv@PLT
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	*%rbx
	jmp	.L226
.L225:
	movq	(%r15), %rax
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	*360(%rax)
	jmp	.L226
.L224:
	movq	(%r15), %rax
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	*376(%rax)
	jmp	.L226
.L223:
	movq	(%r15), %rax
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	*368(%rax)
	jmp	.L226
.L222:
	movq	(%r15), %rax
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	*384(%rax)
	jmp	.L226
.L234:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2572:
	.size	unum_setTextAttribute_67, .-unum_setTextAttribute_67
	.section	.text.unlikely
.LCOLDB3:
	.text
.LHOTB3:
	.p2align 4
	.globl	unum_toPattern_67
	.type	unum_toPattern_67, @function
unum_toPattern_67:
.LFB2573:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r8), %r9d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r9d, %r9d
	jg	.L245
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rdi, %r13
	movl	%esi, %r15d
	movq	%rdx, %rbx
	movq	%rax, -192(%rbp)
	movl	$2, %eax
	movl	%ecx, %r12d
	movq	%r8, %r14
	movw	%ax, -184(%rbp)
	testq	%rdx, %rdx
	jne	.L246
	testl	%ecx, %ecx
	jne	.L246
.L238:
	testq	%r13, %r13
	je	.L240
	movq	%r13, %rdi
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6713DecimalFormatE(%rip), %rdx
	leaq	_ZTIN6icu_6712NumberFormatE(%rip), %rsi
	call	__dynamic_cast@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L241
	leaq	-192(%rbp), %r13
	movq	(%rax), %rax
	movq	%r13, %rsi
	testb	%r15b, %r15b
	je	.L242
	call	*552(%rax)
.L243:
	movl	%r12d, %edx
	leaq	-200(%rbp), %rsi
	movq	%r14, %rcx
	movq	%r13, %rdi
	movq	%rbx, -200(%rbp)
	call	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode@PLT
	movl	%eax, %r12d
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L236:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L251
	addq	$168, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L241:
	.cfi_restore_state
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6721RuleBasedNumberFormatE(%rip), %rdx
	leaq	-128(%rbp), %r15
	movq	%r13, %rdi
	leaq	_ZTIN6icu_6712NumberFormatE(%rip), %rsi
	leaq	-192(%rbp), %r13
	call	__dynamic_cast@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	movq	(%rax), %rax
	call	*296(%rax)
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L246:
	leaq	-192(%rbp), %rdi
	movl	%r12d, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rsi
	call	_ZN6icu_6713UnicodeString5setToEPDsii@PLT
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L242:
	call	*544(%rax)
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L245:
	movl	$-1, %r12d
	jmp	.L236
.L251:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	unum_toPattern_67.cold, @function
unum_toPattern_67.cold:
.LFSB2573:
.L240:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	0, %rax
	ud2
	.cfi_endproc
.LFE2573:
	.text
	.size	unum_toPattern_67, .-unum_toPattern_67
	.section	.text.unlikely
	.size	unum_toPattern_67.cold, .-unum_toPattern_67.cold
.LCOLDE3:
	.text
.LHOTE3:
	.p2align 4
	.globl	unum_getSymbol_67
	.type	unum_getSymbol_67, @function
unum_getSymbol_67:
.LFB2574:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r8, %r8
	je	.L258
	movl	(%r8), %eax
	movq	%r8, %r12
	xorl	%r13d, %r13d
	testl	%eax, %eax
	jg	.L252
	testq	%rdi, %rdi
	je	.L260
	movl	%esi, %ebx
	cmpl	$27, %esi
	ja	.L260
	movq	%rdx, %r15
	movl	%ecx, %r14d
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6713DecimalFormatE(%rip), %rdx
	leaq	_ZTIN6icu_6712NumberFormatE(%rip), %rsi
	call	__dynamic_cast@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L262
	movq	(%rax), %rax
	call	*312(%rax)
	movl	%ebx, %esi
	leaq	-64(%rbp), %r8
	movq	%r12, %rcx
	salq	$6, %rsi
	movl	%r14d, %edx
	movq	%r15, -64(%rbp)
	leaq	8(%rsi,%rax), %rdi
	movq	%r8, %rsi
	call	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode@PLT
	movl	%eax, %r13d
	.p2align 4,,10
	.p2align 3
.L252:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L263
	addq	$24, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L260:
	.cfi_restore_state
	movl	$1, (%r12)
	xorl	%r13d, %r13d
	jmp	.L252
	.p2align 4,,10
	.p2align 3
.L258:
	xorl	%r13d, %r13d
	jmp	.L252
	.p2align 4,,10
	.p2align 3
.L262:
	movl	$16, (%r12)
	jmp	.L252
.L263:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2574:
	.size	unum_getSymbol_67, .-unum_getSymbol_67
	.p2align 4
	.globl	unum_applyPattern_67
	.type	unum_applyPattern_67, @function
unum_applyPattern_67:
.LFB2576:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r9, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	movq	%rdx, %rsi
	movl	%ecx, %edx
	subq	$184, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-128(%rbp), %rax
	testq	%r8, %r8
	movl	$0, -196(%rbp)
	cmove	%rax, %r13
	leaq	-196(%rbp), %rax
	testq	%r9, %r9
	cmove	%rax, %r12
	cmpl	$-1, %ecx
	je	.L281
.L267:
	leaq	-192(%rbp), %r15
	movl	%edx, %ecx
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPDsii@PLT
	testq	%r14, %r14
	je	.L268
	movq	%r14, %rdi
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6713DecimalFormatE(%rip), %rdx
	leaq	_ZTIN6icu_6712NumberFormatE(%rip), %rsi
	call	__dynamic_cast@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L268
	movq	(%rax), %rax
	movq	%r12, %rcx
	movq	%r13, %rdx
	movq	%r15, %rsi
	testb	%bl, %bl
	jne	.L282
	call	*560(%rax)
.L270:
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L264:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L283
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L282:
	.cfi_restore_state
	call	*576(%rax)
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L268:
	movl	$16, (%r12)
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L281:
	movq	%rsi, %rdi
	movq	%rsi, -216(%rbp)
	call	u_strlen_67@PLT
	movq	-216(%rbp), %rsi
	movl	%eax, %edx
	jmp	.L267
.L283:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2576:
	.size	unum_applyPattern_67, .-unum_applyPattern_67
	.p2align 4
	.globl	unum_getLocaleByType_67
	.type	unum_getLocaleByType_67, @function
unum_getLocaleByType_67:
.LFB2577:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L288
	jmp	_ZNK6icu_676Format11getLocaleIDE18ULocDataLocaleTypeR10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L288:
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L284
	movl	$1, (%rdx)
.L284:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2577:
	.size	unum_getLocaleByType_67, .-unum_getLocaleByType_67
	.p2align 4
	.globl	unum_setContext_67
	.type	unum_setContext_67, @function
unum_setContext_67:
.LFB2578:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L289
	movq	(%rdi), %rax
	jmp	*256(%rax)
	.p2align 4,,10
	.p2align 3
.L289:
	ret
	.cfi_endproc
.LFE2578:
	.size	unum_setContext_67, .-unum_setContext_67
	.p2align 4
	.globl	unum_getContext_67
	.type	unum_getContext_67, @function
unum_getContext_67:
.LFB2579:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L292
	movq	(%rdi), %rax
	jmp	*264(%rax)
	.p2align 4,,10
	.p2align 3
.L292:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2579:
	.size	unum_getContext_67, .-unum_getContext_67
	.p2align 4
	.globl	unum_parseToUFormattable_67
	.type	unum_parseToUFormattable_67, @function
unum_parseToUFormattable_67:
.LFB2580:
	.cfi_startproc
	endbr64
	movq	%rsi, %rax
	movl	(%r9), %esi
	testl	%esi, %esi
	jg	.L311
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$40, %rsp
	testq	%rdi, %rdi
	je	.L295
	testq	%rdx, %rdx
	sete	%dil
	testl	%ecx, %ecx
	setne	%sil
	andb	%sil, %dil
	movl	%edi, %r13d
	jne	.L295
	xorl	%r14d, %r14d
	testq	%rax, %rax
	je	.L314
.L297:
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%r9, -48(%rbp)
	movq	%rax, -40(%rbp)
	call	_ZL8parseResRN6icu_6711FormattableEPKPvPKDsiPiP10UErrorCode.part.0
	movq	-48(%rbp), %r9
	movq	-40(%rbp), %rax
	movl	(%r9), %esi
.L299:
	testl	%esi, %esi
	jle	.L294
	testb	%r13b, %r13b
	je	.L294
	movq	%r14, %rdi
	call	ufmt_close_67@PLT
	xorl	%eax, %eax
	jmp	.L294
	.p2align 4,,10
	.p2align 3
.L295:
	movl	$1, (%r9)
.L294:
	addq	$40, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L311:
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	ret
	.p2align 4,,10
	.p2align 3
.L314:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movq	%r9, %rdi
	movq	%r8, -64(%rbp)
	movl	%ecx, -52(%rbp)
	movq	%rdx, -48(%rbp)
	movq	%r9, -40(%rbp)
	call	ufmt_open_67@PLT
	movq	-40(%rbp), %r9
	movq	-48(%rbp), %rdx
	testq	%rax, %rax
	movl	-52(%rbp), %ecx
	movq	-64(%rbp), %r8
	movq	%rax, %r14
	movl	(%r9), %esi
	setne	%r13b
	testl	%esi, %esi
	jg	.L299
	jmp	.L297
	.cfi_endproc
.LFE2580:
	.size	unum_parseToUFormattable_67, .-unum_parseToUFormattable_67
	.p2align 4
	.globl	unum_formatUFormattable_67
	.type	unum_formatUFormattable_67, @function
unum_formatUFormattable_67:
.LFB2581:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r9), %r10d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r10d, %r10d
	jg	.L315
	movq	%rdi, %r13
	movq	%r9, %r12
	testq	%rdi, %rdi
	je	.L317
	movq	%rsi, %r15
	testq	%rsi, %rsi
	je	.L317
	movq	%rdx, %rbx
	movl	%ecx, %r14d
	movq	%r8, %r10
	testq	%rdx, %rdx
	je	.L325
	testl	%ecx, %ecx
	js	.L317
.L319:
	leaq	-128(%rbp), %r9
	xorl	%edx, %edx
	movl	%r14d, %ecx
	movq	%rbx, %rsi
	movq	%r9, %rdi
	movq	%r10, -192(%rbp)
	movq	%r9, -184(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EPDsii@PLT
	movq	-192(%rbp), %r10
	leaq	16+_ZTVN6icu_6713FieldPositionE(%rip), %rax
	movl	$0, -144(%rbp)
	movq	%rax, -160(%rbp)
	movl	$4294967295, %eax
	movq	-184(%rbp), %r9
	testq	%r10, %r10
	movq	%rax, -152(%rbp)
	je	.L320
	movl	(%r10), %eax
	leaq	-160(%rbp), %r11
	movq	%r9, %rdx
	movq	%r12, %r8
	movq	%r10, -200(%rbp)
	movq	%r11, %rcx
	movq	%r15, %rsi
	movq	%r13, %rdi
	movl	%eax, -152(%rbp)
	movq	0(%r13), %rax
	movq	%r11, -192(%rbp)
	call	*40(%rax)
	movq	-148(%rbp), %rax
	movq	-200(%rbp), %r10
	movq	-192(%rbp), %r11
	movq	-184(%rbp), %r9
	movq	%rax, 4(%r10)
.L321:
	leaq	-168(%rbp), %rsi
	movq	%r12, %rcx
	movl	%r14d, %edx
	movq	%r9, %rdi
	movq	%r11, -200(%rbp)
	movq	%rbx, -168(%rbp)
	movq	%r9, -192(%rbp)
	call	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode@PLT
	movl	%eax, -184(%rbp)
	movq	-200(%rbp), %r11
	movq	%r11, %rdi
	call	_ZN6icu_6713FieldPositionD1Ev@PLT
	movq	-192(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-184(%rbp), %eax
.L315:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L326
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L325:
	.cfi_restore_state
	testl	%ecx, %ecx
	je	.L319
	.p2align 4,,10
	.p2align 3
.L317:
	movl	$1, (%r12)
	xorl	%eax, %eax
	jmp	.L315
	.p2align 4,,10
	.p2align 3
.L320:
	movq	0(%r13), %rax
	movq	%r9, %rdx
	movq	%r12, %r8
	movq	%r15, %rsi
	leaq	-160(%rbp), %r11
	movq	%r9, -184(%rbp)
	movq	%r13, %rdi
	movq	%r11, -192(%rbp)
	movq	%r11, %rcx
	call	*40(%rax)
	movq	-184(%rbp), %r9
	movq	-192(%rbp), %r11
	jmp	.L321
.L326:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2581:
	.size	unum_formatUFormattable_67, .-unum_formatUFormattable_67
	.p2align 4
	.globl	unum_open_67
	.type	unum_open_67, @function
unum_open_67:
.LFB2548:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$408, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r9), %r10d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r10d, %r10d
	jg	.L328
	movl	%edi, %r12d
	movq	%r9, %rbx
	cmpl	$16, %edi
	ja	.L329
	movq	%rcx, %r14
	movl	%edi, %ecx
	leaq	.L331(%rip), %rdi
	movq	%r8, %r13
	movslq	(%rdi,%rcx,4), %rax
	addq	%rdi, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L331:
	.long	.L339-.L331
	.long	.L330-.L331
	.long	.L330-.L331
	.long	.L330-.L331
	.long	.L330-.L331
	.long	.L338-.L331
	.long	.L337-.L331
	.long	.L336-.L331
	.long	.L335-.L331
	.long	.L334-.L331
	.long	.L330-.L331
	.long	.L330-.L331
	.long	.L330-.L331
	.long	.L330-.L331
	.long	.L333-.L331
	.long	.L332-.L331
	.long	.L330-.L331
	.text
	.p2align 4,,10
	.p2align 3
.L376:
	testl	%eax, %eax
	jg	.L328
	movl	$7, (%rbx)
	.p2align 4,,10
	.p2align 3
.L328:
	xorl	%r12d, %r12d
.L327:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L375
	addq	$408, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L333:
	.cfi_restore_state
	leaq	-288(%rbp), %r13
	xorl	%edx, %edx
	movq	%r14, %rsi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	movq	%rbx, %rdx
	xorl	%esi, %esi
.L373:
	movq	%r13, %rdi
	call	_ZN6icu_6720CompactDecimalFormat14createInstanceERKNS_6LocaleE19UNumberCompactStyleR10UErrorCode@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN6icu_676LocaleD1Ev@PLT
.L340:
	movl	(%rbx), %eax
	testq	%r12, %r12
	je	.L376
	testl	%eax, %eax
	jle	.L327
	movq	(%r12), %rax
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	*8(%rax)
	jmp	.L327
.L332:
	leaq	-288(%rbp), %r13
	xorl	%edx, %edx
	movq	%r14, %rsi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	movq	%rbx, %rdx
	movl	$1, %esi
	jmp	.L373
.L337:
	leaq	-288(%rbp), %r15
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	movl	$752, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L352
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN6icu_6721RuleBasedNumberFormatC1ENS_15URBNFRuleSetTagERKNS_6LocaleER10UErrorCode@PLT
.L352:
	movq	%r15, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	jmp	.L340
.L336:
	leaq	-288(%rbp), %r15
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	movl	$752, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L352
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movl	$2, %esi
	movq	%rax, %rdi
	call	_ZN6icu_6721RuleBasedNumberFormatC1ENS_15URBNFRuleSetTagERKNS_6LocaleER10UErrorCode@PLT
	jmp	.L352
.L335:
	leaq	-288(%rbp), %r15
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	movl	$752, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L352
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movl	$3, %esi
	movq	%rax, %rdi
	call	_ZN6icu_6721RuleBasedNumberFormatC1ENS_15URBNFRuleSetTagERKNS_6LocaleER10UErrorCode@PLT
	jmp	.L352
.L334:
	leaq	-432(%rbp), %rax
	leaq	-288(%rbp), %r15
	movq	%rax, %rdi
	movq	%rax, -440(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EPKDsi@PLT
	testq	%r13, %r13
	movq	%r14, %rsi
	movq	%r15, %rdi
	leaq	-368(%rbp), %rax
	cmove	%rax, %r13
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	movl	$752, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L348
	movq	-440(%rbp), %rsi
	movq	%rbx, %r8
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%rax, %rdi
	call	_ZN6icu_6721RuleBasedNumberFormatC1ERKNS_13UnicodeStringERKNS_6LocaleER11UParseErrorR10UErrorCode@PLT
.L348:
	movq	%r15, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
.L374:
	movq	-440(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L340
.L330:
	leaq	-288(%rbp), %r13
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	movl	%r12d, %esi
	movq	%r13, %rdi
	movq	%rbx, %rdx
	call	_ZN6icu_6712NumberFormat14createInstanceERKNS_6LocaleE18UNumberFormatStyleR10UErrorCode@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN6icu_676LocaleD1Ev@PLT
	jmp	.L340
.L338:
	leaq	-288(%rbp), %r15
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	movl	$752, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L352
	movq	%rbx, %rcx
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN6icu_6721RuleBasedNumberFormatC1ENS_15URBNFRuleSetTagERKNS_6LocaleER10UErrorCode@PLT
	jmp	.L352
.L339:
	leaq	-432(%rbp), %rax
	leaq	-288(%rbp), %r12
	movq	%rax, %rdi
	movq	%rax, -440(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EPKDsi@PLT
	testq	%r13, %r13
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	-368(%rbp), %rax
	cmove	%rax, %r13
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	movl	$2816, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L342
	movq	%rax, %rdi
	movq	%rbx, %rdx
	movq	%r12, %rsi
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKNS_6LocaleER10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L377
	movl	$368, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L345
	movq	-440(%rbp), %rsi
	movq	%rbx, %r8
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	_ZN6icu_6713DecimalFormatC1ERKNS_13UnicodeStringEPNS_20DecimalFormatSymbolsER11UParseErrorR10UErrorCode@PLT
	jmp	.L374
.L329:
	movl	$16, (%r9)
	xorl	%r12d, %r12d
	jmp	.L327
	.p2align 4,,10
	.p2align 3
.L377:
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*8(%rax)
.L344:
	movq	-440(%rbp), %rdi
	xorl	%r12d, %r12d
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L327
.L375:
	call	__stack_chk_fail@PLT
.L345:
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*8(%rax)
	jmp	.L374
.L342:
	movq	%r12, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	movl	$7, (%rbx)
	jmp	.L344
	.cfi_endproc
.LFE2548:
	.size	unum_open_67, .-unum_open_67
	.p2align 4
	.globl	unum_formatDecimal_67
	.type	unum_formatDecimal_67, @function
unum_formatDecimal_67:
.LFB2555:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$280, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %r13
	movq	%rcx, -296(%rbp)
	movl	0(%r13), %r10d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r10d, %r10d
	jg	.L387
	testq	%rcx, %rcx
	movl	%r8d, %r12d
	sete	%cl
	testl	%r8d, %r8d
	setne	%al
	testb	%al, %cl
	jne	.L388
	testl	%r8d, %r8d
	js	.L388
	leaq	16+_ZTVN6icu_6713FieldPositionE(%rip), %rax
	movq	%rdi, %r10
	movq	%r9, %rbx
	movl	$0, -256(%rbp)
	movq	%rax, -272(%rbp)
	movl	$4294967295, %eax
	movq	%rax, -264(%rbp)
	testq	%r9, %r9
	je	.L382
	movl	(%r9), %eax
	movl	%eax, -264(%rbp)
.L382:
	testl	%edx, %edx
	jns	.L383
	movq	%rsi, %rdi
	movq	%r10, -312(%rbp)
	movq	%rsi, -304(%rbp)
	call	strlen@PLT
	movq	-312(%rbp), %r10
	movq	-304(%rbp), %rsi
	movl	%eax, %edx
.L383:
	leaq	-176(%rbp), %r14
	movl	%edx, %edx
	movq	%r13, %rcx
	movq	%r10, -304(%rbp)
	movq	%r14, %rdi
	leaq	-240(%rbp), %r15
	call	_ZN6icu_6711FormattableC1ENS_11StringPieceER10UErrorCode@PLT
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	testl	%r12d, %r12d
	movq	-304(%rbp), %r10
	movq	%rax, -240(%rbp)
	movl	$2, %eax
	movw	%ax, -232(%rbp)
	jne	.L399
.L384:
	movq	(%r10), %rax
	leaq	-272(%rbp), %r9
	movq	%r13, %r8
	movq	%r15, %rdx
	movq	%r9, -304(%rbp)
	movq	%r9, %rcx
	movq	%r14, %rsi
	movq	%r10, %rdi
	call	*40(%rax)
	testq	%rbx, %rbx
	movq	-304(%rbp), %r9
	je	.L385
	movq	-260(%rbp), %rax
	movq	%rax, 4(%rbx)
.L385:
	movq	-296(%rbp), %rax
	movl	%r12d, %edx
	movq	%r13, %rcx
	movq	%r15, %rdi
	leaq	-280(%rbp), %rsi
	movq	%r9, -304(%rbp)
	movq	%rax, -280(%rbp)
	call	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode@PLT
	movl	%eax, %r12d
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6711FormattableD1Ev@PLT
	movq	-304(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN6icu_6713FieldPositionD1Ev@PLT
.L379:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L400
	addq	$280, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L399:
	.cfi_restore_state
	movq	-296(%rbp), %rsi
	movl	%r12d, %ecx
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeString5setToEPDsii@PLT
	movq	-304(%rbp), %r10
	jmp	.L384
	.p2align 4,,10
	.p2align 3
.L388:
	movl	$1, 0(%r13)
	movl	$-1, %r12d
	jmp	.L379
	.p2align 4,,10
	.p2align 3
.L387:
	movl	$-1, %r12d
	jmp	.L379
.L400:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2555:
	.size	unum_formatDecimal_67, .-unum_formatDecimal_67
	.p2align 4
	.globl	unum_formatDoubleCurrency_67
	.type	unum_formatDoubleCurrency_67, @function
unum_formatDoubleCurrency_67:
.LFB2556:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$280, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -312(%rbp)
	movl	(%r9), %edi
	movq	%rdx, -296(%rbp)
	movl	%ecx, -300(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edi, %edi
	jg	.L411
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rsi, %r13
	movq	%r8, %rbx
	movq	%r9, %r12
	movq	%rax, -240(%rbp)
	movl	$2, %eax
	movw	%ax, -232(%rbp)
	testq	%rdx, %rdx
	jne	.L412
	leaq	-240(%rbp), %r15
	testl	%ecx, %ecx
	jne	.L412
.L403:
	leaq	16+_ZTVN6icu_6713FieldPositionE(%rip), %rax
	movl	$0, -256(%rbp)
	movq	%rax, -272(%rbp)
	movl	$4294967295, %eax
	movq	%rax, -264(%rbp)
	testq	%rbx, %rbx
	je	.L405
	movl	(%rbx), %eax
	movl	%eax, -264(%rbp)
.L405:
	movl	$128, %edi
	movq	%r13, -280(%rbp)
	movsd	%xmm0, -320(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L406
	movsd	-320(%rbp), %xmm0
	leaq	-280(%rbp), %rsi
	movq	%r12, %rdx
	movq	%rax, %rdi
	call	_ZN6icu_6714CurrencyAmountC1EdNS_14ConstChar16PtrER10UErrorCode@PLT
.L406:
	testq	%r14, %r14
	je	.L426
	leaq	-176(%rbp), %r13
	movq	%r14, %rsi
	leaq	-272(%rbp), %r14
	movq	%r13, %rdi
	call	_ZN6icu_6711FormattableC1EPNS_7UObjectE@PLT
	movq	%r12, %r8
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	-312(%rbp), %rdi
	movq	%r13, %rsi
	movq	(%rdi), %rax
	call	*40(%rax)
	testq	%rbx, %rbx
	je	.L409
	movq	-260(%rbp), %rax
	movq	%rax, 4(%rbx)
.L409:
	movq	-296(%rbp), %rax
	movl	-300(%rbp), %edx
	movq	%r12, %rcx
	movq	%r15, %rdi
	leaq	-280(%rbp), %rsi
	movq	%rax, -280(%rbp)
	call	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode@PLT
	movl	%eax, %r12d
	movq	%r13, %rdi
	call	_ZN6icu_6711FormattableD1Ev@PLT
.L408:
	movq	%r14, %rdi
	call	_ZN6icu_6713FieldPositionD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L401:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L427
	addq	$280, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L412:
	.cfi_restore_state
	movl	-300(%rbp), %ecx
	movq	-296(%rbp), %rsi
	xorl	%edx, %edx
	leaq	-240(%rbp), %r15
	movq	%r15, %rdi
	movsd	%xmm0, -320(%rbp)
	call	_ZN6icu_6713UnicodeString5setToEPDsii@PLT
	movsd	-320(%rbp), %xmm0
	jmp	.L403
	.p2align 4,,10
	.p2align 3
.L411:
	movl	$-1, %r12d
	jmp	.L401
.L427:
	call	__stack_chk_fail@PLT
.L426:
	movl	$7, (%r12)
	leaq	-272(%rbp), %r14
	orl	$-1, %r12d
	jmp	.L408
	.cfi_endproc
.LFE2556:
	.size	unum_formatDoubleCurrency_67, .-unum_formatDoubleCurrency_67
	.p2align 4
	.globl	unum_parse_67
	.type	unum_parse_67, @function
unum_parse_67:
.LFB2558:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-176(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$152, %rsp
	movq	%rdi, -184(%rbp)
	movq	%r13, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6711FormattableC1Ev@PLT
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L429
	movq	-184(%rbp), %rsi
	movq	%r12, %r9
	movq	%rbx, %r8
	movl	%r15d, %ecx
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	_ZL8parseResRN6icu_6711FormattableEPKPvPKDsiPiP10UErrorCode.part.0
.L429:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK6icu_6711Formattable7getLongER10UErrorCode@PLT
	movq	%r13, %rdi
	movl	%eax, %r12d
	call	_ZN6icu_6711FormattableD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L432
	addq	$152, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L432:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2558:
	.size	unum_parse_67, .-unum_parse_67
	.p2align 4
	.globl	unum_parseInt64_67
	.type	unum_parseInt64_67, @function
unum_parseInt64_67:
.LFB2559:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-176(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$152, %rsp
	movq	%rdi, -184(%rbp)
	movq	%r13, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6711FormattableC1Ev@PLT
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L434
	movq	-184(%rbp), %rsi
	movq	%r12, %r9
	movq	%rbx, %r8
	movl	%r15d, %ecx
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	_ZL8parseResRN6icu_6711FormattableEPKPvPKDsiPiP10UErrorCode.part.0
.L434:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK6icu_6711Formattable8getInt64ER10UErrorCode@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN6icu_6711FormattableD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L437
	addq	$152, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L437:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2559:
	.size	unum_parseInt64_67, .-unum_parseInt64_67
	.p2align 4
	.globl	unum_parseDouble_67
	.type	unum_parseDouble_67, @function
unum_parseDouble_67:
.LFB2560:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-176(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$152, %rsp
	movq	%rdi, -184(%rbp)
	movq	%r13, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6711FormattableC1Ev@PLT
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L439
	movq	-184(%rbp), %rsi
	movq	%r12, %r9
	movq	%rbx, %r8
	movl	%r15d, %ecx
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	_ZL8parseResRN6icu_6711FormattableEPKPvPKDsiPiP10UErrorCode.part.0
.L439:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK6icu_6711Formattable9getDoubleER10UErrorCode@PLT
	movq	%r13, %rdi
	movsd	%xmm0, -184(%rbp)
	call	_ZN6icu_6711FormattableD1Ev@PLT
	movsd	-184(%rbp), %xmm0
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L442
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L442:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2560:
	.size	unum_parseDouble_67, .-unum_parseDouble_67
	.p2align 4
	.globl	unum_parseDecimal_67
	.type	unum_parseDecimal_67, @function
unum_parseDecimal_67:
.LFB2561:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %r15
	movq	%rdi, -184(%rbp)
	movl	%edx, -188(%rbp)
	movq	%rcx, -200(%rbp)
	movl	(%r15), %edi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edi, %edi
	jg	.L452
	testq	%r8, %r8
	movq	%r8, %r12
	movl	%r9d, %ebx
	sete	%dl
	testl	%r9d, %r9d
	setne	%al
	testb	%al, %dl
	jne	.L454
	testl	%r9d, %r9d
	js	.L454
	leaq	-176(%rbp), %r14
	movq	%rsi, %r13
	movq	%r14, %rdi
	call	_ZN6icu_6711FormattableC1Ev@PLT
	movl	(%r15), %esi
	testl	%esi, %esi
	jg	.L447
	movq	-200(%rbp), %r8
	movq	%r15, %r9
	movq	%r13, %rdx
	movq	%r14, %rdi
	movl	-188(%rbp), %ecx
	movq	-184(%rbp), %rsi
	call	_ZL8parseResRN6icu_6711FormattableEPKPvPKDsiPiP10UErrorCode.part.0
.L447:
	movq	%r15, %rsi
	movq	%r14, %rdi
	movl	$-1, %r13d
	call	_ZN6icu_6711Formattable16getDecimalNumberER10UErrorCode@PLT
	movl	(%r15), %ecx
	testl	%ecx, %ecx
	jg	.L448
	movl	%edx, %r13d
	cmpl	%edx, %ebx
	jge	.L449
	movl	$15, (%r15)
.L448:
	movq	%r14, %rdi
	call	_ZN6icu_6711FormattableD1Ev@PLT
.L444:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L456
	addq	$168, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L449:
	.cfi_restore_state
	je	.L457
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	strcpy@PLT
	jmp	.L448
	.p2align 4,,10
	.p2align 3
.L457:
	movslq	%edx, %rdx
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	strncpy@PLT
	movl	$-124, (%r15)
	jmp	.L448
	.p2align 4,,10
	.p2align 3
.L454:
	movl	$1, (%r15)
	movl	$-1, %r13d
	jmp	.L444
	.p2align 4,,10
	.p2align 3
.L452:
	movl	$-1, %r13d
	jmp	.L444
.L456:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2561:
	.size	unum_parseDecimal_67, .-unum_parseDecimal_67
	.p2align 4
	.globl	unum_setSymbol_67
	.type	unum_setSymbol_67, @function
unum_setSymbol_67:
.LFB2575:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$2984, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r8, %r8
	je	.L458
	movl	(%r8), %eax
	testl	%eax, %eax
	jg	.L458
	testq	%rdi, %rdi
	je	.L460
	movl	%esi, %r14d
	cmpl	$27, %esi
	ja	.L460
	movq	%rdx, %r13
	testq	%rdx, %rdx
	je	.L460
	movl	%ecx, %r12d
	cmpl	$-1, %ecx
	jl	.L460
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6713DecimalFormatE(%rip), %rdx
	leaq	_ZTIN6icu_6712NumberFormatE(%rip), %rsi
	movq	%r8, -3016(%rbp)
	call	__dynamic_cast@PLT
	movq	-3016(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L481
	movq	(%rax), %rax
	movq	%r15, %rdi
	leaq	-2880(%rbp), %rbx
	call	*312(%rax)
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_6720DecimalFormatSymbolsC1ERKS0_@PLT
	leaq	-3008(%rbp), %r8
	movl	%r12d, %edx
	movq	%r13, %rsi
	movq	%r8, %rdi
	movq	%r8, -3016(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EPKDsi@PLT
	movl	%r14d, %eax
	movq	-3016(%rbp), %r8
	salq	$6, %rax
	cmpl	$8, %r14d
	leaq	8(%rbx,%rax), %rdi
	je	.L482
	cmpl	$9, %r14d
	je	.L483
	movq	%r8, %rsi
	movq	%r8, -3016(%rbp)
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	cmpl	$4, %r14d
	movq	-3016(%rbp), %r8
	je	.L484
.L474:
	leal	-18(%r14), %esi
	cmpl	$8, %esi
	ja	.L469
.L470:
	movl	$-1, -1016(%rbp)
.L469:
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	(%r15), %rax
	movq	%r15, %rdi
	movq	%rbx, %rsi
	call	*328(%rax)
	movq	%rbx, %rdi
	call	_ZN6icu_6720DecimalFormatSymbolsD1Ev@PLT
	.p2align 4,,10
	.p2align 3
.L458:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L485
	addq	$2984, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L460:
	.cfi_restore_state
	movl	$1, (%r8)
	jmp	.L458
	.p2align 4,,10
	.p2align 3
.L484:
	movq	%r8, %rdi
	xorl	%esi, %esi
	movq	%r8, -3016(%rbp)
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movl	%eax, %edi
	movl	%eax, %r14d
	call	u_charDigitValue_67@PLT
	movq	-3016(%rbp), %r8
	testl	%eax, %eax
	jne	.L470
	xorl	%esi, %esi
	movq	%r8, %rdi
	movl	$2147483647, %edx
	call	_ZNK6icu_6713UnicodeString11countChar32Eii@PLT
	movq	-3016(%rbp), %r8
	cmpl	$1, %eax
	jne	.L470
	leaq	-1144(%rbp), %rax
	movl	%r14d, -1016(%rbp)
	leaq	-1720(%rbp), %r13
	movq	%rax, -3024(%rbp)
	leaq	-2944(%rbp), %r12
.L468:
	addl	$1, %r14d
	movq	%r12, %rdi
	movq	%r8, -3016(%rbp)
	movl	%r14d, %esi
	call	_ZN6icu_6713UnicodeStringC1Ei@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	addq	$64, %r13
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpq	%r13, -3024(%rbp)
	movq	-3016(%rbp), %r8
	jne	.L468
	jmp	.L469
	.p2align 4,,10
	.p2align 3
.L482:
	movb	$1, -72(%rbp)
.L465:
	movq	%r8, %rsi
	movq	%r8, -3016(%rbp)
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	-3016(%rbp), %r8
	jmp	.L474
	.p2align 4,,10
	.p2align 3
.L483:
	movb	$1, -71(%rbp)
	jmp	.L465
	.p2align 4,,10
	.p2align 3
.L481:
	movl	$16, (%r8)
	jmp	.L458
.L485:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2575:
	.size	unum_setSymbol_67, .-unum_setSymbol_67
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC2:
	.long	0
	.long	-1074790400
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
