	.file	"casetrn.cpp"
	.text
	.p2align 4
	.globl	utrans_rep_caseContextIterator_67
	.type	utrans_rep_caseContextIterator_67, @function
utrans_rep_caseContextIterator_67:
.LFB3043:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	testb	%sil, %sil
	js	.L17
	jne	.L18
	cmpb	$0, 28(%rbx)
	movl	12(%rbx), %r8d
	js	.L3
.L5:
	cmpl	%r8d, 16(%rbx)
	jle	.L9
	movq	(%rdi), %rax
	movl	%r8d, %esi
	call	*80(%rax)
	testl	%eax, %eax
	js	.L19
	xorl	%edx, %edx
	cmpl	$65535, %eax
	setg	%dl
	addl	$1, %edx
	addl	%edx, 12(%rbx)
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	.cfi_restore_state
	movl	24(%rbx), %r8d
	movb	%sil, 28(%rbx)
	movl	%r8d, 12(%rbx)
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L17:
	movl	20(%rbx), %r8d
	movb	%sil, 28(%rbx)
	movl	%r8d, 12(%rbx)
.L3:
	cmpl	%r8d, 8(%rbx)
	jge	.L12
	movq	(%rdi), %rax
	leal	-1(%r8), %esi
	call	*80(%rax)
	testl	%eax, %eax
	js	.L20
	xorl	%edx, %edx
	cmpl	$65535, %eax
	setg	%dl
	addl	$1, %edx
	subl	%edx, 12(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	movl	12(%rbx), %eax
	movb	$1, 29(%rbx)
	movl	%eax, 16(%rbx)
	movl	$-1, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L9:
	movb	$1, 29(%rbx)
	movl	$-1, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L20:
	movl	12(%rbx), %eax
	movl	%eax, 8(%rbx)
	movl	$-1, %eax
	jmp	.L1
.L12:
	movl	$-1, %eax
	jmp	.L1
	.cfi_endproc
.LFE3043:
	.size	utrans_rep_caseContextIterator_67, .-utrans_rep_caseContextIterator_67
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6721CaseMapTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositiona
	.type	_ZNK6icu_6721CaseMapTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositiona, @function
_ZNK6icu_6721CaseMapTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositiona:
.LFB3055:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -184(%rbp)
	movl	8(%rdx), %r13d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	12(%rdx), %r13d
	jl	.L46
.L21:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L47
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	.cfi_restore_state
	movl	(%rdx), %eax
	pxor	%xmm0, %xmm0
	testb	%cl, %cl
	movl	%r13d, %r15d
	movups	%xmm0, -152(%rbp)
	movq	%rdx, %rbx
	leaq	-128(%rbp), %r12
	movq	%rsi, %r13
	movl	%eax, -152(%rbp)
	movl	4(%rdx), %eax
	movq	$0, -136(%rbp)
	movl	%eax, -144(%rbp)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, -128(%rbp)
	movl	$2, %eax
	movw	%ax, -120(%rbp)
	leaq	-176(%rbp), %rax
	movq	%rax, -200(%rbp)
	leaq	-160(%rbp), %rax
	movq	%rax, -208(%rbp)
	leaq	-168(%rbp), %rax
	movq	%rsi, -160(%rbp)
	movq	%rax, -216(%rbp)
	setne	-189(%rbp)
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L49:
	cmpl	$31, %ecx
	jg	.L28
	movq	-176(%rbp), %rax
	movq	-216(%rbp), %rdx
	xorl	%esi, %esi
	movq	%r12, %rdi
	movl	%ecx, -188(%rbp)
	movq	%rax, -168(%rbp)
	call	_ZN6icu_6713UnicodeString5setToEaNS_14ConstChar16PtrEi@PLT
	movl	-188(%rbp), %ecx
	movl	%ecx, %eax
	subl	%r14d, %eax
.L29:
	movq	0(%r13), %r8
	movl	%eax, -188(%rbp)
	movq	%r12, %rcx
	movl	%r15d, %edx
	movl	-140(%rbp), %esi
	movq	%r13, %rdi
	call	*32(%r8)
	movl	-188(%rbp), %eax
	testl	%eax, %eax
	je	.L27
	movl	4(%rbx), %edx
	addl	%eax, %r15d
	addl	%eax, %edx
	addl	12(%rbx), %eax
	movl	%eax, %ecx
	movl	%edx, 4(%rbx)
	movl	%edx, -144(%rbp)
	movl	%eax, 12(%rbx)
	cmpl	%r15d, %ecx
	jle	.L24
.L23:
	movq	0(%r13), %rax
	movl	%r15d, %esi
	movq	%r13, %rdi
	xorl	%r14d, %r14d
	movl	%r15d, -140(%rbp)
	call	*80(%rax)
	movq	-200(%rbp), %rcx
	movl	$1, %r8d
	movq	-208(%rbp), %rdx
	cmpl	$65535, %eax
	movl	%eax, %edi
	movq	-184(%rbp), %rax
	leaq	utrans_rep_caseContextIterator_67(%rip), %rsi
	seta	%r14b
	addl	$1, %r14d
	addl	%r14d, %r15d
	movl	%r15d, -136(%rbp)
	call	*88(%rax)
	cmpb	$0, -131(%rbp)
	movl	%eax, %ecx
	je	.L26
	cmpb	$0, -189(%rbp)
	jne	.L48
.L26:
	testl	%ecx, %ecx
	jns	.L49
.L27:
	movl	12(%rbx), %ecx
	cmpl	%r15d, %ecx
	jg	.L23
.L24:
	movl	%r15d, 8(%rbx)
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L28:
	movq	%r12, %rdi
	movl	%ecx, -188(%rbp)
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	movswl	-120(%rbp), %edx
	movl	-188(%rbp), %ecx
	testw	%dx, %dx
	js	.L30
	sarl	$5, %edx
.L31:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString7replaceEiii@PLT
	movswl	-120(%rbp), %ecx
	testw	%cx, %cx
	js	.L32
	sarl	$5, %ecx
.L33:
	movl	%ecx, %eax
	subl	%r14d, %eax
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L30:
	movl	-116(%rbp), %edx
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L32:
	movl	-116(%rbp), %ecx
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L48:
	movl	-140(%rbp), %eax
	movq	%r12, %rdi
	movl	%eax, 8(%rbx)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L21
.L47:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3055:
	.size	_ZNK6icu_6721CaseMapTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositiona, .-_ZNK6icu_6721CaseMapTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositiona
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721CaseMapTransliterator16getStaticClassIDEv
	.type	_ZN6icu_6721CaseMapTransliterator16getStaticClassIDEv, @function
_ZN6icu_6721CaseMapTransliterator16getStaticClassIDEv:
.LFB3044:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6721CaseMapTransliterator16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3044:
	.size	_ZN6icu_6721CaseMapTransliterator16getStaticClassIDEv, .-_ZN6icu_6721CaseMapTransliterator16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721CaseMapTransliteratorC2ERKNS_13UnicodeStringEPFiiPFiPvaES4_PPKDsiE
	.type	_ZN6icu_6721CaseMapTransliteratorC2ERKNS_13UnicodeStringEPFiiPFiPvaES4_PPKDsiE, @function
_ZN6icu_6721CaseMapTransliteratorC2ERKNS_13UnicodeStringEPFiiPFiPvaES4_PPKDsiE:
.LFB3046:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	xorl	%edx, %edx
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZN6icu_6714TransliteratorC2ERKNS_13UnicodeStringEPNS_13UnicodeFilterE@PLT
	leaq	16+_ZTVN6icu_6721CaseMapTransliteratorE(%rip), %rax
	movq	%r12, 88(%rbx)
	movq	%rax, (%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3046:
	.size	_ZN6icu_6721CaseMapTransliteratorC2ERKNS_13UnicodeStringEPFiiPFiPvaES4_PPKDsiE, .-_ZN6icu_6721CaseMapTransliteratorC2ERKNS_13UnicodeStringEPFiiPFiPvaES4_PPKDsiE
	.globl	_ZN6icu_6721CaseMapTransliteratorC1ERKNS_13UnicodeStringEPFiiPFiPvaES4_PPKDsiE
	.set	_ZN6icu_6721CaseMapTransliteratorC1ERKNS_13UnicodeStringEPFiiPFiPvaES4_PPKDsiE,_ZN6icu_6721CaseMapTransliteratorC2ERKNS_13UnicodeStringEPFiiPFiPvaES4_PPKDsiE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721CaseMapTransliteratorD2Ev
	.type	_ZN6icu_6721CaseMapTransliteratorD2Ev, @function
_ZN6icu_6721CaseMapTransliteratorD2Ev:
.LFB3049:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6721CaseMapTransliteratorE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6714TransliteratorD2Ev@PLT
	.cfi_endproc
.LFE3049:
	.size	_ZN6icu_6721CaseMapTransliteratorD2Ev, .-_ZN6icu_6721CaseMapTransliteratorD2Ev
	.globl	_ZN6icu_6721CaseMapTransliteratorD1Ev
	.set	_ZN6icu_6721CaseMapTransliteratorD1Ev,_ZN6icu_6721CaseMapTransliteratorD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721CaseMapTransliteratorD0Ev
	.type	_ZN6icu_6721CaseMapTransliteratorD0Ev, @function
_ZN6icu_6721CaseMapTransliteratorD0Ev:
.LFB3051:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6721CaseMapTransliteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6714TransliteratorD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3051:
	.size	_ZN6icu_6721CaseMapTransliteratorD0Ev, .-_ZN6icu_6721CaseMapTransliteratorD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721CaseMapTransliteratorC2ERKS0_
	.type	_ZN6icu_6721CaseMapTransliteratorC2ERKS0_, @function
_ZN6icu_6721CaseMapTransliteratorC2ERKS0_:
.LFB3053:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZN6icu_6714TransliteratorC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6721CaseMapTransliteratorE(%rip), %rax
	movq	%rax, (%rbx)
	movq	88(%r12), %rax
	movq	%rax, 88(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3053:
	.size	_ZN6icu_6721CaseMapTransliteratorC2ERKS0_, .-_ZN6icu_6721CaseMapTransliteratorC2ERKS0_
	.globl	_ZN6icu_6721CaseMapTransliteratorC1ERKS0_
	.set	_ZN6icu_6721CaseMapTransliteratorC1ERKS0_,_ZN6icu_6721CaseMapTransliteratorC2ERKS0_
	.weak	_ZTSN6icu_6721CaseMapTransliteratorE
	.section	.rodata._ZTSN6icu_6721CaseMapTransliteratorE,"aG",@progbits,_ZTSN6icu_6721CaseMapTransliteratorE,comdat
	.align 32
	.type	_ZTSN6icu_6721CaseMapTransliteratorE, @object
	.size	_ZTSN6icu_6721CaseMapTransliteratorE, 33
_ZTSN6icu_6721CaseMapTransliteratorE:
	.string	"N6icu_6721CaseMapTransliteratorE"
	.weak	_ZTIN6icu_6721CaseMapTransliteratorE
	.section	.data.rel.ro._ZTIN6icu_6721CaseMapTransliteratorE,"awG",@progbits,_ZTIN6icu_6721CaseMapTransliteratorE,comdat
	.align 8
	.type	_ZTIN6icu_6721CaseMapTransliteratorE, @object
	.size	_ZTIN6icu_6721CaseMapTransliteratorE, 24
_ZTIN6icu_6721CaseMapTransliteratorE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6721CaseMapTransliteratorE
	.quad	_ZTIN6icu_6714TransliteratorE
	.weak	_ZTVN6icu_6721CaseMapTransliteratorE
	.section	.data.rel.ro._ZTVN6icu_6721CaseMapTransliteratorE,"awG",@progbits,_ZTVN6icu_6721CaseMapTransliteratorE,comdat
	.align 8
	.type	_ZTVN6icu_6721CaseMapTransliteratorE, @object
	.size	_ZTVN6icu_6721CaseMapTransliteratorE, 152
_ZTVN6icu_6721CaseMapTransliteratorE:
	.quad	0
	.quad	_ZTIN6icu_6721CaseMapTransliteratorE
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableEii
	.quad	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableE
	.quad	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableER14UTransPositionRKNS_13UnicodeStringER10UErrorCode
	.quad	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableER14UTransPositioniR10UErrorCode
	.quad	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableER14UTransPositionR10UErrorCode
	.quad	_ZNK6icu_6714Transliterator21finishTransliterationERNS_11ReplaceableER14UTransPosition
	.quad	_ZNK6icu_6721CaseMapTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositiona
	.quad	_ZNK6icu_6714Transliterator21filteredTransliterateERNS_11ReplaceableER14UTransPositiona
	.quad	_ZNK6icu_6714Transliterator21filteredTransliterateERNS_11ReplaceableER14UTransPositionaa
	.quad	_ZNK6icu_6714Transliterator5getIDEv
	.quad	_ZNK6icu_6714Transliterator7toRulesERNS_13UnicodeStringEa
	.quad	_ZNK6icu_6714Transliterator18handleGetSourceSetERNS_10UnicodeSetE
	.quad	_ZNK6icu_6714Transliterator12getTargetSetERNS_10UnicodeSetE
	.local	_ZZN6icu_6721CaseMapTransliterator16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6721CaseMapTransliterator16getStaticClassIDEvE7classID,1,1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
