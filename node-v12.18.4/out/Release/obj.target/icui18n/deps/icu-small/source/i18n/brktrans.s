	.file	"brktrans.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6719BreakTransliterator17getDynamicClassIDEv
	.type	_ZNK6icu_6719BreakTransliterator17getDynamicClassIDEv, @function
_ZNK6icu_6719BreakTransliterator17getDynamicClassIDEv:
.LFB3143:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6719BreakTransliterator16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3143:
	.size	_ZNK6icu_6719BreakTransliterator17getDynamicClassIDEv, .-_ZNK6icu_6719BreakTransliterator17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6719BreakTransliterator12getInsertionEv
	.type	_ZNK6icu_6719BreakTransliterator12getInsertionEv, @function
_ZNK6icu_6719BreakTransliterator12getInsertionEv:
.LFB3162:
	.cfi_startproc
	endbr64
	leaq	104(%rdi), %rax
	ret
	.cfi_endproc
.LFE3162:
	.size	_ZNK6icu_6719BreakTransliterator12getInsertionEv, .-_ZNK6icu_6719BreakTransliterator12getInsertionEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719BreakTransliteratorD2Ev
	.type	_ZN6icu_6719BreakTransliteratorD2Ev, @function
_ZN6icu_6719BreakTransliteratorD2Ev:
.LFB3152:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6719BreakTransliteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	addq	$104, %rdi
	subq	$8, %rsp
	movq	%rax, -104(%rdi)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	96(%r12), %rdi
	testq	%rdi, %rdi
	je	.L5
	movq	(%rdi), %rax
	call	*8(%rax)
.L5:
	movq	88(%r12), %rdi
	testq	%rdi, %rdi
	je	.L6
	movq	(%rdi), %rax
	call	*8(%rax)
.L6:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6714TransliteratorD2Ev@PLT
	.cfi_endproc
.LFE3152:
	.size	_ZN6icu_6719BreakTransliteratorD2Ev, .-_ZN6icu_6719BreakTransliteratorD2Ev
	.globl	_ZN6icu_6719BreakTransliteratorD1Ev
	.set	_ZN6icu_6719BreakTransliteratorD1Ev,_ZN6icu_6719BreakTransliteratorD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719BreakTransliterator12setInsertionERKNS_13UnicodeStringE
	.type	_ZN6icu_6719BreakTransliterator12setInsertionERKNS_13UnicodeStringE, @function
_ZN6icu_6719BreakTransliterator12setInsertionERKNS_13UnicodeStringE:
.LFB3163:
	.cfi_startproc
	endbr64
	addq	$104, %rdi
	jmp	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	.cfi_endproc
.LFE3163:
	.size	_ZN6icu_6719BreakTransliterator12setInsertionERKNS_13UnicodeStringE, .-_ZN6icu_6719BreakTransliterator12setInsertionERKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6719BreakTransliterator5cloneEv
	.type	_ZNK6icu_6719BreakTransliterator5cloneEv, @function
_ZNK6icu_6719BreakTransliterator5cloneEv:
.LFB3158:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	$168, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L15
	movq	%rax, %rdi
	movq	%rbx, %rsi
	call	_ZN6icu_6714TransliteratorC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6719BreakTransliteratorE(%rip), %rax
	pxor	%xmm0, %xmm0
	leaq	104(%rbx), %rsi
	movq	%rax, (%r12)
	leaq	104(%r12), %rdi
	movups	%xmm0, 88(%r12)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
.L15:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3158:
	.size	_ZNK6icu_6719BreakTransliterator5cloneEv, .-_ZNK6icu_6719BreakTransliterator5cloneEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719BreakTransliteratorD0Ev
	.type	_ZN6icu_6719BreakTransliteratorD0Ev, @function
_ZN6icu_6719BreakTransliteratorD0Ev:
.LFB3154:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6719BreakTransliteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	addq	$104, %rdi
	subq	$8, %rsp
	movq	%rax, -104(%rdi)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	96(%r12), %rdi
	testq	%rdi, %rdi
	je	.L22
	movq	(%rdi), %rax
	call	*8(%rax)
.L22:
	movq	88(%r12), %rdi
	testq	%rdi, %rdi
	je	.L23
	movq	(%rdi), %rax
	call	*8(%rax)
.L23:
	movq	%r12, %rdi
	call	_ZN6icu_6714TransliteratorD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3154:
	.size	_ZN6icu_6719BreakTransliteratorD0Ev, .-_ZN6icu_6719BreakTransliteratorD0Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6719BreakTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositiona
	.type	_ZNK6icu_6719BreakTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositiona, @function
_ZNK6icu_6719BreakTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositiona:
.LFB3159:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	xorl	%edi, %edi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$152, %rsp
	movl	%ecx, -148(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -132(%rbp)
	call	umtx_lock_67@PLT
	movq	88(%r15), %r12
	pxor	%xmm0, %xmm0
	xorl	%edi, %edi
	movq	96(%r15), %r13
	movups	%xmm0, 88(%r15)
	call	umtx_unlock_67@PLT
	testq	%r12, %r12
	je	.L86
.L32:
	testq	%r13, %r13
	je	.L87
	testq	%r12, %r12
	je	.L88
.L58:
	movl	-132(%rbp), %edx
	testl	%edx, %edx
	jle	.L89
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*8(%rax)
.L85:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
.L31:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L90
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L89:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN6icu_679UVector3217removeAllElementsEv@PLT
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6711ReplaceableE(%rip), %rsi
	movq	%rax, -128(%rbp)
	leaq	_ZTIN6icu_6713UnicodeStringE(%rip), %rdx
	movl	$2, %eax
	movq	%r14, %rdi
	movw	%ax, -120(%rbp)
	call	__dynamic_cast@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L36
	leaq	-128(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -160(%rbp)
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
.L37:
	movq	(%r12), %rax
	movq	-160(%rbp), %rsi
	movq	%r12, %rdi
	call	*56(%rax)
	movq	(%r12), %rax
	movq	%r12, %rdi
	movl	8(%rbx), %esi
	call	*128(%rax)
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*104(%rax)
	cmpl	$-1, %eax
	je	.L42
	movq	%r15, -168(%rbp)
	movq	%r12, %r15
	movq	-160(%rbp), %r12
	movq	%r14, -176(%rbp)
	movl	%eax, %r14d
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L91:
	testl	%r14d, %r14d
	je	.L44
	leal	-1(%r14), %esi
	movq	%r12, %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movl	%eax, %edi
	call	u_charType_67@PLT
	movl	$510, %ecx
	btl	%eax, %ecx
	jnc	.L44
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movl	%eax, %edi
	call	u_charType_67@PLT
	movl	$510, %ecx
	btl	%eax, %ecx
	jnc	.L44
	movslq	8(%r13), %rax
	movl	%eax, %esi
	addl	$1, %esi
	js	.L46
	cmpl	12(%r13), %esi
	jle	.L47
.L46:
	leaq	-132(%rbp), %rdx
	movq	%r13, %rdi
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	je	.L44
	movslq	8(%r13), %rax
.L47:
	movq	24(%r13), %rdx
	movl	%r14d, (%rdx,%rax,4)
	addl	$1, 8(%r13)
	.p2align 4,,10
	.p2align 3
.L44:
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*104(%rax)
	movl	%eax, %r14d
	cmpl	$-1, %eax
	je	.L83
.L38:
	cmpl	%r14d, 12(%rbx)
	jg	.L91
.L83:
	movq	%r15, %r12
	movq	-176(%rbp), %r14
	movq	-168(%rbp), %r15
.L42:
	movl	8(%r13), %eax
	testl	%eax, %eax
	jne	.L39
	movl	$0, -168(%rbp)
.L40:
	movl	-168(%rbp), %ecx
	movl	12(%rbx), %edx
	addl	%ecx, 4(%rbx)
	addl	%ecx, %edx
	addl	%ecx, %eax
	cmpb	$0, -148(%rbp)
	movl	%edx, 12(%rbx)
	cmovne	%eax, %edx
	xorl	%edi, %edi
	movl	%edx, 8(%rbx)
	call	umtx_lock_67@PLT
	cmpq	$0, 88(%r15)
	je	.L92
.L52:
	cmpq	$0, 96(%r15)
	je	.L93
	xorl	%edi, %edi
	call	umtx_unlock_67@PLT
	movq	-160(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*8(%rax)
.L54:
	testq	%r12, %r12
	jne	.L85
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L36:
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	24(%rax), %r8
	movq	%r8, -168(%rbp)
	call	*64(%rax)
	movq	-168(%rbp), %r8
	xorl	%esi, %esi
	movq	%r14, %rdi
	movl	%eax, %edx
	leaq	-128(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	%rax, %rcx
	call	*%r8
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L87:
	movl	$32, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L54
	leaq	-132(%rbp), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_679UVector32C1ER10UErrorCode@PLT
	testq	%r12, %r12
	jne	.L58
	.p2align 4,,10
	.p2align 3
.L88:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*8(%rax)
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L86:
	call	_ZN6icu_676Locale10getEnglishEv@PLT
	leaq	-132(%rbp), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6713BreakIterator18createWordInstanceERKNS_6LocaleER10UErrorCode@PLT
	movq	%rax, %r12
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L39:
	movswl	112(%r15), %edx
	testw	%dx, %dx
	js	.L48
	sarl	$5, %edx
.L49:
	imull	%eax, %edx
	movl	%edx, -168(%rbp)
	testl	%eax, %eax
	jg	.L94
	xorl	%eax, %eax
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L94:
	movq	24(%r13), %rdx
	subl	$1, %eax
	movq	%rbx, -184(%rbp)
	movq	%r14, %rbx
	movslq	%eax, %rcx
	movl	(%rdx,%rcx,4), %ecx
	movl	%ecx, -176(%rbp)
	leaq	104(%r15), %rcx
	movq	%rcx, %r14
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L95:
	movq	24(%r13), %rdx
	subl	$1, %eax
.L50:
	movl	%eax, 8(%r13)
	cltq
	movq	%r14, %rcx
	movq	%rbx, %rdi
	movl	(%rdx,%rax,4), %esi
	movq	(%rbx), %rax
	movl	%esi, %edx
	call	*32(%rax)
	movl	8(%r13), %eax
	testl	%eax, %eax
	jg	.L95
	movq	-184(%rbp), %rbx
	movl	-176(%rbp), %eax
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L93:
	movq	%r13, 96(%r15)
	xorl	%edi, %edi
	call	umtx_unlock_67@PLT
	movq	-160(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L92:
	movq	%r12, 88(%r15)
	xorl	%r12d, %r12d
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L48:
	movl	116(%r15), %edx
	jmp	.L49
.L90:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3159:
	.size	_ZNK6icu_6719BreakTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositiona, .-_ZNK6icu_6719BreakTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositiona
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719BreakTransliterator16getStaticClassIDEv
	.type	_ZN6icu_6719BreakTransliterator16getStaticClassIDEv, @function
_ZN6icu_6719BreakTransliterator16getStaticClassIDEv:
.LFB3142:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6719BreakTransliterator16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3142:
	.size	_ZN6icu_6719BreakTransliterator16getStaticClassIDEv, .-_ZN6icu_6719BreakTransliterator16getStaticClassIDEv
	.section	.rodata.str2.8,"aMS",@progbits,2
	.align 8
.LC0:
	.string	"A"
	.string	"n"
	.string	"y"
	.string	"-"
	.string	"B"
	.string	"r"
	.string	"e"
	.string	"a"
	.string	"k"
	.string	"I"
	.string	"n"
	.string	"t"
	.string	"e"
	.string	"r"
	.string	"n"
	.string	"a"
	.string	"l"
	.string	""
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719BreakTransliteratorC2EPNS_13UnicodeFilterE
	.type	_ZN6icu_6719BreakTransliteratorC2EPNS_13UnicodeFilterE, @function
_ZN6icu_6719BreakTransliteratorC2EPNS_13UnicodeFilterE:
.LFB3149:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$17, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	movl	$1, %esi
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-112(%rbp), %r12
	leaq	-120(%rbp), %rdx
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	.LC0(%rip), %rax
	movq	%rax, -120(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6714TransliteratorC2ERKNS_13UnicodeStringEPNS_13UnicodeFilterE@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	16+_ZTVN6icu_6719BreakTransliteratorE(%rip), %rax
	pxor	%xmm0, %xmm0
	leaq	104(%rbx), %rdi
	movl	$32, %esi
	movq	%rax, (%rbx)
	movups	%xmm0, 88(%rbx)
	call	_ZN6icu_6713UnicodeStringC1EDs@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L100
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L100:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3149:
	.size	_ZN6icu_6719BreakTransliteratorC2EPNS_13UnicodeFilterE, .-_ZN6icu_6719BreakTransliteratorC2EPNS_13UnicodeFilterE
	.globl	_ZN6icu_6719BreakTransliteratorC1EPNS_13UnicodeFilterE
	.set	_ZN6icu_6719BreakTransliteratorC1EPNS_13UnicodeFilterE,_ZN6icu_6719BreakTransliteratorC2EPNS_13UnicodeFilterE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719BreakTransliteratorC2ERKS0_
	.type	_ZN6icu_6719BreakTransliteratorC2ERKS0_, @function
_ZN6icu_6719BreakTransliteratorC2ERKS0_:
.LFB3156:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZN6icu_6714TransliteratorC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6719BreakTransliteratorE(%rip), %rax
	pxor	%xmm0, %xmm0
	leaq	104(%r12), %rsi
	movq	%rax, (%rbx)
	leaq	104(%rbx), %rdi
	movups	%xmm0, 88(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	.cfi_endproc
.LFE3156:
	.size	_ZN6icu_6719BreakTransliteratorC2ERKS0_, .-_ZN6icu_6719BreakTransliteratorC2ERKS0_
	.globl	_ZN6icu_6719BreakTransliteratorC1ERKS0_
	.set	_ZN6icu_6719BreakTransliteratorC1ERKS0_,_ZN6icu_6719BreakTransliteratorC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719BreakTransliterator19replaceableAsStringERNS_11ReplaceableE
	.type	_ZN6icu_6719BreakTransliterator19replaceableAsStringERNS_11ReplaceableE, @function
_ZN6icu_6719BreakTransliterator19replaceableAsStringERNS_11ReplaceableE:
.LFB3164:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6713UnicodeStringE(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	leaq	_ZTIN6icu_6711ReplaceableE(%rip), %rsi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	%rax, (%rdi)
	movl	$2, %eax
	movw	%ax, 8(%rdi)
	movq	%r13, %rdi
	call	__dynamic_cast@PLT
	testq	%rax, %rax
	je	.L104
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L104:
	.cfi_restore_state
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rbx
	call	*64(%rax)
	movq	%r12, %rcx
	movq	%r13, %rdi
	xorl	%esi, %esi
	movl	%eax, %edx
	call	*%rbx
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3164:
	.size	_ZN6icu_6719BreakTransliterator19replaceableAsStringERNS_11ReplaceableE, .-_ZN6icu_6719BreakTransliterator19replaceableAsStringERNS_11ReplaceableE
	.weak	_ZTSN6icu_6719BreakTransliteratorE
	.section	.rodata._ZTSN6icu_6719BreakTransliteratorE,"aG",@progbits,_ZTSN6icu_6719BreakTransliteratorE,comdat
	.align 16
	.type	_ZTSN6icu_6719BreakTransliteratorE, @object
	.size	_ZTSN6icu_6719BreakTransliteratorE, 31
_ZTSN6icu_6719BreakTransliteratorE:
	.string	"N6icu_6719BreakTransliteratorE"
	.weak	_ZTIN6icu_6719BreakTransliteratorE
	.section	.data.rel.ro._ZTIN6icu_6719BreakTransliteratorE,"awG",@progbits,_ZTIN6icu_6719BreakTransliteratorE,comdat
	.align 8
	.type	_ZTIN6icu_6719BreakTransliteratorE, @object
	.size	_ZTIN6icu_6719BreakTransliteratorE, 24
_ZTIN6icu_6719BreakTransliteratorE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6719BreakTransliteratorE
	.quad	_ZTIN6icu_6714TransliteratorE
	.weak	_ZTVN6icu_6719BreakTransliteratorE
	.section	.data.rel.ro._ZTVN6icu_6719BreakTransliteratorE,"awG",@progbits,_ZTVN6icu_6719BreakTransliteratorE,comdat
	.align 8
	.type	_ZTVN6icu_6719BreakTransliteratorE, @object
	.size	_ZTVN6icu_6719BreakTransliteratorE, 168
_ZTVN6icu_6719BreakTransliteratorE:
	.quad	0
	.quad	_ZTIN6icu_6719BreakTransliteratorE
	.quad	_ZN6icu_6719BreakTransliteratorD1Ev
	.quad	_ZN6icu_6719BreakTransliteratorD0Ev
	.quad	_ZNK6icu_6719BreakTransliterator17getDynamicClassIDEv
	.quad	_ZNK6icu_6719BreakTransliterator5cloneEv
	.quad	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableEii
	.quad	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableE
	.quad	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableER14UTransPositionRKNS_13UnicodeStringER10UErrorCode
	.quad	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableER14UTransPositioniR10UErrorCode
	.quad	_ZNK6icu_6714Transliterator13transliterateERNS_11ReplaceableER14UTransPositionR10UErrorCode
	.quad	_ZNK6icu_6714Transliterator21finishTransliterationERNS_11ReplaceableER14UTransPosition
	.quad	_ZNK6icu_6719BreakTransliterator19handleTransliterateERNS_11ReplaceableER14UTransPositiona
	.quad	_ZNK6icu_6714Transliterator21filteredTransliterateERNS_11ReplaceableER14UTransPositiona
	.quad	_ZNK6icu_6714Transliterator21filteredTransliterateERNS_11ReplaceableER14UTransPositionaa
	.quad	_ZNK6icu_6714Transliterator5getIDEv
	.quad	_ZNK6icu_6714Transliterator7toRulesERNS_13UnicodeStringEa
	.quad	_ZNK6icu_6714Transliterator18handleGetSourceSetERNS_10UnicodeSetE
	.quad	_ZNK6icu_6714Transliterator12getTargetSetERNS_10UnicodeSetE
	.quad	_ZNK6icu_6719BreakTransliterator12getInsertionEv
	.quad	_ZN6icu_6719BreakTransliterator12setInsertionERKNS_13UnicodeStringE
	.local	_ZZN6icu_6719BreakTransliterator16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6719BreakTransliterator16getStaticClassIDEvE7classID,1,1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
