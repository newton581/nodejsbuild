	.file	"ucol_sit.cpp"
	.text
	.p2align 4
	.type	_processCollatorOption, @function
_processCollatorOption:
.LFB2535:
	.cfi_startproc
	endbr64
	movzbl	(%rdx), %eax
	cmpb	$49, %al
	je	.L8
	cmpb	$50, %al
	je	.L9
	cmpb	$51, %al
	je	.L10
	cmpb	$52, %al
	je	.L11
	cmpb	$68, %al
	je	.L12
	cmpb	$73, %al
	je	.L13
	cmpb	$76, %al
	je	.L14
	cmpb	$78, %al
	je	.L15
	cmpb	$79, %al
	je	.L16
	cmpb	$83, %al
	je	.L17
	cmpb	$85, %al
	je	.L18
	cmpb	$88, %al
	je	.L24
	movl	$1, (%rcx)
	movl	$-1, %eax
.L4:
	movl	%esi, %esi
	leaq	1(%rdx), %r8
	movl	%eax, 448(%rdi,%rsi,4)
	movzbl	1(%rdx), %eax
	cmpb	$95, %al
	je	.L19
	testb	%al, %al
	jne	.L5
.L19:
	movl	(%rcx), %eax
	testl	%eax, %eax
	jle	.L1
.L5:
	movl	$1, (%rcx)
.L1:
	movq	%r8, %rax
	ret
.L8:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L2:
	leaq	_ZL11conversions(%rip), %r8
	movl	4(%r8,%rax,8), %eax
	jmp	.L4
.L9:
	movl	$1, %eax
	jmp	.L2
.L10:
	movl	$2, %eax
	jmp	.L2
.L11:
	movl	$3, %eax
	jmp	.L2
.L12:
	movl	$4, %eax
	jmp	.L2
.L13:
	movl	$5, %eax
	jmp	.L2
.L14:
	movl	$6, %eax
	jmp	.L2
.L15:
	movl	$7, %eax
	jmp	.L2
.L16:
	movl	$8, %eax
	jmp	.L2
.L17:
	movl	$9, %eax
	jmp	.L2
.L18:
	movl	$10, %eax
	jmp	.L2
.L24:
	movl	$11, %eax
	jmp	.L2
	.cfi_endproc
.LFE2535:
	.size	_processCollatorOption, .-_processCollatorOption
	.p2align 4
	.type	_processLocaleElement, @function
_processLocaleElement:
.LFB2533:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leal	-4(%rsi), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	salq	$6, %r12
	.cfi_offset 3, -40
	movq	%rcx, %rbx
	addq	%rdi, %r12
	subq	$8, %rsp
	movsbl	(%rdx), %edi
	cmpl	$1, %eax
	jbe	.L39
	testl	%esi, %esi
	je	.L39
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L48:
	testb	%dil, %dil
	je	.L25
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L25
.L39:
	call	uprv_asciitolower_67@PLT
	addq	$1, %r13
	movq	%r12, %rdi
	movq	%rbx, %rdx
	movsbl	%al, %esi
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
	movsbl	0(%r13), %edi
	cmpb	$95, %dil
	jne	.L48
.L25:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	.cfi_restore_state
	cmpb	$95, %dil
	je	.L25
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L25
.L28:
	movsbl	%dil, %esi
	movq	%rbx, %rdx
	movq	%r12, %rdi
	addq	$1, %r13
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
	movzbl	0(%r13), %edi
	testb	%dil, %dil
	jne	.L49
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2533:
	.size	_processLocaleElement, .-_processLocaleElement
	.p2align 4
	.type	_processRFC3066Locale, @function
_processRFC3066Locale:
.LFB2534:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	1(%rdx), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	leaq	2(%rdx), %rdi
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movsbl	(%rdx), %esi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	testq	%rax, %rax
	je	.L51
	movq	%rax, %rdx
	movq	%rax, %rbx
	subq	%r14, %rdx
	cmpq	$255, %rdx
	jle	.L52
.L51:
	movl	$15, 0(%r13)
.L50:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L60
	addq	$88, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	.cfi_restore_state
	leaq	-128(%rbp), %r15
	leaq	-115(%rbp), %rax
	movq	%r13, %rcx
	movq	%r14, %rsi
	movq	%rax, -128(%rbp)
	movq	%r15, %rdi
	xorl	%eax, %eax
	movl	$0, -72(%rbp)
	movl	$40, -120(%rbp)
	movw	%ax, -116(%rbp)
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	leaq	384(%r12), %rdi
	movq	%r13, %rdx
	movq	%r15, %rsi
	call	_ZN6icu_6710CharString8copyFromERKS0_R10UErrorCode@PLT
	cmpb	$0, -116(%rbp)
	jne	.L61
.L54:
	leaq	1(%rbx), %r14
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L61:
	movq	-128(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L54
.L60:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2534:
	.size	_processRFC3066Locale, .-_processRFC3066Locale
	.p2align 4
	.type	_ZL18ucol_sit_readSpecsP12CollatorSpecPKcP11UParseErrorP10UErrorCode.isra.0, @function
_ZL18ucol_sit_readSpecsP12CollatorSpecPKcP11UParseErrorP10UErrorCode.isra.0:
.LFB3418:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -152(%rbp)
	movl	(%rcx), %edx
	movq	%rsi, -144(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L73
	movzbl	(%rsi), %eax
	movq	%rdi, %rbx
	movq	%rcx, %r14
	movq	%rsi, %r15
	.p2align 4,,10
	.p2align 3
.L70:
	testb	%al, %al
	je	.L62
	cmpb	$65, %al
	je	.L74
	cmpb	$66, %al
	je	.L75
	cmpb	$67, %al
	je	.L76
	cmpb	$68, %al
	je	.L77
	cmpb	$69, %al
	je	.L78
	cmpb	$70, %al
	je	.L79
	cmpb	$72, %al
	je	.L80
	cmpb	$75, %al
	je	.L81
	cmpb	$76, %al
	je	.L82
	cmpb	$78, %al
	je	.L83
	cmpb	$82, %al
	je	.L84
	cmpb	$83, %al
	je	.L85
	cmpb	$84, %al
	je	.L86
	cmpb	$86, %al
	je	.L87
	cmpb	$88, %al
	je	.L88
	cmpb	$90, %al
	je	.L89
	movl	$16, %r8d
	cmpb	$80, %al
	je	.L65
	movl	$1, (%r14)
	cmpb	$95, (%r15)
	jne	.L97
	movl	$1, %edx
	.p2align 4,,10
	.p2align 3
.L71:
	movzbl	1(%r15), %eax
	addq	$1, %r15
	cmpb	$95, %al
	je	.L71
	testl	%edx, %edx
	jle	.L70
.L97:
	movl	%r15d, %eax
	subl	-144(%rbp), %eax
.L63:
	movq	-152(%rbp), %rbx
	movl	%eax, (%rbx)
.L62:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L99
	addq	$120, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L89:
	.cfi_restore_state
	movl	$15, %r8d
	.p2align 4,,10
	.p2align 3
.L65:
	movslq	%r8d, %r13
	leaq	_ZL7options(%rip), %rcx
	leaq	1(%r15), %rdx
	movq	%rbx, %rdi
	leaq	0(%r13,%r13,2), %rax
	leaq	(%rcx,%rax,8), %rax
	movq	%r14, %rcx
	movl	16(%rax), %esi
	call	*8(%rax)
	movq	%r13, %r8
	movq	%r14, %rcx
	movq	%r15, %rsi
	salq	$6, %r8
	movq	%rax, %r12
	leaq	-115(%rbp), %rax
	movl	$0, -72(%rbp)
	leaq	560(%rbx,%r8), %r13
	movq	%r12, %rdx
	leaq	-128(%rbp), %r8
	movq	%rax, -128(%rbp)
	subq	%r15, %rdx
	xorl	%eax, %eax
	movq	%r8, %rdi
	movq	%r8, -136(%rbp)
	movl	$40, -120(%rbp)
	movw	%ax, -116(%rbp)
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movq	-136(%rbp), %r8
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%r8, %rsi
	call	_ZN6icu_6710CharString8copyFromERKS0_R10UErrorCode@PLT
	cmpb	$0, -116(%rbp)
	jne	.L100
.L67:
	movzbl	(%r12), %eax
	movl	(%r14), %edx
	movq	%r12, %r15
	cmpb	$95, %al
	je	.L71
	testl	%edx, %edx
	jle	.L70
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L100:
	movq	-128(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L67
.L74:
	xorl	%r8d, %r8d
	jmp	.L65
.L75:
	movl	$1, %r8d
	jmp	.L65
.L76:
	movl	$2, %r8d
	jmp	.L65
.L77:
	movl	$3, %r8d
	jmp	.L65
.L78:
	movl	$4, %r8d
	jmp	.L65
.L79:
	movl	$5, %r8d
	jmp	.L65
.L80:
	movl	$6, %r8d
	jmp	.L65
.L81:
	movl	$7, %r8d
	jmp	.L65
.L82:
	movl	$8, %r8d
	jmp	.L65
.L83:
	movl	$9, %r8d
	jmp	.L65
.L84:
	movl	$10, %r8d
	jmp	.L65
.L85:
	movl	$11, %r8d
	jmp	.L65
.L86:
	movl	$12, %r8d
	jmp	.L65
.L87:
	movl	$13, %r8d
	jmp	.L65
.L88:
	movl	$14, %r8d
	jmp	.L65
.L73:
	movq	%rsi, %r15
	xorl	%eax, %eax
	jmp	.L63
.L99:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3418:
	.size	_ZL18ucol_sit_readSpecsP12CollatorSpecPKcP11UParseErrorP10UErrorCode.isra.0, .-_ZL18ucol_sit_readSpecsP12CollatorSpecPKcP11UParseErrorP10UErrorCode.isra.0
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"_"
	.text
	.p2align 4
	.type	_ZL29ucol_sit_calculateWholeLocaleP12CollatorSpecR10UErrorCode.part.0, @function
_ZL29ucol_sit_calculateWholeLocaleP12CollatorSpecR10UErrorCode.part.0:
.LFB3423:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	384(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$32, %rsp
	movl	56(%rdi), %edx
	movq	(%rdi), %rsi
	movq	%r13, %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movl	120(%rbx), %r8d
	testl	%r8d, %r8d
	jne	.L112
.L102:
	movl	184(%rbx), %edi
	testl	%edi, %edi
	jne	.L113
	movl	248(%rbx), %esi
	testl	%esi, %esi
	jne	.L114
.L106:
	movl	312(%rbx), %edx
	testl	%edx, %edx
	jne	.L115
.L108:
	movl	376(%rbx), %eax
	testl	%eax, %eax
	jne	.L116
.L101:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L117
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L114:
	.cfi_restore_state
	leaq	-64(%rbp), %r14
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movl	-56(%rbp), %edx
	movq	-64(%rbp), %rsi
	movq	%r12, %rcx
	movq	%r13, %rdi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
.L104:
	movl	248(%rbx), %ecx
	testl	%ecx, %ecx
	je	.L106
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movl	-56(%rbp), %edx
	movq	-64(%rbp), %rsi
	movq	%r12, %rcx
	movq	%r13, %rdi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movl	248(%rbx), %edx
	movq	%r12, %rcx
	movq	%r13, %rdi
	movq	192(%rbx), %rsi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movl	312(%rbx), %edx
	testl	%edx, %edx
	je	.L108
.L115:
	leaq	-64(%rbp), %rdi
	leaq	_ZL16collationKeyword(%rip), %rsi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movl	-56(%rbp), %edx
	movq	-64(%rbp), %rsi
	movq	%r12, %rcx
	movq	%r13, %rdi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movl	312(%rbx), %edx
	movq	%r12, %rcx
	movq	%r13, %rdi
	movq	256(%rbx), %rsi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movl	376(%rbx), %eax
	testl	%eax, %eax
	je	.L101
.L116:
	leaq	-64(%rbp), %rdi
	leaq	_ZL15providerKeyword(%rip), %rsi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movl	-56(%rbp), %edx
	movq	-64(%rbp), %rsi
	movq	%r12, %rcx
	movq	%r13, %rdi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movl	376(%rbx), %edx
	movq	%r12, %rcx
	movq	%r13, %rdi
	movq	320(%rbx), %rsi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L113:
	leaq	-64(%rbp), %r14
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movl	-56(%rbp), %edx
	movq	-64(%rbp), %rsi
	movq	%r12, %rcx
	movq	%r13, %rdi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movl	184(%rbx), %edx
	movq	%r12, %rcx
	movq	%r13, %rdi
	movq	128(%rbx), %rsi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L112:
	leaq	-64(%rbp), %rdi
	leaq	.LC0(%rip), %rsi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movl	-56(%rbp), %edx
	movq	-64(%rbp), %rsi
	movq	%r12, %rcx
	movq	%r13, %rdi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movl	120(%rbx), %edx
	movq	64(%rbx), %rsi
	movq	%r12, %rcx
	movq	%r13, %rdi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	jmp	.L102
.L117:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3423:
	.size	_ZL29ucol_sit_calculateWholeLocaleP12CollatorSpecR10UErrorCode.part.0, .-_ZL29ucol_sit_calculateWholeLocaleP12CollatorSpecR10UErrorCode.part.0
	.p2align 4
	.type	_processVariableTop, @function
_processVariableTop:
.LFB2537:
	.cfi_startproc
	endbr64
	testl	%esi, %esi
	jne	.L119
	movl	(%rcx), %esi
	xorl	%r9d, %r9d
	testl	%esi, %esi
	jle	.L120
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L199:
	leal	-97(%rax), %esi
	cmpb	$5, %sil
	jbe	.L124
	leal	-65(%rax), %esi
	cmpb	$5, %sil
	ja	.L168
	leal	-55(%rax), %r8d
.L126:
	movsbl	1(%rdx), %eax
	leaq	1(%rdx), %r11
	testb	%al, %al
	je	.L127
.L200:
	leal	-48(%rax), %esi
	cmpb	$9, %sil
	jbe	.L128
	leal	-97(%rax), %esi
	cmpb	$5, %sil
	jbe	.L129
	leal	-65(%rax), %esi
	cmpb	$5, %sil
	ja	.L125
	movsbl	%al, %esi
	leal	-55(%rsi), %eax
.L130:
	movsbl	2(%rdx), %esi
	sall	$4, %r8d
	leaq	2(%rdx), %r11
	orl	%r8d, %eax
	testb	%sil, %sil
	je	.L132
	leal	-48(%rsi), %r8d
	cmpb	$9, %r8b
	jbe	.L133
	leal	-97(%rsi), %r8d
	cmpb	$5, %r8b
	jbe	.L134
	leal	-65(%rsi), %r8d
	cmpb	$5, %r8b
	ja	.L125
	movsbl	%sil, %r8d
	leal	-55(%r8), %esi
.L135:
	movsbl	3(%rdx), %r8d
	sall	$4, %eax
	leaq	3(%rdx), %r11
	orl	%eax, %esi
	testb	%r8b, %r8b
	je	.L197
	leal	-48(%r8), %eax
	cmpb	$9, %al
	jbe	.L137
	leal	-97(%r8), %eax
	cmpb	$5, %al
	jbe	.L138
	leal	-65(%r8), %eax
	cmpb	$5, %al
	ja	.L125
	movsbl	%r8b, %r10d
	leal	-55(%r10), %r8d
.L139:
	movl	%esi, %eax
	leaq	4(%rdx), %r11
	sall	$4, %eax
	orl	%eax, %r8d
	movw	%r8w, 484(%rdi,%r9,2)
	addq	$1, %r9
	cmpq	$32, %r9
	je	.L198
	movq	%r11, %rdx
.L120:
	movsbl	(%rdx), %eax
	movl	%r9d, %r10d
	movl	%r9d, %esi
	cmpb	$95, %al
	je	.L122
	testb	%al, %al
	je	.L122
	leal	-48(%rax), %esi
	addl	$1, %r10d
	cmpb	$9, %sil
	ja	.L199
	leal	-48(%rax), %r8d
	movsbl	1(%rdx), %eax
	leaq	1(%rdx), %r11
	testb	%al, %al
	jne	.L200
.L127:
	movl	%r8d, %eax
.L132:
	movl	$1, (%rcx)
	movw	%ax, 484(%rdi,%r9,2)
.L141:
	movl	%r10d, 548(%rdi)
	cmpl	$32, %r10d
	je	.L201
.L118:
	movq	%r11, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L128:
	subl	$48, %eax
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L133:
	subl	$48, %esi
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L137:
	subl	$48, %r8d
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L124:
	leal	-87(%rax), %r8d
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L129:
	movsbl	%al, %esi
	leal	-87(%rsi), %eax
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L134:
	movsbl	%sil, %r8d
	leal	-87(%r8), %esi
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L138:
	movsbl	%r8b, %r10d
	leal	-87(%r10), %r8d
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L119:
	movsbl	(%rdx), %eax
	movq	%rdx, %r11
	xorl	%r8d, %r8d
	testb	%al, %al
	je	.L144
	leal	-48(%rax), %esi
	cmpb	$9, %sil
	jbe	.L146
	leal	-97(%rax), %esi
	cmpb	$5, %sil
	jbe	.L147
	leal	-65(%rax), %esi
	cmpb	$5, %sil
	ja	.L148
	subl	$55, %eax
.L149:
	movsbl	1(%rdx), %esi
	movzwl	%ax, %r8d
	leaq	1(%rdx), %r11
	testb	%sil, %sil
	je	.L144
	leal	-48(%rsi), %r8d
	cmpb	$9, %r8b
	jbe	.L151
	leal	-97(%rsi), %r8d
	cmpb	$5, %r8b
	jbe	.L152
	leal	-65(%rsi), %r8d
	cmpb	$5, %r8b
	ja	.L148
	subl	$55, %esi
.L153:
	sall	$4, %eax
	leaq	2(%rdx), %r11
	orl	%esi, %eax
	movsbl	2(%rdx), %esi
	movzwl	%ax, %r8d
	testb	%sil, %sil
	je	.L144
	leal	-48(%rsi), %r8d
	cmpb	$9, %r8b
	jbe	.L154
	leal	-97(%rsi), %r8d
	cmpb	$5, %r8b
	jbe	.L155
	leal	-65(%rsi), %r8d
	cmpb	$5, %r8b
	ja	.L148
	subl	$55, %esi
.L156:
	sall	$4, %eax
	leaq	3(%rdx), %r11
	orl	%esi, %eax
	movsbl	3(%rdx), %esi
	movzwl	%ax, %r8d
	testb	%sil, %sil
	je	.L144
	leal	-48(%rsi), %r8d
	cmpb	$9, %r8b
	ja	.L157
	subl	$48, %esi
.L158:
	sall	$4, %eax
	leaq	4(%rdx), %r11
	movl	(%rcx), %edx
	orl	%esi, %eax
	movzwl	%ax, %eax
	movl	%eax, 480(%rdi)
	testl	%edx, %edx
	jle	.L165
	movq	%r11, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L122:
	movl	%esi, 548(%rdi)
	movq	%rdx, %r11
.L165:
	movb	$1, 552(%rdi)
	movq	%r11, %rax
	ret
.L144:
	movl	$1, (%rcx)
	movq	%r11, %rax
	movl	%r8d, 480(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L146:
	subl	$48, %eax
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L198:
	movl	$32, 548(%rdi)
	movzbl	4(%rdx), %eax
	testb	%al, %al
	je	.L165
	cmpb	$95, %al
	je	.L165
.L163:
	movl	$15, (%rcx)
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L197:
	movl	%esi, %eax
	jmp	.L132
.L168:
	movq	%rdx, %r11
.L125:
	xorl	%eax, %eax
	movl	$1, (%rcx)
	movw	%ax, 484(%rdi,%r9,2)
	jmp	.L141
	.p2align 4,,10
	.p2align 3
.L151:
	subl	$48, %esi
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L154:
	subl	$48, %esi
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L157:
	leal	-97(%rsi), %r8d
	cmpb	$5, %r8b
	ja	.L159
	subl	$87, %esi
	jmp	.L158
	.p2align 4,,10
	.p2align 3
.L147:
	subl	$87, %eax
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L152:
	subl	$87, %esi
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L155:
	subl	$87, %esi
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L159:
	leal	-65(%rsi), %r8d
	cmpb	$5, %r8b
	ja	.L148
	subl	$55, %esi
	jmp	.L158
.L201:
	movzbl	(%r11), %eax
	testb	%al, %al
	je	.L118
	cmpb	$95, %al
	je	.L118
	jmp	.L163
.L196:
	movl	$0, 548(%rdi)
	movq	%rdx, %r11
	jmp	.L118
.L148:
	movl	$1, (%rcx)
	movl	$0, 480(%rdi)
	jmp	.L118
	.cfi_endproc
.LFE2537:
	.size	_processVariableTop, .-_processVariableTop
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev:
.LFB2925:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE2925:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei:
.LFB2928:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	cmpl	$40, %esi
	jg	.L215
	ret
	.p2align 4,,10
	.p2align 3
.L215:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L203
	cmpb	$0, 12(%rbx)
	jne	.L216
.L207:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L203:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L216:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L207
	.cfi_endproc
.LFE2928:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EED2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev:
.LFB2931:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L219
	ret
	.p2align 4,,10
	.p2align 3
.L219:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE2931:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5EOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_:
.LFB2934:
	.cfi_startproc
	endbr64
	movzbl	12(%rsi), %eax
	movq	(%rsi), %r8
	movslq	8(%rsi), %rdx
	movb	%al, 12(%rdi)
	leaq	13(%rsi), %rax
	movq	%r8, (%rdi)
	movl	%edx, 8(%rdi)
	cmpq	%rax, %r8
	je	.L222
	movq	%rax, (%rsi)
	movl	$40, 8(%rsi)
	movb	$0, 12(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L222:
	leaq	13(%rdi), %r9
	movq	%r8, %rsi
	movq	%r9, (%rdi)
	movq	%r9, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE2934:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_:
.LFB2936:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 12(%rdi)
	movq	%rsi, %rbx
	jne	.L228
.L224:
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movzbl	12(%rbx), %eax
	movb	%al, 12(%r12)
	movq	(%rbx), %rsi
	leaq	13(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L229
	movq	%rsi, (%r12)
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L228:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L229:
	leaq	13(%r12), %rdi
	movslq	8(%rbx), %rdx
	movq	%rdi, (%r12)
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2936:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv:
.LFB2937:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE2937:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv:
.LFB2938:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE2938:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv:
.LFB2939:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	addq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE2939:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB2940:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE2940:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB2941:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE2941:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci:
.LFB2942:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L245
	testl	%edx, %edx
	jle	.L245
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 12(%rdi)
	jne	.L248
.L237:
	movq	%rsi, (%rbx)
	movl	%edx, 8(%rbx)
	movb	$0, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L245:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L248:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	%edx, -28(%rbp)
	movq	%rsi, -24(%rbp)
	call	uprv_free_67@PLT
	movl	-28(%rbp), %edx
	movq	-24(%rbp), %rsi
	jmp	.L237
	.cfi_endproc
.LFE2942:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii:
.LFB2943:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L252
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movl	%esi, %r13d
	movl	%edx, %r12d
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L252
	testl	%r12d, %r12d
	jg	.L259
	cmpb	$0, 12(%rbx)
	jne	.L260
.L254:
	movq	%r14, (%rbx)
	movq	%r14, %rax
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L259:
	.cfi_restore_state
	cmpl	%r12d, 8(%rbx)
	cmovle	8(%rbx), %r12d
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	je	.L254
.L260:
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L252:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2943:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, .-_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi:
.LFB2944:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 12(%rdi)
	movq	%rdi, %rbx
	je	.L262
	movq	(%rdi), %r8
.L263:
	leaq	13(%rbx), %rax
	movl	%r12d, 0(%r13)
	movq	%rax, (%rbx)
	movq	%r8, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L262:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L266
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	cmovg	%eax, %r12d
	movslq	%r12d, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L266
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L266:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2944:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, .-_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv:
.LFB2945:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L273
	ret
	.p2align 4,,10
	.p2align 3
.L273:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE2945:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv:
.LFB2946:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE2946:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_:
.LFB2947:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2947:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_:
.LFB2948:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE2948:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5ERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_:
.LFB2950:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2950:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_:
.LFB2952:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2952:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.section	.rodata.str1.1
.LC1:
	.string	"icudt67l-coll"
.LC2:
	.string	"collations"
.LC3:
	.string	"collation"
.LC4:
	.string	"default"
	.text
	.p2align 4
	.globl	ucol_prepareShortStringOpen_67
	.type	ucol_prepareShortStringOpen_67, @function
ucol_prepareShortStringOpen_67:
.LFB2542:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$2536, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L279
	testq	%rdx, %rdx
	leaq	-2560(%rbp), %rax
	movq	%rdi, %rsi
	movq	%rcx, %r12
	cmove	%rax, %rdx
	xorl	%ecx, %ecx
	xorl	%edi, %edi
	xorl	%ebx, %ebx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	xorl	%r10d, %r10d
	movw	%bx, -2212(%rbp)
	leaq	-2467(%rbp), %rax
	pxor	%xmm0, %xmm0
	xorl	%r11d, %r11d
	xorl	%ebx, %ebx
	xorl	%r14d, %r14d
	xorl	%r15d, %r15d
	movw	%cx, 8(%rdx)
	xorl	%ecx, %ecx
	movw	%di, 40(%rdx)
	xorl	%edi, %edi
	addq	$4, %rdx
	leaq	-2480(%rbp), %r13
	movq	%rax, -2480(%rbp)
	leaq	-2403(%rbp), %rax
	movq	%rax, -2416(%rbp)
	leaq	-2339(%rbp), %rax
	movq	%rax, -2352(%rbp)
	leaq	-2275(%rbp), %rax
	movq	%rax, -2288(%rbp)
	leaq	-2211(%rbp), %rax
	movq	%rax, -2224(%rbp)
	leaq	-2147(%rbp), %rax
	movq	%rax, -2160(%rbp)
	leaq	-2083(%rbp), %rax
	movq	%rax, -2096(%rbp)
	leaq	-1907(%rbp), %rax
	movq	$0, -4(%rdx)
	movw	%r8w, -2468(%rbp)
	xorl	%r8d, %r8d
	movw	%r9w, -2404(%rbp)
	xorl	%r9d, %r9d
	movw	%r10w, -2340(%rbp)
	xorl	%r10d, %r10d
	movw	%r11w, -2276(%rbp)
	xorl	%r11d, %r11d
	movw	%r14w, -2148(%rbp)
	xorl	%r14d, %r14d
	movw	%r15w, -2084(%rbp)
	xorl	%r15d, %r15d
	movl	$0, -2424(%rbp)
	movl	$40, -2472(%rbp)
	movl	$0, -2360(%rbp)
	movl	$40, -2408(%rbp)
	movl	$0, -2296(%rbp)
	movl	$40, -2344(%rbp)
	movl	$0, -2232(%rbp)
	movl	$40, -2280(%rbp)
	movl	$0, -2168(%rbp)
	movl	$40, -2216(%rbp)
	movl	$0, -2104(%rbp)
	movl	$40, -2152(%rbp)
	movl	$0, -2040(%rbp)
	movl	$40, -2088(%rbp)
	movl	$0, -2000(%rbp)
	movups	%xmm0, -1996(%rbp)
	movq	%rax, -1920(%rbp)
	xorl	%eax, %eax
	movw	%ax, -1908(%rbp)
	leaq	-1843(%rbp), %rax
	movq	%rax, -1856(%rbp)
	xorl	%eax, %eax
	movw	%ax, -1844(%rbp)
	leaq	-1779(%rbp), %rax
	movq	%rax, -1792(%rbp)
	xorl	%eax, %eax
	movw	%ax, -1780(%rbp)
	leaq	-1715(%rbp), %rax
	movq	%rax, -1728(%rbp)
	xorl	%eax, %eax
	movw	%ax, -1716(%rbp)
	leaq	-1651(%rbp), %rax
	movq	%rax, -1664(%rbp)
	xorl	%eax, %eax
	movw	%ax, -1652(%rbp)
	leaq	-1587(%rbp), %rax
	movq	%rax, -1600(%rbp)
	leaq	-1523(%rbp), %rax
	movq	%rax, -1536(%rbp)
	leaq	-1459(%rbp), %rax
	movw	%cx, -1588(%rbp)
	movq	%r12, %rcx
	movw	%di, -1524(%rbp)
	movq	%r13, %rdi
	movups	%xmm0, -1980(%rbp)
	movups	%xmm0, -1964(%rbp)
	movups	%xmm0, -1948(%rbp)
	pcmpeqd	%xmm0, %xmm0
	movb	$0, -1928(%rbp)
	movl	$0, -1864(%rbp)
	movl	$40, -1912(%rbp)
	movl	$0, -1800(%rbp)
	movl	$40, -1848(%rbp)
	movl	$0, -1736(%rbp)
	movl	$40, -1784(%rbp)
	movl	$0, -1672(%rbp)
	movl	$40, -1720(%rbp)
	movl	$0, -1608(%rbp)
	movl	$40, -1656(%rbp)
	movl	$0, -1544(%rbp)
	movl	$40, -1592(%rbp)
	movl	$0, -1480(%rbp)
	movl	$40, -1528(%rbp)
	movq	%rax, -1472(%rbp)
	leaq	-1395(%rbp), %rax
	movq	%rax, -1408(%rbp)
	leaq	-1331(%rbp), %rax
	movq	%rax, -1344(%rbp)
	leaq	-1267(%rbp), %rax
	movq	%rax, -1280(%rbp)
	leaq	-1203(%rbp), %rax
	movq	%rax, -1216(%rbp)
	leaq	-1139(%rbp), %rax
	movq	%rax, -1152(%rbp)
	leaq	-1075(%rbp), %rax
	movq	%rax, -1088(%rbp)
	leaq	-1011(%rbp), %rax
	movq	%rax, -1024(%rbp)
	xorl	%eax, %eax
	movw	%ax, -1012(%rbp)
	leaq	-947(%rbp), %rax
	movq	%rax, -960(%rbp)
	xorl	%eax, %eax
	movw	%r8w, -1460(%rbp)
	movw	%r9w, -1396(%rbp)
	movw	%r10w, -1332(%rbp)
	movw	%r11w, -1268(%rbp)
	movl	$0, -1416(%rbp)
	movl	$40, -1464(%rbp)
	movl	$0, -1352(%rbp)
	movl	$40, -1400(%rbp)
	movl	$0, -1288(%rbp)
	movl	$40, -1336(%rbp)
	movl	$0, -1224(%rbp)
	movl	$40, -1272(%rbp)
	movl	$0, -1160(%rbp)
	movl	$40, -1208(%rbp)
	movw	%bx, -1204(%rbp)
	movl	$0, -1096(%rbp)
	movl	$40, -1144(%rbp)
	movw	%r14w, -1140(%rbp)
	movl	$0, -1032(%rbp)
	movl	$40, -1080(%rbp)
	movw	%r15w, -1076(%rbp)
	movl	$0, -968(%rbp)
	movl	$40, -1016(%rbp)
	movl	$0, -904(%rbp)
	movw	%ax, -948(%rbp)
	leaq	-883(%rbp), %rax
	movq	%rax, -896(%rbp)
	xorl	%eax, %eax
	movl	$40, -952(%rbp)
	movl	$0, -840(%rbp)
	movl	$40, -888(%rbp)
	movw	%ax, -884(%rbp)
	movaps	%xmm0, -2032(%rbp)
	movaps	%xmm0, -2016(%rbp)
	call	_ZL18ucol_sit_readSpecsP12CollatorSpecPKcP11UParseErrorP10UErrorCode.isra.0
	movl	-2040(%rbp), %eax
	testl	%eax, %eax
	je	.L385
.L282:
	leaq	-576(%rbp), %rbx
	xorl	%eax, %eax
	movl	$64, %ecx
	movl	$512, %edx
	movq	%rbx, %rdi
	movq	%rbx, %rsi
	leaq	-832(%rbp), %r15
	rep stosq
	movq	-2096(%rbp), %rdi
	movq	%r12, %rcx
	call	uloc_canonicalize_67@PLT
	movq	%r12, %rdx
	movq	%rbx, %rsi
	leaq	.LC1(%rip), %rdi
	call	ures_open_67@PLT
	xorl	%edx, %edx
	movq	%r12, %rcx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r14
	call	ures_getByKey_67@PLT
	movq	%r12, %r8
	movl	$256, %ecx
	movq	%r15, %rdx
	leaq	.LC3(%rip), %rsi
	movq	%rbx, %rdi
	movq	%rax, %r13
	call	uloc_getKeywordValue_67@PLT
	cmpl	$255, %eax
	jle	.L283
	movl	$0, (%r12)
.L284:
	movq	%r12, %rcx
	xorl	%edx, %edx
	leaq	.LC4(%rip), %rsi
	movq	%r13, %rdi
	call	ures_getByKeyWithFallback_67@PLT
	movq	%rax, %rbx
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L286
	leaq	-2564(%rbp), %rsi
	movq	%r12, %rdx
	movq	%rbx, %rdi
	movl	$0, -2564(%rbp)
	call	ures_getString_67@PLT
	movl	-2564(%rbp), %edx
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	u_UCharsToChars_67@PLT
	movslq	-2564(%rbp), %rax
	movq	%rbx, %rdi
	movb	$0, -832(%rbp,%rax)
	call	ures_close_67@PLT
.L285:
	xorl	%edx, %edx
	movq	%r12, %rcx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	ures_getByKeyWithFallback_67@PLT
	movq	%rax, %rdi
	call	ures_close_67@PLT
	movq	%r13, %rdi
	call	ures_close_67@PLT
	movq	%r14, %rdi
	call	ures_close_67@PLT
	cmpb	$0, -884(%rbp)
	jne	.L384
.L312:
	cmpb	$0, -948(%rbp)
	jne	.L386
.L313:
	cmpb	$0, -1012(%rbp)
	jne	.L387
.L314:
	cmpb	$0, -1076(%rbp)
	jne	.L388
.L315:
	cmpb	$0, -1140(%rbp)
	jne	.L389
.L316:
	cmpb	$0, -1204(%rbp)
	jne	.L390
.L317:
	cmpb	$0, -1268(%rbp)
	jne	.L391
.L318:
	cmpb	$0, -1332(%rbp)
	jne	.L392
.L319:
	cmpb	$0, -1396(%rbp)
	jne	.L393
.L320:
	cmpb	$0, -1460(%rbp)
	jne	.L394
.L321:
	cmpb	$0, -1524(%rbp)
	jne	.L395
.L322:
	cmpb	$0, -1588(%rbp)
	jne	.L396
.L323:
	cmpb	$0, -1652(%rbp)
	jne	.L397
.L324:
	cmpb	$0, -1716(%rbp)
	jne	.L398
.L325:
	cmpb	$0, -1780(%rbp)
	jne	.L399
.L326:
	cmpb	$0, -1844(%rbp)
	jne	.L400
.L327:
	cmpb	$0, -1908(%rbp)
	jne	.L401
.L328:
	cmpb	$0, -2084(%rbp)
	jne	.L402
.L329:
	cmpb	$0, -2148(%rbp)
	jne	.L403
.L330:
	cmpb	$0, -2212(%rbp)
	jne	.L404
.L331:
	cmpb	$0, -2276(%rbp)
	jne	.L405
	cmpb	$0, -2340(%rbp)
	jne	.L406
.L333:
	cmpb	$0, -2404(%rbp)
	jne	.L407
.L334:
	cmpb	$0, -2468(%rbp)
	jne	.L408
.L279:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L409
	addq	$2536, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L283:
	.cfi_restore_state
	testl	%eax, %eax
	jne	.L285
	jmp	.L284
	.p2align 4,,10
	.p2align 3
.L408:
	movq	-2480(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L279
	.p2align 4,,10
	.p2align 3
.L385:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZL29ucol_sit_calculateWholeLocaleP12CollatorSpecR10UErrorCode.part.0
	jmp	.L282
	.p2align 4,,10
	.p2align 3
.L405:
	movq	-2288(%rbp), %rdi
	call	uprv_free_67@PLT
	cmpb	$0, -2340(%rbp)
	je	.L333
.L406:
	movq	-2352(%rbp), %rdi
	call	uprv_free_67@PLT
	cmpb	$0, -2404(%rbp)
	je	.L334
.L407:
	movq	-2416(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L334
	.p2align 4,,10
	.p2align 3
.L404:
	movq	-2224(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L331
	.p2align 4,,10
	.p2align 3
.L403:
	movq	-2160(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L402:
	movq	-2096(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L401:
	movq	-1920(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L328
	.p2align 4,,10
	.p2align 3
.L400:
	movq	-1856(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L327
	.p2align 4,,10
	.p2align 3
.L399:
	movq	-1792(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L398:
	movq	-1728(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L325
	.p2align 4,,10
	.p2align 3
.L286:
	cmpb	$0, -884(%rbp)
	movl	$5, (%r12)
	je	.L312
.L384:
	movq	-896(%rbp), %rdi
	call	uprv_free_67@PLT
	cmpb	$0, -948(%rbp)
	je	.L313
.L386:
	movq	-960(%rbp), %rdi
	call	uprv_free_67@PLT
	cmpb	$0, -1012(%rbp)
	je	.L314
.L387:
	movq	-1024(%rbp), %rdi
	call	uprv_free_67@PLT
	cmpb	$0, -1076(%rbp)
	je	.L315
.L388:
	movq	-1088(%rbp), %rdi
	call	uprv_free_67@PLT
	cmpb	$0, -1140(%rbp)
	je	.L316
.L389:
	movq	-1152(%rbp), %rdi
	call	uprv_free_67@PLT
	cmpb	$0, -1204(%rbp)
	je	.L317
.L390:
	movq	-1216(%rbp), %rdi
	call	uprv_free_67@PLT
	cmpb	$0, -1268(%rbp)
	je	.L318
.L391:
	movq	-1280(%rbp), %rdi
	call	uprv_free_67@PLT
	cmpb	$0, -1332(%rbp)
	je	.L319
.L392:
	movq	-1344(%rbp), %rdi
	call	uprv_free_67@PLT
	cmpb	$0, -1396(%rbp)
	je	.L320
.L393:
	movq	-1408(%rbp), %rdi
	call	uprv_free_67@PLT
	cmpb	$0, -1460(%rbp)
	je	.L321
.L394:
	movq	-1472(%rbp), %rdi
	call	uprv_free_67@PLT
	cmpb	$0, -1524(%rbp)
	je	.L322
.L395:
	movq	-1536(%rbp), %rdi
	call	uprv_free_67@PLT
	cmpb	$0, -1588(%rbp)
	je	.L323
.L396:
	movq	-1600(%rbp), %rdi
	call	uprv_free_67@PLT
	cmpb	$0, -1652(%rbp)
	je	.L324
.L397:
	movq	-1664(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L324
.L409:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2542:
	.size	ucol_prepareShortStringOpen_67, .-ucol_prepareShortStringOpen_67
	.p2align 4
	.globl	ucol_openFromShortString_67
	.type	ucol_openFromShortString_67, @function
ucol_openFromShortString_67:
.LFB2546:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$2280, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -2312(%rbp)
	movl	(%rcx), %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L410
	testq	%rdx, %rdx
	movq	%rdx, %rbx
	leaq	-2304(%rbp), %rax
	movl	%esi, %r15d
	cmove	%rax, %rbx
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	xorl	%r8d, %r8d
	leaq	-2211(%rbp), %rax
	xorl	%r9d, %r9d
	xorl	%r10d, %r10d
	movl	$0, -2168(%rbp)
	movq	%rax, -2224(%rbp)
	xorl	%r11d, %r11d
	xorl	%r12d, %r12d
	movq	%rcx, %r14
	leaq	-2147(%rbp), %rax
	movw	%si, 8(%rbx)
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%rax, -2160(%rbp)
	leaq	-2083(%rbp), %rax
	xorl	%esi, %esi
	leaq	-2224(%rbp), %r13
	movq	%rax, -2096(%rbp)
	leaq	-2019(%rbp), %rax
	movq	%rax, -2032(%rbp)
	leaq	-1955(%rbp), %rax
	movq	%rax, -1968(%rbp)
	xorl	%eax, %eax
	movw	%ax, -1956(%rbp)
	leaq	-1891(%rbp), %rax
	movq	%rax, -1904(%rbp)
	xorl	%eax, %eax
	movw	%ax, -1892(%rbp)
	leaq	-1827(%rbp), %rax
	movq	%rax, -1840(%rbp)
	xorl	%eax, %eax
	movw	%ax, -1828(%rbp)
	leaq	-1651(%rbp), %rax
	movw	%r8w, 40(%rbx)
	xorl	%r8d, %r8d
	movw	%r9w, -2212(%rbp)
	xorl	%r9d, %r9d
	movw	%r10w, -2148(%rbp)
	xorl	%r10d, %r10d
	movw	%r11w, -2084(%rbp)
	xorl	%r11d, %r11d
	movw	%r12w, -2020(%rbp)
	xorl	%r12d, %r12d
	movq	$0, (%rbx)
	movl	$40, -2216(%rbp)
	movl	$0, -2104(%rbp)
	movl	$40, -2152(%rbp)
	movl	$0, -2040(%rbp)
	movl	$40, -2088(%rbp)
	movl	$0, -1976(%rbp)
	movl	$40, -2024(%rbp)
	movl	$0, -1912(%rbp)
	movl	$40, -1960(%rbp)
	movl	$0, -1848(%rbp)
	movl	$40, -1896(%rbp)
	movl	$0, -1784(%rbp)
	movl	$40, -1832(%rbp)
	movl	$0, -1744(%rbp)
	movups	%xmm0, -1740(%rbp)
	movq	%rax, -1664(%rbp)
	xorl	%eax, %eax
	movw	%ax, -1652(%rbp)
	leaq	-1587(%rbp), %rax
	movq	%rax, -1600(%rbp)
	xorl	%eax, %eax
	movw	%ax, -1588(%rbp)
	leaq	-1523(%rbp), %rax
	movq	%rax, -1536(%rbp)
	leaq	-1459(%rbp), %rax
	movq	%rax, -1472(%rbp)
	leaq	-1395(%rbp), %rax
	movq	%rax, -1408(%rbp)
	leaq	-1331(%rbp), %rax
	movq	%rax, -1344(%rbp)
	leaq	-1267(%rbp), %rax
	movq	%rax, -1280(%rbp)
	leaq	-1203(%rbp), %rax
	movw	%dx, -1524(%rbp)
	xorl	%edx, %edx
	movw	%cx, -1460(%rbp)
	xorl	%ecx, %ecx
	movw	%si, -1396(%rbp)
	movq	%rdi, %rsi
	movq	%r13, %rdi
	movw	%r8w, -1332(%rbp)
	movw	%r9w, -1268(%rbp)
	movups	%xmm0, -1724(%rbp)
	movups	%xmm0, -1708(%rbp)
	movups	%xmm0, -1692(%rbp)
	pcmpeqd	%xmm0, %xmm0
	movb	$0, -1672(%rbp)
	movl	$0, -1608(%rbp)
	movl	$40, -1656(%rbp)
	movl	$0, -1544(%rbp)
	movl	$40, -1592(%rbp)
	movl	$0, -1480(%rbp)
	movl	$40, -1528(%rbp)
	movl	$0, -1416(%rbp)
	movl	$40, -1464(%rbp)
	movl	$0, -1352(%rbp)
	movl	$40, -1400(%rbp)
	movl	$0, -1288(%rbp)
	movl	$40, -1336(%rbp)
	movl	$0, -1224(%rbp)
	movl	$40, -1272(%rbp)
	movq	%rax, -1216(%rbp)
	leaq	-1139(%rbp), %rax
	movq	%rax, -1152(%rbp)
	leaq	-1075(%rbp), %rax
	movq	%rax, -1088(%rbp)
	leaq	-1011(%rbp), %rax
	movq	%rax, -1024(%rbp)
	xorl	%eax, %eax
	movw	%ax, -1012(%rbp)
	leaq	-947(%rbp), %rax
	movq	%rax, -960(%rbp)
	xorl	%eax, %eax
	movw	%ax, -948(%rbp)
	leaq	-883(%rbp), %rax
	movq	%rax, -896(%rbp)
	xorl	%eax, %eax
	movw	%ax, -884(%rbp)
	leaq	-819(%rbp), %rax
	movq	%rax, -832(%rbp)
	xorl	%eax, %eax
	movw	%ax, -820(%rbp)
	leaq	-755(%rbp), %rax
	movq	%rax, -768(%rbp)
	xorl	%eax, %eax
	movw	%ax, -756(%rbp)
	leaq	-691(%rbp), %rax
	movw	%r10w, -1204(%rbp)
	movw	%r11w, -1140(%rbp)
	movq	%rax, -704(%rbp)
	leaq	-627(%rbp), %rax
	movl	$0, -1160(%rbp)
	movl	$40, -1208(%rbp)
	movl	$0, -1096(%rbp)
	movl	$40, -1144(%rbp)
	movl	$0, -1032(%rbp)
	movl	$40, -1080(%rbp)
	movw	%r12w, -1076(%rbp)
	movl	$0, -968(%rbp)
	movl	$40, -1016(%rbp)
	movl	$0, -904(%rbp)
	movl	$40, -952(%rbp)
	movl	$0, -840(%rbp)
	movl	$40, -888(%rbp)
	movl	$0, -776(%rbp)
	movl	$40, -824(%rbp)
	movl	$0, -712(%rbp)
	movl	$40, -760(%rbp)
	movl	$0, -648(%rbp)
	movw	%dx, -692(%rbp)
	leaq	4(%rbx), %rdx
	movw	%cx, -628(%rbp)
	movq	%r14, %rcx
	movl	$40, -696(%rbp)
	movq	%rax, -640(%rbp)
	movl	$0, -584(%rbp)
	movl	$40, -632(%rbp)
	movaps	%xmm0, -1776(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_ZL18ucol_sit_readSpecsP12CollatorSpecPKcP11UParseErrorP10UErrorCode.isra.0
	movl	-1784(%rbp), %esi
	movq	%rax, -2320(%rbp)
	testl	%esi, %esi
	je	.L460
.L413:
	leaq	-576(%rbp), %r12
	xorl	%eax, %eax
	movl	$64, %ecx
	movl	$512, %edx
	movq	%r12, %rdi
	movq	%r12, %rsi
	rep stosq
	movq	-1840(%rbp), %rdi
	movq	%r14, %rcx
	call	uloc_canonicalize_67@PLT
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	ucol_open_67@PLT
	testb	%r15b, %r15b
	movl	$0, %r15d
	movq	%rax, %r12
	jne	.L414
	.p2align 4,,10
	.p2align 3
.L420:
	cmpl	$-1, 448(%r13,%r15,4)
	je	.L415
	movq	%r14, %rdx
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	ucol_getAttribute_67@PLT
	movl	448(%r13,%r15,4), %edx
	cmpl	%edx, %eax
	je	.L418
	movq	%r14, %rcx
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	ucol_setAttribute_67@PLT
.L418:
	movl	(%r14), %ecx
	testl	%ecx, %ecx
	jg	.L417
.L415:
	addq	$1, %r15
	cmpq	$8, %r15
	jne	.L420
	cmpb	$0, -1672(%rbp)
	je	.L423
.L487:
	cmpw	$0, -1740(%rbp)
	je	.L424
	movl	-1676(%rbp), %edx
	leaq	-1740(%rbp), %rsi
	movq	%r14, %rcx
	movq	%r12, %rdi
	call	ucol_setVariableTop_67@PLT
.L423:
	movl	(%r14), %eax
	testl	%eax, %eax
	jg	.L461
.L422:
	cmpb	$0, -628(%rbp)
	jne	.L462
.L425:
	cmpb	$0, -692(%rbp)
	jne	.L463
.L426:
	cmpb	$0, -756(%rbp)
	jne	.L464
.L427:
	cmpb	$0, -820(%rbp)
	jne	.L465
.L428:
	cmpb	$0, -884(%rbp)
	jne	.L466
.L429:
	cmpb	$0, -948(%rbp)
	jne	.L467
.L430:
	cmpb	$0, -1012(%rbp)
	jne	.L468
.L431:
	cmpb	$0, -1076(%rbp)
	jne	.L469
.L432:
	cmpb	$0, -1140(%rbp)
	jne	.L470
.L433:
	cmpb	$0, -1204(%rbp)
	jne	.L471
.L434:
	cmpb	$0, -1268(%rbp)
	jne	.L472
.L435:
	cmpb	$0, -1332(%rbp)
	jne	.L473
.L436:
	cmpb	$0, -1396(%rbp)
	jne	.L474
.L437:
	cmpb	$0, -1460(%rbp)
	jne	.L475
.L438:
	cmpb	$0, -1524(%rbp)
	jne	.L476
.L439:
	cmpb	$0, -1588(%rbp)
	jne	.L477
.L440:
	cmpb	$0, -1652(%rbp)
	jne	.L478
.L441:
	cmpb	$0, -1828(%rbp)
	jne	.L479
.L442:
	cmpb	$0, -1892(%rbp)
	jne	.L480
.L443:
	cmpb	$0, -1956(%rbp)
	jne	.L481
.L444:
	cmpb	$0, -2020(%rbp)
	jne	.L482
.L445:
	cmpb	$0, -2084(%rbp)
	jne	.L483
.L446:
	cmpb	$0, -2148(%rbp)
	jne	.L484
.L447:
	cmpb	$0, -2212(%rbp)
	jne	.L485
.L410:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L486
	addq	$2280, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L414:
	.cfi_restore_state
	movl	448(%r13,%r15,4), %edx
	cmpl	$-1, %edx
	je	.L421
	movq	%r14, %rcx
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	ucol_setAttribute_67@PLT
	movl	(%r14), %edx
	testl	%edx, %edx
	jg	.L417
.L421:
	addq	$1, %r15
	cmpq	$8, %r15
	jne	.L414
	cmpb	$0, -1672(%rbp)
	je	.L423
	jmp	.L487
	.p2align 4,,10
	.p2align 3
.L461:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	ucol_close_67@PLT
	cmpb	$0, -628(%rbp)
	je	.L425
.L462:
	movq	-640(%rbp), %rdi
	call	uprv_free_67@PLT
	cmpb	$0, -692(%rbp)
	je	.L426
.L463:
	movq	-704(%rbp), %rdi
	call	uprv_free_67@PLT
	cmpb	$0, -756(%rbp)
	je	.L427
.L464:
	movq	-768(%rbp), %rdi
	call	uprv_free_67@PLT
	cmpb	$0, -820(%rbp)
	je	.L428
.L465:
	movq	-832(%rbp), %rdi
	call	uprv_free_67@PLT
	cmpb	$0, -884(%rbp)
	je	.L429
.L466:
	movq	-896(%rbp), %rdi
	call	uprv_free_67@PLT
	cmpb	$0, -948(%rbp)
	je	.L430
.L467:
	movq	-960(%rbp), %rdi
	call	uprv_free_67@PLT
	cmpb	$0, -1012(%rbp)
	je	.L431
.L468:
	movq	-1024(%rbp), %rdi
	call	uprv_free_67@PLT
	cmpb	$0, -1076(%rbp)
	je	.L432
.L469:
	movq	-1088(%rbp), %rdi
	call	uprv_free_67@PLT
	cmpb	$0, -1140(%rbp)
	je	.L433
.L470:
	movq	-1152(%rbp), %rdi
	call	uprv_free_67@PLT
	cmpb	$0, -1204(%rbp)
	je	.L434
.L471:
	movq	-1216(%rbp), %rdi
	call	uprv_free_67@PLT
	cmpb	$0, -1268(%rbp)
	je	.L435
.L472:
	movq	-1280(%rbp), %rdi
	call	uprv_free_67@PLT
	cmpb	$0, -1332(%rbp)
	je	.L436
.L473:
	movq	-1344(%rbp), %rdi
	call	uprv_free_67@PLT
	cmpb	$0, -1396(%rbp)
	je	.L437
.L474:
	movq	-1408(%rbp), %rdi
	call	uprv_free_67@PLT
	cmpb	$0, -1460(%rbp)
	je	.L438
.L475:
	movq	-1472(%rbp), %rdi
	call	uprv_free_67@PLT
	cmpb	$0, -1524(%rbp)
	je	.L439
.L476:
	movq	-1536(%rbp), %rdi
	call	uprv_free_67@PLT
	cmpb	$0, -1588(%rbp)
	je	.L440
.L477:
	movq	-1600(%rbp), %rdi
	call	uprv_free_67@PLT
	cmpb	$0, -1652(%rbp)
	je	.L441
.L478:
	movq	-1664(%rbp), %rdi
	call	uprv_free_67@PLT
	cmpb	$0, -1828(%rbp)
	je	.L442
.L479:
	movq	-1840(%rbp), %rdi
	call	uprv_free_67@PLT
	cmpb	$0, -1892(%rbp)
	je	.L443
.L480:
	movq	-1904(%rbp), %rdi
	call	uprv_free_67@PLT
	cmpb	$0, -1956(%rbp)
	je	.L444
.L481:
	movq	-1968(%rbp), %rdi
	call	uprv_free_67@PLT
	cmpb	$0, -2020(%rbp)
	je	.L445
.L482:
	movq	-2032(%rbp), %rdi
	call	uprv_free_67@PLT
	cmpb	$0, -2084(%rbp)
	je	.L446
.L483:
	movq	-2096(%rbp), %rdi
	call	uprv_free_67@PLT
	cmpb	$0, -2148(%rbp)
	je	.L447
.L484:
	movq	-2160(%rbp), %rdi
	call	uprv_free_67@PLT
	cmpb	$0, -2212(%rbp)
	je	.L410
.L485:
	movq	-2224(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L410
	.p2align 4,,10
	.p2align 3
.L460:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZL29ucol_sit_calculateWholeLocaleP12CollatorSpecR10UErrorCode.part.0
	jmp	.L413
	.p2align 4,,10
	.p2align 3
.L424:
	movl	-1744(%rbp), %esi
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	ucol_restoreVariableTop_67@PLT
	movl	(%r14), %eax
	testl	%eax, %eax
	jle	.L422
	jmp	.L461
	.p2align 4,,10
	.p2align 3
.L417:
	movq	-2320(%rbp), %rax
	subq	-2312(%rbp), %rax
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	movl	%eax, 4(%rbx)
	call	ucol_close_67@PLT
	jmp	.L422
.L486:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2546:
	.size	ucol_openFromShortString_67, .-ucol_openFromShortString_67
	.p2align 4
	.globl	ucol_getShortDefinitionString_67
	.type	ucol_getShortDefinitionString_67, @function
ucol_getShortDefinitionString_67:
.LFB2547:
	.cfi_startproc
	endbr64
	movl	(%r8), %eax
	testl	%eax, %eax
	jg	.L488
	testq	%rdi, %rdi
	je	.L492
	movq	(%rdi), %rax
	jmp	*288(%rax)
	.p2align 4,,10
	.p2align 3
.L492:
	movl	$1, (%r8)
.L488:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2547:
	.size	ucol_getShortDefinitionString_67, .-ucol_getShortDefinitionString_67
	.p2align 4
	.globl	ucol_normalizeShortDefinitionString_67
	.type	ucol_normalizeShortDefinitionString_67, @function
ucol_normalizeShortDefinitionString_67:
.LFB2548:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$1768, %rsp
	.cfi_offset 3, -56
	movl	(%r8), %r9d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r9d, %r9d
	jg	.L493
	movq	%rdi, %r9
	movq	%rsi, %r13
	movl	%edx, %ebx
	movq	%rcx, %r14
	movq	%r8, %r15
	testq	%rsi, %rsi
	je	.L495
	movq	%rdi, -1800(%rbp)
	movslq	%edx, %rdx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	memset@PLT
	movq	-1800(%rbp), %r9
.L495:
	testq	%r14, %r14
	pxor	%xmm0, %xmm0
	leaq	-1792(%rbp), %rax
	movl	$0, -1656(%rbp)
	cmove	%rax, %r14
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	xorl	%r8d, %r8d
	xorl	%r10d, %r10d
	xorl	%r11d, %r11d
	xorl	%r12d, %r12d
	movw	%cx, -1700(%rbp)
	leaq	-1699(%rbp), %rax
	movw	%si, -1636(%rbp)
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%rax, -1712(%rbp)
	leaq	-1635(%rbp), %rax
	xorl	%esi, %esi
	leaq	-1712(%rbp), %rdi
	movq	%rax, -1648(%rbp)
	leaq	-1571(%rbp), %rax
	movq	%rax, -1584(%rbp)
	leaq	-1507(%rbp), %rax
	movq	%rax, -1520(%rbp)
	leaq	-1443(%rbp), %rax
	movq	%rax, -1456(%rbp)
	leaq	-1379(%rbp), %rax
	movq	%rax, -1392(%rbp)
	leaq	-1315(%rbp), %rax
	movq	%rax, -1328(%rbp)
	xorl	%eax, %eax
	movw	%ax, -1316(%rbp)
	leaq	-1139(%rbp), %rax
	movw	%r8w, -1572(%rbp)
	xorl	%r8d, %r8d
	movw	%r10w, -1508(%rbp)
	xorl	%r10d, %r10d
	movw	%r11w, -1444(%rbp)
	xorl	%r11d, %r11d
	movw	%r12w, -1380(%rbp)
	xorl	%r12d, %r12d
	movups	%xmm0, -1228(%rbp)
	movups	%xmm0, -1212(%rbp)
	movups	%xmm0, -1196(%rbp)
	movl	$40, -1704(%rbp)
	movl	$0, -1592(%rbp)
	movl	$40, -1640(%rbp)
	movl	$0, -1528(%rbp)
	movl	$40, -1576(%rbp)
	movl	$0, -1464(%rbp)
	movl	$40, -1512(%rbp)
	movl	$0, -1400(%rbp)
	movl	$40, -1448(%rbp)
	movl	$0, -1336(%rbp)
	movl	$40, -1384(%rbp)
	movl	$0, -1272(%rbp)
	movl	$40, -1320(%rbp)
	movl	$0, -1232(%rbp)
	movups	%xmm0, -1180(%rbp)
	pcmpeqd	%xmm0, %xmm0
	movq	%rax, -1152(%rbp)
	xorl	%eax, %eax
	movw	%ax, -1140(%rbp)
	leaq	-1075(%rbp), %rax
	movq	%rax, -1088(%rbp)
	xorl	%eax, %eax
	movw	%ax, -1076(%rbp)
	leaq	-1011(%rbp), %rax
	movq	%rax, -1024(%rbp)
	xorl	%eax, %eax
	movw	%ax, -1012(%rbp)
	leaq	-947(%rbp), %rax
	movq	%rax, -960(%rbp)
	xorl	%eax, %eax
	movw	%ax, -948(%rbp)
	leaq	-883(%rbp), %rax
	movq	%rax, -896(%rbp)
	leaq	-819(%rbp), %rax
	movq	%rax, -832(%rbp)
	leaq	-755(%rbp), %rax
	movq	%rax, -768(%rbp)
	leaq	-691(%rbp), %rax
	movq	%rax, -704(%rbp)
	leaq	-627(%rbp), %rax
	movw	%dx, -884(%rbp)
	xorl	%edx, %edx
	movw	%cx, -820(%rbp)
	movq	%r15, %rcx
	movw	%si, -756(%rbp)
	movq	%r9, %rsi
	movb	$0, -1160(%rbp)
	movl	$0, -1096(%rbp)
	movl	$40, -1144(%rbp)
	movl	$0, -1032(%rbp)
	movl	$40, -1080(%rbp)
	movl	$0, -968(%rbp)
	movl	$40, -1016(%rbp)
	movl	$0, -904(%rbp)
	movl	$40, -952(%rbp)
	movl	$0, -840(%rbp)
	movl	$40, -888(%rbp)
	movl	$0, -776(%rbp)
	movl	$40, -824(%rbp)
	movl	$0, -712(%rbp)
	movl	$40, -760(%rbp)
	movl	$0, -648(%rbp)
	movl	$40, -696(%rbp)
	movw	%r8w, -692(%rbp)
	movq	%rax, -640(%rbp)
	leaq	-563(%rbp), %rax
	movq	%rax, -576(%rbp)
	leaq	-499(%rbp), %rax
	movq	%rax, -512(%rbp)
	leaq	-435(%rbp), %rax
	movq	%rax, -448(%rbp)
	xorl	%eax, %eax
	movw	%ax, -436(%rbp)
	leaq	-371(%rbp), %rax
	movq	%rax, -384(%rbp)
	xorl	%eax, %eax
	movw	%ax, -372(%rbp)
	leaq	-307(%rbp), %rax
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movw	%ax, -308(%rbp)
	leaq	-243(%rbp), %rax
	movq	%rax, -256(%rbp)
	xorl	%eax, %eax
	movw	%ax, -244(%rbp)
	leaq	-179(%rbp), %rax
	movq	%rax, -192(%rbp)
	xorl	%eax, %eax
	movw	%ax, -180(%rbp)
	leaq	-115(%rbp), %rax
	movw	%r10w, -628(%rbp)
	movw	%r11w, -564(%rbp)
	movw	%r12w, -500(%rbp)
	xorl	%r12d, %r12d
	movl	$0, -584(%rbp)
	movl	$40, -632(%rbp)
	movl	$0, -520(%rbp)
	movl	$40, -568(%rbp)
	movl	$0, -456(%rbp)
	movl	$40, -504(%rbp)
	movl	$0, -392(%rbp)
	movl	$40, -440(%rbp)
	movl	$0, -328(%rbp)
	movl	$40, -376(%rbp)
	movl	$0, -264(%rbp)
	movl	$40, -312(%rbp)
	movl	$0, -200(%rbp)
	movl	$40, -248(%rbp)
	movl	$0, -136(%rbp)
	movl	$40, -184(%rbp)
	movq	%rax, -128(%rbp)
	movw	%dx, -116(%rbp)
	leaq	4(%r14), %rdx
	movl	$0, -72(%rbp)
	movl	$40, -120(%rbp)
	movaps	%xmm0, -1264(%rbp)
	movaps	%xmm0, -1248(%rbp)
	call	_ZL18ucol_sit_readSpecsP12CollatorSpecPKcP11UParseErrorP10UErrorCode.isra.0
	movl	(%r15), %ecx
	testl	%ecx, %ecx
	jle	.L551
.L497:
	cmpb	$0, -116(%rbp)
	jne	.L552
	cmpb	$0, -180(%rbp)
	jne	.L553
.L511:
	cmpb	$0, -244(%rbp)
	jne	.L554
.L512:
	cmpb	$0, -308(%rbp)
	jne	.L555
.L513:
	cmpb	$0, -372(%rbp)
	jne	.L556
.L514:
	cmpb	$0, -436(%rbp)
	jne	.L557
.L515:
	cmpb	$0, -500(%rbp)
	jne	.L558
.L516:
	cmpb	$0, -564(%rbp)
	jne	.L559
.L517:
	cmpb	$0, -628(%rbp)
	jne	.L560
.L518:
	cmpb	$0, -692(%rbp)
	jne	.L561
.L519:
	cmpb	$0, -756(%rbp)
	jne	.L562
.L520:
	cmpb	$0, -820(%rbp)
	jne	.L563
.L521:
	cmpb	$0, -884(%rbp)
	jne	.L564
.L522:
	cmpb	$0, -948(%rbp)
	jne	.L565
.L523:
	cmpb	$0, -1012(%rbp)
	jne	.L566
.L524:
	cmpb	$0, -1076(%rbp)
	jne	.L567
.L525:
	cmpb	$0, -1140(%rbp)
	jne	.L568
.L526:
	cmpb	$0, -1316(%rbp)
	jne	.L569
.L527:
	cmpb	$0, -1380(%rbp)
	jne	.L570
.L528:
	cmpb	$0, -1444(%rbp)
	jne	.L571
.L529:
	cmpb	$0, -1508(%rbp)
	jne	.L572
.L530:
	cmpb	$0, -1572(%rbp)
	jne	.L573
.L531:
	cmpb	$0, -1636(%rbp)
	jne	.L574
.L532:
	cmpb	$0, -1700(%rbp)
	jne	.L575
.L493:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L576
	addq	$1768, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L551:
	.cfi_restore_state
	leaq	-64(%rbp), %rax
	leaq	-1152(%rbp), %r15
	movq	%rax, -1808(%rbp)
	jmp	.L509
	.p2align 4,,10
	.p2align 3
.L501:
	subl	$75, %eax
	cmpb	$1, %al
	jbe	.L504
	addl	%edx, %r12d
	cmpl	%r12d, %ebx
	jg	.L577
	.p2align 4,,10
	.p2align 3
.L498:
	addq	$64, %r15
	cmpq	%r15, -1808(%rbp)
	je	.L497
.L509:
	movslq	56(%r15), %rdx
	testl	%edx, %edx
	je	.L498
	testl	%r12d, %r12d
	je	.L499
	cmpl	%r12d, %ebx
	jg	.L578
.L500:
	addl	$1, %r12d
.L499:
	movq	(%r15), %rsi
	movzbl	(%rsi), %eax
	movl	%eax, %ecx
	andl	$-5, %ecx
	cmpb	$82, %cl
	jne	.L501
.L504:
	movslq	%r12d, %rax
	xorl	%r14d, %r14d
	movq	%rax, -1800(%rbp)
	testl	%edx, %edx
	jle	.L503
	.p2align 4,,10
	.p2align 3
.L502:
	leal	(%r12,%r14), %eax
	cmpl	%eax, %ebx
	jg	.L579
	addq	$1, %r14
	cmpl	%r14d, %edx
	jg	.L502
.L503:
	addl	%edx, %r12d
	jmp	.L498
	.p2align 4,,10
	.p2align 3
.L579:
	movq	(%r15), %rax
	movsbl	(%rax,%r14), %edi
	call	uprv_toupper_67@PLT
	movl	56(%r15), %edx
	movl	%eax, %r9d
	movq	-1800(%rbp), %rax
	addq	%r13, %rax
	movb	%r9b, (%rax,%r14)
	addq	$1, %r14
	cmpl	%r14d, %edx
	jg	.L502
	addl	%edx, %r12d
	jmp	.L498
	.p2align 4,,10
	.p2align 3
.L577:
	movq	%r13, %rdi
	call	strncat@PLT
	jmp	.L498
	.p2align 4,,10
	.p2align 3
.L578:
	movq	%r13, %rdi
	movl	%edx, -1800(%rbp)
	call	strlen@PLT
	movl	$95, %edx
	movw	%dx, 0(%r13,%rax)
	movslq	-1800(%rbp), %rdx
	jmp	.L500
	.p2align 4,,10
	.p2align 3
.L552:
	movq	-128(%rbp), %rdi
	call	uprv_free_67@PLT
	cmpb	$0, -180(%rbp)
	je	.L511
	jmp	.L553
	.p2align 4,,10
	.p2align 3
.L559:
	movq	-576(%rbp), %rdi
	call	uprv_free_67@PLT
	cmpb	$0, -628(%rbp)
	je	.L518
.L560:
	movq	-640(%rbp), %rdi
	call	uprv_free_67@PLT
	cmpb	$0, -692(%rbp)
	je	.L519
.L561:
	movq	-704(%rbp), %rdi
	call	uprv_free_67@PLT
	cmpb	$0, -756(%rbp)
	je	.L520
.L562:
	movq	-768(%rbp), %rdi
	call	uprv_free_67@PLT
	cmpb	$0, -820(%rbp)
	je	.L521
.L563:
	movq	-832(%rbp), %rdi
	call	uprv_free_67@PLT
	cmpb	$0, -884(%rbp)
	je	.L522
.L564:
	movq	-896(%rbp), %rdi
	call	uprv_free_67@PLT
	cmpb	$0, -948(%rbp)
	je	.L523
.L565:
	movq	-960(%rbp), %rdi
	call	uprv_free_67@PLT
	cmpb	$0, -1012(%rbp)
	je	.L524
.L566:
	movq	-1024(%rbp), %rdi
	call	uprv_free_67@PLT
	cmpb	$0, -1076(%rbp)
	je	.L525
	jmp	.L567
	.p2align 4,,10
	.p2align 3
.L555:
	movq	-320(%rbp), %rdi
	call	uprv_free_67@PLT
	cmpb	$0, -372(%rbp)
	je	.L514
.L556:
	movq	-384(%rbp), %rdi
	call	uprv_free_67@PLT
	cmpb	$0, -436(%rbp)
	je	.L515
.L557:
	movq	-448(%rbp), %rdi
	call	uprv_free_67@PLT
	cmpb	$0, -500(%rbp)
	je	.L516
.L558:
	movq	-512(%rbp), %rdi
	call	uprv_free_67@PLT
	cmpb	$0, -564(%rbp)
	je	.L517
	jmp	.L559
	.p2align 4,,10
	.p2align 3
.L553:
	movq	-192(%rbp), %rdi
	call	uprv_free_67@PLT
	cmpb	$0, -244(%rbp)
	je	.L512
.L554:
	movq	-256(%rbp), %rdi
	call	uprv_free_67@PLT
	cmpb	$0, -308(%rbp)
	je	.L513
	jmp	.L555
	.p2align 4,,10
	.p2align 3
.L575:
	movq	-1712(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L493
	.p2align 4,,10
	.p2align 3
.L567:
	movq	-1088(%rbp), %rdi
	call	uprv_free_67@PLT
	cmpb	$0, -1140(%rbp)
	je	.L526
.L568:
	movq	-1152(%rbp), %rdi
	call	uprv_free_67@PLT
	cmpb	$0, -1316(%rbp)
	je	.L527
.L569:
	movq	-1328(%rbp), %rdi
	call	uprv_free_67@PLT
	cmpb	$0, -1380(%rbp)
	je	.L528
.L570:
	movq	-1392(%rbp), %rdi
	call	uprv_free_67@PLT
	cmpb	$0, -1444(%rbp)
	je	.L529
.L571:
	movq	-1456(%rbp), %rdi
	call	uprv_free_67@PLT
	cmpb	$0, -1508(%rbp)
	je	.L530
.L572:
	movq	-1520(%rbp), %rdi
	call	uprv_free_67@PLT
	cmpb	$0, -1572(%rbp)
	je	.L531
.L573:
	movq	-1584(%rbp), %rdi
	call	uprv_free_67@PLT
	cmpb	$0, -1636(%rbp)
	je	.L532
.L574:
	movq	-1648(%rbp), %rdi
	call	uprv_free_67@PLT
	cmpb	$0, -1700(%rbp)
	je	.L493
	jmp	.L575
.L576:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2548:
	.size	ucol_normalizeShortDefinitionString_67, .-ucol_normalizeShortDefinitionString_67
	.p2align 4
	.globl	ucol_getContractions_67
	.type	ucol_getContractions_67, @function
ucol_getContractions_67:
.LFB2549:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L581
	movq	%rdx, %rbx
	testq	%rdi, %rdi
	je	.L585
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6717RuleBasedCollatorE(%rip), %rdx
	leaq	_ZTIN6icu_678CollatorE(%rip), %rsi
	call	__dynamic_cast@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L586
	movq	%rbx, %r8
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	call	_ZNK6icu_6717RuleBasedCollator36internalGetContractionsAndExpansionsEPNS_10UnicodeSetES2_aR10UErrorCode@PLT
.L581:
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uset_getItemCount_67@PLT
	.p2align 4,,10
	.p2align 3
.L585:
	.cfi_restore_state
	movl	$1, (%rdx)
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uset_getItemCount_67@PLT
	.p2align 4,,10
	.p2align 3
.L586:
	.cfi_restore_state
	movl	$16, (%rbx)
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	uset_getItemCount_67@PLT
	.cfi_endproc
.LFE2549:
	.size	ucol_getContractions_67, .-ucol_getContractions_67
	.p2align 4
	.globl	ucol_getContractionsAndExpansions_67
	.type	ucol_getContractionsAndExpansions_67, @function
ucol_getContractionsAndExpansions_67:
.LFB2550:
	.cfi_startproc
	endbr64
	movl	(%r8), %eax
	testl	%eax, %eax
	jg	.L592
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%r8, %rbx
	testq	%rdi, %rdi
	je	.L595
	movq	%rsi, %r13
	movq	%rdx, %r14
	leaq	_ZTIN6icu_678CollatorE(%rip), %rsi
	movl	%ecx, %r12d
	leaq	_ZTIN6icu_6717RuleBasedCollatorE(%rip), %rdx
	xorl	%ecx, %ecx
	call	__dynamic_cast@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L596
	movsbl	%r12b, %ecx
	movq	%rbx, %r8
	movq	%r14, %rdx
	popq	%rbx
	.cfi_restore 3
	movq	%r13, %rsi
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_6717RuleBasedCollator36internalGetContractionsAndExpansionsEPNS_10UnicodeSetES2_aR10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L592:
	ret
	.p2align 4,,10
	.p2align 3
.L595:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movl	$1, (%r8)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L596:
	.cfi_restore_state
	movl	$16, (%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2550:
	.size	ucol_getContractionsAndExpansions_67, .-ucol_getContractionsAndExpansions_67
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	_ZL7options, @object
	.size	_ZL7options, 408
_ZL7options:
	.byte	65
	.zero	7
	.quad	_processCollatorOption
	.long	1
	.zero	4
	.byte	66
	.zero	7
	.quad	_processVariableTop
	.long	1
	.zero	4
	.byte	67
	.zero	7
	.quad	_processCollatorOption
	.long	2
	.zero	4
	.byte	68
	.zero	7
	.quad	_processCollatorOption
	.long	7
	.zero	4
	.byte	69
	.zero	7
	.quad	_processCollatorOption
	.long	3
	.zero	4
	.byte	70
	.zero	7
	.quad	_processCollatorOption
	.long	0
	.zero	4
	.byte	72
	.zero	7
	.quad	_processCollatorOption
	.long	6
	.zero	4
	.byte	75
	.zero	7
	.quad	_processLocaleElement
	.long	4
	.zero	4
	.byte	76
	.zero	7
	.quad	_processLocaleElement
	.long	0
	.zero	4
	.byte	78
	.zero	7
	.quad	_processCollatorOption
	.long	4
	.zero	4
	.byte	82
	.zero	7
	.quad	_processLocaleElement
	.long	2
	.zero	4
	.byte	83
	.zero	7
	.quad	_processCollatorOption
	.long	5
	.zero	4
	.byte	84
	.zero	7
	.quad	_processVariableTop
	.long	0
	.zero	4
	.byte	86
	.zero	7
	.quad	_processLocaleElement
	.long	3
	.zero	4
	.byte	88
	.zero	7
	.quad	_processRFC3066Locale
	.long	0
	.zero	4
	.byte	90
	.zero	7
	.quad	_processLocaleElement
	.long	1
	.zero	4
	.byte	80
	.zero	7
	.quad	_processLocaleElement
	.long	5
	.zero	4
	.section	.rodata
	.align 32
	.type	_ZL11conversions, @object
	.size	_ZL11conversions, 96
_ZL11conversions:
	.byte	49
	.zero	3
	.long	0
	.byte	50
	.zero	3
	.long	1
	.byte	51
	.zero	3
	.long	2
	.byte	52
	.zero	3
	.long	3
	.byte	68
	.zero	3
	.long	-1
	.byte	73
	.zero	3
	.long	15
	.byte	76
	.zero	3
	.long	24
	.byte	78
	.zero	3
	.long	21
	.byte	79
	.zero	3
	.long	17
	.byte	83
	.zero	3
	.long	20
	.byte	85
	.zero	3
	.long	25
	.byte	88
	.zero	3
	.long	16
	.type	_ZL15providerKeyword, @object
	.size	_ZL15providerKeyword, 5
_ZL15providerKeyword:
	.string	"@sp="
	.align 8
	.type	_ZL16collationKeyword, @object
	.size	_ZL16collationKeyword, 12
_ZL16collationKeyword:
	.string	"@collation="
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
