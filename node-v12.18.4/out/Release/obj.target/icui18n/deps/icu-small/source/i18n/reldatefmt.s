	.file	"reldatefmt.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725RelativeDateTimeCacheDataD2Ev
	.type	_ZN6icu_6725RelativeDateTimeCacheDataD2Ev, @function
_ZN6icu_6725RelativeDateTimeCacheDataD2Ev:
.LFB4485:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6725RelativeDateTimeCacheDataE(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	1440(%rdi), %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -64(%rbp)
	movq	%rdx, (%rdi)
	movq	$0, -56(%rbp)
.L6:
	movq	-64(%rbp), %r12
	addq	-56(%rbp), %r12
	.p2align 4,,10
	.p2align 3
.L5:
	movq	%r12, %r15
	movl	$6, %ebx
	.p2align 4,,10
	.p2align 3
.L4:
	movq	17304(%r15), %r14
	testq	%r14, %r14
	je	.L2
	movq	%r14, %rdi
	call	_ZN6icu_6715SimpleFormatterD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L2:
	movq	17352(%r15), %r14
	testq	%r14, %r14
	je	.L3
	movq	%r14, %rdi
	call	_ZN6icu_6715SimpleFormatterD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L3:
	addq	$8, %r15
	subl	$1, %ebx
	jne	.L4
	addq	$96, %r12
	cmpq	%r13, %r12
	jne	.L5
	addq	$1440, -56(%rbp)
	movq	-56(%rbp), %rax
	leaq	1440(%r12), %r13
	cmpq	$4320, %rax
	jne	.L6
	movq	-64(%rbp), %rax
	movq	21704(%rax), %r12
	testq	%r12, %r12
	je	.L7
	movq	%r12, %rdi
	call	_ZN6icu_6715SimpleFormatterD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L7:
	movq	-64(%rbp), %rbx
	leaq	21624(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%rbx, %rax
	leaq	17240(%rbx), %rbx
	leaq	-40(%rax), %r12
	.p2align 4,,10
	.p2align 3
.L8:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	subq	$64, %rbx
	call	*(%rax)
	cmpq	%r12, %rbx
	jne	.L8
	movq	-64(%rbp), %rdi
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6712SharedObjectD2Ev@PLT
	.cfi_endproc
.LFE4485:
	.size	_ZN6icu_6725RelativeDateTimeCacheDataD2Ev, .-_ZN6icu_6725RelativeDateTimeCacheDataD2Ev
	.globl	_ZN6icu_6725RelativeDateTimeCacheDataD1Ev
	.set	_ZN6icu_6725RelativeDateTimeCacheDataD1Ev,_ZN6icu_6725RelativeDateTimeCacheDataD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725RelativeDateTimeCacheDataD0Ev
	.type	_ZN6icu_6725RelativeDateTimeCacheDataD0Ev, @function
_ZN6icu_6725RelativeDateTimeCacheDataD0Ev:
.LFB4487:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN6icu_6725RelativeDateTimeCacheDataD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE4487:
	.size	_ZN6icu_6725RelativeDateTimeCacheDataD0Ev, .-_ZN6icu_6725RelativeDateTimeCacheDataD0Ev
	.align 2
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_122RelDateTimeFmtDataSinkD2Ev, @function
_ZN6icu_6712_GLOBAL__N_122RelDateTimeFmtDataSinkD2Ev:
.LFB4511:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_122RelDateTimeFmtDataSinkE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6712ResourceSinkD2Ev@PLT
	.cfi_endproc
.LFE4511:
	.size	_ZN6icu_6712_GLOBAL__N_122RelDateTimeFmtDataSinkD2Ev, .-_ZN6icu_6712_GLOBAL__N_122RelDateTimeFmtDataSinkD2Ev
	.set	_ZN6icu_6712_GLOBAL__N_122RelDateTimeFmtDataSinkD1Ev,_ZN6icu_6712_GLOBAL__N_122RelDateTimeFmtDataSinkD2Ev
	.align 2
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_122RelDateTimeFmtDataSinkD0Ev, @function
_ZN6icu_6712_GLOBAL__N_122RelDateTimeFmtDataSinkD0Ev:
.LFB4513:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_122RelDateTimeFmtDataSinkE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6712ResourceSinkD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE4513:
	.size	_ZN6icu_6712_GLOBAL__N_122RelDateTimeFmtDataSinkD0Ev, .-_ZN6icu_6712_GLOBAL__N_122RelDateTimeFmtDataSinkD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6729FormattedRelativeDateTimeDataD2Ev
	.type	_ZN6icu_6729FormattedRelativeDateTimeDataD2Ev, @function
_ZN6icu_6729FormattedRelativeDateTimeDataD2Ev:
.LFB4524:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6729FormattedRelativeDateTimeDataE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6731FormattedValueStringBuilderImplD2Ev@PLT
	.cfi_endproc
.LFE4524:
	.size	_ZN6icu_6729FormattedRelativeDateTimeDataD2Ev, .-_ZN6icu_6729FormattedRelativeDateTimeDataD2Ev
	.globl	_ZN6icu_6729FormattedRelativeDateTimeDataD1Ev
	.set	_ZN6icu_6729FormattedRelativeDateTimeDataD1Ev,_ZN6icu_6729FormattedRelativeDateTimeDataD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6729FormattedRelativeDateTimeDataD0Ev
	.type	_ZN6icu_6729FormattedRelativeDateTimeDataD0Ev, @function
_ZN6icu_6729FormattedRelativeDateTimeDataD0Ev:
.LFB4526:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6729FormattedRelativeDateTimeDataE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6731FormattedValueStringBuilderImplD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE4526:
	.size	_ZN6icu_6729FormattedRelativeDateTimeDataD0Ev, .-_ZN6icu_6729FormattedRelativeDateTimeDataD0Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6725FormattedRelativeDateTime8toStringER10UErrorCode
	.type	_ZNK6icu_6725FormattedRelativeDateTime8toStringER10UErrorCode, @function
_ZNK6icu_6725FormattedRelativeDateTime8toStringER10UErrorCode:
.LFB4535:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movl	(%rdx), %ecx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L38
	movq	8(%rsi), %r8
	testq	%r8, %r8
	je	.L39
	movq	(%r8), %rax
	movq	%r8, %rsi
	call	*16(%rax)
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L39:
	movl	16(%rsi), %eax
	movl	%eax, (%rdx)
	.p2align 4,,10
	.p2align 3
.L38:
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	movl	$2, %eax
	movw	%ax, 8(%r12)
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
.L32:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L40
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L40:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4535:
	.size	_ZNK6icu_6725FormattedRelativeDateTime8toStringER10UErrorCode, .-_ZNK6icu_6725FormattedRelativeDateTime8toStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6725FormattedRelativeDateTime12toTempStringER10UErrorCode
	.type	_ZNK6icu_6725FormattedRelativeDateTime12toTempStringER10UErrorCode, @function
_ZNK6icu_6725FormattedRelativeDateTime12toTempStringER10UErrorCode:
.LFB4536:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movl	(%rdx), %ecx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L47
	movq	8(%rsi), %r8
	testq	%r8, %r8
	je	.L48
	movq	(%r8), %rax
	movq	%r8, %rsi
	call	*24(%rax)
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L48:
	movl	16(%rsi), %eax
	movl	%eax, (%rdx)
	.p2align 4,,10
	.p2align 3
.L47:
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	movl	$2, %eax
	movw	%ax, 8(%r12)
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
.L41:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L49
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L49:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4536:
	.size	_ZNK6icu_6725FormattedRelativeDateTime12toTempStringER10UErrorCode, .-_ZNK6icu_6725FormattedRelativeDateTime12toTempStringER10UErrorCode
	.section	.text._ZNK6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEE5cloneEv,"axG",@progbits,_ZNK6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEE5cloneEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEE5cloneEv
	.type	_ZNK6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEE5cloneEv, @function
_ZNK6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEE5cloneEv:
.LFB6441:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	$240, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L50
	movl	8(%rbx), %eax
	movb	$0, 12(%r12)
	leaq	16(%rbx), %rsi
	leaq	16(%r12), %rdi
	movl	%eax, 8(%r12)
	leaq	16+_ZTVN6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEEE(%rip), %rax
	movq	%rax, (%r12)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
.L50:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6441:
	.size	_ZNK6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEE5cloneEv, .-_ZNK6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEE5cloneEv
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6725RelativeDateTimeFormatter10formatImplEd14UDateDirection17UDateRelativeUnitRNS_29FormattedRelativeDateTimeDataER10UErrorCode
	.type	_ZNK6icu_6725RelativeDateTimeFormatter10formatImplEd14UDateDirection17UDateRelativeUnitRNS_29FormattedRelativeDateTimeDataER10UErrorCode, @function
_ZNK6icu_6725RelativeDateTimeFormatter10formatImplEd14UDateDirection17UDateRelativeUnitRNS_29FormattedRelativeDateTimeDataER10UErrorCode:
.LFB4566:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r8), %r9d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r9d, %r9d
	jg	.L56
	movl	%esi, %eax
	movl	%esi, %r14d
	movq	%r8, %rbx
	andl	$-3, %eax
	cmpl	$1, %eax
	jne	.L70
	movq	24(%rdi), %rax
	leaq	8(%rcx), %r12
	movq	%rdi, %r13
	movl	%edx, %r15d
	leaq	-164(%rbp), %rcx
	movq	%r12, %rdx
	movq	24(%rax), %rsi
	movq	16(%rdi), %rax
	movq	24(%rax), %rdi
	call	_ZN6icu_6717QuantityFormatter15formatAndSelectEdRKNS_12NumberFormatERKNS_11PluralRulesERNS_22FormattedStringBuilderERNS_14StandardPlural4FormER10UErrorCode@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L56
	cmpl	$6, %r15d
	ja	.L61
	cmpl	$3, %r14d
	movl	%r15d, %edx
	movq	8(%r13), %rcx
	movl	32(%r13), %r10d
	leaq	CSWTCH.304(%rip), %rax
	movl	-164(%rbp), %r9d
	movslq	(%rax,%rdx,4), %rax
	sete	%dl
	movzbl	%dl, %edx
	leaq	(%rax,%rax,2), %rsi
	leaq	(%rdx,%rdx), %rax
	addq	%rdx, %rax
	addq	%rax, %rax
	leaq	(%rax,%rsi,4), %r8
.L64:
	movslq	%r10d, %rax
	movslq	%r9d, %rdi
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L72:
	movslq	21688(%rcx,%rax,4), %rax
	cmpl	$-1, %eax
	je	.L71
.L63:
	imulq	$180, %rax, %rdx
	addq	%r8, %rdx
	leaq	2162(%rdx,%rdi), %rdx
	movq	8(%rcx,%rdx,8), %rsi
	testq	%rsi, %rsi
	je	.L72
	movzbl	_ZN6icu_67L16kRDTLiteralFieldE(%rip), %edx
	leaq	-160(%rbp), %r13
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl14SimpleModifierC1ERKNS_15SimpleFormatterENS_22FormattedStringBuilder5FieldEb@PLT
	movq	%r12, %rdi
	call	_ZNK6icu_6722FormattedStringBuilder6lengthEv@PLT
	movq	%rbx, %r8
	xorl	%edx, %edx
	movq	%r12, %rsi
	movl	%eax, %ecx
	movq	%r13, %rdi
	call	_ZNK6icu_676number4impl14SimpleModifier20formatAsPrefixSuffixERNS_22FormattedStringBuilderEiiR10UErrorCode@PLT
	leaq	16+_ZTVN6icu_676number4impl14SimpleModifierE(%rip), %rax
	leaq	-152(%rbp), %rdi
	movq	%rax, -160(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L56:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L73
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L70:
	.cfi_restore_state
	movl	$1, (%r8)
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L61:
	movl	$3, (%rbx)
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L71:
	cmpl	$5, %r9d
	je	.L61
	movl	$5, %r9d
	jmp	.L64
.L73:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4566:
	.size	_ZNK6icu_6725RelativeDateTimeFormatter10formatImplEd14UDateDirection17UDateRelativeUnitRNS_29FormattedRelativeDateTimeDataER10UErrorCode, .-_ZNK6icu_6725RelativeDateTimeFormatter10formatImplEd14UDateDirection17UDateRelativeUnitRNS_29FormattedRelativeDateTimeDataER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6725RelativeDateTimeFormatter17formatNumericImplEd21URelativeDateTimeUnitRNS_29FormattedRelativeDateTimeDataER10UErrorCode
	.type	_ZNK6icu_6725RelativeDateTimeFormatter17formatNumericImplEd21URelativeDateTimeUnitRNS_29FormattedRelativeDateTimeDataER10UErrorCode, @function
_ZNK6icu_6725RelativeDateTimeFormatter17formatNumericImplEd21URelativeDateTimeUnitRNS_29FormattedRelativeDateTimeDataER10UErrorCode:
.LFB4569:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rcx), %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L74
	movmskpd	%xmm0, %eax
	movq	%rdi, %r13
	movslq	%esi, %r14
	movq	%rcx, %rbx
	movl	$1, %r15d
	testb	$1, %al
	jne	.L88
.L76:
	movq	24(%r13), %rax
	leaq	8(%rdx), %r12
	leaq	-164(%rbp), %rcx
	movq	%rbx, %r8
	movq	%r12, %rdx
	movq	24(%rax), %rsi
	movq	16(%r13), %rax
	movq	24(%rax), %rdi
	call	_ZN6icu_6717QuantityFormatter15formatAndSelectEdRKNS_12NumberFormatERKNS_11PluralRulesERNS_22FormattedStringBuilderERNS_14StandardPlural4FormER10UErrorCode@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L74
	leaq	(%r15,%r15), %rax
	leaq	(%r14,%r14,2), %rdx
	movq	8(%r13), %rcx
	movl	32(%r13), %r10d
	addq	%rax, %r15
	movl	-164(%rbp), %r9d
	addq	%r15, %r15
	leaq	(%r15,%rdx,4), %r8
.L80:
	movslq	%r10d, %rax
	movslq	%r9d, %rdi
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L90:
	movslq	21688(%rcx,%rax,4), %rax
	cmpl	$-1, %eax
	je	.L89
.L78:
	imulq	$180, %rax, %rdx
	addq	%r8, %rdx
	leaq	2162(%rdx,%rdi), %rdx
	movq	8(%rcx,%rdx,8), %rsi
	testq	%rsi, %rsi
	je	.L90
	movzbl	_ZN6icu_67L16kRDTLiteralFieldE(%rip), %edx
	leaq	-160(%rbp), %r13
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl14SimpleModifierC1ERKNS_15SimpleFormatterENS_22FormattedStringBuilder5FieldEb@PLT
	movq	%r12, %rdi
	call	_ZNK6icu_6722FormattedStringBuilder6lengthEv@PLT
	movq	%rbx, %r8
	xorl	%edx, %edx
	movq	%r12, %rsi
	movl	%eax, %ecx
	movq	%r13, %rdi
	call	_ZNK6icu_676number4impl14SimpleModifier20formatAsPrefixSuffixERNS_22FormattedStringBuilderEiiR10UErrorCode@PLT
	leaq	16+_ZTVN6icu_676number4impl14SimpleModifierE(%rip), %rax
	leaq	-152(%rbp), %rdi
	movq	%rax, -160(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L74:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L91
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L88:
	.cfi_restore_state
	xorpd	.LC0(%rip), %xmm0
	xorl	%r15d, %r15d
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L89:
	cmpl	$5, %r9d
	je	.L79
	movl	$5, %r9d
	jmp	.L80
.L79:
	movl	$3, (%rbx)
	jmp	.L74
.L91:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4569:
	.size	_ZNK6icu_6725RelativeDateTimeFormatter17formatNumericImplEd21URelativeDateTimeUnitRNS_29FormattedRelativeDateTimeDataER10UErrorCode, .-_ZNK6icu_6725RelativeDateTimeFormatter17formatNumericImplEd21URelativeDateTimeUnitRNS_29FormattedRelativeDateTimeDataER10UErrorCode
	.section	.text._ZNK6icu_678CacheKeyINS_25RelativeDateTimeCacheDataEE16writeDescriptionEPci,"axG",@progbits,_ZNK6icu_678CacheKeyINS_25RelativeDateTimeCacheDataEE16writeDescriptionEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_678CacheKeyINS_25RelativeDateTimeCacheDataEE16writeDescriptionEPci
	.type	_ZNK6icu_678CacheKeyINS_25RelativeDateTimeCacheDataEE16writeDescriptionEPci, @function
_ZNK6icu_678CacheKeyINS_25RelativeDateTimeCacheDataEE16writeDescriptionEPci:
.LFB6446:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	leaq	_ZTSN6icu_6725RelativeDateTimeCacheDataE(%rip), %rsi
	movq	%r8, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movslq	%edx, %rbx
	movq	%rbx, %rdx
	subq	$8, %rsp
	call	strncpy@PLT
	movb	$0, -1(%rax,%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6446:
	.size	_ZNK6icu_678CacheKeyINS_25RelativeDateTimeCacheDataEE16writeDescriptionEPci, .-_ZNK6icu_678CacheKeyINS_25RelativeDateTimeCacheDataEE16writeDescriptionEPci
	.section	.text._ZNK6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEE16writeDescriptionEPci,"axG",@progbits,_ZNK6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEE16writeDescriptionEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEE16writeDescriptionEPci
	.type	_ZNK6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEE16writeDescriptionEPci, @function
_ZNK6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEE16writeDescriptionEPci:
.LFB6443:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movslq	%edx, %rbx
	movq	%rbx, %rdx
	subq	$8, %rsp
	movq	56(%rdi), %rsi
	movq	%r8, %rdi
	call	strncpy@PLT
	movb	$0, -1(%rax,%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6443:
	.size	_ZNK6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEE16writeDescriptionEPci, .-_ZNK6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEE16writeDescriptionEPci
	.section	.text._ZNK6icu_678CacheKeyINS_25RelativeDateTimeCacheDataEEeqERKNS_12CacheKeyBaseE,"axG",@progbits,_ZNK6icu_678CacheKeyINS_25RelativeDateTimeCacheDataEEeqERKNS_12CacheKeyBaseE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_678CacheKeyINS_25RelativeDateTimeCacheDataEEeqERKNS_12CacheKeyBaseE
	.type	_ZNK6icu_678CacheKeyINS_25RelativeDateTimeCacheDataEEeqERKNS_12CacheKeyBaseE, @function
_ZNK6icu_678CacheKeyINS_25RelativeDateTimeCacheDataEEeqERKNS_12CacheKeyBaseE:
.LFB6445:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	-8(%rax), %rax
	movq	8(%rax), %rdi
	movq	(%rsi), %rax
	movq	-8(%rax), %rax
	movq	8(%rax), %rsi
	movl	$1, %eax
	cmpq	%rsi, %rdi
	je	.L101
	xorl	%eax, %eax
	cmpb	$42, (%rdi)
	je	.L101
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	strcmp@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L101:
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE6445:
	.size	_ZNK6icu_678CacheKeyINS_25RelativeDateTimeCacheDataEEeqERKNS_12CacheKeyBaseE, .-_ZNK6icu_678CacheKeyINS_25RelativeDateTimeCacheDataEEeqERKNS_12CacheKeyBaseE
	.section	.text._ZNK6icu_678CacheKeyINS_25RelativeDateTimeCacheDataEE8hashCodeEv,"axG",@progbits,_ZNK6icu_678CacheKeyINS_25RelativeDateTimeCacheDataEE8hashCodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_678CacheKeyINS_25RelativeDateTimeCacheDataEE8hashCodeEv
	.type	_ZNK6icu_678CacheKeyINS_25RelativeDateTimeCacheDataEE8hashCodeEv, @function
_ZNK6icu_678CacheKeyINS_25RelativeDateTimeCacheDataEE8hashCodeEv:
.LFB6444:
	.cfi_startproc
	endbr64
	movl	$36, %esi
	leaq	_ZTSN6icu_6725RelativeDateTimeCacheDataE(%rip), %rdi
	jmp	ustr_hashCharsN_67@PLT
	.cfi_endproc
.LFE6444:
	.size	_ZNK6icu_678CacheKeyINS_25RelativeDateTimeCacheDataEE8hashCodeEv, .-_ZNK6icu_678CacheKeyINS_25RelativeDateTimeCacheDataEE8hashCodeEv
	.section	.text._ZNK6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEE8hashCodeEv,"axG",@progbits,_ZNK6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEE8hashCodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEE8hashCodeEv
	.type	_ZNK6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEE8hashCodeEv, @function
_ZNK6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEE8hashCodeEv:
.LFB6440:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$36, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	_ZTSN6icu_6725RelativeDateTimeCacheDataE(%rip), %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	call	ustr_hashCharsN_67@PLT
	leaq	16(%r12), %rdi
	movl	%eax, %ebx
	call	_ZNK6icu_676Locale8hashCodeEv@PLT
	movl	%eax, %r8d
	leal	(%rbx,%rbx,8), %eax
	leal	(%rbx,%rax,4), %eax
	popq	%rbx
	popq	%r12
	addl	%r8d, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6440:
	.size	_ZNK6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEE8hashCodeEv, .-_ZNK6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEE8hashCodeEv
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6725FormattedRelativeDateTime8appendToERNS_10AppendableER10UErrorCode
	.type	_ZNK6icu_6725FormattedRelativeDateTime8appendToERNS_10AppendableER10UErrorCode, @function
_ZNK6icu_6725FormattedRelativeDateTime8appendToERNS_10AppendableER10UErrorCode:
.LFB4537:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L107
	movq	8(%rdi), %r8
	testq	%r8, %r8
	je	.L111
	movq	(%r8), %rax
	movq	%r8, %rdi
	movq	32(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L111:
	movl	16(%rdi), %eax
	movl	%eax, (%rdx)
.L107:
	movq	%rsi, %rax
	ret
	.cfi_endproc
.LFE4537:
	.size	_ZNK6icu_6725FormattedRelativeDateTime8appendToERNS_10AppendableER10UErrorCode, .-_ZNK6icu_6725FormattedRelativeDateTime8appendToERNS_10AppendableER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6725FormattedRelativeDateTime12nextPositionERNS_24ConstrainedFieldPositionER10UErrorCode
	.type	_ZNK6icu_6725FormattedRelativeDateTime12nextPositionERNS_24ConstrainedFieldPositionER10UErrorCode, @function
_ZNK6icu_6725FormattedRelativeDateTime12nextPositionERNS_24ConstrainedFieldPositionER10UErrorCode:
.LFB4538:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L112
	movq	8(%rdi), %r8
	testq	%r8, %r8
	je	.L116
	movq	(%r8), %rax
	movq	%r8, %rdi
	movq	40(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L116:
	movl	16(%rdi), %eax
	movl	%eax, (%rdx)
.L112:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4538:
	.size	_ZNK6icu_6725FormattedRelativeDateTime12nextPositionERNS_24ConstrainedFieldPositionER10UErrorCode, .-_ZNK6icu_6725FormattedRelativeDateTime12nextPositionERNS_24ConstrainedFieldPositionER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6725RelativeDateTimeFormatter18formatAbsoluteImplE14UDateDirection17UDateAbsoluteUnitRNS_29FormattedRelativeDateTimeDataER10UErrorCode
	.type	_ZNK6icu_6725RelativeDateTimeFormatter18formatAbsoluteImplE14UDateDirection17UDateAbsoluteUnitRNS_29FormattedRelativeDateTimeDataER10UErrorCode, @function
_ZNK6icu_6725RelativeDateTimeFormatter18formatAbsoluteImplE14UDateDirection17UDateAbsoluteUnitRNS_29FormattedRelativeDateTimeDataER10UErrorCode:
.LFB4572:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	movl	(%r8), %edi
	testl	%edi, %edi
	jg	.L117
	cmpl	$11, %edx
	jne	.L119
	cmpl	$5, %esi
	je	.L119
	movl	$1, (%r8)
	ret
	.p2align 4,,10
	.p2align 3
.L117:
	ret
	.p2align 4,,10
	.p2align 3
.L119:
	movslq	%edx, %rdx
	movslq	%esi, %rsi
	movq	8(%rax), %r10
	leaq	8(%rcx), %rdi
	leaq	(%rdx,%rdx,2), %rdx
	movl	32(%rax), %eax
	leaq	(%rsi,%rdx,2), %rsi
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L130:
	movl	21688(%r10,%rdx,4), %eax
	cmpl	$-1, %eax
	je	.L129
.L121:
	movslq	%eax, %rdx
	imulq	$90, %rdx, %rax
	addq	%rsi, %rax
	salq	$6, %rax
	leaq	24(%r10,%rax), %r9
	movswl	8(%r9), %eax
	shrl	$5, %eax
	je	.L130
.L120:
	movl	140(%rcx), %esi
	movzbl	_ZN6icu_67L16kRDTLiteralFieldE(%rip), %ecx
	movq	%r9, %rdx
	jmp	_ZN6icu_6722FormattedStringBuilder6insertEiRKNS_13UnicodeStringENS0_5FieldER10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L129:
	leaq	21624(%r10), %r9
	jmp	.L120
	.cfi_endproc
.LFE4572:
	.size	_ZNK6icu_6725RelativeDateTimeFormatter18formatAbsoluteImplE14UDateDirection17UDateAbsoluteUnitRNS_29FormattedRelativeDateTimeDataER10UErrorCode, .-_ZNK6icu_6725RelativeDateTimeFormatter18formatAbsoluteImplE14UDateDirection17UDateAbsoluteUnitRNS_29FormattedRelativeDateTimeDataER10UErrorCode
	.section	.text._ZNK6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEEeqERKNS_12CacheKeyBaseE,"axG",@progbits,_ZNK6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEEeqERKNS_12CacheKeyBaseE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEEeqERKNS_12CacheKeyBaseE
	.type	_ZNK6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEEeqERKNS_12CacheKeyBaseE, @function
_ZNK6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEEeqERKNS_12CacheKeyBaseE:
.LFB6442:
	.cfi_startproc
	endbr64
	cmpq	%rsi, %rdi
	je	.L134
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	movq	-8(%rax), %rax
	movq	8(%rax), %rdi
	movq	(%rsi), %rax
	movq	-8(%rax), %rax
	movq	8(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L133
	cmpb	$42, (%rdi)
	je	.L136
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L136
.L133:
	leaq	16(%r12), %rsi
	leaq	16(%rbx), %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_676LocaleeqERKS0_@PLT
	.p2align 4,,10
	.p2align 3
.L136:
	.cfi_restore_state
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L134:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE6442:
	.size	_ZNK6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEEeqERKNS_12CacheKeyBaseE, .-_ZNK6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEEeqERKNS_12CacheKeyBaseE
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725FormattedRelativeDateTimeD2Ev
	.type	_ZN6icu_6725FormattedRelativeDateTimeD2Ev, @function
_ZN6icu_6725FormattedRelativeDateTimeD2Ev:
.LFB4531:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6725FormattedRelativeDateTimeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	8(%rdi), %r13
	movq	%rdi, %r12
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L142
	movq	0(%r13), %rax
	leaq	_ZN6icu_6729FormattedRelativeDateTimeDataD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L143
	leaq	16+_ZTVN6icu_6729FormattedRelativeDateTimeDataE(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, 0(%r13)
	call	_ZN6icu_6731FormattedValueStringBuilderImplD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L142:
	movq	$0, 8(%r12)
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6714FormattedValueD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L143:
	.cfi_restore_state
	movq	%r13, %rdi
	call	*%rax
	jmp	.L142
	.cfi_endproc
.LFE4531:
	.size	_ZN6icu_6725FormattedRelativeDateTimeD2Ev, .-_ZN6icu_6725FormattedRelativeDateTimeD2Ev
	.globl	_ZN6icu_6725FormattedRelativeDateTimeD1Ev
	.set	_ZN6icu_6725FormattedRelativeDateTimeD1Ev,_ZN6icu_6725FormattedRelativeDateTimeD2Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6725RelativeDateTimeFormatter18formatRelativeImplEd21URelativeDateTimeUnitRNS_29FormattedRelativeDateTimeDataER10UErrorCode
	.type	_ZNK6icu_6725RelativeDateTimeFormatter18formatRelativeImplEd21URelativeDateTimeUnitRNS_29FormattedRelativeDateTimeDataER10UErrorCode, @function
_ZNK6icu_6725RelativeDateTimeFormatter18formatRelativeImplEd21URelativeDateTimeUnitRNS_29FormattedRelativeDateTimeDataER10UErrorCode:
.LFB4575:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L224
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	comisd	.LC1(%rip), %xmm0
	jbe	.L151
	movsd	.LC2(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jbe	.L151
	movsd	.LC3(%rip), %xmm1
	pxor	%xmm2, %xmm2
	mulsd	%xmm0, %xmm1
	comisd	%xmm1, %xmm2
	ja	.L227
	addsd	.LC5(%rip), %xmm1
	cvttsd2sil	%xmm1, %eax
.L156:
	testl	%eax, %eax
	je	.L157
	jg	.L158
	cmpl	$-200, %eax
	je	.L159
	cmpl	$-100, %eax
	jne	.L151
	movl	$1, %esi
	movl	$1, %edx
.L160:
	cmpl	$14, %r12d
	ja	.L151
	leaq	.L208(%rip), %rdi
	movl	%r12d, %ecx
	movslq	(%rdi,%rcx,4), %rax
	addq	%rdi, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L208:
	.long	.L200-.L208
	.long	.L216-.L208
	.long	.L164-.L208
	.long	.L167-.L208
	.long	.L169-.L208
	.long	.L193-.L208
	.long	.L217-.L208
	.long	.L151-.L208
	.long	.L173-.L208
	.long	.L176-.L208
	.long	.L179-.L208
	.long	.L182-.L208
	.long	.L185-.L208
	.long	.L188-.L208
	.long	.L191-.L208
	.text
.L151:
	addq	$24, %rsp
	movq	%r15, %rcx
	movq	%r13, %rdx
	movl	%r12d, %esi
	popq	%rbx
	.cfi_restore 3
	movq	%r14, %rdi
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_6725RelativeDateTimeFormatter17formatNumericImplEd21URelativeDateTimeUnitRNS_29FormattedRelativeDateTimeDataER10UErrorCode
	.p2align 4,,10
	.p2align 3
.L224:
	ret
	.p2align 4,,10
	.p2align 3
.L157:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	cmpl	$14, %r12d
	ja	.L151
	leaq	.L202(%rip), %rcx
	movl	%r12d, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L202:
	.long	.L207-.L202
	.long	.L206-.L202
	.long	.L205-.L202
	.long	.L204-.L202
	.long	.L213-.L202
	.long	.L203-.L202
	.long	.L214-.L202
	.long	.L215-.L202
	.long	.L170-.L202
	.long	.L174-.L202
	.long	.L177-.L202
	.long	.L180-.L202
	.long	.L183-.L202
	.long	.L186-.L202
	.long	.L189-.L202
	.text
.L215:
	movl	$5, %esi
	movl	$66, %eax
.L195:
	movq	8(%r14), %rcx
	movslq	32(%r14), %rdx
	leaq	8(%r13), %rbx
	addq	%rax, %rsi
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L229:
	movslq	21688(%rcx,%rdx,4), %rdx
	cmpl	$-1, %edx
	je	.L228
.L197:
	imulq	$90, %rdx, %rax
	addq	%rsi, %rax
	salq	$6, %rax
	leaq	24(%rcx,%rax), %r9
	movswl	8(%r9), %eax
	shrl	$5, %eax
	je	.L229
.L196:
	movl	140(%r13), %esi
	movq	%r15, %r8
	movq	%r9, %rdx
	movq	%rbx, %rdi
	movzbl	_ZN6icu_67L16kRDTLiteralFieldE(%rip), %ecx
	movsd	%xmm0, -56(%rbp)
	call	_ZN6icu_6722FormattedStringBuilder6insertEiRKNS_13UnicodeStringENS0_5FieldER10UErrorCode@PLT
	movq	%rbx, %rdi
	call	_ZNK6icu_6722FormattedStringBuilder6lengthEv@PLT
	movsd	-56(%rbp), %xmm0
	testl	%eax, %eax
	je	.L151
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L214:
	.cfi_restore_state
	movl	$2, %edx
	movl	$84, %eax
	movl	%edx, %esi
	jmp	.L195
.L213:
	movl	$42, %eax
.L171:
	movl	$2, %esi
	jmp	.L195
.L205:
	movl	$54, %eax
	jmp	.L171
.L204:
	movl	$48, %eax
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L159:
	cmpl	$14, %r12d
	ja	.L151
	leaq	.L201(%rip), %rcx
	movl	%r12d, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L201:
	.long	.L194-.L201
	.long	.L212-.L201
	.long	.L162-.L201
	.long	.L166-.L201
	.long	.L168-.L201
	.long	.L192-.L201
	.long	.L199-.L201
	.long	.L151-.L201
	.long	.L172-.L201
	.long	.L175-.L201
	.long	.L178-.L201
	.long	.L181-.L201
	.long	.L184-.L201
	.long	.L187-.L201
	.long	.L190-.L201
	.text
.L190:
	xorl	%edx, %edx
	movl	$36, %eax
	movl	%edx, %esi
	jmp	.L195
.L187:
	xorl	%edx, %edx
	movl	$30, %eax
	movl	%edx, %esi
	jmp	.L195
.L184:
	xorl	%edx, %edx
	movl	$24, %eax
	movl	%edx, %esi
	jmp	.L195
.L181:
	xorl	%edx, %edx
	movl	$18, %eax
	movl	%edx, %esi
	jmp	.L195
.L178:
	xorl	%edx, %edx
	movl	$12, %eax
	movl	%edx, %esi
	jmp	.L195
.L175:
	xorl	%edx, %edx
	movl	$6, %eax
	movl	%edx, %esi
	jmp	.L195
.L172:
	xorl	%edx, %edx
	xorl	%eax, %eax
	movl	%edx, %esi
	jmp	.L195
.L199:
	xorl	%edx, %edx
	movl	$84, %eax
	movl	%edx, %esi
	jmp	.L195
.L192:
	xorl	%edx, %edx
	movl	$78, %eax
	movl	%edx, %esi
	jmp	.L195
.L168:
	xorl	%edx, %edx
	movl	$42, %eax
	movl	%edx, %esi
	jmp	.L195
.L166:
	xorl	%edx, %edx
	movl	$48, %eax
	movl	%edx, %esi
	jmp	.L195
.L162:
	xorl	%edx, %edx
	movl	$54, %eax
	movl	%edx, %esi
	jmp	.L195
.L212:
	xorl	%edx, %edx
	movl	$72, %eax
	movl	%edx, %esi
	jmp	.L195
.L194:
	movl	%r12d, %edx
	movl	$60, %eax
	movl	%edx, %esi
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L158:
	cmpl	$100, %eax
	je	.L210
	cmpl	$200, %eax
	jne	.L151
	movl	$4, %esi
	movl	$4, %edx
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L227:
	subsd	.LC5(%rip), %xmm1
	cvttsd2sil	%xmm1, %eax
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L210:
	movl	$3, %esi
	movl	$3, %edx
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L228:
	leaq	21624(%rcx), %r9
	jmp	.L196
.L186:
	movl	$30, %eax
	jmp	.L171
.L183:
	movl	$24, %eax
	jmp	.L171
.L180:
	movl	$18, %eax
	jmp	.L171
.L177:
	movl	$12, %eax
	jmp	.L171
.L174:
	movl	$6, %eax
	jmp	.L171
.L170:
	xorl	%eax, %eax
	jmp	.L171
.L203:
	movl	$78, %eax
	jmp	.L171
.L206:
	movl	$72, %eax
	jmp	.L171
.L207:
	movl	$60, %eax
	jmp	.L171
.L189:
	movl	$36, %eax
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L191:
	movl	$36, %eax
	movl	%edx, %esi
	jmp	.L195
.L188:
	movl	$30, %eax
	movl	%edx, %esi
	jmp	.L195
.L185:
	movl	$24, %eax
	movl	%edx, %esi
	jmp	.L195
.L182:
	movl	$18, %eax
	movl	%edx, %esi
	jmp	.L195
.L179:
	movl	$12, %eax
	movl	%edx, %esi
	jmp	.L195
.L176:
	movl	$6, %eax
	movl	%edx, %esi
	jmp	.L195
.L173:
	xorl	%eax, %eax
	movl	%edx, %esi
	jmp	.L195
.L193:
	movl	$78, %eax
	movl	%edx, %esi
	jmp	.L195
.L169:
	movl	$42, %eax
	movl	%edx, %esi
	jmp	.L195
.L167:
	movl	$48, %eax
	movl	%edx, %esi
	jmp	.L195
.L164:
	movl	$54, %eax
	movl	%edx, %esi
	jmp	.L195
.L217:
	movl	$84, %eax
	jmp	.L195
.L216:
	movl	$72, %eax
	movl	%edx, %esi
	jmp	.L195
.L200:
	movl	$60, %eax
	movl	%edx, %esi
	jmp	.L195
	.cfi_endproc
.LFE4575:
	.size	_ZNK6icu_6725RelativeDateTimeFormatter18formatRelativeImplEd21URelativeDateTimeUnitRNS_29FormattedRelativeDateTimeDataER10UErrorCode, .-_ZNK6icu_6725RelativeDateTimeFormatter18formatRelativeImplEd21URelativeDateTimeUnitRNS_29FormattedRelativeDateTimeDataER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725FormattedRelativeDateTimeD0Ev
	.type	_ZN6icu_6725FormattedRelativeDateTimeD0Ev, @function
_ZN6icu_6725FormattedRelativeDateTimeD0Ev:
.LFB4533:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6725FormattedRelativeDateTimeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	8(%rdi), %r13
	movq	%rdi, %r12
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L231
	movq	0(%r13), %rax
	leaq	_ZN6icu_6729FormattedRelativeDateTimeDataD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L232
	leaq	16+_ZTVN6icu_6729FormattedRelativeDateTimeDataE(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, 0(%r13)
	call	_ZN6icu_6731FormattedValueStringBuilderImplD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L231:
	movq	$0, 8(%r12)
	movq	%r12, %rdi
	call	_ZN6icu_6714FormattedValueD2Ev@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L232:
	.cfi_restore_state
	movq	%r13, %rdi
	call	*%rax
	jmp	.L231
	.cfi_endproc
.LFE4533:
	.size	_ZN6icu_6725FormattedRelativeDateTimeD0Ev, .-_ZN6icu_6725FormattedRelativeDateTimeD0Ev
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC6:
	.string	"-narrow"
.LC7:
	.string	"-short"
.LC8:
	.string	"hour"
.LC9:
	.string	"week"
.LC10:
	.string	"year"
.LC11:
	.string	"month"
.LC12:
	.string	"minute"
.LC13:
	.string	"second"
.LC14:
	.string	"quarter"
.LC15:
	.string	"relative"
.LC16:
	.string	"relativeTime"
.LC17:
	.string	"past"
.LC18:
	.string	"future"
	.text
	.align 2
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_122RelDateTimeFmtDataSink3putEPKcRNS_13ResourceValueEaR10UErrorCode, @function
_ZN6icu_6712_GLOBAL__N_122RelDateTimeFmtDataSink3putEPKcRNS_13ResourceValueEaR10UErrorCode:
.LFB4509:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$520, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -440(%rbp)
	movq	%r13, %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-384(%rbp), %rax
	movq	%rax, -480(%rbp)
	movq	%rax, %rbx
	movq	(%rdx), %rax
	movq	%r8, %rdx
	movq	%rbx, %rdi
	call	*88(%rax)
	movl	(%r14), %r10d
	leaq	-440(%rbp), %rax
	movq	%rax, -472(%rbp)
	testl	%r10d, %r10d
	jg	.L237
	movq	%r14, -448(%rbp)
	xorl	%r15d, %r15d
	movq	%r13, %r14
	movq	%r12, %r13
	.p2align 4,,10
	.p2align 3
.L238:
	movq	-472(%rbp), %rdx
	movq	-480(%rbp), %rdi
	movq	%r14, %rcx
	movl	%r15d, %esi
	call	_ZNK6icu_6713ResourceTable14getKeyAndValueEiRPKcRNS_13ResourceValueE@PLT
	testb	%al, %al
	je	.L237
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*24(%rax)
	movq	-440(%rbp), %r12
	movq	%r12, %rdi
	cmpl	$3, %eax
	je	.L387
	call	strlen@PLT
	cmpl	$6, %eax
	jle	.L254
	cltq
	movl	$8, %ecx
	leaq	.LC6(%rip), %rdi
	leaq	-7(%r12,%rax), %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L388
.L255:
	leaq	-6(%r12,%rax), %rsi
	movl	$7, %ecx
	leaq	.LC7(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L389
.L257:
	movl	$0, 12(%r13)
	movq	%r12, %rdi
	call	strlen@PLT
.L256:
	subl	$3, %eax
	cmpl	$4, %eax
	ja	.L259
	leaq	.L261(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L261:
	.long	.L265-.L261
	.long	.L264-.L261
	.long	.L263-.L261
	.long	.L262-.L261
	.long	.L260-.L261
	.text
	.p2align 4,,10
	.p2align 3
.L262:
	movl	$6, %ecx
	leaq	.LC12(%rip), %rdi
	movq	%r12, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L287
	movl	$6, %ecx
	leaq	.LC13(%rip), %rdi
	movq	%r12, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L259
	movl	$0, 16(%r13)
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L263:
	movl	$5, %ecx
	leaq	.LC11(%rip), %rdi
	movq	%r12, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L259
	movl	$5, 16(%r13)
	.p2align 4,,10
	.p2align 3
.L289:
	leaq	-336(%rbp), %rax
	movq	-448(%rbp), %rbx
	movq	%r12, -416(%rbp)
	movq	%r14, %rsi
	movq	%rax, -464(%rbp)
	movq	%rax, %rdi
	movq	(%r14), %rax
	xorl	%r12d, %r12d
	movq	%rbx, %rdx
	call	*88(%rax)
	movl	(%rbx), %r8d
	leaq	-416(%rbp), %rax
	movq	%rax, -456(%rbp)
	testl	%r8d, %r8d
	jg	.L253
	movq	%r13, -488(%rbp)
	movq	%r14, %r13
	movl	%r15d, -492(%rbp)
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L299:
	addl	$1, %r12d
.L290:
	movq	-456(%rbp), %rdx
	movq	-464(%rbp), %rdi
	movq	%r13, %rcx
	movl	%r12d, %esi
	call	_ZNK6icu_6713ResourceTable14getKeyAndValueEiRPKcRNS_13ResourceValueE@PLT
	testb	%al, %al
	je	.L379
	movq	-416(%rbp), %rdx
	movzbl	(%rdx), %eax
	subl	$100, %eax
	jne	.L292
	movzbl	1(%rdx), %eax
	subl	$110, %eax
	jne	.L292
	movzbl	2(%rdx), %eax
.L292:
	movq	0(%r13), %rdx
	movq	24(%rdx), %rdx
	testl	%eax, %eax
	jne	.L293
	movq	%r13, %rdi
	call	*%rdx
	testl	%eax, %eax
	je	.L294
.L386:
	movq	0(%r13), %rax
	movq	24(%rax), %rdx
.L293:
	movq	%r13, %rdi
	call	*%rdx
	cmpl	$2, %eax
	jne	.L299
	movq	-416(%rbp), %rax
	movl	$9, %ecx
	leaq	.LC15(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%r15b
	sbbb	$0, %r15b
	movsbl	%r15b, %r15d
	testl	%r15d, %r15d
	je	.L390
	movl	$13, %ecx
	leaq	.LC16(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	jne	.L299
	movq	%rax, -408(%rbp)
	leaq	-288(%rbp), %rax
	movq	-448(%rbp), %rbx
	movq	%r13, %rsi
	movq	%rax, -504(%rbp)
	movq	%rax, %rdi
	movq	0(%r13), %rax
	movq	%rbx, %rdx
	call	*88(%rax)
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L299
	movq	-488(%rbp), %rax
	movl	16(%rax), %eax
	cmpl	$14, %eax
	ja	.L299
	leaq	CSWTCH.175(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	testl	%eax, %eax
	js	.L299
	leaq	(%rax,%rax,2), %rax
	movl	%r12d, -496(%rbp)
	xorl	%r15d, %r15d
	salq	$5, %rax
	movq	%rax, -528(%rbp)
	leaq	-408(%rbp), %rax
	movq	%rax, -512(%rbp)
	.p2align 4,,10
	.p2align 3
.L324:
	movq	-512(%rbp), %rdx
	movq	-504(%rbp), %rdi
	movq	%r13, %rcx
	movl	%r15d, %esi
	call	_ZNK6icu_6713ResourceTable14getKeyAndValueEiRPKcRNS_13ResourceValueE@PLT
	testb	%al, %al
	je	.L381
	movq	-408(%rbp), %rdx
	movl	$5, %ecx
	leaq	.LC17(%rip), %rdi
	movq	%rdx, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L315
	movq	-488(%rbp), %rax
	movl	$0, 8(%rax)
.L316:
	movq	0(%r13), %rax
	leaq	-240(%rbp), %r12
	movq	%r13, %rsi
	xorl	%ebx, %ebx
	movq	%rdx, -400(%rbp)
	movq	%r12, %rdi
	movq	-448(%rbp), %rdx
	leaq	-400(%rbp), %r14
	call	*88(%rax)
	movq	-448(%rbp), %rax
	movl	(%rax), %edx
	testl	%edx, %edx
	jg	.L317
	movl	%r15d, -520(%rbp)
	movq	%r13, %r15
	movq	-488(%rbp), %r13
	jmp	.L318
	.p2align 4,,10
	.p2align 3
.L321:
	addl	$1, %ebx
.L318:
	movq	%r15, %rcx
	movq	%r14, %rdx
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_6713ResourceTable14getKeyAndValueEiRPKcRNS_13ResourceValueE@PLT
	testb	%al, %al
	je	.L382
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*24(%rax)
	testl	%eax, %eax
	jne	.L321
	movq	-400(%rbp), %rdi
	call	_ZN6icu_6714StandardPlural25indexOrNegativeFromStringEPKc@PLT
	testl	%eax, %eax
	js	.L321
	movslq	8(%r13), %rdx
	cltq
	leaq	(%rdx,%rdx,2), %rcx
	movslq	12(%r13), %rdx
	salq	$4, %rcx
	imulq	$1440, %rdx, %rdx
	leaq	17304(%rcx,%rdx), %rdx
	addq	-528(%rbp), %rdx
	leaq	(%rdx,%rax,8), %r10
	addq	24(%r13), %r10
	cmpq	$0, (%r10)
	jne	.L321
	movq	(%r15), %rax
	movq	%r10, -544(%rbp)
	movq	%r15, %rdi
	leaq	-420(%rbp), %rsi
	movq	-448(%rbp), %rdx
	movl	$0, -420(%rbp)
	call	*32(%rax)
	leaq	-128(%rbp), %r11
	movl	-420(%rbp), %ecx
	leaq	-392(%rbp), %rdx
	movl	$1, %esi
	movq	%r11, %rdi
	movq	%rax, -392(%rbp)
	movq	%r11, -536(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movl	$72, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	-536(%rbp), %r11
	movq	-544(%rbp), %r10
	testq	%rax, %rax
	movq	%rax, %rdi
	je	.L323
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%r11, %rsi
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%rax, 8(%rdi)
	movl	$2, %eax
	movq	-448(%rbp), %r8
	movw	%ax, 16(%rdi)
	movq	%r10, -552(%rbp)
	movq	%r11, -544(%rbp)
	movq	%rdi, -536(%rbp)
	call	_ZN6icu_6715SimpleFormatter27applyPatternMinMaxArgumentsERKNS_13UnicodeStringEiiR10UErrorCode@PLT
	movq	-552(%rbp), %r10
	movq	-544(%rbp), %r11
	movq	-536(%rbp), %rdi
.L323:
	movq	%rdi, (%r10)
	movq	%r11, %rdi
	movq	%r10, -536(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-536(%rbp), %r10
	cmpq	$0, (%r10)
	jne	.L321
	movq	-448(%rbp), %rax
	movl	$7, (%rax)
	jmp	.L321
	.p2align 4,,10
	.p2align 3
.L264:
	movl	$4, %ecx
	leaq	.LC8(%rip), %rdi
	movq	%r12, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L283
	movl	$4, %ecx
	leaq	.LC9(%rip), %rdi
	movq	%r12, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L284
	movl	$4, %ecx
	leaq	.LC10(%rip), %rdi
	movq	%r12, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L259
	movl	$7, 16(%r13)
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L260:
	movl	$7, %ecx
	leaq	.LC14(%rip), %rdi
	movq	%r12, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L259
	movl	$6, 16(%r13)
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L265:
	movzbl	(%r12), %eax
	cmpl	$100, %eax
	je	.L391
.L336:
	cmpl	$115, %eax
	jne	.L337
	cmpb	$117, 1(%r12)
	jne	.L337
	cmpb	$110, 2(%r12)
	jne	.L337
	movl	$8, 16(%r13)
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L387:
	call	strlen@PLT
	cmpl	$6, %eax
	jg	.L392
	movl	$0, %ebx
	je	.L393
.L242:
	movq	(%r14), %rax
	movq	-448(%rbp), %rdx
	movq	%r14, %rdi
	leaq	-288(%rbp), %rsi
	movl	$0, -288(%rbp)
	leaq	-192(%rbp), %r12
	call	*40(%rax)
	movl	-288(%rbp), %ecx
	leaq	-240(%rbp), %rdx
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, -240(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	-448(%rbp), %rax
	movl	(%rax), %r9d
	testl	%r9d, %r9d
	jle	.L394
.L244:
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L253:
	addl	$1, %r15d
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L254:
	jne	.L257
	movl	$6, %eax
	jmp	.L255
	.p2align 4,,10
	.p2align 3
.L237:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L395
	addq	$520, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L294:
	.cfi_restore_state
	movq	-448(%rbp), %rax
	movq	0(%r13), %rcx
	movl	(%rax), %edi
	testl	%edi, %edi
	jg	.L385
	movq	-488(%rbp), %rax
	movl	16(%rax), %eax
	movl	%eax, -504(%rbp)
	subl	$1, %eax
	cmpl	$13, %eax
	ja	.L385
	leaq	CSWTCH.177(%rip), %rbx
	movslq	(%rbx,%rax,4), %rax
	testl	%eax, %eax
	js	.L385
	movq	-488(%rbp), %rbx
	leaq	(%rax,%rax,2), %rax
	salq	$7, %rax
	movslq	12(%rbx), %rdx
	imulq	$5760, %rdx, %rdx
	leaq	344(%rdx,%rax), %r14
	addq	24(%rbx), %r14
	movswl	8(%r14), %eax
	shrl	$5, %eax
	je	.L298
	.p2align 4,,10
	.p2align 3
.L385:
	movq	24(%rcx), %rdx
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L337:
	cmpl	$109, %eax
	je	.L396
.L338:
	cmpl	$116, %eax
	je	.L397
.L339:
	cmpl	$119, %eax
	je	.L398
.L340:
	cmpl	$116, %eax
	jne	.L341
	cmpb	$104, 1(%r12)
	je	.L399
.L341:
	cmpl	$102, %eax
	jne	.L342
	cmpb	$114, 1(%r12)
	je	.L400
.L342:
	cmpl	$115, %eax
	jne	.L259
	cmpb	$97, 1(%r12)
	je	.L401
	.p2align 4,,10
	.p2align 3
.L259:
	movl	$-1, 16(%r13)
	addl	$1, %r15d
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L379:
	movl	-492(%rbp), %r15d
	movq	%r13, %r14
	movq	-488(%rbp), %r13
	addl	$1, %r15d
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L389:
	movl	$1, 12(%r13)
	movq	%r12, %rdi
	call	strlen@PLT
	subl	$6, %eax
	jmp	.L256
	.p2align 4,,10
	.p2align 3
.L390:
	movq	%rax, -392(%rbp)
	movq	0(%r13), %rax
	leaq	-240(%rbp), %rbx
	movq	%r13, %rsi
	movq	-448(%rbp), %rdx
	movq	%rbx, %rdi
	leaq	-392(%rbp), %r14
	call	*88(%rax)
	movq	-448(%rbp), %rax
	movl	(%rax), %esi
	testl	%esi, %esi
	jg	.L299
	leaq	-400(%rbp), %rax
	movl	%r12d, -504(%rbp)
	movq	-488(%rbp), %r12
	movq	%rax, -512(%rbp)
	leaq	-288(%rbp), %rax
	movq	%rax, -520(%rbp)
	.p2align 4,,10
	.p2align 3
.L301:
	movq	%r13, %rcx
	movq	%r14, %rdx
	movl	%r15d, %esi
	movq	%rbx, %rdi
	call	_ZNK6icu_6713ResourceTable14getKeyAndValueEiRPKcRNS_13ResourceValueE@PLT
	testb	%al, %al
	je	.L380
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	testl	%eax, %eax
	jne	.L304
	movq	-392(%rbp), %rdx
	movzbl	(%rdx), %eax
	cmpl	$45, %eax
	je	.L402
.L344:
	movzbl	(%rdx), %eax
	cmpl	$48, %eax
	jne	.L345
	cmpb	$0, 1(%rdx)
	movl	$2, %r9d
	jne	.L345
	.p2align 4,,10
	.p2align 3
.L306:
	movl	16(%r12), %eax
	cmpl	$14, %eax
	ja	.L312
	movl	%eax, %ecx
	leaq	CSWTCH.175(%rip), %rsi
	cmpl	$7, (%rsi,%rcx,4)
	je	.L403
.L312:
	subl	$1, %eax
	cmpl	$13, %eax
	ja	.L304
	leaq	CSWTCH.177(%rip), %rcx
	movslq	(%rcx,%rax,4), %rax
	testl	%eax, %eax
	js	.L304
	movslq	12(%r12), %rdx
	leaq	(%rax,%rax,2), %rax
	imulq	$90, %rdx, %rdx
	addq	%rdx, %r9
	movq	24(%r12), %rdx
	leaq	(%r9,%rax,2), %rax
	salq	$6, %rax
	leaq	24(%rdx,%rax), %r9
	movswl	8(%r9), %eax
	shrl	$5, %eax
	je	.L404
	.p2align 4,,10
	.p2align 3
.L304:
	addl	$1, %r15d
	jmp	.L301
	.p2align 4,,10
	.p2align 3
.L392:
	cltq
	movl	$8, %ecx
	movl	$2, %ebx
	leaq	-7(%r12,%rax), %rsi
	leaq	.LC6(%rip), %rdi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L242
.L243:
	leaq	-6(%r12,%rax), %rsi
	movl	$7, %ecx
	leaq	.LC7(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	xorl	%ebx, %ebx
	testb	%al, %al
	sete	%bl
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L391:
	cmpb	$97, 1(%r12)
	jne	.L336
	cmpb	$121, 2(%r12)
	jne	.L336
	movl	$3, 16(%r13)
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L287:
	movl	$1, 16(%r13)
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L345:
	cmpl	$49, %eax
	jne	.L346
	cmpb	$0, 1(%rdx)
	movl	$3, %r9d
	je	.L306
.L346:
	cmpl	$50, %eax
	jne	.L304
	cmpb	$0, 1(%rdx)
	jne	.L304
	movl	$4, %r9d
	jmp	.L306
	.p2align 4,,10
	.p2align 3
.L402:
	cmpb	$50, 1(%rdx)
	je	.L405
.L343:
	cmpl	$45, %eax
	jne	.L344
	cmpb	$49, 1(%rdx)
	jne	.L344
	cmpb	$0, 2(%rdx)
	movl	$1, %r9d
	je	.L306
	jmp	.L344
	.p2align 4,,10
	.p2align 3
.L388:
	movl	$2, 12(%r13)
	movq	%r12, %rdi
	call	strlen@PLT
	subl	$7, %eax
	jmp	.L256
	.p2align 4,,10
	.p2align 3
.L394:
	leaq	-128(%rbp), %rdi
	movq	%r12, %rsi
	movq	%rdi, -456(%rbp)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movzwl	-120(%rbp), %eax
	movq	-456(%rbp), %rdi
	testw	%ax, %ax
	js	.L245
	movswl	%ax, %esi
	sarl	$5, %esi
.L246:
	subl	$7, %esi
	movl	$7, %r9d
	xorl	%r8d, %r8d
	movl	$7, %edx
	leaq	_ZZN6icu_6712_GLOBAL__N_122RelDateTimeFmtDataSink27styleFromAliasUnicodeStringENS_13UnicodeStringEE6narrow(%rip), %rcx
	movq	%rdi, -456(%rbp)
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	leaq	_ZZN6icu_6712_GLOBAL__N_122RelDateTimeFmtDataSink27styleFromAliasUnicodeStringENS_13UnicodeStringEE6narrow(%rip), %rdx
	testb	%al, %al
	movq	-456(%rbp), %rdi
	movl	$2, %ecx
	je	.L247
	movzwl	-120(%rbp), %eax
	testw	%ax, %ax
	js	.L248
	movswl	%ax, %esi
	movl	%esi, %eax
	sarl	$5, %eax
.L249:
	leal	-6(%rax), %esi
	movl	$6, %r9d
	xorl	%r8d, %r8d
	movl	$6, %edx
	leaq	_ZZN6icu_6712_GLOBAL__N_122RelDateTimeFmtDataSink27styleFromAliasUnicodeStringENS_13UnicodeStringEE6sshort(%rip), %rcx
	movq	%rdi, -456(%rbp)
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	leaq	_ZZN6icu_6712_GLOBAL__N_122RelDateTimeFmtDataSink27styleFromAliasUnicodeStringENS_13UnicodeStringEE6sshort(%rip), %rdx
	xorl	%ecx, %ecx
	movq	-456(%rbp), %rdi
	testb	%al, %al
	sete	%cl
.L247:
	movl	%ecx, -456(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-456(%rbp), %ecx
	cmpl	%ecx, %ebx
	je	.L252
	movq	24(%r13), %rax
	leaq	(%rax,%rbx,4), %rdx
	movl	21688(%rdx), %eax
	cmpl	$-1, %eax
	je	.L251
	cmpl	%ecx, %eax
	je	.L251
.L252:
	movq	-448(%rbp), %rax
	movl	$3, (%rax)
	jmp	.L244
	.p2align 4,,10
	.p2align 3
.L405:
	xorl	%r9d, %r9d
	cmpb	$0, 2(%rdx)
	je	.L306
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L380:
	movl	-504(%rbp), %r12d
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L396:
	cmpb	$111, 1(%r12)
	jne	.L338
	cmpb	$110, 2(%r12)
	jne	.L338
	movl	$9, 16(%r13)
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L397:
	cmpb	$117, 1(%r12)
	jne	.L339
	cmpb	$101, 2(%r12)
	jne	.L339
	movl	$10, 16(%r13)
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L283:
	movl	$2, 16(%r13)
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L251:
	movl	%ecx, 21688(%rdx)
	movq	%r12, %rdi
	addl	$1, %r15d
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L398:
	cmpb	$101, 1(%r12)
	jne	.L340
	cmpb	$100, 2(%r12)
	jne	.L340
	movl	$11, 16(%r13)
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L248:
	movl	-116(%rbp), %eax
	jmp	.L249
	.p2align 4,,10
	.p2align 3
.L382:
	movq	%r15, %r13
	movl	-520(%rbp), %r15d
.L317:
	addl	$1, %r15d
	jmp	.L324
	.p2align 4,,10
	.p2align 3
.L315:
	movl	$7, %ecx
	movq	%rdx, %rsi
	leaq	.LC18(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L317
	movq	-488(%rbp), %rax
	movl	$1, 8(%rax)
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L403:
	cmpb	$48, (%rdx)
	jne	.L312
	cmpb	$0, 1(%rdx)
	jne	.L312
	movslq	12(%r12), %rdx
	movq	24(%r12), %rcx
	imulq	$5760, %rdx, %rdx
	leaq	4568(%rcx,%rdx), %r10
	movswl	8(%r10), %edx
	shrl	$5, %edx
	jne	.L312
	movq	0(%r13), %rax
	movq	%r9, -544(%rbp)
	movq	%r13, %rdi
	movq	%r10, -536(%rbp)
	movq	-448(%rbp), %rdx
	movq	-512(%rbp), %rsi
	movl	$0, -400(%rbp)
	call	*32(%rax)
	leaq	-128(%rbp), %r11
	movl	-400(%rbp), %ecx
	movq	-520(%rbp), %rdx
	movl	$1, %esi
	movq	%r11, %rdi
	movq	%rax, -288(%rbp)
	movq	%r11, -528(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	-528(%rbp), %r11
	movq	-536(%rbp), %r10
	movq	%r11, %rsi
	movq	%r10, %rdi
	call	_ZN6icu_6713UnicodeString12fastCopyFromERKS0_@PLT
	movq	-528(%rbp), %r11
	movq	%r11, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	16(%r12), %eax
	movq	-544(%rbp), %r9
	jmp	.L312
	.p2align 4,,10
	.p2align 3
.L245:
	movl	-116(%rbp), %esi
	jmp	.L246
	.p2align 4,,10
	.p2align 3
.L404:
	movq	0(%r13), %rax
	movq	%r9, -536(%rbp)
	movq	%r13, %rdi
	movq	-448(%rbp), %rdx
	movq	-512(%rbp), %rsi
	movl	$0, -400(%rbp)
	call	*32(%rax)
	leaq	-128(%rbp), %r10
	movl	-400(%rbp), %ecx
	movq	-520(%rbp), %rdx
	movl	$1, %esi
	movq	%r10, %rdi
	movq	%rax, -288(%rbp)
	movq	%r10, -528(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	-528(%rbp), %r10
	movq	-536(%rbp), %r9
	movq	%r10, %rsi
	movq	%r9, %rdi
	call	_ZN6icu_6713UnicodeString12fastCopyFromERKS0_@PLT
	movq	-528(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L304
	.p2align 4,,10
	.p2align 3
.L284:
	movl	$4, 16(%r13)
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L381:
	movl	-496(%rbp), %r12d
	jmp	.L299
.L393:
	movl	$6, %eax
	jmp	.L243
.L399:
	cmpb	$117, 2(%r12)
	jne	.L341
	movl	$12, 16(%r13)
	jmp	.L289
.L400:
	cmpb	$105, 2(%r12)
	jne	.L342
	movl	$13, 16(%r13)
	jmp	.L289
.L298:
	movq	-448(%rbp), %rdx
	movq	%r13, %rdi
	leaq	-288(%rbp), %rsi
	movl	$0, -288(%rbp)
	call	*32(%rcx)
	leaq	-128(%rbp), %r15
	movl	-288(%rbp), %ecx
	leaq	-240(%rbp), %rdx
	movl	$1, %esi
	movq	%r15, %rdi
	movq	%rax, -240(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	%r14, %rdi
	movq	%r15, %rsi
	call	_ZN6icu_6713UnicodeString12fastCopyFromERKS0_@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L386
.L401:
	cmpb	$116, 2(%r12)
	jne	.L259
	movl	$14, 16(%r13)
	jmp	.L289
.L395:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4509:
	.size	_ZN6icu_6712_GLOBAL__N_122RelDateTimeFmtDataSink3putEPKcRNS_13ResourceValueEaR10UErrorCode, .-_ZN6icu_6712_GLOBAL__N_122RelDateTimeFmtDataSink3putEPKcRNS_13ResourceValueEaR10UErrorCode
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev:
.LFB5195:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE5195:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei:
.LFB5198:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	cmpl	$40, %esi
	jg	.L419
	ret
	.p2align 4,,10
	.p2align 3
.L419:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L407
	cmpb	$0, 12(%rbx)
	jne	.L420
.L411:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L407:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L420:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L411
	.cfi_endproc
.LFE5198:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EED2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev:
.LFB5201:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L423
	ret
	.p2align 4,,10
	.p2align 3
.L423:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE5201:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5EOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_:
.LFB5204:
	.cfi_startproc
	endbr64
	movzbl	12(%rsi), %eax
	movq	(%rsi), %r8
	movslq	8(%rsi), %rdx
	movb	%al, 12(%rdi)
	leaq	13(%rsi), %rax
	movq	%r8, (%rdi)
	movl	%edx, 8(%rdi)
	cmpq	%rax, %r8
	je	.L426
	movq	%rax, (%rsi)
	movl	$40, 8(%rsi)
	movb	$0, 12(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L426:
	leaq	13(%rdi), %r9
	movq	%r8, %rsi
	movq	%r9, (%rdi)
	movq	%r9, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE5204:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_:
.LFB5206:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 12(%rdi)
	movq	%rsi, %rbx
	jne	.L432
.L428:
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movzbl	12(%rbx), %eax
	movb	%al, 12(%r12)
	movq	(%rbx), %rsi
	leaq	13(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L433
	movq	%rsi, (%r12)
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L432:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L428
	.p2align 4,,10
	.p2align 3
.L433:
	leaq	13(%r12), %rdi
	movslq	8(%rbx), %rdx
	movq	%rdi, (%r12)
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5206:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv:
.LFB5207:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE5207:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv:
.LFB5208:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE5208:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv:
.LFB5209:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	addq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE5209:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB5210:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE5210:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB5211:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE5211:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci:
.LFB5212:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L449
	testl	%edx, %edx
	jle	.L449
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 12(%rdi)
	jne	.L452
.L441:
	movq	%rsi, (%rbx)
	movl	%edx, 8(%rbx)
	movb	$0, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L449:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L452:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	%edx, -28(%rbp)
	movq	%rsi, -24(%rbp)
	call	uprv_free_67@PLT
	movl	-28(%rbp), %edx
	movq	-24(%rbp), %rsi
	jmp	.L441
	.cfi_endproc
.LFE5212:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii:
.LFB5213:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L456
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movl	%esi, %r13d
	movl	%edx, %r12d
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L456
	testl	%r12d, %r12d
	jg	.L463
	cmpb	$0, 12(%rbx)
	jne	.L464
.L458:
	movq	%r14, (%rbx)
	movq	%r14, %rax
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L463:
	.cfi_restore_state
	cmpl	%r12d, 8(%rbx)
	cmovle	8(%rbx), %r12d
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	je	.L458
.L464:
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L458
	.p2align 4,,10
	.p2align 3
.L456:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5213:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, .-_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi:
.LFB5214:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 12(%rdi)
	movq	%rdi, %rbx
	je	.L466
	movq	(%rdi), %r8
.L467:
	leaq	13(%rbx), %rax
	movl	%r12d, 0(%r13)
	movq	%rax, (%rbx)
	movq	%r8, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L466:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L470
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	cmovg	%eax, %r12d
	movslq	%r12d, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L470
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L467
	.p2align 4,,10
	.p2align 3
.L470:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5214:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, .-_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv:
.LFB5215:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L477
	ret
	.p2align 4,,10
	.p2align 3
.L477:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE5215:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv:
.LFB5216:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE5216:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_:
.LFB5217:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5217:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_:
.LFB5218:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE5218:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5ERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_:
.LFB5220:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5220:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_:
.LFB5222:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5222:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6725RelativeDateTimeCacheData21getAbsoluteUnitStringEi17UDateAbsoluteUnit14UDateDirection
	.type	_ZNK6icu_6725RelativeDateTimeCacheData21getAbsoluteUnitStringEi17UDateAbsoluteUnit14UDateDirection, @function
_ZNK6icu_6725RelativeDateTimeCacheData21getAbsoluteUnitStringEi17UDateAbsoluteUnit14UDateDirection:
.LFB4488:
	.cfi_startproc
	endbr64
	movslq	%edx, %rdx
	movslq	%ecx, %rcx
	leaq	(%rdx,%rdx,2), %rax
	leaq	(%rcx,%rax,2), %rcx
	jmp	.L485
	.p2align 4,,10
	.p2align 3
.L488:
	movl	21688(%rdi,%rsi,4), %esi
	cmpl	$-1, %esi
	je	.L487
.L485:
	movslq	%esi, %rsi
	imulq	$90, %rsi, %rax
	addq	%rcx, %rax
	salq	$6, %rax
	leaq	24(%rdi,%rax), %rax
	movswl	8(%rax), %edx
	shrl	$5, %edx
	je	.L488
	ret
	.p2align 4,,10
	.p2align 3
.L487:
	leaq	21624(%rdi), %rax
	ret
	.cfi_endproc
.LFE4488:
	.size	_ZNK6icu_6725RelativeDateTimeCacheData21getAbsoluteUnitStringEi17UDateAbsoluteUnit14UDateDirection, .-_ZNK6icu_6725RelativeDateTimeCacheData21getAbsoluteUnitStringEi17UDateAbsoluteUnit14UDateDirection
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6725RelativeDateTimeCacheData24getRelativeUnitFormatterEi17UDateRelativeUnitii
	.type	_ZNK6icu_6725RelativeDateTimeCacheData24getRelativeUnitFormatterEi17UDateRelativeUnitii, @function
_ZNK6icu_6725RelativeDateTimeCacheData24getRelativeUnitFormatterEi17UDateRelativeUnitii:
.LFB4489:
	.cfi_startproc
	endbr64
	xorl	%r9d, %r9d
	cmpl	$6, %edx
	ja	.L489
	leaq	CSWTCH.304(%rip), %rax
	movl	%edx, %edx
	movslq	(%rax,%rdx,4), %rdx
	movslq	%ecx, %rax
	leaq	(%rax,%rax,2), %rax
	leaq	(%rdx,%rdx,2), %rdx
	addq	%rax, %rax
	leaq	(%rax,%rdx,4), %r10
.L492:
	movslq	%esi, %rax
	movslq	%r8d, %rcx
	jmp	.L491
	.p2align 4,,10
	.p2align 3
.L497:
	movslq	21688(%rdi,%rax,4), %rax
	cmpl	$-1, %eax
	je	.L496
.L491:
	imulq	$180, %rax, %rdx
	addq	%r10, %rdx
	leaq	2162(%rdx,%rcx), %rdx
	movq	8(%rdi,%rdx,8), %r9
	testq	%r9, %r9
	je	.L497
.L489:
	movq	%r9, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L496:
	cmpl	$5, %r8d
	je	.L489
	movl	$5, %r8d
	jmp	.L492
	.cfi_endproc
.LFE4489:
	.size	_ZNK6icu_6725RelativeDateTimeCacheData24getRelativeUnitFormatterEi17UDateRelativeUnitii, .-_ZNK6icu_6725RelativeDateTimeCacheData24getRelativeUnitFormatterEi17UDateRelativeUnitii
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6725RelativeDateTimeCacheData32getRelativeDateTimeUnitFormatterEi21URelativeDateTimeUnitii
	.type	_ZNK6icu_6725RelativeDateTimeCacheData32getRelativeDateTimeUnitFormatterEi21URelativeDateTimeUnitii, @function
_ZNK6icu_6725RelativeDateTimeCacheData32getRelativeDateTimeUnitFormatterEi21URelativeDateTimeUnitii:
.LFB4490:
	.cfi_startproc
	endbr64
	movslq	%ecx, %rcx
	movslq	%edx, %rdx
	leaq	(%rcx,%rcx,2), %rax
	leaq	(%rdx,%rdx,2), %rdx
	addq	%rax, %rax
	leaq	(%rax,%rdx,4), %r10
.L501:
	movslq	%esi, %rax
	movslq	%r8d, %rcx
	jmp	.L500
	.p2align 4,,10
	.p2align 3
.L505:
	movslq	21688(%rdi,%rax,4), %rax
	cmpl	$-1, %eax
	je	.L504
.L500:
	imulq	$180, %rax, %rdx
	addq	%r10, %rdx
	leaq	2162(%rdx,%rcx), %rdx
	movq	8(%rdi,%rdx,8), %r9
	testq	%r9, %r9
	je	.L505
.L498:
	movq	%r9, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L504:
	cmpl	$5, %r8d
	je	.L498
	movl	$5, %r8d
	jmp	.L501
	.cfi_endproc
.LFE4490:
	.size	_ZNK6icu_6725RelativeDateTimeCacheData32getRelativeDateTimeUnitFormatterEi21URelativeDateTimeUnitii, .-_ZNK6icu_6725RelativeDateTimeCacheData32getRelativeDateTimeUnitFormatterEi21URelativeDateTimeUnitii
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725FormattedRelativeDateTimeC2EOS0_
	.type	_ZN6icu_6725FormattedRelativeDateTimeC2EOS0_, @function
_ZN6icu_6725FormattedRelativeDateTimeC2EOS0_:
.LFB4528:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6725FormattedRelativeDateTimeE(%rip), %rax
	movq	%rax, (%rdi)
	movq	8(%rsi), %rax
	movq	$0, 8(%rsi)
	movq	%rax, 8(%rdi)
	movl	16(%rsi), %eax
	movl	$27, 16(%rsi)
	movl	%eax, 16(%rdi)
	ret
	.cfi_endproc
.LFE4528:
	.size	_ZN6icu_6725FormattedRelativeDateTimeC2EOS0_, .-_ZN6icu_6725FormattedRelativeDateTimeC2EOS0_
	.globl	_ZN6icu_6725FormattedRelativeDateTimeC1EOS0_
	.set	_ZN6icu_6725FormattedRelativeDateTimeC1EOS0_,_ZN6icu_6725FormattedRelativeDateTimeC2EOS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725FormattedRelativeDateTimeaSEOS0_
	.type	_ZN6icu_6725FormattedRelativeDateTimeaSEOS0_, @function
_ZN6icu_6725FormattedRelativeDateTimeaSEOS0_:
.LFB4534:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %r13
	testq	%r13, %r13
	je	.L508
	movq	0(%r13), %rax
	leaq	_ZN6icu_6729FormattedRelativeDateTimeDataD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L509
	leaq	16+_ZTVN6icu_6729FormattedRelativeDateTimeDataE(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, 0(%r13)
	call	_ZN6icu_6731FormattedValueStringBuilderImplD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L508:
	movq	8(%rbx), %rax
	movq	%rax, 8(%r12)
	movl	16(%rbx), %eax
	movq	$0, 8(%rbx)
	movl	%eax, 16(%r12)
	movq	%r12, %rax
	movl	$27, 16(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L509:
	.cfi_restore_state
	movq	%r13, %rdi
	call	*%rax
	jmp	.L508
	.cfi_endproc
.LFE4534:
	.size	_ZN6icu_6725FormattedRelativeDateTimeaSEOS0_, .-_ZN6icu_6725FormattedRelativeDateTimeaSEOS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725RelativeDateTimeFormatterC2ERKS0_
	.type	_ZN6icu_6725RelativeDateTimeFormatterC2ERKS0_, @function
_ZN6icu_6725RelativeDateTimeFormatterC2ERKS0_:
.LFB4552:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6725RelativeDateTimeFormatterE(%rip), %rax
	addq	$48, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	-24(%rsi), %rdx
	movq	-32(%rsi), %rcx
	movq	%rax, (%rdi)
	movq	-40(%rsi), %rdi
	movq	-8(%rsi), %rax
	movq	%rdx, 24(%rbx)
	movq	-16(%rsi), %rdx
	movq	%rcx, 16(%rbx)
	movq	%rdx, 32(%rbx)
	movq	%rax, 40(%rbx)
	movq	%rdi, 8(%rbx)
	leaq	48(%rbx), %rdi
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movq	8(%rbx), %rdi
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
	movq	16(%rbx), %rdi
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
	movq	24(%rbx), %rdi
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
	movq	40(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L514
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_6712SharedObject6addRefEv@PLT
	.p2align 4,,10
	.p2align 3
.L514:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4552:
	.size	_ZN6icu_6725RelativeDateTimeFormatterC2ERKS0_, .-_ZN6icu_6725RelativeDateTimeFormatterC2ERKS0_
	.globl	_ZN6icu_6725RelativeDateTimeFormatterC1ERKS0_
	.set	_ZN6icu_6725RelativeDateTimeFormatterC1ERKS0_,_ZN6icu_6725RelativeDateTimeFormatterC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725RelativeDateTimeFormatteraSERKS0_
	.type	_ZN6icu_6725RelativeDateTimeFormatteraSERKS0_, @function
_ZN6icu_6725RelativeDateTimeFormatteraSERKS0_:
.LFB4554:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	cmpq	%rsi, %rdi
	je	.L518
	movq	8(%rsi), %r13
	movq	8(%rdi), %rdi
	movq	%rsi, %rbx
	cmpq	%rdi, %r13
	je	.L520
	testq	%rdi, %rdi
	je	.L521
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
.L521:
	movq	%r13, 8(%r12)
	testq	%r13, %r13
	je	.L520
	movq	%r13, %rdi
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
.L520:
	movq	16(%rbx), %r13
	movq	16(%r12), %rdi
	cmpq	%rdi, %r13
	je	.L524
	testq	%rdi, %rdi
	je	.L525
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
.L525:
	movq	%r13, 16(%r12)
	testq	%r13, %r13
	je	.L524
	movq	%r13, %rdi
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
.L524:
	movq	24(%rbx), %r13
	movq	24(%r12), %rdi
	cmpq	%rdi, %r13
	je	.L528
	testq	%rdi, %rdi
	je	.L529
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
.L529:
	movq	%r13, 24(%r12)
	testq	%r13, %r13
	je	.L528
	movq	%r13, %rdi
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
.L528:
	movq	40(%rbx), %r13
	movq	40(%r12), %rdi
	cmpq	%rdi, %r13
	je	.L532
	testq	%rdi, %rdi
	je	.L533
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
.L533:
	movq	%r13, 40(%r12)
	testq	%r13, %r13
	je	.L532
	movq	%r13, %rdi
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
.L532:
	movl	32(%rbx), %eax
	leaq	48(%rbx), %rsi
	leaq	48(%r12), %rdi
	movl	%eax, 32(%r12)
	movl	36(%rbx), %eax
	movl	%eax, 36(%r12)
	call	_ZN6icu_676LocaleaSERKS0_@PLT
.L518:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4554:
	.size	_ZN6icu_6725RelativeDateTimeFormatteraSERKS0_, .-_ZN6icu_6725RelativeDateTimeFormatteraSERKS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6725RelativeDateTimeFormatter15getNumberFormatEv
	.type	_ZNK6icu_6725RelativeDateTimeFormatter15getNumberFormatEv, @function
_ZNK6icu_6725RelativeDateTimeFormatter15getNumberFormatEv:
.LFB4559:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	24(%rax), %rax
	ret
	.cfi_endproc
.LFE4559:
	.size	_ZNK6icu_6725RelativeDateTimeFormatter15getNumberFormatEv, .-_ZNK6icu_6725RelativeDateTimeFormatter15getNumberFormatEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6725RelativeDateTimeFormatter24getCapitalizationContextEv
	.type	_ZNK6icu_6725RelativeDateTimeFormatter24getCapitalizationContextEv, @function
_ZNK6icu_6725RelativeDateTimeFormatter24getCapitalizationContextEv:
.LFB4560:
	.cfi_startproc
	endbr64
	movl	36(%rdi), %eax
	ret
	.cfi_endproc
.LFE4560:
	.size	_ZNK6icu_6725RelativeDateTimeFormatter24getCapitalizationContextEv, .-_ZNK6icu_6725RelativeDateTimeFormatter24getCapitalizationContextEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6725RelativeDateTimeFormatter14getFormatStyleEv
	.type	_ZNK6icu_6725RelativeDateTimeFormatter14getFormatStyleEv, @function
_ZNK6icu_6725RelativeDateTimeFormatter14getFormatStyleEv:
.LFB4561:
	.cfi_startproc
	endbr64
	movl	32(%rdi), %eax
	ret
	.cfi_endproc
.LFE4561:
	.size	_ZNK6icu_6725RelativeDateTimeFormatter14getFormatStyleEv, .-_ZNK6icu_6725RelativeDateTimeFormatter14getFormatStyleEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6725RelativeDateTimeFormatter6formatEd14UDateDirection17UDateRelativeUnitRNS_13UnicodeStringER10UErrorCode
	.type	_ZNK6icu_6725RelativeDateTimeFormatter6formatEd14UDateDirection17UDateRelativeUnitRNS_13UnicodeStringER10UErrorCode, @function
_ZNK6icu_6725RelativeDateTimeFormatter6formatEd14UDateDirection17UDateRelativeUnitRNS_13UnicodeStringER10UErrorCode:
.LFB4564:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-208(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r14, %rdi
	subq	$328, %rsp
	movq	%rcx, -344(%rbp)
	movzbl	_ZN6icu_67L16kRDTNumericFieldE(%rip), %esi
	movl	%edx, -356(%rbp)
	movsd	%xmm0, -352(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6731FormattedValueStringBuilderImplC2ENS_22FormattedStringBuilder5FieldE@PLT
	movl	(%r12), %ecx
	leaq	16+_ZTVN6icu_6729FormattedRelativeDateTimeDataE(%rip), %rax
	movq	%rax, -208(%rbp)
	testl	%ecx, %ecx
	jg	.L583
	movl	%r13d, %eax
	andl	$-3, %eax
	cmpl	$1, %eax
	jne	.L597
	movq	24(%rbx), %rax
	movsd	-352(%rbp), %xmm0
	movq	%r12, %r8
	leaq	-200(%rbp), %r15
	movq	%r15, %rdx
	leaq	-324(%rbp), %rcx
	movq	24(%rax), %rsi
	movq	16(%rbx), %rax
	movq	24(%rax), %rdi
	call	_ZN6icu_6717QuantityFormatter15formatAndSelectEdRKNS_12NumberFormatERKNS_11PluralRulesERNS_22FormattedStringBuilderERNS_14StandardPlural4FormER10UErrorCode@PLT
	movl	(%r12), %edx
	testl	%edx, %edx
	jg	.L583
	movl	-356(%rbp), %eax
	cmpl	$6, %eax
	ja	.L567
	cmpl	$3, %r13d
	movl	%eax, %edx
	movq	8(%rbx), %rcx
	movl	32(%rbx), %r10d
	leaq	CSWTCH.304(%rip), %rax
	movl	-324(%rbp), %r9d
	movslq	(%rax,%rdx,4), %rax
	sete	%dl
	movzbl	%dl, %edx
	leaq	(%rax,%rax,2), %rsi
	leaq	(%rdx,%rdx), %rax
	addq	%rdx, %rax
	addq	%rax, %rax
	leaq	(%rax,%rsi,4), %rdi
.L570:
	movslq	%r10d, %rax
	movslq	%r9d, %rdx
	jmp	.L569
	.p2align 4,,10
	.p2align 3
.L599:
	movslq	21688(%rcx,%rax,4), %rax
	cmpl	$-1, %eax
	je	.L598
.L569:
	imulq	$180, %rax, %rsi
	addq	%rdi, %rsi
	leaq	2162(%rsi,%rdx), %rsi
	movq	8(%rcx,%rsi,8), %rsi
	testq	%rsi, %rsi
	je	.L599
	movzbl	_ZN6icu_67L16kRDTLiteralFieldE(%rip), %edx
	leaq	-320(%rbp), %r13
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl14SimpleModifierC1ERKNS_15SimpleFormatterENS_22FormattedStringBuilder5FieldEb@PLT
	movq	%r15, %rdi
	call	_ZNK6icu_6722FormattedStringBuilder6lengthEv@PLT
	movq	%r12, %r8
	xorl	%edx, %edx
	movq	%r15, %rsi
	movl	%eax, %ecx
	movq	%r13, %rdi
	call	_ZNK6icu_676number4impl14SimpleModifier20formatAsPrefixSuffixERNS_22FormattedStringBuilderEiiR10UErrorCode@PLT
	leaq	16+_ZTVN6icu_676number4impl14SimpleModifierE(%rip), %rax
	leaq	-312(%rbp), %rdi
	movq	%rax, -320(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_676number4impl8ModifierD2Ev@PLT
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L583
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZNK6icu_6722FormattedStringBuilder15toUnicodeStringEv@PLT
	cmpq	$0, 40(%rbx)
	je	.L572
	movswl	-312(%rbp), %ecx
	testw	%cx, %cx
	js	.L573
	sarl	$5, %ecx
	je	.L579
.L575:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movl	%eax, %edi
	call	u_islower_67@PLT
	testb	%al, %al
	je	.L572
	leaq	_ZZNK6icu_6725RelativeDateTimeFormatter16adjustForContextERNS_13UnicodeStringEE13gBrkIterMutex(%rip), %rdi
	call	umtx_lock_67@PLT
	movq	40(%rbx), %rax
	movq	%r13, %rdi
	leaq	48(%rbx), %rdx
	movl	$768, %ecx
	movq	24(%rax), %rsi
	call	_ZN6icu_6713UnicodeString7toTitleEPNS_13BreakIteratorERKNS_6LocaleEj@PLT
	leaq	_ZZNK6icu_6725RelativeDateTimeFormatter16adjustForContextERNS_13UnicodeStringEE13gBrkIterMutex(%rip), %rdi
	call	umtx_unlock_67@PLT
.L572:
	movswl	-312(%rbp), %ecx
	movl	%ecx, %eax
	sarl	$5, %ecx
	testw	%ax, %ax
	cmovs	-308(%rbp), %ecx
.L579:
	movq	-344(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L566
	.p2align 4,,10
	.p2align 3
.L583:
	movq	-344(%rbp), %r12
.L566:
	leaq	16+_ZTVN6icu_6729FormattedRelativeDateTimeDataE(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -208(%rbp)
	call	_ZN6icu_6731FormattedValueStringBuilderImplD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L600
	addq	$328, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L597:
	.cfi_restore_state
	movl	$1, (%r12)
	movq	-344(%rbp), %r12
	jmp	.L566
	.p2align 4,,10
	.p2align 3
.L567:
	movl	$3, (%r12)
	movq	-344(%rbp), %r12
	jmp	.L566
	.p2align 4,,10
	.p2align 3
.L598:
	cmpl	$5, %r9d
	je	.L567
	movl	$5, %r9d
	jmp	.L570
	.p2align 4,,10
	.p2align 3
.L573:
	movl	-308(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L579
	jmp	.L575
.L600:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4564:
	.size	_ZNK6icu_6725RelativeDateTimeFormatter6formatEd14UDateDirection17UDateRelativeUnitRNS_13UnicodeStringER10UErrorCode, .-_ZNK6icu_6725RelativeDateTimeFormatter6formatEd14UDateDirection17UDateRelativeUnitRNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6725RelativeDateTimeFormatter13formatToValueEd14UDateDirection17UDateRelativeUnitR10UErrorCode
	.type	_ZNK6icu_6725RelativeDateTimeFormatter13formatToValueEd14UDateDirection17UDateRelativeUnitR10UErrorCode, @function
_ZNK6icu_6725RelativeDateTimeFormatter13formatToValueEd14UDateDirection17UDateRelativeUnitR10UErrorCode:
.LFB4565:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	cmpq	$0, 40(%rsi)
	movl	%ecx, -60(%rbp)
	movsd	%xmm0, -56(%rbp)
	je	.L613
	leaq	16+_ZTVN6icu_6725FormattedRelativeDateTimeE(%rip), %rax
	movl	$16, (%r8)
	movq	%rax, (%rdi)
	movq	$0, 8(%rdi)
	movl	$16, 16(%rdi)
.L601:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L613:
	.cfi_restore_state
	movl	$152, %edi
	movq	%rsi, %rbx
	movl	%edx, %r15d
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L614
	movzbl	_ZN6icu_67L16kRDTNumericFieldE(%rip), %esi
	movq	%rax, %rdi
	call	_ZN6icu_6731FormattedValueStringBuilderImplC2ENS_22FormattedStringBuilder5FieldE@PLT
	movl	0(%r13), %edx
	leaq	16+_ZTVN6icu_6729FormattedRelativeDateTimeDataE(%rip), %rax
	movq	%rax, (%r14)
	testl	%edx, %edx
	jle	.L615
	leaq	16+_ZTVN6icu_6725FormattedRelativeDateTimeE(%rip), %rbx
	movl	%edx, 16(%r12)
	movq	%r14, %rdi
	movq	%rbx, (%r12)
	movq	$0, 8(%r12)
	movq	%rax, (%r14)
	call	_ZN6icu_6731FormattedValueStringBuilderImplD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	jmp	.L601
	.p2align 4,,10
	.p2align 3
.L615:
	movl	-60(%rbp), %edx
	movsd	-56(%rbp), %xmm0
	movq	%r13, %r8
	movq	%r14, %rcx
	movl	%r15d, %esi
	movq	%rbx, %rdi
	call	_ZNK6icu_6725RelativeDateTimeFormatter10formatImplEd14UDateDirection17UDateRelativeUnitRNS_29FormattedRelativeDateTimeDataER10UErrorCode
	leaq	8(%r14), %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6722FormattedStringBuilder15writeTerminatorER10UErrorCode@PLT
	leaq	16+_ZTVN6icu_6725FormattedRelativeDateTimeE(%rip), %rax
	movq	%r14, 8(%r12)
	movq	%rax, (%r12)
	movl	$0, 16(%r12)
	jmp	.L601
.L614:
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L608
	movl	$7, 0(%r13)
	movl	$7, %eax
.L608:
	leaq	16+_ZTVN6icu_6725FormattedRelativeDateTimeE(%rip), %rbx
	movl	%eax, 16(%r12)
	movq	%rbx, (%r12)
	movq	$0, 8(%r12)
	jmp	.L601
	.cfi_endproc
.LFE4565:
	.size	_ZNK6icu_6725RelativeDateTimeFormatter13formatToValueEd14UDateDirection17UDateRelativeUnitR10UErrorCode, .-_ZNK6icu_6725RelativeDateTimeFormatter13formatToValueEd14UDateDirection17UDateRelativeUnitR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6725RelativeDateTimeFormatter13formatNumericEd21URelativeDateTimeUnitRNS_13UnicodeStringER10UErrorCode
	.type	_ZNK6icu_6725RelativeDateTimeFormatter13formatNumericEd21URelativeDateTimeUnitRNS_13UnicodeStringER10UErrorCode, @function
_ZNK6icu_6725RelativeDateTimeFormatter13formatNumericEd21URelativeDateTimeUnitRNS_13UnicodeStringER10UErrorCode:
.LFB4567:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	16+_ZTVN6icu_6729FormattedRelativeDateTimeDataE(%rip), %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-208(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r14, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$248, %rsp
	movl	%esi, -276(%rbp)
	movzbl	_ZN6icu_67L16kRDTNumericFieldE(%rip), %esi
	movsd	%xmm0, -288(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6731FormattedValueStringBuilderImplC2ENS_22FormattedStringBuilder5FieldE@PLT
	movq	%rbx, %rcx
	movq	%r14, %rdx
	movq	%r13, %rdi
	movl	-276(%rbp), %r8d
	movsd	-288(%rbp), %xmm0
	movq	%r15, -208(%rbp)
	movl	%r8d, %esi
	call	_ZNK6icu_6725RelativeDateTimeFormatter17formatNumericImplEd21URelativeDateTimeUnitRNS_29FormattedRelativeDateTimeDataER10UErrorCode
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L638
.L617:
	movq	%r14, %rdi
	movq	%r15, -208(%rbp)
	call	_ZN6icu_6731FormattedValueStringBuilderImplD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L639
	addq	$248, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L638:
	.cfi_restore_state
	leaq	-272(%rbp), %rbx
	leaq	-200(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZNK6icu_6722FormattedStringBuilder15toUnicodeStringEv@PLT
	cmpq	$0, 40(%r13)
	je	.L619
	movswl	-264(%rbp), %ecx
	testw	%cx, %cx
	js	.L620
	sarl	$5, %ecx
	je	.L626
.L622:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movl	%eax, %edi
	call	u_islower_67@PLT
	testb	%al, %al
	je	.L619
	leaq	_ZZNK6icu_6725RelativeDateTimeFormatter16adjustForContextERNS_13UnicodeStringEE13gBrkIterMutex(%rip), %rdi
	call	umtx_lock_67@PLT
	movq	40(%r13), %rax
	movq	%rbx, %rdi
	leaq	48(%r13), %rdx
	movl	$768, %ecx
	movq	24(%rax), %rsi
	call	_ZN6icu_6713UnicodeString7toTitleEPNS_13BreakIteratorERKNS_6LocaleEj@PLT
	leaq	_ZZNK6icu_6725RelativeDateTimeFormatter16adjustForContextERNS_13UnicodeStringEE13gBrkIterMutex(%rip), %rdi
	call	umtx_unlock_67@PLT
.L619:
	movswl	-264(%rbp), %ecx
	movl	%ecx, %eax
	sarl	$5, %ecx
	testw	%ax, %ax
	cmovs	-260(%rbp), %ecx
.L626:
	movq	%r12, %rdi
	xorl	%edx, %edx
	movq	%rbx, %rsi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	%rbx, %rdi
	movq	%rax, %r12
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L617
	.p2align 4,,10
	.p2align 3
.L620:
	movl	-260(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L626
	jmp	.L622
.L639:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4567:
	.size	_ZNK6icu_6725RelativeDateTimeFormatter13formatNumericEd21URelativeDateTimeUnitRNS_13UnicodeStringER10UErrorCode, .-_ZNK6icu_6725RelativeDateTimeFormatter13formatNumericEd21URelativeDateTimeUnitRNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6725RelativeDateTimeFormatter20formatNumericToValueEd21URelativeDateTimeUnitR10UErrorCode
	.type	_ZNK6icu_6725RelativeDateTimeFormatter20formatNumericToValueEd21URelativeDateTimeUnitR10UErrorCode, @function
_ZNK6icu_6725RelativeDateTimeFormatter20formatNumericToValueEd21URelativeDateTimeUnitR10UErrorCode:
.LFB4568:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	cmpq	$0, 40(%rsi)
	movsd	%xmm0, -56(%rbp)
	je	.L652
	leaq	16+_ZTVN6icu_6725FormattedRelativeDateTimeE(%rip), %rax
	movl	$16, (%rcx)
	movq	%rax, (%rdi)
	movq	$0, 8(%rdi)
	movl	$16, 16(%rdi)
.L640:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L652:
	.cfi_restore_state
	movl	$152, %edi
	movq	%rsi, %rbx
	movl	%edx, %r15d
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L653
	movzbl	_ZN6icu_67L16kRDTNumericFieldE(%rip), %esi
	movq	%rax, %rdi
	call	_ZN6icu_6731FormattedValueStringBuilderImplC2ENS_22FormattedStringBuilder5FieldE@PLT
	movl	0(%r13), %edx
	leaq	16+_ZTVN6icu_6729FormattedRelativeDateTimeDataE(%rip), %rax
	movq	%rax, (%r14)
	testl	%edx, %edx
	jle	.L654
	leaq	16+_ZTVN6icu_6725FormattedRelativeDateTimeE(%rip), %rbx
	movl	%edx, 16(%r12)
	movq	%r14, %rdi
	movq	%rbx, (%r12)
	movq	$0, 8(%r12)
	movq	%rax, (%r14)
	call	_ZN6icu_6731FormattedValueStringBuilderImplD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	jmp	.L640
	.p2align 4,,10
	.p2align 3
.L654:
	movsd	-56(%rbp), %xmm0
	movq	%r13, %rcx
	movq	%r14, %rdx
	movl	%r15d, %esi
	movq	%rbx, %rdi
	call	_ZNK6icu_6725RelativeDateTimeFormatter17formatNumericImplEd21URelativeDateTimeUnitRNS_29FormattedRelativeDateTimeDataER10UErrorCode
	leaq	8(%r14), %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6722FormattedStringBuilder15writeTerminatorER10UErrorCode@PLT
	leaq	16+_ZTVN6icu_6725FormattedRelativeDateTimeE(%rip), %rax
	movq	%r14, 8(%r12)
	movq	%rax, (%r12)
	movl	$0, 16(%r12)
	jmp	.L640
.L653:
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L647
	movl	$7, 0(%r13)
	movl	$7, %eax
.L647:
	leaq	16+_ZTVN6icu_6725FormattedRelativeDateTimeE(%rip), %rbx
	movl	%eax, 16(%r12)
	movq	%rbx, (%r12)
	movq	$0, 8(%r12)
	jmp	.L640
	.cfi_endproc
.LFE4568:
	.size	_ZNK6icu_6725RelativeDateTimeFormatter20formatNumericToValueEd21URelativeDateTimeUnitR10UErrorCode, .-_ZNK6icu_6725RelativeDateTimeFormatter20formatNumericToValueEd21URelativeDateTimeUnitR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6725RelativeDateTimeFormatter6formatE14UDateDirection17UDateAbsoluteUnitRNS_13UnicodeStringER10UErrorCode
	.type	_ZNK6icu_6725RelativeDateTimeFormatter6formatE14UDateDirection17UDateAbsoluteUnitRNS_13UnicodeStringER10UErrorCode, @function
_ZNK6icu_6725RelativeDateTimeFormatter6formatE14UDateDirection17UDateAbsoluteUnitRNS_13UnicodeStringER10UErrorCode:
.LFB4570:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-208(%rbp), %r15
	leaq	16+_ZTVN6icu_6729FormattedRelativeDateTimeDataE(%rip), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movslq	%esi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movslq	%edx, %rbx
	subq	$248, %rsp
	movq	%rdi, -288(%rbp)
	movzbl	_ZN6icu_67L16kRDTNumericFieldE(%rip), %esi
	movq	%r15, %rdi
	movq	%r8, -280(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6731FormattedValueStringBuilderImplC2ENS_22FormattedStringBuilder5FieldE@PLT
	movq	-280(%rbp), %r8
	movq	%r14, -208(%rbp)
	movl	(%r8), %edx
	testl	%edx, %edx
	jg	.L669
	cmpl	$11, %ebx
	jne	.L657
	cmpl	$5, %r13d
	je	.L657
	movl	$1, (%r8)
	.p2align 4,,10
	.p2align 3
.L669:
	movq	%r15, %rdi
	movq	%r14, -208(%rbp)
	call	_ZN6icu_6731FormattedValueStringBuilderImplD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L689
	addq	$248, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L657:
	.cfi_restore_state
	movq	-288(%rbp), %rax
	leaq	(%rbx,%rbx,2), %rdx
	leaq	0(%r13,%rdx,2), %rdi
	movq	8(%rax), %rcx
	movslq	32(%rax), %rax
	jmp	.L659
	.p2align 4,,10
	.p2align 3
.L691:
	movslq	21688(%rcx,%rax,4), %rax
	cmpl	$-1, %eax
	je	.L690
.L659:
	imulq	$90, %rax, %rdx
	addq	%rdi, %rdx
	salq	$6, %rdx
	leaq	24(%rcx,%rdx), %rdx
	movswl	8(%rdx), %esi
	shrl	$5, %esi
	je	.L691
.L658:
	movzbl	_ZN6icu_67L16kRDTLiteralFieldE(%rip), %ecx
	movl	-68(%rbp), %esi
	leaq	-200(%rbp), %rbx
	movq	%r8, -280(%rbp)
	movq	%rbx, %rdi
	call	_ZN6icu_6722FormattedStringBuilder6insertEiRKNS_13UnicodeStringENS0_5FieldER10UErrorCode@PLT
	movq	-280(%rbp), %r8
	movl	(%r8), %eax
	testl	%eax, %eax
	jg	.L669
	leaq	-272(%rbp), %r13
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZNK6icu_6722FormattedStringBuilder15toUnicodeStringEv@PLT
	movq	-288(%rbp), %rax
	cmpq	$0, 40(%rax)
	je	.L661
	movswl	-264(%rbp), %ecx
	testw	%cx, %cx
	js	.L662
	sarl	$5, %ecx
	je	.L668
.L664:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movl	%eax, %edi
	call	u_islower_67@PLT
	testb	%al, %al
	je	.L661
	leaq	_ZZNK6icu_6725RelativeDateTimeFormatter16adjustForContextERNS_13UnicodeStringEE13gBrkIterMutex(%rip), %rdi
	call	umtx_lock_67@PLT
	movq	-288(%rbp), %rax
	movq	%r13, %rdi
	movl	$768, %ecx
	leaq	48(%rax), %rdx
	movq	40(%rax), %rax
	movq	24(%rax), %rsi
	call	_ZN6icu_6713UnicodeString7toTitleEPNS_13BreakIteratorERKNS_6LocaleEj@PLT
	leaq	_ZZNK6icu_6725RelativeDateTimeFormatter16adjustForContextERNS_13UnicodeStringEE13gBrkIterMutex(%rip), %rdi
	call	umtx_unlock_67@PLT
.L661:
	movswl	-264(%rbp), %ecx
	movl	%ecx, %eax
	sarl	$5, %ecx
	testw	%ax, %ax
	cmovs	-260(%rbp), %ecx
.L668:
	movq	%r12, %rdi
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L669
	.p2align 4,,10
	.p2align 3
.L662:
	movl	-260(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L668
	jmp	.L664
	.p2align 4,,10
	.p2align 3
.L690:
	leaq	21624(%rcx), %rdx
	jmp	.L658
.L689:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4570:
	.size	_ZNK6icu_6725RelativeDateTimeFormatter6formatE14UDateDirection17UDateAbsoluteUnitRNS_13UnicodeStringER10UErrorCode, .-_ZNK6icu_6725RelativeDateTimeFormatter6formatE14UDateDirection17UDateAbsoluteUnitRNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6725RelativeDateTimeFormatter13formatToValueE14UDateDirection17UDateAbsoluteUnitR10UErrorCode
	.type	_ZNK6icu_6725RelativeDateTimeFormatter13formatToValueE14UDateDirection17UDateAbsoluteUnitR10UErrorCode, @function
_ZNK6icu_6725RelativeDateTimeFormatter13formatToValueE14UDateDirection17UDateAbsoluteUnitR10UErrorCode:
.LFB4571:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	cmpq	$0, 40(%rsi)
	movl	%edx, -56(%rbp)
	je	.L715
	leaq	16+_ZTVN6icu_6725FormattedRelativeDateTimeE(%rip), %rax
	movl	$16, (%r8)
	movq	%rax, (%rdi)
	movq	$0, 8(%rdi)
	movl	$16, 16(%rdi)
.L692:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L715:
	.cfi_restore_state
	movl	$152, %edi
	movq	%rsi, %r15
	movslq	%ecx, %rbx
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L716
	movzbl	_ZN6icu_67L16kRDTNumericFieldE(%rip), %esi
	movq	%rax, %rdi
	call	_ZN6icu_6731FormattedValueStringBuilderImplC2ENS_22FormattedStringBuilder5FieldE@PLT
	movl	0(%r13), %edx
	leaq	16+_ZTVN6icu_6729FormattedRelativeDateTimeDataE(%rip), %rax
	movq	%rax, (%r14)
	testl	%edx, %edx
	jle	.L717
	leaq	16+_ZTVN6icu_6725FormattedRelativeDateTimeE(%rip), %rbx
	movl	%edx, 16(%r12)
	movq	%r14, %rdi
	movq	%rbx, (%r12)
	movq	$0, 8(%r12)
	movq	%rax, (%r14)
	call	_ZN6icu_6731FormattedValueStringBuilderImplD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	jmp	.L692
	.p2align 4,,10
	.p2align 3
.L717:
	leaq	8(%r14), %rdi
	cmpl	$11, %ebx
	jne	.L700
	cmpl	$5, -56(%rbp)
	je	.L700
	movl	$1, 0(%r13)
.L701:
	movq	%r13, %rsi
	call	_ZN6icu_6722FormattedStringBuilder15writeTerminatorER10UErrorCode@PLT
	leaq	16+_ZTVN6icu_6725FormattedRelativeDateTimeE(%rip), %rax
	movq	%r14, 8(%r12)
	movq	%rax, (%r12)
	movl	$0, 16(%r12)
	jmp	.L692
	.p2align 4,,10
	.p2align 3
.L700:
	movq	8(%r15), %rcx
	movl	32(%r15), %eax
	leaq	(%rbx,%rbx,2), %rdx
	movslq	-56(%rbp), %r15
	leaq	(%r15,%rdx,2), %rsi
	jmp	.L703
	.p2align 4,,10
	.p2align 3
.L719:
	movl	21688(%rcx,%rdx,4), %eax
	cmpl	$-1, %eax
	je	.L718
.L703:
	movslq	%eax, %rdx
	imulq	$90, %rdx, %rax
	addq	%rsi, %rax
	salq	$6, %rax
	leaq	24(%rcx,%rax), %r9
	movswl	8(%r9), %eax
	shrl	$5, %eax
	je	.L719
.L702:
	movl	140(%r14), %esi
	movzbl	_ZN6icu_67L16kRDTLiteralFieldE(%rip), %ecx
	movq	%r13, %r8
	movq	%r9, %rdx
	movq	%rdi, -56(%rbp)
	call	_ZN6icu_6722FormattedStringBuilder6insertEiRKNS_13UnicodeStringENS0_5FieldER10UErrorCode@PLT
	movq	-56(%rbp), %rdi
	jmp	.L701
.L716:
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L699
	movl	$7, 0(%r13)
	movl	$7, %eax
.L699:
	leaq	16+_ZTVN6icu_6725FormattedRelativeDateTimeE(%rip), %rbx
	movl	%eax, 16(%r12)
	movq	%rbx, (%r12)
	movq	$0, 8(%r12)
	jmp	.L692
	.p2align 4,,10
	.p2align 3
.L718:
	leaq	21624(%rcx), %r9
	jmp	.L702
	.cfi_endproc
.LFE4571:
	.size	_ZNK6icu_6725RelativeDateTimeFormatter13formatToValueE14UDateDirection17UDateAbsoluteUnitR10UErrorCode, .-_ZNK6icu_6725RelativeDateTimeFormatter13formatToValueE14UDateDirection17UDateAbsoluteUnitR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6725RelativeDateTimeFormatter6formatEd21URelativeDateTimeUnitRNS_13UnicodeStringER10UErrorCode
	.type	_ZNK6icu_6725RelativeDateTimeFormatter6formatEd21URelativeDateTimeUnitRNS_13UnicodeStringER10UErrorCode, @function
_ZNK6icu_6725RelativeDateTimeFormatter6formatEd21URelativeDateTimeUnitRNS_13UnicodeStringER10UErrorCode:
.LFB4573:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%esi, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	leaq	16+_ZTVN6icu_6729FormattedRelativeDateTimeDataE(%rip), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-208(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$264, %rsp
	movq	%rdx, -288(%rbp)
	movzbl	_ZN6icu_67L16kRDTNumericFieldE(%rip), %esi
	movsd	%xmm0, -280(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6731FormattedValueStringBuilderImplC2ENS_22FormattedStringBuilder5FieldE@PLT
	movl	(%r12), %edx
	movq	%r14, -208(%rbp)
	movsd	-280(%rbp), %xmm0
	testl	%edx, %edx
	jg	.L770
	comisd	.LC1(%rip), %xmm0
	ja	.L818
.L723:
	movq	%r12, %rcx
	movq	%r13, %rdx
	movl	%r15d, %esi
	movq	%rbx, %rdi
	call	_ZNK6icu_6725RelativeDateTimeFormatter17formatNumericImplEd21URelativeDateTimeUnitRNS_29FormattedRelativeDateTimeDataER10UErrorCode
.L769:
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L770
	leaq	-272(%rbp), %r15
	leaq	-200(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZNK6icu_6722FormattedStringBuilder15toUnicodeStringEv@PLT
	cmpq	$0, 40(%rbx)
	je	.L772
	movswl	-264(%rbp), %ecx
	testw	%cx, %cx
	js	.L773
	sarl	$5, %ecx
	je	.L779
.L775:
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movl	%eax, %edi
	call	u_islower_67@PLT
	testb	%al, %al
	je	.L772
	leaq	_ZZNK6icu_6725RelativeDateTimeFormatter16adjustForContextERNS_13UnicodeStringEE13gBrkIterMutex(%rip), %rdi
	call	umtx_lock_67@PLT
	movq	40(%rbx), %rax
	movq	%r15, %rdi
	leaq	48(%rbx), %rdx
	movl	$768, %ecx
	movq	24(%rax), %rsi
	call	_ZN6icu_6713UnicodeString7toTitleEPNS_13BreakIteratorERKNS_6LocaleEj@PLT
	leaq	_ZZNK6icu_6725RelativeDateTimeFormatter16adjustForContextERNS_13UnicodeStringEE13gBrkIterMutex(%rip), %rdi
	call	umtx_unlock_67@PLT
.L772:
	movswl	-264(%rbp), %ecx
	movl	%ecx, %eax
	sarl	$5, %ecx
	testw	%ax, %ax
	cmovs	-260(%rbp), %ecx
.L779:
	movq	-288(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r15, %rsi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L722
	.p2align 4,,10
	.p2align 3
.L770:
	movq	-288(%rbp), %r12
.L722:
	movq	%r13, %rdi
	movq	%r14, -208(%rbp)
	call	_ZN6icu_6731FormattedValueStringBuilderImplD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L819
	addq	$264, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L818:
	.cfi_restore_state
	movsd	.LC2(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jbe	.L723
	movsd	.LC3(%rip), %xmm1
	pxor	%xmm2, %xmm2
	mulsd	%xmm0, %xmm1
	comisd	%xmm1, %xmm2
	ja	.L820
	addsd	.LC5(%rip), %xmm1
	cvttsd2sil	%xmm1, %eax
.L728:
	testl	%eax, %eax
	je	.L729
	jg	.L730
	cmpl	$-200, %eax
	je	.L731
	cmpl	$-100, %eax
	jne	.L723
	movl	$1, %edx
	movl	$1, %eax
.L732:
	cmpl	$14, %r15d
	ja	.L723
	leaq	.L790(%rip), %rdi
	movl	%r15d, %esi
	movslq	(%rdi,%rsi,4), %rcx
	addq	%rdi, %rcx
	notrack jmp	*%rcx
	.section	.rodata
	.align 4
	.align 4
.L790:
	.long	.L817-.L790
	.long	.L799-.L790
	.long	.L736-.L790
	.long	.L739-.L790
	.long	.L741-.L790
	.long	.L765-.L790
	.long	.L800-.L790
	.long	.L723-.L790
	.long	.L743-.L790
	.long	.L747-.L790
	.long	.L750-.L790
	.long	.L753-.L790
	.long	.L756-.L790
	.long	.L759-.L790
	.long	.L762-.L790
	.text
	.p2align 4,,10
	.p2align 3
.L773:
	movl	-260(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L779
	jmp	.L775
.L789:
	movl	%r15d, %eax
.L817:
	movl	$60, %ecx
.L735:
	movl	%eax, %edx
.L766:
	movq	8(%rbx), %rsi
	movl	32(%rbx), %eax
	addq	%rdx, %rcx
	jmp	.L768
	.p2align 4,,10
	.p2align 3
.L822:
	movl	21688(%rsi,%rdx,4), %eax
	cmpl	$-1, %eax
	je	.L821
.L768:
	movslq	%eax, %rdx
	imulq	$90, %rdx, %rax
	addq	%rcx, %rax
	salq	$6, %rax
	leaq	24(%rsi,%rax), %r9
	movswl	8(%r9), %eax
	shrl	$5, %eax
	je	.L822
.L767:
	movzbl	_ZN6icu_67L16kRDTLiteralFieldE(%rip), %ecx
	movl	-68(%rbp), %esi
	movq	%r12, %r8
	movq	%r9, %rdx
	leaq	-200(%rbp), %rdi
	movsd	%xmm0, -296(%rbp)
	movq	%rdi, -280(%rbp)
	call	_ZN6icu_6722FormattedStringBuilder6insertEiRKNS_13UnicodeStringENS0_5FieldER10UErrorCode@PLT
	movq	-280(%rbp), %rdi
	call	_ZNK6icu_6722FormattedStringBuilder6lengthEv@PLT
	movsd	-296(%rbp), %xmm0
	testl	%eax, %eax
	je	.L723
	jmp	.L769
	.p2align 4,,10
	.p2align 3
.L731:
	cmpl	$14, %r15d
	ja	.L723
	leaq	.L788(%rip), %rcx
	movl	%r15d, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L788:
	.long	.L789-.L788
	.long	.L798-.L788
	.long	.L734-.L788
	.long	.L738-.L788
	.long	.L740-.L788
	.long	.L764-.L788
	.long	.L780-.L788
	.long	.L723-.L788
	.long	.L742-.L788
	.long	.L746-.L788
	.long	.L749-.L788
	.long	.L752-.L788
	.long	.L755-.L788
	.long	.L758-.L788
	.long	.L761-.L788
	.text
.L761:
	movl	$36, %ecx
	xorl	%eax, %eax
	jmp	.L735
.L758:
	movl	$30, %ecx
	xorl	%eax, %eax
	jmp	.L735
.L755:
	movl	$24, %ecx
	xorl	%eax, %eax
	jmp	.L735
.L752:
	movl	$18, %ecx
	xorl	%eax, %eax
	jmp	.L735
.L749:
	movl	$12, %ecx
	xorl	%eax, %eax
	jmp	.L735
.L746:
	movl	$6, %ecx
	xorl	%eax, %eax
	jmp	.L735
.L742:
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	jmp	.L735
.L780:
	movl	$84, %ecx
	xorl	%eax, %eax
	jmp	.L735
.L764:
	movl	$78, %ecx
	xorl	%eax, %eax
	jmp	.L735
.L740:
	movl	$42, %ecx
	xorl	%eax, %eax
	jmp	.L735
.L738:
	movl	$48, %ecx
	xorl	%eax, %eax
	jmp	.L735
.L734:
	movl	$54, %ecx
	xorl	%eax, %eax
	jmp	.L735
.L798:
	movl	$72, %ecx
	xorl	%eax, %eax
	jmp	.L735
	.p2align 4,,10
	.p2align 3
.L729:
	cmpl	$14, %r15d
	ja	.L723
	leaq	.L782(%rip), %rcx
	movl	%r15d, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L782:
	.long	.L787-.L782
	.long	.L786-.L782
	.long	.L785-.L782
	.long	.L784-.L782
	.long	.L795-.L782
	.long	.L783-.L782
	.long	.L796-.L782
	.long	.L797-.L782
	.long	.L744-.L782
	.long	.L748-.L782
	.long	.L751-.L782
	.long	.L754-.L782
	.long	.L757-.L782
	.long	.L760-.L782
	.long	.L763-.L782
	.text
.L797:
	movl	$5, %edx
	movl	$66, %ecx
	jmp	.L766
.L796:
	movl	$84, %ecx
	movl	$2, %eax
	jmp	.L735
.L795:
	movl	$42, %ecx
.L745:
	movl	$2, %edx
	jmp	.L766
.L784:
	movl	$48, %ecx
	jmp	.L745
.L785:
	movl	$54, %ecx
	jmp	.L745
	.p2align 4,,10
	.p2align 3
.L730:
	cmpl	$100, %eax
	je	.L793
	cmpl	$200, %eax
	jne	.L723
	movl	$4, %edx
	movl	$4, %eax
	jmp	.L732
	.p2align 4,,10
	.p2align 3
.L820:
	subsd	.LC5(%rip), %xmm1
	cvttsd2sil	%xmm1, %eax
	jmp	.L728
	.p2align 4,,10
	.p2align 3
.L793:
	movl	$3, %edx
	movl	$3, %eax
	jmp	.L732
	.p2align 4,,10
	.p2align 3
.L821:
	leaq	21624(%rsi), %r9
	jmp	.L767
.L760:
	movl	$30, %ecx
	jmp	.L745
.L757:
	movl	$24, %ecx
	jmp	.L745
.L754:
	movl	$18, %ecx
	jmp	.L745
.L751:
	movl	$12, %ecx
	jmp	.L745
.L748:
	movl	$6, %ecx
	jmp	.L745
.L744:
	xorl	%ecx, %ecx
	jmp	.L745
.L783:
	movl	$78, %ecx
	jmp	.L745
.L786:
	movl	$72, %ecx
	jmp	.L745
.L787:
	movl	$60, %ecx
	jmp	.L745
.L763:
	movl	$36, %ecx
	jmp	.L745
	.p2align 4,,10
	.p2align 3
.L762:
	movl	$36, %ecx
	jmp	.L735
.L759:
	movl	$30, %ecx
	jmp	.L735
.L756:
	movl	$24, %ecx
	jmp	.L735
.L753:
	movl	$18, %ecx
	jmp	.L735
.L750:
	movl	$12, %ecx
	jmp	.L735
.L747:
	movl	$6, %ecx
	jmp	.L735
.L743:
	xorl	%ecx, %ecx
	jmp	.L735
.L765:
	movl	$78, %ecx
	jmp	.L735
.L741:
	movl	$42, %ecx
	jmp	.L735
.L739:
	movl	$48, %ecx
	jmp	.L735
.L736:
	movl	$54, %ecx
	jmp	.L735
.L800:
	movl	$84, %ecx
	jmp	.L766
.L799:
	movl	$72, %ecx
	jmp	.L735
.L819:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4573:
	.size	_ZNK6icu_6725RelativeDateTimeFormatter6formatEd21URelativeDateTimeUnitRNS_13UnicodeStringER10UErrorCode, .-_ZNK6icu_6725RelativeDateTimeFormatter6formatEd21URelativeDateTimeUnitRNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6725RelativeDateTimeFormatter13formatToValueEd21URelativeDateTimeUnitR10UErrorCode
	.type	_ZNK6icu_6725RelativeDateTimeFormatter13formatToValueEd21URelativeDateTimeUnitR10UErrorCode, @function
_ZNK6icu_6725RelativeDateTimeFormatter13formatToValueEd21URelativeDateTimeUnitR10UErrorCode:
.LFB4574:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	cmpq	$0, 40(%rsi)
	movsd	%xmm0, -56(%rbp)
	je	.L835
	leaq	16+_ZTVN6icu_6725FormattedRelativeDateTimeE(%rip), %rax
	movl	$16, (%rcx)
	movq	%rax, (%rdi)
	movq	$0, 8(%rdi)
	movl	$16, 16(%rdi)
.L823:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L835:
	.cfi_restore_state
	movl	$152, %edi
	movq	%rsi, %rbx
	movl	%edx, %r15d
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L836
	movzbl	_ZN6icu_67L16kRDTNumericFieldE(%rip), %esi
	movq	%rax, %rdi
	call	_ZN6icu_6731FormattedValueStringBuilderImplC2ENS_22FormattedStringBuilder5FieldE@PLT
	movl	0(%r13), %edx
	leaq	16+_ZTVN6icu_6729FormattedRelativeDateTimeDataE(%rip), %rax
	movq	%rax, (%r14)
	testl	%edx, %edx
	jle	.L837
	leaq	16+_ZTVN6icu_6725FormattedRelativeDateTimeE(%rip), %rbx
	movl	%edx, 16(%r12)
	movq	%r14, %rdi
	movq	%rbx, (%r12)
	movq	$0, 8(%r12)
	movq	%rax, (%r14)
	call	_ZN6icu_6731FormattedValueStringBuilderImplD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	jmp	.L823
	.p2align 4,,10
	.p2align 3
.L837:
	movsd	-56(%rbp), %xmm0
	movq	%r13, %rcx
	movq	%r14, %rdx
	movl	%r15d, %esi
	movq	%rbx, %rdi
	call	_ZNK6icu_6725RelativeDateTimeFormatter18formatRelativeImplEd21URelativeDateTimeUnitRNS_29FormattedRelativeDateTimeDataER10UErrorCode
	leaq	8(%r14), %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6722FormattedStringBuilder15writeTerminatorER10UErrorCode@PLT
	leaq	16+_ZTVN6icu_6725FormattedRelativeDateTimeE(%rip), %rax
	movq	%r14, 8(%r12)
	movq	%rax, (%r12)
	movl	$0, 16(%r12)
	jmp	.L823
.L836:
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L830
	movl	$7, 0(%r13)
	movl	$7, %eax
.L830:
	leaq	16+_ZTVN6icu_6725FormattedRelativeDateTimeE(%rip), %rbx
	movl	%eax, 16(%r12)
	movq	%rbx, (%r12)
	movq	$0, 8(%r12)
	jmp	.L823
	.cfi_endproc
.LFE4574:
	.size	_ZNK6icu_6725RelativeDateTimeFormatter13formatToValueEd21URelativeDateTimeUnitR10UErrorCode, .-_ZNK6icu_6725RelativeDateTimeFormatter13formatToValueEd21URelativeDateTimeUnitR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6725RelativeDateTimeFormatter18combineDateAndTimeERKNS_13UnicodeStringES3_RS1_R10UErrorCode
	.type	_ZNK6icu_6725RelativeDateTimeFormatter18combineDateAndTimeERKNS_13UnicodeStringES3_RS1_R10UErrorCode, @function
_ZNK6icu_6725RelativeDateTimeFormatter18combineDateAndTimeERKNS_13UnicodeStringES3_RS1_R10UErrorCode:
.LFB4576:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movq	%rsi, %r9
	movq	%rdx, %rsi
	movq	%r9, %rdx
	movq	21704(%rax), %rdi
	jmp	_ZNK6icu_6715SimpleFormatter6formatERKNS_13UnicodeStringES3_RS1_R10UErrorCode@PLT
	.cfi_endproc
.LFE4576:
	.size	_ZNK6icu_6725RelativeDateTimeFormatter18combineDateAndTimeERKNS_13UnicodeStringES3_RS1_R10UErrorCode, .-_ZNK6icu_6725RelativeDateTimeFormatter18combineDateAndTimeERKNS_13UnicodeStringES3_RS1_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6725RelativeDateTimeFormatter16adjustForContextERNS_13UnicodeStringE
	.type	_ZNK6icu_6725RelativeDateTimeFormatter16adjustForContextERNS_13UnicodeStringE, @function
_ZNK6icu_6725RelativeDateTimeFormatter16adjustForContextERNS_13UnicodeStringE:
.LFB4577:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpq	$0, 40(%rdi)
	je	.L841
	movswl	8(%rsi), %eax
	movq	%rdi, %rbx
	testw	%ax, %ax
	js	.L842
	sarl	$5, %eax
.L843:
	testl	%eax, %eax
	je	.L841
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movl	%eax, %edi
	call	u_islower_67@PLT
	testb	%al, %al
	jne	.L852
.L841:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L842:
	.cfi_restore_state
	movl	12(%rsi), %eax
	jmp	.L843
	.p2align 4,,10
	.p2align 3
.L852:
	leaq	_ZZNK6icu_6725RelativeDateTimeFormatter16adjustForContextERNS_13UnicodeStringEE13gBrkIterMutex(%rip), %rdi
	call	umtx_lock_67@PLT
	movq	40(%rbx), %rax
	leaq	48(%rbx), %rdx
	movq	%r12, %rdi
	movl	$768, %ecx
	movq	24(%rax), %rsi
	call	_ZN6icu_6713UnicodeString7toTitleEPNS_13BreakIteratorERKNS_6LocaleEj@PLT
	leaq	_ZZNK6icu_6725RelativeDateTimeFormatter16adjustForContextERNS_13UnicodeStringEE13gBrkIterMutex(%rip), %rdi
	call	umtx_unlock_67@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4577:
	.size	_ZNK6icu_6725RelativeDateTimeFormatter16adjustForContextERNS_13UnicodeStringE, .-_ZNK6icu_6725RelativeDateTimeFormatter16adjustForContextERNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6725RelativeDateTimeFormatter23checkNoAdjustForContextER10UErrorCode
	.type	_ZNK6icu_6725RelativeDateTimeFormatter23checkNoAdjustForContextER10UErrorCode, @function
_ZNK6icu_6725RelativeDateTimeFormatter23checkNoAdjustForContextER10UErrorCode:
.LFB4584:
	.cfi_startproc
	endbr64
	cmpq	$0, 40(%rdi)
	movl	$1, %eax
	je	.L853
	movl	$16, (%rsi)
	xorl	%eax, %eax
.L853:
	ret
	.cfi_endproc
.LFE4584:
	.size	_ZNK6icu_6725RelativeDateTimeFormatter23checkNoAdjustForContextER10UErrorCode, .-_ZNK6icu_6725RelativeDateTimeFormatter23checkNoAdjustForContextER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6730UFormattedRelativeDateTimeImplC2Ev
	.type	_ZN6icu_6730UFormattedRelativeDateTimeImplC2Ev, @function
_ZN6icu_6730UFormattedRelativeDateTimeImplC2Ev:
.LFB4603:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6725FormattedRelativeDateTimeE(%rip), %rax
	movl	$1430672896, (%rdi)
	movq	%rax, 24(%rdi)
	leaq	24(%rdi), %rax
	movl	$1179796564, 16(%rdi)
	movq	$0, 32(%rdi)
	movl	$27, 40(%rdi)
	movq	%rax, 8(%rdi)
	ret
	.cfi_endproc
.LFE4603:
	.size	_ZN6icu_6730UFormattedRelativeDateTimeImplC2Ev, .-_ZN6icu_6730UFormattedRelativeDateTimeImplC2Ev
	.globl	_ZN6icu_6730UFormattedRelativeDateTimeImplC1Ev
	.set	_ZN6icu_6730UFormattedRelativeDateTimeImplC1Ev,_ZN6icu_6730UFormattedRelativeDateTimeImplC2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6730UFormattedRelativeDateTimeImplD2Ev
	.type	_ZN6icu_6730UFormattedRelativeDateTimeImplD2Ev, @function
_ZN6icu_6730UFormattedRelativeDateTimeImplD2Ev:
.LFB4606:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6725FormattedRelativeDateTimeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	24(%rdi), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	32(%rdi), %r12
	movq	%rax, 24(%rdi)
	testq	%r12, %r12
	je	.L858
	movq	(%r12), %rax
	leaq	_ZN6icu_6729FormattedRelativeDateTimeDataD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L859
	leaq	16+_ZTVN6icu_6729FormattedRelativeDateTimeDataE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	call	_ZN6icu_6731FormattedValueStringBuilderImplD2Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L858:
	movq	$0, 32(%rbx)
	addq	$8, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6714FormattedValueD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L859:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	jmp	.L858
	.cfi_endproc
.LFE4606:
	.size	_ZN6icu_6730UFormattedRelativeDateTimeImplD2Ev, .-_ZN6icu_6730UFormattedRelativeDateTimeImplD2Ev
	.globl	_ZN6icu_6730UFormattedRelativeDateTimeImplD1Ev
	.set	_ZN6icu_6730UFormattedRelativeDateTimeImplD1Ev,_ZN6icu_6730UFormattedRelativeDateTimeImplD2Ev
	.p2align 4
	.globl	ureldatefmt_openResult_67
	.type	ureldatefmt_openResult_67, @function
ureldatefmt_openResult_67:
.LFB4608:
	.cfi_startproc
	endbr64
	movl	(%rdi), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L869
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$48, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L866
	leaq	16+_ZTVN6icu_6725FormattedRelativeDateTimeE(%rip), %rcx
	leaq	24(%rax), %rdx
	movl	$1430672896, (%rax)
	movl	$1179796564, 16(%rax)
	movq	%rcx, 24(%rax)
	movq	$0, 32(%rax)
	movl	$27, 40(%rax)
	movq	%rdx, 8(%rax)
.L864:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L869:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L866:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L864
	.cfi_endproc
.LFE4608:
	.size	ureldatefmt_openResult_67, .-ureldatefmt_openResult_67
	.p2align 4
	.globl	ureldatefmt_resultAsValue_67
	.type	ureldatefmt_resultAsValue_67, @function
ureldatefmt_resultAsValue_67:
.LFB4609:
	.cfi_startproc
	endbr64
	movl	(%rsi), %edx
	movq	%rdi, %rax
	testl	%edx, %edx
	jg	.L873
	testq	%rdi, %rdi
	je	.L877
	cmpl	$1179796564, 16(%rdi)
	jne	.L878
	ret
	.p2align 4,,10
	.p2align 3
.L878:
	movl	$3, (%rsi)
.L873:
	xorl	%eax, %eax
.L879:
	ret
	.p2align 4,,10
	.p2align 3
.L877:
	movl	$1, (%rsi)
	xorl	%eax, %eax
	jmp	.L879
	.cfi_endproc
.LFE4609:
	.size	ureldatefmt_resultAsValue_67, .-ureldatefmt_resultAsValue_67
	.p2align 4
	.globl	ureldatefmt_closeResult_67
	.type	ureldatefmt_closeResult_67, @function
ureldatefmt_closeResult_67:
.LFB4610:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L891
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	cmpl	$1179796564, 16(%rdi)
	jne	.L880
	movq	32(%rdi), %r13
	leaq	16+_ZTVN6icu_6725FormattedRelativeDateTimeE(%rip), %rax
	leaq	24(%rdi), %r14
	movq	%rax, 24(%rdi)
	testq	%r13, %r13
	je	.L882
	movq	0(%r13), %rax
	leaq	_ZN6icu_6729FormattedRelativeDateTimeDataD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L883
	leaq	16+_ZTVN6icu_6729FormattedRelativeDateTimeDataE(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, 0(%r13)
	call	_ZN6icu_6731FormattedValueStringBuilderImplD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L882:
	movq	$0, 32(%r12)
	movq	%r14, %rdi
	call	_ZN6icu_6714FormattedValueD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L880:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L883:
	.cfi_restore_state
	movq	%r13, %rdi
	call	*%rax
	jmp	.L882
	.p2align 4,,10
	.p2align 3
.L891:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	ret
	.cfi_endproc
.LFE4610:
	.size	ureldatefmt_closeResult_67, .-ureldatefmt_closeResult_67
	.p2align 4
	.globl	ureldatefmt_formatNumeric_67
	.type	ureldatefmt_formatNumeric_67, @function
ureldatefmt_formatNumeric_67:
.LFB4613:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$344, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -360(%rbp)
	movl	(%r8), %edi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edi, %edi
	jg	.L894
	movl	%esi, %r9d
	movq	%rdx, %rbx
	movl	%ecx, %r12d
	movq	%r8, %r13
	testq	%rdx, %rdx
	je	.L926
	testl	%ecx, %ecx
	js	.L897
	movl	$2, %edx
	leaq	-336(%rbp), %r15
	movl	%esi, -372(%rbp)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%dx, -328(%rbp)
	movq	%rbx, %rsi
	xorl	%edx, %edx
	movq	%r15, %rdi
	movsd	%xmm0, -368(%rbp)
	movq	%rax, -336(%rbp)
	call	_ZN6icu_6713UnicodeString5setToEPDsii@PLT
	movsd	-368(%rbp), %xmm0
	movl	-372(%rbp), %r9d
.L911:
	movzbl	_ZN6icu_67L16kRDTNumericFieldE(%rip), %esi
	leaq	-208(%rbp), %r14
	movl	%r9d, -372(%rbp)
	movq	%r14, %rdi
	movsd	%xmm0, -368(%rbp)
	call	_ZN6icu_6731FormattedValueStringBuilderImplC2ENS_22FormattedStringBuilder5FieldE@PLT
	movl	-372(%rbp), %r9d
	movq	%r13, %rcx
	movsd	-368(%rbp), %xmm0
	movq	-360(%rbp), %rdi
	leaq	16+_ZTVN6icu_6729FormattedRelativeDateTimeDataE(%rip), %rax
	movq	%r14, %rdx
	movl	%r9d, %esi
	movq	%rax, -208(%rbp)
	call	_ZNK6icu_6725RelativeDateTimeFormatter17formatNumericImplEd21URelativeDateTimeUnitRNS_29FormattedRelativeDateTimeDataER10UErrorCode
	movl	0(%r13), %esi
	testl	%esi, %esi
	jle	.L927
.L900:
	leaq	16+_ZTVN6icu_6729FormattedRelativeDateTimeDataE(%rip), %rax
	movq	%r14, %rdi
	xorl	%r14d, %r14d
	movq	%rax, -208(%rbp)
	call	_ZN6icu_6731FormattedValueStringBuilderImplD2Ev@PLT
	movl	0(%r13), %ecx
	testl	%ecx, %ecx
	jg	.L910
	leaq	-344(%rbp), %rsi
	movq	%r13, %rcx
	movl	%r12d, %edx
	movq	%r15, %rdi
	movq	%rbx, -344(%rbp)
	call	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode@PLT
	movl	%eax, %r14d
.L910:
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L894:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L928
	addq	$344, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L926:
	.cfi_restore_state
	testl	%ecx, %ecx
	jne	.L897
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	leaq	-336(%rbp), %r15
	movq	%rax, -336(%rbp)
	movl	$2, %eax
	movw	%ax, -328(%rbp)
	jmp	.L911
	.p2align 4,,10
	.p2align 3
.L897:
	movl	$1, 0(%r13)
	xorl	%r14d, %r14d
	jmp	.L894
	.p2align 4,,10
	.p2align 3
.L927:
	leaq	-272(%rbp), %r9
	leaq	-200(%rbp), %rsi
	movq	%r9, %rdi
	movq	%r9, -368(%rbp)
	call	_ZNK6icu_6722FormattedStringBuilder15toUnicodeStringEv@PLT
	movq	-360(%rbp), %rax
	movq	-368(%rbp), %r9
	cmpq	$0, 40(%rax)
	je	.L902
	movswl	-264(%rbp), %ecx
	testw	%cx, %cx
	js	.L903
	sarl	$5, %ecx
	jne	.L905
.L909:
	movq	%r9, %rsi
	movq	%r15, %rdi
	xorl	%edx, %edx
	movq	%r9, -360(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	-360(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L900
	.p2align 4,,10
	.p2align 3
.L903:
	movl	-260(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L909
.L905:
	movq	%r9, %rdi
	xorl	%esi, %esi
	movq	%r9, -368(%rbp)
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movl	%eax, %edi
	call	u_islower_67@PLT
	movq	-368(%rbp), %r9
	testb	%al, %al
	jne	.L929
.L902:
	movswl	-264(%rbp), %ecx
	movl	%ecx, %eax
	sarl	$5, %ecx
	testw	%ax, %ax
	cmovs	-260(%rbp), %ecx
	jmp	.L909
	.p2align 4,,10
	.p2align 3
.L929:
	leaq	_ZZNK6icu_6725RelativeDateTimeFormatter16adjustForContextERNS_13UnicodeStringEE13gBrkIterMutex(%rip), %rdi
	call	umtx_lock_67@PLT
	movq	-360(%rbp), %rax
	movl	$768, %ecx
	movq	-368(%rbp), %r9
	leaq	48(%rax), %rdx
	movq	40(%rax), %rax
	movq	%r9, %rdi
	movq	%r9, -360(%rbp)
	movq	24(%rax), %rsi
	call	_ZN6icu_6713UnicodeString7toTitleEPNS_13BreakIteratorERKNS_6LocaleEj@PLT
	leaq	_ZZNK6icu_6725RelativeDateTimeFormatter16adjustForContextERNS_13UnicodeStringEE13gBrkIterMutex(%rip), %rdi
	call	umtx_unlock_67@PLT
	movq	-360(%rbp), %r9
	jmp	.L902
.L928:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4613:
	.size	ureldatefmt_formatNumeric_67, .-ureldatefmt_formatNumeric_67
	.p2align 4
	.globl	ureldatefmt_formatNumericToResult_67
	.type	ureldatefmt_formatNumericToResult_67, @function
ureldatefmt_formatNumericToResult_67:
.LFB4614:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L930
	movq	%rdi, %r15
	movl	%esi, %r14d
	movq	%rdx, %rbx
	movq	%rcx, %r12
	testq	%rdx, %rdx
	je	.L951
	cmpl	$1179796564, 16(%rdx)
	jne	.L952
.L933:
	cmpq	$0, 40(%r15)
	movsd	%xmm0, -88(%rbp)
	je	.L953
.L934:
	leaq	16+_ZTVN6icu_6725FormattedRelativeDateTimeE(%rip), %r14
	movl	$16, (%r12)
	movq	%r14, -80(%rbp)
	movq	$0, -72(%rbp)
	movl	$16, -64(%rbp)
.L937:
	movq	32(%rbx), %r12
	testq	%r12, %r12
	je	.L941
	movq	(%r12), %rax
	leaq	_ZN6icu_6729FormattedRelativeDateTimeDataD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L942
	leaq	16+_ZTVN6icu_6729FormattedRelativeDateTimeDataE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	call	_ZN6icu_6731FormattedValueStringBuilderImplD2Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L941:
	movq	-72(%rbp), %rax
	leaq	-80(%rbp), %rdi
	movq	%r14, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	%rax, 32(%rbx)
	movl	-64(%rbp), %eax
	movl	$27, -64(%rbp)
	movl	%eax, 40(%rbx)
	call	_ZN6icu_6714FormattedValueD2Ev@PLT
.L930:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L954
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L953:
	.cfi_restore_state
	movl	$152, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movsd	-88(%rbp), %xmm0
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L955
	movzbl	_ZN6icu_67L16kRDTNumericFieldE(%rip), %esi
	movq	%rax, %rdi
	movsd	%xmm0, -88(%rbp)
	call	_ZN6icu_6731FormattedValueStringBuilderImplC2ENS_22FormattedStringBuilder5FieldE@PLT
	movl	(%r12), %edx
	movsd	-88(%rbp), %xmm0
	leaq	16+_ZTVN6icu_6729FormattedRelativeDateTimeDataE(%rip), %rax
	movq	%rax, 0(%r13)
	testl	%edx, %edx
	jle	.L956
	leaq	16+_ZTVN6icu_6725FormattedRelativeDateTimeE(%rip), %r14
	movq	$0, -72(%rbp)
	movq	%r13, %rdi
	movq	%r14, -80(%rbp)
	movl	%edx, -64(%rbp)
	movq	%rax, 0(%r13)
	call	_ZN6icu_6731FormattedValueStringBuilderImplD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	jmp	.L937
	.p2align 4,,10
	.p2align 3
.L952:
	xorl	%ebx, %ebx
	cmpq	$0, 40(%r15)
	movl	$3, (%rcx)
	movsd	%xmm0, -88(%rbp)
	jne	.L934
	jmp	.L953
	.p2align 4,,10
	.p2align 3
.L956:
	movl	%r14d, %esi
	movq	%r12, %rcx
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZNK6icu_6725RelativeDateTimeFormatter17formatNumericImplEd21URelativeDateTimeUnitRNS_29FormattedRelativeDateTimeDataER10UErrorCode
	leaq	16+_ZTVN6icu_6725FormattedRelativeDateTimeE(%rip), %r14
	leaq	8(%r13), %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6722FormattedStringBuilder15writeTerminatorER10UErrorCode@PLT
	movq	%r14, -80(%rbp)
	movq	%r13, -72(%rbp)
	movl	$0, -64(%rbp)
	jmp	.L937
	.p2align 4,,10
	.p2align 3
.L942:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L941
.L955:
	movl	(%r12), %eax
	testl	%eax, %eax
	jle	.L957
.L940:
	leaq	16+_ZTVN6icu_6725FormattedRelativeDateTimeE(%rip), %r14
	movq	$0, -72(%rbp)
	movq	%r14, -80(%rbp)
	movl	%eax, -64(%rbp)
	jmp	.L937
	.p2align 4,,10
	.p2align 3
.L957:
	movl	$7, (%r12)
	movl	$7, %eax
	jmp	.L940
	.p2align 4,,10
	.p2align 3
.L951:
	movl	$1, (%rcx)
	jmp	.L933
.L954:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4614:
	.size	ureldatefmt_formatNumericToResult_67, .-ureldatefmt_formatNumericToResult_67
	.p2align 4
	.globl	ureldatefmt_format_67
	.type	ureldatefmt_format_67, @function
ureldatefmt_format_67:
.LFB4615:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$344, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -360(%rbp)
	movl	(%r8), %r9d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r9d, %r9d
	jg	.L958
	movl	%esi, %r9d
	movq	%rdx, %rbx
	movl	%ecx, %r12d
	movq	%r8, %r13
	testq	%rdx, %rdx
	je	.L1064
	testl	%ecx, %ecx
	js	.L961
	movl	$2, %edx
	leaq	-336(%rbp), %r15
	movl	%esi, -376(%rbp)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%dx, -328(%rbp)
	movq	%rbx, %rsi
	xorl	%edx, %edx
	movq	%r15, %rdi
	movsd	%xmm0, -368(%rbp)
	movq	%rax, -336(%rbp)
	call	_ZN6icu_6713UnicodeString5setToEPDsii@PLT
	movsd	-368(%rbp), %xmm0
	movl	-376(%rbp), %r9d
.L1034:
	movzbl	_ZN6icu_67L16kRDTNumericFieldE(%rip), %esi
	leaq	-208(%rbp), %r14
	movl	%r9d, -376(%rbp)
	movq	%r14, %rdi
	movsd	%xmm0, -368(%rbp)
	call	_ZN6icu_6731FormattedValueStringBuilderImplC2ENS_22FormattedStringBuilder5FieldE@PLT
	movl	0(%r13), %edi
	leaq	16+_ZTVN6icu_6729FormattedRelativeDateTimeDataE(%rip), %rax
	movq	%rax, -208(%rbp)
	testl	%edi, %edi
	jg	.L965
	movsd	-368(%rbp), %xmm0
	comisd	.LC1(%rip), %xmm0
	movl	-376(%rbp), %r9d
	ja	.L1065
.L966:
	movq	-360(%rbp), %rdi
	movq	%r13, %rcx
	movq	%r14, %rdx
	movl	%r9d, %esi
	call	_ZNK6icu_6725RelativeDateTimeFormatter17formatNumericImplEd21URelativeDateTimeUnitRNS_29FormattedRelativeDateTimeDataER10UErrorCode
.L1013:
	movl	0(%r13), %esi
	testl	%esi, %esi
	jg	.L965
	leaq	-272(%rbp), %r9
	leaq	-200(%rbp), %rsi
	movq	%r9, %rdi
	movq	%r9, -368(%rbp)
	call	_ZNK6icu_6722FormattedStringBuilder15toUnicodeStringEv@PLT
	movq	-360(%rbp), %rax
	movq	-368(%rbp), %r9
	cmpq	$0, 40(%rax)
	je	.L1016
	movswl	-264(%rbp), %ecx
	testw	%cx, %cx
	js	.L1017
	sarl	$5, %ecx
	jne	.L1019
.L1023:
	movq	%r9, %rsi
	movq	%r15, %rdi
	xorl	%edx, %edx
	movq	%r9, -360(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	-360(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L965:
	leaq	16+_ZTVN6icu_6729FormattedRelativeDateTimeDataE(%rip), %rax
	movq	%r14, %rdi
	xorl	%r14d, %r14d
	movq	%rax, -208(%rbp)
	call	_ZN6icu_6731FormattedValueStringBuilderImplD2Ev@PLT
	movl	0(%r13), %ecx
	testl	%ecx, %ecx
	jg	.L1024
	leaq	-344(%rbp), %rsi
	movq	%r13, %rcx
	movl	%r12d, %edx
	movq	%r15, %rdi
	movq	%rbx, -344(%rbp)
	call	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode@PLT
	movl	%eax, %r14d
.L1024:
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L958:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1066
	addq	$344, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1064:
	.cfi_restore_state
	testl	%ecx, %ecx
	jne	.L961
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	leaq	-336(%rbp), %r15
	movq	%rax, -336(%rbp)
	movl	$2, %eax
	movw	%ax, -328(%rbp)
	jmp	.L1034
	.p2align 4,,10
	.p2align 3
.L1065:
	movsd	.LC2(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jbe	.L966
	movsd	.LC3(%rip), %xmm1
	pxor	%xmm2, %xmm2
	mulsd	%xmm0, %xmm1
	comisd	%xmm1, %xmm2
	ja	.L1067
	addsd	.LC5(%rip), %xmm1
	cvttsd2sil	%xmm1, %eax
.L971:
	testl	%eax, %eax
	je	.L972
	jg	.L973
	cmpl	$-200, %eax
	je	.L974
	cmpl	$-100, %eax
	jne	.L966
	movl	$1, %esi
	movl	$1, %edx
.L975:
	cmpl	$14, %r9d
	ja	.L966
	leaq	.L1035(%rip), %rdi
	movl	%r9d, %ecx
	movslq	(%rdi,%rcx,4), %rax
	addq	%rdi, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L1035:
	.long	.L1026-.L1035
	.long	.L1046-.L1035
	.long	.L979-.L1035
	.long	.L982-.L1035
	.long	.L984-.L1035
	.long	.L1008-.L1035
	.long	.L1047-.L1035
	.long	.L966-.L1035
	.long	.L988-.L1035
	.long	.L991-.L1035
	.long	.L994-.L1035
	.long	.L997-.L1035
	.long	.L1000-.L1035
	.long	.L1003-.L1035
	.long	.L1006-.L1035
	.text
	.p2align 4,,10
	.p2align 3
.L1017:
	movl	-260(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L1023
.L1019:
	movq	%r9, %rdi
	xorl	%esi, %esi
	movq	%r9, -368(%rbp)
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movl	%eax, %edi
	call	u_islower_67@PLT
	movq	-368(%rbp), %r9
	testb	%al, %al
	jne	.L1068
.L1016:
	movswl	-264(%rbp), %ecx
	movl	%ecx, %eax
	sarl	$5, %ecx
	testw	%ax, %ax
	cmovs	-260(%rbp), %ecx
	jmp	.L1023
	.p2align 4,,10
	.p2align 3
.L961:
	movl	$1, 0(%r13)
	xorl	%r14d, %r14d
	jmp	.L958
	.p2align 4,,10
	.p2align 3
.L1068:
	leaq	_ZZNK6icu_6725RelativeDateTimeFormatter16adjustForContextERNS_13UnicodeStringEE13gBrkIterMutex(%rip), %rdi
	call	umtx_lock_67@PLT
	movq	-360(%rbp), %rax
	movl	$768, %ecx
	movq	-368(%rbp), %r9
	leaq	48(%rax), %rdx
	movq	40(%rax), %rax
	movq	%r9, %rdi
	movq	%r9, -360(%rbp)
	movq	24(%rax), %rsi
	call	_ZN6icu_6713UnicodeString7toTitleEPNS_13BreakIteratorERKNS_6LocaleEj@PLT
	leaq	_ZZNK6icu_6725RelativeDateTimeFormatter16adjustForContextERNS_13UnicodeStringEE13gBrkIterMutex(%rip), %rdi
	call	umtx_unlock_67@PLT
	movq	-360(%rbp), %r9
	jmp	.L1016
	.p2align 4,,10
	.p2align 3
.L1067:
	subsd	.LC5(%rip), %xmm1
	cvttsd2sil	%xmm1, %eax
	jmp	.L971
.L1006:
	movl	$36, %eax
.L978:
	movl	%edx, %esi
.L1010:
	movq	-360(%rbp), %rdi
	addq	%rax, %rsi
	movq	8(%rdi), %rcx
	movslq	32(%rdi), %rdx
	jmp	.L1012
	.p2align 4,,10
	.p2align 3
.L1070:
	movslq	21688(%rcx,%rdx,4), %rdx
	cmpl	$-1, %edx
	je	.L1069
.L1012:
	imulq	$90, %rdx, %rax
	addq	%rsi, %rax
	salq	$6, %rax
	leaq	24(%rcx,%rax), %r10
	movswl	8(%r10), %eax
	shrl	$5, %eax
	je	.L1070
.L1011:
	movzbl	_ZN6icu_67L16kRDTLiteralFieldE(%rip), %ecx
	movl	-68(%rbp), %esi
	movq	%r13, %r8
	movq	%r10, %rdx
	leaq	-200(%rbp), %rdi
	movl	%r9d, -380(%rbp)
	movsd	%xmm0, -376(%rbp)
	movq	%rdi, -368(%rbp)
	call	_ZN6icu_6722FormattedStringBuilder6insertEiRKNS_13UnicodeStringENS0_5FieldER10UErrorCode@PLT
	movq	-368(%rbp), %rdi
	call	_ZNK6icu_6722FormattedStringBuilder6lengthEv@PLT
	movsd	-376(%rbp), %xmm0
	movl	-380(%rbp), %r9d
	testl	%eax, %eax
	je	.L966
	jmp	.L1013
.L1003:
	movl	$30, %eax
	jmp	.L978
.L1000:
	movl	$24, %eax
	jmp	.L978
.L997:
	movl	$18, %eax
	jmp	.L978
.L994:
	movl	$12, %eax
	jmp	.L978
.L991:
	movl	$6, %eax
	jmp	.L978
.L988:
	xorl	%eax, %eax
	jmp	.L978
.L1008:
	movl	$78, %eax
	jmp	.L978
.L984:
	movl	$42, %eax
	jmp	.L978
.L982:
	movl	$48, %eax
	jmp	.L978
.L979:
	movl	$54, %eax
	jmp	.L978
.L1047:
	movl	$84, %eax
	jmp	.L1010
.L1046:
	movl	$72, %eax
	jmp	.L978
.L1026:
	movl	$60, %eax
	jmp	.L978
	.p2align 4,,10
	.p2align 3
.L974:
	cmpl	$14, %r9d
	ja	.L966
	leaq	.L1027(%rip), %rcx
	movl	%r9d, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L1027:
	.long	.L1009-.L1027
	.long	.L1042-.L1027
	.long	.L977-.L1027
	.long	.L981-.L1027
	.long	.L983-.L1027
	.long	.L1007-.L1027
	.long	.L1025-.L1027
	.long	.L966-.L1027
	.long	.L987-.L1027
	.long	.L990-.L1027
	.long	.L993-.L1027
	.long	.L996-.L1027
	.long	.L999-.L1027
	.long	.L1002-.L1027
	.long	.L1005-.L1027
	.text
.L1005:
	movl	$36, %eax
	xorl	%edx, %edx
	jmp	.L978
.L1002:
	movl	$30, %eax
	xorl	%edx, %edx
	jmp	.L978
.L999:
	movl	$24, %eax
	xorl	%edx, %edx
	jmp	.L978
.L996:
	movl	$18, %eax
	xorl	%edx, %edx
	jmp	.L978
.L993:
	movl	$12, %eax
	xorl	%edx, %edx
	jmp	.L978
.L990:
	movl	$6, %eax
	xorl	%edx, %edx
	jmp	.L978
.L987:
	xorl	%eax, %eax
	xorl	%edx, %edx
	jmp	.L978
.L1025:
	movl	$84, %eax
	xorl	%edx, %edx
	jmp	.L978
.L1007:
	movl	$78, %eax
	xorl	%edx, %edx
	jmp	.L978
.L983:
	movl	$42, %eax
	xorl	%edx, %edx
	jmp	.L978
.L981:
	movl	$48, %eax
	xorl	%edx, %edx
	jmp	.L978
.L977:
	movl	$54, %eax
	xorl	%edx, %edx
	jmp	.L978
.L1042:
	movl	$72, %eax
	xorl	%edx, %edx
	jmp	.L978
.L1009:
	movl	%r9d, %edx
	movl	$60, %eax
	jmp	.L978
.L972:
	cmpl	$14, %r9d
	ja	.L966
	leaq	.L1028(%rip), %rcx
	movl	%r9d, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L1028:
	.long	.L1033-.L1028
	.long	.L1032-.L1028
	.long	.L1031-.L1028
	.long	.L1030-.L1028
	.long	.L1043-.L1028
	.long	.L1029-.L1028
	.long	.L1044-.L1028
	.long	.L1045-.L1028
	.long	.L985-.L1028
	.long	.L989-.L1028
	.long	.L992-.L1028
	.long	.L995-.L1028
	.long	.L998-.L1028
	.long	.L1001-.L1028
	.long	.L1004-.L1028
	.text
.L1045:
	movl	$66, %eax
	movl	$5, %esi
	jmp	.L1010
.L1044:
	movl	$84, %eax
	movl	$2, %edx
	jmp	.L978
.L1043:
	movl	$7, %eax
.L986:
	leaq	(%rax,%rax,2), %rax
	movl	$2, %esi
	addq	%rax, %rax
	jmp	.L1010
.L1030:
	movl	$8, %eax
	jmp	.L986
.L1031:
	movl	$9, %eax
	jmp	.L986
.L1029:
	movl	$13, %eax
	jmp	.L986
.L1032:
	movl	$12, %eax
	jmp	.L986
.L1033:
	movl	$10, %eax
	jmp	.L986
.L1004:
	movl	$6, %eax
	jmp	.L986
.L1001:
	movl	$5, %eax
	jmp	.L986
.L998:
	movl	$4, %eax
	jmp	.L986
.L995:
	movl	$3, %eax
	jmp	.L986
.L992:
	movl	$2, %eax
	jmp	.L986
.L989:
	movl	$1, %eax
	jmp	.L986
.L985:
	xorl	%eax, %eax
	jmp	.L986
	.p2align 4,,10
	.p2align 3
.L973:
	cmpl	$100, %eax
	je	.L1039
	cmpl	$200, %eax
	jne	.L966
	movl	$4, %esi
	movl	$4, %edx
	jmp	.L975
	.p2align 4,,10
	.p2align 3
.L1039:
	movl	$3, %esi
	movl	$3, %edx
	jmp	.L975
	.p2align 4,,10
	.p2align 3
.L1069:
	leaq	21624(%rcx), %r10
	jmp	.L1011
.L1066:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4615:
	.size	ureldatefmt_format_67, .-ureldatefmt_format_67
	.p2align 4
	.globl	ureldatefmt_formatToResult_67
	.type	ureldatefmt_formatToResult_67, @function
ureldatefmt_formatToResult_67:
.LFB4616:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L1071
	movq	%rdi, %r15
	movl	%esi, %r14d
	movq	%rdx, %rbx
	movq	%rcx, %r12
	testq	%rdx, %rdx
	je	.L1092
	cmpl	$1179796564, 16(%rdx)
	jne	.L1093
.L1074:
	cmpq	$0, 40(%r15)
	movsd	%xmm0, -88(%rbp)
	je	.L1094
.L1075:
	leaq	16+_ZTVN6icu_6725FormattedRelativeDateTimeE(%rip), %r14
	movl	$16, (%r12)
	movq	%r14, -80(%rbp)
	movq	$0, -72(%rbp)
	movl	$16, -64(%rbp)
.L1078:
	movq	32(%rbx), %r12
	testq	%r12, %r12
	je	.L1082
	movq	(%r12), %rax
	leaq	_ZN6icu_6729FormattedRelativeDateTimeDataD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1083
	leaq	16+_ZTVN6icu_6729FormattedRelativeDateTimeDataE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	call	_ZN6icu_6731FormattedValueStringBuilderImplD2Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L1082:
	movq	-72(%rbp), %rax
	leaq	-80(%rbp), %rdi
	movq	%r14, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	%rax, 32(%rbx)
	movl	-64(%rbp), %eax
	movl	$27, -64(%rbp)
	movl	%eax, 40(%rbx)
	call	_ZN6icu_6714FormattedValueD2Ev@PLT
.L1071:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1095
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1094:
	.cfi_restore_state
	movl	$152, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movsd	-88(%rbp), %xmm0
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L1096
	movzbl	_ZN6icu_67L16kRDTNumericFieldE(%rip), %esi
	movq	%rax, %rdi
	movsd	%xmm0, -88(%rbp)
	call	_ZN6icu_6731FormattedValueStringBuilderImplC2ENS_22FormattedStringBuilder5FieldE@PLT
	movl	(%r12), %edx
	movsd	-88(%rbp), %xmm0
	leaq	16+_ZTVN6icu_6729FormattedRelativeDateTimeDataE(%rip), %rax
	movq	%rax, 0(%r13)
	testl	%edx, %edx
	jle	.L1097
	leaq	16+_ZTVN6icu_6725FormattedRelativeDateTimeE(%rip), %r14
	movq	$0, -72(%rbp)
	movq	%r13, %rdi
	movq	%r14, -80(%rbp)
	movl	%edx, -64(%rbp)
	movq	%rax, 0(%r13)
	call	_ZN6icu_6731FormattedValueStringBuilderImplD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	jmp	.L1078
	.p2align 4,,10
	.p2align 3
.L1093:
	xorl	%ebx, %ebx
	cmpq	$0, 40(%r15)
	movl	$3, (%rcx)
	movsd	%xmm0, -88(%rbp)
	jne	.L1075
	jmp	.L1094
	.p2align 4,,10
	.p2align 3
.L1097:
	movl	%r14d, %esi
	movq	%r12, %rcx
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZNK6icu_6725RelativeDateTimeFormatter18formatRelativeImplEd21URelativeDateTimeUnitRNS_29FormattedRelativeDateTimeDataER10UErrorCode
	leaq	16+_ZTVN6icu_6725FormattedRelativeDateTimeE(%rip), %r14
	leaq	8(%r13), %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6722FormattedStringBuilder15writeTerminatorER10UErrorCode@PLT
	movq	%r14, -80(%rbp)
	movq	%r13, -72(%rbp)
	movl	$0, -64(%rbp)
	jmp	.L1078
	.p2align 4,,10
	.p2align 3
.L1083:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1082
.L1096:
	movl	(%r12), %eax
	testl	%eax, %eax
	jle	.L1098
.L1081:
	leaq	16+_ZTVN6icu_6725FormattedRelativeDateTimeE(%rip), %r14
	movq	$0, -72(%rbp)
	movq	%r14, -80(%rbp)
	movl	%eax, -64(%rbp)
	jmp	.L1078
	.p2align 4,,10
	.p2align 3
.L1098:
	movl	$7, (%r12)
	movl	$7, %eax
	jmp	.L1081
	.p2align 4,,10
	.p2align 3
.L1092:
	movl	$1, (%rcx)
	jmp	.L1074
.L1095:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4616:
	.size	ureldatefmt_formatToResult_67, .-ureldatefmt_formatToResult_67
	.p2align 4
	.globl	ureldatefmt_combineDateAndTime_67
	.type	ureldatefmt_combineDateAndTime_67, @function
ureldatefmt_combineDateAndTime_67:
.LFB4617:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%r8d, %r12d
	pushq	%rbx
	subq	$264, %rsp
	.cfi_offset 3, -56
	movq	24(%rbp), %r8
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%r8), %r10d
	testl	%r10d, %r10d
	jg	.L1099
	movq	%rdi, %r15
	movq	%rcx, %r13
	movq	%r9, %rbx
	testq	%r9, %r9
	je	.L1112
	movl	16(%rbp), %ecx
	testl	%ecx, %ecx
	js	.L1102
	testq	%rsi, %rsi
	je	.L1113
	cmpl	$-1, %edx
	jl	.L1102
.L1105:
	testq	%r13, %r13
	je	.L1114
	cmpl	$-1, %r12d
	jge	.L1103
	.p2align 4,,10
	.p2align 3
.L1102:
	movl	$1, (%r8)
	xorl	%eax, %eax
.L1099:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1115
	addq	$264, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1112:
	.cfi_restore_state
	movl	16(%rbp), %edi
	testl	%edi, %edi
	jne	.L1102
.L1103:
	leaq	-264(%rbp), %r9
	movq	%rsi, -264(%rbp)
	xorl	%esi, %esi
	cmpl	$-1, %edx
	leaq	-256(%rbp), %r14
	movl	%edx, %ecx
	sete	%sil
	movq	%r9, %rdx
	movq	%r14, %rdi
	movq	%r8, -296(%rbp)
	movq	%r9, -280(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	-280(%rbp), %r9
	movq	%r13, -264(%rbp)
	xorl	%esi, %esi
	cmpl	$-1, %r12d
	leaq	-192(%rbp), %r13
	sete	%sil
	movl	%r12d, %ecx
	movq	%r9, %rdx
	movq	%r13, %rdi
	movq	%r9, -288(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movl	16(%rbp), %ecx
	leaq	-128(%rbp), %r12
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPDsii@PLT
	movq	8(%r15), %rax
	movq	%r14, %rdx
	movq	%r12, %rcx
	movq	-296(%rbp), %r8
	movq	%r13, %rsi
	movq	21704(%rax), %rdi
	movq	%r8, -280(%rbp)
	call	_ZNK6icu_6715SimpleFormatter6formatERKNS_13UnicodeStringES3_RS1_R10UErrorCode@PLT
	movq	-280(%rbp), %r8
	xorl	%eax, %eax
	movq	-288(%rbp), %r9
	movl	(%r8), %edx
	testl	%edx, %edx
	jg	.L1107
	movl	16(%rbp), %edx
	movq	%r8, %rcx
	movq	%r9, %rsi
	movq	%r12, %rdi
	movq	%rbx, -264(%rbp)
	call	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode@PLT
.L1107:
	movq	%r12, %rdi
	movl	%eax, -280(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-280(%rbp), %eax
	jmp	.L1099
	.p2align 4,,10
	.p2align 3
.L1114:
	testl	%r12d, %r12d
	je	.L1103
	jmp	.L1102
	.p2align 4,,10
	.p2align 3
.L1113:
	testl	%edx, %edx
	je	.L1105
	jmp	.L1102
.L1115:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4617:
	.size	ureldatefmt_combineDateAndTime_67, .-ureldatefmt_combineDateAndTime_67
	.p2align 4
	.type	_ZN6icu_67L16loadWeekdayNamesEPA15_A6_NS_13UnicodeStringEPKcR10UErrorCode, @function
_ZN6icu_67L16loadWeekdayNamesEPA15_A6_NS_13UnicodeStringEPKcR10UErrorCode:
.LFB4514:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$1528, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rdx), %r8d
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	testl	%r8d, %r8d
	jle	.L1128
.L1116:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1129
	addq	$1528, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1128:
	.cfi_restore_state
	leaq	-1520(%rbp), %r14
	movq	%rdx, -1544(%rbp)
	movq	%rdi, %rbx
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	%r14, -1552(%rbp)
	leaq	-1296(%rbp), %r13
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	movq	%r14, %rsi
	movq	-1544(%rbp), %rax
	movq	%r13, %rdi
	leaq	_ZN6icu_67L28styleToDateFormatSymbolWidthE(%rip), %r12
	addq	$3008, %rbx
	movq	%rax, %rdx
	movq	%rax, -1560(%rbp)
	call	_ZN6icu_6717DateFormatSymbolsC1ERKNS_6LocaleER10UErrorCode@PLT
	leaq	-1524(%rbp), %rax
	movq	%rax, -1544(%rbp)
	movq	-1560(%rbp), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jg	.L1127
.L1120:
	movl	(%r12), %ecx
	movq	-1544(%rbp), %rsi
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	-2688(%rbx), %r15
	call	_ZNK6icu_6717DateFormatSymbols11getWeekdaysERiNS0_13DtContextTypeENS0_11DtWidthTypeE@PLT
	leaq	64(%rax), %r14
	.p2align 4,,10
	.p2align 3
.L1119:
	movq	%r14, %rsi
	movq	%r15, %rdi
	addq	$384, %r15
	addq	$64, %r14
	call	_ZN6icu_6713UnicodeString12fastCopyFromERKS0_@PLT
	cmpq	%rbx, %r15
	jne	.L1119
	addq	$4, %r12
	leaq	12+_ZN6icu_67L28styleToDateFormatSymbolWidthE(%rip), %rax
	leaq	5760(%r15), %rbx
	cmpq	%rax, %r12
	jne	.L1120
.L1127:
	movq	%r13, %rdi
	call	_ZN6icu_6717DateFormatSymbolsD1Ev@PLT
	movq	-1552(%rbp), %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	jmp	.L1116
.L1129:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4514:
	.size	_ZN6icu_67L16loadWeekdayNamesEPA15_A6_NS_13UnicodeStringEPKcR10UErrorCode, .-_ZN6icu_67L16loadWeekdayNamesEPA15_A6_NS_13UnicodeStringEPKcR10UErrorCode
	.section	.rodata.str1.1
.LC19:
	.string	"fields"
.LC20:
	.string	"calendar/default"
.LC21:
	.string	"calendar/"
.LC22:
	.string	"/DateTimePatterns"
	.section	.rodata.str2.2,"aMS",@progbits,2
	.align 2
.LC23:
	.string	"{"
	.string	"1"
	.string	"}"
	.string	" "
	.string	"{"
	.string	"0"
	.string	"}"
	.string	""
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEE12createObjectEPKvR10UErrorCode
	.type	_ZNK6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEE12createObjectEPKvR10UErrorCode, @function
_ZNK6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEE12createObjectEPKvR10UErrorCode:
.LFB4517:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$360, %rsp
	movq	56(%rdi), %r14
	xorl	%edi, %edi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r14, %rsi
	call	ures_open_67@PLT
	movq	%rax, %r12
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L1131
	movl	$21712, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1132
	movq	$0, 8(%rax)
	leaq	24(%r15), %r9
	leaq	17304(%r15), %rdx
	movq	$0, 16(%rax)
	leaq	16+_ZTVN6icu_6725RelativeDateTimeCacheDataE(%rip), %rax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %r8
	movq	%rax, (%r15)
	movq	%r9, %rax
.L1133:
	movl	$2, %ecx
	movl	$2, %esi
	movq	%r8, (%rax)
	movl	$2, %edi
	movl	$2, %r10d
	movl	$2, %r11d
	movl	$2, %r13d
	movw	%cx, 8(%rax)
	movw	%si, 72(%rax)
	movl	$2, %ecx
	movl	$2, %esi
	addq	$5760, %rax
	movw	%di, -5624(%rax)
	movl	$2, %edi
	movw	%r10w, -5560(%rax)
	movl	$2, %r10d
	movw	%r11w, -5496(%rax)
	movl	$2, %r11d
	movw	%r13w, -5432(%rax)
	movl	$2, %r13d
	movw	%cx, -5368(%rax)
	movl	$2, %ecx
	movw	%si, -5304(%rax)
	movl	$2, %esi
	movw	%di, -5240(%rax)
	movl	$2, %edi
	movw	%r10w, -5176(%rax)
	movl	$2, %r10d
	movw	%r11w, -5112(%rax)
	movl	$2, %r11d
	movw	%r13w, -5048(%rax)
	movl	$2, %r13d
	movw	%cx, -4984(%rax)
	movl	$2, %ecx
	movw	%si, -4920(%rax)
	movl	$2, %esi
	movw	%di, -4856(%rax)
	movl	$2, %edi
	movw	%r10w, -4792(%rax)
	movl	$2, %r10d
	movq	%r8, -5696(%rax)
	movq	%r8, -5632(%rax)
	movq	%r8, -5568(%rax)
	movq	%r8, -5504(%rax)
	movq	%r8, -5440(%rax)
	movq	%r8, -5376(%rax)
	movq	%r8, -5312(%rax)
	movq	%r8, -5248(%rax)
	movq	%r8, -5184(%rax)
	movq	%r8, -5120(%rax)
	movq	%r8, -5056(%rax)
	movq	%r8, -4992(%rax)
	movq	%r8, -4928(%rax)
	movq	%r8, -4864(%rax)
	movq	%r8, -4800(%rax)
	movq	%r8, -4736(%rax)
	movw	%r11w, -4728(%rax)
	movl	$2, %r11d
	movw	%r13w, -4664(%rax)
	movl	$2, %r13d
	movw	%cx, -4600(%rax)
	movl	$2, %ecx
	movw	%si, -4536(%rax)
	movl	$2, %esi
	movw	%di, -4472(%rax)
	movl	$2, %edi
	movw	%r10w, -4408(%rax)
	movl	$2, %r10d
	movw	%r11w, -4344(%rax)
	movl	$2, %r11d
	movw	%r13w, -4280(%rax)
	movl	$2, %r13d
	movw	%cx, -4216(%rax)
	movl	$2, %ecx
	movw	%si, -4152(%rax)
	movl	$2, %esi
	movw	%di, -4088(%rax)
	movl	$2, %edi
	movw	%r10w, -4024(%rax)
	movl	$2, %r10d
	movw	%r11w, -3960(%rax)
	movl	$2, %r11d
	movw	%r13w, -3896(%rax)
	movl	$2, %r13d
	movw	%cx, -3832(%rax)
	movl	$2, %ecx
	movw	%si, -3768(%rax)
	movl	$2, %esi
	movq	%r8, -4672(%rax)
	movq	%r8, -4608(%rax)
	movq	%r8, -4544(%rax)
	movq	%r8, -4480(%rax)
	movq	%r8, -4416(%rax)
	movq	%r8, -4352(%rax)
	movq	%r8, -4288(%rax)
	movq	%r8, -4224(%rax)
	movq	%r8, -4160(%rax)
	movq	%r8, -4096(%rax)
	movq	%r8, -4032(%rax)
	movq	%r8, -3968(%rax)
	movq	%r8, -3904(%rax)
	movq	%r8, -3840(%rax)
	movq	%r8, -3776(%rax)
	movq	%r8, -3712(%rax)
	movw	%di, -3704(%rax)
	movl	$2, %edi
	movw	%r10w, -3640(%rax)
	movl	$2, %r10d
	movw	%r11w, -3576(%rax)
	movl	$2, %r11d
	movw	%r13w, -3512(%rax)
	movl	$2, %r13d
	movw	%cx, -3448(%rax)
	movl	$2, %ecx
	movw	%si, -3384(%rax)
	movl	$2, %esi
	movw	%di, -3320(%rax)
	movl	$2, %edi
	movw	%r10w, -3256(%rax)
	movl	$2, %r10d
	movw	%r11w, -3192(%rax)
	movl	$2, %r11d
	movw	%r13w, -3128(%rax)
	movl	$2, %r13d
	movw	%cx, -3064(%rax)
	movl	$2, %ecx
	movw	%si, -3000(%rax)
	movl	$2, %esi
	movw	%di, -2936(%rax)
	movl	$2, %edi
	movw	%r10w, -2872(%rax)
	movl	$2, %r10d
	movw	%r11w, -2808(%rax)
	movl	$2, %r11d
	movw	%r13w, -2744(%rax)
	movl	$2, %r13d
	movw	%cx, -2680(%rax)
	movl	$2, %ecx
	movq	%r8, -3648(%rax)
	movq	%r8, -3584(%rax)
	movq	%r8, -3520(%rax)
	movq	%r8, -3456(%rax)
	movq	%r8, -3392(%rax)
	movq	%r8, -3328(%rax)
	movq	%r8, -3264(%rax)
	movq	%r8, -3200(%rax)
	movq	%r8, -3136(%rax)
	movq	%r8, -3072(%rax)
	movq	%r8, -3008(%rax)
	movq	%r8, -2944(%rax)
	movq	%r8, -2880(%rax)
	movq	%r8, -2816(%rax)
	movq	%r8, -2752(%rax)
	movq	%r8, -2688(%rax)
	movq	%r8, -2624(%rax)
	movw	%si, -2616(%rax)
	movl	$2, %esi
	movw	%di, -2552(%rax)
	movl	$2, %edi
	movw	%r10w, -2488(%rax)
	movl	$2, %r10d
	movw	%r11w, -2424(%rax)
	movl	$2, %r11d
	movw	%r13w, -2360(%rax)
	movl	$2, %r13d
	movw	%cx, -2296(%rax)
	movl	$2, %ecx
	movw	%si, -2232(%rax)
	movl	$2, %esi
	movw	%di, -2168(%rax)
	movl	$2, %edi
	movw	%r10w, -2104(%rax)
	movl	$2, %r10d
	movw	%r11w, -2040(%rax)
	movl	$2, %r11d
	movw	%r13w, -1976(%rax)
	movl	$2, %r13d
	movw	%cx, -1912(%rax)
	movl	$2, %ecx
	movw	%si, -1848(%rax)
	movl	$2, %esi
	movw	%di, -1784(%rax)
	movl	$2, %edi
	movw	%r10w, -1720(%rax)
	movl	$2, %r10d
	movw	%r11w, -1656(%rax)
	movl	$2, %r11d
	movq	%r8, -2560(%rax)
	movq	%r8, -2496(%rax)
	movq	%r8, -2432(%rax)
	movq	%r8, -2368(%rax)
	movq	%r8, -2304(%rax)
	movq	%r8, -2240(%rax)
	movq	%r8, -2176(%rax)
	movq	%r8, -2112(%rax)
	movq	%r8, -2048(%rax)
	movq	%r8, -1984(%rax)
	movq	%r8, -1920(%rax)
	movq	%r8, -1856(%rax)
	movq	%r8, -1792(%rax)
	movq	%r8, -1728(%rax)
	movq	%r8, -1664(%rax)
	movq	%r8, -1600(%rax)
	movw	%r13w, -1592(%rax)
	movl	$2, %r13d
	movw	%cx, -1528(%rax)
	movl	$2, %ecx
	movw	%si, -1464(%rax)
	movl	$2, %esi
	movw	%di, -1400(%rax)
	movl	$2, %edi
	movw	%r10w, -1336(%rax)
	movl	$2, %r10d
	movw	%r11w, -1272(%rax)
	movl	$2, %r11d
	movw	%r13w, -1208(%rax)
	movl	$2, %r13d
	movw	%cx, -1144(%rax)
	movl	$2, %ecx
	movw	%si, -1080(%rax)
	movl	$2, %esi
	movw	%di, -1016(%rax)
	movl	$2, %edi
	movw	%r10w, -952(%rax)
	movl	$2, %r10d
	movw	%r11w, -888(%rax)
	movl	$2, %r11d
	movw	%r13w, -824(%rax)
	movl	$2, %r13d
	movw	%cx, -760(%rax)
	movl	$2, %ecx
	movw	%si, -696(%rax)
	movl	$2, %esi
	movw	%di, -632(%rax)
	movl	$2, %edi
	movw	%r10w, -568(%rax)
	movl	$2, %r10d
	movq	%r8, -1536(%rax)
	movq	%r8, -1472(%rax)
	movq	%r8, -1408(%rax)
	movq	%r8, -1344(%rax)
	movq	%r8, -1280(%rax)
	movq	%r8, -1216(%rax)
	movq	%r8, -1152(%rax)
	movq	%r8, -1088(%rax)
	movq	%r8, -1024(%rax)
	movq	%r8, -960(%rax)
	movq	%r8, -896(%rax)
	movq	%r8, -832(%rax)
	movq	%r8, -768(%rax)
	movq	%r8, -704(%rax)
	movq	%r8, -640(%rax)
	movq	%r8, -576(%rax)
	movq	%r8, -512(%rax)
	movw	%r11w, -504(%rax)
	movl	$2, %r11d
	movw	%r13w, -440(%rax)
	movl	$2, %r13d
	movq	%r8, -448(%rax)
	movq	%r8, -384(%rax)
	movw	%cx, -376(%rax)
	movq	%r8, -320(%rax)
	movw	%si, -312(%rax)
	movq	%r8, -256(%rax)
	movw	%di, -248(%rax)
	movq	%r8, -192(%rax)
	movw	%r10w, -184(%rax)
	movq	%r8, -128(%rax)
	movw	%r11w, -120(%rax)
	movq	%r8, -64(%rax)
	movw	%r13w, -56(%rax)
	cmpq	%rax, %rdx
	jne	.L1133
	movq	%r8, 21624(%r15)
	movl	$2, %eax
	pxor	%xmm0, %xmm0
	movq	$0, 21704(%r15)
	movw	%ax, 21632(%r15)
	movq	%rdx, %rax
	leaq	21624(%r15), %rdx
.L1134:
	movups	%xmm0, (%rax)
	addq	$1440, %rax
	movups	%xmm0, -1424(%rax)
	movups	%xmm0, -1408(%rax)
	movups	%xmm0, -1392(%rax)
	movups	%xmm0, -1376(%rax)
	movups	%xmm0, -1360(%rax)
	movups	%xmm0, -1344(%rax)
	movups	%xmm0, -1328(%rax)
	movups	%xmm0, -1312(%rax)
	movups	%xmm0, -1296(%rax)
	movups	%xmm0, -1280(%rax)
	movups	%xmm0, -1264(%rax)
	movups	%xmm0, -1248(%rax)
	movups	%xmm0, -1232(%rax)
	movups	%xmm0, -1216(%rax)
	movups	%xmm0, -1200(%rax)
	movups	%xmm0, -1184(%rax)
	movups	%xmm0, -1168(%rax)
	movups	%xmm0, -1152(%rax)
	movups	%xmm0, -1136(%rax)
	movups	%xmm0, -1120(%rax)
	movups	%xmm0, -1104(%rax)
	movups	%xmm0, -1088(%rax)
	movups	%xmm0, -1072(%rax)
	movups	%xmm0, -1056(%rax)
	movups	%xmm0, -1040(%rax)
	movups	%xmm0, -1024(%rax)
	movups	%xmm0, -1008(%rax)
	movups	%xmm0, -992(%rax)
	movups	%xmm0, -976(%rax)
	movups	%xmm0, -960(%rax)
	movups	%xmm0, -944(%rax)
	movups	%xmm0, -928(%rax)
	movups	%xmm0, -912(%rax)
	movups	%xmm0, -896(%rax)
	movups	%xmm0, -880(%rax)
	movups	%xmm0, -864(%rax)
	movups	%xmm0, -848(%rax)
	movups	%xmm0, -832(%rax)
	movups	%xmm0, -816(%rax)
	movups	%xmm0, -800(%rax)
	movups	%xmm0, -784(%rax)
	movups	%xmm0, -768(%rax)
	movups	%xmm0, -752(%rax)
	movups	%xmm0, -736(%rax)
	movups	%xmm0, -720(%rax)
	movups	%xmm0, -704(%rax)
	movups	%xmm0, -688(%rax)
	movups	%xmm0, -672(%rax)
	movups	%xmm0, -656(%rax)
	movups	%xmm0, -640(%rax)
	movups	%xmm0, -624(%rax)
	movups	%xmm0, -608(%rax)
	movups	%xmm0, -592(%rax)
	movups	%xmm0, -576(%rax)
	movups	%xmm0, -560(%rax)
	movups	%xmm0, -544(%rax)
	movups	%xmm0, -528(%rax)
	movups	%xmm0, -512(%rax)
	movups	%xmm0, -496(%rax)
	movups	%xmm0, -480(%rax)
	movups	%xmm0, -464(%rax)
	movups	%xmm0, -448(%rax)
	movups	%xmm0, -432(%rax)
	movups	%xmm0, -416(%rax)
	movups	%xmm0, -400(%rax)
	movups	%xmm0, -384(%rax)
	movups	%xmm0, -368(%rax)
	movups	%xmm0, -352(%rax)
	movups	%xmm0, -336(%rax)
	movups	%xmm0, -320(%rax)
	movups	%xmm0, -304(%rax)
	movups	%xmm0, -288(%rax)
	movups	%xmm0, -272(%rax)
	movups	%xmm0, -256(%rax)
	movups	%xmm0, -240(%rax)
	movups	%xmm0, -224(%rax)
	movups	%xmm0, -208(%rax)
	movups	%xmm0, -192(%rax)
	movups	%xmm0, -176(%rax)
	movups	%xmm0, -160(%rax)
	movups	%xmm0, -144(%rax)
	movups	%xmm0, -128(%rax)
	movups	%xmm0, -112(%rax)
	movups	%xmm0, -96(%rax)
	movups	%xmm0, -80(%rax)
	movups	%xmm0, -64(%rax)
	movups	%xmm0, -48(%rax)
	movups	%xmm0, -32(%rax)
	movups	%xmm0, -16(%rax)
	cmpq	%rdx, %rax
	jne	.L1134
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_122RelDateTimeFmtDataSinkE(%rip), %rax
	movq	%r15, -328(%rbp)
	movq	%rbx, %rcx
	movq	%r12, %rdi
	movq	%rax, -352(%rbp)
	leaq	-352(%rbp), %r13
	leaq	.LC19(%rip), %rsi
	movq	$-1, 21688(%r15)
	movq	%r13, %rdx
	movl	$-1, 21696(%r15)
	movq	%r9, -376(%rbp)
	call	ures_getAllItemsWithFallback_67@PLT
	movl	(%rbx), %eax
	movq	-376(%rbp), %r9
	testl	%eax, %eax
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_122RelDateTimeFmtDataSinkE(%rip), %rax
	jle	.L1135
	movq	%r13, %rdi
	movq	%rax, -352(%rbp)
	call	_ZN6icu_6712ResourceSinkD2Ev@PLT
.L1136:
	movq	(%r15), %rax
	leaq	_ZN6icu_6725RelativeDateTimeCacheDataD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1175
	movq	%r15, %rdi
	call	_ZN6icu_6725RelativeDateTimeCacheDataD1Ev
	movq	%r15, %rdi
	xorl	%r15d, %r15d
	call	_ZN6icu_677UMemorydlEPv@PLT
.L1131:
	testq	%r12, %r12
	je	.L1130
	movq	%r12, %rdi
	call	ures_close_67@PLT
.L1130:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1176
	addq	$360, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1135:
	.cfi_restore_state
	movq	%r14, %rsi
	movq	%r9, %rdi
	movq	%rbx, %rdx
	call	_ZN6icu_67L16loadWeekdayNamesEPA15_A6_NS_13UnicodeStringEPKcR10UErrorCode
	movl	(%rbx), %r14d
	leaq	16+_ZTVN6icu_6712_GLOBAL__N_122RelDateTimeFmtDataSinkE(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, -352(%rbp)
	call	_ZN6icu_6712ResourceSinkD2Ev@PLT
	testl	%r14d, %r14d
	jg	.L1136
	leaq	-368(%rbp), %rax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %r8
	movq	%rbx, %rcx
	movq	%r12, %rdi
	movl	$2, %r9d
	movl	$2, %r10d
	movq	%rax, %rdx
	movq	%r8, -320(%rbp)
	leaq	.LC20(%rip), %rsi
	movw	%r9w, -312(%rbp)
	movq	%r8, -256(%rbp)
	movw	%r10w, -248(%rbp)
	movl	$0, -368(%rbp)
	movq	%rax, -376(%rbp)
	call	ures_getStringByKeyWithFallback_67@PLT
	movl	(%rbx), %r11d
	testl	%r11d, %r11d
	jle	.L1177
	leaq	-256(%rbp), %rdi
	leaq	-320(%rbp), %r14
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L1140:
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L1136
	.p2align 4,,10
	.p2align 3
.L1177:
	movq	%rax, -352(%rbp)
	movl	-368(%rbp), %ecx
	movq	%r13, %rdx
	leaq	-256(%rbp), %rax
	movl	$1, %esi
	movq	%rax, %rdi
	movq	%rax, -384(%rbp)
	call	_ZN6icu_6713UnicodeString5setToEaNS_14ConstChar16PtrEi@PLT
	xorl	%esi, %esi
	movq	-376(%rbp), %rdi
	leaq	-179(%rbp), %rax
	leaq	-192(%rbp), %r14
	movw	%si, -180(%rbp)
	leaq	.LC21(%rip), %rsi
	movq	%rax, -192(%rbp)
	movl	$0, -136(%rbp)
	movl	$40, -184(%rbp)
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movl	-360(%rbp), %edx
	movq	%rbx, %rcx
	movq	%r14, %rdi
	movq	-368(%rbp), %rsi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movq	-384(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%rax, %rdi
	call	_ZN6icu_6710CharString20appendInvariantCharsERKNS_13UnicodeStringER10UErrorCode@PLT
	leaq	.LC22(%rip), %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movl	-344(%rbp), %edx
	movq	%r14, %rdi
	movq	%rbx, %rcx
	movq	-352(%rbp), %rsi
	leaq	-320(%rbp), %r14
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movq	%r12, %rdi
	movq	%rbx, %rcx
	xorl	%edx, %edx
	movq	-192(%rbp), %rsi
	call	ures_getByKeyWithFallback_67@PLT
	movl	(%rbx), %edi
	movb	$0, -385(%rbp)
	movq	%rax, %r10
	testl	%edi, %edi
	jle	.L1178
.L1138:
	testq	%r10, %r10
	je	.L1143
	movq	%r10, %rdi
	call	ures_close_67@PLT
.L1143:
	cmpb	$0, -180(%rbp)
	jne	.L1179
.L1144:
	movq	-384(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpb	$0, -385(%rbp)
	je	.L1140
	movl	$72, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1146
	movl	$2, %edx
	movl	$2, %ecx
	movq	%r14, %rsi
	movq	%rax, %rdi
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %r8
	movw	%dx, 16(%rax)
	movl	$2, %edx
	movq	%r8, 8(%rax)
	movq	%rbx, %r8
	call	_ZN6icu_6715SimpleFormatter27applyPatternMinMaxArgumentsERKNS_13UnicodeStringEiiR10UErrorCode@PLT
.L1146:
	movq	21704(%r15), %rdi
	testq	%rdi, %rdi
	je	.L1147
	movq	%rdi, -376(%rbp)
	call	_ZN6icu_6715SimpleFormatterD1Ev@PLT
	movq	-376(%rbp), %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L1147:
	movl	(%rbx), %eax
	movq	%r13, 21704(%r15)
	testl	%eax, %eax
	jg	.L1140
	movq	%r15, %rdi
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L1131
	.p2align 4,,10
	.p2align 3
.L1175:
	movq	%r15, %rdi
	xorl	%r15d, %r15d
	call	*%rax
	jmp	.L1131
	.p2align 4,,10
	.p2align 3
.L1178:
	movq	%rax, %rdi
	movq	%rax, -400(%rbp)
	call	ures_getSize_67@PLT
	movq	-400(%rbp), %r10
	cmpl	$8, %eax
	jle	.L1180
	movq	-376(%rbp), %rdx
	movq	%rbx, %rcx
	movq	%r10, %rdi
	movl	$8, %esi
	movq	%r10, -376(%rbp)
	movl	$0, -368(%rbp)
	call	ures_getStringByIndex_67@PLT
	movl	(%rbx), %ecx
	movq	-376(%rbp), %r10
	testl	%ecx, %ecx
	jg	.L1138
	movl	-368(%rbp), %ecx
	movq	%r13, %rdx
	movl	$1, %esi
	movq	%r14, %rdi
	movq	%rax, -352(%rbp)
	call	_ZN6icu_6713UnicodeString5setToEaNS_14ConstChar16PtrEi@PLT
	movb	$1, -385(%rbp)
	movq	-376(%rbp), %r10
	jmp	.L1138
	.p2align 4,,10
	.p2align 3
.L1180:
	leaq	-128(%rbp), %r9
	movl	$-1, %ecx
	movq	%r13, %rdx
	movl	$1, %esi
	leaq	.LC23(%rip), %rax
	movq	%r9, %rdi
	movq	%r9, -376(%rbp)
	movq	%rax, -352(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	-376(%rbp), %r9
	movq	%r14, %rdi
	movq	%r9, %rsi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	-376(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movb	$1, -385(%rbp)
	movq	-400(%rbp), %r10
	jmp	.L1138
	.p2align 4,,10
	.p2align 3
.L1179:
	movq	-192(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L1144
.L1176:
	call	__stack_chk_fail@PLT
.L1132:
	movl	$7, (%rbx)
	jmp	.L1131
	.cfi_endproc
.LFE4517:
	.size	_ZNK6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEE12createObjectEPKvR10UErrorCode, .-_ZNK6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEE12createObjectEPKvR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725RelativeDateTimeFormatterD2Ev
	.type	_ZN6icu_6725RelativeDateTimeFormatterD2Ev, @function
_ZN6icu_6725RelativeDateTimeFormatterD2Ev:
.LFB4556:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6725RelativeDateTimeFormatterE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1182
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
.L1182:
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1183
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
.L1183:
	movq	24(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1184
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
.L1184:
	movq	40(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1185
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
.L1185:
	leaq	48(%r12), %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE4556:
	.size	_ZN6icu_6725RelativeDateTimeFormatterD2Ev, .-_ZN6icu_6725RelativeDateTimeFormatterD2Ev
	.globl	_ZN6icu_6725RelativeDateTimeFormatterD1Ev
	.set	_ZN6icu_6725RelativeDateTimeFormatterD1Ev,_ZN6icu_6725RelativeDateTimeFormatterD2Ev
	.section	.text._ZN6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEED2Ev,"axG",@progbits,_ZN6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEED2Ev
	.type	_ZN6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEED2Ev, @function
_ZN6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEED2Ev:
.LFB5695:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	16(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -16(%rdi)
	call	_ZN6icu_676LocaleD1Ev@PLT
	leaq	16+_ZTVN6icu_678CacheKeyINS_25RelativeDateTimeCacheDataEEE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6712CacheKeyBaseD2Ev@PLT
	.cfi_endproc
.LFE5695:
	.size	_ZN6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEED2Ev, .-_ZN6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEED2Ev
	.weak	_ZN6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEED1Ev
	.set	_ZN6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEED1Ev,_ZN6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEED2Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725RelativeDateTimeFormatter4initEPNS_12NumberFormatEPNS_13BreakIteratorER10UErrorCode
	.type	_ZN6icu_6725RelativeDateTimeFormatter4initEPNS_12NumberFormatEPNS_13BreakIteratorER10UErrorCode, @function
_ZN6icu_6725RelativeDateTimeFormatter4initEPNS_12NumberFormatEPNS_13BreakIteratorER10UErrorCode:
.LFB4585:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movq	%rcx, %rdi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$312, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6712UnifiedCache11getInstanceER10UErrorCode@PLT
	movl	(%rbx), %r8d
	testl	%r8d, %r8d
	jle	.L1269
.L1202:
	testq	%r13, %r13
	je	.L1230
.L1231:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*8(%rax)
.L1230:
	testq	%r12, %r12
	je	.L1201
.L1232:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
.L1201:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1270
	addq	$312, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1269:
	.cfi_restore_state
	leaq	-288(%rbp), %r9
	leaq	48(%r14), %r15
	movq	%rax, -344(%rbp)
	leaq	16+_ZTVN6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEEE(%rip), %rax
	movq	%r9, %rdi
	movq	%r15, %rsi
	movq	%r9, -336(%rbp)
	movq	%rax, -304(%rbp)
	leaq	-304(%rbp), %rax
	movq	%rax, -328(%rbp)
	movl	$0, -296(%rbp)
	movb	$0, -292(%rbp)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movl	(%rbx), %edi
	movq	-336(%rbp), %r9
	movq	-344(%rbp), %rax
	testl	%edi, %edi
	jle	.L1271
.L1203:
	leaq	16+_ZTVN6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEEE(%rip), %rax
	movq	%r9, %rdi
	movq	%rax, -304(%rbp)
	call	_ZN6icu_676LocaleD1Ev@PLT
	movq	-328(%rbp), %rdi
	leaq	16+_ZTVN6icu_678CacheKeyINS_25RelativeDateTimeCacheDataEEE(%rip), %rax
	movq	%rax, -304(%rbp)
	call	_ZN6icu_6712CacheKeyBaseD2Ev@PLT
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L1202
	movq	%rbx, %rdx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN6icu_6711PluralRules20createSharedInstanceERKNS_6LocaleE11UPluralTypeR10UErrorCode@PLT
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L1202
	movq	24(%r14), %rdi
	cmpq	%rdi, %rax
	je	.L1212
	testq	%rdi, %rdi
	je	.L1213
	movq	%rax, -328(%rbp)
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	movq	-328(%rbp), %rax
.L1213:
	movq	%rax, 24(%r14)
	testq	%rax, %rax
	je	.L1212
	movq	%rax, %rdi
	movq	%rax, -328(%rbp)
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
	movq	-328(%rbp), %rax
.L1212:
	movq	%rax, %rdi
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	testq	%r12, %r12
	je	.L1272
	movl	$32, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1222
	movq	16(%r14), %rdi
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	leaq	16+_ZTVN6icu_6718SharedNumberFormatE(%rip), %rax
	movq	%rax, (%r15)
	movq	%r12, 24(%r15)
	cmpq	%rdi, %r15
	je	.L1221
	testq	%rdi, %rdi
	je	.L1223
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
.L1223:
	movq	%r15, 16(%r14)
	movq	%r15, %rdi
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
.L1221:
	testq	%r13, %r13
	je	.L1273
	movl	$32, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1227
	movq	%rax, %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6719SharedBreakIteratorC1EPNS_13BreakIteratorE@PLT
	movq	40(%r14), %rdi
	cmpq	%rdi, %r12
	je	.L1201
	testq	%rdi, %rdi
	je	.L1229
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
.L1229:
	movq	%r12, 40(%r14)
	movq	%r12, %rdi
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
	jmp	.L1201
	.p2align 4,,10
	.p2align 3
.L1271:
	movq	-328(%rbp), %rsi
	leaq	-316(%rbp), %r8
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	leaq	-312(%rbp), %rdx
	movl	$0, -316(%rbp)
	movq	$0, -312(%rbp)
	call	_ZNK6icu_6712UnifiedCache4_getERKNS_12CacheKeyBaseERPKNS_12SharedObjectEPKvR10UErrorCode@PLT
	movl	-316(%rbp), %eax
	movq	-312(%rbp), %r8
	movq	-336(%rbp), %r9
	testl	%eax, %eax
	jle	.L1274
.L1204:
	testq	%r8, %r8
	je	.L1207
.L1208:
	movq	%r8, %rdi
	movq	%r9, -336(%rbp)
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	movl	-316(%rbp), %eax
	movq	-336(%rbp), %r9
.L1207:
	movl	(%rbx), %esi
	testl	%esi, %esi
	je	.L1209
	testl	%eax, %eax
	jle	.L1203
.L1209:
	movl	%eax, (%rbx)
	jmp	.L1203
	.p2align 4,,10
	.p2align 3
.L1273:
	movq	40(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1201
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	movq	$0, 40(%r14)
	jmp	.L1201
	.p2align 4,,10
	.p2align 3
.L1272:
	movq	%rbx, %rdx
	movl	$1, %esi
	movq	%r15, %rdi
	call	_ZN6icu_6712NumberFormat20createSharedInstanceERKNS_6LocaleE18UNumberFormatStyleR10UErrorCode@PLT
	movq	%rax, %r12
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L1275
	testq	%r13, %r13
	je	.L1201
.L1268:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*8(%rax)
	jmp	.L1201
	.p2align 4,,10
	.p2align 3
.L1274:
	movq	8(%r14), %rdi
	cmpq	%rdi, %r8
	je	.L1204
	testq	%rdi, %rdi
	je	.L1205
	movq	%r9, -344(%rbp)
	movq	%r8, -336(%rbp)
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	movq	-344(%rbp), %r9
	movq	-336(%rbp), %r8
.L1205:
	movq	%r8, 8(%r14)
	testq	%r8, %r8
	je	.L1276
	movq	%r8, %rdi
	movq	%r9, -344(%rbp)
	movq	%r8, -336(%rbp)
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
	movq	-336(%rbp), %r8
	movq	-344(%rbp), %r9
	jmp	.L1208
	.p2align 4,,10
	.p2align 3
.L1275:
	movq	16(%r14), %rdi
	cmpq	%rdi, %r12
	je	.L1218
	testq	%rdi, %rdi
	je	.L1219
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
.L1219:
	movq	%r12, 16(%r14)
	testq	%r12, %r12
	je	.L1218
	movq	%r12, %rdi
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
.L1218:
	movq	%r12, %rdi
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	jmp	.L1221
	.p2align 4,,10
	.p2align 3
.L1276:
	movl	-316(%rbp), %eax
	jmp	.L1207
.L1270:
	call	__stack_chk_fail@PLT
.L1227:
	movl	$7, (%rbx)
	jmp	.L1268
.L1222:
	movl	$7, (%rbx)
	testq	%r13, %r13
	jne	.L1231
	jmp	.L1232
	.cfi_endproc
.LFE4585:
	.size	_ZN6icu_6725RelativeDateTimeFormatter4initEPNS_12NumberFormatEPNS_13BreakIteratorER10UErrorCode, .-_ZN6icu_6725RelativeDateTimeFormatter4initEPNS_12NumberFormatEPNS_13BreakIteratorER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725RelativeDateTimeFormatterC2ERKNS_6LocaleEPNS_12NumberFormatE35UDateRelativeDateTimeFormatterStyle15UDisplayContextR10UErrorCode
	.type	_ZN6icu_6725RelativeDateTimeFormatterC2ERKNS_6LocaleEPNS_12NumberFormatE35UDateRelativeDateTimeFormatterStyle15UDisplayContextR10UErrorCode, @function
_ZN6icu_6725RelativeDateTimeFormatterC2ERKNS_6LocaleEPNS_12NumberFormatE35UDateRelativeDateTimeFormatterStyle15UDisplayContextR10UErrorCode:
.LFB4549:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6725RelativeDateTimeFormatterE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r9, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	addq	$48, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	subq	$8, %rsp
	movq	%rax, -48(%rdi)
	movq	$0, -40(%rdi)
	movq	$0, -32(%rdi)
	movq	$0, -24(%rdi)
	movl	%ecx, -16(%rdi)
	movl	%r8d, -12(%rdi)
	movq	$0, -8(%rdi)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movl	0(%r13), %edx
	testl	%edx, %edx
	jg	.L1277
	movl	%ebx, %eax
	sarl	$8, %eax
	cmpl	$1, %eax
	je	.L1280
	movl	$1, 0(%r13)
.L1277:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1280:
	.cfi_restore_state
	movq	%r13, %rcx
	xorl	%edx, %edx
	cmpl	$258, %ebx
	je	.L1285
.L1284:
	addq	$8, %rsp
	movq	%r14, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6725RelativeDateTimeFormatter4initEPNS_12NumberFormatEPNS_13BreakIteratorER10UErrorCode
	.p2align 4,,10
	.p2align 3
.L1285:
	.cfi_restore_state
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6713BreakIterator22createSentenceInstanceERKNS_6LocaleER10UErrorCode@PLT
	movq	%r13, %rcx
	movq	%rax, %rdx
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L1277
	jmp	.L1284
	.cfi_endproc
.LFE4549:
	.size	_ZN6icu_6725RelativeDateTimeFormatterC2ERKNS_6LocaleEPNS_12NumberFormatE35UDateRelativeDateTimeFormatterStyle15UDisplayContextR10UErrorCode, .-_ZN6icu_6725RelativeDateTimeFormatterC2ERKNS_6LocaleEPNS_12NumberFormatE35UDateRelativeDateTimeFormatterStyle15UDisplayContextR10UErrorCode
	.globl	_ZN6icu_6725RelativeDateTimeFormatterC1ERKNS_6LocaleEPNS_12NumberFormatE35UDateRelativeDateTimeFormatterStyle15UDisplayContextR10UErrorCode
	.set	_ZN6icu_6725RelativeDateTimeFormatterC1ERKNS_6LocaleEPNS_12NumberFormatE35UDateRelativeDateTimeFormatterStyle15UDisplayContextR10UErrorCode,_ZN6icu_6725RelativeDateTimeFormatterC2ERKNS_6LocaleEPNS_12NumberFormatE35UDateRelativeDateTimeFormatterStyle15UDisplayContextR10UErrorCode
	.section	.text._ZN6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEED0Ev,"axG",@progbits,_ZN6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEED0Ev
	.type	_ZN6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEED0Ev, @function
_ZN6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEED0Ev:
.LFB5697:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	16(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -16(%rdi)
	call	_ZN6icu_676LocaleD1Ev@PLT
	leaq	16+_ZTVN6icu_678CacheKeyINS_25RelativeDateTimeCacheDataEEE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	call	_ZN6icu_6712CacheKeyBaseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE5697:
	.size	_ZN6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEED0Ev, .-_ZN6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEED0Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725RelativeDateTimeFormatterD0Ev
	.type	_ZN6icu_6725RelativeDateTimeFormatterD0Ev, @function
_ZN6icu_6725RelativeDateTimeFormatterD0Ev:
.LFB4558:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6725RelativeDateTimeFormatterE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1289
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
.L1289:
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1290
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
.L1290:
	movq	24(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1291
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
.L1291:
	movq	40(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1292
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
.L1292:
	leaq	48(%r12), %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE4558:
	.size	_ZN6icu_6725RelativeDateTimeFormatterD0Ev, .-_ZN6icu_6725RelativeDateTimeFormatterD0Ev
	.p2align 4
	.globl	ureldatefmt_close_67
	.type	ureldatefmt_close_67, @function
ureldatefmt_close_67:
.LFB4612:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L1306
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN6icu_6725RelativeDateTimeFormatterD0Ev(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1308
	leaq	16+_ZTVN6icu_6725RelativeDateTimeFormatterE(%rip), %rax
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1309
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
.L1309:
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1310
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
.L1310:
	movq	24(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1311
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
.L1311:
	movq	40(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1312
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
.L1312:
	leaq	48(%r12), %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L1306:
	ret
	.p2align 4,,10
	.p2align 3
.L1308:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	addq	$8, %rsp
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE4612:
	.size	ureldatefmt_close_67, .-ureldatefmt_close_67
	.p2align 4
	.globl	ureldatefmt_open_67
	.type	ureldatefmt_open_67, @function
ureldatefmt_open_67:
.LFB4611:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$264, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -296(%rbp)
	movl	(%r8), %r9d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r9d, %r9d
	jg	.L1328
	leaq	-288(%rbp), %r15
	movq	%rdi, %rsi
	movl	%edx, %r14d
	movl	%ecx, %ebx
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%r15, %rdi
	movq	%r8, %r13
	xorl	%r8d, %r8d
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	movl	$272, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1330
	leaq	16+_ZTVN6icu_6725RelativeDateTimeFormatterE(%rip), %rax
	movl	%r14d, 32(%r12)
	movq	%r15, %rsi
	leaq	48(%r12), %r14
	movq	%rax, (%r12)
	movq	%r14, %rdi
	movq	$0, 8(%r12)
	movq	$0, 16(%r12)
	movq	$0, 24(%r12)
	movl	%ebx, 36(%r12)
	movq	$0, 40(%r12)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movl	0(%r13), %esi
	testl	%esi, %esi
	jg	.L1333
	movl	%ebx, %eax
	sarl	$8, %eax
	cmpl	$1, %eax
	jne	.L1360
	cmpl	$258, %ebx
	je	.L1361
	movq	-296(%rbp), %rsi
	movq	%r13, %rcx
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN6icu_6725RelativeDateTimeFormatter4initEPNS_12NumberFormatEPNS_13BreakIteratorER10UErrorCode
	.p2align 4,,10
	.p2align 3
.L1333:
	movq	%r15, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	movl	0(%r13), %edx
	testl	%edx, %edx
	jle	.L1328
	movq	(%r12), %rax
	leaq	_ZN6icu_6725RelativeDateTimeFormatterD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1362
	movq	8(%r12), %rdi
	leaq	16+_ZTVN6icu_6725RelativeDateTimeFormatterE(%rip), %rax
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L1336
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
.L1336:
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1337
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
.L1337:
	movq	24(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1338
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
.L1338:
	movq	40(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1339
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
.L1339:
	movq	%r14, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	_ZN6icu_677UMemorydlEPv@PLT
.L1328:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1363
	addq	$264, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1360:
	.cfi_restore_state
	movl	$1, 0(%r13)
	jmp	.L1333
.L1330:
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L1335
	movl	$7, 0(%r13)
.L1335:
	movq	%r15, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	jmp	.L1328
	.p2align 4,,10
	.p2align 3
.L1362:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	*%rax
	jmp	.L1328
	.p2align 4,,10
	.p2align 3
.L1361:
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6713BreakIterator22createSentenceInstanceERKNS_6LocaleER10UErrorCode@PLT
	movl	0(%r13), %ecx
	testl	%ecx, %ecx
	jg	.L1333
	movq	-296(%rbp), %rsi
	movq	%r13, %rcx
	movq	%rax, %rdx
	movq	%r12, %rdi
	call	_ZN6icu_6725RelativeDateTimeFormatter4initEPNS_12NumberFormatEPNS_13BreakIteratorER10UErrorCode
	jmp	.L1333
.L1363:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4611:
	.size	ureldatefmt_open_67, .-ureldatefmt_open_67
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725RelativeDateTimeFormatterC2ER10UErrorCode
	.type	_ZN6icu_6725RelativeDateTimeFormatterC2ER10UErrorCode, @function
_ZN6icu_6725RelativeDateTimeFormatterC2ER10UErrorCode:
.LFB4540:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	48(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$296, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6725RelativeDateTimeFormatterE(%rip), %rax
	movq	$0, 8(%rdi)
	movq	%rax, (%rdi)
	movabsq	$1099511627776, %rax
	movq	%rax, 32(%rdi)
	movq	$0, 16(%rdi)
	movq	$0, 24(%rdi)
	movq	$0, 40(%rdi)
	movq	%r13, %rdi
	call	_ZN6icu_676LocaleC1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6712UnifiedCache11getInstanceER10UErrorCode@PLT
	movl	(%r12), %r8d
	testl	%r8d, %r8d
	jle	.L1408
.L1364:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1409
	addq	$296, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1408:
	.cfi_restore_state
	leaq	-288(%rbp), %r14
	movq	%r13, %rsi
	movq	%rax, -328(%rbp)
	leaq	16+_ZTVN6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEEE(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -304(%rbp)
	leaq	-304(%rbp), %r15
	movl	$0, -296(%rbp)
	movb	$0, -292(%rbp)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movl	(%r12), %edi
	movq	-328(%rbp), %rax
	testl	%edi, %edi
	jle	.L1410
.L1367:
	leaq	16+_ZTVN6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEEE(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -304(%rbp)
	call	_ZN6icu_676LocaleD1Ev@PLT
	leaq	16+_ZTVN6icu_678CacheKeyINS_25RelativeDateTimeCacheDataEEE(%rip), %rax
	movq	%r15, %rdi
	movq	%rax, -304(%rbp)
	call	_ZN6icu_6712CacheKeyBaseD2Ev@PLT
	movl	(%r12), %ecx
	testl	%ecx, %ecx
	jg	.L1364
	movq	%r12, %rdx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6711PluralRules20createSharedInstanceERKNS_6LocaleE11UPluralTypeR10UErrorCode@PLT
	movl	(%r12), %edx
	movq	%rax, %r14
	testl	%edx, %edx
	jg	.L1364
	movq	24(%rbx), %rdi
	cmpq	%rdi, %rax
	je	.L1377
	testq	%rdi, %rdi
	je	.L1378
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
.L1378:
	movq	%r14, 24(%rbx)
	testq	%r14, %r14
	je	.L1377
	movq	%r14, %rdi
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
.L1377:
	movq	%r14, %rdi
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$1, %esi
	call	_ZN6icu_6712NumberFormat20createSharedInstanceERKNS_6LocaleE18UNumberFormatStyleR10UErrorCode@PLT
	movq	%rax, %r13
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L1364
	movq	16(%rbx), %rdi
	cmpq	%rdi, %r13
	je	.L1381
	testq	%rdi, %rdi
	je	.L1382
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
.L1382:
	movq	%r13, 16(%rbx)
	testq	%r13, %r13
	je	.L1381
	movq	%r13, %rdi
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
.L1381:
	movq	%r13, %rdi
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	movq	40(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1364
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	movq	$0, 40(%rbx)
	jmp	.L1364
	.p2align 4,,10
	.p2align 3
.L1410:
	leaq	-316(%rbp), %r8
	movq	%rax, %rdi
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	-312(%rbp), %rdx
	movl	$0, -316(%rbp)
	movq	$0, -312(%rbp)
	call	_ZNK6icu_6712UnifiedCache4_getERKNS_12CacheKeyBaseERPKNS_12SharedObjectEPKvR10UErrorCode@PLT
	movl	-316(%rbp), %eax
	movq	-312(%rbp), %r8
	testl	%eax, %eax
	jle	.L1411
.L1368:
	testq	%r8, %r8
	je	.L1371
.L1372:
	movq	%r8, %rdi
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
.L1407:
	movl	-316(%rbp), %eax
.L1371:
	movl	(%r12), %esi
	testl	%esi, %esi
	je	.L1373
	testl	%eax, %eax
	jle	.L1367
.L1373:
	movl	%eax, (%r12)
	jmp	.L1367
	.p2align 4,,10
	.p2align 3
.L1411:
	movq	8(%rbx), %rdi
	cmpq	%rdi, %r8
	je	.L1368
	testq	%rdi, %rdi
	je	.L1369
	movq	%r8, -328(%rbp)
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	movq	-328(%rbp), %r8
.L1369:
	movq	%r8, 8(%rbx)
	testq	%r8, %r8
	je	.L1407
	movq	%r8, %rdi
	movq	%r8, -328(%rbp)
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
	movq	-328(%rbp), %r8
	jmp	.L1372
.L1409:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4540:
	.size	_ZN6icu_6725RelativeDateTimeFormatterC2ER10UErrorCode, .-_ZN6icu_6725RelativeDateTimeFormatterC2ER10UErrorCode
	.globl	_ZN6icu_6725RelativeDateTimeFormatterC1ER10UErrorCode
	.set	_ZN6icu_6725RelativeDateTimeFormatterC1ER10UErrorCode,_ZN6icu_6725RelativeDateTimeFormatterC2ER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725RelativeDateTimeFormatterC2ERKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_6725RelativeDateTimeFormatterC2ERKNS_6LocaleER10UErrorCode, @function
_ZN6icu_6725RelativeDateTimeFormatterC2ERKNS_6LocaleER10UErrorCode:
.LFB4543:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	48(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$296, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6725RelativeDateTimeFormatterE(%rip), %rax
	movq	$0, 8(%rdi)
	movq	%rax, (%rdi)
	movabsq	$1099511627776, %rax
	movq	%rax, 32(%rdi)
	movq	$0, 16(%rdi)
	movq	$0, 24(%rdi)
	movq	$0, 40(%rdi)
	movq	%r13, %rdi
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6712UnifiedCache11getInstanceER10UErrorCode@PLT
	movl	(%r12), %r8d
	testl	%r8d, %r8d
	jle	.L1456
.L1412:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1457
	addq	$296, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1456:
	.cfi_restore_state
	leaq	-288(%rbp), %r14
	movq	%r13, %rsi
	movq	%rax, -328(%rbp)
	leaq	16+_ZTVN6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEEE(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -304(%rbp)
	leaq	-304(%rbp), %r15
	movl	$0, -296(%rbp)
	movb	$0, -292(%rbp)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movl	(%r12), %edi
	movq	-328(%rbp), %rax
	testl	%edi, %edi
	jle	.L1458
.L1415:
	leaq	16+_ZTVN6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEEE(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -304(%rbp)
	call	_ZN6icu_676LocaleD1Ev@PLT
	leaq	16+_ZTVN6icu_678CacheKeyINS_25RelativeDateTimeCacheDataEEE(%rip), %rax
	movq	%r15, %rdi
	movq	%rax, -304(%rbp)
	call	_ZN6icu_6712CacheKeyBaseD2Ev@PLT
	movl	(%r12), %ecx
	testl	%ecx, %ecx
	jg	.L1412
	movq	%r12, %rdx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6711PluralRules20createSharedInstanceERKNS_6LocaleE11UPluralTypeR10UErrorCode@PLT
	movl	(%r12), %edx
	movq	%rax, %r14
	testl	%edx, %edx
	jg	.L1412
	movq	24(%rbx), %rdi
	cmpq	%rdi, %rax
	je	.L1425
	testq	%rdi, %rdi
	je	.L1426
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
.L1426:
	movq	%r14, 24(%rbx)
	testq	%r14, %r14
	je	.L1425
	movq	%r14, %rdi
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
.L1425:
	movq	%r14, %rdi
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$1, %esi
	call	_ZN6icu_6712NumberFormat20createSharedInstanceERKNS_6LocaleE18UNumberFormatStyleR10UErrorCode@PLT
	movq	%rax, %r13
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L1412
	movq	16(%rbx), %rdi
	cmpq	%rdi, %r13
	je	.L1429
	testq	%rdi, %rdi
	je	.L1430
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
.L1430:
	movq	%r13, 16(%rbx)
	testq	%r13, %r13
	je	.L1429
	movq	%r13, %rdi
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
.L1429:
	movq	%r13, %rdi
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	movq	40(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1412
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	movq	$0, 40(%rbx)
	jmp	.L1412
	.p2align 4,,10
	.p2align 3
.L1458:
	leaq	-316(%rbp), %r8
	movq	%rax, %rdi
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	-312(%rbp), %rdx
	movl	$0, -316(%rbp)
	movq	$0, -312(%rbp)
	call	_ZNK6icu_6712UnifiedCache4_getERKNS_12CacheKeyBaseERPKNS_12SharedObjectEPKvR10UErrorCode@PLT
	movl	-316(%rbp), %eax
	movq	-312(%rbp), %r8
	testl	%eax, %eax
	jle	.L1459
.L1416:
	testq	%r8, %r8
	je	.L1419
.L1420:
	movq	%r8, %rdi
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
.L1455:
	movl	-316(%rbp), %eax
.L1419:
	movl	(%r12), %esi
	testl	%esi, %esi
	je	.L1421
	testl	%eax, %eax
	jle	.L1415
.L1421:
	movl	%eax, (%r12)
	jmp	.L1415
	.p2align 4,,10
	.p2align 3
.L1459:
	movq	8(%rbx), %rdi
	cmpq	%rdi, %r8
	je	.L1416
	testq	%rdi, %rdi
	je	.L1417
	movq	%r8, -328(%rbp)
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	movq	-328(%rbp), %r8
.L1417:
	movq	%r8, 8(%rbx)
	testq	%r8, %r8
	je	.L1455
	movq	%r8, %rdi
	movq	%r8, -328(%rbp)
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
	movq	-328(%rbp), %r8
	jmp	.L1420
.L1457:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4543:
	.size	_ZN6icu_6725RelativeDateTimeFormatterC2ERKNS_6LocaleER10UErrorCode, .-_ZN6icu_6725RelativeDateTimeFormatterC2ERKNS_6LocaleER10UErrorCode
	.globl	_ZN6icu_6725RelativeDateTimeFormatterC1ERKNS_6LocaleER10UErrorCode
	.set	_ZN6icu_6725RelativeDateTimeFormatterC1ERKNS_6LocaleER10UErrorCode,_ZN6icu_6725RelativeDateTimeFormatterC2ERKNS_6LocaleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6725RelativeDateTimeFormatterC2ERKNS_6LocaleEPNS_12NumberFormatER10UErrorCode
	.type	_ZN6icu_6725RelativeDateTimeFormatterC2ERKNS_6LocaleEPNS_12NumberFormatER10UErrorCode, @function
_ZN6icu_6725RelativeDateTimeFormatterC2ERKNS_6LocaleEPNS_12NumberFormatER10UErrorCode:
.LFB4546:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	48(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$296, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6725RelativeDateTimeFormatterE(%rip), %rax
	movq	$0, 8(%rdi)
	movq	%rax, (%rdi)
	movabsq	$1099511627776, %rax
	movq	%rax, 32(%rdi)
	movq	$0, 16(%rdi)
	movq	$0, 24(%rdi)
	movq	$0, 40(%rdi)
	movq	%r13, %rdi
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6712UnifiedCache11getInstanceER10UErrorCode@PLT
	movl	(%rbx), %r8d
	testl	%r8d, %r8d
	jle	.L1516
.L1461:
	testq	%r12, %r12
	je	.L1460
.L1485:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
.L1460:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1517
	addq	$296, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1516:
	.cfi_restore_state
	movq	%rax, -336(%rbp)
	movq	%r13, %rsi
	leaq	16+_ZTVN6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEEE(%rip), %rax
	leaq	-288(%rbp), %r14
	movq	%rax, -304(%rbp)
	movq	%r14, %rdi
	leaq	-304(%rbp), %rax
	movq	%rax, -328(%rbp)
	movl	$0, -296(%rbp)
	movb	$0, -292(%rbp)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movl	(%rbx), %edi
	movq	-336(%rbp), %rax
	testl	%edi, %edi
	jle	.L1518
.L1462:
	leaq	16+_ZTVN6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEEE(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -304(%rbp)
	call	_ZN6icu_676LocaleD1Ev@PLT
	movq	-328(%rbp), %rdi
	leaq	16+_ZTVN6icu_678CacheKeyINS_25RelativeDateTimeCacheDataEEE(%rip), %rax
	movq	%rax, -304(%rbp)
	call	_ZN6icu_6712CacheKeyBaseD2Ev@PLT
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L1461
	movq	%rbx, %rdx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6711PluralRules20createSharedInstanceERKNS_6LocaleE11UPluralTypeR10UErrorCode@PLT
	movl	(%rbx), %edx
	movq	%rax, %r14
	testl	%edx, %edx
	jg	.L1461
	movq	24(%r15), %rdi
	cmpq	%rdi, %rax
	je	.L1471
	testq	%rdi, %rdi
	je	.L1472
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
.L1472:
	movq	%r14, 24(%r15)
	testq	%r14, %r14
	je	.L1471
	movq	%r14, %rdi
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
.L1471:
	movq	%r14, %rdi
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	testq	%r12, %r12
	je	.L1519
	movl	$32, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1482
	movq	16(%r15), %rdi
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	leaq	16+_ZTVN6icu_6718SharedNumberFormatE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	%r12, 24(%r13)
	cmpq	%rdi, %r13
	je	.L1481
	testq	%rdi, %rdi
	je	.L1483
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
.L1483:
	movq	%r13, 16(%r15)
	movq	%r13, %rdi
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
.L1481:
	movq	40(%r15), %rdi
	testq	%rdi, %rdi
	je	.L1460
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	movq	$0, 40(%r15)
	jmp	.L1460
	.p2align 4,,10
	.p2align 3
.L1518:
	movq	-328(%rbp), %rsi
	leaq	-316(%rbp), %r8
	movq	%rax, %rdi
	xorl	%ecx, %ecx
	leaq	-312(%rbp), %rdx
	movl	$0, -316(%rbp)
	movq	$0, -312(%rbp)
	call	_ZNK6icu_6712UnifiedCache4_getERKNS_12CacheKeyBaseERPKNS_12SharedObjectEPKvR10UErrorCode@PLT
	movl	-316(%rbp), %eax
	movq	-312(%rbp), %r8
	testl	%eax, %eax
	jle	.L1520
.L1463:
	testq	%r8, %r8
	je	.L1466
.L1467:
	movq	%r8, %rdi
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
.L1515:
	movl	-316(%rbp), %eax
.L1466:
	movl	(%rbx), %esi
	testl	%esi, %esi
	je	.L1468
	testl	%eax, %eax
	jle	.L1462
.L1468:
	movl	%eax, (%rbx)
	jmp	.L1462
	.p2align 4,,10
	.p2align 3
.L1519:
	movq	%rbx, %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6712NumberFormat20createSharedInstanceERKNS_6LocaleE18UNumberFormatStyleR10UErrorCode@PLT
	movq	%rax, %r12
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L1460
	movq	16(%r15), %rdi
	cmpq	%rdi, %r12
	je	.L1478
	testq	%rdi, %rdi
	je	.L1479
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
.L1479:
	movq	%r12, 16(%r15)
	testq	%r12, %r12
	je	.L1478
	movq	%r12, %rdi
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
.L1478:
	movq	%r12, %rdi
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	jmp	.L1481
	.p2align 4,,10
	.p2align 3
.L1520:
	movq	8(%r15), %rdi
	cmpq	%rdi, %r8
	je	.L1463
	testq	%rdi, %rdi
	je	.L1464
	movq	%r8, -336(%rbp)
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	movq	-336(%rbp), %r8
.L1464:
	movq	%r8, 8(%r15)
	testq	%r8, %r8
	je	.L1515
	movq	%r8, %rdi
	movq	%r8, -336(%rbp)
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
	movq	-336(%rbp), %r8
	jmp	.L1467
.L1517:
	call	__stack_chk_fail@PLT
.L1482:
	movl	$7, (%rbx)
	jmp	.L1485
	.cfi_endproc
.LFE4546:
	.size	_ZN6icu_6725RelativeDateTimeFormatterC2ERKNS_6LocaleEPNS_12NumberFormatER10UErrorCode, .-_ZN6icu_6725RelativeDateTimeFormatterC2ERKNS_6LocaleEPNS_12NumberFormatER10UErrorCode
	.globl	_ZN6icu_6725RelativeDateTimeFormatterC1ERKNS_6LocaleEPNS_12NumberFormatER10UErrorCode
	.set	_ZN6icu_6725RelativeDateTimeFormatterC1ERKNS_6LocaleEPNS_12NumberFormatER10UErrorCode,_ZN6icu_6725RelativeDateTimeFormatterC2ERKNS_6LocaleEPNS_12NumberFormatER10UErrorCode
	.section	.rodata
	.align 16
	.type	CSWTCH.304, @object
	.size	CSWTCH.304, 28
CSWTCH.304:
	.long	7
	.long	6
	.long	5
	.long	4
	.long	3
	.long	2
	.long	0
	.align 32
	.type	CSWTCH.177, @object
	.size	CSWTCH.177, 56
CSWTCH.177:
	.long	14
	.long	13
	.long	7
	.long	8
	.long	9
	.long	12
	.long	10
	.long	0
	.long	1
	.long	2
	.long	3
	.long	4
	.long	5
	.long	6
	.align 32
	.type	CSWTCH.175, @object
	.size	CSWTCH.175, 60
CSWTCH.175:
	.long	7
	.long	6
	.long	5
	.long	4
	.long	3
	.long	2
	.long	1
	.long	0
	.long	8
	.long	9
	.long	10
	.long	11
	.long	12
	.long	13
	.long	14
	.weak	_ZTSN6icu_677UMemoryE
	.section	.rodata._ZTSN6icu_677UMemoryE,"aG",@progbits,_ZTSN6icu_677UMemoryE,comdat
	.align 16
	.type	_ZTSN6icu_677UMemoryE, @object
	.size	_ZTSN6icu_677UMemoryE, 18
_ZTSN6icu_677UMemoryE:
	.string	"N6icu_677UMemoryE"
	.weak	_ZTIN6icu_677UMemoryE
	.section	.data.rel.ro._ZTIN6icu_677UMemoryE,"awG",@progbits,_ZTIN6icu_677UMemoryE,comdat
	.align 8
	.type	_ZTIN6icu_677UMemoryE, @object
	.size	_ZTIN6icu_677UMemoryE, 16
_ZTIN6icu_677UMemoryE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN6icu_677UMemoryE
	.weak	_ZTSN6icu_6725FormattedRelativeDateTimeE
	.section	.rodata._ZTSN6icu_6725FormattedRelativeDateTimeE,"aG",@progbits,_ZTSN6icu_6725FormattedRelativeDateTimeE,comdat
	.align 32
	.type	_ZTSN6icu_6725FormattedRelativeDateTimeE, @object
	.size	_ZTSN6icu_6725FormattedRelativeDateTimeE, 37
_ZTSN6icu_6725FormattedRelativeDateTimeE:
	.string	"N6icu_6725FormattedRelativeDateTimeE"
	.weak	_ZTIN6icu_6725FormattedRelativeDateTimeE
	.section	.data.rel.ro._ZTIN6icu_6725FormattedRelativeDateTimeE,"awG",@progbits,_ZTIN6icu_6725FormattedRelativeDateTimeE,comdat
	.align 8
	.type	_ZTIN6icu_6725FormattedRelativeDateTimeE, @object
	.size	_ZTIN6icu_6725FormattedRelativeDateTimeE, 56
_ZTIN6icu_6725FormattedRelativeDateTimeE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN6icu_6725FormattedRelativeDateTimeE
	.long	0
	.long	2
	.quad	_ZTIN6icu_677UMemoryE
	.quad	2
	.quad	_ZTIN6icu_6714FormattedValueE
	.quad	2
	.weak	_ZTSN6icu_6725RelativeDateTimeFormatterE
	.section	.rodata._ZTSN6icu_6725RelativeDateTimeFormatterE,"aG",@progbits,_ZTSN6icu_6725RelativeDateTimeFormatterE,comdat
	.align 32
	.type	_ZTSN6icu_6725RelativeDateTimeFormatterE, @object
	.size	_ZTSN6icu_6725RelativeDateTimeFormatterE, 37
_ZTSN6icu_6725RelativeDateTimeFormatterE:
	.string	"N6icu_6725RelativeDateTimeFormatterE"
	.weak	_ZTIN6icu_6725RelativeDateTimeFormatterE
	.section	.data.rel.ro._ZTIN6icu_6725RelativeDateTimeFormatterE,"awG",@progbits,_ZTIN6icu_6725RelativeDateTimeFormatterE,comdat
	.align 8
	.type	_ZTIN6icu_6725RelativeDateTimeFormatterE, @object
	.size	_ZTIN6icu_6725RelativeDateTimeFormatterE, 24
_ZTIN6icu_6725RelativeDateTimeFormatterE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6725RelativeDateTimeFormatterE
	.quad	_ZTIN6icu_677UObjectE
	.weak	_ZTSN6icu_6725RelativeDateTimeCacheDataE
	.section	.rodata._ZTSN6icu_6725RelativeDateTimeCacheDataE,"aG",@progbits,_ZTSN6icu_6725RelativeDateTimeCacheDataE,comdat
	.align 32
	.type	_ZTSN6icu_6725RelativeDateTimeCacheDataE, @object
	.size	_ZTSN6icu_6725RelativeDateTimeCacheDataE, 37
_ZTSN6icu_6725RelativeDateTimeCacheDataE:
	.string	"N6icu_6725RelativeDateTimeCacheDataE"
	.weak	_ZTIN6icu_6725RelativeDateTimeCacheDataE
	.section	.data.rel.ro._ZTIN6icu_6725RelativeDateTimeCacheDataE,"awG",@progbits,_ZTIN6icu_6725RelativeDateTimeCacheDataE,comdat
	.align 8
	.type	_ZTIN6icu_6725RelativeDateTimeCacheDataE, @object
	.size	_ZTIN6icu_6725RelativeDateTimeCacheDataE, 24
_ZTIN6icu_6725RelativeDateTimeCacheDataE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6725RelativeDateTimeCacheDataE
	.quad	_ZTIN6icu_6712SharedObjectE
	.section	.data.rel.ro,"aw"
	.align 8
	.type	_ZTIN6icu_6712_GLOBAL__N_122RelDateTimeFmtDataSinkE, @object
	.size	_ZTIN6icu_6712_GLOBAL__N_122RelDateTimeFmtDataSinkE, 24
_ZTIN6icu_6712_GLOBAL__N_122RelDateTimeFmtDataSinkE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6712_GLOBAL__N_122RelDateTimeFmtDataSinkE
	.quad	_ZTIN6icu_6712ResourceSinkE
	.section	.rodata
	.align 32
	.type	_ZTSN6icu_6712_GLOBAL__N_122RelDateTimeFmtDataSinkE, @object
	.size	_ZTSN6icu_6712_GLOBAL__N_122RelDateTimeFmtDataSinkE, 49
_ZTSN6icu_6712_GLOBAL__N_122RelDateTimeFmtDataSinkE:
	.string	"*N6icu_6712_GLOBAL__N_122RelDateTimeFmtDataSinkE"
	.weak	_ZTSN6icu_678CacheKeyINS_25RelativeDateTimeCacheDataEEE
	.section	.rodata._ZTSN6icu_678CacheKeyINS_25RelativeDateTimeCacheDataEEE,"aG",@progbits,_ZTSN6icu_678CacheKeyINS_25RelativeDateTimeCacheDataEEE,comdat
	.align 32
	.type	_ZTSN6icu_678CacheKeyINS_25RelativeDateTimeCacheDataEEE, @object
	.size	_ZTSN6icu_678CacheKeyINS_25RelativeDateTimeCacheDataEEE, 52
_ZTSN6icu_678CacheKeyINS_25RelativeDateTimeCacheDataEEE:
	.string	"N6icu_678CacheKeyINS_25RelativeDateTimeCacheDataEEE"
	.weak	_ZTIN6icu_678CacheKeyINS_25RelativeDateTimeCacheDataEEE
	.section	.data.rel.ro._ZTIN6icu_678CacheKeyINS_25RelativeDateTimeCacheDataEEE,"awG",@progbits,_ZTIN6icu_678CacheKeyINS_25RelativeDateTimeCacheDataEEE,comdat
	.align 8
	.type	_ZTIN6icu_678CacheKeyINS_25RelativeDateTimeCacheDataEEE, @object
	.size	_ZTIN6icu_678CacheKeyINS_25RelativeDateTimeCacheDataEEE, 24
_ZTIN6icu_678CacheKeyINS_25RelativeDateTimeCacheDataEEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_678CacheKeyINS_25RelativeDateTimeCacheDataEEE
	.quad	_ZTIN6icu_6712CacheKeyBaseE
	.weak	_ZTSN6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEEE
	.section	.rodata._ZTSN6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEEE,"aG",@progbits,_ZTSN6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEEE,comdat
	.align 32
	.type	_ZTSN6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEEE, @object
	.size	_ZTSN6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEEE, 59
_ZTSN6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEEE:
	.string	"N6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEEE"
	.weak	_ZTIN6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEEE
	.section	.data.rel.ro._ZTIN6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEEE,"awG",@progbits,_ZTIN6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEEE,comdat
	.align 8
	.type	_ZTIN6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEEE, @object
	.size	_ZTIN6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEEE, 24
_ZTIN6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEEE
	.quad	_ZTIN6icu_678CacheKeyINS_25RelativeDateTimeCacheDataEEE
	.weak	_ZTSN6icu_6729FormattedRelativeDateTimeDataE
	.section	.rodata._ZTSN6icu_6729FormattedRelativeDateTimeDataE,"aG",@progbits,_ZTSN6icu_6729FormattedRelativeDateTimeDataE,comdat
	.align 32
	.type	_ZTSN6icu_6729FormattedRelativeDateTimeDataE, @object
	.size	_ZTSN6icu_6729FormattedRelativeDateTimeDataE, 41
_ZTSN6icu_6729FormattedRelativeDateTimeDataE:
	.string	"N6icu_6729FormattedRelativeDateTimeDataE"
	.weak	_ZTIN6icu_6729FormattedRelativeDateTimeDataE
	.section	.data.rel.ro._ZTIN6icu_6729FormattedRelativeDateTimeDataE,"awG",@progbits,_ZTIN6icu_6729FormattedRelativeDateTimeDataE,comdat
	.align 8
	.type	_ZTIN6icu_6729FormattedRelativeDateTimeDataE, @object
	.size	_ZTIN6icu_6729FormattedRelativeDateTimeDataE, 24
_ZTIN6icu_6729FormattedRelativeDateTimeDataE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6729FormattedRelativeDateTimeDataE
	.quad	_ZTIN6icu_6731FormattedValueStringBuilderImplE
	.weak	_ZTVN6icu_6725RelativeDateTimeCacheDataE
	.section	.data.rel.ro._ZTVN6icu_6725RelativeDateTimeCacheDataE,"awG",@progbits,_ZTVN6icu_6725RelativeDateTimeCacheDataE,comdat
	.align 8
	.type	_ZTVN6icu_6725RelativeDateTimeCacheDataE, @object
	.size	_ZTVN6icu_6725RelativeDateTimeCacheDataE, 40
_ZTVN6icu_6725RelativeDateTimeCacheDataE:
	.quad	0
	.quad	_ZTIN6icu_6725RelativeDateTimeCacheDataE
	.quad	_ZN6icu_6725RelativeDateTimeCacheDataD1Ev
	.quad	_ZN6icu_6725RelativeDateTimeCacheDataD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.section	.data.rel.ro
	.align 8
	.type	_ZTVN6icu_6712_GLOBAL__N_122RelDateTimeFmtDataSinkE, @object
	.size	_ZTVN6icu_6712_GLOBAL__N_122RelDateTimeFmtDataSinkE, 48
_ZTVN6icu_6712_GLOBAL__N_122RelDateTimeFmtDataSinkE:
	.quad	0
	.quad	_ZTIN6icu_6712_GLOBAL__N_122RelDateTimeFmtDataSinkE
	.quad	_ZN6icu_6712_GLOBAL__N_122RelDateTimeFmtDataSinkD1Ev
	.quad	_ZN6icu_6712_GLOBAL__N_122RelDateTimeFmtDataSinkD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZN6icu_6712_GLOBAL__N_122RelDateTimeFmtDataSink3putEPKcRNS_13ResourceValueEaR10UErrorCode
	.weak	_ZTVN6icu_678CacheKeyINS_25RelativeDateTimeCacheDataEEE
	.section	.data.rel.ro._ZTVN6icu_678CacheKeyINS_25RelativeDateTimeCacheDataEEE,"awG",@progbits,_ZTVN6icu_678CacheKeyINS_25RelativeDateTimeCacheDataEEE,comdat
	.align 8
	.type	_ZTVN6icu_678CacheKeyINS_25RelativeDateTimeCacheDataEEE, @object
	.size	_ZTVN6icu_678CacheKeyINS_25RelativeDateTimeCacheDataEEE, 80
_ZTVN6icu_678CacheKeyINS_25RelativeDateTimeCacheDataEEE:
	.quad	0
	.quad	_ZTIN6icu_678CacheKeyINS_25RelativeDateTimeCacheDataEEE
	.quad	0
	.quad	0
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZNK6icu_678CacheKeyINS_25RelativeDateTimeCacheDataEE8hashCodeEv
	.quad	__cxa_pure_virtual
	.quad	_ZNK6icu_678CacheKeyINS_25RelativeDateTimeCacheDataEEeqERKNS_12CacheKeyBaseE
	.quad	__cxa_pure_virtual
	.quad	_ZNK6icu_678CacheKeyINS_25RelativeDateTimeCacheDataEE16writeDescriptionEPci
	.weak	_ZTVN6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEEE
	.section	.data.rel.ro._ZTVN6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEEE,"awG",@progbits,_ZTVN6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEEE,comdat
	.align 8
	.type	_ZTVN6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEEE, @object
	.size	_ZTVN6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEEE, 80
_ZTVN6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEEE:
	.quad	0
	.quad	_ZTIN6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEEE
	.quad	_ZN6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEED1Ev
	.quad	_ZN6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEED0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZNK6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEE8hashCodeEv
	.quad	_ZNK6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEE5cloneEv
	.quad	_ZNK6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEEeqERKNS_12CacheKeyBaseE
	.quad	_ZNK6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEE12createObjectEPKvR10UErrorCode
	.quad	_ZNK6icu_6714LocaleCacheKeyINS_25RelativeDateTimeCacheDataEE16writeDescriptionEPci
	.weak	_ZTVN6icu_6729FormattedRelativeDateTimeDataE
	.section	.data.rel.ro._ZTVN6icu_6729FormattedRelativeDateTimeDataE,"awG",@progbits,_ZTVN6icu_6729FormattedRelativeDateTimeDataE,comdat
	.align 8
	.type	_ZTVN6icu_6729FormattedRelativeDateTimeDataE, @object
	.size	_ZTVN6icu_6729FormattedRelativeDateTimeDataE, 64
_ZTVN6icu_6729FormattedRelativeDateTimeDataE:
	.quad	0
	.quad	_ZTIN6icu_6729FormattedRelativeDateTimeDataE
	.quad	_ZN6icu_6729FormattedRelativeDateTimeDataD1Ev
	.quad	_ZN6icu_6729FormattedRelativeDateTimeDataD0Ev
	.quad	_ZNK6icu_6731FormattedValueStringBuilderImpl8toStringER10UErrorCode
	.quad	_ZNK6icu_6731FormattedValueStringBuilderImpl12toTempStringER10UErrorCode
	.quad	_ZNK6icu_6731FormattedValueStringBuilderImpl8appendToERNS_10AppendableER10UErrorCode
	.quad	_ZNK6icu_6731FormattedValueStringBuilderImpl12nextPositionERNS_24ConstrainedFieldPositionER10UErrorCode
	.weak	_ZTVN6icu_6725FormattedRelativeDateTimeE
	.section	.data.rel.ro.local._ZTVN6icu_6725FormattedRelativeDateTimeE,"awG",@progbits,_ZTVN6icu_6725FormattedRelativeDateTimeE,comdat
	.align 8
	.type	_ZTVN6icu_6725FormattedRelativeDateTimeE, @object
	.size	_ZTVN6icu_6725FormattedRelativeDateTimeE, 64
_ZTVN6icu_6725FormattedRelativeDateTimeE:
	.quad	0
	.quad	_ZTIN6icu_6725FormattedRelativeDateTimeE
	.quad	_ZN6icu_6725FormattedRelativeDateTimeD1Ev
	.quad	_ZN6icu_6725FormattedRelativeDateTimeD0Ev
	.quad	_ZNK6icu_6725FormattedRelativeDateTime8toStringER10UErrorCode
	.quad	_ZNK6icu_6725FormattedRelativeDateTime12toTempStringER10UErrorCode
	.quad	_ZNK6icu_6725FormattedRelativeDateTime8appendToERNS_10AppendableER10UErrorCode
	.quad	_ZNK6icu_6725FormattedRelativeDateTime12nextPositionERNS_24ConstrainedFieldPositionER10UErrorCode
	.weak	_ZTVN6icu_6725RelativeDateTimeFormatterE
	.section	.data.rel.ro._ZTVN6icu_6725RelativeDateTimeFormatterE,"awG",@progbits,_ZTVN6icu_6725RelativeDateTimeFormatterE,comdat
	.align 8
	.type	_ZTVN6icu_6725RelativeDateTimeFormatterE, @object
	.size	_ZTVN6icu_6725RelativeDateTimeFormatterE, 40
_ZTVN6icu_6725RelativeDateTimeFormatterE:
	.quad	0
	.quad	_ZTIN6icu_6725RelativeDateTimeFormatterE
	.quad	_ZN6icu_6725RelativeDateTimeFormatterD1Ev
	.quad	_ZN6icu_6725RelativeDateTimeFormatterD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.local	_ZZNK6icu_6725RelativeDateTimeFormatter16adjustForContextERNS_13UnicodeStringEE13gBrkIterMutex
	.comm	_ZZNK6icu_6725RelativeDateTimeFormatter16adjustForContextERNS_13UnicodeStringEE13gBrkIterMutex,56,32
	.section	.rodata
	.type	_ZN6icu_67L16kRDTLiteralFieldE, @object
	.size	_ZN6icu_67L16kRDTLiteralFieldE, 1
_ZN6icu_67L16kRDTLiteralFieldE:
	.byte	64
	.type	_ZN6icu_67L16kRDTNumericFieldE, @object
	.size	_ZN6icu_67L16kRDTNumericFieldE, 1
_ZN6icu_67L16kRDTNumericFieldE:
	.byte	65
	.align 8
	.type	_ZN6icu_67L28styleToDateFormatSymbolWidthE, @object
	.size	_ZN6icu_67L28styleToDateFormatSymbolWidthE, 12
_ZN6icu_67L28styleToDateFormatSymbolWidthE:
	.long	1
	.long	3
	.long	2
	.align 8
	.type	_ZZN6icu_6712_GLOBAL__N_122RelDateTimeFmtDataSink27styleFromAliasUnicodeStringENS_13UnicodeStringEE6sshort, @object
	.size	_ZZN6icu_6712_GLOBAL__N_122RelDateTimeFmtDataSink27styleFromAliasUnicodeStringENS_13UnicodeStringEE6sshort, 12
_ZZN6icu_6712_GLOBAL__N_122RelDateTimeFmtDataSink27styleFromAliasUnicodeStringENS_13UnicodeStringEE6sshort:
	.value	45
	.value	115
	.value	104
	.value	111
	.value	114
	.value	116
	.align 8
	.type	_ZZN6icu_6712_GLOBAL__N_122RelDateTimeFmtDataSink27styleFromAliasUnicodeStringENS_13UnicodeStringEE6narrow, @object
	.size	_ZZN6icu_6712_GLOBAL__N_122RelDateTimeFmtDataSink27styleFromAliasUnicodeStringENS_13UnicodeStringEE6narrow, 14
_ZZN6icu_6712_GLOBAL__N_122RelDateTimeFmtDataSink27styleFromAliasUnicodeStringENS_13UnicodeStringEE6narrow:
	.value	45
	.value	110
	.value	97
	.value	114
	.value	114
	.value	111
	.value	119
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	3435973837
	.long	-1073689396
	.align 8
.LC2:
	.long	3435973837
	.long	1073794252
	.align 8
.LC3:
	.long	0
	.long	1079574528
	.align 8
.LC5:
	.long	0
	.long	1071644672
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
