	.file	"number_padding.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl6PadderC2Eii24UNumberFormatPadPosition
	.type	_ZN6icu_676number4impl6PadderC2Eii24UNumberFormatPadPosition, @function
_ZN6icu_676number4impl6PadderC2Eii24UNumberFormatPadPosition:
.LFB2708:
	.cfi_startproc
	endbr64
	movl	%edx, (%rdi)
	movl	%esi, 4(%rdi)
	movl	%ecx, 8(%rdi)
	ret
	.cfi_endproc
.LFE2708:
	.size	_ZN6icu_676number4impl6PadderC2Eii24UNumberFormatPadPosition, .-_ZN6icu_676number4impl6PadderC2Eii24UNumberFormatPadPosition
	.globl	_ZN6icu_676number4impl6PadderC1Eii24UNumberFormatPadPosition
	.set	_ZN6icu_676number4impl6PadderC1Eii24UNumberFormatPadPosition,_ZN6icu_676number4impl6PadderC2Eii24UNumberFormatPadPosition
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl6PadderC2Ei
	.type	_ZN6icu_676number4impl6PadderC2Ei, @function
_ZN6icu_676number4impl6PadderC2Ei:
.LFB2711:
	.cfi_startproc
	endbr64
	movl	%esi, (%rdi)
	ret
	.cfi_endproc
.LFE2711:
	.size	_ZN6icu_676number4impl6PadderC2Ei, .-_ZN6icu_676number4impl6PadderC2Ei
	.globl	_ZN6icu_676number4impl6PadderC1Ei
	.set	_ZN6icu_676number4impl6PadderC1Ei,_ZN6icu_676number4impl6PadderC2Ei
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl6Padder4noneEv
	.type	_ZN6icu_676number4impl6Padder4noneEv, @function
_ZN6icu_676number4impl6Padder4noneEv:
.LFB2713:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	$-1, -12(%rbp)
	movl	-4(%rbp), %edx
	movq	-12(%rbp), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2713:
	.size	_ZN6icu_676number4impl6Padder4noneEv, .-_ZN6icu_676number4impl6Padder4noneEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl6Padder10codePointsEii24UNumberFormatPadPosition
	.type	_ZN6icu_676number4impl6Padder10codePointsEii24UNumberFormatPadPosition, @function
_ZN6icu_676number4impl6Padder10codePointsEii24UNumberFormatPadPosition:
.LFB2714:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testl	%esi, %esi
	js	.L8
	movl	%edx, -4(%rbp)
	movl	-4(%rbp), %edx
	movl	%edi, -8(%rbp)
	movl	%esi, -12(%rbp)
	movq	-12(%rbp), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_restore_state
	movl	$65810, %edi
	movl	$-3, %esi
	movl	-4(%rbp), %edx
	movl	%edi, -8(%rbp)
	movl	%esi, -12(%rbp)
	movq	-12(%rbp), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2714:
	.size	_ZN6icu_676number4impl6Padder10codePointsEii24UNumberFormatPadPosition, .-_ZN6icu_676number4impl6Padder10codePointsEii24UNumberFormatPadPosition
	.align 2
	.p2align 4
	.globl	_ZN6icu_676number4impl6Padder13forPropertiesERKNS1_23DecimalFormatPropertiesE
	.type	_ZN6icu_676number4impl6Padder13forPropertiesERKNS1_23DecimalFormatPropertiesE, @function
_ZN6icu_676number4impl6Padder13forPropertiesERKNS1_23DecimalFormatPropertiesE:
.LFB2715:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$40, %rsp
	movswl	400(%rdi), %eax
	testw	%ax, %ax
	js	.L11
	sarl	$5, %eax
.L12:
	movl	$32, %ecx
	testl	%eax, %eax
	jle	.L13
	leaq	392(%rbx), %rdi
	xorl	%esi, %esi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movl	%eax, %ecx
.L13:
	xorl	%edx, %edx
	cmpb	$0, 384(%rbx)
	movl	68(%rbx), %eax
	jne	.L14
	movl	388(%rbx), %edx
.L14:
	movl	%eax, -28(%rbp)
	movl	%ecx, -24(%rbp)
	movq	-28(%rbp), %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore_state
	movl	404(%rdi), %eax
	jmp	.L12
	.cfi_endproc
.LFE2715:
	.size	_ZN6icu_676number4impl6Padder13forPropertiesERKNS1_23DecimalFormatPropertiesE, .-_ZN6icu_676number4impl6Padder13forPropertiesERKNS1_23DecimalFormatPropertiesE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676number4impl6Padder11padAndApplyERKNS1_8ModifierES5_RNS_22FormattedStringBuilderEiiR10UErrorCode
	.type	_ZNK6icu_676number4impl6Padder11padAndApplyERKNS1_8ModifierES5_RNS_22FormattedStringBuilderEiiR10UErrorCode, @function
_ZNK6icu_676number4impl6Padder11padAndApplyERKNS1_8ModifierES5_RNS_22FormattedStringBuilderEiiR10UErrorCode:
.LFB2716:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	movq	%rsi, %rdi
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%r8d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rax
	movq	16(%rbp), %rbx
	movq	%rsi, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movl	%r9d, -56(%rbp)
	call	*32(%rax)
	movq	-64(%rbp), %rdi
	movl	%eax, %r14d
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r12, %rdi
	addl	%eax, %r14d
	movl	(%r15), %eax
	subl	%r14d, %eax
	movl	%eax, %r14d
	call	_ZNK6icu_6722FormattedStringBuilder14codePointCountEv@PLT
	subl	%eax, %r14d
	movl	%r14d, -52(%rbp)
	testl	%r14d, %r14d
	jle	.L38
	movl	8(%r15), %r9d
	movl	4(%r15), %r14d
	cmpl	$1, %r9d
	je	.L39
	cmpl	$2, %r9d
	je	.L40
	movq	-72(%rbp), %rdi
	movl	%r9d, -76(%rbp)
	movq	%rbx, %r8
	movl	%r13d, %edx
	movl	-56(%rbp), %ecx
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	-64(%rbp), %rdi
	movq	%rbx, %r8
	movl	%r13d, %edx
	movl	%eax, %r15d
	movl	-56(%rbp), %eax
	movq	%r12, %rsi
	leal	(%rax,%r15), %ecx
	movq	(%rdi), %rax
	call	*16(%rax)
	movl	-76(%rbp), %r9d
	addl	%r15d, %eax
	movl	%eax, -64(%rbp)
	testl	%r9d, %r9d
	jne	.L27
	movzbl	_ZN6icu_67L15kUndefinedFieldE(%rip), %r15d
	movq	%rbx, %r8
	movl	%r9d, %ebx
	.p2align 4,,10
	.p2align 3
.L28:
	movl	%r15d, %ecx
	movl	%r14d, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	movq	%r8, 16(%rbp)
	addl	$1, %ebx
	call	_ZN6icu_6722FormattedStringBuilder15insertCodePointEiiNS0_5FieldER10UErrorCode@PLT
	cmpl	%ebx, -52(%rbp)
	movq	16(%rbp), %r8
	jne	.L28
	movl	-52(%rbp), %edx
	cmpl	$65535, %r14d
	leal	(%rdx,%rdx), %eax
	cmovbe	%edx, %eax
	addl	%eax, -64(%rbp)
.L18:
	movl	-64(%rbp), %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	.cfi_restore_state
	movzbl	_ZN6icu_67L15kUndefinedFieldE(%rip), %r15d
	xorl	%r9d, %r9d
	movq	%rbx, %r8
	movl	%r15d, %ebx
	movl	%r9d, %r15d
	.p2align 4,,10
	.p2align 3
.L22:
	movl	%ebx, %ecx
	movl	%r14d, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	movq	%r8, 16(%rbp)
	addl	$1, %r15d
	call	_ZN6icu_6722FormattedStringBuilder15insertCodePointEiiNS0_5FieldER10UErrorCode@PLT
	cmpl	%r15d, -52(%rbp)
	movq	16(%rbp), %r8
	jne	.L22
	movl	-52(%rbp), %edx
	cmpl	$65535, %r14d
	movq	%r8, %rbx
	movq	%r12, %rsi
	movq	-72(%rbp), %rdi
	movl	-56(%rbp), %r14d
	leal	(%rdx,%rdx), %eax
	cmovbe	%edx, %eax
	movl	%r13d, %edx
	movl	%eax, %r15d
	movl	%eax, %ecx
	movq	(%rdi), %rax
	addl	%r14d, %ecx
	call	*16(%rax)
	addl	%eax, %r15d
	leal	(%r14,%r15), %ecx
.L37:
	movq	-64(%rbp), %rdi
	movq	%rbx, %r8
	movl	%r13d, %edx
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*16(%rax)
	addl	%r15d, %eax
	movl	%eax, -64(%rbp)
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L38:
	movq	-72(%rbp), %rdi
	movl	-56(%rbp), %r14d
	movq	%rbx, %r8
	movl	%r13d, %edx
	movq	%r12, %rsi
	movq	(%rdi), %rax
	movl	%r14d, %ecx
	call	*16(%rax)
	movl	%r14d, %ecx
	movl	%eax, %r15d
	addl	%eax, %ecx
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L40:
	movzbl	_ZN6icu_67L15kUndefinedFieldE(%rip), %r15d
	xorl	%r9d, %r9d
	movl	%r13d, -76(%rbp)
	movl	%r9d, %r13d
	movl	%r15d, %eax
	movq	%rbx, %r15
	movl	%eax, %ebx
	.p2align 4,,10
	.p2align 3
.L25:
	movl	-56(%rbp), %esi
	movq	%r15, %r8
	movl	%ebx, %ecx
	movl	%r14d, %edx
	movq	%r12, %rdi
	addl	$1, %r13d
	call	_ZN6icu_6722FormattedStringBuilder15insertCodePointEiiNS0_5FieldER10UErrorCode@PLT
	cmpl	%r13d, -52(%rbp)
	jne	.L25
	movl	-52(%rbp), %edx
	cmpl	$65535, %r14d
	movq	%r15, %rbx
	movq	%r12, %rsi
	movl	-56(%rbp), %r14d
	movq	-72(%rbp), %rdi
	movq	%rbx, %r8
	leal	(%rdx,%rdx), %eax
	movl	-76(%rbp), %r13d
	cmovbe	%edx, %eax
	movl	%r14d, %ecx
	movl	%r13d, %edx
	addl	%eax, %ecx
	movl	%eax, %r15d
	movq	(%rdi), %rax
	call	*16(%rax)
	movl	%r14d, %ecx
	addl	%eax, %r15d
	addl	%r15d, %ecx
	jmp	.L37
.L27:
	cmpl	$3, %r9d
	jne	.L18
	movl	-56(%rbp), %eax
	addl	-64(%rbp), %eax
	xorl	%r13d, %r13d
	movl	%eax, -56(%rbp)
	movzbl	_ZN6icu_67L15kUndefinedFieldE(%rip), %r15d
	movq	%r12, %rax
	movl	%r13d, %r12d
	movq	%rax, %r13
	.p2align 4,,10
	.p2align 3
.L30:
	movl	-56(%rbp), %esi
	movq	%rbx, %r8
	movl	%r15d, %ecx
	movl	%r14d, %edx
	movq	%r13, %rdi
	addl	$1, %r12d
	call	_ZN6icu_6722FormattedStringBuilder15insertCodePointEiiNS0_5FieldER10UErrorCode@PLT
	cmpl	%r12d, -52(%rbp)
	jne	.L30
	movl	-52(%rbp), %ebx
	cmpl	$65535, %r14d
	leal	(%rbx,%rbx), %eax
	cmovbe	%ebx, %eax
	addl	%eax, -64(%rbp)
	jmp	.L18
	.cfi_endproc
.LFE2716:
	.size	_ZNK6icu_676number4impl6Padder11padAndApplyERKNS1_8ModifierES5_RNS_22FormattedStringBuilderEiiR10UErrorCode, .-_ZNK6icu_676number4impl6Padder11padAndApplyERKNS1_8ModifierES5_RNS_22FormattedStringBuilderEiiR10UErrorCode
	.section	.rodata
	.type	_ZN6icu_67L15kUndefinedFieldE, @object
	.size	_ZN6icu_67L15kUndefinedFieldE, 1
_ZN6icu_67L15kUndefinedFieldE:
	.zero	1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
