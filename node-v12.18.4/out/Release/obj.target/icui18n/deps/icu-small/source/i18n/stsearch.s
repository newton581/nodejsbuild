	.file	"stsearch.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712StringSearch17getDynamicClassIDEv
	.type	_ZNK6icu_6712StringSearch17getDynamicClassIDEv, @function
_ZNK6icu_6712StringSearch17getDynamicClassIDEv:
.LFB2488:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6712StringSearch16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2488:
	.size	_ZNK6icu_6712StringSearch17getDynamicClassIDEv, .-_ZNK6icu_6712StringSearch17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712StringSearch9setOffsetEiR10UErrorCode
	.type	_ZN6icu_6712StringSearch9setOffsetEiR10UErrorCode, @function
_ZN6icu_6712StringSearch9setOffsetEiR10UErrorCode:
.LFB2511:
	.cfi_startproc
	endbr64
	movq	152(%rdi), %rdi
	jmp	usearch_setOffset_67@PLT
	.cfi_endproc
.LFE2511:
	.size	_ZN6icu_6712StringSearch9setOffsetEiR10UErrorCode, .-_ZN6icu_6712StringSearch9setOffsetEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712StringSearch9getOffsetEv
	.type	_ZNK6icu_6712StringSearch9getOffsetEv, @function
_ZNK6icu_6712StringSearch9getOffsetEv:
.LFB2512:
	.cfi_startproc
	endbr64
	movq	152(%rdi), %rdi
	jmp	usearch_getOffset_67@PLT
	.cfi_endproc
.LFE2512:
	.size	_ZNK6icu_6712StringSearch9getOffsetEv, .-_ZNK6icu_6712StringSearch9getOffsetEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712StringSearch5resetEv
	.type	_ZN6icu_6712StringSearch5resetEv, @function
_ZN6icu_6712StringSearch5resetEv:
.LFB2519:
	.cfi_startproc
	endbr64
	movq	152(%rdi), %rdi
	jmp	usearch_reset_67@PLT
	.cfi_endproc
.LFE2519:
	.size	_ZN6icu_6712StringSearch5resetEv, .-_ZN6icu_6712StringSearch5resetEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712StringSearch10handleNextEiR10UErrorCode
	.type	_ZN6icu_6712StringSearch10handleNextEiR10UErrorCode, @function
_ZN6icu_6712StringSearch10handleNextEiR10UErrorCode:
.LFB2521:
	.cfi_startproc
	endbr64
	movl	(%rdx), %r9d
	testl	%r9d, %r9d
	jg	.L22
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	152(%rdi), %rdi
	movq	8(%rbx), %rdx
	movl	20(%rdi), %r8d
	testl	%r8d, %r8d
	jne	.L9
	movl	32(%rdx), %eax
	leal	1(%rax), %esi
	cmpl	$-1, %eax
	je	.L23
.L11:
	movl	%esi, 32(%rdx)
	movq	4168(%rdi), %rdi
	movl	$0, 36(%rdx)
	movq	%r12, %rdx
	call	ucol_setOffset_67@PLT
	movq	8(%rbx), %rax
	movl	8(%rax), %ecx
	cmpl	%ecx, 32(%rax)
	jne	.L21
	movl	$-1, 32(%rax)
.L21:
	popq	%rbx
	movl	$-1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore_state
	movl	36(%rdx), %ecx
	testl	%ecx, %ecx
	jg	.L13
	leal	-1(%rsi), %eax
	movl	%eax, 32(%rdx)
.L13:
	movq	4168(%rdi), %rdi
	movq	%r12, %rdx
	call	ucol_setOffset_67@PLT
	movq	8(%rbx), %rax
	movq	152(%rbx), %rdi
	movq	%r12, %rsi
	cmpb	$0, 13(%rax)
	je	.L14
	call	usearch_handleNextCanonical_67@PLT
.L15:
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L21
	movq	8(%rbx), %rax
	movq	152(%rbx), %rdx
	movl	32(%rax), %esi
	movq	4168(%rdx), %rdi
	cmpl	$-1, %esi
	je	.L24
	movq	%r12, %rdx
	call	ucol_setOffset_67@PLT
.L17:
	movq	8(%rbx), %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movl	32(%rax), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_restore_state
	call	usearch_handleNextExact_67@PLT
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L24:
	movl	8(%rax), %esi
	movq	%r12, %rdx
	call	ucol_setOffset_67@PLT
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L23:
	call	usearch_getOffset_67@PLT
	movq	8(%rbx), %rdx
	movq	152(%rbx), %rdi
	movl	%eax, %esi
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L22:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE2521:
	.size	_ZN6icu_6712StringSearch10handleNextEiR10UErrorCode, .-_ZN6icu_6712StringSearch10handleNextEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712StringSearch10handlePrevEiR10UErrorCode
	.type	_ZN6icu_6712StringSearch10handlePrevEiR10UErrorCode, @function
_ZN6icu_6712StringSearch10handlePrevEiR10UErrorCode:
.LFB2522:
	.cfi_startproc
	endbr64
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jg	.L38
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	152(%rdi), %rdi
	movl	20(%rdi), %edx
	testl	%edx, %edx
	jne	.L28
	movq	8(%rbx), %rdx
	movl	32(%rdx), %eax
	cmpl	$-1, %eax
	je	.L39
.L29:
	movl	%eax, 32(%rdx)
	testl	%eax, %eax
	jne	.L30
	movq	%rbx, %rdi
	call	_ZN6icu_6714SearchIterator16setMatchNotFoundEv@PLT
	movq	8(%rbx), %rax
.L31:
	movl	32(%rax), %eax
.L25:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	.cfi_restore_state
	movq	4168(%rdi), %rdi
	movq	%r12, %rdx
	call	ucol_setOffset_67@PLT
	movq	8(%rbx), %rax
	movq	152(%rbx), %rdi
	movq	%r12, %rsi
	cmpb	$0, 13(%rax)
	je	.L32
	call	usearch_handlePreviousCanonical_67@PLT
.L33:
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L34
	movq	8(%rbx), %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movl	32(%rax), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_restore_state
	leal	-1(%rax), %esi
	movq	152(%rbx), %rax
	movl	%esi, 32(%rdx)
	movq	%r12, %rdx
	movq	4168(%rax), %rdi
	call	ucol_setOffset_67@PLT
	movq	8(%rbx), %rax
	movl	$0, 36(%rax)
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L32:
	call	usearch_handlePreviousExact_67@PLT
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L39:
	call	usearch_getOffset_67@PLT
	movq	8(%rbx), %rdx
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L34:
	movl	$-1, %eax
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L38:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE2522:
	.size	_ZN6icu_6712StringSearch10handlePrevEiR10UErrorCode, .-_ZN6icu_6712StringSearch10handlePrevEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712StringSearch7setTextERKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6712StringSearch7setTextERKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6712StringSearch7setTextERKNS_13UnicodeStringER10UErrorCode:
.LFB2513:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jle	.L50
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	leaq	24(%rdi), %rdi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movzwl	8(%rbx), %eax
	testw	%ax, %ax
	js	.L42
	movswl	%ax, %edx
	sarl	$5, %edx
.L43:
	testb	$17, %al
	jne	.L46
	leaq	10(%rbx), %rsi
	testb	$2, %al
	jne	.L44
	movq	24(%rbx), %rsi
.L44:
	movq	152(%r13), %rdi
	addq	$8, %rsp
	movq	%r12, %rcx
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	usearch_setText_67@PLT
	.p2align 4,,10
	.p2align 3
.L42:
	.cfi_restore_state
	movl	12(%rbx), %edx
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L46:
	xorl	%esi, %esi
	jmp	.L44
	.cfi_endproc
.LFE2513:
	.size	_ZN6icu_6712StringSearch7setTextERKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6712StringSearch7setTextERKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712StringSearch7setTextERNS_17CharacterIteratorER10UErrorCode
	.type	_ZN6icu_6712StringSearch7setTextERNS_17CharacterIteratorER10UErrorCode, @function
_ZN6icu_6712StringSearch7setTextERNS_17CharacterIteratorER10UErrorCode:
.LFB2514:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jle	.L61
	ret
	.p2align 4,,10
	.p2align 3
.L61:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movq	(%rdi), %rax
	leaq	24(%rbx), %rsi
	call	*208(%rax)
	movzwl	32(%rbx), %eax
	testw	%ax, %ax
	js	.L53
	movswl	%ax, %edx
	sarl	$5, %edx
.L54:
	testb	$17, %al
	jne	.L57
	leaq	34(%rbx), %rsi
	testb	$2, %al
	jne	.L55
	movq	48(%rbx), %rsi
.L55:
	movq	152(%rbx), %rdi
	movq	%r12, %rcx
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	usearch_setText_67@PLT
	.p2align 4,,10
	.p2align 3
.L53:
	.cfi_restore_state
	movl	36(%rbx), %edx
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L57:
	xorl	%esi, %esi
	jmp	.L55
	.cfi_endproc
.LFE2514:
	.size	_ZN6icu_6712StringSearch7setTextERNS_17CharacterIteratorER10UErrorCode, .-_ZN6icu_6712StringSearch7setTextERNS_17CharacterIteratorER10UErrorCode
	.align 2
	.p2align 4
	.type	_ZN6icu_6712StringSearchC2ERKNS_13UnicodeStringES3_PNS_17RuleBasedCollatorEPNS_13BreakIteratorER10UErrorCode.part.0, @function
_ZN6icu_6712StringSearchC2ERKNS_13UnicodeStringES3_PNS_17RuleBasedCollatorEPNS_13BreakIteratorER10UErrorCode.part.0:
.LFB3427:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rdx, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movzwl	32(%rdi), %eax
	movq	%rdi, %rbx
	testw	%ax, %ax
	js	.L63
	movswl	%ax, %ecx
	sarl	$5, %ecx
	testb	$17, %al
	jne	.L72
.L79:
	leaq	34(%rbx), %rdx
	testb	$2, %al
	je	.L75
.L65:
	movzwl	96(%rbx), %eax
	testw	%ax, %ax
	js	.L67
.L77:
	movswl	%ax, %esi
	sarl	$5, %esi
	testb	$17, %al
	jne	.L73
.L78:
	leaq	98(%rbx), %rdi
	testb	$2, %al
	je	.L76
.L69:
	subq	$8, %rsp
	pushq	%r12
	call	usearch_openFromCollator_67@PLT
	movq	8(%rbx), %rdi
	movq	%rax, 152(%rbx)
	call	uprv_free_67@PLT
	movl	(%r12), %ecx
	popq	%rax
	movq	$0, 8(%rbx)
	popq	%rdx
	testl	%ecx, %ecx
	jg	.L62
	movq	152(%rbx), %rax
	movq	(%rax), %rax
	movq	%rax, 8(%rbx)
.L62:
	leaq	-16(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L76:
	.cfi_restore_state
	movq	112(%rbx), %rdi
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L75:
	movzwl	96(%rbx), %eax
	movq	48(%rbx), %rdx
	testw	%ax, %ax
	jns	.L77
.L67:
	movl	100(%rbx), %esi
	testb	$17, %al
	je	.L78
.L73:
	xorl	%edi, %edi
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L63:
	movl	36(%rdi), %ecx
	testb	$17, %al
	je	.L79
.L72:
	xorl	%edx, %edx
	jmp	.L65
	.cfi_endproc
.LFE3427:
	.size	_ZN6icu_6712StringSearchC2ERKNS_13UnicodeStringES3_PNS_17RuleBasedCollatorEPNS_13BreakIteratorER10UErrorCode.part.0, .-_ZN6icu_6712StringSearchC2ERKNS_13UnicodeStringES3_PNS_17RuleBasedCollatorEPNS_13BreakIteratorER10UErrorCode.part.0
	.set	_ZN6icu_6712StringSearchC2ERKNS_13UnicodeStringERNS_17CharacterIteratorEPNS_17RuleBasedCollatorEPNS_13BreakIteratorER10UErrorCode.part.0,_ZN6icu_6712StringSearchC2ERKNS_13UnicodeStringES3_PNS_17RuleBasedCollatorEPNS_13BreakIteratorER10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712StringSearch9safeCloneEv
	.type	_ZNK6icu_6712StringSearch9safeCloneEv, @function
_ZNK6icu_6712StringSearch9safeCloneEv:
.LFB2520:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	152(%rdi), %rax
	movl	$0, -60(%rbp)
	movq	4152(%rax), %r13
	testq	%r13, %r13
	je	.L81
	movq	%r13, %rdi
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6717RuleBasedCollatorE(%rip), %rdx
	leaq	_ZTIN6icu_678CollatorE(%rip), %rsi
	call	__dynamic_cast@PLT
	movq	%rax, %r13
.L81:
	movl	$160, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L82
	movq	16(%rbx), %r14
	movq	%rax, %rdi
	leaq	24(%rbx), %rsi
	movq	%r14, %rdx
	call	_ZN6icu_6714SearchIteratorC2ERKNS_13UnicodeStringEPNS_13BreakIteratorE@PLT
	leaq	16+_ZTVN6icu_6712StringSearchE(%rip), %rax
	leaq	88(%rbx), %rsi
	movq	%rax, (%r12)
	leaq	88(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movl	-60(%rbp), %edx
	testl	%edx, %edx
	jg	.L95
	testq	%r13, %r13
	je	.L96
	leaq	-60(%rbp), %r15
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r15, %rcx
	call	_ZN6icu_6712StringSearchC2ERKNS_13UnicodeStringES3_PNS_17RuleBasedCollatorEPNS_13BreakIteratorER10UErrorCode.part.0
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L96:
	movl	$1, -60(%rbp)
	.p2align 4,,10
	.p2align 3
.L95:
	movq	$0, 152(%r12)
	leaq	-60(%rbp), %r15
.L84:
	movq	152(%rbx), %rdi
	call	usearch_getOffset_67@PLT
	movq	152(%r12), %rdi
	movq	%r15, %rdx
	movl	%eax, %esi
	call	usearch_setOffset_67@PLT
	movq	152(%rbx), %rax
	movq	%r12, %rdi
	movq	(%rax), %rax
	movl	32(%rax), %esi
	movq	(%r12), %rax
	call	*104(%rax)
	movq	152(%rbx), %rax
	movq	%r12, %rdi
	movq	(%rax), %rax
	movl	36(%rax), %esi
	movq	(%r12), %rax
	call	*96(%rax)
	movl	-60(%rbp), %eax
	testl	%eax, %eax
	jg	.L82
.L80:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L97
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L82:
	.cfi_restore_state
	xorl	%r12d, %r12d
	jmp	.L80
.L97:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2520:
	.size	_ZNK6icu_6712StringSearch9safeCloneEv, .-_ZNK6icu_6712StringSearch9safeCloneEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712StringSearch16getStaticClassIDEv
	.type	_ZN6icu_6712StringSearch16getStaticClassIDEv, @function
_ZN6icu_6712StringSearch16getStaticClassIDEv:
.LFB2487:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6712StringSearch16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2487:
	.size	_ZN6icu_6712StringSearch16getStaticClassIDEv, .-_ZN6icu_6712StringSearch16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712StringSearchC2ERKNS_13UnicodeStringES3_RKNS_6LocaleEPNS_13BreakIteratorER10UErrorCode
	.type	_ZN6icu_6712StringSearchC2ERKNS_13UnicodeStringES3_RKNS_6LocaleEPNS_13BreakIteratorER10UErrorCode, @function
_ZN6icu_6712StringSearchC2ERKNS_13UnicodeStringES3_RKNS_6LocaleEPNS_13BreakIteratorER10UErrorCode:
.LFB2490:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movq	%rdx, %rsi
	movq	%r8, %rdx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r9, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_6714SearchIteratorC2ERKNS_13UnicodeStringEPNS_13BreakIteratorE@PLT
	leaq	16+_ZTVN6icu_6712StringSearchE(%rip), %rax
	movq	%r14, %rsi
	leaq	88(%rbx), %rdi
	movq	%rax, (%rbx)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movl	(%r12), %esi
	testl	%esi, %esi
	jg	.L113
	movzwl	32(%rbx), %eax
	movq	40(%r15), %r8
	testw	%ax, %ax
	js	.L102
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L103:
	testb	$17, %al
	jne	.L110
	leaq	34(%rbx), %rdx
	testb	$2, %al
	jne	.L104
	movq	48(%rbx), %rdx
.L104:
	movzwl	96(%rbx), %eax
	testw	%ax, %ax
	js	.L106
	movswl	%ax, %esi
	sarl	$5, %esi
.L107:
	testb	$17, %al
	jne	.L111
	leaq	98(%rbx), %rdi
	testb	$2, %al
	jne	.L108
	movq	112(%rbx), %rdi
.L108:
	subq	$8, %rsp
	movq	%r13, %r9
	pushq	%r12
	call	usearch_open_67@PLT
	movq	8(%rbx), %rdi
	movq	%rax, 152(%rbx)
	call	uprv_free_67@PLT
	movl	(%r12), %ecx
	popq	%rax
	movq	$0, 8(%rbx)
	popq	%rdx
	testl	%ecx, %ecx
	jg	.L99
	movq	152(%rbx), %rax
	movq	(%rax), %rax
	movq	%rax, 8(%rbx)
.L99:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L102:
	.cfi_restore_state
	movl	36(%rbx), %ecx
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L113:
	movq	$0, 152(%rbx)
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L106:
	.cfi_restore_state
	movl	100(%rbx), %esi
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L110:
	xorl	%edx, %edx
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L111:
	xorl	%edi, %edi
	jmp	.L108
	.cfi_endproc
.LFE2490:
	.size	_ZN6icu_6712StringSearchC2ERKNS_13UnicodeStringES3_RKNS_6LocaleEPNS_13BreakIteratorER10UErrorCode, .-_ZN6icu_6712StringSearchC2ERKNS_13UnicodeStringES3_RKNS_6LocaleEPNS_13BreakIteratorER10UErrorCode
	.globl	_ZN6icu_6712StringSearchC1ERKNS_13UnicodeStringES3_RKNS_6LocaleEPNS_13BreakIteratorER10UErrorCode
	.set	_ZN6icu_6712StringSearchC1ERKNS_13UnicodeStringES3_RKNS_6LocaleEPNS_13BreakIteratorER10UErrorCode,_ZN6icu_6712StringSearchC2ERKNS_13UnicodeStringES3_RKNS_6LocaleEPNS_13BreakIteratorER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712StringSearchC2ERKNS_13UnicodeStringES3_PNS_17RuleBasedCollatorEPNS_13BreakIteratorER10UErrorCode
	.type	_ZN6icu_6712StringSearchC2ERKNS_13UnicodeStringES3_PNS_17RuleBasedCollatorEPNS_13BreakIteratorER10UErrorCode, @function
_ZN6icu_6712StringSearchC2ERKNS_13UnicodeStringES3_PNS_17RuleBasedCollatorEPNS_13BreakIteratorER10UErrorCode:
.LFB2493:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r9, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movq	%rdx, %rsi
	movq	%r8, %rdx
	subq	$8, %rsp
	call	_ZN6icu_6714SearchIteratorC2ERKNS_13UnicodeStringEPNS_13BreakIteratorE@PLT
	leaq	16+_ZTVN6icu_6712StringSearchE(%rip), %rax
	leaq	88(%r12), %rdi
	movq	%rbx, %rsi
	movq	%rax, (%r12)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L119
	testq	%r15, %r15
	je	.L120
	addq	$8, %rsp
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%r15, %rsi
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6712StringSearchC2ERKNS_13UnicodeStringES3_PNS_17RuleBasedCollatorEPNS_13BreakIteratorER10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L120:
	.cfi_restore_state
	movl	$1, 0(%r13)
.L119:
	movq	$0, 152(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2493:
	.size	_ZN6icu_6712StringSearchC2ERKNS_13UnicodeStringES3_PNS_17RuleBasedCollatorEPNS_13BreakIteratorER10UErrorCode, .-_ZN6icu_6712StringSearchC2ERKNS_13UnicodeStringES3_PNS_17RuleBasedCollatorEPNS_13BreakIteratorER10UErrorCode
	.globl	_ZN6icu_6712StringSearchC1ERKNS_13UnicodeStringES3_PNS_17RuleBasedCollatorEPNS_13BreakIteratorER10UErrorCode
	.set	_ZN6icu_6712StringSearchC1ERKNS_13UnicodeStringES3_PNS_17RuleBasedCollatorEPNS_13BreakIteratorER10UErrorCode,_ZN6icu_6712StringSearchC2ERKNS_13UnicodeStringES3_PNS_17RuleBasedCollatorEPNS_13BreakIteratorER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712StringSearchC2ERKNS_13UnicodeStringERNS_17CharacterIteratorERKNS_6LocaleEPNS_13BreakIteratorER10UErrorCode
	.type	_ZN6icu_6712StringSearchC2ERKNS_13UnicodeStringERNS_17CharacterIteratorERKNS_6LocaleEPNS_13BreakIteratorER10UErrorCode, @function
_ZN6icu_6712StringSearchC2ERKNS_13UnicodeStringERNS_17CharacterIteratorERKNS_6LocaleEPNS_13BreakIteratorER10UErrorCode:
.LFB2496:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movq	%rdx, %rsi
	movq	%r8, %rdx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r9, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_6714SearchIteratorC2ERNS_17CharacterIteratorEPNS_13BreakIteratorE@PLT
	leaq	16+_ZTVN6icu_6712StringSearchE(%rip), %rax
	movq	%r14, %rsi
	leaq	88(%rbx), %rdi
	movq	%rax, (%rbx)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movl	(%r12), %esi
	testl	%esi, %esi
	jg	.L135
	movzwl	32(%rbx), %eax
	movq	40(%r15), %r8
	testw	%ax, %ax
	js	.L124
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L125:
	testb	$17, %al
	jne	.L132
	leaq	34(%rbx), %rdx
	testb	$2, %al
	jne	.L126
	movq	48(%rbx), %rdx
.L126:
	movzwl	96(%rbx), %eax
	testw	%ax, %ax
	js	.L128
	movswl	%ax, %esi
	sarl	$5, %esi
.L129:
	testb	$17, %al
	jne	.L133
	leaq	98(%rbx), %rdi
	testb	$2, %al
	jne	.L130
	movq	112(%rbx), %rdi
.L130:
	subq	$8, %rsp
	movq	%r13, %r9
	pushq	%r12
	call	usearch_open_67@PLT
	movq	8(%rbx), %rdi
	movq	%rax, 152(%rbx)
	call	uprv_free_67@PLT
	movl	(%r12), %ecx
	popq	%rax
	movq	$0, 8(%rbx)
	popq	%rdx
	testl	%ecx, %ecx
	jg	.L121
	movq	152(%rbx), %rax
	movq	(%rax), %rax
	movq	%rax, 8(%rbx)
.L121:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L124:
	.cfi_restore_state
	movl	36(%rbx), %ecx
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L135:
	movq	$0, 152(%rbx)
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L128:
	.cfi_restore_state
	movl	100(%rbx), %esi
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L132:
	xorl	%edx, %edx
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L133:
	xorl	%edi, %edi
	jmp	.L130
	.cfi_endproc
.LFE2496:
	.size	_ZN6icu_6712StringSearchC2ERKNS_13UnicodeStringERNS_17CharacterIteratorERKNS_6LocaleEPNS_13BreakIteratorER10UErrorCode, .-_ZN6icu_6712StringSearchC2ERKNS_13UnicodeStringERNS_17CharacterIteratorERKNS_6LocaleEPNS_13BreakIteratorER10UErrorCode
	.globl	_ZN6icu_6712StringSearchC1ERKNS_13UnicodeStringERNS_17CharacterIteratorERKNS_6LocaleEPNS_13BreakIteratorER10UErrorCode
	.set	_ZN6icu_6712StringSearchC1ERKNS_13UnicodeStringERNS_17CharacterIteratorERKNS_6LocaleEPNS_13BreakIteratorER10UErrorCode,_ZN6icu_6712StringSearchC2ERKNS_13UnicodeStringERNS_17CharacterIteratorERKNS_6LocaleEPNS_13BreakIteratorER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712StringSearchC2ERKNS_13UnicodeStringERNS_17CharacterIteratorEPNS_17RuleBasedCollatorEPNS_13BreakIteratorER10UErrorCode
	.type	_ZN6icu_6712StringSearchC2ERKNS_13UnicodeStringERNS_17CharacterIteratorEPNS_17RuleBasedCollatorEPNS_13BreakIteratorER10UErrorCode, @function
_ZN6icu_6712StringSearchC2ERKNS_13UnicodeStringERNS_17CharacterIteratorEPNS_17RuleBasedCollatorEPNS_13BreakIteratorER10UErrorCode:
.LFB2499:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r9, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movq	%rdx, %rsi
	movq	%r8, %rdx
	subq	$8, %rsp
	call	_ZN6icu_6714SearchIteratorC2ERNS_17CharacterIteratorEPNS_13BreakIteratorE@PLT
	leaq	16+_ZTVN6icu_6712StringSearchE(%rip), %rax
	leaq	88(%r12), %rdi
	movq	%rbx, %rsi
	movq	%rax, (%r12)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L141
	testq	%r15, %r15
	je	.L142
	addq	$8, %rsp
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%r15, %rsi
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6712StringSearchC2ERKNS_13UnicodeStringERNS_17CharacterIteratorEPNS_17RuleBasedCollatorEPNS_13BreakIteratorER10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L142:
	.cfi_restore_state
	movl	$1, 0(%r13)
.L141:
	movq	$0, 152(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2499:
	.size	_ZN6icu_6712StringSearchC2ERKNS_13UnicodeStringERNS_17CharacterIteratorEPNS_17RuleBasedCollatorEPNS_13BreakIteratorER10UErrorCode, .-_ZN6icu_6712StringSearchC2ERKNS_13UnicodeStringERNS_17CharacterIteratorEPNS_17RuleBasedCollatorEPNS_13BreakIteratorER10UErrorCode
	.globl	_ZN6icu_6712StringSearchC1ERKNS_13UnicodeStringERNS_17CharacterIteratorEPNS_17RuleBasedCollatorEPNS_13BreakIteratorER10UErrorCode
	.set	_ZN6icu_6712StringSearchC1ERKNS_13UnicodeStringERNS_17CharacterIteratorEPNS_17RuleBasedCollatorEPNS_13BreakIteratorER10UErrorCode,_ZN6icu_6712StringSearchC2ERKNS_13UnicodeStringERNS_17CharacterIteratorEPNS_17RuleBasedCollatorEPNS_13BreakIteratorER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712StringSearchC2ERKS0_
	.type	_ZN6icu_6712StringSearchC2ERKS0_, @function
_ZN6icu_6712StringSearchC2ERKS0_:
.LFB2502:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	addq	$24, %rsi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	-8(%rsi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6714SearchIteratorC2ERKNS_13UnicodeStringEPNS_13BreakIteratorE@PLT
	leaq	16+_ZTVN6icu_6712StringSearchE(%rip), %rax
	leaq	88(%r12), %rsi
	movq	%rax, (%rbx)
	leaq	88(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	8(%rbx), %rdi
	movl	$0, -28(%rbp)
	call	uprv_free_67@PLT
	movq	152(%r12), %rax
	movq	$0, 8(%rbx)
	testq	%rax, %rax
	je	.L158
	movq	4152(%rax), %r8
	movzwl	32(%rbx), %eax
	movq	16(%r12), %r9
	testw	%ax, %ax
	js	.L146
	movswl	%ax, %ecx
	sarl	$5, %ecx
	testb	$17, %al
	jne	.L155
.L160:
	leaq	34(%rbx), %rdx
	testb	$2, %al
	jne	.L148
	movq	48(%rbx), %rdx
.L148:
	movzwl	96(%rbx), %eax
	testw	%ax, %ax
	js	.L150
.L161:
	movswl	%ax, %esi
	sarl	$5, %esi
	testb	$17, %al
	jne	.L156
.L162:
	leaq	98(%rbx), %rdi
	testb	$2, %al
	jne	.L152
	movq	112(%rbx), %rdi
.L152:
	subq	$8, %rsp
	leaq	-28(%rbp), %rax
	pushq	%rax
	call	usearch_openFromCollator_67@PLT
	movl	-28(%rbp), %esi
	popq	%rdx
	movq	%rax, 152(%rbx)
	popq	%rcx
	testl	%esi, %esi
	jg	.L143
	movq	(%rax), %rax
	movq	%rax, 8(%rbx)
.L143:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L159
	leaq	-16(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L146:
	.cfi_restore_state
	movl	36(%rbx), %ecx
	testb	$17, %al
	je	.L160
.L155:
	movzwl	96(%rbx), %eax
	xorl	%edx, %edx
	testw	%ax, %ax
	jns	.L161
	.p2align 4,,10
	.p2align 3
.L150:
	movl	100(%rbx), %esi
	testb	$17, %al
	je	.L162
.L156:
	xorl	%edi, %edi
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L158:
	movq	$0, 152(%rbx)
	jmp	.L143
.L159:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2502:
	.size	_ZN6icu_6712StringSearchC2ERKS0_, .-_ZN6icu_6712StringSearchC2ERKS0_
	.globl	_ZN6icu_6712StringSearchC1ERKS0_
	.set	_ZN6icu_6712StringSearchC1ERKS0_,_ZN6icu_6712StringSearchC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712StringSearch5cloneEv
	.type	_ZNK6icu_6712StringSearch5cloneEv, @function
_ZNK6icu_6712StringSearch5cloneEv:
.LFB2508:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	$160, %edi
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L163
	movq	16(%rbx), %rdx
	movq	%rax, %rdi
	leaq	24(%rbx), %rsi
	call	_ZN6icu_6714SearchIteratorC2ERKNS_13UnicodeStringEPNS_13BreakIteratorE@PLT
	leaq	16+_ZTVN6icu_6712StringSearchE(%rip), %rax
	leaq	88(%rbx), %rsi
	movq	%rax, (%r12)
	leaq	88(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	8(%r12), %rdi
	movl	$0, -28(%rbp)
	call	uprv_free_67@PLT
	movq	152(%rbx), %rax
	movq	$0, 8(%r12)
	testq	%rax, %rax
	je	.L182
	movq	4152(%rax), %r8
	movzwl	32(%r12), %eax
	movq	16(%rbx), %r9
	testw	%ax, %ax
	js	.L167
	movswl	%ax, %ecx
	sarl	$5, %ecx
	testb	$17, %al
	jne	.L176
.L184:
	leaq	34(%r12), %rdx
	testb	$2, %al
	jne	.L169
	movq	48(%r12), %rdx
.L169:
	movzwl	96(%r12), %eax
	testw	%ax, %ax
	js	.L171
.L185:
	movswl	%ax, %esi
	sarl	$5, %esi
	testb	$17, %al
	jne	.L177
.L186:
	leaq	98(%r12), %rdi
	testb	$2, %al
	jne	.L173
	movq	112(%r12), %rdi
.L173:
	subq	$8, %rsp
	leaq	-28(%rbp), %rax
	pushq	%rax
	call	usearch_openFromCollator_67@PLT
	movl	-28(%rbp), %esi
	movq	%rax, 152(%r12)
	popq	%rdx
	popq	%rcx
	testl	%esi, %esi
	jg	.L163
	movq	(%rax), %rax
	movq	%rax, 8(%r12)
.L163:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L183
	leaq	-16(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L167:
	.cfi_restore_state
	movl	36(%r12), %ecx
	testb	$17, %al
	je	.L184
.L176:
	movzwl	96(%r12), %eax
	xorl	%edx, %edx
	testw	%ax, %ax
	jns	.L185
	.p2align 4,,10
	.p2align 3
.L171:
	movl	100(%r12), %esi
	testb	$17, %al
	je	.L186
.L177:
	xorl	%edi, %edi
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L182:
	movq	$0, 152(%r12)
	jmp	.L163
.L183:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2508:
	.size	_ZNK6icu_6712StringSearch5cloneEv, .-_ZNK6icu_6712StringSearch5cloneEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712StringSearch11getCollatorEv
	.type	_ZNK6icu_6712StringSearch11getCollatorEv, @function
_ZNK6icu_6712StringSearch11getCollatorEv:
.LFB2515:
	.cfi_startproc
	endbr64
	movq	152(%rdi), %rax
	movq	4152(%rax), %rdi
	testq	%rdi, %rdi
	je	.L187
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6717RuleBasedCollatorE(%rip), %rdx
	leaq	_ZTIN6icu_678CollatorE(%rip), %rsi
	jmp	__dynamic_cast@PLT
	.p2align 4,,10
	.p2align 3
.L187:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2515:
	.size	_ZNK6icu_6712StringSearch11getCollatorEv, .-_ZNK6icu_6712StringSearch11getCollatorEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712StringSearch11setCollatorEPNS_17RuleBasedCollatorER10UErrorCode
	.type	_ZN6icu_6712StringSearch11setCollatorEPNS_17RuleBasedCollatorER10UErrorCode, @function
_ZN6icu_6712StringSearch11setCollatorEPNS_17RuleBasedCollatorER10UErrorCode:
.LFB2516:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jle	.L191
	ret
	.p2align 4,,10
	.p2align 3
.L191:
	movq	152(%rdi), %rdi
	jmp	usearch_setCollator_67@PLT
	.cfi_endproc
.LFE2516:
	.size	_ZN6icu_6712StringSearch11setCollatorEPNS_17RuleBasedCollatorER10UErrorCode, .-_ZN6icu_6712StringSearch11setCollatorEPNS_17RuleBasedCollatorER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712StringSearch10setPatternERKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6712StringSearch10setPatternERKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6712StringSearch10setPatternERKNS_13UnicodeStringER10UErrorCode:
.LFB2517:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jle	.L202
	ret
	.p2align 4,,10
	.p2align 3
.L202:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	leaq	88(%rdi), %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movzwl	96(%rbx), %eax
	testw	%ax, %ax
	js	.L194
	movswl	%ax, %edx
	sarl	$5, %edx
.L195:
	testb	$17, %al
	jne	.L198
	leaq	98(%rbx), %rsi
	testb	$2, %al
	jne	.L196
	movq	112(%rbx), %rsi
.L196:
	movq	152(%rbx), %rdi
	movq	%r12, %rcx
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	usearch_setPattern_67@PLT
	.p2align 4,,10
	.p2align 3
.L194:
	.cfi_restore_state
	movl	100(%rbx), %edx
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L198:
	xorl	%esi, %esi
	jmp	.L196
	.cfi_endproc
.LFE2517:
	.size	_ZN6icu_6712StringSearch10setPatternERKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6712StringSearch10setPatternERKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712StringSearch10getPatternEv
	.type	_ZNK6icu_6712StringSearch10getPatternEv, @function
_ZNK6icu_6712StringSearch10getPatternEv:
.LFB2518:
	.cfi_startproc
	endbr64
	leaq	88(%rdi), %rax
	ret
	.cfi_endproc
.LFE2518:
	.size	_ZNK6icu_6712StringSearch10getPatternEv, .-_ZNK6icu_6712StringSearch10getPatternEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712StringSearchD2Ev
	.type	_ZN6icu_6712StringSearchD2Ev, @function
_ZN6icu_6712StringSearchD2Ev:
.LFB2505:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6712StringSearchE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	152(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L205
	call	usearch_close_67@PLT
	movq	$0, 8(%r12)
.L205:
	leaq	88(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6714SearchIteratorD2Ev@PLT
	.cfi_endproc
.LFE2505:
	.size	_ZN6icu_6712StringSearchD2Ev, .-_ZN6icu_6712StringSearchD2Ev
	.globl	_ZN6icu_6712StringSearchD1Ev
	.set	_ZN6icu_6712StringSearchD1Ev,_ZN6icu_6712StringSearchD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712StringSearchD0Ev
	.type	_ZN6icu_6712StringSearchD0Ev, @function
_ZN6icu_6712StringSearchD0Ev:
.LFB2507:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6712StringSearchE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	152(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L211
	call	usearch_close_67@PLT
	movq	$0, 8(%r12)
.L211:
	leaq	88(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6714SearchIteratorD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2507:
	.size	_ZN6icu_6712StringSearchD0Ev, .-_ZN6icu_6712StringSearchD0Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712StringSearcheqERKNS_14SearchIteratorE
	.type	_ZNK6icu_6712StringSearcheqERKNS_14SearchIteratorE, @function
_ZNK6icu_6712StringSearcheqERKNS_14SearchIteratorE:
.LFB2510:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	cmpq	%rsi, %rdi
	je	.L236
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_ZNK6icu_6714SearchIteratoreqERKS0_@PLT
	testb	%al, %al
	jne	.L239
.L216:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L239:
	.cfi_restore_state
	movswl	96(%r12), %edx
	movswl	96(%rbx), %ecx
	movl	%edx, %eax
	movl	%ecx, %esi
	andl	$1, %esi
	andl	$1, %eax
	jne	.L218
	testw	%dx, %dx
	js	.L219
	sarl	$5, %edx
.L220:
	testw	%cx, %cx
	js	.L221
	sarl	$5, %ecx
.L222:
	testb	%sil, %sil
	jne	.L216
	cmpl	%edx, %ecx
	jne	.L216
	leaq	88(%rbx), %rsi
	leaq	88(%r12), %rdi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%sil
	.p2align 4,,10
	.p2align 3
.L218:
	testb	%sil, %sil
	je	.L225
	movq	152(%r12), %rdx
	movq	152(%rbx), %rax
	popq	%rbx
	popq	%r12
	movq	4152(%rax), %rax
	cmpq	%rax, 4152(%rdx)
	sete	%al
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L236:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.p2align 4,,10
	.p2align 3
.L221:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movl	100(%rbx), %ecx
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L219:
	movl	100(%r12), %edx
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L225:
	xorl	%eax, %eax
	jmp	.L216
	.cfi_endproc
.LFE2510:
	.size	_ZNK6icu_6712StringSearcheqERKNS_14SearchIteratorE, .-_ZNK6icu_6712StringSearcheqERKNS_14SearchIteratorE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712StringSearchaSERKS0_
	.type	_ZN6icu_6712StringSearchaSERKS0_, @function
_ZN6icu_6712StringSearchaSERKS0_:
.LFB2509:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpq	%rdi, %rsi
	je	.L242
	movq	%rsi, %rbx
	leaq	88(%r12), %r13
	call	_ZNK6icu_6714SearchIteratoreqERKS0_@PLT
	leaq	88(%rbx), %r14
	testb	%al, %al
	jne	.L243
.L251:
	leaq	24(%rbx), %rsi
	leaq	24(%r12), %rdi
	movl	$0, -44(%rbp)
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	16(%rbx), %rax
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, 16(%r12)
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	152(%r12), %rdi
	call	usearch_close_67@PLT
	movswl	32(%r12), %ecx
	movl	%ecx, %eax
	sarl	$5, %ecx
	testw	%ax, %ax
	js	.L278
.L253:
	testb	$17, %al
	jne	.L262
	leaq	34(%r12), %rdx
	testb	$2, %al
	jne	.L254
	movq	48(%r12), %rdx
.L254:
	movzwl	96(%r12), %eax
	testw	%ax, %ax
	js	.L256
	movswl	%ax, %esi
	sarl	$5, %esi
.L257:
	testb	$17, %al
	jne	.L263
	leaq	98(%r12), %rdi
	testb	$2, %al
	jne	.L258
	movq	112(%r12), %rdi
.L258:
	movq	152(%rbx), %rax
	subq	$8, %rsp
	xorl	%r9d, %r9d
	movq	4152(%rax), %r8
	leaq	-44(%rbp), %rax
	pushq	%rax
	call	usearch_openFromCollator_67@PLT
	popq	%rdx
	popq	%rcx
	movq	%rax, 152(%r12)
	testq	%rax, %rax
	je	.L242
	movq	(%rax), %rax
	movq	%rax, 8(%r12)
.L242:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L279
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L243:
	.cfi_restore_state
	movswl	96(%rbx), %eax
	movswl	96(%r12), %edx
	movl	%eax, %ecx
	andl	$1, %ecx
	testb	$1, %dl
	jne	.L246
	testw	%dx, %dx
	js	.L247
	sarl	$5, %edx
.L248:
	testw	%ax, %ax
	js	.L249
	sarl	$5, %eax
.L250:
	testb	%cl, %cl
	jne	.L251
	cmpl	%edx, %eax
	jne	.L251
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%cl
	.p2align 4,,10
	.p2align 3
.L246:
	testb	%cl, %cl
	je	.L251
	movq	152(%rbx), %rax
	movq	152(%r12), %rdx
	movq	4152(%rax), %rax
	cmpq	%rax, 4152(%rdx)
	jne	.L251
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L278:
	movl	36(%r12), %ecx
	jmp	.L253
	.p2align 4,,10
	.p2align 3
.L256:
	movl	100(%r12), %esi
	jmp	.L257
	.p2align 4,,10
	.p2align 3
.L263:
	xorl	%edi, %edi
	jmp	.L258
	.p2align 4,,10
	.p2align 3
.L262:
	xorl	%edx, %edx
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L249:
	movl	100(%rbx), %eax
	jmp	.L250
	.p2align 4,,10
	.p2align 3
.L247:
	movl	100(%r12), %edx
	jmp	.L248
.L279:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2509:
	.size	_ZN6icu_6712StringSearchaSERKS0_, .-_ZN6icu_6712StringSearchaSERKS0_
	.weak	_ZTSN6icu_6712StringSearchE
	.section	.rodata._ZTSN6icu_6712StringSearchE,"aG",@progbits,_ZTSN6icu_6712StringSearchE,comdat
	.align 16
	.type	_ZTSN6icu_6712StringSearchE, @object
	.size	_ZTSN6icu_6712StringSearchE, 24
_ZTSN6icu_6712StringSearchE:
	.string	"N6icu_6712StringSearchE"
	.weak	_ZTIN6icu_6712StringSearchE
	.section	.data.rel.ro._ZTIN6icu_6712StringSearchE,"awG",@progbits,_ZTIN6icu_6712StringSearchE,comdat
	.align 8
	.type	_ZTIN6icu_6712StringSearchE, @object
	.size	_ZTIN6icu_6712StringSearchE, 24
_ZTIN6icu_6712StringSearchE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6712StringSearchE
	.quad	_ZTIN6icu_6714SearchIteratorE
	.weak	_ZTVN6icu_6712StringSearchE
	.section	.data.rel.ro._ZTVN6icu_6712StringSearchE,"awG",@progbits,_ZTVN6icu_6712StringSearchE,comdat
	.align 8
	.type	_ZTVN6icu_6712StringSearchE, @object
	.size	_ZTVN6icu_6712StringSearchE, 128
_ZTVN6icu_6712StringSearchE:
	.quad	0
	.quad	_ZTIN6icu_6712StringSearchE
	.quad	_ZN6icu_6712StringSearchD1Ev
	.quad	_ZN6icu_6712StringSearchD0Ev
	.quad	_ZNK6icu_6712StringSearch17getDynamicClassIDEv
	.quad	_ZN6icu_6712StringSearch9setOffsetEiR10UErrorCode
	.quad	_ZNK6icu_6712StringSearch9getOffsetEv
	.quad	_ZN6icu_6712StringSearch7setTextERKNS_13UnicodeStringER10UErrorCode
	.quad	_ZN6icu_6712StringSearch7setTextERNS_17CharacterIteratorER10UErrorCode
	.quad	_ZNK6icu_6712StringSearcheqERKNS_14SearchIteratorE
	.quad	_ZNK6icu_6712StringSearch9safeCloneEv
	.quad	_ZN6icu_6712StringSearch5resetEv
	.quad	_ZN6icu_6712StringSearch10handleNextEiR10UErrorCode
	.quad	_ZN6icu_6712StringSearch10handlePrevEiR10UErrorCode
	.quad	_ZN6icu_6714SearchIterator14setMatchLengthEi
	.quad	_ZN6icu_6714SearchIterator13setMatchStartEi
	.local	_ZZN6icu_6712StringSearch16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6712StringSearch16getStaticClassIDEvE7classID,1,1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
