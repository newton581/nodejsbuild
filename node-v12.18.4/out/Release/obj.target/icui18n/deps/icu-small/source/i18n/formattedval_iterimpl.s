	.file	"formattedval_iterimpl.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6739FormattedValueFieldPositionIteratorImpl8appendToERNS_10AppendableER10UErrorCode
	.type	_ZNK6icu_6739FormattedValueFieldPositionIteratorImpl8appendToERNS_10AppendableER10UErrorCode, @function
_ZNK6icu_6739FormattedValueFieldPositionIteratorImpl8appendToERNS_10AppendableER10UErrorCode:
.LFB2431:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$8, %rsp
	movq	(%rsi), %rax
	movq	40(%rax), %rcx
	movzwl	16(%rdi), %eax
	testw	%ax, %ax
	js	.L2
	movswl	%ax, %edx
	sarl	$5, %edx
	testb	$17, %al
	jne	.L6
.L9:
	leaq	18(%rdi), %rsi
	testb	$2, %al
	jne	.L4
	movq	32(%rdi), %rsi
.L4:
	movq	%r12, %rdi
	call	*%rcx
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	movl	20(%rdi), %edx
	testb	$17, %al
	je	.L9
.L6:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	*%rcx
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2431:
	.size	_ZNK6icu_6739FormattedValueFieldPositionIteratorImpl8appendToERNS_10AppendableER10UErrorCode, .-_ZNK6icu_6739FormattedValueFieldPositionIteratorImpl8appendToERNS_10AppendableER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6739FormattedValueFieldPositionIteratorImpl8toStringER10UErrorCode
	.type	_ZNK6icu_6739FormattedValueFieldPositionIteratorImpl8toStringER10UErrorCode, @function
_ZNK6icu_6739FormattedValueFieldPositionIteratorImpl8toStringER10UErrorCode:
.LFB2429:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$8, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2429:
	.size	_ZNK6icu_6739FormattedValueFieldPositionIteratorImpl8toStringER10UErrorCode, .-_ZNK6icu_6739FormattedValueFieldPositionIteratorImpl8toStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6739FormattedValueFieldPositionIteratorImpl12toTempStringER10UErrorCode
	.type	_ZNK6icu_6739FormattedValueFieldPositionIteratorImpl12toTempStringER10UErrorCode, @function
_ZNK6icu_6739FormattedValueFieldPositionIteratorImpl12toTempStringER10UErrorCode:
.LFB2430:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movzwl	16(%rsi), %eax
	testw	%ax, %ax
	js	.L13
	movswl	%ax, %ecx
	sarl	$5, %ecx
	testb	$17, %al
	jne	.L18
.L22:
	testb	$2, %al
	jne	.L20
	movq	32(%rsi), %rsi
.L15:
	movq	%rsi, -32(%rbp)
	leaq	-32(%rbp), %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L21
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	addq	$18, %rsi
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L13:
	movl	20(%rsi), %ecx
	testb	$17, %al
	je	.L22
.L18:
	xorl	%esi, %esi
	jmp	.L15
.L21:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2430:
	.size	_ZNK6icu_6739FormattedValueFieldPositionIteratorImpl12toTempStringER10UErrorCode, .-_ZNK6icu_6739FormattedValueFieldPositionIteratorImpl12toTempStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6739FormattedValueFieldPositionIteratorImpl12nextPositionERNS_24ConstrainedFieldPositionER10UErrorCode
	.type	_ZNK6icu_6739FormattedValueFieldPositionIteratorImpl12nextPositionERNS_24ConstrainedFieldPositionER10UErrorCode, @function
_ZNK6icu_6739FormattedValueFieldPositionIteratorImpl12nextPositionERNS_24ConstrainedFieldPositionER10UErrorCode:
.LFB2432:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	80(%rdi), %eax
	movq	(%rsi), %rbx
	movq	%rdi, -72(%rbp)
	testl	%eax, %eax
	leal	3(%rax), %r12d
	movl	%ebx, %r14d
	cmovns	%eax, %r12d
	sarl	$2, %r12d
	movl	%r12d, -60(%rbp)
	cmpl	%ebx, %r12d
	jle	.L24
	sall	$2, %ebx
	movslq	%ebx, %rcx
	salq	$2, %rcx
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L52:
	movq	-72(%rbp), %rdi
	movl	80(%rdi), %eax
	testl	%eax, %eax
	jle	.L39
	movl	%eax, %esi
	xorl	%r15d, %r15d
	subl	%ebx, %esi
	testl	%esi, %esi
	jle	.L36
	movq	96(%rdi), %rsi
	movl	(%rsi,%rcx), %r15d
.L36:
	subl	%edx, %eax
	xorl	%r12d, %r12d
	testl	%eax, %eax
	jle	.L26
	movq	-72(%rbp), %rax
	movq	96(%rax), %rax
	movl	4(%rax,%rcx), %r12d
.L26:
	movl	%r12d, %edx
	movl	%r15d, %esi
	movq	%r13, %rdi
	movq	%rcx, -56(%rbp)
	call	_ZNK6icu_6724ConstrainedFieldPosition12matchesFieldEii@PLT
	movq	-56(%rbp), %rcx
	testb	%al, %al
	jne	.L50
	addl	$1, %r14d
	addl	$4, %ebx
	addq	$16, %rcx
	cmpl	%r14d, -60(%rbp)
	je	.L51
.L33:
	leal	1(%rbx), %edx
	testl	%ebx, %ebx
	jns	.L52
	testl	%edx, %edx
	jns	.L53
.L39:
	xorl	%r15d, %r15d
	xorl	%r12d, %r12d
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L50:
	leal	3(%rbx), %ecx
	addl	$2, %ebx
	js	.L29
	movq	-72(%rbp), %rdx
	movl	80(%rdx), %eax
	testl	%eax, %eax
	jle	.L44
	movl	%eax, %esi
	xorl	%r9d, %r9d
	subl	%ebx, %esi
	testl	%esi, %esi
	jle	.L31
	movq	96(%rdx), %rsi
	movslq	%ebx, %rbx
	movl	(%rsi,%rbx,4), %r9d
.L31:
	testl	%ecx, %ecx
	js	.L45
.L35:
	subl	%ecx, %eax
	xorl	%r8d, %r8d
	testl	%eax, %eax
	jle	.L30
	movq	-72(%rbp), %rax
	movslq	%ecx, %rcx
	movq	96(%rax), %rax
	movl	(%rax,%rcx,4), %r8d
.L30:
	movl	%r9d, %ecx
	movl	%r12d, %edx
	movl	%r15d, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6724ConstrainedFieldPosition8setStateEiiii@PLT
.L24:
	cmpl	-60(%rbp), %r14d
	je	.L32
	leal	1(%r14), %esi
	movslq	%esi, %rsi
.L34:
	movq	%r13, %rdi
	call	_ZN6icu_6724ConstrainedFieldPosition24setInt64IterationContextEl@PLT
	cmpl	%r14d, -60(%rbp)
	setg	%al
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L51:
	.cfi_restore_state
	movl	-60(%rbp), %r14d
.L32:
	movslq	-60(%rbp), %rsi
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L44:
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L29:
	testl	%ecx, %ecx
	js	.L44
	movq	-72(%rbp), %rax
	xorl	%r9d, %r9d
	movl	80(%rax), %eax
	testl	%eax, %eax
	jg	.L35
.L45:
	xorl	%r8d, %r8d
	jmp	.L30
.L53:
	movq	-72(%rbp), %rax
	xorl	%r15d, %r15d
	xorl	%r12d, %r12d
	movl	80(%rax), %eax
	testl	%eax, %eax
	jle	.L26
	jmp	.L36
	.cfi_endproc
.LFE2432:
	.size	_ZNK6icu_6739FormattedValueFieldPositionIteratorImpl12nextPositionERNS_24ConstrainedFieldPositionER10UErrorCode, .-_ZNK6icu_6739FormattedValueFieldPositionIteratorImpl12nextPositionERNS_24ConstrainedFieldPositionER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6739FormattedValueFieldPositionIteratorImplC2EiR10UErrorCode
	.type	_ZN6icu_6739FormattedValueFieldPositionIteratorImplC2EiR10UErrorCode, @function
_ZN6icu_6739FormattedValueFieldPositionIteratorImplC2EiR10UErrorCode:
.LFB2423:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	leaq	16+_ZTVN6icu_6739FormattedValueFieldPositionIteratorImplE(%rip), %rcx
	sall	$2, %esi
	addq	$72, %rdi
	movq	%rax, %xmm1
	movl	$2, %eax
	movq	%rcx, %xmm0
	movw	%ax, -56(%rdi)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -72(%rdi)
	jmp	_ZN6icu_679UVector32C1EiR10UErrorCode@PLT
	.cfi_endproc
.LFE2423:
	.size	_ZN6icu_6739FormattedValueFieldPositionIteratorImplC2EiR10UErrorCode, .-_ZN6icu_6739FormattedValueFieldPositionIteratorImplC2EiR10UErrorCode
	.globl	_ZN6icu_6739FormattedValueFieldPositionIteratorImplC1EiR10UErrorCode
	.set	_ZN6icu_6739FormattedValueFieldPositionIteratorImplC1EiR10UErrorCode,_ZN6icu_6739FormattedValueFieldPositionIteratorImplC2EiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6739FormattedValueFieldPositionIteratorImpl10getHandlerER10UErrorCode
	.type	_ZN6icu_6739FormattedValueFieldPositionIteratorImpl10getHandlerER10UErrorCode, @function
_ZN6icu_6739FormattedValueFieldPositionIteratorImpl10getHandlerER10UErrorCode:
.LFB2433:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$72, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN6icu_6728FieldPositionIteratorHandlerC1EPNS_9UVector32ER10UErrorCode@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2433:
	.size	_ZN6icu_6739FormattedValueFieldPositionIteratorImpl10getHandlerER10UErrorCode, .-_ZN6icu_6739FormattedValueFieldPositionIteratorImpl10getHandlerER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6739FormattedValueFieldPositionIteratorImpl12appendStringENS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6739FormattedValueFieldPositionIteratorImpl12appendStringENS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6739FormattedValueFieldPositionIteratorImpl12appendStringENS_13UnicodeStringER10UErrorCode:
.LFB2440:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movswl	8(%rsi), %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	8(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdx, %rbx
	testw	%cx, %cx
	js	.L60
	sarl	$5, %ecx
.L61:
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString19getTerminatedBufferEv@PLT
	testq	%rax, %rax
	je	.L67
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L60:
	.cfi_restore_state
	movl	12(%rsi), %ecx
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L64:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movl	$7, (%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2440:
	.size	_ZN6icu_6739FormattedValueFieldPositionIteratorImpl12appendStringENS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6739FormattedValueFieldPositionIteratorImpl12appendStringENS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6739FormattedValueFieldPositionIteratorImpl15addOverlapSpansE14UFieldCategoryaR10UErrorCode
	.type	_ZN6icu_6739FormattedValueFieldPositionIteratorImpl15addOverlapSpansE14UFieldCategoryaR10UErrorCode, @function
_ZN6icu_6739FormattedValueFieldPositionIteratorImpl15addOverlapSpansE14UFieldCategoryaR10UErrorCode:
.LFB2441:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, -84(%rbp)
	movslq	80(%rdi), %rdx
	movl	%esi, -72(%rbp)
	testl	%edx, %edx
	leal	3(%rdx), %eax
	movq	%rcx, -80(%rbp)
	cmovns	%edx, %eax
	sarl	$2, %eax
	movl	%eax, -64(%rbp)
	cmpl	$3, %edx
	jle	.L68
	movl	$20, %r15d
	sall	$2, %eax
	xorl	%ebx, %ebx
	movl	$0, -52(%rbp)
	movq	%r15, %r14
	movl	%eax, -68(%rbp)
	movq	%rdi, %r15
	movl	$2147483647, %r12d
	movl	$2147483647, -56(%rbp)
	movl	$0, -60(%rbp)
	testl	%edx, %edx
	jle	.L72
	.p2align 4,,10
	.p2align 3
.L132:
	leal	1(,%rbx,4), %eax
	movl	%edx, %ecx
	xorl	%r9d, %r9d
	subl	%eax, %ecx
	testl	%ecx, %ecx
	jle	.L73
	movq	96(%r15), %rax
	movl	-16(%rax,%r14), %r9d
.L73:
	addl	$1, %ebx
	cmpl	-64(%rbp), %ebx
	jge	.L131
	leal	0(,%rbx,4), %eax
	movq	%r14, %r8
	leal	-1(%rdx), %r10d
.L78:
	movl	%r10d, %esi
	movl	%eax, %r13d
	xorl	%ecx, %ecx
	subl	%eax, %esi
	testl	%esi, %esi
	jle	.L75
	movq	96(%r15), %rcx
	movl	(%rcx,%r8), %ecx
.L75:
	cmpl	%r9d, %ecx
	je	.L76
	addl	$4, %eax
	addq	$16, %r8
	cmpl	%eax, -68(%rbp)
	jne	.L78
	addq	$16, %r14
	.p2align 4,,10
	.p2align 3
.L133:
	testl	%edx, %edx
	jg	.L132
.L72:
	addl	$1, %ebx
	cmpl	%ebx, -64(%rbp)
	jle	.L131
	leal	0(,%rbx,4), %r13d
	xorl	%esi, %esi
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L76:
	leal	-2(,%rbx,4), %eax
	xorl	%esi, %esi
	subl	%eax, %edx
	testl	%edx, %edx
	jle	.L74
	movq	96(%r15), %rax
	movl	-12(%rax,%r14), %esi
.L74:
	movl	%r12d, %edi
	call	uprv_min_67@PLT
	xorl	%esi, %esi
	movl	%eax, %r12d
	movl	80(%r15), %eax
	testl	%eax, %eax
	jle	.L79
	leal	-1(,%rbx,4), %edx
	subl	%edx, %eax
	testl	%eax, %eax
	jle	.L79
	movq	96(%r15), %rax
	movl	-8(%rax,%r14), %esi
.L79:
	movl	-60(%rbp), %edi
	call	uprv_max_67@PLT
	xorl	%esi, %esi
	movl	%eax, -60(%rbp)
	movl	80(%r15), %eax
	testl	%eax, %eax
	jle	.L80
	leal	2(%r13), %edx
	xorl	%esi, %esi
	subl	%edx, %eax
	testl	%eax, %eax
	jle	.L80
	movq	96(%r15), %rax
	movslq	%edx, %rdx
	movl	(%rax,%rdx,4), %esi
.L80:
	movl	-56(%rbp), %edi
	call	uprv_min_67@PLT
	xorl	%esi, %esi
	movl	%eax, -56(%rbp)
	movl	80(%r15), %eax
	testl	%eax, %eax
	jle	.L81
	addl	$3, %r13d
	xorl	%esi, %esi
	subl	%r13d, %eax
	testl	%eax, %eax
	jle	.L81
	movq	96(%r15), %rax
	movslq	%r13d, %r13
	movl	(%rax,%r13,4), %esi
.L81:
	movl	-52(%rbp), %edi
	addq	$16, %r14
	call	uprv_max_67@PLT
	movslq	80(%r15), %rdx
	movl	%eax, -52(%rbp)
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L131:
	movq	%r15, %r14
	cmpl	$2147483647, %r12d
	je	.L68
	movl	%edx, %esi
	leaq	72(%r14), %r13
	addl	$1, %esi
	js	.L82
	cmpl	84(%r14), %esi
	jle	.L83
.L82:
	movq	-80(%rbp), %rdx
	movq	%r13, %rdi
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	je	.L134
	movslq	80(%r14), %rdx
.L83:
	movq	96(%r14), %rax
	movl	-72(%rbp), %edi
	movl	%edi, (%rax,%rdx,4)
	movl	80(%r14), %eax
	addl	$1, %eax
	movl	%eax, 80(%r14)
.L85:
	movl	%eax, %esi
	movsbl	-84(%rbp), %ebx
	addl	$1, %esi
	js	.L86
	cmpl	84(%r14), %esi
	jle	.L87
.L86:
	movq	-80(%rbp), %rdx
	movq	%r13, %rdi
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	movl	80(%r14), %eax
	je	.L89
.L87:
	movq	96(%r14), %rdx
	cltq
	movl	%ebx, (%rdx,%rax,4)
	movl	80(%r14), %eax
	addl	$1, %eax
	movl	%eax, 80(%r14)
.L89:
	movl	%eax, %esi
	addl	$1, %esi
	js	.L90
	cmpl	84(%r14), %esi
	jle	.L91
.L90:
	movq	-80(%rbp), %rdx
	movq	%r13, %rdi
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	movl	80(%r14), %eax
	je	.L93
.L91:
	movq	96(%r14), %rdx
	cltq
	movl	%r12d, (%rdx,%rax,4)
	movl	80(%r14), %eax
	addl	$1, %eax
	movl	%eax, 80(%r14)
.L93:
	movl	%eax, %esi
	addl	$1, %esi
	js	.L94
	cmpl	84(%r14), %esi
	jle	.L95
.L94:
	movq	-80(%rbp), %rdx
	movq	%r13, %rdi
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	movl	80(%r14), %eax
	je	.L97
.L95:
	movq	96(%r14), %rdx
	movl	-60(%rbp), %edi
	cltq
	movl	%edi, (%rdx,%rax,4)
	movl	80(%r14), %eax
	addl	$1, %eax
	movl	%eax, 80(%r14)
.L97:
	movl	%eax, %esi
	addl	$1, %esi
	js	.L98
	cmpl	84(%r14), %esi
	jle	.L99
.L98:
	movq	-80(%rbp), %rdx
	movq	%r13, %rdi
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	movl	80(%r14), %eax
	je	.L101
.L99:
	movq	96(%r14), %rdx
	movl	-72(%rbp), %edi
	cltq
	movl	%edi, (%rdx,%rax,4)
	movl	80(%r14), %eax
	addl	$1, %eax
	movl	%eax, 80(%r14)
.L101:
	movl	%eax, %esi
	addl	$1, %esi
	js	.L102
	cmpl	84(%r14), %esi
	jle	.L103
.L102:
	movq	-80(%rbp), %rdx
	movq	%r13, %rdi
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	movl	80(%r14), %eax
	je	.L105
.L103:
	movq	96(%r14), %rcx
	movl	$1, %edx
	cltq
	subl	%ebx, %edx
	movl	%edx, (%rcx,%rax,4)
	movl	80(%r14), %eax
	addl	$1, %eax
	movl	%eax, 80(%r14)
.L105:
	movl	%eax, %esi
	addl	$1, %esi
	js	.L106
	cmpl	84(%r14), %esi
	jle	.L107
.L106:
	movq	-80(%rbp), %rdx
	movq	%r13, %rdi
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	movl	80(%r14), %eax
	je	.L109
.L107:
	movq	96(%r14), %rdx
	movl	-56(%rbp), %edi
	cltq
	movl	%edi, (%rdx,%rax,4)
	movl	80(%r14), %eax
	addl	$1, %eax
	movl	%eax, 80(%r14)
.L109:
	movl	%eax, %esi
	addl	$1, %esi
	js	.L110
	cmpl	84(%r14), %esi
	jle	.L111
.L110:
	movq	-80(%rbp), %rdx
	movq	%r13, %rdi
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	je	.L68
	movl	80(%r14), %eax
.L111:
	movq	96(%r14), %rdx
	movl	-52(%rbp), %edi
	cltq
	movl	%edi, (%rdx,%rax,4)
	addl	$1, 80(%r14)
.L68:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L134:
	.cfi_restore_state
	movl	80(%r14), %eax
	jmp	.L85
	.cfi_endproc
.LFE2441:
	.size	_ZN6icu_6739FormattedValueFieldPositionIteratorImpl15addOverlapSpansE14UFieldCategoryaR10UErrorCode, .-_ZN6icu_6739FormattedValueFieldPositionIteratorImpl15addOverlapSpansE14UFieldCategoryaR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6739FormattedValueFieldPositionIteratorImpl4sortEv
	.type	_ZN6icu_6739FormattedValueFieldPositionIteratorImpl4sortEv, @function
_ZN6icu_6739FormattedValueFieldPositionIteratorImpl4sortEv:
.LFB2442:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	72(%rdi), %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	80(%rdi), %ebx
	testl	%ebx, %ebx
	leal	3(%rbx), %eax
	movl	%ebx, -108(%rbp)
	cmovns	%ebx, %eax
	movq	%r15, %rbx
	sarl	$2, %eax
	leal	-4(,%rax,4), %eax
	movl	%eax, -72(%rbp)
	.p2align 4,,10
	.p2align 3
.L151:
	cmpl	$7, -108(%rbp)
	jle	.L135
	movl	$28, %r11d
	movb	$1, -84(%rbp)
	xorl	%r14d, %r14d
	movq	%rcx, %r13
	movq	%r11, %r12
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L171:
	movl	%r8d, %edi
	subl	%r15d, %edi
	movslq	%edi, %rdi
.L147:
	testq	%rdi, %rdi
	js	.L164
.L138:
	addq	$16, %r12
	cmpl	-72(%rbp), %r14d
	je	.L165
.L136:
	leal	7(%r14), %esi
	movl	80(%r13), %eax
	movl	%r14d, %edx
	movl	%esi, -52(%rbp)
	leal	6(%r14), %esi
	leal	3(%rdx), %r11d
	movl	%esi, -56(%rbp)
	leal	5(%r14), %esi
	leal	2(%rdx), %r9d
	addl	$4, %r14d
	movl	%esi, -60(%rbp)
	leal	1(%rdx), %r10d
	testl	%eax, %eax
	jle	.L138
	movl	%eax, %ecx
	movl	$0, -64(%rbp)
	subl	%edx, %ecx
	testl	%ecx, %ecx
	jle	.L139
	movq	96(%r13), %rcx
	movl	-28(%rcx,%r12), %esi
	movl	%esi, -64(%rbp)
.L139:
	movl	%eax, %ecx
	subl	%r10d, %ecx
	testl	%ecx, %ecx
	jle	.L166
	movq	96(%r13), %rcx
	movl	-24(%rcx,%r12), %esi
	movl	%esi, -80(%rbp)
.L140:
	movl	%eax, %ecx
	xorl	%r15d, %r15d
	subl	%r9d, %ecx
	testl	%ecx, %ecx
	jle	.L141
	movq	96(%r13), %rcx
	movl	-20(%rcx,%r12), %r15d
.L141:
	movl	%eax, %ecx
	subl	%r11d, %ecx
	testl	%ecx, %ecx
	jle	.L167
	movq	96(%r13), %rcx
	movl	-16(%rcx,%r12), %esi
	movl	%esi, -68(%rbp)
.L142:
	movl	%eax, %ecx
	subl	%r14d, %ecx
	testl	%ecx, %ecx
	jle	.L168
	movq	96(%r13), %rcx
	movl	-12(%rcx,%r12), %esi
	movl	%esi, -76(%rbp)
.L143:
	movl	%eax, %ecx
	subl	-60(%rbp), %ecx
	testl	%ecx, %ecx
	jle	.L169
	movq	96(%r13), %rcx
	movl	-8(%rcx,%r12), %ecx
.L144:
	movl	%eax, %edi
	subl	-56(%rbp), %edi
	xorl	%r8d, %r8d
	testl	%edi, %edi
	jle	.L145
	movq	96(%r13), %rdi
	movl	-4(%rdi,%r12), %r8d
.L145:
	subl	-52(%rbp), %eax
	testl	%eax, %eax
	jle	.L170
	movq	96(%r13), %rax
	movl	(%rax,%r12), %eax
.L153:
	cmpl	%r15d, %r8d
	jne	.L171
	movl	-68(%rbp), %edi
	cmpl	%edi, %eax
	je	.L148
	subl	%eax, %edi
	movslq	%edi, %rdi
	testq	%rdi, %rdi
	jns	.L138
.L164:
	movl	-76(%rbp), %esi
	movq	%rbx, %rdi
	movl	%eax, -96(%rbp)
	addq	$16, %r12
	movl	%r11d, -84(%rbp)
	movl	%r8d, -100(%rbp)
	movl	%r9d, -88(%rbp)
	movl	%ecx, -104(%rbp)
	movl	%r10d, -92(%rbp)
	call	_ZN6icu_679UVector3212setElementAtEii@PLT
	movl	-92(%rbp), %r10d
	movl	-104(%rbp), %ecx
	movq	%rbx, %rdi
	movl	%r10d, %edx
	movl	%ecx, %esi
	call	_ZN6icu_679UVector3212setElementAtEii@PLT
	movl	-88(%rbp), %r9d
	movl	-100(%rbp), %r8d
	movq	%rbx, %rdi
	movl	%r9d, %edx
	movl	%r8d, %esi
	call	_ZN6icu_679UVector3212setElementAtEii@PLT
	movl	-84(%rbp), %r11d
	movl	-96(%rbp), %eax
	movq	%rbx, %rdi
	movl	%r11d, %edx
	movl	%eax, %esi
	call	_ZN6icu_679UVector3212setElementAtEii@PLT
	movl	-64(%rbp), %esi
	movl	%r14d, %edx
	movq	%rbx, %rdi
	call	_ZN6icu_679UVector3212setElementAtEii@PLT
	movl	-60(%rbp), %edx
	movl	-80(%rbp), %esi
	movq	%rbx, %rdi
	call	_ZN6icu_679UVector3212setElementAtEii@PLT
	movl	-56(%rbp), %edx
	movl	%r15d, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_679UVector3212setElementAtEii@PLT
	movl	-52(%rbp), %edx
	movl	-68(%rbp), %esi
	movq	%rbx, %rdi
	call	_ZN6icu_679UVector3212setElementAtEii@PLT
	movb	$0, -84(%rbp)
	cmpl	-72(%rbp), %r14d
	jne	.L136
.L165:
	cmpb	$0, -84(%rbp)
	movq	%r13, %rcx
	je	.L151
.L135:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L169:
	.cfi_restore_state
	xorl	%ecx, %ecx
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L168:
	movl	$0, -76(%rbp)
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L167:
	movl	$0, -68(%rbp)
	jmp	.L142
	.p2align 4,,10
	.p2align 3
.L166:
	movl	$0, -80(%rbp)
	jmp	.L140
	.p2align 4,,10
	.p2align 3
.L170:
	xorl	%eax, %eax
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L148:
	movl	-64(%rbp), %edi
	movl	-76(%rbp), %esi
	cmpl	%edi, %esi
	je	.L149
	subl	%esi, %edi
	movslq	%edi, %rdi
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L149:
	movl	-80(%rbp), %edi
	cmpl	%edi, %ecx
	je	.L138
	movl	%ecx, %esi
	subl	%edi, %esi
	movslq	%esi, %rdi
	jmp	.L147
	.cfi_endproc
.LFE2442:
	.size	_ZN6icu_6739FormattedValueFieldPositionIteratorImpl4sortEv, .-_ZN6icu_6739FormattedValueFieldPositionIteratorImpl4sortEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6739FormattedValueFieldPositionIteratorImplD2Ev
	.type	_ZN6icu_6739FormattedValueFieldPositionIteratorImplD2Ev, @function
_ZN6icu_6739FormattedValueFieldPositionIteratorImplD2Ev:
.LFB2426:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6739FormattedValueFieldPositionIteratorImplE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	72(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -72(%rdi)
	call	_ZN6icu_679UVector32D1Ev@PLT
	leaq	8(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6714FormattedValueD2Ev@PLT
	.cfi_endproc
.LFE2426:
	.size	_ZN6icu_6739FormattedValueFieldPositionIteratorImplD2Ev, .-_ZN6icu_6739FormattedValueFieldPositionIteratorImplD2Ev
	.globl	_ZN6icu_6739FormattedValueFieldPositionIteratorImplD1Ev
	.set	_ZN6icu_6739FormattedValueFieldPositionIteratorImplD1Ev,_ZN6icu_6739FormattedValueFieldPositionIteratorImplD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6739FormattedValueFieldPositionIteratorImplD0Ev
	.type	_ZN6icu_6739FormattedValueFieldPositionIteratorImplD0Ev, @function
_ZN6icu_6739FormattedValueFieldPositionIteratorImplD0Ev:
.LFB2428:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6739FormattedValueFieldPositionIteratorImplE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	72(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -72(%rdi)
	call	_ZN6icu_679UVector32D1Ev@PLT
	leaq	8(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6714FormattedValueD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2428:
	.size	_ZN6icu_6739FormattedValueFieldPositionIteratorImplD0Ev, .-_ZN6icu_6739FormattedValueFieldPositionIteratorImplD0Ev
	.weak	_ZTSN6icu_677UMemoryE
	.section	.rodata._ZTSN6icu_677UMemoryE,"aG",@progbits,_ZTSN6icu_677UMemoryE,comdat
	.align 16
	.type	_ZTSN6icu_677UMemoryE, @object
	.size	_ZTSN6icu_677UMemoryE, 18
_ZTSN6icu_677UMemoryE:
	.string	"N6icu_677UMemoryE"
	.weak	_ZTIN6icu_677UMemoryE
	.section	.data.rel.ro._ZTIN6icu_677UMemoryE,"awG",@progbits,_ZTIN6icu_677UMemoryE,comdat
	.align 8
	.type	_ZTIN6icu_677UMemoryE, @object
	.size	_ZTIN6icu_677UMemoryE, 16
_ZTIN6icu_677UMemoryE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN6icu_677UMemoryE
	.weak	_ZTSN6icu_6739FormattedValueFieldPositionIteratorImplE
	.section	.rodata._ZTSN6icu_6739FormattedValueFieldPositionIteratorImplE,"aG",@progbits,_ZTSN6icu_6739FormattedValueFieldPositionIteratorImplE,comdat
	.align 32
	.type	_ZTSN6icu_6739FormattedValueFieldPositionIteratorImplE, @object
	.size	_ZTSN6icu_6739FormattedValueFieldPositionIteratorImplE, 51
_ZTSN6icu_6739FormattedValueFieldPositionIteratorImplE:
	.string	"N6icu_6739FormattedValueFieldPositionIteratorImplE"
	.weak	_ZTIN6icu_6739FormattedValueFieldPositionIteratorImplE
	.section	.data.rel.ro._ZTIN6icu_6739FormattedValueFieldPositionIteratorImplE,"awG",@progbits,_ZTIN6icu_6739FormattedValueFieldPositionIteratorImplE,comdat
	.align 8
	.type	_ZTIN6icu_6739FormattedValueFieldPositionIteratorImplE, @object
	.size	_ZTIN6icu_6739FormattedValueFieldPositionIteratorImplE, 56
_ZTIN6icu_6739FormattedValueFieldPositionIteratorImplE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN6icu_6739FormattedValueFieldPositionIteratorImplE
	.long	0
	.long	2
	.quad	_ZTIN6icu_677UMemoryE
	.quad	2
	.quad	_ZTIN6icu_6714FormattedValueE
	.quad	2
	.weak	_ZTVN6icu_6739FormattedValueFieldPositionIteratorImplE
	.section	.data.rel.ro.local._ZTVN6icu_6739FormattedValueFieldPositionIteratorImplE,"awG",@progbits,_ZTVN6icu_6739FormattedValueFieldPositionIteratorImplE,comdat
	.align 8
	.type	_ZTVN6icu_6739FormattedValueFieldPositionIteratorImplE, @object
	.size	_ZTVN6icu_6739FormattedValueFieldPositionIteratorImplE, 64
_ZTVN6icu_6739FormattedValueFieldPositionIteratorImplE:
	.quad	0
	.quad	_ZTIN6icu_6739FormattedValueFieldPositionIteratorImplE
	.quad	_ZN6icu_6739FormattedValueFieldPositionIteratorImplD1Ev
	.quad	_ZN6icu_6739FormattedValueFieldPositionIteratorImplD0Ev
	.quad	_ZNK6icu_6739FormattedValueFieldPositionIteratorImpl8toStringER10UErrorCode
	.quad	_ZNK6icu_6739FormattedValueFieldPositionIteratorImpl12toTempStringER10UErrorCode
	.quad	_ZNK6icu_6739FormattedValueFieldPositionIteratorImpl8appendToERNS_10AppendableER10UErrorCode
	.quad	_ZNK6icu_6739FormattedValueFieldPositionIteratorImpl12nextPositionERNS_24ConstrainedFieldPositionER10UErrorCode
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
