	.file	"gregocal.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717GregorianCalendar17getDynamicClassIDEv
	.type	_ZNK6icu_6717GregorianCalendar17getDynamicClassIDEv, @function
_ZNK6icu_6717GregorianCalendar17getDynamicClassIDEv:
.LFB2979:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6717GregorianCalendar16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2979:
	.size	_ZNK6icu_6717GregorianCalendar17getDynamicClassIDEv, .-_ZNK6icu_6717GregorianCalendar17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717GregorianCalendar16getActualMinimumE19UCalendarDateFieldsR10UErrorCode
	.type	_ZNK6icu_6717GregorianCalendar16getActualMinimumE19UCalendarDateFieldsR10UErrorCode, @function
_ZNK6icu_6717GregorianCalendar16getActualMinimumE19UCalendarDateFieldsR10UErrorCode:
.LFB3041:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*112(%rax)
	.cfi_endproc
.LFE3041:
	.size	_ZNK6icu_6717GregorianCalendar16getActualMinimumE19UCalendarDateFieldsR10UErrorCode, .-_ZNK6icu_6717GregorianCalendar16getActualMinimumE19UCalendarDateFieldsR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717GregorianCalendar14handleGetLimitE19UCalendarDateFieldsNS_8Calendar10ELimitTypeE
	.type	_ZNK6icu_6717GregorianCalendar14handleGetLimitE19UCalendarDateFieldsNS_8Calendar10ELimitTypeE, @function
_ZNK6icu_6717GregorianCalendar14handleGetLimitE19UCalendarDateFieldsNS_8Calendar10ELimitTypeE:
.LFB3042:
	.cfi_startproc
	endbr64
	movslq	%esi, %rsi
	movslq	%edx, %rdx
	leaq	_ZL24kGregorianCalendarLimits(%rip), %rax
	leaq	(%rdx,%rsi,4), %rdx
	movl	(%rax,%rdx,4), %eax
	ret
	.cfi_endproc
.LFE3042:
	.size	_ZNK6icu_6717GregorianCalendar14handleGetLimitE19UCalendarDateFieldsNS_8Calendar10ELimitTypeE, .-_ZNK6icu_6717GregorianCalendar14handleGetLimitE19UCalendarDateFieldsNS_8Calendar10ELimitTypeE
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"gregorian"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717GregorianCalendar7getTypeEv
	.type	_ZNK6icu_6717GregorianCalendar7getTypeEv, @function
_ZNK6icu_6717GregorianCalendar7getTypeEv:
.LFB3048:
	.cfi_startproc
	endbr64
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE3048:
	.size	_ZNK6icu_6717GregorianCalendar7getTypeEv, .-_ZNK6icu_6717GregorianCalendar7getTypeEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717GregorianCalendar18haveDefaultCenturyEv
	.type	_ZNK6icu_6717GregorianCalendar18haveDefaultCenturyEv, @function
_ZNK6icu_6717GregorianCalendar18haveDefaultCenturyEv:
.LFB3049:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3049:
	.size	_ZNK6icu_6717GregorianCalendar18haveDefaultCenturyEv, .-_ZNK6icu_6717GregorianCalendar18haveDefaultCenturyEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717GregorianCalendarD2Ev
	.type	_ZN6icu_6717GregorianCalendarD2Ev, @function
_ZN6icu_6717GregorianCalendarD2Ev:
.LFB3008:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6717GregorianCalendarE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_678CalendarD2Ev@PLT
	.cfi_endproc
.LFE3008:
	.size	_ZN6icu_6717GregorianCalendarD2Ev, .-_ZN6icu_6717GregorianCalendarD2Ev
	.globl	_ZN6icu_6717GregorianCalendarD1Ev
	.set	_ZN6icu_6717GregorianCalendarD1Ev,_ZN6icu_6717GregorianCalendarD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717GregorianCalendarD0Ev
	.type	_ZN6icu_6717GregorianCalendarD0Ev, @function
_ZN6icu_6717GregorianCalendarD0Ev:
.LFB3010:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6717GregorianCalendarE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_678CalendarD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3010:
	.size	_ZN6icu_6717GregorianCalendarD0Ev, .-_ZN6icu_6717GregorianCalendarD0Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717GregorianCalendar5cloneEv
	.type	_ZNK6icu_6717GregorianCalendar5cloneEv, @function
_ZNK6icu_6717GregorianCalendar5cloneEv:
.LFB3014:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	$656, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L10
	movq	%rax, %rdi
	movq	%rbx, %rsi
	call	_ZN6icu_678CalendarC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6717GregorianCalendarE(%rip), %rax
	movsd	616(%rbx), %xmm0
	movq	%rax, (%r12)
	movl	624(%rbx), %eax
	movsd	%xmm0, 616(%r12)
	movsd	632(%rbx), %xmm0
	movl	%eax, 624(%r12)
	movl	640(%rbx), %eax
	movsd	%xmm0, 632(%r12)
	movl	%eax, 640(%r12)
	movzwl	648(%rbx), %eax
	movw	%ax, 648(%r12)
.L10:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3014:
	.size	_ZNK6icu_6717GregorianCalendar5cloneEv, .-_ZNK6icu_6717GregorianCalendar5cloneEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717GregorianCalendar14internalGetEraEv
	.type	_ZNK6icu_6717GregorianCalendar14internalGetEraEv, @function
_ZNK6icu_6717GregorianCalendar14internalGetEraEv:
.LFB3047:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZNK6icu_678Calendar5isSetE19UCalendarDateFields@PLT
	movl	%eax, %r8d
	movl	$1, %eax
	testb	%r8b, %r8b
	je	.L16
	movl	12(%rbx), %eax
.L16:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3047:
	.size	_ZNK6icu_6717GregorianCalendar14internalGetEraEv, .-_ZNK6icu_6717GregorianCalendar14internalGetEraEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717GregorianCalendar14inDaylightTimeER10UErrorCode
	.type	_ZNK6icu_6717GregorianCalendar14inDaylightTimeER10UErrorCode, @function
_ZNK6icu_6717GregorianCalendar14inDaylightTimeER10UErrorCode:
.LFB3046:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	testl	%ecx, %ecx
	jle	.L23
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_ZNK6icu_678Calendar11getTimeZoneEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*72(%rax)
	testb	%al, %al
	jne	.L34
	xorl	%eax, %eax
.L22:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	.cfi_restore_state
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_678Calendar8completeER10UErrorCode@PLT
	movl	(%rbx), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L22
	movl	76(%r12), %eax
	testl	%eax, %eax
	setne	%al
	jmp	.L22
	.cfi_endproc
.LFE3046:
	.size	_ZNK6icu_6717GregorianCalendar14inDaylightTimeER10UErrorCode, .-_ZNK6icu_6717GregorianCalendar14inDaylightTimeER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717GregorianCalendar11getEpochDayER10UErrorCode
	.type	_ZN6icu_6717GregorianCalendar11getEpochDayER10UErrorCode, @function
_ZN6icu_6717GregorianCalendar11getEpochDayER10UErrorCode:
.LFB3032:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_678Calendar8completeER10UErrorCode@PLT
	movl	72(%rbx), %edx
	addl	76(%rbx), %edx
	pxor	%xmm0, %xmm0
	movslq	%edx, %rax
	sarl	$31, %edx
	movsd	224(%rbx), %xmm1
	divsd	.LC1(%rip), %xmm1
	imulq	$274877907, %rax, %rax
	sarq	$38, %rax
	subl	%edx, %eax
	cvtsi2sdl	%eax, %xmm0
	addsd	%xmm1, %xmm0
	divsd	.LC2(%rip), %xmm0
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	uprv_floor_67@PLT
	.cfi_endproc
.LFE3032:
	.size	_ZN6icu_6717GregorianCalendar11getEpochDayER10UErrorCode, .-_ZN6icu_6717GregorianCalendar11getEpochDayER10UErrorCode
	.p2align 4
	.type	_ZN6icu_67L30initializeSystemDefaultCenturyEv, @function
_ZN6icu_67L30initializeSystemDefaultCenturyEv:
.LFB3050:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-708(%rbp), %r13
	leaq	-704(%rbp), %r12
	pushq	%rbx
	movq	%r13, %rsi
	movq	%r12, %rdi
	.cfi_offset 3, -40
	leaq	16+_ZTVN6icu_6717GregorianCalendarE(%rip), %rbx
	subq	$696, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$0, -708(%rbp)
	call	_ZN6icu_678CalendarC2ER10UErrorCode@PLT
	movsd	.LC3(%rip), %xmm0
	movl	$1, %eax
	movq	%rbx, -704(%rbp)
	movw	%ax, -56(%rbp)
	movl	$2299161, -80(%rbp)
	movl	$1582, -64(%rbp)
	movsd	%xmm0, -88(%rbp)
	movsd	%xmm0, -72(%rbp)
	call	_ZN6icu_678Calendar6getNowEv@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_678Calendar15setTimeInMillisEdR10UErrorCode@PLT
	movl	-708(%rbp), %edx
	testl	%edx, %edx
	jle	.L41
.L38:
	movq	%r12, %rdi
	movq	%rbx, -704(%rbp)
	call	_ZN6icu_678CalendarD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L42
	addq	$696, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	.cfi_restore_state
	call	_ZN6icu_678Calendar6getNowEv@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_678Calendar15setTimeInMillisEdR10UErrorCode@PLT
	movq	%r13, %rcx
	movl	$-80, %edx
	movq	%r12, %rdi
	movl	$1, %esi
	call	_ZN6icu_678Calendar3addE19UCalendarDateFieldsiR10UErrorCode@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK6icu_678Calendar15getTimeInMillisER10UErrorCode@PLT
	movq	%r13, %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	movsd	%xmm0, _ZN6icu_67L26gSystemDefaultCenturyStartE(%rip)
	call	_ZNK6icu_678Calendar3getE19UCalendarDateFieldsR10UErrorCode@PLT
	movl	%eax, _ZN6icu_67L30gSystemDefaultCenturyStartYearE(%rip)
	jmp	.L38
.L42:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3050:
	.size	_ZN6icu_67L30initializeSystemDefaultCenturyEv, .-_ZN6icu_67L30initializeSystemDefaultCenturyEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717GregorianCalendar19defaultCenturyStartEv
	.type	_ZNK6icu_6717GregorianCalendar19defaultCenturyStartEv, @function
_ZNK6icu_6717GregorianCalendar19defaultCenturyStartEv:
.LFB3051:
	.cfi_startproc
	endbr64
	movl	_ZN6icu_67L25gSystemDefaultCenturyInitE(%rip), %eax
	cmpl	$2, %eax
	je	.L51
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN6icu_67L25gSystemDefaultCenturyInitE(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L45
	call	_ZN6icu_67L30initializeSystemDefaultCenturyEv
	leaq	_ZN6icu_67L25gSystemDefaultCenturyInitE(%rip), %rdi
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L45:
	movsd	_ZN6icu_67L26gSystemDefaultCenturyStartE(%rip), %xmm0
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L51:
	.cfi_restore 6
	movsd	_ZN6icu_67L26gSystemDefaultCenturyStartE(%rip), %xmm0
	ret
	.cfi_endproc
.LFE3051:
	.size	_ZNK6icu_6717GregorianCalendar19defaultCenturyStartEv, .-_ZNK6icu_6717GregorianCalendar19defaultCenturyStartEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717GregorianCalendar23defaultCenturyStartYearEv
	.type	_ZNK6icu_6717GregorianCalendar23defaultCenturyStartYearEv, @function
_ZNK6icu_6717GregorianCalendar23defaultCenturyStartYearEv:
.LFB3052:
	.cfi_startproc
	endbr64
	movl	_ZN6icu_67L25gSystemDefaultCenturyInitE(%rip), %eax
	cmpl	$2, %eax
	je	.L62
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN6icu_67L25gSystemDefaultCenturyInitE(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L56
	call	_ZN6icu_67L30initializeSystemDefaultCenturyEv
	leaq	_ZN6icu_67L25gSystemDefaultCenturyInitE(%rip), %rdi
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L56:
	movl	_ZN6icu_67L30gSystemDefaultCenturyStartYearE(%rip), %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	.cfi_restore 6
	movl	_ZN6icu_67L30gSystemDefaultCenturyStartYearE(%rip), %eax
	ret
	.cfi_endproc
.LFE3052:
	.size	_ZNK6icu_6717GregorianCalendar23defaultCenturyStartYearEv, .-_ZNK6icu_6717GregorianCalendar23defaultCenturyStartYearEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717GregorianCalendar23handleComputeMonthStartEiia
	.type	_ZNK6icu_6717GregorianCalendar23handleComputeMonthStartEiia, @function
_ZNK6icu_6717GregorianCalendar23handleComputeMonthStartEiia:
.LFB3022:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$24, %rsp
	movl	%edx, -52(%rbp)
	cmpl	$11, %edx
	ja	.L79
.L66:
	movslq	%ebx, %r12
	movl	$4, %esi
	movl	%ebx, %r15d
	subq	$1, %r12
	andl	$3, %r15d
	movq	%r12, %rdi
	call	_ZN6icu_679ClockMath11floorDivideEll@PLT
	movq	%rax, %r8
	leaq	(%r12,%r12,8), %rax
	leaq	(%r12,%rax,8), %rax
	leaq	(%rax,%rax,4), %rax
	leaq	1721423(%rax,%r8), %rdx
	movl	640(%r13), %eax
	cmpl	%ebx, %eax
	setle	%r14b
	cmpb	$0, 649(%r13)
	movb	%r14b, 648(%r13)
	je	.L67
	cmpl	%ebx, %eax
	setg	%r14b
	movb	%r14b, 648(%r13)
.L67:
	testb	%r14b, %r14b
	jne	.L68
	testl	%r15d, %r15d
	sete	%r14b
.L69:
	movslq	-52(%rbp), %rax
	testl	%eax, %eax
	je	.L71
	testb	%r14b, %r14b
	jne	.L80
	leaq	_ZL8kNumDays(%rip), %rcx
	movswq	(%rcx,%rax,2), %rax
.L73:
	addq	%rax, %rdx
.L71:
	addq	$24, %rsp
	movl	%edx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	.cfi_restore_state
	testl	%r15d, %r15d
	je	.L81
	xorl	%r14d, %r14d
.L70:
	movl	$400, %esi
	movq	%r12, %rdi
	movq	%rdx, -64(%rbp)
	call	_ZN6icu_679ClockMath11floorDivideEll@PLT
	movl	$100, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN6icu_679ClockMath11floorDivideEll@PLT
	addl	$2, %ebx
	movq	-64(%rbp), %rdx
	subl	%eax, %ebx
	movslq	%ebx, %rbx
	addq	%rbx, %rdx
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L81:
	imull	$-1030792151, %ebx, %ebx
	addl	$85899344, %ebx
	movl	%ebx, %eax
	rorl	$2, %eax
	cmpl	$42949672, %eax
	ja	.L70
	rorl	$4, %ebx
	cmpl	$10737418, %ebx
	setbe	%r14b
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L80:
	leaq	_ZL12kLeapNumDays(%rip), %rcx
	movswq	(%rcx,%rax,2), %rax
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L79:
	pxor	%xmm0, %xmm0
	leaq	-52(%rbp), %rsi
	movl	$12, %edi
	cvtsi2sdl	%edx, %xmm0
	call	_ZN6icu_679ClockMath11floorDivideEdiRi@PLT
	addl	%eax, %ebx
	jmp	.L66
	.cfi_endproc
.LFE3022:
	.size	_ZNK6icu_6717GregorianCalendar23handleComputeMonthStartEiia, .-_ZNK6icu_6717GregorianCalendar23handleComputeMonthStartEiia
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717GregorianCalendar19handleComputeFieldsEiR10UErrorCode
	.type	_ZN6icu_6717GregorianCalendar19handleComputeFieldsEiR10UErrorCode, @function
_ZN6icu_6717GregorianCalendar19handleComputeFieldsEiR10UErrorCode:
.LFB3018:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rdx), %ecx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L82
	movq	%rdi, %rbx
	movl	%esi, %r14d
	cmpl	624(%rdi), %esi
	jl	.L84
	movl	280(%rdi), %r12d
	movl	640(%rdi), %r14d
	movl	284(%rdi), %r15d
	movl	292(%rdi), %r13d
	movl	288(%rdi), %edx
	cmpl	%r14d, %r12d
	je	.L85
.L86:
	movabsq	$4294967297, %rax
	movl	%edx, 36(%rbx)
	movl	$257, %edx
	movq	%rax, 148(%rbx)
	movl	$1, %eax
	movl	%r15d, 20(%rbx)
	movl	$1, 136(%rbx)
	movb	$1, 106(%rbx)
	movl	%r13d, 32(%rbx)
	movw	%dx, 109(%rbx)
	movl	%r12d, 88(%rbx)
	movl	$1, 204(%rbx)
	movb	$1, 123(%rbx)
	testl	%r12d, %r12d
	jg	.L90
	subl	%r12d, %eax
	movl	%eax, %r12d
	xorl	%eax, %eax
.L90:
	movl	%eax, 12(%rbx)
	movabsq	$4294967297, %rax
	movq	%rax, 128(%rbx)
	movl	$257, %eax
	movl	%r12d, 16(%rbx)
	movw	%ax, 104(%rbx)
.L82:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L101
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	.cfi_restore_state
	leal	-1721424(%rsi), %r13d
	pxor	%xmm0, %xmm0
	leaq	-60(%rbp), %rsi
	movl	$1461, %edi
	cvtsi2sdl	%r13d, %xmm0
	mulsd	.LC4(%rip), %xmm0
	addsd	.LC5(%rip), %xmm0
	call	_ZN6icu_679ClockMath11floorDivideEdiRi@PLT
	movl	$4, %esi
	leal	-1(%rax), %r15d
	movl	%eax, %r12d
	movl	%r15d, %edi
	imull	$365, %r15d, %r15d
	call	_ZN6icu_679ClockMath11floorDivideEii@PLT
	addl	%eax, %r15d
	subl	%r15d, %r13d
	testb	$3, %r12b
	jne	.L102
	leal	1(%r13), %edx
	cmpl	$59, %r13d
	jg	.L95
	leal	0(%r13,%r13,2), %eax
	leal	6(,%rax,4), %eax
	movslq	%eax, %r15
	sarl	$31, %eax
	imulq	$1497972245, %r15, %r15
	sarq	$39, %r15
	subl	%eax, %r15d
.L91:
	movslq	%r15d, %rax
	leaq	_ZL12kLeapNumDays(%rip), %rcx
	movswl	(%rcx,%rax,2), %eax
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L102:
	cmpl	$58, %r13d
	jg	.L93
	leal	0(%r13,%r13,2), %eax
	leal	6(,%rax,4), %eax
	movslq	%eax, %r15
	sarl	$31, %eax
	imulq	$1497972245, %r15, %r15
	sarq	$39, %r15
	subl	%eax, %r15d
.L92:
	leaq	_ZL8kNumDays(%rip), %rdx
	movslq	%r15d, %rax
	movswl	(%rdx,%rax,2), %eax
	leal	1(%r13), %edx
.L89:
	subl	%eax, %r13d
	addl	$1, %r13d
	cmpl	640(%rbx), %r12d
	jne	.L86
	cmpl	624(%rbx), %r14d
	jl	.L86
	movl	%r12d, %r14d
	.p2align 4,,10
	.p2align 3
.L85:
	movslq	%r14d, %rdi
	movl	$400, %esi
	movl	%edx, -76(%rbp)
	subq	$1, %rdi
	movq	%rdi, -72(%rbp)
	call	_ZN6icu_679ClockMath11floorDivideEll@PLT
	movq	-72(%rbp), %rdi
	movl	$100, %esi
	movq	%rax, %r12
	call	_ZN6icu_679ClockMath11floorDivideEll@PLT
	addl	$2, %r12d
	movl	-76(%rbp), %edx
	subl	%eax, %r12d
	addl	%r12d, %edx
	movl	%r14d, %r12d
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L93:
	leal	6(%r13,%r13,2), %eax
	leal	6(,%rax,4), %r15d
	imulq	$1497972245, %r15, %r15
	shrq	$39, %r15
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L95:
	leal	(%rdx,%rdx,2), %eax
	leal	6(,%rax,4), %r15d
	imulq	$1497972245, %r15, %r15
	shrq	$39, %r15
	jmp	.L91
.L101:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3018:
	.size	_ZN6icu_6717GregorianCalendar19handleComputeFieldsEiR10UErrorCode, .-_ZN6icu_6717GregorianCalendar19handleComputeFieldsEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717GregorianCalendar19handleGetYearLengthEi
	.type	_ZNK6icu_6717GregorianCalendar19handleGetYearLengthEi, @function
_ZNK6icu_6717GregorianCalendar19handleGetYearLengthEi:
.LFB3024:
	.cfi_startproc
	endbr64
	movl	%esi, %edx
	andl	$3, %edx
	cmpl	640(%rdi), %esi
	jl	.L104
	movl	$365, %eax
	testl	%edx, %edx
	jne	.L103
	imull	$-1030792151, %esi, %esi
	movl	$366, %eax
	addl	$85899344, %esi
	movl	%esi, %edx
	rorl	$2, %edx
	cmpl	$42949672, %edx
	jbe	.L110
.L103:
	ret
	.p2align 4,,10
	.p2align 3
.L104:
	xorl	%eax, %eax
	testl	%edx, %edx
	sete	%al
	addl	$365, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L110:
	rorl	$4, %esi
	xorl	%eax, %eax
	cmpl	$10737419, %esi
	setb	%al
	addl	$365, %eax
	ret
	.cfi_endproc
.LFE3024:
	.size	_ZNK6icu_6717GregorianCalendar19handleGetYearLengthEi, .-_ZNK6icu_6717GregorianCalendar19handleGetYearLengthEi
	.globl	_ZNK6icu_6717GregorianCalendar10yearLengthEi
	.set	_ZNK6icu_6717GregorianCalendar10yearLengthEi,_ZNK6icu_6717GregorianCalendar19handleGetYearLengthEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717GregorianCalendar11monthLengthEii
	.type	_ZNK6icu_6717GregorianCalendar11monthLengthEii, @function
_ZNK6icu_6717GregorianCalendar11monthLengthEii:
.LFB3026:
	.cfi_startproc
	endbr64
	movl	%edx, %eax
	andl	$3, %eax
	cmpl	640(%rdi), %edx
	jl	.L112
	testl	%eax, %eax
	jne	.L113
	imull	$-1030792151, %edx, %edx
	addl	$85899344, %edx
	movl	%edx, %eax
	rorl	$2, %eax
	cmpl	$42949672, %eax
	jbe	.L116
.L114:
	movslq	%esi, %rsi
	leaq	_ZL16kLeapMonthLength(%rip), %rax
	movsbl	(%rax,%rsi), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L116:
	rorl	$4, %edx
	cmpl	$10737418, %edx
	jbe	.L114
.L113:
	movslq	%esi, %rsi
	leaq	_ZL12kMonthLength(%rip), %rax
	movsbl	(%rax,%rsi), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L112:
	testl	%eax, %eax
	jne	.L113
	movslq	%esi, %rsi
	leaq	_ZL16kLeapMonthLength(%rip), %rax
	movsbl	(%rax,%rsi), %eax
	ret
	.cfi_endproc
.LFE3026:
	.size	_ZNK6icu_6717GregorianCalendar11monthLengthEii, .-_ZNK6icu_6717GregorianCalendar11monthLengthEii
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717GregorianCalendar11monthLengthEi
	.type	_ZNK6icu_6717GregorianCalendar11monthLengthEi, @function
_ZNK6icu_6717GregorianCalendar11monthLengthEi:
.LFB3025:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZNK6icu_6717GregorianCalendar20handleGetMonthLengthEii(%rip), %rcx
	movslq	%esi, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$16, %rsp
	movl	88(%rdi), %r13d
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	272(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L118
	movl	%edx, -28(%rbp)
	cmpl	$11, %edx
	ja	.L127
.L119:
	movl	%r13d, %eax
	andl	$3, %eax
	cmpl	%r13d, 640(%r12)
	jle	.L128
	testl	%eax, %eax
	jne	.L121
.L122:
	leaq	_ZL16kLeapMonthLength(%rip), %rax
	movsbl	(%rax,%rdx), %eax
.L117:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L129
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L128:
	.cfi_restore_state
	testl	%eax, %eax
	jne	.L121
	imull	$-1030792151, %r13d, %r13d
	addl	$85899344, %r13d
	movl	%r13d, %eax
	rorl	$2, %eax
	cmpl	$42949672, %eax
	ja	.L122
	rorl	$4, %r13d
	cmpl	$10737418, %r13d
	jbe	.L122
	.p2align 4,,10
	.p2align 3
.L121:
	leaq	_ZL12kMonthLength(%rip), %rax
	movsbl	(%rax,%rdx), %eax
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L127:
	pxor	%xmm0, %xmm0
	leaq	-28(%rbp), %rsi
	movl	$12, %edi
	cvtsi2sdl	%edx, %xmm0
	call	_ZN6icu_679ClockMath11floorDivideEdiRi@PLT
	movslq	-28(%rbp), %rdx
	addl	%eax, %r13d
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L118:
	movl	%r13d, %esi
	call	*%rax
	jmp	.L117
.L129:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3025:
	.size	_ZNK6icu_6717GregorianCalendar11monthLengthEi, .-_ZNK6icu_6717GregorianCalendar11monthLengthEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717GregorianCalendar20handleGetMonthLengthEii
	.type	_ZNK6icu_6717GregorianCalendar20handleGetMonthLengthEii, @function
_ZNK6icu_6717GregorianCalendar20handleGetMonthLengthEii:
.LFB3023:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edx, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%esi, %ebx
	subq	$16, %rsp
	movl	%edx, -20(%rbp)
	cmpl	$11, %eax
	ja	.L137
.L131:
	movl	%ebx, %edx
	andl	$3, %edx
	cmpl	640(%r12), %ebx
	jl	.L132
	testl	%edx, %edx
	je	.L138
.L133:
	leaq	_ZL12kMonthLength(%rip), %rdx
	movsbl	(%rdx,%rax), %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L132:
	.cfi_restore_state
	testl	%edx, %edx
	jne	.L133
.L134:
	leaq	_ZL16kLeapMonthLength(%rip), %rdx
	movsbl	(%rdx,%rax), %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L138:
	.cfi_restore_state
	imull	$-1030792151, %ebx, %ebx
	addl	$85899344, %ebx
	movl	%ebx, %edx
	rorl	$2, %edx
	cmpl	$42949672, %edx
	ja	.L134
	rorl	$4, %ebx
	cmpl	$10737418, %ebx
	jbe	.L134
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L137:
	pxor	%xmm0, %xmm0
	leaq	-20(%rbp), %rsi
	movl	$12, %edi
	cvtsi2sdl	%eax, %xmm0
	call	_ZN6icu_679ClockMath11floorDivideEdiRi@PLT
	addl	%eax, %ebx
	movslq	-20(%rbp), %rax
	jmp	.L131
	.cfi_endproc
.LFE3023:
	.size	_ZNK6icu_6717GregorianCalendar20handleGetMonthLengthEii, .-_ZNK6icu_6717GregorianCalendar20handleGetMonthLengthEii
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717GregorianCalendar16getStaticClassIDEv
	.type	_ZN6icu_6717GregorianCalendar16getStaticClassIDEv, @function
_ZN6icu_6717GregorianCalendar16getStaticClassIDEv:
.LFB2978:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6717GregorianCalendar16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2978:
	.size	_ZN6icu_6717GregorianCalendar16getStaticClassIDEv, .-_ZN6icu_6717GregorianCalendar16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717GregorianCalendarC2ER10UErrorCode
	.type	_ZN6icu_6717GregorianCalendarC2ER10UErrorCode, @function
_ZN6icu_6717GregorianCalendarC2ER10UErrorCode:
.LFB2981:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN6icu_678CalendarC2ER10UErrorCode@PLT
	leaq	16+_ZTVN6icu_6717GregorianCalendarE(%rip), %rax
	movsd	.LC3(%rip), %xmm0
	movl	$2299161, 624(%r12)
	movq	%rax, (%r12)
	movl	$1, %eax
	movl	$1582, 640(%r12)
	movw	%ax, 648(%r12)
	movsd	%xmm0, 616(%r12)
	movsd	%xmm0, 632(%r12)
	call	_ZN6icu_678Calendar6getNowEv@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_678Calendar15setTimeInMillisEdR10UErrorCode@PLT
	.cfi_endproc
.LFE2981:
	.size	_ZN6icu_6717GregorianCalendarC2ER10UErrorCode, .-_ZN6icu_6717GregorianCalendarC2ER10UErrorCode
	.globl	_ZN6icu_6717GregorianCalendarC1ER10UErrorCode
	.set	_ZN6icu_6717GregorianCalendarC1ER10UErrorCode,_ZN6icu_6717GregorianCalendarC2ER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717GregorianCalendarC2EPNS_8TimeZoneER10UErrorCode
	.type	_ZN6icu_6717GregorianCalendarC2EPNS_8TimeZoneER10UErrorCode, @function
_ZN6icu_6717GregorianCalendarC2EPNS_8TimeZoneER10UErrorCode:
.LFB2984:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN6icu_676Locale10getDefaultEv@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%r13, %rcx
	movq	%rax, %rdx
	call	_ZN6icu_678CalendarC2EPNS_8TimeZoneERKNS_6LocaleER10UErrorCode@PLT
	leaq	16+_ZTVN6icu_6717GregorianCalendarE(%rip), %rax
	movsd	.LC3(%rip), %xmm0
	movl	$2299161, 624(%r12)
	movq	%rax, (%r12)
	movl	$1, %eax
	movl	$1582, 640(%r12)
	movw	%ax, 648(%r12)
	movsd	%xmm0, 616(%r12)
	movsd	%xmm0, 632(%r12)
	call	_ZN6icu_678Calendar6getNowEv@PLT
	addq	$8, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_678Calendar15setTimeInMillisEdR10UErrorCode@PLT
	.cfi_endproc
.LFE2984:
	.size	_ZN6icu_6717GregorianCalendarC2EPNS_8TimeZoneER10UErrorCode, .-_ZN6icu_6717GregorianCalendarC2EPNS_8TimeZoneER10UErrorCode
	.globl	_ZN6icu_6717GregorianCalendarC1EPNS_8TimeZoneER10UErrorCode
	.set	_ZN6icu_6717GregorianCalendarC1EPNS_8TimeZoneER10UErrorCode,_ZN6icu_6717GregorianCalendarC2EPNS_8TimeZoneER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717GregorianCalendarC2ERKNS_8TimeZoneER10UErrorCode
	.type	_ZN6icu_6717GregorianCalendarC2ERKNS_8TimeZoneER10UErrorCode, @function
_ZN6icu_6717GregorianCalendarC2ERKNS_8TimeZoneER10UErrorCode:
.LFB2987:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN6icu_676Locale10getDefaultEv@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%r13, %rcx
	movq	%rax, %rdx
	call	_ZN6icu_678CalendarC2ERKNS_8TimeZoneERKNS_6LocaleER10UErrorCode@PLT
	leaq	16+_ZTVN6icu_6717GregorianCalendarE(%rip), %rax
	movsd	.LC3(%rip), %xmm0
	movl	$2299161, 624(%r12)
	movq	%rax, (%r12)
	movl	$1, %eax
	movl	$1582, 640(%r12)
	movw	%ax, 648(%r12)
	movsd	%xmm0, 616(%r12)
	movsd	%xmm0, 632(%r12)
	call	_ZN6icu_678Calendar6getNowEv@PLT
	addq	$8, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_678Calendar15setTimeInMillisEdR10UErrorCode@PLT
	.cfi_endproc
.LFE2987:
	.size	_ZN6icu_6717GregorianCalendarC2ERKNS_8TimeZoneER10UErrorCode, .-_ZN6icu_6717GregorianCalendarC2ERKNS_8TimeZoneER10UErrorCode
	.globl	_ZN6icu_6717GregorianCalendarC1ERKNS_8TimeZoneER10UErrorCode
	.set	_ZN6icu_6717GregorianCalendarC1ERKNS_8TimeZoneER10UErrorCode,_ZN6icu_6717GregorianCalendarC2ERKNS_8TimeZoneER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717GregorianCalendarC2ERKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_6717GregorianCalendarC2ERKNS_6LocaleER10UErrorCode, @function
_ZN6icu_6717GregorianCalendarC2ERKNS_6LocaleER10UErrorCode:
.LFB2990:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN6icu_678TimeZone13createDefaultEv@PLT
	movq	%r12, %rdi
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%rax, %rsi
	call	_ZN6icu_678CalendarC2EPNS_8TimeZoneERKNS_6LocaleER10UErrorCode@PLT
	leaq	16+_ZTVN6icu_6717GregorianCalendarE(%rip), %rax
	movsd	.LC3(%rip), %xmm0
	movl	$2299161, 624(%r12)
	movq	%rax, (%r12)
	movl	$1, %eax
	movl	$1582, 640(%r12)
	movw	%ax, 648(%r12)
	movsd	%xmm0, 616(%r12)
	movsd	%xmm0, 632(%r12)
	call	_ZN6icu_678Calendar6getNowEv@PLT
	addq	$8, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_678Calendar15setTimeInMillisEdR10UErrorCode@PLT
	.cfi_endproc
.LFE2990:
	.size	_ZN6icu_6717GregorianCalendarC2ERKNS_6LocaleER10UErrorCode, .-_ZN6icu_6717GregorianCalendarC2ERKNS_6LocaleER10UErrorCode
	.globl	_ZN6icu_6717GregorianCalendarC1ERKNS_6LocaleER10UErrorCode
	.set	_ZN6icu_6717GregorianCalendarC1ERKNS_6LocaleER10UErrorCode,_ZN6icu_6717GregorianCalendarC2ERKNS_6LocaleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717GregorianCalendarC2EPNS_8TimeZoneERKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_6717GregorianCalendarC2EPNS_8TimeZoneERKNS_6LocaleER10UErrorCode, @function
_ZN6icu_6717GregorianCalendarC2EPNS_8TimeZoneERKNS_6LocaleER10UErrorCode:
.LFB2993:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN6icu_678CalendarC2EPNS_8TimeZoneERKNS_6LocaleER10UErrorCode@PLT
	leaq	16+_ZTVN6icu_6717GregorianCalendarE(%rip), %rax
	movsd	.LC3(%rip), %xmm0
	movl	$2299161, 624(%r12)
	movq	%rax, (%r12)
	movl	$1, %eax
	movl	$1582, 640(%r12)
	movw	%ax, 648(%r12)
	movsd	%xmm0, 616(%r12)
	movsd	%xmm0, 632(%r12)
	call	_ZN6icu_678Calendar6getNowEv@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_678Calendar15setTimeInMillisEdR10UErrorCode@PLT
	.cfi_endproc
.LFE2993:
	.size	_ZN6icu_6717GregorianCalendarC2EPNS_8TimeZoneERKNS_6LocaleER10UErrorCode, .-_ZN6icu_6717GregorianCalendarC2EPNS_8TimeZoneERKNS_6LocaleER10UErrorCode
	.globl	_ZN6icu_6717GregorianCalendarC1EPNS_8TimeZoneERKNS_6LocaleER10UErrorCode
	.set	_ZN6icu_6717GregorianCalendarC1EPNS_8TimeZoneERKNS_6LocaleER10UErrorCode,_ZN6icu_6717GregorianCalendarC2EPNS_8TimeZoneERKNS_6LocaleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717GregorianCalendarC2ERKNS_8TimeZoneERKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_6717GregorianCalendarC2ERKNS_8TimeZoneERKNS_6LocaleER10UErrorCode, @function
_ZN6icu_6717GregorianCalendarC2ERKNS_8TimeZoneERKNS_6LocaleER10UErrorCode:
.LFB2996:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN6icu_678CalendarC2ERKNS_8TimeZoneERKNS_6LocaleER10UErrorCode@PLT
	leaq	16+_ZTVN6icu_6717GregorianCalendarE(%rip), %rax
	movsd	.LC3(%rip), %xmm0
	movl	$2299161, 624(%r12)
	movq	%rax, (%r12)
	movl	$1, %eax
	movl	$1582, 640(%r12)
	movw	%ax, 648(%r12)
	movsd	%xmm0, 616(%r12)
	movsd	%xmm0, 632(%r12)
	call	_ZN6icu_678Calendar6getNowEv@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_678Calendar15setTimeInMillisEdR10UErrorCode@PLT
	.cfi_endproc
.LFE2996:
	.size	_ZN6icu_6717GregorianCalendarC2ERKNS_8TimeZoneERKNS_6LocaleER10UErrorCode, .-_ZN6icu_6717GregorianCalendarC2ERKNS_8TimeZoneERKNS_6LocaleER10UErrorCode
	.globl	_ZN6icu_6717GregorianCalendarC1ERKNS_8TimeZoneERKNS_6LocaleER10UErrorCode
	.set	_ZN6icu_6717GregorianCalendarC1ERKNS_8TimeZoneERKNS_6LocaleER10UErrorCode,_ZN6icu_6717GregorianCalendarC2ERKNS_8TimeZoneERKNS_6LocaleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717GregorianCalendarC2EiiiR10UErrorCode
	.type	_ZN6icu_6717GregorianCalendarC2EiiiR10UErrorCode, @function
_ZN6icu_6717GregorianCalendarC2EiiiR10UErrorCode:
.LFB2999:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%esi, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	%r8, -56(%rbp)
	call	_ZN6icu_676Locale10getDefaultEv@PLT
	movq	%rax, %rbx
	call	_ZN6icu_678TimeZone13createDefaultEv@PLT
	movq	-56(%rbp), %rcx
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_678CalendarC2EPNS_8TimeZoneERKNS_6LocaleER10UErrorCode@PLT
	movq	%r12, %rdi
	movl	$1, %edx
	xorl	%esi, %esi
	leaq	16+_ZTVN6icu_6717GregorianCalendarE(%rip), %rax
	movsd	.LC3(%rip), %xmm0
	movl	$2299161, 624(%r12)
	movq	%rax, (%r12)
	movl	$1, %eax
	movw	%ax, 648(%r12)
	movl	$1582, 640(%r12)
	movsd	%xmm0, 616(%r12)
	movsd	%xmm0, 632(%r12)
	call	_ZN6icu_678Calendar3setE19UCalendarDateFieldsi@PLT
	movl	%r15d, %edx
	movq	%r12, %rdi
	movl	$1, %esi
	call	_ZN6icu_678Calendar3setE19UCalendarDateFieldsi@PLT
	movl	%r14d, %edx
	movq	%r12, %rdi
	movl	$2, %esi
	call	_ZN6icu_678Calendar3setE19UCalendarDateFieldsi@PLT
	addq	$24, %rsp
	movl	%r13d, %edx
	movq	%r12, %rdi
	popq	%rbx
	movl	$5, %esi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_678Calendar3setE19UCalendarDateFieldsi@PLT
	.cfi_endproc
.LFE2999:
	.size	_ZN6icu_6717GregorianCalendarC2EiiiR10UErrorCode, .-_ZN6icu_6717GregorianCalendarC2EiiiR10UErrorCode
	.globl	_ZN6icu_6717GregorianCalendarC1EiiiR10UErrorCode
	.set	_ZN6icu_6717GregorianCalendarC1EiiiR10UErrorCode,_ZN6icu_6717GregorianCalendarC2EiiiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717GregorianCalendarC2EiiiiiR10UErrorCode
	.type	_ZN6icu_6717GregorianCalendarC2EiiiiiR10UErrorCode, @function
_ZN6icu_6717GregorianCalendarC2EiiiiiR10UErrorCode:
.LFB3002:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movl	%r8d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%r9d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$40, %rsp
	movq	16(%rbp), %rcx
	movl	%esi, -68(%rbp)
	movq	%rcx, -64(%rbp)
	call	_ZN6icu_676Locale10getDefaultEv@PLT
	movq	%rax, -56(%rbp)
	call	_ZN6icu_678TimeZone13createDefaultEv@PLT
	movq	-64(%rbp), %rcx
	movq	-56(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_678CalendarC2EPNS_8TimeZoneERKNS_6LocaleER10UErrorCode@PLT
	movq	%r12, %rdi
	movl	$1, %edx
	xorl	%esi, %esi
	leaq	16+_ZTVN6icu_6717GregorianCalendarE(%rip), %rax
	movsd	.LC3(%rip), %xmm0
	movl	$2299161, 624(%r12)
	movq	%rax, (%r12)
	movl	$1, %eax
	movw	%ax, 648(%r12)
	movl	$1582, 640(%r12)
	movsd	%xmm0, 616(%r12)
	movsd	%xmm0, 632(%r12)
	call	_ZN6icu_678Calendar3setE19UCalendarDateFieldsi@PLT
	movl	-68(%rbp), %r10d
	movq	%r12, %rdi
	movl	$1, %esi
	movl	%r10d, %edx
	call	_ZN6icu_678Calendar3setE19UCalendarDateFieldsi@PLT
	movl	%ebx, %edx
	movq	%r12, %rdi
	movl	$2, %esi
	call	_ZN6icu_678Calendar3setE19UCalendarDateFieldsi@PLT
	movl	%r15d, %edx
	movq	%r12, %rdi
	movl	$5, %esi
	call	_ZN6icu_678Calendar3setE19UCalendarDateFieldsi@PLT
	movl	%r14d, %edx
	movq	%r12, %rdi
	movl	$11, %esi
	call	_ZN6icu_678Calendar3setE19UCalendarDateFieldsi@PLT
	addq	$40, %rsp
	movl	%r13d, %edx
	movq	%r12, %rdi
	popq	%rbx
	movl	$12, %esi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_678Calendar3setE19UCalendarDateFieldsi@PLT
	.cfi_endproc
.LFE3002:
	.size	_ZN6icu_6717GregorianCalendarC2EiiiiiR10UErrorCode, .-_ZN6icu_6717GregorianCalendarC2EiiiiiR10UErrorCode
	.globl	_ZN6icu_6717GregorianCalendarC1EiiiiiR10UErrorCode
	.set	_ZN6icu_6717GregorianCalendarC1EiiiiiR10UErrorCode,_ZN6icu_6717GregorianCalendarC2EiiiiiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717GregorianCalendarC2EiiiiiiR10UErrorCode
	.type	_ZN6icu_6717GregorianCalendarC2EiiiiiiR10UErrorCode, @function
_ZN6icu_6717GregorianCalendarC2EiiiiiiR10UErrorCode:
.LFB3005:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movl	%r8d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%r9d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$40, %rsp
	movl	16(%rbp), %r8d
	movq	24(%rbp), %rcx
	movl	%esi, -72(%rbp)
	movl	%r8d, -68(%rbp)
	movq	%rcx, -64(%rbp)
	call	_ZN6icu_676Locale10getDefaultEv@PLT
	movq	%rax, -56(%rbp)
	call	_ZN6icu_678TimeZone13createDefaultEv@PLT
	movq	-64(%rbp), %rcx
	movq	-56(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_678CalendarC2EPNS_8TimeZoneERKNS_6LocaleER10UErrorCode@PLT
	movq	%r12, %rdi
	movl	$1, %edx
	xorl	%esi, %esi
	leaq	16+_ZTVN6icu_6717GregorianCalendarE(%rip), %rax
	movsd	.LC3(%rip), %xmm0
	movl	$2299161, 624(%r12)
	movq	%rax, (%r12)
	movl	$1, %eax
	movw	%ax, 648(%r12)
	movl	$1582, 640(%r12)
	movsd	%xmm0, 616(%r12)
	movsd	%xmm0, 632(%r12)
	call	_ZN6icu_678Calendar3setE19UCalendarDateFieldsi@PLT
	movl	-72(%rbp), %r10d
	movq	%r12, %rdi
	movl	$1, %esi
	movl	%r10d, %edx
	call	_ZN6icu_678Calendar3setE19UCalendarDateFieldsi@PLT
	movl	%ebx, %edx
	movq	%r12, %rdi
	movl	$2, %esi
	call	_ZN6icu_678Calendar3setE19UCalendarDateFieldsi@PLT
	movl	%r15d, %edx
	movq	%r12, %rdi
	movl	$5, %esi
	call	_ZN6icu_678Calendar3setE19UCalendarDateFieldsi@PLT
	movl	%r14d, %edx
	movq	%r12, %rdi
	movl	$11, %esi
	call	_ZN6icu_678Calendar3setE19UCalendarDateFieldsi@PLT
	movl	%r13d, %edx
	movq	%r12, %rdi
	movl	$12, %esi
	call	_ZN6icu_678Calendar3setE19UCalendarDateFieldsi@PLT
	movl	-68(%rbp), %r8d
	addq	$40, %rsp
	movq	%r12, %rdi
	popq	%rbx
	movl	$13, %esi
	popq	%r12
	movl	%r8d, %edx
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_678Calendar3setE19UCalendarDateFieldsi@PLT
	.cfi_endproc
.LFE3005:
	.size	_ZN6icu_6717GregorianCalendarC2EiiiiiiR10UErrorCode, .-_ZN6icu_6717GregorianCalendarC2EiiiiiiR10UErrorCode
	.globl	_ZN6icu_6717GregorianCalendarC1EiiiiiiR10UErrorCode
	.set	_ZN6icu_6717GregorianCalendarC1EiiiiiiR10UErrorCode,_ZN6icu_6717GregorianCalendarC2EiiiiiiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717GregorianCalendarC2ERKS0_
	.type	_ZN6icu_6717GregorianCalendarC2ERKS0_, @function
_ZN6icu_6717GregorianCalendarC2ERKS0_:
.LFB3012:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZN6icu_678CalendarC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6717GregorianCalendarE(%rip), %rax
	movsd	616(%r12), %xmm0
	movq	%rax, (%rbx)
	movl	624(%r12), %eax
	movsd	%xmm0, 616(%rbx)
	movsd	632(%r12), %xmm0
	movl	%eax, 624(%rbx)
	movl	640(%r12), %eax
	movsd	%xmm0, 632(%rbx)
	movl	%eax, 640(%rbx)
	movzwl	648(%r12), %eax
	movw	%ax, 648(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3012:
	.size	_ZN6icu_6717GregorianCalendarC2ERKS0_, .-_ZN6icu_6717GregorianCalendarC2ERKS0_
	.globl	_ZN6icu_6717GregorianCalendarC1ERKS0_
	.set	_ZN6icu_6717GregorianCalendarC1ERKS0_,_ZN6icu_6717GregorianCalendarC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717GregorianCalendaraSERKS0_
	.type	_ZN6icu_6717GregorianCalendaraSERKS0_, @function
_ZN6icu_6717GregorianCalendaraSERKS0_:
.LFB3015:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpq	%rsi, %rdi
	je	.L161
	movq	%rsi, %rbx
	call	_ZN6icu_678CalendaraSERKS0_@PLT
	movsd	616(%rbx), %xmm0
	movl	640(%rbx), %eax
	movsd	%xmm0, 616(%r12)
	movsd	632(%rbx), %xmm0
	movl	%eax, 640(%r12)
	movl	624(%rbx), %eax
	movsd	%xmm0, 632(%r12)
	movl	%eax, 624(%r12)
.L161:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3015:
	.size	_ZN6icu_6717GregorianCalendaraSERKS0_, .-_ZN6icu_6717GregorianCalendaraSERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717GregorianCalendar18setGregorianChangeEdR10UErrorCode
	.type	_ZN6icu_6717GregorianCalendar18setGregorianChangeEdR10UErrorCode, @function
_ZN6icu_6717GregorianCalendar18setGregorianChangeEdR10UErrorCode:
.LFB3017:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	(%rsi), %ecx
	movsd	%xmm0, -40(%rbp)
	testl	%ecx, %ecx
	jle	.L180
.L163:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L180:
	.cfi_restore_state
	movq	%rdi, %rbx
	movq	%rsi, %r13
	divsd	.LC8(%rip), %xmm0
	call	uprv_floor_67@PLT
	movapd	%xmm0, %xmm2
	movsd	%xmm0, -48(%rbp)
	movsd	.LC6(%rip), %xmm0
	comisd	%xmm2, %xmm0
	jnb	.L181
	comisd	.LC7(%rip), %xmm2
	jb	.L178
	movq	.LC7(%rip), %rax
	movsd	.LC10(%rip), %xmm0
	movq	%rax, -48(%rbp)
	movsd	%xmm0, 632(%rbx)
	movsd	%xmm0, 616(%rbx)
.L168:
	movq	%rbx, %rdi
	call	_ZNK6icu_678Calendar11getTimeZoneEv@PLT
	movl	$656, %edi
	movq	%rax, %r14
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L171
	call	_ZN6icu_676Locale10getDefaultEv@PLT
	movq	%r14, %rsi
	movq	%r13, %rcx
	movq	%r12, %rdi
	movq	%rax, %rdx
	leaq	16+_ZTVN6icu_6717GregorianCalendarE(%rip), %r14
	call	_ZN6icu_678CalendarC2ERKNS_8TimeZoneERKNS_6LocaleER10UErrorCode@PLT
	movl	$1, %eax
	movq	%r14, (%r12)
	movsd	.LC3(%rip), %xmm0
	movw	%ax, 648(%r12)
	movl	$2299161, 624(%r12)
	movl	$1582, 640(%r12)
	movsd	%xmm0, 616(%r12)
	movsd	%xmm0, 632(%r12)
	call	_ZN6icu_678Calendar6getNowEv@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_678Calendar15setTimeInMillisEdR10UErrorCode@PLT
	movl	0(%r13), %edx
	testl	%edx, %edx
	jg	.L163
	movsd	-40(%rbp), %xmm0
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_678Calendar15setTimeInMillisEdR10UErrorCode@PLT
	movq	%r13, %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_678Calendar3getE19UCalendarDateFieldsR10UErrorCode@PLT
	xorl	%esi, %esi
	movq	%r13, %rdx
	movq	%r12, %rdi
	movl	%eax, 640(%rbx)
	call	_ZNK6icu_678Calendar3getE19UCalendarDateFieldsR10UErrorCode@PLT
	testl	%eax, %eax
	jne	.L173
	movl	$1, %eax
	subl	640(%rbx), %eax
	movl	%eax, 640(%rbx)
.L173:
	cvttsd2sil	-48(%rbp), %eax
	leaq	_ZN6icu_6717GregorianCalendarD0Ev(%rip), %rdx
	movl	%eax, 624(%rbx)
	movq	(%r12), %rax
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L174
	movq	%r14, (%r12)
	movq	%r12, %rdi
	call	_ZN6icu_678CalendarD2Ev@PLT
	addq	$16, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L181:
	.cfi_restore_state
	movsd	.LC9(%rip), %xmm1
	movsd	%xmm0, -48(%rbp)
	movsd	%xmm1, 632(%rbx)
	movsd	%xmm1, 616(%rbx)
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L178:
	movsd	.LC8(%rip), %xmm0
	mulsd	-48(%rbp), %xmm0
	movsd	-40(%rbp), %xmm5
	movsd	%xmm5, 616(%rbx)
	movsd	%xmm0, 632(%rbx)
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L174:
	addq	$16, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
.L171:
	.cfi_restore_state
	movl	$7, 0(%r13)
	jmp	.L163
	.cfi_endproc
.LFE3017:
	.size	_ZN6icu_6717GregorianCalendar18setGregorianChangeEdR10UErrorCode, .-_ZN6icu_6717GregorianCalendar18setGregorianChangeEdR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717GregorianCalendar18getGregorianChangeEv
	.type	_ZNK6icu_6717GregorianCalendar18getGregorianChangeEv, @function
_ZNK6icu_6717GregorianCalendar18getGregorianChangeEv:
.LFB3019:
	.cfi_startproc
	endbr64
	movsd	616(%rdi), %xmm0
	ret
	.cfi_endproc
.LFE3019:
	.size	_ZNK6icu_6717GregorianCalendar18getGregorianChangeEv, .-_ZNK6icu_6717GregorianCalendar18getGregorianChangeEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717GregorianCalendar10isLeapYearEi
	.type	_ZNK6icu_6717GregorianCalendar10isLeapYearEi, @function
_ZNK6icu_6717GregorianCalendar10isLeapYearEi:
.LFB3020:
	.cfi_startproc
	endbr64
	movl	%esi, %edx
	andl	$3, %edx
	cmpl	%esi, 640(%rdi)
	jg	.L184
	xorl	%eax, %eax
	testl	%edx, %edx
	jne	.L183
	imull	$-1030792151, %esi, %esi
	movl	$1, %eax
	addl	$85899344, %esi
	movl	%esi, %edx
	rorl	$2, %edx
	cmpl	$42949672, %edx
	jbe	.L189
.L183:
	ret
	.p2align 4,,10
	.p2align 3
.L184:
	testl	%edx, %edx
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L189:
	rorl	$4, %esi
	cmpl	$10737418, %esi
	setbe	%al
	ret
	.cfi_endproc
.LFE3020:
	.size	_ZNK6icu_6717GregorianCalendar10isLeapYearEi, .-_ZNK6icu_6717GregorianCalendar10isLeapYearEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717GregorianCalendar10yearLengthEv
	.type	_ZNK6icu_6717GregorianCalendar10yearLengthEv, @function
_ZNK6icu_6717GregorianCalendar10yearLengthEv:
.LFB3028:
	.cfi_startproc
	endbr64
	movl	16(%rdi), %eax
	movl	%eax, %edx
	andl	$3, %edx
	cmpl	%eax, 640(%rdi)
	jg	.L191
	movl	$365, %r8d
	testl	%edx, %edx
	jne	.L190
	imull	$-1030792151, %eax, %eax
	movl	$366, %r8d
	addl	$85899344, %eax
	movl	%eax, %edx
	rorl	$2, %edx
	cmpl	$42949672, %edx
	jbe	.L197
.L190:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L191:
	xorl	%r8d, %r8d
	testl	%edx, %edx
	sete	%r8b
	addl	$365, %r8d
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L197:
	rorl	$4, %eax
	xorl	%r8d, %r8d
	cmpl	$10737419, %eax
	setb	%r8b
	addl	$365, %r8d
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE3028:
	.size	_ZNK6icu_6717GregorianCalendar10yearLengthEv, .-_ZNK6icu_6717GregorianCalendar10yearLengthEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717GregorianCalendar13pinDayOfMonthEv
	.type	_ZN6icu_6717GregorianCalendar13pinDayOfMonthEv, @function
_ZN6icu_6717GregorianCalendar13pinDayOfMonthEv:
.LFB3029:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZNK6icu_6717GregorianCalendar11monthLengthEi(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$16, %rsp
	movslq	20(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	408(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L199
	movq	272(%rax), %rax
	leaq	_ZNK6icu_6717GregorianCalendar20handleGetMonthLengthEii(%rip), %rdx
	movl	88(%rdi), %r13d
	cmpq	%rdx, %rax
	jne	.L200
	movl	%esi, -28(%rbp)
	cmpl	$11, %esi
	ja	.L210
	movl	%r13d, %eax
	andl	$3, %eax
	cmpl	%r13d, 640(%r12)
	jle	.L211
.L202:
	testl	%eax, %eax
	jne	.L203
.L204:
	leaq	_ZL16kLeapMonthLength(%rip), %rax
	movsbl	(%rax,%rsi), %edx
.L206:
	cmpl	%edx, 32(%r12)
	jg	.L212
.L198:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L213
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L211:
	.cfi_restore_state
	testl	%eax, %eax
	jne	.L203
	imull	$-1030792151, %r13d, %r13d
	addl	$85899344, %r13d
	movl	%r13d, %eax
	rorl	$2, %eax
	cmpl	$42949672, %eax
	ja	.L204
	rorl	$4, %r13d
	cmpl	$10737418, %r13d
	jbe	.L204
	.p2align 4,,10
	.p2align 3
.L203:
	leaq	_ZL12kMonthLength(%rip), %rax
	movsbl	(%rax,%rsi), %edx
	cmpl	%edx, 32(%r12)
	jle	.L198
.L212:
	movl	$5, %esi
	movq	%r12, %rdi
	call	_ZN6icu_678Calendar3setE19UCalendarDateFieldsi@PLT
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L210:
	pxor	%xmm0, %xmm0
	leaq	-28(%rbp), %r8
	movl	$12, %edi
	cvtsi2sdl	%esi, %xmm0
	movq	%r8, %rsi
	call	_ZN6icu_679ClockMath11floorDivideEdiRi@PLT
	movslq	-28(%rbp), %rsi
	addl	%eax, %r13d
	movl	%r13d, %eax
	andl	$3, %eax
	cmpl	%r13d, 640(%r12)
	jg	.L202
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L199:
	call	*%rdx
	movl	%eax, %edx
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L200:
	movl	%esi, %edx
	movl	%r13d, %esi
	call	*%rax
	movl	%eax, %edx
	jmp	.L206
.L213:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3029:
	.size	_ZN6icu_6717GregorianCalendar13pinDayOfMonthEv, .-_ZN6icu_6717GregorianCalendar13pinDayOfMonthEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717GregorianCalendar14validateFieldsEv
	.type	_ZNK6icu_6717GregorianCalendar14validateFieldsEv, @function
_ZNK6icu_6717GregorianCalendar14validateFieldsEv:
.LFB3030:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	xorl	%ebx, %ebx
	subq	$8, %rsp
	.p2align 4,,10
	.p2align 3
.L217:
	leal	-5(%rbx), %eax
	cmpl	$1, %eax
	ja	.L215
.L218:
	addq	$1, %rbx
	cmpq	$23, %rbx
	jne	.L217
	movl	$5, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_678Calendar5isSetE19UCalendarDateFields@PLT
	testb	%al, %al
	je	.L225
	movq	(%r12), %rax
	movl	32(%r12), %ebx
	movl	$5, %esi
	movq	%r12, %rdi
	call	*112(%rax)
	cmpl	%ebx, %eax
	jg	.L221
	movq	(%r12), %rax
	movl	20(%r12), %esi
	movq	%r12, %rdi
	call	*408(%rax)
	cmpl	%ebx, %eax
	jl	.L221
.L225:
	movl	$6, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_678Calendar5isSetE19UCalendarDateFields@PLT
	testb	%al, %al
	je	.L224
	movl	36(%r12), %edx
	testl	%edx, %edx
	jle	.L221
	movl	16(%r12), %eax
	movl	%eax, %esi
	andl	$3, %esi
	cmpl	640(%r12), %eax
	jl	.L226
	xorl	%ecx, %ecx
	testl	%esi, %esi
	jne	.L228
	imull	$-1030792151, %eax, %eax
	movl	$1, %ecx
	addl	$85899344, %eax
	movl	%eax, %esi
	rorl	$2, %esi
	cmpl	$42949672, %esi
	ja	.L228
	rorl	$4, %eax
	cmpl	$10737418, %eax
	setbe	%cl
	jmp	.L228
.L215:
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_678Calendar5isSetE19UCalendarDateFields@PLT
	testb	%al, %al
	je	.L218
	movq	(%r12), %rax
	movl	12(%r12,%rbx,4), %r13d
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	*112(%rax)
	cmpl	%r13d, %eax
	jg	.L221
	movq	(%r12), %rax
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	*128(%rax)
	cmpl	%r13d, %eax
	jge	.L218
	.p2align 4,,10
	.p2align 3
.L221:
	xorl	%eax, %eax
.L214:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L226:
	.cfi_restore_state
	testl	%esi, %esi
	sete	%cl
.L228:
	cmpb	$1, %cl
	sbbl	%eax, %eax
	addl	$366, %eax
	cmpl	%eax, %edx
	jg	.L221
.L224:
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_678Calendar5isSetE19UCalendarDateFields@PLT
	movl	%eax, %r8d
	movl	$1, %eax
	testb	%r8b, %r8b
	je	.L214
	movl	44(%r12), %eax
	testl	%eax, %eax
	setne	%al
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3030:
	.size	_ZNK6icu_6717GregorianCalendar14validateFieldsEv, .-_ZNK6icu_6717GregorianCalendar14validateFieldsEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717GregorianCalendar11boundsCheckEi19UCalendarDateFields
	.type	_ZNK6icu_6717GregorianCalendar11boundsCheckEi19UCalendarDateFields, @function
_ZNK6icu_6717GregorianCalendar11boundsCheckEi19UCalendarDateFields:
.LFB3031:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%esi, %ebx
	movl	%edx, %esi
	subq	$8, %rsp
	movq	(%rdi), %rax
	call	*112(%rax)
	movl	%eax, %r8d
	xorl	%eax, %eax
	cmpl	%ebx, %r8d
	jg	.L237
	movq	(%r12), %rax
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	*128(%rax)
	cmpl	%ebx, %eax
	setge	%al
.L237:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3031:
	.size	_ZNK6icu_6717GregorianCalendar11boundsCheckEi19UCalendarDateFields, .-_ZNK6icu_6717GregorianCalendar11boundsCheckEi19UCalendarDateFields
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717GregorianCalendar22computeJulianDayOfYearEaiRa
	.type	_ZN6icu_6717GregorianCalendar22computeJulianDayOfYearEaiRa, @function
_ZN6icu_6717GregorianCalendar22computeJulianDayOfYearEaiRa:
.LFB3033:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%edi, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movslq	%esi, %r12
	movl	$4, %esi
	pushq	%rbx
	.cfi_offset 3, -48
	leal	-1(%r12), %ebx
	movl	%ebx, %edi
	subq	$16, %rsp
	testb	$3, %r12b
	sete	(%rdx)
	call	_ZN6icu_679ClockMath11floorDivideEii@PLT
	pxor	%xmm0, %xmm0
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm0
	mulsd	.LC11(%rip), %xmm0
	cvtsi2sdl	%eax, %xmm1
	addsd	%xmm1, %xmm0
	addsd	.LC12(%rip), %xmm0
	testb	%r14b, %r14b
	je	.L242
	movzbl	0(%r13), %eax
	testb	%al, %al
	jne	.L250
.L244:
	movb	%al, 0(%r13)
	subq	$1, %r12
	movl	$400, %esi
	movq	%r12, %rdi
	movsd	%xmm0, -40(%rbp)
	call	_ZN6icu_679ClockMath11floorDivideEll@PLT
	movl	$100, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN6icu_679ClockMath11floorDivideEll@PLT
	addl	$2, %ebx
	pxor	%xmm1, %xmm1
	movsd	-40(%rbp), %xmm0
	subl	%eax, %ebx
	cvtsi2sdl	%ebx, %xmm1
	addsd	%xmm1, %xmm0
.L242:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L250:
	.cfi_restore_state
	imull	$-1030792151, %r12d, %edx
	movl	$1, %eax
	addl	$85899344, %edx
	movl	%edx, %ecx
	rorl	$2, %ecx
	cmpl	$42949672, %ecx
	ja	.L244
	rorl	$4, %edx
	cmpl	$10737418, %edx
	setbe	%al
	jmp	.L244
	.cfi_endproc
.LFE3033:
	.size	_ZN6icu_6717GregorianCalendar22computeJulianDayOfYearEaiRa, .-_ZN6icu_6717GregorianCalendar22computeJulianDayOfYearEaiRa
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717GregorianCalendar17millisToJulianDayEd
	.type	_ZN6icu_6717GregorianCalendar17millisToJulianDayEd, @function
_ZN6icu_6717GregorianCalendar17millisToJulianDayEd:
.LFB3034:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	divsd	.LC8(%rip), %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	uprv_floor_67@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	addsd	.LC13(%rip), %xmm0
	ret
	.cfi_endproc
.LFE3034:
	.size	_ZN6icu_6717GregorianCalendar17millisToJulianDayEd, .-_ZN6icu_6717GregorianCalendar17millisToJulianDayEd
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717GregorianCalendar17julianDayToMillisEd
	.type	_ZN6icu_6717GregorianCalendar17julianDayToMillisEd, @function
_ZN6icu_6717GregorianCalendar17julianDayToMillisEd:
.LFB3035:
	.cfi_startproc
	endbr64
	subsd	.LC13(%rip), %xmm0
	mulsd	.LC8(%rip), %xmm0
	ret
	.cfi_endproc
.LFE3035:
	.size	_ZN6icu_6717GregorianCalendar17julianDayToMillisEd, .-_ZN6icu_6717GregorianCalendar17julianDayToMillisEd
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717GregorianCalendar14aggregateStampEii
	.type	_ZN6icu_6717GregorianCalendar14aggregateStampEii, @function
_ZN6icu_6717GregorianCalendar14aggregateStampEii:
.LFB3036:
	.cfi_startproc
	endbr64
	movl	%esi, %edi
	testl	%esi, %esi
	je	.L255
	testl	%edx, %edx
	jne	.L262
.L255:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L262:
	movl	%edx, %esi
	jmp	uprv_max_67@PLT
	.cfi_endproc
.LFE3036:
	.size	_ZN6icu_6717GregorianCalendar14aggregateStampEii, .-_ZN6icu_6717GregorianCalendar14aggregateStampEii
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717GregorianCalendar16getActualMinimumENS_8Calendar11EDateFieldsE
	.type	_ZNK6icu_6717GregorianCalendar16getActualMinimumENS_8Calendar11EDateFieldsE, @function
_ZNK6icu_6717GregorianCalendar16getActualMinimumENS_8Calendar11EDateFieldsE:
.LFB3039:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*112(%rax)
	.cfi_endproc
.LFE3039:
	.size	_ZNK6icu_6717GregorianCalendar16getActualMinimumENS_8Calendar11EDateFieldsE, .-_ZNK6icu_6717GregorianCalendar16getActualMinimumENS_8Calendar11EDateFieldsE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717GregorianCalendar16getActualMinimumENS_8Calendar11EDateFieldsER10UErrorCode
	.type	_ZNK6icu_6717GregorianCalendar16getActualMinimumENS_8Calendar11EDateFieldsER10UErrorCode, @function
_ZNK6icu_6717GregorianCalendar16getActualMinimumENS_8Calendar11EDateFieldsER10UErrorCode:
.LFB3040:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*112(%rax)
	.cfi_endproc
.LFE3040:
	.size	_ZNK6icu_6717GregorianCalendar16getActualMinimumENS_8Calendar11EDateFieldsER10UErrorCode, .-_ZNK6icu_6717GregorianCalendar16getActualMinimumENS_8Calendar11EDateFieldsER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717GregorianCalendar14isEquivalentToERKNS_8CalendarE
	.type	_ZNK6icu_6717GregorianCalendar14isEquivalentToERKNS_8CalendarE, @function
_ZNK6icu_6717GregorianCalendar14isEquivalentToERKNS_8CalendarE:
.LFB3016:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_ZNK6icu_678Calendar14isEquivalentToERKS0_@PLT
	testb	%al, %al
	je	.L265
	movsd	616(%r12), %xmm0
	movl	$0, %edx
	ucomisd	616(%rbx), %xmm0
	setnp	%al
	cmovne	%edx, %eax
.L265:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3016:
	.size	_ZNK6icu_6717GregorianCalendar14isEquivalentToERKNS_8CalendarE, .-_ZNK6icu_6717GregorianCalendar14isEquivalentToERKNS_8CalendarE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717GregorianCalendar22handleComputeJulianDayE19UCalendarDateFields
	.type	_ZN6icu_6717GregorianCalendar22handleComputeJulianDayE19UCalendarDateFields, @function
_ZN6icu_6717GregorianCalendar22handleComputeJulianDayE19UCalendarDateFields:
.LFB3021:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%esi, %ebx
	movb	$0, 649(%rdi)
	call	_ZN6icu_678Calendar22handleComputeJulianDayE19UCalendarDateFields@PLT
	movl	624(%r12), %edx
	movl	%eax, %r13d
	cmpl	$3, %ebx
	je	.L281
.L273:
	movzbl	648(%r12), %eax
	cmpb	$1, %al
	sete	%cl
	cmpl	%edx, %r13d
	setge	%dl
	cmpb	%dl, %cl
	jne	.L282
	testb	%al, %al
	je	.L271
.L284:
	movslq	88(%r12), %rdi
	cmpl	%edi, 640(%r12)
	je	.L283
.L271:
	popq	%rbx
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L282:
	.cfi_restore_state
	movb	$1, 649(%r12)
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	_ZN6icu_678Calendar22handleComputeJulianDayE19UCalendarDateFields@PLT
	movl	%eax, %r13d
	movzbl	648(%r12), %eax
	testb	%al, %al
	je	.L271
	jmp	.L284
	.p2align 4,,10
	.p2align 3
.L281:
	movl	88(%r12), %eax
	cmpl	%eax, 640(%r12)
	jne	.L273
	cmpl	%edx, %r13d
	jl	.L273
	movb	$1, 649(%r12)
	movq	%r12, %rdi
	popq	%rbx
	movl	$3, %esi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_678Calendar22handleComputeJulianDayE19UCalendarDateFields@PLT
	.p2align 4,,10
	.p2align 3
.L283:
	.cfi_restore_state
	leaq	-1(%rdi), %r12
	movl	$400, %esi
	movq	%r12, %rdi
	call	_ZN6icu_679ClockMath11floorDivideEll@PLT
	movl	$100, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN6icu_679ClockMath11floorDivideEll@PLT
	cmpl	$6, %ebx
	je	.L285
	leal	14(%r13), %eax
	cmpl	$4, %ebx
	cmove	%eax, %r13d
	jmp	.L271
	.p2align 4,,10
	.p2align 3
.L285:
	addl	$2, %r14d
	subl	%eax, %r14d
	subl	%r14d, %r13d
	jmp	.L271
	.cfi_endproc
.LFE3021:
	.size	_ZN6icu_6717GregorianCalendar22handleComputeJulianDayE19UCalendarDateFields, .-_ZN6icu_6717GregorianCalendar22handleComputeJulianDayE19UCalendarDateFields
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717GregorianCalendar4rollENS_8Calendar11EDateFieldsEiR10UErrorCode
	.type	_ZN6icu_6717GregorianCalendar4rollENS_8Calendar11EDateFieldsEiR10UErrorCode, @function
_ZN6icu_6717GregorianCalendar4rollENS_8Calendar11EDateFieldsEiR10UErrorCode:
.LFB3037:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%esi, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	leaq	_ZN6icu_6717GregorianCalendar4rollE19UCalendarDateFieldsiR10UErrorCode(%rip), %rdx
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rax
	movq	72(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L287
	testl	%r13d, %r13d
	je	.L286
	movl	(%rcx), %edx
	testl	%edx, %edx
	jle	.L358
.L286:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L358:
	.cfi_restore_state
	movq	%rcx, %rdx
	movl	$19, %esi
	call	_ZNK6icu_678Calendar3getE19UCalendarDateFieldsR10UErrorCode@PLT
	cmpl	640(%r12), %eax
	je	.L359
	cmpl	$4, %r15d
	je	.L323
.L290:
	cmpl	$5, %r15d
	je	.L324
	cmpl	$3, %r15d
	je	.L360
	movq	%r14, %rcx
	movl	%r13d, %edx
	movl	%r15d, %esi
.L356:
	addq	$40, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_678Calendar4rollE19UCalendarDateFieldsiR10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L287:
	.cfi_restore_state
	addq	$40, %rsp
	movl	%r13d, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L359:
	.cfi_restore_state
	leal	-4(%r15), %eax
	cmpl	$1, %eax
	ja	.L290
	movq	(%r12), %rax
	movl	20(%r12), %esi
	movq	%r12, %rdi
	call	*408(%rax)
	movl	32(%r12), %ebx
	pxor	%xmm0, %xmm0
	movsd	224(%r12), %xmm2
	movsd	616(%r12), %xmm4
	movl	%eax, %esi
	leal	-10(%rbx), %eax
	movapd	%xmm2, %xmm3
	comisd	%xmm4, %xmm2
	cmovnb	%eax, %ebx
	leal	-1(%rbx), %eax
	cvtsi2sdl	%eax, %xmm0
	mulsd	.LC8(%rip), %xmm0
	subsd	%xmm0, %xmm3
	comisd	%xmm3, %xmm4
	jbe	.L293
	leal	-10(%rsi), %r9d
	pxor	%xmm1, %xmm1
	movapd	%xmm3, %xmm0
	cvtsi2sdl	%r9d, %xmm1
	mulsd	.LC8(%rip), %xmm1
	addsd	%xmm1, %xmm0
	comisd	%xmm4, %xmm0
	jb	.L293
	cmpl	$4, %r15d
	jne	.L361
	movl	40(%r12), %r15d
	movq	%r12, %rdi
	movl	%r9d, -76(%rbp)
	movl	%esi, -64(%rbp)
	movsd	%xmm3, -72(%rbp)
	call	_ZNK6icu_678Calendar17getFirstDayOfWeekEv@PLT
	subl	%eax, %r15d
	leal	7(%r15), %eax
	cmovs	%eax, %r15d
	movl	%r15d, %ecx
	subl	%ebx, %ecx
	addl	$1, %ecx
	movslq	%ecx, %rax
	movl	%ecx, %edi
	imulq	$-1840700269, %rax, %rax
	sarl	$31, %edi
	shrq	$32, %rax
	addl	%ecx, %eax
	sarl	$2, %eax
	subl	%edi, %eax
	movq	%r12, %rdi
	movl	%eax, %r8d
	leal	0(,%rax,8), %eax
	subl	%r8d, %eax
	subl	%eax, %ecx
	movl	%ecx, %r8d
	leal	7(%rcx), %eax
	cmovs	%eax, %r8d
	movl	%r8d, -56(%rbp)
	movl	%r8d, -80(%rbp)
	call	_ZNK6icu_678Calendar25getMinimalDaysInFirstWeekEv@PLT
	movl	-56(%rbp), %r8d
	movl	$7, %ecx
	movl	-64(%rbp), %esi
	movzbl	%al, %eax
	movsd	-72(%rbp), %xmm3
	movl	-76(%rbp), %r9d
	subl	%r8d, %ecx
	cmpl	%eax, %ecx
	jge	.L320
	movl	$8, %ecx
	subl	%r8d, %ecx
.L321:
	movl	%r9d, %eax
	pxor	%xmm0, %xmm0
	subl	%ebx, %eax
	addl	%eax, %r15d
	movslq	%r15d, %rax
	movl	%r15d, %edi
	subl	%r15d, %esi
	imulq	$-1840700269, %rax, %rax
	sarl	$31, %edi
	shrq	$32, %rax
	addl	%r15d, %eax
	sarl	$2, %eax
	subl	%edi, %eax
	leal	0(,%rax,8), %edi
	subl	%eax, %edi
	leal	(%rbx,%r13,8), %eax
	subl	%r13d, %eax
	leal	-3(%rsi,%rdi), %esi
	subl	%ecx, %eax
	subl	%ecx, %esi
	cltd
	idivl	%esi
	movl	$1, %eax
	addl	%edx, %esi
	testl	%edx, %edx
	cmovns	%edx, %esi
	addl	%esi, %ecx
	testl	%ecx, %ecx
	cmovle	%eax, %ecx
	cmpl	%ecx, %r9d
	cmovle	%r9d, %ecx
	subl	$1, %ecx
	cvtsi2sdl	%ecx, %xmm0
	mulsd	.LC8(%rip), %xmm0
.L357:
	addsd	%xmm3, %xmm0
	addq	$40, %rsp
	movq	%r14, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_678Calendar15setTimeInMillisEdR10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L293:
	.cfi_restore_state
	cmpl	$4, %r15d
	jne	.L324
.L323:
	movq	%r14, %rcx
	movl	%r13d, %edx
	movl	$4, %esi
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L360:
	movq	%r14, %rdx
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_678Calendar3getE19UCalendarDateFieldsR10UErrorCode@PLT
	movq	%r14, %rdx
	movl	$17, %esi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZNK6icu_678Calendar3getE19UCalendarDateFieldsR10UErrorCode@PLT
	movl	36(%r12), %r14d
	movl	%eax, %r15d
	movl	20(%r12), %eax
	testl	%eax, %eax
	je	.L362
	cmpl	$1, %ebx
	je	.L363
.L297:
	addl	%ebx, %r13d
	leal	-1(%r13), %eax
	cmpl	$51, %eax
	jbe	.L308
	movq	(%r12), %rax
	leaq	_ZNK6icu_6717GregorianCalendar19handleGetYearLengthEi(%rip), %rdx
	movq	280(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L309
	movl	%r15d, %eax
	andl	$3, %eax
	cmpl	640(%r12), %r15d
	jl	.L310
	movl	$365, %ebx
	testl	%eax, %eax
	jne	.L313
	imull	$-1030792151, %r15d, %eax
	movl	$366, %ebx
	addl	$85899344, %eax
	movl	%eax, %edx
	rorl	$2, %edx
	cmpl	$42949672, %edx
	ja	.L313
	rorl	$4, %eax
	xorl	%ebx, %ebx
	cmpl	$10737419, %eax
	setb	%bl
	addl	$365, %ebx
	.p2align 4,,10
	.p2align 3
.L313:
	movl	%ebx, %eax
	movq	%r12, %rdi
	subl	%r14d, %eax
	addl	40(%r12), %eax
	movl	%eax, %r14d
	call	_ZNK6icu_678Calendar17getFirstDayOfWeekEv@PLT
	movq	%r12, %rdi
	subl	%eax, %r14d
	movl	%r14d, %eax
	movslq	%r14d, %r14
	imulq	$-1840700269, %r14, %r14
	cltd
	shrq	$32, %r14
	addl	%eax, %r14d
	sarl	$2, %r14d
	subl	%edx, %r14d
	leal	0(,%r14,8), %edx
	subl	%r14d, %edx
	subl	%edx, %eax
	movl	%eax, %r14d
	leal	7(%rax), %eax
	cmovs	%eax, %r14d
	call	_ZNK6icu_678Calendar25getMinimalDaysInFirstWeekEv@PLT
	movl	$6, %edx
	leal	-7(%rbx), %ecx
	movq	%r12, %rdi
	subl	%r14d, %edx
	movzbl	%al, %eax
	cmpl	%eax, %edx
	cmovge	%ecx, %ebx
	leal	1(%r14), %ecx
	movl	%ebx, %edx
	movl	%ebx, %esi
	call	_ZN6icu_678Calendar10weekNumberEiii@PLT
	movl	%eax, %ecx
	leal	-1(%r13,%rax), %eax
	cltd
	idivl	%ecx
	leal	1(%rdx), %r13d
.L308:
	movl	%r13d, %edx
	movq	%r12, %rdi
	movl	$3, %esi
	call	_ZN6icu_678Calendar3setE19UCalendarDateFieldsi@PLT
	addq	$40, %rsp
	movl	%r15d, %edx
	movq	%r12, %rdi
	popq	%rbx
	movl	$17, %esi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_678Calendar3setE19UCalendarDateFieldsi@PLT
	.p2align 4,,10
	.p2align 3
.L362:
	.cfi_restore_state
	cmpl	$51, %ebx
	jle	.L297
	movq	(%r12), %rax
	leaq	_ZNK6icu_6717GregorianCalendar19handleGetYearLengthEi(%rip), %rdx
	movq	280(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L298
	movl	%r15d, %edx
	andl	$3, %edx
	cmpl	640(%r12), %r15d
	jl	.L299
	movl	$365, %eax
	testl	%edx, %edx
	jne	.L302
	imull	$-1030792151, %r15d, %edx
	movl	$366, %eax
	addl	$85899344, %edx
	movl	%edx, %ecx
	rorl	$2, %ecx
	cmpl	$42949672, %ecx
	ja	.L302
	rorl	$4, %edx
	xorl	%eax, %eax
	cmpl	$10737419, %edx
	setb	%al
	addl	$365, %eax
	.p2align 4,,10
	.p2align 3
.L302:
	addl	%eax, %r14d
	jmp	.L297
.L361:
	pxor	%xmm0, %xmm0
	subsd	%xmm3, %xmm2
	movsd	%xmm3, -56(%rbp)
	cvtsi2sdl	%r13d, %xmm0
	mulsd	.LC8(%rip), %xmm0
	movsd	%xmm1, -64(%rbp)
	addsd	%xmm2, %xmm0
	call	uprv_fmod_67@PLT
	pxor	%xmm2, %xmm2
	movsd	-56(%rbp), %xmm3
	comisd	%xmm0, %xmm2
	jbe	.L357
	movsd	-64(%rbp), %xmm1
	addsd	%xmm1, %xmm0
	jmp	.L357
	.p2align 4,,10
	.p2align 3
.L324:
	movq	%r14, %rcx
	movl	%r13d, %edx
	movl	$5, %esi
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L320:
	movl	$1, %ecx
	subl	%r8d, %ecx
	jmp	.L321
	.p2align 4,,10
	.p2align 3
.L310:
	xorl	%ebx, %ebx
	testl	%eax, %eax
	sete	%bl
	addl	$365, %ebx
	jmp	.L313
.L363:
	movq	(%r12), %rax
	leaq	_ZNK6icu_6717GregorianCalendar19handleGetYearLengthEi(%rip), %rdx
	leal	-1(%r15), %esi
	movq	280(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L303
	movl	%esi, %eax
	andl	$3, %eax
	cmpl	640(%r12), %esi
	jl	.L304
	movl	$365, %edx
	testl	%eax, %eax
	jne	.L307
	imull	$-1030792151, %esi, %eax
	movl	$366, %edx
	addl	$85899344, %eax
	rorl	$2, %eax
	cmpl	$42949672, %eax
	ja	.L307
	movl	%esi, %eax
	movl	$400, %ecx
	cltd
	idivl	%ecx
	cmpl	$1, %edx
	sbbl	%edx, %edx
	notl	%edx
	addl	$366, %edx
.L307:
	subl	%edx, %r14d
	jmp	.L297
.L299:
	xorl	%eax, %eax
	testl	%edx, %edx
	sete	%al
	addl	$365, %eax
	jmp	.L302
.L309:
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	*%rax
	movl	%eax, %ebx
	jmp	.L313
.L304:
	xorl	%edx, %edx
	testl	%eax, %eax
	sete	%dl
	addl	$365, %edx
	jmp	.L307
.L298:
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	*%rax
	jmp	.L302
.L303:
	movq	%r12, %rdi
	call	*%rax
	movl	%eax, %edx
	jmp	.L307
	.cfi_endproc
.LFE3037:
	.size	_ZN6icu_6717GregorianCalendar4rollENS_8Calendar11EDateFieldsEiR10UErrorCode, .-_ZN6icu_6717GregorianCalendar4rollENS_8Calendar11EDateFieldsEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717GregorianCalendar4rollE19UCalendarDateFieldsiR10UErrorCode
	.type	_ZN6icu_6717GregorianCalendar4rollE19UCalendarDateFieldsiR10UErrorCode, @function
_ZN6icu_6717GregorianCalendar4rollE19UCalendarDateFieldsiR10UErrorCode:
.LFB3038:
	.cfi_startproc
	endbr64
	testl	%edx, %edx
	je	.L433
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rcx), %ecx
	testl	%ecx, %ecx
	jle	.L438
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L438:
	.cfi_restore_state
	movq	%rdi, %r12
	movl	%esi, %r15d
	movl	%edx, %r13d
	movl	$19, %esi
	movq	%r14, %rdx
	call	_ZNK6icu_678Calendar3getE19UCalendarDateFieldsR10UErrorCode@PLT
	cmpl	640(%r12), %eax
	je	.L439
	cmpl	$4, %r15d
	je	.L401
.L367:
	cmpl	$5, %r15d
	je	.L400
	cmpl	$3, %r15d
	je	.L440
	movq	%r14, %rcx
	movl	%r13d, %edx
	movl	%r15d, %esi
.L436:
	addq	$40, %rsp
	movq	%r12, %rdi
	popq	%rbx
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_678Calendar4rollE19UCalendarDateFieldsiR10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L433:
	ret
	.p2align 4,,10
	.p2align 3
.L439:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	leal	-4(%r15), %eax
	cmpl	$1, %eax
	ja	.L367
	movq	(%r12), %rax
	movl	20(%r12), %esi
	movq	%r12, %rdi
	call	*408(%rax)
	movl	32(%r12), %ebx
	pxor	%xmm1, %xmm1
	movsd	224(%r12), %xmm2
	movsd	616(%r12), %xmm0
	movl	%eax, %esi
	leal	-10(%rbx), %eax
	movapd	%xmm2, %xmm3
	comisd	%xmm0, %xmm2
	cmovnb	%eax, %ebx
	leal	-1(%rbx), %eax
	cvtsi2sdl	%eax, %xmm1
	mulsd	.LC8(%rip), %xmm1
	subsd	%xmm1, %xmm3
	comisd	%xmm3, %xmm0
	jbe	.L370
	leal	-10(%rsi), %r9d
	pxor	%xmm1, %xmm1
	movapd	%xmm3, %xmm4
	cvtsi2sdl	%r9d, %xmm1
	mulsd	.LC8(%rip), %xmm1
	addsd	%xmm1, %xmm4
	comisd	%xmm0, %xmm4
	jb	.L370
	cmpl	$4, %r15d
	jne	.L441
	movl	40(%r12), %r15d
	movq	%r12, %rdi
	movl	%r9d, -76(%rbp)
	movl	%esi, -64(%rbp)
	movsd	%xmm3, -72(%rbp)
	call	_ZNK6icu_678Calendar17getFirstDayOfWeekEv@PLT
	subl	%eax, %r15d
	leal	7(%r15), %eax
	cmovs	%eax, %r15d
	movl	%r15d, %ecx
	subl	%ebx, %ecx
	addl	$1, %ecx
	movslq	%ecx, %rax
	movl	%ecx, %edi
	imulq	$-1840700269, %rax, %rax
	sarl	$31, %edi
	shrq	$32, %rax
	addl	%ecx, %eax
	sarl	$2, %eax
	subl	%edi, %eax
	movq	%r12, %rdi
	movl	%eax, %r8d
	leal	0(,%rax,8), %eax
	subl	%r8d, %eax
	subl	%eax, %ecx
	movl	%ecx, %r8d
	leal	7(%rcx), %eax
	cmovs	%eax, %r8d
	movl	%r8d, -56(%rbp)
	movl	%r8d, -80(%rbp)
	call	_ZNK6icu_678Calendar25getMinimalDaysInFirstWeekEv@PLT
	movl	-56(%rbp), %r8d
	movl	$7, %ecx
	movl	-64(%rbp), %esi
	movzbl	%al, %eax
	movsd	-72(%rbp), %xmm3
	movl	-76(%rbp), %r9d
	subl	%r8d, %ecx
	cmpl	%eax, %ecx
	jge	.L397
	movl	$8, %ecx
	subl	%r8d, %ecx
.L398:
	movl	%r9d, %eax
	pxor	%xmm0, %xmm0
	subl	%ebx, %eax
	addl	%eax, %r15d
	movslq	%r15d, %rax
	movl	%r15d, %edi
	subl	%r15d, %esi
	imulq	$-1840700269, %rax, %rax
	sarl	$31, %edi
	shrq	$32, %rax
	addl	%r15d, %eax
	sarl	$2, %eax
	subl	%edi, %eax
	leal	0(,%rax,8), %edi
	subl	%eax, %edi
	leal	(%rbx,%r13,8), %eax
	subl	%r13d, %eax
	leal	-3(%rsi,%rdi), %esi
	subl	%ecx, %eax
	subl	%ecx, %esi
	cltd
	idivl	%esi
	movl	$1, %eax
	addl	%edx, %esi
	testl	%edx, %edx
	cmovns	%edx, %esi
	addl	%esi, %ecx
	testl	%ecx, %ecx
	cmovle	%eax, %ecx
	cmpl	%ecx, %r9d
	cmovle	%r9d, %ecx
	subl	$1, %ecx
	cvtsi2sdl	%ecx, %xmm0
	mulsd	.LC8(%rip), %xmm0
.L437:
	addsd	%xmm3, %xmm0
	addq	$40, %rsp
	movq	%r14, %rsi
	movq	%r12, %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_678Calendar15setTimeInMillisEdR10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L440:
	.cfi_restore_state
	movq	%r14, %rdx
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_678Calendar3getE19UCalendarDateFieldsR10UErrorCode@PLT
	movq	%r14, %rdx
	movl	$17, %esi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZNK6icu_678Calendar3getE19UCalendarDateFieldsR10UErrorCode@PLT
	movl	36(%r12), %r14d
	movl	%eax, %r15d
	movl	20(%r12), %eax
	testl	%eax, %eax
	je	.L442
	cmpl	$1, %ebx
	je	.L443
.L374:
	addl	%ebx, %r13d
	leal	-1(%r13), %eax
	cmpl	$51, %eax
	jbe	.L385
	movq	(%r12), %rax
	leaq	_ZNK6icu_6717GregorianCalendar19handleGetYearLengthEi(%rip), %rdx
	movq	280(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L386
	movl	%r15d, %eax
	andl	$3, %eax
	cmpl	640(%r12), %r15d
	jl	.L387
	movl	$365, %ebx
	testl	%eax, %eax
	jne	.L390
	imull	$-1030792151, %r15d, %eax
	movl	$366, %ebx
	addl	$85899344, %eax
	movl	%eax, %edx
	rorl	$2, %edx
	cmpl	$42949672, %edx
	ja	.L390
	rorl	$4, %eax
	xorl	%ebx, %ebx
	cmpl	$10737419, %eax
	setb	%bl
	addl	$365, %ebx
	.p2align 4,,10
	.p2align 3
.L390:
	movl	%ebx, %eax
	movq	%r12, %rdi
	subl	%r14d, %eax
	addl	40(%r12), %eax
	movl	%eax, %r14d
	call	_ZNK6icu_678Calendar17getFirstDayOfWeekEv@PLT
	movq	%r12, %rdi
	subl	%eax, %r14d
	movl	%r14d, %eax
	movslq	%r14d, %r14
	imulq	$-1840700269, %r14, %r14
	cltd
	shrq	$32, %r14
	addl	%eax, %r14d
	sarl	$2, %r14d
	subl	%edx, %r14d
	leal	0(,%r14,8), %edx
	subl	%r14d, %edx
	subl	%edx, %eax
	movl	%eax, %r14d
	leal	7(%rax), %eax
	cmovs	%eax, %r14d
	call	_ZNK6icu_678Calendar25getMinimalDaysInFirstWeekEv@PLT
	movl	$6, %edx
	leal	-7(%rbx), %ecx
	movq	%r12, %rdi
	subl	%r14d, %edx
	movzbl	%al, %eax
	cmpl	%eax, %edx
	cmovge	%ecx, %ebx
	leal	1(%r14), %ecx
	movl	%ebx, %edx
	movl	%ebx, %esi
	call	_ZN6icu_678Calendar10weekNumberEiii@PLT
	movl	%eax, %ecx
	leal	-1(%r13,%rax), %eax
	cltd
	idivl	%ecx
	leal	1(%rdx), %r13d
.L385:
	movl	%r13d, %edx
	movq	%r12, %rdi
	movl	$3, %esi
	call	_ZN6icu_678Calendar3setE19UCalendarDateFieldsi@PLT
	addq	$40, %rsp
	movl	%r15d, %edx
	movq	%r12, %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	movl	$17, %esi
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_678Calendar3setE19UCalendarDateFieldsi@PLT
	.p2align 4,,10
	.p2align 3
.L370:
	.cfi_restore_state
	cmpl	$5, %r15d
	je	.L400
.L401:
	movq	%r14, %rcx
	movl	%r13d, %edx
	movl	$4, %esi
	jmp	.L436
	.p2align 4,,10
	.p2align 3
.L442:
	cmpl	$51, %ebx
	jle	.L374
	movq	(%r12), %rax
	leaq	_ZNK6icu_6717GregorianCalendar19handleGetYearLengthEi(%rip), %rdx
	movq	280(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L375
	movl	%r15d, %edx
	andl	$3, %edx
	cmpl	640(%r12), %r15d
	jl	.L376
	movl	$365, %eax
	testl	%edx, %edx
	jne	.L379
	imull	$-1030792151, %r15d, %edx
	movl	$366, %eax
	addl	$85899344, %edx
	movl	%edx, %ecx
	rorl	$2, %ecx
	cmpl	$42949672, %ecx
	ja	.L379
	rorl	$4, %edx
	xorl	%eax, %eax
	cmpl	$10737419, %edx
	setb	%al
	addl	$365, %eax
	.p2align 4,,10
	.p2align 3
.L379:
	addl	%eax, %r14d
	jmp	.L374
	.p2align 4,,10
	.p2align 3
.L400:
	movq	%r14, %rcx
	movl	%r13d, %edx
	movl	$5, %esi
	jmp	.L436
.L441:
	pxor	%xmm0, %xmm0
	subsd	%xmm3, %xmm2
	movsd	%xmm3, -56(%rbp)
	cvtsi2sdl	%r13d, %xmm0
	mulsd	.LC8(%rip), %xmm0
	movsd	%xmm1, -64(%rbp)
	addsd	%xmm2, %xmm0
	call	uprv_fmod_67@PLT
	pxor	%xmm2, %xmm2
	movsd	-56(%rbp), %xmm3
	comisd	%xmm0, %xmm2
	jbe	.L437
	movsd	-64(%rbp), %xmm1
	addsd	%xmm1, %xmm0
	jmp	.L437
	.p2align 4,,10
	.p2align 3
.L397:
	movl	$1, %ecx
	subl	%r8d, %ecx
	jmp	.L398
	.p2align 4,,10
	.p2align 3
.L387:
	xorl	%ebx, %ebx
	testl	%eax, %eax
	sete	%bl
	addl	$365, %ebx
	jmp	.L390
	.p2align 4,,10
	.p2align 3
.L443:
	movq	(%r12), %rax
	leaq	_ZNK6icu_6717GregorianCalendar19handleGetYearLengthEi(%rip), %rdx
	leal	-1(%r15), %esi
	movq	280(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L380
	movl	%esi, %eax
	andl	$3, %eax
	cmpl	640(%r12), %esi
	jl	.L381
	movl	$365, %edx
	testl	%eax, %eax
	jne	.L384
	imull	$-1030792151, %esi, %eax
	movl	$366, %edx
	addl	$85899344, %eax
	movl	%eax, %ecx
	rorl	$2, %ecx
	cmpl	$42949672, %ecx
	ja	.L384
	rorl	$4, %eax
	xorl	%edx, %edx
	cmpl	$10737419, %eax
	setb	%dl
	addl	$365, %edx
	.p2align 4,,10
	.p2align 3
.L384:
	subl	%edx, %r14d
	jmp	.L374
.L376:
	xorl	%eax, %eax
	testl	%edx, %edx
	sete	%al
	addl	$365, %eax
	jmp	.L379
.L386:
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	*%rax
	movl	%eax, %ebx
	jmp	.L390
.L381:
	xorl	%edx, %edx
	testl	%eax, %eax
	sete	%dl
	addl	$365, %edx
	jmp	.L384
.L375:
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	*%rax
	jmp	.L379
.L380:
	movq	%r12, %rdi
	call	*%rax
	movl	%eax, %edx
	jmp	.L384
	.cfi_endproc
.LFE3038:
	.size	_ZN6icu_6717GregorianCalendar4rollE19UCalendarDateFieldsiR10UErrorCode, .-_ZN6icu_6717GregorianCalendar4rollE19UCalendarDateFieldsiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717GregorianCalendar16getActualMaximumE19UCalendarDateFieldsR10UErrorCode
	.type	_ZNK6icu_6717GregorianCalendar16getActualMaximumE19UCalendarDateFieldsR10UErrorCode, @function
_ZNK6icu_6717GregorianCalendar16getActualMaximumE19UCalendarDateFieldsR10UErrorCode:
.LFB3043:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpl	$1, %esi
	jne	.L445
	movl	(%rdx), %eax
	xorl	%r14d, %r14d
	testl	%eax, %eax
	jg	.L444
	movq	(%rdi), %rax
	leaq	_ZNK6icu_6717GregorianCalendar5cloneEv(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L447
	movl	$656, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L450
	movq	%rax, %rdi
	movq	%r15, %rsi
	call	_ZN6icu_678CalendarC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6717GregorianCalendarE(%rip), %rax
	movsd	616(%r15), %xmm0
	movq	%rax, (%r12)
	movl	624(%r15), %eax
	movsd	%xmm0, 616(%r12)
	movsd	632(%r15), %xmm0
	movl	%eax, 624(%r12)
	movl	640(%r15), %eax
	movsd	%xmm0, 632(%r12)
	movl	%eax, 640(%r12)
	movzwl	648(%r15), %eax
	movw	%ax, 648(%r12)
.L449:
	movl	$1, %esi
	movq	%r12, %rdi
	movl	$140743, %r15d
	movl	$1, %r14d
	call	_ZN6icu_678Calendar10setLenientEa@PLT
	movq	%r13, %rdx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_678Calendar3getE19UCalendarDateFieldsR10UErrorCode@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, -60(%rbp)
	call	_ZNK6icu_678Calendar15getTimeInMillisER10UErrorCode@PLT
	movsd	%xmm0, -56(%rbp)
	jmp	.L451
	.p2align 4,,10
	.p2align 3
.L455:
	movsd	-56(%rbp), %xmm0
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%ebx, %r15d
	call	_ZN6icu_678Calendar15setTimeInMillisEdR10UErrorCode@PLT
	leal	1(%r14), %eax
	cmpl	%r15d, %eax
	jge	.L452
.L451:
	leal	(%r14,%r15), %eax
	movl	$1, %esi
	movq	%r12, %rdi
	movl	%eax, %ebx
	shrl	$31, %ebx
	addl	%eax, %ebx
	sarl	%ebx
	movl	%ebx, %edx
	call	_ZN6icu_678Calendar3setE19UCalendarDateFieldsi@PLT
	movq	%r13, %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_678Calendar3getE19UCalendarDateFieldsR10UErrorCode@PLT
	cmpl	%eax, %ebx
	jne	.L455
	xorl	%esi, %esi
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZNK6icu_678Calendar3getE19UCalendarDateFieldsR10UErrorCode@PLT
	cmpl	%eax, -60(%rbp)
	jne	.L455
	movl	%ebx, %r14d
	leal	1(%r14), %eax
	cmpl	%r15d, %eax
	jl	.L451
	.p2align 4,,10
	.p2align 3
.L452:
	movq	(%r12), %rax
	leaq	_ZN6icu_6717GregorianCalendarD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L456
	leaq	16+_ZTVN6icu_6717GregorianCalendarE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	call	_ZN6icu_678CalendarD2Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L444:
	addq	$24, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L445:
	.cfi_restore_state
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_678Calendar16getActualMaximumE19UCalendarDateFieldsR10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L447:
	.cfi_restore_state
	call	*%rax
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L449
.L450:
	movl	$7, 0(%r13)
	xorl	%r14d, %r14d
	jmp	.L444
	.p2align 4,,10
	.p2align 3
.L456:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L444
	.cfi_endproc
.LFE3043:
	.size	_ZNK6icu_6717GregorianCalendar16getActualMaximumE19UCalendarDateFieldsR10UErrorCode, .-_ZNK6icu_6717GregorianCalendar16getActualMaximumE19UCalendarDateFieldsR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717GregorianCalendar35handleGetExtendedYearFromWeekFieldsEii
	.type	_ZN6icu_6717GregorianCalendar35handleGetExtendedYearFromWeekFieldsEii, @function
_ZN6icu_6717GregorianCalendar35handleGetExtendedYearFromWeekFieldsEii:
.LFB3045:
	.cfi_startproc
	endbr64
	movl	128(%rdi), %ecx
	testl	%ecx, %ecx
	jle	.L464
	movl	12(%rdi), %eax
	testl	%eax, %eax
	jne	.L464
	movl	$1, %eax
	subl	%esi, %eax
	movl	%eax, %esi
.L464:
	jmp	_ZN6icu_678Calendar35handleGetExtendedYearFromWeekFieldsEii@PLT
	.cfi_endproc
.LFE3045:
	.size	_ZN6icu_6717GregorianCalendar35handleGetExtendedYearFromWeekFieldsEii, .-_ZN6icu_6717GregorianCalendar35handleGetExtendedYearFromWeekFieldsEii
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717GregorianCalendar21handleGetExtendedYearEv
	.type	_ZN6icu_6717GregorianCalendar21handleGetExtendedYearEv, @function
_ZN6icu_6717GregorianCalendar21handleGetExtendedYearEv:
.LFB3044:
	.cfi_startproc
	endbr64
	movl	204(%rdi), %eax
	movl	132(%rdi), %edx
	movl	196(%rdi), %ecx
	cmpl	%edx, %eax
	jl	.L466
	cmpl	%ecx, %eax
	jl	.L467
	movl	$1970, %r8d
	testl	%eax, %eax
	jle	.L465
	movl	88(%rdi), %r8d
.L465:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L466:
	cmpl	%ecx, %edx
	jge	.L479
.L467:
	movq	(%rdi), %rax
	leaq	_ZN6icu_6717GregorianCalendar35handleGetExtendedYearFromWeekFieldsEii(%rip), %rcx
	movl	24(%rdi), %edx
	movl	80(%rdi), %esi
	movq	304(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L473
	movl	128(%rdi), %ecx
	testl	%ecx, %ecx
	jle	.L474
	movl	12(%rdi), %eax
	testl	%eax, %eax
	jne	.L474
	movl	$1, %eax
	subl	%esi, %eax
	movl	%eax, %esi
.L474:
	jmp	_ZN6icu_678Calendar35handleGetExtendedYearFromWeekFieldsEii@PLT
	.p2align 4,,10
	.p2align 3
.L479:
	movl	128(%rdi), %esi
	testl	%esi, %esi
	jle	.L471
	movl	12(%rdi), %r8d
	testl	%r8d, %r8d
	jne	.L471
	testl	%edx, %edx
	jle	.L465
	movl	$1, %r8d
	subl	16(%rdi), %r8d
	jmp	.L465
	.p2align 4,,10
	.p2align 3
.L473:
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L471:
	movl	$1970, %r8d
	testl	%edx, %edx
	jle	.L465
	movl	16(%rdi), %r8d
	jmp	.L465
	.cfi_endproc
.LFE3044:
	.size	_ZN6icu_6717GregorianCalendar21handleGetExtendedYearEv, .-_ZN6icu_6717GregorianCalendar21handleGetExtendedYearEv
	.weak	_ZTSN6icu_6717GregorianCalendarE
	.section	.rodata._ZTSN6icu_6717GregorianCalendarE,"aG",@progbits,_ZTSN6icu_6717GregorianCalendarE,comdat
	.align 16
	.type	_ZTSN6icu_6717GregorianCalendarE, @object
	.size	_ZTSN6icu_6717GregorianCalendarE, 29
_ZTSN6icu_6717GregorianCalendarE:
	.string	"N6icu_6717GregorianCalendarE"
	.weak	_ZTIN6icu_6717GregorianCalendarE
	.section	.data.rel.ro._ZTIN6icu_6717GregorianCalendarE,"awG",@progbits,_ZTIN6icu_6717GregorianCalendarE,comdat
	.align 8
	.type	_ZTIN6icu_6717GregorianCalendarE, @object
	.size	_ZTIN6icu_6717GregorianCalendarE, 24
_ZTIN6icu_6717GregorianCalendarE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6717GregorianCalendarE
	.quad	_ZTIN6icu_678CalendarE
	.weak	_ZTVN6icu_6717GregorianCalendarE
	.section	.data.rel.ro._ZTVN6icu_6717GregorianCalendarE,"awG",@progbits,_ZTVN6icu_6717GregorianCalendarE,comdat
	.align 8
	.type	_ZTVN6icu_6717GregorianCalendarE, @object
	.size	_ZTVN6icu_6717GregorianCalendarE, 448
_ZTVN6icu_6717GregorianCalendarE:
	.quad	0
	.quad	_ZTIN6icu_6717GregorianCalendarE
	.quad	_ZN6icu_6717GregorianCalendarD1Ev
	.quad	_ZN6icu_6717GregorianCalendarD0Ev
	.quad	_ZNK6icu_6717GregorianCalendar17getDynamicClassIDEv
	.quad	_ZNK6icu_6717GregorianCalendar5cloneEv
	.quad	_ZNK6icu_678CalendareqERKS0_
	.quad	_ZNK6icu_6717GregorianCalendar14isEquivalentToERKNS_8CalendarE
	.quad	_ZN6icu_678Calendar3addENS0_11EDateFieldsEiR10UErrorCode
	.quad	_ZN6icu_678Calendar3addE19UCalendarDateFieldsiR10UErrorCode
	.quad	_ZN6icu_6717GregorianCalendar4rollENS_8Calendar11EDateFieldsEiR10UErrorCode
	.quad	_ZN6icu_6717GregorianCalendar4rollE19UCalendarDateFieldsiR10UErrorCode
	.quad	_ZN6icu_678Calendar15fieldDifferenceEdNS0_11EDateFieldsER10UErrorCode
	.quad	_ZN6icu_678Calendar15fieldDifferenceEd19UCalendarDateFieldsR10UErrorCode
	.quad	_ZNK6icu_6717GregorianCalendar14inDaylightTimeER10UErrorCode
	.quad	_ZNK6icu_678Calendar10getMinimumENS0_11EDateFieldsE
	.quad	_ZNK6icu_678Calendar10getMinimumE19UCalendarDateFields
	.quad	_ZNK6icu_678Calendar10getMaximumENS0_11EDateFieldsE
	.quad	_ZNK6icu_678Calendar10getMaximumE19UCalendarDateFields
	.quad	_ZNK6icu_678Calendar18getGreatestMinimumENS0_11EDateFieldsE
	.quad	_ZNK6icu_678Calendar18getGreatestMinimumE19UCalendarDateFields
	.quad	_ZNK6icu_678Calendar15getLeastMaximumENS0_11EDateFieldsE
	.quad	_ZNK6icu_678Calendar15getLeastMaximumE19UCalendarDateFields
	.quad	_ZNK6icu_6717GregorianCalendar16getActualMinimumE19UCalendarDateFieldsR10UErrorCode
	.quad	_ZNK6icu_6717GregorianCalendar16getActualMaximumE19UCalendarDateFieldsR10UErrorCode
	.quad	_ZNK6icu_6717GregorianCalendar7getTypeEv
	.quad	_ZNK6icu_678Calendar16getDayOfWeekTypeE19UCalendarDaysOfWeekR10UErrorCode
	.quad	_ZNK6icu_678Calendar20getWeekendTransitionE19UCalendarDaysOfWeekR10UErrorCode
	.quad	_ZNK6icu_678Calendar9isWeekendEdR10UErrorCode
	.quad	_ZNK6icu_678Calendar9isWeekendEv
	.quad	_ZN6icu_678Calendar11computeTimeER10UErrorCode
	.quad	_ZN6icu_678Calendar13computeFieldsER10UErrorCode
	.quad	_ZN6icu_678Calendar16prepareGetActualE19UCalendarDateFieldsaR10UErrorCode
	.quad	_ZNK6icu_6717GregorianCalendar14handleGetLimitE19UCalendarDateFieldsNS_8Calendar10ELimitTypeE
	.quad	_ZNK6icu_678Calendar8getLimitE19UCalendarDateFieldsNS0_10ELimitTypeE
	.quad	_ZNK6icu_6717GregorianCalendar23handleComputeMonthStartEiia
	.quad	_ZNK6icu_6717GregorianCalendar20handleGetMonthLengthEii
	.quad	_ZNK6icu_6717GregorianCalendar19handleGetYearLengthEi
	.quad	_ZN6icu_6717GregorianCalendar21handleGetExtendedYearEv
	.quad	_ZN6icu_6717GregorianCalendar22handleComputeJulianDayE19UCalendarDateFields
	.quad	_ZN6icu_6717GregorianCalendar35handleGetExtendedYearFromWeekFieldsEii
	.quad	_ZN6icu_678Calendar13validateFieldE19UCalendarDateFieldsR10UErrorCode
	.quad	_ZNK6icu_678Calendar23getFieldResolutionTableEv
	.quad	_ZN6icu_6717GregorianCalendar19handleComputeFieldsEiR10UErrorCode
	.quad	_ZN6icu_678Calendar21getDefaultMonthInYearEi
	.quad	_ZN6icu_678Calendar20getDefaultDayInMonthEii
	.quad	_ZN6icu_678Calendar8pinFieldE19UCalendarDateFieldsR10UErrorCode
	.quad	_ZNK6icu_6717GregorianCalendar18haveDefaultCenturyEv
	.quad	_ZNK6icu_6717GregorianCalendar19defaultCenturyStartEv
	.quad	_ZNK6icu_6717GregorianCalendar23defaultCenturyStartYearEv
	.quad	_ZNK6icu_678Calendar14getRelatedYearER10UErrorCode
	.quad	_ZN6icu_678Calendar14setRelatedYearEi
	.quad	_ZNK6icu_6717GregorianCalendar14internalGetEraEv
	.quad	_ZNK6icu_6717GregorianCalendar11monthLengthEi
	.quad	_ZNK6icu_6717GregorianCalendar11monthLengthEii
	.quad	_ZN6icu_6717GregorianCalendar11getEpochDayER10UErrorCode
	.local	_ZN6icu_67L25gSystemDefaultCenturyInitE
	.comm	_ZN6icu_67L25gSystemDefaultCenturyInitE,8,8
	.data
	.align 4
	.type	_ZN6icu_67L30gSystemDefaultCenturyStartYearE, @object
	.size	_ZN6icu_67L30gSystemDefaultCenturyStartYearE, 4
_ZN6icu_67L30gSystemDefaultCenturyStartYearE:
	.long	-1
	.align 8
	.type	_ZN6icu_67L26gSystemDefaultCenturyStartE, @object
	.size	_ZN6icu_67L26gSystemDefaultCenturyStartE, 8
_ZN6icu_67L26gSystemDefaultCenturyStartE:
	.long	0
	.long	1048576
	.local	_ZZN6icu_6717GregorianCalendar16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6717GregorianCalendar16getStaticClassIDEvE7classID,1,1
	.section	.rodata
	.align 32
	.type	_ZL24kGregorianCalendarLimits, @object
	.size	_ZL24kGregorianCalendarLimits, 368
_ZL24kGregorianCalendarLimits:
	.long	0
	.long	0
	.long	1
	.long	1
	.long	1
	.long	1
	.long	140742
	.long	144683
	.long	0
	.long	0
	.long	11
	.long	11
	.long	1
	.long	1
	.long	52
	.long	53
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	1
	.long	1
	.long	28
	.long	31
	.long	1
	.long	1
	.long	365
	.long	366
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	4
	.long	5
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-140742
	.long	-140742
	.long	140742
	.long	144683
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-140742
	.long	-140742
	.long	140742
	.long	144683
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.align 8
	.type	_ZL16kLeapMonthLength, @object
	.size	_ZL16kLeapMonthLength, 12
_ZL16kLeapMonthLength:
	.ascii	"\037\035\037\036\037\036\037\037\036\037\036\037"
	.align 8
	.type	_ZL12kMonthLength, @object
	.size	_ZL12kMonthLength, 12
_ZL12kMonthLength:
	.ascii	"\037\034\037\036\037\036\037\037\036\037\036\037"
	.align 16
	.type	_ZL12kLeapNumDays, @object
	.size	_ZL12kLeapNumDays, 24
_ZL12kLeapNumDays:
	.value	0
	.value	31
	.value	60
	.value	91
	.value	121
	.value	152
	.value	182
	.value	213
	.value	244
	.value	274
	.value	305
	.value	335
	.align 16
	.type	_ZL8kNumDays, @object
	.size	_ZL8kNumDays, 24
_ZL8kNumDays:
	.value	0
	.value	31
	.value	59
	.value	90
	.value	120
	.value	151
	.value	181
	.value	212
	.value	243
	.value	273
	.value	304
	.value	334
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	0
	.long	1083129856
	.align 8
.LC2:
	.long	0
	.long	1089804288
	.align 8
.LC3:
	.long	916979712
	.long	-1029293555
	.align 8
.LC4:
	.long	0
	.long	1074790400
	.align 8
.LC5:
	.long	0
	.long	1083629568
	.align 8
.LC6:
	.long	0
	.long	-1042284544
	.align 8
.LC7:
	.long	4290772992
	.long	1105199103
	.align 8
.LC8:
	.long	0
	.long	1100257648
	.align 8
.LC9:
	.long	0
	.long	-1014720144
	.align 8
.LC10:
	.long	4292267296
	.long	1132763503
	.align 8
.LC11:
	.long	0
	.long	1081528320
	.align 8
.LC12:
	.long	0
	.long	1094337615
	.align 8
.LC13:
	.long	0
	.long	1094885062
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
