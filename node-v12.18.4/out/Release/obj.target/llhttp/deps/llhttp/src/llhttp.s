	.file	"llhttp.c"
	.text
	.p2align 4
	.globl	llhttp__internal__c_update_finish
	.type	llhttp__internal__c_update_finish, @function
llhttp__internal__c_update_finish:
.LFB30:
	.cfi_startproc
	endbr64
	movb	$2, 84(%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE30:
	.size	llhttp__internal__c_update_finish, .-llhttp__internal__c_update_finish
	.p2align 4
	.globl	llhttp__internal__c_load_type
	.type	llhttp__internal__c_load_type, @function
llhttp__internal__c_load_type:
.LFB31:
	.cfi_startproc
	endbr64
	movzbl	72(%rdi), %eax
	ret
	.cfi_endproc
.LFE31:
	.size	llhttp__internal__c_load_type, .-llhttp__internal__c_load_type
	.p2align 4
	.globl	llhttp__internal__c_store_method
	.type	llhttp__internal__c_store_method, @function
llhttp__internal__c_store_method:
.LFB32:
	.cfi_startproc
	endbr64
	movb	%cl, 73(%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE32:
	.size	llhttp__internal__c_store_method, .-llhttp__internal__c_store_method
	.p2align 4
	.globl	llhttp__internal__c_is_equal_method
	.type	llhttp__internal__c_is_equal_method, @function
llhttp__internal__c_is_equal_method:
.LFB33:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpb	$5, 73(%rdi)
	sete	%al
	ret
	.cfi_endproc
.LFE33:
	.size	llhttp__internal__c_is_equal_method, .-llhttp__internal__c_is_equal_method
	.p2align 4
	.globl	llhttp__internal__c_update_http_major
	.type	llhttp__internal__c_update_http_major, @function
llhttp__internal__c_update_http_major:
.LFB34:
	.cfi_startproc
	endbr64
	movb	$0, 74(%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE34:
	.size	llhttp__internal__c_update_http_major, .-llhttp__internal__c_update_http_major
	.p2align 4
	.globl	llhttp__internal__c_update_http_minor
	.type	llhttp__internal__c_update_http_minor, @function
llhttp__internal__c_update_http_minor:
.LFB35:
	.cfi_startproc
	endbr64
	movb	$9, 75(%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE35:
	.size	llhttp__internal__c_update_http_minor, .-llhttp__internal__c_update_http_minor
	.p2align 4
	.globl	llhttp__internal__c_test_flags
	.type	llhttp__internal__c_test_flags, @function
llhttp__internal__c_test_flags:
.LFB36:
	.cfi_startproc
	endbr64
	movzwl	78(%rdi), %eax
	shrw	$7, %ax
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE36:
	.size	llhttp__internal__c_test_flags, .-llhttp__internal__c_test_flags
	.p2align 4
	.globl	llhttp__internal__c_is_equal_upgrade
	.type	llhttp__internal__c_is_equal_upgrade, @function
llhttp__internal__c_is_equal_upgrade:
.LFB37:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpb	$1, 80(%rdi)
	sete	%al
	ret
	.cfi_endproc
.LFE37:
	.size	llhttp__internal__c_is_equal_upgrade, .-llhttp__internal__c_is_equal_upgrade
	.p2align 4
	.globl	llhttp__internal__c_update_finish_1
	.type	llhttp__internal__c_update_finish_1, @function
llhttp__internal__c_update_finish_1:
.LFB38:
	.cfi_startproc
	endbr64
	movb	$0, 84(%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE38:
	.size	llhttp__internal__c_update_finish_1, .-llhttp__internal__c_update_finish_1
	.p2align 4
	.globl	llhttp__internal__c_test_flags_1
	.type	llhttp__internal__c_test_flags_1, @function
llhttp__internal__c_test_flags_1:
.LFB39:
	.cfi_startproc
	endbr64
	movzwl	78(%rdi), %eax
	andw	$544, %ax
	cmpw	$544, %ax
	sete	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE39:
	.size	llhttp__internal__c_test_flags_1, .-llhttp__internal__c_test_flags_1
	.p2align 4
	.globl	llhttp__internal__c_test_flags_2
	.type	llhttp__internal__c_test_flags_2, @function
llhttp__internal__c_test_flags_2:
.LFB40:
	.cfi_startproc
	endbr64
	movzbl	79(%rdi), %eax
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE40:
	.size	llhttp__internal__c_test_flags_2, .-llhttp__internal__c_test_flags_2
	.p2align 4
	.globl	llhttp__internal__c_test_flags_3
	.type	llhttp__internal__c_test_flags_3, @function
llhttp__internal__c_test_flags_3:
.LFB41:
	.cfi_startproc
	endbr64
	movzwl	78(%rdi), %eax
	andl	$40, %eax
	cmpw	$40, %ax
	sete	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE41:
	.size	llhttp__internal__c_test_flags_3, .-llhttp__internal__c_test_flags_3
	.p2align 4
	.globl	llhttp__internal__c_update_content_length
	.type	llhttp__internal__c_update_content_length, @function
llhttp__internal__c_update_content_length:
.LFB42:
	.cfi_startproc
	endbr64
	movq	$0, 64(%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE42:
	.size	llhttp__internal__c_update_content_length, .-llhttp__internal__c_update_content_length
	.p2align 4
	.globl	llhttp__internal__c_mul_add_content_length
	.type	llhttp__internal__c_mul_add_content_length, @function
llhttp__internal__c_mul_add_content_length:
.LFB43:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rdx
	movq	64(%rdi), %rax
	cmpq	%rdx, %rax
	ja	.L19
	salq	$4, %rax
	movslq	%ecx, %rdx
	testl	%ecx, %ecx
	movq	%rax, 64(%rdi)
	movq	%rdx, %rcx
	js	.L18
	notq	%rcx
	cmpq	%rcx, %rax
	ja	.L19
.L20:
	addq	%rdx, %rax
	movq	%rax, 64(%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	negq	%rcx
	cmpq	%rcx, %rax
	jnb	.L20
.L19:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE43:
	.size	llhttp__internal__c_mul_add_content_length, .-llhttp__internal__c_mul_add_content_length
	.p2align 4
	.globl	llhttp__internal__c_is_equal_content_length
	.type	llhttp__internal__c_is_equal_content_length, @function
llhttp__internal__c_is_equal_content_length:
.LFB44:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpq	$0, 64(%rdi)
	sete	%al
	ret
	.cfi_endproc
.LFE44:
	.size	llhttp__internal__c_is_equal_content_length, .-llhttp__internal__c_is_equal_content_length
	.p2align 4
	.globl	llhttp__internal__c_or_flags
	.type	llhttp__internal__c_or_flags, @function
llhttp__internal__c_or_flags:
.LFB45:
	.cfi_startproc
	endbr64
	orw	$128, 78(%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE45:
	.size	llhttp__internal__c_or_flags, .-llhttp__internal__c_or_flags
	.p2align 4
	.globl	llhttp__internal__c_update_finish_2
	.type	llhttp__internal__c_update_finish_2, @function
llhttp__internal__c_update_finish_2:
.LFB46:
	.cfi_startproc
	endbr64
	movb	$1, 84(%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE46:
	.size	llhttp__internal__c_update_finish_2, .-llhttp__internal__c_update_finish_2
	.p2align 4
	.globl	llhttp__internal__c_or_flags_1
	.type	llhttp__internal__c_or_flags_1, @function
llhttp__internal__c_or_flags_1:
.LFB47:
	.cfi_startproc
	endbr64
	orw	$64, 78(%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE47:
	.size	llhttp__internal__c_or_flags_1, .-llhttp__internal__c_or_flags_1
	.p2align 4
	.globl	llhttp__internal__c_update_upgrade
	.type	llhttp__internal__c_update_upgrade, @function
llhttp__internal__c_update_upgrade:
.LFB48:
	.cfi_startproc
	endbr64
	movb	$1, 80(%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE48:
	.size	llhttp__internal__c_update_upgrade, .-llhttp__internal__c_update_upgrade
	.p2align 4
	.globl	llhttp__internal__c_store_header_state
	.type	llhttp__internal__c_store_header_state, @function
llhttp__internal__c_store_header_state:
.LFB49:
	.cfi_startproc
	endbr64
	movb	%cl, 76(%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE49:
	.size	llhttp__internal__c_store_header_state, .-llhttp__internal__c_store_header_state
	.p2align 4
	.globl	llhttp__internal__c_load_header_state
	.type	llhttp__internal__c_load_header_state, @function
llhttp__internal__c_load_header_state:
.LFB50:
	.cfi_startproc
	endbr64
	movzbl	76(%rdi), %eax
	ret
	.cfi_endproc
.LFE50:
	.size	llhttp__internal__c_load_header_state, .-llhttp__internal__c_load_header_state
	.p2align 4
	.globl	llhttp__internal__c_or_flags_3
	.type	llhttp__internal__c_or_flags_3, @function
llhttp__internal__c_or_flags_3:
.LFB51:
	.cfi_startproc
	endbr64
	orw	$1, 78(%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE51:
	.size	llhttp__internal__c_or_flags_3, .-llhttp__internal__c_or_flags_3
	.p2align 4
	.globl	llhttp__internal__c_update_header_state
	.type	llhttp__internal__c_update_header_state, @function
llhttp__internal__c_update_header_state:
.LFB52:
	.cfi_startproc
	endbr64
	movb	$1, 76(%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE52:
	.size	llhttp__internal__c_update_header_state, .-llhttp__internal__c_update_header_state
	.p2align 4
	.globl	llhttp__internal__c_or_flags_4
	.type	llhttp__internal__c_or_flags_4, @function
llhttp__internal__c_or_flags_4:
.LFB53:
	.cfi_startproc
	endbr64
	orw	$2, 78(%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE53:
	.size	llhttp__internal__c_or_flags_4, .-llhttp__internal__c_or_flags_4
	.p2align 4
	.globl	llhttp__internal__c_or_flags_5
	.type	llhttp__internal__c_or_flags_5, @function
llhttp__internal__c_or_flags_5:
.LFB54:
	.cfi_startproc
	endbr64
	orw	$4, 78(%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE54:
	.size	llhttp__internal__c_or_flags_5, .-llhttp__internal__c_or_flags_5
	.p2align 4
	.globl	llhttp__internal__c_or_flags_6
	.type	llhttp__internal__c_or_flags_6, @function
llhttp__internal__c_or_flags_6:
.LFB55:
	.cfi_startproc
	endbr64
	orw	$8, 78(%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE55:
	.size	llhttp__internal__c_or_flags_6, .-llhttp__internal__c_or_flags_6
	.p2align 4
	.globl	llhttp__internal__c_update_header_state_2
	.type	llhttp__internal__c_update_header_state_2, @function
llhttp__internal__c_update_header_state_2:
.LFB56:
	.cfi_startproc
	endbr64
	movb	$6, 76(%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE56:
	.size	llhttp__internal__c_update_header_state_2, .-llhttp__internal__c_update_header_state_2
	.p2align 4
	.globl	llhttp__internal__c_update_header_state_4
	.type	llhttp__internal__c_update_header_state_4, @function
llhttp__internal__c_update_header_state_4:
.LFB57:
	.cfi_startproc
	endbr64
	movb	$0, 76(%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE57:
	.size	llhttp__internal__c_update_header_state_4, .-llhttp__internal__c_update_header_state_4
	.p2align 4
	.globl	llhttp__internal__c_update_header_state_5
	.type	llhttp__internal__c_update_header_state_5, @function
llhttp__internal__c_update_header_state_5:
.LFB58:
	.cfi_startproc
	endbr64
	movb	$5, 76(%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE58:
	.size	llhttp__internal__c_update_header_state_5, .-llhttp__internal__c_update_header_state_5
	.p2align 4
	.globl	llhttp__internal__c_update_header_state_6
	.type	llhttp__internal__c_update_header_state_6, @function
llhttp__internal__c_update_header_state_6:
.LFB59:
	.cfi_startproc
	endbr64
	movb	$7, 76(%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE59:
	.size	llhttp__internal__c_update_header_state_6, .-llhttp__internal__c_update_header_state_6
	.p2align 4
	.globl	llhttp__internal__c_test_flags_5
	.type	llhttp__internal__c_test_flags_5, @function
llhttp__internal__c_test_flags_5:
.LFB60:
	.cfi_startproc
	endbr64
	movzwl	78(%rdi), %eax
	shrw	$5, %ax
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE60:
	.size	llhttp__internal__c_test_flags_5, .-llhttp__internal__c_test_flags_5
	.p2align 4
	.globl	llhttp__internal__c_mul_add_content_length_1
	.type	llhttp__internal__c_mul_add_content_length_1, @function
llhttp__internal__c_mul_add_content_length_1:
.LFB61:
	.cfi_startproc
	endbr64
	movabsq	$1844674407370955161, %rdx
	movq	64(%rdi), %rax
	cmpq	%rdx, %rax
	ja	.L42
	leaq	(%rax,%rax,4), %rax
	movslq	%ecx, %rdx
	addq	%rax, %rax
	testl	%ecx, %ecx
	movq	%rdx, %rcx
	movq	%rax, 64(%rdi)
	js	.L41
	notq	%rcx
	cmpq	%rcx, %rax
	ja	.L42
.L43:
	addq	%rdx, %rax
	movq	%rax, 64(%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	negq	%rcx
	cmpq	%rcx, %rax
	jnb	.L43
.L42:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE61:
	.size	llhttp__internal__c_mul_add_content_length_1, .-llhttp__internal__c_mul_add_content_length_1
	.p2align 4
	.globl	llhttp__internal__c_or_flags_15
	.type	llhttp__internal__c_or_flags_15, @function
llhttp__internal__c_or_flags_15:
.LFB62:
	.cfi_startproc
	endbr64
	orw	$32, 78(%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE62:
	.size	llhttp__internal__c_or_flags_15, .-llhttp__internal__c_or_flags_15
	.p2align 4
	.globl	llhttp__internal__c_or_flags_16
	.type	llhttp__internal__c_or_flags_16, @function
llhttp__internal__c_or_flags_16:
.LFB63:
	.cfi_startproc
	endbr64
	orw	$512, 78(%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE63:
	.size	llhttp__internal__c_or_flags_16, .-llhttp__internal__c_or_flags_16
	.p2align 4
	.globl	llhttp__internal__c_update_header_state_7
	.type	llhttp__internal__c_update_header_state_7, @function
llhttp__internal__c_update_header_state_7:
.LFB64:
	.cfi_startproc
	endbr64
	movb	$8, 76(%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE64:
	.size	llhttp__internal__c_update_header_state_7, .-llhttp__internal__c_update_header_state_7
	.p2align 4
	.globl	llhttp__internal__c_or_flags_17
	.type	llhttp__internal__c_or_flags_17, @function
llhttp__internal__c_or_flags_17:
.LFB65:
	.cfi_startproc
	endbr64
	orw	$16, 78(%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE65:
	.size	llhttp__internal__c_or_flags_17, .-llhttp__internal__c_or_flags_17
	.p2align 4
	.globl	llhttp__internal__c_store_http_major
	.type	llhttp__internal__c_store_http_major, @function
llhttp__internal__c_store_http_major:
.LFB66:
	.cfi_startproc
	endbr64
	movb	%cl, 74(%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE66:
	.size	llhttp__internal__c_store_http_major, .-llhttp__internal__c_store_http_major
	.p2align 4
	.globl	llhttp__internal__c_store_http_minor
	.type	llhttp__internal__c_store_http_minor, @function
llhttp__internal__c_store_http_minor:
.LFB67:
	.cfi_startproc
	endbr64
	movb	%cl, 75(%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE67:
	.size	llhttp__internal__c_store_http_minor, .-llhttp__internal__c_store_http_minor
	.p2align 4
	.globl	llhttp__internal__c_is_equal_method_1
	.type	llhttp__internal__c_is_equal_method_1, @function
llhttp__internal__c_is_equal_method_1:
.LFB68:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpb	$33, 73(%rdi)
	sete	%al
	ret
	.cfi_endproc
.LFE68:
	.size	llhttp__internal__c_is_equal_method_1, .-llhttp__internal__c_is_equal_method_1
	.p2align 4
	.globl	llhttp__internal__c_update_status_code
	.type	llhttp__internal__c_update_status_code, @function
llhttp__internal__c_update_status_code:
.LFB69:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	movw	%ax, 82(%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE69:
	.size	llhttp__internal__c_update_status_code, .-llhttp__internal__c_update_status_code
	.p2align 4
	.globl	llhttp__internal__c_mul_add_status_code
	.type	llhttp__internal__c_mul_add_status_code, @function
llhttp__internal__c_mul_add_status_code:
.LFB70:
	.cfi_startproc
	endbr64
	movzwl	82(%rdi), %eax
	cmpw	$6553, %ax
	ja	.L56
	leal	(%rax,%rax,4), %eax
	addl	%eax, %eax
	movw	%ax, 82(%rdi)
	movzwl	%ax, %esi
	testl	%ecx, %ecx
	js	.L55
	movl	$65535, %edx
	subl	%ecx, %edx
	cmpl	%esi, %edx
	jl	.L56
.L57:
	addl	%ecx, %eax
	cmpw	$999, %ax
	movw	%ax, 82(%rdi)
	seta	%al
	movzbl	%al, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	movl	%ecx, %edx
	negl	%edx
	cmpl	%esi, %edx
	jle	.L57
.L56:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE70:
	.size	llhttp__internal__c_mul_add_status_code, .-llhttp__internal__c_mul_add_status_code
	.p2align 4
	.globl	llhttp__internal__c_update_type
	.type	llhttp__internal__c_update_type, @function
llhttp__internal__c_update_type:
.LFB71:
	.cfi_startproc
	endbr64
	movb	$1, 72(%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE71:
	.size	llhttp__internal__c_update_type, .-llhttp__internal__c_update_type
	.p2align 4
	.globl	llhttp__internal__c_update_type_1
	.type	llhttp__internal__c_update_type_1, @function
llhttp__internal__c_update_type_1:
.LFB72:
	.cfi_startproc
	endbr64
	movb	$2, 72(%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE72:
	.size	llhttp__internal__c_update_type_1, .-llhttp__internal__c_update_type_1
	.p2align 4
	.globl	llhttp__internal_init
	.type	llhttp__internal_init, @function
llhttp__internal_init:
.LFB73:
	.cfi_startproc
	endbr64
	movq	$0, (%rdi)
	movq	%rdi, %rdx
	leaq	8(%rdi), %rdi
	xorl	%eax, %eax
	movq	$0, 80(%rdi)
	movq	%rdx, %rcx
	andq	$-8, %rdi
	subq	%rdi, %rcx
	addl	$96, %ecx
	shrl	$3, %ecx
	rep stosq
	movq	$157, 56(%rdx)
	ret
	.cfi_endproc
.LFE73:
	.size	llhttp__internal_init, .-llhttp__internal_init
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"Pause on CONNECT/Upgrade"
.LC1:
	.string	"on_message_complete pause"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"`on_message_complete` callback error"
	.align 8
.LC3:
	.string	"Invalid character in chunk size"
	.align 8
.LC4:
	.string	"Request has invalid `Transfer-Encoding`"
	.align 8
.LC5:
	.string	"Missing expected LF after header value"
	.section	.rodata.str1.1
.LC6:
	.string	"Content-Length overflow"
	.section	.rodata.str1.8
	.align 8
.LC7:
	.string	"Invalid character in Content-Length"
	.section	.rodata.str1.1
.LC8:
	.string	"Invalid header token"
.LC9:
	.string	"Invalid minor version"
.LC10:
	.string	"Expected dot"
.LC11:
	.string	"Invalid major version"
	.section	.rodata.str1.8
	.align 8
.LC12:
	.string	"Invalid char in url fragment start"
	.section	.rodata.str1.1
.LC13:
	.string	"Invalid char in url query"
.LC14:
	.string	"Invalid char in url path"
.LC15:
	.string	"Double @ in url"
.LC16:
	.string	"Unexpected char in url server"
.LC17:
	.string	"Unexpected char in url schema"
.LC18:
	.string	"Unexpected start char in url"
.LC19:
	.string	"Expected space after method"
.LC20:
	.string	"Invalid response status"
.LC21:
	.string	"Expected space after version"
.LC22:
	.string	"Invalid characters in url"
.LC23:
	.string	"on_chunk_complete pause"
	.section	.rodata.str1.8
	.align 8
.LC24:
	.string	"`on_chunk_complete` callback error"
	.section	.rodata.str1.1
.LC25:
	.string	"on_chunk_header pause"
	.section	.rodata.str1.8
	.align 8
.LC26:
	.string	"`on_chunk_header` callback error"
	.section	.rodata.str1.1
.LC27:
	.string	"Chunk size overflow"
.LC28:
	.string	"Paused by on_headers_complete"
.LC29:
	.string	"User callback error"
	.section	.rodata.str1.8
	.align 8
.LC30:
	.string	"Content-Length can't be present with chunked encoding"
	.align 8
.LC31:
	.string	"Content-Length can't be present with Transfer-Encoding"
	.section	.rodata.str1.1
.LC32:
	.string	"Empty Content-Length"
.LC33:
	.string	"Invalid header value char"
.LC34:
	.string	"Duplicate Content-Length"
.LC35:
	.string	"Expected CRLF after version"
.LC36:
	.string	"Expected HTTP/"
	.section	.rodata.str1.8
	.align 8
.LC37:
	.string	"Expected SOURCE method for ICE/x.x request"
	.section	.rodata.str1.1
.LC38:
	.string	"Invalid method encountered"
.LC39:
	.string	"Response overflow"
.LC40:
	.string	"Invalid word encountered"
.LC41:
	.string	"on_message_begin pause"
	.section	.rodata.str1.8
	.align 8
.LC42:
	.string	"`on_message_begin` callback error"
	.section	.rodata.str1.1
.LC43:
	.string	"Expected CRLF"
	.section	.text.unlikely,"ax",@progbits
.LCOLDB44:
	.text
.LHOTB44:
	.p2align 4
	.globl	llhttp__internal_execute
	.type	llhttp__internal_execute, @function
llhttp__internal_execute:
.LFB75:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	24(%rdi), %r14d
	testl	%r14d, %r14d
	jne	.L61
	movq	%rsi, %r12
	movq	8(%rdi), %rsi
	movq	%rdi, %rbx
	movq	%rdx, %r13
	testq	%rsi, %rsi
	je	.L63
	movq	%r12, 8(%rdi)
	movq	%r12, %rsi
.L63:
	movq	56(%rbx), %rax
	cmpl	$157, %eax
	ja	.L64
	leaq	.L66(%rip), %rdx
	movl	%eax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L66:
	.long	.L64-.L66
	.long	.L222-.L66
	.long	.L221-.L66
	.long	.L220-.L66
	.long	.L219-.L66
	.long	.L218-.L66
	.long	.L217-.L66
	.long	.L216-.L66
	.long	.L215-.L66
	.long	.L214-.L66
	.long	.L213-.L66
	.long	.L1155-.L66
	.long	.L211-.L66
	.long	.L210-.L66
	.long	.L209-.L66
	.long	.L208-.L66
	.long	.L207-.L66
	.long	.L206-.L66
	.long	.L946-.L66
	.long	.L204-.L66
	.long	.L203-.L66
	.long	.L202-.L66
	.long	.L201-.L66
	.long	.L200-.L66
	.long	.L199-.L66
	.long	.L198-.L66
	.long	.L197-.L66
	.long	.L196-.L66
	.long	.L195-.L66
	.long	.L194-.L66
	.long	.L1158-.L66
	.long	.L192-.L66
	.long	.L191-.L66
	.long	.L190-.L66
	.long	.L189-.L66
	.long	.L188-.L66
	.long	.L187-.L66
	.long	.L1160-.L66
	.long	.L185-.L66
	.long	.L1162-.L66
	.long	.L183-.L66
	.long	.L182-.L66
	.long	.L181-.L66
	.long	.L180-.L66
	.long	.L179-.L66
	.long	.L178-.L66
	.long	.L177-.L66
	.long	.L176-.L66
	.long	.L175-.L66
	.long	.L174-.L66
	.long	.L173-.L66
	.long	.L172-.L66
	.long	.L171-.L66
	.long	.L170-.L66
	.long	.L169-.L66
	.long	.L168-.L66
	.long	.L167-.L66
	.long	.L166-.L66
	.long	.L165-.L66
	.long	.L164-.L66
	.long	.L163-.L66
	.long	.L162-.L66
	.long	.L161-.L66
	.long	.L160-.L66
	.long	.L159-.L66
	.long	.L158-.L66
	.long	.L157-.L66
	.long	.L156-.L66
	.long	.L1165-.L66
	.long	.L154-.L66
	.long	.L153-.L66
	.long	.L152-.L66
	.long	.L151-.L66
	.long	.L150-.L66
	.long	.L149-.L66
	.long	.L148-.L66
	.long	.L147-.L66
	.long	.L146-.L66
	.long	.L145-.L66
	.long	.L144-.L66
	.long	.L143-.L66
	.long	.L142-.L66
	.long	.L141-.L66
	.long	.L140-.L66
	.long	.L139-.L66
	.long	.L138-.L66
	.long	.L137-.L66
	.long	.L1168-.L66
	.long	.L135-.L66
	.long	.L134-.L66
	.long	.L133-.L66
	.long	.L132-.L66
	.long	.L131-.L66
	.long	.L130-.L66
	.long	.L129-.L66
	.long	.L128-.L66
	.long	.L127-.L66
	.long	.L126-.L66
	.long	.L125-.L66
	.long	.L124-.L66
	.long	.L123-.L66
	.long	.L122-.L66
	.long	.L121-.L66
	.long	.L120-.L66
	.long	.L119-.L66
	.long	.L118-.L66
	.long	.L117-.L66
	.long	.L116-.L66
	.long	.L115-.L66
	.long	.L114-.L66
	.long	.L113-.L66
	.long	.L112-.L66
	.long	.L111-.L66
	.long	.L110-.L66
	.long	.L109-.L66
	.long	.L108-.L66
	.long	.L107-.L66
	.long	.L106-.L66
	.long	.L105-.L66
	.long	.L104-.L66
	.long	.L103-.L66
	.long	.L102-.L66
	.long	.L101-.L66
	.long	.L100-.L66
	.long	.L99-.L66
	.long	.L98-.L66
	.long	.L97-.L66
	.long	.L96-.L66
	.long	.L95-.L66
	.long	.L94-.L66
	.long	.L93-.L66
	.long	.L92-.L66
	.long	.L91-.L66
	.long	.L90-.L66
	.long	.L89-.L66
	.long	.L88-.L66
	.long	.L87-.L66
	.long	.L86-.L66
	.long	.L85-.L66
	.long	.L84-.L66
	.long	.L83-.L66
	.long	.L82-.L66
	.long	.L81-.L66
	.long	.L80-.L66
	.long	.L79-.L66
	.long	.L78-.L66
	.long	.L77-.L66
	.long	.L76-.L66
	.long	.L75-.L66
	.long	.L74-.L66
	.long	.L73-.L66
	.long	.L72-.L66
	.long	.L71-.L66
	.long	.L70-.L66
	.long	.L69-.L66
	.long	.L68-.L66
	.long	.L67-.L66
	.long	.L65-.L66
	.text
.L313:
	addq	$1, %r12
.L178:
	cmpq	%r12, %r13
	je	.L1176
.L315:
	movzbl	(%r12), %eax
	cmpb	$13, %al
	je	.L424
	ja	.L425
	cmpb	$9, %al
	je	.L426
	cmpb	$10, %al
	jne	.L179
.L427:
	addq	$1, %r12
.L200:
	cmpq	%r12, %r13
	je	.L1177
	movzbl	(%r12), %eax
	cmpb	$9, %al
	je	.L313
	cmpb	$32, %al
	je	.L313
	movzbl	76(%rbx), %eax
	cmpb	$2, %al
	je	.L1178
	cmpb	$7, %al
	je	.L918
	ja	.L919
	cmpb	$5, %al
	je	.L920
	cmpb	$6, %al
	jne	.L309
	orw	$2, 78(%rbx)
.L917:
	movb	$1, 76(%rbx)
.L309:
	movq	llhttp__on_header_value@GOTPCREL(%rip), %rax
	movq	%r12, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	$0, 8(%rbx)
	movq	%rax, 16(%rbx)
	call	llhttp__on_header_value@PLT
	testl	%eax, %eax
	je	.L165
	movl	%eax, 24(%rbx)
	movl	%eax, %r14d
	movq	%r12, 40(%rbx)
	movq	$58, 56(%rbx)
.L61:
	addq	$8, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L700:
	.cfi_restore_state
	addq	$1, %r12
.L115:
	cmpq	%r12, %r13
	je	.L1179
	movzbl	(%r12), %eax
	cmpb	$65, %al
	je	.L693
	cmpb	$67, %al
	jne	.L641
	addq	$1, %r12
.L116:
	cmpq	%r12, %r13
	je	.L1180
	movzbl	(%r12), %eax
	cmpb	$65, %al
	je	.L690
	cmpb	$79, %al
	jne	.L641
	addq	$1, %r12
.L117:
	cmpq	%r12, %r13
	je	.L1181
	cmpb	$76, (%r12)
	jne	.L641
	addq	$1, %r12
	movl	$10, %eax
	.p2align 4,,10
	.p2align 3
.L642:
	movb	%al, 73(%rbx)
.L135:
	cmpq	%r12, %r13
	je	.L1182
	cmpb	$32, (%r12)
	jne	.L622
	.p2align 4,,10
	.p2align 3
.L1172:
	addq	$1, %r12
.L1168:
	cmpq	%r12, %r13
	je	.L1183
	cmpb	$32, (%r12)
	je	.L1172
	cmpb	$5, 73(%rbx)
	je	.L137
.L138:
	cmpq	%r12, %r13
	je	.L1184
	movq	llhttp__on_url@GOTPCREL(%rip), %rax
	movq	%r12, 8(%rbx)
	movq	%rax, 16(%rbx)
.L614:
	movzbl	(%r12), %eax
	leaq	lookup_table.4128(%rip), %rdx
	movzbl	(%rdx,%rax), %eax
	cmpb	$2, %al
	je	.L573
	cmpb	$3, %al
	je	.L140
	cmpb	$1, %al
	je	.L942
	leaq	.LC18(%rip), %rax
	movl	$7, 24(%rbx)
	movl	$7, %r14d
	movq	%rax, 32(%rbx)
	movq	%r12, 40(%rbx)
	movq	$0, 56(%rbx)
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L236:
	movzbl	(%r12), %eax
	addq	$1, %r12
	cmpb	$13, %al
	je	.L213
.L1155:
	cmpq	%r12, %r13
	jne	.L236
	movq	8(%rbx), %rsi
	movl	$11, %eax
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L622:
	leaq	.LC19(%rip), %rax
	movl	$6, 24(%rbx)
	movl	$6, %r14d
	movq	%rax, 32(%rbx)
	movq	%r12, 40(%rbx)
	movq	$0, 56(%rbx)
	jmp	.L61
.L220:
	cmpb	$1, 80(%rbx)
	je	.L221
.L222:
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	llhttp__after_message_complete@PLT
	movb	$0, 84(%rbx)
.L65:
	cmpq	%r12, %r13
	je	.L1185
.L223:
	movzbl	(%r12), %eax
	cmpb	$10, %al
	je	.L899
	cmpb	$13, %al
	je	.L899
	movb	$2, 84(%rbx)
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	llhttp__on_message_begin@PLT
	testl	%eax, %eax
	je	.L67
	cmpl	$21, %eax
	je	.L902
	leaq	.LC42(%rip), %rax
	movl	$16, 24(%rbx)
	movl	$16, %r14d
	movq	%rax, 32(%rbx)
	movq	%r12, 40(%rbx)
	movq	$0, 56(%rbx)
	jmp	.L61
.L1206:
	addq	$1, %r12
.L198:
	cmpq	%r12, %r13
	je	.L1186
	movzbl	(%r12), %eax
	cmpb	$9, %al
	je	.L179
	cmpb	$32, %al
	je	.L179
	movzbl	76(%rbx), %eax
	cmpb	$7, %al
	je	.L320
	ja	.L321
	cmpb	$5, %al
	je	.L322
	cmpb	$6, %al
	jne	.L165
	orw	$2, 78(%rbx)
.L923:
	movb	$1, 76(%rbx)
	.p2align 4,,10
	.p2align 3
.L165:
	cmpq	%r12, %r13
	je	.L1187
	movzbl	(%r12), %eax
	cmpb	$10, %al
	je	.L306
	cmpb	$13, %al
	jne	.L481
	addq	$1, %r12
.L202:
	cmpq	%r12, %r13
	je	.L1188
.L306:
	movzwl	78(%rbx), %eax
	addq	$1, %r12
	testb	$-128, %al
	jne	.L1189
	movl	%eax, %edx
	andw	$544, %dx
	cmpw	$544, %dx
	je	.L1190
.L905:
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	llhttp__before_headers_complete@PLT
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	llhttp__on_headers_complete@PLT
	cmpl	$2, %eax
	je	.L906
	jg	.L907
	testl	%eax, %eax
	je	.L203
	cmpl	$1, %eax
	jne	.L909
.L1156:
	orw	$64, 78(%rbx)
.L203:
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	llhttp__after_headers_complete@PLT
	cmpl	$5, %eax
	ja	.L297
	leaq	.L299(%rip), %rdx
	movl	%eax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L299:
	.long	.L297-.L299
	.long	.L301-.L299
	.long	.L208-.L299
	.long	.L206-.L299
	.long	.L300-.L299
	.long	.L298-.L299
	.text
	.p2align 4,,10
	.p2align 3
.L471:
	movl	$0, (%rbx)
.L453:
	movb	$0, 76(%rbx)
.L176:
	leaq	lookup_table.3878(%rip), %rdx
	cmpq	%r12, %r13
	jne	.L432
	jmp	.L1191
	.p2align 4,,10
	.p2align 3
.L1193:
	addq	$1, %r12
	cmpq	%r12, %r13
	je	.L1192
.L432:
	movzbl	(%r12), %eax
	cmpb	$1, (%rdx,%rax)
	je	.L1193
.L177:
	cmpq	%r12, %r13
	je	.L1194
	cmpb	$58, (%r12)
	je	.L434
	leaq	.LC8(%rip), %rax
	movl	$10, 24(%rbx)
	movl	$10, %r14d
	movq	%rax, 32(%rbx)
	movq	%r12, 40(%rbx)
	movq	$0, 56(%rbx)
	jmp	.L61
.L401:
	addq	$1, %r12
.L1162:
	cmpq	%r12, %r13
	je	.L1195
	movzbl	(%r12), %eax
	cmpb	$13, %al
	je	.L400
	cmpb	$32, %al
	je	.L401
	cmpb	$10, %al
	jne	.L180
.L400:
	movb	$8, 76(%rbx)
	.p2align 4,,10
	.p2align 3
.L195:
	cmpq	%r12, %r13
	je	.L1196
	movzbl	(%r12), %eax
	cmpb	$10, %al
	je	.L332
	cmpb	$13, %al
	je	.L333
	testb	$1, 79(%rbx)
	jne	.L337
	leaq	.LC33(%rip), %rax
	movl	$10, 24(%rbx)
	movl	$10, %r14d
	movq	%rax, 32(%rbx)
	movq	%r12, 40(%rbx)
	movq	$0, 56(%rbx)
	jmp	.L61
.L147:
	cmpq	%r12, %r13
	je	.L1197
.L573:
	addq	$1, %r12
.L149:
	leaq	lookup_table.4063(%rip), %rdx
	cmpq	%r12, %r13
	jne	.L572
	jmp	.L574
	.p2align 4,,10
	.p2align 3
.L1198:
	addq	$1, %r12
	cmpq	%r12, %r13
	je	.L574
.L572:
	movzbl	(%r12), %eax
	cmpb	$1, (%rdx,%rax)
	je	.L1198
.L150:
	cmpq	%r12, %r13
	je	.L1199
	movzbl	(%r12), %eax
	cmpb	$32, %al
	je	.L582
	ja	.L562
	cmpb	$10, %al
	je	.L584
	cmpb	$13, %al
	jne	.L565
.L583:
	movq	8(%rbx), %rsi
	movq	$0, 8(%rbx)
	movq	%r12, %rdx
	movq	%rbx, %rdi
	call	llhttp__on_url@PLT
	testl	%eax, %eax
	je	.L486
.L586:
	movl	%eax, 24(%rbx)
	movl	%eax, %r14d
	movq	%r12, 40(%rbx)
	movq	$60, 56(%rbx)
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L408:
	addq	$1, %r12
.L183:
	cmpq	%r12, %r13
	je	.L1200
	movzbl	(%r12), %eax
	cmpb	$9, %al
	je	.L408
	cmpb	$32, %al
	je	.L408
.L180:
	cmpq	%r12, %r13
	je	.L1201
.L411:
	movl	(%rbx), %eax
	leaq	llparse_blob7(%rip), %rsi
	.p2align 4,,10
	.p2align 3
.L414:
	movzbl	(%r12), %edx
	movl	%eax, %ecx
	orl	$32, %edx
	cmpb	(%rsi,%rcx), %dl
	jne	.L412
	addl	$1, %eax
	addq	$1, %r12
	cmpl	$7, %eax
	je	.L413
	cmpq	%r12, %r13
	jne	.L414
	movl	%eax, (%rbx)
	movq	8(%rbx), %rsi
	movl	$43, %eax
	jmp	.L205
.L582:
	movq	8(%rbx), %rsi
	movq	$0, 8(%rbx)
	movq	%r12, %rdx
	movq	%rbx, %rdi
	call	llhttp__on_url@PLT
	testl	%eax, %eax
	jne	.L587
	.p2align 4,,10
	.p2align 3
.L1171:
	addq	$1, %r12
.L1165:
	cmpq	%r12, %r13
	je	.L1202
	movzbl	(%r12), %eax
	cmpb	$72, %al
	je	.L533
	cmpb	$73, %al
	je	.L534
	cmpb	$32, %al
	je	.L1171
.L1170:
	leaq	.LC36(%rip), %rax
	movl	$8, 24(%rbx)
	movl	$8, %r14d
	movq	%rax, 32(%rbx)
	movq	%r12, 40(%rbx)
	movq	$0, 56(%rbx)
	jmp	.L61
.L462:
	movl	$0, (%rbx)
	movl	$1, %eax
.L931:
	movb	%al, 76(%rbx)
.L175:
	cmpq	%r12, %r13
	je	.L1203
.L436:
	movzbl	(%r12), %eax
	cmpb	$32, %al
	je	.L433
	cmpb	$58, %al
	jne	.L453
.L434:
	movq	8(%rbx), %rsi
	movq	$0, 8(%rbx)
	movq	%r12, %rdx
	movq	%rbx, %rdi
	addq	$1, %r12
	call	llhttp__on_header_field@PLT
	testl	%eax, %eax
	je	.L178
	movl	%eax, 24(%rbx)
	movl	%eax, %r14d
	movq	%r12, 40(%rbx)
	movq	$45, 56(%rbx)
	jmp	.L61
.L344:
	addq	$1, %r12
	.p2align 4,,10
	.p2align 3
.L1158:
	cmpq	%r12, %r13
	je	.L1204
	movzbl	(%r12), %eax
	cmpb	$32, %al
	je	.L344
	ja	.L345
	cmpb	$10, %al
	je	.L195
	cmpb	$13, %al
	je	.L195
.L346:
	movb	$0, 76(%rbx)
.L194:
	leaq	lookup_table.3764(%rip), %rdx
	cmpq	%r12, %r13
	je	.L379
.L341:
	movzbl	(%r12), %eax
	movzbl	(%rdx,%rax), %eax
	cmpb	$1, %al
	je	.L339
	cmpb	$2, %al
	jne	.L195
	addq	$1, %r12
.L189:
	cmpq	%r12, %r13
	je	.L342
.L420:
	movq	%r12, %rdx
.L343:
	movzbl	(%rdx), %eax
	movq	%rdx, %r12
	movl	%eax, %ecx
	leal	-65(%rax), %esi
	orl	$32, %ecx
	cmpb	$25, %sil
	movzbl	%cl, %ecx
	cmovbe	%ecx, %eax
	cmpl	$99, %eax
	je	.L372
	jg	.L373
	cmpl	$9, %eax
	je	.L375
	cmpl	$32, %eax
	jne	.L194
.L375:
	addq	$1, %rdx
	cmpq	%rdx, %r13
	jne	.L343
.L342:
	movq	8(%rbx), %rsi
	movl	$34, %eax
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L327:
	movq	$0, 8(%rbx)
	movq	%r12, %rdx
	movq	%rbx, %rdi
	call	llhttp__on_header_value@PLT
	testl	%eax, %eax
	jne	.L329
	.p2align 4,,10
	.p2align 3
.L197:
	cmpq	%r12, %r13
	je	.L1205
.L325:
	cmpb	$10, (%r12)
	je	.L1206
	leaq	.LC5(%rip), %rax
	movl	$3, 24(%rbx)
	movl	$3, %r14d
	movq	%rax, 32(%rbx)
	movq	%r12, 40(%rbx)
	movq	$0, 56(%rbx)
	jmp	.L61
.L152:
	cmpq	%r12, %r13
	je	.L1207
.L566:
	addq	$1, %r12
.L153:
	leaq	lookup_table.4024(%rip), %rdx
	cmpq	%r12, %r13
	je	.L571
.L545:
	movzbl	(%r12), %eax
	movzbl	(%rdx,%rax), %eax
	cmpb	$3, %al
	je	.L595
	ja	.L540
	cmpb	$1, %al
	je	.L541
	cmpb	$2, %al
	jne	.L543
.L596:
	movq	8(%rbx), %rsi
	movq	$0, 8(%rbx)
	movq	%r12, %rdx
	movq	%rbx, %rdi
	call	llhttp__on_url@PLT
	testl	%eax, %eax
	je	.L164
.L585:
	movl	%eax, 24(%rbx)
	movl	%eax, %r14d
	movq	%r12, 40(%rbx)
	movq	$59, 56(%rbx)
	jmp	.L61
.L875:
	movl	$4, %eax
.L880:
	movb	%al, 74(%rbx)
.L75:
	cmpq	%r12, %r13
	je	.L1208
	cmpb	$46, (%r12)
	je	.L1209
.L866:
	leaq	.LC10(%rip), %rax
	movl	$9, 24(%rbx)
	movl	$9, %r14d
	movq	%rax, 32(%rbx)
	movq	%r12, 40(%rbx)
	movq	$0, 56(%rbx)
	jmp	.L61
.L858:
	movl	$5, %eax
.L864:
	movb	%al, 75(%rbx)
.L77:
	cmpq	%r12, %r13
	je	.L1210
	cmpb	$32, (%r12)
	je	.L1211
	leaq	.LC21(%rip), %rax
	movl	$9, 24(%rbx)
	movl	$9, %r14d
	movq	%rax, 32(%rbx)
	movq	%r12, 40(%rbx)
	movq	$0, 56(%rbx)
	jmp	.L61
.L516:
	movl	$7, %eax
.L524:
	movb	%al, 74(%rbx)
.L159:
	cmpq	%r12, %r13
	je	.L1212
	cmpb	$46, (%r12)
	jne	.L866
	addq	$1, %r12
.L160:
	cmpq	%r12, %r13
	je	.L1213
	movzbl	(%r12), %eax
	subl	$48, %eax
	cmpb	$9, %al
	ja	.L852
	leaq	.L498(%rip), %rdx
	movzbl	%al, %eax
	addq	$1, %r12
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L498:
	.long	.L507-.L498
	.long	.L506-.L498
	.long	.L505-.L498
	.long	.L504-.L498
	.long	.L503-.L498
	.long	.L502-.L498
	.long	.L501-.L498
	.long	.L500-.L498
	.long	.L499-.L498
	.long	.L497-.L498
	.text
.L499:
	movl	$8, %eax
.L508:
	movb	%al, 75(%rbx)
.L161:
	cmpq	%r12, %r13
	je	.L1214
	movzbl	(%r12), %eax
	cmpb	$10, %al
	je	.L830
	cmpb	$13, %al
	jne	.L491
	addq	$1, %r12
.L162:
	cmpq	%r12, %r13
	je	.L1215
	cmpb	$10, (%r12)
	je	.L830
.L491:
	leaq	.LC35(%rip), %rax
	movl	$9, 24(%rbx)
	movl	$9, %r14d
	movq	%rax, 32(%rbx)
	movq	%r12, 40(%rbx)
	movq	$0, 56(%rbx)
	jmp	.L61
.L208:
	movq	$0, 64(%rbx)
.L209:
	cmpq	%r12, %r13
	je	.L1216
	movzbl	(%r12), %eax
	subl	$48, %eax
	cmpb	$54, %al
	ja	.L268
	leaq	.L270(%rip), %rdx
	movzbl	%al, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L270:
	.long	.L291-.L270
	.long	.L290-.L270
	.long	.L289-.L270
	.long	.L288-.L270
	.long	.L287-.L270
	.long	.L286-.L270
	.long	.L285-.L270
	.long	.L284-.L270
	.long	.L283-.L270
	.long	.L282-.L270
	.long	.L268-.L270
	.long	.L268-.L270
	.long	.L268-.L270
	.long	.L268-.L270
	.long	.L268-.L270
	.long	.L268-.L270
	.long	.L268-.L270
	.long	.L275-.L270
	.long	.L274-.L270
	.long	.L273-.L270
	.long	.L272-.L270
	.long	.L271-.L270
	.long	.L269-.L270
	.long	.L268-.L270
	.long	.L268-.L270
	.long	.L268-.L270
	.long	.L268-.L270
	.long	.L268-.L270
	.long	.L268-.L270
	.long	.L268-.L270
	.long	.L268-.L270
	.long	.L268-.L270
	.long	.L268-.L270
	.long	.L268-.L270
	.long	.L268-.L270
	.long	.L268-.L270
	.long	.L268-.L270
	.long	.L268-.L270
	.long	.L268-.L270
	.long	.L268-.L270
	.long	.L268-.L270
	.long	.L268-.L270
	.long	.L268-.L270
	.long	.L268-.L270
	.long	.L268-.L270
	.long	.L268-.L270
	.long	.L268-.L270
	.long	.L268-.L270
	.long	.L268-.L270
	.long	.L275-.L270
	.long	.L274-.L270
	.long	.L273-.L270
	.long	.L272-.L270
	.long	.L271-.L270
	.long	.L269-.L270
	.text
.L215:
	cmpq	%r12, %r13
	je	.L1217
	movq	llhttp__on_body@GOTPCREL(%rip), %rax
	movq	%r12, 8(%rbx)
	movq	%r12, %rsi
	movq	%rax, 16(%rbx)
.L216:
	movq	%r13, %rdx
	movq	64(%rbx), %rax
	subq	%r12, %rdx
	cmpq	%rax, %rdx
	jnb	.L1218
	subq	%rdx, %rax
	movq	%rax, 64(%rbx)
	movl	$7, %eax
	.p2align 4,,10
	.p2align 3
.L205:
	movq	%rax, 56(%rbx)
	testq	%rsi, %rsi
	je	.L61
	movq	%r13, %rdx
	movq	%rbx, %rdi
	call	*16(%rbx)
	movl	%eax, %r14d
	testl	%eax, %eax
	je	.L61
	movl	%eax, 24(%rbx)
	movq	%r13, 40(%rbx)
	jmp	.L61
.L141:
	cmpq	%r12, %r13
	je	.L1219
	addq	$1, %r12
.L142:
	cmpq	%r12, %r13
	je	.L1220
	movzbl	(%r12), %eax
	cmpb	$32, %al
	je	.L942
	ja	.L605
	cmpb	$10, %al
	je	.L942
	cmpb	$13, %al
	jne	.L602
.L942:
.L609:
	endbr64
	addq	$1, %r12
	leaq	.LC22(%rip), %rax
	movl	$7, 24(%rbx)
	movl	$7, %r14d
	movq	%rax, 32(%rbx)
	movq	%r12, 40(%rbx)
	movq	$0, 56(%rbx)
	jmp	.L61
.L217:
	cmpq	%r12, %r13
	je	.L1221
	addq	$1, %r12
.L218:
	cmpq	%r12, %r13
	je	.L1222
	addq	$1, %r12
	movq	%r13, %rdx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	call	llhttp__on_chunk_complete@PLT
	testl	%eax, %eax
	je	.L208
	cmpl	$21, %eax
	jne	.L1131
	leaq	.LC23(%rip), %rax
	movl	$21, 24(%rbx)
	movl	$21, %r14d
	movq	%rax, 32(%rbx)
	movq	%r12, 40(%rbx)
	movq	$15, 56(%rbx)
	jmp	.L61
.L206:
	cmpq	%r12, %r13
	je	.L1223
	movq	llhttp__on_body@GOTPCREL(%rip), %rax
	movq	%r12, 8(%rbx)
	movq	%r12, %rsi
	movq	%rax, 16(%rbx)
.L207:
	movq	%r13, %rdx
	movq	64(%rbx), %rax
	subq	%r12, %rdx
	cmpq	%rax, %rdx
	jnb	.L1224
	subq	%rdx, %rax
	movq	%rax, 64(%rbx)
	movl	$16, %eax
	jmp	.L205
.L833:
	addq	$1, %r12
.L80:
	cmpq	%r12, %r13
	je	.L1225
.L829:
	movzbl	(%r12), %eax
	cmpb	$10, %al
	je	.L830
	cmpb	$13, %al
	jne	.L827
	addq	$1, %r12
.L83:
	cmpq	%r12, %r13
	je	.L1226
.L830:
	addq	$1, %r12
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L580:
	addq	$1, %r12
.L151:
	cmpq	%r12, %r13
	je	.L588
	leaq	lookup_table.4038(%rip), %rcx
	leaq	.L551(%rip), %rdx
.L556:
	movzbl	(%r12), %eax
	cmpb	$5, (%rcx,%rax)
	ja	.L550
	movzbl	(%rcx,%rax), %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L551:
	.long	.L550-.L551
	.long	.L555-.L551
	.long	.L596-.L551
	.long	.L595-.L551
	.long	.L594-.L551
	.long	.L152-.L551
	.text
.L1311:
	addq	$1, %r12
.L213:
	cmpq	%r12, %r13
	je	.L1227
	addq	$1, %r12
	movq	%r13, %rdx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	call	llhttp__on_chunk_header@PLT
	testl	%eax, %eax
	je	.L214
	cmpl	$21, %eax
	je	.L234
	leaq	.LC26(%rip), %rax
	movl	$19, 24(%rbx)
	movl	$19, %r14d
	movq	%rax, 32(%rbx)
	movq	%r12, 40(%rbx)
	movq	$0, 56(%rbx)
	jmp	.L61
.L1317:
	salq	$4, %rdx
	addq	%rax, %rdx
	movq	%rdx, 64(%rbx)
.L210:
	cmpq	%r12, %r13
	je	.L1228
	movzbl	(%r12), %eax
	subl	$48, %eax
	cmpb	$54, %al
	ja	.L211
	leaq	.L244(%rip), %rdx
	movzbl	%al, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L244:
	.long	.L291-.L244
	.long	.L290-.L244
	.long	.L289-.L244
	.long	.L288-.L244
	.long	.L287-.L244
	.long	.L286-.L244
	.long	.L285-.L244
	.long	.L284-.L244
	.long	.L283-.L244
	.long	.L282-.L244
	.long	.L211-.L244
	.long	.L211-.L244
	.long	.L211-.L244
	.long	.L211-.L244
	.long	.L211-.L244
	.long	.L211-.L244
	.long	.L211-.L244
	.long	.L275-.L244
	.long	.L274-.L244
	.long	.L273-.L244
	.long	.L272-.L244
	.long	.L271-.L244
	.long	.L269-.L244
	.long	.L211-.L244
	.long	.L211-.L244
	.long	.L211-.L244
	.long	.L211-.L244
	.long	.L211-.L244
	.long	.L211-.L244
	.long	.L211-.L244
	.long	.L211-.L244
	.long	.L211-.L244
	.long	.L211-.L244
	.long	.L211-.L244
	.long	.L211-.L244
	.long	.L211-.L244
	.long	.L211-.L244
	.long	.L211-.L244
	.long	.L211-.L244
	.long	.L211-.L244
	.long	.L211-.L244
	.long	.L211-.L244
	.long	.L211-.L244
	.long	.L211-.L244
	.long	.L211-.L244
	.long	.L211-.L244
	.long	.L211-.L244
	.long	.L211-.L244
	.long	.L211-.L244
	.long	.L275-.L244
	.long	.L274-.L244
	.long	.L273-.L244
	.long	.L272-.L244
	.long	.L271-.L244
	.long	.L269-.L244
	.text
.L137:
	cmpq	%r12, %r13
	je	.L1229
	movq	llhttp__on_url@GOTPCREL(%rip), %rax
	movq	%r12, 8(%rbx)
	movq	%rax, 16(%rbx)
.L618:
	leaq	lookup_table.4090(%rip), %rcx
	leaq	.L591(%rip), %rdx
.L600:
	movzbl	(%r12), %eax
	cmpb	$7, (%rcx,%rax)
	ja	.L589
	movzbl	(%rcx,%rax), %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L591:
	.long	.L589-.L591
	.long	.L596-.L591
	.long	.L595-.L591
	.long	.L594-.L591
	.long	.L593-.L591
	.long	.L147-.L591
	.long	.L580-.L591
	.long	.L590-.L591
	.text
.L891:
	movl	$0, (%rbx)
	movb	$2, 72(%rbx)
.L74:
	cmpq	%r12, %r13
	je	.L1230
	movzbl	(%r12), %eax
	subl	$48, %eax
	cmpb	$9, %al
	ja	.L868
	leaq	.L870(%rip), %rdx
	movzbl	%al, %eax
	addq	$1, %r12
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L870:
	.long	.L879-.L870
	.long	.L878-.L870
	.long	.L877-.L870
	.long	.L876-.L870
	.long	.L875-.L870
	.long	.L874-.L870
	.long	.L873-.L870
	.long	.L872-.L870
	.long	.L871-.L870
	.long	.L869-.L870
	.text
.L533:
	addq	$1, %r12
.L157:
	cmpq	%r12, %r13
	je	.L1231
	movl	(%rbx), %eax
	leaq	llparse_blob16(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L528:
	movl	%eax, %edx
	movzbl	(%rcx,%rdx), %edi
	cmpb	%dil, (%r12)
	jne	.L882
	addl	$1, %eax
	addq	$1, %r12
	cmpl	$4, %eax
	je	.L527
	cmpq	%r12, %r13
	jne	.L528
	movl	%eax, (%rbx)
	movq	8(%rbx), %rsi
	movl	$66, %eax
	jmp	.L205
.L534:
	addq	$1, %r12
.L156:
	cmpq	%r12, %r13
	je	.L1232
	movl	(%rbx), %eax
	leaq	llparse_blob17(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L532:
	movl	%eax, %edx
	movzbl	(%rcx,%rdx), %edi
	cmpb	%dil, (%r12)
	jne	.L882
	addl	$1, %eax
	addq	$1, %r12
	cmpl	$3, %eax
	je	.L531
	cmpq	%r12, %r13
	jne	.L532
	movl	%eax, (%rbx)
	movq	8(%rbx), %rsi
	movl	$67, %eax
	jmp	.L205
.L219:
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	llhttp__on_message_complete@PLT
	testl	%eax, %eax
	je	.L220
	cmpl	$21, %eax
	jne	.L1134
	leaq	.LC1(%rip), %rax
	movl	$21, 24(%rbx)
	movl	$21, %r14d
	movq	%rax, 32(%rbx)
	movq	%r12, 40(%rbx)
	movq	$3, 56(%rbx)
	jmp	.L61
.L1209:
	addq	$1, %r12
.L76:
	cmpq	%r12, %r13
	je	.L1233
	movzbl	(%r12), %eax
	subl	$48, %eax
	cmpb	$9, %al
	ja	.L852
	leaq	.L854(%rip), %rdx
	movzbl	%al, %eax
	addq	$1, %r12
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L854:
	.long	.L863-.L854
	.long	.L862-.L854
	.long	.L861-.L854
	.long	.L860-.L854
	.long	.L859-.L854
	.long	.L858-.L854
	.long	.L857-.L854
	.long	.L856-.L854
	.long	.L855-.L854
	.long	.L853-.L854
	.text
.L412:
	movl	$0, (%rbx)
.L181:
	leaq	lookup_table.3852(%rip), %rdx
	cmpq	%r12, %r13
	je	.L416
.L410:
	movzbl	(%r12), %eax
	movzbl	(%rdx,%rax), %eax
	cmpb	$1, %al
	je	.L407
	cmpb	$2, %al
	je	.L408
.L409:
	movb	$0, 76(%rbx)
.L182:
	cmpq	%r12, %r13
	je	.L1234
.L422:
	leaq	lookup_table.3847(%rip), %rdx
	jmp	.L406
	.p2align 4,,10
	.p2align 3
.L1236:
	addq	$1, %r12
	cmpq	%r12, %r13
	je	.L1235
.L406:
	movzbl	(%r12), %eax
	cmpb	$1, (%rdx,%rax)
	je	.L1236
	jmp	.L195
.L595:
	movq	8(%rbx), %rsi
	movq	$0, 8(%rbx)
	movq	%r12, %rdx
	movq	%rbx, %rdi
	call	llhttp__on_url@PLT
	testl	%eax, %eax
	jne	.L586
.L163:
	cmpq	%r12, %r13
	je	.L1237
.L486:
	movl	(%rbx), %eax
	leaq	llparse_blob15(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L489:
	movl	%eax, %edx
	movzbl	(%rcx,%rdx), %edi
	cmpb	%dil, (%r12)
	jne	.L487
	addl	$1, %eax
	addq	$1, %r12
	cmpl	$2, %eax
	je	.L488
	cmpq	%r12, %r13
	jne	.L489
	movl	%eax, (%rbx)
	movq	8(%rbx), %rsi
	movl	$60, %eax
	jmp	.L205
.L527:
	movl	$0, (%rbx)
.L158:
	cmpq	%r12, %r13
	je	.L1238
	movzbl	(%r12), %eax
	subl	$48, %eax
	cmpb	$9, %al
	ja	.L868
	leaq	.L514(%rip), %rdx
	movzbl	%al, %eax
	addq	$1, %r12
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L514:
	.long	.L523-.L514
	.long	.L522-.L514
	.long	.L521-.L514
	.long	.L520-.L514
	.long	.L519-.L514
	.long	.L518-.L514
	.long	.L517-.L514
	.long	.L516-.L514
	.long	.L515-.L514
	.long	.L513-.L514
	.text
.L164:
	cmpq	%r12, %r13
	je	.L1239
.L484:
	addq	$1, %r12
.L485:
	movl	$2304, %edx
	movw	%dx, 74(%rbx)
	jmp	.L165
.L140:
	leaq	lookup_table.4120(%rip), %rdx
	cmpq	%r12, %r13
	je	.L615
.L613:
	movzbl	(%r12), %eax
	movzbl	(%rdx,%rax), %eax
	cmpb	$2, %al
	je	.L141
	cmpb	$3, %al
	je	.L611
	cmpb	$1, %al
	je	.L942
.L602:
	leaq	.LC17(%rip), %rax
	movl	$7, 24(%rbx)
	movl	$7, %r14d
	movq	%rax, 32(%rbx)
	movq	%r12, 40(%rbx)
	movq	$0, 56(%rbx)
	jmp	.L61
.L154:
	cmpq	%r12, %r13
	jne	.L1171
	movq	8(%rbx), %rsi
	movl	$69, %eax
	jmp	.L205
.L1211:
	xorl	%ecx, %ecx
	addq	$1, %r12
	movw	%cx, 82(%rbx)
.L78:
	movq	%r12, %rdx
	leaq	.L837(%rip), %rcx
.L933:
	movq	%rdx, %r12
	cmpq	%rdx, %r13
	je	.L1240
	movzbl	(%rdx), %eax
	subl	$48, %eax
	cmpb	$9, %al
	ja	.L79
	movzbl	%al, %eax
	leaq	1(%rdx), %rsi
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L837:
	.long	.L846-.L837
	.long	.L845-.L837
	.long	.L844-.L837
	.long	.L843-.L837
	.long	.L842-.L837
	.long	.L841-.L837
	.long	.L840-.L837
	.long	.L839-.L837
	.long	.L838-.L837
	.long	.L836-.L837
	.text
.L424:
	addq	$1, %r12
.L199:
	cmpq	%r12, %r13
	jne	.L427
	movq	8(%rbx), %rsi
	movl	$24, %eax
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L179:
	cmpq	%r12, %r13
	je	.L1241
	movq	llhttp__on_header_value@GOTPCREL(%rip), %rax
	movq	%r12, 8(%rbx)
	movq	%rax, 16(%rbx)
	movzbl	76(%rbx), %eax
	cmpb	$3, %al
	je	.L418
	ja	.L419
	cmpb	$1, %al
	je	.L420
	cmpb	$2, %al
	jne	.L422
	testb	$32, 78(%rbx)
	je	.L929
	leaq	.LC34(%rip), %rax
	movl	$4, 24(%rbx)
	movl	$4, %r14d
	movq	%rax, 32(%rbx)
	movq	%r12, 40(%rbx)
	movq	$0, 56(%rbx)
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L221:
	leaq	.LC0(%rip), %rax
	movl	$22, 24(%rbx)
	movl	$22, %r14d
	movq	%rax, 32(%rbx)
	movq	%r12, 40(%rbx)
	movq	$1, 56(%rbx)
	jmp	.L61
.L476:
	addq	$1, %r12
.L169:
	cmpq	%r12, %r13
	je	.L1242
	movl	(%rbx), %edx
	leaq	llparse_blob13(%rip), %rdi
	.p2align 4,,10
	.p2align 3
.L468:
	movzbl	(%r12), %eax
	leal	-65(%rax), %esi
	movl	%eax, %ecx
	orl	$32, %ecx
	cmpb	$26, %sil
	cmovb	%ecx, %eax
	movl	%edx, %ecx
	cmpb	%al, (%rdi,%rcx)
	jne	.L471
	addl	$1, %edx
	addq	$1, %r12
	cmpl	$16, %edx
	je	.L467
	cmpq	%r12, %r13
	jne	.L468
	movl	%edx, (%rbx)
	movq	8(%rbx), %rsi
	movl	$54, %eax
	jmp	.L205
.L167:
	cmpq	%r13, %r12
	je	.L948
	.p2align 4,,10
	.p2align 3
.L482:
	movzbl	(%r12), %eax
	movl	%eax, %edx
	leal	-65(%rax), %ecx
	orl	$32, %edx
	cmpb	$25, %cl
	movzbl	%dl, %edx
	cmovbe	%edx, %eax
	cmpl	$116, %eax
	je	.L476
	jg	.L477
	cmpl	$99, %eax
	je	.L478
	cmpl	$112, %eax
	jne	.L453
	addq	$1, %r12
.L170:
	cmpq	%r12, %r13
	je	.L1243
	movl	(%rbx), %edx
	leaq	llparse_blob12(%rip), %rdi
	.p2align 4,,10
	.p2align 3
.L463:
	movzbl	(%r12), %eax
	leal	-65(%rax), %esi
	movl	%eax, %ecx
	orl	$32, %ecx
	cmpb	$26, %sil
	cmovb	%ecx, %eax
	movl	%edx, %ecx
	cmpb	%al, (%rdi,%rcx)
	jne	.L471
	addl	$1, %edx
	addq	$1, %r12
	cmpl	$15, %edx
	je	.L462
	cmpq	%r12, %r13
	jne	.L463
	movl	%edx, (%rbx)
	movq	8(%rbx), %rsi
	movl	$53, %eax
	jmp	.L205
.L477:
	cmpl	$117, %eax
	jne	.L453
	addq	$1, %r12
.L168:
	cmpq	%r12, %r13
	je	.L1244
	movl	(%rbx), %edx
	leaq	llparse_blob14(%rip), %rdi
	.p2align 4,,10
	.p2align 3
.L473:
	movzbl	(%r12), %eax
	leal	-65(%rax), %esi
	movl	%eax, %ecx
	orl	$32, %ecx
	cmpb	$26, %sil
	cmovb	%ecx, %eax
	movl	%edx, %ecx
	cmpb	%al, (%rdi,%rcx)
	jne	.L471
	addl	$1, %edx
	addq	$1, %r12
	cmpl	$6, %edx
	je	.L472
	cmpq	%r12, %r13
	jne	.L473
	movl	%edx, (%rbx)
	movq	8(%rbx), %rsi
	movl	$55, %eax
	jmp	.L205
.L478:
	addq	$1, %r12
.L171:
	cmpq	%r12, %r13
	je	.L1245
	movl	(%rbx), %edx
	leaq	llparse_blob2(%rip), %rdi
	.p2align 4,,10
	.p2align 3
.L458:
	movzbl	(%r12), %eax
	leal	-65(%rax), %esi
	movl	%eax, %ecx
	orl	$32, %ecx
	cmpb	$26, %sil
	cmovb	%ecx, %eax
	movl	%edx, %ecx
	cmpb	%al, (%rdi,%rcx)
	jne	.L471
	addl	$1, %edx
	addq	$1, %r12
	cmpl	$2, %edx
	je	.L457
	cmpq	%r12, %r13
	jne	.L458
	movl	%edx, (%rbx)
	movq	8(%rbx), %rsi
	movl	$52, %eax
	jmp	.L205
.L67:
	movzbl	72(%rbx), %eax
	cmpb	$1, %al
	je	.L84
	cmpb	$2, %al
	je	.L73
.L68:
	cmpq	%r12, %r13
	je	.L1246
	cmpb	$72, (%r12)
	je	.L898
	movb	$1, 72(%rbx)
.L803:
	movzbl	(%r12), %eax
	subl	$65, %eax
	cmpb	$20, %al
	ja	.L641
	leaq	.L805(%rip), %rdx
	movzbl	%al, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L805:
	.long	.L819-.L805
	.long	.L818-.L805
	.long	.L817-.L805
	.long	.L816-.L805
	.long	.L641-.L805
	.long	.L641-.L805
	.long	.L815-.L805
	.long	.L814-.L805
	.long	.L641-.L805
	.long	.L641-.L805
	.long	.L641-.L805
	.long	.L813-.L805
	.long	.L812-.L805
	.long	.L811-.L805
	.long	.L810-.L805
	.long	.L809-.L805
	.long	.L641-.L805
	.long	.L808-.L805
	.long	.L807-.L805
	.long	.L806-.L805
	.long	.L804-.L805
	.text
.L381:
	addq	$1, %r12
.L1160:
	cmpq	%r12, %r13
	je	.L1247
	movzbl	(%r12), %eax
	cmpb	$13, %al
	je	.L380
	cmpb	$32, %al
	je	.L381
	cmpb	$10, %al
	je	.L380
	movq	8(%rbx), %rsi
	movq	$0, 8(%rbx)
	movq	%r12, %rdx
	movq	%rbx, %rdi
	call	llhttp__on_header_value@PLT
	movl	%eax, %r14d
	testl	%eax, %eax
	je	.L187
	movl	%eax, 24(%rbx)
	movq	%r12, 40(%rbx)
	movq	$36, 56(%rbx)
	jmp	.L61
.L84:
	cmpq	%r12, %r13
	jne	.L803
	movq	8(%rbx), %rsi
	movl	$139, %eax
	jmp	.L205
.L73:
	cmpq	%r12, %r13
	je	.L1248
	movl	(%rbx), %eax
	leaq	llparse_blob48(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L884:
	movl	%eax, %edx
	movzbl	(%rcx,%rdx), %edi
	cmpb	%dil, (%r12)
	jne	.L882
	addl	$1, %eax
	addq	$1, %r12
	cmpl	$5, %eax
	je	.L883
	cmpq	%r12, %r13
	jne	.L884
	movl	%eax, (%rbx)
	movq	8(%rbx), %rsi
	movl	$150, %eax
	jmp	.L205
.L457:
	movl	$0, (%rbx)
.L172:
	cmpq	%r12, %r13
	je	.L1249
	movzbl	(%r12), %eax
	movl	%eax, %edx
	leal	-65(%rax), %ecx
	orl	$32, %edx
	cmpb	$25, %cl
	movzbl	%dl, %edx
	cmovbe	%edx, %eax
	cmpl	$110, %eax
	je	.L451
	cmpl	$116, %eax
	jne	.L453
	addq	$1, %r12
.L173:
	cmpq	%r12, %r13
	je	.L1250
	movl	(%rbx), %edx
	leaq	llparse_blob11(%rip), %rdi
	.p2align 4,,10
	.p2align 3
.L447:
	movzbl	(%r12), %eax
	leal	-65(%rax), %esi
	movl	%eax, %ecx
	orl	$32, %ecx
	cmpb	$26, %sil
	cmovb	%ecx, %eax
	movl	%edx, %ecx
	cmpb	%al, (%rdi,%rcx)
	jne	.L471
	addl	$1, %edx
	addq	$1, %r12
	cmpl	$10, %edx
	je	.L446
	cmpq	%r12, %r13
	jne	.L447
	movl	%edx, (%rbx)
	movq	8(%rbx), %rsi
	movl	$50, %eax
	jmp	.L205
.L188:
	leaq	.LC6(%rip), %rax
	movl	$11, 24(%rbx)
	movl	$11, %r14d
	movq	%rax, 32(%rbx)
	movq	%r12, 40(%rbx)
	movq	$0, 56(%rbx)
	jmp	.L61
.L451:
	addq	$1, %r12
.L174:
	cmpq	%r12, %r13
	je	.L1251
	movl	(%rbx), %edx
	leaq	llparse_blob3(%rip), %rdi
	.p2align 4,,10
	.p2align 3
.L442:
	movzbl	(%r12), %eax
	leal	-65(%rax), %esi
	movl	%eax, %ecx
	orl	$32, %ecx
	cmpb	$26, %sil
	cmovb	%ecx, %eax
	movl	%edx, %ecx
	cmpb	%al, (%rdi,%rcx)
	jne	.L471
	addl	$1, %edx
	addq	$1, %r12
	cmpl	$6, %edx
	je	.L462
	cmpq	%r12, %r13
	jne	.L442
	movl	%edx, (%rbx)
	movq	8(%rbx), %rsi
	movl	$49, %eax
	jmp	.L205
.L898:
	addq	$1, %r12
.L70:
	cmpq	%r12, %r13
	je	.L1252
	movzbl	(%r12), %eax
	cmpb	$69, %al
	je	.L894
	cmpb	$84, %al
	jne	.L896
	addq	$1, %r12
.L71:
	cmpq	%r12, %r13
	je	.L1253
	movl	(%rbx), %eax
	leaq	llparse_blob50(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L892:
	movl	%eax, %edx
	movzbl	(%rcx,%rdx), %edi
	cmpb	%dil, (%r12)
	jne	.L890
	addl	$1, %eax
	addq	$1, %r12
	cmpl	$3, %eax
	je	.L891
	cmpq	%r12, %r13
	jne	.L892
	movl	%eax, (%rbx)
	movq	8(%rbx), %rsi
	movl	$152, %eax
	jmp	.L205
.L214:
	cmpq	$0, 64(%rbx)
	jne	.L215
	orw	$128, 78(%rbx)
	jmp	.L165
.L744:
	addq	$1, %r12
.L104:
	cmpq	%r12, %r13
	je	.L1254
	movzbl	(%r12), %eax
	cmpb	$73, %al
	je	.L734
	cmpb	$79, %al
	jne	.L641
	addq	$1, %r12
.L105:
	cmpq	%r12, %r13
	je	.L1255
	cmpb	$80, (%r12)
	jne	.L641
	addq	$1, %r12
.L106:
	cmpq	%r12, %r13
	je	.L1256
	movzbl	(%r12), %eax
	cmpb	$70, %al
	je	.L730
	cmpb	$80, %al
	jne	.L641
	addq	$1, %r12
.L107:
	cmpq	%r12, %r13
	je	.L1257
	movl	(%rbx), %eax
	leaq	llparse_blob36(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L728:
	movl	%eax, %edx
	movzbl	(%rcx,%rdx), %edi
	cmpb	%dil, (%r12)
	jne	.L796
	addl	$1, %eax
	addq	$1, %r12
	cmpl	$4, %eax
	je	.L727
	cmpq	%r12, %r13
	jne	.L728
	movl	%eax, (%rbx)
	movq	8(%rbx), %rsi
	movl	$116, %eax
	jmp	.L205
.L730:
	addq	$1, %r12
.L108:
	cmpq	%r12, %r13
	je	.L1258
	movl	(%rbx), %eax
	leaq	llparse_blob35(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L724:
	movl	%eax, %edx
	movzbl	(%rcx,%rdx), %edi
	cmpb	%dil, (%r12)
	jne	.L796
	addl	$1, %eax
	addq	$1, %r12
	cmpl	$3, %eax
	je	.L723
	cmpq	%r12, %r13
	jne	.L724
	movl	%eax, (%rbx)
	movq	8(%rbx), %rsi
	movl	$115, %eax
	jmp	.L205
.L894:
	addq	$1, %r12
.L72:
	cmpq	%r12, %r13
	je	.L1259
	movl	(%rbx), %eax
	leaq	llparse_blob49(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L888:
	movl	%eax, %edx
	movzbl	(%rcx,%rdx), %edi
	cmpb	%dil, (%r12)
	jne	.L890
	addl	$1, %eax
	addq	$1, %r12
	cmpl	$2, %eax
	je	.L887
	cmpq	%r12, %r13
	jne	.L888
	movl	%eax, (%rbx)
	movq	8(%rbx), %rsi
	movl	$151, %eax
	jmp	.L205
.L605:
	cmpb	$47, %al
	jne	.L602
	addq	$1, %r12
.L143:
	cmpq	%r12, %r13
	je	.L1260
	cmpb	$47, (%r12)
	jne	.L602
.L1166:
	addq	$1, %r12
.L144:
	cmpq	%r12, %r13
	jne	.L618
	movq	8(%rbx), %rsi
	movl	$79, %eax
	jmp	.L205
.L590:
	addq	$1, %r12
.L145:
	cmpq	%r12, %r13
	je	.L1261
	movzbl	(%r12), %eax
	leaq	lookup_table.4074(%rip), %rdx
	cmpb	$7, (%rdx,%rax)
	ja	.L589
	movzbl	(%rdx,%rax), %eax
	leaq	.L579(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L579:
	.long	.L589-.L579
	.long	.L584-.L579
	.long	.L583-.L579
	.long	.L582-.L579
	.long	.L1166-.L579
	.long	.L146-.L579
	.long	.L580-.L579
	.long	.L578-.L579
	.text
.L800:
	addq	$1, %r12
.L88:
	cmpq	%r12, %r13
	je	.L1262
	movzbl	(%r12), %eax
	cmpb	$73, %al
	je	.L793
	cmpb	$79, %al
	jne	.L641
	addq	$1, %r12
.L89:
	cmpq	%r12, %r13
	je	.L1263
	movl	(%rbx), %eax
	leaq	llparse_blob46(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L791:
	movl	%eax, %edx
	movzbl	(%rcx,%rdx), %edi
	cmpb	%dil, (%r12)
	jne	.L796
	addl	$1, %eax
	addq	$1, %r12
	cmpl	$2, %eax
	je	.L790
	cmpq	%r12, %r13
	jne	.L791
	movl	%eax, (%rbx)
	movq	8(%rbx), %rsi
	movl	$134, %eax
	jmp	.L205
.L690:
	addq	$1, %r12
.L118:
	cmpq	%r12, %r13
	je	.L1264
	movl	(%rbx), %eax
	leaq	llparse_blob29(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L687:
	movl	%eax, %edx
	movzbl	(%rcx,%rdx), %edi
	cmpb	%dil, (%r12)
	jne	.L796
	addl	$1, %eax
	addq	$1, %r12
	cmpl	$6, %eax
	je	.L686
	cmpq	%r12, %r13
	jne	.L687
	movl	%eax, (%rbx)
	movq	8(%rbx), %rsi
	movl	$105, %eax
	jmp	.L205
.L817:
	addq	$1, %r12
.L128:
	cmpq	%r12, %r13
	je	.L1265
	movzbl	(%r12), %eax
	cmpb	$72, %al
	je	.L647
	cmpb	$79, %al
	jne	.L641
	addq	$1, %r12
.L129:
	cmpq	%r12, %r13
	je	.L1266
	movzbl	(%r12), %eax
	cmpb	$78, %al
	je	.L644
	cmpb	$80, %al
	jne	.L641
	addq	$1, %r12
.L130:
	cmpq	%r12, %r13
	je	.L1267
	cmpb	$89, (%r12)
	je	.L1268
	.p2align 4,,10
	.p2align 3
.L641:
	leaq	.LC38(%rip), %rax
	movl	$6, 24(%rbx)
	movl	$6, %r14d
	movq	%rax, 32(%rbx)
	movq	%r12, 40(%rbx)
	movq	$0, 56(%rbx)
	jmp	.L61
.L644:
	addq	$1, %r12
.L131:
	cmpq	%r12, %r13
	je	.L1269
	movl	(%rbx), %eax
	leaq	llparse_blob20(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L639:
	movl	%eax, %edx
	movzbl	(%rcx,%rdx), %edi
	cmpb	%dil, (%r12)
	jne	.L796
	addl	$1, %eax
	addq	$1, %r12
	cmpl	$4, %eax
	je	.L638
	cmpq	%r12, %r13
	jne	.L639
	movl	%eax, (%rbx)
	movq	8(%rbx), %rsi
	movl	$92, %eax
	jmp	.L205
.L793:
	addq	$1, %r12
.L90:
	cmpq	%r12, %r13
	je	.L1270
	movl	(%rbx), %eax
	leaq	llparse_blob45(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L787:
	movl	%eax, %edx
	movzbl	(%rcx,%rdx), %edi
	cmpb	%dil, (%r12)
	jne	.L796
	addl	$1, %eax
	addq	$1, %r12
	cmpl	$2, %eax
	je	.L786
	cmpq	%r12, %r13
	jne	.L787
	movl	%eax, (%rbx)
	movq	8(%rbx), %rsi
	movl	$133, %eax
	jmp	.L205
.L758:
	addq	$1, %r12
.L100:
	cmpq	%r12, %r13
	je	.L1271
	movl	(%rbx), %eax
	leaq	llparse_blob38(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L752:
	movl	%eax, %edx
	movzbl	(%rcx,%rdx), %edi
	cmpb	%dil, (%r12)
	jne	.L796
	addl	$1, %eax
	addq	$1, %r12
	cmpl	$3, %eax
	je	.L751
	cmpq	%r12, %r13
	jne	.L752
	movl	%eax, (%rbx)
	movq	8(%rbx), %rsi
	movl	$123, %eax
	jmp	.L205
.L808:
	addq	$1, %r12
.L97:
	cmpq	%r12, %r13
	je	.L1272
	cmpb	$69, (%r12)
	jne	.L641
	addq	$1, %r12
.L98:
	cmpq	%r12, %r13
	je	.L1273
	movzbl	(%r12), %eax
	cmpb	$66, %al
	je	.L758
	cmpb	$80, %al
	jne	.L641
	addq	$1, %r12
.L99:
	cmpq	%r12, %r13
	je	.L1274
	movl	(%rbx), %eax
	leaq	llparse_blob39(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L756:
	movl	%eax, %edx
	movzbl	(%rcx,%rdx), %edi
	cmpb	%dil, (%r12)
	jne	.L796
	addl	$1, %eax
	addq	$1, %r12
	cmpl	$3, %eax
	je	.L755
	cmpq	%r12, %r13
	jne	.L756
	movl	%eax, (%rbx)
	movq	8(%rbx), %rsi
	movl	$124, %eax
	jmp	.L205
.L300:
	movb	$1, 84(%rbx)
.L204:
	cmpq	%r12, %r13
	je	.L1275
	movq	llhttp__on_body@GOTPCREL(%rip), %rax
	movq	%r12, 8(%rbx)
	movq	%r12, %rsi
	movq	%rax, 16(%rbx)
	movl	$18, %eax
	jmp	.L205
.L804:
	addq	$1, %r12
.L85:
	cmpq	%r12, %r13
	je	.L1276
	cmpb	$78, (%r12)
	jne	.L641
	addq	$1, %r12
.L86:
	cmpq	%r12, %r13
	je	.L1277
	movzbl	(%r12), %eax
	cmpb	$76, %al
	je	.L800
	cmpb	$83, %al
	je	.L801
	cmpb	$66, %al
	jne	.L641
	addq	$1, %r12
.L91:
	cmpq	%r12, %r13
	je	.L1278
	movl	(%rbx), %eax
	leaq	llparse_blob44(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L783:
	movl	%eax, %edx
	movzbl	(%rcx,%rdx), %edi
	cmpb	%dil, (%r12)
	jne	.L796
	addl	$1, %eax
	addq	$1, %r12
	cmpl	$3, %eax
	je	.L782
	cmpq	%r12, %r13
	jne	.L783
	movl	%eax, (%rbx)
	movq	8(%rbx), %rsi
	movl	$132, %eax
	jmp	.L205
.L693:
	addq	$1, %r12
.L119:
	cmpq	%r12, %r13
	je	.L1279
	movl	(%rbx), %eax
	leaq	llparse_blob28(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L683:
	movl	%eax, %edx
	movzbl	(%rcx,%rdx), %edi
	cmpb	%dil, (%r12)
	jne	.L796
	addl	$1, %eax
	addq	$1, %r12
	cmpl	$7, %eax
	je	.L682
	cmpq	%r12, %r13
	jne	.L683
	movl	%eax, (%rbx)
	movq	8(%rbx), %rsi
	movl	$104, %eax
	jmp	.L205
.L813:
	addq	$1, %r12
.L122:
	cmpq	%r12, %r13
	je	.L1280
	movzbl	(%r12), %eax
	cmpb	$73, %al
	je	.L670
	cmpb	$79, %al
	jne	.L641
	addq	$1, %r12
.L123:
	cmpq	%r12, %r13
	je	.L1281
	movl	(%rbx), %eax
	leaq	llparse_blob25(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L668:
	movl	%eax, %edx
	movzbl	(%rcx,%rdx), %edi
	cmpb	%dil, (%r12)
	jne	.L796
	addl	$1, %eax
	addq	$1, %r12
	cmpl	$2, %eax
	je	.L667
	cmpq	%r12, %r13
	jne	.L668
	movl	%eax, (%rbx)
	movq	8(%rbx), %rsi
	movl	$100, %eax
	jmp	.L205
.L741:
	addq	$1, %r12
.L103:
	cmpq	%r12, %r13
	je	.L1282
	movl	(%rbx), %eax
	leaq	llparse_blob37(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L739:
	movl	%eax, %edx
	movzbl	(%rcx,%rdx), %edi
	cmpb	%dil, (%r12)
	jne	.L796
	addl	$1, %eax
	addq	$1, %r12
	cmpl	$2, %eax
	je	.L738
	cmpq	%r12, %r13
	jne	.L739
	movl	%eax, (%rbx)
	movq	8(%rbx), %rsi
	movl	$120, %eax
	jmp	.L205
.L670:
	addq	$1, %r12
.L124:
	cmpq	%r12, %r13
	je	.L1283
	movl	(%rbx), %eax
	leaq	llparse_blob24(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L664:
	movl	%eax, %edx
	movzbl	(%rcx,%rdx), %edi
	cmpb	%dil, (%r12)
	jne	.L796
	addl	$1, %eax
	addq	$1, %r12
	cmpl	$2, %eax
	je	.L663
	cmpq	%r12, %r13
	jne	.L664
	movl	%eax, (%rbx)
	movq	8(%rbx), %rsi
	movl	$99, %eax
	jmp	.L205
.L647:
	addq	$1, %r12
.L132:
	cmpq	%r12, %r13
	je	.L1284
	movl	(%rbx), %eax
	leaq	llparse_blob19(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L635:
	movl	%eax, %edx
	movzbl	(%rcx,%rdx), %edi
	cmpb	%dil, (%r12)
	jne	.L796
	addl	$1, %eax
	addq	$1, %r12
	cmpl	$6, %eax
	je	.L634
	cmpq	%r12, %r13
	jne	.L635
	movl	%eax, (%rbx)
	movq	8(%rbx), %rsi
	movl	$91, %eax
	jmp	.L205
.L376:
	addq	$1, %r12
.L191:
	cmpq	%r12, %r13
	je	.L1285
	movl	(%rbx), %edx
	leaq	llparse_blob5(%rip), %rdi
	.p2align 4,,10
	.p2align 3
.L363:
	movzbl	(%r12), %eax
	leal	-65(%rax), %esi
	movl	%eax, %ecx
	orl	$32, %ecx
	cmpb	$26, %sil
	cmovb	%ecx, %eax
	movl	%edx, %ecx
	cmpb	%al, (%rdi,%rcx)
	jne	.L367
	addl	$1, %edx
	addq	$1, %r12
	cmpl	$9, %edx
	je	.L362
	cmpq	%r12, %r13
	jne	.L363
	movl	%edx, (%rbx)
	movq	8(%rbx), %rsi
	movl	$32, %eax
	jmp	.L205
.L801:
	addq	$1, %r12
.L87:
	cmpq	%r12, %r13
	je	.L1286
	movl	(%rbx), %eax
	leaq	llparse_blob47(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L798:
	movl	%eax, %edx
	movzbl	(%rcx,%rdx), %edi
	cmpb	%dil, (%r12)
	jne	.L796
	addl	$1, %eax
	addq	$1, %r12
	cmpl	$8, %eax
	je	.L797
	cmpq	%r12, %r13
	jne	.L798
	movl	%eax, (%rbx)
	movq	8(%rbx), %rsi
	movl	$136, %eax
	jmp	.L205
.L372:
	addq	$1, %r12
.L192:
	cmpq	%r12, %r13
	je	.L1287
	movl	(%rbx), %edx
	leaq	llparse_blob4(%rip), %rdi
	.p2align 4,,10
	.p2align 3
.L358:
	movzbl	(%r12), %eax
	leal	-65(%rax), %esi
	movl	%eax, %ecx
	orl	$32, %ecx
	cmpb	$26, %sil
	cmovb	%ecx, %eax
	movl	%edx, %ecx
	cmpb	%al, (%rdi,%rcx)
	jne	.L367
	addl	$1, %edx
	addq	$1, %r12
	cmpl	$4, %edx
	je	.L357
	cmpq	%r12, %r13
	jne	.L358
	movl	%edx, (%rbx)
	movq	8(%rbx), %rsi
	movl	$31, %eax
	jmp	.L205
.L373:
	cmpl	$107, %eax
	je	.L376
	cmpl	$117, %eax
	jne	.L194
	addq	$1, %r12
.L190:
	cmpq	%r12, %r13
	je	.L1288
	movl	(%rbx), %edx
	leaq	llparse_blob6(%rip), %rdi
	.p2align 4,,10
	.p2align 3
.L369:
	movzbl	(%r12), %eax
	leal	-65(%rax), %esi
	movl	%eax, %ecx
	orl	$32, %ecx
	cmpb	$26, %sil
	cmovb	%ecx, %eax
	movl	%edx, %ecx
	cmpb	%al, (%rdi,%rcx)
	jne	.L367
	addl	$1, %edx
	addq	$1, %r12
	cmpl	$6, %edx
	je	.L368
	cmpq	%r12, %r13
	jne	.L369
	movl	%edx, (%rbx)
	movq	8(%rbx), %rsi
	movl	$33, %eax
	jmp	.L205
.L775:
	addq	$1, %r12
.L94:
	cmpq	%r12, %r13
	je	.L1289
	movl	(%rbx), %eax
	leaq	llparse_blob42(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L772:
	movl	%eax, %edx
	movzbl	(%rcx,%rdx), %edi
	cmpb	%dil, (%r12)
	jne	.L796
	addl	$1, %eax
	addq	$1, %r12
	cmpl	$7, %eax
	je	.L771
	cmpq	%r12, %r13
	jne	.L772
	movl	%eax, (%rbx)
	movq	8(%rbx), %rsi
	movl	$129, %eax
	jmp	.L205
.L774:
	addq	$1, %r12
.L95:
	cmpq	%r12, %r13
	je	.L1290
	movl	(%rbx), %eax
	leaq	llparse_blob41(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L768:
	movl	%eax, %edx
	movzbl	(%rcx,%rdx), %edi
	cmpb	%dil, (%r12)
	jne	.L796
	addl	$1, %eax
	addq	$1, %r12
	cmpl	$4, %eax
	je	.L767
	cmpq	%r12, %r13
	jne	.L768
	movl	%eax, (%rbx)
	movq	8(%rbx), %rsi
	movl	$128, %eax
	jmp	.L205
.L807:
	addq	$1, %r12
.L93:
	cmpq	%r12, %r13
	je	.L1291
	movzbl	(%r12), %eax
	cmpb	$79, %al
	je	.L774
	cmpb	$85, %al
	je	.L775
	cmpb	$69, %al
	jne	.L641
	addq	$1, %r12
.L96:
	cmpq	%r12, %r13
	je	.L1292
	movl	(%rbx), %eax
	leaq	llparse_blob40(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L764:
	movl	%eax, %edx
	movzbl	(%rcx,%rdx), %edi
	cmpb	%dil, (%r12)
	jne	.L796
	addl	$1, %eax
	addq	$1, %r12
	cmpl	$4, %eax
	je	.L763
	cmpq	%r12, %r13
	jne	.L764
	movl	%eax, (%rbx)
	movq	8(%rbx), %rsi
	movl	$127, %eax
	jmp	.L205
.L701:
	cmpb	$79, %al
	jne	.L641
	addq	$1, %r12
.L114:
	cmpq	%r12, %r13
	je	.L1293
	movl	(%rbx), %eax
	leaq	llparse_blob30(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L698:
	movl	%eax, %edx
	movzbl	(%rcx,%rdx), %edi
	cmpb	%dil, (%r12)
	jne	.L796
	addl	$1, %eax
	addq	$1, %r12
	cmpl	$2, %eax
	je	.L697
	cmpq	%r12, %r13
	jne	.L698
	movl	%eax, (%rbx)
	movq	8(%rbx), %rsi
	movl	$109, %eax
	jmp	.L205
.L746:
	addq	$1, %r12
.L110:
	cmpq	%r12, %r13
	je	.L1294
	movl	(%rbx), %eax
	leaq	llparse_blob33(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L716:
	movl	%eax, %edx
	movzbl	(%rcx,%rdx), %edi
	cmpb	%dil, (%r12)
	jne	.L796
	addl	$1, %eax
	addq	$1, %r12
	cmpl	$3, %eax
	je	.L715
	cmpq	%r12, %r13
	jne	.L716
	movl	%eax, (%rbx)
	movq	8(%rbx), %rsi
	movl	$113, %eax
	jmp	.L205
.L745:
	cmpb	$85, %al
	jne	.L641
	addq	$1, %r12
.L102:
	cmpq	%r12, %r13
	je	.L1295
	movzbl	(%r12), %eax
	cmpb	$82, %al
	je	.L741
	cmpb	$84, %al
	jne	.L641
	addq	$1, %r12
	movl	$4, %eax
	jmp	.L642
.L812:
	addq	$1, %r12
.L113:
	cmpq	%r12, %r13
	je	.L1296
	movzbl	(%r12), %eax
	cmpb	$75, %al
	je	.L700
	ja	.L701
	cmpb	$45, %al
	je	.L702
	cmpb	$69, %al
	jne	.L641
	addq	$1, %r12
.L120:
	cmpq	%r12, %r13
	je	.L1297
	movl	(%rbx), %eax
	leaq	llparse_blob27(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L679:
	movl	%eax, %edx
	movzbl	(%rcx,%rdx), %edi
	cmpb	%dil, (%r12)
	jne	.L796
	addl	$1, %eax
	addq	$1, %r12
	cmpl	$3, %eax
	je	.L678
	cmpq	%r12, %r13
	jne	.L679
	movl	%eax, (%rbx)
	movq	8(%rbx), %rsi
	movl	$103, %eax
	jmp	.L205
.L809:
	addq	$1, %r12
.L101:
	cmpq	%r12, %r13
	je	.L1298
	movzbl	(%r12), %eax
	cmpb	$82, %al
	je	.L744
	ja	.L745
	cmpb	$65, %al
	je	.L746
	cmpb	$79, %al
	jne	.L641
	addq	$1, %r12
.L109:
	cmpq	%r12, %r13
	je	.L1299
	movl	(%rbx), %eax
	leaq	llparse_blob34(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L720:
	movl	%eax, %edx
	movzbl	(%rcx,%rdx), %edi
	cmpb	%dil, (%r12)
	jne	.L796
	addl	$1, %eax
	addq	$1, %r12
	cmpl	$2, %eax
	je	.L719
	cmpq	%r12, %r13
	jne	.L720
	movl	%eax, (%rbx)
	movq	8(%rbx), %rsi
	movl	$114, %eax
	jmp	.L205
.L702:
	addq	$1, %r12
.L121:
	cmpq	%r12, %r13
	je	.L1300
	movl	(%rbx), %eax
	leaq	llparse_blob26(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L675:
	movl	%eax, %edx
	movzbl	(%rcx,%rdx), %edi
	cmpb	%dil, (%r12)
	jne	.L796
	addl	$1, %eax
	addq	$1, %r12
	cmpl	$6, %eax
	je	.L674
	cmpq	%r12, %r13
	jne	.L675
	movl	%eax, (%rbx)
	movq	8(%rbx), %rsi
	movl	$102, %eax
	jmp	.L205
.L79:
	cmpq	%r12, %r13
	je	.L1301
	movzbl	(%r12), %eax
	cmpb	$13, %al
	je	.L829
	cmpb	$32, %al
	je	.L833
	cmpb	$10, %al
	je	.L829
	leaq	.LC20(%rip), %rax
	movl	$13, 24(%rbx)
	movl	$13, %r14d
	movq	%rax, 32(%rbx)
	movq	%r12, 40(%rbx)
	movq	$0, 56(%rbx)
	jmp	.L61
.L187:
	leaq	.LC7(%rip), %rax
	movl	$11, 24(%rbx)
	movl	$11, %r14d
	movq	%rax, 32(%rbx)
	movq	%r12, 40(%rbx)
	movq	$0, 56(%rbx)
	jmp	.L61
.L146:
	cmpq	%r12, %r13
	jne	.L573
	movq	8(%rbx), %rsi
	movl	$77, %eax
	jmp	.L205
.L814:
	addq	$1, %r12
.L125:
	cmpq	%r12, %r13
	je	.L1302
	movl	(%rbx), %eax
	leaq	llparse_blob23(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L660:
	movl	%eax, %edx
	movzbl	(%rcx,%rdx), %edi
	cmpb	%dil, (%r12)
	jne	.L796
	addl	$1, %eax
	addq	$1, %r12
	cmpl	$3, %eax
	je	.L659
	cmpq	%r12, %r13
	jne	.L660
	movl	%eax, (%rbx)
	movq	8(%rbx), %rsi
	movl	$98, %eax
	jmp	.L205
.L815:
	addq	$1, %r12
.L126:
	cmpq	%r12, %r13
	je	.L1303
	movl	(%rbx), %eax
	leaq	llparse_blob22(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L656:
	movl	%eax, %edx
	movzbl	(%rcx,%rdx), %edi
	cmpb	%dil, (%r12)
	jne	.L796
	addl	$1, %eax
	addq	$1, %r12
	cmpl	$2, %eax
	je	.L655
	cmpq	%r12, %r13
	jne	.L656
	movl	%eax, (%rbx)
	movq	8(%rbx), %rsi
	movl	$97, %eax
	jmp	.L205
.L819:
	addq	$1, %r12
.L134:
	cmpq	%r12, %r13
	je	.L1304
	movl	(%rbx), %eax
	leaq	llparse_blob0(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L627:
	movl	%eax, %edx
	movzbl	(%rcx,%rdx), %edi
	cmpb	%dil, (%r12)
	jne	.L796
	addl	$1, %eax
	addq	$1, %r12
	cmpl	$2, %eax
	je	.L626
	cmpq	%r12, %r13
	jne	.L627
	movl	%eax, (%rbx)
	movq	8(%rbx), %rsi
	movl	$89, %eax
	jmp	.L205
.L818:
	addq	$1, %r12
.L133:
	cmpq	%r12, %r13
	je	.L1305
	movl	(%rbx), %eax
	leaq	llparse_blob18(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L631:
	movl	%eax, %edx
	movzbl	(%rcx,%rdx), %edi
	cmpb	%dil, (%r12)
	jne	.L796
	addl	$1, %eax
	addq	$1, %r12
	cmpl	$3, %eax
	je	.L630
	cmpq	%r12, %r13
	jne	.L631
	movl	%eax, (%rbx)
	movq	8(%rbx), %rsi
	movl	$90, %eax
	jmp	.L205
.L816:
	addq	$1, %r12
.L127:
	cmpq	%r12, %r13
	je	.L1306
	movl	(%rbx), %eax
	leaq	llparse_blob21(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L652:
	movl	%eax, %edx
	movzbl	(%rcx,%rdx), %edi
	cmpb	%dil, (%r12)
	jne	.L796
	addl	$1, %eax
	addq	$1, %r12
	cmpl	$5, %eax
	je	.L651
	cmpq	%r12, %r13
	jne	.L652
	movl	%eax, (%rbx)
	movq	8(%rbx), %rsi
	movl	$96, %eax
	jmp	.L205
.L811:
	addq	$1, %r12
.L112:
	cmpq	%r12, %r13
	je	.L1307
	movl	(%rbx), %eax
	leaq	llparse_blob31(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L708:
	movl	%eax, %edx
	movzbl	(%rcx,%rdx), %edi
	cmpb	%dil, (%r12)
	jne	.L796
	addl	$1, %eax
	addq	$1, %r12
	cmpl	$5, %eax
	je	.L707
	cmpq	%r12, %r13
	jne	.L708
	movl	%eax, (%rbx)
	movq	8(%rbx), %rsi
	movl	$111, %eax
	jmp	.L205
.L806:
	addq	$1, %r12
.L92:
	cmpq	%r12, %r13
	je	.L1308
	movl	(%rbx), %eax
	leaq	llparse_blob43(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L779:
	movl	%eax, %edx
	movzbl	(%rcx,%rdx), %edi
	cmpb	%dil, (%r12)
	jne	.L796
	addl	$1, %eax
	addq	$1, %r12
	cmpl	$4, %eax
	je	.L778
	cmpq	%r12, %r13
	jne	.L779
	movl	%eax, (%rbx)
	movq	8(%rbx), %rsi
	movl	$131, %eax
	jmp	.L205
.L810:
	addq	$1, %r12
.L111:
	cmpq	%r12, %r13
	je	.L1309
	movl	(%rbx), %eax
	leaq	llparse_blob32(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L712:
	movl	%eax, %edx
	movzbl	(%rcx,%rdx), %edi
	cmpb	%dil, (%r12)
	jne	.L796
	addl	$1, %eax
	addq	$1, %r12
	cmpl	$6, %eax
	je	.L711
	cmpq	%r12, %r13
	jne	.L712
	movl	%eax, (%rbx)
	movq	8(%rbx), %rsi
	movl	$112, %eax
	jmp	.L205
.L211:
	cmpq	%r12, %r13
	je	.L1310
	movzbl	(%r12), %eax
	cmpb	$32, %al
	je	.L239
	cmpb	$59, %al
	je	.L239
	cmpb	$13, %al
	je	.L1311
.L268:
	leaq	.LC3(%rip), %rax
	movl	$12, 24(%rbx)
	movl	$12, %r14d
	movq	%rax, 32(%rbx)
	movq	%r12, 40(%rbx)
	movq	$0, 56(%rbx)
	jmp	.L61
.L946:
	movl	$18, %eax
	jmp	.L205
.L185:
	cmpq	%r13, %r12
	je	.L1312
.L929:
	movq	%r12, %rdx
	leaq	.L386(%rip), %rcx
	movabsq	$1844674407370955161, %rsi
.L927:
	movzbl	(%rdx), %eax
	movq	%rdx, %r12
	subl	$48, %eax
	cmpb	$9, %al
	ja	.L1160
	movzbl	%al, %eax
	leaq	1(%rdx), %r15
	movslq	(%rcx,%rax,4), %rax
	movq	%r15, %r12
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L386:
	.long	.L395-.L386
	.long	.L394-.L386
	.long	.L393-.L386
	.long	.L392-.L386
	.long	.L391-.L386
	.long	.L390-.L386
	.long	.L389-.L386
	.long	.L388-.L386
	.long	.L387-.L386
	.long	.L385-.L386
	.text
.L196:
	cmpq	%r13, %r12
	je	.L1313
.L330:
	movzbl	(%r12), %eax
	cmpb	$10, %al
	je	.L327
	cmpb	$13, %al
	je	.L1175
	addq	$1, %r12
	cmpq	%r12, %r13
	jne	.L330
	movl	$27, %eax
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L148:
	cmpq	%r13, %r12
	jne	.L573
	movl	$75, %eax
	jmp	.L205
.L82:
	cmpq	%r13, %r12
	je	.L1314
.L826:
	movzbl	(%r12), %eax
	leaq	1(%r12), %r15
	cmpb	$10, %al
	je	.L821
.L1315:
	cmpb	$13, %al
	je	.L822
	cmpq	%r15, %r13
	je	.L951
	movq	%r15, %r12
	movzbl	(%r12), %eax
	leaq	1(%r12), %r15
	cmpb	$10, %al
	jne	.L1315
.L821:
	movq	$0, 8(%rbx)
	movq	%r12, %rdx
	movq	%rbx, %rdi
	movq	%r15, %r12
	call	llhttp__on_status@PLT
	testl	%eax, %eax
	je	.L165
	movl	%eax, 24(%rbx)
	movl	%eax, %r14d
	movq	%r15, 40(%rbx)
	movq	$58, 56(%rbx)
	jmp	.L61
.L139:
	cmpq	%r13, %r12
	jne	.L614
	movl	$84, %eax
	jmp	.L205
.L69:
	cmpq	%r13, %r12
	je	.L953
	cmpb	$72, (%r12)
	je	.L898
.L896:
	leaq	.LC40(%rip), %rax
	movl	$8, 24(%rbx)
	movl	$8, %r14d
	movq	%rax, 32(%rbx)
	movq	%r12, 40(%rbx)
	movq	$0, 56(%rbx)
	jmp	.L61
.L201:
	cmpq	%r13, %r12
	jne	.L309
	movq	8(%rbx), %rsi
	movl	$22, %eax
	jmp	.L205
.L81:
	cmpq	%r13, %r12
	je	.L1316
.L827:
	movq	llhttp__on_status@GOTPCREL(%rip), %rax
	movq	%r12, 8(%rbx)
	movq	%r12, %rsi
	movq	%rax, 16(%rbx)
	jmp	.L826
.L166:
	cmpq	%r13, %r12
	jne	.L481
	movq	8(%rbx), %rsi
	movl	$57, %eax
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L594:
	movq	8(%rbx), %rsi
	movq	$0, 8(%rbx)
	movq	%r12, %rdx
	movq	%rbx, %rdi
	call	llhttp__on_url@PLT
	testl	%eax, %eax
	je	.L154
.L587:
	movl	%eax, 24(%rbx)
	movl	%eax, %r14d
	movq	%r12, 40(%rbx)
	movq	$69, 56(%rbx)
	jmp	.L61
.L593:
	addq	$1, %r12
	cmpq	%r12, %r13
	jne	.L600
	movq	8(%rbx), %rsi
	movl	$79, %eax
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L555:
	addq	$1, %r12
	cmpq	%r12, %r13
	jne	.L556
.L588:
	movq	8(%rbx), %rsi
	movl	$72, %eax
	jmp	.L205
.L298:
	leaq	.LC4(%rip), %rax
	movl	$15, 24(%rbx)
	movl	$15, %r14d
	movq	%rax, 32(%rbx)
	movq	%r12, 40(%rbx)
	movq	$0, 56(%rbx)
	jmp	.L61
.L301:
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	llhttp__on_message_complete@PLT
	movl	%eax, %r14d
	testl	%eax, %eax
	je	.L221
	cmpl	$21, %eax
	jne	.L1134
	leaq	.LC1(%rip), %rax
	movl	$21, 24(%rbx)
	movq	%rax, 32(%rbx)
	movq	%r12, 40(%rbx)
	movq	$2, 56(%rbx)
	jmp	.L61
.L841:
	movl	$5, %edx
.L847:
	movzwl	82(%rbx), %eax
	cmpw	$6553, %ax
	ja	.L932
	leal	(%rax,%rax,4), %eax
	addl	%eax, %eax
.L937:
	addl	%edx, %eax
	movq	%rsi, %rdx
	movw	%ax, 82(%rbx)
	cmpw	$999, %ax
	jbe	.L933
.L932:
	leaq	.LC39(%rip), %rax
	movl	$13, 24(%rbx)
	movl	$13, %r14d
	movq	%rax, 32(%rbx)
	movq	%rsi, 40(%rbx)
	movq	$0, 56(%rbx)
	jmp	.L61
.L842:
	movl	$4, %edx
	jmp	.L847
.L843:
	movl	$3, %edx
	jmp	.L847
.L844:
	movl	$2, %edx
	jmp	.L847
.L845:
	movl	$1, %edx
	jmp	.L847
.L846:
	xorl	%edx, %edx
	jmp	.L847
.L836:
	movl	$9, %edx
	movl	$65526, %edi
.L848:
	movzwl	82(%rbx), %eax
	cmpw	$6553, %ax
	ja	.L932
	leal	(%rax,%rax,4), %eax
	addl	%eax, %eax
	movzwl	%ax, %r8d
	movw	%ax, 82(%rbx)
	cmpl	%edi, %r8d
	jg	.L932
	jmp	.L937
.L838:
	movl	$8, %edx
	movl	$65527, %edi
	jmp	.L848
.L839:
	movl	$7, %edx
	movl	$65528, %edi
	jmp	.L848
.L840:
	movl	$6, %edx
	movl	$65529, %edi
	jmp	.L848
.L584:
	movq	8(%rbx), %rsi
	movq	$0, 8(%rbx)
	movq	%r12, %rdx
	movq	%rbx, %rdi
	call	llhttp__on_url@PLT
	testl	%eax, %eax
	jne	.L585
	jmp	.L484
.L578:
	leaq	.LC15(%rip), %rax
	addq	$1, %r12
	movl	$7, 24(%rbx)
	movl	$7, %r14d
	movq	%rax, 32(%rbx)
	movq	%r12, 40(%rbx)
	movq	$0, 56(%rbx)
	jmp	.L61
.L387:
	movq	$-9, %r8
	movl	$8, %eax
.L398:
	movq	64(%rbx), %rdi
	cmpq	%rsi, %rdi
	ja	.L926
	leaq	(%rdi,%rdi,4), %rdi
	addq	%rdi, %rdi
	movq	%rdi, 64(%rbx)
	cmpq	%r8, %rdi
	jbe	.L939
.L926:
	movq	8(%rbx), %rsi
	movq	$0, 8(%rbx)
	movq	%r15, %rdx
	movq	%rbx, %rdi
	call	llhttp__on_header_value@PLT
	movl	%eax, %r14d
	testl	%eax, %eax
	je	.L188
	movl	%r14d, 24(%rbx)
	movq	%r15, 40(%rbx)
	movq	$35, 56(%rbx)
	jmp	.L61
.L388:
	movq	$-8, %r8
	movl	$7, %eax
	jmp	.L398
.L389:
	movq	$-7, %r8
	movl	$6, %eax
	jmp	.L398
.L390:
	movl	$5, %eax
.L397:
	movq	64(%rbx), %rdi
	cmpq	%rsi, %rdi
	ja	.L926
	leaq	(%rdi,%rdi,4), %rdi
	addq	%rdi, %rdi
.L939:
	addq	%rdi, %rax
	movq	%r15, %rdx
	movq	%rax, 64(%rbx)
	cmpq	%r15, %r13
	jne	.L927
	movq	8(%rbx), %rsi
	movl	$38, %eax
	jmp	.L205
.L391:
	movl	$4, %eax
	jmp	.L397
.L392:
	movl	$3, %eax
	jmp	.L397
.L393:
	movl	$2, %eax
	jmp	.L397
.L394:
	movl	$1, %eax
.L396:
	movq	64(%rbx), %rdi
	cmpq	%rsi, %rdi
	ja	.L926
	imulq	$10, %rdi, %rdi
	jmp	.L939
.L395:
	xorl	%eax, %eax
	jmp	.L396
.L385:
	movq	$-10, %r8
	movl	$9, %eax
	jmp	.L398
.L517:
	movl	$6, %eax
	jmp	.L524
.L876:
	movl	$3, %eax
	jmp	.L880
.L877:
	movl	$2, %eax
	jmp	.L880
.L878:
	movl	$1, %eax
	jmp	.L880
.L879:
	xorl	%eax, %eax
	jmp	.L880
.L869:
	movl	$9, %eax
	jmp	.L880
.L859:
	movl	$4, %eax
	jmp	.L864
.L860:
	movl	$3, %eax
	jmp	.L864
.L861:
	movl	$2, %eax
	jmp	.L864
.L862:
	movl	$1, %eax
	jmp	.L864
.L863:
	xorl	%eax, %eax
	jmp	.L864
.L853:
	movl	$9, %eax
	jmp	.L864
.L500:
	movl	$7, %eax
	jmp	.L508
.L501:
	movl	$6, %eax
	jmp	.L508
.L502:
	movl	$5, %eax
	jmp	.L508
.L503:
	movl	$4, %eax
	jmp	.L508
.L504:
	movl	$3, %eax
	jmp	.L508
.L505:
	movl	$2, %eax
	jmp	.L508
.L506:
	movl	$1, %eax
	jmp	.L508
.L507:
	xorl	%eax, %eax
	jmp	.L508
.L497:
	movl	$9, %eax
	jmp	.L508
.L871:
	movl	$8, %eax
	jmp	.L880
.L872:
	movl	$7, %eax
	jmp	.L880
.L873:
	movl	$6, %eax
	jmp	.L880
.L874:
	movl	$5, %eax
	jmp	.L880
.L856:
	movl	$7, %eax
	jmp	.L864
.L857:
	movl	$6, %eax
	jmp	.L864
.L518:
	movl	$5, %eax
	jmp	.L524
.L519:
	movl	$4, %eax
	jmp	.L524
.L520:
	movl	$3, %eax
	jmp	.L524
.L521:
	movl	$2, %eax
	jmp	.L524
.L522:
	movl	$1, %eax
	jmp	.L524
.L523:
	xorl	%eax, %eax
	jmp	.L524
.L513:
	movl	$9, %eax
	jmp	.L524
.L515:
	movl	$8, %eax
	jmp	.L524
.L855:
	movl	$8, %eax
	jmp	.L864
.L275:
	addq	$1, %r12
	movl	$10, %eax
	.p2align 4,,10
	.p2align 3
.L266:
	movabsq	$1152921504606846975, %rcx
	movq	64(%rbx), %rdx
	cmpq	%rcx, %rdx
	jbe	.L1317
	leaq	.LC27(%rip), %rax
	movl	$12, 24(%rbx)
	movl	$12, %r14d
	movq	%rax, 32(%rbx)
	movq	%r12, 40(%rbx)
	movq	$0, 56(%rbx)
	jmp	.L61
.L274:
	addq	$1, %r12
	movl	$11, %eax
	jmp	.L266
.L273:
	addq	$1, %r12
	movl	$12, %eax
	jmp	.L266
.L272:
	addq	$1, %r12
	movl	$13, %eax
	jmp	.L266
.L271:
	addq	$1, %r12
	movl	$14, %eax
	jmp	.L266
.L269:
	addq	$1, %r12
	movl	$15, %eax
	jmp	.L266
.L282:
	addq	$1, %r12
	movl	$9, %eax
	jmp	.L266
.L283:
	addq	$1, %r12
	movl	$8, %eax
	jmp	.L266
.L284:
	addq	$1, %r12
	movl	$7, %eax
	jmp	.L266
.L285:
	addq	$1, %r12
	movl	$6, %eax
	jmp	.L266
.L290:
	addq	$1, %r12
	movl	$1, %eax
	jmp	.L266
.L291:
	addq	$1, %r12
	xorl	%eax, %eax
	jmp	.L266
.L286:
	addq	$1, %r12
	movl	$5, %eax
	jmp	.L266
.L287:
	addq	$1, %r12
	movl	$4, %eax
	jmp	.L266
.L288:
	addq	$1, %r12
	movl	$3, %eax
	jmp	.L266
.L289:
	addq	$1, %r12
	movl	$2, %eax
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L796:
	movl	$0, (%rbx)
	jmp	.L641
.L425:
	cmpb	$32, %al
	jne	.L179
.L426:
	addq	$1, %r12
	cmpq	%r12, %r13
	jne	.L315
	movq	8(%rbx), %rsi
	movl	$45, %eax
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L899:
	addq	$1, %r12
	cmpq	%r12, %r13
	jne	.L223
	movq	8(%rbx), %rsi
	movl	$157, %eax
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L540:
	cmpb	$4, %al
	je	.L594
.L543:
	leaq	.LC12(%rip), %rax
	movl	$7, 24(%rbx)
	movl	$7, %r14d
	movq	%rax, 32(%rbx)
	movq	%r12, 40(%rbx)
	movq	$0, 56(%rbx)
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L1192:
	movq	8(%rbx), %rsi
	movl	$47, %eax
	jmp	.L205
.L562:
	cmpb	$35, %al
	je	.L566
	cmpb	$63, %al
	je	.L580
.L565:
	leaq	.LC14(%rip), %rax
	movl	$7, 24(%rbx)
	movl	$7, %r14d
	movq	%rax, 32(%rbx)
	movq	%r12, 40(%rbx)
	movq	$0, 56(%rbx)
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L1183:
	movq	8(%rbx), %rsi
	movl	$87, %eax
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L333:
	movq	8(%rbx), %rsi
.L1175:
	movq	$0, 8(%rbx)
	movq	%r12, %rdx
	movq	%rbx, %rdi
	addq	$1, %r12
	call	llhttp__on_header_value@PLT
	testl	%eax, %eax
	je	.L197
.L329:
	movl	%eax, 24(%rbx)
	movl	%eax, %r14d
	movq	%r12, 40(%rbx)
	movq	$26, 56(%rbx)
	jmp	.L61
.L345:
	cmpb	$44, %al
	jne	.L346
	movzbl	76(%rbx), %eax
	addq	$1, %r12
	cmpb	$7, %al
	je	.L349
	ja	.L350
	cmpb	$5, %al
	je	.L351
	cmpb	$6, %al
	jne	.L189
	orw	$2, 78(%rbx)
.L925:
	movb	$1, 76(%rbx)
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L481:
	movq	llhttp__on_header_field@GOTPCREL(%rip), %rax
	movq	%r12, 8(%rbx)
	movq	%rax, 16(%rbx)
	jmp	.L482
.L574:
	movq	8(%rbx), %rsi
	movl	$74, %eax
	jmp	.L205
.L907:
	cmpl	$21, %eax
	jne	.L909
	leaq	.LC28(%rip), %rax
	movl	$21, 24(%rbx)
	movl	$21, %r14d
	movq	%rax, 32(%rbx)
	movq	%r12, 40(%rbx)
	movq	$20, 56(%rbx)
	jmp	.L61
.L332:
	movq	8(%rbx), %rsi
	movq	$0, 8(%rbx)
	movq	%r12, %rdx
	movq	%rbx, %rdi
	call	llhttp__on_header_value@PLT
	testl	%eax, %eax
	je	.L325
	jmp	.L329
.L433:
	addq	$1, %r12
	cmpq	%r12, %r13
	jne	.L436
	movq	8(%rbx), %rsi
	movl	$48, %eax
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L1182:
	movq	8(%rbx), %rsi
	movl	$88, %eax
	jmp	.L205
.L339:
	addq	$1, %r12
	cmpq	%r12, %r13
	jne	.L341
.L379:
	movq	8(%rbx), %rsi
	movl	$29, %eax
	jmp	.L205
.L1189:
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	llhttp__on_chunk_complete@PLT
	testl	%eax, %eax
	je	.L219
	cmpl	$21, %eax
	jne	.L1131
	leaq	.LC23(%rip), %rax
	movl	$21, 24(%rbx)
	movl	$21, %r14d
	movq	%rax, 32(%rbx)
	movq	%r12, 40(%rbx)
	movq	$4, 56(%rbx)
	jmp	.L61
.L882:
	movl	$0, (%rbx)
	jmp	.L1170
.L419:
	cmpb	$4, %al
	jne	.L422
	orw	$16, 78(%rbx)
	jmp	.L409
.L822:
	movq	$0, 8(%rbx)
	movq	%r12, %rdx
	movq	%rbx, %rdi
	movq	%r15, %r12
	call	llhttp__on_status@PLT
	testl	%eax, %eax
	je	.L83
	movl	%eax, 24(%rbx)
	movl	%eax, %r14d
	movq	%r15, 40(%rbx)
	movq	$140, 56(%rbx)
	jmp	.L61
.L1235:
	movq	8(%rbx), %rsi
	movl	$41, %eax
	jmp	.L205
.L367:
	movl	$0, (%rbx)
	jmp	.L194
.L1188:
	movq	8(%rbx), %rsi
	movl	$21, %eax
	jmp	.L205
.L1190:
	testb	$1, %ah
	jne	.L1318
	leaq	.LC31(%rip), %rax
	movl	$4, 24(%rbx)
	movl	$4, %r14d
	movq	%rax, 32(%rbx)
	movq	%r12, 40(%rbx)
	movq	$0, 56(%rbx)
	jmp	.L61
.L1131:
	leaq	.LC24(%rip), %rax
	movl	$20, 24(%rbx)
	movl	$20, %r14d
	movq	%rax, 32(%rbx)
	movq	%r12, 40(%rbx)
	movq	$0, 56(%rbx)
	jmp	.L61
.L337:
	movq	8(%rbx), %rsi
	jmp	.L330
.L1134:
	leaq	.LC2(%rip), %rax
	movl	$18, 24(%rbx)
	movl	$18, %r14d
	movq	%rax, 32(%rbx)
	movq	%r12, 40(%rbx)
	movq	$0, 56(%rbx)
	jmp	.L61
.L1187:
	movq	8(%rbx), %rsi
	movl	$58, %eax
	jmp	.L205
.L1218:
	movq	$0, 64(%rbx)
	addq	%rax, %r12
	movq	%rbx, %rdi
	movq	$0, 8(%rbx)
	movq	%r12, %rdx
	call	llhttp__on_body@PLT
	testl	%eax, %eax
	je	.L217
	movl	%eax, 24(%rbx)
	movl	%eax, %r14d
	movq	%r12, 40(%rbx)
	movq	$6, 56(%rbx)
	jmp	.L61
.L380:
	orw	$32, 78(%rbx)
	jmp	.L195
.L890:
	movl	$0, (%rbx)
	jmp	.L896
.L1222:
	movq	8(%rbx), %rsi
	movl	$5, %eax
	jmp	.L205
.L1224:
	movq	$0, 64(%rbx)
	addq	%rax, %r12
	movq	%rbx, %rdi
	movq	$0, 8(%rbx)
	movq	%r12, %rdx
	call	llhttp__on_body@PLT
	testl	%eax, %eax
	je	.L219
	movl	%eax, 24(%rbx)
	movl	%eax, %r14d
	movq	%r12, 40(%rbx)
	movq	$4, 56(%rbx)
	jmp	.L61
.L407:
	addq	$1, %r12
	cmpq	%r12, %r13
	jne	.L410
.L416:
	movq	8(%rbx), %rsi
	movl	$42, %eax
	jmp	.L205
.L1196:
	movq	8(%rbx), %rsi
	movl	$28, %eax
	jmp	.L205
.L541:
	addq	$1, %r12
	cmpq	%r12, %r13
	jne	.L545
.L571:
	movq	8(%rbx), %rsi
	movl	$70, %eax
	jmp	.L205
.L321:
	cmpb	$8, %al
	jne	.L165
	orw	$8, 78(%rbx)
	jmp	.L165
.L1227:
	movq	8(%rbx), %rsi
	movl	$10, %eax
	jmp	.L205
.L488:
	movl	$0, (%rbx)
	jmp	.L485
.L487:
	leaq	.LC43(%rip), %rax
	movl	$0, (%rbx)
	movl	$7, %r14d
	movl	$7, 24(%rbx)
	movq	%rax, 32(%rbx)
	movq	%r12, 40(%rbx)
	movq	$0, 56(%rbx)
	jmp	.L61
.L1275:
	movq	8(%rbx), %rsi
	movl	$19, %eax
	jmp	.L205
.L919:
	cmpb	$8, %al
	jne	.L309
	orw	$8, 78(%rbx)
	jmp	.L309
.L589:
	leaq	.LC16(%rip), %rax
	movl	$7, 24(%rbx)
	movl	$7, %r14d
	movq	%rax, 32(%rbx)
	movq	%r12, 40(%rbx)
	movq	$0, 56(%rbx)
	jmp	.L61
.L902:
	leaq	.LC41(%rip), %rax
	movl	$21, 24(%rbx)
	movl	$21, %r14d
	movq	%rax, 32(%rbx)
	movq	%r12, 40(%rbx)
	movq	$156, 56(%rbx)
	jmp	.L61
.L531:
	cmpb	$33, 73(%rbx)
	movl	$0, (%rbx)
	je	.L158
.L535:
	leaq	.LC37(%rip), %rax
	movl	$8, 24(%rbx)
	movl	$8, %r14d
	movq	%rax, 32(%rbx)
	movq	%r12, 40(%rbx)
	movq	$0, 56(%rbx)
	jmp	.L61
.L611:
	addq	$1, %r12
	cmpq	%r12, %r13
	jne	.L613
.L615:
	movq	8(%rbx), %rsi
	movl	$83, %eax
	jmp	.L205
.L350:
	cmpb	$8, %al
	jne	.L189
	orw	$8, 78(%rbx)
	jmp	.L189
.L1184:
	movq	8(%rbx), %rsi
	movl	$85, %eax
	jmp	.L205
.L467:
	movl	$0, (%rbx)
	movl	$3, %eax
	jmp	.L931
.L472:
	movl	$0, (%rbx)
	movl	$4, %eax
	jmp	.L931
.L239:
	addq	$1, %r12
	jmp	.L1155
.L883:
	movl	$0, (%rbx)
	jmp	.L74
.L1194:
	movq	8(%rbx), %rsi
	movl	$46, %eax
	jmp	.L205
.L234:
	leaq	.LC25(%rip), %rax
	movl	$21, 24(%rbx)
	movl	$21, %r14d
	movq	%rax, 32(%rbx)
	movq	%r12, 40(%rbx)
	movq	$9, 56(%rbx)
	jmp	.L61
.L638:
	movl	$0, (%rbx)
	movl	$5, %eax
	jmp	.L642
.L727:
	movl	$0, (%rbx)
	movl	$13, %eax
	jmp	.L642
.L909:
	leaq	.LC29(%rip), %rax
	movl	$17, 24(%rbx)
	movl	$17, %r14d
	movq	%rax, 32(%rbx)
	movq	%r12, 40(%rbx)
	movq	$0, 56(%rbx)
	jmp	.L61
.L1268:
	addq	$1, %r12
	movl	$8, %eax
	jmp	.L642
.L723:
	movl	$0, (%rbx)
	movl	$12, %eax
	jmp	.L642
.L446:
	movl	$0, (%rbx)
	movl	$2, %eax
	jmp	.L931
.L887:
	movl	$513, %eax
	movl	$0, (%rbx)
	movw	%ax, 72(%rbx)
	jmp	.L135
.L755:
	movl	$0, (%rbx)
	movl	$20, %eax
	jmp	.L642
.L751:
	movl	$0, (%rbx)
	movl	$17, %eax
	jmp	.L642
.L906:
	movb	$1, 80(%rbx)
	jmp	.L1156
.L686:
	movl	$0, (%rbx)
	movl	$30, %eax
	jmp	.L642
.L790:
	movl	$0, (%rbx)
	movl	$15, %eax
	jmp	.L642
.L786:
	movl	$0, (%rbx)
	movl	$32, %eax
	jmp	.L642
.L663:
	movl	$0, (%rbx)
	movl	$31, %eax
	jmp	.L642
.L634:
	movl	$0, (%rbx)
	movl	$22, %eax
	jmp	.L642
.L1205:
	movq	8(%rbx), %rsi
	movl	$26, %eax
	jmp	.L205
.L738:
	movl	$0, (%rbx)
	movl	$29, %eax
	jmp	.L642
.L550:
	leaq	.LC13(%rip), %rax
	movl	$7, 24(%rbx)
	movl	$7, %r14d
	movq	%rax, 32(%rbx)
	movq	%r12, 40(%rbx)
	movq	$0, 56(%rbx)
	jmp	.L61
.L667:
	movl	$0, (%rbx)
	movl	$9, %eax
	jmp	.L642
.L682:
	movl	$0, (%rbx)
	movl	$21, %eax
	jmp	.L642
.L368:
	movl	$0, (%rbx)
	movb	$7, 76(%rbx)
	jmp	.L1158
.L297:
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	llhttp__on_message_complete@PLT
	testl	%eax, %eax
	je	.L222
	cmpl	$21, %eax
	jne	.L1134
	leaq	.LC1(%rip), %rax
	movl	$21, 24(%rbx)
	movl	$21, %r14d
	movq	%rax, 32(%rbx)
	movq	%r12, 40(%rbx)
	movq	$1, 56(%rbx)
	jmp	.L61
.L357:
	movl	$0, (%rbx)
	movb	$6, 76(%rbx)
	jmp	.L1158
.L771:
	movl	$0, (%rbx)
	movl	$26, %eax
	jmp	.L642
.L362:
	movl	$0, (%rbx)
	movb	$5, 76(%rbx)
	jmp	.L1158
.L797:
	movl	$0, (%rbx)
	movl	$27, %eax
	jmp	.L642
.L948:
	movl	$56, %eax
	jmp	.L205
.L767:
	movl	$0, (%rbx)
	movl	$33, %eax
	jmp	.L642
.L413:
	movl	$0, (%rbx)
	jmp	.L1162
.L782:
	movl	$0, (%rbx)
	movl	$18, %eax
	jmp	.L642
.L763:
	movl	$0, (%rbx)
	movl	$14, %eax
	jmp	.L642
.L1239:
	movq	8(%rbx), %rsi
	movl	$59, %eax
	jmp	.L205
.L1226:
	movq	8(%rbx), %rsi
	movl	$140, %eax
	jmp	.L205
.L1229:
	movq	8(%rbx), %rsi
	movl	$86, %eax
	jmp	.L205
.L678:
	movl	$0, (%rbx)
	movl	$23, %eax
	jmp	.L642
.L719:
	movl	$0, (%rbx)
	movl	$3, %eax
	jmp	.L642
.L674:
	movl	$0, (%rbx)
	movl	$24, %eax
	jmp	.L642
.L715:
	movl	$0, (%rbx)
	movl	$28, %eax
	jmp	.L642
.L697:
	movl	$0, (%rbx)
	movl	$11, %eax
	jmp	.L642
.L734:
	addq	$1, %r12
	movl	$34, %eax
	jmp	.L642
.L1191:
	movq	8(%rbx), %rsi
	movl	$47, %eax
	jmp	.L205
.L1318:
	andl	$40, %eax
	cmpw	$40, %ax
	jne	.L905
	leaq	.LC30(%rip), %rax
	movl	$4, 24(%rbx)
	movl	$4, %r14d
	movq	%rax, 32(%rbx)
	movq	%r12, 40(%rbx)
	movq	$0, 56(%rbx)
	jmp	.L61
.L1177:
	movq	8(%rbx), %rsi
	movl	$23, %eax
	jmp	.L205
.L1217:
	movq	8(%rbx), %rsi
	movl	$8, %eax
	jmp	.L205
.L1199:
	movq	8(%rbx), %rsi
	movl	$73, %eax
	jmp	.L205
.L655:
	movl	$0, (%rbx)
	movl	$1, %eax
	jmp	.L642
.L707:
	movl	$0, (%rbx)
	movl	$25, %eax
	jmp	.L642
.L778:
	movl	$0, (%rbx)
	movl	$7, %eax
	jmp	.L642
.L626:
	movl	$0, (%rbx)
	movl	$19, %eax
	jmp	.L642
.L651:
	movl	$0, (%rbx)
	xorl	%eax, %eax
	jmp	.L642
.L659:
	movl	$0, (%rbx)
	movl	$2, %eax
	jmp	.L642
.L630:
	movl	$0, (%rbx)
	movl	$16, %eax
	jmp	.L642
.L711:
	movl	$0, (%rbx)
	movl	$6, %eax
	jmp	.L642
.L1178:
	leaq	.LC32(%rip), %rax
	movl	$11, 24(%rbx)
	movl	$11, %r14d
	movq	%rax, 32(%rbx)
	movq	%r12, 40(%rbx)
	movq	$0, 56(%rbx)
	jmp	.L61
.L1241:
	movq	8(%rbx), %rsi
	movl	$44, %eax
	jmp	.L205
.L1219:
	movq	8(%rbx), %rsi
	movl	$82, %eax
	jmp	.L205
.L418:
	orw	$512, 78(%rbx)
	jmp	.L411
.L852:
	leaq	.LC9(%rip), %rax
	movl	$9, 24(%rbx)
	movl	$9, %r14d
	movq	%rax, 32(%rbx)
	movq	%r12, 40(%rbx)
	movq	$0, 56(%rbx)
	jmp	.L61
.L1316:
	movq	8(%rbx), %rsi
	movl	$142, %eax
	jmp	.L205
.L868:
	leaq	.LC11(%rip), %rax
	movl	$9, 24(%rbx)
	movl	$9, %r14d
	movq	%rax, 32(%rbx)
	movq	%r12, 40(%rbx)
	movq	$0, 56(%rbx)
	jmp	.L61
.L1223:
	movq	8(%rbx), %rsi
	movl	$17, %eax
	jmp	.L205
.L1208:
	movq	8(%rbx), %rsi
	movl	$148, %eax
	jmp	.L205
.L1197:
	movq	8(%rbx), %rsi
	movl	$76, %eax
	jmp	.L205
.L1221:
	movq	8(%rbx), %rsi
	movl	$6, %eax
	jmp	.L205
.L1214:
	movq	8(%rbx), %rsi
	movl	$62, %eax
	jmp	.L205
.L1210:
	movq	8(%rbx), %rsi
	movl	$146, %eax
	jmp	.L205
.L1212:
	movq	8(%rbx), %rsi
	movl	$64, %eax
	jmp	.L205
.L1216:
	movq	8(%rbx), %rsi
	movl	$14, %eax
	jmp	.L205
.L1207:
	movq	8(%rbx), %rsi
	movl	$71, %eax
	jmp	.L205
.L1220:
	movq	8(%rbx), %rsi
	movl	$81, %eax
	jmp	.L205
.L1200:
	movq	8(%rbx), %rsi
	movl	$40, %eax
	jmp	.L205
.L1186:
	movq	8(%rbx), %rsi
	movl	$25, %eax
	jmp	.L205
.L1237:
	movq	8(%rbx), %rsi
	movl	$60, %eax
	jmp	.L205
.L1225:
	movq	8(%rbx), %rsi
	movl	$143, %eax
	jmp	.L205
.L1231:
	movq	8(%rbx), %rsi
	movl	$66, %eax
	jmp	.L205
.L1238:
	movq	8(%rbx), %rsi
	movl	$65, %eax
	jmp	.L205
.L1233:
	movq	8(%rbx), %rsi
	movl	$147, %eax
	jmp	.L205
.L1213:
	movq	8(%rbx), %rsi
	movl	$63, %eax
	jmp	.L205
.L1228:
	movq	8(%rbx), %rsi
	movl	$13, %eax
	jmp	.L205
.L1232:
	movq	8(%rbx), %rsi
	movl	$67, %eax
	jmp	.L205
.L1230:
	movq	8(%rbx), %rsi
	movl	$149, %eax
	jmp	.L205
.L1215:
	movq	8(%rbx), %rsi
	movl	$61, %eax
	jmp	.L205
.L1244:
	movq	8(%rbx), %rsi
	movl	$55, %eax
	jmp	.L205
.L1245:
	movq	8(%rbx), %rsi
	movl	$52, %eax
	jmp	.L205
.L1249:
	movq	8(%rbx), %rsi
	movl	$51, %eax
	jmp	.L205
.L1246:
	movq	8(%rbx), %rsi
	movl	$155, %eax
	jmp	.L205
.L1248:
	movq	8(%rbx), %rsi
	movl	$150, %eax
	jmp	.L205
.L1242:
	movq	8(%rbx), %rsi
	movl	$54, %eax
	jmp	.L205
.L1243:
	movq	8(%rbx), %rsi
	movl	$53, %eax
	jmp	.L205
.L953:
	movl	$154, %eax
	jmp	.L205
.L1264:
	movq	8(%rbx), %rsi
	movl	$105, %eax
	jmp	.L205
.L1284:
	movq	8(%rbx), %rsi
	movl	$91, %eax
	jmp	.L205
.L1292:
	movq	8(%rbx), %rsi
	movl	$127, %eax
	jmp	.L205
.L1253:
	movq	8(%rbx), %rsi
	movl	$152, %eax
	jmp	.L205
.L1278:
	movq	8(%rbx), %rsi
	movl	$132, %eax
	jmp	.L205
.L1282:
	movq	8(%rbx), %rsi
	movl	$120, %eax
	jmp	.L205
.L1273:
	movq	8(%rbx), %rsi
	movl	$125, %eax
	jmp	.L205
.L1270:
	movq	8(%rbx), %rsi
	movl	$133, %eax
	jmp	.L205
.L1283:
	movq	8(%rbx), %rsi
	movl	$99, %eax
	jmp	.L205
.L1267:
	movq	8(%rbx), %rsi
	movl	$93, %eax
	jmp	.L205
.L1290:
	movq	8(%rbx), %rsi
	movl	$128, %eax
	jmp	.L205
.L1277:
	movq	8(%rbx), %rsi
	movl	$137, %eax
	jmp	.L205
.L1201:
	movq	8(%rbx), %rsi
	movl	$43, %eax
	jmp	.L205
.L1252:
	movq	8(%rbx), %rsi
	movl	$153, %eax
	jmp	.L205
.L1271:
	movq	8(%rbx), %rsi
	movl	$123, %eax
	jmp	.L205
.L1279:
	movq	8(%rbx), %rsi
	movl	$104, %eax
	jmp	.L205
.L1180:
	movq	8(%rbx), %rsi
	movl	$107, %eax
	jmp	.L205
.L1258:
	movq	8(%rbx), %rsi
	movl	$115, %eax
	jmp	.L205
.L1259:
	movq	8(%rbx), %rsi
	movl	$151, %eax
	jmp	.L205
.L1289:
	movq	8(%rbx), %rsi
	movl	$129, %eax
	jmp	.L205
.L1255:
	movq	8(%rbx), %rsi
	movl	$118, %eax
	jmp	.L205
.L1286:
	movq	8(%rbx), %rsi
	movl	$136, %eax
	jmp	.L205
.L1262:
	movq	8(%rbx), %rsi
	movl	$135, %eax
	jmp	.L205
.L1250:
	movq	8(%rbx), %rsi
	movl	$50, %eax
	jmp	.L205
.L1281:
	movq	8(%rbx), %rsi
	movl	$100, %eax
	jmp	.L205
.L1287:
	movq	8(%rbx), %rsi
	movl	$31, %eax
	jmp	.L205
.L1288:
	movq	8(%rbx), %rsi
	movl	$33, %eax
	jmp	.L205
.L1256:
	movq	8(%rbx), %rsi
	movl	$117, %eax
	jmp	.L205
.L1274:
	movq	8(%rbx), %rsi
	movl	$124, %eax
	jmp	.L205
.L1269:
	movq	8(%rbx), %rsi
	movl	$92, %eax
	jmp	.L205
.L1263:
	movq	8(%rbx), %rsi
	movl	$134, %eax
	jmp	.L205
.L1266:
	movq	8(%rbx), %rsi
	movl	$94, %eax
	jmp	.L205
.L1181:
	movq	8(%rbx), %rsi
	movl	$106, %eax
	jmp	.L205
.L1251:
	movq	8(%rbx), %rsi
	movl	$49, %eax
	jmp	.L205
.L1260:
	movq	8(%rbx), %rsi
	movl	$80, %eax
	jmp	.L205
.L1261:
	movq	8(%rbx), %rsi
	movl	$78, %eax
	jmp	.L205
.L1257:
	movq	8(%rbx), %rsi
	movl	$116, %eax
	jmp	.L205
.L1285:
	movq	8(%rbx), %rsi
	movl	$32, %eax
	jmp	.L205
.L1293:
	movq	8(%rbx), %rsi
	movl	$109, %eax
	jmp	.L205
.L1254:
	movq	8(%rbx), %rsi
	movl	$119, %eax
	jmp	.L205
.L1294:
	movq	8(%rbx), %rsi
	movl	$113, %eax
	jmp	.L205
.L1295:
	movq	8(%rbx), %rsi
	movl	$121, %eax
	jmp	.L205
.L320:
	orw	$4, 78(%rbx)
	jmp	.L923
.L1296:
	movq	8(%rbx), %rsi
	movl	$110, %eax
	jmp	.L205
.L1272:
	movq	8(%rbx), %rsi
	movl	$126, %eax
	jmp	.L205
.L1297:
	movq	8(%rbx), %rsi
	movl	$103, %eax
	jmp	.L205
.L1299:
	movq	8(%rbx), %rsi
	movl	$114, %eax
	jmp	.L205
.L1298:
	movq	8(%rbx), %rsi
	movl	$122, %eax
	jmp	.L205
.L1265:
	movq	8(%rbx), %rsi
	movl	$95, %eax
	jmp	.L205
.L1304:
	movq	8(%rbx), %rsi
	movl	$89, %eax
	jmp	.L205
.L1307:
	movq	8(%rbx), %rsi
	movl	$111, %eax
	jmp	.L205
.L1308:
	movq	8(%rbx), %rsi
	movl	$131, %eax
	jmp	.L205
.L322:
	orw	$1, 78(%rbx)
	jmp	.L923
.L1179:
	movq	8(%rbx), %rsi
	movl	$108, %eax
	jmp	.L205
.L1300:
	movq	8(%rbx), %rsi
	movl	$102, %eax
	jmp	.L205
.L1301:
	movq	8(%rbx), %rsi
	movl	$144, %eax
	jmp	.L205
.L1291:
	movq	8(%rbx), %rsi
	movl	$130, %eax
	jmp	.L205
.L1310:
	movq	8(%rbx), %rsi
	movl	$12, %eax
	jmp	.L205
.L1203:
	movq	8(%rbx), %rsi
	movl	$48, %eax
	jmp	.L205
.L920:
	orw	$1, 78(%rbx)
	jmp	.L917
.L1185:
	movq	8(%rbx), %rsi
	movl	$157, %eax
	jmp	.L205
.L918:
	orw	$4, 78(%rbx)
	jmp	.L917
.L1234:
	movq	8(%rbx), %rsi
	movl	$41, %eax
	jmp	.L205
.L1305:
	movq	8(%rbx), %rsi
	movl	$90, %eax
	jmp	.L205
.L1309:
	movq	8(%rbx), %rsi
	movl	$112, %eax
	jmp	.L205
.L1306:
	movq	8(%rbx), %rsi
	movl	$96, %eax
	jmp	.L205
.L1302:
	movq	8(%rbx), %rsi
	movl	$98, %eax
	jmp	.L205
.L1276:
	movq	8(%rbx), %rsi
	movl	$138, %eax
	jmp	.L205
.L1303:
	movq	8(%rbx), %rsi
	movl	$97, %eax
	jmp	.L205
.L1280:
	movq	8(%rbx), %rsi
	movl	$101, %eax
	jmp	.L205
.L1202:
	movq	8(%rbx), %rsi
	movl	$68, %eax
	jmp	.L205
.L351:
	orw	$1, 78(%rbx)
	jmp	.L925
.L349:
	orw	$4, 78(%rbx)
	jmp	.L925
.L1314:
	movl	$141, %eax
	jmp	.L205
.L1176:
	movq	8(%rbx), %rsi
	movl	$45, %eax
	jmp	.L205
.L1313:
	movl	$27, %eax
	jmp	.L205
.L1204:
	movq	8(%rbx), %rsi
	movl	$30, %eax
	jmp	.L205
.L951:
	movl	$141, %eax
	jmp	.L205
.L1247:
	movq	8(%rbx), %rsi
	movl	$37, %eax
	jmp	.L205
.L1240:
	movq	8(%rbx), %rsi
	movl	$145, %eax
	jmp	.L205
.L1195:
	movq	8(%rbx), %rsi
	movl	$39, %eax
	jmp	.L205
.L1312:
	movl	$38, %eax
	jmp	.L205
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	llhttp__internal_execute.cold, @function
llhttp__internal_execute.cold:
.LFSB75:
.L64:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	call	abort@PLT
	.cfi_endproc
.LFE75:
	.text
	.size	llhttp__internal_execute, .-llhttp__internal_execute
	.section	.text.unlikely
	.size	llhttp__internal_execute.cold, .-llhttp__internal_execute.cold
.LCOLDE44:
	.text
.LHOTE44:
	.section	.rodata
	.align 32
	.type	lookup_table.4128, @object
	.size	lookup_table.4128, 256
lookup_table.4128:
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\001"
	.string	""
	.string	"\001"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\001"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\002"
	.string	""
	.string	""
	.string	""
	.string	"\002"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.align 32
	.type	lookup_table.4120, @object
	.size	lookup_table.4120, 256
lookup_table.4120:
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\001"
	.string	""
	.string	"\001"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\001"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\002"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.align 32
	.type	lookup_table.4074, @object
	.size	lookup_table.4074, 256
lookup_table.4074:
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\001"
	.string	""
	.string	"\002"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\003\004"
	.string	""
	.string	"\004\004\004\004\004\004\004\004\004\004\004\005\004\004\004\004\004\004\004\004\004\004\004\004"
	.string	"\004"
	.string	"\006\007\004\004\004\004\004\004\004\004\004\004\004\004\004\004\004\004\004\004\004\004\004\004\004\004\004\004\004"
	.string	"\004"
	.string	"\004"
	.string	"\004\004\004\004\004\004\004\004\004\004\004\004\004\004\004\004\004\004\004\004\004\004\004\004\004\004"
	.string	""
	.string	""
	.string	"\004"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.set	lookup_table.4090,lookup_table.4074
	.align 32
	.type	lookup_table.4063, @object
	.size	lookup_table.4063, 256
lookup_table.4063:
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\001"
	.string	""
	.string	"\001"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\001\001"
	.string	"\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001"
	.string	"\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001"
	.ascii	"\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001"
	.ascii	"\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001"
	.ascii	"\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001"
	.ascii	"\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001"
	.ascii	"\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001"
	.ascii	"\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001"
	.ascii	"\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001"
	.ascii	"\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001"
	.ascii	"\001\001\001\001\001\001\001\001"
	.align 32
	.type	lookup_table.4038, @object
	.size	lookup_table.4038, 256
lookup_table.4038:
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\001\002"
	.string	"\001\003"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\004\001\001\005\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001"
	.ascii	"\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001"
	.ascii	"\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001"
	.ascii	"\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001"
	.ascii	"\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001"
	.ascii	"\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001"
	.ascii	"\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001"
	.ascii	"\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001"
	.ascii	"\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001"
	.ascii	"\001\001\001\001\001\001\001\001"
	.align 32
	.type	lookup_table.4024, @object
	.size	lookup_table.4024, 256
lookup_table.4024:
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\001\002"
	.string	"\001\003"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\004\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001"
	.ascii	"\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001"
	.ascii	"\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001"
	.ascii	"\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001"
	.ascii	"\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001"
	.ascii	"\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001"
	.ascii	"\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001"
	.ascii	"\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001"
	.ascii	"\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001"
	.ascii	"\001\001\001\001\001\001\001\001"
	.align 32
	.type	lookup_table.3878, @object
	.size	lookup_table.3878, 256
lookup_table.3878:
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\001\001"
	.string	"\001\001\001\001\001"
	.string	""
	.string	"\001\001"
	.string	"\001\001"
	.string	"\001\001\001\001\001\001\001\001\001\001"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001"
	.string	""
	.string	""
	.string	"\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001"
	.string	"\001"
	.string	"\001"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.align 32
	.type	lookup_table.3847, @object
	.size	lookup_table.3847, 256
lookup_table.3847:
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\001"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001"
	.string	"\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001"
	.align 32
	.type	lookup_table.3764, @object
	.size	lookup_table.3764, 256
lookup_table.3764:
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\001"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\001\001\001\001\001\001\001\001\001\001\001\001\002\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001"
	.string	"\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001"
	.set	lookup_table.3852,lookup_table.3764
	.type	llparse_blob50, @object
	.size	llparse_blob50, 3
llparse_blob50:
	.ascii	"TP/"
	.type	llparse_blob49, @object
	.size	llparse_blob49, 2
llparse_blob49:
	.ascii	"AD"
	.type	llparse_blob48, @object
	.size	llparse_blob48, 5
llparse_blob48:
	.ascii	"HTTP/"
	.align 8
	.type	llparse_blob47, @object
	.size	llparse_blob47, 8
llparse_blob47:
	.ascii	"UBSCRIBE"
	.type	llparse_blob46, @object
	.size	llparse_blob46, 2
llparse_blob46:
	.ascii	"CK"
	.type	llparse_blob45, @object
	.size	llparse_blob45, 2
llparse_blob45:
	.ascii	"NK"
	.type	llparse_blob44, @object
	.size	llparse_blob44, 3
llparse_blob44:
	.ascii	"IND"
	.type	llparse_blob43, @object
	.size	llparse_blob43, 4
llparse_blob43:
	.ascii	"RACE"
	.type	llparse_blob42, @object
	.size	llparse_blob42, 7
llparse_blob42:
	.ascii	"BSCRIBE"
	.type	llparse_blob41, @object
	.size	llparse_blob41, 4
llparse_blob41:
	.ascii	"URCE"
	.type	llparse_blob40, @object
	.size	llparse_blob40, 4
llparse_blob40:
	.ascii	"ARCH"
	.type	llparse_blob39, @object
	.size	llparse_blob39, 3
llparse_blob39:
	.ascii	"ORT"
	.type	llparse_blob38, @object
	.size	llparse_blob38, 3
llparse_blob38:
	.ascii	"IND"
	.type	llparse_blob37, @object
	.size	llparse_blob37, 2
llparse_blob37:
	.ascii	"GE"
	.type	llparse_blob36, @object
	.size	llparse_blob36, 4
llparse_blob36:
	.ascii	"ATCH"
	.type	llparse_blob35, @object
	.size	llparse_blob35, 3
llparse_blob35:
	.ascii	"IND"
	.type	llparse_blob34, @object
	.size	llparse_blob34, 2
llparse_blob34:
	.ascii	"ST"
	.type	llparse_blob33, @object
	.size	llparse_blob33, 3
llparse_blob33:
	.ascii	"TCH"
	.type	llparse_blob32, @object
	.size	llparse_blob32, 6
llparse_blob32:
	.ascii	"PTIONS"
	.type	llparse_blob31, @object
	.size	llparse_blob31, 5
llparse_blob31:
	.ascii	"OTIFY"
	.type	llparse_blob30, @object
	.size	llparse_blob30, 2
llparse_blob30:
	.ascii	"VE"
	.type	llparse_blob29, @object
	.size	llparse_blob29, 6
llparse_blob29:
	.ascii	"LENDAR"
	.type	llparse_blob28, @object
	.size	llparse_blob28, 7
llparse_blob28:
	.ascii	"CTIVITY"
	.type	llparse_blob27, @object
	.size	llparse_blob27, 3
llparse_blob27:
	.ascii	"RGE"
	.type	llparse_blob26, @object
	.size	llparse_blob26, 6
llparse_blob26:
	.ascii	"SEARCH"
	.type	llparse_blob25, @object
	.size	llparse_blob25, 2
llparse_blob25:
	.ascii	"CK"
	.type	llparse_blob24, @object
	.size	llparse_blob24, 2
llparse_blob24:
	.ascii	"NK"
	.type	llparse_blob23, @object
	.size	llparse_blob23, 3
llparse_blob23:
	.ascii	"EAD"
	.type	llparse_blob22, @object
	.size	llparse_blob22, 2
llparse_blob22:
	.ascii	"ET"
	.type	llparse_blob21, @object
	.size	llparse_blob21, 5
llparse_blob21:
	.ascii	"ELETE"
	.type	llparse_blob20, @object
	.size	llparse_blob20, 4
llparse_blob20:
	.ascii	"NECT"
	.type	llparse_blob19, @object
	.size	llparse_blob19, 6
llparse_blob19:
	.ascii	"ECKOUT"
	.type	llparse_blob18, @object
	.size	llparse_blob18, 3
llparse_blob18:
	.ascii	"IND"
	.type	llparse_blob17, @object
	.size	llparse_blob17, 3
llparse_blob17:
	.ascii	"CE/"
	.type	llparse_blob16, @object
	.size	llparse_blob16, 4
llparse_blob16:
	.ascii	"TTP/"
	.type	llparse_blob15, @object
	.size	llparse_blob15, 2
llparse_blob15:
	.ascii	"\r\n"
	.type	llparse_blob14, @object
	.size	llparse_blob14, 6
llparse_blob14:
	.ascii	"pgrade"
	.align 16
	.type	llparse_blob13, @object
	.size	llparse_blob13, 16
llparse_blob13:
	.ascii	"ransfer-encoding"
	.align 8
	.type	llparse_blob12, @object
	.size	llparse_blob12, 15
llparse_blob12:
	.ascii	"roxy-connection"
	.align 8
	.type	llparse_blob11, @object
	.size	llparse_blob11, 10
llparse_blob11:
	.ascii	"ent-length"
	.type	llparse_blob7, @object
	.size	llparse_blob7, 7
llparse_blob7:
	.ascii	"chunked"
	.type	llparse_blob6, @object
	.size	llparse_blob6, 6
llparse_blob6:
	.ascii	"pgrade"
	.align 8
	.type	llparse_blob5, @object
	.size	llparse_blob5, 9
llparse_blob5:
	.ascii	"eep-alive"
	.type	llparse_blob4, @object
	.size	llparse_blob4, 4
llparse_blob4:
	.ascii	"lose"
	.type	llparse_blob3, @object
	.size	llparse_blob3, 6
llparse_blob3:
	.ascii	"ection"
	.type	llparse_blob2, @object
	.size	llparse_blob2, 2
llparse_blob2:
	.ascii	"on"
	.type	llparse_blob0, @object
	.size	llparse_blob0, 2
llparse_blob0:
	.ascii	"CL"
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
