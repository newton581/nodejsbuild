	.file	"api.c"
	.text
	.p2align 4
	.globl	llhttp_init
	.type	llhttp_init, @function
llhttp_init:
.LFB50:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	llhttp__internal_init@PLT
	movb	%r13b, 72(%rbx)
	movq	%r12, 88(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE50:
	.size	llhttp_init, .-llhttp_init
	.p2align 4
	.globl	llhttp_execute
	.type	llhttp_execute, @function
llhttp_execute:
.LFB51:
	.cfi_startproc
	endbr64
	addq	%rsi, %rdx
	jmp	llhttp__internal_execute@PLT
	.cfi_endproc
.LFE51:
	.size	llhttp_execute, .-llhttp_execute
	.p2align 4
	.globl	llhttp_settings_init
	.type	llhttp_settings_init, @function
llhttp_settings_init:
.LFB52:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movups	%xmm0, (%rdi)
	movups	%xmm0, 16(%rdi)
	movups	%xmm0, 32(%rdi)
	movups	%xmm0, 48(%rdi)
	movups	%xmm0, 64(%rdi)
	ret
	.cfi_endproc
.LFE52:
	.size	llhttp_settings_init, .-llhttp_settings_init
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"Invalid EOF state"
	.section	.text.unlikely,"ax",@progbits
.LCOLDB1:
	.text
.LHOTB1:
	.p2align 4
	.globl	llhttp_finish
	.type	llhttp_finish, @function
llhttp_finish:
.LFB53:
	.cfi_startproc
	endbr64
	movl	24(%rdi), %eax
	testl	%eax, %eax
	jne	.L25
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movzbl	84(%rdi), %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpb	$1, %dl
	je	.L8
	cmpb	$2, %dl
	jne	.L29
	leaq	.LC0(%rip), %rax
	movq	%rax, 32(%rdi)
	movl	$14, %eax
.L6:
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	.cfi_restore_state
	xorl	%eax, %eax
	testb	%dl, %dl
	je	.L6
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L8:
	movq	88(%rdi), %rax
	testq	%rax, %rax
	je	.L7
	movq	56(%rax), %rax
	testq	%rax, %rax
	je	.L7
	call	*%rax
	testl	%eax, %eax
	jne	.L6
.L7:
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	llhttp_finish.cold, @function
llhttp_finish.cold:
.LFSB53:
.L23:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	call	abort@PLT
	.cfi_endproc
.LFE53:
	.text
	.size	llhttp_finish, .-llhttp_finish
	.section	.text.unlikely
	.size	llhttp_finish.cold, .-llhttp_finish.cold
.LCOLDE1:
	.text
.LHOTE1:
	.section	.rodata.str1.1
.LC2:
	.string	"Paused"
	.text
	.p2align 4
	.globl	llhttp_pause
	.type	llhttp_pause, @function
llhttp_pause:
.LFB54:
	.cfi_startproc
	endbr64
	movl	24(%rdi), %eax
	testl	%eax, %eax
	jne	.L30
	leaq	.LC2(%rip), %rax
	movl	$21, 24(%rdi)
	movq	%rax, 32(%rdi)
.L30:
	ret
	.cfi_endproc
.LFE54:
	.size	llhttp_pause, .-llhttp_pause
	.p2align 4
	.globl	llhttp_resume
	.type	llhttp_resume, @function
llhttp_resume:
.LFB55:
	.cfi_startproc
	endbr64
	cmpl	$21, 24(%rdi)
	jne	.L32
	movl	$0, 24(%rdi)
.L32:
	ret
	.cfi_endproc
.LFE55:
	.size	llhttp_resume, .-llhttp_resume
	.p2align 4
	.globl	llhttp_resume_after_upgrade
	.type	llhttp_resume_after_upgrade, @function
llhttp_resume_after_upgrade:
.LFB56:
	.cfi_startproc
	endbr64
	cmpl	$22, 24(%rdi)
	jne	.L34
	movl	$0, 24(%rdi)
.L34:
	ret
	.cfi_endproc
.LFE56:
	.size	llhttp_resume_after_upgrade, .-llhttp_resume_after_upgrade
	.p2align 4
	.globl	llhttp_get_errno
	.type	llhttp_get_errno, @function
llhttp_get_errno:
.LFB57:
	.cfi_startproc
	endbr64
	movl	24(%rdi), %eax
	ret
	.cfi_endproc
.LFE57:
	.size	llhttp_get_errno, .-llhttp_get_errno
	.p2align 4
	.globl	llhttp_get_error_reason
	.type	llhttp_get_error_reason, @function
llhttp_get_error_reason:
.LFB58:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	ret
	.cfi_endproc
.LFE58:
	.size	llhttp_get_error_reason, .-llhttp_get_error_reason
	.p2align 4
	.globl	llhttp_set_error_reason
	.type	llhttp_set_error_reason, @function
llhttp_set_error_reason:
.LFB59:
	.cfi_startproc
	endbr64
	movq	%rsi, 32(%rdi)
	ret
	.cfi_endproc
.LFE59:
	.size	llhttp_set_error_reason, .-llhttp_set_error_reason
	.p2align 4
	.globl	llhttp_get_error_pos
	.type	llhttp_get_error_pos, @function
llhttp_get_error_pos:
.LFB60:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	ret
	.cfi_endproc
.LFE60:
	.size	llhttp_get_error_pos, .-llhttp_get_error_pos
	.section	.rodata.str1.1
.LC3:
	.string	"HPE_USER"
.LC4:
	.string	"HPE_OK"
.LC5:
	.string	"HPE_STRICT"
.LC6:
	.string	"HPE_LF_EXPECTED"
.LC7:
	.string	"HPE_UNEXPECTED_CONTENT_LENGTH"
.LC8:
	.string	"HPE_CLOSED_CONNECTION"
.LC9:
	.string	"HPE_INVALID_METHOD"
.LC10:
	.string	"HPE_INVALID_URL"
.LC11:
	.string	"HPE_INVALID_CONSTANT"
.LC12:
	.string	"HPE_INVALID_VERSION"
.LC13:
	.string	"HPE_INVALID_HEADER_TOKEN"
.LC14:
	.string	"HPE_INVALID_CONTENT_LENGTH"
.LC15:
	.string	"HPE_INVALID_CHUNK_SIZE"
.LC16:
	.string	"HPE_INVALID_STATUS"
.LC17:
	.string	"HPE_INVALID_EOF_STATE"
.LC18:
	.string	"HPE_INVALID_TRANSFER_ENCODING"
.LC19:
	.string	"HPE_CB_MESSAGE_BEGIN"
.LC20:
	.string	"HPE_CB_HEADERS_COMPLETE"
.LC21:
	.string	"HPE_CB_MESSAGE_COMPLETE"
.LC22:
	.string	"HPE_CB_CHUNK_HEADER"
.LC23:
	.string	"HPE_CB_CHUNK_COMPLETE"
.LC24:
	.string	"HPE_PAUSED"
.LC25:
	.string	"HPE_PAUSED_UPGRADE"
.LC26:
	.string	"HPE_INTERNAL"
	.section	.text.unlikely
.LCOLDB27:
	.text
.LHOTB27:
	.p2align 4
	.globl	llhttp_errno_name
	.type	llhttp_errno_name, @function
llhttp_errno_name:
.LFB61:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpl	$23, %edi
	ja	.L41
	leaq	.L43(%rip), %rdx
	movl	%edi, %edi
	movslq	(%rdx,%rdi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L43:
	.long	.L66-.L43
	.long	.L65-.L43
	.long	.L67-.L43
	.long	.L63-.L43
	.long	.L62-.L43
	.long	.L61-.L43
	.long	.L60-.L43
	.long	.L59-.L43
	.long	.L58-.L43
	.long	.L57-.L43
	.long	.L56-.L43
	.long	.L55-.L43
	.long	.L54-.L43
	.long	.L53-.L43
	.long	.L52-.L43
	.long	.L51-.L43
	.long	.L50-.L43
	.long	.L49-.L43
	.long	.L48-.L43
	.long	.L47-.L43
	.long	.L46-.L43
	.long	.L45-.L43
	.long	.L44-.L43
	.long	.L42-.L43
	.text
	.p2align 4,,10
	.p2align 3
.L44:
	leaq	.LC25(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	.cfi_restore_state
	leaq	.LC24(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	.cfi_restore_state
	leaq	.LC23(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	.cfi_restore_state
	leaq	.LC22(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore_state
	leaq	.LC21(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	.cfi_restore_state
	leaq	.LC20(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	.cfi_restore_state
	leaq	.LC19(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L51:
	.cfi_restore_state
	leaq	.LC18(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	.cfi_restore_state
	leaq	.LC17(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	.cfi_restore_state
	leaq	.LC16(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	.cfi_restore_state
	leaq	.LC15(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	.cfi_restore_state
	leaq	.LC14(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	.cfi_restore_state
	leaq	.LC13(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	.cfi_restore_state
	leaq	.LC12(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L58:
	.cfi_restore_state
	leaq	.LC11(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore_state
	leaq	.LC10(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L60:
	.cfi_restore_state
	leaq	.LC9(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L61:
	.cfi_restore_state
	leaq	.LC8(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	.cfi_restore_state
	leaq	.LC7(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L63:
	.cfi_restore_state
	leaq	.LC6(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	.cfi_restore_state
	leaq	.LC3(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	.cfi_restore_state
	leaq	.LC5(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	.cfi_restore_state
	leaq	.LC26(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	.cfi_restore_state
	leaq	.LC4(%rip), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	llhttp_errno_name.cold, @function
llhttp_errno_name.cold:
.LFSB61:
.L41:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	call	abort@PLT
	.cfi_endproc
.LFE61:
	.text
	.size	llhttp_errno_name, .-llhttp_errno_name
	.section	.text.unlikely
	.size	llhttp_errno_name.cold, .-llhttp_errno_name.cold
.LCOLDE27:
	.text
.LHOTE27:
	.section	.rodata.str1.1
.LC28:
	.string	"PRI"
.LC29:
	.string	"DELETE"
.LC30:
	.string	"HEAD"
.LC31:
	.string	"POST"
.LC32:
	.string	"PUT"
.LC33:
	.string	"CONNECT"
.LC34:
	.string	"OPTIONS"
.LC35:
	.string	"TRACE"
.LC36:
	.string	"COPY"
.LC37:
	.string	"LOCK"
.LC38:
	.string	"MKCOL"
.LC39:
	.string	"MOVE"
.LC40:
	.string	"PROPFIND"
.LC41:
	.string	"PROPPATCH"
.LC42:
	.string	"SEARCH"
.LC43:
	.string	"UNLOCK"
.LC44:
	.string	"BIND"
.LC45:
	.string	"REBIND"
.LC46:
	.string	"UNBIND"
.LC47:
	.string	"ACL"
.LC48:
	.string	"REPORT"
.LC49:
	.string	"MKACTIVITY"
.LC50:
	.string	"CHECKOUT"
.LC51:
	.string	"MERGE"
.LC52:
	.string	"M-SEARCH"
.LC53:
	.string	"NOTIFY"
.LC54:
	.string	"SUBSCRIBE"
.LC55:
	.string	"UNSUBSCRIBE"
.LC56:
	.string	"PATCH"
.LC57:
	.string	"PURGE"
.LC58:
	.string	"MKCALENDAR"
.LC59:
	.string	"LINK"
.LC60:
	.string	"UNLINK"
.LC61:
	.string	"SOURCE"
.LC62:
	.string	"GET"
	.section	.text.unlikely
.LCOLDB63:
	.text
.LHOTB63:
	.p2align 4
	.globl	llhttp_method_name
	.type	llhttp_method_name, @function
llhttp_method_name:
.LFB62:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpl	$34, %edi
	ja	.L70
	leaq	.L72(%rip), %rdx
	movl	%edi, %edi
	movslq	(%rdx,%rdi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L72:
	.long	.L106-.L72
	.long	.L105-.L72
	.long	.L107-.L72
	.long	.L103-.L72
	.long	.L102-.L72
	.long	.L101-.L72
	.long	.L100-.L72
	.long	.L99-.L72
	.long	.L98-.L72
	.long	.L97-.L72
	.long	.L96-.L72
	.long	.L95-.L72
	.long	.L94-.L72
	.long	.L93-.L72
	.long	.L92-.L72
	.long	.L91-.L72
	.long	.L90-.L72
	.long	.L89-.L72
	.long	.L88-.L72
	.long	.L87-.L72
	.long	.L86-.L72
	.long	.L85-.L72
	.long	.L84-.L72
	.long	.L83-.L72
	.long	.L82-.L72
	.long	.L81-.L72
	.long	.L80-.L72
	.long	.L79-.L72
	.long	.L78-.L72
	.long	.L77-.L72
	.long	.L76-.L72
	.long	.L75-.L72
	.long	.L74-.L72
	.long	.L73-.L72
	.long	.L71-.L72
	.text
	.p2align 4,,10
	.p2align 3
.L73:
	leaq	.LC61(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L74:
	.cfi_restore_state
	leaq	.LC60(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L75:
	.cfi_restore_state
	leaq	.LC59(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L76:
	.cfi_restore_state
	leaq	.LC58(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L77:
	.cfi_restore_state
	leaq	.LC57(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L78:
	.cfi_restore_state
	leaq	.LC56(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	.cfi_restore_state
	leaq	.LC55(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L80:
	.cfi_restore_state
	leaq	.LC54(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L81:
	.cfi_restore_state
	leaq	.LC53(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L82:
	.cfi_restore_state
	leaq	.LC52(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L83:
	.cfi_restore_state
	leaq	.LC51(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	.cfi_restore_state
	leaq	.LC50(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L85:
	.cfi_restore_state
	leaq	.LC49(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L86:
	.cfi_restore_state
	leaq	.LC48(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L87:
	.cfi_restore_state
	leaq	.LC47(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L88:
	.cfi_restore_state
	leaq	.LC46(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L89:
	.cfi_restore_state
	leaq	.LC45(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L90:
	.cfi_restore_state
	leaq	.LC44(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L91:
	.cfi_restore_state
	leaq	.LC43(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L92:
	.cfi_restore_state
	leaq	.LC42(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L93:
	.cfi_restore_state
	leaq	.LC41(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	.cfi_restore_state
	leaq	.LC40(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L95:
	.cfi_restore_state
	leaq	.LC39(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L96:
	.cfi_restore_state
	leaq	.LC38(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L97:
	.cfi_restore_state
	leaq	.LC37(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L98:
	.cfi_restore_state
	leaq	.LC36(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L99:
	.cfi_restore_state
	leaq	.LC35(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L100:
	.cfi_restore_state
	leaq	.LC34(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L101:
	.cfi_restore_state
	leaq	.LC33(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L102:
	.cfi_restore_state
	leaq	.LC32(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L103:
	.cfi_restore_state
	leaq	.LC31(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L71:
	.cfi_restore_state
	leaq	.LC28(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L107:
	.cfi_restore_state
	leaq	.LC30(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L105:
	.cfi_restore_state
	leaq	.LC62(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L106:
	.cfi_restore_state
	leaq	.LC29(%rip), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	llhttp_method_name.cold, @function
llhttp_method_name.cold:
.LFSB62:
.L70:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	call	abort@PLT
	.cfi_endproc
.LFE62:
	.text
	.size	llhttp_method_name, .-llhttp_method_name
	.section	.text.unlikely
	.size	llhttp_method_name.cold, .-llhttp_method_name.cold
.LCOLDE63:
	.text
.LHOTE63:
	.p2align 4
	.globl	llhttp_set_lenient
	.type	llhttp_set_lenient, @function
llhttp_set_lenient:
.LFB63:
	.cfi_startproc
	endbr64
	movzwl	78(%rdi), %eax
	movl	%eax, %edx
	andb	$-2, %ah
	orb	$1, %dh
	testl	%esi, %esi
	cmovne	%edx, %eax
	movw	%ax, 78(%rdi)
	ret
	.cfi_endproc
.LFE63:
	.size	llhttp_set_lenient, .-llhttp_set_lenient
	.p2align 4
	.globl	llhttp__on_message_begin
	.type	llhttp__on_message_begin, @function
llhttp__on_message_begin:
.LFB64:
	.cfi_startproc
	endbr64
	movq	88(%rdi), %rax
	testq	%rax, %rax
	je	.L112
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L112
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L112:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE64:
	.size	llhttp__on_message_begin, .-llhttp__on_message_begin
	.p2align 4
	.globl	llhttp__on_url
	.type	llhttp__on_url, @function
llhttp__on_url:
.LFB65:
	.cfi_startproc
	endbr64
	movq	88(%rdi), %rax
	testq	%rax, %rax
	je	.L118
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L118
	subq	%rsi, %rdx
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L118:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE65:
	.size	llhttp__on_url, .-llhttp__on_url
	.p2align 4
	.globl	llhttp__on_status
	.type	llhttp__on_status, @function
llhttp__on_status:
.LFB66:
	.cfi_startproc
	endbr64
	movq	88(%rdi), %rax
	testq	%rax, %rax
	je	.L124
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L124
	subq	%rsi, %rdx
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L124:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE66:
	.size	llhttp__on_status, .-llhttp__on_status
	.p2align 4
	.globl	llhttp__on_header_field
	.type	llhttp__on_header_field, @function
llhttp__on_header_field:
.LFB67:
	.cfi_startproc
	endbr64
	movq	88(%rdi), %rax
	testq	%rax, %rax
	je	.L130
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L130
	subq	%rsi, %rdx
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L130:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE67:
	.size	llhttp__on_header_field, .-llhttp__on_header_field
	.p2align 4
	.globl	llhttp__on_header_value
	.type	llhttp__on_header_value, @function
llhttp__on_header_value:
.LFB68:
	.cfi_startproc
	endbr64
	movq	88(%rdi), %rax
	testq	%rax, %rax
	je	.L136
	movq	32(%rax), %rax
	testq	%rax, %rax
	je	.L136
	subq	%rsi, %rdx
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L136:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE68:
	.size	llhttp__on_header_value, .-llhttp__on_header_value
	.p2align 4
	.globl	llhttp__on_headers_complete
	.type	llhttp__on_headers_complete, @function
llhttp__on_headers_complete:
.LFB69:
	.cfi_startproc
	endbr64
	movq	88(%rdi), %rax
	testq	%rax, %rax
	je	.L142
	movq	40(%rax), %rax
	testq	%rax, %rax
	je	.L142
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L142:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE69:
	.size	llhttp__on_headers_complete, .-llhttp__on_headers_complete
	.p2align 4
	.globl	llhttp__on_message_complete
	.type	llhttp__on_message_complete, @function
llhttp__on_message_complete:
.LFB70:
	.cfi_startproc
	endbr64
	movq	88(%rdi), %rax
	testq	%rax, %rax
	je	.L148
	movq	56(%rax), %rax
	testq	%rax, %rax
	je	.L148
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L148:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE70:
	.size	llhttp__on_message_complete, .-llhttp__on_message_complete
	.p2align 4
	.globl	llhttp__on_body
	.type	llhttp__on_body, @function
llhttp__on_body:
.LFB71:
	.cfi_startproc
	endbr64
	movq	88(%rdi), %rax
	testq	%rax, %rax
	je	.L154
	movq	48(%rax), %rax
	testq	%rax, %rax
	je	.L154
	subq	%rsi, %rdx
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L154:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE71:
	.size	llhttp__on_body, .-llhttp__on_body
	.p2align 4
	.globl	llhttp__on_chunk_header
	.type	llhttp__on_chunk_header, @function
llhttp__on_chunk_header:
.LFB72:
	.cfi_startproc
	endbr64
	movq	88(%rdi), %rax
	testq	%rax, %rax
	je	.L160
	movq	64(%rax), %rax
	testq	%rax, %rax
	je	.L160
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L160:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE72:
	.size	llhttp__on_chunk_header, .-llhttp__on_chunk_header
	.p2align 4
	.globl	llhttp__on_chunk_complete
	.type	llhttp__on_chunk_complete, @function
llhttp__on_chunk_complete:
.LFB73:
	.cfi_startproc
	endbr64
	movq	88(%rdi), %rax
	testq	%rax, %rax
	je	.L166
	movq	72(%rax), %rax
	testq	%rax, %rax
	je	.L166
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L166:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE73:
	.size	llhttp__on_chunk_complete, .-llhttp__on_chunk_complete
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC64:
	.string	"p=%p type=%d flags=%02x next=null debug=%s\n"
	.align 8
.LC65:
	.string	"p=%p type=%d flags=%02x next=%02x   debug=%s\n"
	.text
	.p2align 4
	.globl	llhttp__debug
	.type	llhttp__debug, @function
llhttp__debug:
.LFB74:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r10
	movzwl	78(%rdi), %r9d
	movzbl	72(%rdi), %r8d
	movq	stderr(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpq	%rdx, %rsi
	je	.L176
	pushq	%rcx
	movsbl	(%rsi), %eax
	leaq	.LC65(%rip), %rdx
	movq	%r10, %rcx
	movl	$1, %esi
	pushq	%rax
	xorl	%eax, %eax
	call	__fprintf_chk@PLT
	popq	%rax
	popq	%rdx
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L176:
	.cfi_restore_state
	subq	$8, %rsp
	movl	$1, %esi
	leaq	.LC64(%rip), %rdx
	xorl	%eax, %eax
	pushq	%rcx
	movq	%r10, %rcx
	call	__fprintf_chk@PLT
	popq	%rcx
	popq	%rsi
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE74:
	.size	llhttp__debug, .-llhttp__debug
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
