	.file	"http.c"
	.text
	.p2align 4
	.globl	llhttp__before_headers_complete
	.type	llhttp__before_headers_complete, @function
llhttp__before_headers_complete:
.LFB23:
	.cfi_startproc
	endbr64
	movzwl	78(%rdi), %eax
	andl	$20, %eax
	cmpw	$20, %ax
	je	.L9
	cmpb	$5, 73(%rdi)
	sete	80(%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movzbl	72(%rdi), %eax
	cmpb	$1, %al
	je	.L3
	cmpw	$101, 82(%rdi)
	sete	%al
.L3:
	movb	%al, 80(%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE23:
	.size	llhttp__before_headers_complete, .-llhttp__before_headers_complete
	.p2align 4
	.globl	llhttp__after_headers_complete
	.type	llhttp__after_headers_complete, @function
llhttp__after_headers_complete:
.LFB24:
	.cfi_startproc
	endbr64
	movzwl	78(%rdi), %eax
	movl	$1, %edx
	movl	%eax, %ecx
	andw	$8, %cx
	jne	.L11
	xorl	%edx, %edx
	cmpq	$0, 64(%rdi)
	setne	%dl
.L11:
	cmpb	$0, 80(%rdi)
	je	.L12
	cmpb	$5, 73(%rdi)
	movl	$1, %r8d
	je	.L10
	testb	$64, %al
	jne	.L19
	testl	%edx, %edx
	je	.L19
.L14:
	movl	$2, %r8d
	testw	%cx, %cx
	jne	.L10
	testb	$2, %ah
	jne	.L31
	testb	$32, %al
	jne	.L16
	xorl	%r8d, %r8d
	cmpb	$1, 72(%rdi)
	je	.L10
	movzwl	82(%rdi), %edx
	cmpw	$204, %dx
	sete	%sil
	cmpw	$304, %dx
	sete	%cl
	orb	%cl, %sil
	jne	.L25
	subl	$100, %edx
	cmpw	$99, %dx
	jbe	.L25
	movl	%eax, %edx
	movl	$4, %r8d
	andw	$520, %dx
	cmpw	$512, %dx
	je	.L10
	xorl	%r8d, %r8d
	testb	$40, %al
	sete	%r8b
	sall	$2, %r8d
.L10:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	xorl	%r8d, %r8d
	testb	$64, %al
	je	.L14
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	movl	$1, %r8d
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	cmpb	$1, 72(%rdi)
	movl	$4, %r8d
	jne	.L10
	xorl	%r8d, %r8d
	testb	$1, %ah
	sete	%r8b
	addl	$4, %r8d
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	cmpq	$1, 64(%rdi)
	sbbl	%r8d, %r8d
	notl	%r8d
	andl	$3, %r8d
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L25:
	xorl	%r8d, %r8d
	jmp	.L10
	.cfi_endproc
.LFE24:
	.size	llhttp__after_headers_complete, .-llhttp__after_headers_complete
	.p2align 4
	.globl	llhttp__after_message_complete
	.type	llhttp__after_message_complete, @function
llhttp__after_message_complete:
.LFB25:
	.cfi_startproc
	endbr64
	xorl	%r8d, %r8d
	cmpb	$0, 74(%rdi)
	movzwl	78(%rdi), %eax
	je	.L33
	cmpb	$0, 75(%rdi)
	je	.L33
	testb	$2, %al
	jne	.L34
.L35:
	cmpb	$1, 72(%rdi)
	movl	$1, %r8d
	je	.L34
	movzwl	82(%rdi), %edx
	cmpw	$304, %dx
	sete	%sil
	cmpw	$204, %dx
	sete	%cl
	orb	%cl, %sil
	jne	.L40
	subl	$100, %edx
	cmpw	$99, %dx
	jbe	.L40
	testb	$64, %al
	jne	.L34
	movl	%eax, %edx
	xorl	%r8d, %r8d
	andw	$520, %dx
	cmpw	$512, %dx
	je	.L34
	xorl	%r8d, %r8d
	testb	$40, %al
	setne	%r8b
.L34:
	andw	$256, %ax
	movb	$0, 84(%rdi)
	movw	%ax, 78(%rdi)
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	testb	$1, %al
	jne	.L35
	andw	$256, %ax
	movb	$0, 84(%rdi)
	movw	%ax, 78(%rdi)
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	andw	$256, %ax
	movl	$1, %r8d
	movb	$0, 84(%rdi)
	movw	%ax, 78(%rdi)
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE25:
	.size	llhttp__after_message_complete, .-llhttp__after_message_complete
	.p2align 4
	.globl	llhttp_message_needs_eof
	.type	llhttp_message_needs_eof, @function
llhttp_message_needs_eof:
.LFB26:
	.cfi_startproc
	endbr64
	xorl	%r8d, %r8d
	cmpb	$1, 72(%rdi)
	je	.L46
	movzwl	82(%rdi), %eax
	cmpw	$204, %ax
	sete	%cl
	cmpw	$304, %ax
	sete	%dl
	orb	%dl, %cl
	jne	.L49
	subl	$100, %eax
	cmpw	$99, %ax
	jbe	.L49
	movzwl	78(%rdi), %eax
	testb	$64, %al
	jne	.L46
	movl	%eax, %edx
	movl	$1, %r8d
	andw	$520, %dx
	cmpw	$512, %dx
	je	.L46
	xorl	%r8d, %r8d
	testb	$40, %al
	sete	%r8b
.L46:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	xorl	%r8d, %r8d
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE26:
	.size	llhttp_message_needs_eof, .-llhttp_message_needs_eof
	.p2align 4
	.globl	llhttp_should_keep_alive
	.type	llhttp_should_keep_alive, @function
llhttp_should_keep_alive:
.LFB27:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpb	$0, 74(%rdi)
	movzwl	78(%rdi), %ecx
	je	.L54
	cmpb	$0, 75(%rdi)
	je	.L54
	testb	$2, %cl
	jne	.L66
.L56:
	cmpb	$1, 72(%rdi)
	movl	$1, %eax
	je	.L53
	movzwl	82(%rdi), %edx
	cmpw	$304, %dx
	sete	%dil
	cmpw	$204, %dx
	sete	%sil
	orb	%sil, %dil
	jne	.L61
	subl	$100, %edx
	cmpw	$99, %dx
	jbe	.L61
	testb	$64, %cl
	jne	.L53
	movl	%ecx, %edx
	xorl	%eax, %eax
	andw	$520, %dx
	cmpw	$512, %dx
	je	.L53
	xorl	%eax, %eax
	andl	$40, %ecx
	setne	%al
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	testb	$1, %cl
	jne	.L56
.L53:
	ret
	.p2align 4,,10
	.p2align 3
.L61:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	ret
	.cfi_endproc
.LFE27:
	.size	llhttp_should_keep_alive, .-llhttp_should_keep_alive
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
