	.file	"node_main.cc"
	.text
	.section	.text.startup,"ax",@progbits
	.p2align 4
	.globl	main
	.type	main, @function
main:
.LFB2811:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	environ(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movl	%edi, %r12d
	.p2align 4,,10
	.p2align 3
.L2:
	addq	$8, %rax
	cmpq	$0, -8(%rax)
	jne	.L2
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L3
	.p2align 4,,10
	.p2align 3
.L5:
	cmpq	$23, %rdx
	je	.L14
	movq	16(%rax), %rdx
	addq	$16, %rax
	testq	%rdx, %rdx
	jne	.L5
.L3:
	movq	stdout(%rip), %rdi
	xorl	%ecx, %ecx
	movl	$2, %edx
	xorl	%esi, %esi
	call	setvbuf@PLT
	movq	stderr(%rip), %rdi
	xorl	%esi, %esi
	xorl	%ecx, %ecx
	movl	$2, %edx
	call	setvbuf@PLT
	movq	%r13, %rsi
	movl	%r12d, %edi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node5StartEiPPc@PLT
.L14:
	.cfi_restore_state
	cmpq	$0, 8(%rax)
	setne	_ZN4node11per_process15linux_at_secureE(%rip)
	jmp	.L3
	.cfi_endproc
.LFE2811:
	.size	main, .-main
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
