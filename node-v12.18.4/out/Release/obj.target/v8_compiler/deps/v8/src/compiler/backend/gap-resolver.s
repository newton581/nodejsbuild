	.file	"gap-resolver.cc"
	.text
	.section	.text._ZN2v88internal8compiler11GapResolver11PerformMoveEPNS1_12ParallelMoveEPNS1_12MoveOperandsE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler11GapResolver11PerformMoveEPNS1_12ParallelMoveEPNS1_12MoveOperandsE
	.type	_ZN2v88internal8compiler11GapResolver11PerformMoveEPNS1_12ParallelMoveEPNS1_12MoveOperandsE, @function
_ZN2v88internal8compiler11GapResolver11PerformMoveEPNS1_12ParallelMoveEPNS1_12MoveOperandsE:
.LFB10763:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-64(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	%rdi, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	movq	8(%rdx), %rdx
	movq	$0, 8(%r12)
	movq	8(%rsi), %rcx
	movq	16(%rsi), %rsi
	movq	%rax, -72(%rbp)
	movq	%rdx, -64(%rbp)
	cmpq	%rsi, %rcx
	je	.L7
.L2:
	movq	(%rcx,%r13,8), %r14
	testb	$7, (%r14)
	je	.L5
	testb	$7, 8(%r14)
	je	.L5
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler18InstructionOperand14InterferesWithERKS2_@PLT
	testb	%al, %al
	jne	.L6
.L117:
	movq	8(%rbx), %rcx
	movq	16(%rbx), %rsi
.L5:
	movq	%rsi, %rax
	addq	$1, %r13
	subq	%rcx, %rax
	sarq	$3, %rax
	cmpq	%r13, %rax
	ja	.L2
	movq	(%r12), %rax
.L7:
	movq	%rax, -72(%rbp)
	movq	%rax, %rsi
	testb	$4, %al
	je	.L4
	xorl	%edx, %edx
	testb	$24, %al
	je	.L120
.L8:
	andq	$-8161, %rax
	orq	%rdx, %rax
	andq	$-8, %rax
	orq	$4, %rax
	movq	%rax, %rsi
.L4:
	movq	-64(%rbp), %rcx
	movq	%rcx, %rdx
	testb	$4, %cl
	je	.L9
	xorl	%eax, %eax
	testb	$24, %cl
	je	.L121
.L10:
	movq	%rcx, %rdx
	andq	$-8161, %rdx
	orq	%rax, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L9:
	cmpq	%rsi, %rdx
	jne	.L11
.L118:
	movq	$0, 8(%r12)
	movq	$0, (%r12)
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L122
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L121:
	.cfi_restore_state
	movq	%rcx, %rax
	shrq	$5, %rax
	cmpb	$12, %al
	sbbq	%rax, %rax
	notq	%rax
	andl	$416, %eax
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L120:
	movq	%rax, %rdx
	shrq	$5, %rdx
	cmpb	$12, %dl
	sbbq	%rdx, %rdx
	notq	%rdx
	andl	$416, %edx
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L11:
	movq	%rcx, 8(%r12)
	movq	16(%rbx), %rax
	leaq	-64(%rbp), %r15
	movq	8(%rbx), %r14
	movq	%rax, %r13
	movq	%rax, -96(%rbp)
	subq	%r14, %r13
	movq	%r13, %rax
	sarq	$5, %r13
	sarq	$3, %rax
	testq	%r13, %r13
	jle	.L13
	salq	$5, %r13
	leaq	-64(%rbp), %r15
	addq	%r14, %r13
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L17:
	movq	8(%r14), %rdi
	testb	$7, (%rdi)
	jne	.L123
.L16:
	movq	16(%r14), %rdi
	testb	$7, (%rdi)
	jne	.L124
.L21:
	movq	24(%r14), %rdi
	testb	$7, (%rdi)
	jne	.L125
.L24:
	addq	$32, %r14
	cmpq	%r13, %r14
	je	.L126
.L27:
	movq	(%r14), %rdi
	testb	$7, (%rdi)
	je	.L17
	movq	%r15, %rsi
	call	_ZNK2v88internal8compiler18InstructionOperand14InterferesWithERKS2_@PLT
	testb	%al, %al
	je	.L17
.L18:
	movq	-88(%rbp), %rax
	movq	(%rax), %rdi
	cmpq	%r14, 16(%rbx)
	je	.L127
	movq	-72(%rbp), %rax
	testb	$4, %al
	je	.L39
	movq	%rax, %rdx
	shrq	$3, %rdx
	andl	$3, %edx
	cmpl	$1, %edx
	je	.L128
.L39:
	movq	(%rdi), %rax
	movq	%r15, %rdx
	leaq	-72(%rbp), %rsi
	call	*24(%rax)
	movq	$0, (%r12)
	movq	$0, 8(%r12)
	movq	16(%rbx), %r9
	movq	8(%rbx), %rdx
	cmpq	%rdx, %r9
	jne	.L51
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L129:
	movq	%rdi, (%r10)
.L41:
	addq	$8, %rdx
	cmpq	%rdx, %r9
	je	.L1
.L51:
	movq	(%rdx), %r10
	movq	(%r10), %rax
	movl	%eax, %edi
	testb	$7, %al
	je	.L41
	movq	-72(%rbp), %r8
	movq	%r8, %rcx
	testb	$4, %r8b
	je	.L42
	xorl	%esi, %esi
	testb	$24, %r8b
	jne	.L43
	shrq	$5, %rcx
	cmpb	$12, %cl
	sbbq	%rsi, %rsi
	notq	%rsi
	andl	$416, %esi
.L43:
	movq	%r8, %rcx
	andq	$-8161, %rcx
	orq	%rsi, %rcx
	andq	$-8, %rcx
	orq	$4, %rcx
.L42:
	andl	$4, %edi
	movq	%rax, %rsi
	movl	%edi, %r11d
	je	.L44
	xorl	%edi, %edi
	testb	$24, %al
	jne	.L45
	shrq	$5, %rsi
	cmpb	$12, %sil
	sbbq	%rdi, %rdi
	notq	%rdi
	andl	$416, %edi
.L45:
	movq	%rax, %rsi
	andq	$-8161, %rsi
	orq	%rdi, %rsi
	andq	$-8, %rsi
	orq	$4, %rsi
.L44:
	movq	-64(%rbp), %rdi
	cmpq	%rcx, %rsi
	je	.L129
	testb	$4, %dil
	je	.L47
	xorl	%ecx, %ecx
	testb	$24, %dil
	jne	.L48
	movq	%rdi, %rcx
	shrq	$5, %rcx
	cmpb	$12, %cl
	sbbq	%rcx, %rcx
	notq	%rcx
	andl	$416, %ecx
.L48:
	andq	$-8161, %rdi
	orq	%rcx, %rdi
	andq	$-8, %rdi
	orq	$4, %rdi
.L47:
	testl	%r11d, %r11d
	je	.L49
	xorl	%ecx, %ecx
	testb	$24, %al
	jne	.L50
	movq	%rax, %rcx
	shrq	$5, %rcx
	cmpb	$12, %cl
	sbbq	%rcx, %rcx
	notq	%rcx
	andl	$416, %ecx
.L50:
	andq	$-8161, %rax
	orq	%rcx, %rax
	andq	$-8, %rax
	orq	$4, %rax
.L49:
	cmpq	%rdi, %rax
	jne	.L41
	movq	%r8, (%r10)
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L123:
	movq	%r15, %rsi
	call	_ZNK2v88internal8compiler18InstructionOperand14InterferesWithERKS2_@PLT
	testb	%al, %al
	je	.L16
	addq	$8, %r14
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L124:
	movq	%r15, %rsi
	call	_ZNK2v88internal8compiler18InstructionOperand14InterferesWithERKS2_@PLT
	testb	%al, %al
	je	.L21
	addq	$16, %r14
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L125:
	movq	%r15, %rsi
	call	_ZNK2v88internal8compiler18InstructionOperand14InterferesWithERKS2_@PLT
	testb	%al, %al
	je	.L24
	addq	$24, %r14
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L6:
	movq	-88(%rbp), %rdi
	movq	%r14, %rdx
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler11GapResolver11PerformMoveEPNS1_12ParallelMoveEPNS1_12MoveOperandsE
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L128:
	movq	-64(%rbp), %rdx
	movq	%rax, -64(%rbp)
	movq	%rdx, -72(%rbp)
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L126:
	movq	-96(%rbp), %rax
	subq	%r14, %rax
	sarq	$3, %rax
.L13:
	cmpq	$2, %rax
	je	.L28
	cmpq	$3, %rax
	je	.L29
	cmpq	$1, %rax
	je	.L30
.L31:
	movq	-96(%rbp), %r14
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L127:
	movq	(%rdi), %rax
	leaq	-72(%rbp), %rsi
	movq	%r15, %rdx
	call	*16(%rax)
	jmp	.L118
.L29:
	movq	(%r14), %rdi
	testb	$7, (%rdi)
	jne	.L130
.L32:
	addq	$8, %r14
.L28:
	movq	(%r14), %rdi
	testb	$7, (%rdi)
	jne	.L131
.L34:
	addq	$8, %r14
.L30:
	movq	(%r14), %rdi
	testb	$7, (%rdi)
	je	.L31
	movq	%r15, %rsi
	call	_ZNK2v88internal8compiler18InstructionOperand14InterferesWithERKS2_@PLT
	testb	%al, %al
	jne	.L18
	jmp	.L31
.L131:
	movq	%r15, %rsi
	call	_ZNK2v88internal8compiler18InstructionOperand14InterferesWithERKS2_@PLT
	testb	%al, %al
	jne	.L18
	jmp	.L34
.L130:
	movq	%r15, %rsi
	call	_ZNK2v88internal8compiler18InstructionOperand14InterferesWithERKS2_@PLT
	testb	%al, %al
	jne	.L18
	jmp	.L32
.L122:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10763:
	.size	_ZN2v88internal8compiler11GapResolver11PerformMoveEPNS1_12ParallelMoveEPNS1_12MoveOperandsE, .-_ZN2v88internal8compiler11GapResolver11PerformMoveEPNS1_12ParallelMoveEPNS1_12MoveOperandsE
	.section	.rodata._ZNSt6vectorIPN2v88internal8compiler12MoveOperandsENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm.str1.1,"aMS",@progbits,1
.LC0:
	.string	"vector::_M_default_append"
	.section	.text._ZNSt6vectorIPN2v88internal8compiler12MoveOperandsENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm,"axG",@progbits,_ZNSt6vectorIPN2v88internal8compiler12MoveOperandsENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal8compiler12MoveOperandsENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm
	.type	_ZNSt6vectorIPN2v88internal8compiler12MoveOperandsENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm, @function
_ZNSt6vectorIPN2v88internal8compiler12MoveOperandsENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm:
.LFB12123:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L160
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movl	$268435455, %esi
	subq	$24, %rsp
	movq	16(%rdi), %rcx
	movq	24(%r13), %rax
	movq	%rcx, %rdx
	subq	8(%rdi), %rdx
	subq	%rcx, %rax
	movq	%rsi, %rdi
	movq	%rdx, %r14
	sarq	$3, %rax
	sarq	$3, %r14
	subq	%r14, %rdi
	cmpq	%rbx, %rax
	jb	.L134
	salq	$3, %rbx
	movq	%rcx, %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	call	memset@PLT
	movq	%rax, %rcx
	addq	%rbx, %rcx
	movq	%rcx, 16(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L160:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L134:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	cmpq	%rbx, %rdi
	jb	.L163
	cmpq	%r14, %rbx
	movq	%r14, %r15
	movq	0(%r13), %rdi
	cmovnb	%rbx, %r15
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	addq	%r14, %r15
	cmpq	$268435455, %r15
	cmova	%rsi, %r15
	subq	%r12, %rax
	salq	$3, %r15
	movq	%r15, %rsi
	cmpq	%r15, %rax
	jb	.L164
	addq	%r12, %rsi
	movq	%rsi, 16(%rdi)
.L138:
	leaq	0(,%rbx,8), %r8
	leaq	(%r12,%rdx), %rdi
	xorl	%esi, %esi
	movq	%r8, %rdx
	call	memset@PLT
	movq	16(%r13), %rax
	movq	8(%r13), %rdx
	cmpq	%rdx, %rax
	je	.L143
	subq	$8, %rax
	leaq	15(%rdx), %rcx
	subq	%rdx, %rax
	subq	%r12, %rcx
	movq	%rax, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rcx
	jbe	.L140
	movabsq	$2305843009213693948, %rcx
	testq	%rcx, %rsi
	je	.L140
	addq	$1, %rsi
	movq	%rdx, %rdi
	movq	%r12, %rax
	movq	%rsi, %rcx
	subq	%r12, %rdi
	shrq	%rcx
	salq	$4, %rcx
	addq	%r12, %rcx
	.p2align 4,,10
	.p2align 3
.L141:
	movdqu	(%rax,%rdi), %xmm0
	addq	$16, %rax
	movups	%xmm0, -16(%rax)
	cmpq	%rax, %rcx
	jne	.L141
	movq	%rsi, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rcx
	addq	%rcx, %rdx
	addq	%r12, %rcx
	cmpq	%rax, %rsi
	je	.L143
	movq	(%rdx), %rax
	movq	%rax, (%rcx)
.L143:
	addq	%r14, %rbx
	movq	%r12, 8(%r13)
	leaq	(%r12,%rbx,8), %rax
	addq	%r15, %r12
	movq	%rax, 16(%r13)
	movq	%r12, 24(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L140:
	.cfi_restore_state
	leaq	8(%rax,%r12), %rsi
	subq	%r12, %rdx
	movq	%r12, %rax
	.p2align 4,,10
	.p2align 3
.L145:
	movq	(%rax,%rdx), %rcx
	addq	$8, %rax
	movq	%rcx, -8(%rax)
	cmpq	%rax, %rsi
	jne	.L145
	jmp	.L143
.L164:
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %r12
	jmp	.L138
.L163:
	leaq	.LC0(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE12123:
	.size	_ZNSt6vectorIPN2v88internal8compiler12MoveOperandsENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm, .-_ZNSt6vectorIPN2v88internal8compiler12MoveOperandsENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm
	.section	.text._ZN2v88internal8compiler11GapResolver7ResolveEPNS1_12ParallelMoveE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler11GapResolver7ResolveEPNS1_12ParallelMoveE
	.type	_ZN2v88internal8compiler11GapResolver7ResolveEPNS1_12ParallelMoveE, @function
_ZN2v88internal8compiler11GapResolver7ResolveEPNS1_12ParallelMoveE:
.LFB10759:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	16(%rsi), %r14
	movq	8(%rsi), %rax
	movq	%r14, %rsi
	subq	%rax, %rsi
	sarq	$3, %rsi
	je	.L166
	xorl	%r10d, %r10d
	xorl	%r11d, %r11d
	xorl	%edi, %edi
	.p2align 4,,10
	.p2align 3
.L167:
	leaq	(%rax,%rdi,8), %r13
	movq	0(%r13), %rcx
	movq	(%rcx), %rdx
	movl	%edx, %r14d
	andl	$7, %r14d
	je	.L168
	movq	%rdx, %r8
	testb	$4, %dl
	je	.L169
	xorl	%r9d, %r9d
	testb	$24, %dl
	jne	.L170
	shrq	$5, %r8
	cmpb	$12, %r8b
	sbbq	%r9, %r9
	notq	%r9
	andl	$416, %r9d
.L170:
	movq	%rdx, %r8
	andq	$-8161, %r8
	orq	%r9, %r8
	andq	$-8, %r8
	orq	$4, %r8
.L169:
	movq	8(%rcx), %rcx
	movq	%rcx, %r9
	testb	$4, %cl
	je	.L171
	xorl	%r15d, %r15d
	testb	$24, %cl
	jne	.L172
	shrq	$5, %r9
	cmpb	$12, %r9b
	sbbq	%r15, %r15
	notq	%r15
	andl	$416, %r15d
.L172:
	movq	%rcx, %r9
	andq	$-8161, %r9
	orq	%r15, %r9
	andq	$-8, %r9
	orq	$4, %r9
.L171:
	cmpq	%r8, %r9
	je	.L168
	addq	$1, %rdi
	movl	$1, %r8d
	cmpl	$2, %r14d
	je	.L173
	movl	$8, %r8d
	testb	$24, %dl
	jne	.L173
	shrq	$5, %rdx
	cmpb	$12, %dl
	sbbl	%r8d, %r8d
	andl	$-2, %r8d
	addl	$4, %r8d
.L173:
	orl	%r8d, %r11d
	movl	%ecx, %r8d
	movl	$1, %edx
	andl	$7, %r8d
	cmpl	$2, %r8d
	je	.L177
	movl	$8, %edx
	testb	$24, %cl
	jne	.L177
	shrq	$5, %rcx
	cmpb	$12, %cl
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
.L177:
	orl	%edx, %r10d
	cmpq	%rsi, %rdi
	jb	.L167
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L168:
	subq	$1, %rsi
	cmpq	%rdi, %rsi
	ja	.L218
.L175:
	movq	16(%rbx), %r14
	andl	%r10d, %r11d
	movl	%r11d, %r13d
	movq	%r14, %rdx
	subq	%rax, %rdx
	sarq	$3, %rdx
	cmpq	%rsi, %rdx
	je	.L178
	jb	.L219
	jbe	.L178
	leaq	(%rax,%rsi,8), %rdx
	cmpq	%rdx, %r14
	je	.L178
	movq	%rdx, 16(%rbx)
	movq	%rdx, %r14
	.p2align 4,,10
	.p2align 3
.L178:
	testb	%r13b, %r13b
	je	.L166
	movq	%r14, %rdx
	xorl	%r13d, %r13d
	subq	%rax, %rdx
	cmpq	$15, %rdx
	ja	.L180
.L166:
	movq	%rax, %rbx
	cmpq	%r14, %rax
	je	.L165
	.p2align 4,,10
	.p2align 3
.L182:
	movq	(%r12), %rdi
	movq	(%rbx), %rsi
	addq	$8, %rbx
	movq	(%rdi), %rax
	leaq	8(%rsi), %rdx
	call	*16(%rax)
	cmpq	%rbx, %r14
	jne	.L182
.L165:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L220:
	.cfi_restore_state
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler11GapResolver11PerformMoveEPNS1_12ParallelMoveEPNS1_12MoveOperandsE
	movq	8(%rbx), %rax
	movq	16(%rbx), %r14
.L184:
	movq	%r14, %rdx
	addq	$1, %r13
	subq	%rax, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %r13
	jnb	.L165
.L180:
	movq	(%rax,%r13,8), %rdx
	testb	$7, (%rdx)
	je	.L184
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L218:
	movq	(%rax,%rsi,8), %rax
	movq	%rax, 0(%r13)
	movq	8(%rbx), %rax
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L219:
	subq	%rdx, %rsi
	movq	%rbx, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler12MoveOperandsENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm
	movq	8(%rbx), %rax
	movq	16(%rbx), %r14
	jmp	.L178
	.cfi_endproc
.LFE10759:
	.size	_ZN2v88internal8compiler11GapResolver7ResolveEPNS1_12ParallelMoveE, .-_ZN2v88internal8compiler11GapResolver7ResolveEPNS1_12ParallelMoveE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler11GapResolver7ResolveEPNS1_12ParallelMoveE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler11GapResolver7ResolveEPNS1_12ParallelMoveE, @function
_GLOBAL__sub_I__ZN2v88internal8compiler11GapResolver7ResolveEPNS1_12ParallelMoveE:
.LFB12980:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE12980:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler11GapResolver7ResolveEPNS1_12ParallelMoveE, .-_GLOBAL__sub_I__ZN2v88internal8compiler11GapResolver7ResolveEPNS1_12ParallelMoveE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler11GapResolver7ResolveEPNS1_12ParallelMoveE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
