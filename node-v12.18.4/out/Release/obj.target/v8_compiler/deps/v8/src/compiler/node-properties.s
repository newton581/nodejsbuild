	.file	"node-properties.cc"
	.text
	.section	.text._ZNK2v88internal8compiler8Operator6EqualsEPKS2_,"axG",@progbits,_ZNK2v88internal8compiler8Operator6EqualsEPKS2_,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal8compiler8Operator6EqualsEPKS2_
	.type	_ZNK2v88internal8compiler8Operator6EqualsEPKS2_, @function
_ZNK2v88internal8compiler8Operator6EqualsEPKS2_:
.LFB4473:
	.cfi_startproc
	endbr64
	movzwl	16(%rsi), %eax
	cmpw	%ax, 16(%rdi)
	sete	%al
	ret
	.cfi_endproc
.LFE4473:
	.size	_ZNK2v88internal8compiler8Operator6EqualsEPKS2_, .-_ZNK2v88internal8compiler8Operator6EqualsEPKS2_
	.section	.text._ZNK2v88internal8compiler8Operator8HashCodeEv,"axG",@progbits,_ZNK2v88internal8compiler8Operator8HashCodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal8compiler8Operator8HashCodeEv
	.type	_ZNK2v88internal8compiler8Operator8HashCodeEv, @function
_ZNK2v88internal8compiler8Operator8HashCodeEv:
.LFB4474:
	.cfi_startproc
	endbr64
	movzwl	16(%rdi), %eax
	ret
	.cfi_endproc
.LFE4474:
	.size	_ZNK2v88internal8compiler8Operator8HashCodeEv, .-_ZNK2v88internal8compiler8Operator8HashCodeEv
	.section	.text._ZN2v88internal8compiler14NodeProperties14PastValueIndexEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14NodeProperties14PastValueIndexEPNS1_4NodeE
	.type	_ZN2v88internal8compiler14NodeProperties14PastValueIndexEPNS1_4NodeE, @function
_ZN2v88internal8compiler14NodeProperties14PastValueIndexEPNS1_4NodeE:
.LFB22693:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movl	20(%rax), %eax
	ret
	.cfi_endproc
.LFE22693:
	.size	_ZN2v88internal8compiler14NodeProperties14PastValueIndexEPNS1_4NodeE, .-_ZN2v88internal8compiler14NodeProperties14PastValueIndexEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler14NodeProperties16PastContextIndexEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14NodeProperties16PastContextIndexEPNS1_4NodeE
	.type	_ZN2v88internal8compiler14NodeProperties16PastContextIndexEPNS1_4NodeE, @function
_ZN2v88internal8compiler14NodeProperties16PastContextIndexEPNS1_4NodeE:
.LFB22694:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rdi
	movl	20(%rdi), %ebx
	call	_ZN2v88internal8compiler18OperatorProperties15HasContextInputEPKNS1_8OperatorE@PLT
	addq	$8, %rsp
	movzbl	%al, %eax
	addl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22694:
	.size	_ZN2v88internal8compiler14NodeProperties16PastContextIndexEPNS1_4NodeE, .-_ZN2v88internal8compiler14NodeProperties16PastContextIndexEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler14NodeProperties19PastFrameStateIndexEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14NodeProperties19PastFrameStateIndexEPNS1_4NodeE
	.type	_ZN2v88internal8compiler14NodeProperties19PastFrameStateIndexEPNS1_4NodeE, @function
_ZN2v88internal8compiler14NodeProperties19PastFrameStateIndexEPNS1_4NodeE:
.LFB22695:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	(%rdi), %rdi
	movl	20(%rdi), %ebx
	call	_ZN2v88internal8compiler18OperatorProperties15HasContextInputEPKNS1_8OperatorE@PLT
	movq	(%r12), %rdi
	movzbl	%al, %eax
	addl	%eax, %ebx
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	movzbl	%al, %eax
	addl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22695:
	.size	_ZN2v88internal8compiler14NodeProperties19PastFrameStateIndexEPNS1_4NodeE, .-_ZN2v88internal8compiler14NodeProperties19PastFrameStateIndexEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler14NodeProperties15PastEffectIndexEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14NodeProperties15PastEffectIndexEPNS1_4NodeE
	.type	_ZN2v88internal8compiler14NodeProperties15PastEffectIndexEPNS1_4NodeE, @function
_ZN2v88internal8compiler14NodeProperties15PastEffectIndexEPNS1_4NodeE:
.LFB22696:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	(%rdi), %rdi
	movl	20(%rdi), %ebx
	call	_ZN2v88internal8compiler18OperatorProperties15HasContextInputEPKNS1_8OperatorE@PLT
	movq	(%r12), %rdi
	movzbl	%al, %eax
	addl	%eax, %ebx
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	movq	(%r12), %rdx
	movzbl	%al, %eax
	addl	%ebx, %eax
	popq	%rbx
	addl	24(%rdx), %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22696:
	.size	_ZN2v88internal8compiler14NodeProperties15PastEffectIndexEPNS1_4NodeE, .-_ZN2v88internal8compiler14NodeProperties15PastEffectIndexEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler14NodeProperties16PastControlIndexEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14NodeProperties16PastControlIndexEPNS1_4NodeE
	.type	_ZN2v88internal8compiler14NodeProperties16PastControlIndexEPNS1_4NodeE, @function
_ZN2v88internal8compiler14NodeProperties16PastControlIndexEPNS1_4NodeE:
.LFB22697:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	(%rdi), %rdi
	movl	20(%rdi), %ebx
	call	_ZN2v88internal8compiler18OperatorProperties15HasContextInputEPKNS1_8OperatorE@PLT
	movq	(%r12), %rdi
	movzbl	%al, %eax
	addl	%eax, %ebx
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	movq	(%r12), %rdx
	movzbl	%al, %eax
	addl	%ebx, %eax
	popq	%rbx
	addl	24(%rdx), %eax
	popq	%r12
	addl	28(%rdx), %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22697:
	.size	_ZN2v88internal8compiler14NodeProperties16PastControlIndexEPNS1_4NodeE, .-_ZN2v88internal8compiler14NodeProperties16PastControlIndexEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi
	.type	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi, @function
_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi:
.LFB22698:
	.cfi_startproc
	endbr64
	movzbl	23(%rdi), %eax
	movslq	%esi, %rsi
	leaq	32(%rdi), %rdx
	salq	$3, %rsi
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L14
	addq	%rdx, %rsi
	movq	(%rsi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	movq	32(%rdi), %rax
	leaq	16(%rax,%rsi), %rsi
	movq	(%rsi), %rax
	ret
	.cfi_endproc
.LFE22698:
	.size	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi, .-_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi
	.section	.text._ZN2v88internal8compiler14NodeProperties15GetContextInputEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14NodeProperties15GetContextInputEPNS1_4NodeE
	.type	_ZN2v88internal8compiler14NodeProperties15GetContextInputEPNS1_4NodeE, @function
_ZN2v88internal8compiler14NodeProperties15GetContextInputEPNS1_4NodeE:
.LFB22699:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movzbl	23(%rdi), %edx
	leaq	32(%rdi), %rcx
	movslq	20(%rax), %rax
	andl	$15, %edx
	salq	$3, %rax
	cmpl	$15, %edx
	je	.L17
	addq	%rcx, %rax
	movq	(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	movq	32(%rdi), %rdx
	leaq	16(%rdx,%rax), %rax
	movq	(%rax), %rax
	ret
	.cfi_endproc
.LFE22699:
	.size	_ZN2v88internal8compiler14NodeProperties15GetContextInputEPNS1_4NodeE, .-_ZN2v88internal8compiler14NodeProperties15GetContextInputEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler14NodeProperties18GetFrameStateInputEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14NodeProperties18GetFrameStateInputEPNS1_4NodeE
	.type	_ZN2v88internal8compiler14NodeProperties18GetFrameStateInputEPNS1_4NodeE, @function
_ZN2v88internal8compiler14NodeProperties18GetFrameStateInputEPNS1_4NodeE:
.LFB22700:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	movl	20(%rdi), %r12d
	call	_ZN2v88internal8compiler18OperatorProperties15HasContextInputEPKNS1_8OperatorE@PLT
	movzbl	23(%rbx), %edx
	movzbl	%al, %eax
	addl	%r12d, %eax
	andl	$15, %edx
	cltq
	salq	$3, %rax
	cmpl	$15, %edx
	je	.L20
	leaq	32(%rbx), %rcx
	popq	%rbx
	popq	%r12
	addq	%rcx, %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	movq	32(%rbx), %rdx
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	16(%rdx,%rax), %rax
	movq	(%rax), %rax
	ret
	.cfi_endproc
.LFE22700:
	.size	_ZN2v88internal8compiler14NodeProperties18GetFrameStateInputEPNS1_4NodeE, .-_ZN2v88internal8compiler14NodeProperties18GetFrameStateInputEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi
	.type	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi, @function
_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi:
.LFB22701:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	movl	20(%rdi), %r13d
	call	_ZN2v88internal8compiler18OperatorProperties15HasContextInputEPKNS1_8OperatorE@PLT
	movq	(%rbx), %rdi
	movzbl	%al, %eax
	addl	%eax, %r13d
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	movzbl	23(%rbx), %edx
	movzbl	%al, %eax
	addl	%r13d, %eax
	andl	$15, %edx
	addl	%r12d, %eax
	cltq
	salq	$3, %rax
	cmpl	$15, %edx
	je	.L24
	leaq	32(%rbx), %rcx
	addq	%rcx, %rax
	movq	(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_restore_state
	movq	32(%rbx), %rdx
	leaq	16(%rdx,%rax), %rax
	movq	(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22701:
	.size	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi, .-_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi
	.section	.text._ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi
	.type	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi, @function
_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi:
.LFB22702:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%esi, %ebx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	movl	20(%rdi), %r13d
	call	_ZN2v88internal8compiler18OperatorProperties15HasContextInputEPKNS1_8OperatorE@PLT
	movq	(%r12), %rdi
	movzbl	%al, %eax
	addl	%eax, %r13d
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	movq	(%r12), %rdx
	movzbl	%al, %eax
	addl	%r13d, %eax
	addl	24(%rdx), %eax
	movzbl	23(%r12), %edx
	addl	%ebx, %eax
	cltq
	andl	$15, %edx
	salq	$3, %rax
	cmpl	$15, %edx
	je	.L28
	leaq	32(%r12), %rcx
	addq	%rcx, %rax
	movq	(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	.cfi_restore_state
	movq	32(%r12), %rdx
	leaq	16(%rdx,%rax), %rax
	movq	(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22702:
	.size	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi, .-_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi
	.section	.text._ZN2v88internal8compiler14NodeProperties11IsValueEdgeENS1_4EdgeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14NodeProperties11IsValueEdgeENS1_4EdgeE
	.type	_ZN2v88internal8compiler14NodeProperties11IsValueEdgeENS1_4EdgeE, @function
_ZN2v88internal8compiler14NodeProperties11IsValueEdgeENS1_4EdgeE:
.LFB22703:
	.cfi_startproc
	endbr64
	movl	16(%rdi), %edx
	movl	%edx, %ecx
	shrl	%ecx
	andl	$1, %edx
	movl	%ecx, %eax
	leaq	3(%rax,%rax,2), %rax
	movq	(%rdi,%rax,8), %rax
	jne	.L32
	movq	(%rax), %rax
.L32:
	movl	20(%rax), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jne	.L37
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	cmpl	%edx, %ecx
	setl	%al
	ret
	.cfi_endproc
.LFE22703:
	.size	_ZN2v88internal8compiler14NodeProperties11IsValueEdgeENS1_4EdgeE, .-_ZN2v88internal8compiler14NodeProperties11IsValueEdgeENS1_4EdgeE
	.section	.text._ZN2v88internal8compiler14NodeProperties13IsContextEdgeENS1_4EdgeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14NodeProperties13IsContextEdgeENS1_4EdgeE
	.type	_ZN2v88internal8compiler14NodeProperties13IsContextEdgeENS1_4EdgeE, @function
_ZN2v88internal8compiler14NodeProperties13IsContextEdgeENS1_4EdgeE:
.LFB22704:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movl	16(%rdi), %edx
	movq	%rdi, %rbx
	movl	%edx, %eax
	shrl	%eax
	andl	$1, %edx
	leaq	3(%rax,%rax,2), %rax
	leaq	(%rdi,%rax,8), %r12
	movq	(%r12), %rdi
	jne	.L39
	movq	%rdi, %r12
	movq	(%rdi), %rdi
.L39:
	call	_ZN2v88internal8compiler18OperatorProperties15HasContextInputEPKNS1_8OperatorE@PLT
	testb	%al, %al
	je	.L38
	movq	(%r12), %rdx
	movl	16(%rbx), %eax
	shrl	%eax
	cmpl	%eax, 20(%rdx)
	sete	%al
.L38:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22704:
	.size	_ZN2v88internal8compiler14NodeProperties13IsContextEdgeENS1_4EdgeE, .-_ZN2v88internal8compiler14NodeProperties13IsContextEdgeENS1_4EdgeE
	.section	.text._ZN2v88internal8compiler14NodeProperties16IsFrameStateEdgeENS1_4EdgeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14NodeProperties16IsFrameStateEdgeENS1_4EdgeE
	.type	_ZN2v88internal8compiler14NodeProperties16IsFrameStateEdgeENS1_4EdgeE, @function
_ZN2v88internal8compiler14NodeProperties16IsFrameStateEdgeENS1_4EdgeE:
.LFB22705:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	16(%rdi), %edx
	movl	%edx, %eax
	shrl	%eax
	andl	$1, %edx
	leaq	3(%rax,%rax,2), %rax
	leaq	(%rdi,%rax,8), %r13
	movq	0(%r13), %rdi
	jne	.L46
	movq	%rdi, %r13
	movq	(%rdi), %rdi
.L46:
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	movq	0(%r13), %rdi
	movl	%eax, %r12d
	testb	%al, %al
	je	.L52
	movl	20(%rdi), %r12d
	call	_ZN2v88internal8compiler18OperatorProperties15HasContextInputEPKNS1_8OperatorE@PLT
	cmpb	$1, %al
	movl	16(%rbx), %eax
	sbbl	$-1, %r12d
	shrl	%eax
	cmpl	%r12d, %eax
	sete	%r12b
	addq	$8, %rsp
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	.cfi_restore_state
	call	_ZN2v88internal8compiler18OperatorProperties15HasContextInputEPKNS1_8OperatorE@PLT
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22705:
	.size	_ZN2v88internal8compiler14NodeProperties16IsFrameStateEdgeENS1_4EdgeE, .-_ZN2v88internal8compiler14NodeProperties16IsFrameStateEdgeENS1_4EdgeE
	.section	.text._ZN2v88internal8compiler14NodeProperties12IsEffectEdgeENS1_4EdgeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14NodeProperties12IsEffectEdgeENS1_4EdgeE
	.type	_ZN2v88internal8compiler14NodeProperties12IsEffectEdgeENS1_4EdgeE, @function
_ZN2v88internal8compiler14NodeProperties12IsEffectEdgeENS1_4EdgeE:
.LFB22706:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	16(%rdi), %edx
	movl	%edx, %eax
	shrl	%eax
	andl	$1, %edx
	leaq	3(%rax,%rax,2), %rax
	leaq	(%rdi,%rax,8), %r14
	movq	(%r14), %rdi
	jne	.L55
	movq	%rdi, %r14
	movq	(%rdi), %rdi
.L55:
	movl	24(%rdi), %r15d
	movl	20(%rdi), %r12d
	call	_ZN2v88internal8compiler18OperatorProperties15HasContextInputEPKNS1_8OperatorE@PLT
	movq	(%r14), %rdi
	movzbl	%al, %r13d
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	xorl	%r8d, %r8d
	testl	%r15d, %r15d
	je	.L54
	movl	16(%rbx), %edx
	addl	%r13d, %r12d
	movzbl	%al, %eax
	addl	%r12d, %eax
	shrl	%edx
	cmpl	%edx, %eax
	jg	.L54
	addl	%r15d, %eax
	cmpl	%eax, %edx
	setl	%r8b
.L54:
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22706:
	.size	_ZN2v88internal8compiler14NodeProperties12IsEffectEdgeENS1_4EdgeE, .-_ZN2v88internal8compiler14NodeProperties12IsEffectEdgeENS1_4EdgeE
	.section	.text._ZN2v88internal8compiler14NodeProperties13IsControlEdgeENS1_4EdgeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14NodeProperties13IsControlEdgeENS1_4EdgeE
	.type	_ZN2v88internal8compiler14NodeProperties13IsControlEdgeENS1_4EdgeE, @function
_ZN2v88internal8compiler14NodeProperties13IsControlEdgeENS1_4EdgeE:
.LFB22707:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movl	16(%rdi), %edx
	movl	%edx, %eax
	shrl	%eax
	andl	$1, %edx
	leaq	3(%rax,%rax,2), %rax
	leaq	(%rdi,%rax,8), %r15
	movq	(%r15), %rdi
	jne	.L62
	movq	%rdi, %r15
	movq	(%rdi), %rdi
.L62:
	movl	28(%rdi), %r14d
	movl	20(%rdi), %r13d
	call	_ZN2v88internal8compiler18OperatorProperties15HasContextInputEPKNS1_8OperatorE@PLT
	movq	(%r15), %rdi
	movzbl	%al, %ebx
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	xorl	%r8d, %r8d
	testl	%r14d, %r14d
	je	.L61
	movzbl	%al, %eax
	addl	%r13d, %ebx
	movq	(%r15), %rdx
	addl	%eax, %ebx
	movl	16(%r12), %eax
	addl	24(%rdx), %ebx
	shrl	%eax
	cmpl	%eax, %ebx
	jg	.L61
	addl	%r14d, %ebx
	cmpl	%ebx, %eax
	setl	%r8b
.L61:
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22707:
	.size	_ZN2v88internal8compiler14NodeProperties13IsControlEdgeENS1_4EdgeE, .-_ZN2v88internal8compiler14NodeProperties13IsControlEdgeENS1_4EdgeE
	.section	.text._ZN2v88internal8compiler14NodeProperties17IsExceptionalCallEPNS1_4NodeEPS4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14NodeProperties17IsExceptionalCallEPNS1_4NodeEPS4_
	.type	_ZN2v88internal8compiler14NodeProperties17IsExceptionalCallEPNS1_4NodeEPS4_, @function
_ZN2v88internal8compiler14NodeProperties17IsExceptionalCallEPNS1_4NodeEPS4_:
.LFB22708:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rdx
	movq	%rsi, -64(%rbp)
	testb	$32, 18(%rdx)
	jne	.L68
	movq	24(%rdi), %r14
	testq	%r14, %r14
	je	.L70
	movq	(%r14), %r15
	.p2align 4,,10
	.p2align 3
.L77:
	movl	16(%r14), %edx
	movl	%edx, %eax
	shrl	%eax
	andl	$1, %edx
	leaq	3(%rax,%rax,2), %rax
	leaq	(%r14,%rax,8), %r13
	movq	0(%r13), %rdi
	jne	.L71
	movq	%rdi, %r13
	movq	(%rdi), %rdi
.L71:
	movl	20(%rdi), %eax
	movl	28(%rdi), %r12d
	movl	%eax, -52(%rbp)
	call	_ZN2v88internal8compiler18OperatorProperties15HasContextInputEPKNS1_8OperatorE@PLT
	movq	0(%r13), %rdi
	movzbl	%al, %ebx
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	testl	%r12d, %r12d
	je	.L73
	movq	0(%r13), %rcx
	movzbl	%al, %eax
	addl	-52(%rbp), %ebx
	addl	%eax, %ebx
	addl	24(%rcx), %ebx
	movl	16(%r14), %ecx
	movl	%ecx, %eax
	shrl	%eax
	cmpl	%eax, %ebx
	jg	.L73
	addl	%r12d, %ebx
	cmpl	%ebx, %eax
	jge	.L73
	leaq	3(%rax,%rax,2), %rax
	andl	$1, %ecx
	leaq	(%r14,%rax,8), %rdx
	movq	(%rdx), %rax
	jne	.L75
	movq	%rax, %rdx
	movq	(%rax), %rax
.L75:
	cmpw	$7, 16(%rax)
	je	.L89
.L73:
	testq	%r15, %r15
	je	.L70
	movq	%r15, %r14
	movq	(%r15), %r15
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L70:
	xorl	%eax, %eax
.L68:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L89:
	.cfi_restore_state
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L76
	movq	%rdx, (%rax)
.L76:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22708:
	.size	_ZN2v88internal8compiler14NodeProperties17IsExceptionalCallEPNS1_4NodeEPS4_, .-_ZN2v88internal8compiler14NodeProperties17IsExceptionalCallEPNS1_4NodeEPS4_
	.section	.text._ZN2v88internal8compiler14NodeProperties31FindSuccessfulControlProjectionEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14NodeProperties31FindSuccessfulControlProjectionEPNS1_4NodeE
	.type	_ZN2v88internal8compiler14NodeProperties31FindSuccessfulControlProjectionEPNS1_4NodeE, @function
_ZN2v88internal8compiler14NodeProperties31FindSuccessfulControlProjectionEPNS1_4NodeE:
.LFB22709:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rax
	movq	%rdi, -64(%rbp)
	testb	$32, 18(%rax)
	jne	.L91
	movq	24(%rdi), %r14
	testq	%r14, %r14
	je	.L91
	movq	(%r14), %r15
	.p2align 4,,10
	.p2align 3
.L98:
	movl	16(%r14), %edx
	movl	%edx, %eax
	shrl	%eax
	andl	$1, %edx
	leaq	3(%rax,%rax,2), %rax
	leaq	(%r14,%rax,8), %r13
	movq	0(%r13), %rdi
	jne	.L93
	movq	%rdi, %r13
	movq	(%rdi), %rdi
.L93:
	movl	20(%rdi), %eax
	movl	28(%rdi), %r12d
	movl	%eax, -52(%rbp)
	call	_ZN2v88internal8compiler18OperatorProperties15HasContextInputEPKNS1_8OperatorE@PLT
	movq	0(%r13), %rdi
	movzbl	%al, %ebx
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	testl	%r12d, %r12d
	je	.L95
	movq	0(%r13), %rcx
	movzbl	%al, %eax
	addl	-52(%rbp), %ebx
	addl	%eax, %ebx
	addl	24(%rcx), %ebx
	movl	16(%r14), %ecx
	movl	%ecx, %eax
	shrl	%eax
	cmpl	%eax, %ebx
	jg	.L95
	addl	%r12d, %ebx
	cmpl	%ebx, %eax
	jge	.L95
	leaq	3(%rax,%rax,2), %rax
	andl	$1, %ecx
	leaq	(%r14,%rax,8), %rdx
	movq	(%rdx), %rax
	jne	.L97
	movq	%rax, %rdx
	movq	(%rax), %rax
.L97:
	cmpw	$6, 16(%rax)
	je	.L106
.L95:
	testq	%r15, %r15
	je	.L91
	movq	%r15, %r14
	movq	(%r15), %r15
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L106:
	movq	%rdx, -64(%rbp)
.L91:
	movq	-64(%rbp), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22709:
	.size	_ZN2v88internal8compiler14NodeProperties31FindSuccessfulControlProjectionEPNS1_4NodeE, .-_ZN2v88internal8compiler14NodeProperties31FindSuccessfulControlProjectionEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler14NodeProperties17ReplaceValueInputEPNS1_4NodeES4_i,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14NodeProperties17ReplaceValueInputEPNS1_4NodeES4_i
	.type	_ZN2v88internal8compiler14NodeProperties17ReplaceValueInputEPNS1_4NodeES4_i, @function
_ZN2v88internal8compiler14NodeProperties17ReplaceValueInputEPNS1_4NodeES4_i:
.LFB22710:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edx, %rax
	salq	$3, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movzbl	23(%rdi), %ecx
	andl	$15, %ecx
	cmpl	$15, %ecx
	je	.L108
	leaq	32(%rdi), %rbx
	addq	%rax, %rbx
	movq	(%rbx), %r8
	cmpq	%r8, %rsi
	je	.L107
.L109:
	notl	%edx
	movslq	%edx, %rdx
	leaq	(%rdx,%rdx,2), %rax
	leaq	(%rdi,%rax,8), %r13
	testq	%r8, %r8
	je	.L112
	movq	%r13, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L112:
	movq	%r12, (%rbx)
	testq	%r12, %r12
	je	.L107
	addq	$8, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	.p2align 4,,10
	.p2align 3
.L108:
	.cfi_restore_state
	movq	32(%rdi), %rdi
	leaq	16(%rdi,%rax), %rbx
	movq	(%rbx), %r8
	cmpq	%r8, %rsi
	jne	.L109
.L107:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22710:
	.size	_ZN2v88internal8compiler14NodeProperties17ReplaceValueInputEPNS1_4NodeES4_i, .-_ZN2v88internal8compiler14NodeProperties17ReplaceValueInputEPNS1_4NodeES4_i
	.section	.text._ZN2v88internal8compiler14NodeProperties18ReplaceValueInputsEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14NodeProperties18ReplaceValueInputsEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler14NodeProperties18ReplaceValueInputsEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler14NodeProperties18ReplaceValueInputsEPNS1_4NodeES4_:
.LFB22711:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	32(%rdi), %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rax
	movq	32(%rdi), %rdi
	movl	20(%rax), %ebx
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L121
	movq	%r12, %rsi
	cmpq	%rdi, %r13
	je	.L123
	leaq	-24(%rsi), %r14
	testq	%rdi, %rdi
	je	.L125
.L138:
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L125:
	movq	%r13, (%r15)
	testq	%r13, %r13
	je	.L123
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L123:
	subl	$1, %ebx
	testl	%ebx, %ebx
	jle	.L120
	.p2align 4,,10
	.p2align 3
.L126:
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node11RemoveInputEi@PLT
	subl	$1, %ebx
	jne	.L126
.L120:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L121:
	.cfi_restore_state
	movq	16(%rdi), %rax
	leaq	16(%rdi), %r15
	cmpq	%rax, %rsi
	je	.L123
	movq	%rdi, %rsi
	movq	%rax, %rdi
	leaq	-24(%rsi), %r14
	testq	%rdi, %rdi
	jne	.L138
	jmp	.L125
	.cfi_endproc
.LFE22711:
	.size	_ZN2v88internal8compiler14NodeProperties18ReplaceValueInputsEPNS1_4NodeES4_, .-_ZN2v88internal8compiler14NodeProperties18ReplaceValueInputsEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler14NodeProperties19ReplaceContextInputEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14NodeProperties19ReplaceContextInputEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler14NodeProperties19ReplaceContextInputEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler14NodeProperties19ReplaceContextInputEPNS1_4NodeES4_:
.LFB22712:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	(%rdi), %rax
	movzbl	23(%rdi), %ecx
	movslq	20(%rax), %rdx
	andl	$15, %ecx
	movq	%rdx, %rax
	salq	$3, %rdx
	cmpl	$15, %ecx
	je	.L140
	leaq	32(%rdi), %rbx
	addq	%rdx, %rbx
	movq	(%rbx), %r8
	cmpq	%r8, %rsi
	je	.L139
.L141:
	notl	%eax
	cltq
	leaq	(%rax,%rax,2), %rax
	leaq	(%rdi,%rax,8), %r13
	testq	%r8, %r8
	je	.L144
	movq	%r13, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L144:
	movq	%r12, (%rbx)
	testq	%r12, %r12
	je	.L139
	addq	$8, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	.p2align 4,,10
	.p2align 3
.L140:
	.cfi_restore_state
	movq	32(%rdi), %rdi
	leaq	16(%rdi,%rdx), %rbx
	movq	(%rbx), %r8
	cmpq	%r8, %rsi
	jne	.L141
.L139:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22712:
	.size	_ZN2v88internal8compiler14NodeProperties19ReplaceContextInputEPNS1_4NodeES4_, .-_ZN2v88internal8compiler14NodeProperties19ReplaceContextInputEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler14NodeProperties19ReplaceControlInputEPNS1_4NodeES4_i,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14NodeProperties19ReplaceControlInputEPNS1_4NodeES4_i
	.type	_ZN2v88internal8compiler14NodeProperties19ReplaceControlInputEPNS1_4NodeES4_i, @function
_ZN2v88internal8compiler14NodeProperties19ReplaceControlInputEPNS1_4NodeES4_i:
.LFB22713:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rdi), %rdi
	movl	%edx, %ebx
	movl	20(%rdi), %r15d
	call	_ZN2v88internal8compiler18OperatorProperties15HasContextInputEPKNS1_8OperatorE@PLT
	movq	(%r12), %rdi
	movzbl	%al, %edx
	addl	%edx, %r15d
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	movq	(%r12), %rcx
	movzbl	%al, %edx
	addl	%r15d, %edx
	addl	24(%rcx), %edx
	movzbl	23(%r12), %ecx
	addl	%ebx, %edx
	movslq	%edx, %rax
	andl	$15, %ecx
	salq	$3, %rax
	cmpl	$15, %ecx
	je	.L153
	leaq	32(%r12), %rbx
	addq	%rax, %rbx
	movq	(%rbx), %rdi
	cmpq	%rdi, %r14
	je	.L152
.L154:
	notl	%edx
	movslq	%edx, %rdx
	leaq	(%rdx,%rdx,2), %rax
	leaq	(%r12,%rax,8), %r12
	testq	%rdi, %rdi
	je	.L157
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L157:
	movq	%r14, (%rbx)
	testq	%r14, %r14
	je	.L152
	popq	%rbx
	movq	%r12, %rsi
	movq	%r14, %rdi
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	.p2align 4,,10
	.p2align 3
.L153:
	.cfi_restore_state
	movq	32(%r12), %r12
	leaq	16(%r12,%rax), %rbx
	movq	(%rbx), %rdi
	cmpq	%rdi, %r14
	jne	.L154
.L152:
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22713:
	.size	_ZN2v88internal8compiler14NodeProperties19ReplaceControlInputEPNS1_4NodeES4_i, .-_ZN2v88internal8compiler14NodeProperties19ReplaceControlInputEPNS1_4NodeES4_i
	.section	.text._ZN2v88internal8compiler14NodeProperties18ReplaceEffectInputEPNS1_4NodeES4_i,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14NodeProperties18ReplaceEffectInputEPNS1_4NodeES4_i
	.type	_ZN2v88internal8compiler14NodeProperties18ReplaceEffectInputEPNS1_4NodeES4_i, @function
_ZN2v88internal8compiler14NodeProperties18ReplaceEffectInputEPNS1_4NodeES4_i:
.LFB22714:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	movl	20(%rdi), %r15d
	call	_ZN2v88internal8compiler18OperatorProperties15HasContextInputEPKNS1_8OperatorE@PLT
	movq	(%rbx), %rdi
	movzbl	%al, %edx
	addl	%edx, %r15d
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	movzbl	23(%rbx), %ecx
	movzbl	%al, %edx
	addl	%r15d, %edx
	andl	$15, %ecx
	addl	%r12d, %edx
	movslq	%edx, %rax
	salq	$3, %rax
	cmpl	$15, %ecx
	je	.L166
	leaq	32(%rbx), %r12
	addq	%rax, %r12
	movq	(%r12), %rdi
	cmpq	%rdi, %r14
	je	.L165
.L167:
	notl	%edx
	movslq	%edx, %rdx
	leaq	(%rdx,%rdx,2), %rax
	leaq	(%rbx,%rax,8), %r13
	testq	%rdi, %rdi
	je	.L170
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L170:
	movq	%r14, (%r12)
	testq	%r14, %r14
	je	.L165
	addq	$8, %rsp
	movq	%r13, %rsi
	movq	%r14, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	.p2align 4,,10
	.p2align 3
.L166:
	.cfi_restore_state
	movq	32(%rbx), %rbx
	leaq	16(%rbx,%rax), %r12
	movq	(%r12), %rdi
	cmpq	%rdi, %r14
	jne	.L167
.L165:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22714:
	.size	_ZN2v88internal8compiler14NodeProperties18ReplaceEffectInputEPNS1_4NodeES4_i, .-_ZN2v88internal8compiler14NodeProperties18ReplaceEffectInputEPNS1_4NodeES4_i
	.section	.text._ZN2v88internal8compiler14NodeProperties22ReplaceFrameStateInputEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14NodeProperties22ReplaceFrameStateInputEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler14NodeProperties22ReplaceFrameStateInputEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler14NodeProperties22ReplaceFrameStateInputEPNS1_4NodeES4_:
.LFB22715:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	movl	20(%rdi), %r13d
	call	_ZN2v88internal8compiler18OperatorProperties15HasContextInputEPKNS1_8OperatorE@PLT
	movzbl	23(%rbx), %ecx
	movzbl	%al, %eax
	addl	%r13d, %eax
	andl	$15, %ecx
	movslq	%eax, %rdx
	salq	$3, %rdx
	cmpl	$15, %ecx
	je	.L179
	leaq	32(%rbx), %r13
	addq	%rdx, %r13
	movq	0(%r13), %rdi
	cmpq	%rdi, %r12
	je	.L178
.L180:
	notl	%eax
	cltq
	leaq	(%rax,%rax,2), %rax
	leaq	(%rbx,%rax,8), %r14
	testq	%rdi, %rdi
	je	.L183
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L183:
	movq	%r12, 0(%r13)
	testq	%r12, %r12
	je	.L178
	popq	%rbx
	movq	%r14, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	.p2align 4,,10
	.p2align 3
.L179:
	.cfi_restore_state
	movq	32(%rbx), %rbx
	leaq	16(%rbx,%rdx), %r13
	movq	0(%r13), %rdi
	cmpq	%rdi, %r12
	jne	.L180
.L178:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22715:
	.size	_ZN2v88internal8compiler14NodeProperties22ReplaceFrameStateInputEPNS1_4NodeES4_, .-_ZN2v88internal8compiler14NodeProperties22ReplaceFrameStateInputEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler14NodeProperties20RemoveNonValueInputsEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14NodeProperties20RemoveNonValueInputsEPNS1_4NodeE
	.type	_ZN2v88internal8compiler14NodeProperties20RemoveNonValueInputsEPNS1_4NodeE, @function
_ZN2v88internal8compiler14NodeProperties20RemoveNonValueInputsEPNS1_4NodeE:
.LFB22716:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movl	20(%rax), %esi
	jmp	_ZN2v88internal8compiler4Node14TrimInputCountEi@PLT
	.cfi_endproc
.LFE22716:
	.size	_ZN2v88internal8compiler14NodeProperties20RemoveNonValueInputsEPNS1_4NodeE, .-_ZN2v88internal8compiler14NodeProperties20RemoveNonValueInputsEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler14NodeProperties17RemoveValueInputsEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14NodeProperties17RemoveValueInputsEPNS1_4NodeE
	.type	_ZN2v88internal8compiler14NodeProperties17RemoveValueInputsEPNS1_4NodeE, @function
_ZN2v88internal8compiler14NodeProperties17RemoveValueInputsEPNS1_4NodeE:
.LFB22717:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	movl	20(%rax), %ebx
	subl	$1, %ebx
	js	.L192
	movq	%rdi, %r12
	.p2align 4,,10
	.p2align 3
.L194:
	movl	%ebx, %esi
	movq	%r12, %rdi
	subl	$1, %ebx
	call	_ZN2v88internal8compiler4Node11RemoveInputEi@PLT
	cmpl	$-1, %ebx
	jne	.L194
.L192:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22717:
	.size	_ZN2v88internal8compiler14NodeProperties17RemoveValueInputsEPNS1_4NodeE, .-_ZN2v88internal8compiler14NodeProperties17RemoveValueInputsEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler14NodeProperties17MergeControlToEndEPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14NodeProperties17MergeControlToEndEPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS1_4NodeE
	.type	_ZN2v88internal8compiler14NodeProperties17MergeControlToEndEPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS1_4NodeE, @function
_ZN2v88internal8compiler14NodeProperties17MergeControlToEndEPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS1_4NodeE:
.LFB22718:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	16(%rdi), %rdi
	movq	(%rbx), %rsi
	call	_ZN2v88internal8compiler4Node11AppendInputEPNS0_4ZoneEPS2_@PLT
	movq	16(%rbx), %rbx
	movzbl	23(%rbx), %esi
	andl	$15, %esi
	cmpl	$15, %esi
	jne	.L199
	movq	32(%rbx), %rax
	movl	8(%rax), %esi
.L199:
	movq	%r12, %rdi
	movslq	%esi, %rsi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3EndEm@PLT
	movq	%rax, (%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22718:
	.size	_ZN2v88internal8compiler14NodeProperties17MergeControlToEndEPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS1_4NodeE, .-_ZN2v88internal8compiler14NodeProperties17MergeControlToEndEPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler14NodeProperties11ReplaceUsesEPNS1_4NodeES4_S4_S4_S4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14NodeProperties11ReplaceUsesEPNS1_4NodeES4_S4_S4_S4_
	.type	_ZN2v88internal8compiler14NodeProperties11ReplaceUsesEPNS1_4NodeES4_S4_S4_S4_, @function
_ZN2v88internal8compiler14NodeProperties11ReplaceUsesEPNS1_4NodeES4_S4_S4_S4_:
.LFB22719:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	24(%rdi), %r15
	movq	%rsi, -80(%rbp)
	movq	%rdx, -88(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%r8, -72(%rbp)
	testq	%r15, %r15
	je	.L201
	movq	(%r15), %r13
	.p2align 4,,10
	.p2align 3
.L225:
	movl	16(%r15), %edx
	movl	%edx, %eax
	shrl	%eax
	leaq	3(%rax,%rax,2), %rcx
	salq	$3, %rax
	andl	$1, %edx
	leaq	(%r15,%rcx,8), %rbx
	movq	(%rbx), %r8
	je	.L203
	leaq	32(%rbx,%rax), %r14
	movq	%r8, %rdi
.L204:
	movl	28(%rdi), %edx
	movl	20(%rdi), %eax
	movl	%edx, -52(%rbp)
	movl	%eax, -56(%rbp)
	call	_ZN2v88internal8compiler18OperatorProperties15HasContextInputEPKNS1_8OperatorE@PLT
	movq	(%rbx), %rdi
	movzbl	%al, %r12d
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	movl	-52(%rbp), %edx
	testl	%edx, %edx
	je	.L270
	movq	(%rbx), %rcx
	movl	-56(%rbp), %ebx
	movzbl	%al, %eax
	addl	%r12d, %ebx
	addl	%ebx, %eax
	addl	24(%rcx), %eax
	movl	%eax, %ebx
	movl	16(%r15), %eax
	movl	%eax, %r8d
	andl	$1, %eax
	shrl	%r8d
	movl	%r8d, %ecx
	leaq	3(%rcx,%rcx,2), %rcx
	leaq	(%r15,%rcx,8), %r12
	movq	(%r12), %rdi
	cmpl	%r8d, %ebx
	jg	.L206
	addl	%edx, %ebx
	cmpl	%ebx, %r8d
	jl	.L271
	.p2align 4,,10
	.p2align 3
.L206:
	testb	%al, %al
	jne	.L218
	movq	%rdi, %r12
	movq	(%rdi), %rdi
.L218:
	movl	24(%rdi), %edx
	movl	20(%rdi), %eax
	movl	%edx, -52(%rbp)
	movl	%eax, -56(%rbp)
	call	_ZN2v88internal8compiler18OperatorProperties15HasContextInputEPKNS1_8OperatorE@PLT
	movq	(%r12), %rdi
	movzbl	%al, %ebx
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	movl	-52(%rbp), %edx
	testl	%edx, %edx
	je	.L219
	movl	-56(%rbp), %r12d
	movzbl	%al, %eax
	movq	(%r14), %rdi
	addl	%ebx, %r12d
	addl	%eax, %r12d
	movl	16(%r15), %eax
	shrl	%eax
	cmpl	%eax, %r12d
	jg	.L222
	addl	%edx, %r12d
	cmpl	%r12d, %eax
	jge	.L222
	cmpq	%rdi, -88(%rbp)
	je	.L212
	testq	%rdi, %rdi
	je	.L223
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L223:
	movq	-88(%rbp), %rax
	movq	%rax, (%r14)
	testq	%rax, %rax
	je	.L212
	.p2align 4,,10
	.p2align 3
.L263:
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L212:
	testq	%r13, %r13
	je	.L201
.L272:
	movq	%r13, %r15
	movq	0(%r13), %r13
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L219:
	movq	(%r14), %rdi
.L222:
	cmpq	%rdi, -80(%rbp)
	je	.L212
	testq	%rdi, %rdi
	je	.L224
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L224:
	movq	-80(%rbp), %rax
	movq	%rax, (%r14)
	testq	%rax, %rax
	jne	.L263
	testq	%r13, %r13
	jne	.L272
.L201:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L271:
	.cfi_restore_state
	testb	%al, %al
	jne	.L208
	movq	(%rdi), %rdi
.L208:
	movzwl	16(%rdi), %eax
	movq	(%r14), %rdi
	cmpl	$6, %eax
	je	.L215
	cmpl	$7, %eax
	je	.L273
.L215:
	cmpq	%rdi, -64(%rbp)
	je	.L212
	testq	%rdi, %rdi
	je	.L217
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L217:
	movq	-64(%rbp), %rax
	movq	%rax, (%r14)
	testq	%rax, %rax
	jne	.L263
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L203:
	leaq	16(%rbx,%rax), %r14
	movq	(%r8), %rdi
	movq	%r8, %rbx
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L270:
	movl	16(%r15), %eax
	movl	%eax, %edx
	andl	$1, %eax
	shrl	%edx
	leaq	3(%rdx,%rdx,2), %rdx
	leaq	(%r15,%rdx,8), %r12
	movq	(%r12), %rdi
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L273:
	cmpq	%rdi, -72(%rbp)
	je	.L212
	testq	%rdi, %rdi
	je	.L216
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L216:
	movq	-72(%rbp), %rax
	movq	%rax, (%r14)
	testq	%rax, %rax
	jne	.L263
	jmp	.L212
	.cfi_endproc
.LFE22719:
	.size	_ZN2v88internal8compiler14NodeProperties11ReplaceUsesEPNS1_4NodeES4_S4_S4_S4_, .-_ZN2v88internal8compiler14NodeProperties11ReplaceUsesEPNS1_4NodeES4_S4_S4_S4_
	.section	.text._ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE
	.type	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE, @function
_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE:
.LFB22720:
	.cfi_startproc
	endbr64
	movq	%rsi, (%rdi)
	ret
	.cfi_endproc
.LFE22720:
	.size	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE, .-_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE
	.section	.text._ZN2v88internal8compiler14NodeProperties20FindFrameStateBeforeEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14NodeProperties20FindFrameStateBeforeEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler14NodeProperties20FindFrameStateBeforeEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler14NodeProperties20FindFrameStateBeforeEPNS1_4NodeES4_:
.LFB22721:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	movl	20(%rdi), %r13d
	call	_ZN2v88internal8compiler18OperatorProperties15HasContextInputEPKNS1_8OperatorE@PLT
	movq	(%rbx), %rdi
	movzbl	%al, %eax
	addl	%eax, %r13d
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	movzbl	23(%rbx), %edx
	movzbl	%al, %eax
	addl	%r13d, %eax
	andl	$15, %edx
	cltq
	salq	$3, %rax
	cmpl	$15, %edx
	je	.L276
	leaq	32(%rbx), %rcx
	addq	%rcx, %rax
.L277:
	movq	(%rax), %r13
	movq	0(%r13), %rdi
	movzwl	16(%rdi), %eax
	cmpl	$38, %eax
	je	.L278
	.p2align 4,,10
	.p2align 3
.L283:
	subl	$59, %eax
	andl	$-3, %eax
	je	.L279
	movl	20(%rdi), %ebx
	call	_ZN2v88internal8compiler18OperatorProperties15HasContextInputEPKNS1_8OperatorE@PLT
	movq	0(%r13), %rdi
	movzbl	%al, %eax
	addl	%eax, %ebx
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	movzbl	23(%r13), %edx
	movzbl	%al, %eax
	addl	%ebx, %eax
	andl	$15, %edx
	cltq
	salq	$3, %rax
	cmpl	$15, %edx
	je	.L280
	movq	32(%r13,%rax), %r13
	movq	0(%r13), %rdi
	movzwl	16(%rdi), %eax
	cmpl	$38, %eax
	jne	.L283
.L278:
	movl	20(%rdi), %ebx
	call	_ZN2v88internal8compiler18OperatorProperties15HasContextInputEPKNS1_8OperatorE@PLT
	movzbl	23(%r13), %edx
	leaq	32(%r13), %rcx
	movzbl	%al, %eax
	andl	$15, %edx
	addl	%ebx, %eax
	cmpl	$15, %edx
	je	.L284
	cltq
	leaq	(%rcx,%rax,8), %rax
.L285:
	movq	(%rax), %r12
.L279:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L280:
	.cfi_restore_state
	movq	32(%r13), %rdx
	movq	16(%rdx,%rax), %r13
	movq	0(%r13), %rdi
	movzwl	16(%rdi), %eax
	cmpl	$38, %eax
	jne	.L283
	jmp	.L278
	.p2align 4,,10
	.p2align 3
.L276:
	movq	32(%rbx), %rdx
	leaq	16(%rdx,%rax), %rax
	jmp	.L277
	.p2align 4,,10
	.p2align 3
.L284:
	movq	32(%r13), %rdx
	cltq
	leaq	16(%rdx,%rax,8), %rax
	jmp	.L285
	.cfi_endproc
.LFE22721:
	.size	_ZN2v88internal8compiler14NodeProperties20FindFrameStateBeforeEPNS1_4NodeES4_, .-_ZN2v88internal8compiler14NodeProperties20FindFrameStateBeforeEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler14NodeProperties14FindProjectionEPNS1_4NodeEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14NodeProperties14FindProjectionEPNS1_4NodeEm
	.type	_ZN2v88internal8compiler14NodeProperties14FindProjectionEPNS1_4NodeEm, @function
_ZN2v88internal8compiler14NodeProperties14FindProjectionEPNS1_4NodeEm:
.LFB22722:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	24(%rdi), %rbx
	testq	%rbx, %rbx
	jne	.L297
	jmp	.L301
	.p2align 4,,10
	.p2align 3
.L302:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L301
.L297:
	movl	16(%rbx), %edx
	movl	%edx, %eax
	shrl	%eax
	andl	$1, %edx
	leaq	3(%rax,%rax,2), %rax
	leaq	(%rbx,%rax,8), %r13
	movq	0(%r13), %rdi
	jne	.L299
	movq	%rdi, %r13
	movq	(%rdi), %rdi
.L299:
	cmpw	$55, 16(%rdi)
	jne	.L302
	call	_ZN2v88internal8compiler17ProjectionIndexOfEPKNS1_8OperatorE@PLT
	cmpq	%r12, %rax
	jne	.L302
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L301:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%r13d, %r13d
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22722:
	.size	_ZN2v88internal8compiler14NodeProperties14FindProjectionEPNS1_4NodeEm, .-_ZN2v88internal8compiler14NodeProperties14FindProjectionEPNS1_4NodeEm
	.section	.text._ZN2v88internal8compiler14NodeProperties23CollectValueProjectionsEPNS1_4NodeEPS4_m,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14NodeProperties23CollectValueProjectionsEPNS1_4NodeEPS4_m
	.type	_ZN2v88internal8compiler14NodeProperties23CollectValueProjectionsEPNS1_4NodeEPS4_m, @function
_ZN2v88internal8compiler14NodeProperties23CollectValueProjectionsEPNS1_4NodeEPS4_m:
.LFB22723:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	testq	%rax, %rax
	je	.L326
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	(%rax), %rbx
	.p2align 4,,10
	.p2align 3
.L311:
	movl	16(%rax), %edx
	movl	%edx, %esi
	shrl	%esi
	andl	$1, %edx
	movl	%esi, %ecx
	leaq	3(%rcx,%rcx,2), %rcx
	leaq	(%rax,%rcx,8), %r12
	movq	(%r12), %rdi
	jne	.L309
	movq	%rdi, %r12
	movq	(%rdi), %rdi
.L309:
	movl	20(%rdi), %eax
	testl	%eax, %eax
	jne	.L329
.L310:
	testq	%rbx, %rbx
	je	.L307
.L330:
	movq	%rbx, %rax
	movq	(%rbx), %rbx
	jmp	.L311
	.p2align 4,,10
	.p2align 3
.L329:
	cmpl	%eax, %esi
	jge	.L310
	call	_ZN2v88internal8compiler17ProjectionIndexOfEPKNS1_8OperatorE@PLT
	movq	%r12, 0(%r13,%rax,8)
	testq	%rbx, %rbx
	jne	.L330
.L307:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L326:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE22723:
	.size	_ZN2v88internal8compiler14NodeProperties23CollectValueProjectionsEPNS1_4NodeEPS4_m, .-_ZN2v88internal8compiler14NodeProperties23CollectValueProjectionsEPNS1_4NodeEPS4_m
	.section	.text._ZN2v88internal8compiler14NodeProperties25CollectControlProjectionsEPNS1_4NodeEPS4_m,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14NodeProperties25CollectControlProjectionsEPNS1_4NodeEPS4_m
	.type	_ZN2v88internal8compiler14NodeProperties25CollectControlProjectionsEPNS1_4NodeEPS4_m, @function
_ZN2v88internal8compiler14NodeProperties25CollectControlProjectionsEPNS1_4NodeEPS4_m:
.LFB22724:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	24(%rdi), %r14
	movq	%rsi, -64(%rbp)
	testq	%r14, %r14
	je	.L331
	leaq	-8(%rsi,%rdx,8), %rax
	movq	$0, -72(%rbp)
	movq	(%r14), %r15
	movq	%rax, -80(%rbp)
	.p2align 4,,10
	.p2align 3
.L345:
	movl	16(%r14), %edx
	movl	%edx, %eax
	shrl	%eax
	andl	$1, %edx
	leaq	3(%rax,%rax,2), %rax
	leaq	(%r14,%rax,8), %rbx
	movq	(%rbx), %rdi
	jne	.L333
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
.L333:
	movl	20(%rdi), %eax
	movl	28(%rdi), %r13d
	movl	%eax, -52(%rbp)
	call	_ZN2v88internal8compiler18OperatorProperties15HasContextInputEPKNS1_8OperatorE@PLT
	movq	(%rbx), %rdi
	movzbl	%al, %r12d
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	testl	%r13d, %r13d
	je	.L335
	movq	(%rbx), %rdi
	movl	-52(%rbp), %ebx
	movzbl	%al, %eax
	addl	%r12d, %ebx
	addl	%eax, %ebx
	addl	24(%rdi), %ebx
	movl	16(%r14), %edi
	movl	%edi, %eax
	shrl	%eax
	cmpl	%eax, %ebx
	jg	.L335
	addl	%r13d, %ebx
	cmpl	%ebx, %eax
	jge	.L335
	leaq	3(%rax,%rax,2), %rax
	andl	$1, %edi
	leaq	(%r14,%rax,8), %rdx
	movq	(%rdx), %rax
	jne	.L337
	movq	%rax, %rdx
	movq	(%rax), %rax
.L337:
	movzwl	16(%rax), %eax
	subl	$4, %eax
	cmpw	$5, %ax
	ja	.L335
	leaq	.L340(%rip), %rcx
	movzwl	%ax, %eax
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler14NodeProperties25CollectControlProjectionsEPNS1_4NodeEPS4_m,"a",@progbits
	.align 4
	.align 4
.L340:
	.long	.L343-.L340
	.long	.L342-.L340
	.long	.L343-.L340
	.long	.L342-.L340
	.long	.L341-.L340
	.long	.L346-.L340
	.section	.text._ZN2v88internal8compiler14NodeProperties25CollectControlProjectionsEPNS1_4NodeEPS4_m
	.p2align 4,,10
	.p2align 3
.L342:
	movq	-64(%rbp), %rax
	addq	$8, %rax
.L339:
	movq	%rdx, (%rax)
.L335:
	testq	%r15, %r15
	je	.L331
	movq	%r15, %r14
	movq	(%r15), %r15
	jmp	.L345
	.p2align 4,,10
	.p2align 3
.L343:
	movq	-64(%rbp), %rax
	jmp	.L339
	.p2align 4,,10
	.p2align 3
.L346:
	movq	-80(%rbp), %rax
	jmp	.L339
	.p2align 4,,10
	.p2align 3
.L341:
	movq	-72(%rbp), %rsi
	movq	-64(%rbp), %rax
	leaq	(%rax,%rsi,8), %rax
	addq	$1, %rsi
	movq	%rsi, -72(%rbp)
	jmp	.L339
	.p2align 4,,10
	.p2align 3
.L331:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22724:
	.size	_ZN2v88internal8compiler14NodeProperties25CollectControlProjectionsEPNS1_4NodeEPS4_m, .-_ZN2v88internal8compiler14NodeProperties25CollectControlProjectionsEPNS1_4NodeEPS4_m
	.section	.text._ZN2v88internal8compiler14NodeProperties6IsSameEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14NodeProperties6IsSameEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler14NodeProperties6IsSameEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler14NodeProperties6IsSameEPNS1_4NodeES4_:
.LFB22725:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	movzwl	16(%rax), %edx
.L361:
	movq	(%rdi), %rax
	movzwl	16(%rax), %eax
.L355:
	cmpw	$228, %ax
	je	.L362
	cmpw	$228, %dx
	je	.L363
	cmpq	%rsi, %rdi
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L362:
	movzbl	23(%rdi), %eax
	movq	32(%rdi), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L361
	movq	16(%rdi), %rdi
	jmp	.L361
	.p2align 4,,10
	.p2align 3
.L363:
	movzbl	23(%rsi), %edx
	movq	32(%rsi), %rsi
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L364
.L360:
	movq	(%rsi), %rdx
	movzwl	16(%rdx), %edx
	jmp	.L355
	.p2align 4,,10
	.p2align 3
.L364:
	movq	16(%rsi), %rsi
	jmp	.L360
	.cfi_endproc
.LFE22725:
	.size	_ZN2v88internal8compiler14NodeProperties6IsSameEPNS1_4NodeES4_, .-_ZN2v88internal8compiler14NodeProperties6IsSameEPNS1_4NodeES4_
	.section	.rodata._ZN2v88internal8compiler14NodeProperties14GetJSCreateMapEPNS1_12JSHeapBrokerEPNS1_4NodeE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"IsHeapObject()"
.LC1:
	.string	"Check failed: %s."
.LC2:
	.string	"Missing "
.LC3:
	.string	"initial map on "
.LC4:
	.string	" ("
	.section	.rodata._ZN2v88internal8compiler14NodeProperties14GetJSCreateMapEPNS1_12JSHeapBrokerEPNS1_4NodeE.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"../deps/v8/src/compiler/node-properties.cc"
	.section	.rodata._ZN2v88internal8compiler14NodeProperties14GetJSCreateMapEPNS1_12JSHeapBrokerEPNS1_4NodeE.str1.1
.LC6:
	.string	":"
.LC7:
	.string	")"
	.section	.text._ZN2v88internal8compiler14NodeProperties14GetJSCreateMapEPNS1_12JSHeapBrokerEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14NodeProperties14GetJSCreateMapEPNS1_12JSHeapBrokerEPNS1_4NodeE
	.type	_ZN2v88internal8compiler14NodeProperties14GetJSCreateMapEPNS1_12JSHeapBrokerEPNS1_4NodeE, @function
_ZN2v88internal8compiler14NodeProperties14GetJSCreateMapEPNS1_12JSHeapBrokerEPNS1_4NodeE:
.LFB22726:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	32(%rdx), %rcx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	32(%rdx), %rax
	movzbl	23(%rdx), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L366
	movq	(%rcx), %rcx
	movzwl	16(%rcx), %edx
	cmpw	$30, %dx
	je	.L369
.L404:
	xorl	%r15d, %r15d
.L370:
	addq	$8, %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	cmpw	$30, 16(%rax)
	jne	.L377
	cmpw	$30, %dx
	je	.L405
.L377:
	movb	$0, (%r12)
	movb	$0, 8(%r12)
.L365:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L406
	addq	$104, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L366:
	.cfi_restore_state
	movq	16(%rcx), %rdx
	leaq	16(%rcx), %rax
	movq	(%rdx), %rcx
	movzwl	16(%rcx), %edx
	cmpw	$30, %dx
	jne	.L404
.L369:
	movq	48(%rcx), %r15
	jmp	.L370
	.p2align 4,,10
	.p2align 3
.L405:
	movq	48(%rax), %rax
	leaq	-112(%rbp), %rbx
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	%rax, %rdx
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L374
	movdqa	-112(%rbp), %xmm0
	leaq	-80(%rbp), %r13
	movq	%r13, %rdi
	movaps	%xmm0, -80(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef12IsJSFunctionEv@PLT
	testb	%al, %al
	je	.L377
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L374
	movdqa	-80(%rbp), %xmm1
	leaq	-96(%rbp), %r15
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	-136(%rbp), %rdx
	movq	%r15, %rdi
	movaps	%xmm1, -128(%rbp)
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L374
	movdqa	-96(%rbp), %xmm2
	movq	%r13, %rdi
	movaps	%xmm2, -80(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef12AsJSFunctionEv@PLT
	movq	%rbx, %rdi
	movq	%rax, -112(%rbp)
	movq	%rdx, -104(%rbp)
	call	_ZNK2v88internal8compiler13HeapObjectRef3mapEv@PLT
	movq	%r13, %rdi
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZNK2v88internal8compiler6MapRef18has_prototype_slotEv@PLT
	testb	%al, %al
	je	.L377
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler13JSFunctionRef15has_initial_mapEv@PLT
	testb	%al, %al
	je	.L377
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler13JSFunctionRef10serializedEv@PLT
	testb	%al, %al
	jne	.L379
	cmpb	$0, 124(%r14)
	je	.L377
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler12JSHeapBroker5TraceEv@PLT
	movl	$8, %edx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$15, %edx
	movq	%r13, %rdi
	leaq	.LC3(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r13, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compilerlsERSoRKNS1_9ObjectRefE@PLT
	movl	$2, %edx
	leaq	.LC4(%rip), %rsi
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r13, %rdi
	movl	$42, %edx
	leaq	.LC5(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC6(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r13, %rdi
	movl	$384, %esi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC7(%rip), %rsi
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	0(%r13), %rax
	movq	-24(%rax), %rax
	movq	240(%r13,%rax), %r14
	testq	%r14, %r14
	je	.L407
	cmpb	$0, 56(%r14)
	je	.L382
	movzbl	67(%r14), %eax
.L383:
	movq	%r13, %rdi
	movsbl	%al, %esi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	jmp	.L377
	.p2align 4,,10
	.p2align 3
.L374:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L379:
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler13JSFunctionRef11initial_mapEv@PLT
	movq	%r15, %rdi
	movq	%rax, -96(%rbp)
	movq	%rdx, -88(%rbp)
	call	_ZNK2v88internal8compiler6MapRef14GetConstructorEv@PLT
	leaq	-128(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef6equalsERKS2_@PLT
	testb	%al, %al
	je	.L377
	movdqa	-96(%rbp), %xmm3
	movb	$1, (%r12)
	movups	%xmm3, 8(%r12)
	jmp	.L365
.L382:
	movq	%r14, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%r14), %rax
	movl	$10, %esi
	movq	%r14, %rdi
	call	*48(%rax)
	jmp	.L383
.L406:
	call	__stack_chk_fail@PLT
.L407:
	call	_ZSt16__throw_bad_castv@PLT
	.cfi_endproc
.LFE22726:
	.size	_ZN2v88internal8compiler14NodeProperties14GetJSCreateMapEPNS1_12JSHeapBrokerEPNS1_4NodeE, .-_ZN2v88internal8compiler14NodeProperties14GetJSCreateMapEPNS1_12JSHeapBrokerEPNS1_4NodeE
	.section	.rodata._ZN2v88internal8compiler14NodeProperties23InferReceiverMapsUnsafeEPNS1_12JSHeapBrokerEPNS1_4NodeES6_PNS0_13ZoneHandleSetINS0_3MapEEE.str1.1,"aMS",@progbits,1
.LC8:
	.string	"storage_.is_populated_"
	.section	.text._ZN2v88internal8compiler14NodeProperties23InferReceiverMapsUnsafeEPNS1_12JSHeapBrokerEPNS1_4NodeES6_PNS0_13ZoneHandleSetINS0_3MapEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14NodeProperties23InferReceiverMapsUnsafeEPNS1_12JSHeapBrokerEPNS1_4NodeES6_PNS0_13ZoneHandleSetINS0_3MapEEE
	.type	_ZN2v88internal8compiler14NodeProperties23InferReceiverMapsUnsafeEPNS1_12JSHeapBrokerEPNS1_4NodeES6_PNS0_13ZoneHandleSetINS0_3MapEEE, @function
_ZN2v88internal8compiler14NodeProperties23InferReceiverMapsUnsafeEPNS1_12JSHeapBrokerEPNS1_4NodeES6_PNS0_13ZoneHandleSetINS0_3MapEEE:
.LFB22727:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$104, %rsp
	movq	%rdi, -120(%rbp)
	movq	%rcx, -128(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	cmpw	$30, 16(%rax)
	jne	.L409
	leaq	-96(%rbp), %r12
	movq	48(%rax), %rdx
	xorl	%ecx, %ecx
	movq	%rdi, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L464
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef10IsJSObjectEv@PLT
	testb	%al, %al
	je	.L495
	movq	%r12, %rdi
	leaq	-80(%rbp), %r14
	call	_ZNK2v88internal8compiler9ObjectRef10AsJSObjectEv@PLT
	movq	-120(%rbp), %rdi
	movq	%r14, %rsi
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZNK2v88internal8compiler12JSHeapBroker24IsArrayOrObjectPrototypeERKNS1_11JSObjectRefE@PLT
	testb	%al, %al
	jne	.L409
.L414:
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13HeapObjectRef3mapEv@PLT
	movq	%r14, %rdi
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZNK2v88internal8compiler6MapRef9is_stableEv@PLT
	testb	%al, %al
	jne	.L496
.L409:
	movabsq	$-4294967041, %r14
	movl	$1, %r12d
.L416:
	movq	0(%r13), %rdi
	movzwl	16(%rdi), %eax
	cmpw	$245, %ax
	je	.L417
	.p2align 4,,10
	.p2align 3
.L501:
	ja	.L418
	cmpw	$57, %ax
	je	.L419
	ja	.L420
	cmpw	$36, %ax
	je	.L421
	cmpw	$40, %ax
	jne	.L423
	movq	(%rbx), %rdx
	movzwl	16(%rdx), %ecx
	movq	%rbx, %rdx
.L431:
	cmpw	$228, %cx
	je	.L497
	cmpq	%r13, %rdx
	je	.L498
	.p2align 4,,10
	.p2align 3
.L427:
	movq	(%rbx), %rdx
	movq	%r13, %rcx
	movzwl	16(%rdx), %esi
	movq	%rbx, %rdx
.L474:
	cmpw	$228, %si
	je	.L499
	cmpw	$228, %ax
	je	.L500
	cmpq	%rcx, %rdx
	je	.L470
	movl	20(%rdi), %r15d
	call	_ZN2v88internal8compiler18OperatorProperties15HasContextInputEPKNS1_8OperatorE@PLT
	movq	0(%r13), %rdi
	movzbl	%al, %eax
	addl	%eax, %r15d
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	movzbl	23(%r13), %edx
	leaq	32(%r13), %rcx
	movzbl	%al, %eax
	addl	%r15d, %eax
	andl	$15, %edx
	cltq
	salq	$3, %rax
	cmpl	$15, %edx
	je	.L480
	addq	%rcx, %rax
.L481:
	movq	(%rax), %r13
	movq	0(%r13), %rdi
	movzwl	16(%rdi), %eax
	cmpw	$245, %ax
	jne	.L501
.L417:
	leaq	32(%r13), %rax
	movq	32(%r13), %r15
	movq	%rax, -136(%rbp)
	movzbl	23(%r13), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L452
	movq	16(%r15), %r15
.L452:
	call	_ZN2v88internal8compiler13FieldAccessOfEPKNS1_8OperatorE@PLT
	movq	(%rax), %rsi
	andq	%r14, %rsi
	cmpq	$1, %rsi
	je	.L453
	movq	0(%r13), %rdi
	movzwl	16(%rdi), %eax
	jmp	.L427
	.p2align 4,,10
	.p2align 3
.L420:
	cmpw	$220, %ax
	jne	.L423
	movzbl	23(%r13), %ecx
	movq	32(%r13), %rdx
	andl	$15, %ecx
	cmpl	$15, %ecx
	jne	.L439
	movq	16(%rdx), %rdx
.L439:
	movq	(%rbx), %rcx
	movzwl	16(%rcx), %esi
	movq	(%rdx), %rcx
	movzwl	16(%rcx), %r10d
	movq	%rbx, %rcx
.L440:
	cmpw	$228, %si
	je	.L502
	cmpw	$228, %r10w
	je	.L503
	cmpq	%rdx, %rcx
	jne	.L427
	call	_ZN2v88internal8compiler21CheckMapsParametersOfEPKNS1_8OperatorE@PLT
	movq	-128(%rbp), %rbx
	movq	8(%rax), %rax
	movq	%rax, (%rbx)
	jmp	.L408
	.p2align 4,,10
	.p2align 3
.L418:
	cmpw	$734, %ax
	je	.L425
	jbe	.L504
	movl	%eax, %edx
	andl	$-3, %edx
	cmpw	$772, %dx
	je	.L427
.L423:
	cmpl	$1, 24(%rdi)
	jne	.L470
	testb	$16, 18(%rdi)
	movl	$2, %edx
	cmove	%edx, %r12d
	jmp	.L427
	.p2align 4,,10
	.p2align 3
.L504:
	cmpw	$247, %ax
	jbe	.L427
	cmpw	$716, %ax
	jne	.L423
	movq	(%rbx), %rdx
	movzwl	16(%rdx), %ecx
	movq	%rbx, %rdx
.L430:
	cmpw	$228, %cx
	je	.L505
	cmpq	%r13, %rdx
	jne	.L427
	movq	-120(%rbp), %rsi
	leaq	-80(%rbp), %rdi
	movq	%rbx, %rdx
	call	_ZN2v88internal8compiler14NodeProperties14GetJSCreateMapEPNS1_12JSHeapBrokerEPNS1_4NodeE
	cmpb	$0, -80(%rbp)
	je	.L470
	leaq	-72(%rbp), %rdi
	call	_ZNK2v88internal8compiler6MapRef6objectEv@PLT
	movq	-128(%rbp), %rbx
	movq	%rax, (%rbx)
	jmp	.L408
	.p2align 4,,10
	.p2align 3
.L480:
	movq	32(%r13), %rdx
	leaq	16(%rdx,%rax), %rax
	jmp	.L481
	.p2align 4,,10
	.p2align 3
.L499:
	movzbl	23(%rdx), %esi
	movq	32(%rdx), %rdx
	andl	$15, %esi
	cmpl	$15, %esi
	jne	.L476
	movq	16(%rdx), %rdx
.L476:
	movq	(%rdx), %rsi
	movzwl	16(%rsi), %esi
	jmp	.L474
	.p2align 4,,10
	.p2align 3
.L500:
	movzbl	23(%rcx), %eax
	movq	32(%rcx), %rcx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L479
	movq	16(%rcx), %rcx
.L479:
	movq	(%rcx), %rax
	movzwl	16(%rax), %eax
	jmp	.L474
	.p2align 4,,10
	.p2align 3
.L425:
	movq	(%rbx), %rdx
	movzwl	16(%rdx), %ecx
	movq	%rbx, %rdx
.L429:
	cmpw	$228, %cx
	je	.L506
	cmpq	%r13, %rdx
	jne	.L427
	movq	-120(%rbp), %rax
	cmpb	$0, 24(%rax)
	je	.L507
	movdqu	32(%rax), %xmm0
	leaq	-112(%rbp), %rdi
	movaps	%xmm0, -112(%rbp)
	call	_ZNK2v88internal8compiler16NativeContextRef16promise_functionEv@PLT
	leaq	-96(%rbp), %rdi
	movq	%rax, -96(%rbp)
	movq	%rdx, -88(%rbp)
	call	_ZNK2v88internal8compiler13JSFunctionRef11initial_mapEv@PLT
	leaq	-80(%rbp), %rdi
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZNK2v88internal8compiler6MapRef6objectEv@PLT
	movq	-128(%rbp), %rbx
	movq	%rax, (%rbx)
	jmp	.L408
	.p2align 4,,10
	.p2align 3
.L421:
	movl	20(%rdi), %r15d
	call	_ZN2v88internal8compiler18OperatorProperties15HasContextInputEPKNS1_8OperatorE@PLT
	movq	0(%r13), %rdi
	movzbl	%al, %r12d
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	addl	%r15d, %r12d
	movq	0(%r13), %rdi
	leaq	32(%r13), %r15
	movzbl	%al, %eax
	addl	%eax, %r12d
	movzbl	23(%r13), %eax
	addl	24(%rdi), %r12d
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L468
	movslq	%r12d, %r12
	leaq	(%r15,%r12,8), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	cmpw	$1, 16(%rax)
	je	.L508
.L470:
	xorl	%r12d, %r12d
.L408:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L509
	addq	$104, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L419:
	.cfi_restore_state
	movzbl	23(%r13), %ecx
	movq	32(%r13), %rdx
	andl	$15, %ecx
	cmpl	$15, %ecx
	jne	.L432
	movq	16(%rdx), %rdx
.L432:
	movq	(%rbx), %rcx
	movzwl	16(%rcx), %esi
	movq	(%rdx), %rcx
	movzwl	16(%rcx), %r10d
	movq	%rbx, %rcx
.L433:
	cmpw	$228, %si
	je	.L510
	cmpw	$228, %r10w
	je	.L511
	cmpq	%rdx, %rcx
	jne	.L427
	call	_ZN2v88internal8compiler14MapGuardMapsOfEPKNS1_8OperatorE@PLT
	movq	-128(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	jmp	.L408
	.p2align 4,,10
	.p2align 3
.L498:
	movzbl	23(%r13), %edx
	movq	32(%r13), %rbx
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L427
	movq	16(%rbx), %rbx
	jmp	.L427
	.p2align 4,,10
	.p2align 3
.L468:
	movq	32(%r13), %rax
	movslq	%r12d, %r12
	leaq	16(%rax,%r12,8), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	cmpw	$1, 16(%rax)
	jne	.L470
.L508:
	movl	20(%rdi), %eax
	movl	%eax, -136(%rbp)
	call	_ZN2v88internal8compiler18OperatorProperties15HasContextInputEPKNS1_8OperatorE@PLT
	movq	0(%r13), %rdi
	movzbl	%al, %r12d
	addl	-136(%rbp), %r12d
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	movzbl	%al, %eax
	addl	%eax, %r12d
	movzbl	23(%r13), %eax
	movslq	%r12d, %r12
	salq	$3, %r12
	andl	$15, %eax
	leaq	(%r15,%r12), %rdx
	cmpl	$15, %eax
	jne	.L472
	movq	(%r15), %rax
	leaq	16(%rax,%r12), %rdx
.L472:
	movq	(%rdx), %r13
	movl	$2, %r12d
	jmp	.L416
	.p2align 4,,10
	.p2align 3
.L506:
	movzbl	23(%rdx), %ecx
	movq	32(%rdx), %rdx
	andl	$15, %ecx
	cmpl	$15, %ecx
	je	.L512
.L450:
	movq	(%rdx), %rcx
	movzwl	16(%rcx), %ecx
	jmp	.L429
	.p2align 4,,10
	.p2align 3
.L505:
	movzbl	23(%rdx), %ecx
	movq	32(%rdx), %rdx
	andl	$15, %ecx
	cmpl	$15, %ecx
	je	.L513
.L447:
	movq	(%rdx), %rcx
	movzwl	16(%rcx), %ecx
	jmp	.L430
	.p2align 4,,10
	.p2align 3
.L497:
	movzbl	23(%rdx), %ecx
	movq	32(%rdx), %rdx
	andl	$15, %ecx
	cmpl	$15, %ecx
	je	.L514
.L466:
	movq	(%rdx), %rcx
	movzwl	16(%rcx), %ecx
	jmp	.L431
	.p2align 4,,10
	.p2align 3
.L502:
	movzbl	23(%rcx), %esi
	movq	32(%rcx), %rcx
	andl	$15, %esi
	cmpl	$15, %esi
	je	.L515
.L442:
	movq	(%rcx), %rsi
	movzwl	16(%rsi), %esi
	jmp	.L440
	.p2align 4,,10
	.p2align 3
.L510:
	movzbl	23(%rcx), %esi
	movq	32(%rcx), %rcx
	andl	$15, %esi
	cmpl	$15, %esi
	je	.L516
.L435:
	movq	(%rcx), %rsi
	movzwl	16(%rsi), %esi
	jmp	.L433
	.p2align 4,,10
	.p2align 3
.L511:
	movzbl	23(%rdx), %r10d
	movq	32(%rdx), %rdx
	andl	$15, %r10d
	cmpl	$15, %r10d
	je	.L517
.L438:
	movq	(%rdx), %r10
	movzwl	16(%r10), %r10d
	jmp	.L433
	.p2align 4,,10
	.p2align 3
.L503:
	movzbl	23(%rdx), %r10d
	movq	32(%rdx), %rdx
	andl	$15, %r10d
	cmpl	$15, %r10d
	je	.L518
.L445:
	movq	(%rdx), %r10
	movzwl	16(%r10), %r10d
	jmp	.L440
	.p2align 4,,10
	.p2align 3
.L495:
	leaq	-80(%rbp), %r14
	jmp	.L414
	.p2align 4,,10
	.p2align 3
.L516:
	movq	16(%rcx), %rcx
	jmp	.L435
	.p2align 4,,10
	.p2align 3
.L515:
	movq	16(%rcx), %rcx
	jmp	.L442
	.p2align 4,,10
	.p2align 3
.L514:
	movq	16(%rdx), %rdx
	jmp	.L466
	.p2align 4,,10
	.p2align 3
.L513:
	movq	16(%rdx), %rdx
	jmp	.L447
	.p2align 4,,10
	.p2align 3
.L512:
	movq	16(%rdx), %rdx
	jmp	.L450
.L518:
	movq	16(%rdx), %rdx
	jmp	.L445
.L517:
	movq	16(%rdx), %rdx
	jmp	.L438
	.p2align 4,,10
	.p2align 3
.L453:
	movq	(%rbx), %rax
	movzwl	16(%rax), %ecx
	movq	(%r15), %rax
	movzwl	16(%rax), %esi
	movq	%rbx, %rax
.L454:
	cmpw	$228, %cx
	je	.L519
	cmpw	$228, %si
	je	.L520
	cmpq	%r15, %rax
	jne	.L494
	movzbl	23(%r13), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L461
	movq	-136(%rbp), %r15
	addq	$8, %r15
.L462:
	movq	(%r15), %rax
	movq	(%rax), %rax
	cmpw	$30, 16(%rax)
	jne	.L494
	leaq	-80(%rbp), %r14
	movq	48(%rax), %rdx
	movq	-120(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L464
	movdqa	-80(%rbp), %xmm1
	leaq	-96(%rbp), %rdi
	movaps	%xmm1, -96(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef5AsMapEv@PLT
	movq	%r14, %rdi
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZNK2v88internal8compiler6MapRef6objectEv@PLT
	movq	-128(%rbp), %rbx
	movq	%rax, (%rbx)
	jmp	.L408
	.p2align 4,,10
	.p2align 3
.L494:
	movq	0(%r13), %rdi
	movl	$2, %r12d
	movzwl	16(%rdi), %eax
	jmp	.L427
.L519:
	movzbl	23(%rax), %ecx
	movq	32(%rax), %rax
	andl	$15, %ecx
	cmpl	$15, %ecx
	je	.L521
.L456:
	movq	(%rax), %rcx
	movzwl	16(%rcx), %ecx
	jmp	.L454
.L464:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L496:
	movq	%r12, %rdi
	movl	$2, %r12d
	call	_ZNK2v88internal8compiler13HeapObjectRef3mapEv@PLT
	movq	%r14, %rdi
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZNK2v88internal8compiler6MapRef6objectEv@PLT
	movq	-128(%rbp), %rbx
	movq	%rax, (%rbx)
	jmp	.L408
.L521:
	movq	16(%rax), %rax
	jmp	.L456
.L520:
	movzbl	23(%r15), %esi
	movq	32(%r15), %r15
	andl	$15, %esi
	cmpl	$15, %esi
	jne	.L459
	movq	16(%r15), %r15
.L459:
	movq	(%r15), %rsi
	movzwl	16(%rsi), %esi
	jmp	.L454
.L461:
	movq	-136(%rbp), %rax
	movq	(%rax), %r15
	addq	$24, %r15
	jmp	.L462
.L507:
	leaq	.LC8(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L509:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22727:
	.size	_ZN2v88internal8compiler14NodeProperties23InferReceiverMapsUnsafeEPNS1_12JSHeapBrokerEPNS1_4NodeES6_PNS0_13ZoneHandleSetINS0_3MapEEE, .-_ZN2v88internal8compiler14NodeProperties23InferReceiverMapsUnsafeEPNS1_12JSHeapBrokerEPNS1_4NodeES6_PNS0_13ZoneHandleSetINS0_3MapEEE
	.section	.text._ZN2v88internal8compiler14NodeProperties29NoObservableSideEffectBetweenEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14NodeProperties29NoObservableSideEffectBetweenEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler14NodeProperties29NoObservableSideEffectBetweenEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler14NodeProperties29NoObservableSideEffectBetweenEPNS1_4NodeES4_:
.LFB22729:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	cmpq	%rsi, %rdi
	je	.L526
	.p2align 4,,10
	.p2align 3
.L523:
	movq	(%r12), %rdi
	cmpl	$1, 24(%rdi)
	jne	.L529
.L533:
	testb	$16, 18(%rdi)
	je	.L529
	movl	20(%rdi), %r13d
	call	_ZN2v88internal8compiler18OperatorProperties15HasContextInputEPKNS1_8OperatorE@PLT
	movq	(%r12), %rdi
	movzbl	%al, %eax
	addl	%eax, %r13d
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	movzbl	23(%r12), %edx
	movzbl	%al, %eax
	addl	%r13d, %eax
	andl	$15, %edx
	cltq
	salq	$3, %rax
	cmpl	$15, %edx
	je	.L525
	movq	32(%r12,%rax), %r12
	cmpq	%r12, %rbx
	jne	.L523
.L526:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L525:
	.cfi_restore_state
	movq	32(%r12), %rdx
	movq	16(%rdx,%rax), %r12
	cmpq	%r12, %rbx
	je	.L526
	movq	(%r12), %rdi
	cmpl	$1, 24(%rdi)
	je	.L533
.L529:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22729:
	.size	_ZN2v88internal8compiler14NodeProperties29NoObservableSideEffectBetweenEPNS1_4NodeES4_, .-_ZN2v88internal8compiler14NodeProperties29NoObservableSideEffectBetweenEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler14NodeProperties14CanBePrimitiveEPNS1_12JSHeapBrokerEPNS1_4NodeES6_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14NodeProperties14CanBePrimitiveEPNS1_12JSHeapBrokerEPNS1_4NodeES6_
	.type	_ZN2v88internal8compiler14NodeProperties14CanBePrimitiveEPNS1_12JSHeapBrokerEPNS1_4NodeES6_, @function
_ZN2v88internal8compiler14NodeProperties14CanBePrimitiveEPNS1_12JSHeapBrokerEPNS1_4NodeES6_:
.LFB22730:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	subq	$80, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	(%rsi), %rcx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movzwl	16(%rcx), %eax
	cmpw	$223, %ax
	je	.L545
	ja	.L536
	cmpw	$30, %ax
	jne	.L538
	movq	48(%rcx), %rdx
	leaq	-80(%rbp), %r12
	xorl	%ecx, %ecx
	movq	%rdi, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L550
	movdqa	-80(%rbp), %xmm0
	leaq	-96(%rbp), %rdi
	movaps	%xmm0, -96(%rbp)
	call	_ZNK2v88internal8compiler13HeapObjectRef3mapEv@PLT
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZNK2v88internal8compiler6MapRef14IsPrimitiveMapEv@PLT
	movl	%eax, %r12d
.L534:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L551
	addq	$80, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L536:
	.cfi_restore_state
	cmpw	$234, %ax
	je	.L545
	subw	$708, %ax
	cmpw	$54, %ax
	ja	.L538
	movabsq	$33778097253777281, %rcx
	xorl	%r12d, %r12d
	btq	%rax, %rcx
	jc	.L534
.L538:
	leaq	-80(%rbp), %r13
	movq	%rdx, %rcx
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	movq	%r13, %rdi
	movl	$1, %r12d
	call	_ZN2v88internal8compiler12MapInferenceC1EPNS1_12JSHeapBrokerEPNS1_4NodeES6_@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler12MapInference8HaveMapsEv@PLT
	testb	%al, %al
	jne	.L552
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler12MapInferenceD1Ev@PLT
	jmp	.L534
	.p2align 4,,10
	.p2align 3
.L552:
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler12MapInference31AllOfInstanceTypesAreJSReceiverEv@PLT
	movq	%r13, %rdi
	xorl	$1, %eax
	movl	%eax, %r12d
	call	_ZN2v88internal8compiler12MapInferenceD1Ev@PLT
	jmp	.L534
	.p2align 4,,10
	.p2align 3
.L545:
	xorl	%r12d, %r12d
	jmp	.L534
	.p2align 4,,10
	.p2align 3
.L550:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L551:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22730:
	.size	_ZN2v88internal8compiler14NodeProperties14CanBePrimitiveEPNS1_12JSHeapBrokerEPNS1_4NodeES6_, .-_ZN2v88internal8compiler14NodeProperties14CanBePrimitiveEPNS1_12JSHeapBrokerEPNS1_4NodeES6_
	.section	.text._ZN2v88internal8compiler14NodeProperties20CanBeNullOrUndefinedEPNS1_12JSHeapBrokerEPNS1_4NodeES6_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14NodeProperties20CanBeNullOrUndefinedEPNS1_12JSHeapBrokerEPNS1_4NodeES6_
	.type	_ZN2v88internal8compiler14NodeProperties20CanBeNullOrUndefinedEPNS1_12JSHeapBrokerEPNS1_4NodeES6_, @function
_ZN2v88internal8compiler14NodeProperties20CanBeNullOrUndefinedEPNS1_12JSHeapBrokerEPNS1_4NodeES6_:
.LFB22731:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$80, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rsi), %rcx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzwl	16(%rcx), %eax
	cmpw	$223, %ax
	je	.L587
	movq	%rdi, %r12
	movq	%rsi, %rbx
	ja	.L555
	cmpw	$30, %ax
	jne	.L557
	movq	48(%rcx), %rdx
	leaq	-96(%rbp), %r13
	xorl	%ecx, %ecx
	movq	%rdi, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L564
	movdqa	-96(%rbp), %xmm0
	leaq	-112(%rbp), %rdi
	movaps	%xmm0, -112(%rbp)
	call	_ZNK2v88internal8compiler13HeapObjectRef3mapEv@PLT
	movq	%r13, %rdi
	movq	%rax, -96(%rbp)
	movq	%rdx, -88(%rbp)
	call	_ZNK2v88internal8compiler6MapRef14IsPrimitiveMapEv@PLT
	testb	%al, %al
	je	.L553
	movq	(%rbx), %rdx
	movzwl	16(%rdx), %eax
	cmpw	$254, %ax
	jbe	.L589
.L565:
	cmpw	$707, %ax
	ja	.L568
	cmpw	$702, %ax
	ja	.L587
.L588:
	movl	$1, %eax
	jmp	.L553
	.p2align 4,,10
	.p2align 3
.L555:
	cmpw	$234, %ax
	je	.L587
	subw	$708, %ax
	cmpw	$54, %ax
	ja	.L557
	movabsq	$33778097253777281, %rcx
	btq	%rax, %rcx
	jc	.L587
.L557:
	leaq	-96(%rbp), %r13
	movq	%rdx, %rcx
	movq	%r12, %rsi
	movq	%rbx, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler12MapInferenceC1EPNS1_12JSHeapBrokerEPNS1_4NodeES6_@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler12MapInference8HaveMapsEv@PLT
	testb	%al, %al
	jne	.L590
.L562:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler12MapInferenceD1Ev@PLT
	movq	(%rbx), %rdx
	movzwl	16(%rdx), %eax
	cmpw	$254, %ax
	ja	.L565
.L589:
	cmpw	$220, %ax
	ja	.L566
	cmpw	$30, %ax
	jne	.L588
	leaq	-112(%rbp), %r14
	movq	48(%rdx), %rdx
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L564
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler13HeapObjectRef3mapEv@PLT
	movq	%r13, %rdi
	movq	%rax, -96(%rbp)
	movq	%rdx, -88(%rbp)
	call	_ZNK2v88internal8compiler6MapRef12oddball_typeEv@PLT
	subl	$2, %eax
	cmpb	$1, %al
	setbe	%al
	jmp	.L553
	.p2align 4,,10
	.p2align 3
.L590:
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler12MapInference31AllOfInstanceTypesAreJSReceiverEv@PLT
	testb	%al, %al
	je	.L562
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler12MapInferenceD1Ev@PLT
.L587:
	xorl	%eax, %eax
.L553:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L591
	addq	$80, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L566:
	.cfi_restore_state
	movabsq	$8589934707, %rdx
	subw	$221, %ax
	btq	%rax, %rdx
	jc	.L587
	movl	$1, %eax
	jmp	.L553
	.p2align 4,,10
	.p2align 3
.L568:
	cmpw	$709, %ax
	je	.L587
	movl	$1, %eax
	jmp	.L553
	.p2align 4,,10
	.p2align 3
.L564:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L591:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22731:
	.size	_ZN2v88internal8compiler14NodeProperties20CanBeNullOrUndefinedEPNS1_12JSHeapBrokerEPNS1_4NodeES6_, .-_ZN2v88internal8compiler14NodeProperties20CanBeNullOrUndefinedEPNS1_12JSHeapBrokerEPNS1_4NodeES6_
	.section	.text._ZN2v88internal8compiler14NodeProperties15GetOuterContextEPNS1_4NodeEPm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14NodeProperties15GetOuterContextEPNS1_4NodeEPm
	.type	_ZN2v88internal8compiler14NodeProperties15GetOuterContextEPNS1_4NodeEPm, @function
_ZN2v88internal8compiler14NodeProperties15GetOuterContextEPNS1_4NodeEPm:
.LFB22732:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	leaq	32(%rdi), %rcx
	movslq	20(%rax), %rax
	leaq	0(,%rax,8), %rdx
	movzbl	23(%rdi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L593
	leaq	(%rcx,%rdx), %rax
.L594:
	movq	(%rsi), %rdx
	movq	(%rax), %rax
	testq	%rdx, %rdx
	je	.L592
	.p2align 4,,10
	.p2align 3
.L600:
	movq	(%rax), %rdi
	movzwl	16(%rdi), %ecx
	subl	$751, %ecx
	cmpl	$3, %ecx
	ja	.L592
	movslq	20(%rdi), %rcx
	leaq	0(,%rcx,8), %rdi
	movzbl	23(%rax), %ecx
	andl	$15, %ecx
	cmpl	$15, %ecx
	je	.L607
	subq	$1, %rdx
	movq	32(%rax,%rdi), %rax
	movq	%rdx, (%rsi)
	jne	.L600
	ret
	.p2align 4,,10
	.p2align 3
.L607:
	movq	32(%rax), %rax
	subq	$1, %rdx
	movq	16(%rax,%rdi), %rax
	movq	%rdx, (%rsi)
	jne	.L600
.L592:
	ret
	.p2align 4,,10
	.p2align 3
.L593:
	movq	32(%rdi), %rax
	leaq	16(%rax,%rdx), %rax
	jmp	.L594
	.cfi_endproc
.LFE22732:
	.size	_ZN2v88internal8compiler14NodeProperties15GetOuterContextEPNS1_4NodeEPm, .-_ZN2v88internal8compiler14NodeProperties15GetOuterContextEPNS1_4NodeEPm
	.section	.text._ZN2v88internal8compiler14NodeProperties12GetTypeOrAnyEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14NodeProperties12GetTypeOrAnyEPNS1_4NodeE
	.type	_ZN2v88internal8compiler14NodeProperties12GetTypeOrAnyEPNS1_4NodeE, @function
_ZN2v88internal8compiler14NodeProperties12GetTypeOrAnyEPNS1_4NodeE:
.LFB22733:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movl	$4294967295, %edx
	testq	%rax, %rax
	cmove	%rdx, %rax
	ret
	.cfi_endproc
.LFE22733:
	.size	_ZN2v88internal8compiler14NodeProperties12GetTypeOrAnyEPNS1_4NodeE, .-_ZN2v88internal8compiler14NodeProperties12GetTypeOrAnyEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler14NodeProperties22AllValueInputsAreTypedEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14NodeProperties22AllValueInputsAreTypedEPNS1_4NodeE
	.type	_ZN2v88internal8compiler14NodeProperties22AllValueInputsAreTypedEPNS1_4NodeE, @function
_ZN2v88internal8compiler14NodeProperties22AllValueInputsAreTypedEPNS1_4NodeE:
.LFB22734:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movl	20(%rax), %ecx
	testl	%ecx, %ecx
	jle	.L618
	movzbl	23(%rdi), %edx
	leaq	32(%rdi), %rax
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L622
	leal	-1(%rcx), %edx
	leaq	40(%rdi,%rdx,8), %rcx
	jmp	.L615
	.p2align 4,,10
	.p2align 3
.L623:
	addq	$8, %rax
	cmpq	%rcx, %rax
	je	.L618
.L615:
	movq	(%rax), %rdx
	cmpq	$0, 8(%rdx)
	jne	.L623
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L618:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L622:
	movq	32(%rdi), %rdx
	subl	$1, %ecx
	leaq	16(%rdx), %rax
	leaq	24(%rdx,%rcx,8), %rcx
	jmp	.L614
	.p2align 4,,10
	.p2align 3
.L624:
	addq	$8, %rax
	cmpq	%rax, %rcx
	je	.L618
.L614:
	movq	(%rax), %rdx
	cmpq	$0, 8(%rdx)
	jne	.L624
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE22734:
	.size	_ZN2v88internal8compiler14NodeProperties22AllValueInputsAreTypedEPNS1_4NodeE, .-_ZN2v88internal8compiler14NodeProperties22AllValueInputsAreTypedEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler14NodeProperties8HashCodeEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14NodeProperties8HashCodeEPNS1_4NodeE
	.type	_ZN2v88internal8compiler14NodeProperties8HashCodeEPNS1_4NodeE, @function
_ZN2v88internal8compiler14NodeProperties8HashCodeEPNS1_4NodeE:
.LFB22736:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movzbl	23(%rdi), %r13d
	leaq	32(%rdi), %rbx
	andl	$15, %r13d
	cmpl	$15, %r13d
	jne	.L626
	movq	32(%rdi), %rax
	movl	8(%rax), %r13d
.L626:
	movq	(%r12), %rdi
	leaq	_ZNK2v88internal8compiler8Operator8HashCodeEv(%rip), %rdx
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L627
	movzwl	16(%rdi), %edi
.L628:
	call	_ZN2v84base10hash_valueEm@PLT
	movl	%r13d, %edi
	movq	%rax, %r14
	call	_ZN2v84base10hash_valueEj@PLT
	xorl	%edi, %edi
	movq	%rax, %rsi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	%rax, %rdi
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L630
	movq	32(%r12), %rbx
	movl	8(%rbx), %eax
	addq	$16, %rbx
.L630:
	cltq
	leaq	(%rbx,%rax,8), %r12
	cmpq	%r12, %rbx
	je	.L625
	.p2align 4,,10
	.p2align 3
.L632:
	movq	(%rbx), %rax
	addq	$8, %rbx
	movl	20(%rax), %r14d
	call	_ZN2v84base10hash_valueEm@PLT
	andl	$16777215, %r14d
	movq	%rax, %r13
	movl	%r14d, %edi
	call	_ZN2v84base10hash_valueEj@PLT
	xorl	%edi, %edi
	movq	%rax, %rsi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	%rax, %rdi
	cmpq	%rbx, %r12
	jne	.L632
.L625:
	popq	%rbx
	movq	%rdi, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L627:
	.cfi_restore_state
	call	*%rax
	movq	%rax, %rdi
	jmp	.L628
	.cfi_endproc
.LFE22736:
	.size	_ZN2v88internal8compiler14NodeProperties8HashCodeEPNS1_4NodeE, .-_ZN2v88internal8compiler14NodeProperties8HashCodeEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler14NodeProperties6EqualsEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14NodeProperties6EqualsEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler14NodeProperties6EqualsEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler14NodeProperties6EqualsEPNS1_4NodeES4_:
.LFB22737:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZNK2v88internal8compiler8Operator6EqualsEPKS2_(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	(%rdi), %rdi
	movq	%rsi, %rbx
	movq	(%rsi), %rsi
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L636
	movzwl	16(%rsi), %eax
	cmpw	%ax, 16(%rdi)
	sete	%al
.L637:
	testb	%al, %al
	je	.L635
	movzbl	23(%r12), %esi
	andl	$15, %esi
	movslq	%esi, %r8
	cmpl	$15, %esi
	je	.L653
	movzbl	23(%rbx), %ecx
	andl	$15, %ecx
	movl	%ecx, %edx
	cmpl	$15, %ecx
	je	.L654
.L642:
	cmpl	%r8d, %edx
	jne	.L647
.L656:
	leaq	32(%r12), %rdx
	cmpl	$15, %esi
	jne	.L643
	movq	32(%r12), %rdx
	addq	$16, %rdx
.L643:
	leaq	32(%rbx), %rdi
	cmpl	$15, %ecx
	jne	.L644
	movq	32(%rbx), %rdi
	addq	$16, %rdi
.L644:
	leaq	(%rdx,%r8,8), %r8
	cmpq	%r8, %rdx
	jne	.L645
	jmp	.L635
	.p2align 4,,10
	.p2align 3
.L655:
	addq	$8, %rdx
	addq	$8, %rdi
	cmpq	%rdx, %r8
	je	.L635
.L645:
	movq	(%rdi), %rcx
	movl	20(%rcx), %esi
	movq	(%rdx), %rcx
	movl	20(%rcx), %ecx
	andl	$16777215, %esi
	andl	$16777215, %ecx
	cmpl	%ecx, %esi
	je	.L655
.L647:
	xorl	%eax, %eax
.L635:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L654:
	.cfi_restore_state
	movq	32(%rbx), %rdx
	movl	8(%rdx), %edx
	cmpl	%r8d, %edx
	jne	.L647
	jmp	.L656
	.p2align 4,,10
	.p2align 3
.L653:
	movzbl	23(%rbx), %ecx
	movq	32(%r12), %rdx
	andl	$15, %ecx
	movslq	8(%rdx), %r8
	movl	%ecx, %edx
	cmpl	$15, %ecx
	jne	.L642
	jmp	.L654
	.p2align 4,,10
	.p2align 3
.L636:
	call	*%rax
	jmp	.L637
	.cfi_endproc
.LFE22737:
	.size	_ZN2v88internal8compiler14NodeProperties6EqualsEPNS1_4NodeES4_, .-_ZN2v88internal8compiler14NodeProperties6EqualsEPNS1_4NodeES4_
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler14NodeProperties14PastValueIndexEPNS1_4NodeE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler14NodeProperties14PastValueIndexEPNS1_4NodeE, @function
_GLOBAL__sub_I__ZN2v88internal8compiler14NodeProperties14PastValueIndexEPNS1_4NodeE:
.LFB27464:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE27464:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler14NodeProperties14PastValueIndexEPNS1_4NodeE, .-_GLOBAL__sub_I__ZN2v88internal8compiler14NodeProperties14PastValueIndexEPNS1_4NodeE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler14NodeProperties14PastValueIndexEPNS1_4NodeE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
