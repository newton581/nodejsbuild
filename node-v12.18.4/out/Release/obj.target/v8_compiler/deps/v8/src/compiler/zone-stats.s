	.file	"zone-stats.cc"
	.text
	.section	.text._ZN2v88internal8compiler9ZoneStats10StatsScope20GetMaxAllocatedBytesEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler9ZoneStats10StatsScope20GetMaxAllocatedBytesEv
	.type	_ZN2v88internal8compiler9ZoneStats10StatsScope20GetMaxAllocatedBytesEv, @function
_ZN2v88internal8compiler9ZoneStats10StatsScope20GetMaxAllocatedBytesEv:
.LFB4312:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %r9
	cmpq	%r9, %rcx
	je	.L10
	movq	24(%rdi), %r11
	leaq	16(%rdi), %r10
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L9:
	movq	(%rcx), %rsi
	xorl	%edx, %edx
	movq	40(%rsi), %r8
	testq	%r8, %r8
	je	.L3
	movq	16(%rsi), %rdx
	subq	$24, %rdx
	subq	%r8, %rdx
.L3:
	addq	(%rsi), %rax
	addq	%rdx, %rax
	testq	%r11, %r11
	je	.L4
	movq	%r10, %r8
	movq	%r11, %rdx
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L18:
	movq	%rdx, %r8
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L6
.L5:
	cmpq	%rsi, 32(%rdx)
	jnb	.L18
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L5
.L6:
	cmpq	%r8, %r10
	je	.L4
	cmpq	%rsi, 32(%r8)
	ja	.L4
	subq	40(%r8), %rax
.L4:
	addq	$8, %rcx
	cmpq	%rcx, %r9
	jne	.L9
	cmpq	%rax, 64(%rdi)
	cmovnb	64(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	xorl	%eax, %eax
	cmpq	%rax, 64(%rdi)
	cmovnb	64(%rdi), %rax
	ret
	.cfi_endproc
.LFE4312:
	.size	_ZN2v88internal8compiler9ZoneStats10StatsScope20GetMaxAllocatedBytesEv, .-_ZN2v88internal8compiler9ZoneStats10StatsScope20GetMaxAllocatedBytesEv
	.section	.text._ZN2v88internal8compiler9ZoneStats10StatsScope24GetCurrentAllocatedBytesEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler9ZoneStats10StatsScope24GetCurrentAllocatedBytesEv
	.type	_ZN2v88internal8compiler9ZoneStats10StatsScope24GetCurrentAllocatedBytesEv, @function
_ZN2v88internal8compiler9ZoneStats10StatsScope24GetCurrentAllocatedBytesEv:
.LFB4313:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	(%rax), %rsi
	movq	8(%rax), %r10
	cmpq	%r10, %rsi
	je	.L28
	movq	24(%rdi), %r9
	xorl	%r8d, %r8d
	addq	$16, %rdi
	.p2align 4,,10
	.p2align 3
.L27:
	movq	(%rsi), %rdx
	xorl	%eax, %eax
	movq	40(%rdx), %rcx
	testq	%rcx, %rcx
	je	.L21
	movq	16(%rdx), %rax
	subq	$24, %rax
	subq	%rcx, %rax
.L21:
	addq	(%rdx), %r8
	addq	%rax, %r8
	testq	%r9, %r9
	je	.L22
	movq	%rdi, %rcx
	movq	%r9, %rax
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L35:
	movq	%rax, %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L24
.L23:
	cmpq	%rdx, 32(%rax)
	jnb	.L35
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L23
.L24:
	cmpq	%rcx, %rdi
	je	.L22
	cmpq	%rdx, 32(%rcx)
	ja	.L22
	subq	40(%rcx), %r8
.L22:
	addq	$8, %rsi
	cmpq	%rsi, %r10
	jne	.L27
	movq	%r8, %rax
	ret
.L28:
	xorl	%r8d, %r8d
	movq	%r8, %rax
	ret
	.cfi_endproc
.LFE4313:
	.size	_ZN2v88internal8compiler9ZoneStats10StatsScope24GetCurrentAllocatedBytesEv, .-_ZN2v88internal8compiler9ZoneStats10StatsScope24GetCurrentAllocatedBytesEv
	.section	.text._ZN2v88internal8compiler9ZoneStats10StatsScope22GetTotalAllocatedBytesEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler9ZoneStats10StatsScope22GetTotalAllocatedBytesEv
	.type	_ZN2v88internal8compiler9ZoneStats10StatsScope22GetTotalAllocatedBytesEv, @function
_ZN2v88internal8compiler9ZoneStats10StatsScope22GetTotalAllocatedBytesEv:
.LFB4314:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %r9
	movq	56(%rax), %r10
	xorl	%eax, %eax
	cmpq	%r9, %rcx
	je	.L37
	.p2align 4,,10
	.p2align 3
.L38:
	movq	(%rcx), %rsi
	movq	40(%rsi), %r8
	testq	%r8, %r8
	je	.L39
	movq	16(%rsi), %rdx
	addq	$8, %rcx
	subq	$24, %rdx
	subq	%r8, %rdx
	addq	(%rsi), %rdx
	addq	%rdx, %rax
	cmpq	%rcx, %r9
	jne	.L38
.L37:
	movq	%r10, %rdx
	subq	56(%rdi), %rdx
	addq	%rdx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	addq	$8, %rcx
	addq	(%rsi), %rax
	cmpq	%rcx, %r9
	jne	.L38
	movq	%r10, %rdx
	subq	56(%rdi), %rdx
	addq	%rdx, %rax
	ret
	.cfi_endproc
.LFE4314:
	.size	_ZN2v88internal8compiler9ZoneStats10StatsScope22GetTotalAllocatedBytesEv, .-_ZN2v88internal8compiler9ZoneStats10StatsScope22GetTotalAllocatedBytesEv
	.section	.text._ZN2v88internal8compiler9ZoneStats10StatsScope12ZoneReturnedEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler9ZoneStats10StatsScope12ZoneReturnedEPNS0_4ZoneE
	.type	_ZN2v88internal8compiler9ZoneStats10StatsScope12ZoneReturnedEPNS0_4ZoneE, @function
_ZN2v88internal8compiler9ZoneStats10StatsScope12ZoneReturnedEPNS0_4ZoneE:
.LFB4315:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r10
	xorl	%ecx, %ecx
	leaq	16(%rdi), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rax
	movq	24(%rdi), %rdx
	movq	(%rax), %r8
	movq	8(%rax), %r11
	cmpq	%r11, %r8
	je	.L44
	.p2align 4,,10
	.p2align 3
.L51:
	movq	(%r8), %rdi
	xorl	%eax, %eax
	movq	40(%rdi), %r9
	testq	%r9, %r9
	je	.L45
	movq	16(%rdi), %rax
	subq	$24, %rax
	subq	%r9, %rax
.L45:
	addq	(%rdi), %rcx
	addq	%rax, %rcx
	testq	%rdx, %rdx
	je	.L46
	movq	%rsi, %r9
	movq	%rdx, %rax
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L68:
	movq	%rax, %r9
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L48
.L47:
	cmpq	%rdi, 32(%rax)
	jnb	.L68
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L47
.L48:
	cmpq	%r9, %rsi
	je	.L46
	cmpq	%rdi, 32(%r9)
	ja	.L46
	subq	40(%r9), %rcx
.L46:
	addq	$8, %r8
	cmpq	%r8, %r11
	jne	.L51
.L44:
	cmpq	%rcx, 64(%rbx)
	cmovnb	64(%rbx), %rcx
	movq	%rcx, 64(%rbx)
	testq	%rdx, %rdx
	je	.L43
	movq	%rsi, %rdi
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L69:
	movq	%rdx, %rdi
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L54
.L53:
	cmpq	%r10, 32(%rdx)
	jnb	.L69
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L53
.L54:
	cmpq	%rsi, %rdi
	je	.L43
	cmpq	%r10, 32(%rdi)
	ja	.L43
	call	_ZSt28_Rb_tree_rebalance_for_erasePSt18_Rb_tree_node_baseRS_@PLT
	movq	%rax, %rdi
	call	_ZdlPv@PLT
	subq	$1, 48(%rbx)
.L43:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4315:
	.size	_ZN2v88internal8compiler9ZoneStats10StatsScope12ZoneReturnedEPNS0_4ZoneE, .-_ZN2v88internal8compiler9ZoneStats10StatsScope12ZoneReturnedEPNS0_4ZoneE
	.section	.text._ZN2v88internal8compiler9ZoneStatsC2EPNS0_19AccountingAllocatorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler9ZoneStatsC2EPNS0_19AccountingAllocatorE
	.type	_ZN2v88internal8compiler9ZoneStatsC2EPNS0_19AccountingAllocatorE, @function
_ZN2v88internal8compiler9ZoneStatsC2EPNS0_19AccountingAllocatorE:
.LFB4335:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movq	%rsi, 64(%rdi)
	movups	%xmm0, (%rdi)
	movups	%xmm0, 16(%rdi)
	movups	%xmm0, 32(%rdi)
	movups	%xmm0, 48(%rdi)
	ret
	.cfi_endproc
.LFE4335:
	.size	_ZN2v88internal8compiler9ZoneStatsC2EPNS0_19AccountingAllocatorE, .-_ZN2v88internal8compiler9ZoneStatsC2EPNS0_19AccountingAllocatorE
	.globl	_ZN2v88internal8compiler9ZoneStatsC1EPNS0_19AccountingAllocatorE
	.set	_ZN2v88internal8compiler9ZoneStatsC1EPNS0_19AccountingAllocatorE,_ZN2v88internal8compiler9ZoneStatsC2EPNS0_19AccountingAllocatorE
	.section	.text._ZN2v88internal8compiler9ZoneStatsD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler9ZoneStatsD2Ev
	.type	_ZN2v88internal8compiler9ZoneStatsD2Ev, @function
_ZN2v88internal8compiler9ZoneStatsD2Ev:
.LFB4338:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L72
	call	_ZdlPv@PLT
.L72:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L71
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L71:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4338:
	.size	_ZN2v88internal8compiler9ZoneStatsD2Ev, .-_ZN2v88internal8compiler9ZoneStatsD2Ev
	.globl	_ZN2v88internal8compiler9ZoneStatsD1Ev
	.set	_ZN2v88internal8compiler9ZoneStatsD1Ev,_ZN2v88internal8compiler9ZoneStatsD2Ev
	.section	.text._ZNK2v88internal8compiler9ZoneStats20GetMaxAllocatedBytesEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler9ZoneStats20GetMaxAllocatedBytesEv
	.type	_ZNK2v88internal8compiler9ZoneStats20GetMaxAllocatedBytesEv, @function
_ZNK2v88internal8compiler9ZoneStats20GetMaxAllocatedBytesEv:
.LFB4340:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rcx
	movq	8(%rdi), %r9
	xorl	%eax, %eax
	cmpq	%r9, %rcx
	je	.L79
	.p2align 4,,10
	.p2align 3
.L80:
	movq	(%rcx), %rsi
	movq	40(%rsi), %r8
	testq	%r8, %r8
	je	.L81
	movq	16(%rsi), %rdx
	addq	$8, %rcx
	subq	$24, %rdx
	subq	%r8, %rdx
	addq	(%rsi), %rdx
	addq	%rdx, %rax
	cmpq	%rcx, %r9
	jne	.L80
.L79:
	cmpq	%rax, 48(%rdi)
	cmovnb	48(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L81:
	addq	$8, %rcx
	addq	(%rsi), %rax
	cmpq	%rcx, %r9
	jne	.L80
	cmpq	%rax, 48(%rdi)
	cmovnb	48(%rdi), %rax
	ret
	.cfi_endproc
.LFE4340:
	.size	_ZNK2v88internal8compiler9ZoneStats20GetMaxAllocatedBytesEv, .-_ZNK2v88internal8compiler9ZoneStats20GetMaxAllocatedBytesEv
	.section	.text._ZNK2v88internal8compiler9ZoneStats24GetCurrentAllocatedBytesEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler9ZoneStats24GetCurrentAllocatedBytesEv
	.type	_ZNK2v88internal8compiler9ZoneStats24GetCurrentAllocatedBytesEv, @function
_ZNK2v88internal8compiler9ZoneStats24GetCurrentAllocatedBytesEv:
.LFB4341:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movq	8(%rdi), %rdi
	xorl	%r8d, %r8d
	cmpq	%rdi, %rdx
	je	.L85
	.p2align 4,,10
	.p2align 3
.L87:
	movq	(%rdx), %rcx
	movq	40(%rcx), %rsi
	testq	%rsi, %rsi
	je	.L88
	movq	16(%rcx), %rax
	addq	$8, %rdx
	subq	$24, %rax
	subq	%rsi, %rax
	addq	(%rcx), %rax
	addq	%rax, %r8
	cmpq	%rdx, %rdi
	jne	.L87
.L85:
	movq	%r8, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L88:
	addq	$8, %rdx
	addq	(%rcx), %r8
	cmpq	%rdx, %rdi
	jne	.L87
	movq	%r8, %rax
	ret
	.cfi_endproc
.LFE4341:
	.size	_ZNK2v88internal8compiler9ZoneStats24GetCurrentAllocatedBytesEv, .-_ZNK2v88internal8compiler9ZoneStats24GetCurrentAllocatedBytesEv
	.section	.text._ZNK2v88internal8compiler9ZoneStats22GetTotalAllocatedBytesEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler9ZoneStats22GetTotalAllocatedBytesEv
	.type	_ZNK2v88internal8compiler9ZoneStats22GetTotalAllocatedBytesEv, @function
_ZNK2v88internal8compiler9ZoneStats22GetTotalAllocatedBytesEv:
.LFB4342:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movq	8(%rdi), %r8
	movq	56(%rdi), %r9
	cmpq	%r8, %rdx
	je	.L92
	xorl	%edi, %edi
	.p2align 4,,10
	.p2align 3
.L97:
	movq	(%rdx), %rcx
	movq	40(%rcx), %rsi
	testq	%rsi, %rsi
	je	.L94
	movq	16(%rcx), %rax
	addq	$8, %rdx
	subq	$24, %rax
	subq	%rsi, %rax
	addq	(%rcx), %rax
	addq	%rax, %rdi
	cmpq	%rdx, %r8
	jne	.L97
.L96:
	addq	%rdi, %r9
.L92:
	movq	%r9, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	addq	$8, %rdx
	addq	(%rcx), %rdi
	cmpq	%rdx, %r8
	jne	.L97
	jmp	.L96
	.cfi_endproc
.LFE4342:
	.size	_ZNK2v88internal8compiler9ZoneStats22GetTotalAllocatedBytesEv, .-_ZNK2v88internal8compiler9ZoneStats22GetTotalAllocatedBytesEv
	.section	.text._ZN2v88internal8compiler9ZoneStats10ReturnZoneEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler9ZoneStats10ReturnZoneEPNS0_4ZoneE
	.type	_ZN2v88internal8compiler9ZoneStats10ReturnZoneEPNS0_4ZoneE, @function
_ZN2v88internal8compiler9ZoneStats10ReturnZoneEPNS0_4ZoneE:
.LFB4344:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	movq	8(%r14), %rdx
	cmpq	%rdx, %rdi
	je	.L131
	movq	%rdi, %rcx
	xorl	%r9d, %r9d
	.p2align 4,,10
	.p2align 3
.L100:
	movq	(%rcx), %rsi
	movq	40(%rsi), %r8
	testq	%r8, %r8
	je	.L101
	movq	16(%rsi), %rax
	addq	$8, %rcx
	subq	$24, %rax
	subq	%r8, %rax
	addq	(%rsi), %rax
	addq	%rax, %r9
	cmpq	%rcx, %rdx
	jne	.L100
.L99:
	cmpq	%r9, 48(%r14)
	movq	24(%r14), %r15
	cmovnb	48(%r14), %r9
	movq	32(%r14), %rbx
	movq	%r9, 48(%r14)
	cmpq	%r15, %rbx
	je	.L104
	.p2align 4,,10
	.p2align 3
.L118:
	movq	(%r15), %r12
	xorl	%edx, %edx
	movq	(%r12), %rax
	leaq	16(%r12), %r8
	movq	(%rax), %rsi
	movq	8(%rax), %r9
	movq	24(%r12), %rax
	cmpq	%r9, %rsi
	je	.L105
	.p2align 4,,10
	.p2align 3
.L112:
	movq	(%rsi), %rdi
	xorl	%ecx, %ecx
	movq	40(%rdi), %r10
	testq	%r10, %r10
	je	.L106
	movq	16(%rdi), %rcx
	subq	$24, %rcx
	subq	%r10, %rcx
.L106:
	addq	(%rdi), %rdx
	addq	%rcx, %rdx
	testq	%rax, %rax
	je	.L107
	movq	%r8, %r10
	movq	%rax, %rcx
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L152:
	movq	%rcx, %r10
	movq	16(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L109
.L108:
	cmpq	%rdi, 32(%rcx)
	jnb	.L152
	movq	24(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.L108
.L109:
	cmpq	%r8, %r10
	je	.L107
	cmpq	%rdi, 32(%r10)
	ja	.L107
	subq	40(%r10), %rdx
.L107:
	addq	$8, %rsi
	cmpq	%rsi, %r9
	jne	.L112
.L105:
	cmpq	%rdx, 64(%r12)
	cmovnb	64(%r12), %rdx
	movq	%rdx, 64(%r12)
	testq	%rax, %rax
	je	.L113
	movq	%r8, %rdi
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L153:
	movq	%rax, %rdi
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L115
.L114:
	cmpq	%r13, 32(%rax)
	jnb	.L153
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L114
.L115:
	cmpq	%r8, %rdi
	je	.L113
	cmpq	%r13, 32(%rdi)
	ja	.L113
	movq	%r8, %rsi
	call	_ZSt28_Rb_tree_rebalance_for_erasePSt18_Rb_tree_node_baseRS_@PLT
	movq	%rax, %rdi
	call	_ZdlPv@PLT
	subq	$1, 48(%r12)
.L113:
	addq	$8, %r15
	cmpq	%r15, %rbx
	jne	.L118
	movq	(%r14), %rdi
	movq	8(%r14), %rdx
.L104:
	movq	%rdx, %rax
	subq	%rdi, %rax
	movq	%rax, %rcx
	sarq	$5, %rax
	sarq	$3, %rcx
	testq	%rax, %rax
	jle	.L119
	salq	$5, %rax
	addq	%rdi, %rax
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L158:
	cmpq	8(%rdi), %r13
	je	.L154
	cmpq	16(%rdi), %r13
	je	.L155
	cmpq	24(%rdi), %r13
	je	.L156
	addq	$32, %rdi
	cmpq	%rax, %rdi
	je	.L157
.L124:
	leaq	8(%rdi), %rsi
	cmpq	(%rdi), %r13
	jne	.L158
.L120:
	cmpq	%rdx, %rsi
	je	.L129
	subq	%rsi, %rdx
	call	memmove@PLT
	movq	8(%r14), %rdx
.L129:
	subq	$8, %rdx
	movq	%rdx, 8(%r14)
	movq	40(%r13), %rcx
	xorl	%edx, %edx
	testq	%rcx, %rcx
	je	.L130
	movq	16(%r13), %rax
	subq	$24, %rax
	subq	%rcx, %rax
	movq	%rax, %rdx
.L130:
	movq	0(%r13), %rax
	addq	56(%r14), %rax
	movq	%r13, %rdi
	addq	%rdx, %rax
	movq	%rax, 56(%r14)
	call	_ZN2v88internal4ZoneD1Ev@PLT
	addq	$8, %rsp
	movq	%r13, %rdi
	movl	$64, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L101:
	.cfi_restore_state
	addq	$8, %rcx
	addq	(%rsi), %r9
	cmpq	%rcx, %rdx
	jne	.L100
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L157:
	movq	%rdx, %rcx
	subq	%rdi, %rcx
	sarq	$3, %rcx
.L119:
	cmpq	$2, %rcx
	je	.L134
	cmpq	$3, %rcx
	je	.L126
	cmpq	$1, %rcx
	je	.L127
.L128:
	leaq	8(%rdx), %rsi
	movq	%rdx, %rdi
	jmp	.L120
.L134:
	movq	%rdi, %rsi
.L125:
	leaq	8(%rsi), %rdi
	cmpq	(%rsi), %r13
	je	.L135
.L127:
	leaq	8(%rdi), %rsi
	cmpq	(%rdi), %r13
	jne	.L128
	jmp	.L120
.L126:
	leaq	8(%rdi), %rsi
	cmpq	(%rdi), %r13
	jne	.L125
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L154:
	leaq	16(%rdi), %rax
	movq	%rsi, %rdi
	movq	%rax, %rsi
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L155:
	leaq	24(%rdi), %rsi
	addq	$16, %rdi
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L156:
	leaq	32(%rdi), %rsi
	addq	$24, %rdi
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L131:
	xorl	%r9d, %r9d
	jmp	.L99
.L135:
	movq	%rdi, %rax
	movq	%rsi, %rdi
	movq	%rax, %rsi
	jmp	.L120
	.cfi_endproc
.LFE4344:
	.size	_ZN2v88internal8compiler9ZoneStats10ReturnZoneEPNS0_4ZoneE, .-_ZN2v88internal8compiler9ZoneStats10ReturnZoneEPNS0_4ZoneE
	.section	.text._ZNSt8_Rb_treeIPN2v88internal4ZoneESt4pairIKS3_mESt10_Select1stIS6_ESt4lessIS3_ESaIS6_EE8_M_eraseEPSt13_Rb_tree_nodeIS6_E,"axG",@progbits,_ZNSt8_Rb_treeIPN2v88internal4ZoneESt4pairIKS3_mESt10_Select1stIS6_ESt4lessIS3_ESaIS6_EE8_M_eraseEPSt13_Rb_tree_nodeIS6_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPN2v88internal4ZoneESt4pairIKS3_mESt10_Select1stIS6_ESt4lessIS3_ESaIS6_EE8_M_eraseEPSt13_Rb_tree_nodeIS6_E
	.type	_ZNSt8_Rb_treeIPN2v88internal4ZoneESt4pairIKS3_mESt10_Select1stIS6_ESt4lessIS3_ESaIS6_EE8_M_eraseEPSt13_Rb_tree_nodeIS6_E, @function
_ZNSt8_Rb_treeIPN2v88internal4ZoneESt4pairIKS3_mESt10_Select1stIS6_ESt4lessIS3_ESaIS6_EE8_M_eraseEPSt13_Rb_tree_nodeIS6_E:
.LFB4807:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L167
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L161:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal4ZoneESt4pairIKS3_mESt10_Select1stIS6_ESt4lessIS3_ESaIS6_EE8_M_eraseEPSt13_Rb_tree_nodeIS6_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L161
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L167:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE4807:
	.size	_ZNSt8_Rb_treeIPN2v88internal4ZoneESt4pairIKS3_mESt10_Select1stIS6_ESt4lessIS3_ESaIS6_EE8_M_eraseEPSt13_Rb_tree_nodeIS6_E, .-_ZNSt8_Rb_treeIPN2v88internal4ZoneESt4pairIKS3_mESt10_Select1stIS6_ESt4lessIS3_ESaIS6_EE8_M_eraseEPSt13_Rb_tree_nodeIS6_E
	.section	.text._ZN2v88internal8compiler9ZoneStats10StatsScopeD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler9ZoneStats10StatsScopeD2Ev
	.type	_ZN2v88internal8compiler9ZoneStats10StatsScopeD2Ev, @function
_ZN2v88internal8compiler9ZoneStats10StatsScopeD2Ev:
.LFB4310:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	subq	$8, 32(%rax)
	movq	24(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L170
	leaq	8(%rdi), %r12
.L172:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal4ZoneESt4pairIKS3_mESt10_Select1stIS6_ESt4lessIS3_ESaIS6_EE8_M_eraseEPSt13_Rb_tree_nodeIS6_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L172
.L170:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4310:
	.size	_ZN2v88internal8compiler9ZoneStats10StatsScopeD2Ev, .-_ZN2v88internal8compiler9ZoneStats10StatsScopeD2Ev
	.globl	_ZN2v88internal8compiler9ZoneStats10StatsScopeD1Ev
	.set	_ZN2v88internal8compiler9ZoneStats10StatsScopeD1Ev,_ZN2v88internal8compiler9ZoneStats10StatsScopeD2Ev
	.section	.rodata._ZNSt6vectorIPN2v88internal4ZoneESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_.str1.1,"aMS",@progbits,1
.LC0:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIPN2v88internal4ZoneESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPN2v88internal4ZoneESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal4ZoneESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.type	_ZNSt6vectorIPN2v88internal4ZoneESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, @function
_ZNSt6vectorIPN2v88internal4ZoneESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_:
.LFB4863:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L192
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L188
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L193
.L180:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L187:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L194
	testq	%r13, %r13
	jg	.L183
	testq	%r9, %r9
	jne	.L186
.L184:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L194:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L183
.L186:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L193:
	testq	%rsi, %rsi
	jne	.L181
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L187
	.p2align 4,,10
	.p2align 3
.L183:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L184
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L188:
	movl	$8, %r14d
	jmp	.L180
.L192:
	leaq	.LC0(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L181:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L180
	.cfi_endproc
.LFE4863:
	.size	_ZNSt6vectorIPN2v88internal4ZoneESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, .-_ZNSt6vectorIPN2v88internal4ZoneESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.section	.text._ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc
	.type	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc, @function
_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc:
.LFB4343:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movl	$64, %edi
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	64(%r12), %rsi
	movq	%r13, %rdx
	movq	%rax, %rbx
	movq	%rax, %rdi
	call	_ZN2v88internal4ZoneC1EPNS0_19AccountingAllocatorEPKc@PLT
	movq	%rbx, -48(%rbp)
	movq	8(%r12), %rsi
	cmpq	16(%r12), %rsi
	je	.L196
	movq	%rbx, (%rsi)
	addq	$8, 8(%r12)
.L197:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	movq	-48(%rbp), %rax
	jne	.L200
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L196:
	.cfi_restore_state
	leaq	-48(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIPN2v88internal4ZoneESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	jmp	.L197
.L200:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4343:
	.size	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc, .-_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc
	.section	.text._ZNSt6vectorIPN2v88internal8compiler9ZoneStats10StatsScopeESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPN2v88internal8compiler9ZoneStats10StatsScopeESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal8compiler9ZoneStats10StatsScopeESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.type	_ZNSt6vectorIPN2v88internal8compiler9ZoneStats10StatsScopeESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, @function
_ZNSt6vectorIPN2v88internal8compiler9ZoneStats10StatsScopeESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_:
.LFB4919:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L215
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L211
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L216
.L203:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L210:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L217
	testq	%r13, %r13
	jg	.L206
	testq	%r9, %r9
	jne	.L209
.L207:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L217:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L206
.L209:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L216:
	testq	%rsi, %rsi
	jne	.L204
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L206:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L207
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L211:
	movl	$8, %r14d
	jmp	.L203
.L215:
	leaq	.LC0(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L204:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L203
	.cfi_endproc
.LFE4919:
	.size	_ZNSt6vectorIPN2v88internal8compiler9ZoneStats10StatsScopeESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, .-_ZNSt6vectorIPN2v88internal8compiler9ZoneStats10StatsScopeESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.section	.text._ZN2v88internal8compiler9ZoneStats10StatsScopeC2EPS2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler9ZoneStats10StatsScopeC2EPS2_
	.type	_ZN2v88internal8compiler9ZoneStats10StatsScopeC2EPS2_, @function
_ZN2v88internal8compiler9ZoneStats10StatsScopeC2EPS2_:
.LFB4307:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	16(%rdi), %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, 16(%rdi)
	movq	$0, 24(%rdi)
	movq	%r14, 32(%rdi)
	movq	%r14, 40(%rdi)
	movq	$0, 48(%rdi)
	movq	(%rsi), %rdx
	movq	8(%rsi), %r9
	movq	56(%rsi), %r10
	movq	%rsi, (%rdi)
	cmpq	%rdx, %r9
	je	.L219
	xorl	%r8d, %r8d
	.p2align 4,,10
	.p2align 3
.L223:
	movq	(%rdx), %rcx
	movq	40(%rcx), %rdi
	testq	%rdi, %rdi
	je	.L220
	movq	16(%rcx), %rax
	addq	$8, %rdx
	subq	$24, %rax
	subq	%rdi, %rax
	addq	(%rcx), %rax
	addq	%rax, %r8
	cmpq	%rdx, %r9
	jne	.L223
.L222:
	addq	%r8, %r10
.L219:
	movq	%r10, 56(%rbx)
	movq	32(%rsi), %r8
	movq	$0, 64(%rbx)
	movq	%rbx, -64(%rbp)
	cmpq	40(%rsi), %r8
	je	.L224
	movq	%rbx, (%r8)
	addq	$8, 32(%rsi)
.L225:
	movq	(%rbx), %rax
	movq	8(%rax), %rdx
	movq	(%rax), %r13
	movq	%rdx, -72(%rbp)
	cmpq	%rdx, %r13
	je	.L218
	.p2align 4,,10
	.p2align 3
.L237:
	movq	0(%r13), %r15
	xorl	%eax, %eax
	movq	40(%r15), %rcx
	testq	%rcx, %rcx
	je	.L227
	movq	16(%r15), %rax
	subq	$24, %rax
	subq	%rcx, %rax
.L227:
	addq	(%r15), %rax
	movl	$48, %edi
	movq	%rax, %r12
	call	_Znwm@PLT
	movq	%r12, 40(%rax)
	movq	24(%rbx), %r12
	movq	%rax, %rsi
	movq	%r15, 32(%rax)
	testq	%r12, %r12
	jne	.L229
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L252:
	movq	16(%r12), %rax
	movl	$1, %ecx
	testq	%rax, %rax
	je	.L230
.L253:
	movq	%rax, %r12
.L229:
	movq	32(%r12), %rdi
	cmpq	%r15, %rdi
	ja	.L252
	movq	24(%r12), %rax
	xorl	%ecx, %ecx
	testq	%rax, %rax
	jne	.L253
.L230:
	testb	%cl, %cl
	jne	.L254
	cmpq	%r15, %rdi
	jb	.L239
.L240:
	movq	%rsi, %rdi
	call	_ZdlPv@PLT
.L236:
	addq	$8, %r13
	cmpq	%r13, -72(%rbp)
	jne	.L237
.L218:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L255
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L254:
	.cfi_restore_state
	cmpq	%r12, 32(%rbx)
	je	.L239
.L241:
	movq	%r12, %rdi
	movq	%rsi, -80(%rbp)
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	-80(%rbp), %rsi
	cmpq	32(%rax), %r15
	jbe	.L240
.L239:
	movl	$1, %edi
	cmpq	%r12, %r14
	jne	.L256
.L235:
	movq	%r14, %rcx
	movq	%r12, %rdx
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 48(%rbx)
	jmp	.L236
	.p2align 4,,10
	.p2align 3
.L220:
	addq	$8, %rdx
	addq	(%rcx), %r8
	cmpq	%rdx, %r9
	jne	.L223
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L256:
	xorl	%edi, %edi
	cmpq	32(%r12), %r15
	setb	%dil
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L251:
	movq	%r14, %r12
	cmpq	32(%rbx), %r14
	jne	.L241
	movl	$1, %edi
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L224:
	leaq	24(%rsi), %rdi
	leaq	-64(%rbp), %rdx
	movq	%r8, %rsi
	call	_ZNSt6vectorIPN2v88internal8compiler9ZoneStats10StatsScopeESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	jmp	.L225
.L255:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4307:
	.size	_ZN2v88internal8compiler9ZoneStats10StatsScopeC2EPS2_, .-_ZN2v88internal8compiler9ZoneStats10StatsScopeC2EPS2_
	.globl	_ZN2v88internal8compiler9ZoneStats10StatsScopeC1EPS2_
	.set	_ZN2v88internal8compiler9ZoneStats10StatsScopeC1EPS2_,_ZN2v88internal8compiler9ZoneStats10StatsScopeC2EPS2_
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
