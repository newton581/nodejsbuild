	.file	"osr.cc"
	.text
	.section	.text._ZN2v88internal8compiler9OsrHelperC2EPNS0_24OptimizedCompilationInfoE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler9OsrHelperC2EPNS0_24OptimizedCompilationInfoE
	.type	_ZN2v88internal8compiler9OsrHelperC2EPNS0_24OptimizedCompilationInfoE, @function
_ZN2v88internal8compiler9OsrHelperC2EPNS0_24OptimizedCompilationInfoE:
.LFB18178:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rsi), %rax
	movq	(%rax), %rdx
	movl	43(%rdx), %eax
	sarl	$3, %eax
	cltq
	movq	%rax, (%rdi)
	movl	39(%rdx), %eax
	testl	%eax, %eax
	leal	7(%rax), %edi
	cmovns	%eax, %edi
	sarl	$3, %edi
	call	_ZN2v88internal25InterpreterFrameConstants22RegisterStackSlotCountEi@PLT
	addl	$2, %eax
	cltq
	movq	%rax, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18178:
	.size	_ZN2v88internal8compiler9OsrHelperC2EPNS0_24OptimizedCompilationInfoE, .-_ZN2v88internal8compiler9OsrHelperC2EPNS0_24OptimizedCompilationInfoE
	.globl	_ZN2v88internal8compiler9OsrHelperC1EPNS0_24OptimizedCompilationInfoE
	.set	_ZN2v88internal8compiler9OsrHelperC1EPNS0_24OptimizedCompilationInfoE,_ZN2v88internal8compiler9OsrHelperC2EPNS0_24OptimizedCompilationInfoE
	.section	.text._ZN2v88internal8compiler9OsrHelper10SetupFrameEPNS1_5FrameE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler9OsrHelper10SetupFrameEPNS1_5FrameE
	.type	_ZN2v88internal8compiler9OsrHelper10SetupFrameEPNS1_5FrameE, @function
_ZN2v88internal8compiler9OsrHelper10SetupFrameEPNS1_5FrameE:
.LFB18180:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	addl	%eax, 8(%rsi)
	addl	%eax, 4(%rsi)
	ret
	.cfi_endproc
.LFE18180:
	.size	_ZN2v88internal8compiler9OsrHelper10SetupFrameEPNS1_5FrameE, .-_ZN2v88internal8compiler9OsrHelper10SetupFrameEPNS1_5FrameE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler9OsrHelperC2EPNS0_24OptimizedCompilationInfoE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler9OsrHelperC2EPNS0_24OptimizedCompilationInfoE, @function
_GLOBAL__sub_I__ZN2v88internal8compiler9OsrHelperC2EPNS0_24OptimizedCompilationInfoE:
.LFB21885:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE21885:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler9OsrHelperC2EPNS0_24OptimizedCompilationInfoE, .-_GLOBAL__sub_I__ZN2v88internal8compiler9OsrHelperC2EPNS0_24OptimizedCompilationInfoE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler9OsrHelperC2EPNS0_24OptimizedCompilationInfoE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
