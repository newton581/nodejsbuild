	.file	"frame.cc"
	.text
	.section	.text._ZN2v88internal8compiler5FrameC2Ei,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler5FrameC2Ei
	.type	_ZN2v88internal8compiler5FrameC2Ei, @function
_ZN2v88internal8compiler5FrameC2Ei:
.LFB11422:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movl	%esi, (%rdi)
	movl	%esi, 4(%rdi)
	movq	$0, 8(%rdi)
	movups	%xmm0, 16(%rdi)
	ret
	.cfi_endproc
.LFE11422:
	.size	_ZN2v88internal8compiler5FrameC2Ei, .-_ZN2v88internal8compiler5FrameC2Ei
	.globl	_ZN2v88internal8compiler5FrameC1Ei
	.set	_ZN2v88internal8compiler5FrameC1Ei,_ZN2v88internal8compiler5FrameC2Ei
	.section	.text._ZN2v88internal8compiler5Frame10AlignFrameEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler5Frame10AlignFrameEi
	.type	_ZN2v88internal8compiler5Frame10AlignFrameEi, @function
_ZN2v88internal8compiler5Frame10AlignFrameEi:
.LFB11424:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	leal	7(%rsi), %esi
	testl	%eax, %eax
	cmovns	%eax, %esi
	movl	12(%rdi), %eax
	sarl	$3, %esi
	leal	-1(%rsi), %edx
	movl	%esi, %ecx
	andl	%edx, %eax
	subl	%eax, %ecx
	movl	4(%rdi), %eax
	cmpl	%ecx, %esi
	je	.L4
	addl	%ecx, %eax
	movl	%eax, 4(%rdi)
.L4:
	andl	%eax, %edx
	movl	%esi, %r8d
	subl	%edx, %r8d
	cmpl	%r8d, %esi
	je	.L3
	addl	%r8d, %eax
	movl	%eax, 4(%rdi)
	movl	8(%rdi), %eax
	testl	%eax, %eax
	je	.L3
	addl	%r8d, %eax
	movl	%eax, 8(%rdi)
.L3:
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE11424:
	.size	_ZN2v88internal8compiler5Frame10AlignFrameEi, .-_ZN2v88internal8compiler5Frame10AlignFrameEi
	.section	.text._ZN2v88internal8compiler16FrameAccessState12MarkHasFrameEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16FrameAccessState12MarkHasFrameEb
	.type	_ZN2v88internal8compiler16FrameAccessState12MarkHasFrameEb, @function
_ZN2v88internal8compiler16FrameAccessState12MarkHasFrameEb:
.LFB11425:
	.cfi_startproc
	endbr64
	movb	%sil, 16(%rdi)
	movl	%esi, %eax
	testb	%sil, %sil
	je	.L12
	movzbl	_ZN2v88internal26FLAG_turbo_sp_frame_accessE(%rip), %eax
	xorl	$1, %eax
.L12:
	movb	%al, 8(%rdi)
	ret
	.cfi_endproc
.LFE11425:
	.size	_ZN2v88internal8compiler16FrameAccessState12MarkHasFrameEb, .-_ZN2v88internal8compiler16FrameAccessState12MarkHasFrameEb
	.section	.text._ZN2v88internal8compiler16FrameAccessState23SetFrameAccessToDefaultEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16FrameAccessState23SetFrameAccessToDefaultEv
	.type	_ZN2v88internal8compiler16FrameAccessState23SetFrameAccessToDefaultEv, @function
_ZN2v88internal8compiler16FrameAccessState23SetFrameAccessToDefaultEv:
.LFB11426:
	.cfi_startproc
	endbr64
	movzbl	16(%rdi), %eax
	testb	%al, %al
	je	.L14
	movzbl	_ZN2v88internal26FLAG_turbo_sp_frame_accessE(%rip), %eax
	xorl	$1, %eax
.L14:
	movb	%al, 8(%rdi)
	ret
	.cfi_endproc
.LFE11426:
	.size	_ZN2v88internal8compiler16FrameAccessState23SetFrameAccessToDefaultEv, .-_ZN2v88internal8compiler16FrameAccessState23SetFrameAccessToDefaultEv
	.section	.text._ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi
	.type	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi, @function
_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi:
.LFB11427:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	subl	%esi, %eax
	sall	$3, %eax
	cmpb	$0, 8(%rdi)
	jne	.L20
	cmpb	$0, 16(%rdi)
	movl	$-1, %edx
	je	.L21
	movq	(%rdi), %rdx
	movl	4(%rdx), %edx
	subl	$2, %edx
.L21:
	addl	12(%rdi), %edx
	leal	(%rax,%rdx,8), %eax
	orl	$1, %eax
.L20:
	ret
	.cfi_endproc
.LFE11427:
	.size	_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi, .-_ZNK2v88internal8compiler16FrameAccessState14GetFrameOffsetEi
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler5FrameC2Ei,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler5FrameC2Ei, @function
_GLOBAL__sub_I__ZN2v88internal8compiler5FrameC2Ei:
.LFB13482:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE13482:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler5FrameC2Ei, .-_GLOBAL__sub_I__ZN2v88internal8compiler5FrameC2Ei
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler5FrameC2Ei
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
