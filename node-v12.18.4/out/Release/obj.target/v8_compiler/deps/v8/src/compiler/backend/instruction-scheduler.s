	.file	"instruction-scheduler.cc"
	.text
	.section	.text._ZN2v88internal8compiler20InstructionScheduler19SchedulingQueueBase7AddNodeEPNS2_17ScheduleGraphNodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20InstructionScheduler19SchedulingQueueBase7AddNodeEPNS2_17ScheduleGraphNodeE
	.type	_ZN2v88internal8compiler20InstructionScheduler19SchedulingQueueBase7AddNodeEPNS2_17ScheduleGraphNodeE, @function
_ZN2v88internal8compiler20InstructionScheduler19SchedulingQueueBase7AddNodeEPNS2_17ScheduleGraphNodeE:
.LFB10753:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	16(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %rax
	cmpq	%r12, %rax
	je	.L2
	movl	112(%rsi), %ecx
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L10:
	movq	(%rax), %rax
	cmpq	%r12, %rax
	je	.L2
.L3:
	movq	16(%rax), %rdx
	cmpl	%ecx, 112(%rdx)
	jge	.L10
	movq	%rax, %r12
.L2:
	movq	8(%rbx), %r8
	movq	16(%r8), %rdi
	movq	24(%r8), %rax
	subq	%rdi, %rax
	cmpq	$23, %rax
	jbe	.L11
	leaq	24(%rdi), %rax
	movq	%rax, 16(%r8)
.L5:
	movq	%r13, 16(%rdi)
	movq	%r12, %rsi
	call	_ZNSt8__detail15_List_node_base7_M_hookEPS0_@PLT
	addq	$1, 32(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore_state
	movq	%r8, %rdi
	movl	$24, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdi
	jmp	.L5
	.cfi_endproc
.LFE10753:
	.size	_ZN2v88internal8compiler20InstructionScheduler19SchedulingQueueBase7AddNodeEPNS2_17ScheduleGraphNodeE, .-_ZN2v88internal8compiler20InstructionScheduler19SchedulingQueueBase7AddNodeEPNS2_17ScheduleGraphNodeE
	.section	.text._ZN2v88internal8compiler20InstructionScheduler22CriticalPathFirstQueue16PopBestCandidateEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20InstructionScheduler22CriticalPathFirstQueue16PopBestCandidateEi
	.type	_ZN2v88internal8compiler20InstructionScheduler22CriticalPathFirstQueue16PopBestCandidateEi, @function
_ZN2v88internal8compiler20InstructionScheduler22CriticalPathFirstQueue16PopBestCandidateEi:
.LFB10754:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rdx
	leaq	16(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	16(%rdi), %rdi
	cmpq	%rdi, %rax
	jne	.L15
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L19:
	movq	(%rdi), %rdi
	cmpq	%rdi, %rax
	je	.L16
.L15:
	movq	16(%rdi), %r12
	cmpl	116(%r12), %esi
	jl	.L19
	subq	$1, 32(%rdx)
	call	_ZNSt8__detail15_List_node_base9_M_unhookEv@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	.cfi_restore_state
	xorl	%r12d, %r12d
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10754:
	.size	_ZN2v88internal8compiler20InstructionScheduler22CriticalPathFirstQueue16PopBestCandidateEi, .-_ZN2v88internal8compiler20InstructionScheduler22CriticalPathFirstQueue16PopBestCandidateEi
	.section	.text._ZN2v88internal8compiler20InstructionScheduler20StressSchedulerQueue16PopBestCandidateEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20InstructionScheduler20StressSchedulerQueue16PopBestCandidateEi
	.type	_ZN2v88internal8compiler20InstructionScheduler20StressSchedulerQueue16PopBestCandidateEi, @function
_ZN2v88internal8compiler20InstructionScheduler20StressSchedulerQueue16PopBestCandidateEi:
.LFB10755:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rax
	movq	16(%rdi), %r12
	movq	8(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal7Isolate23random_number_generatorEv@PLT
	movl	32(%rbx), %esi
	movq	%rax, %rdi
	call	_ZN2v84base21RandomNumberGenerator7NextIntEi@PLT
	cltq
	testq	%rax, %rax
	jle	.L21
	subq	$1, %rax
	.p2align 4,,10
	.p2align 3
.L22:
	subq	$1, %rax
	movq	(%r12), %r12
	cmpq	$-1, %rax
	jne	.L22
.L23:
	movq	16(%r12), %r13
	subq	$1, 32(%rbx)
	movq	%r12, %rdi
	call	_ZNSt8__detail15_List_node_base9_M_unhookEv@PLT
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	.cfi_restore_state
	leaq	1(%rax), %rdx
	je	.L23
	.p2align 4,,10
	.p2align 3
.L24:
	addq	$1, %rdx
	movq	8(%r12), %r12
	cmpq	$1, %rdx
	jne	.L24
	jmp	.L23
	.cfi_endproc
.LFE10755:
	.size	_ZN2v88internal8compiler20InstructionScheduler20StressSchedulerQueue16PopBestCandidateEi, .-_ZN2v88internal8compiler20InstructionScheduler20StressSchedulerQueue16PopBestCandidateEi
	.section	.text._ZN2v88internal8compiler20InstructionScheduler17ScheduleGraphNodeC2EPNS0_4ZoneEPNS1_11InstructionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20InstructionScheduler17ScheduleGraphNodeC2EPNS0_4ZoneEPNS1_11InstructionE
	.type	_ZN2v88internal8compiler20InstructionScheduler17ScheduleGraphNodeC2EPNS0_4ZoneEPNS1_11InstructionE, @function
_ZN2v88internal8compiler20InstructionScheduler17ScheduleGraphNodeC2EPNS0_4ZoneEPNS1_11InstructionE:
.LFB10760:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm0
	movq	%rsi, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$8, %rsp
	movups	%xmm0, (%rbx)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 16(%rbx)
	movups	%xmm0, 40(%rbx)
	movups	%xmm0, 56(%rbx)
	movups	%xmm0, 72(%rbx)
	movups	%xmm0, 88(%rbx)
	movq	16(%rsi), %r12
	movq	24(%rsi), %rax
	movq	$8, 32(%rbx)
	subq	%r12, %rax
	cmpq	$63, %rax
	jbe	.L41
	leaq	64(%r12), %rax
	movq	%r12, 24(%rbx)
	addq	$24, %r12
	movq	%rax, 16(%rsi)
.L34:
	movq	8(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L35
	leaq	512(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L37:
	leaq	512(%rax), %rdx
	movq	%r12, 64(%rbx)
	movq	%rax, %xmm0
	movq	%r13, %rdi
	movq	%r12, 96(%rbx)
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, (%r12)
	movq	%rdx, 56(%rbx)
	movq	%rax, 80(%rbx)
	movq	%rdx, 88(%rbx)
	movq	%rax, 72(%rbx)
	movl	$0, 104(%rbx)
	movups	%xmm0, 40(%rbx)
	call	_ZN2v88internal8compiler20InstructionScheduler21GetInstructionLatencyEPKNS1_11InstructionE@PLT
	movq	$-1, 112(%rbx)
	movl	%eax, 108(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	.cfi_restore_state
	movl	$64, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	movq	32(%rbx), %rax
	movq	%rcx, 24(%rbx)
	leaq	-4(,%rax,4), %r12
	movq	16(%rbx), %rax
	andq	$-8, %r12
	addq	%rcx, %r12
	testq	%rax, %rax
	je	.L34
	cmpq	$63, 8(%rax)
	jbe	.L34
	movq	(%rax), %rdx
	movq	%rdx, 16(%rbx)
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L35:
	movl	$512, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L37
	.cfi_endproc
.LFE10760:
	.size	_ZN2v88internal8compiler20InstructionScheduler17ScheduleGraphNodeC2EPNS0_4ZoneEPNS1_11InstructionE, .-_ZN2v88internal8compiler20InstructionScheduler17ScheduleGraphNodeC2EPNS0_4ZoneEPNS1_11InstructionE
	.globl	_ZN2v88internal8compiler20InstructionScheduler17ScheduleGraphNodeC1EPNS0_4ZoneEPNS1_11InstructionE
	.set	_ZN2v88internal8compiler20InstructionScheduler17ScheduleGraphNodeC1EPNS0_4ZoneEPNS1_11InstructionE,_ZN2v88internal8compiler20InstructionScheduler17ScheduleGraphNodeC2EPNS0_4ZoneEPNS1_11InstructionE
	.section	.text._ZN2v88internal8compiler20InstructionSchedulerC2EPNS0_4ZoneEPNS1_19InstructionSequenceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20InstructionSchedulerC2EPNS0_4ZoneEPNS1_19InstructionSequenceE
	.type	_ZN2v88internal8compiler20InstructionSchedulerC2EPNS0_4ZoneEPNS1_19InstructionSequenceE, @function
_ZN2v88internal8compiler20InstructionSchedulerC2EPNS0_4ZoneEPNS1_19InstructionSequenceE:
.LFB10773:
	.cfi_startproc
	endbr64
	movq	%rsi, %xmm1
	movq	%rdx, %xmm2
	leaq	120(%rdi), %rax
	movq	%rsi, 16(%rdi)
	punpcklqdq	%xmm2, %xmm1
	movq	$0, 24(%rdi)
	movups	%xmm1, (%rdi)
	pxor	%xmm1, %xmm1
	movq	$0, 48(%rdi)
	movq	%rsi, 56(%rdi)
	movq	$0, 96(%rdi)
	movq	%rsi, 104(%rdi)
	movl	$0, 120(%rdi)
	movq	$0, 128(%rdi)
	movq	%rax, 136(%rdi)
	movq	%rax, 144(%rdi)
	movq	$0, 152(%rdi)
	movups	%xmm1, 32(%rdi)
	movups	%xmm1, 64(%rdi)
	movups	%xmm1, 80(%rdi)
	ret
	.cfi_endproc
.LFE10773:
	.size	_ZN2v88internal8compiler20InstructionSchedulerC2EPNS0_4ZoneEPNS1_19InstructionSequenceE, .-_ZN2v88internal8compiler20InstructionSchedulerC2EPNS0_4ZoneEPNS1_19InstructionSequenceE
	.globl	_ZN2v88internal8compiler20InstructionSchedulerC1EPNS0_4ZoneEPNS1_19InstructionSequenceE
	.set	_ZN2v88internal8compiler20InstructionSchedulerC1EPNS0_4ZoneEPNS1_19InstructionSequenceE,_ZN2v88internal8compiler20InstructionSchedulerC2EPNS0_4ZoneEPNS1_19InstructionSequenceE
	.section	.text._ZN2v88internal8compiler20InstructionScheduler10StartBlockENS1_9RpoNumberE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20InstructionScheduler10StartBlockENS1_9RpoNumberE
	.type	_ZN2v88internal8compiler20InstructionScheduler10StartBlockENS1_9RpoNumberE, @function
_ZN2v88internal8compiler20InstructionScheduler10StartBlockENS1_9RpoNumberE:
.LFB10775:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	jmp	_ZN2v88internal8compiler19InstructionSequence10StartBlockENS1_9RpoNumberE@PLT
	.cfi_endproc
.LFE10775:
	.size	_ZN2v88internal8compiler20InstructionScheduler10StartBlockENS1_9RpoNumberE, .-_ZN2v88internal8compiler20InstructionScheduler10StartBlockENS1_9RpoNumberE
	.section	.rodata._ZNK2v88internal8compiler20InstructionScheduler19GetInstructionFlagsEPKNS1_11InstructionE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZNK2v88internal8compiler20InstructionScheduler19GetInstructionFlagsEPKNS1_11InstructionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler20InstructionScheduler19GetInstructionFlagsEPKNS1_11InstructionE
	.type	_ZNK2v88internal8compiler20InstructionScheduler19GetInstructionFlagsEPKNS1_11InstructionE, @function
_ZNK2v88internal8compiler20InstructionScheduler19GetInstructionFlagsEPKNS1_11InstructionE:
.LFB10780:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	andl	$511, %eax
	cmpl	$94, %eax
	ja	.L45
	subl	$13, %eax
	cmpl	$81, %eax
	ja	.L51
	leaq	.L48(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZNK2v88internal8compiler20InstructionScheduler19GetInstructionFlagsEPKNS1_11InstructionE,"a",@progbits
	.align 4
	.align 4
.L48:
	.long	.L47-.L48
	.long	.L47-.L48
	.long	.L47-.L48
	.long	.L47-.L48
	.long	.L47-.L48
	.long	.L51-.L48
	.long	.L51-.L48
	.long	.L47-.L48
	.long	.L47-.L48
	.long	.L47-.L48
	.long	.L47-.L48
	.long	.L47-.L48
	.long	.L47-.L48
	.long	.L47-.L48
	.long	.L51-.L48
	.long	.L47-.L48
	.long	.L51-.L48
	.long	.L49-.L48
	.long	.L49-.L48
	.long	.L49-.L48
	.long	.L49-.L48
	.long	.L49-.L48
	.long	.L49-.L48
	.long	.L51-.L48
	.long	.L51-.L48
	.long	.L51-.L48
	.long	.L51-.L48
	.long	.L51-.L48
	.long	.L51-.L48
	.long	.L51-.L48
	.long	.L51-.L48
	.long	.L51-.L48
	.long	.L51-.L48
	.long	.L51-.L48
	.long	.L51-.L48
	.long	.L51-.L48
	.long	.L51-.L48
	.long	.L51-.L48
	.long	.L51-.L48
	.long	.L51-.L48
	.long	.L51-.L48
	.long	.L51-.L48
	.long	.L51-.L48
	.long	.L51-.L48
	.long	.L51-.L48
	.long	.L51-.L48
	.long	.L51-.L48
	.long	.L51-.L48
	.long	.L51-.L48
	.long	.L51-.L48
	.long	.L51-.L48
	.long	.L51-.L48
	.long	.L51-.L48
	.long	.L51-.L48
	.long	.L51-.L48
	.long	.L51-.L48
	.long	.L51-.L48
	.long	.L51-.L48
	.long	.L51-.L48
	.long	.L51-.L48
	.long	.L51-.L48
	.long	.L47-.L48
	.long	.L47-.L48
	.long	.L47-.L48
	.long	.L47-.L48
	.long	.L47-.L48
	.long	.L47-.L48
	.long	.L47-.L48
	.long	.L47-.L48
	.long	.L47-.L48
	.long	.L47-.L48
	.long	.L47-.L48
	.long	.L47-.L48
	.long	.L47-.L48
	.long	.L47-.L48
	.long	.L47-.L48
	.long	.L47-.L48
	.long	.L47-.L48
	.long	.L47-.L48
	.long	.L47-.L48
	.long	.L47-.L48
	.long	.L47-.L48
	.section	.text._ZNK2v88internal8compiler20InstructionScheduler19GetInstructionFlagsEPKNS1_11InstructionE
.L51:
	movl	$1, %eax
	ret
.L49:
	movl	$2, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	subl	$95, %eax
	cmpl	$357, %eax
	ja	.L50
	jmp	_ZNK2v88internal8compiler20InstructionScheduler25GetTargetInstructionFlagsEPKNS1_11InstructionE@PLT
.L47:
	xorl	%eax, %eax
	ret
.L50:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE10780:
	.size	_ZNK2v88internal8compiler20InstructionScheduler19GetInstructionFlagsEPKNS1_11InstructionE, .-_ZNK2v88internal8compiler20InstructionScheduler19GetInstructionFlagsEPKNS1_11InstructionE
	.section	.text._ZN2v88internal8compiler20InstructionScheduler21ComputeTotalLatenciesEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20InstructionScheduler21ComputeTotalLatenciesEv
	.type	_ZN2v88internal8compiler20InstructionScheduler21ComputeTotalLatenciesEv, @function
_ZN2v88internal8compiler20InstructionScheduler21ComputeTotalLatenciesEv:
.LFB10781:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %r10
	movq	24(%rdi), %r11
	cmpq	%r11, %r10
	je	.L54
	.p2align 4,,10
	.p2align 3
.L60:
	movq	-8(%r10), %r9
	xorl	%edx, %edx
	movq	40(%r9), %rax
	movq	72(%r9), %rdi
	movq	56(%r9), %rsi
	movq	64(%r9), %r8
	cmpq	%rax, %rdi
	je	.L57
	.p2align 4,,10
	.p2align 3
.L56:
	movq	(%rax), %rcx
	movl	112(%rcx), %ecx
	cmpl	%ecx, %edx
	cmovl	%ecx, %edx
	addq	$8, %rax
	cmpq	%rax, %rsi
	je	.L65
	cmpq	%rax, %rdi
	jne	.L56
.L57:
	addl	108(%r9), %edx
	subq	$8, %r10
	movl	%edx, 112(%r9)
	cmpq	%r10, %r11
	jne	.L60
.L54:
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	movq	8(%r8), %rax
	leaq	8(%r8), %rcx
	leaq	512(%rax), %rsi
	cmpq	%rax, %rdi
	je	.L57
	movq	%rcx, %r8
	jmp	.L56
	.cfi_endproc
.LFE10781:
	.size	_ZN2v88internal8compiler20InstructionScheduler21ComputeTotalLatenciesEv, .-_ZN2v88internal8compiler20InstructionScheduler21ComputeTotalLatenciesEv
	.section	.text._ZN2v88internal8compiler20InstructionScheduler13ScheduleBlockINS2_20StressSchedulerQueueEEEvv,"axG",@progbits,_ZN2v88internal8compiler20InstructionScheduler13ScheduleBlockINS2_20StressSchedulerQueueEEEvv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler20InstructionScheduler13ScheduleBlockINS2_20StressSchedulerQueueEEEvv
	.type	_ZN2v88internal8compiler20InstructionScheduler13ScheduleBlockINS2_20StressSchedulerQueueEEEvv, @function
_ZN2v88internal8compiler20InstructionScheduler13ScheduleBlockINS2_20StressSchedulerQueueEEEvv:
.LFB11698:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r14
	pushq	%r13
	movq	%r14, %xmm0
	pushq	%r12
	punpcklqdq	%xmm0, %xmm0
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	%rdi, -136(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%rdi, -96(%rbp)
	movq	$0, -64(%rbp)
	movq	%rax, -88(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler20InstructionScheduler21ComputeTotalLatenciesEv
	movq	24(%rbx), %rax
	movq	32(%rbx), %r15
	movq	-80(%rbp), %r12
	movq	%rax, -104(%rbp)
	movq	%rax, %rbx
	cmpq	%r15, %rax
	jne	.L74
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L69:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L75
.L74:
	movq	(%rbx), %r13
	movl	104(%r13), %eax
	testl	%eax, %eax
	jne	.L69
	cmpq	%r14, %r12
	je	.L70
	movl	112(%r13), %ecx
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L109:
	movq	(%r12), %r12
	cmpq	%r14, %r12
	je	.L70
.L71:
	movq	16(%r12), %rax
	cmpl	%ecx, 112(%rax)
	jge	.L109
.L70:
	movq	-88(%rbp), %r8
	movq	16(%r8), %rdi
	movq	24(%r8), %rax
	subq	%rdi, %rax
	cmpq	$23, %rax
	jbe	.L110
	leaq	24(%rdi), %rax
	movq	%rax, 16(%r8)
.L73:
	movq	%r13, 16(%rdi)
	movq	%r12, %rsi
	addq	$8, %rbx
	call	_ZNSt8__detail15_List_node_base7_M_hookEPS0_@PLT
	addq	$1, -64(%rbp)
	movq	-80(%rbp), %r12
	cmpq	%rbx, %r15
	jne	.L74
.L75:
	xorl	%r13d, %r13d
	cmpq	%r14, %r12
	je	.L66
	.p2align 4,,10
	.p2align 3
.L68:
	movq	-96(%rbp), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal7Isolate23random_number_generatorEv@PLT
	movl	-64(%rbp), %esi
	movq	%rax, %rdi
	call	_ZN2v84base21RandomNumberGenerator7NextIntEi@PLT
	cltq
	testq	%rax, %rax
	jle	.L76
	subq	$1, %rax
	.p2align 4,,10
	.p2align 3
.L77:
	subq	$1, %rax
	movq	(%r12), %r12
	cmpq	$-1, %rax
	jne	.L77
.L78:
	movq	16(%r12), %rcx
	movq	%r12, %rdi
	subq	$1, -64(%rbp)
	movq	%rcx, -104(%rbp)
	call	_ZNSt8__detail15_List_node_base9_M_unhookEv@PLT
	movq	-104(%rbp), %rcx
	testq	%rcx, %rcx
	jne	.L80
	movq	-80(%rbp), %r12
.L81:
	addl	$1, %r13d
	cmpq	%r14, %r12
	jne	.L68
.L66:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L111
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L76:
	.cfi_restore_state
	leaq	1(%rax), %rcx
	je	.L78
	.p2align 4,,10
	.p2align 3
.L79:
	addq	$1, %rcx
	movq	8(%r12), %r12
	cmpq	$1, %rcx
	jne	.L79
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L80:
	movq	-136(%rbp), %rax
	movq	(%rcx), %rsi
	movq	%rcx, -104(%rbp)
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler19InstructionSequence14AddInstructionEPNS1_11InstructionE@PLT
	movq	-104(%rbp), %rcx
	movq	-80(%rbp), %r12
	movq	40(%rcx), %rbx
	movq	56(%rcx), %r8
	movq	64(%rcx), %r10
	movq	72(%rcx), %r9
	.p2align 4,,10
	.p2align 3
.L88:
	cmpq	%rbx, %r9
	je	.L81
	movq	(%rbx), %r15
	movl	104(%r15), %eax
	movl	116(%r15), %edi
	subl	$1, %eax
	movl	%eax, 104(%r15)
	movl	108(%rcx), %esi
	addl	%r13d, %esi
	cmpl	%edi, %esi
	cmovl	%edi, %esi
	movl	%esi, 116(%r15)
	testl	%eax, %eax
	je	.L112
.L83:
	addq	$8, %rbx
	cmpq	%rbx, %r8
	jne	.L88
	movq	8(%r10), %rbx
	addq	$8, %r10
	leaq	512(%rbx), %r8
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L112:
	cmpq	%r14, %r12
	je	.L84
	movl	112(%r15), %esi
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L113:
	movq	(%r12), %r12
	cmpq	%r14, %r12
	je	.L84
.L85:
	movq	16(%r12), %rax
	cmpl	%esi, 112(%rax)
	jge	.L113
.L84:
	movq	-88(%rbp), %r11
	movq	16(%r11), %rdi
	movq	24(%r11), %rax
	subq	%rdi, %rax
	cmpq	$23, %rax
	jbe	.L114
	leaq	24(%rdi), %rax
	movq	%rax, 16(%r11)
.L87:
	movq	%r15, 16(%rdi)
	movq	%r12, %rsi
	movq	%rcx, -128(%rbp)
	movq	%r9, -120(%rbp)
	movq	%r8, -112(%rbp)
	movq	%r10, -104(%rbp)
	call	_ZNSt8__detail15_List_node_base7_M_hookEPS0_@PLT
	addq	$1, -64(%rbp)
	movq	-80(%rbp), %r12
	movq	-128(%rbp), %rcx
	movq	-120(%rbp), %r9
	movq	-112(%rbp), %r8
	movq	-104(%rbp), %r10
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L114:
	movq	%r11, %rdi
	movl	$24, %esi
	movq	%rcx, -128(%rbp)
	movq	%r9, -120(%rbp)
	movq	%r8, -112(%rbp)
	movq	%r10, -104(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-104(%rbp), %r10
	movq	-112(%rbp), %r8
	movq	-120(%rbp), %r9
	movq	-128(%rbp), %rcx
	movq	%rax, %rdi
	jmp	.L87
.L110:
	movq	%r8, %rdi
	movl	$24, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdi
	jmp	.L73
.L111:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11698:
	.size	_ZN2v88internal8compiler20InstructionScheduler13ScheduleBlockINS2_20StressSchedulerQueueEEEvv, .-_ZN2v88internal8compiler20InstructionScheduler13ScheduleBlockINS2_20StressSchedulerQueueEEEvv
	.section	.text._ZN2v88internal8compiler20InstructionScheduler13ScheduleBlockINS2_22CriticalPathFirstQueueEEEvv,"axG",@progbits,_ZN2v88internal8compiler20InstructionScheduler13ScheduleBlockINS2_22CriticalPathFirstQueueEEEvv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler20InstructionScheduler13ScheduleBlockINS2_22CriticalPathFirstQueueEEEvv
	.type	_ZN2v88internal8compiler20InstructionScheduler13ScheduleBlockINS2_22CriticalPathFirstQueueEEEvv, @function
_ZN2v88internal8compiler20InstructionScheduler13ScheduleBlockINS2_22CriticalPathFirstQueueEEEvv:
.LFB11702:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r14
	pushq	%r13
	movq	%r14, %xmm0
	pushq	%r12
	punpcklqdq	%xmm0, %xmm0
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	%rdi, -136(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%rdi, -96(%rbp)
	movq	$0, -64(%rbp)
	movq	%rax, -88(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler20InstructionScheduler21ComputeTotalLatenciesEv
	movq	24(%rbx), %rax
	movq	32(%rbx), %r15
	movq	-80(%rbp), %r12
	movq	%rax, -104(%rbp)
	movq	%rax, %rbx
	cmpq	%r15, %rax
	jne	.L124
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L119:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L125
.L124:
	movq	(%rbx), %r13
	movl	104(%r13), %eax
	testl	%eax, %eax
	jne	.L119
	cmpq	%r14, %r12
	je	.L120
	movl	112(%r13), %ecx
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L151:
	movq	(%r12), %r12
	cmpq	%r14, %r12
	je	.L120
.L121:
	movq	16(%r12), %rax
	cmpl	%ecx, 112(%rax)
	jge	.L151
.L120:
	movq	-88(%rbp), %r8
	movq	16(%r8), %rdi
	movq	24(%r8), %rax
	subq	%rdi, %rax
	cmpq	$23, %rax
	jbe	.L152
	leaq	24(%rdi), %rax
	movq	%rax, 16(%r8)
.L123:
	movq	%r13, 16(%rdi)
	movq	%r12, %rsi
	addq	$8, %rbx
	call	_ZNSt8__detail15_List_node_base7_M_hookEPS0_@PLT
	addq	$1, -64(%rbp)
	movq	-80(%rbp), %r12
	cmpq	%rbx, %r15
	jne	.L124
.L125:
	xorl	%ecx, %ecx
	cmpq	%r14, %r12
	je	.L115
	.p2align 4,,10
	.p2align 3
.L127:
	movq	16(%r12), %r15
	cmpl	116(%r15), %ecx
	jge	.L126
	movq	(%r12), %r12
	cmpq	%r14, %r12
	jne	.L127
	movq	-80(%rbp), %r12
.L128:
	addl	$1, %ecx
	cmpq	%r14, %r12
	jne	.L127
.L115:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L153
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L126:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	%ecx, -104(%rbp)
	subq	$1, -64(%rbp)
	call	_ZNSt8__detail15_List_node_base9_M_unhookEv@PLT
	movq	-136(%rbp), %rax
	movq	(%r15), %rsi
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler19InstructionSequence14AddInstructionEPNS1_11InstructionE@PLT
	movq	40(%r15), %rbx
	movq	56(%r15), %r8
	movq	64(%r15), %r10
	movq	72(%r15), %r9
	movq	-80(%rbp), %r12
	movl	-104(%rbp), %ecx
	.p2align 4,,10
	.p2align 3
.L135:
	cmpq	%rbx, %r9
	je	.L128
	movq	(%rbx), %r13
	movl	104(%r13), %eax
	movl	116(%r13), %edi
	subl	$1, %eax
	movl	%eax, 104(%r13)
	movl	108(%r15), %esi
	addl	%ecx, %esi
	cmpl	%edi, %esi
	cmovl	%edi, %esi
	movl	%esi, 116(%r13)
	testl	%eax, %eax
	je	.L154
.L130:
	addq	$8, %rbx
	cmpq	%rbx, %r8
	jne	.L135
	movq	8(%r10), %rbx
	addq	$8, %r10
	leaq	512(%rbx), %r8
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L154:
	cmpq	%r14, %r12
	je	.L131
	movl	112(%r13), %esi
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L155:
	movq	(%r12), %r12
	cmpq	%r14, %r12
	je	.L131
.L132:
	movq	16(%r12), %rax
	cmpl	%esi, 112(%rax)
	jge	.L155
.L131:
	movq	-88(%rbp), %r11
	movq	16(%r11), %rdi
	movq	24(%r11), %rax
	subq	%rdi, %rax
	cmpq	$23, %rax
	jbe	.L156
	leaq	24(%rdi), %rax
	movq	%rax, 16(%r11)
.L134:
	movq	%r13, 16(%rdi)
	movq	%r12, %rsi
	movq	%r9, -128(%rbp)
	movl	%ecx, -116(%rbp)
	movq	%r8, -112(%rbp)
	movq	%r10, -104(%rbp)
	call	_ZNSt8__detail15_List_node_base7_M_hookEPS0_@PLT
	addq	$1, -64(%rbp)
	movq	-80(%rbp), %r12
	movq	-128(%rbp), %r9
	movl	-116(%rbp), %ecx
	movq	-112(%rbp), %r8
	movq	-104(%rbp), %r10
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L156:
	movq	%r11, %rdi
	movl	$24, %esi
	movq	%r9, -128(%rbp)
	movl	%ecx, -116(%rbp)
	movq	%r8, -112(%rbp)
	movq	%r10, -104(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-104(%rbp), %r10
	movq	-112(%rbp), %r8
	movl	-116(%rbp), %ecx
	movq	-128(%rbp), %r9
	movq	%rax, %rdi
	jmp	.L134
.L152:
	movq	%r8, %rdi
	movl	$24, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdi
	jmp	.L123
.L153:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11702:
	.size	_ZN2v88internal8compiler20InstructionScheduler13ScheduleBlockINS2_22CriticalPathFirstQueueEEEvv, .-_ZN2v88internal8compiler20InstructionScheduler13ScheduleBlockINS2_22CriticalPathFirstQueueEEEvv
	.section	.rodata._ZNSt5dequeIPN2v88internal8compiler20InstructionScheduler17ScheduleGraphNodeENS1_22RecyclingZoneAllocatorIS5_EEE16_M_push_back_auxIJRKS5_EEEvDpOT_.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"cannot create std::deque larger than max_size()"
	.section	.text._ZNSt5dequeIPN2v88internal8compiler20InstructionScheduler17ScheduleGraphNodeENS1_22RecyclingZoneAllocatorIS5_EEE16_M_push_back_auxIJRKS5_EEEvDpOT_,"axG",@progbits,_ZNSt5dequeIPN2v88internal8compiler20InstructionScheduler17ScheduleGraphNodeENS1_22RecyclingZoneAllocatorIS5_EEE16_M_push_back_auxIJRKS5_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt5dequeIPN2v88internal8compiler20InstructionScheduler17ScheduleGraphNodeENS1_22RecyclingZoneAllocatorIS5_EEE16_M_push_back_auxIJRKS5_EEEvDpOT_
	.type	_ZNSt5dequeIPN2v88internal8compiler20InstructionScheduler17ScheduleGraphNodeENS1_22RecyclingZoneAllocatorIS5_EEE16_M_push_back_auxIJRKS5_EEEvDpOT_, @function
_ZNSt5dequeIPN2v88internal8compiler20InstructionScheduler17ScheduleGraphNodeENS1_22RecyclingZoneAllocatorIS5_EEE16_M_push_back_auxIJRKS5_EEEvDpOT_:
.LFB12233:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	88(%rdi), %r13
	movq	56(%rdi), %rsi
	movq	64(%rbx), %rdx
	subq	72(%rbx), %rdx
	movq	%r13, %r14
	movq	%rdx, %rcx
	subq	%rsi, %r14
	sarq	$3, %rcx
	movq	%r14, %rdi
	sarq	$3, %rdi
	leaq	-1(%rdi), %rax
	salq	$6, %rax
	leaq	(%rcx,%rax), %rdx
	movq	48(%rbx), %rax
	subq	32(%rbx), %rax
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpq	$268435455, %rax
	je	.L175
	movq	16(%rbx), %rcx
	movq	24(%rbx), %rdx
	movq	%r13, %rax
	subq	%rcx, %rax
	movq	%rdx, %r8
	sarq	$3, %rax
	subq	%rax, %r8
	cmpq	$1, %r8
	jbe	.L176
.L159:
	movq	8(%rbx), %rax
	testq	%rax, %rax
	je	.L167
	cmpq	$63, 8(%rax)
	ja	.L177
.L167:
	movq	(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L178
	leaq	512(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L168:
	movq	%rax, 8(%r13)
	movq	(%r12), %rdx
	movq	64(%rbx), %rax
	movq	%rdx, (%rax)
	movq	88(%rbx), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 88(%rbx)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 72(%rbx)
	movq	%rdx, 80(%rbx)
	movq	%rax, 64(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L176:
	.cfi_restore_state
	leaq	2(%rdi), %r15
	leaq	(%r15,%r15), %rax
	cmpq	%rax, %rdx
	ja	.L179
	testq	%rdx, %rdx
	movl	$1, %eax
	movq	(%rbx), %rdi
	cmovne	%rdx, %rax
	movq	16(%rdi), %r13
	leaq	2(%rdx,%rax), %rcx
	movq	24(%rdi), %rax
	leaq	0(,%rcx,8), %rsi
	subq	%r13, %rax
	cmpq	%rax, %rsi
	ja	.L180
	addq	%r13, %rsi
	movq	%rsi, 16(%rdi)
.L164:
	movq	%rcx, %rax
	movq	56(%rbx), %rsi
	subq	%r15, %rax
	shrq	%rax
	leaq	0(%r13,%rax,8), %r15
	movq	88(%rbx), %rax
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L165
	subq	%rsi, %rdx
	movq	%r15, %rdi
	movq	%rcx, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %rcx
.L165:
	movq	24(%rbx), %rax
	leaq	0(,%rax,8), %rdx
	cmpq	$15, %rdx
	jbe	.L166
	movq	16(%rbx), %rdx
	movq	%rax, 8(%rdx)
	movq	$0, (%rdx)
.L166:
	movq	%r13, 16(%rbx)
	movq	%rcx, 24(%rbx)
.L162:
	movq	%r15, 56(%rbx)
	movq	(%r15), %rax
	leaq	(%r15,%r14), %r13
	movq	(%r15), %xmm0
	movq	%r13, 88(%rbx)
	addq	$512, %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 40(%rbx)
	movq	0(%r13), %rax
	movq	%rax, 72(%rbx)
	addq	$512, %rax
	movq	%rax, 80(%rbx)
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L177:
	movq	(%rax), %rdx
	movq	%rdx, 8(%rbx)
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L179:
	subq	%r15, %rdx
	addq	$8, %r13
	shrq	%rdx
	leaq	(%rcx,%rdx,8), %r15
	movq	%r13, %rdx
	subq	%rsi, %rdx
	cmpq	%r15, %rsi
	jbe	.L161
	cmpq	%r13, %rsi
	je	.L162
	movq	%r15, %rdi
	call	memmove@PLT
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L161:
	cmpq	%r13, %rsi
	je	.L162
	leaq	8(%r14), %rdi
	subq	%rdx, %rdi
	addq	%r15, %rdi
	call	memmove@PLT
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L178:
	movl	$512, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L180:
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	movq	%rax, %r13
	jmp	.L164
.L175:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE12233:
	.size	_ZNSt5dequeIPN2v88internal8compiler20InstructionScheduler17ScheduleGraphNodeENS1_22RecyclingZoneAllocatorIS5_EEE16_M_push_back_auxIJRKS5_EEEvDpOT_, .-_ZNSt5dequeIPN2v88internal8compiler20InstructionScheduler17ScheduleGraphNodeENS1_22RecyclingZoneAllocatorIS5_EEE16_M_push_back_auxIJRKS5_EEEvDpOT_
	.section	.text._ZN2v88internal8compiler20InstructionScheduler17ScheduleGraphNode12AddSuccessorEPS3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20InstructionScheduler17ScheduleGraphNode12AddSuccessorEPS3_
	.type	_ZN2v88internal8compiler20InstructionScheduler17ScheduleGraphNode12AddSuccessorEPS3_, @function
_ZN2v88internal8compiler20InstructionScheduler17ScheduleGraphNode12AddSuccessorEPS3_:
.LFB10762:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	88(%rdi), %rax
	movq	72(%rdi), %rdx
	movq	%rsi, -8(%rbp)
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L182
	movq	%rsi, (%rdx)
	movq	-8(%rbp), %rax
	addq	$8, 72(%rdi)
	addl	$1, 104(%rax)
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L182:
	.cfi_restore_state
	leaq	-8(%rbp), %rsi
	addq	$8, %rdi
	call	_ZNSt5dequeIPN2v88internal8compiler20InstructionScheduler17ScheduleGraphNodeENS1_22RecyclingZoneAllocatorIS5_EEE16_M_push_back_auxIJRKS5_EEEvDpOT_
	movq	-8(%rbp), %rax
	addl	$1, 104(%rax)
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10762:
	.size	_ZN2v88internal8compiler20InstructionScheduler17ScheduleGraphNode12AddSuccessorEPS3_, .-_ZN2v88internal8compiler20InstructionScheduler17ScheduleGraphNode12AddSuccessorEPS3_
	.section	.text._ZNSt8_Rb_treeIiSt4pairIKiPN2v88internal8compiler20InstructionScheduler17ScheduleGraphNodeEESt10_Select1stIS8_ESt4lessIiENS3_13ZoneAllocatorIS8_EEE8_M_eraseEPSt13_Rb_tree_nodeIS8_E,"axG",@progbits,_ZNSt8_Rb_treeIiSt4pairIKiPN2v88internal8compiler20InstructionScheduler17ScheduleGraphNodeEESt10_Select1stIS8_ESt4lessIiENS3_13ZoneAllocatorIS8_EEE8_M_eraseEPSt13_Rb_tree_nodeIS8_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIiSt4pairIKiPN2v88internal8compiler20InstructionScheduler17ScheduleGraphNodeEESt10_Select1stIS8_ESt4lessIiENS3_13ZoneAllocatorIS8_EEE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	.type	_ZNSt8_Rb_treeIiSt4pairIKiPN2v88internal8compiler20InstructionScheduler17ScheduleGraphNodeEESt10_Select1stIS8_ESt4lessIiENS3_13ZoneAllocatorIS8_EEE8_M_eraseEPSt13_Rb_tree_nodeIS8_E, @function
_ZNSt8_Rb_treeIiSt4pairIKiPN2v88internal8compiler20InstructionScheduler17ScheduleGraphNodeEESt10_Select1stIS8_ESt4lessIiENS3_13ZoneAllocatorIS8_EEE8_M_eraseEPSt13_Rb_tree_nodeIS8_E:
.LFB12249:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L193
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L187:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIiSt4pairIKiPN2v88internal8compiler20InstructionScheduler17ScheduleGraphNodeEESt10_Select1stIS8_ESt4lessIiENS3_13ZoneAllocatorIS8_EEE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L187
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L193:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE12249:
	.size	_ZNSt8_Rb_treeIiSt4pairIKiPN2v88internal8compiler20InstructionScheduler17ScheduleGraphNodeEESt10_Select1stIS8_ESt4lessIiENS3_13ZoneAllocatorIS8_EEE8_M_eraseEPSt13_Rb_tree_nodeIS8_E, .-_ZNSt8_Rb_treeIiSt4pairIKiPN2v88internal8compiler20InstructionScheduler17ScheduleGraphNodeEESt10_Select1stIS8_ESt4lessIiENS3_13ZoneAllocatorIS8_EEE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	.section	.text._ZN2v88internal8compiler20InstructionScheduler8EndBlockENS1_9RpoNumberE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20InstructionScheduler8EndBlockENS1_9RpoNumberE
	.type	_ZN2v88internal8compiler20InstructionScheduler8EndBlockENS1_9RpoNumberE, @function
_ZN2v88internal8compiler20InstructionScheduler8EndBlockENS1_9RpoNumberE:
.LFB10776:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	cmpb	$0, _ZN2v88internal40FLAG_turbo_stress_instruction_schedulingE(%rip)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	je	.L197
	call	_ZN2v88internal8compiler20InstructionScheduler13ScheduleBlockINS2_20StressSchedulerQueueEEEvv
.L198:
	movq	8(%rbx), %rdi
	movl	%r12d, %esi
	call	_ZN2v88internal8compiler19InstructionSequence8EndBlockENS1_9RpoNumberE@PLT
	movq	24(%rbx), %rax
	cmpq	32(%rbx), %rax
	je	.L199
	movq	%rax, 32(%rbx)
.L199:
	movq	$0, 48(%rbx)
	movq	64(%rbx), %rax
	cmpq	72(%rbx), %rax
	je	.L200
	movq	%rax, 72(%rbx)
.L200:
	movq	128(%rbx), %r12
	pxor	%xmm0, %xmm0
	leaq	104(%rbx), %r14
	movups	%xmm0, 88(%rbx)
	testq	%r12, %r12
	je	.L201
.L204:
	movq	24(%r12), %r13
	testq	%r13, %r13
	je	.L202
.L203:
	movq	24(%r13), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeIiSt4pairIKiPN2v88internal8compiler20InstructionScheduler17ScheduleGraphNodeEESt10_Select1stIS8_ESt4lessIiENS3_13ZoneAllocatorIS8_EEE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	movq	16(%r13), %r13
	testq	%r13, %r13
	jne	.L203
.L202:
	movq	16(%r12), %r12
	testq	%r12, %r12
	jne	.L204
.L201:
	movq	$0, 128(%rbx)
	leaq	120(%rbx), %rax
	movq	%rax, 136(%rbx)
	movq	%rax, 144(%rbx)
	movq	$0, 152(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L197:
	.cfi_restore_state
	call	_ZN2v88internal8compiler20InstructionScheduler13ScheduleBlockINS2_22CriticalPathFirstQueueEEEvv
	jmp	.L198
	.cfi_endproc
.LFE10776:
	.size	_ZN2v88internal8compiler20InstructionScheduler8EndBlockENS1_9RpoNumberE, .-_ZN2v88internal8compiler20InstructionScheduler8EndBlockENS1_9RpoNumberE
	.section	.rodata._ZNSt6vectorIPN2v88internal8compiler20InstructionScheduler17ScheduleGraphNodeENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_.str1.1,"aMS",@progbits,1
.LC2:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIPN2v88internal8compiler20InstructionScheduler17ScheduleGraphNodeENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPN2v88internal8compiler20InstructionScheduler17ScheduleGraphNodeENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal8compiler20InstructionScheduler17ScheduleGraphNodeENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_
	.type	_ZNSt6vectorIPN2v88internal8compiler20InstructionScheduler17ScheduleGraphNodeENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_, @function
_ZNSt6vectorIPN2v88internal8compiler20InstructionScheduler17ScheduleGraphNodeENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_:
.LFB12259:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r14
	movq	8(%rdi), %rbx
	movq	%r14, %rax
	subq	%rbx, %rax
	sarq	$3, %rax
	cmpq	$268435455, %rax
	je	.L252
	movq	%rsi, %r8
	movq	%rdi, %r13
	movq	%rsi, %r12
	movq	%rdx, %r15
	subq	%rbx, %r8
	testq	%rax, %rax
	je	.L230
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L253
	movl	$2147483640, %esi
	movl	$2147483640, %edx
.L216:
	movq	0(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	%rsi, %rcx
	jb	.L254
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L219:
	leaq	(%rax,%rdx), %rdi
	leaq	8(%rax), %rcx
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L253:
	testq	%rdx, %rdx
	jne	.L255
	movl	$8, %ecx
	xorl	%edi, %edi
	xorl	%eax, %eax
.L217:
	movq	(%r15), %rdx
	movq	%rdx, (%rax,%r8)
	cmpq	%rbx, %r12
	je	.L220
	leaq	-8(%r12), %r8
	leaq	15(%rax), %rdx
	subq	%rbx, %r8
	subq	%rbx, %rdx
	movq	%r8, %rcx
	shrq	$3, %rcx
	cmpq	$30, %rdx
	jbe	.L233
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rcx
	je	.L233
	leaq	1(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L222:
	movdqu	(%rbx,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L222
	movq	%rsi, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rcx
	addq	%rcx, %rbx
	addq	%rax, %rcx
	cmpq	%rdx, %rsi
	je	.L224
	movq	(%rbx), %rdx
	movq	%rdx, (%rcx)
.L224:
	leaq	16(%rax,%r8), %rcx
.L220:
	cmpq	%r14, %r12
	je	.L225
	subq	%r12, %r14
	leaq	15(%r12), %rdx
	leaq	-8(%r14), %r9
	subq	%rcx, %rdx
	movq	%r9, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rdx
	jbe	.L234
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rsi
	je	.L234
	leaq	1(%rsi), %r8
	xorl	%edx, %edx
	movq	%r8, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L227:
	movdqu	(%r12,%rdx), %xmm2
	movups	%xmm2, (%rcx,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rsi
	jne	.L227
	movq	%r8, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rsi
	leaq	(%rcx,%rsi), %r10
	addq	%rsi, %r12
	cmpq	%rdx, %r8
	je	.L229
	movq	(%r12), %rdx
	movq	%rdx, (%r10)
.L229:
	leaq	8(%rcx,%r9), %rcx
.L225:
	movq	%rax, %xmm0
	movq	%rcx, %xmm3
	movq	%rdi, 24(%r13)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 8(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L230:
	.cfi_restore_state
	movl	$8, %esi
	movl	$8, %edx
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L234:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L226:
	movq	(%r12,%rdx,8), %r8
	movq	%r8, (%rcx,%rdx,8)
	movq	%rdx, %r8
	addq	$1, %rdx
	cmpq	%rsi, %r8
	jne	.L226
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L233:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L221:
	movq	(%rbx,%rdx,8), %rsi
	movq	%rsi, (%rax,%rdx,8)
	movq	%rdx, %rsi
	addq	$1, %rdx
	cmpq	%rsi, %rcx
	jne	.L221
	jmp	.L224
.L254:
	movq	%r8, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %r8
	jmp	.L219
.L252:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L255:
	cmpq	$268435455, %rdx
	movl	$268435455, %eax
	cmova	%rax, %rdx
	salq	$3, %rdx
	movq	%rdx, %rsi
	jmp	.L216
	.cfi_endproc
.LFE12259:
	.size	_ZNSt6vectorIPN2v88internal8compiler20InstructionScheduler17ScheduleGraphNodeENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_, .-_ZNSt6vectorIPN2v88internal8compiler20InstructionScheduler17ScheduleGraphNodeENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_
	.section	.text._ZN2v88internal8compiler20InstructionScheduler13AddTerminatorEPNS1_11InstructionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20InstructionScheduler13AddTerminatorEPNS1_11InstructionE
	.type	_ZN2v88internal8compiler20InstructionScheduler13AddTerminatorEPNS1_11InstructionE, @function
_ZN2v88internal8compiler20InstructionScheduler13AddTerminatorEPNS1_11InstructionE:
.LFB10777:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%r14), %r12
	movq	24(%r14), %rax
	subq	%r12, %rax
	cmpq	$119, %rax
	jbe	.L270
	leaq	120(%r12), %rax
	movq	%rax, 16(%r14)
.L258:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler20InstructionScheduler17ScheduleGraphNodeC1EPNS0_4ZoneEPNS1_11InstructionE
	movq	24(%r13), %rbx
	movq	32(%r13), %r14
	movq	%r12, -72(%rbp)
	cmpq	%r14, %rbx
	je	.L259
	leaq	-64(%rbp), %r15
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L272:
	movq	%r12, (%rax)
	addq	$8, 72(%rdi)
.L269:
	movq	-64(%rbp), %rax
	addq	$8, %rbx
	addl	$1, 104(%rax)
	cmpq	%rbx, %r14
	je	.L271
	movq	-72(%rbp), %r12
.L263:
	movq	(%rbx), %rdi
	movq	%r12, -64(%rbp)
	movq	88(%rdi), %rcx
	movq	72(%rdi), %rax
	leaq	-8(%rcx), %rdx
	cmpq	%rdx, %rax
	jne	.L272
	addq	$8, %rdi
	movq	%r15, %rsi
	call	_ZNSt5dequeIPN2v88internal8compiler20InstructionScheduler17ScheduleGraphNodeENS1_22RecyclingZoneAllocatorIS5_EEE16_M_push_back_auxIJRKS5_EEEvDpOT_
	jmp	.L269
	.p2align 4,,10
	.p2align 3
.L271:
	movq	32(%r13), %r14
.L259:
	cmpq	%r14, 40(%r13)
	je	.L264
	movq	-72(%rbp), %rax
	movq	%rax, (%r14)
	addq	$8, 32(%r13)
.L256:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L273
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L264:
	.cfi_restore_state
	leaq	-72(%rbp), %rdx
	leaq	16(%r13), %rdi
	movq	%r14, %rsi
	call	_ZNSt6vectorIPN2v88internal8compiler20InstructionScheduler17ScheduleGraphNodeENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_
	jmp	.L256
	.p2align 4,,10
	.p2align 3
.L270:
	movq	%rsi, -88(%rbp)
	movq	%r14, %rdi
	movl	$120, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-88(%rbp), %rdx
	movq	%rax, %r12
	jmp	.L258
.L273:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10777:
	.size	_ZN2v88internal8compiler20InstructionScheduler13AddTerminatorEPNS1_11InstructionE, .-_ZN2v88internal8compiler20InstructionScheduler13AddTerminatorEPNS1_11InstructionE
	.section	.text._ZNSt8_Rb_treeIiSt4pairIKiPN2v88internal8compiler20InstructionScheduler17ScheduleGraphNodeEESt10_Select1stIS8_ESt4lessIiENS3_13ZoneAllocatorIS8_EEE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJOiEESK_IJEEEEESt17_Rb_tree_iteratorIS8_ESt23_Rb_tree_const_iteratorIS8_EDpOT_,"axG",@progbits,_ZNSt8_Rb_treeIiSt4pairIKiPN2v88internal8compiler20InstructionScheduler17ScheduleGraphNodeEESt10_Select1stIS8_ESt4lessIiENS3_13ZoneAllocatorIS8_EEE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJOiEESK_IJEEEEESt17_Rb_tree_iteratorIS8_ESt23_Rb_tree_const_iteratorIS8_EDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIiSt4pairIKiPN2v88internal8compiler20InstructionScheduler17ScheduleGraphNodeEESt10_Select1stIS8_ESt4lessIiENS3_13ZoneAllocatorIS8_EEE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJOiEESK_IJEEEEESt17_Rb_tree_iteratorIS8_ESt23_Rb_tree_const_iteratorIS8_EDpOT_
	.type	_ZNSt8_Rb_treeIiSt4pairIKiPN2v88internal8compiler20InstructionScheduler17ScheduleGraphNodeEESt10_Select1stIS8_ESt4lessIiENS3_13ZoneAllocatorIS8_EEE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJOiEESK_IJEEEEESt17_Rb_tree_iteratorIS8_ESt23_Rb_tree_const_iteratorIS8_EDpOT_, @function
_ZNSt8_Rb_treeIiSt4pairIKiPN2v88internal8compiler20InstructionScheduler17ScheduleGraphNodeEESt10_Select1stIS8_ESt4lessIiENS3_13ZoneAllocatorIS8_EEE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJOiEESK_IJEEEEESt17_Rb_tree_iteratorIS8_ESt23_Rb_tree_const_iteratorIS8_EDpOT_:
.LFB12300:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	$47, %rax
	jbe	.L336
	leaq	48(%rbx), %rax
	movq	%rax, 16(%rdi)
.L276:
	movq	(%rcx), %rax
	leaq	16(%r13), %rcx
	movq	%r15, %r12
	movq	$0, 40(%rbx)
	movl	(%rax), %r14d
	movl	%r14d, 32(%rbx)
	cmpq	%r15, %rcx
	je	.L337
	movl	32(%r15), %r15d
	cmpl	%r15d, %r14d
	jge	.L289
	movq	32(%r13), %r15
	cmpq	%r12, %r15
	je	.L316
	movq	%r12, %rdi
	movq	%rcx, -56(%rbp)
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	-56(%rbp), %rcx
	cmpl	32(%rax), %r14d
	jle	.L291
	cmpq	$0, 24(%rax)
	je	.L279
.L316:
	movq	%r12, %rax
.L311:
	testq	%rax, %rax
	setne	%al
.L310:
	cmpq	%r12, %rcx
	je	.L324
	testb	%al, %al
	je	.L338
.L324:
	movl	$1, %edi
.L301:
	movq	%r12, %rdx
	movq	%rbx, %rsi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 48(%r13)
	addq	$24, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L289:
	.cfi_restore_state
	jle	.L287
	cmpq	%r12, 40(%r13)
	je	.L333
	movq	%r12, %rdi
	movq	%rcx, -56(%rbp)
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	-56(%rbp), %rcx
	cmpl	32(%rax), %r14d
	jge	.L299
	cmpq	$0, 24(%r12)
	je	.L300
	movq	%rax, %r12
	movl	$1, %edi
	jmp	.L301
	.p2align 4,,10
	.p2align 3
.L282:
	movq	%rdx, %r12
	testb	%dil, %dil
	jne	.L280
.L285:
	cmpl	%esi, %r14d
	jg	.L288
	movq	%rdx, %r12
.L287:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L337:
	.cfi_restore_state
	cmpq	$0, 48(%r13)
	je	.L278
	movq	40(%r13), %rax
	cmpl	32(%rax), %r14d
	jg	.L279
.L278:
	movq	24(%r13), %rdx
	testq	%rdx, %rdx
	jne	.L281
	jmp	.L339
	.p2align 4,,10
	.p2align 3
.L340:
	movq	16(%rdx), %rax
	movl	$1, %edi
.L284:
	testq	%rax, %rax
	je	.L282
	movq	%rax, %rdx
.L281:
	movl	32(%rdx), %esi
	cmpl	%esi, %r14d
	jl	.L340
	movq	24(%rdx), %rax
	xorl	%edi, %edi
	jmp	.L284
	.p2align 4,,10
	.p2align 3
.L279:
	movq	%rax, %r12
	xorl	%eax, %eax
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L338:
	movl	32(%r12), %r15d
.L300:
	xorl	%edi, %edi
	cmpl	%r15d, %r14d
	setl	%dil
	jmp	.L301
	.p2align 4,,10
	.p2align 3
.L291:
	movq	24(%r13), %r12
	testq	%r12, %r12
	jne	.L293
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L342:
	movq	16(%r12), %rax
	movl	$1, %esi
.L296:
	testq	%rax, %rax
	je	.L294
	movq	%rax, %r12
.L293:
	movl	32(%r12), %edx
	cmpl	%edx, %r14d
	jl	.L342
	movq	24(%r12), %rax
	xorl	%esi, %esi
	jmp	.L296
.L341:
	movq	%rcx, %r12
.L292:
	cmpq	%r12, %r15
	je	.L319
.L335:
	movq	%r12, %rdi
	movq	%rcx, -56(%rbp)
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	-56(%rbp), %rcx
	movq	%r12, %rdi
	movl	32(%rax), %edx
	movq	%rax, %r12
.L307:
	cmpl	%edx, %r14d
	jle	.L287
.L308:
	movq	%rdi, %r12
.L288:
	testq	%r12, %r12
	je	.L287
.L333:
	xorl	%eax, %eax
	jmp	.L311
	.p2align 4,,10
	.p2align 3
.L336:
	movl	$48, %esi
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	movq	%rax, %rbx
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L339:
	movq	%r15, %rdx
.L280:
	cmpq	%rdx, 32(%r13)
	je	.L314
	movq	%rdx, %rdi
	movq	%rcx, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rcx
	movl	32(%rax), %esi
	movq	%rdx, %r12
	movq	%rax, %rdx
	jmp	.L285
	.p2align 4,,10
	.p2align 3
.L294:
	movq	%r12, %rdi
	testb	%sil, %sil
	je	.L307
	jmp	.L292
	.p2align 4,,10
	.p2align 3
.L299:
	movq	24(%r13), %r12
	testq	%r12, %r12
	jne	.L303
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L344:
	movq	16(%r12), %rax
	movl	$1, %esi
.L306:
	testq	%rax, %rax
	je	.L304
	movq	%rax, %r12
.L303:
	movl	32(%r12), %edx
	cmpl	%edx, %r14d
	jl	.L344
	movq	24(%r12), %rax
	xorl	%esi, %esi
	jmp	.L306
	.p2align 4,,10
	.p2align 3
.L304:
	movq	%r12, %rdi
	testb	%sil, %sil
	je	.L307
.L302:
	cmpq	%r12, 32(%r13)
	jne	.L335
	movq	%r12, %rdi
	jmp	.L308
	.p2align 4,,10
	.p2align 3
.L314:
	movq	%rdx, %r12
	jmp	.L288
	.p2align 4,,10
	.p2align 3
.L319:
	movq	%r15, %rdi
	jmp	.L308
.L343:
	movq	%rcx, %r12
	jmp	.L302
	.cfi_endproc
.LFE12300:
	.size	_ZNSt8_Rb_treeIiSt4pairIKiPN2v88internal8compiler20InstructionScheduler17ScheduleGraphNodeEESt10_Select1stIS8_ESt4lessIiENS3_13ZoneAllocatorIS8_EEE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJOiEESK_IJEEEEESt17_Rb_tree_iteratorIS8_ESt23_Rb_tree_const_iteratorIS8_EDpOT_, .-_ZNSt8_Rb_treeIiSt4pairIKiPN2v88internal8compiler20InstructionScheduler17ScheduleGraphNodeEESt10_Select1stIS8_ESt4lessIiENS3_13ZoneAllocatorIS8_EEE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJOiEESK_IJEEEEESt17_Rb_tree_iteratorIS8_ESt23_Rb_tree_const_iteratorIS8_EDpOT_
	.section	.text._ZN2v88internal8compiler20InstructionScheduler14AddInstructionEPNS1_11InstructionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20InstructionScheduler14AddInstructionEPNS1_11InstructionE
	.type	_ZN2v88internal8compiler20InstructionScheduler14AddInstructionEPNS1_11InstructionE, @function
_ZN2v88internal8compiler20InstructionScheduler14AddInstructionEPNS1_11InstructionE:
.LFB10778:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%r14), %rbx
	movq	24(%r14), %rax
	subq	%rbx, %rax
	cmpq	$119, %rax
	jbe	.L456
	leaq	120(%rbx), %rax
	movq	%rax, 16(%r14)
.L347:
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	%rbx, %r15
	call	_ZN2v88internal8compiler20InstructionScheduler17ScheduleGraphNodeC1EPNS0_4ZoneEPNS1_11InstructionE
	movl	(%r12), %eax
	movq	%rbx, -72(%rbp)
	andl	$511, %eax
	cmpl	$17, %eax
	movq	88(%r13), %rax
	je	.L457
.L349:
	testq	%rax, %rax
	je	.L354
	movq	%rbx, -64(%rbp)
	movq	88(%rax), %rdi
	movq	72(%rax), %rcx
	leaq	-8(%rdi), %rdx
	cmpq	%rdx, %rcx
	je	.L355
	movq	%rbx, (%rcx)
	addq	$8, 72(%rax)
.L356:
	movq	-64(%rbp), %rax
	addl	$1, 104(%rax)
.L354:
	cmpq	$0, 96(%r13)
	je	.L358
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler20InstructionScheduler19GetInstructionFlagsEPKNS1_11InstructionE
	testb	$4, %al
	je	.L359
.L362:
	movq	96(%r13), %rdx
	movq	-72(%rbp), %rsi
	movq	%rsi, -64(%rbp)
	movq	88(%rdx), %rax
	movq	72(%rdx), %rcx
	subq	$8, %rax
	cmpq	%rax, %rcx
	je	.L458
	movq	%rsi, (%rcx)
	addq	$8, 72(%rdx)
.L364:
	movq	-64(%rbp), %rax
	addl	$1, 104(%rax)
.L358:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler20InstructionScheduler19GetInstructionFlagsEPKNS1_11InstructionE
	testb	$1, %al
	je	.L365
	movq	48(%r13), %rax
	movq	-72(%rbp), %rdx
	testq	%rax, %rax
	je	.L366
	movq	%rdx, -64(%rbp)
	movq	88(%rax), %rdi
	movq	72(%rax), %rsi
	leaq	-8(%rdi), %rcx
	cmpq	%rcx, %rsi
	je	.L367
	movq	%rdx, (%rsi)
	addq	$8, 72(%rax)
.L368:
	movq	-64(%rbp), %rax
	movq	-72(%rbp), %rdx
	addl	$1, 104(%rax)
.L366:
	movq	64(%r13), %rbx
	movq	72(%r13), %r14
	cmpq	%rbx, %r14
	je	.L369
	leaq	-64(%rbp), %r15
	jmp	.L373
	.p2align 4,,10
	.p2align 3
.L460:
	movq	%rdx, (%rcx)
	addq	$8, 72(%rdi)
.L453:
	movq	-64(%rbp), %rax
	addq	$8, %rbx
	addl	$1, 104(%rax)
	movq	-72(%rbp), %rdx
	cmpq	%rbx, %r14
	je	.L459
.L373:
	movq	(%rbx), %rdi
	movq	%rdx, -64(%rbp)
	movq	88(%rdi), %rax
	movq	72(%rdi), %rcx
	subq	$8, %rax
	cmpq	%rax, %rcx
	jne	.L460
	addq	$8, %rdi
	movq	%r15, %rsi
	call	_ZNSt5dequeIPN2v88internal8compiler20InstructionScheduler17ScheduleGraphNodeENS1_22RecyclingZoneAllocatorIS5_EEE16_M_push_back_auxIJRKS5_EEEvDpOT_
	jmp	.L453
	.p2align 4,,10
	.p2align 3
.L459:
	movq	64(%r13), %rax
	cmpq	72(%r13), %rax
	je	.L369
	movq	%rax, 72(%r13)
.L369:
	movq	%rdx, 48(%r13)
.L374:
	movl	4(%r12), %esi
	xorl	%ebx, %ebx
	leaq	120(%r13), %r14
	movzbl	%sil, %ecx
	testl	$16776960, %esi
	jne	.L380
	jmp	.L381
	.p2align 4,,10
	.p2align 3
.L388:
	movl	%esi, %eax
	addq	$1, %rbx
	shrl	$8, %eax
	movzwl	%ax, %eax
	cmpq	%rax, %rbx
	jnb	.L381
.L380:
	movl	%ecx, %eax
	leaq	5(%rbx,%rax), %rax
	movq	(%r12,%rax,8), %rax
	movl	%eax, %edx
	andl	$7, %edx
	cmpl	$1, %edx
	jne	.L388
	shrq	$3, %rax
	movq	%rax, %rdx
	movl	%eax, %edi
	movq	128(%r13), %rax
	testq	%rax, %rax
	je	.L388
	movq	%r14, %r8
	jmp	.L389
	.p2align 4,,10
	.p2align 3
.L461:
	movq	%rax, %r8
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L390
.L389:
	cmpl	%edi, 32(%rax)
	jge	.L461
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L389
.L390:
	cmpq	%r8, %r14
	je	.L388
	cmpl	%edx, 32(%r8)
	jg	.L388
	movq	40(%r8), %rax
	movq	-72(%rbp), %rsi
	movq	%rsi, -64(%rbp)
	movq	88(%rax), %rdi
	movq	72(%rax), %rcx
	leaq	-8(%rdi), %rdx
	cmpq	%rdx, %rcx
	je	.L393
	movq	%rsi, (%rcx)
	addq	$8, 72(%rax)
.L394:
	movq	-64(%rbp), %rax
	addl	$1, 104(%rax)
	movl	4(%r12), %esi
	movzbl	%sil, %ecx
	jmp	.L388
	.p2align 4,,10
	.p2align 3
.L381:
	xorl	%ebx, %ebx
	leaq	120(%r13), %r14
	testl	%ecx, %ecx
	jne	.L387
	jmp	.L353
	.p2align 4,,10
	.p2align 3
.L395:
	cmpl	$2, %edx
	je	.L462
	addq	$1, %rbx
	movl	%ecx, %eax
	cmpq	%rax, %rbx
	jnb	.L353
.L387:
	movq	40(%r12,%rbx,8), %rax
	movl	%eax, %edx
	andl	$7, %edx
	cmpl	$1, %edx
	jne	.L395
	movq	128(%r13), %rdx
	shrq	$3, %rax
	movq	%r14, %rsi
	movl	%eax, -76(%rbp)
	movl	%eax, %ecx
	testq	%rdx, %rdx
	jne	.L397
	jmp	.L403
	.p2align 4,,10
	.p2align 3
.L463:
	movq	%rdx, %rsi
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L405
.L397:
	cmpl	32(%rdx), %ecx
	jle	.L463
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L397
.L405:
	cmpq	%r14, %rsi
	je	.L403
	cmpl	32(%rsi), %eax
	jge	.L408
.L403:
	leaq	-76(%rbp), %rax
	leaq	-64(%rbp), %rcx
	leaq	104(%r13), %rdi
	leaq	-77(%rbp), %r8
	movq	%rax, -64(%rbp)
	leaq	_ZStL19piecewise_construct(%rip), %rdx
	call	_ZNSt8_Rb_treeIiSt4pairIKiPN2v88internal8compiler20InstructionScheduler17ScheduleGraphNodeEESt10_Select1stIS8_ESt4lessIiENS3_13ZoneAllocatorIS8_EEE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJOiEESK_IJEEEEESt17_Rb_tree_iteratorIS8_ESt23_Rb_tree_const_iteratorIS8_EDpOT_
	movq	%rax, %rsi
.L408:
	movq	-72(%rbp), %rax
	addq	$1, %rbx
	movq	%rax, 40(%rsi)
	movzbl	4(%r12), %ecx
	movl	%ecx, %eax
	cmpq	%rax, %rbx
	jb	.L387
.L353:
	movq	32(%r13), %rsi
	cmpq	40(%r13), %rsi
	je	.L410
	movq	-72(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 32(%r13)
.L345:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L464
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L462:
	.cfi_restore_state
	movq	128(%r13), %rdx
	shrq	$3, %rax
	movq	%r14, %rsi
	movl	%eax, -76(%rbp)
	movl	%eax, %ecx
	testq	%rdx, %rdx
	jne	.L404
	jmp	.L403
	.p2align 4,,10
	.p2align 3
.L465:
	movq	%rdx, %rsi
	movq	16(%rdx), %rdx
.L407:
	testq	%rdx, %rdx
	je	.L405
.L404:
	cmpl	32(%rdx), %ecx
	jle	.L465
	movq	24(%rdx), %rdx
	jmp	.L407
	.p2align 4,,10
	.p2align 3
.L359:
	movl	(%r12), %eax
	movl	%eax, %edx
	andl	$511, %edx
	cmpl	$22, %edx
	je	.L362
	shrl	$14, %eax
	andl	$7, %eax
	leal	-3(%rax), %edx
	cmpl	$1, %edx
	jbe	.L362
	cmpl	$6, %eax
	je	.L362
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler20InstructionScheduler19GetInstructionFlagsEPKNS1_11InstructionE
	testb	$1, %al
	jne	.L362
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler20InstructionScheduler19GetInstructionFlagsEPKNS1_11InstructionE
	testb	$2, %al
	jne	.L362
	jmp	.L358
	.p2align 4,,10
	.p2align 3
.L457:
	cmpb	$1, 4(%r12)
	jne	.L349
	movq	40(%r12), %rdx
	movl	%edx, %ecx
	andl	$7, %ecx
	cmpl	$1, %ecx
	jne	.L349
	btq	$35, %rdx
	jnc	.L349
	shrq	$36, %rdx
	andl	$7, %edx
	subl	$3, %edx
	cmpl	$1, %edx
	ja	.L349
	testq	%rax, %rax
	je	.L350
	movq	%rbx, -64(%rbp)
	movq	88(%rax), %rdi
	movq	72(%rax), %rcx
	leaq	-8(%rdi), %rdx
	cmpq	%rdx, %rcx
	je	.L351
	movq	%rbx, (%rcx)
	addq	$8, 72(%rax)
.L352:
	movq	-64(%rbp), %rax
	movq	-72(%rbp), %r15
	addl	$1, 104(%rax)
.L350:
	movq	%r15, 88(%r13)
	jmp	.L353
	.p2align 4,,10
	.p2align 3
.L365:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler20InstructionScheduler19GetInstructionFlagsEPKNS1_11InstructionE
	testb	$2, %al
	je	.L375
	movq	48(%r13), %rax
	testq	%rax, %rax
	je	.L376
	movq	-72(%rbp), %rsi
	movq	%rsi, -64(%rbp)
	movq	88(%rax), %rdi
	movq	72(%rax), %rcx
	leaq	-8(%rdi), %rdx
	cmpq	%rdx, %rcx
	je	.L377
	movq	%rsi, (%rcx)
	addq	$8, 72(%rax)
.L378:
	movq	-64(%rbp), %rax
	addl	$1, 104(%rax)
.L376:
	movq	72(%r13), %rsi
	cmpq	80(%r13), %rsi
	je	.L379
	movq	-72(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 72(%r13)
	jmp	.L374
	.p2align 4,,10
	.p2align 3
.L375:
	movl	(%r12), %eax
	movl	%eax, %edx
	andl	$511, %edx
	cmpl	$22, %edx
	je	.L382
	shrl	$14, %eax
	andl	$7, %eax
	leal	-3(%rax), %edx
	cmpl	$1, %edx
	jbe	.L382
	cmpl	$6, %eax
	jne	.L374
.L382:
	movq	48(%r13), %rax
	movq	-72(%rbp), %rdx
	testq	%rax, %rax
	je	.L384
	movq	%rdx, -64(%rbp)
	movq	88(%rax), %rdi
	movq	72(%rax), %rsi
	leaq	-8(%rdi), %rcx
	cmpq	%rcx, %rsi
	je	.L385
	movq	%rdx, (%rsi)
	addq	$8, 72(%rax)
.L386:
	movq	-64(%rbp), %rax
	movq	-72(%rbp), %rdx
	addl	$1, 104(%rax)
.L384:
	movq	%rdx, 96(%r13)
	jmp	.L374
.L355:
	leaq	-64(%rbp), %rsi
	leaq	8(%rax), %rdi
	call	_ZNSt5dequeIPN2v88internal8compiler20InstructionScheduler17ScheduleGraphNodeENS1_22RecyclingZoneAllocatorIS5_EEE16_M_push_back_auxIJRKS5_EEEvDpOT_
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L410:
	leaq	-72(%rbp), %rdx
	leaq	16(%r13), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler20InstructionScheduler17ScheduleGraphNodeENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_
	jmp	.L345
	.p2align 4,,10
	.p2align 3
.L393:
	leaq	-64(%rbp), %rsi
	leaq	8(%rax), %rdi
	call	_ZNSt5dequeIPN2v88internal8compiler20InstructionScheduler17ScheduleGraphNodeENS1_22RecyclingZoneAllocatorIS5_EEE16_M_push_back_auxIJRKS5_EEEvDpOT_
	jmp	.L394
.L456:
	movl	$120, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rbx
	jmp	.L347
.L367:
	leaq	-64(%rbp), %rsi
	leaq	8(%rax), %rdi
	call	_ZNSt5dequeIPN2v88internal8compiler20InstructionScheduler17ScheduleGraphNodeENS1_22RecyclingZoneAllocatorIS5_EEE16_M_push_back_auxIJRKS5_EEEvDpOT_
	jmp	.L368
.L458:
	leaq	-64(%rbp), %rsi
	leaq	8(%rdx), %rdi
	call	_ZNSt5dequeIPN2v88internal8compiler20InstructionScheduler17ScheduleGraphNodeENS1_22RecyclingZoneAllocatorIS5_EEE16_M_push_back_auxIJRKS5_EEEvDpOT_
	jmp	.L364
.L379:
	leaq	-72(%rbp), %rdx
	leaq	56(%r13), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler20InstructionScheduler17ScheduleGraphNodeENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_
	jmp	.L374
.L377:
	leaq	-64(%rbp), %rsi
	leaq	8(%rax), %rdi
	call	_ZNSt5dequeIPN2v88internal8compiler20InstructionScheduler17ScheduleGraphNodeENS1_22RecyclingZoneAllocatorIS5_EEE16_M_push_back_auxIJRKS5_EEEvDpOT_
	jmp	.L378
.L385:
	leaq	-64(%rbp), %rsi
	leaq	8(%rax), %rdi
	call	_ZNSt5dequeIPN2v88internal8compiler20InstructionScheduler17ScheduleGraphNodeENS1_22RecyclingZoneAllocatorIS5_EEE16_M_push_back_auxIJRKS5_EEEvDpOT_
	jmp	.L386
.L351:
	leaq	-64(%rbp), %rsi
	leaq	8(%rax), %rdi
	call	_ZNSt5dequeIPN2v88internal8compiler20InstructionScheduler17ScheduleGraphNodeENS1_22RecyclingZoneAllocatorIS5_EEE16_M_push_back_auxIJRKS5_EEEvDpOT_
	jmp	.L352
.L464:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10778:
	.size	_ZN2v88internal8compiler20InstructionScheduler14AddInstructionEPNS1_11InstructionE, .-_ZN2v88internal8compiler20InstructionScheduler14AddInstructionEPNS1_11InstructionE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler20InstructionScheduler19SchedulingQueueBase7AddNodeEPNS2_17ScheduleGraphNodeE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler20InstructionScheduler19SchedulingQueueBase7AddNodeEPNS2_17ScheduleGraphNodeE, @function
_GLOBAL__sub_I__ZN2v88internal8compiler20InstructionScheduler19SchedulingQueueBase7AddNodeEPNS2_17ScheduleGraphNodeE:
.LFB13402:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE13402:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler20InstructionScheduler19SchedulingQueueBase7AddNodeEPNS2_17ScheduleGraphNodeE, .-_GLOBAL__sub_I__ZN2v88internal8compiler20InstructionScheduler19SchedulingQueueBase7AddNodeEPNS2_17ScheduleGraphNodeE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler20InstructionScheduler19SchedulingQueueBase7AddNodeEPNS2_17ScheduleGraphNodeE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata._ZStL19piecewise_construct,"a"
	.type	_ZStL19piecewise_construct, @object
	.size	_ZStL19piecewise_construct, 1
_ZStL19piecewise_construct:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
