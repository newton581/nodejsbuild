	.file	"add-type-assertions-reducer.cc"
	.text
	.section	.rodata._ZNK2v88internal8compiler24AddTypeAssertionsReducer12reducer_nameEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"AddTypeAssertionsReducer"
	.section	.text._ZNK2v88internal8compiler24AddTypeAssertionsReducer12reducer_nameEv,"axG",@progbits,_ZNK2v88internal8compiler24AddTypeAssertionsReducer12reducer_nameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal8compiler24AddTypeAssertionsReducer12reducer_nameEv
	.type	_ZNK2v88internal8compiler24AddTypeAssertionsReducer12reducer_nameEv, @function
_ZNK2v88internal8compiler24AddTypeAssertionsReducer12reducer_nameEv:
.LFB10918:
	.cfi_startproc
	endbr64
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE10918:
	.size	_ZNK2v88internal8compiler24AddTypeAssertionsReducer12reducer_nameEv, .-_ZNK2v88internal8compiler24AddTypeAssertionsReducer12reducer_nameEv
	.section	.text._ZN2v88internal8compiler24AddTypeAssertionsReducerD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler24AddTypeAssertionsReducerD2Ev
	.type	_ZN2v88internal8compiler24AddTypeAssertionsReducerD2Ev, @function
_ZN2v88internal8compiler24AddTypeAssertionsReducerD2Ev:
.LFB10935:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE10935:
	.size	_ZN2v88internal8compiler24AddTypeAssertionsReducerD2Ev, .-_ZN2v88internal8compiler24AddTypeAssertionsReducerD2Ev
	.globl	_ZN2v88internal8compiler24AddTypeAssertionsReducerD1Ev
	.set	_ZN2v88internal8compiler24AddTypeAssertionsReducerD1Ev,_ZN2v88internal8compiler24AddTypeAssertionsReducerD2Ev
	.section	.text._ZN2v88internal8compiler24AddTypeAssertionsReducerD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler24AddTypeAssertionsReducerD0Ev
	.type	_ZN2v88internal8compiler24AddTypeAssertionsReducerD0Ev, @function
_ZN2v88internal8compiler24AddTypeAssertionsReducerD0Ev:
.LFB10937:
	.cfi_startproc
	endbr64
	movl	$72, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10937:
	.size	_ZN2v88internal8compiler24AddTypeAssertionsReducerD0Ev, .-_ZN2v88internal8compiler24AddTypeAssertionsReducerD0Ev
	.section	.text._ZN2v88internal8compiler24AddTypeAssertionsReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler24AddTypeAssertionsReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS0_4ZoneE
	.type	_ZN2v88internal8compiler24AddTypeAssertionsReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS0_4ZoneE, @function
_ZN2v88internal8compiler24AddTypeAssertionsReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS0_4ZoneE:
.LFB10932:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal8compiler24AddTypeAssertionsReducerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rsi, 8(%rdi)
	movq	%rax, (%rdi)
	movq	(%rdx), %rax
	movq	%rdx, 16(%rdi)
	movl	28(%rax), %r12d
	movq	%rcx, 24(%rdi)
	movq	$0, 32(%rdi)
	movl	$0, 40(%rdi)
	movq	$0, 48(%rdi)
	movl	$0, 56(%rdi)
	movq	$0, 64(%rdi)
	testq	%r12, %r12
	jne	.L17
.L5:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	movq	24(%rcx), %rax
	movq	%rdi, %rbx
	leaq	63(%r12), %r13
	movq	16(%rcx), %rdi
	shrq	$6, %r13
	movq	%r12, %rdx
	salq	$3, %r13
	subq	%rdi, %rax
	movq	%r13, %rsi
	cmpq	%rax, %r13
	ja	.L18
	addq	%rdi, %rsi
	movq	%rsi, 16(%rcx)
.L8:
	leaq	(%rdi,%r13), %rax
	sarq	$6, %r12
	andl	$63, %edx
	movq	%rdi, 32(%rbx)
	movq	%rax, 64(%rbx)
	leaq	(%rdi,%r12,8), %rax
	movl	$0, 40(%rbx)
	movq	%rax, 48(%rbx)
	movl	%edx, 56(%rbx)
	testq	%rdi, %rdi
	je	.L5
	addq	$24, %rsp
	movq	%r13, %rdx
	xorl	%esi, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	memset@PLT
	.p2align 4,,10
	.p2align 3
.L18:
	.cfi_restore_state
	movq	%rcx, %rdi
	movl	%r12d, -36(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-36(%rbp), %edx
	movq	%rax, %rdi
	jmp	.L8
	.cfi_endproc
.LFE10932:
	.size	_ZN2v88internal8compiler24AddTypeAssertionsReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS0_4ZoneE, .-_ZN2v88internal8compiler24AddTypeAssertionsReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS0_4ZoneE
	.globl	_ZN2v88internal8compiler24AddTypeAssertionsReducerC1EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS0_4ZoneE
	.set	_ZN2v88internal8compiler24AddTypeAssertionsReducerC1EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS0_4ZoneE,_ZN2v88internal8compiler24AddTypeAssertionsReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS0_4ZoneE
	.section	.rodata._ZNSt6vectorIbN2v88internal13ZoneAllocatorIbEEE14_M_fill_insertESt13_Bit_iteratormb.str1.1,"aMS",@progbits,1
.LC1:
	.string	"vector<bool>::_M_fill_insert"
	.section	.text._ZNSt6vectorIbN2v88internal13ZoneAllocatorIbEEE14_M_fill_insertESt13_Bit_iteratormb,"axG",@progbits,_ZNSt6vectorIbN2v88internal13ZoneAllocatorIbEEE14_M_fill_insertESt13_Bit_iteratormb,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIbN2v88internal13ZoneAllocatorIbEEE14_M_fill_insertESt13_Bit_iteratormb
	.type	_ZNSt6vectorIbN2v88internal13ZoneAllocatorIbEEE14_M_fill_insertESt13_Bit_iteratormb, @function
_ZNSt6vectorIbN2v88internal13ZoneAllocatorIbEEE14_M_fill_insertESt13_Bit_iteratormb:
.LFB12475:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%r8d, -72(%rbp)
	movq	%rsi, -64(%rbp)
	movq	%rdx, -56(%rbp)
	testq	%rcx, %rcx
	je	.L19
	movq	%rdi, %rbx
	movq	8(%rdi), %rax
	movq	%rsi, -80(%rbp)
	movq	%rsi, %r12
	movq	24(%rbx), %r9
	movq	40(%rbx), %rsi
	movq	%rcx, %r13
	movl	32(%rdi), %edx
	movl	-56(%rbp), %r14d
	movq	%r9, %rcx
	subq	%rax, %rsi
	subq	%rax, %rcx
	movq	%rsi, %rax
	movq	%rdx, %rdi
	leaq	(%rdx,%rcx,8), %rcx
	salq	$3, %rax
	subq	%rcx, %rax
	cmpq	%r13, %rax
	jb	.L21
	leaq	0(%r13,%rdx), %rax
	testq	%rax, %rax
	leaq	63(%rax), %rcx
	cmovns	%rax, %rcx
	sarq	$6, %rcx
	leaq	(%r9,%rcx,8), %r8
	movq	%rax, %rcx
	sarq	$63, %rcx
	shrq	$58, %rcx
	addq	%rcx, %rax
	andl	$63, %eax
	subq	%rcx, %rax
	jns	.L22
	addq	$64, %rax
	subq	$8, %r8
.L22:
	movabsq	$-9223372036854775808, %r15
	movq	%r9, %rcx
	movl	%r14d, %r11d
	subq	%r12, %rcx
	movq	%r11, -88(%rbp)
	leaq	(%rdx,%rcx,8), %rsi
	subq	%r11, %rsi
	movl	$1, %r11d
	testq	%rsi, %rsi
	jle	.L31
	.p2align 4,,10
	.p2align 3
.L23:
	testl	%edi, %edi
	je	.L26
.L103:
	subl	$1, %edi
	movq	%r11, %r10
	movl	%edi, %ecx
	salq	%cl, %r10
	testl	%eax, %eax
	je	.L28
.L104:
	subl	$1, %eax
	movq	%r11, %rdx
	movl	%eax, %ecx
	salq	%cl, %rdx
.L29:
	movq	(%r8), %rcx
	testq	%r10, (%r9)
	je	.L30
	orq	%rcx, %rdx
	movq	%rdx, (%r8)
	subq	$1, %rsi
	jne	.L23
.L31:
	movq	-88(%rbp), %rax
	addq	%r13, %rax
	leaq	63(%rax), %rdx
	cmovns	%rax, %rdx
	sarq	$6, %rdx
	leaq	(%r12,%rdx,8), %r8
	cqto
	shrq	$58, %rdx
	leaq	(%rax,%rdx), %r15
	andl	$63, %r15d
	subq	%rdx, %r15
	jns	.L25
	addq	$64, %r15
	subq	$8, %r8
.L25:
	movl	%r15d, %r9d
	cmpq	%r12, %r8
	je	.L33
	testl	%r14d, %r14d
	jne	.L97
	movq	%r8, %r10
	subq	%r12, %r10
	cmpb	$0, -72(%rbp)
	je	.L37
.L36:
	movq	-80(%rbp), %rdi
	movq	%r10, %rdx
	movl	$-1, %esi
	movl	%r9d, -88(%rbp)
	movq	%r8, -72(%rbp)
	call	memset@PLT
	testq	%r15, %r15
	movq	-72(%rbp), %r8
	movl	-88(%rbp), %r9d
	je	.L39
	movl	$64, %ecx
	movq	$-1, %rax
	subl	%r9d, %ecx
	shrq	%cl, %rax
	orq	%rax, (%r8)
	.p2align 4,,10
	.p2align 3
.L39:
	movl	32(%rbx), %ecx
	movq	24(%rbx), %rdx
	addq	%rcx, %r13
	leaq	63(%r13), %rax
	cmovns	%r13, %rax
	sarq	$6, %rax
	leaq	(%rdx,%rax,8), %rdx
	movq	%r13, %rax
	sarq	$63, %rax
	shrq	$58, %rax
	addq	%rax, %r13
	andl	$63, %r13d
	subq	%rax, %r13
	jns	.L94
	addq	$64, %r13
	subq	$8, %rdx
.L94:
	movq	%rdx, 24(%rbx)
.L95:
	movl	%r13d, 32(%rbx)
.L19:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	.cfi_restore_state
	movabsq	$17179869120, %rax
	movq	%rax, %rdx
	subq	%rcx, %rdx
	cmpq	%rdx, %r13
	ja	.L98
	cmpq	%rcx, %r13
	movq	%rcx, %rdx
	cmovnb	%r13, %rdx
	addq	%rdx, %rcx
	jc	.L47
	cmpq	%rax, %rcx
	cmova	%rax, %rcx
	addq	$63, %rcx
	shrq	$6, %rcx
	leaq	0(,%rcx,8), %rax
	movq	%rax, -88(%rbp)
	movq	%rax, %rsi
.L48:
	movq	(%rbx), %rdi
	movq	16(%rdi), %r15
	movq	24(%rdi), %rax
	subq	%r15, %rax
	cmpq	%rax, %rsi
	ja	.L99
	addq	%r15, %rsi
	movq	%rsi, 16(%rdi)
.L50:
	movq	8(%rbx), %rsi
	movq	%r12, %rdx
	subq	%rsi, %rdx
	cmpq	%r12, %rsi
	je	.L51
	movq	%r15, %rdi
	movq	%rdx, -96(%rbp)
	call	memmove@PLT
	movq	-96(%rbp), %rdx
.L51:
	movl	%r14d, %esi
	leaq	(%r15,%rdx), %rdi
	movq	%rsi, -96(%rbp)
	testq	%rsi, %rsi
	je	.L79
	xorl	%edx, %edx
	movq	%r12, %r9
	movl	$1, %r10d
	movl	%edx, %ecx
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L55:
	addl	$1, %ecx
	subq	$1, %rsi
	je	.L100
.L57:
	movq	%r10, %rdx
	movq	(%rdi), %r11
	salq	%cl, %rdx
	movq	%rdx, %rax
	movq	%r11, %r8
	notq	%rax
	orq	%rdx, %r8
	andq	%r11, %rax
	testq	%rdx, (%r9)
	cmovne	%r8, %rax
	movq	%rax, (%rdi)
	cmpl	$63, %ecx
	jne	.L55
	addq	$8, %r9
	addq	$8, %rdi
	xorl	%ecx, %ecx
	subq	$1, %rsi
	jne	.L57
.L100:
	movl	%ecx, %ecx
	movq	%rcx, %rdx
.L52:
	addq	%rcx, %r13
	leaq	63(%r13), %rax
	cmovns	%r13, %rax
	sarq	$6, %rax
	leaq	(%rdi,%rax,8), %r8
	movq	%r13, %rax
	sarq	$63, %rax
	shrq	$58, %rax
	leaq	0(%r13,%rax), %r9
	andl	$63, %r9d
	subq	%rax, %r9
	jns	.L58
	addq	$64, %r9
	subq	$8, %r8
.L58:
	movl	%r9d, %r13d
	cmpq	%rdi, %r8
	je	.L59
	testl	%edx, %edx
	je	.L60
	movl	%edx, %ecx
	leaq	8(%rdi), %rsi
	movq	$-1, %rax
	movq	%r8, %rdx
	salq	%cl, %rax
	subq	%rsi, %rdx
	cmpb	$0, -72(%rbp)
	movq	(%rdi), %rcx
	je	.L61
	orq	%rcx, %rax
	movq	%rax, (%rdi)
	movq	%rsi, %rdi
.L62:
	movl	$-1, %esi
	movq	%r8, -104(%rbp)
	movq	%r9, -72(%rbp)
	call	memset@PLT
	movq	-72(%rbp), %r9
	movq	-104(%rbp), %r8
	testq	%r9, %r9
	jne	.L101
	.p2align 4,,10
	.p2align 3
.L65:
	movq	24(%rbx), %rax
	movl	32(%rbx), %edx
	subq	%r12, %rax
	leaq	(%rdx,%rax,8), %rdx
	subq	-96(%rbp), %rdx
	testq	%rdx, %rdx
	jle	.L69
	movq	-80(%rbp), %r9
	movl	$1, %edi
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L72:
	addl	$1, %r14d
	cmpl	$63, %r13d
	je	.L102
.L74:
	addl	$1, %r13d
	subq	$1, %rdx
	je	.L69
.L76:
	movq	(%r8), %rsi
	movl	%r13d, %ecx
	movq	%rdi, %rax
	movq	%rdi, %r10
	salq	%cl, %rax
	movl	%r14d, %ecx
	salq	%cl, %r10
	movq	%rsi, %rcx
	orq	%rax, %rcx
	notq	%rax
	andq	%rsi, %rax
	testq	%r10, (%r9)
	cmovne	%rcx, %rax
	movq	%rax, (%r8)
	cmpl	$63, %r14d
	jne	.L72
	addq	$8, %r9
	xorl	%r14d, %r14d
	cmpl	$63, %r13d
	jne	.L74
.L102:
	addq	$8, %r8
	xorl	%r13d, %r13d
	subq	$1, %rdx
	jne	.L76
.L69:
	cmpq	$0, 8(%rbx)
	je	.L77
	movq	$0, 24(%rbx)
	movl	$0, 32(%rbx)
	movq	$0, 8(%rbx)
	movl	$0, 16(%rbx)
.L77:
	movq	-88(%rbp), %rax
	movq	%r15, 8(%rbx)
	movl	$0, 16(%rbx)
	addq	%r15, %rax
	movq	%r8, 24(%rbx)
	movq	%rax, 40(%rbx)
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L30:
	notq	%rdx
	andq	%rcx, %rdx
	movq	%rdx, (%r8)
	subq	$1, %rsi
	je	.L31
	testl	%edi, %edi
	jne	.L103
.L26:
	subq	$8, %r9
	movq	%r15, %r10
	movl	$63, %edi
	testl	%eax, %eax
	jne	.L104
.L28:
	subq	$8, %r8
	movq	%r15, %rdx
	movl	$63, %eax
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L97:
	movl	%r14d, %ecx
	leaq	8(%r12), %rdx
	movq	$-1, %rax
	movq	%r8, %r10
	salq	%cl, %rax
	subq	%rdx, %r10
	cmpb	$0, -72(%rbp)
	movq	(%r12), %rcx
	je	.L35
	orq	%rcx, %rax
	movq	%rdx, -80(%rbp)
	movq	%rax, (%r12)
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L35:
	notq	%rax
	movq	%rdx, -80(%rbp)
	andq	%rcx, %rax
	movq	%rax, (%r12)
.L37:
	movq	-80(%rbp), %rdi
	xorl	%esi, %esi
	movq	%r10, %rdx
	movl	%r9d, -88(%rbp)
	movq	%r8, -72(%rbp)
	call	memset@PLT
	testq	%r15, %r15
	movq	-72(%rbp), %r8
	movl	-88(%rbp), %r9d
	je	.L39
	movl	$64, %ecx
	movq	$-1, %rax
	subl	%r9d, %ecx
	shrq	%cl, %rax
	notq	%rax
	andq	%rax, (%r8)
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L33:
	cmpl	%r14d, %r15d
	je	.L39
	movq	$-1, %rdx
	movl	$64, %ecx
	subl	%r15d, %ecx
	movq	%rdx, %rax
	shrq	%cl, %rax
	movl	%r14d, %ecx
	salq	%cl, %rdx
	movq	(%r8), %rcx
	andq	%rdx, %rax
	movq	%rax, %rdx
	orq	%rcx, %rax
	notq	%rdx
	andq	%rcx, %rdx
	cmpb	$0, -72(%rbp)
	cmove	%rdx, %rax
	movq	%rax, (%r8)
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L60:
	movq	%r8, %rdx
	subq	%rdi, %rdx
	cmpb	$0, -72(%rbp)
	jne	.L62
.L63:
	xorl	%esi, %esi
	movq	%r8, -104(%rbp)
	movq	%r9, -72(%rbp)
	call	memset@PLT
	movq	-72(%rbp), %r9
	movq	-104(%rbp), %r8
	testq	%r9, %r9
	je	.L65
	movl	$64, %ecx
	movq	$-1, %rax
	subl	%r13d, %ecx
	shrq	%cl, %rax
	notq	%rax
	andq	%rax, (%r8)
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L59:
	cmpl	%edx, %r9d
	je	.L65
	movq	$-1, %rsi
	movl	$64, %ecx
	subl	%r9d, %ecx
	movq	%rsi, %rax
	shrq	%cl, %rax
	movl	%edx, %ecx
	salq	%cl, %rsi
	movq	(%r8), %rcx
	andq	%rsi, %rax
	movq	%rax, %rdx
	orq	%rcx, %rax
	notq	%rdx
	andq	%rcx, %rdx
	cmpb	$0, -72(%rbp)
	cmove	%rdx, %rax
	movq	%rax, (%r8)
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L101:
	movl	$64, %ecx
	movq	$-1, %rax
	subl	%r13d, %ecx
	shrq	%cl, %rax
	orq	%rax, (%r8)
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L61:
	notq	%rax
	andq	%rcx, %rax
	movq	%rax, (%rdi)
	movq	%rsi, %rdi
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L99:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r15
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L79:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	jmp	.L52
.L98:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L47:
	movq	$2147483640, -88(%rbp)
	movl	$2147483640, %esi
	jmp	.L48
	.cfi_endproc
.LFE12475:
	.size	_ZNSt6vectorIbN2v88internal13ZoneAllocatorIbEEE14_M_fill_insertESt13_Bit_iteratormb, .-_ZNSt6vectorIbN2v88internal13ZoneAllocatorIbEEE14_M_fill_insertESt13_Bit_iteratormb
	.section	.text._ZN2v88internal8compiler24AddTypeAssertionsReducer6ReduceEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler24AddTypeAssertionsReducer6ReduceEPNS1_4NodeE
	.type	_ZN2v88internal8compiler24AddTypeAssertionsReducer6ReduceEPNS1_4NodeE, @function
_ZN2v88internal8compiler24AddTypeAssertionsReducer6ReduceEPNS1_4NodeE:
.LFB10938:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movzwl	16(%rax), %eax
	cmpl	$35, %eax
	je	.L108
	cmpl	$292, %eax
	je	.L108
	cmpq	$0, 8(%rsi)
	movq	%rsi, %rbx
	je	.L108
	movl	20(%rsi), %ecx
	movq	%rdi, %r12
	movq	32(%rdi), %rax
	movl	$1, %r13d
	movq	48(%r12), %r9
	movl	56(%rdi), %esi
	movl	%ecx, %edx
	salq	%cl, %r13
	andl	$16777215, %edx
	movq	%r9, %r8
	movq	%rsi, %rdi
	subq	%rax, %r8
	movq	%rdx, %r14
	shrq	$6, %r14
	leaq	(%rsi,%r8,8), %rsi
	salq	$3, %r14
	cmpq	%rsi, %rdx
	jnb	.L110
	addq	%r14, %rax
	movq	(%rax), %rdx
	testq	%r13, %rdx
	jne	.L108
.L111:
	orq	%rdx, %r13
	movq	%r13, (%rax)
.L114:
	movq	8(%rbx), %r14
	testb	$1, %r14b
	jne	.L108
	cmpl	$4, (%r14)
	je	.L140
	.p2align 4,,10
	.p2align 3
.L108:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L141
	addq	$56, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L110:
	.cfi_restore_state
	addq	$1, %rdx
	cmpq	%rdx, %rsi
	jbe	.L112
	movq	%rdx, %rcx
	andl	$63, %edx
	sarq	$6, %rcx
	movl	%edx, 56(%r12)
	leaq	(%rax,%rcx,8), %rcx
	movq	%rcx, 48(%r12)
.L113:
	addq	%r14, %rax
	movq	(%rax), %rdx
	testq	%r13, %rdx
	jne	.L114
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L140:
	movq	16(%r12), %rax
	movq	%r14, %rsi
	movq	376(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10AssertTypeENS1_4TypeE@PLT
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%rbx, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r14, 8(%rax)
	movq	24(%rbx), %r15
	movq	%rax, -88(%rbp)
	testq	%r15, %r15
	je	.L108
	movq	(%r15), %rbx
	.p2align 4,,10
	.p2align 3
.L121:
	movl	16(%r15), %edx
	movl	%edx, %eax
	shrl	%eax
	leaq	3(%rax,%rax,2), %rcx
	salq	$3, %rax
	andl	$1, %edx
	leaq	(%r15,%rcx,8), %r14
	leaq	32(%r14,%rax), %r13
	jne	.L117
	leaq	16(%r14,%rax), %r13
	movq	(%r14), %r14
.L117:
	movq	%r15, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler14NodeProperties11IsValueEdgeENS1_4EdgeE@PLT
	movq	-88(%rbp), %rcx
	cmpq	%r14, %rcx
	je	.L118
	testb	%al, %al
	jne	.L142
.L118:
	testq	%rbx, %rbx
	je	.L108
	movq	%rbx, %r15
	movq	(%rbx), %rbx
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L112:
	movl	%edi, -72(%rbp)
	movq	%rdx, %rcx
	movq	-72(%rbp), %rdx
	leaq	24(%r12), %rdi
	subq	%rsi, %rcx
	xorl	%r8d, %r8d
	movq	%r9, %rsi
	movq	%r9, -80(%rbp)
	call	_ZNSt6vectorIbN2v88internal13ZoneAllocatorIbEEE14_M_fill_insertESt13_Bit_iteratormb
	movq	32(%r12), %rax
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L142:
	movq	0(%r13), %rdi
	cmpq	%rdi, %rcx
	je	.L119
	testq	%rdi, %rdi
	je	.L120
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L120:
	movq	-88(%rbp), %rax
	movq	%r15, %rsi
	movq	%rax, 0(%r13)
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L119:
	movq	8(%r12), %rdi
	movq	%r14, %rsi
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L118
.L141:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10938:
	.size	_ZN2v88internal8compiler24AddTypeAssertionsReducer6ReduceEPNS1_4NodeE, .-_ZN2v88internal8compiler24AddTypeAssertionsReducer6ReduceEPNS1_4NodeE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler24AddTypeAssertionsReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS0_4ZoneE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler24AddTypeAssertionsReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS0_4ZoneE, @function
_GLOBAL__sub_I__ZN2v88internal8compiler24AddTypeAssertionsReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS0_4ZoneE:
.LFB12690:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE12690:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler24AddTypeAssertionsReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS0_4ZoneE, .-_GLOBAL__sub_I__ZN2v88internal8compiler24AddTypeAssertionsReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS0_4ZoneE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler24AddTypeAssertionsReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS0_4ZoneE
	.weak	_ZTVN2v88internal8compiler24AddTypeAssertionsReducerE
	.section	.data.rel.ro._ZTVN2v88internal8compiler24AddTypeAssertionsReducerE,"awG",@progbits,_ZTVN2v88internal8compiler24AddTypeAssertionsReducerE,comdat
	.align 8
	.type	_ZTVN2v88internal8compiler24AddTypeAssertionsReducerE, @object
	.size	_ZTVN2v88internal8compiler24AddTypeAssertionsReducerE, 56
_ZTVN2v88internal8compiler24AddTypeAssertionsReducerE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal8compiler24AddTypeAssertionsReducerD1Ev
	.quad	_ZN2v88internal8compiler24AddTypeAssertionsReducerD0Ev
	.quad	_ZNK2v88internal8compiler24AddTypeAssertionsReducer12reducer_nameEv
	.quad	_ZN2v88internal8compiler24AddTypeAssertionsReducer6ReduceEPNS1_4NodeE
	.quad	_ZN2v88internal8compiler7Reducer8FinalizeEv
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
