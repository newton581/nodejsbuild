	.file	"dead-code-elimination.cc"
	.text
	.section	.rodata._ZNK2v88internal8compiler19DeadCodeElimination12reducer_nameEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"DeadCodeElimination"
	.section	.text._ZNK2v88internal8compiler19DeadCodeElimination12reducer_nameEv,"axG",@progbits,_ZNK2v88internal8compiler19DeadCodeElimination12reducer_nameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal8compiler19DeadCodeElimination12reducer_nameEv
	.type	_ZNK2v88internal8compiler19DeadCodeElimination12reducer_nameEv, @function
_ZNK2v88internal8compiler19DeadCodeElimination12reducer_nameEv:
.LFB10242:
	.cfi_startproc
	endbr64
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE10242:
	.size	_ZNK2v88internal8compiler19DeadCodeElimination12reducer_nameEv, .-_ZNK2v88internal8compiler19DeadCodeElimination12reducer_nameEv
	.section	.text._ZN2v88internal8compiler19DeadCodeEliminationD2Ev,"axG",@progbits,_ZN2v88internal8compiler19DeadCodeEliminationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler19DeadCodeEliminationD2Ev
	.type	_ZN2v88internal8compiler19DeadCodeEliminationD2Ev, @function
_ZN2v88internal8compiler19DeadCodeEliminationD2Ev:
.LFB12343:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE12343:
	.size	_ZN2v88internal8compiler19DeadCodeEliminationD2Ev, .-_ZN2v88internal8compiler19DeadCodeEliminationD2Ev
	.weak	_ZN2v88internal8compiler19DeadCodeEliminationD1Ev
	.set	_ZN2v88internal8compiler19DeadCodeEliminationD1Ev,_ZN2v88internal8compiler19DeadCodeEliminationD2Ev
	.section	.text._ZN2v88internal8compiler19DeadCodeEliminationD0Ev,"axG",@progbits,_ZN2v88internal8compiler19DeadCodeEliminationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler19DeadCodeEliminationD0Ev
	.type	_ZN2v88internal8compiler19DeadCodeEliminationD0Ev, @function
_ZN2v88internal8compiler19DeadCodeEliminationD0Ev:
.LFB12345:
	.cfi_startproc
	endbr64
	movl	$48, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12345:
	.size	_ZN2v88internal8compiler19DeadCodeEliminationD0Ev, .-_ZN2v88internal8compiler19DeadCodeEliminationD0Ev
	.section	.text._ZN2v88internal8compiler19DeadCodeEliminationC2EPNS1_15AdvancedReducer6EditorEPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19DeadCodeEliminationC2EPNS1_15AdvancedReducer6EditorEPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS0_4ZoneE
	.type	_ZN2v88internal8compiler19DeadCodeEliminationC2EPNS1_15AdvancedReducer6EditorEPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS0_4ZoneE, @function
_ZN2v88internal8compiler19DeadCodeEliminationC2EPNS1_15AdvancedReducer6EditorEPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS0_4ZoneE:
.LFB10629:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal8compiler19DeadCodeEliminationE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rcx, %rdi
	subq	$16, %rsp
	movq	%rsi, 8(%rbx)
	movq	%rdx, 16(%rbx)
	movq	%rcx, 24(%rbx)
	movq	%rax, (%rbx)
	movq	%r8, -24(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4DeadEv@PLT
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rax, %rsi
	xorl	%edx, %edx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %xmm0
	movq	$1, 8(%rax)
	movhps	-24(%rbp), %xmm0
	movups	%xmm0, 32(%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10629:
	.size	_ZN2v88internal8compiler19DeadCodeEliminationC2EPNS1_15AdvancedReducer6EditorEPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS0_4ZoneE, .-_ZN2v88internal8compiler19DeadCodeEliminationC2EPNS1_15AdvancedReducer6EditorEPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS0_4ZoneE
	.globl	_ZN2v88internal8compiler19DeadCodeEliminationC1EPNS1_15AdvancedReducer6EditorEPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS0_4ZoneE
	.set	_ZN2v88internal8compiler19DeadCodeEliminationC1EPNS1_15AdvancedReducer6EditorEPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS0_4ZoneE,_ZN2v88internal8compiler19DeadCodeEliminationC2EPNS1_15AdvancedReducer6EditorEPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS0_4ZoneE
	.section	.text._ZN2v88internal8compiler19DeadCodeElimination20PropagateDeadControlEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19DeadCodeElimination20PropagateDeadControlEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19DeadCodeElimination20PropagateDeadControlEPNS1_4NodeE, @function
_ZN2v88internal8compiler19DeadCodeElimination20PropagateDeadControlEPNS1_4NodeE:
.LFB10637:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	(%rax), %rdx
	cmpw	$61, 16(%rdx)
	movl	$0, %edx
	cmovne	%rdx, %rax
	ret
	.cfi_endproc
.LFE10637:
	.size	_ZN2v88internal8compiler19DeadCodeElimination20PropagateDeadControlEPNS1_4NodeE, .-_ZN2v88internal8compiler19DeadCodeElimination20PropagateDeadControlEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19DeadCodeElimination9ReduceEndEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19DeadCodeElimination9ReduceEndEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19DeadCodeElimination9ReduceEndEPNS1_4NodeE, @function
_ZN2v88internal8compiler19DeadCodeElimination9ReduceEndEPNS1_4NodeE:
.LFB10638:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	32(%rsi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movzbl	23(%rsi), %r13d
	movq	%rdi, -96(%rbp)
	movq	%r15, -56(%rbp)
	andl	$15, %r13d
	cmpl	$15, %r13d
	je	.L11
	movl	%r13d, -60(%rbp)
.L12:
	movl	-60(%rbp), %eax
	testl	%eax, %eax
	jle	.L21
	movl	-60(%rbp), %eax
	xorl	%ebx, %ebx
	xorl	%r12d, %r12d
	leal	-1(%rax), %r13d
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L34:
	addq	-56(%rbp), %rax
	movq	%r14, %r10
	movq	(%rax), %rdi
	cmpq	%rdi, %r8
	je	.L16
.L18:
	movl	%r12d, %esi
	notl	%esi
	movslq	%esi, %rsi
	leaq	(%rsi,%rsi,2), %rsi
	leaq	(%r10,%rsi,8), %rsi
	testq	%rdi, %rdi
	je	.L19
	movq	%rax, -88(%rbp)
	movq	%r8, -80(%rbp)
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-72(%rbp), %rsi
	movq	-80(%rbp), %r8
	movq	-88(%rbp), %rax
.L19:
	movq	%r8, (%rax)
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L16:
	addl	$1, %r12d
.L15:
	leaq	1(%rbx), %rax
	cmpq	%rbx, %r13
	je	.L33
	movq	%rax, %rbx
.L20:
	movq	(%r15,%rbx,8), %r8
	movq	(%r8), %rax
	cmpw	$61, 16(%rax)
	je	.L15
	cmpl	%ebx, %r12d
	je	.L16
	movzbl	23(%r14), %esi
	movslq	%r12d, %rax
	salq	$3, %rax
	andl	$15, %esi
	cmpl	$15, %esi
	jne	.L34
	movq	-56(%rbp), %rcx
	movq	(%rcx), %r10
	leaq	16(%r10,%rax), %rax
	movq	(%rax), %rdi
	cmpq	%rdi, %r8
	jne	.L18
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L33:
	testl	%r12d, %r12d
	je	.L21
	xorl	%eax, %eax
	cmpl	-60(%rbp), %r12d
	jl	.L35
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	.cfi_restore_state
	movq	-96(%rbp), %rax
	movq	32(%rax), %rax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore_state
	movq	32(%rsi), %rcx
	movl	8(%rcx), %eax
	leaq	16(%rcx), %r15
	movl	%eax, -60(%rbp)
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L35:
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node14TrimInputCountEi@PLT
	movq	-96(%rbp), %rax
	movslq	%r12d, %rsi
	movq	24(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3EndEm@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	addq	$56, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10638:
	.size	_ZN2v88internal8compiler19DeadCodeElimination9ReduceEndEPNS1_4NodeE, .-_ZN2v88internal8compiler19DeadCodeElimination9ReduceEndEPNS1_4NodeE
	.section	.rodata._ZN2v88internal8compiler19DeadCodeElimination17ReduceLoopOrMergeEPNS1_4NodeE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZN2v88internal8compiler19DeadCodeElimination17ReduceLoopOrMergeEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19DeadCodeElimination17ReduceLoopOrMergeEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19DeadCodeElimination17ReduceLoopOrMergeEPNS1_4NodeE, @function
_ZN2v88internal8compiler19DeadCodeElimination17ReduceLoopOrMergeEPNS1_4NodeE:
.LFB10639:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	32(%rsi), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$88, %rsp
	movzbl	23(%rsi), %eax
	movq	(%rsi), %rdx
	movq	%rdi, -120(%rbp)
	movq	%rcx, -96(%rbp)
	andl	$15, %eax
	movzwl	16(%rdx), %edx
	cmpl	$15, %eax
	je	.L37
	movl	%eax, -104(%rbp)
	movq	%rcx, -56(%rbp)
	cmpw	$1, %dx
	je	.L39
.L38:
	movl	-104(%rbp), %eax
	testl	%eax, %eax
	jle	.L41
	xorl	%r13d, %r13d
	subl	$1, %eax
	xorl	%r15d, %r15d
	movq	%rbx, -80(%rbp)
	movq	%rax, -64(%rbp)
	movl	%r13d, %ebx
	movq	%r15, %r13
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L48:
	addl	$1, %ebx
.L42:
	leaq	1(%r13), %rax
	cmpq	-64(%rbp), %r13
	je	.L150
	movq	%rax, %r13
.L57:
	movq	-56(%rbp), %rax
	leaq	0(,%r13,8), %r15
	movq	(%rax,%r13,8), %r14
	movq	(%r14), %rax
	cmpw	$61, 16(%rax)
	je	.L42
	cmpl	%r13d, %ebx
	je	.L48
	movq	-80(%rbp), %rsi
	movslq	%ebx, %r12
	salq	$3, %r12
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	movq	-96(%rbp), %rax
	je	.L44
	leaq	(%rax,%r12), %rdx
	movq	(%rdx), %rdi
	cmpq	%rdi, %r14
	je	.L46
.L45:
	movl	%ebx, %eax
	notl	%eax
	cltq
	leaq	(%rax,%rax,2), %rax
	leaq	(%rsi,%rax,8), %rsi
	testq	%rdi, %rdi
	je	.L47
	movq	%rdx, -88(%rbp)
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-72(%rbp), %rsi
	movq	-88(%rbp), %rdx
.L47:
	movq	%r14, (%rdx)
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L46:
	movq	-80(%rbp), %rax
	movq	24(%rax), %r14
	testq	%r14, %r14
	je	.L48
	movl	%ebx, %edx
	movl	%ebx, -100(%rbp)
	notl	%edx
	movq	%r13, -112(%rbp)
	movslq	%edx, %rdx
	leaq	(%rdx,%rdx,2), %rax
	salq	$3, %rax
	movq	%rax, -72(%rbp)
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L51:
	movq	(%r14), %r14
	testq	%r14, %r14
	je	.L151
.L56:
	movl	16(%r14), %ecx
	movl	%ecx, %edx
	shrl	%edx
	andl	$1, %ecx
	leaq	3(%rdx,%rdx,2), %rdx
	leaq	(%r14,%rdx,8), %rsi
	movq	(%rsi), %rdx
	jne	.L49
	movq	%rdx, %rsi
	movq	(%rdx), %rdx
.L49:
	movzwl	16(%rdx), %edx
	subl	$35, %edx
	cmpl	$1, %edx
	ja	.L51
	movzbl	23(%rsi), %ecx
	leaq	32(%rsi), %rdx
	andl	$15, %ecx
	cmpl	$15, %ecx
	je	.L52
	leaq	(%rdx,%r12), %rbx
	movq	32(%rsi,%r15), %r13
	movq	(%rbx), %rdi
	cmpq	%r13, %rdi
	je	.L51
.L53:
	addq	-72(%rbp), %rsi
	testq	%rdi, %rdi
	je	.L55
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-88(%rbp), %rsi
.L55:
	movq	%r13, (%rbx)
	testq	%r13, %r13
	je	.L51
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movq	(%r14), %r14
	testq	%r14, %r14
	jne	.L56
	.p2align 4,,10
	.p2align 3
.L151:
	movl	-100(%rbp), %ebx
	movq	-112(%rbp), %r13
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L52:
	movq	32(%rsi), %rsi
	leaq	16(%rsi,%r12), %rbx
	movq	16(%rsi,%r15), %r13
	movq	(%rbx), %rdi
	cmpq	%r13, %rdi
	jne	.L53
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L150:
	movl	%ebx, %r13d
	movq	-80(%rbp), %rbx
	testl	%r13d, %r13d
	je	.L41
	cmpl	$1, %r13d
	je	.L152
	xorl	%eax, %eax
	cmpl	-104(%rbp), %r13d
	jge	.L129
	movq	24(%rbx), %r14
	testq	%r14, %r14
	je	.L98
	movslq	%r13d, %r12
	leaq	0(,%r12,8), %rax
	movq	-120(%rbp), %r12
	movq	%rax, -64(%rbp)
	movl	%r13d, %eax
	notl	%eax
	cltq
	leaq	(%rax,%rax,2), %rax
	salq	$3, %rax
	movq	%rax, -72(%rbp)
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L92:
	movq	(%r14), %r14
	testq	%r14, %r14
	je	.L98
.L97:
	movl	16(%r14), %edx
	movl	%edx, %eax
	shrl	%eax
	andl	$1, %edx
	leaq	3(%rax,%rax,2), %rax
	leaq	(%r14,%rax,8), %r15
	movq	(%r15), %rsi
	jne	.L91
	movq	%rsi, %r15
	movq	(%rsi), %rsi
.L91:
	movzwl	16(%rsi), %eax
	subl	$35, %eax
	cmpl	$1, %eax
	ja	.L92
	movzbl	23(%r15), %eax
	leaq	32(%r15), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L93
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	movq	%r15, %rdx
	movq	(%rax), %rdi
	cmpq	%rdi, %rbx
	je	.L95
.L94:
	movq	-72(%rbp), %rcx
	leaq	(%rdx,%rcx), %rsi
	testq	%rdi, %rdi
	je	.L96
	movq	%rax, -80(%rbp)
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-56(%rbp), %rsi
	movq	-80(%rbp), %rax
.L96:
	movq	%rbx, (%rax)
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movq	(%r15), %rsi
.L95:
	movq	24(%r12), %rdi
	movl	%r13d, %edx
	call	_ZN2v88internal8compiler21CommonOperatorBuilder16ResizeMergeOrPhiEPKNS1_8OperatorEi@PLT
	movq	%rax, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal8compiler18OperatorProperties18GetTotalInputCountEPKNS1_8OperatorE@PLT
	movq	%r15, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler4Node14TrimInputCountEi@PLT
	movq	-56(%rbp), %r9
	movq	%r15, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	8(%r12), %rdi
	movq	%r15, %rsi
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	(%r14), %r14
	testq	%r14, %r14
	jne	.L97
.L98:
	movq	-120(%rbp), %rax
	movq	(%rbx), %rsi
	movl	%r13d, %edx
	movq	24(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder16ResizeMergeOrPhiEPKNS1_8OperatorEi@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler18OperatorProperties18GetTotalInputCountEPKNS1_8OperatorE@PLT
	movq	%rbx, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler4Node14TrimInputCountEi@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	addq	$88, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	.cfi_restore_state
	movq	32(%rsi), %r13
	movl	8(%r13), %eax
	movl	%eax, -104(%rbp)
	leaq	16(%r13), %rax
	movq	%rax, -56(%rbp)
	cmpw	$1, %dx
	jne	.L38
.L39:
	movq	-56(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	cmpw	$61, 16(%rax)
	jne	.L38
.L41:
	movq	-120(%rbp), %rax
	movq	32(%rax), %rax
.L129:
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	.cfi_restore_state
	movq	(%rax), %rsi
	leaq	16(%rsi,%r12), %rdx
	movq	(%rdx), %rdi
	cmpq	%rdi, %r14
	jne	.L45
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L152:
	movq	-120(%rbp), %rax
	movq	24(%rbx), %r15
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
	movq	40(%rax), %rax
	movq	%rax, -64(%rbp)
	testq	%r15, %r15
	je	.L82
	movq	%r14, -56(%rbp)
	movq	-120(%rbp), %r14
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L63:
	cmpl	$52, %eax
	je	.L153
	cmpl	$18, %eax
	jne	.L65
	movq	8(%r14), %rdi
	movq	32(%r14), %rdx
	movq	%r8, %rsi
	movq	(%rdi), %rax
	call	*16(%rax)
.L65:
	movq	(%r15), %r15
	testq	%r15, %r15
	je	.L154
.L59:
	movl	16(%r15), %edx
	movl	%edx, %eax
	shrl	%eax
	andl	$1, %edx
	leaq	3(%rax,%rax,2), %rax
	leaq	(%r15,%rax,8), %r8
	movq	(%r8), %rax
	jne	.L62
	movq	%rax, %r8
	movq	(%rax), %rax
.L62:
	movzwl	16(%rax), %eax
	leal	-35(%rax), %edx
	cmpl	$1, %edx
	ja	.L63
	movzbl	23(%r8), %eax
	movq	32(%r8), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L64
	movq	16(%rdx), %rdx
.L64:
	movq	8(%r14), %rdi
	movq	%r8, %rsi
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	(%r15), %r15
	testq	%r15, %r15
	jne	.L59
.L154:
	cmpq	%r12, %r13
	je	.L82
	movq	%rbx, -72(%rbp)
	movq	-120(%rbp), %r15
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L156:
	movq	40(%r14), %rdi
	cmpq	%rdi, %rbx
	je	.L86
	leaq	40(%r14), %rax
	movq	%r14, %rsi
.L85:
	subq	$48, %rsi
	testq	%rdi, %rdi
	je	.L87
	movq	%rax, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-64(%rbp), %rax
	movq	-56(%rbp), %rsi
.L87:
	movq	%rbx, (%rax)
	testq	%rbx, %rbx
	je	.L86
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L86:
	movq	8(%r15), %rdi
	addq	$8, %r12
	movq	%r14, %rsi
	movq	(%rdi), %rax
	call	*24(%rax)
	cmpq	%r12, %r13
	je	.L155
.L88:
	movq	(%r12), %r14
	movq	32(%r15), %rbx
	movzbl	23(%r14), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L156
	movq	32(%r14), %rsi
	movq	24(%rsi), %rdi
	cmpq	%rdi, %rbx
	je	.L86
	leaq	24(%rsi), %rax
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L153:
	movzbl	23(%r8), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L67
	leaq	40(%r8), %rax
.L68:
	cmpq	(%rax), %rbx
	jne	.L65
	cmpq	%r13, -56(%rbp)
	je	.L71
	movq	%r8, 0(%r13)
	addq	$8, %r13
	jmp	.L65
.L155:
	movq	-72(%rbp), %rbx
.L82:
	movzbl	23(%rbx), %edx
	movq	32(%rbx), %rax
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L129
	movq	16(%rax), %rax
	jmp	.L129
.L93:
	movq	32(%r15), %rdx
	movq	-64(%rbp), %rax
	leaq	16(%rdx,%rax), %rax
	movq	(%rax), %rdi
	cmpq	%rdi, %rbx
	jne	.L94
	jmp	.L95
.L67:
	movq	32(%r8), %rax
	addq	$24, %rax
	jmp	.L68
.L71:
	movq	-56(%rbp), %rdx
	subq	%r12, %rdx
	movq	%rdx, %rsi
	sarq	$3, %rsi
	cmpq	$268435455, %rsi
	je	.L157
	testq	%rsi, %rsi
	je	.L103
	leaq	(%rsi,%rsi), %rax
	cmpq	%rax, %rsi
	jbe	.L158
	movl	$2147483640, %esi
	movl	$2147483640, %ecx
.L73:
	movq	-64(%rbp), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdi
	subq	%rax, %rdi
	cmpq	%rsi, %rdi
	jb	.L159
	movq	-64(%rbp), %rdi
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L76:
	leaq	(%rax,%rcx), %rdi
	leaq	8(%rax), %rsi
	movq	%rdi, -56(%rbp)
.L74:
	movq	%r8, (%rax,%rdx)
	cmpq	%r12, %r13
	je	.L106
	leaq	-8(%r13), %rdi
	leaq	15(%rax), %rsi
	subq	%r12, %rdi
	subq	%r12, %rsi
	movq	%rdi, %rdx
	shrq	$3, %rdx
	cmpq	$30, %rsi
	jbe	.L107
	movabsq	$2305843009213693948, %rcx
	testq	%rcx, %rdx
	je	.L107
	leaq	1(%rdx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %r8
	shrq	%r8
	salq	$4, %r8
.L79:
	movdqu	(%r12,%rdx), %xmm0
	movups	%xmm0, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%r8, %rdx
	jne	.L79
	movq	%rsi, %r8
	andq	$-2, %r8
	leaq	0(,%r8,8), %rdx
	addq	%rdx, %r12
	addq	%rax, %rdx
	cmpq	%rsi, %r8
	je	.L81
	movq	(%r12), %rsi
	movq	%rsi, (%rdx)
.L81:
	leaq	16(%rax,%rdi), %r13
.L77:
	movq	%rax, %r12
	jmp	.L65
.L158:
	testq	%rax, %rax
	jne	.L160
	movq	$0, -56(%rbp)
	movl	$8, %esi
	xorl	%eax, %eax
	jmp	.L74
.L103:
	movl	$8, %esi
	movl	$8, %ecx
	jmp	.L73
.L107:
	xorl	%esi, %esi
.L78:
	movq	(%r12,%rsi,8), %r8
	movq	%r8, (%rax,%rsi,8)
	movq	%rsi, %r8
	addq	$1, %rsi
	cmpq	%rdx, %r8
	jne	.L78
	jmp	.L81
.L106:
	movq	%rsi, %r13
	jmp	.L77
.L159:
	movq	-64(%rbp), %rdi
	movq	%rcx, -80(%rbp)
	movq	%rdx, -72(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %r8
	movq	-72(%rbp), %rdx
	movq	-80(%rbp), %rcx
	jmp	.L76
.L157:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L160:
	movl	$268435455, %ecx
	cmpq	$268435455, %rax
	cmova	%rcx, %rax
	leaq	0(,%rax,8), %rcx
	movq	%rcx, %rsi
	jmp	.L73
	.cfi_endproc
.LFE10639:
	.size	_ZN2v88internal8compiler19DeadCodeElimination17ReduceLoopOrMergeEPNS1_4NodeE, .-_ZN2v88internal8compiler19DeadCodeElimination17ReduceLoopOrMergeEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19DeadCodeElimination14RemoveLoopExitEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19DeadCodeElimination14RemoveLoopExitEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19DeadCodeElimination14RemoveLoopExitEPNS1_4NodeE, @function
_ZN2v88internal8compiler19DeadCodeElimination14RemoveLoopExitEPNS1_4NodeE:
.LFB10643:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	24(%rsi), %rbx
	testq	%rbx, %rbx
	je	.L166
	.p2align 4,,10
	.p2align 3
.L162:
	movl	16(%rbx), %edx
	movl	%edx, %eax
	shrl	%eax
	andl	$1, %edx
	leaq	3(%rax,%rax,2), %rax
	leaq	(%rbx,%rax,8), %rsi
	movq	(%rsi), %rax
	jne	.L163
	movq	%rax, %rsi
	movq	(%rax), %rax
.L163:
	movzwl	16(%rax), %eax
	subl	$53, %eax
	cmpl	$1, %eax
	ja	.L164
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L165
	movq	16(%rdx), %rdx
.L165:
	movq	8(%r13), %rdi
	movq	(%rdi), %rax
	call	*16(%rax)
.L164:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L162
.L166:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	8(%r13), %rdi
	movq	%r12, %rsi
	movq	%rax, %r14
	movq	(%rdi), %rax
	movq	%r14, %rdx
	call	*16(%rax)
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10643:
	.size	_ZN2v88internal8compiler19DeadCodeElimination14RemoveLoopExitEPNS1_4NodeE, .-_ZN2v88internal8compiler19DeadCodeElimination14RemoveLoopExitEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19DeadCodeElimination9ReducePhiEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19DeadCodeElimination9ReducePhiEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19DeadCodeElimination9ReducePhiEPNS1_4NodeE, @function
_ZN2v88internal8compiler19DeadCodeElimination9ReducePhiEPNS1_4NodeE:
.LFB10645:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	(%rax), %rdx
	cmpw	$61, 16(%rdx)
	je	.L173
	movq	(%r12), %rdi
	call	_ZN2v88internal8compiler19PhiRepresentationOfEPKNS1_8OperatorE@PLT
	movl	%eax, %r13d
	testb	%al, %al
	je	.L177
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties12GetTypeOrAnyEPNS1_4NodeE@PLT
	cmpq	$1, %rax
	je	.L177
	movq	(%r12), %rax
	movl	20(%rax), %r14d
	testl	%r14d, %r14d
	jle	.L181
	leaq	-64(%rbp), %rax
	xorl	%r15d, %r15d
	movq	%rax, -80(%rbp)
	jmp	.L187
	.p2align 4,,10
	.p2align 3
.L183:
	addl	$1, %r15d
	cmpl	%r15d, %r14d
	je	.L181
.L187:
	movq	%r12, %rdi
	movl	%r15d, %esi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	(%rax), %rdi
	cmpw	$60, 16(%rdi)
	jne	.L183
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal8compiler25DeadValueRepresentationOfEPKNS1_8OperatorE@PLT
	cmpb	%al, %r13b
	je	.L183
	movq	-72(%rbp), %r8
	movq	(%r8), %rdi
	cmpw	$60, 16(%rdi)
	je	.L195
.L185:
	movq	16(%rbx), %r10
	movq	24(%rbx), %rdi
	movl	%r13d, %esi
	movq	%r8, -88(%rbp)
	movq	%r10, -72(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9DeadValueENS0_21MachineRepresentationE@PLT
	movq	-88(%rbp), %r8
	movq	-72(%rbp), %r10
	movl	$1, %edx
	movq	-80(%rbp), %rcx
	movq	%rax, %rsi
	movq	%r8, -64(%rbp)
	movq	%r10, %rdi
	xorl	%r8d, %r8d
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	$1, 8(%rax)
	movq	%rax, %r8
.L186:
	movl	%r15d, %edx
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties17ReplaceValueInputEPNS1_4NodeES4_i@PLT
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L177:
	movq	(%r12), %rdi
	cmpw	$60, 16(%rdi)
	je	.L196
.L176:
	movq	24(%rbx), %rdi
	movq	16(%rbx), %r14
	movl	%r13d, %esi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9DeadValueENS0_21MachineRepresentationE@PLT
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r12, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	$1, 8(%rax)
	movq	%rax, %r12
.L180:
	movq	%r12, %rax
.L173:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L197
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L181:
	.cfi_restore_state
	xorl	%r12d, %r12d
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L196:
	call	_ZN2v88internal8compiler25DeadValueRepresentationOfEPKNS1_8OperatorE@PLT
	cmpb	%al, %r13b
	je	.L180
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rax, %r12
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L195:
	call	_ZN2v88internal8compiler25DeadValueRepresentationOfEPKNS1_8OperatorE@PLT
	movq	-72(%rbp), %r8
	cmpb	%al, %r13b
	je	.L186
	movq	%r8, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rax, %r8
	jmp	.L185
.L197:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10645:
	.size	_ZN2v88internal8compiler19DeadCodeElimination9ReducePhiEPNS1_4NodeE, .-_ZN2v88internal8compiler19DeadCodeElimination9ReducePhiEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19DeadCodeElimination15ReduceEffectPhiEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19DeadCodeElimination15ReduceEffectPhiEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19DeadCodeElimination15ReduceEffectPhiEPNS1_4NodeE, @function
_ZN2v88internal8compiler19DeadCodeElimination15ReduceEffectPhiEPNS1_4NodeE:
.LFB10646:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	(%rax), %rdx
	cmpw	$61, 16(%rdx)
	je	.L200
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	$0, -88(%rbp)
	movq	%rax, %r14
	movq	(%rbx), %rax
	movl	24(%rax), %r13d
	testl	%r13d, %r13d
	jle	.L201
	leaq	-80(%rbp), %rax
	xorl	%r15d, %r15d
	movq	%rax, -112(%rbp)
	jmp	.L203
	.p2align 4,,10
	.p2align 3
.L202:
	addl	$1, %r15d
	cmpl	%r15d, %r13d
	je	.L201
.L203:
	movl	%r15d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	%rax, %rdx
	movq	(%rax), %rax
	cmpw	$59, 16(%rax)
	jne	.L202
	movl	%r15d, %esi
	movq	%r14, %rdi
	movq	%rdx, -104(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	16(%r12), %r9
	movq	24(%r12), %rdi
	movq	%rax, -88(%rbp)
	movq	%r9, -96(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5ThrowEv@PLT
	movq	-104(%rbp), %rdx
	movq	-96(%rbp), %r9
	xorl	%r8d, %r8d
	movq	-112(%rbp), %rcx
	movq	%rax, %rsi
	movq	%rdx, %xmm0
	movq	%r9, %rdi
	movl	$2, %edx
	movhps	-88(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	24(%r12), %rsi
	movq	16(%r12), %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14NodeProperties17MergeControlToEndEPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS1_4NodeE@PLT
	movq	32(%r12), %rsi
	movl	%r15d, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties18ReplaceEffectInputEPNS1_4NodeES4_i@PLT
	movq	32(%r12), %rsi
	movl	%r15d, %edx
	movq	%r14, %rdi
	addl	$1, %r15d
	call	_ZN2v88internal8compiler14NodeProperties19ReplaceControlInputEPNS1_4NodeES4_i@PLT
	movq	8(%r12), %rdi
	movq	%r14, %rsi
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	16(%r12), %rax
	movq	8(%r12), %rdi
	movq	16(%rax), %rsi
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	%rbx, -88(%rbp)
	cmpl	%r15d, %r13d
	jne	.L203
	.p2align 4,,10
	.p2align 3
.L201:
	movq	-88(%rbp), %rax
.L200:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L208
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L208:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10646:
	.size	_ZN2v88internal8compiler19DeadCodeElimination15ReduceEffectPhiEPNS1_4NodeE, .-_ZN2v88internal8compiler19DeadCodeElimination15ReduceEffectPhiEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19DeadCodeElimination14ReducePureNodeEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19DeadCodeElimination14ReducePureNodeEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19DeadCodeElimination14ReducePureNodeEPNS1_4NodeE, @function
_ZN2v88internal8compiler19DeadCodeElimination14ReducePureNodeEPNS1_4NodeE:
.LFB10647:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	cmpw	$60, 16(%rax)
	jne	.L210
	xorl	%eax, %eax
.L211:
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L237
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L210:
	.cfi_restore_state
	movzbl	23(%rsi), %eax
	movq	%rdi, %r13
	leaq	32(%rsi), %rbx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L213
	movq	32(%rsi), %rbx
	movl	8(%rbx), %eax
	addq	$16, %rbx
.L213:
	cltq
	leaq	(%rbx,%rax,8), %r14
	cmpq	%r14, %rbx
	je	.L214
	.p2align 4,,10
	.p2align 3
.L221:
	movq	(%rbx), %r12
	movq	(%r12), %rdi
	movzwl	16(%rdi), %eax
	cmpl	$61, %eax
	je	.L216
	cmpl	$59, %eax
	je	.L216
	cmpl	$60, %eax
	jne	.L238
.L220:
	call	_ZN2v88internal8compiler25DeadValueRepresentationOfEPKNS1_8OperatorE@PLT
	testb	%al, %al
	je	.L223
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rax, %r12
	.p2align 4,,10
	.p2align 3
.L216:
	movq	24(%r13), %rdi
	movq	16(%r13), %r14
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9DeadValueENS0_21MachineRepresentationE@PLT
	leaq	-48(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r12, -48(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	$1, 8(%rax)
	movq	%rax, %r12
.L223:
	movq	%r12, %rax
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L238:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties12GetTypeOrAnyEPNS1_4NodeE@PLT
	cmpq	$1, %rax
	je	.L239
	addq	$8, %rbx
	cmpq	%rbx, %r14
	jne	.L221
.L214:
	xorl	%r12d, %r12d
	movq	%r12, %rax
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L239:
	movq	(%r12), %rdi
	cmpw	$60, 16(%rdi)
	je	.L220
	jmp	.L216
.L237:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10647:
	.size	_ZN2v88internal8compiler19DeadCodeElimination14ReducePureNodeEPNS1_4NodeE, .-_ZN2v88internal8compiler19DeadCodeElimination14ReducePureNodeEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19DeadCodeElimination30ReduceUnreachableOrIfExceptionEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19DeadCodeElimination30ReduceUnreachableOrIfExceptionEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19DeadCodeElimination30ReduceUnreachableOrIfExceptionEPNS1_4NodeE, @function
_ZN2v88internal8compiler19DeadCodeElimination30ReduceUnreachableOrIfExceptionEPNS1_4NodeE:
.LFB10648:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	xorl	%esi, %esi
	movq	%r12, %rdi
	subq	$8, %rsp
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	(%rax), %rdx
	cmpw	$61, 16(%rdx)
	je	.L242
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	(%rax), %rdx
	movzwl	16(%rdx), %edx
	subl	$59, %edx
	testw	$-3, %dx
	movl	$0, %edx
	cmovne	%rdx, %rax
.L242:
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10648:
	.size	_ZN2v88internal8compiler19DeadCodeElimination30ReduceUnreachableOrIfExceptionEPNS1_4NodeE, .-_ZN2v88internal8compiler19DeadCodeElimination30ReduceUnreachableOrIfExceptionEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19DeadCodeElimination16ReduceEffectNodeEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19DeadCodeElimination16ReduceEffectNodeEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19DeadCodeElimination16ReduceEffectNodeEPNS1_4NodeE, @function
_ZN2v88internal8compiler19DeadCodeElimination16ReduceEffectNodeEPNS1_4NodeE:
.LFB10649:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	movq	%r12, %rdi
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	%rax, %rbx
	movq	(%rax), %rax
	cmpw	$61, 16(%rax)
	jne	.L247
.L283:
	movq	%rbx, %rax
.L248:
	movq	-56(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L284
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L247:
	.cfi_restore_state
	movzbl	23(%r12), %eax
	leaq	32(%r12), %r15
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L250
	movq	32(%r12), %r15
	movl	8(%r15), %eax
	addq	$16, %r15
.L250:
	cltq
	leaq	(%r15,%rax,8), %rax
	movq	%rax, -88(%rbp)
	cmpq	%rax, %r15
	jne	.L256
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L252:
	cmpl	$59, %eax
	je	.L255
	cmpl	$60, %eax
	je	.L255
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler14NodeProperties12GetTypeOrAnyEPNS1_4NodeE@PLT
	cmpq	$1, %rax
	je	.L255
	addq	$8, %r15
	cmpq	%r15, -88(%rbp)
	je	.L251
.L256:
	movq	(%r15), %r14
	movq	(%r14), %rax
	movzwl	16(%rax), %eax
	cmpl	$61, %eax
	jne	.L252
.L255:
	movq	(%rbx), %rax
	cmpw	$59, 16(%rax)
	je	.L285
	movq	(%r12), %rax
	cmpl	$1, 28(%rax)
	je	.L286
	movq	16(%r13), %r10
	movq	8(%r10), %rax
	movq	%rax, -88(%rbp)
.L260:
	movq	24(%r13), %rdi
	movq	%r10, -96(%rbp)
	leaq	-80(%rbp), %r15
	call	_ZN2v88internal8compiler21CommonOperatorBuilder11UnreachableEv@PLT
	movq	-96(%rbp), %r10
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%rbx, %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movhps	-88(%rbp), %xmm0
	movq	%r10, %rdi
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	$1, 8(%rax)
	movq	(%r14), %rdi
	movq	%rax, %rbx
	cmpw	$60, 16(%rdi)
	je	.L287
.L261:
	movq	16(%r13), %r10
	movq	24(%r13), %rdi
	xorl	%esi, %esi
	movq	%r10, -96(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9DeadValueENS0_21MachineRepresentationE@PLT
	movq	-96(%rbp), %r10
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r14, -80(%rbp)
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	$1, 8(%rax)
	movq	%rax, %r14
.L262:
	movq	8(%r13), %rdi
	movq	-88(%rbp), %r8
	movq	%r12, %rcx
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	jmp	.L283
	.p2align 4,,10
	.p2align 3
.L251:
	xorl	%eax, %eax
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L285:
	movq	8(%r13), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdx
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r14), %rdi
	cmpw	$60, 16(%rdi)
	je	.L288
.L257:
	movq	24(%r13), %rdi
	movq	16(%r13), %r12
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9DeadValueENS0_21MachineRepresentationE@PLT
	leaq	-80(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r14, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	$1, 8(%rax)
	movq	%rax, %r14
.L258:
	movq	%r14, %rax
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L287:
	call	_ZN2v88internal8compiler25DeadValueRepresentationOfEPKNS1_8OperatorE@PLT
	testb	%al, %al
	je	.L262
	movq	%r14, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rax, %r14
	jmp	.L261
	.p2align 4,,10
	.p2align 3
.L286:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	16(%r13), %r10
	movq	%rax, -88(%rbp)
	jmp	.L260
	.p2align 4,,10
	.p2align 3
.L288:
	call	_ZN2v88internal8compiler25DeadValueRepresentationOfEPKNS1_8OperatorE@PLT
	testb	%al, %al
	je	.L258
	movq	%r14, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rax, %r14
	jmp	.L257
.L284:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10649:
	.size	_ZN2v88internal8compiler19DeadCodeElimination16ReduceEffectNodeEPNS1_4NodeE, .-_ZN2v88internal8compiler19DeadCodeElimination16ReduceEffectNodeEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19DeadCodeElimination10ReduceNodeEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19DeadCodeElimination10ReduceNodeEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19DeadCodeElimination10ReduceNodeEPNS1_4NodeE, @function
_ZN2v88internal8compiler19DeadCodeElimination10ReduceNodeEPNS1_4NodeE:
.LFB10644:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movl	28(%rax), %edx
	movl	24(%rax), %ebx
	cmpl	$1, %edx
	je	.L331
	testl	%ebx, %ebx
	je	.L332
.L293:
	testl	%ebx, %ebx
	jle	.L295
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler19DeadCodeElimination16ReduceEffectNodeEPNS1_4NodeE
.L292:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L333
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L332:
	.cfi_restore_state
	testl	%edx, %edx
	jne	.L308
.L294:
	xorl	%r12d, %r12d
	cmpw	$60, 16(%rax)
	jne	.L334
.L296:
	movq	%r12, %rax
	jmp	.L292
	.p2align 4,,10
	.p2align 3
.L331:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	(%rax), %rdx
	cmpw	$61, 16(%rdx)
	je	.L292
	testl	%ebx, %ebx
	jne	.L293
	movq	0(%r13), %rax
.L308:
	movl	40(%rax), %edx
	testl	%edx, %edx
	je	.L294
.L295:
	xorl	%eax, %eax
	jmp	.L292
	.p2align 4,,10
	.p2align 3
.L334:
	movzbl	23(%r13), %eax
	leaq	32(%r13), %rbx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L298
	movq	32(%r13), %rbx
	movl	8(%rbx), %eax
	addq	$16, %rbx
.L298:
	cltq
	leaq	(%rbx,%rax,8), %r13
	cmpq	%r13, %rbx
	je	.L299
	.p2align 4,,10
	.p2align 3
.L305:
	movq	(%rbx), %r12
	movq	(%r12), %rdi
	movzwl	16(%rdi), %eax
	cmpl	$61, %eax
	je	.L304
	cmpl	$59, %eax
	je	.L304
	cmpl	$60, %eax
	jne	.L335
.L303:
	call	_ZN2v88internal8compiler25DeadValueRepresentationOfEPKNS1_8OperatorE@PLT
	testb	%al, %al
	je	.L296
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rax, %r12
	.p2align 4,,10
	.p2align 3
.L304:
	movq	24(%r14), %rdi
	movq	16(%r14), %r13
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9DeadValueENS0_21MachineRepresentationE@PLT
	leaq	-48(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r12, -48(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	$1, 8(%rax)
	movq	%rax, %r12
	jmp	.L296
	.p2align 4,,10
	.p2align 3
.L335:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties12GetTypeOrAnyEPNS1_4NodeE@PLT
	cmpq	$1, %rax
	je	.L336
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jne	.L305
.L299:
	xorl	%r12d, %r12d
	jmp	.L296
	.p2align 4,,10
	.p2align 3
.L336:
	movq	(%r12), %rdi
	cmpw	$60, 16(%rdi)
	je	.L303
	jmp	.L304
.L333:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10644:
	.size	_ZN2v88internal8compiler19DeadCodeElimination10ReduceNodeEPNS1_4NodeE, .-_ZN2v88internal8compiler19DeadCodeElimination10ReduceNodeEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19DeadCodeElimination45ReduceDeoptimizeOrReturnOrTerminateOrTailCallEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19DeadCodeElimination45ReduceDeoptimizeOrReturnOrTerminateOrTailCallEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19DeadCodeElimination45ReduceDeoptimizeOrReturnOrTerminateOrTailCallEPNS1_4NodeE, @function
_ZN2v88internal8compiler19DeadCodeElimination45ReduceDeoptimizeOrReturnOrTerminateOrTailCallEPNS1_4NodeE:
.LFB10650:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	(%rax), %rdx
	cmpw	$61, 16(%rdx)
	je	.L339
	movzbl	23(%rbx), %eax
	leaq	32(%rbx), %r14
	movq	%r14, %r12
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L371
.L341:
	cltq
	leaq	(%r12,%rax,8), %r15
	cmpq	%r15, %r12
	jne	.L347
	jmp	.L342
	.p2align 4,,10
	.p2align 3
.L372:
	call	_ZN2v88internal8compiler14NodeProperties12GetTypeOrAnyEPNS1_4NodeE@PLT
	cmpq	$1, %rax
	je	.L343
	addq	$8, %r12
	cmpq	%r12, %r15
	je	.L342
.L347:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	movzwl	16(%rax), %eax
	subl	$59, %eax
	cmpw	$2, %ax
	ja	.L372
.L343:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r15
	movq	(%r12), %rax
	cmpw	$59, 16(%rax)
	je	.L346
	movq	%r15, %xmm1
	movq	24(%r13), %rdi
	movq	%r12, %xmm0
	movq	16(%r13), %r12
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder11UnreachableEv@PLT
	movdqa	-96(%rbp), %xmm0
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	leaq	-80(%rbp), %rcx
	movl	$2, %edx
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	$1, 8(%rax)
	movq	%rax, %r12
	.p2align 4,,10
	.p2align 3
.L346:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler4Node14TrimInputCountEi@PLT
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L348
	movq	32(%rbx), %rdi
	movq	%r14, %rax
	movq	%rbx, %rsi
	cmpq	%rdi, %r12
	je	.L350
.L349:
	subq	$24, %rsi
	testq	%rdi, %rdi
	je	.L352
	movq	%rax, -104(%rbp)
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-96(%rbp), %rsi
	movq	-104(%rbp), %rax
.L352:
	movq	%r12, (%rax)
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L373
.L350:
	movq	8(%r14), %rdi
	cmpq	%rdi, %r15
	je	.L355
	addq	$8, %r14
	movq	%rbx, %rsi
.L354:
	leaq	-48(%rsi), %r12
	testq	%rdi, %rdi
	je	.L356
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L356:
	movq	%r15, (%r14)
	testq	%r15, %r15
	je	.L355
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L355:
	movq	24(%r13), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5ThrowEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	%rbx, %rax
	.p2align 4,,10
	.p2align 3
.L339:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L374
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L371:
	.cfi_restore_state
	movq	32(%rbx), %r12
	movl	8(%r12), %eax
	addq	$16, %r12
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L342:
	xorl	%eax, %eax
	jmp	.L339
	.p2align 4,,10
	.p2align 3
.L373:
	movq	32(%rbx), %rsi
	leaq	16(%rsi), %rax
.L351:
	movq	8(%rax), %rdi
	cmpq	%rdi, %r15
	je	.L355
	leaq	8(%rax), %r14
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L348:
	movq	32(%rbx), %rsi
	movq	16(%rsi), %rdi
	leaq	16(%rsi), %rax
	cmpq	%rdi, %r12
	jne	.L349
	jmp	.L351
.L374:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10650:
	.size	_ZN2v88internal8compiler19DeadCodeElimination45ReduceDeoptimizeOrReturnOrTerminateOrTailCallEPNS1_4NodeE, .-_ZN2v88internal8compiler19DeadCodeElimination45ReduceDeoptimizeOrReturnOrTerminateOrTailCallEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19DeadCodeElimination14ReduceLoopExitEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19DeadCodeElimination14ReduceLoopExitEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19DeadCodeElimination14ReduceLoopExitEPNS1_4NodeE, @function
_ZN2v88internal8compiler19DeadCodeElimination14ReduceLoopExitEPNS1_4NodeE:
.LFB10651:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	movq	%r12, %rdi
	subq	$8, %rsp
	.cfi_offset 3, -40
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	(%rbx), %rdx
	cmpw	$61, 16(%rdx)
	je	.L376
	movq	(%rax), %rax
	cmpw	$61, 16(%rax)
	je	.L376
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L376:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler19DeadCodeElimination14RemoveLoopExitEPNS1_4NodeE
	.cfi_endproc
.LFE10651:
	.size	_ZN2v88internal8compiler19DeadCodeElimination14ReduceLoopExitEPNS1_4NodeE, .-_ZN2v88internal8compiler19DeadCodeElimination14ReduceLoopExitEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19DeadCodeElimination20ReduceBranchOrSwitchEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19DeadCodeElimination20ReduceBranchOrSwitchEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19DeadCodeElimination20ReduceBranchOrSwitchEPNS1_4NodeE, @function
_ZN2v88internal8compiler19DeadCodeElimination20ReduceBranchOrSwitchEPNS1_4NodeE:
.LFB10652:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$24, %rsp
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	(%rax), %rdx
	cmpw	$61, 16(%rdx)
	je	.L381
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	(%rax), %rdx
	xorl	%eax, %eax
	cmpw	$60, 16(%rdx)
	je	.L386
.L381:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L386:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	40(%rbx), %rdi
	movslq	40(%rax), %rdx
	movq	16(%rdi), %r13
	movq	24(%rdi), %rax
	leaq	0(,%rdx,8), %rsi
	subq	%r13, %rax
	cmpq	%rax, %rsi
	ja	.L387
	addq	%r13, %rsi
	movq	%rsi, 16(%rdi)
.L384:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties25CollectControlProjectionsEPNS1_4NodeEPS4_m@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	8(%rbx), %rdi
	movq	0(%r13), %rsi
	movq	%rax, %rdx
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	32(%rbx), %rax
	jmp	.L381
.L387:
	movq	%rdx, -40(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-40(%rbp), %rdx
	movq	%rax, %r13
	jmp	.L384
	.cfi_endproc
.LFE10652:
	.size	_ZN2v88internal8compiler19DeadCodeElimination20ReduceBranchOrSwitchEPNS1_4NodeE, .-_ZN2v88internal8compiler19DeadCodeElimination20ReduceBranchOrSwitchEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19DeadCodeElimination6ReduceEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19DeadCodeElimination6ReduceEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19DeadCodeElimination6ReduceEPNS1_4NodeE, @function
_ZN2v88internal8compiler19DeadCodeElimination6ReduceEPNS1_4NodeE:
.LFB10633:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	(%rsi), %rax
	cmpw	$59, 16(%rax)
	ja	.L389
	movzwl	16(%rax), %eax
	leaq	.L391(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler19DeadCodeElimination6ReduceEPNS1_4NodeE,"a",@progbits
	.align 4
	.align 4
.L391:
	.long	.L389-.L391
	.long	.L398-.L391
	.long	.L399-.L391
	.long	.L399-.L391
	.long	.L389-.L391
	.long	.L389-.L391
	.long	.L389-.L391
	.long	.L390-.L391
	.long	.L389-.L391
	.long	.L389-.L391
	.long	.L398-.L391
	.long	.L397-.L391
	.long	.L389-.L391
	.long	.L389-.L391
	.long	.L389-.L391
	.long	.L389-.L391
	.long	.L397-.L391
	.long	.L397-.L391
	.long	.L397-.L391
	.long	.L389-.L391
	.long	.L389-.L391
	.long	.L396-.L391
	.long	.L395-.L391
	.long	.L389-.L391
	.long	.L389-.L391
	.long	.L389-.L391
	.long	.L389-.L391
	.long	.L389-.L391
	.long	.L389-.L391
	.long	.L389-.L391
	.long	.L389-.L391
	.long	.L389-.L391
	.long	.L389-.L391
	.long	.L389-.L391
	.long	.L389-.L391
	.long	.L394-.L391
	.long	.L393-.L391
	.long	.L389-.L391
	.long	.L389-.L391
	.long	.L389-.L391
	.long	.L389-.L391
	.long	.L389-.L391
	.long	.L389-.L391
	.long	.L389-.L391
	.long	.L389-.L391
	.long	.L389-.L391
	.long	.L389-.L391
	.long	.L389-.L391
	.long	.L389-.L391
	.long	.L389-.L391
	.long	.L389-.L391
	.long	.L389-.L391
	.long	.L392-.L391
	.long	.L389-.L391
	.long	.L389-.L391
	.long	.L389-.L391
	.long	.L389-.L391
	.long	.L389-.L391
	.long	.L389-.L391
	.long	.L390-.L391
	.section	.text._ZN2v88internal8compiler19DeadCodeElimination6ReduceEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L389:
	addq	$8, %rsp
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler19DeadCodeElimination10ReduceNodeEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L397:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler19DeadCodeElimination45ReduceDeoptimizeOrReturnOrTerminateOrTailCallEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L390:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	(%rax), %rdx
	cmpw	$61, 16(%rdx)
	je	.L402
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	(%rax), %rdx
	movzwl	16(%rdx), %edx
	subl	$59, %edx
	testw	$-3, %dx
	movl	$0, %edx
	cmovne	%rdx, %rax
.L402:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L398:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler19DeadCodeElimination17ReduceLoopOrMergeEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L399:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler19DeadCodeElimination20ReduceBranchOrSwitchEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L396:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	(%rax), %rdx
	cmpw	$61, 16(%rdx)
	movl	$0, %edx
	cmovne	%rdx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L395:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler19DeadCodeElimination9ReduceEndEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L394:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler19DeadCodeElimination9ReducePhiEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L393:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler19DeadCodeElimination15ReduceEffectPhiEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L392:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	(%rbx), %rdx
	cmpw	$61, 16(%rdx)
	je	.L400
	movq	(%rax), %rdx
	xorl	%eax, %eax
	cmpw	$61, 16(%rdx)
	jne	.L402
.L400:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19DeadCodeElimination14RemoveLoopExitEPNS1_4NodeE
	jmp	.L402
	.cfi_endproc
.LFE10633:
	.size	_ZN2v88internal8compiler19DeadCodeElimination6ReduceEPNS1_4NodeE, .-_ZN2v88internal8compiler19DeadCodeElimination6ReduceEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19DeadCodeElimination14TrimMergeOrPhiEPNS1_4NodeEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19DeadCodeElimination14TrimMergeOrPhiEPNS1_4NodeEi
	.type	_ZN2v88internal8compiler19DeadCodeElimination14TrimMergeOrPhiEPNS1_4NodeEi, @function
_ZN2v88internal8compiler19DeadCodeElimination14TrimMergeOrPhiEPNS1_4NodeEi:
.LFB10653:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	movq	24(%rdi), %rdi
	movq	(%rsi), %rsi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder16ResizeMergeOrPhiEPKNS1_8OperatorEi@PLT
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler18OperatorProperties18GetTotalInputCountEPKNS1_8OperatorE@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler4Node14TrimInputCountEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	.cfi_endproc
.LFE10653:
	.size	_ZN2v88internal8compiler19DeadCodeElimination14TrimMergeOrPhiEPNS1_4NodeEi, .-_ZN2v88internal8compiler19DeadCodeElimination14TrimMergeOrPhiEPNS1_4NodeEi
	.section	.text._ZN2v88internal8compiler19DeadCodeElimination9DeadValueEPNS1_4NodeENS0_21MachineRepresentationE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19DeadCodeElimination9DeadValueEPNS1_4NodeENS0_21MachineRepresentationE
	.type	_ZN2v88internal8compiler19DeadCodeElimination9DeadValueEPNS1_4NodeENS0_21MachineRepresentationE, @function
_ZN2v88internal8compiler19DeadCodeElimination9DeadValueEPNS1_4NodeENS0_21MachineRepresentationE:
.LFB10654:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpw	$60, 16(%rdi)
	je	.L417
.L412:
	movq	24(%rbx), %rdi
	movq	16(%rbx), %r14
	movl	%r13d, %esi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9DeadValueENS0_21MachineRepresentationE@PLT
	leaq	-48(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r12, -48(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	$1, 8(%rax)
.L411:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L418
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L417:
	.cfi_restore_state
	call	_ZN2v88internal8compiler25DeadValueRepresentationOfEPKNS1_8OperatorE@PLT
	movl	%eax, %r8d
	movq	%r12, %rax
	cmpb	%r13b, %r8b
	je	.L411
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rax, %r12
	jmp	.L412
.L418:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10654:
	.size	_ZN2v88internal8compiler19DeadCodeElimination9DeadValueEPNS1_4NodeENS0_21MachineRepresentationE, .-_ZN2v88internal8compiler19DeadCodeElimination9DeadValueEPNS1_4NodeENS0_21MachineRepresentationE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler19DeadCodeEliminationC2EPNS1_15AdvancedReducer6EditorEPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS0_4ZoneE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler19DeadCodeEliminationC2EPNS1_15AdvancedReducer6EditorEPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS0_4ZoneE, @function
_GLOBAL__sub_I__ZN2v88internal8compiler19DeadCodeEliminationC2EPNS1_15AdvancedReducer6EditorEPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS0_4ZoneE:
.LFB12383:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE12383:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler19DeadCodeEliminationC2EPNS1_15AdvancedReducer6EditorEPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS0_4ZoneE, .-_GLOBAL__sub_I__ZN2v88internal8compiler19DeadCodeEliminationC2EPNS1_15AdvancedReducer6EditorEPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS0_4ZoneE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler19DeadCodeEliminationC2EPNS1_15AdvancedReducer6EditorEPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS0_4ZoneE
	.weak	_ZTVN2v88internal8compiler19DeadCodeEliminationE
	.section	.data.rel.ro._ZTVN2v88internal8compiler19DeadCodeEliminationE,"awG",@progbits,_ZTVN2v88internal8compiler19DeadCodeEliminationE,comdat
	.align 8
	.type	_ZTVN2v88internal8compiler19DeadCodeEliminationE, @object
	.size	_ZTVN2v88internal8compiler19DeadCodeEliminationE, 56
_ZTVN2v88internal8compiler19DeadCodeEliminationE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal8compiler19DeadCodeEliminationD1Ev
	.quad	_ZN2v88internal8compiler19DeadCodeEliminationD0Ev
	.quad	_ZNK2v88internal8compiler19DeadCodeElimination12reducer_nameEv
	.quad	_ZN2v88internal8compiler19DeadCodeElimination6ReduceEPNS1_4NodeE
	.quad	_ZN2v88internal8compiler7Reducer8FinalizeEv
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
