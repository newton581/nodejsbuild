	.file	"code-generator.cc"
	.text
	.section	.text._ZN2v88internal6Logger27is_listening_to_code_eventsEv,"axG",@progbits,_ZN2v88internal6Logger27is_listening_to_code_eventsEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6Logger27is_listening_to_code_eventsEv
	.type	_ZN2v88internal6Logger27is_listening_to_code_eventsEv, @function
_ZN2v88internal6Logger27is_listening_to_code_eventsEv:
.LFB24142:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	jne	.L1
	cmpq	$0, 80(%rbx)
	setne	%al
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24142:
	.size	_ZN2v88internal6Logger27is_listening_to_code_eventsEv, .-_ZN2v88internal6Logger27is_listening_to_code_eventsEv
	.section	.text._ZNK2v88internal8compiler13CodeGenerator30wasm_runtime_exception_supportEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler13CodeGenerator30wasm_runtime_exception_supportEv
	.type	_ZNK2v88internal8compiler13CodeGenerator30wasm_runtime_exception_supportEv, @function
_ZNK2v88internal8compiler13CodeGenerator30wasm_runtime_exception_supportEv:
.LFB24248:
	.cfi_startproc
	endbr64
	movq	152(%rdi), %rax
	movl	(%rax), %eax
	shrl	$19, %eax
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE24248:
	.size	_ZNK2v88internal8compiler13CodeGenerator30wasm_runtime_exception_supportEv, .-_ZNK2v88internal8compiler13CodeGenerator30wasm_runtime_exception_supportEv
	.section	.rodata._ZN2v88internal8compiler13CodeGenerator30AddProtectedInstructionLandingEjj.str1.1,"aMS",@progbits,1
.LC0:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZN2v88internal8compiler13CodeGenerator30AddProtectedInstructionLandingEjj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13CodeGenerator30AddProtectedInstructionLandingEjj
	.type	_ZN2v88internal8compiler13CodeGenerator30AddProtectedInstructionLandingEjj, @function
_ZN2v88internal8compiler13CodeGenerator30AddProtectedInstructionLandingEjj:
.LFB24249:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	1224(%rdi), %r12
	cmpq	1232(%rdi), %r12
	je	.L7
	movl	%esi, (%r12)
	movl	%edx, 4(%r12)
	addq	$8, 1224(%rdi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
	movq	1216(%rdi), %r14
	movq	%r12, %r15
	subq	%r14, %r15
	movq	%r15, %rax
	sarq	$3, %rax
	cmpq	$268435455, %rax
	je	.L32
	testq	%rax, %rax
	je	.L19
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L33
	movl	$2147483640, %esi
	movl	$2147483640, %ecx
.L10:
	movq	1208(%rbx), %r8
	movq	16(%r8), %rax
	movq	24(%r8), %rdi
	subq	%rax, %rdi
	cmpq	%rdi, %rsi
	ja	.L34
	addq	%rax, %rsi
	movq	%rsi, 16(%r8)
.L13:
	leaq	(%rax,%rcx), %rsi
	leaq	8(%rax), %rcx
.L11:
	addq	%rax, %r15
	movl	%r13d, (%r15)
	movl	%edx, 4(%r15)
	cmpq	%r14, %r12
	je	.L14
	leaq	-8(%r12), %rdi
	leaq	15(%rax), %rcx
	movq	%r14, %rdx
	subq	%r14, %rdi
	subq	%r14, %rcx
	movq	%rdi, %r8
	shrq	$3, %r8
	cmpq	$30, %rcx
	jbe	.L22
	movabsq	$2305843009213693950, %rcx
	testq	%rcx, %r8
	je	.L22
	addq	$1, %r8
	xorl	%edx, %edx
	movq	%r8, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L16:
	movdqu	(%r14,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L16
	movq	%r8, %r9
	andq	$-2, %r9
	leaq	0(,%r9,8), %rcx
	leaq	(%r14,%rcx), %rdx
	addq	%rax, %rcx
	cmpq	%r9, %r8
	je	.L18
	movq	(%rdx), %rdx
	movq	%rdx, (%rcx)
.L18:
	leaq	16(%rax,%rdi), %rcx
.L14:
	movq	%rax, %xmm0
	movq	%rcx, %xmm2
	movq	%rsi, 1232(%rbx)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 1216(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	.cfi_restore_state
	testq	%rcx, %rcx
	jne	.L35
	movl	$8, %ecx
	xorl	%esi, %esi
	xorl	%eax, %eax
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L19:
	movl	$8, %esi
	movl	$8, %ecx
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L22:
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L15:
	movl	(%rdx), %r9d
	movl	4(%rdx), %r8d
	addq	$8, %rdx
	addq	$8, %rcx
	movl	%r9d, -8(%rcx)
	movl	%r8d, -4(%rcx)
	cmpq	%rdx, %r12
	jne	.L15
	jmp	.L18
.L34:
	movq	%r8, %rdi
	movl	%edx, -60(%rbp)
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	movl	-60(%rbp), %edx
	jmp	.L13
.L32:
	leaq	.LC0(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L35:
	cmpq	$268435455, %rcx
	movl	$268435455, %eax
	cmova	%rax, %rcx
	salq	$3, %rcx
	movq	%rcx, %rsi
	jmp	.L10
	.cfi_endproc
.LFE24249:
	.size	_ZN2v88internal8compiler13CodeGenerator30AddProtectedInstructionLandingEjj, .-_ZN2v88internal8compiler13CodeGenerator30AddProtectedInstructionLandingEjj
	.section	.text._ZN2v88internal8compiler13CodeGenerator22CreateFrameAccessStateEPNS1_5FrameE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13CodeGenerator22CreateFrameAccessStateEPNS1_5FrameE
	.type	_ZN2v88internal8compiler13CodeGenerator22CreateFrameAccessStateEPNS1_5FrameE, @function
_ZN2v88internal8compiler13CodeGenerator22CreateFrameAccessStateEPNS1_5FrameE:
.LFB24250:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZN2v88internal8compiler13CodeGenerator11FinishFrameEPNS1_5FrameE@PLT
	movq	8(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$23, %rdx
	jbe	.L40
	leaq	24(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L38:
	movq	%r12, (%rax)
	movb	$0, 8(%rax)
	movl	$0, 12(%rax)
	movb	$0, 16(%rax)
	movq	%rax, 24(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	.cfi_restore_state
	movl	$24, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L38
	.cfi_endproc
.LFE24250:
	.size	_ZN2v88internal8compiler13CodeGenerator22CreateFrameAccessStateEPNS1_5FrameE, .-_ZN2v88internal8compiler13CodeGenerator22CreateFrameAccessStateEPNS1_5FrameE
	.section	.text._ZN2v88internal8compiler13CodeGenerator23AssembleDeoptimizerCallEPNS1_18DeoptimizationExitE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13CodeGenerator23AssembleDeoptimizerCallEPNS1_18DeoptimizationExitE
	.type	_ZN2v88internal8compiler13CodeGenerator23AssembleDeoptimizerCallEPNS1_18DeoptimizationExitE, @function
_ZN2v88internal8compiler13CodeGenerator23AssembleDeoptimizerCallEPNS1_18DeoptimizationExitE:
.LFB24251:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rsi), %r13d
	cmpl	$16384, %r13d
	jle	.L50
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	.cfi_restore_state
	movq	%rdi, %r12
	movzbl	37(%rsi), %r15d
	movq	%rsi, %rbx
	movq	712(%rdi), %rdi
	movzbl	36(%rsi), %esi
	call	_ZN2v88internal11Deoptimizer22GetDeoptimizationEntryEPNS0_7IsolateENS0_14DeoptimizeKindE@PLT
	leaq	200(%r12), %rdi
	movq	%rax, %r14
	movq	152(%r12), %rax
	testb	$32, (%rax)
	jne	.L51
.L43:
	movl	%r13d, %edx
	movq	%r14, %rsi
	call	_ZN2v88internal14TurboAssembler21CallForDeoptimizationEmi@PLT
	movb	$1, 38(%rbx)
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L51:
	.cfi_restore_state
	movq	8(%rbx), %rdx
	movl	%r13d, %ecx
	movl	%r15d, %esi
	movq	%rdi, -56(%rbp)
	call	_ZN2v88internal9Assembler17RecordDeoptReasonENS0_16DeoptimizeReasonENS0_14SourcePositionEi@PLT
	movq	-56(%rbp), %rdi
	jmp	.L43
	.cfi_endproc
.LFE24251:
	.size	_ZN2v88internal8compiler13CodeGenerator23AssembleDeoptimizerCallEPNS1_18DeoptimizationExitE, .-_ZN2v88internal8compiler13CodeGenerator23AssembleDeoptimizerCallEPNS1_18DeoptimizationExitE
	.section	.text._ZN2v88internal8compiler13CodeGenerator30MaybeEmitOutOfLineConstantPoolEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13CodeGenerator30MaybeEmitOutOfLineConstantPoolEv
	.type	_ZN2v88internal8compiler13CodeGenerator30MaybeEmitOutOfLineConstantPoolEv, @function
_ZN2v88internal8compiler13CodeGenerator30MaybeEmitOutOfLineConstantPoolEv:
.LFB24252:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE24252:
	.size	_ZN2v88internal8compiler13CodeGenerator30MaybeEmitOutOfLineConstantPoolEv, .-_ZN2v88internal8compiler13CodeGenerator30MaybeEmitOutOfLineConstantPoolEv
	.section	.text._ZN2v88internal8compiler13CodeGenerator35AssembleArchBinarySearchSwitchRangeENS0_8RegisterENS1_9RpoNumberEPSt4pairIiPNS0_5LabelEES9_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13CodeGenerator35AssembleArchBinarySearchSwitchRangeENS0_8RegisterENS1_9RpoNumberEPSt4pairIiPNS0_5LabelEES9_
	.type	_ZN2v88internal8compiler13CodeGenerator35AssembleArchBinarySearchSwitchRangeENS0_8RegisterENS1_9RpoNumberEPSt4pairIiPNS0_5LabelEES9_, @function
_ZN2v88internal8compiler13CodeGenerator35AssembleArchBinarySearchSwitchRangeENS0_8RegisterENS1_9RpoNumberEPSt4pairIiPNS0_5LabelEES9_:
.LFB24255:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, -84(%rbp)
	movq	%r8, %rdx
	movq	%rdi, -80(%rbp)
	subq	%rcx, %rdx
	movq	%r8, -72(%rbp)
	movq	%fs:40, %rbx
	movq	%rbx, -56(%rbp)
	xorl	%ebx, %ebx
	cmpq	$48, %rdx
	jg	.L54
	leaq	200(%rdi), %r12
	cmpq	%rcx, %r8
	je	.L57
	.p2align 4,,10
	.p2align 3
.L58:
	movl	(%r15), %eax
	movq	8(%r15), %rbx
	movl	$4, %r8d
	movl	%r13d, %edx
	movabsq	$-4294967296, %rsi
	movq	%r12, %rdi
	addq	$16, %r15
	andq	%rsi, %r14
	movl	$7, %esi
	orq	%rax, %r14
	movabsq	$-1095216660481, %rax
	andq	%rax, %r14
	movabsq	$81604378624, %rax
	orq	%rax, %r14
	movq	%r14, %rcx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	movl	$1, %ecx
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movl	$4, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	cmpq	%r15, -72(%rbp)
	jne	.L58
.L57:
	movl	-84(%rbp), %esi
	movq	-80(%rbp), %rdi
	call	_ZN2v88internal8compiler13CodeGenerator16AssembleArchJumpENS1_9RpoNumberE@PLT
.L53:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L63
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	.cfi_restore_state
	sarq	$5, %rdx
	movl	$4, %r8d
	movabsq	$81604378624, %rax
	movq	$0, -64(%rbp)
	salq	$4, %rdx
	leaq	200(%rdi), %r14
	leaq	-64(%rbp), %rbx
	leaq	(%rcx,%rdx), %r12
	movq	%r14, %rdi
	movl	%esi, %edx
	movl	$7, %esi
	movl	(%r12), %ecx
	orq	%rax, %rcx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	movq	%rbx, %rdx
	movq	%r14, %rdi
	movl	$1, %ecx
	movl	$12, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	-72(%rbp), %r8
	movl	-84(%rbp), %edx
	movq	%r12, %rcx
	movq	-80(%rbp), %rdi
	movl	%r13d, %esi
	call	_ZN2v88internal8compiler13CodeGenerator35AssembleArchBinarySearchSwitchRangeENS0_8RegisterENS1_9RpoNumberEPSt4pairIiPNS0_5LabelEES9_
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	-84(%rbp), %edx
	movq	-80(%rbp), %rdi
	movq	%r12, %r8
	movq	%r15, %rcx
	movl	%r13d, %esi
	call	_ZN2v88internal8compiler13CodeGenerator35AssembleArchBinarySearchSwitchRangeENS0_8RegisterENS1_9RpoNumberEPSt4pairIiPNS0_5LabelEES9_
	jmp	.L53
.L63:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24255:
	.size	_ZN2v88internal8compiler13CodeGenerator35AssembleArchBinarySearchSwitchRangeENS0_8RegisterENS1_9RpoNumberEPSt4pairIiPNS0_5LabelEES9_, .-_ZN2v88internal8compiler13CodeGenerator35AssembleArchBinarySearchSwitchRangeENS0_8RegisterENS1_9RpoNumberEPSt4pairIiPNS0_5LabelEES9_
	.section	.text._ZN2v88internal8compiler13CodeGenerator22GetSourcePositionTableEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13CodeGenerator22GetSourcePositionTableEv
	.type	_ZN2v88internal8compiler13CodeGenerator22GetSourcePositionTableEv, @function
_ZN2v88internal8compiler13CodeGenerator22GetSourcePositionTableEv:
.LFB24256:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$1152, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal26SourcePositionTableBuilder27ToSourcePositionTableVectorEv@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L67
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L67:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24256:
	.size	_ZN2v88internal8compiler13CodeGenerator22GetSourcePositionTableEv, .-_ZN2v88internal8compiler13CodeGenerator22GetSourcePositionTableEv
	.section	.text._ZN2v88internal8compiler13CodeGenerator24GetProtectedInstructionsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13CodeGenerator24GetProtectedInstructionsEv
	.type	_ZN2v88internal8compiler13CodeGenerator24GetProtectedInstructionsEv, @function
_ZN2v88internal8compiler13CodeGenerator24GetProtectedInstructionsEv:
.LFB24263:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	1224(%rsi), %rbx
	movq	1216(%rsi), %r14
	movq	%rbx, %r13
	subq	%r14, %r13
	movq	%r13, %r15
	sarq	$3, %r15
	jne	.L69
	movq	$0, (%rdi)
	movq	$0, 8(%rdi)
	xorl	%edi, %edi
.L70:
	cmpq	%rbx, %r14
	je	.L68
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memmove@PLT
.L68:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L69:
	.cfi_restore_state
	movabsq	$1152921504606846975, %rax
	movq	$-1, %rdi
	cmpq	%rax, %r15
	cmovbe	%r13, %rdi
	call	_Znam@PLT
	movq	%r15, 8(%r12)
	movq	%rax, (%r12)
	movq	%rax, %rdi
	jmp	.L70
	.cfi_endproc
.LFE24263:
	.size	_ZN2v88internal8compiler13CodeGenerator24GetProtectedInstructionsEv, .-_ZN2v88internal8compiler13CodeGenerator24GetProtectedInstructionsEv
	.section	.rodata._ZNK2v88internal8compiler13CodeGenerator21IsNextInAssemblyOrderENS1_9RpoNumberE.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"vector::_M_range_check: __n (which is %zu) >= this->size() (which is %zu)"
	.section	.text._ZNK2v88internal8compiler13CodeGenerator21IsNextInAssemblyOrderENS1_9RpoNumberE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler13CodeGenerator21IsNextInAssemblyOrderENS1_9RpoNumberE
	.type	_ZNK2v88internal8compiler13CodeGenerator21IsNextInAssemblyOrderENS1_9RpoNumberE, @function
_ZNK2v88internal8compiler13CodeGenerator21IsNextInAssemblyOrderENS1_9RpoNumberE:
.LFB24271:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	40(%rdi), %rax
	movslq	176(%rdi), %r8
	movq	16(%rax), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	8(%rdx), %rax
	movq	16(%rdx), %rdx
	subq	%rax, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %r8
	jnb	.L79
	movq	(%rax,%r8,8), %rcx
	movslq	%esi, %rsi
	movl	96(%rcx), %ecx
	cmpq	%rdx, %rsi
	jnb	.L80
	movq	(%rax,%rsi,8), %rax
	addl	$1, %ecx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	cmpl	%ecx, 96(%rax)
	sete	%al
	ret
.L79:
	.cfi_restore_state
	movq	%r8, %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L80:
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE24271:
	.size	_ZNK2v88internal8compiler13CodeGenerator21IsNextInAssemblyOrderENS1_9RpoNumberE, .-_ZNK2v88internal8compiler13CodeGenerator21IsNextInAssemblyOrderENS1_9RpoNumberE
	.section	.text._ZN2v88internal8compiler13CodeGenerator15RecordSafepointEPNS1_12ReferenceMapENS0_9Safepoint9DeoptModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13CodeGenerator15RecordSafepointEPNS1_12ReferenceMapENS0_9Safepoint9DeoptModeE
	.type	_ZN2v88internal8compiler13CodeGenerator15RecordSafepointEPNS1_12ReferenceMapENS0_9Safepoint9DeoptModeE, @function
_ZN2v88internal8compiler13CodeGenerator15RecordSafepointEPNS1_12ReferenceMapENS0_9Safepoint9DeoptModeE:
.LFB24272:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	leaq	200(%rdi), %rsi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$760, %rdi
	subq	$24, %rsp
	call	_ZN2v88internal21SafepointTableBuilder15DefineSafepointEPNS0_9AssemblerENS0_9Safepoint9DeoptModeE@PLT
	movq	16(%r12), %r13
	movq	%rax, %r14
	movq	24(%rbx), %rax
	movq	8(%r12), %rbx
	movq	(%rax), %rax
	movl	4(%rax), %r15d
	subl	8(%rax), %r15d
	cmpq	%r13, %rbx
	je	.L81
	movl	$256, %ecx
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L83:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	je	.L81
.L91:
	movq	(%rbx), %rax
	testb	$4, %al
	je	.L83
	movq	%rax, %rdx
	shrq	$3, %rdx
	andl	$3, %edx
	cmpl	$1, %edx
	jne	.L83
	movq	%rax, %rdx
	shrq	$5, %rdx
	cmpb	$11, %dl
	ja	.L83
	sarq	$35, %rax
	movq	%rax, %r12
	cmpl	%eax, %r15d
	jg	.L83
	movq	24(%r14), %rax
	testq	%rax, %rax
	je	.L99
.L84:
	movl	4(%rax), %edx
	cmpl	(%rax), %edx
	je	.L100
.L87:
	movl	%r12d, 24(%rax,%rdx,4)
	movq	24(%r14), %rax
	addq	$8, %rbx
	addl	$1, 4(%rax)
	addq	$1, 8(%r14)
	cmpq	%rbx, %r13
	jne	.L91
.L81:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L100:
	.cfi_restore_state
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L101
.L88:
	movq	%rax, 24(%r14)
	movl	4(%rax), %edx
	jmp	.L87
.L99:
	movq	(%r14), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$55, %rdx
	jbe	.L102
	leaq	56(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L86:
	pxor	%xmm0, %xmm0
	movq	$8, (%rax)
	movups	%xmm0, 8(%rax)
	movq	%rax, 16(%r14)
	movq	%rax, 24(%r14)
	jmp	.L84
.L101:
	addl	%edx, %edx
	movq	(%r14), %rdi
	cmpl	$256, %edx
	cmova	%ecx, %edx
	movq	24(%rdi), %r8
	movl	%edx, %eax
	leaq	31(,%rax,4), %rsi
	movq	16(%rdi), %rax
	andq	$-8, %rsi
	subq	%rax, %r8
	cmpq	%r8, %rsi
	ja	.L103
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L90:
	pxor	%xmm0, %xmm0
	movl	$0, 4(%rax)
	movups	%xmm0, 8(%rax)
	movl	%edx, (%rax)
	movq	24(%r14), %rdx
	movq	%rax, 8(%rdx)
	movq	24(%r14), %rdx
	movq	%rdx, 16(%rax)
	movq	24(%r14), %rax
	movq	8(%rax), %rax
	jmp	.L88
.L102:
	movl	$56, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	$256, %ecx
	jmp	.L86
.L103:
	movl	%edx, -52(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-52(%rbp), %edx
	movl	$256, %ecx
	jmp	.L90
	.cfi_endproc
.LFE24272:
	.size	_ZN2v88internal8compiler13CodeGenerator15RecordSafepointEPNS1_12ReferenceMapENS0_9Safepoint9DeoptModeE, .-_ZN2v88internal8compiler13CodeGenerator15RecordSafepointEPNS1_12ReferenceMapENS0_9Safepoint9DeoptModeE
	.section	.text._ZN2v88internal8compiler13CodeGenerator24IsMaterializableFromRootENS0_6HandleINS0_10HeapObjectEEEPNS0_9RootIndexE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13CodeGenerator24IsMaterializableFromRootENS0_6HandleINS0_10HeapObjectEEEPNS0_9RootIndexE
	.type	_ZN2v88internal8compiler13CodeGenerator24IsMaterializableFromRootENS0_6HandleINS0_10HeapObjectEEEPNS0_9RootIndexE, @function
_ZN2v88internal8compiler13CodeGenerator24IsMaterializableFromRootENS0_6HandleINS0_10HeapObjectEEEPNS0_9RootIndexE:
.LFB24273:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	movq	(%rax), %rcx
	xorl	%eax, %eax
	testb	$4, 64(%rcx)
	je	.L104
	movq	16(%rdi), %rax
	leaq	56(%rax), %rcx
	addq	$4856, %rax
	cmpq	%rsi, %rax
	jbe	.L107
	cmpq	%rsi, %rcx
	ja	.L107
	subq	%rcx, %rsi
	shrq	$3, %rsi
	cmpw	$573, %si
	movw	%si, (%rdx)
	setbe	%al
	ret
	.p2align 4,,10
	.p2align 3
.L107:
	xorl	%eax, %eax
.L104:
	ret
	.cfi_endproc
.LFE24273:
	.size	_ZN2v88internal8compiler13CodeGenerator24IsMaterializableFromRootENS0_6HandleINS0_10HeapObjectEEEPNS0_9RootIndexE, .-_ZN2v88internal8compiler13CodeGenerator24IsMaterializableFromRootENS0_6HandleINS0_10HeapObjectEEEPNS0_9RootIndexE
	.section	.text._ZN2v88internal8compiler13CodeGenerator11IsValidPushENS1_18InstructionOperandENS_4base5FlagsINS2_12PushTypeFlagEiEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13CodeGenerator11IsValidPushENS1_18InstructionOperandENS_4base5FlagsINS2_12PushTypeFlagEiEE
	.type	_ZN2v88internal8compiler13CodeGenerator11IsValidPushENS1_18InstructionOperandENS_4base5FlagsINS2_12PushTypeFlagEiEE, @function
_ZN2v88internal8compiler13CodeGenerator11IsValidPushENS1_18InstructionOperandENS_4base5FlagsINS2_12PushTypeFlagEiEE:
.LFB24275:
	.cfi_startproc
	endbr64
	movl	%edi, %eax
	andl	$7, %eax
	cmpl	$3, %eax
	je	.L118
	xorl	%eax, %eax
	testb	$4, %dil
	je	.L109
	movq	%rdi, %rdx
	shrq	$3, %rdx
	andl	$3, %edx
	jne	.L112
	shrq	$5, %rdi
	shrl	%esi
	andl	$1, %esi
	cmpb	$12, %dil
	cmovb	%esi, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L112:
	cmpl	$1, %edx
	jne	.L109
	shrl	$2, %esi
	shrq	$5, %rdi
	andl	$1, %esi
	cmpb	$12, %dil
	cmovb	%esi, %eax
.L109:
	ret
	.p2align 4,,10
	.p2align 3
.L118:
	movl	%esi, %eax
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE24275:
	.size	_ZN2v88internal8compiler13CodeGenerator11IsValidPushENS1_18InstructionOperandENS_4base5FlagsINS2_12PushTypeFlagEiEE, .-_ZN2v88internal8compiler13CodeGenerator11IsValidPushENS1_18InstructionOperandENS_4base5FlagsINS2_12PushTypeFlagEiEE
	.section	.text._ZN2v88internal8compiler13CodeGenerator8MoveType9InferMoveEPNS1_18InstructionOperandES5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13CodeGenerator8MoveType9InferMoveEPNS1_18InstructionOperandES5_
	.type	_ZN2v88internal8compiler13CodeGenerator8MoveType9InferMoveEPNS1_18InstructionOperandES5_, @function
_ZN2v88internal8compiler13CodeGenerator8MoveType9InferMoveEPNS1_18InstructionOperandES5_:
.LFB24286:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	(%rsi), %rdx
	movl	%eax, %esi
	movl	%edx, %ecx
	andl	$7, %esi
	andl	$4, %ecx
	cmpl	$2, %esi
	je	.L134
	testb	$4, %al
	jne	.L135
.L122:
	movl	$3, %eax
	testl	%ecx, %ecx
	je	.L119
	xorl	%eax, %eax
	andl	$24, %edx
	setne	%al
	addl	$2, %eax
.L119:
	ret
	.p2align 4,,10
	.p2align 3
.L135:
	testb	$24, %al
	jne	.L122
	movl	$1, %eax
	testl	%ecx, %ecx
	je	.L119
	xorl	%eax, %eax
	andl	$24, %edx
	setne	%al
	ret
	.p2align 4,,10
	.p2align 3
.L134:
	movl	$5, %eax
	testl	%ecx, %ecx
	je	.L119
	xorl	%eax, %eax
	andl	$24, %edx
	setne	%al
	addl	$4, %eax
	ret
	.cfi_endproc
.LFE24286:
	.size	_ZN2v88internal8compiler13CodeGenerator8MoveType9InferMoveEPNS1_18InstructionOperandES5_, .-_ZN2v88internal8compiler13CodeGenerator8MoveType9InferMoveEPNS1_18InstructionOperandES5_
	.section	.text._ZN2v88internal8compiler13CodeGenerator8MoveType9InferSwapEPNS1_18InstructionOperandES5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13CodeGenerator8MoveType9InferSwapEPNS1_18InstructionOperandES5_
	.type	_ZN2v88internal8compiler13CodeGenerator8MoveType9InferSwapEPNS1_18InstructionOperandES5_, @function
_ZN2v88internal8compiler13CodeGenerator8MoveType9InferSwapEPNS1_18InstructionOperandES5_:
.LFB24287:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movl	$3, %r8d
	testb	$4, %al
	je	.L136
	testb	$24, %al
	jne	.L136
	movq	(%rsi), %rax
	movl	$1, %r8d
	testb	$4, %al
	je	.L136
	xorl	%r8d, %r8d
	testb	$24, %al
	setne	%r8b
.L136:
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE24287:
	.size	_ZN2v88internal8compiler13CodeGenerator8MoveType9InferSwapEPNS1_18InstructionOperandES5_, .-_ZN2v88internal8compiler13CodeGenerator8MoveType9InferSwapEPNS1_18InstructionOperandES5_
	.section	.text._ZN2v88internal8compiler13CodeGenerator17ComputeBranchInfoEPNS1_10BranchInfoEPNS1_11InstructionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13CodeGenerator17ComputeBranchInfoEPNS1_10BranchInfoEPNS1_11InstructionE
	.type	_ZN2v88internal8compiler13CodeGenerator17ComputeBranchInfoEPNS1_10BranchInfoEPNS1_11InstructionE, @function
_ZN2v88internal8compiler13CodeGenerator17ComputeBranchInfoEPNS1_10BranchInfoEPNS1_11InstructionE:
.LFB24288:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	4(%rdx), %eax
	movl	%eax, %ecx
	movzbl	%al, %eax
	shrl	$8, %ecx
	movzwl	%cx, %ecx
	addq	%rax, %rcx
	movq	24(%rdx,%rcx,8), %rsi
	movq	40(%rdi), %rdx
	movl	%esi, %eax
	movq	%rsi, %r8
	andl	$7, %eax
	shrq	$3, %r8
	cmpl	$3, %eax
	je	.L180
	movq	112(%rdx), %rax
	movl	%r8d, %r9d
	leaq	104(%rdx), %rsi
	testq	%rax, %rax
	je	.L148
	movq	%rsi, %rdi
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L181:
	movq	%rax, %rdi
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L150
.L149:
	cmpl	32(%rax), %r9d
	jle	.L181
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L149
.L150:
	cmpq	%rdi, %rsi
	je	.L148
	cmpl	%r8d, 32(%rdi)
	cmovle	%rdi, %rsi
.L148:
	movq	48(%rsi), %r12
.L147:
	movq	32(%r13,%rcx,8), %rsi
	movl	%esi, %eax
	movq	%rsi, %rdi
	andl	$7, %eax
	shrq	$3, %rdi
	cmpl	$3, %eax
	je	.L182
	movq	112(%rdx), %rax
	movl	%edi, %esi
	leaq	104(%rdx), %rcx
	testq	%rax, %rax
	je	.L157
	movq	%rcx, %rdx
	jmp	.L158
	.p2align 4,,10
	.p2align 3
.L183:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L159
.L158:
	cmpl	32(%rax), %esi
	jle	.L183
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L158
.L159:
	cmpq	%rdx, %rcx
	je	.L157
	cmpl	%edi, 32(%rdx)
	cmovle	%rdx, %rcx
.L157:
	movq	48(%rcx), %rax
.L156:
	movslq	%eax, %r8
	cmpl	%r12d, %eax
	jne	.L184
.L163:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L185
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L184:
	.cfi_restore_state
	movq	40(%rbx), %rdx
	movl	0(%r13), %eax
	movslq	176(%rbx), %rsi
	movq	16(%rdx), %rdx
	shrl	$17, %eax
	andl	$31, %eax
	movq	8(%rdx), %r9
	movq	16(%rdx), %rdx
	subq	%r9, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L179
	movq	(%r9,%rsi,8), %rcx
	movslq	%r12d, %r12
	movl	96(%rcx), %edi
	cmpq	%rdx, %r12
	jnb	.L186
	movq	(%r9,%r12,8), %rsi
	leaq	0(,%r12,8), %rdx
	leaq	0(,%r8,8), %rcx
	addl	$1, %edi
	cmpl	%edi, 96(%rsi)
	jne	.L166
	movq	%rcx, %rsi
	xorl	$1, %eax
	movq	%rdx, %rcx
	movq	%r12, %r8
	movq	%rsi, %rdx
.L166:
	movl	%eax, (%r14)
	addq	160(%rbx), %rdx
	movq	%rdx, 8(%r14)
	movq	160(%rbx), %rax
	addq	%rcx, %rax
	movq	%rax, 16(%r14)
	movq	40(%rbx), %rax
	movslq	176(%rbx), %r9
	movq	16(%rax), %rax
	movq	8(%rax), %rsi
	movq	16(%rax), %rdx
	subq	%rsi, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %r9
	jnb	.L187
	movq	(%rsi,%r9,8), %rax
	movl	96(%rax), %eax
	cmpq	%r8, %rdx
	jbe	.L188
	movq	(%rsi,%rcx), %rdx
	addl	$1, %eax
	cmpl	%eax, 96(%rdx)
	movl	$-1, %eax
	sete	24(%r14)
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L182:
	sarq	$32, %rsi
	andl	$1, %edi
	je	.L189
	salq	$4, %rsi
	addq	152(%rdx), %rsi
	movq	8(%rsi), %rax
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L180:
	sarq	$32, %rsi
	andl	$1, %r8d
	je	.L190
	salq	$4, %rsi
	addq	152(%rdx), %rsi
	movq	8(%rsi), %r12
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L189:
	leaq	-64(%rbp), %rdi
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movq	-56(%rbp), %rax
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L190:
	leaq	-64(%rbp), %rdi
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movl	4(%r13), %eax
	movq	-56(%rbp), %r12
	movq	40(%rbx), %rdx
	movl	%eax, %ecx
	movzbl	%al, %eax
	shrl	$8, %ecx
	movzwl	%cx, %ecx
	addq	%rax, %rcx
	jmp	.L147
.L185:
	call	__stack_chk_fail@PLT
.L186:
	movq	%r12, %rsi
.L179:
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L187:
	movq	%r9, %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L188:
	movq	%r8, %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE24288:
	.size	_ZN2v88internal8compiler13CodeGenerator17ComputeBranchInfoEPNS1_10BranchInfoEPNS1_11InstructionE, .-_ZN2v88internal8compiler13CodeGenerator17ComputeBranchInfoEPNS1_10BranchInfoEPNS1_11InstructionE
	.section	.rodata._ZN2v88internal8compiler13CodeGenerator24TryInsertBranchPoisoningEPKNS1_16InstructionBlockE.str1.1,"aMS",@progbits,1
.LC2:
	.string	"unreachable code"
	.section	.text._ZN2v88internal8compiler13CodeGenerator24TryInsertBranchPoisoningEPKNS1_16InstructionBlockE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13CodeGenerator24TryInsertBranchPoisoningEPKNS1_16InstructionBlockE
	.type	_ZN2v88internal8compiler13CodeGenerator24TryInsertBranchPoisoningEPKNS1_16InstructionBlockE, @function
_ZN2v88internal8compiler13CodeGenerator24TryInsertBranchPoisoningEPKNS1_16InstructionBlockE:
.LFB24254:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	40(%rsi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	48(%rsi), %rax
	subq	%rdx, %rax
	cmpq	$4, %rax
	je	.L205
.L191:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L206
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L205:
	.cfi_restore_state
	movq	40(%rdi), %rcx
	movq	%rdi, %r12
	movq	%rsi, %rbx
	movslq	(%rdx), %rsi
	movq	16(%rcx), %rax
	movq	8(%rax), %rdi
	movq	16(%rax), %rdx
	subq	%rdi, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L207
	movq	(%rdi,%rsi,8), %rdx
	movl	116(%rdx), %eax
	cmpl	%eax, 112(%rdx)
	je	.L191
	movq	208(%rcx), %rsi
	leal	-1(%rax), %edx
	movslq	%edx, %rdx
	movq	%rsi, %rax
	subq	216(%rcx), %rax
	sarq	$3, %rax
	addq	%rdx, %rax
	js	.L194
	cmpq	$63, %rax
	jg	.L195
	leaq	(%rsi,%rdx,8), %rax
.L196:
	movq	(%rax), %r13
	movl	0(%r13), %eax
	shrl	$14, %eax
	andl	$7, %eax
	cmpl	$2, %eax
	jne	.L208
	leaq	-80(%rbp), %rsi
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeGenerator17ComputeBranchInfoEPNS1_10BranchInfoEPNS1_11InstructionE
	testl	%eax, %eax
	jns	.L191
	movslq	100(%rbx), %rdx
	movq	160(%r12), %rax
	movl	-80(%rbp), %esi
	leaq	(%rax,%rdx,8), %rax
	cmpq	%rax, -64(%rbp)
	je	.L209
.L202:
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeGenerator23AssembleBranchPoisoningENS1_14FlagsConditionEPNS1_11InstructionE@PLT
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L194:
	movq	%rax, %rdx
	notq	%rdx
	shrq	$6, %rdx
	notq	%rdx
.L197:
	movq	232(%rcx), %rcx
	movq	%rdx, %rsi
	salq	$6, %rsi
	movq	(%rcx,%rdx,8), %rdx
	subq	%rsi, %rax
	leaq	(%rdx,%rax,8), %rax
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L208:
	cmpl	$4, %eax
	jne	.L191
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L195:
	movq	%rax, %rdx
	sarq	$6, %rdx
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L209:
	xorl	$1, %esi
	jmp	.L202
.L206:
	call	__stack_chk_fail@PLT
.L207:
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE24254:
	.size	_ZN2v88internal8compiler13CodeGenerator24TryInsertBranchPoisoningEPKNS1_16InstructionBlockE, .-_ZN2v88internal8compiler13CodeGenerator24TryInsertBranchPoisoningEPKNS1_16InstructionBlockE
	.section	.rodata._ZN2v88internal8compiler13CodeGenerator22AssembleSourcePositionENS0_14SourcePositionE.str1.1,"aMS",@progbits,1
.LC3:
	.string	"-- "
.LC4:
	.string	" --"
	.section	.rodata._ZN2v88internal8compiler13CodeGenerator22AssembleSourcePositionENS0_14SourcePositionE.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"basic_string::_M_construct null not valid"
	.section	.text._ZN2v88internal8compiler13CodeGenerator22AssembleSourcePositionENS0_14SourcePositionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13CodeGenerator22AssembleSourcePositionENS0_14SourcePositionE
	.type	_ZN2v88internal8compiler13CodeGenerator22AssembleSourcePositionENS0_14SourcePositionE, @function
_ZN2v88internal8compiler13CodeGenerator22AssembleSourcePositionENS0_14SourcePositionE:
.LFB24291:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$552, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -536(%rbp)
	movq	%fs:40, %rbx
	movq	%rbx, -56(%rbp)
	xorl	%ebx, %ebx
	cmpq	%rsi, 192(%rdi)
	je	.L210
	movq	%rsi, %rax
	movq	%rsi, 192(%rdi)
	movq	%rdi, %r15
	testb	$1, %al
	jne	.L213
	movq	%rsi, %rdx
	shrq	%rax
	shrq	$31, %rdx
	andl	$1073741823, %eax
	movzwl	%dx, %edx
	orl	%eax, %edx
	jne	.L213
.L210:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L247
	addq	$552, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L213:
	.cfi_restore_state
	movq	232(%r15), %rsi
	movq	-536(%rbp), %rdx
	xorl	%ecx, %ecx
	leaq	1152(%r15), %rdi
	subq	216(%r15), %rsi
	movslq	%esi, %rsi
	call	_ZN2v88internal26SourcePositionTableBuilder11AddPositionEmNS0_14SourcePositionEb@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	je	.L210
	movq	152(%r15), %r8
	movl	8(%r8), %eax
	testl	%eax, %eax
	je	.L215
	cmpl	$5, %eax
	jne	.L210
.L215:
	movq	.LC6(%rip), %xmm1
	leaq	-320(%rbp), %r13
	movq	%r8, -544(%rbp)
	leaq	-432(%rbp), %r12
	movq	%r13, %rdi
	leaq	-368(%rbp), %r14
	movhps	.LC7(%rip), %xmm1
	movaps	%xmm1, -560(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movups	%xmm0, -88(%rbp)
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movq	%rbx, -432(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	$0, -104(%rbp)
	movw	%ax, -96(%rbp)
	movq	-24(%rbx), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	-432(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movdqa	-560(%rbp), %xmm1
	pxor	%xmm0, %xmm0
	movq	%r14, %rdi
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -320(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r13, %rdi
	leaq	-424(%rbp), %rsi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -560(%rbp)
	movq	%rax, -352(%rbp)
	movl	$16, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movl	$3, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-544(%rbp), %r8
	testl	$16384, (%r8)
	jne	.L216
	movq	712(%r15), %rax
	testq	%rax, %rax
	je	.L216
	cmpq	$0, 45416(%rax)
	je	.L248
.L216:
	leaq	-536(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internallsERSoRKNS0_14SourcePositionE@PLT
.L219:
	movq	%r12, %rdi
	movl	$3, %edx
	leaq	.LC4(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-384(%rbp), %rax
	leaq	-480(%rbp), %r12
	movq	$0, -488(%rbp)
	movq	%r12, -496(%rbp)
	leaq	-496(%rbp), %rdi
	movb	$0, -480(%rbp)
	testq	%rax, %rax
	je	.L220
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L221
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L222:
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	movq	-496(%rbp), %r8
	jne	.L249
.L223:
	cmpq	%r12, %r8
	je	.L230
	movq	%r8, %rdi
	call	_ZdlPv@PLT
.L230:
	movq	.LC6(%rip), %xmm0
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -320(%rbp)
	movhps	.LC8(%rip), %xmm0
	movaps	%xmm0, -432(%rbp)
	cmpq	-560(%rbp), %rdi
	je	.L231
	call	_ZdlPv@PLT
.L231:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	%rbx, -432(%rbp)
	movq	-24(%rbx), %rax
	movq	%r13, %rdi
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rcx, -432(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L221:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L249:
	leaq	240(%r15), %rax
	movq	%rax, -576(%rbp)
	leaq	-464(%rbp), %rax
	movq	%rax, -568(%rbp)
	leaq	-448(%rbp), %rax
	movq	%rax, -544(%rbp)
	movq	%rax, -464(%rbp)
	testq	%r8, %r8
	je	.L250
	movq	%r8, %rdi
	movq	%r8, -584(%rbp)
	call	strlen@PLT
	movq	-584(%rbp), %r8
	cmpq	$15, %rax
	movq	%rax, -528(%rbp)
	movq	%rax, %r10
	ja	.L251
	cmpq	$1, %rax
	jne	.L227
	movzbl	(%r8), %edx
	movb	%dl, -448(%rbp)
	movq	-544(%rbp), %rdx
.L228:
	movq	%rax, -456(%rbp)
	movq	-576(%rbp), %rdi
	movb	$0, (%rdx,%rax)
	movq	-568(%rbp), %rdx
	movq	232(%r15), %rsi
	subq	216(%r15), %rsi
	call	_ZN2v88internal18CodeCommentsWriter3AddEjNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-464(%rbp), %rdi
	cmpq	-544(%rbp), %rdi
	je	.L229
	call	_ZdlPv@PLT
.L229:
	movq	-496(%rbp), %r8
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L220:
	leaq	-352(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L251:
	movq	-568(%rbp), %rdi
	leaq	-528(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rax, -592(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-584(%rbp), %r8
	movq	-592(%rbp), %r10
	movq	%rax, -464(%rbp)
	movq	%rax, %rdi
	movq	-528(%rbp), %rax
	movq	%rax, -448(%rbp)
.L226:
	movq	%r10, %rdx
	movq	%r8, %rsi
	call	memcpy@PLT
	movq	-528(%rbp), %rax
	movq	-464(%rbp), %rdx
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L248:
	leaq	-528(%rbp), %r10
	leaq	-536(%rbp), %rsi
	movq	%r8, %rdx
	movq	%r10, %rdi
	movq	%r10, -544(%rbp)
	call	_ZNK2v88internal14SourcePosition13InliningStackEPNS0_24OptimizedCompilationInfoE@PLT
	movq	-544(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	call	_ZN2v88internallsERSoRKSt6vectorINS0_18SourcePositionInfoESaIS3_EE@PLT
	movq	-528(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L219
	call	_ZdlPv@PLT
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L227:
	testq	%rax, %rax
	jne	.L252
	movq	-544(%rbp), %rdx
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L250:
	leaq	.LC5(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L247:
	call	__stack_chk_fail@PLT
.L252:
	movq	-544(%rbp), %rdi
	jmp	.L226
	.cfi_endproc
.LFE24291:
	.size	_ZN2v88internal8compiler13CodeGenerator22AssembleSourcePositionENS0_14SourcePositionE, .-_ZN2v88internal8compiler13CodeGenerator22AssembleSourcePositionENS0_14SourcePositionE
	.section	.text._ZN2v88internal8compiler13CodeGenerator22AssembleSourcePositionEPNS1_11InstructionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13CodeGenerator22AssembleSourcePositionEPNS1_11InstructionE
	.type	_ZN2v88internal8compiler13CodeGenerator22AssembleSourcePositionEPNS1_11InstructionE, @function
_ZN2v88internal8compiler13CodeGenerator22AssembleSourcePositionEPNS1_11InstructionE:
.LFB24290:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	(%rsi), %eax
	movq	$0, -32(%rbp)
	andl	$511, %eax
	cmpl	$17, %eax
	je	.L254
.L257:
	movq	40(%r13), %rdi
	leaq	-32(%rbp), %rdx
	call	_ZNK2v88internal8compiler19InstructionSequence17GetSourcePositionEPKNS1_11InstructionEPNS0_14SourcePositionE@PLT
	testb	%al, %al
	jne	.L261
.L253:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L262
	addq	$40, %rsp
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L261:
	.cfi_restore_state
	movq	-32(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeGenerator22AssembleSourcePositionENS0_14SourcePositionE
	jmp	.L253
	.p2align 4,,10
	.p2align 3
.L254:
	movq	%rsi, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZNK2v88internal8compiler11Instruction17AreMovesRedundantEv@PLT
	movq	-40(%rbp), %rsi
	testb	%al, %al
	je	.L257
	jmp	.L253
.L262:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24290:
	.size	_ZN2v88internal8compiler13CodeGenerator22AssembleSourcePositionEPNS1_11InstructionE, .-_ZN2v88internal8compiler13CodeGenerator22AssembleSourcePositionEPNS1_11InstructionE
	.section	.text._ZN2v88internal8compiler13CodeGenerator28GetSlotAboveSPBeforeTailCallEPNS1_11InstructionEPi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13CodeGenerator28GetSlotAboveSPBeforeTailCallEPNS1_11InstructionEPi
	.type	_ZN2v88internal8compiler13CodeGenerator28GetSlotAboveSPBeforeTailCallEPNS1_11InstructionEPi, @function
_ZN2v88internal8compiler13CodeGenerator28GetSlotAboveSPBeforeTailCallEPNS1_11InstructionEPi:
.LFB24292:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	(%rsi), %eax
	andl	$511, %eax
	cmpl	$11, %eax
	ja	.L263
	movq	%rdx, %rbx
	movl	$2070, %edx
	btq	%rax, %rdx
	jnc	.L263
	movl	4(%rsi), %eax
	movl	%eax, %edx
	movzbl	%al, %eax
	shrl	$8, %edx
	movzwl	%dx, %edx
	leaq	4(%rdx,%rax), %rax
	movq	(%rsi,%rax,8), %rsi
	movq	40(%rdi), %rax
	movl	%esi, %edx
	movq	%rsi, %rdi
	andl	$7, %edx
	shrq	$3, %rdi
	cmpl	$3, %edx
	je	.L282
	leaq	104(%rax), %rdx
	movq	112(%rax), %rax
	movl	%edi, %esi
	testq	%rax, %rax
	je	.L269
	movq	%rdx, %rcx
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L283:
	movq	%rax, %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L271
.L270:
	cmpl	32(%rax), %esi
	jle	.L283
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L270
.L271:
	cmpq	%rcx, %rdx
	je	.L269
	cmpl	%edi, 32(%rcx)
	cmovle	%rcx, %rdx
.L269:
	movq	48(%rdx), %rax
.L268:
	movl	%eax, (%rbx)
	movl	$1, %r8d
.L263:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L284
	addq	$40, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L282:
	.cfi_restore_state
	sarq	$32, %rsi
	andl	$1, %edi
	jne	.L266
	leaq	-48(%rbp), %rdi
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movq	-40(%rbp), %rax
	jmp	.L268
	.p2align 4,,10
	.p2align 3
.L266:
	salq	$4, %rsi
	addq	152(%rax), %rsi
	movq	8(%rsi), %rax
	jmp	.L268
.L284:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24292:
	.size	_ZN2v88internal8compiler13CodeGenerator28GetSlotAboveSPBeforeTailCallEPNS1_11InstructionEPi, .-_ZN2v88internal8compiler13CodeGenerator28GetSlotAboveSPBeforeTailCallEPNS1_11InstructionEPi
	.section	.text._ZNK2v88internal8compiler13CodeGenerator21DetermineStubCallModeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler13CodeGenerator21DetermineStubCallModeEv
	.type	_ZNK2v88internal8compiler13CodeGenerator21DetermineStubCallModeEv, @function
_ZNK2v88internal8compiler13CodeGenerator21DetermineStubCallModeEv:
.LFB24293:
	.cfi_startproc
	endbr64
	movq	152(%rdi), %rax
	movl	8(%rax), %eax
	subl	$5, %eax
	cmpl	$2, %eax
	setbe	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE24293:
	.size	_ZNK2v88internal8compiler13CodeGenerator21DetermineStubCallModeEv, .-_ZNK2v88internal8compiler13CodeGenerator21DetermineStubCallModeEv
	.section	.text._ZN2v88internal8compiler13CodeGenerator12AssembleGapsEPNS1_11InstructionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13CodeGenerator12AssembleGapsEPNS1_11InstructionE
	.type	_ZN2v88internal8compiler13CodeGenerator12AssembleGapsEPNS1_11InstructionE, @function
_ZN2v88internal8compiler13CodeGenerator12AssembleGapsEPNS1_11InstructionE:
.LFB24294:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	8(%rsi), %rsi
	movq	%rdi, %rbx
	testq	%rsi, %rsi
	je	.L287
	leaq	744(%rdi), %rdi
	call	_ZN2v88internal8compiler11GapResolver7ResolveEPNS1_12ParallelMoveE@PLT
.L287:
	movq	16(%r12), %rsi
	testq	%rsi, %rsi
	je	.L286
	leaq	744(%rbx), %rdi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler11GapResolver7ResolveEPNS1_12ParallelMoveE@PLT
	.p2align 4,,10
	.p2align 3
.L286:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24294:
	.size	_ZN2v88internal8compiler13CodeGenerator12AssembleGapsEPNS1_11InstructionE, .-_ZN2v88internal8compiler13CodeGenerator12AssembleGapsEPNS1_11InstructionE
	.section	.rodata._ZN2v88internal8compiler13CodeGenerator26GenerateDeoptimizationDataEv.str1.8,"aMS",@progbits,1
	.align 8
.LC9:
	.string	"(deoptimization_exit) != nullptr"
	.section	.rodata._ZN2v88internal8compiler13CodeGenerator26GenerateDeoptimizationDataEv.str1.1,"aMS",@progbits,1
.LC10:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal8compiler13CodeGenerator26GenerateDeoptimizationDataEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13CodeGenerator26GenerateDeoptimizationDataEv
	.type	_ZN2v88internal8compiler13CodeGenerator26GenerateDeoptimizationDataEv, @function
_ZN2v88internal8compiler13CodeGenerator26GenerateDeoptimizationDataEv:
.LFB24296:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	936(%rdi), %rax
	subq	904(%rdi), %rax
	sarq	$3, %rax
	movq	912(%rdi), %rbx
	subq	920(%rdi), %rbx
	subq	$1, %rax
	sarq	$3, %rbx
	movq	152(%rdi), %r12
	salq	$6, %rax
	addq	%rbx, %rax
	movq	896(%rdi), %rbx
	subq	880(%rdi), %rbx
	sarq	$3, %rbx
	movq	16(%rdi), %rdi
	addq	%rbx, %rax
	movq	%rax, -88(%rbp)
	testl	%eax, %eax
	jne	.L294
	cmpl	$-1, 56(%r12)
	je	.L396
.L294:
	movl	-88(%rbp), %esi
	movl	$1, %edx
	call	_ZN2v88internal18DeoptimizationData3NewEPNS0_7IsolateEiNS0_14AllocationTypeE@PLT
	movq	16(%r14), %rsi
	leaq	1048(%r14), %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17TranslationBuffer15CreateByteArrayEPNS0_7FactoryE@PLT
	movq	(%rbx), %r15
	movq	(%rax), %r13
	leaq	15(%r15), %rsi
	movq	%r13, 15(%r15)
	testb	$1, %r13b
	je	.L346
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	jne	.L397
	testb	$24, %al
	je	.L346
.L409:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L398
	.p2align 4,,10
	.p2align 3
.L346:
	movslq	1040(%r14), %rax
	movq	(%rbx), %rdx
	salq	$32, %rax
	movq	%rax, 23(%rdx)
	movslq	120(%r12), %rax
	movq	(%rbx), %rdx
	salq	$32, %rax
	movq	%rax, 55(%rdx)
	movslq	844(%r14), %rax
	movq	(%rbx), %rdx
	salq	$32, %rax
	movq	%rax, 79(%rdx)
	movq	24(%r12), %rax
	movq	(%rbx), %r15
	testq	%rax, %rax
	je	.L298
	movq	(%rax), %r13
	leaq	63(%r15), %rsi
	movq	%r13, 63(%r15)
	testb	$1, %r13b
	je	.L302
.L395:
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	jne	.L399
	testb	$24, %al
	je	.L302
.L410:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L400
	.p2align 4,,10
	.p2align 3
.L302:
	movq	1032(%r14), %rax
	subq	1000(%r14), %rax
	movl	$1, %edx
	xorl	%r15d, %r15d
	sarq	$3, %rax
	movq	1008(%r14), %rsi
	movq	16(%r14), %rdi
	subq	1016(%r14), %rsi
	subq	$1, %rax
	sarq	$5, %rsi
	salq	$4, %rax
	leaq	(%rax,%rsi), %rax
	movq	992(%r14), %rsi
	subq	976(%r14), %rsi
	sarq	$5, %rsi
	addq	%rax, %rsi
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	1000(%r14), %rsi
	xorl	%r9d, %r9d
	xorl	%r10d, %r10d
	movq	%rax, %r13
	movq	1032(%r14), %rax
	movq	976(%r14), %rdi
	subq	%rsi, %rax
	sarq	$3, %rax
	subq	$1, %rax
	salq	$4, %rax
	movq	%rax, %rdx
	movq	1008(%r14), %rax
	subq	1016(%r14), %rax
	sarq	$5, %rax
	addq	%rdx, %rax
	movq	992(%r14), %rdx
	subq	%rdi, %rdx
	sarq	$5, %rdx
	addq	%rdx, %rax
	jne	.L306
	jmp	.L320
	.p2align 4,,10
	.p2align 3
.L314:
	movq	24(%rax), %rdi
	movq	%rcx, %rsi
	call	_ZNK2v88internal18StringConstantBase22AllocateStringConstantEPNS0_7IsolateE@PLT
.L316:
	movq	0(%r13), %rdi
	movq	(%rax), %rdx
	leal	16(,%r15,8), %eax
	cltq
	leaq	-1(%rdi,%rax), %rsi
	movq	%rdx, (%rsi)
	testb	$1, %dl
	je	.L341
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	je	.L318
	movq	%rdx, -80(%rbp)
	movq	%rsi, -72(%rbp)
	movq	%rdi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-80(%rbp), %rdx
	movq	-72(%rbp), %rsi
	movq	-64(%rbp), %rdi
	movq	8(%rcx), %rax
.L318:
	testb	$24, %al
	je	.L341
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L341
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L341:
	movq	1000(%r14), %rsi
	movq	1032(%r14), %rcx
	leal	1(%r15), %r10d
	movq	1008(%r14), %rdx
	subq	1016(%r14), %rdx
	movq	%r10, %r15
	movq	%r10, %r9
	subq	%rsi, %rcx
	sarq	$5, %rdx
	movq	976(%r14), %rdi
	sarq	$3, %rcx
	subq	$1, %rcx
	movq	%rcx, %rax
	salq	$4, %rax
	leaq	(%rax,%rdx), %rcx
	movq	992(%r14), %rdx
	subq	%rdi, %rdx
	sarq	$5, %rdx
	addq	%rcx, %rdx
	cmpq	%rdx, %r10
	jnb	.L320
.L306:
	movq	%rdi, %rax
	subq	984(%r14), %rax
	sarq	$5, %rax
	addq	%r10, %rax
	js	.L309
	cmpq	$15, %rax
	jg	.L310
	movq	%r9, %rax
	movq	16(%r14), %rcx
	salq	$5, %rax
	addq	%rdi, %rax
	movl	(%rax), %edx
	cmpl	$1, %edx
	je	.L313
.L402:
	cmpl	$2, %edx
	je	.L314
	testl	%edx, %edx
	je	.L401
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L310:
	movq	%rax, %rdx
	sarq	$4, %rdx
.L312:
	movq	%rdx, %rcx
	salq	$4, %rcx
	subq	%rcx, %rax
	movq	16(%r14), %rcx
	salq	$5, %rax
	addq	(%rsi,%rdx,8), %rax
	movl	(%rax), %edx
	cmpl	$1, %edx
	jne	.L402
.L313:
	movsd	16(%rax), %xmm0
	xorl	%esi, %esi
	movq	%rcx, %rdi
	call	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L401:
	movq	8(%rax), %rax
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L309:
	movq	%rax, %rdx
	notq	%rdx
	shrq	$4, %rdx
	notq	%rdx
	jmp	.L312
	.p2align 4,,10
	.p2align 3
.L320:
	movq	(%rbx), %r15
	movq	0(%r13), %r13
	movq	%r13, 31(%r15)
	leaq	31(%r15), %rsi
	testb	$1, %r13b
	je	.L343
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	jne	.L403
	testb	$24, %al
	je	.L343
.L411:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L404
	.p2align 4,,10
	.p2align 3
.L343:
	movq	96(%r12), %rax
	movq	104(%r12), %rsi
	movq	16(%r14), %rdi
	cmpq	%rsi, %rax
	je	.L405
	subq	%rax, %rsi
	movl	$1, %edx
	sarq	$5, %rsi
	sall	$4, %esi
	call	_ZN2v88internal7Factory12NewByteArrayEiNS0_14AllocationTypeE@PLT
	movq	96(%r12), %rsi
	xorl	%ecx, %ecx
	cmpq	104(%r12), %rsi
	je	.L324
	.p2align 4,,10
	.p2align 3
.L325:
	movq	%rcx, %rdx
	salq	$5, %rdx
	movdqu	16(%rsi,%rdx), %xmm0
	movl	%ecx, %edx
	movq	(%rax), %rsi
	addq	$1, %rcx
	sall	$4, %edx
	movslq	%edx, %rdx
	movups	%xmm0, 15(%rdx,%rsi)
	movq	96(%r12), %rsi
	movq	104(%r12), %rdx
	subq	%rsi, %rdx
	sarq	$5, %rdx
	cmpq	%rcx, %rdx
	ja	.L325
.L324:
	movq	(%rbx), %r15
	movq	(%rax), %r13
	movq	%r13, 71(%r15)
	leaq	71(%r15), %rsi
	testb	$1, %r13b
	je	.L342
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	je	.L328
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rsi
	movq	8(%rcx), %rax
.L328:
	testb	$24, %al
	je	.L342
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L406
	.p2align 4,,10
	.p2align 3
.L342:
	cmpl	$-1, 56(%r12)
	movq	(%rbx), %rdx
	je	.L330
	movq	152(%r14), %rax
	movslq	56(%rax), %rax
	salq	$32, %rax
	movq	%rax, 39(%rdx)
	movslq	1144(%r14), %rax
	movq	(%rbx), %rdx
	salq	$32, %rax
	movq	%rax, 47(%rdx)
.L333:
	movl	-88(%rbp), %eax
	testl	%eax, %eax
	jle	.L361
	movq	-88(%rbp), %rax
	movl	$88, %ecx
	xorl	%edx, %edx
	leal	-1(%rax), %edi
	jmp	.L340
	.p2align 4,,10
	.p2align 3
.L408:
	cmpq	$63, %rax
	jg	.L335
	leaq	(%rsi,%rdx,8), %rax
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L407
.L338:
	movslq	24(%rax), %rsi
	movq	(%rbx), %r9
	addq	$24, %rcx
	salq	$32, %rsi
	movq	%rsi, -25(%rcx,%r9)
	movslq	28(%rax), %rsi
	movq	(%rbx), %r9
	salq	$32, %rsi
	movq	%rsi, -17(%rcx,%r9)
	movslq	32(%rax), %rax
	movq	(%rbx), %rsi
	salq	$32, %rax
	movq	%rax, -9(%rcx,%rsi)
	leaq	1(%rdx), %rax
	cmpq	%rdi, %rdx
	je	.L361
	movq	%rax, %rdx
.L340:
	movq	880(%r14), %rsi
	movq	%rsi, %rax
	subq	888(%r14), %rax
	sarq	$3, %rax
	addq	%rdx, %rax
	jns	.L408
	movq	%rax, %rsi
	notq	%rsi
	shrq	$6, %rsi
	notq	%rsi
.L337:
	movq	904(%r14), %r9
	movq	%rsi, %r10
	salq	$6, %r10
	movq	(%r9,%rsi,8), %rsi
	subq	%r10, %rax
	leaq	(%rsi,%rax,8), %rax
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L338
.L407:
	leaq	.LC9(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L405:
	leaq	976(%rdi), %rax
	jmp	.L324
	.p2align 4,,10
	.p2align 3
.L397:
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L409
	jmp	.L346
	.p2align 4,,10
	.p2align 3
.L399:
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L410
	jmp	.L302
	.p2align 4,,10
	.p2align 3
.L403:
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L411
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L335:
	movq	%rax, %rsi
	sarq	$6, %rsi
	jmp	.L337
	.p2align 4,,10
	.p2align 3
.L361:
	addq	$56, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L330:
	.cfi_restore_state
	movabsq	$-4294967296, %rax
	movq	%rax, 39(%rdx)
	movq	(%rbx), %rdx
	movq	%rax, 47(%rdx)
	jmp	.L333
	.p2align 4,,10
	.p2align 3
.L298:
	movq	_ZN2v88internal3Smi5kZeroE(%rip), %r13
	leaq	63(%r15), %rsi
	movq	%r13, 63(%r15)
	testb	$1, %r13b
	jne	.L395
	jmp	.L302
	.p2align 4,,10
	.p2align 3
.L404:
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L398:
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L346
	.p2align 4,,10
	.p2align 3
.L400:
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L302
	.p2align 4,,10
	.p2align 3
.L406:
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L342
	.p2align 4,,10
	.p2align 3
.L396:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal18DeoptimizationData5EmptyEPNS0_7IsolateE@PLT
	.cfi_endproc
.LFE24296:
	.size	_ZN2v88internal8compiler13CodeGenerator26GenerateDeoptimizationDataEv, .-_ZN2v88internal8compiler13CodeGenerator26GenerateDeoptimizationDataEv
	.section	.text._ZN2v88internal8compiler13CodeGenerator12FinalizeCodeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13CodeGenerator12FinalizeCodeEv
	.type	_ZN2v88internal8compiler13CodeGenerator12FinalizeCodeEv, @function
_ZN2v88internal8compiler13CodeGenerator12FinalizeCodeEv:
.LFB24264:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	1240(%rdi), %eax
	testl	%eax, %eax
	je	.L413
.L416:
	xorl	%eax, %eax
.L414:
	movq	-56(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L433
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L413:
	.cfi_restore_state
	movq	16(%rdi), %rsi
	movq	%rdi, %rbx
	leaq	1152(%rdi), %rdi
	leaq	-144(%rbp), %r12
	call	_ZN2v88internal26SourcePositionTableBuilder21ToSourcePositionTableEPNS0_7IsolateE@PLT
	movq	%rbx, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeGenerator26GenerateDeoptimizationDataEv
	movq	16(%rbx), %rsi
	pxor	%xmm0, %xmm0
	movq	%r12, %rdx
	movl	1080(%rbx), %r8d
	leaq	760(%rbx), %rcx
	leaq	200(%rbx), %rdi
	movq	%rax, -216(%rbp)
	movaps	%xmm0, -144(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal9Assembler7GetCodeEPNS0_7IsolateEPNS0_8CodeDescEPNS0_21SafepointTableBuilderEi@PLT
	cmpb	$0, _ZN2v88internal29FLAG_perf_prof_unwinding_infoE(%rip)
	je	.L415
	leaq	56(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal13EhFrameWriter10GetEhFrameEPNS0_8CodeDescE@PLT
.L415:
	movq	152(%rbx), %rax
	movq	16(%rbx), %rsi
	leaq	-208(%rbp), %r14
	movq	%r12, %rdx
	movq	%r14, %rdi
	movl	8(%rax), %ecx
	call	_ZN2v88internal7Factory11CodeBuilderC1EPNS0_7IsolateERKNS0_8CodeDescENS0_4Code4KindE@PLT
	movq	152(%rbx), %rax
	movq	%r13, %xmm0
	movq	%r14, %rdi
	movhps	-216(%rbp), %xmm0
	movl	12(%rax), %eax
	movb	$1, -150(%rbp)
	movups	%xmm0, -168(%rbp)
	movl	%eax, -176(%rbp)
	movq	24(%rbx), %rax
	movq	(%rax), %rax
	movl	4(%rax), %eax
	movl	%eax, -148(%rbp)
	call	_ZN2v88internal7Factory11CodeBuilder8TryBuildEv@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L416
	movq	16(%rbx), %rax
	movq	40960(%rax), %r14
	movq	(%r12), %rax
	cmpb	$0, 6720(%r14)
	movl	39(%rax), %r15d
	je	.L417
	movq	6712(%r14), %rax
.L418:
	testq	%rax, %rax
	je	.L419
	addl	%r15d, (%rax)
.L419:
	movq	16(%rbx), %rax
	leaq	_ZN2v88internal6Logger27is_listening_to_code_eventsEv(%rip), %rdx
	movq	41016(%rax), %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	136(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L420
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	jne	.L423
	cmpq	$0, 80(%r14)
	je	.L422
.L423:
	movq	(%r12), %rax
	movq	0(%r13), %rdx
	movq	%r14, %rdi
	leaq	63(%rax), %rsi
	call	_ZN2v88internal6Logger26CodeLinePosInfoRecordEventEmNS0_9ByteArrayE@PLT
.L422:
	movq	%r12, %rax
	jmp	.L414
	.p2align 4,,10
	.p2align 3
.L417:
	movb	$1, 6720(%r14)
	leaq	6696(%r14), %rdi
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	%rax, 6712(%r14)
	jmp	.L418
	.p2align 4,,10
	.p2align 3
.L420:
	call	*%rax
	testb	%al, %al
	je	.L422
	jmp	.L423
.L433:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24264:
	.size	_ZN2v88internal8compiler13CodeGenerator12FinalizeCodeEv, .-_ZN2v88internal8compiler13CodeGenerator12FinalizeCodeEv
	.section	.text._ZN2v88internal8compiler13CodeGenerator12AddJumpTableEPPNS0_5LabelEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13CodeGenerator12AddJumpTableEPPNS0_5LabelEm
	.type	_ZN2v88internal8compiler13CodeGenerator12AddJumpTableEPPNS0_5LabelEm, @function
_ZN2v88internal8compiler13CodeGenerator12AddJumpTableEPPNS0_5LabelEm:
.LFB24297:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$31, %rdx
	jbe	.L438
	leaq	32(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L436:
	movq	1104(%rbx), %rdx
	movq	$0, (%rax)
	movq	%r13, 16(%rax)
	movq	%rdx, 8(%rax)
	movq	%r12, 24(%rax)
	movq	%rax, 1104(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L438:
	.cfi_restore_state
	movl	$32, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L436
	.cfi_endproc
.LFE24297:
	.size	_ZN2v88internal8compiler13CodeGenerator12AddJumpTableEPPNS0_5LabelEm, .-_ZN2v88internal8compiler13CodeGenerator12AddJumpTableEPPNS0_5LabelEm
	.section	.text._ZN2v88internal8compiler13CodeGenerator22GetDeoptimizationEntryEPNS1_11InstructionEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13CodeGenerator22GetDeoptimizationEntryEPNS1_11InstructionEm
	.type	_ZN2v88internal8compiler13CodeGenerator22GetDeoptimizationEntryEPNS1_11InstructionEm, @function
_ZN2v88internal8compiler13CodeGenerator22GetDeoptimizationEntryEPNS1_11InstructionEm:
.LFB24304:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	40(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movzbl	4(%rsi), %eax
	leaq	5(%rax,%rdx), %rax
	movq	(%rsi,%rax,8), %rsi
	movl	%esi, %eax
	movq	%rsi, %r8
	andl	$7, %eax
	shrq	$3, %r8
	cmpl	$3, %eax
	je	.L455
	movq	112(%rdi), %rax
	movl	%r8d, %esi
	leaq	104(%rdi), %rdx
	testq	%rax, %rax
	je	.L444
	movq	%rdx, %rcx
	jmp	.L445
	.p2align 4,,10
	.p2align 3
.L456:
	movq	%rax, %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L446
.L445:
	cmpl	32(%rax), %esi
	jle	.L456
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L445
.L446:
	cmpq	%rcx, %rdx
	je	.L444
	cmpl	%r8d, 32(%rcx)
	cmovle	%rcx, %rdx
.L444:
	movq	48(%rdx), %rsi
.L443:
	call	_ZN2v88internal8compiler19InstructionSequence22GetDeoptimizationEntryEi@PLT
	movq	-24(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L457
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L455:
	.cfi_restore_state
	sarq	$32, %rsi
	andl	$1, %r8d
	je	.L458
	salq	$4, %rsi
	addq	152(%rdi), %rsi
	movq	8(%rsi), %rsi
	jmp	.L443
	.p2align 4,,10
	.p2align 3
.L458:
	leaq	-48(%rbp), %rdi
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movq	-40(%rbp), %rsi
	movq	40(%rbx), %rdi
	jmp	.L443
.L457:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24304:
	.size	_ZN2v88internal8compiler13CodeGenerator22GetDeoptimizationEntryEPNS1_11InstructionEm, .-_ZN2v88internal8compiler13CodeGenerator22GetDeoptimizationEntryEPNS1_11InstructionEm
	.section	.text._ZN2v88internal8compiler13CodeGenerator17MarkLazyDeoptSiteEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13CodeGenerator17MarkLazyDeoptSiteEv
	.type	_ZN2v88internal8compiler13CodeGenerator17MarkLazyDeoptSiteEv, @function
_ZN2v88internal8compiler13CodeGenerator17MarkLazyDeoptSiteEv:
.LFB24310:
	.cfi_startproc
	endbr64
	movq	232(%rdi), %rax
	subq	216(%rdi), %rax
	movl	%eax, 1084(%rdi)
	ret
	.cfi_endproc
.LFE24310:
	.size	_ZN2v88internal8compiler13CodeGenerator17MarkLazyDeoptSiteEv, .-_ZN2v88internal8compiler13CodeGenerator17MarkLazyDeoptSiteEv
	.section	.text._ZN2v88internal8compiler13CodeGenerator27InitializeSpeculationPoisonEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13CodeGenerator27InitializeSpeculationPoisonEv
	.type	_ZN2v88internal8compiler13CodeGenerator27InitializeSpeculationPoisonEv, @function
_ZN2v88internal8compiler13CodeGenerator27InitializeSpeculationPoisonEv:
.LFB24312:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$1, 1244(%rdi)
	je	.L460
	movq	152(%rdi), %rax
	movq	%rdi, %r12
	movl	(%rax), %eax
	testb	$4, %ah
	je	.L463
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L472
.L464:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeGenerator46GenerateSpeculationPoisonFromCodeStartRegisterEv@PLT
	movq	152(%r12), %rax
	movl	(%rax), %eax
	testb	$8, %ah
	jne	.L473
.L460:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L474
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L463:
	.cfi_restore_state
	leaq	200(%rdi), %rdi
	call	_ZN2v88internal14TurboAssembler30ResetSpeculationPoisonRegisterEv@PLT
	jmp	.L460
	.p2align 4,,10
	.p2align 3
.L473:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeGenerator33AssembleRegisterArgumentPoisoningEv@PLT
	jmp	.L460
	.p2align 4,,10
	.p2align 3
.L472:
	leaq	-80(%rbp), %r13
	leaq	-88(%rbp), %rsi
	xorl	%edx, %edx
	movq	$43, -88(%rbp)
	movq	%r13, %rdi
	leaq	-64(%rbp), %rbx
	movq	%rbx, -80(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-88(%rbp), %rdx
	movdqa	.LC11(%rip), %xmm0
	movabsq	$7957705963265335406, %rcx
	movq	%rax, -80(%rbp)
	leaq	240(%r12), %rdi
	movq	%rdx, -64(%rbp)
	movl	$11552, %edx
	movups	%xmm0, (%rax)
	movdqa	.LC12(%rip), %xmm0
	movw	%dx, 40(%rax)
	movq	-80(%rbp), %rdx
	movq	%rcx, 32(%rax)
	movb	$45, 42(%rax)
	movups	%xmm0, 16(%rax)
	movq	-88(%rbp), %rax
	movq	%rax, -72(%rbp)
	movb	$0, (%rdx,%rax)
	movq	%r13, %rdx
	movq	232(%r12), %rsi
	subq	216(%r12), %rsi
	call	_ZN2v88internal18CodeCommentsWriter3AddEjNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L464
	call	_ZdlPv@PLT
	jmp	.L464
.L474:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24312:
	.size	_ZN2v88internal8compiler13CodeGenerator27InitializeSpeculationPoisonEv, .-_ZN2v88internal8compiler13CodeGenerator27InitializeSpeculationPoisonEv
	.section	.text._ZN2v88internal8compiler13CodeGenerator22ResetSpeculationPoisonEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13CodeGenerator22ResetSpeculationPoisonEv
	.type	_ZN2v88internal8compiler13CodeGenerator22ResetSpeculationPoisonEv, @function
_ZN2v88internal8compiler13CodeGenerator22ResetSpeculationPoisonEv:
.LFB24313:
	.cfi_startproc
	endbr64
	cmpl	$1, 1244(%rdi)
	jne	.L477
	ret
	.p2align 4,,10
	.p2align 3
.L477:
	addq	$200, %rdi
	jmp	_ZN2v88internal14TurboAssembler30ResetSpeculationPoisonRegisterEv@PLT
	.cfi_endproc
.LFE24313:
	.size	_ZN2v88internal8compiler13CodeGenerator22ResetSpeculationPoisonEv, .-_ZN2v88internal8compiler13CodeGenerator22ResetSpeculationPoisonEv
	.section	.text._ZN2v88internal8compiler13OutOfLineCodeC2EPNS1_13CodeGeneratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13OutOfLineCodeC2EPNS1_13CodeGeneratorE
	.type	_ZN2v88internal8compiler13OutOfLineCodeC2EPNS1_13CodeGeneratorE, @function
_ZN2v88internal8compiler13OutOfLineCodeC2EPNS1_13CodeGeneratorE:
.LFB24315:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal8compiler13OutOfLineCodeE(%rip), %rax
	pxor	%xmm0, %xmm0
	movups	%xmm0, 8(%rdi)
	movq	%rax, (%rdi)
	movq	24(%rsi), %rax
	movq	(%rax), %rax
	movq	%rax, 24(%rdi)
	leaq	200(%rsi), %rax
	movq	%rax, 32(%rdi)
	movq	1112(%rsi), %rax
	movq	%rdi, 1112(%rsi)
	movq	%rax, 40(%rdi)
	ret
	.cfi_endproc
.LFE24315:
	.size	_ZN2v88internal8compiler13OutOfLineCodeC2EPNS1_13CodeGeneratorE, .-_ZN2v88internal8compiler13OutOfLineCodeC2EPNS1_13CodeGeneratorE
	.globl	_ZN2v88internal8compiler13OutOfLineCodeC1EPNS1_13CodeGeneratorE
	.set	_ZN2v88internal8compiler13OutOfLineCodeC1EPNS1_13CodeGeneratorE,_ZN2v88internal8compiler13OutOfLineCodeC2EPNS1_13CodeGeneratorE
	.section	.text._ZN2v88internal8compiler13OutOfLineCodeD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13OutOfLineCodeD2Ev
	.type	_ZN2v88internal8compiler13OutOfLineCodeD2Ev, @function
_ZN2v88internal8compiler13OutOfLineCodeD2Ev:
.LFB24318:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE24318:
	.size	_ZN2v88internal8compiler13OutOfLineCodeD2Ev, .-_ZN2v88internal8compiler13OutOfLineCodeD2Ev
	.globl	_ZN2v88internal8compiler13OutOfLineCodeD1Ev
	.set	_ZN2v88internal8compiler13OutOfLineCodeD1Ev,_ZN2v88internal8compiler13OutOfLineCodeD2Ev
	.section	.text._ZN2v88internal8compiler13OutOfLineCodeD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13OutOfLineCodeD0Ev
	.type	_ZN2v88internal8compiler13OutOfLineCodeD0Ev, @function
_ZN2v88internal8compiler13OutOfLineCodeD0Ev:
.LFB24320:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE24320:
	.size	_ZN2v88internal8compiler13OutOfLineCodeD0Ev, .-_ZN2v88internal8compiler13OutOfLineCodeD0Ev
	.section	.text._ZNK2v88internal8compiler21DeoptimizationLiteral5ReifyEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler21DeoptimizationLiteral5ReifyEPNS0_7IsolateE
	.type	_ZNK2v88internal8compiler21DeoptimizationLiteral5ReifyEPNS0_7IsolateE, @function
_ZNK2v88internal8compiler21DeoptimizationLiteral5ReifyEPNS0_7IsolateE:
.LFB24321:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	movq	%rsi, %rdx
	cmpl	$1, %eax
	je	.L483
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpl	$2, %eax
	je	.L484
	testl	%eax, %eax
	je	.L490
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L483:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	movsd	16(%rdi), %xmm0
	xorl	%esi, %esi
	movq	%rdx, %rdi
	jmp	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	.p2align 4,,10
	.p2align 3
.L490:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	movq	8(%rdi), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L484:
	.cfi_restore_state
	movq	24(%rdi), %rdi
	call	_ZNK2v88internal18StringConstantBase22AllocateStringConstantEPNS0_7IsolateE@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24321:
	.size	_ZNK2v88internal8compiler21DeoptimizationLiteral5ReifyEPNS0_7IsolateE, .-_ZNK2v88internal8compiler21DeoptimizationLiteral5ReifyEPNS0_7IsolateE
	.section	.text._ZN2v88internal13ZoneChunkListINS0_21SafepointTableBuilder18DeoptimizationInfoEEC2EPNS0_4ZoneENS4_9StartModeE,"axG",@progbits,_ZN2v88internal13ZoneChunkListINS0_21SafepointTableBuilder18DeoptimizationInfoEEC5EPNS0_4ZoneENS4_9StartModeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ZoneChunkListINS0_21SafepointTableBuilder18DeoptimizationInfoEEC2EPNS0_4ZoneENS4_9StartModeE
	.type	_ZN2v88internal13ZoneChunkListINS0_21SafepointTableBuilder18DeoptimizationInfoEEC2EPNS0_4ZoneENS4_9StartModeE, @function
_ZN2v88internal13ZoneChunkListINS0_21SafepointTableBuilder18DeoptimizationInfoEEC2EPNS0_4ZoneENS4_9StartModeE:
.LFB24845:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%rsi, (%rdi)
	movq	$0, 8(%rdi)
	movups	%xmm0, 16(%rdi)
	testl	%edx, %edx
	jne	.L499
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L499:
	.cfi_restore_state
	movq	%rsi, %rdi
	movl	%edx, %eax
	leaq	3(%rax,%rax,2), %rsi
	movq	24(%rdi), %rcx
	movq	16(%rdi), %rax
	salq	$3, %rsi
	subq	%rax, %rcx
	cmpq	%rcx, %rsi
	ja	.L500
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L494:
	pxor	%xmm0, %xmm0
	movl	$0, 4(%rax)
	movl	%edx, (%rax)
	movups	%xmm0, 8(%rax)
	movq	%rax, 16(%rbx)
	movq	%rax, 24(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L500:
	.cfi_restore_state
	movl	%edx, -20(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-20(%rbp), %edx
	jmp	.L494
	.cfi_endproc
.LFE24845:
	.size	_ZN2v88internal13ZoneChunkListINS0_21SafepointTableBuilder18DeoptimizationInfoEEC2EPNS0_4ZoneENS4_9StartModeE, .-_ZN2v88internal13ZoneChunkListINS0_21SafepointTableBuilder18DeoptimizationInfoEEC2EPNS0_4ZoneENS4_9StartModeE
	.weak	_ZN2v88internal13ZoneChunkListINS0_21SafepointTableBuilder18DeoptimizationInfoEEC1EPNS0_4ZoneENS4_9StartModeE
	.set	_ZN2v88internal13ZoneChunkListINS0_21SafepointTableBuilder18DeoptimizationInfoEEC1EPNS0_4ZoneENS4_9StartModeE,_ZN2v88internal13ZoneChunkListINS0_21SafepointTableBuilder18DeoptimizationInfoEEC2EPNS0_4ZoneENS4_9StartModeE
	.section	.text._ZN2v88internal13ZoneChunkListIhEC2EPNS0_4ZoneENS2_9StartModeE,"axG",@progbits,_ZN2v88internal13ZoneChunkListIhEC5EPNS0_4ZoneENS2_9StartModeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ZoneChunkListIhEC2EPNS0_4ZoneENS2_9StartModeE
	.type	_ZN2v88internal13ZoneChunkListIhEC2EPNS0_4ZoneENS2_9StartModeE, @function
_ZN2v88internal13ZoneChunkListIhEC2EPNS0_4ZoneENS2_9StartModeE:
.LFB25295:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%rsi, (%rdi)
	movq	$0, 8(%rdi)
	movups	%xmm0, 16(%rdi)
	testl	%edx, %edx
	jne	.L509
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L509:
	.cfi_restore_state
	movq	%rsi, %rdi
	movl	%edx, %esi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	addq	$31, %rsi
	andq	$-8, %rsi
	subq	%rax, %rcx
	cmpq	%rcx, %rsi
	ja	.L510
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L504:
	pxor	%xmm0, %xmm0
	movl	$0, 4(%rax)
	movl	%edx, (%rax)
	movups	%xmm0, 8(%rax)
	movq	%rax, 16(%rbx)
	movq	%rax, 24(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L510:
	.cfi_restore_state
	movl	%edx, -20(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-20(%rbp), %edx
	jmp	.L504
	.cfi_endproc
.LFE25295:
	.size	_ZN2v88internal13ZoneChunkListIhEC2EPNS0_4ZoneENS2_9StartModeE, .-_ZN2v88internal13ZoneChunkListIhEC2EPNS0_4ZoneENS2_9StartModeE
	.weak	_ZN2v88internal13ZoneChunkListIhEC1EPNS0_4ZoneENS2_9StartModeE
	.set	_ZN2v88internal13ZoneChunkListIhEC1EPNS0_4ZoneENS2_9StartModeE,_ZN2v88internal13ZoneChunkListIhEC2EPNS0_4ZoneENS2_9StartModeE
	.section	.rodata._ZN2v88internal8compiler13CodeGeneratorC2EPNS0_4ZoneEPNS1_5FrameEPNS1_7LinkageEPNS1_19InstructionSequenceEPNS0_24OptimizedCompilationInfoEPNS0_7IsolateENS_4base8OptionalINS1_9OsrHelperEEEiPNS0_20JumpOptimizationInfoENS0_24PoisoningMitigationLevelERKNS0_16AssemblerOptionsEimSt10unique_ptrINS0_15AssemblerBufferESt14default_deleteISQ_EE.str1.8,"aMS",@progbits,1
	.align 8
.LC13:
	.string	"info->is_osr() == osr_helper_.has_value()"
	.section	.text._ZN2v88internal8compiler13CodeGeneratorC2EPNS0_4ZoneEPNS1_5FrameEPNS1_7LinkageEPNS1_19InstructionSequenceEPNS0_24OptimizedCompilationInfoEPNS0_7IsolateENS_4base8OptionalINS1_9OsrHelperEEEiPNS0_20JumpOptimizationInfoENS0_24PoisoningMitigationLevelERKNS0_16AssemblerOptionsEimSt10unique_ptrINS0_15AssemblerBufferESt14default_deleteISQ_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13CodeGeneratorC2EPNS0_4ZoneEPNS1_5FrameEPNS1_7LinkageEPNS1_19InstructionSequenceEPNS0_24OptimizedCompilationInfoEPNS0_7IsolateENS_4base8OptionalINS1_9OsrHelperEEEiPNS0_20JumpOptimizationInfoENS0_24PoisoningMitigationLevelERKNS0_16AssemblerOptionsEimSt10unique_ptrINS0_15AssemblerBufferESt14default_deleteISQ_EE
	.type	_ZN2v88internal8compiler13CodeGeneratorC2EPNS0_4ZoneEPNS1_5FrameEPNS1_7LinkageEPNS1_19InstructionSequenceEPNS0_24OptimizedCompilationInfoEPNS0_7IsolateENS_4base8OptionalINS1_9OsrHelperEEEiPNS0_20JumpOptimizationInfoENS0_24PoisoningMitigationLevelERKNS0_16AssemblerOptionsEimSt10unique_ptrINS0_15AssemblerBufferESt14default_deleteISQ_EE, @function
_ZN2v88internal8compiler13CodeGeneratorC2EPNS0_4ZoneEPNS1_5FrameEPNS1_7LinkageEPNS1_19InstructionSequenceEPNS0_24OptimizedCompilationInfoEPNS0_7IsolateENS_4base8OptionalINS1_9OsrHelperEEEiPNS0_20JumpOptimizationInfoENS0_24PoisoningMitigationLevelERKNS0_16AssemblerOptionsEimSt10unique_ptrINS0_15AssemblerBufferESt14default_deleteISQ_EE:
.LFB24246:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r11
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r9, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$56, %rdi
	subq	$72, %rsp
	movq	56(%rbp), %rax
	movq	72(%rbp), %rdx
	movq	96(%rbp), %rcx
	movq	16(%rbp), %r15
	movq	%rdx, -96(%rbp)
	movq	%rcx, -88(%rbp)
	movq	%rax, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN2v88internal8compiler13CodeGeneratorE(%rip), %rax
	movq	%rsi, -48(%rdi)
	movq	%rax, -56(%rdi)
	movq	%rsi, -8(%rdi)
	movq	%r15, -40(%rdi)
	movq	$0, -32(%rdi)
	movq	%r11, -24(%rdi)
	movq	%r8, -16(%rdi)
	movq	%rsi, -80(%rbp)
	movq	%rdi, -72(%rbp)
	call	_ZN2v88internal13EhFrameWriterC1EPNS0_4ZoneE@PLT
	movq	-80(%rbp), %rsi
	xorl	%eax, %eax
	movq	-72(%rbp), %rdi
	cmpb	$0, _ZN2v88internal29FLAG_perf_prof_unwinding_infoE(%rip)
	movw	%ax, 112(%rbx)
	movq	%rsi, 120(%rbx)
	movq	-88(%rbp), %rcx
	movq	$0, 128(%rbx)
	movq	-96(%rbp), %rdx
	movq	$0, 136(%rbx)
	movq	$0, 144(%rbx)
	jne	.L551
.L512:
	movq	16(%r14), %rax
	movq	8(%rbx), %rdi
	movq	%r12, 152(%rbx)
	movq	16(%rax), %rsi
	subq	8(%rax), %rsi
	movq	24(%rdi), %r8
	movq	16(%rdi), %rax
	sarq	$3, %rsi
	movslq	%esi, %rsi
	salq	$3, %rsi
	subq	%rax, %r8
	cmpq	%r8, %rsi
	ja	.L552
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L514:
	movq	%rax, 160(%rbx)
	movl	48(%rbp), %eax
	movabsq	$-140735340871681, %rsi
	leaq	200(%rbx), %rdi
	movq	$0, 168(%rbx)
	leaq	-64(%rbp), %r8
	movl	$-1, 176(%rbx)
	addl	$1, %eax
	movq	$0, 192(%rbx)
	cltq
	addq	%rax, %rax
	andq	%rsi, %rax
	movq	%r15, %rsi
	movq	%rax, 184(%rbx)
	movq	(%rcx), %rax
	movq	$0, (%rcx)
	xorl	%ecx, %ecx
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal18TurboAssemblerBaseC2EPNS0_7IsolateERKNS0_16AssemblerOptionsENS0_18CodeObjectRequiredESt10unique_ptrINS0_15AssemblerBufferESt14default_deleteIS9_EE@PLT
	movq	-64(%rbp), %rdi
	leaq	16+_ZTVN2v88internal14TurboAssemblerE(%rip), %rax
	movq	%rax, 200(%rbx)
	testq	%rdi, %rdi
	je	.L515
	movq	(%rdi), %rax
	call	*8(%rax)
.L515:
	movq	8(%rbx), %r15
	movq	%rbx, 744(%rbx)
	leaq	760(%rbx), %rdi
	xorl	%edx, %edx
	movb	$14, 752(%rbx)
	movq	%r15, %rsi
	call	_ZN2v88internal13ZoneChunkListINS0_21SafepointTableBuilder18DeoptimizationInfoEEC1EPNS0_4ZoneENS4_9StartModeE
	movq	8(%rbx), %rdi
	pxor	%xmm0, %xmm0
	movb	$0, 796(%rbx)
	movq	%r15, 800(%rbx)
	movq	%rdi, 808(%rbx)
	movq	$0, 832(%rbx)
	movq	$0, 840(%rbx)
	movq	%rdi, 848(%rbx)
	movq	$0, 856(%rbx)
	movq	$0, 864(%rbx)
	movq	$8, 872(%rbx)
	movups	%xmm0, 816(%rbx)
	movups	%xmm0, 880(%rbx)
	movups	%xmm0, 896(%rbx)
	movups	%xmm0, 912(%rbx)
	movups	%xmm0, 928(%rbx)
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$63, %rdx
	jbe	.L553
	leaq	64(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L517:
	movq	872(%rbx), %rdx
	movq	%rax, 864(%rbx)
	leaq	-4(,%rdx,4), %rdx
	andq	$-8, %rdx
	addq	%rax, %rdx
	movq	856(%rbx), %rax
	testq	%rax, %rax
	je	.L523
	cmpq	$63, 8(%rax)
	ja	.L554
.L523:
	movq	848(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	$511, %rcx
	jbe	.L519
	leaq	512(%rax), %rcx
	movq	%rcx, 16(%rdi)
.L521:
	movq	%rax, (%rdx)
	movq	8(%rbx), %rdi
	pxor	%xmm0, %xmm0
	movq	%rdx, 904(%rbx)
	movq	(%rdx), %rcx
	movq	%rdx, 936(%rbx)
	leaq	512(%rcx), %rax
	movq	%rcx, 888(%rbx)
	movq	%rax, 896(%rbx)
	movq	(%rdx), %rax
	movq	%rcx, 880(%rbx)
	leaq	512(%rax), %rdx
	movq	%rax, 920(%rbx)
	movq	%rdx, 928(%rbx)
	movq	%rax, 912(%rbx)
	movq	%rdi, 944(%rbx)
	movq	$0, 952(%rbx)
	movq	$0, 960(%rbx)
	movq	$8, 968(%rbx)
	movups	%xmm0, 976(%rbx)
	movups	%xmm0, 992(%rbx)
	movups	%xmm0, 1008(%rbx)
	movups	%xmm0, 1024(%rbx)
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$63, %rdx
	jbe	.L555
	leaq	64(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L525:
	movq	968(%rbx), %rdx
	movq	%rax, 960(%rbx)
	leaq	-4(,%rdx,4), %rdx
	andq	$-8, %rdx
	addq	%rax, %rdx
	movq	952(%rbx), %rax
	testq	%rax, %rax
	je	.L531
	cmpq	$15, 8(%rax)
	ja	.L556
.L531:
	movq	944(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	$511, %rcx
	jbe	.L527
	leaq	512(%rax), %rcx
	movq	%rcx, 16(%rdi)
.L529:
	movq	%rax, (%rdx)
	movq	8(%rbx), %rsi
	leaq	1048(%rbx), %rdi
	movq	%rdx, 1000(%rbx)
	movq	(%rdx), %rcx
	movq	%rdx, 1032(%rbx)
	leaq	512(%rcx), %rax
	movq	%rcx, 984(%rbx)
	movq	%rax, 992(%rbx)
	movq	(%rdx), %rax
	movq	%rcx, 976(%rbx)
	leaq	512(%rax), %rdx
	movq	%rax, 1016(%rbx)
	movq	%rdx, 1024(%rbx)
	xorl	%edx, %edx
	movq	%rax, 1008(%rbx)
	movq	$0, 1040(%rbx)
	call	_ZN2v88internal13ZoneChunkListIhEC1EPNS0_4ZoneENS2_9StartModeE
	movq	88(%rbp), %rax
	movdqu	24(%rbp), %xmm1
	movq	$0, 1080(%rbx)
	movb	$0, 1096(%rbx)
	pxor	%xmm0, %xmm0
	leaq	1152(%rbx), %rdi
	movl	$2, %esi
	movq	%rax, 1088(%rbx)
	movq	40(%rbp), %rax
	movq	$-1, 1144(%rbx)
	movq	%rax, 1136(%rbx)
	movups	%xmm0, 1104(%rbx)
	movups	%xmm1, 1120(%rbx)
	call	_ZN2v88internal26SourcePositionTableBuilderC1ENS1_13RecordingModeE@PLT
	movq	8(%rbx), %rax
	movl	64(%rbp), %edx
	pcmpeqd	%xmm0, %xmm0
	movq	$0, 1216(%rbx)
	movq	%rax, 1208(%rbx)
	movq	$0, 1224(%rbx)
	movq	$0, 1232(%rbx)
	movl	%edx, 1244(%rbx)
	movq	%rax, 1248(%rbx)
	movq	$0, 1256(%rbx)
	movq	$0, 1264(%rbx)
	movq	$0, 1272(%rbx)
	movq	%rax, 1312(%rbx)
	movq	$0, 1320(%rbx)
	movq	$0, 1328(%rbx)
	movq	$0, 1336(%rbx)
	movups	%xmm0, 1280(%rbx)
	movups	%xmm0, 1296(%rbx)
	movq	16(%r14), %rdx
	movl	$0, 1240(%rbx)
	movq	16(%rdx), %rax
	subq	8(%rdx), %rax
	sarq	$3, %rax
	testl	%eax, %eax
	jle	.L532
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L533:
	movq	160(%rbx), %rax
	movq	$0, (%rax,%rdx,8)
	movq	16(%r14), %rcx
	addq	$1, %rdx
	movq	16(%rcx), %rax
	subq	8(%rcx), %rax
	sarq	$3, %rax
	cmpl	%edx, %eax
	jg	.L533
.L532:
	movq	%rbx, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler13CodeGenerator11FinishFrameEPNS1_5FrameE@PLT
	movq	8(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$23, %rdx
	jbe	.L557
	leaq	24(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L535:
	movq	%r13, (%rax)
	movb	$0, 8(%rax)
	movl	$0, 12(%rax)
	movb	$0, 16(%rax)
	cmpl	$-1, 56(%r12)
	movq	%rax, 24(%rbx)
	setne	%al
	cmpb	%al, 1120(%rbx)
	jne	.L558
	movq	-104(%rbp), %rax
	movq	%rax, 416(%rbx)
	movl	8(%r12), %eax
	leal	-5(%rax), %edx
	cmpl	$3, %edx
	jbe	.L541
	cmpl	$10, %eax
	jne	.L537
.L541:
	movb	$1, 730(%rbx)
.L539:
	movl	80(%rbp), %eax
	movl	%eax, 732(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L559
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L537:
	.cfi_restore_state
	cmpl	$1552, 80(%rbp)
	ja	.L539
	movl	80(%rbp), %edi
	call	_ZN2v88internal8Builtins17IsWasmRuntimeStubEi@PLT
	testb	%al, %al
	jne	.L541
	jmp	.L539
	.p2align 4,,10
	.p2align 3
.L551:
	movq	%rcx, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal13EhFrameWriter10InitializeEv@PLT
	movq	-80(%rbp), %rcx
	movq	-72(%rbp), %rdx
	jmp	.L512
	.p2align 4,,10
	.p2align 3
.L556:
	movq	(%rax), %rcx
	movq	%rcx, 952(%rbx)
	jmp	.L529
	.p2align 4,,10
	.p2align 3
.L554:
	movq	(%rax), %rcx
	movq	%rcx, 856(%rbx)
	jmp	.L521
	.p2align 4,,10
	.p2align 3
.L552:
	movq	%rcx, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-72(%rbp), %rdx
	movq	-80(%rbp), %rcx
	jmp	.L514
	.p2align 4,,10
	.p2align 3
.L558:
	leaq	.LC13(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L557:
	movl	$24, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L535
	.p2align 4,,10
	.p2align 3
.L553:
	movl	$64, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L517
.L527:
	movl	$512, %esi
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-72(%rbp), %rdx
	jmp	.L529
.L519:
	movl	$512, %esi
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-72(%rbp), %rdx
	jmp	.L521
	.p2align 4,,10
	.p2align 3
.L555:
	movl	$64, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L525
.L559:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24246:
	.size	_ZN2v88internal8compiler13CodeGeneratorC2EPNS0_4ZoneEPNS1_5FrameEPNS1_7LinkageEPNS1_19InstructionSequenceEPNS0_24OptimizedCompilationInfoEPNS0_7IsolateENS_4base8OptionalINS1_9OsrHelperEEEiPNS0_20JumpOptimizationInfoENS0_24PoisoningMitigationLevelERKNS0_16AssemblerOptionsEimSt10unique_ptrINS0_15AssemblerBufferESt14default_deleteISQ_EE, .-_ZN2v88internal8compiler13CodeGeneratorC2EPNS0_4ZoneEPNS1_5FrameEPNS1_7LinkageEPNS1_19InstructionSequenceEPNS0_24OptimizedCompilationInfoEPNS0_7IsolateENS_4base8OptionalINS1_9OsrHelperEEEiPNS0_20JumpOptimizationInfoENS0_24PoisoningMitigationLevelERKNS0_16AssemblerOptionsEimSt10unique_ptrINS0_15AssemblerBufferESt14default_deleteISQ_EE
	.globl	_ZN2v88internal8compiler13CodeGeneratorC1EPNS0_4ZoneEPNS1_5FrameEPNS1_7LinkageEPNS1_19InstructionSequenceEPNS0_24OptimizedCompilationInfoEPNS0_7IsolateENS_4base8OptionalINS1_9OsrHelperEEEiPNS0_20JumpOptimizationInfoENS0_24PoisoningMitigationLevelERKNS0_16AssemblerOptionsEimSt10unique_ptrINS0_15AssemblerBufferESt14default_deleteISQ_EE
	.set	_ZN2v88internal8compiler13CodeGeneratorC1EPNS0_4ZoneEPNS1_5FrameEPNS1_7LinkageEPNS1_19InstructionSequenceEPNS0_24OptimizedCompilationInfoEPNS0_7IsolateENS_4base8OptionalINS1_9OsrHelperEEEiPNS0_20JumpOptimizationInfoENS0_24PoisoningMitigationLevelERKNS0_16AssemblerOptionsEimSt10unique_ptrINS0_15AssemblerBufferESt14default_deleteISQ_EE,_ZN2v88internal8compiler13CodeGeneratorC2EPNS0_4ZoneEPNS1_5FrameEPNS1_7LinkageEPNS1_19InstructionSequenceEPNS0_24OptimizedCompilationInfoEPNS0_7IsolateENS_4base8OptionalINS1_9OsrHelperEEEiPNS0_20JumpOptimizationInfoENS0_24PoisoningMitigationLevelERKNS0_16AssemblerOptionsEimSt10unique_ptrINS0_15AssemblerBufferESt14default_deleteISQ_EE
	.section	.rodata._ZNSt6vectorIPKN2v88internal8compiler19UnwindingInfoWriter17BlockInitialStateENS1_13ZoneAllocatorIS6_EEE17_M_default_appendEm.str1.1,"aMS",@progbits,1
.LC14:
	.string	"vector::_M_default_append"
	.section	.text._ZNSt6vectorIPKN2v88internal8compiler19UnwindingInfoWriter17BlockInitialStateENS1_13ZoneAllocatorIS6_EEE17_M_default_appendEm,"axG",@progbits,_ZNSt6vectorIPKN2v88internal8compiler19UnwindingInfoWriter17BlockInitialStateENS1_13ZoneAllocatorIS6_EEE17_M_default_appendEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPKN2v88internal8compiler19UnwindingInfoWriter17BlockInitialStateENS1_13ZoneAllocatorIS6_EEE17_M_default_appendEm
	.type	_ZNSt6vectorIPKN2v88internal8compiler19UnwindingInfoWriter17BlockInitialStateENS1_13ZoneAllocatorIS6_EEE17_M_default_appendEm, @function
_ZNSt6vectorIPKN2v88internal8compiler19UnwindingInfoWriter17BlockInitialStateENS1_13ZoneAllocatorIS6_EEE17_M_default_appendEm:
.LFB27234:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L588
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movl	$268435455, %esi
	subq	$24, %rsp
	movq	16(%rdi), %rcx
	movq	24(%r13), %rax
	movq	%rcx, %rdx
	subq	8(%rdi), %rdx
	subq	%rcx, %rax
	movq	%rsi, %rdi
	movq	%rdx, %r14
	sarq	$3, %rax
	sarq	$3, %r14
	subq	%r14, %rdi
	cmpq	%rbx, %rax
	jb	.L562
	salq	$3, %rbx
	movq	%rcx, %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	call	memset@PLT
	movq	%rax, %rcx
	addq	%rbx, %rcx
	movq	%rcx, 16(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L588:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L562:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	cmpq	%rbx, %rdi
	jb	.L591
	cmpq	%r14, %rbx
	movq	%r14, %r15
	movq	0(%r13), %rdi
	cmovnb	%rbx, %r15
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	addq	%r14, %r15
	cmpq	$268435455, %r15
	cmova	%rsi, %r15
	subq	%r12, %rax
	salq	$3, %r15
	movq	%r15, %rsi
	cmpq	%r15, %rax
	jb	.L592
	addq	%r12, %rsi
	movq	%rsi, 16(%rdi)
.L566:
	leaq	0(,%rbx,8), %r8
	leaq	(%r12,%rdx), %rdi
	xorl	%esi, %esi
	movq	%r8, %rdx
	call	memset@PLT
	movq	16(%r13), %rax
	movq	8(%r13), %rdx
	cmpq	%rdx, %rax
	je	.L571
	subq	$8, %rax
	leaq	15(%rdx), %rcx
	subq	%rdx, %rax
	subq	%r12, %rcx
	movq	%rax, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rcx
	jbe	.L568
	movabsq	$2305843009213693948, %rcx
	testq	%rcx, %rsi
	je	.L568
	addq	$1, %rsi
	movq	%rdx, %rdi
	movq	%r12, %rax
	movq	%rsi, %rcx
	subq	%r12, %rdi
	shrq	%rcx
	salq	$4, %rcx
	addq	%r12, %rcx
	.p2align 4,,10
	.p2align 3
.L569:
	movdqu	(%rax,%rdi), %xmm0
	addq	$16, %rax
	movups	%xmm0, -16(%rax)
	cmpq	%rax, %rcx
	jne	.L569
	movq	%rsi, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rcx
	addq	%rcx, %rdx
	addq	%r12, %rcx
	cmpq	%rax, %rsi
	je	.L571
	movq	(%rdx), %rax
	movq	%rax, (%rcx)
.L571:
	addq	%r14, %rbx
	movq	%r12, 8(%r13)
	leaq	(%r12,%rbx,8), %rax
	addq	%r15, %r12
	movq	%rax, 16(%r13)
	movq	%r12, 24(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L568:
	.cfi_restore_state
	leaq	8(%rax,%r12), %rsi
	subq	%r12, %rdx
	movq	%r12, %rax
	.p2align 4,,10
	.p2align 3
.L573:
	movq	(%rax,%rdx), %rcx
	addq	$8, %rax
	movq	%rcx, -8(%rax)
	cmpq	%rax, %rsi
	jne	.L573
	jmp	.L571
.L592:
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %r12
	jmp	.L566
.L591:
	leaq	.LC14(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE27234:
	.size	_ZNSt6vectorIPKN2v88internal8compiler19UnwindingInfoWriter17BlockInitialStateENS1_13ZoneAllocatorIS6_EEE17_M_default_appendEm, .-_ZNSt6vectorIPKN2v88internal8compiler19UnwindingInfoWriter17BlockInitialStateENS1_13ZoneAllocatorIS6_EEE17_M_default_appendEm
	.section	.rodata._ZNSt6vectorIN2v88internal8compiler30TurbolizerInstructionStartInfoENS1_13ZoneAllocatorIS3_EEE14_M_fill_assignEmRKS3_.str1.8,"aMS",@progbits,1
	.align 8
.LC15:
	.string	"cannot create std::vector larger than max_size()"
	.section	.text._ZNSt6vectorIN2v88internal8compiler30TurbolizerInstructionStartInfoENS1_13ZoneAllocatorIS3_EEE14_M_fill_assignEmRKS3_,"axG",@progbits,_ZNSt6vectorIN2v88internal8compiler30TurbolizerInstructionStartInfoENS1_13ZoneAllocatorIS3_EEE14_M_fill_assignEmRKS3_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal8compiler30TurbolizerInstructionStartInfoENS1_13ZoneAllocatorIS3_EEE14_M_fill_assignEmRKS3_
	.type	_ZNSt6vectorIN2v88internal8compiler30TurbolizerInstructionStartInfoENS1_13ZoneAllocatorIS3_EEE14_M_fill_assignEmRKS3_, @function
_ZNSt6vectorIN2v88internal8compiler30TurbolizerInstructionStartInfoENS1_13ZoneAllocatorIS3_EEE14_M_fill_assignEmRKS3_:
.LFB28122:
	.cfi_startproc
	endbr64
	movabsq	$-6148914691236517205, %r8
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	8(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	sarq	$2, %rcx
	imulq	%r8, %rcx
	cmpq	%rsi, %rcx
	jb	.L620
	movq	16(%rdi), %rdi
	movq	%rdi, %rcx
	subq	%rax, %rcx
	sarq	$2, %rcx
	imulq	%r8, %rcx
	cmpq	%rcx, %rsi
	ja	.L621
	testq	%rsi, %rsi
	je	.L606
	movq	%rax, %rcx
	movq	%rsi, %rdi
	.p2align 4,,10
	.p2align 3
.L607:
	movq	(%rdx), %r8
	addq	$12, %rcx
	movq	%r8, -12(%rcx)
	movl	8(%rdx), %r8d
	movl	%r8d, -4(%rcx)
	subq	$1, %rdi
	jne	.L607
	leaq	(%rsi,%rsi,2), %rdx
	movq	16(%rbx), %rdi
	leaq	(%rax,%rdx,4), %rax
.L606:
	cmpq	%rax, %rdi
	je	.L593
	movq	%rax, 16(%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L621:
	.cfi_restore_state
	cmpq	%rdi, %rax
	je	.L602
	.p2align 4,,10
	.p2align 3
.L603:
	movq	(%rdx), %rcx
	addq	$12, %rax
	movq	%rcx, -12(%rax)
	movl	8(%rdx), %ecx
	movl	%ecx, -4(%rax)
	cmpq	%rax, %rdi
	jne	.L603
	movabsq	$-6148914691236517205, %rax
	movq	16(%rbx), %rdi
	movq	%rdi, %rcx
	subq	8(%rbx), %rcx
	sarq	$2, %rcx
	imulq	%rax, %rcx
.L602:
	subq	%rcx, %rsi
	je	.L604
	movq	%rdi, %rax
	movq	%rsi, %rcx
	.p2align 4,,10
	.p2align 3
.L605:
	movq	(%rdx), %r8
	addq	$12, %rax
	movq	%r8, -12(%rax)
	movl	8(%rdx), %r8d
	movl	%r8d, -4(%rax)
	subq	$1, %rcx
	jne	.L605
	leaq	(%rsi,%rsi,2), %rax
	leaq	(%rdi,%rax,4), %rdi
.L604:
	movq	%rdi, 16(%rbx)
.L593:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L620:
	.cfi_restore_state
	cmpq	$178956970, %rsi
	ja	.L622
	movq	(%rdi), %r8
	leaq	(%rsi,%rsi,2), %rdi
	leaq	0(,%rdi,4), %r12
	testq	%rsi, %rsi
	je	.L608
	movq	16(%r8), %rax
	movq	24(%r8), %rcx
	leaq	7(%r12), %rsi
	andq	$-8, %rsi
	subq	%rax, %rcx
	cmpq	%rcx, %rsi
	ja	.L623
	addq	%rax, %rsi
	movq	%rsi, 16(%r8)
.L598:
	leaq	(%rax,%r12), %rdi
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L599:
	movq	(%rdx), %rsi
	addq	$12, %rcx
	movq	%rsi, -12(%rcx)
	movl	8(%rdx), %esi
	movl	%esi, -4(%rcx)
	cmpq	%rcx, %rdi
	jne	.L599
.L596:
	movq	%rax, %xmm0
	movq	%rdi, %xmm1
	movq	%rdi, 24(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 8(%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L608:
	.cfi_restore_state
	xorl	%eax, %eax
	xorl	%edi, %edi
	jmp	.L596
.L623:
	movq	%r8, %rdi
	movq	%rdx, -24(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-24(%rbp), %rdx
	jmp	.L598
.L622:
	leaq	.LC15(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE28122:
	.size	_ZNSt6vectorIN2v88internal8compiler30TurbolizerInstructionStartInfoENS1_13ZoneAllocatorIS3_EEE14_M_fill_assignEmRKS3_, .-_ZNSt6vectorIN2v88internal8compiler30TurbolizerInstructionStartInfoENS1_13ZoneAllocatorIS3_EEE14_M_fill_assignEmRKS3_
	.section	.text._ZNSt6vectorIPN2v88internal8compiler12MoveOperandsENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm,"axG",@progbits,_ZNSt6vectorIPN2v88internal8compiler12MoveOperandsENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal8compiler12MoveOperandsENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm
	.type	_ZNSt6vectorIPN2v88internal8compiler12MoveOperandsENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm, @function
_ZNSt6vectorIPN2v88internal8compiler12MoveOperandsENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm:
.LFB28210:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L652
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movl	$268435455, %esi
	subq	$24, %rsp
	movq	16(%rdi), %rcx
	movq	24(%r13), %rax
	movq	%rcx, %rdx
	subq	8(%rdi), %rdx
	subq	%rcx, %rax
	movq	%rsi, %rdi
	movq	%rdx, %r14
	sarq	$3, %rax
	sarq	$3, %r14
	subq	%r14, %rdi
	cmpq	%rbx, %rax
	jb	.L626
	salq	$3, %rbx
	movq	%rcx, %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	call	memset@PLT
	movq	%rax, %rcx
	addq	%rbx, %rcx
	movq	%rcx, 16(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L652:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L626:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	cmpq	%rbx, %rdi
	jb	.L655
	cmpq	%r14, %rbx
	movq	%r14, %r15
	movq	0(%r13), %rdi
	cmovnb	%rbx, %r15
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	addq	%r14, %r15
	cmpq	$268435455, %r15
	cmova	%rsi, %r15
	subq	%r12, %rax
	salq	$3, %r15
	movq	%r15, %rsi
	cmpq	%r15, %rax
	jb	.L656
	addq	%r12, %rsi
	movq	%rsi, 16(%rdi)
.L630:
	leaq	0(,%rbx,8), %r8
	leaq	(%r12,%rdx), %rdi
	xorl	%esi, %esi
	movq	%r8, %rdx
	call	memset@PLT
	movq	16(%r13), %rax
	movq	8(%r13), %rdx
	cmpq	%rdx, %rax
	je	.L635
	subq	$8, %rax
	leaq	15(%rdx), %rcx
	subq	%rdx, %rax
	subq	%r12, %rcx
	movq	%rax, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rcx
	jbe	.L632
	movabsq	$2305843009213693948, %rcx
	testq	%rcx, %rsi
	je	.L632
	addq	$1, %rsi
	movq	%rdx, %rdi
	movq	%r12, %rax
	movq	%rsi, %rcx
	subq	%r12, %rdi
	shrq	%rcx
	salq	$4, %rcx
	addq	%r12, %rcx
	.p2align 4,,10
	.p2align 3
.L633:
	movdqu	(%rax,%rdi), %xmm0
	addq	$16, %rax
	movups	%xmm0, -16(%rax)
	cmpq	%rax, %rcx
	jne	.L633
	movq	%rsi, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rcx
	addq	%rcx, %rdx
	addq	%r12, %rcx
	cmpq	%rax, %rsi
	je	.L635
	movq	(%rdx), %rax
	movq	%rax, (%rcx)
.L635:
	addq	%r14, %rbx
	movq	%r12, 8(%r13)
	leaq	(%r12,%rbx,8), %rax
	addq	%r15, %r12
	movq	%rax, 16(%r13)
	movq	%r12, 24(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L632:
	.cfi_restore_state
	leaq	8(%rax,%r12), %rsi
	subq	%r12, %rdx
	movq	%r12, %rax
	.p2align 4,,10
	.p2align 3
.L637:
	movq	(%rax,%rdx), %rcx
	addq	$8, %rax
	movq	%rcx, -8(%rax)
	cmpq	%rax, %rsi
	jne	.L637
	jmp	.L635
.L656:
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %r12
	jmp	.L630
.L655:
	leaq	.LC14(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE28210:
	.size	_ZNSt6vectorIPN2v88internal8compiler12MoveOperandsENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm, .-_ZNSt6vectorIPN2v88internal8compiler12MoveOperandsENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm
	.section	.text._ZN2v88internal8compiler13CodeGenerator22GetPushCompatibleMovesEPNS1_11InstructionENS_4base5FlagsINS2_12PushTypeFlagEiEEPNS0_10ZoneVectorIPNS1_12MoveOperandsEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13CodeGenerator22GetPushCompatibleMovesEPNS1_11InstructionENS_4base5FlagsINS2_12PushTypeFlagEiEEPNS0_10ZoneVectorIPNS1_12MoveOperandsEEE
	.type	_ZN2v88internal8compiler13CodeGenerator22GetPushCompatibleMovesEPNS1_11InstructionENS_4base5FlagsINS2_12PushTypeFlagEiEEPNS0_10ZoneVectorIPNS1_12MoveOperandsEEE, @function
_ZN2v88internal8compiler13CodeGenerator22GetPushCompatibleMovesEPNS1_11InstructionENS_4base5FlagsINS2_12PushTypeFlagEiEEPNS0_10ZoneVectorIPNS1_12MoveOperandsEEE:
.LFB24285:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rcx
	movl	%esi, %r9d
	movq	%rdx, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdx), %rdi
	cmpq	%rdi, 16(%rdx)
	je	.L658
	movq	%rdi, 16(%rdx)
.L658:
	xorl	%r13d, %r13d
	movl	%r9d, %edx
	movq	%rdi, %r10
	movq	8(%rcx,%r13,8), %rax
	andl	$4, %edx
	testq	%rax, %rax
	jne	.L707
.L659:
	cmpq	$1, %r13
	jne	.L682
	movq	%r10, %rbx
	subq	%rdi, %rbx
	sarq	$3, %rbx
	movq	%rbx, %rcx
	cmpq	%r10, %rdi
	je	.L657
	movq	%rbx, %rdx
	movq	%r10, %rax
	jmp	.L679
	.p2align 4,,10
	.p2align 3
.L708:
	subq	$8, %rax
	subq	$1, %rdx
	cmpq	%rdi, %rax
	je	.L678
.L679:
	cmpq	$0, -8(%rax)
	jne	.L708
.L678:
	subq	%rdx, %rbx
	movq	%rbx, %r12
	salq	$3, %r12
	je	.L680
	leaq	(%rdi,%rdx,8), %rsi
	movq	%r12, %rdx
	movq	%r8, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %r8
	movq	16(%r8), %r10
	movq	8(%r8), %rdi
	movq	%r10, %rcx
	subq	%rdi, %rcx
	sarq	$3, %rcx
.L680:
	cmpq	%rcx, %rbx
	ja	.L709
	jnb	.L657
	addq	%r12, %rdi
.L706:
	cmpq	%r10, %rdi
	je	.L657
	movq	%rdi, 16(%r8)
.L657:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L682:
	.cfi_restore_state
	movl	$1, %r13d
	movq	8(%rcx,%r13,8), %rax
	testq	%rax, %rax
	je	.L659
.L707:
	movq	8(%rax), %rbx
	movq	16(%rax), %r14
	cmpq	%r14, %rbx
	jne	.L675
	jmp	.L659
	.p2align 4,,10
	.p2align 3
.L663:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	je	.L659
.L675:
	movq	(%rbx), %r12
	movq	(%r12), %rax
	movl	%eax, %r11d
	andl	$4, %r11d
	je	.L660
	movq	%rax, %rsi
	shrq	$3, %rsi
	andl	$3, %esi
	cmpl	$1, %esi
	je	.L710
.L660:
	testq	%r13, %r13
	jne	.L663
	movq	8(%r12), %r15
	testb	$4, %r15b
	je	.L663
	movq	%r15, %rsi
	shrq	$3, %rsi
	andl	$3, %esi
	cmpl	$1, %esi
	jne	.L663
	movq	%r15, %rsi
	shrq	$5, %rsi
	cmpb	$11, %sil
	ja	.L663
	sarq	$35, %r15
	testq	%r15, %r15
	jle	.L663
	movl	%eax, %esi
	andl	$7, %esi
	cmpl	$3, %esi
	je	.L711
	testl	%r11d, %r11d
	je	.L663
	movq	%rax, %rsi
	shrq	$3, %rsi
	andl	$3, %esi
	jne	.L670
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L663
	testb	$2, %r9b
	je	.L663
.L672:
	movq	%r10, %rax
	subq	%rdi, %rax
	sarq	$3, %rax
	cmpl	%eax, %r15d
	jge	.L712
.L669:
	movq	%r12, (%rdi,%r15,8)
	movq	8(%r8), %rdi
	movq	16(%r8), %r10
	jmp	.L663
	.p2align 4,,10
	.p2align 3
.L710:
	movq	%rax, %rsi
	shrq	$5, %rsi
	cmpb	$11, %sil
	ja	.L660
	movq	%rax, %rsi
	sarq	$35, %rsi
	testq	%rsi, %rsi
	jle	.L660
	jmp	.L706
	.p2align 4,,10
	.p2align 3
.L709:
	addq	$40, %rsp
	subq	%rcx, %rbx
	movq	%r8, %rdi
	movq	%rbx, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNSt6vectorIPN2v88internal8compiler12MoveOperandsENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm
.L711:
	.cfi_restore_state
	testb	$1, %r9b
	je	.L663
	jmp	.L672
.L670:
	cmpl	$1, %esi
	jne	.L663
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L663
	testl	%edx, %edx
	je	.L663
	jmp	.L672
.L712:
	leal	1(%r15), %esi
	movslq	%esi, %rsi
	cmpq	%rax, %rsi
	ja	.L713
	jnb	.L669
	leaq	(%rdi,%rsi,8), %rax
	cmpq	%r10, %rax
	je	.L669
	movq	%rax, 16(%r8)
	jmp	.L669
.L713:
	movq	%r8, %rdi
	subq	%rax, %rsi
	movl	%edx, -72(%rbp)
	movl	%r9d, -68(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler12MoveOperandsENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %rcx
	movl	-68(%rbp), %r9d
	movl	-72(%rbp), %edx
	movq	8(%r8), %rdi
	jmp	.L669
	.cfi_endproc
.LFE24285:
	.size	_ZN2v88internal8compiler13CodeGenerator22GetPushCompatibleMovesEPNS1_11InstructionENS_4base5FlagsINS2_12PushTypeFlagEiEEPNS0_10ZoneVectorIPNS1_12MoveOperandsEEE, .-_ZN2v88internal8compiler13CodeGenerator22GetPushCompatibleMovesEPNS1_11InstructionENS_4base5FlagsINS2_12PushTypeFlagEiEEPNS0_10ZoneVectorIPNS1_12MoveOperandsEEE
	.section	.rodata._ZNSt5dequeIN2v88internal8compiler21DeoptimizationLiteralENS1_22RecyclingZoneAllocatorIS3_EEE16_M_push_back_auxIJRKS3_EEEvDpOT_.str1.8,"aMS",@progbits,1
	.align 8
.LC16:
	.string	"cannot create std::deque larger than max_size()"
	.section	.text._ZNSt5dequeIN2v88internal8compiler21DeoptimizationLiteralENS1_22RecyclingZoneAllocatorIS3_EEE16_M_push_back_auxIJRKS3_EEEvDpOT_,"axG",@progbits,_ZNSt5dequeIN2v88internal8compiler21DeoptimizationLiteralENS1_22RecyclingZoneAllocatorIS3_EEE16_M_push_back_auxIJRKS3_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt5dequeIN2v88internal8compiler21DeoptimizationLiteralENS1_22RecyclingZoneAllocatorIS3_EEE16_M_push_back_auxIJRKS3_EEEvDpOT_
	.type	_ZNSt5dequeIN2v88internal8compiler21DeoptimizationLiteralENS1_22RecyclingZoneAllocatorIS3_EEE16_M_push_back_auxIJRKS3_EEEvDpOT_, @function
_ZNSt5dequeIN2v88internal8compiler21DeoptimizationLiteralENS1_22RecyclingZoneAllocatorIS3_EEE16_M_push_back_auxIJRKS3_EEEvDpOT_:
.LFB28249:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	88(%rdi), %r13
	movq	56(%rdi), %rsi
	movq	64(%rbx), %rdx
	subq	72(%rbx), %rdx
	movq	%r13, %r14
	movq	%rdx, %rcx
	subq	%rsi, %r14
	sarq	$5, %rcx
	movq	%r14, %rdi
	sarq	$3, %rdi
	leaq	-1(%rdi), %rax
	salq	$4, %rax
	leaq	(%rcx,%rax), %rdx
	movq	48(%rbx), %rax
	subq	32(%rbx), %rax
	sarq	$5, %rax
	addq	%rdx, %rax
	cmpq	$67108863, %rax
	je	.L732
	movq	16(%rbx), %rcx
	movq	24(%rbx), %rdx
	movq	%r13, %rax
	subq	%rcx, %rax
	movq	%rdx, %r8
	sarq	$3, %rax
	subq	%rax, %r8
	cmpq	$1, %r8
	jbe	.L733
.L716:
	movq	8(%rbx), %rax
	testq	%rax, %rax
	je	.L724
	cmpq	$15, 8(%rax)
	ja	.L734
.L724:
	movq	(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L735
	leaq	512(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L725:
	movq	%rax, 8(%r13)
	movdqu	(%r12), %xmm1
	movq	64(%rbx), %rax
	movups	%xmm1, (%rax)
	movdqu	16(%r12), %xmm2
	movups	%xmm2, 16(%rax)
	movq	88(%rbx), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 88(%rbx)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 72(%rbx)
	movq	%rdx, 80(%rbx)
	movq	%rax, 64(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L733:
	.cfi_restore_state
	leaq	2(%rdi), %r15
	leaq	(%r15,%r15), %rax
	cmpq	%rax, %rdx
	ja	.L736
	testq	%rdx, %rdx
	movl	$1, %eax
	movq	(%rbx), %rdi
	cmovne	%rdx, %rax
	movq	16(%rdi), %r13
	leaq	2(%rdx,%rax), %rcx
	movq	24(%rdi), %rax
	leaq	0(,%rcx,8), %rsi
	subq	%r13, %rax
	cmpq	%rax, %rsi
	ja	.L737
	addq	%r13, %rsi
	movq	%rsi, 16(%rdi)
.L721:
	movq	%rcx, %rax
	movq	56(%rbx), %rsi
	subq	%r15, %rax
	shrq	%rax
	leaq	0(%r13,%rax,8), %r15
	movq	88(%rbx), %rax
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L722
	subq	%rsi, %rdx
	movq	%r15, %rdi
	movq	%rcx, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %rcx
.L722:
	movq	24(%rbx), %rax
	leaq	0(,%rax,8), %rdx
	cmpq	$15, %rdx
	jbe	.L723
	movq	16(%rbx), %rdx
	movq	%rax, 8(%rdx)
	movq	$0, (%rdx)
.L723:
	movq	%r13, 16(%rbx)
	movq	%rcx, 24(%rbx)
.L719:
	movq	%r15, 56(%rbx)
	movq	(%r15), %rax
	leaq	(%r15,%r14), %r13
	movq	(%r15), %xmm0
	movq	%r13, 88(%rbx)
	addq	$512, %rax
	movq	%rax, %xmm3
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 40(%rbx)
	movq	0(%r13), %rax
	movq	%rax, 72(%rbx)
	addq	$512, %rax
	movq	%rax, 80(%rbx)
	jmp	.L716
	.p2align 4,,10
	.p2align 3
.L734:
	movq	(%rax), %rdx
	movq	%rdx, 8(%rbx)
	jmp	.L725
	.p2align 4,,10
	.p2align 3
.L736:
	subq	%r15, %rdx
	addq	$8, %r13
	shrq	%rdx
	leaq	(%rcx,%rdx,8), %r15
	movq	%r13, %rdx
	subq	%rsi, %rdx
	cmpq	%r15, %rsi
	jbe	.L718
	cmpq	%r13, %rsi
	je	.L719
	movq	%r15, %rdi
	call	memmove@PLT
	jmp	.L719
	.p2align 4,,10
	.p2align 3
.L718:
	cmpq	%r13, %rsi
	je	.L719
	leaq	8(%r14), %rdi
	subq	%rdx, %rdi
	addq	%r15, %rdi
	call	memmove@PLT
	jmp	.L719
	.p2align 4,,10
	.p2align 3
.L735:
	movl	$512, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L725
	.p2align 4,,10
	.p2align 3
.L737:
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	movq	%rax, %r13
	jmp	.L721
.L732:
	leaq	.LC16(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE28249:
	.size	_ZNSt5dequeIN2v88internal8compiler21DeoptimizationLiteralENS1_22RecyclingZoneAllocatorIS3_EEE16_M_push_back_auxIJRKS3_EEEvDpOT_, .-_ZNSt5dequeIN2v88internal8compiler21DeoptimizationLiteralENS1_22RecyclingZoneAllocatorIS3_EEE16_M_push_back_auxIJRKS3_EEEvDpOT_
	.section	.text._ZN2v88internal8compiler13CodeGenerator27DefineDeoptimizationLiteralENS1_21DeoptimizationLiteralE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13CodeGenerator27DefineDeoptimizationLiteralENS1_21DeoptimizationLiteralE
	.type	_ZN2v88internal8compiler13CodeGenerator27DefineDeoptimizationLiteralENS1_21DeoptimizationLiteralE, @function
_ZN2v88internal8compiler13CodeGenerator27DefineDeoptimizationLiteralENS1_21DeoptimizationLiteralE:
.LFB24303:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	1000(%rdi), %r9
	movq	1032(%rdi), %rax
	movq	1008(%rdi), %r11
	movq	976(%rdi), %r10
	subq	%r9, %rax
	sarq	$3, %rax
	movq	%r11, %rsi
	subq	1016(%rdi), %rsi
	subq	$1, %rax
	sarq	$5, %rsi
	salq	$4, %rax
	addq	%rsi, %rax
	movq	992(%rdi), %rsi
	subq	%r10, %rsi
	sarq	$5, %rsi
	addq	%rax, %rsi
	movl	%esi, %r12d
	je	.L749
	movq	%r10, %rcx
	subq	984(%rdi), %rcx
	movl	16(%rbp), %r8d
	xorl	%edx, %edx
	sarq	$5, %rcx
	xorl	%ebx, %ebx
	xorl	%eax, %eax
	jmp	.L748
	.p2align 4,,10
	.p2align 3
.L754:
	cmpq	$15, %rax
	jg	.L743
	salq	$5, %rbx
	leaq	(%r10,%rbx), %rax
	cmpl	(%rax), %r8d
	je	.L753
	.p2align 4,,10
	.p2align 3
.L746:
	leal	1(%rdx), %eax
	movq	%rax, %rdx
	movq	%rax, %rbx
	cmpq	%rsi, %rax
	jnb	.L749
.L748:
	addq	%rcx, %rax
	jns	.L754
	movq	%rax, %rbx
	notq	%rbx
	shrq	$4, %rbx
	notq	%rbx
.L745:
	movq	%rbx, %r13
	salq	$4, %r13
	subq	%r13, %rax
	salq	$5, %rax
	addq	(%r9,%rbx,8), %rax
	cmpl	(%rax), %r8d
	jne	.L746
.L753:
	movq	8(%rax), %rbx
	cmpq	%rbx, 24(%rbp)
	jne	.L746
	movq	32(%rbp), %rbx
	cmpq	%rbx, 16(%rax)
	jne	.L746
	movq	40(%rbp), %rbx
	cmpq	%rbx, 24(%rax)
	jne	.L746
	addq	$8, %rsp
	movl	%edx, %r12d
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L743:
	.cfi_restore_state
	movq	%rax, %rbx
	sarq	$4, %rbx
	jmp	.L745
	.p2align 4,,10
	.p2align 3
.L749:
	movq	1024(%rdi), %rax
	subq	$32, %rax
	cmpq	%rax, %r11
	je	.L755
	movdqu	16(%rbp), %xmm0
	movl	%r12d, %eax
	movups	%xmm0, (%r11)
	movdqu	32(%rbp), %xmm1
	movups	%xmm1, 16(%r11)
	addq	$32, 1008(%rdi)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L755:
	.cfi_restore_state
	leaq	16(%rbp), %rsi
	addq	$944, %rdi
	call	_ZNSt5dequeIN2v88internal8compiler21DeoptimizationLiteralENS1_22RecyclingZoneAllocatorIS3_EEE16_M_push_back_auxIJRKS3_EEEvDpOT_
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24303:
	.size	_ZN2v88internal8compiler13CodeGenerator27DefineDeoptimizationLiteralENS1_21DeoptimizationLiteralE, .-_ZN2v88internal8compiler13CodeGenerator27DefineDeoptimizationLiteralENS1_21DeoptimizationLiteralE
	.section	.rodata._ZN2v88internal8compiler13CodeGenerator24AddTranslationForOperandEPNS0_11TranslationEPNS1_11InstructionEPNS1_18InstructionOperandENS0_11MachineTypeE.str1.8,"aMS",@progbits,1
	.align 8
.LC17:
	.string	"MachineRepresentation::kTagged == type.representation()"
	.align 8
.LC18:
	.string	"MachineRepresentation::kFloat32 == type.representation()"
	.section	.rodata._ZN2v88internal8compiler13CodeGenerator24AddTranslationForOperandEPNS0_11TranslationEPNS1_11InstructionEPNS1_18InstructionOperandENS0_11MachineTypeE.str1.1,"aMS",@progbits,1
.LC19:
	.string	"op->IsImmediate()"
	.section	.text._ZN2v88internal8compiler13CodeGenerator24AddTranslationForOperandEPNS0_11TranslationEPNS1_11InstructionEPNS1_18InstructionOperandENS0_11MachineTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13CodeGenerator24AddTranslationForOperandEPNS0_11TranslationEPNS1_11InstructionEPNS1_18InstructionOperandENS0_11MachineTypeE
	.type	_ZN2v88internal8compiler13CodeGenerator24AddTranslationForOperandEPNS0_11TranslationEPNS1_11InstructionEPNS1_18InstructionOperandENS0_11MachineTypeE, @function
_ZN2v88internal8compiler13CodeGenerator24AddTranslationForOperandEPNS0_11TranslationEPNS1_11InstructionEPNS1_18InstructionOperandENS0_11MachineTypeE:
.LFB24309:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%r8d, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%r8d, %ebx
	movzbl	%bh, %ebx
	subq	$80, %rsp
	movq	(%rcx), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testb	$4, %sil
	je	.L757
	movq	%rsi, %rax
	shrq	$3, %rax
	andl	$3, %eax
	cmpl	$1, %eax
	je	.L887
	testl	%eax, %eax
	jne	.L813
	movq	%rsi, %rax
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L774
	cmpb	$1, %r14b
	je	.L888
	cmpb	$2, %r14b
	je	.L886
	cmpb	$3, %r14b
	je	.L886
	cmpb	$4, %r14b
	sete	%al
	cmpb	$2, %bl
	jne	.L811
	testb	%al, %al
	jne	.L777
.L811:
	cmpb	$3, %bl
	jne	.L783
	testb	%al, %al
	jne	.L781
.L783:
	cmpb	$4, %bl
	jne	.L782
	cmpb	$5, %r14b
	je	.L889
.L782:
	cmpb	$8, %r14b
	jne	.L769
	sarq	$35, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11Translation13StoreRegisterENS0_8RegisterE@PLT
	jmp	.L756
	.p2align 4,,10
	.p2align 3
.L757:
	movl	%esi, %eax
	andl	$7, %eax
	cmpl	$3, %eax
	jne	.L813
	movq	%rsi, %rax
	movq	%rdi, %r13
	sarq	$32, %rax
	andl	$8, %esi
	je	.L890
	movq	40(%rdi), %rdx
	salq	$4, %rax
	addq	152(%rdx), %rax
	movq	%rax, %rdx
	movl	(%rax), %eax
	movzbl	4(%rdx), %ecx
	movq	8(%rdx), %rdx
.L788:
	movl	%eax, -96(%rbp)
	movb	%cl, -92(%rbp)
	movq	%rdx, -88(%rbp)
	movq	$0x000000000, -64(%rbp)
	movq	$0, -56(%rbp)
	cmpl	$8, %eax
	ja	.L789
	leaq	.L791(%rip), %rcx
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler13CodeGenerator24AddTranslationForOperandEPNS0_11TranslationEPNS1_11InstructionEPNS1_18InstructionOperandENS0_11MachineTypeE,"a",@progbits
	.align 4
	.align 4
.L791:
	.long	.L797-.L791
	.long	.L796-.L791
	.long	.L795-.L791
	.long	.L794-.L791
	.long	.L789-.L791
	.long	.L793-.L791
	.long	.L793-.L791
	.long	.L789-.L791
	.long	.L790-.L791
	.section	.text._ZN2v88internal8compiler13CodeGenerator24AddTranslationForOperandEPNS0_11TranslationEPNS1_11InstructionEPNS1_18InstructionOperandENS0_11MachineTypeE
	.p2align 4,,10
	.p2align 3
.L890:
	movl	%eax, %esi
	leaq	-80(%rbp), %rdi
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movl	-80(%rbp), %eax
	movzbl	-76(%rbp), %ecx
	movq	-72(%rbp), %rdx
	jmp	.L788
	.p2align 4,,10
	.p2align 3
.L887:
	movq	%rsi, %rax
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L759
	cmpb	$1, %r8b
	je	.L891
	cmpb	$2, %r8b
	je	.L884
	cmpb	$3, %r8b
	je	.L884
	cmpb	$4, %r8b
	sete	%al
	cmpb	$2, %bl
	jne	.L809
	testb	%al, %al
	jne	.L763
.L809:
	cmpb	$3, %bl
	jne	.L770
	testb	%al, %al
	jne	.L767
.L770:
	cmpb	$5, %r14b
	jne	.L768
	cmpb	$4, %bl
	je	.L892
.L768:
	cmpb	$8, %r14b
	jne	.L769
	sarq	$35, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11Translation14StoreStackSlotEi@PLT
	jmp	.L756
	.p2align 4,,10
	.p2align 3
.L793:
	leaq	-96(%rbp), %rdi
	call	_ZNK2v88internal8compiler8Constant12ToHeapObjectEv@PLT
	movl	$0, -80(%rbp)
	movq	%rax, %rcx
.L799:
	movq	152(%r13), %rdx
	cmpq	%rcx, 32(%rdx)
	je	.L893
.L806:
	pushq	-56(%rbp)
	movq	%r13, %rdi
	pushq	-64(%rbp)
	movq	%rax, -72(%rbp)
	pushq	-72(%rbp)
	pushq	-80(%rbp)
	call	_ZN2v88internal8compiler13CodeGenerator27DefineDeoptimizationLiteralENS1_21DeoptimizationLiteralE
	addq	$32, %rsp
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal11Translation12StoreLiteralEi@PLT
.L756:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L894
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L774:
	.cfi_restore_state
	cmpb	$13, %r14b
	je	.L895
	cmpb	$12, %r14b
	jne	.L785
	sarq	$35, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11Translation18StoreFloatRegisterENS0_11XMMRegisterE@PLT
	jmp	.L756
	.p2align 4,,10
	.p2align 3
.L790:
	leaq	-96(%rbp), %rdi
	call	_ZNK2v88internal8compiler8Constant23ToDelayedStringConstantEv@PLT
	movl	$2, -80(%rbp)
	xorl	%ecx, %ecx
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	jmp	.L799
	.p2align 4,,10
	.p2align 3
.L794:
	movl	$1, -80(%rbp)
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	movq	%rdx, -64(%rbp)
	jmp	.L799
	.p2align 4,,10
	.p2align 3
.L795:
	movl	%edx, -100(%rbp)
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	movl	$1, -80(%rbp)
	cvtss2sd	-100(%rbp), %xmm0
	movsd	%xmm0, -64(%rbp)
	jmp	.L799
	.p2align 4,,10
	.p2align 3
.L796:
	movl	$1, -80(%rbp)
	cmpb	$5, %r14b
	je	.L896
.L805:
	sarq	$32, %rdx
.L882:
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	cvtsi2sdl	%edx, %xmm0
	movq	152(%r13), %rdx
	movsd	%xmm0, -64(%rbp)
	cmpq	%rcx, 32(%rdx)
	jne	.L806
	.p2align 4,,10
	.p2align 3
.L893:
	movq	%r12, %rdi
	call	_ZN2v88internal11Translation20StoreJSFrameFunctionEv@PLT
	jmp	.L756
	.p2align 4,,10
	.p2align 3
.L797:
	cmpb	$8, %r14b
	je	.L897
	cmpb	$1, %r14b
	je	.L898
	movl	$1, -80(%rbp)
	cmpb	$3, %bl
	jne	.L882
	cmpb	$4, %r14b
	jne	.L882
	movl	%edx, %edx
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	cvtsi2sdq	%rdx, %xmm0
	movsd	%xmm0, -64(%rbp)
	jmp	.L799
	.p2align 4,,10
	.p2align 3
.L759:
	cmpb	$13, %r8b
	je	.L899
	cmpb	$12, %r8b
	jne	.L785
	sarq	$35, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11Translation19StoreFloatStackSlotEi@PLT
	jmp	.L756
	.p2align 4,,10
	.p2align 3
.L813:
	leaq	.LC19(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L789:
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L884:
	cmpb	$2, %bl
	je	.L763
	cmpb	$3, %bl
	je	.L767
.L769:
	leaq	.LC17(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L886:
	cmpb	$2, %bl
	je	.L777
	cmpb	$3, %bl
	jne	.L769
.L781:
	sarq	$35, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11Translation19StoreUint32RegisterENS0_8RegisterE@PLT
	jmp	.L756
	.p2align 4,,10
	.p2align 3
.L898:
	movq	16(%r13), %rax
	testl	%edx, %edx
	jne	.L801
	addq	$120, %rax
	movl	$0, -80(%rbp)
	movq	%rax, %rcx
	jmp	.L799
	.p2align 4,,10
	.p2align 3
.L897:
	movl	$1, -80(%rbp)
	movslq	%edx, %rdx
	jmp	.L805
	.p2align 4,,10
	.p2align 3
.L896:
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	cvtsi2sdq	%rdx, %xmm0
	movsd	%xmm0, -64(%rbp)
	jmp	.L799
	.p2align 4,,10
	.p2align 3
.L777:
	sarq	$35, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11Translation18StoreInt32RegisterENS0_8RegisterE@PLT
	jmp	.L756
	.p2align 4,,10
	.p2align 3
.L888:
	sarq	$35, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11Translation17StoreBoolRegisterENS0_8RegisterE@PLT
	jmp	.L756
	.p2align 4,,10
	.p2align 3
.L763:
	sarq	$35, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11Translation19StoreInt32StackSlotEi@PLT
	jmp	.L756
	.p2align 4,,10
	.p2align 3
.L895:
	sarq	$35, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11Translation19StoreDoubleRegisterENS0_11XMMRegisterE@PLT
	jmp	.L756
	.p2align 4,,10
	.p2align 3
.L891:
	sarq	$35, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11Translation18StoreBoolStackSlotEi@PLT
	jmp	.L756
	.p2align 4,,10
	.p2align 3
.L899:
	sarq	$35, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11Translation20StoreDoubleStackSlotEi@PLT
	jmp	.L756
	.p2align 4,,10
	.p2align 3
.L767:
	sarq	$35, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11Translation20StoreUint32StackSlotEi@PLT
	jmp	.L756
	.p2align 4,,10
	.p2align 3
.L785:
	leaq	.LC18(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L801:
	addq	$112, %rax
	movl	$0, -80(%rbp)
	movq	%rax, %rcx
	jmp	.L799
.L889:
	sarq	$35, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11Translation18StoreInt64RegisterENS0_8RegisterE@PLT
	jmp	.L756
.L892:
	sarq	$35, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11Translation19StoreInt64StackSlotEi@PLT
	jmp	.L756
.L894:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24309:
	.size	_ZN2v88internal8compiler13CodeGenerator24AddTranslationForOperandEPNS0_11TranslationEPNS1_11InstructionEPNS1_18InstructionOperandENS0_11MachineTypeE, .-_ZN2v88internal8compiler13CodeGenerator24AddTranslationForOperandEPNS0_11TranslationEPNS1_11InstructionEPNS1_18InstructionOperandENS0_11MachineTypeE
	.section	.text._ZN2v88internal8compiler13CodeGenerator29TranslateStateValueDescriptorEPNS1_20StateValueDescriptorEPNS1_14StateValueListEPNS0_11TranslationEPNS1_26InstructionOperandIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13CodeGenerator29TranslateStateValueDescriptorEPNS1_20StateValueDescriptorEPNS1_14StateValueListEPNS0_11TranslationEPNS1_26InstructionOperandIteratorE
	.type	_ZN2v88internal8compiler13CodeGenerator29TranslateStateValueDescriptorEPNS1_20StateValueDescriptorEPNS1_14StateValueListEPNS0_11TranslationEPNS1_26InstructionOperandIteratorE, @function
_ZN2v88internal8compiler13CodeGenerator29TranslateStateValueDescriptorEPNS1_20StateValueDescriptorEPNS1_14StateValueListEPNS0_11TranslationEPNS1_26InstructionOperandIteratorE:
.LFB24305:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movzbl	(%rsi), %eax
	cmpb	$4, %al
	je	.L935
	testb	%al, %al
	je	.L936
	cmpb	$1, %al
	je	.L937
	cmpb	$5, %al
	je	.L938
	cmpb	$2, %al
	je	.L939
	testq	%rcx, %rcx
	je	.L900
	movl	1148(%rdi), %esi
	cmpl	$-1, %esi
	je	.L940
.L913:
	leaq	-40(%rbp), %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11Translation12StoreLiteralEi@PLT
	.p2align 4,,10
	.p2align 3
.L936:
	.cfi_restore_state
	testq	%rcx, %rcx
	je	.L900
	movzbl	8(%rsi), %esi
	leaq	-40(%rbp), %rsp
	movq	%rcx, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11Translation17ArgumentsElementsENS0_19CreateArgumentsTypeE@PLT
	.p2align 4,,10
	.p2align 3
.L938:
	.cfi_restore_state
	testq	%rcx, %rcx
	jne	.L941
.L900:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L935:
	.cfi_restore_state
	movq	16(%rdx), %rax
	movq	8(%rdx), %rbx
	movq	%rdx, %r14
	movq	%rax, -88(%rbp)
	testq	%rcx, %rcx
	je	.L902
	subq	%rbx, %rax
	movq	%rcx, %rdi
	movq	%rax, %rsi
	sarq	$4, %rsi
	call	_ZN2v88internal11Translation19BeginCapturedObjectEi@PLT
	movq	16(%r14), %rax
	movq	8(%r14), %rbx
	movq	%rax, -88(%rbp)
.L902:
	cmpq	-88(%rbp), %rbx
	je	.L900
	movq	40(%r14), %r14
.L907:
	xorl	%edx, %edx
	cmpb	$4, (%rbx)
	jne	.L905
	movq	(%r14), %rdx
.L905:
	movq	%rbx, %rsi
	movq	%r13, %r8
	movq	%r12, %rcx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeGenerator29TranslateStateValueDescriptorEPNS1_20StateValueDescriptorEPNS1_14StateValueListEPNS0_11TranslationEPNS1_26InstructionOperandIteratorE
	cmpb	$4, (%rbx)
	leaq	8(%r14), %rdx
	cmove	%rdx, %r14
	addq	$16, %rbx
	cmpq	%rbx, -88(%rbp)
	jne	.L907
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L937:
	.cfi_restore_state
	testq	%rcx, %rcx
	je	.L900
	movzbl	8(%rsi), %esi
	leaq	-40(%rbp), %rsp
	movq	%rcx, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11Translation15ArgumentsLengthENS0_19CreateArgumentsTypeE@PLT
	.p2align 4,,10
	.p2align 3
.L941:
	.cfi_restore_state
	movq	8(%rsi), %rsi
	leaq	-40(%rbp), %rsp
	movq	%rcx, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11Translation15DuplicateObjectEi@PLT
	.p2align 4,,10
	.p2align 3
.L939:
	.cfi_restore_state
	movq	8(%r8), %rax
	movq	(%r8), %rdx
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%r8)
	testq	%r12, %r12
	je	.L900
	movzbl	4(%rdx), %ecx
	movzwl	1(%rsi), %r8d
	leaq	-40(%rbp), %rsp
	movq	%r12, %rsi
	popq	%rbx
	popq	%r12
	leaq	5(%rax,%rcx), %rax
	popq	%r13
	popq	%r14
	leaq	(%rdx,%rax,8), %rcx
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeGenerator24AddTranslationForOperandEPNS0_11TranslationEPNS1_11InstructionEPNS1_18InstructionOperandENS0_11MachineTypeE
.L940:
	.cfi_restore_state
	movq	16(%rdi), %rax
	movl	$0, -80(%rbp)
	movq	$0x000000000, -64(%rbp)
	addq	$328, %rax
	movq	$0, -56(%rbp)
	movq	%rax, -72(%rbp)
	pushq	-56(%rbp)
	pushq	-64(%rbp)
	pushq	-72(%rbp)
	pushq	-80(%rbp)
	call	_ZN2v88internal8compiler13CodeGenerator27DefineDeoptimizationLiteralENS1_21DeoptimizationLiteralE
	addq	$32, %rsp
	movl	%eax, 1148(%r15)
	movl	%eax, %esi
	jmp	.L913
	.cfi_endproc
.LFE24305:
	.size	_ZN2v88internal8compiler13CodeGenerator29TranslateStateValueDescriptorEPNS1_20StateValueDescriptorEPNS1_14StateValueListEPNS0_11TranslationEPNS1_26InstructionOperandIteratorE, .-_ZN2v88internal8compiler13CodeGenerator29TranslateStateValueDescriptorEPNS1_20StateValueDescriptorEPNS1_14StateValueListEPNS0_11TranslationEPNS1_26InstructionOperandIteratorE
	.section	.text._ZN2v88internal8compiler13CodeGenerator37TranslateFrameStateDescriptorOperandsEPNS1_20FrameStateDescriptorEPNS1_26InstructionOperandIteratorEPNS0_11TranslationE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13CodeGenerator37TranslateFrameStateDescriptorOperandsEPNS1_20FrameStateDescriptorEPNS1_26InstructionOperandIteratorEPNS0_11TranslationE
	.type	_ZN2v88internal8compiler13CodeGenerator37TranslateFrameStateDescriptorOperandsEPNS1_20FrameStateDescriptorEPNS1_26InstructionOperandIteratorEPNS0_11TranslationE, @function
_ZN2v88internal8compiler13CodeGenerator37TranslateFrameStateDescriptorOperandsEPNS1_20FrameStateDescriptorEPNS1_26InstructionOperandIteratorEPNS0_11TranslationE:
.LFB24306:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -96(%rbp)
	movq	56(%rax), %r15
	movq	%rdx, -120(%rbp)
	movq	88(%rsi), %rsi
	movq	64(%rax), %rdx
	movq	%rsi, -104(%rbp)
	cmpq	%rdx, %r15
	je	.L942
	movq	%rdi, %r13
	movq	%rcx, %r14
	jmp	.L957
	.p2align 4,,10
	.p2align 3
.L944:
	testb	%al, %al
	jne	.L986
	testq	%r14, %r14
	je	.L952
	movzbl	8(%r15), %esi
	movq	%r14, %rdi
	call	_ZN2v88internal11Translation17ArgumentsElementsENS0_19CreateArgumentsTypeE@PLT
.L947:
	movq	-96(%rbp), %rax
	cmpb	$4, (%r15)
	movq	64(%rax), %rdx
	je	.L961
.L952:
	addq	$16, %r15
	cmpq	%r15, %rdx
	je	.L942
.L957:
	movzbl	(%r15), %eax
	cmpb	$4, %al
	jne	.L944
	movq	-104(%rbp), %rax
	movq	(%rax), %rbx
	movq	16(%rbx), %rcx
	movq	8(%rbx), %rax
	movq	%rcx, -88(%rbp)
	testq	%r14, %r14
	je	.L945
	subq	%rax, %rcx
	movq	%r14, %rdi
	movq	%rcx, %rsi
	sarq	$4, %rsi
	call	_ZN2v88internal11Translation19BeginCapturedObjectEi@PLT
	movq	16(%rbx), %rdi
	movq	8(%rbx), %rax
	movq	%rdi, -88(%rbp)
	cmpq	%rdi, %rax
	je	.L947
.L984:
	movq	%r15, -112(%rbp)
	movq	40(%rbx), %rbx
	movq	%r13, %r15
	movq	%r14, %r13
	movq	-120(%rbp), %r14
	movq	%rax, %r12
.L950:
	xorl	%edx, %edx
	cmpb	$4, (%r12)
	jne	.L948
	movq	(%rbx), %rdx
.L948:
	movq	%r12, %rsi
	movq	%r14, %r8
	movq	%r13, %rcx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeGenerator29TranslateStateValueDescriptorEPNS1_20StateValueDescriptorEPNS1_14StateValueListEPNS0_11TranslationEPNS1_26InstructionOperandIteratorE
	cmpb	$4, (%r12)
	leaq	8(%rbx), %rax
	cmove	%rax, %rbx
	addq	$16, %r12
	cmpq	%r12, -88(%rbp)
	jne	.L950
	movq	%r13, %r14
	movq	%r15, %r13
	movq	-112(%rbp), %r15
	movq	-96(%rbp), %rax
	cmpb	$4, (%r15)
	movq	64(%rax), %rdx
	jne	.L952
.L961:
	addq	$8, -104(%rbp)
.L991:
	addq	$16, %r15
	cmpq	%r15, %rdx
	jne	.L957
.L942:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L986:
	.cfi_restore_state
	cmpb	$1, %al
	je	.L987
	cmpb	$5, %al
	je	.L988
	cmpb	$2, %al
	je	.L989
	testq	%r14, %r14
	je	.L952
	movl	1148(%r13), %esi
	cmpl	$-1, %esi
	je	.L990
	movq	%r14, %rdi
	call	_ZN2v88internal11Translation12StoreLiteralEi@PLT
	jmp	.L947
	.p2align 4,,10
	.p2align 3
.L945:
	cmpq	-88(%rbp), %rax
	jne	.L984
	addq	$8, -104(%rbp)
	jmp	.L991
	.p2align 4,,10
	.p2align 3
.L987:
	testq	%r14, %r14
	je	.L952
	movzbl	8(%r15), %esi
	movq	%r14, %rdi
	call	_ZN2v88internal11Translation15ArgumentsLengthENS0_19CreateArgumentsTypeE@PLT
	jmp	.L947
	.p2align 4,,10
	.p2align 3
.L988:
	testq	%r14, %r14
	je	.L952
	movl	8(%r15), %esi
	movq	%r14, %rdi
	call	_ZN2v88internal11Translation15DuplicateObjectEi@PLT
	jmp	.L947
	.p2align 4,,10
	.p2align 3
.L989:
	movq	-120(%rbp), %rdi
	movq	8(%rdi), %rax
	movq	(%rdi), %rdx
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%rdi)
	testq	%r14, %r14
	je	.L947
	movzbl	1(%r15), %esi
	movzbl	2(%r15), %ecx
	movq	%r13, %rdi
	movb	%sil, -122(%rbp)
	movzwl	-122(%rbp), %ebx
	movq	%r14, %rsi
	movb	%cl, %bh
	movzbl	4(%rdx), %ecx
	movl	%ebx, %r8d
	movw	%bx, -122(%rbp)
	leaq	5(%rax,%rcx), %rax
	leaq	(%rdx,%rax,8), %rcx
	call	_ZN2v88internal8compiler13CodeGenerator24AddTranslationForOperandEPNS0_11TranslationEPNS1_11InstructionEPNS1_18InstructionOperandENS0_11MachineTypeE
	jmp	.L947
	.p2align 4,,10
	.p2align 3
.L990:
	movq	16(%r13), %rax
	movl	$0, -80(%rbp)
	movq	%r13, %rdi
	movq	$0x000000000, -64(%rbp)
	addq	$328, %rax
	movq	$0, -56(%rbp)
	movq	%rax, -72(%rbp)
	pushq	-56(%rbp)
	pushq	-64(%rbp)
	pushq	-72(%rbp)
	pushq	-80(%rbp)
	call	_ZN2v88internal8compiler13CodeGenerator27DefineDeoptimizationLiteralENS1_21DeoptimizationLiteralE
	addq	$32, %rsp
	movq	%r14, %rdi
	movl	%eax, 1148(%r13)
	movl	%eax, %esi
	call	_ZN2v88internal11Translation12StoreLiteralEi@PLT
	jmp	.L947
	.cfi_endproc
.LFE24306:
	.size	_ZN2v88internal8compiler13CodeGenerator37TranslateFrameStateDescriptorOperandsEPNS1_20FrameStateDescriptorEPNS1_26InstructionOperandIteratorEPNS0_11TranslationE, .-_ZN2v88internal8compiler13CodeGenerator37TranslateFrameStateDescriptorOperandsEPNS1_20FrameStateDescriptorEPNS1_26InstructionOperandIteratorEPNS0_11TranslationE
	.section	.text._ZN2v88internal8compiler13CodeGenerator39BuildTranslationForFrameStateDescriptorEPNS1_20FrameStateDescriptorEPNS1_26InstructionOperandIteratorEPNS0_11TranslationENS1_23OutputFrameStateCombineE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13CodeGenerator39BuildTranslationForFrameStateDescriptorEPNS1_20FrameStateDescriptorEPNS1_26InstructionOperandIteratorEPNS0_11TranslationENS1_23OutputFrameStateCombineE
	.type	_ZN2v88internal8compiler13CodeGenerator39BuildTranslationForFrameStateDescriptorEPNS1_20FrameStateDescriptorEPNS1_26InstructionOperandIteratorEPNS0_11TranslationENS1_23OutputFrameStateCombineE, @function
_ZN2v88internal8compiler13CodeGenerator39BuildTranslationForFrameStateDescriptorEPNS1_20FrameStateDescriptorEPNS1_26InstructionOperandIteratorEPNS0_11TranslationENS1_23OutputFrameStateCombineE:
.LFB24307:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$88, %rsp
	movq	120(%rsi), %rsi
	movq	%rdx, -88(%rbp)
	testq	%rsi, %rsi
	je	.L993
	call	_ZN2v88internal8compiler13CodeGenerator39BuildTranslationForFrameStateDescriptorEPNS1_20FrameStateDescriptorEPNS1_26InstructionOperandIteratorEPNS0_11TranslationENS1_23OutputFrameStateCombineE
.L993:
	movq	112(%rbx), %rax
	testq	%rax, %rax
	je	.L1053
.L994:
	movl	4(%rbx), %r15d
	movq	%rax, -72(%rbp)
	movq	%r12, %rdi
	movl	$0, -80(%rbp)
	movq	$0x000000000, -64(%rbp)
	movq	$0, -56(%rbp)
	pushq	-56(%rbp)
	pushq	-64(%rbp)
	pushq	-72(%rbp)
	pushq	-80(%rbp)
	call	_ZN2v88internal8compiler13CodeGenerator27DefineDeoptimizationLiteralENS1_21DeoptimizationLiteralE
	addq	$32, %rsp
	movq	%rbx, %rdi
	movl	%eax, -96(%rbp)
	call	_ZNK2v88internal8compiler20FrameStateDescriptor9GetHeightEv@PLT
	movl	(%rbx), %r8d
	movq	%rax, %rcx
	cmpl	$5, %r8d
	ja	.L995
	leaq	.L997(%rip), %rax
	movl	%r8d, %r9d
	movl	-96(%rbp), %r11d
	movslq	(%rax,%r9,4), %rdi
	addq	%rax, %rdi
	notrack jmp	*%rdi
	.section	.rodata._ZN2v88internal8compiler13CodeGenerator39BuildTranslationForFrameStateDescriptorEPNS1_20FrameStateDescriptorEPNS1_26InstructionOperandIteratorEPNS0_11TranslationENS1_23OutputFrameStateCombineE,"a",@progbits
	.align 4
	.align 4
.L997:
	.long	.L1002-.L997
	.long	.L1001-.L997
	.long	.L1000-.L997
	.long	.L999-.L997
	.long	.L998-.L997
	.long	.L996-.L997
	.section	.text._ZN2v88internal8compiler13CodeGenerator39BuildTranslationForFrameStateDescriptorEPNS1_20FrameStateDescriptorEPNS1_26InstructionOperandIteratorEPNS0_11TranslationENS1_23OutputFrameStateCombineE
	.p2align 4,,10
	.p2align 3
.L1002:
	movl	%r8d, %r9d
	cmpq	$-1, %r14
	je	.L1003
	movq	-88(%rbp), %rax
	movl	%r14d, %r8d
	movq	(%rax), %rax
	movzbl	4(%rax), %r9d
.L1003:
	movl	%r11d, %edx
	movl	%r15d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal11Translation21BeginInterpretedFrameENS0_9BailoutIdEijii@PLT
	.p2align 4,,10
	.p2align 3
.L995:
	movq	64(%rbx), %rcx
	movq	56(%rbx), %r15
	movq	88(%rbx), %r14
	cmpq	%rcx, %r15
	je	.L992
	movq	%rbx, -112(%rbp)
	jmp	.L1019
	.p2align 4,,10
	.p2align 3
.L1006:
	testb	%al, %al
	jne	.L1054
	testq	%r13, %r13
	je	.L1014
	movzbl	8(%r15), %esi
	movq	%r13, %rdi
	call	_ZN2v88internal11Translation17ArgumentsElementsENS0_19CreateArgumentsTypeE@PLT
.L1009:
	movq	-112(%rbp), %rax
	cmpb	$4, (%r15)
	movq	64(%rax), %rcx
	je	.L1023
.L1014:
	addq	$16, %r15
	cmpq	%r15, %rcx
	je	.L992
.L1019:
	movzbl	(%r15), %eax
	cmpb	$4, %al
	jne	.L1006
	movq	(%r14), %rbx
	movq	8(%rbx), %rsi
	movq	16(%rbx), %rax
	testq	%r13, %r13
	je	.L1007
	subq	%rsi, %rax
	movq	%r13, %rdi
	movq	%rax, %rsi
	sarq	$4, %rsi
	call	_ZN2v88internal11Translation19BeginCapturedObjectEi@PLT
	movq	8(%rbx), %rsi
	movq	16(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L1009
.L1051:
	movq	40(%rbx), %rbx
	movq	%rax, -96(%rbp)
	movq	%r15, -104(%rbp)
	movq	%r13, %r15
	movq	%r14, %r13
	movq	%r12, %r14
	movq	%rbx, %r12
	movq	%rsi, %rbx
.L1012:
	xorl	%edx, %edx
	cmpb	$4, (%rbx)
	jne	.L1010
	movq	(%r12), %rdx
.L1010:
	movq	-88(%rbp), %r8
	movq	%rbx, %rsi
	movq	%r15, %rcx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeGenerator29TranslateStateValueDescriptorEPNS1_20StateValueDescriptorEPNS1_14StateValueListEPNS0_11TranslationEPNS1_26InstructionOperandIteratorE
	cmpb	$4, (%rbx)
	leaq	8(%r12), %rdx
	cmove	%rdx, %r12
	addq	$16, %rbx
	cmpq	%rbx, -96(%rbp)
	jne	.L1012
	movq	%r14, %r12
	movq	%r13, %r14
	movq	%r15, %r13
	movq	-104(%rbp), %r15
	movq	-112(%rbp), %rax
	cmpb	$4, (%r15)
	movq	64(%rax), %rcx
	jne	.L1014
.L1023:
	addq	$8, %r14
.L1055:
	addq	$16, %r15
	cmpq	%r15, %rcx
	jne	.L1019
.L992:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1007:
	.cfi_restore_state
	cmpq	%rax, %rsi
	jne	.L1051
	addq	$8, %r14
	jmp	.L1055
	.p2align 4,,10
	.p2align 3
.L1054:
	cmpb	$1, %al
	je	.L1056
	cmpb	$5, %al
	je	.L1057
	cmpb	$2, %al
	je	.L1058
	testq	%r13, %r13
	je	.L1014
	movl	1148(%r12), %esi
	cmpl	$-1, %esi
	je	.L1059
	movq	%r13, %rdi
	call	_ZN2v88internal11Translation12StoreLiteralEi@PLT
	jmp	.L1009
	.p2align 4,,10
	.p2align 3
.L1056:
	testq	%r13, %r13
	je	.L1014
	movzbl	8(%r15), %esi
	movq	%r13, %rdi
	call	_ZN2v88internal11Translation15ArgumentsLengthENS0_19CreateArgumentsTypeE@PLT
	jmp	.L1009
	.p2align 4,,10
	.p2align 3
.L1057:
	testq	%r13, %r13
	je	.L1014
	movl	8(%r15), %esi
	movq	%r13, %rdi
	call	_ZN2v88internal11Translation15DuplicateObjectEi@PLT
	jmp	.L1009
	.p2align 4,,10
	.p2align 3
.L1058:
	movq	-88(%rbp), %rdi
	movq	8(%rdi), %rax
	movq	(%rdi), %rdx
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%rdi)
	testq	%r13, %r13
	je	.L1009
	movzbl	1(%r15), %edi
	movzbl	2(%r15), %ecx
	movq	%r13, %rsi
	movb	%dil, -114(%rbp)
	movzwl	-114(%rbp), %ebx
	movq	%r12, %rdi
	movb	%cl, %bh
	movzbl	4(%rdx), %ecx
	movl	%ebx, %r8d
	movw	%bx, -114(%rbp)
	leaq	5(%rax,%rcx), %rax
	leaq	(%rdx,%rax,8), %rcx
	call	_ZN2v88internal8compiler13CodeGenerator24AddTranslationForOperandEPNS0_11TranslationEPNS1_11InstructionEPNS1_18InstructionOperandENS0_11MachineTypeE
	jmp	.L1009
	.p2align 4,,10
	.p2align 3
.L1001:
	movl	%ecx, %edx
	movl	%r11d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal11Translation26BeginArgumentsAdaptorFrameEij@PLT
	jmp	.L995
	.p2align 4,,10
	.p2align 3
.L1000:
	movl	%r11d, %edx
	movl	%r15d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal11Translation23BeginConstructStubFrameENS0_9BailoutIdEij@PLT
	jmp	.L995
	.p2align 4,,10
	.p2align 3
.L999:
	movl	%r11d, %edx
	movl	%r15d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal11Translation29BeginBuiltinContinuationFrameENS0_9BailoutIdEij@PLT
	jmp	.L995
	.p2align 4,,10
	.p2align 3
.L998:
	movl	%r11d, %edx
	movl	%r15d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal11Translation39BeginJavaScriptBuiltinContinuationFrameENS0_9BailoutIdEij@PLT
	jmp	.L995
	.p2align 4,,10
	.p2align 3
.L996:
	movl	%r11d, %edx
	movl	%r15d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal11Translation48BeginJavaScriptBuiltinContinuationWithCatchFrameENS0_9BailoutIdEij@PLT
	jmp	.L995
	.p2align 4,,10
	.p2align 3
.L1059:
	movq	16(%r12), %rax
	movl	$0, -80(%rbp)
	movq	%r12, %rdi
	movq	$0x000000000, -64(%rbp)
	addq	$328, %rax
	movq	$0, -56(%rbp)
	movq	%rax, -72(%rbp)
	pushq	-56(%rbp)
	pushq	-64(%rbp)
	pushq	-72(%rbp)
	pushq	-80(%rbp)
	call	_ZN2v88internal8compiler13CodeGenerator27DefineDeoptimizationLiteralENS1_21DeoptimizationLiteralE
	addq	$32, %rsp
	movq	%r13, %rdi
	movl	%eax, 1148(%r12)
	movl	%eax, %esi
	call	_ZN2v88internal11Translation12StoreLiteralEi@PLT
	jmp	.L1009
	.p2align 4,,10
	.p2align 3
.L1053:
	movq	152(%r12), %rax
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L994
	jmp	.L992
	.cfi_endproc
.LFE24307:
	.size	_ZN2v88internal8compiler13CodeGenerator39BuildTranslationForFrameStateDescriptorEPNS1_20FrameStateDescriptorEPNS1_26InstructionOperandIteratorEPNS0_11TranslationENS1_23OutputFrameStateCombineE, .-_ZN2v88internal8compiler13CodeGenerator39BuildTranslationForFrameStateDescriptorEPNS1_20FrameStateDescriptorEPNS1_26InstructionOperandIteratorEPNS0_11TranslationENS1_23OutputFrameStateCombineE
	.section	.text._ZNSt5dequeIPN2v88internal8compiler18DeoptimizationExitENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_,"axG",@progbits,_ZNSt5dequeIPN2v88internal8compiler18DeoptimizationExitENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt5dequeIPN2v88internal8compiler18DeoptimizationExitENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	.type	_ZNSt5dequeIPN2v88internal8compiler18DeoptimizationExitENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_, @function
_ZNSt5dequeIPN2v88internal8compiler18DeoptimizationExitENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_:
.LFB28256:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	88(%rdi), %r13
	movq	56(%rdi), %rsi
	movq	64(%rbx), %rdx
	subq	72(%rbx), %rdx
	movq	%r13, %r14
	movq	%rdx, %rcx
	subq	%rsi, %r14
	sarq	$3, %rcx
	movq	%r14, %rdi
	sarq	$3, %rdi
	leaq	-1(%rdi), %rax
	salq	$6, %rax
	leaq	(%rcx,%rax), %rdx
	movq	48(%rbx), %rax
	subq	32(%rbx), %rax
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpq	$268435455, %rax
	je	.L1078
	movq	16(%rbx), %rcx
	movq	24(%rbx), %rdx
	movq	%r13, %rax
	subq	%rcx, %rax
	movq	%rdx, %r8
	sarq	$3, %rax
	subq	%rax, %r8
	cmpq	$1, %r8
	jbe	.L1079
.L1062:
	movq	8(%rbx), %rax
	testq	%rax, %rax
	je	.L1070
	cmpq	$63, 8(%rax)
	ja	.L1080
.L1070:
	movq	(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L1081
	leaq	512(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L1071:
	movq	%rax, 8(%r13)
	movq	(%r12), %rdx
	movq	64(%rbx), %rax
	movq	%rdx, (%rax)
	movq	88(%rbx), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 88(%rbx)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 72(%rbx)
	movq	%rdx, 80(%rbx)
	movq	%rax, 64(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1079:
	.cfi_restore_state
	leaq	2(%rdi), %r15
	leaq	(%r15,%r15), %rax
	cmpq	%rax, %rdx
	ja	.L1082
	testq	%rdx, %rdx
	movl	$1, %eax
	movq	(%rbx), %rdi
	cmovne	%rdx, %rax
	movq	16(%rdi), %r13
	leaq	2(%rdx,%rax), %rcx
	movq	24(%rdi), %rax
	leaq	0(,%rcx,8), %rsi
	subq	%r13, %rax
	cmpq	%rax, %rsi
	ja	.L1083
	addq	%r13, %rsi
	movq	%rsi, 16(%rdi)
.L1067:
	movq	%rcx, %rax
	movq	56(%rbx), %rsi
	subq	%r15, %rax
	shrq	%rax
	leaq	0(%r13,%rax,8), %r15
	movq	88(%rbx), %rax
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L1068
	subq	%rsi, %rdx
	movq	%r15, %rdi
	movq	%rcx, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %rcx
.L1068:
	movq	24(%rbx), %rax
	leaq	0(,%rax,8), %rdx
	cmpq	$15, %rdx
	jbe	.L1069
	movq	16(%rbx), %rdx
	movq	%rax, 8(%rdx)
	movq	$0, (%rdx)
.L1069:
	movq	%r13, 16(%rbx)
	movq	%rcx, 24(%rbx)
.L1065:
	movq	%r15, 56(%rbx)
	movq	(%r15), %rax
	leaq	(%r15,%r14), %r13
	movq	(%r15), %xmm0
	movq	%r13, 88(%rbx)
	addq	$512, %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 40(%rbx)
	movq	0(%r13), %rax
	movq	%rax, 72(%rbx)
	addq	$512, %rax
	movq	%rax, 80(%rbx)
	jmp	.L1062
	.p2align 4,,10
	.p2align 3
.L1080:
	movq	(%rax), %rdx
	movq	%rdx, 8(%rbx)
	jmp	.L1071
	.p2align 4,,10
	.p2align 3
.L1082:
	subq	%r15, %rdx
	addq	$8, %r13
	shrq	%rdx
	leaq	(%rcx,%rdx,8), %r15
	movq	%r13, %rdx
	subq	%rsi, %rdx
	cmpq	%r15, %rsi
	jbe	.L1064
	cmpq	%r13, %rsi
	je	.L1065
	movq	%r15, %rdi
	call	memmove@PLT
	jmp	.L1065
	.p2align 4,,10
	.p2align 3
.L1064:
	cmpq	%r13, %rsi
	je	.L1065
	leaq	8(%r14), %rdi
	subq	%rdx, %rdi
	addq	%r15, %rdi
	call	memmove@PLT
	jmp	.L1065
	.p2align 4,,10
	.p2align 3
.L1081:
	movl	$512, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1071
	.p2align 4,,10
	.p2align 3
.L1083:
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	movq	%rax, %r13
	jmp	.L1067
.L1078:
	leaq	.LC16(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE28256:
	.size	_ZNSt5dequeIPN2v88internal8compiler18DeoptimizationExitENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_, .-_ZNSt5dequeIPN2v88internal8compiler18DeoptimizationExitENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	.section	.text._ZN2v88internal8compiler13CodeGenerator16BuildTranslationEPNS1_11InstructionEimNS1_23OutputFrameStateCombineE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13CodeGenerator16BuildTranslationEPNS1_11InstructionEimNS1_23OutputFrameStateCombineE
	.type	_ZN2v88internal8compiler13CodeGenerator16BuildTranslationEPNS1_11InstructionEimNS1_23OutputFrameStateCombineE, @function
_ZN2v88internal8compiler13CodeGenerator16BuildTranslationEPNS1_11InstructionEimNS1_23OutputFrameStateCombineE:
.LFB24308:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$168, %rsp
	movq	%r8, -184(%rbp)
	movq	40(%rdi), %rdi
	movl	%edx, -172(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	4(%rsi), %eax
	leaq	5(%rcx,%rax), %rax
	movq	(%rsi,%rax,8), %rsi
	movl	%esi, %eax
	movq	%rsi, %r8
	andl	$7, %eax
	shrq	$3, %r8
	cmpl	$3, %eax
	je	.L1112
	movq	112(%rdi), %rax
	movl	%r8d, %esi
	leaq	104(%rdi), %rdx
	testq	%rax, %rax
	je	.L1089
	movq	%rdx, %rcx
	jmp	.L1090
	.p2align 4,,10
	.p2align 3
.L1113:
	movq	%rax, %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L1091
.L1090:
	cmpl	32(%rax), %esi
	jle	.L1113
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L1090
.L1091:
	cmpq	%rcx, %rdx
	je	.L1089
	cmpl	%r8d, 32(%rcx)
	cmovle	%rcx, %rdx
.L1089:
	leaq	-96(%rbp), %rax
	movq	48(%rdx), %rsi
	movq	%rax, -200(%rbp)
.L1088:
	call	_ZN2v88internal8compiler19InstructionSequence22GetDeoptimizationEntryEi@PLT
	movq	%rax, %r14
	movq	(%rax), %r12
	leaq	1(%r13), %rax
	xorl	%r13d, %r13d
	cmpq	$0, 16(%r14)
	movq	%rax, -168(%rbp)
	je	.L1094
	xorl	%r13d, %r13d
	cmpl	$-1, 24(%r14)
	setne	%r13b
.L1094:
	movq	8(%r15), %rdx
	movq	%r12, %rdi
	movq	%rdx, -192(%rbp)
	call	_ZNK2v88internal8compiler20FrameStateDescriptor15GetJSFrameCountEv@PLT
	movq	%r12, %rdi
	movq	%rax, -152(%rbp)
	call	_ZNK2v88internal8compiler20FrameStateDescriptor13GetFrameCountEv@PLT
	movq	-192(%rbp), %rdx
	leaq	1048(%r15), %rdi
	xorl	%esi, %esi
	movq	%rax, -160(%rbp)
	movq	1056(%r15), %rax
	movq	%rdx, -112(%rbp)
	movl	%eax, -120(%rbp)
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal17TranslationBuffer3AddEi@PLT
	movl	-160(%rbp), %esi
	movq	-128(%rbp), %rdi
	call	_ZN2v88internal17TranslationBuffer3AddEi@PLT
	movl	-152(%rbp), %esi
	movq	-128(%rbp), %rdi
	call	_ZN2v88internal17TranslationBuffer3AddEi@PLT
	movq	-128(%rbp), %rdi
	movl	%r13d, %esi
	leaq	-128(%rbp), %r13
	call	_ZN2v88internal17TranslationBuffer3AddEi@PLT
	movq	16(%r14), %rax
	testq	%rax, %rax
	je	.L1095
	cmpl	$-1, 24(%r14)
	je	.L1095
	movl	$0, -96(%rbp)
	movq	%r15, %rdi
	movq	%rax, -88(%rbp)
	movq	$0x000000000, -80(%rbp)
	movq	$0, -72(%rbp)
	pushq	-72(%rbp)
	pushq	-80(%rbp)
	pushq	-88(%rbp)
	pushq	-96(%rbp)
	call	_ZN2v88internal8compiler13CodeGenerator27DefineDeoptimizationLiteralENS1_21DeoptimizationLiteralE
	movl	24(%r14), %edx
	addq	$32, %rsp
	movq	%r13, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal11Translation17AddUpdateFeedbackEii@PLT
.L1095:
	movq	-168(%rbp), %rax
	movq	%r13, %rcx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	-200(%rbp), %rdx
	movq	-184(%rbp), %r8
	movq	%rbx, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal8compiler13CodeGenerator39BuildTranslationForFrameStateDescriptorEPNS1_20FrameStateDescriptorEPNS1_26InstructionOperandIteratorEPNS0_11TranslationENS1_23OutputFrameStateCombineE
	movq	8(%r15), %rdi
	movl	4(%r12), %ecx
	movq	192(%r15), %r13
	movl	-120(%rbp), %r12d
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	movzwl	8(%r14), %ebx
	subq	%rax, %rdx
	cmpq	$39, %rdx
	jbe	.L1114
	leaq	40(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L1097:
	movl	%ecx, 24(%rax)
	movl	-172(%rbp), %ecx
	cmpb	$0, _ZN2v88internal11Deoptimizer27kSupportsFixedDeoptExitSizeE(%rip)
	movl	$32768, (%rax)
	movq	%r13, 8(%rax)
	movq	$0, 16(%rax)
	movl	%r12d, 28(%rax)
	movl	%ecx, 32(%rax)
	movw	%bx, 36(%rax)
	movb	$0, 38(%rax)
	movq	%rax, -136(%rbp)
	jne	.L1098
	movl	840(%r15), %edx
	leal	1(%rdx), %ecx
	movl	%ecx, 840(%r15)
	movl	%edx, (%rax)
.L1098:
	movq	928(%r15), %rax
	movq	912(%r15), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L1099
	movq	-136(%rbp), %rax
	movq	%rax, (%rdx)
	addq	$8, 912(%r15)
.L1100:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	movq	-136(%rbp), %rax
	jne	.L1115
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1112:
	.cfi_restore_state
	sarq	$32, %rsi
	andl	$1, %r8d
	je	.L1116
	salq	$4, %rsi
	leaq	-96(%rbp), %rax
	addq	152(%rdi), %rsi
	movq	%rax, -200(%rbp)
	movq	8(%rsi), %rsi
	jmp	.L1088
	.p2align 4,,10
	.p2align 3
.L1116:
	leaq	-96(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -200(%rbp)
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movq	-88(%rbp), %rsi
	movq	40(%r15), %rdi
	jmp	.L1088
	.p2align 4,,10
	.p2align 3
.L1099:
	leaq	-136(%rbp), %rsi
	leaq	848(%r15), %rdi
	call	_ZNSt5dequeIPN2v88internal8compiler18DeoptimizationExitENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	jmp	.L1100
	.p2align 4,,10
	.p2align 3
.L1114:
	movl	$40, %esi
	movl	%ecx, -152(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-152(%rbp), %ecx
	jmp	.L1097
.L1115:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24308:
	.size	_ZN2v88internal8compiler13CodeGenerator16BuildTranslationEPNS1_11InstructionEimNS1_23OutputFrameStateCombineE, .-_ZN2v88internal8compiler13CodeGenerator16BuildTranslationEPNS1_11InstructionEimNS1_23OutputFrameStateCombineE
	.section	.text._ZN2v88internal8compiler13CodeGenerator21AddDeoptimizationExitEPNS1_11InstructionEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13CodeGenerator21AddDeoptimizationExitEPNS1_11InstructionEm
	.type	_ZN2v88internal8compiler13CodeGenerator21AddDeoptimizationExitEPNS1_11InstructionEm, @function
_ZN2v88internal8compiler13CodeGenerator21AddDeoptimizationExitEPNS1_11InstructionEm:
.LFB24311:
	.cfi_startproc
	endbr64
	movq	%rdx, %rcx
	movq	$-1, %r8
	movl	$-1, %edx
	jmp	_ZN2v88internal8compiler13CodeGenerator16BuildTranslationEPNS1_11InstructionEimNS1_23OutputFrameStateCombineE
	.cfi_endproc
.LFE24311:
	.size	_ZN2v88internal8compiler13CodeGenerator21AddDeoptimizationExitEPNS1_11InstructionEm, .-_ZN2v88internal8compiler13CodeGenerator21AddDeoptimizationExitEPNS1_11InstructionEm
	.section	.text._ZN2v88internal8compiler13CodeGenerator19AssembleInstructionEiPKNS1_16InstructionBlockE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13CodeGenerator19AssembleInstructionEiPKNS1_16InstructionBlockE
	.type	_ZN2v88internal8compiler13CodeGenerator19AssembleInstructionEiPKNS1_16InstructionBlockE, @function
_ZN2v88internal8compiler13CodeGenerator19AssembleInstructionEiPKNS1_16InstructionBlockE:
.LFB24289:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movslq	%esi, %rbx
	subq	$88, %rsp
	movq	%rdx, -120(%rbp)
	movq	40(%rdi), %rdx
	movq	208(%rdx), %rcx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rcx, %rax
	subq	216(%rdx), %rax
	sarq	$3, %rax
	addq	%rbx, %rax
	js	.L1119
	cmpq	$63, %rax
	jle	.L1193
	movq	%rax, %rcx
	sarq	$6, %rcx
.L1122:
	movq	232(%rdx), %rdx
	movq	%rcx, %rsi
	salq	$6, %rsi
	movq	(%rdx,%rcx,8), %rdx
	subq	%rsi, %rax
	leaq	(%rdx,%rax,8), %rax
.L1121:
	movq	(%rax), %r15
	movq	152(%r12), %rax
	movl	(%rax), %eax
	testb	$64, %ah
	je	.L1123
	movq	1320(%r12), %rsi
	leaq	(%rbx,%rbx,2), %rax
	movq	232(%r12), %rdx
	subq	216(%r12), %rdx
	leaq	(%rsi,%rax,4), %rax
	movl	%edx, (%rax)
.L1123:
	movl	(%r15), %eax
	movl	%eax, %r13d
	shrl	$14, %r13d
	andl	$7, %r13d
	cmpl	$6, %r13d
	je	.L1124
	movq	$0, -96(%rbp)
	movl	(%r15), %eax
	andl	$511, %eax
	cmpl	$17, %eax
	je	.L1125
.L1128:
	movq	40(%r12), %rdi
	leaq	-96(%rbp), %rdx
	movq	%r15, %rsi
	call	_ZNK2v88internal8compiler19InstructionSequence17GetSourcePositionEPKNS1_11InstructionEPNS0_14SourcePositionE@PLT
	testb	%al, %al
	je	.L1127
	movq	-96(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeGenerator22AssembleSourcePositionENS0_14SourcePositionE
	movl	(%r15), %eax
.L1124:
	andl	$511, %eax
	xorl	%r14d, %r14d
	cmpl	$11, %eax
	ja	.L1129
	movl	$2070, %ecx
	btq	%rax, %rcx
	jnc	.L1129
	movl	4(%r15), %eax
	movl	%eax, %edx
	movzbl	%al, %eax
	shrl	$8, %edx
	movzwl	%dx, %edx
	leaq	4(%rdx,%rax), %rax
	movq	(%r15,%rax,8), %rsi
	movq	40(%r12), %rax
	movl	%esi, %edx
	movq	%rsi, %rdi
	andl	$7, %edx
	shrq	$3, %rdi
	cmpl	$3, %edx
	je	.L1194
	leaq	104(%rax), %rsi
	movq	112(%rax), %rax
	movl	%edi, %ecx
	testq	%rax, %rax
	je	.L1134
	movq	%rsi, %rdx
	jmp	.L1135
	.p2align 4,,10
	.p2align 3
.L1195:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L1136
.L1135:
	cmpl	32(%rax), %ecx
	jle	.L1195
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L1135
.L1136:
	cmpq	%rdx, %rsi
	je	.L1134
	cmpl	%edi, 32(%rdx)
	cmovle	%rdx, %rsi
.L1134:
	movq	48(%rsi), %rdx
.L1133:
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%edx, -124(%rbp)
	movl	$1, %r14d
	call	_ZN2v88internal8compiler13CodeGenerator25AssembleTailCallBeforeGapEPNS1_11InstructionEi@PLT
.L1129:
	movq	8(%r15), %rsi
	testq	%rsi, %rsi
	je	.L1139
	leaq	744(%r12), %rdi
	call	_ZN2v88internal8compiler11GapResolver7ResolveEPNS1_12ParallelMoveE@PLT
.L1139:
	movq	16(%r15), %rsi
	testq	%rsi, %rsi
	je	.L1140
	leaq	744(%r12), %rdi
	call	_ZN2v88internal8compiler11GapResolver7ResolveEPNS1_12ParallelMoveE@PLT
.L1140:
	testb	%r14b, %r14b
	jne	.L1196
.L1141:
	movl	(%r15), %eax
	andl	$511, %eax
	cmpl	$13, %eax
	je	.L1197
.L1142:
	movq	152(%r12), %rax
	movl	(%rax), %eax
	testb	$64, %ah
	je	.L1143
	movq	1320(%r12), %rcx
	leaq	(%rbx,%rbx,2), %rax
	movq	232(%r12), %rdx
	subq	216(%r12), %rdx
	leaq	(%rcx,%rax,4), %rax
	movl	%edx, 4(%rax)
.L1143:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeGenerator23AssembleArchInstructionEPNS1_11InstructionE@PLT
	movl	%eax, %r14d
	testl	%eax, %eax
	jne	.L1118
	movq	152(%r12), %rax
	movl	(%rax), %eax
	testb	$64, %ah
	je	.L1145
	movq	1320(%r12), %rdx
	leaq	(%rbx,%rbx,2), %rax
	leaq	(%rdx,%rax,4), %rax
	movq	232(%r12), %rdx
	subq	216(%r12), %rdx
	movl	%edx, 8(%rax)
.L1145:
	movl	(%r15), %ecx
	movl	%ecx, %eax
	shrl	$17, %eax
	andl	$31, %eax
	movl	%eax, %ebx
	cmpl	$5, %r13d
	je	.L1146
	ja	.L1147
	cmpl	$2, %r13d
	ja	.L1148
	testl	%r13d, %r13d
	jne	.L1198
.L1150:
	testb	$64, 7(%r15)
	je	.L1162
.L1201:
	cmpl	$1, 1244(%r12)
	jne	.L1199
	.p2align 4,,10
	.p2align 3
.L1118:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1200
	addq	$88, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1193:
	.cfi_restore_state
	leaq	(%rcx,%rbx,8), %rax
	jmp	.L1121
	.p2align 4,,10
	.p2align 3
.L1148:
	cmpl	$5, %r13d
	je	.L1150
	shrl	$22, %ecx
	movq	$-1, %r8
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$-1, %edx
	call	_ZN2v88internal8compiler13CodeGenerator16BuildTranslationEPNS1_11InstructionEimNS1_23OutputFrameStateCombineE
	movl	%ebx, -96(%rbp)
	movq	%r15, %rsi
	leaq	-104(%rbp), %rbx
	addq	$16, %rax
	movq	%r12, %rdi
	leaq	-96(%rbp), %rdx
	movq	%rbx, -80(%rbp)
	movq	$0, -104(%rbp)
	movq	%rax, -88(%rbp)
	movb	$1, -72(%rbp)
	call	_ZN2v88internal8compiler13CodeGenerator23AssembleArchDeoptBranchEPNS1_11InstructionEPNS1_10BranchInfoE@PLT
	leaq	200(%r12), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	cmpl	$4, %r13d
	jne	.L1150
	movl	-96(%rbp), %esi
	movq	%r15, %rdx
	movq	%r12, %rdi
	xorl	$1, %esi
	call	_ZN2v88internal8compiler13CodeGenerator23AssembleBranchPoisoningENS1_14FlagsConditionEPNS1_11InstructionE@PLT
	jmp	.L1150
	.p2align 4,,10
	.p2align 3
.L1147:
	cmpl	$6, %r13d
	jne	.L1150
	movl	%eax, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeGenerator16AssembleArchTrapEPNS1_11InstructionENS1_14FlagsConditionE@PLT
	testb	$64, 7(%r15)
	jne	.L1201
.L1162:
	xorl	%r14d, %r14d
	jmp	.L1118
	.p2align 4,,10
	.p2align 3
.L1197:
	movq	-120(%rbp), %rax
	cmpb	$0, 126(%rax)
	je	.L1142
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeGenerator24AssembleDeconstructFrameEv@PLT
	jmp	.L1142
	.p2align 4,,10
	.p2align 3
.L1196:
	movl	-124(%rbp), %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeGenerator24AssembleTailCallAfterGapEPNS1_11InstructionEi@PLT
	jmp	.L1141
	.p2align 4,,10
	.p2align 3
.L1119:
	movq	%rax, %rcx
	notq	%rcx
	shrq	$6, %rcx
	notq	%rcx
	jmp	.L1122
	.p2align 4,,10
	.p2align 3
.L1125:
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler11Instruction17AreMovesRedundantEv@PLT
	testb	%al, %al
	je	.L1128
.L1127:
	movl	(%r15), %eax
	jmp	.L1124
	.p2align 4,,10
	.p2align 3
.L1199:
	leaq	200(%r12), %rdi
	call	_ZN2v88internal14TurboAssembler30ResetSpeculationPoisonRegisterEv@PLT
	jmp	.L1118
	.p2align 4,,10
	.p2align 3
.L1194:
	sarq	$32, %rsi
	andl	$1, %edi
	jne	.L1131
	leaq	-96(%rbp), %rdi
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movq	-88(%rbp), %rdx
	jmp	.L1133
	.p2align 4,,10
	.p2align 3
.L1146:
	movl	%eax, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeGenerator19AssembleArchBooleanEPNS1_11InstructionENS1_14FlagsConditionE@PLT
	jmp	.L1150
	.p2align 4,,10
	.p2align 3
.L1198:
	leaq	-96(%rbp), %r13
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler13CodeGenerator17ComputeBranchInfoEPNS1_10BranchInfoEPNS1_11InstructionE
	movl	%eax, %esi
	testl	%eax, %eax
	jns	.L1202
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeGenerator18AssembleArchBranchEPNS1_11InstructionEPNS1_10BranchInfoE@PLT
	jmp	.L1150
	.p2align 4,,10
	.p2align 3
.L1131:
	salq	$4, %rsi
	addq	152(%rax), %rsi
	movq	8(%rsi), %rdx
	jmp	.L1133
	.p2align 4,,10
	.p2align 3
.L1202:
	movq	40(%r12), %rax
	movslq	176(%r12), %r8
	movq	16(%rax), %rax
	movq	8(%rax), %rcx
	movq	16(%rax), %rdx
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %r8
	jnb	.L1192
	movq	(%rcx,%r8,8), %rax
	movslq	%esi, %r8
	movl	96(%rax), %eax
	cmpq	%rdx, %r8
	jnb	.L1192
	movq	(%rcx,%r8,8), %rdx
	addl	$1, %eax
	cmpl	%eax, 96(%rdx)
	je	.L1118
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeGenerator16AssembleArchJumpENS1_9RpoNumberE@PLT
	jmp	.L1118
.L1200:
	call	__stack_chk_fail@PLT
.L1192:
	movq	%r8, %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE24289:
	.size	_ZN2v88internal8compiler13CodeGenerator19AssembleInstructionEiPKNS1_16InstructionBlockE, .-_ZN2v88internal8compiler13CodeGenerator19AssembleInstructionEiPKNS1_16InstructionBlockE
	.section	.text._ZN2v88internal8compiler13CodeGenerator13AssembleBlockEPKNS1_16InstructionBlockE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13CodeGenerator13AssembleBlockEPKNS1_16InstructionBlockE
	.type	_ZN2v88internal8compiler13CodeGenerator13AssembleBlockEPKNS1_16InstructionBlockE, @function
_ZN2v88internal8compiler13CodeGenerator13AssembleBlockEPKNS1_16InstructionBlockE:
.LFB24274:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	112(%rsi), %ebx
	cmpl	116(%rsi), %ebx
	jge	.L1204
	movq	%rdi, %r13
	movq	%rsi, %r12
	jmp	.L1206
	.p2align 4,,10
	.p2align 3
.L1209:
	addl	$1, %ebx
	cmpl	116(%r12), %ebx
	jge	.L1204
.L1206:
	movq	%r12, %rdx
	movl	%ebx, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeGenerator19AssembleInstructionEiPKNS1_16InstructionBlockE
	testl	%eax, %eax
	je	.L1209
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1204:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24274:
	.size	_ZN2v88internal8compiler13CodeGenerator13AssembleBlockEPKNS1_16InstructionBlockE, .-_ZN2v88internal8compiler13CodeGenerator13AssembleBlockEPKNS1_16InstructionBlockE
	.section	.rodata._ZN2v88internal8compiler13CodeGenerator12AssembleCodeEv.str1.1,"aMS",@progbits,1
.LC21:
	.string	"-- B"
.LC22:
	.string	" start"
.LC23:
	.string	" (deferred)"
.LC24:
	.string	" (no frame)"
.LC25:
	.string	" (construct frame)"
.LC26:
	.string	" (deconstruct frame)"
.LC27:
	.string	" (loop up to "
.LC28:
	.string	")"
.LC29:
	.string	" (in loop "
	.section	.text._ZN2v88internal8compiler13CodeGenerator12AssembleCodeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13CodeGenerator12AssembleCodeEv
	.type	_ZN2v88internal8compiler13CodeGenerator12AssembleCodeEv, @function
_ZN2v88internal8compiler13CodeGenerator12AssembleCodeEv:
.LFB24253:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	200(%rdi), %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$600, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movzbl	736(%rdi), %ebx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	152(%rdi), %rax
	movb	$1, 736(%rdi)
	movb	%bl, -601(%rbp)
	movq	%rax, -544(%rbp)
	testb	$32, (%rax)
	jne	.L1365
.L1211:
	movq	232(%r15), %rdx
	subq	216(%r15), %rdx
	cmpb	$0, _ZN2v88internal15FLAG_debug_codeE(%rip)
	movl	%edx, 1280(%r15)
	movl	%edx, %eax
	je	.L1212
	movq	-544(%rbp), %rbx
	cmpl	$1, 8(%rbx)
	ja	.L1212
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L1366
.L1213:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeGenerator30AssembleCodeStartRegisterCheckEv@PLT
	movq	232(%r15), %rax
	subl	216(%r15), %eax
.L1212:
	movq	-544(%rbp), %rbx
	movl	%eax, 1284(%r15)
	movl	8(%rbx), %r9d
	testl	%r9d, %r9d
	jne	.L1215
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L1367
.L1216:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeGenerator20BailoutIfDeoptimizedEv@PLT
	movq	232(%r15), %rax
	subl	216(%r15), %eax
.L1215:
	cmpl	$1, 1244(%r15)
	movl	%eax, 1288(%r15)
	je	.L1219
	movq	152(%r15), %rax
	movl	(%rax), %eax
	testb	$4, %ah
	je	.L1220
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L1368
.L1221:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeGenerator46GenerateSpeculationPoisonFromCodeStartRegisterEv@PLT
	movq	152(%r15), %rax
	movl	(%rax), %eax
	testb	$8, %ah
	jne	.L1369
.L1219:
	movq	-544(%rbp), %rax
	movq	104(%rax), %r12
	movq	96(%rax), %rbx
	movq	%rax, %r13
	cmpq	%r12, %rbx
	je	.L1229
	.p2align 4,,10
	.p2align 3
.L1228:
	movq	(%rbx), %rax
	cmpq	%rax, 24(%r13)
	je	.L1227
	movl	$0, -528(%rbp)
	movq	%r15, %rdi
	movq	%rax, -520(%rbp)
	movq	$0x000000000, -512(%rbp)
	movq	$0, -504(%rbp)
	pushq	-504(%rbp)
	pushq	-512(%rbp)
	pushq	-520(%rbp)
	pushq	-528(%rbp)
	call	_ZN2v88internal8compiler13CodeGenerator27DefineDeoptimizationLiteralENS1_21DeoptimizationLiteralE
	addq	$32, %rsp
	movl	%eax, 24(%rbx)
.L1227:
	addq	$32, %rbx
	cmpq	%rbx, %r12
	jne	.L1228
.L1229:
	movq	1032(%r15), %rdx
	subq	1000(%r15), %rdx
	sarq	$3, %rdx
	movq	1008(%r15), %rax
	subq	1016(%r15), %rax
	subq	$1, %rdx
	sarq	$5, %rax
	movq	%rdx, %rcx
	salq	$4, %rcx
	leaq	(%rcx,%rax), %rdx
	movq	992(%r15), %rax
	subq	976(%r15), %rax
	sarq	$5, %rax
	addq	%rdx, %rax
	movq	%rax, 1040(%r15)
	movq	-544(%rbp), %rax
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L1226
	movl	$0, -528(%rbp)
	movq	%r15, %rdi
	movq	%rax, -520(%rbp)
	movq	$0x000000000, -512(%rbp)
	movq	$0, -504(%rbp)
	pushq	-504(%rbp)
	pushq	-512(%rbp)
	pushq	-520(%rbp)
	pushq	-528(%rbp)
	call	_ZN2v88internal8compiler13CodeGenerator27DefineDeoptimizationLiteralENS1_21DeoptimizationLiteralE
	addq	$32, %rsp
.L1226:
	movq	-544(%rbp), %rax
	movq	96(%rax), %r12
	movq	104(%rax), %rbx
	cmpq	%rbx, %r12
	je	.L1233
	.p2align 4,,10
	.p2align 3
.L1230:
	movq	8(%r12), %rax
	movq	%r15, %rdi
	addq	$32, %r12
	movl	$0, -528(%rbp)
	movq	$0x000000000, -512(%rbp)
	movq	%rax, -520(%rbp)
	movq	$0, -504(%rbp)
	pushq	-504(%rbp)
	pushq	-512(%rbp)
	pushq	-520(%rbp)
	pushq	-528(%rbp)
	call	_ZN2v88internal8compiler13CodeGenerator27DefineDeoptimizationLiteralENS1_21DeoptimizationLiteralE
	addq	$32, %rsp
	cmpq	%rbx, %r12
	jne	.L1230
.L1233:
	cmpb	$0, _ZN2v88internal29FLAG_perf_prof_unwinding_infoE(%rip)
	movq	40(%r15), %rax
	jne	.L1370
.L1232:
	movq	-544(%rbp), %rbx
	movl	(%rbx), %ebx
	movl	%ebx, -536(%rbp)
	andb	$64, %bh
	jne	.L1371
.L1235:
	movq	232(%r15), %rdx
	subq	216(%r15), %rdx
	movl	%edx, 1292(%r15)
	movq	24(%rax), %rax
	movl	%edx, %ecx
	movq	8(%rax), %rdx
	movq	16(%rax), %rax
	movq	%rax, -560(%rbp)
	cmpq	%rax, %rdx
	je	.L1246
	movq	.LC6(%rip), %xmm2
	movq	%r14, -552(%rbp)
	leaq	48(%r15), %r13
	movq	%rdx, %rbx
	movhps	.LC7(%rip), %xmm2
	movaps	%xmm2, -592(%rbp)
	.p2align 4,,10
	.p2align 3
.L1305:
	movq	(%rbx), %r14
	cmpb	$0, 123(%r14)
	je	.L1247
	cmpq	$0, 416(%r15)
	je	.L1372
.L1247:
	movq	-544(%rbp), %rcx
	movq	232(%r15), %rax
	subq	216(%r15), %rax
	movslq	100(%r14), %rdx
	movl	(%rcx), %ecx
	movl	%eax, %esi
	movl	%ecx, -536(%rbp)
	andb	$64, %ch
	je	.L1248
	movq	1256(%r15), %rcx
	movl	%eax, (%rcx,%rdx,4)
	movl	100(%r14), %edx
	movq	232(%r15), %rsi
	subl	216(%r15), %esi
.L1248:
	movl	%edx, 176(%r15)
	movq	%r13, %rdi
	movq	%r14, %rdx
	call	_ZN2v88internal8compiler19UnwindingInfoWriter21BeginInstructionBlockEiPKNS1_16InstructionBlockE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L1373
.L1249:
	movzbl	124(%r14), %esi
	movq	24(%r15), %rdi
	call	_ZN2v88internal8compiler16FrameAccessState12MarkHasFrameEb@PLT
	movslq	176(%r15), %rdx
	movq	160(%r15), %rax
	movq	-552(%rbp), %rdi
	leaq	(%rax,%rdx,8), %rsi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeGenerator24TryInsertBranchPoisoningEPKNS1_16InstructionBlockE
	cmpb	$0, 125(%r14)
	jne	.L1374
.L1269:
	movl	112(%r14), %r12d
	cmpl	116(%r14), %r12d
	jl	.L1273
	jmp	.L1271
	.p2align 4,,10
	.p2align 3
.L1375:
	addl	$1, %r12d
	cmpl	%r12d, 116(%r14)
	jle	.L1271
.L1273:
	movq	%r14, %rdx
	movl	%r12d, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeGenerator19AssembleInstructionEiPKNS1_16InstructionBlockE
	testl	%eax, %eax
	je	.L1375
	movl	%eax, 1240(%r15)
.L1292:
	movzbl	-601(%rbp), %eax
	movb	%al, 736(%r15)
.L1210:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1376
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1271:
	.cfi_restore_state
	movq	%r14, %rsi
	movq	%r13, %rdi
	addq	$8, %rbx
	movl	$0, 1240(%r15)
	call	_ZN2v88internal8compiler19UnwindingInfoWriter19EndInstructionBlockEPKNS1_16InstructionBlockE@PLT
	cmpq	%rbx, -560(%rbp)
	jne	.L1305
	movq	-552(%rbp), %r14
	movq	232(%r15), %rcx
	subl	216(%r15), %ecx
.L1246:
	movq	1112(%r15), %rbx
	movl	%ecx, 1296(%r15)
	testq	%rbx, %rbx
	je	.L1279
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L1377
	.p2align 4,,10
	.p2align 3
.L1282:
	leaq	8(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*16(%rax)
	movl	16(%rbx), %eax
	testl	%eax, %eax
	js	.L1378
	movq	40(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L1282
.L1279:
	movq	%r14, %rdi
	call	_ZN2v88internal9Assembler3nopEv@PLT
	movq	936(%r15), %rax
	subq	904(%r15), %rax
	movq	%r15, %rdi
	sarq	$3, %rax
	movq	896(%r15), %rsi
	subq	880(%r15), %rsi
	subq	$1, %rax
	sarq	$3, %rsi
	salq	$6, %rax
	movq	%rax, %rdx
	movq	912(%r15), %rax
	subq	920(%r15), %rax
	sarq	$3, %rax
	addq	%rdx, %rax
	addq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeGenerator29PrepareForDeoptimizationExitsEi@PLT
	cmpb	$0, _ZN2v88internal11Deoptimizer27kSupportsFixedDeoptExitSizeE(%rip)
	jne	.L1275
	movq	232(%r15), %rax
	subl	216(%r15), %eax
.L1283:
	movq	904(%r15), %rcx
	movq	880(%r15), %r13
	leaq	760(%r15), %rdi
	movl	%eax, 1300(%r15)
	movq	%rdi, -576(%rbp)
	movq	896(%r15), %rbx
	movq	%rcx, -552(%rbp)
	movq	912(%r15), %rcx
	movl	$0, -568(%rbp)
	movq	%rcx, -536(%rbp)
	cmpq	%r13, %rcx
	je	.L1285
	.p2align 4,,10
	.p2align 3
.L1284:
	movq	0(%r13), %r12
	cmpb	$0, 38(%r12)
	jne	.L1287
	cmpb	$0, _ZN2v88internal11Deoptimizer27kSupportsFixedDeoptExitSizeE(%rip)
	je	.L1288
	movl	840(%r15), %eax
	leal	1(%rax), %edx
	movl	%edx, 840(%r15)
	movl	%eax, (%r12)
.L1288:
	leaq	16(%r12), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	cmpb	$2, 36(%r12)
	je	.L1379
.L1289:
	movl	(%r12), %r8d
	cmpl	$16384, %r8d
	movl	%r8d, -544(%rbp)
	jg	.L1290
	movzbl	37(%r12), %eax
	movzbl	36(%r12), %esi
	movq	712(%r15), %rdi
	movb	%al, -560(%rbp)
	call	_ZN2v88internal11Deoptimizer22GetDeoptimizationEntryEPNS0_7IsolateENS0_14DeoptimizeKindE@PLT
	movl	-544(%rbp), %r8d
	movq	%rax, %r10
	movq	152(%r15), %rax
	testb	$32, (%rax)
	jne	.L1380
.L1291:
	movl	%r8d, %edx
	movq	%r10, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal14TurboAssembler21CallForDeoptimizationEmi@PLT
	movb	$1, 38(%r12)
	movl	$0, 1240(%r15)
.L1287:
	addq	$8, %r13
	cmpq	%r13, %rbx
	je	.L1381
	cmpq	%r13, -536(%rbp)
	jne	.L1284
.L1286:
	movq	232(%r15), %rax
	subl	216(%r15), %eax
.L1285:
	movl	%eax, 1304(%r15)
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeGenerator10FinishCodeEv@PLT
	movq	232(%r15), %rax
	subq	216(%r15), %rax
	cmpq	$0, 1104(%r15)
	movl	%eax, 1308(%r15)
	movl	%eax, %esi
	je	.L1295
	movl	$8, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal9Assembler5AlignEi@PLT
	movq	1104(%r15), %rbx
	testq	%rbx, %rbx
	je	.L1364
	.p2align 4,,10
	.p2align 3
.L1296:
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	24(%rbx), %rdx
	movq	16(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeGenerator17AssembleJumpTableEPPNS0_5LabelEm@PLT
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L1296
.L1364:
	movq	232(%r15), %rsi
	subl	216(%r15), %esi
.L1295:
	cmpb	$0, _ZN2v88internal29FLAG_perf_prof_unwinding_infoE(%rip)
	jne	.L1382
.L1297:
	movq	24(%r15), %rax
	leaq	760(%r15), %rdi
	movq	%r14, %rsi
	movq	(%rax), %rax
	movl	4(%rax), %edx
	call	_ZN2v88internal21SafepointTableBuilder4EmitEPNS0_9AssemblerEi@PLT
	movq	816(%r15), %rax
	cmpq	%rax, 824(%r15)
	je	.L1300
	movq	%r14, %rdi
	call	_ZN2v88internal12HandlerTable20EmitReturnTableStartEPNS0_9AssemblerE@PLT
	movl	%eax, 1080(%r15)
	movq	816(%r15), %rax
	cmpq	%rax, 824(%r15)
	je	.L1300
	xorl	%ebx, %ebx
	jmp	.L1304
	.p2align 4,,10
	.p2align 3
.L1301:
	je	.L1303
	subl	$1, %edx
.L1302:
	movl	8(%rax), %esi
	movq	%r14, %rdi
	addq	$1, %rbx
	call	_ZN2v88internal12HandlerTable15EmitReturnEntryEPNS0_9AssemblerEii@PLT
	movq	816(%r15), %rax
	movq	824(%r15), %rdx
	subq	%rax, %rdx
	sarq	$4, %rdx
	cmpq	%rdx, %rbx
	jnb	.L1300
.L1304:
	movq	%rbx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	movq	(%rax), %rdx
	movl	(%rdx), %edx
	testl	%edx, %edx
	jns	.L1301
	notl	%edx
	jmp	.L1302
	.p2align 4,,10
	.p2align 3
.L1374:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeGenerator22AssembleConstructFrameEv@PLT
	movq	32(%r15), %rax
	movq	(%rax), %rax
	testb	$8, 64(%rax)
	je	.L1269
	movq	712(%r15), %rdi
	call	_ZN2v88internal17ExternalReference12isolate_rootEPNS0_7IsolateE@PLT
	movl	_ZN2v88internalL13kRootRegisterE(%rip), %esi
	movq	-552(%rbp), %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal14TurboAssembler4MoveENS0_8RegisterENS0_17ExternalReferenceE@PLT
	jmp	.L1269
	.p2align 4,,10
	.p2align 3
.L1373:
	leaq	-320(%rbp), %rax
	leaq	-432(%rbp), %r12
	movq	%rax, %rdi
	movq	%rax, -536(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rcx
	xorl	%edi, %edi
	xorl	%esi, %esi
	movq	%rcx, -320(%rbp)
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	pxor	%xmm0, %xmm0
	movw	%di, -96(%rbp)
	movq	%rcx, -432(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	$0, -104(%rbp)
	movq	-24(%rcx), %rax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rcx, -432(%rbp,%rax)
	movq	-432(%rbp), %rax
	movq	-24(%rax), %rcx
	addq	%r12, %rcx
	movq	%rcx, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	pxor	%xmm0, %xmm0
	movdqa	-592(%rbp), %xmm1
	movq	%rcx, -320(%rbp)
	leaq	-368(%rbp), %rcx
	movq	%rcx, %rdi
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rcx, -568(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	movq	-536(%rbp), %rdi
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	leaq	-424(%rbp), %rsi
	movq	%rcx, -424(%rbp)
	leaq	-336(%rbp), %rcx
	movq	%rcx, -576(%rbp)
	movq	%rcx, -352(%rbp)
	movl	$16, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movl	$4, %edx
	leaq	.LC21(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	100(%r14), %esi
	movq	%r12, %rdi
	call	_ZNSolsEi@PLT
	movl	$6, %edx
	leaq	.LC22(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 120(%r14)
	jne	.L1383
.L1250:
	cmpb	$0, 124(%r14)
	je	.L1384
.L1251:
	cmpb	$0, 125(%r14)
	jne	.L1385
.L1252:
	cmpb	$0, 126(%r14)
	jne	.L1386
.L1253:
	movl	108(%r14), %esi
	testl	%esi, %esi
	js	.L1254
	movl	$13, %edx
	leaq	.LC27(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	108(%r14), %esi
	movq	%r12, %rdi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC28(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L1254:
	movl	104(%r14), %ecx
	testl	%ecx, %ecx
	js	.L1255
	movl	$10, %edx
	leaq	.LC29(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	104(%r14), %esi
	movq	%r12, %rdi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC28(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L1255:
	movq	%r12, %rdi
	movl	$3, %edx
	leaq	.LC4(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-384(%rbp), %rax
	leaq	-480(%rbp), %r12
	movq	$0, -488(%rbp)
	movq	%r12, -496(%rbp)
	leaq	-496(%rbp), %rdi
	movb	$0, -480(%rbp)
	testq	%rax, %rax
	je	.L1256
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L1257
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L1258:
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	movq	-496(%rbp), %r8
	jne	.L1387
.L1259:
	cmpq	%r12, %r8
	je	.L1266
	movq	%r8, %rdi
	call	_ZdlPv@PLT
.L1266:
	movq	.LC6(%rip), %xmm0
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -320(%rbp)
	movhps	.LC8(%rip), %xmm0
	movaps	%xmm0, -432(%rbp)
	cmpq	-576(%rbp), %rdi
	je	.L1267
	call	_ZdlPv@PLT
.L1267:
	movq	-568(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	-536(%rbp), %rdi
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L1249
	.p2align 4,,10
	.p2align 3
.L1257:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	movq	-496(%rbp), %r8
	je	.L1259
.L1387:
	leaq	-448(%rbp), %rax
	leaq	-464(%rbp), %r11
	movq	%rax, -600(%rbp)
	movq	%rax, -464(%rbp)
	testq	%r8, %r8
	je	.L1388
	movq	%r8, %rdi
	movq	%r11, -624(%rbp)
	movq	%r8, -616(%rbp)
	call	strlen@PLT
	movq	-616(%rbp), %r8
	movq	-624(%rbp), %r11
	cmpq	$15, %rax
	movq	%rax, -528(%rbp)
	movq	%rax, %r10
	ja	.L1389
	cmpq	$1, %rax
	jne	.L1263
	movzbl	(%r8), %edx
	movb	%dl, -448(%rbp)
	movq	-600(%rbp), %rdx
.L1264:
	movq	%rax, -456(%rbp)
	leaq	240(%r15), %rdi
	movb	$0, (%rdx,%rax)
	movq	%r11, %rdx
	movq	232(%r15), %rsi
	subq	216(%r15), %rsi
	call	_ZN2v88internal18CodeCommentsWriter3AddEjNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-464(%rbp), %rdi
	cmpq	-600(%rbp), %rdi
	je	.L1265
	call	_ZdlPv@PLT
.L1265:
	movq	-496(%rbp), %r8
	jmp	.L1259
	.p2align 4,,10
	.p2align 3
.L1386:
	movl	$20, %edx
	leaq	.LC26(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L1253
	.p2align 4,,10
	.p2align 3
.L1385:
	movl	$18, %edx
	leaq	.LC25(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L1252
	.p2align 4,,10
	.p2align 3
.L1384:
	movl	$11, %edx
	leaq	.LC24(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L1251
	.p2align 4,,10
	.p2align 3
.L1383:
	movl	$11, %edx
	leaq	.LC23(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L1250
.L1372:
	movq	-552(%rbp), %rdi
	call	_ZN2v88internal9Assembler15CodeTargetAlignEv@PLT
	jmp	.L1247
.L1256:
	leaq	-352(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L1258
.L1263:
	testq	%rax, %rax
	jne	.L1390
	movq	-600(%rbp), %rdx
	jmp	.L1264
.L1275:
	movq	232(%r15), %rdx
	subq	216(%r15), %rdx
	movl	%edx, 844(%r15)
	movl	%edx, %eax
	jmp	.L1283
	.p2align 4,,10
	.p2align 3
.L1290:
	movl	$1, 1240(%r15)
	jmp	.L1292
	.p2align 4,,10
	.p2align 3
.L1381:
	movq	-552(%rbp), %rbx
	movq	8(%rbx), %r13
	leaq	8(%rbx), %rax
	leaq	512(%r13), %rbx
	cmpq	%r13, -536(%rbp)
	je	.L1286
	movq	%rax, -552(%rbp)
	jmp	.L1284
.L1378:
	leaq	16(%rbx), %rsi
	movl	$1, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal9Assembler3jmpEPNS0_5LabelENS2_8DistanceE@PLT
	movq	40(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L1282
	jmp	.L1279
.L1379:
	movl	32(%r12), %esi
	movl	(%r12), %r8d
	movl	-568(%rbp), %ecx
	movq	-576(%rbp), %rdi
	movq	232(%r15), %rdx
	subq	216(%r15), %rdx
	call	_ZN2v88internal21SafepointTableBuilder24UpdateDeoptimizationInfoEiiij@PLT
	movl	%eax, -568(%rbp)
	jmp	.L1289
.L1380:
	movq	8(%r12), %rdx
	movzbl	-560(%rbp), %esi
	movl	%r8d, %ecx
	movq	%r14, %rdi
	movq	%r10, -592(%rbp)
	call	_ZN2v88internal9Assembler17RecordDeoptReasonENS0_16DeoptimizeReasonENS0_14SourcePositionEi@PLT
	movq	-592(%rbp), %r10
	movl	-544(%rbp), %r8d
	jmp	.L1291
.L1365:
	movq	184(%rdi), %rsi
	call	_ZN2v88internal8compiler13CodeGenerator22AssembleSourcePositionENS0_14SourcePositionE
	jmp	.L1211
.L1371:
	movq	16(%rax), %rdx
	movq	1256(%r15), %rdi
	movq	16(%rdx), %rbx
	subq	8(%rdx), %rbx
	movq	1272(%r15), %rdx
	sarq	$3, %rbx
	subq	%rdi, %rdx
	sarq	$2, %rdx
	cmpq	%rdx, %rbx
	ja	.L1391
	movq	1264(%r15), %rcx
	movq	%rcx, %rdx
	subq	%rdi, %rdx
	sarq	$2, %rdx
	cmpq	%rdx, %rbx
	jbe	.L1242
	cmpq	%rcx, %rdi
	je	.L1243
	subq	%rdi, %rcx
	movl	$255, %esi
	movq	%rcx, %rdx
	call	memset@PLT
	movq	1264(%r15), %rcx
	movq	%rcx, %rdx
	subq	1256(%r15), %rdx
	sarq	$2, %rdx
.L1243:
	subq	%rdx, %rbx
	je	.L1244
	salq	$2, %rbx
	movq	%rcx, %rdi
	movl	$255, %esi
	movq	%rbx, %rdx
	call	memset@PLT
	movq	%rax, %rcx
	addq	%rbx, %rcx
.L1244:
	movq	%rcx, 1264(%r15)
	movq	40(%r15), %rax
	jmp	.L1241
.L1370:
	movq	16(%rax), %rcx
	movq	136(%r15), %rsi
	movq	128(%r15), %rdi
	movq	16(%rcx), %rdx
	subq	8(%rcx), %rdx
	movq	%rsi, %rcx
	sarq	$3, %rdx
	subq	%rdi, %rcx
	movslq	%edx, %rdx
	sarq	$3, %rcx
	cmpq	%rcx, %rdx
	ja	.L1392
	jnb	.L1232
	leaq	(%rdi,%rdx,8), %rdx
	cmpq	%rdx, %rsi
	je	.L1232
	movq	%rdx, 136(%r15)
	jmp	.L1232
.L1389:
	movq	%r11, %rdi
	leaq	-528(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r8, -624(%rbp)
	movq	%r11, -616(%rbp)
	movq	%rax, -632(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-616(%rbp), %r11
	movq	-624(%rbp), %r8
	movq	%rax, -464(%rbp)
	movq	%rax, %rdi
	movq	-528(%rbp), %rax
	movq	-632(%rbp), %r10
	movq	%rax, -448(%rbp)
.L1262:
	movq	%r10, %rdx
	movq	%r8, %rsi
	movq	%r11, -616(%rbp)
	call	memcpy@PLT
	movq	-528(%rbp), %rax
	movq	-464(%rbp), %rdx
	movq	-616(%rbp), %r11
	jmp	.L1264
.L1300:
	movq	%r14, %rdi
	call	_ZN2v88internal9Assembler28FinalizeJumpOptimizationInfoEv@PLT
	movzbl	-601(%rbp), %eax
	movl	$0, 1240(%r15)
	movb	%al, 736(%r15)
	jmp	.L1210
.L1388:
	leaq	.LC5(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L1220:
	movq	%r14, %rdi
	call	_ZN2v88internal14TurboAssembler30ResetSpeculationPoisonRegisterEv@PLT
	jmp	.L1219
.L1369:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeGenerator33AssembleRegisterArgumentPoisoningEv@PLT
	jmp	.L1219
.L1368:
	leaq	-432(%rbp), %r12
	leaq	-528(%rbp), %rsi
	xorl	%edx, %edx
	movq	$43, -528(%rbp)
	movq	%r12, %rdi
	leaq	-416(%rbp), %rbx
	movq	%rbx, -432(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-528(%rbp), %rdx
	movdqa	.LC11(%rip), %xmm0
	movabsq	$7957705963265335406, %rcx
	movq	%rax, -432(%rbp)
	movl	$11552, %r8d
	leaq	240(%r15), %rdi
	movq	%rdx, -416(%rbp)
	movups	%xmm0, (%rax)
	movdqa	.LC12(%rip), %xmm0
	movq	%rcx, 32(%rax)
	movw	%r8w, 40(%rax)
	movb	$45, 42(%rax)
	movups	%xmm0, 16(%rax)
	movq	-528(%rbp), %rax
	movq	-432(%rbp), %rdx
	movq	%rax, -424(%rbp)
	movb	$0, (%rdx,%rax)
	movq	%r12, %rdx
	movq	232(%r15), %rsi
	subq	216(%r15), %rsi
	call	_ZN2v88internal18CodeCommentsWriter3AddEjNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-432(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1221
	call	_ZdlPv@PLT
	jmp	.L1221
.L1367:
	leaq	-432(%rbp), %r12
	leaq	-528(%rbp), %rsi
	xorl	%edx, %edx
	movq	$40, -528(%rbp)
	movq	%r12, %rdi
	leaq	-416(%rbp), %rbx
	movq	%rbx, -432(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-528(%rbp), %rdx
	movdqa	.LC30(%rip), %xmm0
	movabsq	$3255293764346410081, %rcx
	movq	%rax, -432(%rbp)
	leaq	240(%r15), %rdi
	movq	%rdx, -416(%rbp)
	movups	%xmm0, (%rax)
	movdqa	.LC32(%rip), %xmm0
	movq	%rcx, 32(%rax)
	movups	%xmm0, 16(%rax)
	movq	-528(%rbp), %rax
	movq	-432(%rbp), %rdx
	movq	%rax, -424(%rbp)
	movb	$0, (%rdx,%rax)
	movq	%r12, %rdx
	movq	232(%r15), %rsi
	subq	216(%r15), %rsi
	call	_ZN2v88internal18CodeCommentsWriter3AddEjNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-432(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1216
	call	_ZdlPv@PLT
	jmp	.L1216
.L1242:
	testq	%rbx, %rbx
	je	.L1245
	salq	$2, %rbx
	movl	$255, %esi
	movq	%rbx, %rdx
	call	memset@PLT
	movq	1264(%r15), %rcx
	movq	%rax, %rdi
	movq	40(%r15), %rax
	addq	%rbx, %rdi
.L1245:
	cmpq	%rcx, %rdi
	je	.L1241
	movq	%rdi, 1264(%r15)
.L1241:
	leaq	-528(%rbp), %r8
	leaq	1312(%r15), %rdi
	movq	$-1, -528(%rbp)
	movl	$-1, -520(%rbp)
	movq	264(%rax), %rdx
	subq	232(%rax), %rdx
	movq	240(%rax), %rsi
	sarq	$3, %rdx
	subq	248(%rax), %rsi
	subq	$1, %rdx
	sarq	$3, %rsi
	salq	$6, %rdx
	addq	%rsi, %rdx
	movq	224(%rax), %rsi
	subq	208(%rax), %rsi
	sarq	$3, %rsi
	addq	%rdx, %rsi
	movq	%r8, %rdx
	call	_ZNSt6vectorIN2v88internal8compiler30TurbolizerInstructionStartInfoENS1_13ZoneAllocatorIS3_EEE14_M_fill_assignEmRKS3_
	movq	40(%r15), %rax
	jmp	.L1235
.L1377:
	leaq	-432(%rbp), %r12
	leaq	-528(%rbp), %rsi
	xorl	%edx, %edx
	movq	$22, -528(%rbp)
	movq	%r12, %rdi
	leaq	-416(%rbp), %rbx
	movq	%rbx, -432(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-528(%rbp), %rdx
	movdqa	.LC33(%rip), %xmm0
	leaq	240(%r15), %rdi
	movq	%rax, -432(%rbp)
	movq	%rdx, -416(%rbp)
	movl	$11565, %edx
	movl	$543515759, 16(%rax)
	movw	%dx, 20(%rax)
	movups	%xmm0, (%rax)
	movq	-528(%rbp), %rax
	movq	-432(%rbp), %rdx
	movq	%rax, -424(%rbp)
	movb	$0, (%rdx,%rax)
	movq	%r12, %rdx
	movq	232(%r15), %rsi
	subq	216(%r15), %rsi
	call	_ZN2v88internal18CodeCommentsWriter3AddEjNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-432(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1278
	call	_ZdlPv@PLT
.L1278:
	movq	1112(%r15), %rbx
	testq	%rbx, %rbx
	jne	.L1282
	jmp	.L1279
.L1366:
	leaq	-432(%rbp), %r12
	leaq	-528(%rbp), %rsi
	xorl	%edx, %edx
	movq	$41, -528(%rbp)
	movq	%r12, %rdi
	leaq	-416(%rbp), %rbx
	movq	%rbx, -432(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-528(%rbp), %rdx
	movdqa	.LC30(%rip), %xmm0
	movabsq	$3251724711032482151, %rcx
	movq	%rax, -432(%rbp)
	leaq	240(%r15), %rdi
	movq	%rdx, -416(%rbp)
	movups	%xmm0, (%rax)
	movdqa	.LC31(%rip), %xmm0
	movq	%rcx, 32(%rax)
	movb	$45, 40(%rax)
	movups	%xmm0, 16(%rax)
	movq	-528(%rbp), %rax
	movq	-432(%rbp), %rdx
	movq	%rax, -424(%rbp)
	movb	$0, (%rdx,%rax)
	movq	%r12, %rdx
	movq	232(%r15), %rsi
	subq	216(%r15), %rsi
	call	_ZN2v88internal18CodeCommentsWriter3AddEjNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-432(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1213
	call	_ZdlPv@PLT
	jmp	.L1213
.L1392:
	subq	%rcx, %rdx
	leaq	120(%r15), %rdi
	movq	%rdx, %rsi
	call	_ZNSt6vectorIPKN2v88internal8compiler19UnwindingInfoWriter17BlockInitialStateENS1_13ZoneAllocatorIS6_EEE17_M_default_appendEm
	movq	40(%r15), %rax
	jmp	.L1232
.L1391:
	cmpq	$536870911, %rbx
	ja	.L1393
	movq	1248(%r15), %rdi
	leaq	0(,%rbx,4), %r12
	testq	%rbx, %rbx
	je	.L1307
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	leaq	7(%r12), %rsi
	andq	$-8, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L1394
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
.L1240:
	movq	%rcx, %rdi
	movq	%r12, %rdx
	leaq	(%rcx,%r12), %rbx
	movl	$255, %esi
	call	memset@PLT
	movq	%rax, %rcx
	movq	40(%r15), %rax
.L1238:
	movq	%rcx, %xmm0
	movq	%rbx, %xmm3
	movq	%rbx, 1272(%r15)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 1256(%r15)
	jmp	.L1241
.L1382:
	leaq	56(%r15), %rdi
	call	_ZN2v88internal13EhFrameWriter6FinishEi@PLT
	jmp	.L1297
.L1307:
	xorl	%ecx, %ecx
	xorl	%ebx, %ebx
	jmp	.L1238
.L1394:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L1240
.L1393:
	leaq	.LC15(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.p2align 4,,10
	.p2align 3
.L1303:
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1376:
	call	__stack_chk_fail@PLT
.L1390:
	movq	-600(%rbp), %rdi
	jmp	.L1262
	.cfi_endproc
.LFE24253:
	.size	_ZN2v88internal8compiler13CodeGenerator12AssembleCodeEv, .-_ZN2v88internal8compiler13CodeGenerator12AssembleCodeEv
	.section	.text._ZN2v88internal8compiler13CodeGenerator18RecordCallPositionEPNS1_11InstructionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13CodeGenerator18RecordCallPositionEPNS1_11InstructionE
	.type	_ZN2v88internal8compiler13CodeGenerator18RecordCallPositionEPNS1_11InstructionE, @function
_ZN2v88internal8compiler13CodeGenerator18RecordCallPositionEPNS1_11InstructionE:
.LFB24298:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movl	(%rsi), %ebx
	movq	24(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	shrl	$22, %ebx
	movl	%ebx, %r14d
	andl	$1, %r14d
	movl	%r14d, %edx
	call	_ZN2v88internal8compiler13CodeGenerator15RecordSafepointEPNS1_12ReferenceMapENS0_9Safepoint9DeoptModeE
	andl	$2, %ebx
	jne	.L1445
.L1396:
	testl	%r14d, %r14d
	jne	.L1446
.L1395:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1447
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1445:
	.cfi_restore_state
	movl	4(%r13), %eax
	movl	%eax, %edx
	movzbl	%al, %eax
	shrl	$8, %edx
	movzwl	%dx, %edx
	leaq	4(%rdx,%rax), %rax
	movq	0(%r13,%rax,8), %rsi
	movq	40(%r12), %rax
	movl	%esi, %edx
	movq	%rsi, %rdi
	andl	$7, %edx
	shrq	$3, %rdi
	cmpl	$3, %edx
	je	.L1448
	leaq	104(%rax), %rcx
	movq	112(%rax), %rax
	movl	%edi, %esi
	testq	%rax, %rax
	je	.L1401
	movq	%rcx, %rdx
	jmp	.L1402
	.p2align 4,,10
	.p2align 3
.L1449:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L1403
.L1402:
	cmpl	32(%rax), %esi
	jle	.L1449
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L1402
.L1403:
	cmpq	%rdx, %rcx
	je	.L1401
	cmpl	%edi, 32(%rdx)
	cmovle	%rdx, %rcx
.L1401:
	movq	48(%rcx), %rax
.L1400:
	movq	160(%r12), %rdx
	cltq
	movq	824(%r12), %rbx
	leaq	(%rdx,%rax,8), %rdx
	movq	232(%r12), %rax
	subq	216(%r12), %rax
	movq	%rax, %r15
	cmpq	832(%r12), %rbx
	je	.L1406
	movq	%rdx, (%rbx)
	movl	%eax, 8(%rbx)
	addq	$16, 824(%r12)
	testl	%r14d, %r14d
	je	.L1395
.L1446:
	movq	232(%r12), %rax
	subq	216(%r12), %rax
	movl	%eax, 1084(%r12)
	movzbl	4(%r13), %eax
	movq	40(%r12), %r8
	movq	56(%r13,%rax,8), %rax
	movl	%eax, %edx
	andl	$7, %edx
	cmpl	$3, %edx
	je	.L1450
	shrq	$3, %rax
	leaq	104(%r8), %rsi
	movq	%rax, %rcx
	movl	%eax, %edi
	movq	112(%r8), %rax
	testq	%rax, %rax
	je	.L1420
	movq	%rsi, %rdx
	jmp	.L1421
	.p2align 4,,10
	.p2align 3
.L1451:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L1422
.L1421:
	cmpl	32(%rax), %edi
	jle	.L1451
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L1421
.L1422:
	cmpq	%rdx, %rsi
	je	.L1420
	cmpl	%ecx, 32(%rdx)
	cmovle	%rdx, %rsi
.L1420:
	movq	48(%rsi), %rsi
.L1419:
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler19InstructionSequence22GetDeoptimizationEntryEi@PLT
	movl	$2, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	(%rax), %rax
	movq	232(%r12), %rdx
	subq	216(%r12), %rdx
	movq	8(%rax), %r8
	call	_ZN2v88internal8compiler13CodeGenerator16BuildTranslationEPNS1_11InstructionEimNS1_23OutputFrameStateCombineE
	jmp	.L1395
	.p2align 4,,10
	.p2align 3
.L1450:
	movq	%rax, %rsi
	sarq	$32, %rsi
	testb	$8, %al
	je	.L1452
	salq	$4, %rsi
	addq	152(%r8), %rsi
	movq	8(%rsi), %rsi
	jmp	.L1419
	.p2align 4,,10
	.p2align 3
.L1448:
	sarq	$32, %rsi
	andl	$1, %edi
	je	.L1453
	salq	$4, %rsi
	addq	152(%rax), %rsi
	movq	8(%rsi), %rax
	jmp	.L1400
	.p2align 4,,10
	.p2align 3
.L1406:
	movq	816(%r12), %r8
	movq	%rbx, %rcx
	subq	%r8, %rcx
	movq	%rcx, %rax
	sarq	$4, %rax
	cmpq	$134217727, %rax
	je	.L1454
	testq	%rax, %rax
	je	.L1427
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L1455
	movl	$2147483632, %esi
	movl	$2147483632, %r9d
.L1409:
	movq	808(%r12), %r10
	movq	16(%r10), %rdi
	movq	24(%r10), %rax
	subq	%rdi, %rax
	cmpq	%rsi, %rax
	jb	.L1456
	addq	%rdi, %rsi
	movq	%rsi, 16(%r10)
.L1412:
	addq	%rdi, %r9
	leaq	16(%rdi), %rax
.L1410:
	addq	%rdi, %rcx
	movq	%rdx, (%rcx)
	movl	%r15d, 8(%rcx)
	cmpq	%r8, %rbx
	je	.L1413
	movq	%r8, %rax
	movq	%rdi, %rdx
	.p2align 4,,10
	.p2align 3
.L1414:
	movq	(%rax), %rsi
	movl	8(%rax), %ecx
	addq	$16, %rax
	addq	$16, %rdx
	movq	%rsi, -16(%rdx)
	movl	%ecx, -8(%rdx)
	cmpq	%rax, %rbx
	jne	.L1414
	subq	%r8, %rbx
	leaq	16(%rdi,%rbx), %rax
.L1413:
	movq	%rdi, %xmm0
	movq	%rax, %xmm1
	movq	%r9, 832(%r12)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 816(%r12)
	jmp	.L1396
	.p2align 4,,10
	.p2align 3
.L1452:
	leaq	-80(%rbp), %rdi
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movq	-72(%rbp), %rsi
	movq	40(%r12), %r8
	jmp	.L1419
	.p2align 4,,10
	.p2align 3
.L1453:
	leaq	-80(%rbp), %rdi
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movq	-72(%rbp), %rax
	jmp	.L1400
	.p2align 4,,10
	.p2align 3
.L1427:
	movl	$16, %esi
	movl	$16, %r9d
	jmp	.L1409
	.p2align 4,,10
	.p2align 3
.L1455:
	testq	%rsi, %rsi
	jne	.L1457
	movl	$16, %eax
	xorl	%r9d, %r9d
	xorl	%edi, %edi
	jmp	.L1410
.L1456:
	movq	%r10, %rdi
	movq	%r9, -112(%rbp)
	movq	%rcx, -104(%rbp)
	movq	%r8, -96(%rbp)
	movq	%rdx, -88(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %r8
	movq	-104(%rbp), %rcx
	movq	-112(%rbp), %r9
	movq	%rax, %rdi
	jmp	.L1412
.L1447:
	call	__stack_chk_fail@PLT
.L1454:
	leaq	.LC0(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1457:
	cmpq	$134217727, %rsi
	movl	$134217727, %r9d
	cmovbe	%rsi, %r9
	salq	$4, %r9
	movq	%r9, %rsi
	jmp	.L1409
	.cfi_endproc
.LFE24298:
	.size	_ZN2v88internal8compiler13CodeGenerator18RecordCallPositionEPNS1_11InstructionE, .-_ZN2v88internal8compiler13CodeGenerator18RecordCallPositionEPNS1_11InstructionE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler13CodeGeneratorC2EPNS0_4ZoneEPNS1_5FrameEPNS1_7LinkageEPNS1_19InstructionSequenceEPNS0_24OptimizedCompilationInfoEPNS0_7IsolateENS_4base8OptionalINS1_9OsrHelperEEEiPNS0_20JumpOptimizationInfoENS0_24PoisoningMitigationLevelERKNS0_16AssemblerOptionsEimSt10unique_ptrINS0_15AssemblerBufferESt14default_deleteISQ_EE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler13CodeGeneratorC2EPNS0_4ZoneEPNS1_5FrameEPNS1_7LinkageEPNS1_19InstructionSequenceEPNS0_24OptimizedCompilationInfoEPNS0_7IsolateENS_4base8OptionalINS1_9OsrHelperEEEiPNS0_20JumpOptimizationInfoENS0_24PoisoningMitigationLevelERKNS0_16AssemblerOptionsEimSt10unique_ptrINS0_15AssemblerBufferESt14default_deleteISQ_EE, @function
_GLOBAL__sub_I__ZN2v88internal8compiler13CodeGeneratorC2EPNS0_4ZoneEPNS1_5FrameEPNS1_7LinkageEPNS1_19InstructionSequenceEPNS0_24OptimizedCompilationInfoEPNS0_7IsolateENS_4base8OptionalINS1_9OsrHelperEEEiPNS0_20JumpOptimizationInfoENS0_24PoisoningMitigationLevelERKNS0_16AssemblerOptionsEimSt10unique_ptrINS0_15AssemblerBufferESt14default_deleteISQ_EE:
.LFB31166:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE31166:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler13CodeGeneratorC2EPNS0_4ZoneEPNS1_5FrameEPNS1_7LinkageEPNS1_19InstructionSequenceEPNS0_24OptimizedCompilationInfoEPNS0_7IsolateENS_4base8OptionalINS1_9OsrHelperEEEiPNS0_20JumpOptimizationInfoENS0_24PoisoningMitigationLevelERKNS0_16AssemblerOptionsEimSt10unique_ptrINS0_15AssemblerBufferESt14default_deleteISQ_EE, .-_GLOBAL__sub_I__ZN2v88internal8compiler13CodeGeneratorC2EPNS0_4ZoneEPNS1_5FrameEPNS1_7LinkageEPNS1_19InstructionSequenceEPNS0_24OptimizedCompilationInfoEPNS0_7IsolateENS_4base8OptionalINS1_9OsrHelperEEEiPNS0_20JumpOptimizationInfoENS0_24PoisoningMitigationLevelERKNS0_16AssemblerOptionsEimSt10unique_ptrINS0_15AssemblerBufferESt14default_deleteISQ_EE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler13CodeGeneratorC2EPNS0_4ZoneEPNS1_5FrameEPNS1_7LinkageEPNS1_19InstructionSequenceEPNS0_24OptimizedCompilationInfoEPNS0_7IsolateENS_4base8OptionalINS1_9OsrHelperEEEiPNS0_20JumpOptimizationInfoENS0_24PoisoningMitigationLevelERKNS0_16AssemblerOptionsEimSt10unique_ptrINS0_15AssemblerBufferESt14default_deleteISQ_EE
	.weak	_ZTVN2v88internal8compiler13OutOfLineCodeE
	.section	.data.rel.ro._ZTVN2v88internal8compiler13OutOfLineCodeE,"awG",@progbits,_ZTVN2v88internal8compiler13OutOfLineCodeE,comdat
	.align 8
	.type	_ZTVN2v88internal8compiler13OutOfLineCodeE, @object
	.size	_ZTVN2v88internal8compiler13OutOfLineCodeE, 40
_ZTVN2v88internal8compiler13OutOfLineCodeE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.section	.rodata._ZN2v88internalL13kRootRegisterE,"a"
	.align 4
	.type	_ZN2v88internalL13kRootRegisterE, @object
	.size	_ZN2v88internalL13kRootRegisterE, 4
_ZN2v88internalL13kRootRegisterE:
	.long	13
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.data.rel.ro,"aw"
	.align 8
.LC6:
	.quad	_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE+24
	.align 8
.LC7:
	.quad	_ZTVSt15basic_streambufIcSt11char_traitsIcEE+16
	.align 8
.LC8:
	.quad	_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE+16
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC11:
	.quad	8028914772455992621
	.quad	7954877704936191335
	.align 16
.LC12:
	.quad	8102855774685983333
	.quad	8028075772544901989
	.align 16
.LC30:
	.quad	8028914772455992621
	.quad	7307199385478460775
	.align 16
.LC31:
	.quad	2334382411428686691
	.quad	7309940829683020915
	.align 16
.LC32:
	.quad	7214892425827871587
	.quad	8820701644874346341
	.align 16
.LC33:
	.quad	8007528184652246317
	.quad	7142820533899108454
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
