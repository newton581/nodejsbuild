	.file	"common-operator-reducer.cc"
	.text
	.section	.rodata._ZNK2v88internal8compiler21CommonOperatorReducer12reducer_nameEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"CommonOperatorReducer"
	.section	.text._ZNK2v88internal8compiler21CommonOperatorReducer12reducer_nameEv,"axG",@progbits,_ZNK2v88internal8compiler21CommonOperatorReducer12reducer_nameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal8compiler21CommonOperatorReducer12reducer_nameEv
	.type	_ZNK2v88internal8compiler21CommonOperatorReducer12reducer_nameEv, @function
_ZNK2v88internal8compiler21CommonOperatorReducer12reducer_nameEv:
.LFB10163:
	.cfi_startproc
	endbr64
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE10163:
	.size	_ZNK2v88internal8compiler21CommonOperatorReducer12reducer_nameEv, .-_ZNK2v88internal8compiler21CommonOperatorReducer12reducer_nameEv
	.section	.text._ZN2v88internal8compiler21CommonOperatorReducerD2Ev,"axG",@progbits,_ZN2v88internal8compiler21CommonOperatorReducerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler21CommonOperatorReducerD2Ev
	.type	_ZN2v88internal8compiler21CommonOperatorReducerD2Ev, @function
_ZN2v88internal8compiler21CommonOperatorReducerD2Ev:
.LFB13238:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE13238:
	.size	_ZN2v88internal8compiler21CommonOperatorReducerD2Ev, .-_ZN2v88internal8compiler21CommonOperatorReducerD2Ev
	.weak	_ZN2v88internal8compiler21CommonOperatorReducerD1Ev
	.set	_ZN2v88internal8compiler21CommonOperatorReducerD1Ev,_ZN2v88internal8compiler21CommonOperatorReducerD2Ev
	.section	.rodata._ZN2v88internal8compiler12_GLOBAL__N_115DecideConditionEPNS1_12JSHeapBrokerEPNS1_4NodeE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"IsHeapObject()"
.LC2:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_115DecideConditionEPNS1_12JSHeapBrokerEPNS1_4NodeE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_115DecideConditionEPNS1_12JSHeapBrokerEPNS1_4NodeE, @function
_ZN2v88internal8compiler12_GLOBAL__N_115DecideConditionEPNS1_12JSHeapBrokerEPNS1_4NodeE:
.LFB11539:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$40, %rsp
	.cfi_offset 12, -24
	movq	(%rsi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movzwl	16(%rdx), %eax
	cmpw	$23, %ax
	je	.L5
	cmpw	$30, %ax
	jne	.L14
	leaq	-48(%rbp), %r12
	movq	48(%rdx), %rdx
	xorl	%ecx, %ecx
	movq	%rdi, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L15
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12BooleanValueEv@PLT
	testb	%al, %al
	sete	%al
	addl	$1, %eax
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L14:
	xorl	%eax, %eax
.L4:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L16
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	movl	44(%rdx), %eax
	testl	%eax, %eax
	sete	%al
	addl	$1, %eax
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L15:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L16:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11539:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_115DecideConditionEPNS1_12JSHeapBrokerEPNS1_4NodeE, .-_ZN2v88internal8compiler12_GLOBAL__N_115DecideConditionEPNS1_12JSHeapBrokerEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler21CommonOperatorReducerD0Ev,"axG",@progbits,_ZN2v88internal8compiler21CommonOperatorReducerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler21CommonOperatorReducerD0Ev
	.type	_ZN2v88internal8compiler21CommonOperatorReducerD0Ev, @function
_ZN2v88internal8compiler21CommonOperatorReducerD0Ev:
.LFB13240:
	.cfi_startproc
	endbr64
	movl	$64, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE13240:
	.size	_ZN2v88internal8compiler21CommonOperatorReducerD0Ev, .-_ZN2v88internal8compiler21CommonOperatorReducerD0Ev
	.section	.text._ZN2v88internal8compiler21CommonOperatorReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_5GraphEPNS1_12JSHeapBrokerEPNS1_21CommonOperatorBuilderEPNS1_22MachineOperatorBuilderEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21CommonOperatorReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_5GraphEPNS1_12JSHeapBrokerEPNS1_21CommonOperatorBuilderEPNS1_22MachineOperatorBuilderEPNS0_4ZoneE
	.type	_ZN2v88internal8compiler21CommonOperatorReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_5GraphEPNS1_12JSHeapBrokerEPNS1_21CommonOperatorBuilderEPNS1_22MachineOperatorBuilderEPNS0_4ZoneE, @function
_ZN2v88internal8compiler21CommonOperatorReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_5GraphEPNS1_12JSHeapBrokerEPNS1_21CommonOperatorBuilderEPNS1_22MachineOperatorBuilderEPNS0_4ZoneE:
.LFB11545:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal8compiler21CommonOperatorReducerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%r8, %rdi
	movq	%rsi, 8(%rbx)
	movq	%rdx, 16(%rbx)
	movq	%rcx, 24(%rbx)
	movq	%r8, 32(%rbx)
	movq	%r9, 40(%rbx)
	movq	%rax, (%rbx)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4DeadEv@PLT
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rax, %rsi
	xorl	%edx, %edx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %xmm0
	movq	$1, 8(%rax)
	movhps	16(%rbp), %xmm0
	movups	%xmm0, 48(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11545:
	.size	_ZN2v88internal8compiler21CommonOperatorReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_5GraphEPNS1_12JSHeapBrokerEPNS1_21CommonOperatorBuilderEPNS1_22MachineOperatorBuilderEPNS0_4ZoneE, .-_ZN2v88internal8compiler21CommonOperatorReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_5GraphEPNS1_12JSHeapBrokerEPNS1_21CommonOperatorBuilderEPNS1_22MachineOperatorBuilderEPNS0_4ZoneE
	.globl	_ZN2v88internal8compiler21CommonOperatorReducerC1EPNS1_15AdvancedReducer6EditorEPNS1_5GraphEPNS1_12JSHeapBrokerEPNS1_21CommonOperatorBuilderEPNS1_22MachineOperatorBuilderEPNS0_4ZoneE
	.set	_ZN2v88internal8compiler21CommonOperatorReducerC1EPNS1_15AdvancedReducer6EditorEPNS1_5GraphEPNS1_12JSHeapBrokerEPNS1_21CommonOperatorBuilderEPNS1_22MachineOperatorBuilderEPNS0_4ZoneE,_ZN2v88internal8compiler21CommonOperatorReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_5GraphEPNS1_12JSHeapBrokerEPNS1_21CommonOperatorBuilderEPNS1_22MachineOperatorBuilderEPNS0_4ZoneE
	.section	.rodata._ZN2v88internal8compiler21CommonOperatorReducer12ReduceBranchEPNS1_4NodeE.str1.1,"aMS",@progbits,1
.LC3:
	.string	"unreachable code"
	.section	.text._ZN2v88internal8compiler21CommonOperatorReducer12ReduceBranchEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21CommonOperatorReducer12ReduceBranchEPNS1_4NodeE
	.type	_ZN2v88internal8compiler21CommonOperatorReducer12ReduceBranchEPNS1_4NodeE, @function
_ZN2v88internal8compiler21CommonOperatorReducer12ReduceBranchEPNS1_4NodeE:
.LFB11551:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	32(%rsi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	%rax, -56(%rbp)
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %r13
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L21
	movq	16(%r13), %r13
.L21:
	movq	0(%r13), %rax
	movzwl	16(%rax), %eax
	cmpl	$205, %eax
	je	.L30
	cmpl	$34, %eax
	jne	.L25
	movzbl	23(%r13), %eax
	leaq	32(%r13), %rbx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L26
	leaq	40(%r13), %rax
.L27:
	movq	24(%r14), %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_115DecideConditionEPNS1_12JSHeapBrokerEPNS1_4NodeE
	cmpb	$2, %al
	je	.L86
.L25:
	movq	24(%r14), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_115DecideConditionEPNS1_12JSHeapBrokerEPNS1_4NodeE
	testb	%al, %al
	je	.L33
	movzbl	23(%r12), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L47
	leaq	40(%r12), %rdx
.L48:
	movq	24(%r12), %rbx
	movq	(%rdx), %r13
	testq	%rbx, %rbx
	je	.L55
	cmpb	$2, %al
	je	.L54
	cmpb	$1, %al
	je	.L60
	.p2align 4,,10
	.p2align 3
.L56:
	movl	16(%rbx), %edx
	movl	%edx, %eax
	shrl	%eax
	andl	$1, %edx
	leaq	3(%rax,%rax,2), %rax
	leaq	(%rbx,%rax,8), %rsi
	movq	(%rsi), %rax
	jne	.L61
	movq	%rax, %rsi
	movq	(%rax), %rax
.L61:
	movzwl	16(%rax), %eax
	cmpw	$4, %ax
	je	.L63
	cmpw	$5, %ax
	jne	.L38
.L63:
	movq	8(%r14), %rdi
	movq	48(%r14), %rdx
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L56
.L55:
	movq	48(%r14), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	.cfi_restore_state
	movq	8(%r14), %rdi
	movq	48(%r14), %rdx
	movq	(%rdi), %rax
	call	*16(%rax)
.L53:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L55
.L54:
	movl	16(%rbx), %edx
	movl	%edx, %eax
	shrl	%eax
	andl	$1, %edx
	leaq	3(%rax,%rax,2), %rax
	leaq	(%rbx,%rax,8), %rsi
	movq	(%rsi), %rax
	jne	.L51
	movq	%rax, %rsi
	movq	(%rax), %rax
.L51:
	movzwl	16(%rax), %eax
	cmpw	$4, %ax
	je	.L52
	cmpw	$5, %ax
	jne	.L38
	movq	8(%r14), %rdi
	movq	%r13, %rdx
	movq	(%rdi), %rax
	call	*16(%rax)
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L33:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L86:
	.cfi_restore_state
	movzbl	23(%r13), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L28
	addq	$16, %rbx
.L29:
	movq	24(%r14), %rdi
	movq	(%rbx), %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_115DecideConditionEPNS1_12JSHeapBrokerEPNS1_4NodeE
	cmpb	$1, %al
	jne	.L25
	.p2align 4,,10
	.p2align 3
.L30:
	movq	24(%r12), %rbx
	testq	%rbx, %rbx
	jne	.L23
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L36:
	movq	32(%r14), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L24
.L23:
	movl	16(%rbx), %edx
	movl	%edx, %eax
	shrl	%eax
	andl	$1, %edx
	leaq	3(%rax,%rax,2), %rax
	leaq	(%rbx,%rax,8), %r15
	movq	(%r15), %rax
	jne	.L35
	movq	%rax, %r15
	movq	(%rax), %rax
.L35:
	movzwl	16(%rax), %eax
	cmpw	$4, %ax
	je	.L36
	cmpw	$5, %ax
	jne	.L38
	movq	32(%r14), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L23
.L24:
	movzbl	23(%r13), %eax
	movq	32(%r13), %r15
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L31
	movq	16(%r15), %r15
.L31:
	movzbl	23(%r12), %eax
	movq	32(%r12), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L40
	movq	%r12, %rsi
	cmpq	%r15, %rdi
	je	.L42
.L41:
	leaq	-24(%rsi), %r13
	testq	%rdi, %rdi
	je	.L44
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L44:
	movq	-56(%rbp), %rax
	movq	%r15, (%rax)
	testq	%r15, %r15
	je	.L42
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L42:
	movq	(%r12), %rdi
	movq	32(%r14), %r13
	call	_ZN2v88internal8compiler12BranchHintOfEPKNS1_8OperatorE@PLT
	movl	%eax, %esi
	cmpb	$1, %al
	je	.L66
	cmpb	$2, %al
	je	.L67
	testb	%al, %al
	je	.L45
.L38:
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L67:
	movl	$1, %esi
.L45:
	movq	%r13, %rdi
	movl	$1, %edx
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L58:
	.cfi_restore_state
	movq	8(%r14), %rdi
	movq	%r13, %rdx
	movq	(%rdi), %rax
	call	*16(%rax)
.L59:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L55
.L60:
	movl	16(%rbx), %edx
	movl	%edx, %eax
	shrl	%eax
	andl	$1, %edx
	leaq	3(%rax,%rax,2), %rax
	leaq	(%rbx,%rax,8), %rsi
	movq	(%rsi), %rax
	jne	.L57
	movq	%rax, %rsi
	movq	(%rax), %rax
.L57:
	movzwl	16(%rax), %eax
	cmpw	$4, %ax
	je	.L58
	cmpw	$5, %ax
	jne	.L38
	movq	8(%r14), %rdi
	movq	48(%r14), %rdx
	movq	(%rdi), %rax
	call	*16(%rax)
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L40:
	leaq	16(%rdi), %rax
	movq	%rax, -56(%rbp)
	movq	16(%rdi), %rax
	cmpq	%r15, %rax
	je	.L42
	movq	%rdi, %rsi
	movq	%rax, %rdi
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L66:
	movl	$2, %esi
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L26:
	movq	32(%r13), %rax
	addq	$24, %rax
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L47:
	movq	32(%r12), %rcx
	leaq	24(%rcx), %rdx
	jmp	.L48
.L28:
	movq	(%rbx), %rbx
	addq	$32, %rbx
	jmp	.L29
	.cfi_endproc
.LFE11551:
	.size	_ZN2v88internal8compiler21CommonOperatorReducer12ReduceBranchEPNS1_4NodeE, .-_ZN2v88internal8compiler21CommonOperatorReducer12ReduceBranchEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler21CommonOperatorReducer27ReduceDeoptimizeConditionalEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21CommonOperatorReducer27ReduceDeoptimizeConditionalEPNS1_4NodeE
	.type	_ZN2v88internal8compiler21CommonOperatorReducer27ReduceDeoptimizeConditionalEPNS1_4NodeE, @function
_ZN2v88internal8compiler21CommonOperatorReducer27ReduceDeoptimizeConditionalEPNS1_4NodeE:
.LFB11552:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzwl	16(%rdi), %r15d
	call	_ZN2v88internal8compiler22DeoptimizeParametersOfEPKNS1_8OperatorE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movdqu	(%rax), %xmm1
	movaps	%xmm1, -112(%rbp)
	movdqu	16(%rax), %xmm2
	movaps	%xmm2, -96(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r13
	movq	(%r14), %rax
	cmpw	$205, 16(%rax)
	jne	.L88
	movzbl	23(%r14), %eax
	movq	32(%r14), %rsi
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L98
.L89:
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties17ReplaceValueInputEPNS1_4NodeES4_i@PLT
	movq	32(%rbx), %rdi
	movzbl	-111(%rbp), %edx
	leaq	-104(%rbp), %rcx
	movzbl	-112(%rbp), %esi
	movl	$1, %r8d
	cmpw	$13, %r15w
	je	.L99
	call	_ZN2v88internal8compiler21CommonOperatorBuilder16DeoptimizeUnlessENS0_14DeoptimizeKindENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceENS1_13IsSafetyCheckE@PLT
	movq	%rax, %rsi
.L91:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	%r12, %rax
.L92:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L100
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L88:
	.cfi_restore_state
	movq	24(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_115DecideConditionEPNS1_12JSHeapBrokerEPNS1_4NodeE
	testb	%al, %al
	jne	.L93
	xorl	%eax, %eax
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L93:
	cmpb	$1, %al
	sete	%dl
	cmpw	$13, %r15w
	sete	%al
	cmpb	%al, %dl
	jne	.L94
	movq	8(%rbx), %rdi
	movq	48(%rbx), %rdx
	movq	%r13, %r8
	movq	%r12, %rsi
	movq	-128(%rbp), %rcx
	movq	(%rdi), %rax
	call	*32(%rax)
.L95:
	movq	48(%rbx), %rax
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L99:
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12DeoptimizeIfENS0_14DeoptimizeKindENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceENS1_13IsSafetyCheckE@PLT
	movq	%rax, %rsi
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L98:
	movq	16(%rsi), %rsi
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L94:
	movq	32(%rbx), %rdi
	movzbl	-111(%rbp), %edx
	leaq	-104(%rbp), %rcx
	movq	-136(%rbp), %xmm0
	movzbl	-112(%rbp), %esi
	movq	16(%rbx), %r12
	movhps	-128(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder10DeoptimizeENS0_14DeoptimizeKindENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceE@PLT
	movdqa	-128(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	leaq	-80(%rbp), %rcx
	movq	%rax, %rsi
	movl	$3, %edx
	movq	%r13, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	32(%rbx), %rsi
	movq	16(%rbx), %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14NodeProperties17MergeControlToEndEPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS1_4NodeE@PLT
	movq	16(%rbx), %rax
	movq	8(%rbx), %rdi
	movq	16(%rax), %rsi
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L95
.L100:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11552:
	.size	_ZN2v88internal8compiler21CommonOperatorReducer27ReduceDeoptimizeConditionalEPNS1_4NodeE, .-_ZN2v88internal8compiler21CommonOperatorReducer27ReduceDeoptimizeConditionalEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler21CommonOperatorReducer11ReduceMergeEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21CommonOperatorReducer11ReduceMergeEPNS1_4NodeE
	.type	_ZN2v88internal8compiler21CommonOperatorReducer11ReduceMergeEPNS1_4NodeE, @function
_ZN2v88internal8compiler21CommonOperatorReducer11ReduceMergeEPNS1_4NodeE:
.LFB11553:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %edi
	andl	$15, %edi
	movl	%edi, %eax
	cmpl	$15, %edi
	jne	.L103
	movq	32(%rsi), %rax
	movl	8(%rax), %eax
.L103:
	cmpl	$2, %eax
	je	.L133
.L104:
	xorl	%eax, %eax
.L108:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L133:
	.cfi_restore_state
	movq	24(%rsi), %rax
	testq	%rax, %rax
	jne	.L109
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L107:
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L105
.L109:
	movl	16(%rax), %ecx
	movl	%ecx, %edx
	shrl	%edx
	andl	$1, %ecx
	leaq	3(%rdx,%rdx,2), %rdx
	movq	(%rax,%rdx,8), %rdx
	jne	.L106
	movq	(%rdx), %rdx
.L106:
	movzwl	16(%rdx), %edx
	subl	$35, %edx
	cmpl	$1, %edx
	ja	.L107
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L105:
	movq	32(%rsi), %rax
	leaq	40(%rsi), %rdx
	cmpl	$15, %edi
	jne	.L111
	leaq	16(%rax), %rdx
	movq	16(%rax), %rax
	addq	$8, %rdx
.L111:
	movq	(%rdx), %rdx
	movq	(%rax), %rcx
	movq	(%rdx), %rdi
	movzwl	16(%rcx), %ecx
	movzwl	16(%rdi), %edi
	cmpw	$4, %cx
	je	.L112
	cmpw	$4, %di
	jne	.L104
	movl	%ecx, %edi
	movq	%rax, %rcx
	movq	%rdx, %rax
	movq	%rcx, %rdx
	.p2align 4,,10
	.p2align 3
.L112:
	cmpw	$5, %di
	jne	.L104
	movzbl	23(%rax), %ecx
	movq	32(%rax), %r13
	andl	$15, %ecx
	cmpl	$15, %ecx
	jne	.L113
	movq	16(%r13), %r13
.L113:
	movzbl	23(%rdx), %ecx
	movq	32(%rdx), %rdi
	andl	$15, %ecx
	cmpl	$15, %ecx
	jne	.L114
	movq	16(%rdi), %rdi
.L114:
	cmpq	%rdi, %r13
	jne	.L104
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L104
	movl	16(%rax), %edi
	movl	%edi, %ecx
	shrl	%ecx
	andl	$1, %edi
	leaq	3(%rcx,%rcx,2), %rcx
	leaq	(%rax,%rcx,8), %rcx
	jne	.L115
	movq	(%rcx), %rcx
.L115:
	cmpq	%rcx, %rsi
	jne	.L104
	cmpq	$0, (%rax)
	jne	.L104
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L104
	movl	16(%rdx), %ecx
	movl	%ecx, %eax
	shrl	%eax
	addq	$1, %rax
	imulq	$24, %rax, %rax
	addq	%rdx, %rax
	andb	$1, %cl
	jne	.L116
	movq	(%rax), %rax
.L116:
	cmpq	%rax, %rsi
	jne	.L104
	cmpq	$0, (%rdx)
	jne	.L104
	movzbl	23(%r13), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L117
	leaq	40(%r13), %rax
.L118:
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler4Node14TrimInputCountEi@PLT
	movq	32(%rbx), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4DeadEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	%r12, %rax
	jmp	.L108
.L117:
	movq	32(%r13), %rax
	addq	$24, %rax
	jmp	.L118
	.cfi_endproc
.LFE11553:
	.size	_ZN2v88internal8compiler21CommonOperatorReducer11ReduceMergeEPNS1_4NodeE, .-_ZN2v88internal8compiler21CommonOperatorReducer11ReduceMergeEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler21CommonOperatorReducer15ReduceEffectPhiEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21CommonOperatorReducer15ReduceEffectPhiEPNS1_4NodeE
	.type	_ZN2v88internal8compiler21CommonOperatorReducer15ReduceEffectPhiEPNS1_4NodeE, @function
_ZN2v88internal8compiler21CommonOperatorReducer15ReduceEffectPhiEPNS1_4NodeE:
.LFB11554:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	32(%rsi), %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %rbx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L136
	movl	8(%rbx), %eax
	leaq	16(%rbx), %r8
	movq	16(%rbx), %rbx
.L136:
	leal	-1(%rax), %r9d
	cmpl	$1, %r9d
	jle	.L137
	subl	$3, %eax
	leaq	8(%r8), %rdx
	leaq	16(%r8,%rax,8), %rax
	.p2align 4,,10
	.p2align 3
.L140:
	movq	(%rdx), %rcx
	cmpq	%rbx, %rcx
	je	.L138
	cmpq	%rcx, %rsi
	jne	.L150
.L138:
	addq	$8, %rdx
	cmpq	%rdx, %rax
	jne	.L140
.L137:
	movq	8(%rdi), %rdi
	movslq	%r9d, %r9
	movq	(%r8,%r9,8), %rsi
	movq	(%rdi), %rax
	call	*24(%rax)
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L150:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11554:
	.size	_ZN2v88internal8compiler21CommonOperatorReducer15ReduceEffectPhiEPNS1_4NodeE, .-_ZN2v88internal8compiler21CommonOperatorReducer15ReduceEffectPhiEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler21CommonOperatorReducer12ReduceReturnEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21CommonOperatorReducer12ReduceReturnEPNS1_4NodeE
	.type	_ZN2v88internal8compiler21CommonOperatorReducer12ReduceReturnEPNS1_4NodeE, @function
_ZN2v88internal8compiler21CommonOperatorReducer12ReduceReturnEPNS1_4NodeE:
.LFB11556:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	%rax, %r14
	movq	(%rax), %rax
	cmpw	$38, 16(%rax)
	je	.L181
	movq	(%rbx), %rdi
	call	_ZN2v88internal8compiler23ValueInputCountOfReturnEPKNS1_8OperatorE@PLT
	cmpl	$1, %eax
	je	.L156
.L170:
	xorl	%eax, %eax
.L155:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L182
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L181:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties18ReplaceEffectInputEPNS1_4NodeES4_i@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler21CommonOperatorReducer12ReduceReturnEPNS1_4NodeE
	testq	%rax, %rax
	cmove	%rbx, %rax
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L156:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movl	$1, %esi
	movq	%rbx, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r15
	movq	0(%r13), %rax
	cmpw	$35, 16(%rax)
	jne	.L170
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	cmpq	%rax, %r15
	jne	.L170
	movq	(%r15), %rax
	cmpw	$10, 16(%rax)
	jne	.L170
	movzbl	23(%r15), %eax
	leaq	32(%r15), %rdx
	movq	%rdx, -120(%rbp)
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L158
	movl	%eax, -112(%rbp)
.L159:
	leaq	32(%r13), %rax
	movq	%rax, -128(%rbp)
	movzbl	23(%r13), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L160
	movq	32(%r13), %rax
	addq	$16, %rax
	movq	%rax, -128(%rbp)
.L160:
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Node7OwnedByEPKS2_S4_@PLT
	testb	%al, %al
	je	.L161
	movq	24(%r13), %rdx
	testq	%rdx, %rdx
	je	.L161
	movl	16(%rdx), %ecx
	movl	%ecx, %eax
	shrl	%eax
	addq	$1, %rax
	imulq	$24, %rax, %rax
	addq	%rdx, %rax
	andb	$1, %cl
	jne	.L162
	movq	(%rax), %rax
.L162:
	cmpq	%rax, %rbx
	je	.L183
.L161:
	movq	(%r14), %rax
	cmpw	$36, 16(%rax)
	jne	.L170
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	cmpq	%rax, %r15
	jne	.L170
	movzbl	23(%r14), %eax
	leaq	32(%r14), %r11
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L166
	movq	32(%r14), %r11
	addq	$16, %r11
.L166:
	cmpl	$0, -112(%rbp)
	jle	.L169
	movl	-112(%rbp), %eax
	leaq	-96(%rbp), %r14
	xorl	%r9d, %r9d
	movq	%r15, -112(%rbp)
	movq	%r14, %rcx
	movq	%rbx, %r15
	movq	%r9, %rbx
	leal	-1(%rax), %r13d
	movq	%r13, %r14
	movq	%r11, %r13
.L168:
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rax
	movq	%rcx, -136(%rbp)
	movq	0(%r13,%rbx,8), %rdx
	movq	-104(%rbp), %r9
	movq	(%rsi,%rbx,8), %r8
	movq	(%rax,%rbx,8), %rax
	movq	16(%r12), %rdi
	movq	(%r15), %rsi
	movq	%rdx, -80(%rbp)
	movl	$4, %edx
	movq	%r8, -88(%rbp)
	xorl	%r8d, %r8d
	movq	%r9, -96(%rbp)
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	32(%r12), %rsi
	movq	16(%r12), %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14NodeProperties17MergeControlToEndEPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS1_4NodeE@PLT
	movq	%rbx, %rax
	addq	$1, %rbx
	movq	-136(%rbp), %rcx
	cmpq	%r14, %rax
	jne	.L168
	movq	-112(%rbp), %r15
.L169:
	movq	8(%r12), %rdi
	movq	48(%r12), %rdx
	movq	%r15, %rsi
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	48(%r12), %rax
	jmp	.L155
.L158:
	movq	32(%r15), %rax
	movl	8(%rax), %edx
	addq	$16, %rax
	movq	%rax, -120(%rbp)
	movl	%edx, -112(%rbp)
	jmp	.L159
.L182:
	call	__stack_chk_fail@PLT
.L183:
	cmpq	$0, (%rdx)
	jne	.L161
	xorl	%r11d, %r11d
	leaq	-96(%rbp), %rcx
	movq	%r11, %r13
	jmp	.L163
.L184:
	movq	-128(%rbp), %rdx
	movq	-120(%rbp), %rax
	xorl	%r8d, %r8d
	movq	%rcx, -136(%rbp)
	movq	-104(%rbp), %r10
	movq	16(%r12), %rdi
	movq	(%rdx,%r13,8), %rdx
	movq	(%rax,%r13,8), %rax
	movq	%r14, -80(%rbp)
	addq	$1, %r13
	movq	(%rbx), %rsi
	movq	%r10, -96(%rbp)
	movq	%rdx, -88(%rbp)
	movl	$4, %edx
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	32(%r12), %rsi
	movq	16(%r12), %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14NodeProperties17MergeControlToEndEPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS1_4NodeE@PLT
	movq	-136(%rbp), %rcx
.L163:
	cmpl	%r13d, -112(%rbp)
	jg	.L184
	jmp	.L169
	.cfi_endproc
.LFE11556:
	.size	_ZN2v88internal8compiler21CommonOperatorReducer12ReduceReturnEPNS1_4NodeE, .-_ZN2v88internal8compiler21CommonOperatorReducer12ReduceReturnEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler21CommonOperatorReducer12ReduceSwitchEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21CommonOperatorReducer12ReduceSwitchEPNS1_4NodeE
	.type	_ZN2v88internal8compiler21CommonOperatorReducer12ReduceSwitchEPNS1_4NodeE, @function
_ZN2v88internal8compiler21CommonOperatorReducer12ReduceSwitchEPNS1_4NodeE:
.LFB11558:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	40(%rsi), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %rdx
	movq	%rdi, -56(%rbp)
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L187
	leaq	16(%rdx), %rcx
	movq	16(%rdx), %rdx
	addq	$8, %rcx
.L187:
	movq	(%rdx), %rax
	xorl	%r9d, %r9d
	cmpw	$23, 16(%rax)
	jne	.L197
	movl	44(%rax), %r13d
	movq	(%r14), %rax
	movq	(%rcx), %rcx
	movslq	40(%rax), %rbx
	movq	-56(%rbp), %rax
	movq	%rcx, -64(%rbp)
	movq	56(%rax), %rdi
	leaq	0(,%rbx,8), %rax
	movq	%rax, -72(%rbp)
	movq	%rax, %rsi
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	subq	%r12, %rax
	cmpq	%rax, %rsi
	ja	.L200
	addq	%r12, %rsi
	movq	%rsi, 16(%rdi)
.L190:
	movq	%r14, %rdi
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r12, -80(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties25CollectControlProjectionsEPNS1_4NodeEPS4_m@PLT
	xorl	%r14d, %r14d
	subq	$1, %rbx
	jne	.L191
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L193:
	addq	$1, %r14
	addq	$8, %r12
	cmpq	%rbx, %r14
	je	.L194
.L191:
	movq	(%r12), %r15
	movq	(%r15), %rdi
	call	_ZN2v88internal8compiler19IfValueParametersOfEPKNS1_8OperatorE@PLT
	cmpl	(%rax), %r13d
	jne	.L193
	movq	-56(%rbp), %rax
	movq	-64(%rbp), %rdx
	movq	%r15, %rsi
	movq	8(%rax), %rdi
	movq	(%rdi), %rax
	call	*16(%rax)
.L192:
	movq	-56(%rbp), %rax
	movq	48(%rax), %r9
.L197:
	addq	$40, %rsp
	movq	%r9, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L194:
	.cfi_restore_state
	movq	-56(%rbp), %rax
	movq	-72(%rbp), %rcx
	movq	-64(%rbp), %rdx
	movq	8(%rax), %rdi
	movq	-80(%rbp), %rax
	movq	-8(%rax,%rcx), %rsi
	movq	(%rdi), %rax
	call	*16(%rax)
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L200:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L190
	.cfi_endproc
.LFE11558:
	.size	_ZN2v88internal8compiler21CommonOperatorReducer12ReduceSwitchEPNS1_4NodeE, .-_ZN2v88internal8compiler21CommonOperatorReducer12ReduceSwitchEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler21CommonOperatorReducer18ReduceStaticAssertEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21CommonOperatorReducer18ReduceStaticAssertEPNS1_4NodeE
	.type	_ZN2v88internal8compiler21CommonOperatorReducer18ReduceStaticAssertEPNS1_4NodeE, @function
_ZN2v88internal8compiler21CommonOperatorReducer18ReduceStaticAssertEPNS1_4NodeE:
.LFB11559:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	32(%rsi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L202
	movq	(%rdx), %rax
	movzwl	16(%rax), %edx
	cmpw	$23, %dx
	je	.L203
.L225:
	cmpw	$30, %dx
	je	.L204
.L205:
	xorl	%eax, %eax
.L209:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L226
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L204:
	.cfi_restore_state
	leaq	-64(%rbp), %r13
	movq	24(%r12), %rsi
	movq	48(%rax), %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L227
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12BooleanValueEv@PLT
	testb	%al, %al
	je	.L205
.L206:
	movq	8(%r12), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rbx, %rdx
	movq	%rbx, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%rbx, %rax
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L202:
	movq	16(%rdx), %rax
	movq	(%rax), %rax
	movzwl	16(%rax), %edx
	cmpw	$23, %dx
	jne	.L225
.L203:
	movl	44(%rax), %eax
	testl	%eax, %eax
	jne	.L206
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L227:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L226:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11559:
	.size	_ZN2v88internal8compiler21CommonOperatorReducer18ReduceStaticAssertEPNS1_4NodeE, .-_ZN2v88internal8compiler21CommonOperatorReducer18ReduceStaticAssertEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler21CommonOperatorReducer6ChangeEPNS1_4NodeEPKNS1_8OperatorES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21CommonOperatorReducer6ChangeEPNS1_4NodeEPKNS1_8OperatorES4_
	.type	_ZN2v88internal8compiler21CommonOperatorReducer6ChangeEPNS1_4NodeEPKNS1_8OperatorES4_, @function
_ZN2v88internal8compiler21CommonOperatorReducer6ChangeEPNS1_4NodeEPKNS1_8OperatorES4_:
.LFB11560:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L229
	leaq	32(%rsi), %rbx
	cmpq	%rdi, %rcx
	je	.L231
	leaq	-24(%rsi), %r15
	testq	%rdi, %rdi
	je	.L233
.L242:
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L233:
	movq	%r13, (%rbx)
	testq	%r13, %r13
	je	.L231
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L231:
	movq	%r12, %rdi
	movl	$1, %esi
	call	_ZN2v88internal8compiler4Node14TrimInputCountEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L229:
	.cfi_restore_state
	movq	16(%rdi), %rax
	leaq	16(%rdi), %rbx
	cmpq	%rax, %rcx
	je	.L231
	movq	%rdi, %rsi
	movq	%rax, %rdi
	leaq	-24(%rsi), %r15
	testq	%rdi, %rdi
	jne	.L242
	jmp	.L233
	.cfi_endproc
.LFE11560:
	.size	_ZN2v88internal8compiler21CommonOperatorReducer6ChangeEPNS1_4NodeEPKNS1_8OperatorES4_, .-_ZN2v88internal8compiler21CommonOperatorReducer6ChangeEPNS1_4NodeEPKNS1_8OperatorES4_
	.section	.text._ZN2v88internal8compiler21CommonOperatorReducer6ChangeEPNS1_4NodeEPKNS1_8OperatorES4_S4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21CommonOperatorReducer6ChangeEPNS1_4NodeEPKNS1_8OperatorES4_S4_
	.type	_ZN2v88internal8compiler21CommonOperatorReducer6ChangeEPNS1_4NodeEPKNS1_8OperatorES4_S4_, @function
_ZN2v88internal8compiler21CommonOperatorReducer6ChangeEPNS1_4NodeEPKNS1_8OperatorES4_S4_:
.LFB11561:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$24, %rsp
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L244
	movq	%rbx, %rax
	cmpq	%rdi, %rcx
	je	.L246
	subq	$24, %rsi
	testq	%rdi, %rdi
	je	.L248
.L269:
	movq	%rax, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-64(%rbp), %rax
	movq	-56(%rbp), %rsi
.L248:
	movq	%r13, (%rax)
	testq	%r13, %r13
	je	.L249
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L249:
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L268
.L246:
	movq	8(%rbx), %r8
	cmpq	%r8, %r14
	je	.L252
	addq	$8, %rbx
	movq	%r12, %rdi
.L251:
	leaq	-48(%rdi), %r13
	testq	%r8, %r8
	je	.L253
	movq	%r13, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L253:
	movq	%r14, (%rbx)
	testq	%r14, %r14
	je	.L252
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L252:
	movq	%r12, %rdi
	movl	$2, %esi
	call	_ZN2v88internal8compiler4Node14TrimInputCountEi@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L268:
	.cfi_restore_state
	movq	32(%r12), %rdi
	leaq	16(%rdi), %rax
.L247:
	movq	8(%rax), %r8
	cmpq	%r14, %r8
	je	.L252
	leaq	8(%rax), %rbx
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L244:
	movq	16(%rdi), %rdx
	leaq	16(%rdi), %rax
	cmpq	%rdx, %rcx
	je	.L247
	movq	%rdi, %rsi
	movq	%rdx, %rdi
	subq	$24, %rsi
	testq	%rdi, %rdi
	jne	.L269
	jmp	.L248
	.cfi_endproc
.LFE11561:
	.size	_ZN2v88internal8compiler21CommonOperatorReducer6ChangeEPNS1_4NodeEPKNS1_8OperatorES4_S4_, .-_ZN2v88internal8compiler21CommonOperatorReducer6ChangeEPNS1_4NodeEPKNS1_8OperatorES4_S4_
	.section	.text._ZN2v88internal8compiler12BinopMatcherINS1_12FloatMatcherIfLNS1_8IrOpcode5ValueE25EEES6_EC2EPNS1_4NodeE,"axG",@progbits,_ZN2v88internal8compiler12BinopMatcherINS1_12FloatMatcherIfLNS1_8IrOpcode5ValueE25EEES6_EC5EPNS1_4NodeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler12BinopMatcherINS1_12FloatMatcherIfLNS1_8IrOpcode5ValueE25EEES6_EC2EPNS1_4NodeE
	.type	_ZN2v88internal8compiler12BinopMatcherINS1_12FloatMatcherIfLNS1_8IrOpcode5ValueE25EEES6_EC2EPNS1_4NodeE, @function
_ZN2v88internal8compiler12BinopMatcherINS1_12FloatMatcherIfLNS1_8IrOpcode5ValueE25EEES6_EC2EPNS1_4NodeE:
.LFB12384:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	leaq	32(%rsi), %r12
	pushq	%rbx
	movq	%r12, %rdx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	%rsi, (%rdi)
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L271
	movq	32(%rsi), %rax
	leaq	16(%rax), %rdx
.L271:
	movq	(%rdx), %rax
	movl	$0x00000000, 16(%rbx)
	movq	%rax, 8(%rbx)
	movq	(%rax), %rdx
	movzwl	16(%rdx), %eax
	cmpw	$25, %ax
	sete	%cl
	movb	%cl, 20(%rbx)
	jne	.L272
	movss	44(%rdx), %xmm0
	movss	%xmm0, 16(%rbx)
.L272:
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L273
	leaq	8(%r12), %rax
.L274:
	movq	(%rax), %r13
	movl	$0x00000000, 32(%rbx)
	movq	%r13, 24(%rbx)
	movq	0(%r13), %rdx
	movzwl	16(%rdx), %eax
	cmpw	$25, %ax
	sete	%dil
	movb	%dil, 36(%rbx)
	jne	.L275
	movss	44(%rdx), %xmm0
	movss	%xmm0, 32(%rbx)
.L270:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L275:
	.cfi_restore_state
	movq	(%rsi), %rax
	testb	$1, 18(%rax)
	je	.L270
	testb	%cl, %cl
	je	.L270
	movdqu	8(%rbx), %xmm1
	movl	32(%rbx), %edx
	movb	%dil, 20(%rbx)
	movq	8(%rbx), %rax
	movq	(%r12), %rdi
	movq	%r13, 8(%rbx)
	movaps	%xmm1, -48(%rbp)
	movl	%edx, 16(%rbx)
	movl	-40(%rbp), %edx
	movq	%rax, 24(%rbx)
	movl	%edx, 32(%rbx)
	movzbl	-36(%rbp), %edx
	movb	%dl, 36(%rbx)
	movzbl	23(%rsi), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L279
	cmpq	%rdi, %r13
	je	.L305
.L280:
	leaq	-24(%rsi), %r14
	testq	%rdi, %rdi
	je	.L283
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L283:
	movq	%r13, (%r12)
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movq	(%rbx), %rsi
	movq	24(%rbx), %r13
	movzbl	23(%rsi), %eax
	leaq	32(%rsi), %r12
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L288
.L287:
	movq	8(%r12), %rdi
	addq	$8, %r12
	cmpq	%rdi, %r13
	je	.L270
.L285:
	leaq	-48(%rsi), %r14
	testq	%rdi, %rdi
	je	.L286
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L286:
	movq	%r13, (%r12)
	testq	%r13, %r13
	je	.L270
	addq	$16, %rsp
	movq	%r14, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	.p2align 4,,10
	.p2align 3
.L273:
	.cfi_restore_state
	movq	(%r12), %rax
	addq	$24, %rax
	jmp	.L274
.L282:
	movq	%rax, %r13
.L288:
	movq	(%r12), %rsi
	movq	24(%rsi), %rdi
	cmpq	%rdi, %r13
	je	.L270
	leaq	24(%rsi), %r12
	jmp	.L285
	.p2align 4,,10
	.p2align 3
.L305:
	movq	%rax, %r13
	jmp	.L287
	.p2align 4,,10
	.p2align 3
.L279:
	movq	16(%rdi), %rdx
	leaq	16(%rdi), %rcx
	cmpq	%rdx, %r13
	je	.L282
	movq	%rdi, %rsi
	movq	%rcx, %r12
	movq	%rdx, %rdi
	jmp	.L280
	.cfi_endproc
.LFE12384:
	.size	_ZN2v88internal8compiler12BinopMatcherINS1_12FloatMatcherIfLNS1_8IrOpcode5ValueE25EEES6_EC2EPNS1_4NodeE, .-_ZN2v88internal8compiler12BinopMatcherINS1_12FloatMatcherIfLNS1_8IrOpcode5ValueE25EEES6_EC2EPNS1_4NodeE
	.weak	_ZN2v88internal8compiler12BinopMatcherINS1_12FloatMatcherIfLNS1_8IrOpcode5ValueE25EEES6_EC1EPNS1_4NodeE
	.set	_ZN2v88internal8compiler12BinopMatcherINS1_12FloatMatcherIfLNS1_8IrOpcode5ValueE25EEES6_EC1EPNS1_4NodeE,_ZN2v88internal8compiler12BinopMatcherINS1_12FloatMatcherIfLNS1_8IrOpcode5ValueE25EEES6_EC2EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler12BinopMatcherINS1_12FloatMatcherIdLNS1_8IrOpcode5ValueE26EEES6_EC2EPNS1_4NodeE,"axG",@progbits,_ZN2v88internal8compiler12BinopMatcherINS1_12FloatMatcherIdLNS1_8IrOpcode5ValueE26EEES6_EC5EPNS1_4NodeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler12BinopMatcherINS1_12FloatMatcherIdLNS1_8IrOpcode5ValueE26EEES6_EC2EPNS1_4NodeE
	.type	_ZN2v88internal8compiler12BinopMatcherINS1_12FloatMatcherIdLNS1_8IrOpcode5ValueE26EEES6_EC2EPNS1_4NodeE, @function
_ZN2v88internal8compiler12BinopMatcherINS1_12FloatMatcherIdLNS1_8IrOpcode5ValueE26EEES6_EC2EPNS1_4NodeE:
.LFB12391:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	leaq	32(%rsi), %r12
	pushq	%rbx
	movq	%r12, %rdx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	%rsi, (%rdi)
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L307
	movq	32(%rsi), %rax
	leaq	16(%rax), %rdx
.L307:
	movq	(%rdx), %rax
	movq	$0x000000000, 16(%rbx)
	movq	%rax, 8(%rbx)
	movq	(%rax), %rdx
	movzwl	16(%rdx), %eax
	cmpw	$26, %ax
	sete	%cl
	movb	%cl, 24(%rbx)
	jne	.L308
	movsd	48(%rdx), %xmm0
	movsd	%xmm0, 16(%rbx)
.L308:
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L309
	leaq	8(%r12), %rax
.L310:
	movq	(%rax), %r13
	movq	$0x000000000, 40(%rbx)
	movq	%r13, 32(%rbx)
	movq	0(%r13), %rdx
	movzwl	16(%rdx), %eax
	cmpw	$26, %ax
	sete	%dil
	movb	%dil, 48(%rbx)
	jne	.L311
	movsd	48(%rdx), %xmm0
	movsd	%xmm0, 40(%rbx)
.L306:
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L311:
	.cfi_restore_state
	movq	(%rsi), %rax
	testb	$1, 18(%rax)
	je	.L306
	testb	%cl, %cl
	je	.L306
	movq	24(%rbx), %rax
	movdqu	8(%rbx), %xmm0
	movb	%dil, 24(%rbx)
	movdqu	32(%rbx), %xmm1
	movq	(%r12), %rdi
	movq	%rax, -48(%rbp)
	movb	%al, 48(%rbx)
	movzbl	23(%rsi), %eax
	movaps	%xmm0, -64(%rbp)
	andl	$15, %eax
	movups	%xmm1, 8(%rbx)
	movups	%xmm0, 32(%rbx)
	cmpl	$15, %eax
	je	.L315
	cmpq	%rdi, %r13
	je	.L341
.L316:
	leaq	-24(%rsi), %r14
	testq	%rdi, %rdi
	je	.L319
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L319:
	movq	%r13, (%r12)
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movq	(%rbx), %rsi
	movq	32(%rbx), %r13
	movzbl	23(%rsi), %eax
	leaq	32(%rsi), %r12
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L324
.L323:
	movq	8(%r12), %rdi
	addq	$8, %r12
	cmpq	%rdi, %r13
	je	.L306
.L321:
	leaq	-48(%rsi), %r14
	testq	%rdi, %rdi
	je	.L322
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L322:
	movq	%r13, (%r12)
	testq	%r13, %r13
	je	.L306
	addq	$32, %rsp
	movq	%r14, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	.p2align 4,,10
	.p2align 3
.L309:
	.cfi_restore_state
	movq	(%r12), %rax
	addq	$24, %rax
	jmp	.L310
.L318:
	movq	32(%rbx), %r13
.L324:
	movq	(%r12), %rsi
	movq	24(%rsi), %rdi
	cmpq	%rdi, %r13
	je	.L306
	leaq	24(%rsi), %r12
	jmp	.L321
	.p2align 4,,10
	.p2align 3
.L341:
	movq	32(%rbx), %r13
	jmp	.L323
	.p2align 4,,10
	.p2align 3
.L315:
	movq	16(%rdi), %rax
	leaq	16(%rdi), %rdx
	cmpq	%rax, %r13
	je	.L318
	movq	%rdi, %rsi
	movq	%rdx, %r12
	movq	%rax, %rdi
	jmp	.L316
	.cfi_endproc
.LFE12391:
	.size	_ZN2v88internal8compiler12BinopMatcherINS1_12FloatMatcherIdLNS1_8IrOpcode5ValueE26EEES6_EC2EPNS1_4NodeE, .-_ZN2v88internal8compiler12BinopMatcherINS1_12FloatMatcherIdLNS1_8IrOpcode5ValueE26EEES6_EC2EPNS1_4NodeE
	.weak	_ZN2v88internal8compiler12BinopMatcherINS1_12FloatMatcherIdLNS1_8IrOpcode5ValueE26EEES6_EC1EPNS1_4NodeE
	.set	_ZN2v88internal8compiler12BinopMatcherINS1_12FloatMatcherIdLNS1_8IrOpcode5ValueE26EEES6_EC1EPNS1_4NodeE,_ZN2v88internal8compiler12BinopMatcherINS1_12FloatMatcherIdLNS1_8IrOpcode5ValueE26EEES6_EC2EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler21CommonOperatorReducer9ReducePhiEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21CommonOperatorReducer9ReducePhiEPNS1_4NodeE
	.type	_ZN2v88internal8compiler21CommonOperatorReducer9ReducePhiEPNS1_4NodeE, @function
_ZN2v88internal8compiler21CommonOperatorReducer9ReducePhiEPNS1_4NodeE:
.LFB11555:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	32(%rsi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movzbl	23(%rsi), %ebx
	movq	32(%rsi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	andl	$15, %ebx
	cmpl	$15, %ebx
	jne	.L344
	movl	8(%r12), %ebx
	leaq	16(%r12), %r15
	movq	16(%r12), %r12
.L344:
	subl	$1, %ebx
	movslq	%ebx, %rax
	movq	(%r15,%rax,8), %r8
	cmpl	$2, %ebx
	je	.L384
	cmpl	$1, %ebx
	jle	.L365
.L369:
	movl	$1, %edx
	.p2align 4,,10
	.p2align 3
.L368:
	movq	(%r15,%rdx,8), %rcx
	cmpq	%r12, %rcx
	je	.L376
	cmpq	%rcx, %r13
	jne	.L366
.L376:
	addq	$1, %rdx
	cmpl	%edx, %ebx
	jg	.L368
.L365:
	movq	8(%r14), %rdi
	movq	%r8, %rsi
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	%r12, %rax
.L352:
	movq	-56(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L385
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L366:
	.cfi_restore_state
	xorl	%eax, %eax
	jmp	.L352
	.p2align 4,,10
	.p2align 3
.L384:
	movzbl	23(%r8), %eax
	movq	8(%r15), %r9
	leaq	32(%r8), %rcx
	movq	32(%r8), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L346
	leaq	16(%rdx), %rcx
	movq	16(%rdx), %rdx
.L346:
	movq	8(%rcx), %rax
	movq	(%rdx), %rcx
	movzwl	16(%rcx), %esi
	movq	(%rax), %rcx
	movzwl	16(%rcx), %ecx
	cmpw	$4, %si
	je	.L371
	cmpw	$4, %cx
	jne	.L369
	movl	%esi, %ecx
	movq	%rdx, %rsi
	movq	%r9, %r10
	movq	%rax, %rdx
	movq	%r12, %r9
	movq	%rsi, %rax
	jmp	.L347
	.p2align 4,,10
	.p2align 3
.L371:
	movq	%r12, %r10
.L347:
	cmpw	$5, %cx
	jne	.L369
	movq	32(%rdx), %rcx
	movzbl	23(%rdx), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L349
	movq	16(%rcx), %rcx
.L349:
	movq	32(%rax), %rdx
	movzbl	23(%rax), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L350
	movq	16(%rdx), %rdx
.L350:
	cmpq	%rdx, %rcx
	jne	.L369
	movq	(%rcx), %rax
	cmpw	$2, 16(%rax)
	jne	.L366
	movzbl	23(%rcx), %eax
	movq	32(%rcx), %rsi
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L353
	movq	16(%rsi), %rsi
.L353:
	movq	(%rsi), %rax
	movzwl	16(%rax), %eax
	cmpl	$345, %eax
	je	.L386
	cmpl	$348, %eax
	jne	.L369
	leaq	-176(%rbp), %rdi
	movq	%r8, -184(%rbp)
	movq	%r10, -200(%rbp)
	movq	%r9, -192(%rbp)
	call	_ZN2v88internal8compiler12BinopMatcherINS1_12FloatMatcherIdLNS1_8IrOpcode5ValueE26EEES6_EC1EPNS1_4NodeE
	cmpb	$0, -152(%rbp)
	movq	-184(%rbp), %r8
	je	.L360
	pxor	%xmm3, %xmm3
	ucomisd	-160(%rbp), %xmm3
	jp	.L360
	jne	.L360
	movq	-200(%rbp), %r10
	cmpq	%r10, -144(%rbp)
	movq	-192(%rbp), %r9
	je	.L387
.L360:
	movq	(%r15), %r12
	jmp	.L369
.L386:
	leaq	-176(%rbp), %rdi
	movq	%r8, -184(%rbp)
	movq	%r10, -200(%rbp)
	movq	%r9, -192(%rbp)
	call	_ZN2v88internal8compiler12BinopMatcherINS1_12FloatMatcherIfLNS1_8IrOpcode5ValueE25EEES6_EC1EPNS1_4NodeE
	cmpb	$0, -156(%rbp)
	movq	-184(%rbp), %r8
	je	.L360
	pxor	%xmm1, %xmm1
	ucomiss	-160(%rbp), %xmm1
	jp	.L360
	jne	.L360
	movq	-200(%rbp), %r10
	cmpq	%r10, -152(%rbp)
	movq	-192(%rbp), %r9
	jne	.L360
	movq	(%r9), %rax
	cmpw	$351, 16(%rax)
	jne	.L360
	leaq	-112(%rbp), %rdi
	movq	%r9, %rsi
	movq	%r10, -192(%rbp)
	call	_ZN2v88internal8compiler12BinopMatcherINS1_12FloatMatcherIfLNS1_8IrOpcode5ValueE25EEES6_EC1EPNS1_4NodeE
	cmpb	$0, -92(%rbp)
	movq	-184(%rbp), %r8
	je	.L360
	movss	-96(%rbp), %xmm0
	pxor	%xmm2, %xmm2
	ucomiss	%xmm2, %xmm0
	jp	.L360
	jne	.L360
	movd	%xmm0, %eax
	testl	%eax, %eax
	js	.L360
	movq	-192(%rbp), %r10
	cmpq	%r10, -88(%rbp)
	jne	.L360
	movq	8(%r14), %rdi
	movq	%r10, -184(%rbp)
	movq	%r8, %rsi
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	40(%r14), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder10Float32AbsEv@PLT
.L383:
	movq	-184(%rbp), %r10
	movq	%rax, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%r10, %rcx
	call	_ZN2v88internal8compiler21CommonOperatorReducer6ChangeEPNS1_4NodeEPKNS1_8OperatorES4_
	jmp	.L352
.L387:
	movq	(%r9), %rax
	cmpw	$367, 16(%rax)
	jne	.L360
	leaq	-112(%rbp), %rdi
	movq	%r9, %rsi
	movq	%r10, -192(%rbp)
	call	_ZN2v88internal8compiler12BinopMatcherINS1_12FloatMatcherIdLNS1_8IrOpcode5ValueE26EEES6_EC1EPNS1_4NodeE
	cmpb	$0, -88(%rbp)
	movq	-184(%rbp), %r8
	je	.L360
	movsd	-96(%rbp), %xmm0
	pxor	%xmm4, %xmm4
	ucomisd	%xmm4, %xmm0
	jp	.L360
	jne	.L360
	movmskpd	%xmm0, %eax
	testb	$1, %al
	jne	.L360
	movq	-192(%rbp), %r10
	cmpq	%r10, -80(%rbp)
	jne	.L360
	movq	8(%r14), %rdi
	movq	%r10, -184(%rbp)
	movq	%r8, %rsi
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	40(%r14), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder10Float64AbsEv@PLT
	jmp	.L383
.L385:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11555:
	.size	_ZN2v88internal8compiler21CommonOperatorReducer9ReducePhiEPNS1_4NodeE, .-_ZN2v88internal8compiler21CommonOperatorReducer9ReducePhiEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler21CommonOperatorReducer12ReduceSelectEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21CommonOperatorReducer12ReduceSelectEPNS1_4NodeE
	.type	_ZN2v88internal8compiler21CommonOperatorReducer12ReduceSelectEPNS1_4NodeE, @function
_ZN2v88internal8compiler21CommonOperatorReducer12ReduceSelectEPNS1_4NodeE:
.LFB11557:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	32(%rsi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L389
	movq	40(%rsi), %r13
	leaq	48(%rsi), %rax
.L390:
	movq	(%rax), %r15
	cmpq	%r13, %r15
	je	.L398
	movq	(%rbx), %rax
	movzwl	16(%rax), %edx
	cmpw	$23, %dx
	je	.L393
	cmpw	$30, %dx
	je	.L394
	cmpw	$345, %dx
	je	.L395
	cmpw	$348, %dx
	jne	.L397
	movzbl	23(%rbx), %edx
	movq	32(%rbx), %rcx
	leaq	32(%rbx), %r8
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L422
	movq	(%rcx), %r9
	movzwl	16(%r9), %esi
	cmpw	$26, %si
	sete	%dil
	je	.L423
	movb	$0, -120(%rbp)
	pxor	%xmm0, %xmm0
.L424:
	leaq	8(%r8), %rsi
	movq	%rcx, %r10
	movq	%r8, %r11
.L427:
	movq	(%rsi), %r9
	movq	(%r9), %rsi
	cmpw	$26, 16(%rsi)
	je	.L428
	testb	$1, 18(%rax)
	je	.L428
	cmpb	$0, -120(%rbp)
	jne	.L482
.L428:
	testb	%dil, %dil
	je	.L397
	pxor	%xmm2, %xmm2
	ucomisd	%xmm2, %xmm0
	jp	.L397
	jne	.L397
	cmpq	%r13, %r9
	jne	.L397
	movq	(%r15), %rax
	cmpw	$367, 16(%rax)
	jne	.L397
	leaq	-112(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler12BinopMatcherINS1_12FloatMatcherIdLNS1_8IrOpcode5ValueE26EEES6_EC1EPNS1_4NodeE
	cmpb	$0, -88(%rbp)
	je	.L397
	movsd	-96(%rbp), %xmm0
	pxor	%xmm4, %xmm4
	ucomisd	%xmm4, %xmm0
	jp	.L397
	jne	.L397
	movmskpd	%xmm0, %eax
	testb	$1, %al
	jne	.L397
	cmpq	%r13, -80(%rbp)
	jne	.L397
	movq	40(%r14), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder10Float64AbsEv@PLT
	jmp	.L475
	.p2align 4,,10
	.p2align 3
.L393:
	movl	44(%rax), %eax
	testl	%eax, %eax
	je	.L399
	.p2align 4,,10
	.p2align 3
.L398:
	movq	%r13, %rax
.L392:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L483
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L389:
	.cfi_restore_state
	leaq	16(%rbx), %rax
	movq	8(%rax), %r13
	movq	16(%rbx), %rbx
	addq	$16, %rax
	jmp	.L390
	.p2align 4,,10
	.p2align 3
.L394:
	leaq	-112(%rbp), %r12
	movq	24(%r14), %rsi
	movq	48(%rax), %rdx
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L484
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12BooleanValueEv@PLT
	testb	%al, %al
	jne	.L398
.L399:
	movq	%r15, %rax
	jmp	.L392
.L485:
	cmpl	$15, %edx
	je	.L409
	movq	%rbx, %rcx
	cmpq	%r10, %r9
	je	.L431
.L410:
	leaq	-24(%rcx), %r12
	movq	%r10, %rdi
	movq	%r10, -120(%rbp)
	movq	%r12, %rsi
	movq	%r8, -144(%rbp)
	movq	%r9, -136(%rbp)
	movq	%r11, -128(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-136(%rbp), %r9
	movq	-128(%rbp), %r11
	movq	%r12, %rsi
	movq	%r9, (%r11)
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movzbl	23(%rbx), %eax
	movq	-120(%rbp), %r10
	movq	-144(%rbp), %r8
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L432
.L431:
	movq	8(%r8), %rdi
	leaq	8(%r8), %r13
	cmpq	%r10, %rdi
	je	.L397
.L434:
	leaq	-48(%rbx), %r12
	testq	%rdi, %rdi
	je	.L435
	movq	%r12, %rsi
	movq	%r10, -120(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-120(%rbp), %r10
.L435:
	movq	%r10, 0(%r13)
	movq	%r12, %rsi
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	.p2align 4,,10
	.p2align 3
.L397:
	xorl	%eax, %eax
	jmp	.L392
	.p2align 4,,10
	.p2align 3
.L395:
	movzbl	23(%rbx), %edx
	movq	32(%rbx), %rcx
	leaq	32(%rbx), %r8
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L402
	movq	(%rcx), %r9
	movzwl	16(%r9), %esi
	cmpw	$25, %si
	sete	%dil
	je	.L403
	movb	$0, -120(%rbp)
	pxor	%xmm0, %xmm0
.L404:
	leaq	8(%r8), %rsi
	movq	%rcx, %r10
	movq	%r8, %r11
.L407:
	movq	(%rsi), %r9
	movq	(%r9), %rsi
	cmpw	$25, 16(%rsi)
	je	.L408
	testb	$1, 18(%rax)
	je	.L408
	cmpb	$0, -120(%rbp)
	jne	.L485
.L408:
	testb	%dil, %dil
	je	.L397
	pxor	%xmm1, %xmm1
	ucomiss	%xmm1, %xmm0
	jp	.L397
	jne	.L397
	cmpq	%r13, %r9
	jne	.L397
	movq	(%r15), %rax
	cmpw	$351, 16(%rax)
	jne	.L397
	leaq	-112(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler12BinopMatcherINS1_12FloatMatcherIfLNS1_8IrOpcode5ValueE25EEES6_EC1EPNS1_4NodeE
	cmpb	$0, -92(%rbp)
	je	.L397
	movss	-96(%rbp), %xmm0
	pxor	%xmm3, %xmm3
	ucomiss	%xmm3, %xmm0
	jp	.L397
	jne	.L397
	movd	%xmm0, %eax
	testl	%eax, %eax
	js	.L397
	cmpq	%r13, -88(%rbp)
	jne	.L397
	movq	40(%r14), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder10Float32AbsEv@PLT
.L475:
	movq	%rax, %rdx
	movq	%r13, %rcx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21CommonOperatorReducer6ChangeEPNS1_4NodeEPKNS1_8OperatorES4_
	jmp	.L392
	.p2align 4,,10
	.p2align 3
.L422:
	movq	16(%rcx), %r10
	leaq	16(%rcx), %r11
	movq	(%r10), %r9
	movzwl	16(%r9), %esi
	cmpw	$26, %si
	sete	%dil
	je	.L425
	movb	$0, -120(%rbp)
	pxor	%xmm0, %xmm0
.L426:
	leaq	8(%r11), %rsi
	jmp	.L427
	.p2align 4,,10
	.p2align 3
.L402:
	movq	16(%rcx), %r10
	leaq	16(%rcx), %r11
	movq	(%r10), %r9
	movzwl	16(%r9), %esi
	cmpw	$25, %si
	sete	%dil
	je	.L405
	movb	$0, -120(%rbp)
	pxor	%xmm0, %xmm0
.L406:
	leaq	8(%r11), %rsi
	jmp	.L407
	.p2align 4,,10
	.p2align 3
.L423:
	movb	$1, -120(%rbp)
	movsd	48(%r9), %xmm0
	jmp	.L424
	.p2align 4,,10
	.p2align 3
.L403:
	movb	$1, -120(%rbp)
	movss	44(%r9), %xmm0
	jmp	.L404
	.p2align 4,,10
	.p2align 3
.L484:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L405:
	movb	$1, -120(%rbp)
	movss	44(%r9), %xmm0
	jmp	.L406
.L425:
	movb	$1, -120(%rbp)
	movsd	48(%r9), %xmm0
	jmp	.L426
.L482:
	cmpl	$15, %edx
	je	.L429
	movq	%rbx, %rcx
	cmpq	%r10, %r9
	je	.L431
.L430:
	leaq	-24(%rcx), %r12
	movq	%r10, %rdi
	movq	%r8, -128(%rbp)
	movq	%r12, %rsi
	movq	%r10, -120(%rbp)
	movq	%r9, -144(%rbp)
	movq	%r11, -136(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-144(%rbp), %r9
	movq	-136(%rbp), %r11
	movq	%r12, %rsi
	movq	%r9, (%r11)
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movzbl	23(%rbx), %eax
	movq	-120(%rbp), %r10
	movq	-128(%rbp), %r8
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L431
.L432:
	movq	(%r8), %rbx
	movq	24(%rbx), %rdi
	cmpq	%r10, %rdi
	je	.L397
	leaq	24(%rbx), %r13
	jmp	.L434
.L409:
	cmpq	%r10, %r9
	jne	.L410
	jmp	.L432
.L429:
	cmpq	%r10, %r9
	jne	.L430
	jmp	.L432
.L483:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11557:
	.size	_ZN2v88internal8compiler21CommonOperatorReducer12ReduceSelectEPNS1_4NodeE, .-_ZN2v88internal8compiler21CommonOperatorReducer12ReduceSelectEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler21CommonOperatorReducer6ReduceEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21CommonOperatorReducer6ReduceEPNS1_4NodeE
	.type	_ZN2v88internal8compiler21CommonOperatorReducer6ReduceEPNS1_4NodeE, @function
_ZN2v88internal8compiler21CommonOperatorReducer6ReduceEPNS1_4NodeE:
.LFB11547:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	cmpw	$62, 16(%rax)
	ja	.L503
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.L489(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movzwl	16(%rax), %eax
	movq	%rsi, %r12
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler21CommonOperatorReducer6ReduceEPNS1_4NodeE,"a",@progbits
	.align 4
	.align 4
.L489:
	.long	.L487-.L489
	.long	.L487-.L489
	.long	.L497-.L489
	.long	.L496-.L489
	.long	.L487-.L489
	.long	.L487-.L489
	.long	.L487-.L489
	.long	.L487-.L489
	.long	.L487-.L489
	.long	.L487-.L489
	.long	.L495-.L489
	.long	.L487-.L489
	.long	.L494-.L489
	.long	.L494-.L489
	.long	.L487-.L489
	.long	.L487-.L489
	.long	.L493-.L489
	.long	.L487-.L489
	.long	.L487-.L489
	.long	.L487-.L489
	.long	.L487-.L489
	.long	.L487-.L489
	.long	.L487-.L489
	.long	.L487-.L489
	.long	.L487-.L489
	.long	.L487-.L489
	.long	.L487-.L489
	.long	.L487-.L489
	.long	.L487-.L489
	.long	.L487-.L489
	.long	.L487-.L489
	.long	.L487-.L489
	.long	.L487-.L489
	.long	.L487-.L489
	.long	.L492-.L489
	.long	.L491-.L489
	.long	.L490-.L489
	.long	.L487-.L489
	.long	.L487-.L489
	.long	.L487-.L489
	.long	.L487-.L489
	.long	.L487-.L489
	.long	.L487-.L489
	.long	.L487-.L489
	.long	.L487-.L489
	.long	.L487-.L489
	.long	.L487-.L489
	.long	.L487-.L489
	.long	.L487-.L489
	.long	.L487-.L489
	.long	.L487-.L489
	.long	.L487-.L489
	.long	.L487-.L489
	.long	.L487-.L489
	.long	.L487-.L489
	.long	.L487-.L489
	.long	.L487-.L489
	.long	.L487-.L489
	.long	.L487-.L489
	.long	.L487-.L489
	.long	.L487-.L489
	.long	.L487-.L489
	.long	.L488-.L489
	.section	.text._ZN2v88internal8compiler21CommonOperatorReducer6ReduceEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L487:
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L494:
	.cfi_restore_state
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler21CommonOperatorReducer27ReduceDeoptimizeConditionalEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L495:
	.cfi_restore_state
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler21CommonOperatorReducer11ReduceMergeEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L493:
	.cfi_restore_state
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler21CommonOperatorReducer12ReduceReturnEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L496:
	.cfi_restore_state
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler21CommonOperatorReducer12ReduceSwitchEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L488:
	.cfi_restore_state
	movzbl	23(%r12), %eax
	movq	32(%rsi), %rsi
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L498
	movq	16(%rsi), %rsi
.L498:
	movq	24(%r13), %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_115DecideConditionEPNS1_12JSHeapBrokerEPNS1_4NodeE
	cmpb	$1, %al
	je	.L507
	xorl	%r12d, %r12d
.L499:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L492:
	.cfi_restore_state
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler21CommonOperatorReducer12ReduceSelectEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L491:
	.cfi_restore_state
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler21CommonOperatorReducer9ReducePhiEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L490:
	.cfi_restore_state
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler21CommonOperatorReducer15ReduceEffectPhiEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L497:
	.cfi_restore_state
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler21CommonOperatorReducer12ReduceBranchEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L503:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L507:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	8(%r13), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdx
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	jmp	.L499
	.cfi_endproc
.LFE11547:
	.size	_ZN2v88internal8compiler21CommonOperatorReducer6ReduceEPNS1_4NodeE, .-_ZN2v88internal8compiler21CommonOperatorReducer6ReduceEPNS1_4NodeE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler21CommonOperatorReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_5GraphEPNS1_12JSHeapBrokerEPNS1_21CommonOperatorBuilderEPNS1_22MachineOperatorBuilderEPNS0_4ZoneE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler21CommonOperatorReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_5GraphEPNS1_12JSHeapBrokerEPNS1_21CommonOperatorBuilderEPNS1_22MachineOperatorBuilderEPNS0_4ZoneE, @function
_GLOBAL__sub_I__ZN2v88internal8compiler21CommonOperatorReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_5GraphEPNS1_12JSHeapBrokerEPNS1_21CommonOperatorBuilderEPNS1_22MachineOperatorBuilderEPNS0_4ZoneE:
.LFB13278:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE13278:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler21CommonOperatorReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_5GraphEPNS1_12JSHeapBrokerEPNS1_21CommonOperatorBuilderEPNS1_22MachineOperatorBuilderEPNS0_4ZoneE, .-_GLOBAL__sub_I__ZN2v88internal8compiler21CommonOperatorReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_5GraphEPNS1_12JSHeapBrokerEPNS1_21CommonOperatorBuilderEPNS1_22MachineOperatorBuilderEPNS0_4ZoneE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler21CommonOperatorReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_5GraphEPNS1_12JSHeapBrokerEPNS1_21CommonOperatorBuilderEPNS1_22MachineOperatorBuilderEPNS0_4ZoneE
	.weak	_ZTVN2v88internal8compiler21CommonOperatorReducerE
	.section	.data.rel.ro._ZTVN2v88internal8compiler21CommonOperatorReducerE,"awG",@progbits,_ZTVN2v88internal8compiler21CommonOperatorReducerE,comdat
	.align 8
	.type	_ZTVN2v88internal8compiler21CommonOperatorReducerE, @object
	.size	_ZTVN2v88internal8compiler21CommonOperatorReducerE, 56
_ZTVN2v88internal8compiler21CommonOperatorReducerE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal8compiler21CommonOperatorReducerD1Ev
	.quad	_ZN2v88internal8compiler21CommonOperatorReducerD0Ev
	.quad	_ZNK2v88internal8compiler21CommonOperatorReducer12reducer_nameEv
	.quad	_ZN2v88internal8compiler21CommonOperatorReducer6ReduceEPNS1_4NodeE
	.quad	_ZN2v88internal8compiler7Reducer8FinalizeEv
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
