	.file	"map-inference.cc"
	.text
	.section	.text._ZN2v88internal19InstanceTypeChecker12IsJSReceiverENS0_12InstanceTypeE,"axG",@progbits,_ZN2v88internal19InstanceTypeChecker12IsJSReceiverENS0_12InstanceTypeE,comdat
	.p2align 4
	.weak	_ZN2v88internal19InstanceTypeChecker12IsJSReceiverENS0_12InstanceTypeE
	.type	_ZN2v88internal19InstanceTypeChecker12IsJSReceiverENS0_12InstanceTypeE, @function
_ZN2v88internal19InstanceTypeChecker12IsJSReceiverENS0_12InstanceTypeE:
.LFB16181:
	.cfi_startproc
	endbr64
	cmpw	$1023, %di
	seta	%al
	ret
	.cfi_endproc
.LFE16181:
	.size	_ZN2v88internal19InstanceTypeChecker12IsJSReceiverENS0_12InstanceTypeE, .-_ZN2v88internal19InstanceTypeChecker12IsJSReceiverENS0_12InstanceTypeE
	.section	.text._ZNSt17_Function_handlerIFbN2v88internal12InstanceTypeEEPS3_E9_M_invokeERKSt9_Any_dataOS2_,"axG",@progbits,_ZNSt17_Function_handlerIFbN2v88internal12InstanceTypeEEPS3_E9_M_invokeERKSt9_Any_dataOS2_,comdat
	.p2align 4
	.weak	_ZNSt17_Function_handlerIFbN2v88internal12InstanceTypeEEPS3_E9_M_invokeERKSt9_Any_dataOS2_
	.type	_ZNSt17_Function_handlerIFbN2v88internal12InstanceTypeEEPS3_E9_M_invokeERKSt9_Any_dataOS2_, @function
_ZNSt17_Function_handlerIFbN2v88internal12InstanceTypeEEPS3_E9_M_invokeERKSt9_Any_dataOS2_:
.LFB22421:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	movzwl	(%rsi), %edi
	jmp	*(%r8)
	.cfi_endproc
.LFE22421:
	.size	_ZNSt17_Function_handlerIFbN2v88internal12InstanceTypeEEPS3_E9_M_invokeERKSt9_Any_dataOS2_, .-_ZNSt17_Function_handlerIFbN2v88internal12InstanceTypeEEPS3_E9_M_invokeERKSt9_Any_dataOS2_
	.section	.text._ZNSt14_Function_base13_Base_managerIPFbN2v88internal12InstanceTypeEEE10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation,"axG",@progbits,_ZNSt14_Function_base13_Base_managerIPFbN2v88internal12InstanceTypeEEE10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation,comdat
	.p2align 4
	.weak	_ZNSt14_Function_base13_Base_managerIPFbN2v88internal12InstanceTypeEEE10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation
	.type	_ZNSt14_Function_base13_Base_managerIPFbN2v88internal12InstanceTypeEEE10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIPFbN2v88internal12InstanceTypeEEE10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation:
.LFB22422:
	.cfi_startproc
	endbr64
	cmpl	$1, %edx
	je	.L5
	cmpl	$2, %edx
	jne	.L7
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
.L7:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE22422:
	.size	_ZNSt14_Function_base13_Base_managerIPFbN2v88internal12InstanceTypeEEE10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIPFbN2v88internal12InstanceTypeEEE10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation
	.section	.text._ZNSt17_Function_handlerIFbN2v88internal12InstanceTypeEEZNKS1_8compiler12MapInference21AllOfInstanceTypesAreES2_EUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFbN2v88internal12InstanceTypeEEZNKS1_8compiler12MapInference21AllOfInstanceTypesAreES2_EUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_, @function
_ZNSt17_Function_handlerIFbN2v88internal12InstanceTypeEEZNKS1_8compiler12MapInference21AllOfInstanceTypesAreES2_EUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_:
.LFB22426:
	.cfi_startproc
	endbr64
	movzwl	(%rdi), %eax
	cmpw	%ax, (%rsi)
	sete	%al
	ret
	.cfi_endproc
.LFE22426:
	.size	_ZNSt17_Function_handlerIFbN2v88internal12InstanceTypeEEZNKS1_8compiler12MapInference21AllOfInstanceTypesAreES2_EUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_, .-_ZNSt17_Function_handlerIFbN2v88internal12InstanceTypeEEZNKS1_8compiler12MapInference21AllOfInstanceTypesAreES2_EUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_
	.section	.text._ZNSt14_Function_base13_Base_managerIZNK2v88internal8compiler12MapInference21AllOfInstanceTypesAreENS2_12InstanceTypeEEUlS5_E_E10_M_managerERSt9_Any_dataRKS8_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZNK2v88internal8compiler12MapInference21AllOfInstanceTypesAreENS2_12InstanceTypeEEUlS5_E_E10_M_managerERSt9_Any_dataRKS8_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZNK2v88internal8compiler12MapInference21AllOfInstanceTypesAreENS2_12InstanceTypeEEUlS5_E_E10_M_managerERSt9_Any_dataRKS8_St18_Manager_operation:
.LFB22427:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L10
	cmpl	$3, %edx
	je	.L11
	cmpl	$1, %edx
	je	.L15
.L11:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	movzwl	(%rsi), %eax
	movw	%ax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE22427:
	.size	_ZNSt14_Function_base13_Base_managerIZNK2v88internal8compiler12MapInference21AllOfInstanceTypesAreENS2_12InstanceTypeEEUlS5_E_E10_M_managerERSt9_Any_dataRKS8_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZNK2v88internal8compiler12MapInference21AllOfInstanceTypesAreENS2_12InstanceTypeEEUlS5_E_E10_M_managerERSt9_Any_dataRKS8_St18_Manager_operation
	.section	.text._ZNSt17_Function_handlerIFbN2v88internal12InstanceTypeEEZNKS1_8compiler12MapInference21AnyOfInstanceTypesAreES2_EUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFbN2v88internal12InstanceTypeEEZNKS1_8compiler12MapInference21AnyOfInstanceTypesAreES2_EUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_, @function
_ZNSt17_Function_handlerIFbN2v88internal12InstanceTypeEEZNKS1_8compiler12MapInference21AnyOfInstanceTypesAreES2_EUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_:
.LFB22430:
	.cfi_startproc
	endbr64
	movzwl	(%rdi), %eax
	cmpw	%ax, (%rsi)
	sete	%al
	ret
	.cfi_endproc
.LFE22430:
	.size	_ZNSt17_Function_handlerIFbN2v88internal12InstanceTypeEEZNKS1_8compiler12MapInference21AnyOfInstanceTypesAreES2_EUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_, .-_ZNSt17_Function_handlerIFbN2v88internal12InstanceTypeEEZNKS1_8compiler12MapInference21AnyOfInstanceTypesAreES2_EUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_
	.section	.text._ZNSt14_Function_base13_Base_managerIZNK2v88internal8compiler12MapInference21AnyOfInstanceTypesAreENS2_12InstanceTypeEEUlS5_E_E10_M_managerERSt9_Any_dataRKS8_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZNK2v88internal8compiler12MapInference21AnyOfInstanceTypesAreENS2_12InstanceTypeEEUlS5_E_E10_M_managerERSt9_Any_dataRKS8_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZNK2v88internal8compiler12MapInference21AnyOfInstanceTypesAreENS2_12InstanceTypeEEUlS5_E_E10_M_managerERSt9_Any_dataRKS8_St18_Manager_operation:
.LFB22431:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L18
	cmpl	$3, %edx
	je	.L19
	cmpl	$1, %edx
	je	.L23
.L19:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	movzwl	(%rsi), %eax
	movw	%ax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE22431:
	.size	_ZNSt14_Function_base13_Base_managerIZNK2v88internal8compiler12MapInference21AnyOfInstanceTypesAreENS2_12InstanceTypeEEUlS5_E_E10_M_managerERSt9_Any_dataRKS8_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZNK2v88internal8compiler12MapInference21AnyOfInstanceTypesAreENS2_12InstanceTypeEEUlS5_E_E10_M_managerERSt9_Any_dataRKS8_St18_Manager_operation
	.section	.rodata._ZN2v88internal8compiler6MapRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb.str1.1,"aMS",@progbits,1
.LC0:
	.string	"IsMap()"
.LC1:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal8compiler6MapRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,"axG",@progbits,_ZN2v88internal8compiler6MapRefC5EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler6MapRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.type	_ZN2v88internal8compiler6MapRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, @function
_ZN2v88internal8compiler6MapRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb:
.LFB9708:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%ecx, %ebx
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	testb	%bl, %bl
	jne	.L27
.L24:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef5IsMapEv@PLT
	testb	%al, %al
	jne	.L24
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE9708:
	.size	_ZN2v88internal8compiler6MapRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, .-_ZN2v88internal8compiler6MapRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.weak	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.set	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,_ZN2v88internal8compiler6MapRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.section	.rodata._ZN2v88internal8compiler12MapInferenceD2Ev.str1.1,"aMS",@progbits,1
.LC2:
	.string	"Safe()"
	.section	.text._ZN2v88internal8compiler12MapInferenceD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler12MapInferenceD2Ev
	.type	_ZN2v88internal8compiler12MapInferenceD2Ev, @function
_ZN2v88internal8compiler12MapInferenceD2Ev:
.LFB19307:
	.cfi_startproc
	endbr64
	cmpl	$2, 40(%rdi)
	je	.L34
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L28
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L28:
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC2(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19307:
	.size	_ZN2v88internal8compiler12MapInferenceD2Ev, .-_ZN2v88internal8compiler12MapInferenceD2Ev
	.globl	_ZN2v88internal8compiler12MapInferenceD1Ev
	.set	_ZN2v88internal8compiler12MapInferenceD1Ev,_ZN2v88internal8compiler12MapInferenceD2Ev
	.section	.text._ZNK2v88internal8compiler12MapInference4SafeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler12MapInference4SafeEv
	.type	_ZNK2v88internal8compiler12MapInference4SafeEv, @function
_ZNK2v88internal8compiler12MapInference4SafeEv:
.LFB19309:
	.cfi_startproc
	endbr64
	cmpl	$2, 40(%rdi)
	setne	%al
	ret
	.cfi_endproc
.LFE19309:
	.size	_ZNK2v88internal8compiler12MapInference4SafeEv, .-_ZNK2v88internal8compiler12MapInference4SafeEv
	.section	.rodata._ZN2v88internal8compiler12MapInference24SetNeedGuardIfUnreliableEv.str1.1,"aMS",@progbits,1
.LC3:
	.string	"HaveMaps()"
	.section	.text._ZN2v88internal8compiler12MapInference24SetNeedGuardIfUnreliableEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler12MapInference24SetNeedGuardIfUnreliableEv
	.type	_ZN2v88internal8compiler12MapInference24SetNeedGuardIfUnreliableEv, @function
_ZN2v88internal8compiler12MapInference24SetNeedGuardIfUnreliableEv:
.LFB19310:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	cmpq	%rax, 24(%rdi)
	je	.L42
	cmpl	$1, 40(%rdi)
	je	.L43
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	movl	$2, 40(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC3(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19310:
	.size	_ZN2v88internal8compiler12MapInference24SetNeedGuardIfUnreliableEv, .-_ZN2v88internal8compiler12MapInference24SetNeedGuardIfUnreliableEv
	.section	.text._ZN2v88internal8compiler12MapInference10SetGuardedEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler12MapInference10SetGuardedEv
	.type	_ZN2v88internal8compiler12MapInference10SetGuardedEv, @function
_ZN2v88internal8compiler12MapInference10SetGuardedEv:
.LFB19311:
	.cfi_startproc
	endbr64
	movl	$0, 40(%rdi)
	ret
	.cfi_endproc
.LFE19311:
	.size	_ZN2v88internal8compiler12MapInference10SetGuardedEv, .-_ZN2v88internal8compiler12MapInference10SetGuardedEv
	.section	.text._ZNK2v88internal8compiler12MapInference8HaveMapsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler12MapInference8HaveMapsEv
	.type	_ZNK2v88internal8compiler12MapInference8HaveMapsEv, @function
_ZNK2v88internal8compiler12MapInference8HaveMapsEv:
.LFB19312:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	cmpq	%rax, 24(%rdi)
	setne	%al
	ret
	.cfi_endproc
.LFE19312:
	.size	_ZNK2v88internal8compiler12MapInference8HaveMapsEv, .-_ZNK2v88internal8compiler12MapInference8HaveMapsEv
	.section	.text._ZNK2v88internal8compiler12MapInference24AllOfInstanceTypesUnsafeESt8functionIFbNS0_12InstanceTypeEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler12MapInference24AllOfInstanceTypesUnsafeESt8functionIFbNS0_12InstanceTypeEEE
	.type	_ZNK2v88internal8compiler12MapInference24AllOfInstanceTypesUnsafeESt8functionIFbNS0_12InstanceTypeEEE, @function
_ZNK2v88internal8compiler12MapInference24AllOfInstanceTypesUnsafeESt8functionIFbNS0_12InstanceTypeEEE:
.LFB19322:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$536, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	24(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	16(%rdi), %r12
	je	.L111
	movq	16(%rsi), %rax
	movq	%rdi, -432(%rbp)
	movq	%rdi, %r13
	movq	%rsi, %rbx
	movq	$0, -408(%rbp)
	testq	%rax, %rax
	je	.L112
	leaq	-424(%rbp), %r12
	movl	$2, %edx
	movq	%r12, %rdi
	call	*%rax
	movdqu	16(%rbx), %xmm6
	movq	-432(%rbp), %rdx
	movq	$0, -360(%rbp)
	movq	16(%rbx), %rax
	movq	%rdx, -384(%rbp)
	movups	%xmm6, -408(%rbp)
	testq	%rax, %rax
	je	.L113
	leaq	-376(%rbp), %r14
	movl	$2, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	*%rax
	movdqu	-408(%rbp), %xmm5
	movq	-384(%rbp), %rdx
	movq	$0, -312(%rbp)
	movq	-408(%rbp), %rax
	movq	24(%r13), %r12
	movq	%rdx, -336(%rbp)
	movq	16(%r13), %rbx
	movups	%xmm5, -360(%rbp)
	testq	%rax, %rax
	je	.L114
	leaq	-328(%rbp), %r15
	movl	$2, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	*%rax
	movq	-336(%rbp), %r13
	movq	-360(%rbp), %rax
	movq	$0, -264(%rbp)
	movdqu	-360(%rbp), %xmm7
	movq	%r13, -288(%rbp)
	movups	%xmm7, -312(%rbp)
	testq	%rax, %rax
	je	.L52
	movl	$2, %edx
	leaq	-280(%rbp), %rdi
	movq	%r15, %rsi
	call	*%rax
	movq	-304(%rbp), %rdx
	movq	-312(%rbp), %rax
	movq	-288(%rbp), %r13
.L54:
	movdqu	-88(%rbp), %xmm1
	movq	%rdx, %xmm2
	movq	-64(%rbp), %rcx
	movq	$0, -264(%rbp)
	movdqu	-280(%rbp), %xmm0
	movq	%r13, -240(%rbp)
	movq	%rcx, -256(%rbp)
	movq	-112(%rbp), %rdx
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -232(%rbp)
	movq	%rax, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movq	%r13, -144(%rbp)
	movq	$0, -120(%rbp)
	movups	%xmm1, -280(%rbp)
	movups	%xmm0, -216(%rbp)
	testq	%rax, %rax
	je	.L56
	movl	$2, %edx
	leaq	-232(%rbp), %rsi
	leaq	-136(%rbp), %rdi
	call	*%rax
	movq	-208(%rbp), %rdx
	movq	-216(%rbp), %rax
	movq	-144(%rbp), %r13
.L56:
	movq	%rdx, %xmm4
	movdqu	-136(%rbp), %xmm0
	movdqu	-184(%rbp), %xmm3
	movq	$0, -120(%rbp)
	movq	-160(%rbp), %rcx
	movq	%r13, -96(%rbp)
	movups	%xmm0, -184(%rbp)
	movups	%xmm0, -88(%rbp)
	movq	%rax, %xmm0
	movq	%r12, %rax
	subq	%rbx, %rax
	punpcklqdq	%xmm4, %xmm0
	movq	%rcx, -112(%rbp)
	movq	%rax, %rdx
	sarq	$5, %rax
	movups	%xmm3, -136(%rbp)
	movups	%xmm0, -72(%rbp)
	sarq	$3, %rdx
	testq	%rax, %rax
	jle	.L57
	salq	$5, %rax
	leaq	-544(%rbp), %r14
	leaq	-558(%rbp), %r15
	addq	%rbx, %rax
	movq	%rax, -576(%rbp)
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L59:
	movq	-96(%rbp), %rax
	movq	8(%rbx), %rdx
	movl	$1, %ecx
	leaq	-528(%rbp), %rdi
	movq	%rdi, -568(%rbp)
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	-568(%rbp), %rdi
	call	_ZNK2v88internal8compiler6MapRef13instance_typeEv@PLT
	cmpq	$0, -72(%rbp)
	movw	%ax, -556(%rbp)
	je	.L61
	leaq	-556(%rbp), %rsi
	movq	%r13, %rdi
	call	*-64(%rbp)
	testb	%al, %al
	je	.L115
	movq	-96(%rbp), %rax
	movq	16(%rbx), %rdx
	movl	$1, %ecx
	leaq	-512(%rbp), %rdi
	movq	%rdi, -568(%rbp)
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	-568(%rbp), %rdi
	call	_ZNK2v88internal8compiler6MapRef13instance_typeEv@PLT
	cmpq	$0, -72(%rbp)
	movw	%ax, -554(%rbp)
	je	.L61
	leaq	-554(%rbp), %rsi
	movq	%r13, %rdi
	call	*-64(%rbp)
	testb	%al, %al
	je	.L116
	movq	-96(%rbp), %rax
	movq	24(%rbx), %rdx
	movl	$1, %ecx
	leaq	-496(%rbp), %rdi
	movq	%rdi, -568(%rbp)
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	-568(%rbp), %rdi
	call	_ZNK2v88internal8compiler6MapRef13instance_typeEv@PLT
	cmpq	$0, -72(%rbp)
	movw	%ax, -552(%rbp)
	je	.L61
	leaq	-552(%rbp), %rsi
	movq	%r13, %rdi
	call	*-64(%rbp)
	testb	%al, %al
	je	.L117
	addq	$32, %rbx
	cmpq	%rbx, -576(%rbp)
	je	.L118
	movq	-96(%rbp), %r13
.L66:
	movq	(%rbx), %rdx
	movq	0(%r13), %rsi
	movl	$1, %ecx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler6MapRef13instance_typeEv@PLT
	cmpq	$0, -72(%rbp)
	movw	%ax, -558(%rbp)
	je	.L61
	leaq	-88(%rbp), %r13
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	*-64(%rbp)
	testb	%al, %al
	jne	.L59
.L110:
	cmpq	%r12, %rbx
	sete	%r12b
.L60:
	movq	-72(%rbp), %rax
	testq	%rax, %rax
	je	.L73
	leaq	-88(%rbp), %rdi
	movl	$3, %edx
	movq	%rdi, %rsi
	call	*%rax
.L73:
	movq	-120(%rbp), %rax
	testq	%rax, %rax
	je	.L74
	leaq	-136(%rbp), %rdi
	movl	$3, %edx
	movq	%rdi, %rsi
	call	*%rax
.L74:
	movq	-216(%rbp), %rax
	testq	%rax, %rax
	je	.L75
	leaq	-232(%rbp), %rdi
	movl	$3, %edx
	movq	%rdi, %rsi
	call	*%rax
.L75:
	movq	-264(%rbp), %rax
	testq	%rax, %rax
	je	.L76
	leaq	-280(%rbp), %rdi
	movl	$3, %edx
	movq	%rdi, %rsi
	call	*%rax
.L76:
	movq	-312(%rbp), %rax
	testq	%rax, %rax
	je	.L77
	leaq	-328(%rbp), %rdi
	movl	$3, %edx
	movq	%rdi, %rsi
	call	*%rax
.L77:
	movq	-360(%rbp), %rax
	testq	%rax, %rax
	je	.L78
	leaq	-376(%rbp), %rdi
	movl	$3, %edx
	movq	%rdi, %rsi
	call	*%rax
.L78:
	movq	-408(%rbp), %rax
	testq	%rax, %rax
	je	.L46
	leaq	-424(%rbp), %rdi
	movl	$3, %edx
	movq	%rdi, %rsi
	call	*%rax
.L46:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L119
	addq	$536, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L112:
	.cfi_restore_state
	movq	%rdi, -384(%rbp)
	movq	16(%rdi), %rbx
	movq	$0, -360(%rbp)
	movq	%rdi, -336(%rbp)
	movq	$0, -312(%rbp)
.L49:
	movq	%r13, -288(%rbp)
.L52:
	movq	-256(%rbp), %rdx
	xorl	%eax, %eax
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L118:
	movq	%r12, %rdx
	subq	%rbx, %rdx
	sarq	$3, %rdx
.L57:
	cmpq	$2, %rdx
	je	.L67
	cmpq	$3, %rdx
	je	.L68
	cmpq	$1, %rdx
	je	.L69
.L70:
	movl	$1, %r12d
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L115:
	addq	$8, %rbx
	cmpq	%rbx, %r12
	sete	%r12b
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L116:
	addq	$16, %rbx
	cmpq	%rbx, %r12
	sete	%r12b
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L117:
	addq	$24, %rbx
	cmpq	%rbx, %r12
	sete	%r12b
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L111:
	leaq	.LC3(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L68:
	movq	-96(%rbp), %rax
	movq	(%rbx), %rdx
	movl	$1, %ecx
	leaq	-480(%rbp), %r13
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler6MapRef13instance_typeEv@PLT
	cmpq	$0, -72(%rbp)
	movw	%ax, -550(%rbp)
	je	.L61
	leaq	-550(%rbp), %rsi
	leaq	-88(%rbp), %rdi
	call	*-64(%rbp)
	testb	%al, %al
	je	.L110
	addq	$8, %rbx
.L67:
	movq	-96(%rbp), %rax
	movq	(%rbx), %rdx
	movl	$1, %ecx
	leaq	-464(%rbp), %r13
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler6MapRef13instance_typeEv@PLT
	cmpq	$0, -72(%rbp)
	movw	%ax, -548(%rbp)
	je	.L61
	leaq	-548(%rbp), %rsi
	leaq	-88(%rbp), %rdi
	call	*-64(%rbp)
	testb	%al, %al
	je	.L110
	addq	$8, %rbx
.L69:
	movq	-96(%rbp), %rax
	movq	(%rbx), %rdx
	movl	$1, %ecx
	leaq	-448(%rbp), %r13
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler6MapRef13instance_typeEv@PLT
	cmpq	$0, -72(%rbp)
	movw	%ax, -546(%rbp)
	je	.L61
	leaq	-546(%rbp), %rsi
	leaq	-88(%rbp), %rdi
	call	*-64(%rbp)
	testb	%al, %al
	jne	.L70
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L113:
	movq	24(%r13), %r12
	movq	16(%r13), %rbx
	movq	%rdx, -336(%rbp)
	movq	%rdx, %r13
	movq	$0, -312(%rbp)
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L61:
	call	_ZSt25__throw_bad_function_callv@PLT
.L119:
	call	__stack_chk_fail@PLT
.L114:
	movq	%rdx, %r13
	jmp	.L49
	.cfi_endproc
.LFE19322:
	.size	_ZNK2v88internal8compiler12MapInference24AllOfInstanceTypesUnsafeESt8functionIFbNS0_12InstanceTypeEEE, .-_ZNK2v88internal8compiler12MapInference24AllOfInstanceTypesUnsafeESt8functionIFbNS0_12InstanceTypeEEE
	.section	.text._ZNK2v88internal8compiler12MapInference31AllOfInstanceTypesAreJSReceiverEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler12MapInference31AllOfInstanceTypesAreJSReceiverEv
	.type	_ZNK2v88internal8compiler12MapInference31AllOfInstanceTypesAreJSReceiverEv, @function
_ZNK2v88internal8compiler12MapInference31AllOfInstanceTypesAreJSReceiverEv:
.LFB19313:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZNSt14_Function_base13_Base_managerIPFbN2v88internal12InstanceTypeEEE10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation(%rip), %rcx
	movq	%rcx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	movq	%r13, %rsi
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	_ZN2v88internal19InstanceTypeChecker12IsJSReceiverENS0_12InstanceTypeE(%rip), %rax
	movq	%rax, -64(%rbp)
	leaq	_ZNSt17_Function_handlerIFbN2v88internal12InstanceTypeEEPS3_E9_M_invokeERKSt9_Any_dataOS2_(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_ZNK2v88internal8compiler12MapInference24AllOfInstanceTypesUnsafeESt8functionIFbNS0_12InstanceTypeEEE
	movl	%eax, %r12d
	movq	-48(%rbp), %rax
	testq	%rax, %rax
	je	.L120
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	*%rax
.L120:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L127
	addq	$48, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L127:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19313:
	.size	_ZNK2v88internal8compiler12MapInference31AllOfInstanceTypesAreJSReceiverEv, .-_ZNK2v88internal8compiler12MapInference31AllOfInstanceTypesAreJSReceiverEv
	.section	.rodata._ZNK2v88internal8compiler12MapInference21AllOfInstanceTypesAreENS0_12InstanceTypeE.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"!InstanceTypeChecker::IsString(type)"
	.section	.text._ZNK2v88internal8compiler12MapInference21AllOfInstanceTypesAreENS0_12InstanceTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler12MapInference21AllOfInstanceTypesAreENS0_12InstanceTypeE
	.type	_ZNK2v88internal8compiler12MapInference21AllOfInstanceTypesAreENS0_12InstanceTypeE, @function
_ZNK2v88internal8compiler12MapInference21AllOfInstanceTypesAreENS0_12InstanceTypeE:
.LFB19317:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	subq	$48, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpw	$63, %si
	jbe	.L136
	leaq	_ZNSt17_Function_handlerIFbN2v88internal12InstanceTypeEEZNKS1_8compiler12MapInference21AllOfInstanceTypesAreES2_EUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZNK2v88internal8compiler12MapInference21AllOfInstanceTypesAreENS2_12InstanceTypeEEUlS5_E_E10_M_managerERSt9_Any_dataRKS8_St18_Manager_operation(%rip), %rcx
	movw	%si, -64(%rbp)
	movq	%rcx, %xmm0
	movq	%rax, %xmm1
	leaq	-64(%rbp), %r13
	punpcklqdq	%xmm1, %xmm0
	movq	%r13, %rsi
	movaps	%xmm0, -48(%rbp)
	call	_ZNK2v88internal8compiler12MapInference24AllOfInstanceTypesUnsafeESt8functionIFbNS0_12InstanceTypeEEE
	movl	%eax, %r12d
	movq	-48(%rbp), %rax
	testq	%rax, %rax
	je	.L128
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	*%rax
.L128:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L137
	addq	$48, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L136:
	.cfi_restore_state
	leaq	.LC4(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L137:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19317:
	.size	_ZNK2v88internal8compiler12MapInference21AllOfInstanceTypesAreENS0_12InstanceTypeE, .-_ZNK2v88internal8compiler12MapInference21AllOfInstanceTypesAreENS0_12InstanceTypeE
	.section	.text._ZN2v88internal8compiler12MapInference18AllOfInstanceTypesESt8functionIFbNS0_12InstanceTypeEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler12MapInference18AllOfInstanceTypesESt8functionIFbNS0_12InstanceTypeEEE
	.type	_ZN2v88internal8compiler12MapInference18AllOfInstanceTypesESt8functionIFbNS0_12InstanceTypeEEE, @function
_ZN2v88internal8compiler12MapInference18AllOfInstanceTypesESt8functionIFbNS0_12InstanceTypeEEE:
.LFB19321:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	cmpq	%rax, 24(%rdi)
	je	.L151
	cmpl	$1, 40(%rdi)
	movq	%rdi, %r12
	movq	%rsi, %rbx
	jne	.L140
	movl	$2, 40(%rdi)
.L140:
	movq	16(%rbx), %rax
	movq	$0, -64(%rbp)
	leaq	-80(%rbp), %r13
	testq	%rax, %rax
	je	.L141
	movl	$2, %edx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	*%rax
	movdqu	16(%rbx), %xmm0
	movaps	%xmm0, -64(%rbp)
.L141:
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZNK2v88internal8compiler12MapInference24AllOfInstanceTypesUnsafeESt8functionIFbNS0_12InstanceTypeEEE
	movl	%eax, %r12d
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L138
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	*%rax
.L138:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L152
	addq	$56, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L151:
	.cfi_restore_state
	leaq	.LC3(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L152:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19321:
	.size	_ZN2v88internal8compiler12MapInference18AllOfInstanceTypesESt8functionIFbNS0_12InstanceTypeEEE, .-_ZN2v88internal8compiler12MapInference18AllOfInstanceTypesESt8functionIFbNS0_12InstanceTypeEEE
	.section	.text._ZNK2v88internal8compiler12MapInference24AnyOfInstanceTypesUnsafeESt8functionIFbNS0_12InstanceTypeEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler12MapInference24AnyOfInstanceTypesUnsafeESt8functionIFbNS0_12InstanceTypeEEE
	.type	_ZNK2v88internal8compiler12MapInference24AnyOfInstanceTypesUnsafeESt8functionIFbNS0_12InstanceTypeEEE, @function
_ZNK2v88internal8compiler12MapInference24AnyOfInstanceTypesUnsafeESt8functionIFbNS0_12InstanceTypeEEE:
.LFB19333:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$488, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	24(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	16(%rdi), %r12
	je	.L225
	movq	16(%rsi), %rax
	movq	%rdi, -384(%rbp)
	movq	%rdi, %r13
	movq	%rsi, %rbx
	movq	$0, -360(%rbp)
	testq	%rax, %rax
	je	.L226
	leaq	-376(%rbp), %r12
	movl	$2, %edx
	movq	%r12, %rdi
	call	*%rax
	movdqu	16(%rbx), %xmm4
	movq	-384(%rbp), %rdx
	movq	$0, -312(%rbp)
	movq	16(%rbx), %rax
	movq	%rdx, -336(%rbp)
	movups	%xmm4, -360(%rbp)
	testq	%rax, %rax
	je	.L227
	leaq	-328(%rbp), %r14
	movl	$2, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	*%rax
	movdqu	-360(%rbp), %xmm3
	movq	-336(%rbp), %rdx
	movq	$0, -264(%rbp)
	movq	-360(%rbp), %rax
	movq	24(%r13), %r12
	movq	%rdx, -288(%rbp)
	movq	16(%r13), %rbx
	movups	%xmm3, -312(%rbp)
	testq	%rax, %rax
	je	.L228
	leaq	-280(%rbp), %r15
	movl	$2, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	*%rax
	movq	-288(%rbp), %r13
	movq	-312(%rbp), %rax
	movq	$0, -216(%rbp)
	movdqu	-312(%rbp), %xmm5
	movq	%r13, -240(%rbp)
	movups	%xmm5, -264(%rbp)
	testq	%rax, %rax
	je	.L159
	leaq	-232(%rbp), %r14
	movl	$2, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	*%rax
	movq	-240(%rbp), %r13
	movq	-264(%rbp), %rax
	movq	$0, -168(%rbp)
	movdqu	-264(%rbp), %xmm6
	movq	%r13, -192(%rbp)
	movups	%xmm6, -216(%rbp)
	testq	%rax, %rax
	je	.L161
	movl	$2, %edx
	leaq	-184(%rbp), %rdi
	movq	%r14, %rsi
	call	*%rax
	movq	-208(%rbp), %rdx
	movq	-216(%rbp), %rax
	movq	-192(%rbp), %r13
.L163:
	movdqu	-184(%rbp), %xmm0
	movdqu	-88(%rbp), %xmm1
	movq	%r13, -96(%rbp)
	movq	%rdx, %xmm2
	movq	-64(%rbp), %rcx
	movq	%r13, -144(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -136(%rbp)
	movq	%rax, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movq	%rcx, -160(%rbp)
	movq	$0, -168(%rbp)
	movq	$0, -72(%rbp)
	movups	%xmm1, -184(%rbp)
	movups	%xmm0, -120(%rbp)
	testq	%rax, %rax
	je	.L164
	leaq	-136(%rbp), %rsi
	leaq	-88(%rbp), %rdi
	movl	$2, %edx
	call	*%rax
	movdqu	-120(%rbp), %xmm7
	movups	%xmm7, -72(%rbp)
.L164:
	movq	%r12, %rax
	subq	%rbx, %rax
	movq	%rax, %rdx
	sarq	$5, %rax
	sarq	$3, %rdx
	testq	%rax, %rax
	jle	.L165
	salq	$5, %rax
	leaq	-496(%rbp), %r14
	leaq	-510(%rbp), %r15
	addq	%rbx, %rax
	movq	%rax, -528(%rbp)
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L167:
	movq	-96(%rbp), %rax
	movq	8(%rbx), %rdx
	movl	$1, %ecx
	leaq	-480(%rbp), %rdi
	movq	%rdi, -520(%rbp)
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	-520(%rbp), %rdi
	call	_ZNK2v88internal8compiler6MapRef13instance_typeEv@PLT
	cmpq	$0, -72(%rbp)
	movw	%ax, -508(%rbp)
	je	.L169
	leaq	-508(%rbp), %rsi
	movq	%r13, %rdi
	call	*-64(%rbp)
	testb	%al, %al
	jne	.L229
	movq	-96(%rbp), %rax
	movq	16(%rbx), %rdx
	movl	$1, %ecx
	leaq	-464(%rbp), %rdi
	movq	%rdi, -520(%rbp)
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	-520(%rbp), %rdi
	call	_ZNK2v88internal8compiler6MapRef13instance_typeEv@PLT
	cmpq	$0, -72(%rbp)
	movw	%ax, -506(%rbp)
	je	.L169
	leaq	-506(%rbp), %rsi
	movq	%r13, %rdi
	call	*-64(%rbp)
	testb	%al, %al
	jne	.L230
	movq	-96(%rbp), %rax
	movq	24(%rbx), %rdx
	movl	$1, %ecx
	leaq	-448(%rbp), %rdi
	movq	%rdi, -520(%rbp)
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	-520(%rbp), %rdi
	call	_ZNK2v88internal8compiler6MapRef13instance_typeEv@PLT
	cmpq	$0, -72(%rbp)
	movw	%ax, -504(%rbp)
	je	.L169
	leaq	-504(%rbp), %rsi
	movq	%r13, %rdi
	call	*-64(%rbp)
	testb	%al, %al
	jne	.L231
	addq	$32, %rbx
	cmpq	%rbx, -528(%rbp)
	je	.L232
.L173:
	movq	-96(%rbp), %rax
	movq	(%rbx), %rdx
	movl	$1, %ecx
	movq	%r14, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler6MapRef13instance_typeEv@PLT
	cmpq	$0, -72(%rbp)
	movw	%ax, -510(%rbp)
	je	.L169
	leaq	-88(%rbp), %r13
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	*-64(%rbp)
	testb	%al, %al
	je	.L167
.L223:
	cmpq	%r12, %rbx
	setne	%r12b
.L168:
	movq	-72(%rbp), %rax
	testq	%rax, %rax
	je	.L180
	leaq	-88(%rbp), %rdi
	movl	$3, %edx
	movq	%rdi, %rsi
	call	*%rax
.L180:
	movq	-120(%rbp), %rax
	testq	%rax, %rax
	je	.L181
	leaq	-136(%rbp), %rdi
	movl	$3, %edx
	movq	%rdi, %rsi
	call	*%rax
.L181:
	movq	-168(%rbp), %rax
	testq	%rax, %rax
	je	.L182
	leaq	-184(%rbp), %rdi
	movl	$3, %edx
	movq	%rdi, %rsi
	call	*%rax
.L182:
	movq	-216(%rbp), %rax
	testq	%rax, %rax
	je	.L183
	leaq	-232(%rbp), %rdi
	movl	$3, %edx
	movq	%rdi, %rsi
	call	*%rax
.L183:
	movq	-264(%rbp), %rax
	testq	%rax, %rax
	je	.L184
	leaq	-280(%rbp), %rdi
	movl	$3, %edx
	movq	%rdi, %rsi
	call	*%rax
.L184:
	movq	-312(%rbp), %rax
	testq	%rax, %rax
	je	.L185
	leaq	-328(%rbp), %rdi
	movl	$3, %edx
	movq	%rdi, %rsi
	call	*%rax
.L185:
	movq	-360(%rbp), %rax
	testq	%rax, %rax
	je	.L153
	leaq	-376(%rbp), %rdi
	movl	$3, %edx
	movq	%rdi, %rsi
	call	*%rax
.L153:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L233
	addq	$488, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L226:
	.cfi_restore_state
	movq	%rdi, -336(%rbp)
	movq	16(%rdi), %rbx
	movq	$0, -312(%rbp)
	movq	%rdi, -288(%rbp)
	movq	$0, -264(%rbp)
.L156:
	movq	%r13, -240(%rbp)
	movq	$0, -216(%rbp)
.L159:
	movq	%r13, -192(%rbp)
.L161:
	movq	-160(%rbp), %rdx
	xorl	%eax, %eax
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L232:
	movq	%r12, %rdx
	subq	%rbx, %rdx
	sarq	$3, %rdx
.L165:
	cmpq	$2, %rdx
	je	.L174
	cmpq	$3, %rdx
	je	.L175
	cmpq	$1, %rdx
	je	.L176
.L177:
	xorl	%r12d, %r12d
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L229:
	addq	$8, %rbx
	cmpq	%rbx, %r12
	setne	%r12b
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L230:
	addq	$16, %rbx
	cmpq	%rbx, %r12
	setne	%r12b
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L231:
	addq	$24, %rbx
	cmpq	%rbx, %r12
	setne	%r12b
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L225:
	leaq	.LC3(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L175:
	movq	-96(%rbp), %rax
	movq	(%rbx), %rdx
	movl	$1, %ecx
	leaq	-432(%rbp), %r13
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler6MapRef13instance_typeEv@PLT
	cmpq	$0, -72(%rbp)
	movw	%ax, -502(%rbp)
	je	.L169
	leaq	-502(%rbp), %rsi
	leaq	-88(%rbp), %rdi
	call	*-64(%rbp)
	testb	%al, %al
	jne	.L223
	addq	$8, %rbx
.L174:
	movq	-96(%rbp), %rax
	movq	(%rbx), %rdx
	movl	$1, %ecx
	leaq	-416(%rbp), %r13
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler6MapRef13instance_typeEv@PLT
	cmpq	$0, -72(%rbp)
	movw	%ax, -500(%rbp)
	je	.L169
	leaq	-500(%rbp), %rsi
	leaq	-88(%rbp), %rdi
	call	*-64(%rbp)
	testb	%al, %al
	jne	.L224
	addq	$8, %rbx
.L176:
	movq	-96(%rbp), %rax
	movq	(%rbx), %rdx
	movl	$1, %ecx
	leaq	-400(%rbp), %r13
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler6MapRef13instance_typeEv@PLT
	cmpq	$0, -72(%rbp)
	movw	%ax, -498(%rbp)
	je	.L169
	leaq	-498(%rbp), %rsi
	leaq	-88(%rbp), %rdi
	call	*-64(%rbp)
	testb	%al, %al
	je	.L177
.L224:
	cmpq	%rbx, %r12
	setne	%r12b
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L227:
	movq	24(%r13), %r12
	movq	16(%r13), %rbx
	movq	%rdx, -288(%rbp)
	movq	%rdx, %r13
	movq	$0, -264(%rbp)
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L169:
	call	_ZSt25__throw_bad_function_callv@PLT
.L233:
	call	__stack_chk_fail@PLT
.L228:
	movq	%rdx, %r13
	jmp	.L156
	.cfi_endproc
.LFE19333:
	.size	_ZNK2v88internal8compiler12MapInference24AnyOfInstanceTypesUnsafeESt8functionIFbNS0_12InstanceTypeEEE, .-_ZNK2v88internal8compiler12MapInference24AnyOfInstanceTypesUnsafeESt8functionIFbNS0_12InstanceTypeEEE
	.section	.text._ZNK2v88internal8compiler12MapInference21AnyOfInstanceTypesAreENS0_12InstanceTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler12MapInference21AnyOfInstanceTypesAreENS0_12InstanceTypeE
	.type	_ZNK2v88internal8compiler12MapInference21AnyOfInstanceTypesAreENS0_12InstanceTypeE, @function
_ZNK2v88internal8compiler12MapInference21AnyOfInstanceTypesAreENS0_12InstanceTypeE:
.LFB19319:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	subq	$48, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpw	$63, %si
	jbe	.L242
	leaq	_ZNSt17_Function_handlerIFbN2v88internal12InstanceTypeEEZNKS1_8compiler12MapInference21AnyOfInstanceTypesAreES2_EUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZNK2v88internal8compiler12MapInference21AnyOfInstanceTypesAreENS2_12InstanceTypeEEUlS5_E_E10_M_managerERSt9_Any_dataRKS8_St18_Manager_operation(%rip), %rcx
	movw	%si, -64(%rbp)
	movq	%rcx, %xmm0
	movq	%rax, %xmm1
	leaq	-64(%rbp), %r13
	punpcklqdq	%xmm1, %xmm0
	movq	%r13, %rsi
	movaps	%xmm0, -48(%rbp)
	call	_ZNK2v88internal8compiler12MapInference24AnyOfInstanceTypesUnsafeESt8functionIFbNS0_12InstanceTypeEEE
	movl	%eax, %r12d
	movq	-48(%rbp), %rax
	testq	%rax, %rax
	je	.L234
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	*%rax
.L234:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L243
	addq	$48, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L242:
	.cfi_restore_state
	leaq	.LC4(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L243:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19319:
	.size	_ZNK2v88internal8compiler12MapInference21AnyOfInstanceTypesAreENS0_12InstanceTypeE, .-_ZNK2v88internal8compiler12MapInference21AnyOfInstanceTypesAreENS0_12InstanceTypeE
	.section	.text._ZN2v88internal8compiler12MapInference7GetMapsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler12MapInference7GetMapsEv
	.type	_ZN2v88internal8compiler12MapInference7GetMapsEv, @function
_ZN2v88internal8compiler12MapInference7GetMapsEv:
.LFB19344:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	cmpq	%rax, 24(%rdi)
	je	.L250
	cmpl	$1, 40(%rdi)
	jne	.L246
	movl	$2, 40(%rdi)
.L246:
	leaq	16(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L250:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC3(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19344:
	.size	_ZN2v88internal8compiler12MapInference7GetMapsEv, .-_ZN2v88internal8compiler12MapInference7GetMapsEv
	.section	.text._ZN2v88internal8compiler12MapInference8NoChangeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler12MapInference8NoChangeEv
	.type	_ZN2v88internal8compiler12MapInference8NoChangeEv, @function
_ZN2v88internal8compiler12MapInference8NoChangeEv:
.LFB19350:
	.cfi_startproc
	endbr64
	movl	$0, 40(%rdi)
	movq	16(%rdi), %rax
	cmpq	24(%rdi), %rax
	je	.L252
	movq	%rax, 24(%rdi)
.L252:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE19350:
	.size	_ZN2v88internal8compiler12MapInference8NoChangeEv, .-_ZN2v88internal8compiler12MapInference8NoChangeEv
	.section	.rodata._ZNSt6vectorIN2v88internal6HandleINS1_3MapEEESaIS4_EE15_M_range_insertINS1_13ZoneHandleSetIS3_E14const_iteratorEEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EET_SF_St20forward_iterator_tag.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"vector::_M_range_check: __n (which is %zu) >= this->size() (which is %zu)"
	.section	.rodata._ZNSt6vectorIN2v88internal6HandleINS1_3MapEEESaIS4_EE15_M_range_insertINS1_13ZoneHandleSetIS3_E14const_iteratorEEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EET_SF_St20forward_iterator_tag.str1.1,"aMS",@progbits,1
.LC6:
	.string	"vector::_M_range_insert"
	.section	.text._ZNSt6vectorIN2v88internal6HandleINS1_3MapEEESaIS4_EE15_M_range_insertINS1_13ZoneHandleSetIS3_E14const_iteratorEEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EET_SF_St20forward_iterator_tag,"axG",@progbits,_ZNSt6vectorIN2v88internal6HandleINS1_3MapEEESaIS4_EE15_M_range_insertINS1_13ZoneHandleSetIS3_E14const_iteratorEEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EET_SF_St20forward_iterator_tag,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal6HandleINS1_3MapEEESaIS4_EE15_M_range_insertINS1_13ZoneHandleSetIS3_E14const_iteratorEEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EET_SF_St20forward_iterator_tag
	.type	_ZNSt6vectorIN2v88internal6HandleINS1_3MapEEESaIS4_EE15_M_range_insertINS1_13ZoneHandleSetIS3_E14const_iteratorEEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EET_SF_St20forward_iterator_tag, @function
_ZNSt6vectorIN2v88internal6HandleINS1_3MapEEESaIS4_EE15_M_range_insertINS1_13ZoneHandleSetIS3_E14const_iteratorEEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EET_SF_St20forward_iterator_tag:
.LFB23009:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	xorl	%edi, %edi
	subq	$24, %rsp
	movq	(%rdx), %rsi
	movq	(%rcx), %rdx
	movq	8(%rcx), %r9
	movq	8(%r13), %r8
	cmpq	%rsi, %rdx
	je	.L377
.L254:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L260:
	addq	$1, %rax
	leaq	(%r8,%rax), %rcx
	cmpq	%r9, %rcx
	jne	.L260
	testb	%dil, %dil
	je	.L260
	movq	8(%rbx), %rdi
	movq	16(%rbx), %rcx
	subq	%rdi, %rcx
	sarq	$3, %rcx
	cmpq	%rax, %rcx
	jb	.L378
	movq	%rdi, %rdx
	subq	%r12, %rdx
	movq	%rdx, %r15
	movq	%rdx, -56(%rbp)
	sarq	$3, %r15
	cmpq	%rax, %r15
	jbe	.L261
	salq	$3, %rax
	movq	%rdi, %rdx
	subq	%rax, %rdx
	cmpq	%rdx, %rdi
	je	.L324
	leaq	-8(%rdi), %rsi
	leaq	16(%rdi), %rcx
	subq	%rdx, %rsi
	shrq	$3, %rsi
	cmpq	%rcx, %rdx
	movl	$16, %ecx
	setnb	%r8b
	subq	%rax, %rcx
	testq	%rcx, %rcx
	setle	%cl
	orb	%cl, %r8b
	je	.L325
	movabsq	$2305843009213693948, %rcx
	testq	%rcx, %rsi
	je	.L325
	addq	$1, %rsi
	xorl	%ecx, %ecx
	movq	%rsi, %r8
	shrq	%r8
	salq	$4, %r8
	.p2align 4,,10
	.p2align 3
.L264:
	movdqu	(%rdx,%rcx), %xmm1
	movups	%xmm1, (%rdi,%rcx)
	addq	$16, %rcx
	cmpq	%r8, %rcx
	jne	.L264
	movq	%rsi, %r9
	andq	$-2, %r9
	leaq	0(,%r9,8), %r8
	leaq	(%rdx,%r8), %rcx
	addq	%rdi, %r8
	cmpq	%r9, %rsi
	je	.L266
	movq	(%rcx), %rcx
	movq	%rcx, (%r8)
.L266:
	movq	8(%rbx), %rcx
.L262:
	addq	%rcx, %rax
	movq	%rax, 8(%rbx)
	cmpq	%r12, %rdx
	je	.L267
	subq	%r12, %rdx
	movq	%r12, %rsi
	subq	%rdx, %rdi
	call	memmove@PLT
.L267:
	movq	8(%r13), %rcx
	movq	8(%r14), %rdi
	movq	0(%r13), %rsi
	movq	%rcx, %rax
	negq	%rax
	cmpq	(%r14), %rsi
	je	.L268
	leaq	(%r12,%rax,8), %r9
	jmp	.L272
	.p2align 4,,10
	.p2align 3
.L379:
	movq	(%rdi,%r8,8), %rax
.L271:
	movq	%rax, (%r9,%rcx,8)
	addq	$1, %rcx
.L272:
	movq	(%rsi), %rax
	testb	$3, %al
	je	.L271
	movq	6(%rax), %rdi
	movq	14(%rax), %rdx
	movslq	%ecx, %r8
	subq	%rdi, %rdx
	sarq	$3, %rdx
	cmpq	%r8, %rdx
	ja	.L379
.L270:
	movq	%r8, %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L261:
	leaq	(%r8,%r15), %rdx
	cmpq	$0, -56(%rbp)
	cmovne	%rdx, %r8
	cmpq	%r8, %r9
	je	.L380
	movq	%r8, %rdx
	movq	%r8, %rcx
	negq	%rdx
	leaq	(%rdi,%rdx,8), %r14
	.p2align 4,,10
	.p2align 3
.L285:
	movq	(%rsi), %rdx
	testb	$3, %dl
	je	.L283
	movq	6(%rdx), %r11
	subq	$2, %rdx
	movslq	%ecx, %r10
	movq	16(%rdx), %rdx
	subq	%r11, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %r10
	jnb	.L381
	movq	(%r11,%r10,8), %rdx
.L283:
	movq	%rdx, (%r14,%rcx,8)
	addq	$1, %rcx
	cmpq	%rcx, %r9
	jne	.L285
	movq	8(%rbx), %r9
.L286:
	movq	%rax, %rdx
	subq	%r15, %rdx
	leaq	(%r9,%rdx,8), %rdx
	movq	%rdx, 8(%rbx)
	cmpq	%r12, %rdi
	je	.L281
	subq	%r12, %rdi
	subq	%r15, %rax
	leaq	-8(%rdi), %rcx
	leaq	16(%r9,%rax,8), %rax
	shrq	$3, %rcx
	cmpq	%rax, %r12
	leaq	16(%r12), %rax
	setnb	%dil
	cmpq	%rax, %rdx
	setnb	%al
	orb	%al, %dil
	je	.L326
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rcx
	je	.L326
	leaq	1(%rcx), %rdi
	xorl	%eax, %eax
	movq	%rdi, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L288:
	movdqu	(%r12,%rax), %xmm2
	movups	%xmm2, (%rdx,%rax)
	addq	$16, %rax
	cmpq	%rcx, %rax
	jne	.L288
	movq	%rdi, %r9
	andq	$-2, %r9
	leaq	0(,%r9,8), %rax
	leaq	(%r12,%rax), %rcx
	addq	%rax, %rdx
	cmpq	%r9, %rdi
	je	.L290
	movq	(%rcx), %rax
	movq	%rax, (%rdx)
.L290:
	movq	8(%rbx), %rdx
.L281:
	addq	-56(%rbp), %rdx
	movq	%rdx, 8(%rbx)
	movq	8(%r13), %rcx
	movq	0(%r13), %rdi
	movq	%rcx, %rax
	negq	%rax
	cmpq	%rdi, %rsi
	je	.L291
	leaq	(%r12,%rax,8), %r9
	jmp	.L295
	.p2align 4,,10
	.p2align 3
.L382:
	movq	(%r8,%rsi,8), %rax
.L294:
	movq	%rax, (%r9,%rcx,8)
	addq	$1, %rcx
.L295:
	movq	(%rdi), %rax
	testb	$3, %al
	je	.L294
	movq	6(%rax), %r8
	movq	14(%rax), %rdx
	movslq	%ecx, %rsi
	subq	%r8, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jb	.L382
	jmp	.L374
	.p2align 4,,10
	.p2align 3
.L377:
	movl	$1, %edi
	cmpq	%r8, %r9
	jne	.L254
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L378:
	.cfi_restore_state
	movabsq	$1152921504606846975, %r11
	movq	(%rbx), %r15
	movq	%r11, %rcx
	subq	%r15, %rdi
	sarq	$3, %rdi
	subq	%rdi, %rcx
	cmpq	%rax, %rcx
	jb	.L383
	cmpq	%rax, %rdi
	cmovnb	%rdi, %rax
	addq	%rdi, %rax
	movq	%rax, %rcx
	jc	.L327
	xorl	%r10d, %r10d
	testq	%rax, %rax
	jne	.L384
.L305:
	cmpq	%r15, %r12
	je	.L329
	leaq	-8(%r12), %r11
	leaq	15(%r10), %rax
	subq	%r15, %r11
	subq	%r15, %rax
	movq	%r11, %r13
	shrq	$3, %r13
	cmpq	$30, %rax
	jbe	.L330
	movabsq	$2305843009213693948, %rax
	testq	%rax, %r13
	je	.L330
	addq	$1, %r13
	xorl	%eax, %eax
	movq	%r13, %rdi
	shrq	%rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L308:
	movdqu	(%r15,%rax), %xmm3
	movups	%xmm3, (%r10,%rax)
	addq	$16, %rax
	cmpq	%rdi, %rax
	jne	.L308
	movq	%r13, %r14
	andq	$-2, %r14
	leaq	0(,%r14,8), %rdi
	leaq	(%r15,%rdi), %rax
	addq	%r10, %rdi
	cmpq	%r14, %r13
	je	.L310
	movq	(%rax), %rax
	movq	%rax, (%rdi)
.L310:
	leaq	8(%r10,%r11), %r13
.L306:
	cmpq	%rsi, %rdx
	je	.L311
	movq	(%rsi), %r9
	movq	%r8, %rax
	negq	%rax
	movq	%r9, %rcx
	leaq	0(%r13,%rax,8), %rdi
	leaq	-2(%r9), %rsi
	andl	$3, %ecx
	jmp	.L315
	.p2align 4,,10
	.p2align 3
.L385:
	movq	8(%rsi), %rax
	movq	16(%rsi), %rdx
	movslq	%r8d, %r11
	subq	%rax, %rdx
	sarq	$3, %rdx
	cmpq	%r11, %rdx
	jbe	.L313
	movq	(%rax,%r11,8), %rax
.L314:
	movq	%rax, (%rdi,%r8,8)
	addq	$1, %r8
.L315:
	testq	%rcx, %rcx
	jne	.L385
	movq	%r9, %rax
	jmp	.L314
	.p2align 4,,10
	.p2align 3
.L316:
	movq	(%rsi), %rax
	testb	$3, %al
	je	.L320
	movq	6(%rax), %rdi
	movq	14(%rax), %rdx
	movslq	%r8d, %r11
	subq	%rdi, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %r11
	jnb	.L313
	movq	(%rdi,%r11,8), %rax
.L320:
	movq	%rax, 0(%r13)
	addq	$1, %r8
	addq	$8, %r13
.L311:
	cmpq	%r8, %r9
	jne	.L316
	movq	8(%rbx), %rax
	cmpq	%r12, %rax
	je	.L318
	subq	%r12, %rax
	movq	%r13, %rdi
	movq	%r12, %rsi
	movq	%r10, -64(%rbp)
	movq	%rax, %rdx
	movq	%rcx, -56(%rbp)
	movq	%rax, %r14
	call	memcpy@PLT
	movq	-64(%rbp), %r10
	movq	-56(%rbp), %rcx
	addq	%r14, %r13
.L318:
	testq	%r15, %r15
	je	.L322
	movq	%r15, %rdi
	movq	%r10, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-64(%rbp), %r10
	movq	-56(%rbp), %rcx
.L322:
	movq	%r13, %xmm4
	movq	%r10, %xmm0
	addq	%r10, %rcx
	punpcklqdq	%xmm4, %xmm0
	movq	%rcx, 16(%rbx)
	movups	%xmm0, (%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L327:
	.cfi_restore_state
	movabsq	$9223372036854775800, %rcx
.L304:
	movq	%rcx, %rdi
	movq	%rcx, -56(%rbp)
	call	_Znwm@PLT
	movq	(%rbx), %r15
	movq	0(%r13), %rsi
	movq	8(%r13), %r8
	movq	(%r14), %rdx
	movq	%rax, %r10
	movq	8(%r14), %r9
	movq	-56(%rbp), %rcx
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L380:
	movq	%rdi, %r9
	jmp	.L286
	.p2align 4,,10
	.p2align 3
.L268:
	leaq	(%r12,%rax,8), %r10
	jmp	.L277
	.p2align 4,,10
	.p2align 3
.L386:
	movq	(%rsi), %rax
	testb	$3, %al
	je	.L275
	movq	6(%rax), %r9
	movq	14(%rax), %rdx
	movslq	%ecx, %r8
	subq	%r9, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %r8
	jnb	.L270
	movq	(%r9,%r8,8), %rax
.L275:
	movq	%rax, (%r10,%rcx,8)
	addq	$1, %rcx
.L277:
	cmpq	%rcx, %rdi
	jne	.L386
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L291:
	.cfi_restore_state
	leaq	(%r12,%rax,8), %r10
	jmp	.L300
	.p2align 4,,10
	.p2align 3
.L387:
	movq	(%rdi), %rax
	testb	$3, %al
	je	.L298
	movq	6(%rax), %r9
	movq	14(%rax), %rdx
	movslq	%ecx, %rsi
	subq	%r9, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L374
	movq	(%r9,%rsi,8), %rax
.L298:
	movq	%rax, (%r10,%rcx,8)
	addq	$1, %rcx
.L300:
	cmpq	%r8, %rcx
	jne	.L387
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L384:
	.cfi_restore_state
	cmpq	%r11, %rax
	cmova	%r11, %rcx
	salq	$3, %rcx
	jmp	.L304
.L326:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L287:
	movq	(%r12,%rax,8), %rdi
	movq	%rdi, (%rdx,%rax,8)
	movq	%rax, %rdi
	addq	$1, %rax
	cmpq	%rdi, %rcx
	jne	.L287
	jmp	.L290
.L325:
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L263:
	movq	(%rdx,%rcx,8), %r8
	movq	%r8, (%rdi,%rcx,8)
	movq	%rcx, %r8
	addq	$1, %rcx
	cmpq	%r8, %rsi
	jne	.L263
	jmp	.L266
.L330:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L307:
	movq	(%r15,%rax,8), %rdi
	movq	%rdi, (%r10,%rax,8)
	movq	%rax, %rdi
	addq	$1, %rax
	cmpq	%rdi, %r13
	jne	.L307
	jmp	.L310
.L324:
	movq	%rdi, %rcx
	jmp	.L262
.L329:
	movq	%r10, %r13
	jmp	.L306
.L313:
	movq	%r11, %rsi
.L374:
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L381:
	movq	%r10, %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L383:
	leaq	.LC6(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE23009:
	.size	_ZNSt6vectorIN2v88internal6HandleINS1_3MapEEESaIS4_EE15_M_range_insertINS1_13ZoneHandleSetIS3_E14const_iteratorEEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EET_SF_St20forward_iterator_tag, .-_ZNSt6vectorIN2v88internal6HandleINS1_3MapEEESaIS4_EE15_M_range_insertINS1_13ZoneHandleSetIS3_E14const_iteratorEEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EET_SF_St20forward_iterator_tag
	.section	.text._ZN2v88internal8compiler12MapInferenceC2EPNS1_12JSHeapBrokerEPNS1_4NodeES6_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler12MapInferenceC2EPNS1_12JSHeapBrokerEPNS1_4NodeES6_
	.type	_ZN2v88internal8compiler12MapInferenceC2EPNS1_12JSHeapBrokerEPNS1_4NodeES6_, @function
_ZN2v88internal8compiler12MapInferenceC2EPNS1_12JSHeapBrokerEPNS1_4NodeES6_:
.LFB19304:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-88(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movq	%rdx, %rsi
	movq	%rdi, %xmm0
	movq	%rsi, %xmm1
	leaq	16(%rbx), %r14
	movq	%rcx, %rdx
	punpcklqdq	%xmm1, %xmm0
	movq	%r13, %rcx
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, 32(%rbx)
	movups	%xmm0, (%rbx)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 16(%rbx)
	movq	$1, -88(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties23InferReceiverMapsUnsafeEPNS1_12JSHeapBrokerEPNS1_4NodeES6_PNS0_13ZoneHandleSetINS0_3MapEEE@PLT
	movq	-88(%rbp), %rdx
	movl	%eax, %r12d
	xorl	%eax, %eax
	movq	%rdx, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L389
	movl	$1, %eax
	testq	%rcx, %rcx
	je	.L389
	movq	14(%rdx), %rax
	subq	6(%rdx), %rax
	sarq	$3, %rax
.L389:
	movq	24(%rbx), %rsi
	leaq	-64(%rbp), %rcx
	leaq	-80(%rbp), %rdx
	movq	%r14, %rdi
	movq	%rax, -56(%rbp)
	movq	%r13, -64(%rbp)
	movq	%r13, -80(%rbp)
	movq	$0, -72(%rbp)
	call	_ZNSt6vectorIN2v88internal6HandleINS1_3MapEEESaIS4_EE15_M_range_insertINS1_13ZoneHandleSetIS3_E14const_iteratorEEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EET_SF_St20forward_iterator_tag
	xorl	%eax, %eax
	cmpl	$2, %r12d
	sete	%al
	movl	%eax, 40(%rbx)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L396
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L396:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19304:
	.size	_ZN2v88internal8compiler12MapInferenceC2EPNS1_12JSHeapBrokerEPNS1_4NodeES6_, .-_ZN2v88internal8compiler12MapInferenceC2EPNS1_12JSHeapBrokerEPNS1_4NodeES6_
	.globl	_ZN2v88internal8compiler12MapInferenceC1EPNS1_12JSHeapBrokerEPNS1_4NodeES6_
	.set	_ZN2v88internal8compiler12MapInferenceC1EPNS1_12JSHeapBrokerEPNS1_4NodeES6_,_ZN2v88internal8compiler12MapInferenceC2EPNS1_12JSHeapBrokerEPNS1_4NodeES6_
	.section	.rodata._ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE12emplace_backIJS0_EEEvDpOT_.str1.1,"aMS",@progbits,1
.LC7:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE12emplace_backIJS0_EEEvDpOT_,"axG",@progbits,_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE12emplace_backIJS0_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE12emplace_backIJS0_EEEvDpOT_
	.type	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE12emplace_backIJS0_EEEvDpOT_, @function
_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE12emplace_backIJS0_EEEvDpOT_:
.LFB23063:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	16(%rdi), %r13
	cmpq	24(%rdi), %r13
	je	.L398
	movq	(%rsi), %rax
	movq	%rax, 0(%r13)
	addq	$8, 16(%rdi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L398:
	.cfi_restore_state
	movq	8(%rdi), %r14
	movq	%r13, %rcx
	subq	%r14, %rcx
	movq	%rcx, %rax
	sarq	$3, %rax
	cmpq	$268435455, %rax
	je	.L423
	testq	%rax, %rax
	je	.L410
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L424
	movl	$2147483640, %esi
	movl	$2147483640, %r15d
.L401:
	movq	(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L425
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L404:
	leaq	(%rax,%r15), %rdi
	leaq	8(%rax), %rdx
.L402:
	movq	(%r12), %rsi
	movq	%rsi, (%rax,%rcx)
	cmpq	%r14, %r13
	je	.L405
	leaq	-8(%r13), %rdx
	movq	%rdx, %rcx
	leaq	15(%rax), %rdx
	subq	%r14, %rcx
	subq	%r14, %rdx
	movq	%rcx, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rdx
	jbe	.L413
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rsi
	je	.L413
	leaq	1(%rsi), %r8
	xorl	%edx, %edx
	movq	%r8, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L407:
	movdqu	(%r14,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rsi, %rdx
	jne	.L407
	movq	%r8, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rsi
	addq	%rsi, %r14
	addq	%rax, %rsi
	cmpq	%rdx, %r8
	je	.L409
	movq	(%r14), %rdx
	movq	%rdx, (%rsi)
.L409:
	leaq	16(%rax,%rcx), %rdx
.L405:
	movq	%rax, %xmm0
	movq	%rdx, %xmm2
	movq	%rdi, 24(%rbx)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 8(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L424:
	.cfi_restore_state
	testq	%rdx, %rdx
	jne	.L426
	movl	$8, %edx
	xorl	%edi, %edi
	xorl	%eax, %eax
	jmp	.L402
	.p2align 4,,10
	.p2align 3
.L410:
	movl	$8, %esi
	movl	$8, %r15d
	jmp	.L401
	.p2align 4,,10
	.p2align 3
.L413:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L406:
	movq	(%r14,%rdx,8), %r8
	movq	%r8, (%rax,%rdx,8)
	movq	%rdx, %r8
	addq	$1, %rdx
	cmpq	%r8, %rsi
	jne	.L406
	jmp	.L409
.L425:
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	jmp	.L404
.L423:
	leaq	.LC7(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L426:
	movl	$268435455, %r15d
	cmpq	$268435455, %rdx
	cmova	%r15, %rdx
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rsi
	jmp	.L401
	.cfi_endproc
.LFE23063:
	.size	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE12emplace_backIJS0_EEEvDpOT_, .-_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE12emplace_backIJS0_EEEvDpOT_
	.section	.text._ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_
	.type	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_, @function
_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_:
.LFB23068:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r14
	movq	8(%rdi), %rbx
	movq	%r14, %rax
	subq	%rbx, %rax
	sarq	$3, %rax
	cmpq	$268435455, %rax
	je	.L465
	movq	%rsi, %r8
	movq	%rdi, %r13
	movq	%rsi, %r12
	movq	%rdx, %r15
	subq	%rbx, %r8
	testq	%rax, %rax
	je	.L443
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L466
	movl	$2147483640, %esi
	movl	$2147483640, %edx
.L429:
	movq	0(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	%rsi, %rcx
	jb	.L467
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L432:
	leaq	(%rax,%rdx), %rdi
	leaq	8(%rax), %rcx
	jmp	.L430
	.p2align 4,,10
	.p2align 3
.L466:
	testq	%rdx, %rdx
	jne	.L468
	movl	$8, %ecx
	xorl	%edi, %edi
	xorl	%eax, %eax
.L430:
	movq	(%r15), %rdx
	movq	%rdx, (%rax,%r8)
	cmpq	%rbx, %r12
	je	.L433
	leaq	-8(%r12), %r8
	leaq	15(%rax), %rdx
	subq	%rbx, %r8
	subq	%rbx, %rdx
	movq	%r8, %rcx
	shrq	$3, %rcx
	cmpq	$30, %rdx
	jbe	.L446
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rcx
	je	.L446
	leaq	1(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L435:
	movdqu	(%rbx,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L435
	movq	%rsi, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rcx
	addq	%rcx, %rbx
	addq	%rax, %rcx
	cmpq	%rdx, %rsi
	je	.L437
	movq	(%rbx), %rdx
	movq	%rdx, (%rcx)
.L437:
	leaq	16(%rax,%r8), %rcx
.L433:
	cmpq	%r14, %r12
	je	.L438
	subq	%r12, %r14
	leaq	15(%r12), %rdx
	leaq	-8(%r14), %r9
	subq	%rcx, %rdx
	movq	%r9, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rdx
	jbe	.L447
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rsi
	je	.L447
	leaq	1(%rsi), %r8
	xorl	%edx, %edx
	movq	%r8, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L440:
	movdqu	(%r12,%rdx), %xmm2
	movups	%xmm2, (%rcx,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rsi
	jne	.L440
	movq	%r8, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rsi
	leaq	(%rcx,%rsi), %r10
	addq	%rsi, %r12
	cmpq	%rdx, %r8
	je	.L442
	movq	(%r12), %rdx
	movq	%rdx, (%r10)
.L442:
	leaq	8(%rcx,%r9), %rcx
.L438:
	movq	%rax, %xmm0
	movq	%rcx, %xmm3
	movq	%rdi, 24(%r13)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 8(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L443:
	.cfi_restore_state
	movl	$8, %esi
	movl	$8, %edx
	jmp	.L429
	.p2align 4,,10
	.p2align 3
.L447:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L439:
	movq	(%r12,%rdx,8), %r8
	movq	%r8, (%rcx,%rdx,8)
	movq	%rdx, %r8
	addq	$1, %rdx
	cmpq	%rsi, %r8
	jne	.L439
	jmp	.L442
	.p2align 4,,10
	.p2align 3
.L446:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L434:
	movq	(%rbx,%rdx,8), %rsi
	movq	%rsi, (%rax,%rdx,8)
	movq	%rdx, %rsi
	addq	$1, %rdx
	cmpq	%rsi, %rcx
	jne	.L434
	jmp	.L437
.L467:
	movq	%r8, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %r8
	jmp	.L432
.L465:
	leaq	.LC7(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L468:
	cmpq	$268435455, %rdx
	movl	$268435455, %eax
	cmova	%rax, %rdx
	salq	$3, %rdx
	movq	%rdx, %rsi
	jmp	.L429
	.cfi_endproc
.LFE23068:
	.size	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_, .-_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_
	.section	.rodata._ZN2v88internal8compiler12MapInference15InsertMapChecksEPNS1_7JSGraphEPPNS1_4NodeES6_RKNS1_14FeedbackSourceE.str1.1,"aMS",@progbits,1
.LC8:
	.string	"feedback.IsValid()"
.LC9:
	.string	"vector::reserve"
	.section	.text._ZN2v88internal8compiler12MapInference15InsertMapChecksEPNS1_7JSGraphEPPNS1_4NodeES6_RKNS1_14FeedbackSourceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler12MapInference15InsertMapChecksEPNS1_7JSGraphEPPNS1_4NodeES6_RKNS1_14FeedbackSourceE
	.type	_ZN2v88internal8compiler12MapInference15InsertMapChecksEPNS1_7JSGraphEPPNS1_4NodeES6_RKNS1_14FeedbackSourceE, @function
_ZN2v88internal8compiler12MapInference15InsertMapChecksEPNS1_7JSGraphEPPNS1_4NodeES6_RKNS1_14FeedbackSourceE:
.LFB19345:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -120(%rbp)
	movq	24(%rdi), %r15
	movq	%rsi, -104(%rbp)
	movq	16(%rdi), %r9
	movq	%rdx, -128(%rbp)
	movq	%rcx, -136(%rbp)
	movq	%r8, -112(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	%r15, %r9
	je	.L520
	cmpq	$0, (%r8)
	je	.L471
	cmpl	$-1, 8(%r8)
	je	.L471
	movl	$1, %r11d
	movq	(%rsi), %r10
	movq	%r9, %r13
	movq	%r11, %rbx
	jmp	.L502
	.p2align 4,,10
	.p2align 3
.L523:
	testq	%rax, %rax
	jne	.L474
	cmpq	%rdx, %rbx
	je	.L501
	movq	16(%r12), %r14
	movq	24(%r12), %rax
	subq	%r14, %rax
	cmpq	$31, %rax
	jbe	.L521
	leaq	32(%r14), %rax
	movq	%rax, 16(%r12)
.L477:
	movq	%r12, (%r14)
	movq	$0, 8(%r14)
	movq	$0, 16(%r14)
	movq	$0, 24(%r14)
	cmpq	-96(%rbp), %rbx
	jnb	.L478
	leaq	-88(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rbx, -88(%rbp)
	call	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE12emplace_backIJS0_EEEvDpOT_
	movq	16(%r14), %rsi
	cmpq	24(%r14), %rsi
	je	.L479
	movq	-96(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 16(%r14)
.L480:
	movq	-104(%rbp), %rax
	movq	%r14, %rbx
	orq	$2, %rbx
	movq	(%rax), %r10
.L501:
	addq	$8, %r13
	cmpq	%r13, %r15
	je	.L522
.L502:
	movq	0(%r13), %rdx
	movq	%rbx, %rax
	movq	(%r10), %r12
	andl	$3, %eax
	movq	%rdx, -96(%rbp)
	cmpq	$1, %rax
	jne	.L523
	addq	$8, %r13
	movq	%rdx, %rbx
	cmpq	%r13, %r15
	jne	.L502
.L522:
	movq	%rbx, %r11
	movq	-128(%rbp), %rbx
	movq	-120(%rbp), %r14
	xorl	%esi, %esi
	movq	-112(%rbp), %rcx
	movq	%r11, %rdx
	movq	%r10, -144(%rbp)
	movq	(%rbx), %rax
	movq	8(%r14), %r15
	movq	%rax, %r13
	movq	-104(%rbp), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9CheckMapsENS_4base5FlagsINS1_13CheckMapsFlagEiEENS0_13ZoneHandleSetINS0_3MapEEERKNS1_14FeedbackSourceE@PLT
	movq	-144(%rbp), %r10
	movq	%r13, %xmm1
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movq	%r15, %xmm0
	leaq	-80(%rbp), %rcx
	movq	-136(%rbp), %rax
	punpcklqdq	%xmm1, %xmm0
	movl	$3, %edx
	movq	%r10, %rdi
	movq	%rax, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, (%rbx)
	movl	$0, 40(%r14)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L524
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L474:
	.cfi_restore_state
	movq	6(%rbx), %rdi
	movq	14(%rbx), %rsi
	leaq	-2(%rbx), %r14
	subq	%rdi, %rsi
	sarq	$3, %rsi
	je	.L482
	xorl	%eax, %eax
	jmp	.L483
	.p2align 4,,10
	.p2align 3
.L525:
	addq	$1, %rax
	cmpq	%rsi, %rax
	je	.L482
.L483:
	cmpq	%rdx, (%rdi,%rax,8)
	je	.L501
	jbe	.L525
.L482:
	movq	16(%r12), %rbx
	movq	24(%r12), %rax
	subq	%rbx, %rax
	cmpq	$31, %rax
	jbe	.L526
	leaq	32(%rbx), %rax
	movq	%rax, 16(%r12)
.L485:
	movq	%r12, (%rbx)
	movq	$0, 8(%rbx)
	movq	$0, 16(%rbx)
	movq	$0, 24(%rbx)
	movq	8(%r14), %rdx
	movq	16(%r14), %r8
	subq	%rdx, %r8
	movq	%r8, %rax
	sarq	$3, %rax
	addq	$1, %rax
	cmpq	$268435455, %rax
	ja	.L527
	xorl	%edi, %edi
	xorl	%esi, %esi
	testq	%rax, %rax
	jne	.L528
.L487:
	xorl	%r12d, %r12d
	jmp	.L494
	.p2align 4,,10
	.p2align 3
.L529:
	movq	%rax, (%rsi)
	movq	16(%rbx), %rax
	leaq	8(%rax), %rsi
	movq	%rsi, 16(%rbx)
.L518:
	movq	8(%r14), %rdx
	movq	16(%r14), %rax
	addq	$1, %r12
	movq	24(%rbx), %rdi
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%r12, %rax
	jbe	.L490
.L494:
	leaq	(%rdx,%r12,8), %rdx
	movq	(%rdx), %rax
	cmpq	-96(%rbp), %rax
	ja	.L490
	cmpq	%rdi, %rsi
	jne	.L529
	movq	%rbx, %rdi
	call	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_
	movq	16(%rbx), %rsi
	jmp	.L518
	.p2align 4,,10
	.p2align 3
.L528:
	movq	16(%r12), %rsi
	movq	24(%r12), %rdx
	leaq	15(%r8), %r9
	andq	$-8, %r9
	subq	%rsi, %rdx
	cmpq	%rdx, %r9
	ja	.L530
	addq	%rsi, %r9
	movq	%r9, 16(%r12)
.L489:
	leaq	8(%rsi,%r8), %rdi
	movq	%rsi, 8(%rbx)
	movq	%rsi, 16(%rbx)
	movq	%rdi, 24(%rbx)
	movq	8(%r14), %rdx
	cmpq	%rdx, 16(%r14)
	jne	.L487
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L490:
	cmpq	%rdi, %rsi
	je	.L495
	movq	-96(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 16(%rbx)
.L496:
	movq	8(%r14), %rdx
	movq	16(%r14), %rax
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%r12, %rax
	jbe	.L500
.L497:
	movq	16(%rbx), %rsi
	leaq	(%rdx,%r12,8), %rdx
	cmpq	24(%rbx), %rsi
	je	.L498
	movq	(%rdx), %rax
	addq	$1, %r12
	movq	%rax, (%rsi)
	addq	$8, 16(%rbx)
	movq	8(%r14), %rdx
	movq	16(%r14), %rax
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rax, %r12
	jb	.L497
.L500:
	movq	-104(%rbp), %rax
	orq	$2, %rbx
	movq	(%rax), %r10
	jmp	.L501
	.p2align 4,,10
	.p2align 3
.L498:
	movq	%rbx, %rdi
	addq	$1, %r12
	call	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_
	jmp	.L496
	.p2align 4,,10
	.p2align 3
.L478:
	xorl	%esi, %esi
	movq	%r14, %rdi
	leaq	-96(%rbp), %rdx
	call	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_
	leaq	-88(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rbx, -88(%rbp)
	call	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE12emplace_backIJS0_EEEvDpOT_
	jmp	.L480
	.p2align 4,,10
	.p2align 3
.L495:
	leaq	-96(%rbp), %rdx
	movq	%rbx, %rdi
	call	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_
	jmp	.L496
	.p2align 4,,10
	.p2align 3
.L471:
	leaq	.LC8(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L521:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r14
	jmp	.L477
	.p2align 4,,10
	.p2align 3
.L479:
	leaq	-96(%rbp), %rdx
	movq	%r14, %rdi
	call	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_
	jmp	.L480
	.p2align 4,,10
	.p2align 3
.L526:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rbx
	jmp	.L485
	.p2align 4,,10
	.p2align 3
.L520:
	leaq	.LC3(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L530:
	movq	%r9, %rsi
	movq	%r12, %rdi
	movq	%r8, -144(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-144(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L489
.L527:
	leaq	.LC9(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L524:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19345:
	.size	_ZN2v88internal8compiler12MapInference15InsertMapChecksEPNS1_7JSGraphEPPNS1_4NodeES6_RKNS1_14FeedbackSourceE, .-_ZN2v88internal8compiler12MapInference15InsertMapChecksEPNS1_7JSGraphEPPNS1_4NodeES6_RKNS1_14FeedbackSourceE
	.section	.text._ZN2v88internal8compiler12MapInference16RelyOnMapsHelperEPNS1_23CompilationDependenciesEPNS1_7JSGraphEPPNS1_4NodeES8_RKNS1_14FeedbackSourceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler12MapInference16RelyOnMapsHelperEPNS1_23CompilationDependenciesEPNS1_7JSGraphEPPNS1_4NodeES8_RKNS1_14FeedbackSourceE
	.type	_ZN2v88internal8compiler12MapInference16RelyOnMapsHelperEPNS1_23CompilationDependenciesEPNS1_7JSGraphEPPNS1_4NodeES8_RKNS1_14FeedbackSourceE, @function
_ZN2v88internal8compiler12MapInference16RelyOnMapsHelperEPNS1_23CompilationDependenciesEPNS1_7JSGraphEPPNS1_4NodeES8_RKNS1_14FeedbackSourceE:
.LFB19348:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -96(%rbp)
	movq	%rcx, -104(%rbp)
	movq	%r8, -112(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$2, 40(%rdi)
	movl	$1, %eax
	jne	.L531
	movq	%rdi, %r12
	movq	%rsi, %r13
	movq	%r9, %rbx
	testq	%rsi, %rsi
	je	.L533
	movq	24(%rdi), %rax
	movq	16(%rdi), %r14
	movq	%rax, -120(%rbp)
	subq	%r14, %rax
	movq	%rax, %rdx
	sarq	$5, %rax
	sarq	$3, %rdx
	testq	%rax, %rax
	jle	.L534
	salq	$5, %rax
	leaq	-80(%rbp), %r15
	addq	%r14, %rax
	movq	%rax, -88(%rbp)
	jmp	.L539
	.p2align 4,,10
	.p2align 3
.L580:
	movq	8(%r14), %rdx
	movq	(%r12), %rsi
	movq	%r15, %rdi
	movl	$1, %ecx
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler6MapRef9is_stableEv@PLT
	testb	%al, %al
	je	.L576
	movq	16(%r14), %rdx
	movq	(%r12), %rsi
	movq	%r15, %rdi
	movl	$1, %ecx
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler6MapRef9is_stableEv@PLT
	testb	%al, %al
	je	.L577
	movq	24(%r14), %rdx
	movq	(%r12), %rsi
	movq	%r15, %rdi
	movl	$1, %ecx
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler6MapRef9is_stableEv@PLT
	testb	%al, %al
	je	.L578
	addq	$32, %r14
	cmpq	-88(%rbp), %r14
	je	.L579
.L539:
	movq	(%r14), %rdx
	movq	(%r12), %rsi
	movq	%r15, %rdi
	movl	$1, %ecx
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler6MapRef9is_stableEv@PLT
	testb	%al, %al
	jne	.L580
.L535:
	cmpq	%r14, -120(%rbp)
	je	.L544
.L533:
	xorl	%eax, %eax
	cmpq	$0, (%rbx)
	je	.L531
	cmpl	$-1, 8(%rbx)
	je	.L531
	movq	-112(%rbp), %rcx
	movq	-104(%rbp), %rdx
	movq	%rbx, %r8
	movq	%r12, %rdi
	movq	-96(%rbp), %rsi
	call	_ZN2v88internal8compiler12MapInference15InsertMapChecksEPNS1_7JSGraphEPPNS1_4NodeES6_RKNS1_14FeedbackSourceE
	movl	$1, %eax
.L531:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L581
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L579:
	.cfi_restore_state
	movq	-120(%rbp), %rdx
	subq	%r14, %rdx
	sarq	$3, %rdx
.L534:
	leaq	-80(%rbp), %r15
	cmpq	$2, %rdx
	je	.L540
	cmpq	$3, %rdx
	je	.L541
	leaq	-80(%rbp), %r15
	cmpq	$1, %rdx
	je	.L542
.L544:
	movq	24(%r12), %r14
	movq	16(%r12), %rbx
	leaq	-80(%rbp), %r15
	cmpq	%r14, %rbx
	je	.L547
	.p2align 4,,10
	.p2align 3
.L546:
	movq	(%rbx), %rdx
	movq	(%r12), %rsi
	movl	$1, %ecx
	movq	%r15, %rdi
	addq	$8, %rbx
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler23CompilationDependencies17DependOnStableMapERKNS1_6MapRefE@PLT
	cmpq	%rbx, %r14
	jne	.L546
.L547:
	movl	$0, 40(%r12)
	movl	$1, %eax
	jmp	.L531
	.p2align 4,,10
	.p2align 3
.L576:
	addq	$8, %r14
	jmp	.L535
	.p2align 4,,10
	.p2align 3
.L577:
	addq	$16, %r14
	jmp	.L535
	.p2align 4,,10
	.p2align 3
.L578:
	addq	$24, %r14
	jmp	.L535
	.p2align 4,,10
	.p2align 3
.L541:
	leaq	-80(%rbp), %r15
	movq	(%r14), %rdx
	movq	(%r12), %rsi
	movl	$1, %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler6MapRef9is_stableEv@PLT
	testb	%al, %al
	je	.L535
	addq	$8, %r14
.L540:
	movq	(%r14), %rdx
	movq	(%r12), %rsi
	movq	%r15, %rdi
	movl	$1, %ecx
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler6MapRef9is_stableEv@PLT
	testb	%al, %al
	je	.L535
	addq	$8, %r14
.L542:
	movq	(%r14), %rdx
	movq	(%r12), %rsi
	movq	%r15, %rdi
	movl	$1, %ecx
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler6MapRef9is_stableEv@PLT
	testb	%al, %al
	jne	.L544
	jmp	.L535
.L581:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19348:
	.size	_ZN2v88internal8compiler12MapInference16RelyOnMapsHelperEPNS1_23CompilationDependenciesEPNS1_7JSGraphEPPNS1_4NodeES8_RKNS1_14FeedbackSourceE, .-_ZN2v88internal8compiler12MapInference16RelyOnMapsHelperEPNS1_23CompilationDependenciesEPNS1_7JSGraphEPPNS1_4NodeES8_RKNS1_14FeedbackSourceE
	.section	.text._ZN2v88internal8compiler12MapInference22RelyOnMapsViaStabilityEPNS1_23CompilationDependenciesE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler12MapInference22RelyOnMapsViaStabilityEPNS1_23CompilationDependenciesE
	.type	_ZN2v88internal8compiler12MapInference22RelyOnMapsViaStabilityEPNS1_23CompilationDependenciesE, @function
_ZN2v88internal8compiler12MapInference22RelyOnMapsViaStabilityEPNS1_23CompilationDependenciesE:
.LFB19346:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	cmpq	%rax, 24(%rdi)
	je	.L586
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	$0, -32(%rbp)
	leaq	-32(%rbp), %r9
	movl	$-1, -24(%rbp)
	call	_ZN2v88internal8compiler12MapInference16RelyOnMapsHelperEPNS1_23CompilationDependenciesEPNS1_7JSGraphEPPNS1_4NodeES8_RKNS1_14FeedbackSourceE
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L587
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L586:
	.cfi_restore_state
	leaq	.LC3(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L587:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19346:
	.size	_ZN2v88internal8compiler12MapInference22RelyOnMapsViaStabilityEPNS1_23CompilationDependenciesE, .-_ZN2v88internal8compiler12MapInference22RelyOnMapsViaStabilityEPNS1_23CompilationDependenciesE
	.section	.rodata._ZN2v88internal8compiler12MapInference25RelyOnMapsPreferStabilityEPNS1_23CompilationDependenciesEPNS1_7JSGraphEPPNS1_4NodeES8_RKNS1_14FeedbackSourceE.str1.8,"aMS",@progbits,1
	.align 8
.LC10:
	.string	"RelyOnMapsHelper(nullptr, jsgraph, effect, control, feedback)"
	.section	.text._ZN2v88internal8compiler12MapInference25RelyOnMapsPreferStabilityEPNS1_23CompilationDependenciesEPNS1_7JSGraphEPPNS1_4NodeES8_RKNS1_14FeedbackSourceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler12MapInference25RelyOnMapsPreferStabilityEPNS1_23CompilationDependenciesEPNS1_7JSGraphEPPNS1_4NodeES8_RKNS1_14FeedbackSourceE
	.type	_ZN2v88internal8compiler12MapInference25RelyOnMapsPreferStabilityEPNS1_23CompilationDependenciesEPNS1_7JSGraphEPPNS1_4NodeES8_RKNS1_14FeedbackSourceE, @function
_ZN2v88internal8compiler12MapInference25RelyOnMapsPreferStabilityEPNS1_23CompilationDependenciesEPNS1_7JSGraphEPPNS1_4NodeES8_RKNS1_14FeedbackSourceE:
.LFB19347:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	cmpq	%rax, 16(%rdi)
	je	.L595
	cmpl	$2, 40(%rdi)
	movq	%rdi, %r12
	je	.L596
.L590:
	xorl	%eax, %eax
.L588:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L597
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L596:
	.cfi_restore_state
	movq	%rdx, %r13
	movq	%rcx, %r14
	movq	%r8, %r15
	movq	%r9, %rbx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	$0, -80(%rbp)
	leaq	-80(%rbp), %r9
	movl	$-1, -72(%rbp)
	call	_ZN2v88internal8compiler12MapInference16RelyOnMapsHelperEPNS1_23CompilationDependenciesEPNS1_7JSGraphEPPNS1_4NodeES8_RKNS1_14FeedbackSourceE
	testb	%al, %al
	jne	.L588
	cmpl	$2, 40(%r12)
	movb	%al, -81(%rbp)
	jne	.L590
	cmpq	$0, (%rbx)
	je	.L592
	cmpl	$-1, 8(%rbx)
	je	.L592
	movq	%rbx, %r8
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12MapInference15InsertMapChecksEPNS1_7JSGraphEPPNS1_4NodeES6_RKNS1_14FeedbackSourceE
	movzbl	-81(%rbp), %eax
	jmp	.L588
	.p2align 4,,10
	.p2align 3
.L595:
	leaq	.LC3(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L592:
	leaq	.LC10(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L597:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19347:
	.size	_ZN2v88internal8compiler12MapInference25RelyOnMapsPreferStabilityEPNS1_23CompilationDependenciesEPNS1_7JSGraphEPPNS1_4NodeES8_RKNS1_14FeedbackSourceE, .-_ZN2v88internal8compiler12MapInference25RelyOnMapsPreferStabilityEPNS1_23CompilationDependenciesEPNS1_7JSGraphEPPNS1_4NodeES8_RKNS1_14FeedbackSourceE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler12MapInferenceC2EPNS1_12JSHeapBrokerEPNS1_4NodeES6_,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler12MapInferenceC2EPNS1_12JSHeapBrokerEPNS1_4NodeES6_, @function
_GLOBAL__sub_I__ZN2v88internal8compiler12MapInferenceC2EPNS1_12JSHeapBrokerEPNS1_4NodeES6_:
.LFB23585:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE23585:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler12MapInferenceC2EPNS1_12JSHeapBrokerEPNS1_4NodeES6_, .-_GLOBAL__sub_I__ZN2v88internal8compiler12MapInferenceC2EPNS1_12JSHeapBrokerEPNS1_4NodeES6_
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler12MapInferenceC2EPNS1_12JSHeapBrokerEPNS1_4NodeES6_
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
