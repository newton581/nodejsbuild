	.file	"machine-graph-verifier.cc"
	.text
	.section	.text._ZNKSt5ctypeIcE8do_widenEc,"axG",@progbits,_ZNKSt5ctypeIcE8do_widenEc,comdat
	.align 2
	.p2align 4
	.weak	_ZNKSt5ctypeIcE8do_widenEc
	.type	_ZNKSt5ctypeIcE8do_widenEc, @function
_ZNKSt5ctypeIcE8do_widenEc:
.LFB1554:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	ret
	.cfi_endproc
.LFE1554:
	.size	_ZNKSt5ctypeIcE8do_widenEc, .-_ZNKSt5ctypeIcE8do_widenEc
	.section	.rodata._ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker27CheckValueInputForFloat64OpEPKNS1_4NodeEi.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"vector::_M_range_check: __n (which is %zu) >= this->size() (which is %zu)"
	.section	.rodata._ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker27CheckValueInputForFloat64OpEPKNS1_4NodeEi.isra.0.str1.1,"aMS",@progbits,1
.LC1:
	.string	"TypeError: node #"
.LC2:
	.string	":"
.LC3:
	.string	" uses node #"
	.section	.rodata._ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker27CheckValueInputForFloat64OpEPKNS1_4NodeEi.isra.0.str1.8
	.align 8
.LC4:
	.string	" which doesn't have a kFloat64 representation."
	.section	.rodata._ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker27CheckValueInputForFloat64OpEPKNS1_4NodeEi.isra.0.str1.1
.LC5:
	.string	"%s"
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker27CheckValueInputForFloat64OpEPKNS1_4NodeEi.isra.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker27CheckValueInputForFloat64OpEPKNS1_4NodeEi.isra.0, @function
_ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker27CheckValueInputForFloat64OpEPKNS1_4NodeEi.isra.0:
.LFB16029:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edx, %rdx
	salq	$3, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$424, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L4
	leaq	32(%rsi), %rcx
	addq	%rcx, %rdx
.L5:
	movq	(%rdx), %r12
	movq	24(%rdi), %rax
	movq	32(%rdi), %rdx
	movl	20(%r12), %esi
	subq	%rax, %rdx
	andl	$16777215, %esi
	cmpq	%rdx, %rsi
	jnb	.L11
	cmpb	$13, (%rax,%rsi)
	jne	.L12
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L13
	addq	$424, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	.cfi_restore_state
	movq	32(%rsi), %rax
	leaq	16(%rax,%rdx), %rdx
	jmp	.L5
.L13:
	call	__stack_chk_fail@PLT
.L12:
	leaq	-416(%rbp), %r13
	movq	%r13, %rdi
	call	_ZNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
	movl	$17, %edx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	20(%rbx), %esi
	movq	%r13, %rdi
	andl	$16777215, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_8OperatorE@PLT
	movl	$12, %edx
	leaq	.LC3(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	20(%r12), %esi
	movq	%r13, %rdi
	andl	$16777215, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r12), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_8OperatorE@PLT
	leaq	.LC4(%rip), %rsi
	movq	%rax, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	leaq	-448(%rbp), %rdi
	leaq	-408(%rbp), %rsi
	call	_ZNKSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE3strEv@PLT
	movq	-448(%rbp), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L11:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE16029:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker27CheckValueInputForFloat64OpEPKNS1_4NodeEi.isra.0, .-_ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker27CheckValueInputForFloat64OpEPKNS1_4NodeEi.isra.0
	.section	.rodata._ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker23CheckValueInputIsTaggedEPKNS1_4NodeEi.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	" which doesn't have a tagged representation."
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker23CheckValueInputIsTaggedEPKNS1_4NodeEi.isra.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker23CheckValueInputIsTaggedEPKNS1_4NodeEi.isra.0, @function
_ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker23CheckValueInputIsTaggedEPKNS1_4NodeEi.isra.0:
.LFB16032:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edx, %rdx
	salq	$3, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$424, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L15
	leaq	32(%rsi), %rcx
	addq	%rcx, %rdx
.L16:
	movq	(%rdx), %r12
	movq	24(%rdi), %rax
	movq	32(%rdi), %rdx
	movl	20(%r12), %esi
	subq	%rax, %rdx
	andl	$16777215, %esi
	cmpq	%rdx, %rsi
	jnb	.L21
	movzbl	(%rax,%rsi), %eax
	subl	$6, %eax
	cmpb	$2, %al
	ja	.L22
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L23
	addq	$424, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	.cfi_restore_state
	movq	32(%rsi), %rax
	leaq	16(%rax,%rdx), %rdx
	jmp	.L16
.L23:
	call	__stack_chk_fail@PLT
.L22:
	leaq	-416(%rbp), %r13
	movq	%r13, %rdi
	call	_ZNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
	movl	$17, %edx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	20(%rbx), %esi
	movq	%r13, %rdi
	andl	$16777215, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_8OperatorE@PLT
	movl	$12, %edx
	leaq	.LC3(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	20(%r12), %esi
	movq	%r13, %rdi
	andl	$16777215, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r12), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_8OperatorE@PLT
	leaq	.LC6(%rip), %rsi
	movq	%rax, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	leaq	-448(%rbp), %rdi
	leaq	-408(%rbp), %rsi
	call	_ZNKSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE3strEv@PLT
	movq	-448(%rbp), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L21:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE16032:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker23CheckValueInputIsTaggedEPKNS1_4NodeEi.isra.0, .-_ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker23CheckValueInputIsTaggedEPKNS1_4NodeEi.isra.0
	.section	.rodata._ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker32CheckValueInputIsTaggedOrPointerEPKNS1_4NodeEi.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC7:
	.string	" which doesn't have a tagged or pointer representation."
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker32CheckValueInputIsTaggedOrPointerEPKNS1_4NodeEi.isra.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker32CheckValueInputIsTaggedOrPointerEPKNS1_4NodeEi.isra.0, @function
_ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker32CheckValueInputIsTaggedOrPointerEPKNS1_4NodeEi.isra.0:
.LFB16030:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edx, %rdx
	salq	$3, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$424, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L25
	leaq	32(%rsi), %rcx
	addq	%rcx, %rdx
.L26:
	movq	(%rdx), %r12
	movq	24(%rdi), %rax
	movq	32(%rdi), %rdx
	movl	20(%r12), %esi
	subq	%rax, %rdx
	andl	$16777215, %esi
	cmpq	%rdx, %rsi
	jnb	.L31
	movzbl	(%rax,%rsi), %eax
	subl	$5, %eax
	cmpb	$3, %al
	ja	.L32
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L33
	addq	$424, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_restore_state
	movq	32(%rsi), %rax
	leaq	16(%rax,%rdx), %rdx
	jmp	.L26
.L33:
	call	__stack_chk_fail@PLT
.L31:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L32:
	leaq	-416(%rbp), %r13
	movq	%r13, %rdi
	call	_ZNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
	movl	$17, %edx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	20(%rbx), %esi
	movq	%r13, %rdi
	andl	$16777215, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_8OperatorE@PLT
	movl	$12, %edx
	leaq	.LC3(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	20(%r12), %esi
	movq	%r13, %rdi
	andl	$16777215, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r12), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_8OperatorE@PLT
	leaq	.LC7(%rip), %rsi
	movq	%rax, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	leaq	-448(%rbp), %rdi
	leaq	-408(%rbp), %rsi
	call	_ZNKSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE3strEv@PLT
	movq	-448(%rbp), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE16030:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker32CheckValueInputIsTaggedOrPointerEPKNS1_4NodeEi.isra.0, .-_ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker32CheckValueInputIsTaggedOrPointerEPKNS1_4NodeEi.isra.0
	.section	.rodata._ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker31CheckValueInputRepresentationIsEPKNS1_4NodeEiNS0_21MachineRepresentationE.isra.0.str1.1,"aMS",@progbits,1
.LC8:
	.string	" which doesn't have a "
.LC9:
	.string	" representation."
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker31CheckValueInputRepresentationIsEPKNS1_4NodeEiNS0_21MachineRepresentationE.isra.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker31CheckValueInputRepresentationIsEPKNS1_4NodeEiNS0_21MachineRepresentationE.isra.0, @function
_ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker31CheckValueInputRepresentationIsEPKNS1_4NodeEiNS0_21MachineRepresentationE.isra.0:
.LFB16034:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edx, %rdx
	salq	$3, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$448, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L35
	addq	$32, %rsi
	addq	%rsi, %rdx
.L36:
	movq	(%rdx), %r12
	movq	24(%rdi), %rax
	movq	32(%rdi), %rdx
	movl	20(%r12), %esi
	subq	%rax, %rdx
	andl	$16777215, %esi
	cmpq	%rdx, %rsi
	jnb	.L41
	movzbl	(%rax,%rsi), %r13d
	cmpb	%r13b, %cl
	jne	.L42
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L43
	addq	$448, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore_state
	movq	32(%rsi), %rax
	leaq	16(%rax,%rdx), %rdx
	jmp	.L36
.L43:
	call	__stack_chk_fail@PLT
.L42:
	leaq	-416(%rbp), %r14
	leaq	-432(%rbp), %rdi
	movl	%ecx, -468(%rbp)
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
	movl	$17, %edx
	movq	%r14, %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	20(%rbx), %esi
	movq	%r14, %rdi
	andl	$16777215, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r14
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_8OperatorE@PLT
	movl	$12, %edx
	leaq	.LC3(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r14
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	20(%r12), %esi
	movq	%r14, %rdi
	andl	$16777215, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r14
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r12), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_8OperatorE@PLT
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rdi
	movl	%r13d, %esi
	call	_ZN2v88internallsERSoNS0_21MachineRepresentationE@PLT
	movl	$22, %edx
	leaq	.LC8(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	-468(%rbp), %ecx
	movq	%r12, %rdi
	movl	%ecx, %esi
	call	_ZN2v88internallsERSoNS0_21MachineRepresentationE@PLT
	leaq	.LC9(%rip), %rsi
	movq	%rax, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	leaq	-464(%rbp), %rdi
	leaq	-408(%rbp), %rsi
	call	_ZNKSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE3strEv@PLT
	movq	-464(%rbp), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L41:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE16034:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker31CheckValueInputRepresentationIsEPKNS1_4NodeEiNS0_21MachineRepresentationE.isra.0, .-_ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker31CheckValueInputRepresentationIsEPKNS1_4NodeEiNS0_21MachineRepresentationE.isra.0
	.section	.rodata._ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker25CheckValueInputForInt32OpEPKNS1_4NodeEi.isra.0.str1.1,"aMS",@progbits,1
.LC10:
	.string	" is untyped."
	.section	.rodata._ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker25CheckValueInputForInt32OpEPKNS1_4NodeEi.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC11:
	.string	" which doesn't have an int32-compatible representation."
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker25CheckValueInputForInt32OpEPKNS1_4NodeEi.isra.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker25CheckValueInputForInt32OpEPKNS1_4NodeEi.isra.0, @function
_ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker25CheckValueInputForInt32OpEPKNS1_4NodeEi.isra.0:
.LFB16033:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edx, %rdx
	salq	$3, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$424, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L45
	leaq	32(%rsi), %rcx
	addq	%rcx, %rdx
.L46:
	movq	(%rdx), %r12
	movq	24(%rdi), %rax
	movq	32(%rdi), %rdx
	movl	20(%r12), %esi
	subq	%rax, %rdx
	andl	$16777215, %esi
	cmpq	%rdx, %rsi
	jnb	.L55
	movzbl	(%rax,%rsi), %eax
	testb	%al, %al
	je	.L48
	subl	$1, %eax
	cmpb	$3, %al
	ja	.L56
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L57
	addq	$424, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	.cfi_restore_state
	movq	32(%rsi), %rax
	leaq	16(%rax,%rdx), %rdx
	jmp	.L46
.L56:
	leaq	-416(%rbp), %r13
	movq	%r13, %rdi
	call	_ZNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
	movl	$17, %edx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	20(%rbx), %esi
	movq	%r13, %rdi
	andl	$16777215, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_8OperatorE@PLT
	movl	$12, %edx
	leaq	.LC3(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	20(%r12), %esi
	movq	%r13, %rdi
	andl	$16777215, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r12), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_8OperatorE@PLT
	leaq	.LC11(%rip), %rsi
	movq	%rax, %rdi
.L54:
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	leaq	-448(%rbp), %rdi
	leaq	-408(%rbp), %rsi
	call	_ZNKSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE3strEv@PLT
	movq	-448(%rbp), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L48:
	leaq	-416(%rbp), %r13
	movq	%r13, %rdi
	call	_ZNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
	movl	$17, %edx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	20(%r12), %esi
	movq	%r13, %rdi
	andl	$16777215, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r12), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_8OperatorE@PLT
	leaq	.LC10(%rip), %rsi
	movq	%rax, %rdi
	jmp	.L54
.L57:
	call	__stack_chk_fail@PLT
.L55:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE16033:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker25CheckValueInputForInt32OpEPKNS1_4NodeEi.isra.0, .-_ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker25CheckValueInputForInt32OpEPKNS1_4NodeEi.isra.0
	.section	.rodata._ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker25CheckValueInputForInt64OpEPKNS1_4NodeEi.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC12:
	.string	" which doesn't have a kWord64 representation."
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker25CheckValueInputForInt64OpEPKNS1_4NodeEi.isra.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker25CheckValueInputForInt64OpEPKNS1_4NodeEi.isra.0, @function
_ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker25CheckValueInputForInt64OpEPKNS1_4NodeEi.isra.0:
.LFB16027:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edx, %rdx
	salq	$3, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$416, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L59
	leaq	32(%rsi), %rcx
	addq	%rcx, %rdx
.L60:
	movq	(%rdx), %r13
	movq	24(%rdi), %rax
	movq	32(%rdi), %rdx
	movl	20(%r13), %esi
	subq	%rax, %rdx
	andl	$16777215, %esi
	cmpq	%rdx, %rsi
	jnb	.L69
	movzbl	(%rax,%rsi), %r12d
	testb	%r12b, %r12b
	je	.L62
	cmpb	$5, %r12b
	jne	.L70
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L71
	addq	$416, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore_state
	movq	32(%rsi), %rax
	leaq	16(%rax,%rdx), %rdx
	jmp	.L60
.L70:
	leaq	-416(%rbp), %r14
	movq	%r14, %rdi
	call	_ZNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
	movl	$17, %edx
	movq	%r14, %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	20(%rbx), %esi
	movq	%r14, %rdi
	andl	$16777215, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r14
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_8OperatorE@PLT
	movl	$12, %edx
	leaq	.LC3(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r14
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	20(%r13), %esi
	movq	%r14, %rdi
	andl	$16777215, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r14
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	0(%r13), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_8OperatorE@PLT
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internallsERSoNS0_21MachineRepresentationE@PLT
	leaq	.LC12(%rip), %rsi
	movq	%rax, %rdi
.L68:
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	leaq	-448(%rbp), %rdi
	leaq	-408(%rbp), %rsi
	call	_ZNKSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE3strEv@PLT
	movq	-448(%rbp), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L62:
	leaq	-416(%rbp), %r12
	movq	%r12, %rdi
	call	_ZNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
	movl	$17, %edx
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	20(%r13), %esi
	movq	%r12, %rdi
	andl	$16777215, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	0(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_8OperatorE@PLT
	leaq	.LC10(%rip), %rsi
	movq	%rax, %rdi
	jmp	.L68
.L71:
	call	__stack_chk_fail@PLT
.L69:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE16027:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker25CheckValueInputForInt64OpEPKNS1_4NodeEi.isra.0, .-_ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker25CheckValueInputForInt64OpEPKNS1_4NodeEi.isra.0
	.section	.rodata._ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker3RunEv.constprop.0.str1.1,"aMS",@progbits,1
.LC13:
	.string	"unreachable code"
.LC14:
	.string	" has wrong type for:"
.LC15:
	.string	" * input "
.LC16:
	.string	" ("
.LC17:
	.string	") has a "
.LC18:
	.string	" representation (expected: "
.LC19:
	.string	")."
	.section	.rodata._ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker3RunEv.constprop.0.str1.8,"aMS",@progbits,1
	.align 8
.LC20:
	.string	"MachineRepresentation::kBit == inferrer_->GetRepresentation(node->InputAt(0))"
	.section	.rodata._ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker3RunEv.constprop.0.str1.1
.LC21:
	.string	"Check failed: %s."
	.section	.rodata._ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker3RunEv.constprop.0.str1.8
	.align 8
.LC22:
	.string	"MachineRepresentation::kTagged == inferrer_->GetRepresentation(node->InputAt(0))"
	.align 8
.LC23:
	.string	"IsAnyCompressed( inferrer_->GetRepresentation(node->InputAt(0)))"
	.align 8
.LC24:
	.string	"CanBeCompressedPointer( inferrer_->GetRepresentation(node->InputAt(0)))"
	.align 8
.LC25:
	.string	"CanBeCompressedSigned( inferrer_->GetRepresentation(node->InputAt(0)))"
	.align 8
.LC26:
	.string	"IsAnyTagged(inferrer_->GetRepresentation(node->InputAt(0)))"
	.align 8
.LC27:
	.string	"CanBeTaggedPointer( inferrer_->GetRepresentation(node->InputAt(0)))"
	.align 8
.LC28:
	.string	"CanBeTaggedSigned( inferrer_->GetRepresentation(node->InputAt(0)))"
	.align 8
.LC29:
	.string	" which doesn't have a compressed representation."
	.align 8
.LC30:
	.string	" which doesn't have a compressed or int32-compatible representation."
	.align 8
.LC31:
	.string	" which doesn't have a kFloat32 representation."
	.section	.rodata._ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker3RunEv.constprop.0.str1.1
.LC32:
	.string	"Node #"
	.section	.rodata._ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker3RunEv.constprop.0.str1.8
	.align 8
.LC33:
	.string	" in the machine graph is not being checked."
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker3RunEv.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker3RunEv.constprop.0, @function
_ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker3RunEv.constprop.0:
.LFB16046:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$568, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	16(%rax), %rdx
	movq	24(%rax), %rax
	movq	%rax, -536(%rbp)
	cmpq	%rax, %rdx
	je	.L72
	movq	.LC34(%rip), %xmm1
	movq	%rdx, -512(%rbp)
	movq	%rdi, %r15
	movhps	.LC35(%rip), %xmm1
	movaps	%xmm1, -592(%rbp)
.L77:
	movq	-512(%rbp), %rax
	xorl	%r12d, %r12d
	movq	(%rax), %rbx
	movq	%rbx, 32(%r15)
	movq	72(%rbx), %rdx
	movq	80(%rbx), %rax
	subq	%rdx, %rax
	sarq	$3, %rax
	.p2align 4,,10
	.p2align 3
.L273:
	cmpq	%rax, %r12
	jnb	.L74
	movq	(%rdx,%r12,8), %r14
.L75:
	testq	%r14, %r14
	je	.L274
	movq	(%r14), %rdi
	cmpw	$636, 16(%rdi)
	ja	.L79
	movzwl	16(%rdi), %eax
	leaq	.L81(%rip), %rcx
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker3RunEv.constprop.0,"a",@progbits
	.align 4
	.align 4
.L81:
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L122-.L81
	.long	.L122-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L121-.L81
	.long	.L119-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L118-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L120-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L118-.L81
	.long	.L79-.L81
	.long	.L118-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L119-.L81
	.long	.L118-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L118-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L118-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L122-.L81
	.long	.L79-.L81
	.long	.L122-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L117-.L81
	.long	.L116-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L122-.L81
	.long	.L122-.L81
	.long	.L122-.L81
	.long	.L122-.L81
	.long	.L122-.L81
	.long	.L112-.L81
	.long	.L112-.L81
	.long	.L112-.L81
	.long	.L112-.L81
	.long	.L112-.L81
	.long	.L112-.L81
	.long	.L112-.L81
	.long	.L112-.L81
	.long	.L112-.L81
	.long	.L112-.L81
	.long	.L112-.L81
	.long	.L112-.L81
	.long	.L112-.L81
	.long	.L112-.L81
	.long	.L112-.L81
	.long	.L112-.L81
	.long	.L112-.L81
	.long	.L112-.L81
	.long	.L112-.L81
	.long	.L115-.L81
	.long	.L115-.L81
	.long	.L115-.L81
	.long	.L115-.L81
	.long	.L115-.L81
	.long	.L115-.L81
	.long	.L115-.L81
	.long	.L115-.L81
	.long	.L115-.L81
	.long	.L115-.L81
	.long	.L115-.L81
	.long	.L115-.L81
	.long	.L115-.L81
	.long	.L115-.L81
	.long	.L115-.L81
	.long	.L115-.L81
	.long	.L114-.L81
	.long	.L113-.L81
	.long	.L112-.L81
	.long	.L112-.L81
	.long	.L112-.L81
	.long	.L112-.L81
	.long	.L115-.L81
	.long	.L115-.L81
	.long	.L115-.L81
	.long	.L115-.L81
	.long	.L110-.L81
	.long	.L110-.L81
	.long	.L110-.L81
	.long	.L109-.L81
	.long	.L109-.L81
	.long	.L109-.L81
	.long	.L110-.L81
	.long	.L110-.L81
	.long	.L110-.L81
	.long	.L110-.L81
	.long	.L110-.L81
	.long	.L110-.L81
	.long	.L91-.L81
	.long	.L91-.L81
	.long	.L91-.L81
	.long	.L91-.L81
	.long	.L91-.L81
	.long	.L91-.L81
	.long	.L91-.L81
	.long	.L109-.L81
	.long	.L109-.L81
	.long	.L109-.L81
	.long	.L109-.L81
	.long	.L109-.L81
	.long	.L109-.L81
	.long	.L109-.L81
	.long	.L109-.L81
	.long	.L109-.L81
	.long	.L99-.L81
	.long	.L99-.L81
	.long	.L99-.L81
	.long	.L99-.L81
	.long	.L99-.L81
	.long	.L99-.L81
	.long	.L99-.L81
	.long	.L99-.L81
	.long	.L99-.L81
	.long	.L99-.L81
	.long	.L99-.L81
	.long	.L99-.L81
	.long	.L99-.L81
	.long	.L99-.L81
	.long	.L99-.L81
	.long	.L99-.L81
	.long	.L99-.L81
	.long	.L99-.L81
	.long	.L99-.L81
	.long	.L99-.L81
	.long	.L99-.L81
	.long	.L99-.L81
	.long	.L99-.L81
	.long	.L99-.L81
	.long	.L99-.L81
	.long	.L99-.L81
	.long	.L99-.L81
	.long	.L104-.L81
	.long	.L103-.L81
	.long	.L103-.L81
	.long	.L106-.L81
	.long	.L103-.L81
	.long	.L103-.L81
	.long	.L103-.L81
	.long	.L103-.L81
	.long	.L103-.L81
	.long	.L104-.L81
	.long	.L108-.L81
	.long	.L108-.L81
	.long	.L108-.L81
	.long	.L108-.L81
	.long	.L108-.L81
	.long	.L108-.L81
	.long	.L108-.L81
	.long	.L107-.L81
	.long	.L104-.L81
	.long	.L103-.L81
	.long	.L103-.L81
	.long	.L103-.L81
	.long	.L103-.L81
	.long	.L103-.L81
	.long	.L103-.L81
	.long	.L103-.L81
	.long	.L106-.L81
	.long	.L105-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L104-.L81
	.long	.L104-.L81
	.long	.L103-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L105-.L81
	.long	.L105-.L81
	.long	.L84-.L81
	.long	.L84-.L81
	.long	.L101-.L81
	.long	.L100-.L81
	.long	.L99-.L81
	.long	.L91-.L81
	.long	.L99-.L81
	.long	.L79-.L81
	.long	.L99-.L81
	.long	.L99-.L81
	.long	.L99-.L81
	.long	.L79-.L81
	.long	.L99-.L81
	.long	.L91-.L81
	.long	.L91-.L81
	.long	.L79-.L81
	.long	.L99-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L122-.L81
	.long	.L122-.L81
	.long	.L79-.L81
	.long	.L122-.L81
	.long	.L122-.L81
	.long	.L98-.L81
	.long	.L97-.L81
	.long	.L96-.L81
	.long	.L95-.L81
	.long	.L94-.L81
	.long	.L93-.L81
	.long	.L99-.L81
	.long	.L92-.L81
	.long	.L99-.L81
	.long	.L122-.L81
	.long	.L92-.L81
	.long	.L92-.L81
	.long	.L122-.L81
	.long	.L92-.L81
	.long	.L92-.L81
	.long	.L91-.L81
	.long	.L99-.L81
	.long	.L122-.L81
	.long	.L79-.L81
	.long	.L99-.L81
	.long	.L99-.L81
	.long	.L88-.L81
	.long	.L88-.L81
	.long	.L105-.L81
	.long	.L101-.L81
	.long	.L84-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L84-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L122-.L81
	.long	.L80-.L81
	.long	.L82-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L80-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L80-.L81
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker3RunEv.constprop.0
	.p2align 4,,10
	.p2align 3
.L79:
	movl	20(%rdi), %eax
	testl	%eax, %eax
	jne	.L322
	.p2align 4,,10
	.p2align 3
.L118:
	movq	72(%rbx), %rdx
	movq	80(%rbx), %rax
	addq	$1, %r12
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%r12, %rax
	jnb	.L273
.L274:
	addq	$8, -512(%rbp)
	movq	-512(%rbp), %rax
	cmpq	%rax, -536(%rbp)
	jne	.L77
.L72:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L323
	addq	$568, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L99:
	.cfi_restore_state
	movq	8(%r15), %rdi
	xorl	%edx, %edx
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker27CheckValueInputForFloat64OpEPKNS1_4NodeEi.isra.0
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L112:
	movq	8(%r15), %rdi
	xorl	%edx, %edx
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker25CheckValueInputForInt32OpEPKNS1_4NodeEi.isra.0
	movq	8(%r15), %rdi
	movl	$1, %edx
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker25CheckValueInputForInt32OpEPKNS1_4NodeEi.isra.0
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L115:
	movq	8(%r15), %rdi
	xorl	%edx, %edx
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker25CheckValueInputForInt64OpEPKNS1_4NodeEi.isra.0
	movq	8(%r15), %rdi
	movl	$1, %edx
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker25CheckValueInputForInt64OpEPKNS1_4NodeEi.isra.0
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L122:
	movq	8(%r15), %rdi
	xorl	%edx, %edx
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker25CheckValueInputForInt32OpEPKNS1_4NodeEi.isra.0
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L108:
	movq	8(%r15), %rdi
	movl	$4, %ecx
	movl	$3, %edx
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker31CheckValueInputRepresentationIsEPKNS1_4NodeEiNS0_21MachineRepresentationE.isra.0
.L103:
	movl	20(%r14), %eax
	movq	32(%r14), %r9
	leaq	32(%r14), %r10
	movq	8(%r15), %rdi
	movl	%eax, %esi
	movq	%r9, %r13
	shrl	$24, %esi
	andl	$15, %esi
	cmpl	$15, %esi
	jne	.L200
	movq	16(%r9), %r13
.L200:
	movl	20(%r13), %r11d
	movq	24(%rdi), %rcx
	movq	32(%rdi), %rdx
	andl	$16777215, %r11d
	subq	%rcx, %rdx
	cmpq	%rdx, %r11
	jnb	.L314
	movzbl	(%rcx,%r11), %r8d
	subl	$5, %r8d
	cmpb	$3, %r8b
	ja	.L318
	leaq	8(%r10), %r11
	cmpl	$15, %esi
	leaq	24(%r9), %r8
	cmovne	%r11, %r8
	movq	(%r8), %r13
	movl	20(%r13), %r11d
	andl	$16777215, %r11d
	cmpq	%r11, %rdx
	jbe	.L314
	movzbl	(%rcx,%r11), %r8d
	cmpb	$5, %r8b
	jne	.L321
	andl	$16777215, %eax
	cmpq	%rdx, %rax
	jnb	.L324
	movzbl	(%rcx,%rax), %r8d
	cmpb	$8, %r8b
	ja	.L208
	cmpb	$5, %r8b
	ja	.L325
.L210:
	movl	%r8d, %ecx
	movl	$2, %edx
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker31CheckValueInputRepresentationIsEPKNS1_4NodeEiNS0_21MachineRepresentationE.isra.0
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L109:
	movq	8(%r15), %rdi
	xorl	%edx, %edx
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker27CheckValueInputForFloat64OpEPKNS1_4NodeEi.isra.0
	movzbl	23(%r14), %eax
	movq	8(%r15), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L196
	leaq	40(%r14), %rax
.L197:
	movq	(%rax), %r13
	movq	24(%rdx), %rax
	movq	32(%rdx), %rdx
	movl	20(%r13), %esi
	subq	%rax, %rdx
	andl	$16777215, %esi
	cmpq	%rdx, %rsi
	jnb	.L309
	cmpb	$13, (%rax,%rsi)
	je	.L118
	leaq	-448(%rbp), %r12
	movq	%r12, %rdi
	call	_ZNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
	movl	$17, %edx
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	20(%r14), %esi
	movq	%r12, %rdi
	andl	$16777215, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r14), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_8OperatorE@PLT
	movl	$12, %edx
	leaq	.LC3(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	20(%r13), %esi
	movq	%r12, %rdi
	andl	$16777215, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	0(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_8OperatorE@PLT
	leaq	.LC4(%rip), %rsi
	movq	%rax, %rdi
.L310:
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	leaq	-480(%rbp), %rdi
	leaq	-440(%rbp), %rsi
.L307:
	call	_ZNKSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE3strEv@PLT
	movq	-480(%rbp), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L91:
	movzbl	23(%r14), %eax
	movq	8(%r15), %rdx
	movq	32(%r14), %r13
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L193
	movq	16(%r13), %r13
.L193:
	movq	24(%rdx), %rax
	movl	20(%r13), %esi
	movq	32(%rdx), %rdx
	andl	$16777215, %esi
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	jnb	.L309
	cmpb	$12, (%rax,%rsi)
	je	.L118
.L316:
	leaq	-448(%rbp), %r12
	movq	%r12, %rdi
	call	_ZNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
	movl	$17, %edx
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	20(%r14), %esi
	movq	%r12, %rdi
	andl	$16777215, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r14), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_8OperatorE@PLT
	movl	$12, %edx
	leaq	.LC3(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	20(%r13), %esi
	movq	%r12, %rdi
	andl	$16777215, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	0(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_8OperatorE@PLT
	leaq	.LC31(%rip), %rsi
	movq	%rax, %rdi
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L110:
	movl	20(%r14), %eax
	movq	32(%r14), %rsi
	leaq	32(%r14), %rcx
	movq	8(%r15), %rdx
	movl	%eax, %edi
	movq	%rsi, %r13
	xorl	$251658240, %edi
	andl	$251658240, %edi
	jne	.L185
	movq	16(%rsi), %r13
.L185:
	movq	24(%rdx), %rdi
	movl	20(%r13), %r8d
	movq	32(%rdx), %rdx
	andl	$16777215, %r8d
	subq	%rdi, %rdx
	cmpq	%rdx, %r8
	jnb	.L326
	cmpb	$12, (%rdi,%r8)
	jne	.L316
	addq	$24, %rsi
	xorl	$251658240, %eax
	addq	$8, %rcx
	testl	$251658240, %eax
	movq	%rsi, %rax
	cmovne	%rcx, %rax
	movq	(%rax), %r13
	movl	20(%r13), %esi
	andl	$16777215, %esi
	cmpq	%rdx, %rsi
	jnb	.L309
	cmpb	$12, (%rdi,%rsi)
	je	.L118
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L92:
	movq	8(%r15), %rdi
	xorl	%edx, %edx
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker25CheckValueInputForInt64OpEPKNS1_4NodeEi.isra.0
	jmp	.L118
.L104:
	movq	8(%r15), %rdi
	xorl	%edx, %edx
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker32CheckValueInputIsTaggedOrPointerEPKNS1_4NodeEi.isra.0
	movq	8(%r15), %rdi
	movl	$5, %ecx
	movq	%r14, %rsi
	movl	$1, %edx
	call	_ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker31CheckValueInputRepresentationIsEPKNS1_4NodeEiNS0_21MachineRepresentationE.isra.0
	jmp	.L118
.L84:
	movq	8(%r15), %rdi
	movl	$5, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker31CheckValueInputRepresentationIsEPKNS1_4NodeEiNS0_21MachineRepresentationE.isra.0
	jmp	.L118
.L105:
	movq	8(%r15), %rdi
	xorl	%edx, %edx
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker23CheckValueInputIsTaggedEPKNS1_4NodeEi.isra.0
	jmp	.L118
.L80:
	movq	8(%r15), %rdi
	movl	$14, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker31CheckValueInputRepresentationIsEPKNS1_4NodeEiNS0_21MachineRepresentationE.isra.0
	jmp	.L118
.L101:
	movq	8(%r15), %rdi
	movl	$4, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker31CheckValueInputRepresentationIsEPKNS1_4NodeEiNS0_21MachineRepresentationE.isra.0
	jmp	.L118
.L107:
	movq	8(%r15), %rdi
	movl	$4, %ecx
	movl	$4, %edx
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker31CheckValueInputRepresentationIsEPKNS1_4NodeEiNS0_21MachineRepresentationE.isra.0
	movq	8(%r15), %rdi
	movl	$4, %ecx
	movq	%r14, %rsi
	movl	$5, %edx
	call	_ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker31CheckValueInputRepresentationIsEPKNS1_4NodeEiNS0_21MachineRepresentationE.isra.0
.L106:
	movl	20(%r14), %ecx
	movq	32(%r14), %rax
	leaq	32(%r14), %r10
	movq	8(%r15), %rdi
	movl	%ecx, %r8d
	movq	%rax, %r13
	shrl	$24, %r8d
	andl	$15, %r8d
	cmpl	$15, %r8d
	jne	.L216
	movq	16(%rax), %r13
.L216:
	movl	20(%r13), %r9d
	movq	24(%rdi), %rsi
	movq	32(%rdi), %rdx
	andl	$16777215, %r9d
	subq	%rsi, %rdx
	cmpq	%rdx, %r9
	jnb	.L313
	movzbl	(%rsi,%r9), %r9d
	subl	$5, %r9d
	cmpb	$3, %r9b
	ja	.L318
	leaq	8(%r10), %r9
	addq	$24, %rax
	cmpl	$15, %r8d
	cmovne	%r9, %rax
	movq	(%rax), %r13
	movl	20(%r13), %r9d
	andl	$16777215, %r9d
	cmpq	%r9, %rdx
	jbe	.L313
	movzbl	(%rsi,%r9), %r8d
	cmpb	$5, %r8b
	jne	.L321
	andl	$16777215, %ecx
	cmpq	%rdx, %rcx
	jnb	.L327
	movzbl	(%rsi,%rcx), %ecx
	movl	$2, %edx
	movq	%r14, %rsi
	leal	-6(%rcx), %eax
	cmpb	$2, %al
	ja	.L224
	call	_ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker23CheckValueInputIsTaggedEPKNS1_4NodeEi.isra.0
	movq	8(%r15), %rdi
	movl	$3, %edx
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker23CheckValueInputIsTaggedEPKNS1_4NodeEi.isra.0
	jmp	.L118
.L88:
	movq	8(%r15), %rdi
	xorl	%edx, %edx
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker27CheckValueInputForFloat64OpEPKNS1_4NodeEi.isra.0
	movq	8(%r15), %rdi
	movl	$1, %edx
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker25CheckValueInputForInt32OpEPKNS1_4NodeEi.isra.0
	jmp	.L118
.L119:
	call	_ZN2v88internal8compiler16CallDescriptorOfEPKNS1_8OperatorE@PLT
	leaq	-336(%rbp), %r13
	leaq	-448(%rbp), %rdx
	movq	%r13, %rdi
	movq	%rax, -488(%rbp)
	movq	%rdx, -520(%rbp)
	movq	%r13, -560(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movq	%rcx, -336(%rbp)
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movw	%dx, -112(%rbp)
	movq	-520(%rbp), %rdi
	movq	%rcx, -448(%rbp)
	movups	%xmm0, -104(%rbp)
	movups	%xmm0, -88(%rbp)
	movq	$0, -120(%rbp)
	movq	-24(%rcx), %rax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rcx, -448(%rbp,%rax)
	movq	-448(%rbp), %rax
	addq	-24(%rax), %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	pxor	%xmm0, %xmm0
	movdqa	-592(%rbp), %xmm2
	movq	%rdx, -336(%rbp)
	leaq	-384(%rbp), %rdx
	movq	%rdx, %rdi
	movq	%rdx, -544(%rbp)
	movaps	%xmm2, -448(%rbp)
	movaps	%xmm0, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%r13, %rdi
	leaq	-352(%rbp), %rcx
	movq	%rdx, -440(%rbp)
	leaq	-440(%rbp), %rdx
	movq	%rdx, %rsi
	movq	%rdx, -608(%rbp)
	movl	$16, -376(%rbp)
	movq	%rcx, -552(%rbp)
	movq	%rcx, -368(%rbp)
	movq	$0, -360(%rbp)
	movb	$0, -352(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-488(%rbp), %rax
	movq	24(%rax), %rdx
	cmpq	$-1, 8(%rdx)
	je	.L123
	leaq	32(%r14), %rax
	movq	%r14, -504(%rbp)
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movq	%rax, -496(%rbp)
	movq	8(%r15), %rax
	movq	%r12, -600(%rbp)
	movl	20(%r14), %r11d
	movq	24(%rax), %rdi
	movq	%rbx, -576(%rbp)
	movq	32(%rax), %r10
	movq	-520(%rbp), %rbx
	movq	%r15, -528(%rbp)
	movq	%rcx, %r15
	.p2align 4,,10
	.p2align 3
.L148:
	movl	%r11d, %r8d
	movslq	%r15d, %rax
	xorl	$251658240, %r8d
	salq	$3, %rax
	andl	$251658240, %r8d
	je	.L124
	addq	-496(%rbp), %rax
.L125:
	movq	(%rax), %r13
	movq	%r10, %r8
	subq	%rdi, %r8
	movl	20(%r13), %eax
	andl	$16777215, %eax
	cmpq	%r8, %rax
	jnb	.L328
	movzbl	(%rdi,%rax), %r12d
	movq	-488(%rbp), %rax
	movzbl	12(%rax), %r14d
	testq	%r15, %r15
	je	.L127
	movq	(%rdx), %r8
	movq	16(%rdx), %rax
	addq	%r15, %r8
	movzbl	-4(%rax,%r8,8), %r14d
.L127:
	cmpb	$14, %r14b
	ja	.L128
	leaq	.L130(%rip), %rcx
	movzbl	%r14b, %eax
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker3RunEv.constprop.0
	.align 4
	.align 4
.L130:
	.long	.L134-.L130
	.long	.L129-.L130
	.long	.L129-.L130
	.long	.L129-.L130
	.long	.L133-.L130
	.long	.L129-.L130
	.long	.L129-.L130
	.long	.L129-.L130
	.long	.L132-.L130
	.long	.L129-.L130
	.long	.L129-.L130
	.long	.L131-.L130
	.long	.L129-.L130
	.long	.L129-.L130
	.long	.L129-.L130
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker3RunEv.constprop.0
	.p2align 4,,10
	.p2align 3
.L129:
	cmpb	%r12b, %r14b
	sete	%al
.L136:
	testb	%al, %al
	je	.L128
.L135:
	movq	8(%rdx), %rax
	leaq	1(%r15), %rcx
	addq	$1, %rax
	cmpq	%rcx, %rax
	jbe	.L147
.L146:
	movq	%rcx, %r15
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L131:
	leal	-9(%r12), %eax
	cmpb	$2, %al
	jbe	.L135
.L128:
	testb	%sil, %sil
	jne	.L137
	movl	$17, %edx
	leaq	.LC1(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-504(%rbp), %rax
	movq	%rbx, %rdi
	movl	20(%rax), %esi
	andl	$16777215, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-504(%rbp), %rax
	movq	-520(%rbp), %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal8compilerlsERSoRKNS1_8OperatorE@PLT
	movl	$20, %edx
	leaq	.LC14(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-520(%rbp), %r10
	movq	(%r10), %rax
	movq	-24(%rax), %rax
	movq	240(%r10,%rax), %rdi
	testq	%rdi, %rdi
	je	.L142
	cmpb	$0, 56(%rdi)
	je	.L139
	movsbl	67(%rdi), %esi
.L140:
	movq	%r10, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
.L141:
	movl	$9, %edx
	leaq	.LC15(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$2, %edx
	leaq	.LC16(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	20(%r13), %esi
	movq	-520(%rbp), %rdi
	andl	$16777215, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	0(%r13), %rsi
	movq	-520(%rbp), %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_8OperatorE@PLT
	movl	$8, %edx
	leaq	.LC17(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-520(%rbp), %rdi
	movl	%r12d, %esi
	call	_ZN2v88internallsERSoNS0_21MachineRepresentationE@PLT
	movl	$27, %edx
	leaq	.LC18(%rip), %rsi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internallsERSoNS0_21MachineRepresentationE@PLT
	movl	$2, %edx
	leaq	.LC19(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-488(%rbp), %rax
	leaq	1(%r15), %rcx
	movq	24(%rax), %rdx
	movq	8(%rdx), %rax
	addq	$1, %rax
	cmpq	%rcx, %rax
	jbe	.L149
	movq	-504(%rbp), %rax
	movl	$1, %esi
	movl	20(%rax), %r11d
	movq	-528(%rbp), %rax
	movq	8(%rax), %rax
	movq	24(%rax), %rdi
	movq	32(%rax), %r10
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L132:
	leal	-6(%r12), %eax
	cmpb	$2, %al
	jbe	.L135
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L133:
	leal	-1(%r12), %eax
	cmpb	$3, %al
	setbe	%al
	jmp	.L136
.L95:
	movzbl	23(%r14), %eax
	movq	8(%r15), %rdx
	movq	32(%r14), %rcx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L157
	movq	16(%rcx), %rcx
.L157:
	movq	24(%rdx), %rax
	movl	20(%rcx), %esi
	movq	32(%rdx), %rdx
	andl	$16777215, %esi
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	jnb	.L309
	movzbl	(%rax,%rsi), %eax
	subl	$9, %eax
	cmpb	$2, %al
	jbe	.L118
	leaq	.LC23(%rip), %rsi
	leaq	.LC21(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L96:
	movzbl	23(%r14), %eax
	movq	8(%r15), %rdx
	movq	32(%r14), %rcx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L167
	movq	16(%rcx), %rcx
.L167:
	movq	24(%rdx), %rax
	movl	20(%rcx), %esi
	movq	32(%rdx), %rdx
	andl	$16777215, %esi
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	jnb	.L309
	movzbl	(%rax,%rsi), %eax
	subl	$6, %eax
	testb	$-3, %al
	je	.L118
	leaq	.LC28(%rip), %rsi
	leaq	.LC21(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L97:
	movzbl	23(%r14), %eax
	movq	8(%r15), %rdx
	movq	32(%r14), %rcx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L165
	movq	16(%rcx), %rcx
.L165:
	movq	24(%rdx), %rax
	movl	20(%rcx), %esi
	movq	32(%rdx), %rdx
	andl	$16777215, %esi
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	jnb	.L309
	movzbl	(%rax,%rsi), %eax
	subl	$7, %eax
	cmpb	$1, %al
	jbe	.L118
	leaq	.LC27(%rip), %rsi
	leaq	.LC21(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L98:
	movzbl	23(%r14), %eax
	movq	8(%r15), %rdx
	movq	32(%r14), %rcx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L163
	movq	16(%rcx), %rcx
.L163:
	movq	24(%rdx), %rax
	movl	20(%rcx), %esi
	movq	32(%rdx), %rdx
	andl	$16777215, %esi
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	jnb	.L309
	movzbl	(%rax,%rsi), %eax
	subl	$6, %eax
	cmpb	$2, %al
	jbe	.L118
	leaq	.LC26(%rip), %rsi
	leaq	.LC21(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L121:
	movq	8(%r15), %rsi
	movq	8(%rsi), %r11
	movq	(%r11), %rax
	movq	24(%rax), %rax
	movq	(%rax), %r9
	testq	%r9, %r9
	je	.L118
	movq	24(%rsi), %r10
	movq	32(%rsi), %r8
	leaq	32(%r14), %rcx
	xorl	%edx, %edx
	movzbl	23(%r14), %edi
	subq	%r10, %r8
	andl	$15, %edi
	jmp	.L272
	.p2align 4,,10
	.p2align 3
.L329:
	addq	%rcx, %rax
.L259:
	movq	(%rax), %r13
	movl	20(%r13), %esi
	andl	$16777215, %esi
	cmpq	%r8, %rsi
	jnb	.L311
	movzbl	(%r10,%rsi), %eax
	subl	$6, %eax
	cmpb	$2, %al
	ja	.L317
.L262:
	addq	$1, %rdx
	cmpq	%rdx, %r9
	je	.L118
	movq	(%r11), %rax
	movq	24(%rax), %rax
.L272:
	movq	16(%rax), %rax
	movzbl	4(%rax,%rdx,8), %r13d
	leal	1(%rdx), %eax
	cltq
	salq	$3, %rax
	cmpb	$4, %r13b
	je	.L256
	leal	-6(%r13), %esi
	cmpb	$2, %sil
	ja	.L257
	cmpl	$15, %edi
	jne	.L329
	movq	(%rcx), %rsi
	leaq	16(%rsi,%rax), %rax
	jmp	.L259
.L113:
	movq	8(%r15), %rdi
	xorl	%edx, %edx
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker32CheckValueInputIsTaggedOrPointerEPKNS1_4NodeEi.isra.0
	movq	8(%r15), %rdi
	movl	$1, %edx
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker32CheckValueInputIsTaggedOrPointerEPKNS1_4NodeEi.isra.0
	cmpb	$0, 16(%r15)
	jne	.L118
	movzbl	23(%r14), %eax
	movq	8(%r15), %rdi
	movq	32(%r14), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L330
.L172:
	movl	20(%rdx), %esi
	movq	24(%rdi), %rax
	movq	32(%rdi), %rdx
	andl	$16777215, %esi
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	jnb	.L309
	movzbl	(%rax,%rsi), %ecx
	movl	$1, %edx
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker31CheckValueInputRepresentationIsEPKNS1_4NodeEiNS0_21MachineRepresentationE.isra.0
	jmp	.L118
.L114:
	movl	20(%r14), %eax
	movq	32(%r14), %rdx
	leaq	32(%r14), %r9
	movq	8(%r15), %rcx
	movl	%eax, %esi
	movq	%rdx, %r13
	xorl	$251658240, %esi
	andl	$251658240, %esi
	jne	.L174
	movq	16(%rdx), %r13
.L174:
	movq	32(%rcx), %rdi
	movl	20(%r13), %esi
	movq	24(%rcx), %r8
	movq	%rdi, %r10
	andl	$16777215, %esi
	subq	%r8, %r10
	cmpq	%r10, %rsi
	jnb	.L331
	movzbl	(%r8,%rsi), %ecx
	cmpb	$4, %cl
	ja	.L176
	testb	%cl, %cl
	jne	.L177
.L184:
	leaq	-448(%rbp), %r12
	movq	%r12, %rdi
	call	_ZNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
	movl	$17, %edx
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	20(%r14), %esi
	movq	%r12, %rdi
	andl	$16777215, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r14), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_8OperatorE@PLT
	movl	$12, %edx
	leaq	.LC3(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	20(%r13), %esi
	movq	%r12, %rdi
	andl	$16777215, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	0(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_8OperatorE@PLT
	leaq	.LC30(%rip), %rsi
	movq	%rax, %rdi
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L93:
	movzbl	23(%r14), %eax
	movq	8(%r15), %rdx
	movq	32(%r14), %rcx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L161
	movq	16(%rcx), %rcx
.L161:
	movq	24(%rdx), %rax
	movl	20(%rcx), %esi
	movq	32(%rdx), %rdx
	andl	$16777215, %esi
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	jnb	.L309
	movzbl	(%rax,%rsi), %eax
	andl	$-3, %eax
	cmpb	$9, %al
	je	.L118
	leaq	.LC25(%rip), %rsi
	leaq	.LC21(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L116:
	movzbl	23(%r14), %eax
	movq	8(%r15), %rdx
	movq	32(%r14), %rcx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L151
	movq	16(%rcx), %rcx
.L151:
	movq	24(%rdx), %rax
	movl	20(%rcx), %esi
	movq	32(%rdx), %rdx
	andl	$16777215, %esi
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	jnb	.L309
	cmpb	$1, (%rax,%rsi)
	je	.L118
	leaq	.LC20(%rip), %rsi
	leaq	.LC21(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L117:
	movzbl	23(%r14), %eax
	movq	8(%r15), %rdx
	movq	32(%r14), %rcx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L154
	movq	16(%rcx), %rcx
.L154:
	movq	24(%rdx), %rax
	movl	20(%rcx), %esi
	movq	32(%rdx), %rdx
	andl	$16777215, %esi
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	jnb	.L309
	cmpb	$8, (%rax,%rsi)
	je	.L118
	leaq	.LC22(%rip), %rsi
	leaq	.LC21(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L82:
	movq	8(%r15), %rdi
	xorl	%edx, %edx
	movq	%r14, %rsi
	movl	$14, %ecx
	call	_ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker31CheckValueInputRepresentationIsEPKNS1_4NodeEiNS0_21MachineRepresentationE.isra.0
	movq	8(%r15), %rdi
	movl	$1, %edx
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker25CheckValueInputForInt32OpEPKNS1_4NodeEi.isra.0
	jmp	.L118
.L120:
	movq	8(%r15), %rdx
	movl	20(%r14), %eax
	movq	24(%rdx), %rcx
	movq	32(%rdx), %rdx
	movl	%eax, %esi
	andl	$16777215, %esi
	subq	%rcx, %rdx
	cmpq	%rdx, %rsi
	jnb	.L309
	movzbl	(%rcx,%rsi), %r13d
	movl	20(%rdi), %esi
	cmpb	$8, %r13b
	ja	.L227
	cmpb	$5, %r13b
	ja	.L228
	cmpb	$4, %r13b
	jne	.L230
	testl	%esi, %esi
	jle	.L118
	shrl	$24, %eax
	leaq	32(%r14), %r9
	xorl	%r8d, %r8d
	andl	$15, %eax
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L332:
	addq	%r9, %rsi
.L243:
	movq	(%rsi), %r13
	movl	20(%r13), %esi
	andl	$16777215, %esi
	cmpq	%rdx, %rsi
	jnb	.L309
	movzbl	(%rcx,%rsi), %esi
	testb	%sil, %sil
	je	.L266
	subl	$1, %esi
	cmpb	$3, %sil
	ja	.L315
	addq	$1, %r8
	cmpl	%r8d, 20(%rdi)
	jle	.L118
.L248:
	leaq	0(,%r8,8), %rsi
	cmpl	$15, %eax
	jne	.L332
	movq	(%r9), %r10
	leaq	16(%r10,%rsi), %rsi
	jmp	.L243
.L100:
	movzbl	23(%r14), %eax
	movq	8(%r15), %rdx
	movq	32(%r14), %r13
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L169
	movq	16(%r13), %r13
.L169:
	movq	24(%rdx), %rax
	movl	20(%r13), %esi
	movq	32(%rdx), %rdx
	andl	$16777215, %esi
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	jnb	.L309
	movzbl	(%rax,%rsi), %eax
	subl	$9, %eax
	cmpb	$2, %al
	jbe	.L118
.L312:
	leaq	-448(%rbp), %r12
	movq	%r12, %rdi
	call	_ZNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
	movl	$17, %edx
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	20(%r14), %esi
	movq	%r12, %rdi
	andl	$16777215, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r14), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_8OperatorE@PLT
	movl	$12, %edx
	leaq	.LC3(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	20(%r13), %esi
	movq	%r12, %rdi
	andl	$16777215, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	0(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_8OperatorE@PLT
	leaq	.LC29(%rip), %rsi
	movq	%rax, %rdi
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L94:
	movzbl	23(%r14), %eax
	movq	8(%r15), %rdx
	movq	32(%r14), %rcx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L159
	movq	16(%rcx), %rcx
.L159:
	movq	24(%rdx), %rax
	movl	20(%rcx), %esi
	movq	32(%rdx), %rdx
	andl	$16777215, %esi
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	jnb	.L309
	movzbl	(%rax,%rsi), %eax
	subl	$10, %eax
	cmpb	$1, %al
	jbe	.L118
	leaq	.LC24(%rip), %rsi
	leaq	.LC21(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L74:
	movq	56(%rbx), %r14
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L124:
	movq	-496(%rbp), %rcx
	movq	(%rcx), %r8
	leaq	16(%r8,%rax), %rax
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L257:
	cmpl	$15, %edi
	je	.L269
	addq	%rcx, %rax
.L270:
	movq	(%rax), %rax
	movl	20(%rax), %esi
	andl	$16777215, %esi
	cmpq	%r8, %rsi
	jnb	.L311
	movzbl	(%r10,%rsi), %esi
	cmpb	%sil, %r13b
	je	.L262
	leaq	-432(%rbp), %r12
	leaq	-448(%rbp), %rdi
	movq	%rax, %rbx
	movb	%sil, -488(%rbp)
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
	movl	$17, %edx
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	20(%r14), %esi
	movq	%r12, %rdi
	andl	$16777215, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r14), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_8OperatorE@PLT
	movl	$12, %edx
	leaq	.LC3(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	20(%rbx), %esi
	movq	%r12, %rdi
	andl	$16777215, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_8OperatorE@PLT
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movzbl	-488(%rbp), %r8d
	jmp	.L319
	.p2align 4,,10
	.p2align 3
.L256:
	cmpl	$15, %edi
	je	.L263
	addq	%rcx, %rax
.L264:
	movq	(%rax), %r13
	movl	20(%r13), %esi
	andl	$16777215, %esi
	cmpq	%r8, %rsi
	jnb	.L311
	movzbl	(%r10,%rsi), %eax
	testb	%al, %al
	je	.L266
	subl	$1, %eax
	cmpb	$3, %al
	jbe	.L262
.L315:
	leaq	-448(%rbp), %r12
	movq	%r12, %rdi
	call	_ZNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
	movl	$17, %edx
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	20(%r14), %esi
	movq	%r12, %rdi
	andl	$16777215, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r14), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_8OperatorE@PLT
	movl	$12, %edx
	leaq	.LC3(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	20(%r13), %esi
	movq	%r12, %rdi
	andl	$16777215, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	0(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_8OperatorE@PLT
	leaq	.LC11(%rip), %rsi
	movq	%rax, %rdi
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L137:
	movq	-448(%rbp), %rax
	movq	-24(%rax), %rax
	movq	-208(%rbp,%rax), %rdi
	testq	%rdi, %rdi
	je	.L142
	cmpb	$0, 56(%rdi)
	je	.L143
	movsbl	67(%rdi), %esi
.L144:
	movq	%rbx, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	jmp	.L141
	.p2align 4,,10
	.p2align 3
.L208:
	leal	-9(%r8), %eax
	cmpb	$2, %al
	ja	.L210
	addq	$16, %r10
	leaq	32(%r9), %rax
	cmpl	$15, %esi
	cmovne	%r10, %rax
	movq	(%rax), %r13
	movl	20(%r13), %esi
	andl	$16777215, %esi
	cmpq	%rsi, %rdx
	jbe	.L309
	movzbl	(%rcx,%rsi), %eax
	subl	$9, %eax
	cmpb	$2, %al
	jbe	.L118
	jmp	.L312
	.p2align 4,,10
	.p2align 3
.L176:
	subl	$9, %ecx
	cmpb	$2, %cl
	ja	.L184
.L177:
	xorl	$251658240, %eax
	leaq	8(%r9), %rcx
	addq	$24, %rdx
	testl	$251658240, %eax
	movq	%rcx, %rax
	cmove	%rdx, %rax
	subq	%r8, %rdi
	movq	(%rax), %r13
	movl	20(%r13), %esi
	andl	$16777215, %esi
	cmpq	%rdi, %rsi
	jnb	.L333
	movzbl	(%r8,%rsi), %eax
	cmpb	$4, %al
	ja	.L182
	testb	%al, %al
	je	.L184
	jmp	.L118
.L227:
	leal	-9(%r13), %r8d
	cmpb	$2, %r8b
	ja	.L230
	testl	%esi, %esi
	jle	.L118
	shrl	$24, %eax
	leaq	32(%r14), %r9
	xorl	%r8d, %r8d
	andl	$15, %eax
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L334:
	addq	%r9, %rsi
.L238:
	movq	(%rsi), %r13
	movl	20(%r13), %esi
	andl	$16777215, %esi
	cmpq	%rdx, %rsi
	jnb	.L309
	movzbl	(%rcx,%rsi), %esi
	subl	$9, %esi
	cmpb	$2, %sil
	ja	.L312
	addq	$1, %r8
	cmpl	%r8d, 20(%rdi)
	jle	.L118
.L241:
	leaq	0(,%r8,8), %rsi
	cmpl	$15, %eax
	jne	.L334
	movq	(%r9), %r10
	leaq	16(%r10,%rsi), %rsi
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L182:
	subl	$9, %eax
	cmpb	$2, %al
	jbe	.L118
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L224:
	call	_ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker31CheckValueInputRepresentationIsEPKNS1_4NodeEiNS0_21MachineRepresentationE.isra.0
	movq	8(%r15), %rdi
	movl	20(%r14), %esi
	movq	24(%rdi), %rax
	movq	32(%rdi), %rdx
	andl	$16777215, %esi
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	jnb	.L309
	movzbl	(%rax,%rsi), %ecx
	movl	$3, %edx
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker31CheckValueInputRepresentationIsEPKNS1_4NodeEiNS0_21MachineRepresentationE.isra.0
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L147:
	movq	-576(%rbp), %rbx
	movq	-600(%rbp), %r12
	movq	-528(%rbp), %r15
	testb	%sil, %sil
	jne	.L149
.L123:
	movq	.LC34(%rip), %xmm0
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-368(%rbp), %rdi
	movq	%rax, -336(%rbp)
	movhps	.LC36(%rip), %xmm0
	movaps	%xmm0, -448(%rbp)
	cmpq	-552(%rbp), %rdi
	je	.L150
	call	_ZdlPv@PLT
.L150:
	movq	-544(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -440(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	-560(%rbp), %rdi
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -336(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L139:
	movq	%r10, -568(%rbp)
	movq	%rdi, -520(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-520(%rbp), %rdi
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	-568(%rbp), %r10
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L140
	movq	%r10, -520(%rbp)
	call	*%rax
	movq	-520(%rbp), %r10
	movsbl	%al, %esi
	jmp	.L140
	.p2align 4,,10
	.p2align 3
.L143:
	movq	%rdi, -520(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-520(%rbp), %rdi
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rcx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rcx, %rax
	je	.L144
	call	*%rax
	movsbl	%al, %esi
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L263:
	movq	(%rcx), %rsi
	leaq	16(%rsi,%rax), %rax
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L269:
	movq	(%rcx), %rsi
	leaq	16(%rsi,%rax), %rax
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L325:
	movl	$2, %edx
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker23CheckValueInputIsTaggedEPKNS1_4NodeEi.isra.0
	jmp	.L118
.L330:
	movq	16(%rdx), %rdx
	jmp	.L172
.L196:
	movq	32(%r14), %rax
	addq	$24, %rax
	jmp	.L197
.L228:
	testl	%esi, %esi
	jle	.L118
	shrl	$24, %eax
	leaq	32(%r14), %r9
	xorl	%r8d, %r8d
	andl	$15, %eax
	jmp	.L236
	.p2align 4,,10
	.p2align 3
.L335:
	addq	%r9, %rsi
.L233:
	movq	(%rsi), %r13
	movl	20(%r13), %esi
	andl	$16777215, %esi
	cmpq	%rdx, %rsi
	jnb	.L309
	movzbl	(%rcx,%rsi), %esi
	subl	$6, %esi
	cmpb	$2, %sil
	ja	.L317
	addq	$1, %r8
	cmpl	%r8d, 20(%rdi)
	jle	.L118
.L236:
	leaq	0(,%r8,8), %rsi
	cmpl	$15, %eax
	jne	.L335
	movq	(%r9), %r10
	leaq	16(%r10,%rsi), %rsi
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L230:
	testl	%esi, %esi
	jle	.L118
	xorl	$251658240, %eax
	leaq	32(%r14), %rdi
	testl	$251658240, %eax
	je	.L336
	leal	-1(%rsi), %eax
	leaq	40(%r14,%rax,8), %rax
	jmp	.L255
	.p2align 4,,10
	.p2align 3
.L253:
	movzbl	(%rcx,%rsi), %r8d
	cmpb	%r8b, %r13b
	jne	.L304
	addq	$8, %rdi
	cmpq	%rdi, %rax
	je	.L118
.L255:
	movq	(%rdi), %r9
	movl	20(%r9), %esi
	andl	$16777215, %esi
	cmpq	%rsi, %rdx
	ja	.L253
.L309:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L336:
	movq	32(%r14), %rdi
	subl	$1, %esi
	leaq	16(%rdi), %rax
	leaq	24(%rdi,%rsi,8), %rdi
	jmp	.L252
	.p2align 4,,10
	.p2align 3
.L337:
	movzbl	(%rcx,%rsi), %r8d
	cmpb	%r8b, %r13b
	jne	.L304
	addq	$8, %rax
	cmpq	%rax, %rdi
	je	.L118
.L252:
	movq	(%rax), %r9
	movl	20(%r9), %esi
	andl	$16777215, %esi
	cmpq	%rsi, %rdx
	ja	.L337
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L134:
	leaq	.LC13(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L328:
	movq	%rax, %rsi
	movq	%r8, %rdx
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L311:
	movq	%r8, %rdx
	jmp	.L309
.L331:
	movq	%r10, %rdx
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L323:
	call	__stack_chk_fail@PLT
.L304:
	leaq	-432(%rbp), %r12
	leaq	-448(%rbp), %rdi
	movb	%r8b, -496(%rbp)
	movq	%r9, -488(%rbp)
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
	movl	$17, %edx
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	20(%r14), %esi
	movq	%r12, %rdi
	andl	$16777215, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r14), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_8OperatorE@PLT
	movl	$12, %edx
	leaq	.LC3(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-488(%rbp), %r9
	movq	%r12, %rdi
	movl	20(%r9), %esi
	andl	$16777215, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-488(%rbp), %r9
	movq	%r12, %rdi
	movq	(%r9), %rsi
	call	_ZN2v88internal8compilerlsERSoRKNS1_8OperatorE@PLT
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movzbl	-496(%rbp), %r8d
.L319:
	movl	%r8d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internallsERSoNS0_21MachineRepresentationE@PLT
	leaq	.LC8(%rip), %rsi
	movl	$22, %edx
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	%r13d, %esi
.L320:
	movq	%r12, %rdi
	call	_ZN2v88internallsERSoNS0_21MachineRepresentationE@PLT
	leaq	.LC9(%rip), %rsi
	movq	%rax, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	leaq	-480(%rbp), %rdi
	leaq	-424(%rbp), %rsi
	jmp	.L307
.L142:
	call	_ZSt16__throw_bad_castv@PLT
.L327:
	movq	%rcx, %rsi
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L149:
	movq	-608(%rbp), %rsi
	leaq	-480(%rbp), %rdi
	jmp	.L307
.L333:
	movq	%rdi, %rdx
	xorl	%eax, %eax
	leaq	.LC0(%rip), %rdi
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L317:
	leaq	-448(%rbp), %r12
	movq	%r12, %rdi
	call	_ZNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
	movl	$17, %edx
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	20(%r14), %esi
	movq	%r12, %rdi
	andl	$16777215, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r14), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_8OperatorE@PLT
	movl	$12, %edx
	leaq	.LC3(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	20(%r13), %esi
	movq	%r12, %rdi
	andl	$16777215, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	0(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_8OperatorE@PLT
	leaq	.LC6(%rip), %rsi
	movq	%rax, %rdi
	jmp	.L310
.L313:
	movq	%r9, %rsi
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L326:
	movq	%r8, %rsi
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L266:
	leaq	-448(%rbp), %r12
	movq	%r12, %rdi
	call	_ZNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
	movl	$17, %edx
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	20(%r13), %esi
	movq	%r12, %rdi
	andl	$16777215, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	0(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_8OperatorE@PLT
	leaq	.LC10(%rip), %rsi
	movq	%rax, %rdi
	jmp	.L310
.L324:
	movq	%rax, %rsi
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L321:
	leaq	-432(%rbp), %r12
	leaq	-448(%rbp), %rdi
	movb	%r8b, -488(%rbp)
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
	movl	$17, %edx
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	20(%r14), %esi
	movq	%r12, %rdi
	andl	$16777215, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r14), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_8OperatorE@PLT
	movl	$12, %edx
	leaq	.LC3(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	20(%r13), %esi
	movq	%r12, %rdi
	andl	$16777215, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	0(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_8OperatorE@PLT
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movzbl	-488(%rbp), %r8d
	movq	%r12, %rdi
	movl	%r8d, %esi
	call	_ZN2v88internallsERSoNS0_21MachineRepresentationE@PLT
	leaq	.LC8(%rip), %rsi
	movl	$22, %edx
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$5, %esi
	jmp	.L320
.L322:
	leaq	-432(%rbp), %r12
	leaq	-448(%rbp), %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
	movl	$6, %edx
	movq	%r12, %rdi
	leaq	.LC32(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	20(%r14), %esi
	movq	%r12, %rdi
	andl	$16777215, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r14), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_8OperatorE@PLT
	leaq	.LC33(%rip), %rsi
	movq	%rax, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	leaq	-480(%rbp), %rdi
	leaq	-424(%rbp), %rsi
	jmp	.L307
.L318:
	leaq	-448(%rbp), %r12
	movq	%r12, %rdi
	call	_ZNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
	movl	$17, %edx
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	20(%r14), %esi
	movq	%r12, %rdi
	andl	$16777215, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r14), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_8OperatorE@PLT
	movl	$12, %edx
	leaq	.LC3(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	20(%r13), %esi
	movq	%r12, %rdi
	andl	$16777215, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	0(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_8OperatorE@PLT
	leaq	.LC7(%rip), %rsi
	movq	%rax, %rdi
	jmp	.L310
.L314:
	movq	%r11, %rsi
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE16046:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker3RunEv.constprop.0, .-_ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker3RunEv.constprop.0
	.section	.rodata._ZN2v88internal8compiler20MachineGraphVerifier3RunEPNS1_5GraphEPKNS1_8ScheduleEPNS1_7LinkageEbPKcPNS0_4ZoneE.str1.8,"aMS",@progbits,1
	.align 8
.LC37:
	.string	"cannot create std::vector larger than max_size()"
	.align 8
.LC38:
	.string	"index <= static_cast<size_t>(1)"
	.section	.text._ZN2v88internal8compiler20MachineGraphVerifier3RunEPNS1_5GraphEPKNS1_8ScheduleEPNS1_7LinkageEbPKcPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20MachineGraphVerifier3RunEPNS1_5GraphEPKNS1_8ScheduleEPNS1_7LinkageEbPKcPNS0_4ZoneE
	.type	_ZN2v88internal8compiler20MachineGraphVerifier3RunEPNS1_5GraphEPKNS1_8ScheduleEPNS1_7LinkageEbPKcPNS0_4ZoneE, @function
_ZN2v88internal8compiler20MachineGraphVerifier3RunEPNS1_5GraphEPKNS1_8ScheduleEPNS1_7LinkageEbPKcPNS0_4ZoneE:
.LFB13335:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm2
	movq	%rsi, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%ecx, -188(%rbp)
	movl	28(%rdi), %edx
	movq	%rsi, -168(%rbp)
	movq	%r8, -200(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	28(%rdi), %eax
	movaps	%xmm0, -112(%rbp)
	testl	%eax, %eax
	js	.L441
	movq	%r9, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	testq	%rdx, %rdx
	je	.L410
	movq	16(%r9), %rdi
	movq	24(%r9), %rax
	leaq	7(%rdx), %rsi
	andq	$-8, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rsi
	ja	.L442
	addq	%rdi, %rsi
	movq	%rsi, 16(%r9)
.L342:
	leaq	(%rdi,%rdx), %rbx
	xorl	%esi, %esi
	movq	%rdi, -88(%rbp)
	movq	%rbx, -72(%rbp)
	call	memset@PLT
	movq	-112(%rbp), %rax
.L340:
	movq	%rbx, -80(%rbp)
	movq	24(%rax), %rsi
	movl	$4, %ebx
	movq	16(%rax), %r14
	movq	%rsi, -184(%rbp)
	cmpq	%rsi, %r14
	je	.L348
	.p2align 4,,10
	.p2align 3
.L347:
	movq	(%r14), %r12
	xorl	%r13d, %r13d
	movq	72(%r12), %rdx
	movq	80(%r12), %rax
	movq	%r12, -64(%rbp)
	subq	%rdx, %rax
	sarq	$3, %rax
	.p2align 4,,10
	.p2align 3
.L407:
	cmpq	%rax, %r13
	jnb	.L344
	movq	(%rdx,%r13,8), %r15
.L345:
	testq	%r15, %r15
	je	.L408
	movq	(%r15), %rdi
	movzwl	16(%rdi), %eax
	subl	$7, %eax
	cmpw	$629, %ax
	ja	.L349
	leaq	.L351(%rip), %rsi
	movzwl	%ax, %eax
	movslq	(%rsi,%rax,4), %rax
	addq	%rsi, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler20MachineGraphVerifier3RunEPNS1_5GraphEPKNS1_8ScheduleEPNS1_7LinkageEbPKcPNS0_4ZoneE,"a",@progbits
	.align 4
	.align 4
.L351:
	.long	.L361-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L384-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L350-.L351
	.long	.L364-.L351
	.long	.L363-.L351
	.long	.L362-.L351
	.long	.L364-.L351
	.long	.L361-.L351
	.long	.L349-.L351
	.long	.L361-.L351
	.long	.L382-.L351
	.long	.L350-.L351
	.long	.L364-.L351
	.long	.L349-.L351
	.long	.L381-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L380-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L379-.L351
	.long	.L378-.L351
	.long	.L361-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L377-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L361-.L351
	.long	.L349-.L351
	.long	.L361-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L353-.L351
	.long	.L361-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L361-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L350-.L351
	.long	.L350-.L351
	.long	.L350-.L351
	.long	.L350-.L351
	.long	.L350-.L351
	.long	.L350-.L351
	.long	.L350-.L351
	.long	.L350-.L351
	.long	.L350-.L351
	.long	.L350-.L351
	.long	.L350-.L351
	.long	.L350-.L351
	.long	.L350-.L351
	.long	.L350-.L351
	.long	.L350-.L351
	.long	.L350-.L351
	.long	.L350-.L351
	.long	.L350-.L351
	.long	.L350-.L351
	.long	.L350-.L351
	.long	.L350-.L351
	.long	.L350-.L351
	.long	.L350-.L351
	.long	.L350-.L351
	.long	.L364-.L351
	.long	.L364-.L351
	.long	.L364-.L351
	.long	.L364-.L351
	.long	.L364-.L351
	.long	.L364-.L351
	.long	.L364-.L351
	.long	.L364-.L351
	.long	.L364-.L351
	.long	.L364-.L351
	.long	.L364-.L351
	.long	.L364-.L351
	.long	.L364-.L351
	.long	.L364-.L351
	.long	.L364-.L351
	.long	.L364-.L351
	.long	.L353-.L351
	.long	.L353-.L351
	.long	.L353-.L351
	.long	.L353-.L351
	.long	.L353-.L351
	.long	.L353-.L351
	.long	.L353-.L351
	.long	.L353-.L351
	.long	.L353-.L351
	.long	.L353-.L351
	.long	.L353-.L351
	.long	.L353-.L351
	.long	.L353-.L351
	.long	.L353-.L351
	.long	.L353-.L351
	.long	.L353-.L351
	.long	.L363-.L351
	.long	.L363-.L351
	.long	.L363-.L351
	.long	.L363-.L351
	.long	.L363-.L351
	.long	.L363-.L351
	.long	.L363-.L351
	.long	.L363-.L351
	.long	.L363-.L351
	.long	.L363-.L351
	.long	.L363-.L351
	.long	.L363-.L351
	.long	.L363-.L351
	.long	.L362-.L351
	.long	.L362-.L351
	.long	.L362-.L351
	.long	.L362-.L351
	.long	.L362-.L351
	.long	.L362-.L351
	.long	.L362-.L351
	.long	.L362-.L351
	.long	.L362-.L351
	.long	.L362-.L351
	.long	.L362-.L351
	.long	.L362-.L351
	.long	.L362-.L351
	.long	.L362-.L351
	.long	.L362-.L351
	.long	.L362-.L351
	.long	.L362-.L351
	.long	.L362-.L351
	.long	.L362-.L351
	.long	.L362-.L351
	.long	.L362-.L351
	.long	.L362-.L351
	.long	.L362-.L351
	.long	.L362-.L351
	.long	.L362-.L351
	.long	.L362-.L351
	.long	.L362-.L351
	.long	.L362-.L351
	.long	.L362-.L351
	.long	.L362-.L351
	.long	.L362-.L351
	.long	.L362-.L351
	.long	.L362-.L351
	.long	.L362-.L351
	.long	.L362-.L351
	.long	.L362-.L351
	.long	.L357-.L351
	.long	.L375-.L351
	.long	.L374-.L351
	.long	.L374-.L351
	.long	.L374-.L351
	.long	.L374-.L351
	.long	.L374-.L351
	.long	.L374-.L351
	.long	.L374-.L351
	.long	.L350-.L351
	.long	.L350-.L351
	.long	.L350-.L351
	.long	.L350-.L351
	.long	.L350-.L351
	.long	.L350-.L351
	.long	.L350-.L351
	.long	.L350-.L351
	.long	.L350-.L351
	.long	.L357-.L351
	.long	.L375-.L351
	.long	.L374-.L351
	.long	.L374-.L351
	.long	.L374-.L351
	.long	.L374-.L351
	.long	.L374-.L351
	.long	.L374-.L351
	.long	.L374-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L357-.L351
	.long	.L357-.L351
	.long	.L354-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L364-.L351
	.long	.L364-.L351
	.long	.L361-.L351
	.long	.L372-.L351
	.long	.L371-.L351
	.long	.L350-.L351
	.long	.L350-.L351
	.long	.L362-.L351
	.long	.L350-.L351
	.long	.L349-.L351
	.long	.L350-.L351
	.long	.L364-.L351
	.long	.L362-.L351
	.long	.L349-.L351
	.long	.L350-.L351
	.long	.L350-.L351
	.long	.L350-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L362-.L351
	.long	.L364-.L351
	.long	.L349-.L351
	.long	.L362-.L351
	.long	.L364-.L351
	.long	.L369-.L351
	.long	.L382-.L351
	.long	.L371-.L351
	.long	.L361-.L351
	.long	.L366-.L351
	.long	.L372-.L351
	.long	.L363-.L351
	.long	.L350-.L351
	.long	.L350-.L351
	.long	.L363-.L351
	.long	.L363-.L351
	.long	.L362-.L351
	.long	.L363-.L351
	.long	.L363-.L351
	.long	.L362-.L351
	.long	.L350-.L351
	.long	.L364-.L351
	.long	.L363-.L351
	.long	.L349-.L351
	.long	.L350-.L351
	.long	.L350-.L351
	.long	.L362-.L351
	.long	.L362-.L351
	.long	.L361-.L351
	.long	.L350-.L351
	.long	.L364-.L351
	.long	.L364-.L351
	.long	.L364-.L351
	.long	.L357-.L351
	.long	.L356-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L357-.L351
	.long	.L354-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L353-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L352-.L351
	.long	.L350-.L351
	.long	.L352-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L350-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L349-.L351
	.long	.L350-.L351
	.section	.text._ZN2v88internal8compiler20MachineGraphVerifier3RunEPNS1_5GraphEPKNS1_8ScheduleEPNS1_7LinkageEbPKcPNS0_4ZoneE
	.p2align 4,,10
	.p2align 3
.L374:
	call	_ZN2v88internal8compiler12AtomicOpTypeEPKNS1_8OperatorE@PLT
.L438:
	movl	20(%r15), %edx
	leal	-2(%rax), %ecx
	andl	$16777215, %edx
	addq	-88(%rbp), %rdx
	cmpb	$2, %cl
	cmovbe	%ebx, %eax
	movb	%al, (%rdx)
	.p2align 4,,10
	.p2align 3
.L349:
	movq	72(%r12), %rdx
	movq	80(%r12), %rax
	addq	$1, %r13
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%r13, %rax
	jnb	.L407
.L408:
	addq	$8, %r14
	cmpq	%r14, -184(%rbp)
	jne	.L347
.L348:
	movq	-168(%rbp), %rax
	leaq	-160(%rbp), %rdi
	movq	$0, -128(%rbp)
	movq	%rax, -160(%rbp)
	leaq	-112(%rbp), %rax
	movq	%rax, -152(%rbp)
	movzbl	-188(%rbp), %eax
	movb	%al, -144(%rbp)
	movq	-200(%rbp), %rax
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal8compiler12_GLOBAL__N_128MachineRepresentationChecker3RunEv.constprop.0
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L443
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L353:
	.cfi_restore_state
	movl	20(%r15), %eax
	movq	-88(%rbp), %rdx
	andl	$16777215, %eax
	movb	$1, (%rdx,%rax)
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L350:
	movl	20(%r15), %eax
	movq	-88(%rbp), %rdx
	andl	$16777215, %eax
	movb	$4, (%rdx,%rax)
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L361:
	movl	20(%r15), %eax
	movq	-88(%rbp), %rdx
	andl	$16777215, %eax
	movb	$8, (%rdx,%rax)
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L362:
	movl	20(%r15), %eax
	movq	-88(%rbp), %rdx
	andl	$16777215, %eax
	movb	$13, (%rdx,%rax)
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L363:
	movl	20(%r15), %eax
	movq	-88(%rbp), %rdx
	andl	$16777215, %eax
	movb	$12, (%rdx,%rax)
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L364:
	movl	20(%r15), %eax
	movq	-88(%rbp), %rdx
	andl	$16777215, %eax
	movb	$5, (%rdx,%rax)
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L357:
	call	_ZN2v88internal8compiler20LoadRepresentationOfEPKNS1_8OperatorE@PLT
	jmp	.L438
	.p2align 4,,10
	.p2align 3
.L354:
	call	_ZN2v88internal8compiler21StoreRepresentationOfEPKNS1_8OperatorE@PLT
.L439:
	movzbl	(%rax), %ecx
.L437:
	movl	20(%r15), %eax
	leal	-2(%rcx), %edx
	andl	$16777215, %eax
	addq	-88(%rbp), %rax
	cmpb	$2, %dl
	cmovbe	%ebx, %ecx
	movb	%cl, (%rax)
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L372:
	movl	20(%r15), %eax
	movq	-88(%rbp), %rdx
	andl	$16777215, %eax
	movb	$6, (%rdx,%rax)
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L382:
	movl	20(%r15), %eax
	movq	-88(%rbp), %rdx
	andl	$16777215, %eax
	movb	$10, (%rdx,%rax)
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L371:
	movl	20(%r15), %eax
	movq	-88(%rbp), %rdx
	andl	$16777215, %eax
	movb	$9, (%rdx,%rax)
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L352:
	movl	20(%r15), %eax
	movq	-88(%rbp), %rdx
	andl	$16777215, %eax
	movb	$14, (%rdx,%rax)
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L375:
	call	_ZN2v88internal8compiler27AtomicStoreRepresentationOfEPKNS1_8OperatorE@PLT
	jmp	.L438
	.p2align 4,,10
	.p2align 3
.L366:
	movl	20(%r15), %eax
	movq	-88(%rbp), %rdx
	andl	$16777215, %eax
	movb	$7, (%rdx,%rax)
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L381:
	movl	20(%r15), %edx
	andl	$16777215, %edx
	addq	-88(%rbp), %rdx
	movq	%rdx, %r15
	call	_ZN2v88internal8compiler19PhiRepresentationOfEPKNS1_8OperatorE@PLT
	movb	%al, (%r15)
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L379:
	call	_ZN2v88internal8compiler16CallDescriptorOfEPKNS1_8OperatorE@PLT
	movq	24(%rax), %rax
	cmpq	$0, (%rax)
	je	.L361
	movq	16(%rax), %rax
	movzbl	4(%rax), %ecx
.L436:
	movl	20(%r15), %eax
	movq	-88(%rbp), %rdx
	andl	$16777215, %eax
	movb	%cl, (%rdx,%rax)
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L380:
	movl	20(%r15), %eax
	movq	-88(%rbp), %rdx
	andl	$16777215, %eax
	movb	$0, (%rdx,%rax)
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L356:
	call	_ZN2v88internal8compiler30UnalignedStoreRepresentationOfEPKNS1_8OperatorE@PLT
	jmp	.L439
	.p2align 4,,10
	.p2align 3
.L384:
	movq	-104(%rbp), %rax
	movq	(%rax), %rax
	movq	24(%rax), %rax
	movq	16(%rax), %rax
	movzbl	4(%rax), %ecx
	jmp	.L437
	.p2align 4,,10
	.p2align 3
.L369:
	movl	20(%r15), %eax
	movq	-88(%rbp), %rdx
	andl	$16777215, %eax
	movb	$11, (%rdx,%rax)
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L377:
	movl	20(%r15), %eax
	andl	$16777215, %eax
	addq	-88(%rbp), %rax
	movq	%rax, -176(%rbp)
	call	_ZN2v88internal8compiler17ProjectionIndexOfEPKNS1_8OperatorE@PLT
	leaq	32(%r15), %rsi
	movq	%rax, %rcx
	movzbl	23(%r15), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L387
	movq	32(%r15), %rsi
	addq	$16, %rsi
.L387:
	movq	(%rsi), %rax
	movq	(%rax), %rdi
	movzwl	16(%rdi), %eax
	cmpw	$328, %ax
	ja	.L388
	cmpw	$306, %ax
	ja	.L389
	cmpw	$49, %ax
	jne	.L413
	movq	%rcx, -208(%rbp)
	call	_ZN2v88internal8compiler16CallDescriptorOfEPKNS1_8OperatorE@PLT
	movq	-208(%rbp), %rcx
	movq	24(%rax), %rax
	movq	16(%rax), %rax
	movzbl	4(%rax,%rcx,8), %eax
.L391:
	movq	-176(%rbp), %rsi
	movb	%al, (%rsi)
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L378:
	movq	-104(%rbp), %rcx
	movq	%rcx, -176(%rbp)
	call	_ZN2v88internal8compiler16ParameterIndexOfEPKNS1_8OperatorE@PLT
	movq	-176(%rbp), %rcx
	addl	$1, %eax
	movq	(%rcx), %rsi
	cltq
	movzbl	12(%rsi), %ecx
	testq	%rax, %rax
	je	.L436
	movq	24(%rsi), %rcx
	addq	(%rcx), %rax
	movq	16(%rcx), %rcx
	movzbl	-4(%rcx,%rax,8), %ecx
	jmp	.L436
	.p2align 4,,10
	.p2align 3
.L389:
	subw	$307, %ax
	cmpw	$21, %ax
	ja	.L413
	leaq	.L393(%rip), %rsi
	movzwl	%ax, %eax
	movslq	(%rsi,%rax,4), %rax
	addq	%rsi, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler20MachineGraphVerifier3RunEPNS1_5GraphEPKNS1_8ScheduleEPNS1_7LinkageEbPKcPNS0_4ZoneE
	.align 4
	.align 4
.L393:
	.long	.L394-.L393
	.long	.L413-.L393
	.long	.L394-.L393
	.long	.L413-.L393
	.long	.L394-.L393
	.long	.L413-.L393
	.long	.L413-.L393
	.long	.L413-.L393
	.long	.L413-.L393
	.long	.L413-.L393
	.long	.L413-.L393
	.long	.L413-.L393
	.long	.L413-.L393
	.long	.L413-.L393
	.long	.L413-.L393
	.long	.L413-.L393
	.long	.L413-.L393
	.long	.L413-.L393
	.long	.L413-.L393
	.long	.L433-.L393
	.long	.L413-.L393
	.long	.L433-.L393
	.section	.text._ZN2v88internal8compiler20MachineGraphVerifier3RunEPNS1_5GraphEPKNS1_8ScheduleEPNS1_7LinkageEbPKcPNS0_4ZoneE
	.p2align 4,,10
	.p2align 3
.L395:
	subw	$457, %ax
	cmpw	$2, %ax
	jbe	.L433
.L413:
	xorl	%eax, %eax
	jmp	.L391
.L433:
	cmpq	$1, %rcx
	ja	.L399
	sbbl	%eax, %eax
	andl	$4, %eax
	addl	$1, %eax
	jmp	.L391
.L394:
	cmpq	$1, %rcx
	ja	.L399
	sbbl	%eax, %eax
	andl	$3, %eax
	addl	$1, %eax
	jmp	.L391
	.p2align 4,,10
	.p2align 3
.L388:
	cmpw	$416, %ax
	ja	.L395
	cmpw	$409, %ax
	ja	.L396
	cmpw	$408, %ax
	jne	.L413
.L396:
	cmpq	$1, %rcx
	ja	.L399
	movl	$4, %eax
	jmp	.L391
	.p2align 4,,10
	.p2align 3
.L344:
	movq	56(%r12), %r15
	jmp	.L345
	.p2align 4,,10
	.p2align 3
.L410:
	movq	%rsi, %rax
	xorl	%ebx, %ebx
	jmp	.L340
	.p2align 4,,10
	.p2align 3
.L442:
	movq	%r9, %rdi
	movq	%rdx, -176(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-176(%rbp), %rdx
	movq	%rax, %rdi
	jmp	.L342
	.p2align 4,,10
	.p2align 3
.L399:
	leaq	.LC38(%rip), %rsi
	leaq	.LC21(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L443:
	call	__stack_chk_fail@PLT
.L441:
	leaq	.LC37(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE13335:
	.size	_ZN2v88internal8compiler20MachineGraphVerifier3RunEPNS1_5GraphEPKNS1_8ScheduleEPNS1_7LinkageEbPKcPNS0_4ZoneE, .-_ZN2v88internal8compiler20MachineGraphVerifier3RunEPNS1_5GraphEPKNS1_8ScheduleEPNS1_7LinkageEbPKcPNS0_4ZoneE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler20MachineGraphVerifier3RunEPNS1_5GraphEPKNS1_8ScheduleEPNS1_7LinkageEbPKcPNS0_4ZoneE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler20MachineGraphVerifier3RunEPNS1_5GraphEPKNS1_8ScheduleEPNS1_7LinkageEbPKcPNS0_4ZoneE, @function
_GLOBAL__sub_I__ZN2v88internal8compiler20MachineGraphVerifier3RunEPNS1_5GraphEPKNS1_8ScheduleEPNS1_7LinkageEbPKcPNS0_4ZoneE:
.LFB15960:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE15960:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler20MachineGraphVerifier3RunEPNS1_5GraphEPKNS1_8ScheduleEPNS1_7LinkageEbPKcPNS0_4ZoneE, .-_GLOBAL__sub_I__ZN2v88internal8compiler20MachineGraphVerifier3RunEPNS1_5GraphEPKNS1_8ScheduleEPNS1_7LinkageEbPKcPNS0_4ZoneE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler20MachineGraphVerifier3RunEPNS1_5GraphEPKNS1_8ScheduleEPNS1_7LinkageEbPKcPNS0_4ZoneE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.data.rel.ro,"aw"
	.align 8
.LC34:
	.quad	_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE+24
	.align 8
.LC35:
	.quad	_ZTVSt15basic_streambufIcSt11char_traitsIcEE+16
	.align 8
.LC36:
	.quad	_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE+16
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
