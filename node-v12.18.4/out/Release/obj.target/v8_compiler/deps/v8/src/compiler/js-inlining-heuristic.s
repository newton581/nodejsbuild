	.file	"js-inlining-heuristic.cc"
	.text
	.section	.text._ZNKSt5ctypeIcE8do_widenEc,"axG",@progbits,_ZNKSt5ctypeIcE8do_widenEc,comdat
	.align 2
	.p2align 4
	.weak	_ZNKSt5ctypeIcE8do_widenEc
	.type	_ZNKSt5ctypeIcE8do_widenEc, @function
_ZNKSt5ctypeIcE8do_widenEc:
.LFB1527:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	ret
	.cfi_endproc
.LFE1527:
	.size	_ZNKSt5ctypeIcE8do_widenEc, .-_ZNKSt5ctypeIcE8do_widenEc
	.section	.rodata._ZNK2v88internal8compiler19JSInliningHeuristic12reducer_nameEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"JSInliningHeuristic"
	.section	.text._ZNK2v88internal8compiler19JSInliningHeuristic12reducer_nameEv,"axG",@progbits,_ZNK2v88internal8compiler19JSInliningHeuristic12reducer_nameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal8compiler19JSInliningHeuristic12reducer_nameEv
	.type	_ZNK2v88internal8compiler19JSInliningHeuristic12reducer_nameEv, @function
_ZNK2v88internal8compiler19JSInliningHeuristic12reducer_nameEv:
.LFB10819:
	.cfi_startproc
	endbr64
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE10819:
	.size	_ZNK2v88internal8compiler19JSInliningHeuristic12reducer_nameEv, .-_ZNK2v88internal8compiler19JSInliningHeuristic12reducer_nameEv
	.section	.text._ZN2v88internal12StdoutStreamD1Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD1Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StdoutStreamD1Ev
	.type	_ZN2v88internal12StdoutStreamD1Ev, @function
_ZN2v88internal12StdoutStreamD1Ev:
.LFB24874:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE24874:
	.size	_ZN2v88internal12StdoutStreamD1Ev, .-_ZN2v88internal12StdoutStreamD1Ev
	.section	.text._ZN2v88internal8compiler12MachineGraph4DeadEv.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler12MachineGraph4DeadEv.part.0, @function
_ZN2v88internal8compiler12MachineGraph4DeadEv.part.0:
.LFB24971:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	(%rdi), %r12
	movq	8(%rdi), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4DeadEv@PLT
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rax, %rsi
	xorl	%edx, %edx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 352(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24971:
	.size	_ZN2v88internal8compiler12MachineGraph4DeadEv.part.0, .-_ZN2v88internal8compiler12MachineGraph4DeadEv.part.0
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_127CollectStateValuesOwnedUsesEPNS1_4NodeES4_PNS2_12NodeAndIndexEPmm.constprop.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_127CollectStateValuesOwnedUsesEPNS1_4NodeES4_PNS2_12NodeAndIndexEPmm.constprop.0, @function
_ZN2v88internal8compiler12_GLOBAL__N_127CollectStateValuesOwnedUsesEPNS1_4NodeES4_PNS2_12NodeAndIndexEPmm.constprop.0:
.LFB25022:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movq	%rsi, %rdi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -56(%rbp)
	call	_ZNK2v88internal8compiler4Node8UseCountEv@PLT
	cmpl	$1, %eax
	jg	.L9
	movl	20(%r13), %edi
	xorl	%ebx, %ebx
	leaq	32(%r13), %r15
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L24:
	cmpl	%ebx, %eax
	jle	.L9
	leaq	(%r15,%rbx,8), %rax
	movq	(%rax), %rsi
	movq	(%rsi), %rax
	cmpw	$42, 16(%rax)
	je	.L22
.L14:
	cmpq	%r14, %rsi
	je	.L23
.L17:
	addq	$1, %rbx
.L19:
	movl	%edi, %eax
	movl	%ebx, %edx
	shrl	$24, %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L24
	movq	32(%r13), %rax
	cmpl	%ebx, 8(%rax)
	jle	.L9
	leaq	16(%rax,%rbx,8), %rax
	movq	(%rax), %rsi
	movq	(%rsi), %rax
	cmpw	$42, 16(%rax)
	jne	.L14
.L22:
	movq	-56(%rbp), %rdx
	movq	%r12, %rcx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_127CollectStateValuesOwnedUsesEPNS1_4NodeES4_PNS2_12NodeAndIndexEPmm.constprop.0
	testb	%al, %al
	jne	.L21
.L18:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	.cfi_restore_state
	movq	(%r12), %rax
	cmpq	$7, %rax
	ja	.L18
	salq	$4, %rax
	addq	-56(%rbp), %rax
	movq	%r13, (%rax)
	movl	%edx, 8(%rax)
	addq	$1, (%r12)
.L21:
	movl	20(%r13), %edi
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L9:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25022:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_127CollectStateValuesOwnedUsesEPNS1_4NodeES4_PNS2_12NodeAndIndexEPmm.constprop.0, .-_ZN2v88internal8compiler12_GLOBAL__N_127CollectStateValuesOwnedUsesEPNS1_4NodeES4_PNS2_12NodeAndIndexEPmm.constprop.0
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_127CollectFrameStateUniqueUsesEPNS1_4NodeES4_PNS2_12NodeAndIndexEPmm.constprop.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_127CollectFrameStateUniqueUsesEPNS1_4NodeES4_PNS2_12NodeAndIndexEPmm.constprop.0, @function
_ZN2v88internal8compiler12_GLOBAL__N_127CollectFrameStateUniqueUsesEPNS1_4NodeES4_PNS2_12NodeAndIndexEPmm.constprop.0:
.LFB25024:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	call	_ZNK2v88internal8compiler4Node8UseCountEv@PLT
	cmpl	$1, %eax
	jg	.L32
	movzbl	23(%rbx), %edx
	leaq	32(%rbx), %rax
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L27
	cmpq	16(%rax), %r12
	je	.L28
.L29:
	addq	$8, %rax
.L31:
	movq	(%rax), %rsi
	popq	%rbx
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_127CollectStateValuesOwnedUsesEPNS1_4NodeES4_PNS2_12NodeAndIndexEPmm.constprop.0
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore_state
	movl	$1, %eax
.L25:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	.cfi_restore_state
	movq	32(%rbx), %rdx
	leaq	16(%rdx), %rcx
	cmpq	32(%rdx), %r12
	je	.L28
.L30:
	leaq	8(%rcx), %rax
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L28:
	movq	(%r14), %rdx
	cmpq	$7, %rdx
	ja	.L33
	salq	$4, %rdx
	addq	%r13, %rdx
	movq	%rbx, (%rdx)
	movl	$2, 8(%rdx)
	addq	$1, (%r14)
	movzbl	23(%rbx), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L29
	movq	32(%rbx), %rax
	leaq	16(%rax), %rcx
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L33:
	xorl	%eax, %eax
	jmp	.L25
	.cfi_endproc
.LFE25024:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_127CollectFrameStateUniqueUsesEPNS1_4NodeES4_PNS2_12NodeAndIndexEPmm.constprop.0, .-_ZN2v88internal8compiler12_GLOBAL__N_127CollectFrameStateUniqueUsesEPNS1_4NodeES4_PNS2_12NodeAndIndexEPmm.constprop.0
	.section	.text._ZN2v88internal12StdoutStreamD0Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD0Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.type	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev, @function
_ZTv0_n24_N2v88internal12StdoutStreamD0Ev:
.LFB25020:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %rax
	addq	-24(%rax), %rdi
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	movq	%rax, 80(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE25020:
	.size	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev, .-_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StdoutStreamD0Ev
	.type	_ZN2v88internal12StdoutStreamD0Ev, @function
_ZN2v88internal12StdoutStreamD0Ev:
.LFB24875:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE24875:
	.size	_ZN2v88internal12StdoutStreamD0Ev, .-_ZN2v88internal12StdoutStreamD0Ev
	.section	.text._ZN2v88internal12StdoutStreamD1Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD1Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.type	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev, @function
_ZTv0_n24_N2v88internal12StdoutStreamD1Ev:
.LFB25021:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rax
	movq	-24(%rax), %rbx
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	addq	%rdi, %rbx
	movq	%rax, 80(%rbx)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	64(%rbx), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE25021:
	.size	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev, .-_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.section	.text._ZN2v88internal8compiler19JSInliningHeuristic29DuplicateStateValuesAndRenameEPNS1_4NodeES4_S4_NS2_14StateCloneModeE.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler19JSInliningHeuristic29DuplicateStateValuesAndRenameEPNS1_4NodeES4_S4_NS2_14StateCloneModeE.part.0, @function
_ZN2v88internal8compiler19JSInliningHeuristic29DuplicateStateValuesAndRenameEPNS1_4NodeES4_S4_NS2_14StateCloneModeE.part.0:
.LFB24952:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	32(%rsi), %rax
	movl	$0, %r10d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$56, %rsp
	movl	20(%rsi), %edx
	cmpl	$1, %r8d
	movq	%rax, -56(%rbp)
	cmove	%rsi, %r10
	movq	%rdi, -88(%rbp)
	xorl	%r14d, %r14d
	movl	%edx, %eax
	movq	%rcx, -64(%rbp)
	shrl	$24, %eax
	movl	%r8d, -92(%rbp)
	movq	%r10, %r13
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L43
.L72:
	cmpl	%r14d, %eax
	jle	.L45
	movq	-56(%rbp), %rax
	leaq	(%rax,%r14,8), %rax
	movq	(%rax), %rsi
	movq	(%rsi), %rax
	cmpw	$42, 16(%rax)
	je	.L70
.L48:
	leaq	1(%r14), %r15
	cmpq	%rbx, %rsi
	je	.L71
.L50:
	movl	%edx, %eax
	movq	%r15, %r14
	shrl	$24, %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L72
.L43:
	movq	32(%r12), %rax
	cmpl	%r14d, 8(%rax)
	jle	.L45
	leaq	16(%rax,%r14,8), %rax
	movq	(%rax), %rsi
	movq	(%rsi), %rax
	cmpw	$42, 16(%rax)
	jne	.L48
.L70:
	movq	%rsi, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZNK2v88internal8compiler4Node8UseCountEv@PLT
	movq	-72(%rbp), %rsi
	cmpl	$1, %eax
	jg	.L68
	movl	-92(%rbp), %r8d
	movq	-64(%rbp), %rcx
	movq	%rbx, %rdx
	movq	%rsi, -72(%rbp)
	movq	-88(%rbp), %rdi
	call	_ZN2v88internal8compiler19JSInliningHeuristic29DuplicateStateValuesAndRenameEPNS1_4NodeES4_S4_NS2_14StateCloneModeE.part.0
	movq	-72(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L45:
	testq	%r13, %r13
	cmove	%r12, %r13
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	movq	%r13, %rax
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L71:
	.cfi_restore_state
	movq	-64(%rbp), %r8
.L51:
	cmpq	%r8, %rsi
	je	.L68
	testq	%r13, %r13
	je	.L73
.L53:
	movzbl	23(%r13), %eax
	leaq	0(,%r14,8), %rdx
	leaq	32(%r13), %rcx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L54
	addq	%rcx, %rdx
	movq	%r13, %rcx
	movq	(%rdx), %rdi
	cmpq	%rdi, %r8
	je	.L68
.L55:
	leaq	1(%r14), %r15
	leaq	0(,%r15,4), %rsi
	movq	%r15, %rax
	subq	%rsi, %rax
	leaq	(%rcx,%rax,8), %r14
	testq	%rdi, %rdi
	je	.L56
	movq	%r14, %rsi
	movq	%rdx, -80(%rbp)
	movq	%r8, -72(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-80(%rbp), %rdx
	movq	-72(%rbp), %r8
.L56:
	movq	%r8, (%rdx)
	testq	%r8, %r8
	je	.L69
	movq	%r14, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L69:
	movl	20(%r12), %edx
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L54:
	movq	32(%r13), %rcx
	leaq	16(%rcx,%rdx), %rdx
	movq	(%rdx), %rdi
	cmpq	%rdi, %r8
	jne	.L55
.L68:
	movl	20(%r12), %edx
	leaq	1(%r14), %r15
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L73:
	movq	-88(%rbp), %rax
	movq	%r12, %rsi
	movq	%r8, -72(%rbp)
	movq	200(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler5Graph9CloneNodeEPKNS1_4NodeE@PLT
	movq	-72(%rbp), %r8
	movq	%rax, %r13
	jmp	.L53
	.cfi_endproc
.LFE24952:
	.size	_ZN2v88internal8compiler19JSInliningHeuristic29DuplicateStateValuesAndRenameEPNS1_4NodeES4_S4_NS2_14StateCloneModeE.part.0, .-_ZN2v88internal8compiler19JSInliningHeuristic29DuplicateStateValuesAndRenameEPNS1_4NodeES4_S4_NS2_14StateCloneModeE.part.0
	.section	.rodata._ZN2v88internal8compiler21SharedFunctionInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb.str1.1,"aMS",@progbits,1
.LC1:
	.string	"IsSharedFunctionInfo()"
.LC2:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal8compiler21SharedFunctionInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,"axG",@progbits,_ZN2v88internal8compiler21SharedFunctionInfoRefC5EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler21SharedFunctionInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.type	_ZN2v88internal8compiler21SharedFunctionInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, @function
_ZN2v88internal8compiler21SharedFunctionInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb:
.LFB9759:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%ecx, %ebx
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	testb	%bl, %bl
	jne	.L77
.L74:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L77:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef20IsSharedFunctionInfoEv@PLT
	testb	%al, %al
	jne	.L74
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE9759:
	.size	_ZN2v88internal8compiler21SharedFunctionInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, .-_ZN2v88internal8compiler21SharedFunctionInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.weak	_ZN2v88internal8compiler21SharedFunctionInfoRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.set	_ZN2v88internal8compiler21SharedFunctionInfoRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,_ZN2v88internal8compiler21SharedFunctionInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.section	.rodata._ZN2v88internal8compiler19JSInliningHeuristic16CollectFunctionsEPNS1_4NodeEi.str1.1,"aMS",@progbits,1
.LC3:
	.string	"IsHeapObject()"
.LC4:
	.string	"storage_.is_populated_"
	.section	.text._ZN2v88internal8compiler19JSInliningHeuristic16CollectFunctionsEPNS1_4NodeEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19JSInliningHeuristic16CollectFunctionsEPNS1_4NodeEi
	.type	_ZN2v88internal8compiler19JSInliningHeuristic16CollectFunctionsEPNS1_4NodeEi, @function
_ZN2v88internal8compiler19JSInliningHeuristic16CollectFunctionsEPNS1_4NodeEi:
.LFB20554:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 3, -56
	movl	%ecx, -144(%rbp)
	movq	32(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rdx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L79
	movq	16(%rbx), %rbx
.L79:
	xorl	%eax, %eax
	movl	$12, %ecx
	movq	%r13, %rdi
	movb	$0, 200(%r13)
	rep stosq
	leaq	104(%r13), %rdi
	movl	$12, %ecx
	movb	$0, 208(%r13)
	rep stosq
	movq	(%rbx), %rax
	movq	$2143289344, 240(%r13)
	movq	%rdx, 232(%r13)
	cmpw	$30, 16(%rax)
	jne	.L80
	movq	48(%rax), %r14
	leaq	-128(%rbp), %r15
	movq	208(%r12), %rsi
	movq	%r15, %rdi
	movq	%r14, %rdx
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L82
	movdqa	-128(%rbp), %xmm3
	leaq	-80(%rbp), %r15
	movq	%r15, %rdi
	movaps	%xmm3, -80(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef12IsJSFunctionEv@PLT
	testb	%al, %al
	jne	.L129
.L80:
	movq	(%rbx), %rdi
	movzwl	16(%rdi), %eax
	cmpw	$35, %ax
	je	.L130
	cmpw	$723, %ax
	je	.L131
.L92:
	movl	$0, 224(%r13)
.L78:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L132
	addq	$152, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L130:
	.cfi_restore_state
	movl	20(%rdi), %eax
	movl	%eax, -156(%rbp)
	cmpl	%eax, -144(%rbp)
	jl	.L92
	movl	-156(%rbp), %eax
	testl	%eax, %eax
	jle	.L100
	leaq	32(%rbx), %rax
	movq	%rbx, -144(%rbp)
	movq	%r13, %r15
	movq	%rax, -192(%rbp)
	movl	-156(%rbp), %eax
	subl	$1, %eax
	leaq	8(%rbx,%rax,8), %rax
	movq	%rax, -184(%rbp)
	leaq	-96(%rbp), %rax
	movq	%rax, -152(%rbp)
.L101:
	movq	-144(%rbp), %rsi
	movzbl	23(%rbx), %eax
	movq	%rsi, %rdx
	andl	$15, %eax
	subq	%rbx, %rdx
	cmpl	$15, %eax
	je	.L90
	leaq	32(%rsi), %rax
.L91:
	movq	(%rax), %rax
	movq	(%rax), %rax
	cmpw	$30, 16(%rax)
	jne	.L92
	movq	-152(%rbp), %r14
	movq	48(%rax), %rdx
	xorl	%ecx, %ecx
	movq	208(%r12), %rsi
	movq	%r14, %rdi
	movq	%rdx, -176(%rbp)
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L82
	movdqa	-96(%rbp), %xmm1
	leaq	-80(%rbp), %r14
	movq	%r14, %rdi
	movaps	%xmm1, -80(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef12IsJSFunctionEv@PLT
	testb	%al, %al
	je	.L92
	movq	208(%r12), %rsi
	movq	-176(%rbp), %rdx
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L82
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12AsJSFunctionEv@PLT
	cmpb	$0, (%r15)
	movq	%rax, -176(%rbp)
	movq	%rdx, -168(%rbp)
	movdqa	-176(%rbp), %xmm0
	je	.L133
	movups	%xmm0, 8(%r15)
.L95:
	cmpb	$0, (%r15)
	je	.L134
	movdqu	8(%r15), %xmm2
	movq	-152(%rbp), %rdi
	movaps	%xmm2, -96(%rbp)
	call	_ZNK2v88internal8compiler13JSFunctionRef26IsSerializedForCompilationEv@PLT
	testb	%al, %al
	jne	.L135
.L97:
	addq	$8, -144(%rbp)
	addq	$24, %r15
	movq	-144(%rbp), %rax
	cmpq	%rax, -184(%rbp)
	jne	.L101
.L100:
	movl	-156(%rbp), %eax
	movl	%eax, 224(%r13)
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L131:
	call	_ZN2v88internal8compiler25CreateClosureParametersOfEPKNS1_8OperatorE@PLT
	movq	208(%r12), %rsi
	leaq	-80(%rbp), %r12
	movl	$1, %ecx
	movq	(%rax), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler21SharedFunctionInfoRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movdqa	-80(%rbp), %xmm0
	movb	$1, 200(%r13)
	movq	%r12, %rdi
	movups	%xmm0, 208(%r13)
	movaps	%xmm0, -80(%rbp)
	call	_ZNK2v88internal8compiler21SharedFunctionInfoRef16HasBytecodeArrayEv@PLT
	testb	%al, %al
	jne	.L136
.L103:
	movl	$1, 224(%r13)
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L129:
	movq	208(%r12), %rsi
	leaq	-112(%rbp), %r12
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L82
	movdqa	-112(%rbp), %xmm4
	movq	%r15, %rdi
	leaq	-96(%rbp), %r12
	movaps	%xmm4, -80(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef12AsJSFunctionEv@PLT
	movb	$1, 0(%r13)
	movq	%r12, %rdi
	movq	%rax, -144(%rbp)
	movq	%rdx, -136(%rbp)
	movdqa	-144(%rbp), %xmm0
	movups	%xmm0, 8(%r13)
	movaps	%xmm0, -96(%rbp)
	call	_ZNK2v88internal8compiler13JSFunctionRef26IsSerializedForCompilationEv@PLT
	testb	%al, %al
	je	.L103
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13JSFunctionRef6sharedEv@PLT
	movq	%r15, %rdi
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZNK2v88internal8compiler21SharedFunctionInfoRef16GetBytecodeArrayEv@PLT
	movb	$1, 104(%r13)
	movq	%rax, -144(%rbp)
	movq	%rdx, -136(%rbp)
	movdqa	-144(%rbp), %xmm6
	movups	%xmm6, 112(%r13)
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L82:
	leaq	.LC3(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L133:
	movb	$1, (%r15)
	movups	%xmm0, 8(%r15)
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L90:
	movq	-192(%rbp), %rax
	movq	(%rax), %rax
	leaq	16(%rax,%rdx), %rax
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L135:
	movq	-152(%rbp), %rdi
	call	_ZNK2v88internal8compiler13JSFunctionRef6sharedEv@PLT
	movq	%r14, %rdi
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZNK2v88internal8compiler21SharedFunctionInfoRef16GetBytecodeArrayEv@PLT
	cmpb	$0, 104(%r15)
	movq	%rax, -176(%rbp)
	movq	%rdx, -168(%rbp)
	movdqa	-176(%rbp), %xmm0
	je	.L98
	movups	%xmm0, 112(%r15)
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L98:
	movb	$1, 104(%r15)
	movups	%xmm0, 112(%r15)
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L136:
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler21SharedFunctionInfoRef16GetBytecodeArrayEv@PLT
	movb	$1, 104(%r13)
	movq	%rax, -144(%rbp)
	movq	%rdx, -136(%rbp)
	movdqa	-144(%rbp), %xmm5
	movups	%xmm5, 112(%r13)
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L134:
	leaq	.LC4(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L132:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20554:
	.size	_ZN2v88internal8compiler19JSInliningHeuristic16CollectFunctionsEPNS1_4NodeEi, .-_ZN2v88internal8compiler19JSInliningHeuristic16CollectFunctionsEPNS1_4NodeEi
	.section	.text._ZN2v88internal8compiler19JSInliningHeuristic29DuplicateStateValuesAndRenameEPNS1_4NodeES4_S4_NS2_14StateCloneModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19JSInliningHeuristic29DuplicateStateValuesAndRenameEPNS1_4NodeES4_S4_NS2_14StateCloneModeE
	.type	_ZN2v88internal8compiler19JSInliningHeuristic29DuplicateStateValuesAndRenameEPNS1_4NodeES4_S4_NS2_14StateCloneModeE, @function
_ZN2v88internal8compiler19JSInliningHeuristic29DuplicateStateValuesAndRenameEPNS1_4NodeES4_S4_NS2_14StateCloneModeE:
.LFB20599:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	subq	$8, %rsp
	call	_ZNK2v88internal8compiler4Node8UseCountEv@PLT
	cmpl	$1, %eax
	jg	.L138
	addq	$8, %rsp
	movl	%ebx, %r8d
	movq	%r15, %rcx
	movq	%r14, %rdx
	popq	%rbx
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler19JSInliningHeuristic29DuplicateStateValuesAndRenameEPNS1_4NodeES4_S4_NS2_14StateCloneModeE.part.0
	.p2align 4,,10
	.p2align 3
.L138:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20599:
	.size	_ZN2v88internal8compiler19JSInliningHeuristic29DuplicateStateValuesAndRenameEPNS1_4NodeES4_S4_NS2_14StateCloneModeE, .-_ZN2v88internal8compiler19JSInliningHeuristic29DuplicateStateValuesAndRenameEPNS1_4NodeES4_S4_NS2_14StateCloneModeE
	.section	.text._ZN2v88internal8compiler19JSInliningHeuristic28DuplicateFrameStateAndRenameEPNS1_4NodeES4_S4_NS2_14StateCloneModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19JSInliningHeuristic28DuplicateFrameStateAndRenameEPNS1_4NodeES4_S4_NS2_14StateCloneModeE
	.type	_ZN2v88internal8compiler19JSInliningHeuristic28DuplicateFrameStateAndRenameEPNS1_4NodeES4_S4_NS2_14StateCloneModeE, @function
_ZN2v88internal8compiler19JSInliningHeuristic28DuplicateFrameStateAndRenameEPNS1_4NodeES4_S4_NS2_14StateCloneModeE:
.LFB20601:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	subq	$40, %rsp
	movq	%rdi, -56(%rbp)
	movq	%rsi, %rdi
	call	_ZNK2v88internal8compiler4Node8UseCountEv@PLT
	cmpl	$1, %eax
	jg	.L141
	cmpl	$1, %ebx
	movzbl	23(%r12), %eax
	movl	$0, %r9d
	leaq	32(%r12), %rdx
	cmove	%r12, %r9
	andl	$15, %eax
	movq	%r9, %r15
	cmpl	$15, %eax
	je	.L143
	cmpq	16(%rdx), %r13
	je	.L144
.L145:
	leaq	8(%rdx), %rax
.L153:
	movq	(%rax), %rsi
	movq	%rsi, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZNK2v88internal8compiler4Node8UseCountEv@PLT
	cmpl	$1, %eax
	jg	.L154
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	movq	%r13, %rdx
	movl	%ebx, %r8d
	movq	%r14, %rcx
	call	_ZN2v88internal8compiler19JSInliningHeuristic29DuplicateStateValuesAndRenameEPNS1_4NodeES4_S4_NS2_14StateCloneModeE.part.0
	movq	-64(%rbp), %rsi
	movq	%rax, %r13
	cmpq	%rax, %rsi
	je	.L154
	testq	%r15, %r15
	je	.L174
.L155:
	movzbl	23(%r15), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L156
	movq	40(%r15), %rdi
	cmpq	%rdi, %r13
	je	.L140
	leaq	40(%r15), %rbx
	movq	%r15, %rsi
.L158:
	leaq	-48(%rsi), %r12
	testq	%rdi, %rdi
	je	.L159
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L159:
	movq	%r13, (%rbx)
	testq	%r13, %r13
	je	.L140
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	jmp	.L140
	.p2align 4,,10
	.p2align 3
.L154:
	testq	%r15, %r15
	jne	.L140
.L141:
	movq	%r12, %r15
.L140:
	addq	$40, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L143:
	.cfi_restore_state
	movq	32(%r12), %rcx
	leaq	16(%rcx), %rax
	cmpq	32(%rcx), %r13
	je	.L144
.L146:
	addq	$8, %rax
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L144:
	testq	%r15, %r15
	je	.L175
.L147:
	movzbl	23(%r15), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L148
	movq	48(%r15), %rdi
	cmpq	%rdi, %r14
	je	.L151
	leaq	48(%r15), %rax
	movq	%r15, %rsi
.L150:
	subq	$72, %rsi
	testq	%rdi, %rdi
	je	.L152
	movq	%rdx, -80(%rbp)
	movq	%rax, -72(%rbp)
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-80(%rbp), %rdx
	movq	-72(%rbp), %rax
	movq	-64(%rbp), %rsi
.L152:
	movq	%r14, (%rax)
	testq	%r14, %r14
	je	.L151
	movq	%r14, %rdi
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movq	-64(%rbp), %rdx
.L151:
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L145
	movq	32(%r12), %rax
	addq	$16, %rax
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L156:
	movq	32(%r15), %rsi
	movq	24(%rsi), %rdi
	cmpq	%rdi, %r13
	je	.L140
	leaq	24(%rsi), %rbx
	jmp	.L158
	.p2align 4,,10
	.p2align 3
.L174:
	movq	-56(%rbp), %rax
	movq	%r12, %rsi
	movq	200(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler5Graph9CloneNodeEPKNS1_4NodeE@PLT
	movq	%rax, %r15
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L148:
	movq	32(%r15), %rsi
	movq	32(%rsi), %rdi
	cmpq	%rdi, %r14
	je	.L151
	leaq	32(%rsi), %rax
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L175:
	movq	-56(%rbp), %rax
	movq	%r12, %rsi
	movq	%rdx, -64(%rbp)
	movq	200(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler5Graph9CloneNodeEPKNS1_4NodeE@PLT
	movq	-64(%rbp), %rdx
	movq	%rax, %r15
	jmp	.L147
	.cfi_endproc
.LFE20601:
	.size	_ZN2v88internal8compiler19JSInliningHeuristic28DuplicateFrameStateAndRenameEPNS1_4NodeES4_S4_NS2_14StateCloneModeE, .-_ZN2v88internal8compiler19JSInliningHeuristic28DuplicateFrameStateAndRenameEPNS1_4NodeES4_S4_NS2_14StateCloneModeE
	.section	.text._ZN2v88internal8compiler19JSInliningHeuristic16TryReuseDispatchEPNS1_4NodeES4_PS4_S5_S5_i,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19JSInliningHeuristic16TryReuseDispatchEPNS1_4NodeES4_PS4_S5_S5_i
	.type	_ZN2v88internal8compiler19JSInliningHeuristic16TryReuseDispatchEPNS1_4NodeES4_PS4_S5_S5_i, @function
_ZN2v88internal8compiler19JSInliningHeuristic16TryReuseDispatchEPNS1_4NodeES4_PS4_S5_S5_i:
.LFB20602:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$360, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r8, -248(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	cmpw	$35, 16(%rax)
	je	.L177
.L179:
	xorl	%r9d, %r9d
.L176:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L331
	addq	$360, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L177:
	.cfi_restore_state
	movl	20(%rax), %eax
	movq	%rsi, %r12
	movq	%rdi, %r13
	xorl	%esi, %esi
	movq	%rdx, %rdi
	movq	%rdx, %rbx
	movq	%rcx, %r15
	movq	%r9, %r14
	movl	%eax, -260(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -256(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	cmpq	%rax, -256(%rbp)
	jne	.L179
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	xorl	%r10d, %r10d
	movq	%rax, %r11
	movq	(%rax), %rax
	movzwl	16(%rax), %eax
	cmpw	$38, %ax
	je	.L332
.L180:
	cmpw	$36, %ax
	jne	.L179
	xorl	%esi, %esi
	movq	%r11, %rdi
	movq	%r10, -280(%rbp)
	movq	%r11, -272(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	-256(%rbp), %rsi
	cmpq	%rax, %rsi
	jne	.L179
	movq	24(%rsi), %rax
	movq	-272(%rbp), %r11
	movq	-280(%rbp), %r10
	testq	%rax, %rax
	je	.L181
	.p2align 4,,10
	.p2align 3
.L185:
	movl	16(%rax), %ecx
	movl	%ecx, %edx
	shrl	%edx
	andl	$1, %ecx
	leaq	3(%rdx,%rdx,2), %rdx
	leaq	(%rax,%rdx,8), %rdx
	jne	.L182
	movq	(%rdx), %rdx
.L182:
	cmpq	%rdx, %r11
	je	.L183
	cmpq	%rdx, %rbx
	je	.L183
	cmpq	%rdx, %r12
	je	.L183
	cmpq	%rdx, %r10
	jne	.L179
.L183:
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L185
.L181:
	movq	24(%r11), %rax
	testq	%rax, %rax
	je	.L186
	.p2align 4,,10
	.p2align 3
.L189:
	movl	16(%rax), %ecx
	movl	%ecx, %edx
	shrl	%edx
	andl	$1, %ecx
	leaq	3(%rdx,%rdx,2), %rdx
	leaq	(%rax,%rdx,8), %rdx
	jne	.L187
	movq	(%rdx), %rdx
.L187:
	cmpq	%rdx, %r12
	je	.L243
	cmpq	%rdx, %r10
	jne	.L179
.L243:
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L189
.L186:
	movq	$0, -232(%rbp)
	testq	%r10, %r10
	je	.L242
	movq	32(%r10), %rax
	movq	%rax, -312(%rbp)
	movzbl	23(%r10), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L191
	movq	-312(%rbp), %rax
	movq	16(%rax), %rax
	movq	%rax, -312(%rbp)
.L191:
	movq	-312(%rbp), %rsi
	leaq	-232(%rbp), %rcx
	movq	%rbx, %rdi
	leaq	-192(%rbp), %rdx
	movq	%r10, -296(%rbp)
	movq	%r11, -288(%rbp)
	movq	%rcx, -280(%rbp)
	movq	%rdx, -272(%rbp)
	call	_ZN2v88internal8compiler12_GLOBAL__N_127CollectFrameStateUniqueUsesEPNS1_4NodeES4_PNS2_12NodeAndIndexEPmm.constprop.0
	movq	-272(%rbp), %rdx
	movq	-280(%rbp), %rcx
	testb	%al, %al
	movq	-288(%rbp), %r11
	movq	-296(%rbp), %r10
	je	.L179
.L190:
	movq	%r12, %rdi
	movq	%rdx, -280(%rbp)
	movq	%rcx, -272(%rbp)
	movq	%r10, -296(%rbp)
	movq	%r11, -288(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties18GetFrameStateInputEPNS1_4NodeE@PLT
	movq	-272(%rbp), %rcx
	movq	-280(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	movq	%rax, -328(%rbp)
	call	_ZN2v88internal8compiler12_GLOBAL__N_127CollectFrameStateUniqueUsesEPNS1_4NodeES4_PNS2_12NodeAndIndexEPmm.constprop.0
	movl	%eax, %r9d
	testb	%al, %al
	je	.L179
	movq	24(%rbx), %r8
	movq	-288(%rbp), %r11
	movq	-296(%rbp), %r10
	testq	%r8, %r8
	je	.L192
	movq	-192(%rbp), %rdi
	movq	(%r8), %rcx
	movq	-232(%rbp), %rax
	movq	%rdi, -272(%rbp)
	movl	-184(%rbp), %edi
	movl	%edi, -352(%rbp)
	movq	-176(%rbp), %rdi
	movq	%rdi, -288(%rbp)
	movl	-168(%rbp), %edi
	movl	%edi, -376(%rbp)
	movq	-160(%rbp), %rdi
	movq	%rdi, -280(%rbp)
	movl	-152(%rbp), %edi
	movl	%edi, -368(%rbp)
	movq	-144(%rbp), %rdi
	movq	%rdi, -296(%rbp)
	movl	-136(%rbp), %edi
	movl	%edi, -264(%rbp)
	movq	-128(%rbp), %rdi
	movq	%rdi, -304(%rbp)
	movl	-120(%rbp), %edi
	movl	%edi, -360(%rbp)
	movq	-112(%rbp), %rdi
	movq	%rdi, -320(%rbp)
	movl	-104(%rbp), %edi
	movl	%edi, -396(%rbp)
	movq	-96(%rbp), %rdi
	movq	%rdi, -344(%rbp)
	movl	-88(%rbp), %edi
	movl	%edi, -392(%rbp)
	movq	-80(%rbp), %rdi
	movq	%rdi, -336(%rbp)
	movl	-72(%rbp), %edi
	movl	%edi, -384(%rbp)
	.p2align 4,,10
	.p2align 3
.L203:
	movl	16(%r8), %esi
	movl	%esi, %edx
	shrl	%edx
	andl	$1, %esi
	movl	%edx, %edi
	leaq	3(%rdi,%rdi,2), %rdi
	leaq	(%r8,%rdi,8), %rdi
	jne	.L193
	movq	(%rdi), %rdi
.L193:
	testl	%edx, %edx
	jne	.L244
	cmpq	%rdi, %r12
	je	.L194
.L244:
	testq	%rax, %rax
	je	.L179
	cmpq	-272(%rbp), %rdi
	je	.L333
.L196:
	cmpq	$1, %rax
	jbe	.L179
	cmpq	-288(%rbp), %rdi
	je	.L334
.L197:
	cmpq	$2, %rax
	je	.L179
	cmpq	-280(%rbp), %rdi
	je	.L335
.L198:
	cmpq	$3, %rax
	je	.L179
	cmpq	-296(%rbp), %rdi
	je	.L336
.L199:
	cmpq	$4, %rax
	je	.L179
	cmpq	-304(%rbp), %rdi
	je	.L337
.L200:
	cmpq	$5, %rax
	je	.L179
	cmpq	-320(%rbp), %rdi
	je	.L338
.L201:
	cmpq	$6, %rax
	je	.L179
	cmpq	-344(%rbp), %rdi
	je	.L339
.L202:
	cmpq	$7, %rax
	je	.L179
	cmpq	%rdi, -336(%rbp)
	jne	.L179
	cmpl	%edx, -384(%rbp)
	jne	.L179
.L194:
	testq	%rcx, %rcx
	je	.L192
	movq	%rcx, %r8
	movq	(%rcx), %rcx
	jmp	.L203
	.p2align 4,,10
	.p2align 3
.L332:
	xorl	%esi, %esi
	movq	%r11, %rdi
	movq	%r11, -272(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	cmpq	%rax, -256(%rbp)
	movq	-272(%rbp), %r11
	jne	.L179
	movq	%r11, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	-272(%rbp), %r11
	movq	%rax, %rdx
	movq	(%rax), %rax
	movq	%r11, %r10
	movq	%rdx, %r11
	movzwl	16(%rax), %eax
	jmp	.L180
.L192:
	leaq	32(%r11), %rax
	movq	%rax, -296(%rbp)
	leaq	32(%rbx), %rax
	movq	%rax, -304(%rbp)
	movl	-260(%rbp), %eax
	testl	%eax, %eax
	jle	.L214
	movslq	16(%rbp), %rax
	xorl	%esi, %esi
	movq	%r12, -344(%rbp)
	movq	%r15, -368(%rbp)
	movq	%rsi, %r12
	salq	$3, %rax
	movq	%r11, -336(%rbp)
	leaq	-24(%r14,%rax), %rdi
	leaq	-8(%r14,%rax), %rax
	movq	%r10, -288(%rbp)
	movq	%rax, -360(%rbp)
	movl	-260(%rbp), %eax
	movq	%rdi, -352(%rbp)
	xorl	%edi, %edi
	subl	$1, %eax
	movb	%r9b, -396(%rbp)
	movl	%edi, %r15d
	movl	%eax, -264(%rbp)
	movq	-256(%rbp), %rax
	movq	%r14, -376(%rbp)
	addq	$32, %rax
	movq	%rax, -384(%rbp)
	leaq	-224(%rbp), %rax
	movq	%rax, -392(%rbp)
	jmp	.L215
.L341:
	addq	%r12, %rax
.L208:
	movq	(%rax), %r14
	movq	-336(%rbp), %rax
	movzbl	23(%rax), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	movq	-296(%rbp), %rax
	je	.L209
	addq	%r12, %rax
.L210:
	movq	(%rax), %rax
	movq	%rax, -272(%rbp)
	movq	-256(%rbp), %rax
	movzbl	23(%rax), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	movq	-384(%rbp), %rax
	je	.L211
	addq	%r12, %rax
.L212:
	xorl	%r11d, %r11d
	movq	(%rax), %rax
	cmpl	-264(%rbp), %r15d
	sete	%r11b
	cmpq	$0, -288(%rbp)
	movq	%rax, -280(%rbp)
	je	.L213
	movq	-312(%rbp), %rsi
	movl	%r11d, %r8d
	movq	%r14, %rcx
	movq	%rbx, %rdx
	movq	%r13, %rdi
	movl	%r11d, -320(%rbp)
	call	_ZN2v88internal8compiler19JSInliningHeuristic28DuplicateFrameStateAndRenameEPNS1_4NodeES4_S4_NS2_14StateCloneModeE
	movq	200(%r13), %rdx
	movq	-288(%rbp), %rsi
	xorl	%r8d, %r8d
	movq	%rax, %xmm0
	movq	-392(%rbp), %rcx
	movq	-280(%rbp), %rax
	movq	(%rdx), %rdi
	movq	(%rsi), %rsi
	movhps	-272(%rbp), %xmm0
	movl	$3, %edx
	movq	%rax, -208(%rbp)
	movaps	%xmm0, -224(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	-320(%rbp), %r11d
	movq	%rax, -272(%rbp)
.L213:
	movq	-328(%rbp), %rsi
	movl	%r11d, %r8d
	movq	%r14, %rcx
	movq	%rbx, %rdx
	movq	%r13, %rdi
	addl	$1, %r15d
	call	_ZN2v88internal8compiler19JSInliningHeuristic28DuplicateFrameStateAndRenameEPNS1_4NodeES4_S4_NS2_14StateCloneModeE
	movq	-376(%rbp), %rcx
	movl	16(%rbp), %edx
	xorl	%r8d, %r8d
	movq	%rax, %xmm0
	movq	-352(%rbp), %rax
	movq	-280(%rbp), %rdi
	movhps	-272(%rbp), %xmm0
	movq	%r14, (%rcx)
	movups	%xmm0, (%rax)
	movq	-360(%rbp), %rax
	movq	%rdi, (%rax)
	movq	-344(%rbp), %rdi
	movq	200(%r13), %rax
	movq	(%rdi), %rsi
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-368(%rbp), %rdi
	movq	%rax, (%rdi,%r12)
	movq	-248(%rbp), %rdi
	movq	%rax, (%rdi,%r12)
	addq	$8, %r12
	cmpl	%r15d, -260(%rbp)
	je	.L340
.L215:
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	movq	-304(%rbp), %rax
	jne	.L341
	movq	(%rax), %rax
	leaq	16(%rax,%r12), %rax
	jmp	.L208
.L333:
	cmpl	%edx, -352(%rbp)
	jne	.L196
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L334:
	cmpl	-376(%rbp), %edx
	jne	.L197
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L335:
	cmpl	-368(%rbp), %edx
	jne	.L198
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L211:
	movq	(%rax), %rax
	leaq	16(%rax,%r12), %rax
	jmp	.L212
.L209:
	movq	(%rax), %rax
	leaq	16(%rax,%r12), %rax
	jmp	.L210
.L336:
	cmpl	-264(%rbp), %edx
	jne	.L199
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L337:
	cmpl	-360(%rbp), %edx
	jne	.L200
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L338:
	cmpl	-396(%rbp), %edx
	jne	.L201
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L339:
	cmpl	-392(%rbp), %edx
	jne	.L202
	jmp	.L194
.L340:
	movq	-336(%rbp), %r11
	movq	-288(%rbp), %r10
	movzbl	-396(%rbp), %r9d
	movq	-344(%rbp), %r12
.L214:
	movq	200(%r13), %rdi
	movq	352(%rdi), %r14
	testq	%r14, %r14
	je	.L342
.L205:
	movl	16(%rbp), %eax
	movzbl	23(%r12), %edx
	leaq	32(%r12), %rcx
	subl	$1, %eax
	andl	$15, %edx
	cltq
	salq	$3, %rax
	cmpl	$15, %edx
	je	.L216
	leaq	(%rcx,%rax), %r15
	movq	(%r15), %rdi
	cmpq	%rdi, %r14
	je	.L218
.L217:
	movl	16(%rbp), %eax
	negl	%eax
	cltq
	imulq	$24, %rax, %rax
	addq	%rax, %r12
	testq	%rdi, %rdi
	je	.L220
	movq	%r12, %rsi
	movb	%r9b, -280(%rbp)
	movq	%r10, -272(%rbp)
	movq	%r11, -248(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movzbl	-280(%rbp), %r9d
	movq	-272(%rbp), %r10
	movq	-248(%rbp), %r11
.L220:
	movq	%r14, (%r15)
	testq	%r14, %r14
	je	.L218
	movq	%r12, %rsi
	movq	%r14, %rdi
	movb	%r9b, -280(%rbp)
	movq	%r10, -272(%rbp)
	movq	%r11, -248(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movzbl	-280(%rbp), %r9d
	movq	-272(%rbp), %r10
	movq	-248(%rbp), %r11
.L218:
	movq	200(%r13), %rdi
	movq	352(%rdi), %r15
	testq	%r15, %r15
	je	.L343
.L221:
	movzbl	23(%rbx), %eax
	movslq	-260(%rbp), %r12
	andl	$15, %eax
	salq	$3, %r12
	cmpl	$15, %eax
	movq	-304(%rbp), %rax
	je	.L222
	addq	%r12, %rax
	movq	(%rax), %rdi
	cmpq	%rdi, %r15
	je	.L224
.L223:
	movl	-260(%rbp), %r14d
	notl	%r14d
	movslq	%r14d, %r14
	imulq	$24, %r14, %r14
	addq	%rbx, %r14
	testq	%rdi, %rdi
	je	.L226
	movq	%r14, %rsi
	movb	%r9b, -288(%rbp)
	movq	%r10, -280(%rbp)
	movq	%rax, -272(%rbp)
	movq	%r11, -248(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movzbl	-288(%rbp), %r9d
	movq	-280(%rbp), %r10
	movq	-272(%rbp), %rax
	movq	-248(%rbp), %r11
.L226:
	movq	%r15, (%rax)
	testq	%r15, %r15
	je	.L224
	movq	%r14, %rsi
	movq	%r15, %rdi
	movb	%r9b, -280(%rbp)
	movq	%r10, -272(%rbp)
	movq	%r11, -248(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movzbl	-280(%rbp), %r9d
	movq	-272(%rbp), %r10
	movq	-248(%rbp), %r11
.L224:
	movq	200(%r13), %rdi
	movq	352(%rdi), %r15
	testq	%r15, %r15
	je	.L344
.L227:
	movzbl	23(%r11), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L228
	addq	-296(%rbp), %r12
	movq	(%r12), %rdi
	cmpq	%rdi, %r15
	je	.L230
.L229:
	movl	-260(%rbp), %r14d
	notl	%r14d
	movslq	%r14d, %r14
	imulq	$24, %r14, %r14
	addq	%r11, %r14
	testq	%rdi, %rdi
	je	.L232
	movq	%r14, %rsi
	movb	%r9b, -260(%rbp)
	movq	%r10, -248(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movzbl	-260(%rbp), %r9d
	movq	-248(%rbp), %r10
.L232:
	movq	%r15, (%r12)
	testq	%r15, %r15
	je	.L230
	movq	%r14, %rsi
	movq	%r15, %rdi
	movb	%r9b, -260(%rbp)
	movq	%r10, -248(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movzbl	-260(%rbp), %r9d
	movq	-248(%rbp), %r10
.L230:
	testq	%r10, %r10
	je	.L234
	movq	200(%r13), %rdi
	movq	352(%rdi), %r13
	testq	%r13, %r13
	je	.L345
.L235:
	movzbl	23(%r10), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L236
	movq	48(%r10), %rdi
	cmpq	%rdi, %r13
	je	.L234
	leaq	48(%r10), %rbx
.L238:
	leaq	-72(%r10), %r12
	testq	%rdi, %rdi
	je	.L239
	movq	%r12, %rsi
	movb	%r9b, -248(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movzbl	-248(%rbp), %r9d
.L239:
	movq	%r13, (%rbx)
	testq	%r13, %r13
	je	.L234
	movq	%r12, %rsi
	movq	%r13, %rdi
	movb	%r9b, -248(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movzbl	-248(%rbp), %r9d
.L234:
	movq	-256(%rbp), %rdi
	movb	%r9b, -248(%rbp)
	call	_ZN2v88internal8compiler4Node4KillEv@PLT
	movzbl	-248(%rbp), %r9d
	jmp	.L176
.L242:
	leaq	-232(%rbp), %rcx
	leaq	-192(%rbp), %rdx
	movq	$0, -312(%rbp)
	jmp	.L190
.L228:
	movq	-296(%rbp), %rax
	movq	(%rax), %r11
	leaq	16(%r11,%r12), %r12
	movq	(%r12), %rdi
	cmpq	%rdi, %r15
	jne	.L229
	jmp	.L230
.L331:
	call	__stack_chk_fail@PLT
.L222:
	movq	(%rax), %rbx
	leaq	16(%rbx,%r12), %rax
	movq	(%rax), %rdi
	cmpq	%rdi, %r15
	jne	.L223
	jmp	.L224
.L216:
	movq	32(%r12), %r12
	leaq	16(%r12,%rax), %r15
	movq	(%r15), %rdi
	cmpq	%rdi, %r14
	jne	.L217
	jmp	.L218
.L236:
	movq	32(%r10), %r10
	movq	32(%r10), %rdi
	cmpq	%rdi, %r13
	je	.L234
	leaq	32(%r10), %rbx
	jmp	.L238
.L343:
	movb	%r9b, -280(%rbp)
	movq	%r10, -272(%rbp)
	movq	%r11, -248(%rbp)
	call	_ZN2v88internal8compiler12MachineGraph4DeadEv.part.0
	movzbl	-280(%rbp), %r9d
	movq	-272(%rbp), %r10
	movq	-248(%rbp), %r11
	movq	%rax, %r15
	jmp	.L221
.L345:
	movb	%r9b, -260(%rbp)
	movq	%r10, -248(%rbp)
	call	_ZN2v88internal8compiler12MachineGraph4DeadEv.part.0
	movzbl	-260(%rbp), %r9d
	movq	-248(%rbp), %r10
	movq	%rax, %r13
	jmp	.L235
.L342:
	movb	%r9b, -280(%rbp)
	movq	%r10, -272(%rbp)
	movq	%r11, -248(%rbp)
	call	_ZN2v88internal8compiler12MachineGraph4DeadEv.part.0
	movzbl	-280(%rbp), %r9d
	movq	-272(%rbp), %r10
	movq	-248(%rbp), %r11
	movq	%rax, %r14
	jmp	.L205
.L344:
	movb	%r9b, -280(%rbp)
	movq	%r10, -272(%rbp)
	movq	%r11, -248(%rbp)
	call	_ZN2v88internal8compiler12MachineGraph4DeadEv.part.0
	movzbl	-280(%rbp), %r9d
	movq	-272(%rbp), %r10
	movq	-248(%rbp), %r11
	movq	%rax, %r15
	jmp	.L227
	.cfi_endproc
.LFE20602:
	.size	_ZN2v88internal8compiler19JSInliningHeuristic16TryReuseDispatchEPNS1_4NodeES4_PS4_S5_S5_i, .-_ZN2v88internal8compiler19JSInliningHeuristic16TryReuseDispatchEPNS1_4NodeES4_PS4_S5_S5_i
	.section	.text._ZN2v88internal8compiler19JSInliningHeuristic21CreateOrReuseDispatchEPNS1_4NodeES4_RKNS2_9CandidateEPS4_S8_S8_i,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19JSInliningHeuristic21CreateOrReuseDispatchEPNS1_4NodeES4_RKNS2_9CandidateEPS4_S8_S8_i
	.type	_ZN2v88internal8compiler19JSInliningHeuristic21CreateOrReuseDispatchEPNS1_4NodeES4_RKNS2_9CandidateEPS4_S8_S8_i, @function
_ZN2v88internal8compiler19JSInliningHeuristic21CreateOrReuseDispatchEPNS1_4NodeES4_RKNS2_9CandidateEPS4_S8_S8_i:
.LFB20603:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$152, %rsp
	movq	%r9, -112(%rbp)
	movq	16(%rbp), %r14
	movq	%rdx, -144(%rbp)
	movq	192(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal8compiler19SourcePositionTable17GetSourcePositionEPNS1_4NodeE@PLT
	movq	192(%rbx), %rdx
	movq	%rdx, -184(%rbp)
	movq	16(%rdx), %rdx
	movq	%rdx, -88(%rbp)
	testb	$1, %al
	jne	.L347
	movq	%rax, %rcx
	movq	%rax, %rdx
	shrq	$31, %rcx
	shrq	%rdx
	movzwl	%cx, %ecx
	andl	$1073741823, %edx
	orl	%edx, %ecx
	jne	.L347
.L348:
	movl	24(%rbp), %eax
	subq	$8, %rsp
	movq	-112(%rbp), %r8
	movq	%r12, %rcx
	movq	-144(%rbp), %rdx
	movq	%r14, %r9
	movq	%r13, %rsi
	movq	%rbx, %rdi
	pushq	%rax
	call	_ZN2v88internal8compiler19JSInliningHeuristic16TryReuseDispatchEPNS1_4NodeES4_PS4_S5_S5_i
	popq	%rdx
	popq	%rcx
	testb	%al, %al
	je	.L349
.L356:
	movq	-88(%rbp), %rax
	movq	-184(%rbp), %rbx
	movq	%rax, 16(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L363
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L347:
	.cfi_restore_state
	movq	-184(%rbp), %rsi
	movq	%rax, 16(%rsi)
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L349:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, -120(%rbp)
	movl	224(%r15), %eax
	movl	%eax, -132(%rbp)
	testl	%eax, %eax
	jle	.L356
	movl	%eax, %edx
	movslq	24(%rbp), %rax
	movq	%r13, -104(%rbp)
	leal	-1(%rdx), %esi
	movq	%r14, -128(%rbp)
	leaq	-8(%r14,%rax,8), %rax
	movl	%esi, -136(%rbp)
	movq	%rax, -152(%rbp)
	leaq	8(%r15), %rax
	xorl	%r15d, %r15d
	movq	%rax, %r13
	movq	%r15, %r14
	jmp	.L357
	.p2align 4,,10
	.p2align 3
.L366:
	movq	%rax, -160(%rbp)
	movq	200(%rbx), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14ReferenceEqualEv@PLT
	leaq	-80(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movq	-160(%rbp), %r10
	movq	%rax, %rsi
	movq	-144(%rbp), %xmm0
	movl	$2, %edx
	movq	%rcx, -168(%rbp)
	movq	%r10, %xmm1
	movq	%r10, -176(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%rax, -160(%rbp)
	movq	200(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	xorl	%r8d, %r8d
	movl	$2, %edx
	movq	%r15, %rdi
	movq	-160(%rbp), %xmm0
	movq	-168(%rbp), %rcx
	movq	%rax, %rsi
	movhps	-120(%rbp), %xmm0
	movq	%rcx, -160(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r15
	movq	200(%rbx), %rax
	movq	(%rax), %r11
	movq	8(%rax), %rdi
	movq	%r11, -120(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	movq	-120(%rbp), %r11
	xorl	%r8d, %r8d
	movq	%r15, -80(%rbp)
	movq	-160(%rbp), %rcx
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r11, %rdi
	movq	%rcx, -168(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -120(%rbp)
	movq	200(%rbx), %rax
	movq	(%rax), %r11
	movq	8(%rax), %rdi
	movq	%r11, -160(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	movq	-160(%rbp), %r11
	xorl	%r8d, %r8d
	movq	-168(%rbp), %rcx
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r15, -80(%rbp)
	movq	%r11, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-176(%rbp), %r10
	movq	%rax, (%r12,%r14,8)
	movq	-104(%rbp), %rax
	movq	(%rax), %rax
	cmpw	$760, 16(%rax)
	je	.L364
.L355:
	movq	-128(%rbp), %rcx
	movq	-104(%rbp), %rsi
	xorl	%r8d, %r8d
	addq	$24, %r13
	movq	-152(%rbp), %rdx
	movq	%r10, (%rcx)
	movq	(%r12,%r14,8), %rax
	movq	%rax, (%rdx)
	movq	200(%rbx), %rax
	movl	24(%rbp), %edx
	movq	(%rsi), %rsi
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-112(%rbp), %rdx
	movq	%rax, (%r12,%r14,8)
	movq	%rax, (%rdx,%r14,8)
	addq	$1, %r14
	cmpl	%r14d, -132(%rbp)
	jle	.L356
.L357:
	cmpb	$0, -8(%r13)
	movq	200(%rbx), %rdi
	je	.L365
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	%rax, %r10
	cmpl	%r14d, -136(%rbp)
	jne	.L366
	movq	-120(%rbp), %rax
	movq	%rax, (%r12,%r14,8)
	movq	-104(%rbp), %rax
	movq	(%rax), %rax
	cmpw	$760, 16(%rax)
	jne	.L355
.L364:
	movq	-128(%rbp), %rax
	movq	8(%rax), %rsi
	cmpq	%rsi, (%rax)
	jne	.L355
	movq	%r10, 8(%rax)
	jmp	.L355
	.p2align 4,,10
	.p2align 3
.L365:
	leaq	.LC4(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L363:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20603:
	.size	_ZN2v88internal8compiler19JSInliningHeuristic21CreateOrReuseDispatchEPNS1_4NodeES4_RKNS2_9CandidateEPS4_S8_S8_i, .-_ZN2v88internal8compiler19JSInliningHeuristic21CreateOrReuseDispatchEPNS1_4NodeES4_RKNS2_9CandidateEPS4_S8_S8_i
	.section	.text._ZN2v88internal8compiler19JSInliningHeuristic15InlineCandidateERKNS2_9CandidateEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19JSInliningHeuristic15InlineCandidateERKNS2_9CandidateEb
	.type	_ZN2v88internal8compiler19JSInliningHeuristic15InlineCandidateERKNS2_9CandidateEb, @function
_ZN2v88internal8compiler19JSInliningHeuristic15InlineCandidateERKNS2_9CandidateEb:
.LFB20604:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$232, %rsp
	movl	%edx, -252(%rbp)
	movl	224(%rsi), %r12d
	movq	232(%rsi), %r10
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$1, %r12d
	je	.L410
	movq	%r10, %rdi
	xorl	%esi, %esi
	movq	%r10, -216(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	-216(%rbp), %r10
	movq	%rax, %rdx
	movzbl	23(%r10), %r14d
	andl	$15, %r14d
	cmpl	$15, %r14d
	je	.L411
.L373:
	movq	200(%rbx), %rax
	movslq	%r14d, %rsi
	salq	$3, %rsi
	movq	(%rax), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	%rcx, %rsi
	ja	.L412
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L375:
	testl	%r14d, %r14d
	jle	.L380
	leal	-1(%r14), %ecx
	leaq	32(%r10), %r9
	leaq	8(,%rcx,8), %r8
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L382:
	movzbl	23(%r10), %esi
	leaq	(%rax,%rcx), %rdi
	andl	$15, %esi
	cmpl	$15, %esi
	je	.L379
	movq	(%r9,%rcx), %rsi
	addq	$8, %rcx
	movq	%rsi, (%rdi)
	cmpq	%r8, %rcx
	jne	.L382
.L380:
	pushq	%r14
	leaq	-176(%rbp), %rcx
	movq	%r10, %rsi
	movq	%rbx, %rdi
	pushq	%rax
	leaq	-144(%rbp), %r9
	movq	%rcx, %r8
	movslq	%r12d, %r14
	movq	%rcx, -224(%rbp)
	movq	%r13, %rcx
	movq	%r9, -216(%rbp)
	movq	%r10, -232(%rbp)
	call	_ZN2v88internal8compiler19JSInliningHeuristic21CreateOrReuseDispatchEPNS1_4NodeES4_RKNS2_9CandidateEPS4_S8_S8_i
	movq	-232(%rbp), %r10
	leaq	-200(%rbp), %rsi
	movq	$0, -200(%rbp)
	movq	%r10, %rdi
	movq	%r10, -240(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties17IsExceptionalCallEPNS1_4NodeEPS4_@PLT
	leal	1(%r12), %edx
	movq	-240(%rbp), %r10
	testb	%al, %al
	movl	%edx, -232(%rbp)
	popq	%rdx
	popq	%rcx
	je	.L378
	testl	%r12d, %r12d
	jle	.L413
	leaq	-96(%rbp), %rax
	movl	%r12d, -248(%rbp)
	xorl	%r14d, %r14d
	leaq	-192(%rbp), %r15
	movq	%rax, -240(%rbp)
	movq	%r10, -264(%rbp)
	movq	%r13, -272(%rbp)
	movq	%rbx, %r13
	movq	%r14, %rbx
.L383:
	movq	200(%r13), %rax
	movq	-216(%rbp), %rdx
	movq	8(%rax), %rdi
	movq	(%rax), %r12
	movq	(%rdx,%rbx,8), %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9IfSuccessEv@PLT
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movq	%r15, %rcx
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r14, -192(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-224(%rbp), %rcx
	movq	-216(%rbp), %rdx
	movq	%rax, (%rcx,%rbx,8)
	movq	200(%r13), %rax
	movq	(%rdx,%rbx,8), %r14
	movq	8(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder11IfExceptionEv@PLT
	movq	%r14, %xmm0
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	punpcklqdq	%xmm0, %xmm0
	movl	$2, %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	movaps	%xmm0, -192(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-240(%rbp), %rdx
	movq	%rax, (%rdx,%rbx,8)
	addq	$1, %rbx
	cmpl	%ebx, -248(%rbp)
	jg	.L383
	movq	%r13, %rbx
	movl	-248(%rbp), %r12d
	movq	-264(%rbp), %r10
	movq	-272(%rbp), %r13
.L384:
	movq	200(%rbx), %rax
	movl	%r12d, %esi
	movq	%r10, -272(%rbp)
	movq	8(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
	xorl	%r8d, %r8d
	movl	%r12d, %edx
	movq	%r14, %rdi
	movq	-240(%rbp), %rcx
	movq	%rax, %rsi
	movslq	%r12d, %r14
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	%r12d, %esi
	movq	%rax, -96(%rbp,%r14,8)
	movq	%rax, -264(%rbp)
	movq	200(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9EffectPhiEi@PLT
	movq	-240(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movl	-232(%rbp), %edx
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	%r12d, %edx
	movl	$8, %esi
	movq	%rax, -248(%rbp)
	movq	200(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	movq	-240(%rbp), %rcx
	movq	%r15, %rdi
	xorl	%r8d, %r8d
	movl	-232(%rbp), %edx
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%rbx), %rdi
	movq	-264(%rbp), %r9
	movq	-248(%rbp), %r11
	movq	-200(%rbp), %rsi
	movq	%rax, %rdx
	movq	(%rdi), %r15
	movq	%r9, %r8
	movq	%r11, %rcx
	call	*32(%r15)
	movq	-272(%rbp), %r10
.L378:
	movq	200(%rbx), %rax
	movl	%r12d, %esi
	movq	%r10, -240(%rbp)
	movq	8(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
	xorl	%r8d, %r8d
	movl	%r12d, %edx
	movq	%r15, %rdi
	movq	-224(%rbp), %rcx
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	%r12d, %esi
	movq	%rax, -144(%rbp,%r14,8)
	movq	%rax, %r15
	movq	200(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9EffectPhiEi@PLT
	movq	-216(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movl	-232(%rbp), %edx
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	%r12d, %edx
	movl	$8, %esi
	movq	%rax, -224(%rbp)
	movq	200(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	movq	-216(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movl	-232(%rbp), %edx
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%rbx), %rdi
	movq	-224(%rbp), %r9
	movq	%r15, %r8
	movq	%rax, -232(%rbp)
	movq	-240(%rbp), %r10
	movq	%rax, %rdx
	movq	(%rdi), %rax
	movq	%r9, %rcx
	movq	%r10, %rsi
	call	*32(%rax)
	testl	%r12d, %r12d
	jle	.L396
	cmpb	$0, -252(%rbp)
	jne	.L401
	leaq	24(%rbx), %rax
	movq	%r13, %r15
	xorl	%r14d, %r14d
	movq	%rbx, %r13
	movq	%rax, -224(%rbp)
	jmp	.L394
	.p2align 4,,10
	.p2align 3
.L414:
	cmpb	$0, 96(%r15,%r14)
	je	.L389
	cmpl	%eax, _ZN2v88internal41FLAG_max_inlined_bytecode_size_cumulativeE(%rip)
	jle	.L389
	movq	-216(%rbp), %rax
	movq	-224(%rbp), %rdi
	movq	(%rax,%r14,8), %rbx
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler9JSInliner12ReduceJSCallEPNS1_4NodeE@PLT
	testq	%rax, %rax
	je	.L389
	leaq	(%r14,%r14,2), %rax
	leaq	112(%r15,%rax,8), %rdi
	call	_ZNK2v88internal8compiler17FixedArrayBaseRef6lengthEv@PLT
	addl	%eax, 216(%r13)
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler4Node4KillEv@PLT
	.p2align 4,,10
	.p2align 3
.L389:
	addq	$1, %r14
	cmpl	%r14d, %r12d
	jle	.L396
.L394:
	movl	216(%r13), %eax
	cmpl	_ZN2v88internal39FLAG_max_inlined_bytecode_size_absoluteE(%rip), %eax
	jl	.L414
.L396:
	movq	-232(%rbp), %rax
.L371:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L415
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L379:
	.cfi_restore_state
	movq	(%r9), %rsi
	movq	16(%rsi,%rcx), %rsi
	addq	$8, %rcx
	movq	%rsi, (%rdi)
	cmpq	%r8, %rcx
	jne	.L382
	jmp	.L380
	.p2align 4,,10
	.p2align 3
.L411:
	movq	32(%r10), %rax
	movl	8(%rax), %r14d
	jmp	.L373
	.p2align 4,,10
	.p2align 3
.L401:
	leaq	24(%rbx), %rax
	xorl	%r15d, %r15d
	movq	%r13, %r14
	movq	%rax, -224(%rbp)
	jmp	.L386
	.p2align 4,,10
	.p2align 3
.L416:
	cmpb	$0, 96(%r14,%r15)
	je	.L397
	movq	-216(%rbp), %rax
	movq	-224(%rbp), %rdi
	movq	(%rax,%r15,8), %r13
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler9JSInliner12ReduceJSCallEPNS1_4NodeE@PLT
	testq	%rax, %rax
	je	.L397
	leaq	(%r15,%r15,2), %rax
	leaq	112(%r14,%rax,8), %rdi
	call	_ZNK2v88internal8compiler17FixedArrayBaseRef6lengthEv@PLT
	addl	%eax, 216(%rbx)
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node4KillEv@PLT
.L397:
	addq	$1, %r15
	cmpl	%r15d, %r12d
	jle	.L396
.L386:
	movl	_ZN2v88internal39FLAG_max_inlined_bytecode_size_absoluteE(%rip), %eax
	cmpl	%eax, 216(%rbx)
	jl	.L416
	jmp	.L396
	.p2align 4,,10
	.p2align 3
.L410:
	leaq	24(%rdi), %rdi
	movq	%r10, %rsi
	call	_ZN2v88internal8compiler9JSInliner12ReduceJSCallEPNS1_4NodeE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L369
	cmpb	$0, 104(%r13)
	je	.L417
	leaq	112(%r13), %rdi
	call	_ZNK2v88internal8compiler17FixedArrayBaseRef6lengthEv@PLT
	addl	%eax, 216(%rbx)
.L369:
	movq	%r14, %rax
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L413:
	leaq	-96(%rbp), %rax
	movq	%rax, -240(%rbp)
	jmp	.L384
	.p2align 4,,10
	.p2align 3
.L412:
	movq	%rdx, -224(%rbp)
	movq	%r10, -216(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-216(%rbp), %r10
	movq	-224(%rbp), %rdx
	jmp	.L375
.L417:
	leaq	.LC4(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L415:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20604:
	.size	_ZN2v88internal8compiler19JSInliningHeuristic15InlineCandidateERKNS2_9CandidateEb, .-_ZN2v88internal8compiler19JSInliningHeuristic15InlineCandidateERKNS2_9CandidateEb
	.section	.text._ZNK2v88internal8compiler19JSInliningHeuristic16CandidateCompareclERKNS2_9CandidateES6_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler19JSInliningHeuristic16CandidateCompareclERKNS2_9CandidateES6_
	.type	_ZNK2v88internal8compiler19JSInliningHeuristic16CandidateCompareclERKNS2_9CandidateES6_, @function
_ZNK2v88internal8compiler19JSInliningHeuristic16CandidateCompareclERKNS2_9CandidateES6_:
.LFB20605:
	.cfi_startproc
	endbr64
	movss	240(%rdx), %xmm1
	movss	240(%rsi), %xmm0
	ucomiss	%xmm1, %xmm1
	jp	.L425
	ucomiss	%xmm0, %xmm0
	jp	.L422
	comiss	%xmm1, %xmm0
	movl	$1, %eax
	ja	.L418
	xorl	%eax, %eax
	comiss	%xmm0, %xmm1
	ja	.L418
	movq	232(%rdx), %rax
	movl	20(%rax), %edx
	movq	232(%rsi), %rax
	movl	20(%rax), %eax
	andl	$16777215, %edx
	andl	$16777215, %eax
	cmpl	%eax, %edx
	setb	%al
	ret
	.p2align 4,,10
	.p2align 3
.L422:
	xorl	%eax, %eax
.L418:
	ret
	.p2align 4,,10
	.p2align 3
.L425:
	ucomiss	%xmm0, %xmm0
	movl	$1, %eax
	jnp	.L418
	movq	232(%rsi), %rax
	movl	20(%rax), %ecx
	movq	232(%rdx), %rax
	movl	20(%rax), %eax
	andl	$16777215, %ecx
	andl	$16777215, %eax
	cmpl	%eax, %ecx
	seta	%al
	ret
	.cfi_endproc
.LFE20605:
	.size	_ZNK2v88internal8compiler19JSInliningHeuristic16CandidateCompareclERKNS2_9CandidateES6_, .-_ZNK2v88internal8compiler19JSInliningHeuristic16CandidateCompareclERKNS2_9CandidateES6_
	.section	.rodata._ZN2v88internal8compiler19JSInliningHeuristic15PrintCandidatesEv.str1.1,"aMS",@progbits,1
.LC5:
	.string	" candidate(s) for inlining:"
.LC6:
	.string	"- candidate: "
.LC7:
	.string	" node #"
.LC8:
	.string	" with frequency "
.LC9:
	.string	", "
.LC10:
	.string	" target(s):"
.LC11:
	.string	"  - target: "
.LC12:
	.string	", bytecode size: "
.LC13:
	.string	", no bytecode"
	.section	.text._ZN2v88internal8compiler19JSInliningHeuristic15PrintCandidatesEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19JSInliningHeuristic15PrintCandidatesEv
	.type	_ZN2v88internal8compiler19JSInliningHeuristic15PrintCandidatesEv, @function
_ZN2v88internal8compiler19JSInliningHeuristic15PrintCandidatesEv:
.LFB20606:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-400(%rbp), %rbx
	subq	$392, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-320(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -432(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	xorl	%edx, %edx
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdi
	movw	%dx, -96(%rbp)
	movq	stdout(%rip), %rdx
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	%rax, -320(%rbp)
	movq	$0, -104(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	movq	128(%r12), %rsi
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movq	%rbx, %rdi
	movq	%rax, -400(%rbp)
	addq	$40, %rax
	movq	%rax, -320(%rbp)
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$27, %edx
	leaq	.LC5(%rip), %rsi
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	0(%r13), %rax
	movq	-24(%rax), %rax
	movq	240(%r13,%rax), %r14
	testq	%r14, %r14
	je	.L433
	cmpb	$0, 56(%r14)
	je	.L428
	movsbl	67(%r14), %esi
.L429:
	movq	%r13, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movq	112(%r12), %r13
	leaq	96(%r12), %rax
	movq	%rax, -424(%rbp)
	cmpq	%rax, %r13
	je	.L437
.L430:
	movl	$13, %edx
	leaq	.LC6(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	264(%r13), %rax
	movq	(%rax), %rax
	movq	8(%rax), %r12
	testq	%r12, %r12
	je	.L462
	movq	%r12, %rdi
	call	strlen@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L432:
	movl	$7, %edx
	leaq	.LC7(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	264(%r13), %rax
	movq	%rbx, %rdi
	movl	20(%rax), %esi
	andl	$16777215, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$16, %edx
	leaq	.LC8(%rip), %rsi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rdi
	leaq	272(%r13), %rsi
	call	_ZN2v88internal8compilerlsERSoRKNS1_13CallFrequencyE@PLT
	movl	$2, %edx
	leaq	.LC9(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	256(%r13), %esi
	movq	%r12, %rdi
	call	_ZNSolsEi@PLT
	movl	$11, %edx
	leaq	.LC10(%rip), %rsi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r12), %rax
	movq	-24(%rax), %rax
	movq	240(%r12,%rax), %r14
	testq	%r14, %r14
	je	.L433
	cmpb	$0, 56(%r14)
	je	.L434
	movsbl	67(%r14), %esi
.L435:
	movq	%r12, %rdi
	xorl	%r14d, %r14d
	leaq	144(%r13), %r12
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movl	256(%r13), %eax
	testl	%eax, %eax
	jg	.L446
	jmp	.L445
	.p2align 4,,10
	.p2align 3
.L465:
	leaq	.LC12(%rip), %rsi
	movl	$17, %edx
	movq	%rbx, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler17FixedArrayBaseRef6lengthEv@PLT
	movq	%rbx, %rdi
	movl	%eax, %esi
	call	_ZNSolsEi@PLT
.L442:
	movq	-400(%rbp), %rax
	movq	-24(%rax), %rax
	movq	-160(%rbp,%rax), %r15
	testq	%r15, %r15
	je	.L433
	cmpb	$0, 56(%r15)
	je	.L443
	movsbl	67(%r15), %esi
.L444:
	movq	%rbx, %rdi
	addl	$1, %r14d
	addq	$24, %r12
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	cmpl	%r14d, 256(%r13)
	jle	.L445
.L446:
	cmpb	$0, -112(%r12)
	jne	.L463
	cmpb	$0, 232(%r13)
	je	.L464
	movdqu	240(%r13), %xmm1
	movaps	%xmm1, -416(%rbp)
.L439:
	movl	$12, %edx
	leaq	.LC11(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	-416(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_9ObjectRefE@PLT
	cmpb	$0, -8(%r12)
	jne	.L465
	movl	$13, %edx
	leaq	.LC13(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L442
	.p2align 4,,10
	.p2align 3
.L443:
	movq	%r15, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%r15), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rcx
	movq	48(%rax), %rax
	cmpq	%rcx, %rax
	je	.L444
	movq	%r15, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L444
	.p2align 4,,10
	.p2align 3
.L463:
	leaq	-104(%r12), %rdi
	call	_ZNK2v88internal8compiler13JSFunctionRef6sharedEv@PLT
	movq	%rax, -416(%rbp)
	movq	%rdx, -408(%rbp)
	jmp	.L439
	.p2align 4,,10
	.p2align 3
.L464:
	leaq	.LC4(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L434:
	movq	%r14, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%r14), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rcx
	movq	48(%rax), %rax
	cmpq	%rcx, %rax
	je	.L435
	movq	%r14, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L435
	.p2align 4,,10
	.p2align 3
.L445:
	movq	%r13, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r13
	cmpq	%rax, -424(%rbp)
	jne	.L430
.L437:
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rcx
	movq	%rax, -320(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-336(%rbp), %rdi
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -400(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	-432(%rbp), %rdi
	movq	%rax, -400(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L466
	addq	$392, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L462:
	.cfi_restore_state
	movq	-400(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%rbx, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L432
	.p2align 4,,10
	.p2align 3
.L428:
	movq	%r14, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%r14), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L429
	movq	%r14, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L429
.L433:
	call	_ZSt16__throw_bad_castv@PLT
.L466:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20606:
	.size	_ZN2v88internal8compiler19JSInliningHeuristic15PrintCandidatesEv, .-_ZN2v88internal8compiler19JSInliningHeuristic15PrintCandidatesEv
	.section	.text._ZN2v88internal8compiler19JSInliningHeuristic8FinalizeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19JSInliningHeuristic8FinalizeEv
	.type	_ZN2v88internal8compiler19JSInliningHeuristic8FinalizeEv, @function
_ZN2v88internal8compiler19JSInliningHeuristic8FinalizeEv:
.LFB20597:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$264, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 128(%rdi)
	je	.L467
	cmpb	$0, _ZN2v88internal25FLAG_trace_turbo_inliningE(%rip)
	movq	%rdi, %r13
	jne	.L481
.L469:
	leaq	96(%r13), %rbx
	leaq	-288(%rbp), %r12
	jmp	.L470
	.p2align 4,,10
	.p2align 3
.L482:
	addq	$32, %rdx
.L472:
	testl	%eax, %eax
	jle	.L473
	cmpq	$0, (%rdx)
	je	.L477
.L473:
	pxor	%xmm0, %xmm0
	cvtsi2sdl	-44(%rbp), %xmm0
	mulsd	_ZN2v88internal39FLAG_reserve_inline_budget_scale_factorE(%rip), %xmm0
	cvttsd2sil	%xmm0, %eax
	addl	216(%r13), %eax
	cmpl	%eax, _ZN2v88internal41FLAG_max_inlined_bytecode_size_cumulativeE(%rip)
	jl	.L477
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19JSInliningHeuristic15InlineCandidateERKNS2_9CandidateEb
	testq	%rax, %rax
	jne	.L467
	movq	128(%r13), %rcx
.L477:
	testq	%rcx, %rcx
	je	.L467
.L470:
	movq	112(%r13), %rdi
	movq	%rbx, %rsi
	movdqu	32(%rdi), %xmm1
	movaps	%xmm1, -288(%rbp)
	movdqu	48(%rdi), %xmm2
	movaps	%xmm2, -272(%rbp)
	movdqu	64(%rdi), %xmm3
	movaps	%xmm3, -256(%rbp)
	movdqu	80(%rdi), %xmm4
	movaps	%xmm4, -240(%rbp)
	movdqu	96(%rdi), %xmm5
	movaps	%xmm5, -224(%rbp)
	movdqu	112(%rdi), %xmm6
	movaps	%xmm6, -208(%rbp)
	movdqu	128(%rdi), %xmm7
	movaps	%xmm7, -192(%rbp)
	movdqu	144(%rdi), %xmm1
	movaps	%xmm1, -176(%rbp)
	movdqu	160(%rdi), %xmm2
	movaps	%xmm2, -160(%rbp)
	movdqu	176(%rdi), %xmm3
	movaps	%xmm3, -144(%rbp)
	movdqu	192(%rdi), %xmm4
	movaps	%xmm4, -128(%rbp)
	movdqu	208(%rdi), %xmm5
	movaps	%xmm5, -112(%rbp)
	movdqu	224(%rdi), %xmm6
	movaps	%xmm6, -96(%rbp)
	movdqu	240(%rdi), %xmm7
	movaps	%xmm7, -80(%rbp)
	movdqu	256(%rdi), %xmm0
	movaps	%xmm0, -64(%rbp)
	movq	272(%rdi), %rax
	movq	%rax, -48(%rbp)
	call	_ZSt28_Rb_tree_rebalance_for_erasePSt18_Rb_tree_node_baseRS_@PLT
	movq	128(%r13), %rax
	movq	-56(%rbp), %rdx
	leaq	-1(%rax), %rcx
	movq	%rcx, 128(%r13)
	movzbl	23(%rdx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L482
	movq	32(%rdx), %rdx
	movl	8(%rdx), %eax
	addq	$16, %rdx
	jmp	.L472
	.p2align 4,,10
	.p2align 3
.L481:
	call	_ZN2v88internal8compiler19JSInliningHeuristic15PrintCandidatesEv
	cmpq	$0, 128(%r13)
	jne	.L469
	.p2align 4,,10
	.p2align 3
.L467:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L483
	addq	$264, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L483:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20597:
	.size	_ZN2v88internal8compiler19JSInliningHeuristic8FinalizeEv, .-_ZN2v88internal8compiler19JSInliningHeuristic8FinalizeEv
	.section	.text._ZNK2v88internal8compiler19JSInliningHeuristic5graphEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler19JSInliningHeuristic5graphEv
	.type	_ZNK2v88internal8compiler19JSInliningHeuristic5graphEv, @function
_ZNK2v88internal8compiler19JSInliningHeuristic5graphEv:
.LFB20607:
	.cfi_startproc
	endbr64
	movq	200(%rdi), %rax
	movq	(%rax), %rax
	ret
	.cfi_endproc
.LFE20607:
	.size	_ZNK2v88internal8compiler19JSInliningHeuristic5graphEv, .-_ZNK2v88internal8compiler19JSInliningHeuristic5graphEv
	.section	.text._ZNK2v88internal8compiler19JSInliningHeuristic6commonEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler19JSInliningHeuristic6commonEv
	.type	_ZNK2v88internal8compiler19JSInliningHeuristic6commonEv, @function
_ZNK2v88internal8compiler19JSInliningHeuristic6commonEv:
.LFB20608:
	.cfi_startproc
	endbr64
	movq	200(%rdi), %rax
	movq	8(%rax), %rax
	ret
	.cfi_endproc
.LFE20608:
	.size	_ZNK2v88internal8compiler19JSInliningHeuristic6commonEv, .-_ZNK2v88internal8compiler19JSInliningHeuristic6commonEv
	.section	.text._ZNK2v88internal8compiler19JSInliningHeuristic10simplifiedEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler19JSInliningHeuristic10simplifiedEv
	.type	_ZNK2v88internal8compiler19JSInliningHeuristic10simplifiedEv, @function
_ZNK2v88internal8compiler19JSInliningHeuristic10simplifiedEv:
.LFB20609:
	.cfi_startproc
	endbr64
	movq	200(%rdi), %rax
	movq	376(%rax), %rax
	ret
	.cfi_endproc
.LFE20609:
	.size	_ZNK2v88internal8compiler19JSInliningHeuristic10simplifiedEv, .-_ZNK2v88internal8compiler19JSInliningHeuristic10simplifiedEv
	.section	.text._ZNSt8_Rb_treeIN2v88internal8compiler19JSInliningHeuristic9CandidateES4_St9_IdentityIS4_ENS3_16CandidateCompareENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E,"axG",@progbits,_ZNSt8_Rb_treeIN2v88internal8compiler19JSInliningHeuristic9CandidateES4_St9_IdentityIS4_ENS3_16CandidateCompareENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN2v88internal8compiler19JSInliningHeuristic9CandidateES4_St9_IdentityIS4_ENS3_16CandidateCompareENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	.type	_ZNSt8_Rb_treeIN2v88internal8compiler19JSInliningHeuristic9CandidateES4_St9_IdentityIS4_ENS3_16CandidateCompareENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E, @function
_ZNSt8_Rb_treeIN2v88internal8compiler19JSInliningHeuristic9CandidateES4_St9_IdentityIS4_ENS3_16CandidateCompareENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E:
.LFB23016:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L495
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L489:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal8compiler19JSInliningHeuristic9CandidateES4_St9_IdentityIS4_ENS3_16CandidateCompareENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L489
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L495:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE23016:
	.size	_ZNSt8_Rb_treeIN2v88internal8compiler19JSInliningHeuristic9CandidateES4_St9_IdentityIS4_ENS3_16CandidateCompareENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E, .-_ZNSt8_Rb_treeIN2v88internal8compiler19JSInliningHeuristic9CandidateES4_St9_IdentityIS4_ENS3_16CandidateCompareENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	.section	.text._ZNSt8_Rb_treeIjjSt9_IdentityIjESt4lessIjEN2v88internal13ZoneAllocatorIjEEE8_M_eraseEPSt13_Rb_tree_nodeIjE,"axG",@progbits,_ZNSt8_Rb_treeIjjSt9_IdentityIjESt4lessIjEN2v88internal13ZoneAllocatorIjEEE8_M_eraseEPSt13_Rb_tree_nodeIjE,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIjjSt9_IdentityIjESt4lessIjEN2v88internal13ZoneAllocatorIjEEE8_M_eraseEPSt13_Rb_tree_nodeIjE
	.type	_ZNSt8_Rb_treeIjjSt9_IdentityIjESt4lessIjEN2v88internal13ZoneAllocatorIjEEE8_M_eraseEPSt13_Rb_tree_nodeIjE, @function
_ZNSt8_Rb_treeIjjSt9_IdentityIjESt4lessIjEN2v88internal13ZoneAllocatorIjEEE8_M_eraseEPSt13_Rb_tree_nodeIjE:
.LFB23024:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L506
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L500:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIjjSt9_IdentityIjESt4lessIjEN2v88internal13ZoneAllocatorIjEEE8_M_eraseEPSt13_Rb_tree_nodeIjE
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L500
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L506:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE23024:
	.size	_ZNSt8_Rb_treeIjjSt9_IdentityIjESt4lessIjEN2v88internal13ZoneAllocatorIjEEE8_M_eraseEPSt13_Rb_tree_nodeIjE, .-_ZNSt8_Rb_treeIjjSt9_IdentityIjESt4lessIjEN2v88internal13ZoneAllocatorIjEEE8_M_eraseEPSt13_Rb_tree_nodeIjE
	.section	.text._ZN2v88internal8compiler19JSInliningHeuristicD2Ev,"axG",@progbits,_ZN2v88internal8compiler19JSInliningHeuristicD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler19JSInliningHeuristicD2Ev
	.type	_ZN2v88internal8compiler19JSInliningHeuristicD2Ev, @function
_ZN2v88internal8compiler19JSInliningHeuristicD2Ev:
.LFB24865:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal8compiler19JSInliningHeuristicE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	160(%rdi), %r12
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L510
	leaq	136(%rdi), %r14
.L513:
	movq	24(%r12), %rbx
	testq	%rbx, %rbx
	je	.L511
.L512:
	movq	24(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeIjjSt9_IdentityIjESt4lessIjEN2v88internal13ZoneAllocatorIjEEE8_M_eraseEPSt13_Rb_tree_nodeIjE
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L512
.L511:
	movq	16(%r12), %r12
	testq	%r12, %r12
	jne	.L513
.L510:
	movq	104(%r13), %r12
	leaq	80(%r13), %r14
	testq	%r12, %r12
	je	.L509
.L517:
	movq	24(%r12), %rbx
	testq	%rbx, %rbx
	je	.L515
.L516:
	movq	24(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal8compiler19JSInliningHeuristic9CandidateES4_St9_IdentityIS4_ENS3_16CandidateCompareENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L516
.L515:
	movq	16(%r12), %r12
	testq	%r12, %r12
	jne	.L517
.L509:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24865:
	.size	_ZN2v88internal8compiler19JSInliningHeuristicD2Ev, .-_ZN2v88internal8compiler19JSInliningHeuristicD2Ev
	.weak	_ZN2v88internal8compiler19JSInliningHeuristicD1Ev
	.set	_ZN2v88internal8compiler19JSInliningHeuristicD1Ev,_ZN2v88internal8compiler19JSInliningHeuristicD2Ev
	.section	.text._ZN2v88internal8compiler19JSInliningHeuristicD0Ev,"axG",@progbits,_ZN2v88internal8compiler19JSInliningHeuristicD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler19JSInliningHeuristicD0Ev
	.type	_ZN2v88internal8compiler19JSInliningHeuristicD0Ev, @function
_ZN2v88internal8compiler19JSInliningHeuristicD0Ev:
.LFB24867:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal8compiler19JSInliningHeuristicE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	160(%rdi), %rbx
	movq	%rax, (%rdi)
	testq	%rbx, %rbx
	je	.L536
	leaq	136(%rdi), %r14
.L539:
	movq	24(%rbx), %r13
	testq	%r13, %r13
	je	.L537
.L538:
	movq	24(%r13), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeIjjSt9_IdentityIjESt4lessIjEN2v88internal13ZoneAllocatorIjEEE8_M_eraseEPSt13_Rb_tree_nodeIjE
	movq	16(%r13), %r13
	testq	%r13, %r13
	jne	.L538
.L537:
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L539
.L536:
	movq	104(%r12), %rbx
	leaq	80(%r12), %r14
	testq	%rbx, %rbx
	je	.L540
.L543:
	movq	24(%rbx), %r13
	testq	%r13, %r13
	je	.L541
.L542:
	movq	24(%r13), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal8compiler19JSInliningHeuristic9CandidateES4_St9_IdentityIS4_ENS3_16CandidateCompareENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	movq	16(%r13), %r13
	testq	%r13, %r13
	jne	.L542
.L541:
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L543
.L540:
	popq	%rbx
	movq	%r12, %rdi
	movl	$224, %esi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE24867:
	.size	_ZN2v88internal8compiler19JSInliningHeuristicD0Ev, .-_ZN2v88internal8compiler19JSInliningHeuristicD0Ev
	.section	.text._ZNSt8_Rb_treeIjjSt9_IdentityIjESt4lessIjEN2v88internal13ZoneAllocatorIjEEE16_M_insert_uniqueIjEESt4pairISt17_Rb_tree_iteratorIjEbEOT_,"axG",@progbits,_ZNSt8_Rb_treeIjjSt9_IdentityIjESt4lessIjEN2v88internal13ZoneAllocatorIjEEE16_M_insert_uniqueIjEESt4pairISt17_Rb_tree_iteratorIjEbEOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIjjSt9_IdentityIjESt4lessIjEN2v88internal13ZoneAllocatorIjEEE16_M_insert_uniqueIjEESt4pairISt17_Rb_tree_iteratorIjEbEOT_
	.type	_ZNSt8_Rb_treeIjjSt9_IdentityIjESt4lessIjEN2v88internal13ZoneAllocatorIjEEE16_M_insert_uniqueIjEESt4pairISt17_Rb_tree_iteratorIjEbEOT_, @function
_ZNSt8_Rb_treeIjjSt9_IdentityIjESt4lessIjEN2v88internal13ZoneAllocatorIjEEE16_M_insert_uniqueIjEESt4pairISt17_Rb_tree_iteratorIjEbEOT_:
.LFB23751:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	16(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	24(%rdi), %r12
	testq	%r12, %r12
	je	.L575
	movl	(%rsi), %esi
	jmp	.L563
	.p2align 4,,10
	.p2align 3
.L582:
	movq	16(%r12), %rax
	movl	$1, %edx
	testq	%rax, %rax
	je	.L564
.L583:
	movq	%rax, %r12
.L563:
	movl	32(%r12), %ecx
	cmpl	%ecx, %esi
	jb	.L582
	movq	24(%r12), %rax
	xorl	%edx, %edx
	testq	%rax, %rax
	jne	.L583
.L564:
	testb	%dl, %dl
	jne	.L562
	cmpl	%ecx, %esi
	jbe	.L568
.L573:
	movl	$1, %r8d
	cmpq	%r12, %r15
	jne	.L584
.L569:
	movq	(%rbx), %rdi
	movq	16(%rdi), %r13
	movq	24(%rdi), %rax
	subq	%r13, %rax
	cmpq	$39, %rax
	jbe	.L585
	leaq	40(%r13), %rax
	movq	%rax, 16(%rdi)
.L571:
	movl	(%r14), %eax
	movq	%r12, %rdx
	movq	%r15, %rcx
	movq	%r13, %rsi
	movl	%r8d, %edi
	movl	%eax, 32(%r13)
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 48(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	movl	$1, %edx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L575:
	.cfi_restore_state
	movq	%r15, %r12
.L562:
	cmpq	%r12, 32(%rbx)
	je	.L573
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movl	(%r14), %ecx
	cmpl	%ecx, 32(%rax)
	jb	.L573
	movq	%rax, %r12
.L568:
	addq	$24, %rsp
	movq	%r12, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L584:
	.cfi_restore_state
	xorl	%r8d, %r8d
	movl	32(%r12), %eax
	cmpl	%eax, (%r14)
	setb	%r8b
	jmp	.L569
	.p2align 4,,10
	.p2align 3
.L585:
	movl	$40, %esi
	movl	%r8d, -52(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-52(%rbp), %r8d
	movq	%rax, %r13
	jmp	.L571
	.cfi_endproc
.LFE23751:
	.size	_ZNSt8_Rb_treeIjjSt9_IdentityIjESt4lessIjEN2v88internal13ZoneAllocatorIjEEE16_M_insert_uniqueIjEESt4pairISt17_Rb_tree_iteratorIjEbEOT_, .-_ZNSt8_Rb_treeIjjSt9_IdentityIjESt4lessIjEN2v88internal13ZoneAllocatorIjEEE16_M_insert_uniqueIjEESt4pairISt17_Rb_tree_iteratorIjEbEOT_
	.section	.text._ZNSt8_Rb_treeIN2v88internal8compiler19JSInliningHeuristic9CandidateES4_St9_IdentityIS4_ENS3_16CandidateCompareENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_,"axG",@progbits,_ZNSt8_Rb_treeIN2v88internal8compiler19JSInliningHeuristic9CandidateES4_St9_IdentityIS4_ENS3_16CandidateCompareENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN2v88internal8compiler19JSInliningHeuristic9CandidateES4_St9_IdentityIS4_ENS3_16CandidateCompareENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_
	.type	_ZNSt8_Rb_treeIN2v88internal8compiler19JSInliningHeuristic9CandidateES4_St9_IdentityIS4_ENS3_16CandidateCompareENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_, @function
_ZNSt8_Rb_treeIN2v88internal8compiler19JSInliningHeuristic9CandidateES4_St9_IdentityIS4_ENS3_16CandidateCompareENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_:
.LFB23774:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	16(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	24(%rdi), %rax
	testq	%rax, %rax
	je	.L587
	movss	240(%rsi), %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.L594
	jmp	.L588
	.p2align 4,,10
	.p2align 3
.L632:
	comiss	%xmm1, %xmm0
	ja	.L592
	movq	232(%r12), %rdx
	movl	20(%rdx), %ecx
	movq	264(%rax), %rdx
	movl	20(%rdx), %edx
	andl	$16777215, %ecx
	andl	$16777215, %edx
	cmpl	%edx, %ecx
	ja	.L589
.L592:
	movq	24(%rax), %rdx
	xorl	%ecx, %ecx
	testq	%rdx, %rdx
	je	.L593
.L633:
	movq	%rdx, %rax
.L594:
	movss	272(%rax), %xmm0
	ucomiss	%xmm0, %xmm0
	jp	.L589
	comiss	%xmm0, %xmm1
	jbe	.L632
.L589:
	movq	16(%rax), %rdx
	movl	$1, %ecx
	testq	%rdx, %rdx
	jne	.L633
.L593:
	movq	%rax, %r15
	testb	%cl, %cl
	jne	.L611
.L599:
	ucomiss	%xmm0, %xmm0
	jp	.L602
	comiss	%xmm1, %xmm0
	ja	.L600
	comiss	%xmm0, %xmm1
	ja	.L602
	movq	264(%rax), %rdx
	movl	20(%rdx), %ecx
	movq	232(%r12), %rdx
	movl	20(%rdx), %edx
	andl	$16777215, %ecx
	andl	$16777215, %edx
	cmpl	%edx, %ecx
	seta	%dl
.L601:
	testb	%dl, %dl
	jne	.L600
.L602:
	addq	$24, %rsp
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L635:
	.cfi_restore_state
	movq	%rdx, %rax
.L588:
	movss	272(%rax), %xmm0
	ucomiss	%xmm0, %xmm0
	jp	.L634
.L596:
	movq	24(%rax), %rdx
	xorl	%ecx, %ecx
.L597:
	testq	%rdx, %rdx
	jne	.L635
	testb	%cl, %cl
	jne	.L611
	ucomiss	%xmm0, %xmm0
	jp	.L622
	movq	%rax, %r15
	jmp	.L609
	.p2align 4,,10
	.p2align 3
.L634:
	movq	232(%r12), %rdx
	movl	20(%rdx), %ecx
	movq	264(%rax), %rdx
	movl	20(%rdx), %edx
	andl	$16777215, %ecx
	andl	$16777215, %edx
	cmpl	%edx, %ecx
	jbe	.L596
	movq	16(%rax), %rdx
	movl	$1, %ecx
	jmp	.L597
	.p2align 4,,10
	.p2align 3
.L611:
	movq	%rax, %r15
	cmpq	%rax, 32(%r13)
	je	.L609
.L610:
	movq	%r15, %rdi
	movss	%xmm1, -52(%rbp)
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movss	-52(%rbp), %xmm1
	movss	272(%rax), %xmm0
	ucomiss	%xmm1, %xmm1
	jnp	.L599
	ucomiss	%xmm0, %xmm0
	jnp	.L600
	movq	%rax, %rsi
	movq	%r15, %rax
	jmp	.L612
	.p2align 4,,10
	.p2align 3
.L600:
	testq	%r15, %r15
	je	.L636
.L609:
	movl	$1, %r8d
	cmpq	%r15, %r14
	jne	.L637
.L603:
	movq	0(%r13), %rdi
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	$279, %rax
	jbe	.L638
	leaq	280(%rbx), %rax
	movq	%rax, 16(%rdi)
.L607:
	movdqu	(%r12), %xmm2
	movq	%r15, %rdx
	movq	%r14, %rcx
	movq	%rbx, %rsi
	movl	%r8d, %edi
	movups	%xmm2, 32(%rbx)
	movdqu	16(%r12), %xmm3
	movups	%xmm3, 48(%rbx)
	movdqu	32(%r12), %xmm4
	movups	%xmm4, 64(%rbx)
	movdqu	48(%r12), %xmm5
	movups	%xmm5, 80(%rbx)
	movdqu	64(%r12), %xmm6
	movups	%xmm6, 96(%rbx)
	movdqu	80(%r12), %xmm7
	movups	%xmm7, 112(%rbx)
	movdqu	96(%r12), %xmm2
	movups	%xmm2, 128(%rbx)
	movdqu	112(%r12), %xmm3
	movups	%xmm3, 144(%rbx)
	movdqu	128(%r12), %xmm4
	movups	%xmm4, 160(%rbx)
	movdqu	144(%r12), %xmm5
	movups	%xmm5, 176(%rbx)
	movdqu	160(%r12), %xmm6
	movups	%xmm6, 192(%rbx)
	movdqu	176(%r12), %xmm7
	movups	%xmm7, 208(%rbx)
	movdqu	192(%r12), %xmm2
	movups	%xmm2, 224(%rbx)
	movdqu	208(%r12), %xmm3
	movups	%xmm3, 240(%rbx)
	movdqu	224(%r12), %xmm4
	movups	%xmm4, 256(%rbx)
	movq	240(%r12), %rax
	movq	%rax, 272(%rbx)
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 48(%r13)
	addq	$24, %rsp
	movq	%rbx, %rax
	movl	$1, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L637:
	.cfi_restore_state
	movss	272(%r15), %xmm1
	movss	240(%r12), %xmm0
	ucomiss	%xmm1, %xmm1
	jp	.L639
	ucomiss	%xmm0, %xmm0
	jp	.L618
	comiss	%xmm1, %xmm0
	ja	.L603
	xorl	%r8d, %r8d
	comiss	%xmm0, %xmm1
	ja	.L603
.L631:
	movq	232(%r12), %rax
	xorl	%r8d, %r8d
	movl	20(%rax), %edx
	movq	264(%r15), %rax
	movl	20(%rax), %eax
	andl	$16777215, %edx
	andl	$16777215, %eax
	cmpl	%eax, %edx
	seta	%r8b
	jmp	.L603
	.p2align 4,,10
	.p2align 3
.L587:
	movq	%r14, %r15
	cmpq	32(%rdi), %r14
	je	.L609
	movss	240(%rsi), %xmm1
	jmp	.L610
	.p2align 4,,10
	.p2align 3
.L638:
	movl	$280, %esi
	movl	%r8d, -52(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-52(%rbp), %r8d
	movq	%rax, %rbx
	jmp	.L607
	.p2align 4,,10
	.p2align 3
.L636:
	xorl	%eax, %eax
	jmp	.L602
	.p2align 4,,10
	.p2align 3
.L622:
	movq	%rax, %rsi
.L612:
	movq	264(%rsi), %rdx
	movq	%rax, %r15
	movq	%rsi, %rax
	movl	20(%rdx), %ecx
	movq	232(%r12), %rdx
	movl	20(%rdx), %edx
	andl	$16777215, %ecx
	andl	$16777215, %edx
	cmpl	%edx, %ecx
	seta	%dl
	jmp	.L601
	.p2align 4,,10
	.p2align 3
.L639:
	ucomiss	%xmm0, %xmm0
	jnp	.L603
	jmp	.L631
.L618:
	xorl	%r8d, %r8d
	jmp	.L603
	.cfi_endproc
.LFE23774:
	.size	_ZNSt8_Rb_treeIN2v88internal8compiler19JSInliningHeuristic9CandidateES4_St9_IdentityIS4_ENS3_16CandidateCompareENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_, .-_ZNSt8_Rb_treeIN2v88internal8compiler19JSInliningHeuristic9CandidateES4_St9_IdentityIS4_ENS3_16CandidateCompareENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_
	.section	.rodata._ZN2v88internal8compiler19JSInliningHeuristic6ReduceEPNS1_4NodeE.part.0.str1.8,"aMS",@progbits,1
	.align 8
.LC14:
	.string	"Not considering call site #%d:%s, because polymorphic inlining is disabled\n"
	.align 8
.LC15:
	.string	"Missing bytecode array trying to inline JSFunction "
	.align 8
.LC16:
	.string	"Missing bytecode array trying to inline SharedFunctionInfo "
	.align 8
.LC17:
	.string	"Not considering call site #%d:%s, because of recursive inlining\n"
	.align 8
.LC18:
	.string	"Inlining small function(s) at call site #%d:%s\n"
	.section	.text._ZN2v88internal8compiler19JSInliningHeuristic6ReduceEPNS1_4NodeE.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler19JSInliningHeuristic6ReduceEPNS1_4NodeE.part.0, @function
_ZN2v88internal8compiler19JSInliningHeuristic6ReduceEPNS1_4NodeE.part.0:
.LFB25019:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$376, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	20(%rsi), %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	160(%rdi), %rax
	andl	$16777215, %edx
	testq	%rax, %rax
	je	.L641
	leaq	152(%rdi), %rsi
	movq	%rsi, %rcx
	jmp	.L642
	.p2align 4,,10
	.p2align 3
.L705:
	movq	%rax, %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L643
.L642:
	cmpl	32(%rax), %edx
	jbe	.L705
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L642
.L643:
	cmpq	%rcx, %rsi
	je	.L641
	cmpl	32(%rcx), %edx
	jb	.L641
.L704:
	xorl	%eax, %eax
.L646:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L706
	addq	$376, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L641:
	.cfi_restore_state
	leaq	-336(%rbp), %rax
	leaq	136(%r14), %rdi
	movl	%edx, -336(%rbp)
	movq	%rax, %rsi
	movq	%rax, -392(%rbp)
	call	_ZNSt8_Rb_treeIjjSt9_IdentityIjESt4lessIjEN2v88internal13ZoneAllocatorIjEEE16_M_insert_uniqueIjEESt4pairISt17_Rb_tree_iteratorIjEbEOT_
	movl	$4, %ecx
	movq	%r15, %rdx
	movq	%r14, %rsi
	leaq	-304(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -400(%rbp)
	call	_ZN2v88internal8compiler19JSInliningHeuristic16CollectFunctionsEPNS1_4NodeEi
	movl	-80(%rbp), %eax
	testl	%eax, %eax
	je	.L704
	cmpl	$1, %eax
	jle	.L648
	cmpb	$0, _ZN2v88internal25FLAG_polymorphic_inliningE(%rip)
	je	.L707
.L648:
	movq	%r15, %rdi
	movl	$0, -60(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties18GetFrameStateInputEPNS1_4NodeE@PLT
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler16FrameStateInfoOfEPKNS1_8OperatorE@PLT
	movl	-80(%rbp), %edx
	movq	%rax, -384(%rbp)
	testl	%edx, %edx
	jle	.L704
	movq	-400(%rbp), %r12
	movq	%r15, -416(%rbp)
	xorl	%r13d, %r13d
	leaq	-368(%rbp), %rbx
	movb	$0, -370(%rbp)
	movb	$1, -369(%rbp)
	movq	%r12, %r15
	movq	%r14, -408(%rbp)
	jmp	.L665
	.p2align 4,,10
	.p2align 3
.L651:
	testb	%al, %al
	jne	.L708
	cmpb	$0, -104(%rbp)
	je	.L662
	movdqa	-96(%rbp), %xmm3
	movaps	%xmm3, -368(%rbp)
.L656:
	movq	%rbx, %rdi
	leaq	-208(%rbp), %r14
	call	_ZNK2v88internal8compiler21SharedFunctionInfoRef12IsInlineableEv@PLT
	movb	%al, (%r14,%r13)
	movq	-384(%rbp), %rax
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L659
	movq	16(%rax), %r12
	testq	%r12, %r12
	je	.L659
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler21SharedFunctionInfoRef6objectEv@PLT
	cmpq	%rax, %r12
	je	.L709
.L659:
	cmpb	$0, 104(%r15)
	je	.L662
	movdqu	112(%r15), %xmm2
	movzbl	(%r14,%r13), %edx
	movaps	%xmm2, -352(%rbp)
	testb	%dl, %dl
	jne	.L710
.L663:
	cmpb	$0, -369(%rbp)
	jne	.L711
.L664:
	movl	-80(%rbp), %edx
.L654:
	addq	$1, %r13
	addq	$24, %r15
	cmpl	%r13d, %edx
	jle	.L712
.L665:
	cmpb	$0, 104(%r15)
	movzbl	(%r15), %eax
	jne	.L651
	movq	-408(%rbp), %rdi
	movq	208(%rdi), %rdi
	movzbl	124(%rdi), %esi
	testb	%al, %al
	je	.L652
	testb	%sil, %sil
	je	.L653
	cmpb	$0, _ZN2v88internal30FLAG_trace_heap_broker_verboseE(%rip)
	je	.L653
	call	_ZN2v88internal8compiler12JSHeapBroker5TraceEv@PLT
	leaq	.LC15(%rip), %rsi
	movl	$51, %edx
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	8(%r15), %rsi
	jmp	.L703
.L652:
	testb	%sil, %sil
	je	.L653
	cmpb	$0, _ZN2v88internal30FLAG_trace_heap_broker_verboseE(%rip)
	je	.L653
	call	_ZN2v88internal8compiler12JSHeapBroker5TraceEv@PLT
	leaq	.LC16(%rip), %rsi
	movl	$59, %edx
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	-96(%rbp), %rsi
.L703:
	movq	%r12, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_9ObjectRefE@PLT
	movq	-392(%rbp), %rsi
	movl	$1, %edx
	movb	$10, -336(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	-80(%rbp), %edx
	.p2align 4,,10
	.p2align 3
.L653:
	movb	$0, -208(%rbp,%r13)
	jmp	.L654
.L708:
	leaq	8(%r15), %rdi
	call	_ZNK2v88internal8compiler13JSFunctionRef6sharedEv@PLT
	movq	%rax, -368(%rbp)
	movq	%rdx, -360(%rbp)
	jmp	.L656
.L711:
	movdqa	-352(%rbp), %xmm4
	movq	-392(%rbp), %rdi
	movaps	%xmm4, -336(%rbp)
	call	_ZNK2v88internal8compiler17FixedArrayBaseRef6lengthEv@PLT
	cmpl	_ZN2v88internal36FLAG_max_inlined_bytecode_size_smallE(%rip), %eax
	setle	-369(%rbp)
	jmp	.L664
.L710:
	leaq	-352(%rbp), %rdi
	movb	%dl, -370(%rbp)
	call	_ZNK2v88internal8compiler17FixedArrayBaseRef6lengthEv@PLT
	addl	%eax, -60(%rbp)
	jmp	.L663
.L709:
	cmpb	$0, _ZN2v88internal25FLAG_trace_turbo_inliningE(%rip)
	jne	.L713
.L661:
	movb	$0, (%r14,%r13)
	jmp	.L659
.L707:
	cmpb	$0, _ZN2v88internal25FLAG_trace_turbo_inliningE(%rip)
	je	.L704
	movq	(%r15), %rax
	movl	20(%r15), %esi
	leaq	.LC14(%rip), %rdi
	movq	8(%rax), %rdx
	andl	$16777215, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L704
.L713:
	movq	-416(%rbp), %rdi
	movq	(%rdi), %rax
	movq	8(%rax), %rdx
	movl	20(%rdi), %eax
	leaq	.LC17(%rip), %rdi
	movl	%eax, %esi
	movl	%eax, -376(%rbp)
	xorl	%eax, %eax
	andl	$16777215, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L661
.L662:
	leaq	.LC4(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L712:
	cmpb	$0, -370(%rbp)
	movq	-408(%rbp), %r14
	movq	-416(%rbp), %r15
	je	.L704
	movq	(%r15), %rdi
	cmpw	$755, 16(%rdi)
	je	.L714
	call	_ZN2v88internal8compiler21ConstructParametersOfEPKNS1_8OperatorE@PLT
	movdqu	(%rax), %xmm6
	movaps	%xmm6, -336(%rbp)
	movq	16(%rax), %rax
	movss	-332(%rbp), %xmm0
	movq	%rax, -320(%rbp)
	movss	%xmm0, -64(%rbp)
.L667:
	movl	16(%r14), %edx
	xorl	%eax, %eax
	cmpl	$1, %edx
	je	.L646
	cmpl	$2, %edx
	jne	.L701
	movq	-400(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler19JSInliningHeuristic15InlineCandidateERKNS2_9CandidateEb
	jmp	.L646
.L714:
	call	_ZN2v88internal8compiler16CallParametersOfEPKNS1_8OperatorE@PLT
	movdqu	(%rax), %xmm5
	movaps	%xmm5, -336(%rbp)
	movq	16(%rax), %rax
	movss	-332(%rbp), %xmm0
	movq	%rax, -320(%rbp)
	movss	%xmm0, -64(%rbp)
	jmp	.L667
.L701:
	movss	-64(%rbp), %xmm0
	ucomiss	%xmm0, %xmm0
	jp	.L671
	movsd	_ZN2v88internal27FLAG_min_inlining_frequencyE(%rip), %xmm1
	cvtss2sd	%xmm0, %xmm0
	comisd	%xmm0, %xmm1
	ja	.L704
.L671:
	cmpb	$0, -369(%rbp)
	je	.L672
	cmpb	$0, _ZN2v88internal25FLAG_trace_turbo_inliningE(%rip)
	jne	.L715
.L673:
	movq	-400(%rbp), %rsi
	movl	$1, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler19JSInliningHeuristic15InlineCandidateERKNS2_9CandidateEb
	jmp	.L646
.L706:
	call	__stack_chk_fail@PLT
.L715:
	movq	(%r15), %rax
	movl	20(%r15), %esi
	leaq	.LC18(%rip), %rdi
	movq	8(%rax), %rdx
	andl	$16777215, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L673
.L672:
	movq	-400(%rbp), %rsi
	leaq	80(%r14), %rdi
	call	_ZNSt8_Rb_treeIN2v88internal8compiler19JSInliningHeuristic9CandidateES4_St9_IdentityIS4_ENS3_16CandidateCompareENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_
	jmp	.L704
	.cfi_endproc
.LFE25019:
	.size	_ZN2v88internal8compiler19JSInliningHeuristic6ReduceEPNS1_4NodeE.part.0, .-_ZN2v88internal8compiler19JSInliningHeuristic6ReduceEPNS1_4NodeE.part.0
	.section	.text._ZN2v88internal8compiler19JSInliningHeuristic6ReduceEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19JSInliningHeuristic6ReduceEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19JSInliningHeuristic6ReduceEPNS1_4NodeE, @function
_ZN2v88internal8compiler19JSInliningHeuristic6ReduceEPNS1_4NodeE:
.LFB20594:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	movzwl	16(%rax), %eax
	cmpl	$755, %eax
	je	.L717
	cmpl	$760, %eax
	je	.L717
.L721:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L717:
	movl	_ZN2v88internal39FLAG_max_inlined_bytecode_size_absoluteE(%rip), %eax
	cmpl	%eax, 216(%rdi)
	jl	.L719
	cmpl	$2, 16(%rdi)
	jne	.L721
.L719:
	jmp	_ZN2v88internal8compiler19JSInliningHeuristic6ReduceEPNS1_4NodeE.part.0
	.cfi_endproc
.LFE20594:
	.size	_ZN2v88internal8compiler19JSInliningHeuristic6ReduceEPNS1_4NodeE, .-_ZN2v88internal8compiler19JSInliningHeuristic6ReduceEPNS1_4NodeE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler19JSInliningHeuristic16CollectFunctionsEPNS1_4NodeEi,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler19JSInliningHeuristic16CollectFunctionsEPNS1_4NodeEi, @function
_GLOBAL__sub_I__ZN2v88internal8compiler19JSInliningHeuristic16CollectFunctionsEPNS1_4NodeEi:
.LFB24909:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE24909:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler19JSInliningHeuristic16CollectFunctionsEPNS1_4NodeEi, .-_GLOBAL__sub_I__ZN2v88internal8compiler19JSInliningHeuristic16CollectFunctionsEPNS1_4NodeEi
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler19JSInliningHeuristic16CollectFunctionsEPNS1_4NodeEi
	.hidden	_ZTCN2v88internal12StdoutStreamE0_So
	.weak	_ZTCN2v88internal12StdoutStreamE0_So
	.section	.rodata._ZTCN2v88internal12StdoutStreamE0_So,"aG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTCN2v88internal12StdoutStreamE0_So, @object
	.size	_ZTCN2v88internal12StdoutStreamE0_So, 80
_ZTCN2v88internal12StdoutStreamE0_So:
	.quad	80
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-80
	.quad	-80
	.quad	0
	.quad	0
	.quad	0
	.hidden	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE
	.weak	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE
	.section	.rodata._ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE,"aG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE, @object
	.size	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE, 80
_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE:
	.quad	80
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-80
	.quad	-80
	.quad	0
	.quad	0
	.quad	0
	.weak	_ZTTN2v88internal12StdoutStreamE
	.section	.data.rel.ro.local._ZTTN2v88internal12StdoutStreamE,"awG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTTN2v88internal12StdoutStreamE, @object
	.size	_ZTTN2v88internal12StdoutStreamE, 48
_ZTTN2v88internal12StdoutStreamE:
	.quad	_ZTVN2v88internal12StdoutStreamE+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_So+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_So+64
	.quad	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE+64
	.quad	_ZTVN2v88internal12StdoutStreamE+64
	.weak	_ZTVN2v88internal12StdoutStreamE
	.section	.data.rel.ro.local._ZTVN2v88internal12StdoutStreamE,"awG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTVN2v88internal12StdoutStreamE, @object
	.size	_ZTVN2v88internal12StdoutStreamE, 80
_ZTVN2v88internal12StdoutStreamE:
	.quad	80
	.quad	0
	.quad	0
	.quad	_ZN2v88internal12StdoutStreamD1Ev
	.quad	_ZN2v88internal12StdoutStreamD0Ev
	.quad	-80
	.quad	-80
	.quad	0
	.quad	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.quad	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.weak	_ZTVN2v88internal8compiler19JSInliningHeuristicE
	.section	.data.rel.ro.local._ZTVN2v88internal8compiler19JSInliningHeuristicE,"awG",@progbits,_ZTVN2v88internal8compiler19JSInliningHeuristicE,comdat
	.align 8
	.type	_ZTVN2v88internal8compiler19JSInliningHeuristicE, @object
	.size	_ZTVN2v88internal8compiler19JSInliningHeuristicE, 56
_ZTVN2v88internal8compiler19JSInliningHeuristicE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal8compiler19JSInliningHeuristicD1Ev
	.quad	_ZN2v88internal8compiler19JSInliningHeuristicD0Ev
	.quad	_ZNK2v88internal8compiler19JSInliningHeuristic12reducer_nameEv
	.quad	_ZN2v88internal8compiler19JSInliningHeuristic6ReduceEPNS1_4NodeE
	.quad	_ZN2v88internal8compiler19JSInliningHeuristic8FinalizeEv
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
