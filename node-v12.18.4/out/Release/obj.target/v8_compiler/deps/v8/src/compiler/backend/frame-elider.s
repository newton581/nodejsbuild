	.file	"frame-elider.cc"
	.text
	.section	.rodata._ZN2v88internal8compiler11FrameElider18PropagateIntoBlockEPNS1_16InstructionBlockE.part.0.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"vector::_M_range_check: __n (which is %zu) >= this->size() (which is %zu)"
	.section	.text._ZN2v88internal8compiler11FrameElider18PropagateIntoBlockEPNS1_16InstructionBlockE.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler11FrameElider18PropagateIntoBlockEPNS1_16InstructionBlockE.part.0, @function
_ZN2v88internal8compiler11FrameElider18PropagateIntoBlockEPNS1_16InstructionBlockE.part.0:
.LFB12985:
	.cfi_startproc
	movq	16(%rsi), %r11
	movq	8(%rsi), %r10
	cmpq	%r10, %r11
	je	.L16
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %rax
	movq	48(%rsi), %rcx
	movq	16(%rax), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	8(%rax), %rdi
	movq	16(%rax), %r9
	movq	40(%rsi), %rax
	subq	%rdi, %r9
	sarq	$3, %r9
	cmpq	%rcx, %rax
	jne	.L9
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L7:
	addq	$4, %rax
	cmpq	%rax, %rcx
	je	.L10
.L9:
	movslq	(%rax), %r8
	cmpq	%r9, %r8
	jnb	.L30
	movq	(%rdi,%r8,8), %rdx
	cmpb	$0, 124(%rdx)
	je	.L7
	cmpb	$0, 120(%rdx)
	je	.L8
	cmpb	$0, 120(%rsi)
	je	.L7
.L8:
	movb	$1, 124(%rsi)
	movl	$1, %r10d
.L1:
	movl	%r10d, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_restore_state
	movq	%r11, %rax
	subq	%r10, %rax
	cmpq	$4, %rax
	je	.L31
	movq	%r10, %rax
	xorl	%r10d, %r10d
	.p2align 4,,10
	.p2align 3
.L15:
	movslq	(%rax), %r8
	cmpq	%r8, %r9
	jbe	.L32
	movq	(%rdi,%r8,8), %rdx
	cmpb	$0, 120(%rdx)
	jne	.L14
	movzbl	124(%rdx), %r10d
	testb	%r10b, %r10b
	je	.L1
.L14:
	addq	$4, %rax
	cmpq	%rax, %r11
	jne	.L15
	testb	%r10b, %r10b
	je	.L1
.L29:
	movb	$1, 124(%rsi)
	movl	$1, %r10d
	jmp	.L1
.L31:
	movslq	(%r10), %r8
	cmpq	%r9, %r8
	jnb	.L33
	movq	(%rdi,%r8,8), %rax
	movzbl	124(%rax), %r10d
	testb	%r10b, %r10b
	jne	.L29
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L16:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	xorl	%r10d, %r10d
	movl	%r10d, %eax
	ret
.L30:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	movq	%r9, %rdx
	movq	%r8, %rsi
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L32:
	movq	%r9, %rdx
	movq	%r8, %rsi
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L33:
	movq	%r9, %rdx
	movq	%r8, %rsi
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE12985:
	.size	_ZN2v88internal8compiler11FrameElider18PropagateIntoBlockEPNS1_16InstructionBlockE.part.0, .-_ZN2v88internal8compiler11FrameElider18PropagateIntoBlockEPNS1_16InstructionBlockE.part.0
	.section	.text._ZN2v88internal8compiler11FrameEliderC2EPNS1_19InstructionSequenceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler11FrameEliderC2EPNS1_19InstructionSequenceE
	.type	_ZN2v88internal8compiler11FrameEliderC2EPNS1_19InstructionSequenceE, @function
_ZN2v88internal8compiler11FrameEliderC2EPNS1_19InstructionSequenceE:
.LFB10717:
	.cfi_startproc
	endbr64
	movq	%rsi, (%rdi)
	ret
	.cfi_endproc
.LFE10717:
	.size	_ZN2v88internal8compiler11FrameEliderC2EPNS1_19InstructionSequenceE, .-_ZN2v88internal8compiler11FrameEliderC2EPNS1_19InstructionSequenceE
	.globl	_ZN2v88internal8compiler11FrameEliderC1EPNS1_19InstructionSequenceE
	.set	_ZN2v88internal8compiler11FrameEliderC1EPNS1_19InstructionSequenceE,_ZN2v88internal8compiler11FrameEliderC2EPNS1_19InstructionSequenceE
	.section	.text._ZN2v88internal8compiler11FrameElider10MarkBlocksEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler11FrameElider10MarkBlocksEv
	.type	_ZN2v88internal8compiler11FrameElider10MarkBlocksEv, @function
_ZN2v88internal8compiler11FrameElider10MarkBlocksEv:
.LFB10720:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	movq	16(%rax), %r11
	movq	8(%rax), %rax
	cmpq	%r11, %rax
	jne	.L55
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L62:
	addq	$8, %rax
	cmpq	%rax, %r11
	je	.L61
.L55:
	movq	(%rax), %rcx
	cmpb	$0, 124(%rcx)
	jne	.L62
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
.L58:
	movl	112(%rcx), %r8d
	movl	116(%rcx), %ebx
	cmpl	%ebx, %r8d
	jge	.L38
	movq	(%rdi), %rdx
	movq	208(%rdx), %r10
	movq	232(%rdx), %r12
	movq	%r10, %rsi
	subq	216(%rdx), %rsi
	movslq	%r8d, %rdx
	movq	%rsi, %r9
	subq	%rsi, %r10
	sarq	$3, %r9
	addq	%r9, %rdx
	subl	%edx, %r8d
.L45:
	testq	%rdx, %rdx
	js	.L40
	cmpq	$63, %rdx
	jg	.L41
	leaq	(%r10,%rdx,8), %rsi
.L42:
	movq	(%rsi), %rsi
	testb	$64, 7(%rsi)
	jne	.L44
	movl	(%rsi), %esi
	movl	%esi, %r9d
	andl	$503, %r9d
	cmpl	$22, %r9d
	je	.L44
	movl	%esi, %r9d
	shrl	$14, %r9d
	andl	$7, %r9d
	subl	$3, %r9d
	cmpl	$1, %r9d
	jbe	.L44
	andl	$511, %esi
	cmpl	$24, %esi
	je	.L44
	addq	$1, %rdx
	leal	(%r8,%rdx), %esi
	cmpl	%esi, %ebx
	jg	.L45
	.p2align 4,,10
	.p2align 3
.L38:
	addq	$8, %rax
	cmpq	%rax, %r11
	je	.L63
.L46:
	movq	(%rax), %rcx
	cmpb	$0, 124(%rcx)
	je	.L58
	addq	$8, %rax
	cmpq	%rax, %r11
	jne	.L46
.L63:
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	.cfi_restore_state
	movb	$1, 124(%rcx)
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L41:
	movq	%rdx, %rsi
	sarq	$6, %rsi
.L43:
	movq	%rsi, %r9
	movq	%rdx, %r14
	movq	(%r12,%rsi,8), %rsi
	salq	$6, %r9
	subq	%r9, %r14
	leaq	(%rsi,%r14,8), %rsi
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L40:
	movq	%rdx, %rsi
	notq	%rsi
	shrq	$6, %rsi
	notq	%rsi
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L56:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 14
	ret
	.p2align 4,,10
	.p2align 3
.L61:
	ret
	.cfi_endproc
.LFE10720:
	.size	_ZN2v88internal8compiler11FrameElider10MarkBlocksEv, .-_ZN2v88internal8compiler11FrameElider10MarkBlocksEv
	.section	.text._ZN2v88internal8compiler11FrameElider14PropagateMarksEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler11FrameElider14PropagateMarksEv
	.type	_ZN2v88internal8compiler11FrameElider14PropagateMarksEv, @function
_ZN2v88internal8compiler11FrameElider14PropagateMarksEv:
.LFB10721:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	.p2align 4,,10
	.p2align 3
.L68:
	movq	(%r14), %rax
	movq	16(%rax), %rax
	movq	8(%rax), %rbx
	movq	16(%rax), %r13
	cmpq	%r13, %rbx
	je	.L64
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L67:
	movq	(%rbx), %rsi
	cmpb	$0, 124(%rsi)
	jne	.L66
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler11FrameElider18PropagateIntoBlockEPNS1_16InstructionBlockE.part.0
	orl	%eax, %r12d
.L66:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jne	.L67
	testb	%r12b, %r12b
	jne	.L68
	movq	(%r14), %rax
	movq	16(%rax), %rax
	movq	16(%rax), %rbx
	movq	8(%rax), %r13
	cmpq	%r13, %rbx
	je	.L64
	.p2align 4,,10
	.p2align 3
.L70:
	movq	-8(%rbx), %rsi
	cmpb	$0, 124(%rsi)
	jne	.L69
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler11FrameElider18PropagateIntoBlockEPNS1_16InstructionBlockE.part.0
	orl	%eax, %r12d
.L69:
	subq	$8, %rbx
	cmpq	%rbx, %r13
	jne	.L70
	testb	%r12b, %r12b
	jne	.L68
.L64:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10721:
	.size	_ZN2v88internal8compiler11FrameElider14PropagateMarksEv, .-_ZN2v88internal8compiler11FrameElider14PropagateMarksEv
	.section	.text._ZN2v88internal8compiler11FrameElider18MarkDeConstructionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler11FrameElider18MarkDeConstructionEv
	.type	_ZN2v88internal8compiler11FrameElider18MarkDeConstructionEv, @function
_ZN2v88internal8compiler11FrameElider18MarkDeConstructionEv:
.LFB10722:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	movq	16(%rax), %r8
	movq	8(%rax), %rsi
	cmpq	%r8, %rsi
	je	.L109
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$6293526, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	.p2align 4,,10
	.p2align 3
.L84:
	movq	(%rsi), %rbx
	cmpb	$0, 124(%rbx)
	movq	16(%rbx), %rcx
	movq	8(%rbx), %rax
	je	.L81
	movq	48(%rbx), %rdx
	cmpq	%rdx, 40(%rbx)
	jne	.L112
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L88:
	addq	$4, %rax
.L112:
	cmpq	%rax, %rcx
	je	.L96
	movq	(%rdi), %r12
	movslq	(%rax), %r10
	movq	16(%r12), %rdx
	movq	8(%rdx), %r11
	movq	16(%rdx), %rdx
	subq	%r11, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %r10
	jnb	.L114
	movq	(%r11,%r10,8), %rdx
	cmpb	$0, 124(%rdx)
	jne	.L88
	movl	116(%rbx), %edx
	movq	208(%r12), %r11
	subl	$1, %edx
	movq	%r11, %r10
	subq	216(%r12), %r10
	sarq	$3, %r10
	movslq	%edx, %rdx
	addq	%rdx, %r10
	js	.L89
	cmpq	$63, %r10
	jg	.L90
	leaq	(%r11,%rdx,8), %rdx
.L91:
	movq	(%rdx), %rdx
	movl	(%rdx), %edx
	movl	%edx, %r10d
	andl	$511, %r10d
	cmpl	$22, %r10d
	jbe	.L115
.L93:
	shrl	$14, %edx
	andl	$7, %edx
	subl	$3, %edx
	cmpl	$1, %edx
	jbe	.L88
	movb	$1, 126(%rbx)
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L96:
	addq	$8, %rsi
	cmpq	%rsi, %r8
	jne	.L84
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L115:
	.cfi_restore_state
	btq	%r10, %r9
	jc	.L88
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L90:
	movq	%r10, %rdx
	sarq	$6, %rdx
.L92:
	movq	232(%r12), %r11
	movq	%rdx, %r12
	salq	$6, %r12
	movq	(%r11,%rdx,8), %rdx
	subq	%r12, %r10
	leaq	(%rdx,%r10,8), %rdx
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L81:
	cmpq	%rax, %rcx
	jne	.L99
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L98:
	addq	$4, %rax
	cmpq	%rax, %rcx
	je	.L96
.L99:
	movq	(%rdi), %rdx
	movslq	(%rax), %r10
	movq	16(%rdx), %rdx
	movq	8(%rdx), %r11
	movq	16(%rdx), %rdx
	subq	%r11, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %r10
	jnb	.L116
	movq	(%r11,%r10,8), %rdx
	cmpb	$0, 124(%rdx)
	je	.L98
	movb	$1, 125(%rdx)
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L89:
	movq	%r10, %rdx
	notq	%rdx
	shrq	$6, %rdx
	notq	%rdx
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L113:
	movb	$1, 125(%rbx)
	jmp	.L112
.L109:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
.L116:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movq	%r10, %rsi
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L114:
	movq	%r10, %rsi
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE10722:
	.size	_ZN2v88internal8compiler11FrameElider18MarkDeConstructionEv, .-_ZN2v88internal8compiler11FrameElider18MarkDeConstructionEv
	.section	.text._ZN2v88internal8compiler11FrameElider3RunEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler11FrameElider3RunEv
	.type	_ZN2v88internal8compiler11FrameElider3RunEv, @function
_ZN2v88internal8compiler11FrameElider3RunEv:
.LFB10719:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal8compiler11FrameElider10MarkBlocksEv
	call	_ZN2v88internal8compiler11FrameElider14PropagateMarksEv
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler11FrameElider18MarkDeConstructionEv
	.cfi_endproc
.LFE10719:
	.size	_ZN2v88internal8compiler11FrameElider3RunEv, .-_ZN2v88internal8compiler11FrameElider3RunEv
	.section	.text._ZN2v88internal8compiler11FrameElider16PropagateInOrderEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler11FrameElider16PropagateInOrderEv
	.type	_ZN2v88internal8compiler11FrameElider16PropagateInOrderEv, @function
_ZN2v88internal8compiler11FrameElider16PropagateInOrderEv:
.LFB10723:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	movq	8(%rax), %rbx
	movq	16(%rax), %r13
	cmpq	%r13, %rbx
	je	.L123
	movq	%rdi, %r14
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L122:
	movq	(%rbx), %rsi
	cmpb	$0, 124(%rsi)
	jne	.L121
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler11FrameElider18PropagateIntoBlockEPNS1_16InstructionBlockE.part.0
	orl	%eax, %r12d
.L121:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jne	.L122
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L123:
	.cfi_restore_state
	xorl	%r12d, %r12d
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10723:
	.size	_ZN2v88internal8compiler11FrameElider16PropagateInOrderEv, .-_ZN2v88internal8compiler11FrameElider16PropagateInOrderEv
	.section	.text._ZN2v88internal8compiler11FrameElider17PropagateReversedEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler11FrameElider17PropagateReversedEv
	.type	_ZN2v88internal8compiler11FrameElider17PropagateReversedEv, @function
_ZN2v88internal8compiler11FrameElider17PropagateReversedEv:
.LFB10724:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	movq	16(%rax), %rbx
	movq	8(%rax), %r13
	cmpq	%rbx, %r13
	je	.L130
	movq	%rdi, %r14
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L129:
	movq	-8(%rbx), %rsi
	cmpb	$0, 124(%rsi)
	jne	.L128
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler11FrameElider18PropagateIntoBlockEPNS1_16InstructionBlockE.part.0
	orl	%eax, %r12d
.L128:
	subq	$8, %rbx
	cmpq	%rbx, %r13
	jne	.L129
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L130:
	.cfi_restore_state
	xorl	%r12d, %r12d
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10724:
	.size	_ZN2v88internal8compiler11FrameElider17PropagateReversedEv, .-_ZN2v88internal8compiler11FrameElider17PropagateReversedEv
	.section	.text._ZN2v88internal8compiler11FrameElider18PropagateIntoBlockEPNS1_16InstructionBlockE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler11FrameElider18PropagateIntoBlockEPNS1_16InstructionBlockE
	.type	_ZN2v88internal8compiler11FrameElider18PropagateIntoBlockEPNS1_16InstructionBlockE, @function
_ZN2v88internal8compiler11FrameElider18PropagateIntoBlockEPNS1_16InstructionBlockE:
.LFB10725:
	.cfi_startproc
	endbr64
	cmpb	$0, 124(%rsi)
	jne	.L134
	jmp	_ZN2v88internal8compiler11FrameElider18PropagateIntoBlockEPNS1_16InstructionBlockE.part.0
	.p2align 4,,10
	.p2align 3
.L134:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE10725:
	.size	_ZN2v88internal8compiler11FrameElider18PropagateIntoBlockEPNS1_16InstructionBlockE, .-_ZN2v88internal8compiler11FrameElider18PropagateIntoBlockEPNS1_16InstructionBlockE
	.section	.text._ZNK2v88internal8compiler11FrameElider18instruction_blocksEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler11FrameElider18instruction_blocksEv
	.type	_ZNK2v88internal8compiler11FrameElider18instruction_blocksEv, @function
_ZNK2v88internal8compiler11FrameElider18instruction_blocksEv:
.LFB10726:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	ret
	.cfi_endproc
.LFE10726:
	.size	_ZNK2v88internal8compiler11FrameElider18instruction_blocksEv, .-_ZNK2v88internal8compiler11FrameElider18instruction_blocksEv
	.section	.text._ZNK2v88internal8compiler11FrameElider18InstructionBlockAtENS1_9RpoNumberE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler11FrameElider18InstructionBlockAtENS1_9RpoNumberE
	.type	_ZNK2v88internal8compiler11FrameElider18InstructionBlockAtENS1_9RpoNumberE, @function
_ZNK2v88internal8compiler11FrameElider18InstructionBlockAtENS1_9RpoNumberE:
.LFB10727:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movslq	%esi, %rsi
	movq	16(%rax), %rax
	movq	8(%rax), %rcx
	movq	16(%rax), %rdx
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L141
	movq	(%rcx,%rsi,8), %rax
	ret
.L141:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE10727:
	.size	_ZNK2v88internal8compiler11FrameElider18InstructionBlockAtENS1_9RpoNumberE, .-_ZNK2v88internal8compiler11FrameElider18InstructionBlockAtENS1_9RpoNumberE
	.section	.text._ZNK2v88internal8compiler11FrameElider13InstructionAtEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler11FrameElider13InstructionAtEi
	.type	_ZNK2v88internal8compiler11FrameElider13InstructionAtEi, @function
_ZNK2v88internal8compiler11FrameElider13InstructionAtEi:
.LFB10728:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movslq	%esi, %rsi
	movq	208(%rdx), %rcx
	movq	%rcx, %rax
	subq	216(%rdx), %rax
	sarq	$3, %rax
	addq	%rsi, %rax
	js	.L143
	cmpq	$63, %rax
	jle	.L147
	movq	%rax, %rcx
	sarq	$6, %rcx
.L146:
	movq	232(%rdx), %rdx
	movq	%rcx, %rsi
	salq	$6, %rsi
	movq	(%rdx,%rcx,8), %rdx
	subq	%rsi, %rax
	leaq	(%rdx,%rax,8), %rax
	movq	(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L147:
	leaq	(%rcx,%rsi,8), %rax
	movq	(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L143:
	movq	%rax, %rcx
	notq	%rcx
	shrq	$6, %rcx
	notq	%rcx
	jmp	.L146
	.cfi_endproc
.LFE10728:
	.size	_ZNK2v88internal8compiler11FrameElider13InstructionAtEi, .-_ZNK2v88internal8compiler11FrameElider13InstructionAtEi
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler11FrameEliderC2EPNS1_19InstructionSequenceE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler11FrameEliderC2EPNS1_19InstructionSequenceE, @function
_GLOBAL__sub_I__ZN2v88internal8compiler11FrameEliderC2EPNS1_19InstructionSequenceE:
.LFB12958:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE12958:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler11FrameEliderC2EPNS1_19InstructionSequenceE, .-_GLOBAL__sub_I__ZN2v88internal8compiler11FrameEliderC2EPNS1_19InstructionSequenceE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler11FrameEliderC2EPNS1_19InstructionSequenceE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
