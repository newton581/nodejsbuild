	.file	"js-typed-lowering.cc"
	.text
	.section	.text._ZNKSt5ctypeIcE8do_widenEc,"axG",@progbits,_ZNKSt5ctypeIcE8do_widenEc,comdat
	.align 2
	.p2align 4
	.weak	_ZNKSt5ctypeIcE8do_widenEc
	.type	_ZNKSt5ctypeIcE8do_widenEc, @function
_ZNKSt5ctypeIcE8do_widenEc:
.LFB1527:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	ret
	.cfi_endproc
.LFE1527:
	.size	_ZNKSt5ctypeIcE8do_widenEc, .-_ZNKSt5ctypeIcE8do_widenEc
	.section	.rodata._ZNK2v88internal8compiler15JSTypedLowering12reducer_nameEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"JSTypedLowering"
	.section	.text._ZNK2v88internal8compiler15JSTypedLowering12reducer_nameEv,"axG",@progbits,_ZNK2v88internal8compiler15JSTypedLowering12reducer_nameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal8compiler15JSTypedLowering12reducer_nameEv
	.type	_ZNK2v88internal8compiler15JSTypedLowering12reducer_nameEv, @function
_ZNK2v88internal8compiler15JSTypedLowering12reducer_nameEv:
.LFB10163:
	.cfi_startproc
	endbr64
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE10163:
	.size	_ZNK2v88internal8compiler15JSTypedLowering12reducer_nameEv, .-_ZNK2v88internal8compiler15JSTypedLowering12reducer_nameEv
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD2Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD2Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD2Ev:
.LFB11155:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11155:
	.size	_ZN2v88internal23CallInterfaceDescriptorD2Ev, .-_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.weak	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.set	_ZN2v88internal23CallInterfaceDescriptorD1Ev,_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.section	.text._ZN2v88internal8compiler15JSTypedLoweringD2Ev,"axG",@progbits,_ZN2v88internal8compiler15JSTypedLoweringD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler15JSTypedLoweringD2Ev
	.type	_ZN2v88internal8compiler15JSTypedLoweringD2Ev, @function
_ZN2v88internal8compiler15JSTypedLoweringD2Ev:
.LFB28350:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE28350:
	.size	_ZN2v88internal8compiler15JSTypedLoweringD2Ev, .-_ZN2v88internal8compiler15JSTypedLoweringD2Ev
	.weak	_ZN2v88internal8compiler15JSTypedLoweringD1Ev
	.set	_ZN2v88internal8compiler15JSTypedLoweringD1Ev,_ZN2v88internal8compiler15JSTypedLoweringD2Ev
	.section	.rodata._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"unreachable code"
	.section	.text._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB11148:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE11148:
	.size	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB11149:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	movq	%rsi, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %edx
	xorl	%esi, %esi
	jmp	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	.cfi_endproc
.LFE11149:
	.size	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD0Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD0Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD0Ev:
.LFB11157:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11157:
	.size	_ZN2v88internal23CallInterfaceDescriptorD0Ev, .-_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.section	.text._ZN2v88internal8compiler15JSTypedLoweringD0Ev,"axG",@progbits,_ZN2v88internal8compiler15JSTypedLoweringD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler15JSTypedLoweringD0Ev
	.type	_ZN2v88internal8compiler15JSTypedLoweringD0Ev, @function
_ZN2v88internal8compiler15JSTypedLoweringD0Ev:
.LFB28352:
	.cfi_startproc
	endbr64
	movl	$56, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE28352:
	.size	_ZN2v88internal8compiler15JSTypedLoweringD0Ev, .-_ZN2v88internal8compiler15JSTypedLoweringD0Ev
	.section	.text._ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.0, @function
_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.0:
.LFB29263:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movzbl	23(%rdi), %eax
	movq	32(%rdi), %r8
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L12
	leaq	32(%rdi), %rbx
	cmpq	%r8, %rsi
	je	.L11
	leaq	-24(%rdi), %r12
	testq	%r8, %r8
	je	.L16
.L24:
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L16:
	movq	%r13, (%rbx)
	testq	%r13, %r13
	je	.L11
	addq	$8, %rsp
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	.cfi_restore_state
	movq	16(%r8), %rax
	leaq	16(%r8), %rbx
	cmpq	%rax, %rsi
	je	.L11
	movq	%r8, %rdi
	movq	%rax, %r8
	leaq	-24(%rdi), %r12
	testq	%r8, %r8
	jne	.L24
	jmp	.L16
	.cfi_endproc
.LFE29263:
	.size	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.0, .-_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.0
	.section	.text._ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.1,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.1, @function
_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.1:
.LFB29262:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movzbl	23(%rdi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L26
	movq	40(%rdi), %r8
	leaq	40(%rdi), %rbx
	cmpq	%r8, %rsi
	je	.L25
	leaq	-48(%rdi), %r12
	testq	%r8, %r8
	je	.L30
.L38:
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L30:
	movq	%r13, (%rbx)
	testq	%r13, %r13
	je	.L25
	addq	$8, %rsp
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	movq	32(%rdi), %rdi
	movq	24(%rdi), %r8
	cmpq	%r8, %rsi
	je	.L25
	leaq	24(%rdi), %rbx
	leaq	-48(%rdi), %r12
	testq	%r8, %r8
	jne	.L38
	jmp	.L30
	.cfi_endproc
.LFE29262:
	.size	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.1, .-_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.1
	.section	.rodata._ZN2v88internal8compiler7NameRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb.str1.1,"aMS",@progbits,1
.LC2:
	.string	"IsName()"
.LC3:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal8compiler7NameRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,"axG",@progbits,_ZN2v88internal8compiler7NameRefC5EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler7NameRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.type	_ZN2v88internal8compiler7NameRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, @function
_ZN2v88internal8compiler7NameRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb:
.LFB9654:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%ecx, %ebx
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	testb	%bl, %bl
	jne	.L42
.L39:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef6IsNameEv@PLT
	testb	%al, %al
	jne	.L39
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE9654:
	.size	_ZN2v88internal8compiler7NameRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, .-_ZN2v88internal8compiler7NameRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.weak	_ZN2v88internal8compiler7NameRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.set	_ZN2v88internal8compiler7NameRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,_ZN2v88internal8compiler7NameRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.section	.rodata._ZN2v88internal8compiler7CellRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb.str1.1,"aMS",@progbits,1
.LC4:
	.string	"IsCell()"
	.section	.text._ZN2v88internal8compiler7CellRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,"axG",@progbits,_ZN2v88internal8compiler7CellRefC5EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler7CellRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.type	_ZN2v88internal8compiler7CellRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, @function
_ZN2v88internal8compiler7CellRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb:
.LFB9789:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%ecx, %ebx
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	testb	%bl, %bl
	jne	.L46
.L43:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef6IsCellEv@PLT
	testb	%al, %al
	jne	.L43
	leaq	.LC4(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE9789:
	.size	_ZN2v88internal8compiler7CellRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, .-_ZN2v88internal8compiler7CellRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.weak	_ZN2v88internal8compiler7CellRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.set	_ZN2v88internal8compiler7CellRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,_ZN2v88internal8compiler7CellRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.section	.rodata._ZN2v88internal8compiler7CodeRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb.str1.1,"aMS",@progbits,1
.LC5:
	.string	"IsCode()"
	.section	.text._ZN2v88internal8compiler7CodeRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,"axG",@progbits,_ZN2v88internal8compiler7CodeRefC5EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler7CodeRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.type	_ZN2v88internal8compiler7CodeRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, @function
_ZN2v88internal8compiler7CodeRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb:
.LFB9807:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%ecx, %ebx
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	testb	%bl, %bl
	jne	.L50
.L47:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef6IsCodeEv@PLT
	testb	%al, %al
	jne	.L47
	leaq	.LC5(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE9807:
	.size	_ZN2v88internal8compiler7CodeRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, .-_ZN2v88internal8compiler7CodeRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.weak	_ZN2v88internal8compiler7CodeRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.set	_ZN2v88internal8compiler7CodeRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,_ZN2v88internal8compiler7CodeRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.section	.text._ZN2v88internal8compiler16JSBinopReduction26IsReceiverCompareOperationEv,"axG",@progbits,_ZN2v88internal8compiler16JSBinopReduction26IsReceiverCompareOperationEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler16JSBinopReduction26IsReceiverCompareOperationEv
	.type	_ZN2v88internal8compiler16JSBinopReduction26IsReceiverCompareOperationEv, @function
_ZN2v88internal8compiler16JSBinopReduction26IsReceiverCompareOperationEv:
.LFB23746:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler22CompareOperationHintOfEPKNS1_8OperatorE@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	cmpb	$8, %r8b
	je	.L64
.L51:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L65
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L64:
	.cfi_restore_state
	movq	8(%rbx), %rax
	movq	32(%rax), %rdx
	movzbl	23(%rax), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L66
.L53:
	movq	8(%rdx), %rax
	leaq	-32(%rbp), %rdi
	movl	$75431937, %esi
	movq	%rax, -32(%rbp)
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	je	.L57
	movq	8(%rbx), %rax
	movzbl	23(%rax), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L55
	addq	$40, %rax
.L56:
	movq	(%rax), %rax
	leaq	-40(%rbp), %rdi
	movl	$75431937, %esi
	movq	8(%rax), %rax
	movq	%rax, -40(%rbp)
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L51
.L57:
	xorl	%eax, %eax
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L66:
	movq	16(%rdx), %rdx
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L55:
	movq	32(%rax), %rax
	addq	$24, %rax
	jmp	.L56
.L65:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23746:
	.size	_ZN2v88internal8compiler16JSBinopReduction26IsReceiverCompareOperationEv, .-_ZN2v88internal8compiler16JSBinopReduction26IsReceiverCompareOperationEv
	.section	.text._ZN2v88internal8compiler16JSBinopReduction43IsReceiverOrNullOrUndefinedCompareOperationEv,"axG",@progbits,_ZN2v88internal8compiler16JSBinopReduction43IsReceiverOrNullOrUndefinedCompareOperationEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler16JSBinopReduction43IsReceiverOrNullOrUndefinedCompareOperationEv
	.type	_ZN2v88internal8compiler16JSBinopReduction43IsReceiverOrNullOrUndefinedCompareOperationEv, @function
_ZN2v88internal8compiler16JSBinopReduction43IsReceiverOrNullOrUndefinedCompareOperationEv:
.LFB23747:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler22CompareOperationHintOfEPKNS1_8OperatorE@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	cmpb	$9, %r8b
	je	.L80
.L67:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L81
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L80:
	.cfi_restore_state
	movq	8(%rbx), %rax
	movq	32(%rax), %rdx
	movzbl	23(%rax), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L82
.L69:
	movq	8(%rdx), %rax
	leaq	-32(%rbp), %rdi
	movl	$75432321, %esi
	movq	%rax, -32(%rbp)
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	je	.L73
	movq	8(%rbx), %rax
	movzbl	23(%rax), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L71
	addq	$40, %rax
.L72:
	movq	(%rax), %rax
	leaq	-40(%rbp), %rdi
	movl	$75432321, %esi
	movq	8(%rax), %rax
	movq	%rax, -40(%rbp)
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L67
.L73:
	xorl	%eax, %eax
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L82:
	movq	16(%rdx), %rdx
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L71:
	movq	32(%rax), %rax
	addq	$24, %rax
	jmp	.L72
.L81:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23747:
	.size	_ZN2v88internal8compiler16JSBinopReduction43IsReceiverOrNullOrUndefinedCompareOperationEv, .-_ZN2v88internal8compiler16JSBinopReduction43IsReceiverOrNullOrUndefinedCompareOperationEv
	.section	.text._ZN2v88internal8compiler16JSBinopReduction24IsStringCompareOperationEv,"axG",@progbits,_ZN2v88internal8compiler16JSBinopReduction24IsStringCompareOperationEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler16JSBinopReduction24IsStringCompareOperationEv
	.type	_ZN2v88internal8compiler16JSBinopReduction24IsStringCompareOperationEv, @function
_ZN2v88internal8compiler16JSBinopReduction24IsStringCompareOperationEv:
.LFB23748:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler22CompareOperationHintOfEPKNS1_8OperatorE@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	cmpb	$5, %r8b
	je	.L96
.L83:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L97
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L96:
	.cfi_restore_state
	movq	8(%rbx), %rax
	movq	32(%rax), %rdx
	movzbl	23(%rax), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L98
.L85:
	movq	8(%rdx), %rax
	leaq	-32(%rbp), %rdi
	movl	$16417, %esi
	movq	%rax, -32(%rbp)
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	je	.L89
	movq	8(%rbx), %rax
	movzbl	23(%rax), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L87
	addq	$40, %rax
.L88:
	movq	(%rax), %rax
	leaq	-40(%rbp), %rdi
	movl	$16417, %esi
	movq	8(%rax), %rax
	movq	%rax, -40(%rbp)
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L83
.L89:
	xorl	%eax, %eax
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L98:
	movq	16(%rdx), %rdx
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L87:
	movq	32(%rax), %rax
	addq	$24, %rax
	jmp	.L88
.L97:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23748:
	.size	_ZN2v88internal8compiler16JSBinopReduction24IsStringCompareOperationEv, .-_ZN2v88internal8compiler16JSBinopReduction24IsStringCompareOperationEv
	.section	.text._ZN2v88internal8compiler16JSBinopReduction24IsSymbolCompareOperationEv,"axG",@progbits,_ZN2v88internal8compiler16JSBinopReduction24IsSymbolCompareOperationEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler16JSBinopReduction24IsSymbolCompareOperationEv
	.type	_ZN2v88internal8compiler16JSBinopReduction24IsSymbolCompareOperationEv, @function
_ZN2v88internal8compiler16JSBinopReduction24IsSymbolCompareOperationEv:
.LFB23749:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler22CompareOperationHintOfEPKNS1_8OperatorE@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	cmpb	$6, %r8b
	je	.L112
.L99:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L113
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L112:
	.cfi_restore_state
	movq	8(%rbx), %rax
	movq	32(%rax), %rdx
	movzbl	23(%rax), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L114
.L101:
	movq	8(%rdx), %rax
	leaq	-32(%rbp), %rdi
	movl	$8193, %esi
	movq	%rax, -32(%rbp)
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	je	.L105
	movq	8(%rbx), %rax
	movzbl	23(%rax), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L103
	addq	$40, %rax
.L104:
	movq	(%rax), %rax
	leaq	-40(%rbp), %rdi
	movl	$8193, %esi
	movq	8(%rax), %rax
	movq	%rax, -40(%rbp)
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L99
.L105:
	xorl	%eax, %eax
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L114:
	movq	16(%rdx), %rdx
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L103:
	movq	32(%rax), %rax
	addq	$24, %rax
	jmp	.L104
.L113:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23749:
	.size	_ZN2v88internal8compiler16JSBinopReduction24IsSymbolCompareOperationEv, .-_ZN2v88internal8compiler16JSBinopReduction24IsSymbolCompareOperationEv
	.section	.rodata._ZN2v88internal8compiler16JSBinopReduction22ShouldCreateConsStringEv.str1.1,"aMS",@progbits,1
.LC6:
	.string	"IsHeapObject()"
	.section	.text._ZN2v88internal8compiler16JSBinopReduction22ShouldCreateConsStringEv,"axG",@progbits,_ZN2v88internal8compiler16JSBinopReduction22ShouldCreateConsStringEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler16JSBinopReduction22ShouldCreateConsStringEv
	.type	_ZN2v88internal8compiler16JSBinopReduction22ShouldCreateConsStringEv, @function
_ZN2v88internal8compiler16JSBinopReduction22ShouldCreateConsStringEv:
.LFB23750:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	8(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%r12), %eax
	movq	32(%r12), %rcx
	leaq	32(%r12), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L116
	movq	16(%rcx), %rcx
.L116:
	movq	8(%rcx), %rcx
	movq	%rcx, -80(%rbp)
	cmpq	$16417, %rcx
	jne	.L183
	leaq	8(%rdx), %rcx
	cmpl	$15, %eax
	je	.L184
	.p2align 4,,10
	.p2align 3
.L122:
	movq	(%rcx), %rcx
	movq	8(%rcx), %rcx
	movq	%rcx, -80(%rbp)
	cmpq	$16417, %rcx
	jne	.L185
.L126:
	movq	(%rdx), %r14
	cmpl	$15, %eax
	jne	.L182
	leaq	16(%r14), %rdx
	movq	16(%r14), %r14
.L182:
	movq	(%r14), %rcx
	movzwl	16(%rcx), %eax
	cmpw	$30, %ax
	sete	%r15b
	je	.L129
	xorl	%r13d, %r13d
	xorl	%ecx, %ecx
.L132:
	movq	8(%rdx), %rsi
	addq	$8, %rdx
	movq	(%rsi), %rax
	cmpw	$30, 16(%rax)
	jne	.L134
	movq	48(%rax), %r12
	movq	(%rbx), %rax
	movq	24(%rax), %r14
.L135:
	leaq	-128(%rbp), %rbx
	xorl	%ecx, %ecx
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L140
	movdqa	-128(%rbp), %xmm0
	leaq	-80(%rbp), %rbx
	movq	%rbx, %rdi
	movaps	%xmm0, -80(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef8IsStringEv@PLT
	testb	%al, %al
	je	.L139
	leaq	-112(%rbp), %rdi
	movq	%r12, %rdx
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	%rdi, -136(%rbp)
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	-136(%rbp), %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	movl	%eax, %r12d
	testb	%al, %al
	je	.L140
	movdqa	-112(%rbp), %xmm2
	movq	%rbx, %rdi
	movaps	%xmm2, -80(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef8AsStringEv@PLT
	leaq	-96(%rbp), %rdi
	movq	%rax, -96(%rbp)
	movq	%rdx, -88(%rbp)
	call	_ZNK2v88internal8compiler9StringRef6lengthEv@PLT
	cmpl	$12, %eax
	jle	.L139
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L183:
	leaq	-80(%rbp), %rdi
	movl	$16417, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L120
	movq	8(%rbx), %r12
	movzbl	23(%r12), %eax
	leaq	32(%r12), %rdx
	leaq	8(%rdx), %rcx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L122
.L184:
	movq	(%rdx), %rsi
	leaq	24(%rsi), %rcx
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L185:
	leaq	-80(%rbp), %rdi
	movl	$16417, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L179
.L120:
	movq	8(%rbx), %rax
	xorl	%r12d, %r12d
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler21BinaryOperationHintOfEPKNS1_8OperatorE@PLT
	cmpb	$6, %al
	je	.L179
.L115:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L186
	addq	$104, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L134:
	.cfi_restore_state
	movq	(%r12), %rax
	testb	$1, 18(%rax)
	je	.L136
	testb	%cl, %cl
	je	.L136
	movq	%r12, %rdi
	xorl	%r15d, %r15d
	call	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.0
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%r13, %r12
	call	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.1
	movq	(%rbx), %rax
	xorl	%r13d, %r13d
	movq	24(%rax), %r14
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L136:
	movq	(%rbx), %rax
	movq	24(%rax), %r14
.L139:
	testb	%r15b, %r15b
	jne	.L187
.L144:
	xorl	%r12d, %r12d
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L129:
	movq	48(%rcx), %r13
	movl	$1, %ecx
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L179:
	movq	8(%rbx), %r12
	movzbl	23(%r12), %eax
	leaq	32(%r12), %rdx
	andl	$15, %eax
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L187:
	leaq	-96(%rbp), %r15
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L140
	movdqa	-96(%rbp), %xmm1
	leaq	-80(%rbp), %r12
	movq	%r12, %rdi
	movaps	%xmm1, -80(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef8IsStringEv@PLT
	testb	%al, %al
	je	.L144
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L140
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef8AsStringEv@PLT
	movq	%r15, %rdi
	movq	%rax, -96(%rbp)
	movq	%rdx, -88(%rbp)
	call	_ZNK2v88internal8compiler9StringRef6lengthEv@PLT
	cmpl	$12, %eax
	jle	.L144
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler9StringRef11IsSeqStringEv@PLT
	movl	%eax, %r12d
	testb	%al, %al
	jne	.L115
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler9StringRef16IsExternalStringEv@PLT
	movl	%eax, %r12d
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L140:
	leaq	.LC6(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L186:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23750:
	.size	_ZN2v88internal8compiler16JSBinopReduction22ShouldCreateConsStringEv, .-_ZN2v88internal8compiler16JSBinopReduction22ShouldCreateConsStringEv
	.section	.text._ZN2v88internal8compiler16JSBinopReduction24CheckLeftInputToReceiverEv,"axG",@progbits,_ZN2v88internal8compiler16JSBinopReduction24CheckLeftInputToReceiverEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler16JSBinopReduction24CheckLeftInputToReceiverEv
	.type	_ZN2v88internal8compiler16JSBinopReduction24CheckLeftInputToReceiverEv, @function
_ZN2v88internal8compiler16JSBinopReduction24CheckLeftInputToReceiverEv:
.LFB23751:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	8(%rdi), %rdi
	movq	16(%rax), %rax
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	8(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	8(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rax, -72(%rbp)
	movq	(%rbx), %rax
	movq	16(%rax), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder13CheckReceiverEv@PLT
	movq	%r13, %rdi
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	-72(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$3, %edx
	movq	%r12, -48(%rbp)
	movhps	-80(%rbp), %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%rbx), %rdi
	movq	%rax, %r13
	movzbl	23(%rdi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L189
	movq	32(%rdi), %r8
	leaq	32(%rdi), %r14
	cmpq	%r8, %r13
	je	.L191
	leaq	-24(%rdi), %r12
	testq	%r8, %r8
	je	.L192
.L201:
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L192:
	movq	%r13, (%r14)
	testq	%r13, %r13
	je	.L199
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L199:
	movq	8(%rbx), %rdi
.L191:
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler14NodeProperties18ReplaceEffectInputEPNS1_4NodeES4_i@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L200
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L189:
	.cfi_restore_state
	movq	32(%rdi), %rax
	movq	16(%rax), %r8
	leaq	16(%rax), %r14
	cmpq	%r8, %r13
	je	.L191
	movq	%rax, %rdi
	leaq	-24(%rdi), %r12
	testq	%r8, %r8
	jne	.L201
	jmp	.L192
.L200:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23751:
	.size	_ZN2v88internal8compiler16JSBinopReduction24CheckLeftInputToReceiverEv, .-_ZN2v88internal8compiler16JSBinopReduction24CheckLeftInputToReceiverEv
	.section	.text._ZN2v88internal8compiler16JSBinopReduction41CheckLeftInputToReceiverOrNullOrUndefinedEv,"axG",@progbits,_ZN2v88internal8compiler16JSBinopReduction41CheckLeftInputToReceiverOrNullOrUndefinedEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler16JSBinopReduction41CheckLeftInputToReceiverOrNullOrUndefinedEv
	.type	_ZN2v88internal8compiler16JSBinopReduction41CheckLeftInputToReceiverOrNullOrUndefinedEv, @function
_ZN2v88internal8compiler16JSBinopReduction41CheckLeftInputToReceiverOrNullOrUndefinedEv:
.LFB23752:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	8(%rdi), %rdi
	movq	16(%rax), %rax
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	8(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	8(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rax, -72(%rbp)
	movq	(%rbx), %rax
	movq	16(%rax), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder30CheckReceiverOrNullOrUndefinedEv@PLT
	movq	%r13, %rdi
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	-72(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$3, %edx
	movq	%r12, -48(%rbp)
	movhps	-80(%rbp), %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%rbx), %rdi
	movq	%rax, %r13
	movzbl	23(%rdi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L203
	movq	32(%rdi), %r8
	leaq	32(%rdi), %r14
	cmpq	%r8, %r13
	je	.L205
	leaq	-24(%rdi), %r12
	testq	%r8, %r8
	je	.L206
.L215:
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L206:
	movq	%r13, (%r14)
	testq	%r13, %r13
	je	.L213
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L213:
	movq	8(%rbx), %rdi
.L205:
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler14NodeProperties18ReplaceEffectInputEPNS1_4NodeES4_i@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L214
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L203:
	.cfi_restore_state
	movq	32(%rdi), %rax
	movq	16(%rax), %r8
	leaq	16(%rax), %r14
	cmpq	%r8, %r13
	je	.L205
	movq	%rax, %rdi
	leaq	-24(%rdi), %r12
	testq	%r8, %r8
	jne	.L215
	jmp	.L206
.L214:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23752:
	.size	_ZN2v88internal8compiler16JSBinopReduction41CheckLeftInputToReceiverOrNullOrUndefinedEv, .-_ZN2v88internal8compiler16JSBinopReduction41CheckLeftInputToReceiverOrNullOrUndefinedEv
	.section	.text._ZN2v88internal8compiler16JSBinopReduction21CheckInputsToReceiverEv,"axG",@progbits,_ZN2v88internal8compiler16JSBinopReduction21CheckInputsToReceiverEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler16JSBinopReduction21CheckInputsToReceiverEv
	.type	_ZN2v88internal8compiler16JSBinopReduction21CheckInputsToReceiverEv, @function
_ZN2v88internal8compiler16JSBinopReduction21CheckInputsToReceiverEv:
.LFB23753:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$64, %rsp
	movq	8(%rdi), %rcx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rcx), %edx
	leaq	32(%rcx), %rax
	movq	32(%rcx), %rcx
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L217
	movq	16(%rcx), %rcx
.L217:
	movq	8(%rcx), %rcx
	movq	%rcx, -72(%rbp)
	cmpq	$75431937, %rcx
	jne	.L250
	cmpl	$15, %edx
	je	.L226
.L253:
	addq	$8, %rax
.L227:
	movq	(%rax), %rax
	movq	8(%rax), %rax
	movq	%rax, -72(%rbp)
	cmpq	$75431937, %rax
	jne	.L251
.L216:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L252
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L250:
	.cfi_restore_state
	leaq	-72(%rbp), %rdi
	movl	$75431937, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L248
	movq	(%rbx), %rax
	movq	8(%rbx), %rdi
	xorl	%esi, %esi
	movq	16(%rax), %rax
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	8(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	8(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rax, -96(%rbp)
	movq	(%rbx), %rax
	movq	16(%rax), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder13CheckReceiverEv@PLT
	movq	%r13, %rdi
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	-96(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$3, %edx
	movq	%r12, -48(%rbp)
	movhps	-88(%rbp), %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%rbx), %rdi
	movq	%rax, %r13
	movzbl	23(%rdi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L221
	movq	32(%rdi), %r8
	leaq	32(%rdi), %r14
	cmpq	%r8, %r13
	je	.L223
.L222:
	leaq	-24(%rdi), %r12
	testq	%r8, %r8
	je	.L224
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L224:
	movq	%r13, (%r14)
	testq	%r13, %r13
	je	.L247
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L247:
	movq	8(%rbx), %rdi
.L223:
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler14NodeProperties18ReplaceEffectInputEPNS1_4NodeES4_i@PLT
.L248:
	movq	8(%rbx), %rax
	movzbl	23(%rax), %edx
	addq	$32, %rax
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L253
.L226:
	movq	(%rax), %rax
	addq	$24, %rax
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L251:
	leaq	-72(%rbp), %rdi
	movl	$75431937, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L216
	movq	(%rbx), %rax
	movq	8(%rbx), %rdi
	xorl	%esi, %esi
	movq	16(%rax), %rax
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	8(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	8(%rbx), %rdi
	movl	$1, %esi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rax, -88(%rbp)
	movq	(%rbx), %rax
	movq	16(%rax), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder13CheckReceiverEv@PLT
	movq	%r13, %rdi
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	-88(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$3, %edx
	movq	%r12, -48(%rbp)
	movhps	-96(%rbp), %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%rbx), %rdi
	movq	%rax, %r13
	movzbl	23(%rdi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L231
	movq	40(%rdi), %r8
	leaq	40(%rdi), %r14
	cmpq	%r8, %r13
	je	.L232
.L233:
	leaq	-48(%rdi), %r12
	testq	%r8, %r8
	je	.L234
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L234:
	movq	%r13, (%r14)
	testq	%r13, %r13
	je	.L249
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L249:
	movq	8(%rbx), %rdi
.L232:
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler14NodeProperties18ReplaceEffectInputEPNS1_4NodeES4_i@PLT
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L221:
	movq	32(%rdi), %rax
	movq	16(%rax), %r8
	leaq	16(%rax), %r14
	cmpq	%r8, %r13
	je	.L223
	movq	%rax, %rdi
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L231:
	movq	32(%rdi), %rax
	movq	24(%rax), %r8
	cmpq	%r8, %r13
	je	.L232
	leaq	24(%rax), %r14
	movq	%rax, %rdi
	jmp	.L233
.L252:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23753:
	.size	_ZN2v88internal8compiler16JSBinopReduction21CheckInputsToReceiverEv, .-_ZN2v88internal8compiler16JSBinopReduction21CheckInputsToReceiverEv
	.section	.text._ZN2v88internal8compiler16JSBinopReduction38CheckInputsToReceiverOrNullOrUndefinedEv,"axG",@progbits,_ZN2v88internal8compiler16JSBinopReduction38CheckInputsToReceiverOrNullOrUndefinedEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler16JSBinopReduction38CheckInputsToReceiverOrNullOrUndefinedEv
	.type	_ZN2v88internal8compiler16JSBinopReduction38CheckInputsToReceiverOrNullOrUndefinedEv, @function
_ZN2v88internal8compiler16JSBinopReduction38CheckInputsToReceiverOrNullOrUndefinedEv:
.LFB23754:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$64, %rsp
	movq	8(%rdi), %rcx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rcx), %edx
	leaq	32(%rcx), %rax
	movq	32(%rcx), %rcx
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L255
	movq	16(%rcx), %rcx
.L255:
	movq	8(%rcx), %rcx
	movq	%rcx, -72(%rbp)
	cmpq	$75432321, %rcx
	jne	.L288
	cmpl	$15, %edx
	je	.L264
.L291:
	addq	$8, %rax
.L265:
	movq	(%rax), %rax
	movq	8(%rax), %rax
	movq	%rax, -72(%rbp)
	cmpq	$75432321, %rax
	jne	.L289
.L254:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L290
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L288:
	.cfi_restore_state
	leaq	-72(%rbp), %rdi
	movl	$75432321, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L286
	movq	(%rbx), %rax
	movq	8(%rbx), %rdi
	xorl	%esi, %esi
	movq	16(%rax), %rax
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	8(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	8(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rax, -96(%rbp)
	movq	(%rbx), %rax
	movq	16(%rax), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder30CheckReceiverOrNullOrUndefinedEv@PLT
	movq	%r13, %rdi
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	-96(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$3, %edx
	movq	%r12, -48(%rbp)
	movhps	-88(%rbp), %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%rbx), %rdi
	movq	%rax, %r13
	movzbl	23(%rdi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L259
	movq	32(%rdi), %r8
	leaq	32(%rdi), %r14
	cmpq	%r8, %r13
	je	.L261
.L260:
	leaq	-24(%rdi), %r12
	testq	%r8, %r8
	je	.L262
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L262:
	movq	%r13, (%r14)
	testq	%r13, %r13
	je	.L285
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L285:
	movq	8(%rbx), %rdi
.L261:
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler14NodeProperties18ReplaceEffectInputEPNS1_4NodeES4_i@PLT
.L286:
	movq	8(%rbx), %rax
	movzbl	23(%rax), %edx
	addq	$32, %rax
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L291
.L264:
	movq	(%rax), %rax
	addq	$24, %rax
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L289:
	leaq	-72(%rbp), %rdi
	movl	$75432321, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L254
	movq	(%rbx), %rax
	movq	8(%rbx), %rdi
	xorl	%esi, %esi
	movq	16(%rax), %rax
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	8(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	8(%rbx), %rdi
	movl	$1, %esi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rax, -88(%rbp)
	movq	(%rbx), %rax
	movq	16(%rax), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder30CheckReceiverOrNullOrUndefinedEv@PLT
	movq	%r13, %rdi
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	-88(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$3, %edx
	movq	%r12, -48(%rbp)
	movhps	-96(%rbp), %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%rbx), %rdi
	movq	%rax, %r13
	movzbl	23(%rdi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L269
	movq	40(%rdi), %r8
	leaq	40(%rdi), %r14
	cmpq	%r8, %r13
	je	.L270
.L271:
	leaq	-48(%rdi), %r12
	testq	%r8, %r8
	je	.L272
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L272:
	movq	%r13, (%r14)
	testq	%r13, %r13
	je	.L287
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L287:
	movq	8(%rbx), %rdi
.L270:
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler14NodeProperties18ReplaceEffectInputEPNS1_4NodeES4_i@PLT
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L259:
	movq	32(%rdi), %rax
	movq	16(%rax), %r8
	leaq	16(%rax), %r14
	cmpq	%r8, %r13
	je	.L261
	movq	%rax, %rdi
	jmp	.L260
	.p2align 4,,10
	.p2align 3
.L269:
	movq	32(%rdi), %rax
	movq	24(%rax), %r8
	cmpq	%r8, %r13
	je	.L270
	leaq	24(%rax), %r14
	movq	%rax, %rdi
	jmp	.L271
.L290:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23754:
	.size	_ZN2v88internal8compiler16JSBinopReduction38CheckInputsToReceiverOrNullOrUndefinedEv, .-_ZN2v88internal8compiler16JSBinopReduction38CheckInputsToReceiverOrNullOrUndefinedEv
	.section	.text._ZN2v88internal8compiler16JSBinopReduction22CheckLeftInputToSymbolEv,"axG",@progbits,_ZN2v88internal8compiler16JSBinopReduction22CheckLeftInputToSymbolEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler16JSBinopReduction22CheckLeftInputToSymbolEv
	.type	_ZN2v88internal8compiler16JSBinopReduction22CheckLeftInputToSymbolEv, @function
_ZN2v88internal8compiler16JSBinopReduction22CheckLeftInputToSymbolEv:
.LFB23755:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	8(%rdi), %rdi
	movq	16(%rax), %rax
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	8(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	8(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rax, -72(%rbp)
	movq	(%rbx), %rax
	movq	16(%rax), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder11CheckSymbolEv@PLT
	movq	%r13, %rdi
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	-72(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$3, %edx
	movq	%r12, -48(%rbp)
	movhps	-80(%rbp), %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%rbx), %rdi
	movq	%rax, %r13
	movzbl	23(%rdi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L293
	movq	32(%rdi), %r8
	leaq	32(%rdi), %r14
	cmpq	%r8, %r13
	je	.L295
	leaq	-24(%rdi), %r12
	testq	%r8, %r8
	je	.L296
.L305:
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L296:
	movq	%r13, (%r14)
	testq	%r13, %r13
	je	.L303
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L303:
	movq	8(%rbx), %rdi
.L295:
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler14NodeProperties18ReplaceEffectInputEPNS1_4NodeES4_i@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L304
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L293:
	.cfi_restore_state
	movq	32(%rdi), %rax
	movq	16(%rax), %r8
	leaq	16(%rax), %r14
	cmpq	%r8, %r13
	je	.L295
	movq	%rax, %rdi
	leaq	-24(%rdi), %r12
	testq	%r8, %r8
	jne	.L305
	jmp	.L296
.L304:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23755:
	.size	_ZN2v88internal8compiler16JSBinopReduction22CheckLeftInputToSymbolEv, .-_ZN2v88internal8compiler16JSBinopReduction22CheckLeftInputToSymbolEv
	.section	.text._ZN2v88internal8compiler16JSBinopReduction19CheckInputsToSymbolEv,"axG",@progbits,_ZN2v88internal8compiler16JSBinopReduction19CheckInputsToSymbolEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler16JSBinopReduction19CheckInputsToSymbolEv
	.type	_ZN2v88internal8compiler16JSBinopReduction19CheckInputsToSymbolEv, @function
_ZN2v88internal8compiler16JSBinopReduction19CheckInputsToSymbolEv:
.LFB23756:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$64, %rsp
	movq	8(%rdi), %rcx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rcx), %edx
	leaq	32(%rcx), %rax
	movq	32(%rcx), %rcx
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L307
	movq	16(%rcx), %rcx
.L307:
	movq	8(%rcx), %rcx
	movq	%rcx, -72(%rbp)
	cmpq	$8193, %rcx
	jne	.L340
	cmpl	$15, %edx
	je	.L316
.L343:
	addq	$8, %rax
.L317:
	movq	(%rax), %rax
	movq	8(%rax), %rax
	movq	%rax, -72(%rbp)
	cmpq	$8193, %rax
	jne	.L341
.L306:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L342
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L340:
	.cfi_restore_state
	leaq	-72(%rbp), %rdi
	movl	$8193, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L338
	movq	(%rbx), %rax
	movq	8(%rbx), %rdi
	xorl	%esi, %esi
	movq	16(%rax), %rax
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	8(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	8(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rax, -96(%rbp)
	movq	(%rbx), %rax
	movq	16(%rax), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder11CheckSymbolEv@PLT
	movq	%r13, %rdi
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	-96(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$3, %edx
	movq	%r12, -48(%rbp)
	movhps	-88(%rbp), %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%rbx), %rdi
	movq	%rax, %r13
	movzbl	23(%rdi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L311
	movq	32(%rdi), %r8
	leaq	32(%rdi), %r14
	cmpq	%r8, %r13
	je	.L313
.L312:
	leaq	-24(%rdi), %r12
	testq	%r8, %r8
	je	.L314
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L314:
	movq	%r13, (%r14)
	testq	%r13, %r13
	je	.L337
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L337:
	movq	8(%rbx), %rdi
.L313:
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler14NodeProperties18ReplaceEffectInputEPNS1_4NodeES4_i@PLT
.L338:
	movq	8(%rbx), %rax
	movzbl	23(%rax), %edx
	addq	$32, %rax
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L343
.L316:
	movq	(%rax), %rax
	addq	$24, %rax
	jmp	.L317
	.p2align 4,,10
	.p2align 3
.L341:
	leaq	-72(%rbp), %rdi
	movl	$8193, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L306
	movq	(%rbx), %rax
	movq	8(%rbx), %rdi
	xorl	%esi, %esi
	movq	16(%rax), %rax
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	8(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	8(%rbx), %rdi
	movl	$1, %esi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rax, -88(%rbp)
	movq	(%rbx), %rax
	movq	16(%rax), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder11CheckSymbolEv@PLT
	movq	%r13, %rdi
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	-88(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$3, %edx
	movq	%r12, -48(%rbp)
	movhps	-96(%rbp), %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%rbx), %rdi
	movq	%rax, %r13
	movzbl	23(%rdi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L321
	movq	40(%rdi), %r8
	leaq	40(%rdi), %r14
	cmpq	%r8, %r13
	je	.L322
.L323:
	leaq	-48(%rdi), %r12
	testq	%r8, %r8
	je	.L324
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L324:
	movq	%r13, (%r14)
	testq	%r13, %r13
	je	.L339
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L339:
	movq	8(%rbx), %rdi
.L322:
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler14NodeProperties18ReplaceEffectInputEPNS1_4NodeES4_i@PLT
	jmp	.L306
	.p2align 4,,10
	.p2align 3
.L311:
	movq	32(%rdi), %rax
	movq	16(%rax), %r8
	leaq	16(%rax), %r14
	cmpq	%r8, %r13
	je	.L313
	movq	%rax, %rdi
	jmp	.L312
	.p2align 4,,10
	.p2align 3
.L321:
	movq	32(%rdi), %rax
	movq	24(%rax), %r8
	cmpq	%r8, %r13
	je	.L322
	leaq	24(%rax), %r14
	movq	%rax, %rdi
	jmp	.L323
.L342:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23756:
	.size	_ZN2v88internal8compiler16JSBinopReduction19CheckInputsToSymbolEv, .-_ZN2v88internal8compiler16JSBinopReduction19CheckInputsToSymbolEv
	.section	.text._ZN2v88internal8compiler16JSBinopReduction19CheckInputsToStringEv,"axG",@progbits,_ZN2v88internal8compiler16JSBinopReduction19CheckInputsToStringEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler16JSBinopReduction19CheckInputsToStringEv
	.type	_ZN2v88internal8compiler16JSBinopReduction19CheckInputsToStringEv, @function
_ZN2v88internal8compiler16JSBinopReduction19CheckInputsToStringEv:
.LFB23757:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$64, %rsp
	movq	8(%rdi), %rcx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rcx), %edx
	leaq	32(%rcx), %rax
	movq	32(%rcx), %rcx
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L345
	movq	16(%rcx), %rcx
.L345:
	movq	8(%rcx), %rcx
	movq	%rcx, -80(%rbp)
	cmpq	$16417, %rcx
	jne	.L378
	cmpl	$15, %edx
	je	.L354
.L381:
	addq	$8, %rax
.L355:
	movq	(%rax), %rax
	movq	8(%rax), %rax
	movq	%rax, -80(%rbp)
	cmpq	$16417, %rax
	jne	.L379
.L344:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L380
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L378:
	.cfi_restore_state
	leaq	-80(%rbp), %r12
	movl	$16417, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L376
	movq	(%rbx), %rax
	movq	8(%rbx), %rdi
	xorl	%esi, %esi
	movq	16(%rax), %rax
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	8(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	8(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%r12, %rsi
	movq	%rax, -88(%rbp)
	movq	(%rbx), %rax
	movq	16(%rax), %rax
	movq	376(%rax), %rdi
	movq	$0, -80(%rbp)
	movl	$-1, -72(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder11CheckStringERKNS1_14FeedbackSourceE@PLT
	movq	%r14, %rdi
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	-88(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$3, %edx
	movq	%r13, -48(%rbp)
	movhps	-96(%rbp), %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%rbx), %rdi
	movq	%rax, %r13
	movzbl	23(%rdi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L349
	movq	32(%rdi), %r8
	leaq	32(%rdi), %r14
	cmpq	%r8, %r13
	je	.L351
.L350:
	leaq	-24(%rdi), %r12
	testq	%r8, %r8
	je	.L352
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L352:
	movq	%r13, (%r14)
	testq	%r13, %r13
	je	.L375
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L375:
	movq	8(%rbx), %rdi
.L351:
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler14NodeProperties18ReplaceEffectInputEPNS1_4NodeES4_i@PLT
.L376:
	movq	8(%rbx), %rax
	movzbl	23(%rax), %edx
	addq	$32, %rax
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L381
.L354:
	movq	(%rax), %rax
	addq	$24, %rax
	jmp	.L355
	.p2align 4,,10
	.p2align 3
.L379:
	leaq	-80(%rbp), %r12
	movl	$16417, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L344
	movq	(%rbx), %rax
	movq	8(%rbx), %rdi
	xorl	%esi, %esi
	movq	16(%rax), %rax
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	8(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	8(%rbx), %rdi
	movl	$1, %esi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%r12, %rsi
	movq	%rax, -88(%rbp)
	movq	(%rbx), %rax
	movq	16(%rax), %rax
	movq	376(%rax), %rdi
	movq	$0, -80(%rbp)
	movl	$-1, -72(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder11CheckStringERKNS1_14FeedbackSourceE@PLT
	movq	%r14, %rdi
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	-88(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$3, %edx
	movq	%r13, -48(%rbp)
	movhps	-96(%rbp), %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%rbx), %rdi
	movq	%rax, %r13
	movzbl	23(%rdi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L359
	movq	40(%rdi), %r8
	leaq	40(%rdi), %r14
	cmpq	%r8, %r13
	je	.L360
.L361:
	leaq	-48(%rdi), %r12
	testq	%r8, %r8
	je	.L362
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L362:
	movq	%r13, (%r14)
	testq	%r13, %r13
	je	.L377
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L377:
	movq	8(%rbx), %rdi
.L360:
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler14NodeProperties18ReplaceEffectInputEPNS1_4NodeES4_i@PLT
	jmp	.L344
	.p2align 4,,10
	.p2align 3
.L349:
	movq	32(%rdi), %rax
	movq	16(%rax), %r8
	leaq	16(%rax), %r14
	cmpq	%r8, %r13
	je	.L351
	movq	%rax, %rdi
	jmp	.L350
	.p2align 4,,10
	.p2align 3
.L359:
	movq	32(%rdi), %rax
	movq	24(%rax), %r8
	cmpq	%r8, %r13
	je	.L360
	leaq	24(%rax), %r14
	movq	%rax, %rdi
	jmp	.L361
.L380:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23757:
	.size	_ZN2v88internal8compiler16JSBinopReduction19CheckInputsToStringEv, .-_ZN2v88internal8compiler16JSBinopReduction19CheckInputsToStringEv
	.section	.text._ZN2v88internal8compiler16JSBinopReduction31CheckInputsToInternalizedStringEv,"axG",@progbits,_ZN2v88internal8compiler16JSBinopReduction31CheckInputsToInternalizedStringEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler16JSBinopReduction31CheckInputsToInternalizedStringEv
	.type	_ZN2v88internal8compiler16JSBinopReduction31CheckInputsToInternalizedStringEv, @function
_ZN2v88internal8compiler16JSBinopReduction31CheckInputsToInternalizedStringEv:
.LFB23758:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$64, %rsp
	movq	8(%rdi), %rcx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rcx), %edx
	leaq	32(%rcx), %rax
	movq	32(%rcx), %rcx
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L383
	movq	16(%rcx), %rcx
.L383:
	movq	8(%rcx), %rcx
	movq	%rcx, -72(%rbp)
	cmpq	$24577, %rcx
	jne	.L416
	cmpl	$15, %edx
	je	.L392
.L419:
	addq	$8, %rax
.L393:
	movq	(%rax), %rax
	movq	8(%rax), %rax
	movq	%rax, -72(%rbp)
	cmpq	$24577, %rax
	jne	.L417
.L382:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L418
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L416:
	.cfi_restore_state
	leaq	-72(%rbp), %rdi
	movl	$24577, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L414
	movq	(%rbx), %rax
	movq	8(%rbx), %rdi
	xorl	%esi, %esi
	movq	16(%rax), %rax
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	8(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	8(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rax, -88(%rbp)
	movq	(%rbx), %rax
	movq	16(%rax), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder23CheckInternalizedStringEv@PLT
	movq	%r13, %rdi
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	-88(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$3, %edx
	movq	%r12, -48(%rbp)
	movhps	-96(%rbp), %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%rbx), %rdi
	movq	%rax, %r13
	movzbl	23(%rdi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L387
	movq	32(%rdi), %r8
	leaq	32(%rdi), %r14
	cmpq	%r8, %r13
	je	.L389
.L388:
	leaq	-24(%rdi), %r12
	testq	%r8, %r8
	je	.L390
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L390:
	movq	%r13, (%r14)
	testq	%r13, %r13
	je	.L413
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L413:
	movq	8(%rbx), %rdi
.L389:
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler14NodeProperties18ReplaceEffectInputEPNS1_4NodeES4_i@PLT
.L414:
	movq	8(%rbx), %rax
	movzbl	23(%rax), %edx
	addq	$32, %rax
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L419
.L392:
	movq	(%rax), %rax
	addq	$24, %rax
	jmp	.L393
	.p2align 4,,10
	.p2align 3
.L417:
	leaq	-72(%rbp), %rdi
	movl	$24577, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L382
	movq	(%rbx), %rax
	movq	8(%rbx), %rdi
	xorl	%esi, %esi
	movq	16(%rax), %rax
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	8(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	8(%rbx), %rdi
	movl	$1, %esi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rax, -88(%rbp)
	movq	(%rbx), %rax
	movq	16(%rax), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder23CheckInternalizedStringEv@PLT
	movq	%r13, %rdi
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	-88(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$3, %edx
	movq	%r12, -48(%rbp)
	movhps	-96(%rbp), %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%rbx), %rdi
	movq	%rax, %r13
	movzbl	23(%rdi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L397
	movq	40(%rdi), %r8
	leaq	40(%rdi), %r14
	cmpq	%r8, %r13
	je	.L398
.L399:
	leaq	-48(%rdi), %r12
	testq	%r8, %r8
	je	.L400
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L400:
	movq	%r13, (%r14)
	testq	%r13, %r13
	je	.L415
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L415:
	movq	8(%rbx), %rdi
.L398:
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler14NodeProperties18ReplaceEffectInputEPNS1_4NodeES4_i@PLT
	jmp	.L382
	.p2align 4,,10
	.p2align 3
.L387:
	movq	32(%rdi), %rax
	movq	16(%rax), %r8
	leaq	16(%rax), %r14
	cmpq	%r8, %r13
	je	.L389
	movq	%rax, %rdi
	jmp	.L388
	.p2align 4,,10
	.p2align 3
.L397:
	movq	32(%rdi), %rax
	movq	24(%rax), %r8
	cmpq	%r8, %r13
	je	.L398
	leaq	24(%rax), %r14
	movq	%rax, %rdi
	jmp	.L399
.L418:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23758:
	.size	_ZN2v88internal8compiler16JSBinopReduction31CheckInputsToInternalizedStringEv, .-_ZN2v88internal8compiler16JSBinopReduction31CheckInputsToInternalizedStringEv
	.section	.text._ZN2v88internal8compiler16JSBinopReduction20ChangeToPureOperatorEPKNS1_8OperatorENS1_4TypeE,"axG",@progbits,_ZN2v88internal8compiler16JSBinopReduction20ChangeToPureOperatorEPKNS1_8OperatorENS1_4TypeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler16JSBinopReduction20ChangeToPureOperatorEPKNS1_8OperatorENS1_4TypeE
	.type	_ZN2v88internal8compiler16JSBinopReduction20ChangeToPureOperatorEPKNS1_8OperatorENS1_4TypeE, @function
_ZN2v88internal8compiler16JSBinopReduction20ChangeToPureOperatorEPKNS1_8OperatorENS1_4TypeE:
.LFB23762:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rdi
	movq	(%rdi), %rax
	movl	24(%rax), %eax
	testl	%eax, %eax
	jle	.L421
	movq	(%rbx), %rax
	movq	%rdi, %rdx
	movq	%rdi, %rsi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	8(%rax), %r9
	movq	(%r9), %rax
	movq	%r9, %rdi
	call	*32(%rax)
	movq	8(%rbx), %rdi
.L421:
	call	_ZN2v88internal8compiler14NodeProperties20RemoveNonValueInputsEPNS1_4NodeE@PLT
	movq	8(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	8(%rbx), %rax
	movq	%r12, %rsi
	movq	8(%rax), %rdi
	movq	(%rbx), %rax
	movq	16(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdx
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	8(%rbx), %rdx
	movq	%rax, 8(%rdx)
	movq	8(%rbx), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23762:
	.size	_ZN2v88internal8compiler16JSBinopReduction20ChangeToPureOperatorEPKNS1_8OperatorENS1_4TypeE, .-_ZN2v88internal8compiler16JSBinopReduction20ChangeToPureOperatorEPKNS1_8OperatorENS1_4TypeE
	.section	.text._ZN2v88internal8compiler16JSBinopReduction10OneInputIsENS1_4TypeE,"axG",@progbits,_ZN2v88internal8compiler16JSBinopReduction10OneInputIsENS1_4TypeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler16JSBinopReduction10OneInputIsENS1_4TypeE
	.type	_ZN2v88internal8compiler16JSBinopReduction10OneInputIsENS1_4TypeE, @function
_ZN2v88internal8compiler16JSBinopReduction10OneInputIsENS1_4TypeE:
.LFB23767:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	32(%rax), %rdx
	movzbl	23(%rax), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L424
	movq	16(%rdx), %rdx
.L424:
	movq	8(%rdx), %rax
	movq	%rax, -48(%rbp)
	cmpq	%r12, %rax
	jne	.L425
.L430:
	movl	$1, %eax
.L423:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L437
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L425:
	.cfi_restore_state
	leaq	-48(%rbp), %r13
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L430
	movq	8(%rbx), %rax
	movzbl	23(%rax), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L428
	addq	$40, %rax
.L429:
	movq	(%rax), %rax
	movq	8(%rax), %rax
	movq	%rax, -48(%rbp)
	cmpq	%r12, %rax
	je	.L430
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L423
	jmp	.L430
	.p2align 4,,10
	.p2align 3
.L428:
	movq	32(%rax), %rax
	addq	$24, %rax
	jmp	.L429
.L437:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23767:
	.size	_ZN2v88internal8compiler16JSBinopReduction10OneInputIsENS1_4TypeE, .-_ZN2v88internal8compiler16JSBinopReduction10OneInputIsENS1_4TypeE
	.section	.text._ZN2v88internal8compiler16JSBinopReduction13BothInputsAreENS1_4TypeE,"axG",@progbits,_ZN2v88internal8compiler16JSBinopReduction13BothInputsAreENS1_4TypeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler16JSBinopReduction13BothInputsAreENS1_4TypeE
	.type	_ZN2v88internal8compiler16JSBinopReduction13BothInputsAreENS1_4TypeE, @function
_ZN2v88internal8compiler16JSBinopReduction13BothInputsAreENS1_4TypeE:
.LFB23768:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	8(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rdx), %eax
	leaq	32(%rdx), %rcx
	movq	32(%rdx), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L439
	movq	16(%rdx), %rdx
.L439:
	movq	8(%rdx), %rdx
	movq	%rdx, -32(%rbp)
	cmpq	%rdx, %r12
	jne	.L456
	cmpl	$15, %eax
	je	.L444
.L459:
	movq	8(%rcx), %rax
	addq	$8, %rcx
	movq	8(%rax), %rax
	movq	%rax, -32(%rbp)
	cmpq	%rax, %r12
	jne	.L457
.L446:
	movl	$1, %eax
.L438:
	movq	-24(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L458
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L456:
	.cfi_restore_state
	leaq	-32(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L438
	movq	8(%rbx), %rcx
	movzbl	23(%rcx), %eax
	addq	$32, %rcx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L459
.L444:
	movq	(%rcx), %rcx
	movq	24(%rcx), %rax
	addq	$24, %rcx
	movq	8(%rax), %rax
	movq	%rax, -32(%rbp)
	cmpq	%rax, %r12
	je	.L446
.L457:
	leaq	-32(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L438
	jmp	.L446
.L458:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23768:
	.size	_ZN2v88internal8compiler16JSBinopReduction13BothInputsAreENS1_4TypeE, .-_ZN2v88internal8compiler16JSBinopReduction13BothInputsAreENS1_4TypeE
	.section	.text._ZN2v88internal8compiler16JSBinopReduction15BothInputsMaybeENS1_4TypeE,"axG",@progbits,_ZN2v88internal8compiler16JSBinopReduction15BothInputsMaybeENS1_4TypeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler16JSBinopReduction15BothInputsMaybeENS1_4TypeE
	.type	_ZN2v88internal8compiler16JSBinopReduction15BothInputsMaybeENS1_4TypeE, @function
_ZN2v88internal8compiler16JSBinopReduction15BothInputsMaybeENS1_4TypeE:
.LFB23769:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	32(%rax), %rdx
	movzbl	23(%rax), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L461
	movq	16(%rdx), %rdx
.L461:
	movq	8(%rdx), %rax
	leaq	-40(%rbp), %rdi
	movq	%r12, %rsi
	movq	%rax, -40(%rbp)
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	je	.L460
	movq	8(%rbx), %rax
	movzbl	23(%rax), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L463
	addq	$40, %rax
.L464:
	movq	(%rax), %rax
	leaq	-32(%rbp), %rdi
	movq	%r12, %rsi
	movq	8(%rax), %rax
	movq	%rax, -32(%rbp)
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
.L460:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L470
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L463:
	.cfi_restore_state
	movq	32(%rax), %rax
	addq	$24, %rax
	jmp	.L464
.L470:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23769:
	.size	_ZN2v88internal8compiler16JSBinopReduction15BothInputsMaybeENS1_4TypeE, .-_ZN2v88internal8compiler16JSBinopReduction15BothInputsMaybeENS1_4TypeE
	.section	.text._ZN2v88internal8compiler15JSTypedLoweringC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15JSTypedLoweringC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerEPNS0_4ZoneE
	.type	_ZN2v88internal8compiler15JSTypedLoweringC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerEPNS0_4ZoneE, @function
_ZN2v88internal8compiler15JSTypedLoweringC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerEPNS0_4ZoneE:
.LFB23795:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal8compiler15JSTypedLoweringE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rcx, %rdi
	movq	%rsi, 8(%rbx)
	movq	360(%rdx), %rsi
	movq	%rax, (%rbx)
	movq	(%rdx), %rax
	movq	%rcx, 24(%rbx)
	subq	$-128, %rsi
	movq	(%rax), %r8
	movq	%rdx, 16(%rbx)
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler4Type12HeapConstantEPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEPNS0_4ZoneE@PLT
	movl	$75440129, %edi
	movq	%rax, 32(%rbx)
	movq	%rax, %rsi
	movq	16(%rbx), %rax
	movq	(%rax), %rax
	movq	(%rax), %r12
	movq	%r12, %rdx
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%r12, %rdx
	movl	$8389505, %edi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, 40(%rbx)
	call	_ZN2v88internal8compiler9TypeCache3GetEv@PLT
	movq	%rax, 48(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23795:
	.size	_ZN2v88internal8compiler15JSTypedLoweringC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerEPNS0_4ZoneE, .-_ZN2v88internal8compiler15JSTypedLoweringC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerEPNS0_4ZoneE
	.globl	_ZN2v88internal8compiler15JSTypedLoweringC1EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerEPNS0_4ZoneE
	.set	_ZN2v88internal8compiler15JSTypedLoweringC1EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerEPNS0_4ZoneE,_ZN2v88internal8compiler15JSTypedLoweringC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerEPNS0_4ZoneE
	.section	.text._ZN2v88internal8compiler15JSTypedLowering13ReduceJSEqualEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15JSTypedLowering13ReduceJSEqualEPNS1_4NodeE
	.type	_ZN2v88internal8compiler15JSTypedLowering13ReduceJSEqualEPNS1_4NodeE, @function
_ZN2v88internal8compiler15JSTypedLowering13ReduceJSEqualEPNS1_4NodeE:
.LFB23812:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %xmm0
	movq	%rsi, %xmm1
	leaq	32(%rsi), %rdx
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	32(%rsi), %rcx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	movaps	%xmm0, -96(%rbp)
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L474
	movq	16(%rcx), %rcx
.L474:
	movq	8(%rcx), %rcx
	movq	%rcx, -104(%rbp)
	cmpq	$24577, %rcx
	jne	.L564
	cmpl	$15, %eax
	je	.L479
.L567:
	addq	$8, %rdx
.L480:
	movq	(%rdx), %rax
	movq	8(%rax), %rax
	movq	%rax, -104(%rbp)
	cmpq	$24577, %rax
	jne	.L565
.L563:
	movq	16(%rbx), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14ReferenceEqualEv@PLT
	movq	-88(%rbp), %rdi
	movq	%rax, %r12
	movq	(%rdi), %rax
	movl	24(%rax), %eax
	testl	%eax, %eax
	jle	.L487
	movq	-96(%rbp), %rax
	movq	%rdi, %rdx
	movq	%rdi, %rsi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	8(%rax), %r9
	movq	(%r9), %rax
	movq	%r9, %rdi
	call	*32(%rax)
	movq	-88(%rbp), %rdi
.L487:
	call	_ZN2v88internal8compiler14NodeProperties20RemoveNonValueInputsEPNS1_4NodeE@PLT
	movq	-88(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	-88(%rbp), %rax
	movl	$4294967295, %esi
	movq	8(%rax), %rdi
	movq	-96(%rbp), %rax
	movq	16(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdx
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	-88(%rbp), %rdx
	movq	%rax, 8(%rdx)
	movq	-88(%rbp), %rax
.L485:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L566
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L564:
	.cfi_restore_state
	leaq	-104(%rbp), %r13
	movl	$24577, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L478
	movq	-88(%rbp), %rdx
	movzbl	23(%rdx), %eax
	addq	$32, %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L567
.L479:
	movq	(%rdx), %rdx
	addq	$24, %rdx
	jmp	.L480
	.p2align 4,,10
	.p2align 3
.L565:
	leaq	-104(%rbp), %r13
	movl	$24577, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L563
.L478:
	movq	-88(%rbp), %rax
	leaq	-96(%rbp), %r14
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler22CompareOperationHintOfEPKNS1_8OperatorE@PLT
	cmpb	$4, %al
	je	.L568
.L486:
	movq	-88(%rbp), %rdx
	movzbl	23(%rdx), %eax
	leaq	32(%rdx), %rcx
	movq	32(%rdx), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L569
.L488:
	movq	8(%rdx), %rdx
	movq	%rdx, -104(%rbp)
	cmpq	$16417, %rdx
	jne	.L570
.L491:
	cmpl	$15, %eax
	je	.L493
	addq	$8, %rcx
.L494:
	movq	(%rcx), %rax
	movq	8(%rax), %rax
	movq	%rax, -104(%rbp)
	cmpq	$16417, %rax
	jne	.L571
.L558:
	movq	16(%rbx), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder11StringEqualEv@PLT
	movl	$4294967295, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler16JSBinopReduction20ChangeToPureOperatorEPKNS1_8OperatorENS1_4TypeE
	jmp	.L485
	.p2align 4,,10
	.p2align 3
.L570:
	movl	$16417, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L492
	movq	-88(%rbp), %rcx
	movzbl	23(%rcx), %eax
	addq	$32, %rcx
	andl	$15, %eax
	jmp	.L491
	.p2align 4,,10
	.p2align 3
.L571:
	movl	$16417, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L558
.L492:
	movl	$513, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler16JSBinopReduction13BothInputsAreENS1_4TypeE
	testb	%al, %al
	jne	.L559
	movl	$75431937, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler16JSBinopReduction13BothInputsAreENS1_4TypeE
	testb	%al, %al
	jne	.L559
	movq	-88(%rbp), %rax
	movq	32(%rax), %rdx
	movzbl	23(%rax), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L499
	movq	16(%rdx), %rdx
.L499:
	movq	8(%rdx), %rax
	movq	%rax, -104(%rbp)
	cmpq	$262529, %rax
	je	.L502
	movl	$262529, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L502
	movq	-88(%rbp), %rax
	movzbl	23(%rax), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L503
	addq	$40, %rax
.L504:
	movq	(%rax), %rax
	movq	8(%rax), %rax
	movq	%rax, -104(%rbp)
	cmpq	$262529, %rax
	je	.L502
	movl	$262529, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L502
	movl	$1099, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler16JSBinopReduction13BothInputsAreENS1_4TypeE
	testb	%al, %al
	jne	.L560
	movl	$1031, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler16JSBinopReduction13BothInputsAreENS1_4TypeE
	testb	%al, %al
	je	.L572
.L560:
	movq	16(%rbx), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder11NumberEqualEv@PLT
	movl	$4294967295, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler16JSBinopReduction20ChangeToPureOperatorEPKNS1_8OperatorENS1_4TypeE
	jmp	.L485
.L572:
	movl	$7263, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler16JSBinopReduction13BothInputsAreENS1_4TypeE
	testb	%al, %al
	jne	.L560
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler16JSBinopReduction26IsReceiverCompareOperationEv
	movq	%r14, %rdi
	testb	%al, %al
	je	.L515
	call	_ZN2v88internal8compiler16JSBinopReduction21CheckInputsToReceiverEv
	.p2align 4,,10
	.p2align 3
.L559:
	movq	16(%rbx), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14ReferenceEqualEv@PLT
	movl	$4294967295, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler16JSBinopReduction20ChangeToPureOperatorEPKNS1_8OperatorENS1_4TypeE
	jmp	.L485
	.p2align 4,,10
	.p2align 3
.L568:
	movl	$16385, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler16JSBinopReduction15BothInputsMaybeENS1_4TypeE
	testb	%al, %al
	je	.L486
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler16JSBinopReduction31CheckInputsToInternalizedStringEv
	jmp	.L563
	.p2align 4,,10
	.p2align 3
.L569:
	movq	16(%rdx), %rdx
	jmp	.L488
	.p2align 4,,10
	.p2align 3
.L493:
	movq	(%rcx), %rcx
	addq	$24, %rcx
	jmp	.L494
	.p2align 4,,10
	.p2align 3
.L502:
	movq	8(%rbx), %rdi
	movq	%r12, %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	-88(%rbp), %rax
	movzbl	23(%rax), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L508
	addq	$32, %rax
.L509:
	movq	(%rax), %rax
	movq	8(%rax), %rax
	movq	%rax, -104(%rbp)
	cmpq	$262529, %rax
	je	.L510
	movl	$262529, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	movl	$1, %esi
	testb	%al, %al
	je	.L512
.L510:
	xorl	%esi, %esi
.L512:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node11RemoveInputEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node14TrimInputCountEi@PLT
	movq	16(%rbx), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder20ObjectIsUndetectableEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	%r12, %rax
	jmp	.L485
.L508:
	movq	32(%rax), %rax
	addq	$16, %rax
	jmp	.L509
.L503:
	movq	32(%rax), %rax
	addq	$24, %rax
	jmp	.L504
.L566:
	call	__stack_chk_fail@PLT
.L515:
	call	_ZN2v88internal8compiler16JSBinopReduction43IsReceiverOrNullOrUndefinedCompareOperationEv
	movq	%r14, %rdi
	testb	%al, %al
	je	.L516
	call	_ZN2v88internal8compiler16JSBinopReduction38CheckInputsToReceiverOrNullOrUndefinedEv
	movl	$75169793, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler16JSBinopReduction10OneInputIsENS1_4TypeE
	testb	%al, %al
	jne	.L559
	movq	-88(%rbp), %rdi
	xorl	%esi, %esi
	leaq	-80(%rbp), %r13
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	-88(%rbp), %rdi
	movl	$1, %esi
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	-88(%rbp), %rdi
	xorl	%esi, %esi
	movq	%rax, -120(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	-88(%rbp), %rdi
	xorl	%esi, %esi
	movq	%rax, -160(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, -144(%rbp)
	movq	16(%rbx), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder20ObjectIsUndetectableEv@PLT
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movq	%rax, %rsi
	movq	-128(%rbp), %rax
	movl	$1, %edx
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$1, %edx
	movl	$2, %esi
	movq	%rax, -136(%rbp)
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$2, %edx
	movq	-136(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movhps	-144(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r15
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r14, %rdi
	movq	%r15, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -152(%rbp)
	movq	16(%rbx), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder20ObjectIsUndetectableEv@PLT
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%rax, %rsi
	movq	-120(%rbp), %rax
	movq	%r14, %rdi
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -144(%rbp)
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r14, %rdi
	movq	%r15, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -136(%rbp)
	movq	16(%rbx), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14ReferenceEqualEv@PLT
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$2, %edx
	movq	-128(%rbp), %xmm0
	movq	%r15, %rdi
	movq	%rax, %rsi
	movhps	-120(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$2, %esi
	movq	%rax, -120(%rbp)
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movq	-152(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movhps	-136(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$2, %edx
	movl	$8, %esi
	movq	%rax, %r14
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$3, %edx
	movq	-144(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movq	%r14, -64(%rbp)
	movhps	-120(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%rbx), %rdi
	movq	%r14, %r8
	movq	%r12, %rsi
	movq	%rax, %r13
	movq	-160(%rbp), %r9
	movq	(%rdi), %rax
	movq	%r13, %rdx
	movq	%r9, %rcx
	call	*32(%rax)
	movq	%r13, %rax
	jmp	.L485
.L516:
	call	_ZN2v88internal8compiler16JSBinopReduction24IsStringCompareOperationEv
	movq	%r14, %rdi
	testb	%al, %al
	je	.L518
	call	_ZN2v88internal8compiler16JSBinopReduction19CheckInputsToStringEv
	jmp	.L558
.L518:
	call	_ZN2v88internal8compiler16JSBinopReduction24IsSymbolCompareOperationEv
	movl	%eax, %r8d
	xorl	%eax, %eax
	testb	%r8b, %r8b
	je	.L485
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler16JSBinopReduction19CheckInputsToSymbolEv
	jmp	.L559
	.cfi_endproc
.LFE23812:
	.size	_ZN2v88internal8compiler15JSTypedLowering13ReduceJSEqualEPNS1_4NodeE, .-_ZN2v88internal8compiler15JSTypedLowering13ReduceJSEqualEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler15JSTypedLowering19ReduceJSStrictEqualEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15JSTypedLowering19ReduceJSStrictEqualEPNS1_4NodeE
	.type	_ZN2v88internal8compiler15JSTypedLowering19ReduceJSStrictEqualEPNS1_4NodeE, @function
_ZN2v88internal8compiler15JSTypedLowering19ReduceJSStrictEqualEPNS1_4NodeE:
.LFB23813:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %xmm1
	movq	%rdi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	-72(%rbp), %rdi
	movl	$1, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	cmpq	%rax, %r13
	je	.L652
	movq	-72(%rbp), %rax
	movq	32(%rax), %rdx
	movzbl	23(%rax), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L653
.L576:
	movq	8(%rdx), %rax
	leaq	-88(%rbp), %r13
	movl	$134241407, %esi
	movq	%r13, %rdi
	movq	%rax, -88(%rbp)
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	je	.L582
	movq	-72(%rbp), %rax
	movzbl	23(%rax), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L580
	addq	$40, %rax
.L581:
	movq	(%rax), %rax
	leaq	-96(%rbp), %rdi
	movl	$134241407, %esi
	movq	8(%rax), %rax
	movq	%rax, -96(%rbp)
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L583
.L582:
	movq	-72(%rbp), %rax
	movzbl	23(%rax), %edx
	movq	32(%rax), %rcx
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L654
	movq	8(%rcx), %rdx
	addq	$40, %rax
	movq	%rdx, -88(%rbp)
.L584:
	movq	(%rax), %rax
	movq	%r13, %rdi
	movq	8(%rax), %rsi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	je	.L655
.L583:
	movq	-72(%rbp), %rdx
	movzbl	23(%rdx), %eax
	leaq	32(%rdx), %rcx
	movq	32(%rdx), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L585
	movq	16(%rdx), %rdx
.L585:
	movq	8(%rdx), %rdx
	movq	%rdx, -88(%rbp)
	cmpq	$75457409, %rdx
	jne	.L656
.L588:
	cmpl	$15, %eax
	je	.L590
	addq	$8, %rcx
.L591:
	movq	(%rcx), %rax
	movq	8(%rax), %rax
	movq	%rax, -88(%rbp)
	cmpq	$75457409, %rax
	je	.L592
	movl	$75457409, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L589
.L592:
	movq	16(%rbx), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14ReferenceEqualEv@PLT
	movq	-72(%rbp), %rdi
	movq	%rax, %r12
	movq	(%rdi), %rax
	movl	24(%rax), %eax
	testl	%eax, %eax
	jle	.L595
	movq	-80(%rbp), %rax
	movq	%rdi, %rdx
	movq	%rdi, %rsi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	8(%rax), %r9
	movq	(%r9), %rax
	movq	%r9, %rdi
	call	*32(%rax)
	movq	-72(%rbp), %rdi
.L595:
	call	_ZN2v88internal8compiler14NodeProperties20RemoveNonValueInputsEPNS1_4NodeE@PLT
	movq	-72(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	-72(%rbp), %rax
	movl	$4294967295, %esi
	movq	8(%rax), %rdi
	movq	-80(%rbp), %rax
	movq	16(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdx
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	-72(%rbp), %rdx
	movq	%rax, 8(%rdx)
	movq	-72(%rbp), %rax
	jmp	.L575
	.p2align 4,,10
	.p2align 3
.L653:
	movq	16(%rdx), %rdx
	jmp	.L576
	.p2align 4,,10
	.p2align 3
.L652:
	movq	16(%rbx), %rax
	movq	-72(%rbp), %rdi
	xorl	%esi, %esi
	leaq	-64(%rbp), %r15
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rax, %r13
	movq	16(%rbx), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder11ObjectIsNaNEv@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r14, %rdi
	movq	%r13, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r13
	movq	16(%rbx), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10BooleanNotEv@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r14, %rdi
	movq	%r13, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
.L649:
	movq	8(%rbx), %rdi
	movq	%rax, %r13
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r13, %rax
.L575:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L657
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L656:
	.cfi_restore_state
	movl	$75457409, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L589
	movq	-72(%rbp), %rcx
	movzbl	23(%rcx), %eax
	addq	$32, %rcx
	andl	$15, %eax
	jmp	.L588
	.p2align 4,,10
	.p2align 3
.L589:
	movq	-72(%rbp), %rax
	movq	40(%rbx), %r12
	movq	32(%rax), %rdx
	movzbl	23(%rax), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L658
.L596:
	movq	8(%rdx), %rax
	movq	%rax, -88(%rbp)
	cmpq	%r12, %rax
	jne	.L659
.L599:
	movq	16(%rbx), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14ReferenceEqualEv@PLT
	leaq	-80(%rbp), %rdi
	movl	$4294967295, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler16JSBinopReduction20ChangeToPureOperatorEPKNS1_8OperatorENS1_4TypeE
	jmp	.L575
	.p2align 4,,10
	.p2align 3
.L654:
	movq	16(%rcx), %rdx
	movq	8(%rdx), %rdx
	movq	%rdx, -88(%rbp)
	movq	32(%rax), %rax
	addq	$24, %rax
	jmp	.L584
	.p2align 4,,10
	.p2align 3
.L659:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L599
	movq	-72(%rbp), %rax
	movzbl	23(%rax), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L600
	addq	$40, %rax
.L601:
	movq	(%rax), %rax
	movq	8(%rax), %rax
	movq	%rax, -88(%rbp)
	cmpq	%r12, %rax
	je	.L599
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L599
	movq	-72(%rbp), %rax
	leaq	-80(%rbp), %r12
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler22CompareOperationHintOfEPKNS1_8OperatorE@PLT
	cmpb	$4, %al
	je	.L604
.L605:
	movl	$16417, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler16JSBinopReduction13BothInputsAreENS1_4TypeE
	testb	%al, %al
	jne	.L651
	movl	$1099, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler16JSBinopReduction13BothInputsAreENS1_4TypeE
	testb	%al, %al
	jne	.L610
	movl	$1031, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler16JSBinopReduction13BothInputsAreENS1_4TypeE
	testb	%al, %al
	je	.L660
.L610:
	movq	16(%rbx), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder11NumberEqualEv@PLT
	movl	$4294967295, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler16JSBinopReduction20ChangeToPureOperatorEPKNS1_8OperatorENS1_4TypeE
	jmp	.L575
	.p2align 4,,10
	.p2align 3
.L658:
	movq	16(%rdx), %rdx
	jmp	.L596
	.p2align 4,,10
	.p2align 3
.L590:
	movq	(%rcx), %rcx
	addq	$24, %rcx
	jmp	.L591
	.p2align 4,,10
	.p2align 3
.L655:
	movq	16(%rbx), %rdi
	call	_ZN2v88internal8compiler7JSGraph13FalseConstantEv@PLT
	jmp	.L649
	.p2align 4,,10
	.p2align 3
.L580:
	movq	32(%rax), %rax
	addq	$24, %rax
	jmp	.L581
.L614:
	call	_ZN2v88internal8compiler16JSBinopReduction24IsStringCompareOperationEv
	movq	%r12, %rdi
	testb	%al, %al
	je	.L615
	call	_ZN2v88internal8compiler16JSBinopReduction19CheckInputsToStringEv
	.p2align 4,,10
	.p2align 3
.L651:
	movq	16(%rbx), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder11StringEqualEv@PLT
	movl	$4294967295, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler16JSBinopReduction20ChangeToPureOperatorEPKNS1_8OperatorENS1_4TypeE
	jmp	.L575
	.p2align 4,,10
	.p2align 3
.L600:
	movq	32(%rax), %rax
	addq	$24, %rax
	jmp	.L601
	.p2align 4,,10
	.p2align 3
.L604:
	movl	$16385, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler16JSBinopReduction15BothInputsMaybeENS1_4TypeE
	testb	%al, %al
	je	.L605
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler16JSBinopReduction31CheckInputsToInternalizedStringEv
.L650:
	movq	16(%rbx), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14ReferenceEqualEv@PLT
	movl	$4294967295, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler16JSBinopReduction20ChangeToPureOperatorEPKNS1_8OperatorENS1_4TypeE
	jmp	.L575
.L660:
	movq	-72(%rbp), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler22CompareOperationHintOfEPKNS1_8OperatorE@PLT
	cmpb	$2, %al
	je	.L618
	cmpb	$3, %al
	je	.L619
	cmpb	$1, %al
	je	.L620
	movl	$7263, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler16JSBinopReduction13BothInputsAreENS1_4TypeE
	testb	%al, %al
	jne	.L610
	movq	-72(%rbp), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler22CompareOperationHintOfEPKNS1_8OperatorE@PLT
	cmpb	$8, %al
	je	.L661
.L613:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler16JSBinopReduction43IsReceiverOrNullOrUndefinedCompareOperationEv
	movq	%r12, %rdi
	testb	%al, %al
	je	.L614
	call	_ZN2v88internal8compiler16JSBinopReduction41CheckLeftInputToReceiverOrNullOrUndefinedEv
	jmp	.L650
.L619:
	movl	$4, %esi
.L609:
	movq	16(%rbx), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder22SpeculativeNumberEqualENS1_19NumberOperationHintE@PLT
	movq	-72(%rbp), %rsi
	xorl	%r8d, %r8d
	movq	%rax, %r12
	movq	-80(%rbp), %rax
	movq	%rsi, %rcx
	movq	%rsi, %rdx
	movq	8(%rax), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	-72(%rbp), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	testb	%al, %al
	jne	.L662
.L612:
	movq	-72(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler14NodeProperties14PastValueIndexEPNS1_4NodeE@PLT
	movq	%r13, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler4Node11RemoveInputEi@PLT
	movq	-72(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	-72(%rbp), %rax
	movl	$513, %esi
	movq	8(%rax), %rdi
	movq	-80(%rbp), %rax
	movq	16(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdx
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	-72(%rbp), %rdx
	movq	%rax, 8(%rdx)
	movq	-72(%rbp), %rax
	jmp	.L575
.L620:
	xorl	%esi, %esi
	jmp	.L609
.L618:
	movl	$3, %esi
	jmp	.L609
.L662:
	movq	-72(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler14NodeProperties16PastContextIndexEPNS1_4NodeE@PLT
	movq	%r13, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler4Node11RemoveInputEi@PLT
	jmp	.L612
.L657:
	call	__stack_chk_fail@PLT
.L661:
	movl	$75431937, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler16JSBinopReduction15BothInputsMaybeENS1_4TypeE
	testb	%al, %al
	je	.L613
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler16JSBinopReduction24CheckLeftInputToReceiverEv
	jmp	.L650
.L615:
	call	_ZN2v88internal8compiler16JSBinopReduction24IsSymbolCompareOperationEv
	movl	%eax, %r8d
	xorl	%eax, %eax
	testb	%r8b, %r8b
	je	.L575
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler16JSBinopReduction22CheckLeftInputToSymbolEv
	jmp	.L650
	.cfi_endproc
.LFE23813:
	.size	_ZN2v88internal8compiler15JSTypedLowering19ReduceJSStrictEqualEPNS1_4NodeE, .-_ZN2v88internal8compiler15JSTypedLowering19ReduceJSStrictEqualEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler15JSTypedLowering14ReduceJSToNameEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15JSTypedLowering14ReduceJSToNameEPNS1_4NodeE
	.type	_ZN2v88internal8compiler15JSTypedLowering14ReduceJSToNameEPNS1_4NodeE, @function
_ZN2v88internal8compiler15JSTypedLowering14ReduceJSToNameEPNS1_4NodeE:
.LFB23814:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	movq	%r12, %rdi
	subq	$24, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rax, %rbx
	movq	8(%rax), %rax
	movq	%rax, -48(%rbp)
	cmpq	$24609, %rax
	jne	.L671
.L664:
	movq	8(%r13), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%rbx, %rax
.L665:
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L672
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L671:
	.cfi_restore_state
	leaq	-48(%rbp), %rdi
	movl	$24609, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L664
	xorl	%eax, %eax
	jmp	.L665
.L672:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23814:
	.size	_ZN2v88internal8compiler15JSTypedLowering14ReduceJSToNameEPNS1_4NodeE, .-_ZN2v88internal8compiler15JSTypedLowering14ReduceJSToNameEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler15JSTypedLowering16ReduceJSToLengthEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15JSTypedLowering16ReduceJSToLengthEPNS1_4NodeE
	.type	_ZN2v88internal8compiler15JSTypedLowering16ReduceJSToLengthEPNS1_4NodeE, @function
_ZN2v88internal8compiler15JSTypedLowering16ReduceJSToLengthEPNS1_4NodeE:
.LFB23815:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	xorl	%esi, %esi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	48(%r12), %rdx
	movq	%rax, %rbx
	movq	8(%rax), %rax
	movq	328(%rdx), %rsi
	movq	%rax, -88(%rbp)
	cmpq	%rsi, %rax
	jne	.L693
.L674:
	cmpq	$1, %rsi
	je	.L677
	leaq	-88(%rbp), %r14
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type3MaxEv@PLT
	pxor	%xmm1, %xmm1
	comisd	%xmm0, %xmm1
	jnb	.L677
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	comisd	.LC8(%rip), %xmm0
	jnb	.L678
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	pxor	%xmm2, %xmm2
	comisd	%xmm0, %xmm2
	jnb	.L694
.L681:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type3MaxEv@PLT
	comisd	.LC8(%rip), %xmm0
	jbe	.L680
	movq	.LC8(%rip), %rax
	movq	16(%r12), %rdi
	movq	%rax, %xmm0
	movq	(%rdi), %r14
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%rax, -104(%rbp)
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9NumberMinEv@PLT
	movq	%rbx, %xmm3
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movq	-104(%rbp), %xmm0
	movq	%rax, %rsi
	leaq	-80(%rbp), %rcx
	movl	$2, %edx
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %rbx
	jmp	.L680
	.p2align 4,,10
	.p2align 3
.L677:
	movq	16(%r12), %rdi
	call	_ZN2v88internal8compiler7JSGraph12ZeroConstantEv@PLT
	movq	%rax, %rbx
.L680:
	movq	8(%r12), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%rbx, %rax
.L676:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L695
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L693:
	.cfi_restore_state
	leaq	-88(%rbp), %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	testb	%r8b, %r8b
	je	.L676
	movq	-88(%rbp), %rsi
	jmp	.L674
	.p2align 4,,10
	.p2align 3
.L678:
	movq	.LC8(%rip), %rax
	movq	16(%r12), %rdi
	movq	%rax, %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%rax, %rbx
	jmp	.L680
	.p2align 4,,10
	.p2align 3
.L694:
	movq	16(%r12), %rdi
	movq	(%rdi), %r15
	call	_ZN2v88internal8compiler7JSGraph12ZeroConstantEv@PLT
	movq	%rax, -104(%rbp)
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9NumberMaxEv@PLT
	movq	%rbx, %xmm4
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movq	-104(%rbp), %xmm0
	movq	%rax, %rsi
	leaq	-80(%rbp), %rcx
	movl	$2, %edx
	punpcklqdq	%xmm4, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %rbx
	jmp	.L681
.L695:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23815:
	.size	_ZN2v88internal8compiler15JSTypedLowering16ReduceJSToLengthEPNS1_4NodeE, .-_ZN2v88internal8compiler15JSTypedLowering16ReduceJSToLengthEPNS1_4NodeE
	.section	.rodata._ZN2v88internal8compiler15JSTypedLowering21ReduceJSToNumberInputEPNS1_4NodeE.str1.1,"aMS",@progbits,1
.LC9:
	.string	"ReduceJSToNumberInput"
	.section	.text._ZN2v88internal8compiler15JSTypedLowering21ReduceJSToNumberInputEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15JSTypedLowering21ReduceJSToNumberInputEPNS1_4NodeE
	.type	_ZN2v88internal8compiler15JSTypedLowering21ReduceJSToNumberInputEPNS1_4NodeE, @function
_ZN2v88internal8compiler15JSTypedLowering21ReduceJSToNumberInputEPNS1_4NodeE:
.LFB23816:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rsi), %rax
	movq	%rax, -104(%rbp)
	cmpq	$16417, %rax
	jne	.L697
.L700:
	movq	(%rbx), %rax
	cmpw	$30, 16(%rax)
	jne	.L701
	movq	48(%rax), %r14
	leaq	-96(%rbp), %r13
	movq	24(%r12), %rsi
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	movq	%r14, %rdx
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L703
	movdqa	-96(%rbp), %xmm1
	leaq	-80(%rbp), %r15
	movq	%r15, %rdi
	movaps	%xmm1, -80(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef8IsStringEv@PLT
	testb	%al, %al
	jne	.L728
	.p2align 4,,10
	.p2align 3
.L701:
	movq	-104(%rbp), %rax
	testb	$1, %al
	jne	.L707
	movl	(%rax), %edx
	testl	%edx, %edx
	je	.L729
.L707:
	cmpq	$7263, %rax
	jne	.L730
.L709:
	movq	%rbx, %rax
.L706:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L731
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L730:
	.cfi_restore_state
	leaq	-104(%rbp), %r13
	movl	$7263, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L709
	cmpq	$257, -104(%rbp)
	jne	.L732
.L710:
	movq	16(%r12), %rdi
	call	_ZN2v88internal8compiler7JSGraph11NaNConstantEv@PLT
	jmp	.L706
	.p2align 4,,10
	.p2align 3
.L697:
	leaq	-104(%rbp), %rdi
	movl	$16417, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L700
	jmp	.L701
	.p2align 4,,10
	.p2align 3
.L729:
	leaq	-104(%rbp), %rdi
	call	_ZNK2v88internal8compiler4Type14AsHeapConstantEv@PLT
	leaq	-80(%rbp), %rdi
	movdqu	8(%rax), %xmm2
	movaps	%xmm2, -80(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef15OddballToNumberEv@PLT
	testb	%al, %al
	je	.L708
	movq	16(%r12), %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	jmp	.L706
	.p2align 4,,10
	.p2align 3
.L732:
	movl	$257, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L710
	cmpq	$129, -104(%rbp)
	je	.L712
	movl	$129, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L712
	xorl	%eax, %eax
	jmp	.L706
	.p2align 4,,10
	.p2align 3
.L728:
	movq	24(%r12), %rsi
	xorl	%ecx, %ecx
	movq	%r15, %rdi
	movq	%r14, %rdx
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L703
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef8AsStringEv@PLT
	movq	%r13, %rdi
	movq	%rax, -96(%rbp)
	movq	%rdx, -88(%rbp)
	call	_ZN2v88internal8compiler9StringRef8ToNumberEv@PLT
	testb	%al, %al
	jne	.L704
	movq	24(%r12), %rdi
	movl	$961, %edx
	leaq	.LC9(%rip), %rsi
	call	_ZN2v88internal8compiler28NoChangeBecauseOfMissingDataEPNS1_12JSHeapBrokerEPKci@PLT
	jmp	.L706
	.p2align 4,,10
	.p2align 3
.L708:
	movq	-104(%rbp), %rax
	cmpq	$7263, %rax
	je	.L709
	jmp	.L730
	.p2align 4,,10
	.p2align 3
.L704:
	movq	16(%r12), %rdi
	movq	%rdx, %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	jmp	.L706
	.p2align 4,,10
	.p2align 3
.L712:
	movq	16(%r12), %rdi
	call	_ZN2v88internal8compiler7JSGraph12ZeroConstantEv@PLT
	jmp	.L706
	.p2align 4,,10
	.p2align 3
.L703:
	leaq	.LC6(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L731:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23816:
	.size	_ZN2v88internal8compiler15JSTypedLowering21ReduceJSToNumberInputEPNS1_4NodeE, .-_ZN2v88internal8compiler15JSTypedLowering21ReduceJSToNumberInputEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler16JSBinopReduction21ConvertInputsToNumberEv,"axG",@progbits,_ZN2v88internal8compiler16JSBinopReduction21ConvertInputsToNumberEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler16JSBinopReduction21ConvertInputsToNumberEv
	.type	_ZN2v88internal8compiler16JSBinopReduction21ConvertInputsToNumberEv, @function
_ZN2v88internal8compiler16JSBinopReduction21ConvertInputsToNumberEv:
.LFB23759:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	(%r14), %rdi
	movq	%rax, %rsi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler15JSTypedLowering21ReduceJSToNumberInputEPNS1_4NodeE
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L766
.L734:
	movzbl	23(%rbx), %eax
	movq	32(%rbx), %rdi
	leaq	32(%rbx), %r15
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L737
	cmpq	%r13, %rdi
	je	.L739
	leaq	-24(%rbx), %r12
	testq	%rdi, %rdi
	je	.L741
.L769:
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L741:
	movq	%r13, (%r15)
	testq	%r13, %r13
	je	.L739
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L739:
	movq	8(%r14), %rbx
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	(%r14), %rdi
	movq	%rax, %rsi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler15JSTypedLowering21ReduceJSToNumberInputEPNS1_4NodeE
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L767
.L742:
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L745
	movq	40(%rbx), %rdi
	leaq	40(%rbx), %r14
	cmpq	%rdi, %r13
	je	.L733
	leaq	-48(%rbx), %r12
	testq	%rdi, %rdi
	je	.L749
.L770:
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L749:
	movq	%r13, (%r14)
	testq	%r13, %r13
	je	.L733
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L733:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L768
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L737:
	.cfi_restore_state
	movq	16(%rdi), %rax
	leaq	16(%rdi), %r15
	cmpq	%r13, %rax
	je	.L739
	movq	%rdi, %rbx
	movq	%rax, %rdi
	leaq	-24(%rbx), %r12
	testq	%rdi, %rdi
	jne	.L769
	jmp	.L741
	.p2align 4,,10
	.p2align 3
.L745:
	movq	32(%rbx), %rbx
	movq	24(%rbx), %rdi
	cmpq	%rdi, %r13
	je	.L733
	leaq	24(%rbx), %r14
	leaq	-48(%rbx), %r12
	testq	%rdi, %rdi
	jne	.L770
	jmp	.L749
	.p2align 4,,10
	.p2align 3
.L767:
	movq	8(%r12), %rax
	movq	%rax, -80(%rbp)
	cmpq	$7263, %rax
	jne	.L743
.L765:
	movq	%r12, %r13
	jmp	.L742
	.p2align 4,,10
	.p2align 3
.L766:
	movq	8(%r12), %rax
	movq	%rax, -72(%rbp)
	cmpq	$7263, %rax
	jne	.L735
.L764:
	movq	%r12, %r13
	jmp	.L734
	.p2align 4,,10
	.p2align 3
.L735:
	leaq	-72(%rbp), %rdi
	movl	$7263, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L764
	movq	(%r14), %rax
	movq	16(%rax), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder22PlainPrimitiveToNumberEv@PLT
	movq	%r13, %rdi
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r12, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r13
	jmp	.L734
	.p2align 4,,10
	.p2align 3
.L743:
	leaq	-80(%rbp), %rdi
	movl	$7263, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L765
	movq	(%r14), %rax
	movq	16(%rax), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder22PlainPrimitiveToNumberEv@PLT
	movq	%r13, %rdi
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r12, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r13
	jmp	.L742
.L768:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23759:
	.size	_ZN2v88internal8compiler16JSBinopReduction21ConvertInputsToNumberEv, .-_ZN2v88internal8compiler16JSBinopReduction21ConvertInputsToNumberEv
	.section	.text._ZN2v88internal8compiler15JSTypedLowering18ReduceJSComparisonEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15JSTypedLowering18ReduceJSComparisonEPNS1_4NodeE
	.type	_ZN2v88internal8compiler15JSTypedLowering18ReduceJSComparisonEPNS1_4NodeE, @function
_ZN2v88internal8compiler15JSTypedLowering18ReduceJSComparisonEPNS1_4NodeE:
.LFB23811:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %xmm0
	movq	%rsi, %xmm1
	leaq	32(%rsi), %rdx
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$72, %rsp
	movq	32(%rsi), %rcx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	movaps	%xmm0, -80(%rbp)
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L772
	movq	16(%rcx), %rcx
.L772:
	movq	8(%rcx), %rcx
	movq	%rcx, -88(%rbp)
	cmpq	$16417, %rcx
	jne	.L969
	cmpl	$15, %eax
	je	.L777
.L971:
	addq	$8, %rdx
.L778:
	movq	(%rdx), %rax
	movq	8(%rax), %rax
	movq	%rax, -88(%rbp)
	cmpq	$16417, %rax
	jne	.L970
.L779:
	movq	(%rbx), %rax
	movzwl	16(%rax), %eax
	cmpw	$686, %ax
	je	.L781
	ja	.L782
	cmpw	$684, %ax
	je	.L783
	cmpw	$685, %ax
	jne	.L837
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14StringLessThanEv@PLT
.L968:
	movq	-72(%rbp), %rdi
	xorl	%esi, %esi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	-72(%rbp), %rdi
	movl	$1, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	-72(%rbp), %rdi
	movq	%rax, %r14
	movzbl	23(%rdi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L798
	movq	32(%rdi), %r8
	leaq	32(%rdi), %r15
	cmpq	%r8, %r14
	je	.L800
.L799:
	leaq	-24(%rdi), %rsi
	testq	%r8, %r8
	je	.L802
	movq	%r8, %rdi
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-104(%rbp), %rsi
.L802:
	movq	%r14, (%r15)
	testq	%r14, %r14
	je	.L803
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L803:
	movq	-72(%rbp), %rdi
	movzbl	23(%rdi), %eax
	leaq	32(%rdi), %r15
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L804
.L800:
	movq	8(%r15), %r8
	addq	$8, %r15
	cmpq	%r8, %r13
	je	.L787
.L805:
	leaq	-48(%rdi), %r14
	testq	%r8, %r8
	je	.L806
	movq	%r14, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L806:
	movq	%r13, (%r15)
	testq	%r13, %r13
	je	.L947
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L947:
	movq	-72(%rbp), %rdi
.L787:
	movq	(%rdi), %rax
	movl	24(%rax), %edx
	testl	%edx, %edx
	jle	.L809
	movq	-80(%rbp), %rax
	movq	%rdi, %rdx
	movq	%rdi, %rsi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	8(%rax), %r9
	movq	(%r9), %rax
	movq	%r9, %rdi
	call	*32(%rax)
	movq	-72(%rbp), %rdi
.L809:
	call	_ZN2v88internal8compiler14NodeProperties20RemoveNonValueInputsEPNS1_4NodeE@PLT
	movq	-72(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	-72(%rbp), %rax
	movl	$4294967295, %esi
	movq	8(%rax), %rdi
	movq	-80(%rbp), %rax
	movq	16(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdx
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	-72(%rbp), %rdx
	movq	%rax, 8(%rdx)
	movq	%rbx, %rax
	jmp	.L808
	.p2align 4,,10
	.p2align 3
.L782:
	cmpw	$687, %ax
	jne	.L837
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder21StringLessThanOrEqualEv@PLT
	jmp	.L968
	.p2align 4,,10
	.p2align 3
.L969:
	leaq	-88(%rbp), %r13
	movl	$16417, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L776
	movq	-72(%rbp), %rdx
	movzbl	23(%rdx), %eax
	addq	$32, %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L971
.L777:
	movq	(%rdx), %rdx
	addq	$24, %rdx
	jmp	.L778
	.p2align 4,,10
	.p2align 3
.L970:
	leaq	-88(%rbp), %r13
	movl	$16417, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L779
.L776:
	movq	-72(%rbp), %rdx
	movzbl	23(%rdx), %eax
	leaq	32(%rdx), %rcx
	movq	32(%rdx), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L810
	movq	16(%rdx), %rdx
.L810:
	movq	8(%rdx), %rdx
	movq	%rdx, -88(%rbp)
	cmpq	$1099, %rdx
	jne	.L972
.L813:
	cmpl	$15, %eax
	je	.L815
	addq	$8, %rcx
.L816:
	movq	(%rcx), %rax
	movq	8(%rax), %rax
	movq	%rax, -88(%rbp)
	cmpq	$1099, %rax
	je	.L948
	movl	$1099, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L814
.L948:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14NumberLessThanEv@PLT
	movq	%rax, %r13
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder21NumberLessThanOrEqualEv@PLT
	movq	%rax, %r12
.L820:
	movq	(%rbx), %rdx
	movzwl	16(%rdx), %edx
	cmpw	$686, %dx
	je	.L951
	ja	.L839
	cmpw	$684, %dx
	je	.L952
	cmpw	$685, %dx
	jne	.L837
	movq	-72(%rbp), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	-72(%rbp), %rdi
	movl	$1, %esi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	-72(%rbp), %rdi
	movq	%rax, %r14
	movzbl	23(%rdi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L843
	movq	32(%rdi), %r8
	leaq	32(%rdi), %rbx
	cmpq	%r8, %r14
	je	.L845
.L844:
	leaq	-24(%rdi), %r15
	testq	%r8, %r8
	je	.L847
	movq	%r15, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L847:
	movq	%r14, (%rbx)
	testq	%r14, %r14
	je	.L848
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L848:
	movq	-72(%rbp), %rdi
	movzbl	23(%rdi), %eax
	leaq	32(%rdi), %rbx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L849
.L845:
	movq	8(%rbx), %r8
	cmpq	%r8, %r12
	je	.L850
.L949:
	addq	$8, %rbx
	leaq	-48(%rdi), %r14
	testq	%r8, %r8
	je	.L852
	movq	%r14, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L852:
	movq	%r12, (%rbx)
	testq	%r12, %r12
	je	.L952
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	.p2align 4,,10
	.p2align 3
.L952:
	movq	-72(%rbp), %rdi
.L850:
	movq	(%rdi), %rax
	movl	24(%rax), %eax
	testl	%eax, %eax
	jle	.L864
	movq	-80(%rbp), %rax
	movq	%rdi, %rdx
	movq	%rdi, %rsi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	8(%rax), %r9
	movq	(%r9), %rax
	movq	%r9, %rdi
	call	*32(%rax)
	movq	-72(%rbp), %rdi
.L864:
	call	_ZN2v88internal8compiler14NodeProperties20RemoveNonValueInputsEPNS1_4NodeE@PLT
	movq	-72(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	-72(%rbp), %rax
	movl	$4294967295, %esi
	movq	8(%rax), %rdi
	movq	-80(%rbp), %rax
	movq	16(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdx
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	-72(%rbp), %rdx
	movq	%rax, 8(%rdx)
	movq	-72(%rbp), %rax
	jmp	.L808
	.p2align 4,,10
	.p2align 3
.L837:
	xorl	%eax, %eax
.L808:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L973
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L972:
	.cfi_restore_state
	movl	$1099, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L814
	movq	-72(%rbp), %rcx
	movzbl	23(%rcx), %eax
	addq	$32, %rcx
	andl	$15, %eax
	jmp	.L813
	.p2align 4,,10
	.p2align 3
.L798:
	movq	32(%rdi), %rax
	movq	16(%rax), %r8
	leaq	16(%rax), %r15
	cmpq	%r8, %r14
	je	.L801
	movq	%rax, %rdi
	jmp	.L799
	.p2align 4,,10
	.p2align 3
.L814:
	movq	-72(%rbp), %rdx
	movzbl	23(%rdx), %eax
	leaq	32(%rdx), %rcx
	movq	32(%rdx), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L821
	movq	16(%rdx), %rdx
.L821:
	movq	8(%rdx), %rdx
	movq	%rdx, -88(%rbp)
	cmpq	$1031, %rdx
	jne	.L974
.L824:
	cmpl	$15, %eax
	je	.L826
	addq	$8, %rcx
.L827:
	movq	(%rcx), %rax
	movq	8(%rax), %rax
	movq	%rax, -88(%rbp)
	cmpq	$1031, %rax
	je	.L948
	movl	$1031, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L948
.L825:
	movq	-72(%rbp), %rax
	movq	32(%rax), %rdx
	movzbl	23(%rax), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L975
.L830:
	movq	8(%rdx), %rax
	movl	$75448353, %esi
	movq	%r13, %rdi
	movq	%rax, -88(%rbp)
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L976
.L831:
	leaq	-80(%rbp), %r13
	movl	$24575, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler16JSBinopReduction13BothInputsAreENS1_4TypeE
	testb	%al, %al
	jne	.L835
.L836:
	movq	-72(%rbp), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler22CompareOperationHintOfEPKNS1_8OperatorE@PLT
	cmpb	$5, %al
	jne	.L837
	leaq	-80(%rbp), %r13
	movl	$16417, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler16JSBinopReduction15BothInputsMaybeENS1_4TypeE
	testb	%al, %al
	je	.L837
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler16JSBinopReduction19CheckInputsToStringEv
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14StringLessThanEv@PLT
	movq	%rax, %r13
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder21StringLessThanOrEqualEv@PLT
	movq	%rax, %r12
	jmp	.L820
	.p2align 4,,10
	.p2align 3
.L839:
	cmpw	$687, %dx
	jne	.L837
	movq	-72(%rbp), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	-72(%rbp), %rdi
	movl	$1, %esi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	-72(%rbp), %rdi
	movq	%rax, %r13
	movzbl	23(%rdi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L854
	movq	32(%rdi), %r8
	leaq	32(%rdi), %rbx
	cmpq	%r8, %r13
	je	.L856
.L855:
	leaq	-24(%rdi), %r15
	testq	%r8, %r8
	je	.L858
	movq	%r15, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L858:
	movq	%r13, (%rbx)
	testq	%r13, %r13
	je	.L859
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L859:
	movq	-72(%rbp), %rdi
	movzbl	23(%rdi), %eax
	leaq	32(%rdi), %rbx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L860
.L856:
	movq	8(%rbx), %r8
	movq	%r12, %r13
	cmpq	%r8, %r14
	je	.L850
.L950:
	addq	$8, %rbx
	leaq	-48(%rdi), %r13
	testq	%r8, %r8
	je	.L862
	movq	%r13, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L862:
	movq	%r14, (%rbx)
	testq	%r14, %r14
	je	.L951
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L951:
	movq	-72(%rbp), %rdi
	movq	%r12, %r13
	jmp	.L850
	.p2align 4,,10
	.p2align 3
.L781:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder21StringLessThanOrEqualEv@PLT
	movq	-72(%rbp), %rdi
	movq	%rax, %r12
	jmp	.L787
	.p2align 4,,10
	.p2align 3
.L783:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14StringLessThanEv@PLT
	movq	-72(%rbp), %rdi
	movq	%rax, %r12
	jmp	.L787
	.p2align 4,,10
	.p2align 3
.L804:
	movq	32(%rdi), %rax
	leaq	16(%rax), %r15
.L801:
	movq	8(%r15), %r8
	cmpq	%r8, %r13
	je	.L787
	addq	$8, %r15
	movq	%rax, %rdi
	jmp	.L805
	.p2align 4,,10
	.p2align 3
.L974:
	movl	$1031, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L825
	movq	-72(%rbp), %rcx
	movzbl	23(%rcx), %eax
	addq	$32, %rcx
	andl	$15, %eax
	jmp	.L824
	.p2align 4,,10
	.p2align 3
.L976:
	movq	-72(%rbp), %rax
	movzbl	23(%rax), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L832
	addq	$40, %rax
.L833:
	movq	(%rax), %rax
	leaq	-96(%rbp), %rdi
	movl	$75448353, %esi
	movq	8(%rax), %rax
	movq	%rax, -96(%rbp)
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L836
	jmp	.L831
	.p2align 4,,10
	.p2align 3
.L843:
	movq	32(%rdi), %rax
	movq	16(%rax), %r8
	leaq	16(%rax), %rbx
	cmpq	%r8, %r14
	je	.L866
	movq	%rax, %rdi
	jmp	.L844
	.p2align 4,,10
	.p2align 3
.L854:
	movq	32(%rdi), %rax
	movq	16(%rax), %r8
	leaq	16(%rax), %rbx
	cmpq	%r8, %r13
	je	.L868
	movq	%rax, %rdi
	jmp	.L855
	.p2align 4,,10
	.p2align 3
.L975:
	movq	16(%rdx), %rdx
	jmp	.L830
	.p2align 4,,10
	.p2align 3
.L826:
	movq	(%rcx), %rcx
	addq	$24, %rcx
	jmp	.L827
	.p2align 4,,10
	.p2align 3
.L815:
	movq	(%rcx), %rcx
	addq	$24, %rcx
	jmp	.L816
	.p2align 4,,10
	.p2align 3
.L860:
	movq	32(%rdi), %rax
	movq	%rdi, %rdx
	leaq	16(%rax), %rbx
	movq	%rax, %rdi
.L857:
	movq	8(%rbx), %r8
	cmpq	%r8, %r14
	jne	.L950
	movq	%rdx, %rdi
	movq	%r12, %r13
	jmp	.L850
	.p2align 4,,10
	.p2align 3
.L849:
	movq	32(%rdi), %rax
	movq	%rdi, %rdx
	leaq	16(%rax), %rbx
	movq	%rax, %rdi
.L846:
	movq	8(%rbx), %r8
	cmpq	%r8, %r12
	jne	.L949
	movq	%rdx, %rdi
	jmp	.L850
.L832:
	movq	32(%rax), %rax
	addq	$24, %rax
	jmp	.L833
	.p2align 4,,10
	.p2align 3
.L835:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler16JSBinopReduction21ConvertInputsToNumberEv
	jmp	.L948
.L868:
	movq	%rdi, %rdx
	movq	%rax, %rdi
	jmp	.L857
.L866:
	movq	%rdi, %rdx
	movq	%rax, %rdi
	jmp	.L846
.L973:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23811:
	.size	_ZN2v88internal8compiler15JSTypedLowering18ReduceJSComparisonEPNS1_4NodeE, .-_ZN2v88internal8compiler15JSTypedLowering18ReduceJSComparisonEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler15JSTypedLowering17ReduceJSDecrementEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15JSTypedLowering17ReduceJSDecrementEPNS1_4NodeE
	.type	_ZN2v88internal8compiler15JSTypedLowering17ReduceJSDecrementEPNS1_4NodeE, @function
_ZN2v88internal8compiler15JSTypedLowering17ReduceJSDecrementEPNS1_4NodeE:
.LFB23798:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	8(%rax), %rax
	movq	%rax, -88(%rbp)
	cmpq	$24575, %rax
	jne	.L1032
.L978:
	movq	16(%r12), %rdi
	call	_ZN2v88internal8compiler7JSGraph11OneConstantEv@PLT
	movl	$1, %edx
	movq	%rbx, %rdi
	movq	%rax, %rcx
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%r12), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder8SubtractEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler15JSTypedLowering21ReduceJSToNumberInputEPNS1_4NodeE
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1033
.L980:
	movzbl	23(%rbx), %eax
	movq	32(%rbx), %rdi
	leaq	32(%rbx), %r14
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L983
	movq	%r14, %rax
	movq	%rbx, %rsi
	cmpq	%r13, %rdi
	je	.L985
.L984:
	leaq	-24(%rsi), %r15
	testq	%rdi, %rdi
	je	.L987
	movq	%r15, %rsi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-104(%rbp), %rax
.L987:
	movq	%r13, (%rax)
	testq	%r13, %r13
	je	.L985
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L985:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler15JSTypedLowering21ReduceJSToNumberInputEPNS1_4NodeE
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1034
.L988:
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L991
	movq	8(%r14), %rdi
	cmpq	%rdi, %r13
	je	.L994
	addq	$8, %r14
	movq	%rbx, %rsi
.L993:
	leaq	-48(%rsi), %r15
	testq	%rdi, %rdi
	je	.L995
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L995:
	movq	%r13, (%r14)
	testq	%r13, %r13
	je	.L994
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L994:
	movq	(%rbx), %rax
	movzwl	16(%rax), %eax
	subw	$688, %ax
	cmpw	$11, %ax
	ja	.L996
	leaq	.L998(%rip), %rdx
	movzwl	%ax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler15JSTypedLowering17ReduceJSDecrementEPNS1_4NodeE,"a",@progbits
	.align 4
	.align 4
.L998:
	.long	.L1009-.L998
	.long	.L1008-.L998
	.long	.L1007-.L998
	.long	.L1006-.L998
	.long	.L1005-.L998
	.long	.L1004-.L998
	.long	.L1003-.L998
	.long	.L1002-.L998
	.long	.L1001-.L998
	.long	.L1000-.L998
	.long	.L999-.L998
	.long	.L997-.L998
	.section	.text._ZN2v88internal8compiler15JSTypedLowering17ReduceJSDecrementEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L983:
	movq	16(%rdi), %rdx
	leaq	16(%rdi), %rax
	cmpq	%rdx, %r13
	je	.L985
	movq	%rdi, %rsi
	movq	%rdx, %rdi
	jmp	.L984
	.p2align 4,,10
	.p2align 3
.L1032:
	leaq	-88(%rbp), %rdi
	movl	$24575, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L978
	xorl	%eax, %eax
.L979:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1035
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L991:
	.cfi_restore_state
	movq	32(%rbx), %rsi
	movq	24(%rsi), %rdi
	cmpq	%rdi, %r13
	je	.L994
	leaq	24(%rsi), %r14
	jmp	.L993
	.p2align 4,,10
	.p2align 3
.L1034:
	movq	8(%r15), %rax
	movq	%rax, -80(%rbp)
	cmpq	$7263, %rax
	jne	.L989
.L1031:
	movq	%r15, %r13
	jmp	.L988
	.p2align 4,,10
	.p2align 3
.L1033:
	movq	8(%r14), %rax
	movq	%rax, -72(%rbp)
	cmpq	$7263, %rax
	jne	.L981
.L1030:
	movq	%r14, %r13
	jmp	.L980
	.p2align 4,,10
	.p2align 3
.L981:
	leaq	-72(%rbp), %rdi
	movl	$7263, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1030
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder22PlainPrimitiveToNumberEv@PLT
	movq	%r13, %rdi
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r14, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r13
	jmp	.L980
	.p2align 4,,10
	.p2align 3
.L989:
	leaq	-80(%rbp), %rdi
	movl	$7263, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1031
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder22PlainPrimitiveToNumberEv@PLT
	movq	%r13, %rdi
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r15, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r13
	jmp	.L988
	.p2align 4,,10
	.p2align 3
.L999:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder13NumberModulusEv@PLT
	movq	%rax, %r13
	.p2align 4,,10
	.p2align 3
.L1010:
	movq	(%rbx), %rdx
	movl	24(%rdx), %eax
	testl	%eax, %eax
	jle	.L1011
	movq	8(%r12), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rbx, %rdx
	movq	%rbx, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
.L1011:
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties20RemoveNonValueInputsEPNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	16(%r12), %rax
	movq	8(%rbx), %rdi
	movl	$7263, %esi
	movq	(%rax), %rax
	movq	(%rax), %rdx
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, 8(%rbx)
	movq	%rbx, %rax
	jmp	.L979
	.p2align 4,,10
	.p2align 3
.L1008:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder16NumberBitwiseXorEv@PLT
	movq	%rax, %r13
	jmp	.L1010
	.p2align 4,,10
	.p2align 3
.L1009:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder15NumberBitwiseOrEv@PLT
	movq	%rax, %r13
	jmp	.L1010
	.p2align 4,,10
	.p2align 3
.L997:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9NumberPowEv@PLT
	movq	%rax, %r13
	jmp	.L1010
	.p2align 4,,10
	.p2align 3
.L1000:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder12NumberDivideEv@PLT
	movq	%rax, %r13
	jmp	.L1010
	.p2align 4,,10
	.p2align 3
.L1001:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14NumberMultiplyEv@PLT
	movq	%rax, %r13
	jmp	.L1010
	.p2align 4,,10
	.p2align 3
.L1002:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14NumberSubtractEv@PLT
	movq	%rax, %r13
	jmp	.L1010
	.p2align 4,,10
	.p2align 3
.L1003:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9NumberAddEv@PLT
	movq	%rax, %r13
	jmp	.L1010
	.p2align 4,,10
	.p2align 3
.L1004:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder23NumberShiftRightLogicalEv@PLT
	movq	%rax, %r13
	jmp	.L1010
	.p2align 4,,10
	.p2align 3
.L1005:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder16NumberShiftRightEv@PLT
	movq	%rax, %r13
	jmp	.L1010
	.p2align 4,,10
	.p2align 3
.L1006:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder15NumberShiftLeftEv@PLT
	movq	%rax, %r13
	jmp	.L1010
	.p2align 4,,10
	.p2align 3
.L1007:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder16NumberBitwiseAndEv@PLT
	movq	%rax, %r13
	jmp	.L1010
.L1035:
	call	__stack_chk_fail@PLT
.L996:
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE23798:
	.size	_ZN2v88internal8compiler15JSTypedLowering17ReduceJSDecrementEPNS1_4NodeE, .-_ZN2v88internal8compiler15JSTypedLowering17ReduceJSDecrementEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler15JSTypedLowering14ReduceJSNegateEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15JSTypedLowering14ReduceJSNegateEPNS1_4NodeE
	.type	_ZN2v88internal8compiler15JSTypedLowering14ReduceJSNegateEPNS1_4NodeE, @function
_ZN2v88internal8compiler15JSTypedLowering14ReduceJSNegateEPNS1_4NodeE:
.LFB23800:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	8(%rax), %rax
	movq	%rax, -88(%rbp)
	cmpq	$24575, %rax
	jne	.L1091
.L1037:
	movsd	.LC10(%rip), %xmm0
	movq	16(%r12), %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movl	$1, %edx
	movq	%rbx, %rdi
	movq	%rax, %rcx
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%r12), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder8MultiplyEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler15JSTypedLowering21ReduceJSToNumberInputEPNS1_4NodeE
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1092
.L1039:
	movzbl	23(%rbx), %eax
	movq	32(%rbx), %rdi
	leaq	32(%rbx), %r14
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1042
	movq	%r14, %rax
	movq	%rbx, %rsi
	cmpq	%r13, %rdi
	je	.L1044
.L1043:
	leaq	-24(%rsi), %r15
	testq	%rdi, %rdi
	je	.L1046
	movq	%r15, %rsi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-104(%rbp), %rax
.L1046:
	movq	%r13, (%rax)
	testq	%r13, %r13
	je	.L1044
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L1044:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler15JSTypedLowering21ReduceJSToNumberInputEPNS1_4NodeE
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1093
.L1047:
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1050
	movq	8(%r14), %rdi
	cmpq	%rdi, %r13
	je	.L1053
	addq	$8, %r14
	movq	%rbx, %rsi
.L1052:
	leaq	-48(%rsi), %r15
	testq	%rdi, %rdi
	je	.L1054
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L1054:
	movq	%r13, (%r14)
	testq	%r13, %r13
	je	.L1053
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L1053:
	movq	(%rbx), %rax
	movzwl	16(%rax), %eax
	subw	$688, %ax
	cmpw	$11, %ax
	ja	.L1055
	leaq	.L1057(%rip), %rdx
	movzwl	%ax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler15JSTypedLowering14ReduceJSNegateEPNS1_4NodeE,"a",@progbits
	.align 4
	.align 4
.L1057:
	.long	.L1068-.L1057
	.long	.L1067-.L1057
	.long	.L1066-.L1057
	.long	.L1065-.L1057
	.long	.L1064-.L1057
	.long	.L1063-.L1057
	.long	.L1062-.L1057
	.long	.L1061-.L1057
	.long	.L1060-.L1057
	.long	.L1059-.L1057
	.long	.L1058-.L1057
	.long	.L1056-.L1057
	.section	.text._ZN2v88internal8compiler15JSTypedLowering14ReduceJSNegateEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L1042:
	movq	16(%rdi), %rdx
	leaq	16(%rdi), %rax
	cmpq	%rdx, %r13
	je	.L1044
	movq	%rdi, %rsi
	movq	%rdx, %rdi
	jmp	.L1043
	.p2align 4,,10
	.p2align 3
.L1091:
	leaq	-88(%rbp), %rdi
	movl	$24575, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1037
	xorl	%eax, %eax
.L1038:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1094
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1050:
	.cfi_restore_state
	movq	32(%rbx), %rsi
	movq	24(%rsi), %rdi
	cmpq	%rdi, %r13
	je	.L1053
	leaq	24(%rsi), %r14
	jmp	.L1052
	.p2align 4,,10
	.p2align 3
.L1093:
	movq	8(%r15), %rax
	movq	%rax, -80(%rbp)
	cmpq	$7263, %rax
	jne	.L1048
.L1090:
	movq	%r15, %r13
	jmp	.L1047
	.p2align 4,,10
	.p2align 3
.L1092:
	movq	8(%r14), %rax
	movq	%rax, -72(%rbp)
	cmpq	$7263, %rax
	jne	.L1040
.L1089:
	movq	%r14, %r13
	jmp	.L1039
	.p2align 4,,10
	.p2align 3
.L1040:
	leaq	-72(%rbp), %rdi
	movl	$7263, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1089
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder22PlainPrimitiveToNumberEv@PLT
	movq	%r13, %rdi
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r14, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r13
	jmp	.L1039
	.p2align 4,,10
	.p2align 3
.L1048:
	leaq	-80(%rbp), %rdi
	movl	$7263, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1090
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder22PlainPrimitiveToNumberEv@PLT
	movq	%r13, %rdi
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r15, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r13
	jmp	.L1047
	.p2align 4,,10
	.p2align 3
.L1058:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder13NumberModulusEv@PLT
	movq	%rax, %r13
	.p2align 4,,10
	.p2align 3
.L1069:
	movq	(%rbx), %rdx
	movl	24(%rdx), %eax
	testl	%eax, %eax
	jle	.L1070
	movq	8(%r12), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rbx, %rdx
	movq	%rbx, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
.L1070:
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties20RemoveNonValueInputsEPNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	16(%r12), %rax
	movq	8(%rbx), %rdi
	movl	$7263, %esi
	movq	(%rax), %rax
	movq	(%rax), %rdx
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, 8(%rbx)
	movq	%rbx, %rax
	jmp	.L1038
	.p2align 4,,10
	.p2align 3
.L1067:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder16NumberBitwiseXorEv@PLT
	movq	%rax, %r13
	jmp	.L1069
	.p2align 4,,10
	.p2align 3
.L1068:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder15NumberBitwiseOrEv@PLT
	movq	%rax, %r13
	jmp	.L1069
	.p2align 4,,10
	.p2align 3
.L1056:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9NumberPowEv@PLT
	movq	%rax, %r13
	jmp	.L1069
	.p2align 4,,10
	.p2align 3
.L1059:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder12NumberDivideEv@PLT
	movq	%rax, %r13
	jmp	.L1069
	.p2align 4,,10
	.p2align 3
.L1060:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14NumberMultiplyEv@PLT
	movq	%rax, %r13
	jmp	.L1069
	.p2align 4,,10
	.p2align 3
.L1061:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14NumberSubtractEv@PLT
	movq	%rax, %r13
	jmp	.L1069
	.p2align 4,,10
	.p2align 3
.L1062:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9NumberAddEv@PLT
	movq	%rax, %r13
	jmp	.L1069
	.p2align 4,,10
	.p2align 3
.L1063:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder23NumberShiftRightLogicalEv@PLT
	movq	%rax, %r13
	jmp	.L1069
	.p2align 4,,10
	.p2align 3
.L1064:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder16NumberShiftRightEv@PLT
	movq	%rax, %r13
	jmp	.L1069
	.p2align 4,,10
	.p2align 3
.L1065:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder15NumberShiftLeftEv@PLT
	movq	%rax, %r13
	jmp	.L1069
	.p2align 4,,10
	.p2align 3
.L1066:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder16NumberBitwiseAndEv@PLT
	movq	%rax, %r13
	jmp	.L1069
.L1094:
	call	__stack_chk_fail@PLT
.L1055:
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE23800:
	.size	_ZN2v88internal8compiler15JSTypedLowering14ReduceJSNegateEPNS1_4NodeE, .-_ZN2v88internal8compiler15JSTypedLowering14ReduceJSNegateEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler15JSTypedLowering17ReduceJSIncrementEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15JSTypedLowering17ReduceJSIncrementEPNS1_4NodeE
	.type	_ZN2v88internal8compiler15JSTypedLowering17ReduceJSIncrementEPNS1_4NodeE, @function
_ZN2v88internal8compiler15JSTypedLowering17ReduceJSIncrementEPNS1_4NodeE:
.LFB23799:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	8(%rax), %rax
	movq	%rax, -88(%rbp)
	cmpq	$24575, %rax
	jne	.L1150
.L1096:
	movq	16(%r12), %rdi
	call	_ZN2v88internal8compiler7JSGraph11OneConstantEv@PLT
	movl	$1, %edx
	movq	%rbx, %rdi
	movq	%rax, %rcx
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%r12), %rax
	movl	$8, %esi
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder3AddENS0_19BinaryOperationHintE@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler15JSTypedLowering21ReduceJSToNumberInputEPNS1_4NodeE
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1151
.L1098:
	movzbl	23(%rbx), %eax
	movq	32(%rbx), %rdi
	leaq	32(%rbx), %r14
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1101
	movq	%r14, %rax
	movq	%rbx, %rsi
	cmpq	%r13, %rdi
	je	.L1103
.L1102:
	leaq	-24(%rsi), %r15
	testq	%rdi, %rdi
	je	.L1105
	movq	%r15, %rsi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-104(%rbp), %rax
.L1105:
	movq	%r13, (%rax)
	testq	%r13, %r13
	je	.L1103
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L1103:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler15JSTypedLowering21ReduceJSToNumberInputEPNS1_4NodeE
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1152
.L1106:
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1109
	movq	8(%r14), %rdi
	cmpq	%rdi, %r13
	je	.L1112
	addq	$8, %r14
	movq	%rbx, %rsi
.L1111:
	leaq	-48(%rsi), %r15
	testq	%rdi, %rdi
	je	.L1113
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L1113:
	movq	%r13, (%r14)
	testq	%r13, %r13
	je	.L1112
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L1112:
	movq	(%rbx), %rax
	movzwl	16(%rax), %eax
	subw	$688, %ax
	cmpw	$11, %ax
	ja	.L1114
	leaq	.L1116(%rip), %rdx
	movzwl	%ax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler15JSTypedLowering17ReduceJSIncrementEPNS1_4NodeE,"a",@progbits
	.align 4
	.align 4
.L1116:
	.long	.L1127-.L1116
	.long	.L1126-.L1116
	.long	.L1125-.L1116
	.long	.L1124-.L1116
	.long	.L1123-.L1116
	.long	.L1122-.L1116
	.long	.L1121-.L1116
	.long	.L1120-.L1116
	.long	.L1119-.L1116
	.long	.L1118-.L1116
	.long	.L1117-.L1116
	.long	.L1115-.L1116
	.section	.text._ZN2v88internal8compiler15JSTypedLowering17ReduceJSIncrementEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L1101:
	movq	16(%rdi), %rdx
	leaq	16(%rdi), %rax
	cmpq	%rdx, %r13
	je	.L1103
	movq	%rdi, %rsi
	movq	%rdx, %rdi
	jmp	.L1102
	.p2align 4,,10
	.p2align 3
.L1150:
	leaq	-88(%rbp), %rdi
	movl	$24575, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1096
	xorl	%eax, %eax
.L1097:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1153
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1109:
	.cfi_restore_state
	movq	32(%rbx), %rsi
	movq	24(%rsi), %rdi
	cmpq	%rdi, %r13
	je	.L1112
	leaq	24(%rsi), %r14
	jmp	.L1111
	.p2align 4,,10
	.p2align 3
.L1152:
	movq	8(%r15), %rax
	movq	%rax, -80(%rbp)
	cmpq	$7263, %rax
	jne	.L1107
.L1149:
	movq	%r15, %r13
	jmp	.L1106
	.p2align 4,,10
	.p2align 3
.L1151:
	movq	8(%r14), %rax
	movq	%rax, -72(%rbp)
	cmpq	$7263, %rax
	jne	.L1099
.L1148:
	movq	%r14, %r13
	jmp	.L1098
	.p2align 4,,10
	.p2align 3
.L1099:
	leaq	-72(%rbp), %rdi
	movl	$7263, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1148
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder22PlainPrimitiveToNumberEv@PLT
	movq	%r13, %rdi
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r14, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r13
	jmp	.L1098
	.p2align 4,,10
	.p2align 3
.L1107:
	leaq	-80(%rbp), %rdi
	movl	$7263, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1149
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder22PlainPrimitiveToNumberEv@PLT
	movq	%r13, %rdi
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r15, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r13
	jmp	.L1106
	.p2align 4,,10
	.p2align 3
.L1117:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder13NumberModulusEv@PLT
	movq	%rax, %r13
	.p2align 4,,10
	.p2align 3
.L1128:
	movq	(%rbx), %rdx
	movl	24(%rdx), %eax
	testl	%eax, %eax
	jle	.L1129
	movq	8(%r12), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rbx, %rdx
	movq	%rbx, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
.L1129:
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties20RemoveNonValueInputsEPNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	16(%r12), %rax
	movq	8(%rbx), %rdi
	movl	$7263, %esi
	movq	(%rax), %rax
	movq	(%rax), %rdx
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, 8(%rbx)
	movq	%rbx, %rax
	jmp	.L1097
	.p2align 4,,10
	.p2align 3
.L1126:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder16NumberBitwiseXorEv@PLT
	movq	%rax, %r13
	jmp	.L1128
	.p2align 4,,10
	.p2align 3
.L1127:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder15NumberBitwiseOrEv@PLT
	movq	%rax, %r13
	jmp	.L1128
	.p2align 4,,10
	.p2align 3
.L1115:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9NumberPowEv@PLT
	movq	%rax, %r13
	jmp	.L1128
	.p2align 4,,10
	.p2align 3
.L1118:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder12NumberDivideEv@PLT
	movq	%rax, %r13
	jmp	.L1128
	.p2align 4,,10
	.p2align 3
.L1119:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14NumberMultiplyEv@PLT
	movq	%rax, %r13
	jmp	.L1128
	.p2align 4,,10
	.p2align 3
.L1120:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14NumberSubtractEv@PLT
	movq	%rax, %r13
	jmp	.L1128
	.p2align 4,,10
	.p2align 3
.L1121:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9NumberAddEv@PLT
	movq	%rax, %r13
	jmp	.L1128
	.p2align 4,,10
	.p2align 3
.L1122:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder23NumberShiftRightLogicalEv@PLT
	movq	%rax, %r13
	jmp	.L1128
	.p2align 4,,10
	.p2align 3
.L1123:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder16NumberShiftRightEv@PLT
	movq	%rax, %r13
	jmp	.L1128
	.p2align 4,,10
	.p2align 3
.L1124:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder15NumberShiftLeftEv@PLT
	movq	%rax, %r13
	jmp	.L1128
	.p2align 4,,10
	.p2align 3
.L1125:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder16NumberBitwiseAndEv@PLT
	movq	%rax, %r13
	jmp	.L1128
.L1153:
	call	__stack_chk_fail@PLT
.L1114:
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE23799:
	.size	_ZN2v88internal8compiler15JSTypedLowering17ReduceJSIncrementEPNS1_4NodeE, .-_ZN2v88internal8compiler15JSTypedLowering17ReduceJSIncrementEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler15JSTypedLowering18ReduceJSBitwiseNotEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15JSTypedLowering18ReduceJSBitwiseNotEPNS1_4NodeE
	.type	_ZN2v88internal8compiler15JSTypedLowering18ReduceJSBitwiseNotEPNS1_4NodeE, @function
_ZN2v88internal8compiler15JSTypedLowering18ReduceJSBitwiseNotEPNS1_4NodeE:
.LFB23797:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	8(%rax), %rax
	movq	%rax, -96(%rbp)
	cmpq	$24575, %rax
	jne	.L1240
.L1155:
	movsd	.LC10(%rip), %xmm0
	movq	16(%r12), %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movl	$1, %edx
	movq	%rbx, %rdi
	movq	%rax, %rcx
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%r12), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder10BitwiseXorEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler15JSTypedLowering21ReduceJSToNumberInputEPNS1_4NodeE
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1241
.L1157:
	movzbl	23(%rbx), %eax
	movq	32(%rbx), %rdi
	leaq	32(%rbx), %r14
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1160
	movq	%r14, %rax
	movq	%rbx, %rsi
	cmpq	%r13, %rdi
	je	.L1162
.L1161:
	leaq	-24(%rsi), %r15
	testq	%rdi, %rdi
	je	.L1164
	movq	%r15, %rsi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-104(%rbp), %rax
.L1164:
	movq	%r13, (%rax)
	testq	%r13, %r13
	je	.L1162
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L1162:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler15JSTypedLowering21ReduceJSToNumberInputEPNS1_4NodeE
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1242
.L1165:
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1168
	movq	8(%r14), %rdi
	cmpq	%rdi, %r13
	je	.L1171
	leaq	8(%r14), %rax
	movq	%rbx, %rsi
.L1170:
	leaq	-48(%rsi), %r15
	testq	%rdi, %rdi
	je	.L1172
	movq	%r15, %rsi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-104(%rbp), %rax
.L1172:
	movq	%r13, (%rax)
	testq	%r13, %r13
	je	.L1171
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L1171:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rax, %r13
	movq	8(%rax), %rax
	movq	%rax, -72(%rbp)
	cmpq	$1099, %rax
	je	.L1173
	leaq	-72(%rbp), %rdi
	movl	$1099, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L1243
.L1173:
	movzbl	23(%rbx), %eax
	movq	32(%rbx), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1174
	movq	%r14, %rax
	movq	%rbx, %rsi
	cmpq	%r13, %rdi
	je	.L1176
.L1175:
	leaq	-24(%rsi), %r15
	testq	%rdi, %rdi
	je	.L1178
	movq	%r15, %rsi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-104(%rbp), %rax
.L1178:
	movq	%r13, (%rax)
	testq	%r13, %r13
	je	.L1176
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L1176:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rax, %r13
	movq	8(%rax), %rax
	movq	%rax, -72(%rbp)
	cmpq	$1099, %rax
	je	.L1179
	leaq	-72(%rbp), %rdi
	movl	$1099, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L1244
.L1179:
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1180
	movq	8(%r14), %rdi
	cmpq	%rdi, %r13
	je	.L1183
	addq	$8, %r14
	movq	%rbx, %rsi
.L1182:
	leaq	-48(%rsi), %r15
	testq	%rdi, %rdi
	je	.L1184
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L1184:
	movq	%r13, (%r14)
	testq	%r13, %r13
	je	.L1183
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L1183:
	movq	(%rbx), %rax
	movzwl	16(%rax), %eax
	subw	$688, %ax
	cmpw	$11, %ax
	ja	.L1185
	leaq	.L1187(%rip), %rdx
	movzwl	%ax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler15JSTypedLowering18ReduceJSBitwiseNotEPNS1_4NodeE,"a",@progbits
	.align 4
	.align 4
.L1187:
	.long	.L1198-.L1187
	.long	.L1197-.L1187
	.long	.L1196-.L1187
	.long	.L1195-.L1187
	.long	.L1194-.L1187
	.long	.L1193-.L1187
	.long	.L1192-.L1187
	.long	.L1191-.L1187
	.long	.L1190-.L1187
	.long	.L1189-.L1187
	.long	.L1188-.L1187
	.long	.L1186-.L1187
	.section	.text._ZN2v88internal8compiler15JSTypedLowering18ReduceJSBitwiseNotEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L1160:
	movq	16(%rdi), %rdx
	leaq	16(%rdi), %rax
	cmpq	%rdx, %r13
	je	.L1162
	movq	%rdi, %rsi
	movq	%rdx, %rdi
	jmp	.L1161
	.p2align 4,,10
	.p2align 3
.L1240:
	leaq	-96(%rbp), %rdi
	movl	$24575, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1155
	xorl	%eax, %eax
.L1156:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1245
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1168:
	.cfi_restore_state
	movq	32(%rbx), %rsi
	movq	24(%rsi), %rdi
	cmpq	%rdi, %r13
	je	.L1171
	leaq	24(%rsi), %rax
	jmp	.L1170
	.p2align 4,,10
	.p2align 3
.L1180:
	movq	32(%rbx), %rsi
	movq	24(%rsi), %rdi
	cmpq	%r13, %rdi
	je	.L1183
	leaq	24(%rsi), %r14
	jmp	.L1182
	.p2align 4,,10
	.p2align 3
.L1174:
	movq	16(%rdi), %rdx
	leaq	16(%rdi), %rax
	cmpq	%r13, %rdx
	je	.L1176
	movq	%rdi, %rsi
	movq	%rdx, %rdi
	jmp	.L1175
	.p2align 4,,10
	.p2align 3
.L1242:
	movq	8(%r15), %rax
	movq	%rax, -88(%rbp)
	cmpq	$7263, %rax
	jne	.L1166
.L1239:
	movq	%r15, %r13
	jmp	.L1165
	.p2align 4,,10
	.p2align 3
.L1241:
	movq	8(%r14), %rax
	movq	%rax, -80(%rbp)
	cmpq	$7263, %rax
	jne	.L1158
.L1238:
	movq	%r14, %r13
	jmp	.L1157
	.p2align 4,,10
	.p2align 3
.L1244:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder13NumberToInt32Ev@PLT
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r13, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r13
	jmp	.L1179
	.p2align 4,,10
	.p2align 3
.L1243:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder13NumberToInt32Ev@PLT
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r13, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r13
	jmp	.L1173
	.p2align 4,,10
	.p2align 3
.L1158:
	leaq	-80(%rbp), %rdi
	movl	$7263, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1238
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder22PlainPrimitiveToNumberEv@PLT
	movq	%r13, %rdi
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r14, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r13
	jmp	.L1157
	.p2align 4,,10
	.p2align 3
.L1166:
	leaq	-88(%rbp), %rdi
	movl	$7263, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1239
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder22PlainPrimitiveToNumberEv@PLT
	movq	%r13, %rdi
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r15, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r13
	jmp	.L1165
	.p2align 4,,10
	.p2align 3
.L1188:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder13NumberModulusEv@PLT
	movq	%rax, %r13
	.p2align 4,,10
	.p2align 3
.L1199:
	movq	(%rbx), %rdx
	movl	24(%rdx), %eax
	testl	%eax, %eax
	jle	.L1200
	movq	8(%r12), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rbx, %rdx
	movq	%rbx, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
.L1200:
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties20RemoveNonValueInputsEPNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	16(%r12), %rax
	movq	8(%rbx), %rdi
	movl	$1099, %esi
	movq	(%rax), %rax
	movq	(%rax), %rdx
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, 8(%rbx)
	movq	%rbx, %rax
	jmp	.L1156
	.p2align 4,,10
	.p2align 3
.L1189:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder12NumberDivideEv@PLT
	movq	%rax, %r13
	jmp	.L1199
	.p2align 4,,10
	.p2align 3
.L1190:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14NumberMultiplyEv@PLT
	movq	%rax, %r13
	jmp	.L1199
	.p2align 4,,10
	.p2align 3
.L1191:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14NumberSubtractEv@PLT
	movq	%rax, %r13
	jmp	.L1199
	.p2align 4,,10
	.p2align 3
.L1192:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9NumberAddEv@PLT
	movq	%rax, %r13
	jmp	.L1199
	.p2align 4,,10
	.p2align 3
.L1193:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder23NumberShiftRightLogicalEv@PLT
	movq	%rax, %r13
	jmp	.L1199
	.p2align 4,,10
	.p2align 3
.L1194:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder16NumberShiftRightEv@PLT
	movq	%rax, %r13
	jmp	.L1199
	.p2align 4,,10
	.p2align 3
.L1195:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder15NumberShiftLeftEv@PLT
	movq	%rax, %r13
	jmp	.L1199
	.p2align 4,,10
	.p2align 3
.L1196:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder16NumberBitwiseAndEv@PLT
	movq	%rax, %r13
	jmp	.L1199
	.p2align 4,,10
	.p2align 3
.L1197:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder16NumberBitwiseXorEv@PLT
	movq	%rax, %r13
	jmp	.L1199
	.p2align 4,,10
	.p2align 3
.L1198:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder15NumberBitwiseOrEv@PLT
	movq	%rax, %r13
	jmp	.L1199
	.p2align 4,,10
	.p2align 3
.L1186:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9NumberPowEv@PLT
	movq	%rax, %r13
	jmp	.L1199
.L1245:
	call	__stack_chk_fail@PLT
.L1185:
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE23797:
	.size	_ZN2v88internal8compiler15JSTypedLowering18ReduceJSBitwiseNotEPNS1_4NodeE, .-_ZN2v88internal8compiler15JSTypedLowering18ReduceJSBitwiseNotEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler15JSTypedLowering17ReduceNumberBinopEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15JSTypedLowering17ReduceNumberBinopEPNS1_4NodeE
	.type	_ZN2v88internal8compiler15JSTypedLowering17ReduceNumberBinopEPNS1_4NodeE, @function
_ZN2v88internal8compiler15JSTypedLowering17ReduceNumberBinopEPNS1_4NodeE:
.LFB23808:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	32(%rsi), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	32(%rsi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1247
	movq	16(%rdx), %rdx
.L1247:
	movq	8(%rdx), %rdx
	movq	%rdx, -72(%rbp)
	cmpq	$24575, %rdx
	jne	.L1316
	cmpl	$15, %eax
	je	.L1252
.L1320:
	leaq	8(%r13), %rax
.L1253:
	movq	(%rax), %rax
	movq	8(%rax), %rax
	movq	%rax, -72(%rbp)
	cmpq	$24575, %rax
	jne	.L1317
.L1254:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler15JSTypedLowering21ReduceJSToNumberInputEPNS1_4NodeE
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1318
.L1256:
	movzbl	23(%rbx), %eax
	movq	32(%rbx), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1260
	movq	%r13, %rax
	movq	%rbx, %rsi
	cmpq	%r14, %rdi
	je	.L1262
.L1261:
	leaq	-24(%rsi), %r15
	testq	%rdi, %rdi
	je	.L1264
	movq	%r15, %rsi
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-88(%rbp), %rax
.L1264:
	movq	%r14, (%rax)
	testq	%r14, %r14
	je	.L1262
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L1262:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler15JSTypedLowering21ReduceJSToNumberInputEPNS1_4NodeE
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1319
.L1265:
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1268
	movq	8(%r13), %rdi
	cmpq	%r14, %rdi
	je	.L1271
	addq	$8, %r13
	movq	%rbx, %rsi
.L1270:
	leaq	-48(%rsi), %r15
	testq	%rdi, %rdi
	je	.L1272
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L1272:
	movq	%r14, 0(%r13)
	testq	%r14, %r14
	je	.L1271
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L1271:
	movq	(%rbx), %rax
	movzwl	16(%rax), %eax
	subw	$688, %ax
	cmpw	$11, %ax
	ja	.L1273
	leaq	.L1275(%rip), %rdx
	movzwl	%ax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler15JSTypedLowering17ReduceNumberBinopEPNS1_4NodeE,"a",@progbits
	.align 4
	.align 4
.L1275:
	.long	.L1286-.L1275
	.long	.L1285-.L1275
	.long	.L1284-.L1275
	.long	.L1283-.L1275
	.long	.L1282-.L1275
	.long	.L1281-.L1275
	.long	.L1280-.L1275
	.long	.L1279-.L1275
	.long	.L1278-.L1275
	.long	.L1277-.L1275
	.long	.L1276-.L1275
	.long	.L1274-.L1275
	.section	.text._ZN2v88internal8compiler15JSTypedLowering17ReduceNumberBinopEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L1316:
	leaq	-72(%rbp), %rdi
	movl	$24575, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L1251
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1320
.L1252:
	movq	32(%rbx), %rax
	addq	$24, %rax
	jmp	.L1253
	.p2align 4,,10
	.p2align 3
.L1317:
	leaq	-72(%rbp), %rdi
	movl	$24575, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1254
.L1251:
	xorl	%eax, %eax
.L1289:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1321
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1260:
	.cfi_restore_state
	movq	16(%rdi), %rdx
	leaq	16(%rdi), %rax
	cmpq	%rdx, %r14
	je	.L1262
	movq	%rdi, %rsi
	movq	%rdx, %rdi
	jmp	.L1261
	.p2align 4,,10
	.p2align 3
.L1268:
	movq	32(%rbx), %rsi
	movq	24(%rsi), %rdi
	cmpq	%r14, %rdi
	je	.L1271
	leaq	24(%rsi), %r13
	jmp	.L1270
	.p2align 4,,10
	.p2align 3
.L1318:
	movq	8(%r15), %rax
	movq	%rax, -72(%rbp)
	cmpq	$7263, %rax
	jne	.L1258
.L1314:
	movq	%r15, %r14
	jmp	.L1256
	.p2align 4,,10
	.p2align 3
.L1319:
	movq	8(%r15), %rax
	movq	%rax, -80(%rbp)
	cmpq	$7263, %rax
	jne	.L1266
.L1315:
	movq	%r15, %r14
	jmp	.L1265
	.p2align 4,,10
	.p2align 3
.L1277:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder12NumberDivideEv@PLT
	movq	%rax, %r13
	.p2align 4,,10
	.p2align 3
.L1287:
	movq	(%rbx), %rax
	movl	24(%rax), %eax
	testl	%eax, %eax
	jle	.L1288
	movq	8(%r12), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rbx, %rdx
	movq	%rbx, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
.L1288:
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties20RemoveNonValueInputsEPNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	16(%r12), %rax
	movq	8(%rbx), %rdi
	movl	$7263, %esi
	movq	(%rax), %rax
	movq	(%rax), %rdx
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, 8(%rbx)
	movq	%rbx, %rax
	jmp	.L1289
	.p2align 4,,10
	.p2align 3
.L1274:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9NumberPowEv@PLT
	movq	%rax, %r13
	jmp	.L1287
	.p2align 4,,10
	.p2align 3
.L1286:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder15NumberBitwiseOrEv@PLT
	movq	%rax, %r13
	jmp	.L1287
	.p2align 4,,10
	.p2align 3
.L1285:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder16NumberBitwiseXorEv@PLT
	movq	%rax, %r13
	jmp	.L1287
	.p2align 4,,10
	.p2align 3
.L1284:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder16NumberBitwiseAndEv@PLT
	movq	%rax, %r13
	jmp	.L1287
	.p2align 4,,10
	.p2align 3
.L1283:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder15NumberShiftLeftEv@PLT
	movq	%rax, %r13
	jmp	.L1287
	.p2align 4,,10
	.p2align 3
.L1282:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder16NumberShiftRightEv@PLT
	movq	%rax, %r13
	jmp	.L1287
	.p2align 4,,10
	.p2align 3
.L1281:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder23NumberShiftRightLogicalEv@PLT
	movq	%rax, %r13
	jmp	.L1287
	.p2align 4,,10
	.p2align 3
.L1276:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder13NumberModulusEv@PLT
	movq	%rax, %r13
	jmp	.L1287
	.p2align 4,,10
	.p2align 3
.L1280:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9NumberAddEv@PLT
	movq	%rax, %r13
	jmp	.L1287
	.p2align 4,,10
	.p2align 3
.L1279:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14NumberSubtractEv@PLT
	movq	%rax, %r13
	jmp	.L1287
	.p2align 4,,10
	.p2align 3
.L1278:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14NumberMultiplyEv@PLT
	movq	%rax, %r13
	jmp	.L1287
	.p2align 4,,10
	.p2align 3
.L1266:
	leaq	-80(%rbp), %rdi
	movl	$7263, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1315
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder22PlainPrimitiveToNumberEv@PLT
	movq	%r14, %rdi
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r15, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r14
	jmp	.L1265
	.p2align 4,,10
	.p2align 3
.L1258:
	leaq	-72(%rbp), %rdi
	movl	$7263, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1314
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder22PlainPrimitiveToNumberEv@PLT
	movq	%r14, %rdi
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r15, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r14
	jmp	.L1256
.L1321:
	call	__stack_chk_fail@PLT
.L1273:
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE23808:
	.size	_ZN2v88internal8compiler15JSTypedLowering17ReduceNumberBinopEPNS1_4NodeE, .-_ZN2v88internal8compiler15JSTypedLowering17ReduceNumberBinopEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler15JSTypedLowering16ReduceInt32BinopEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15JSTypedLowering16ReduceInt32BinopEPNS1_4NodeE
	.type	_ZN2v88internal8compiler15JSTypedLowering16ReduceInt32BinopEPNS1_4NodeE, @function
_ZN2v88internal8compiler15JSTypedLowering16ReduceInt32BinopEPNS1_4NodeE:
.LFB23809:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	32(%rsi), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$72, %rsp
	movq	32(%rsi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1323
	movq	16(%rdx), %rdx
.L1323:
	movq	8(%rdx), %rdx
	movq	%rdx, -72(%rbp)
	cmpq	$24575, %rdx
	jne	.L1423
	cmpl	$15, %eax
	je	.L1328
.L1429:
	leaq	8(%r13), %rax
.L1329:
	movq	(%rax), %rax
	movq	8(%rax), %rax
	movq	%rax, -72(%rbp)
	cmpq	$24575, %rax
	jne	.L1424
.L1330:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler15JSTypedLowering21ReduceJSToNumberInputEPNS1_4NodeE
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1425
.L1332:
	movzbl	23(%rbx), %eax
	movq	32(%rbx), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1336
	movq	%r13, %rax
	movq	%rbx, %rsi
	cmpq	%rdi, %r14
	je	.L1338
.L1337:
	leaq	-24(%rsi), %r15
	testq	%rdi, %rdi
	je	.L1340
	movq	%r15, %rsi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-104(%rbp), %rax
.L1340:
	movq	%r14, (%rax)
	testq	%r14, %r14
	je	.L1338
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L1338:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler15JSTypedLowering21ReduceJSToNumberInputEPNS1_4NodeE
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1426
.L1341:
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1344
	movq	8(%r13), %rdi
	cmpq	%r14, %rdi
	je	.L1347
	leaq	8(%r13), %rax
	movq	%rbx, %rsi
.L1346:
	leaq	-48(%rsi), %r15
	testq	%rdi, %rdi
	je	.L1348
	movq	%r15, %rsi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-104(%rbp), %rax
.L1348:
	movq	%r14, (%rax)
	testq	%r14, %r14
	je	.L1347
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L1347:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rax, %r14
	movq	8(%rax), %rax
	movq	%rax, -72(%rbp)
	cmpq	$1099, %rax
	jne	.L1427
.L1349:
	movzbl	23(%rbx), %eax
	movq	32(%rbx), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1350
	movq	%r13, %rax
	movq	%rbx, %rsi
	cmpq	%r14, %rdi
	je	.L1352
.L1351:
	leaq	-24(%rsi), %r15
	testq	%rdi, %rdi
	je	.L1354
	movq	%r15, %rsi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-104(%rbp), %rax
.L1354:
	movq	%r14, (%rax)
	testq	%r14, %r14
	je	.L1352
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L1352:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rax, %r14
	movq	8(%rax), %rax
	movq	%rax, -72(%rbp)
	cmpq	$1099, %rax
	jne	.L1428
.L1355:
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1356
	movq	8(%r13), %rdi
	cmpq	%r14, %rdi
	je	.L1359
	addq	$8, %r13
	movq	%rbx, %rsi
.L1358:
	leaq	-48(%rsi), %r15
	testq	%rdi, %rdi
	je	.L1360
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L1360:
	movq	%r14, 0(%r13)
	testq	%r14, %r14
	je	.L1359
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L1359:
	movq	(%rbx), %rax
	movzwl	16(%rax), %eax
	subw	$688, %ax
	cmpw	$11, %ax
	ja	.L1361
	leaq	.L1363(%rip), %rdx
	movzwl	%ax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler15JSTypedLowering16ReduceInt32BinopEPNS1_4NodeE,"a",@progbits
	.align 4
	.align 4
.L1363:
	.long	.L1374-.L1363
	.long	.L1373-.L1363
	.long	.L1372-.L1363
	.long	.L1371-.L1363
	.long	.L1370-.L1363
	.long	.L1369-.L1363
	.long	.L1368-.L1363
	.long	.L1367-.L1363
	.long	.L1366-.L1363
	.long	.L1365-.L1363
	.long	.L1364-.L1363
	.long	.L1362-.L1363
	.section	.text._ZN2v88internal8compiler15JSTypedLowering16ReduceInt32BinopEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L1423:
	leaq	-72(%rbp), %rdi
	movl	$24575, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L1327
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1429
.L1328:
	movq	32(%rbx), %rax
	addq	$24, %rax
	jmp	.L1329
	.p2align 4,,10
	.p2align 3
.L1424:
	leaq	-72(%rbp), %rdi
	movl	$24575, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1330
.L1327:
	xorl	%eax, %eax
.L1377:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1430
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1428:
	.cfi_restore_state
	leaq	-72(%rbp), %rdi
	movl	$1099, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1355
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder13NumberToInt32Ev@PLT
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r14, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r14
	jmp	.L1355
	.p2align 4,,10
	.p2align 3
.L1427:
	leaq	-72(%rbp), %rdi
	movl	$1099, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1349
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder13NumberToInt32Ev@PLT
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r14, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r14
	jmp	.L1349
	.p2align 4,,10
	.p2align 3
.L1336:
	movq	16(%rdi), %rdx
	leaq	16(%rdi), %rax
	cmpq	%rdx, %r14
	je	.L1338
	movq	%rdi, %rsi
	movq	%rdx, %rdi
	jmp	.L1337
	.p2align 4,,10
	.p2align 3
.L1350:
	movq	16(%rdi), %rdx
	leaq	16(%rdi), %rax
	cmpq	%r14, %rdx
	je	.L1352
	movq	%rdi, %rsi
	movq	%rdx, %rdi
	jmp	.L1351
	.p2align 4,,10
	.p2align 3
.L1344:
	movq	32(%rbx), %rsi
	movq	24(%rsi), %rdi
	cmpq	%rdi, %r14
	je	.L1347
	leaq	24(%rsi), %rax
	jmp	.L1346
	.p2align 4,,10
	.p2align 3
.L1356:
	movq	32(%rbx), %rsi
	movq	24(%rsi), %rdi
	cmpq	%rdi, %r14
	je	.L1359
	leaq	24(%rsi), %r13
	jmp	.L1358
	.p2align 4,,10
	.p2align 3
.L1426:
	movq	8(%r15), %rax
	movq	%rax, -88(%rbp)
	cmpq	$7263, %rax
	jne	.L1342
.L1422:
	movq	%r15, %r14
	jmp	.L1341
	.p2align 4,,10
	.p2align 3
.L1425:
	movq	8(%r15), %rax
	movq	%rax, -80(%rbp)
	cmpq	$7263, %rax
	jne	.L1334
.L1421:
	movq	%r15, %r14
	jmp	.L1332
	.p2align 4,,10
	.p2align 3
.L1362:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9NumberPowEv@PLT
	movq	%rax, %r13
	.p2align 4,,10
	.p2align 3
.L1375:
	movq	(%rbx), %rax
	movl	24(%rax), %eax
	testl	%eax, %eax
	jle	.L1376
	movq	8(%r12), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rbx, %rdx
	movq	%rbx, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
.L1376:
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties20RemoveNonValueInputsEPNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	16(%r12), %rax
	movq	8(%rbx), %rdi
	movl	$1099, %esi
	movq	(%rax), %rax
	movq	(%rax), %rdx
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, 8(%rbx)
	movq	%rbx, %rax
	jmp	.L1377
	.p2align 4,,10
	.p2align 3
.L1370:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder16NumberShiftRightEv@PLT
	movq	%rax, %r13
	jmp	.L1375
	.p2align 4,,10
	.p2align 3
.L1369:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder23NumberShiftRightLogicalEv@PLT
	movq	%rax, %r13
	jmp	.L1375
	.p2align 4,,10
	.p2align 3
.L1368:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9NumberAddEv@PLT
	movq	%rax, %r13
	jmp	.L1375
	.p2align 4,,10
	.p2align 3
.L1367:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14NumberSubtractEv@PLT
	movq	%rax, %r13
	jmp	.L1375
	.p2align 4,,10
	.p2align 3
.L1366:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14NumberMultiplyEv@PLT
	movq	%rax, %r13
	jmp	.L1375
	.p2align 4,,10
	.p2align 3
.L1365:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder12NumberDivideEv@PLT
	movq	%rax, %r13
	jmp	.L1375
	.p2align 4,,10
	.p2align 3
.L1364:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder13NumberModulusEv@PLT
	movq	%rax, %r13
	jmp	.L1375
	.p2align 4,,10
	.p2align 3
.L1374:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder15NumberBitwiseOrEv@PLT
	movq	%rax, %r13
	jmp	.L1375
	.p2align 4,,10
	.p2align 3
.L1373:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder16NumberBitwiseXorEv@PLT
	movq	%rax, %r13
	jmp	.L1375
	.p2align 4,,10
	.p2align 3
.L1372:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder16NumberBitwiseAndEv@PLT
	movq	%rax, %r13
	jmp	.L1375
	.p2align 4,,10
	.p2align 3
.L1371:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder15NumberShiftLeftEv@PLT
	movq	%rax, %r13
	jmp	.L1375
	.p2align 4,,10
	.p2align 3
.L1334:
	leaq	-80(%rbp), %rdi
	movl	$7263, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1421
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder22PlainPrimitiveToNumberEv@PLT
	movq	%r14, %rdi
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r15, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r14
	jmp	.L1332
	.p2align 4,,10
	.p2align 3
.L1342:
	leaq	-88(%rbp), %rdi
	movl	$7263, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1422
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder22PlainPrimitiveToNumberEv@PLT
	movq	%r14, %rdi
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r15, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r14
	jmp	.L1341
.L1430:
	call	__stack_chk_fail@PLT
.L1361:
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE23809:
	.size	_ZN2v88internal8compiler15JSTypedLowering16ReduceInt32BinopEPNS1_4NodeE, .-_ZN2v88internal8compiler15JSTypedLowering16ReduceInt32BinopEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler15JSTypedLowering15ReduceUI32ShiftEPNS1_4NodeENS1_10SignednessE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15JSTypedLowering15ReduceUI32ShiftEPNS1_4NodeENS1_10SignednessE
	.type	_ZN2v88internal8compiler15JSTypedLowering15ReduceUI32ShiftEPNS1_4NodeENS1_10SignednessE, @function
_ZN2v88internal8compiler15JSTypedLowering15ReduceUI32ShiftEPNS1_4NodeENS1_10SignednessE:
.LFB23810:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	32(%rsi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$72, %rsp
	movq	32(%rsi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1432
	movq	16(%rdx), %rdx
.L1432:
	movq	8(%rdx), %rdx
	movq	%rdx, -72(%rbp)
	cmpq	$24575, %rdx
	jne	.L1539
	cmpl	$15, %eax
	je	.L1437
.L1544:
	leaq	8(%r14), %rax
.L1438:
	movq	(%rax), %rax
	movq	8(%rax), %rax
	movq	%rax, -72(%rbp)
	cmpq	$24575, %rax
	jne	.L1540
.L1439:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal8compiler15JSTypedLowering21ReduceJSToNumberInputEPNS1_4NodeE
	movq	-104(%rbp), %rdx
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L1541
.L1441:
	movzbl	23(%rbx), %eax
	movq	32(%rbx), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1445
	movq	%r14, %rax
	movq	%rbx, %rsi
	cmpq	%r15, %rdi
	je	.L1447
.L1446:
	subq	$24, %rsi
	testq	%rdi, %rdi
	je	.L1449
	movq	%rax, -112(%rbp)
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-112(%rbp), %rax
	movq	-104(%rbp), %rsi
.L1449:
	movq	%r15, (%rax)
	testq	%r15, %r15
	je	.L1447
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L1447:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal8compiler15JSTypedLowering21ReduceJSToNumberInputEPNS1_4NodeE
	movq	-104(%rbp), %rdx
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L1542
.L1450:
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1453
	movq	8(%r14), %rdi
	cmpq	%rdi, %r15
	je	.L1456
	leaq	8(%r14), %rax
	movq	%rbx, %rsi
.L1455:
	subq	$48, %rsi
	testq	%rdi, %rdi
	je	.L1457
	movq	%rax, -112(%rbp)
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-112(%rbp), %rax
	movq	-104(%rbp), %rsi
.L1457:
	movq	%r15, (%rax)
	testq	%r15, %r15
	je	.L1456
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L1456:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rax, %r15
	movq	8(%rax), %rax
	movq	%rax, -72(%rbp)
	testl	%r13d, %r13d
	jne	.L1458
	cmpq	$1099, %rax
	je	.L1459
	leaq	-72(%rbp), %rdi
	movl	$1099, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L1543
	.p2align 4,,10
	.p2align 3
.L1459:
	movzbl	23(%rbx), %eax
	movq	32(%rbx), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1460
	movq	%r14, %rax
	movq	%rbx, %rsi
	cmpq	%r15, %rdi
	je	.L1462
.L1461:
	subq	$24, %rsi
	testq	%rdi, %rdi
	je	.L1464
	movq	%rax, -112(%rbp)
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-112(%rbp), %rax
	movq	-104(%rbp), %rsi
.L1464:
	movq	%r15, (%rax)
	testq	%r15, %r15
	je	.L1462
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L1462:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rax, %r15
	movq	8(%rax), %rax
	movq	%rax, -72(%rbp)
	cmpq	$1031, %rax
	je	.L1465
	leaq	-72(%rbp), %rdi
	movl	$1031, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1465
	movq	16(%r12), %rax
	movq	(%rax), %r9
	movq	376(%rax), %rdi
	movq	%r9, -104(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14NumberToUint32Ev@PLT
	movq	-104(%rbp), %r9
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r15, -64(%rbp)
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r15
.L1465:
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1466
	movq	8(%r14), %rdi
	cmpq	%r15, %rdi
	je	.L1469
	addq	$8, %r14
	movq	%rbx, %rsi
.L1468:
	subq	$48, %rsi
	testq	%rdi, %rdi
	je	.L1470
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-104(%rbp), %rsi
.L1470:
	movq	%r15, (%r14)
	testq	%r15, %r15
	je	.L1469
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L1469:
	cmpl	$1, %r13d
	movl	$1099, %eax
	movl	$1031, %r13d
	cmovne	%rax, %r13
	movq	(%rbx), %rax
	movzwl	16(%rax), %eax
	subw	$688, %ax
	cmpw	$11, %ax
	ja	.L1472
	leaq	.L1474(%rip), %rdx
	movzwl	%ax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler15JSTypedLowering15ReduceUI32ShiftEPNS1_4NodeENS1_10SignednessE,"a",@progbits
	.align 4
	.align 4
.L1474:
	.long	.L1485-.L1474
	.long	.L1484-.L1474
	.long	.L1483-.L1474
	.long	.L1482-.L1474
	.long	.L1481-.L1474
	.long	.L1480-.L1474
	.long	.L1479-.L1474
	.long	.L1478-.L1474
	.long	.L1477-.L1474
	.long	.L1476-.L1474
	.long	.L1475-.L1474
	.long	.L1473-.L1474
	.section	.text._ZN2v88internal8compiler15JSTypedLowering15ReduceUI32ShiftEPNS1_4NodeENS1_10SignednessE
	.p2align 4,,10
	.p2align 3
.L1539:
	leaq	-72(%rbp), %rdi
	movl	$24575, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L1436
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1544
.L1437:
	movq	32(%rbx), %rax
	addq	$24, %rax
	jmp	.L1438
	.p2align 4,,10
	.p2align 3
.L1540:
	leaq	-72(%rbp), %rdi
	movl	$24575, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1439
.L1436:
	xorl	%eax, %eax
.L1488:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1545
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1458:
	.cfi_restore_state
	cmpq	$1031, %rax
	je	.L1459
	leaq	-72(%rbp), %rdi
	movl	$1031, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1459
	movq	16(%r12), %rax
	movq	(%rax), %r9
	movq	376(%rax), %rdi
	movq	%r9, -104(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14NumberToUint32Ev@PLT
	jmp	.L1538
	.p2align 4,,10
	.p2align 3
.L1466:
	movq	32(%rbx), %rsi
	movq	24(%rsi), %rdi
	cmpq	%r15, %rdi
	je	.L1469
	leaq	24(%rsi), %r14
	jmp	.L1468
	.p2align 4,,10
	.p2align 3
.L1460:
	movq	16(%rdi), %rdx
	leaq	16(%rdi), %rax
	cmpq	%rdx, %r15
	je	.L1462
	movq	%rdi, %rsi
	movq	%rdx, %rdi
	jmp	.L1461
	.p2align 4,,10
	.p2align 3
.L1453:
	movq	32(%rbx), %rsi
	movq	24(%rsi), %rdi
	cmpq	%rdi, %r15
	je	.L1456
	leaq	24(%rsi), %rax
	jmp	.L1455
	.p2align 4,,10
	.p2align 3
.L1445:
	movq	16(%rdi), %rdx
	leaq	16(%rdi), %rax
	cmpq	%rdx, %r15
	je	.L1447
	movq	%rdi, %rsi
	movq	%rdx, %rdi
	jmp	.L1446
	.p2align 4,,10
	.p2align 3
.L1541:
	movq	8(%rdx), %rax
	movq	%rax, -80(%rbp)
	cmpq	$7263, %rax
	jne	.L1443
.L1536:
	movq	%rdx, %r15
	jmp	.L1441
	.p2align 4,,10
	.p2align 3
.L1542:
	movq	8(%rdx), %rax
	movq	%rax, -88(%rbp)
	cmpq	$7263, %rax
	jne	.L1451
.L1537:
	movq	%rdx, %r15
	jmp	.L1450
	.p2align 4,,10
	.p2align 3
.L1543:
	movq	16(%r12), %rax
	movq	(%rax), %r9
	movq	376(%rax), %rdi
	movq	%r9, -104(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder13NumberToInt32Ev@PLT
.L1538:
	movq	-104(%rbp), %r9
	movq	%rax, %rsi
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%r15, -64(%rbp)
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r15
	jmp	.L1459
	.p2align 4,,10
	.p2align 3
.L1475:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder13NumberModulusEv@PLT
	movq	%rax, %r14
	.p2align 4,,10
	.p2align 3
.L1486:
	movq	(%rbx), %rax
	movl	24(%rax), %eax
	testl	%eax, %eax
	jle	.L1487
	movq	8(%r12), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rbx, %rdx
	movq	%rbx, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
.L1487:
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties20RemoveNonValueInputsEPNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	16(%r12), %rax
	movq	8(%rbx), %rdi
	movq	%r13, %rsi
	movq	(%rax), %rax
	movq	(%rax), %rdx
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, 8(%rbx)
	movq	%rbx, %rax
	jmp	.L1488
	.p2align 4,,10
	.p2align 3
.L1476:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder12NumberDivideEv@PLT
	movq	%rax, %r14
	jmp	.L1486
	.p2align 4,,10
	.p2align 3
.L1477:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14NumberMultiplyEv@PLT
	movq	%rax, %r14
	jmp	.L1486
	.p2align 4,,10
	.p2align 3
.L1478:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14NumberSubtractEv@PLT
	movq	%rax, %r14
	jmp	.L1486
	.p2align 4,,10
	.p2align 3
.L1479:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9NumberAddEv@PLT
	movq	%rax, %r14
	jmp	.L1486
	.p2align 4,,10
	.p2align 3
.L1480:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder23NumberShiftRightLogicalEv@PLT
	movq	%rax, %r14
	jmp	.L1486
	.p2align 4,,10
	.p2align 3
.L1481:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder16NumberShiftRightEv@PLT
	movq	%rax, %r14
	jmp	.L1486
	.p2align 4,,10
	.p2align 3
.L1482:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder15NumberShiftLeftEv@PLT
	movq	%rax, %r14
	jmp	.L1486
	.p2align 4,,10
	.p2align 3
.L1483:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder16NumberBitwiseAndEv@PLT
	movq	%rax, %r14
	jmp	.L1486
	.p2align 4,,10
	.p2align 3
.L1484:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder16NumberBitwiseXorEv@PLT
	movq	%rax, %r14
	jmp	.L1486
	.p2align 4,,10
	.p2align 3
.L1485:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder15NumberBitwiseOrEv@PLT
	movq	%rax, %r14
	jmp	.L1486
	.p2align 4,,10
	.p2align 3
.L1473:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9NumberPowEv@PLT
	movq	%rax, %r14
	jmp	.L1486
	.p2align 4,,10
	.p2align 3
.L1443:
	leaq	-80(%rbp), %rdi
	movl	$7263, %esi
	movq	%rdx, -104(%rbp)
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	movq	-104(%rbp), %rdx
	testb	%al, %al
	jne	.L1536
	movq	16(%r12), %rax
	movq	%rdx, -104(%rbp)
	movq	376(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder22PlainPrimitiveToNumberEv@PLT
	movq	-104(%rbp), %rdx
	movq	%r15, %rdi
	leaq	-64(%rbp), %rcx
	movq	%rax, %rsi
	xorl	%r8d, %r8d
	movq	%rdx, -64(%rbp)
	movl	$1, %edx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r15
	jmp	.L1441
	.p2align 4,,10
	.p2align 3
.L1451:
	leaq	-88(%rbp), %rdi
	movl	$7263, %esi
	movq	%rdx, -104(%rbp)
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	movq	-104(%rbp), %rdx
	testb	%al, %al
	jne	.L1537
	movq	16(%r12), %rax
	movq	%rdx, -104(%rbp)
	movq	376(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder22PlainPrimitiveToNumberEv@PLT
	movq	-104(%rbp), %rdx
	movq	%r15, %rdi
	leaq	-64(%rbp), %rcx
	movq	%rax, %rsi
	xorl	%r8d, %r8d
	movq	%rdx, -64(%rbp)
	movl	$1, %edx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r15
	jmp	.L1450
.L1545:
	call	__stack_chk_fail@PLT
.L1472:
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE23810:
	.size	_ZN2v88internal8compiler15JSTypedLowering15ReduceUI32ShiftEPNS1_4NodeENS1_10SignednessE, .-_ZN2v88internal8compiler15JSTypedLowering15ReduceUI32ShiftEPNS1_4NodeENS1_10SignednessE
	.section	.text._ZN2v88internal8compiler15JSTypedLowering16ReduceJSToNumberEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15JSTypedLowering16ReduceJSToNumberEPNS1_4NodeE
	.type	_ZN2v88internal8compiler15JSTypedLowering16ReduceJSToNumberEPNS1_4NodeE, @function
_ZN2v88internal8compiler15JSTypedLowering16ReduceJSToNumberEPNS1_4NodeE:
.LFB23818:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	32(%rsi), %r14
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1547
	movq	16(%r14), %r14
.L1547:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler15JSTypedLowering21ReduceJSToNumberInputEPNS1_4NodeE
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L1548
	movq	8(%r13), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%rbx, %rax
.L1549:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1556
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1548:
	.cfi_restore_state
	movq	8(%r14), %rax
	movq	%rax, -48(%rbp)
	cmpq	$24575, %rax
	jne	.L1557
.L1550:
	movq	8(%r13), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdx
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node14TrimInputCountEi@PLT
	movq	16(%r13), %rax
	movq	8(%r12), %rdi
	movl	$7263, %esi
	movq	(%rax), %rax
	movq	(%rax), %rdx
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, 8(%r12)
	movq	16(%r13), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder22PlainPrimitiveToNumberEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	%r12, %rax
	jmp	.L1549
	.p2align 4,,10
	.p2align 3
.L1557:
	leaq	-48(%rbp), %rdi
	movl	$24575, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1550
	xorl	%eax, %eax
	jmp	.L1549
.L1556:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23818:
	.size	_ZN2v88internal8compiler15JSTypedLowering16ReduceJSToNumberEPNS1_4NodeE, .-_ZN2v88internal8compiler15JSTypedLowering16ReduceJSToNumberEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler15JSTypedLowering17ReduceJSToNumericEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15JSTypedLowering17ReduceJSToNumericEPNS1_4NodeE
	.type	_ZN2v88internal8compiler15JSTypedLowering17ReduceJSToNumericEPNS1_4NodeE, @function
_ZN2v88internal8compiler15JSTypedLowering17ReduceJSToNumericEPNS1_4NodeE:
.LFB23819:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	8(%rax), %rax
	movq	%rax, -32(%rbp)
	cmpq	$32767, %rax
	jne	.L1568
.L1559:
	movq	16(%r12), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder8ToNumberEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler15JSTypedLowering16ReduceJSToNumberEPNS1_4NodeE
	testq	%rax, %rax
	cmove	%rbx, %rax
.L1560:
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L1569
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1568:
	.cfi_restore_state
	leaq	-32(%rbp), %rdi
	movl	$32767, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1559
	xorl	%eax, %eax
	jmp	.L1560
.L1569:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23819:
	.size	_ZN2v88internal8compiler15JSTypedLowering17ReduceJSToNumericEPNS1_4NodeE, .-_ZN2v88internal8compiler15JSTypedLowering17ReduceJSToNumericEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler15JSTypedLowering16ReduceJSToStringEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15JSTypedLowering16ReduceJSToStringEPNS1_4NodeE
	.type	_ZN2v88internal8compiler15JSTypedLowering16ReduceJSToStringEPNS1_4NodeE, @function
_ZN2v88internal8compiler15JSTypedLowering16ReduceJSToStringEPNS1_4NodeE:
.LFB23821:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movzbl	23(%r12), %eax
	movq	32(%rsi), %rsi
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1571
	movq	16(%rsi), %rsi
.L1571:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler15JSTypedLowering21ReduceJSToStringInputEPNS1_4NodeE
	movq	%rax, %rbx
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.L1573
	movq	8(%r13), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%rbx, %rax
.L1573:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23821:
	.size	_ZN2v88internal8compiler15JSTypedLowering16ReduceJSToStringEPNS1_4NodeE, .-_ZN2v88internal8compiler15JSTypedLowering16ReduceJSToStringEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler15JSTypedLowering21ReduceJSToStringInputEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15JSTypedLowering21ReduceJSToStringInputEPNS1_4NodeE
	.type	_ZN2v88internal8compiler15JSTypedLowering21ReduceJSToStringInputEPNS1_4NodeE, @function
_ZN2v88internal8compiler15JSTypedLowering21ReduceJSToStringInputEPNS1_4NodeE:
.LFB23820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	cmpw	$709, 16(%rax)
	je	.L1602
	movq	8(%rsi), %rax
	movq	%rax, -72(%rbp)
	cmpq	$16417, %rax
	jne	.L1603
.L1581:
	movq	%rbx, %rax
.L1580:
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L1604
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1603:
	.cfi_restore_state
	leaq	-72(%rbp), %r13
	movl	$16417, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1581
	cmpq	$513, -72(%rbp)
	je	.L1582
	movl	$513, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1582
	cmpq	$257, -72(%rbp)
	je	.L1584
	movl	$257, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1584
	cmpq	$129, -72(%rbp)
	je	.L1586
	movl	$129, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1586
	cmpq	$4097, -72(%rbp)
	je	.L1588
	movl	$4097, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1588
	cmpq	$7263, -72(%rbp)
	je	.L1590
	movl	$7263, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1590
	xorl	%eax, %eax
	jmp	.L1580
	.p2align 4,,10
	.p2align 3
.L1602:
	call	_ZN2v88internal8compiler15JSTypedLowering16ReduceJSToStringEPNS1_4NodeE
	testq	%rax, %rax
	cmove	%rbx, %rax
	jmp	.L1580
	.p2align 4,,10
	.p2align 3
.L1582:
	movq	16(%r12), %rdi
	movq	360(%rdi), %rax
	movq	(%rdi), %r14
	leaq	2520(%rax), %rsi
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	16(%r12), %rdi
	movq	%rax, %r13
	movq	360(%rdi), %rax
	leaq	3464(%rax), %rsi
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	xorl	%edx, %edx
	movl	$8, %esi
	movq	%rax, -88(%rbp)
	movq	16(%r12), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6SelectENS0_21MachineRepresentationENS1_10BranchHintE@PLT
	movq	%rbx, %xmm0
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movhps	-88(%rbp), %xmm0
	movq	%rax, %rsi
	leaq	-64(%rbp), %rcx
	movl	$3, %edx
	movq	%r13, -48(%rbp)
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	jmp	.L1580
	.p2align 4,,10
	.p2align 3
.L1584:
	movq	16(%r12), %rdi
	movq	360(%rdi), %rax
	leaq	3512(%rax), %rsi
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	jmp	.L1580
.L1586:
	movq	16(%r12), %rdi
	movq	360(%rdi), %rax
	leaq	2960(%rax), %rsi
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	jmp	.L1580
.L1588:
	movq	16(%r12), %rdi
	movq	360(%rdi), %rax
	leaq	2880(%rax), %rsi
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	jmp	.L1580
.L1590:
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14NumberToStringEv@PLT
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%rbx, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	jmp	.L1580
.L1604:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23820:
	.size	_ZN2v88internal8compiler15JSTypedLowering21ReduceJSToStringInputEPNS1_4NodeE, .-_ZN2v88internal8compiler15JSTypedLowering21ReduceJSToStringInputEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler15JSTypedLowering11ReduceJSAddEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15JSTypedLowering11ReduceJSAddEPNS1_4NodeE
	.type	_ZN2v88internal8compiler15JSTypedLowering11ReduceJSAddEPNS1_4NodeE, @function
_ZN2v88internal8compiler15JSTypedLowering11ReduceJSAddEPNS1_4NodeE:
.LFB23801:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %xmm0
	movq	%rsi, %xmm2
	leaq	32(%rsi), %rdx
	punpcklqdq	%xmm2, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$200, %rsp
	.cfi_offset 3, -56
	movq	32(%rsi), %rcx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	movaps	%xmm0, -160(%rbp)
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1606
	movq	16(%rcx), %rcx
.L1606:
	movq	8(%rcx), %rcx
	movq	%rcx, -128(%rbp)
	cmpq	$7263, %rcx
	jne	.L1771
	cmpl	$15, %eax
	je	.L1611
.L1774:
	addq	$8, %rdx
.L1612:
	movq	(%rdx), %rax
	movq	8(%rax), %rax
	movq	%rax, -128(%rbp)
	cmpq	$7263, %rax
	jne	.L1772
.L1613:
	movq	16(%r13), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9NumberAddEv@PLT
	movq	-152(%rbp), %rdi
	movq	%rax, %r12
	movq	(%rdi), %rax
	movl	24(%rax), %eax
	testl	%eax, %eax
	jle	.L1616
	movq	-160(%rbp), %rax
	movq	%rdi, %rdx
	movq	%rdi, %rsi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	8(%rax), %r9
	movq	(%r9), %rax
	movq	%r9, %rdi
	call	*32(%rax)
	movq	-152(%rbp), %rdi
.L1616:
	call	_ZN2v88internal8compiler14NodeProperties20RemoveNonValueInputsEPNS1_4NodeE@PLT
	movq	-152(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	-152(%rbp), %rax
	movl	$7263, %esi
	movq	8(%rax), %rdi
	movq	-160(%rbp), %rax
	movq	16(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdx
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	-152(%rbp), %rdx
	movq	%rax, 8(%rdx)
	movq	-152(%rbp), %rax
.L1617:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1773
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1771:
	.cfi_restore_state
	leaq	-128(%rbp), %r14
	movl	$7263, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L1610
	movq	-152(%rbp), %rdx
	movzbl	23(%rdx), %eax
	addq	$32, %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1774
.L1611:
	movq	(%rdx), %rdx
	addq	$24, %rdx
	jmp	.L1612
	.p2align 4,,10
	.p2align 3
.L1772:
	leaq	-128(%rbp), %r14
	movl	$7263, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1613
.L1610:
	movq	-152(%rbp), %rdx
	movzbl	23(%rdx), %eax
	leaq	32(%rdx), %rcx
	movq	32(%rdx), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1618
	movq	16(%rdx), %rdx
.L1618:
	movq	8(%rdx), %rdx
	movq	%rdx, -128(%rbp)
	cmpq	$24575, %rdx
	jne	.L1775
.L1621:
	leaq	8(%rcx), %rdx
	cmpl	$15, %eax
	je	.L1776
.L1624:
	movq	(%rdx), %rdx
	movq	8(%rdx), %rdx
	movq	%rdx, -128(%rbp)
	cmpq	$24575, %rdx
	je	.L1627
	movl	$24575, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1777
	.p2align 4,,10
	.p2align 3
.L1622:
	movq	-152(%rbp), %rdi
	movzbl	23(%rdi), %eax
	movq	32(%rdi), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1633
	movq	16(%rdx), %rdx
.L1633:
	movq	8(%rdx), %rax
	movq	%rax, -128(%rbp)
	cmpq	$16417, %rax
	je	.L1636
	movq	%r14, %rdi
	movl	$16417, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	movq	-152(%rbp), %rdi
	testb	%al, %al
	je	.L1635
.L1636:
	movl	$1, %esi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler15JSTypedLowering21ReduceJSToStringInputEPNS1_4NodeE
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1640
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties17ReplaceValueInputEPNS1_4NodeES4_i@PLT
.L1640:
	movq	(%r12), %rdi
	call	_ZN2v88internal8compiler21BinaryOperationHintOfEPKNS1_8OperatorE@PLT
	cmpb	$6, %al
	je	.L1778
.L1646:
	movq	-152(%rbp), %rdi
	movzbl	23(%rdi), %eax
	movq	32(%rdi), %rdx
	leaq	32(%rdi), %rcx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1647
	movq	16(%rdx), %rdx
.L1647:
	movq	8(%rdx), %rdx
	movq	%rdx, -128(%rbp)
	cmpq	$134250495, %rdx
	jne	.L1779
.L1650:
	leaq	8(%rcx), %rdx
	cmpl	$15, %eax
	je	.L1780
.L1653:
	movq	(%rdx), %rdx
	movq	8(%rdx), %rdx
	movq	%rdx, -128(%rbp)
	cmpq	$134250495, %rdx
	je	.L1656
	movl	$134250495, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L1651
	movq	-152(%rbp), %rdi
	movzbl	23(%rdi), %eax
	leaq	32(%rdi), %rcx
	andl	$15, %eax
.L1656:
	movq	32(%r13), %rsi
	movq	(%rcx), %rdx
	cmpl	$15, %eax
	jne	.L1657
	movq	16(%rdx), %rdx
.L1657:
	movq	8(%rdx), %rax
	movq	%rax, -128(%rbp)
	cmpq	%rax, %rsi
	je	.L1660
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L1659
	movq	-152(%rbp), %rdi
.L1660:
	movl	$1, %esi
.L1770:
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties18ReplaceValueInputsEPNS1_4NodeES4_@PLT
	movq	16(%r13), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder8ToStringEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	16(%r13), %rax
	movl	$16417, %esi
	movq	(%rax), %rax
	movq	(%rax), %rdx
	movq	-152(%rbp), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, 8(%r12)
	call	_ZN2v88internal8compiler15JSTypedLowering16ReduceJSToStringEPNS1_4NodeE
	testq	%rax, %rax
	cmove	%r12, %rax
	jmp	.L1617
	.p2align 4,,10
	.p2align 3
.L1775:
	movl	$24575, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L1622
	movq	-152(%rbp), %rcx
	movzbl	23(%rcx), %eax
	addq	$32, %rcx
	andl	$15, %eax
	jmp	.L1621
	.p2align 4,,10
	.p2align 3
.L1635:
	movzbl	23(%rdi), %eax
	leaq	40(%rdi), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1781
.L1641:
	movq	(%rdx), %rax
	movq	8(%rax), %rax
	movq	%rax, -128(%rbp)
	cmpq	$16417, %rax
	jne	.L1782
.L1644:
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler15JSTypedLowering21ReduceJSToStringInputEPNS1_4NodeE
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1640
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties17ReplaceValueInputEPNS1_4NodeES4_i@PLT
	jmp	.L1640
	.p2align 4,,10
	.p2align 3
.L1779:
	movl	$134250495, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L1651
	movq	-152(%rbp), %rdi
	movzbl	23(%rdi), %eax
	leaq	32(%rdi), %rcx
	andl	$15, %eax
	jmp	.L1650
	.p2align 4,,10
	.p2align 3
.L1659:
	movq	-152(%rbp), %rdi
	movq	32(%r13), %rsi
	movzbl	23(%rdi), %eax
	leaq	40(%rdi), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1665
	movq	32(%rdi), %rax
	leaq	24(%rax), %rdx
.L1665:
	movq	(%rdx), %rax
	movq	8(%rax), %rax
	movq	%rax, -128(%rbp)
	cmpq	%rsi, %rax
	je	.L1668
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1783
	.p2align 4,,10
	.p2align 3
.L1651:
	leaq	-160(%rbp), %rbx
	movl	$16417, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler16JSBinopReduction13BothInputsAreENS1_4TypeE
	testb	%al, %al
	je	.L1671
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties15GetContextInputEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties18GetFrameStateInputEPNS1_4NodeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -208(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -192(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	-152(%rbp), %rdi
	xorl	%esi, %esi
	movq	%rax, -168(%rbp)
	movq	16(%r13), %rax
	movq	(%rax), %r9
	movq	%r9, -176(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rax, %r15
	movq	16(%r13), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder12StringLengthEv@PLT
	movq	-176(%rbp), %r9
	movq	%r15, -96(%rbp)
	leaq	-96(%rbp), %r15
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-152(%rbp), %rdi
	movl	$1, %esi
	movq	%rax, -176(%rbp)
	movq	16(%r13), %rax
	movq	(%rax), %r9
	movq	%r9, -224(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rax, -200(%rbp)
	movq	16(%r13), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder12StringLengthEv@PLT
	movq	-200(%rbp), %rdx
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	-224(%rbp), %r9
	movq	%rax, %rsi
	movq	%rdx, -96(%rbp)
	movl	$1, %edx
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -200(%rbp)
	movq	16(%r13), %rax
	movq	(%rax), %r9
	movq	376(%rax), %rdi
	movq	%r9, -224(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9NumberAddEv@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$2, %edx
	movq	-224(%rbp), %r9
	movq	%rax, %rsi
	movq	-176(%rbp), %xmm0
	movq	%r9, %rdi
	movhps	-200(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	24(%r13), %rsi
	movl	$1, %ecx
	leaq	-144(%rbp), %rdi
	movq	%rax, -176(%rbp)
	movq	16(%r13), %rax
	movq	%rdi, -200(%rbp)
	movq	360(%rax), %rdx
	addq	$4544, %rdx
	call	_ZN2v88internal8compiler7CellRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	-200(%rbp), %rdi
	call	_ZNK2v88internal8compiler7CellRef5valueEv@PLT
	movq	%r14, %rdi
	movq	%rax, -128(%rbp)
	movq	%rdx, -120(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef5AsSmiEv@PLT
	cmpl	$1, %eax
	je	.L1784
	movq	16(%r13), %rdi
	movsd	.LC12(%rip), %xmm0
	movq	-240(%rbp), %xmm1
	movq	(%rdi), %r9
	movhps	-208(%rbp), %xmm1
	movaps	%xmm1, -240(%rbp)
	movq	%r9, -224(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%rax, -200(%rbp)
	movq	16(%r13), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder21NumberLessThanOrEqualEv@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$2, %edx
	movq	-224(%rbp), %r9
	movq	%rax, %rsi
	movq	-176(%rbp), %xmm0
	movq	%r9, %rdi
	movhps	-200(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$1, %edx
	movl	$1, %esi
	movq	%rax, -200(%rbp)
	movq	16(%r13), %rax
	movq	(%rax), %r9
	movq	8(%rax), %rdi
	movq	%r9, -224(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$2, %edx
	movq	-224(%rbp), %r9
	movq	%rax, %rsi
	movq	-200(%rbp), %xmm0
	movq	%r9, %rdi
	movhps	-168(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -200(%rbp)
	movq	16(%r13), %rax
	movq	(%rax), %r9
	movq	8(%rax), %rdi
	movq	%r9, -168(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$1, %edx
	movq	-168(%rbp), %r9
	movq	%rax, %rsi
	movq	-200(%rbp), %rax
	movq	%r9, %rdi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$165, %esi
	movq	%rax, -168(%rbp)
	movq	16(%r13), %rax
	movq	(%rax), %r9
	movq	368(%rax), %rdi
	movq	%r9, -224(%rbp)
	call	_ZN2v88internal8compiler17JSOperatorBuilder11CallRuntimeENS0_7Runtime10FunctionIdE@PLT
	movq	%r15, %rcx
	xorl	%r8d, %r8d
	movl	$4, %edx
	movq	-224(%rbp), %r9
	movdqa	-240(%rbp), %xmm1
	movq	%rax, %rsi
	movq	-192(%rbp), %xmm0
	movq	%r9, %rdi
	movaps	%xmm1, -96(%rbp)
	movhps	-168(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	$0, -128(%rbp)
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties17IsExceptionalCallEPNS1_4NodeEPS4_@PLT
	movq	-168(%rbp), %rcx
	movq	%rcx, -224(%rbp)
	testb	%al, %al
	jne	.L1785
.L1674:
	movq	-176(%rbp), %xmm0
	movq	16(%r13), %rax
	movq	8(%rax), %rdi
	movhps	-192(%rbp), %xmm0
	movq	(%rax), %r14
	movaps	%xmm0, -192(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5ThrowEv@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%r14, %rdi
	movq	-168(%rbp), %xmm1
	movq	%rax, %rsi
	movl	$2, %edx
	movhps	-224(%rbp), %xmm1
	movaps	%xmm1, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %rdx
	movq	16(%r13), %rax
	movq	8(%rax), %rsi
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler14NodeProperties17MergeControlToEndEPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS1_4NodeE@PLT
	movq	16(%r13), %rax
	movq	8(%r13), %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rsi
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	16(%r13), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%r14, %rdi
	movq	%rax, %rsi
	movq	-200(%rbp), %rax
	movl	$1, %edx
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	48(%r13), %rdx
	movq	%rax, -168(%rbp)
	movq	16(%r13), %rax
	movq	472(%rdx), %rsi
	movq	8(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9TypeGuardENS1_4TypeE@PLT
	movq	%r14, %rdi
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movdqa	-192(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$3, %edx
	movq	-168(%rbp), %rax
	movaps	%xmm0, -96(%rbp)
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r14
.L1673:
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler16JSBinopReduction22ShouldCreateConsStringEv
	testb	%al, %al
	movq	16(%r13), %rax
	movq	376(%rax), %rdi
	je	.L1675
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder13NewConsStringEv@PLT
	movq	%rax, %r9
.L1676:
	movq	16(%r13), %rax
	movq	-152(%rbp), %rdi
	movl	$1, %esi
	movq	%r9, -176(%rbp)
	movq	(%rax), %r10
	movq	%r10, -192(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	-152(%rbp), %rdi
	xorl	%esi, %esi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%r14, %xmm0
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	-176(%rbp), %r9
	movq	-192(%rbp), %r10
	movq	%rax, %xmm3
	movl	$3, %edx
	punpcklqdq	%xmm3, %xmm0
	movq	%rbx, -80(%rbp)
	movq	%r9, %rsi
	movq	%r10, %rdi
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%r13), %rdi
	movq	%r14, %rcx
	movq	%r12, %rsi
	movq	%rax, %rbx
	movq	-168(%rbp), %r8
	movq	(%rdi), %rax
	movq	%rbx, %rdx
	call	*32(%rax)
	movq	%rbx, %rax
	jmp	.L1617
	.p2align 4,,10
	.p2align 3
.L1671:
	movq	-152(%rbp), %rdx
	movzbl	23(%rdx), %eax
	leaq	32(%rdx), %rcx
	movq	32(%rdx), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1677
	movq	16(%rdx), %rdx
.L1677:
	movq	8(%rdx), %rdx
	movq	%rdx, -128(%rbp)
	cmpq	$16417, %rdx
	jne	.L1786
.L1687:
	movq	(%rcx), %rdx
	cmpl	$15, %eax
	je	.L1787
.L1688:
	movq	8(%rdx), %rdx
	movq	%rdx, -128(%rbp)
	cmpq	$16417, %rdx
	jne	.L1788
.L1691:
	leaq	8(%rcx), %rdx
	cmpl	$15, %eax
	jne	.L1694
	movq	(%rcx), %rbx
	leaq	24(%rbx), %rdx
.L1694:
	movl	$0, -168(%rbp)
	movq	(%rdx), %rdx
	movq	8(%rdx), %rdx
	movq	%rdx, -128(%rbp)
	cmpq	$16417, %rdx
	je	.L1692
	movl	$16417, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	movq	-152(%rbp), %rcx
	testb	%al, %al
	movzbl	23(%rcx), %eax
	je	.L1696
	andl	$15, %eax
	addq	$32, %rcx
	.p2align 4,,10
	.p2align 3
.L1692:
	movq	(%r12), %rdx
	movzbl	18(%rdx), %ebx
	movq	(%rcx), %rdx
	cmpl	$15, %eax
	jne	.L1697
	movq	16(%rdx), %rdx
.L1697:
	movq	8(%rdx), %rax
	movl	$75431937, %esi
	movq	%r14, %rdi
	leaq	-144(%rbp), %r15
	movq	%rax, -128(%rbp)
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1702
	movq	-152(%rbp), %rax
	movzbl	23(%rax), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L1699
	addq	$40, %rax
.L1700:
	movq	(%rax), %rax
	leaq	-144(%rbp), %r15
	movl	$75431937, %esi
	movq	%r15, %rdi
	movq	8(%rax), %rax
	movq	%rax, -144(%rbp)
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	movl	$80, %eax
	cmove	%eax, %ebx
.L1702:
	movq	16(%r13), %rax
	movl	-168(%rbp), %edx
	movq	%r14, %rdi
	movq	360(%rax), %rsi
	call	_ZN2v88internal11CodeFactory9StringAddEPNS0_7IsolateENS0_14StringAddFlagsE@PLT
	movq	-112(%rbp), %rax
	xorl	%r9d, %r9d
	movl	%ebx, %r8d
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	movq	%r15, %rsi
	movl	8(%rax), %edx
	subl	(%rax), %edx
	movq	%rax, -136(%rbp)
	movq	16(%r13), %rax
	movq	%rcx, -144(%rbp)
	movl	$1, %ecx
	movq	(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler7Linkage21GetStubCallDescriptorEPNS0_4ZoneERKNS0_23CallInterfaceDescriptorEiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEENS9_INS1_8Operator8PropertyEhEENS0_12StubCallModeE@PLT
	movq	16(%r13), %rdi
	movq	-128(%rbp), %rsi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	16(%r13), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%r13), %rax
	movq	%r14, %rsi
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	%r12, %rax
	jmp	.L1617
	.p2align 4,,10
	.p2align 3
.L1782:
	movl	$16417, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L1640
	movq	-152(%rbp), %rdi
	jmp	.L1644
	.p2align 4,,10
	.p2align 3
.L1781:
	movq	32(%rdi), %rax
	leaq	24(%rax), %rdx
	jmp	.L1641
	.p2align 4,,10
	.p2align 3
.L1776:
	movq	(%rcx), %rbx
	leaq	24(%rbx), %rdx
	jmp	.L1624
	.p2align 4,,10
	.p2align 3
.L1777:
	movq	-152(%rbp), %rcx
	movzbl	23(%rcx), %eax
	addq	$32, %rcx
	andl	$15, %eax
.L1627:
	movq	(%rcx), %rdx
	cmpl	$15, %eax
	jne	.L1628
	movq	16(%rdx), %rdx
.L1628:
	movq	8(%rdx), %rax
	movl	$75448353, %esi
	movq	%r14, %rdi
	movq	%rax, -128(%rbp)
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1622
	movq	-152(%rbp), %rax
	movzbl	23(%rax), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L1630
	addq	$40, %rax
.L1631:
	movq	(%rax), %rax
	leaq	-144(%rbp), %r15
	movl	$75448353, %esi
	movq	%r15, %rdi
	movq	8(%rax), %rax
	movq	%rax, -144(%rbp)
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1622
	leaq	-160(%rbp), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler16JSBinopReduction21ConvertInputsToNumberEv
	movq	16(%r13), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9NumberAddEv@PLT
	movl	$7263, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler16JSBinopReduction20ChangeToPureOperatorEPKNS1_8OperatorENS1_4TypeE
	jmp	.L1617
	.p2align 4,,10
	.p2align 3
.L1778:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal8compiler16JSBinopReduction19CheckInputsToStringEv
	jmp	.L1646
	.p2align 4,,10
	.p2align 3
.L1675:
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder12StringConcatEv@PLT
	movq	%rax, %r9
	jmp	.L1676
	.p2align 4,,10
	.p2align 3
.L1786:
	movl	$16417, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1769
	movq	-152(%rbp), %rdx
	movzbl	23(%rdx), %eax
	leaq	32(%rdx), %rcx
	leaq	40(%rdx), %rsi
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1683
	movq	32(%rdx), %rsi
	addq	$24, %rsi
.L1683:
	movq	(%rsi), %rdx
	movq	8(%rdx), %rdx
	movq	%rdx, -128(%rbp)
	cmpq	$16417, %rdx
	je	.L1687
	movl	$16417, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	testb	%r8b, %r8b
	je	.L1617
.L1769:
	movq	-152(%rbp), %rcx
	movzbl	23(%rcx), %eax
	addq	$32, %rcx
	andl	$15, %eax
	jmp	.L1687
	.p2align 4,,10
	.p2align 3
.L1783:
	movq	-152(%rbp), %rdi
.L1668:
	xorl	%esi, %esi
	jmp	.L1770
	.p2align 4,,10
	.p2align 3
.L1780:
	movq	(%rcx), %rbx
	leaq	24(%rbx), %rdx
	jmp	.L1653
	.p2align 4,,10
	.p2align 3
.L1787:
	movq	16(%rdx), %rdx
	jmp	.L1688
	.p2align 4,,10
	.p2align 3
.L1784:
	movq	16(%r13), %rdi
	movsd	.LC11(%rip), %xmm0
	movq	-192(%rbp), %xmm1
	movq	(%rdi), %r9
	movhps	-168(%rbp), %xmm1
	movq	%r9, -200(%rbp)
	movaps	%xmm1, -224(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%r14, %rsi
	movq	%rax, -192(%rbp)
	movq	16(%r13), %rax
	movq	376(%rax), %rdi
	movq	$0, -128(%rbp)
	movl	$-1, -120(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder11CheckBoundsERKNS1_14FeedbackSourceE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$4, %edx
	movq	-176(%rbp), %xmm0
	movq	-200(%rbp), %r9
	movq	%rax, %rsi
	movdqa	-224(%rbp), %xmm1
	movhps	-192(%rbp), %xmm0
	movq	%r9, %rdi
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm1, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r14
	jmp	.L1673
	.p2align 4,,10
	.p2align 3
.L1630:
	movq	32(%rax), %rax
	addq	$24, %rax
	jmp	.L1631
	.p2align 4,,10
	.p2align 3
.L1785:
	movq	-128(%rbp), %rdi
	movq	%rcx, %rsi
	xorl	%edx, %edx
	movq	%rcx, %r14
	call	_ZN2v88internal8compiler14NodeProperties19ReplaceControlInputEPNS1_4NodeES4_i@PLT
	movq	-128(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r14, -168(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties18ReplaceEffectInputEPNS1_4NodeES4_i@PLT
	movq	16(%r13), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9IfSuccessEv@PLT
	movq	%r14, %rdi
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%rax, %rsi
	movq	-168(%rbp), %rax
	movl	$1, %edx
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%r13), %rdi
	movq	-128(%rbp), %rsi
	movq	%rax, -224(%rbp)
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L1674
	.p2align 4,,10
	.p2align 3
.L1788:
	movl	$16417, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	movq	-152(%rbp), %rcx
	testb	%al, %al
	movzbl	23(%rcx), %eax
	je	.L1690
	andl	$15, %eax
	addq	$32, %rcx
	jmp	.L1691
	.p2align 4,,10
	.p2align 3
.L1690:
	movl	$1, -168(%rbp)
	andl	$15, %eax
	addq	$32, %rcx
	jmp	.L1692
.L1699:
	movq	32(%rax), %rax
	addq	$24, %rax
	jmp	.L1700
.L1696:
	movl	$2, -168(%rbp)
	andl	$15, %eax
	addq	$32, %rcx
	jmp	.L1692
.L1773:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23801:
	.size	_ZN2v88internal8compiler15JSTypedLowering11ReduceJSAddEPNS1_4NodeE, .-_ZN2v88internal8compiler15JSTypedLowering11ReduceJSAddEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler15JSTypedLowering16ReduceJSToObjectEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15JSTypedLowering16ReduceJSToObjectEPNS1_4NodeE
	.type	_ZN2v88internal8compiler15JSTypedLowering16ReduceJSToObjectEPNS1_4NodeE, @function
_ZN2v88internal8compiler15JSTypedLowering16ReduceJSToObjectEPNS1_4NodeE:
.LFB23822:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$216, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	movq	8(%rax), %rax
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetContextInputEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler14NodeProperties18GetFrameStateInputEPNS1_4NodeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -184(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	cmpq	$75431937, -168(%rbp)
	movq	%rax, %r9
	jne	.L1829
.L1790:
	movq	8(%rbx), %rdi
	movq	%r9, %r8
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r13, %rax
.L1793:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1830
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1829:
	.cfi_restore_state
	leaq	-168(%rbp), %r10
	movl	$75431937, %esi
	movq	%rax, -192(%rbp)
	movq	%r10, %rdi
	movq	%r10, -208(%rbp)
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	movq	-192(%rbp), %r9
	testb	%al, %al
	jne	.L1790
	movq	16(%rbx), %rax
	movq	%r15, %xmm0
	movq	%r9, -216(%rbp)
	leaq	-112(%rbp), %r15
	movhps	-184(%rbp), %xmm0
	movq	(%rax), %r11
	movq	376(%rax), %rdi
	movaps	%xmm0, -256(%rbp)
	movq	%r11, -184(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder16ObjectIsReceiverEv@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$1, %edx
	movq	-184(%rbp), %r11
	movq	%rax, %rsi
	movq	%r13, -112(%rbp)
	movq	%r11, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$1, %edx
	movl	$1, %esi
	movq	%rax, -184(%rbp)
	movq	16(%rbx), %rax
	movq	(%rax), %r11
	movq	8(%rax), %rdi
	movq	%r11, -192(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$2, %edx
	movq	-216(%rbp), %r9
	movq	-192(%rbp), %r11
	movq	%rax, %rsi
	movq	-184(%rbp), %xmm1
	movq	%r9, %xmm2
	movq	%r11, %rdi
	punpcklqdq	%xmm2, %xmm1
	movaps	%xmm1, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -184(%rbp)
	movq	16(%rbx), %rax
	movq	(%rax), %r9
	movq	8(%rax), %rdi
	movq	%r9, -192(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$1, %edx
	movq	-192(%rbp), %r9
	movq	%rax, %rsi
	movq	-184(%rbp), %rax
	movq	%r9, %rdi
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -216(%rbp)
	movq	16(%rbx), %rax
	movq	(%rax), %r9
	movq	8(%rax), %rdi
	movq	%r9, -192(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$1, %edx
	movq	-192(%rbp), %r9
	movq	%rax, %rsi
	movq	-184(%rbp), %rax
	movq	%r9, %rdi
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	leaq	-144(%rbp), %r11
	movl	$91, %edx
	movq	%rax, -192(%rbp)
	movq	16(%rbx), %rax
	movq	%r11, %rdi
	movq	%r11, -240(%rbp)
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-128(%rbp), %rax
	movq	(%r12), %rcx
	xorl	%r9d, %r9d
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rsi
	movl	8(%rax), %edx
	subl	(%rax), %edx
	movq	%rax, -152(%rbp)
	movq	16(%rbx), %rax
	movzbl	18(%rcx), %r8d
	movq	%rsi, -160(%rbp)
	movl	$1, %ecx
	leaq	-160(%rbp), %rsi
	movq	(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler7Linkage21GetStubCallDescriptorEPNS0_4ZoneERKNS0_23CallInterfaceDescriptorEiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEENS9_INS1_8Operator8PropertyEhEENS0_12StubCallModeE@PLT
	movq	16(%rbx), %rdi
	movq	-144(%rbp), %rsi
	movq	%rax, -232(%rbp)
	movq	(%rdi), %r9
	movq	%r9, -224(%rbp)
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-232(%rbp), %r8
	movq	%rax, -184(%rbp)
	movq	16(%rbx), %rax
	movq	%r8, %rsi
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	movq	%r13, %xmm3
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movdqa	-256(%rbp), %xmm0
	movq	-224(%rbp), %r9
	movq	%rax, %rsi
	movl	$6, %edx
	movq	-184(%rbp), %xmm1
	movaps	%xmm0, -96(%rbp)
	movq	%r14, %xmm0
	movq	%r9, %rdi
	punpcklqdq	%xmm3, %xmm1
	movhps	-192(%rbp), %xmm0
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-208(%rbp), %r10
	movl	$385, %esi
	movq	$0, -144(%rbp)
	movq	%rax, -184(%rbp)
	movq	%r10, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	movq	-240(%rbp), %r11
	testb	%al, %al
	jne	.L1791
	movq	-184(%rbp), %rax
	movq	%rax, -192(%rbp)
.L1792:
	movq	16(%rbx), %rax
	movq	%r14, %xmm0
	movl	$2, %esi
	movhps	-184(%rbp), %xmm0
	movq	8(%rax), %rdi
	movaps	%xmm0, -208(%rbp)
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$2, %edx
	movq	-216(%rbp), %xmm1
	movq	%r14, %rdi
	movq	%rax, %rsi
	movhps	-192(%rbp), %xmm1
	movaps	%xmm1, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$2, %esi
	movq	%rax, %r14
	movq	16(%rbx), %rax
	movq	(%rax), %r9
	movq	8(%rax), %rdi
	movq	%r9, -192(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9EffectPhiEi@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$3, %edx
	movq	-192(%rbp), %r9
	movdqa	-208(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r14, -96(%rbp)
	movq	%r9, %rdi
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%rbx), %rdi
	movq	%r12, %rdx
	movq	%r14, %r8
	movq	%rax, %rcx
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	movzbl	23(%r12), %edx
	leaq	32(%r12), %rcx
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L1794
	movq	32(%r12), %rdi
	movq	%rcx, %rax
	movq	%r12, %rsi
	cmpq	%rdi, %r13
	je	.L1796
.L1795:
	leaq	-24(%rsi), %r15
	testq	%rdi, %rdi
	je	.L1798
	movq	%r15, %rsi
	movq	%rcx, -208(%rbp)
	movq	%rax, -192(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-192(%rbp), %rax
	movq	-208(%rbp), %rcx
.L1798:
	movq	%r13, (%rax)
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rcx, -192(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movzbl	23(%r12), %eax
	movq	-192(%rbp), %rcx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1831
.L1796:
	movq	8(%rcx), %rdi
	cmpq	%rdi, -184(%rbp)
	je	.L1799
	leaq	8(%rcx), %r15
	movq	%r12, %rsi
.L1800:
	leaq	-48(%rsi), %r13
	testq	%rdi, %rdi
	je	.L1802
	movq	%r13, %rsi
	movq	%rcx, -192(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-192(%rbp), %rcx
.L1802:
	movq	-184(%rbp), %rdi
	movq	%rdi, (%r15)
	testq	%rdi, %rdi
	je	.L1803
	movq	%r13, %rsi
	movq	%rcx, -184(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movq	-184(%rbp), %rcx
.L1803:
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1832
.L1799:
	movq	16(%rcx), %rdi
	cmpq	%rdi, %r14
	je	.L1806
	leaq	16(%rcx), %r15
	movq	%r12, %rsi
.L1805:
	leaq	-72(%rsi), %r13
	testq	%rdi, %rdi
	je	.L1807
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L1807:
	movq	%r14, (%r15)
	testq	%r14, %r14
	je	.L1806
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L1806:
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node14TrimInputCountEi@PLT
	movq	16(%rbx), %rax
	movl	$2, %edx
	movl	$8, %esi
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	%r12, %rax
	jmp	.L1793
	.p2align 4,,10
	.p2align 3
.L1791:
	movq	%r11, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties17IsExceptionalCallEPNS1_4NodeEPS4_@PLT
	movq	-184(%rbp), %rcx
	movq	%rcx, -192(%rbp)
	testb	%al, %al
	je	.L1792
	movq	-144(%rbp), %rdi
	movq	%rcx, %rsi
	xorl	%edx, %edx
	call	_ZN2v88internal8compiler14NodeProperties19ReplaceControlInputEPNS1_4NodeES4_i@PLT
	movq	-184(%rbp), %rsi
	movq	-144(%rbp), %rdi
	xorl	%edx, %edx
	call	_ZN2v88internal8compiler14NodeProperties18ReplaceEffectInputEPNS1_4NodeES4_i@PLT
	movq	16(%rbx), %rax
	movq	(%rax), %r9
	movq	8(%rax), %rdi
	movq	%r9, -192(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9IfSuccessEv@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$1, %edx
	movq	-192(%rbp), %r9
	movq	%rax, %rsi
	movq	-184(%rbp), %rax
	movq	%r9, %rdi
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%rbx), %rdi
	movq	-144(%rbp), %rsi
	movq	%rax, -192(%rbp)
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L1792
	.p2align 4,,10
	.p2align 3
.L1794:
	movq	32(%r12), %rsi
	movq	16(%rsi), %rdi
	leaq	16(%rsi), %rax
	cmpq	%rdi, %r13
	jne	.L1795
.L1797:
	movq	8(%rax), %rdi
	cmpq	%rdi, -184(%rbp)
	je	.L1801
	leaq	8(%rax), %r15
	jmp	.L1800
	.p2align 4,,10
	.p2align 3
.L1832:
	movq	32(%r12), %rsi
	leaq	16(%rsi), %rax
.L1801:
	movq	16(%rax), %rdi
	cmpq	%rdi, %r14
	je	.L1806
	leaq	16(%rax), %r15
	jmp	.L1805
	.p2align 4,,10
	.p2align 3
.L1831:
	movq	32(%r12), %rsi
	leaq	16(%rsi), %rax
	jmp	.L1797
.L1830:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23822:
	.size	_ZN2v88internal8compiler15JSTypedLowering16ReduceJSToObjectEPNS1_4NodeE, .-_ZN2v88internal8compiler15JSTypedLowering16ReduceJSToObjectEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler15JSTypedLowering17ReduceJSLoadNamedEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15JSTypedLowering17ReduceJSLoadNamedEPNS1_4NodeE
	.type	_ZN2v88internal8compiler15JSTypedLowering17ReduceJSLoadNamedEPNS1_4NodeE, @function
_ZN2v88internal8compiler15JSTypedLowering17ReduceJSLoadNamedEPNS1_4NodeE:
.LFB23823:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-96(%rbp), %r14
	leaq	-80(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	(%r12), %rdi
	movq	%rax, %r13
	movq	8(%rax), %rax
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal8compiler13NamedAccessOfEPKNS1_8OperatorE@PLT
	movq	24(%rbx), %rsi
	movl	$1, %ecx
	movq	%r14, %rdi
	movq	(%rax), %rdx
	call	_ZN2v88internal8compiler7NameRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	16(%rbx), %rax
	movq	24(%rbx), %rsi
	movq	%r15, %rdi
	movl	$1, %ecx
	movq	360(%rax), %rdx
	addq	$2768, %rdx
	call	_ZN2v88internal8compiler7NameRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef6equalsERKS2_@PLT
	testb	%al, %al
	je	.L1837
	cmpq	$16417, -104(%rbp)
	jne	.L1843
.L1836:
	movq	16(%rbx), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder12StringLengthEv@PLT
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r13, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%rbx), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rax, %r13
	movq	%r12, %rsi
	movq	(%rdi), %rax
	movq	%r13, %rdx
	call	*32(%rax)
	movq	%r13, %rax
	jmp	.L1835
	.p2align 4,,10
	.p2align 3
.L1843:
	leaq	-104(%rbp), %rdi
	movl	$16417, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1836
	.p2align 4,,10
	.p2align 3
.L1837:
	xorl	%eax, %eax
.L1835:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1844
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1844:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23823:
	.size	_ZN2v88internal8compiler15JSTypedLowering17ReduceJSLoadNamedEPNS1_4NodeE, .-_ZN2v88internal8compiler15JSTypedLowering17ReduceJSLoadNamedEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler15JSTypedLowering27ReduceJSHasInPrototypeChainEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15JSTypedLowering27ReduceJSHasInPrototypeChainEPNS1_4NodeE
	.type	_ZN2v88internal8compiler15JSTypedLowering27ReduceJSHasInPrototypeChainEPNS1_4NodeE, @function
_ZN2v88internal8compiler15JSTypedLowering27ReduceJSHasInPrototypeChainEPNS1_4NodeE:
.LFB23824:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$312, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	movq	8(%rax), %rax
	movq	%rax, -184(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%r12, %rdi
	movq	%rax, -208(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetContextInputEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler14NodeProperties18GetFrameStateInputEPNS1_4NodeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -224(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -200(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	cmpq	$134250495, -184(%rbp)
	movq	%rax, %r14
	jne	.L1896
.L1846:
	movq	16(%rbx), %rdi
	call	_ZN2v88internal8compiler7JSGraph13FalseConstantEv@PLT
	movq	8(%rbx), %rdi
	movq	%r14, %r8
	movq	%r12, %rsi
	movq	%rax, %r13
	movq	-200(%rbp), %rcx
	movq	(%rdi), %rax
	movq	%r13, %rdx
	call	*32(%rax)
	movq	%r13, %rax
.L1849:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1897
	addq	$312, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1896:
	.cfi_restore_state
	leaq	-184(%rbp), %rdi
	movl	$134250495, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1846
	movq	16(%rbx), %rax
	movq	%r15, %xmm1
	movq	%r13, %xmm2
	movhps	-224(%rbp), %xmm1
	punpcklqdq	%xmm2, %xmm2
	movq	376(%rax), %rdi
	movq	(%rax), %r15
	movaps	%xmm1, -336(%rbp)
	movaps	%xmm2, -240(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder11ObjectIsSmiEv@PLT
	movq	%r13, -112(%rbp)
	leaq	-112(%rbp), %r13
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	%r15, %rdi
	movq	%rax, %rsi
	movl	$1, %edx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$1, %edx
	movl	$2, %esi
	movq	%rax, -224(%rbp)
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	movq	%r14, %xmm4
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	-224(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movq	%r15, %rdi
	punpcklqdq	%xmm4, %xmm0
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r14
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r15, %rdi
	movq	%r14, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%rbx), %rdi
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal8compiler7JSGraph13FalseConstantEv@PLT
	movq	%rax, -288(%rbp)
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$1, %edx
	movq	%r15, %rdi
	movq	%rax, %rsi
	movq	%r14, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$2, %esi
	movq	%rax, -224(%rbp)
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4LoopEi@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$2, %edx
	movq	-224(%rbp), %xmm0
	movq	%r14, %rdi
	movq	%rax, %rsi
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$2, %esi
	movq	%rax, %r15
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9EffectPhiEi@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$3, %edx
	movq	-200(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r14, %rdi
	movq	%r15, -96(%rbp)
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -264(%rbp)
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9TerminateEv@PLT
	movq	%r15, %xmm6
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	-264(%rbp), %xmm0
	movq	%r14, %rdi
	movq	%rax, %rsi
	movl	$2, %edx
	punpcklqdq	%xmm6, %xmm0
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %rdx
	movq	16(%rbx), %rax
	movq	8(%rax), %rsi
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler14NodeProperties17MergeControlToEndEPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS1_4NodeE@PLT
	movq	16(%rbx), %rax
	movl	$2, %edx
	movl	$8, %esi
	movq	8(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	movq	%r13, %rcx
	movl	$3, %edx
	xorl	%r8d, %r8d
	movdqa	-240(%rbp), %xmm2
	movq	%rax, %rsi
	movq	%r14, %rdi
	movq	%r15, -96(%rbp)
	movq	%r15, -240(%rbp)
	leaq	-176(%rbp), %r15
	movaps	%xmm2, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r15, %rdi
	movq	%rax, -256(%rbp)
	movq	$209682431, 8(%rax)
	movq	16(%rbx), %rax
	movq	376(%rax), %r8
	movq	(%rax), %r14
	movq	%r8, -224(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	-224(%rbp), %r8
	movq	%r15, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9LoadFieldERKNS1_11FieldAccessE@PLT
	movq	%r13, %rcx
	movl	$3, %edx
	xorl	%r8d, %r8d
	movq	-256(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r14, %rdi
	movq	-240(%rbp), %rax
	movhps	-264(%rbp), %xmm0
	movq	%rax, -96(%rbp)
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r15, %rdi
	movq	%rax, -272(%rbp)
	movq	16(%rbx), %rax
	movq	376(%rax), %r8
	movq	(%rax), %r14
	movq	%r8, -224(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder18ForMapInstanceTypeEv@PLT
	movq	-224(%rbp), %r8
	movq	%r15, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9LoadFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$3, %edx
	movq	-272(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r14, %rdi
	movq	-240(%rbp), %rax
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, -96(%rbp)
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%rbx), %rdi
	movsd	.LC13(%rip), %xmm0
	movq	%rax, -224(%rbp)
	movq	(%rdi), %r14
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%rax, -248(%rbp)
	movq	16(%rbx), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder21NumberLessThanOrEqualEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	%r14, %rdi
	movq	-224(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movhps	-248(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$1, %edx
	movl	$2, %esi
	movq	%rax, -248(%rbp)
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$2, %edx
	movq	-248(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r14, %rdi
	movhps	-240(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r14
	movq	16(%rbx), %rax
	movq	(%rax), %r9
	movq	8(%rax), %rdi
	movq	%r9, -248(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$1, %edx
	movq	-248(%rbp), %r9
	movq	%rax, %rsi
	movq	%r14, -112(%rbp)
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -280(%rbp)
	movq	16(%rbx), %rax
	movq	(%rax), %r10
	movq	8(%rax), %rdi
	movq	%r10, -248(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$1, %edx
	movq	-248(%rbp), %r10
	movq	%rax, %rsi
	movq	%r14, -112(%rbp)
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%rbx), %rdi
	movsd	.LC14(%rip), %xmm0
	movq	%rax, -296(%rbp)
	movq	(%rdi), %r14
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%rax, -248(%rbp)
	movq	16(%rbx), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14NumberLessThanEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	%r14, %rdi
	movq	-224(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movhps	-248(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$1, %edx
	movl	$1, %esi
	movq	%rax, -248(%rbp)
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$2, %edx
	movq	-248(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r14, %rdi
	movhps	-296(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r14
	movq	16(%rbx), %rax
	movq	(%rax), %r10
	movq	8(%rax), %rdi
	movq	%r10, -248(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$1, %edx
	movq	-248(%rbp), %r10
	movq	%rax, %rsi
	movq	%r14, -112(%rbp)
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%rbx), %rdi
	movq	%rax, -320(%rbp)
	call	_ZN2v88internal8compiler7JSGraph13FalseConstantEv@PLT
	movq	%rax, -296(%rbp)
	movq	16(%rbx), %rax
	movq	(%rax), %r10
	movq	8(%rax), %rdi
	movq	%r10, -248(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$1, %edx
	movq	-248(%rbp), %r10
	movq	%rax, %rsi
	movq	%r14, -112(%rbp)
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$226, %esi
	movq	%rax, -248(%rbp)
	movq	16(%rbx), %rax
	movq	368(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler17JSOperatorBuilder11CallRuntimeENS0_7Runtime10FunctionIdE@PLT
	movq	%r14, %rdi
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movdqa	-336(%rbp), %xmm1
	movq	%rax, %rsi
	movl	$6, %edx
	movq	-256(%rbp), %xmm0
	movhps	-208(%rbp), %xmm0
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -112(%rbp)
	movq	-224(%rbp), %xmm0
	movhps	-248(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	$0, -176(%rbp)
	movq	%rax, -248(%rbp)
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14NodeProperties17IsExceptionalCallEPNS1_4NodeEPS4_@PLT
	testb	%al, %al
	jne	.L1847
	movq	%r14, -344(%rbp)
.L1848:
	movq	-224(%rbp), %xmm3
	movq	16(%rbx), %rax
	movq	%r15, %rdi
	movq	-200(%rbp), %xmm0
	movq	-304(%rbp), %xmm2
	movq	-272(%rbp), %xmm1
	movq	376(%rax), %r8
	punpcklqdq	%xmm3, %xmm0
	movhps	-320(%rbp), %xmm2
	movq	(%rax), %r14
	punpcklqdq	%xmm3, %xmm1
	movaps	%xmm0, -336(%rbp)
	movaps	%xmm2, -320(%rbp)
	movaps	%xmm1, -224(%rbp)
	movq	%r8, -200(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder15ForMapPrototypeEv@PLT
	movq	-200(%rbp), %r8
	movq	%r15, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9LoadFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$3, %edx
	movdqa	-224(%rbp), %xmm1
	movq	%rax, %rsi
	movq	%r14, %rdi
	movq	-280(%rbp), %rax
	movaps	%xmm1, -112(%rbp)
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%rbx), %rdi
	movq	%rax, -200(%rbp)
	movq	(%rdi), %r14
	call	_ZN2v88internal8compiler7JSGraph12NullConstantEv@PLT
	movq	%rax, -224(%rbp)
	movq	16(%rbx), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14ReferenceEqualEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	%r14, %rdi
	movq	-200(%rbp), %xmm1
	movq	%rax, %rsi
	movl	$2, %edx
	movhps	-224(%rbp), %xmm1
	movaps	%xmm1, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%rax, -224(%rbp)
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$2, %edx
	movq	-224(%rbp), %xmm1
	movq	%rax, %rsi
	movq	%r14, %rdi
	movhps	-280(%rbp), %xmm1
	movaps	%xmm1, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r14
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r15, %rdi
	movq	%r14, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%rbx), %rdi
	movq	%rax, -272(%rbp)
	call	_ZN2v88internal8compiler7JSGraph13FalseConstantEv@PLT
	movq	%rax, -280(%rbp)
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r15, %rdi
	movq	%r14, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -224(%rbp)
	movq	16(%rbx), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14ReferenceEqualEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	%r14, %rdi
	movq	-200(%rbp), %xmm1
	movq	%rax, %rsi
	movl	$2, %edx
	movhps	-208(%rbp), %xmm1
	movaps	%xmm1, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%rax, -208(%rbp)
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$2, %edx
	movq	-208(%rbp), %xmm1
	movq	%rax, %rsi
	movq	%r14, %rdi
	movhps	-224(%rbp), %xmm1
	movaps	%xmm1, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r14
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r15, %rdi
	movq	%r14, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%rbx), %rdi
	movq	%rax, -208(%rbp)
	call	_ZN2v88internal8compiler7JSGraph12TrueConstantEv@PLT
	movq	%rax, %r15
	movq	16(%rbx), %rax
	movq	(%rax), %r10
	movq	8(%rax), %rdi
	movq	%r10, -224(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	-224(%rbp), %r10
	movq	%rax, %rsi
	movq	%r14, -112(%rbp)
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-200(%rbp), %rsi
	movq	-256(%rbp), %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.1
	movq	-200(%rbp), %rsi
	movq	-264(%rbp), %rdi
	call	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.1
	movq	-240(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.1
	movq	16(%rbx), %rax
	movl	$5, %esi
	movq	8(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$5, %edx
	movq	-272(%rbp), %xmm1
	movdqa	-320(%rbp), %xmm2
	movq	%rax, %rsi
	movq	%r14, %rdi
	movq	-344(%rbp), %rax
	movhps	-208(%rbp), %xmm1
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm1, -96(%rbp)
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$5, %esi
	movq	%rax, %r14
	movq	16(%rbx), %rax
	movq	(%rax), %r10
	movq	8(%rax), %rdi
	movq	%r10, -208(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9EffectPhiEi@PLT
	movq	%r14, %xmm5
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movdqa	-336(%rbp), %xmm0
	movq	-208(%rbp), %r10
	movq	%rax, %rsi
	movl	$6, %edx
	leaq	32(%r12), %r13
	movaps	%xmm0, -112(%rbp)
	movq	%r10, %rdi
	movq	-200(%rbp), %xmm0
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-248(%rbp), %xmm0
	punpcklqdq	%xmm5, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%rbx), %rdi
	movq	%r14, %r8
	movq	%r12, %rdx
	movq	%rax, %rcx
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	-288(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.0
	movq	-296(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.1
	movzbl	23(%r12), %eax
	movq	-280(%rbp), %r9
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1850
	movq	16(%r13), %rdi
	cmpq	%rdi, %r9
	je	.L1851
	leaq	48(%r12), %rax
	movq	%r12, %rsi
.L1852:
	subq	$72, %rsi
	testq	%rdi, %rdi
	je	.L1854
	movq	%rax, -224(%rbp)
	movq	%r9, -208(%rbp)
	movq	%rsi, -200(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-224(%rbp), %rax
	movq	-208(%rbp), %r9
	movq	-200(%rbp), %rsi
.L1854:
	movq	%r9, (%rax)
	testq	%r9, %r9
	je	.L1855
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L1855:
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1898
.L1851:
	movq	24(%r13), %rdi
	cmpq	%rdi, %r15
	je	.L1856
	leaq	24(%r13), %rax
	movq	%r12, %rsi
.L1857:
	subq	$96, %rsi
	testq	%rdi, %rdi
	je	.L1859
	movq	%rax, -208(%rbp)
	movq	%rsi, -200(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-208(%rbp), %rax
	movq	-200(%rbp), %rsi
.L1859:
	movq	%r15, (%rax)
	testq	%r15, %r15
	je	.L1860
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L1860:
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1899
.L1856:
	movq	32(%r13), %rdi
	cmpq	%rdi, -248(%rbp)
	je	.L1861
	leaq	32(%r13), %r15
	movq	%r12, %rsi
.L1862:
	subq	$120, %rsi
	testq	%rdi, %rdi
	je	.L1864
	movq	%rsi, -200(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-200(%rbp), %rsi
.L1864:
	movq	-248(%rbp), %rax
	movq	%rax, (%r15)
	testq	%rax, %rax
	je	.L1865
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L1865:
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1900
.L1861:
	movq	40(%r13), %rdi
	cmpq	%rdi, %r14
	je	.L1868
	leaq	40(%r13), %r15
	movq	%r12, %rsi
.L1867:
	leaq	-144(%rsi), %r13
	testq	%rdi, %rdi
	je	.L1869
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L1869:
	movq	%r14, (%r15)
	testq	%r14, %r14
	je	.L1868
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L1868:
	movl	$6, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node14TrimInputCountEi@PLT
	movq	16(%rbx), %rax
	movl	$5, %edx
	movl	$8, %esi
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	%r12, %rax
	jmp	.L1849
	.p2align 4,,10
	.p2align 3
.L1847:
	movq	-248(%rbp), %r14
	movq	-176(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler14NodeProperties19ReplaceControlInputEPNS1_4NodeES4_i@PLT
	movq	-176(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r14, -248(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties18ReplaceEffectInputEPNS1_4NodeES4_i@PLT
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9IfSuccessEv@PLT
	movq	%r14, %rdi
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	%rax, %rsi
	movq	-248(%rbp), %rax
	movl	$1, %edx
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%rbx), %rdi
	movq	-176(%rbp), %rsi
	movq	%rax, -344(%rbp)
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L1848
	.p2align 4,,10
	.p2align 3
.L1850:
	movq	32(%r12), %rsi
	movq	32(%rsi), %rdi
	leaq	16(%rsi), %rax
	cmpq	%rdi, %r9
	je	.L1853
	leaq	32(%rsi), %rax
	jmp	.L1852
	.p2align 4,,10
	.p2align 3
.L1898:
	movq	32(%r12), %rsi
	leaq	16(%rsi), %rax
.L1853:
	movq	24(%rax), %rdi
	cmpq	%rdi, %r15
	je	.L1858
	addq	$24, %rax
	jmp	.L1857
	.p2align 4,,10
	.p2align 3
.L1899:
	movq	32(%r12), %rsi
	leaq	16(%rsi), %rax
.L1858:
	movq	32(%rax), %rdi
	cmpq	%rdi, -248(%rbp)
	je	.L1863
	leaq	32(%rax), %r15
	jmp	.L1862
	.p2align 4,,10
	.p2align 3
.L1900:
	movq	32(%r12), %rsi
	leaq	16(%rsi), %rax
.L1863:
	movq	40(%rax), %rdi
	cmpq	%rdi, %r14
	je	.L1868
	leaq	40(%rax), %r15
	jmp	.L1867
.L1897:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23824:
	.size	_ZN2v88internal8compiler15JSTypedLowering27ReduceJSHasInPrototypeChainEPNS1_4NodeE, .-_ZN2v88internal8compiler15JSTypedLowering27ReduceJSHasInPrototypeChainEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler15JSTypedLowering27ReduceJSOrdinaryHasInstanceEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15JSTypedLowering27ReduceJSOrdinaryHasInstanceEPNS1_4NodeE
	.type	_ZN2v88internal8compiler15JSTypedLowering27ReduceJSOrdinaryHasInstanceEPNS1_4NodeE, @function
_ZN2v88internal8compiler15JSTypedLowering27ReduceJSOrdinaryHasInstanceEPNS1_4NodeE:
.LFB23825:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	leaq	-56(%rbp), %r14
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	movq	%r12, %rdi
	subq	$32, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	8(%rax), %rax
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movl	$7143425, %esi
	movq	%r14, %rdi
	movq	8(%rax), %rax
	movq	%rax, -48(%rbp)
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	je	.L1907
	leaq	-48(%rbp), %rdi
	movl	$75431937, %esi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	je	.L1908
.L1904:
	xorl	%eax, %eax
	jmp	.L1903
	.p2align 4,,10
	.p2align 3
.L1908:
	movl	$4194305, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1904
	.p2align 4,,10
	.p2align 3
.L1907:
	movq	16(%r13), %rdi
	call	_ZN2v88internal8compiler7JSGraph13FalseConstantEv@PLT
	movq	8(%r13), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rax, %rbx
	movq	%r12, %rsi
	movq	(%rdi), %rax
	movq	%rbx, %rdx
	call	*32(%rax)
	movq	%rbx, %rax
.L1903:
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L1909
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1909:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23825:
	.size	_ZN2v88internal8compiler15JSTypedLowering27ReduceJSOrdinaryHasInstanceEPNS1_4NodeE, .-_ZN2v88internal8compiler15JSTypedLowering27ReduceJSOrdinaryHasInstanceEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler15JSTypedLowering19ReduceJSLoadContextEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15JSTypedLowering19ReduceJSLoadContextEPNS1_4NodeE
	.type	_ZN2v88internal8compiler15JSTypedLowering19ReduceJSLoadContextEPNS1_4NodeE, @function
_ZN2v88internal8compiler15JSTypedLowering19ReduceJSLoadContextEPNS1_4NodeE:
.LFB23826:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$152, %rsp
	movq	%rdi, -176(%rbp)
	movq	(%rsi), %rdi
	movq	%rsi, -192(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler15ContextAccessOfEPKNS1_8OperatorE@PLT
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	%rax, %r12
	movq	%rax, -160(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	%rbx, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14NodeProperties15GetContextInputEPNS1_4NodeE@PLT
	cmpw	$0, 2(%r12)
	movq	%rax, %rbx
	movq	16(%r15), %rax
	movq	(%rax), %r13
	movq	8(%r13), %rcx
	movq	%rcx, -168(%rbp)
	je	.L1941
	leaq	-80(%rbp), %rdx
	xorl	%r15d, %r15d
	leaq	-144(%rbp), %r12
	movq	%rdx, -184(%rbp)
	jmp	.L1912
	.p2align 4,,10
	.p2align 3
.L1942:
	movq	-176(%rbp), %rax
	movq	%r14, %rbx
	movq	16(%rax), %rax
	movq	(%rax), %r13
.L1912:
	movq	376(%rax), %r8
	movl	$1, %esi
	movq	%r12, %rdi
	addq	$1, %r15
	movq	%r8, -152(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder26ForContextSlotKnownPointerEm@PLT
	movq	-152(%rbp), %r8
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9LoadFieldERKNS1_11FieldAccessE@PLT
	movq	%r14, %xmm1
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	movq	%rax, %rsi
	movq	%rbx, %xmm0
	movl	$3, %edx
	movq	-168(%rbp), %rax
	movq	-184(%rbp), %rcx
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r14
	movq	-160(%rbp), %rax
	movzwl	2(%rax), %eax
	cmpq	%r15, %rax
	ja	.L1942
	movq	%r14, %rbx
.L1911:
	movq	-192(%rbp), %rsi
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %rdi
	leaq	32(%rsi), %r15
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1913
	movq	%r15, %rax
	cmpq	%rdi, %rbx
	je	.L1915
	subq	$24, %rsi
	testq	%rdi, %rdi
	je	.L1917
.L1945:
	movq	%rax, -184(%rbp)
	movq	%rsi, -152(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-184(%rbp), %rax
	movq	-152(%rbp), %rsi
.L1917:
	movq	%rbx, (%rax)
	testq	%rbx, %rbx
	je	.L1918
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L1918:
	movq	-192(%rbp), %rax
	movzbl	23(%rax), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1943
.L1915:
	movq	8(%r15), %r9
	cmpq	%r9, %r14
	je	.L1921
	movq	-192(%rbp), %rdi
	leaq	8(%r15), %rax
.L1920:
	leaq	-48(%rdi), %r15
	testq	%r9, %r9
	je	.L1922
	movq	%r15, %rsi
	movq	%r9, %rdi
	movq	%rax, -152(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-152(%rbp), %rax
.L1922:
	movq	%r14, (%rax)
	testq	%r14, %r14
	je	.L1921
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L1921:
	movq	-176(%rbp), %rbx
	movq	-192(%rbp), %r14
	movq	-168(%rbp), %rdx
	movq	16(%rbx), %rax
	movq	%r14, %rdi
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11AppendInputEPNS0_4ZoneEPS2_@PLT
	movq	16(%rbx), %rax
	movq	%r12, %rdi
	movq	376(%rax), %r15
	movq	-160(%rbp), %rax
	movl	4(%rax), %esi
	call	_ZN2v88internal8compiler13AccessBuilder14ForContextSlotEm@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9LoadFieldERKNS1_11FieldAccessE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1944
	movq	-192(%rbp), %rax
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1943:
	.cfi_restore_state
	movq	-192(%rbp), %rax
	movq	32(%rax), %rdi
	leaq	16(%rdi), %rax
.L1916:
	movq	8(%rax), %r9
	cmpq	%r9, %r14
	je	.L1921
	addq	$8, %rax
	jmp	.L1920
	.p2align 4,,10
	.p2align 3
.L1913:
	movq	16(%rdi), %rdx
	leaq	16(%rdi), %rax
	cmpq	%rdx, %rbx
	je	.L1916
	movq	%rdi, %rsi
	movq	%rdx, %rdi
	subq	$24, %rsi
	testq	%rdi, %rdi
	jne	.L1945
	jmp	.L1917
	.p2align 4,,10
	.p2align 3
.L1941:
	leaq	-144(%rbp), %r12
	jmp	.L1911
.L1944:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23826:
	.size	_ZN2v88internal8compiler15JSTypedLowering19ReduceJSLoadContextEPNS1_4NodeE, .-_ZN2v88internal8compiler15JSTypedLowering19ReduceJSLoadContextEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler15JSTypedLowering20ReduceJSStoreContextEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15JSTypedLowering20ReduceJSStoreContextEPNS1_4NodeE
	.type	_ZN2v88internal8compiler15JSTypedLowering20ReduceJSStoreContextEPNS1_4NodeE, @function
_ZN2v88internal8compiler15JSTypedLowering20ReduceJSStoreContextEPNS1_4NodeE:
.LFB23827:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -192(%rbp)
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler15ContextAccessOfEPKNS1_8OperatorE@PLT
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rax, %r12
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler14NodeProperties15GetContextInputEPNS1_4NodeE@PLT
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rax, %r13
	movq	16(%r14), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rax
	movq	%rax, -176(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	cmpw	$0, 2(%r12)
	movq	%rax, -200(%rbp)
	je	.L1988
	leaq	-80(%rbp), %rax
	xorl	%r15d, %r15d
	leaq	-144(%rbp), %r12
	movq	%rax, -184(%rbp)
	.p2align 4,,10
	.p2align 3
.L1948:
	movq	16(%r14), %rax
	movl	$1, %esi
	movq	%r12, %rdi
	addq	$1, %r15
	movq	(%rax), %r10
	movq	376(%rax), %r8
	movq	%r10, -160(%rbp)
	movq	%r8, -152(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder26ForContextSlotKnownPointerEm@PLT
	movq	-152(%rbp), %r8
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9LoadFieldERKNS1_11FieldAccessE@PLT
	movq	-160(%rbp), %r10
	xorl	%r8d, %r8d
	movq	%rbx, %xmm1
	movq	%rax, %rsi
	movq	%r13, %xmm0
	movl	$3, %edx
	movq	-176(%rbp), %rax
	movq	-184(%rbp), %rcx
	punpcklqdq	%xmm1, %xmm0
	movq	%r10, %rdi
	movq	%rax, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %rbx
	movq	-168(%rbp), %rax
	movq	%rbx, %r13
	movzwl	2(%rax), %eax
	cmpq	%r15, %rax
	ja	.L1948
.L1947:
	movq	-192(%rbp), %rsi
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %rdi
	leaq	32(%rsi), %r15
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1949
	movq	%r15, %rax
	cmpq	%r13, %rdi
	je	.L1951
	subq	$24, %rsi
	testq	%rdi, %rdi
	je	.L1953
.L1991:
	movq	%rax, -160(%rbp)
	movq	%rsi, -152(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-160(%rbp), %rax
	movq	-152(%rbp), %rsi
.L1953:
	movq	%r13, (%rax)
	testq	%r13, %r13
	je	.L1954
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L1954:
	movq	-192(%rbp), %rax
	movzbl	23(%rax), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1989
.L1951:
	movq	8(%r15), %r9
	cmpq	%r9, -200(%rbp)
	je	.L1955
	movq	-192(%rbp), %rdi
	leaq	8(%r15), %rax
.L1956:
	leaq	-48(%rdi), %rsi
	testq	%r9, %r9
	je	.L1958
	movq	%r9, %rdi
	movq	%rax, -160(%rbp)
	movq	%rsi, -152(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-160(%rbp), %rax
	movq	-152(%rbp), %rsi
.L1958:
	movq	-200(%rbp), %rcx
	movq	%rcx, (%rax)
	testq	%rcx, %rcx
	je	.L1959
	movq	%rcx, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L1959:
	movq	-192(%rbp), %rax
	movzbl	23(%rax), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1955
	movq	-192(%rbp), %rax
	movq	32(%rax), %rdi
	leaq	16(%rdi), %rax
.L1957:
	movq	16(%rax), %r9
	cmpq	%rbx, %r9
	je	.L1962
	addq	$16, %rax
	jmp	.L1961
	.p2align 4,,10
	.p2align 3
.L1955:
	movq	16(%r15), %r9
	cmpq	%rbx, %r9
	je	.L1962
	movq	-192(%rbp), %rdi
	leaq	16(%r15), %rax
.L1961:
	leaq	-72(%rdi), %r15
	testq	%r9, %r9
	je	.L1963
	movq	%r15, %rsi
	movq	%r9, %rdi
	movq	%rax, -152(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-152(%rbp), %rax
.L1963:
	movq	%rbx, (%rax)
	testq	%rbx, %rbx
	je	.L1962
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L1962:
	movq	16(%r14), %rax
	movq	%r12, %rdi
	movq	376(%rax), %r14
	movq	-168(%rbp), %rax
	movl	4(%rax), %esi
	call	_ZN2v88internal8compiler13AccessBuilder14ForContextSlotEm@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	movq	-192(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1990
	movq	-192(%rbp), %rax
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1989:
	.cfi_restore_state
	movq	-192(%rbp), %rax
	movq	32(%rax), %rdi
	leaq	16(%rdi), %rax
.L1952:
	movq	8(%rax), %r9
	cmpq	%r9, -200(%rbp)
	je	.L1957
	addq	$8, %rax
	jmp	.L1956
	.p2align 4,,10
	.p2align 3
.L1949:
	movq	16(%rdi), %rdx
	leaq	16(%rdi), %rax
	cmpq	%rdx, %r13
	je	.L1952
	movq	%rdi, %rsi
	movq	%rdx, %rdi
	subq	$24, %rsi
	testq	%rdi, %rdi
	jne	.L1991
	jmp	.L1953
	.p2align 4,,10
	.p2align 3
.L1988:
	leaq	-144(%rbp), %r12
	jmp	.L1947
.L1990:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23827:
	.size	_ZN2v88internal8compiler15JSTypedLowering20ReduceJSStoreContextEPNS1_4NodeE, .-_ZN2v88internal8compiler15JSTypedLowering20ReduceJSStoreContextEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler15JSTypedLowering18BuildGetModuleCellEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15JSTypedLowering18BuildGetModuleCellEPNS1_4NodeE
	.type	_ZN2v88internal8compiler15JSTypedLowering18BuildGetModuleCellEPNS1_4NodeE, @function
_ZN2v88internal8compiler15JSTypedLowering18BuildGetModuleCellEPNS1_4NodeE:
.LFB23828:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-208(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	xorl	%esi, %esi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$216, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	%r13, %rdi
	xorl	%esi, %esi
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%r13, %rdi
	xorl	%esi, %esi
	movq	%rax, %r12
	movq	0(%r13), %rax
	leaq	-144(%rbp), %r13
	movl	44(%rax), %r14d
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rax, -248(%rbp)
	movq	8(%rax), %rax
	movq	%rax, -216(%rbp)
	testb	$1, %al
	jne	.L1993
	movl	(%rax), %edx
	testl	%edx, %edx
	je	.L2003
.L1993:
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	movl	%r14d, %edi
	movb	$1, -208(%rbp)
	movl	$0, -204(%rbp)
	movq	$1, -184(%rbp)
	movw	%ax, -176(%rbp)
	movb	$5, -174(%rbp)
	movl	$1, -172(%rbp)
	movq	$0, -168(%rbp)
	movb	$0, -160(%rbp)
	movups	%xmm0, -200(%rbp)
	call	_ZN2v88internal26SourceTextModuleDescriptor16GetCellIndexKindEi@PLT
	movq	%r13, %rdi
	cmpl	$1, %eax
	je	.L2004
	call	_ZN2v88internal8compiler13AccessBuilder23ForModuleRegularImportsEv@PLT
	movzbl	-96(%rbp), %eax
	notl	%r14d
	movdqa	-144(%rbp), %xmm1
	movdqa	-128(%rbp), %xmm2
	movdqa	-112(%rbp), %xmm3
	movl	%r14d, -252(%rbp)
	movb	%al, -160(%rbp)
	movaps	%xmm1, -208(%rbp)
	movaps	%xmm2, -192(%rbp)
	movaps	%xmm3, -176(%rbp)
.L1997:
	movq	-248(%rbp), %xmm0
	movq	16(%rbx), %rax
	movq	%r15, %rsi
	leaq	-80(%rbp), %r15
	movq	376(%rax), %rdi
	movq	(%rax), %r14
	movhps	-240(%rbp), %xmm0
	movaps	%xmm0, -240(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9LoadFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%r14, %rdi
	movdqa	-240(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$3, %edx
	movq	%r12, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movslq	-252(%rbp), %rsi
	movl	$5, %edx
	movq	%r13, %rdi
	movq	%rax, -240(%rbp)
	movq	16(%rbx), %rax
	movq	376(%rax), %r14
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler13AccessBuilder17ForFixedArraySlotEmNS1_16WriteBarrierKindE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9LoadFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$3, %edx
	movq	-240(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%rbx, %rdi
	movq	%r12, -64(%rbp)
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
.L1992:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L2005
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2003:
	.cfi_restore_state
	leaq	-216(%rbp), %rdi
	call	_ZNK2v88internal8compiler4Type14AsHeapConstantEv@PLT
	leaq	8(%rax), %rdi
	call	_ZNK2v88internal8compiler9ObjectRef18AsSourceTextModuleEv@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rdx, -200(%rbp)
	movl	%r14d, %edx
	movq	%rax, -208(%rbp)
	call	_ZNK2v88internal8compiler19SourceTextModuleRef7GetCellEi@PLT
	cmpb	$0, -144(%rbp)
	je	.L1993
	movq	16(%rbx), %rdi
	leaq	-136(%rbp), %rsi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	jmp	.L1992
	.p2align 4,,10
	.p2align 3
.L2004:
	call	_ZN2v88internal8compiler13AccessBuilder23ForModuleRegularExportsEv@PLT
	movzbl	-96(%rbp), %eax
	movdqa	-128(%rbp), %xmm5
	movdqa	-144(%rbp), %xmm4
	movdqa	-112(%rbp), %xmm6
	movb	%al, -160(%rbp)
	leal	-1(%r14), %eax
	movl	%eax, -252(%rbp)
	movaps	%xmm4, -208(%rbp)
	movaps	%xmm5, -192(%rbp)
	movaps	%xmm6, -176(%rbp)
	jmp	.L1997
.L2005:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23828:
	.size	_ZN2v88internal8compiler15JSTypedLowering18BuildGetModuleCellEPNS1_4NodeE, .-_ZN2v88internal8compiler15JSTypedLowering18BuildGetModuleCellEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler15JSTypedLowering18ReduceJSLoadModuleEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15JSTypedLowering18ReduceJSLoadModuleEPNS1_4NodeE
	.type	_ZN2v88internal8compiler15JSTypedLowering18ReduceJSLoadModuleEPNS1_4NodeE, @function
_ZN2v88internal8compiler15JSTypedLowering18ReduceJSLoadModuleEPNS1_4NodeE:
.LFB23830:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	movq	%r12, %rdi
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler15JSTypedLowering18BuildGetModuleCellEPNS1_4NodeE
	leaq	-144(%rbp), %rsi
	movq	%rax, %rbx
	movq	(%rax), %rax
	movq	%rsi, %rdi
	movq	%rsi, -152(%rbp)
	cmpb	$0, 36(%rax)
	movq	16(%r13), %rax
	cmovne	%rbx, %r14
	movq	(%rax), %r9
	movq	376(%rax), %r8
	movq	%r9, -168(%rbp)
	movq	%r8, -160(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder12ForCellValueEv@PLT
	movq	-160(%rbp), %r8
	movq	-152(%rbp), %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9LoadFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	leaq	-80(%rbp), %rcx
	movq	%r15, -64(%rbp)
	movq	-168(%rbp), %r9
	movq	%r14, %xmm1
	movq	%rbx, %xmm0
	movq	%rax, %rsi
	punpcklqdq	%xmm1, %xmm0
	movl	$3, %edx
	movq	%r9, %rdi
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%r13), %rdi
	movq	%r15, %r8
	movq	%r12, %rsi
	movq	%rax, %r14
	movq	(%rdi), %rax
	movq	%r14, %rcx
	movq	%r14, %rdx
	call	*32(%rax)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2010
	addq	$136, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2010:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23830:
	.size	_ZN2v88internal8compiler15JSTypedLowering18ReduceJSLoadModuleEPNS1_4NodeE, .-_ZN2v88internal8compiler15JSTypedLowering18ReduceJSLoadModuleEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler15JSTypedLowering19ReduceJSStoreModuleEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15JSTypedLowering19ReduceJSStoreModuleEPNS1_4NodeE
	.type	_ZN2v88internal8compiler15JSTypedLowering19ReduceJSStoreModuleEPNS1_4NodeE, @function
_ZN2v88internal8compiler15JSTypedLowering19ReduceJSStoreModuleEPNS1_4NodeE:
.LFB23831:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	movq	%r12, %rdi
	subq	$168, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -176(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal8compiler15JSTypedLowering18BuildGetModuleCellEPNS1_4NodeE
	leaq	-160(%rbp), %rsi
	movq	(%rax), %rdx
	movq	%rax, %xmm0
	movq	%rsi, %rdi
	movq	%rsi, -184(%rbp)
	movhps	-168(%rbp), %xmm0
	cmpb	$0, 36(%rdx)
	movaps	%xmm0, -208(%rbp)
	cmovne	%rax, %rbx
	movq	16(%r13), %rax
	movq	376(%rax), %r14
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler13AccessBuilder12ForCellValueEv@PLT
	movq	-184(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	leaq	-96(%rbp), %rcx
	movq	%r15, %rdi
	movdqa	-208(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$4, %edx
	movaps	%xmm0, -96(%rbp)
	movq	%rbx, %xmm0
	movq	-176(%rbp), %rbx
	movq	%rbx, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%r13), %rdi
	movq	%rbx, %r8
	movq	%r12, %rsi
	movq	%rax, %rdx
	movq	(%rdi), %rax
	movq	%rdx, %rcx
	call	*32(%rax)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2015
	movq	-168(%rbp), %rax
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2015:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23831:
	.size	_ZN2v88internal8compiler15JSTypedLowering19ReduceJSStoreModuleEPNS1_4NodeE, .-_ZN2v88internal8compiler15JSTypedLowering19ReduceJSStoreModuleEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler15JSTypedLowering31ReduceJSConstructForwardVarargsEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15JSTypedLowering31ReduceJSConstructForwardVarargsEPNS1_4NodeE
	.type	_ZN2v88internal8compiler15JSTypedLowering31ReduceJSConstructForwardVarargsEPNS1_4NodeE, @function
_ZN2v88internal8compiler15JSTypedLowering31ReduceJSConstructForwardVarargsEPNS1_4NodeE:
.LFB23834:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$104, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler35ConstructForwardVarargsParametersOfEPKNS1_8OperatorE@PLT
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movl	(%rax), %r13d
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rbx, %rdi
	movzwl	%r13w, %r14d
	movq	8(%rax), %rax
	leal	-1(%r14), %r15d
	movl	%r15d, %esi
	movq	%rax, -120(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rax, %r8
	movq	-120(%rbp), %rax
	testb	$1, %al
	jne	.L2027
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L2028
.L2027:
	xorl	%eax, %eax
.L2018:
	movq	-56(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L2029
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2028:
	.cfi_restore_state
	leaq	-120(%rbp), %r9
	movq	%r8, -144(%rbp)
	movq	%r9, %rdi
	movq	%r9, -136(%rbp)
	call	_ZNK2v88internal8compiler4Type14AsHeapConstantEv@PLT
	leaq	8(%rax), %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsJSFunctionEv@PLT
	movq	-136(%rbp), %r9
	testb	%al, %al
	je	.L2027
	movq	%r9, %rdi
	call	_ZNK2v88internal8compiler4Type14AsHeapConstantEv@PLT
	leaq	8(%rax), %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12AsJSFunctionEv@PLT
	leaq	-112(%rbp), %rdi
	movq	%rax, -112(%rbp)
	movq	%rdx, -104(%rbp)
	call	_ZNK2v88internal8compiler13HeapObjectRef3mapEv@PLT
	leaq	-80(%rbp), %rdi
	movq	%rdi, -136(%rbp)
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZNK2v88internal8compiler6MapRef14is_constructorEv@PLT
	movq	-136(%rbp), %rdi
	movq	-144(%rbp), %r8
	testb	%al, %al
	je	.L2027
	movq	16(%r12), %rax
	movq	%r8, -136(%rbp)
	subl	$2, %r14d
	shrl	$16, %r13d
	movq	360(%rax), %rsi
	call	_ZN2v88internal11CodeFactory31ConstructFunctionForwardVarargsEPNS0_7IsolateE@PLT
	movl	%r15d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler4Node11RemoveInputEi@PLT
	movq	16(%r12), %rdi
	movq	-80(%rbp), %rsi
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%rax, %rcx
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%r12), %rax
	movl	$2, %edx
	movq	%rbx, %rdi
	movq	-136(%rbp), %r8
	movq	(%rax), %rax
	movq	%r8, %rcx
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	pxor	%xmm0, %xmm0
	movq	16(%r12), %rdi
	cvtsi2sdl	%r14d, %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movl	$3, %edx
	movq	%rbx, %rdi
	movq	%rax, %rcx
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	pxor	%xmm0, %xmm0
	movq	16(%r12), %rdi
	cvtsi2sdl	%r13d, %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movl	$4, %edx
	movq	%rbx, %rdi
	movq	%rax, %rcx
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%r12), %rdi
	call	_ZN2v88internal8compiler7JSGraph17UndefinedConstantEv@PLT
	movl	$5, %edx
	movq	%rbx, %rdi
	movq	%rax, %rcx
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%r12), %rax
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	-64(%rbp), %rdx
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	leaq	-96(%rbp), %rsi
	movq	%rcx, -96(%rbp)
	movq	8(%rax), %r12
	movl	$1, %ecx
	movq	%rdx, -88(%rbp)
	movq	(%rax), %rax
	movl	%r15d, %edx
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler7Linkage21GetStubCallDescriptorEPNS0_4ZoneERKNS0_23CallInterfaceDescriptorEiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEENS9_INS1_8Operator8PropertyEhEENS0_12StubCallModeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	%rbx, %rax
	jmp	.L2018
.L2029:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23834:
	.size	_ZN2v88internal8compiler15JSTypedLowering31ReduceJSConstructForwardVarargsEPNS1_4NodeE, .-_ZN2v88internal8compiler15JSTypedLowering31ReduceJSConstructForwardVarargsEPNS1_4NodeE
	.section	.rodata._ZN2v88internal8compiler15JSTypedLowering17ReduceJSConstructEPNS1_4NodeE.str1.1,"aMS",@progbits,1
.LC15:
	.string	"Missing "
.LC16:
	.string	"data for function "
.LC17:
	.string	" ("
	.section	.rodata._ZN2v88internal8compiler15JSTypedLowering17ReduceJSConstructEPNS1_4NodeE.str1.8,"aMS",@progbits,1
	.align 8
.LC18:
	.string	"../deps/v8/src/compiler/js-typed-lowering.cc"
	.section	.rodata._ZN2v88internal8compiler15JSTypedLowering17ReduceJSConstructEPNS1_4NodeE.str1.1
.LC19:
	.string	":"
.LC20:
	.string	")"
	.section	.text._ZN2v88internal8compiler15JSTypedLowering17ReduceJSConstructEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15JSTypedLowering17ReduceJSConstructEPNS1_4NodeE
	.type	_ZN2v88internal8compiler15JSTypedLowering17ReduceJSConstructEPNS1_4NodeE, @function
_ZN2v88internal8compiler15JSTypedLowering17ReduceJSConstructEPNS1_4NodeE:
.LFB23835:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$104, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler21ConstructParametersOfEPKNS1_8OperatorE@PLT
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movl	(%rax), %r13d
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rbx, %rdi
	movq	8(%rax), %rax
	leal	-1(%r13), %r15d
	movl	%r15d, %esi
	movq	%rax, -120(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rax, %r8
	movq	-120(%rbp), %rax
	testb	$1, %al
	jne	.L2037
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L2049
.L2037:
	xorl	%eax, %eax
.L2032:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L2050
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2049:
	.cfi_restore_state
	leaq	-120(%rbp), %r14
	movq	%r8, -136(%rbp)
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type14AsHeapConstantEv@PLT
	leaq	8(%rax), %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsJSFunctionEv@PLT
	movq	-136(%rbp), %r8
	testb	%al, %al
	je	.L2037
	movq	%r14, %rdi
	movq	%r8, -144(%rbp)
	leaq	-80(%rbp), %r14
	call	_ZNK2v88internal8compiler4Type14AsHeapConstantEv@PLT
	leaq	8(%rax), %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12AsJSFunctionEv@PLT
	leaq	-112(%rbp), %r9
	movq	%r9, %rdi
	movq	%r9, -136(%rbp)
	movq	%rax, -112(%rbp)
	movq	%rdx, -104(%rbp)
	call	_ZNK2v88internal8compiler13HeapObjectRef3mapEv@PLT
	movq	%r14, %rdi
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZNK2v88internal8compiler6MapRef14is_constructorEv@PLT
	movq	-136(%rbp), %r9
	movq	-144(%rbp), %r8
	testb	%al, %al
	je	.L2037
	movq	%r9, %rdi
	movq	%r8, -144(%rbp)
	movq	%r9, -136(%rbp)
	call	_ZNK2v88internal8compiler13JSFunctionRef10serializedEv@PLT
	movq	-136(%rbp), %r9
	movq	-144(%rbp), %r8
	testb	%al, %al
	jne	.L2036
	movq	24(%r12), %rdi
	cmpb	$0, 124(%rdi)
	je	.L2037
	call	_ZN2v88internal8compiler12JSHeapBroker5TraceEv@PLT
	movl	$8, %edx
	leaq	.LC15(%rip), %rsi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$18, %edx
	movq	%r12, %rdi
	leaq	.LC16(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-136(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal8compilerlsERSoRKNS1_9ObjectRefE@PLT
	movl	$2, %edx
	leaq	.LC17(%rip), %rsi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rdi
	movl	$44, %edx
	leaq	.LC18(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rdi
	leaq	.LC19(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rdi
	movl	$1562, %esi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC20(%rip), %rsi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r12), %rax
	movq	-24(%rax), %rax
	movq	240(%r12,%rax), %r13
	testq	%r13, %r13
	je	.L2051
	cmpb	$0, 56(%r13)
	je	.L2039
	movsbl	67(%r13), %esi
.L2040:
	movq	%r12, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	jmp	.L2037
.L2036:
	movq	%r9, %rdi
	movq	%r8, -136(%rbp)
	call	_ZNK2v88internal8compiler13JSFunctionRef6sharedEv@PLT
	movq	%r14, %rdi
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZNK2v88internal8compiler21SharedFunctionInfoRef20construct_as_builtinEv@PLT
	movq	-136(%rbp), %r8
	testb	%al, %al
	je	.L2041
	movq	16(%r12), %rax
	movl	$30, %esi
	movq	360(%rax), %rdi
	addq	$41184, %rdi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	movq	-136(%rbp), %r8
	movq	%rax, %rdx
.L2042:
	leaq	-96(%rbp), %r9
	movq	24(%r12), %rsi
	movl	$1, %ecx
	movq	%r8, -144(%rbp)
	movq	%r9, %rdi
	movq	%r9, -136(%rbp)
	subl	$2, %r13d
	call	_ZN2v88internal8compiler7CodeRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movl	%r15d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler4Node11RemoveInputEi@PLT
	movq	-136(%rbp), %r9
	movq	16(%r12), %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%rax, %rcx
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%r12), %rax
	movl	$2, %edx
	movq	%rbx, %rdi
	movq	-144(%rbp), %r8
	movq	(%rax), %rax
	movq	%r8, %rcx
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	pxor	%xmm0, %xmm0
	movq	16(%r12), %rdi
	cvtsi2sdl	%r13d, %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movl	$3, %edx
	movq	%rbx, %rdi
	movq	%rax, %rcx
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%r12), %rdi
	call	_ZN2v88internal8compiler7JSGraph17UndefinedConstantEv@PLT
	movl	$4, %edx
	movq	%rbx, %rdi
	movq	%rax, %rcx
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%r12), %rdi
	call	_ZN2v88internal8compiler7JSGraph17UndefinedConstantEv@PLT
	movl	$5, %edx
	movq	%rbx, %rdi
	movq	%rax, %rcx
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%r12), %rax
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	leaq	1040+_ZN2v88internal15CallDescriptors21call_descriptor_data_E(%rip), %rdx
	leaq	16+_ZTVN2v88internal23ConstructStubDescriptorE(%rip), %rcx
	movq	%r14, %rsi
	movq	%rdx, %xmm1
	movq	%rcx, %xmm0
	movq	8(%rax), %r12
	movl	%r15d, %edx
	punpcklqdq	%xmm1, %xmm0
	movl	$1, %ecx
	movaps	%xmm0, -80(%rbp)
	movq	(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler7Linkage21GetStubCallDescriptorEPNS0_4ZoneERKNS0_23CallInterfaceDescriptorEiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEENS9_INS1_8Operator8PropertyEhEENS0_12StubCallModeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	%rbx, %rax
	jmp	.L2032
.L2041:
	movq	16(%r12), %rax
	movl	$29, %esi
	movq	%r8, -136(%rbp)
	movq	360(%rax), %rdi
	addq	$41184, %rdi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	movq	-136(%rbp), %r8
	movq	%rax, %rdx
	jmp	.L2042
.L2039:
	movq	%r13, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	0(%r13), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L2040
	movq	%r13, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L2040
.L2050:
	call	__stack_chk_fail@PLT
.L2051:
	call	_ZSt16__throw_bad_castv@PLT
	.cfi_endproc
.LFE23835:
	.size	_ZN2v88internal8compiler15JSTypedLowering17ReduceJSConstructEPNS1_4NodeE, .-_ZN2v88internal8compiler15JSTypedLowering17ReduceJSConstructEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler15JSTypedLowering26ReduceJSCallForwardVarargsEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15JSTypedLowering26ReduceJSCallForwardVarargsEPNS1_4NodeE
	.type	_ZN2v88internal8compiler15JSTypedLowering26ReduceJSCallForwardVarargsEPNS1_4NodeE, @function
_ZN2v88internal8compiler15JSTypedLowering26ReduceJSCallForwardVarargsEPNS1_4NodeE:
.LFB23836:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$72, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler30CallForwardVarargsParametersOfEPKNS1_8OperatorE@PLT
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movl	(%rax), %eax
	movl	%eax, %r13d
	shrl	$15, %eax
	andl	$32767, %eax
	andl	$32767, %r13d
	movl	%eax, %r15d
	leal	-2(%r13), %r14d
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	8(%rax), %rax
	movq	%rax, -104(%rbp)
	cmpq	$2097153, %rax
	jne	.L2060
.L2053:
	movq	16(%r12), %rax
	leaq	-80(%rbp), %rdi
	movq	360(%rax), %rsi
	call	_ZN2v88internal11CodeFactory26CallFunctionForwardVarargsEPNS0_7IsolateE@PLT
	movq	16(%r12), %rdi
	movq	-80(%rbp), %rsi
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%rax, %rcx
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	pxor	%xmm0, %xmm0
	movq	16(%r12), %rdi
	cvtsi2sdl	%r14d, %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movl	$2, %edx
	movq	%rbx, %rdi
	movq	%rax, %rcx
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	pxor	%xmm0, %xmm0
	movq	16(%r12), %rdi
	cvtsi2sdl	%r15d, %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movl	$3, %edx
	movq	%rbx, %rdi
	movq	%rax, %rcx
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%r12), %rax
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rdx
	movl	$1, %ecx
	leaq	-96(%rbp), %rsi
	movq	%rdx, -96(%rbp)
	movq	-64(%rbp), %rdx
	movq	8(%rax), %r12
	movq	%rdx, -88(%rbp)
	movq	(%rax), %rax
	leal	-1(%r13), %edx
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler7Linkage21GetStubCallDescriptorEPNS0_4ZoneERKNS0_23CallInterfaceDescriptorEiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEENS9_INS1_8Operator8PropertyEhEENS0_12StubCallModeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	%rbx, %rax
.L2054:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L2061
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2060:
	.cfi_restore_state
	leaq	-104(%rbp), %rdi
	movl	$2097153, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L2053
	xorl	%eax, %eax
	jmp	.L2054
.L2061:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23836:
	.size	_ZN2v88internal8compiler15JSTypedLowering26ReduceJSCallForwardVarargsEPNS1_4NodeE, .-_ZN2v88internal8compiler15JSTypedLowering26ReduceJSCallForwardVarargsEPNS1_4NodeE
	.section	.rodata._ZN2v88internal8compiler15JSTypedLowering12ReduceJSCallEPNS1_4NodeE.str1.1,"aMS",@progbits,1
.LC21:
	.string	"storage_.is_populated_"
	.section	.text._ZN2v88internal8compiler15JSTypedLowering12ReduceJSCallEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15JSTypedLowering12ReduceJSCallEPNS1_4NodeE
	.type	_ZN2v88internal8compiler15JSTypedLowering12ReduceJSCallEPNS1_4NodeE, @function
_ZN2v88internal8compiler15JSTypedLowering12ReduceJSCallEPNS1_4NodeE:
.LFB23837:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$264, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler16CallParametersOfEPKNS1_8OperatorE@PLT
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movl	(%rax), %r13d
	movq	%rax, -248(%rbp)
	movl	%r13d, %eax
	andl	$268435455, %eax
	movl	%eax, -232(%rbp)
	subl	$2, %eax
	movl	%eax, -240(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movl	$1, %esi
	movq	%rbx, %rdi
	movq	%rax, %r14
	movq	8(%rax), %rax
	movq	%rax, -224(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	%rax, %r15
	movq	8(%rax), %rax
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	%rax, -256(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	cmpq	$385, -216(%rbp)
	movq	%rax, -264(%rbp)
	jne	.L2063
.L2065:
	xorl	%r13d, %r13d
.L2064:
	movq	-224(%rbp), %rax
	testb	$1, %al
	jne	.L2066
	movl	(%rax), %edx
	testl	%edx, %edx
	je	.L2144
.L2066:
	cmpq	$2097153, %rax
	jne	.L2145
.L2068:
	movq	16(%r12), %rax
	movl	%r13d, %edx
	leaq	-160(%rbp), %rdi
	movq	360(%rax), %rsi
	call	_ZN2v88internal11CodeFactory12CallFunctionEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	movq	16(%r12), %rdi
	movq	-160(%rbp), %rsi
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%rax, %rcx
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	pxor	%xmm0, %xmm0
	movq	16(%r12), %rdi
	cvtsi2sdl	-240(%rbp), %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movl	$2, %edx
	movq	%rbx, %rdi
	movq	%rax, %rcx
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%r12), %rax
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	-144(%rbp), %rdx
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	leaq	-176(%rbp), %rsi
	movq	8(%rax), %r12
	movq	%rcx, -176(%rbp)
	movl	$1, %ecx
	movq	%rdx, -168(%rbp)
	movq	(%rax), %rax
	movl	-232(%rbp), %edx
	movq	(%rax), %rdi
	subl	$1, %edx
	call	_ZN2v88internal8compiler7Linkage21GetStubCallDescriptorEPNS0_4ZoneERKNS0_23CallInterfaceDescriptorEiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEENS9_INS1_8Operator8PropertyEhEENS0_12StubCallModeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
.L2143:
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
.L2085:
	movq	%rbx, %rax
.L2099:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L2146
	addq	$264, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2145:
	.cfi_restore_state
	leaq	-224(%rbp), %rdi
	movl	$2097153, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L2068
	movq	-248(%rbp), %rax
	movl	(%rax), %esi
	movl	%esi, %eax
	shrl	$29, %eax
	andl	$3, %eax
	cmpl	%eax, %r13d
	jne	.L2147
.L2137:
	xorl	%eax, %eax
	jmp	.L2099
	.p2align 4,,10
	.p2align 3
.L2144:
	leaq	-224(%rbp), %r8
	movq	%r8, %rdi
	movq	%r8, -272(%rbp)
	call	_ZNK2v88internal8compiler4Type14AsHeapConstantEv@PLT
	leaq	8(%rax), %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsJSFunctionEv@PLT
	movq	-272(%rbp), %r8
	testb	%al, %al
	jne	.L2067
	movq	-224(%rbp), %rax
	cmpq	$2097153, %rax
	je	.L2068
	jmp	.L2145
	.p2align 4,,10
	.p2align 3
.L2063:
	leaq	-216(%rbp), %rdi
	movl	$385, %esi
	movq	%rdi, -272(%rbp)
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L2065
	movq	-272(%rbp), %rdi
	movl	$385, %esi
	shrl	$29, %r13d
	andl	$3, %r13d
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	movl	$1, %r8d
	testb	%al, %al
	cmove	%r8d, %r13d
	jmp	.L2064
	.p2align 4,,10
	.p2align 3
.L2147:
	movq	-248(%rbp), %rax
	movl	%esi, %r9d
	movl	%r13d, %r8d
	andl	$268435455, %esi
	shrl	$28, %r9d
	leaq	8(%rax), %rcx
	leaq	4(%rax), %rdx
	movq	16(%r12), %rax
	andl	$1, %r9d
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder4CallEmRKNS1_13CallFrequencyERKNS1_14FeedbackSourceENS0_19ConvertReceiverModeENS0_15SpeculationModeE@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	%rbx, %rax
	jmp	.L2099
	.p2align 4,,10
	.p2align 3
.L2067:
	movq	%r8, %rdi
	call	_ZNK2v88internal8compiler4Type14AsHeapConstantEv@PLT
	leaq	8(%rax), %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12AsJSFunctionEv@PLT
	leaq	-208(%rbp), %r8
	movq	%r8, %rdi
	movq	%r8, -248(%rbp)
	movq	%rax, -208(%rbp)
	movq	%rdx, -200(%rbp)
	call	_ZNK2v88internal8compiler13JSFunctionRef10serializedEv@PLT
	movq	-248(%rbp), %r8
	testb	%al, %al
	jne	.L2070
	movq	24(%r12), %rdi
	cmpb	$0, 124(%rdi)
	je	.L2137
	movq	%r8, -232(%rbp)
	call	_ZN2v88internal8compiler12JSHeapBroker5TraceEv@PLT
	movl	$8, %edx
	leaq	.LC15(%rip), %rsi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$18, %edx
	movq	%r12, %rdi
	leaq	.LC16(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-232(%rbp), %r8
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal8compilerlsERSoRKNS1_9ObjectRefE@PLT
	movl	$2, %edx
	leaq	.LC17(%rip), %rsi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rdi
	movl	$44, %edx
	leaq	.LC18(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rdi
	leaq	.LC19(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rdi
	movl	$1641, %esi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC20(%rip), %rsi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r12), %rax
	movq	-24(%rax), %rax
	movq	240(%r12,%rax), %r13
	testq	%r13, %r13
	je	.L2148
	cmpb	$0, 56(%r13)
	je	.L2073
	movsbl	67(%r13), %esi
.L2074:
	movq	%r12, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	xorl	%eax, %eax
	jmp	.L2099
	.p2align 4,,10
	.p2align 3
.L2070:
	movq	%r8, %rdi
	movq	%r8, -272(%rbp)
	call	_ZNK2v88internal8compiler13JSFunctionRef6sharedEv@PLT
	leaq	-192(%rbp), %r9
	movq	%r9, %rdi
	movq	%r9, -248(%rbp)
	movq	%rax, -192(%rbp)
	movq	%rdx, -184(%rbp)
	call	_ZNK2v88internal8compiler21SharedFunctionInfoRef12HasBreakInfoEv@PLT
	movq	-248(%rbp), %r9
	testb	%al, %al
	jne	.L2137
	movq	%r9, %rdi
	call	_ZNK2v88internal8compiler21SharedFunctionInfoRef4kindEv@PLT
	movq	-248(%rbp), %r9
	subl	$2, %eax
	cmpb	$3, %al
	jbe	.L2137
	movq	%r9, %rdi
	call	_ZNK2v88internal8compiler21SharedFunctionInfoRef13language_modeEv@PLT
	movq	-248(%rbp), %r9
	movq	-272(%rbp), %r8
	testb	%al, %al
	jne	.L2140
	movq	%r9, %rdi
	movq	%r8, -272(%rbp)
	movq	%r9, -248(%rbp)
	call	_ZNK2v88internal8compiler21SharedFunctionInfoRef6nativeEv@PLT
	movq	-248(%rbp), %r9
	movq	-272(%rbp), %r8
	testb	%al, %al
	jne	.L2140
	cmpq	$75431937, -216(%rbp)
	je	.L2140
	leaq	-216(%rbp), %rdi
	movl	$75431937, %esi
	movq	%r9, -272(%rbp)
	movq	%r8, -248(%rbp)
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	movq	-248(%rbp), %r8
	movq	-272(%rbp), %r9
	testb	%al, %al
	jne	.L2140
	movq	%r8, %rdi
	movq	%r9, -272(%rbp)
	movq	%r8, -248(%rbp)
	call	_ZNK2v88internal8compiler13JSFunctionRef14native_contextEv@PLT
	movq	-248(%rbp), %r8
	movq	-272(%rbp), %r9
	movq	%rax, -176(%rbp)
	movq	24(%r12), %rax
	movq	%rdx, -168(%rbp)
	cmpb	$0, 24(%rax)
	je	.L2149
	movdqu	32(%rax), %xmm3
	leaq	-176(%rbp), %r10
	leaq	-160(%rbp), %rax
	movq	%r8, -288(%rbp)
	movq	%rax, %rsi
	movq	%r10, %rdi
	movq	%r9, -296(%rbp)
	movq	%rax, -248(%rbp)
	movq	%r10, -272(%rbp)
	movaps	%xmm3, -160(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef6equalsERKS2_@PLT
	movq	-288(%rbp), %r8
	testb	%al, %al
	je	.L2137
	movq	-256(%rbp), %xmm1
	movq	16(%r12), %r11
	movq	%r8, %rdi
	movhps	-264(%rbp), %xmm1
	movq	%r11, -256(%rbp)
	movaps	%xmm1, -288(%rbp)
	call	_ZNK2v88internal8compiler13JSFunctionRef14native_contextEv@PLT
	movq	-272(%rbp), %r10
	movq	%rax, -176(%rbp)
	movq	%r10, %rdi
	movq	%rdx, -168(%rbp)
	call	_ZNK2v88internal8compiler16NativeContextRef19global_proxy_objectEv@PLT
	movq	-256(%rbp), %r11
	movq	-248(%rbp), %rsi
	movq	%rdx, -152(%rbp)
	movq	%r11, %rdi
	movq	%rax, -160(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movl	%r13d, %esi
	movq	%rax, -256(%rbp)
	movq	16(%r12), %rax
	movq	(%rax), %r10
	movq	376(%rax), %rdi
	movq	%r10, -272(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder15ConvertReceiverENS0_19ConvertReceiverModeE@PLT
	movq	-272(%rbp), %r10
	movq	%r15, %xmm0
	xorl	%r8d, %r8d
	movdqa	-288(%rbp), %xmm1
	leaq	-96(%rbp), %r15
	movq	%rax, %rsi
	movl	$4, %edx
	movhps	-256(%rbp), %xmm0
	movq	%r10, %rdi
	movq	%r15, %rcx
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm1, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rbx, %rdi
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%rax, -256(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties17ReplaceValueInputEPNS1_4NodeES4_i@PLT
	movq	-296(%rbp), %r9
	movq	-248(%rbp), %rdi
.L2076:
	movq	16(%r12), %rax
	movq	%r9, -288(%rbp)
	movq	(%rax), %r10
	movq	376(%rax), %r13
	movq	%r10, -272(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder20ForJSFunctionContextEv@PLT
	movq	-248(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9LoadFieldERKNS1_11FieldAccessE@PLT
	movq	%r14, %xmm0
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	-272(%rbp), %r10
	movq	%rax, %rsi
	movl	$3, %edx
	movq	-264(%rbp), %rax
	movhps	-256(%rbp), %xmm0
	movq	%r10, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rbx, %rdi
	movq	%rax, %r14
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties19ReplaceContextInputEPNS1_4NodeES4_@PLT
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties18ReplaceEffectInputEPNS1_4NodeES4_i@PLT
	movq	16(%r12), %rdi
	call	_ZN2v88internal8compiler7JSGraph17UndefinedConstantEv@PLT
	movdqa	-192(%rbp), %xmm2
	movq	-248(%rbp), %rdi
	movq	%rax, %r14
	movaps	%xmm2, -160(%rbp)
	call	_ZNK2v88internal8compiler21SharedFunctionInfoRef31internal_formal_parameter_countEv@PLT
	movq	-288(%rbp), %r9
	cmpl	$65535, %eax
	movq	%r9, %rdi
	je	.L2078
	movl	-240(%rbp), %r15d
	cmpl	%eax, %r15d
	je	.L2078
	movq	%r9, -256(%rbp)
	call	_ZNK2v88internal8compiler21SharedFunctionInfoRef33is_safe_to_skip_arguments_adaptorEv@PLT
	movq	-256(%rbp), %r9
	testb	%al, %al
	je	.L2079
	movq	%r9, %rdi
	call	_ZNK2v88internal8compiler21SharedFunctionInfoRef31internal_formal_parameter_countEv@PLT
	movl	%eax, %r13d
	cmpl	%eax, %r15d
	jle	.L2083
	movl	-240(%rbp), %r15d
.L2080:
	leal	1(%r15), %esi
	movq	%rbx, %rdi
	subl	$1, %r15d
	call	_ZN2v88internal8compiler4Node11RemoveInputEi@PLT
	cmpl	%r15d, %r13d
	jl	.L2080
	movl	-232(%rbp), %eax
	movl	%r13d, %edx
	movl	$0, %ecx
	subl	$3, %eax
	subl	%eax, %edx
	cmpl	%eax, %r13d
	cmovg	%ecx, %edx
	addl	%edx, %eax
	movl	%eax, -240(%rbp)
.L2083:
	cmpl	-240(%rbp), %r13d
	jle	.L2081
	movl	-240(%rbp), %r15d
	leal	2(%r13), %eax
	movl	%r13d, -232(%rbp)
	movq	%r12, %r13
	movq	%rbx, %r12
	movl	%eax, %ebx
	addl	$2, %r15d
.L2084:
	movq	16(%r13), %rdi
	call	_ZN2v88internal8compiler7JSGraph17UndefinedConstantEv@PLT
	movl	%r15d, %edx
	movq	%r12, %rdi
	addl	$1, %r15d
	movq	%rax, %rcx
	movq	16(%r13), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	cmpl	%r15d, %ebx
	jne	.L2084
	movq	%r12, %rbx
	movq	%r13, %r12
	movl	-232(%rbp), %r13d
	movl	%r13d, -240(%rbp)
.L2081:
	movq	16(%r12), %rax
	movl	-240(%rbp), %r15d
	movq	%r14, %rcx
	movq	%rbx, %rdi
	movq	(%rax), %rax
	leal	2(%r15), %edx
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	pxor	%xmm0, %xmm0
	movq	16(%r12), %rdi
	cvtsi2sdl	%r15d, %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	leal	3(%r15), %edx
	movq	%rbx, %rdi
	movq	%rax, %rcx
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%r12), %rax
	leal	1(%r15), %edx
	movl	$5, %ecx
	movq	8(%rax), %r12
	movq	(%rax), %rax
.L2141:
	movq	(%rax), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler7Linkage19GetJSCallDescriptorEPNS0_4ZoneEbiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEE@PLT
.L2142:
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	jmp	.L2085
.L2073:
	movq	%r13, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	0(%r13), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L2074
	movq	%r13, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L2074
.L2140:
	leaq	-160(%rbp), %rax
	leaq	-96(%rbp), %r15
	movq	%rax, -248(%rbp)
	movq	%rax, %rdi
	jmp	.L2076
.L2078:
	movq	%r9, -256(%rbp)
	call	_ZNK2v88internal8compiler21SharedFunctionInfoRef12HasBuiltinIdEv@PLT
	movq	-256(%rbp), %r9
	testb	%al, %al
	je	.L2090
	movq	%r9, %rdi
	movq	%r9, -256(%rbp)
	call	_ZNK2v88internal8compiler21SharedFunctionInfoRef10builtin_idEv@PLT
	movl	%eax, %edi
	call	_ZN2v88internal8Builtins5IsCppEi@PLT
	movq	-256(%rbp), %r9
	testb	%al, %al
	jne	.L2150
.L2090:
	movq	%r9, %rdi
	movq	%r9, -256(%rbp)
	call	_ZNK2v88internal8compiler21SharedFunctionInfoRef12HasBuiltinIdEv@PLT
	movl	-232(%rbp), %ecx
	movq	-256(%rbp), %r9
	testb	%al, %al
	leal	-1(%rcx), %r15d
	je	.L2088
	movq	%r9, %rdi
	movq	%r9, -256(%rbp)
	call	_ZNK2v88internal8compiler21SharedFunctionInfoRef10builtin_idEv@PLT
	movl	%eax, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	movq	-256(%rbp), %r9
	subl	$1, %eax
	je	.L2151
.L2088:
	movq	16(%r12), %rax
	movq	%r14, %rcx
	movl	-232(%rbp), %r14d
	movq	%rbx, %rdi
	movq	(%rax), %rax
	movl	%r14d, %edx
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	pxor	%xmm0, %xmm0
	movq	16(%r12), %rdi
	cvtsi2sdl	-240(%rbp), %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movl	%r14d, %edx
	movq	%rbx, %rdi
	movq	%rax, %rcx
	movq	16(%r12), %rax
	addl	$1, %edx
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%r12), %rax
	movl	$5, %ecx
	movl	%r15d, %edx
	movq	8(%rax), %r12
	movq	(%rax), %rax
	jmp	.L2141
.L2079:
	movq	16(%r12), %rax
	movq	-248(%rbp), %rdi
	movq	%r9, -256(%rbp)
	movq	360(%rax), %rsi
	call	_ZN2v88internal11CodeFactory15ArgumentAdaptorEPNS0_7IsolateE@PLT
	movq	16(%r12), %rdi
	movq	-160(%rbp), %rsi
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%rax, %rcx
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%r12), %rax
	movq	%r14, %rcx
	movq	%rbx, %rdi
	movl	$2, %edx
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	pxor	%xmm0, %xmm0
	movq	16(%r12), %rdi
	cvtsi2sdl	-240(%rbp), %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movl	$3, %edx
	movq	%rbx, %rdi
	movq	%rax, %rcx
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	-256(%rbp), %r9
	movq	16(%r12), %r13
	movq	%r9, %rdi
	call	_ZNK2v88internal8compiler21SharedFunctionInfoRef31internal_formal_parameter_countEv@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rdi
	cvtsi2sdl	%eax, %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%rbx, %rdi
	movl	$4, %edx
	movq	%rax, %rcx
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%r12), %rax
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	-144(%rbp), %rdx
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	leaq	-176(%rbp), %rsi
	movq	8(%rax), %r12
	movq	%rcx, -176(%rbp)
	movl	$1, %ecx
	movq	%rdx, -168(%rbp)
	movq	(%rax), %rax
	movl	-232(%rbp), %edx
	movq	(%rax), %rdi
	subl	$1, %edx
	call	_ZN2v88internal8compiler7Linkage21GetStubCallDescriptorEPNS0_4ZoneERKNS0_23CallInterfaceDescriptorEiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEENS9_INS1_8Operator8PropertyEhEENS0_12StubCallModeE@PLT
	jmp	.L2142
.L2146:
	call	__stack_chk_fail@PLT
.L2150:
	movq	%r9, %rdi
	call	_ZNK2v88internal8compiler21SharedFunctionInfoRef10builtin_idEv@PLT
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	16(%r12), %r12
	movl	%eax, %r13d
	movq	(%rbx), %rax
	movzwl	16(%rax), %r15d
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rax, -248(%rbp)
	cmpl	$760, %r15d
	je	.L2152
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler7JSGraph17UndefinedConstantEv@PLT
	movq	%rax, -240(%rbp)
.L2092:
	xorl	%edx, %edx
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler7JSGraph18CEntryStubConstantEiNS0_14SaveFPRegsModeENS0_8ArgvModeEb@PLT
	leaq	32(%rbx), %rdx
	movq	%rax, %r14
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L2093
	movq	32(%rbx), %rdi
	movq	%rbx, %rax
	cmpq	%rdi, %r14
	je	.L2095
.L2094:
	leaq	-24(%rax), %rsi
	testq	%rdi, %rdi
	je	.L2097
	movq	%rdx, -264(%rbp)
	movq	%rsi, -256(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-264(%rbp), %rdx
	movq	-256(%rbp), %rsi
.L2097:
	movq	%r14, (%rdx)
	testq	%r14, %r14
	je	.L2095
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L2095:
	movq	(%r12), %rax
	movq	(%rax), %r14
	cmpl	$760, %r15d
	je	.L2153
.L2098:
	movl	-232(%rbp), %eax
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	leal	3(%rax), %r15d
	cvtsi2sdl	%r15d, %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%r12, %rdi
	movq	%rax, -256(%rbp)
	call	_ZN2v88internal8compiler7JSGraph15TheHoleConstantEv@PLT
	movl	-232(%rbp), %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	-256(%rbp), %r8
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movl	-232(%rbp), %eax
	movq	%r8, %rcx
	leal	1(%rax), %edx
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movl	-232(%rbp), %eax
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	-248(%rbp), %rcx
	leal	2(%rax), %edx
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movl	%r15d, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	-240(%rbp), %rcx
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movl	%r13d, %edi
	call	_ZN2v88internal8Builtins10CppEntryOfEi@PLT
	movq	%rax, %rdi
	call	_ZN2v88internal17ExternalReference6CreateEm@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler12MachineGraph16ExternalConstantENS0_17ExternalReferenceE@PLT
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	%rax, %rcx
	movl	-232(%rbp), %eax
	leal	4(%rax), %edx
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	-256(%rbp), %r8
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movl	-232(%rbp), %eax
	movq	%r8, %rcx
	leal	5(%rax), %edx
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movl	%r13d, %edi
	call	_ZN2v88internal8Builtins4nameEi@PLT
	movl	%r15d, %edx
	movq	%r14, %rdi
	movl	$1, %esi
	movq	%rax, %rcx
	movq	(%rbx), %rax
	movl	$1, %r9d
	movzbl	18(%rax), %r8d
	call	_ZN2v88internal8compiler7Linkage27GetCEntryStubCallDescriptorEPNS0_4ZoneEiiPKcNS_4base5FlagsINS1_8Operator8PropertyEhEENS8_INS1_14CallDescriptor4FlagEiEE@PLT
	movq	8(%r12), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	jmp	.L2085
.L2093:
	movq	32(%rbx), %rax
	movq	16(%rax), %rdi
	leaq	16(%rax), %rdx
	cmpq	%rdi, %r14
	jne	.L2094
	jmp	.L2095
.L2152:
	movl	-232(%rbp), %eax
	movq	%rbx, %rdi
	leal	-1(%rax), %esi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rax, -240(%rbp)
	jmp	.L2092
.L2151:
	movq	%r9, %rdi
	call	_ZNK2v88internal8compiler21SharedFunctionInfoRef10builtin_idEv@PLT
	movq	-248(%rbp), %rdi
	movl	%eax, %edx
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %ecx
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movl	%r15d, %edx
	leaq	-176(%rbp), %rsi
	movq	%rax, -176(%rbp)
	movq	-144(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler7Linkage21GetStubCallDescriptorEPNS0_4ZoneERKNS0_23CallInterfaceDescriptorEiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEENS9_INS1_8Operator8PropertyEhEENS0_12StubCallModeE@PLT
	movq	16(%r12), %rdi
	movq	-160(%rbp), %rsi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%rax, %rcx
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%r12), %rax
	movq	%r14, %rcx
	movq	%rbx, %rdi
	movl	$2, %edx
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	pxor	%xmm0, %xmm0
	movq	16(%r12), %rdi
	cvtsi2sdl	-240(%rbp), %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%rbx, %rdi
	movl	$3, %edx
	movq	%rax, %rcx
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%r12), %rax
	movq	%r13, %rsi
	movq	8(%rax), %rdi
	jmp	.L2143
.L2153:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler7JSGraph17UndefinedConstantEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %r15
	movl	-232(%rbp), %eax
	leal	-1(%rax), %esi
	call	_ZN2v88internal8compiler4Node11RemoveInputEi@PLT
	movq	%r15, %rcx
	movl	$1, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	jmp	.L2098
.L2148:
	call	_ZSt16__throw_bad_castv@PLT
.L2149:
	leaq	.LC21(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE23837:
	.size	_ZN2v88internal8compiler15JSTypedLowering12ReduceJSCallEPNS1_4NodeE, .-_ZN2v88internal8compiler15JSTypedLowering12ReduceJSCallEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler15JSTypedLowering17ReduceJSForInNextEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15JSTypedLowering17ReduceJSForInNextEPNS1_4NodeE
	.type	_ZN2v88internal8compiler15JSTypedLowering17ReduceJSForInNextEPNS1_4NodeE, @function
_ZN2v88internal8compiler15JSTypedLowering17ReduceJSForInNextEPNS1_4NodeE:
.LFB23841:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-176(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$264, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler11ForInModeOfEPKNS1_8OperatorE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movb	%al, -240(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -200(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, -248(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, -272(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%r12, %rdi
	movq	%rax, -256(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetContextInputEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, -288(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties18GetFrameStateInputEPNS1_4NodeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -296(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -208(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%r15, %rdi
	movq	%rax, %r13
	movq	16(%rbx), %rax
	movq	(%rax), %r10
	movq	376(%rax), %r14
	movq	%r10, -224(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	%r14, %rdi
	movq	%r15, %rsi
	leaq	-112(%rbp), %r14
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9LoadFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$3, %edx
	movq	-200(%rbp), %xmm0
	movq	-224(%rbp), %r10
	movq	%rax, %rsi
	movq	%r13, -96(%rbp)
	movhps	-208(%rbp), %xmm0
	movq	%r10, %rdi
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movzbl	-240(%rbp), %r9d
	movq	%rax, -208(%rbp)
	cmpb	$1, %r9b
	jbe	.L2155
	cmpb	$2, %r9b
	jne	.L2157
	movq	%rax, %xmm3
	movq	%rax, %xmm1
	movq	16(%rbx), %rax
	movq	-200(%rbp), %xmm0
	movq	%r13, %xmm5
	movhps	-272(%rbp), %xmm3
	movq	%r15, %rdi
	movq	-248(%rbp), %xmm2
	movq	(%rax), %r9
	movq	376(%rax), %r8
	punpcklqdq	%xmm5, %xmm1
	movhps	-288(%rbp), %xmm0
	movhps	-256(%rbp), %xmm2
	movaps	%xmm0, -288(%rbp)
	movaps	%xmm3, -272(%rbp)
	movq	%r9, -208(%rbp)
	movaps	%xmm2, -240(%rbp)
	movaps	%xmm1, -224(%rbp)
	movq	%r8, -200(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder20ForFixedArrayElementEv@PLT
	movq	-200(%rbp), %r8
	movq	%r15, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder11LoadElementERKNS1_13ElementAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$4, %edx
	movq	-208(%rbp), %r9
	movdqa	-240(%rbp), %xmm2
	movq	%rax, %rsi
	movdqa	-224(%rbp), %xmm1
	movq	%r9, %rdi
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm1, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -200(%rbp)
	movq	16(%rbx), %rax
	movq	(%rax), %r9
	movq	376(%rax), %rdi
	movq	%r9, -208(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14ReferenceEqualEv@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$2, %edx
	movq	-208(%rbp), %r9
	movdqa	-272(%rbp), %xmm3
	movq	%rax, %rsi
	movq	%r9, %rdi
	movaps	%xmm3, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$1, %edx
	movl	$1, %esi
	movq	%rax, -208(%rbp)
	movq	16(%rbx), %rax
	movq	(%rax), %r9
	movq	8(%rax), %rdi
	movq	%r9, -224(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	movq	%r13, %xmm6
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	-224(%rbp), %r9
	movq	%rax, %rsi
	movl	$2, %edx
	movq	-208(%rbp), %xmm1
	movq	%r9, %rdi
	punpcklqdq	%xmm6, %xmm1
	movaps	%xmm1, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r13
	movq	16(%rbx), %rax
	movq	(%rax), %r9
	movq	8(%rax), %rdi
	movq	%r9, -208(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$1, %edx
	movq	-208(%rbp), %r9
	movq	%rax, %rsi
	movq	%r13, -112(%rbp)
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -224(%rbp)
	movq	16(%rbx), %rax
	movq	(%rax), %r9
	movq	8(%rax), %rdi
	movq	%r9, -208(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$1, %edx
	movq	-208(%rbp), %r9
	movq	%rax, %rsi
	movq	%r13, -112(%rbp)
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r15, %rdi
	movl	$492, %edx
	leaq	-192(%rbp), %r15
	movq	%rax, %r13
	movq	16(%rbx), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	%r15, %rsi
	movq	-160(%rbp), %rax
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	movl	8(%rax), %edx
	subl	(%rax), %edx
	movq	%rax, -184(%rbp)
	movq	16(%rbx), %rax
	movq	%rcx, -192(%rbp)
	movl	$1, %ecx
	movq	(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler7Linkage21GetStubCallDescriptorEPNS0_4ZoneERKNS0_23CallInterfaceDescriptorEiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEENS9_INS1_8Operator8PropertyEhEENS0_12StubCallModeE@PLT
	movq	16(%rbx), %rdi
	movq	-176(%rbp), %rsi
	movq	%rax, -248(%rbp)
	movq	(%rdi), %r9
	movq	%r9, -240(%rbp)
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-248(%rbp), %r8
	movq	%rax, -208(%rbp)
	movq	16(%rbx), %rax
	movq	%r8, %rsi
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$7, %edx
	movdqa	-288(%rbp), %xmm0
	movq	-240(%rbp), %r9
	movq	%rax, %rsi
	movq	%r13, -64(%rbp)
	movq	-208(%rbp), %xmm1
	movaps	%xmm0, -96(%rbp)
	movq	%r9, %rdi
	movq	-296(%rbp), %xmm0
	movhps	-200(%rbp), %xmm1
	movhps	-200(%rbp), %xmm0
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	$0, -192(%rbp)
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14NodeProperties17IsExceptionalCallEPNS1_4NodeEPS4_@PLT
	movq	%r13, -208(%rbp)
	testb	%al, %al
	jne	.L2242
.L2178:
	movq	16(%rbx), %rax
	movl	$2, %esi
	movq	8(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$2, %edx
	movq	-224(%rbp), %xmm0
	movq	%r15, %rdi
	movq	%rax, %rsi
	movhps	-208(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$2, %esi
	movq	%rax, %r15
	movq	16(%rbx), %rax
	movq	(%rax), %r9
	movq	8(%rax), %rdi
	movq	%r9, -208(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9EffectPhiEi@PLT
	movq	%r13, %xmm7
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	-208(%rbp), %r9
	movq	%rax, %rsi
	movl	$3, %edx
	movq	-200(%rbp), %xmm0
	movq	%r15, -96(%rbp)
	punpcklqdq	%xmm7, %xmm0
	movq	%r9, %rdi
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%rbx), %rdi
	movq	%r12, %rdx
	movq	%r15, %r8
	movq	%rax, %rcx
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	movzbl	23(%r12), %edx
	leaq	32(%r12), %rax
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L2179
	movq	32(%r12), %rdi
	movq	%rax, %rdx
	movq	%r12, %rsi
	cmpq	%rdi, -200(%rbp)
	je	.L2181
.L2180:
	leaq	-24(%rsi), %r14
	testq	%rdi, %rdi
	je	.L2183
	movq	%r14, %rsi
	movq	%rax, -224(%rbp)
	movq	%rdx, -208(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-224(%rbp), %rax
	movq	-208(%rbp), %rdx
.L2183:
	movq	-200(%rbp), %rdi
	movq	%rdi, (%rdx)
	testq	%rdi, %rdi
	je	.L2184
	movq	%r14, %rsi
	movq	%rax, -200(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movq	-200(%rbp), %rax
.L2184:
	movzbl	23(%r12), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L2243
.L2181:
	movq	8(%rax), %rdi
	cmpq	%rdi, %r13
	je	.L2185
	leaq	8(%rax), %rdx
	movq	%r12, %rsi
.L2186:
	leaq	-48(%rsi), %r14
	testq	%rdi, %rdi
	je	.L2188
	movq	%r14, %rsi
	movq	%rax, -208(%rbp)
	movq	%rdx, -200(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-208(%rbp), %rax
	movq	-200(%rbp), %rdx
.L2188:
	movq	%r13, (%rdx)
	testq	%r13, %r13
	je	.L2189
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, -200(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movq	-200(%rbp), %rax
.L2189:
	movzbl	23(%r12), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L2185
	movq	32(%r12), %rsi
	leaq	16(%rsi), %rdx
.L2187:
	movq	16(%rdx), %rdi
	cmpq	%rdi, %r15
	je	.L2192
	leaq	16(%rdx), %r14
	jmp	.L2191
	.p2align 4,,10
	.p2align 3
.L2185:
	movq	16(%rax), %rdi
	cmpq	%rdi, %r15
	je	.L2192
	leaq	16(%rax), %r14
	movq	%r12, %rsi
.L2191:
	leaq	-72(%rsi), %r13
	testq	%rdi, %rdi
	je	.L2193
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L2193:
	movq	%r15, (%r14)
	testq	%r15, %r15
	je	.L2192
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L2192:
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node14TrimInputCountEi@PLT
	movq	16(%rbx), %rax
	movl	$8, %esi
	movl	$2, %edx
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
.L2157:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2244
	addq	$264, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2155:
	.cfi_restore_state
	movq	%rax, %xmm0
	movq	16(%rbx), %rax
	movhps	-272(%rbp), %xmm0
	movq	(%rax), %r9
	movq	376(%rax), %rdi
	movaps	%xmm0, -224(%rbp)
	movq	%r9, -200(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14ReferenceEqualEv@PLT
	movq	%r14, %rcx
	xorl	%r8d, %r8d
	movl	$2, %edx
	movq	-200(%rbp), %r9
	movdqa	-224(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r9, %rdi
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r15, %rdx
	movl	$37, %esi
	movq	%rax, -200(%rbp)
	movq	16(%rbx), %rax
	movq	(%rax), %r9
	movq	376(%rax), %rdi
	movq	$0, -176(%rbp)
	movl	$-1, -168(%rbp)
	movq	%r9, -224(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder7CheckIfENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceE@PLT
	movq	%r14, %rcx
	xorl	%r8d, %r8d
	movl	$3, %edx
	movq	-224(%rbp), %r9
	movq	%rax, %rsi
	movq	%r13, -96(%rbp)
	leaq	32(%r12), %r14
	movq	-200(%rbp), %xmm0
	movq	%r9, %rdi
	movhps	-208(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%rbx), %rdi
	movq	%r13, %r8
	movq	%r12, %rcx
	movq	%rax, -200(%rbp)
	movq	%r12, %rdx
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L2158
	movq	32(%r12), %rdi
	movq	%r14, %rax
	movq	%r12, %rsi
	cmpq	%rdi, -248(%rbp)
	je	.L2160
.L2159:
	subq	$24, %rsi
	testq	%rdi, %rdi
	je	.L2162
	movq	%rax, -224(%rbp)
	movq	%rsi, -208(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-224(%rbp), %rax
	movq	-208(%rbp), %rsi
.L2162:
	movq	-248(%rbp), %rcx
	movq	%rcx, (%rax)
	testq	%rcx, %rcx
	je	.L2163
	movq	%rcx, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L2163:
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L2245
.L2160:
	movq	8(%r14), %rdi
	cmpq	%rdi, -256(%rbp)
	je	.L2164
	leaq	8(%r14), %rax
	movq	%r12, %rsi
.L2165:
	subq	$48, %rsi
	testq	%rdi, %rdi
	je	.L2167
	movq	%rax, -224(%rbp)
	movq	%rsi, -208(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-224(%rbp), %rax
	movq	-208(%rbp), %rsi
.L2167:
	movq	-256(%rbp), %rcx
	movq	%rcx, (%rax)
	testq	%rcx, %rcx
	je	.L2168
	movq	%rcx, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L2168:
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2164
	movq	32(%r12), %rsi
	leaq	16(%rsi), %rax
.L2166:
	movq	16(%rax), %rdi
	cmpq	%rdi, -200(%rbp)
	je	.L2171
	addq	$16, %rax
	jmp	.L2170
	.p2align 4,,10
	.p2align 3
.L2164:
	movq	16(%r14), %rdi
	cmpq	%rdi, -200(%rbp)
	je	.L2169
	leaq	16(%r14), %rax
	movq	%r12, %rsi
.L2170:
	subq	$72, %rsi
	testq	%rdi, %rdi
	je	.L2172
	movq	%rax, -224(%rbp)
	movq	%rsi, -208(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-224(%rbp), %rax
	movq	-208(%rbp), %rsi
.L2172:
	movq	-200(%rbp), %rcx
	movq	%rcx, (%rax)
	testq	%rcx, %rcx
	je	.L2173
	movq	%rcx, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L2173:
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2169
	movq	32(%r12), %rsi
	leaq	16(%rsi), %rax
.L2171:
	movq	24(%rax), %rdi
	cmpq	%rdi, %r13
	je	.L2176
	leaq	24(%rax), %rdx
	jmp	.L2175
	.p2align 4,,10
	.p2align 3
.L2169:
	movq	24(%r14), %rdi
	cmpq	%rdi, %r13
	je	.L2176
	leaq	24(%r14), %rdx
	movq	%r12, %rsi
.L2175:
	leaq	-96(%rsi), %r14
	testq	%rdi, %rdi
	je	.L2177
	movq	%r14, %rsi
	movq	%rdx, -200(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-200(%rbp), %rdx
.L2177:
	movq	%r13, (%rdx)
	testq	%r13, %r13
	je	.L2176
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L2176:
	movl	$4, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node14TrimInputCountEi@PLT
	movq	16(%rbx), %rax
	movq	%r15, %rdi
	movq	376(%rax), %r13
	call	_ZN2v88internal8compiler13AccessBuilder20ForFixedArrayElementEv@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder11LoadElementERKNS1_13ElementAccessE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	$16385, 8(%r12)
	jmp	.L2157
	.p2align 4,,10
	.p2align 3
.L2242:
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9IfSuccessEv@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	%r15, %rdi
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r13, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-192(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%rax, -208(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties19ReplaceControlInputEPNS1_4NodeES4_i@PLT
	movq	-192(%rbp), %rdi
	movq	%r13, %rsi
	xorl	%edx, %edx
	call	_ZN2v88internal8compiler14NodeProperties18ReplaceEffectInputEPNS1_4NodeES4_i@PLT
	movq	8(%rbx), %rdi
	movq	-192(%rbp), %rsi
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L2178
	.p2align 4,,10
	.p2align 3
.L2243:
	movq	32(%r12), %rsi
	leaq	16(%rsi), %rdx
.L2182:
	movq	8(%rdx), %rdi
	cmpq	%rdi, %r13
	je	.L2187
	addq	$8, %rdx
	jmp	.L2186
	.p2align 4,,10
	.p2align 3
.L2245:
	movq	32(%r12), %rsi
	leaq	16(%rsi), %rax
.L2161:
	movq	8(%rax), %rdi
	cmpq	%rdi, -256(%rbp)
	je	.L2166
	addq	$8, %rax
	jmp	.L2165
	.p2align 4,,10
	.p2align 3
.L2158:
	movq	32(%r12), %rsi
	movq	16(%rsi), %rdi
	leaq	16(%rsi), %rax
	cmpq	%rdi, -248(%rbp)
	jne	.L2159
	jmp	.L2161
	.p2align 4,,10
	.p2align 3
.L2179:
	movq	32(%r12), %rsi
	movq	16(%rsi), %rdi
	leaq	16(%rsi), %rdx
	cmpq	%rdi, -200(%rbp)
	jne	.L2180
	jmp	.L2182
.L2244:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23841:
	.size	_ZN2v88internal8compiler15JSTypedLowering17ReduceJSForInNextEPNS1_4NodeE, .-_ZN2v88internal8compiler15JSTypedLowering17ReduceJSForInNextEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler15JSTypedLowering20ReduceJSForInPrepareEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15JSTypedLowering20ReduceJSForInPrepareEPNS1_4NodeE
	.type	_ZN2v88internal8compiler15JSTypedLowering20ReduceJSForInPrepareEPNS1_4NodeE, @function
_ZN2v88internal8compiler15JSTypedLowering20ReduceJSForInPrepareEPNS1_4NodeE:
.LFB23842:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$200, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -176(%rbp)
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler11ForInModeOfEPKNS1_8OperatorE@PLT
	xorl	%esi, %esi
	movq	%r15, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rax, -184(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rax, -160(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, -168(%rbp)
	cmpb	$1, %bl
	ja	.L2289
	movq	16(%r13), %rax
	leaq	-144(%rbp), %r12
	xorl	%esi, %esi
	leaq	-80(%rbp), %rbx
	movq	-184(%rbp), %xmm0
	movq	%r12, %rcx
	movq	(%rax), %r14
	movq	376(%rax), %rdi
	movq	$0, -144(%rbp)
	movl	$-1, -136(%rbp)
	movq	360(%rax), %rdx
	movhps	-160(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	addq	$136, %rdx
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9CheckMapsENS_4base5FlagsINS1_13CheckMapsFlagEiEENS0_13ZoneHandleSetINS0_3MapEEERKNS1_14FeedbackSourceE@PLT
	movq	%rbx, %rcx
	movl	$3, %edx
	movq	%r14, %rdi
	movq	-168(%rbp), %r15
	movdqa	-160(%rbp), %xmm0
	movq	%rax, %rsi
	xorl	%r8d, %r8d
	movaps	%xmm0, -80(%rbp)
	movq	%r15, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r12, %rdi
	movq	%rax, -160(%rbp)
	movq	16(%r13), %rax
	movq	376(%rax), %r8
	movq	(%rax), %r14
	movq	%r8, -192(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder17ForMapDescriptorsEv@PLT
	movq	-192(%rbp), %r8
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9LoadFieldERKNS1_11FieldAccessE@PLT
	movq	%rbx, %rcx
	movl	$3, %edx
	movq	%r14, %rdi
	movq	-184(%rbp), %xmm0
	movq	%rax, %rsi
	xorl	%r8d, %r8d
	movq	%r15, -64(%rbp)
	movhps	-160(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r12, %rdi
	movq	%rax, -160(%rbp)
	movq	16(%r13), %rax
	movq	376(%rax), %r8
	movq	(%rax), %r14
	movq	%r8, -192(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder27ForDescriptorArrayEnumCacheEv@PLT
	movq	-192(%rbp), %r8
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9LoadFieldERKNS1_11FieldAccessE@PLT
	movq	%rbx, %rcx
	movl	$3, %edx
	movq	%r14, %rdi
	movq	-160(%rbp), %xmm0
	movq	%rax, %rsi
	xorl	%r8d, %r8d
	movq	%r15, -64(%rbp)
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r12, %rdi
	movq	%rax, -160(%rbp)
	movq	16(%r13), %rax
	movq	376(%rax), %r8
	movq	(%rax), %r14
	movq	%r8, -192(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder16ForEnumCacheKeysEv@PLT
	movq	-192(%rbp), %r8
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9LoadFieldERKNS1_11FieldAccessE@PLT
	movq	%rbx, %rcx
	movl	$3, %edx
	movq	%r14, %rdi
	movq	-160(%rbp), %xmm0
	movq	%rax, %rsi
	xorl	%r8d, %r8d
	movq	%r15, -64(%rbp)
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r12, %rdi
	movq	%rax, -192(%rbp)
	movq	16(%r13), %rax
	movq	376(%rax), %r8
	movq	(%rax), %r14
	movq	%r8, -160(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder15ForMapBitField3Ev@PLT
	movq	-160(%rbp), %r8
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9LoadFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$3, %edx
	movq	-184(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r14, %rdi
	movq	%r15, -64(%rbp)
	movhps	-192(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%r13), %rdi
	movsd	.LC22(%rip), %xmm0
	movq	%rax, %r15
	movq	%rax, -160(%rbp)
	movq	(%rdi), %r14
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%rax, -200(%rbp)
	movq	16(%r13), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder16NumberBitwiseAndEv@PLT
	movq	%r15, %xmm0
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movhps	-200(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movq	%r14, %rdi
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -200(%rbp)
.L2249:
	movq	-176(%rbp), %rax
	movq	24(%rax), %r14
	testq	%r14, %r14
	je	.L2250
	movq	(%r14), %rbx
	.p2align 4,,10
	.p2align 3
.L2267:
	movl	16(%r14), %edx
	movl	%edx, %eax
	shrl	%eax
	leaq	3(%rax,%rax,2), %rcx
	salq	$3, %rax
	andl	$1, %edx
	leaq	(%r14,%rcx,8), %r15
	leaq	32(%r15,%rax), %r12
	jne	.L2252
	leaq	16(%r15,%rax), %r12
	movq	(%r15), %r15
.L2252:
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler14NodeProperties12IsEffectEdgeENS1_4EdgeE@PLT
	testb	%al, %al
	je	.L2253
	movq	(%r12), %rdi
	cmpq	%rdi, -160(%rbp)
	je	.L2261
	testq	%rdi, %rdi
	je	.L2256
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L2256:
	movq	-160(%rbp), %rax
	movq	%rax, (%r12)
	testq	%rax, %rax
	je	.L2261
.L2288:
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L2261:
	movq	8(%r13), %rdi
	movq	%r15, %rsi
	movq	(%rdi), %rax
	call	*24(%rax)
.L2258:
	testq	%rbx, %rbx
	je	.L2250
	movq	%rbx, %r14
	movq	(%rbx), %rbx
	jmp	.L2267
	.p2align 4,,10
	.p2align 3
.L2253:
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler14NodeProperties13IsControlEdgeENS1_4EdgeE@PLT
	testb	%al, %al
	je	.L2259
	movq	(%r12), %rdi
	cmpq	%rdi, -168(%rbp)
	je	.L2261
	testq	%rdi, %rdi
	je	.L2262
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L2262:
	movq	-168(%rbp), %rax
	movq	%rax, (%r12)
	testq	%rax, %rax
	jne	.L2288
	jmp	.L2261
	.p2align 4,,10
	.p2align 3
.L2250:
	movq	-176(%rbp), %rdi
	call	_ZN2v88internal8compiler4Node4KillEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2290
	movq	-160(%rbp), %rax
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2259:
	.cfi_restore_state
	movq	(%r15), %rdi
	call	_ZN2v88internal8compiler17ProjectionIndexOfEPKNS1_8OperatorE@PLT
	cmpq	$1, %rax
	je	.L2264
	cmpq	$2, %rax
	jne	.L2291
	movq	8(%r13), %rdi
	movq	-200(%rbp), %rdx
	movq	%r15, %rsi
	movq	(%rdi), %rax
	call	*16(%rax)
	jmp	.L2258
	.p2align 4,,10
	.p2align 3
.L2291:
	testq	%rax, %rax
	je	.L2292
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2264:
	movq	8(%r13), %rdi
	movq	-192(%rbp), %rdx
	movq	%r15, %rsi
	movq	(%rdi), %rax
	call	*16(%rax)
	jmp	.L2258
	.p2align 4,,10
	.p2align 3
.L2292:
	movq	8(%r13), %rdi
	movq	-184(%rbp), %rdx
	movq	%r15, %rsi
	movq	(%rdi), %rax
	call	*16(%rax)
	jmp	.L2258
	.p2align 4,,10
	.p2align 3
.L2289:
	cmpb	$2, %bl
	jne	.L2293
	movq	16(%r13), %rax
	leaq	-80(%rbp), %rbx
	movq	-184(%rbp), %xmm0
	movq	360(%rax), %rsi
	movhps	-160(%rbp), %xmm0
	movq	376(%rax), %rdi
	movq	(%rax), %r12
	movaps	%xmm0, -160(%rbp)
	addq	$136, %rsi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder11CompareMapsENS0_13ZoneHandleSetINS0_3MapEEE@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movq	%r12, %rdi
	movdqa	-160(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$3, %edx
	movq	-168(%rbp), %rax
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$1, %edx
	movl	$1, %esi
	movq	%rax, -192(%rbp)
	movq	16(%r13), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$2, %edx
	movq	-192(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r12, %rdi
	movhps	-168(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r14
	movq	16(%r13), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	movq	%rbx, %rcx
	movl	$1, %edx
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movq	%r12, %rdi
	movq	%r14, -80(%rbp)
	leaq	-144(%rbp), %r12
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r12, %rdi
	movq	%rax, -160(%rbp)
	movq	16(%r13), %rax
	movq	(%rax), %r9
	movq	376(%rax), %r8
	movq	%r9, -200(%rbp)
	movq	%r8, -168(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder17ForMapDescriptorsEv@PLT
	movq	-168(%rbp), %r8
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9LoadFieldERKNS1_11FieldAccessE@PLT
	movq	%rbx, %rcx
	movl	$3, %edx
	xorl	%r8d, %r8d
	movq	-200(%rbp), %r9
	movq	%rax, %rsi
	movq	-160(%rbp), %rax
	movq	-184(%rbp), %xmm1
	movq	%r9, %rdi
	movq	%rax, -64(%rbp)
	movhps	-192(%rbp), %xmm1
	movaps	%xmm1, -80(%rbp)
	movaps	%xmm1, -240(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r12, %rdi
	movq	%rax, -168(%rbp)
	movq	16(%r13), %rax
	movq	(%rax), %r9
	movq	376(%rax), %r8
	movq	%r9, -200(%rbp)
	movq	%r8, -192(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder27ForDescriptorArrayEnumCacheEv@PLT
	movq	-192(%rbp), %r8
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9LoadFieldERKNS1_11FieldAccessE@PLT
	movq	%rbx, %rcx
	movl	$3, %edx
	xorl	%r8d, %r8d
	movq	-200(%rbp), %r9
	movq	%rax, %rsi
	movq	-160(%rbp), %rax
	movq	-168(%rbp), %xmm0
	movq	%r9, %rdi
	movq	%rax, -64(%rbp)
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r12, %rdi
	movq	%rax, -168(%rbp)
	movq	16(%r13), %rax
	movq	(%rax), %r9
	movq	376(%rax), %r8
	movq	%r9, -200(%rbp)
	movq	%r8, -192(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder16ForEnumCacheKeysEv@PLT
	movq	-192(%rbp), %r8
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9LoadFieldERKNS1_11FieldAccessE@PLT
	movq	%rbx, %rcx
	movl	$3, %edx
	xorl	%r8d, %r8d
	movq	-200(%rbp), %r9
	movq	%rax, %rsi
	movq	-160(%rbp), %rax
	movq	-168(%rbp), %xmm0
	movq	%r9, %rdi
	movq	%rax, -64(%rbp)
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r12, %rdi
	movq	%rax, -208(%rbp)
	movq	16(%r13), %rax
	movq	(%rax), %r9
	movq	376(%rax), %r8
	movq	%r9, -192(%rbp)
	movq	%r8, -168(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder15ForMapBitField3Ev@PLT
	movq	-168(%rbp), %r8
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9LoadFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$3, %edx
	movq	-192(%rbp), %r9
	movq	%rax, %rsi
	movq	-160(%rbp), %rax
	movq	-184(%rbp), %xmm0
	movq	%r9, %rdi
	movq	%rax, -64(%rbp)
	movhps	-208(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%r13), %rdi
	movsd	.LC22(%rip), %xmm0
	movq	%rax, -192(%rbp)
	movq	(%rdi), %r9
	movq	%r9, -200(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%rax, -168(%rbp)
	movq	16(%r13), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder16NumberBitwiseAndEv@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$2, %edx
	movq	-200(%rbp), %r9
	movq	%rax, %rsi
	movq	-192(%rbp), %xmm0
	movq	%r9, %rdi
	movhps	-168(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -216(%rbp)
	movq	16(%r13), %rax
	movq	(%rax), %r9
	movq	8(%rax), %rdi
	movq	%r9, -168(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	movq	%rbx, %rcx
	movl	$1, %edx
	xorl	%r8d, %r8d
	movq	-168(%rbp), %r9
	movq	%rax, %rsi
	movq	%r14, -80(%rbp)
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	movq	16(%r13), %rax
	movq	376(%rax), %r8
	movq	(%rax), %r14
	movq	%r8, -168(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder19ForFixedArrayLengthEv@PLT
	movq	-168(%rbp), %r8
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9LoadFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$3, %edx
	movdqa	-240(%rbp), %xmm1
	movq	%r14, %rdi
	movq	%rax, %rsi
	movq	%r15, -64(%rbp)
	movaps	%xmm1, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$2, %esi
	movq	%rax, -200(%rbp)
	movq	16(%r13), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
	movq	%r15, %xmm2
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movq	-160(%rbp), %xmm0
	movl	$2, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$2, %esi
	movq	%rax, %r15
	movq	%rax, -168(%rbp)
	movq	16(%r13), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9EffectPhiEi@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$3, %edx
	movq	-192(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r12, %rdi
	movq	%r15, -64(%rbp)
	movhps	-200(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$2, %edx
	movl	$8, %esi
	movq	%rax, -160(%rbp)
	movq	16(%r13), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movq	%r14, %rdi
	movq	-208(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$3, %edx
	movq	%r15, -64(%rbp)
	movhps	-184(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$2, %edx
	movl	$8, %esi
	movq	%rax, -192(%rbp)
	movq	16(%r13), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$3, %edx
	movq	-216(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r14, %rdi
	movq	%r15, -64(%rbp)
	movhps	-200(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -200(%rbp)
	jmp	.L2249
	.p2align 4,,10
	.p2align 3
.L2293:
	movq	$0, -200(%rbp)
	movq	$0, -192(%rbp)
	jmp	.L2249
.L2290:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23842:
	.size	_ZN2v88internal8compiler15JSTypedLowering20ReduceJSForInPrepareEPNS1_4NodeE, .-_ZN2v88internal8compiler15JSTypedLowering20ReduceJSForInPrepareEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler15JSTypedLowering19ReduceJSLoadMessageEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15JSTypedLowering19ReduceJSLoadMessageEPNS1_4NodeE
	.type	_ZN2v88internal8compiler15JSTypedLowering19ReduceJSLoadMessageEPNS1_4NodeE, @function
_ZN2v88internal8compiler15JSTypedLowering19ReduceJSLoadMessageEPNS1_4NodeE:
.LFB23843:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	360(%rax), %rdi
	call	_ZN2v88internal17ExternalReference30address_of_pending_message_objEPNS0_7IsolateE@PLT
	movq	16(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler12MachineGraph16ExternalConstantENS0_17ExternalReferenceE@PLT
	movq	%rax, %r13
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L2295
	movq	32(%r12), %rdi
	leaq	32(%r12), %r15
	movq	%r12, %rsi
	cmpq	%rdi, %r13
	je	.L2297
	leaq	-24(%rsi), %r14
	testq	%rdi, %rdi
	je	.L2299
.L2310:
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L2299:
	movq	%r13, (%r15)
	testq	%r13, %r13
	je	.L2297
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L2297:
	movq	16(%rbx), %rax
	leaq	-112(%rbp), %r13
	movq	%r13, %rdi
	movq	376(%rax), %r14
	call	_ZN2v88internal8compiler13AccessBuilder22ForExternalTaggedValueEv@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9LoadFieldERKNS1_11FieldAccessE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2309
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2295:
	.cfi_restore_state
	movq	32(%r12), %rsi
	movq	16(%rsi), %rdi
	leaq	16(%rsi), %r15
	cmpq	%rdi, %r13
	je	.L2297
	leaq	-24(%rsi), %r14
	testq	%rdi, %rdi
	jne	.L2310
	jmp	.L2299
.L2309:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23843:
	.size	_ZN2v88internal8compiler15JSTypedLowering19ReduceJSLoadMessageEPNS1_4NodeE, .-_ZN2v88internal8compiler15JSTypedLowering19ReduceJSLoadMessageEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler15JSTypedLowering20ReduceJSStoreMessageEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15JSTypedLowering20ReduceJSStoreMessageEPNS1_4NodeE
	.type	_ZN2v88internal8compiler15JSTypedLowering20ReduceJSStoreMessageEPNS1_4NodeE, @function
_ZN2v88internal8compiler15JSTypedLowering20ReduceJSStoreMessageEPNS1_4NodeE:
.LFB23844:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	360(%rax), %rdi
	call	_ZN2v88internal17ExternalReference30address_of_pending_message_objEPNS0_7IsolateE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	16(%rbx), %rdi
	movq	%r13, %rsi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler12MachineGraph16ExternalConstantENS0_17ExternalReferenceE@PLT
	movzbl	23(%r12), %edx
	movq	%rax, %r15
	leaq	32(%r12), %rax
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L2312
	movq	32(%r12), %rdi
	movq	%rax, %rdx
	movq	%r12, %rsi
	cmpq	%rdi, %r15
	je	.L2314
	leaq	-24(%rsi), %r13
	testq	%rdi, %rdi
	je	.L2316
.L2339:
	movq	%r13, %rsi
	movq	%rax, -128(%rbp)
	movq	%rdx, -120(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-128(%rbp), %rax
	movq	-120(%rbp), %rdx
.L2316:
	movq	%r15, (%rdx)
	testq	%r15, %r15
	je	.L2317
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, -120(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movq	-120(%rbp), %rax
.L2317:
	movzbl	23(%r12), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L2337
.L2314:
	movq	8(%rax), %rdi
	cmpq	%rdi, %r14
	je	.L2320
	leaq	8(%rax), %r15
	movq	%r12, %rsi
.L2319:
	leaq	-48(%rsi), %r13
	testq	%rdi, %rdi
	je	.L2321
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L2321:
	movq	%r14, (%r15)
	testq	%r14, %r14
	je	.L2320
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L2320:
	movq	16(%rbx), %rax
	leaq	-112(%rbp), %r13
	movq	%r13, %rdi
	movq	376(%rax), %r14
	call	_ZN2v88internal8compiler13AccessBuilder22ForExternalTaggedValueEv@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2338
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2337:
	.cfi_restore_state
	movq	32(%r12), %rsi
	leaq	16(%rsi), %rdx
.L2315:
	movq	8(%rdx), %rdi
	cmpq	%rdi, %r14
	je	.L2320
	leaq	8(%rdx), %r15
	jmp	.L2319
	.p2align 4,,10
	.p2align 3
.L2312:
	movq	32(%r12), %rsi
	movq	16(%rsi), %rdi
	leaq	16(%rsi), %rdx
	cmpq	%rdi, %r15
	je	.L2315
	leaq	-24(%rsi), %r13
	testq	%rdi, %rdi
	jne	.L2339
	jmp	.L2316
.L2338:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23844:
	.size	_ZN2v88internal8compiler15JSTypedLowering20ReduceJSStoreMessageEPNS1_4NodeE, .-_ZN2v88internal8compiler15JSTypedLowering20ReduceJSStoreMessageEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler15JSTypedLowering22ReduceJSGeneratorStoreEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15JSTypedLowering22ReduceJSGeneratorStoreEPNS1_4NodeE
	.type	_ZN2v88internal8compiler15JSTypedLowering22ReduceJSGeneratorStoreEPNS1_4NodeE, @function
_ZN2v88internal8compiler15JSTypedLowering22ReduceJSGeneratorStoreEPNS1_4NodeE:
.LFB23845:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-416(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	xorl	%esi, %esi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	subq	$520, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	movq	%rax, -512(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movl	$2, %esi
	movq	%r13, %rdi
	movq	%rax, -520(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%r13, %rdi
	movq	%rax, -528(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetContextInputEPNS1_4NodeE@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%rax, -536(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%rax, -424(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	0(%r13), %rdi
	movq	%rax, -432(%rbp)
	call	_ZN2v88internal8compiler26GeneratorStoreValueCountOfEPKNS1_8OperatorE@PLT
	movq	%r15, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler13AccessBuilder42ForJSGeneratorObjectParametersAndRegistersEv@PLT
	leaq	-352(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -544(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder27ForJSGeneratorObjectContextEv@PLT
	leaq	-288(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -552(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder32ForJSGeneratorObjectContinuationEv@PLT
	leaq	-224(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -560(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder35ForJSGeneratorObjectInputOrDebugPosEv@PLT
	movq	16(%r12), %rax
	movq	%r15, %rsi
	leaq	-96(%rbp), %r15
	movq	376(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9LoadFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$3, %edx
	movq	-512(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r14, %rdi
	movq	-432(%rbp), %rax
	movhps	-424(%rbp), %xmm0
	movq	%rax, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -504(%rbp)
	movq	%rax, %xmm5
	testl	%ebx, %ebx
	jle	.L2345
	movq	%xmm5, -424(%rbp)
	leal	-1(%rbx), %eax
	xorl	%r14d, %r14d
	movq	%rax, -464(%rbp)
	leaq	-160(%rbp), %rax
	movq	%rax, -448(%rbp)
	jmp	.L2343
	.p2align 4,,10
	.p2align 3
.L2346:
	movq	%rax, %r14
.L2343:
	movq	%r13, %rdi
	leal	3(%r14), %esi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	16(%r12), %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler7JSGraph20OptimizedOutConstantEv@PLT
	cmpq	%rax, %rbx
	je	.L2342
	movq	16(%r12), %rax
	movq	%rbx, %xmm3
	movl	$5, %edx
	movq	%r14, %rsi
	movq	-424(%rbp), %xmm0
	movq	-448(%rbp), %rdi
	movq	-504(%rbp), %xmm1
	movq	376(%rax), %r8
	movhps	-432(%rbp), %xmm0
	movq	(%rax), %rbx
	punpcklqdq	%xmm3, %xmm1
	movaps	%xmm0, -480(%rbp)
	movaps	%xmm1, -496(%rbp)
	movq	%r8, -424(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder17ForFixedArraySlotEmNS1_16WriteBarrierKindE@PLT
	movq	-424(%rbp), %r8
	movq	-448(%rbp), %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$4, %edx
	movdqa	-496(%rbp), %xmm1
	movdqa	-480(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%rbx, %rdi
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -424(%rbp)
.L2342:
	leaq	1(%r14), %rax
	cmpq	%r14, -464(%rbp)
	jne	.L2346
.L2341:
	movq	-512(%rbp), %xmm4
	movq	16(%r12), %rax
	movq	-544(%rbp), %rsi
	movdqa	%xmm4, %xmm1
	movdqa	%xmm4, %xmm2
	movdqa	%xmm4, %xmm0
	movq	(%rax), %r14
	movq	376(%rax), %rdi
	movhps	-528(%rbp), %xmm1
	movhps	-520(%rbp), %xmm2
	movhps	-536(%rbp), %xmm0
	movaps	%xmm1, -480(%rbp)
	movaps	%xmm2, -464(%rbp)
	movaps	%xmm0, -448(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$4, %edx
	movdqa	-448(%rbp), %xmm0
	movq	%r14, %rdi
	movq	%rax, %rsi
	movaps	%xmm0, -96(%rbp)
	movq	-424(%rbp), %xmm0
	movhps	-432(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-552(%rbp), %rsi
	movq	%rax, -424(%rbp)
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$4, %edx
	movq	-424(%rbp), %xmm0
	movdqa	-464(%rbp), %xmm2
	movq	%r14, %rdi
	movq	%rax, %rsi
	movhps	-432(%rbp), %xmm0
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-560(%rbp), %rsi
	movq	%rax, -424(%rbp)
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movq	%r15, %rcx
	movdqa	-480(%rbp), %xmm1
	movq	%rax, %rsi
	movl	$4, %edx
	movq	-424(%rbp), %xmm0
	movhps	-432(%rbp), %xmm0
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%r12), %rdi
	movq	-432(%rbp), %r8
	movq	%r13, %rsi
	movq	%rax, %r14
	movq	(%rdi), %rax
	movq	%r14, %rcx
	movq	%r14, %rdx
	call	*32(%rax)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2348
	addq	$520, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2345:
	.cfi_restore_state
	movq	%rax, -424(%rbp)
	jmp	.L2341
.L2348:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23845:
	.size	_ZN2v88internal8compiler15JSTypedLowering22ReduceJSGeneratorStoreEPNS1_4NodeE, .-_ZN2v88internal8compiler15JSTypedLowering22ReduceJSGeneratorStoreEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler15JSTypedLowering36ReduceJSGeneratorRestoreContinuationEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15JSTypedLowering36ReduceJSGeneratorRestoreContinuationEPNS1_4NodeE
	.type	_ZN2v88internal8compiler15JSTypedLowering36ReduceJSGeneratorRestoreContinuationEPNS1_4NodeE, @function
_ZN2v88internal8compiler15JSTypedLowering36ReduceJSGeneratorRestoreContinuationEPNS1_4NodeE:
.LFB23846:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-160(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$152, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -176(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13AccessBuilder32ForJSGeneratorObjectContinuationEv@PLT
	movq	16(%rbx), %rax
	movq	%r15, %rsi
	movq	376(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9LoadFieldERKNS1_11FieldAccessE@PLT
	leaq	-96(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movq	-168(%rbp), %xmm0
	movl	$3, %edx
	movq	%rax, %rsi
	movq	%rcx, -192(%rbp)
	movq	%r13, -80(%rbp)
	movhps	-176(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movsd	.LC23(%rip), %xmm0
	movq	16(%rbx), %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%r15, %rsi
	movq	%rax, -176(%rbp)
	movq	16(%rbx), %rax
	movq	(%rax), %r9
	movq	376(%rax), %rdi
	movq	%r9, -184(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	movq	-184(%rbp), %r9
	movq	%r13, %xmm1
	xorl	%r8d, %r8d
	movq	-192(%rbp), %rcx
	movq	%rax, %rsi
	movl	$4, %edx
	movq	-168(%rbp), %xmm0
	movq	%r9, %rdi
	movhps	-176(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	%r14, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%rbx), %rdi
	movq	%r13, %r8
	movq	%r14, %rdx
	movq	%rax, %rcx
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2352
	addq	$152, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2352:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23846:
	.size	_ZN2v88internal8compiler15JSTypedLowering36ReduceJSGeneratorRestoreContinuationEPNS1_4NodeE, .-_ZN2v88internal8compiler15JSTypedLowering36ReduceJSGeneratorRestoreContinuationEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler15JSTypedLowering31ReduceJSGeneratorRestoreContextEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15JSTypedLowering31ReduceJSGeneratorRestoreContextEPNS1_4NodeE
	.type	_ZN2v88internal8compiler15JSTypedLowering31ReduceJSGeneratorRestoreContextEPNS1_4NodeE, @function
_ZN2v88internal8compiler15JSTypedLowering31ReduceJSGeneratorRestoreContextEPNS1_4NodeE:
.LFB23847:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-96(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	%r13, %rdi
	movq	376(%rax), %r14
	call	_ZN2v88internal8compiler13AccessBuilder27ForJSGeneratorObjectContextEv@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9LoadFieldERKNS1_11FieldAccessE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14NodeProperties14PastValueIndexEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler4Node11RemoveInputEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2356
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2356:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23847:
	.size	_ZN2v88internal8compiler15JSTypedLowering31ReduceJSGeneratorRestoreContextEPNS1_4NodeE, .-_ZN2v88internal8compiler15JSTypedLowering31ReduceJSGeneratorRestoreContextEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler15JSTypedLowering32ReduceJSGeneratorRestoreRegisterEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15JSTypedLowering32ReduceJSGeneratorRestoreRegisterEPNS1_4NodeE
	.type	_ZN2v88internal8compiler15JSTypedLowering32ReduceJSGeneratorRestoreRegisterEPNS1_4NodeE, @function
_ZN2v88internal8compiler15JSTypedLowering32ReduceJSGeneratorRestoreRegisterEPNS1_4NodeE:
.LFB23848:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-224(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-160(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$216, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -248(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	(%r12), %rdi
	movq	%rax, -232(%rbp)
	call	_ZN2v88internal8compiler22RestoreRegisterIndexOfEPKNS1_8OperatorE@PLT
	movq	%r15, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler13AccessBuilder42ForJSGeneratorObjectParametersAndRegistersEv@PLT
	movslq	%r14d, %rsi
	movl	$5, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder17ForFixedArraySlotEmNS1_16WriteBarrierKindE@PLT
	movq	16(%rbx), %rax
	movq	%r15, %rsi
	leaq	-96(%rbp), %r15
	movq	376(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9LoadFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$3, %edx
	movq	-240(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r14, %rdi
	movq	-232(%rbp), %rax
	movhps	-248(%rbp), %xmm0
	movq	%rax, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r13, %rsi
	movq	%rax, -240(%rbp)
	movq	16(%rbx), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9LoadFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$3, %edx
	movq	-240(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r14, %rdi
	movq	-232(%rbp), %rax
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%rbx), %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler7JSGraph21StaleRegisterConstantEv@PLT
	movq	%r13, %rsi
	movq	%rax, -248(%rbp)
	movq	16(%rbx), %rax
	movq	(%rax), %r9
	movq	376(%rax), %rdi
	movq	%r9, -256(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$4, %edx
	movq	-240(%rbp), %xmm0
	movq	-232(%rbp), %r13
	movq	%rax, %rsi
	movq	-256(%rbp), %r9
	movhps	-248(%rbp), %xmm0
	movq	%r13, %xmm1
	movaps	%xmm0, -96(%rbp)
	movq	%r14, %xmm0
	movq	%r9, %rdi
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%rbx), %rdi
	movq	%r13, %r8
	movq	%r14, %rdx
	movq	%rax, %rcx
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2360
	addq	$216, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2360:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23848:
	.size	_ZN2v88internal8compiler15JSTypedLowering32ReduceJSGeneratorRestoreRegisterEPNS1_4NodeE, .-_ZN2v88internal8compiler15JSTypedLowering32ReduceJSGeneratorRestoreRegisterEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler15JSTypedLowering39ReduceJSGeneratorRestoreInputOrDebugPosEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15JSTypedLowering39ReduceJSGeneratorRestoreInputOrDebugPosEPNS1_4NodeE
	.type	_ZN2v88internal8compiler15JSTypedLowering39ReduceJSGeneratorRestoreInputOrDebugPosEPNS1_4NodeE, @function
_ZN2v88internal8compiler15JSTypedLowering39ReduceJSGeneratorRestoreInputOrDebugPosEPNS1_4NodeE:
.LFB23849:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-96(%rbp), %r14
	movq	%rdi, %r13
	pushq	%r12
	movq	%r14, %rdi
	.cfi_offset 12, -40
	movq	%rsi, %r12
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13AccessBuilder35ForJSGeneratorObjectInputOrDebugPosEv@PLT
	movq	16(%r13), %rax
	movq	%r14, %rsi
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9LoadFieldERKNS1_11FieldAccessE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14NodeProperties14PastValueIndexEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler4Node11RemoveInputEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2364
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2364:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23849:
	.size	_ZN2v88internal8compiler15JSTypedLowering39ReduceJSGeneratorRestoreInputOrDebugPosEPNS1_4NodeE, .-_ZN2v88internal8compiler15JSTypedLowering39ReduceJSGeneratorRestoreInputOrDebugPosEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler15JSTypedLowering19ReduceObjectIsArrayEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15JSTypedLowering19ReduceObjectIsArrayEPNS1_4NodeE
	.type	_ZN2v88internal8compiler15JSTypedLowering19ReduceObjectIsArrayEPNS1_4NodeE, @function
_ZN2v88internal8compiler15JSTypedLowering19ReduceObjectIsArrayEPNS1_4NodeE:
.LFB23850:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$328, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	movq	8(%rax), %rax
	movq	%rax, -296(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetContextInputEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, -312(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties18GetFrameStateInputEPNS1_4NodeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	cmpq	$67108865, -296(%rbp)
	movq	%rax, -320(%rbp)
	jne	.L2377
.L2366:
	movq	16(%rbx), %rdi
	call	_ZN2v88internal8compiler7JSGraph12TrueConstantEv@PLT
.L2376:
	movq	8(%rbx), %rdi
	movq	%rax, %r13
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r13, %rax
.L2369:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L2378
	addq	$328, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2377:
	.cfi_restore_state
	leaq	-296(%rbp), %rdi
	movl	$67108865, %esi
	movq	%rdi, -328(%rbp)
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L2366
	movq	-328(%rbp), %rdi
	movl	$68681729, %esi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	je	.L2379
	movq	16(%rbx), %rax
	movq	%r15, %xmm3
	movq	%r13, %xmm1
	movq	%r13, %xmm2
	movhps	-312(%rbp), %xmm1
	punpcklqdq	%xmm3, %xmm2
	movq	(%rax), %r9
	movq	376(%rax), %rdi
	movaps	%xmm1, -368(%rbp)
	movaps	%xmm2, -352(%rbp)
	movq	%r9, -312(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder11ObjectIsSmiEv@PLT
	movq	-312(%rbp), %r9
	movq	%r13, -96(%rbp)
	leaq	-96(%rbp), %r13
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$1, %edx
	movl	$2, %esi
	movq	%rax, -312(%rbp)
	movq	16(%rbx), %rax
	movq	(%rax), %r9
	movq	8(%rax), %rdi
	movq	%r9, -328(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$2, %edx
	movq	-328(%rbp), %r9
	movq	%rax, %rsi
	movq	-312(%rbp), %xmm0
	movq	%r9, %rdi
	movhps	-320(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -312(%rbp)
	movq	16(%rbx), %rax
	movq	(%rax), %r9
	movq	8(%rax), %rdi
	movq	%r9, -320(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$1, %edx
	movq	-320(%rbp), %r9
	movq	%rax, %rsi
	movq	-312(%rbp), %rax
	movq	%r9, %rdi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%rbx), %rdi
	movq	%r15, -144(%rbp)
	movq	%rax, -224(%rbp)
	call	_ZN2v88internal8compiler7JSGraph13FalseConstantEv@PLT
	movq	%rax, -192(%rbp)
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	movq	%r13, %rcx
	movl	$1, %edx
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movq	-312(%rbp), %rax
	movq	%r15, %rdi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	leaq	-288(%rbp), %r9
	movq	%rax, %r15
	movq	16(%rbx), %rax
	movq	%r9, %rdi
	movq	%r9, -312(%rbp)
	movq	(%rax), %r10
	movq	376(%rax), %r8
	movq	%r10, -336(%rbp)
	movq	%r8, -328(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	-312(%rbp), %r9
	movq	-328(%rbp), %r8
	movq	%r9, %rsi
	movq	%r8, %rdi
	movq	%r9, -320(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9LoadFieldERKNS1_11FieldAccessE@PLT
	movq	%r13, %rcx
	movl	$3, %edx
	xorl	%r8d, %r8d
	movq	-336(%rbp), %r10
	movdqa	-352(%rbp), %xmm2
	movq	%rax, %rsi
	movq	%r15, -80(%rbp)
	movq	%r10, %rdi
	movaps	%xmm2, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-320(%rbp), %r9
	movq	%rax, -312(%rbp)
	movq	16(%rbx), %rax
	movq	%r9, %rdi
	movq	(%rax), %r10
	movq	376(%rax), %r8
	movq	%r10, -352(%rbp)
	movq	%r8, -328(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder18ForMapInstanceTypeEv@PLT
	movq	-320(%rbp), %r9
	movq	-328(%rbp), %r8
	movq	%r9, %rsi
	movq	%r8, %rdi
	movq	%r9, -336(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9LoadFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$3, %edx
	movq	-352(%rbp), %r10
	movq	%rax, %rsi
	movq	%r15, -80(%rbp)
	movq	-312(%rbp), %xmm0
	movq	%r10, %rdi
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%rbx), %rdi
	movsd	.LC24(%rip), %xmm0
	movq	%rax, -312(%rbp)
	movq	(%rdi), %r10
	movq	%r10, -328(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%rax, -320(%rbp)
	movq	16(%rbx), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder11NumberEqualEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$2, %edx
	movq	-328(%rbp), %r10
	movq	%rax, %rsi
	movq	-312(%rbp), %xmm0
	movq	%r10, %rdi
	movhps	-320(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%rax, -320(%rbp)
	movq	16(%rbx), %rax
	movq	(%rax), %r10
	movq	8(%rax), %rdi
	movq	%r10, -328(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	movq	%r15, %xmm4
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	-328(%rbp), %r10
	movq	%rax, %rsi
	movl	$2, %edx
	movq	-320(%rbp), %xmm0
	movq	%r10, %rdi
	punpcklqdq	%xmm4, %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r15
	movq	16(%rbx), %rax
	movq	(%rax), %r10
	movq	8(%rax), %rdi
	movq	%r10, -320(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$1, %edx
	movq	-320(%rbp), %r10
	movq	%rax, %rsi
	movq	%r15, -96(%rbp)
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%rbx), %rdi
	movq	%rax, -216(%rbp)
	movq	-312(%rbp), %rax
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal8compiler7JSGraph12TrueConstantEv@PLT
	movq	%rax, -184(%rbp)
	movq	16(%rbx), %rax
	movq	(%rax), %r10
	movq	8(%rax), %rdi
	movq	%r10, -320(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$1, %edx
	movq	-320(%rbp), %r10
	movq	%rax, %rsi
	movq	%r15, -96(%rbp)
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%rbx), %rdi
	movsd	.LC14(%rip), %xmm0
	movq	%rax, -328(%rbp)
	movq	(%rdi), %r15
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%rax, -320(%rbp)
	movq	16(%rbx), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder11NumberEqualEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	%r15, %rdi
	movq	-312(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movhps	-320(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$1, %edx
	movl	$2, %esi
	movq	%rax, -320(%rbp)
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$2, %edx
	movq	-320(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movhps	-328(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r15
	movq	16(%rbx), %rax
	movq	(%rax), %r10
	movq	8(%rax), %rdi
	movq	%r10, -320(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$1, %edx
	movq	-320(%rbp), %r10
	movq	%rax, %rsi
	movq	%r15, -96(%rbp)
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%rbx), %rdi
	movq	%rax, -208(%rbp)
	movq	-312(%rbp), %rax
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal8compiler7JSGraph13FalseConstantEv@PLT
	movq	%rax, -176(%rbp)
	movq	16(%rbx), %rax
	movq	(%rax), %r10
	movq	8(%rax), %rdi
	movq	%r10, -320(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$1, %edx
	movq	-320(%rbp), %r10
	movq	%rax, %rsi
	movq	%r15, -96(%rbp)
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$4, %esi
	movq	%rax, -320(%rbp)
	movq	16(%rbx), %rax
	movq	368(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler17JSOperatorBuilder11CallRuntimeENS0_7Runtime10FunctionIdE@PLT
	movq	%r14, %xmm0
	movq	%r15, %rdi
	xorl	%r8d, %r8d
	movq	-320(%rbp), %rdx
	movhps	-312(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r13, %rcx
	movdqa	-368(%rbp), %xmm1
	movaps	%xmm0, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movl	$5, %edx
	movaps	%xmm1, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-336(%rbp), %r9
	movq	%r12, %rdi
	movq	$513, 8(%rax)
	movq	%rax, %r14
	movq	%r9, %rsi
	movq	%r14, %r15
	movq	$0, -288(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties17IsExceptionalCallEPNS1_4NodeEPS4_@PLT
	testb	%al, %al
	jne	.L2380
.L2370:
	movq	16(%rbx), %rax
	movq	%r14, -120(%rbp)
	movl	$4, %esi
	movq	%r15, -200(%rbp)
	movq	%r14, -168(%rbp)
	movq	8(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
	leaq	-224(%rbp), %rcx
	xorl	%r8d, %r8d
	movl	$4, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$4, %esi
	movq	%rax, -112(%rbp)
	movq	%rax, %r14
	movq	%rax, -160(%rbp)
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9EffectPhiEi@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	movl	$5, %edx
	leaq	-144(%rbp), %rcx
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$4, %edx
	movl	$8, %esi
	movq	%rax, %r15
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	movq	%r13, %rdi
	xorl	%r8d, %r8d
	movl	$5, %edx
	movq	%rax, %rsi
	leaq	-192(%rbp), %rcx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%rbx), %rdi
	movq	%r14, %r8
	movq	%r15, %rcx
	movq	%rax, %r13
	movq	%r12, %rsi
	movq	(%rdi), %rax
	movq	%r13, %rdx
	call	*32(%rax)
	movq	%r13, %rax
	jmp	.L2369
	.p2align 4,,10
	.p2align 3
.L2379:
	movq	16(%rbx), %rdi
	call	_ZN2v88internal8compiler7JSGraph13FalseConstantEv@PLT
	jmp	.L2376
	.p2align 4,,10
	.p2align 3
.L2380:
	movq	-288(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler14NodeProperties19ReplaceControlInputEPNS1_4NodeES4_i@PLT
	movq	-288(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler14NodeProperties18ReplaceEffectInputEPNS1_4NodeES4_i@PLT
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9IfSuccessEv@PLT
	movq	%r15, %rdi
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r14, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%rbx), %rdi
	movq	-288(%rbp), %rsi
	movq	%rax, %r15
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L2370
.L2378:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23850:
	.size	_ZN2v88internal8compiler15JSTypedLowering19ReduceObjectIsArrayEPNS1_4NodeE, .-_ZN2v88internal8compiler15JSTypedLowering19ReduceObjectIsArrayEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler15JSTypedLowering16ReduceJSParseIntEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15JSTypedLowering16ReduceJSParseIntEPNS1_4NodeE
	.type	_ZN2v88internal8compiler15JSTypedLowering16ReduceJSParseIntEPNS1_4NodeE, @function
_ZN2v88internal8compiler15JSTypedLowering16ReduceJSParseIntEPNS1_4NodeE:
.LFB23851:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	movq	%r12, %rdi
	subq	$32, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	movq	8(%rax), %rax
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	48(%r13), %rdx
	movq	8(%rax), %rax
	movq	384(%rdx), %rsi
	movq	%rax, -48(%rbp)
	cmpq	%rsi, -56(%rbp)
	jne	.L2392
.L2382:
	movq	256(%rdx), %rsi
	cmpq	%rax, %rsi
	jne	.L2384
.L2386:
	movq	8(%r13), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%rbx, %rax
.L2385:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L2393
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2384:
	.cfi_restore_state
	leaq	-48(%rbp), %r14
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L2386
	movq	48(%r13), %rax
	movq	248(%rax), %rsi
	cmpq	-48(%rbp), %rsi
	je	.L2386
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L2386
	.p2align 4,,10
	.p2align 3
.L2383:
	xorl	%eax, %eax
	jmp	.L2385
	.p2align 4,,10
	.p2align 3
.L2392:
	leaq	-56(%rbp), %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L2383
	movq	48(%r13), %rdx
	movq	-48(%rbp), %rax
	jmp	.L2382
.L2393:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23851:
	.size	_ZN2v88internal8compiler15JSTypedLowering16ReduceJSParseIntEPNS1_4NodeE, .-_ZN2v88internal8compiler15JSTypedLowering16ReduceJSParseIntEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler15JSTypedLowering6ReduceEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15JSTypedLowering6ReduceEPNS1_4NodeE
	.type	_ZN2v88internal8compiler15JSTypedLowering6ReduceEPNS1_4NodeE, @function
_ZN2v88internal8compiler15JSTypedLowering6ReduceEPNS1_4NodeE:
.LFB23853:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$64, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movzwl	16(%rax), %eax
	subw	$682, %ax
	cmpw	$104, %ax
	ja	.L2395
	leaq	.L2397(%rip), %rdx
	movzwl	%ax, %eax
	movq	%rdi, %r13
	movq	%rsi, %r12
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler15JSTypedLowering6ReduceEPNS1_4NodeE,"a",@progbits
	.align 4
	.align 4
.L2397:
	.long	.L2437-.L2397
	.long	.L2436-.L2397
	.long	.L2435-.L2397
	.long	.L2435-.L2397
	.long	.L2435-.L2397
	.long	.L2435-.L2397
	.long	.L2434-.L2397
	.long	.L2434-.L2397
	.long	.L2434-.L2397
	.long	.L2433-.L2397
	.long	.L2433-.L2397
	.long	.L2432-.L2397
	.long	.L2431-.L2397
	.long	.L2430-.L2397
	.long	.L2430-.L2397
	.long	.L2430-.L2397
	.long	.L2430-.L2397
	.long	.L2430-.L2397
	.long	.L2429-.L2397
	.long	.L2395-.L2397
	.long	.L2428-.L2397
	.long	.L2427-.L2397
	.long	.L2426-.L2397
	.long	.L2425-.L2397
	.long	.L2425-.L2397
	.long	.L2424-.L2397
	.long	.L2423-.L2397
	.long	.L2422-.L2397
	.long	.L2421-.L2397
	.long	.L2420-.L2397
	.long	.L2419-.L2397
	.long	.L2418-.L2397
	.long	.L2417-.L2397
	.long	.L2395-.L2397
	.long	.L2395-.L2397
	.long	.L2395-.L2397
	.long	.L2395-.L2397
	.long	.L2395-.L2397
	.long	.L2395-.L2397
	.long	.L2395-.L2397
	.long	.L2395-.L2397
	.long	.L2395-.L2397
	.long	.L2395-.L2397
	.long	.L2395-.L2397
	.long	.L2395-.L2397
	.long	.L2395-.L2397
	.long	.L2395-.L2397
	.long	.L2395-.L2397
	.long	.L2395-.L2397
	.long	.L2395-.L2397
	.long	.L2395-.L2397
	.long	.L2395-.L2397
	.long	.L2395-.L2397
	.long	.L2395-.L2397
	.long	.L2395-.L2397
	.long	.L2395-.L2397
	.long	.L2416-.L2397
	.long	.L2395-.L2397
	.long	.L2395-.L2397
	.long	.L2395-.L2397
	.long	.L2395-.L2397
	.long	.L2395-.L2397
	.long	.L2395-.L2397
	.long	.L2395-.L2397
	.long	.L2395-.L2397
	.long	.L2395-.L2397
	.long	.L2395-.L2397
	.long	.L2415-.L2397
	.long	.L2414-.L2397
	.long	.L2395-.L2397
	.long	.L2395-.L2397
	.long	.L2395-.L2397
	.long	.L2395-.L2397
	.long	.L2413-.L2397
	.long	.L2412-.L2397
	.long	.L2395-.L2397
	.long	.L2395-.L2397
	.long	.L2411-.L2397
	.long	.L2410-.L2397
	.long	.L2395-.L2397
	.long	.L2395-.L2397
	.long	.L2395-.L2397
	.long	.L2395-.L2397
	.long	.L2395-.L2397
	.long	.L2395-.L2397
	.long	.L2395-.L2397
	.long	.L2409-.L2397
	.long	.L2408-.L2397
	.long	.L2395-.L2397
	.long	.L2407-.L2397
	.long	.L2406-.L2397
	.long	.L2405-.L2397
	.long	.L2404-.L2397
	.long	.L2403-.L2397
	.long	.L2402-.L2397
	.long	.L2401-.L2397
	.long	.L2400-.L2397
	.long	.L2399-.L2397
	.long	.L2395-.L2397
	.long	.L2395-.L2397
	.long	.L2395-.L2397
	.long	.L2395-.L2397
	.long	.L2398-.L2397
	.long	.L2395-.L2397
	.long	.L2396-.L2397
	.section	.text._ZN2v88internal8compiler15JSTypedLowering6ReduceEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L2395:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L2438:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L2455
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2430:
	.cfi_restore_state
	call	_ZN2v88internal8compiler15JSTypedLowering17ReduceNumberBinopEPNS1_4NodeE
	jmp	.L2438
	.p2align 4,,10
	.p2align 3
.L2435:
	call	_ZN2v88internal8compiler15JSTypedLowering18ReduceJSComparisonEPNS1_4NodeE
	jmp	.L2438
	.p2align 4,,10
	.p2align 3
.L2434:
	call	_ZN2v88internal8compiler15JSTypedLowering16ReduceInt32BinopEPNS1_4NodeE
	jmp	.L2438
	.p2align 4,,10
	.p2align 3
.L2433:
	xorl	%edx, %edx
	call	_ZN2v88internal8compiler15JSTypedLowering15ReduceUI32ShiftEPNS1_4NodeENS1_10SignednessE
	jmp	.L2438
	.p2align 4,,10
	.p2align 3
.L2425:
	call	_ZN2v88internal8compiler15JSTypedLowering16ReduceJSToNumberEPNS1_4NodeE
	jmp	.L2438
.L2421:
	call	_ZN2v88internal8compiler15JSTypedLowering16ReduceJSParseIntEPNS1_4NodeE
	jmp	.L2438
.L2429:
	call	_ZN2v88internal8compiler15JSTypedLowering27ReduceJSHasInPrototypeChainEPNS1_4NodeE
	jmp	.L2438
.L2396:
	call	_ZN2v88internal8compiler15JSTypedLowering19ReduceObjectIsArrayEPNS1_4NodeE
	jmp	.L2438
.L2436:
	call	_ZN2v88internal8compiler15JSTypedLowering19ReduceJSStrictEqualEPNS1_4NodeE
	jmp	.L2438
.L2398:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	8(%rax), %rax
	movq	%rax, -96(%rbp)
	cmpq	$134250495, %rax
	je	.L2441
	leaq	-96(%rbp), %rdi
	movl	$134250495, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L2441
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L2454:
	movq	%r12, %rax
	jmp	.L2438
.L2399:
	leaq	-96(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder35ForJSGeneratorObjectInputOrDebugPosEv@PLT
	movq	16(%r13), %rax
	movq	%r14, %rsi
	movq	376(%rax), %rdi
.L2453:
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9LoadFieldERKNS1_11FieldAccessE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14NodeProperties14PastValueIndexEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler4Node11RemoveInputEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	jmp	.L2454
.L2406:
	call	_ZN2v88internal8compiler15JSTypedLowering20ReduceJSStoreMessageEPNS1_4NodeE
	jmp	.L2438
.L2407:
	call	_ZN2v88internal8compiler15JSTypedLowering19ReduceJSLoadMessageEPNS1_4NodeE
	jmp	.L2438
.L2408:
	call	_ZN2v88internal8compiler15JSTypedLowering20ReduceJSForInPrepareEPNS1_4NodeE
	jmp	.L2438
.L2409:
	call	_ZN2v88internal8compiler15JSTypedLowering17ReduceJSForInNextEPNS1_4NodeE
	jmp	.L2438
.L2410:
	call	_ZN2v88internal8compiler15JSTypedLowering17ReduceJSConstructEPNS1_4NodeE
	jmp	.L2438
.L2411:
	call	_ZN2v88internal8compiler15JSTypedLowering31ReduceJSConstructForwardVarargsEPNS1_4NodeE
	jmp	.L2438
.L2412:
	call	_ZN2v88internal8compiler15JSTypedLowering26ReduceJSCallForwardVarargsEPNS1_4NodeE
	jmp	.L2438
.L2431:
	call	_ZN2v88internal8compiler15JSTypedLowering11ReduceJSAddEPNS1_4NodeE
	jmp	.L2438
.L2432:
	movl	$1, %edx
	call	_ZN2v88internal8compiler15JSTypedLowering15ReduceUI32ShiftEPNS1_4NodeENS1_10SignednessE
	jmp	.L2438
.L2424:
	call	_ZN2v88internal8compiler15JSTypedLowering17ReduceJSToNumericEPNS1_4NodeE
	jmp	.L2438
.L2422:
	call	_ZN2v88internal8compiler15JSTypedLowering16ReduceJSToStringEPNS1_4NodeE
	jmp	.L2438
.L2423:
	call	_ZN2v88internal8compiler15JSTypedLowering16ReduceJSToObjectEPNS1_4NodeE
	jmp	.L2438
.L2426:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rax, %rbx
	movq	8(%rax), %rax
	movq	%rax, -96(%rbp)
	cmpq	$24609, %rax
	je	.L2439
	leaq	-96(%rbp), %rdi
	movl	$24609, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L2439
	xorl	%ebx, %ebx
.L2440:
	movq	%rbx, %rax
	jmp	.L2438
.L2427:
	call	_ZN2v88internal8compiler15JSTypedLowering16ReduceJSToLengthEPNS1_4NodeE
	jmp	.L2438
.L2428:
	call	_ZN2v88internal8compiler15JSTypedLowering27ReduceJSOrdinaryHasInstanceEPNS1_4NodeE
	jmp	.L2438
.L2413:
	call	_ZN2v88internal8compiler15JSTypedLowering12ReduceJSCallEPNS1_4NodeE
	jmp	.L2438
.L2414:
	call	_ZN2v88internal8compiler15JSTypedLowering20ReduceJSStoreContextEPNS1_4NodeE
	jmp	.L2438
.L2415:
	call	_ZN2v88internal8compiler15JSTypedLowering19ReduceJSLoadContextEPNS1_4NodeE
	jmp	.L2438
.L2416:
	call	_ZN2v88internal8compiler15JSTypedLowering17ReduceJSLoadNamedEPNS1_4NodeE
	jmp	.L2438
.L2417:
	call	_ZN2v88internal8compiler15JSTypedLowering14ReduceJSNegateEPNS1_4NodeE
	jmp	.L2438
.L2418:
	call	_ZN2v88internal8compiler15JSTypedLowering17ReduceJSIncrementEPNS1_4NodeE
	jmp	.L2438
.L2419:
	call	_ZN2v88internal8compiler15JSTypedLowering17ReduceJSDecrementEPNS1_4NodeE
	jmp	.L2438
.L2420:
	call	_ZN2v88internal8compiler15JSTypedLowering18ReduceJSBitwiseNotEPNS1_4NodeE
	jmp	.L2438
.L2437:
	call	_ZN2v88internal8compiler15JSTypedLowering13ReduceJSEqualEPNS1_4NodeE
	jmp	.L2438
.L2402:
	call	_ZN2v88internal8compiler15JSTypedLowering36ReduceJSGeneratorRestoreContinuationEPNS1_4NodeE
	jmp	.L2438
.L2403:
	call	_ZN2v88internal8compiler15JSTypedLowering22ReduceJSGeneratorStoreEPNS1_4NodeE
	jmp	.L2438
.L2404:
	call	_ZN2v88internal8compiler15JSTypedLowering19ReduceJSStoreModuleEPNS1_4NodeE
	jmp	.L2438
.L2405:
	call	_ZN2v88internal8compiler15JSTypedLowering18ReduceJSLoadModuleEPNS1_4NodeE
	jmp	.L2438
.L2400:
	call	_ZN2v88internal8compiler15JSTypedLowering32ReduceJSGeneratorRestoreRegisterEPNS1_4NodeE
	jmp	.L2438
.L2401:
	movq	16(%rdi), %rax
	leaq	-96(%rbp), %r13
	movq	%r13, %rdi
	movq	376(%rax), %r14
	call	_ZN2v88internal8compiler13AccessBuilder27ForJSGeneratorObjectContextEv@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	jmp	.L2453
	.p2align 4,,10
	.p2align 3
.L2439:
	movq	8(%r13), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	jmp	.L2440
	.p2align 4,,10
	.p2align 3
.L2441:
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node11RemoveInputEi@PLT
	movq	16(%r13), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder14FulfillPromiseEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	jmp	.L2454
.L2455:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23853:
	.size	_ZN2v88internal8compiler15JSTypedLowering6ReduceEPNS1_4NodeE, .-_ZN2v88internal8compiler15JSTypedLowering6ReduceEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler15JSTypedLowering22ReduceJSResolvePromiseEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15JSTypedLowering22ReduceJSResolvePromiseEPNS1_4NodeE
	.type	_ZN2v88internal8compiler15JSTypedLowering22ReduceJSResolvePromiseEPNS1_4NodeE, @function
_ZN2v88internal8compiler15JSTypedLowering22ReduceJSResolvePromiseEPNS1_4NodeE:
.LFB23852:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	movl	$1, %esi
	movq	%rbx, %rdi
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	8(%rax), %rax
	movq	%rax, -32(%rbp)
	cmpq	$134250495, %rax
	jne	.L2464
.L2457:
	movl	$3, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler4Node11RemoveInputEi@PLT
	movq	16(%r12), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder14FulfillPromiseEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	%rbx, %rax
.L2458:
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L2465
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2464:
	.cfi_restore_state
	leaq	-32(%rbp), %rdi
	movl	$134250495, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L2457
	xorl	%eax, %eax
	jmp	.L2458
.L2465:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23852:
	.size	_ZN2v88internal8compiler15JSTypedLowering22ReduceJSResolvePromiseEPNS1_4NodeE, .-_ZN2v88internal8compiler15JSTypedLowering22ReduceJSResolvePromiseEPNS1_4NodeE
	.section	.text._ZNK2v88internal8compiler15JSTypedLowering7factoryEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler15JSTypedLowering7factoryEv
	.type	_ZNK2v88internal8compiler15JSTypedLowering7factoryEv, @function
_ZNK2v88internal8compiler15JSTypedLowering7factoryEv:
.LFB23857:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	360(%rax), %rax
	ret
	.cfi_endproc
.LFE23857:
	.size	_ZNK2v88internal8compiler15JSTypedLowering7factoryEv, .-_ZNK2v88internal8compiler15JSTypedLowering7factoryEv
	.section	.text._ZNK2v88internal8compiler15JSTypedLowering5graphEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler15JSTypedLowering5graphEv
	.type	_ZNK2v88internal8compiler15JSTypedLowering5graphEv, @function
_ZNK2v88internal8compiler15JSTypedLowering5graphEv:
.LFB23858:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	(%rax), %rax
	ret
	.cfi_endproc
.LFE23858:
	.size	_ZNK2v88internal8compiler15JSTypedLowering5graphEv, .-_ZNK2v88internal8compiler15JSTypedLowering5graphEv
	.section	.text._ZNK2v88internal8compiler15JSTypedLowering7isolateEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler15JSTypedLowering7isolateEv
	.type	_ZNK2v88internal8compiler15JSTypedLowering7isolateEv, @function
_ZNK2v88internal8compiler15JSTypedLowering7isolateEv:
.LFB23859:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	360(%rax), %rax
	ret
	.cfi_endproc
.LFE23859:
	.size	_ZNK2v88internal8compiler15JSTypedLowering7isolateEv, .-_ZNK2v88internal8compiler15JSTypedLowering7isolateEv
	.section	.text._ZNK2v88internal8compiler15JSTypedLowering10javascriptEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler15JSTypedLowering10javascriptEv
	.type	_ZNK2v88internal8compiler15JSTypedLowering10javascriptEv, @function
_ZNK2v88internal8compiler15JSTypedLowering10javascriptEv:
.LFB23860:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	368(%rax), %rax
	ret
	.cfi_endproc
.LFE23860:
	.size	_ZNK2v88internal8compiler15JSTypedLowering10javascriptEv, .-_ZNK2v88internal8compiler15JSTypedLowering10javascriptEv
	.section	.text._ZNK2v88internal8compiler15JSTypedLowering6commonEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler15JSTypedLowering6commonEv
	.type	_ZNK2v88internal8compiler15JSTypedLowering6commonEv, @function
_ZNK2v88internal8compiler15JSTypedLowering6commonEv:
.LFB23861:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	8(%rax), %rax
	ret
	.cfi_endproc
.LFE23861:
	.size	_ZNK2v88internal8compiler15JSTypedLowering6commonEv, .-_ZNK2v88internal8compiler15JSTypedLowering6commonEv
	.section	.text._ZNK2v88internal8compiler15JSTypedLowering10simplifiedEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler15JSTypedLowering10simplifiedEv
	.type	_ZNK2v88internal8compiler15JSTypedLowering10simplifiedEv, @function
_ZNK2v88internal8compiler15JSTypedLowering10simplifiedEv:
.LFB23862:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	376(%rax), %rax
	ret
	.cfi_endproc
.LFE23862:
	.size	_ZNK2v88internal8compiler15JSTypedLowering10simplifiedEv, .-_ZNK2v88internal8compiler15JSTypedLowering10simplifiedEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler15JSTypedLoweringC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerEPNS0_4ZoneE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler15JSTypedLoweringC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerEPNS0_4ZoneE, @function
_GLOBAL__sub_I__ZN2v88internal8compiler15JSTypedLoweringC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerEPNS0_4ZoneE:
.LFB29182:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE29182:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler15JSTypedLoweringC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerEPNS0_4ZoneE, .-_GLOBAL__sub_I__ZN2v88internal8compiler15JSTypedLoweringC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerEPNS0_4ZoneE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler15JSTypedLoweringC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerEPNS0_4ZoneE
	.weak	_ZTVN2v88internal23CallInterfaceDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal23CallInterfaceDescriptorE,"awG",@progbits,_ZTVN2v88internal23CallInterfaceDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal23CallInterfaceDescriptorE, @object
	.size	_ZTVN2v88internal23CallInterfaceDescriptorE, 48
_ZTVN2v88internal23CallInterfaceDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.weak	_ZTVN2v88internal8compiler15JSTypedLoweringE
	.section	.data.rel.ro._ZTVN2v88internal8compiler15JSTypedLoweringE,"awG",@progbits,_ZTVN2v88internal8compiler15JSTypedLoweringE,comdat
	.align 8
	.type	_ZTVN2v88internal8compiler15JSTypedLoweringE, @object
	.size	_ZTVN2v88internal8compiler15JSTypedLoweringE, 56
_ZTVN2v88internal8compiler15JSTypedLoweringE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal8compiler15JSTypedLoweringD1Ev
	.quad	_ZN2v88internal8compiler15JSTypedLoweringD0Ev
	.quad	_ZNK2v88internal8compiler15JSTypedLowering12reducer_nameEv
	.quad	_ZN2v88internal8compiler15JSTypedLowering6ReduceEPNS1_4NodeE
	.quad	_ZN2v88internal8compiler7Reducer8FinalizeEv
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC8:
	.long	4294967295
	.long	1128267775
	.align 8
.LC10:
	.long	0
	.long	-1074790400
	.align 8
.LC11:
	.long	4093640704
	.long	1104150527
	.align 8
.LC12:
	.long	4085252096
	.long	1104150527
	.align 8
.LC13:
	.long	0
	.long	1083195392
	.align 8
.LC14:
	.long	0
	.long	1083179008
	.align 8
.LC22:
	.long	0
	.long	1083176960
	.align 8
.LC23:
	.long	0
	.long	-1073741824
	.align 8
.LC24:
	.long	0
	.long	1083216896
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
