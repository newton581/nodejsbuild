	.file	"control-equivalence.cc"
	.text
	.section	.rodata._ZN2v88internal8compiler18ControlEquivalence8VisitPreEPNS1_4NodeE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"CEQ: Pre-visit of #%d:%s\n"
	.section	.text._ZN2v88internal8compiler18ControlEquivalence8VisitPreEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18ControlEquivalence8VisitPreEPNS1_4NodeE
	.type	_ZN2v88internal8compiler18ControlEquivalence8VisitPreEPNS1_4NodeE, @function
_ZN2v88internal8compiler18ControlEquivalence8VisitPreEPNS1_4NodeE:
.LFB10222:
	.cfi_startproc
	endbr64
	cmpb	$0, _ZN2v88internal20FLAG_trace_turbo_ceqE(%rip)
	jne	.L4
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movq	(%rsi), %rax
	movl	20(%rsi), %esi
	leaq	.LC0(%rip), %rdi
	movq	8(%rax), %rdx
	andl	$16777215, %esi
	xorl	%eax, %eax
	jmp	_ZN2v88internal6PrintFEPKcz@PLT
	.cfi_endproc
.LFE10222:
	.size	_ZN2v88internal8compiler18ControlEquivalence8VisitPreEPNS1_4NodeE, .-_ZN2v88internal8compiler18ControlEquivalence8VisitPreEPNS1_4NodeE
	.section	.rodata._ZN2v88internal8compiler18ControlEquivalence17BracketListDeleteERNS0_14ZoneLinkedListINS2_7BracketEEEPNS1_4NodeENS2_12DFSDirectionE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"  BList erased: {%d->%d}\n"
	.section	.text._ZN2v88internal8compiler18ControlEquivalence17BracketListDeleteERNS0_14ZoneLinkedListINS2_7BracketEEEPNS1_4NodeENS2_12DFSDirectionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18ControlEquivalence17BracketListDeleteERNS0_14ZoneLinkedListINS2_7BracketEEEPNS1_4NodeENS2_12DFSDirectionE
	.type	_ZN2v88internal8compiler18ControlEquivalence17BracketListDeleteERNS0_14ZoneLinkedListINS2_7BracketEEEPNS1_4NodeENS2_12DFSDirectionE, @function
_ZN2v88internal8compiler18ControlEquivalence17BracketListDeleteERNS0_14ZoneLinkedListINS2_7BracketEEEPNS1_4NodeENS2_12DFSDirectionE:
.LFB10249:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	8(%rsi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$24, %rsp
	movq	8(%rsi), %r12
	cmpq	%r15, %r12
	jne	.L6
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L8:
	movq	(%r12), %r12
	cmpq	%r15, %r12
	je	.L5
.L6:
	cmpq	%rbx, 48(%r12)
	jne	.L8
	cmpl	%r13d, 16(%r12)
	je	.L8
	cmpb	$0, _ZN2v88internal20FLAG_trace_turbo_ceqE(%rip)
	jne	.L15
.L10:
	movq	(%r12), %rax
	subq	$1, 24(%r14)
	movq	%r12, %rdi
	movq	%rax, -56(%rbp)
	call	_ZNSt8__detail15_List_node_base9_M_unhookEv@PLT
	movq	-56(%rbp), %r12
	cmpq	%r15, %r12
	jne	.L6
.L5:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	.cfi_restore_state
	movq	40(%r12), %rax
	movl	20(%rbx), %edx
	leaq	.LC1(%rip), %rdi
	movl	20(%rax), %esi
	andl	$16777215, %edx
	xorl	%eax, %eax
	andl	$16777215, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L10
	.cfi_endproc
.LFE10249:
	.size	_ZN2v88internal8compiler18ControlEquivalence17BracketListDeleteERNS0_14ZoneLinkedListINS2_7BracketEEEPNS1_4NodeENS2_12DFSDirectionE, .-_ZN2v88internal8compiler18ControlEquivalence17BracketListDeleteERNS0_14ZoneLinkedListINS2_7BracketEEEPNS1_4NodeENS2_12DFSDirectionE
	.section	.rodata._ZN2v88internal8compiler18ControlEquivalence16BracketListTRACEERNS0_14ZoneLinkedListINS2_7BracketEEE.str1.1,"aMS",@progbits,1
.LC2:
	.string	"  BList: "
.LC3:
	.string	"{%d->%d} "
.LC4:
	.string	"\n"
	.section	.text._ZN2v88internal8compiler18ControlEquivalence16BracketListTRACEERNS0_14ZoneLinkedListINS2_7BracketEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18ControlEquivalence16BracketListTRACEERNS0_14ZoneLinkedListINS2_7BracketEEE
	.type	_ZN2v88internal8compiler18ControlEquivalence16BracketListTRACEERNS0_14ZoneLinkedListINS2_7BracketEEE, @function
_ZN2v88internal8compiler18ControlEquivalence16BracketListTRACEERNS0_14ZoneLinkedListINS2_7BracketEEE:
.LFB10250:
	.cfi_startproc
	endbr64
	cmpb	$0, _ZN2v88internal20FLAG_trace_turbo_ceqE(%rip)
	jne	.L35
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	leaq	.LC2(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	addq	$8, %r12
	subq	$8, %rsp
	.cfi_offset 3, -40
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	(%r12), %rbx
	movzbl	_ZN2v88internal20FLAG_trace_turbo_ceqE(%rip), %eax
	cmpq	%rbx, %r12
	je	.L22
	leaq	.LC3(%rip), %r13
.L21:
	testb	%al, %al
	je	.L20
	movq	48(%rbx), %rax
	movq	%r13, %rdi
	movl	20(%rax), %edx
	movq	40(%rbx), %rax
	movl	20(%rax), %esi
	andl	$16777215, %edx
	xorl	%eax, %eax
	andl	$16777215, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	(%rbx), %rbx
	movzbl	_ZN2v88internal20FLAG_trace_turbo_ceqE(%rip), %eax
	cmpq	%r12, %rbx
	jne	.L21
.L22:
	testb	%al, %al
	je	.L16
	addq	$8, %rsp
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal6PrintFEPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	movq	(%rbx), %rbx
	cmpq	%rbx, %r12
	jne	.L20
.L16:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10250:
	.size	_ZN2v88internal8compiler18ControlEquivalence16BracketListTRACEERNS0_14ZoneLinkedListINS2_7BracketEEE, .-_ZN2v88internal8compiler18ControlEquivalence16BracketListTRACEERNS0_14ZoneLinkedListINS2_7BracketEEE
	.section	.rodata._ZNSt6vectorIPN2v88internal8compiler18ControlEquivalence8NodeDataENS1_13ZoneAllocatorIS5_EEE17_M_default_appendEm.str1.1,"aMS",@progbits,1
.LC5:
	.string	"vector::_M_default_append"
	.section	.text._ZNSt6vectorIPN2v88internal8compiler18ControlEquivalence8NodeDataENS1_13ZoneAllocatorIS5_EEE17_M_default_appendEm,"axG",@progbits,_ZNSt6vectorIPN2v88internal8compiler18ControlEquivalence8NodeDataENS1_13ZoneAllocatorIS5_EEE17_M_default_appendEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal8compiler18ControlEquivalence8NodeDataENS1_13ZoneAllocatorIS5_EEE17_M_default_appendEm
	.type	_ZNSt6vectorIPN2v88internal8compiler18ControlEquivalence8NodeDataENS1_13ZoneAllocatorIS5_EEE17_M_default_appendEm, @function
_ZNSt6vectorIPN2v88internal8compiler18ControlEquivalence8NodeDataENS1_13ZoneAllocatorIS5_EEE17_M_default_appendEm:
.LFB11424:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movl	$268435455, %esi
	subq	$24, %rsp
	movq	16(%rdi), %rcx
	movq	24(%r13), %rax
	movq	%rcx, %rdx
	subq	8(%rdi), %rdx
	subq	%rcx, %rax
	movq	%rsi, %rdi
	movq	%rdx, %r14
	sarq	$3, %rax
	sarq	$3, %r14
	subq	%r14, %rdi
	cmpq	%rbx, %rax
	jb	.L38
	salq	$3, %rbx
	movq	%rcx, %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	call	memset@PLT
	movq	%rax, %rcx
	addq	%rbx, %rcx
	movq	%rcx, 16(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L64:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	cmpq	%rbx, %rdi
	jb	.L67
	cmpq	%r14, %rbx
	movq	%r14, %r15
	movq	0(%r13), %rdi
	cmovnb	%rbx, %r15
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	addq	%r14, %r15
	cmpq	$268435455, %r15
	cmova	%rsi, %r15
	subq	%r12, %rax
	salq	$3, %r15
	movq	%r15, %rsi
	cmpq	%r15, %rax
	jb	.L68
	addq	%r12, %rsi
	movq	%rsi, 16(%rdi)
.L42:
	leaq	0(,%rbx,8), %r8
	leaq	(%r12,%rdx), %rdi
	xorl	%esi, %esi
	movq	%r8, %rdx
	call	memset@PLT
	movq	16(%r13), %rax
	movq	8(%r13), %rdx
	cmpq	%rdx, %rax
	je	.L47
	subq	$8, %rax
	leaq	15(%rdx), %rcx
	subq	%rdx, %rax
	subq	%r12, %rcx
	movq	%rax, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rcx
	jbe	.L44
	movabsq	$2305843009213693948, %rcx
	testq	%rcx, %rsi
	je	.L44
	addq	$1, %rsi
	movq	%rdx, %rdi
	movq	%r12, %rax
	movq	%rsi, %rcx
	subq	%r12, %rdi
	shrq	%rcx
	salq	$4, %rcx
	addq	%r12, %rcx
	.p2align 4,,10
	.p2align 3
.L45:
	movdqu	(%rax,%rdi), %xmm0
	addq	$16, %rax
	movups	%xmm0, -16(%rax)
	cmpq	%rax, %rcx
	jne	.L45
	movq	%rsi, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rcx
	addq	%rcx, %rdx
	addq	%r12, %rcx
	cmpq	%rax, %rsi
	je	.L47
	movq	(%rdx), %rax
	movq	%rax, (%rcx)
.L47:
	addq	%r14, %rbx
	movq	%r12, 8(%r13)
	leaq	(%r12,%rbx,8), %rax
	addq	%r15, %r12
	movq	%rax, 16(%r13)
	movq	%r12, 24(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L44:
	.cfi_restore_state
	leaq	8(%rax,%r12), %rsi
	subq	%r12, %rdx
	movq	%r12, %rax
	.p2align 4,,10
	.p2align 3
.L49:
	movq	(%rax,%rdx), %rcx
	addq	$8, %rax
	movq	%rcx, -8(%rax)
	cmpq	%rax, %rsi
	jne	.L49
	jmp	.L47
.L68:
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %r12
	jmp	.L42
.L67:
	leaq	.LC5(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE11424:
	.size	_ZNSt6vectorIPN2v88internal8compiler18ControlEquivalence8NodeDataENS1_13ZoneAllocatorIS5_EEE17_M_default_appendEm, .-_ZNSt6vectorIPN2v88internal8compiler18ControlEquivalence8NodeDataENS1_13ZoneAllocatorIS5_EEE17_M_default_appendEm
	.section	.text._ZN2v88internal8compiler18ControlEquivalence6DFSPopERNS0_9ZoneStackINS2_13DFSStackEntryEEEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18ControlEquivalence6DFSPopERNS0_9ZoneStackINS2_13DFSStackEntryEEEPNS1_4NodeE
	.type	_ZN2v88internal8compiler18ControlEquivalence6DFSPopERNS0_9ZoneStackINS2_13DFSStackEntryEEEPNS1_4NodeE, @function
_ZN2v88internal8compiler18ControlEquivalence6DFSPopERNS0_9ZoneStackINS2_13DFSStackEntryEEEPNS1_4NodeE:
.LFB10248:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	40(%rdi), %rcx
	movq	%rdi, %rbx
	movl	20(%rdx), %r13d
	movq	32(%rdi), %rdx
	movq	%rcx, %rax
	subq	%rdx, %rax
	andl	$16777215, %r13d
	sarq	$3, %rax
	cmpq	%rax, %r13
	jnb	.L82
.L70:
	movq	(%rdx,%r13,8), %rax
	andb	$-3, 40(%rax)
	movq	40(%rbx), %rcx
	movq	32(%rbx), %rdx
	movl	20(%r14), %r13d
	movq	%rcx, %rax
	subq	%rdx, %rax
	andl	$16777215, %r13d
	sarq	$3, %rax
	cmpq	%rax, %r13
	jnb	.L83
.L72:
	movq	(%rdx,%r13,8), %rax
	orb	$1, 40(%rax)
	movq	64(%r12), %rax
	movq	72(%r12), %rdx
	cmpq	%rdx, %rax
	je	.L74
	subq	$56, %rax
	popq	%rbx
	movq	%rax, 64(%r12)
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L82:
	.cfi_restore_state
	leaq	1(%r13), %rsi
	cmpq	%rsi, %rax
	jb	.L84
	jbe	.L70
	leaq	(%rdx,%rsi,8), %rax
	cmpq	%rax, %rcx
	je	.L70
	movq	%rax, 40(%rdi)
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L83:
	leaq	1(%r13), %rsi
	cmpq	%rsi, %rax
	jb	.L85
	jbe	.L72
	leaq	(%rdx,%rsi,8), %rax
	cmpq	%rax, %rcx
	je	.L72
	movq	%rax, 40(%rbx)
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L74:
	movq	8(%r12), %rax
	testq	%rax, %rax
	je	.L76
	cmpq	$9, 8(%rax)
	ja	.L77
.L76:
	movq	$9, 8(%rdx)
	movq	8(%r12), %rax
	movq	%rax, (%rdx)
	movq	%rdx, 8(%r12)
.L77:
	movq	88(%r12), %rax
	popq	%rbx
	leaq	-8(%rax), %rdx
	movq	%rdx, 88(%r12)
	movq	-8(%rax), %rax
	movq	%rax, 72(%r12)
	leaq	504(%rax), %rdx
	addq	$448, %rax
	movq	%rdx, 80(%r12)
	movq	%rax, 64(%r12)
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L85:
	.cfi_restore_state
	subq	%rax, %rsi
	leaq	24(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler18ControlEquivalence8NodeDataENS1_13ZoneAllocatorIS5_EEE17_M_default_appendEm
	movq	32(%rbx), %rdx
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L84:
	subq	%rax, %rsi
	leaq	24(%rdi), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler18ControlEquivalence8NodeDataENS1_13ZoneAllocatorIS5_EEE17_M_default_appendEm
	movq	32(%rbx), %rdx
	jmp	.L70
	.cfi_endproc
.LFE10248:
	.size	_ZN2v88internal8compiler18ControlEquivalence6DFSPopERNS0_9ZoneStackINS2_13DFSStackEntryEEEPNS1_4NodeE, .-_ZN2v88internal8compiler18ControlEquivalence6DFSPopERNS0_9ZoneStackINS2_13DFSStackEntryEEEPNS1_4NodeE
	.section	.rodata._ZN2v88internal8compiler18ControlEquivalence13VisitBackedgeEPNS1_4NodeES4_NS2_12DFSDirectionE.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"CEQ: Backedge from #%d:%s to #%d:%s\n"
	.section	.text._ZN2v88internal8compiler18ControlEquivalence13VisitBackedgeEPNS1_4NodeES4_NS2_12DFSDirectionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18ControlEquivalence13VisitBackedgeEPNS1_4NodeES4_NS2_12DFSDirectionE
	.type	_ZN2v88internal8compiler18ControlEquivalence13VisitBackedgeEPNS1_4NodeES4_NS2_12DFSDirectionE, @function
_ZN2v88internal8compiler18ControlEquivalence13VisitBackedgeEPNS1_4NodeES4_NS2_12DFSDirectionE:
.LFB10225:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movl	20(%rsi), %esi
	andl	$16777215, %esi
	cmpb	$0, _ZN2v88internal20FLAG_trace_turbo_ceqE(%rip)
	jne	.L93
.L87:
	movq	40(%r13), %rcx
	movq	32(%r13), %rdx
	pxor	%xmm0, %xmm0
	movl	%esi, %ebx
	movq	$0, -80(%rbp)
	movq	%rcx, %rax
	movaps	%xmm0, -96(%rbp)
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rax, %rbx
	jnb	.L94
.L88:
	movq	(%rdx,%rbx,8), %rbx
	movq	8(%rbx), %r8
	leaq	16(%rbx), %r13
	movq	16(%r8), %rdi
	movq	24(%r8), %rax
	subq	%rdi, %rax
	cmpq	$55, %rax
	jbe	.L95
	leaq	56(%rdi), %rax
	movq	%rax, 16(%r8)
.L91:
	movq	%r14, %xmm1
	movq	%r12, %xmm0
	movl	%r15d, -96(%rbp)
	movq	%r13, %rsi
	punpcklqdq	%xmm1, %xmm0
	movq	$-1, -88(%rbp)
	movdqa	-96(%rbp), %xmm2
	movups	%xmm0, -72(%rbp)
	movq	-64(%rbp), %rax
	movdqa	-80(%rbp), %xmm3
	movups	%xmm2, 16(%rdi)
	movq	%rax, 48(%rdi)
	movups	%xmm3, 32(%rdi)
	call	_ZNSt8__detail15_List_node_base7_M_hookEPS0_@PLT
	addq	$1, 32(%rbx)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L93:
	.cfi_restore_state
	movq	(%rdx), %rax
	movl	20(%rdx), %ecx
	leaq	.LC6(%rip), %rdi
	movq	(%r12), %rdx
	movq	8(%rax), %r8
	andl	$16777215, %ecx
	xorl	%eax, %eax
	movq	8(%rdx), %rdx
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movl	20(%r12), %esi
	andl	$16777215, %esi
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L94:
	leaq	1(%rbx), %rsi
	cmpq	%rsi, %rax
	jb	.L96
	jbe	.L88
	leaq	(%rdx,%rsi,8), %rax
	cmpq	%rax, %rcx
	je	.L88
	movq	%rax, 40(%r13)
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L96:
	subq	%rax, %rsi
	leaq	24(%r13), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler18ControlEquivalence8NodeDataENS1_13ZoneAllocatorIS5_EEE17_M_default_appendEm
	movq	32(%r13), %rdx
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L95:
	movq	%r8, %rdi
	movl	$56, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdi
	jmp	.L91
	.cfi_endproc
.LFE10225:
	.size	_ZN2v88internal8compiler18ControlEquivalence13VisitBackedgeEPNS1_4NodeES4_NS2_12DFSDirectionE, .-_ZN2v88internal8compiler18ControlEquivalence13VisitBackedgeEPNS1_4NodeES4_NS2_12DFSDirectionE
	.section	.rodata._ZN2v88internal8compiler18ControlEquivalence9VisitPostEPNS1_4NodeES4_NS2_12DFSDirectionE.str1.1,"aMS",@progbits,1
.LC7:
	.string	"CEQ: Post-visit of #%d:%s\n"
	.section	.text.unlikely._ZN2v88internal8compiler18ControlEquivalence9VisitPostEPNS1_4NodeES4_NS2_12DFSDirectionE,"ax",@progbits
	.align 2
.LCOLDB8:
	.section	.text._ZN2v88internal8compiler18ControlEquivalence9VisitPostEPNS1_4NodeES4_NS2_12DFSDirectionE,"ax",@progbits
.LHOTB8:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18ControlEquivalence9VisitPostEPNS1_4NodeES4_NS2_12DFSDirectionE
	.type	_ZN2v88internal8compiler18ControlEquivalence9VisitPostEPNS1_4NodeES4_NS2_12DFSDirectionE, @function
_ZN2v88internal8compiler18ControlEquivalence9VisitPostEPNS1_4NodeES4_NS2_12DFSDirectionE:
.LFB10224:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$24, %rsp
	movl	20(%rsi), %esi
	movl	%ecx, -60(%rbp)
	andl	$16777215, %esi
	cmpb	$0, _ZN2v88internal20FLAG_trace_turbo_ceqE(%rip)
	jne	.L118
.L98:
	movq	40(%r15), %rcx
	movq	32(%r15), %rdx
	movl	%esi, %r13d
	movq	%rcx, %rax
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rax, %r13
	jnb	.L119
.L99:
	movq	(%rdx,%r13,8), %r14
	movq	16(%r14), %r8
	leaq	16(%r14), %r13
	cmpq	%r8, %r13
	jne	.L101
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L103:
	movq	(%r8), %r8
	cmpq	%r8, %r13
	je	.L102
.L101:
	cmpq	48(%r8), %r12
	jne	.L103
	movl	-60(%rbp), %eax
	cmpl	16(%r8), %eax
	je	.L103
	cmpb	$0, _ZN2v88internal20FLAG_trace_turbo_ceqE(%rip)
	jne	.L120
.L105:
	movq	(%r8), %rax
	subq	$1, 32(%r14)
	movq	%r8, %rdi
	movq	%rax, -56(%rbp)
	call	_ZNSt8__detail15_List_node_base9_M_unhookEv@PLT
	movq	-56(%rbp), %r8
	cmpq	%r8, %r13
	jne	.L101
	.p2align 4,,10
	.p2align 3
.L102:
	testq	%rbx, %rbx
	je	.L97
	movq	40(%r15), %rdi
	movq	32(%r15), %rdx
	movl	20(%rbx), %ebx
	movq	%rdi, %rax
	subq	%rdx, %rax
	andl	$16777215, %ebx
	sarq	$3, %rax
	cmpq	%rax, %rbx
	jnb	.L121
.L108:
	movq	16(%r14), %rsi
	cmpq	%rsi, %r8
	je	.L97
	movq	(%rdx,%rbx,8), %rbx
	movq	8(%r14), %rax
	cmpq	%rax, 8(%rbx)
	jne	.L116
	leaq	16(%rbx), %rdi
	movq	%r8, %rdx
	call	_ZNSt8__detail15_List_node_base11_M_transferEPS0_S1_@PLT
	movq	32(%r14), %rax
	addq	%rax, 32(%rbx)
	movq	$0, 32(%r14)
.L97:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L120:
	.cfi_restore_state
	movq	40(%r8), %rax
	movl	20(%r12), %edx
	leaq	.LC1(%rip), %rdi
	movq	%r8, -56(%rbp)
	movl	20(%rax), %esi
	andl	$16777215, %edx
	xorl	%eax, %eax
	andl	$16777215, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-56(%rbp), %r8
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L119:
	leaq	1(%r13), %rsi
	cmpq	%rsi, %rax
	jb	.L122
	jbe	.L99
	leaq	(%rdx,%rsi,8), %rax
	cmpq	%rax, %rcx
	je	.L99
	movq	%rax, 40(%r15)
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L118:
	movq	(%r12), %rax
	leaq	.LC7(%rip), %rdi
	movq	8(%rax), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movl	20(%r12), %esi
	andl	$16777215, %esi
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L121:
	leaq	1(%rbx), %rsi
	cmpq	%rsi, %rax
	jb	.L123
	jbe	.L108
	leaq	(%rdx,%rsi,8), %rax
	cmpq	%rax, %rdi
	je	.L108
	movq	%rax, 40(%r15)
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L122:
	subq	%rax, %rsi
	leaq	24(%r15), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler18ControlEquivalence8NodeDataENS1_13ZoneAllocatorIS5_EEE17_M_default_appendEm
	movq	32(%r15), %rdx
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L123:
	subq	%rax, %rsi
	leaq	24(%r15), %rdi
	movq	%r8, -56(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler18ControlEquivalence8NodeDataENS1_13ZoneAllocatorIS5_EEE17_M_default_appendEm
	movq	32(%r15), %rdx
	movq	-56(%rbp), %r8
	jmp	.L108
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal8compiler18ControlEquivalence9VisitPostEPNS1_4NodeES4_NS2_12DFSDirectionE
	.cfi_startproc
	.type	_ZN2v88internal8compiler18ControlEquivalence9VisitPostEPNS1_4NodeES4_NS2_12DFSDirectionE.cold, @function
_ZN2v88internal8compiler18ControlEquivalence9VisitPostEPNS1_4NodeES4_NS2_12DFSDirectionE.cold:
.LFSB10224:
.L116:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	call	abort@PLT
	.cfi_endproc
.LFE10224:
	.section	.text._ZN2v88internal8compiler18ControlEquivalence9VisitPostEPNS1_4NodeES4_NS2_12DFSDirectionE
	.size	_ZN2v88internal8compiler18ControlEquivalence9VisitPostEPNS1_4NodeES4_NS2_12DFSDirectionE, .-_ZN2v88internal8compiler18ControlEquivalence9VisitPostEPNS1_4NodeES4_NS2_12DFSDirectionE
	.section	.text.unlikely._ZN2v88internal8compiler18ControlEquivalence9VisitPostEPNS1_4NodeES4_NS2_12DFSDirectionE
	.size	_ZN2v88internal8compiler18ControlEquivalence9VisitPostEPNS1_4NodeES4_NS2_12DFSDirectionE.cold, .-_ZN2v88internal8compiler18ControlEquivalence9VisitPostEPNS1_4NodeES4_NS2_12DFSDirectionE.cold
.LCOLDE8:
	.section	.text._ZN2v88internal8compiler18ControlEquivalence9VisitPostEPNS1_4NodeES4_NS2_12DFSDirectionE
.LHOTE8:
	.section	.rodata._ZN2v88internal8compiler18ControlEquivalence8VisitMidEPNS1_4NodeENS2_12DFSDirectionE.str1.1,"aMS",@progbits,1
.LC9:
	.string	"CEQ: Mid-visit of #%d:%s\n"
	.section	.rodata._ZN2v88internal8compiler18ControlEquivalence8VisitMidEPNS1_4NodeENS2_12DFSDirectionE.str1.8,"aMS",@progbits,1
	.align 8
.LC10:
	.string	"  Assigned class number is %zu\n"
	.section	.text._ZN2v88internal8compiler18ControlEquivalence8VisitMidEPNS1_4NodeENS2_12DFSDirectionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18ControlEquivalence8VisitMidEPNS1_4NodeENS2_12DFSDirectionE
	.type	_ZN2v88internal8compiler18ControlEquivalence8VisitMidEPNS1_4NodeENS2_12DFSDirectionE, @function
_ZN2v88internal8compiler18ControlEquivalence8VisitMidEPNS1_4NodeENS2_12DFSDirectionE:
.LFB10223:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	20(%rsi), %esi
	andl	$16777215, %esi
	cmpb	$0, _ZN2v88internal20FLAG_trace_turbo_ceqE(%rip)
	jne	.L160
.L125:
	movq	40(%r14), %rcx
	movq	32(%r14), %rdx
	movl	%esi, %ebx
	movq	%rcx, %rax
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rax, %rbx
	jnb	.L161
.L126:
	movq	(%rdx,%rbx,8), %rbx
	movq	16(%rbx), %r12
	leaq	16(%rbx), %rax
	movq	%rax, -56(%rbp)
	cmpq	%rax, %r12
	jne	.L128
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L131:
	movq	(%r12), %r12
	cmpq	-56(%rbp), %r12
	je	.L162
.L128:
	cmpq	48(%r12), %r15
	jne	.L131
	cmpl	16(%r12), %r13d
	je	.L131
	cmpb	$0, _ZN2v88internal20FLAG_trace_turbo_ceqE(%rip)
	jne	.L163
.L133:
	movq	(%r12), %rax
	subq	$1, 32(%rbx)
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	call	_ZNSt8__detail15_List_node_base9_M_unhookEv@PLT
	movq	-64(%rbp), %r12
	cmpq	-56(%rbp), %r12
	jne	.L128
	.p2align 4,,10
	.p2align 3
.L162:
	movq	-56(%rbp), %rax
	cmpq	16(%rbx), %rax
	je	.L129
.L130:
	cmpb	$0, _ZN2v88internal20FLAG_trace_turbo_ceqE(%rip)
	jne	.L164
.L136:
	movq	24(%rbx), %rax
	movq	32(%rbx), %rdx
	cmpq	%rdx, 32(%rax)
	je	.L165
	movq	%rdx, 32(%rax)
	movslq	20(%r14), %r12
	leal	1(%r12), %edx
	movl	%edx, 20(%r14)
	movq	%r12, 24(%rax)
.L143:
	movq	40(%r14), %rcx
	movq	32(%r14), %rdx
	movl	20(%r15), %ebx
	movq	%rcx, %rax
	subq	%rdx, %rax
	andl	$16777215, %ebx
	sarq	$3, %rax
	cmpq	%rax, %rbx
	jnb	.L166
.L144:
	movq	(%rdx,%rbx,8), %rax
	movq	%r12, (%rax)
	cmpb	$0, _ZN2v88internal20FLAG_trace_turbo_ceqE(%rip)
	jne	.L167
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L163:
	.cfi_restore_state
	movq	40(%r12), %rax
	movl	20(%r15), %edx
	leaq	.LC1(%rip), %rdi
	movl	20(%rax), %esi
	andl	$16777215, %edx
	xorl	%eax, %eax
	andl	$16777215, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L165:
	movq	24(%rax), %r12
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L164:
	xorl	%eax, %eax
	leaq	.LC2(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	16(%rbx), %r13
	movzbl	_ZN2v88internal20FLAG_trace_turbo_ceqE(%rip), %eax
	cmpq	-56(%rbp), %r13
	je	.L140
	leaq	.LC3(%rip), %r12
.L139:
	testb	%al, %al
	je	.L138
	movq	48(%r13), %rax
	movq	%r12, %rdi
	movl	20(%rax), %edx
	movq	40(%r13), %rax
	movl	20(%rax), %esi
	andl	$16777215, %edx
	xorl	%eax, %eax
	andl	$16777215, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movzbl	_ZN2v88internal20FLAG_trace_turbo_ceqE(%rip), %eax
	movq	0(%r13), %r13
	cmpq	%r13, -56(%rbp)
	jne	.L139
.L140:
	testb	%al, %al
	je	.L136
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L138:
	movq	0(%r13), %r13
	cmpq	-56(%rbp), %r13
	je	.L136
	movq	0(%r13), %r13
	cmpq	-56(%rbp), %r13
	jne	.L138
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L161:
	leaq	1(%rbx), %rsi
	cmpq	%rsi, %rax
	jb	.L168
	jbe	.L126
	leaq	(%rdx,%rsi,8), %rax
	cmpq	%rax, %rcx
	je	.L126
	movq	%rax, 40(%r14)
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L160:
	movq	(%r15), %rax
	leaq	.LC9(%rip), %rdi
	movq	8(%rax), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movl	20(%r15), %esi
	andl	$16777215, %esi
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L167:
	movq	40(%r14), %rcx
	movq	32(%r14), %rdx
	movl	20(%r15), %ebx
	movq	%rcx, %rax
	subq	%rdx, %rax
	andl	$16777215, %ebx
	sarq	$3, %rax
	cmpq	%rax, %rbx
	jnb	.L169
.L147:
	movq	(%rdx,%rbx,8), %rax
	leaq	.LC10(%rip), %rdi
	movq	(%rax), %rsi
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal6PrintFEPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L166:
	.cfi_restore_state
	leaq	1(%rbx), %rsi
	cmpq	%rsi, %rax
	jb	.L170
	jbe	.L144
	leaq	(%rdx,%rsi,8), %rax
	cmpq	%rax, %rcx
	je	.L144
	movq	%rax, 40(%r14)
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L129:
	movq	8(%r14), %rax
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	16(%rax), %rdx
	call	_ZN2v88internal8compiler18ControlEquivalence13VisitBackedgeEPNS1_4NodeES4_NS2_12DFSDirectionE
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L168:
	subq	%rax, %rsi
	leaq	24(%r14), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler18ControlEquivalence8NodeDataENS1_13ZoneAllocatorIS5_EEE17_M_default_appendEm
	movq	32(%r14), %rdx
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L170:
	subq	%rax, %rsi
	leaq	24(%r14), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler18ControlEquivalence8NodeDataENS1_13ZoneAllocatorIS5_EEE17_M_default_appendEm
	movq	32(%r14), %rdx
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L169:
	leaq	1(%rbx), %rsi
	cmpq	%rsi, %rax
	jb	.L171
	jbe	.L147
	leaq	(%rdx,%rsi,8), %rax
	cmpq	%rax, %rcx
	je	.L147
	movq	%rax, 40(%r14)
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L171:
	subq	%rax, %rsi
	leaq	24(%r14), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler18ControlEquivalence8NodeDataENS1_13ZoneAllocatorIS5_EEE17_M_default_appendEm
	movq	32(%r14), %rdx
	jmp	.L147
	.cfi_endproc
.LFE10223:
	.size	_ZN2v88internal8compiler18ControlEquivalence8VisitMidEPNS1_4NodeENS2_12DFSDirectionE, .-_ZN2v88internal8compiler18ControlEquivalence8VisitMidEPNS1_4NodeENS2_12DFSDirectionE
	.section	.rodata._ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_.str1.8,"aMS",@progbits,1
	.align 8
.LC11:
	.string	"cannot create std::deque larger than max_size()"
	.section	.text._ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_,"axG",@progbits,_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	.type	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_, @function
_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_:
.LFB11793:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	88(%rdi), %r13
	movq	56(%rdi), %rsi
	movq	64(%rbx), %rdx
	subq	72(%rbx), %rdx
	movq	%r13, %r14
	movq	%rdx, %rcx
	subq	%rsi, %r14
	sarq	$3, %rcx
	movq	%r14, %rdi
	sarq	$3, %rdi
	leaq	-1(%rdi), %rax
	salq	$6, %rax
	leaq	(%rcx,%rax), %rdx
	movq	48(%rbx), %rax
	subq	32(%rbx), %rax
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpq	$268435455, %rax
	je	.L190
	movq	16(%rbx), %rcx
	movq	24(%rbx), %rdx
	movq	%r13, %rax
	subq	%rcx, %rax
	movq	%rdx, %r8
	sarq	$3, %rax
	subq	%rax, %r8
	cmpq	$1, %r8
	jbe	.L191
.L174:
	movq	8(%rbx), %rax
	testq	%rax, %rax
	je	.L182
	cmpq	$63, 8(%rax)
	ja	.L192
.L182:
	movq	(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L193
	leaq	512(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L183:
	movq	%rax, 8(%r13)
	movq	(%r12), %rdx
	movq	64(%rbx), %rax
	movq	%rdx, (%rax)
	movq	88(%rbx), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 88(%rbx)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 72(%rbx)
	movq	%rdx, 80(%rbx)
	movq	%rax, 64(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L191:
	.cfi_restore_state
	leaq	2(%rdi), %r15
	leaq	(%r15,%r15), %rax
	cmpq	%rax, %rdx
	ja	.L194
	testq	%rdx, %rdx
	movl	$1, %eax
	movq	(%rbx), %rdi
	cmovne	%rdx, %rax
	movq	16(%rdi), %r13
	leaq	2(%rdx,%rax), %rcx
	movq	24(%rdi), %rax
	leaq	0(,%rcx,8), %rsi
	subq	%r13, %rax
	cmpq	%rax, %rsi
	ja	.L195
	addq	%r13, %rsi
	movq	%rsi, 16(%rdi)
.L179:
	movq	%rcx, %rax
	movq	56(%rbx), %rsi
	subq	%r15, %rax
	shrq	%rax
	leaq	0(%r13,%rax,8), %r15
	movq	88(%rbx), %rax
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L180
	subq	%rsi, %rdx
	movq	%r15, %rdi
	movq	%rcx, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %rcx
.L180:
	movq	24(%rbx), %rax
	leaq	0(,%rax,8), %rdx
	cmpq	$15, %rdx
	jbe	.L181
	movq	16(%rbx), %rdx
	movq	%rax, 8(%rdx)
	movq	$0, (%rdx)
.L181:
	movq	%r13, 16(%rbx)
	movq	%rcx, 24(%rbx)
.L177:
	movq	%r15, 56(%rbx)
	movq	(%r15), %rax
	leaq	(%r15,%r14), %r13
	movq	(%r15), %xmm0
	movq	%r13, 88(%rbx)
	addq	$512, %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 40(%rbx)
	movq	0(%r13), %rax
	movq	%rax, 72(%rbx)
	addq	$512, %rax
	movq	%rax, 80(%rbx)
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L192:
	movq	(%rax), %rdx
	movq	%rdx, 8(%rbx)
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L194:
	subq	%r15, %rdx
	addq	$8, %r13
	shrq	%rdx
	leaq	(%rcx,%rdx,8), %r15
	movq	%r13, %rdx
	subq	%rsi, %rdx
	cmpq	%r15, %rsi
	jbe	.L176
	cmpq	%r13, %rsi
	je	.L177
	movq	%r15, %rdi
	call	memmove@PLT
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L176:
	cmpq	%r13, %rsi
	je	.L177
	leaq	8(%r14), %rdi
	subq	%rdx, %rdi
	addq	%r15, %rdi
	call	memmove@PLT
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L193:
	movl	$512, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L195:
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	movq	%rax, %r13
	jmp	.L179
.L190:
	leaq	.LC11(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE11793:
	.size	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_, .-_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	.section	.text._ZN2v88internal8compiler18ControlEquivalence29DetermineParticipationEnqueueERNS0_9ZoneQueueIPNS1_4NodeEEES5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18ControlEquivalence29DetermineParticipationEnqueueERNS0_9ZoneQueueIPNS1_4NodeEEES5_
	.type	_ZN2v88internal8compiler18ControlEquivalence29DetermineParticipationEnqueueERNS0_9ZoneQueueIPNS1_4NodeEEES5_, @function
_ZN2v88internal8compiler18ControlEquivalence29DetermineParticipationEnqueueERNS0_9ZoneQueueIPNS1_4NodeEEES5_:
.LFB10236:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	40(%rdi), %rcx
	movl	20(%rdx), %ebx
	movq	%rdx, -56(%rbp)
	movq	32(%rdi), %rdx
	movq	%rcx, %rax
	andl	$16777215, %ebx
	subq	%rdx, %rax
	leaq	0(,%rbx,8), %r14
	sarq	$3, %rax
	cmpq	%rax, %rbx
	jnb	.L210
	cmpq	$0, (%rdx,%rbx,8)
	je	.L203
.L196:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L210:
	.cfi_restore_state
	leaq	1(%rbx), %rsi
	leaq	24(%rdi), %r15
	cmpq	%rsi, %rax
	jb	.L211
	movq	%rax, %rdi
	jbe	.L200
	leaq	8(%rdx,%r14), %rax
	cmpq	%rax, %rcx
	je	.L200
	movq	%rax, 40(%r12)
.L199:
	cmpq	$0, (%rdx,%rbx,8)
	jne	.L196
	movq	-56(%rbp), %rax
	movq	40(%r12), %rcx
	movl	20(%rax), %esi
	movq	%rcx, %rax
	subq	%rdx, %rax
	andl	$16777215, %esi
	sarq	$3, %rax
	movq	%rax, %rdi
	leaq	0(,%rsi,8), %r14
	cmpq	%rax, %rsi
	jb	.L203
	addq	$1, %rsi
	cmpq	%rsi, %rax
	jnb	.L204
	subq	%rax, %rsi
	movq	%r15, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler18ControlEquivalence8NodeDataENS1_13ZoneAllocatorIS5_EEE17_M_default_appendEm
	.p2align 4,,10
	.p2align 3
.L203:
	movq	(%r12), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$47, %rdx
	jbe	.L212
	leaq	48(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L206:
	movq	(%r12), %rdx
	andb	$-4, 40(%rax)
	movq	$-1, (%rax)
	movq	%rdx, 8(%rax)
	leaq	16(%rax), %rdx
	movq	%rdx, 24(%rax)
	movq	%rdx, 16(%rax)
	movq	$0, 32(%rax)
	movq	32(%r12), %rdx
	movq	%rax, (%rdx,%r14)
	movq	80(%r13), %rax
	movq	64(%r13), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L207
	movq	-56(%rbp), %rax
	movq	%rax, (%rdx)
	addq	$8, 64(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L200:
	.cfi_restore_state
	cmpq	$0, (%rdx,%rbx,8)
	jne	.L196
.L204:
	cmpq	%rdi, %rsi
	jnb	.L203
	leaq	(%rdx,%rsi,8), %rax
	cmpq	%rcx, %rax
	je	.L203
	movq	%rax, 40(%r12)
	jmp	.L203
	.p2align 4,,10
	.p2align 3
.L211:
	subq	%rax, %rsi
	movq	%r15, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler18ControlEquivalence8NodeDataENS1_13ZoneAllocatorIS5_EEE17_M_default_appendEm
	movq	32(%r12), %rdx
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L207:
	leaq	-56(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L212:
	movl	$48, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L206
	.cfi_endproc
.LFE10236:
	.size	_ZN2v88internal8compiler18ControlEquivalence29DetermineParticipationEnqueueERNS0_9ZoneQueueIPNS1_4NodeEEES5_, .-_ZN2v88internal8compiler18ControlEquivalence29DetermineParticipationEnqueueERNS0_9ZoneQueueIPNS1_4NodeEEES5_
	.section	.text._ZNSt5dequeIN2v88internal8compiler18ControlEquivalence13DFSStackEntryENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJS4_EEEvDpOT_,"axG",@progbits,_ZNSt5dequeIN2v88internal8compiler18ControlEquivalence13DFSStackEntryENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJS4_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt5dequeIN2v88internal8compiler18ControlEquivalence13DFSStackEntryENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJS4_EEEvDpOT_
	.type	_ZNSt5dequeIN2v88internal8compiler18ControlEquivalence13DFSStackEntryENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJS4_EEEvDpOT_, @function
_ZNSt5dequeIN2v88internal8compiler18ControlEquivalence13DFSStackEntryENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJS4_EEEvDpOT_:
.LFB11993:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	88(%rdi), %r14
	movq	56(%rdi), %rsi
	movq	64(%rdi), %rdx
	subq	72(%rdi), %rdx
	movabsq	$7905747460161236407, %rdi
	movq	%r14, %r13
	sarq	$3, %rdx
	subq	%rsi, %r13
	imulq	%rdi, %rdx
	movq	%r13, %rcx
	sarq	$3, %rcx
	leaq	-9(%rcx,%rcx,8), %rax
	addq	%rax, %rdx
	movq	48(%rbx), %rax
	subq	32(%rbx), %rax
	sarq	$3, %rax
	imulq	%rdi, %rax
	addq	%rdx, %rax
	cmpq	$38347922, %rax
	je	.L231
	movq	16(%rbx), %rdi
	movq	24(%rbx), %rdx
	movq	%r14, %rax
	subq	%rdi, %rax
	movq	%rdx, %r8
	sarq	$3, %rax
	subq	%rax, %r8
	cmpq	$1, %r8
	jbe	.L232
.L215:
	movq	8(%rbx), %rax
	testq	%rax, %rax
	je	.L223
	cmpq	$8, 8(%rax)
	ja	.L233
.L223:
	movq	(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$503, %rdx
	jbe	.L234
	leaq	504(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L224:
	movq	%rax, 8(%r14)
	movdqu	(%r12), %xmm1
	movq	64(%rbx), %rax
	movups	%xmm1, (%rax)
	movdqu	16(%r12), %xmm2
	movups	%xmm2, 16(%rax)
	movdqu	32(%r12), %xmm3
	movups	%xmm3, 32(%rax)
	movq	48(%r12), %rdx
	movq	%rdx, 48(%rax)
	movq	88(%rbx), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 88(%rbx)
	movq	8(%rax), %rax
	leaq	504(%rax), %rdx
	movq	%rax, 72(%rbx)
	movq	%rdx, 80(%rbx)
	movq	%rax, 64(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L232:
	.cfi_restore_state
	leaq	2(%rcx), %r15
	leaq	(%r15,%r15), %rax
	cmpq	%rax, %rdx
	ja	.L235
	testq	%rdx, %rdx
	movl	$1, %eax
	movq	(%rbx), %rdi
	cmovne	%rdx, %rax
	movq	16(%rdi), %r14
	leaq	2(%rdx,%rax), %rcx
	movq	24(%rdi), %rax
	leaq	0(,%rcx,8), %rsi
	subq	%r14, %rax
	cmpq	%rax, %rsi
	ja	.L236
	addq	%r14, %rsi
	movq	%rsi, 16(%rdi)
.L220:
	movq	%rcx, %rax
	movq	56(%rbx), %rsi
	subq	%r15, %rax
	shrq	%rax
	leaq	(%r14,%rax,8), %r15
	movq	88(%rbx), %rax
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L221
	subq	%rsi, %rdx
	movq	%r15, %rdi
	movq	%rcx, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %rcx
.L221:
	movq	24(%rbx), %rax
	leaq	0(,%rax,8), %rdx
	cmpq	$15, %rdx
	jbe	.L222
	movq	16(%rbx), %rdx
	movq	%rax, 8(%rdx)
	movq	$0, (%rdx)
.L222:
	movq	%r14, 16(%rbx)
	movq	%rcx, 24(%rbx)
.L218:
	movq	%r15, 56(%rbx)
	movq	(%r15), %rax
	leaq	(%r15,%r13), %r14
	movq	(%r15), %xmm0
	movq	%r14, 88(%rbx)
	addq	$504, %rax
	movq	%rax, %xmm4
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, 40(%rbx)
	movq	(%r14), %rax
	movq	%rax, 72(%rbx)
	addq	$504, %rax
	movq	%rax, 80(%rbx)
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L233:
	movq	(%rax), %rdx
	movq	%rdx, 8(%rbx)
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L235:
	subq	%r15, %rdx
	addq	$8, %r14
	shrq	%rdx
	leaq	(%rdi,%rdx,8), %r15
	movq	%r14, %rdx
	subq	%rsi, %rdx
	cmpq	%r15, %rsi
	jbe	.L217
	cmpq	%r14, %rsi
	je	.L218
	movq	%r15, %rdi
	call	memmove@PLT
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L217:
	cmpq	%r14, %rsi
	je	.L218
	leaq	8(%r13), %rdi
	subq	%rdx, %rdi
	addq	%r15, %rdi
	call	memmove@PLT
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L234:
	movl	$504, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L236:
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	movq	%rax, %r14
	jmp	.L220
.L231:
	leaq	.LC11(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE11993:
	.size	_ZNSt5dequeIN2v88internal8compiler18ControlEquivalence13DFSStackEntryENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJS4_EEEvDpOT_, .-_ZNSt5dequeIN2v88internal8compiler18ControlEquivalence13DFSStackEntryENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJS4_EEEvDpOT_
	.section	.text._ZN2v88internal8compiler18ControlEquivalence7DFSPushERNS0_9ZoneStackINS2_13DFSStackEntryEEEPNS1_4NodeES8_NS2_12DFSDirectionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18ControlEquivalence7DFSPushERNS0_9ZoneStackINS2_13DFSStackEntryEEEPNS1_4NodeES8_NS2_12DFSDirectionE
	.type	_ZN2v88internal8compiler18ControlEquivalence7DFSPushERNS0_9ZoneStackINS2_13DFSStackEntryEEEPNS1_4NodeES8_NS2_12DFSDirectionE, @function
_ZN2v88internal8compiler18ControlEquivalence7DFSPushERNS0_9ZoneStackINS2_13DFSStackEntryEEEPNS1_4NodeES8_NS2_12DFSDirectionE:
.LFB10247:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	subq	$80, %rsp
	movq	%rcx, -104(%rbp)
	movq	40(%rdi), %rcx
	movl	20(%rdx), %r13d
	movq	32(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rcx, %rax
	andl	$16777215, %r13d
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rax, %r13
	jnb	.L249
.L238:
	movq	(%rdx,%r13,8), %rax
	leaq	32(%rbx), %rdx
	orb	$2, 40(%rax)
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L240
	leaq	-24(%rbx), %rax
	movq	%rax, %xmm0
.L241:
	movq	24(%rbx), %rax
	xorl	%ecx, %ecx
	testq	%rax, %rax
	je	.L242
	movq	(%rax), %rcx
.L242:
	movq	%rdx, %xmm1
	movq	%rcx, %xmm2
	movq	80(%r12), %rdi
	movq	%rbx, %xmm3
	punpcklqdq	%xmm1, %xmm0
	movl	%r8d, -96(%rbp)
	movups	%xmm0, -88(%rbp)
	movq	%rax, %xmm0
	movq	64(%r12), %rax
	leaq	-56(%rdi), %rdx
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, -72(%rbp)
	movq	-104(%rbp), %xmm0
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, -56(%rbp)
	cmpq	%rdx, %rax
	je	.L243
	movdqa	-96(%rbp), %xmm4
	movups	%xmm4, (%rax)
	movdqa	-80(%rbp), %xmm5
	movups	%xmm5, 16(%rax)
	movdqa	-64(%rbp), %xmm6
	movups	%xmm6, 32(%rax)
	movq	-48(%rbp), %rdx
	movq	%rdx, 48(%rax)
	addq	$56, 64(%r12)
.L237:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L250
	addq	$80, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L240:
	.cfi_restore_state
	movq	32(%rbx), %rdx
	leaq	-24(%rdx), %rax
	addq	$16, %rdx
	movq	%rax, %xmm0
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L249:
	leaq	1(%r13), %rsi
	movq	%rdi, %r14
	cmpq	%rsi, %rax
	jb	.L251
	jbe	.L238
	leaq	(%rdx,%rsi,8), %rax
	cmpq	%rax, %rcx
	je	.L238
	movq	%rax, 40(%rdi)
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L243:
	leaq	-96(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt5dequeIN2v88internal8compiler18ControlEquivalence13DFSStackEntryENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJS4_EEEvDpOT_
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L251:
	subq	%rax, %rsi
	leaq	24(%rdi), %rdi
	movl	%r8d, -108(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler18ControlEquivalence8NodeDataENS1_13ZoneAllocatorIS5_EEE17_M_default_appendEm
	movq	32(%r14), %rdx
	movl	-108(%rbp), %r8d
	jmp	.L238
.L250:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10247:
	.size	_ZN2v88internal8compiler18ControlEquivalence7DFSPushERNS0_9ZoneStackINS2_13DFSStackEntryEEEPNS1_4NodeES8_NS2_12DFSDirectionE, .-_ZN2v88internal8compiler18ControlEquivalence7DFSPushERNS0_9ZoneStackINS2_13DFSStackEntryEEEPNS1_4NodeES8_NS2_12DFSDirectionE
	.section	.text._ZNSt11_Deque_baseIN2v88internal8compiler18ControlEquivalence13DFSStackEntryENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm,"axG",@progbits,_ZNSt11_Deque_baseIN2v88internal8compiler18ControlEquivalence13DFSStackEntryENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt11_Deque_baseIN2v88internal8compiler18ControlEquivalence13DFSStackEntryENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	.type	_ZNSt11_Deque_baseIN2v88internal8compiler18ControlEquivalence13DFSStackEntryENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm, @function
_ZNSt11_Deque_baseIN2v88internal8compiler18ControlEquivalence13DFSStackEntryENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm:
.LFB12104:
	.cfi_startproc
	endbr64
	movabsq	$-2049638230412172401, %rdx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	mulq	%rdx
	movl	$8, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	shrq	$3, %rdx
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	leaq	3(%rdx), %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	1(%rdx), %rbx
	subq	$8, %rsp
	cmpq	$8, %rsi
	cmovb	%rax, %rsi
	movq	%rsi, 24(%rdi)
	movq	(%rdi), %rdi
	salq	$3, %rsi
	movq	16(%rdi), %rdx
	movq	24(%rdi), %rax
	subq	%rdx, %rax
	cmpq	%rax, %rsi
	ja	.L266
	addq	%rdx, %rsi
	movq	%rsi, 16(%rdi)
.L254:
	movq	24(%r12), %rax
	movq	%rdx, 16(%r12)
	subq	%rbx, %rax
	shrq	%rax
	leaq	(%rdx,%rax,8), %r15
	leaq	(%r15,%rbx,8), %r14
	cmpq	%r14, %r15
	jnb	.L255
	movq	%r15, %rbx
	jmp	.L260
	.p2align 4,,10
	.p2align 3
.L256:
	movq	(%r12), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$503, %rdx
	jbe	.L267
	leaq	504(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L257:
	movq	%rax, (%rbx)
	addq	$8, %rbx
	cmpq	%rbx, %r14
	jbe	.L255
.L260:
	movq	8(%r12), %rax
	testq	%rax, %rax
	je	.L256
	cmpq	$8, 8(%rax)
	jbe	.L256
	movq	(%rax), %rdx
	addq	$8, %rbx
	movq	%rdx, 8(%r12)
	movq	%rax, -8(%rbx)
	cmpq	%rbx, %r14
	ja	.L260
.L255:
	movq	%r15, 56(%r12)
	movq	(%r15), %rax
	leaq	504(%rax), %rdx
	movq	%rax, 40(%r12)
	movq	%rdx, 48(%r12)
	leaq	-8(%r14), %rdx
	movq	%rdx, 88(%r12)
	movq	-8(%r14), %rcx
	movq	%rax, 32(%r12)
	movq	%r13, %rax
	leaq	504(%rcx), %rdx
	movq	%rcx, 72(%r12)
	movq	%rdx, 80(%r12)
	movabsq	$-2049638230412172401, %rdx
	mulq	%rdx
	movq	%rdx, %rax
	andq	$-8, %rdx
	shrq	$3, %rax
	addq	%rax, %rdx
	subq	%rdx, %r13
	leaq	0(,%r13,8), %rax
	subq	%r13, %rax
	leaq	(%rcx,%rax,8), %rax
	movq	%rax, 64(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L267:
	.cfi_restore_state
	movl	$504, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L257
	.p2align 4,,10
	.p2align 3
.L266:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L254
	.cfi_endproc
.LFE12104:
	.size	_ZNSt11_Deque_baseIN2v88internal8compiler18ControlEquivalence13DFSStackEntryENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm, .-_ZNSt11_Deque_baseIN2v88internal8compiler18ControlEquivalence13DFSStackEntryENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	.section	.text._ZN2v88internal8compiler18ControlEquivalence16RunUndirectedDFSEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18ControlEquivalence16RunUndirectedDFSEPNS1_4NodeE
	.type	_ZN2v88internal8compiler18ControlEquivalence16RunUndirectedDFSEPNS1_4NodeE, @function
_ZN2v88internal8compiler18ControlEquivalence16RunUndirectedDFSEPNS1_4NodeE:
.LFB10226:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	xorl	%esi, %esi
	subq	$344, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	leaq	-256(%rbp), %rdi
	movq	$0, -248(%rbp)
	movq	$0, -240(%rbp)
	movq	%rax, -256(%rbp)
	movq	$0, -232(%rbp)
	movaps	%xmm0, -224(%rbp)
	movaps	%xmm0, -208(%rbp)
	movaps	%xmm0, -192(%rbp)
	movaps	%xmm0, -176(%rbp)
	call	_ZNSt11_Deque_baseIN2v88internal8compiler18ControlEquivalence13DFSStackEntryENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	cmpq	$0, -240(%rbp)
	je	.L374
	movdqa	-256(%rbp), %xmm7
	pxor	%xmm0, %xmm0
	leaq	-160(%rbp), %rdi
	xorl	%esi, %esi
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm7, -160(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	$0, -144(%rbp)
	movq	$0, -136(%rbp)
	call	_ZNSt11_Deque_baseIN2v88internal8compiler18ControlEquivalence13DFSStackEntryENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	movdqa	-128(%rbp), %xmm7
	movq	-224(%rbp), %xmm3
	movq	-216(%rbp), %r9
	movq	-200(%rbp), %r8
	movaps	%xmm7, -224(%rbp)
	movdqa	-112(%rbp), %xmm7
	movq	-208(%rbp), %xmm2
	movq	-192(%rbp), %xmm1
	movq	-184(%rbp), %rcx
	movaps	%xmm7, -208(%rbp)
	movdqa	-96(%rbp), %xmm7
	movdqa	-80(%rbp), %xmm4
	movq	-168(%rbp), %rdx
	movq	-248(%rbp), %rax
	movaps	%xmm7, -192(%rbp)
	movq	%r9, %xmm7
	movq	-176(%rbp), %xmm0
	movq	-240(%rbp), %r11
	punpcklqdq	%xmm7, %xmm3
	movaps	%xmm4, -176(%rbp)
	movq	-256(%rbp), %xmm4
	movq	-232(%rbp), %r10
	movaps	%xmm3, -320(%rbp)
	movq	%r8, %xmm3
	movq	%rax, %xmm6
	movq	-144(%rbp), %rsi
	punpcklqdq	%xmm3, %xmm2
	movq	-136(%rbp), %rdi
	punpcklqdq	%xmm6, %xmm4
	movq	%r11, -336(%rbp)
	movaps	%xmm2, -304(%rbp)
	movq	%rcx, %xmm2
	punpcklqdq	%xmm2, %xmm1
	movq	%rsi, -240(%rbp)
	movaps	%xmm1, -288(%rbp)
	movq	%rdx, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movq	%rdi, -232(%rbp)
	movq	%r10, -328(%rbp)
	movaps	%xmm4, -352(%rbp)
	movaps	%xmm0, -272(%rbp)
	testq	%rsi, %rsi
	je	.L270
	movq	-168(%rbp), %rcx
	movq	-200(%rbp), %rdx
	addq	$8, %rcx
	cmpq	%rdx, %rcx
	jbe	.L271
	.p2align 4,,10
	.p2align 3
.L274:
	testq	%rax, %rax
	je	.L272
	cmpq	$9, 8(%rax)
	ja	.L273
.L272:
	movq	(%rdx), %rax
	movq	$9, 8(%rax)
	movq	-248(%rbp), %rsi
	movq	%rsi, (%rax)
	movq	%rax, -248(%rbp)
.L273:
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	ja	.L274
	movq	-232(%rbp), %rdi
	movq	-240(%rbp), %rsi
.L271:
	leaq	0(,%rdi,8), %rax
	cmpq	$15, %rax
	jbe	.L270
	movq	%rdi, 8(%rsi)
	movq	$0, (%rsi)
.L270:
	leaq	-352(%rbp), %r14
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler18ControlEquivalence7DFSPushERNS0_9ZoneStackINS2_13DFSStackEntryEEEPNS1_4NodeES8_NS2_12DFSDirectionE
	cmpb	$0, _ZN2v88internal20FLAG_trace_turbo_ceqE(%rip)
	jne	.L375
.L276:
	leaq	24(%r12), %rax
	movq	-288(%rbp), %rbx
	movq	%rax, -376(%rbp)
	cmpq	-320(%rbp), %rbx
	je	.L278
	.p2align 4,,10
	.p2align 3
.L277:
	cmpq	%rbx, -280(%rbp)
	je	.L376
.L279:
	movl	-56(%rbx), %eax
	movq	-8(%rbx), %r13
	testl	%eax, %eax
	je	.L377
	cmpl	$1, %eax
	jne	.L378
	movq	-32(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L304
	movl	16(%rdx), %ecx
	movl	%ecx, %eax
	shrl	%eax
	leaq	3(%rax,%rax,2), %rsi
	salq	$3, %rax
	andl	$1, %ecx
	leaq	(%rdx,%rsi,8), %r15
	je	.L305
	leaq	32(%r15,%rax), %rax
.L306:
	movq	%rax, %xmm6
	movq	%rdx, %xmm5
	movq	-24(%rbx), %rax
	punpcklqdq	%xmm6, %xmm5
	movq	%rax, -32(%rbx)
	movaps	%xmm5, -368(%rbp)
	testq	%rax, %rax
	je	.L307
	movq	(%rax), %rax
.L307:
	movq	%rax, -24(%rbx)
	movq	-360(%rbp), %rsi
	movq	-368(%rbp), %rdi
	call	_ZN2v88internal8compiler14NodeProperties13IsControlEdgeENS1_4EdgeE@PLT
	testb	%al, %al
	je	.L301
	movq	40(%r12), %rdi
	movq	32(%r12), %rcx
	movl	20(%r15), %edx
	movq	%rdi, %rax
	subq	%rcx, %rax
	andl	$16777215, %edx
	sarq	$3, %rax
	movq	%rax, %r8
	cmpq	%rax, %rdx
	jnb	.L379
	cmpq	$0, (%rcx,%rdx,8)
	je	.L301
	leaq	0(,%rdx,8), %rax
.L315:
	movq	(%rcx,%rax), %rax
	testb	$1, 40(%rax)
	je	.L320
	.p2align 4,,10
	.p2align 3
.L301:
	movq	-288(%rbp), %rbx
	cmpq	%rbx, -320(%rbp)
	jne	.L277
.L278:
	movq	-336(%rbp), %rax
	testq	%rax, %rax
	je	.L268
	movq	-264(%rbp), %rdi
	movq	-296(%rbp), %rdx
	leaq	8(%rdi), %rcx
	cmpq	%rdx, %rcx
	jbe	.L336
	movq	-344(%rbp), %rax
	.p2align 4,,10
	.p2align 3
.L339:
	testq	%rax, %rax
	je	.L337
	cmpq	$9, 8(%rax)
	ja	.L338
.L337:
	movq	(%rdx), %rax
	movq	$9, 8(%rax)
	movq	-344(%rbp), %rsi
	movq	%rsi, (%rax)
	movq	%rax, -344(%rbp)
.L338:
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	ja	.L339
	movq	-336(%rbp), %rax
.L336:
	movq	-328(%rbp), %rdx
	leaq	0(,%rdx,8), %rcx
	cmpq	$15, %rcx
	jbe	.L268
	movq	%rdx, 8(%rax)
	movq	$0, (%rax)
.L268:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L380
	addq	$344, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L377:
	.cfi_restore_state
	movl	20(%r13), %eax
	leaq	32(%r13), %rcx
	movl	%eax, %edx
	shrl	$24, %edx
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L282
	movq	32(%r13), %rcx
	movl	8(%rcx), %edx
	addq	$16, %rcx
.L282:
	movslq	%edx, %rdx
	movq	-40(%rbx), %rsi
	leaq	(%rcx,%rdx,8), %rdx
	cmpq	%rdx, %rsi
	je	.L283
	movq	-48(%rbx), %rdi
	leaq	8(%rsi), %rax
	movq	(%rsi), %r15
	movq	%rax, -40(%rbx)
	leaq	-24(%rdi), %rax
	movq	%rax, -48(%rbx)
	call	_ZN2v88internal8compiler14NodeProperties13IsControlEdgeENS1_4EdgeE@PLT
	testb	%al, %al
	je	.L301
	movq	40(%r12), %r8
	movq	32(%r12), %rdi
	movl	20(%r15), %eax
	movq	%r8, %rcx
	subq	%rdi, %rcx
	andl	$16777215, %eax
	sarq	$3, %rcx
	movq	%rcx, %r9
	cmpq	%rcx, %rax
	jnb	.L381
	cmpq	$0, (%rdi,%rax,8)
	je	.L301
	leaq	0(,%rax,8), %rcx
.L291:
	movq	(%rdi,%rcx), %rax
	testb	$1, 40(%rax)
	jne	.L301
.L296:
	testb	$2, 40(%rax)
	je	.L300
	cmpq	%r15, -16(%rbx)
	je	.L301
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler18ControlEquivalence13VisitBackedgeEPNS1_4NodeES4_NS2_12DFSDirectionE
	jmp	.L301
	.p2align 4,,10
	.p2align 3
.L376:
	movq	-264(%rbp), %rax
	movq	-8(%rax), %rbx
	addq	$504, %rbx
	jmp	.L279
	.p2align 4,,10
	.p2align 3
.L378:
	movl	20(%r13), %eax
.L302:
	movq	40(%r12), %rdi
	movq	32(%r12), %rcx
	andl	$16777215, %eax
	movq	%rdi, %rdx
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rax
	jnb	.L382
.L327:
	movq	(%rcx,%rax,8), %rax
	andb	$-3, 40(%rax)
	movq	40(%r12), %rdi
	movq	32(%r12), %rcx
	movl	20(%r13), %eax
	movq	%rdi, %rdx
	subq	%rcx, %rdx
	andl	$16777215, %eax
	sarq	$3, %rdx
	cmpq	%rdx, %rax
	jnb	.L383
.L329:
	movq	(%rcx,%rax,8), %rax
	orb	$1, 40(%rax)
	movq	-288(%rbp), %rax
	movq	-280(%rbp), %rdx
	cmpq	%rdx, %rax
	je	.L331
	subq	$56, %rax
	movq	%rax, -288(%rbp)
.L332:
	movl	-56(%rbx), %ecx
	movq	-16(%rbx), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler18ControlEquivalence9VisitPostEPNS1_4NodeES4_NS2_12DFSDirectionE
	movq	-288(%rbp), %rbx
	cmpq	%rbx, -320(%rbp)
	jne	.L277
	jmp	.L278
	.p2align 4,,10
	.p2align 3
.L305:
	leaq	16(%r15,%rax), %rax
	movq	(%r15), %r15
	jmp	.L306
	.p2align 4,,10
	.p2align 3
.L283:
	cmpq	$0, -32(%rbx)
	je	.L302
	movl	$1, -56(%rbx)
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler18ControlEquivalence8VisitMidEPNS1_4NodeENS2_12DFSDirectionE
	movq	-288(%rbp), %rbx
	cmpq	%rbx, -320(%rbp)
	jne	.L277
	jmp	.L278
	.p2align 4,,10
	.p2align 3
.L383:
	leaq	1(%rax), %rsi
	cmpq	%rsi, %rdx
	jb	.L384
	jbe	.L329
	leaq	(%rcx,%rsi,8), %rdx
	cmpq	%rdx, %rdi
	je	.L329
	movq	%rdx, 40(%r12)
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L382:
	leaq	1(%rax), %rsi
	cmpq	%rsi, %rdx
	jb	.L385
	jbe	.L327
	leaq	(%rcx,%rsi,8), %rdx
	cmpq	%rdx, %rdi
	je	.L327
	movq	%rdx, 40(%r12)
	jmp	.L327
	.p2align 4,,10
	.p2align 3
.L331:
	movq	-344(%rbp), %rax
	testq	%rax, %rax
	je	.L333
	cmpq	$9, 8(%rax)
	ja	.L334
.L333:
	movq	$9, 8(%rdx)
	movq	-344(%rbp), %rax
	movq	%rax, (%rdx)
	movq	%rdx, -344(%rbp)
.L334:
	movq	-264(%rbp), %rax
	leaq	-8(%rax), %rdx
	movq	%rdx, -264(%rbp)
	movq	-8(%rax), %rax
	leaq	504(%rax), %rdx
	movq	%rax, -280(%rbp)
	addq	$448, %rax
	movq	%rdx, -272(%rbp)
	movq	%rax, -288(%rbp)
	jmp	.L332
	.p2align 4,,10
	.p2align 3
.L304:
	movl	20(%r13), %eax
	leaq	32(%r13), %rcx
	movl	%eax, %edx
	shrl	$24, %edx
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L386
	movslq	%edx, %rdx
	leaq	(%rcx,%rdx,8), %rdx
	cmpq	%rdx, -40(%rbx)
	je	.L302
.L392:
	movl	$0, -56(%rbx)
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler18ControlEquivalence8VisitMidEPNS1_4NodeENS2_12DFSDirectionE
	movq	-288(%rbp), %rbx
	cmpq	%rbx, -320(%rbp)
	jne	.L277
	jmp	.L278
	.p2align 4,,10
	.p2align 3
.L381:
	leaq	1(%rax), %rsi
	cmpq	%rsi, %rcx
	jb	.L387
	jbe	.L289
	leaq	(%rdi,%rsi,8), %rcx
	cmpq	%rcx, %r8
	je	.L289
	movq	%rcx, 40(%r12)
.L288:
	cmpq	$0, (%rdi,%rax,8)
	je	.L301
	movq	40(%r12), %r8
	movl	20(%r15), %esi
	movq	%r8, %rax
	andl	$16777215, %esi
	subq	%rdi, %rax
	leaq	0(,%rsi,8), %rcx
	sarq	$3, %rax
	movq	%rax, %r9
	cmpq	%rax, %rsi
	jb	.L291
	addq	$1, %rsi
	cmpq	%rsi, %rax
	jnb	.L292
	movq	-376(%rbp), %rdi
	subq	%rax, %rsi
	movq	%rcx, -368(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler18ControlEquivalence8NodeDataENS1_13ZoneAllocatorIS5_EEE17_M_default_appendEm
	movq	32(%r12), %rdi
	movq	-368(%rbp), %rcx
.L293:
	movq	(%rdi,%rcx), %rax
	testb	$1, 40(%rax)
	jne	.L301
	movq	40(%r12), %r8
	movl	20(%r15), %eax
	movq	%r8, %r10
	andl	$16777215, %eax
	subq	%rdi, %r10
	leaq	0(,%rax,8), %rcx
	sarq	$3, %r10
	movq	%r10, %r9
	cmpq	%r10, %rax
	jb	.L388
	leaq	1(%rax), %rsi
	movq	%rax, -368(%rbp)
	cmpq	%rsi, %r10
	jnb	.L297
	movq	-376(%rbp), %rdi
	subq	%r10, %rsi
	call	_ZNSt6vectorIPN2v88internal8compiler18ControlEquivalence8NodeDataENS1_13ZoneAllocatorIS5_EEE17_M_default_appendEm
	movq	32(%r12), %rcx
	movq	-368(%rbp), %rax
	movq	(%rcx,%rax,8), %rax
	jmp	.L296
	.p2align 4,,10
	.p2align 3
.L379:
	leaq	1(%rdx), %rsi
	cmpq	%rsi, %rax
	jb	.L389
	ja	.L390
.L313:
	cmpq	$0, (%rcx,%rdx,8)
	leaq	0(,%rdx,8), %rax
	je	.L301
.L316:
	cmpq	%r8, %rsi
	jnb	.L318
	leaq	(%rcx,%rsi,8), %rdx
	cmpq	%rdi, %rdx
	je	.L318
	movq	%rdx, 40(%r12)
.L317:
	movq	(%rcx,%rax), %rax
	testb	$1, 40(%rax)
	jne	.L301
	movq	40(%r12), %rdi
	movl	20(%r15), %edx
	movq	%rdi, %r10
	andl	$16777215, %edx
	subq	%rcx, %r10
	leaq	0(,%rdx,8), %rax
	sarq	$3, %r10
	movq	%r10, %r8
	cmpq	%r10, %rdx
	jb	.L391
	leaq	1(%rdx), %rsi
	movq	%rdx, -368(%rbp)
	cmpq	%rsi, %r10
	jnb	.L321
	movq	-376(%rbp), %rdi
	subq	%r10, %rsi
	call	_ZNSt6vectorIPN2v88internal8compiler18ControlEquivalence8NodeDataENS1_13ZoneAllocatorIS5_EEE17_M_default_appendEm
	movq	32(%r12), %rax
	movq	-368(%rbp), %rdx
	movq	(%rax,%rdx,8), %rax
	.p2align 4,,10
	.p2align 3
.L320:
	testb	$2, 40(%rax)
	je	.L324
	cmpq	%r15, -16(%rbx)
	je	.L301
	movl	$1, %ecx
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler18ControlEquivalence13VisitBackedgeEPNS1_4NodeES4_NS2_12DFSDirectionE
	movq	-288(%rbp), %rbx
	cmpq	%rbx, -320(%rbp)
	jne	.L277
	jmp	.L278
	.p2align 4,,10
	.p2align 3
.L374:
	movdqa	-256(%rbp), %xmm1
	movdqa	-224(%rbp), %xmm2
	movq	$0, -336(%rbp)
	movq	-232(%rbp), %rax
	movdqa	-208(%rbp), %xmm3
	movaps	%xmm1, -352(%rbp)
	movdqa	-192(%rbp), %xmm1
	movaps	%xmm2, -320(%rbp)
	movdqa	-176(%rbp), %xmm2
	movq	%rax, -328(%rbp)
	movaps	%xmm3, -304(%rbp)
	movaps	%xmm1, -288(%rbp)
	movaps	%xmm2, -272(%rbp)
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L385:
	movq	-376(%rbp), %rdi
	subq	%rdx, %rsi
	movq	%rax, -368(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler18ControlEquivalence8NodeDataENS1_13ZoneAllocatorIS5_EEE17_M_default_appendEm
	movq	32(%r12), %rcx
	movq	-368(%rbp), %rax
	jmp	.L327
	.p2align 4,,10
	.p2align 3
.L384:
	movq	-376(%rbp), %rdi
	subq	%rdx, %rsi
	movq	%rax, -368(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler18ControlEquivalence8NodeDataENS1_13ZoneAllocatorIS5_EEE17_M_default_appendEm
	movq	32(%r12), %rcx
	movq	-368(%rbp), %rax
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L375:
	movq	(%rbx), %rax
	movl	20(%rbx), %esi
	leaq	.LC0(%rip), %rdi
	movq	8(%rax), %rdx
	andl	$16777215, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L386:
	movq	32(%r13), %rcx
	movl	8(%rcx), %edx
	addq	$16, %rcx
	movslq	%edx, %rdx
	leaq	(%rcx,%rdx,8), %rdx
	cmpq	%rdx, -40(%rbx)
	je	.L302
	jmp	.L392
	.p2align 4,,10
	.p2align 3
.L390:
	leaq	(%rcx,%rsi,8), %rax
	cmpq	%rax, %rdi
	je	.L313
	movq	%rax, 40(%r12)
.L312:
	cmpq	$0, (%rcx,%rdx,8)
	je	.L301
	movq	40(%r12), %rdi
	movl	20(%r15), %esi
	movq	%rdi, %rdx
	andl	$16777215, %esi
	subq	%rcx, %rdx
	leaq	0(,%rsi,8), %rax
	sarq	$3, %rdx
	movq	%rdx, %r8
	cmpq	%rdx, %rsi
	jb	.L315
	addq	$1, %rsi
	cmpq	%rsi, %rdx
	jnb	.L316
	movq	-376(%rbp), %rdi
	subq	%rdx, %rsi
	movq	%rax, -368(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler18ControlEquivalence8NodeDataENS1_13ZoneAllocatorIS5_EEE17_M_default_appendEm
	movq	32(%r12), %rcx
	movq	-368(%rbp), %rax
	jmp	.L317
.L387:
	movq	-376(%rbp), %rdi
	subq	%rcx, %rsi
	movq	%rax, -368(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler18ControlEquivalence8NodeDataENS1_13ZoneAllocatorIS5_EEE17_M_default_appendEm
	movq	32(%r12), %rdi
	movq	-368(%rbp), %rax
	jmp	.L288
	.p2align 4,,10
	.p2align 3
.L318:
	movq	(%rcx,%rax), %rdx
	testb	$1, 40(%rdx)
	jne	.L301
.L321:
	addq	%rcx, %rax
	cmpq	%r8, %rsi
	jnb	.L371
	leaq	(%rcx,%rsi,8), %rdx
	cmpq	%rdi, %rdx
	je	.L371
	movq	%rdx, 40(%r12)
.L371:
	movq	(%rax), %rax
	jmp	.L320
	.p2align 4,,10
	.p2align 3
.L389:
	movq	-376(%rbp), %rdi
	subq	%rax, %rsi
	movq	%rdx, -368(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler18ControlEquivalence8NodeDataENS1_13ZoneAllocatorIS5_EEE17_M_default_appendEm
	movq	32(%r12), %rcx
	movq	-368(%rbp), %rdx
	jmp	.L312
	.p2align 4,,10
	.p2align 3
.L289:
	cmpq	$0, (%rdi,%rax,8)
	leaq	0(,%rax,8), %rcx
	je	.L301
.L292:
	cmpq	%r9, %rsi
	jnb	.L294
	leaq	(%rdi,%rsi,8), %rax
	cmpq	%r8, %rax
	je	.L294
	movq	%rax, 40(%r12)
	jmp	.L293
.L294:
	movq	(%rdi,%rcx), %rax
	testb	$1, 40(%rax)
	jne	.L301
.L297:
	addq	%rdi, %rcx
	cmpq	%r9, %rsi
	jnb	.L370
	leaq	(%rdi,%rsi,8), %rax
	cmpq	%r8, %rax
	je	.L370
	movq	%rax, 40(%r12)
.L370:
	movq	(%rcx), %rax
	jmp	.L296
	.p2align 4,,10
	.p2align 3
.L324:
	movl	$1, %r8d
.L373:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler18ControlEquivalence7DFSPushERNS0_9ZoneStackINS2_13DFSStackEntryEEEPNS1_4NodeES8_NS2_12DFSDirectionE
	cmpb	$0, _ZN2v88internal20FLAG_trace_turbo_ceqE(%rip)
	je	.L301
	movq	(%r15), %rax
	movl	20(%r15), %esi
	leaq	.LC0(%rip), %rdi
	movq	8(%rax), %rdx
	andl	$16777215, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L301
.L300:
	xorl	%r8d, %r8d
	jmp	.L373
.L391:
	movq	(%rcx,%rdx,8), %rax
	jmp	.L320
.L380:
	call	__stack_chk_fail@PLT
.L388:
	movq	(%rdi,%rax,8), %rax
	jmp	.L296
	.cfi_endproc
.LFE10226:
	.size	_ZN2v88internal8compiler18ControlEquivalence16RunUndirectedDFSEPNS1_4NodeE, .-_ZN2v88internal8compiler18ControlEquivalence16RunUndirectedDFSEPNS1_4NodeE
	.section	.text._ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm,"axG",@progbits,_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	.type	_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm, @function
_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm:
.LFB12129:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$8, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	shrq	$6, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	1(%rsi), %rbx
	addq	$3, %rsi
	subq	$8, %rsp
	cmpq	$8, %rsi
	cmovb	%rax, %rsi
	movq	%rsi, 24(%rdi)
	movq	(%rdi), %rdi
	salq	$3, %rsi
	movq	16(%rdi), %rdx
	movq	24(%rdi), %rax
	subq	%rdx, %rax
	cmpq	%rax, %rsi
	ja	.L407
	addq	%rdx, %rsi
	movq	%rsi, 16(%rdi)
.L395:
	movq	24(%r12), %rax
	movq	%rdx, 16(%r12)
	subq	%rbx, %rax
	shrq	%rax
	leaq	(%rdx,%rax,8), %r15
	leaq	(%r15,%rbx,8), %r14
	cmpq	%r14, %r15
	jnb	.L396
	movq	%r15, %rbx
	jmp	.L401
	.p2align 4,,10
	.p2align 3
.L397:
	movq	(%r12), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L408
	leaq	512(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L398:
	movq	%rax, (%rbx)
	addq	$8, %rbx
	cmpq	%rbx, %r14
	jbe	.L396
.L401:
	movq	8(%r12), %rax
	testq	%rax, %rax
	je	.L397
	cmpq	$63, 8(%rax)
	jbe	.L397
	movq	(%rax), %rdx
	addq	$8, %rbx
	movq	%rdx, 8(%r12)
	movq	%rax, -8(%rbx)
	cmpq	%rbx, %r14
	ja	.L401
.L396:
	movq	%r15, 56(%r12)
	movq	(%r15), %rdx
	andl	$63, %r13d
	leaq	512(%rdx), %rax
	movq	%rdx, 40(%r12)
	movq	%rax, 48(%r12)
	leaq	-8(%r14), %rax
	movq	%rax, 88(%r12)
	movq	-8(%r14), %rax
	movq	%rdx, 32(%r12)
	movq	%rax, 72(%r12)
	leaq	512(%rax), %rcx
	leaq	(%rax,%r13,8), %rax
	movq	%rcx, 80(%r12)
	movq	%rax, 64(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L408:
	.cfi_restore_state
	movl	$512, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L398
	.p2align 4,,10
	.p2align 3
.L407:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L395
	.cfi_endproc
.LFE12129:
	.size	_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm, .-_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	.section	.text._ZN2v88internal8compiler18ControlEquivalence22DetermineParticipationEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18ControlEquivalence22DetermineParticipationEPNS1_4NodeE
	.type	_ZN2v88internal8compiler18ControlEquivalence22DetermineParticipationEPNS1_4NodeE, @function
_ZN2v88internal8compiler18ControlEquivalence22DetermineParticipationEPNS1_4NodeE:
.LFB10237:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	subq	$344, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	leaq	-256(%rbp), %rdi
	movq	$0, -248(%rbp)
	movq	$0, -240(%rbp)
	movq	%rax, -256(%rbp)
	movq	$0, -232(%rbp)
	movaps	%xmm0, -224(%rbp)
	movaps	%xmm0, -208(%rbp)
	movaps	%xmm0, -192(%rbp)
	movaps	%xmm0, -176(%rbp)
	call	_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	cmpq	$0, -240(%rbp)
	je	.L466
	movdqa	-256(%rbp), %xmm5
	pxor	%xmm0, %xmm0
	leaq	-160(%rbp), %rdi
	xorl	%esi, %esi
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm5, -160(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	$0, -144(%rbp)
	movq	$0, -136(%rbp)
	call	_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	movdqa	-96(%rbp), %xmm5
	movq	-216(%rbp), %r9
	movq	-224(%rbp), %xmm3
	movq	-200(%rbp), %r8
	movq	-192(%rbp), %xmm1
	movq	-184(%rbp), %rcx
	movaps	%xmm5, -192(%rbp)
	movq	%r9, %xmm5
	punpcklqdq	%xmm5, %xmm3
	movdqa	-128(%rbp), %xmm6
	movq	-208(%rbp), %xmm2
	movaps	%xmm3, -320(%rbp)
	movq	%r8, %xmm3
	movq	-168(%rbp), %rdx
	movdqa	-112(%rbp), %xmm7
	punpcklqdq	%xmm3, %xmm2
	movq	-248(%rbp), %rax
	movq	-176(%rbp), %xmm0
	movaps	%xmm6, -224(%rbp)
	movaps	%xmm2, -304(%rbp)
	movq	%rcx, %xmm2
	movq	-256(%rbp), %xmm4
	movq	-240(%rbp), %r11
	punpcklqdq	%xmm2, %xmm1
	movq	-232(%rbp), %r10
	movdqa	-80(%rbp), %xmm6
	movaps	%xmm7, -208(%rbp)
	movq	-144(%rbp), %rsi
	movq	-136(%rbp), %rdi
	movq	%rax, %xmm7
	movaps	%xmm1, -288(%rbp)
	movq	%rdx, %xmm1
	punpcklqdq	%xmm7, %xmm4
	movq	%r11, -336(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movq	%rsi, -240(%rbp)
	movq	%rdi, -232(%rbp)
	movq	%r10, -328(%rbp)
	movaps	%xmm6, -176(%rbp)
	movaps	%xmm4, -352(%rbp)
	movaps	%xmm0, -272(%rbp)
	testq	%rsi, %rsi
	je	.L411
	movq	-168(%rbp), %rcx
	movq	-200(%rbp), %rdx
	addq	$8, %rcx
	cmpq	%rdx, %rcx
	jbe	.L412
	.p2align 4,,10
	.p2align 3
.L415:
	testq	%rax, %rax
	je	.L413
	cmpq	$64, 8(%rax)
	ja	.L414
.L413:
	movq	(%rdx), %rax
	movq	$64, 8(%rax)
	movq	-248(%rbp), %rsi
	movq	%rsi, (%rax)
	movq	%rax, -248(%rbp)
.L414:
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	ja	.L415
	movq	-232(%rbp), %rdi
	movq	-240(%rbp), %rsi
.L412:
	leaq	0(,%rdi,8), %rax
	cmpq	$15, %rax
	jbe	.L411
	movq	%rdi, 8(%rsi)
	movq	$0, (%rsi)
.L411:
	leaq	-352(%rbp), %rax
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%rax, %rsi
	movq	%rax, -376(%rbp)
	call	_ZN2v88internal8compiler18ControlEquivalence29DetermineParticipationEnqueueERNS0_9ZoneQueueIPNS1_4NodeEEES5_
	movq	-320(%rbp), %rax
	cmpq	-288(%rbp), %rax
	je	.L417
	leaq	-160(%rbp), %rdi
	movq	%rdi, -384(%rbp)
	.p2align 4,,10
	.p2align 3
.L423:
	movq	-304(%rbp), %rsi
	movq	(%rax), %r15
	leaq	-8(%rsi), %rdx
	cmpq	%rdx, %rax
	je	.L418
	addq	$8, %rax
	movq	%rax, -320(%rbp)
.L419:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler14NodeProperties16PastControlIndexEPNS1_4NodeE@PLT
	movq	%r15, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler14NodeProperties15PastEffectIndexEPNS1_4NodeE@PLT
	cmpl	%eax, %ebx
	jle	.L437
	subl	$1, %ebx
	movslq	%eax, %rdx
	leaq	32(%r15), %r10
	subl	%eax, %ebx
	leaq	0(,%rdx,8), %r12
	leaq	1(%rdx,%rbx), %r8
	leaq	0(,%r8,8), %r13
	jmp	.L438
	.p2align 4,,10
	.p2align 3
.L468:
	leaq	(%r12,%r10), %rax
.L425:
	movq	(%rax), %rax
	movq	40(%r14), %rcx
	movq	32(%r14), %rdx
	movq	%rax, -160(%rbp)
	movl	20(%rax), %ebx
	movq	%rcx, %rax
	subq	%rdx, %rax
	andl	$16777215, %ebx
	sarq	$3, %rax
	movq	%rax, %r9
	leaq	0(,%rbx,8), %r8
	cmpq	%rax, %rbx
	jnb	.L467
	cmpq	$0, (%rdx,%rbx,8)
	je	.L432
.L431:
	addq	$8, %r12
	cmpq	%r12, %r13
	je	.L437
.L438:
	movzbl	23(%r15), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L468
	movq	(%r10), %rax
	leaq	16(%rax,%r12), %rax
	jmp	.L425
	.p2align 4,,10
	.p2align 3
.L467:
	leaq	1(%rbx), %rsi
	leaq	24(%r14), %rdi
	cmpq	%rsi, %rax
	jb	.L469
	jbe	.L429
	leaq	8(%rdx,%r8), %rax
	cmpq	%rax, %rcx
	je	.L429
	movq	%rax, 40(%r14)
.L428:
	cmpq	$0, (%rdx,%rbx,8)
	jne	.L431
	movq	-160(%rbp), %rax
	movq	40(%r14), %rcx
	movl	20(%rax), %esi
	movq	%rcx, %rax
	subq	%rdx, %rax
	andl	$16777215, %esi
	sarq	$3, %rax
	movq	%rax, %r9
	leaq	0(,%rsi,8), %r8
	cmpq	%rax, %rsi
	jb	.L432
	addq	$1, %rsi
	cmpq	%rsi, %rax
	jnb	.L433
	subq	%rax, %rsi
	movq	%r10, -368(%rbp)
	movq	%r8, -360(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler18ControlEquivalence8NodeDataENS1_13ZoneAllocatorIS5_EEE17_M_default_appendEm
	movq	-360(%rbp), %r8
	movq	-368(%rbp), %r10
	.p2align 4,,10
	.p2align 3
.L432:
	movq	(%r14), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$47, %rdx
	jbe	.L470
	leaq	48(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L435:
	movq	(%r14), %rdx
	andb	$-4, 40(%rax)
	movq	$-1, (%rax)
	movq	%rdx, 8(%rax)
	leaq	16(%rax), %rdx
	movq	%rdx, 24(%rax)
	movq	%rdx, 16(%rax)
	movq	$0, 32(%rax)
	movq	32(%r14), %rdx
	movq	%rax, (%rdx,%r8)
	movq	-272(%rbp), %rax
	movq	-288(%rbp), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L436
	movq	-160(%rbp), %rax
	addq	$8, %r12
	movq	%rax, (%rdx)
	addq	$8, -288(%rbp)
	cmpq	%r12, %r13
	jne	.L438
	.p2align 4,,10
	.p2align 3
.L437:
	movq	-320(%rbp), %rax
	cmpq	-288(%rbp), %rax
	jne	.L423
.L417:
	movq	-336(%rbp), %rax
	testq	%rax, %rax
	je	.L409
	movq	-264(%rbp), %rdi
	movq	-296(%rbp), %rdx
	leaq	8(%rdi), %rcx
	cmpq	%rdx, %rcx
	jbe	.L440
	movq	-344(%rbp), %rax
	.p2align 4,,10
	.p2align 3
.L443:
	testq	%rax, %rax
	je	.L441
	cmpq	$64, 8(%rax)
	ja	.L442
.L441:
	movq	(%rdx), %rax
	movq	$64, 8(%rax)
	movq	-344(%rbp), %rsi
	movq	%rsi, (%rax)
	movq	%rax, -344(%rbp)
.L442:
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	ja	.L443
	movq	-336(%rbp), %rax
.L440:
	movq	-328(%rbp), %rdx
	leaq	0(,%rdx,8), %rcx
	cmpq	$15, %rcx
	jbe	.L409
	movq	%rdx, 8(%rax)
	movq	$0, (%rax)
.L409:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L471
	addq	$344, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L429:
	.cfi_restore_state
	cmpq	$0, (%rdx,%rbx,8)
	jne	.L431
.L433:
	cmpq	%r9, %rsi
	jnb	.L432
	leaq	(%rdx,%rsi,8), %rax
	cmpq	%rcx, %rax
	je	.L432
	movq	%rax, 40(%r14)
	jmp	.L432
	.p2align 4,,10
	.p2align 3
.L469:
	subq	%rax, %rsi
	movq	%r10, -368(%rbp)
	movq	%rdi, -360(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler18ControlEquivalence8NodeDataENS1_13ZoneAllocatorIS5_EEE17_M_default_appendEm
	movq	32(%r14), %rdx
	movq	-360(%rbp), %rdi
	movq	-368(%rbp), %r10
	jmp	.L428
	.p2align 4,,10
	.p2align 3
.L436:
	movq	-384(%rbp), %rsi
	movq	-376(%rbp), %rdi
	movq	%r10, -360(%rbp)
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	movq	-360(%rbp), %r10
	jmp	.L431
	.p2align 4,,10
	.p2align 3
.L418:
	movq	-344(%rbp), %rax
	testq	%rax, %rax
	je	.L420
	cmpq	$64, 8(%rax)
	ja	.L421
.L420:
	movq	-312(%rbp), %rax
	movq	$64, 8(%rax)
	movq	-344(%rbp), %rdx
	movq	%rdx, (%rax)
	movq	%rax, -344(%rbp)
.L421:
	movq	-296(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, -296(%rbp)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, -312(%rbp)
	movq	%rdx, -304(%rbp)
	movq	%rax, -320(%rbp)
	jmp	.L419
	.p2align 4,,10
	.p2align 3
.L470:
	movl	$48, %esi
	movq	%r10, -368(%rbp)
	movq	%r8, -360(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-360(%rbp), %r8
	movq	-368(%rbp), %r10
	jmp	.L435
.L466:
	movdqa	-256(%rbp), %xmm6
	movdqa	-224(%rbp), %xmm7
	movq	$0, -336(%rbp)
	movq	-232(%rbp), %rax
	movdqa	-176(%rbp), %xmm1
	movaps	%xmm6, -352(%rbp)
	movdqa	-208(%rbp), %xmm6
	movaps	%xmm7, -320(%rbp)
	movdqa	-192(%rbp), %xmm7
	movq	%rax, -328(%rbp)
	movaps	%xmm6, -304(%rbp)
	movaps	%xmm7, -288(%rbp)
	movaps	%xmm1, -272(%rbp)
	jmp	.L411
.L471:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10237:
	.size	_ZN2v88internal8compiler18ControlEquivalence22DetermineParticipationEPNS1_4NodeE, .-_ZN2v88internal8compiler18ControlEquivalence22DetermineParticipationEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler18ControlEquivalence3RunEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18ControlEquivalence3RunEPNS1_4NodeE
	.type	_ZN2v88internal8compiler18ControlEquivalence3RunEPNS1_4NodeE, @function
_ZN2v88internal8compiler18ControlEquivalence3RunEPNS1_4NodeE:
.LFB10221:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	40(%rdi), %rcx
	movq	32(%rdi), %rdx
	movl	20(%rsi), %ebx
	movq	%rcx, %rax
	subq	%rdx, %rax
	andl	$16777215, %ebx
	sarq	$3, %rax
	cmpq	%rax, %rbx
	jnb	.L490
	movq	(%rdx,%rbx,8), %rax
	testq	%rax, %rax
	je	.L483
.L479:
	cmpq	$-1, (%rax)
	je	.L483
.L487:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L483:
	.cfi_restore_state
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler18ControlEquivalence22DetermineParticipationEPNS1_4NodeE
	popq	%rbx
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler18ControlEquivalence16RunUndirectedDFSEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L490:
	.cfi_restore_state
	leaq	1(%rbx), %rsi
	leaq	24(%rdi), %r14
	cmpq	%rsi, %rax
	jb	.L491
	movq	%rax, %r8
	leaq	0(,%rbx,8), %rdi
	jbe	.L476
	leaq	8(%rdx,%rdi), %rax
	cmpq	%rax, %rcx
	je	.L476
	movq	%rax, 40(%r12)
.L475:
	cmpq	$0, (%rdx,%rbx,8)
	je	.L483
	movq	40(%r12), %rcx
	movl	20(%r13), %ebx
	movq	%rcx, %rax
	andl	$16777215, %ebx
	subq	%rdx, %rax
	leaq	0(,%rbx,8), %rdi
	sarq	$3, %rax
	movq	%rax, %r8
	cmpq	%rax, %rbx
	jb	.L492
	leaq	1(%rbx), %rsi
	cmpq	%rsi, %rax
	jnb	.L480
	subq	%rax, %rsi
	movq	%r14, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler18ControlEquivalence8NodeDataENS1_13ZoneAllocatorIS5_EEE17_M_default_appendEm
	movq	32(%r12), %rax
	movq	(%rax,%rbx,8), %rax
	jmp	.L479
	.p2align 4,,10
	.p2align 3
.L476:
	cmpq	$0, (%rdx,%rbx,8)
	je	.L483
.L480:
	addq	%rdx, %rdi
	cmpq	%r8, %rsi
	jnb	.L489
	leaq	(%rdx,%rsi,8), %rax
	cmpq	%rcx, %rax
	je	.L489
	movq	%rax, 40(%r12)
.L489:
	movq	(%rdi), %rax
	cmpq	$-1, (%rax)
	jne	.L487
	jmp	.L483
	.p2align 4,,10
	.p2align 3
.L491:
	subq	%rax, %rsi
	movq	%r14, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler18ControlEquivalence8NodeDataENS1_13ZoneAllocatorIS5_EEE17_M_default_appendEm
	movq	32(%r12), %rdx
	jmp	.L475
.L492:
	movq	(%rdx,%rbx,8), %rax
	jmp	.L479
	.cfi_endproc
.LFE10221:
	.size	_ZN2v88internal8compiler18ControlEquivalence3RunEPNS1_4NodeE, .-_ZN2v88internal8compiler18ControlEquivalence3RunEPNS1_4NodeE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler18ControlEquivalence3RunEPNS1_4NodeE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler18ControlEquivalence3RunEPNS1_4NodeE, @function
_GLOBAL__sub_I__ZN2v88internal8compiler18ControlEquivalence3RunEPNS1_4NodeE:
.LFB12351:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE12351:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler18ControlEquivalence3RunEPNS1_4NodeE, .-_GLOBAL__sub_I__ZN2v88internal8compiler18ControlEquivalence3RunEPNS1_4NodeE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler18ControlEquivalence3RunEPNS1_4NodeE
	.globl	_ZN2v88internal8compiler18ControlEquivalence13kInvalidClassE
	.section	.rodata._ZN2v88internal8compiler18ControlEquivalence13kInvalidClassE,"a"
	.align 8
	.type	_ZN2v88internal8compiler18ControlEquivalence13kInvalidClassE, @object
	.size	_ZN2v88internal8compiler18ControlEquivalence13kInvalidClassE, 8
_ZN2v88internal8compiler18ControlEquivalence13kInvalidClassE:
	.quad	-1
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
