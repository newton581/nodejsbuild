	.file	"loop-peeling.cc"
	.text
	.section	.text._ZN2v88internal8compiler15PeeledIteration3mapEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15PeeledIteration3mapEPNS1_4NodeE
	.type	_ZN2v88internal8compiler15PeeledIteration3mapEPNS1_4NodeE, @function
_ZN2v88internal8compiler15PeeledIteration3mapEPNS1_4NodeE:
.LFB10577:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rcx
	movq	%rsi, %rax
	movq	8(%rdi), %rsi
	subq	%rsi, %rcx
	sarq	$3, %rcx
	je	.L2
	xorl	%edx, %edx
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L3:
	addq	$2, %rdx
	cmpq	%rcx, %rdx
	jnb	.L2
.L4:
	cmpq	%rax, (%rsi,%rdx,8)
	jne	.L3
	movq	8(%rsi,%rdx,8), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	ret
	.cfi_endproc
.LFE10577:
	.size	_ZN2v88internal8compiler15PeeledIteration3mapEPNS1_4NodeE, .-_ZN2v88internal8compiler15PeeledIteration3mapEPNS1_4NodeE
	.section	.rodata._ZN2v88internal8compiler10LoopPeeler7CanPeelEPNS1_8LoopTree4LoopE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.rodata._ZN2v88internal8compiler10LoopPeeler7CanPeelEPNS1_8LoopTree4LoopE.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"Cannot peel loop %i. Loop exit without explicit mark: Node %i (%s) is inside loop, but its use %i (%s) is outside.\n"
	.section	.text._ZN2v88internal8compiler10LoopPeeler7CanPeelEPNS1_8LoopTree4LoopE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler10LoopPeeler7CanPeelEPNS1_8LoopTree4LoopE
	.type	_ZN2v88internal8compiler10LoopPeeler7CanPeelEPNS1_8LoopTree4LoopE, @function
_ZN2v88internal8compiler10LoopPeeler7CanPeelEPNS1_8LoopTree4LoopE:
.LFB10578:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r10
	movslq	52(%rsi), %rax
	movq	112(%r10), %r8
	leaq	(%r8,%rax,8), %rcx
	movslq	48(%rsi), %rax
	leaq	(%r8,%rax,8), %rdi
	cmpq	%rdi, %rcx
	je	.L11
	movq	%rdi, %r14
	movq	%rdi, %rax
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L12:
	addq	$8, %rax
	cmpq	%rax, %rcx
	je	.L11
.L15:
	movq	(%rax), %r9
	movq	(%r9), %rdx
	cmpw	$1, 16(%rdx)
	jne	.L12
	movslq	60(%rsi), %rax
	leaq	(%r8,%rax,8), %rax
	movq	%rax, -56(%rbp)
	cmpq	%rdi, %rax
	je	.L33
.L14:
	movq	(%r14), %rdi
	movq	24(%rdi), %rax
	testq	%rax, %rax
	je	.L16
	movq	80(%r10), %r12
	movq	88(%r10), %r11
	leaq	40(%rdi), %r13
	subq	%r12, %r11
	sarq	$2, %r11
	.p2align 4,,10
	.p2align 3
.L28:
	movl	16(%rax), %ecx
	movl	%ecx, %edx
	shrl	%edx
	andl	$1, %ecx
	leaq	3(%rdx,%rdx,2), %rdx
	leaq	(%rax,%rdx,8), %rdx
	jne	.L17
	movq	(%rdx), %rdx
.L17:
	movl	20(%rdx), %r8d
	andl	$16777215, %r8d
	movl	%r8d, %ecx
	cmpq	%r11, %rcx
	jnb	.L18
	movl	(%r12,%rcx,4), %ecx
	testl	%ecx, %ecx
	jle	.L18
	subl	$1, %ecx
	movslq	%ecx, %rcx
	salq	$6, %rcx
	addq	48(%r10), %rcx
	jne	.L52
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L58:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L18
.L52:
	cmpq	%rcx, %rsi
	jne	.L58
.L19:
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L28
.L16:
	addq	$8, %r14
	cmpq	%r14, -56(%rbp)
	jne	.L14
.L33:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	.cfi_restore_state
	movq	(%rdi), %rbx
	movzwl	16(%rbx), %ecx
	cmpw	$52, %cx
	je	.L29
	subl	$53, %ecx
	cmpw	$1, %cx
	ja	.L30
	movzbl	23(%rdi), %ecx
	movq	%r13, %r15
	andl	$15, %ecx
	cmpl	$15, %ecx
	je	.L59
.L25:
	movq	(%r15), %rcx
	movzbl	23(%rcx), %r15d
	andl	$15, %r15d
	cmpl	$15, %r15d
	je	.L26
	addq	$40, %rcx
.L27:
	cmpq	%r9, (%rcx)
	setne	%cl
.L24:
	testb	%cl, %cl
	je	.L19
	movzbl	_ZN2v88internal21FLAG_trace_turbo_loopE(%rip), %eax
	testb	%al, %al
	jne	.L60
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_restore_state
	movq	(%rdx), %rcx
	cmpw	$18, 16(%rcx)
	setne	%cl
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L29:
	movzbl	23(%rdi), %ecx
	andl	$15, %ecx
	cmpl	$15, %ecx
	je	.L22
	movq	%r13, %rcx
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L26:
	movq	32(%rcx), %rcx
	addq	$24, %rcx
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L59:
	movq	32(%rdi), %rcx
	leaq	24(%rcx), %r15
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L22:
	movq	32(%rdi), %rcx
	addq	$24, %rcx
	jmp	.L27
.L60:
	movq	(%rdx), %rax
	movl	20(%r9), %esi
	movl	20(%rdi), %edx
	movq	8(%rbx), %rcx
	leaq	.LC1(%rip), %rdi
	movq	8(%rax), %r9
	andl	$16777215, %esi
	xorl	%eax, %eax
	andl	$16777215, %edx
	call	_ZN2v88internal6PrintFEPKcz@PLT
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L11:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE10578:
	.size	_ZN2v88internal8compiler10LoopPeeler7CanPeelEPNS1_8LoopTree4LoopE, .-_ZN2v88internal8compiler10LoopPeeler7CanPeelEPNS1_8LoopTree4LoopE
	.section	.rodata._ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_.str1.1,"aMS",@progbits,1
.LC2:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	.type	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_, @function
_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_:
.LFB11858:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r14
	movq	8(%rdi), %rbx
	movq	%r14, %rax
	subq	%rbx, %rax
	sarq	$3, %rax
	cmpq	$268435455, %rax
	je	.L99
	movq	%rsi, %r8
	movq	%rdi, %r13
	movq	%rsi, %r12
	movq	%rdx, %r15
	subq	%rbx, %r8
	testq	%rax, %rax
	je	.L77
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L100
	movl	$2147483640, %esi
	movl	$2147483640, %edx
.L63:
	movq	0(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	%rsi, %rcx
	jb	.L101
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L66:
	leaq	(%rax,%rdx), %rdi
	leaq	8(%rax), %rcx
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L100:
	testq	%rdx, %rdx
	jne	.L102
	movl	$8, %ecx
	xorl	%edi, %edi
	xorl	%eax, %eax
.L64:
	movq	(%r15), %rdx
	movq	%rdx, (%rax,%r8)
	cmpq	%rbx, %r12
	je	.L67
	leaq	-8(%r12), %r8
	leaq	15(%rax), %rdx
	subq	%rbx, %r8
	subq	%rbx, %rdx
	movq	%r8, %rcx
	shrq	$3, %rcx
	cmpq	$30, %rdx
	jbe	.L80
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rcx
	je	.L80
	leaq	1(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L69:
	movdqu	(%rbx,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L69
	movq	%rsi, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rcx
	addq	%rcx, %rbx
	addq	%rax, %rcx
	cmpq	%rdx, %rsi
	je	.L71
	movq	(%rbx), %rdx
	movq	%rdx, (%rcx)
.L71:
	leaq	16(%rax,%r8), %rcx
.L67:
	cmpq	%r14, %r12
	je	.L72
	subq	%r12, %r14
	leaq	15(%r12), %rdx
	leaq	-8(%r14), %r9
	subq	%rcx, %rdx
	movq	%r9, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rdx
	jbe	.L81
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rsi
	je	.L81
	leaq	1(%rsi), %r8
	xorl	%edx, %edx
	movq	%r8, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L74:
	movdqu	(%r12,%rdx), %xmm2
	movups	%xmm2, (%rcx,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rsi
	jne	.L74
	movq	%r8, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rsi
	leaq	(%rcx,%rsi), %r10
	addq	%rsi, %r12
	cmpq	%rdx, %r8
	je	.L76
	movq	(%r12), %rdx
	movq	%rdx, (%r10)
.L76:
	leaq	8(%rcx,%r9), %rcx
.L72:
	movq	%rax, %xmm0
	movq	%rcx, %xmm3
	movq	%rdi, 24(%r13)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 8(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L77:
	.cfi_restore_state
	movl	$8, %esi
	movl	$8, %edx
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L81:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L73:
	movq	(%r12,%rdx,8), %r8
	movq	%r8, (%rcx,%rdx,8)
	movq	%rdx, %r8
	addq	$1, %rdx
	cmpq	%rsi, %r8
	jne	.L73
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L80:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L68:
	movq	(%rbx,%rdx,8), %rsi
	movq	%rsi, (%rax,%rdx,8)
	movq	%rdx, %rsi
	addq	$1, %rdx
	cmpq	%rsi, %rcx
	jne	.L68
	jmp	.L71
.L101:
	movq	%r8, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %r8
	jmp	.L66
.L99:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L102:
	cmpq	$268435455, %rdx
	movl	$268435455, %eax
	cmova	%rax, %rdx
	salq	$3, %rdx
	movq	%rdx, %rsi
	jmp	.L63
	.cfi_endproc
.LFE11858:
	.size	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_, .-_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	.section	.text._ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_,"axG",@progbits,_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_
	.type	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_, @function
_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_:
.LFB11862:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	16(%rdi), %r13
	cmpq	24(%rdi), %r13
	je	.L104
	movq	(%rsi), %rax
	movq	%rax, 0(%r13)
	addq	$8, 16(%rdi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L104:
	.cfi_restore_state
	movq	8(%rdi), %r14
	movq	%r13, %rcx
	subq	%r14, %rcx
	movq	%rcx, %rax
	sarq	$3, %rax
	cmpq	$268435455, %rax
	je	.L129
	testq	%rax, %rax
	je	.L116
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L130
	movl	$2147483640, %esi
	movl	$2147483640, %r15d
.L107:
	movq	(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L131
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L110:
	leaq	(%rax,%r15), %rdi
	leaq	8(%rax), %rdx
.L108:
	movq	(%r12), %rsi
	movq	%rsi, (%rax,%rcx)
	cmpq	%r14, %r13
	je	.L111
	leaq	-8(%r13), %rdx
	movq	%rdx, %rcx
	leaq	15(%rax), %rdx
	subq	%r14, %rcx
	subq	%r14, %rdx
	movq	%rcx, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rdx
	jbe	.L119
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rsi
	je	.L119
	leaq	1(%rsi), %r8
	xorl	%edx, %edx
	movq	%r8, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L113:
	movdqu	(%r14,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rsi, %rdx
	jne	.L113
	movq	%r8, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rsi
	addq	%rsi, %r14
	addq	%rax, %rsi
	cmpq	%rdx, %r8
	je	.L115
	movq	(%r14), %rdx
	movq	%rdx, (%rsi)
.L115:
	leaq	16(%rax,%rcx), %rdx
.L111:
	movq	%rax, %xmm0
	movq	%rdx, %xmm2
	movq	%rdi, 24(%rbx)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 8(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L130:
	.cfi_restore_state
	testq	%rdx, %rdx
	jne	.L132
	movl	$8, %edx
	xorl	%edi, %edi
	xorl	%eax, %eax
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L116:
	movl	$8, %esi
	movl	$8, %r15d
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L119:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L112:
	movq	(%r14,%rdx,8), %r8
	movq	%r8, (%rax,%rdx,8)
	movq	%rdx, %r8
	addq	$1, %rdx
	cmpq	%r8, %rsi
	jne	.L112
	jmp	.L115
.L131:
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	jmp	.L110
.L129:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L132:
	movl	$268435455, %r15d
	cmpq	$268435455, %rdx
	cmova	%r15, %rdx
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rsi
	jmp	.L107
	.cfi_endproc
.LFE11862:
	.size	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_, .-_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_
	.section	.rodata._ZN2v88internal8compiler7Peeling9CopyNodesEPNS1_5GraphEPNS0_4ZoneEPNS1_4NodeENS_4base14iterator_rangeIPS8_EEPNS1_19SourcePositionTableEPNS1_15NodeOriginTableE.str1.1,"aMS",@progbits,1
.LC3:
	.string	""
.LC4:
	.string	"copy nodes"
	.section	.rodata._ZN2v88internal8compiler7Peeling9CopyNodesEPNS1_5GraphEPNS0_4ZoneEPNS1_4NodeENS_4base14iterator_rangeIPS8_EEPNS1_19SourcePositionTableEPNS1_15NodeOriginTableE.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"vector::_M_range_check: __n (which is %zu) >= this->size() (which is %zu)"
	.section	.text._ZN2v88internal8compiler7Peeling9CopyNodesEPNS1_5GraphEPNS0_4ZoneEPNS1_4NodeENS_4base14iterator_rangeIPS8_EEPNS1_19SourcePositionTableEPNS1_15NodeOriginTableE,"axG",@progbits,_ZN2v88internal8compiler7Peeling9CopyNodesEPNS1_5GraphEPNS0_4ZoneEPNS1_4NodeENS_4base14iterator_rangeIPS8_EEPNS1_19SourcePositionTableEPNS1_15NodeOriginTableE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler7Peeling9CopyNodesEPNS1_5GraphEPNS0_4ZoneEPNS1_4NodeENS_4base14iterator_rangeIPS8_EEPNS1_19SourcePositionTableEPNS1_15NodeOriginTableE
	.type	_ZN2v88internal8compiler7Peeling9CopyNodesEPNS1_5GraphEPNS0_4ZoneEPNS1_4NodeENS_4base14iterator_rangeIPS8_EEPNS1_19SourcePositionTableEPNS1_15NodeOriginTableE, @function
_ZN2v88internal8compiler7Peeling9CopyNodesEPNS1_5GraphEPNS0_4ZoneEPNS1_4NodeENS_4base14iterator_rangeIPS8_EEPNS1_19SourcePositionTableEPNS1_15NodeOriginTableE:
.LFB10572:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %rax
	movq	24(%rbp), %r15
	movq	%rsi, -208(%rbp)
	movq	%r9, -168(%rbp)
	movq	%rax, -144(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdx, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	%r8, -160(%rbp)
	movq	%r8, -136(%rbp)
	cmpq	%r8, %r9
	je	.L133
	.p2align 4,,10
	.p2align 3
.L155:
	movq	-136(%rbp), %rax
	movq	-144(%rbp), %r14
	movq	(%rax), %rbx
	movq	%r14, %rdi
	movq	%rbx, %rsi
	call	_ZNK2v88internal8compiler19SourcePositionTable17GetSourcePositionEPNS1_4NodeE@PLT
	movq	16(%r14), %rdx
	movq	%rdx, -104(%rbp)
	testb	$1, %al
	jne	.L135
	movq	%rax, %rcx
	movq	%rax, %rdx
	shrq	%rcx
	shrq	$31, %rdx
	andl	$1073741823, %ecx
	movzwl	%dx, %edx
	orl	%edx, %ecx
	je	.L136
.L135:
	movq	-144(%rbp), %rdi
	movq	%rax, 16(%rdi)
.L136:
	testq	%r15, %r15
	je	.L174
	movq	16(%r15), %rax
	movq	48(%r15), %rdx
	leaq	.LC4(%rip), %rdi
	movq	%rax, -192(%rbp)
	movq	24(%r15), %rax
	movq	%rax, -176(%rbp)
	movl	32(%r15), %eax
	movl	%eax, -196(%rbp)
	movq	40(%r15), %rax
	movq	%rax, -184(%rbp)
	movl	20(%rbx), %eax
	movq	%rdx, 16(%r15)
	andl	$16777215, %eax
	movq	%rdi, 24(%r15)
	movl	$1, 32(%r15)
	movq	%rax, 40(%r15)
.L137:
	movq	-88(%rbp), %rcx
	cmpq	-80(%rbp), %rcx
	je	.L138
	movq	%rcx, -80(%rbp)
.L138:
	movzbl	23(%rbx), %edx
	leaq	32(%rbx), %rax
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L139
	movl	%edx, %esi
	leaq	(%rax,%rsi,8), %rdi
	cmpq	%rdi, %rax
	je	.L146
.L140:
	movq	%rbx, -152(%rbp)
	leaq	-120(%rbp), %r14
	movq	%r13, %rbx
	movq	%rdi, %r12
	movq	%rax, %r13
	.p2align 4,,10
	.p2align 3
.L145:
	movq	0(%r13), %rdx
	movl	(%rbx), %esi
	movl	16(%rdx), %eax
	cmpl	%esi, %eax
	jb	.L143
	subl	%esi, %eax
	movl	%eax, %esi
	je	.L143
	movq	8(%rbx), %rdx
	movq	8(%rdx), %rax
	movq	16(%rdx), %rdx
	subq	%rax, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L202
	movq	(%rax,%rsi,8), %rdx
.L143:
	leaq	-96(%rbp), %rdi
	movq	%r14, %rsi
	addq	$8, %r13
	movq	%rdx, -120(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_
	cmpq	%r13, %r12
	jne	.L145
	movq	%rbx, %r13
	movq	-152(%rbp), %rbx
	movq	-88(%rbp), %rcx
	movzbl	23(%rbx), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L203
.L146:
	movq	(%rbx), %rsi
	movq	-208(%rbp), %rdi
	xorl	%r8d, %r8d
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L147
	movq	%rdx, 8(%rax)
.L147:
	movq	8(%r13), %rdx
	movq	%rax, -120(%rbp)
	movq	%rbx, -128(%rbp)
	movq	16(%rdx), %rax
	subq	8(%rdx), %rax
	movl	0(%r13), %edx
	sarq	$3, %rax
	leal	1(%rdx,%rax), %eax
	movl	%eax, 16(%rbx)
	movq	8(%r13), %rdi
	movq	16(%rdi), %rsi
	cmpq	24(%rdi), %rsi
	je	.L148
	movq	%rbx, (%rsi)
	addq	$8, 16(%rdi)
.L149:
	movq	8(%r13), %rdi
	movq	16(%rdi), %rsi
	cmpq	24(%rdi), %rsi
	je	.L150
	movq	-120(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 16(%rdi)
.L151:
	testq	%r15, %r15
	je	.L152
	movq	-192(%rbp), %xmm0
	movl	-196(%rbp), %eax
	movl	%eax, 32(%r15)
	movq	-184(%rbp), %rax
	movhps	-176(%rbp), %xmm0
	movups	%xmm0, 16(%r15)
	movq	%rax, 40(%r15)
.L152:
	movq	-104(%rbp), %rax
	movq	-144(%rbp), %rbx
	addq	$8, -136(%rbp)
	movq	%rax, 16(%rbx)
	movq	-136(%rbp), %rax
	cmpq	%rax, -168(%rbp)
	jne	.L155
	movq	%r13, %r15
	.p2align 4,,10
	.p2align 3
.L172:
	movq	-160(%rbp), %rax
	movl	(%r15), %ecx
	movq	8(%r15), %rdx
	movq	(%rax), %rbx
	movl	16(%rbx), %eax
	movl	%eax, %esi
	subl	%ecx, %esi
	cmpl	%eax, %ecx
	movl	$0, %eax
	cmova	%rax, %rsi
	movq	8(%rdx), %rax
	movq	16(%rdx), %rdx
	subq	%rax, %rdx
	sarq	$3, %rdx
	cmpq	%rsi, %rdx
	jbe	.L202
	movq	(%rax,%rsi,8), %r14
	leaq	32(%rbx), %rax
	xorl	%ecx, %ecx
	movq	%rax, -136(%rbp)
	movl	20(%r14), %edi
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L205:
	addq	%rsi, %rdx
.L162:
	movq	(%rdx), %r13
	movl	(%r15), %r9d
	movl	16(%r13), %edx
	cmpl	%r9d, %edx
	jb	.L163
	subl	%r9d, %edx
	movl	%edx, %r9d
	je	.L163
	movq	8(%r15), %rdx
	movq	8(%rdx), %r11
	movq	16(%rdx), %rdx
	subq	%r11, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %r9
	jnb	.L204
	movq	(%r11,%r9,8), %r13
.L163:
	leaq	32(%r14), %rdx
	cmpl	$15, %eax
	je	.L165
	movq	32(%r14,%rcx,8), %r9
	addq	%rsi, %rdx
	movq	%r14, %r11
	leaq	1(%rcx), %r12
	cmpq	%r9, %r13
	je	.L167
.L166:
	leaq	1(%rcx), %r12
	leaq	0(,%r12,4), %rcx
	movq	%r12, %rax
	subq	%rcx, %rax
	leaq	(%r11,%rax,8), %rsi
	testq	%r9, %r9
	je	.L168
	movq	%r9, %rdi
	movq	%rdx, -152(%rbp)
	movq	%rsi, -144(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-152(%rbp), %rdx
	movq	-144(%rbp), %rsi
.L168:
	movq	%r13, (%rdx)
	testq	%r13, %r13
	je	.L200
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L200:
	movl	20(%r14), %edi
.L167:
	movq	%r12, %rcx
.L170:
	movl	%edi, %eax
	shrl	$24, %eax
	andl	$15, %eax
	movl	%eax, %edx
	cmpl	$15, %eax
	jne	.L159
	movq	32(%r14), %rdx
	movl	8(%rdx), %edx
.L159:
	cmpl	%ecx, %edx
	jle	.L160
	movzbl	23(%rbx), %edx
	leaq	0(,%rcx,8), %rsi
	andl	$15, %edx
	cmpl	$15, %edx
	movq	-136(%rbp), %rdx
	jne	.L205
	movq	(%rdx), %rdx
	leaq	16(%rdx,%rsi), %rdx
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L165:
	movq	32(%r14), %r11
	leaq	1(%rcx), %r12
	leaq	16(%r11,%rsi), %rdx
	movq	(%rdx), %r9
	cmpq	%r9, %r13
	jne	.L166
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L160:
	addq	$8, -160(%rbp)
	movq	-160(%rbp), %rax
	cmpq	%rax, -168(%rbp)
	jne	.L172
.L133:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L206
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L139:
	.cfi_restore_state
	movq	32(%rbx), %rdx
	movslq	8(%rdx), %rsi
	leaq	16(%rdx), %rax
	leaq	(%rax,%rsi,8), %rdi
	cmpq	%rdi, %rax
	jne	.L140
	movl	8(%rdx), %edx
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L203:
	movq	32(%rbx), %rdx
	movl	8(%rdx), %edx
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L174:
	movabsq	$-9223372036854775808, %rax
	movq	%rax, -184(%rbp)
	leaq	.LC3(%rip), %rax
	movq	%rax, -176(%rbp)
	movq	%rax, -192(%rbp)
	jmp	.L137
.L150:
	leaq	-120(%rbp), %rdx
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L151
.L148:
	leaq	-128(%rbp), %rdx
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L149
.L204:
	movq	%r9, %rsi
.L202:
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L206:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10572:
	.size	_ZN2v88internal8compiler7Peeling9CopyNodesEPNS1_5GraphEPNS0_4ZoneEPNS1_4NodeENS_4base14iterator_rangeIPS8_EEPNS1_19SourcePositionTableEPNS1_15NodeOriginTableE, .-_ZN2v88internal8compiler7Peeling9CopyNodesEPNS1_5GraphEPNS0_4ZoneEPNS1_4NodeENS_4base14iterator_rangeIPS8_EEPNS1_19SourcePositionTableEPNS1_15NodeOriginTableE
	.section	.text._ZN2v88internal8compiler10LoopPeeler4PeelEPNS1_8LoopTree4LoopE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler10LoopPeeler4PeelEPNS1_8LoopTree4LoopE
	.type	_ZN2v88internal8compiler10LoopPeeler4PeelEPNS1_8LoopTree4LoopE, @function
_ZN2v88internal8compiler10LoopPeeler4PeelEPNS1_8LoopTree4LoopE:
.LFB10579:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -152(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler10LoopPeeler7CanPeelEPNS1_8LoopTree4LoopE
	testb	%al, %al
	je	.L308
	movq	24(%r12), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$31, %rdx
	jbe	.L370
	leaq	32(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L210:
	movq	-152(%rbp), %r15
	movq	%rax, %rbx
	movq	%rax, -184(%rbp)
	leaq	-112(%rbp), %r14
	movq	24(%r12), %rax
	movq	$0, 8(%rbx)
	movq	%r14, %rdi
	movq	$0, 16(%rbx)
	movq	%rax, (%rbx)
	movq	$0, 24(%rbx)
	movl	60(%r15), %eax
	subl	48(%r15), %eax
	movq	(%r12), %rsi
	cltq
	leaq	5(%rax,%rax), %rdx
	call	_ZN2v88internal8compiler14NodeMarkerBaseC2EPNS1_5GraphEj@PLT
	movq	8(%r12), %rdi
	movq	(%r12), %r13
	movq	%rbx, -104(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4DeadEv@PLT
	xorl	%edx, %edx
	movq	%r13, %rdi
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movslq	52(%r15), %rdx
	movq	%rax, -136(%rbp)
	movq	16(%r12), %rax
	movq	112(%rax), %rax
	leaq	(%rax,%rdx,8), %r13
	movslq	48(%r15), %rdx
	leaq	(%rax,%rdx,8), %r15
	cmpq	%r13, %r15
	je	.L211
	leaq	-120(%rbp), %rbx
	.p2align 4,,10
	.p2align 3
.L218:
	movq	(%r15), %rdx
	movzbl	23(%rdx), %eax
	movq	32(%rdx), %rcx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L212
	movq	16(%rcx), %rcx
.L212:
	movq	-104(%rbp), %rdi
	movq	%rcx, -96(%rbp)
	movq	%rdx, -120(%rbp)
	movl	-112(%rbp), %ecx
	movq	16(%rdi), %rax
	subq	8(%rdi), %rax
	sarq	$3, %rax
	leal	1(%rcx,%rax), %eax
	movl	%eax, 16(%rdx)
	movq	16(%rdi), %rsi
	cmpq	24(%rdi), %rsi
	je	.L213
	movq	%rdx, (%rsi)
	addq	$8, 16(%rdi)
	movq	-104(%rbp), %rdi
	movq	16(%rdi), %rsi
	cmpq	24(%rdi), %rsi
	je	.L215
.L373:
	movq	-96(%rbp), %rax
	addq	$8, %r15
	movq	%rax, (%rsi)
	addq	$8, 16(%rdi)
	cmpq	%r15, %r13
	jne	.L218
.L216:
	movq	16(%r12), %rax
	movq	-152(%rbp), %rbx
	movq	112(%rax), %rax
	movslq	52(%rbx), %rdx
	leaq	(%rax,%rdx,8), %r13
.L211:
	movq	-152(%rbp), %rbx
	movq	24(%r12), %rdx
	movq	%r13, %r8
	movq	%r14, %rdi
	movslq	56(%rbx), %rcx
	pushq	40(%r12)
	pushq	32(%r12)
	leaq	(%rax,%rcx,8), %r9
	movq	(%r12), %rsi
	movq	-136(%rbp), %rcx
	call	_ZN2v88internal8compiler7Peeling9CopyNodesEPNS1_5GraphEPNS0_4ZoneEPNS1_4NodeENS_4base14iterator_rangeIPS8_EEPNS1_19SourcePositionTableEPNS1_15NodeOriginTableE
	movq	16(%r12), %rax
	movslq	52(%rbx), %rdx
	movq	%rbx, %rcx
	movq	112(%rax), %rax
	leaq	(%rax,%rdx,8), %rbx
	movslq	48(%rcx), %rdx
	leaq	(%rax,%rdx,8), %r13
	popq	%rax
	popq	%rdx
	cmpq	%r13, %rbx
	je	.L219
	movq	%r13, %rax
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L220:
	addq	$8, %rax
	cmpq	%rax, %rbx
	je	.L219
.L223:
	movq	(%rax), %r15
	movq	(%r15), %rdx
	cmpw	$1, 16(%rdx)
	jne	.L220
	movzbl	23(%r15), %eax
	andl	$15, %eax
	movl	%eax, -172(%rbp)
	cmpl	$15, %eax
	je	.L371
.L224:
	movl	-172(%rbp), %eax
	subl	$1, %eax
	movl	%eax, -176(%rbp)
	cmpl	$1, %eax
	jle	.L225
	movq	24(%r12), %rax
	xorl	%r13d, %r13d
	leaq	40(%r15), %rbx
	movq	$0, -88(%rbp)
	movq	$0, -80(%rbp)
	movq	%rax, -96(%rbp)
	movq	$0, -72(%rbp)
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L372:
	leal	1(%r13), %edx
	cmpl	%edx, %eax
	jle	.L228
	leaq	(%rbx,%r13,8), %rax
.L230:
	movq	(%rax), %rdx
	movl	-112(%rbp), %ecx
	movl	16(%rdx), %eax
	cmpl	%ecx, %eax
	jb	.L231
	subl	%ecx, %eax
	movl	%eax, %esi
	je	.L231
	movq	-104(%rbp), %rdx
	movq	8(%rdx), %rax
	movq	16(%rdx), %rdx
	subq	%rax, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L369
	movq	(%rax,%rsi,8), %rdx
.L231:
	leaq	-120(%rbp), %rsi
	leaq	-96(%rbp), %rdi
	movq	%rdx, -120(%rbp)
	addq	$1, %r13
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_
.L233:
	movzbl	23(%r15), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L372
	movq	32(%r15), %rax
	leal	1(%r13), %edx
	cmpl	%edx, 8(%rax)
	jle	.L228
	leaq	24(%rax,%r13,8), %rax
	jmp	.L230
	.p2align 4,,10
	.p2align 3
.L213:
	movq	%rbx, %rdx
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	movq	-104(%rbp), %rdi
	movq	16(%rdi), %rsi
	cmpq	24(%rdi), %rsi
	jne	.L373
	.p2align 4,,10
	.p2align 3
.L215:
	leaq	-96(%rbp), %rdx
	addq	$8, %r15
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	cmpq	%r15, %r13
	jne	.L218
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L225:
	movq	%r15, -136(%rbp)
	movl	-112(%rbp), %esi
	movq	%r12, -144(%rbp)
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L376:
	movq	8(%r12), %r14
	movl	16(%r14), %edx
	cmpl	%esi, %edx
	jb	.L256
	subl	%esi, %edx
	movl	%edx, %r8d
	je	.L256
.L302:
	movq	-104(%rbp), %rdx
	movq	8(%rdx), %rdi
	movq	16(%rdx), %rdx
	subq	%rdi, %rdx
	sarq	$3, %rdx
	cmpq	%r8, %rdx
	jbe	.L374
	movq	(%rdi,%r8,8), %r14
	movq	(%r12), %rdi
	cmpl	$15, %ecx
	je	.L300
.L366:
	cmpq	%rdi, %r14
	je	.L260
	leaq	-24(%rax), %r15
	testq	%rdi, %rdi
	je	.L261
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L261:
	movq	%r14, (%r12)
	testq	%r14, %r14
	je	.L367
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L367:
	movl	-112(%rbp), %esi
.L260:
	addq	$8, %r13
	cmpq	%r13, %rbx
	je	.L375
.L263:
	movq	0(%r13), %rax
	movzbl	23(%rax), %ecx
	leaq	32(%rax), %r12
	andl	$15, %ecx
	cmpl	$15, %ecx
	jne	.L376
	movq	32(%rax), %rdi
	movq	24(%rdi), %r14
	leaq	16(%rdi), %r8
	movl	16(%r14), %edx
	cmpl	%esi, %edx
	jb	.L377
	subl	%esi, %edx
	movl	%edx, %r8d
	jne	.L302
	.p2align 4,,10
	.p2align 3
.L300:
	leaq	16(%rdi), %r12
	movq	%rdi, %rax
.L256:
	movq	(%r12), %rdi
	jmp	.L366
	.p2align 4,,10
	.p2align 3
.L371:
	movq	32(%r15), %rax
	movl	8(%rax), %eax
	movl	%eax, -172(%rbp)
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L228:
	movl	-176(%rbp), %ebx
	movq	8(%r12), %rdi
	movq	(%r12), %r13
	movq	-88(%rbp), %r14
	movl	%ebx, %esi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
	movl	%ebx, %edx
	movq	%r13, %rdi
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movq	%r14, %rcx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-152(%rbp), %rbx
	movq	%rax, -128(%rbp)
	movq	%rax, %r13
	movq	16(%r12), %rax
	movslq	52(%rbx), %rdx
	movq	112(%rax), %rax
	leaq	(%rax,%rdx,8), %rdi
	movslq	48(%rbx), %rdx
	movq	%rdi, -160(%rbp)
	leaq	(%rax,%rdx,8), %rax
	cmpq	%rax, %rdi
	je	.L234
	movq	%rax, -144(%rbp)
	movl	-172(%rbp), %eax
	leaq	-120(%rbp), %r14
	movq	%r15, -200(%rbp)
	subl	$2, %eax
	movq	%r12, -192(%rbp)
	leaq	16(,%rax,8), %rax
	movq	%rax, -136(%rbp)
	.p2align 4,,10
	.p2align 3
.L251:
	movq	-144(%rbp), %rax
	movq	(%rax), %rbx
	movq	(%rbx), %rax
	cmpw	$1, 16(%rax)
	je	.L236
	movq	-88(%rbp), %rax
	cmpq	-80(%rbp), %rax
	je	.L237
	movq	%rax, -80(%rbp)
.L237:
	leaq	32(%rbx), %r12
	movl	$8, %r13d
	movq	%r12, -168(%rbp)
	movq	%r13, %r15
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L379:
	leaq	(%r15,%r12), %rax
.L239:
	movq	(%rax), %rdx
	movl	-112(%rbp), %ecx
	movl	16(%rdx), %eax
	cmpl	%ecx, %eax
	jb	.L240
	subl	%ecx, %eax
	movl	%eax, %esi
	je	.L240
	movq	-104(%rbp), %rax
	movq	8(%rax), %rcx
	movq	16(%rax), %rdx
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L369
	movq	(%rcx,%rsi,8), %rdx
.L240:
	leaq	-96(%rbp), %r13
	movq	%r14, %rsi
	movq	%rdx, -120(%rbp)
	addq	$8, %r15
	movq	%r13, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_
	cmpq	%r15, -136(%rbp)
	je	.L378
.L242:
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L379
	movq	(%r12), %rax
	leaq	16(%rax,%r15), %rax
	jmp	.L239
	.p2align 4,,10
	.p2align 3
.L378:
	movq	-88(%rbp), %rax
	movq	-80(%rbp), %rsi
	movq	%r13, %r15
	cmpq	%rax, %rsi
	je	.L236
	movq	(%rax), %rdx
	jmp	.L250
	.p2align 4,,10
	.p2align 3
.L244:
	addq	$8, %rax
	cmpq	%rax, %rsi
	je	.L236
.L250:
	cmpq	(%rax), %rdx
	je	.L244
	cmpq	-72(%rbp), %rsi
	je	.L245
	movq	-128(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, -80(%rbp)
.L246:
	movq	-192(%rbp), %r15
	movl	-176(%rbp), %edx
	movq	(%rbx), %rsi
	movq	8(%r15), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder16ResizeMergeOrPhiEPKNS1_8OperatorEi@PLT
	movq	-88(%rbp), %rcx
	movq	(%r15), %rdi
	xorl	%r8d, %r8d
	movl	-172(%rbp), %edx
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r8
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L247
	movq	(%r12), %rdi
	cmpq	%rdi, %r8
	je	.L236
.L248:
	leaq	-24(%rbx), %r12
	testq	%rdi, %rdi
	je	.L249
	movq	%r12, %rsi
	movq	%r8, -208(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-208(%rbp), %r8
.L249:
	movq	-168(%rbp), %rax
	movq	%r8, (%rax)
	testq	%r8, %r8
	je	.L236
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	.p2align 4,,10
	.p2align 3
.L236:
	addq	$8, -144(%rbp)
	movq	-144(%rbp), %rax
	cmpq	%rax, -160(%rbp)
	jne	.L251
	movq	-200(%rbp), %r15
	movq	-192(%rbp), %r12
	movq	-128(%rbp), %r13
.L234:
	movzbl	23(%r15), %eax
	leaq	32(%r15), %rbx
	movq	(%rbx), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L296
.L266:
	cmpq	%rdi, %r13
	je	.L271
.L270:
	subq	$24, %r15
	testq	%rdi, %rdi
	je	.L273
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L273:
	movq	%r13, (%rbx)
	testq	%r13, %r13
	je	.L271
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L271:
	movq	-152(%rbp), %rcx
	movq	16(%r12), %rax
	movslq	60(%rcx), %rdx
	movq	112(%rax), %rax
	leaq	(%rax,%rdx,8), %rbx
	movslq	56(%rcx), %rdx
	leaq	(%rax,%rdx,8), %r14
	cmpq	%r14, %rbx
	jne	.L292
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L381:
	cmpw	$52, %ax
	je	.L380
.L276:
	addq	$8, %r14
	cmpq	%r14, %rbx
	je	.L207
.L292:
	movq	(%r14), %r13
	movq	0(%r13), %rax
	movzwl	16(%rax), %eax
	cmpw	$53, %ax
	je	.L274
	cmpw	$54, %ax
	jne	.L381
	movzbl	23(%r13), %eax
	movq	32(%r13), %rcx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L289
	movq	16(%rcx), %rcx
.L289:
	movl	16(%rcx), %eax
	movl	-112(%rbp), %edx
	cmpl	%edx, %eax
	jb	.L290
	subl	%edx, %eax
	movl	%eax, %esi
	je	.L290
	movq	-104(%rbp), %rdx
	movq	8(%rdx), %rax
	movq	16(%rdx), %rdx
	subq	%rax, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L369
	movq	(%rax,%rsi,8), %rcx
.L290:
	movq	(%r12), %rax
	movl	$1, %edx
	movq	%r13, %rdi
	addq	$8, %r14
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	8(%r12), %rdi
	movl	$2, %esi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9EffectPhiEi@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	cmpq	%r14, %rbx
	jne	.L292
	.p2align 4,,10
	.p2align 3
.L207:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L382
	movq	-184(%rbp), %rax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L274:
	.cfi_restore_state
	movzbl	23(%r13), %eax
	movq	32(%r13), %rcx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L286
	movq	16(%rcx), %rcx
.L286:
	movl	16(%rcx), %eax
	movl	-112(%rbp), %edx
	cmpl	%edx, %eax
	jb	.L287
	subl	%edx, %eax
	movl	%eax, %esi
	je	.L287
	movq	-104(%rbp), %rdx
	movq	8(%rdx), %rax
	movq	16(%rdx), %rdx
	subq	%rax, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L369
	movq	(%rax,%rsi,8), %rcx
.L287:
	movq	(%r12), %rax
	movq	%r13, %rdi
	movl	$1, %edx
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	8(%r12), %rdi
	movl	$2, %edx
	movl	$8, %esi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L380:
	movzbl	23(%r13), %edx
	movq	32(%r13), %r15
	leaq	32(%r13), %rax
	movl	-112(%rbp), %ecx
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L277
	movl	16(%r15), %edx
	cmpl	%ecx, %edx
	jb	.L278
	subl	%ecx, %edx
	movl	%edx, %esi
	je	.L278
	movq	-104(%rbp), %rdx
	movq	8(%rdx), %rcx
	movq	16(%rdx), %rdx
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L369
	movq	(%rcx,%rsi,8), %r15
.L278:
	movq	8(%rax), %rdi
	cmpq	%rdi, %r15
	je	.L284
	addq	$8, %rax
	movq	%r13, %rsi
.L283:
	subq	$48, %rsi
	testq	%rdi, %rdi
	je	.L285
	movq	%rax, -144(%rbp)
	movq	%rsi, -136(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-144(%rbp), %rax
	movq	-136(%rbp), %rsi
.L285:
	movq	%r15, (%rax)
	testq	%r15, %r15
	je	.L284
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L284:
	movq	8(%r12), %rdi
	movl	$2, %esi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L277:
	movq	16(%r15), %r8
	leaq	16(%r15), %rax
	movl	16(%r8), %edx
	cmpl	%ecx, %edx
	jb	.L280
	subl	%ecx, %edx
	movl	%edx, %esi
	je	.L280
	movq	-104(%rbp), %rdx
	movq	8(%rdx), %rcx
	movq	16(%rdx), %rdx
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L369
	movq	(%rcx,%rsi,8), %r8
.L280:
	movq	8(%rax), %rdi
	cmpq	%rdi, %r8
	je	.L284
	movq	%r15, %rsi
	addq	$8, %rax
	movq	%r8, %r15
	jmp	.L283
	.p2align 4,,10
	.p2align 3
.L247:
	movq	(%r12), %rbx
	movq	16(%rbx), %rdi
	leaq	16(%rbx), %rax
	movq	%rax, -168(%rbp)
	cmpq	%rdi, %r8
	jne	.L248
	jmp	.L236
	.p2align 4,,10
	.p2align 3
.L308:
	movq	$0, -184(%rbp)
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L375:
	movq	-136(%rbp), %r15
	movq	-144(%rbp), %r12
	movzbl	23(%r15), %eax
	leaq	32(%r15), %rbx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L264
	movq	8(%rbx), %r13
	movl	16(%r13), %edx
	cmpl	%esi, %edx
	jb	.L299
	subl	%esi, %edx
	movl	%edx, %esi
	je	.L299
.L298:
	movq	-104(%rbp), %rdx
	movq	8(%rdx), %rcx
	movq	16(%rdx), %rdx
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rsi, %rdx
	jbe	.L369
	movq	(%rcx,%rsi,8), %r13
	movq	(%rbx), %rdi
	cmpl	$15, %eax
	jne	.L266
.L296:
	leaq	16(%rdi), %rbx
	movq	%rdi, %r15
.L267:
	movq	(%rbx), %rdi
	cmpq	%r13, %rdi
	jne	.L270
	jmp	.L271
	.p2align 4,,10
	.p2align 3
.L377:
	movq	%r8, %r12
	movq	%rdi, %rax
	movq	(%r12), %rdi
	jmp	.L366
.L264:
	movq	32(%r15), %rdi
	movq	24(%rdi), %r13
	leaq	16(%rdi), %rcx
	movl	16(%r13), %edx
	cmpl	%esi, %edx
	jb	.L383
	subl	%esi, %edx
	movl	%edx, %esi
	jne	.L298
	jmp	.L296
	.p2align 4,,10
	.p2align 3
.L245:
	leaq	-128(%rbp), %rdx
	movq	%r15, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L246
.L370:
	movl	$32, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L210
.L299:
	movq	(%rbx), %rdi
	jmp	.L266
.L383:
	movq	%rcx, %rbx
	movq	%rdi, %r15
	jmp	.L267
.L374:
	movq	%r8, %rsi
.L369:
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L219:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L382:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10579:
	.size	_ZN2v88internal8compiler10LoopPeeler4PeelEPNS1_8LoopTree4LoopE, .-_ZN2v88internal8compiler10LoopPeeler4PeelEPNS1_8LoopTree4LoopE
	.section	.rodata._ZN2v88internal8compiler10LoopPeeler14PeelInnerLoopsEPNS1_8LoopTree4LoopE.str1.1,"aMS",@progbits,1
.LC6:
	.string	"Peeling loop with header: "
.LC7:
	.string	"%i "
.LC8:
	.string	"\n"
	.section	.text._ZN2v88internal8compiler10LoopPeeler14PeelInnerLoopsEPNS1_8LoopTree4LoopE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler10LoopPeeler14PeelInnerLoopsEPNS1_8LoopTree4LoopE
	.type	_ZN2v88internal8compiler10LoopPeeler14PeelInnerLoopsEPNS1_8LoopTree4LoopE, @function
_ZN2v88internal8compiler10LoopPeeler14PeelInnerLoopsEPNS1_8LoopTree4LoopE:
.LFB10580:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	32(%rsi), %r14
	movq	24(%rsi), %rbx
	cmpq	%r14, %rbx
	je	.L385
.L386:
	movq	(%rbx), %rsi
	movq	%r13, %rdi
	addq	$8, %rbx
	call	_ZN2v88internal8compiler10LoopPeeler14PeelInnerLoopsEPNS1_8LoopTree4LoopE
	cmpq	%rbx, %r14
	jne	.L386
.L384:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L385:
	.cfi_restore_state
	movl	60(%rsi), %eax
	subl	48(%rsi), %eax
	movq	%rsi, %r12
	cltq
	cmpq	$1000, %rax
	ja	.L384
	cmpb	$0, _ZN2v88internal21FLAG_trace_turbo_loopE(%rip)
	jne	.L395
.L389:
	addq	$8, %rsp
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler10LoopPeeler4PeelEPNS1_8LoopTree4LoopE
	.p2align 4,,10
	.p2align 3
.L395:
	.cfi_restore_state
	xorl	%eax, %eax
	leaq	.LC6(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	16(%r13), %rax
	movslq	52(%r12), %rdx
	movq	112(%rax), %rax
	leaq	(%rax,%rdx,8), %r14
	movslq	48(%r12), %rdx
	leaq	(%rax,%rdx,8), %rbx
	cmpq	%rbx, %r14
	je	.L390
	leaq	.LC7(%rip), %r15
	.p2align 4,,10
	.p2align 3
.L391:
	movq	(%rbx), %rax
	movq	%r15, %rdi
	addq	$8, %rbx
	movl	20(%rax), %esi
	xorl	%eax, %eax
	andl	$16777215, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	cmpq	%rbx, %r14
	jne	.L391
.L390:
	leaq	.LC8(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L389
	.cfi_endproc
.LFE10580:
	.size	_ZN2v88internal8compiler10LoopPeeler14PeelInnerLoopsEPNS1_8LoopTree4LoopE, .-_ZN2v88internal8compiler10LoopPeeler14PeelInnerLoopsEPNS1_8LoopTree4LoopE
	.section	.text._ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm,"axG",@progbits,_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	.type	_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm, @function
_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm:
.LFB12546:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$8, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	shrq	$6, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	1(%rsi), %rbx
	addq	$3, %rsi
	subq	$8, %rsp
	cmpq	$8, %rsi
	cmovb	%rax, %rsi
	movq	%rsi, 24(%rdi)
	movq	(%rdi), %rdi
	salq	$3, %rsi
	movq	16(%rdi), %rdx
	movq	24(%rdi), %rax
	subq	%rdx, %rax
	cmpq	%rax, %rsi
	ja	.L410
	addq	%rdx, %rsi
	movq	%rsi, 16(%rdi)
.L398:
	movq	24(%r12), %rax
	movq	%rdx, 16(%r12)
	subq	%rbx, %rax
	shrq	%rax
	leaq	(%rdx,%rax,8), %r15
	leaq	(%r15,%rbx,8), %r14
	cmpq	%r14, %r15
	jnb	.L399
	movq	%r15, %rbx
	jmp	.L404
	.p2align 4,,10
	.p2align 3
.L400:
	movq	(%r12), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L411
	leaq	512(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L401:
	movq	%rax, (%rbx)
	addq	$8, %rbx
	cmpq	%rbx, %r14
	jbe	.L399
.L404:
	movq	8(%r12), %rax
	testq	%rax, %rax
	je	.L400
	cmpq	$63, 8(%rax)
	jbe	.L400
	movq	(%rax), %rdx
	addq	$8, %rbx
	movq	%rdx, 8(%r12)
	movq	%rax, -8(%rbx)
	cmpq	%rbx, %r14
	ja	.L404
.L399:
	movq	%r15, 56(%r12)
	movq	(%r15), %rdx
	andl	$63, %r13d
	leaq	512(%rdx), %rax
	movq	%rdx, 40(%r12)
	movq	%rax, 48(%r12)
	leaq	-8(%r14), %rax
	movq	%rax, 88(%r12)
	movq	-8(%r14), %rax
	movq	%rdx, 32(%r12)
	movq	%rax, 72(%r12)
	leaq	512(%rax), %rcx
	leaq	(%rax,%r13,8), %rax
	movq	%rcx, 80(%r12)
	movq	%rax, 64(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L411:
	.cfi_restore_state
	movl	$512, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L401
	.p2align 4,,10
	.p2align 3
.L410:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L398
	.cfi_endproc
.LFE12546:
	.size	_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm, .-_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	.section	.text._ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_reallocate_mapEmb,"axG",@progbits,_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_reallocate_mapEmb,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_reallocate_mapEmb
	.type	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_reallocate_mapEmb, @function
_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_reallocate_mapEmb:
.LFB12571:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	88(%rdi), %rax
	movq	56(%rdi), %rsi
	movq	24(%rdi), %rbx
	movq	%rax, %r13
	subq	%rsi, %r13
	movq	%r13, %rcx
	sarq	$3, %rcx
	leaq	1(%r14,%rcx), %r8
	leaq	(%r8,%r8), %rcx
	cmpq	%rcx, %rbx
	jbe	.L413
	subq	%r8, %rbx
	shrq	%rbx
	salq	$3, %rbx
	testb	%dl, %dl
	leaq	(%rbx,%r14,8), %rcx
	cmovne	%rcx, %rbx
	addq	$8, %rax
	addq	16(%rdi), %rbx
	movq	%rax, %rdx
	subq	%rsi, %rdx
	cmpq	%rbx, %rsi
	jbe	.L415
	cmpq	%rax, %rsi
	je	.L416
	movq	%rbx, %rdi
	call	memmove@PLT
	jmp	.L416
	.p2align 4,,10
	.p2align 3
.L413:
	cmpq	%r14, %rbx
	movq	%r14, %rax
	movq	(%rdi), %rdi
	cmovnb	%rbx, %rax
	movq	16(%rdi), %r15
	leaq	2(%rbx,%rax), %rcx
	movq	24(%rdi), %rax
	leaq	0(,%rcx,8), %rsi
	subq	%r15, %rax
	cmpq	%rax, %rsi
	ja	.L423
	addq	%r15, %rsi
	movq	%rsi, 16(%rdi)
.L418:
	movq	%rcx, %rax
	subq	%r8, %rax
	shrq	%rax
	salq	$3, %rax
	testb	%dl, %dl
	leaq	(%rax,%r14,8), %rsi
	cmovne	%rsi, %rax
	movq	56(%r12), %rsi
	leaq	(%r15,%rax), %rbx
	movq	88(%r12), %rax
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L420
	subq	%rsi, %rdx
	movq	%rbx, %rdi
	movq	%rcx, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %rcx
.L420:
	movq	24(%r12), %rax
	leaq	0(,%rax,8), %rdx
	cmpq	$15, %rdx
	jbe	.L421
	movq	16(%r12), %rdx
	movq	%rax, 8(%rdx)
	movq	$0, (%rdx)
.L421:
	movq	%r15, 16(%r12)
	movq	%rcx, 24(%r12)
.L416:
	movq	%rbx, 56(%r12)
	movq	(%rbx), %rax
	movq	(%rbx), %xmm0
	addq	%r13, %rbx
	addq	$512, %rax
	movq	%rbx, 88(%r12)
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 40(%r12)
	movq	(%rbx), %rax
	movq	%rax, 72(%r12)
	addq	$512, %rax
	movq	%rax, 80(%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L415:
	.cfi_restore_state
	cmpq	%rax, %rsi
	je	.L416
	leaq	8(%r13), %rdi
	subq	%rdx, %rdi
	addq	%rbx, %rdi
	call	memmove@PLT
	jmp	.L416
	.p2align 4,,10
	.p2align 3
.L423:
	movl	%edx, -68(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %rcx
	movl	-68(%rbp), %edx
	movq	%rax, %r15
	jmp	.L418
	.cfi_endproc
.LFE12571:
	.size	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_reallocate_mapEmb, .-_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_reallocate_mapEmb
	.section	.rodata._ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_.str1.8,"aMS",@progbits,1
	.align 8
.LC9:
	.string	"cannot create std::deque larger than max_size()"
	.section	.text._ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_,"axG",@progbits,_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	.type	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_, @function
_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_:
.LFB12225:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	88(%rdi), %r13
	movq	64(%rdi), %rax
	subq	72(%rdi), %rax
	movq	%r13, %rdx
	subq	56(%rdi), %rdx
	sarq	$3, %rax
	sarq	$3, %rdx
	subq	$1, %rdx
	movq	%rdx, %rcx
	salq	$6, %rcx
	leaq	(%rcx,%rax), %rdx
	movq	48(%rdi), %rax
	subq	32(%rdi), %rax
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpq	$268435455, %rax
	je	.L435
	movq	%rsi, %r12
	movq	%r13, %rax
	movq	24(%rdi), %rsi
	subq	16(%rdi), %rax
	sarq	$3, %rax
	movq	%rdi, %rbx
	subq	%rax, %rsi
	cmpq	$1, %rsi
	jbe	.L436
.L426:
	movq	8(%rbx), %rax
	testq	%rax, %rax
	je	.L427
	cmpq	$63, 8(%rax)
	ja	.L437
.L427:
	movq	(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L438
	leaq	512(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L428:
	movq	%rax, 8(%r13)
	movq	(%r12), %rdx
	movq	64(%rbx), %rax
	movq	%rdx, (%rax)
	movq	88(%rbx), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 88(%rbx)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 72(%rbx)
	movq	%rdx, 80(%rbx)
	movq	%rax, 64(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L436:
	.cfi_restore_state
	xorl	%edx, %edx
	movl	$1, %esi
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_reallocate_mapEmb
	movq	88(%rbx), %r13
	jmp	.L426
	.p2align 4,,10
	.p2align 3
.L437:
	movq	(%rax), %rdx
	movq	%rdx, 8(%rbx)
	jmp	.L428
	.p2align 4,,10
	.p2align 3
.L438:
	movl	$512, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L428
.L435:
	leaq	.LC9(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE12225:
	.size	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_, .-_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	.section	.text._ZN2v88internal8compiler10LoopPeeler18EliminateLoopExitsEPNS1_5GraphEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler10LoopPeeler18EliminateLoopExitsEPNS1_5GraphEPNS0_4ZoneE
	.type	_ZN2v88internal8compiler10LoopPeeler18EliminateLoopExitsEPNS1_5GraphEPNS0_4ZoneE, @function
_ZN2v88internal8compiler10LoopPeeler18EliminateLoopExitsEPNS1_5GraphEPNS0_4ZoneE:
.LFB10583:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	leaq	-256(%rbp), %rdi
	pushq	%rbx
	subq	$328, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rsi, -256(%rbp)
	xorl	%esi, %esi
	movq	$0, -248(%rbp)
	movq	$0, -240(%rbp)
	movq	$0, -232(%rbp)
	movaps	%xmm0, -224(%rbp)
	movaps	%xmm0, -208(%rbp)
	movaps	%xmm0, -192(%rbp)
	movaps	%xmm0, -176(%rbp)
	call	_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	cmpq	$0, -240(%rbp)
	je	.L525
	movdqa	-256(%rbp), %xmm5
	pxor	%xmm0, %xmm0
	leaq	-160(%rbp), %rdi
	xorl	%esi, %esi
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm5, -160(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	$0, -144(%rbp)
	movq	$0, -136(%rbp)
	call	_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	movdqa	-96(%rbp), %xmm5
	movq	-216(%rbp), %r9
	movq	-224(%rbp), %xmm3
	movq	-200(%rbp), %r8
	movq	-192(%rbp), %xmm1
	movq	-184(%rbp), %rcx
	movaps	%xmm5, -192(%rbp)
	movq	%r9, %xmm5
	punpcklqdq	%xmm5, %xmm3
	movdqa	-128(%rbp), %xmm6
	movq	-208(%rbp), %xmm2
	movaps	%xmm3, -320(%rbp)
	movq	%r8, %xmm3
	movq	-168(%rbp), %rdx
	movdqa	-112(%rbp), %xmm7
	punpcklqdq	%xmm3, %xmm2
	movq	-248(%rbp), %rax
	movq	-176(%rbp), %xmm0
	movaps	%xmm6, -224(%rbp)
	movaps	%xmm2, -304(%rbp)
	movq	%rcx, %xmm2
	movq	-256(%rbp), %xmm4
	movq	-240(%rbp), %r11
	punpcklqdq	%xmm2, %xmm1
	movq	-232(%rbp), %r10
	movdqa	-80(%rbp), %xmm6
	movaps	%xmm7, -208(%rbp)
	movq	-144(%rbp), %rsi
	movq	-136(%rbp), %rdi
	movq	%rax, %xmm7
	movaps	%xmm1, -288(%rbp)
	movq	%rdx, %xmm1
	punpcklqdq	%xmm7, %xmm4
	movq	%r11, -336(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movq	%rsi, -240(%rbp)
	movq	%rdi, -232(%rbp)
	movq	%r10, -328(%rbp)
	movaps	%xmm6, -176(%rbp)
	movaps	%xmm4, -352(%rbp)
	movaps	%xmm0, -272(%rbp)
	testq	%rsi, %rsi
	je	.L441
	movq	-168(%rbp), %rbx
	movq	-200(%rbp), %rdx
	leaq	8(%rbx), %rcx
	cmpq	%rdx, %rcx
	jbe	.L442
	.p2align 4,,10
	.p2align 3
.L445:
	testq	%rax, %rax
	je	.L443
	cmpq	$64, 8(%rax)
	ja	.L444
.L443:
	movq	(%rdx), %rax
	movq	$64, 8(%rax)
	movq	-248(%rbp), %rsi
	movq	%rsi, (%rax)
	movq	%rax, -248(%rbp)
.L444:
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	ja	.L445
	movq	-232(%rbp), %rdi
	movq	-240(%rbp), %rsi
.L442:
	leaq	0(,%rdi,8), %rax
	cmpq	$15, %rax
	jbe	.L441
	movq	%rdi, 8(%rsi)
	movq	$0, (%rsi)
.L441:
	movl	28(%r12), %eax
	xorl	%ebx, %ebx
	testq	%rax, %rax
	jne	.L526
.L450:
	movq	-272(%rbp), %rax
	movq	-288(%rbp), %rdx
	movq	16(%r12), %r12
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L451
	movq	%r12, (%rdx)
	movq	-288(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, -288(%rbp)
.L452:
	movq	-320(%rbp), %rax
	movl	$1, %r12d
	cmpq	%rdx, %rax
	je	.L460
	.p2align 4,,10
	.p2align 3
.L459:
	movq	-304(%rbp), %rdi
	movq	(%rax), %r13
	leaq	-8(%rdi), %rdx
	cmpq	%rdx, %rax
	je	.L461
	addq	$8, %rax
	movq	%rax, -320(%rbp)
.L462:
	movq	0(%r13), %rdx
	cmpw	$52, 16(%rdx)
	je	.L527
	movl	28(%rdx), %edx
	testl	%edx, %edx
	jle	.L478
	xorl	%r14d, %r14d
	jmp	.L481
	.p2align 4,,10
	.p2align 3
.L479:
	movq	0(%r13), %rax
	addl	$1, %r14d
	cmpl	28(%rax), %r14d
	jge	.L528
.L481:
	movq	%r13, %rdi
	movl	%r14d, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%r12, %rdi
	movq	%rax, -160(%rbp)
	movl	20(%rax), %ecx
	movq	%rcx, %rax
	salq	%cl, %rdi
	shrq	$3, %rax
	andl	$2097144, %eax
	addq	%rbx, %rax
	movq	(%rax), %rdx
	testq	%rdi, %rdx
	jne	.L479
	orq	%rdi, %rdx
	movq	%rdx, (%rax)
	movq	-272(%rbp), %rax
	movq	-288(%rbp), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L480
	movq	-160(%rbp), %rax
	addl	$1, %r14d
	movq	%rax, (%rdx)
	addq	$8, -288(%rbp)
	movq	0(%r13), %rax
	cmpl	28(%rax), %r14d
	jl	.L481
.L528:
	movq	-320(%rbp), %rax
	movq	-288(%rbp), %rdx
.L477:
	cmpq	%rdx, %rax
	jne	.L459
.L460:
	movq	-336(%rbp), %rax
	testq	%rax, %rax
	je	.L439
	movq	-264(%rbp), %rbx
	movq	-296(%rbp), %rdx
	leaq	8(%rbx), %rcx
	cmpq	%rdx, %rcx
	jbe	.L483
	movq	-344(%rbp), %rax
	.p2align 4,,10
	.p2align 3
.L486:
	testq	%rax, %rax
	je	.L484
	cmpq	$64, 8(%rax)
	ja	.L485
.L484:
	movq	(%rdx), %rax
	movq	$64, 8(%rax)
	movq	-344(%rbp), %rsi
	movq	%rsi, (%rax)
	movq	%rax, -344(%rbp)
.L485:
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	ja	.L486
	movq	-336(%rbp), %rax
.L483:
	movq	-328(%rbp), %rdx
	leaq	0(,%rdx,8), %rcx
	cmpq	$15, %rcx
	jbe	.L439
	movq	%rdx, 8(%rax)
	movq	$0, (%rax)
.L439:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L529
	addq	$328, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L527:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, -160(%rbp)
	movq	24(%r13), %r15
	testq	%r15, %r15
	je	.L466
	movq	(%r15), %r14
	.p2align 4,,10
	.p2align 3
.L473:
	movl	16(%r15), %ecx
	movq	%r15, %rdi
	movl	%ecx, %edx
	shrl	%edx
	leaq	3(%rdx,%rdx,2), %rax
	leaq	(%r15,%rax,8), %rax
	leaq	32(%rax), %rsi
	addq	$16, %rax
	andl	$1, %ecx
	cmovne	%rsi, %rax
	leaq	(%rax,%rdx,8), %rsi
	call	_ZN2v88internal8compiler14NodeProperties13IsControlEdgeENS1_4EdgeE@PLT
	testb	%al, %al
	je	.L469
	movl	16(%r15), %edx
	movl	%edx, %eax
	shrl	%eax
	andl	$1, %edx
	leaq	3(%rax,%rax,2), %rax
	leaq	(%r15,%rax,8), %r15
	movq	(%r15), %rax
	jne	.L470
	movq	%rax, %r15
	movq	(%rax), %rax
.L470:
	movzwl	16(%rax), %eax
	cmpl	$53, %eax
	je	.L530
	cmpl	$54, %eax
	je	.L531
.L469:
	testq	%r14, %r14
	je	.L466
	movq	%r14, %r15
	movq	(%r14), %r14
	jmp	.L473
	.p2align 4,,10
	.p2align 3
.L466:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	xorl	%edx, %edx
	xorl	%esi, %esi
	xorl	%r8d, %r8d
	movq	%rax, %rcx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler14NodeProperties11ReplaceUsesEPNS1_4NodeES4_S4_S4_S4_@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node4KillEv@PLT
	movq	-160(%rbp), %rax
	movq	%r12, %rsi
	movl	20(%rax), %ecx
	movq	%rcx, %rax
	salq	%cl, %rsi
	shrq	$3, %rax
	andl	$2097144, %eax
	addq	%rbx, %rax
	movq	(%rax), %rdx
	testq	%rsi, %rdx
	je	.L474
.L524:
	movq	-288(%rbp), %rdx
.L475:
	movq	-320(%rbp), %rax
	jmp	.L477
	.p2align 4,,10
	.p2align 3
.L530:
	movzbl	23(%r15), %eax
	movq	32(%r15), %rsi
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L472
	movq	16(%rsi), %rsi
.L472:
	movq	%r15, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	_ZN2v88internal8compiler14NodeProperties11ReplaceUsesEPNS1_4NodeES4_S4_S4_S4_@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler4Node4KillEv@PLT
	jmp	.L469
	.p2align 4,,10
	.p2align 3
.L480:
	leaq	-160(%rbp), %rsi
	leaq	-352(%rbp), %rdi
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	jmp	.L479
	.p2align 4,,10
	.p2align 3
.L531:
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	%r15, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rax, %rdx
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties11ReplaceUsesEPNS1_4NodeES4_S4_S4_S4_@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler4Node4KillEv@PLT
	jmp	.L469
	.p2align 4,,10
	.p2align 3
.L461:
	movq	-344(%rbp), %rax
	testq	%rax, %rax
	je	.L463
	cmpq	$64, 8(%rax)
	ja	.L464
.L463:
	movq	-312(%rbp), %rax
	movq	$64, 8(%rax)
	movq	-344(%rbp), %rdx
	movq	%rdx, (%rax)
	movq	%rax, -344(%rbp)
.L464:
	movq	-296(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, -296(%rbp)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, -312(%rbp)
	movq	%rdx, -304(%rbp)
	movq	%rax, -320(%rbp)
	jmp	.L462
	.p2align 4,,10
	.p2align 3
.L474:
	orq	%rsi, %rdx
	movq	%rdx, (%rax)
	movq	-272(%rbp), %rax
	movq	-288(%rbp), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L476
	movq	-160(%rbp), %rax
	movq	%rax, (%rdx)
	movq	-288(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, -288(%rbp)
	jmp	.L475
	.p2align 4,,10
	.p2align 3
.L478:
	movq	-288(%rbp), %rdx
	jmp	.L477
	.p2align 4,,10
	.p2align 3
.L525:
	movdqa	-256(%rbp), %xmm6
	movq	-232(%rbp), %rax
	xorl	%ebx, %ebx
	movq	$0, -336(%rbp)
	movdqa	-224(%rbp), %xmm7
	movdqa	-176(%rbp), %xmm1
	movq	%rax, -328(%rbp)
	movl	28(%r12), %eax
	movaps	%xmm6, -352(%rbp)
	movdqa	-208(%rbp), %xmm6
	movaps	%xmm7, -320(%rbp)
	movdqa	-192(%rbp), %xmm7
	movaps	%xmm6, -304(%rbp)
	movaps	%xmm7, -288(%rbp)
	movaps	%xmm1, -272(%rbp)
	testq	%rax, %rax
	je	.L450
.L526:
	addq	$63, %rax
	movq	16(%r13), %rbx
	shrq	$6, %rax
	leaq	0(,%rax,8), %rdx
	movq	24(%r13), %rax
	movq	%rdx, %rsi
	subq	%rbx, %rax
	cmpq	%rax, %rdx
	ja	.L532
	addq	%rbx, %rsi
	movq	%rsi, 16(%r13)
.L449:
	testq	%rbx, %rbx
	je	.L450
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	memset@PLT
	jmp	.L450
.L476:
	leaq	-160(%rbp), %rsi
	leaq	-352(%rbp), %rdi
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	jmp	.L524
.L451:
	movq	-264(%rbp), %r13
	subq	-280(%rbp), %rdx
	sarq	$3, %rdx
	movq	%r13, %rax
	subq	-296(%rbp), %rax
	sarq	$3, %rax
	subq	$1, %rax
	salq	$6, %rax
	addq	%rax, %rdx
	movq	-304(%rbp), %rax
	subq	-320(%rbp), %rax
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpq	$268435455, %rax
	je	.L533
	movq	-328(%rbp), %rdi
	movq	%r13, %rax
	subq	-336(%rbp), %rax
	sarq	$3, %rax
	subq	%rax, %rdi
	cmpq	$1, %rdi
	jbe	.L534
.L454:
	movq	-344(%rbp), %rax
	testq	%rax, %rax
	je	.L455
	cmpq	$63, 8(%rax)
	ja	.L535
.L455:
	movq	-352(%rbp), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L536
	leaq	512(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L456:
	movq	%rax, 8(%r13)
	movq	-288(%rbp), %rax
	movq	%r12, (%rax)
	movq	-264(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, -264(%rbp)
	movq	8(%rax), %rdx
	leaq	512(%rdx), %rax
	movq	%rdx, -280(%rbp)
	movq	%rax, -272(%rbp)
	movq	%rdx, -288(%rbp)
	jmp	.L452
.L534:
	leaq	-352(%rbp), %rdi
	xorl	%edx, %edx
	movl	$1, %esi
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_reallocate_mapEmb
	movq	-264(%rbp), %r13
	jmp	.L454
.L535:
	movq	(%rax), %rdx
	movq	%rdx, -344(%rbp)
	jmp	.L456
.L532:
	movq	%r13, %rdi
	movq	%rdx, -360(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-360(%rbp), %rdx
	movq	%rax, %rbx
	jmp	.L449
.L536:
	movl	$512, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L456
.L533:
	leaq	.LC9(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L529:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10583:
	.size	_ZN2v88internal8compiler10LoopPeeler18EliminateLoopExitsEPNS1_5GraphEPNS0_4ZoneE, .-_ZN2v88internal8compiler10LoopPeeler18EliminateLoopExitsEPNS1_5GraphEPNS0_4ZoneE
	.section	.text._ZN2v88internal8compiler10LoopPeeler20PeelInnerLoopsOfTreeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler10LoopPeeler20PeelInnerLoopsOfTreeEv
	.type	_ZN2v88internal8compiler10LoopPeeler20PeelInnerLoopsOfTreeEv, @function
_ZN2v88internal8compiler10LoopPeeler20PeelInnerLoopsOfTreeEv:
.LFB10582:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %rax
	movq	16(%rax), %r12
	movq	24(%rax), %rax
	movq	%rax, -56(%rbp)
	cmpq	%rax, %r12
	je	.L538
	.p2align 4,,10
	.p2align 3
.L546:
	movq	(%r12), %r14
	movq	32(%r14), %r15
	movq	24(%r14), %rbx
	cmpq	%rbx, %r15
	je	.L539
.L540:
	movq	(%rbx), %rsi
	movq	%r13, %rdi
	addq	$8, %rbx
	call	_ZN2v88internal8compiler10LoopPeeler14PeelInnerLoopsEPNS1_8LoopTree4LoopE
	cmpq	%rbx, %r15
	jne	.L540
.L541:
	addq	$8, %r12
	cmpq	%r12, -56(%rbp)
	jne	.L546
.L538:
	movq	24(%r13), %rsi
	movq	0(%r13), %rdi
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler10LoopPeeler18EliminateLoopExitsEPNS1_5GraphEPNS0_4ZoneE
	.p2align 4,,10
	.p2align 3
.L539:
	.cfi_restore_state
	movl	60(%r14), %eax
	subl	48(%r14), %eax
	cltq
	cmpq	$1000, %rax
	ja	.L541
	cmpb	$0, _ZN2v88internal21FLAG_trace_turbo_loopE(%rip)
	jne	.L551
.L543:
	movq	%r14, %rsi
	movq	%r13, %rdi
	addq	$8, %r12
	call	_ZN2v88internal8compiler10LoopPeeler4PeelEPNS1_8LoopTree4LoopE
	cmpq	%r12, -56(%rbp)
	jne	.L546
	jmp	.L538
	.p2align 4,,10
	.p2align 3
.L551:
	xorl	%eax, %eax
	leaq	.LC6(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	16(%r13), %rax
	movslq	52(%r14), %rdx
	movq	112(%rax), %rax
	leaq	(%rax,%rdx,8), %r15
	movslq	48(%r14), %rdx
	leaq	(%rax,%rdx,8), %rdx
	cmpq	%rdx, %r15
	je	.L544
	movq	%rdx, %rbx
	.p2align 4,,10
	.p2align 3
.L545:
	movq	(%rbx), %rax
	leaq	.LC7(%rip), %rdi
	addq	$8, %rbx
	movl	20(%rax), %esi
	xorl	%eax, %eax
	andl	$16777215, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	cmpq	%rbx, %r15
	jne	.L545
.L544:
	leaq	.LC8(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L543
	.cfi_endproc
.LFE10582:
	.size	_ZN2v88internal8compiler10LoopPeeler20PeelInnerLoopsOfTreeEv, .-_ZN2v88internal8compiler10LoopPeeler20PeelInnerLoopsOfTreeEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler15PeeledIteration3mapEPNS1_4NodeE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler15PeeledIteration3mapEPNS1_4NodeE, @function
_GLOBAL__sub_I__ZN2v88internal8compiler15PeeledIteration3mapEPNS1_4NodeE:
.LFB12752:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE12752:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler15PeeledIteration3mapEPNS1_4NodeE, .-_GLOBAL__sub_I__ZN2v88internal8compiler15PeeledIteration3mapEPNS1_4NodeE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler15PeeledIteration3mapEPNS1_4NodeE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
