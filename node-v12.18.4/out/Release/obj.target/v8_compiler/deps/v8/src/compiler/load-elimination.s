	.file	"load-elimination.cc"
	.text
	.section	.text._ZNKSt5ctypeIcE8do_widenEc,"axG",@progbits,_ZNKSt5ctypeIcE8do_widenEc,comdat
	.align 2
	.p2align 4
	.weak	_ZNKSt5ctypeIcE8do_widenEc
	.type	_ZNKSt5ctypeIcE8do_widenEc, @function
_ZNKSt5ctypeIcE8do_widenEc:
.LFB1554:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	ret
	.cfi_endproc
.LFE1554:
	.size	_ZNKSt5ctypeIcE8do_widenEc, .-_ZNKSt5ctypeIcE8do_widenEc
	.section	.rodata._ZNK2v88internal8compiler15LoadElimination12reducer_nameEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"LoadElimination"
	.section	.text._ZNK2v88internal8compiler15LoadElimination12reducer_nameEv,"axG",@progbits,_ZNK2v88internal8compiler15LoadElimination12reducer_nameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal8compiler15LoadElimination12reducer_nameEv
	.type	_ZNK2v88internal8compiler15LoadElimination12reducer_nameEv, @function
_ZNK2v88internal8compiler15LoadElimination12reducer_nameEv:
.LFB10401:
	.cfi_startproc
	endbr64
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE10401:
	.size	_ZNK2v88internal8compiler15LoadElimination12reducer_nameEv, .-_ZNK2v88internal8compiler15LoadElimination12reducer_nameEv
	.section	.text._ZN2v88internal8compiler15LoadEliminationD2Ev,"axG",@progbits,_ZN2v88internal8compiler15LoadEliminationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler15LoadEliminationD2Ev
	.type	_ZN2v88internal8compiler15LoadEliminationD2Ev, @function
_ZN2v88internal8compiler15LoadEliminationD2Ev:
.LFB24330:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE24330:
	.size	_ZN2v88internal8compiler15LoadEliminationD2Ev, .-_ZN2v88internal8compiler15LoadEliminationD2Ev
	.weak	_ZN2v88internal8compiler15LoadEliminationD1Ev
	.set	_ZN2v88internal8compiler15LoadEliminationD1Ev,_ZN2v88internal8compiler15LoadEliminationD2Ev
	.section	.text._ZN2v88internal8compiler15LoadEliminationD0Ev,"axG",@progbits,_ZN2v88internal8compiler15LoadEliminationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler15LoadEliminationD0Ev
	.type	_ZN2v88internal8compiler15LoadEliminationD0Ev, @function
_ZN2v88internal8compiler15LoadEliminationD0Ev:
.LFB24332:
	.cfi_startproc
	endbr64
	movl	$56, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE24332:
	.size	_ZN2v88internal8compiler15LoadEliminationD0Ev, .-_ZN2v88internal8compiler15LoadEliminationD0Ev
	.section	.text._ZN2v88internal12StdoutStreamD1Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD1Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StdoutStreamD1Ev
	.type	_ZN2v88internal12StdoutStreamD1Ev, @function
_ZN2v88internal12StdoutStreamD1Ev:
.LFB24335:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE24335:
	.size	_ZN2v88internal12StdoutStreamD1Ev, .-_ZN2v88internal12StdoutStreamD1Ev
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE, @function
_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE:
.LFB19311:
	.cfi_startproc
	movq	%rdi, %rax
	.p2align 4,,10
	.p2align 3
.L16:
	movq	(%rax), %rdx
	movzwl	16(%rdx), %edx
	cmpw	$58, %dx
	je	.L9
	cmpw	$228, %dx
	je	.L9
	cmpw	$40, %dx
	je	.L9
.L23:
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movzbl	23(%rax), %edx
	leaq	32(%rax), %rcx
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L11
	testl	%edx, %edx
	jne	.L30
.L29:
	movq	(%rcx), %rax
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L11:
	movq	32(%rax), %rdx
	movl	8(%rdx), %ecx
	testl	%ecx, %ecx
	jle	.L17
	cmpq	$0, 16(%rdx)
	je	.L23
.L17:
	movq	16(%rdx), %rax
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L30:
	cmpq	$0, (%rcx)
	jne	.L29
	ret
	.cfi_endproc
.LFE19311:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE, .-_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	.section	.text._ZN2v88internal12StdoutStreamD0Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD0Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.type	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev, @function
_ZTv0_n24_N2v88internal12StdoutStreamD0Ev:
.LFB24683:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %rax
	addq	-24(%rax), %rdi
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	movq	%rax, 80(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE24683:
	.size	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev, .-_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StdoutStreamD0Ev
	.type	_ZN2v88internal12StdoutStreamD0Ev, @function
_ZN2v88internal12StdoutStreamD0Ev:
.LFB24336:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE24336:
	.size	_ZN2v88internal12StdoutStreamD0Ev, .-_ZN2v88internal12StdoutStreamD0Ev
	.section	.text._ZN2v88internal12StdoutStreamD1Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD1Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.type	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev, @function
_ZTv0_n24_N2v88internal12StdoutStreamD1Ev:
.LFB24684:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rax
	movq	-24(%rax), %rbx
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	addq	%rdi, %rbx
	movq	%rax, 80(%rbx)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	64(%rbx), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE24684:
	.size	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev, .-_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_18MayAliasEPNS1_4NodeES4_,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_18MayAliasEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler12_GLOBAL__N_18MayAliasEPNS1_4NodeES4_:
.LFB19312:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpq	%rsi, %rdi
	je	.L55
	movq	8(%rdi), %rax
	movq	%rsi, %r12
	movq	8(%rsi), %rsi
	movq	%rdi, %r13
	leaq	-32(%rbp), %rdi
	movq	%rax, -32(%rbp)
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	je	.L37
	movq	(%r12), %rax
	movzwl	16(%rax), %eax
	cmpw	$58, %ax
	je	.L40
	cmpw	$228, %ax
	je	.L40
	cmpw	$40, %ax
	je	.L40
.L41:
	movq	0(%r13), %rdx
	movzwl	16(%rdx), %edx
	cmpw	$58, %dx
	je	.L47
	cmpw	$228, %dx
	je	.L47
	cmpw	$40, %dx
	je	.L47
.L48:
	cmpw	$237, %ax
	je	.L88
	cmpw	$237, %dx
	je	.L89
	.p2align 4,,10
	.p2align 3
.L55:
	movl	$1, %eax
.L37:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L90
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	.cfi_restore_state
	movzbl	23(%r13), %ecx
	andl	$15, %ecx
	cmpl	$15, %ecx
	je	.L49
	testl	%ecx, %ecx
	je	.L85
	cmpq	$0, 32(%r13)
	je	.L48
.L85:
	movq	32(%r13), %rdi
.L57:
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_18MayAliasEPNS1_4NodeES4_
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L40:
	movzbl	23(%r12), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L42
	testl	%edx, %edx
	je	.L86
	cmpq	$0, 32(%r12)
	je	.L41
.L86:
	movq	32(%r12), %rsi
.L58:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_18MayAliasEPNS1_4NodeES4_
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L42:
	movq	32(%r12), %rdx
	movl	8(%rdx), %edi
	testl	%edi, %edi
	jle	.L59
	cmpq	$0, 16(%rdx)
	je	.L41
	.p2align 4,,10
	.p2align 3
.L59:
	movq	16(%rdx), %rsi
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L89:
	cmpw	$30, %ax
	je	.L67
	cmpw	$50, %ax
	setne	%al
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L49:
	movq	32(%r13), %rcx
	movl	8(%rcx), %esi
	testl	%esi, %esi
	jle	.L56
	cmpq	$0, 16(%rcx)
	je	.L48
	.p2align 4,,10
	.p2align 3
.L56:
	movq	16(%rcx), %rdi
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L88:
	cmpw	$50, %dx
	je	.L67
	cmpw	$237, %dx
	je	.L67
	cmpw	$30, %dx
	setne	%al
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L67:
	xorl	%eax, %eax
	jmp	.L37
.L90:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19312:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_18MayAliasEPNS1_4NodeES4_, .-_ZN2v88internal8compiler12_GLOBAL__N_18MayAliasEPNS1_4NodeES4_
	.section	.text._ZNK2v88internal8compiler15LoadElimination16AbstractElements6LookupEPNS1_4NodeES5_NS0_21MachineRepresentationE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler15LoadElimination16AbstractElements6LookupEPNS1_4NodeES5_NS0_21MachineRepresentationE
	.type	_ZNK2v88internal8compiler15LoadElimination16AbstractElements6LookupEPNS1_4NodeES5_NS0_21MachineRepresentationE, @function
_ZNK2v88internal8compiler15LoadElimination16AbstractElements6LookupEPNS1_4NodeES5_NS0_21MachineRepresentationE:
.LFB19316:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leal	-6(%rcx), %eax
	movq	%rsi, %r9
	movq	%rdi, %r8
	movq	%rdx, %r11
	movl	%ecx, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r12
	cmpb	$2, %al
	ja	.L164
	testq	%r12, %r12
	je	.L102
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	movq	%r12, %rdi
	movq	%rax, %r10
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	cmpq	%rax, %r10
	je	.L165
.L102:
	movq	32(%r8), %r12
	testq	%r12, %r12
	je	.L104
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	movq	%r12, %rdi
	movq	%rax, %r10
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	cmpq	%rax, %r10
	je	.L166
.L104:
	movq	64(%r8), %r12
	testq	%r12, %r12
	je	.L105
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	movq	%r12, %rdi
	movq	%rax, %r10
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	cmpq	%rax, %r10
	je	.L167
.L105:
	movq	96(%r8), %r12
	testq	%r12, %r12
	je	.L106
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	movq	%r12, %rdi
	movq	%rax, %r10
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	cmpq	%rax, %r10
	je	.L168
.L106:
	movq	128(%r8), %r12
	testq	%r12, %r12
	je	.L107
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	movq	%r12, %rdi
	movq	%rax, %r10
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	cmpq	%rax, %r10
	je	.L169
.L107:
	movq	160(%r8), %r12
	testq	%r12, %r12
	je	.L108
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	movq	%r12, %rdi
	movq	%rax, %r10
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	cmpq	%rax, %r10
	je	.L170
.L108:
	movq	192(%r8), %r12
	testq	%r12, %r12
	je	.L109
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	movq	%r12, %rdi
	movq	%rax, %r10
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	cmpq	%rax, %r10
	je	.L171
.L109:
	movq	224(%r8), %r10
	testq	%r10, %r10
	je	.L91
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	movq	%r10, %rdi
	movq	%rax, %r9
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	xorl	%r10d, %r10d
	cmpq	%rax, %r9
	je	.L172
.L91:
	popq	%rbx
	movq	%r10, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L164:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L93
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	movq	%r12, %rdi
	movq	%rax, %r10
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	cmpq	%rax, %r10
	je	.L173
.L93:
	movq	32(%r8), %r12
	testq	%r12, %r12
	je	.L95
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	movq	%r12, %rdi
	movq	%rax, %r10
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	cmpq	%rax, %r10
	je	.L174
.L95:
	movq	64(%r8), %r12
	testq	%r12, %r12
	je	.L96
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	movq	%r12, %rdi
	movq	%rax, %r10
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	cmpq	%rax, %r10
	je	.L175
.L96:
	movq	96(%r8), %r12
	testq	%r12, %r12
	je	.L97
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	movq	%r12, %rdi
	movq	%rax, %r10
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	cmpq	%rax, %r10
	je	.L176
.L97:
	movq	128(%r8), %r12
	testq	%r12, %r12
	je	.L98
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	movq	%r12, %rdi
	movq	%rax, %r10
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	cmpq	%rax, %r10
	je	.L177
.L98:
	movq	160(%r8), %r12
	testq	%r12, %r12
	je	.L99
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	movq	%r12, %rdi
	movq	%rax, %r10
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	cmpq	%rax, %r10
	je	.L178
.L99:
	movq	192(%r8), %r12
	testq	%r12, %r12
	je	.L100
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	movq	%r12, %rdi
	movq	%rax, %r10
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	cmpq	%rax, %r10
	je	.L179
.L100:
	movq	224(%r8), %r10
	testq	%r10, %r10
	je	.L91
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	movq	%r10, %rdi
	movq	%rax, %r9
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	xorl	%r10d, %r10d
	cmpq	%rax, %r9
	jne	.L91
	movq	%r11, %rdi
	movq	240(%r8), %r10
	movzbl	248(%r8), %ebx
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	movq	232(%r8), %rdi
	movq	%rax, %r9
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	cmpq	%rax, %r9
	jne	.L113
	cmpb	%sil, %bl
	movl	$0, %eax
	cmovne	%rax, %r10
	jmp	.L91
.L166:
	movq	%r11, %rdi
	movq	48(%r8), %r10
	movzbl	56(%r8), %ebx
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	movq	40(%r8), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	cmpq	%rax, %r12
	jne	.L104
	cmpb	%bl, %sil
	je	.L91
	subl	$6, %ebx
	cmpb	$2, %bl
	jbe	.L91
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L165:
	movq	%r11, %rdi
	movq	16(%r8), %r10
	movzbl	24(%r8), %ebx
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	movq	8(%r8), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	cmpq	%rax, %r12
	jne	.L102
	cmpb	%bl, %sil
	je	.L91
	subl	$6, %ebx
	cmpb	$2, %bl
	jbe	.L91
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L167:
	movq	%r11, %rdi
	movq	80(%r8), %r10
	movzbl	88(%r8), %ebx
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	movq	72(%r8), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	cmpq	%rax, %r12
	jne	.L105
	cmpb	%bl, %sil
	je	.L91
	subl	$6, %ebx
	cmpb	$2, %bl
	jbe	.L91
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L168:
	movq	%r11, %rdi
	movq	112(%r8), %r10
	movzbl	120(%r8), %ebx
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	movq	104(%r8), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	cmpq	%rax, %r12
	jne	.L106
	cmpb	%bl, %sil
	je	.L91
	subl	$6, %ebx
	cmpb	$2, %bl
	jbe	.L91
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L170:
	movq	%r11, %rdi
	movq	176(%r8), %r10
	movzbl	184(%r8), %ebx
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	movq	168(%r8), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	cmpq	%rax, %r12
	jne	.L108
	cmpb	%bl, %sil
	je	.L91
	subl	$6, %ebx
	cmpb	$2, %bl
	jbe	.L91
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L169:
	movq	%r11, %rdi
	movq	144(%r8), %r10
	movzbl	152(%r8), %ebx
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	movq	136(%r8), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	cmpq	%rax, %r12
	jne	.L107
	cmpb	%bl, %sil
	je	.L91
	subl	$6, %ebx
	cmpb	$2, %bl
	jbe	.L91
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L171:
	movq	%r11, %rdi
	movq	208(%r8), %r10
	movzbl	216(%r8), %ebx
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	movq	200(%r8), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	cmpq	%rax, %r12
	jne	.L109
	cmpb	%bl, %sil
	je	.L91
	subl	$6, %ebx
	cmpb	$2, %bl
	jbe	.L91
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L172:
	movq	%r11, %rdi
	movq	240(%r8), %r10
	movzbl	248(%r8), %r12d
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	movq	232(%r8), %rdi
	movq	%rax, %r9
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	cmpq	%rax, %r9
	je	.L180
.L113:
	xorl	%r10d, %r10d
	jmp	.L91
.L180:
	cmpb	%sil, %r12b
	je	.L91
	subl	$6, %r12d
	movl	$0, %eax
	cmpb	$3, %r12b
	cmovnb	%rax, %r10
	jmp	.L91
.L173:
	movq	%r11, %rdi
	movq	16(%r8), %r10
	movzbl	24(%r8), %ebx
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	movq	8(%r8), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	cmpq	%rax, %r12
	jne	.L93
	cmpb	%bl, %sil
	jne	.L93
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L174:
	movq	%r11, %rdi
	movq	48(%r8), %r10
	movzbl	56(%r8), %ebx
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	movq	40(%r8), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	cmpq	%rax, %r12
	jne	.L95
	cmpb	%bl, %sil
	jne	.L95
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L175:
	movq	%r11, %rdi
	movq	80(%r8), %r10
	movzbl	88(%r8), %ebx
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	movq	72(%r8), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	cmpq	%rax, %r12
	jne	.L96
	cmpb	%bl, %sil
	jne	.L96
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L176:
	movq	%r11, %rdi
	movq	112(%r8), %r10
	movzbl	120(%r8), %ebx
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	movq	104(%r8), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	cmpq	%rax, %r12
	jne	.L97
	cmpb	%bl, %sil
	jne	.L97
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L177:
	movq	%r11, %rdi
	movq	144(%r8), %r10
	movzbl	152(%r8), %ebx
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	movq	136(%r8), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	cmpq	%rax, %r12
	jne	.L98
	cmpb	%bl, %sil
	jne	.L98
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L178:
	movq	%r11, %rdi
	movq	176(%r8), %r10
	movzbl	184(%r8), %ebx
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	movq	168(%r8), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	cmpq	%rax, %r12
	jne	.L99
	cmpb	%bl, %sil
	jne	.L99
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L179:
	movq	%r11, %rdi
	movq	208(%r8), %r10
	movzbl	216(%r8), %ebx
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	movq	200(%r8), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	cmpq	%rax, %r12
	jne	.L100
	cmpb	%bl, %sil
	jne	.L100
	jmp	.L91
	.cfi_endproc
.LFE19316:
	.size	_ZNK2v88internal8compiler15LoadElimination16AbstractElements6LookupEPNS1_4NodeES5_NS0_21MachineRepresentationE, .-_ZNK2v88internal8compiler15LoadElimination16AbstractElements6LookupEPNS1_4NodeES5_NS0_21MachineRepresentationE
	.section	.text._ZNK2v88internal8compiler15LoadElimination16AbstractElements4KillEPNS1_4NodeES5_PNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler15LoadElimination16AbstractElements4KillEPNS1_4NodeES5_PNS0_4ZoneE
	.type	_ZNK2v88internal8compiler15LoadElimination16AbstractElements4KillEPNS1_4NodeES5_PNS0_4ZoneE, @function
_ZNK2v88internal8compiler15LoadElimination16AbstractElements4KillEPNS1_4NodeES5_PNS0_4ZoneE:
.LFB19317:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	256(%rdi), %rbx
	subq	$104, %rsp
	movq	%rdx, -144(%rbp)
	movq	%rcx, -136(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-96(%rbp), %rax
	movq	%rax, -128(%rbp)
	.p2align 4,,10
	.p2align 3
.L210:
	movq	(%r15), %r8
	testq	%r8, %r8
	je	.L201
	cmpq	%r8, %r13
	je	.L184
	movq	8(%r13), %rax
	movq	-128(%rbp), %rdi
	movq	%r8, -120(%rbp)
	movq	%rax, -96(%rbp)
	movq	8(%r8), %rsi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	je	.L201
	movq	-120(%rbp), %r8
	movq	(%r8), %rax
	movzwl	16(%rax), %eax
	cmpw	$58, %ax
	je	.L185
	cmpw	$228, %ax
	je	.L185
	cmpw	$40, %ax
	je	.L185
.L186:
	movq	0(%r13), %rcx
	movzwl	16(%rcx), %ecx
	cmpw	$58, %cx
	je	.L193
	cmpw	$228, %cx
	je	.L193
	cmpw	$40, %cx
	je	.L193
.L194:
	cmpw	$237, %ax
	je	.L267
	cmpw	$237, %cx
	je	.L268
	.p2align 4,,10
	.p2align 3
.L184:
	movq	-136(%rbp), %rax
	movq	16(%rax), %r15
	movq	24(%rax), %rax
	movq	%rax, -120(%rbp)
	subq	%r15, %rax
	cmpq	$263, %rax
	jbe	.L269
	movq	-136(%rbp), %rdx
	leaq	264(%r15), %rax
	movq	%rax, 16(%rdx)
.L204:
	xorl	%eax, %eax
	movl	$32, %ecx
	movq	%r15, %rdi
	movq	%r15, %r12
	rep stosq
	leaq	-104(%rbp), %rax
	movq	$0, 256(%r15)
	movq	%rax, -136(%rbp)
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L207:
	movq	256(%r15), %rax
	movq	%rcx, %xmm0
	movhps	-120(%rbp), %xmm0
	leaq	1(%rax), %rsi
	salq	$5, %rax
	addq	%r15, %rax
	movq	%rsi, 256(%r15)
	movaps	%xmm0, -96(%rbp)
	movups	%xmm0, (%rax)
	movq	-80(%rbp), %rcx
	movq	%rcx, 16(%rax)
	movzbl	-72(%rbp), %ecx
	movb	%cl, 24(%rax)
.L205:
	addq	$32, %r14
	cmpq	%rbx, %r14
	je	.L270
.L208:
	movdqu	(%r14), %xmm1
	movdqu	16(%r14), %xmm2
	movq	(%r14), %rcx
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm2, -80(%rbp)
	testq	%rcx, %rcx
	je	.L205
	movq	8(%r14), %rax
	movq	%rcx, %rsi
	movq	%r13, %rdi
	movq	%rcx, -128(%rbp)
	movq	%rax, -120(%rbp)
	call	_ZN2v88internal8compiler12_GLOBAL__N_18MayAliasEPNS1_4NodeES4_
	movq	-128(%rbp), %rcx
	testb	%al, %al
	je	.L207
	movq	-144(%rbp), %rax
	movq	-136(%rbp), %rdi
	movq	%rcx, -128(%rbp)
	movq	8(%rax), %rax
	movq	%rax, -104(%rbp)
	movq	-120(%rbp), %rax
	movq	8(%rax), %rsi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	movq	-128(%rbp), %rcx
	testb	%al, %al
	je	.L207
	addq	$32, %r14
	cmpq	%rbx, %r14
	jne	.L208
	.p2align 4,,10
	.p2align 3
.L270:
	andq	$7, 256(%r15)
.L209:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L271
	addq	$104, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L185:
	.cfi_restore_state
	movzbl	23(%r8), %ecx
	andl	$15, %ecx
	cmpl	$15, %ecx
	je	.L187
	testl	%ecx, %ecx
	je	.L265
	cmpq	$0, 32(%r8)
	je	.L186
.L265:
	movq	32(%r8), %rsi
.L214:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_18MayAliasEPNS1_4NodeES4_
.L192:
	testb	%al, %al
	jne	.L184
.L201:
	addq	$32, %r15
	cmpq	%rbx, %r15
	jne	.L210
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L187:
	movq	32(%r8), %rcx
	movl	8(%rcx), %esi
	testl	%esi, %esi
	jle	.L213
	cmpq	$0, 16(%rcx)
	je	.L186
	.p2align 4,,10
	.p2align 3
.L213:
	movq	16(%rcx), %rsi
	jmp	.L214
	.p2align 4,,10
	.p2align 3
.L193:
	movzbl	23(%r13), %esi
	andl	$15, %esi
	cmpl	$15, %esi
	je	.L195
	testl	%esi, %esi
	je	.L264
	cmpq	$0, 32(%r13)
	je	.L194
.L264:
	movq	32(%r13), %rdi
.L212:
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_18MayAliasEPNS1_4NodeES4_
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L267:
	cmpw	$50, %cx
	je	.L201
	cmpw	$237, %cx
	je	.L201
	cmpw	$30, %cx
	jne	.L184
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L268:
	cmpw	$30, %ax
	je	.L201
	cmpw	$50, %ax
	jne	.L184
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L195:
	movq	32(%r13), %rsi
	movl	8(%rsi), %edx
	testl	%edx, %edx
	jle	.L211
	cmpq	$0, 16(%rsi)
	je	.L194
.L211:
	movq	16(%rsi), %rdi
	jmp	.L212
.L269:
	movq	-136(%rbp), %rdi
	movl	$264, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r15
	jmp	.L204
.L271:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19317:
	.size	_ZNK2v88internal8compiler15LoadElimination16AbstractElements4KillEPNS1_4NodeES5_PNS0_4ZoneE, .-_ZNK2v88internal8compiler15LoadElimination16AbstractElements4KillEPNS1_4NodeES5_PNS0_4ZoneE
	.section	.text._ZNK2v88internal8compiler15LoadElimination16AbstractElements6EqualsEPKS3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler15LoadElimination16AbstractElements6EqualsEPKS3_
	.type	_ZNK2v88internal8compiler15LoadElimination16AbstractElements6EqualsEPKS3_, @function
_ZNK2v88internal8compiler15LoadElimination16AbstractElements6EqualsEPKS3_:
.LFB19318:
	.cfi_startproc
	endbr64
	cmpq	%rsi, %rdi
	je	.L292
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rsi), %r10
	movq	%rdi, %r8
	leaq	256(%rdi), %r11
	movq	8(%rsi), %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	16(%rsi), %rbx
.L282:
	movq	(%r8), %rdx
	testq	%rdx, %rdx
	je	.L274
	movq	8(%r8), %rax
	cmpq	%r10, %rdx
	movq	16(%r8), %rcx
	sete	%r13b
	cmpq	%r9, %rax
	sete	%r12b
	testb	%r12b, %r13b
	je	.L293
	cmpq	%rbx, %rcx
	je	.L274
.L293:
	cmpq	%rdx, 32(%rsi)
	sete	%r13b
	cmpq	%rax, 40(%rsi)
	sete	%r12b
	testb	%r12b, %r13b
	je	.L294
	cmpq	%rcx, 48(%rsi)
	je	.L274
.L294:
	cmpq	%rax, 72(%rsi)
	sete	%r13b
	cmpq	%rdx, 64(%rsi)
	sete	%r12b
	testb	%r12b, %r13b
	je	.L295
	cmpq	%rcx, 80(%rsi)
	je	.L274
.L295:
	cmpq	%rdx, 96(%rsi)
	sete	%r13b
	cmpq	%rax, 104(%rsi)
	sete	%r12b
	testb	%r12b, %r13b
	je	.L296
	cmpq	%rcx, 112(%rsi)
	je	.L274
.L296:
	cmpq	%rdx, 128(%rsi)
	sete	%r13b
	cmpq	%rax, 136(%rsi)
	sete	%r12b
	testb	%r12b, %r13b
	je	.L297
	cmpq	%rcx, 144(%rsi)
	je	.L274
.L297:
	cmpq	%rax, 168(%rsi)
	sete	%r13b
	cmpq	%rdx, 160(%rsi)
	sete	%r12b
	testb	%r12b, %r13b
	je	.L298
	cmpq	%rcx, 176(%rsi)
	je	.L274
.L298:
	cmpq	%rax, 200(%rsi)
	sete	%r13b
	cmpq	%rdx, 192(%rsi)
	sete	%r12b
	testb	%r12b, %r13b
	je	.L299
	cmpq	%rcx, 208(%rsi)
	je	.L274
.L299:
	cmpq	%rax, 232(%rsi)
	sete	%al
	cmpq	%rdx, 224(%rsi)
	sete	%dl
	andl	%edx, %eax
	cmpq	%rcx, 240(%rsi)
	sete	%dl
	andb	%dl, %al
	jne	.L274
.L272:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L274:
	.cfi_restore_state
	addq	$32, %r8
	cmpq	%r8, %r11
	jne	.L282
	leaq	256(%rsi), %r10
.L291:
	movq	(%rsi), %rdx
	testq	%rdx, %rdx
	je	.L283
	movq	8(%rsi), %rax
	cmpq	%rax, 8(%rdi)
	sete	%r9b
	cmpq	%rdx, (%rdi)
	movq	16(%rsi), %rcx
	sete	%r8b
	testb	%r8b, %r9b
	je	.L300
	cmpq	%rcx, 16(%rdi)
	je	.L283
.L300:
	cmpq	%rax, 40(%rdi)
	sete	%r9b
	cmpq	%rdx, 32(%rdi)
	sete	%r8b
	testb	%r8b, %r9b
	je	.L301
	cmpq	%rcx, 48(%rdi)
	je	.L283
.L301:
	cmpq	%rax, 72(%rdi)
	sete	%r9b
	cmpq	%rdx, 64(%rdi)
	sete	%r8b
	testb	%r8b, %r9b
	je	.L302
	cmpq	%rcx, 80(%rdi)
	je	.L283
.L302:
	cmpq	%rax, 104(%rdi)
	sete	%r9b
	cmpq	%rdx, 96(%rdi)
	sete	%r8b
	testb	%r8b, %r9b
	je	.L303
	cmpq	%rcx, 112(%rdi)
	je	.L283
.L303:
	cmpq	%rdx, 128(%rdi)
	sete	%r9b
	cmpq	%rax, 136(%rdi)
	sete	%r8b
	testb	%r8b, %r9b
	je	.L304
	cmpq	%rcx, 144(%rdi)
	je	.L283
.L304:
	cmpq	%rax, 168(%rdi)
	sete	%r9b
	cmpq	%rdx, 160(%rdi)
	sete	%r8b
	testb	%r8b, %r9b
	je	.L305
	cmpq	%rcx, 176(%rdi)
	je	.L283
.L305:
	cmpq	%rax, 200(%rdi)
	sete	%r9b
	cmpq	%rdx, 192(%rdi)
	sete	%r8b
	testb	%r8b, %r9b
	je	.L306
	cmpq	%rcx, 208(%rdi)
	je	.L283
.L306:
	cmpq	%rax, 232(%rdi)
	sete	%al
	cmpq	%rdx, 224(%rdi)
	sete	%dl
	andl	%edx, %eax
	cmpq	%rcx, 240(%rdi)
	sete	%dl
	andb	%dl, %al
	je	.L272
	.p2align 4,,10
	.p2align 3
.L283:
	addq	$32, %rsi
	cmpq	%rsi, %r10
	jne	.L291
	movl	$1, %eax
	jmp	.L272
	.p2align 4,,10
	.p2align 3
.L292:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE19318:
	.size	_ZNK2v88internal8compiler15LoadElimination16AbstractElements6EqualsEPKS3_, .-_ZNK2v88internal8compiler15LoadElimination16AbstractElements6EqualsEPKS3_
	.section	.text._ZNK2v88internal8compiler15LoadElimination16AbstractElements5MergeEPKS3_PNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler15LoadElimination16AbstractElements5MergeEPKS3_PNS0_4ZoneE
	.type	_ZNK2v88internal8compiler15LoadElimination16AbstractElements5MergeEPKS3_PNS0_4ZoneE, @function
_ZNK2v88internal8compiler15LoadElimination16AbstractElements5MergeEPKS3_PNS0_4ZoneE:
.LFB19319:
	.cfi_startproc
	endbr64
	cmpq	%rsi, %rdi
	je	.L401
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	256(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	(%rsi), %r10
	movq	8(%rsi), %r9
	movq	16(%rsi), %r15
	movq	%rdi, %rsi
.L378:
	movq	(%rsi), %rax
	testq	%rax, %rax
	je	.L369
	movq	8(%rsi), %rcx
	cmpq	%r10, %rax
	movq	16(%rsi), %r8
	sete	%r14b
	cmpq	%r9, %rcx
	sete	%r11b
	testb	%r11b, %r14b
	je	.L403
	cmpq	%r15, %r8
	je	.L369
.L403:
	cmpq	%rcx, 40(%rbx)
	sete	%r14b
	cmpq	%rax, 32(%rbx)
	sete	%r11b
	testb	%r11b, %r14b
	je	.L404
	cmpq	%r8, 48(%rbx)
	je	.L369
.L404:
	cmpq	%rax, 64(%rbx)
	sete	%r14b
	cmpq	%rcx, 72(%rbx)
	sete	%r11b
	testb	%r11b, %r14b
	je	.L405
	cmpq	%r8, 80(%rbx)
	je	.L369
.L405:
	cmpq	%rcx, 104(%rbx)
	sete	%r14b
	cmpq	%rax, 96(%rbx)
	sete	%r11b
	testb	%r11b, %r14b
	je	.L406
	cmpq	%r8, 112(%rbx)
	je	.L369
.L406:
	cmpq	%rax, 128(%rbx)
	sete	%r14b
	cmpq	%rcx, 136(%rbx)
	sete	%r11b
	testb	%r11b, %r14b
	je	.L407
	cmpq	%r8, 144(%rbx)
	je	.L369
.L407:
	cmpq	%rax, 160(%rbx)
	sete	%r14b
	cmpq	%rcx, 168(%rbx)
	sete	%r11b
	testb	%r11b, %r14b
	je	.L408
	cmpq	%r8, 176(%rbx)
	je	.L369
.L408:
	cmpq	%rcx, 200(%rbx)
	sete	%r14b
	cmpq	%rax, 192(%rbx)
	sete	%r11b
	testb	%r11b, %r14b
	je	.L409
	cmpq	%r8, 208(%rbx)
	je	.L369
.L409:
	cmpq	%rcx, 232(%rbx)
	sete	%cl
	cmpq	%rax, 224(%rbx)
	sete	%al
	testb	%al, %cl
	je	.L377
	cmpq	%r8, 240(%rbx)
	je	.L369
.L377:
	movq	16(%rdx), %rsi
	movq	24(%rdx), %rax
	subq	%rsi, %rax
	cmpq	$263, %rax
	jbe	.L521
	leaq	264(%rsi), %rax
	movq	%rax, 16(%rdx)
.L388:
	movl	$32, %ecx
	xorl	%eax, %eax
	movq	%rsi, %rdi
	movq	%rsi, %r8
	rep stosq
	movq	$0, 256(%rsi)
	.p2align 4,,10
	.p2align 3
.L398:
	movdqu	(%r12), %xmm1
	movdqu	16(%r12), %xmm2
	movq	(%r12), %rax
	movaps	%xmm1, -80(%rbp)
	movaps	%xmm2, -64(%rbp)
	testq	%rax, %rax
	je	.L389
	cmpq	%rax, (%rbx)
	movq	8(%r12), %rdx
	sete	%r9b
	cmpq	%rdx, 8(%rbx)
	movq	16(%r12), %rcx
	sete	%dil
	testb	%dil, %r9b
	je	.L417
	cmpq	%rcx, 16(%rbx)
	je	.L390
.L417:
	cmpq	%rdx, 40(%rbx)
	sete	%r9b
	cmpq	%rax, 32(%rbx)
	sete	%dil
	testb	%dil, %r9b
	je	.L418
	cmpq	%rcx, 48(%rbx)
	je	.L390
.L418:
	cmpq	%rdx, 72(%rbx)
	sete	%r9b
	cmpq	%rax, 64(%rbx)
	sete	%dil
	testb	%dil, %r9b
	je	.L419
	cmpq	%rcx, 80(%rbx)
	je	.L390
.L419:
	cmpq	%rdx, 104(%rbx)
	sete	%r9b
	cmpq	%rax, 96(%rbx)
	sete	%dil
	testb	%dil, %r9b
	je	.L420
	cmpq	%rcx, 112(%rbx)
	je	.L390
.L420:
	cmpq	%rdx, 136(%rbx)
	sete	%r9b
	cmpq	%rax, 128(%rbx)
	sete	%dil
	testb	%dil, %r9b
	je	.L421
	cmpq	%rcx, 144(%rbx)
	je	.L390
.L421:
	cmpq	%rdx, 168(%rbx)
	sete	%r9b
	cmpq	%rax, 160(%rbx)
	sete	%dil
	testb	%dil, %r9b
	je	.L422
	cmpq	%rcx, 176(%rbx)
	je	.L390
.L422:
	cmpq	%rdx, 200(%rbx)
	sete	%r9b
	cmpq	%rax, 192(%rbx)
	sete	%dil
	testb	%dil, %r9b
	je	.L423
	cmpq	%rcx, 208(%rbx)
	je	.L390
.L423:
	cmpq	%rax, 224(%rbx)
	sete	%r9b
	cmpq	%rdx, 232(%rbx)
	sete	%dil
	testb	%dil, %r9b
	je	.L389
	cmpq	%rcx, 240(%rbx)
	je	.L390
.L389:
	addq	$32, %r12
	cmpq	%r13, %r12
	jne	.L398
	andq	$7, 256(%rsi)
.L367:
	addq	$40, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L369:
	.cfi_restore_state
	addq	$32, %rsi
	cmpq	%rsi, %r13
	jne	.L378
	movq	%rbx, %rsi
	leaq	256(%rbx), %r11
.L387:
	movq	(%rsi), %rax
	testq	%rax, %rax
	je	.L379
	cmpq	%rax, (%rdi)
	movq	8(%rsi), %rcx
	sete	%r10b
	cmpq	%rcx, 8(%rdi)
	movq	16(%rsi), %r8
	sete	%r9b
	testb	%r9b, %r10b
	je	.L410
	cmpq	%r8, 16(%rdi)
	je	.L379
.L410:
	cmpq	%rcx, 40(%rdi)
	sete	%r10b
	cmpq	%rax, 32(%rdi)
	sete	%r9b
	testb	%r9b, %r10b
	je	.L411
	cmpq	%r8, 48(%rdi)
	je	.L379
.L411:
	cmpq	%rax, 64(%rdi)
	sete	%r10b
	cmpq	%rcx, 72(%rdi)
	sete	%r9b
	testb	%r9b, %r10b
	je	.L412
	cmpq	%r8, 80(%rdi)
	je	.L379
.L412:
	cmpq	%rcx, 104(%rdi)
	sete	%r10b
	cmpq	%rax, 96(%rdi)
	sete	%r9b
	testb	%r9b, %r10b
	je	.L413
	cmpq	%r8, 112(%rdi)
	je	.L379
.L413:
	cmpq	%rcx, 136(%rdi)
	sete	%r10b
	cmpq	%rax, 128(%rdi)
	sete	%r9b
	testb	%r9b, %r10b
	je	.L414
	cmpq	%r8, 144(%rdi)
	je	.L379
.L414:
	cmpq	%rcx, 168(%rdi)
	sete	%r10b
	cmpq	%rax, 160(%rdi)
	sete	%r9b
	testb	%r9b, %r10b
	je	.L415
	cmpq	%r8, 176(%rdi)
	je	.L379
.L415:
	cmpq	%rax, 192(%rdi)
	sete	%r10b
	cmpq	%rcx, 200(%rdi)
	sete	%r9b
	testb	%r9b, %r10b
	je	.L416
	cmpq	%r8, 208(%rdi)
	je	.L379
.L416:
	cmpq	%rax, 224(%rdi)
	sete	%r9b
	cmpq	%rcx, 232(%rdi)
	sete	%al
	testb	%al, %r9b
	je	.L377
	cmpq	%r8, 240(%rdi)
	jne	.L377
.L379:
	addq	$32, %rsi
	cmpq	%r11, %rsi
	jne	.L387
	movq	%rdi, %r8
	jmp	.L367
.L401:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	movq	%rdi, %rax
	ret
.L390:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	256(%rsi), %rdi
	movq	%rax, %xmm0
	movq	%rdx, %xmm3
	movzbl	-56(%rbp), %edx
	punpcklqdq	%xmm3, %xmm0
	movq	%rcx, -64(%rbp)
	leaq	1(%rdi), %r9
	salq	$5, %rdi
	movaps	%xmm0, -80(%rbp)
	leaq	(%rsi,%rdi), %rax
	movq	%r9, 256(%rsi)
	movq	%rcx, 16(%rax)
	movb	%dl, 24(%rax)
	movups	%xmm0, (%rax)
	jmp	.L389
.L521:
	movl	$264, %esi
	movq	%rdx, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rsi
	jmp	.L388
	.cfi_endproc
.LFE19319:
	.size	_ZNK2v88internal8compiler15LoadElimination16AbstractElements5MergeEPKS3_PNS0_4ZoneE, .-_ZNK2v88internal8compiler15LoadElimination16AbstractElements5MergeEPKS3_PNS0_4ZoneE
	.section	.rodata._ZNK2v88internal8compiler15LoadElimination16AbstractElements5PrintEv.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"    #%d:%s @ #%d:%s -> #%d:%s\n"
	.section	.text._ZNK2v88internal8compiler15LoadElimination16AbstractElements5PrintEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler15LoadElimination16AbstractElements5PrintEv
	.type	_ZNK2v88internal8compiler15LoadElimination16AbstractElements5PrintEv, @function
_ZNK2v88internal8compiler15LoadElimination16AbstractElements5PrintEv:
.LFB19320:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	.LC1(%rip), %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	256(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	.p2align 4,,10
	.p2align 3
.L524:
	movq	(%rbx), %rax
	testq	%rax, %rax
	je	.L523
	movq	8(%rbx), %rcx
	movq	16(%rbx), %rdx
	subq	$8, %rsp
	movq	%r13, %rdi
	movq	(%rcx), %rsi
	movl	20(%rdx), %r9d
	movl	20(%rcx), %ecx
	movq	8(%rsi), %r8
	movq	(%rax), %rsi
	andl	$16777215, %r9d
	andl	$16777215, %ecx
	movq	8(%rsi), %r10
	movl	20(%rax), %esi
	movq	(%rdx), %rax
	andl	$16777215, %esi
	movq	%r10, %rdx
	pushq	8(%rax)
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	popq	%rax
	popq	%rdx
.L523:
	addq	$32, %rbx
	cmpq	%rbx, %r12
	jne	.L524
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19320:
	.size	_ZNK2v88internal8compiler15LoadElimination16AbstractElements5PrintEv, .-_ZNK2v88internal8compiler15LoadElimination16AbstractElements5PrintEv
	.section	.text._ZNK2v88internal8compiler15LoadElimination13AbstractField6LookupEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler15LoadElimination13AbstractField6LookupEPNS1_4NodeE
	.type	_ZNK2v88internal8compiler15LoadElimination13AbstractField6LookupEPNS1_4NodeE, @function
_ZNK2v88internal8compiler15LoadElimination13AbstractField6LookupEPNS1_4NodeE:
.LFB19321:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	32(%rdi), %r8
	leaq	16(%rdi), %rbx
	cmpq	%rbx, %r8
	je	.L537
	movq	%rsi, %r12
	.p2align 4,,10
	.p2align 3
.L536:
	movq	32(%r8), %r9
	movzbl	23(%r9), %edx
	leaq	32(%r9), %rax
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L533
	movq	32(%r9), %rax
	movl	8(%rax), %edx
	addq	$16, %rax
.L533:
	testl	%edx, %edx
	jle	.L534
	cmpq	$0, (%rax)
	je	.L535
.L534:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	movq	%r9, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	cmpq	%rax, %rsi
	je	.L540
.L535:
	movq	%r8, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r8
	cmpq	%rax, %rbx
	jne	.L536
.L537:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L540:
	.cfi_restore_state
	popq	%rbx
	leaq	40(%r8), %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19321:
	.size	_ZNK2v88internal8compiler15LoadElimination13AbstractField6LookupEPNS1_4NodeE, .-_ZNK2v88internal8compiler15LoadElimination13AbstractField6LookupEPNS1_4NodeE
	.section	.rodata._ZNK2v88internal8compiler15LoadElimination13AbstractField5PrintEv.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"    #%d:%s -> #%d:%s [repr=%s]\n"
	.section	.text._ZNK2v88internal8compiler15LoadElimination13AbstractField5PrintEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler15LoadElimination13AbstractField5PrintEv
	.type	_ZNK2v88internal8compiler15LoadElimination13AbstractField5PrintEv, @function
_ZNK2v88internal8compiler15LoadElimination13AbstractField5PrintEv:
.LFB19331:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	16(%rdi), %rbx
	subq	$8, %rsp
	movq	32(%rdi), %r12
	cmpq	%rbx, %r12
	je	.L541
	leaq	.LC2(%rip), %r13
	.p2align 4,,10
	.p2align 3
.L543:
	movzbl	48(%r12), %edi
	movq	32(%r12), %r14
	movq	40(%r12), %r15
	call	_ZN2v88internal19MachineReprToStringENS0_21MachineRepresentationE@PLT
	movl	20(%r14), %esi
	movq	(%r15), %rdi
	movq	%rax, %r9
	movq	(%r14), %rax
	movl	20(%r15), %ecx
	andl	$16777215, %esi
	movq	8(%rax), %rdx
	movq	8(%rdi), %r8
	xorl	%eax, %eax
	movq	%r13, %rdi
	andl	$16777215, %ecx
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rbx, %rax
	jne	.L543
.L541:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19331:
	.size	_ZNK2v88internal8compiler15LoadElimination13AbstractField5PrintEv, .-_ZNK2v88internal8compiler15LoadElimination13AbstractField5PrintEv
	.section	.text._ZN2v88internal8compiler15LoadElimination12AbstractMapsC2EPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15LoadElimination12AbstractMapsC2EPNS0_4ZoneE
	.type	_ZN2v88internal8compiler15LoadElimination12AbstractMapsC2EPNS0_4ZoneE, @function
_ZN2v88internal8compiler15LoadElimination12AbstractMapsC2EPNS0_4ZoneE:
.LFB19339:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rax
	movq	%rsi, (%rdi)
	movl	$0, 16(%rdi)
	movq	$0, 24(%rdi)
	movq	%rax, 32(%rdi)
	movq	%rax, 40(%rdi)
	movq	$0, 48(%rdi)
	ret
	.cfi_endproc
.LFE19339:
	.size	_ZN2v88internal8compiler15LoadElimination12AbstractMapsC2EPNS0_4ZoneE, .-_ZN2v88internal8compiler15LoadElimination12AbstractMapsC2EPNS0_4ZoneE
	.globl	_ZN2v88internal8compiler15LoadElimination12AbstractMapsC1EPNS0_4ZoneE
	.set	_ZN2v88internal8compiler15LoadElimination12AbstractMapsC1EPNS0_4ZoneE,_ZN2v88internal8compiler15LoadElimination12AbstractMapsC2EPNS0_4ZoneE
	.section	.text._ZN2v88internal8compiler15LoadElimination12AbstractMapsC2EPNS1_4NodeENS0_13ZoneHandleSetINS0_3MapEEEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15LoadElimination12AbstractMapsC2EPNS1_4NodeENS0_13ZoneHandleSetINS0_3MapEEEPNS0_4ZoneE
	.type	_ZN2v88internal8compiler15LoadElimination12AbstractMapsC2EPNS1_4NodeENS0_13ZoneHandleSetINS0_3MapEEEPNS0_4ZoneE, @function
_ZN2v88internal8compiler15LoadElimination12AbstractMapsC2EPNS1_4NodeENS0_13ZoneHandleSetINS0_3MapEEEPNS0_4ZoneE:
.LFB19360:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	leaq	16(%rbx), %r15
	subq	$8, %rsp
	movq	%rcx, (%rbx)
	movl	$0, 16(%rbx)
	movq	$0, 24(%rbx)
	movq	%r15, 32(%rbx)
	movq	%r15, 40(%rbx)
	movq	$0, 48(%rbx)
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	movq	16(%r8), %r14
	movq	%rax, %r13
	movq	24(%r8), %rax
	subq	%r14, %rax
	cmpq	$47, %rax
	jbe	.L565
	leaq	48(%r14), %rax
	movq	%rax, 16(%r8)
.L549:
	movq	%r13, 32(%r14)
	movq	%r12, 40(%r14)
	movq	24(%rbx), %r12
	testq	%r12, %r12
	jne	.L551
	jmp	.L566
	.p2align 4,,10
	.p2align 3
.L567:
	movq	16(%r12), %rax
	movl	$1, %edx
	testq	%rax, %rax
	je	.L552
.L568:
	movq	%rax, %r12
.L551:
	movq	32(%r12), %rcx
	cmpq	%r13, %rcx
	ja	.L567
	movq	24(%r12), %rax
	xorl	%edx, %edx
	testq	%rax, %rax
	jne	.L568
.L552:
	testb	%dl, %dl
	jne	.L569
	cmpq	%r13, %rcx
	jnb	.L547
.L558:
	movl	$1, %edi
	cmpq	%r12, %r15
	jne	.L570
.L557:
	movq	%r15, %rcx
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 48(%rbx)
.L547:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L569:
	.cfi_restore_state
	cmpq	%r12, 32(%rbx)
	je	.L558
.L560:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	cmpq	32(%rax), %r13
	ja	.L558
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L570:
	.cfi_restore_state
	xorl	%edi, %edi
	cmpq	32(%r12), %r13
	setb	%dil
	jmp	.L557
	.p2align 4,,10
	.p2align 3
.L566:
	movq	%r15, %r12
	cmpq	32(%rbx), %r15
	jne	.L560
	movl	$1, %edi
	jmp	.L557
	.p2align 4,,10
	.p2align 3
.L565:
	movl	$48, %esi
	movq	%r8, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r14
	jmp	.L549
	.cfi_endproc
.LFE19360:
	.size	_ZN2v88internal8compiler15LoadElimination12AbstractMapsC2EPNS1_4NodeENS0_13ZoneHandleSetINS0_3MapEEEPNS0_4ZoneE, .-_ZN2v88internal8compiler15LoadElimination12AbstractMapsC2EPNS1_4NodeENS0_13ZoneHandleSetINS0_3MapEEEPNS0_4ZoneE
	.globl	_ZN2v88internal8compiler15LoadElimination12AbstractMapsC1EPNS1_4NodeENS0_13ZoneHandleSetINS0_3MapEEEPNS0_4ZoneE
	.set	_ZN2v88internal8compiler15LoadElimination12AbstractMapsC1EPNS1_4NodeENS0_13ZoneHandleSetINS0_3MapEEEPNS0_4ZoneE,_ZN2v88internal8compiler15LoadElimination12AbstractMapsC2EPNS1_4NodeENS0_13ZoneHandleSetINS0_3MapEEEPNS0_4ZoneE
	.section	.text._ZNK2v88internal8compiler15LoadElimination12AbstractMaps6LookupEPNS1_4NodeEPNS0_13ZoneHandleSetINS0_3MapEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler15LoadElimination12AbstractMaps6LookupEPNS1_4NodeEPNS0_13ZoneHandleSetINS0_3MapEEE
	.type	_ZNK2v88internal8compiler15LoadElimination12AbstractMaps6LookupEPNS1_4NodeEPNS0_13ZoneHandleSetINS0_3MapEEE, @function
_ZNK2v88internal8compiler15LoadElimination12AbstractMaps6LookupEPNS1_4NodeEPNS0_13ZoneHandleSetINS0_3MapEEE:
.LFB19362:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movq	%rsi, %rdi
	movq	%rdx, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	movq	24(%r8), %rdx
	testq	%rdx, %rdx
	je	.L577
	leaq	16(%r8), %rdi
	movq	%rdi, %rcx
	jmp	.L573
	.p2align 4,,10
	.p2align 3
.L581:
	movq	%rdx, %rcx
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L574
.L573:
	cmpq	%rax, 32(%rdx)
	jnb	.L581
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L573
.L574:
	xorl	%r8d, %r8d
	cmpq	%rcx, %rdi
	je	.L571
	cmpq	%rax, 32(%rcx)
	jbe	.L582
.L571:
	movl	%r8d, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L582:
	.cfi_restore_state
	movq	40(%rcx), %rax
	movl	$1, %r8d
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	%rax, (%rsi)
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L577:
	.cfi_restore_state
	xorl	%r8d, %r8d
	popq	%rbp
	.cfi_def_cfa 7, 8
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE19362:
	.size	_ZNK2v88internal8compiler15LoadElimination12AbstractMaps6LookupEPNS1_4NodeEPNS0_13ZoneHandleSetINS0_3MapEEE, .-_ZNK2v88internal8compiler15LoadElimination12AbstractMaps6LookupEPNS1_4NodeEPNS0_13ZoneHandleSetINS0_3MapEEE
	.section	.rodata._ZNK2v88internal8compiler15LoadElimination12AbstractMaps5PrintEv.str1.1,"aMS",@progbits,1
.LC3:
	.string	"    #"
.LC4:
	.string	":"
	.section	.rodata._ZNK2v88internal8compiler15LoadElimination12AbstractMaps5PrintEv.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"vector::_M_range_check: __n (which is %zu) >= this->size() (which is %zu)"
	.section	.rodata._ZNK2v88internal8compiler15LoadElimination12AbstractMaps5PrintEv.str1.1
.LC6:
	.string	"     - "
	.section	.text._ZNK2v88internal8compiler15LoadElimination12AbstractMaps5PrintEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler15LoadElimination12AbstractMaps5PrintEv
	.type	_ZNK2v88internal8compiler15LoadElimination12AbstractMaps5PrintEv, @function
_ZNK2v88internal8compiler15LoadElimination12AbstractMaps5PrintEv:
.LFB19372:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-400(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$408, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-320(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -448(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	stdout(%rip), %rdx
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movq	%r12, %rdi
	movups	%xmm0, -88(%rbp)
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movw	%ax, -96(%rbp)
	movq	$0, -104(%rbp)
	movups	%xmm0, -72(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	leaq	16(%rbx), %rcx
	movq	%rax, -400(%rbp)
	addq	$40, %rax
	movq	%rax, -320(%rbp)
	movq	32(%rbx), %rax
	movq	%rcx, -440(%rbp)
	movq	%rax, -432(%rbp)
	cmpq	%rcx, %rax
	je	.L597
	.p2align 4,,10
	.p2align 3
.L584:
	movq	-432(%rbp), %rax
	movl	$5, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r12, %rdi
	movq	32(%rax), %rbx
	movq	40(%rax), %r13
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rdi
	movl	20(%rbx), %esi
	andl	$16777215, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC4(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r14
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	movq	8(%rax), %rbx
	testq	%rbx, %rbx
	je	.L616
	movq	%rbx, %rdi
	call	strlen@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L586:
	movq	(%r14), %rax
	movq	-24(%rax), %rax
	movq	240(%r14,%rax), %rbx
	testq	%rbx, %rbx
	je	.L593
	cmpb	$0, 56(%rbx)
	je	.L588
	movsbl	67(%rbx), %esi
.L589:
	movq	%r14, %rdi
	movq	%r13, %r14
	call	_ZNSo3putEc@PLT
	andl	$3, %r14d
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	cmpq	$1, %r14
	je	.L590
	xorl	%ebx, %ebx
	jmp	.L596
	.p2align 4,,10
	.p2align 3
.L618:
	movsbl	67(%rdi), %esi
.L595:
	movq	%r15, %rdi
	addq	$1, %rbx
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
.L596:
	testq	%r14, %r14
	je	.L591
	movq	14(%r13), %rax
	subq	6(%r13), %rax
	leaq	-2(%r13), %r15
	sarq	$3, %rax
	cmpq	%rbx, %rax
	jbe	.L590
	movl	$7, %edx
	leaq	.LC6(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r15), %rax
	movq	16(%r15), %rdx
	movslq	%ebx, %rsi
	subq	%rax, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L617
	movq	(%rax,%rsi,8), %rax
.L598:
	movq	(%rax), %rax
	movq	%r12, %rdi
	leaq	-408(%rbp), %rsi
	movq	%rax, -408(%rbp)
	call	_ZN2v88internallsERSoRKNS0_5BriefE@PLT
	movq	%rax, %r15
	movq	(%rax), %rax
	movq	-24(%rax), %rax
	movq	240(%r15,%rax), %rdi
	testq	%rdi, %rdi
	je	.L593
	cmpb	$0, 56(%rdi)
	jne	.L618
	movq	%rdi, -424(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-424(%rbp), %rdi
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rcx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rcx, %rax
	je	.L595
	call	*%rax
	movsbl	%al, %esi
	jmp	.L595
	.p2align 4,,10
	.p2align 3
.L591:
	testq	%rbx, %rbx
	jne	.L590
	movl	$7, %edx
	leaq	.LC6(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r13, %rax
	jmp	.L598
	.p2align 4,,10
	.p2align 3
.L590:
	movq	-432(%rbp), %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, -432(%rbp)
	cmpq	%rax, -440(%rbp)
	jne	.L584
.L597:
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rcx
	movq	%rax, -320(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-336(%rbp), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -400(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	-448(%rbp), %rdi
	movq	%rax, -400(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L619
	addq	$408, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L588:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%rbx), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rcx
	movq	48(%rax), %rax
	cmpq	%rcx, %rax
	je	.L589
	movq	%rbx, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L589
	.p2align 4,,10
	.p2align 3
.L616:
	movq	(%r14), %rax
	movq	-24(%rax), %rdi
	addq	%r14, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L586
.L593:
	call	_ZSt16__throw_bad_castv@PLT
.L617:
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L619:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19372:
	.size	_ZNK2v88internal8compiler15LoadElimination12AbstractMaps5PrintEv, .-_ZNK2v88internal8compiler15LoadElimination12AbstractMaps5PrintEv
	.section	.text._ZNK2v88internal8compiler15LoadElimination13AbstractState12FieldsEqualsERKSt5arrayIPKNS2_13AbstractFieldELm32EESA_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler15LoadElimination13AbstractState12FieldsEqualsERKSt5arrayIPKNS2_13AbstractFieldELm32EESA_
	.type	_ZNK2v88internal8compiler15LoadElimination13AbstractState12FieldsEqualsERKSt5arrayIPKNS2_13AbstractFieldELm32EESA_, @function
_ZNK2v88internal8compiler15LoadElimination13AbstractState12FieldsEqualsERKSt5arrayIPKNS2_13AbstractFieldELm32EESA_:
.LFB19381:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$24, %rsp
.L627:
	movq	0(%r13,%rbx,8), %rcx
	movq	(%r12,%rbx,8), %rax
	testq	%rcx, %rcx
	je	.L621
	testq	%rax, %rax
	je	.L630
	cmpq	%rax, %rcx
	je	.L623
	movq	48(%rcx), %rdi
	cmpq	%rdi, 48(%rax)
	je	.L637
.L630:
	xorl	%eax, %eax
.L620:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L621:
	.cfi_restore_state
	testq	%rax, %rax
	jne	.L630
.L623:
	addq	$1, %rbx
	cmpq	$32, %rbx
	jne	.L627
	movl	$1, %eax
	jmp	.L620
	.p2align 4,,10
	.p2align 3
.L637:
	movq	32(%rax), %r14
	leaq	16(%rax), %rdx
	movq	32(%rcx), %r15
	cmpq	%r14, %rdx
	je	.L623
.L626:
	movq	32(%r15), %rax
	cmpq	%rax, 32(%r14)
	jne	.L630
	movq	40(%r15), %rax
	cmpq	%rax, 40(%r14)
	jne	.L630
	movzbl	48(%r15), %eax
	cmpb	%al, 48(%r14)
	jne	.L630
	movq	56(%r15), %rax
	cmpq	%rax, 56(%r14)
	jne	.L630
	leaq	64(%r15), %rsi
	leaq	64(%r14), %rdi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal8compilereqERKNS1_14ConstFieldInfoES4_@PLT
	testb	%al, %al
	je	.L630
	movq	%r14, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%r15, %rdi
	movq	%rax, %r14
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %r15
	cmpq	%r14, %rdx
	jne	.L626
	jmp	.L623
	.cfi_endproc
.LFE19381:
	.size	_ZNK2v88internal8compiler15LoadElimination13AbstractState12FieldsEqualsERKSt5arrayIPKNS2_13AbstractFieldELm32EESA_, .-_ZNK2v88internal8compiler15LoadElimination13AbstractState12FieldsEqualsERKSt5arrayIPKNS2_13AbstractFieldELm32EESA_
	.section	.text._ZNK2v88internal8compiler15LoadElimination13AbstractState6EqualsEPKS3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler15LoadElimination13AbstractState6EqualsEPKS3_
	.type	_ZNK2v88internal8compiler15LoadElimination13AbstractState6EqualsEPKS3_, @function
_ZNK2v88internal8compiler15LoadElimination13AbstractState6EqualsEPKS3_:
.LFB19383:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %rsi
	movq	(%r12), %rdi
	testq	%rsi, %rsi
	je	.L639
	testq	%rdi, %rdi
	je	.L653
	call	_ZNK2v88internal8compiler15LoadElimination16AbstractElements6EqualsEPKS3_
	xorl	%r13d, %r13d
	testb	%al, %al
	je	.L653
.L650:
	movq	8(%rbx,%r13,8), %rdx
	movq	8(%r12,%r13,8), %rax
	testq	%rdx, %rdx
	je	.L644
	testq	%rax, %rax
	je	.L653
	cmpq	%rax, %rdx
	je	.L645
	movq	48(%rdx), %rcx
	cmpq	%rcx, 48(%rax)
	je	.L678
.L653:
	xorl	%r13d, %r13d
.L638:
	addq	$24, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L644:
	.cfi_restore_state
	testq	%rax, %rax
	jne	.L653
.L645:
	addq	$1, %r13
	cmpq	$32, %r13
	jne	.L650
	leaq	264(%r12), %rdx
	leaq	264(%rbx), %rsi
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler15LoadElimination13AbstractState12FieldsEqualsERKSt5arrayIPKNS2_13AbstractFieldELm32EESA_
	movl	%eax, %r13d
	testb	%al, %al
	je	.L653
	movq	520(%rbx), %rdx
	movq	520(%r12), %rax
	testq	%rdx, %rdx
	je	.L651
	testq	%rax, %rax
	je	.L653
	cmpq	%rax, %rdx
	je	.L638
	movq	48(%rdx), %rcx
	cmpq	%rcx, 48(%rax)
	jne	.L653
	movq	32(%rax), %r12
	leaq	16(%rax), %rbx
	movq	32(%rdx), %r14
	cmpq	%r12, %rbx
	je	.L638
.L658:
	movq	32(%r14), %rax
	cmpq	%rax, 32(%r12)
	jne	.L653
	movq	40(%r12), %rax
	movq	40(%r14), %rdx
	cmpq	%rdx, %rax
	je	.L654
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$2, %rcx
	jne	.L653
	movq	%rdx, %rcx
	andl	$3, %ecx
	cmpq	$2, %rcx
	jne	.L653
	movq	6(%rax), %rcx
	movq	6(%rdx), %rdi
	subq	$2, %rax
	subq	$2, %rdx
	movq	16(%rax), %rax
	movq	16(%rdx), %rdx
	subq	%rcx, %rax
	subq	%rdi, %rdx
	cmpq	%rdx, %rax
	jne	.L653
	sarq	$3, %rax
	je	.L654
	movq	(%rcx), %rsi
	xorl	%edx, %edx
	jmp	.L656
.L679:
	addq	$1, %rdx
	cmpq	%rdx, %rax
	je	.L654
	movq	(%rcx,%rdx,8), %rsi
.L656:
	cmpq	%rsi, (%rdi,%rdx,8)
	je	.L679
	jmp	.L653
	.p2align 4,,10
	.p2align 3
.L639:
	testq	%rdi, %rdi
	jne	.L653
	xorl	%r13d, %r13d
	jmp	.L650
	.p2align 4,,10
	.p2align 3
.L678:
	movq	32(%rax), %r14
	movq	32(%rdx), %r15
	leaq	16(%rax), %rdx
	cmpq	%r14, %rdx
	je	.L645
.L648:
	movq	32(%r15), %rax
	cmpq	%rax, 32(%r14)
	jne	.L653
	movq	40(%r15), %rax
	cmpq	%rax, 40(%r14)
	jne	.L653
	movzbl	48(%r15), %eax
	cmpb	%al, 48(%r14)
	jne	.L653
	movq	56(%r15), %rax
	cmpq	%rax, 56(%r14)
	jne	.L653
	leaq	64(%r15), %rsi
	leaq	64(%r14), %rdi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal8compilereqERKNS1_14ConstFieldInfoES4_@PLT
	testb	%al, %al
	je	.L653
	movq	%r14, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%r15, %rdi
	movq	%rax, %r14
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %r15
	cmpq	%r14, %rdx
	jne	.L648
	jmp	.L645
	.p2align 4,,10
	.p2align 3
.L651:
	testq	%rax, %rax
	sete	%r13b
	jmp	.L638
.L654:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r14
	cmpq	%r12, %rbx
	jne	.L658
	jmp	.L638
	.cfi_endproc
.LFE19383:
	.size	_ZNK2v88internal8compiler15LoadElimination13AbstractState6EqualsEPKS3_, .-_ZNK2v88internal8compiler15LoadElimination13AbstractState6EqualsEPKS3_
	.section	.text._ZNK2v88internal8compiler15LoadElimination13AbstractState10LookupMapsEPNS1_4NodeEPNS0_13ZoneHandleSetINS0_3MapEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler15LoadElimination13AbstractState10LookupMapsEPNS1_4NodeEPNS0_13ZoneHandleSetINS0_3MapEEE
	.type	_ZNK2v88internal8compiler15LoadElimination13AbstractState10LookupMapsEPNS1_4NodeEPNS0_13ZoneHandleSetINS0_3MapEEE, @function
_ZNK2v88internal8compiler15LoadElimination13AbstractState10LookupMapsEPNS1_4NodeEPNS0_13ZoneHandleSetINS0_3MapEEE:
.LFB19386:
	.cfi_startproc
	endbr64
	movq	520(%rdi), %r8
	xorl	%r9d, %r9d
	testq	%r8, %r8
	je	.L692
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rdx, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	movq	24(%r8), %rcx
	leaq	16(%r8), %rdi
	testq	%rcx, %rcx
	je	.L680
	movq	%rdi, %rdx
	jmp	.L682
	.p2align 4,,10
	.p2align 3
.L695:
	movq	%rcx, %rdx
	movq	16(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L683
.L682:
	cmpq	%rax, 32(%rcx)
	jnb	.L695
	movq	24(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.L682
.L683:
	xorl	%r9d, %r9d
	cmpq	%rdx, %rdi
	je	.L680
	cmpq	%rax, 32(%rdx)
	jbe	.L696
.L680:
	movl	%r9d, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L696:
	.cfi_restore_state
	movq	40(%rdx), %rax
	movl	$1, %r9d
	movq	%rax, (%rsi)
	movl	%r9d, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L692:
	.cfi_restore 6
	movl	%r9d, %eax
	ret
	.cfi_endproc
.LFE19386:
	.size	_ZNK2v88internal8compiler15LoadElimination13AbstractState10LookupMapsEPNS1_4NodeEPNS0_13ZoneHandleSetINS0_3MapEEE, .-_ZNK2v88internal8compiler15LoadElimination13AbstractState10LookupMapsEPNS1_4NodeEPNS0_13ZoneHandleSetINS0_3MapEEE
	.section	.text._ZNK2v88internal8compiler15LoadElimination13AbstractState13LookupElementEPNS1_4NodeES5_NS0_21MachineRepresentationE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler15LoadElimination13AbstractState13LookupElementEPNS1_4NodeES5_NS0_21MachineRepresentationE
	.type	_ZNK2v88internal8compiler15LoadElimination13AbstractState13LookupElementEPNS1_4NodeES5_NS0_21MachineRepresentationE, @function
_ZNK2v88internal8compiler15LoadElimination13AbstractState13LookupElementEPNS1_4NodeES5_NS0_21MachineRepresentationE:
.LFB19390:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L697
	jmp	_ZNK2v88internal8compiler15LoadElimination16AbstractElements6LookupEPNS1_4NodeES5_NS0_21MachineRepresentationE
	.p2align 4,,10
	.p2align 3
.L697:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE19390:
	.size	_ZNK2v88internal8compiler15LoadElimination13AbstractState13LookupElementEPNS1_4NodeES5_NS0_21MachineRepresentationE, .-_ZNK2v88internal8compiler15LoadElimination13AbstractState13LookupElementEPNS1_4NodeES5_NS0_21MachineRepresentationE
	.section	.text._ZNK2v88internal8compiler15LoadElimination13AbstractState10AddElementEPNS1_4NodeES5_S5_NS0_21MachineRepresentationEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler15LoadElimination13AbstractState10AddElementEPNS1_4NodeES5_S5_NS0_21MachineRepresentationEPNS0_4ZoneE
	.type	_ZNK2v88internal8compiler15LoadElimination13AbstractState10AddElementEPNS1_4NodeES5_S5_NS0_21MachineRepresentationEPNS0_4ZoneE, @function
_ZNK2v88internal8compiler15LoadElimination13AbstractState10AddElementEPNS1_4NodeES5_S5_NS0_21MachineRepresentationEPNS0_4ZoneE:
.LFB19391:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%r8d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	16(%r9), %r13
	movq	24(%r9), %rax
	movq	%rsi, -40(%rbp)
	movq	%rdx, -48(%rbp)
	subq	%r13, %rax
	cmpq	$527, %rax
	jbe	.L709
	leaq	528(%r13), %rax
	movq	%rax, 16(%r9)
.L701:
	movq	%rbx, %rsi
	movl	$66, %ecx
	movq	%r13, %rdi
	rep movsq
	movq	0(%r13), %rbx
	testq	%rbx, %rbx
	je	.L702
	movq	16(%r9), %rax
	movq	24(%r9), %rdx
	subq	%rax, %rdx
	cmpq	$263, %rdx
	jbe	.L710
	leaq	264(%rax), %rdx
	movq	%rdx, 16(%r9)
.L704:
	movq	%rax, %rdi
	movq	%rbx, %rsi
	movl	$33, %ecx
	movq	-40(%rbp), %xmm0
	rep movsq
	movhps	-48(%rbp), %xmm0
	movq	256(%rax), %rdx
	salq	$5, %rdx
	addq	%rax, %rdx
	movq	%r14, 16(%rdx)
	movb	%r15b, 24(%rdx)
	movups	%xmm0, (%rdx)
	movq	256(%rax), %rbx
	leaq	1(%rbx), %rdx
	andl	$7, %edx
	movq	%rdx, 256(%rax)
	movq	%rax, 0(%r13)
	addq	$32, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L702:
	.cfi_restore_state
	movq	16(%r9), %rdx
	movq	24(%r9), %rax
	subq	%rdx, %rax
	cmpq	$263, %rax
	jbe	.L711
	leaq	264(%rdx), %rax
	movq	%rax, 16(%r9)
.L707:
	movq	-40(%rbp), %xmm0
	xorl	%eax, %eax
	movl	$32, %ecx
	movq	%rdx, %rdi
	rep stosq
	movq	%r14, 16(%rdx)
	movq	%r13, %rax
	movhps	-48(%rbp), %xmm0
	movb	%r15b, 24(%rdx)
	movq	$1, 256(%rdx)
	movups	%xmm0, (%rdx)
	movq	%rdx, 0(%r13)
	addq	$32, %rsp
	popq	%rbx
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L709:
	.cfi_restore_state
	movq	%r9, %rdi
	movl	$528, %esi
	movq	%r9, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %r9
	movq	%rax, %r13
	jmp	.L701
	.p2align 4,,10
	.p2align 3
.L710:
	movl	$264, %esi
	movq	%r9, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L704
	.p2align 4,,10
	.p2align 3
.L711:
	movl	$264, %esi
	movq	%r9, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L707
	.cfi_endproc
.LFE19391:
	.size	_ZNK2v88internal8compiler15LoadElimination13AbstractState10AddElementEPNS1_4NodeES5_S5_NS0_21MachineRepresentationEPNS0_4ZoneE, .-_ZNK2v88internal8compiler15LoadElimination13AbstractState10AddElementEPNS1_4NodeES5_S5_NS0_21MachineRepresentationEPNS0_4ZoneE
	.section	.text._ZNK2v88internal8compiler15LoadElimination13AbstractState11KillElementEPNS1_4NodeES5_PNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler15LoadElimination13AbstractState11KillElementEPNS1_4NodeES5_PNS0_4ZoneE
	.type	_ZNK2v88internal8compiler15LoadElimination13AbstractState11KillElementEPNS1_4NodeES5_PNS0_4ZoneE, @function
_ZNK2v88internal8compiler15LoadElimination13AbstractState11KillElementEPNS1_4NodeES5_PNS0_4ZoneE:
.LFB19392:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L715
	movq	%rcx, %r12
	call	_ZNK2v88internal8compiler15LoadElimination16AbstractElements4KillEPNS1_4NodeES5_PNS0_4ZoneE
	movq	%rax, %r13
	cmpq	%rax, (%rbx)
	jne	.L719
.L715:
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L719:
	.cfi_restore_state
	movq	16(%r12), %rax
	movq	24(%r12), %rdx
	subq	%rax, %rdx
	cmpq	$527, %rdx
	jbe	.L720
	leaq	528(%rax), %rdx
	movq	%rdx, 16(%r12)
.L717:
	movq	%rbx, %rsi
	movl	$66, %ecx
	movq	%rax, %rdi
	rep movsq
	movq	%r13, (%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L720:
	.cfi_restore_state
	movl	$528, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L717
	.cfi_endproc
.LFE19392:
	.size	_ZNK2v88internal8compiler15LoadElimination13AbstractState11KillElementEPNS1_4NodeES5_PNS0_4ZoneE, .-_ZNK2v88internal8compiler15LoadElimination13AbstractState11KillElementEPNS1_4NodeES5_PNS0_4ZoneE
	.section	.text._ZNK2v88internal8compiler15LoadElimination13AbstractState7KillAllEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler15LoadElimination13AbstractState7KillAllEPNS0_4ZoneE
	.type	_ZNK2v88internal8compiler15LoadElimination13AbstractState7KillAllEPNS0_4ZoneE, @function
_ZNK2v88internal8compiler15LoadElimination13AbstractState7KillAllEPNS0_4ZoneE:
.LFB19398:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	leaq	264(%rbx), %rax
	leaq	520(%rbx), %rdx
	subq	$8, %rsp
	jmp	.L726
	.p2align 4,,10
	.p2align 3
.L722:
	addq	$8, %rax
	cmpq	%rdx, %rax
	je	.L729
.L726:
	cmpq	$0, (%rax)
	je	.L722
	movq	16(%rdi), %r8
	movq	24(%rdi), %rax
	subq	%r8, %rax
	cmpq	$527, %rax
	jbe	.L730
	leaq	528(%r8), %rax
	movq	%rax, 16(%rdi)
.L724:
	xorl	%eax, %eax
	leaq	8(%r8), %rdi
	movl	$32, %ecx
	movq	$0, (%r8)
	rep stosq
	leaq	264(%r8), %rdi
	movl	$32, %ecx
	rep stosq
	movq	%r8, %rax
	movq	$0, 520(%r8)
	movdqu	264(%rbx), %xmm0
	movups	%xmm0, 264(%r8)
	movdqu	280(%rbx), %xmm1
	movups	%xmm1, 280(%r8)
	movdqu	296(%rbx), %xmm2
	movups	%xmm2, 296(%r8)
	movdqu	312(%rbx), %xmm3
	movups	%xmm3, 312(%r8)
	movdqu	328(%rbx), %xmm4
	movups	%xmm4, 328(%r8)
	movdqu	344(%rbx), %xmm5
	movups	%xmm5, 344(%r8)
	movdqu	360(%rbx), %xmm6
	movups	%xmm6, 360(%r8)
	movdqu	376(%rbx), %xmm7
	movups	%xmm7, 376(%r8)
	movdqu	392(%rbx), %xmm0
	movups	%xmm0, 392(%r8)
	movdqu	408(%rbx), %xmm1
	movups	%xmm1, 408(%r8)
	movdqu	424(%rbx), %xmm2
	movups	%xmm2, 424(%r8)
	movdqu	440(%rbx), %xmm3
	movups	%xmm3, 440(%r8)
	movdqu	456(%rbx), %xmm4
	movups	%xmm4, 456(%r8)
	movdqu	472(%rbx), %xmm5
	movups	%xmm5, 472(%r8)
	movdqu	488(%rbx), %xmm6
	movups	%xmm6, 488(%r8)
	movdqu	504(%rbx), %xmm7
	movups	%xmm7, 504(%r8)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L729:
	.cfi_restore_state
	addq	$8, %rsp
	leaq	_ZN2v88internal8compiler15LoadElimination13AbstractState12empty_state_E(%rip), %r8
	movq	%r8, %rax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L730:
	.cfi_restore_state
	movl	$528, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r8
	jmp	.L724
	.cfi_endproc
.LFE19398:
	.size	_ZNK2v88internal8compiler15LoadElimination13AbstractState7KillAllEPNS0_4ZoneE, .-_ZNK2v88internal8compiler15LoadElimination13AbstractState7KillAllEPNS0_4ZoneE
	.section	.text._ZNK2v88internal8compiler15LoadElimination13AbstractState11LookupFieldEPNS1_4NodeENS2_10IndexRangeENS1_14ConstFieldInfoE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler15LoadElimination13AbstractState11LookupFieldEPNS1_4NodeENS2_10IndexRangeENS1_14ConstFieldInfoE
	.type	_ZNK2v88internal8compiler15LoadElimination13AbstractState11LookupFieldEPNS1_4NodeENS2_10IndexRangeENS1_14ConstFieldInfoE, @function
_ZNK2v88internal8compiler15LoadElimination13AbstractState11LookupFieldEPNS1_4NodeENS2_10IndexRangeENS1_14ConstFieldInfoE:
.LFB19399:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rax
	sarq	$32, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, -56(%rbp)
	cmpl	%eax, %edx
	je	.L735
	subl	$1, %eax
	movslq	%edx, %rcx
	xorl	%r14d, %r14d
	xorl	%r15d, %r15d
	subl	%edx, %eax
	movq	%rsi, %r13
	leaq	(%rdi,%rcx,8), %r12
	addq	%rcx, %rax
	cmpq	$0, -56(%rbp)
	leaq	8(%rdi,%rax,8), %rax
	movq	%rax, -64(%rbp)
	leaq	-56(%rbp), %rax
	movq	%rax, -72(%rbp)
	je	.L734
	.p2align 4,,10
	.p2align 3
.L761:
	movq	264(%r12), %rbx
	testq	%rbx, %rbx
	je	.L735
	movq	32(%rbx), %r8
	addq	$16, %rbx
	cmpq	%rbx, %r8
	je	.L735
	.p2align 4,,10
	.p2align 3
.L741:
	movq	32(%r8), %r9
	movzbl	23(%r9), %eax
	leaq	32(%r9), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L737
	movq	32(%r9), %rdx
	movl	8(%rdx), %eax
	addq	$16, %rdx
.L737:
	testl	%eax, %eax
	jle	.L738
	cmpq	$0, (%rdx)
	je	.L739
.L738:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	movq	%r9, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	cmpq	%rax, %rsi
	je	.L760
.L739:
	movq	%r8, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r8
	cmpq	%rax, %rbx
	jne	.L741
.L735:
	xorl	%r15d, %r15d
.L731:
	addq	$40, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L760:
	.cfi_restore_state
	movq	-72(%rbp), %rsi
	leaq	64(%r8), %rdi
	leaq	40(%r8), %rbx
	call	_ZN2v88internal8compilereqERKNS1_14ConstFieldInfoES4_@PLT
	testb	%al, %al
	je	.L735
.L740:
	testb	%r14b, %r14b
	jne	.L747
	movq	%rbx, %r15
	movl	$1, %r14d
.L747:
	addq	$8, %r12
	cmpq	%r12, -64(%rbp)
	je	.L731
	cmpq	$0, -56(%rbp)
	jne	.L761
.L734:
	movq	8(%r12), %rbx
	testq	%rbx, %rbx
	je	.L735
	movq	32(%rbx), %r8
	addq	$16, %rbx
	cmpq	%rbx, %r8
	je	.L735
	.p2align 4,,10
	.p2align 3
.L746:
	movq	32(%r8), %r9
	movzbl	23(%r9), %edx
	leaq	32(%r9), %rax
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L743
	movq	32(%r9), %rax
	movl	8(%rax), %edx
	addq	$16, %rax
.L743:
	testl	%edx, %edx
	jle	.L744
	cmpq	$0, (%rax)
	je	.L745
.L744:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	movq	%r9, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	cmpq	%rax, %rsi
	je	.L762
.L745:
	movq	%r8, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r8
	cmpq	%rax, %rbx
	jne	.L746
	jmp	.L735
	.p2align 4,,10
	.p2align 3
.L762:
	leaq	40(%r8), %rbx
	jmp	.L740
	.cfi_endproc
.LFE19399:
	.size	_ZNK2v88internal8compiler15LoadElimination13AbstractState11LookupFieldEPNS1_4NodeENS2_10IndexRangeENS1_14ConstFieldInfoE, .-_ZNK2v88internal8compiler15LoadElimination13AbstractState11LookupFieldEPNS1_4NodeENS2_10IndexRangeENS1_14ConstFieldInfoE
	.section	.text._ZNK2v88internal8compiler15LoadElimination14AliasStateInfo8MayAliasEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler15LoadElimination14AliasStateInfo8MayAliasEPNS1_4NodeE
	.type	_ZNK2v88internal8compiler15LoadElimination14AliasStateInfo8MayAliasEPNS1_4NodeE, @function
_ZNK2v88internal8compiler15LoadElimination14AliasStateInfo8MayAliasEPNS1_4NodeE:
.LFB19414:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	movq	8(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	0(%r13), %rax
	cmpw	$237, 16(%rax)
	je	.L834
	movq	%rdi, %rbx
	cmpq	%rsi, %r13
	je	.L767
	movq	8(%r13), %rax
	movq	8(%rsi), %rsi
	leaq	-48(%rbp), %rdi
	movq	%rax, -48(%rbp)
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	je	.L806
	movq	(%r12), %rax
	movzwl	16(%rax), %eax
	cmpw	$58, %ax
	je	.L769
	cmpw	$228, %ax
	je	.L769
	cmpw	$40, %ax
	je	.L769
.L770:
	movq	0(%r13), %rdx
	movzwl	16(%rdx), %edx
	cmpw	$58, %dx
	je	.L777
	cmpw	$228, %dx
	je	.L777
	cmpw	$40, %dx
	je	.L777
.L778:
	cmpw	$237, %ax
	je	.L835
	cmpw	$237, %dx
	jne	.L767
	cmpw	$30, %ax
	je	.L806
	cmpw	$50, %ax
	je	.L806
	.p2align 4,,10
	.p2align 3
.L767:
	movq	16(%rbx), %r8
	testq	%r8, %r8
	je	.L812
	movq	(%rbx), %rax
	movq	520(%rax), %rsi
	testq	%rsi, %rsi
	je	.L812
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	movq	24(%rsi), %rdx
	leaq	16(%rsi), %rdi
	testq	%rdx, %rdx
	je	.L812
	movq	%rdi, %rcx
	jmp	.L787
	.p2align 4,,10
	.p2align 3
.L836:
	movq	%rdx, %rcx
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L788
.L787:
	cmpq	%rax, 32(%rdx)
	jnb	.L836
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L787
.L788:
	cmpq	%rcx, %rdi
	je	.L812
	cmpq	%rax, 32(%rcx)
	ja	.L812
	movq	40(%rcx), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	je	.L812
	testq	%rdx, %rdx
	je	.L791
	movq	14(%rax), %rcx
	movq	6(%rax), %rdx
	movl	$1, %eax
	movq	%rcx, %rsi
	subq	%rdx, %rsi
	cmpq	$8, %rsi
	jne	.L763
	cmpq	%rcx, %rdx
	je	.L837
	movq	(%rdx), %rax
.L791:
	cmpq	%rax, %r8
	jne	.L806
	.p2align 4,,10
	.p2align 3
.L812:
	movl	$1, %eax
.L763:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L838
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L834:
	.cfi_restore_state
	cmpq	%rsi, %r13
	sete	%al
	jmp	.L763
	.p2align 4,,10
	.p2align 3
.L769:
	movzbl	23(%r12), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L771
	testl	%edx, %edx
	je	.L832
	cmpq	$0, 32(%r12)
	je	.L770
.L832:
	movq	32(%r12), %rsi
.L796:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_18MayAliasEPNS1_4NodeES4_
.L776:
	testb	%al, %al
	jne	.L767
.L806:
	xorl	%eax, %eax
	jmp	.L763
	.p2align 4,,10
	.p2align 3
.L777:
	movzbl	23(%r13), %ecx
	andl	$15, %ecx
	cmpl	$15, %ecx
	je	.L779
	testl	%ecx, %ecx
	je	.L831
	cmpq	$0, 32(%r13)
	je	.L778
.L831:
	movq	32(%r13), %rdi
.L794:
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_18MayAliasEPNS1_4NodeES4_
	jmp	.L776
	.p2align 4,,10
	.p2align 3
.L779:
	movq	32(%r13), %rcx
	movl	8(%rcx), %esi
	testl	%esi, %esi
	jle	.L793
	cmpq	$0, 16(%rcx)
	je	.L778
.L793:
	movq	16(%rcx), %rdi
	jmp	.L794
	.p2align 4,,10
	.p2align 3
.L835:
	cmpw	$50, %dx
	je	.L806
	cmpw	$237, %dx
	je	.L806
	cmpw	$30, %dx
	jne	.L767
	xorl	%eax, %eax
	jmp	.L763
	.p2align 4,,10
	.p2align 3
.L771:
	movq	32(%r12), %rdx
	movl	8(%rdx), %edi
	testl	%edi, %edi
	jle	.L795
	cmpq	$0, 16(%rdx)
	je	.L770
	.p2align 4,,10
	.p2align 3
.L795:
	movq	16(%rdx), %rsi
	jmp	.L796
.L838:
	call	__stack_chk_fail@PLT
.L837:
	xorl	%edx, %edx
	xorl	%esi, %esi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE19414:
	.size	_ZNK2v88internal8compiler15LoadElimination14AliasStateInfo8MayAliasEPNS1_4NodeE, .-_ZNK2v88internal8compiler15LoadElimination14AliasStateInfo8MayAliasEPNS1_4NodeE
	.section	.rodata._ZNK2v88internal8compiler15LoadElimination13AbstractState5PrintEv.str1.1,"aMS",@progbits,1
.LC7:
	.string	"   maps:\n"
.LC8:
	.string	"   elements:\n"
.LC9:
	.string	"   field %zu:\n"
.LC10:
	.string	"   const field %zu:\n"
	.section	.text._ZNK2v88internal8compiler15LoadElimination13AbstractState5PrintEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler15LoadElimination13AbstractState5PrintEv
	.type	_ZNK2v88internal8compiler15LoadElimination13AbstractState5PrintEv, @function
_ZNK2v88internal8compiler15LoadElimination13AbstractState5PrintEv:
.LFB19415:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpq	$0, 520(%rdi)
	je	.L840
	leaq	.LC7(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	520(%r13), %rdi
	call	_ZNK2v88internal8compiler15LoadElimination12AbstractMaps5PrintEv
.L840:
	cmpq	$0, 0(%r13)
	je	.L841
	leaq	.LC8(%rip), %rdi
	xorl	%eax, %eax
	leaq	.LC1(%rip), %r14
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	0(%r13), %rbx
	leaq	256(%rbx), %r12
	.p2align 4,,10
	.p2align 3
.L843:
	movq	(%rbx), %rax
	testq	%rax, %rax
	je	.L842
	movq	8(%rbx), %rcx
	movq	16(%rbx), %rdx
	subq	$8, %rsp
	movq	%r14, %rdi
	movq	(%rcx), %rsi
	movl	20(%rdx), %r9d
	movl	20(%rcx), %ecx
	movq	8(%rsi), %r8
	movq	(%rax), %rsi
	andl	$16777215, %r9d
	andl	$16777215, %ecx
	movq	8(%rsi), %r10
	movl	20(%rax), %esi
	movq	(%rdx), %rax
	andl	$16777215, %esi
	movq	%r10, %rdx
	pushq	8(%rax)
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	popq	%rax
	popq	%rdx
.L842:
	addq	$32, %rbx
	cmpq	%rbx, %r12
	jne	.L843
.L841:
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L846:
	movq	8(%r13,%r14,8), %rbx
	testq	%rbx, %rbx
	je	.L844
	xorl	%eax, %eax
	movq	%r14, %rsi
	leaq	.LC9(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	32(%rbx), %r12
	leaq	16(%rbx), %rax
	movq	%rax, -56(%rbp)
	cmpq	%rax, %r12
	je	.L844
	.p2align 4,,10
	.p2align 3
.L845:
	movzbl	48(%r12), %edi
	movq	32(%r12), %r15
	movq	40(%r12), %rbx
	call	_ZN2v88internal19MachineReprToStringENS0_21MachineRepresentationE@PLT
	movl	20(%r15), %esi
	movq	%rax, %r9
	movq	(%r15), %rax
	movq	(%rbx), %rdi
	movl	20(%rbx), %ecx
	andl	$16777215, %esi
	movq	8(%rax), %r10
	movq	8(%rdi), %r8
	xorl	%eax, %eax
	leaq	.LC2(%rip), %rdi
	andl	$16777215, %ecx
	movq	%r10, %rdx
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rax, -56(%rbp)
	jne	.L845
.L844:
	addq	$1, %r14
	cmpq	$32, %r14
	jne	.L846
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L849:
	movq	264(%r13,%r14,8), %rbx
	testq	%rbx, %rbx
	je	.L847
	xorl	%eax, %eax
	movq	%r14, %rsi
	leaq	.LC10(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	32(%rbx), %r12
	leaq	16(%rbx), %rax
	movq	%rax, -56(%rbp)
	cmpq	%rax, %r12
	je	.L847
	.p2align 4,,10
	.p2align 3
.L848:
	movzbl	48(%r12), %edi
	movq	32(%r12), %r15
	movq	40(%r12), %rbx
	call	_ZN2v88internal19MachineReprToStringENS0_21MachineRepresentationE@PLT
	movl	20(%r15), %esi
	movq	%rax, %r9
	movq	(%r15), %rax
	movq	(%rbx), %rdi
	movl	20(%rbx), %ecx
	andl	$16777215, %esi
	movq	8(%rax), %r10
	movq	8(%rdi), %r8
	xorl	%eax, %eax
	leaq	.LC2(%rip), %rdi
	andl	$16777215, %ecx
	movq	%r10, %rdx
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rax, -56(%rbp)
	jne	.L848
.L847:
	addq	$1, %r14
	cmpq	$32, %r14
	jne	.L849
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19415:
	.size	_ZNK2v88internal8compiler15LoadElimination13AbstractState5PrintEv, .-_ZNK2v88internal8compiler15LoadElimination13AbstractState5PrintEv
	.section	.text._ZNK2v88internal8compiler15LoadElimination27AbstractStateForEffectNodes3GetEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler15LoadElimination27AbstractStateForEffectNodes3GetEPNS1_4NodeE
	.type	_ZNK2v88internal8compiler15LoadElimination27AbstractStateForEffectNodes3GetEPNS1_4NodeE, @function
_ZNK2v88internal8compiler15LoadElimination27AbstractStateForEffectNodes3GetEPNS1_4NodeE:
.LFB19416:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rcx
	movq	16(%rdi), %rax
	xorl	%r8d, %r8d
	movl	20(%rsi), %edx
	subq	%rcx, %rax
	andl	$16777215, %edx
	sarq	$3, %rax
	cmpq	%rax, %rdx
	jnb	.L865
	movq	(%rcx,%rdx,8), %r8
.L865:
	movq	%r8, %rax
	ret
	.cfi_endproc
.LFE19416:
	.size	_ZNK2v88internal8compiler15LoadElimination27AbstractStateForEffectNodes3GetEPNS1_4NodeE, .-_ZNK2v88internal8compiler15LoadElimination27AbstractStateForEffectNodes3GetEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler15LoadElimination12FieldIndexOfEii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15LoadElimination12FieldIndexOfEii
	.type	_ZN2v88internal8compiler15LoadElimination12FieldIndexOfEii, @function
_ZN2v88internal8compiler15LoadElimination12FieldIndexOfEii:
.LFB19455:
	.cfi_startproc
	endbr64
	testl	%edi, %edi
	leal	7(%rdi), %edx
	leal	7(%rsi), %eax
	cmovns	%edi, %edx
	sarl	$3, %edx
	subl	$1, %edx
	testl	%esi, %esi
	cmovns	%esi, %eax
	sarl	$3, %eax
	addl	%edx, %eax
	cmpl	$32, %eax
	jle	.L869
	movl	$-1, %eax
	movl	$-1, %edx
.L869:
	movq	%rax, %rcx
	movl	%edx, %eax
	salq	$32, %rcx
	orq	%rcx, %rax
	ret
	.cfi_endproc
.LFE19455:
	.size	_ZN2v88internal8compiler15LoadElimination12FieldIndexOfEii, .-_ZN2v88internal8compiler15LoadElimination12FieldIndexOfEii
	.section	.rodata._ZN2v88internal8compiler15LoadElimination12FieldIndexOfERKNS1_11FieldAccessE.str1.1,"aMS",@progbits,1
.LC11:
	.string	"unreachable code"
	.section	.text._ZN2v88internal8compiler15LoadElimination12FieldIndexOfERKNS1_11FieldAccessE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15LoadElimination12FieldIndexOfERKNS1_11FieldAccessE
	.type	_ZN2v88internal8compiler15LoadElimination12FieldIndexOfERKNS1_11FieldAccessE, @function
_ZN2v88internal8compiler15LoadElimination12FieldIndexOfERKNS1_11FieldAccessE:
.LFB19456:
	.cfi_startproc
	endbr64
	movzbl	32(%rdi), %eax
	cmpb	$12, %al
	je	.L878
	ja	.L872
	cmpb	$1, %al
	jbe	.L873
	leal	-2(%rax), %edx
	cmpb	$1, %dl
	ja	.L887
.L878:
	movq	$-1, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L872:
	cmpb	$14, %al
	je	.L873
	subl	$1, %eax
	cmpb	$13, %al
	ja	.L873
.L880:
	movzbl	%al, %eax
	leaq	CSWTCH.421(%rip), %rdx
	movl	(%rdx,%rax,4), %ecx
	movl	$1, %eax
	sall	%cl, %eax
	cmpl	$7, %eax
	jle	.L878
	cmpb	$1, (%rdi)
	jne	.L878
	movl	4(%rdi), %ecx
	testl	%ecx, %ecx
	leal	7(%rcx), %edx
	cmovns	%ecx, %edx
	sarl	$3, %eax
	sarl	$3, %edx
	subl	$1, %edx
	addl	%edx, %eax
	cmpl	$32, %eax
	jle	.L879
	movl	$-1, %eax
	movl	$-1, %edx
.L879:
	movq	%rax, %rcx
	movl	%edx, %eax
	salq	$32, %rcx
	orq	%rcx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L887:
	subl	$1, %eax
	jmp	.L880
.L873:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19456:
	.size	_ZN2v88internal8compiler15LoadElimination12FieldIndexOfERKNS1_11FieldAccessE, .-_ZN2v88internal8compiler15LoadElimination12FieldIndexOfERKNS1_11FieldAccessE
	.section	.text._ZNK2v88internal8compiler15LoadElimination6commonEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler15LoadElimination6commonEv
	.type	_ZNK2v88internal8compiler15LoadElimination6commonEv, @function
_ZNK2v88internal8compiler15LoadElimination6commonEv:
.LFB19457:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %rax
	movq	8(%rax), %rax
	ret
	.cfi_endproc
.LFE19457:
	.size	_ZNK2v88internal8compiler15LoadElimination6commonEv, .-_ZNK2v88internal8compiler15LoadElimination6commonEv
	.section	.text._ZNK2v88internal8compiler15LoadElimination5graphEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler15LoadElimination5graphEv
	.type	_ZNK2v88internal8compiler15LoadElimination5graphEv, @function
_ZNK2v88internal8compiler15LoadElimination5graphEv:
.LFB19458:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %rax
	movq	(%rax), %rax
	ret
	.cfi_endproc
.LFE19458:
	.size	_ZNK2v88internal8compiler15LoadElimination5graphEv, .-_ZNK2v88internal8compiler15LoadElimination5graphEv
	.section	.text._ZNK2v88internal8compiler15LoadElimination7isolateEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler15LoadElimination7isolateEv
	.type	_ZNK2v88internal8compiler15LoadElimination7isolateEv, @function
_ZNK2v88internal8compiler15LoadElimination7isolateEv:
.LFB19459:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %rax
	movq	360(%rax), %rax
	ret
	.cfi_endproc
.LFE19459:
	.size	_ZNK2v88internal8compiler15LoadElimination7isolateEv, .-_ZNK2v88internal8compiler15LoadElimination7isolateEv
	.section	.text._ZNK2v88internal8compiler15LoadElimination7factoryEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler15LoadElimination7factoryEv
	.type	_ZNK2v88internal8compiler15LoadElimination7factoryEv, @function
_ZNK2v88internal8compiler15LoadElimination7factoryEv:
.LFB19460:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %rax
	movq	360(%rax), %rax
	ret
	.cfi_endproc
.LFE19460:
	.size	_ZNK2v88internal8compiler15LoadElimination7factoryEv, .-_ZNK2v88internal8compiler15LoadElimination7factoryEv
	.section	.text._ZNSt8_Rb_treeIPN2v88internal8compiler4NodeESt4pairIKS4_NS2_15LoadElimination9FieldInfoEESt10_Select1stIS9_ESt4lessIS4_ENS1_13ZoneAllocatorIS9_EEE8_M_eraseEPSt13_Rb_tree_nodeIS9_E,"axG",@progbits,_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeESt4pairIKS4_NS2_15LoadElimination9FieldInfoEESt10_Select1stIS9_ESt4lessIS4_ENS1_13ZoneAllocatorIS9_EEE8_M_eraseEPSt13_Rb_tree_nodeIS9_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeESt4pairIKS4_NS2_15LoadElimination9FieldInfoEESt10_Select1stIS9_ESt4lessIS4_ENS1_13ZoneAllocatorIS9_EEE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	.type	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeESt4pairIKS4_NS2_15LoadElimination9FieldInfoEESt10_Select1stIS9_ESt4lessIS4_ENS1_13ZoneAllocatorIS9_EEE8_M_eraseEPSt13_Rb_tree_nodeIS9_E, @function
_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeESt4pairIKS4_NS2_15LoadElimination9FieldInfoEESt10_Select1stIS9_ESt4lessIS4_ENS1_13ZoneAllocatorIS9_EEE8_M_eraseEPSt13_Rb_tree_nodeIS9_E:
.LFB21914:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L900
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L894:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeESt4pairIKS4_NS2_15LoadElimination9FieldInfoEESt10_Select1stIS9_ESt4lessIS4_ENS1_13ZoneAllocatorIS9_EEE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L894
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L900:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE21914:
	.size	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeESt4pairIKS4_NS2_15LoadElimination9FieldInfoEESt10_Select1stIS9_ESt4lessIS4_ENS1_13ZoneAllocatorIS9_EEE8_M_eraseEPSt13_Rb_tree_nodeIS9_E, .-_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeESt4pairIKS4_NS2_15LoadElimination9FieldInfoEESt10_Select1stIS9_ESt4lessIS4_ENS1_13ZoneAllocatorIS9_EEE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	.section	.text._ZNSt8_Rb_treeIPN2v88internal8compiler4NodeESt4pairIKS4_NS2_15LoadElimination9FieldInfoEESt10_Select1stIS9_ESt4lessIS4_ENS1_13ZoneAllocatorIS9_EEE17_M_emplace_uniqueIJRS9_EEES5_ISt17_Rb_tree_iteratorIS9_EbEDpOT_,"axG",@progbits,_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeESt4pairIKS4_NS2_15LoadElimination9FieldInfoEESt10_Select1stIS9_ESt4lessIS4_ENS1_13ZoneAllocatorIS9_EEE17_M_emplace_uniqueIJRS9_EEES5_ISt17_Rb_tree_iteratorIS9_EbEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeESt4pairIKS4_NS2_15LoadElimination9FieldInfoEESt10_Select1stIS9_ESt4lessIS4_ENS1_13ZoneAllocatorIS9_EEE17_M_emplace_uniqueIJRS9_EEES5_ISt17_Rb_tree_iteratorIS9_EbEDpOT_
	.type	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeESt4pairIKS4_NS2_15LoadElimination9FieldInfoEESt10_Select1stIS9_ESt4lessIS4_ENS1_13ZoneAllocatorIS9_EEE17_M_emplace_uniqueIJRS9_EEES5_ISt17_Rb_tree_iteratorIS9_EbEDpOT_, @function
_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeESt4pairIKS4_NS2_15LoadElimination9FieldInfoEESt10_Select1stIS9_ESt4lessIS4_ENS1_13ZoneAllocatorIS9_EEE17_M_emplace_uniqueIJRS9_EEES5_ISt17_Rb_tree_iteratorIS9_EbEDpOT_:
.LFB21970:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	$71, %rax
	jbe	.L923
	leaq	72(%rbx), %rax
	movq	%rax, 16(%rdi)
.L905:
	movdqu	(%r12), %xmm0
	leaq	16(%r13), %r15
	movups	%xmm0, 32(%rbx)
	movdqu	16(%r12), %xmm1
	movups	%xmm1, 48(%rbx)
	movq	32(%r12), %rax
	movq	%rax, 64(%rbx)
	movq	24(%r13), %r12
	testq	%r12, %r12
	je	.L906
	movq	32(%rbx), %r14
	jmp	.L907
	.p2align 4,,10
	.p2align 3
.L924:
	movq	16(%r12), %rax
	movl	$1, %edx
	testq	%rax, %rax
	je	.L908
.L925:
	movq	%rax, %r12
.L907:
	movq	32(%r12), %rcx
	cmpq	%rcx, %r14
	jb	.L924
	movq	24(%r12), %rax
	xorl	%edx, %edx
	testq	%rax, %rax
	jne	.L925
.L908:
	testb	%dl, %dl
	jne	.L926
	cmpq	%rcx, %r14
	jbe	.L916
.L915:
	movl	$1, %edi
	cmpq	%r12, %r15
	jne	.L927
.L913:
	movq	%r12, %rdx
	movq	%r15, %rcx
	movq	%rbx, %rsi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 48(%r13)
	addq	$8, %rsp
	movq	%rbx, %rax
	movl	$1, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L926:
	.cfi_restore_state
	cmpq	%r12, 32(%r13)
	je	.L915
.L917:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	cmpq	32(%rax), %r14
	ja	.L915
	movq	%rax, %r12
.L916:
	addq	$8, %rsp
	movq	%r12, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L927:
	.cfi_restore_state
	xorl	%edi, %edi
	cmpq	32(%r12), %r14
	setb	%dil
	jmp	.L913
	.p2align 4,,10
	.p2align 3
.L906:
	movq	%r15, %r12
	cmpq	32(%r13), %r15
	je	.L919
	movq	32(%rbx), %r14
	jmp	.L917
	.p2align 4,,10
	.p2align 3
.L923:
	movl	$72, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rbx
	jmp	.L905
	.p2align 4,,10
	.p2align 3
.L919:
	movl	$1, %edi
	jmp	.L913
	.cfi_endproc
.LFE21970:
	.size	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeESt4pairIKS4_NS2_15LoadElimination9FieldInfoEESt10_Select1stIS9_ESt4lessIS4_ENS1_13ZoneAllocatorIS9_EEE17_M_emplace_uniqueIJRS9_EEES5_ISt17_Rb_tree_iteratorIS9_EbEDpOT_, .-_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeESt4pairIKS4_NS2_15LoadElimination9FieldInfoEESt10_Select1stIS9_ESt4lessIS4_ENS1_13ZoneAllocatorIS9_EEE17_M_emplace_uniqueIJRS9_EEES5_ISt17_Rb_tree_iteratorIS9_EbEDpOT_
	.section	.text._ZNK2v88internal8compiler15LoadElimination13AbstractField9KillConstEPNS1_4NodeEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler15LoadElimination13AbstractField9KillConstEPNS1_4NodeEPNS0_4ZoneE
	.type	_ZNK2v88internal8compiler15LoadElimination13AbstractField9KillConstEPNS1_4NodeEPNS0_4ZoneE, @function
_ZNK2v88internal8compiler15LoadElimination13AbstractField9KillConstEPNS1_4NodeEPNS0_4ZoneE:
.LFB19329:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	16(%rdi), %rbx
	subq	$56, %rsp
	movq	32(%rdi), %r8
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	%rbx, %r8
	je	.L929
	movq	%rsi, %r12
	movq	%rdx, %r14
	.p2align 4,,10
	.p2align 3
.L940:
	movq	32(%r8), %r9
	movzbl	23(%r9), %eax
	leaq	32(%r9), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L931
	movq	32(%r9), %rdx
	movl	8(%rdx), %eax
	addq	$16, %rdx
.L931:
	testl	%eax, %eax
	jle	.L932
	cmpq	$0, (%rdx)
	je	.L933
.L932:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	movq	%r9, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	cmpq	%rax, %rsi
	je	.L946
.L933:
	movq	%r8, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r8
	cmpq	%rax, %rbx
	jne	.L940
.L929:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L947
	addq	$56, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L946:
	.cfi_restore_state
	movq	16(%r14), %r15
	movq	24(%r14), %rax
	subq	%r15, %rax
	cmpq	$55, %rax
	jbe	.L948
	leaq	56(%r15), %rax
	movq	%rax, 16(%r14)
.L935:
	leaq	16(%r15), %rax
	movq	%r14, (%r15)
	leaq	-96(%rbp), %r14
	movl	$0, 16(%r15)
	movq	$0, 24(%r15)
	movq	%rax, 32(%r15)
	movq	%rax, 40(%r15)
	movq	$0, 48(%r15)
	movq	32(%r13), %r13
	cmpq	%r13, %rbx
	je	.L939
	.p2align 4,,10
	.p2align 3
.L936:
	movdqu	32(%r13), %xmm0
	movdqu	48(%r13), %xmm1
	movq	%r12, %rdi
	movq	64(%r13), %rax
	movq	32(%r13), %r8
	movaps	%xmm0, -96(%rbp)
	movq	%rax, -64(%rbp)
	movaps	%xmm1, -80(%rbp)
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	movq	%r8, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	cmpq	%rax, %rsi
	je	.L937
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeESt4pairIKS4_NS2_15LoadElimination9FieldInfoEESt10_Select1stIS9_ESt4lessIS4_ENS1_13ZoneAllocatorIS9_EEE17_M_emplace_uniqueIJRS9_EEES5_ISt17_Rb_tree_iteratorIS9_EbEDpOT_
.L937:
	movq	%r13, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r13
	cmpq	%rax, %rbx
	jne	.L936
.L939:
	movq	%r15, %r13
	jmp	.L929
.L948:
	movl	$56, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r15
	jmp	.L935
.L947:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19329:
	.size	_ZNK2v88internal8compiler15LoadElimination13AbstractField9KillConstEPNS1_4NodeEPNS0_4ZoneE, .-_ZNK2v88internal8compiler15LoadElimination13AbstractField9KillConstEPNS1_4NodeEPNS0_4ZoneE
	.section	.text._ZNK2v88internal8compiler15LoadElimination13AbstractState14KillConstFieldEPNS1_4NodeENS2_10IndexRangeEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler15LoadElimination13AbstractState14KillConstFieldEPNS1_4NodeENS2_10IndexRangeEPNS0_4ZoneE
	.type	_ZNK2v88internal8compiler15LoadElimination13AbstractState14KillConstFieldEPNS1_4NodeENS2_10IndexRangeEPNS0_4ZoneE, @function
_ZNK2v88internal8compiler15LoadElimination13AbstractState14KillConstFieldEPNS1_4NodeENS2_10IndexRangeEPNS0_4ZoneE:
.LFB19394:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rax
	sarq	$32, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -64(%rbp)
	movl	%eax, -52(%rbp)
	cmpl	%eax, %edx
	je	.L950
	movq	%rcx, %r14
	movslq	%edx, %rbx
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L955:
	movq	264(%r13,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.L951
	movq	-64(%rbp), %rsi
	movq	%r14, %rdx
	call	_ZNK2v88internal8compiler15LoadElimination13AbstractField9KillConstEPNS1_4NodeEPNS0_4ZoneE
	movq	%rax, %r12
	cmpq	%rax, 264(%r13,%rbx,8)
	je	.L951
	testq	%r15, %r15
	je	.L967
.L952:
	movq	%r12, 264(%r15,%rbx,8)
.L951:
	addq	$1, %rbx
	cmpl	%ebx, -52(%rbp)
	jne	.L955
	testq	%r15, %r15
	je	.L950
.L949:
	addq	$24, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L967:
	.cfi_restore_state
	movq	16(%r14), %r15
	movq	24(%r14), %rax
	subq	%r15, %rax
	cmpq	$527, %rax
	jbe	.L968
	leaq	528(%r15), %rax
	movq	%rax, 16(%r14)
.L954:
	movl	$66, %ecx
	movq	%r15, %rdi
	movq	%r13, %rsi
	rep movsq
	jmp	.L952
	.p2align 4,,10
	.p2align 3
.L950:
	movq	%r13, %r15
	jmp	.L949
.L968:
	movl	$528, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r15
	jmp	.L954
	.cfi_endproc
.LFE19394:
	.size	_ZNK2v88internal8compiler15LoadElimination13AbstractState14KillConstFieldEPNS1_4NodeENS2_10IndexRangeEPNS0_4ZoneE, .-_ZNK2v88internal8compiler15LoadElimination13AbstractState14KillConstFieldEPNS1_4NodeENS2_10IndexRangeEPNS0_4ZoneE
	.section	.text._ZNK2v88internal8compiler15LoadElimination13AbstractField4KillERKNS2_14AliasStateInfoENS0_11MaybeHandleINS0_4NameEEEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler15LoadElimination13AbstractField4KillERKNS2_14AliasStateInfoENS0_11MaybeHandleINS0_4NameEEEPNS0_4ZoneE
	.type	_ZNK2v88internal8compiler15LoadElimination13AbstractField4KillERKNS2_14AliasStateInfoENS0_11MaybeHandleINS0_4NameEEEPNS0_4ZoneE, @function
_ZNK2v88internal8compiler15LoadElimination13AbstractField4KillERKNS2_14AliasStateInfoENS0_11MaybeHandleINS0_4NameEEEPNS0_4ZoneE:
.LFB19330:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	16(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$88, %rsp
	movq	%rdi, -112(%rbp)
	movq	32(%rdi), %r13
	movq	%rdx, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	%r12, %r13
	je	.L1001
	.p2align 4,,10
	.p2align 3
.L970:
	movq	32(%r13), %r15
	movzbl	23(%r15), %eax
	leaq	32(%r15), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L973
	movq	32(%r15), %rdx
	movl	8(%rdx), %eax
	addq	$16, %rdx
.L973:
	testl	%eax, %eax
	jle	.L974
	cmpq	$0, (%rdx)
	je	.L975
.L974:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	cmpw	$237, 16(%rax)
	je	.L1054
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_18MayAliasEPNS1_4NodeES4_
	testb	%al, %al
	je	.L975
	movq	16(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L979
	movq	(%rbx), %rax
	movq	520(%rax), %r9
	testq	%r9, %r9
	je	.L979
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	movq	24(%r9), %rdx
	leaq	16(%r9), %rdi
	testq	%rdx, %rdx
	je	.L979
	movq	%rdi, %rcx
	jmp	.L980
	.p2align 4,,10
	.p2align 3
.L1055:
	movq	%rdx, %rcx
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L981
.L980:
	cmpq	%rax, 32(%rdx)
	jnb	.L1055
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L980
.L981:
	cmpq	%rcx, %rdi
	je	.L979
	cmpq	%rax, 32(%rcx)
	ja	.L979
	movq	40(%rcx), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	je	.L979
	testq	%rdx, %rdx
	je	.L984
	movq	6(%rax), %rdx
	subq	$2, %rax
	movq	16(%rax), %rax
	movq	%rax, %rcx
	subq	%rdx, %rcx
	cmpq	$8, %rcx
	je	.L1056
	.p2align 4,,10
	.p2align 3
.L979:
	movq	16(%r14), %r13
	movq	24(%r14), %rax
	subq	%r13, %rax
	cmpq	$55, %rax
	jbe	.L1057
	leaq	56(%r13), %rax
	movq	%rax, 16(%r14)
.L986:
	leaq	16(%r13), %rax
	movq	%r14, 0(%r13)
	leaq	-96(%rbp), %r15
	movq	%rax, 32(%r13)
	movq	%rax, 40(%r13)
	movq	-112(%rbp), %rax
	movl	$0, 16(%r13)
	movq	$0, 24(%r13)
	movq	$0, 48(%r13)
	movq	32(%rax), %r14
	movq	%r13, -120(%rbp)
	cmpq	%r14, %r12
	je	.L969
	.p2align 4,,10
	.p2align 3
.L987:
	movdqu	32(%r14), %xmm0
	movdqu	48(%r14), %xmm1
	movq	64(%r14), %rax
	movq	8(%rbx), %rdi
	movq	32(%r14), %r9
	movaps	%xmm0, -96(%rbp)
	movq	%rax, -64(%rbp)
	movaps	%xmm1, -80(%rbp)
	movq	(%rdi), %rax
	cmpw	$237, 16(%rax)
	je	.L1058
	movq	%r9, %rsi
	movq	%r9, -112(%rbp)
	call	_ZN2v88internal8compiler12_GLOBAL__N_18MayAliasEPNS1_4NodeES4_
	testb	%al, %al
	je	.L989
	movq	16(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L992
	movq	(%rbx), %rax
	movq	520(%rax), %r10
	testq	%r10, %r10
	je	.L992
	movq	-112(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	movq	24(%r10), %rdx
	leaq	16(%r10), %rdi
	testq	%rdx, %rdx
	je	.L992
	movq	%rdi, %rcx
	jmp	.L993
	.p2align 4,,10
	.p2align 3
.L1059:
	movq	%rdx, %rcx
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L994
.L993:
	cmpq	%rax, 32(%rdx)
	jnb	.L1059
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L993
.L994:
	cmpq	%rcx, %rdi
	je	.L992
	cmpq	%rax, 32(%rcx)
	ja	.L992
	movq	40(%rcx), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	je	.L992
	testq	%rdx, %rdx
	je	.L997
	movq	6(%rax), %rdx
	subq	$2, %rax
	movq	16(%rax), %rax
	movq	%rax, %rcx
	subq	%rdx, %rcx
	cmpq	$8, %rcx
	jne	.L992
	cmpq	%rax, %rdx
	je	.L998
	movq	(%rdx), %rax
.L997:
	cmpq	%rax, %rsi
	jne	.L989
	.p2align 4,,10
	.p2align 3
.L992:
	cmpq	$0, -104(%rbp)
	je	.L990
	movq	-72(%rbp), %rax
	testq	%rax, %rax
	je	.L990
	cmpq	-104(%rbp), %rax
	je	.L990
.L989:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeESt4pairIKS4_NS2_15LoadElimination9FieldInfoEESt10_Select1stIS9_ESt4lessIS4_ENS1_13ZoneAllocatorIS9_EEE17_M_emplace_uniqueIJRS9_EEES5_ISt17_Rb_tree_iteratorIS9_EbEDpOT_
.L990:
	movq	%r14, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r14
	cmpq	%rax, %r12
	jne	.L987
	jmp	.L969
	.p2align 4,,10
	.p2align 3
.L1054:
	cmpq	%rdi, %r15
	je	.L979
.L975:
	movq	%r13, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r13
	cmpq	%rax, %r12
	jne	.L970
.L1001:
	movq	-112(%rbp), %rax
	movq	%rax, -120(%rbp)
.L969:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1060
	movq	-120(%rbp), %rax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1056:
	.cfi_restore_state
	cmpq	%rax, %rdx
	je	.L998
	movq	(%rdx), %rax
.L984:
	cmpq	%rax, %rsi
	je	.L979
	movq	%r13, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r13
	cmpq	%rax, %r12
	jne	.L970
	jmp	.L1001
	.p2align 4,,10
	.p2align 3
.L1058:
	cmpq	%rdi, %r9
	je	.L992
	jmp	.L989
	.p2align 4,,10
	.p2align 3
.L1057:
	movl	$56, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r13
	jmp	.L986
.L998:
	xorl	%edx, %edx
	xorl	%esi, %esi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L1060:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19330:
	.size	_ZNK2v88internal8compiler15LoadElimination13AbstractField4KillERKNS2_14AliasStateInfoENS0_11MaybeHandleINS0_4NameEEEPNS0_4ZoneE, .-_ZNK2v88internal8compiler15LoadElimination13AbstractField4KillERKNS2_14AliasStateInfoENS0_11MaybeHandleINS0_4NameEEEPNS0_4ZoneE
	.section	.text._ZNK2v88internal8compiler15LoadElimination13AbstractState9KillFieldERKNS2_14AliasStateInfoENS2_10IndexRangeENS0_11MaybeHandleINS0_4NameEEEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler15LoadElimination13AbstractState9KillFieldERKNS2_14AliasStateInfoENS2_10IndexRangeENS0_11MaybeHandleINS0_4NameEEEPNS0_4ZoneE
	.type	_ZNK2v88internal8compiler15LoadElimination13AbstractState9KillFieldERKNS2_14AliasStateInfoENS2_10IndexRangeENS0_11MaybeHandleINS0_4NameEEEPNS0_4ZoneE, @function
_ZNK2v88internal8compiler15LoadElimination13AbstractState9KillFieldERKNS2_14AliasStateInfoENS2_10IndexRangeENS0_11MaybeHandleINS0_4NameEEEPNS0_4ZoneE:
.LFB19396:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rax
	sarq	$32, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -64(%rbp)
	movq	%rcx, -72(%rbp)
	movl	%eax, -52(%rbp)
	cmpl	%eax, %edx
	je	.L1062
	movq	%r8, %r14
	movslq	%edx, %rbx
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L1067:
	movq	8(%r13,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.L1063
	movq	-72(%rbp), %rdx
	movq	-64(%rbp), %rsi
	movq	%r14, %rcx
	call	_ZNK2v88internal8compiler15LoadElimination13AbstractField4KillERKNS2_14AliasStateInfoENS0_11MaybeHandleINS0_4NameEEEPNS0_4ZoneE
	movq	%rax, %r15
	cmpq	%rax, 8(%r13,%rbx,8)
	je	.L1063
	testq	%r12, %r12
	je	.L1079
.L1064:
	movq	%r15, 8(%r12,%rbx,8)
.L1063:
	addq	$1, %rbx
	cmpl	%ebx, -52(%rbp)
	jne	.L1067
	testq	%r12, %r12
	je	.L1062
.L1061:
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1079:
	.cfi_restore_state
	movq	16(%r14), %r10
	movq	24(%r14), %rax
	subq	%r10, %rax
	cmpq	$527, %rax
	jbe	.L1080
	leaq	528(%r10), %rax
	movq	%rax, 16(%r14)
.L1066:
	movl	$66, %ecx
	movq	%r10, %rdi
	movq	%r13, %rsi
	movq	%r10, %r12
	rep movsq
	jmp	.L1064
	.p2align 4,,10
	.p2align 3
.L1062:
	movq	%r13, %r12
	jmp	.L1061
.L1080:
	movl	$528, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r10
	jmp	.L1066
	.cfi_endproc
.LFE19396:
	.size	_ZNK2v88internal8compiler15LoadElimination13AbstractState9KillFieldERKNS2_14AliasStateInfoENS2_10IndexRangeENS0_11MaybeHandleINS0_4NameEEEPNS0_4ZoneE, .-_ZNK2v88internal8compiler15LoadElimination13AbstractState9KillFieldERKNS2_14AliasStateInfoENS2_10IndexRangeENS0_11MaybeHandleINS0_4NameEEEPNS0_4ZoneE
	.section	.text._ZNK2v88internal8compiler15LoadElimination13AbstractState10KillFieldsEPNS1_4NodeENS0_11MaybeHandleINS0_4NameEEEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler15LoadElimination13AbstractState10KillFieldsEPNS1_4NodeENS0_11MaybeHandleINS0_4NameEEEPNS0_4ZoneE
	.type	_ZNK2v88internal8compiler15LoadElimination13AbstractState10KillFieldsEPNS1_4NodeENS0_11MaybeHandleINS0_4NameEEEPNS0_4ZoneE, @function
_ZNK2v88internal8compiler15LoadElimination13AbstractState10KillFieldsEPNS1_4NodeENS0_11MaybeHandleINS0_4NameEEEPNS0_4ZoneE:
.LFB19397:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %xmm0
	movq	%rsi, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r15
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$56, %rsp
	movq	%rdx, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	.p2align 4,,10
	.p2align 3
.L1088:
	movq	8(%r14,%rbx,8), %r12
	testq	%r12, %r12
	je	.L1082
	movq	-88(%rbp), %rdx
	movq	%r13, %rcx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler15LoadElimination13AbstractField4KillERKNS2_14AliasStateInfoENS0_11MaybeHandleINS0_4NameEEEPNS0_4ZoneE
	movq	%rax, %rdx
	cmpq	%rax, %r12
	jne	.L1100
.L1082:
	addq	$1, %rbx
	cmpq	$32, %rbx
	jne	.L1088
.L1086:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1101
	addq	$56, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1100:
	.cfi_restore_state
	movq	16(%r13), %r9
	movq	24(%r13), %rax
	subq	%r9, %rax
	cmpq	$527, %rax
	jbe	.L1102
	leaq	528(%r9), %rax
	movq	%rax, 16(%r13)
.L1084:
	movl	$66, %ecx
	movq	%r9, %rdi
	leaq	1(%rbx), %r12
	movq	%r14, %rsi
	rep movsq
	movq	%rdx, 8(%r9,%rbx,8)
	cmpq	$31, %rbx
	je	.L1090
	.p2align 4,,10
	.p2align 3
.L1085:
	movq	8(%r14,%r12,8), %rdi
	testq	%rdi, %rdi
	je	.L1087
	movq	-88(%rbp), %rdx
	movq	%r13, %rcx
	movq	%r15, %rsi
	movq	%r9, -96(%rbp)
	call	_ZNK2v88internal8compiler15LoadElimination13AbstractField4KillERKNS2_14AliasStateInfoENS0_11MaybeHandleINS0_4NameEEEPNS0_4ZoneE
	movq	-96(%rbp), %r9
	movq	%rax, 8(%r9,%r12,8)
.L1087:
	addq	$1, %r12
	cmpq	$32, %r12
	jne	.L1085
.L1090:
	movq	%r9, %r14
	jmp	.L1086
.L1102:
	movl	$528, %esi
	movq	%r13, %rdi
	movq	%rdx, -96(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-96(%rbp), %rdx
	movq	%rax, %r9
	jmp	.L1084
.L1101:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19397:
	.size	_ZNK2v88internal8compiler15LoadElimination13AbstractState10KillFieldsEPNS1_4NodeENS0_11MaybeHandleINS0_4NameEEEPNS0_4ZoneE, .-_ZNK2v88internal8compiler15LoadElimination13AbstractState10KillFieldsEPNS1_4NodeENS0_11MaybeHandleINS0_4NameEEEPNS0_4ZoneE
	.section	.text._ZNK2v88internal8compiler15LoadElimination13AbstractState9KillFieldEPNS1_4NodeENS2_10IndexRangeENS0_11MaybeHandleINS0_4NameEEEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler15LoadElimination13AbstractState9KillFieldEPNS1_4NodeENS2_10IndexRangeENS0_11MaybeHandleINS0_4NameEEEPNS0_4ZoneE
	.type	_ZNK2v88internal8compiler15LoadElimination13AbstractState9KillFieldEPNS1_4NodeENS2_10IndexRangeENS0_11MaybeHandleINS0_4NameEEEPNS0_4ZoneE, @function
_ZNK2v88internal8compiler15LoadElimination13AbstractState9KillFieldEPNS1_4NodeENS2_10IndexRangeENS0_11MaybeHandleINS0_4NameEEEPNS0_4ZoneE:
.LFB19395:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %xmm0
	movq	%rsi, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdx, %rax
	movq	$0, -64(%rbp)
	sarq	$32, %rax
	movaps	%xmm0, -80(%rbp)
	movl	%eax, -84(%rbp)
	cmpl	%edx, %eax
	je	.L1111
	movq	%rcx, %r9
	movq	%r8, %r15
	movslq	%edx, %r13
	xorl	%r12d, %r12d
	leaq	-80(%rbp), %r14
	.p2align 4,,10
	.p2align 3
.L1110:
	movq	8(%rbx,%r13,8), %rdi
	testq	%rdi, %rdi
	je	.L1106
	movq	%r9, %rdx
	movq	%r15, %rcx
	movq	%r14, %rsi
	movq	%r9, -96(%rbp)
	call	_ZNK2v88internal8compiler15LoadElimination13AbstractField4KillERKNS2_14AliasStateInfoENS0_11MaybeHandleINS0_4NameEEEPNS0_4ZoneE
	cmpq	8(%rbx,%r13,8), %rax
	movq	-96(%rbp), %r9
	movq	%rax, %rdx
	je	.L1106
	testq	%r12, %r12
	je	.L1123
.L1107:
	movq	%rdx, 8(%r12,%r13,8)
.L1106:
	addq	$1, %r13
	cmpl	%r13d, -84(%rbp)
	jne	.L1110
	testq	%r12, %r12
	je	.L1111
.L1103:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1124
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1123:
	.cfi_restore_state
	movq	16(%r15), %r12
	movq	24(%r15), %rax
	subq	%r12, %rax
	cmpq	$527, %rax
	jbe	.L1125
	leaq	528(%r12), %rax
	movq	%rax, 16(%r15)
.L1109:
	movl	$66, %ecx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	rep movsq
	jmp	.L1107
	.p2align 4,,10
	.p2align 3
.L1111:
	movq	%rbx, %r12
	jmp	.L1103
	.p2align 4,,10
	.p2align 3
.L1125:
	movl	$528, %esi
	movq	%r15, %rdi
	movq	%r9, -104(%rbp)
	movq	%rdx, -96(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-96(%rbp), %rdx
	movq	-104(%rbp), %r9
	movq	%rax, %r12
	jmp	.L1109
.L1124:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19395:
	.size	_ZNK2v88internal8compiler15LoadElimination13AbstractState9KillFieldEPNS1_4NodeENS2_10IndexRangeENS0_11MaybeHandleINS0_4NameEEEPNS0_4ZoneE, .-_ZNK2v88internal8compiler15LoadElimination13AbstractState9KillFieldEPNS1_4NodeENS2_10IndexRangeENS0_11MaybeHandleINS0_4NameEEEPNS0_4ZoneE
	.section	.text._ZN2v88internal8compiler15LoadElimination13AbstractState11FieldsMergeEPSt5arrayIPKNS2_13AbstractFieldELm32EERKS8_PNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15LoadElimination13AbstractState11FieldsMergeEPSt5arrayIPKNS2_13AbstractFieldELm32EERKS8_PNS0_4ZoneE
	.type	_ZN2v88internal8compiler15LoadElimination13AbstractState11FieldsMergeEPSt5arrayIPKNS2_13AbstractFieldELm32EERKS8_PNS0_4ZoneE, @function
_ZN2v88internal8compiler15LoadElimination13AbstractState11FieldsMergeEPSt5arrayIPKNS2_13AbstractFieldELm32EERKS8_PNS0_4ZoneE:
.LFB19384:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -160(%rbp)
	movq	%rcx, -152(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-104(%rbp), %rax
	movq	%rax, -176(%rbp)
	.p2align 4,,10
	.p2align 3
.L1147:
	movq	(%r12,%r14,8), %rbx
	testq	%rbx, %rbx
	je	.L1127
	movq	-160(%rbp), %rax
	movq	(%rax,%r14,8), %r15
	testq	%r15, %r15
	je	.L1128
	movq	%rbx, -144(%rbp)
	cmpq	%r15, %rbx
	je	.L1129
	leaq	16(%rbx), %rax
	movq	%rax, -136(%rbp)
	movq	48(%r15), %rax
	cmpq	%rax, 48(%rbx)
	je	.L1167
.L1130:
	movq	-152(%rbp), %rax
	movq	16(%rax), %r13
	movq	24(%rax), %rax
	subq	%r13, %rax
	cmpq	$55, %rax
	jbe	.L1168
	movq	-152(%rbp), %rcx
	leaq	56(%r13), %rax
	movq	%rax, 16(%rcx)
	movq	%rcx, %rax
.L1135:
	movq	%rax, 0(%r13)
	leaq	16(%r13), %rax
	movl	$0, 16(%r13)
	movq	$0, 24(%r13)
	movq	%rax, 32(%r13)
	movq	%rax, 40(%r13)
	movq	$0, 48(%r13)
	movq	32(%rbx), %r8
	leaq	16(%r15), %rbx
	movq	%r13, -144(%rbp)
	cmpq	-136(%rbp), %r8
	je	.L1129
	.p2align 4,,10
	.p2align 3
.L1136:
	movdqu	32(%r8), %xmm0
	movdqu	48(%r8), %xmm1
	movq	64(%r8), %rax
	movq	32(%r8), %rsi
	movaps	%xmm1, -80(%rbp)
	movq	%rax, -64(%rbp)
	movdqu	-72(%rbp), %xmm3
	movq	%rsi, %rdx
	movaps	%xmm0, -96(%rbp)
	movdqu	-88(%rbp), %xmm2
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm2, -128(%rbp)
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1137
	leaq	32(%rsi), %rsi
.L1138:
	testl	%eax, %eax
	jle	.L1139
	cmpq	$0, (%rsi)
	je	.L1142
.L1139:
	movq	24(%r15), %rax
	movq	%rbx, %rdi
	testq	%rax, %rax
	jne	.L1141
	jmp	.L1142
	.p2align 4,,10
	.p2align 3
.L1169:
	movq	%rax, %rdi
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L1143
.L1141:
	cmpq	%rdx, 32(%rax)
	jnb	.L1169
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L1141
.L1143:
	cmpq	%rdi, %rbx
	je	.L1142
	cmpq	%rdx, 32(%rdi)
	ja	.L1142
	movq	-128(%rbp), %rax
	cmpq	%rax, 40(%rdi)
	je	.L1170
.L1142:
	movq	%r8, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r8
	cmpq	-136(%rbp), %rax
	jne	.L1136
.L1129:
	movq	-144(%rbp), %rax
	movq	%rax, (%r12,%r14,8)
.L1127:
	addq	$1, %r14
	cmpq	$32, %r14
	jne	.L1147
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1171
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1170:
	.cfi_restore_state
	movzbl	-120(%rbp), %eax
	cmpb	%al, 48(%rdi)
	jne	.L1142
	movq	-112(%rbp), %rax
	cmpq	%rax, 56(%rdi)
	jne	.L1142
	movq	-176(%rbp), %rsi
	addq	$64, %rdi
	movq	%r8, -168(%rbp)
	call	_ZN2v88internal8compilereqERKNS1_14ConstFieldInfoES4_@PLT
	movq	-168(%rbp), %r8
	testb	%al, %al
	je	.L1142
	leaq	-96(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeESt4pairIKS4_NS2_15LoadElimination9FieldInfoEESt10_Select1stIS9_ESt4lessIS4_ENS1_13ZoneAllocatorIS9_EEE17_M_emplace_uniqueIJRS9_EEES5_ISt17_Rb_tree_iteratorIS9_EbEDpOT_
	movq	-168(%rbp), %r8
	jmp	.L1142
	.p2align 4,,10
	.p2align 3
.L1137:
	movq	32(%rsi), %rsi
	movl	8(%rsi), %eax
	addq	$16, %rsi
	jmp	.L1138
	.p2align 4,,10
	.p2align 3
.L1128:
	movq	$0, (%r12,%r14,8)
	jmp	.L1127
	.p2align 4,,10
	.p2align 3
.L1167:
	movq	32(%r15), %r8
	movq	32(%rbx), %r13
	cmpq	-136(%rbp), %r13
	je	.L1131
.L1133:
	movq	32(%r8), %rax
	cmpq	%rax, 32(%r13)
	jne	.L1130
	movq	40(%r8), %rax
	cmpq	%rax, 40(%r13)
	jne	.L1130
	movzbl	48(%r8), %eax
	cmpb	%al, 48(%r13)
	jne	.L1130
	movq	56(%r8), %rax
	cmpq	%rax, 56(%r13)
	jne	.L1130
	leaq	64(%r8), %rsi
	leaq	64(%r13), %rdi
	movq	%r8, -144(%rbp)
	call	_ZN2v88internal8compilereqERKNS1_14ConstFieldInfoES4_@PLT
	testb	%al, %al
	je	.L1130
	movq	%r13, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	-144(%rbp), %r8
	movq	%rax, %r13
	movq	%r8, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r8
	cmpq	-136(%rbp), %r13
	jne	.L1133
.L1131:
	movq	%rbx, -144(%rbp)
	jmp	.L1129
	.p2align 4,,10
	.p2align 3
.L1168:
	movq	-152(%rbp), %rdi
	movl	$56, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r13
	movq	-152(%rbp), %rax
	jmp	.L1135
.L1171:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19384:
	.size	_ZN2v88internal8compiler15LoadElimination13AbstractState11FieldsMergeEPSt5arrayIPKNS2_13AbstractFieldELm32EERKS8_PNS0_4ZoneE, .-_ZN2v88internal8compiler15LoadElimination13AbstractState11FieldsMergeEPSt5arrayIPKNS2_13AbstractFieldELm32EERKS8_PNS0_4ZoneE
	.section	.text._ZNSt8_Rb_treeIPN2v88internal8compiler4NodeESt4pairIKS4_NS1_13ZoneHandleSetINS1_3MapEEEESt10_Select1stISA_ESt4lessIS4_ENS1_13ZoneAllocatorISA_EEE8_M_eraseEPSt13_Rb_tree_nodeISA_E,"axG",@progbits,_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeESt4pairIKS4_NS1_13ZoneHandleSetINS1_3MapEEEESt10_Select1stISA_ESt4lessIS4_ENS1_13ZoneAllocatorISA_EEE8_M_eraseEPSt13_Rb_tree_nodeISA_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeESt4pairIKS4_NS1_13ZoneHandleSetINS1_3MapEEEESt10_Select1stISA_ESt4lessIS4_ENS1_13ZoneAllocatorISA_EEE8_M_eraseEPSt13_Rb_tree_nodeISA_E
	.type	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeESt4pairIKS4_NS1_13ZoneHandleSetINS1_3MapEEEESt10_Select1stISA_ESt4lessIS4_ENS1_13ZoneAllocatorISA_EEE8_M_eraseEPSt13_Rb_tree_nodeISA_E, @function
_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeESt4pairIKS4_NS1_13ZoneHandleSetINS1_3MapEEEESt10_Select1stISA_ESt4lessIS4_ENS1_13ZoneAllocatorISA_EEE8_M_eraseEPSt13_Rb_tree_nodeISA_E:
.LFB22682:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L1180
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L1174:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeESt4pairIKS4_NS1_13ZoneHandleSetINS1_3MapEEEESt10_Select1stISA_ESt4lessIS4_ENS1_13ZoneAllocatorISA_EEE8_M_eraseEPSt13_Rb_tree_nodeISA_E
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L1174
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1180:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE22682:
	.size	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeESt4pairIKS4_NS1_13ZoneHandleSetINS1_3MapEEEESt10_Select1stISA_ESt4lessIS4_ENS1_13ZoneAllocatorISA_EEE8_M_eraseEPSt13_Rb_tree_nodeISA_E, .-_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeESt4pairIKS4_NS1_13ZoneHandleSetINS1_3MapEEEESt10_Select1stISA_ESt4lessIS4_ENS1_13ZoneAllocatorISA_EEE8_M_eraseEPSt13_Rb_tree_nodeISA_E
	.section	.text._ZNSt8_Rb_treeIPN2v88internal8compiler4NodeESt4pairIKS4_NS1_13ZoneHandleSetINS1_3MapEEEESt10_Select1stISA_ESt4lessIS4_ENS1_13ZoneAllocatorISA_EEE17_M_emplace_uniqueIJRSA_EEES5_ISt17_Rb_tree_iteratorISA_EbEDpOT_,"axG",@progbits,_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeESt4pairIKS4_NS1_13ZoneHandleSetINS1_3MapEEEESt10_Select1stISA_ESt4lessIS4_ENS1_13ZoneAllocatorISA_EEE17_M_emplace_uniqueIJRSA_EEES5_ISt17_Rb_tree_iteratorISA_EbEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeESt4pairIKS4_NS1_13ZoneHandleSetINS1_3MapEEEESt10_Select1stISA_ESt4lessIS4_ENS1_13ZoneAllocatorISA_EEE17_M_emplace_uniqueIJRSA_EEES5_ISt17_Rb_tree_iteratorISA_EbEDpOT_
	.type	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeESt4pairIKS4_NS1_13ZoneHandleSetINS1_3MapEEEESt10_Select1stISA_ESt4lessIS4_ENS1_13ZoneAllocatorISA_EEE17_M_emplace_uniqueIJRSA_EEES5_ISt17_Rb_tree_iteratorISA_EbEDpOT_, @function
_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeESt4pairIKS4_NS1_13ZoneHandleSetINS1_3MapEEEESt10_Select1stISA_ESt4lessIS4_ENS1_13ZoneAllocatorISA_EEE17_M_emplace_uniqueIJRSA_EEES5_ISt17_Rb_tree_iteratorISA_EbEDpOT_:
.LFB22697:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	$47, %rax
	jbe	.L1203
	leaq	48(%rbx), %rax
	movq	%rax, 16(%rdi)
.L1185:
	movq	(%r14), %r15
	movq	8(%r14), %rax
	leaq	16(%r13), %r14
	movq	%r15, 32(%rbx)
	movq	%rax, 40(%rbx)
	movq	24(%r13), %r12
	testq	%r12, %r12
	jne	.L1187
	jmp	.L1204
	.p2align 4,,10
	.p2align 3
.L1205:
	movq	16(%r12), %rax
	movl	$1, %edx
	testq	%rax, %rax
	je	.L1188
.L1206:
	movq	%rax, %r12
.L1187:
	movq	32(%r12), %rcx
	cmpq	%r15, %rcx
	ja	.L1205
	movq	24(%r12), %rax
	xorl	%edx, %edx
	testq	%rax, %rax
	jne	.L1206
.L1188:
	testb	%dl, %dl
	jne	.L1207
	cmpq	%r15, %rcx
	jnb	.L1196
.L1195:
	movl	$1, %edi
	cmpq	%r12, %r14
	jne	.L1208
.L1193:
	movq	%r12, %rdx
	movq	%r14, %rcx
	movq	%rbx, %rsi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 48(%r13)
	addq	$8, %rsp
	movq	%rbx, %rax
	movl	$1, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1207:
	.cfi_restore_state
	cmpq	%r12, 32(%r13)
	je	.L1195
.L1197:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	cmpq	32(%rax), %r15
	ja	.L1195
	movq	%rax, %r12
.L1196:
	addq	$8, %rsp
	movq	%r12, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1208:
	.cfi_restore_state
	xorl	%edi, %edi
	cmpq	32(%r12), %r15
	setb	%dil
	jmp	.L1193
	.p2align 4,,10
	.p2align 3
.L1204:
	movq	%r14, %r12
	cmpq	32(%r13), %r14
	jne	.L1197
	movl	$1, %edi
	jmp	.L1193
	.p2align 4,,10
	.p2align 3
.L1203:
	movl	$48, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rbx
	jmp	.L1185
	.cfi_endproc
.LFE22697:
	.size	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeESt4pairIKS4_NS1_13ZoneHandleSetINS1_3MapEEEESt10_Select1stISA_ESt4lessIS4_ENS1_13ZoneAllocatorISA_EEE17_M_emplace_uniqueIJRSA_EEES5_ISt17_Rb_tree_iteratorISA_EbEDpOT_, .-_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeESt4pairIKS4_NS1_13ZoneHandleSetINS1_3MapEEEESt10_Select1stISA_ESt4lessIS4_ENS1_13ZoneAllocatorISA_EEE17_M_emplace_uniqueIJRSA_EEES5_ISt17_Rb_tree_iteratorISA_EbEDpOT_
	.section	.text._ZNK2v88internal8compiler15LoadElimination12AbstractMaps4KillERKNS2_14AliasStateInfoEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler15LoadElimination12AbstractMaps4KillERKNS2_14AliasStateInfoEPNS0_4ZoneE
	.type	_ZNK2v88internal8compiler15LoadElimination12AbstractMaps4KillERKNS2_14AliasStateInfoEPNS0_4ZoneE, @function
_ZNK2v88internal8compiler15LoadElimination12AbstractMaps4KillERKNS2_14AliasStateInfoEPNS0_4ZoneE:
.LFB19363:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	16(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	%rdi, -88(%rbp)
	movq	32(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	%r12, %r13
	je	.L1236
	.p2align 4,,10
	.p2align 3
.L1210:
	movq	8(%rbx), %rdi
	movq	32(%r13), %r15
	movq	(%rdi), %rax
	cmpw	$237, 16(%rax)
	je	.L1283
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_18MayAliasEPNS1_4NodeES4_
	testb	%al, %al
	je	.L1213
	movq	16(%rbx), %r9
	testq	%r9, %r9
	je	.L1216
	movq	(%rbx), %rax
	movq	520(%rax), %rsi
	testq	%rsi, %rsi
	je	.L1216
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	movq	24(%rsi), %rdx
	leaq	16(%rsi), %rdi
	testq	%rdx, %rdx
	je	.L1216
	movq	%rdi, %rcx
	jmp	.L1217
	.p2align 4,,10
	.p2align 3
.L1284:
	movq	%rdx, %rcx
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L1218
.L1217:
	cmpq	%rax, 32(%rdx)
	jnb	.L1284
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L1217
.L1218:
	cmpq	%rcx, %rdi
	je	.L1216
	cmpq	%rax, 32(%rcx)
	ja	.L1216
	movq	40(%rcx), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	je	.L1216
	testq	%rdx, %rdx
	je	.L1221
	movq	6(%rax), %rdx
	subq	$2, %rax
	movq	16(%rax), %rax
	movq	%rax, %rcx
	subq	%rdx, %rcx
	cmpq	$8, %rcx
	je	.L1285
	.p2align 4,,10
	.p2align 3
.L1216:
	movq	16(%r14), %r13
	movq	24(%r14), %rax
	subq	%r13, %rax
	cmpq	$55, %rax
	jbe	.L1286
	leaq	56(%r13), %rax
	movq	%rax, 16(%r14)
.L1223:
	leaq	16(%r13), %rax
	movq	%r14, 0(%r13)
	movq	%rax, 32(%r13)
	movq	%rax, 40(%r13)
	movq	-88(%rbp), %rax
	movl	$0, 16(%r13)
	movq	$0, 24(%r13)
	movq	$0, 48(%r13)
	movq	32(%rax), %r15
	leaq	-80(%rbp), %rax
	movq	%r13, -96(%rbp)
	movq	%rax, -88(%rbp)
	cmpq	%r15, %r12
	je	.L1209
	.p2align 4,,10
	.p2align 3
.L1224:
	movdqu	32(%r15), %xmm0
	movq	8(%rbx), %rdi
	movq	32(%r15), %r8
	movaps	%xmm0, -80(%rbp)
	movq	(%rdi), %rax
	movq	%r8, %r14
	cmpw	$237, 16(%rax)
	je	.L1287
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_18MayAliasEPNS1_4NodeES4_
	testb	%al, %al
	je	.L1226
	movq	16(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L1227
	movq	(%rbx), %rax
	movq	520(%rax), %r9
	testq	%r9, %r9
	je	.L1227
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	movq	24(%r9), %rdx
	leaq	16(%r9), %rdi
	testq	%rdx, %rdx
	je	.L1227
	movq	%rdi, %rcx
	jmp	.L1229
	.p2align 4,,10
	.p2align 3
.L1288:
	movq	%rdx, %rcx
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L1230
.L1229:
	cmpq	%rax, 32(%rdx)
	jnb	.L1288
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L1229
.L1230:
	cmpq	%rcx, %rdi
	je	.L1227
	cmpq	%rax, 32(%rcx)
	ja	.L1227
	movq	40(%rcx), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	je	.L1227
	testq	%rdx, %rdx
	je	.L1233
	movq	6(%rax), %rdx
	subq	$2, %rax
	movq	16(%rax), %rax
	movq	%rax, %rcx
	subq	%rdx, %rcx
	cmpq	$8, %rcx
	jne	.L1227
	cmpq	%rax, %rdx
	je	.L1234
	movq	(%rdx), %rax
.L1233:
	cmpq	%rax, %rsi
	je	.L1227
	.p2align 4,,10
	.p2align 3
.L1226:
	movq	-88(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeESt4pairIKS4_NS1_13ZoneHandleSetINS1_3MapEEEESt10_Select1stISA_ESt4lessIS4_ENS1_13ZoneAllocatorISA_EEE17_M_emplace_uniqueIJRSA_EEES5_ISt17_Rb_tree_iteratorISA_EbEDpOT_
.L1227:
	movq	%r15, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r15
	cmpq	%rax, %r12
	jne	.L1224
	jmp	.L1209
	.p2align 4,,10
	.p2align 3
.L1283:
	cmpq	%rdi, %r15
	je	.L1216
.L1213:
	movq	%r13, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r13
	cmpq	%rax, %r12
	jne	.L1210
.L1236:
	movq	-88(%rbp), %rax
	movq	%rax, -96(%rbp)
.L1209:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1289
	movq	-96(%rbp), %rax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1285:
	.cfi_restore_state
	cmpq	%rax, %rdx
	je	.L1234
	movq	(%rdx), %rax
.L1221:
	cmpq	%rax, %r9
	je	.L1216
	movq	%r13, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r13
	cmpq	%rax, %r12
	jne	.L1210
	jmp	.L1236
	.p2align 4,,10
	.p2align 3
.L1287:
	cmpq	%rdi, %r8
	jne	.L1226
	movq	%r15, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r15
	cmpq	%rax, %r12
	jne	.L1224
	jmp	.L1209
	.p2align 4,,10
	.p2align 3
.L1286:
	movl	$56, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r13
	jmp	.L1223
.L1234:
	xorl	%edx, %edx
	xorl	%esi, %esi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L1289:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19363:
	.size	_ZNK2v88internal8compiler15LoadElimination12AbstractMaps4KillERKNS2_14AliasStateInfoEPNS0_4ZoneE, .-_ZNK2v88internal8compiler15LoadElimination12AbstractMaps4KillERKNS2_14AliasStateInfoEPNS0_4ZoneE
	.section	.text._ZNK2v88internal8compiler15LoadElimination13AbstractState8KillMapsERKNS2_14AliasStateInfoEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler15LoadElimination13AbstractState8KillMapsERKNS2_14AliasStateInfoEPNS0_4ZoneE
	.type	_ZNK2v88internal8compiler15LoadElimination13AbstractState8KillMapsERKNS2_14AliasStateInfoEPNS0_4ZoneE, @function
_ZNK2v88internal8compiler15LoadElimination13AbstractState8KillMapsERKNS2_14AliasStateInfoEPNS0_4ZoneE:
.LFB19388:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	520(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1293
	movq	%rdx, %r12
	call	_ZNK2v88internal8compiler15LoadElimination12AbstractMaps4KillERKNS2_14AliasStateInfoEPNS0_4ZoneE
	movq	%rax, %r13
	cmpq	%rax, 520(%rbx)
	jne	.L1297
.L1293:
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1297:
	.cfi_restore_state
	movq	16(%r12), %rax
	movq	24(%r12), %rdx
	subq	%rax, %rdx
	cmpq	$527, %rdx
	jbe	.L1298
	leaq	528(%rax), %rdx
	movq	%rdx, 16(%r12)
.L1295:
	movq	%rbx, %rsi
	movl	$66, %ecx
	movq	%rax, %rdi
	rep movsq
	movq	%r13, 520(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1298:
	.cfi_restore_state
	movl	$528, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1295
	.cfi_endproc
.LFE19388:
	.size	_ZNK2v88internal8compiler15LoadElimination13AbstractState8KillMapsERKNS2_14AliasStateInfoEPNS0_4ZoneE, .-_ZNK2v88internal8compiler15LoadElimination13AbstractState8KillMapsERKNS2_14AliasStateInfoEPNS0_4ZoneE
	.section	.text._ZNK2v88internal8compiler15LoadElimination13AbstractState8KillMapsEPNS1_4NodeEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler15LoadElimination13AbstractState8KillMapsEPNS1_4NodeEPNS0_4ZoneE
	.type	_ZNK2v88internal8compiler15LoadElimination13AbstractState8KillMapsEPNS1_4NodeEPNS0_4ZoneE, @function
_ZNK2v88internal8compiler15LoadElimination13AbstractState8KillMapsEPNS1_4NodeEPNS0_4ZoneE:
.LFB19389:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %xmm0
	movq	%rsi, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	520(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -48(%rbp)
	movaps	%xmm0, -64(%rbp)
	testq	%rdi, %rdi
	je	.L1302
	leaq	-64(%rbp), %rsi
	movq	%rdx, %r12
	call	_ZNK2v88internal8compiler15LoadElimination12AbstractMaps4KillERKNS2_14AliasStateInfoEPNS0_4ZoneE
	movq	%rax, %r13
	cmpq	520(%rbx), %rax
	jne	.L1307
.L1302:
	movq	%rbx, %rax
.L1299:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1308
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1307:
	.cfi_restore_state
	movq	16(%r12), %rax
	movq	24(%r12), %rdx
	subq	%rax, %rdx
	cmpq	$527, %rdx
	jbe	.L1309
	leaq	528(%rax), %rdx
	movq	%rdx, 16(%r12)
.L1304:
	movl	$66, %ecx
	movq	%rax, %rdi
	movq	%rbx, %rsi
	rep movsq
	movq	%r13, 520(%rax)
	jmp	.L1299
	.p2align 4,,10
	.p2align 3
.L1309:
	movl	$528, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1304
.L1308:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19389:
	.size	_ZNK2v88internal8compiler15LoadElimination13AbstractState8KillMapsEPNS1_4NodeEPNS0_4ZoneE, .-_ZNK2v88internal8compiler15LoadElimination13AbstractState8KillMapsEPNS1_4NodeEPNS0_4ZoneE
	.section	.text._ZNK2v88internal8compiler15LoadElimination29ComputeLoopStateForStoreFieldEPNS1_4NodeEPKNS2_13AbstractStateERKNS1_11FieldAccessE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler15LoadElimination29ComputeLoopStateForStoreFieldEPNS1_4NodeEPKNS2_13AbstractStateERKNS1_11FieldAccessE
	.type	_ZNK2v88internal8compiler15LoadElimination29ComputeLoopStateForStoreFieldEPNS1_4NodeEPKNS2_13AbstractStateERKNS1_11FieldAccessE, @function
_ZNK2v88internal8compiler15LoadElimination29ComputeLoopStateForStoreFieldEPNS1_4NodeEPKNS2_13AbstractStateERKNS1_11FieldAccessE:
.LFB19435:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	xorl	%esi, %esi
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rcx, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rax, -72(%rbp)
	movl	4(%rbx), %eax
	testl	%eax, %eax
	jne	.L1311
	movq	%r12, %xmm0
	movq	520(%r12), %rdi
	movq	$0, -48(%rbp)
	movhps	-72(%rbp), %xmm0
	movq	16(%r13), %r13
	movaps	%xmm0, -64(%rbp)
	testq	%rdi, %rdi
	je	.L1314
	leaq	-64(%rbp), %rsi
	movq	%r13, %rdx
	call	_ZNK2v88internal8compiler15LoadElimination12AbstractMaps4KillERKNS2_14AliasStateInfoEPNS0_4ZoneE
	movq	%rax, %rbx
	cmpq	520(%r12), %rax
	jne	.L1321
.L1314:
	movq	%r12, %rax
	jmp	.L1310
	.p2align 4,,10
	.p2align 3
.L1311:
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler15LoadElimination12FieldIndexOfERKNS1_11FieldAccessE
	movq	%rax, %rdx
	movl	%eax, %ecx
	sarq	$32, %rdx
	andl	%edx, %ecx
	cmpl	$-1, %ecx
	je	.L1322
	movq	%rdx, %rcx
	movl	%eax, %edx
	movq	%r12, %xmm0
	movq	16(%r13), %r8
	salq	$32, %rcx
	movhps	-72(%rbp), %xmm0
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdi
	orq	%rcx, %rdx
	movq	8(%rbx), %rcx
	movq	$0, -48(%rbp)
	movaps	%xmm0, -64(%rbp)
	call	_ZNK2v88internal8compiler15LoadElimination13AbstractState9KillFieldERKNS2_14AliasStateInfoENS2_10IndexRangeENS0_11MaybeHandleINS0_4NameEEEPNS0_4ZoneE
.L1310:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1323
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1321:
	.cfi_restore_state
	movq	16(%r13), %rax
	movq	24(%r13), %rdx
	subq	%rax, %rdx
	cmpq	$527, %rdx
	jbe	.L1324
	leaq	528(%rax), %rdx
	movq	%rdx, 16(%r13)
.L1316:
	movl	$66, %ecx
	movq	%rax, %rdi
	movq	%r12, %rsi
	rep movsq
	movq	%rbx, 520(%rax)
	jmp	.L1310
	.p2align 4,,10
	.p2align 3
.L1322:
	movq	16(%r13), %rcx
	movq	8(%rbx), %rdx
	movq	%r12, %rdi
	movq	-72(%rbp), %rsi
	call	_ZNK2v88internal8compiler15LoadElimination13AbstractState10KillFieldsEPNS1_4NodeENS0_11MaybeHandleINS0_4NameEEEPNS0_4ZoneE
	jmp	.L1310
.L1324:
	movl	$528, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1316
.L1323:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19435:
	.size	_ZNK2v88internal8compiler15LoadElimination29ComputeLoopStateForStoreFieldEPNS1_4NodeEPKNS2_13AbstractStateERKNS1_11FieldAccessE, .-_ZNK2v88internal8compiler15LoadElimination29ComputeLoopStateForStoreFieldEPNS1_4NodeEPKNS2_13AbstractStateERKNS1_11FieldAccessE
	.section	.text._ZNK2v88internal8compiler15LoadElimination12AbstractMaps5MergeEPKS3_PNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler15LoadElimination12AbstractMaps5MergeEPKS3_PNS0_4ZoneE
	.type	_ZNK2v88internal8compiler15LoadElimination12AbstractMaps5MergeEPKS3_PNS0_4ZoneE, @function
_ZNK2v88internal8compiler15LoadElimination12AbstractMaps5MergeEPKS3_PNS0_4ZoneE:
.LFB19368:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	%rsi, %rdi
	je	.L1325
	movq	48(%rdi), %rax
	movq	%rdi, %r12
	movq	%rsi, %rbx
	leaq	16(%rdi), %r14
	cmpq	%rax, 48(%rsi)
	je	.L1368
.L1327:
	movq	16(%rdx), %r13
	movq	24(%rdx), %rax
	subq	%r13, %rax
	cmpq	$55, %rax
	jbe	.L1369
	leaq	56(%r13), %rax
	movq	%rax, 16(%rdx)
.L1336:
	leaq	16(%r13), %rax
	movq	%rdx, 0(%r13)
	movq	%r13, %r15
	movl	$0, 16(%r13)
	movq	$0, 24(%r13)
	movq	%rax, 32(%r13)
	movq	%rax, 40(%r13)
	movq	$0, 48(%r13)
	movq	32(%r12), %r8
	leaq	16(%rbx), %r12
	cmpq	%r14, %r8
	je	.L1325
	.p2align 4,,10
	.p2align 3
.L1347:
	movdqu	32(%r8), %xmm0
	movq	24(%rbx), %rax
	movq	32(%r8), %rdx
	movaps	%xmm0, -80(%rbp)
	testq	%rax, %rax
	je	.L1338
	movq	%r12, %rcx
	jmp	.L1339
	.p2align 4,,10
	.p2align 3
.L1370:
	movq	%rax, %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L1340
.L1339:
	cmpq	%rdx, 32(%rax)
	jnb	.L1370
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L1339
.L1340:
	cmpq	%r12, %rcx
	je	.L1338
	cmpq	%rdx, 32(%rcx)
	ja	.L1338
	movq	-72(%rbp), %rdx
	movq	40(%rcx), %rax
	cmpq	%rdx, %rax
	je	.L1343
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$2, %rcx
	je	.L1371
.L1338:
	movq	%r8, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r8
	cmpq	%r14, %rax
	jne	.L1347
.L1325:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1372
	addq	$56, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1371:
	.cfi_restore_state
	movq	%rdx, %rcx
	andl	$3, %ecx
	cmpq	$2, %rcx
	jne	.L1338
	movq	6(%rax), %rsi
	movq	14(%rax), %rcx
	movq	6(%rdx), %rdi
	movq	14(%rdx), %rax
	subq	%rsi, %rcx
	subq	%rdi, %rax
	cmpq	%rax, %rcx
	jne	.L1338
	sarq	$3, %rcx
	je	.L1343
	movq	(%rsi), %rdx
	xorl	%eax, %eax
	jmp	.L1345
	.p2align 4,,10
	.p2align 3
.L1373:
	addq	$1, %rax
	cmpq	%rax, %rcx
	je	.L1343
	movq	(%rsi,%rax,8), %rdx
.L1345:
	cmpq	%rdx, (%rdi,%rax,8)
	je	.L1373
	jmp	.L1338
	.p2align 4,,10
	.p2align 3
.L1343:
	leaq	-80(%rbp), %rsi
	movq	%r13, %rdi
	movq	%r8, -88(%rbp)
	call	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeESt4pairIKS4_NS1_13ZoneHandleSetINS1_3MapEEEESt10_Select1stISA_ESt4lessIS4_ENS1_13ZoneAllocatorISA_EEE17_M_emplace_uniqueIJRSA_EEES5_ISt17_Rb_tree_iteratorISA_EbEDpOT_
	movq	-88(%rbp), %r8
	movq	%r8, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r8
	cmpq	%r14, %rax
	jne	.L1347
	jmp	.L1325
.L1368:
	movq	32(%rdi), %r13
	movq	32(%rsi), %r15
	cmpq	%r14, %r13
	je	.L1328
.L1334:
	movq	32(%r15), %rax
	cmpq	%rax, 32(%r13)
	jne	.L1327
	movq	40(%r13), %rcx
	movq	40(%r15), %rax
	cmpq	%rax, %rcx
	je	.L1330
	movq	%rcx, %rsi
	andl	$3, %esi
	cmpq	$2, %rsi
	jne	.L1327
	movq	%rax, %rsi
	andl	$3, %esi
	cmpq	$2, %rsi
	jne	.L1327
	leaq	-2(%rcx), %rsi
	leaq	-2(%rax), %rcx
	movq	8(%rsi), %rdi
	movq	16(%rsi), %rax
	movq	8(%rcx), %rsi
	movq	16(%rcx), %rcx
	subq	%rdi, %rax
	subq	%rsi, %rcx
	cmpq	%rcx, %rax
	jne	.L1327
	sarq	$3, %rax
	je	.L1330
	movq	(%rdi), %r8
	xorl	%ecx, %ecx
	jmp	.L1332
	.p2align 4,,10
	.p2align 3
.L1333:
	movq	(%rdi,%rcx,8), %r8
.L1332:
	cmpq	%r8, (%rsi,%rcx,8)
	jne	.L1327
	addq	$1, %rcx
	cmpq	%rcx, %rax
	jne	.L1333
.L1330:
	movq	%r13, %rdi
	movq	%rdx, -88(%rbp)
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	cmpq	%r14, %r13
	movq	-88(%rbp), %rdx
	movq	%rax, %r15
	jne	.L1334
.L1328:
	movq	%r12, %r15
	jmp	.L1325
	.p2align 4,,10
	.p2align 3
.L1369:
	movq	%rdx, %rdi
	movl	$56, %esi
	movq	%rdx, -88(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-88(%rbp), %rdx
	movq	%rax, %r13
	jmp	.L1336
.L1372:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19368:
	.size	_ZNK2v88internal8compiler15LoadElimination12AbstractMaps5MergeEPKS3_PNS0_4ZoneE, .-_ZNK2v88internal8compiler15LoadElimination12AbstractMaps5MergeEPKS3_PNS0_4ZoneE
	.section	.text._ZN2v88internal8compiler15LoadElimination13AbstractState5MergeEPKS3_PNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15LoadElimination13AbstractState5MergeEPKS3_PNS0_4ZoneE
	.type	_ZN2v88internal8compiler15LoadElimination13AbstractState5MergeEPKS3_PNS0_4ZoneE, @function
_ZN2v88internal8compiler15LoadElimination13AbstractState5MergeEPKS3_PNS0_4ZoneE:
.LFB19385:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rsi
	testq	%rsi, %rsi
	je	.L1375
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1376
	call	_ZNK2v88internal8compiler15LoadElimination16AbstractElements5MergeEPKS3_PNS0_4ZoneE
	movq	%rax, %rdi
.L1376:
	movq	%rdi, (%rbx)
.L1375:
	leaq	8(%r12), %rdx
	leaq	8(%rbx), %rsi
	movq	%r13, %rcx
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler15LoadElimination13AbstractState11FieldsMergeEPSt5arrayIPKNS2_13AbstractFieldELm32EERKS8_PNS0_4ZoneE
	leaq	264(%rbx), %rsi
	movq	%r13, %rcx
	movq	%rbx, %rdi
	leaq	264(%r12), %rdx
	call	_ZN2v88internal8compiler15LoadElimination13AbstractState11FieldsMergeEPSt5arrayIPKNS2_13AbstractFieldELm32EERKS8_PNS0_4ZoneE
	movq	520(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L1374
	movq	520(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1378
	movq	%r13, %rdx
	call	_ZNK2v88internal8compiler15LoadElimination12AbstractMaps5MergeEPKS3_PNS0_4ZoneE
	movq	%rax, %rdi
.L1378:
	movq	%rdi, 520(%rbx)
.L1374:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19385:
	.size	_ZN2v88internal8compiler15LoadElimination13AbstractState5MergeEPKS3_PNS0_4ZoneE, .-_ZN2v88internal8compiler15LoadElimination13AbstractState5MergeEPKS3_PNS0_4ZoneE
	.section	.rodata._ZNSt6vectorIPKN2v88internal8compiler15LoadElimination13AbstractStateENS1_13ZoneAllocatorIS6_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS6_S9_EEmRKS6_.str1.1,"aMS",@progbits,1
.LC12:
	.string	"vector::_M_fill_insert"
	.section	.text._ZNSt6vectorIPKN2v88internal8compiler15LoadElimination13AbstractStateENS1_13ZoneAllocatorIS6_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS6_S9_EEmRKS6_,"axG",@progbits,_ZNSt6vectorIPKN2v88internal8compiler15LoadElimination13AbstractStateENS1_13ZoneAllocatorIS6_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS6_S9_EEmRKS6_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPKN2v88internal8compiler15LoadElimination13AbstractStateENS1_13ZoneAllocatorIS6_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS6_S9_EEmRKS6_
	.type	_ZNSt6vectorIPKN2v88internal8compiler15LoadElimination13AbstractStateENS1_13ZoneAllocatorIS6_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS6_S9_EEmRKS6_, @function
_ZNSt6vectorIPKN2v88internal8compiler15LoadElimination13AbstractStateENS1_13ZoneAllocatorIS6_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS6_S9_EEmRKS6_:
.LFB22733:
	.cfi_startproc
	endbr64
	testq	%rdx, %rdx
	je	.L1502
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	16(%rdi), %rdi
	movq	24(%r12), %rax
	subq	%rdi, %rax
	sarq	$3, %rax
	cmpq	%rdx, %rax
	jb	.L1395
	movq	%rdi, %r8
	movq	(%rcx), %xmm0
	subq	%rsi, %r8
	movq	%r8, %rax
	sarq	$3, %rax
	cmpq	%rax, %rdx
	jnb	.L1396
	leaq	0(,%rdx,8), %r14
	movq	%rdi, %rdx
	subq	%r14, %rdx
	cmpq	%rdx, %rdi
	je	.L1443
	leaq	-8(%rdi), %rax
	movl	$16, %ecx
	subq	%rdx, %rax
	subq	%r14, %rcx
	shrq	$3, %rax
	testq	%rcx, %rcx
	leaq	16(%rdi), %rcx
	setle	%sil
	cmpq	%rcx, %rdx
	setnb	%cl
	orb	%cl, %sil
	je	.L1444
	movabsq	$2305843009213693948, %rcx
	testq	%rcx, %rax
	je	.L1444
	leaq	1(%rax), %r8
	xorl	%eax, %eax
	movq	%r8, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L1399:
	movdqu	(%rdx,%rax), %xmm2
	movups	%xmm2, (%rdi,%rax)
	addq	$16, %rax
	cmpq	%rax, %rcx
	jne	.L1399
	movq	%r8, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rcx
	leaq	(%rdx,%rcx), %rsi
	addq	%rdi, %rcx
	cmpq	%rax, %r8
	je	.L1401
	movq	(%rsi), %rax
	movq	%rax, (%rcx)
.L1401:
	movq	16(%r12), %rax
.L1397:
	addq	%r14, %rax
	movq	%rax, 16(%r12)
	cmpq	%rdx, %rbx
	je	.L1402
	subq	%rbx, %rdx
	movq	%rbx, %rsi
	movq	%xmm0, -40(%rbp)
	subq	%rdx, %rdi
	call	memmove@PLT
	movq	-40(%rbp), %xmm0
.L1402:
	leaq	(%rbx,%r14), %rdx
	cmpq	%rdx, %rbx
	je	.L1392
	subq	%rbx, %rdx
	movq	%rbx, %rax
	subq	$8, %rdx
	movq	%rdx, %rcx
	shrq	$3, %rcx
	addq	$1, %rcx
	testq	%rdx, %rdx
	je	.L1442
	movq	%rcx, %rdx
	movdqa	%xmm0, %xmm1
	shrq	%rdx
	punpcklqdq	%xmm1, %xmm1
	salq	$4, %rdx
	addq	%rbx, %rdx
	.p2align 4,,10
	.p2align 3
.L1406:
	movups	%xmm1, (%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L1406
	movq	%rcx, %rax
	andq	$-2, %rax
	leaq	(%rbx,%rax,8), %r13
	cmpq	%rax, %rcx
	je	.L1392
.L1442:
	movq	%xmm0, 0(%r13)
.L1392:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1502:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	ret
	.p2align 4,,10
	.p2align 3
.L1396:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	subq	%rax, %rdx
	je	.L1445
	cmpq	$1, %rdx
	je	.L1446
	movq	%rdx, %rsi
	movdqa	%xmm0, %xmm1
	xorl	%eax, %eax
	shrq	%rsi
	punpcklqdq	%xmm1, %xmm1
	.p2align 4,,10
	.p2align 3
.L1410:
	movq	%rax, %rcx
	addq	$1, %rax
	salq	$4, %rcx
	movups	%xmm1, (%rdi,%rcx)
	cmpq	%rax, %rsi
	jne	.L1410
	movq	%rdx, %rcx
	andq	$-2, %rcx
	leaq	(%rdi,%rcx,8), %rax
	cmpq	%rcx, %rdx
	je	.L1411
.L1409:
	movq	%xmm0, (%rax)
.L1411:
	leaq	(%rdi,%rdx,8), %rsi
.L1408:
	movq	%rsi, 16(%r12)
	cmpq	%rbx, %rdi
	je	.L1412
	subq	%rbx, %rdi
	leaq	15(%rbx), %rcx
	movq	%rbx, %rdx
	leaq	-8(%rdi), %r9
	subq	%rsi, %rcx
	movq	%r9, %rax
	shrq	$3, %rax
	cmpq	$30, %rcx
	jbe	.L1447
	movabsq	$2305843009213693948, %rcx
	testq	%rcx, %rax
	je	.L1447
	leaq	1(%rax), %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	shrq	%rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L1414:
	movdqu	(%rbx,%rax), %xmm3
	movups	%xmm3, (%rsi,%rax)
	addq	$16, %rax
	cmpq	%rdi, %rax
	jne	.L1414
	movq	%rcx, %r9
	andq	$-2, %r9
	leaq	0(,%r9,8), %rax
	leaq	(%rbx,%rax), %rdi
	addq	%rax, %rsi
	cmpq	%r9, %rcx
	je	.L1415
	movq	(%rdi), %rax
	movq	%rax, (%rsi)
.L1415:
	addq	%r8, 16(%r12)
	movq	%rcx, %rax
.L1441:
	movq	%rax, %rcx
	movdqa	%xmm0, %xmm1
	shrq	%rcx
	punpcklqdq	%xmm1, %xmm1
	salq	$4, %rcx
	addq	%rbx, %rcx
	.p2align 4,,10
	.p2align 3
.L1419:
	movups	%xmm1, (%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rcx
	jne	.L1419
	movq	%rax, %rdx
	andq	$-2, %rdx
	leaq	(%rbx,%rdx,8), %r13
	cmpq	%rax, %rdx
	jne	.L1442
	jmp	.L1392
	.p2align 4,,10
	.p2align 3
.L1395:
	movq	8(%r12), %rsi
	movl	$268435455, %r8d
	movq	%r8, %rax
	subq	%rsi, %rdi
	sarq	$3, %rdi
	subq	%rdi, %rax
	cmpq	%rax, %rdx
	ja	.L1505
	cmpq	%rdi, %rdx
	movq	%rdi, %rax
	cmovnb	%rdx, %rax
	addq	%rax, %rdi
	setc	%al
	subq	%rsi, %r13
	movzbl	%al, %eax
	testq	%rax, %rax
	jne	.L1448
	testq	%rdi, %rdi
	jne	.L1506
	xorl	%r8d, %r8d
	xorl	%eax, %eax
.L1424:
	leaq	0(,%rdx,8), %r9
	leaq	(%rax,%r13), %r11
	leaq	0(%r13,%r9), %rsi
	addq	%rax, %rsi
	cmpq	%rsi, %rcx
	leaq	8(%rcx), %rsi
	setnb	%dil
	cmpq	%rsi, %r11
	setnb	%sil
	orb	%sil, %dil
	je	.L1450
	leaq	-1(%rdx), %rsi
	cmpq	$3, %rsi
	jbe	.L1450
	movq	(%rcx), %xmm0
	leaq	-2(%rdx), %rdi
	xorl	%esi, %esi
	shrq	%rdi
	addq	$1, %rdi
	punpcklqdq	%xmm0, %xmm0
	.p2align 4,,10
	.p2align 3
.L1428:
	movq	%rsi, %r10
	addq	$1, %rsi
	salq	$4, %r10
	movups	%xmm0, (%r11,%r10)
	cmpq	%rsi, %rdi
	ja	.L1428
	leaq	(%rdi,%rdi), %rsi
	salq	$4, %rdi
	addq	%r11, %rdi
	cmpq	%rsi, %rdx
	je	.L1430
	movq	(%rcx), %rdx
	movq	%rdx, (%rdi)
.L1430:
	movq	8(%r12), %rcx
	cmpq	%rcx, %rbx
	je	.L1451
	leaq	-8(%rbx), %r10
	leaq	15(%rcx), %rdx
	subq	%rcx, %r10
	subq	%rax, %rdx
	movq	%r10, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rdx
	jbe	.L1452
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rsi
	je	.L1452
	leaq	1(%rsi), %r11
	xorl	%edx, %edx
	movq	%r11, %rdi
	shrq	%rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L1433:
	movdqu	(%rcx,%rdx), %xmm4
	movups	%xmm4, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rdi, %rdx
	jne	.L1433
	movq	%r11, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rsi
	addq	%rsi, %rcx
	addq	%rax, %rsi
	cmpq	%rdx, %r11
	je	.L1435
	movq	(%rcx), %rdx
	movq	%rdx, (%rsi)
.L1435:
	leaq	8(%rax,%r10), %rdi
.L1431:
	movq	16(%r12), %rdx
	leaq	(%rdi,%r9), %rcx
	cmpq	%rdx, %rbx
	je	.L1436
	subq	%rbx, %rdx
	leaq	-8(%rdx), %r10
	leaq	16(%rdi,%r9), %rdx
	movq	%r10, %rsi
	shrq	$3, %rsi
	cmpq	%rdx, %rbx
	leaq	16(%rbx), %rdx
	setnb	%dil
	cmpq	%rdx, %rcx
	setnb	%dl
	orb	%dl, %dil
	je	.L1453
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rsi
	je	.L1453
	addq	$1, %rsi
	xorl	%edx, %edx
	movq	%rsi, %rdi
	shrq	%rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L1438:
	movdqu	(%rbx,%rdx), %xmm5
	movups	%xmm5, (%rcx,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rdi
	jne	.L1438
	movq	%rsi, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rdi
	addq	%rdi, %rbx
	addq	%rcx, %rdi
	cmpq	%rdx, %rsi
	je	.L1440
	movq	(%rbx), %rdx
	movq	%rdx, (%rdi)
.L1440:
	leaq	8(%rcx,%r10), %rcx
.L1436:
	movq	%rax, %xmm0
	movq	%rcx, %xmm6
	movq	%r8, 24(%r12)
	punpcklqdq	%xmm6, %xmm0
	movups	%xmm0, 8(%r12)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1448:
	.cfi_restore_state
	movl	$2147483640, %esi
	movl	$2147483640, %r14d
.L1423:
	movq	(%r12), %r8
	movq	16(%r8), %rax
	movq	24(%r8), %rdi
	subq	%rax, %rdi
	cmpq	%rsi, %rdi
	jb	.L1507
	addq	%rax, %rsi
	movq	%rsi, 16(%r8)
.L1426:
	leaq	(%rax,%r14), %r8
	jmp	.L1424
	.p2align 4,,10
	.p2align 3
.L1450:
	xorl	%esi, %esi
	.p2align 4,,10
	.p2align 3
.L1427:
	movq	(%rcx), %rdi
	movq	%rdi, (%r11,%rsi,8)
	addq	$1, %rsi
	cmpq	%rsi, %rdx
	jne	.L1427
	jmp	.L1430
	.p2align 4,,10
	.p2align 3
.L1444:
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L1398:
	movq	(%rdx,%rcx,8), %rsi
	movq	%rsi, (%rdi,%rcx,8)
	movq	%rcx, %rsi
	addq	$1, %rcx
	cmpq	%rax, %rsi
	jne	.L1398
	jmp	.L1401
	.p2align 4,,10
	.p2align 3
.L1447:
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L1413:
	movq	(%rbx,%rcx,8), %rdi
	movq	%rdi, (%rsi,%rcx,8)
	movq	%rcx, %rdi
	addq	$1, %rcx
	cmpq	%rax, %rdi
	jne	.L1413
	addq	%r8, 16(%r12)
	addq	$1, %rax
	testq	%r9, %r9
	je	.L1442
	jmp	.L1441
	.p2align 4,,10
	.p2align 3
.L1452:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L1432:
	movq	(%rcx,%rdx,8), %rdi
	movq	%rdi, (%rax,%rdx,8)
	movq	%rdx, %rdi
	addq	$1, %rdx
	cmpq	%rsi, %rdi
	jne	.L1432
	jmp	.L1435
	.p2align 4,,10
	.p2align 3
.L1453:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L1437:
	movq	(%rbx,%rdx,8), %rdi
	movq	%rdi, (%rcx,%rdx,8)
	movq	%rdx, %rdi
	addq	$1, %rdx
	cmpq	%rsi, %rdi
	jne	.L1437
	jmp	.L1440
	.p2align 4,,10
	.p2align 3
.L1445:
	movq	%rdi, %rsi
	jmp	.L1408
	.p2align 4,,10
	.p2align 3
.L1443:
	movq	%rdi, %rax
	jmp	.L1397
	.p2align 4,,10
	.p2align 3
.L1412:
	addq	%r8, %rsi
	movq	%rsi, 16(%r12)
	jmp	.L1392
	.p2align 4,,10
	.p2align 3
.L1451:
	movq	%rax, %rdi
	jmp	.L1431
.L1446:
	movq	%rdi, %rax
	jmp	.L1409
.L1507:
	movq	%r8, %rdi
	movq	%rcx, -48(%rbp)
	movq	%rdx, -40(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-40(%rbp), %rdx
	movq	-48(%rbp), %rcx
	jmp	.L1426
.L1506:
	cmpq	$268435455, %rdi
	cmova	%r8, %rdi
	leaq	0(,%rdi,8), %r14
	movq	%r14, %rsi
	jmp	.L1423
.L1505:
	leaq	.LC12(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE22733:
	.size	_ZNSt6vectorIPKN2v88internal8compiler15LoadElimination13AbstractStateENS1_13ZoneAllocatorIS6_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS6_S9_EEmRKS6_, .-_ZNSt6vectorIPKN2v88internal8compiler15LoadElimination13AbstractStateENS1_13ZoneAllocatorIS6_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS6_S9_EEmRKS6_
	.section	.text._ZN2v88internal8compiler15LoadElimination27AbstractStateForEffectNodes3SetEPNS1_4NodeEPKNS2_13AbstractStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15LoadElimination27AbstractStateForEffectNodes3SetEPNS1_4NodeEPKNS2_13AbstractStateE
	.type	_ZN2v88internal8compiler15LoadElimination27AbstractStateForEffectNodes3SetEPNS1_4NodeEPKNS2_13AbstractStateE, @function
_ZN2v88internal8compiler15LoadElimination27AbstractStateForEffectNodes3SetEPNS1_4NodeEPKNS2_13AbstractStateE:
.LFB19417:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -32
	movl	20(%rsi), %ebx
	movq	16(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rcx
	movq	%rsi, %rax
	andl	$16777215, %ebx
	subq	%rcx, %rax
	sarq	$3, %rax
	cmpq	%rax, %rbx
	jnb	.L1514
.L1509:
	movq	%r13, (%rcx,%rbx,8)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1515
	addq	$32, %rsp
	popq	%rbx
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1514:
	.cfi_restore_state
	leaq	1(%rbx), %rdx
	movq	$0, -32(%rbp)
	cmpq	%rax, %rdx
	ja	.L1516
	jnb	.L1509
	leaq	(%rcx,%rdx,8), %rax
	cmpq	%rax, %rsi
	je	.L1509
	movq	%rax, 16(%rdi)
	jmp	.L1509
	.p2align 4,,10
	.p2align 3
.L1516:
	leaq	-32(%rbp), %rcx
	subq	%rax, %rdx
	movq	%rdi, -40(%rbp)
	call	_ZNSt6vectorIPKN2v88internal8compiler15LoadElimination13AbstractStateENS1_13ZoneAllocatorIS6_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS6_S9_EEmRKS6_
	movq	-40(%rbp), %rdi
	movq	8(%rdi), %rcx
	jmp	.L1509
.L1515:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19417:
	.size	_ZN2v88internal8compiler15LoadElimination27AbstractStateForEffectNodes3SetEPNS1_4NodeEPKNS2_13AbstractStateE, .-_ZN2v88internal8compiler15LoadElimination27AbstractStateForEffectNodes3SetEPNS1_4NodeEPKNS2_13AbstractStateE
	.section	.text._ZN2v88internal8compiler15LoadElimination11UpdateStateEPNS1_4NodeEPKNS2_13AbstractStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15LoadElimination11UpdateStateEPNS1_4NodeEPKNS2_13AbstractStateE
	.type	_ZN2v88internal8compiler15LoadElimination11UpdateStateEPNS1_4NodeEPKNS2_13AbstractStateE, @function
_ZN2v88internal8compiler15LoadElimination11UpdateStateEPNS1_4NodeEPKNS2_13AbstractStateE:
.LFB19434:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movl	20(%rsi), %r12d
	movq	32(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	movq	%rsi, %rcx
	andl	$16777215, %r12d
	subq	%rax, %rcx
	sarq	$3, %rcx
	cmpq	%rcx, %r12
	jnb	.L1564
	leaq	(%rax,%r12,8), %r12
	movq	(%r12), %rcx
	cmpq	%rcx, %rdx
	je	.L1544
	testq	%rcx, %rcx
	je	.L1521
	movq	(%rdx), %rsi
	movq	(%rcx), %rdi
	testq	%rsi, %rsi
	je	.L1522
	movq	%rcx, -72(%rbp)
	testq	%rdi, %rdi
	je	.L1521
	call	_ZNK2v88internal8compiler15LoadElimination16AbstractElements6EqualsEPKS3_
	xorl	%r15d, %r15d
	movq	-72(%rbp), %rcx
	testb	%al, %al
	je	.L1521
.L1533:
	movq	8(%r13,%r15,8), %rdx
	movq	8(%rcx,%r15,8), %rax
	testq	%rdx, %rdx
	je	.L1524
	testq	%rax, %rax
	je	.L1526
	cmpq	%rax, %rdx
	je	.L1527
	movq	48(%rdx), %rdi
	cmpq	%rdi, 48(%rax)
	je	.L1565
.L1526:
	movq	32(%r14), %rsi
	movq	24(%r14), %rax
	movl	20(%rbx), %r12d
	movq	%rsi, %rcx
	subq	%rax, %rcx
	andl	$16777215, %r12d
	sarq	$3, %rcx
	leaq	0(,%r12,8), %rdx
	cmpq	%rcx, %r12
	jnb	.L1519
	leaq	(%rax,%rdx), %r12
	.p2align 4,,10
	.p2align 3
.L1521:
	movq	%r13, (%r12)
	movq	%rbx, %rax
	jmp	.L1543
	.p2align 4,,10
	.p2align 3
.L1534:
	testq	%rax, %rax
	jne	.L1526
	.p2align 4,,10
	.p2align 3
.L1544:
	xorl	%eax, %eax
.L1543:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1566
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1564:
	.cfi_restore_state
	testq	%rdx, %rdx
	je	.L1544
.L1519:
	leaq	1(%r12), %rdx
	movq	$0, -64(%rbp)
	cmpq	%rcx, %rdx
	ja	.L1567
	jnb	.L1542
	leaq	(%rax,%rdx,8), %rdx
	cmpq	%rsi, %rdx
	je	.L1542
	movq	%rdx, 32(%r14)
.L1542:
	leaq	(%rax,%r12,8), %r12
	movq	%rbx, %rax
	movq	%r13, (%r12)
	jmp	.L1543
	.p2align 4,,10
	.p2align 3
.L1524:
	testq	%rax, %rax
	jne	.L1526
.L1527:
	addq	$1, %r15
	cmpq	$32, %r15
	jne	.L1533
	leaq	264(%rcx), %rdx
	leaq	264(%r13), %rsi
	movq	%r13, %rdi
	movq	%rcx, -72(%rbp)
	call	_ZNK2v88internal8compiler15LoadElimination13AbstractState12FieldsEqualsERKSt5arrayIPKNS2_13AbstractFieldELm32EESA_
	movq	-72(%rbp), %rcx
	testb	%al, %al
	je	.L1526
	movq	520(%r13), %rdx
	movq	520(%rcx), %rax
	testq	%rdx, %rdx
	je	.L1534
	testq	%rax, %rax
	je	.L1526
	cmpq	%rax, %rdx
	je	.L1544
	movq	48(%rdx), %rdi
	cmpq	%rdi, 48(%rax)
	jne	.L1526
	movq	32(%rax), %r12
	movq	32(%rdx), %r15
	leaq	16(%rax), %rdx
	cmpq	%r12, %rdx
	je	.L1544
.L1540:
	movq	32(%r15), %rax
	cmpq	%rax, 32(%r12)
	jne	.L1526
	movq	40(%r12), %rcx
	movq	40(%r15), %rax
	cmpq	%rax, %rcx
	je	.L1537
	movq	%rcx, %rsi
	andl	$3, %esi
	cmpq	$2, %rsi
	jne	.L1526
	movq	%rax, %rsi
	andl	$3, %esi
	cmpq	$2, %rsi
	jne	.L1526
	leaq	-2(%rcx), %rsi
	leaq	-2(%rax), %rcx
	movq	8(%rsi), %rdi
	movq	16(%rsi), %rax
	movq	8(%rcx), %rsi
	movq	16(%rcx), %rcx
	subq	%rdi, %rax
	subq	%rsi, %rcx
	cmpq	%rcx, %rax
	jne	.L1526
	sarq	$3, %rax
	xorl	%ecx, %ecx
	jmp	.L1539
.L1568:
	movq	(%rsi,%rcx,8), %r9
	cmpq	%r9, (%rdi,%rcx,8)
	jne	.L1526
	addq	$1, %rcx
.L1539:
	cmpq	%rcx, %rax
	jne	.L1568
.L1537:
	movq	%r12, %rdi
	movq	%rdx, -72(%rbp)
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	-72(%rbp), %rdx
	movq	%rax, %r15
	cmpq	%r12, %rdx
	jne	.L1540
	jmp	.L1544
	.p2align 4,,10
	.p2align 3
.L1565:
	movq	32(%rax), %r12
	movq	32(%rdx), %r8
	leaq	16(%rax), %rdx
	cmpq	%r12, %rdx
	je	.L1527
.L1531:
	movq	32(%r8), %rax
	cmpq	%rax, 32(%r12)
	jne	.L1526
	movq	40(%r8), %rax
	cmpq	%rax, 40(%r12)
	jne	.L1526
	movzbl	48(%r8), %eax
	cmpb	%al, 48(%r12)
	jne	.L1526
	movq	56(%r8), %rax
	cmpq	%rax, 56(%r12)
	jne	.L1526
	leaq	64(%r8), %rsi
	leaq	64(%r12), %rdi
	movq	%rdx, -88(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%r8, -72(%rbp)
	call	_ZN2v88internal8compilereqERKNS1_14ConstFieldInfoES4_@PLT
	testb	%al, %al
	je	.L1526
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	-72(%rbp), %r8
	movq	%rax, %r12
	movq	%r8, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	-88(%rbp), %rdx
	movq	-80(%rbp), %rcx
	movq	%rax, %r8
	cmpq	%r12, %rdx
	jne	.L1531
	jmp	.L1527
	.p2align 4,,10
	.p2align 3
.L1522:
	testq	%rdi, %rdi
	jne	.L1521
	xorl	%r15d, %r15d
	jmp	.L1533
	.p2align 4,,10
	.p2align 3
.L1567:
	leaq	-64(%rbp), %r8
	subq	%rcx, %rdx
	leaq	16(%r14), %rdi
	movq	%r8, %rcx
	call	_ZNSt6vectorIPKN2v88internal8compiler15LoadElimination13AbstractStateENS1_13ZoneAllocatorIS6_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS6_S9_EEmRKS6_
	movq	24(%r14), %rax
	jmp	.L1542
.L1566:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19434:
	.size	_ZN2v88internal8compiler15LoadElimination11UpdateStateEPNS1_4NodeEPKNS2_13AbstractStateE, .-_ZN2v88internal8compiler15LoadElimination11UpdateStateEPNS1_4NodeEPKNS2_13AbstractStateE
	.section	.text._ZN2v88internal8compiler15LoadElimination17ReduceLoadElementEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15LoadElimination17ReduceLoadElementEPNS1_4NodeE
	.type	_ZN2v88internal8compiler15LoadElimination17ReduceLoadElementEPNS1_4NodeE, @function
_ZN2v88internal8compiler15LoadElimination17ReduceLoadElementEPNS1_4NodeE:
.LFB19427:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	movq	%r12, %rdi
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	24(%r15), %rcx
	movl	20(%rax), %edx
	movq	%rax, %rbx
	movq	32(%r15), %rax
	subq	%rcx, %rax
	andl	$16777215, %edx
	sarq	$3, %rax
	cmpq	%rdx, %rax
	jbe	.L1575
	movq	(%rcx,%rdx,8), %r10
	testq	%r10, %r10
	je	.L1575
	movq	(%r12), %rdi
	movq	%r10, -72(%rbp)
	call	_ZN2v88internal8compiler15ElementAccessOfEPKNS1_8OperatorE@PLT
	movq	-72(%rbp), %r10
	movzbl	16(%rax), %r8d
	movq	%rax, %r9
	cmpb	$11, %r8b
	ja	.L1573
	cmpb	$5, %r8b
	ja	.L1574
.L1575:
	xorl	%eax, %eax
.L1572:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1590
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1573:
	.cfi_restore_state
	leal	-13(%r8), %eax
	cmpb	$1, %al
	ja	.L1575
.L1574:
	movq	(%r10), %rdi
	movq	%r9, -72(%rbp)
	testq	%rdi, %rdi
	je	.L1576
	movl	%r8d, %ecx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r10, -88(%rbp)
	movb	%r8b, -80(%rbp)
	call	_ZNK2v88internal8compiler15LoadElimination16AbstractElements6LookupEPNS1_4NodeES5_NS0_21MachineRepresentationE
	movzbl	-80(%rbp), %r8d
	movq	-88(%rbp), %r10
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L1576
	movzbl	23(%rax), %eax
	movq	-72(%rbp), %r9
	leaq	32(%rdx), %rcx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1578
	movq	32(%rdx), %rcx
	movl	8(%rcx), %eax
	addq	$16, %rcx
.L1578:
	testl	%eax, %eax
	jle	.L1579
	cmpq	$0, (%rcx)
	je	.L1576
.L1579:
	movq	8(%rdx), %rax
	movq	8(%r12), %rsi
	movq	%r10, -80(%rbp)
	movq	%r9, -72(%rbp)
	movq	%rax, -64(%rbp)
	cmpq	%rax, %rsi
	jne	.L1591
.L1581:
	movq	8(%r15), %rdi
	movq	%rdx, -72(%rbp)
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	-72(%rbp), %rdx
	movq	%rdx, %rax
	jmp	.L1572
	.p2align 4,,10
	.p2align 3
.L1591:
	leaq	-64(%rbp), %rdi
	movq	%rdx, -88(%rbp)
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	movq	-88(%rbp), %rdx
	testb	%al, %al
	jne	.L1581
	movq	-72(%rbp), %r9
	movq	-80(%rbp), %r10
	movzbl	16(%r9), %r8d
	.p2align 4,,10
	.p2align 3
.L1576:
	movq	16(%r15), %r9
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r10, %rdi
	movq	%r12, %rcx
	call	_ZNK2v88internal8compiler15LoadElimination13AbstractState10AddElementEPNS1_4NodeES5_S5_NS0_21MachineRepresentationEPNS0_4ZoneE
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler15LoadElimination11UpdateStateEPNS1_4NodeEPKNS2_13AbstractStateE
	jmp	.L1572
.L1590:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19427:
	.size	_ZN2v88internal8compiler15LoadElimination17ReduceLoadElementEPNS1_4NodeE, .-_ZN2v88internal8compiler15LoadElimination17ReduceLoadElementEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler15LoadElimination18ReduceStoreElementEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15LoadElimination18ReduceStoreElementEPNS1_4NodeE
	.type	_ZN2v88internal8compiler15LoadElimination18ReduceStoreElementEPNS1_4NodeE, @function
_ZN2v88internal8compiler15LoadElimination18ReduceStoreElementEPNS1_4NodeE:
.LFB19428:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rdi
	call	_ZN2v88internal8compiler15ElementAccessOfEPKNS1_8OperatorE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	24(%r15), %rcx
	movq	32(%r15), %rdx
	movq	%rax, %r9
	movl	20(%rax), %eax
	subq	%rcx, %rdx
	andl	$16777215, %eax
	sarq	$3, %rdx
	cmpq	%rax, %rdx
	jbe	.L1593
	movq	(%rcx,%rax,8), %r11
	testq	%r11, %r11
	je	.L1593
	movq	(%r11), %rdi
	movzbl	16(%rbx), %ecx
	testq	%rdi, %rdi
	je	.L1596
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r9, -80(%rbp)
	movq	%r11, -72(%rbp)
	movq	%rdi, -64(%rbp)
	call	_ZNK2v88internal8compiler15LoadElimination16AbstractElements6LookupEPNS1_4NodeES5_NS0_21MachineRepresentationE
	cmpq	%rax, -56(%rbp)
	movq	-64(%rbp), %rdi
	movq	-72(%rbp), %r11
	movq	-80(%rbp), %r9
	jne	.L1597
.L1603:
	addq	$40, %rsp
	movq	%r9, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1593:
	.cfi_restore_state
	addq	$40, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1597:
	.cfi_restore_state
	movq	16(%r15), %r8
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r11, -72(%rbp)
	movq	%r8, %rcx
	movq	%r8, -64(%rbp)
	call	_ZNK2v88internal8compiler15LoadElimination16AbstractElements4KillEPNS1_4NodeES5_PNS0_4ZoneE
	movq	-72(%rbp), %r11
	movq	-64(%rbp), %r8
	movq	%rax, %rdx
	cmpq	(%r11), %rax
	jne	.L1605
	movzbl	16(%rbx), %ecx
.L1604:
	cmpb	$11, %cl
	ja	.L1600
	cmpb	$5, %cl
	ja	.L1601
.L1602:
	addq	$40, %rsp
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%r11, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler15LoadElimination11UpdateStateEPNS1_4NodeEPKNS2_13AbstractStateE
	.p2align 4,,10
	.p2align 3
.L1600:
	.cfi_restore_state
	leal	-13(%rcx), %eax
	cmpb	$1, %al
	ja	.L1602
.L1601:
	movl	%ecx, %r8d
	movq	16(%r15), %r9
	movq	-56(%rbp), %rcx
	movq	%r11, %rdi
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	_ZNK2v88internal8compiler15LoadElimination13AbstractState10AddElementEPNS1_4NodeES5_S5_NS0_21MachineRepresentationEPNS0_4ZoneE
	movq	%rax, %r11
	jmp	.L1602
	.p2align 4,,10
	.p2align 3
.L1596:
	cmpq	$0, -56(%rbp)
	jne	.L1604
	jmp	.L1603
	.p2align 4,,10
	.p2align 3
.L1605:
	movq	16(%r8), %r9
	movq	24(%r8), %rax
	subq	%r9, %rax
	cmpq	$527, %rax
	jbe	.L1611
	leaq	528(%r9), %rax
	movq	%rax, 16(%r8)
.L1599:
	movq	%r11, %rsi
	movl	$66, %ecx
	movq	%r9, %rdi
	movq	%r9, %r11
	rep movsq
	movq	%rdx, (%r9)
	movzbl	16(%rbx), %ecx
	jmp	.L1604
.L1611:
	movl	$528, %esi
	movq	%r8, %rdi
	movq	%rdx, -72(%rbp)
	movq	%r11, -64(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-64(%rbp), %r11
	movq	-72(%rbp), %rdx
	movq	%rax, %r9
	jmp	.L1599
	.cfi_endproc
.LFE19428:
	.size	_ZN2v88internal8compiler15LoadElimination18ReduceStoreElementEPNS1_4NodeE, .-_ZN2v88internal8compiler15LoadElimination18ReduceStoreElementEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler15LoadElimination23ReduceStoreTypedElementEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15LoadElimination23ReduceStoreTypedElementEPNS1_4NodeE
	.type	_ZN2v88internal8compiler15LoadElimination23ReduceStoreTypedElementEPNS1_4NodeE, @function
_ZN2v88internal8compiler15LoadElimination23ReduceStoreTypedElementEPNS1_4NodeE:
.LFB19429:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	xorl	%esi, %esi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	24(%r12), %rcx
	movl	20(%rax), %edx
	movq	32(%r12), %rax
	subq	%rcx, %rax
	andl	$16777215, %edx
	sarq	$3, %rax
	cmpq	%rax, %rdx
	jnb	.L1616
	movq	(%rcx,%rdx,8), %rdx
	testq	%rdx, %rdx
	je	.L1616
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler15LoadElimination11UpdateStateEPNS1_4NodeEPKNS2_13AbstractStateE
	.p2align 4,,10
	.p2align 3
.L1616:
	.cfi_restore_state
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19429:
	.size	_ZN2v88internal8compiler15LoadElimination23ReduceStoreTypedElementEPNS1_4NodeE, .-_ZN2v88internal8compiler15LoadElimination23ReduceStoreTypedElementEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler15LoadElimination15ReduceOtherNodeEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15LoadElimination15ReduceOtherNodeEPNS1_4NodeE
	.type	_ZN2v88internal8compiler15LoadElimination15ReduceOtherNodeEPNS1_4NodeE, @function
_ZN2v88internal8compiler15LoadElimination15ReduceOtherNodeEPNS1_4NodeE:
.LFB19433:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	cmpl	$1, 24(%rax)
	je	.L1636
.L1619:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1636:
	cmpb	$1, 36(%rax)
	jne	.L1619
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	xorl	%esi, %esi
	movq	%r12, %rdi
	subq	$16, %rsp
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	24(%r13), %rsi
	xorl	%edx, %edx
	movl	20(%rax), %ecx
	movq	32(%r13), %rax
	subq	%rsi, %rax
	andl	$16777215, %ecx
	sarq	$3, %rax
	cmpq	%rax, %rcx
	jnb	.L1620
	movq	(%rsi,%rcx,8), %rdx
	testq	%rdx, %rdx
	je	.L1620
	movq	(%r12), %rax
	testb	$16, 18(%rax)
	jne	.L1621
	leaq	264(%rdx), %rax
	leaq	520(%rdx), %rcx
	jmp	.L1625
	.p2align 4,,10
	.p2align 3
.L1622:
	addq	$8, %rax
	cmpq	%rcx, %rax
	je	.L1637
.L1625:
	cmpq	$0, (%rax)
	je	.L1622
	movq	16(%r13), %rdi
	movq	16(%rdi), %rsi
	movq	24(%rdi), %rax
	subq	%rsi, %rax
	cmpq	$527, %rax
	jbe	.L1638
	leaq	528(%rsi), %rax
	movq	%rax, 16(%rdi)
.L1624:
	xorl	%eax, %eax
	leaq	8(%rsi), %rdi
	movl	$32, %ecx
	movq	$0, (%rsi)
	rep stosq
	leaq	264(%rsi), %rdi
	movl	$32, %ecx
	rep stosq
	movq	$0, 520(%rsi)
	movdqu	264(%rdx), %xmm0
	movups	%xmm0, 264(%rsi)
	movdqu	280(%rdx), %xmm1
	movups	%xmm1, 280(%rsi)
	movdqu	296(%rdx), %xmm2
	movups	%xmm2, 296(%rsi)
	movdqu	312(%rdx), %xmm3
	movups	%xmm3, 312(%rsi)
	movdqu	328(%rdx), %xmm4
	movups	%xmm4, 328(%rsi)
	movdqu	344(%rdx), %xmm5
	movups	%xmm5, 344(%rsi)
	movdqu	360(%rdx), %xmm6
	movups	%xmm6, 360(%rsi)
	movdqu	376(%rdx), %xmm7
	movups	%xmm7, 376(%rsi)
	movdqu	392(%rdx), %xmm0
	movups	%xmm0, 392(%rsi)
	movdqu	408(%rdx), %xmm1
	movups	%xmm1, 408(%rsi)
	movdqu	424(%rdx), %xmm2
	movups	%xmm2, 424(%rsi)
	movdqu	440(%rdx), %xmm3
	movups	%xmm3, 440(%rsi)
	movdqu	456(%rdx), %xmm4
	movups	%xmm4, 456(%rsi)
	movdqu	472(%rdx), %xmm5
	movups	%xmm5, 472(%rsi)
	movdqu	488(%rdx), %xmm6
	movups	%xmm6, 488(%rsi)
	movdqu	504(%rdx), %xmm7
	movq	%rsi, %rdx
	movups	%xmm7, 504(%rsi)
.L1621:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler15LoadElimination11UpdateStateEPNS1_4NodeEPKNS2_13AbstractStateE
	movq	%rax, %rdx
.L1620:
	addq	$16, %rsp
	movq	%rdx, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1637:
	.cfi_restore_state
	leaq	_ZN2v88internal8compiler15LoadElimination13AbstractState12empty_state_E(%rip), %rdx
	jmp	.L1621
.L1638:
	movl	$528, %esi
	movq	%rdx, -24(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-24(%rbp), %rdx
	movq	%rax, %rsi
	jmp	.L1624
	.cfi_endproc
.LFE19433:
	.size	_ZN2v88internal8compiler15LoadElimination15ReduceOtherNodeEPNS1_4NodeE, .-_ZN2v88internal8compiler15LoadElimination15ReduceOtherNodeEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler15LoadElimination11ReduceStartEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15LoadElimination11ReduceStartEPNS1_4NodeE
	.type	_ZN2v88internal8compiler15LoadElimination11ReduceStartEPNS1_4NodeE, @function
_ZN2v88internal8compiler15LoadElimination11ReduceStartEPNS1_4NodeE:
.LFB19432:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	_ZN2v88internal8compiler15LoadElimination13AbstractState12empty_state_E(%rip), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movl	20(%rsi), %edx
	movq	32(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rcx
	movq	%rsi, %rax
	andl	$16777215, %edx
	subq	%rcx, %rax
	leaq	0(,%rdx,8), %r14
	sarq	$3, %rax
	cmpq	%rax, %rdx
	jnb	.L1640
	addq	%rcx, %r14
	leaq	_ZN2v88internal8compiler15LoadElimination13AbstractState12empty_state_E(%rip), %r13
	movq	(%r14), %r15
	cmpq	%r13, %r15
	je	.L1665
	testq	%r15, %r15
	je	.L1642
	movq	_ZN2v88internal8compiler15LoadElimination13AbstractState12empty_state_E(%rip), %rsi
	movq	(%r15), %rdi
	testq	%rsi, %rsi
	je	.L1643
	testq	%rdi, %rdi
	je	.L1642
	call	_ZNK2v88internal8compiler15LoadElimination16AbstractElements6EqualsEPKS3_
	testb	%al, %al
	je	.L1642
.L1644:
	leaq	8+_ZN2v88internal8compiler15LoadElimination13AbstractState12empty_state_E(%rip), %r14
	leaq	8(%r15), %rcx
	leaq	256(%r14), %r8
.L1654:
	movq	(%r14), %rdx
	movq	(%rcx), %rax
	testq	%rdx, %rdx
	je	.L1645
	testq	%rax, %rax
	je	.L1647
	cmpq	%rax, %rdx
	je	.L1648
	movq	48(%rdx), %rdi
	cmpq	%rdi, 48(%rax)
	je	.L1686
.L1647:
	movq	32(%rbx), %rsi
	movq	24(%rbx), %rcx
	movl	20(%r12), %edx
	movq	%rsi, %rax
	subq	%rcx, %rax
	andl	$16777215, %edx
	sarq	$3, %rax
	leaq	0(,%rdx,8), %r14
	cmpq	%rax, %rdx
	jnb	.L1640
	.p2align 4,,10
	.p2align 3
.L1663:
	addq	%rcx, %r14
.L1642:
	movq	%r13, (%r14)
	jmp	.L1664
	.p2align 4,,10
	.p2align 3
.L1655:
	testq	%rax, %rax
	jne	.L1647
	.p2align 4,,10
	.p2align 3
.L1665:
	xorl	%r12d, %r12d
.L1664:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1687
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1645:
	.cfi_restore_state
	testq	%rax, %rax
	jne	.L1647
.L1648:
	addq	$8, %r14
	addq	$8, %rcx
	cmpq	%r14, %r8
	jne	.L1654
	leaq	264+_ZN2v88internal8compiler15LoadElimination13AbstractState12empty_state_E(%rip), %rsi
	leaq	264(%r15), %rdx
	leaq	-264(%rsi), %rdi
	call	_ZNK2v88internal8compiler15LoadElimination13AbstractState12FieldsEqualsERKSt5arrayIPKNS2_13AbstractFieldELm32EESA_
	testb	%al, %al
	je	.L1647
	movq	520+_ZN2v88internal8compiler15LoadElimination13AbstractState12empty_state_E(%rip), %rdx
	movq	520(%r15), %rax
	testq	%rdx, %rdx
	je	.L1655
	testq	%rax, %rax
	je	.L1647
	cmpq	%rdx, %rax
	je	.L1665
	movq	48(%rdx), %rcx
	cmpq	%rcx, 48(%rax)
	jne	.L1647
	movq	32(%rax), %r14
	movq	32(%rdx), %r15
	leaq	16(%rax), %rdx
	cmpq	%r14, %rdx
	je	.L1665
.L1661:
	movq	32(%r15), %rax
	cmpq	%rax, 32(%r14)
	jne	.L1647
	movq	40(%r14), %rcx
	movq	40(%r15), %rax
	cmpq	%rax, %rcx
	je	.L1658
	movq	%rcx, %rsi
	andl	$3, %esi
	cmpq	$2, %rsi
	jne	.L1647
	movq	%rax, %rsi
	andl	$3, %esi
	cmpq	$2, %rsi
	jne	.L1647
	leaq	-2(%rcx), %rsi
	leaq	-2(%rax), %rcx
	movq	8(%rsi), %rdi
	movq	16(%rsi), %rax
	movq	8(%rcx), %rsi
	movq	16(%rcx), %rcx
	subq	%rdi, %rax
	subq	%rsi, %rcx
	cmpq	%rcx, %rax
	jne	.L1647
	sarq	$3, %rax
	xorl	%ecx, %ecx
	jmp	.L1660
.L1688:
	movq	(%rsi,%rcx,8), %r11
	cmpq	%r11, (%rdi,%rcx,8)
	jne	.L1647
	addq	$1, %rcx
.L1660:
	cmpq	%rcx, %rax
	jne	.L1688
.L1658:
	movq	%r14, %rdi
	movq	%rdx, -72(%rbp)
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%r15, %rdi
	movq	%rax, %r14
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	-72(%rbp), %rdx
	movq	%rax, %r15
	cmpq	%r14, %rdx
	jne	.L1661
	jmp	.L1665
	.p2align 4,,10
	.p2align 3
.L1686:
	movq	32(%rax), %r9
	movq	32(%rdx), %r10
	leaq	16(%rax), %rdx
	cmpq	%r9, %rdx
	je	.L1648
.L1652:
	movq	32(%r10), %rax
	cmpq	%rax, 32(%r9)
	jne	.L1647
	movq	40(%r10), %rax
	cmpq	%rax, 40(%r9)
	jne	.L1647
	movzbl	48(%r10), %eax
	cmpb	%al, 48(%r9)
	jne	.L1647
	movq	56(%r10), %rax
	cmpq	%rax, 56(%r9)
	jne	.L1647
	leaq	64(%r10), %rsi
	leaq	64(%r9), %rdi
	movq	%rcx, -96(%rbp)
	movq	%rdx, -88(%rbp)
	movq	%r10, -80(%rbp)
	movq	%r9, -72(%rbp)
	call	_ZN2v88internal8compilereqERKNS1_14ConstFieldInfoES4_@PLT
	testb	%al, %al
	je	.L1647
	movq	-72(%rbp), %r9
	movq	%r9, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	-80(%rbp), %r10
	movq	%rax, -72(%rbp)
	movq	%r10, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	-72(%rbp), %r9
	movq	-88(%rbp), %rdx
	leaq	264+_ZN2v88internal8compiler15LoadElimination13AbstractState12empty_state_E(%rip), %r8
	movq	-96(%rbp), %rcx
	movq	%rax, %r10
	cmpq	%r9, %rdx
	jne	.L1652
	jmp	.L1648
	.p2align 4,,10
	.p2align 3
.L1640:
	addq	$1, %rdx
	movq	$0, -64(%rbp)
	cmpq	%rax, %rdx
	ja	.L1689
	jnb	.L1663
	leaq	(%rcx,%rdx,8), %rax
	cmpq	%rsi, %rax
	je	.L1663
	movq	%rax, 32(%rbx)
	jmp	.L1663
	.p2align 4,,10
	.p2align 3
.L1643:
	testq	%rdi, %rdi
	jne	.L1642
	jmp	.L1644
	.p2align 4,,10
	.p2align 3
.L1689:
	leaq	-64(%rbp), %rcx
	subq	%rax, %rdx
	leaq	16(%rbx), %rdi
	call	_ZNSt6vectorIPKN2v88internal8compiler15LoadElimination13AbstractStateENS1_13ZoneAllocatorIS6_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS6_S9_EEmRKS6_
	movq	24(%rbx), %rcx
	jmp	.L1663
.L1687:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19432:
	.size	_ZN2v88internal8compiler15LoadElimination11ReduceStartEPNS1_4NodeE, .-_ZN2v88internal8compiler15LoadElimination11ReduceStartEPNS1_4NodeE
	.section	.text._ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E,"axG",@progbits,_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	.type	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E, @function
_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E:
.LFB22791:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L1698
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L1692:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L1692
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1698:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE22791:
	.size	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E, .-_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	.section	.text._ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_,"axG",@progbits,_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_
	.type	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_, @function
_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_:
.LFB22793:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	16(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	24(%rdi), %r12
	testq	%r12, %r12
	je	.L1715
	movq	(%rsi), %rsi
	jmp	.L1703
	.p2align 4,,10
	.p2align 3
.L1722:
	movq	16(%r12), %rax
	movl	$1, %edx
	testq	%rax, %rax
	je	.L1704
.L1723:
	movq	%rax, %r12
.L1703:
	movq	32(%r12), %rcx
	cmpq	%rcx, %rsi
	jb	.L1722
	movq	24(%r12), %rax
	xorl	%edx, %edx
	testq	%rax, %rax
	jne	.L1723
.L1704:
	testb	%dl, %dl
	jne	.L1702
	cmpq	%rcx, %rsi
	jbe	.L1708
.L1713:
	movl	$1, %r8d
	cmpq	%r12, %r15
	jne	.L1724
.L1709:
	movq	(%rbx), %rdi
	movq	16(%rdi), %r13
	movq	24(%rdi), %rax
	subq	%r13, %rax
	cmpq	$39, %rax
	jbe	.L1725
	leaq	40(%r13), %rax
	movq	%rax, 16(%rdi)
.L1711:
	movq	(%r14), %rax
	movq	%r12, %rdx
	movq	%r15, %rcx
	movq	%r13, %rsi
	movl	%r8d, %edi
	movq	%rax, 32(%r13)
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 48(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	movl	$1, %edx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1715:
	.cfi_restore_state
	movq	%r15, %r12
.L1702:
	cmpq	%r12, 32(%rbx)
	je	.L1713
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	(%r14), %rcx
	cmpq	%rcx, 32(%rax)
	jb	.L1713
	movq	%rax, %r12
.L1708:
	addq	$24, %rsp
	movq	%r12, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1724:
	.cfi_restore_state
	xorl	%r8d, %r8d
	movq	32(%r12), %rax
	cmpq	%rax, (%r14)
	setb	%r8b
	jmp	.L1709
	.p2align 4,,10
	.p2align 3
.L1725:
	movl	$40, %esi
	movl	%r8d, -52(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-52(%rbp), %r8d
	movq	%rax, %r13
	jmp	.L1711
	.cfi_endproc
.LFE22793:
	.size	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_, .-_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_
	.section	.text._ZNSt8_Rb_treeIPN2v88internal8compiler4NodeESt4pairIKS4_NS2_15LoadElimination9FieldInfoEESt10_Select1stIS9_ESt4lessIS4_ENS1_13ZoneAllocatorIS9_EEE7_M_copyINSG_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS9_EPKSK_PSt18_Rb_tree_node_baseRT_,"axG",@progbits,_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeESt4pairIKS4_NS2_15LoadElimination9FieldInfoEESt10_Select1stIS9_ESt4lessIS4_ENS1_13ZoneAllocatorIS9_EEE7_M_copyINSG_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS9_EPKSK_PSt18_Rb_tree_node_baseRT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeESt4pairIKS4_NS2_15LoadElimination9FieldInfoEESt10_Select1stIS9_ESt4lessIS4_ENS1_13ZoneAllocatorIS9_EEE7_M_copyINSG_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS9_EPKSK_PSt18_Rb_tree_node_baseRT_
	.type	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeESt4pairIKS4_NS2_15LoadElimination9FieldInfoEESt10_Select1stIS9_ESt4lessIS4_ENS1_13ZoneAllocatorIS9_EEE7_M_copyINSG_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS9_EPKSK_PSt18_Rb_tree_node_baseRT_, @function
_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeESt4pairIKS4_NS2_15LoadElimination9FieldInfoEESt10_Select1stIS9_ESt4lessIS4_ENS1_13ZoneAllocatorIS9_EEE7_M_copyINSG_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS9_EPKSK_PSt18_Rb_tree_node_baseRT_:
.LFB23100:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	8(%rcx), %r15
	testq	%r15, %r15
	je	.L1727
	movq	8(%r15), %rax
	movq	%rax, 8(%rcx)
	testq	%rax, %rax
	je	.L1728
	cmpq	24(%rax), %r15
	je	.L1789
	movq	$0, 16(%rax)
.L1733:
	movdqu	32(%rbx), %xmm4
	movups	%xmm4, 32(%r15)
	movdqu	48(%rbx), %xmm5
	movups	%xmm5, 48(%r15)
	movq	64(%rbx), %rax
	movq	%rax, 64(%r15)
.L1751:
	movl	(%rbx), %eax
	movq	$0, 16(%r15)
	movq	$0, 24(%r15)
	movl	%eax, (%r15)
	movq	%rdx, 8(%r15)
	movq	24(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L1735
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeESt4pairIKS4_NS2_15LoadElimination9FieldInfoEESt10_Select1stIS9_ESt4lessIS4_ENS1_13ZoneAllocatorIS9_EEE7_M_copyINSG_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS9_EPKSK_PSt18_Rb_tree_node_baseRT_
	movq	%rax, 24(%r15)
.L1735:
	movq	16(%rbx), %r12
	testq	%r12, %r12
	je	.L1726
	movq	8(%r14), %rbx
	movq	%r15, %rdx
	testq	%rbx, %rbx
	je	.L1737
.L1791:
	movq	8(%rbx), %rax
	movq	%rax, 8(%r14)
	testq	%rax, %rax
	je	.L1738
	cmpq	24(%rax), %rbx
	je	.L1790
	movq	$0, 16(%rax)
.L1743:
	movdqu	32(%r12), %xmm0
	movups	%xmm0, 32(%rbx)
	movdqu	48(%r12), %xmm1
	movups	%xmm1, 48(%rbx)
	movq	64(%r12), %rax
	movq	%rax, 64(%rbx)
.L1748:
	movl	(%r12), %eax
	movq	$0, 16(%rbx)
	movq	$0, 24(%rbx)
	movl	%eax, (%rbx)
	movq	%rbx, 16(%rdx)
	movq	%rdx, 8(%rbx)
	movq	24(%r12), %rsi
	testq	%rsi, %rsi
	je	.L1745
	movq	%r14, %rcx
	movq	%rbx, %rdx
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeESt4pairIKS4_NS2_15LoadElimination9FieldInfoEESt10_Select1stIS9_ESt4lessIS4_ENS1_13ZoneAllocatorIS9_EEE7_M_copyINSG_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS9_EPKSK_PSt18_Rb_tree_node_baseRT_
	movq	%rax, 24(%rbx)
	movq	16(%r12), %r12
	testq	%r12, %r12
	je	.L1726
.L1746:
	movq	%rbx, %rdx
	movq	8(%r14), %rbx
	testq	%rbx, %rbx
	jne	.L1791
.L1737:
	movq	16(%r14), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	$71, %rax
	jbe	.L1792
	leaq	72(%rbx), %rax
	movq	%rax, 16(%rdi)
.L1744:
	movdqu	32(%r12), %xmm2
	movups	%xmm2, 32(%rbx)
	movdqu	48(%r12), %xmm3
	movups	%xmm3, 48(%rbx)
	movq	64(%r12), %rax
	movq	%rax, 64(%rbx)
	jmp	.L1748
	.p2align 4,,10
	.p2align 3
.L1745:
	movq	16(%r12), %r12
	testq	%r12, %r12
	jne	.L1746
.L1726:
	addq	$24, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1738:
	.cfi_restore_state
	movq	$0, (%r14)
	jmp	.L1743
	.p2align 4,,10
	.p2align 3
.L1790:
	movq	$0, 24(%rax)
	movq	8(%r14), %rax
	movq	16(%rax), %rcx
	testq	%rcx, %rcx
	je	.L1743
	movq	%rcx, 8(%r14)
	movq	24(%rcx), %rax
	testq	%rax, %rax
	je	.L1741
	.p2align 4,,10
	.p2align 3
.L1742:
	movq	%rax, 8(%r14)
	movq	%rax, %rcx
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L1742
.L1741:
	movq	16(%rcx), %rax
	testq	%rax, %rax
	je	.L1743
	movq	%rax, 8(%r14)
	jmp	.L1743
	.p2align 4,,10
	.p2align 3
.L1727:
	movq	16(%rcx), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %r15
	movq	24(%rdi), %rax
	subq	%r15, %rax
	cmpq	$71, %rax
	jbe	.L1793
	leaq	72(%r15), %rax
	movq	%rax, 16(%rdi)
.L1734:
	movdqu	32(%rbx), %xmm6
	movups	%xmm6, 32(%r15)
	movdqu	48(%rbx), %xmm7
	movups	%xmm7, 48(%r15)
	movq	64(%rbx), %rax
	movq	%rax, 64(%r15)
	jmp	.L1751
	.p2align 4,,10
	.p2align 3
.L1728:
	movq	$0, (%rcx)
	jmp	.L1733
	.p2align 4,,10
	.p2align 3
.L1789:
	movq	$0, 24(%rax)
	movq	8(%rcx), %rax
	movq	16(%rax), %rcx
	testq	%rcx, %rcx
	je	.L1733
	movq	%rcx, 8(%r14)
	movq	24(%rcx), %rax
	testq	%rax, %rax
	je	.L1731
	.p2align 4,,10
	.p2align 3
.L1732:
	movq	%rax, 8(%r14)
	movq	%rax, %rcx
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L1732
.L1731:
	movq	16(%rcx), %rax
	testq	%rax, %rax
	je	.L1733
	movq	%rax, 8(%r14)
	jmp	.L1733
	.p2align 4,,10
	.p2align 3
.L1792:
	movl	$72, %esi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %rbx
	jmp	.L1744
.L1793:
	movl	$72, %esi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %r15
	jmp	.L1734
	.cfi_endproc
.LFE23100:
	.size	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeESt4pairIKS4_NS2_15LoadElimination9FieldInfoEESt10_Select1stIS9_ESt4lessIS4_ENS1_13ZoneAllocatorIS9_EEE7_M_copyINSG_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS9_EPKSK_PSt18_Rb_tree_node_baseRT_, .-_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeESt4pairIKS4_NS2_15LoadElimination9FieldInfoEESt10_Select1stIS9_ESt4lessIS4_ENS1_13ZoneAllocatorIS9_EEE7_M_copyINSG_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS9_EPKSK_PSt18_Rb_tree_node_baseRT_
	.section	.text._ZNSt8_Rb_treeIPN2v88internal8compiler4NodeESt4pairIKS4_NS2_15LoadElimination9FieldInfoEESt10_Select1stIS9_ESt4lessIS4_ENS1_13ZoneAllocatorIS9_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS9_ERS6_,"axG",@progbits,_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeESt4pairIKS4_NS2_15LoadElimination9FieldInfoEESt10_Select1stIS9_ESt4lessIS4_ENS1_13ZoneAllocatorIS9_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS9_ERS6_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeESt4pairIKS4_NS2_15LoadElimination9FieldInfoEESt10_Select1stIS9_ESt4lessIS4_ENS1_13ZoneAllocatorIS9_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS9_ERS6_
	.type	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeESt4pairIKS4_NS2_15LoadElimination9FieldInfoEESt10_Select1stIS9_ESt4lessIS4_ENS1_13ZoneAllocatorIS9_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS9_ERS6_, @function
_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeESt4pairIKS4_NS2_15LoadElimination9FieldInfoEESt10_Select1stIS9_ESt4lessIS4_ENS1_13ZoneAllocatorIS9_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS9_ERS6_:
.LFB23109:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	16(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	cmpq	%rsi, %r15
	je	.L1844
	movq	(%rdx), %r14
	cmpq	%r14, 32(%rsi)
	jbe	.L1805
	movq	32(%rdi), %rbx
	movq	%rbx, %rax
	movq	%rbx, %rdx
	cmpq	%rsi, %rbx
	je	.L1797
	movq	%rsi, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, %rdx
	cmpq	%r14, 32(%rax)
	jnb	.L1807
	movl	$0, %ebx
	cmpq	$0, 24(%rax)
	movq	%rbx, %rax
	cmovne	%r12, %rdx
	cmovne	%r12, %rax
.L1797:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1805:
	.cfi_restore_state
	jnb	.L1816
	movq	40(%rdi), %rdx
	cmpq	%rsi, %rdx
	je	.L1843
	movq	%rsi, %rdi
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, %rdx
	cmpq	%r14, 32(%rax)
	jbe	.L1818
	movl	$0, %ebx
	cmpq	$0, 24(%r12)
	movq	%rbx, %rax
	cmovne	%rdx, %rax
	cmove	%r12, %rdx
	jmp	.L1797
	.p2align 4,,10
	.p2align 3
.L1844:
	cmpq	$0, 48(%rdi)
	je	.L1796
	movq	40(%rdi), %rdx
	movq	32(%rdx), %rax
	cmpq	%rax, (%r14)
	ja	.L1843
.L1796:
	movq	24(%r13), %rbx
	testq	%rbx, %rbx
	je	.L1827
	movq	(%r14), %rsi
	jmp	.L1799
	.p2align 4,,10
	.p2align 3
.L1845:
	movq	16(%rbx), %rax
	movl	$1, %ecx
	testq	%rax, %rax
	je	.L1800
.L1846:
	movq	%rax, %rbx
.L1799:
	movq	32(%rbx), %rdx
	cmpq	%rdx, %rsi
	jb	.L1845
	movq	24(%rbx), %rax
	xorl	%ecx, %ecx
	testq	%rax, %rax
	jne	.L1846
.L1800:
	movq	%rbx, %r12
	testb	%cl, %cl
	jne	.L1798
.L1803:
	xorl	%eax, %eax
	cmpq	%rdx, %rsi
	cmova	%rax, %rbx
	cmovbe	%rax, %r12
.L1804:
	addq	$8, %rsp
	movq	%rbx, %rax
	movq	%r12, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1816:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%rsi, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1843:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1807:
	.cfi_restore_state
	movq	24(%r13), %r12
	testq	%r12, %r12
	jne	.L1810
	jmp	.L1847
	.p2align 4,,10
	.p2align 3
.L1848:
	movq	16(%r12), %rax
	movl	$1, %esi
.L1813:
	testq	%rax, %rax
	je	.L1811
	movq	%rax, %r12
.L1810:
	movq	32(%r12), %rcx
	cmpq	%rcx, %r14
	jb	.L1848
	movq	24(%r12), %rax
	xorl	%esi, %esi
	jmp	.L1813
	.p2align 4,,10
	.p2align 3
.L1827:
	movq	%r12, %rbx
.L1798:
	cmpq	%rbx, 32(%r13)
	je	.L1829
	movq	%rbx, %rdi
	movq	%rbx, %r12
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	(%r14), %rsi
	movq	32(%rax), %rdx
	movq	%rax, %rbx
	jmp	.L1803
	.p2align 4,,10
	.p2align 3
.L1811:
	movq	%r12, %rdx
	testb	%sil, %sil
	jne	.L1809
.L1814:
	xorl	%eax, %eax
	cmpq	%rcx, %r14
	cmova	%rax, %r12
	cmovbe	%rax, %rdx
.L1815:
	movq	%r12, %rax
	jmp	.L1797
.L1847:
	movq	%r15, %r12
.L1809:
	cmpq	%r12, %rbx
	je	.L1833
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	%r12, %rdx
	movq	32(%rax), %rcx
	movq	%rax, %r12
	jmp	.L1814
	.p2align 4,,10
	.p2align 3
.L1818:
	movq	24(%r13), %rbx
	testq	%rbx, %rbx
	jne	.L1821
	jmp	.L1849
	.p2align 4,,10
	.p2align 3
.L1850:
	movq	16(%rbx), %rax
	movl	$1, %esi
.L1824:
	testq	%rax, %rax
	je	.L1822
	movq	%rax, %rbx
.L1821:
	movq	32(%rbx), %rcx
	cmpq	%rcx, %r14
	jb	.L1850
	movq	24(%rbx), %rax
	xorl	%esi, %esi
	jmp	.L1824
	.p2align 4,,10
	.p2align 3
.L1822:
	movq	%rbx, %rdx
	testb	%sil, %sil
	jne	.L1820
.L1825:
	xorl	%eax, %eax
	cmpq	%rcx, %r14
	cmova	%rax, %rbx
	cmovbe	%rax, %rdx
.L1826:
	movq	%rbx, %rax
	jmp	.L1797
	.p2align 4,,10
	.p2align 3
.L1829:
	movq	%rbx, %r12
	xorl	%ebx, %ebx
	jmp	.L1804
.L1849:
	movq	%r15, %rbx
.L1820:
	cmpq	%rbx, 32(%r13)
	je	.L1837
	movq	%rbx, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	%rbx, %rdx
	movq	32(%rax), %rcx
	movq	%rax, %rbx
	jmp	.L1825
	.p2align 4,,10
	.p2align 3
.L1833:
	movq	%r12, %rdx
	xorl	%r12d, %r12d
	jmp	.L1815
.L1837:
	movq	%rbx, %rdx
	xorl	%ebx, %ebx
	jmp	.L1826
	.cfi_endproc
.LFE23109:
	.size	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeESt4pairIKS4_NS2_15LoadElimination9FieldInfoEESt10_Select1stIS9_ESt4lessIS4_ENS1_13ZoneAllocatorIS9_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS9_ERS6_, .-_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeESt4pairIKS4_NS2_15LoadElimination9FieldInfoEESt10_Select1stIS9_ESt4lessIS4_ENS1_13ZoneAllocatorIS9_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS9_ERS6_
	.section	.text._ZNK2v88internal8compiler15LoadElimination13AbstractState8AddFieldEPNS1_4NodeENS2_10IndexRangeENS2_9FieldInfoEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler15LoadElimination13AbstractState8AddFieldEPNS1_4NodeENS2_10IndexRangeENS2_9FieldInfoEPNS0_4ZoneE
	.type	_ZNK2v88internal8compiler15LoadElimination13AbstractState8AddFieldEPNS1_4NodeENS2_10IndexRangeENS2_9FieldInfoEPNS0_4ZoneE, @function
_ZNK2v88internal8compiler15LoadElimination13AbstractState8AddFieldEPNS1_4NodeENS2_10IndexRangeENS2_9FieldInfoEPNS0_4ZoneE:
.LFB19393:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$136, %rsp
	movq	%rsi, -88(%rbp)
	movq	32(%rbp), %r8
	movq	40(%rbp), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rbp), %rax
	movq	%rax, -112(%rbp)
	movzbl	24(%rbp), %eax
	movb	%al, -89(%rbp)
	movq	16(%rcx), %rax
	movq	%rax, %rdi
	movq	%rax, -160(%rbp)
	movq	24(%rcx), %rax
	subq	%rdi, %rax
	cmpq	$527, %rax
	jbe	.L1920
	movq	%rdi, %rax
	addq	$528, %rax
	movq	%rax, 16(%rcx)
.L1853:
	movq	-160(%rbp), %rdi
	movl	$66, %ecx
	movq	%rbx, %rsi
	testq	%rdx, %rdx
	rep movsq
	movq	-160(%rbp), %rdi
	leaq	264(%rdi), %rax
	leaq	8(%rdi), %rcx
	cmovne	%rax, %rcx
	movq	%r14, %rax
	sarq	$32, %rax
	cmpl	%eax, %r14d
	je	.L1851
	subl	$1, %eax
	movq	%r8, %xmm4
	movq	%rdx, %xmm6
	movslq	%r14d, %rsi
	subl	%r14d, %eax
	movq	-88(%rbp), %xmm5
	punpcklqdq	%xmm6, %xmm4
	leaq	(%rcx,%rsi,8), %rbx
	leaq	1(%rsi,%rax), %rax
	movaps	%xmm4, -128(%rbp)
	leaq	(%rcx,%rax,8), %rax
	movhps	-112(%rbp), %xmm5
	movq	%rax, -104(%rbp)
	movaps	%xmm5, -176(%rbp)
	.p2align 4,,10
	.p2align 3
.L1890:
	movq	16(%r15), %r13
	movq	24(%r15), %rax
	movq	(%rbx), %r11
	subq	%r13, %rax
	testq	%r11, %r11
	je	.L1857
	cmpq	$55, %rax
	jbe	.L1921
	leaq	56(%r13), %rax
	movq	%rax, 16(%r15)
.L1859:
	leaq	16(%r13), %r12
	movq	%r15, 0(%r13)
	movl	$0, 16(%r13)
	movq	$0, 24(%r13)
	movq	%r12, 32(%r13)
	movq	%r12, 40(%r13)
	movq	$0, 48(%r13)
	cmpq	%r13, %r11
	je	.L1896
	movq	24(%r11), %rsi
	pxor	%xmm0, %xmm0
	movq	%r13, -64(%rbp)
	movq	%r11, -136(%rbp)
	movaps	%xmm0, -80(%rbp)
	testq	%rsi, %rsi
	je	.L1896
	movq	%r12, %rdx
	leaq	-80(%rbp), %rcx
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeESt4pairIKS4_NS2_15LoadElimination9FieldInfoEESt10_Select1stIS9_ESt4lessIS4_ENS1_13ZoneAllocatorIS9_EEE7_M_copyINSG_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS9_EPKSK_PSt18_Rb_tree_node_baseRT_
	movq	-136(%rbp), %r11
	movq	%rax, %r14
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L1862:
	movq	%rdx, %rcx
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L1862
	movq	%rcx, 32(%r13)
	movq	%r14, %rdx
	.p2align 4,,10
	.p2align 3
.L1863:
	movq	%rdx, %rcx
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L1863
	movq	%rcx, 40(%r13)
	movq	-80(%rbp), %rcx
	movq	48(%r11), %rdx
	movq	-64(%rbp), %rdi
	movq	%r14, 24(%r13)
	movq	%rdx, 48(%r13)
	testq	%rcx, %rcx
	je	.L1864
	movq	%r13, -144(%rbp)
	movq	%rbx, -136(%rbp)
	movq	%rdi, %rbx
	movq	%r12, -152(%rbp)
	movq	%rcx, %r12
.L1867:
	movq	24(%r12), %r13
	testq	%r13, %r13
	je	.L1865
.L1866:
	movq	24(%r13), %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeESt4pairIKS4_NS2_15LoadElimination9FieldInfoEESt10_Select1stIS9_ESt4lessIS4_ENS1_13ZoneAllocatorIS9_EEE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	movq	16(%r13), %r13
	testq	%r13, %r13
	jne	.L1866
.L1865:
	movq	16(%r12), %r12
	testq	%r12, %r12
	jne	.L1867
	movq	-136(%rbp), %rbx
	movq	-144(%rbp), %r13
	movq	-152(%rbp), %r12
.L1864:
	movq	%r12, %r8
	testq	%r14, %r14
	jne	.L1868
	jmp	.L1860
	.p2align 4,,10
	.p2align 3
.L1922:
	movq	%r14, %r8
	movq	16(%r14), %r14
	testq	%r14, %r14
	je	.L1869
.L1868:
	movq	-88(%rbp), %rax
	cmpq	%rax, 32(%r14)
	jnb	.L1922
	movq	24(%r14), %r14
	testq	%r14, %r14
	jne	.L1868
.L1869:
	cmpq	%r8, %r12
	je	.L1860
	movq	-88(%rbp), %rax
	cmpq	%rax, 32(%r8)
	jbe	.L1872
.L1860:
	movq	0(%r13), %rdi
	movq	16(%rdi), %r14
	movq	24(%rdi), %rax
	subq	%r14, %rax
	cmpq	$71, %rax
	jbe	.L1923
	leaq	72(%r14), %rax
	movq	%rax, 16(%rdi)
.L1874:
	movq	-88(%rbp), %rax
	movb	$0, 48(%r14)
	pxor	%xmm0, %xmm0
	movq	%r8, %rsi
	movq	$0, 40(%r14)
	leaq	32(%r14), %rdx
	movq	%r13, %rdi
	movq	%rax, 32(%r14)
	movups	%xmm0, 56(%r14)
	call	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeESt4pairIKS4_NS2_15LoadElimination9FieldInfoEESt10_Select1stIS9_ESt4lessIS4_ENS1_13ZoneAllocatorIS9_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS9_ERS6_
	movq	%rax, %r8
	testq	%rdx, %rdx
	je	.L1872
	cmpq	%rdx, %r12
	je	.L1898
	testq	%rax, %rax
	je	.L1924
.L1898:
	movl	$1, %edi
.L1876:
	movq	%r12, %rcx
	movq	%r14, %rsi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 48(%r13)
	movq	%r14, %r8
.L1872:
	movq	-112(%rbp), %rax
	movdqa	-128(%rbp), %xmm1
	movq	%rax, 40(%r8)
	movzbl	-89(%rbp), %eax
	movups	%xmm1, 56(%r8)
	movb	%al, 48(%r8)
	movq	%r13, (%rbx)
.L1877:
	addq	$8, %rbx
	cmpq	-104(%rbp), %rbx
	jne	.L1890
.L1851:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1925
	movq	-160(%rbp), %rax
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1896:
	.cfi_restore_state
	movq	%r12, %r8
	jmp	.L1860
	.p2align 4,,10
	.p2align 3
.L1857:
	cmpq	$55, %rax
	jbe	.L1926
	leaq	56(%r13), %rax
	movq	%rax, 16(%r15)
.L1879:
	leaq	16(%r13), %rcx
	movq	%r15, 0(%r13)
	movl	$0, 16(%r13)
	movq	$0, 24(%r13)
	movq	%rcx, 32(%r13)
	movq	%rcx, 40(%r13)
	movq	$0, 48(%r13)
	movq	16(%r15), %rsi
	movq	24(%r15), %rax
	subq	%rsi, %rax
	cmpq	$71, %rax
	jbe	.L1927
	leaq	72(%rsi), %rax
	movq	%rax, 16(%r15)
.L1881:
	movdqa	-176(%rbp), %xmm2
	movzbl	-89(%rbp), %eax
	movdqa	-128(%rbp), %xmm3
	movq	-88(%rbp), %r8
	movb	%al, 48(%rsi)
	movups	%xmm2, 32(%rsi)
	movups	%xmm3, 56(%rsi)
	movq	24(%r13), %rdx
	testq	%rdx, %rdx
	jne	.L1883
	jmp	.L1928
	.p2align 4,,10
	.p2align 3
.L1929:
	movq	16(%rdx), %rax
	movl	$1, %edi
	testq	%rax, %rax
	je	.L1884
.L1930:
	movq	%rax, %rdx
.L1883:
	movq	32(%rdx), %r10
	cmpq	%r10, %r8
	jb	.L1929
	movq	24(%rdx), %rax
	xorl	%edi, %edi
	testq	%rax, %rax
	jne	.L1930
.L1884:
	testb	%dil, %dil
	jne	.L1931
	cmpq	%r10, -88(%rbp)
	jbe	.L1893
.L1892:
	movl	$1, %edi
	cmpq	%rdx, %rcx
	jne	.L1932
.L1889:
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 48(%r13)
.L1893:
	movq	%r13, (%rbx)
	jmp	.L1877
	.p2align 4,,10
	.p2align 3
.L1931:
	cmpq	%rdx, 32(%r13)
	je	.L1892
.L1894:
	movq	%rdx, %rdi
	movq	%rsi, -152(%rbp)
	movq	%rcx, -144(%rbp)
	movq	%rdx, -136(%rbp)
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	-88(%rbp), %rsi
	movq	-136(%rbp), %rdx
	cmpq	32(%rax), %rsi
	movq	-144(%rbp), %rcx
	movq	-152(%rbp), %rsi
	jbe	.L1893
	movl	$1, %edi
	cmpq	%rdx, %rcx
	je	.L1889
.L1932:
	xorl	%edi, %edi
	movq	-88(%rbp), %rax
	cmpq	32(%rdx), %rax
	setb	%dil
	jmp	.L1889
	.p2align 4,,10
	.p2align 3
.L1921:
	movl	$56, %esi
	movq	%r15, %rdi
	movq	%r11, -136(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-136(%rbp), %r11
	movq	%rax, %r13
	jmp	.L1859
	.p2align 4,,10
	.p2align 3
.L1924:
	xorl	%edi, %edi
	movq	32(%rdx), %rax
	cmpq	%rax, 32(%r14)
	setb	%dil
	jmp	.L1876
	.p2align 4,,10
	.p2align 3
.L1923:
	movl	$72, %esi
	movq	%r8, -136(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-136(%rbp), %r8
	movq	%rax, %r14
	jmp	.L1874
	.p2align 4,,10
	.p2align 3
.L1928:
	movq	%rcx, %rdx
	cmpq	32(%r13), %rcx
	jne	.L1894
	movl	$1, %edi
	jmp	.L1889
	.p2align 4,,10
	.p2align 3
.L1927:
	movl	$72, %esi
	movq	%r15, %rdi
	movq	%rcx, -136(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-136(%rbp), %rcx
	movq	%rax, %rsi
	jmp	.L1881
	.p2align 4,,10
	.p2align 3
.L1926:
	movl	$56, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r13
	jmp	.L1879
	.p2align 4,,10
	.p2align 3
.L1920:
	movl	$528, %esi
	movq	%rcx, %rdi
	movq	%rdx, -128(%rbp)
	movq	%r8, -104(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-104(%rbp), %r8
	movq	-128(%rbp), %rdx
	movq	%rax, -160(%rbp)
	jmp	.L1853
.L1925:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19393:
	.size	_ZNK2v88internal8compiler15LoadElimination13AbstractState8AddFieldEPNS1_4NodeENS2_10IndexRangeENS2_9FieldInfoEPNS0_4ZoneE, .-_ZNK2v88internal8compiler15LoadElimination13AbstractState8AddFieldEPNS1_4NodeENS2_10IndexRangeENS2_9FieldInfoEPNS0_4ZoneE
	.section	.text._ZNSt8_Rb_treeIPN2v88internal8compiler4NodeESt4pairIKS4_NS1_13ZoneHandleSetINS1_3MapEEEESt10_Select1stISA_ESt4lessIS4_ENS1_13ZoneAllocatorISA_EEE7_M_copyINSH_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeISA_EPKSL_PSt18_Rb_tree_node_baseRT_,"axG",@progbits,_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeESt4pairIKS4_NS1_13ZoneHandleSetINS1_3MapEEEESt10_Select1stISA_ESt4lessIS4_ENS1_13ZoneAllocatorISA_EEE7_M_copyINSH_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeISA_EPKSL_PSt18_Rb_tree_node_baseRT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeESt4pairIKS4_NS1_13ZoneHandleSetINS1_3MapEEEESt10_Select1stISA_ESt4lessIS4_ENS1_13ZoneAllocatorISA_EEE7_M_copyINSH_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeISA_EPKSL_PSt18_Rb_tree_node_baseRT_
	.type	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeESt4pairIKS4_NS1_13ZoneHandleSetINS1_3MapEEEESt10_Select1stISA_ESt4lessIS4_ENS1_13ZoneAllocatorISA_EEE7_M_copyINSH_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeISA_EPKSL_PSt18_Rb_tree_node_baseRT_, @function
_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeESt4pairIKS4_NS1_13ZoneHandleSetINS1_3MapEEEESt10_Select1stISA_ESt4lessIS4_ENS1_13ZoneAllocatorISA_EEE7_M_copyINSH_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeISA_EPKSL_PSt18_Rb_tree_node_baseRT_:
.LFB23452:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	8(%rcx), %r15
	testq	%r15, %r15
	je	.L1934
	movq	8(%r15), %rax
	movq	%rax, 8(%rcx)
	testq	%rax, %rax
	je	.L1935
	cmpq	24(%rax), %r15
	je	.L1996
	movq	$0, 16(%rax)
.L1941:
	movq	40(%rbx), %rax
	movq	32(%rbx), %rcx
	movq	%rax, 40(%r15)
	movq	%rcx, 32(%r15)
	movl	(%rbx), %eax
	movq	$0, 16(%r15)
	movl	%eax, (%r15)
	movq	$0, 24(%r15)
	movq	%rdx, 8(%r15)
	movq	24(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L1942
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeESt4pairIKS4_NS1_13ZoneHandleSetINS1_3MapEEEESt10_Select1stISA_ESt4lessIS4_ENS1_13ZoneAllocatorISA_EEE7_M_copyINSH_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeISA_EPKSL_PSt18_Rb_tree_node_baseRT_
	movq	%rax, 24(%r15)
.L1942:
	movq	16(%rbx), %r12
	testq	%r12, %r12
	je	.L1933
	movq	8(%r14), %rbx
	movq	%r15, %rdx
	testq	%rbx, %rbx
	je	.L1944
.L1998:
	movq	8(%rbx), %rax
	movq	%rax, 8(%r14)
	testq	%rax, %rax
	je	.L1945
	cmpq	24(%rax), %rbx
	je	.L1997
	movq	$0, 16(%rax)
.L1951:
	movq	40(%r12), %rax
	movq	32(%r12), %rcx
	movq	%rax, 40(%rbx)
	movq	%rcx, 32(%rbx)
	movl	(%r12), %eax
	movq	$0, 16(%rbx)
	movl	%eax, (%rbx)
	movq	$0, 24(%rbx)
	movq	%rbx, 16(%rdx)
	movq	%rdx, 8(%rbx)
	movq	24(%r12), %rsi
	testq	%rsi, %rsi
	je	.L1952
	movq	%r14, %rcx
	movq	%rbx, %rdx
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeESt4pairIKS4_NS1_13ZoneHandleSetINS1_3MapEEEESt10_Select1stISA_ESt4lessIS4_ENS1_13ZoneAllocatorISA_EEE7_M_copyINSH_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeISA_EPKSL_PSt18_Rb_tree_node_baseRT_
	movq	%rax, 24(%rbx)
	movq	16(%r12), %r12
	testq	%r12, %r12
	je	.L1933
.L1953:
	movq	%rbx, %rdx
	movq	8(%r14), %rbx
	testq	%rbx, %rbx
	jne	.L1998
.L1944:
	movq	16(%r14), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	$47, %rax
	jbe	.L1999
	leaq	48(%rbx), %rax
	movq	%rax, 16(%rdi)
	jmp	.L1951
	.p2align 4,,10
	.p2align 3
.L1952:
	movq	16(%r12), %r12
	testq	%r12, %r12
	jne	.L1953
.L1933:
	addq	$24, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1945:
	.cfi_restore_state
	movq	$0, (%r14)
	jmp	.L1951
	.p2align 4,,10
	.p2align 3
.L1997:
	movq	$0, 24(%rax)
	movq	8(%r14), %rax
	movq	16(%rax), %rcx
	testq	%rcx, %rcx
	je	.L1951
	movq	%rcx, 8(%r14)
	movq	24(%rcx), %rax
	testq	%rax, %rax
	je	.L1948
	.p2align 4,,10
	.p2align 3
.L1949:
	movq	%rax, 8(%r14)
	movq	%rax, %rcx
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L1949
.L1948:
	movq	16(%rcx), %rax
	testq	%rax, %rax
	je	.L1951
	movq	%rax, 8(%r14)
	jmp	.L1951
	.p2align 4,,10
	.p2align 3
.L1934:
	movq	16(%rcx), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %r15
	movq	24(%rdi), %rax
	subq	%r15, %rax
	cmpq	$47, %rax
	jbe	.L2000
	leaq	48(%r15), %rax
	movq	%rax, 16(%rdi)
	jmp	.L1941
	.p2align 4,,10
	.p2align 3
.L1935:
	movq	$0, (%rcx)
	jmp	.L1941
	.p2align 4,,10
	.p2align 3
.L1996:
	movq	$0, 24(%rax)
	movq	8(%rcx), %rax
	movq	16(%rax), %rcx
	testq	%rcx, %rcx
	je	.L1941
	movq	%rcx, 8(%r14)
	movq	24(%rcx), %rax
	testq	%rax, %rax
	je	.L1938
	.p2align 4,,10
	.p2align 3
.L1939:
	movq	%rax, 8(%r14)
	movq	%rax, %rcx
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L1939
.L1938:
	movq	16(%rcx), %rax
	testq	%rax, %rax
	je	.L1941
	movq	%rax, 8(%r14)
	jmp	.L1941
	.p2align 4,,10
	.p2align 3
.L1999:
	movl	$48, %esi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %rbx
	jmp	.L1951
.L2000:
	movl	$48, %esi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %r15
	jmp	.L1941
	.cfi_endproc
.LFE23452:
	.size	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeESt4pairIKS4_NS1_13ZoneHandleSetINS1_3MapEEEESt10_Select1stISA_ESt4lessIS4_ENS1_13ZoneAllocatorISA_EEE7_M_copyINSH_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeISA_EPKSL_PSt18_Rb_tree_node_baseRT_, .-_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeESt4pairIKS4_NS1_13ZoneHandleSetINS1_3MapEEEESt10_Select1stISA_ESt4lessIS4_ENS1_13ZoneAllocatorISA_EEE7_M_copyINSH_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeISA_EPKSL_PSt18_Rb_tree_node_baseRT_
	.section	.text._ZNSt8_Rb_treeIPN2v88internal8compiler4NodeESt4pairIKS4_NS1_13ZoneHandleSetINS1_3MapEEEESt10_Select1stISA_ESt4lessIS4_ENS1_13ZoneAllocatorISA_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorISA_ERS6_,"axG",@progbits,_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeESt4pairIKS4_NS1_13ZoneHandleSetINS1_3MapEEEESt10_Select1stISA_ESt4lessIS4_ENS1_13ZoneAllocatorISA_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorISA_ERS6_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeESt4pairIKS4_NS1_13ZoneHandleSetINS1_3MapEEEESt10_Select1stISA_ESt4lessIS4_ENS1_13ZoneAllocatorISA_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorISA_ERS6_
	.type	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeESt4pairIKS4_NS1_13ZoneHandleSetINS1_3MapEEEESt10_Select1stISA_ESt4lessIS4_ENS1_13ZoneAllocatorISA_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorISA_ERS6_, @function
_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeESt4pairIKS4_NS1_13ZoneHandleSetINS1_3MapEEEESt10_Select1stISA_ESt4lessIS4_ENS1_13ZoneAllocatorISA_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorISA_ERS6_:
.LFB23461:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	16(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	cmpq	%rsi, %r15
	je	.L2051
	movq	(%rdx), %r14
	cmpq	%r14, 32(%rsi)
	jbe	.L2012
	movq	32(%rdi), %rbx
	movq	%rbx, %rax
	movq	%rbx, %rdx
	cmpq	%rsi, %rbx
	je	.L2004
	movq	%rsi, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, %rdx
	cmpq	%r14, 32(%rax)
	jnb	.L2014
	movl	$0, %ebx
	cmpq	$0, 24(%rax)
	movq	%rbx, %rax
	cmovne	%r12, %rdx
	cmovne	%r12, %rax
.L2004:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2012:
	.cfi_restore_state
	jnb	.L2023
	movq	40(%rdi), %rdx
	cmpq	%rsi, %rdx
	je	.L2050
	movq	%rsi, %rdi
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, %rdx
	cmpq	%r14, 32(%rax)
	jbe	.L2025
	movl	$0, %ebx
	cmpq	$0, 24(%r12)
	movq	%rbx, %rax
	cmovne	%rdx, %rax
	cmove	%r12, %rdx
	jmp	.L2004
	.p2align 4,,10
	.p2align 3
.L2051:
	cmpq	$0, 48(%rdi)
	je	.L2003
	movq	40(%rdi), %rdx
	movq	32(%rdx), %rax
	cmpq	%rax, (%r14)
	ja	.L2050
.L2003:
	movq	24(%r13), %rbx
	testq	%rbx, %rbx
	je	.L2034
	movq	(%r14), %rsi
	jmp	.L2006
	.p2align 4,,10
	.p2align 3
.L2052:
	movq	16(%rbx), %rax
	movl	$1, %ecx
	testq	%rax, %rax
	je	.L2007
.L2053:
	movq	%rax, %rbx
.L2006:
	movq	32(%rbx), %rdx
	cmpq	%rdx, %rsi
	jb	.L2052
	movq	24(%rbx), %rax
	xorl	%ecx, %ecx
	testq	%rax, %rax
	jne	.L2053
.L2007:
	movq	%rbx, %r12
	testb	%cl, %cl
	jne	.L2005
.L2010:
	xorl	%eax, %eax
	cmpq	%rdx, %rsi
	cmova	%rax, %rbx
	cmovbe	%rax, %r12
.L2011:
	addq	$8, %rsp
	movq	%rbx, %rax
	movq	%r12, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2023:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%rsi, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2050:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2014:
	.cfi_restore_state
	movq	24(%r13), %r12
	testq	%r12, %r12
	jne	.L2017
	jmp	.L2054
	.p2align 4,,10
	.p2align 3
.L2055:
	movq	16(%r12), %rax
	movl	$1, %esi
.L2020:
	testq	%rax, %rax
	je	.L2018
	movq	%rax, %r12
.L2017:
	movq	32(%r12), %rcx
	cmpq	%rcx, %r14
	jb	.L2055
	movq	24(%r12), %rax
	xorl	%esi, %esi
	jmp	.L2020
	.p2align 4,,10
	.p2align 3
.L2034:
	movq	%r12, %rbx
.L2005:
	cmpq	%rbx, 32(%r13)
	je	.L2036
	movq	%rbx, %rdi
	movq	%rbx, %r12
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	(%r14), %rsi
	movq	32(%rax), %rdx
	movq	%rax, %rbx
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2018:
	movq	%r12, %rdx
	testb	%sil, %sil
	jne	.L2016
.L2021:
	xorl	%eax, %eax
	cmpq	%rcx, %r14
	cmova	%rax, %r12
	cmovbe	%rax, %rdx
.L2022:
	movq	%r12, %rax
	jmp	.L2004
.L2054:
	movq	%r15, %r12
.L2016:
	cmpq	%r12, %rbx
	je	.L2040
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	%r12, %rdx
	movq	32(%rax), %rcx
	movq	%rax, %r12
	jmp	.L2021
	.p2align 4,,10
	.p2align 3
.L2025:
	movq	24(%r13), %rbx
	testq	%rbx, %rbx
	jne	.L2028
	jmp	.L2056
	.p2align 4,,10
	.p2align 3
.L2057:
	movq	16(%rbx), %rax
	movl	$1, %esi
.L2031:
	testq	%rax, %rax
	je	.L2029
	movq	%rax, %rbx
.L2028:
	movq	32(%rbx), %rcx
	cmpq	%rcx, %r14
	jb	.L2057
	movq	24(%rbx), %rax
	xorl	%esi, %esi
	jmp	.L2031
	.p2align 4,,10
	.p2align 3
.L2029:
	movq	%rbx, %rdx
	testb	%sil, %sil
	jne	.L2027
.L2032:
	xorl	%eax, %eax
	cmpq	%rcx, %r14
	cmova	%rax, %rbx
	cmovbe	%rax, %rdx
.L2033:
	movq	%rbx, %rax
	jmp	.L2004
	.p2align 4,,10
	.p2align 3
.L2036:
	movq	%rbx, %r12
	xorl	%ebx, %ebx
	jmp	.L2011
.L2056:
	movq	%r15, %rbx
.L2027:
	cmpq	%rbx, 32(%r13)
	je	.L2044
	movq	%rbx, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	%rbx, %rdx
	movq	32(%rax), %rcx
	movq	%rax, %rbx
	jmp	.L2032
	.p2align 4,,10
	.p2align 3
.L2040:
	movq	%r12, %rdx
	xorl	%r12d, %r12d
	jmp	.L2022
.L2044:
	movq	%rbx, %rdx
	xorl	%ebx, %ebx
	jmp	.L2033
	.cfi_endproc
.LFE23461:
	.size	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeESt4pairIKS4_NS1_13ZoneHandleSetINS1_3MapEEEESt10_Select1stISA_ESt4lessIS4_ENS1_13ZoneAllocatorISA_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorISA_ERS6_, .-_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeESt4pairIKS4_NS1_13ZoneHandleSetINS1_3MapEEEESt10_Select1stISA_ESt4lessIS4_ENS1_13ZoneAllocatorISA_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorISA_ERS6_
	.section	.text._ZNK2v88internal8compiler15LoadElimination12AbstractMaps6ExtendEPNS1_4NodeENS0_13ZoneHandleSetINS0_3MapEEEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler15LoadElimination12AbstractMaps6ExtendEPNS1_4NodeENS0_13ZoneHandleSetINS0_3MapEEEPNS0_4ZoneE
	.type	_ZNK2v88internal8compiler15LoadElimination12AbstractMaps6ExtendEPNS1_4NodeENS0_13ZoneHandleSetINS0_3MapEEEPNS0_4ZoneE, @function
_ZNK2v88internal8compiler15LoadElimination12AbstractMaps6ExtendEPNS1_4NodeENS0_13ZoneHandleSetINS0_3MapEEEPNS0_4ZoneE:
.LFB19369:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$56, %rsp
	movq	16(%rcx), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rcx), %rax
	subq	%r12, %rax
	cmpq	$55, %rax
	jbe	.L2098
	leaq	56(%r12), %rax
	movq	%rax, 16(%rcx)
.L2060:
	leaq	16(%r12), %r14
	movq	%rcx, (%r12)
	movl	$0, 16(%r12)
	movq	$0, 24(%r12)
	movq	%r14, 32(%r12)
	movq	%r14, 40(%r12)
	movq	$0, 48(%r12)
	cmpq	%r12, %r8
	je	.L2061
	movq	24(%r8), %rsi
	pxor	%xmm0, %xmm0
	movq	%r12, -64(%rbp)
	movq	%r8, -88(%rbp)
	movaps	%xmm0, -80(%rbp)
	testq	%rsi, %rsi
	je	.L2061
	movq	%r14, %rdx
	leaq	-80(%rbp), %rcx
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeESt4pairIKS4_NS1_13ZoneHandleSetINS1_3MapEEEESt10_Select1stISA_ESt4lessIS4_ENS1_13ZoneAllocatorISA_EEE7_M_copyINSH_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeISA_EPKSL_PSt18_Rb_tree_node_baseRT_
	movq	-88(%rbp), %r8
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L2063:
	movq	%rdx, %rcx
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L2063
	movq	%rcx, 32(%r12)
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L2064:
	movq	%rdx, %rcx
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L2064
	movq	%rcx, 40(%r12)
	movq	48(%r8), %rdx
	movq	%rax, 24(%r12)
	movq	-80(%rbp), %rax
	movq	%rdx, 48(%r12)
	movq	-64(%rbp), %rdx
	testq	%rax, %rax
	je	.L2061
.L2068:
	movq	24(%rax), %r13
	testq	%r13, %r13
	je	.L2066
.L2067:
	movq	24(%r13), %rsi
	movq	%rdx, %rdi
	movq	%rax, -96(%rbp)
	movq	%rdx, -88(%rbp)
	call	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeESt4pairIKS4_NS1_13ZoneHandleSetINS1_3MapEEEESt10_Select1stISA_ESt4lessIS4_ENS1_13ZoneAllocatorISA_EEE8_M_eraseEPSt13_Rb_tree_nodeISA_E
	movq	16(%r13), %r13
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rax
	testq	%r13, %r13
	jne	.L2067
.L2066:
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L2068
.L2061:
	movq	%r15, %rdi
	movq	%r14, %r13
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	movq	%rax, %r15
	movq	24(%r12), %rax
	testq	%rax, %rax
	jne	.L2070
	jmp	.L2069
	.p2align 4,,10
	.p2align 3
.L2099:
	movq	%rax, %r13
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L2071
.L2070:
	cmpq	%r15, 32(%rax)
	jnb	.L2099
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L2070
.L2071:
	cmpq	%r13, %r14
	je	.L2069
	cmpq	%r15, 32(%r13)
	jbe	.L2074
.L2069:
	movq	(%r12), %rdi
	movq	16(%rdi), %r8
	movq	24(%rdi), %rax
	subq	%r8, %rax
	cmpq	$47, %rax
	jbe	.L2100
	leaq	48(%r8), %rax
	movq	%rax, 16(%rdi)
.L2076:
	movq	%r15, 32(%r8)
	movq	%r13, %rsi
	leaq	32(%r8), %rdx
	movq	%r12, %rdi
	movq	$1, 40(%r8)
	movq	%r8, -88(%rbp)
	call	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeESt4pairIKS4_NS1_13ZoneHandleSetINS1_3MapEEEESt10_Select1stISA_ESt4lessIS4_ENS1_13ZoneAllocatorISA_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorISA_ERS6_
	movq	%rax, %r13
	testq	%rdx, %rdx
	je	.L2074
	testq	%rax, %rax
	movq	-88(%rbp), %r8
	jne	.L2081
	cmpq	%rdx, %r14
	je	.L2081
	xorl	%edi, %edi
	movq	32(%rdx), %rax
	cmpq	%rax, 32(%r8)
	setb	%dil
	jmp	.L2078
	.p2align 4,,10
	.p2align 3
.L2081:
	movl	$1, %edi
.L2078:
	movq	%r8, %rsi
	movq	%r14, %rcx
	movq	%r8, -88(%rbp)
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 48(%r12)
	movq	-88(%rbp), %r8
	movq	%r8, %r13
.L2074:
	movq	%rbx, 40(%r13)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2101
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2098:
	.cfi_restore_state
	movq	%rdi, -96(%rbp)
	movl	$56, %esi
	movq	%rcx, %rdi
	movq	%rcx, -88(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-88(%rbp), %rcx
	movq	-96(%rbp), %r8
	movq	%rax, %r12
	jmp	.L2060
	.p2align 4,,10
	.p2align 3
.L2100:
	movl	$48, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r8
	jmp	.L2076
.L2101:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19369:
	.size	_ZNK2v88internal8compiler15LoadElimination12AbstractMaps6ExtendEPNS1_4NodeENS0_13ZoneHandleSetINS0_3MapEEEPNS0_4ZoneE, .-_ZNK2v88internal8compiler15LoadElimination12AbstractMaps6ExtendEPNS1_4NodeENS0_13ZoneHandleSetINS0_3MapEEEPNS0_4ZoneE
	.section	.text._ZNK2v88internal8compiler15LoadElimination13AbstractState7SetMapsEPNS1_4NodeENS0_13ZoneHandleSetINS0_3MapEEEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler15LoadElimination13AbstractState7SetMapsEPNS1_4NodeENS0_13ZoneHandleSetINS0_3MapEEEPNS0_4ZoneE
	.type	_ZNK2v88internal8compiler15LoadElimination13AbstractState7SetMapsEPNS1_4NodeENS0_13ZoneHandleSetINS0_3MapEEEPNS0_4ZoneE, @function
_ZNK2v88internal8compiler15LoadElimination13AbstractState7SetMapsEPNS1_4NodeENS0_13ZoneHandleSetINS0_3MapEEEPNS0_4ZoneE:
.LFB19387:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	16(%rcx), %r12
	movq	24(%rcx), %rax
	subq	%r12, %rax
	cmpq	$527, %rax
	jbe	.L2110
	leaq	528(%r12), %rax
	movq	%rax, 16(%rcx)
.L2104:
	movl	$66, %ecx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	rep movsq
	movq	520(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2105
	movq	%r13, %rcx
	movq	%r14, %rsi
	call	_ZNK2v88internal8compiler15LoadElimination12AbstractMaps6ExtendEPNS1_4NodeENS0_13ZoneHandleSetINS0_3MapEEEPNS0_4ZoneE
	movq	%rax, 520(%r12)
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2105:
	.cfi_restore_state
	movq	16(%r13), %rbx
	movq	24(%r13), %rax
	subq	%rbx, %rax
	cmpq	$55, %rax
	jbe	.L2111
	leaq	56(%rbx), %rax
	movq	%rax, 16(%r13)
.L2108:
	movq	%r13, %rcx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler15LoadElimination12AbstractMapsC1EPNS1_4NodeENS0_13ZoneHandleSetINS0_3MapEEEPNS0_4ZoneE
	movq	%rbx, 520(%r12)
	movq	%r12, %rax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2110:
	.cfi_restore_state
	movl	$528, %esi
	movq	%rcx, %rdi
	movq	%rdx, -40(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-40(%rbp), %rdx
	movq	%rax, %r12
	jmp	.L2104
	.p2align 4,,10
	.p2align 3
.L2111:
	movl	$56, %esi
	movq	%r13, %rdi
	movq	%rdx, -40(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-40(%rbp), %rdx
	movq	%rax, %rbx
	jmp	.L2108
	.cfi_endproc
.LFE19387:
	.size	_ZNK2v88internal8compiler15LoadElimination13AbstractState7SetMapsEPNS1_4NodeENS0_13ZoneHandleSetINS0_3MapEEEPNS0_4ZoneE, .-_ZNK2v88internal8compiler15LoadElimination13AbstractState7SetMapsEPNS1_4NodeENS0_13ZoneHandleSetINS0_3MapEEEPNS0_4ZoneE
	.section	.text._ZN2v88internal8compiler15LoadElimination15ReduceLoadFieldEPNS1_4NodeERKNS1_11FieldAccessE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15LoadElimination15ReduceLoadFieldEPNS1_4NodeERKNS1_11FieldAccessE
	.type	_ZN2v88internal8compiler15LoadElimination15ReduceLoadFieldEPNS1_4NodeERKNS1_11FieldAccessE, @function
_ZN2v88internal8compiler15LoadElimination15ReduceLoadFieldEPNS1_4NodeERKNS1_11FieldAccessE:
.LFB19425:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	24(%r15), %rcx
	movl	20(%r13), %edx
	movq	%rax, -120(%rbp)
	movq	32(%r15), %rax
	andl	$16777215, %edx
	subq	%rcx, %rax
	sarq	$3, %rax
	cmpq	%rdx, %rax
	jbe	.L2113
	movq	(%rcx,%rdx,8), %r8
	testq	%r8, %r8
	je	.L2113
	movabsq	$-4294967041, %rax
	andq	(%rbx), %rax
	cmpq	$1, %rax
	jne	.L2116
	movq	520(%r8), %rsi
	testq	%rsi, %rsi
	je	.L2117
	movq	%r14, %rdi
	movq	%r8, -120(%rbp)
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	movq	-120(%rbp), %r8
	movq	24(%rsi), %rdx
	leaq	16(%rsi), %rdi
	testq	%rdx, %rdx
	je	.L2117
	movq	%rdi, %rcx
	jmp	.L2118
	.p2align 4,,10
	.p2align 3
.L2162:
	movq	%rdx, %rcx
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L2119
.L2118:
	cmpq	%rax, 32(%rdx)
	jnb	.L2162
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L2118
.L2119:
	cmpq	%rcx, %rdi
	je	.L2117
	cmpq	%rax, 32(%rcx)
	jbe	.L2163
	.p2align 4,,10
	.p2align 3
.L2117:
	movq	16(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L2136
	movq	16(%r15), %rcx
	movq	%r8, %rdi
	movq	%r12, %rsi
	call	_ZNK2v88internal8compiler15LoadElimination13AbstractState7SetMapsEPNS1_4NodeENS0_13ZoneHandleSetINS0_3MapEEEPNS0_4ZoneE
	movq	%rax, %r8
.L2136:
	movq	%r8, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler15LoadElimination11UpdateStateEPNS1_4NodeEPKNS2_13AbstractStateE
	jmp	.L2115
	.p2align 4,,10
	.p2align 3
.L2113:
	xorl	%eax, %eax
.L2115:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L2164
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2116:
	.cfi_restore_state
	movq	%rbx, %rdi
	movq	%r8, -144(%rbp)
	call	_ZN2v88internal8compiler15LoadElimination12FieldIndexOfERKNS1_11FieldAccessE
	movq	-144(%rbp), %r8
	movq	%rax, %rdx
	sarq	$32, %rax
	andl	%edx, %eax
	cmpl	$-1, %eax
	je	.L2117
	movzbl	32(%rbx), %r11d
	movq	40(%rbx), %rcx
	movq	%r8, %rdi
	movq	%r14, %rsi
	movq	%rdx, -128(%rbp)
	movb	%r11b, -145(%rbp)
	call	_ZNK2v88internal8compiler15LoadElimination13AbstractState11LookupFieldEPNS1_4NodeENS2_10IndexRangeENS1_14ConstFieldInfoE
	movq	-144(%rbp), %r8
	movq	-128(%rbp), %rdx
	testq	%rax, %rax
	movzbl	-145(%rbp), %r11d
	je	.L2165
.L2125:
	movzbl	8(%rax), %ecx
	cmpb	%r11b, %cl
	je	.L2127
	leal	-6(%r11), %esi
	cmpb	$2, %sil
	ja	.L2129
	subl	$6, %ecx
	cmpb	$2, %cl
	ja	.L2129
.L2127:
	movq	(%rax), %r10
	movzbl	23(%r10), %eax
	leaq	32(%r10), %rcx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2131
	movq	32(%r10), %rcx
	movl	8(%rcx), %eax
	addq	$16, %rcx
.L2131:
	testl	%eax, %eax
	jle	.L2132
	cmpq	$0, (%rcx)
	je	.L2129
.L2132:
	movq	8(%r10), %rax
	movq	8(%r12), %rsi
	movq	%rax, -112(%rbp)
	cmpq	%rax, %rsi
	jne	.L2166
.L2135:
	movq	8(%r15), %rdi
	movq	%r10, -120(%rbp)
	movq	%r10, %rdx
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	-120(%rbp), %r10
	movq	%r10, %rax
	jmp	.L2115
	.p2align 4,,10
	.p2align 3
.L2165:
	movq	40(%rbx), %rax
	testq	%rax, %rax
	je	.L2126
	xorl	%ecx, %ecx
	movq	%r8, %rdi
	movq	%r14, %rsi
	call	_ZNK2v88internal8compiler15LoadElimination13AbstractState11LookupFieldEPNS1_4NodeENS2_10IndexRangeENS1_14ConstFieldInfoE
	movq	-144(%rbp), %r8
	movq	-128(%rbp), %rdx
	testq	%rax, %rax
	movzbl	-145(%rbp), %r11d
	jne	.L2125
	.p2align 4,,10
	.p2align 3
.L2129:
	movq	40(%rbx), %rax
.L2126:
	movq	8(%rbx), %rcx
	movq	%r12, -112(%rbp)
	movq	%r8, %rdi
	movq	%r14, %rsi
	movb	%r11b, -104(%rbp)
	movq	%rcx, -96(%rbp)
	movq	16(%r15), %rcx
	movq	%rax, -88(%rbp)
	pushq	-88(%rbp)
	pushq	-96(%rbp)
	pushq	-104(%rbp)
	pushq	-112(%rbp)
	call	_ZNK2v88internal8compiler15LoadElimination13AbstractState8AddFieldEPNS1_4NodeENS2_10IndexRangeENS2_9FieldInfoEPNS0_4ZoneE
	addq	$32, %rsp
	movq	%rax, %r8
	jmp	.L2117
	.p2align 4,,10
	.p2align 3
.L2163:
	movq	40(%rcx), %rsi
	movq	%rsi, %rax
	andl	$3, %eax
	cmpq	$1, %rax
	je	.L2117
	testq	%rax, %rax
	je	.L2122
	movq	14(%rsi), %rdx
	movq	6(%rsi), %rax
	movq	%rdx, %rcx
	subq	%rax, %rcx
	cmpq	$8, %rcx
	jne	.L2117
	movq	48(%r15), %rdi
	cmpq	%rdx, %rax
	je	.L2167
	movq	(%rax), %rsi
.L2137:
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	%r12, %rsi
	movq	$16777217, 8(%rax)
	movq	8(%r15), %rdi
	movq	%rax, %rbx
	movq	%rbx, %rdx
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%rbx, %rax
	jmp	.L2115
	.p2align 4,,10
	.p2align 3
.L2166:
	leaq	-112(%rbp), %rdi
	movq	%r10, -144(%rbp)
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	movq	-144(%rbp), %r10
	testb	%al, %al
	jne	.L2135
	movq	48(%r15), %rax
	movq	8(%r10), %rsi
	movq	%r10, %xmm0
	movq	%r13, %xmm1
	movq	8(%r12), %rdi
	punpcklqdq	%xmm1, %xmm0
	movq	(%rax), %rax
	movaps	%xmm0, -144(%rbp)
	movq	(%rax), %rdx
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, %rbx
	movq	48(%r15), %rax
	movq	%rbx, %rsi
	movq	8(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9TypeGuardENS1_4TypeE@PLT
	movq	%r13, %rdi
	leaq	-80(%rbp), %rcx
	xorl	%r8d, %r8d
	movdqa	-144(%rbp), %xmm0
	movq	%rax, %rsi
	movq	-120(%rbp), %rax
	movl	$3, %edx
	movq	%rax, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rbx, 8(%rax)
	movq	%rax, %r13
	movq	%rax, %r10
	jmp	.L2135
	.p2align 4,,10
	.p2align 3
.L2122:
	movq	48(%r15), %rdi
	jmp	.L2137
.L2164:
	call	__stack_chk_fail@PLT
.L2167:
	xorl	%edx, %edx
	xorl	%esi, %esi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE19425:
	.size	_ZN2v88internal8compiler15LoadElimination15ReduceLoadFieldEPNS1_4NodeERKNS1_11FieldAccessE, .-_ZN2v88internal8compiler15LoadElimination15ReduceLoadFieldEPNS1_4NodeERKNS1_11FieldAccessE
	.section	.text._ZN2v88internal8compiler15LoadElimination16ReduceStoreFieldEPNS1_4NodeERKNS1_11FieldAccessE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15LoadElimination16ReduceStoreFieldEPNS1_4NodeERKNS1_11FieldAccessE
	.type	_ZN2v88internal8compiler15LoadElimination16ReduceStoreFieldEPNS1_4NodeERKNS1_11FieldAccessE, @function
_ZN2v88internal8compiler15LoadElimination16ReduceStoreFieldEPNS1_4NodeERKNS1_11FieldAccessE:
.LFB19426:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	movq	%r12, %rdi
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -152(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -160(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	24(%r13), %rcx
	movl	20(%rax), %edx
	movq	%rax, %rbx
	movq	32(%r13), %rax
	subq	%rcx, %rax
	andl	$16777215, %edx
	sarq	$3, %rax
	cmpq	%rdx, %rax
	jbe	.L2169
	movq	(%rcx,%rdx,8), %r15
	testq	%r15, %r15
	je	.L2169
	movabsq	$-4294967041, %rax
	andq	(%r14), %rax
	cmpq	$1, %rax
	je	.L2202
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler15LoadElimination12FieldIndexOfERKNS1_11FieldAccessE
	movq	%rax, %r9
	movq	%rax, %r11
	sarq	$32, %r9
	andl	%r9d, %eax
	movq	%r9, -168(%rbp)
	cmpl	$-1, %eax
	je	.L2178
	movq	40(%r14), %rcx
	movzbl	32(%r14), %r8d
	movq	%r11, %rdx
	movq	%r15, %rdi
	movq	-152(%rbp), %rsi
	movq	%r11, -176(%rbp)
	movb	%r8b, -185(%rbp)
	movq	%rcx, -184(%rbp)
	call	_ZNK2v88internal8compiler15LoadElimination13AbstractState11LookupFieldEPNS1_4NodeENS2_10IndexRangeENS1_14ConstFieldInfoE
	movq	-176(%rbp), %r11
	movq	-184(%rbp), %rcx
	testq	%rax, %rax
	movzbl	-185(%rbp), %r8d
	movq	-168(%rbp), %r9
	je	.L2181
	testq	%rcx, %rcx
	jne	.L2181
	cmpq	$0, 16(%rax)
	je	.L2182
	movzbl	8(%rax), %edx
	cmpb	%r8b, %dl
	je	.L2182
	leal	-6(%r8), %ecx
	cmpb	$2, %cl
	ja	.L2183
	subl	$6, %edx
	cmpb	$2, %dl
	jbe	.L2182
.L2183:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, -152(%rbp)
	movq	48(%r13), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder11UnreachableEv@PLT
	movq	%rbx, %xmm0
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movhps	-152(%rbp), %xmm0
	movq	%rax, %rsi
	leaq	-80(%rbp), %rcx
	movl	$2, %edx
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	jmp	.L2171
	.p2align 4,,10
	.p2align 3
.L2169:
	xorl	%eax, %eax
.L2171:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L2203
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2181:
	.cfi_restore_state
	movq	8(%r14), %rdx
	movq	40(%r14), %rax
	movb	%r8b, -104(%rbp)
	movq	-160(%rbp), %rbx
	movq	16(%r13), %r8
	movq	%rdx, -96(%rbp)
	movq	%rbx, -112(%rbp)
	movq	%r8, %rsi
	movq	%rax, -88(%rbp)
	testq	%rcx, %rcx
	je	.L2186
	cmpb	$0, 48(%r14)
	jne	.L2204
.L2187:
	movq	-152(%rbp), %rbx
	movq	%r9, %rax
	movl	%r11d, %edx
	movq	%r15, %rdi
	movq	8(%r14), %rcx
	salq	$32, %rax
	movq	%r15, %xmm0
	leaq	-144(%rbp), %rsi
	movq	%rbx, %xmm1
	orq	%rax, %rdx
	movq	%r11, -160(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movq	$0, -128(%rbp)
	movaps	%xmm0, -144(%rbp)
	call	_ZNK2v88internal8compiler15LoadElimination13AbstractState9KillFieldERKNS2_14AliasStateInfoENS2_10IndexRangeENS0_11MaybeHandleINS0_4NameEEEPNS0_4ZoneE
	pushq	-88(%rbp)
	movq	16(%r13), %rcx
	movq	%rbx, %rsi
	movq	-160(%rbp), %r11
	pushq	-96(%rbp)
	movq	%rax, %rdi
	pushq	-104(%rbp)
	pushq	-112(%rbp)
	movq	%r11, %rdx
	movq	%r11, -152(%rbp)
	call	_ZNK2v88internal8compiler15LoadElimination13AbstractState8AddFieldEPNS1_4NodeENS2_10IndexRangeENS2_9FieldInfoEPNS0_4ZoneE
	addq	$32, %rsp
	movq	16(%r13), %rcx
	movq	$0, -88(%rbp)
	pushq	-88(%rbp)
	movq	-152(%rbp), %r11
	movq	%rax, %rdi
	pushq	-96(%rbp)
	pushq	-104(%rbp)
	pushq	-112(%rbp)
.L2201:
	movq	%r11, %rdx
	movq	%rbx, %rsi
	call	_ZNK2v88internal8compiler15LoadElimination13AbstractState8AddFieldEPNS1_4NodeENS2_10IndexRangeENS2_9FieldInfoEPNS0_4ZoneE
	addq	$32, %rsp
	movq	%rax, %r15
.L2177:
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler15LoadElimination11UpdateStateEPNS1_4NodeEPKNS2_13AbstractStateE
	jmp	.L2171
	.p2align 4,,10
	.p2align 3
.L2202:
	movq	%r15, %xmm0
	movq	$0, -96(%rbp)
	movq	16(%r13), %r14
	movhps	-152(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	520(%r15), %rdi
	testq	%rdi, %rdi
	je	.L2173
	leaq	-112(%rbp), %rsi
	movq	%r14, %rdx
	call	_ZNK2v88internal8compiler15LoadElimination12AbstractMaps4KillERKNS2_14AliasStateInfoEPNS0_4ZoneE
	movq	%rax, %rbx
	cmpq	520(%r15), %rax
	jne	.L2205
.L2173:
	movq	-160(%rbp), %rax
	movq	8(%rax), %rax
	movq	%rax, -144(%rbp)
	testb	$1, %al
	jne	.L2177
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L2177
	leaq	-144(%rbp), %rdi
	call	_ZNK2v88internal8compiler4Type14AsHeapConstantEv@PLT
	leaq	8(%rax), %rdi
	call	_ZNK2v88internal8compiler9ObjectRef5AsMapEv@PLT
	leaq	-112(%rbp), %rdi
	movq	%rdx, -104(%rbp)
	movq	%rax, -112(%rbp)
	call	_ZNK2v88internal8compiler6MapRef6objectEv@PLT
	movq	16(%r13), %rcx
	movq	-152(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZNK2v88internal8compiler15LoadElimination13AbstractState7SetMapsEPNS1_4NodeENS0_13ZoneHandleSetINS0_3MapEEEPNS0_4ZoneE
	movq	%rax, %r15
	jmp	.L2177
	.p2align 4,,10
	.p2align 3
.L2182:
	movq	-160(%rbp), %rcx
	cmpq	%rcx, (%rax)
	je	.L2206
	movq	8(%r14), %rdx
	movq	40(%r14), %rax
	movq	%rcx, -112(%rbp)
	movb	%r8b, -104(%rbp)
	movq	16(%r13), %rsi
	movq	%rdx, -96(%rbp)
	movq	%rax, -88(%rbp)
.L2186:
	movq	-152(%rbp), %rbx
	movq	%r9, %rdx
	movl	%r11d, %eax
	movq	%rsi, %r8
	movq	8(%r14), %rcx
	salq	$32, %rdx
	movq	%r15, %xmm0
	movq	%r15, %rdi
	movq	%rbx, %xmm2
	leaq	-144(%rbp), %r9
	orq	%rax, %rdx
	movq	%r11, -160(%rbp)
	punpcklqdq	%xmm2, %xmm0
	movq	%r9, %rsi
	movq	$0, -128(%rbp)
	movaps	%xmm0, -144(%rbp)
	call	_ZNK2v88internal8compiler15LoadElimination13AbstractState9KillFieldERKNS2_14AliasStateInfoENS2_10IndexRangeENS0_11MaybeHandleINS0_4NameEEEPNS0_4ZoneE
	pushq	-88(%rbp)
	movq	16(%r13), %rcx
	movq	-160(%rbp), %r11
	pushq	-96(%rbp)
	movq	%rax, %rdi
	pushq	-104(%rbp)
	pushq	-112(%rbp)
	jmp	.L2201
	.p2align 4,,10
	.p2align 3
.L2204:
	movq	-152(%rbp), %rsi
	movq	%r8, %rcx
	movq	%r11, %rdx
	movq	%r15, %rdi
	movq	%r9, -168(%rbp)
	movq	%r11, -160(%rbp)
	call	_ZNK2v88internal8compiler15LoadElimination13AbstractState14KillConstFieldEPNS1_4NodeENS2_10IndexRangeEPNS0_4ZoneE
	movq	16(%r13), %r8
	movq	-160(%rbp), %r11
	movq	-168(%rbp), %r9
	movq	%rax, %r15
	jmp	.L2187
	.p2align 4,,10
	.p2align 3
.L2178:
	movq	16(%r13), %rcx
	movq	8(%r14), %rdx
	movq	%r15, %rdi
	movq	-152(%rbp), %rsi
	call	_ZNK2v88internal8compiler15LoadElimination13AbstractState10KillFieldsEPNS1_4NodeENS0_11MaybeHandleINS0_4NameEEEPNS0_4ZoneE
	movq	%rax, %r15
	jmp	.L2177
	.p2align 4,,10
	.p2align 3
.L2205:
	movq	16(%r14), %rdx
	movq	24(%r14), %rax
	subq	%rdx, %rax
	cmpq	$527, %rax
	jbe	.L2207
	leaq	528(%rdx), %rax
	movq	%rax, 16(%r14)
.L2175:
	movq	%r15, %rsi
	movl	$66, %ecx
	movq	%rdx, %rdi
	movq	%rdx, %r15
	rep movsq
	movq	%rbx, 520(%rdx)
	jmp	.L2173
	.p2align 4,,10
	.p2align 3
.L2206:
	movq	%rbx, %rax
	jmp	.L2171
.L2207:
	movl	$528, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L2175
.L2203:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19426:
	.size	_ZN2v88internal8compiler15LoadElimination16ReduceStoreFieldEPNS1_4NodeERKNS1_11FieldAccessE, .-_ZN2v88internal8compiler15LoadElimination16ReduceStoreFieldEPNS1_4NodeERKNS1_11FieldAccessE
	.section	.text._ZN2v88internal8compiler15LoadElimination17UpdateStateForPhiEPKNS2_13AbstractStateEPNS1_4NodeES7_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15LoadElimination17UpdateStateForPhiEPKNS2_13AbstractStateEPNS1_4NodeES7_
	.type	_ZN2v88internal8compiler15LoadElimination17UpdateStateForPhiEPKNS2_13AbstractStateEPNS1_4NodeES7_, @function
_ZN2v88internal8compiler15LoadElimination17UpdateStateForPhiEPKNS2_13AbstractStateEPNS1_4NodeES7_:
.LFB19430:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	32(%rcx), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$56, %rsp
	movzbl	23(%rcx), %r15d
	movq	%rax, -56(%rbp)
	andl	$15, %r15d
	cmpl	$15, %r15d
	jne	.L2210
	movq	32(%rcx), %rax
	movl	8(%rax), %r15d
.L2210:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	24(%r14), %rsi
	xorl	%ecx, %ecx
	movl	20(%rax), %edx
	movq	32(%r14), %rax
	subq	%rsi, %rax
	andl	$16777215, %edx
	sarq	$3, %rax
	cmpq	%rax, %rdx
	jnb	.L2211
	movq	(%rsi,%rdx,8), %rcx
.L2211:
	movzbl	23(%r13), %eax
	movq	32(%r13), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2212
	movq	16(%rdi), %rdi
.L2212:
	movq	520(%rcx), %rsi
	testq	%rsi, %rsi
	je	.L2214
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	movq	24(%rsi), %rdx
	leaq	16(%rsi), %rdi
	testq	%rdx, %rdx
	je	.L2214
	movq	%rdi, %rcx
	jmp	.L2216
	.p2align 4,,10
	.p2align 3
.L2259:
	movq	%rdx, %rcx
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L2217
.L2216:
	cmpq	%rax, 32(%rdx)
	jnb	.L2259
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L2216
.L2217:
	cmpq	%rcx, %rdi
	je	.L2214
	cmpq	%rax, 32(%rcx)
	ja	.L2214
	movq	40(%rcx), %rax
	movq	%rax, -64(%rbp)
	cmpl	$2, %r15d
	jle	.L2232
	andl	$3, %eax
	movq	$8, -72(%rbp)
	movq	%rax, -88(%rbp)
	leal	-1(%r15), %eax
	movl	$1, %r15d
	movl	%eax, -76(%rbp)
.L2233:
	movl	%r15d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	24(%r14), %rsi
	xorl	%ecx, %ecx
	movl	20(%rax), %edx
	movq	32(%r14), %rax
	subq	%rsi, %rax
	andl	$16777215, %edx
	sarq	$3, %rax
	cmpq	%rax, %rdx
	jnb	.L2221
	movq	(%rsi,%rdx,8), %rcx
.L2221:
	movzbl	23(%r13), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	movq	-56(%rbp), %rax
	je	.L2222
	addq	-72(%rbp), %rax
.L2223:
	movq	520(%rcx), %rsi
	testq	%rsi, %rsi
	je	.L2214
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	movq	24(%rsi), %rdx
	leaq	16(%rsi), %rdi
	testq	%rdx, %rdx
	je	.L2214
	movq	%rdi, %rcx
	jmp	.L2224
	.p2align 4,,10
	.p2align 3
.L2260:
	movq	%rdx, %rcx
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L2225
.L2224:
	cmpq	%rax, 32(%rdx)
	jnb	.L2260
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L2224
.L2225:
	cmpq	%rdi, %rcx
	je	.L2214
	cmpq	%rax, 32(%rcx)
	ja	.L2214
	movq	40(%rcx), %rax
	cmpq	%rax, -64(%rbp)
	je	.L2228
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$2, %rdx
	je	.L2261
.L2214:
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2222:
	.cfi_restore_state
	movq	(%rax), %rax
	movq	-72(%rbp), %rsi
	leaq	16(%rax,%rsi), %rax
	jmp	.L2223
	.p2align 4,,10
	.p2align 3
.L2228:
	addq	$8, -72(%rbp)
	addl	$1, %r15d
	cmpl	-76(%rbp), %r15d
	jne	.L2233
.L2232:
	movq	16(%r14), %rcx
	movq	-64(%rbp), %rdx
	addq	$56, %rsp
	movq	%r13, %rsi
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNK2v88internal8compiler15LoadElimination13AbstractState7SetMapsEPNS1_4NodeENS0_13ZoneHandleSetINS0_3MapEEEPNS0_4ZoneE
	.p2align 4,,10
	.p2align 3
.L2261:
	.cfi_restore_state
	cmpq	$2, -88(%rbp)
	jne	.L2214
	movq	-64(%rbp), %rdi
	movq	6(%rax), %rsi
	movq	14(%rax), %rcx
	leaq	-2(%rdi), %rdx
	movq	16(%rdx), %rax
	movq	6(%rdi), %rdi
	subq	%rsi, %rcx
	subq	%rdi, %rax
	cmpq	%rax, %rcx
	jne	.L2214
	sarq	$3, %rcx
	je	.L2228
	movq	(%rsi), %rdx
	xorl	%eax, %eax
	jmp	.L2230
	.p2align 4,,10
	.p2align 3
.L2262:
	addq	$1, %rax
	cmpq	%rax, %rcx
	je	.L2228
	movq	(%rsi,%rax,8), %rdx
.L2230:
	cmpq	%rdx, (%rdi,%rax,8)
	je	.L2262
	jmp	.L2214
	.cfi_endproc
.LFE19430:
	.size	_ZN2v88internal8compiler15LoadElimination17UpdateStateForPhiEPKNS2_13AbstractStateEPNS1_4NodeES7_, .-_ZN2v88internal8compiler15LoadElimination17UpdateStateForPhiEPKNS2_13AbstractStateEPNS1_4NodeES7_
	.section	.rodata._ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE12emplace_backIJS0_EEEvDpOT_.str1.1,"aMS",@progbits,1
.LC13:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE12emplace_backIJS0_EEEvDpOT_,"axG",@progbits,_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE12emplace_backIJS0_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE12emplace_backIJS0_EEEvDpOT_
	.type	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE12emplace_backIJS0_EEEvDpOT_, @function
_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE12emplace_backIJS0_EEEvDpOT_:
.LFB23503:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	16(%rdi), %r13
	cmpq	24(%rdi), %r13
	je	.L2264
	movq	(%rsi), %rax
	movq	%rax, 0(%r13)
	addq	$8, 16(%rdi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2264:
	.cfi_restore_state
	movq	8(%rdi), %r14
	movq	%r13, %rcx
	subq	%r14, %rcx
	movq	%rcx, %rax
	sarq	$3, %rax
	cmpq	$268435455, %rax
	je	.L2289
	testq	%rax, %rax
	je	.L2276
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L2290
	movl	$2147483640, %esi
	movl	$2147483640, %r15d
.L2267:
	movq	(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L2291
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L2270:
	leaq	(%rax,%r15), %rdi
	leaq	8(%rax), %rdx
.L2268:
	movq	(%r12), %rsi
	movq	%rsi, (%rax,%rcx)
	cmpq	%r14, %r13
	je	.L2271
	leaq	-8(%r13), %rdx
	movq	%rdx, %rcx
	leaq	15(%rax), %rdx
	subq	%r14, %rcx
	subq	%r14, %rdx
	movq	%rcx, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rdx
	jbe	.L2279
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rsi
	je	.L2279
	leaq	1(%rsi), %r8
	xorl	%edx, %edx
	movq	%r8, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L2273:
	movdqu	(%r14,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rsi, %rdx
	jne	.L2273
	movq	%r8, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rsi
	addq	%rsi, %r14
	addq	%rax, %rsi
	cmpq	%rdx, %r8
	je	.L2275
	movq	(%r14), %rdx
	movq	%rdx, (%rsi)
.L2275:
	leaq	16(%rax,%rcx), %rdx
.L2271:
	movq	%rax, %xmm0
	movq	%rdx, %xmm2
	movq	%rdi, 24(%rbx)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 8(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2290:
	.cfi_restore_state
	testq	%rdx, %rdx
	jne	.L2292
	movl	$8, %edx
	xorl	%edi, %edi
	xorl	%eax, %eax
	jmp	.L2268
	.p2align 4,,10
	.p2align 3
.L2276:
	movl	$8, %esi
	movl	$8, %r15d
	jmp	.L2267
	.p2align 4,,10
	.p2align 3
.L2279:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L2272:
	movq	(%r14,%rdx,8), %r8
	movq	%r8, (%rax,%rdx,8)
	movq	%rdx, %r8
	addq	$1, %rdx
	cmpq	%r8, %rsi
	jne	.L2272
	jmp	.L2275
.L2291:
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	jmp	.L2270
.L2289:
	leaq	.LC13(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L2292:
	movl	$268435455, %r15d
	cmpq	$268435455, %rdx
	cmova	%r15, %rdx
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rsi
	jmp	.L2267
	.cfi_endproc
.LFE23503:
	.size	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE12emplace_backIJS0_EEEvDpOT_, .-_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE12emplace_backIJS0_EEEvDpOT_
	.section	.text._ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_
	.type	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_, @function
_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_:
.LFB23508:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r14
	movq	8(%rdi), %rbx
	movq	%r14, %rax
	subq	%rbx, %rax
	sarq	$3, %rax
	cmpq	$268435455, %rax
	je	.L2331
	movq	%rsi, %r8
	movq	%rdi, %r13
	movq	%rsi, %r12
	movq	%rdx, %r15
	subq	%rbx, %r8
	testq	%rax, %rax
	je	.L2309
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L2332
	movl	$2147483640, %esi
	movl	$2147483640, %edx
.L2295:
	movq	0(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	%rsi, %rcx
	jb	.L2333
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L2298:
	leaq	(%rax,%rdx), %rdi
	leaq	8(%rax), %rcx
	jmp	.L2296
	.p2align 4,,10
	.p2align 3
.L2332:
	testq	%rdx, %rdx
	jne	.L2334
	movl	$8, %ecx
	xorl	%edi, %edi
	xorl	%eax, %eax
.L2296:
	movq	(%r15), %rdx
	movq	%rdx, (%rax,%r8)
	cmpq	%rbx, %r12
	je	.L2299
	leaq	-8(%r12), %r8
	leaq	15(%rax), %rdx
	subq	%rbx, %r8
	subq	%rbx, %rdx
	movq	%r8, %rcx
	shrq	$3, %rcx
	cmpq	$30, %rdx
	jbe	.L2312
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rcx
	je	.L2312
	leaq	1(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L2301:
	movdqu	(%rbx,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L2301
	movq	%rsi, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rcx
	addq	%rcx, %rbx
	addq	%rax, %rcx
	cmpq	%rdx, %rsi
	je	.L2303
	movq	(%rbx), %rdx
	movq	%rdx, (%rcx)
.L2303:
	leaq	16(%rax,%r8), %rcx
.L2299:
	cmpq	%r14, %r12
	je	.L2304
	subq	%r12, %r14
	leaq	15(%r12), %rdx
	leaq	-8(%r14), %r9
	subq	%rcx, %rdx
	movq	%r9, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rdx
	jbe	.L2313
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rsi
	je	.L2313
	leaq	1(%rsi), %r8
	xorl	%edx, %edx
	movq	%r8, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L2306:
	movdqu	(%r12,%rdx), %xmm2
	movups	%xmm2, (%rcx,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rsi
	jne	.L2306
	movq	%r8, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rsi
	leaq	(%rcx,%rsi), %r10
	addq	%rsi, %r12
	cmpq	%rdx, %r8
	je	.L2308
	movq	(%r12), %rdx
	movq	%rdx, (%r10)
.L2308:
	leaq	8(%rcx,%r9), %rcx
.L2304:
	movq	%rax, %xmm0
	movq	%rcx, %xmm3
	movq	%rdi, 24(%r13)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 8(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2309:
	.cfi_restore_state
	movl	$8, %esi
	movl	$8, %edx
	jmp	.L2295
	.p2align 4,,10
	.p2align 3
.L2313:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L2305:
	movq	(%r12,%rdx,8), %r8
	movq	%r8, (%rcx,%rdx,8)
	movq	%rdx, %r8
	addq	$1, %rdx
	cmpq	%rsi, %r8
	jne	.L2305
	jmp	.L2308
	.p2align 4,,10
	.p2align 3
.L2312:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L2300:
	movq	(%rbx,%rdx,8), %rsi
	movq	%rsi, (%rax,%rdx,8)
	movq	%rdx, %rsi
	addq	$1, %rdx
	cmpq	%rsi, %rcx
	jne	.L2300
	jmp	.L2303
.L2333:
	movq	%r8, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %r8
	jmp	.L2298
.L2331:
	leaq	.LC13(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L2334:
	cmpq	$268435455, %rdx
	movl	$268435455, %eax
	cmova	%rax, %rdx
	salq	$3, %rdx
	movq	%rdx, %rsi
	jmp	.L2295
	.cfi_endproc
.LFE23508:
	.size	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_, .-_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_
	.section	.rodata._ZN2v88internal8compiler15LoadElimination27ReduceMaybeGrowFastElementsEPNS1_4NodeE.str1.1,"aMS",@progbits,1
.LC14:
	.string	"vector::reserve"
	.section	.text._ZN2v88internal8compiler15LoadElimination27ReduceMaybeGrowFastElementsEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15LoadElimination27ReduceMaybeGrowFastElementsEPNS1_4NodeE
	.type	_ZN2v88internal8compiler15LoadElimination27ReduceMaybeGrowFastElementsEPNS1_4NodeE, @function
_ZN2v88internal8compiler15LoadElimination27ReduceMaybeGrowFastElementsEPNS1_4NodeE:
.LFB19422:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler28GrowFastElementsParametersOfEPKNS1_8OperatorE@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	movzbl	(%rax), %r13d
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	%rax, -120(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	24(%r12), %rcx
	movl	20(%rax), %edx
	movq	32(%r12), %rax
	subq	%rcx, %rax
	andl	$16777215, %edx
	sarq	$3, %rax
	cmpq	%rax, %rdx
	jnb	.L2336
	movq	(%rcx,%rdx,8), %r15
	testq	%r15, %r15
	je	.L2336
	movq	48(%r12), %rax
	movq	16(%r12), %r9
	movq	360(%rax), %rbx
	leaq	488(%rbx), %rdx
	testb	%r13b, %r13b
	je	.L2369
	leaq	152(%rbx), %rcx
	leaq	160(%rbx), %rax
	movq	%rcx, %rsi
	movq	%rax, -104(%rbp)
	movq	%rcx, %rdx
	andl	$3, %esi
	cmpq	$1, %rsi
	je	.L2389
	testq	%rsi, %rsi
	jne	.L2343
	movq	16(%r9), %r13
	movq	24(%r9), %rax
	subq	%r13, %rax
	cmpq	$31, %rax
	jbe	.L2390
	leaq	32(%r13), %rax
	movq	%rax, 16(%r9)
.L2345:
	movq	%r9, 0(%r13)
	movq	$0, 8(%r13)
	movq	$0, 16(%r13)
	movq	$0, 24(%r13)
	cmpq	-104(%rbp), %rcx
	jnb	.L2346
	leaq	-96(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rcx, -96(%rbp)
	call	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE12emplace_backIJS0_EEEvDpOT_
	movq	16(%r13), %rsi
	cmpq	24(%r13), %rsi
	je	.L2347
	movq	-104(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 16(%r13)
	jmp	.L2368
	.p2align 4,,10
	.p2align 3
.L2336:
	xorl	%eax, %eax
.L2338:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L2391
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2346:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r13, %rdi
	leaq	-104(%rbp), %rdx
	movq	%rcx, -128(%rbp)
	call	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_
	movq	-128(%rbp), %rcx
	leaq	-96(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rcx, -96(%rbp)
	call	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE12emplace_backIJS0_EEEvDpOT_
.L2368:
	movq	%r13, %rdx
	movq	16(%r12), %r9
	orq	$2, %rdx
.L2369:
	movq	%r15, %rdi
	movq	%r9, %rcx
	movq	%r14, %rsi
	call	_ZNK2v88internal8compiler15LoadElimination13AbstractState7SetMapsEPNS1_4NodeENS0_13ZoneHandleSetINS0_3MapEEEPNS0_4ZoneE
	movq	$0, -80(%rbp)
	movq	16(%r12), %r15
	movq	%rax, %xmm0
	movq	%rax, %r13
	movhps	-120(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.L2370
	leaq	-96(%rbp), %rsi
	movq	%r15, %rcx
	xorl	%edx, %edx
	call	_ZNK2v88internal8compiler15LoadElimination13AbstractField4KillERKNS2_14AliasStateInfoENS0_11MaybeHandleINS0_4NameEEEPNS0_4ZoneE
	movq	%rax, %rbx
	cmpq	16(%r13), %rax
	je	.L2392
	movq	16(%r15), %rdx
	movq	24(%r15), %rax
	subq	%rdx, %rax
	cmpq	$527, %rax
	jbe	.L2393
	leaq	528(%rdx), %rax
	movq	%rax, 16(%r15)
.L2373:
	movq	%r13, %rsi
	movl	$66, %ecx
	movq	%rdx, %rdi
	movq	%rdx, %r13
	rep movsq
	movq	%rbx, 16(%rdx)
	movq	16(%r12), %r15
.L2370:
	pushq	$0
	movq	-120(%rbp), %rsi
	pxor	%xmm0, %xmm0
	movq	%r13, %rdi
	pushq	$0
	movq	%r15, %rcx
	movabsq	$8589934593, %rdx
	movq	%r14, -96(%rbp)
	movb	$7, -88(%rbp)
	pushq	-88(%rbp)
	pushq	-96(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZNK2v88internal8compiler15LoadElimination13AbstractState8AddFieldEPNS1_4NodeENS2_10IndexRangeENS2_9FieldInfoEPNS0_4ZoneE
	addq	$32, %rsp
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler15LoadElimination11UpdateStateEPNS1_4NodeEPKNS2_13AbstractStateE
	jmp	.L2338
	.p2align 4,,10
	.p2align 3
.L2392:
	movq	16(%r12), %r15
	jmp	.L2370
	.p2align 4,,10
	.p2align 3
.L2389:
	movq	%rax, %rdx
	jmp	.L2369
	.p2align 4,,10
	.p2align 3
.L2343:
	movq	158(%rbx), %rdi
	movq	166(%rbx), %rsi
	addq	$150, %rbx
	subq	%rdi, %rsi
	sarq	$3, %rsi
	je	.L2349
	xorl	%ecx, %ecx
	jmp	.L2351
	.p2align 4,,10
	.p2align 3
.L2394:
	jb	.L2349
	addq	$1, %rcx
	cmpq	%rsi, %rcx
	je	.L2349
.L2351:
	cmpq	(%rdi,%rcx,8), %rax
	jne	.L2394
	jmp	.L2369
	.p2align 4,,10
	.p2align 3
.L2349:
	movq	16(%r9), %r13
	movq	24(%r9), %rax
	subq	%r13, %rax
	cmpq	$31, %rax
	jbe	.L2395
	leaq	32(%r13), %rax
	movq	%rax, 16(%r9)
.L2353:
	movq	%r9, 0(%r13)
	movq	$0, 8(%r13)
	movq	$0, 16(%r13)
	movq	$0, 24(%r13)
	movq	8(%rbx), %rsi
	movq	16(%rbx), %rcx
	subq	%rsi, %rcx
	movq	%rcx, %rax
	sarq	$3, %rax
	addq	$1, %rax
	cmpq	$268435455, %rax
	ja	.L2396
	xorl	%r10d, %r10d
	xorl	%edx, %edx
	testq	%rax, %rax
	jne	.L2397
.L2355:
	xorl	%ecx, %ecx
	jmp	.L2362
	.p2align 4,,10
	.p2align 3
.L2398:
	movq	%rax, (%rdx)
	movq	16(%r13), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 16(%r13)
.L2387:
	movq	8(%rbx), %rsi
	movq	16(%rbx), %rax
	addq	$1, %rcx
	movq	24(%r13), %r10
	subq	%rsi, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	jbe	.L2358
.L2362:
	leaq	(%rsi,%rcx,8), %r9
	movq	(%r9), %rax
	cmpq	-104(%rbp), %rax
	ja	.L2358
	cmpq	%rdx, %r10
	jne	.L2398
	movq	%r9, %rdx
	movq	%r10, %rsi
	movq	%r13, %rdi
	movq	%rcx, -128(%rbp)
	call	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_
	movq	16(%r13), %rdx
	movq	-128(%rbp), %rcx
	jmp	.L2387
	.p2align 4,,10
	.p2align 3
.L2397:
	movq	16(%r9), %rdx
	movq	24(%r9), %rax
	leaq	15(%rcx), %rsi
	andq	$-8, %rsi
	subq	%rdx, %rax
	cmpq	%rax, %rsi
	ja	.L2399
	addq	%rdx, %rsi
	movq	%rsi, 16(%r9)
.L2357:
	leaq	8(%rdx,%rcx), %r10
	movq	%rdx, 8(%r13)
	movq	%rdx, 16(%r13)
	movq	%r10, 24(%r13)
	movq	8(%rbx), %rsi
	cmpq	16(%rbx), %rsi
	jne	.L2355
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L2358:
	cmpq	%rdx, %r10
	je	.L2363
	movq	-104(%rbp), %rax
	movq	%rax, (%rdx)
	addq	$8, 16(%r13)
	jmp	.L2364
	.p2align 4,,10
	.p2align 3
.L2400:
	movq	(%rdx), %rax
	addq	$1, %rcx
	movq	%rax, (%rsi)
	addq	$8, 16(%r13)
.L2364:
	movq	8(%rbx), %rdx
	movq	16(%rbx), %rax
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rax, %rcx
	jnb	.L2368
.L2365:
	movq	16(%r13), %rsi
	leaq	(%rdx,%rcx,8), %rdx
	cmpq	24(%r13), %rsi
	jne	.L2400
	movq	%r13, %rdi
	movq	%rcx, -128(%rbp)
	call	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_
	movq	8(%rbx), %rdx
	movq	16(%rbx), %rax
	movq	-128(%rbp), %rcx
	subq	%rdx, %rax
	addq	$1, %rcx
	sarq	$3, %rax
	cmpq	%rcx, %rax
	ja	.L2365
	jmp	.L2368
.L2395:
	movq	%r9, %rdi
	movl	$32, %esi
	movq	%r9, -128(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-128(%rbp), %r9
	movq	%rax, %r13
	jmp	.L2353
	.p2align 4,,10
	.p2align 3
.L2393:
	movl	$528, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L2373
.L2363:
	leaq	-104(%rbp), %rdx
	movq	%r10, %rsi
	movq	%r13, %rdi
	movq	%rcx, -128(%rbp)
	call	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_
	movq	-128(%rbp), %rcx
	jmp	.L2364
.L2390:
	movq	%r9, %rdi
	movl	$32, %esi
	movq	%rcx, -136(%rbp)
	movq	%r9, -128(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-128(%rbp), %r9
	movq	-136(%rbp), %rcx
	movq	%rax, %r13
	jmp	.L2345
.L2347:
	leaq	-104(%rbp), %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_
	jmp	.L2368
.L2399:
	movq	%r9, %rdi
	movq	%rcx, -128(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-128(%rbp), %rcx
	movq	%rax, %rdx
	jmp	.L2357
.L2391:
	call	__stack_chk_fail@PLT
.L2396:
	leaq	.LC14(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE19422:
	.size	_ZN2v88internal8compiler15LoadElimination27ReduceMaybeGrowFastElementsEPNS1_4NodeE, .-_ZN2v88internal8compiler15LoadElimination27ReduceMaybeGrowFastElementsEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler15LoadElimination31ReduceTransitionAndStoreElementEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15LoadElimination31ReduceTransitionAndStoreElementEPNS1_4NodeE
	.type	_ZN2v88internal8compiler15LoadElimination31ReduceTransitionAndStoreElementEPNS1_4NodeE, @function
_ZN2v88internal8compiler15LoadElimination31ReduceTransitionAndStoreElementEPNS1_4NodeE:
.LFB19424:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	xorl	%esi, %esi
	pushq	%r13
	movq	%r14, %rdi
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	(%r14), %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal8compiler20DoubleMapParameterOfEPKNS1_8OperatorE@PLT
	movq	(%r14), %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler18FastMapParameterOfEPKNS1_8OperatorE@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	24(%r15), %rcx
	movl	20(%rax), %edx
	movq	32(%r15), %rax
	subq	%rcx, %rax
	andl	$16777215, %edx
	sarq	$3, %rax
	cmpq	%rdx, %rax
	jbe	.L2402
	movq	(%rcx,%rdx,8), %r12
	testq	%r12, %r12
	je	.L2402
	movq	520(%r12), %rsi
	movq	16(%r15), %rcx
	testq	%rsi, %rsi
	je	.L2405
	movq	-104(%rbp), %rdi
	movq	%rcx, -120(%rbp)
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	movq	-120(%rbp), %rcx
	movq	24(%rsi), %r13
	leaq	16(%rsi), %rdi
	testq	%r13, %r13
	je	.L2405
	movq	%rdi, %rdx
	jmp	.L2406
	.p2align 4,,10
	.p2align 3
.L2512:
	movq	%r13, %rdx
	movq	16(%r13), %r13
	testq	%r13, %r13
	je	.L2407
.L2406:
	cmpq	%rax, 32(%r13)
	jnb	.L2512
	movq	24(%r13), %r13
	testq	%r13, %r13
	jne	.L2406
.L2407:
	cmpq	%rdx, %rdi
	je	.L2405
	cmpq	%rax, 32(%rdx)
	ja	.L2405
	movq	40(%rdx), %r11
	movq	%rbx, -88(%rbp)
	movq	%rbx, %r9
	movq	%r11, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	je	.L2410
	testq	%rdx, %rdx
	jne	.L2411
	cmpq	%r11, %rbx
	je	.L2412
	movq	16(%rcx), %rbx
	movq	24(%rcx), %rax
	subq	%rbx, %rax
	cmpq	$31, %rax
	jbe	.L2513
	leaq	32(%rbx), %rax
	movq	%rax, 16(%rcx)
.L2414:
	movq	%rcx, (%rbx)
	movq	$0, 8(%rbx)
	movq	$0, 16(%rbx)
	movq	$0, 24(%rbx)
	cmpq	-88(%rbp), %r11
	jnb	.L2415
	leaq	-80(%rbp), %rsi
	movq	%rbx, %rdi
	movq	%r11, -80(%rbp)
	call	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE12emplace_backIJS0_EEEvDpOT_
	movq	16(%rbx), %rsi
	cmpq	24(%rbx), %rsi
	je	.L2416
	movq	-88(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 16(%rbx)
.L2417:
	orq	$2, %rbx
	movq	16(%r15), %rcx
	movq	%rbx, %r9
.L2410:
	movq	-112(%rbp), %rax
	movq	%r9, %rdx
	andl	$3, %edx
	movq	%rax, -96(%rbp)
	movq	%rax, %rbx
	cmpq	$1, %rdx
	je	.L2465
.L2438:
	testq	%rdx, %rdx
	jne	.L2439
	cmpq	-112(%rbp), %r9
	je	.L2440
	movq	16(%rcx), %rbx
	movq	24(%rcx), %rax
	subq	%rbx, %rax
	cmpq	$31, %rax
	jbe	.L2514
	leaq	32(%rbx), %rax
	movq	%rax, 16(%rcx)
.L2442:
	movq	%rcx, (%rbx)
	movq	$0, 8(%rbx)
	movq	$0, 16(%rbx)
	movq	$0, 24(%rbx)
	cmpq	-96(%rbp), %r9
	jnb	.L2443
	leaq	-80(%rbp), %rsi
	movq	%rbx, %rdi
	movq	%r9, -80(%rbp)
	call	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE12emplace_backIJS0_EEEvDpOT_
	movq	16(%rbx), %rsi
	cmpq	24(%rbx), %rsi
	je	.L2444
	movq	-96(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 16(%rbx)
.L2464:
	movq	16(%r15), %rcx
	orq	$2, %rbx
.L2465:
	movq	%r12, %xmm0
	movq	$0, -64(%rbp)
	movhps	-104(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	movq	520(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2466
	movq	%rcx, %rdx
	leaq	-80(%rbp), %rsi
	movq	%rcx, -112(%rbp)
	call	_ZNK2v88internal8compiler15LoadElimination12AbstractMaps4KillERKNS2_14AliasStateInfoEPNS0_4ZoneE
	cmpq	520(%r12), %rax
	movq	-112(%rbp), %rcx
	movq	%rax, %r13
	jne	.L2467
	movq	16(%r15), %rcx
.L2466:
	movq	-104(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rbx, %rdx
	call	_ZNK2v88internal8compiler15LoadElimination13AbstractState7SetMapsEPNS1_4NodeENS0_13ZoneHandleSetINS0_3MapEEEPNS0_4ZoneE
	movq	16(%r15), %rcx
	movq	%rax, %r12
.L2405:
	movq	%r12, %xmm0
	movq	$0, -64(%rbp)
	movhps	-104(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2470
	leaq	-80(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rcx, -104(%rbp)
	call	_ZNK2v88internal8compiler15LoadElimination13AbstractField4KillERKNS2_14AliasStateInfoENS0_11MaybeHandleINS0_4NameEEEPNS0_4ZoneE
	movq	%rax, %rbx
	cmpq	16(%r12), %rax
	je	.L2470
	movq	-104(%rbp), %rcx
	movq	16(%rcx), %rdx
	movq	24(%rcx), %rax
	subq	%rdx, %rax
	cmpq	$527, %rax
	jbe	.L2515
	leaq	528(%rdx), %rax
	movq	%rax, 16(%rcx)
.L2472:
	movq	%r12, %rsi
	movl	$66, %ecx
	movq	%rdx, %rdi
	movq	%rdx, %r12
	rep movsq
	movq	%rbx, 16(%rdx)
.L2470:
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler15LoadElimination11UpdateStateEPNS1_4NodeEPKNS2_13AbstractStateE
	jmp	.L2404
	.p2align 4,,10
	.p2align 3
.L2402:
	xorl	%eax, %eax
.L2404:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L2516
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2439:
	.cfi_restore_state
	movq	6(%r9), %rsi
	movq	14(%r9), %rdx
	leaq	-2(%r9), %r10
	subq	%rsi, %rdx
	sarq	$3, %rdx
	je	.L2446
	xorl	%eax, %eax
	jmp	.L2447
	.p2align 4,,10
	.p2align 3
.L2517:
	ja	.L2446
	addq	$1, %rax
	cmpq	%rdx, %rax
	je	.L2446
.L2447:
	movq	-112(%rbp), %rbx
	cmpq	%rbx, (%rsi,%rax,8)
	jne	.L2517
.L2440:
	movq	16(%r15), %rcx
	movq	%r9, %rbx
	jmp	.L2465
	.p2align 4,,10
	.p2align 3
.L2411:
	movq	6(%r11), %rdi
	movq	14(%r11), %rsi
	leaq	-2(%r11), %r10
	subq	%rdi, %rsi
	sarq	$3, %rsi
	je	.L2418
	xorl	%eax, %eax
	jmp	.L2419
	.p2align 4,,10
	.p2align 3
.L2518:
	jb	.L2418
	addq	$1, %rax
	cmpq	%rsi, %rax
	je	.L2418
.L2419:
	cmpq	(%rdi,%rax,8), %rbx
	jne	.L2518
.L2412:
	movq	-112(%rbp), %rax
	movq	%r11, %r9
	movq	%rax, -96(%rbp)
	jmp	.L2438
	.p2align 4,,10
	.p2align 3
.L2467:
	movq	16(%rcx), %rdx
	movq	24(%rcx), %rax
	subq	%rdx, %rax
	cmpq	$527, %rax
	jbe	.L2519
	leaq	528(%rdx), %rax
	movq	%rax, 16(%rcx)
.L2469:
	movq	%r12, %rsi
	movl	$66, %ecx
	movq	%rdx, %rdi
	movq	%rdx, %r12
	rep movsq
	movq	%r13, 520(%rdx)
	movq	16(%r15), %rcx
	jmp	.L2466
	.p2align 4,,10
	.p2align 3
.L2418:
	movq	16(%rcx), %r9
	movq	24(%rcx), %rax
	subq	%r9, %rax
	cmpq	$31, %rax
	jbe	.L2520
	leaq	32(%r9), %rax
	movq	%rax, 16(%rcx)
.L2421:
	movq	%rcx, (%r9)
	movq	$0, 8(%r9)
	movq	$0, 16(%r9)
	movq	$0, 24(%r9)
	movq	8(%r10), %rdx
	movq	16(%r10), %rbx
	subq	%rdx, %rbx
	movq	%rbx, %rax
	sarq	$3, %rax
	addq	$1, %rax
	cmpq	$268435455, %rax
	ja	.L2450
	xorl	%edi, %edi
	xorl	%esi, %esi
	testq	%rax, %rax
	jne	.L2521
.L2423:
	xorl	%ebx, %ebx
	jmp	.L2430
	.p2align 4,,10
	.p2align 3
.L2522:
	movq	%rax, (%rsi)
	movq	16(%r9), %rax
	addq	$1, %rbx
	leaq	8(%rax), %rsi
	movq	%rsi, 16(%r9)
.L2509:
	movq	8(%r10), %rdx
	movq	16(%r10), %rax
	movq	24(%r9), %rdi
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rbx, %rax
	jbe	.L2426
.L2430:
	leaq	(%rdx,%rbx,8), %rdx
	movq	(%rdx), %rax
	cmpq	-88(%rbp), %rax
	ja	.L2426
	cmpq	%rdi, %rsi
	jne	.L2522
	movq	%r9, %rdi
	movq	%r10, -128(%rbp)
	addq	$1, %rbx
	movq	%r9, -120(%rbp)
	call	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_
	movq	-120(%rbp), %r9
	movq	-128(%rbp), %r10
	movq	16(%r9), %rsi
	jmp	.L2509
.L2521:
	movq	16(%rcx), %rsi
	movq	24(%rcx), %rax
	leaq	15(%rbx), %r11
	andq	$-8, %r11
	subq	%rsi, %rax
	cmpq	%rax, %r11
	ja	.L2523
	addq	%rsi, %r11
	movq	%r11, 16(%rcx)
.L2425:
	leaq	8(%rsi,%rbx), %rdi
	movq	%rsi, 8(%r9)
	movq	%rsi, 16(%r9)
	movq	%rdi, 24(%r9)
	movq	8(%r10), %rdx
	cmpq	%rdx, 16(%r10)
	jne	.L2423
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L2426:
	cmpq	%rdi, %rsi
	je	.L2431
	movq	-88(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 16(%r9)
.L2432:
	movq	8(%r10), %rdx
	movq	16(%r10), %rax
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rax, %rbx
	jnb	.L2436
	.p2align 4,,10
	.p2align 3
.L2433:
	movq	16(%r9), %rsi
	leaq	(%rdx,%rbx,8), %rdx
	cmpq	24(%r9), %rsi
	je	.L2434
	movq	(%rdx), %rax
	addq	$1, %rbx
	movq	%rax, (%rsi)
	addq	$8, 16(%r9)
	movq	8(%r10), %rdx
	movq	16(%r10), %rax
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rbx, %rax
	ja	.L2433
.L2436:
	movq	16(%r15), %rcx
	orq	$2, %r9
	jmp	.L2410
	.p2align 4,,10
	.p2align 3
.L2434:
	movq	%r9, %rdi
	movq	%r10, -128(%rbp)
	addq	$1, %rbx
	movq	%r9, -120(%rbp)
	call	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_
	movq	-128(%rbp), %r10
	movq	-120(%rbp), %r9
	movq	8(%r10), %rdx
	movq	16(%r10), %rax
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rbx, %rax
	ja	.L2433
	jmp	.L2436
	.p2align 4,,10
	.p2align 3
.L2446:
	movq	16(%rcx), %rbx
	movq	24(%rcx), %rax
	subq	%rbx, %rax
	cmpq	$31, %rax
	jbe	.L2524
	leaq	32(%rbx), %rax
	movq	%rax, 16(%rcx)
.L2449:
	movq	%rcx, (%rbx)
	movq	$0, 8(%rbx)
	movq	$0, 16(%rbx)
	movq	$0, 24(%rbx)
	movq	8(%r10), %rdx
	movq	16(%r10), %r8
	subq	%rdx, %r8
	movq	%r8, %rax
	sarq	$3, %rax
	addq	$1, %rax
	cmpq	$268435455, %rax
	ja	.L2450
	xorl	%esi, %esi
	testq	%rax, %rax
	jne	.L2525
.L2451:
	xorl	%ecx, %ecx
	jmp	.L2458
	.p2align 4,,10
	.p2align 3
.L2526:
	movq	%rax, 0(%r13)
	movq	16(%rbx), %rax
	addq	$1, %rcx
	leaq	8(%rax), %r13
	movq	%r13, 16(%rbx)
.L2510:
	movq	8(%r10), %rdx
	movq	16(%r10), %rax
	movq	24(%rbx), %rsi
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	jbe	.L2454
.L2458:
	leaq	(%rdx,%rcx,8), %rdx
	movq	(%rdx), %rax
	cmpq	-96(%rbp), %rax
	ja	.L2454
	cmpq	%rsi, %r13
	jne	.L2526
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%rcx, -120(%rbp)
	movq	%r10, -112(%rbp)
	call	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_
	movq	-120(%rbp), %rcx
	movq	16(%rbx), %r13
	movq	-112(%rbp), %r10
	addq	$1, %rcx
	jmp	.L2510
.L2525:
	movq	16(%rcx), %r13
	movq	24(%rcx), %rax
	leaq	15(%r8), %rsi
	andq	$-8, %rsi
	subq	%r13, %rax
	cmpq	%rax, %rsi
	ja	.L2527
	addq	%r13, %rsi
	movq	%rsi, 16(%rcx)
.L2453:
	leaq	8(%r13,%r8), %rsi
	movq	%r13, 8(%rbx)
	movq	%r13, 16(%rbx)
	movq	%rsi, 24(%rbx)
	movq	8(%r10), %rdx
	cmpq	16(%r10), %rdx
	jne	.L2451
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L2454:
	cmpq	%rsi, %r13
	je	.L2459
	movq	-96(%rbp), %rax
	movq	%rax, 0(%r13)
	addq	$8, 16(%rbx)
	jmp	.L2460
	.p2align 4,,10
	.p2align 3
.L2528:
	movq	(%rdx), %rax
	addq	$1, %rcx
	movq	%rax, (%rsi)
	addq	$8, 16(%rbx)
.L2460:
	movq	8(%r10), %rdx
	movq	16(%r10), %rax
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rax, %rcx
	jnb	.L2464
.L2461:
	movq	16(%rbx), %rsi
	leaq	(%rdx,%rcx,8), %rdx
	cmpq	24(%rbx), %rsi
	jne	.L2528
	movq	%rbx, %rdi
	movq	%rcx, -120(%rbp)
	movq	%r10, -112(%rbp)
	call	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_
	movq	-112(%rbp), %r10
	movq	-120(%rbp), %rcx
	movq	8(%r10), %rdx
	movq	16(%r10), %rax
	addq	$1, %rcx
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	ja	.L2461
	jmp	.L2464
	.p2align 4,,10
	.p2align 3
.L2515:
	movl	$528, %esi
	movq	%rcx, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L2472
	.p2align 4,,10
	.p2align 3
.L2415:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	leaq	-88(%rbp), %rdx
	movq	%r11, -120(%rbp)
	call	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_
	movq	-120(%rbp), %r11
	leaq	-80(%rbp), %rsi
	movq	%rbx, %rdi
	movq	%r11, -80(%rbp)
	call	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE12emplace_backIJS0_EEEvDpOT_
	jmp	.L2417
	.p2align 4,,10
	.p2align 3
.L2443:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	leaq	-96(%rbp), %rdx
	movq	%r9, -112(%rbp)
	call	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_
	movq	-112(%rbp), %r9
	leaq	-80(%rbp), %rsi
	movq	%rbx, %rdi
	movq	%r9, -80(%rbp)
	call	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE12emplace_backIJS0_EEEvDpOT_
	jmp	.L2464
.L2431:
	movq	%r9, %rdi
	leaq	-88(%rbp), %rdx
	movq	%r10, -128(%rbp)
	movq	%r9, -120(%rbp)
	call	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_
	movq	-128(%rbp), %r10
	movq	-120(%rbp), %r9
	jmp	.L2432
.L2459:
	leaq	-96(%rbp), %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%rcx, -120(%rbp)
	movq	%r10, -112(%rbp)
	call	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_
	movq	-120(%rbp), %rcx
	movq	-112(%rbp), %r10
	jmp	.L2460
.L2513:
	movq	%rcx, %rdi
	movl	$32, %esi
	movq	%r11, -128(%rbp)
	movq	%rcx, -120(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-120(%rbp), %rcx
	movq	-128(%rbp), %r11
	movq	%rax, %rbx
	jmp	.L2414
.L2514:
	movq	%rcx, %rdi
	movl	$32, %esi
	movq	%r9, -120(%rbp)
	movq	%rcx, -112(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-112(%rbp), %rcx
	movq	-120(%rbp), %r9
	movq	%rax, %rbx
	jmp	.L2442
.L2444:
	leaq	-96(%rbp), %rdx
	movq	%rbx, %rdi
	call	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_
	jmp	.L2464
.L2416:
	leaq	-88(%rbp), %rdx
	movq	%rbx, %rdi
	call	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_
	jmp	.L2417
.L2524:
	movq	%rcx, %rdi
	movl	$32, %esi
	movq	%r10, -120(%rbp)
	movq	%rcx, -112(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-112(%rbp), %rcx
	movq	-120(%rbp), %r10
	movq	%rax, %rbx
	jmp	.L2449
.L2520:
	movq	%rcx, %rdi
	movl	$32, %esi
	movq	%r10, -128(%rbp)
	movq	%rcx, -120(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-120(%rbp), %rcx
	movq	-128(%rbp), %r10
	movq	%rax, %r9
	jmp	.L2421
.L2519:
	movl	$528, %esi
	movq	%rcx, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L2469
.L2523:
	movq	%r11, %rsi
	movq	%rcx, %rdi
	movq	%r9, -128(%rbp)
	movq	%r10, -120(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-120(%rbp), %r10
	movq	-128(%rbp), %r9
	movq	%rax, %rsi
	jmp	.L2425
.L2527:
	movq	%rcx, %rdi
	movq	%r8, -120(%rbp)
	movq	%r10, -112(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-112(%rbp), %r10
	movq	-120(%rbp), %r8
	movq	%rax, %r13
	jmp	.L2453
.L2516:
	call	__stack_chk_fail@PLT
.L2450:
	leaq	.LC14(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE19424:
	.size	_ZN2v88internal8compiler15LoadElimination31ReduceTransitionAndStoreElementEPNS1_4NodeE, .-_ZN2v88internal8compiler15LoadElimination31ReduceTransitionAndStoreElementEPNS1_4NodeE
	.section	.text._ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPKPmSt6vectorIS2_N2v88internal13ZoneAllocatorIS2_EEEEENS0_5__ops16_Iter_equals_valIS3_EEET_SF_SF_T0_St26random_access_iterator_tag,"axG",@progbits,_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPKPmSt6vectorIS2_N2v88internal13ZoneAllocatorIS2_EEEEENS0_5__ops16_Iter_equals_valIS3_EEET_SF_SF_T0_St26random_access_iterator_tag,comdat
	.p2align 4
	.weak	_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPKPmSt6vectorIS2_N2v88internal13ZoneAllocatorIS2_EEEEENS0_5__ops16_Iter_equals_valIS3_EEET_SF_SF_T0_St26random_access_iterator_tag
	.type	_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPKPmSt6vectorIS2_N2v88internal13ZoneAllocatorIS2_EEEEENS0_5__ops16_Iter_equals_valIS3_EEET_SF_SF_T0_St26random_access_iterator_tag, @function
_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPKPmSt6vectorIS2_N2v88internal13ZoneAllocatorIS2_EEEEENS0_5__ops16_Iter_equals_valIS3_EEET_SF_SF_T0_St26random_access_iterator_tag:
.LFB23809:
	.cfi_startproc
	endbr64
	movq	%rsi, %r8
	subq	%rdi, %r8
	movq	%r8, %rax
	sarq	$5, %r8
	sarq	$3, %rax
	testq	%r8, %r8
	jle	.L2530
	salq	$5, %r8
	movq	(%rdx), %rcx
	addq	%rdi, %r8
	jmp	.L2536
	.p2align 4,,10
	.p2align 3
.L2531:
	cmpq	8(%rdi), %rcx
	je	.L2548
	cmpq	16(%rdi), %rcx
	je	.L2549
	cmpq	24(%rdi), %rcx
	je	.L2550
	addq	$32, %rdi
	cmpq	%r8, %rdi
	je	.L2551
.L2536:
	cmpq	(%rdi), %rcx
	jne	.L2531
.L2547:
	movq	%rdi, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L2551:
	movq	%rsi, %rax
	subq	%rdi, %rax
	sarq	$3, %rax
.L2530:
	cmpq	$2, %rax
	je	.L2537
	cmpq	$3, %rax
	je	.L2538
	cmpq	$1, %rax
	je	.L2552
.L2540:
	movq	%rsi, %rax
	ret
.L2538:
	movq	(%rdx), %rax
	cmpq	%rax, (%rdi)
	je	.L2547
	addq	$8, %rdi
	jmp	.L2541
.L2552:
	movq	(%rdx), %rax
.L2542:
	cmpq	(%rdi), %rax
	jne	.L2540
	jmp	.L2547
.L2537:
	movq	(%rdx), %rax
.L2541:
	cmpq	%rax, (%rdi)
	je	.L2547
	addq	$8, %rdi
	jmp	.L2542
	.p2align 4,,10
	.p2align 3
.L2548:
	leaq	8(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L2549:
	leaq	16(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L2550:
	leaq	24(%rdi), %rax
	ret
	.cfi_endproc
.LFE23809:
	.size	_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPKPmSt6vectorIS2_N2v88internal13ZoneAllocatorIS2_EEEEENS0_5__ops16_Iter_equals_valIS3_EEET_SF_SF_T0_St26random_access_iterator_tag, .-_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPKPmSt6vectorIS2_N2v88internal13ZoneAllocatorIS2_EEEEENS0_5__ops16_Iter_equals_valIS3_EEET_SF_SF_T0_St26random_access_iterator_tag
	.section	.text._ZN2v88internal8compiler15LoadElimination15ReduceCheckMapsEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15LoadElimination15ReduceCheckMapsEPNS1_4NodeE
	.type	_ZN2v88internal8compiler15LoadElimination15ReduceCheckMapsEPNS1_4NodeE, @function
_ZN2v88internal8compiler15LoadElimination15ReduceCheckMapsEPNS1_4NodeE:
.LFB19419:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler21CheckMapsParametersOfEPKNS1_8OperatorE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	24(%r15), %rcx
	movl	20(%rax), %edx
	movq	%rax, %rbx
	movq	32(%r15), %rax
	subq	%rcx, %rax
	andl	$16777215, %edx
	sarq	$3, %rax
	cmpq	%rax, %rdx
	jnb	.L2554
	movq	(%rcx,%rdx,8), %r8
	testq	%r8, %r8
	je	.L2554
	movq	520(%r8), %rsi
	testq	%rsi, %rsi
	je	.L2557
	movq	%r13, %rdi
	movq	%r8, -72(%rbp)
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	movq	-72(%rbp), %r8
	movq	24(%rsi), %rdx
	leaq	16(%rsi), %rdi
	testq	%rdx, %rdx
	je	.L2557
	movq	%rdi, %rcx
	jmp	.L2558
	.p2align 4,,10
	.p2align 3
.L2584:
	movq	%rdx, %rcx
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L2559
.L2558:
	cmpq	%rax, 32(%rdx)
	jnb	.L2584
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L2558
.L2559:
	cmpq	%rcx, %rdi
	je	.L2557
	cmpq	%rax, 32(%rcx)
	ja	.L2557
	movq	40(%rcx), %r9
	movq	8(%r14), %rcx
	cmpq	%r9, %rcx
	je	.L2563
	cmpq	$1, %rcx
	je	.L2557
	cmpq	$1, %r9
	je	.L2563
	testb	$3, %cl
	je	.L2557
	subq	$2, %rcx
	testb	$3, %r9b
	je	.L2585
	movq	6(%r9), %rdx
	subq	$2, %r9
	cmpq	16(%r9), %rdx
	je	.L2563
	xorl	%r10d, %r10d
	jmp	.L2565
	.p2align 4,,10
	.p2align 3
.L2586:
	movq	-104(%rbp), %r9
	movq	-88(%rbp), %r10
	movq	-96(%rbp), %rcx
	movq	8(%r9), %rdx
	movq	16(%r9), %rax
	addq	$1, %r10
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%r10, %rax
	jbe	.L2563
.L2565:
	movq	16(%rcx), %rsi
	movq	8(%rcx), %rdi
	leaq	(%rdx,%r10,8), %rdx
	movq	%r8, -80(%rbp)
	movq	%r9, -104(%rbp)
	movq	%rsi, -72(%rbp)
	movq	%rcx, -96(%rbp)
	movq	%r10, -88(%rbp)
	call	_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPKPmSt6vectorIS2_N2v88internal13ZoneAllocatorIS2_EEEEENS0_5__ops16_Iter_equals_valIS3_EEET_SF_SF_T0_St26random_access_iterator_tag
	movq	-72(%rbp), %rsi
	movq	-80(%rbp), %r8
	cmpq	%rax, %rsi
	jne	.L2586
	.p2align 4,,10
	.p2align 3
.L2557:
	movq	8(%r14), %rdx
	movq	16(%r15), %rcx
	movq	%r13, %rsi
	movq	%r8, %rdi
	call	_ZNK2v88internal8compiler15LoadElimination13AbstractState7SetMapsEPNS1_4NodeENS0_13ZoneHandleSetINS0_3MapEEEPNS0_4ZoneE
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler15LoadElimination11UpdateStateEPNS1_4NodeEPKNS2_13AbstractStateE
	jmp	.L2556
	.p2align 4,,10
	.p2align 3
.L2554:
	xorl	%eax, %eax
.L2556:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L2587
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2585:
	.cfi_restore_state
	movq	16(%rcx), %rsi
	movq	8(%rcx), %rdi
	leaq	-64(%rbp), %rdx
	movq	%r8, -80(%rbp)
	movq	%r9, -64(%rbp)
	movq	%rsi, -72(%rbp)
	call	_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPKPmSt6vectorIS2_N2v88internal13ZoneAllocatorIS2_EEEEENS0_5__ops16_Iter_equals_valIS3_EEET_SF_SF_T0_St26random_access_iterator_tag
	movq	-72(%rbp), %rsi
	movq	-80(%rbp), %r8
	cmpq	%rax, %rsi
	je	.L2557
	.p2align 4,,10
	.p2align 3
.L2563:
	movq	%rbx, %rax
	jmp	.L2556
.L2587:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19419:
	.size	_ZN2v88internal8compiler15LoadElimination15ReduceCheckMapsEPNS1_4NodeE, .-_ZN2v88internal8compiler15LoadElimination15ReduceCheckMapsEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler15LoadElimination14ReduceMapGuardEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15LoadElimination14ReduceMapGuardEPNS1_4NodeE
	.type	_ZN2v88internal8compiler15LoadElimination14ReduceMapGuardEPNS1_4NodeE, @function
_ZN2v88internal8compiler15LoadElimination14ReduceMapGuardEPNS1_4NodeE:
.LFB19418:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14MapGuardMapsOfEPKNS1_8OperatorE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	24(%r15), %rcx
	movl	20(%rax), %edx
	movq	%rax, %rbx
	movq	32(%r15), %rax
	subq	%rcx, %rax
	andl	$16777215, %edx
	sarq	$3, %rax
	cmpq	%rax, %rdx
	jnb	.L2589
	movq	(%rcx,%rdx,8), %r8
	testq	%r8, %r8
	je	.L2589
	movq	520(%r8), %rsi
	testq	%rsi, %rsi
	je	.L2592
	movq	%r13, %rdi
	movq	%r8, -72(%rbp)
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	movq	-72(%rbp), %r8
	movq	24(%rsi), %rdx
	leaq	16(%rsi), %rdi
	testq	%rdx, %rdx
	je	.L2592
	movq	%rdi, %rcx
	jmp	.L2593
	.p2align 4,,10
	.p2align 3
.L2619:
	movq	%rdx, %rcx
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L2594
.L2593:
	cmpq	%rax, 32(%rdx)
	jnb	.L2619
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L2593
.L2594:
	cmpq	%rcx, %rdi
	je	.L2592
	cmpq	%rax, 32(%rcx)
	ja	.L2592
	movq	40(%rcx), %r9
	movq	(%r14), %rcx
	cmpq	%r9, %rcx
	je	.L2598
	cmpq	$1, %rcx
	je	.L2592
	cmpq	$1, %r9
	je	.L2598
	testb	$3, %cl
	je	.L2592
	subq	$2, %rcx
	testb	$3, %r9b
	je	.L2620
	movq	6(%r9), %rdx
	subq	$2, %r9
	cmpq	16(%r9), %rdx
	je	.L2598
	xorl	%r10d, %r10d
	jmp	.L2600
	.p2align 4,,10
	.p2align 3
.L2621:
	movq	-104(%rbp), %r9
	movq	-88(%rbp), %r10
	movq	-96(%rbp), %rcx
	movq	8(%r9), %rdx
	movq	16(%r9), %rax
	addq	$1, %r10
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%r10, %rax
	jbe	.L2598
.L2600:
	movq	16(%rcx), %rsi
	movq	8(%rcx), %rdi
	leaq	(%rdx,%r10,8), %rdx
	movq	%r8, -80(%rbp)
	movq	%r9, -104(%rbp)
	movq	%rsi, -72(%rbp)
	movq	%rcx, -96(%rbp)
	movq	%r10, -88(%rbp)
	call	_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPKPmSt6vectorIS2_N2v88internal13ZoneAllocatorIS2_EEEEENS0_5__ops16_Iter_equals_valIS3_EEET_SF_SF_T0_St26random_access_iterator_tag
	movq	-72(%rbp), %rsi
	movq	-80(%rbp), %r8
	cmpq	%rax, %rsi
	jne	.L2621
	.p2align 4,,10
	.p2align 3
.L2592:
	movq	(%r14), %rdx
	movq	16(%r15), %rcx
	movq	%r13, %rsi
	movq	%r8, %rdi
	call	_ZNK2v88internal8compiler15LoadElimination13AbstractState7SetMapsEPNS1_4NodeENS0_13ZoneHandleSetINS0_3MapEEEPNS0_4ZoneE
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler15LoadElimination11UpdateStateEPNS1_4NodeEPKNS2_13AbstractStateE
	jmp	.L2591
	.p2align 4,,10
	.p2align 3
.L2589:
	xorl	%eax, %eax
.L2591:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L2622
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2620:
	.cfi_restore_state
	movq	16(%rcx), %rsi
	movq	8(%rcx), %rdi
	leaq	-64(%rbp), %rdx
	movq	%r8, -80(%rbp)
	movq	%r9, -64(%rbp)
	movq	%rsi, -72(%rbp)
	call	_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPKPmSt6vectorIS2_N2v88internal13ZoneAllocatorIS2_EEEEENS0_5__ops16_Iter_equals_valIS3_EEET_SF_SF_T0_St26random_access_iterator_tag
	movq	-72(%rbp), %rsi
	movq	-80(%rbp), %r8
	cmpq	%rax, %rsi
	je	.L2592
	.p2align 4,,10
	.p2align 3
.L2598:
	movq	%rbx, %rax
	jmp	.L2591
.L2622:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19418:
	.size	_ZN2v88internal8compiler15LoadElimination14ReduceMapGuardEPNS1_4NodeE, .-_ZN2v88internal8compiler15LoadElimination14ReduceMapGuardEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler15LoadElimination32ReduceEnsureWritableFastElementsEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15LoadElimination32ReduceEnsureWritableFastElementsEPNS1_4NodeE
	.type	_ZN2v88internal8compiler15LoadElimination32ReduceEnsureWritableFastElementsEPNS1_4NodeE, @function
_ZN2v88internal8compiler15LoadElimination32ReduceEnsureWritableFastElementsEPNS1_4NodeE:
.LFB19421:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	xorl	%esi, %esi
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r14, %rdi
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movl	$1, %esi
	movq	%r14, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	24(%r12), %rcx
	movl	20(%rax), %edx
	movq	%rax, %rbx
	movq	32(%r12), %rax
	subq	%rcx, %rax
	andl	$16777215, %edx
	sarq	$3, %rax
	cmpq	%rax, %rdx
	jnb	.L2624
	movq	(%rcx,%rdx,8), %r15
	testq	%r15, %r15
	je	.L2624
	movq	48(%r12), %rax
	movq	520(%r15), %rsi
	movq	360(%rax), %r8
	addq	$152, %r8
	testq	%rsi, %rsi
	je	.L2627
	movq	%r13, %rdi
	movq	%r8, -112(%rbp)
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	movq	-112(%rbp), %r8
	movq	24(%rsi), %rdx
	leaq	16(%rsi), %rdi
	testq	%rdx, %rdx
	je	.L2627
	movq	%rdi, %rcx
	jmp	.L2628
	.p2align 4,,10
	.p2align 3
.L2660:
	movq	%rdx, %rcx
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L2629
.L2628:
	cmpq	%rax, 32(%rdx)
	jnb	.L2660
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L2628
.L2629:
	cmpq	%rcx, %rdi
	je	.L2627
	cmpq	%rax, 32(%rcx)
	ja	.L2627
	movq	40(%rcx), %rcx
	cmpq	%rcx, %r8
	je	.L2632
	cmpq	$1, %r8
	je	.L2627
	cmpq	$1, %rcx
	je	.L2632
	testb	$3, %r8b
	je	.L2627
	leaq	-2(%r8), %r11
	testb	$3, %cl
	je	.L2661
	movq	6(%rcx), %rdx
	subq	$2, %rcx
	cmpq	16(%rcx), %rdx
	je	.L2632
	xorl	%r10d, %r10d
	jmp	.L2634
	.p2align 4,,10
	.p2align 3
.L2662:
	movq	-144(%rbp), %rcx
	movq	-128(%rbp), %r10
	movq	-136(%rbp), %r11
	movq	8(%rcx), %rdx
	movq	16(%rcx), %rax
	addq	$1, %r10
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%r10, %rax
	jbe	.L2632
.L2634:
	movq	16(%r11), %rsi
	movq	8(%r11), %rdi
	leaq	(%rdx,%r10,8), %rdx
	movq	%r8, -120(%rbp)
	movq	%rcx, -144(%rbp)
	movq	%rsi, -112(%rbp)
	movq	%r11, -136(%rbp)
	movq	%r10, -128(%rbp)
	call	_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPKPmSt6vectorIS2_N2v88internal13ZoneAllocatorIS2_EEEEENS0_5__ops16_Iter_equals_valIS3_EEET_SF_SF_T0_St26random_access_iterator_tag
	movq	-112(%rbp), %rsi
	movq	-120(%rbp), %r8
	cmpq	%rax, %rsi
	jne	.L2662
	.p2align 4,,10
	.p2align 3
.L2627:
	movq	16(%r12), %rcx
	movq	%r15, %rdi
	movq	%r8, %rdx
	movq	%r14, %rsi
	call	_ZNK2v88internal8compiler15LoadElimination13AbstractState7SetMapsEPNS1_4NodeENS0_13ZoneHandleSetINS0_3MapEEEPNS0_4ZoneE
	movq	$0, -80(%rbp)
	movq	16(%r12), %r15
	movq	%rax, %xmm0
	movq	%rax, %r13
	movhps	-104(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.L2635
	leaq	-96(%rbp), %rsi
	movq	%r15, %rcx
	xorl	%edx, %edx
	call	_ZNK2v88internal8compiler15LoadElimination13AbstractField4KillERKNS2_14AliasStateInfoENS0_11MaybeHandleINS0_4NameEEEPNS0_4ZoneE
	movq	%rax, %rbx
	cmpq	16(%r13), %rax
	je	.L2663
	movq	16(%r15), %rdx
	movq	24(%r15), %rax
	subq	%rdx, %rax
	cmpq	$527, %rax
	jbe	.L2664
	leaq	528(%rdx), %rax
	movq	%rax, 16(%r15)
.L2638:
	movq	%r13, %rsi
	movl	$66, %ecx
	movq	%rdx, %rdi
	movq	%rdx, %r13
	rep movsq
	movq	%rbx, 16(%rdx)
	movq	16(%r12), %r15
.L2635:
	pushq	$0
	movq	-104(%rbp), %rsi
	pxor	%xmm0, %xmm0
	movq	%r13, %rdi
	pushq	$0
	movq	%r15, %rcx
	movabsq	$8589934593, %rdx
	movq	%r14, -96(%rbp)
	movb	$7, -88(%rbp)
	pushq	-88(%rbp)
	pushq	-96(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZNK2v88internal8compiler15LoadElimination13AbstractState8AddFieldEPNS1_4NodeENS2_10IndexRangeENS2_9FieldInfoEPNS0_4ZoneE
	addq	$32, %rsp
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler15LoadElimination11UpdateStateEPNS1_4NodeEPKNS2_13AbstractStateE
	jmp	.L2626
	.p2align 4,,10
	.p2align 3
.L2624:
	xorl	%eax, %eax
.L2626:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L2665
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2663:
	.cfi_restore_state
	movq	16(%r12), %r15
	jmp	.L2635
	.p2align 4,,10
	.p2align 3
.L2661:
	movq	16(%r11), %rsi
	movq	%rcx, -96(%rbp)
	leaq	-96(%rbp), %rdx
	movq	8(%r11), %rdi
	movq	%r8, -120(%rbp)
	movq	%rsi, -112(%rbp)
	call	_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPKPmSt6vectorIS2_N2v88internal13ZoneAllocatorIS2_EEEEENS0_5__ops16_Iter_equals_valIS3_EEET_SF_SF_T0_St26random_access_iterator_tag
	movq	-112(%rbp), %rsi
	movq	-120(%rbp), %r8
	cmpq	%rax, %rsi
	je	.L2627
	.p2align 4,,10
	.p2align 3
.L2632:
	movq	8(%r12), %rdi
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r13, %rax
	jmp	.L2626
	.p2align 4,,10
	.p2align 3
.L2664:
	movl	$528, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L2638
.L2665:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19421:
	.size	_ZN2v88internal8compiler15LoadElimination32ReduceEnsureWritableFastElementsEPNS1_4NodeE, .-_ZN2v88internal8compiler15LoadElimination32ReduceEnsureWritableFastElementsEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler15LoadElimination17ReduceCompareMapsEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15LoadElimination17ReduceCompareMapsEPNS1_4NodeE
	.type	_ZN2v88internal8compiler15LoadElimination17ReduceCompareMapsEPNS1_4NodeE, @function
_ZN2v88internal8compiler15LoadElimination17ReduceCompareMapsEPNS1_4NodeE:
.LFB19420:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler23CompareMapsParametersOfEPKNS1_8OperatorE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	24(%r14), %rcx
	movl	20(%rax), %edx
	movq	%rax, %r15
	movq	32(%r14), %rax
	subq	%rcx, %rax
	andl	$16777215, %edx
	sarq	$3, %rax
	cmpq	%rax, %rdx
	jnb	.L2667
	movq	(%rcx,%rdx,8), %r8
	testq	%r8, %r8
	je	.L2667
	movq	520(%r8), %rsi
	testq	%rsi, %rsi
	je	.L2670
	movq	%r13, %rdi
	movq	%r8, -72(%rbp)
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	movq	-72(%rbp), %r8
	movq	24(%rsi), %rdx
	leaq	16(%rsi), %rdi
	testq	%rdx, %rdx
	je	.L2670
	movq	%rdi, %rcx
	jmp	.L2671
	.p2align 4,,10
	.p2align 3
.L2697:
	movq	%rdx, %rcx
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L2672
.L2671:
	cmpq	%rax, 32(%rdx)
	jnb	.L2697
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L2671
.L2672:
	cmpq	%rcx, %rdi
	je	.L2670
	cmpq	%rax, 32(%rcx)
	ja	.L2670
	movq	40(%rcx), %r13
	movq	(%rbx), %rbx
	cmpq	%r13, %rbx
	je	.L2676
	cmpq	$1, %rbx
	je	.L2670
	cmpq	$1, %r13
	je	.L2676
	testb	$3, %bl
	je	.L2670
	subq	$2, %rbx
	testb	$3, %r13b
	je	.L2698
	subq	$2, %r13
	movq	8(%r13), %rdx
	cmpq	16(%r13), %rdx
	je	.L2676
	xorl	%ecx, %ecx
	jmp	.L2678
	.p2align 4,,10
	.p2align 3
.L2699:
	movq	8(%r13), %rdx
	movq	16(%r13), %rax
	movq	-88(%rbp), %rcx
	subq	%rdx, %rax
	addq	$1, %rcx
	sarq	$3, %rax
	cmpq	%rcx, %rax
	jbe	.L2676
.L2678:
	movq	16(%rbx), %rsi
	movq	8(%rbx), %rdi
	leaq	(%rdx,%rcx,8), %rdx
	movq	%r8, -80(%rbp)
	movq	%rcx, -88(%rbp)
	movq	%rsi, -72(%rbp)
	call	_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPKPmSt6vectorIS2_N2v88internal13ZoneAllocatorIS2_EEEEENS0_5__ops16_Iter_equals_valIS3_EEET_SF_SF_T0_St26random_access_iterator_tag
	movq	-72(%rbp), %rsi
	movq	-80(%rbp), %r8
	cmpq	%rax, %rsi
	jne	.L2699
	.p2align 4,,10
	.p2align 3
.L2670:
	movq	%r8, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler15LoadElimination11UpdateStateEPNS1_4NodeEPKNS2_13AbstractStateE
	jmp	.L2669
	.p2align 4,,10
	.p2align 3
.L2667:
	xorl	%eax, %eax
.L2669:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L2700
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2698:
	.cfi_restore_state
	movq	16(%rbx), %rcx
	movq	8(%rbx), %rdi
	leaq	-64(%rbp), %rdx
	movq	%r8, -80(%rbp)
	movq	%r13, -64(%rbp)
	movq	%rcx, %rsi
	movq	%rcx, -72(%rbp)
	call	_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPKPmSt6vectorIS2_N2v88internal13ZoneAllocatorIS2_EEEEENS0_5__ops16_Iter_equals_valIS3_EEET_SF_SF_T0_St26random_access_iterator_tag
	movq	-72(%rbp), %rcx
	movq	-80(%rbp), %r8
	cmpq	%rax, %rcx
	je	.L2670
	.p2align 4,,10
	.p2align 3
.L2676:
	movq	48(%r14), %rdi
	call	_ZN2v88internal8compiler7JSGraph12TrueConstantEv@PLT
	movq	8(%r14), %rdi
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%rax, %rbx
	movq	%r12, %rsi
	movq	(%rdi), %rax
	movq	%rbx, %rdx
	call	*32(%rax)
	movq	%rbx, %rax
	jmp	.L2669
.L2700:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19420:
	.size	_ZN2v88internal8compiler15LoadElimination17ReduceCompareMapsEPNS1_4NodeE, .-_ZN2v88internal8compiler15LoadElimination17ReduceCompareMapsEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler15LoadElimination28ReduceTransitionElementsKindEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15LoadElimination28ReduceTransitionElementsKindEPNS1_4NodeE
	.type	_ZN2v88internal8compiler15LoadElimination28ReduceTransitionElementsKindEPNS1_4NodeE, @function
_ZN2v88internal8compiler15LoadElimination28ReduceTransitionElementsKindEPNS1_4NodeE:
.LFB19423:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler20ElementsTransitionOfEPKNS1_8OperatorE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movzbl	(%rax), %ebx
	movq	%rcx, -112(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -120(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	24(%r13), %rcx
	movl	20(%rax), %edx
	movq	%rax, %r14
	movq	32(%r13), %rax
	subq	%rcx, %rax
	andl	$16777215, %edx
	sarq	$3, %rax
	cmpq	%rdx, %rax
	jbe	.L2702
	movq	(%rcx,%rdx,8), %r15
	testq	%r15, %r15
	je	.L2702
	testb	%bl, %bl
	je	.L2706
	cmpb	$1, %bl
	jne	.L2706
	movq	-112(%rbp), %rax
	movq	%r15, %xmm0
	movq	16(%r13), %r10
	movq	%r15, %rdx
	movhps	-104(%rbp), %xmm0
	movq	%rax, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	16(%r15), %rdi
	testq	%rdi, %rdi
	je	.L2707
	xorl	%edx, %edx
	leaq	-80(%rbp), %rsi
	movq	%r10, %rcx
	movq	%r10, -128(%rbp)
	call	_ZNK2v88internal8compiler15LoadElimination13AbstractField4KillERKNS2_14AliasStateInfoENS0_11MaybeHandleINS0_4NameEEEPNS0_4ZoneE
	movq	%r15, %rdx
	movq	%rax, %rbx
	cmpq	16(%r15), %rax
	je	.L2707
	movq	-128(%rbp), %r10
	movq	16(%r10), %rdx
	movq	24(%r10), %rax
	subq	%rdx, %rax
	cmpq	$527, %rax
	jbe	.L2885
	leaq	528(%rdx), %rax
	movq	%rax, 16(%r10)
.L2709:
	movl	$66, %ecx
	movq	%rdx, %rdi
	movq	%r15, %rsi
	rep movsq
	movq	%rbx, 16(%rdx)
.L2707:
	movq	%rdx, %r15
.L2706:
	movq	520(%r15), %r10
	testq	%r10, %r10
	je	.L2733
	movq	-104(%rbp), %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	movq	24(%r10), %rbx
	leaq	16(%r10), %rcx
	testq	%rbx, %rbx
	je	.L2711
	movq	%rcx, %rdx
	jmp	.L2712
	.p2align 4,,10
	.p2align 3
.L2886:
	movq	%rbx, %rdx
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L2713
.L2712:
	cmpq	%rax, 32(%rbx)
	jnb	.L2886
	movq	24(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L2712
.L2713:
	cmpq	%rdx, %rcx
	je	.L2711
	cmpq	%rax, 32(%rdx)
	jbe	.L2716
.L2711:
	movq	16(%r13), %r14
	movq	-112(%rbp), %rax
	movq	%r15, %xmm0
	movq	%r10, %rdi
	movhps	-104(%rbp), %xmm0
	leaq	-80(%rbp), %rsi
	movq	%r14, %rdx
	movq	%rax, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZNK2v88internal8compiler15LoadElimination12AbstractMaps4KillERKNS2_14AliasStateInfoEPNS0_4ZoneE
	movq	%rax, %rbx
	cmpq	520(%r15), %rax
	je	.L2733
	movq	16(%r14), %rdx
	movq	24(%r14), %rax
	subq	%rdx, %rax
	cmpq	$527, %rax
	jbe	.L2887
	leaq	528(%rdx), %rax
	movq	%rax, 16(%r14)
.L2807:
	movq	%r15, %rsi
	movl	$66, %ecx
	movq	%rdx, %rdi
	movq	%rdx, %r15
	rep movsq
	movq	%rbx, 520(%rdx)
	.p2align 4,,10
	.p2align 3
.L2733:
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler15LoadElimination11UpdateStateEPNS1_4NodeEPKNS2_13AbstractStateE
	jmp	.L2704
	.p2align 4,,10
	.p2align 3
.L2702:
	xorl	%eax, %eax
.L2704:
	movq	-56(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L2888
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2716:
	.cfi_restore_state
	movq	40(%rdx), %rax
	movq	-120(%rbp), %rdi
	movq	%rax, -128(%rbp)
	movq	%rdi, -136(%rbp)
	cmpq	%rax, %rdi
	je	.L2722
	cmpq	$1, %rdi
	je	.L2719
	cmpq	$1, %rax
	je	.L2722
	andl	$3, %eax
	movq	%rax, -144(%rbp)
	testb	$3, %dil
	je	.L2723
	leaq	-2(%rdi), %r10
	testq	%rax, %rax
	je	.L2889
	movq	-128(%rbp), %rax
	movq	6(%rax), %rdx
	leaq	-2(%rax), %r11
	cmpq	14(%rax), %rdx
	je	.L2722
	xorl	%ecx, %ecx
	movq	%r14, -176(%rbp)
	movq	%r10, %r14
	movq	%rbx, -152(%rbp)
	movq	%rcx, %rbx
	movq	%r12, -168(%rbp)
	movq	%r11, %r12
	movq	%r13, -160(%rbp)
	jmp	.L2725
	.p2align 4,,10
	.p2align 3
.L2891:
	movq	8(%r12), %rdx
	movq	16(%r12), %rax
	addq	$1, %rbx
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rbx, %rax
	jbe	.L2890
.L2725:
	movq	16(%r14), %r13
	movq	8(%r14), %rdi
	leaq	(%rdx,%rbx,8), %rdx
	movq	%r13, %rsi
	call	_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPKPmSt6vectorIS2_N2v88internal13ZoneAllocatorIS2_EEEEENS0_5__ops16_Iter_equals_valIS3_EEET_SF_SF_T0_St26random_access_iterator_tag
	cmpq	%rax, %r13
	jne	.L2891
	movq	-152(%rbp), %rbx
	movq	-160(%rbp), %r13
	movq	-168(%rbp), %r12
.L2723:
	movq	-112(%rbp), %rsi
	cmpq	%rsi, -128(%rbp)
	je	.L2732
.L2811:
	movq	-112(%rbp), %rax
	cmpq	$1, %rax
	je	.L2732
	cmpq	$0, -144(%rbp)
	je	.L2733
	movq	-128(%rbp), %rsi
	leaq	-2(%rsi), %r9
	testb	$3, %al
	je	.L2892
	movq	6(%rax), %rdx
	leaq	-2(%rax), %r10
	cmpq	14(%rax), %rdx
	je	.L2732
	xorl	%ecx, %ecx
	movq	%rbx, -168(%rbp)
	movq	%r9, %rbx
	movq	%r12, -160(%rbp)
	movq	%rcx, %r14
	movq	%r10, %r12
	movq	%r13, -152(%rbp)
	jmp	.L2736
	.p2align 4,,10
	.p2align 3
.L2894:
	movq	8(%r12), %rdx
	movq	16(%r12), %rax
	addq	$1, %r14
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%r14, %rax
	jbe	.L2893
.L2736:
	movq	16(%rbx), %r13
	movq	8(%rbx), %rdi
	leaq	(%rdx,%r14,8), %rdx
	movq	%r13, %rsi
	call	_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPKPmSt6vectorIS2_N2v88internal13ZoneAllocatorIS2_EEEEENS0_5__ops16_Iter_equals_valIS3_EEET_SF_SF_T0_St26random_access_iterator_tag
	cmpq	%rax, %r13
	jne	.L2894
	movq	-152(%rbp), %r13
	movq	-160(%rbp), %r12
	jmp	.L2733
	.p2align 4,,10
	.p2align 3
.L2889:
	movq	-128(%rbp), %rax
	movq	16(%r10), %rcx
	leaq	-80(%rbp), %rdx
	movq	%rax, -80(%rbp)
	movq	8(%r10), %rdi
	movq	%rcx, %rsi
	movq	%rcx, -152(%rbp)
	call	_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPKPmSt6vectorIS2_N2v88internal13ZoneAllocatorIS2_EEEEENS0_5__ops16_Iter_equals_valIS3_EEET_SF_SF_T0_St26random_access_iterator_tag
	movq	-152(%rbp), %rcx
	cmpq	%rax, %rcx
	je	.L2723
.L2722:
	movq	%r14, %rax
	jmp	.L2704
.L2885:
	movl	$528, %esi
	movq	%r10, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L2709
	.p2align 4,,10
	.p2align 3
.L2719:
	cmpq	%rax, -112(%rbp)
	je	.L2895
	cmpq	$1, -128(%rbp)
	je	.L2733
	andl	$3, %eax
	movq	%rax, -144(%rbp)
	jmp	.L2811
.L2895:
	movq	-112(%rbp), %rax
	andl	$3, %eax
	movq	%rax, -144(%rbp)
.L2732:
	movq	16(%r13), %r14
	movq	-144(%rbp), %rax
	movq	%r14, -152(%rbp)
	cmpq	$1, %rax
	je	.L2801
	movl	$1, %ecx
	xorl	%r8d, %r8d
	movq	%r15, -152(%rbp)
	movq	%rax, %r15
	movq	%rbx, -160(%rbp)
	movq	%rcx, %rbx
	movq	%r13, -168(%rbp)
	movq	-128(%rbp), %r13
	movq	%r12, -176(%rbp)
	movq	%r8, %r12
	jmp	.L2772
	.p2align 4,,10
	.p2align 3
.L2898:
	movq	6(%r13), %rcx
	movq	14(%r13), %rdx
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %r12
	jnb	.L2882
	movslq	%r12d, %rsi
	cmpq	%rsi, %rdx
	jbe	.L2896
	movq	(%rcx,%rsi,8), %rdx
	movq	%rdx, %rax
.L2741:
	cmpq	-112(%rbp), %rax
	je	.L2743
	movq	%rbx, %rcx
	movq	%rdx, -88(%rbp)
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L2815
	testq	%rcx, %rcx
	jne	.L2745
	cmpq	%rbx, %rdx
	je	.L2743
	movq	16(%r14), %rax
	movq	24(%r14), %rdx
	subq	%rax, %rdx
	cmpq	$31, %rdx
	jbe	.L2897
	leaq	32(%rax), %rdx
	movq	%rdx, 16(%r14)
.L2748:
	movq	%r14, (%rax)
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	cmpq	-88(%rbp), %rbx
	jnb	.L2749
	leaq	-80(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rax, -128(%rbp)
	movq	%rbx, -80(%rbp)
	call	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE12emplace_backIJS0_EEEvDpOT_
	movq	-128(%rbp), %rax
	movq	16(%rax), %rsi
	cmpq	24(%rax), %rsi
	je	.L2750
	movq	-88(%rbp), %rdx
	movq	%rdx, (%rsi)
	addq	$8, 16(%rax)
.L2751:
	orq	$2, %rax
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L2743:
	addq	$1, %r12
.L2772:
	testq	%r15, %r15
	jne	.L2898
	testq	%r12, %r12
	jne	.L2882
	movq	%r13, %rdx
	movq	%r13, %rax
	jmp	.L2741
	.p2align 4,,10
	.p2align 3
.L2815:
	movq	%rax, %rbx
	jmp	.L2743
	.p2align 4,,10
	.p2align 3
.L2745:
	movq	6(%rbx), %rsi
	movq	14(%rbx), %rcx
	leaq	-2(%rbx), %r9
	subq	%rsi, %rcx
	sarq	$3, %rcx
	je	.L2752
	xorl	%eax, %eax
	jmp	.L2753
	.p2align 4,,10
	.p2align 3
.L2899:
	addq	$1, %rax
	cmpq	%rcx, %rax
	je	.L2752
.L2753:
	cmpq	(%rsi,%rax,8), %rdx
	je	.L2743
	jnb	.L2899
.L2752:
	movq	16(%r14), %rbx
	movq	24(%r14), %rax
	subq	%rbx, %rax
	cmpq	$31, %rax
	jbe	.L2900
	leaq	32(%rbx), %rax
	movq	%rax, 16(%r14)
.L2755:
	movq	%r14, (%rbx)
	movq	$0, 8(%rbx)
	movq	$0, 16(%rbx)
	movq	$0, 24(%rbx)
	movq	8(%r9), %rdi
	movq	16(%r9), %rdx
	subq	%rdi, %rdx
	movq	%rdx, %rax
	sarq	$3, %rax
	addq	$1, %rax
	cmpq	$268435455, %rax
	ja	.L2786
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	testq	%rax, %rax
	jne	.L2901
.L2757:
	xorl	%eax, %eax
	jmp	.L2764
	.p2align 4,,10
	.p2align 3
.L2902:
	movq	%rdx, (%rsi)
	movq	16(%rbx), %rcx
	addq	$1, %rax
	leaq	8(%rcx), %rsi
	movq	%rsi, 16(%rbx)
.L2878:
	movq	8(%r9), %rdi
	movq	16(%r9), %rdx
	movq	24(%rbx), %r8
	subq	%rdi, %rdx
	sarq	$3, %rdx
	cmpq	%rax, %rdx
	jbe	.L2760
.L2764:
	leaq	(%rdi,%rax,8), %r10
	movq	(%r10), %rdx
	cmpq	-88(%rbp), %rdx
	ja	.L2760
	cmpq	%rsi, %r8
	jne	.L2902
	movq	%r8, %rsi
	movq	%r10, %rdx
	movq	%rbx, %rdi
	movq	%rax, -144(%rbp)
	movq	%r9, -128(%rbp)
	call	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_
	movq	-144(%rbp), %rax
	movq	16(%rbx), %rsi
	movq	-128(%rbp), %r9
	addq	$1, %rax
	jmp	.L2878
.L2901:
	movq	16(%r14), %rsi
	movq	24(%r14), %rcx
	leaq	15(%rdx), %rax
	andq	$-8, %rax
	subq	%rsi, %rcx
	cmpq	%rcx, %rax
	ja	.L2903
	addq	%rsi, %rax
	movq	%rax, 16(%r14)
.L2759:
	leaq	8(%rsi,%rdx), %r8
	movq	%rsi, 8(%rbx)
	movq	%rsi, 16(%rbx)
	movq	%r8, 24(%rbx)
	movq	8(%r9), %rdi
	cmpq	%rdi, 16(%r9)
	jne	.L2757
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L2760:
	cmpq	%r8, %rsi
	je	.L2765
	movq	-88(%rbp), %rdx
	movq	%rdx, (%rsi)
	addq	$8, 16(%rbx)
	.p2align 4,,10
	.p2align 3
.L2766:
	movq	8(%r9), %rcx
	movq	16(%r9), %rdx
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rax, %rdx
	jbe	.L2904
	movq	16(%rbx), %rsi
	leaq	(%rcx,%rax,8), %rdx
	cmpq	24(%rbx), %rsi
	je	.L2768
	movq	(%rdx), %rdx
	addq	$1, %rax
	movq	%rdx, (%rsi)
	addq	$8, 16(%rbx)
	jmp	.L2766
	.p2align 4,,10
	.p2align 3
.L2768:
	movq	%rbx, %rdi
	movq	%rax, -144(%rbp)
	movq	%r9, -128(%rbp)
	call	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_
	movq	-144(%rbp), %rax
	addq	$1, %rax
.L2881:
	movq	-128(%rbp), %r9
	jmp	.L2766
.L2882:
	movq	-168(%rbp), %r13
	movq	%rbx, %rcx
	movq	-120(%rbp), %rdi
	movq	%rcx, %rax
	movq	-152(%rbp), %r15
	movq	-160(%rbp), %rbx
	movq	16(%r13), %rsi
	andl	$3, %eax
	movq	%rdi, -96(%rbp)
	movq	-176(%rbp), %r12
	movq	%rsi, -152(%rbp)
	cmpq	$1, %rax
	je	.L2801
	testq	%rax, %rax
	jne	.L2775
	cmpq	%rdi, %rcx
	je	.L2776
	movq	24(%rsi), %rax
	movq	16(%rsi), %rbx
	movq	%rax, -120(%rbp)
	subq	%rbx, %rax
	cmpq	$31, %rax
	jbe	.L2905
	leaq	32(%rbx), %rax
	movq	%rax, 16(%rsi)
	movq	%rsi, %rax
.L2778:
	movq	%rax, (%rbx)
	movq	$0, 8(%rbx)
	movq	$0, 16(%rbx)
	movq	$0, 24(%rbx)
	cmpq	-96(%rbp), %rcx
	jnb	.L2779
	leaq	-80(%rbp), %rsi
	movq	%rbx, %rdi
	movq	%rcx, -80(%rbp)
	call	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE12emplace_backIJS0_EEEvDpOT_
	movq	16(%rbx), %rsi
	cmpq	24(%rbx), %rsi
	je	.L2780
	movq	-96(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 16(%rbx)
.L2781:
	movq	16(%r13), %rax
	orq	$2, %rbx
	movq	%rbx, -136(%rbp)
	movq	%rax, -152(%rbp)
.L2801:
	movq	-112(%rbp), %rax
	movq	%r15, %xmm0
	movhps	-104(%rbp), %xmm0
	movq	%rax, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	520(%r15), %rdi
	testq	%rdi, %rdi
	je	.L2802
	movq	-152(%rbp), %rdx
	leaq	-80(%rbp), %rsi
	call	_ZNK2v88internal8compiler15LoadElimination12AbstractMaps4KillERKNS2_14AliasStateInfoEPNS0_4ZoneE
	movq	%rax, %rbx
	cmpq	520(%r15), %rax
	jne	.L2803
	movq	16(%r13), %rax
	movq	%rax, -152(%rbp)
.L2802:
	movq	-152(%rbp), %rcx
	movq	-136(%rbp), %rdx
	movq	%r15, %rdi
	movq	-104(%rbp), %rsi
	call	_ZNK2v88internal8compiler15LoadElimination13AbstractState7SetMapsEPNS1_4NodeENS0_13ZoneHandleSetINS0_3MapEEEPNS0_4ZoneE
	movq	%rax, %r15
	jmp	.L2733
	.p2align 4,,10
	.p2align 3
.L2890:
	movq	-176(%rbp), %r14
	jmp	.L2722
	.p2align 4,,10
	.p2align 3
.L2904:
	orq	$2, %rbx
	jmp	.L2743
.L2749:
	movq	%rax, %rdi
	leaq	-88(%rbp), %rdx
	xorl	%esi, %esi
	movq	%rax, -128(%rbp)
	call	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_
	movq	-128(%rbp), %rax
	leaq	-80(%rbp), %rsi
	movq	%rbx, -80(%rbp)
	movq	%rax, %rdi
	call	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE12emplace_backIJS0_EEEvDpOT_
	movq	-128(%rbp), %rax
	jmp	.L2751
.L2892:
	movq	16(%r9), %rcx
	movq	%rax, -80(%rbp)
	leaq	-80(%rbp), %rdx
	movq	8(%r9), %rdi
	movq	%rcx, %rsi
	movq	%rcx, -152(%rbp)
	call	_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPKPmSt6vectorIS2_N2v88internal13ZoneAllocatorIS2_EEEEENS0_5__ops16_Iter_equals_valIS3_EEET_SF_SF_T0_St26random_access_iterator_tag
	movq	-152(%rbp), %rcx
	cmpq	%rax, %rcx
	je	.L2733
	jmp	.L2732
	.p2align 4,,10
	.p2align 3
.L2887:
	movl	$528, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L2807
.L2893:
	movq	-168(%rbp), %rbx
	movq	-152(%rbp), %r13
	movq	-160(%rbp), %r12
	jmp	.L2732
.L2803:
	movq	-152(%rbp), %rax
	movq	16(%rax), %rdx
	movq	24(%rax), %rax
	movq	%rax, -112(%rbp)
	subq	%rdx, %rax
	cmpq	$527, %rax
	jbe	.L2906
	movq	-152(%rbp), %rcx
	leaq	528(%rdx), %rax
	movq	%rax, 16(%rcx)
.L2805:
	movq	%r15, %rsi
	movl	$66, %ecx
	movq	%rdx, %rdi
	movq	%rdx, %r15
	rep movsq
	movq	%rbx, 520(%rdx)
	movq	16(%r13), %rax
	movq	%rax, -152(%rbp)
	jmp	.L2802
.L2765:
	leaq	-88(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%rax, -144(%rbp)
	movq	%r9, -128(%rbp)
	call	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_
	movq	-144(%rbp), %rax
	jmp	.L2881
.L2897:
	movl	$32, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L2748
.L2750:
	movq	%rax, %rdi
	leaq	-88(%rbp), %rdx
	movq	%rax, -128(%rbp)
	call	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_
	movq	-128(%rbp), %rax
	jmp	.L2751
.L2900:
	movl	$32, %esi
	movq	%r14, %rdi
	movq	%r9, -128(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-128(%rbp), %r9
	movq	%rax, %rbx
	jmp	.L2755
.L2888:
	call	__stack_chk_fail@PLT
.L2906:
	movq	-152(%rbp), %rdi
	movl	$528, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L2805
.L2903:
	movq	%rax, %rsi
	movq	%r14, %rdi
	movq	%rdx, -144(%rbp)
	movq	%r9, -128(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-128(%rbp), %r9
	movq	-144(%rbp), %rdx
	movq	%rax, %rsi
	jmp	.L2759
.L2786:
	leaq	.LC14(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L2896:
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2775:
	movq	6(%rcx), %rsi
	movq	14(%rcx), %rdx
	leaq	-2(%rcx), %r9
	subq	%rsi, %rdx
	sarq	$3, %rdx
	je	.L2782
	xorl	%eax, %eax
	jmp	.L2783
	.p2align 4,,10
	.p2align 3
.L2907:
	ja	.L2782
	addq	$1, %rax
	cmpq	%rdx, %rax
	je	.L2782
.L2783:
	cmpq	%rdi, (%rsi,%rax,8)
	jne	.L2907
.L2776:
	movq	%rcx, -136(%rbp)
	jmp	.L2801
.L2782:
	movq	-152(%rbp), %rax
	movq	16(%rax), %rcx
	movq	24(%rax), %rax
	movq	%rax, -120(%rbp)
	subq	%rcx, %rax
	cmpq	$31, %rax
	jbe	.L2908
	movq	-152(%rbp), %rsi
	leaq	32(%rcx), %rax
	movq	%rax, 16(%rsi)
	movq	%rsi, %rax
.L2785:
	movq	%rax, (%rcx)
	movq	$0, 8(%rcx)
	movq	$0, 16(%rcx)
	movq	$0, 24(%rcx)
	movq	8(%r9), %rdx
	movq	16(%r9), %r14
	subq	%rdx, %r14
	movq	%r14, %rax
	sarq	$3, %rax
	addq	$1, %rax
	cmpq	$268435455, %rax
	ja	.L2786
	xorl	%esi, %esi
	testq	%rax, %rax
	jne	.L2909
.L2787:
	movq	%rbx, %r8
	xorl	%r14d, %r14d
	movq	%r9, %rbx
	jmp	.L2794
	.p2align 4,,10
	.p2align 3
.L2910:
	movq	%rax, (%r8)
	movq	16(%rcx), %rax
	leaq	8(%rax), %r8
	movq	%r8, 16(%rcx)
.L2884:
	movq	8(%rbx), %rdx
	movq	16(%rbx), %rax
	addq	$1, %r14
	movq	24(%rcx), %rsi
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%r14, %rax
	jbe	.L2875
.L2794:
	leaq	(%rdx,%r14,8), %rdx
	movq	(%rdx), %rax
	cmpq	-96(%rbp), %rax
	ja	.L2875
	cmpq	%rsi, %r8
	jne	.L2910
	movq	%r8, %rsi
	movq	%rcx, %rdi
	movq	%rcx, -120(%rbp)
	call	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_
	movq	-120(%rbp), %rcx
	movq	16(%rcx), %r8
	jmp	.L2884
.L2875:
	movq	%rbx, %r9
	movq	%r8, %rbx
.L2790:
	cmpq	%rsi, %rbx
	je	.L2795
	movq	-96(%rbp), %rax
	movq	%rax, (%rbx)
	addq	$8, 16(%rcx)
.L2796:
	movq	8(%r9), %rdx
	movq	16(%r9), %rax
	movq	%r9, %rbx
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%r14, %rax
	jbe	.L2800
	.p2align 4,,10
	.p2align 3
.L2797:
	movq	16(%rcx), %rsi
	leaq	(%rdx,%r14,8), %rdx
	cmpq	24(%rcx), %rsi
	je	.L2798
	movq	(%rdx), %rax
	addq	$1, %r14
	movq	%rax, (%rsi)
	addq	$8, 16(%rcx)
	movq	8(%rbx), %rdx
	movq	16(%rbx), %rax
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%r14, %rax
	ja	.L2797
.L2800:
	movq	16(%r13), %rax
	orq	$2, %rcx
	movq	%rcx, -136(%rbp)
	movq	%rax, -152(%rbp)
	jmp	.L2801
.L2798:
	movq	%rcx, %rdi
	movq	%rcx, -120(%rbp)
	addq	$1, %r14
	call	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_
	movq	8(%rbx), %rdx
	movq	16(%rbx), %rax
	movq	-120(%rbp), %rcx
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%r14, %rax
	ja	.L2797
	jmp	.L2800
.L2779:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	leaq	-96(%rbp), %rdx
	movq	%rcx, -120(%rbp)
	call	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_
	movq	-120(%rbp), %rcx
	leaq	-80(%rbp), %rsi
	movq	%rbx, %rdi
	movq	%rcx, -80(%rbp)
	call	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE12emplace_backIJS0_EEEvDpOT_
	jmp	.L2781
.L2909:
	movq	-152(%rbp), %rax
	leaq	15(%r14), %rsi
	andq	$-8, %rsi
	movq	16(%rax), %rbx
	movq	24(%rax), %rax
	movq	%rax, -120(%rbp)
	subq	%rbx, %rax
	cmpq	%rax, %rsi
	ja	.L2911
	movq	-152(%rbp), %rax
	addq	%rbx, %rsi
	movq	%rsi, 16(%rax)
.L2789:
	leaq	8(%rbx,%r14), %rsi
	movq	%rbx, 8(%rcx)
	movq	%rbx, 16(%rcx)
	movq	%rsi, 24(%rcx)
	movq	8(%r9), %rdx
	cmpq	16(%r9), %rdx
	jne	.L2787
	xorl	%r14d, %r14d
	jmp	.L2790
.L2795:
	movq	%rcx, %rdi
	leaq	-96(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r9, -128(%rbp)
	movq	%rcx, -120(%rbp)
	call	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_
	movq	-128(%rbp), %r9
	movq	-120(%rbp), %rcx
	jmp	.L2796
.L2905:
	movq	-152(%rbp), %rdi
	movl	$32, %esi
	movq	%rcx, -120(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-120(%rbp), %rcx
	movq	%rax, %rbx
	movq	-152(%rbp), %rax
	jmp	.L2778
.L2908:
	movq	-152(%rbp), %rdi
	movl	$32, %esi
	movq	%r9, -120(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-120(%rbp), %r9
	movq	%rax, %rcx
	movq	-152(%rbp), %rax
	jmp	.L2785
.L2780:
	leaq	-96(%rbp), %rdx
	movq	%rbx, %rdi
	call	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_
	jmp	.L2781
.L2911:
	movq	-152(%rbp), %rdi
	movq	%rcx, -128(%rbp)
	movq	%r9, -120(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-120(%rbp), %r9
	movq	-128(%rbp), %rcx
	movq	%rax, %rbx
	jmp	.L2789
	.cfi_endproc
.LFE19423:
	.size	_ZN2v88internal8compiler15LoadElimination28ReduceTransitionElementsKindEPNS1_4NodeE, .-_ZN2v88internal8compiler15LoadElimination28ReduceTransitionElementsKindEPNS1_4NodeE
	.section	.rodata._ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJS4_EEEvDpOT_.str1.8,"aMS",@progbits,1
	.align 8
.LC15:
	.string	"cannot create std::deque larger than max_size()"
	.section	.text._ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJS4_EEEvDpOT_,"axG",@progbits,_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJS4_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJS4_EEEvDpOT_
	.type	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJS4_EEEvDpOT_, @function
_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJS4_EEEvDpOT_:
.LFB23873:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	88(%rdi), %r13
	movq	56(%rdi), %rsi
	movq	64(%rbx), %rdx
	subq	72(%rbx), %rdx
	movq	%r13, %r14
	movq	%rdx, %rcx
	subq	%rsi, %r14
	sarq	$3, %rcx
	movq	%r14, %rdi
	sarq	$3, %rdi
	leaq	-1(%rdi), %rax
	salq	$6, %rax
	leaq	(%rcx,%rax), %rdx
	movq	48(%rbx), %rax
	subq	32(%rbx), %rax
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpq	$268435455, %rax
	je	.L2930
	movq	16(%rbx), %rcx
	movq	24(%rbx), %rdx
	movq	%r13, %rax
	subq	%rcx, %rax
	movq	%rdx, %r8
	sarq	$3, %rax
	subq	%rax, %r8
	cmpq	$1, %r8
	jbe	.L2931
.L2914:
	movq	8(%rbx), %rax
	testq	%rax, %rax
	je	.L2922
	cmpq	$63, 8(%rax)
	ja	.L2932
.L2922:
	movq	(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L2933
	leaq	512(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L2923:
	movq	%rax, 8(%r13)
	movq	(%r12), %rdx
	movq	64(%rbx), %rax
	movq	%rdx, (%rax)
	movq	88(%rbx), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 88(%rbx)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 72(%rbx)
	movq	%rdx, 80(%rbx)
	movq	%rax, 64(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2931:
	.cfi_restore_state
	leaq	2(%rdi), %r15
	leaq	(%r15,%r15), %rax
	cmpq	%rax, %rdx
	ja	.L2934
	testq	%rdx, %rdx
	movl	$1, %eax
	movq	(%rbx), %rdi
	cmovne	%rdx, %rax
	movq	16(%rdi), %r13
	leaq	2(%rdx,%rax), %rcx
	movq	24(%rdi), %rax
	leaq	0(,%rcx,8), %rsi
	subq	%r13, %rax
	cmpq	%rax, %rsi
	ja	.L2935
	addq	%r13, %rsi
	movq	%rsi, 16(%rdi)
.L2919:
	movq	%rcx, %rax
	movq	56(%rbx), %rsi
	subq	%r15, %rax
	shrq	%rax
	leaq	0(%r13,%rax,8), %r15
	movq	88(%rbx), %rax
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L2920
	subq	%rsi, %rdx
	movq	%r15, %rdi
	movq	%rcx, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %rcx
.L2920:
	movq	24(%rbx), %rax
	leaq	0(,%rax,8), %rdx
	cmpq	$15, %rdx
	jbe	.L2921
	movq	16(%rbx), %rdx
	movq	%rax, 8(%rdx)
	movq	$0, (%rdx)
.L2921:
	movq	%r13, 16(%rbx)
	movq	%rcx, 24(%rbx)
.L2917:
	movq	%r15, 56(%rbx)
	movq	(%r15), %rax
	leaq	(%r15,%r14), %r13
	movq	(%r15), %xmm0
	movq	%r13, 88(%rbx)
	addq	$512, %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 40(%rbx)
	movq	0(%r13), %rax
	movq	%rax, 72(%rbx)
	addq	$512, %rax
	movq	%rax, 80(%rbx)
	jmp	.L2914
	.p2align 4,,10
	.p2align 3
.L2932:
	movq	(%rax), %rdx
	movq	%rdx, 8(%rbx)
	jmp	.L2923
	.p2align 4,,10
	.p2align 3
.L2934:
	subq	%r15, %rdx
	addq	$8, %r13
	shrq	%rdx
	leaq	(%rcx,%rdx,8), %r15
	movq	%r13, %rdx
	subq	%rsi, %rdx
	cmpq	%r15, %rsi
	jbe	.L2916
	cmpq	%r13, %rsi
	je	.L2917
	movq	%r15, %rdi
	call	memmove@PLT
	jmp	.L2917
	.p2align 4,,10
	.p2align 3
.L2916:
	cmpq	%r13, %rsi
	je	.L2917
	leaq	8(%r14), %rdi
	subq	%rdx, %rdi
	addq	%r15, %rdi
	call	memmove@PLT
	jmp	.L2917
	.p2align 4,,10
	.p2align 3
.L2933:
	movl	$512, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L2923
	.p2align 4,,10
	.p2align 3
.L2935:
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	movq	%rax, %r13
	jmp	.L2919
.L2930:
	leaq	.LC15(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE23873:
	.size	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJS4_EEEvDpOT_, .-_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJS4_EEEvDpOT_
	.section	.text._ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm,"axG",@progbits,_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	.type	_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm, @function
_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm:
.LFB24045:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$8, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	shrq	$6, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	1(%rsi), %rbx
	addq	$3, %rsi
	subq	$8, %rsp
	cmpq	$8, %rsi
	cmovb	%rax, %rsi
	movq	%rsi, 24(%rdi)
	movq	(%rdi), %rdi
	salq	$3, %rsi
	movq	16(%rdi), %rdx
	movq	24(%rdi), %rax
	subq	%rdx, %rax
	cmpq	%rax, %rsi
	ja	.L2950
	addq	%rdx, %rsi
	movq	%rsi, 16(%rdi)
.L2938:
	movq	24(%r12), %rax
	movq	%rdx, 16(%r12)
	subq	%rbx, %rax
	shrq	%rax
	leaq	(%rdx,%rax,8), %r15
	leaq	(%r15,%rbx,8), %r14
	cmpq	%r14, %r15
	jnb	.L2939
	movq	%r15, %rbx
	jmp	.L2944
	.p2align 4,,10
	.p2align 3
.L2940:
	movq	(%r12), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L2951
	leaq	512(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L2941:
	movq	%rax, (%rbx)
	addq	$8, %rbx
	cmpq	%rbx, %r14
	jbe	.L2939
.L2944:
	movq	8(%r12), %rax
	testq	%rax, %rax
	je	.L2940
	cmpq	$63, 8(%rax)
	jbe	.L2940
	movq	(%rax), %rdx
	addq	$8, %rbx
	movq	%rdx, 8(%r12)
	movq	%rax, -8(%rbx)
	cmpq	%rbx, %r14
	ja	.L2944
.L2939:
	movq	%r15, 56(%r12)
	movq	(%r15), %rdx
	andl	$63, %r13d
	leaq	512(%rdx), %rax
	movq	%rdx, 40(%r12)
	movq	%rax, 48(%r12)
	leaq	-8(%r14), %rax
	movq	%rax, 88(%r12)
	movq	-8(%r14), %rax
	movq	%rdx, 32(%r12)
	movq	%rax, 72(%r12)
	leaq	512(%rax), %rcx
	leaq	(%rax,%r13,8), %rax
	movq	%rcx, 80(%r12)
	movq	%rax, 64(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2951:
	.cfi_restore_state
	movl	$512, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L2941
	.p2align 4,,10
	.p2align 3
.L2950:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L2938
	.cfi_endproc
.LFE24045:
	.size	_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm, .-_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	.section	.rodata._ZNK2v88internal8compiler15LoadElimination16ComputeLoopStateEPNS1_4NodeEPKNS2_13AbstractStateE.str1.1,"aMS",@progbits,1
.LC16:
	.string	"Temporary scoped zone"
	.section	.text._ZNK2v88internal8compiler15LoadElimination16ComputeLoopStateEPNS1_4NodeEPKNS2_13AbstractStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler15LoadElimination16ComputeLoopStateEPNS1_4NodeEPKNS2_13AbstractStateE
	.type	_ZNK2v88internal8compiler15LoadElimination16ComputeLoopStateEPNS1_4NodeEPKNS2_13AbstractStateE, @function
_ZNK2v88internal8compiler15LoadElimination16ComputeLoopStateEPNS1_4NodeEPKNS2_13AbstractStateE:
.LFB19436:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$520, %rsp
	movq	%rdi, -448(%rbp)
	movq	%rsi, %rdi
	movq	%rsi, -440(%rbp)
	xorl	%esi, %esi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	leaq	.LC16(%rip), %rdx
	movq	%rax, %r13
	movq	16(%rbx), %rax
	movq	32(%rax), %rsi
	leaq	-416(%rbp), %rax
	movq	%rax, %rbx
	movq	%rax, %rdi
	movq	%rax, -480(%rbp)
	call	_ZN2v88internal4ZoneC1EPNS0_19AccountingAllocatorEPKc@PLT
	movq	%rbx, -256(%rbp)
	leaq	-256(%rbp), %rbx
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdi
	movq	$0, -248(%rbp)
	movq	$0, -240(%rbp)
	movq	$0, -232(%rbp)
	movaps	%xmm0, -224(%rbp)
	movaps	%xmm0, -208(%rbp)
	movaps	%xmm0, -192(%rbp)
	movaps	%xmm0, -176(%rbp)
	call	_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	cmpq	$0, -240(%rbp)
	je	.L3140
	movdqa	-256(%rbp), %xmm5
	pxor	%xmm0, %xmm0
	leaq	-160(%rbp), %rdi
	xorl	%esi, %esi
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm5, -160(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	$0, -144(%rbp)
	movq	$0, -136(%rbp)
	call	_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	movdqa	-96(%rbp), %xmm5
	movq	-216(%rbp), %r9
	movq	-224(%rbp), %xmm3
	movq	-200(%rbp), %r8
	movq	-192(%rbp), %xmm1
	movq	-184(%rbp), %rcx
	movaps	%xmm5, -192(%rbp)
	movq	%r9, %xmm5
	punpcklqdq	%xmm5, %xmm3
	movdqa	-128(%rbp), %xmm6
	movq	-208(%rbp), %xmm2
	movaps	%xmm3, -320(%rbp)
	movq	%r8, %xmm3
	movq	-168(%rbp), %rdx
	movdqa	-112(%rbp), %xmm7
	punpcklqdq	%xmm3, %xmm2
	movq	-248(%rbp), %rax
	movq	-176(%rbp), %xmm0
	movaps	%xmm6, -224(%rbp)
	movaps	%xmm2, -304(%rbp)
	movq	%rcx, %xmm2
	movq	-256(%rbp), %xmm4
	movq	-240(%rbp), %r11
	punpcklqdq	%xmm2, %xmm1
	movq	-232(%rbp), %r10
	movdqa	-80(%rbp), %xmm6
	movaps	%xmm7, -208(%rbp)
	movq	-144(%rbp), %rsi
	movq	-136(%rbp), %rdi
	movq	%rax, %xmm7
	movaps	%xmm1, -288(%rbp)
	movq	%rdx, %xmm1
	punpcklqdq	%xmm7, %xmm4
	movq	%r11, -336(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movq	%rsi, -240(%rbp)
	movq	%rdi, -232(%rbp)
	movq	%r10, -328(%rbp)
	movaps	%xmm6, -176(%rbp)
	movaps	%xmm4, -352(%rbp)
	movaps	%xmm0, -272(%rbp)
	testq	%rsi, %rsi
	je	.L2954
	movq	-168(%rbp), %rcx
	movq	-200(%rbp), %rdx
	addq	$8, %rcx
	cmpq	%rdx, %rcx
	jbe	.L2955
	.p2align 4,,10
	.p2align 3
.L2958:
	testq	%rax, %rax
	je	.L2956
	cmpq	$64, 8(%rax)
	ja	.L2957
.L2956:
	movq	(%rdx), %rax
	movq	$64, 8(%rax)
	movq	-248(%rbp), %rsi
	movq	%rsi, (%rax)
	movq	%rax, -248(%rbp)
.L2957:
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	ja	.L2958
	movq	-232(%rbp), %rdi
	movq	-240(%rbp), %rsi
.L2955:
	leaq	0(,%rdi,8), %rax
	cmpq	$15, %rax
	jbe	.L2954
	movq	%rdi, 8(%rsi)
	movq	$0, (%rsi)
.L2954:
	movq	-480(%rbp), %rax
	movq	%rbx, %rdi
	leaq	-440(%rbp), %rsi
	movl	$0, -240(%rbp)
	movl	$8, %r15d
	movl	$1, %r14d
	movq	$0, -232(%rbp)
	movq	%rax, -256(%rbp)
	leaq	-240(%rbp), %rax
	movq	%rax, -488(%rbp)
	movq	%rax, -224(%rbp)
	movq	%rax, -216(%rbp)
	movq	$0, -208(%rbp)
	call	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_
	leaq	-160(%rbp), %rcx
	movq	%rbx, -456(%rbp)
	movq	-288(%rbp), %rdx
	movq	%rcx, %rbx
	jmp	.L2967
	.p2align 4,,10
	.p2align 3
.L3141:
	leaq	(%rsi,%r15), %rax
.L2964:
	movq	(%rax), %rsi
	movq	-272(%rbp), %rax
	subq	$8, %rax
	movq	%rsi, -160(%rbp)
	cmpq	%rax, %rdx
	je	.L2965
	movq	%rsi, (%rdx)
	movq	-288(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, -288(%rbp)
.L2966:
	addl	$1, %r14d
	addq	$8, %r15
.L2967:
	movzbl	23(%r13), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2961
	movq	32(%r13), %rax
	movl	8(%rax), %eax
.L2961:
	cmpl	%eax, %r14d
	jge	.L2962
	movq	-440(%rbp), %rdi
	movzbl	23(%rdi), %eax
	leaq	32(%rdi), %rsi
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L3141
	movq	32(%rdi), %rax
	leaq	16(%rax,%r15), %rax
	jmp	.L2964
	.p2align 4,,10
	.p2align 3
.L2965:
	leaq	-352(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJS4_EEEvDpOT_
	movq	-288(%rbp), %rdx
	jmp	.L2966
.L2962:
	movq	-320(%rbp), %rax
	movq	-456(%rbp), %rbx
	cmpq	%rdx, %rax
	je	.L2968
	leaq	-424(%rbp), %rdi
	movq	$0, -496(%rbp)
	movq	-488(%rbp), %r13
	leaq	-160(%rbp), %r15
	movq	$0, -464(%rbp)
	movq	$0, -472(%rbp)
	movq	%rdi, -456(%rbp)
	.p2align 4,,10
	.p2align 3
.L3033:
	movq	(%rax), %rdx
	movq	-304(%rbp), %rsi
	movq	%rdx, -424(%rbp)
	leaq	-8(%rsi), %rdx
	cmpq	%rdx, %rax
	je	.L2969
	addq	$8, %rax
	movq	%rax, -320(%rbp)
.L2970:
	movq	-232(%rbp), %rax
	testq	%rax, %rax
	je	.L2973
	movq	-424(%rbp), %rdx
	movq	%r13, %rcx
	jmp	.L2974
	.p2align 4,,10
	.p2align 3
.L3142:
	movq	%rax, %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L2975
.L2974:
	cmpq	%rdx, 32(%rax)
	jnb	.L3142
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L2974
.L2975:
	cmpq	%r13, %rcx
	je	.L2973
	cmpq	%rdx, 32(%rcx)
	jbe	.L2978
.L2973:
	movq	-456(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_
	movq	-424(%rbp), %rdi
	movq	(%rdi), %r8
	testb	$16, 18(%r8)
	jne	.L2985
	movzwl	16(%r8), %eax
	subw	$245, %ax
	cmpw	$42, %ax
	ja	.L2979
	leaq	.L2981(%rip), %rsi
	movzwl	%ax, %eax
	movslq	(%rsi,%rax,4), %rax
	addq	%rsi, %rax
	notrack jmp	*%rax
	.section	.rodata._ZNK2v88internal8compiler15LoadElimination16ComputeLoopStateEPNS1_4NodeEPKNS2_13AbstractStateE,"a",@progbits
	.align 4
	.align 4
.L2981:
	.long	.L2987-.L2981
	.long	.L2986-.L2981
	.long	.L2985-.L2981
	.long	.L2979-.L2981
	.long	.L2979-.L2981
	.long	.L2979-.L2981
	.long	.L2984-.L2981
	.long	.L2979-.L2981
	.long	.L2979-.L2981
	.long	.L2979-.L2981
	.long	.L2979-.L2981
	.long	.L2979-.L2981
	.long	.L2979-.L2981
	.long	.L2979-.L2981
	.long	.L2979-.L2981
	.long	.L2979-.L2981
	.long	.L2979-.L2981
	.long	.L2979-.L2981
	.long	.L2979-.L2981
	.long	.L2979-.L2981
	.long	.L2979-.L2981
	.long	.L2979-.L2981
	.long	.L2979-.L2981
	.long	.L2979-.L2981
	.long	.L2979-.L2981
	.long	.L2979-.L2981
	.long	.L2979-.L2981
	.long	.L2979-.L2981
	.long	.L2979-.L2981
	.long	.L2979-.L2981
	.long	.L2979-.L2981
	.long	.L2979-.L2981
	.long	.L2979-.L2981
	.long	.L2979-.L2981
	.long	.L2979-.L2981
	.long	.L2979-.L2981
	.long	.L2979-.L2981
	.long	.L2979-.L2981
	.long	.L2979-.L2981
	.long	.L2979-.L2981
	.long	.L2983-.L2981
	.long	.L2982-.L2981
	.long	.L2980-.L2981
	.section	.text._ZNK2v88internal8compiler15LoadElimination16ComputeLoopStateEPNS1_4NodeEPKNS2_13AbstractStateE
.L2984:
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%r12, %xmm0
	movq	$0, -144(%rbp)
	movq	%rax, -488(%rbp)
	movq	-448(%rbp), %rax
	movq	16(%rax), %r8
	movhps	-488(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	520(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3013
	movq	%r8, %rdx
	movq	%r15, %rsi
	movq	%r8, -504(%rbp)
	movaps	%xmm0, -528(%rbp)
	call	_ZNK2v88internal8compiler15LoadElimination12AbstractMaps4KillERKNS2_14AliasStateInfoEPNS0_4ZoneE
	cmpq	520(%r12), %rax
	movq	-504(%rbp), %r8
	movq	%rax, %r14
	jne	.L3014
	movq	-448(%rbp), %rax
	movdqa	-528(%rbp), %xmm0
	movq	16(%rax), %r8
.L3013:
	movq	$0, -144(%rbp)
	movq	%r12, %rax
	movaps	%xmm0, -160(%rbp)
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3130
	movq	%r8, %rcx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r8, -488(%rbp)
	call	_ZNK2v88internal8compiler15LoadElimination13AbstractField4KillERKNS2_14AliasStateInfoENS0_11MaybeHandleINS0_4NameEEEPNS0_4ZoneE
	movq	%rax, %r14
	movq	%r12, %rax
	cmpq	16(%r12), %r14
	je	.L3130
	movq	-488(%rbp), %r8
	movq	16(%r8), %rax
	movq	24(%r8), %rcx
	subq	%rax, %rcx
	cmpq	$527, %rcx
	jbe	.L3143
	leaq	528(%rax), %rcx
	movq	%rcx, 16(%r8)
.L3019:
	movl	$66, %ecx
	movq	%rax, %rdi
	movq	%r12, %rsi
	rep movsq
	movq	%r14, 16(%rax)
.L3130:
	movq	-424(%rbp), %rdi
	movq	%rax, %r12
	movq	(%rdi), %r8
.L2985:
	movl	24(%r8), %eax
	testl	%eax, %eax
	jle	.L2978
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L3032:
	movl	%r14d, %esi
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	-272(%rbp), %rsi
	movq	-288(%rbp), %rcx
	movq	%rax, -160(%rbp)
	leaq	-8(%rsi), %rdx
	cmpq	%rdx, %rcx
	je	.L3030
	movq	%rax, (%rcx)
	movq	-424(%rbp), %rdi
	addl	$1, %r14d
	addq	$8, -288(%rbp)
	movq	(%rdi), %rax
	cmpl	%r14d, 24(%rax)
	jg	.L3032
.L2978:
	movq	-320(%rbp), %rax
	cmpq	%rax, -288(%rbp)
	jne	.L3033
.L3149:
	movq	-472(%rbp), %r14
	movq	-464(%rbp), %r13
	cmpq	%r13, %r14
	je	.L2968
	leaq	-160(%rbp), %rax
	movq	%rbx, -456(%rbp)
	movq	520(%r12), %rdi
	movq	%rax, %rbx
	jmp	.L3037
	.p2align 4,,10
	.p2align 3
.L3034:
	addq	$32, %r14
	cmpq	%r13, %r14
	je	.L3144
.L3037:
	movq	8(%r14), %rax
	movq	24(%r14), %rdx
	movq	%r12, -160(%rbp)
	movq	%rdx, -152(%rbp)
	movq	%rax, -144(%rbp)
	testq	%rdi, %rdi
	je	.L3034
	movq	-448(%rbp), %rax
	movq	%rbx, %rsi
	movq	16(%rax), %r15
	movq	%r15, %rdx
	call	_ZNK2v88internal8compiler15LoadElimination12AbstractMaps4KillERKNS2_14AliasStateInfoEPNS0_4ZoneE
	movq	520(%r12), %rdi
	movq	%rax, %rdx
	cmpq	%rdi, %rax
	je	.L3034
	movq	16(%r15), %rax
	movq	24(%r15), %rcx
	subq	%rax, %rcx
	cmpq	$527, %rcx
	jbe	.L3145
	leaq	528(%rax), %rcx
	movq	%rcx, 16(%r15)
.L3036:
	addq	$32, %r14
	movq	%r12, %rsi
	movl	$66, %ecx
	movq	%rax, %rdi
	rep movsq
	movq	%rax, %r12
	movq	%rdx, 520(%rax)
	movq	%rdx, %rdi
	cmpq	%r13, %r14
	jne	.L3037
.L3144:
	movq	%r12, %r13
	movq	-472(%rbp), %r15
	movq	-464(%rbp), %r12
	leaq	-160(%rbp), %r14
	jmp	.L3042
	.p2align 4,,10
	.p2align 3
.L3038:
	addq	$32, %r15
	cmpq	%r15, %r12
	je	.L3146
.L3042:
	cmpb	$1, (%r15)
	jne	.L3038
	movq	8(%r15), %rax
	movq	24(%r15), %rdx
	movq	%r13, -160(%rbp)
	movq	%rax, -144(%rbp)
	movq	-448(%rbp), %rax
	movq	%rdx, -152(%rbp)
	movq	16(%r13), %rdi
	movq	16(%rax), %rbx
	movq	%r13, %rax
	testq	%rdi, %rdi
	je	.L3039
	xorl	%edx, %edx
	movq	%rbx, %rcx
	movq	%r14, %rsi
	call	_ZNK2v88internal8compiler15LoadElimination13AbstractField4KillERKNS2_14AliasStateInfoENS0_11MaybeHandleINS0_4NameEEEPNS0_4ZoneE
	movq	%rax, %rdx
	movq	%r13, %rax
	cmpq	16(%r13), %rdx
	je	.L3039
	movq	16(%rbx), %rax
	movq	24(%rbx), %rcx
	subq	%rax, %rcx
	cmpq	$527, %rcx
	jbe	.L3147
	leaq	528(%rax), %rcx
	movq	%rcx, 16(%rbx)
.L3041:
	movl	$66, %ecx
	movq	%rax, %rdi
	movq	%r13, %rsi
	rep movsq
	movq	%rdx, 16(%rax)
.L3039:
	addq	$32, %r15
	movq	%rax, %r13
	cmpq	%r15, %r12
	jne	.L3042
.L3146:
	movq	-456(%rbp), %rbx
.L3029:
	movq	-232(%rbp), %r14
	testq	%r14, %r14
	je	.L3043
.L3046:
	movq	24(%r14), %r12
	testq	%r12, %r12
	je	.L3044
.L3045:
	movq	24(%r12), %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	movq	16(%r12), %r12
	testq	%r12, %r12
	jne	.L3045
.L3044:
	movq	16(%r14), %r14
	testq	%r14, %r14
	jne	.L3046
.L3043:
	movq	-336(%rbp), %rax
	testq	%rax, %rax
	je	.L3047
	movq	-264(%rbp), %rbx
	movq	-296(%rbp), %rdx
	leaq	8(%rbx), %rcx
	cmpq	%rdx, %rcx
	jbe	.L3048
	movq	-344(%rbp), %rax
	.p2align 4,,10
	.p2align 3
.L3051:
	testq	%rax, %rax
	je	.L3049
	cmpq	$64, 8(%rax)
	ja	.L3050
.L3049:
	movq	(%rdx), %rax
	movq	$64, 8(%rax)
	movq	-344(%rbp), %rsi
	movq	%rsi, (%rax)
	movq	%rax, -344(%rbp)
.L3050:
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	ja	.L3051
	movq	-336(%rbp), %rax
.L3048:
	movq	-328(%rbp), %rdx
	leaq	0(,%rdx,8), %rcx
	cmpq	$15, %rcx
	jbe	.L3047
	movq	%rdx, 8(%rax)
	movq	$0, (%rax)
.L3047:
	movq	-480(%rbp), %rdi
	call	_ZN2v88internal4ZoneD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3148
	addq	$520, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3030:
	.cfi_restore_state
	leaq	-352(%rbp), %rdi
	movq	%r15, %rsi
	addl	$1, %r14d
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJS4_EEEvDpOT_
	movq	-424(%rbp), %rdi
	movq	(%rdi), %rax
	cmpl	24(%rax), %r14d
	jl	.L3032
	movq	-320(%rbp), %rax
	cmpq	%rax, -288(%rbp)
	jne	.L3033
	jmp	.L3149
	.p2align 4,,10
	.p2align 3
.L2969:
	movq	-344(%rbp), %rax
	testq	%rax, %rax
	je	.L2971
	cmpq	$64, 8(%rax)
	ja	.L2972
.L2971:
	movq	-312(%rbp), %rax
	movq	$64, 8(%rax)
	movq	-344(%rbp), %rdx
	movq	%rdx, (%rax)
	movq	%rax, -344(%rbp)
.L2972:
	movq	-296(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, -296(%rbp)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, -312(%rbp)
	movq	%rdx, -304(%rbp)
	movq	%rax, -320(%rbp)
	jmp	.L2970
.L3140:
	movdqa	-256(%rbp), %xmm6
	movdqa	-224(%rbp), %xmm7
	movq	$0, -336(%rbp)
	movq	-232(%rbp), %rax
	movdqa	-176(%rbp), %xmm1
	movaps	%xmm6, -352(%rbp)
	movdqa	-208(%rbp), %xmm6
	movaps	%xmm7, -320(%rbp)
	movdqa	-192(%rbp), %xmm7
	movq	%rax, -328(%rbp)
	movaps	%xmm6, -304(%rbp)
	movaps	%xmm7, -288(%rbp)
	movaps	%xmm1, -272(%rbp)
	jmp	.L2954
.L2979:
	leaq	264(%r12), %rax
	leaq	520(%r12), %rdx
	jmp	.L3028
	.p2align 4,,10
	.p2align 3
.L3024:
	addq	$8, %rax
	cmpq	%rdx, %rax
	je	.L3150
.L3028:
	cmpq	$0, (%rax)
	je	.L3024
	movq	-448(%rbp), %rax
	movq	16(%rax), %rdi
	movq	16(%rdi), %r13
	movq	24(%rdi), %rax
	subq	%r13, %rax
	cmpq	$527, %rax
	jbe	.L3151
	leaq	528(%r13), %rax
	movq	%rax, 16(%rdi)
.L3026:
	xorl	%eax, %eax
	leaq	8(%r13), %rdi
	movl	$32, %ecx
	movq	$0, 0(%r13)
	rep stosq
	leaq	264(%r13), %rdi
	movl	$32, %ecx
	rep stosq
	movq	$0, 520(%r13)
	movdqu	264(%r12), %xmm7
	movups	%xmm7, 264(%r13)
	movdqu	280(%r12), %xmm2
	movups	%xmm2, 280(%r13)
	movdqu	296(%r12), %xmm3
	movups	%xmm3, 296(%r13)
	movdqu	312(%r12), %xmm5
	movups	%xmm5, 312(%r13)
	movdqu	328(%r12), %xmm6
	movups	%xmm6, 328(%r13)
	movdqu	344(%r12), %xmm7
	movups	%xmm7, 344(%r13)
	movdqu	360(%r12), %xmm4
	movups	%xmm4, 360(%r13)
	movdqu	376(%r12), %xmm1
	movups	%xmm1, 376(%r13)
	movdqu	392(%r12), %xmm2
	movups	%xmm2, 392(%r13)
	movdqu	408(%r12), %xmm3
	movups	%xmm3, 408(%r13)
	movdqu	424(%r12), %xmm5
	movups	%xmm5, 424(%r13)
	movdqu	440(%r12), %xmm6
	movups	%xmm6, 440(%r13)
	movdqu	456(%r12), %xmm7
	movups	%xmm7, 456(%r13)
	movdqu	472(%r12), %xmm4
	movups	%xmm4, 472(%r13)
	movdqu	488(%r12), %xmm1
	movups	%xmm1, 488(%r13)
	movdqu	504(%r12), %xmm2
	movups	%xmm2, 504(%r13)
	jmp	.L3029
.L2987:
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler13FieldAccessOfEPKNS1_8OperatorE@PLT
	movq	-448(%rbp), %rdi
	movq	%r12, %rdx
	movq	%r15, %rcx
	movdqu	(%rax), %xmm3
	movq	-424(%rbp), %rsi
	movaps	%xmm3, -160(%rbp)
	movdqu	16(%rax), %xmm5
	movaps	%xmm5, -144(%rbp)
	movdqu	32(%rax), %xmm6
	movaps	%xmm6, -128(%rbp)
	movq	48(%rax), %rax
	movq	%rax, -112(%rbp)
	call	_ZNK2v88internal8compiler15LoadElimination29ComputeLoopStateForStoreFieldEPNS1_4NodeEPKNS2_13AbstractStateERKNS1_11FieldAccessE
	movq	-424(%rbp), %rdi
	movq	%rax, %r12
	movq	(%rdi), %r8
	jmp	.L2985
.L2980:
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler20ElementsTransitionOfEPKNS1_8OperatorE@PLT
	movq	8(%rax), %rdi
	movzbl	(%rax), %esi
	movq	16(%rax), %rax
	movq	%rdi, -504(%rbp)
	movq	-424(%rbp), %rdi
	movb	%sil, -528(%rbp)
	xorl	%esi, %esi
	movq	%rax, -488(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	520(%r12), %rsi
	movq	%rax, %r14
	testq	%rsi, %rsi
	je	.L2994
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_114ResolveRenamesEPNS1_4NodeE
	movq	%rax, %rdx
	movq	24(%rsi), %rax
	leaq	16(%rsi), %rdi
	testq	%rax, %rax
	je	.L2994
	movq	%rdi, %rcx
	jmp	.L2995
.L3152:
	movq	%rax, %rcx
	movq	16(%rax), %rax
.L2998:
	testq	%rax, %rax
	je	.L2996
.L2995:
	cmpq	%rdx, 32(%rax)
	jnb	.L3152
	movq	24(%rax), %rax
	jmp	.L2998
.L2986:
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	-424(%rbp), %rdi
	movl	$1, %esi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	(%r12), %rdi
	movq	%rax, %rdx
	testq	%rdi, %rdi
	je	.L3129
	movq	-448(%rbp), %rax
	movq	%r14, %rsi
	movq	16(%rax), %r8
	movq	%r8, %rcx
	movq	%r8, -488(%rbp)
	call	_ZNK2v88internal8compiler15LoadElimination16AbstractElements4KillEPNS1_4NodeES5_PNS0_4ZoneE
	cmpq	(%r12), %rax
	movq	-488(%rbp), %r8
	movq	%rax, %r14
	jne	.L3021
.L3129:
	movq	-424(%rbp), %rdi
	movq	(%rdi), %r8
	jmp	.L2985
.L2983:
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	-448(%rbp), %rdi
	movq	%r12, %xmm0
	movq	%rax, %xmm2
	movq	16(%rdi), %r8
	punpcklqdq	%xmm2, %xmm0
	jmp	.L3013
.L2982:
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	-448(%rbp), %rdi
	movq	%r12, %xmm0
	movq	%rax, %xmm3
	movq	16(%rdi), %r8
	punpcklqdq	%xmm3, %xmm0
	jmp	.L3013
.L2996:
	cmpq	%rcx, %rdi
	je	.L2994
	cmpq	%rdx, 32(%rcx)
	ja	.L2994
	movq	40(%rcx), %rax
	movq	-488(%rbp), %rdi
	cmpq	%rax, %rdi
	je	.L3129
	cmpq	$1, %rdi
	je	.L2994
	cmpq	$1, %rax
	je	.L3129
	testb	$3, %dil
	je	.L2994
	leaq	-2(%rdi), %r8
	testb	$3, %al
	je	.L3153
	movq	6(%rax), %rdx
	leaq	-2(%rax), %rcx
	cmpq	14(%rax), %rdx
	je	.L3129
	xorl	%r9d, %r9d
	movq	%rbx, -512(%rbp)
	movq	%r13, -536(%rbp)
	movq	%r9, %rbx
	movq	%r8, %r13
	movq	%r15, -544(%rbp)
	movq	%r12, %r15
	movq	%rcx, %r12
	jmp	.L3003
.L3155:
	movq	8(%r12), %rdx
	movq	16(%r12), %rax
	addq	$1, %rbx
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rbx, %rax
	jbe	.L3154
.L3003:
	movq	16(%r13), %rsi
	movq	8(%r13), %rdi
	leaq	(%rdx,%rbx,8), %rdx
	movq	%rsi, -552(%rbp)
	call	_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPKPmSt6vectorIS2_N2v88internal13ZoneAllocatorIS2_EEEEENS0_5__ops16_Iter_equals_valIS3_EEET_SF_SF_T0_St26random_access_iterator_tag
	movq	-552(%rbp), %rsi
	cmpq	%rax, %rsi
	jne	.L3155
	movq	%r15, %r12
	movq	-512(%rbp), %rbx
	movq	-536(%rbp), %r13
	movq	-544(%rbp), %r15
.L2994:
	movq	-464(%rbp), %rax
	cmpq	%rax, -496(%rbp)
	je	.L3004
	movzbl	-528(%rbp), %edi
	movq	%r14, 24(%rax)
	addq	$32, %rax
	movq	-504(%rbp), %xmm0
	movb	%dil, -32(%rax)
	movhps	-488(%rbp), %xmm0
	movups	%xmm0, -24(%rax)
	movq	%rax, -464(%rbp)
	jmp	.L3129
.L3145:
	movl	$528, %esi
	movq	%r15, %rdi
	movq	%rdx, -488(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-488(%rbp), %rdx
	jmp	.L3036
.L2968:
	movq	%r12, %r13
	jmp	.L3029
.L3147:
	movl	$528, %esi
	movq	%rbx, %rdi
	movq	%rdx, -464(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-464(%rbp), %rdx
	jmp	.L3041
.L3150:
	leaq	_ZN2v88internal8compiler15LoadElimination13AbstractState12empty_state_E(%rip), %r13
	jmp	.L3029
.L3004:
	movq	-496(%rbp), %rcx
	subq	-472(%rbp), %rcx
	movq	%rcx, %rdx
	sarq	$5, %rdx
	cmpq	$67108863, %rdx
	je	.L3156
	testq	%rdx, %rdx
	je	.L3059
	leaq	(%rdx,%rdx), %rax
	cmpq	%rax, %rdx
	jbe	.L3157
	movl	$2147483616, %esi
	movl	$2147483616, %edx
.L3007:
	movq	-400(%rbp), %rax
	movq	-392(%rbp), %rdi
	subq	%rax, %rdi
	cmpq	%rsi, %rdi
	jb	.L3158
	addq	%rax, %rsi
	movq	%rsi, -400(%rbp)
.L3010:
	leaq	(%rax,%rdx), %rsi
	leaq	32(%rax), %rdx
	movq	%rsi, -496(%rbp)
.L3008:
	movq	-504(%rbp), %xmm0
	movzbl	-528(%rbp), %edi
	addq	%rax, %rcx
	movq	%r14, 24(%rcx)
	movhps	-488(%rbp), %xmm0
	movb	%dil, (%rcx)
	movq	-472(%rbp), %rdi
	movups	%xmm0, 8(%rcx)
	cmpq	%rdi, -464(%rbp)
	je	.L3062
	movq	%rdi, %rdx
	movq	%rax, %rcx
.L3012:
	movdqu	(%rdx), %xmm5
	addq	$32, %rcx
	addq	$32, %rdx
	movups	%xmm5, -32(%rcx)
	movdqu	-16(%rdx), %xmm2
	movups	%xmm2, -16(%rcx)
	cmpq	%rdx, -464(%rbp)
	jne	.L3012
	movq	-464(%rbp), %rdx
	subq	-472(%rbp), %rdx
	leaq	32(%rax,%rdx), %rdi
	movq	%rdi, -464(%rbp)
.L3011:
	movq	%rax, -472(%rbp)
	jmp	.L3129
.L3143:
	movl	$528, %esi
	movq	%r8, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L3019
.L3014:
	movq	16(%r8), %rax
	movq	24(%r8), %rcx
	subq	%rax, %rcx
	cmpq	$527, %rcx
	jbe	.L3159
	leaq	528(%rax), %rcx
	movq	%rcx, 16(%r8)
.L3016:
	movq	%r12, %rsi
	movl	$66, %ecx
	movq	%rax, %rdi
	movq	%rax, %r12
	rep movsq
	movq	-448(%rbp), %rsi
	movq	%rax, %xmm0
	movq	%r14, 520(%rax)
	movhps	-488(%rbp), %xmm0
	movq	16(%rsi), %r8
	jmp	.L3013
.L3021:
	movq	16(%r8), %rax
	movq	24(%r8), %rcx
	subq	%rax, %rcx
	cmpq	$527, %rcx
	jbe	.L3160
	leaq	528(%rax), %rcx
	movq	%rcx, 16(%r8)
.L3023:
	movl	$66, %ecx
	movq	%rax, %rdi
	movq	%r12, %rsi
	rep movsq
	movq	%r14, (%rax)
	jmp	.L3130
.L3151:
	movl	$528, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r13
	jmp	.L3026
.L3157:
	testq	%rax, %rax
	jne	.L3161
	movq	$0, -496(%rbp)
	movl	$32, %edx
	xorl	%eax, %eax
	jmp	.L3008
.L3059:
	movl	$32, %esi
	movl	$32, %edx
	jmp	.L3007
.L3148:
	call	__stack_chk_fail@PLT
.L3160:
	movl	$528, %esi
	movq	%r8, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L3023
.L3062:
	movq	%rdx, -464(%rbp)
	jmp	.L3011
.L3154:
	movq	%r15, %r12
	movq	-512(%rbp), %rbx
	movq	-536(%rbp), %r13
	movq	-544(%rbp), %r15
	jmp	.L3129
.L3153:
	movq	16(%r8), %rcx
	movq	%rax, -160(%rbp)
	movq	%r15, %rdx
	movq	8(%r8), %rdi
	movq	%rcx, %rsi
	movq	%rcx, -512(%rbp)
	call	_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPKPmSt6vectorIS2_N2v88internal13ZoneAllocatorIS2_EEEEENS0_5__ops16_Iter_equals_valIS3_EEET_SF_SF_T0_St26random_access_iterator_tag
	movq	-512(%rbp), %rcx
	cmpq	%rax, %rcx
	je	.L2994
	jmp	.L3129
.L3159:
	movl	$528, %esi
	movq	%r8, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L3016
.L3158:
	movq	-480(%rbp), %rdi
	movq	%rdx, -512(%rbp)
	movq	%rcx, -496(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-496(%rbp), %rcx
	movq	-512(%rbp), %rdx
	jmp	.L3010
.L3156:
	leaq	.LC13(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L3161:
	cmpq	$67108863, %rax
	movl	$67108863, %edx
	cmova	%rdx, %rax
	salq	$5, %rax
	movq	%rax, %rdx
	movq	%rax, %rsi
	jmp	.L3007
	.cfi_endproc
.LFE19436:
	.size	_ZNK2v88internal8compiler15LoadElimination16ComputeLoopStateEPNS1_4NodeEPKNS2_13AbstractStateE, .-_ZNK2v88internal8compiler15LoadElimination16ComputeLoopStateEPNS1_4NodeEPKNS2_13AbstractStateE
	.section	.text._ZN2v88internal8compiler15LoadElimination15ReduceEffectPhiEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15LoadElimination15ReduceEffectPhiEPNS1_4NodeE
	.type	_ZN2v88internal8compiler15LoadElimination15ReduceEffectPhiEPNS1_4NodeE, @function
_ZN2v88internal8compiler15LoadElimination15ReduceEffectPhiEPNS1_4NodeE:
.LFB19431:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	xorl	%esi, %esi
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r14, %rdi
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	24(%r12), %rcx
	movl	20(%r13), %edx
	movq	%rax, %rbx
	movq	32(%r12), %rax
	andl	$16777215, %edx
	subq	%rcx, %rax
	sarq	$3, %rax
	cmpq	%rdx, %rax
	jbe	.L3194
	movq	(%rcx,%rdx,8), %rax
	movq	%rax, -56(%rbp)
	testq	%rax, %rax
	je	.L3194
	movq	(%rbx), %rax
	cmpw	$1, 16(%rax)
	je	.L3216
	movq	(%r14), %rax
	movl	$1, %r15d
	movl	24(%rax), %r13d
	cmpl	$1, %r13d
	jle	.L3170
.L3167:
	movl	%r15d, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	24(%r12), %rcx
	movl	20(%rax), %edx
	movq	32(%r12), %rax
	subq	%rcx, %rax
	andl	$16777215, %edx
	sarq	$3, %rax
	cmpq	%rax, %rdx
	jnb	.L3194
	cmpq	$0, (%rcx,%rdx,8)
	je	.L3194
	addl	$1, %r15d
	cmpl	%r13d, %r15d
	jne	.L3167
.L3170:
	movq	16(%r12), %rdi
	movq	16(%rdi), %r15
	movq	24(%rdi), %rax
	subq	%r15, %rax
	cmpq	$527, %rax
	jbe	.L3217
	leaq	528(%r15), %rax
	movq	%rax, 16(%rdi)
.L3171:
	leaq	8(%r15), %rax
	movq	-56(%rbp), %rsi
	movl	$66, %ecx
	movq	%r15, %rdi
	rep movsq
	movq	%rax, -64(%rbp)
	leaq	264(%r15), %rax
	movq	%rax, -72(%rbp)
	movl	$1, %eax
	movq	%r15, -88(%rbp)
	cmpl	$1, %r13d
	jle	.L3180
	movl	%r13d, -76(%rbp)
	movq	%r14, -56(%rbp)
	movq	%rbx, -96(%rbp)
	movl	%eax, %ebx
	.p2align 4,,10
	.p2align 3
.L3181:
	movq	-56(%rbp), %rdi
	movl	%ebx, %esi
	xorl	%r14d, %r14d
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	24(%r12), %rcx
	movq	16(%r12), %r13
	movl	20(%rax), %edx
	movq	32(%r12), %rax
	subq	%rcx, %rax
	andl	$16777215, %edx
	sarq	$3, %rax
	cmpq	%rax, %rdx
	jnb	.L3175
	movq	(%rcx,%rdx,8), %r14
.L3175:
	movq	(%r15), %rsi
	testq	%rsi, %rsi
	je	.L3176
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L3177
	movq	%r13, %rdx
	call	_ZNK2v88internal8compiler15LoadElimination16AbstractElements5MergeEPKS3_PNS0_4ZoneE
	movq	%rax, %rdi
.L3177:
	movq	%rdi, (%r15)
.L3176:
	movq	-64(%rbp), %rsi
	leaq	8(%r14), %rdx
	movq	%r13, %rcx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler15LoadElimination13AbstractState11FieldsMergeEPSt5arrayIPKNS2_13AbstractFieldELm32EERKS8_PNS0_4ZoneE
	movq	-72(%rbp), %rsi
	movq	%r13, %rcx
	movq	%r15, %rdi
	leaq	264(%r14), %rdx
	call	_ZN2v88internal8compiler15LoadElimination13AbstractState11FieldsMergeEPSt5arrayIPKNS2_13AbstractFieldELm32EERKS8_PNS0_4ZoneE
	movq	520(%r15), %rsi
	testq	%rsi, %rsi
	je	.L3178
	movq	520(%r14), %rdi
	testq	%rdi, %rdi
	je	.L3179
	movq	%r13, %rdx
	call	_ZNK2v88internal8compiler15LoadElimination12AbstractMaps5MergeEPKS3_PNS0_4ZoneE
	movq	%rax, %rdi
.L3179:
	movq	%rdi, 520(%r15)
.L3178:
	addl	$1, %ebx
	cmpl	-76(%rbp), %ebx
	jne	.L3181
	movq	-96(%rbp), %rbx
	movq	-56(%rbp), %r14
.L3180:
	movq	24(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L3174
	.p2align 4,,10
	.p2align 3
.L3173:
	movl	16(%rbx), %edx
	movl	%edx, %eax
	shrl	%eax
	andl	$1, %edx
	leaq	3(%rax,%rax,2), %rax
	leaq	(%rbx,%rax,8), %rcx
	movq	(%rcx), %rax
	jne	.L3182
	movq	%rax, %rcx
	movq	(%rax), %rax
.L3182:
	cmpw	$35, 16(%rax)
	je	.L3218
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L3173
.L3174:
	movq	-88(%rbp), %rdx
	jmp	.L3215
	.p2align 4,,10
	.p2align 3
.L3194:
	addq	$56, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3216:
	.cfi_restore_state
	movq	-56(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler15LoadElimination16ComputeLoopStateEPNS1_4NodeEPKNS2_13AbstractStateE
	movq	%rax, %rdx
.L3215:
	addq	$56, %rsp
	movq	%r14, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler15LoadElimination11UpdateStateEPNS1_4NodeEPKNS2_13AbstractStateE
	.p2align 4,,10
	.p2align 3
.L3218:
	.cfi_restore_state
	movq	-88(%rbp), %rsi
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler15LoadElimination17UpdateStateForPhiEPKNS2_13AbstractStateEPNS1_4NodeES7_
	movq	(%rbx), %rbx
	movq	%rax, -88(%rbp)
	testq	%rbx, %rbx
	jne	.L3173
	jmp	.L3174
.L3217:
	movl	$528, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r15
	jmp	.L3171
	.cfi_endproc
.LFE19431:
	.size	_ZN2v88internal8compiler15LoadElimination15ReduceEffectPhiEPNS1_4NodeE, .-_ZN2v88internal8compiler15LoadElimination15ReduceEffectPhiEPNS1_4NodeE
	.section	.rodata._ZN2v88internal8compiler15LoadElimination6ReduceEPNS1_4NodeE.str1.1,"aMS",@progbits,1
.LC17:
	.string	" visit #%d:%s"
.LC18:
	.string	"("
.LC19:
	.string	")"
.LC20:
	.string	", "
.LC21:
	.string	"#%d:%s"
.LC22:
	.string	"\n"
.LC23:
	.string	"  state[%i]: #%d:%s\n"
.LC24:
	.string	"  no state[%i]: #%d:%s\n"
	.section	.text._ZN2v88internal8compiler15LoadElimination6ReduceEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15LoadElimination6ReduceEPNS1_4NodeE
	.type	_ZN2v88internal8compiler15LoadElimination6ReduceEPNS1_4NodeE, @function
_ZN2v88internal8compiler15LoadElimination6ReduceEPNS1_4NodeE:
.LFB19314:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	cmpb	$0, _ZN2v88internal33FLAG_trace_turbo_load_eliminationE(%rip)
	movq	(%rsi), %rdi
	je	.L3220
	movl	24(%rdi), %esi
	testl	%esi, %esi
	jg	.L3262
.L3220:
	movzwl	16(%rdi), %ecx
	cmpw	$287, %cx
	ja	.L3229
	cmpw	$219, %cx
	jbe	.L3263
	subw	$220, %cx
	cmpw	$67, %cx
	ja	.L3229
	leaq	.L3236(%rip), %rdx
	movzwl	%cx, %ecx
	movslq	(%rdx,%rcx,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler15LoadElimination6ReduceEPNS1_4NodeE,"a",@progbits
	.align 4
	.align 4
.L3236:
	.long	.L3246-.L3236
	.long	.L3229-.L3236
	.long	.L3229-.L3236
	.long	.L3229-.L3236
	.long	.L3229-.L3236
	.long	.L3229-.L3236
	.long	.L3229-.L3236
	.long	.L3229-.L3236
	.long	.L3229-.L3236
	.long	.L3229-.L3236
	.long	.L3229-.L3236
	.long	.L3229-.L3236
	.long	.L3229-.L3236
	.long	.L3245-.L3236
	.long	.L3229-.L3236
	.long	.L3229-.L3236
	.long	.L3229-.L3236
	.long	.L3229-.L3236
	.long	.L3229-.L3236
	.long	.L3229-.L3236
	.long	.L3244-.L3236
	.long	.L3243-.L3236
	.long	.L3229-.L3236
	.long	.L3229-.L3236
	.long	.L3229-.L3236
	.long	.L3242-.L3236
	.long	.L3241-.L3236
	.long	.L3240-.L3236
	.long	.L3229-.L3236
	.long	.L3229-.L3236
	.long	.L3229-.L3236
	.long	.L3239-.L3236
	.long	.L3229-.L3236
	.long	.L3229-.L3236
	.long	.L3229-.L3236
	.long	.L3229-.L3236
	.long	.L3229-.L3236
	.long	.L3229-.L3236
	.long	.L3229-.L3236
	.long	.L3229-.L3236
	.long	.L3229-.L3236
	.long	.L3229-.L3236
	.long	.L3229-.L3236
	.long	.L3229-.L3236
	.long	.L3229-.L3236
	.long	.L3229-.L3236
	.long	.L3229-.L3236
	.long	.L3229-.L3236
	.long	.L3229-.L3236
	.long	.L3229-.L3236
	.long	.L3229-.L3236
	.long	.L3229-.L3236
	.long	.L3229-.L3236
	.long	.L3229-.L3236
	.long	.L3229-.L3236
	.long	.L3229-.L3236
	.long	.L3229-.L3236
	.long	.L3229-.L3236
	.long	.L3229-.L3236
	.long	.L3229-.L3236
	.long	.L3229-.L3236
	.long	.L3229-.L3236
	.long	.L3229-.L3236
	.long	.L3229-.L3236
	.long	.L3229-.L3236
	.long	.L3238-.L3236
	.long	.L3237-.L3236
	.long	.L3235-.L3236
	.section	.text._ZN2v88internal8compiler15LoadElimination6ReduceEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L3263:
	cmpw	$57, %cx
	je	.L3231
	jbe	.L3264
	xorl	%eax, %eax
	cmpw	$61, %cx
	jne	.L3229
.L3249:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3264:
	.cfi_restore_state
	testw	%cx, %cx
	je	.L3233
	cmpw	$36, %cx
	jne	.L3229
	addq	$8, %rsp
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler15LoadElimination15ReduceEffectPhiEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L3262:
	.cfi_restore_state
	movl	20(%r12), %esi
	movq	8(%rdi), %rdx
	xorl	%eax, %eax
	leaq	.LC17(%rip), %rdi
	andl	$16777215, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	(%r12), %rax
	movl	20(%rax), %ecx
	testl	%ecx, %ecx
	jg	.L3265
.L3221:
	leaq	.LC22(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	(%r12), %rdi
	movl	24(%rdi), %eax
	testl	%eax, %eax
	jle	.L3220
	xorl	%r14d, %r14d
	leaq	.LC24(%rip), %rbx
	jmp	.L3228
	.p2align 4,,10
	.p2align 3
.L3266:
	leaq	.LC23(%rip), %rdi
	movl	%r8d, %edx
	movl	%r14d, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler15LoadElimination13AbstractState5PrintEv
.L3227:
	movq	(%r12), %rdi
	addl	$1, %r14d
	cmpl	%r14d, 24(%rdi)
	jle	.L3220
.L3228:
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	24(%r13), %rcx
	movq	32(%r13), %rdx
	movl	20(%rax), %r8d
	subq	%rcx, %rdx
	andl	$16777215, %r8d
	sarq	$3, %rdx
	movl	%r8d, %esi
	cmpq	%rdx, %rsi
	jnb	.L3225
	movq	(%rcx,%rsi,8), %r15
	movq	(%rax), %rax
	movq	8(%rax), %rcx
	testq	%r15, %r15
	jne	.L3266
	movl	%r8d, %edx
	movl	%r14d, %esi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L3227
.L3235:
	addq	$8, %rsp
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler15LoadElimination28ReduceTransitionElementsKindEPNS1_4NodeE
.L3229:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler15LoadElimination15ReduceOtherNodeEPNS1_4NodeE
.L3246:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler15LoadElimination15ReduceCheckMapsEPNS1_4NodeE
.L3245:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler15LoadElimination17ReduceCompareMapsEPNS1_4NodeE
.L3244:
	.cfi_restore_state
	call	_ZN2v88internal8compiler13FieldAccessOfEPKNS1_8OperatorE@PLT
	addq	$8, %rsp
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%rbx
	movq	%rax, %rdx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler15LoadElimination15ReduceLoadFieldEPNS1_4NodeERKNS1_11FieldAccessE
.L3243:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler15LoadElimination17ReduceLoadElementEPNS1_4NodeE
.L3242:
	.cfi_restore_state
	call	_ZN2v88internal8compiler13FieldAccessOfEPKNS1_8OperatorE@PLT
	addq	$8, %rsp
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%rbx
	movq	%rax, %rdx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler15LoadElimination16ReduceStoreFieldEPNS1_4NodeERKNS1_11FieldAccessE
.L3241:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler15LoadElimination18ReduceStoreElementEPNS1_4NodeE
.L3240:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	24(%r13), %rsi
	movq	32(%r13), %rdx
	movl	20(%rax), %ecx
	xorl	%eax, %eax
	subq	%rsi, %rdx
	andl	$16777215, %ecx
	sarq	$3, %rdx
	cmpq	%rdx, %rcx
	jnb	.L3249
	movq	(%rsi,%rcx,8), %rax
	testq	%rax, %rax
	je	.L3249
	movq	%rax, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler15LoadElimination11UpdateStateEPNS1_4NodeEPKNS2_13AbstractStateE
	jmp	.L3249
.L3239:
	addq	$8, %rsp
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler15LoadElimination31ReduceTransitionAndStoreElementEPNS1_4NodeE
.L3238:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler15LoadElimination32ReduceEnsureWritableFastElementsEPNS1_4NodeE
.L3237:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler15LoadElimination27ReduceMaybeGrowFastElementsEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L3265:
	.cfi_restore_state
	xorl	%eax, %eax
	leaq	.LC18(%rip), %rdi
	xorl	%r14d, %r14d
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	(%r12), %rax
	leaq	.LC21(%rip), %rbx
	movl	20(%rax), %edx
	testl	%edx, %edx
	jg	.L3222
	jmp	.L3224
	.p2align 4,,10
	.p2align 3
.L3223:
	leaq	.LC20(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
.L3222:
	movl	%r14d, %esi
	movq	%r12, %rdi
	addl	$1, %r14d
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rbx, %rdi
	movq	(%rax), %rdx
	movl	20(%rax), %esi
	xorl	%eax, %eax
	movq	8(%rdx), %rdx
	andl	$16777215, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	(%r12), %rax
	cmpl	20(%rax), %r14d
	jl	.L3223
.L3224:
	leaq	.LC19(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L3221
	.p2align 4,,10
	.p2align 3
.L3225:
	movq	(%rax), %rax
	movl	%r8d, %edx
	movl	%r14d, %esi
	movq	%rbx, %rdi
	movq	8(%rax), %rcx
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L3227
	.p2align 4,,10
	.p2align 3
.L3231:
	addq	$8, %rsp
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler15LoadElimination14ReduceMapGuardEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L3233:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%rbx
	leaq	_ZN2v88internal8compiler15LoadElimination13AbstractState12empty_state_E(%rip), %rdx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler15LoadElimination11UpdateStateEPNS1_4NodeEPKNS2_13AbstractStateE
	.cfi_endproc
.LFE19314:
	.size	_ZN2v88internal8compiler15LoadElimination6ReduceEPNS1_4NodeE, .-_ZN2v88internal8compiler15LoadElimination6ReduceEPNS1_4NodeE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler15LoadElimination6ReduceEPNS1_4NodeE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler15LoadElimination6ReduceEPNS1_4NodeE, @function
_GLOBAL__sub_I__ZN2v88internal8compiler15LoadElimination6ReduceEPNS1_4NodeE:
.LFB24370:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	_ZStL8__ioinit(%rip), %rsi
	leaq	__dso_handle(%rip), %rdx
	call	__cxa_atexit@PLT
	leaq	8+_ZN2v88internal8compiler15LoadElimination13AbstractState12empty_state_E(%rip), %rsi
	xorl	%eax, %eax
	movl	$32, %ecx
	movq	$0, _ZN2v88internal8compiler15LoadElimination13AbstractState12empty_state_E(%rip)
	movq	%rsi, %rdi
	rep stosq
	movl	$32, %ecx
	movq	$0, 520+_ZN2v88internal8compiler15LoadElimination13AbstractState12empty_state_E(%rip)
	rep stosq
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24370:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler15LoadElimination6ReduceEPNS1_4NodeE, .-_GLOBAL__sub_I__ZN2v88internal8compiler15LoadElimination6ReduceEPNS1_4NodeE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler15LoadElimination6ReduceEPNS1_4NodeE
	.section	.rodata.CSWTCH.421,"a"
	.align 32
	.type	CSWTCH.421, @object
	.size	CSWTCH.421, 56
CSWTCH.421:
	.long	0
	.long	0
	.long	1
	.long	2
	.long	3
	.long	3
	.long	3
	.long	3
	.long	3
	.long	3
	.long	3
	.long	2
	.long	3
	.long	4
	.hidden	_ZTCN2v88internal12StdoutStreamE0_So
	.weak	_ZTCN2v88internal12StdoutStreamE0_So
	.section	.rodata._ZTCN2v88internal12StdoutStreamE0_So,"aG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTCN2v88internal12StdoutStreamE0_So, @object
	.size	_ZTCN2v88internal12StdoutStreamE0_So, 80
_ZTCN2v88internal12StdoutStreamE0_So:
	.quad	80
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-80
	.quad	-80
	.quad	0
	.quad	0
	.quad	0
	.hidden	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE
	.weak	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE
	.section	.rodata._ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE,"aG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE, @object
	.size	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE, 80
_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE:
	.quad	80
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-80
	.quad	-80
	.quad	0
	.quad	0
	.quad	0
	.weak	_ZTTN2v88internal12StdoutStreamE
	.section	.data.rel.ro.local._ZTTN2v88internal12StdoutStreamE,"awG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTTN2v88internal12StdoutStreamE, @object
	.size	_ZTTN2v88internal12StdoutStreamE, 48
_ZTTN2v88internal12StdoutStreamE:
	.quad	_ZTVN2v88internal12StdoutStreamE+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_So+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_So+64
	.quad	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE+64
	.quad	_ZTVN2v88internal12StdoutStreamE+64
	.weak	_ZTVN2v88internal12StdoutStreamE
	.section	.data.rel.ro.local._ZTVN2v88internal12StdoutStreamE,"awG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTVN2v88internal12StdoutStreamE, @object
	.size	_ZTVN2v88internal12StdoutStreamE, 80
_ZTVN2v88internal12StdoutStreamE:
	.quad	80
	.quad	0
	.quad	0
	.quad	_ZN2v88internal12StdoutStreamD1Ev
	.quad	_ZN2v88internal12StdoutStreamD0Ev
	.quad	-80
	.quad	-80
	.quad	0
	.quad	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.quad	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.weak	_ZTVN2v88internal8compiler15LoadEliminationE
	.section	.data.rel.ro._ZTVN2v88internal8compiler15LoadEliminationE,"awG",@progbits,_ZTVN2v88internal8compiler15LoadEliminationE,comdat
	.align 8
	.type	_ZTVN2v88internal8compiler15LoadEliminationE, @object
	.size	_ZTVN2v88internal8compiler15LoadEliminationE, 56
_ZTVN2v88internal8compiler15LoadEliminationE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal8compiler15LoadEliminationD1Ev
	.quad	_ZN2v88internal8compiler15LoadEliminationD0Ev
	.quad	_ZNK2v88internal8compiler15LoadElimination12reducer_nameEv
	.quad	_ZN2v88internal8compiler15LoadElimination6ReduceEPNS1_4NodeE
	.quad	_ZN2v88internal8compiler7Reducer8FinalizeEv
	.globl	_ZN2v88internal8compiler15LoadElimination13AbstractState12empty_state_E
	.section	.bss._ZN2v88internal8compiler15LoadElimination13AbstractState12empty_state_E,"aw",@nobits
	.align 32
	.type	_ZN2v88internal8compiler15LoadElimination13AbstractState12empty_state_E, @object
	.size	_ZN2v88internal8compiler15LoadElimination13AbstractState12empty_state_E, 528
_ZN2v88internal8compiler15LoadElimination13AbstractState12empty_state_E:
	.zero	528
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
