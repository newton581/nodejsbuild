	.file	"common-node-cache.cc"
	.text
	.section	.text._ZN2v88internal8compiler15CommonNodeCache20FindExternalConstantENS0_17ExternalReferenceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15CommonNodeCache20FindExternalConstantENS0_17ExternalReferenceE
	.type	_ZN2v88internal8compiler15CommonNodeCache20FindExternalConstantENS0_17ExternalReferenceE, @function
_ZN2v88internal8compiler15CommonNodeCache20FindExternalConstantENS0_17ExternalReferenceE:
.LFB10181:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movq	320(%rdi), %rsi
	subq	$-128, %rdi
	jmp	_ZN2v88internal8compiler9NodeCacheIlNS_4base4hashIlEESt8equal_toIlEE4FindEPNS0_4ZoneEl@PLT
	.cfi_endproc
.LFE10181:
	.size	_ZN2v88internal8compiler15CommonNodeCache20FindExternalConstantENS0_17ExternalReferenceE, .-_ZN2v88internal8compiler15CommonNodeCache20FindExternalConstantENS0_17ExternalReferenceE
	.section	.text._ZN2v88internal8compiler15CommonNodeCache16FindHeapConstantENS0_6HandleINS0_10HeapObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15CommonNodeCache16FindHeapConstantENS0_6HandleINS0_10HeapObjectEEE
	.type	_ZN2v88internal8compiler15CommonNodeCache16FindHeapConstantENS0_6HandleINS0_10HeapObjectEEE, @function
_ZN2v88internal8compiler15CommonNodeCache16FindHeapConstantENS0_6HandleINS0_10HeapObjectEEE:
.LFB10182:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movq	320(%rdi), %rsi
	addq	$224, %rdi
	jmp	_ZN2v88internal8compiler9NodeCacheIlNS_4base4hashIlEESt8equal_toIlEE4FindEPNS0_4ZoneEl@PLT
	.cfi_endproc
.LFE10182:
	.size	_ZN2v88internal8compiler15CommonNodeCache16FindHeapConstantENS0_6HandleINS0_10HeapObjectEEE, .-_ZN2v88internal8compiler15CommonNodeCache16FindHeapConstantENS0_6HandleINS0_10HeapObjectEEE
	.section	.text._ZN2v88internal8compiler15CommonNodeCache14GetCachedNodesEPNS0_10ZoneVectorIPNS1_4NodeEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15CommonNodeCache14GetCachedNodesEPNS0_10ZoneVectorIPNS1_4NodeEEE
	.type	_ZN2v88internal8compiler15CommonNodeCache14GetCachedNodesEPNS0_10ZoneVectorIPNS1_4NodeEEE, @function
_ZN2v88internal8compiler15CommonNodeCache14GetCachedNodesEPNS0_10ZoneVectorIPNS1_4NodeEEE:
.LFB10183:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZN2v88internal8compiler9NodeCacheIiNS_4base4hashIiEESt8equal_toIiEE14GetCachedNodesEPNS0_10ZoneVectorIPNS1_4NodeEEE@PLT
	leaq	32(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler9NodeCacheIlNS_4base4hashIlEESt8equal_toIlEE14GetCachedNodesEPNS0_10ZoneVectorIPNS1_4NodeEEE@PLT
	leaq	64(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler9NodeCacheIiNS_4base4hashIiEESt8equal_toIiEE14GetCachedNodesEPNS0_10ZoneVectorIPNS1_4NodeEEE@PLT
	leaq	96(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler9NodeCacheIlNS_4base4hashIlEESt8equal_toIlEE14GetCachedNodesEPNS0_10ZoneVectorIPNS1_4NodeEEE@PLT
	leaq	128(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler9NodeCacheIlNS_4base4hashIlEESt8equal_toIlEE14GetCachedNodesEPNS0_10ZoneVectorIPNS1_4NodeEEE@PLT
	leaq	192(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler9NodeCacheIlNS_4base4hashIlEESt8equal_toIlEE14GetCachedNodesEPNS0_10ZoneVectorIPNS1_4NodeEEE@PLT
	leaq	224(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler9NodeCacheIlNS_4base4hashIlEESt8equal_toIlEE14GetCachedNodesEPNS0_10ZoneVectorIPNS1_4NodeEEE@PLT
	leaq	256(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler9NodeCacheISt4pairIicENS_4base4hashIS4_EESt8equal_toIS4_EE14GetCachedNodesEPNS0_10ZoneVectorIPNS1_4NodeEEE@PLT
	leaq	288(%rbx), %rdi
	movq	%r12, %rsi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler9NodeCacheISt4pairIlcENS_4base4hashIS4_EESt8equal_toIS4_EE14GetCachedNodesEPNS0_10ZoneVectorIPNS1_4NodeEEE@PLT
	.cfi_endproc
.LFE10183:
	.size	_ZN2v88internal8compiler15CommonNodeCache14GetCachedNodesEPNS0_10ZoneVectorIPNS1_4NodeEEE, .-_ZN2v88internal8compiler15CommonNodeCache14GetCachedNodesEPNS0_10ZoneVectorIPNS1_4NodeEEE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler15CommonNodeCache20FindExternalConstantENS0_17ExternalReferenceE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler15CommonNodeCache20FindExternalConstantENS0_17ExternalReferenceE, @function
_GLOBAL__sub_I__ZN2v88internal8compiler15CommonNodeCache20FindExternalConstantENS0_17ExternalReferenceE:
.LFB11799:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE11799:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler15CommonNodeCache20FindExternalConstantENS0_17ExternalReferenceE, .-_GLOBAL__sub_I__ZN2v88internal8compiler15CommonNodeCache20FindExternalConstantENS0_17ExternalReferenceE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler15CommonNodeCache20FindExternalConstantENS0_17ExternalReferenceE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
