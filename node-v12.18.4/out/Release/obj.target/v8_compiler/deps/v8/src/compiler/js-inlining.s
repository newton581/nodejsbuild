	.file	"js-inlining.cc"
	.text
	.section	.text._ZN2v88internal12StdoutStreamD1Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD1Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StdoutStreamD1Ev
	.type	_ZN2v88internal12StdoutStreamD1Ev, @function
_ZN2v88internal12StdoutStreamD1Ev:
.LFB27065:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE27065:
	.size	_ZN2v88internal12StdoutStreamD1Ev, .-_ZN2v88internal12StdoutStreamD1Ev
	.section	.text._ZN2v88internal12StdoutStreamD0Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD0Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.type	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev, @function
_ZTv0_n24_N2v88internal12StdoutStreamD0Ev:
.LFB27207:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %rax
	addq	-24(%rax), %rdi
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	movq	%rax, 80(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE27207:
	.size	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev, .-_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StdoutStreamD0Ev
	.type	_ZN2v88internal12StdoutStreamD0Ev, @function
_ZN2v88internal12StdoutStreamD0Ev:
.LFB27066:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE27066:
	.size	_ZN2v88internal12StdoutStreamD0Ev, .-_ZN2v88internal12StdoutStreamD0Ev
	.section	.text._ZN2v88internal12StdoutStreamD1Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD1Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.type	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev, @function
_ZTv0_n24_N2v88internal12StdoutStreamD1Ev:
.LFB27208:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rax
	movq	-24(%rax), %rbx
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	addq	%rdi, %rbx
	movq	%rax, 80(%rbx)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	64(%rbx), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE27208:
	.size	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev, .-_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.section	.rodata._ZN2v88internal8compiler15FeedbackCellRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb.str1.1,"aMS",@progbits,1
.LC0:
	.string	"IsFeedbackCell()"
.LC1:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal8compiler15FeedbackCellRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,"axG",@progbits,_ZN2v88internal8compiler15FeedbackCellRefC5EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler15FeedbackCellRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.type	_ZN2v88internal8compiler15FeedbackCellRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, @function
_ZN2v88internal8compiler15FeedbackCellRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb:
.LFB9672:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%ecx, %ebx
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	testb	%bl, %bl
	jne	.L13
.L10:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef14IsFeedbackCellEv@PLT
	testb	%al, %al
	jne	.L10
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE9672:
	.size	_ZN2v88internal8compiler15FeedbackCellRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, .-_ZN2v88internal8compiler15FeedbackCellRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.weak	_ZN2v88internal8compiler15FeedbackCellRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.set	_ZN2v88internal8compiler15FeedbackCellRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,_ZN2v88internal8compiler15FeedbackCellRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.section	.rodata._ZN2v88internal8compiler21SharedFunctionInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb.str1.1,"aMS",@progbits,1
.LC2:
	.string	"IsSharedFunctionInfo()"
	.section	.text._ZN2v88internal8compiler21SharedFunctionInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,"axG",@progbits,_ZN2v88internal8compiler21SharedFunctionInfoRefC5EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler21SharedFunctionInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.type	_ZN2v88internal8compiler21SharedFunctionInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, @function
_ZN2v88internal8compiler21SharedFunctionInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb:
.LFB9759:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%ecx, %ebx
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	testb	%bl, %bl
	jne	.L17
.L14:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef20IsSharedFunctionInfoEv@PLT
	testb	%al, %al
	jne	.L14
	leaq	.LC2(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE9759:
	.size	_ZN2v88internal8compiler21SharedFunctionInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, .-_ZN2v88internal8compiler21SharedFunctionInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.weak	_ZN2v88internal8compiler21SharedFunctionInfoRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.set	_ZN2v88internal8compiler21SharedFunctionInfoRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,_ZN2v88internal8compiler21SharedFunctionInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.section	.rodata._ZN2v88internal8compiler9JSInliner26CreateArtificialFrameStateEPNS1_4NodeES4_iNS0_9BailoutIdENS1_14FrameStateTypeENS1_21SharedFunctionInfoRefES4_.str1.1,"aMS",@progbits,1
.LC3:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZN2v88internal8compiler9JSInliner26CreateArtificialFrameStateEPNS1_4NodeES4_iNS0_9BailoutIdENS1_14FrameStateTypeENS1_21SharedFunctionInfoRefES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler9JSInliner26CreateArtificialFrameStateEPNS1_4NodeES4_iNS0_9BailoutIdENS1_14FrameStateTypeENS1_21SharedFunctionInfoRefES4_
	.type	_ZN2v88internal8compiler9JSInliner26CreateArtificialFrameStateEPNS1_4NodeES4_iNS0_9BailoutIdENS1_14FrameStateTypeENS1_21SharedFunctionInfoRefES4_, @function
_ZN2v88internal8compiler9JSInliner26CreateArtificialFrameStateEPNS1_4NodeES4_iNS0_9BailoutIdENS1_14FrameStateTypeENS1_21SharedFunctionInfoRefES4_:
.LFB22121:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%r9d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movl	%r8d, %r12d
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	32(%rbp), %rax
	movl	%ecx, -128(%rbp)
	movq	%rdx, -152(%rbp)
	movq	%rax, -120(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rax
	leaq	16(%rbp), %rdi
	movq	8(%rax), %rbx
	call	_ZNK2v88internal8compiler21SharedFunctionInfoRef6objectEv@PLT
	movl	-128(%rbp), %r10d
	xorl	%ecx, %ecx
	movl	%r13d, %esi
	movq	%rax, %r8
	movq	%rbx, %rdi
	leal	1(%r10), %edx
	call	_ZN2v88internal8compiler21CommonOperatorBuilder28CreateFrameStateFunctionInfoENS1_14FrameStateTypeEiiNS0_6HandleINS0_18SharedFunctionInfoEEE@PLT
	movq	$-1, %rdx
	movl	%r12d, %esi
	movq	%rax, %rcx
	movq	32(%r15), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder10FrameStateENS0_9BailoutIdENS1_23OutputFrameStateCombineEPKNS1_22FrameStateFunctionInfoE@PLT
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, -136(%rbp)
	movq	32(%r15), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder11StateValuesEiNS1_15SparseInputMaskE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	32(%r15), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	-128(%rbp), %r10d
	movq	16(%r15), %rdi
	movq	%rax, -144(%rbp)
	testl	%r10d, %r10d
	js	.L38
	leaq	32(%r14), %r9
	xorl	%ebx, %ebx
	xorl	%edx, %edx
	xorl	%r12d, %r12d
	xorl	%r13d, %r13d
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L55:
	leaq	32(%r14,%rcx), %rax
	movq	(%rax), %rcx
	cmpq	%r12, %rdx
	je	.L23
.L56:
	movq	%rcx, (%r12)
	addq	$8, %r12
	cmpl	%ebx, %r10d
	jl	.L54
.L20:
	movzbl	23(%r14), %eax
	addq	$1, %rbx
	leaq	0(,%rbx,8), %rcx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L55
	movq	(%r9), %rax
	leaq	16(%rax,%rcx), %rax
	movq	(%rax), %rcx
	cmpq	%r12, %rdx
	jne	.L56
.L23:
	subq	%r13, %rdx
	movq	%rdx, %rax
	movq	%rdx, %r8
	sarq	$3, %rax
	cmpq	$268435455, %rax
	je	.L57
	testq	%rax, %rax
	je	.L39
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L58
	movl	$2147483640, %esi
	movl	$2147483640, %edx
.L26:
	movq	16(%rdi), %rax
	movq	24(%rdi), %r11
	subq	%rax, %r11
	cmpq	%rsi, %r11
	jb	.L59
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L29:
	movq	%rcx, (%rax,%r8)
	addq	%rax, %rdx
	leaq	8(%rax), %rsi
	cmpq	%r13, %r12
	jne	.L60
.L42:
	movq	%rsi, %r12
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L58:
	testq	%rdx, %rdx
	jne	.L61
	xorl	%eax, %eax
	movl	$8, %esi
	xorl	%edx, %edx
	movq	%rcx, (%rax,%r8)
	cmpq	%r13, %r12
	je	.L42
.L60:
	subq	$8, %r12
	leaq	15(%rax), %rcx
	subq	%r13, %r12
	subq	%r13, %rcx
	movq	%r12, %r8
	shrq	$3, %r8
	cmpq	$30, %rcx
	jbe	.L43
	movabsq	$2305843009213693948, %rsi
	testq	%rsi, %r8
	je	.L43
	addq	$1, %r8
	xorl	%ecx, %ecx
	movq	%r8, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L32:
	movdqu	0(%r13,%rcx), %xmm1
	movups	%xmm1, (%rax,%rcx)
	addq	$16, %rcx
	cmpq	%rsi, %rcx
	jne	.L32
	movq	%r8, %rcx
	andq	$-2, %rcx
	leaq	0(,%rcx,8), %rsi
	addq	%rsi, %r13
	addq	%rax, %rsi
	cmpq	%r8, %rcx
	je	.L34
	movq	0(%r13), %rcx
	movq	%rcx, (%rsi)
.L34:
	leaq	16(%rax,%r12), %r12
.L30:
	movq	%rax, %r13
	cmpl	%ebx, %r10d
	jge	.L20
	.p2align 4,,10
	.p2align 3
.L54:
	subq	%r13, %r12
	sarq	$3, %r12
.L19:
	movq	32(%r15), %rax
	xorl	%edx, %edx
	movl	%r12d, %esi
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder11StateValuesEiNS1_15SparseInputMaskE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	%r12d, %edx
	movq	%rax, %rsi
	movq	32(%r15), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	cmpq	$0, -120(%rbp)
	movq	%rax, -128(%rbp)
	je	.L62
.L35:
	movzbl	23(%r14), %edx
	movq	32(%r15), %rax
	andl	$15, %edx
	movq	(%rax), %rdi
	movq	32(%r14), %rax
	cmpl	$15, %edx
	jne	.L36
	movq	16(%rax), %rax
.L36:
	movq	-144(%rbp), %xmm2
	movq	-128(%rbp), %xmm0
	xorl	%r8d, %r8d
	leaq	-112(%rbp), %rcx
	movq	-136(%rbp), %rsi
	movl	$6, %edx
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -112(%rbp)
	movdqa	%xmm2, %xmm0
	movhps	-120(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	%rax, %xmm0
	movhps	-152(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-56(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L63
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	.cfi_restore_state
	movl	$8, %esi
	movl	$8, %edx
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L43:
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L31:
	movq	0(%r13,%rcx,8), %rsi
	movq	%rsi, (%rax,%rcx,8)
	movq	%rcx, %rsi
	addq	$1, %rcx
	cmpq	%r8, %rsi
	jne	.L31
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L62:
	movq	32(%r15), %rdi
	call	_ZN2v88internal8compiler7JSGraph17UndefinedConstantEv@PLT
	movq	%rax, -120(%rbp)
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L38:
	xorl	%r12d, %r12d
	xorl	%r13d, %r13d
	jmp	.L19
.L59:
	movl	%r10d, -188(%rbp)
	movq	%r9, -184(%rbp)
	movq	%rdx, -176(%rbp)
	movq	%r8, -168(%rbp)
	movq	%rcx, -160(%rbp)
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-128(%rbp), %rdi
	movq	-160(%rbp), %rcx
	movq	-168(%rbp), %r8
	movq	-176(%rbp), %rdx
	movq	-184(%rbp), %r9
	movl	-188(%rbp), %r10d
	jmp	.L29
.L57:
	leaq	.LC3(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L63:
	call	__stack_chk_fail@PLT
.L61:
	cmpq	$268435455, %rdx
	movl	$268435455, %eax
	cmova	%rax, %rdx
	salq	$3, %rdx
	movq	%rdx, %rsi
	jmp	.L26
	.cfi_endproc
.LFE22121:
	.size	_ZN2v88internal8compiler9JSInliner26CreateArtificialFrameStateEPNS1_4NodeES4_iNS0_9BailoutIdENS1_14FrameStateTypeENS1_21SharedFunctionInfoRefES4_, .-_ZN2v88internal8compiler9JSInliner26CreateArtificialFrameStateEPNS1_4NodeES4_iNS0_9BailoutIdENS1_14FrameStateTypeENS1_21SharedFunctionInfoRefES4_
	.section	.rodata._ZN2v88internal8compiler9JSInliner19DetermineCallTargetEPNS1_4NodeE.str1.1,"aMS",@progbits,1
.LC4:
	.string	"IsHeapObject()"
.LC5:
	.string	"storage_.is_populated_"
	.section	.text._ZN2v88internal8compiler9JSInliner19DetermineCallTargetEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler9JSInliner19DetermineCallTargetEPNS1_4NodeE
	.type	_ZN2v88internal8compiler9JSInliner19DetermineCallTargetEPNS1_4NodeE, @function
_ZN2v88internal8compiler9JSInliner19DetermineCallTargetEPNS1_4NodeE:
.LFB22123:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$88, %rsp
	movq	32(%rdx), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rdx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L65
	movq	16(%r13), %r13
.L65:
	movq	0(%r13), %rax
	cmpw	$30, 16(%rax)
	jne	.L66
	movq	48(%rax), %r15
	leaq	-96(%rbp), %r14
	movq	40(%rbx), %rsi
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	movq	%r15, %rdx
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L68
	leaq	-80(%rbp), %r8
	movdqa	-96(%rbp), %xmm0
	movq	%r8, %rdi
	movq	%r8, -120(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef12IsJSFunctionEv@PLT
	movq	-120(%rbp), %r8
	testb	%al, %al
	jne	.L89
.L66:
	movq	0(%r13), %rdi
	cmpw	$723, 16(%rdi)
	je	.L90
.L87:
	movb	$0, (%r12)
	movb	$0, 8(%r12)
.L64:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L91
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L89:
	.cfi_restore_state
	movq	40(%rbx), %rsi
	movq	%r8, %rdi
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	-120(%rbp), %r8
	movq	%r8, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L68
	movq	-120(%rbp), %r8
	leaq	-112(%rbp), %r13
	movq	%r8, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12AsJSFunctionEv@PLT
	movq	%r13, %rdi
	movq	%rax, -112(%rbp)
	movq	%rdx, -104(%rbp)
	call	_ZNK2v88internal8compiler13JSFunctionRef19has_feedback_vectorEv@PLT
	movq	-120(%rbp), %r8
	testb	%al, %al
	je	.L87
	movq	%r13, %rdi
	movq	%r8, -120(%rbp)
	call	_ZNK2v88internal8compiler13JSFunctionRef14native_contextEv@PLT
	movq	-120(%rbp), %r8
	movq	%rax, -96(%rbp)
	movq	40(%rbx), %rax
	movq	%rdx, -88(%rbp)
	cmpb	$0, 24(%rax)
	je	.L92
	movdqu	32(%rax), %xmm2
	movq	%r8, %rsi
	movq	%r14, %rdi
	movaps	%xmm2, -80(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef6equalsERKS2_@PLT
	testb	%al, %al
	je	.L87
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler13JSFunctionRef6sharedEv@PLT
	movb	$1, (%r12)
	movq	%rax, 8(%r12)
	movq	%rdx, 16(%r12)
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L90:
	call	_ZN2v88internal8compiler25CreateClosureParametersOfEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %r14
	movq	40(%rbx), %rsi
	movl	$1, %ecx
	movq	8(%rax), %rdx
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler15FeedbackCellRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r14, %rdi
	leaq	-80(%rbp), %r14
	call	_ZNK2v88internal8compiler15FeedbackCellRef5valueEv@PLT
	movq	%r14, %rdi
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef16IsFeedbackVectorEv@PLT
	testb	%al, %al
	je	.L87
	movq	0(%r13), %rdx
	movq	40(%rbx), %rsi
	movl	$1, %ecx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21SharedFunctionInfoRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movdqa	-80(%rbp), %xmm1
	movb	$1, (%r12)
	movups	%xmm1, 8(%r12)
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L68:
	leaq	.LC4(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L92:
	leaq	.LC5(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L91:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22123:
	.size	_ZN2v88internal8compiler9JSInliner19DetermineCallTargetEPNS1_4NodeE, .-_ZN2v88internal8compiler9JSInliner19DetermineCallTargetEPNS1_4NodeE
	.section	.rodata._ZN2v88internal8compiler9JSInliner20DetermineCallContextEPNS1_4NodeEPS4_.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"function.has_feedback_vector()"
	.section	.rodata._ZN2v88internal8compiler9JSInliner20DetermineCallContextEPNS1_4NodeEPS4_.str1.1,"aMS",@progbits,1
.LC7:
	.string	"unreachable code"
	.section	.text._ZN2v88internal8compiler9JSInliner20DetermineCallContextEPNS1_4NodeEPS4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler9JSInliner20DetermineCallContextEPNS1_4NodeEPS4_
	.type	_ZN2v88internal8compiler9JSInliner20DetermineCallContextEPNS1_4NodeEPS4_, @function
_ZN2v88internal8compiler9JSInliner20DetermineCallContextEPNS1_4NodeEPS4_:
.LFB22124:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	32(%rsi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L94
	movq	16(%r12), %r12
.L94:
	movq	(%r12), %rax
	cmpw	$30, 16(%rax)
	jne	.L95
	movq	48(%rax), %r15
	leaq	-96(%rbp), %r14
	movq	40(%rbx), %rsi
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	movq	%r15, %rdx
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L97
	leaq	-80(%rbp), %r8
	movdqa	-96(%rbp), %xmm0
	movq	%r8, %rdi
	movq	%r8, -104(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef12IsJSFunctionEv@PLT
	movq	-104(%rbp), %r8
	testb	%al, %al
	jne	.L113
.L95:
	movq	(%r12), %rdi
	cmpw	$723, 16(%rdi)
	jne	.L114
	call	_ZN2v88internal8compiler25CreateClosureParametersOfEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %r14
	movq	40(%rbx), %rsi
	movl	$1, %ecx
	movq	8(%rax), %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler15FeedbackCellRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties15GetContextInputEPNS1_4NodeE@PLT
	movq	%r14, %rdi
	movq	%rax, 0(%r13)
	call	_ZNK2v88internal8compiler15FeedbackCellRef5valueEv@PLT
	leaq	-80(%rbp), %rdi
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef16AsFeedbackVectorEv@PLT
.L102:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L115
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L113:
	.cfi_restore_state
	movq	40(%rbx), %rsi
	movq	%r8, %rdi
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	-104(%rbp), %r8
	movq	%r8, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L97
	movq	-104(%rbp), %r8
	movq	%r8, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12AsJSFunctionEv@PLT
	movq	%r14, %rdi
	movq	%rax, -96(%rbp)
	movq	%rdx, -88(%rbp)
	call	_ZNK2v88internal8compiler13JSFunctionRef19has_feedback_vectorEv@PLT
	movq	-104(%rbp), %r8
	testb	%al, %al
	je	.L116
	movq	%r14, %rdi
	movq	%r8, -104(%rbp)
	movq	32(%rbx), %r12
	call	_ZNK2v88internal8compiler13JSFunctionRef7contextEv@PLT
	movq	-104(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	movq	%r8, %rsi
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	%r14, %rdi
	movq	%rax, 0(%r13)
	call	_ZNK2v88internal8compiler13JSFunctionRef15feedback_vectorEv@PLT
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L97:
	leaq	.LC4(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L116:
	leaq	.LC6(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L115:
	call	__stack_chk_fail@PLT
.L114:
	leaq	.LC7(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE22124:
	.size	_ZN2v88internal8compiler9JSInliner20DetermineCallContextEPNS1_4NodeEPS4_, .-_ZN2v88internal8compiler9JSInliner20DetermineCallContextEPNS1_4NodeEPS4_
	.section	.text._ZNK2v88internal8compiler9JSInliner5graphEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler9JSInliner5graphEv
	.type	_ZNK2v88internal8compiler9JSInliner5graphEv, @function
_ZNK2v88internal8compiler9JSInliner5graphEv:
.LFB22136:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	movq	(%rax), %rax
	ret
	.cfi_endproc
.LFE22136:
	.size	_ZNK2v88internal8compiler9JSInliner5graphEv, .-_ZNK2v88internal8compiler9JSInliner5graphEv
	.section	.text._ZNK2v88internal8compiler9JSInliner10javascriptEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler9JSInliner10javascriptEv
	.type	_ZNK2v88internal8compiler9JSInliner10javascriptEv, @function
_ZNK2v88internal8compiler9JSInliner10javascriptEv:
.LFB22137:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	movq	368(%rax), %rax
	ret
	.cfi_endproc
.LFE22137:
	.size	_ZNK2v88internal8compiler9JSInliner10javascriptEv, .-_ZNK2v88internal8compiler9JSInliner10javascriptEv
	.section	.text._ZNK2v88internal8compiler9JSInliner6commonEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler9JSInliner6commonEv
	.type	_ZNK2v88internal8compiler9JSInliner6commonEv, @function
_ZNK2v88internal8compiler9JSInliner6commonEv:
.LFB22138:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	movq	8(%rax), %rax
	ret
	.cfi_endproc
.LFE22138:
	.size	_ZNK2v88internal8compiler9JSInliner6commonEv, .-_ZNK2v88internal8compiler9JSInliner6commonEv
	.section	.text._ZNK2v88internal8compiler9JSInliner10simplifiedEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler9JSInliner10simplifiedEv
	.type	_ZNK2v88internal8compiler9JSInliner10simplifiedEv, @function
_ZNK2v88internal8compiler9JSInliner10simplifiedEv:
.LFB22139:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	movq	376(%rax), %rax
	ret
	.cfi_endproc
.LFE22139:
	.size	_ZNK2v88internal8compiler9JSInliner10simplifiedEv, .-_ZNK2v88internal8compiler9JSInliner10simplifiedEv
	.section	.text._ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	.type	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_, @function
_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_:
.LFB25630:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r14
	movq	8(%rdi), %rbx
	movq	%r14, %rax
	subq	%rbx, %rax
	sarq	$3, %rax
	cmpq	$268435455, %rax
	je	.L159
	movq	%rsi, %r8
	movq	%rdi, %r13
	movq	%rsi, %r12
	movq	%rdx, %r15
	subq	%rbx, %r8
	testq	%rax, %rax
	je	.L137
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L160
	movl	$2147483640, %esi
	movl	$2147483640, %edx
.L123:
	movq	0(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	%rsi, %rcx
	jb	.L161
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L126:
	leaq	(%rax,%rdx), %rdi
	leaq	8(%rax), %rcx
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L160:
	testq	%rdx, %rdx
	jne	.L162
	movl	$8, %ecx
	xorl	%edi, %edi
	xorl	%eax, %eax
.L124:
	movq	(%r15), %rdx
	movq	%rdx, (%rax,%r8)
	cmpq	%rbx, %r12
	je	.L127
	leaq	-8(%r12), %r8
	leaq	15(%rax), %rdx
	subq	%rbx, %r8
	subq	%rbx, %rdx
	movq	%r8, %rcx
	shrq	$3, %rcx
	cmpq	$30, %rdx
	jbe	.L140
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rcx
	je	.L140
	leaq	1(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L129:
	movdqu	(%rbx,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L129
	movq	%rsi, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rcx
	addq	%rcx, %rbx
	addq	%rax, %rcx
	cmpq	%rdx, %rsi
	je	.L131
	movq	(%rbx), %rdx
	movq	%rdx, (%rcx)
.L131:
	leaq	16(%rax,%r8), %rcx
.L127:
	cmpq	%r14, %r12
	je	.L132
	subq	%r12, %r14
	leaq	15(%r12), %rdx
	leaq	-8(%r14), %r9
	subq	%rcx, %rdx
	movq	%r9, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rdx
	jbe	.L141
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rsi
	je	.L141
	leaq	1(%rsi), %r8
	xorl	%edx, %edx
	movq	%r8, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L134:
	movdqu	(%r12,%rdx), %xmm2
	movups	%xmm2, (%rcx,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rsi
	jne	.L134
	movq	%r8, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rsi
	leaq	(%rcx,%rsi), %r10
	addq	%rsi, %r12
	cmpq	%rdx, %r8
	je	.L136
	movq	(%r12), %rdx
	movq	%rdx, (%r10)
.L136:
	leaq	8(%rcx,%r9), %rcx
.L132:
	movq	%rax, %xmm0
	movq	%rcx, %xmm3
	movq	%rdi, 24(%r13)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 8(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L137:
	.cfi_restore_state
	movl	$8, %esi
	movl	$8, %edx
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L141:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L133:
	movq	(%r12,%rdx,8), %r8
	movq	%r8, (%rcx,%rdx,8)
	movq	%rdx, %r8
	addq	$1, %rdx
	cmpq	%rsi, %r8
	jne	.L133
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L140:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L128:
	movq	(%rbx,%rdx,8), %rsi
	movq	%rsi, (%rax,%rdx,8)
	movq	%rdx, %rsi
	addq	$1, %rdx
	cmpq	%rsi, %rcx
	jne	.L128
	jmp	.L131
.L161:
	movq	%r8, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %r8
	jmp	.L126
.L159:
	leaq	.LC3(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L162:
	cmpq	$268435455, %rdx
	movl	$268435455, %eax
	cmova	%rax, %rdx
	salq	$3, %rdx
	movq	%rdx, %rsi
	jmp	.L123
	.cfi_endproc
.LFE25630:
	.size	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_, .-_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	.section	.text._ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_,"axG",@progbits,_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_
	.type	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_, @function
_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_:
.LFB25644:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	16(%rdi), %r13
	cmpq	24(%rdi), %r13
	je	.L164
	movq	(%rsi), %rax
	movq	%rax, 0(%r13)
	addq	$8, 16(%rdi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L164:
	.cfi_restore_state
	movq	8(%rdi), %r14
	movq	%r13, %rcx
	subq	%r14, %rcx
	movq	%rcx, %rax
	sarq	$3, %rax
	cmpq	$268435455, %rax
	je	.L189
	testq	%rax, %rax
	je	.L176
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L190
	movl	$2147483640, %esi
	movl	$2147483640, %r15d
.L167:
	movq	(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L191
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L170:
	leaq	(%rax,%r15), %rdi
	leaq	8(%rax), %rdx
.L168:
	movq	(%r12), %rsi
	movq	%rsi, (%rax,%rcx)
	cmpq	%r14, %r13
	je	.L171
	leaq	-8(%r13), %rdx
	movq	%rdx, %rcx
	leaq	15(%rax), %rdx
	subq	%r14, %rcx
	subq	%r14, %rdx
	movq	%rcx, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rdx
	jbe	.L179
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rsi
	je	.L179
	leaq	1(%rsi), %r8
	xorl	%edx, %edx
	movq	%r8, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L173:
	movdqu	(%r14,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rsi, %rdx
	jne	.L173
	movq	%r8, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rsi
	addq	%rsi, %r14
	addq	%rax, %rsi
	cmpq	%rdx, %r8
	je	.L175
	movq	(%r14), %rdx
	movq	%rdx, (%rsi)
.L175:
	leaq	16(%rax,%rcx), %rdx
.L171:
	movq	%rax, %xmm0
	movq	%rdx, %xmm2
	movq	%rdi, 24(%rbx)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 8(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L190:
	.cfi_restore_state
	testq	%rdx, %rdx
	jne	.L192
	movl	$8, %edx
	xorl	%edi, %edi
	xorl	%eax, %eax
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L176:
	movl	$8, %esi
	movl	$8, %r15d
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L179:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L172:
	movq	(%r14,%rdx,8), %r8
	movq	%r8, (%rax,%rdx,8)
	movq	%rdx, %r8
	addq	$1, %rdx
	cmpq	%r8, %rsi
	jne	.L172
	jmp	.L175
.L191:
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	jmp	.L170
.L189:
	leaq	.LC3(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L192:
	movl	$268435455, %r15d
	cmpq	$268435455, %rdx
	cmova	%r15, %rdx
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rsi
	jmp	.L167
	.cfi_endproc
.LFE25644:
	.size	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_, .-_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_
	.section	.rodata._ZN2v88internal8compiler9JSInliner10InlineCallEPNS1_4NodeES4_S4_S4_S4_S4_S4_RKNS0_10ZoneVectorIS4_EE.str1.1,"aMS",@progbits,1
.LC8:
	.string	"Inlinee contains "
	.section	.rodata._ZN2v88internal8compiler9JSInliner10InlineCallEPNS1_4NodeES4_S4_S4_S4_S4_S4_RKNS0_10ZoneVectorIS4_EE.str1.8,"aMS",@progbits,1
	.align 8
.LC9:
	.string	" calls without local exception handler; "
	.align 8
.LC10:
	.string	"linking to surrounding exception handler."
	.section	.rodata._ZN2v88internal8compiler9JSInliner10InlineCallEPNS1_4NodeES4_S4_S4_S4_S4_S4_RKNS0_10ZoneVectorIS4_EE.str1.1
.LC11:
	.string	"\n"
	.section	.text._ZN2v88internal8compiler9JSInliner10InlineCallEPNS1_4NodeES4_S4_S4_S4_S4_S4_RKNS0_10ZoneVectorIS4_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler9JSInliner10InlineCallEPNS1_4NodeES4_S4_S4_S4_S4_S4_RKNS0_10ZoneVectorIS4_EE
	.type	_ZN2v88internal8compiler9JSInliner10InlineCallEPNS1_4NodeES4_S4_S4_S4_S4_S4_RKNS0_10ZoneVectorIS4_EE, @function
_ZN2v88internal8compiler9JSInliner10InlineCallEPNS1_4NodeES4_S4_S4_S4_S4_S4_RKNS0_10ZoneVectorIS4_EE:
.LFB22116:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%r9, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r14, %rdi
	subq	$584, %rsp
	movq	16(%rbp), %rax
	movq	%rsi, -560(%rbp)
	xorl	%esi, %esi
	movq	%rdx, -600(%rbp)
	movq	%rax, -592(%rbp)
	movq	24(%rbp), %rax
	movq	%rcx, -616(%rbp)
	movq	%rax, -544(%rbp)
	movq	32(%rbp), %rax
	movq	%r8, -576(%rbp)
	movq	%rax, -608(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	%rax, -568(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	24(%r12), %r15
	testq	%r15, %r15
	je	.L194
	movq	%rax, %r13
	movq	(%r12), %rax
	movq	(%r15), %r12
	movl	32(%rax), %eax
	leal	-3(%rax), %ecx
	movl	%ecx, -536(%rbp)
	leal	-2(%rax), %ecx
	subl	$1, %eax
	movl	%eax, -584(%rbp)
	movq	(%r14), %rax
	movl	%ecx, -580(%rbp)
	movl	20(%rax), %eax
	movl	%eax, -552(%rbp)
	subl	$2, %eax
	movl	%eax, -620(%rbp)
	.p2align 4,,10
	.p2align 3
.L212:
	movl	16(%r15), %edx
	movl	%edx, %eax
	shrl	%eax
	leaq	3(%rax,%rax,2), %rcx
	salq	$3, %rax
	andl	$1, %edx
	leaq	(%r15,%rcx,8), %rsi
	movq	(%rsi), %rcx
	je	.L195
	leaq	32(%rsi,%rax), %r14
	movq	%rcx, %rdi
.L196:
	movq	%r15, %xmm1
	movq	%r14, %xmm2
	cmpw	$50, 16(%rdi)
	punpcklqdq	%xmm2, %xmm1
	movaps	%xmm1, -528(%rbp)
	je	.L310
	movq	%r15, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler14NodeProperties12IsEffectEdgeENS1_4EdgeE@PLT
	testb	%al, %al
	je	.L205
	movq	(%r14), %rdi
	cmpq	%rdi, %r13
	je	.L201
	testq	%rdi, %rdi
	je	.L207
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L207:
	movq	%r13, (%r14)
	testq	%r13, %r13
	je	.L201
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	.p2align 4,,10
	.p2align 3
.L201:
	testq	%r12, %r12
	je	.L194
.L311:
	movq	%r12, %r15
	movq	(%r12), %r12
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L195:
	leaq	16(%rsi,%rax), %r14
	movq	(%rcx), %rdi
	movq	%rcx, %rsi
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L310:
	movq	%rsi, -528(%rbp)
	call	_ZN2v88internal8compiler16ParameterIndexOfEPKNS1_8OperatorE@PLT
	movl	-536(%rbp), %ecx
	movl	-552(%rbp), %edx
	addl	$1, %eax
	movq	-528(%rbp), %rsi
	cmpl	%edx, %ecx
	cmovle	%ecx, %edx
	cmpl	%eax, %edx
	jle	.L198
	movq	-560(%rbp), %rdx
	cltq
	salq	$3, %rax
	leaq	32(%rdx), %rcx
	movzbl	23(%rdx), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L199
	addq	%rcx, %rax
.L200:
	movq	8(%rbx), %rdi
	movq	(%rax), %rdx
	movq	(%rdi), %rcx
	call	*16(%rcx)
	testq	%r12, %r12
	jne	.L311
	.p2align 4,,10
	.p2align 3
.L194:
	cmpq	$0, -544(%rbp)
	je	.L213
	movq	-608(%rbp), %rax
	movq	8(%rax), %r11
	movq	16(%rax), %rax
	movq	%rax, %r14
	movq	%rax, -528(%rbp)
	subq	%r11, %r14
	sarq	$3, %r14
	movq	%r14, -536(%rbp)
	testl	%r14d, %r14d
	jle	.L214
	cmpb	$0, _ZN2v88internal25FLAG_trace_turbo_inliningE(%rip)
	jne	.L312
.L215:
	movq	$0, -456(%rbp)
	movq	16(%rbx), %rax
	movq	$0, -448(%rbp)
	movq	%rax, -464(%rbp)
	movq	$0, -440(%rbp)
	cmpq	%r11, -528(%rbp)
	je	.L313
.L252:
	leaq	-432(%rbp), %rax
	movq	%r11, %r15
	leaq	-400(%rbp), %r12
	movq	%rax, -552(%rbp)
	.p2align 4,,10
	.p2align 3
.L221:
	movq	32(%rbx), %rax
	movq	(%r15), %r13
	movq	8(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9IfSuccessEv@PLT
	movq	%r14, %rdi
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r13, -400(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rdx
	movq	%r13, %rsi
	movq	%rax, %r14
	movq	%rax, %rcx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler14NodeProperties11ReplaceUsesEPNS1_4NodeES4_S4_S4_S4_@PLT
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler14NodeProperties19ReplaceControlInputEPNS1_4NodeES4_i@PLT
	movq	32(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder11IfExceptionEv@PLT
	movq	%r13, %xmm0
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movq	%rax, %rsi
	punpcklqdq	%xmm0, %xmm0
	movl	$2, %edx
	movq	%r14, %rdi
	movaps	%xmm0, -400(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-448(%rbp), %rsi
	movq	%rax, -432(%rbp)
	cmpq	-440(%rbp), %rsi
	je	.L218
	movq	%rax, (%rsi)
	addq	$8, %r15
	addq	$8, -448(%rbp)
	cmpq	%r15, -528(%rbp)
	jne	.L221
	movl	-536(%rbp), %eax
	movq	32(%rbx), %r12
	testl	%eax, %eax
	jg	.L216
.L217:
	movq	352(%r12), %r8
	testq	%r8, %r8
	je	.L314
.L234:
	movq	8(%rbx), %rdi
	movq	-544(%rbp), %rdx
	movq	(%rdi), %rax
	movq	%rdx, %rcx
	movq	%rdx, %rsi
	call	*32(%rax)
	.p2align 4,,10
	.p2align 3
.L213:
	movq	16(%rbx), %rax
	movq	-592(%rbp), %rcx
	movq	$0, -488(%rbp)
	movq	$0, -480(%rbp)
	movq	%rax, -496(%rbp)
	leaq	32(%rcx), %r14
	movq	%rax, -464(%rbp)
	movq	%rax, -432(%rbp)
	movzbl	23(%rcx), %eax
	movq	$0, -472(%rbp)
	movq	$0, -456(%rbp)
	andl	$15, %eax
	movq	$0, -448(%rbp)
	movq	$0, -440(%rbp)
	movq	$0, -424(%rbp)
	movq	$0, -416(%rbp)
	movq	$0, -408(%rbp)
	cmpl	$15, %eax
	jne	.L236
	movq	32(%rcx), %r14
	movl	8(%r14), %eax
	addq	$16, %r14
.L236:
	cltq
	leaq	(%r14,%rax,8), %r13
	cmpq	%r13, %r14
	je	.L237
	leaq	-504(%rbp), %r12
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L238:
	movl	$1, %esi
	movq	%r15, %rdi
	addq	$8, %r14
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	leaq	-496(%rbp), %rdi
	movq	%r12, %rsi
	movq	%rax, -504(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	leaq	-464(%rbp), %rdi
	movq	%r12, %rsi
	movq	%rax, -504(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	leaq	-432(%rbp), %rdi
	movq	%r12, %rsi
	movq	%rax, -504(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_
	cmpq	%r14, %r13
	je	.L315
.L243:
	movq	(%r14), %r15
	movq	(%r15), %rax
	movzwl	16(%rax), %eax
	cmpw	$16, %ax
	je	.L238
	jbe	.L316
	cmpw	$18, %ax
	jne	.L317
	.p2align 4,,10
	.p2align 3
.L240:
	movq	32(%rbx), %rax
	movq	%r15, %rdx
	addq	$8, %r14
	movq	8(%rax), %rsi
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler14NodeProperties17MergeControlToEndEPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS1_4NodeE@PLT
	movq	32(%rbx), %rax
	movq	8(%rbx), %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rsi
	movq	(%rdi), %rax
	call	*24(%rax)
	cmpq	%r14, %r13
	jne	.L243
.L315:
	movq	32(%rbx), %r13
	movq	-480(%rbp), %rax
	cmpq	%rax, -488(%rbp)
	je	.L244
	movq	-424(%rbp), %r14
	movq	-416(%rbp), %r12
	movq	8(%r13), %rdi
	movq	0(%r13), %r15
	subq	%r14, %r12
	sarq	$3, %r12
	movl	%r12d, %esi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	%r12d, %edx
	movq	%rax, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-480(%rbp), %rsi
	movq	%rax, -504(%rbp)
	cmpq	-472(%rbp), %rsi
	je	.L245
	movq	%rax, (%rsi)
	addq	$8, -480(%rbp)
.L246:
	movq	-448(%rbp), %rsi
	cmpq	-440(%rbp), %rsi
	je	.L247
	movq	-504(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, -448(%rbp)
.L248:
	movq	32(%rbx), %rax
	movq	-488(%rbp), %r15
	movl	%r12d, %edx
	movl	$8, %esi
	movq	-480(%rbp), %r13
	movq	8(%rax), %rdi
	movq	(%rax), %r14
	subq	%r15, %r13
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	sarq	$3, %r13
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	%r13d, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-456(%rbp), %rcx
	movq	-448(%rbp), %r14
	movl	%r12d, %esi
	movq	%rax, %r13
	movq	32(%rbx), %rax
	subq	%rcx, %r14
	movq	%rcx, -528(%rbp)
	movq	8(%rax), %rdi
	movq	(%rax), %r15
	sarq	$3, %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9EffectPhiEi@PLT
	xorl	%r8d, %r8d
	movl	%r14d, %edx
	movq	%r15, %rdi
	movq	-528(%rbp), %rcx
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%rbx), %rdi
	movq	-504(%rbp), %r8
	movq	%r13, %rdx
	movq	%rax, %rcx
	movq	-560(%rbp), %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r13, %rax
.L249:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L318
	addq	$584, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L205:
	.cfi_restore_state
	movq	-528(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler14NodeProperties13IsControlEdgeENS1_4EdgeE@PLT
	testb	%al, %al
	je	.L208
	movq	(%r14), %rdi
	cmpq	%rdi, -568(%rbp)
	je	.L201
	testq	%rdi, %rdi
	je	.L209
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L209:
	movq	-568(%rbp), %rax
	movq	%rax, (%r14)
	testq	%rax, %rax
	je	.L201
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L198:
	cmpl	%eax, -536(%rbp)
	je	.L319
	cmpl	%eax, -580(%rbp)
	je	.L320
	cmpl	%eax, -584(%rbp)
	jne	.L204
	movq	8(%rbx), %rdi
	movq	-616(%rbp), %rdx
	movq	(%rdi), %rax
	call	*16(%rax)
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L208:
	movq	-528(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler14NodeProperties16IsFrameStateEdgeENS1_4EdgeE@PLT
	testb	%al, %al
	je	.L241
	movq	(%r14), %rdi
	cmpq	%rdi, -576(%rbp)
	je	.L201
	testq	%rdi, %rdi
	je	.L211
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L211:
	movq	-576(%rbp), %rdi
	movq	%rdi, (%r14)
	testq	%rdi, %rdi
	je	.L201
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L316:
	cmpw	$11, %ax
	je	.L240
.L241:
	leaq	.LC7(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L317:
	cmpw	$21, %ax
	je	.L240
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L319:
	movq	8(%rbx), %rdi
	movq	-600(%rbp), %rdx
	movq	(%rdi), %rax
	call	*16(%rax)
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L199:
	movq	-560(%rbp), %rcx
	movq	32(%rcx), %rdx
	leaq	16(%rdx,%rax), %rax
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L204:
	movq	32(%rbx), %rdi
	movq	%rsi, -528(%rbp)
	call	_ZN2v88internal8compiler7JSGraph17UndefinedConstantEv@PLT
.L309:
	movq	8(%rbx), %rdi
	movq	%rax, %rdx
	movq	-528(%rbp), %rsi
	movq	(%rdi), %rax
	call	*16(%rax)
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L214:
	movq	$0, -456(%rbp)
	movq	16(%rbx), %rax
	movq	$0, -448(%rbp)
	movq	%rax, -464(%rbp)
	movq	$0, -440(%rbp)
	cmpq	-528(%rbp), %r11
	jne	.L252
	movq	32(%rbx), %r12
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L218:
	movq	-552(%rbp), %rdx
	leaq	-464(%rbp), %rdi
	addq	$8, %r15
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	cmpq	%r15, -528(%rbp)
	jne	.L221
	movl	-536(%rbp), %eax
	movq	32(%rbx), %r12
	testl	%eax, %eax
	jle	.L217
	.p2align 4,,10
	.p2align 3
.L216:
	movq	-536(%rbp), %r14
	movq	8(%r12), %rdi
	movq	(%r12), %r13
	movq	-456(%rbp), %r15
	movl	%r14d, %esi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
	movq	%r13, %rdi
	movq	%r15, %rcx
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movl	%r14d, %edx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-448(%rbp), %r15
	movq	16(%rbx), %rdi
	xorl	%esi, %esi
	movq	-456(%rbp), %r12
	movq	%rax, -496(%rbp)
	movq	%r15, %r13
	movq	%rdi, -432(%rbp)
	movq	$0, -424(%rbp)
	movq	$0, -416(%rbp)
	movq	$0, -408(%rbp)
	subq	%r12, %r13
	jne	.L321
.L222:
	leaq	-496(%rbp), %rdx
	leaq	-432(%rbp), %rdi
	movq	%rsi, -416(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	movq	32(%rbx), %rax
	movl	$8, %esi
	movq	-536(%rbp), %r14
	movq	-424(%rbp), %r15
	movq	8(%rax), %rdi
	movq	(%rax), %r12
	movl	%r14d, %edx
	leal	1(%r14), %r13d
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	xorl	%r8d, %r8d
	movl	%r13d, %edx
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-424(%rbp), %rcx
	movl	%r14d, %esi
	movq	%rax, %r12
	movq	32(%rbx), %rax
	movq	%rcx, -528(%rbp)
	movq	8(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9EffectPhiEi@PLT
	xorl	%r8d, %r8d
	movl	%r13d, %edx
	movq	%r15, %rdi
	movq	-528(%rbp), %rcx
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%rbx), %rdi
	movq	-496(%rbp), %r8
	movq	%r12, %rdx
	movq	%rax, %rcx
	movq	-544(%rbp), %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	jmp	.L213
.L237:
	movq	32(%rbx), %r13
	.p2align 4,,10
	.p2align 3
.L244:
	movq	352(%r13), %rdx
	movq	%rdx, %r12
	testq	%rdx, %rdx
	je	.L322
.L250:
	movq	%rdx, %r13
.L251:
	movq	8(%rbx), %rdi
	movq	-560(%rbp), %rbx
	movq	%r12, %r8
	movq	%r13, %rcx
	movq	(%rdi), %rax
	movq	%rbx, %rsi
	call	*32(%rax)
	movq	%rbx, %rax
	jmp	.L249
	.p2align 4,,10
	.p2align 3
.L320:
	pxor	%xmm0, %xmm0
	movq	32(%rbx), %rdi
	movq	%rsi, -528(%rbp)
	cvtsi2sdl	-620(%rbp), %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	jmp	.L309
.L321:
	movq	16(%rdi), %rsi
	movq	24(%rdi), %rax
	leaq	7(%r13), %r8
	andq	$-8, %r8
	subq	%rsi, %rax
	cmpq	%rax, %r8
	ja	.L323
	addq	%rsi, %r8
	movq	%r8, 16(%rdi)
.L224:
	cmpq	%r15, %r12
	je	.L229
	subq	$8, %r15
	leaq	15(%r12), %rax
	subq	%r12, %r15
	subq	%rsi, %rax
	movq	%r15, %rcx
	shrq	$3, %rcx
	cmpq	$30, %rax
	jbe	.L226
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rcx
	je	.L226
	addq	$1, %rcx
	movq	%r12, %rdi
	movq	%rsi, %rax
	movq	%rcx, %rdx
	subq	%rsi, %rdi
	shrq	%rdx
	salq	$4, %rdx
	addq	%rsi, %rdx
	.p2align 4,,10
	.p2align 3
.L227:
	movdqu	(%rax,%rdi), %xmm3
	addq	$16, %rax
	movups	%xmm3, -16(%rax)
	cmpq	%rax, %rdx
	jne	.L227
	movq	%rcx, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rdx
	addq	%rdx, %r12
	addq	%rsi, %rdx
	cmpq	%rcx, %rax
	je	.L229
	movq	(%r12), %rax
	movq	%rax, (%rdx)
.L229:
	movq	%rsi, -424(%rbp)
	addq	%r13, %rsi
	movq	%rsi, -408(%rbp)
	jmp	.L222
.L245:
	leaq	-504(%rbp), %rdx
	leaq	-496(%rbp), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L246
.L312:
	leaq	-320(%rbp), %r13
	leaq	-400(%rbp), %r12
	movq	%r13, %rdi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %r15
	call	_ZNSt8ios_baseC2Ev@PLT
	xorl	%edx, %edx
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movw	%dx, -96(%rbp)
	movq	stdout(%rip), %rdx
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	%r15, -320(%rbp)
	movq	$0, -104(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movq	%r12, %rdi
	movl	$17, %edx
	movq	%rax, -400(%rbp)
	leaq	.LC8(%rip), %rsi
	addq	$40, %rax
	movq	%rax, -320(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	-536(%rbp), %esi
	movq	%r12, %rdi
	call	_ZNSolsEi@PLT
	movl	$40, %edx
	leaq	.LC9(%rip), %rsi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$41, %edx
	leaq	.LC10(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	leaq	.LC11(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rcx
	movq	%rax, -320(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-336(%rbp), %rdi
	movq	%rax, %xmm4
	punpcklqdq	%xmm4, %xmm0
	movaps	%xmm0, -400(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%r13, %rdi
	movq	%r15, -320(%rbp)
	movq	%rax, -400(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-608(%rbp), %rax
	movq	8(%rax), %r11
	movq	16(%rax), %rax
	movq	%rax, -528(%rbp)
	jmp	.L215
.L247:
	leaq	-504(%rbp), %rdx
	leaq	-464(%rbp), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L248
.L322:
	movq	8(%r13), %rdi
	movq	0(%r13), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4DeadEv@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 352(%r13)
	movq	32(%rbx), %r14
	movq	%rax, %r12
	movq	352(%r14), %rdx
	testq	%rdx, %rdx
	jne	.L250
	movq	8(%r14), %rdi
	movq	(%r14), %r13
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4DeadEv@PLT
	xorl	%edx, %edx
	movq	%r13, %rdi
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 352(%r14)
	movq	32(%rbx), %r14
	movq	%rax, %r13
	movq	352(%r14), %rdx
	testq	%rdx, %rdx
	jne	.L251
	movq	8(%r14), %rdi
	movq	(%r14), %r15
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4DeadEv@PLT
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rax, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 352(%r14)
	movq	%rax, %rdx
	jmp	.L251
.L314:
	movq	8(%r12), %rdi
	movq	(%r12), %r13
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4DeadEv@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 352(%r12)
	movq	%rax, %r8
	jmp	.L234
.L313:
	movq	32(%rbx), %r12
	jmp	.L216
.L226:
	leaq	8(%rsi,%r15), %rcx
	movq	%rsi, %rax
	subq	%rsi, %r12
.L231:
	movq	(%rax,%r12), %rdx
	addq	$8, %rax
	movq	%rdx, -8(%rax)
	cmpq	%rax, %rcx
	jne	.L231
	jmp	.L229
.L323:
	movq	%r8, %rsi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rsi
	jmp	.L224
.L318:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22116:
	.size	_ZN2v88internal8compiler9JSInliner10InlineCallEPNS1_4NodeES4_S4_S4_S4_S4_S4_RKNS0_10ZoneVectorIS4_EE, .-_ZN2v88internal8compiler9JSInliner10InlineCallEPNS1_4NodeES4_S4_S4_S4_S4_S4_RKNS0_10ZoneVectorIS4_EE
	.section	.rodata._ZN2v88internal8compiler9JSInliner12ReduceJSCallEPNS1_4NodeE.str1.1,"aMS",@progbits,1
.LC12:
	.string	" (inside try-block)"
.LC13:
	.string	""
.LC14:
	.string	"Not inlining "
.LC15:
	.string	" into "
	.section	.rodata._ZN2v88internal8compiler9JSInliner12ReduceJSCallEPNS1_4NodeE.str1.8,"aMS",@progbits,1
	.align 8
.LC16:
	.string	" because constructor is not constructable."
	.align 8
.LC17:
	.string	" because callee is a class constructor."
	.align 8
.LC18:
	.string	" because call has exceeded the maximum depth for function inlining."
	.section	.rodata._ZN2v88internal8compiler9JSInliner12ReduceJSCallEPNS1_4NodeE.str1.1
.LC19:
	.string	"shared_info->is_compiled()"
.LC20:
	.string	"Inlining "
	.section	.rodata._ZN2v88internal8compiler9JSInliner12ReduceJSCallEPNS1_4NodeE.str1.8
	.align 8
.LC21:
	.string	"Missed opportunity to inline a function ("
	.section	.rodata._ZN2v88internal8compiler9JSInliner12ReduceJSCallEPNS1_4NodeE.str1.1
.LC22:
	.string	" with "
.LC23:
	.string	")"
	.section	.text._ZN2v88internal8compiler9JSInliner12ReduceJSCallEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler9JSInliner12ReduceJSCallEPNS1_4NodeE
	.type	_ZN2v88internal8compiler9JSInliner12ReduceJSCallEPNS1_4NodeE, @function
_ZN2v88internal8compiler9JSInliner12ReduceJSCallEPNS1_4NodeE:
.LFB22125:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	leaq	-560(%rbp), %rdi
	pushq	%rbx
	movq	%r12, %rsi
	subq	$744, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler9JSInliner19DetermineCallTargetEPNS1_4NodeE
	cmpb	$0, -560(%rbp)
	jne	.L325
.L330:
	xorl	%eax, %eax
.L326:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L411
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L325:
	.cfi_restore_state
	movq	24(%r12), %rax
	movq	40(%r12), %rsi
	movl	$1, %ecx
	leaq	-624(%rbp), %r15
	movq	%r15, %rdi
	movq	24(%rax), %rdx
	call	_ZN2v88internal8compiler21SharedFunctionInfoRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	0(%r13), %rax
	movzwl	16(%rax), %eax
	cmpw	$760, %ax
	je	.L412
.L327:
	cmpw	$755, %ax
	je	.L331
.L334:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler14NodeProperties18GetFrameStateInputEPNS1_4NodeE@PLT
	movl	$50, %ecx
	movq	(%rax), %rdx
	cmpw	$41, 16(%rdx)
	je	.L332
	jmp	.L333
	.p2align 4,,10
	.p2align 3
.L414:
	addq	$72, %rax
	movq	(%rax), %rax
	movq	(%rax), %rdx
	cmpw	$41, 16(%rdx)
	jne	.L333
.L338:
	subl	$1, %ecx
	je	.L413
.L332:
	movzbl	23(%rax), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L414
	movq	32(%rax), %rax
	addq	$56, %rax
	movq	(%rax), %rax
	movq	(%rax), %rdx
	cmpw	$41, 16(%rdx)
	je	.L338
.L333:
	movq	%r13, %rdi
	leaq	-640(%rbp), %rsi
	leaq	-552(%rbp), %r14
	movq	$0, -640(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties17IsExceptionalCallEPNS1_4NodeEPS4_@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21SharedFunctionInfoRef11is_compiledEv@PLT
	testb	%al, %al
	je	.L415
	cmpb	$0, _ZN2v88internal24FLAG_concurrent_inliningE(%rip)
	jne	.L340
	movq	24(%r12), %rax
	testb	$32, (%rax)
	jne	.L416
.L340:
	cmpb	$0, _ZN2v88internal25FLAG_trace_turbo_inliningE(%rip)
	jne	.L417
.L341:
	leaq	-632(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler9JSInliner20DetermineCallContextEPNS1_4NodeEPS4_
	cmpb	$0, _ZN2v88internal24FLAG_concurrent_inliningE(%rip)
	movq	%rax, -608(%rbp)
	movq	%rdx, -600(%rbp)
	je	.L346
	movq	-608(%rbp), %rsi
	movq	-600(%rbp), %rdx
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21SharedFunctionInfoRef26IsSerializedForCompilationENS1_17FeedbackVectorRefE@PLT
	testb	%al, %al
	je	.L418
.L346:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21SharedFunctionInfoRef16GetBytecodeArrayEv@PLT
	movq	24(%r12), %r8
	movq	48(%r12), %rdi
	movq	%r13, %rsi
	movq	%rdx, -584(%rbp)
	movq	%r8, -648(%rbp)
	movq	%rax, -592(%rbp)
	call	_ZNK2v88internal8compiler19SourcePositionTable17GetSourcePositionEPNS1_4NodeE@PLT
	leaq	-592(%rbp), %rdi
	movq	%rax, %rbx
	call	_ZNK2v88internal8compiler16BytecodeArrayRef6objectEv@PLT
	movq	%r14, %rdi
	movq	%rax, %r15
	call	_ZNK2v88internal8compiler21SharedFunctionInfoRef6objectEv@PLT
	movq	-648(%rbp), %r8
	movq	%r15, %rdx
	movq	%rbx, %rcx
	movq	%rax, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal24OptimizedCompilationInfo18AddInlinedFunctionENS0_6HandleINS0_18SharedFunctionInfoEEENS2_INS0_13BytecodeArrayEEENS0_14SourcePositionE@PLT
	movq	0(%r13), %rdi
	movl	%eax, %ebx
	movq	32(%r12), %rax
	movq	(%rax), %r15
	movq	8(%r15), %rax
	movq	%rax, -648(%rbp)
	movq	16(%r15), %rax
	movq	%rax, -656(%rbp)
	movq	24(%r12), %rax
	movl	(%rax), %esi
	movl	%esi, %edx
	andl	$8192, %edx
	cmpl	$1, %edx
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$7, %eax
	cmpl	$1, %edx
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$3, %edx
	andl	$64, %esi
	cmovne	%eax, %edx
	cmpw	$755, 16(%rdi)
	je	.L419
	movl	%edx, -672(%rbp)
	call	_ZN2v88internal8compiler21ConstructParametersOfEPKNS1_8OperatorE@PLT
	movl	-672(%rbp), %edx
	addq	$4, %rax
.L349:
	movss	(%rax), %xmm1
	movq	24(%r12), %rax
	subq	$8, %rsp
	leaq	-608(%rbp), %rcx
	movq	16(%r12), %rsi
	movq	40(%r12), %rdi
	movl	$-1, %r8d
	addq	$160, %rax
	movss	%xmm1, -496(%rbp)
	movq	-648(%rbp), %xmm0
	pushq	%rax
	pushq	%rdx
	movhps	-656(%rbp), %xmm0
	movq	%r14, %rdx
	pushq	%rbx
	leaq	-496(%rbp), %rbx
	pushq	48(%r12)
	pushq	%rbx
	movq	32(%r12), %r9
	movaps	%xmm0, -672(%rbp)
	movq	%rbx, -680(%rbp)
	call	_ZN2v88internal8compiler22BuildGraphFromBytecodeEPNS1_12JSHeapBrokerEPNS0_4ZoneERKNS1_21SharedFunctionInfoRefERKNS1_17FeedbackVectorRefENS0_9BailoutIdEPNS1_7JSGraphERKNS1_13CallFrequencyEPNS1_19SourcePositionTableEiNS_4base5FlagsINS1_24BytecodeGraphBuilderFlagEiEEPNS0_11TickCounterE@PLT
	movq	32(%r12), %rax
	addq	$48, %rsp
	movdqa	-672(%rbp), %xmm0
	cmpq	$0, -640(%rbp)
	movq	(%rax), %rax
	movq	8(%rax), %rcx
	movq	16(%rax), %rdx
	movups	%xmm0, 8(%r15)
	movq	$0, -520(%rbp)
	movq	16(%r12), %rsi
	movq	%rcx, -648(%rbp)
	movq	%rdx, -656(%rbp)
	movq	%rsi, -528(%rbp)
	movq	$0, -512(%rbp)
	movq	$0, -504(%rbp)
	je	.L350
	movq	32(%r12), %rax
	movq	%rbx, %rdi
	movl	$1, %r8d
	movq	(%rax), %rcx
	call	_ZN2v88internal8compiler8AllNodesC1EPNS0_4ZoneEPNS1_4NodeEPKNS1_5GraphEb@PLT
	movq	-480(%rbp), %rbx
	movq	-488(%rbp), %r15
	leaq	-576(%rbp), %rax
	movq	%rax, -672(%rbp)
	cmpq	%r15, %rbx
	jne	.L356
	jmp	.L350
	.p2align 4,,10
	.p2align 3
.L353:
	addq	$8, %r15
	cmpq	%r15, %rbx
	je	.L350
.L356:
	movq	(%r15), %rdi
	movq	%rdi, -576(%rbp)
	movq	(%rdi), %rax
	testb	$32, 18(%rax)
	jne	.L353
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties17IsExceptionalCallEPNS1_4NodeEPS4_@PLT
	testb	%al, %al
	jne	.L353
	movq	-512(%rbp), %rsi
	cmpq	-504(%rbp), %rsi
	je	.L355
	movq	-576(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, -512(%rbp)
	jmp	.L353
	.p2align 4,,10
	.p2align 3
.L331:
	leaq	-552(%rbp), %r14
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21SharedFunctionInfoRef4kindEv@PLT
	subl	$2, %eax
	cmpb	$3, %al
	ja	.L334
	cmpb	$0, _ZN2v88internal25FLAG_trace_turbo_inliningE(%rip)
	je	.L330
	leaq	-320(%rbp), %r13
	leaq	-400(%rbp), %r12
	movq	%r13, %rdi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rbx
	call	_ZNSt8ios_baseC2Ev@PLT
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movq	stdout(%rip), %rdx
	movw	%si, -96(%rbp)
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	%rbx, -320(%rbp)
	movq	$0, -104(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movq	%r12, %rdi
	movl	$13, %edx
	movq	%rax, -400(%rbp)
	leaq	.LC14(%rip), %rsi
	addq	$40, %rax
	movq	%rax, -320(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compilerlsERSoRKNS1_9ObjectRefE@PLT
	movl	$6, %edx
	leaq	.LC15(%rip), %rsi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_9ObjectRefE@PLT
	movl	$39, %edx
	leaq	.LC17(%rip), %rsi
	movq	%rax, %r12
	jmp	.L409
	.p2align 4,,10
	.p2align 3
.L350:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler14NodeProperties18GetFrameStateInputEPNS1_4NodeE@PLT
	movq	32(%r12), %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler7JSGraph17UndefinedConstantEv@PLT
	movq	0(%r13), %rdx
	movq	%rax, %rbx
	movzwl	16(%rdx), %eax
	cmpw	$760, %ax
	je	.L420
.L358:
	cmpw	$755, %ax
	je	.L421
.L376:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21SharedFunctionInfoRef31internal_formal_parameter_countEv@PLT
	movq	0(%r13), %rdx
	movl	20(%rdx), %ecx
	subl	$2, %ecx
	cmpl	%ecx, %eax
	je	.L383
	subq	$8, %rsp
	movq	%r15, %rdx
	movl	$1, %r9d
	movq	%r13, %rsi
	pushq	$0
	movl	$-1, %r8d
	movq	%r12, %rdi
	pushq	-544(%rbp)
	pushq	-552(%rbp)
	call	_ZN2v88internal8compiler9JSInliner26CreateArtificialFrameStateEPNS1_4NodeES4_iNS0_9BailoutIdENS1_14FrameStateTypeENS1_21SharedFunctionInfoRefES4_
	addq	$32, %rsp
	movq	%rax, %r15
.L383:
	subq	$8, %rsp
	movq	%r15, %r8
	movq	%rbx, %rdx
	movq	%r13, %rsi
	leaq	-528(%rbp), %rax
	movq	-648(%rbp), %r9
	movq	-632(%rbp), %rcx
	movq	%r12, %rdi
	pushq	%rax
	pushq	-640(%rbp)
	pushq	-656(%rbp)
	call	_ZN2v88internal8compiler9JSInliner10InlineCallEPNS1_4NodeES4_S4_S4_S4_S4_S4_RKNS0_10ZoneVectorIS4_EE
	addq	$32, %rsp
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L412:
	leaq	-552(%rbp), %r14
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21SharedFunctionInfoRef4kindEv@PLT
	cmpb	$5, %al
	jbe	.L422
	cmpb	$0, _ZN2v88internal25FLAG_trace_turbo_inliningE(%rip)
	je	.L330
	leaq	-320(%rbp), %r13
	leaq	-400(%rbp), %r12
	movq	%r13, %rdi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rbx
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	stdout(%rip), %rdx
	xorl	%edi, %edi
	pxor	%xmm0, %xmm0
	movw	%di, -96(%rbp)
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movq	%r12, %rdi
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	%rbx, -320(%rbp)
	movq	$0, -104(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movq	%r12, %rdi
	movl	$13, %edx
	movq	%rax, -400(%rbp)
	leaq	.LC14(%rip), %rsi
	addq	$40, %rax
	movq	%rax, -320(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compilerlsERSoRKNS1_9ObjectRefE@PLT
	movl	$6, %edx
	leaq	.LC15(%rip), %rsi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compilerlsERSoRKNS1_9ObjectRefE@PLT
	movl	$42, %edx
	leaq	.LC16(%rip), %rsi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	leaq	.LC11(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rcx
	movq	%rax, -320(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	movq	%rax, %xmm7
	punpcklqdq	%xmm7, %xmm0
	jmp	.L410
	.p2align 4,,10
	.p2align 3
.L413:
	cmpb	$0, _ZN2v88internal25FLAG_trace_turbo_inliningE(%rip)
	je	.L330
	leaq	-320(%rbp), %r13
	leaq	-400(%rbp), %r12
	movq	%r13, %rdi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rbx
	call	_ZNSt8ios_baseC2Ev@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	stdout(%rip), %rdx
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movw	%cx, -96(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	%rbx, -320(%rbp)
	movq	$0, -104(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movq	%r12, %rdi
	movl	$13, %edx
	movq	%rax, -400(%rbp)
	leaq	.LC14(%rip), %rsi
	addq	$40, %rax
	movq	%rax, -320(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rdi
	leaq	-552(%rbp), %rsi
	call	_ZN2v88internal8compilerlsERSoRKNS1_9ObjectRefE@PLT
	movl	$6, %edx
	leaq	.LC15(%rip), %rsi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_9ObjectRefE@PLT
	movl	$67, %edx
	leaq	.LC18(%rip), %rsi
	movq	%rax, %r12
.L409:
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	leaq	.LC11(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rcx
	movq	%rax, -320(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	movq	%rax, %xmm5
	punpcklqdq	%xmm5, %xmm0
	jmp	.L410
	.p2align 4,,10
	.p2align 3
.L417:
	leaq	-320(%rbp), %r8
	leaq	-400(%rbp), %rbx
	movq	%r8, %rdi
	movq	%r8, -648(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	xorl	%edx, %edx
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdi
	movw	%dx, -96(%rbp)
	movq	stdout(%rip), %rdx
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rcx
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movq	%rcx, -320(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	$0, -104(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movq	%rbx, %rdi
	movl	$9, %edx
	movq	%rax, -400(%rbp)
	leaq	.LC20(%rip), %rsi
	addq	$40, %rax
	movq	%rax, -320(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_9ObjectRefE@PLT
	movl	$6, %edx
	leaq	.LC15(%rip), %rsi
	movq	%rax, %rbx
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r15, %rsi
	movq	%rbx, %rdi
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rbx
	call	_ZN2v88internal8compilerlsERSoRKNS1_9ObjectRefE@PLT
	cmpq	$1, -640(%rbp)
	leaq	.LC12(%rip), %rsi
	sbbq	%rdx, %rdx
	movq	%rax, %r15
	leaq	.LC13(%rip), %rax
	notq	%rdx
	movq	%r15, %rdi
	andl	$19, %edx
	cmpq	$0, -640(%rbp)
	cmove	%rax, %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	leaq	.LC11(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	movq	%rbx, %xmm0
	leaq	-336(%rbp), %rdi
	movq	%rax, -320(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm3
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -400(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	-648(%rbp), %r8
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rcx
	movq	%rax, -400(%rbp)
	movq	%r8, %rdi
	movq	%rcx, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L418:
	cmpb	$0, _ZN2v88internal25FLAG_trace_turbo_inliningE(%rip)
	je	.L330
	leaq	-320(%rbp), %r13
	leaq	-400(%rbp), %r12
	movq	%r13, %rdi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rbx
	call	_ZNSt8ios_baseC2Ev@PLT
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	stdout(%rip), %rdx
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movw	%ax, -96(%rbp)
	movq	%rbx, -320(%rbp)
	movq	$0, -104(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movq	%r12, %rdi
	movl	$41, %edx
	movq	%rax, -400(%rbp)
	leaq	.LC21(%rip), %rsi
	addq	$40, %rax
	movq	%rax, -320(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compilerlsERSoRKNS1_9ObjectRefE@PLT
	movl	$6, %edx
	leaq	.LC22(%rip), %rsi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rdi
	leaq	-608(%rbp), %rsi
	call	_ZN2v88internal8compilerlsERSoRKNS1_9ObjectRefE@PLT
	movl	$1, %edx
	leaq	.LC23(%rip), %rsi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	leaq	.LC11(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rcx
	movq	%rax, -320(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	movq	%rax, %xmm6
	punpcklqdq	%xmm6, %xmm0
.L410:
	leaq	-336(%rbp), %rdi
	movaps	%xmm0, -400(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%r13, %rdi
	movq	%rbx, -320(%rbp)
	movq	%rax, -400(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L330
.L422:
	movq	0(%r13), %rax
	movzwl	16(%rax), %eax
	jmp	.L327
	.p2align 4,,10
	.p2align 3
.L421:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21SharedFunctionInfoRef13language_modeEv@PLT
	testb	%al, %al
	jne	.L376
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21SharedFunctionInfoRef6nativeEv@PLT
	testb	%al, %al
	jne	.L376
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	%rax, -672(%rbp)
	leaq	32(%r13), %rax
	movq	%rax, -704(%rbp)
	movzbl	23(%r13), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L378
	leaq	40(%r13), %rax
.L379:
	movq	40(%r12), %rdi
	movq	-672(%rbp), %rdx
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler14NodeProperties14CanBePrimitiveEPNS1_12JSHeapBrokerEPNS1_4NodeES6_@PLT
	testb	%al, %al
	je	.L376
	movq	0(%r13), %rdi
	call	_ZN2v88internal8compiler16CallParametersOfEPKNS1_8OperatorE@PLT
	movq	32(%r12), %r8
	movq	%rax, %rcx
	movq	40(%r12), %rax
	cmpb	$0, 24(%rax)
	je	.L423
	movdqu	32(%rax), %xmm7
	leaq	-576(%rbp), %rdi
	movq	%rcx, -704(%rbp)
	movq	%r8, -688(%rbp)
	movaps	%xmm7, -576(%rbp)
	call	_ZNK2v88internal8compiler16NativeContextRef19global_proxy_objectEv@PLT
	movq	-688(%rbp), %r8
	movq	-680(%rbp), %rsi
	movq	%rdx, -488(%rbp)
	movq	%r8, %rdi
	movq	%rax, -496(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	32(%r12), %rdx
	movq	-704(%rbp), %rcx
	movq	%rax, -680(%rbp)
	movzbl	23(%r13), %eax
	movq	(%rdx), %r9
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L381
	leaq	40(%r13), %rax
.L382:
	movl	(%rcx), %esi
	movq	(%rax), %rax
	movq	%r9, -688(%rbp)
	movq	-672(%rbp), %xmm0
	movq	376(%rdx), %rdi
	shrl	$29, %esi
	movq	%rax, -672(%rbp)
	movhps	-648(%rbp), %xmm0
	andl	$3, %esi
	movaps	%xmm0, -704(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder15ConvertReceiverENS0_19ConvertReceiverModeE@PLT
	movq	-688(%rbp), %r9
	xorl	%r8d, %r8d
	movl	$4, %edx
	movq	-672(%rbp), %xmm1
	movdqa	-704(%rbp), %xmm0
	leaq	-400(%rbp), %rcx
	movq	%rax, %rsi
	movq	%r9, %rdi
	movhps	-680(%rbp), %xmm1
	movaps	%xmm0, -384(%rbp)
	movaps	%xmm1, -400(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r13, %rdi
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%rax, -672(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties17ReplaceValueInputEPNS1_4NodeES4_i@PLT
	movq	-672(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler14NodeProperties18ReplaceEffectInputEPNS1_4NodeES4_i@PLT
	jmp	.L376
	.p2align 4,,10
	.p2align 3
.L420:
	movl	20(%rdx), %esi
	movzbl	23(%r13), %edx
	leaq	32(%r13), %rbx
	movq	%rbx, -688(%rbp)
	subl	$1, %esi
	andl	$15, %edx
	movslq	%esi, %rax
	salq	$3, %rax
	cmpl	$15, %edx
	je	.L359
	addq	%rbx, %rax
.L360:
	movq	%r13, %rdi
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler4Node11RemoveInputEi@PLT
	movq	32(%r12), %rax
	movl	$1, %edx
	movq	%r13, %rdi
	movq	%rbx, %rcx
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	32(%r12), %rdi
	call	_ZN2v88internal8compiler7JSGraph15TheHoleConstantEv@PLT
	movq	%r13, %rdi
	movq	%rax, -672(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetContextInputEPNS1_4NodeE@PLT
	movdqu	-552(%rbp), %xmm4
	movq	-680(%rbp), %rdi
	movq	%rax, -704(%rbp)
	movaps	%xmm4, -496(%rbp)
	call	_ZNK2v88internal8compiler21SharedFunctionInfoRef20construct_as_builtinEv@PLT
	testb	%al, %al
	je	.L361
.L364:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21SharedFunctionInfoRef4kindEv@PLT
	subl	$4, %eax
	cmpb	$1, %al
	jbe	.L424
.L363:
	movzbl	23(%r13), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L370
	movq	-688(%rbp), %rax
	movq	8(%rax), %rdi
	cmpq	%rdi, -672(%rbp)
	je	.L373
	addq	$8, %rax
	movq	%rax, %rdx
	movq	%r13, %rax
.L372:
	leaq	-48(%rax), %rsi
	testq	%rdi, %rdi
	je	.L374
	movq	%rdx, -720(%rbp)
	movq	%rsi, -688(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-720(%rbp), %rdx
	movq	-688(%rbp), %rsi
.L374:
	movq	-672(%rbp), %rax
	movq	%rax, (%rdx)
	testq	%rax, %rax
	je	.L373
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L373:
	movq	0(%r13), %rax
	subq	$8, %rsp
	movq	%r15, %rdx
	movq	%r13, %rsi
	movl	$2, %r9d
	movl	$2, %r8d
	movq	%r12, %rdi
	movl	20(%rax), %ecx
	pushq	-704(%rbp)
	pushq	-544(%rbp)
	pushq	-552(%rbp)
	subl	$2, %ecx
	call	_ZN2v88internal8compiler9JSInliner26CreateArtificialFrameStateEPNS1_4NodeES4_iNS0_9BailoutIdENS1_14FrameStateTypeENS1_21SharedFunctionInfoRefES4_
	addq	$32, %rsp
	movq	%rax, %r15
	movq	0(%r13), %rax
	movzwl	16(%rax), %eax
	jmp	.L358
	.p2align 4,,10
	.p2align 3
.L419:
	movl	%edx, -672(%rbp)
	call	_ZN2v88internal8compiler16CallParametersOfEPKNS1_8OperatorE@PLT
	movl	-672(%rbp), %edx
	addq	$4, %rax
	jmp	.L349
.L416:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21SharedFunctionInfoRef6objectEv@PLT
	movq	%rax, %rsi
	movq	32(%r12), %rax
	movq	360(%rax), %rdi
	call	_ZN2v88internal18SharedFunctionInfo30EnsureSourcePositionsAvailableEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	jmp	.L340
.L415:
	leaq	.LC19(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L355:
	movq	-672(%rbp), %rdx
	leaq	-528(%rbp), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L353
.L370:
	movq	32(%r13), %rax
	movq	24(%rax), %rdi
	cmpq	%rdi, -672(%rbp)
	je	.L373
	leaq	24(%rax), %rdx
	jmp	.L372
.L359:
	movq	32(%r13), %rdx
	leaq	16(%rdx,%rax), %rax
	jmp	.L360
.L361:
	movq	-680(%rbp), %rdi
	call	_ZNK2v88internal8compiler21SharedFunctionInfoRef4kindEv@PLT
	subl	$4, %eax
	cmpb	$1, %al
	jbe	.L364
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%rax, -672(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	subq	$8, %rsp
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%rax, -720(%rbp)
	movq	0(%r13), %rax
	movl	$2, %r9d
	movq	%r12, %rdi
	movl	$1, %r8d
	movl	20(%rax), %ecx
	pushq	-704(%rbp)
	pushq	-544(%rbp)
	pushq	-552(%rbp)
	subl	$2, %ecx
	call	_ZN2v88internal8compiler9JSInliner26CreateArtificialFrameStateEPNS1_4NodeES4_iNS0_9BailoutIdENS1_14FrameStateTypeENS1_21SharedFunctionInfoRefES4_
	movq	32(%r12), %rdx
	movq	-688(%rbp), %rcx
	addq	$32, %rsp
	movq	%rax, -736(%rbp)
	movzbl	23(%r13), %eax
	movq	(%rdx), %r9
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L365
	movq	32(%r13), %rax
	leaq	16(%rax), %rcx
.L365:
	movq	-672(%rbp), %xmm0
	movq	(%rcx), %rax
	movq	%r9, -744(%rbp)
	movq	-704(%rbp), %xmm2
	movq	368(%rdx), %rdi
	movhps	-720(%rbp), %xmm0
	movq	%rax, -672(%rbp)
	movhps	-736(%rbp), %xmm2
	movaps	%xmm0, -720(%rbp)
	movaps	%xmm2, -736(%rbp)
	call	_ZN2v88internal8compiler17JSOperatorBuilder6CreateEv@PLT
	movq	-744(%rbp), %r9
	movq	%rbx, %xmm6
	movq	-672(%rbp), %xmm1
	movdqa	-720(%rbp), %xmm0
	movq	%rax, %rsi
	leaq	-400(%rbp), %rax
	xorl	%r8d, %r8d
	punpcklqdq	%xmm6, %xmm1
	movq	%rax, %rcx
	movl	$6, %edx
	movq	%r9, %rdi
	movdqa	-736(%rbp), %xmm2
	movq	%rax, -720(%rbp)
	movaps	%xmm1, -400(%rbp)
	movaps	%xmm2, -384(%rbp)
	movaps	%xmm0, -368(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-512(%rbp), %rsi
	movq	%rax, -496(%rbp)
	cmpq	-504(%rbp), %rsi
	je	.L366
	movq	%rax, (%rsi)
	addq	$8, -512(%rbp)
.L367:
	movq	-496(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler14NodeProperties19ReplaceControlInputEPNS1_4NodeES4_i@PLT
	movq	-496(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler14NodeProperties18ReplaceEffectInputEPNS1_4NodeES4_i@PLT
	movq	32(%r12), %rax
	movq	(%rax), %r9
	movq	8(%rax), %rdi
	movq	%r9, -672(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4DeadEv@PLT
	movq	-672(%rbp), %r9
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r13, %r8
	movq	%r13, %rcx
	movq	%r13, %rdx
	movq	%rax, %rsi
	movq	%r13, %rdi
	movq	%rax, -752(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties11ReplaceUsesEPNS1_4NodeES4_S4_S4_S4_@PLT
	movq	32(%r12), %rax
	movq	(%rax), %r10
	movq	376(%rax), %rdi
	movq	%r10, -672(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder16ObjectIsReceiverEv@PLT
	movq	-672(%rbp), %r10
	xorl	%r8d, %r8d
	movq	-720(%rbp), %rcx
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r13, -400(%rbp)
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-496(%rbp), %rcx
	xorl	%edx, %edx
	movl	$8, %esi
	movq	%rax, -672(%rbp)
	movq	32(%r12), %rax
	movq	%rcx, -736(%rbp)
	movq	(%rax), %r10
	movq	8(%rax), %rdi
	movq	%r10, -744(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6SelectENS0_21MachineRepresentationENS1_10BranchHintE@PLT
	movq	-736(%rbp), %rcx
	movq	%r13, %xmm7
	xorl	%r8d, %r8d
	movq	-744(%rbp), %r10
	movq	%rax, %rsi
	movl	$3, %edx
	movq	-672(%rbp), %xmm0
	movq	%rcx, -384(%rbp)
	movq	-720(%rbp), %rcx
	punpcklqdq	%xmm7, %xmm0
	movq	%r10, %rdi
	movaps	%xmm0, -400(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%r12), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rax, %rdx
	movq	-496(%rbp), %rax
	movq	-752(%rbp), %r9
	movq	%rax, -672(%rbp)
	movq	(%rdi), %rax
	movq	%r9, %rsi
	call	*32(%rax)
	jmp	.L363
.L424:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler14NodeProperties31FindSuccessfulControlProjectionEPNS1_4NodeE@PLT
	movq	%rax, -736(%rbp)
	movq	32(%r12), %rax
	movq	(%rax), %r9
	movq	376(%rax), %rdi
	movq	%r9, -744(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder16ObjectIsReceiverEv@PLT
	movq	-744(%rbp), %r9
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%rax, %rsi
	leaq	-400(%rbp), %rax
	movq	%r13, -400(%rbp)
	movq	%rax, %rcx
	movq	%r9, %rdi
	movq	%rax, -720(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$1, %edx
	xorl	%esi, %esi
	movq	%rax, -744(%rbp)
	movq	32(%r12), %rax
	movq	(%rax), %r9
	movq	8(%rax), %rdi
	movq	%r9, -752(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	movq	-752(%rbp), %r9
	xorl	%r8d, %r8d
	movq	-744(%rbp), %xmm0
	movq	-720(%rbp), %rcx
	movq	%rax, %rsi
	movl	$2, %edx
	movhps	-736(%rbp), %xmm0
	movq	%r9, %rdi
	movaps	%xmm0, -400(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -744(%rbp)
	movq	32(%r12), %rax
	movq	(%rax), %r9
	movq	8(%rax), %rdi
	movq	%r9, -752(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	movq	-744(%rbp), %rdi
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	-752(%rbp), %r9
	movq	-720(%rbp), %rcx
	movq	%rax, %rsi
	movq	%rdi, -400(%rbp)
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -768(%rbp)
	movq	32(%r12), %rax
	movq	(%rax), %r9
	movq	8(%rax), %rdi
	movq	%r9, -752(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	movq	-744(%rbp), %rdi
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	-752(%rbp), %r9
	movq	-720(%rbp), %rcx
	movq	%rax, %rsi
	movq	%rdi, -400(%rbp)
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r13, %rdi
	movq	%rax, -496(%rbp)
	movq	%rax, -752(%rbp)
	movq	32(%r12), %rax
	movq	(%rax), %r9
	movq	%r9, -776(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties18GetFrameStateInputEPNS1_4NodeE@PLT
	movl	$164, %esi
	movq	%rax, -760(%rbp)
	movq	32(%r12), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder11CallRuntimeENS0_7Runtime10FunctionIdE@PLT
	movq	-776(%rbp), %r9
	xorl	%r8d, %r8d
	movq	-704(%rbp), %xmm0
	movq	-720(%rbp), %rcx
	movq	%rax, %rsi
	movl	$4, %edx
	movhps	-760(%rbp), %xmm0
	movq	%r9, %rdi
	movaps	%xmm0, -400(%rbp)
	movq	%r13, %xmm0
	movhps	-752(%rbp), %xmm0
	movaps	%xmm0, -384(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-512(%rbp), %rsi
	movq	%rax, -496(%rbp)
	cmpq	-504(%rbp), %rsi
	je	.L368
	movq	%rax, (%rsi)
	addq	$8, -512(%rbp)
.L369:
	movq	32(%r12), %rax
	movq	-496(%rbp), %rcx
	movq	(%rax), %r9
	movq	8(%rax), %rdi
	movq	%rcx, -752(%rbp)
	movq	%r9, -760(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5ThrowEv@PLT
	movq	-760(%rbp), %r9
	xorl	%r8d, %r8d
	movq	-752(%rbp), %xmm0
	movq	-720(%rbp), %rcx
	movq	%rax, %rsi
	movl	$2, %edx
	punpcklqdq	%xmm0, %xmm0
	movq	%r9, %rdi
	movaps	%xmm0, -400(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -496(%rbp)
	movq	%rax, %rdx
	movq	32(%r12), %rax
	movq	8(%rax), %rsi
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler14NodeProperties17MergeControlToEndEPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS1_4NodeE@PLT
	movq	8(%r12), %rdi
	movq	-736(%rbp), %rdx
	movq	-768(%rbp), %r8
	movq	(%rdi), %rax
	movq	%rdx, %rcx
	movq	%rdx, %rsi
	call	*32(%rax)
	movq	-736(%rbp), %rsi
	movq	-744(%rbp), %rdi
	xorl	%edx, %edx
	call	_ZN2v88internal8compiler14NodeProperties19ReplaceControlInputEPNS1_4NodeES4_i@PLT
	jmp	.L363
.L366:
	movq	-680(%rbp), %rdx
	leaq	-528(%rbp), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L367
.L378:
	movq	32(%r13), %rax
	addq	$24, %rax
	jmp	.L379
.L368:
	movq	-680(%rbp), %rdx
	leaq	-528(%rbp), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L369
.L381:
	movq	32(%r13), %rax
	addq	$24, %rax
	jmp	.L382
.L411:
	call	__stack_chk_fail@PLT
.L423:
	leaq	.LC5(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE22125:
	.size	_ZN2v88internal8compiler9JSInliner12ReduceJSCallEPNS1_4NodeE, .-_ZN2v88internal8compiler9JSInliner12ReduceJSCallEPNS1_4NodeE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler9JSInliner10InlineCallEPNS1_4NodeES4_S4_S4_S4_S4_S4_RKNS0_10ZoneVectorIS4_EE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler9JSInliner10InlineCallEPNS1_4NodeES4_S4_S4_S4_S4_S4_RKNS0_10ZoneVectorIS4_EE, @function
_GLOBAL__sub_I__ZN2v88internal8compiler9JSInliner10InlineCallEPNS1_4NodeES4_S4_S4_S4_S4_S4_RKNS0_10ZoneVectorIS4_EE:
.LFB27104:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE27104:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler9JSInliner10InlineCallEPNS1_4NodeES4_S4_S4_S4_S4_S4_RKNS0_10ZoneVectorIS4_EE, .-_GLOBAL__sub_I__ZN2v88internal8compiler9JSInliner10InlineCallEPNS1_4NodeES4_S4_S4_S4_S4_S4_RKNS0_10ZoneVectorIS4_EE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler9JSInliner10InlineCallEPNS1_4NodeES4_S4_S4_S4_S4_S4_RKNS0_10ZoneVectorIS4_EE
	.hidden	_ZTCN2v88internal12StdoutStreamE0_So
	.weak	_ZTCN2v88internal12StdoutStreamE0_So
	.section	.rodata._ZTCN2v88internal12StdoutStreamE0_So,"aG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTCN2v88internal12StdoutStreamE0_So, @object
	.size	_ZTCN2v88internal12StdoutStreamE0_So, 80
_ZTCN2v88internal12StdoutStreamE0_So:
	.quad	80
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-80
	.quad	-80
	.quad	0
	.quad	0
	.quad	0
	.hidden	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE
	.weak	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE
	.section	.rodata._ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE,"aG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE, @object
	.size	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE, 80
_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE:
	.quad	80
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-80
	.quad	-80
	.quad	0
	.quad	0
	.quad	0
	.weak	_ZTTN2v88internal12StdoutStreamE
	.section	.data.rel.ro.local._ZTTN2v88internal12StdoutStreamE,"awG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTTN2v88internal12StdoutStreamE, @object
	.size	_ZTTN2v88internal12StdoutStreamE, 48
_ZTTN2v88internal12StdoutStreamE:
	.quad	_ZTVN2v88internal12StdoutStreamE+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_So+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_So+64
	.quad	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE+64
	.quad	_ZTVN2v88internal12StdoutStreamE+64
	.weak	_ZTVN2v88internal12StdoutStreamE
	.section	.data.rel.ro.local._ZTVN2v88internal12StdoutStreamE,"awG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTVN2v88internal12StdoutStreamE, @object
	.size	_ZTVN2v88internal12StdoutStreamE, 80
_ZTVN2v88internal12StdoutStreamE:
	.quad	80
	.quad	0
	.quad	0
	.quad	_ZN2v88internal12StdoutStreamD1Ev
	.quad	_ZN2v88internal12StdoutStreamD0Ev
	.quad	-80
	.quad	-80
	.quad	0
	.quad	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.quad	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
