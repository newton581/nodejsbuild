	.file	"machine-operator-reducer.cc"
	.text
	.section	.rodata._ZNK2v88internal8compiler22MachineOperatorReducer12reducer_nameEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"MachineOperatorReducer"
	.section	.text._ZNK2v88internal8compiler22MachineOperatorReducer12reducer_nameEv,"axG",@progbits,_ZNK2v88internal8compiler22MachineOperatorReducer12reducer_nameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal8compiler22MachineOperatorReducer12reducer_nameEv
	.type	_ZNK2v88internal8compiler22MachineOperatorReducer12reducer_nameEv, @function
_ZNK2v88internal8compiler22MachineOperatorReducer12reducer_nameEv:
.LFB10338:
	.cfi_startproc
	endbr64
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE10338:
	.size	_ZNK2v88internal8compiler22MachineOperatorReducer12reducer_nameEv, .-_ZNK2v88internal8compiler22MachineOperatorReducer12reducer_nameEv
	.section	.text._ZN2v88internal8compiler22MachineOperatorReducerD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22MachineOperatorReducerD2Ev
	.type	_ZN2v88internal8compiler22MachineOperatorReducerD2Ev, @function
_ZN2v88internal8compiler22MachineOperatorReducerD2Ev:
.LFB19892:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE19892:
	.size	_ZN2v88internal8compiler22MachineOperatorReducerD2Ev, .-_ZN2v88internal8compiler22MachineOperatorReducerD2Ev
	.globl	_ZN2v88internal8compiler22MachineOperatorReducerD1Ev
	.set	_ZN2v88internal8compiler22MachineOperatorReducerD1Ev,_ZN2v88internal8compiler22MachineOperatorReducerD2Ev
	.section	.text._ZN2v88internal8compiler22MachineOperatorReducerD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22MachineOperatorReducerD0Ev
	.type	_ZN2v88internal8compiler22MachineOperatorReducerD0Ev, @function
_ZN2v88internal8compiler22MachineOperatorReducerD0Ev:
.LFB19894:
	.cfi_startproc
	endbr64
	movl	$32, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE19894:
	.size	_ZN2v88internal8compiler22MachineOperatorReducerD0Ev, .-_ZN2v88internal8compiler22MachineOperatorReducerD0Ev
	.section	.text._ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.0, @function
_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.0:
.LFB23982:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movzbl	23(%rdi), %eax
	movq	32(%rdi), %r8
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L6
	leaq	32(%rdi), %rbx
	cmpq	%r8, %rsi
	je	.L5
	leaq	-24(%rdi), %r12
	testq	%r8, %r8
	je	.L10
.L18:
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L10:
	movq	%r13, (%rbx)
	testq	%r13, %r13
	je	.L5
	addq	$8, %rsp
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	movq	16(%r8), %rax
	leaq	16(%r8), %rbx
	cmpq	%rax, %rsi
	je	.L5
	movq	%r8, %rdi
	movq	%rax, %r8
	leaq	-24(%rdi), %r12
	testq	%r8, %r8
	jne	.L18
	jmp	.L10
	.cfi_endproc
.LFE23982:
	.size	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.0, .-_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.0
	.section	.text._ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.1,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.1, @function
_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.1:
.LFB23981:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movzbl	23(%rdi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L20
	movq	40(%rdi), %r8
	leaq	40(%rdi), %rbx
	cmpq	%r8, %rsi
	je	.L19
	leaq	-48(%rdi), %r12
	testq	%r8, %r8
	je	.L24
.L32:
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L24:
	movq	%r13, (%rbx)
	testq	%r13, %r13
	je	.L19
	addq	$8, %rsp
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	movq	32(%rdi), %rdi
	movq	24(%rdi), %r8
	cmpq	%r8, %rsi
	je	.L19
	leaq	24(%rdi), %rbx
	leaq	-48(%rdi), %r12
	testq	%r8, %r8
	jne	.L32
	jmp	.L24
	.cfi_endproc
.LFE23981:
	.size	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.1, .-_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.1
	.section	.text._ZN2v88internal8compiler22MachineOperatorReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_12MachineGraphEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22MachineOperatorReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_12MachineGraphEb
	.type	_ZN2v88internal8compiler22MachineOperatorReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_12MachineGraphEb, @function
_ZN2v88internal8compiler22MachineOperatorReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_12MachineGraphEb:
.LFB19889:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal8compiler22MachineOperatorReducerE(%rip), %rax
	movq	%rsi, 8(%rdi)
	movq	%rax, (%rdi)
	movq	%rdx, 16(%rdi)
	movb	%cl, 24(%rdi)
	ret
	.cfi_endproc
.LFE19889:
	.size	_ZN2v88internal8compiler22MachineOperatorReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_12MachineGraphEb, .-_ZN2v88internal8compiler22MachineOperatorReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_12MachineGraphEb
	.globl	_ZN2v88internal8compiler22MachineOperatorReducerC1EPNS1_15AdvancedReducer6EditorEPNS1_12MachineGraphEb
	.set	_ZN2v88internal8compiler22MachineOperatorReducerC1EPNS1_15AdvancedReducer6EditorEPNS1_12MachineGraphEb,_ZN2v88internal8compiler22MachineOperatorReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_12MachineGraphEb
	.section	.text._ZN2v88internal8compiler22MachineOperatorReducer15Float32ConstantEf,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22MachineOperatorReducer15Float32ConstantEf
	.type	_ZN2v88internal8compiler22MachineOperatorReducer15Float32ConstantEf, @function
_ZN2v88internal8compiler22MachineOperatorReducer15Float32ConstantEf:
.LFB19895:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 12, -24
	movq	16(%rdi), %rax
	movss	%xmm0, -20(%rbp)
	movss	-20(%rbp), %xmm0
	movq	8(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder15Float32ConstantEf@PLT
	addq	$24, %rsp
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	popq	%r12
	movq	%rax, %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	.cfi_endproc
.LFE19895:
	.size	_ZN2v88internal8compiler22MachineOperatorReducer15Float32ConstantEf, .-_ZN2v88internal8compiler22MachineOperatorReducer15Float32ConstantEf
	.section	.text._ZN2v88internal8compiler22MachineOperatorReducer15Float64ConstantEd,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22MachineOperatorReducer15Float64ConstantEd
	.type	_ZN2v88internal8compiler22MachineOperatorReducer15Float64ConstantEd, @function
_ZN2v88internal8compiler22MachineOperatorReducer15Float64ConstantEd:
.LFB19896:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movsd	%xmm0, -8(%rbp)
	movq	16(%rdi), %rdi
	movsd	-8(%rbp), %xmm0
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler12MachineGraph15Float64ConstantEd@PLT
	.cfi_endproc
.LFE19896:
	.size	_ZN2v88internal8compiler22MachineOperatorReducer15Float64ConstantEd, .-_ZN2v88internal8compiler22MachineOperatorReducer15Float64ConstantEd
	.section	.text._ZN2v88internal8compiler22MachineOperatorReducer13Int32ConstantEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22MachineOperatorReducer13Int32ConstantEi
	.type	_ZN2v88internal8compiler22MachineOperatorReducer13Int32ConstantEi, @function
_ZN2v88internal8compiler22MachineOperatorReducer13Int32ConstantEi:
.LFB19897:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	jmp	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	.cfi_endproc
.LFE19897:
	.size	_ZN2v88internal8compiler22MachineOperatorReducer13Int32ConstantEi, .-_ZN2v88internal8compiler22MachineOperatorReducer13Int32ConstantEi
	.section	.text._ZN2v88internal8compiler22MachineOperatorReducer13Int64ConstantEl,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22MachineOperatorReducer13Int64ConstantEl
	.type	_ZN2v88internal8compiler22MachineOperatorReducer13Int64ConstantEl, @function
_ZN2v88internal8compiler22MachineOperatorReducer13Int64ConstantEl:
.LFB19898:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder13Int64ConstantEl@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	popq	%r12
	movq	%rax, %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	.cfi_endproc
.LFE19898:
	.size	_ZN2v88internal8compiler22MachineOperatorReducer13Int64ConstantEl, .-_ZN2v88internal8compiler22MachineOperatorReducer13Int64ConstantEl
	.section	.text._ZN2v88internal8compiler22MachineOperatorReducer10Float64MulEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22MachineOperatorReducer10Float64MulEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler22MachineOperatorReducer10Float64MulEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler22MachineOperatorReducer10Float64MulEPNS1_4NodeES4_:
.LFB19899:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$56, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movaps	%xmm0, -64(%rbp)
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder10Float64MulEv@PLT
	movdqa	-64(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	leaq	-48(%rbp), %rcx
	movq	%rax, %rsi
	movl	$2, %edx
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L44
	addq	$56, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L44:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19899:
	.size	_ZN2v88internal8compiler22MachineOperatorReducer10Float64MulEPNS1_4NodeES4_, .-_ZN2v88internal8compiler22MachineOperatorReducer10Float64MulEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler22MachineOperatorReducer14Float64PowHalfEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22MachineOperatorReducer14Float64PowHalfEPNS1_4NodeE
	.type	_ZN2v88internal8compiler22MachineOperatorReducer14Float64PowHalfEPNS1_4NodeE, @function
_ZN2v88internal8compiler22MachineOperatorReducer14Float64PowHalfEPNS1_4NodeE:
.LFB19900:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-80(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%rsi, -96(%rbp)
	movq	16(%rdi), %rdi
	movq	(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler12MachineGraph15Float64ConstantEd@PLT
	movq	%rax, -88(%rbp)
	movq	16(%rbx), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder10Float64AddEv@PLT
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movl	$2, %edx
	movq	-88(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r13, %rdi
	movhps	-96(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%rbx), %rdi
	movsd	.LC2(%rip), %xmm0
	movq	%rax, -88(%rbp)
	movq	(%rdi), %r13
	call	_ZN2v88internal8compiler12MachineGraph15Float64ConstantEd@PLT
	movq	%rax, -96(%rbp)
	movq	16(%rbx), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder22Float64LessThanOrEqualEv@PLT
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movq	%r13, %rdi
	movq	-88(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movhps	-96(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$1, %edx
	movl	$2, %esi
	movq	%rax, -96(%rbp)
	movq	16(%rbx), %rax
	movq	(%rax), %r13
	movq	8(%rax), %r14
	movq	8(%r13), %r15
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movl	$2, %edx
	movq	-96(%rbp), %xmm0
	movq	%r15, %xmm1
	movq	%rax, %rsi
	movq	%r13, %rdi
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r14, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	movq	%r15, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r14, %rdi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	movq	%r15, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$2, %esi
	movq	%r14, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movl	$2, %edx
	movq	-96(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r13, %rdi
	movhps	-104(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r15
	movq	16(%rbx), %rax
	movq	(%rax), %r9
	movq	16(%rax), %rdi
	movq	%r9, -96(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder11Float64SqrtEv@PLT
	movq	-96(%rbp), %r9
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movq	%rax, %rsi
	movq	-88(%rbp), %rax
	movl	$1, %edx
	movq	%r9, %rdi
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movsd	.LC3(%rip), %xmm0
	movq	16(%rbx), %rdi
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal8compiler12MachineGraph15Float64ConstantEd@PLT
	movl	$2, %edx
	movl	$13, %esi
	movq	%r14, %rdi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movl	$3, %edx
	movq	-96(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r13, %rdi
	movq	%r15, -64(%rbp)
	movhps	-88(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L48
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L48:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19900:
	.size	_ZN2v88internal8compiler22MachineOperatorReducer14Float64PowHalfEPNS1_4NodeE, .-_ZN2v88internal8compiler22MachineOperatorReducer14Float64PowHalfEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler22MachineOperatorReducer9Word32SarEPNS1_4NodeEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22MachineOperatorReducer9Word32SarEPNS1_4NodeEj
	.type	_ZN2v88internal8compiler22MachineOperatorReducer9Word32SarEPNS1_4NodeEj, @function
_ZN2v88internal8compiler22MachineOperatorReducer9Word32SarEPNS1_4NodeEj:
.LFB19902:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	je	.L50
	movq	%rdi, %rbx
	movq	16(%rdi), %rdi
	movl	%edx, %esi
	movq	(%rdi), %r13
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%rax, -72(%rbp)
	movq	16(%rbx), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32SarEv@PLT
	movq	%r12, %xmm0
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	movhps	-72(%rbp), %xmm0
	movq	%rax, %rsi
	leaq	-64(%rbp), %rcx
	movl	$2, %edx
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r12
.L50:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L56
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L56:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19902:
	.size	_ZN2v88internal8compiler22MachineOperatorReducer9Word32SarEPNS1_4NodeEj, .-_ZN2v88internal8compiler22MachineOperatorReducer9Word32SarEPNS1_4NodeEj
	.section	.text._ZN2v88internal8compiler22MachineOperatorReducer9Word32ShrEPNS1_4NodeEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22MachineOperatorReducer9Word32ShrEPNS1_4NodeEj
	.type	_ZN2v88internal8compiler22MachineOperatorReducer9Word32ShrEPNS1_4NodeEj, @function
_ZN2v88internal8compiler22MachineOperatorReducer9Word32ShrEPNS1_4NodeEj:
.LFB19903:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	je	.L58
	movq	%rdi, %rbx
	movq	16(%rdi), %rdi
	movl	%edx, %esi
	movq	(%rdi), %r13
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%rax, -72(%rbp)
	movq	16(%rbx), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32ShrEv@PLT
	movq	%r12, %xmm0
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	movhps	-72(%rbp), %xmm0
	movq	%rax, %rsi
	leaq	-64(%rbp), %rcx
	movl	$2, %edx
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r12
.L58:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L64
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L64:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19903:
	.size	_ZN2v88internal8compiler22MachineOperatorReducer9Word32ShrEPNS1_4NodeEj, .-_ZN2v88internal8compiler22MachineOperatorReducer9Word32ShrEPNS1_4NodeEj
	.section	.text._ZN2v88internal8compiler22MachineOperatorReducer11Word32EqualEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22MachineOperatorReducer11Word32EqualEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler22MachineOperatorReducer11Word32EqualEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler22MachineOperatorReducer11Word32EqualEPNS1_4NodeES4_:
.LFB19904:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$56, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movaps	%xmm0, -64(%rbp)
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder11Word32EqualEv@PLT
	movdqa	-64(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	leaq	-48(%rbp), %rcx
	movq	%rax, %rsi
	movl	$2, %edx
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L68
	addq	$56, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L68:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19904:
	.size	_ZN2v88internal8compiler22MachineOperatorReducer11Word32EqualEPNS1_4NodeES4_, .-_ZN2v88internal8compiler22MachineOperatorReducer11Word32EqualEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler22MachineOperatorReducer31BitcastWord32ToCompressedSignedEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22MachineOperatorReducer31BitcastWord32ToCompressedSignedEPNS1_4NodeE
	.type	_ZN2v88internal8compiler22MachineOperatorReducer31BitcastWord32ToCompressedSignedEPNS1_4NodeE, @function
_ZN2v88internal8compiler22MachineOperatorReducer31BitcastWord32ToCompressedSignedEPNS1_4NodeE:
.LFB19905:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder31BitcastWord32ToCompressedSignedEv@PLT
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	leaq	-32(%rbp), %rcx
	movq	%rbx, -32(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L72
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L72:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19905:
	.size	_ZN2v88internal8compiler22MachineOperatorReducer31BitcastWord32ToCompressedSignedEPNS1_4NodeE, .-_ZN2v88internal8compiler22MachineOperatorReducer31BitcastWord32ToCompressedSignedEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler22MachineOperatorReducer31BitcastCompressedSignedToWord32EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22MachineOperatorReducer31BitcastCompressedSignedToWord32EPNS1_4NodeE
	.type	_ZN2v88internal8compiler22MachineOperatorReducer31BitcastCompressedSignedToWord32EPNS1_4NodeE, @function
_ZN2v88internal8compiler22MachineOperatorReducer31BitcastCompressedSignedToWord32EPNS1_4NodeE:
.LFB19906:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder31BitcastCompressedSignedToWord32Ev@PLT
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	leaq	-32(%rbp), %rcx
	movq	%rbx, -32(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L76
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L76:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19906:
	.size	_ZN2v88internal8compiler22MachineOperatorReducer31BitcastCompressedSignedToWord32EPNS1_4NodeE, .-_ZN2v88internal8compiler22MachineOperatorReducer31BitcastCompressedSignedToWord32EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler22MachineOperatorReducer8Int32MulEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22MachineOperatorReducer8Int32MulEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler22MachineOperatorReducer8Int32MulEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler22MachineOperatorReducer8Int32MulEPNS1_4NodeES4_:
.LFB19909:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$56, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movaps	%xmm0, -64(%rbp)
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Int32MulEv@PLT
	movdqa	-64(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	leaq	-48(%rbp), %rcx
	movq	%rax, %rsi
	movl	$2, %edx
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L80
	addq	$56, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L80:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19909:
	.size	_ZN2v88internal8compiler22MachineOperatorReducer8Int32MulEPNS1_4NodeES4_, .-_ZN2v88internal8compiler22MachineOperatorReducer8Int32MulEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler22MachineOperatorReducer14ReduceInt32SubEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22MachineOperatorReducer14ReduceInt32SubEPNS1_4NodeE
	.type	_ZN2v88internal8compiler22MachineOperatorReducer14ReduceInt32SubEPNS1_4NodeE, @function
_ZN2v88internal8compiler22MachineOperatorReducer14ReduceInt32SubEPNS1_4NodeE:
.LFB19915:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	32(%rsi), %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %rsi
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L82
	movq	(%rsi), %rdi
	movzwl	16(%rdi), %edx
	cmpw	$23, %dx
	sete	%cl
	je	.L83
	xorl	%r8d, %r8d
	xorl	%edi, %edi
.L84:
	leaq	8(%r15), %rdx
	movq	%rsi, %r12
	movq	%r15, %r9
.L87:
	movq	(%rdx), %r14
	movq	(%r14), %rdx
	cmpw	$23, 16(%rdx)
	jne	.L88
	movl	44(%rdx), %esi
	testl	%esi, %esi
	je	.L89
	movq	16(%r13), %rdi
	testb	%cl, %cl
	je	.L113
	subl	%esi, %r8d
	movl	%r8d, %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L83:
	.cfi_restore_state
	movl	44(%rdi), %r8d
	movl	$1, %edi
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L88:
	movq	(%rbx), %rdx
	testb	$1, 18(%rdx)
	je	.L91
	testb	%dil, %dil
	jne	.L147
.L91:
	testb	%cl, %cl
	jne	.L148
	cmpq	%r12, %r14
	jne	.L111
.L146:
	movq	16(%r13), %rdi
.L110:
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L82:
	.cfi_restore_state
	movq	16(%rsi), %r12
	leaq	16(%rsi), %r9
	movq	(%r12), %rdi
	movzwl	16(%rdi), %edx
	cmpw	$23, %dx
	sete	%cl
	je	.L85
	xorl	%r8d, %r8d
	xorl	%edi, %edi
.L86:
	leaq	8(%r9), %rdx
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L148:
	cmpq	%r12, %r14
	je	.L146
.L111:
	addq	$40, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L85:
	.cfi_restore_state
	movl	44(%rdi), %r8d
	movl	$1, %edi
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L92:
	cmpq	%r12, %r14
	je	.L95
.L93:
	subq	$24, %rsi
	movq	%r12, %rdi
	movl	%r8d, -68(%rbp)
	movq	%r9, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-64(%rbp), %r9
	movq	-56(%rbp), %rsi
	movq	%r14, %rdi
	movq	%r14, (%r9)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movzbl	23(%rbx), %eax
	movl	-68(%rbp), %r8d
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L149
.L94:
	movq	8(%r15), %rdi
	cmpq	%r12, %rdi
	je	.L114
	leaq	8(%r15), %rax
	movq	%rbx, %rsi
.L97:
	subq	$48, %rsi
	testq	%rdi, %rdi
	je	.L98
	movl	%r8d, -68(%rbp)
	movq	%rax, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %rax
	movl	-68(%rbp), %r8d
.L98:
	movq	%r12, (%rax)
	movq	%r12, %rdi
	movl	%r8d, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movl	-56(%rbp), %r8d
.L114:
	testl	%r8d, %r8d
	jne	.L150
	movq	%r14, %r12
.L89:
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L150:
	.cfi_restore_state
	movq	%r12, %rax
	movq	16(%r13), %rdi
	movq	%r14, %r12
	movl	%r8d, %esi
	movq	%rax, %r14
.L113:
	cmpq	%r12, %r14
	je	.L110
	cmpl	$-2147483648, %esi
	je	.L102
	negl	%esi
.L102:
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%rax, %r12
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L103
	movq	8(%r15), %rdi
	cmpq	%rdi, %r12
	je	.L106
	addq	$8, %r15
	movq	%rbx, %rsi
.L105:
	leaq	-48(%rsi), %r14
	testq	%rdi, %rdi
	je	.L107
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L107:
	movq	%r12, (%r15)
	testq	%r12, %r12
	je	.L106
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L106:
	movq	16(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Int32AddEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler22MachineOperatorReducer14ReduceInt32AddEPNS1_4NodeE
	testq	%rax, %rax
	cmove	%rbx, %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L103:
	.cfi_restore_state
	movq	32(%rbx), %rsi
	movq	24(%rsi), %rdi
	cmpq	%rdi, %r12
	je	.L106
	leaq	24(%rsi), %r15
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L147:
	cmpl	$15, %eax
	je	.L92
	movq	%rbx, %rsi
	cmpq	%r12, %r14
	jne	.L93
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L149:
	movq	32(%rbx), %rsi
.L95:
	movq	24(%rsi), %rdi
	cmpq	%r12, %rdi
	je	.L114
	leaq	24(%rsi), %rax
	jmp	.L97
	.cfi_endproc
.LFE19915:
	.size	_ZN2v88internal8compiler22MachineOperatorReducer14ReduceInt32SubEPNS1_4NodeE, .-_ZN2v88internal8compiler22MachineOperatorReducer14ReduceInt32SubEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler22MachineOperatorReducer14ReduceInt32AddEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22MachineOperatorReducer14ReduceInt32AddEPNS1_4NodeE
	.type	_ZN2v88internal8compiler22MachineOperatorReducer14ReduceInt32AddEPNS1_4NodeE, @function
_ZN2v88internal8compiler22MachineOperatorReducer14ReduceInt32AddEPNS1_4NodeE:
.LFB19913:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	32(%rsi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$72, %rsp
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %rsi
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L152
	movq	(%rsi), %rcx
	movzwl	16(%rcx), %edx
	cmpw	$23, %dx
	sete	%r10b
	je	.L153
	xorl	%ecx, %ecx
	xorl	%edi, %edi
.L154:
	leaq	8(%r14), %rdx
	movq	%rsi, %r12
	movq	%r14, %r9
.L157:
	movq	(%rdx), %r8
	movq	(%r8), %rdx
	cmpw	$23, 16(%rdx)
	jne	.L158
	movl	44(%rdx), %r15d
	testl	%r15d, %r15d
	je	.L159
	movl	$1, %r11d
	testb	%r10b, %r10b
	je	.L161
	movq	16(%r13), %rdi
	leal	(%r15,%rcx), %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
.L327:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L153:
	.cfi_restore_state
	movl	44(%rcx), %ecx
	movl	$1, %edi
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L162:
	cmpq	%r12, %r8
	je	.L165
.L163:
	leaq	-24(%rsi), %r15
	movq	%r12, %rdi
	movb	%r11b, -80(%rbp)
	movq	%r15, %rsi
	movl	%ecx, -72(%rbp)
	movq	%r8, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-64(%rbp), %r8
	movq	-56(%rbp), %r9
	movq	%r15, %rsi
	movq	%r8, (%r9)
	movq	%r8, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movzbl	23(%rbx), %eax
	movq	-56(%rbp), %r8
	movl	-72(%rbp), %ecx
	movzbl	-80(%rbp), %r11d
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L331
.L164:
	movq	8(%r14), %rdi
	cmpq	%r12, %rdi
	je	.L252
	leaq	8(%r14), %rax
	movq	%rbx, %rsi
.L167:
	leaq	-48(%rsi), %r15
	testq	%rdi, %rdi
	je	.L168
	movq	%r15, %rsi
	movb	%r11b, -80(%rbp)
	movl	%ecx, -72(%rbp)
	movq	%rax, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %rax
	movl	-72(%rbp), %ecx
	movzbl	-80(%rbp), %r11d
.L168:
	movq	%r12, (%rax)
	movq	%r15, %rsi
	movq	%r12, %rdi
	movb	%r11b, -72(%rbp)
	movl	%ecx, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movq	-56(%rbp), %r8
	movl	-64(%rbp), %ecx
	movzbl	-72(%rbp), %r11d
.L252:
	testl	%ecx, %ecx
	je	.L260
	movq	%r12, %rax
	movl	%ecx, %r15d
	movq	%r8, %r12
	movq	%rax, %r8
	.p2align 4,,10
	.p2align 3
.L161:
	movq	(%r12), %rax
	cmpw	$308, 16(%rax)
	je	.L332
.L170:
	movq	(%r8), %rax
	cmpw	$308, 16(%rax)
	je	.L333
.L197:
	testb	%r11b, %r11b
	je	.L219
	movq	(%r12), %rax
	cmpw	$306, 16(%rax)
	je	.L334
.L219:
	xorl	%eax, %eax
.L336:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L158:
	.cfi_restore_state
	movq	(%rbx), %rdx
	xorl	%r15d, %r15d
	andb	18(%rdx), %dil
	movl	%edi, %r11d
	je	.L161
	cmpl	$15, %eax
	je	.L162
	movq	%rbx, %rsi
	cmpq	%r12, %r8
	jne	.L163
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L152:
	movq	16(%rsi), %r12
	leaq	16(%rsi), %r9
	movq	(%r12), %rcx
	movzwl	16(%rcx), %edx
	cmpw	$23, %dx
	sete	%r10b
	je	.L155
	xorl	%ecx, %ecx
	xorl	%edi, %edi
.L156:
	leaq	8(%r9), %rdx
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L155:
	movl	44(%rcx), %ecx
	movl	$1, %edi
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L334:
	movzbl	23(%r12), %edx
	movq	32(%r12), %rsi
	leaq	32(%r12), %rcx
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L220
	movq	(%rsi), %rdi
	cmpw	$23, 16(%rdi)
	je	.L221
	xorl	%r10d, %r10d
	xorl	%r11d, %r11d
.L222:
	movq	%rsi, -56(%rbp)
	leaq	8(%rcx), %rdi
	movq	%rcx, %r9
.L225:
	movq	(%rdi), %r8
	movq	(%r8), %rdi
	cmpw	$23, 16(%rdi)
	jne	.L226
	movl	44(%rdi), %r10d
.L227:
	movq	24(%r12), %rax
	testq	%rax, %rax
	jne	.L238
	jmp	.L236
	.p2align 4,,10
	.p2align 3
.L335:
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L236
.L238:
	movl	16(%rax), %ecx
	movl	%ecx, %edx
	shrl	%edx
	andl	$1, %ecx
	leaq	3(%rdx,%rdx,2), %rdx
	leaq	(%rax,%rdx,8), %rdx
	jne	.L237
	movq	(%rdx), %rdx
.L237:
	cmpq	%rdx, %rbx
	je	.L335
	xorl	%eax, %eax
	jmp	.L336
.L260:
	movq	%r8, %r12
.L159:
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L332:
	.cfi_restore_state
	movzbl	23(%r12), %edx
	leaq	32(%r12), %rcx
	andl	$15, %edx
	movl	%edx, %r10d
	movq	32(%r12), %rdx
	cmpl	$15, %r10d
	je	.L171
	movq	(%rdx), %r9
	movzwl	16(%r9), %esi
	cmpw	$23, %si
	sete	%dil
	je	.L172
	movl	$0, -56(%rbp)
	movb	$0, -64(%rbp)
.L173:
	movq	%rdx, -72(%rbp)
	leaq	8(%rcx), %rsi
	movq	%rcx, -80(%rbp)
.L176:
	movq	(%rsi), %r9
	movq	(%r9), %rsi
	cmpw	$23, 16(%rsi)
	je	.L177
	testb	$1, 18(%rax)
	je	.L177
	cmpb	$0, -64(%rbp)
	jne	.L337
.L177:
	testb	%dil, %dil
	je	.L170
	movl	-56(%rbp), %edx
	testl	%edx, %edx
	jne	.L170
	movzbl	23(%rbx), %eax
	movq	32(%rbx), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L186
	movq	%r14, %r12
	movq	%rbx, %r15
	cmpq	%rdi, %r8
	je	.L188
.L187:
	subq	$24, %r15
	testq	%rdi, %rdi
	je	.L190
	movq	%r15, %rsi
	movq	%r9, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-64(%rbp), %r9
	movq	-56(%rbp), %r8
.L190:
	movq	%r8, (%r12)
	movq	%r15, %rsi
	movq	%r8, %rdi
	movq	%r9, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movzbl	23(%rbx), %eax
	movq	-56(%rbp), %r9
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L338
.L188:
	movq	8(%r14), %r8
	cmpq	%r8, %r9
	je	.L214
	addq	$8, %r14
	movq	%rbx, %rdi
.L192:
	leaq	-48(%rdi), %r12
	testq	%r8, %r8
	je	.L216
	movq	%r12, %rsi
	movq	%r8, %rdi
	movq	%r9, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-56(%rbp), %r9
.L216:
	movq	%r9, (%r14)
	movq	%r12, %rsi
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L214:
	movq	16(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Int32SubEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler22MachineOperatorReducer14ReduceInt32SubEPNS1_4NodeE
	testq	%rax, %rax
	cmove	%rbx, %rax
	jmp	.L327
	.p2align 4,,10
	.p2align 3
.L333:
	movzbl	23(%r8), %edx
	leaq	32(%r8), %rcx
	andl	$15, %edx
	movl	%edx, %r10d
	movq	32(%r8), %rdx
	cmpl	$15, %r10d
	je	.L198
	movq	(%rdx), %r9
	movzwl	16(%r9), %esi
	cmpw	$23, %si
	sete	%dil
	je	.L199
	movl	$0, -56(%rbp)
	movb	$0, -64(%rbp)
.L200:
	movq	%rdx, -72(%rbp)
	leaq	8(%rcx), %rsi
	movq	%rcx, -80(%rbp)
.L203:
	movq	(%rsi), %r9
	movq	(%r9), %rsi
	cmpw	$23, 16(%rsi)
	je	.L204
	testb	$1, 18(%rax)
	je	.L204
	cmpb	$0, -64(%rbp)
	jne	.L339
.L204:
	testb	%dil, %dil
	je	.L197
	movl	-56(%rbp), %eax
	testl	%eax, %eax
	jne	.L197
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L213
	movq	8(%r14), %rdi
	cmpq	%r9, %rdi
	je	.L214
	addq	$8, %r14
	movq	%rbx, %rax
.L215:
	leaq	-48(%rax), %r12
	testq	%rdi, %rdi
	je	.L216
	movq	%r12, %rsi
	movq	%r9, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-56(%rbp), %r9
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L172:
	movl	44(%r9), %esi
	movb	$1, -64(%rbp)
	movl	%esi, -56(%rbp)
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L199:
	movl	44(%r9), %esi
	movb	$1, -64(%rbp)
	movl	%esi, -56(%rbp)
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L220:
	movq	16(%rsi), %rdi
	leaq	16(%rsi), %r9
	movq	%rdi, -56(%rbp)
	movq	(%rdi), %rdi
	cmpw	$23, 16(%rdi)
	je	.L223
	xorl	%r10d, %r10d
	xorl	%r11d, %r11d
.L224:
	leaq	8(%r9), %rdi
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L331:
	movq	32(%rbx), %rsi
.L165:
	movq	24(%rsi), %rdi
	cmpq	%r12, %rdi
	je	.L252
	leaq	24(%rsi), %rax
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L171:
	leaq	16(%rdx), %rdi
	movq	%rdi, -80(%rbp)
	movq	16(%rdx), %rdi
	movq	(%rdi), %rsi
	movq	%rdi, -72(%rbp)
	movzwl	16(%rsi), %r9d
	cmpw	$23, %r9w
	sete	%dil
	je	.L174
	movl	$0, -56(%rbp)
	movb	$0, -64(%rbp)
.L175:
	leaq	24(%rdx), %rsi
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L198:
	leaq	16(%rdx), %rdi
	movq	%rdi, -80(%rbp)
	movq	16(%rdx), %rdi
	movq	(%rdi), %rsi
	movq	%rdi, -72(%rbp)
	movzwl	16(%rsi), %r9d
	cmpw	$23, %r9w
	sete	%dil
	je	.L201
	movl	$0, -56(%rbp)
	movb	$0, -64(%rbp)
.L202:
	leaq	24(%rdx), %rsi
	jmp	.L203
	.p2align 4,,10
	.p2align 3
.L226:
	testb	$1, 18(%rax)
	je	.L219
	testb	%r11b, %r11b
	je	.L219
	cmpl	$15, %edx
	je	.L229
	movq	%r12, %rsi
	cmpq	-56(%rbp), %r8
	je	.L231
.L230:
	movq	-56(%rbp), %rdi
	subq	$24, %rsi
	movq	%rcx, -96(%rbp)
	movl	%r10d, -88(%rbp)
	movq	%r8, -80(%rbp)
	movq	%r9, -72(%rbp)
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-80(%rbp), %r8
	movq	-72(%rbp), %r9
	movq	-64(%rbp), %rsi
	movq	%r8, (%r9)
	movq	%r8, %rdi
	movq	%r8, -64(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movzbl	23(%r12), %eax
	movq	-64(%rbp), %r8
	movl	-88(%rbp), %r10d
	movq	-96(%rbp), %rcx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L232
.L231:
	movq	8(%rcx), %rdi
	cmpq	-56(%rbp), %rdi
	je	.L250
	addq	$8, %rcx
	movq	%r12, %rax
.L234:
	leaq	-48(%rax), %rsi
	testq	%rdi, %rdi
	je	.L235
	movl	%r10d, -88(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%r8, -72(%rbp)
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-64(%rbp), %rsi
	movq	-72(%rbp), %r8
	movq	-80(%rbp), %rcx
	movl	-88(%rbp), %r10d
.L235:
	movq	-56(%rbp), %rax
	movl	%r10d, -72(%rbp)
	movq	%r8, -64(%rbp)
	movq	%rax, (%rcx)
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movq	-64(%rbp), %r8
	movl	-72(%rbp), %r10d
.L250:
	movq	%r8, -56(%rbp)
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L174:
	movl	44(%rsi), %esi
	movb	$1, -64(%rbp)
	movl	%esi, -56(%rbp)
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L201:
	movl	44(%rsi), %esi
	movb	$1, -64(%rbp)
	movl	%esi, -56(%rbp)
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L221:
	movl	44(%rdi), %r10d
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L337:
	cmpl	$15, %r10d
	je	.L178
	movq	%r12, %rdx
	cmpq	-72(%rbp), %r9
	je	.L180
.L179:
	movq	-72(%rbp), %rdi
	leaq	-24(%rdx), %rsi
	movq	%rcx, -104(%rbp)
	movb	%r11b, -96(%rbp)
	movq	%r8, -64(%rbp)
	movq	%r9, -88(%rbp)
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-80(%rbp), %rax
	movq	-88(%rbp), %r9
	movq	-56(%rbp), %rsi
	movq	%r9, (%rax)
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movzbl	23(%r12), %eax
	movq	-64(%rbp), %r8
	movzbl	-96(%rbp), %r11d
	movq	-104(%rbp), %rcx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L340
.L180:
	movq	8(%rcx), %rdi
	cmpq	-72(%rbp), %rdi
	je	.L170
	addq	$8, %rcx
	movq	%r12, %rdx
.L183:
	leaq	-48(%rdx), %rsi
	testq	%rdi, %rdi
	je	.L184
	movb	%r11b, -88(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%r8, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %r8
	movq	-80(%rbp), %rcx
	movzbl	-88(%rbp), %r11d
.L184:
	movq	-72(%rbp), %rax
	movb	%r11b, -64(%rbp)
	movq	%r8, -56(%rbp)
	movq	%rax, (%rcx)
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movq	-56(%rbp), %r8
	movzbl	-64(%rbp), %r11d
	jmp	.L170
.L223:
	movl	44(%rdi), %r10d
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L339:
	cmpl	$15, %r10d
	je	.L205
	movq	%r8, %rdx
	cmpq	-72(%rbp), %r9
	je	.L207
.L206:
	movq	-72(%rbp), %rdi
	leaq	-24(%rdx), %rsi
	movq	%rcx, -104(%rbp)
	movb	%r11b, -96(%rbp)
	movq	%r8, -64(%rbp)
	movq	%r9, -88(%rbp)
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-80(%rbp), %rax
	movq	-88(%rbp), %r9
	movq	-56(%rbp), %rsi
	movq	%r9, (%rax)
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movq	-64(%rbp), %r8
	movzbl	-96(%rbp), %r11d
	movq	-104(%rbp), %rcx
	movzbl	23(%r8), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L341
.L207:
	movq	8(%rcx), %rdi
	addq	$8, %rcx
	cmpq	-72(%rbp), %rdi
	je	.L197
.L210:
	leaq	-48(%r8), %rsi
	testq	%rdi, %rdi
	je	.L211
	movb	%r11b, -80(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %rcx
	movzbl	-80(%rbp), %r11d
.L211:
	movq	-72(%rbp), %rax
	movb	%r11b, -56(%rbp)
	movq	%rax, (%rcx)
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movzbl	-56(%rbp), %r11d
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L236:
	movq	16(%r13), %rdi
	leal	(%r15,%r10), %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%rax, %r12
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L342
	movq	8(%r14), %rdi
	cmpq	%rdi, %r12
	je	.L239
	leaq	8(%r14), %r15
	movq	%rbx, %rdx
.L240:
	leaq	-48(%rdx), %r13
	testq	%rdi, %rdi
	je	.L244
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L244:
	movq	%r12, (%r15)
	testq	%r12, %r12
	je	.L245
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L245:
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L343
.L239:
	movq	32(%rbx), %rdi
	movq	%rbx, %rax
	cmpq	-56(%rbp), %rdi
	je	.L246
.L241:
	leaq	-24(%rax), %r12
	testq	%rdi, %rdi
	je	.L247
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L247:
	movq	-56(%rbp), %rax
	movq	%r12, %rsi
	movq	%rax, (%r14)
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L246:
	movq	%rbx, %rax
	jmp	.L327
.L186:
	movq	16(%rdi), %rax
	leaq	16(%rdi), %r12
	cmpq	%r8, %rax
	je	.L189
	movq	%rdi, %r15
	movq	%rax, %rdi
	jmp	.L187
.L338:
	movq	32(%rbx), %rdi
	leaq	16(%rdi), %r12
.L189:
	movq	8(%r12), %r8
	cmpq	%r8, %r9
	je	.L214
	leaq	8(%r12), %r14
	jmp	.L192
.L213:
	movq	32(%rbx), %rax
	movq	24(%rax), %rdi
	cmpq	%r9, %rdi
	je	.L214
	leaq	24(%rax), %r14
	jmp	.L215
.L178:
	cmpq	-72(%rbp), %r9
	jne	.L179
.L181:
	movq	24(%rdx), %rdi
	cmpq	-72(%rbp), %rdi
	je	.L170
	leaq	24(%rdx), %rcx
	jmp	.L183
.L205:
	cmpq	-72(%rbp), %r9
	jne	.L206
	movq	%rdx, %r8
.L208:
	movq	24(%r8), %rdi
	cmpq	-72(%rbp), %rdi
	je	.L197
	leaq	24(%r8), %rcx
	jmp	.L210
.L342:
	movq	32(%rbx), %rdx
	movq	24(%rdx), %rdi
	movq	%rdx, %rax
	cmpq	%rdi, %r12
	je	.L243
	leaq	24(%rdx), %r15
	jmp	.L240
.L343:
	movq	32(%rbx), %rax
.L243:
	movq	16(%rax), %rdi
	leaq	16(%rax), %r14
	cmpq	-56(%rbp), %rdi
	jne	.L241
	jmp	.L246
.L340:
	movq	(%rcx), %rdx
	jmp	.L181
.L341:
	movq	(%rcx), %r8
	jmp	.L208
.L229:
	cmpq	-56(%rbp), %r8
	jne	.L230
.L232:
	movq	(%rcx), %rax
	movq	24(%rax), %rdi
	cmpq	-56(%rbp), %rdi
	je	.L250
	leaq	24(%rax), %rcx
	jmp	.L234
	.cfi_endproc
.LFE19913:
	.size	_ZN2v88internal8compiler22MachineOperatorReducer14ReduceInt32AddEPNS1_4NodeE, .-_ZN2v88internal8compiler22MachineOperatorReducer14ReduceInt32AddEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler22MachineOperatorReducer8Int32AddEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22MachineOperatorReducer8Int32AddEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler22MachineOperatorReducer8Int32AddEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler22MachineOperatorReducer8Int32AddEPNS1_4NodeES4_:
.LFB19907:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movaps	%xmm0, -80(%rbp)
	movq	16(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Int32AddEv@PLT
	movdqa	-80(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	leaq	-64(%rbp), %rcx
	movq	%rax, %rsi
	movl	$2, %edx
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler22MachineOperatorReducer14ReduceInt32AddEPNS1_4NodeE
	testq	%rax, %rax
	cmove	%rbx, %rax
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L349
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L349:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19907:
	.size	_ZN2v88internal8compiler22MachineOperatorReducer8Int32AddEPNS1_4NodeES4_, .-_ZN2v88internal8compiler22MachineOperatorReducer8Int32AddEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler22MachineOperatorReducer8Int32SubEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22MachineOperatorReducer8Int32SubEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler22MachineOperatorReducer8Int32SubEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler22MachineOperatorReducer8Int32SubEPNS1_4NodeES4_:
.LFB19908:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movaps	%xmm0, -80(%rbp)
	movq	16(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Int32SubEv@PLT
	movdqa	-80(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	leaq	-64(%rbp), %rcx
	movq	%rax, %rsi
	movl	$2, %edx
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler22MachineOperatorReducer14ReduceInt32SubEPNS1_4NodeE
	testq	%rax, %rax
	cmove	%rbx, %rax
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L355
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L355:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19908:
	.size	_ZN2v88internal8compiler22MachineOperatorReducer8Int32SubEPNS1_4NodeES4_, .-_ZN2v88internal8compiler22MachineOperatorReducer8Int32SubEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler22MachineOperatorReducer9Uint32DivEPNS1_4NodeEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22MachineOperatorReducer9Uint32DivEPNS1_4NodeEj
	.type	_ZN2v88internal8compiler22MachineOperatorReducer9Uint32DivEPNS1_4NodeEj, @function
_ZN2v88internal8compiler22MachineOperatorReducer9Uint32DivEPNS1_4NodeEj:
.LFB19911:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	%rsi, -112(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	je	.L364
	xorl	%esi, %esi
	rep bsfl	%edx, %esi
	movl	%esi, %r14d
	testl	%esi, %esi
	jne	.L357
	xorl	%r14d, %r14d
	leaq	-80(%rbp), %r13
	jmp	.L358
	.p2align 4,,10
	.p2align 3
.L364:
	movl	$32, %esi
	movl	$32, %r14d
.L357:
	movq	16(%rbx), %rdi
	leaq	-80(%rbp), %r13
	movq	(%rdi), %r15
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%rax, -120(%rbp)
	movq	16(%rbx), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32ShrEv@PLT
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$2, %edx
	movq	-112(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movhps	-120(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	%r14d, %ecx
	movq	%rax, -112(%rbp)
	shrl	%cl, %r12d
.L358:
	movl	%r14d, %esi
	movl	%r12d, %edi
	call	_ZN2v84base26UnsignedDivisionByConstantIjEENS0_23MagicNumbersForDivisionIT_EES3_j@PLT
	movq	16(%rbx), %rdi
	movq	%rax, %rcx
	movl	%eax, %esi
	movl	%edx, -84(%rbp)
	shrq	$32, %rcx
	movq	(%rdi), %r12
	movq	%rax, -92(%rbp)
	movq	%rcx, %r14
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%rax, -120(%rbp)
	movq	16(%rbx), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder13Uint32MulHighEv@PLT
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movq	%r13, %rcx
	movq	-112(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movhps	-120(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	cmpb	$0, -84(%rbp)
	movq	%rax, %r12
	jne	.L375
	testl	%r14d, %r14d
	jne	.L376
.L356:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L377
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L376:
	.cfi_restore_state
	movq	16(%rbx), %rdi
	movl	%r14d, %esi
	movq	(%rdi), %r15
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%rax, -112(%rbp)
	movq	16(%rbx), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32ShrEv@PLT
	movq	%r12, %xmm0
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movhps	-112(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movq	%r15, %rdi
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r12
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L375:
	movq	%rax, %xmm1
	movq	16(%rbx), %rax
	movq	-112(%rbp), %xmm0
	leal	-1(%r14), %r9d
	movl	%r9d, -120(%rbp)
	movq	16(%rax), %rdi
	punpcklqdq	%xmm1, %xmm0
	movq	(%rax), %r14
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Int32SubEv@PLT
	movdqa	-112(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$2, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler22MachineOperatorReducer14ReduceInt32SubEPNS1_4NodeE
	movq	16(%rbx), %rdi
	movl	$1, %esi
	testq	%rax, %rax
	movq	%rax, %r15
	cmove	%r14, %r15
	movq	(%rdi), %r14
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%rax, -112(%rbp)
	movq	16(%rbx), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32ShrEv@PLT
	movq	%r15, %xmm0
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movhps	-112(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movq	%r14, %rdi
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -112(%rbp)
	movq	16(%rbx), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Int32AddEv@PLT
	movq	%r12, %xmm2
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	-112(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movq	%r14, %rdi
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler22MachineOperatorReducer14ReduceInt32AddEPNS1_4NodeE
	movl	-120(%rbp), %r9d
	movq	%rax, %r12
	testq	%rax, %rax
	cmove	%r15, %r12
	testl	%r9d, %r9d
	je	.L356
	movq	16(%rbx), %rdi
	movl	%r9d, %esi
	movq	(%rdi), %r14
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%rax, -112(%rbp)
	movq	16(%rbx), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32ShrEv@PLT
	movq	%r12, %xmm0
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movhps	-112(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movq	%r14, %rdi
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r12
	jmp	.L356
.L377:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19911:
	.size	_ZN2v88internal8compiler22MachineOperatorReducer9Uint32DivEPNS1_4NodeEj, .-_ZN2v88internal8compiler22MachineOperatorReducer9Uint32DivEPNS1_4NodeEj
	.section	.text._ZN2v88internal8compiler22MachineOperatorReducer8Int32DivEPNS1_4NodeEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22MachineOperatorReducer8Int32DivEPNS1_4NodeEi
	.type	_ZN2v88internal8compiler22MachineOperatorReducer8Int32DivEPNS1_4NodeEi, @function
_ZN2v88internal8compiler22MachineOperatorReducer8Int32DivEPNS1_4NodeEi:
.LFB19910:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	%edx, %edi
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -120(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v84base24SignedDivisionByConstantIjEENS0_23MagicNumbersForDivisionIT_EES3_@PLT
	movq	16(%r12), %rdi
	movl	%edx, -84(%rbp)
	movq	(%rdi), %r15
	movl	%eax, %esi
	movq	%rax, -92(%rbp)
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%rax, -112(%rbp)
	movq	16(%r12), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder12Int32MulHighEv@PLT
	movq	%r15, %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	-120(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movhps	-112(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%r12), %rdi
	movq	%rax, -112(%rbp)
	movq	(%rdi), %r15
	testl	%r13d, %r13d
	jle	.L379
	testl	%ebx, %ebx
	js	.L394
.L380:
	movl	$31, %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%rax, -128(%rbp)
	movq	16(%r12), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32ShrEv@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$2, %edx
	movq	-120(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movhps	-128(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	-88(%rbp), %esi
	movq	%rax, -120(%rbp)
	testl	%esi, %esi
	je	.L383
	movq	16(%r12), %rdi
	movq	(%rdi), %r13
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%rax, -128(%rbp)
	movq	16(%r12), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32SarEv@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$2, %edx
	movq	-112(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r13, %rdi
	movhps	-128(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -112(%rbp)
.L383:
	movq	16(%r12), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Int32AddEv@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$2, %edx
	movq	-112(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r13, %rdi
	movhps	-120(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler22MachineOperatorReducer14ReduceInt32AddEPNS1_4NodeE
	testq	%rax, %rax
	cmove	%rbx, %rax
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L395
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L379:
	.cfi_restore_state
	testl	%ebx, %ebx
	jle	.L380
	testl	%r13d, %r13d
	jns	.L380
	movq	16(%rdi), %rdi
	movq	%rax, %xmm0
	movhps	-120(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Int32SubEv@PLT
	movdqa	-112(%rbp), %xmm0
	movq	%r15, %rdi
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movq	%r14, %rcx
	movl	$2, %edx
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler22MachineOperatorReducer14ReduceInt32SubEPNS1_4NodeE
	movq	16(%r12), %rdi
	movq	%rax, -112(%rbp)
	movq	(%rdi), %r15
	testq	%rax, %rax
	jne	.L380
.L382:
	movq	%rbx, -112(%rbp)
	jmp	.L380
	.p2align 4,,10
	.p2align 3
.L394:
	movq	16(%rdi), %rdi
	movq	%rax, %xmm0
	movhps	-120(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Int32AddEv@PLT
	movdqa	-112(%rbp), %xmm0
	movq	%r15, %rdi
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movq	%r14, %rcx
	movl	$2, %edx
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler22MachineOperatorReducer14ReduceInt32AddEPNS1_4NodeE
	movq	16(%r12), %rdi
	movq	%rax, -112(%rbp)
	movq	(%rdi), %r15
	testq	%rax, %rax
	jne	.L380
	jmp	.L382
.L395:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19910:
	.size	_ZN2v88internal8compiler22MachineOperatorReducer8Int32DivEPNS1_4NodeEi, .-_ZN2v88internal8compiler22MachineOperatorReducer8Int32DivEPNS1_4NodeEi
	.section	.rodata._ZN2v88internal8compiler22MachineOperatorReducer14ReduceInt32DivEPNS1_4NodeE.str1.1,"aMS",@progbits,1
.LC4:
	.string	"unreachable code"
	.section	.text._ZN2v88internal8compiler22MachineOperatorReducer14ReduceInt32DivEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22MachineOperatorReducer14ReduceInt32DivEPNS1_4NodeE
	.type	_ZN2v88internal8compiler22MachineOperatorReducer14ReduceInt32DivEPNS1_4NodeE, @function
_ZN2v88internal8compiler22MachineOperatorReducer14ReduceInt32DivEPNS1_4NodeE:
.LFB19917:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	32(%rsi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$152, %rsp
	movzbl	23(%rsi), %edx
	movq	32(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L397
	movq	(%rsi), %rcx
	movzwl	16(%rcx), %eax
	cmpw	$23, %ax
	sete	%r10b
	je	.L398
	xorl	%r8d, %r8d
	xorl	%r11d, %r11d
.L399:
	leaq	8(%r14), %rax
	movq	%rsi, %r12
	movq	%r14, %r9
.L402:
	movq	(%rax), %r15
	movq	(%r15), %rcx
	movzwl	16(%rcx), %edi
	cmpw	$23, %di
	jne	.L403
	movl	44(%rcx), %r9d
	testb	%r10b, %r10b
	je	.L405
	testl	%r8d, %r8d
	je	.L527
.L414:
	cmpw	$23, %di
	je	.L529
.L416:
	cmpq	%r15, %r12
	jne	.L530
.L460:
	movq	16(%r13), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%rax, -88(%rbp)
	movq	16(%r13), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler22MachineOperatorBuilder11Word32EqualEv@PLT
	movq	%r12, %xmm0
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	leaq	-80(%rbp), %r12
	movhps	-88(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movq	%r12, %rcx
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -96(%rbp)
	movq	16(%r13), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler22MachineOperatorBuilder11Word32EqualEv@PLT
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movl	$2, %edx
	movq	-96(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r13, %rdi
	movhps	-88(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	.p2align 4,,10
	.p2align 3
.L415:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L531
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L398:
	.cfi_restore_state
	movl	44(%rcx), %r8d
	movl	$1, %r11d
	jmp	.L399
	.p2align 4,,10
	.p2align 3
.L403:
	movq	(%rbx), %rax
	testb	$1, 18(%rax)
	je	.L406
	testb	%r11b, %r11b
	jne	.L532
.L406:
	xorl	%r9d, %r9d
	testb	%r10b, %r10b
	je	.L416
	testl	%r8d, %r8d
	jne	.L414
.L527:
	movq	%r12, %rax
	jmp	.L415
	.p2align 4,,10
	.p2align 3
.L529:
	movl	$1, %r10d
.L405:
	movq	%r15, %rax
	testl	%r9d, %r9d
	je	.L415
	cmpl	$1, %r9d
	je	.L527
	testb	%r10b, %r10b
	je	.L533
	movl	%r9d, %esi
	movl	%r8d, %edi
	call	_ZN2v84base4bits11SignedDiv32Eii@PLT
	movq	16(%r13), %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	jmp	.L415
	.p2align 4,,10
	.p2align 3
.L397:
	movq	16(%rsi), %r12
	leaq	16(%rsi), %r9
	movq	(%r12), %rcx
	movzwl	16(%rcx), %eax
	cmpw	$23, %ax
	sete	%r10b
	je	.L400
	xorl	%r8d, %r8d
	xorl	%r11d, %r11d
.L401:
	leaq	8(%r9), %rax
	jmp	.L402
	.p2align 4,,10
	.p2align 3
.L400:
	movl	44(%rcx), %r8d
	movl	$1, %r11d
	jmp	.L401
	.p2align 4,,10
	.p2align 3
.L530:
	xorl	%eax, %eax
	jmp	.L415
	.p2align 4,,10
	.p2align 3
.L532:
	cmpl	$15, %edx
	je	.L407
	movq	%rbx, %rsi
	cmpq	%r15, %r12
	je	.L409
.L408:
	subq	$24, %rsi
	movq	%r12, %rdi
	movl	%r8d, -96(%rbp)
	movq	%r9, -104(%rbp)
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-104(%rbp), %r9
	movq	-88(%rbp), %rsi
	movq	%r15, %rdi
	movq	%r15, (%r9)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movzbl	23(%rbx), %eax
	movl	-96(%rbp), %r8d
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L534
.L409:
	movq	8(%r14), %rdi
	cmpq	%rdi, %r12
	je	.L459
	leaq	8(%r14), %rax
	movq	%rbx, %rsi
.L412:
	subq	$48, %rsi
	testq	%rdi, %rdi
	je	.L413
	movq	%rax, -104(%rbp)
	movl	%r8d, -96(%rbp)
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-88(%rbp), %rsi
	movl	-96(%rbp), %r8d
	movq	-104(%rbp), %rax
.L413:
	movq	%r12, (%rax)
	movq	%r12, %rdi
	movl	%r8d, -88(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movl	-88(%rbp), %r8d
.L459:
	movq	%r12, %rax
	movl	%r8d, %r9d
	movq	%r15, %r12
	xorl	%r8d, %r8d
	movq	%rax, %r15
	xorl	%r10d, %r10d
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L533:
	cmpq	%r15, %r12
	je	.L460
	cmpl	$-1, %r9d
	je	.L535
	movl	%r9d, %eax
	movl	%r9d, %edx
	sarl	$31, %eax
	xorl	%eax, %edx
	subl	%eax, %edx
	je	.L430
	leal	-1(%rdx), %eax
	testl	%edx, %eax
	jne	.L430
	cmpl	$65535, %edx
	jbe	.L466
	movl	$4, -192(%rbp)
	shrl	$16, %edx
	movl	$10, %r15d
	movl	$9, %r8d
	movl	$3, -188(%rbp)
	movl	$23, %edi
	movl	$22, %r11d
	movl	$13, %ecx
	movl	$2, -184(%rbp)
	movl	$19, %eax
	movl	$16, %esi
	movl	$16, %r10d
	movl	$1, -180(%rbp)
	movl	$31, -176(%rbp)
	movl	$30, -172(%rbp)
	movl	$29, -168(%rbp)
	movl	$5, -164(%rbp)
	movl	$27, -160(%rbp)
	movl	$6, -156(%rbp)
	movl	$26, -152(%rbp)
	movl	$7, -148(%rbp)
	movl	$25, -144(%rbp)
	movl	$8, -140(%rbp)
	movl	$28, -136(%rbp)
	movl	$12, -104(%rbp)
	movl	$11, -128(%rbp)
	movl	$21, -124(%rbp)
	movl	$14, -112(%rbp)
	movl	$18, -88(%rbp)
	movl	$15, -120(%rbp)
	movl	$17, -116(%rbp)
	movl	$20, -96(%rbp)
	movl	$24, -132(%rbp)
.L433:
	cmpl	$255, %edx
	jbe	.L434
	movl	-156(%rbp), %esi
	movl	-192(%rbp), %eax
	shrl	$8, %edx
	movl	-136(%rbp), %r10d
	movl	-184(%rbp), %r15d
	movl	-180(%rbp), %r8d
	movl	-176(%rbp), %edi
	movl	%esi, -112(%rbp)
	movl	-152(%rbp), %esi
	movl	-164(%rbp), %ecx
	movl	%eax, -104(%rbp)
	movl	-188(%rbp), %eax
	movl	%r10d, -96(%rbp)
	movl	%esi, -88(%rbp)
	movl	-148(%rbp), %esi
	movl	%eax, -128(%rbp)
	movl	-168(%rbp), %eax
	movl	%esi, -120(%rbp)
	movl	-144(%rbp), %esi
	movl	%eax, -124(%rbp)
	movl	-172(%rbp), %r11d
	movl	-160(%rbp), %eax
	movl	-132(%rbp), %r10d
	movl	%esi, -116(%rbp)
	movl	-140(%rbp), %esi
.L434:
	cmpl	$15, %edx
	jbe	.L435
	shrl	$4, %edx
	cmpl	$2, %edx
	je	.L467
	jbe	.L536
	cmpl	$4, %edx
	jne	.L537
	movl	%r15d, %esi
	movl	%r11d, %r10d
	.p2align 4,,10
	.p2align 3
.L443:
	movq	%r12, -88(%rbp)
	leaq	-80(%rbp), %r15
	cmpl	$1, %r10d
	jg	.L538
.L444:
	movq	16(%r13), %rdi
	movl	%r10d, -116(%rbp)
	movl	%r9d, -112(%rbp)
	movq	(%rdi), %r11
	movq	%r11, -104(%rbp)
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%rax, -96(%rbp)
	movq	16(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32ShrEv@PLT
	movq	-104(%rbp), %r11
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	-88(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movq	%r11, %rdi
	movhps	-96(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -88(%rbp)
	movq	16(%r13), %rax
	movq	(%rax), %r11
	movq	16(%rax), %rdi
	movq	%r11, -96(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Int32AddEv@PLT
	movq	-96(%rbp), %r11
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	-88(%rbp), %xmm0
	movq	%r12, %xmm1
	movl	$2, %edx
	movq	%rax, %rsi
	movq	%r11, %rdi
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorReducer14ReduceInt32AddEPNS1_4NodeE
	movq	-88(%rbp), %rdx
	movl	-116(%rbp), %r10d
	testq	%rax, %rax
	movq	%rax, %r12
	movl	-112(%rbp), %r9d
	cmove	%rdx, %r12
	testl	%r10d, %r10d
	je	.L432
	movq	16(%r13), %rdi
	movl	%r10d, %esi
	movl	%r9d, -104(%rbp)
	movq	(%rdi), %r11
	movq	%r11, -96(%rbp)
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%rax, -88(%rbp)
	movq	16(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32SarEv@PLT
	movq	-96(%rbp), %r11
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%r12, %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movhps	-88(%rbp), %xmm0
	movq	%r11, %rdi
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	-104(%rbp), %r9d
	movq	%rax, %r12
	.p2align 4,,10
	.p2align 3
.L432:
	movq	%r12, %rax
	testl	%r9d, %r9d
	jns	.L415
	movq	16(%r13), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%rax, %r8
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L447
	movq	32(%rbx), %rdi
	movq	%r14, %rax
	movq	%rbx, %rsi
	cmpq	%rdi, %r8
	je	.L449
.L448:
	leaq	-24(%rsi), %r15
	testq	%rdi, %rdi
	je	.L451
	movq	%r15, %rsi
	movq	%rax, -96(%rbp)
	movq	%r8, -88(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-96(%rbp), %rax
	movq	-88(%rbp), %r8
.L451:
	movq	%r8, (%rax)
	testq	%r8, %r8
	je	.L452
	movq	%r15, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L452:
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L539
.L449:
	movq	8(%r14), %rdi
	cmpq	%rdi, %r12
	je	.L455
	addq	$8, %r14
	movq	%rbx, %rsi
.L454:
	leaq	-48(%rsi), %r15
	testq	%rdi, %rdi
	je	.L456
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L456:
	movq	%r12, (%r14)
	testq	%r12, %r12
	je	.L455
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	.p2align 4,,10
	.p2align 3
.L455:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler4Node14TrimInputCountEi@PLT
	movq	16(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Int32SubEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	%rbx, %rax
	jmp	.L415
	.p2align 4,,10
	.p2align 3
.L534:
	movq	32(%rbx), %rsi
.L410:
	movq	24(%rsi), %rdi
	cmpq	%rdi, %r12
	je	.L459
	leaq	24(%rsi), %rax
	jmp	.L412
	.p2align 4,,10
	.p2align 3
.L430:
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	%r9d, -88(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorReducer8Int32DivEPNS1_4NodeEi
	movl	-88(%rbp), %r9d
	movq	%rax, %r12
	jmp	.L432
	.p2align 4,,10
	.p2align 3
.L535:
	movq	16(%r13), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%rax, %r8
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L420
	movq	32(%rbx), %rdi
	movq	%r14, %rax
	movq	%rbx, %rsi
	cmpq	%rdi, %r8
	je	.L422
.L421:
	leaq	-24(%rsi), %r15
	testq	%rdi, %rdi
	je	.L424
	movq	%r15, %rsi
	movq	%rax, -96(%rbp)
	movq	%r8, -88(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-96(%rbp), %rax
	movq	-88(%rbp), %r8
.L424:
	movq	%r8, (%rax)
	testq	%r8, %r8
	je	.L425
	movq	%r15, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L425:
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L540
.L422:
	movq	8(%r14), %rdi
	cmpq	%rdi, %r12
	je	.L455
	addq	$8, %r14
	movq	%rbx, %rsi
.L427:
	leaq	-48(%rsi), %r15
	testq	%rdi, %rdi
	je	.L429
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L429:
	movq	%r12, (%r14)
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	jmp	.L455
	.p2align 4,,10
	.p2align 3
.L407:
	cmpq	%r15, %r12
	jne	.L408
	jmp	.L410
	.p2align 4,,10
	.p2align 3
.L420:
	movq	32(%rbx), %rsi
	movq	16(%rsi), %rdi
	leaq	16(%rsi), %rax
	cmpq	%rdi, %r8
	jne	.L421
.L423:
	movq	8(%rax), %rdi
	cmpq	%rdi, %r12
	je	.L455
	leaq	8(%rax), %r14
	jmp	.L427
	.p2align 4,,10
	.p2align 3
.L435:
	cmpl	$2, %edx
	je	.L469
	jbe	.L541
	cmpl	$4, %edx
	jne	.L542
	movl	-112(%rbp), %esi
	movl	-88(%rbp), %r10d
	jmp	.L443
	.p2align 4,,10
	.p2align 3
.L466:
	movl	$20, -192(%rbp)
	movl	$26, %r15d
	movl	$25, %r8d
	movl	$7, %edi
	movl	$19, -188(%rbp)
	movl	$6, %r11d
	movl	$29, %ecx
	movl	$3, %eax
	movl	$18, -184(%rbp)
	movl	$32, %esi
	xorl	%r10d, %r10d
	movl	$17, -180(%rbp)
	movl	$15, -176(%rbp)
	movl	$14, -172(%rbp)
	movl	$13, -168(%rbp)
	movl	$21, -164(%rbp)
	movl	$11, -160(%rbp)
	movl	$22, -156(%rbp)
	movl	$10, -152(%rbp)
	movl	$23, -148(%rbp)
	movl	$9, -144(%rbp)
	movl	$24, -140(%rbp)
	movl	$12, -136(%rbp)
	movl	$28, -104(%rbp)
	movl	$27, -128(%rbp)
	movl	$5, -124(%rbp)
	movl	$30, -112(%rbp)
	movl	$2, -88(%rbp)
	movl	$31, -120(%rbp)
	movl	$1, -116(%rbp)
	movl	$4, -96(%rbp)
	movl	$8, -132(%rbp)
	jmp	.L433
.L542:
	cmpl	$8, %edx
	jne	.L439
	movl	%ecx, %r8d
	movl	%eax, %edi
.L441:
	movl	%r8d, %esi
	movl	%edi, %r10d
	jmp	.L443
.L541:
	cmpl	$1, %edx
	je	.L443
.L439:
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L537:
	cmpl	$8, %edx
	je	.L441
	jmp	.L439
	.p2align 4,,10
	.p2align 3
.L536:
	cmpl	$1, %edx
	jne	.L439
.L438:
	movq	16(%r13), %rdi
	movl	$31, %esi
	movl	%r9d, -116(%rbp)
	leaq	-80(%rbp), %r15
	movq	(%rdi), %r10
	movq	%r10, -112(%rbp)
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%rax, -88(%rbp)
	movq	16(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32SarEv@PLT
	movq	-112(%rbp), %r10
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%r12, %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movhps	-88(%rbp), %xmm0
	movq	%r10, %rdi
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	-104(%rbp), %esi
	movl	-96(%rbp), %r10d
	movq	%rax, -88(%rbp)
	movl	-116(%rbp), %r9d
	jmp	.L444
	.p2align 4,,10
	.p2align 3
.L540:
	movq	32(%rbx), %rsi
	leaq	16(%rsi), %rax
	jmp	.L423
	.p2align 4,,10
	.p2align 3
.L447:
	movq	32(%rbx), %rsi
	movq	16(%rsi), %rdi
	leaq	16(%rsi), %rax
	cmpq	%rdi, %r8
	jne	.L448
.L450:
	movq	8(%rax), %rdi
	cmpq	%rdi, %r12
	je	.L455
	leaq	8(%rax), %r14
	jmp	.L454
	.p2align 4,,10
	.p2align 3
.L538:
	movl	%esi, -104(%rbp)
	movl	%r10d, -96(%rbp)
	jmp	.L438
.L539:
	movq	32(%rbx), %rsi
	leaq	16(%rsi), %rax
	jmp	.L450
.L469:
	movl	-120(%rbp), %esi
	movl	-116(%rbp), %r10d
	jmp	.L443
.L467:
	movl	-128(%rbp), %esi
	movl	-124(%rbp), %r10d
	jmp	.L443
.L531:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19917:
	.size	_ZN2v88internal8compiler22MachineOperatorReducer14ReduceInt32DivEPNS1_4NodeE, .-_ZN2v88internal8compiler22MachineOperatorReducer14ReduceInt32DivEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler22MachineOperatorReducer15ReduceUint32DivEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22MachineOperatorReducer15ReduceUint32DivEPNS1_4NodeE
	.type	_ZN2v88internal8compiler22MachineOperatorReducer15ReduceUint32DivEPNS1_4NodeE, @function
_ZN2v88internal8compiler22MachineOperatorReducer15ReduceUint32DivEPNS1_4NodeE:
.LFB19918:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	32(%rsi), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$72, %rsp
	movzbl	23(%rsi), %ecx
	movq	32(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	andl	$15, %ecx
	cmpl	$15, %ecx
	je	.L544
	movq	(%rdi), %rdx
	movzwl	16(%rdx), %eax
	cmpw	$23, %ax
	sete	%sil
	je	.L545
	xorl	%eax, %eax
	xorl	%r10d, %r10d
.L546:
	leaq	8(%r13), %r8
	movq	%rdi, %r14
	movq	%r13, %rdx
.L549:
	movq	(%r8), %r15
	movq	(%r15), %r8
	movzwl	16(%r8), %r9d
	cmpw	$23, %r9w
	jne	.L550
	movl	44(%r8), %r8d
	testb	%sil, %sil
	je	.L552
	testl	%eax, %eax
	je	.L621
.L561:
	cmpw	$23, %r9w
	je	.L623
.L563:
	cmpq	%r14, %r15
	jne	.L624
.L583:
	movq	16(%r12), %rdi
	xorl	%esi, %esi
	leaq	-80(%rbp), %r13
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%rax, -88(%rbp)
	movq	16(%r12), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler22MachineOperatorBuilder11Word32EqualEv@PLT
	movq	%r14, %xmm0
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movhps	-88(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movq	%r15, %rdi
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -96(%rbp)
	movq	16(%r12), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder11Word32EqualEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$2, %edx
	movq	-96(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r12, %rdi
	movhps	-88(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	.p2align 4,,10
	.p2align 3
.L562:
	movq	-56(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L625
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L545:
	.cfi_restore_state
	movl	44(%rdx), %eax
	movl	$1, %r10d
	jmp	.L546
	.p2align 4,,10
	.p2align 3
.L550:
	movq	(%rbx), %r8
	testb	$1, 18(%r8)
	je	.L553
	testb	%r10b, %r10b
	jne	.L626
.L553:
	xorl	%r8d, %r8d
	testb	%sil, %sil
	je	.L563
	testl	%eax, %eax
	jne	.L561
.L621:
	movq	%r14, %rax
	jmp	.L562
	.p2align 4,,10
	.p2align 3
.L623:
	movl	$1, %esi
.L552:
	testl	%r8d, %r8d
	je	.L627
	cmpl	$1, %r8d
	je	.L621
	testb	%sil, %sil
	je	.L628
	xorl	%edx, %edx
	movq	16(%r12), %rdi
	divl	%r8d
	movl	%eax, %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	jmp	.L562
	.p2align 4,,10
	.p2align 3
.L544:
	movq	16(%rdi), %r14
	leaq	16(%rdi), %rdx
	movq	(%r14), %r8
	movzwl	16(%r8), %eax
	cmpw	$23, %ax
	sete	%sil
	je	.L547
	xorl	%eax, %eax
	xorl	%r10d, %r10d
.L548:
	leaq	8(%rdx), %r8
	jmp	.L549
	.p2align 4,,10
	.p2align 3
.L547:
	movl	44(%r8), %eax
	movl	$1, %r10d
	jmp	.L548
	.p2align 4,,10
	.p2align 3
.L624:
	xorl	%eax, %eax
	jmp	.L562
	.p2align 4,,10
	.p2align 3
.L627:
	movq	%r15, %rax
	jmp	.L562
	.p2align 4,,10
	.p2align 3
.L626:
	cmpl	$15, %ecx
	je	.L554
	movq	%rbx, %rdi
	cmpq	%r14, %r15
	je	.L556
.L555:
	leaq	-24(%rdi), %rsi
	movq	%r14, %rdi
	movl	%eax, -100(%rbp)
	movq	%rdx, -96(%rbp)
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-96(%rbp), %rdx
	movq	-88(%rbp), %rsi
	movq	%r15, %rdi
	movq	%r15, (%rdx)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movzbl	23(%rbx), %edx
	movl	-100(%rbp), %eax
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L629
.L556:
	movq	8(%r13), %r8
	cmpq	%r14, %r8
	je	.L582
	leaq	8(%r13), %rdx
	movq	%rbx, %rdi
.L559:
	leaq	-48(%rdi), %rsi
	testq	%r8, %r8
	je	.L560
	movq	%r8, %rdi
	movl	%eax, -100(%rbp)
	movq	%rdx, -96(%rbp)
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-88(%rbp), %rsi
	movq	-96(%rbp), %rdx
	movl	-100(%rbp), %eax
.L560:
	movq	%r14, (%rdx)
	movq	%r14, %rdi
	movl	%eax, -88(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movl	-88(%rbp), %eax
.L582:
	movl	%eax, %r8d
	movq	%r14, %rax
	xorl	%esi, %esi
	movq	%r15, %r14
	movq	%rax, %r15
	xorl	%eax, %eax
	jmp	.L552
	.p2align 4,,10
	.p2align 3
.L628:
	cmpq	%r14, %r15
	je	.L583
	leal	-1(%r8), %eax
	testl	%r8d, %eax
	jne	.L630
	cmpl	$65535, %r8d
	jbe	.L588
	movl	$30, -108(%rbp)
	shrl	$16, %r8d
	movl	$31, %r15d
	movl	$23, %r11d
	movl	$29, -104(%rbp)
	movl	$22, %r10d
	movl	$21, %r9d
	movl	$19, %edx
	movl	$27, -100(%rbp)
	movl	$18, %ecx
	movl	$17, %eax
	movl	$20, %edi
	movl	$26, -96(%rbp)
	movl	$24, %r14d
	movl	$16, %esi
	movl	$25, -88(%rbp)
	movl	$28, -112(%rbp)
.L567:
	cmpl	$255, %r8d
	jbe	.L568
	movl	-108(%rbp), %r10d
	movl	-104(%rbp), %r9d
	shrl	$8, %r8d
	movl	%r15d, %r11d
	movl	-100(%rbp), %edx
	movl	-96(%rbp), %ecx
	movl	%r14d, %esi
	movl	-88(%rbp), %eax
	movl	-112(%rbp), %edi
.L568:
	cmpl	$15, %r8d
	jbe	.L569
	shrl	$4, %r8d
	movl	%r11d, %edx
	movl	%r10d, %ecx
	movl	%r9d, %eax
	movl	%edi, %esi
.L569:
	cmpl	$4, %r8d
	je	.L589
	ja	.L571
	cmpl	$1, %r8d
	je	.L572
	cmpl	$2, %r8d
	jne	.L574
.L573:
	movl	%eax, %esi
.L572:
	movq	16(%r12), %rdi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%rax, %r14
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L575
	movq	8(%r13), %rdi
	cmpq	%rdi, %r14
	je	.L578
	addq	$8, %r13
	movq	%rbx, %rsi
.L577:
	leaq	-48(%rsi), %r15
	testq	%rdi, %rdi
	je	.L579
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L579:
	movq	%r14, 0(%r13)
	testq	%r14, %r14
	je	.L578
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L578:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler4Node14TrimInputCountEi@PLT
	movq	16(%r12), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32ShrEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	%rbx, %rax
	jmp	.L562
	.p2align 4,,10
	.p2align 3
.L629:
	movq	32(%rbx), %rdi
.L557:
	movq	24(%rdi), %r8
	cmpq	%r14, %r8
	je	.L582
	leaq	24(%rdi), %rdx
	jmp	.L559
	.p2align 4,,10
	.p2align 3
.L630:
	movl	%r8d, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22MachineOperatorReducer9Uint32DivEPNS1_4NodeEj
	jmp	.L562
	.p2align 4,,10
	.p2align 3
.L571:
	cmpl	$8, %r8d
	jne	.L574
	movl	%edx, %eax
	jmp	.L573
	.p2align 4,,10
	.p2align 3
.L554:
	cmpq	%r14, %r15
	jne	.L555
	jmp	.L557
	.p2align 4,,10
	.p2align 3
.L588:
	movl	$14, -108(%rbp)
	movl	$15, %r15d
	xorl	%esi, %esi
	movl	$7, %r11d
	movl	$13, -104(%rbp)
	movl	$6, %r10d
	movl	$5, %r9d
	movl	$3, %edx
	movl	$11, -100(%rbp)
	movl	$2, %ecx
	movl	$1, %eax
	movl	$4, %edi
	movl	$10, -96(%rbp)
	movl	$8, %r14d
	movl	$9, -88(%rbp)
	movl	$12, -112(%rbp)
	jmp	.L567
	.p2align 4,,10
	.p2align 3
.L575:
	movq	32(%rbx), %rsi
	movq	24(%rsi), %rdi
	cmpq	%rdi, %r14
	je	.L578
	leaq	24(%rsi), %r13
	jmp	.L577
	.p2align 4,,10
	.p2align 3
.L589:
	movl	%ecx, %edx
	movl	%edx, %eax
	jmp	.L573
.L625:
	call	__stack_chk_fail@PLT
.L574:
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19918:
	.size	_ZN2v88internal8compiler22MachineOperatorReducer15ReduceUint32DivEPNS1_4NodeE, .-_ZN2v88internal8compiler22MachineOperatorReducer15ReduceUint32DivEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler22MachineOperatorReducer15ReduceUint32ModEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22MachineOperatorReducer15ReduceUint32ModEPNS1_4NodeE
	.type	_ZN2v88internal8compiler22MachineOperatorReducer15ReduceUint32ModEPNS1_4NodeE, @function
_ZN2v88internal8compiler22MachineOperatorReducer15ReduceUint32ModEPNS1_4NodeE:
.LFB19921:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	32(%rsi), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$72, %rsp
	movzbl	23(%rsi), %edx
	movq	32(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L632
	movq	(%rsi), %rcx
	movzwl	16(%rcx), %eax
	cmpw	$23, %ax
	sete	%r9b
	je	.L633
	xorl	%eax, %eax
	xorl	%r10d, %r10d
.L634:
	leaq	8(%r13), %rcx
	movq	%rsi, %r8
	movq	%r13, %r14
.L637:
	movq	(%rcx), %r15
	movq	(%r15), %rcx
	movzwl	16(%rcx), %edi
	cmpw	$23, %di
	jne	.L638
	movl	44(%rcx), %r14d
	testb	%r9b, %r9b
	je	.L640
	testl	%eax, %eax
	je	.L707
.L649:
	cmpw	$23, %di
	je	.L708
.L651:
	xorl	%eax, %eax
	cmpq	%r8, %r15
	je	.L654
.L650:
	movq	-56(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L709
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L633:
	.cfi_restore_state
	movl	44(%rcx), %eax
	movl	$1, %r10d
	jmp	.L634
	.p2align 4,,10
	.p2align 3
.L638:
	movq	(%rbx), %rcx
	testb	$1, 18(%rcx)
	je	.L641
	testb	%r10b, %r10b
	jne	.L710
.L641:
	xorl	%r14d, %r14d
	testb	%r9b, %r9b
	je	.L651
	testl	%eax, %eax
	jne	.L649
.L707:
	movq	%r8, %rax
	jmp	.L650
	.p2align 4,,10
	.p2align 3
.L708:
	movl	$1, %r9d
.L640:
	testl	%r14d, %r14d
	je	.L711
	cmpl	$1, %r14d
	je	.L654
	cmpq	%r8, %r15
	je	.L654
	testb	%r9b, %r9b
	je	.L655
	xorl	%esi, %esi
	testl	%r14d, %r14d
	je	.L656
	xorl	%edx, %edx
	divl	%r14d
	movl	%edx, %esi
.L656:
	movq	16(%r12), %rdi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	jmp	.L650
	.p2align 4,,10
	.p2align 3
.L632:
	movq	16(%rsi), %r8
	leaq	16(%rsi), %r14
	movq	(%r8), %rcx
	movzwl	16(%rcx), %eax
	cmpw	$23, %ax
	sete	%r9b
	je	.L635
	xorl	%eax, %eax
	xorl	%r10d, %r10d
.L636:
	leaq	8(%r14), %rcx
	jmp	.L637
	.p2align 4,,10
	.p2align 3
.L654:
	movq	16(%r12), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	jmp	.L650
	.p2align 4,,10
	.p2align 3
.L635:
	movl	44(%rcx), %eax
	movl	$1, %r10d
	jmp	.L636
	.p2align 4,,10
	.p2align 3
.L655:
	testl	%r14d, %r14d
	je	.L657
	leal	-1(%r14), %esi
	testl	%r14d, %esi
	jne	.L657
	movq	16(%r12), %rdi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%rax, %r14
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L665
	movq	8(%r13), %rdi
	cmpq	%rdi, %r14
	je	.L668
	addq	$8, %r13
	movq	%rbx, %rsi
.L667:
	leaq	-48(%rsi), %r15
	testq	%rdi, %rdi
	je	.L669
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L669:
	movq	%r14, 0(%r13)
	testq	%r14, %r14
	je	.L668
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L668:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler4Node14TrimInputCountEi@PLT
	movq	16(%r12), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32AndEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	jmp	.L664
	.p2align 4,,10
	.p2align 3
.L657:
	movl	%r14d, %edx
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22MachineOperatorReducer9Uint32DivEPNS1_4NodeEj
	movq	16(%r12), %rdi
	movl	%r14d, %esi
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%rax, -96(%rbp)
	movq	16(%r12), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Int32MulEv@PLT
	movq	%r14, %rdi
	leaq	-80(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	-88(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movhps	-96(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r14
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L659
	movq	8(%r13), %rdi
	cmpq	%rdi, %r14
	je	.L662
	addq	$8, %r13
	movq	%rbx, %rsi
.L661:
	leaq	-48(%rsi), %r15
	testq	%rdi, %rdi
	je	.L663
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L663:
	movq	%r14, 0(%r13)
	testq	%r14, %r14
	je	.L662
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L662:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler4Node14TrimInputCountEi@PLT
	movq	16(%r12), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Int32SubEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
.L664:
	movq	%rbx, %rax
	jmp	.L650
	.p2align 4,,10
	.p2align 3
.L711:
	movq	%r15, %rax
	jmp	.L650
	.p2align 4,,10
	.p2align 3
.L710:
	cmpl	$15, %edx
	je	.L642
	movq	%rbx, %rsi
	cmpq	%r8, %r15
	je	.L644
.L643:
	subq	$24, %rsi
	movq	%r8, %rdi
	movl	%eax, -100(%rbp)
	movq	%r8, -88(%rbp)
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	%r15, (%r14)
	movq	-96(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movzbl	23(%rbx), %edx
	movq	-88(%rbp), %r8
	movl	-100(%rbp), %eax
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L712
.L644:
	movq	8(%r13), %rdi
	cmpq	%r8, %rdi
	je	.L670
	leaq	8(%r13), %rdx
	movq	%rbx, %rsi
.L647:
	leaq	-48(%rsi), %r14
	testq	%rdi, %rdi
	je	.L648
	movq	%r14, %rsi
	movl	%eax, -100(%rbp)
	movq	%r8, -96(%rbp)
	movq	%rdx, -88(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %r8
	movl	-100(%rbp), %eax
.L648:
	movq	%r8, (%rdx)
	movq	%r8, %rdi
	movq	%r14, %rsi
	movl	%eax, -96(%rbp)
	movq	%r8, -88(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movq	-88(%rbp), %r8
	movl	-96(%rbp), %eax
.L670:
	movl	%eax, %r14d
	movq	%r8, %rax
	xorl	%r9d, %r9d
	movq	%r15, %r8
	movq	%rax, %r15
	xorl	%eax, %eax
	jmp	.L640
	.p2align 4,,10
	.p2align 3
.L712:
	movq	32(%rbx), %rsi
.L645:
	movq	24(%rsi), %rdi
	cmpq	%rdi, %r8
	je	.L670
	leaq	24(%rsi), %rdx
	jmp	.L647
	.p2align 4,,10
	.p2align 3
.L659:
	movq	32(%rbx), %rsi
	movq	24(%rsi), %rdi
	cmpq	%rdi, %r14
	je	.L662
	leaq	24(%rsi), %r13
	jmp	.L661
	.p2align 4,,10
	.p2align 3
.L642:
	cmpq	%r8, %r15
	jne	.L643
	jmp	.L645
	.p2align 4,,10
	.p2align 3
.L665:
	movq	32(%rbx), %rsi
	movq	24(%rsi), %rdi
	cmpq	%rdi, %r14
	je	.L668
	leaq	24(%rsi), %r13
	jmp	.L667
.L709:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19921:
	.size	_ZN2v88internal8compiler22MachineOperatorReducer15ReduceUint32ModEPNS1_4NodeE, .-_ZN2v88internal8compiler22MachineOperatorReducer15ReduceUint32ModEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler22MachineOperatorReducer11ReduceStoreEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22MachineOperatorReducer11ReduceStoreEPNS1_4NodeE
	.type	_ZN2v88internal8compiler22MachineOperatorReducer11ReduceStoreEPNS1_4NodeE, @function
_ZN2v88internal8compiler22MachineOperatorReducer11ReduceStoreEPNS1_4NodeE:
.LFB19922:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	(%rsi), %rdi
	cmpw	$431, 16(%rdi)
	je	.L827
	call	_ZN2v88internal8compiler30UnalignedStoreRepresentationOfEPKNS1_8OperatorE@PLT
	leaq	32(%rbx), %r14
	movzbl	(%rax), %r13d
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L716
.L831:
	leaq	48(%rbx), %rax
.L717:
	movq	(%rax), %r12
	movq	(%r12), %rdx
	movzwl	16(%rdx), %eax
	cmpw	$299, %ax
	je	.L718
	cmpw	$304, %ax
	jne	.L720
	movzbl	23(%r12), %eax
	movq	32(%r12), %rsi
	leaq	32(%r12), %rcx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L744
	movq	(%rsi), %rdi
	cmpw	$23, 16(%rdi)
	je	.L745
	xorl	%r9d, %r9d
	xorl	%r11d, %r11d
.L746:
	movq	8(%rcx), %r8
	leaq	8(%rcx), %rdi
	movq	%rsi, %r15
	movq	%rcx, %r10
	movq	(%r8), %rdi
	cmpw	$23, 16(%rdi)
	je	.L828
.L750:
	andb	18(%rdx), %r11b
	jne	.L829
	movq	(%r15), %rax
	xorl	%r9d, %r9d
	cmpw	$302, 16(%rax)
	je	.L830
	.p2align 4,,10
	.p2align 3
.L720:
	xorl	%eax, %eax
.L825:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L827:
	.cfi_restore_state
	call	_ZN2v88internal8compiler21StoreRepresentationOfEPKNS1_8OperatorE@PLT
	leaq	32(%rbx), %r14
	movzbl	(%rax), %r13d
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L831
.L716:
	movq	32(%rbx), %rax
	addq	$32, %rax
	jmp	.L717
	.p2align 4,,10
	.p2align 3
.L718:
	movzbl	23(%r12), %eax
	movq	32(%r12), %rsi
	leaq	32(%r12), %r15
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L721
	movq	(%rsi), %rcx
	cmpw	$23, 16(%rcx)
	je	.L722
	xorl	%ecx, %ecx
	xorl	%r11d, %r11d
.L723:
	leaq	8(%r15), %rdi
	movq	%rsi, %r9
	movq	%r15, %r10
.L726:
	movq	(%rdi), %r8
	movq	(%r8), %rdi
	cmpw	$23, 16(%rdi)
	jne	.L727
	movl	44(%rdi), %ecx
.L728:
	cmpb	$2, %r13b
	je	.L832
	cmpb	$3, %r13b
	jne	.L720
	movzwl	%cx, %ecx
	cmpl	$65535, %ecx
	jne	.L720
.L738:
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L739
	movq	16(%r14), %rdi
	cmpq	%r9, %rdi
	je	.L778
	addq	$16, %r14
	movq	%rbx, %rsi
.L741:
	leaq	-72(%rsi), %r12
	testq	%rdi, %rdi
	je	.L742
	movq	%r12, %rsi
	movq	%r9, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-56(%rbp), %r9
.L742:
	movq	%r9, (%r14)
	movq	%r12, %rsi
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	jmp	.L778
	.p2align 4,,10
	.p2align 3
.L744:
	movq	16(%rsi), %r15
	leaq	16(%rsi), %r10
	movq	(%r15), %rdi
	cmpw	$23, 16(%rdi)
	je	.L747
	xorl	%r9d, %r9d
	xorl	%r11d, %r11d
.L748:
	movq	8(%r10), %r8
	leaq	8(%r10), %rdi
	movq	(%r8), %rdi
	cmpw	$23, 16(%rdi)
	jne	.L750
.L828:
	movl	44(%rdi), %r9d
	movl	$1, %r11d
.L751:
	movq	(%r15), %rax
	cmpw	$302, 16(%rax)
	jne	.L720
.L830:
	cmpb	$2, %r13b
	je	.L833
	cmpb	$3, %r13b
	jne	.L720
	testb	%r11b, %r11b
	je	.L720
	leal	-1(%r9), %edx
	cmpl	$15, %edx
	ja	.L720
.L760:
	movzbl	23(%r15), %edx
	movq	32(%r15), %rcx
	leaq	32(%r15), %r12
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L761
	movq	(%rcx), %rsi
	cmpw	$23, 16(%rsi)
	je	.L762
	xorl	%r10d, %r10d
	xorl	%edi, %edi
.L763:
	leaq	8(%r12), %rsi
	movq	%rcx, %r13
	movq	%r12, %r11
.L766:
	movq	(%rsi), %r8
	movq	(%r8), %rsi
	cmpw	$23, 16(%rsi)
	jne	.L767
	movl	44(%rsi), %r10d
.L768:
	cmpl	%r10d, %r9d
	jne	.L720
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L777
	movq	16(%r14), %rdi
	cmpq	%r13, %rdi
	je	.L778
	addq	$16, %r14
	movq	%rbx, %rax
.L779:
	leaq	-72(%rax), %r12
	testq	%rdi, %rdi
	je	.L780
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L780:
	movq	%r13, (%r14)
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	.p2align 4,,10
	.p2align 3
.L778:
	movq	%rbx, %rax
	jmp	.L825
	.p2align 4,,10
	.p2align 3
.L745:
	movl	44(%rdi), %r9d
	movl	$1, %r11d
	jmp	.L746
	.p2align 4,,10
	.p2align 3
.L722:
	movl	44(%rcx), %ecx
	movl	$1, %r11d
	jmp	.L723
	.p2align 4,,10
	.p2align 3
.L727:
	testb	$1, 18(%rdx)
	je	.L720
	testb	%r11b, %r11b
	je	.L720
	cmpl	$15, %eax
	je	.L730
	movq	%r12, %rsi
	cmpq	%r9, %r8
	je	.L732
.L731:
	subq	$24, %rsi
	movq	%r9, %rdi
	movl	%ecx, -88(%rbp)
	movq	%r9, -64(%rbp)
	movq	%r8, -80(%rbp)
	movq	%r10, -72(%rbp)
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-80(%rbp), %r8
	movq	-72(%rbp), %r10
	movq	-56(%rbp), %rsi
	movq	%r8, (%r10)
	movq	%r8, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movzbl	23(%r12), %eax
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %r9
	movl	-88(%rbp), %ecx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L834
.L732:
	movq	8(%r15), %rdi
	addq	$8, %r15
	cmpq	%r9, %rdi
	je	.L781
.L735:
	subq	$48, %r12
	testq	%rdi, %rdi
	je	.L736
	movq	%r12, %rsi
	movl	%ecx, -72(%rbp)
	movq	%r9, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %r9
	movl	-72(%rbp), %ecx
.L736:
	movq	%r9, (%r15)
	movq	%r12, %rsi
	movq	%r9, %rdi
	movl	%ecx, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movq	-56(%rbp), %r8
	movl	-64(%rbp), %ecx
.L781:
	movq	%r8, %r9
	jmp	.L728
	.p2align 4,,10
	.p2align 3
.L721:
	movq	16(%rsi), %r9
	leaq	16(%rsi), %r10
	movq	(%r9), %rcx
	cmpw	$23, 16(%rcx)
	je	.L724
	xorl	%ecx, %ecx
	xorl	%r11d, %r11d
.L725:
	leaq	8(%r10), %rdi
	jmp	.L726
	.p2align 4,,10
	.p2align 3
.L832:
	movzbl	%cl, %ecx
	cmpl	$255, %ecx
	jne	.L720
	jmp	.L738
	.p2align 4,,10
	.p2align 3
.L747:
	movl	44(%rdi), %r9d
	movl	$1, %r11d
	jmp	.L748
	.p2align 4,,10
	.p2align 3
.L724:
	movl	44(%rcx), %ecx
	movl	$1, %r11d
	jmp	.L725
	.p2align 4,,10
	.p2align 3
.L833:
	testb	%r11b, %r11b
	je	.L720
	leal	-1(%r9), %edx
	cmpl	$23, %edx
	ja	.L720
	jmp	.L760
	.p2align 4,,10
	.p2align 3
.L829:
	cmpl	$15, %eax
	je	.L752
	movq	%r12, %rsi
	cmpq	%r15, %r8
	je	.L754
.L753:
	subq	$24, %rsi
	movq	%r15, %rdi
	movl	%r9d, -92(%rbp)
	movb	%r11b, -72(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%r8, -88(%rbp)
	movq	%r10, -80(%rbp)
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-88(%rbp), %r8
	movq	-80(%rbp), %r10
	movq	-56(%rbp), %rsi
	movq	%r8, (%r10)
	movq	%r8, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movzbl	23(%r12), %eax
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %rcx
	movzbl	-72(%rbp), %r11d
	andl	$15, %eax
	movl	-92(%rbp), %r9d
	cmpl	$15, %eax
	je	.L835
.L754:
	movq	8(%rcx), %rdi
	addq	$8, %rcx
	cmpq	%r15, %rdi
	je	.L787
.L756:
	subq	$48, %r12
	testq	%rdi, %rdi
	je	.L757
	movq	%r12, %rsi
	movl	%r9d, -80(%rbp)
	movq	%rcx, -72(%rbp)
	movq	%r8, -64(%rbp)
	movb	%r11b, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movzbl	-56(%rbp), %r11d
	movq	-64(%rbp), %r8
	movq	-72(%rbp), %rcx
	movl	-80(%rbp), %r9d
.L757:
	movq	%r15, (%rcx)
	movq	%r15, %rdi
	movq	%r12, %rsi
	movl	%r9d, -72(%rbp)
	movq	%r8, -64(%rbp)
	movb	%r11b, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movq	-64(%rbp), %r8
	movzbl	-56(%rbp), %r11d
	movl	-72(%rbp), %r9d
	movq	%r8, %r15
	jmp	.L751
	.p2align 4,,10
	.p2align 3
.L739:
	movq	32(%rbx), %rsi
	movq	32(%rsi), %rdi
	cmpq	%r9, %rdi
	je	.L778
	leaq	32(%rsi), %r14
	jmp	.L741
.L752:
	cmpq	%r15, %r8
	jne	.L753
	movq	%rsi, %r12
.L755:
	movq	24(%r12), %rdi
	cmpq	%r15, %rdi
	je	.L787
	leaq	24(%r12), %rcx
	jmp	.L756
.L730:
	cmpq	%r9, %r8
	jne	.L731
	movq	%rsi, %r12
.L733:
	movq	24(%r12), %rdi
	cmpq	%r9, %rdi
	je	.L781
	leaq	24(%r12), %r15
	jmp	.L735
.L787:
	movq	%r8, %r15
	jmp	.L751
.L835:
	movq	(%rcx), %r12
	jmp	.L755
.L834:
	movq	(%r15), %r12
	jmp	.L733
.L767:
	testb	$1, 18(%rax)
	je	.L720
	testb	%dil, %dil
	je	.L720
	cmpl	$15, %edx
	je	.L770
	movq	%r15, %rcx
	cmpq	%r13, %r8
	je	.L772
.L771:
	leaq	-24(%rcx), %rsi
	movq	%r13, %rdi
	movl	%r9d, -88(%rbp)
	movl	%r10d, -80(%rbp)
	movq	%r8, -72(%rbp)
	movq	%r11, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-72(%rbp), %r8
	movq	-64(%rbp), %r11
	movq	-56(%rbp), %rsi
	movq	%r8, (%r11)
	movq	%r8, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movzbl	23(%r15), %eax
	movq	-56(%rbp), %r8
	movl	-80(%rbp), %r10d
	movl	-88(%rbp), %r9d
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L773
.L772:
	movq	8(%r12), %rdi
	cmpq	%r13, %rdi
	je	.L782
	addq	$8, %r12
.L775:
	subq	$48, %r15
	testq	%rdi, %rdi
	je	.L776
	movq	%r15, %rsi
	movl	%r9d, -72(%rbp)
	movl	%r10d, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-56(%rbp), %r8
	movl	-64(%rbp), %r10d
	movl	-72(%rbp), %r9d
.L776:
	movq	%r13, (%r12)
	movq	%r15, %rsi
	movq	%r13, %rdi
	movl	%r9d, -72(%rbp)
	movl	%r10d, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movq	-56(%rbp), %r8
	movl	-64(%rbp), %r10d
	movl	-72(%rbp), %r9d
.L782:
	movq	%r8, %r13
	jmp	.L768
.L761:
	movq	16(%rcx), %r13
	leaq	16(%rcx), %r11
	movq	0(%r13), %rsi
	cmpw	$23, 16(%rsi)
	je	.L764
	xorl	%r10d, %r10d
	xorl	%edi, %edi
.L765:
	leaq	8(%r11), %rsi
	jmp	.L766
.L762:
	movl	44(%rsi), %r10d
	movl	$1, %edi
	jmp	.L763
.L764:
	movl	44(%rsi), %r10d
	movl	$1, %edi
	jmp	.L765
.L777:
	movq	32(%rbx), %rax
	movq	32(%rax), %rdi
	cmpq	%r13, %rdi
	je	.L778
	leaq	32(%rax), %r14
	jmp	.L779
.L770:
	cmpq	%r13, %r8
	jne	.L771
.L773:
	movq	(%r12), %r15
	movq	24(%r15), %rdi
	cmpq	%r13, %rdi
	je	.L782
	leaq	24(%r15), %r12
	jmp	.L775
	.cfi_endproc
.LFE19922:
	.size	_ZN2v88internal8compiler22MachineOperatorReducer11ReduceStoreEPNS1_4NodeE, .-_ZN2v88internal8compiler22MachineOperatorReducer11ReduceStoreEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler22MachineOperatorReducer16ReduceProjectionEmPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22MachineOperatorReducer16ReduceProjectionEmPNS1_4NodeE
	.type	_ZN2v88internal8compiler22MachineOperatorReducer16ReduceProjectionEmPNS1_4NodeE, @function
_ZN2v88internal8compiler22MachineOperatorReducer16ReduceProjectionEmPNS1_4NodeE:
.LFB19923:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	(%rdx), %rcx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzwl	16(%rcx), %eax
	cmpw	$309, %ax
	je	.L837
	cmpw	$311, %ax
	je	.L838
	cmpw	$307, %ax
	je	.L953
.L839:
	xorl	%eax, %eax
.L860:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L954
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L953:
	.cfi_restore_state
	movzbl	23(%rdx), %esi
	movq	32(%rdx), %rdi
	leaq	32(%rdx), %r14
	andl	$15, %esi
	cmpl	$15, %esi
	je	.L840
	movq	(%rdi), %r8
	movzwl	16(%r8), %eax
	cmpw	$23, %ax
	sete	%r10b
	je	.L841
	xorl	%r9d, %r9d
	xorl	%r11d, %r11d
.L842:
	leaq	8(%r14), %r8
	movq	%rdi, %rbx
	movq	%r14, %rax
.L845:
	movq	(%r8), %r15
	movq	(%r15), %r8
	cmpw	$23, 16(%r8)
	jne	.L846
	movl	44(%r8), %eax
	testb	%r10b, %r10b
	je	.L869
	leal	(%rax,%r9), %esi
	testq	%r12, %r12
	je	.L950
	xorl	%esi, %eax
	xorl	%r9d, %esi
	andl	%eax, %esi
	shrl	$31, %esi
	jmp	.L950
	.p2align 4,,10
	.p2align 3
.L838:
	movzbl	23(%rdx), %esi
	movq	32(%rdx), %rdi
	leaq	32(%rdx), %r14
	andl	$15, %esi
	cmpl	$15, %esi
	je	.L881
	movq	(%rdi), %r8
	movzwl	16(%r8), %eax
	cmpw	$23, %ax
	sete	%r10b
	je	.L882
	xorl	%r11d, %r11d
	xorl	%r9d, %r9d
.L883:
	leaq	8(%r14), %r8
	movq	%rdi, %r15
	movq	%r14, %rax
.L886:
	movq	(%r8), %rbx
	movq	(%rbx), %r8
	cmpw	$23, 16(%r8)
	jne	.L887
	movl	44(%r8), %esi
	testb	%r10b, %r10b
	je	.L889
	leaq	-60(%rbp), %rdx
	movl	%r9d, %edi
	call	_ZN2v84base4bits19SignedMulOverflow32EiiPi@PLT
	testq	%r12, %r12
	movzbl	%al, %esi
	cmove	-60(%rbp), %esi
.L950:
	movq	16(%r13), %rdi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	jmp	.L860
	.p2align 4,,10
	.p2align 3
.L837:
	movzbl	23(%rdx), %esi
	movq	32(%rdx), %rdi
	leaq	32(%rdx), %r14
	andl	$15, %esi
	cmpl	$15, %esi
	je	.L861
	movq	(%rdi), %r8
	movzwl	16(%r8), %eax
	cmpw	$23, %ax
	sete	%r10b
	je	.L862
	xorl	%r9d, %r9d
	xorl	%r11d, %r11d
.L863:
	leaq	8(%r14), %r8
	movq	%rdi, %rbx
	movq	%r14, %rax
.L866:
	movq	(%r8), %r15
	movq	(%r15), %r8
	cmpw	$23, 16(%r8)
	jne	.L867
	movl	44(%r8), %eax
	testb	%r10b, %r10b
	je	.L869
	movl	%r9d, %edx
	subl	%eax, %edx
	testq	%r12, %r12
	je	.L955
	xorl	%edx, %eax
	xorl	%edx, %r9d
	movl	%eax, %esi
	notl	%esi
	andl	%r9d, %esi
	shrl	$31, %esi
	jmp	.L950
	.p2align 4,,10
	.p2align 3
.L861:
	movq	16(%rdi), %rbx
	leaq	16(%rdi), %rax
	movq	(%rbx), %r9
	movzwl	16(%r9), %r8d
	cmpw	$23, %r8w
	sete	%r10b
	je	.L864
	xorl	%r9d, %r9d
	xorl	%r11d, %r11d
.L865:
	leaq	8(%rax), %r8
	jmp	.L866
	.p2align 4,,10
	.p2align 3
.L881:
	movq	16(%rdi), %r15
	leaq	16(%rdi), %rax
	movq	(%r15), %r9
	movzwl	16(%r9), %r8d
	cmpw	$23, %r8w
	sete	%r10b
	je	.L884
	xorl	%r11d, %r11d
	xorl	%r9d, %r9d
.L885:
	leaq	8(%rax), %r8
	jmp	.L886
	.p2align 4,,10
	.p2align 3
.L840:
	movq	16(%rdi), %rbx
	leaq	16(%rdi), %rax
	movq	(%rbx), %r9
	movzwl	16(%r9), %r8d
	cmpw	$23, %r8w
	sete	%r10b
	je	.L843
	xorl	%r9d, %r9d
	xorl	%r11d, %r11d
.L844:
	leaq	8(%rax), %r8
	jmp	.L845
	.p2align 4,,10
	.p2align 3
.L846:
	testb	$1, 18(%rcx)
	je	.L839
	testb	%r11b, %r11b
	je	.L839
	cmpl	$15, %esi
	je	.L850
	movq	%rdx, %rdi
	cmpq	%rbx, %r15
	je	.L852
.L851:
	leaq	-24(%rdi), %r13
	movq	%rbx, %rdi
	movq	%rdx, -88(%rbp)
	movq	%r13, %rsi
	movl	%r9d, -80(%rbp)
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-72(%rbp), %rax
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%r15, (%rax)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movq	-88(%rbp), %rdx
	movl	-80(%rbp), %r9d
	movzbl	23(%rdx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L956
.L852:
	movq	8(%r14), %rdi
	addq	$8, %r14
	cmpq	%rbx, %rdi
	je	.L902
.L855:
	leaq	-48(%rdx), %r13
	testq	%rdi, %rdi
	je	.L856
	movq	%r13, %rsi
	movl	%r9d, -72(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movl	-72(%rbp), %r9d
.L856:
	movq	%rbx, (%r14)
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movl	%r9d, -72(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movl	-72(%rbp), %r9d
.L902:
	movq	%rbx, %rax
	movq	%r15, %rbx
	movq	%rax, %r15
	movl	%r9d, %eax
	.p2align 4,,10
	.p2align 3
.L869:
	testl	%eax, %eax
	jne	.L839
	testq	%r12, %r12
	movq	%r15, %rax
	cmove	%rbx, %rax
	jmp	.L860
	.p2align 4,,10
	.p2align 3
.L887:
	testb	$1, 18(%rcx)
	je	.L839
	testb	%r11b, %r11b
	je	.L839
	cmpl	$15, %esi
	je	.L891
	movq	%rdx, %rdi
	cmpq	%r15, %rbx
	je	.L893
.L892:
	leaq	-24(%rdi), %rsi
	movq	%r15, %rdi
	movq	%rdx, -96(%rbp)
	movl	%r9d, -88(%rbp)
	movq	%rax, -80(%rbp)
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-80(%rbp), %rax
	movq	-72(%rbp), %rsi
	movq	%rbx, %rdi
	movq	%rbx, (%rax)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movq	-96(%rbp), %rdx
	movl	-88(%rbp), %r9d
	movzbl	23(%rdx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L957
.L893:
	movq	8(%r14), %rdi
	leaq	8(%r14), %rax
	cmpq	%r15, %rdi
	je	.L904
.L896:
	leaq	-48(%rdx), %r14
	testq	%rdi, %rdi
	je	.L897
	movq	%r14, %rsi
	movl	%r9d, -80(%rbp)
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-72(%rbp), %rax
	movl	-80(%rbp), %r9d
.L897:
	movq	%r15, (%rax)
	movq	%r14, %rsi
	movq	%r15, %rdi
	movl	%r9d, -72(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movl	-72(%rbp), %r9d
.L904:
	movq	%r15, %rax
	movl	%r9d, %esi
	movq	%rbx, %r15
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L889:
	movq	%rbx, %rax
	testl	%esi, %esi
	je	.L860
	cmpl	$1, %esi
	jne	.L839
	movq	%r15, %rax
	testq	%r12, %r12
	je	.L860
	movq	16(%r13), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	jmp	.L860
	.p2align 4,,10
	.p2align 3
.L867:
	testb	$1, 18(%rcx)
	je	.L839
	testb	%r11b, %r11b
	je	.L839
	cmpl	$15, %esi
	je	.L871
	movq	%rdx, %rdi
	cmpq	%rbx, %r15
	je	.L873
.L872:
	leaq	-24(%rdi), %r13
	movq	%rbx, %rdi
	movq	%rdx, -88(%rbp)
	movq	%r13, %rsi
	movl	%r9d, -80(%rbp)
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-72(%rbp), %rax
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%r15, (%rax)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movq	-88(%rbp), %rdx
	movl	-80(%rbp), %r9d
	movzbl	23(%rdx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L958
.L873:
	movq	8(%r14), %rdi
	addq	$8, %r14
	cmpq	%rbx, %rdi
	je	.L903
.L876:
	leaq	-48(%rdx), %r13
	testq	%rdi, %rdi
	je	.L877
	movq	%r13, %rsi
	movl	%r9d, -72(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movl	-72(%rbp), %r9d
.L877:
	movq	%rbx, (%r14)
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movl	%r9d, -72(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movl	-72(%rbp), %r9d
.L903:
	movq	%rbx, %rdx
	movl	%r9d, %eax
	movq	%r15, %rbx
	movq	%rdx, %r15
	jmp	.L869
	.p2align 4,,10
	.p2align 3
.L841:
	movl	44(%r8), %r9d
	movl	$1, %r11d
	jmp	.L842
	.p2align 4,,10
	.p2align 3
.L882:
	movl	44(%r8), %r9d
	movl	$1, %r11d
	jmp	.L883
	.p2align 4,,10
	.p2align 3
.L862:
	movl	44(%r8), %r9d
	movl	$1, %r11d
	jmp	.L863
	.p2align 4,,10
	.p2align 3
.L843:
	movl	44(%r9), %r9d
	movl	$1, %r11d
	jmp	.L844
	.p2align 4,,10
	.p2align 3
.L884:
	movl	44(%r9), %r9d
	movl	$1, %r11d
	jmp	.L885
	.p2align 4,,10
	.p2align 3
.L864:
	movl	44(%r9), %r9d
	movl	$1, %r11d
	jmp	.L865
	.p2align 4,,10
	.p2align 3
.L955:
	movl	%edx, %esi
	jmp	.L950
.L850:
	cmpq	%rbx, %r15
	jne	.L851
	movq	%rdi, %rdx
.L853:
	movq	24(%rdx), %rdi
	cmpq	%rbx, %rdi
	je	.L902
	leaq	24(%rdx), %r14
	jmp	.L855
.L871:
	cmpq	%rbx, %r15
	jne	.L872
	movq	%rdi, %rdx
.L874:
	movq	24(%rdx), %rdi
	cmpq	%rbx, %rdi
	je	.L903
	leaq	24(%rdx), %r14
	jmp	.L876
.L891:
	cmpq	%r15, %rbx
	jne	.L892
	movq	%rdi, %rdx
.L894:
	movq	24(%rdx), %rdi
	cmpq	%r15, %rdi
	je	.L904
	leaq	24(%rdx), %rax
	jmp	.L896
.L956:
	movq	(%r14), %rdx
	jmp	.L853
.L957:
	movq	(%r14), %rdx
	jmp	.L894
.L958:
	movq	(%r14), %rdx
	jmp	.L874
.L954:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19923:
	.size	_ZN2v88internal8compiler22MachineOperatorReducer16ReduceProjectionEmPNS1_4NodeE, .-_ZN2v88internal8compiler22MachineOperatorReducer16ReduceProjectionEmPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler22MachineOperatorReducer18ReduceWord32ShiftsEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22MachineOperatorReducer18ReduceWord32ShiftsEPNS1_4NodeE
	.type	_ZN2v88internal8compiler22MachineOperatorReducer18ReduceWord32ShiftsEPNS1_4NodeE, @function
_ZN2v88internal8compiler22MachineOperatorReducer18ReduceWord32ShiftsEPNS1_4NodeE:
.LFB19924:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	16(%rax), %rax
	testb	$8, 21(%rax)
	jne	.L1023
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1023:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	32(%rsi), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %rsi
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L961
	movq	(%rsi), %rdx
	movq	%rsi, %r14
	movq	%r13, %r15
	cmpw	$23, 16(%rdx)
	leaq	40(%rbx), %rdx
	sete	%r8b
.L962:
	movq	(%rdx), %r12
	movq	(%rbx), %rdi
	movq	(%r12), %rcx
	movzwl	16(%rcx), %edx
	testb	%r8b, 18(%rdi)
	je	.L964
	cmpw	$23, %dx
	jne	.L1024
.L964:
	cmpw	$299, %dx
	je	.L1025
.L960:
	xorl	%eax, %eax
.L1017:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1025:
	.cfi_restore_state
	movzbl	23(%r12), %eax
	movq	32(%r12), %rdx
	leaq	32(%r12), %r14
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L974
	movq	(%rdx), %rsi
	cmpw	$23, 16(%rsi)
	je	.L975
	xorl	%edi, %edi
	xorl	%r9d, %r9d
.L976:
	leaq	8(%r14), %rsi
	movq	%rdx, %r8
	movq	%r14, %r10
.L979:
	movq	(%rsi), %r15
	movq	(%r15), %rsi
	cmpw	$23, 16(%rsi)
	jne	.L980
	movl	44(%rsi), %r9d
.L981:
	cmpl	$31, %r9d
	jne	.L960
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L990
	movq	8(%r13), %rdi
	cmpq	%r8, %rdi
	je	.L991
	addq	$8, %r13
	movq	%rbx, %rax
.L992:
	leaq	-48(%rax), %r12
	testq	%rdi, %rdi
	je	.L993
	movq	%r12, %rsi
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-56(%rbp), %r8
.L993:
	movq	%r8, 0(%r13)
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L991:
	movq	%rbx, %rax
	jmp	.L1017
	.p2align 4,,10
	.p2align 3
.L961:
	movq	16(%rsi), %r14
	leaq	16(%rsi), %r15
	movq	(%r14), %rdx
	cmpw	$23, 16(%rdx)
	leaq	24(%rsi), %rdx
	sete	%r8b
	jmp	.L962
	.p2align 4,,10
	.p2align 3
.L1024:
	cmpl	$15, %eax
	je	.L965
	movq	%rbx, %rsi
	cmpq	%r14, %r12
	je	.L967
.L966:
	subq	$24, %rsi
	movq	%r14, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	%r12, (%r15)
	movq	-56(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1026
.L967:
	movq	8(%r13), %rdi
	leaq	8(%r13), %r15
	movq	%rbx, %rsi
	cmpq	%r14, %rdi
	je	.L1022
.L971:
	leaq	-48(%rsi), %r12
	testq	%rdi, %rdi
	je	.L972
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L972:
	movq	%r14, (%r15)
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L1022:
	movq	(%r14), %rcx
	movq	%r14, %r12
	movzwl	16(%rcx), %edx
	jmp	.L964
	.p2align 4,,10
	.p2align 3
.L974:
	movq	16(%rdx), %r8
	leaq	16(%rdx), %r10
	movq	(%r8), %rsi
	cmpw	$23, 16(%rsi)
	je	.L977
	xorl	%edi, %edi
	xorl	%r9d, %r9d
.L978:
	leaq	8(%r10), %rsi
	jmp	.L979
	.p2align 4,,10
	.p2align 3
.L980:
	testb	$1, 18(%rcx)
	je	.L960
	testb	%dil, %dil
	je	.L960
	cmpl	$15, %eax
	je	.L983
	movq	%r12, %rdx
	cmpq	%r8, %r15
	je	.L985
.L984:
	leaq	-24(%rdx), %rsi
	movq	%r8, %rdi
	movl	%r9d, -76(%rbp)
	movq	%r8, -56(%rbp)
	movq	%r10, -72(%rbp)
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-72(%rbp), %r10
	movq	-64(%rbp), %rsi
	movq	%r15, %rdi
	movq	%r15, (%r10)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movzbl	23(%r12), %eax
	movq	-56(%rbp), %r8
	movl	-76(%rbp), %r9d
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L986
.L985:
	movq	8(%r14), %rdi
	addq	$8, %r14
	cmpq	%r8, %rdi
	je	.L995
.L988:
	subq	$48, %r12
	testq	%rdi, %rdi
	je	.L989
	movq	%r12, %rsi
	movl	%r9d, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-56(%rbp), %r8
	movl	-64(%rbp), %r9d
.L989:
	movq	%r8, (%r14)
	movq	%r12, %rsi
	movq	%r8, %rdi
	movl	%r9d, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movl	-56(%rbp), %r9d
.L995:
	movq	%r15, %r8
	jmp	.L981
	.p2align 4,,10
	.p2align 3
.L975:
	movl	44(%rsi), %r9d
	movl	$1, %edi
	jmp	.L976
	.p2align 4,,10
	.p2align 3
.L977:
	movl	44(%rsi), %r9d
	movl	$1, %edi
	jmp	.L978
.L965:
	cmpq	%r14, %r12
	jne	.L966
.L968:
	movq	24(%rsi), %rdi
	cmpq	%r14, %rdi
	je	.L1022
	leaq	24(%rsi), %r15
	jmp	.L971
.L1026:
	movq	32(%rbx), %rsi
	jmp	.L968
.L990:
	movq	32(%rbx), %rax
	movq	24(%rax), %rdi
	cmpq	%r8, %rdi
	je	.L991
	leaq	24(%rax), %r13
	jmp	.L992
.L983:
	cmpq	%r8, %r15
	jne	.L984
.L986:
	movq	(%r14), %r12
	movq	24(%r12), %rdi
	cmpq	%r8, %rdi
	je	.L995
	leaq	24(%r12), %r14
	jmp	.L988
	.cfi_endproc
.LFE19924:
	.size	_ZN2v88internal8compiler22MachineOperatorReducer18ReduceWord32ShiftsEPNS1_4NodeE, .-_ZN2v88internal8compiler22MachineOperatorReducer18ReduceWord32ShiftsEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler22MachineOperatorReducer15ReduceWord32ShrEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22MachineOperatorReducer15ReduceWord32ShrEPNS1_4NodeE
	.type	_ZN2v88internal8compiler22MachineOperatorReducer15ReduceWord32ShrEPNS1_4NodeE, @function
_ZN2v88internal8compiler22MachineOperatorReducer15ReduceWord32ShrEPNS1_4NodeE:
.LFB19927:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	32(%rsi), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %rsi
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1028
	movq	(%rsi), %rcx
	movzwl	16(%rcx), %edx
	cmpw	$23, %dx
	sete	%dil
	je	.L1029
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
.L1030:
	leaq	8(%r13), %rdx
	movq	%rsi, %r14
	movq	%r13, %rcx
.L1033:
	movq	(%rdx), %r15
	movq	(%r15), %rdx
	cmpw	$23, 16(%rdx)
	jne	.L1034
	movl	44(%rdx), %ecx
	testl	%ecx, %ecx
	je	.L1035
	testb	%dil, %dil
	je	.L1103
	movl	%r8d, %esi
	movq	16(%r12), %rdi
	shrl	%cl, %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
.L1151:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1029:
	.cfi_restore_state
	movl	44(%rcx), %r8d
	movl	$1, %r9d
	jmp	.L1030
	.p2align 4,,10
	.p2align 3
.L1034:
	movq	(%rbx), %rdx
	testb	$1, 18(%rdx)
	je	.L1056
	testb	%r9b, %r9b
	jne	.L1154
.L1056:
	movq	16(%r12), %rdi
.L1045:
	movq	16(%rdi), %rax
	testb	$8, 21(%rax)
	jne	.L1155
.L1065:
	xorl	%ebx, %ebx
.L1099:
	addq	$56, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1028:
	.cfi_restore_state
	movq	16(%rsi), %r14
	leaq	16(%rsi), %rcx
	movq	(%r14), %r8
	movzwl	16(%r8), %edx
	cmpw	$23, %dx
	sete	%dil
	je	.L1031
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
.L1032:
	leaq	8(%rcx), %rdx
	jmp	.L1033
	.p2align 4,,10
	.p2align 3
.L1155:
	movzbl	23(%rbx), %eax
	movq	0(%r13), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1066
	movq	(%rdx), %rcx
	movq	%rdx, %r14
	movq	%r13, %r15
	cmpw	$23, 16(%rcx)
	leaq	8(%r13), %rcx
	sete	%r8b
.L1067:
	movq	(%rcx), %r12
	movq	(%rbx), %rdi
	movq	(%r12), %rsi
	movzwl	16(%rsi), %ecx
	testb	%r8b, 18(%rdi)
	je	.L1069
	cmpw	$23, %cx
	jne	.L1156
.L1069:
	cmpw	$299, %cx
	jne	.L1065
	movzbl	23(%r12), %eax
	movq	32(%r12), %rdx
	leaq	32(%r12), %r15
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1079
	movq	(%rdx), %rcx
	cmpw	$23, 16(%rcx)
	je	.L1080
	xorl	%r9d, %r9d
	xorl	%edi, %edi
.L1081:
	leaq	8(%r15), %rcx
	movq	%rdx, %r8
	movq	%r15, %r10
.L1084:
	movq	(%rcx), %r14
	movq	(%r14), %rcx
	cmpw	$23, 16(%rcx)
	jne	.L1085
	movl	44(%rcx), %r9d
.L1086:
	cmpl	$31, %r9d
	jne	.L1065
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1095
	movq	8(%r13), %rdi
	cmpq	%r8, %rdi
	je	.L1099
	addq	$8, %r13
	movq	%rbx, %rax
.L1097:
	leaq	-48(%rax), %r12
	testq	%rdi, %rdi
	je	.L1098
	movq	%r12, %rsi
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-56(%rbp), %r8
.L1098:
	movq	%r8, 0(%r13)
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	jmp	.L1099
	.p2align 4,,10
	.p2align 3
.L1031:
	movl	44(%r8), %r8d
	movl	$1, %r9d
	jmp	.L1032
	.p2align 4,,10
	.p2align 3
.L1038:
	cmpq	%r14, %r15
	je	.L1041
.L1039:
	subq	$24, %rsi
	movq	%r14, %rdi
	movl	%r8d, -72(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-64(%rbp), %rcx
	movq	-56(%rbp), %rsi
	movq	%r15, %rdi
	movq	%r15, (%rcx)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movzbl	23(%rbx), %eax
	movl	-72(%rbp), %r8d
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1157
.L1040:
	movq	8(%r13), %rdi
	cmpq	%r14, %rdi
	je	.L1104
	leaq	8(%r13), %rax
	movq	%rbx, %rsi
.L1043:
	subq	$48, %rsi
	testq	%rdi, %rdi
	je	.L1044
	movl	%r8d, -72(%rbp)
	movq	%rax, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %rax
	movl	-72(%rbp), %r8d
.L1044:
	movq	%r14, (%rax)
	movq	%r14, %rdi
	movl	%r8d, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movl	-56(%rbp), %r8d
.L1104:
	movq	%r15, %r14
	testl	%r8d, %r8d
	jne	.L1158
.L1035:
	addq	$56, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1158:
	.cfi_restore_state
	movl	%r8d, %ecx
.L1103:
	movq	(%r14), %rsi
	cmpw	$299, 16(%rsi)
	jne	.L1056
	movzbl	23(%r14), %eax
	movq	32(%r14), %rdx
	leaq	32(%r14), %r8
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1048
	movq	(%rdx), %rdi
	cmpw	$23, 16(%rdi)
	je	.L1049
	movb	$0, -56(%rbp)
	xorl	%r15d, %r15d
.L1050:
	leaq	8(%r8), %rdi
	movq	%rdx, %r10
	movq	%r8, %r11
.L1053:
	movq	(%rdi), %r9
	movq	(%r9), %rdi
	cmpw	$23, 16(%rdi)
	jne	.L1054
	movl	44(%rdi), %r15d
	movq	16(%r12), %rdi
.L1055:
	shrl	%cl, %r15d
	testl	%r15d, %r15d
	jne	.L1045
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	jmp	.L1151
	.p2align 4,,10
	.p2align 3
.L1154:
	cmpl	$15, %eax
	je	.L1038
	movq	%rbx, %rsi
	cmpq	%r14, %r15
	jne	.L1039
	jmp	.L1040
	.p2align 4,,10
	.p2align 3
.L1066:
	movq	16(%rdx), %r14
	leaq	16(%rdx), %r15
	movq	(%r14), %rcx
	cmpw	$23, 16(%rcx)
	leaq	24(%rdx), %rcx
	sete	%r8b
	jmp	.L1067
	.p2align 4,,10
	.p2align 3
.L1049:
	movb	$1, -56(%rbp)
	movl	44(%rdi), %r15d
	jmp	.L1050
	.p2align 4,,10
	.p2align 3
.L1157:
	movq	0(%r13), %rsi
.L1041:
	movq	24(%rsi), %rdi
	cmpq	%r14, %rdi
	je	.L1104
	leaq	24(%rsi), %rax
	jmp	.L1043
	.p2align 4,,10
	.p2align 3
.L1054:
	testb	$1, 18(%rsi)
	je	.L1056
	cmpb	$0, -56(%rbp)
	je	.L1056
	cmpl	$15, %eax
	je	.L1057
	movq	%r14, %rdx
	cmpq	%r10, %r9
	je	.L1059
.L1058:
	leaq	-24(%rdx), %rsi
	movq	%r10, %rdi
	movq	%r8, -96(%rbp)
	movl	%ecx, -72(%rbp)
	movq	%r10, -56(%rbp)
	movq	%r9, -88(%rbp)
	movq	%r11, -80(%rbp)
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-88(%rbp), %r9
	movq	-80(%rbp), %r11
	movq	-64(%rbp), %rsi
	movq	%r9, (%r11)
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movzbl	23(%r14), %eax
	movq	-56(%rbp), %r10
	movl	-72(%rbp), %ecx
	movq	-96(%rbp), %r8
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1060
.L1059:
	movq	8(%r8), %rdi
	addq	$8, %r8
	cmpq	%r10, %rdi
	je	.L1101
.L1062:
	subq	$48, %r14
	testq	%rdi, %rdi
	je	.L1063
	movq	%r14, %rsi
	movq	%r10, -72(%rbp)
	movq	%r8, -64(%rbp)
	movl	%ecx, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movl	-56(%rbp), %ecx
	movq	-64(%rbp), %r8
	movq	-72(%rbp), %r10
.L1063:
	movq	%r10, (%r8)
	movq	%r14, %rsi
	movq	%r10, %rdi
	movl	%ecx, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movl	-56(%rbp), %ecx
.L1101:
	movq	16(%r12), %rdi
	jmp	.L1055
	.p2align 4,,10
	.p2align 3
.L1048:
	movq	16(%rdx), %r10
	leaq	16(%rdx), %r11
	movq	(%r10), %rdi
	cmpw	$23, 16(%rdi)
	je	.L1051
	movb	$0, -56(%rbp)
	xorl	%r15d, %r15d
.L1052:
	leaq	8(%r11), %rdi
	jmp	.L1053
	.p2align 4,,10
	.p2align 3
.L1085:
	testb	$1, 18(%rsi)
	je	.L1065
	testb	%dil, %dil
	je	.L1065
	cmpl	$15, %eax
	je	.L1088
	movq	%r12, %rdx
	cmpq	%r8, %r14
	je	.L1090
.L1089:
	leaq	-24(%rdx), %rsi
	movq	%r8, %rdi
	movl	%r9d, -80(%rbp)
	movq	%r8, -56(%rbp)
	movq	%r10, -72(%rbp)
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-72(%rbp), %r10
	movq	-64(%rbp), %rsi
	movq	%r14, %rdi
	movq	%r14, (%r10)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movzbl	23(%r12), %eax
	movq	-56(%rbp), %r8
	movl	-80(%rbp), %r9d
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1091
.L1090:
	movq	8(%r15), %rdi
	addq	$8, %r15
	cmpq	%r8, %rdi
	je	.L1100
.L1093:
	subq	$48, %r12
	testq	%rdi, %rdi
	je	.L1094
	movq	%r12, %rsi
	movl	%r9d, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-56(%rbp), %r8
	movl	-64(%rbp), %r9d
.L1094:
	movq	%r8, (%r15)
	movq	%r12, %rsi
	movq	%r8, %rdi
	movl	%r9d, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movl	-56(%rbp), %r9d
.L1100:
	movq	%r14, %r8
	jmp	.L1086
	.p2align 4,,10
	.p2align 3
.L1051:
	movb	$1, -56(%rbp)
	movl	44(%rdi), %r15d
	jmp	.L1052
	.p2align 4,,10
	.p2align 3
.L1156:
	cmpl	$15, %eax
	je	.L1070
	movq	%rbx, %rdx
	cmpq	%r14, %r12
	je	.L1072
.L1071:
	leaq	-24(%rdx), %rsi
	movq	%r14, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	%r12, (%r15)
	movq	-56(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1159
.L1072:
	movq	8(%r13), %rdi
	leaq	8(%r13), %r15
	movq	%rbx, %rdx
	cmpq	%r14, %rdi
	je	.L1153
.L1076:
	leaq	-48(%rdx), %r12
	testq	%rdi, %rdi
	je	.L1077
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L1077:
	movq	%r14, (%r15)
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L1153:
	movq	(%r14), %rsi
	movq	%r14, %r12
	movzwl	16(%rsi), %ecx
	jmp	.L1069
	.p2align 4,,10
	.p2align 3
.L1079:
	movq	16(%rdx), %r8
	leaq	16(%rdx), %r10
	movq	(%r8), %rcx
	cmpw	$23, 16(%rcx)
	je	.L1082
	xorl	%r9d, %r9d
	xorl	%edi, %edi
.L1083:
	leaq	8(%r10), %rcx
	jmp	.L1084
	.p2align 4,,10
	.p2align 3
.L1080:
	movl	44(%rcx), %r9d
	movl	$1, %edi
	jmp	.L1081
.L1082:
	movl	44(%rcx), %r9d
	movl	$1, %edi
	jmp	.L1083
.L1070:
	cmpq	%r14, %r12
	jne	.L1071
.L1073:
	movq	24(%rdx), %rdi
	cmpq	%r14, %rdi
	je	.L1153
	leaq	24(%rdx), %r15
	jmp	.L1076
.L1159:
	movq	0(%r13), %rdx
	jmp	.L1073
.L1057:
	cmpq	%r10, %r9
	jne	.L1058
.L1060:
	movq	(%r8), %r14
	movq	24(%r14), %rdi
	cmpq	%r10, %rdi
	je	.L1101
	leaq	24(%r14), %r8
	jmp	.L1062
.L1095:
	movq	0(%r13), %rax
	movq	24(%rax), %rdi
	cmpq	%r8, %rdi
	je	.L1099
	leaq	24(%rax), %r13
	jmp	.L1097
.L1088:
	cmpq	%r8, %r14
	jne	.L1089
.L1091:
	movq	(%r15), %r12
	movq	24(%r12), %rdi
	cmpq	%r8, %rdi
	je	.L1100
	leaq	24(%r12), %r15
	jmp	.L1093
	.cfi_endproc
.LFE19927:
	.size	_ZN2v88internal8compiler22MachineOperatorReducer15ReduceWord32ShrEPNS1_4NodeE, .-_ZN2v88internal8compiler22MachineOperatorReducer15ReduceWord32ShrEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler22MachineOperatorReducer15ReduceWord32SarEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22MachineOperatorReducer15ReduceWord32SarEPNS1_4NodeE
	.type	_ZN2v88internal8compiler22MachineOperatorReducer15ReduceWord32SarEPNS1_4NodeE, @function
_ZN2v88internal8compiler22MachineOperatorReducer15ReduceWord32SarEPNS1_4NodeE:
.LFB19929:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	32(%rsi), %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$104, %rsp
	movzbl	23(%rsi), %edx
	movq	32(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L1161
	movq	(%rsi), %rcx
	movzwl	16(%rcx), %eax
	cmpw	$23, %ax
	sete	%dil
	je	.L1162
	xorl	%r9d, %r9d
	xorl	%r14d, %r14d
.L1163:
	leaq	8(%r15), %rax
	movq	%rsi, %r13
	movq	%r15, %r10
.L1166:
	movq	(%rax), %r8
	movq	(%r8), %rax
	cmpw	$23, 16(%rax)
	jne	.L1167
	movl	44(%rax), %ecx
	testl	%ecx, %ecx
	je	.L1168
	movl	$1, %r14d
	testb	%dil, %dil
	je	.L1170
	movl	%r9d, %esi
	movq	16(%r12), %rdi
	sarl	%cl, %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
.L1178:
	movq	-56(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L1345
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1162:
	.cfi_restore_state
	movl	44(%rcx), %r9d
	movl	$1, %r14d
	jmp	.L1163
	.p2align 4,,10
	.p2align 3
.L1171:
	cmpq	%r13, %r8
	je	.L1174
.L1172:
	subq	$24, %rsi
	movq	%r13, %rdi
	movq	%r10, -128(%rbp)
	movl	%r9d, -140(%rbp)
	movq	%r8, -136(%rbp)
	movq	%rsi, -120(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-136(%rbp), %r8
	movq	-128(%rbp), %r10
	movq	-120(%rbp), %rsi
	movq	%r8, (%r10)
	movq	%r8, %rdi
	movq	%r8, -120(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movzbl	23(%rbx), %eax
	movq	-120(%rbp), %r8
	movl	-140(%rbp), %r9d
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1346
.L1173:
	movq	8(%r15), %rdi
	cmpq	%r13, %rdi
	je	.L1258
	leaq	8(%r15), %rax
	movq	%rbx, %rsi
.L1176:
	subq	$48, %rsi
	testq	%rdi, %rdi
	je	.L1177
	movl	%r9d, -140(%rbp)
	movq	%rax, -136(%rbp)
	movq	%r8, -128(%rbp)
	movq	%rsi, -120(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-120(%rbp), %rsi
	movq	-128(%rbp), %r8
	movq	-136(%rbp), %rax
	movl	-140(%rbp), %r9d
.L1177:
	movq	%r13, (%rax)
	movq	%r13, %rdi
	movl	%r9d, -128(%rbp)
	movq	%r8, -120(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movq	-120(%rbp), %r8
	movl	-128(%rbp), %r9d
.L1258:
	testl	%r9d, %r9d
	je	.L1270
	movl	%r9d, %ecx
	movq	%r8, %r13
	.p2align 4,,10
	.p2align 3
.L1170:
	movq	0(%r13), %rax
	cmpw	$302, 16(%rax)
	je	.L1347
.L1179:
	movq	16(%r12), %rdi
.L1218:
	movq	16(%rdi), %rax
	testb	$8, 21(%rax)
	jne	.L1348
.L1219:
	xorl	%ebx, %ebx
.L1253:
	movq	%rbx, %rax
	jmp	.L1178
	.p2align 4,,10
	.p2align 3
.L1348:
	movzbl	23(%rbx), %eax
	movq	(%r15), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1220
	movq	(%rdx), %rcx
	movq	%rdx, %r13
	movq	%r15, %r14
	cmpw	$23, 16(%rcx)
	leaq	8(%r15), %rcx
	sete	%r8b
.L1221:
	movq	(%rcx), %r12
	movq	(%rbx), %rdi
	movq	(%r12), %rsi
	movzwl	16(%rsi), %ecx
	testb	%r8b, 18(%rdi)
	je	.L1223
	cmpw	$23, %cx
	jne	.L1349
.L1223:
	cmpw	$299, %cx
	jne	.L1219
	movzbl	23(%r12), %eax
	movq	32(%r12), %rcx
	leaq	32(%r12), %r13
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1233
	movq	(%rcx), %rdx
	cmpw	$23, 16(%rdx)
	je	.L1234
	xorl	%edi, %edi
	xorl	%r8d, %r8d
.L1235:
	leaq	8(%r13), %rdx
	movq	%rcx, %r14
	movq	%r13, %r9
.L1238:
	movq	(%rdx), %r10
	movq	(%r10), %rdx
	cmpw	$23, 16(%rdx)
	jne	.L1239
	movl	44(%rdx), %r8d
.L1240:
	cmpl	$31, %r8d
	jne	.L1219
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1249
	movq	8(%r15), %rdi
	cmpq	%r14, %rdi
	je	.L1253
	addq	$8, %r15
	movq	%rbx, %rax
.L1251:
	leaq	-48(%rax), %r12
	testq	%rdi, %rdi
	je	.L1252
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L1252:
	movq	%r14, (%r15)
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	jmp	.L1253
	.p2align 4,,10
	.p2align 3
.L1167:
	movq	(%rbx), %rax
	xorl	%ecx, %ecx
	andb	18(%rax), %r14b
	je	.L1170
	cmpl	$15, %edx
	je	.L1171
	movq	%rbx, %rsi
	cmpq	%r13, %r8
	jne	.L1172
	jmp	.L1173
	.p2align 4,,10
	.p2align 3
.L1161:
	movq	16(%rsi), %r13
	leaq	16(%rsi), %r10
	movq	0(%r13), %rcx
	movzwl	16(%rcx), %eax
	cmpw	$23, %ax
	sete	%dil
	je	.L1164
	xorl	%r9d, %r9d
	xorl	%r14d, %r14d
.L1165:
	leaq	8(%r10), %rax
	jmp	.L1166
	.p2align 4,,10
	.p2align 3
.L1347:
	movzbl	23(%r13), %eax
	leaq	32(%r13), %rdx
	movq	%r13, -96(%rbp)
	movq	%rdx, %rsi
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1350
.L1180:
	movq	(%rsi), %rax
	movl	$0, -80(%rbp)
	movq	%rax, -88(%rbp)
	movq	(%rax), %rdi
	movzwl	16(%rdi), %eax
	cmpw	$23, %ax
	sete	%sil
	movb	%sil, -76(%rbp)
	jne	.L1181
	movl	44(%rdi), %eax
	movl	%eax, -80(%rbp)
.L1181:
	movzbl	23(%r13), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1182
	leaq	8(%rdx), %rax
.L1183:
	movq	(%rax), %r8
	movl	$0, -64(%rbp)
	movq	%r8, -72(%rbp)
	movq	(%r8), %rdi
	movzwl	16(%rdi), %eax
	cmpw	$23, %ax
	sete	%r9b
	movb	%r9b, -60(%rbp)
	jne	.L1184
	movl	44(%rdi), %eax
	movl	%eax, -64(%rbp)
.L1185:
	leaq	-88(%rbp), %rdi
	movl	%ecx, -120(%rbp)
	call	_ZNK2v88internal8compiler11NodeMatcher12IsComparisonEv@PLT
	movl	-120(%rbp), %ecx
	testb	%al, %al
	je	.L1196
	movq	16(%r12), %rdi
	testb	%r14b, %r14b
	je	.L1218
	cmpl	$31, %ecx
	jne	.L1218
	cmpb	$0, -60(%rbp)
	je	.L1218
	cmpl	$31, -64(%rbp)
	jne	.L1218
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%rax, %r13
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1198
	movq	(%r15), %rdi
	movq	%r15, %rcx
	movq	%rbx, %rdx
	cmpq	%rdi, %r13
	je	.L1351
.L1199:
	leaq	-24(%rdx), %r14
	testq	%rdi, %rdi
	je	.L1202
	movq	%r14, %rsi
	movq	%rcx, -120(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-120(%rbp), %rcx
.L1202:
	movq	%r13, (%rcx)
	testq	%r13, %r13
	je	.L1203
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L1203:
	movzbl	23(%rbx), %eax
	movq	-88(%rbp), %r14
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1352
.L1259:
	movq	8(%r15), %rdi
	cmpq	%r14, %rdi
	je	.L1207
	addq	$8, %r15
	movq	%rbx, %rax
.L1206:
	leaq	-48(%rax), %r13
	testq	%rdi, %rdi
	je	.L1208
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L1208:
	movq	%r14, (%r15)
	testq	%r14, %r14
	je	.L1207
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L1207:
	movq	16(%r12), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Int32SubEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22MachineOperatorReducer14ReduceInt32SubEPNS1_4NodeE
	testq	%rax, %rax
	cmove	%rbx, %rax
	jmp	.L1178
	.p2align 4,,10
	.p2align 3
.L1164:
	movl	44(%rcx), %r9d
	movl	$1, %r14d
	jmp	.L1165
	.p2align 4,,10
	.p2align 3
.L1182:
	movq	(%rdx), %rax
	addq	$24, %rax
	jmp	.L1183
	.p2align 4,,10
	.p2align 3
.L1350:
	movq	32(%r13), %rax
	leaq	16(%rax), %rsi
	jmp	.L1180
	.p2align 4,,10
	.p2align 3
.L1220:
	movq	16(%rdx), %r13
	leaq	16(%rdx), %r14
	movq	0(%r13), %rcx
	cmpw	$23, 16(%rcx)
	leaq	24(%rdx), %rcx
	sete	%r8b
	jmp	.L1221
.L1270:
	movq	%r8, %r13
	.p2align 4,,10
	.p2align 3
.L1168:
	movq	%r13, %rax
	jmp	.L1178
	.p2align 4,,10
	.p2align 3
.L1196:
	movq	-88(%rbp), %rax
	movq	(%rax), %rdi
	cmpw	$429, 16(%rdi)
	jne	.L1179
	movl	%ecx, -120(%rbp)
	call	_ZN2v88internal8compiler20LoadRepresentationOfEPKNS1_8OperatorE@PLT
	testb	%r14b, %r14b
	movl	-120(%rbp), %ecx
	je	.L1179
	movl	%eax, %edx
	movzbl	%ah, %eax
	cmpl	$24, %ecx
	je	.L1353
	cmpl	$16, %ecx
	jne	.L1179
	cmpb	$0, -60(%rbp)
	je	.L1179
	cmpl	$16, -64(%rbp)
	sete	%cl
	cmpb	$3, %dl
	sete	%dl
	testb	%dl, %cl
	je	.L1179
.L1342:
	cmpb	$2, %al
	jne	.L1179
	movq	-88(%rbp), %rax
	jmp	.L1178
	.p2align 4,,10
	.p2align 3
.L1184:
	movq	0(%r13), %rax
	testb	$1, 18(%rax)
	je	.L1185
	testb	%sil, %sil
	je	.L1185
	movdqu	-88(%rbp), %xmm0
	movq	-88(%rbp), %rax
	movl	$0, -80(%rbp)
	movb	%r9b, -76(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	-104(%rbp), %esi
	movq	%rax, -72(%rbp)
	movl	%esi, -64(%rbp)
	movzbl	-100(%rbp), %esi
	movq	%r8, -88(%rbp)
	movb	%sil, -60(%rbp)
	movzbl	23(%r13), %esi
	andl	$15, %esi
	cmpl	$15, %esi
	je	.L1188
	movq	(%rdx), %rdi
	cmpq	%rdi, %r8
	je	.L1191
.L1189:
	subq	$24, %r13
	testq	%rdi, %rdi
	je	.L1192
	movq	%r13, %rsi
	movq	%rdx, -136(%rbp)
	movq	%r8, -128(%rbp)
	movl	%ecx, -120(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movl	-120(%rbp), %ecx
	movq	-128(%rbp), %r8
	movq	-136(%rbp), %rdx
.L1192:
	movq	%r8, (%rdx)
	movq	%r13, %rsi
	movq	%r8, %rdi
	movl	%ecx, -120(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movq	-96(%rbp), %r13
	movq	-72(%rbp), %r8
	movl	-120(%rbp), %ecx
	movzbl	23(%r13), %eax
	leaq	32(%r13), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1261
	movq	32(%r13), %r13
	leaq	16(%r13), %rdx
.L1261:
	movq	8(%rdx), %rdi
	cmpq	%r8, %rdi
	je	.L1185
	addq	$8, %rdx
	subq	$48, %r13
	testq	%rdi, %rdi
	je	.L1195
	movq	%r13, %rsi
	movq	%rdx, -136(%rbp)
	movq	%r8, -128(%rbp)
	movl	%ecx, -120(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-136(%rbp), %rdx
	movq	-128(%rbp), %r8
	movl	-120(%rbp), %ecx
.L1195:
	movq	%r8, (%rdx)
	testq	%r8, %r8
	je	.L1185
	movq	%r13, %rsi
	movq	%r8, %rdi
	movl	%ecx, -120(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movl	-120(%rbp), %ecx
	jmp	.L1185
	.p2align 4,,10
	.p2align 3
.L1346:
	movq	(%r15), %rsi
.L1174:
	movq	24(%rsi), %rdi
	cmpq	%r13, %rdi
	je	.L1258
	leaq	24(%rsi), %rax
	jmp	.L1176
	.p2align 4,,10
	.p2align 3
.L1239:
	testb	$1, 18(%rsi)
	je	.L1219
	testb	%dil, %dil
	je	.L1219
	cmpl	$15, %eax
	je	.L1242
	movq	%r12, %rcx
	cmpq	%r14, %r10
	je	.L1244
.L1243:
	leaq	-24(%rcx), %rsi
	movq	%r14, %rdi
	movq	%r9, -128(%rbp)
	movl	%r8d, -140(%rbp)
	movq	%r10, -136(%rbp)
	movq	%rsi, -120(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-136(%rbp), %r10
	movq	-128(%rbp), %r9
	movq	-120(%rbp), %rsi
	movq	%r10, (%r9)
	movq	%r10, %rdi
	movq	%r10, -120(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movzbl	23(%r12), %eax
	movq	-120(%rbp), %r10
	movl	-140(%rbp), %r8d
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1245
.L1244:
	movq	8(%r13), %rdi
	addq	$8, %r13
	cmpq	%r14, %rdi
	je	.L1254
.L1247:
	subq	$48, %r12
	testq	%rdi, %rdi
	je	.L1248
	movq	%r12, %rsi
	movl	%r8d, -128(%rbp)
	movq	%r10, -120(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-120(%rbp), %r10
	movl	-128(%rbp), %r8d
.L1248:
	movq	%r14, 0(%r13)
	movq	%r12, %rsi
	movq	%r14, %rdi
	movl	%r8d, -128(%rbp)
	movq	%r10, -120(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movq	-120(%rbp), %r10
	movl	-128(%rbp), %r8d
.L1254:
	movq	%r10, %r14
	jmp	.L1240
	.p2align 4,,10
	.p2align 3
.L1349:
	cmpl	$15, %eax
	je	.L1224
	movq	%rbx, %rdx
	cmpq	%r13, %r12
	je	.L1226
.L1225:
	leaq	-24(%rdx), %rsi
	movq	%r13, %rdi
	movq	%rsi, -120(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	%r12, (%r14)
	movq	-120(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1354
.L1226:
	movq	8(%r15), %rdi
	leaq	8(%r15), %r14
	movq	%rbx, %rdx
	cmpq	%r13, %rdi
	je	.L1343
.L1230:
	leaq	-48(%rdx), %r12
	testq	%rdi, %rdi
	je	.L1231
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L1231:
	movq	%r13, (%r14)
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L1343:
	movq	0(%r13), %rsi
	movq	%r13, %r12
	movzwl	16(%rsi), %ecx
	jmp	.L1223
	.p2align 4,,10
	.p2align 3
.L1233:
	movq	16(%rcx), %r14
	leaq	16(%rcx), %r9
	movq	(%r14), %rdx
	cmpw	$23, 16(%rdx)
	je	.L1236
	xorl	%edi, %edi
	xorl	%r8d, %r8d
.L1237:
	leaq	8(%r9), %rdx
	jmp	.L1238
	.p2align 4,,10
	.p2align 3
.L1234:
	movl	44(%rdx), %r8d
	movl	$1, %edi
	jmp	.L1235
.L1236:
	movl	44(%rdx), %r8d
	movl	$1, %edi
	jmp	.L1237
.L1188:
	movq	(%rdx), %r13
	movq	16(%r13), %rdi
	leaq	16(%r13), %rdx
	cmpq	%rdi, %r8
	jne	.L1189
.L1191:
	movq	%rax, %r8
	jmp	.L1261
.L1224:
	cmpq	%r13, %r12
	jne	.L1225
.L1227:
	movq	24(%rdx), %rdi
	cmpq	%r13, %rdi
	je	.L1343
	leaq	24(%rdx), %r14
	jmp	.L1230
.L1354:
	movq	(%r15), %rdx
	jmp	.L1227
.L1353:
	cmpb	$0, -60(%rbp)
	je	.L1179
	cmpl	$24, -64(%rbp)
	sete	%cl
	cmpb	$2, %dl
	sete	%dl
	testb	%dl, %cl
	jne	.L1342
	jmp	.L1179
.L1249:
	movq	(%r15), %rax
	movq	24(%rax), %rdi
	cmpq	%r14, %rdi
	je	.L1253
	leaq	24(%rax), %r15
	jmp	.L1251
.L1242:
	cmpq	%r14, %r10
	jne	.L1243
.L1245:
	movq	0(%r13), %r12
	movq	24(%r12), %rdi
	cmpq	%r14, %rdi
	je	.L1254
	leaq	24(%r12), %r13
	jmp	.L1247
.L1198:
	movq	(%r15), %rax
	movq	16(%rax), %rdi
	movq	%rax, %rdx
	leaq	16(%rax), %rcx
	cmpq	%rdi, %r13
	jne	.L1199
	movq	-88(%rbp), %r14
.L1204:
	movq	24(%rax), %rdi
	cmpq	%r14, %rdi
	je	.L1207
	leaq	24(%rax), %r15
	jmp	.L1206
.L1352:
	movq	(%r15), %rax
	jmp	.L1204
.L1351:
	movq	-88(%rbp), %r14
	jmp	.L1259
.L1345:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19929:
	.size	_ZN2v88internal8compiler22MachineOperatorReducer15ReduceWord32SarEPNS1_4NodeE, .-_ZN2v88internal8compiler22MachineOperatorReducer15ReduceWord32SarEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler22MachineOperatorReducer17TryMatchWord32RorEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22MachineOperatorReducer17TryMatchWord32RorEPNS1_4NodeE
	.type	_ZN2v88internal8compiler22MachineOperatorReducer17TryMatchWord32RorEPNS1_4NodeE, @function
_ZN2v88internal8compiler22MachineOperatorReducer17TryMatchWord32RorEPNS1_4NodeE:
.LFB19933:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	32(%rsi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$88, %rsp
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %rsi
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1356
	movq	(%rsi), %rdx
	leaq	40(%rbx), %rcx
	movq	%r14, %r15
	cmpw	$23, 16(%rdx)
	movq	%rsi, %rdx
	sete	%dil
.L1357:
	movq	(%rcx), %r12
	movq	(%r12), %rcx
	cmpw	$23, 16(%rcx)
	je	.L1359
	movq	(%rbx), %rcx
	testb	$1, 18(%rcx)
	je	.L1359
	testb	%dil, %dil
	jne	.L1496
.L1359:
	movq	(%rdx), %rax
	movzwl	16(%rax), %eax
	cmpw	$302, %ax
	je	.L1497
	cmpw	$303, %ax
	je	.L1498
.L1431:
	xorl	%eax, %eax
.L1488:
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1498:
	.cfi_restore_state
	movq	(%r12), %rax
	cmpw	$302, 16(%rax)
	jne	.L1431
	movq	%rdx, %rax
	movq	%r12, %rdx
	movq	%rax, %r12
.L1367:
	movzbl	23(%rdx), %eax
	movq	32(%rdx), %rsi
	leaq	32(%rdx), %rcx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1370
	movq	(%rsi), %rdi
	cmpw	$23, 16(%rdi)
	je	.L1371
	xorl	%r10d, %r10d
	xorl	%r9d, %r9d
.L1372:
	leaq	8(%rcx), %rdi
	movq	%rsi, %r11
	movq	%rcx, %r8
.L1375:
	movq	(%rdi), %r15
	movq	(%r15), %rdi
	cmpw	$23, 16(%rdi)
	jne	.L1376
	movb	$1, -64(%rbp)
	movl	44(%rdi), %r10d
.L1377:
	movzbl	23(%r12), %eax
	movq	32(%r12), %rdx
	leaq	32(%r12), %r9
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1384
	movq	(%rdx), %rcx
	cmpw	$23, 16(%rcx)
	je	.L1385
	movl	$0, -56(%rbp)
	xorl	%edi, %edi
.L1386:
	movq	%r9, -72(%rbp)
	leaq	8(%r9), %rsi
	movq	%rdx, %rcx
.L1389:
	movq	(%rsi), %r8
	movq	(%r8), %rsi
	cmpw	$23, 16(%rsi)
	jne	.L1390
	movl	44(%rsi), %eax
	movl	%eax, -56(%rbp)
	cmpq	%rcx, %r11
	jne	.L1431
	cmpb	$0, -64(%rbp)
	je	.L1402
.L1401:
	addl	-56(%rbp), %r10d
	cmpl	$32, %r10d
	jne	.L1431
.L1405:
	movzbl	23(%rbx), %edx
	movq	32(%rbx), %rdi
	andl	$15, %edx
	movq	%rdi, %rax
	cmpl	$15, %edx
	je	.L1421
	movq	%r14, %r15
	movq	%rbx, %r12
	cmpq	%rdi, %r11
	je	.L1427
.L1422:
	subq	$24, %r12
	testq	%rdi, %rdi
	je	.L1426
	movq	%r12, %rsi
	movq	%r11, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-64(%rbp), %r11
	movq	-56(%rbp), %r8
.L1426:
	movq	%r11, (%r15)
	movq	%r12, %rsi
	movq	%r11, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movzbl	23(%rbx), %eax
	movq	-56(%rbp), %r8
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1499
.L1427:
	movq	8(%r14), %rdi
	cmpq	%r8, %rdi
	je	.L1428
	addq	$8, %r14
	movq	%rbx, %rax
.L1424:
	leaq	-48(%rax), %r12
	testq	%rdi, %rdi
	je	.L1429
	movq	%r12, %rsi
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-56(%rbp), %r8
.L1429:
	movq	%r8, (%r14)
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L1428:
	movq	16(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32RorEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	%rbx, %rax
	jmp	.L1488
	.p2align 4,,10
	.p2align 3
.L1497:
	movq	(%r12), %rax
	cmpw	$303, 16(%rax)
	jne	.L1431
	jmp	.L1367
	.p2align 4,,10
	.p2align 3
.L1356:
	movq	16(%rsi), %rdx
	leaq	16(%rsi), %r15
	movq	(%rdx), %rcx
	cmpw	$23, 16(%rcx)
	leaq	24(%rsi), %rcx
	sete	%dil
	jmp	.L1357
	.p2align 4,,10
	.p2align 3
.L1496:
	cmpl	$15, %eax
	je	.L1360
	movq	%rbx, %rsi
	cmpq	%rdx, %r12
	je	.L1362
.L1361:
	subq	$24, %rsi
	movq	%rdx, %rdi
	movq	%rdx, -56(%rbp)
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	%r12, (%r15)
	movq	-64(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movzbl	23(%rbx), %eax
	movq	-56(%rbp), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1500
.L1362:
	movq	8(%r14), %rdi
	cmpq	%rdx, %rdi
	je	.L1435
	leaq	8(%r14), %rax
	movq	%rbx, %rsi
.L1364:
	leaq	-48(%rsi), %r15
	testq	%rdi, %rdi
	je	.L1365
	movq	%r15, %rsi
	movq	%rdx, -64(%rbp)
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-56(%rbp), %rax
	movq	-64(%rbp), %rdx
.L1365:
	movq	%rdx, (%rax)
	movq	%rdx, %rdi
	movq	%r15, %rsi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movq	-56(%rbp), %rdx
	movq	%rdx, %rax
	movq	%r12, %rdx
	movq	%rax, %r12
	jmp	.L1359
	.p2align 4,,10
	.p2align 3
.L1370:
	movq	16(%rsi), %r11
	leaq	16(%rsi), %r8
	movq	(%r11), %rdi
	cmpw	$23, 16(%rdi)
	je	.L1373
	xorl	%r10d, %r10d
	xorl	%r9d, %r9d
.L1374:
	leaq	8(%r8), %rdi
	jmp	.L1375
	.p2align 4,,10
	.p2align 3
.L1390:
	movq	(%r12), %rsi
	testb	$1, 18(%rsi)
	je	.L1392
	testb	%dil, %dil
	jne	.L1501
.L1392:
	cmpq	%rcx, %r11
	jne	.L1431
.L1402:
	movq	(%r15), %rax
	cmpw	$308, 16(%rax)
	je	.L1502
	movq	(%r8), %rax
	cmpw	$308, 16(%rax)
	jne	.L1431
	movq	%r15, -64(%rbp)
	movq	%r8, %r15
.L1403:
	movzbl	23(%r15), %eax
	movq	32(%r15), %rdx
	leaq	32(%r15), %r12
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1406
	movq	(%rdx), %rsi
	movzwl	16(%rsi), %ecx
	cmpw	$23, %cx
	sete	%dil
	je	.L1407
	movb	$0, -56(%rbp)
	xorl	%ecx, %ecx
.L1408:
	movq	%r12, -72(%rbp)
	leaq	8(%r12), %rsi
	movq	%rdx, %r10
.L1411:
	movq	(%rsi), %r9
	movq	(%r9), %rsi
	cmpw	$23, 16(%rsi)
	je	.L1412
	movq	(%r15), %rsi
	testb	$1, 18(%rsi)
	je	.L1412
	cmpb	$0, -56(%rbp)
	jne	.L1503
.L1412:
	testb	%dil, %dil
	je	.L1431
	cmpl	$32, %ecx
	jne	.L1431
	cmpq	%r9, -64(%rbp)
	jne	.L1431
	jmp	.L1405
	.p2align 4,,10
	.p2align 3
.L1376:
	movq	(%rdx), %rdi
	andb	18(%rdi), %r9b
	movb	%r9b, -64(%rbp)
	jne	.L1504
	xorl	%r10d, %r10d
	jmp	.L1377
	.p2align 4,,10
	.p2align 3
.L1384:
	movq	16(%rdx), %rcx
	leaq	16(%rdx), %rdi
	movq	%rdi, -72(%rbp)
	movq	(%rcx), %rsi
	cmpw	$23, 16(%rsi)
	je	.L1387
	movl	$0, -56(%rbp)
	xorl	%edi, %edi
.L1388:
	leaq	24(%rdx), %rsi
	jmp	.L1389
	.p2align 4,,10
	.p2align 3
.L1371:
	movl	44(%rdi), %r10d
	movl	$1, %r9d
	jmp	.L1372
	.p2align 4,,10
	.p2align 3
.L1385:
	movl	44(%rcx), %edi
	movl	%edi, -56(%rbp)
	movl	$1, %edi
	jmp	.L1386
	.p2align 4,,10
	.p2align 3
.L1373:
	movl	44(%rdi), %r10d
	movl	$1, %r9d
	jmp	.L1374
	.p2align 4,,10
	.p2align 3
.L1387:
	movl	44(%rsi), %edi
	movl	%edi, -56(%rbp)
	movl	$1, %edi
	jmp	.L1388
	.p2align 4,,10
	.p2align 3
.L1500:
	movq	32(%rbx), %rsi
.L1363:
	movq	24(%rsi), %rdi
	cmpq	%rdx, %rdi
	je	.L1435
	leaq	24(%rsi), %rax
	jmp	.L1364
	.p2align 4,,10
	.p2align 3
.L1501:
	cmpl	$15, %eax
	je	.L1393
	movq	%r12, %rdx
	cmpq	%rcx, %r8
	je	.L1395
.L1394:
	leaq	-24(%rdx), %rsi
	movq	%rcx, %rdi
	movl	%r10d, -116(%rbp)
	movq	%r9, -112(%rbp)
	movq	%r11, -104(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%r8, -96(%rbp)
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-96(%rbp), %r8
	movq	-72(%rbp), %rax
	movq	-88(%rbp), %rsi
	movq	%r8, (%rax)
	movq	%r8, %rdi
	movq	%r8, -72(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movzbl	23(%r12), %eax
	movq	-72(%rbp), %r8
	movq	-80(%rbp), %rcx
	movq	-104(%rbp), %r11
	andl	$15, %eax
	movq	-112(%rbp), %r9
	movl	-116(%rbp), %r10d
	cmpl	$15, %eax
	je	.L1505
.L1395:
	movq	8(%r9), %rdi
	addq	$8, %r9
	cmpq	%rcx, %rdi
	je	.L1397
.L1398:
	subq	$48, %r12
	testq	%rdi, %rdi
	je	.L1400
	movq	%r12, %rsi
	movl	%r10d, -104(%rbp)
	movq	%r11, -96(%rbp)
	movq	%r9, -88(%rbp)
	movq	%r8, -80(%rbp)
	movq	%rcx, -72(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-72(%rbp), %rcx
	movq	-80(%rbp), %r8
	movq	-88(%rbp), %r9
	movq	-96(%rbp), %r11
	movl	-104(%rbp), %r10d
.L1400:
	movq	%rcx, (%r9)
	movq	%rcx, %rdi
	movq	%r12, %rsi
	movl	%r10d, -96(%rbp)
	movq	%r11, -88(%rbp)
	movq	%r8, -80(%rbp)
	movq	%rcx, -72(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movq	-80(%rbp), %r8
	movq	-88(%rbp), %r11
	movq	-72(%rbp), %rcx
	movl	-96(%rbp), %r10d
	cmpq	%r11, %r8
	jne	.L1431
.L1494:
	cmpb	$0, -64(%rbp)
	movq	%rcx, %r8
	jne	.L1401
	jmp	.L1402
	.p2align 4,,10
	.p2align 3
.L1435:
	movq	%r12, %rdx
	movq	%rdi, %r12
	jmp	.L1359
	.p2align 4,,10
	.p2align 3
.L1360:
	cmpq	%rdx, %r12
	jne	.L1361
	jmp	.L1363
	.p2align 4,,10
	.p2align 3
.L1504:
	cmpl	$15, %eax
	je	.L1378
	movq	%rdx, %rsi
	cmpq	%r11, %r15
	je	.L1380
.L1379:
	subq	$24, %rsi
	movq	%r11, %rdi
	movl	%r10d, -104(%rbp)
	movq	%rcx, -96(%rbp)
	movq	%rdx, -88(%rbp)
	movq	%r11, -56(%rbp)
	movq	%r8, -80(%rbp)
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-80(%rbp), %r8
	movq	-72(%rbp), %rsi
	movq	%r15, %rdi
	movq	%r15, (%r8)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movq	-88(%rbp), %rdx
	movq	-56(%rbp), %r11
	movq	-96(%rbp), %rcx
	movl	-104(%rbp), %r10d
	movzbl	23(%rdx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1506
.L1380:
	movq	8(%rcx), %rdi
	addq	$8, %rcx
	cmpq	%r11, %rdi
	je	.L1440
.L1382:
	leaq	-48(%rdx), %rsi
	testq	%rdi, %rdi
	je	.L1383
	movl	%r10d, -88(%rbp)
	movq	%r11, -80(%rbp)
	movq	%rcx, -72(%rbp)
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-56(%rbp), %rsi
	movq	-72(%rbp), %rcx
	movq	-80(%rbp), %r11
	movl	-88(%rbp), %r10d
.L1383:
	movq	%r11, (%rcx)
	movq	%r11, %rdi
	movl	%r10d, -72(%rbp)
	movq	%r11, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movq	-56(%rbp), %r11
	movl	-72(%rbp), %r10d
	movq	%r11, %rax
	movq	%r15, %r11
	movq	%rax, %r15
	jmp	.L1377
	.p2align 4,,10
	.p2align 3
.L1502:
	movq	%r8, -64(%rbp)
	jmp	.L1403
.L1406:
	movq	16(%rdx), %r10
	leaq	16(%rdx), %rcx
	movq	%rcx, -72(%rbp)
	movq	(%r10), %r9
	movzwl	16(%r9), %esi
	cmpw	$23, %si
	sete	%dil
	je	.L1409
	movb	$0, -56(%rbp)
	xorl	%ecx, %ecx
.L1410:
	leaq	24(%rdx), %rsi
	jmp	.L1411
.L1407:
	movb	$1, -56(%rbp)
	movl	44(%rsi), %ecx
	jmp	.L1408
.L1378:
	cmpq	%r11, %r15
	jne	.L1379
	movq	%rsi, %rdx
.L1381:
	movq	24(%rdx), %rdi
	cmpq	%r11, %rdi
	je	.L1440
	leaq	24(%rdx), %rcx
	jmp	.L1382
.L1393:
	cmpq	%rcx, %r8
	jne	.L1394
	movq	%rdx, %r12
.L1396:
	movq	24(%r12), %rdi
	cmpq	%rcx, %rdi
	je	.L1397
	leaq	24(%r12), %r9
	jmp	.L1398
.L1440:
	movq	%r15, %r11
	movq	%rdi, %r15
	jmp	.L1377
.L1397:
	cmpq	%r11, %r8
	jne	.L1431
	jmp	.L1494
.L1409:
	movb	$1, -56(%rbp)
	movl	44(%r9), %ecx
	jmp	.L1410
.L1505:
	movq	(%r9), %r12
	jmp	.L1396
.L1506:
	movq	(%rcx), %rdx
	jmp	.L1381
.L1421:
	movq	16(%rdi), %rdx
	leaq	16(%rdi), %r15
	cmpq	%r11, %rdx
	je	.L1425
	movq	%rdi, %r12
	movq	%rdx, %rdi
	jmp	.L1422
.L1499:
	movq	32(%rbx), %rax
.L1425:
	movq	24(%rax), %rdi
	cmpq	%r8, %rdi
	je	.L1428
	leaq	24(%rax), %r14
	jmp	.L1424
.L1503:
	cmpl	$15, %eax
	je	.L1413
	movq	%r15, %rdx
	cmpq	%r10, %r9
	je	.L1415
.L1414:
	leaq	-24(%rdx), %r13
	movq	%r10, %rdi
	movq	%r10, -56(%rbp)
	movq	%r13, %rsi
	movq	%r9, -64(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-72(%rbp), %rax
	movq	-64(%rbp), %r9
	movq	%r13, %rsi
	movq	%r9, (%rax)
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movzbl	23(%r15), %eax
	movq	-56(%rbp), %r10
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1416
.L1415:
	movq	8(%r12), %rdi
	addq	$8, %r12
	cmpq	%r10, %rdi
	je	.L1431
.L1418:
	subq	$48, %r15
	testq	%rdi, %rdi
	je	.L1419
	movq	%r15, %rsi
	movq	%r10, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-56(%rbp), %r10
.L1419:
	movq	%r10, (%r12)
	movq	%r15, %rsi
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	jmp	.L1431
.L1413:
	cmpq	%r10, %r9
	jne	.L1414
.L1416:
	movq	(%r12), %r15
	movq	24(%r15), %rdi
	cmpq	%r10, %rdi
	je	.L1431
	leaq	24(%r15), %r12
	jmp	.L1418
	.cfi_endproc
.LFE19933:
	.size	_ZN2v88internal8compiler22MachineOperatorReducer17TryMatchWord32RorEPNS1_4NodeE, .-_ZN2v88internal8compiler22MachineOperatorReducer17TryMatchWord32RorEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler22MachineOperatorReducer14ReduceWord32OrEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22MachineOperatorReducer14ReduceWord32OrEPNS1_4NodeE
	.type	_ZN2v88internal8compiler22MachineOperatorReducer14ReduceWord32OrEPNS1_4NodeE, @function
_ZN2v88internal8compiler22MachineOperatorReducer14ReduceWord32OrEPNS1_4NodeE:
.LFB19934:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	32(%rsi), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movzbl	23(%rsi), %edx
	movq	32(%rsi), %rsi
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L1508
	movq	(%rsi), %rcx
	movzwl	16(%rcx), %eax
	cmpw	$23, %ax
	sete	%dil
	je	.L1509
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
.L1510:
	leaq	8(%r14), %rax
	movq	%rsi, %rbx
	movq	%r14, %r8
.L1513:
	movq	(%rax), %r13
	movq	0(%r13), %rax
	cmpw	$23, 16(%rax)
	jne	.L1514
	movl	44(%rax), %esi
.L1515:
	testl	%esi, %esi
	je	.L1542
	movq	%r13, %rax
	cmpl	$-1, %esi
	je	.L1540
	testb	%dil, %dil
	je	.L1516
	movq	16(%r15), %rdi
	orl	%ecx, %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
.L1540:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1509:
	.cfi_restore_state
	movl	44(%rcx), %ecx
	movl	$1, %r9d
	jmp	.L1510
	.p2align 4,,10
	.p2align 3
.L1514:
	movq	(%r12), %rax
	testb	$1, 18(%rax)
	je	.L1516
	testb	%r9b, %r9b
	jne	.L1543
.L1516:
	cmpq	%r13, %rbx
	je	.L1542
	addq	$40, %rsp
	movq	%r12, %rsi
	movq	%r15, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler22MachineOperatorReducer17TryMatchWord32RorEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L1542:
	.cfi_restore_state
	addq	$40, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1508:
	.cfi_restore_state
	movq	16(%rsi), %rbx
	leaq	16(%rsi), %r8
	movq	(%rbx), %rcx
	movzwl	16(%rcx), %eax
	cmpw	$23, %ax
	sete	%dil
	je	.L1511
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
.L1512:
	leaq	8(%r8), %rax
	jmp	.L1513
	.p2align 4,,10
	.p2align 3
.L1511:
	movl	44(%rcx), %ecx
	movl	$1, %r9d
	jmp	.L1512
	.p2align 4,,10
	.p2align 3
.L1543:
	cmpl	$15, %edx
	je	.L1517
	movq	%r12, %rsi
	cmpq	%rbx, %r13
	je	.L1519
.L1518:
	subq	$24, %rsi
	movq	%rbx, %rdi
	movl	%ecx, -68(%rbp)
	movq	%r8, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-64(%rbp), %r8
	movq	-56(%rbp), %rsi
	movq	%r13, %rdi
	movq	%r13, (%r8)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movzbl	23(%r12), %eax
	movl	-68(%rbp), %ecx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1544
.L1519:
	movq	8(%r14), %rdi
	cmpq	%rbx, %rdi
	je	.L1529
	leaq	8(%r14), %rax
	movq	%r12, %rsi
.L1522:
	leaq	-48(%rsi), %r14
	testq	%rdi, %rdi
	je	.L1523
	movq	%r14, %rsi
	movl	%ecx, -64(%rbp)
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-56(%rbp), %rax
	movl	-64(%rbp), %ecx
.L1523:
	movq	%rbx, (%rax)
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movl	%ecx, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movl	-56(%rbp), %ecx
.L1529:
	movq	%rbx, %rax
	movl	%ecx, %esi
	movq	%r13, %rbx
	xorl	%edi, %edi
	movq	%rax, %r13
	xorl	%ecx, %ecx
	jmp	.L1515
	.p2align 4,,10
	.p2align 3
.L1544:
	movq	32(%r12), %rsi
.L1520:
	movq	24(%rsi), %rdi
	cmpq	%rbx, %rdi
	je	.L1529
	leaq	24(%rsi), %rax
	jmp	.L1522
	.p2align 4,,10
	.p2align 3
.L1517:
	cmpq	%rbx, %r13
	jne	.L1518
	jmp	.L1520
	.cfi_endproc
.LFE19934:
	.size	_ZN2v88internal8compiler22MachineOperatorReducer14ReduceWord32OrEPNS1_4NodeE, .-_ZN2v88internal8compiler22MachineOperatorReducer14ReduceWord32OrEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler22MachineOperatorReducer15ReduceWord32XorEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22MachineOperatorReducer15ReduceWord32XorEPNS1_4NodeE
	.type	_ZN2v88internal8compiler22MachineOperatorReducer15ReduceWord32XorEPNS1_4NodeE, @function
_ZN2v88internal8compiler22MachineOperatorReducer15ReduceWord32XorEPNS1_4NodeE:
.LFB19935:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	32(%rsi), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %rsi
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1546
	movq	(%rsi), %rcx
	movzwl	16(%rcx), %edx
	cmpw	$23, %dx
	sete	%dil
	je	.L1547
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
.L1548:
	leaq	8(%r14), %rdx
	movq	%rsi, %rbx
	movq	%r14, %r8
.L1551:
	movq	(%rdx), %r13
	movq	0(%r13), %rdx
	cmpw	$23, 16(%rdx)
	jne	.L1552
	movl	44(%rdx), %esi
	testl	%esi, %esi
	je	.L1553
	testb	%dil, %dil
	je	.L1587
	movq	16(%r15), %rdi
	xorl	%ecx, %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
.L1614:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1547:
	.cfi_restore_state
	movl	44(%rcx), %ecx
	movl	$1, %r9d
	jmp	.L1548
	.p2align 4,,10
	.p2align 3
.L1552:
	movq	(%r12), %rdx
	testb	$1, 18(%rdx)
	je	.L1555
	testb	%r9b, %r9b
	jne	.L1616
.L1555:
	cmpq	%rbx, %r13
	je	.L1563
.L1568:
	addq	$40, %rsp
	movq	%r12, %rsi
	movq	%r15, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler22MachineOperatorReducer17TryMatchWord32RorEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L1546:
	.cfi_restore_state
	movq	16(%rsi), %rbx
	leaq	16(%rsi), %r8
	movq	(%rbx), %rcx
	movzwl	16(%rcx), %edx
	cmpw	$23, %dx
	sete	%dil
	je	.L1549
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
.L1550:
	leaq	8(%r8), %rdx
	jmp	.L1551
	.p2align 4,,10
	.p2align 3
.L1549:
	movl	44(%rcx), %ecx
	movl	$1, %r9d
	jmp	.L1550
	.p2align 4,,10
	.p2align 3
.L1556:
	cmpq	%rbx, %r13
	je	.L1559
.L1557:
	subq	$24, %rsi
	movq	%rbx, %rdi
	movl	%ecx, -72(%rbp)
	movq	%r8, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-64(%rbp), %r8
	movq	-56(%rbp), %rsi
	movq	%r13, %rdi
	movq	%r13, (%r8)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movzbl	23(%r12), %eax
	movl	-72(%rbp), %ecx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1617
.L1558:
	movq	8(%r14), %rdi
	cmpq	%rbx, %rdi
	je	.L1588
	leaq	8(%r14), %rax
	movq	%r12, %rsi
.L1561:
	leaq	-48(%rsi), %r14
	testq	%rdi, %rdi
	je	.L1562
	movq	%r14, %rsi
	movl	%ecx, -64(%rbp)
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-56(%rbp), %rax
	movl	-64(%rbp), %ecx
.L1562:
	movq	%rbx, (%rax)
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movl	%ecx, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movl	-56(%rbp), %ecx
.L1588:
	testl	%ecx, %ecx
	jne	.L1618
	movq	%r13, %rbx
.L1553:
	addq	$40, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1618:
	.cfi_restore_state
	movq	%r13, %rax
	movl	%ecx, %esi
	movq	%rbx, %r13
	movq	%rax, %rbx
.L1587:
	cmpq	%r13, %rbx
	je	.L1563
	movq	(%rbx), %rdx
	cmpw	$301, 16(%rdx)
	jne	.L1568
	cmpl	$-1, %esi
	jne	.L1568
	movzbl	23(%rbx), %ecx
	movq	32(%rbx), %rsi
	leaq	32(%rbx), %r13
	andl	$15, %ecx
	cmpl	$15, %ecx
	je	.L1569
	movq	(%rsi), %rax
	cmpw	$23, 16(%rax)
	je	.L1570
	xorl	%r8d, %r8d
	xorl	%r10d, %r10d
.L1571:
	leaq	8(%r13), %rdi
	movq	%rsi, %rax
	movq	%r13, %r9
.L1574:
	movq	(%rdi), %r14
	movq	(%r14), %rdi
	cmpw	$23, 16(%rdi)
	jne	.L1575
	movl	44(%rdi), %r8d
.L1576:
	cmpl	$-1, %r8d
	je	.L1614
	jmp	.L1568
	.p2align 4,,10
	.p2align 3
.L1563:
	movq	16(%r15), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1616:
	.cfi_restore_state
	cmpl	$15, %eax
	je	.L1556
	movq	%r12, %rsi
	cmpq	%rbx, %r13
	jne	.L1557
	jmp	.L1558
	.p2align 4,,10
	.p2align 3
.L1617:
	movq	32(%r12), %rsi
.L1559:
	movq	24(%rsi), %rdi
	cmpq	%rbx, %rdi
	je	.L1588
	leaq	24(%rsi), %rax
	jmp	.L1561
	.p2align 4,,10
	.p2align 3
.L1575:
	testb	$1, 18(%rdx)
	je	.L1568
	testb	%r10b, %r10b
	je	.L1568
	cmpl	$15, %ecx
	je	.L1578
	movq	%rbx, %rsi
	cmpq	%rax, %r14
	je	.L1580
.L1579:
	subq	$24, %rsi
	movq	%rax, %rdi
	movl	%r8d, -76(%rbp)
	movq	%rax, -56(%rbp)
	movq	%r9, -72(%rbp)
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-72(%rbp), %r9
	movq	-64(%rbp), %rsi
	movq	%r14, %rdi
	movq	%r14, (%r9)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movzbl	23(%rbx), %edx
	movq	-56(%rbp), %rax
	movl	-76(%rbp), %r8d
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L1581
.L1580:
	movq	8(%r13), %rdi
	addq	$8, %r13
	cmpq	%rax, %rdi
	je	.L1585
.L1583:
	subq	$48, %rbx
	testq	%rdi, %rdi
	je	.L1584
	movq	%rbx, %rsi
	movl	%r8d, -64(%rbp)
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-56(%rbp), %rax
	movl	-64(%rbp), %r8d
.L1584:
	movq	%rax, 0(%r13)
	movq	%rbx, %rsi
	movq	%rax, %rdi
	movl	%r8d, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movl	-56(%rbp), %r8d
.L1585:
	movq	%r14, %rax
	jmp	.L1576
	.p2align 4,,10
	.p2align 3
.L1569:
	movq	16(%rsi), %rax
	leaq	16(%rsi), %r9
	movq	(%rax), %rdi
	cmpw	$23, 16(%rdi)
	je	.L1572
	xorl	%r8d, %r8d
	xorl	%r10d, %r10d
.L1573:
	leaq	8(%r9), %rdi
	jmp	.L1574
	.p2align 4,,10
	.p2align 3
.L1570:
	movl	44(%rax), %r8d
	movl	$1, %r10d
	jmp	.L1571
.L1572:
	movl	44(%rdi), %r8d
	movl	$1, %r10d
	jmp	.L1573
.L1578:
	cmpq	%rax, %r14
	jne	.L1579
.L1581:
	movq	0(%r13), %rbx
	movq	24(%rbx), %rdi
	cmpq	%rax, %rdi
	je	.L1585
	leaq	24(%rbx), %r13
	jmp	.L1583
	.cfi_endproc
.LFE19935:
	.size	_ZN2v88internal8compiler22MachineOperatorReducer15ReduceWord32XorEPNS1_4NodeE, .-_ZN2v88internal8compiler22MachineOperatorReducer15ReduceWord32XorEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler22MachineOperatorReducer28ReduceFloat64InsertLowWord32EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22MachineOperatorReducer28ReduceFloat64InsertLowWord32EPNS1_4NodeE
	.type	_ZN2v88internal8compiler22MachineOperatorReducer28ReduceFloat64InsertLowWord32EPNS1_4NodeE, @function
_ZN2v88internal8compiler22MachineOperatorReducer28ReduceFloat64InsertLowWord32EPNS1_4NodeE:
.LFB19936:
	.cfi_startproc
	endbr64
	movzbl	23(%rsi), %edx
	movq	32(%rsi), %rcx
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L1620
	movq	(%rcx), %rcx
	leaq	32(%rsi), %rax
	movzwl	16(%rcx), %edx
	cmpw	$26, %dx
	je	.L1623
.L1633:
	xorl	%ecx, %ecx
.L1624:
	addq	$8, %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	cmpw	$23, 16(%rax)
	jne	.L1626
	cmpw	$26, %dx
	je	.L1634
.L1626:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1620:
	movq	16(%rcx), %rdx
	leaq	16(%rcx), %rax
	movq	(%rdx), %rcx
	movzwl	16(%rcx), %edx
	cmpw	$26, %dx
	jne	.L1633
.L1623:
	movq	48(%rcx), %rcx
	jmp	.L1624
	.p2align 4,,10
	.p2align 3
.L1634:
	movabsq	$-4294967296, %rdx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	44(%rax), %eax
	andq	%rdx, %rcx
	movq	16(%rdi), %rdi
	orq	%rax, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rcx, %xmm0
	call	_ZN2v88internal8compiler12MachineGraph15Float64ConstantEd@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19936:
	.size	_ZN2v88internal8compiler22MachineOperatorReducer28ReduceFloat64InsertLowWord32EPNS1_4NodeE, .-_ZN2v88internal8compiler22MachineOperatorReducer28ReduceFloat64InsertLowWord32EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler22MachineOperatorReducer29ReduceFloat64InsertHighWord32EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22MachineOperatorReducer29ReduceFloat64InsertHighWord32EPNS1_4NodeE
	.type	_ZN2v88internal8compiler22MachineOperatorReducer29ReduceFloat64InsertHighWord32EPNS1_4NodeE, @function
_ZN2v88internal8compiler22MachineOperatorReducer29ReduceFloat64InsertHighWord32EPNS1_4NodeE:
.LFB19937:
	.cfi_startproc
	endbr64
	movzbl	23(%rsi), %edx
	movq	32(%rsi), %rcx
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L1636
	movq	(%rcx), %rcx
	leaq	32(%rsi), %rax
	movzwl	16(%rcx), %edx
	cmpw	$26, %dx
	je	.L1639
.L1649:
	xorl	%ecx, %ecx
.L1640:
	addq	$8, %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	cmpw	$23, 16(%rax)
	jne	.L1642
	cmpw	$26, %dx
	je	.L1650
.L1642:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1636:
	movq	16(%rcx), %rdx
	leaq	16(%rcx), %rax
	movq	(%rdx), %rcx
	movzwl	16(%rcx), %edx
	cmpw	$26, %dx
	jne	.L1649
.L1639:
	movq	48(%rcx), %rcx
	jmp	.L1640
	.p2align 4,,10
	.p2align 3
.L1650:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	44(%rax), %eax
	movl	%ecx, %ecx
	movq	16(%rdi), %rdi
	salq	$32, %rax
	orq	%rcx, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rax, %xmm0
	call	_ZN2v88internal8compiler12MachineGraph15Float64ConstantEd@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19937:
	.size	_ZN2v88internal8compiler22MachineOperatorReducer29ReduceFloat64InsertHighWord32EPNS1_4NodeE, .-_ZN2v88internal8compiler22MachineOperatorReducer29ReduceFloat64InsertHighWord32EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler22MachineOperatorReducer20ReduceFloat64CompareEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22MachineOperatorReducer20ReduceFloat64CompareEPNS1_4NodeE
	.type	_ZN2v88internal8compiler22MachineOperatorReducer20ReduceFloat64CompareEPNS1_4NodeE, @function
_ZN2v88internal8compiler22MachineOperatorReducer20ReduceFloat64CompareEPNS1_4NodeE:
.LFB19939:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	32(%rsi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movzbl	23(%rsi), %edx
	movq	32(%rsi), %rsi
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L1652
	movq	(%rsi), %rcx
	movzwl	16(%rcx), %eax
	cmpw	$26, %ax
	sete	%r8b
	je	.L1653
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
.L1654:
	leaq	8(%r14), %rdi
	movq	%rsi, %rcx
	movq	%r14, %r9
.L1657:
	movq	(%rdi), %r15
	movq	(%r15), %rdi
	movzwl	16(%rdi), %r10d
	cmpw	$26, %r10w
	jne	.L1658
	movsd	48(%rdi), %xmm1
	movl	$1, %r12d
	testb	%r8b, %r8b
	je	.L1660
	movq	(%rbx), %rax
	movzwl	16(%rax), %eax
	cmpw	$348, %ax
	je	.L1669
	cmpw	$349, %ax
	je	.L1670
	cmpw	$347, %ax
	jne	.L1671
	xorl	%esi, %esi
	ucomisd	%xmm0, %xmm1
	movl	$0, %eax
	movq	16(%r13), %rdi
	setnp	%sil
	cmovne	%eax, %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	jmp	.L1740
	.p2align 4,,10
	.p2align 3
.L1653:
	movsd	48(%rcx), %xmm0
	movl	$1, %eax
	jmp	.L1654
	.p2align 4,,10
	.p2align 3
.L1662:
	cmpq	%rcx, %r15
	je	.L1665
.L1663:
	subq	$24, %rsi
	movq	%rcx, %rdi
	movq	%rcx, -56(%rbp)
	movsd	%xmm0, -80(%rbp)
	movq	%r9, -72(%rbp)
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-72(%rbp), %r9
	movq	-64(%rbp), %rsi
	movq	%r15, %rdi
	movq	%r15, (%r9)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movzbl	23(%rbx), %eax
	movq	-56(%rbp), %rcx
	movsd	-80(%rbp), %xmm0
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1749
.L1664:
	movq	8(%r14), %rdi
	cmpq	%rcx, %rdi
	je	.L1707
	leaq	8(%r14), %rax
	movq	%rbx, %rsi
.L1667:
	subq	$48, %rsi
	testq	%rdi, %rdi
	je	.L1668
	movq	%rcx, -72(%rbp)
	movq	%rax, -64(%rbp)
	movq	%rsi, -56(%rbp)
	movsd	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %rax
	movq	-72(%rbp), %rcx
	movsd	-80(%rbp), %xmm0
.L1668:
	movq	%rcx, (%rax)
	movq	%rcx, %rdi
	movq	%rcx, -56(%rbp)
	movsd	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movq	-56(%rbp), %rcx
	movsd	-64(%rbp), %xmm0
.L1707:
	movq	%rcx, %rax
	movapd	%xmm0, %xmm1
	pxor	%xmm0, %xmm0
	movq	%r15, %rcx
	movq	%rax, %r15
	.p2align 4,,10
	.p2align 3
.L1660:
	movq	(%rcx), %rax
	cmpw	$447, 16(%rax)
	je	.L1750
.L1673:
	xorl	%eax, %eax
.L1740:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1658:
	.cfi_restore_state
	movq	(%rbx), %rdi
	andb	18(%rdi), %al
	movl	%eax, %r12d
	jne	.L1751
	movl	%r8d, %r12d
	pxor	%xmm1, %xmm1
	testb	%r8b, %r8b
	je	.L1660
	movq	(%rcx), %rax
	cmpw	$447, 16(%rax)
	je	.L1752
.L1709:
	comisd	.LC5(%rip), %xmm0
	jbe	.L1743
	movsd	.LC6(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jnb	.L1673
	movsd	.LC3(%rip), %xmm1
.L1683:
	ucomisd	%xmm1, %xmm0
	jp	.L1673
	jne	.L1673
	cmpw	$447, %r10w
	jne	.L1673
.L1747:
	xorl	%r12d, %r12d
	pxor	%xmm1, %xmm1
.L1679:
	movzwl	16(%rdi), %eax
	cmpw	$348, %ax
	je	.L1687
	cmpw	$349, %ax
	jne	.L1753
	movq	16(%r13), %rax
	movb	%r8b, -72(%rbp)
	movq	%rcx, -64(%rbp)
	movq	16(%rax), %rdi
	movsd	%xmm0, -80(%rbp)
	movsd	%xmm1, -56(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder22Float32LessThanOrEqualEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movsd	-80(%rbp), %xmm0
	movzbl	-72(%rbp), %r8d
	movq	-64(%rbp), %rcx
	movsd	-56(%rbp), %xmm1
.L1690:
	testb	%r8b, %r8b
	jne	.L1754
	movzbl	23(%rcx), %eax
	movq	32(%rcx), %r8
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1692
	movq	16(%r8), %r8
.L1692:
	movzbl	23(%rbx), %eax
	movq	32(%rbx), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1694
	movq	%r14, %rax
	movq	%rbx, %rsi
	cmpq	%rdi, %r8
	je	.L1696
.L1695:
	subq	$24, %rsi
	testq	%rdi, %rdi
	je	.L1698
	movq	%r8, -80(%rbp)
	movq	%rax, -72(%rbp)
	movq	%rsi, -56(%rbp)
	movsd	%xmm1, -64(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-80(%rbp), %r8
	movq	-72(%rbp), %rax
	movsd	-64(%rbp), %xmm1
	movq	-56(%rbp), %rsi
.L1698:
	movq	%r8, (%rax)
	testq	%r8, %r8
	je	.L1696
	movq	%r8, %rdi
	movsd	%xmm1, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movsd	-56(%rbp), %xmm1
.L1696:
	testb	%r12b, %r12b
	jne	.L1755
	movzbl	23(%r15), %eax
	movq	32(%r15), %r12
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1700
	movq	16(%r12), %r12
.L1700:
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1702
	movq	8(%r14), %rdi
	cmpq	%rdi, %r12
	je	.L1705
	addq	$8, %r14
	movq	%rbx, %rsi
.L1704:
	leaq	-48(%rsi), %r13
	testq	%rdi, %rdi
	je	.L1706
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L1706:
	movq	%r12, (%r14)
	testq	%r12, %r12
	je	.L1705
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L1705:
	addq	$40, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1652:
	.cfi_restore_state
	movq	16(%rsi), %rcx
	leaq	16(%rsi), %r9
	movq	(%rcx), %rdi
	movzwl	16(%rdi), %eax
	cmpw	$26, %ax
	sete	%r8b
	je	.L1655
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
.L1656:
	leaq	8(%r9), %rdi
	jmp	.L1657
	.p2align 4,,10
	.p2align 3
.L1750:
	movq	(%r15), %rax
	cmpw	$447, 16(%rax)
	je	.L1715
	testb	%r12b, %r12b
	je	.L1673
	comisd	.LC5(%rip), %xmm1
	jbe	.L1741
	movsd	.LC6(%rip), %xmm2
	comisd	%xmm1, %xmm2
	jnb	.L1673
	movsd	.LC3(%rip), %xmm2
.L1676:
	ucomisd	%xmm2, %xmm1
	jp	.L1673
	jne	.L1673
.L1715:
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	jmp	.L1679
	.p2align 4,,10
	.p2align 3
.L1655:
	movsd	48(%rdi), %xmm0
	movl	$1, %eax
	jmp	.L1656
	.p2align 4,,10
	.p2align 3
.L1743:
	movsd	.LC7(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jbe	.L1744
	comisd	.LC8(%rip), %xmm0
	movsd	.LC2(%rip), %xmm1
	jb	.L1683
	jmp	.L1673
	.p2align 4,,10
	.p2align 3
.L1670:
	xorl	%esi, %esi
	comisd	%xmm0, %xmm1
	movq	16(%r13), %rdi
	setnb	%sil
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	jmp	.L1740
	.p2align 4,,10
	.p2align 3
.L1669:
	xorl	%esi, %esi
	comisd	%xmm0, %xmm1
	movq	16(%r13), %rdi
	seta	%sil
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	jmp	.L1740
	.p2align 4,,10
	.p2align 3
.L1752:
	cmpw	$447, %r10w
	jne	.L1709
	movl	%r12d, %r8d
	jmp	.L1747
	.p2align 4,,10
	.p2align 3
.L1753:
	cmpw	$347, %ax
	je	.L1756
.L1671:
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1751:
	cmpl	$15, %edx
	je	.L1662
	movq	%rbx, %rsi
	cmpq	%rcx, %r15
	jne	.L1663
	jmp	.L1664
	.p2align 4,,10
	.p2align 3
.L1744:
	pxor	%xmm1, %xmm1
	cvtsd2ss	%xmm0, %xmm1
	cvtss2sd	%xmm1, %xmm1
	jmp	.L1683
	.p2align 4,,10
	.p2align 3
.L1702:
	movq	32(%rbx), %rsi
	movq	24(%rsi), %rdi
	cmpq	%rdi, %r12
	je	.L1705
	leaq	24(%rsi), %r14
	jmp	.L1704
	.p2align 4,,10
	.p2align 3
.L1694:
	movq	16(%rdi), %rdx
	leaq	16(%rdi), %rax
	cmpq	%rdx, %r8
	je	.L1696
	movq	%rdi, %rsi
	movq	%rdx, %rdi
	jmp	.L1695
	.p2align 4,,10
	.p2align 3
.L1756:
	movq	16(%r13), %rax
	movb	%r8b, -72(%rbp)
	movq	%rcx, -64(%rbp)
	movq	16(%rax), %rdi
	movsd	%xmm0, -80(%rbp)
	movsd	%xmm1, -56(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder12Float32EqualEv@PLT
.L1748:
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movsd	-56(%rbp), %xmm1
	movq	-64(%rbp), %rcx
	movzbl	-72(%rbp), %r8d
	movsd	-80(%rbp), %xmm0
	jmp	.L1690
	.p2align 4,,10
	.p2align 3
.L1687:
	movq	16(%r13), %rax
	movb	%r8b, -72(%rbp)
	movq	%rcx, -64(%rbp)
	movq	16(%rax), %rdi
	movsd	%xmm0, -80(%rbp)
	movsd	%xmm1, -56(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder15Float32LessThanEv@PLT
	jmp	.L1748
	.p2align 4,,10
	.p2align 3
.L1755:
	movq	16(%r13), %rax
	pxor	%xmm0, %xmm0
	cvtsd2ss	%xmm1, %xmm0
	movq	8(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder15Float32ConstantEf@PLT
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rax, %rsi
	xorl	%edx, %edx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r12
	jmp	.L1700
	.p2align 4,,10
	.p2align 3
.L1754:
	movq	16(%r13), %rax
	movsd	%xmm1, -64(%rbp)
	cvtsd2ss	%xmm0, %xmm0
	movq	(%rax), %r9
	movq	8(%rax), %rdi
	movq	%r9, -56(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder15Float32ConstantEf@PLT
	movq	-56(%rbp), %r9
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rax, %rsi
	xorl	%edx, %edx
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movsd	-64(%rbp), %xmm1
	movq	%rax, %r8
	jmp	.L1692
	.p2align 4,,10
	.p2align 3
.L1741:
	movsd	.LC7(%rip), %xmm2
	comisd	%xmm1, %xmm2
	jbe	.L1742
	comisd	.LC8(%rip), %xmm1
	movsd	.LC2(%rip), %xmm2
	jb	.L1676
	jmp	.L1673
	.p2align 4,,10
	.p2align 3
.L1742:
	pxor	%xmm2, %xmm2
	cvtsd2ss	%xmm1, %xmm2
	cvtss2sd	%xmm2, %xmm2
	jmp	.L1676
	.p2align 4,,10
	.p2align 3
.L1749:
	movq	32(%rbx), %rsi
.L1665:
	movq	24(%rsi), %rdi
	cmpq	%rcx, %rdi
	je	.L1707
	leaq	24(%rsi), %rax
	jmp	.L1667
	.cfi_endproc
.LFE19939:
	.size	_ZN2v88internal8compiler22MachineOperatorReducer20ReduceFloat64CompareEPNS1_4NodeE, .-_ZN2v88internal8compiler22MachineOperatorReducer20ReduceFloat64CompareEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler22MachineOperatorReducer22ReduceFloat64RoundDownEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22MachineOperatorReducer22ReduceFloat64RoundDownEPNS1_4NodeE
	.type	_ZN2v88internal8compiler22MachineOperatorReducer22ReduceFloat64RoundDownEPNS1_4NodeE, @function
_ZN2v88internal8compiler22MachineOperatorReducer22ReduceFloat64RoundDownEPNS1_4NodeE:
.LFB19940:
	.cfi_startproc
	endbr64
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1758
	movq	16(%rdx), %rdx
.L1758:
	movq	(%rdx), %rdx
	xorl	%eax, %eax
	cmpw	$26, 16(%rdx)
	jne	.L1764
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movsd	48(%rdx), %xmm3
	movsd	.LC10(%rip), %xmm2
	movsd	.LC9(%rip), %xmm4
	movapd	%xmm3, %xmm1
	movapd	%xmm3, %xmm0
	andpd	%xmm2, %xmm1
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	ucomisd	%xmm1, %xmm4
	ja	.L1767
	movq	16(%rdi), %rdi
	call	_ZN2v88internal8compiler12MachineGraph15Float64ConstantEd@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1764:
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L1767:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	cvttsd2siq	%xmm3, %rax
	pxor	%xmm1, %xmm1
	andnpd	%xmm3, %xmm2
	movq	16(%rdi), %rdi
	movsd	.LC11(%rip), %xmm4
	cvtsi2sdq	%rax, %xmm1
	movapd	%xmm1, %xmm5
	cmpnlesd	%xmm3, %xmm5
	movapd	%xmm5, %xmm0
	andpd	%xmm4, %xmm0
	subsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	orpd	%xmm2, %xmm0
	call	_ZN2v88internal8compiler12MachineGraph15Float64ConstantEd@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19940:
	.size	_ZN2v88internal8compiler22MachineOperatorReducer22ReduceFloat64RoundDownEPNS1_4NodeE, .-_ZN2v88internal8compiler22MachineOperatorReducer22ReduceFloat64RoundDownEPNS1_4NodeE
	.section	.text._ZNK2v88internal8compiler22MachineOperatorReducer6commonEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler22MachineOperatorReducer6commonEv
	.type	_ZNK2v88internal8compiler22MachineOperatorReducer6commonEv, @function
_ZNK2v88internal8compiler22MachineOperatorReducer6commonEv:
.LFB19941:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	8(%rax), %rax
	ret
	.cfi_endproc
.LFE19941:
	.size	_ZNK2v88internal8compiler22MachineOperatorReducer6commonEv, .-_ZNK2v88internal8compiler22MachineOperatorReducer6commonEv
	.section	.text._ZNK2v88internal8compiler22MachineOperatorReducer7machineEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler22MachineOperatorReducer7machineEv
	.type	_ZNK2v88internal8compiler22MachineOperatorReducer7machineEv, @function
_ZNK2v88internal8compiler22MachineOperatorReducer7machineEv:
.LFB19942:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	16(%rax), %rax
	ret
	.cfi_endproc
.LFE19942:
	.size	_ZNK2v88internal8compiler22MachineOperatorReducer7machineEv, .-_ZNK2v88internal8compiler22MachineOperatorReducer7machineEv
	.section	.text._ZNK2v88internal8compiler22MachineOperatorReducer5graphEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler22MachineOperatorReducer5graphEv
	.type	_ZNK2v88internal8compiler22MachineOperatorReducer5graphEv, @function
_ZNK2v88internal8compiler22MachineOperatorReducer5graphEv:
.LFB19943:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	(%rax), %rax
	ret
	.cfi_endproc
.LFE19943:
	.size	_ZNK2v88internal8compiler22MachineOperatorReducer5graphEv, .-_ZNK2v88internal8compiler22MachineOperatorReducer5graphEv
	.section	.text._ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES6_EC2EPNS1_4NodeE,"axG",@progbits,_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES6_EC5EPNS1_4NodeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES6_EC2EPNS1_4NodeE
	.type	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES6_EC2EPNS1_4NodeE, @function
_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES6_EC2EPNS1_4NodeE:
.LFB21895:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	leaq	32(%rsi), %r12
	pushq	%rbx
	movq	%r12, %rdx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	%rsi, (%rdi)
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1772
	movq	32(%rsi), %rax
	leaq	16(%rax), %rdx
.L1772:
	movq	(%rdx), %rax
	movl	$0, 16(%rbx)
	movq	%rax, 8(%rbx)
	movq	(%rax), %rdx
	movzwl	16(%rdx), %eax
	cmpw	$23, %ax
	sete	%cl
	movb	%cl, 20(%rbx)
	jne	.L1773
	movl	44(%rdx), %eax
	movl	%eax, 16(%rbx)
.L1773:
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1774
	leaq	8(%r12), %rax
.L1775:
	movq	(%rax), %r13
	movl	$0, 32(%rbx)
	movq	%r13, 24(%rbx)
	movq	0(%r13), %rdx
	movzwl	16(%rdx), %eax
	cmpw	$23, %ax
	sete	%dil
	movb	%dil, 36(%rbx)
	jne	.L1776
	movl	44(%rdx), %eax
	movl	%eax, 32(%rbx)
.L1771:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1776:
	.cfi_restore_state
	movq	(%rsi), %rax
	testb	$1, 18(%rax)
	je	.L1771
	testb	%cl, %cl
	je	.L1771
	movdqu	8(%rbx), %xmm0
	movq	8(%rbx), %rax
	movb	%dil, 20(%rbx)
	movq	%r13, 8(%rbx)
	movq	(%r12), %rdi
	movaps	%xmm0, -48(%rbp)
	movl	-40(%rbp), %edx
	movl	$0, 16(%rbx)
	movl	%edx, 32(%rbx)
	movzbl	-36(%rbp), %edx
	movq	%rax, 24(%rbx)
	movb	%dl, 36(%rbx)
	movzbl	23(%rsi), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L1780
	cmpq	%rdi, %r13
	je	.L1806
.L1781:
	leaq	-24(%rsi), %r14
	testq	%rdi, %rdi
	je	.L1784
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L1784:
	movq	%r13, (%r12)
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movq	(%rbx), %rsi
	movq	24(%rbx), %r13
	movzbl	23(%rsi), %eax
	leaq	32(%rsi), %r12
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1789
.L1788:
	movq	8(%r12), %rdi
	addq	$8, %r12
	cmpq	%rdi, %r13
	je	.L1771
.L1786:
	leaq	-48(%rsi), %r14
	testq	%rdi, %rdi
	je	.L1787
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L1787:
	movq	%r13, (%r12)
	testq	%r13, %r13
	je	.L1771
	addq	$16, %rsp
	movq	%r14, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	.p2align 4,,10
	.p2align 3
.L1774:
	.cfi_restore_state
	movq	(%r12), %rax
	addq	$24, %rax
	jmp	.L1775
.L1783:
	movq	%rax, %r13
.L1789:
	movq	(%r12), %rsi
	movq	24(%rsi), %rdi
	cmpq	%rdi, %r13
	je	.L1771
	leaq	24(%rsi), %r12
	jmp	.L1786
	.p2align 4,,10
	.p2align 3
.L1806:
	movq	%rax, %r13
	jmp	.L1788
	.p2align 4,,10
	.p2align 3
.L1780:
	movq	16(%rdi), %rdx
	leaq	16(%rdi), %rcx
	cmpq	%rdx, %r13
	je	.L1783
	movq	%rdi, %rsi
	movq	%rcx, %r12
	movq	%rdx, %rdi
	jmp	.L1781
	.cfi_endproc
.LFE21895:
	.size	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES6_EC2EPNS1_4NodeE, .-_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES6_EC2EPNS1_4NodeE
	.weak	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES6_EC1EPNS1_4NodeE
	.set	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES6_EC1EPNS1_4NodeE,_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES6_EC2EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler22MachineOperatorReducer15ReduceWord32AndEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22MachineOperatorReducer15ReduceWord32AndEPNS1_4NodeE
	.type	_ZN2v88internal8compiler22MachineOperatorReducer15ReduceWord32AndEPNS1_4NodeE, @function
_ZN2v88internal8compiler22MachineOperatorReducer15ReduceWord32AndEPNS1_4NodeE:
.LFB19931:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	32(%rsi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$168, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	movq	%rsi, -144(%rbp)
	movq	32(%rsi), %rsi
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1808
	movl	$0, -128(%rbp)
	xorl	%r8d, %r8d
	movq	%rsi, -136(%rbp)
	movq	(%rsi), %rcx
	movzwl	16(%rcx), %edx
	cmpw	$23, %dx
	sete	-124(%rbp)
	je	.L2080
.L1810:
	leaq	8(%r14), %rdx
	movq	%r14, %r15
.L1813:
	movq	(%rdx), %r12
	movl	$0, -112(%rbp)
	movq	%r12, -120(%rbp)
	movq	(%r12), %rcx
	movzwl	16(%rcx), %edx
	cmpw	$23, %dx
	sete	%dil
	movb	%dil, -108(%rbp)
	jne	.L1814
	movl	44(%rcx), %eax
	movl	%eax, -112(%rbp)
.L1815:
	movl	-112(%rbp), %eax
	testl	%eax, %eax
	je	.L2081
	cmpl	$-1, %eax
	je	.L2074
.L1827:
	leaq	-136(%rbp), %rdi
	call	_ZNK2v88internal8compiler11NodeMatcher12IsComparisonEv@PLT
	testb	%al, %al
	je	.L1830
	cmpb	$0, -108(%rbp)
	je	.L1831
	movl	-112(%rbp), %esi
	cmpl	$1, %esi
	je	.L2074
	cmpb	$0, -124(%rbp)
	je	.L1831
.L1974:
	movq	16(%r13), %rdi
	andl	-128(%rbp), %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	jmp	.L1829
	.p2align 4,,10
	.p2align 3
.L1814:
	movq	(%rbx), %rdx
	testb	$1, 18(%rdx)
	je	.L1827
	testb	%r8b, %r8b
	je	.L1827
	movdqu	-136(%rbp), %xmm0
	movq	-136(%rbp), %rdx
	movb	%dil, -124(%rbp)
	movq	%r12, -136(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	-88(%rbp), %ecx
	movl	$0, -128(%rbp)
	movl	%ecx, -112(%rbp)
	movzbl	-84(%rbp), %ecx
	movq	%rdx, -120(%rbp)
	movb	%cl, -108(%rbp)
	movq	(%r15), %rdi
	cmpl	$15, %eax
	je	.L1816
	movq	%rbx, %rsi
	cmpq	%rdi, %r12
	je	.L2082
.L1817:
	subq	$24, %rsi
	testq	%rdi, %rdi
	je	.L1820
	movq	%rsi, -152(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-152(%rbp), %rsi
.L1820:
	movq	%r12, (%r15)
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movq	-144(%rbp), %rsi
	movq	-120(%rbp), %r12
	movzbl	23(%rsi), %edx
	leaq	32(%rsi), %rax
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L1976
.L1975:
	movq	8(%rax), %rdi
	addq	$8, %rax
	cmpq	%r12, %rdi
	je	.L2072
.L1824:
	leaq	-48(%rsi), %r15
	testq	%rdi, %rdi
	je	.L1825
	movq	%r15, %rsi
	movq	%rax, -152(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-152(%rbp), %rax
.L1825:
	movq	%r12, (%rax)
	testq	%r12, %r12
	je	.L2072
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L2072:
	movzbl	-108(%rbp), %eax
	testb	%al, %al
	je	.L1827
	jmp	.L1815
	.p2align 4,,10
	.p2align 3
.L1830:
	cmpb	$0, -124(%rbp)
	jne	.L2083
.L1831:
	movq	-136(%rbp), %r12
	cmpq	%r12, -120(%rbp)
	je	.L2075
	movq	(%r12), %rdx
	movzbl	-108(%rbp), %ecx
	movzwl	16(%rdx), %eax
	cmpw	$299, %ax
	je	.L2084
	testb	%cl, %cl
	je	.L1836
.L1979:
	movl	-112(%rbp), %r15d
	testl	%r15d, %r15d
	jns	.L1836
	cmpl	$-2147483648, %r15d
	je	.L1984
	movl	%r15d, %ecx
	movl	%r15d, %esi
	negl	%ecx
	notl	%esi
	testl	%ecx, %esi
	jne	.L1836
.L1865:
	cmpw	$302, %ax
	je	.L2085
	cmpw	$306, %ax
	je	.L2086
	cmpw	$310, %ax
	jne	.L1836
	movzbl	23(%r12), %esi
	movq	32(%r12), %rdi
	leaq	32(%r12), %r14
	andl	$15, %esi
	cmpl	$15, %esi
	je	.L1945
	movq	(%rdi), %rax
	cmpw	$23, 16(%rax)
	je	.L1946
	xorl	%r10d, %r10d
	xorl	%ebx, %ebx
.L1947:
	leaq	8(%r14), %rax
	movq	%rdi, %r15
	movq	%r14, %r9
.L1950:
	movq	(%rax), %r13
	movq	0(%r13), %r8
	cmpw	$23, 16(%r8)
	jne	.L1951
	movl	44(%r8), %ebx
.L1952:
	movl	%ebx, %eax
	cltd
	idivl	%ecx
	testl	%edx, %edx
	je	.L2075
	.p2align 4,,10
	.p2align 3
.L1836:
	xorl	%eax, %eax
.L1829:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L2087
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2080:
	.cfi_restore_state
	movl	44(%rcx), %edx
	movl	$1, %r8d
	movl	%edx, -128(%rbp)
	jmp	.L1810
	.p2align 4,,10
	.p2align 3
.L1808:
	movq	16(%rsi), %rdx
	movl	$0, -128(%rbp)
	xorl	%r8d, %r8d
	leaq	16(%rsi), %r15
	movq	%rdx, -136(%rbp)
	movq	(%rdx), %rcx
	movzwl	16(%rcx), %edx
	cmpw	$23, %dx
	sete	-124(%rbp)
	je	.L2088
	leaq	8(%r15), %rdx
	jmp	.L1813
	.p2align 4,,10
	.p2align 3
.L2083:
	cmpb	$0, -108(%rbp)
	je	.L1831
	movl	-112(%rbp), %esi
	jmp	.L1974
	.p2align 4,,10
	.p2align 3
.L2088:
	movl	44(%rcx), %edx
	movl	$1, %r8d
	movl	%edx, -128(%rbp)
	leaq	8(%r15), %rdx
	jmp	.L1813
	.p2align 4,,10
	.p2align 3
.L2074:
	movq	-136(%rbp), %rax
	jmp	.L1829
	.p2align 4,,10
	.p2align 3
.L2081:
	movq	-120(%rbp), %rax
	jmp	.L1829
	.p2align 4,,10
	.p2align 3
.L2075:
	movq	%r12, %rax
	jmp	.L1829
	.p2align 4,,10
	.p2align 3
.L2084:
	testb	%cl, %cl
	je	.L1836
	movzbl	23(%r12), %esi
	movq	32(%r12), %rdi
	leaq	32(%r12), %r15
	andl	$15, %esi
	cmpl	$15, %esi
	je	.L1837
	movq	(%rdi), %r8
	cmpw	$23, 16(%r8)
	je	.L1838
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
.L1839:
	movq	%r15, -152(%rbp)
	leaq	8(%r15), %r10
	movq	%rdi, %r9
.L1842:
	movq	(%r10), %r10
	movq	(%r10), %r11
	cmpw	$23, 16(%r11)
	jne	.L1843
	movl	44(%r11), %r8d
.L1844:
	movzbl	23(%rbx), %eax
	movq	32(%rbx), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1853
	movq	%r14, %r15
	movq	%rbx, %r12
	cmpq	%r9, %rdi
	je	.L1855
.L1854:
	subq	$24, %r12
	testq	%rdi, %rdi
	je	.L1856
	movq	%r12, %rsi
	movl	%r8d, -160(%rbp)
	movq	%r9, -152(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movl	-160(%rbp), %r8d
	movq	-152(%rbp), %r9
.L1856:
	movq	%r9, (%r15)
	movq	%r12, %rsi
	movq	%r9, %rdi
	movl	%r8d, -152(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movl	-152(%rbp), %r8d
.L1855:
	movl	-112(%rbp), %esi
	movq	16(%r13), %rdi
	andl	%r8d, %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%rax, %r12
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1857
	movq	8(%r14), %rdi
	cmpq	%rdi, %r12
	je	.L1860
	addq	$8, %r14
	movq	%rbx, %rax
.L1859:
	leaq	-48(%rax), %r15
	testq	%rdi, %rdi
	je	.L1861
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L1861:
	movq	%r12, (%r14)
	testq	%r12, %r12
	je	.L1860
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L1860:
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler22MachineOperatorReducer15ReduceWord32AndEPNS1_4NodeE
	testq	%rax, %rax
	cmove	%rbx, %rax
	jmp	.L1829
	.p2align 4,,10
	.p2align 3
.L1984:
	movl	$-2147483648, %ecx
	jmp	.L1865
	.p2align 4,,10
	.p2align 3
.L1816:
	cmpq	%rdi, %r12
	jne	.L1817
	movq	%rdx, %r12
	movq	%r14, %rax
.L1976:
	movq	(%rax), %rsi
	movq	24(%rsi), %rdi
	cmpq	%r12, %rdi
	je	.L2072
	leaq	24(%rsi), %rax
	jmp	.L1824
	.p2align 4,,10
	.p2align 3
.L2082:
	movq	%rdx, %r12
	movq	%r14, %rax
	jmp	.L1975
	.p2align 4,,10
	.p2align 3
.L1843:
	testb	$1, 18(%rdx)
	je	.L1979
	testb	%cl, %cl
	je	.L1979
	cmpl	$15, %esi
	je	.L1846
	movq	%r12, %rdi
	cmpq	%r9, %r10
	je	.L1848
.L1847:
	leaq	-24(%rdi), %rsi
	movq	%r9, %rdi
	movl	%r8d, -184(%rbp)
	movq	%r9, -160(%rbp)
	movq	%r10, -176(%rbp)
	movq	%rsi, -168(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-176(%rbp), %r10
	movq	-152(%rbp), %rax
	movq	-168(%rbp), %rsi
	movq	%r10, (%rax)
	movq	%r10, %rdi
	movq	%r10, -152(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movzbl	23(%r12), %eax
	movq	-152(%rbp), %r10
	movq	-160(%rbp), %r9
	movl	-184(%rbp), %r8d
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1849
.L1848:
	movq	8(%r15), %rdi
	addq	$8, %r15
	cmpq	%r9, %rdi
	je	.L1971
.L1851:
	subq	$48, %r12
	testq	%rdi, %rdi
	je	.L1852
	movq	%r12, %rsi
	movl	%r8d, -168(%rbp)
	movq	%r9, -160(%rbp)
	movq	%r10, -152(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-152(%rbp), %r10
	movq	-160(%rbp), %r9
	movl	-168(%rbp), %r8d
.L1852:
	movq	%r9, (%r15)
	movq	%r12, %rsi
	movq	%r9, %rdi
	movl	%r8d, -160(%rbp)
	movq	%r10, -152(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movq	-152(%rbp), %r10
	movl	-160(%rbp), %r8d
.L1971:
	movq	%r10, %r9
	jmp	.L1844
	.p2align 4,,10
	.p2align 3
.L2085:
	movzbl	23(%r12), %eax
	movq	32(%r12), %rcx
	leaq	32(%r12), %rbx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1867
	movq	(%rcx), %rsi
	cmpw	$23, 16(%rsi)
	je	.L1868
	xorl	%r14d, %r14d
	xorl	%edi, %edi
.L1869:
	leaq	8(%rbx), %rsi
	movq	%rcx, %r8
	movq	%rbx, %r9
.L1872:
	movq	(%rsi), %r13
	movq	0(%r13), %rsi
	cmpw	$23, 16(%rsi)
	jne	.L1873
	movl	44(%rsi), %r14d
.L1874:
	rep bsfl	%r15d, %r15d
	andl	$31, %r14d
	movq	%r12, %rax
	cmpl	%r14d, %r15d
	jbe	.L1829
	jmp	.L1836
	.p2align 4,,10
	.p2align 3
.L2086:
	movzbl	23(%r12), %eax
	movq	32(%r12), %rsi
	leaq	32(%r12), %r10
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1884
	movq	(%rsi), %rdi
	cmpw	$23, 16(%rdi)
	je	.L1885
	movb	$0, -152(%rbp)
	xorl	%r11d, %r11d
.L1886:
	movq	%r10, -160(%rbp)
	leaq	8(%r10), %rdi
	movq	%rsi, %r8
.L1889:
	movq	(%rdi), %r9
	movq	(%r9), %rdi
	cmpw	$23, 16(%rdi)
	jne	.L1890
	movl	44(%rdi), %r11d
.L1891:
	movl	%r15d, %eax
	andl	%r11d, %eax
	cmpl	%r11d, %eax
	je	.L1900
.L1892:
	movq	(%r8), %rax
	cmpw	$310, 16(%rax)
	je	.L2089
.L1902:
	movq	(%r9), %rax
	cmpw	$310, 16(%rax)
	je	.L2090
.L1934:
	movq	(%r8), %rax
	cmpw	$302, 16(%rax)
	je	.L2091
.L1941:
	movq	(%r9), %rax
	cmpw	$302, 16(%rax)
	jne	.L1836
	leaq	-96(%rbp), %rdi
	movq	%r9, %rsi
	movq	%r8, -152(%rbp)
	call	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES6_EC1EPNS1_4NodeE
	cmpb	$0, -60(%rbp)
	movq	-152(%rbp), %r8
	je	.L1836
	rep bsfl	%r15d, %r15d
	cmpl	%r15d, -64(%rbp)
	jne	.L1836
.L2079:
	movq	-120(%rbp), %rdx
	movq	%r8, %rsi
.L2078:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler22MachineOperatorReducer9Word32AndEPNS1_4NodeES4_
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.0
	movq	-96(%rbp), %rsi
.L2077:
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.1
.L2076:
	movq	16(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Int32AddEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler22MachineOperatorReducer14ReduceInt32AddEPNS1_4NodeE
	testq	%rax, %rax
	cmove	%rbx, %rax
	jmp	.L1829
	.p2align 4,,10
	.p2align 3
.L1837:
	movq	16(%rdi), %r9
	leaq	16(%rdi), %r11
	movq	%r11, -152(%rbp)
	movq	(%r9), %r8
	cmpw	$23, 16(%r8)
	je	.L1840
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
.L1841:
	leaq	24(%rdi), %r10
	jmp	.L1842
	.p2align 4,,10
	.p2align 3
.L1838:
	movl	44(%r8), %r8d
	jmp	.L1839
.L1873:
	testb	$1, 18(%rdx)
	je	.L1836
	testb	%dil, %dil
	je	.L1836
	cmpl	$15, %eax
	je	.L1876
	movq	%r12, %rcx
	cmpq	%r8, %r13
	je	.L1878
.L1877:
	leaq	-24(%rcx), %rsi
	movq	%r8, %rdi
	movq	%r8, -152(%rbp)
	movq	%r9, -168(%rbp)
	movq	%rsi, -160(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-168(%rbp), %r9
	movq	-160(%rbp), %rsi
	movq	%r13, %rdi
	movq	%r13, (%r9)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movzbl	23(%r12), %eax
	movq	-152(%rbp), %r8
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1879
.L1878:
	movq	8(%rbx), %rdi
	cmpq	%r8, %rdi
	je	.L1874
	addq	$8, %rbx
	movq	%r12, %rax
.L1881:
	leaq	-48(%rax), %r13
	testq	%rdi, %rdi
	je	.L1882
	movq	%r13, %rsi
	movq	%r8, -152(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-152(%rbp), %r8
.L1882:
	movq	%r8, (%rbx)
	movq	%r13, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	jmp	.L1874
	.p2align 4,,10
	.p2align 3
.L1890:
	testb	$1, 18(%rdx)
	je	.L1892
	cmpb	$0, -152(%rbp)
	je	.L1892
	cmpl	$15, %eax
	je	.L1893
	movq	%r12, %rsi
	cmpq	%r8, %r9
	je	.L1895
.L1894:
	subq	$24, %rsi
	movq	%r8, %rdi
	movl	%r11d, -200(%rbp)
	movq	%r10, -184(%rbp)
	movl	%ecx, -176(%rbp)
	movq	%r8, -168(%rbp)
	movq	%r9, -192(%rbp)
	movq	%rsi, -152(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-192(%rbp), %r9
	movq	-160(%rbp), %rax
	movq	-152(%rbp), %rsi
	movq	%r9, (%rax)
	movq	%r9, %rdi
	movq	%r9, -152(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movzbl	23(%r12), %eax
	movq	-152(%rbp), %r9
	movq	-168(%rbp), %r8
	movl	-176(%rbp), %ecx
	andl	$15, %eax
	movq	-184(%rbp), %r10
	movl	-200(%rbp), %r11d
	cmpl	$15, %eax
	je	.L1896
.L1895:
	movq	8(%r10), %rdi
	cmpq	%r8, %rdi
	je	.L1969
	addq	$8, %r10
.L1898:
	subq	$48, %r12
	testq	%rdi, %rdi
	je	.L1899
	movq	%r12, %rsi
	movl	%r11d, -184(%rbp)
	movq	%r8, -176(%rbp)
	movq	%r10, -168(%rbp)
	movq	%r9, -160(%rbp)
	movl	%ecx, -152(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movl	-152(%rbp), %ecx
	movq	-160(%rbp), %r9
	movq	-168(%rbp), %r10
	movq	-176(%rbp), %r8
	movl	-184(%rbp), %r11d
.L1899:
	movq	%r8, (%r10)
	movq	%r8, %rdi
	movq	%r12, %rsi
	movl	%r11d, -176(%rbp)
	movq	%r9, -168(%rbp)
	movl	%ecx, -160(%rbp)
	movq	%r8, -152(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movq	-152(%rbp), %r8
	movl	-160(%rbp), %ecx
	movq	-168(%rbp), %r9
	movl	-176(%rbp), %r11d
.L1969:
	movq	%r9, %rax
	movq	%r8, %r9
	movq	%rax, %r8
	jmp	.L1891
.L1840:
	movl	44(%r8), %r8d
	jmp	.L1841
.L1867:
	movq	16(%rcx), %r8
	leaq	16(%rcx), %r9
	movq	(%r8), %rsi
	cmpw	$23, 16(%rsi)
	je	.L1870
	xorl	%r14d, %r14d
	xorl	%edi, %edi
.L1871:
	leaq	8(%r9), %rsi
	jmp	.L1872
.L1868:
	movl	44(%rsi), %r14d
	movl	$1, %edi
	jmp	.L1869
.L1951:
	testb	$1, 18(%rdx)
	je	.L1836
	testb	%r10b, %r10b
	je	.L1836
	cmpl	$15, %esi
	je	.L1954
	movq	%r12, %rdi
	cmpq	%r15, %r13
	je	.L1956
.L1955:
	leaq	-24(%rdi), %rsi
	movq	%r15, %rdi
	movl	%ecx, -160(%rbp)
	movq	%r9, -168(%rbp)
	movq	%rsi, -152(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-168(%rbp), %r9
	movq	-152(%rbp), %rsi
	movq	%r13, %rdi
	movq	%r13, (%r9)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movzbl	23(%r12), %eax
	movl	-160(%rbp), %ecx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1957
.L1956:
	movq	8(%r14), %rdi
	cmpq	%r15, %rdi
	je	.L1952
	addq	$8, %r14
	movq	%r12, %rax
.L1959:
	leaq	-48(%rax), %r13
	testq	%rdi, %rdi
	je	.L1960
	movq	%r13, %rsi
	movl	%ecx, -152(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movl	-152(%rbp), %ecx
.L1960:
	movq	%r15, (%r14)
	movq	%r13, %rsi
	movq	%r15, %rdi
	movl	%ecx, -152(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movl	-152(%rbp), %ecx
	jmp	.L1952
	.p2align 4,,10
	.p2align 3
.L1884:
	movq	16(%rsi), %r8
	leaq	16(%rsi), %rdi
	movq	%rdi, -160(%rbp)
	movq	(%r8), %rdi
	cmpw	$23, 16(%rdi)
	je	.L1887
	movb	$0, -152(%rbp)
	xorl	%r11d, %r11d
.L1888:
	leaq	24(%rsi), %rdi
	jmp	.L1889
.L1885:
	movb	$1, -152(%rbp)
	movl	44(%rdi), %r11d
	jmp	.L1886
.L1857:
	movq	32(%rbx), %rax
	movq	24(%rax), %rdi
	cmpq	%r12, %rdi
	je	.L1860
	leaq	24(%rax), %r14
	jmp	.L1859
.L1853:
	movq	16(%rdi), %rax
	leaq	16(%rdi), %r15
	cmpq	%r9, %rax
	je	.L1855
	movq	%rdi, %r12
	movq	%rax, %rdi
	jmp	.L1854
.L1945:
	movq	16(%rdi), %r15
	leaq	16(%rdi), %r9
	movq	(%r15), %rax
	cmpw	$23, 16(%rax)
	je	.L1948
	xorl	%r10d, %r10d
	xorl	%ebx, %ebx
.L1949:
	leaq	8(%r9), %rax
	jmp	.L1950
.L1946:
	movl	44(%rax), %ebx
	movl	$1, %r10d
	jmp	.L1947
.L1870:
	movl	44(%rsi), %r14d
	movl	$1, %edi
	jmp	.L1871
.L2089:
	movzbl	23(%r8), %edx
	leaq	32(%r8), %r14
	andl	$15, %edx
	movl	%edx, %esi
	movl	%edx, -152(%rbp)
	movq	32(%r8), %rdx
	cmpl	$15, %esi
	je	.L1916
	movq	(%rdx), %rsi
	cmpw	$23, 16(%rsi)
	je	.L1917
	xorl	%r12d, %r12d
	xorl	%esi, %esi
.L1918:
	movq	%r14, -160(%rbp)
	leaq	8(%r14), %rdi
	movq	%rdx, %r10
.L1921:
	movq	(%rdi), %r11
	movq	(%r11), %rdi
	cmpw	$23, 16(%rdi)
	jne	.L1922
	movl	44(%rdi), %r12d
.L1923:
	movl	%r12d, %eax
	cltd
	idivl	%ecx
	testl	%edx, %edx
	jne	.L1902
	movq	-120(%rbp), %rdx
	movq	%r9, %rsi
	movq	%r13, %rdi
	movq	%r8, -152(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorReducer9Word32AndEPNS1_4NodeES4_
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.0
	movq	-152(%rbp), %r8
	movq	%r8, %rsi
	jmp	.L2077
.L2090:
	movq	%r9, %rsi
	leaq	-96(%rbp), %rdi
	movq	%r8, -160(%rbp)
	movq	%r9, -152(%rbp)
	movl	%ecx, -168(%rbp)
	call	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES6_EC1EPNS1_4NodeE
	cmpb	$0, -60(%rbp)
	movq	-152(%rbp), %r9
	movq	-160(%rbp), %r8
	je	.L1934
	movl	-64(%rbp), %eax
	movl	-168(%rbp), %ecx
	cltd
	idivl	%ecx
	testl	%edx, %edx
	jne	.L1934
	jmp	.L2079
.L1887:
	movb	$1, -152(%rbp)
	movl	44(%rdi), %r11d
	jmp	.L1888
.L2091:
	movq	%r8, %rsi
	leaq	-96(%rbp), %rdi
	movq	%r9, -160(%rbp)
	movq	%r8, -152(%rbp)
	call	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES6_EC1EPNS1_4NodeE
	cmpb	$0, -60(%rbp)
	movq	-152(%rbp), %r8
	movq	-160(%rbp), %r9
	je	.L1941
	xorl	%eax, %eax
	rep bsfl	%r15d, %eax
	cmpl	%eax, -64(%rbp)
	jne	.L1941
	movq	-120(%rbp), %rdx
	movq	%r9, %rsi
	jmp	.L2078
.L1948:
	movl	44(%rax), %ebx
	movl	$1, %r10d
	jmp	.L1949
.L1900:
	movq	-120(%rbp), %rdx
	movq	%r8, %rsi
	movq	%r13, %rdi
	movq	%r9, -152(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorReducer9Word32AndEPNS1_4NodeES4_
	movq	-152(%rbp), %r9
	movq	%rax, %r12
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1903
	movq	32(%rbx), %rdi
	cmpq	%rdi, %r12
	je	.L1910
	movq	%r14, %rdx
	movq	%rbx, %r15
.L1904:
	subq	$24, %r15
	testq	%rdi, %rdi
	je	.L1908
	movq	%r15, %rsi
	movq	%rdx, -160(%rbp)
	movq	%r9, -152(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-160(%rbp), %rdx
	movq	-152(%rbp), %r9
.L1908:
	movq	%r12, (%rdx)
	testq	%r12, %r12
	je	.L1909
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%r9, -152(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movq	-152(%rbp), %r9
.L1909:
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L2092
.L1910:
	movq	8(%r14), %rdi
	cmpq	%r9, %rdi
	je	.L2076
	addq	$8, %r14
	movq	%rbx, %rax
.L1906:
	leaq	-48(%rax), %r12
	testq	%rdi, %rdi
	je	.L1912
	movq	%r12, %rsi
	movq	%r9, -152(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-152(%rbp), %r9
.L1912:
	movq	%r9, (%r14)
	movq	%r12, %rsi
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	jmp	.L2076
.L1922:
	testb	$1, 18(%rax)
	je	.L1902
	testb	%sil, %sil
	je	.L1902
	cmpl	$15, -152(%rbp)
	je	.L1925
	movq	%r8, %rdx
	cmpq	%r10, %r11
	je	.L1927
.L1926:
	leaq	-24(%rdx), %rsi
	movq	%r10, %rdi
	movq	%r8, -200(%rbp)
	movq	%r9, -184(%rbp)
	movl	%ecx, -176(%rbp)
	movq	%r10, -152(%rbp)
	movq	%r11, -192(%rbp)
	movq	%rsi, -168(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-160(%rbp), %rax
	movq	-192(%rbp), %r11
	movq	-168(%rbp), %rsi
	movq	%r11, (%rax)
	movq	%r11, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movq	-200(%rbp), %r8
	movq	-152(%rbp), %r10
	movl	-176(%rbp), %ecx
	movq	-184(%rbp), %r9
	movzbl	23(%r8), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1928
.L1927:
	movq	8(%r14), %rdi
	cmpq	%r10, %rdi
	je	.L1923
	leaq	8(%r14), %rdx
	movq	%r8, %rax
.L1930:
	leaq	-48(%rax), %r14
	testq	%rdi, %rdi
	je	.L1931
	movq	%r14, %rsi
	movq	%r8, -184(%rbp)
	movq	%rdx, -176(%rbp)
	movq	%r10, -168(%rbp)
	movq	%r9, -160(%rbp)
	movl	%ecx, -152(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movl	-152(%rbp), %ecx
	movq	-160(%rbp), %r9
	movq	-168(%rbp), %r10
	movq	-176(%rbp), %rdx
	movq	-184(%rbp), %r8
.L1931:
	movq	%r10, (%rdx)
	movq	%r14, %rsi
	movq	%r10, %rdi
	movq	%r8, -168(%rbp)
	movq	%r9, -160(%rbp)
	movl	%ecx, -152(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movl	-152(%rbp), %ecx
	movq	-160(%rbp), %r9
	movq	-168(%rbp), %r8
	jmp	.L1923
	.p2align 4,,10
	.p2align 3
.L1846:
	cmpq	%r9, %r10
	jne	.L1847
.L1849:
	movq	(%r15), %r12
	movq	24(%r12), %rdi
	cmpq	%r9, %rdi
	je	.L1971
	leaq	24(%r12), %r15
	jmp	.L1851
.L1917:
	movl	44(%rsi), %r12d
	movl	$1, %esi
	jmp	.L1918
.L1916:
	movq	16(%rdx), %r10
	leaq	16(%rdx), %rsi
	movq	%rsi, -160(%rbp)
	movq	(%r10), %rsi
	cmpw	$23, 16(%rsi)
	je	.L1919
	xorl	%r12d, %r12d
	xorl	%esi, %esi
.L1920:
	leaq	24(%rdx), %rdi
	jmp	.L1921
.L1876:
	cmpq	%r8, %r13
	jne	.L1877
.L1879:
	movq	(%rbx), %rax
	movq	24(%rax), %rdi
	cmpq	%r8, %rdi
	je	.L1874
	leaq	24(%rax), %rbx
	jmp	.L1881
.L1903:
	movq	32(%rbx), %rax
	movq	16(%rax), %rdi
	movq	%rax, %r15
	leaq	16(%rax), %rdx
	cmpq	%rdi, %r12
	jne	.L1904
.L1907:
	movq	24(%rax), %rdi
	cmpq	%r9, %rdi
	je	.L2076
	leaq	24(%rax), %r14
	jmp	.L1906
.L1919:
	movl	44(%rsi), %r12d
	movl	$1, %esi
	jmp	.L1920
.L2087:
	call	__stack_chk_fail@PLT
.L1893:
	cmpq	%r8, %r9
	jne	.L1894
.L1896:
	movq	(%r10), %r12
	movq	24(%r12), %rdi
	cmpq	%r8, %rdi
	je	.L1969
	leaq	24(%r12), %r10
	jmp	.L1898
.L2092:
	movq	32(%rbx), %rax
	jmp	.L1907
.L1954:
	cmpq	%r15, %r13
	jne	.L1955
.L1957:
	movq	(%r14), %rax
	movq	24(%rax), %rdi
	cmpq	%r15, %rdi
	je	.L1952
	leaq	24(%rax), %r14
	jmp	.L1959
.L1925:
	cmpq	%r10, %r11
	jne	.L1926
.L1928:
	movq	(%r14), %rax
	movq	24(%rax), %rdi
	cmpq	%r10, %rdi
	je	.L1923
	leaq	24(%rax), %rdx
	jmp	.L1930
	.cfi_endproc
.LFE19931:
	.size	_ZN2v88internal8compiler22MachineOperatorReducer15ReduceWord32AndEPNS1_4NodeE, .-_ZN2v88internal8compiler22MachineOperatorReducer15ReduceWord32AndEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler22MachineOperatorReducer9Word32AndEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22MachineOperatorReducer9Word32AndEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler22MachineOperatorReducer9Word32AndEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler22MachineOperatorReducer9Word32AndEPNS1_4NodeES4_:
.LFB19901:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movaps	%xmm0, -80(%rbp)
	movq	16(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32AndEv@PLT
	movdqa	-80(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	leaq	-64(%rbp), %rcx
	movq	%rax, %rsi
	movl	$2, %edx
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler22MachineOperatorReducer15ReduceWord32AndEPNS1_4NodeE
	testq	%rax, %rax
	cmove	%rbx, %rax
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L2098
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2098:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19901:
	.size	_ZN2v88internal8compiler22MachineOperatorReducer9Word32AndEPNS1_4NodeES4_, .-_ZN2v88internal8compiler22MachineOperatorReducer9Word32AndEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler22MachineOperatorReducer15ReduceWord32ShlEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22MachineOperatorReducer15ReduceWord32ShlEPNS1_4NodeE
	.type	_ZN2v88internal8compiler22MachineOperatorReducer15ReduceWord32ShlEPNS1_4NodeE, @function
_ZN2v88internal8compiler22MachineOperatorReducer15ReduceWord32ShlEPNS1_4NodeE:
.LFB19925:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	32(%rsi), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %rsi
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L2100
	movq	(%rsi), %rcx
	movzwl	16(%rcx), %edx
	cmpw	$23, %dx
	sete	%dil
	je	.L2101
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
.L2102:
	leaq	8(%r13), %rdx
	movq	%rsi, %r14
	movq	%r13, %rcx
.L2105:
	movq	(%rdx), %r15
	movq	(%r15), %rdx
	cmpw	$23, 16(%rdx)
	jne	.L2106
	movl	44(%rdx), %ecx
	testl	%ecx, %ecx
	je	.L2107
	movl	%ecx, %eax
	testb	%dil, %dil
	je	.L2184
	movl	%r8d, %esi
	movq	16(%r12), %rdi
	sall	%cl, %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
.L2243:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2101:
	.cfi_restore_state
	movl	44(%rcx), %r8d
	movl	$1, %r9d
	jmp	.L2102
	.p2align 4,,10
	.p2align 3
.L2106:
	movq	(%rbx), %rdx
	testb	$1, 18(%rdx)
	je	.L2117
	testb	%r9b, %r9b
	jne	.L2247
.L2117:
	movq	16(%r12), %rax
	movq	16(%rax), %rax
	testb	$8, 21(%rax)
	jne	.L2248
.L2146:
	xorl	%ebx, %ebx
.L2180:
	addq	$56, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2248:
	.cfi_restore_state
	movzbl	23(%rbx), %eax
	movq	0(%r13), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L2147
	movq	(%rdx), %rcx
	movq	%rdx, %r14
	movq	%r13, %r15
	cmpw	$23, 16(%rcx)
	leaq	8(%r13), %rcx
	sete	%dil
.L2148:
	movq	(%rcx), %r12
	movq	(%rbx), %r8
	movq	(%r12), %rsi
	movzwl	16(%rsi), %ecx
	testb	%dil, 18(%r8)
	je	.L2150
	cmpw	$23, %cx
	jne	.L2249
.L2150:
	cmpw	$299, %cx
	jne	.L2146
	movzbl	23(%r12), %eax
	movq	32(%r12), %rdx
	leaq	32(%r12), %r14
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L2160
	movq	(%rdx), %rcx
	cmpw	$23, 16(%rcx)
	je	.L2161
	xorl	%r9d, %r9d
	xorl	%edi, %edi
.L2162:
	leaq	8(%r14), %rcx
	movq	%rdx, %r8
	movq	%r14, %r10
.L2165:
	movq	(%rcx), %r15
	movq	(%r15), %rcx
	cmpw	$23, 16(%rcx)
	jne	.L2166
	movl	44(%rcx), %r9d
.L2167:
	cmpl	$31, %r9d
	jne	.L2146
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L2176
	movq	8(%r13), %rdi
	cmpq	%r8, %rdi
	je	.L2180
	addq	$8, %r13
	movq	%rbx, %rax
.L2178:
	leaq	-48(%rax), %r12
	testq	%rdi, %rdi
	je	.L2179
	movq	%r12, %rsi
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-56(%rbp), %r8
.L2179:
	movq	%r8, 0(%r13)
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	jmp	.L2180
	.p2align 4,,10
	.p2align 3
.L2100:
	movq	16(%rsi), %r14
	leaq	16(%rsi), %rcx
	movq	(%r14), %r8
	movzwl	16(%r8), %edx
	cmpw	$23, %dx
	sete	%dil
	je	.L2103
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
.L2104:
	leaq	8(%rcx), %rdx
	jmp	.L2105
	.p2align 4,,10
	.p2align 3
.L2110:
	cmpq	%r14, %r15
	je	.L2113
.L2111:
	subq	$24, %rsi
	movq	%r14, %rdi
	movl	%r8d, -72(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-64(%rbp), %rcx
	movq	-56(%rbp), %rsi
	movq	%r15, %rdi
	movq	%r15, (%rcx)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movzbl	23(%rbx), %eax
	movl	-72(%rbp), %r8d
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L2250
.L2112:
	movq	8(%r13), %rdi
	cmpq	%r14, %rdi
	je	.L2185
	leaq	8(%r13), %rax
	movq	%rbx, %rsi
.L2115:
	subq	$48, %rsi
	testq	%rdi, %rdi
	je	.L2116
	movl	%r8d, -72(%rbp)
	movq	%rax, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %rax
	movl	-72(%rbp), %r8d
.L2116:
	movq	%r14, (%rax)
	movq	%r14, %rdi
	movl	%r8d, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movl	-56(%rbp), %r8d
.L2185:
	movl	%r8d, %eax
	movq	%r15, %r14
	movl	%r8d, %ecx
	testl	%r8d, %r8d
	je	.L2251
.L2184:
	subl	$1, %eax
	cmpl	$30, %eax
	ja	.L2117
	movq	(%r14), %rax
	movzwl	16(%rax), %edi
	leal	-303(%rdi), %edx
	cmpw	$1, %dx
	ja	.L2117
	movzbl	23(%r14), %edx
	movq	32(%r14), %rsi
	leaq	32(%r14), %r15
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L2119
	movq	(%rsi), %rdi
	cmpw	$23, 16(%rdi)
	je	.L2120
	movb	$0, -56(%rbp)
	xorl	%r10d, %r10d
.L2121:
	leaq	8(%r15), %rdi
	movq	%rsi, %r9
	movq	%r15, %r11
.L2124:
	movq	(%rdi), %r8
	movq	(%r8), %rdi
	cmpw	$23, 16(%rdi)
	jne	.L2125
	movl	44(%rdi), %r10d
.L2126:
	cmpl	%ecx, %r10d
	jne	.L2117
	movzbl	23(%rbx), %eax
	movq	0(%r13), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L2135
	movq	%r13, %r15
	movq	%rbx, %r14
	cmpq	%rdi, %r9
	je	.L2137
.L2136:
	subq	$24, %r14
	testq	%rdi, %rdi
	je	.L2138
	movq	%r14, %rsi
	movq	%r9, -64(%rbp)
	movl	%ecx, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-64(%rbp), %r9
	movl	-56(%rbp), %ecx
.L2138:
	movq	%r9, (%r15)
	movq	%r14, %rsi
	movq	%r9, %rdi
	movl	%ecx, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movl	-56(%rbp), %ecx
.L2137:
	movq	16(%r12), %rdi
	movl	$-1, %esi
	sall	%cl, %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%rax, %r14
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L2139
	movq	8(%r13), %rdi
	cmpq	%r14, %rdi
	je	.L2142
	addq	$8, %r13
	movq	%rbx, %rax
.L2141:
	leaq	-48(%rax), %r15
	testq	%rdi, %rdi
	je	.L2143
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L2143:
	movq	%r14, 0(%r13)
	testq	%r14, %r14
	je	.L2142
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L2142:
	movq	16(%r12), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32AndEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22MachineOperatorReducer15ReduceWord32AndEPNS1_4NodeE
	testq	%rax, %rax
	cmove	%rbx, %rax
	jmp	.L2243
	.p2align 4,,10
	.p2align 3
.L2103:
	movl	44(%r8), %r8d
	movl	$1, %r9d
	jmp	.L2104
	.p2align 4,,10
	.p2align 3
.L2147:
	movq	16(%rdx), %r14
	leaq	16(%rdx), %r15
	movq	(%r14), %rcx
	cmpw	$23, 16(%rcx)
	leaq	24(%rdx), %rcx
	sete	%dil
	jmp	.L2148
.L2251:
	movq	%r15, %r14
.L2107:
	addq	$56, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2247:
	.cfi_restore_state
	cmpl	$15, %eax
	je	.L2110
	movq	%rbx, %rsi
	cmpq	%r14, %r15
	jne	.L2111
	jmp	.L2112
	.p2align 4,,10
	.p2align 3
.L2250:
	movq	0(%r13), %rsi
.L2113:
	movq	24(%rsi), %rdi
	cmpq	%r14, %rdi
	je	.L2185
	leaq	24(%rsi), %rax
	jmp	.L2115
	.p2align 4,,10
	.p2align 3
.L2166:
	testb	$1, 18(%rsi)
	je	.L2146
	testb	%dil, %dil
	je	.L2146
	cmpl	$15, %eax
	je	.L2169
	movq	%r12, %rdx
	cmpq	%r8, %r15
	je	.L2171
.L2170:
	leaq	-24(%rdx), %rsi
	movq	%r8, %rdi
	movl	%r9d, -80(%rbp)
	movq	%r8, -56(%rbp)
	movq	%r10, -72(%rbp)
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-72(%rbp), %r10
	movq	-64(%rbp), %rsi
	movq	%r15, %rdi
	movq	%r15, (%r10)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movzbl	23(%r12), %eax
	movq	-56(%rbp), %r8
	movl	-80(%rbp), %r9d
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L2172
.L2171:
	movq	8(%r14), %rdi
	addq	$8, %r14
	cmpq	%r8, %rdi
	je	.L2181
.L2174:
	subq	$48, %r12
	testq	%rdi, %rdi
	je	.L2175
	movq	%r12, %rsi
	movl	%r9d, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-56(%rbp), %r8
	movl	-64(%rbp), %r9d
.L2175:
	movq	%r8, (%r14)
	movq	%r12, %rsi
	movq	%r8, %rdi
	movl	%r9d, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movl	-56(%rbp), %r9d
.L2181:
	movq	%r15, %r8
	jmp	.L2167
	.p2align 4,,10
	.p2align 3
.L2125:
	testb	$1, 18(%rax)
	je	.L2117
	cmpb	$0, -56(%rbp)
	je	.L2117
	cmpl	$15, %edx
	je	.L2128
	movq	%r14, %rsi
	cmpq	%r9, %r8
	je	.L2130
.L2129:
	subq	$24, %rsi
	movq	%r9, %rdi
	movl	%r10d, -92(%rbp)
	movl	%ecx, -72(%rbp)
	movq	%r9, -64(%rbp)
	movq	%r8, -88(%rbp)
	movq	%r11, -80(%rbp)
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-88(%rbp), %r8
	movq	-80(%rbp), %r11
	movq	-56(%rbp), %rsi
	movq	%r8, (%r11)
	movq	%r8, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movzbl	23(%r14), %eax
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %r9
	movl	-72(%rbp), %ecx
	andl	$15, %eax
	movl	-92(%rbp), %r10d
	cmpl	$15, %eax
	je	.L2131
.L2130:
	movq	8(%r15), %rdi
	addq	$8, %r15
	cmpq	%r9, %rdi
	je	.L2182
.L2133:
	subq	$48, %r14
	testq	%rdi, %rdi
	je	.L2134
	movq	%r14, %rsi
	movl	%r10d, -80(%rbp)
	movq	%r9, -72(%rbp)
	movq	%r8, -64(%rbp)
	movl	%ecx, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movl	-56(%rbp), %ecx
	movq	-64(%rbp), %r8
	movq	-72(%rbp), %r9
	movl	-80(%rbp), %r10d
.L2134:
	movq	%r9, (%r15)
	movq	%r14, %rsi
	movq	%r9, %rdi
	movl	%r10d, -72(%rbp)
	movq	%r8, -64(%rbp)
	movl	%ecx, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movl	-56(%rbp), %ecx
	movq	-64(%rbp), %r8
	movl	-72(%rbp), %r10d
.L2182:
	movq	%r8, %r9
	jmp	.L2126
	.p2align 4,,10
	.p2align 3
.L2249:
	cmpl	$15, %eax
	je	.L2151
	movq	%rbx, %rdx
	cmpq	%r14, %r12
	je	.L2153
.L2152:
	leaq	-24(%rdx), %rsi
	movq	%r14, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	%r12, (%r15)
	movq	-56(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L2252
.L2153:
	movq	8(%r13), %rdi
	leaq	8(%r13), %r15
	movq	%rbx, %rdx
	cmpq	%r14, %rdi
	je	.L2246
.L2157:
	leaq	-48(%rdx), %r12
	testq	%rdi, %rdi
	je	.L2158
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L2158:
	movq	%r14, (%r15)
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L2246:
	movq	(%r14), %rsi
	movq	%r14, %r12
	movzwl	16(%rsi), %ecx
	jmp	.L2150
	.p2align 4,,10
	.p2align 3
.L2160:
	movq	16(%rdx), %r8
	leaq	16(%rdx), %r10
	movq	(%r8), %rcx
	cmpw	$23, 16(%rcx)
	je	.L2163
	xorl	%r9d, %r9d
	xorl	%edi, %edi
.L2164:
	leaq	8(%r10), %rcx
	jmp	.L2165
	.p2align 4,,10
	.p2align 3
.L2161:
	movl	44(%rcx), %r9d
	movl	$1, %edi
	jmp	.L2162
	.p2align 4,,10
	.p2align 3
.L2119:
	movq	16(%rsi), %r9
	leaq	16(%rsi), %r11
	movq	(%r9), %rdi
	cmpw	$23, 16(%rdi)
	je	.L2122
	movb	$0, -56(%rbp)
	xorl	%r10d, %r10d
.L2123:
	leaq	8(%r11), %rdi
	jmp	.L2124
	.p2align 4,,10
	.p2align 3
.L2120:
	movb	$1, -56(%rbp)
	movl	44(%rdi), %r10d
	jmp	.L2121
.L2163:
	movl	44(%rcx), %r9d
	movl	$1, %edi
	jmp	.L2164
.L2122:
	movb	$1, -56(%rbp)
	movl	44(%rdi), %r10d
	jmp	.L2123
.L2151:
	cmpq	%r14, %r12
	jne	.L2152
.L2154:
	movq	24(%rdx), %rdi
	cmpq	%r14, %rdi
	je	.L2246
	leaq	24(%rdx), %r15
	jmp	.L2157
.L2252:
	movq	0(%r13), %rdx
	jmp	.L2154
.L2176:
	movq	0(%r13), %rax
	movq	24(%rax), %rdi
	cmpq	%r8, %rdi
	je	.L2180
	leaq	24(%rax), %r13
	jmp	.L2178
.L2135:
	movq	16(%rdi), %rax
	leaq	16(%rdi), %r15
	cmpq	%r9, %rax
	je	.L2137
	movq	%rdi, %r14
	movq	%rax, %rdi
	jmp	.L2136
.L2139:
	movq	0(%r13), %rax
	movq	24(%rax), %rdi
	cmpq	%rdi, %r14
	je	.L2142
	leaq	24(%rax), %r13
	jmp	.L2141
.L2169:
	cmpq	%r8, %r15
	jne	.L2170
.L2172:
	movq	(%r14), %r12
	movq	24(%r12), %rdi
	cmpq	%r8, %rdi
	je	.L2181
	leaq	24(%r12), %r14
	jmp	.L2174
.L2128:
	cmpq	%r9, %r8
	jne	.L2129
.L2131:
	movq	(%r15), %r14
	movq	24(%r14), %rdi
	cmpq	%r9, %rdi
	je	.L2182
	leaq	24(%r14), %r15
	jmp	.L2133
	.cfi_endproc
.LFE19925:
	.size	_ZN2v88internal8compiler22MachineOperatorReducer15ReduceWord32ShlEPNS1_4NodeE, .-_ZN2v88internal8compiler22MachineOperatorReducer15ReduceWord32ShlEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler22MachineOperatorReducer14ReduceInt32ModEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22MachineOperatorReducer14ReduceInt32ModEPNS1_4NodeE
	.type	_ZN2v88internal8compiler22MachineOperatorReducer14ReduceInt32ModEPNS1_4NodeE, @function
_ZN2v88internal8compiler22MachineOperatorReducer14ReduceInt32ModEPNS1_4NodeE:
.LFB19920:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	32(%rsi), %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$104, %rsp
	movzbl	23(%rsi), %edx
	movq	32(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L2254
	movq	(%rsi), %rcx
	movzwl	16(%rcx), %eax
	cmpw	$23, %ax
	sete	%r10b
	je	.L2255
	xorl	%r8d, %r8d
	xorl	%r11d, %r11d
.L2256:
	leaq	8(%r15), %rax
	movq	%rsi, %r12
	movq	%r15, %r9
.L2259:
	movq	(%rax), %r14
	movq	(%r14), %rcx
	movzwl	16(%rcx), %edi
	cmpw	$23, %di
	jne	.L2260
	movl	44(%rcx), %esi
	testb	%r10b, %r10b
	je	.L2262
.L2261:
	movq	%r12, %rax
	testl	%r8d, %r8d
	je	.L2272
	cmpw	$23, %di
	je	.L2323
	cmpq	%r12, %r14
	je	.L2275
.L2326:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L2272:
	movq	-56(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L2324
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2255:
	.cfi_restore_state
	movl	44(%rcx), %r8d
	movl	$1, %r11d
	jmp	.L2256
	.p2align 4,,10
	.p2align 3
.L2260:
	movq	(%rbx), %rax
	testb	$1, 18(%rax)
	je	.L2263
	testb	%r11b, %r11b
	jne	.L2325
.L2263:
	xorl	%esi, %esi
	testb	%r10b, %r10b
	jne	.L2261
	cmpq	%r12, %r14
	jne	.L2326
.L2275:
	movq	16(%r13), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	jmp	.L2272
	.p2align 4,,10
	.p2align 3
.L2323:
	movl	$1, %r10d
.L2262:
	movq	%r14, %rax
	testl	%esi, %esi
	je	.L2272
	leal	1(%rsi), %eax
	andl	$-3, %eax
	je	.L2275
	cmpq	%r14, %r12
	je	.L2275
	testb	%r10b, %r10b
	je	.L2276
	movl	%r8d, %edi
	call	_ZN2v84base4bits11SignedMod32Eii@PLT
	movq	16(%r13), %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	jmp	.L2272
	.p2align 4,,10
	.p2align 3
.L2254:
	movq	16(%rsi), %r12
	leaq	16(%rsi), %r9
	movq	(%r12), %rcx
	movzwl	16(%rcx), %eax
	cmpw	$23, %ax
	sete	%r10b
	je	.L2257
	xorl	%r8d, %r8d
	xorl	%r11d, %r11d
.L2258:
	leaq	8(%r9), %rax
	jmp	.L2259
	.p2align 4,,10
	.p2align 3
.L2257:
	movl	44(%rcx), %r8d
	movl	$1, %r11d
	jmp	.L2258
	.p2align 4,,10
	.p2align 3
.L2276:
	movl	%esi, %eax
	sarl	$31, %eax
	xorl	%eax, %esi
	subl	%eax, %esi
	movl	%esi, %r14d
	je	.L2277
	leal	-1(%rsi), %r9d
	testl	%esi, %r9d
	je	.L2278
.L2277:
	movl	%r14d, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler22MachineOperatorReducer8Int32DivEPNS1_4NodeEi
	movq	16(%r13), %rdi
	movl	%r14d, %esi
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%rax, -112(%rbp)
	movq	16(%r13), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Int32MulEv@PLT
	movq	%r12, %rdi
	leaq	-80(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	-88(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movhps	-112(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r12
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L2279
	movq	8(%r15), %rdi
	cmpq	%r12, %rdi
	je	.L2282
	addq	$8, %r15
	movq	%rbx, %rsi
.L2281:
	leaq	-48(%rsi), %r14
	testq	%rdi, %rdi
	je	.L2283
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L2283:
	movq	%r12, (%r15)
	testq	%r12, %r12
	je	.L2282
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L2282:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler4Node14TrimInputCountEi@PLT
	movq	16(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Int32SubEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	%rbx, %rax
	jmp	.L2272
	.p2align 4,,10
	.p2align 3
.L2325:
	cmpl	$15, %edx
	je	.L2264
	movq	%rbx, %rsi
	cmpq	%r12, %r14
	je	.L2266
.L2265:
	subq	$24, %rsi
	movq	%r12, %rdi
	movl	%r8d, -96(%rbp)
	movq	%r9, -112(%rbp)
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-112(%rbp), %r9
	movq	-88(%rbp), %rsi
	movq	%r14, %rdi
	movq	%r14, (%r9)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movzbl	23(%rbx), %eax
	movl	-96(%rbp), %r8d
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L2327
.L2266:
	movq	8(%r15), %rdi
	cmpq	%r12, %rdi
	je	.L2288
	leaq	8(%r15), %rax
	movq	%rbx, %rsi
.L2269:
	subq	$48, %rsi
	testq	%rdi, %rdi
	je	.L2270
	movl	%r8d, -96(%rbp)
	movq	%rax, -112(%rbp)
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-88(%rbp), %rsi
	movq	-112(%rbp), %rax
	movl	-96(%rbp), %r8d
.L2270:
	movq	%r12, (%rax)
	movq	%r12, %rdi
	movl	%r8d, -88(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movl	-88(%rbp), %r8d
.L2288:
	movq	%r12, %rax
	movl	%r8d, %esi
	movq	%r14, %r12
	xorl	%r8d, %r8d
	movq	%rax, %r14
	xorl	%r10d, %r10d
	jmp	.L2262
	.p2align 4,,10
	.p2align 3
.L2278:
	movq	16(%r13), %rdi
	xorl	%esi, %esi
	movl	%r9d, -132(%rbp)
	leaq	-80(%rbp), %r14
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%rax, -88(%rbp)
	movq	16(%r13), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler22MachineOperatorBuilder13Int32LessThanEv@PLT
	movq	%r12, %xmm0
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movhps	-88(%rbp), %xmm0
	movq	%r15, %rdi
	movq	%rax, %rsi
	movl	$2, %edx
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$1, %edx
	movl	$2, %esi
	movq	%rax, -112(%rbp)
	movq	16(%r13), %rax
	movq	(%rax), %r15
	movq	8(%rax), %r10
	movq	8(%r15), %rbx
	movq	%r10, %rdi
	movq	%r10, -96(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$2, %edx
	movq	-112(%rbp), %xmm0
	movq	%rbx, %xmm1
	movq	%rax, %rsi
	movq	%r15, %rdi
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-96(%rbp), %r10
	movq	%rax, %rbx
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r15, %rdi
	movq	%rbx, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-96(%rbp), %r10
	movq	%rax, -112(%rbp)
	movq	%r10, %rdi
	movq	%r10, -120(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$1, %edx
	movq	%r15, %rdi
	movq	%rax, %rsi
	movq	%rbx, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-120(%rbp), %r10
	movl	$2, %esi
	movq	%rax, -96(%rbp)
	movq	%r10, %rdi
	movq	%r10, -128(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$2, %edx
	movq	-112(%rbp), %xmm0
	movq	%r15, %rdi
	movq	%rax, %rsi
	movhps	-96(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	-132(%rbp), %r9d
	movq	16(%r13), %rdi
	movq	%rax, -120(%rbp)
	movl	%r9d, %esi
	movl	%r9d, -96(%rbp)
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%rax, -112(%rbp)
	movq	16(%r13), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32AndEv@PLT
	movq	%r12, %xmm0
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movhps	-112(%rbp), %xmm0
	movl	$2, %edx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorReducer15ReduceWord32AndEPNS1_4NodeE
	movq	-88(%rbp), %xmm0
	movq	%r12, %xmm2
	movq	-112(%rbp), %rdx
	testq	%rax, %rax
	movq	%rax, %rbx
	movq	16(%r13), %rax
	punpcklqdq	%xmm2, %xmm0
	cmove	%rdx, %rbx
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Int32SubEv@PLT
	movdqa	-112(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$2, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorReducer14ReduceInt32SubEPNS1_4NodeE
	movl	-96(%rbp), %r9d
	movq	-112(%rbp), %rdx
	movq	16(%r13), %rdi
	testq	%rax, %rax
	movq	%rax, %r12
	movl	%r9d, %esi
	cmove	%rdx, %r12
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%rax, -112(%rbp)
	movq	16(%r13), %rax
	movq	(%rax), %r9
	movq	16(%rax), %rdi
	movq	%r9, -96(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32AndEv@PLT
	movq	-96(%rbp), %r9
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	%r12, %xmm0
	movl	$2, %edx
	movq	%rax, %rsi
	movhps	-112(%rbp), %xmm0
	movq	%r9, %rdi
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorReducer15ReduceWord32AndEPNS1_4NodeE
	movq	-112(%rbp), %rdx
	testq	%rax, %rax
	movq	%rax, %r12
	movq	16(%r13), %rax
	cmove	%rdx, %r12
	movq	(%rax), %r9
	movq	16(%rax), %rdi
	movq	%r9, -112(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Int32SubEv@PLT
	movq	-112(%rbp), %r9
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	-88(%rbp), %xmm0
	movq	%r12, %xmm3
	movl	$2, %edx
	movq	%rax, %rsi
	movq	%r9, %rdi
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorReducer14ReduceInt32SubEPNS1_4NodeE
	movq	-88(%rbp), %rdx
	movq	-128(%rbp), %r10
	movl	$4, %esi
	testq	%rax, %rax
	movq	%rax, %r12
	cmove	%rdx, %r12
	movq	%r10, %rdi
	movl	$2, %edx
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	movq	-120(%rbp), %r11
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	%r12, %xmm0
	movq	%rbx, %xmm4
	movq	%rax, %rsi
	movq	%r15, %rdi
	punpcklqdq	%xmm4, %xmm0
	movl	$3, %edx
	movq	%r11, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	jmp	.L2272
	.p2align 4,,10
	.p2align 3
.L2327:
	movq	32(%rbx), %rsi
.L2267:
	movq	24(%rsi), %rdi
	cmpq	%rdi, %r12
	je	.L2288
	leaq	24(%rsi), %rax
	jmp	.L2269
	.p2align 4,,10
	.p2align 3
.L2279:
	movq	32(%rbx), %rsi
	movq	24(%rsi), %rdi
	cmpq	%r12, %rdi
	je	.L2282
	leaq	24(%rsi), %r15
	jmp	.L2281
	.p2align 4,,10
	.p2align 3
.L2264:
	cmpq	%r12, %r14
	jne	.L2265
	jmp	.L2267
.L2324:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19920:
	.size	_ZN2v88internal8compiler22MachineOperatorReducer14ReduceInt32ModEPNS1_4NodeE, .-_ZN2v88internal8compiler22MachineOperatorReducer14ReduceInt32ModEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES6_EC2EPNS1_4NodeE,"axG",@progbits,_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES6_EC5EPNS1_4NodeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES6_EC2EPNS1_4NodeE
	.type	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES6_EC2EPNS1_4NodeE, @function
_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES6_EC2EPNS1_4NodeE:
.LFB21904:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	leaq	32(%rsi), %r12
	pushq	%rbx
	movq	%r12, %rdx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	%rsi, (%rdi)
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2329
	movq	32(%rsi), %rax
	leaq	16(%rax), %rdx
.L2329:
	movq	(%rdx), %rax
	movq	$0, 16(%rbx)
	movb	$0, 24(%rbx)
	movq	%rax, 8(%rbx)
	movq	(%rax), %rdx
	movzwl	16(%rdx), %eax
	cmpl	$23, %eax
	je	.L2362
	cmpl	$24, %eax
	je	.L2363
.L2331:
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L2332
.L2367:
	leaq	8(%r12), %rax
.L2333:
	movq	(%rax), %r13
	movq	$0, 40(%rbx)
	movb	$0, 48(%rbx)
	movq	%r13, 32(%rbx)
	movq	0(%r13), %rdx
	movzwl	16(%rdx), %eax
	cmpl	$23, %eax
	je	.L2364
	xorl	%ecx, %ecx
	cmpl	$24, %eax
	je	.L2365
.L2335:
	movq	(%rsi), %rax
	testb	$1, 18(%rax)
	je	.L2328
	cmpb	$0, 24(%rbx)
	je	.L2328
	testb	%cl, %cl
	je	.L2366
.L2328:
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2365:
	.cfi_restore_state
	movq	48(%rdx), %rax
	movb	$1, 48(%rbx)
	movl	$1, %ecx
	movq	%rax, 40(%rbx)
	jmp	.L2335
	.p2align 4,,10
	.p2align 3
.L2363:
	movq	48(%rdx), %rax
	movb	$1, 24(%rbx)
	movq	%rax, 16(%rbx)
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2367
.L2332:
	movq	(%r12), %rax
	addq	$24, %rax
	jmp	.L2333
	.p2align 4,,10
	.p2align 3
.L2362:
	movslq	44(%rdx), %rax
	movb	$1, 24(%rbx)
	movq	%rax, 16(%rbx)
	jmp	.L2331
	.p2align 4,,10
	.p2align 3
.L2364:
	movslq	44(%rdx), %rax
	movb	$1, 48(%rbx)
	movl	$1, %ecx
	movq	%rax, 40(%rbx)
	jmp	.L2335
	.p2align 4,,10
	.p2align 3
.L2366:
	movq	24(%rbx), %rax
	movzbl	48(%rbx), %edx
	movdqu	8(%rbx), %xmm0
	movdqu	32(%rbx), %xmm1
	movq	%rax, -48(%rbp)
	movq	(%r12), %rdi
	movb	%al, 48(%rbx)
	movzbl	23(%rsi), %eax
	movb	%dl, 24(%rbx)
	andl	$15, %eax
	movaps	%xmm0, -64(%rbp)
	movups	%xmm1, 8(%rbx)
	movups	%xmm0, 32(%rbx)
	cmpl	$15, %eax
	je	.L2339
	cmpq	%rdi, %r13
	je	.L2368
.L2340:
	leaq	-24(%rsi), %r14
	testq	%rdi, %rdi
	je	.L2343
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L2343:
	movq	%r13, (%r12)
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movq	(%rbx), %rsi
	movq	32(%rbx), %r13
	movzbl	23(%rsi), %eax
	leaq	32(%rsi), %r12
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L2347
.L2346:
	movq	8(%r12), %rdi
	addq	$8, %r12
	cmpq	%rdi, %r13
	je	.L2328
.L2344:
	leaq	-48(%rsi), %r14
	testq	%rdi, %rdi
	je	.L2345
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L2345:
	movq	%r13, (%r12)
	testq	%r13, %r13
	je	.L2328
	addq	$32, %rsp
	movq	%r14, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L2342:
	.cfi_restore_state
	movq	32(%rbx), %r13
.L2347:
	movq	(%r12), %rsi
	movq	24(%rsi), %rdi
	cmpq	%rdi, %r13
	je	.L2328
	leaq	24(%rsi), %r12
	jmp	.L2344
	.p2align 4,,10
	.p2align 3
.L2368:
	movq	32(%rbx), %r13
	jmp	.L2346
	.p2align 4,,10
	.p2align 3
.L2339:
	movq	16(%rdi), %rax
	leaq	16(%rdi), %rdx
	cmpq	%rax, %r13
	je	.L2342
	movq	%rdi, %rsi
	movq	%rdx, %r12
	movq	%rax, %rdi
	jmp	.L2340
	.cfi_endproc
.LFE21904:
	.size	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES6_EC2EPNS1_4NodeE, .-_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES6_EC2EPNS1_4NodeE
	.weak	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES6_EC1EPNS1_4NodeE
	.set	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES6_EC1EPNS1_4NodeE,_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES6_EC2EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler22MachineOperatorReducer14ReduceInt64AddEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22MachineOperatorReducer14ReduceInt64AddEPNS1_4NodeE
	.type	_ZN2v88internal8compiler22MachineOperatorReducer14ReduceInt64AddEPNS1_4NodeE, @function
_ZN2v88internal8compiler22MachineOperatorReducer14ReduceInt64AddEPNS1_4NodeE:
.LFB19914:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	leaq	-176(%rbp), %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$136, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES6_EC1EPNS1_4NodeE
	cmpb	$0, -128(%rbp)
	je	.L2370
	movq	-136(%rbp), %rsi
	movq	-168(%rbp), %rax
	testq	%rsi, %rsi
	je	.L2372
	cmpb	$0, -152(%rbp)
	je	.L2416
	movq	16(%r12), %rax
	addq	-160(%rbp), %rsi
	movq	8(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder13Int64ConstantEl@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
.L2372:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L2417
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2416:
	.cfi_restore_state
	movq	-168(%rbp), %rsi
	movq	(%rsi), %rax
	cmpw	$325, 16(%rax)
	je	.L2418
.L2370:
	xorl	%eax, %eax
	jmp	.L2372
	.p2align 4,,10
	.p2align 3
.L2418:
	leaq	-112(%rbp), %rdi
	call	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES6_EC1EPNS1_4NodeE
	cmpb	$0, -64(%rbp)
	je	.L2370
	movq	-168(%rbp), %rax
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L2374
	movq	-176(%rbp), %rsi
	jmp	.L2376
	.p2align 4,,10
	.p2align 3
.L2419:
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L2374
.L2376:
	movl	16(%rax), %ecx
	movl	%ecx, %edx
	shrl	%edx
	andl	$1, %ecx
	leaq	3(%rdx,%rdx,2), %rdx
	leaq	(%rax,%rdx,8), %rdx
	jne	.L2375
	movq	(%rdx), %rdx
.L2375:
	cmpq	%rdx, %rsi
	je	.L2419
	xorl	%eax, %eax
	jmp	.L2372
	.p2align 4,,10
	.p2align 3
.L2374:
	movq	16(%r12), %rax
	movq	-72(%rbp), %rsi
	leaq	32(%rbx), %r14
	addq	-136(%rbp), %rsi
	movq	8(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder13Int64ConstantEl@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r13
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L2420
	movq	8(%r14), %rdi
	cmpq	%rdi, %r13
	je	.L2377
	leaq	40(%rbx), %r15
	movq	%rbx, %rdx
.L2378:
	leaq	-48(%rdx), %r12
	testq	%rdi, %rdi
	je	.L2380
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L2380:
	movq	%r13, (%r15)
	testq	%r13, %r13
	je	.L2381
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L2381:
	movzbl	23(%rbx), %eax
	movq	-104(%rbp), %r13
	movq	32(%rbx), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L2382
.L2391:
	movq	%rbx, %rdx
	cmpq	%rdi, %r13
	je	.L2384
.L2383:
	leaq	-24(%rdx), %r12
	testq	%rdi, %rdi
	je	.L2386
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L2386:
	movq	%r13, (%r14)
	testq	%r13, %r13
	je	.L2384
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L2384:
	movq	%rbx, %rax
	jmp	.L2372
.L2420:
	movq	32(%rbx), %rdx
	movq	24(%rdx), %rdi
	leaq	16(%rdx), %rax
	cmpq	%rdi, %r13
	je	.L2379
	leaq	24(%rdx), %r15
	jmp	.L2378
.L2382:
	leaq	16(%rdi), %r14
	movq	%rdi, %rdx
.L2392:
	movq	(%r14), %rdi
	cmpq	%rdi, %r13
	jne	.L2383
	jmp	.L2384
.L2377:
	movq	-104(%rbp), %r13
	movq	32(%rbx), %rdi
	jmp	.L2391
.L2379:
	movq	-104(%rbp), %r13
	movq	%rax, %r14
	jmp	.L2392
.L2417:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19914:
	.size	_ZN2v88internal8compiler22MachineOperatorReducer14ReduceInt64AddEPNS1_4NodeE, .-_ZN2v88internal8compiler22MachineOperatorReducer14ReduceInt64AddEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler22MachineOperatorReducer14ReduceInt64SubEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22MachineOperatorReducer14ReduceInt64SubEPNS1_4NodeE
	.type	_ZN2v88internal8compiler22MachineOperatorReducer14ReduceInt64SubEPNS1_4NodeE, @function
_ZN2v88internal8compiler22MachineOperatorReducer14ReduceInt64SubEPNS1_4NodeE:
.LFB19916:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	leaq	-112(%rbp), %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES6_EC1EPNS1_4NodeE
	cmpb	$0, -64(%rbp)
	je	.L2422
	movq	-72(%rbp), %rsi
	movq	-104(%rbp), %rax
	testq	%rsi, %rsi
	je	.L2424
	movq	16(%r12), %rax
	cmpb	$0, -88(%rbp)
	movq	(%rax), %r13
	movq	8(%rax), %rdi
	je	.L2451
	movq	-96(%rbp), %rax
	subq	%rsi, %rax
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder13Int64ConstantEl@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
.L2424:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L2452
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2422:
	.cfi_restore_state
	movq	-80(%rbp), %rax
	cmpq	%rax, -104(%rbp)
	je	.L2453
	xorl	%eax, %eax
	jmp	.L2424
	.p2align 4,,10
	.p2align 3
.L2453:
	movq	16(%r12), %rax
.L2435:
	movq	8(%rax), %rdi
	movq	(%rax), %r12
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder13Int64ConstantEl@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	jmp	.L2424
	.p2align 4,,10
	.p2align 3
.L2451:
	movq	-80(%rbp), %rdx
	cmpq	%rdx, -104(%rbp)
	je	.L2435
	movabsq	$-9223372036854775808, %rax
	cmpq	%rax, %rsi
	je	.L2425
	negq	%rsi
.L2425:
	call	_ZN2v88internal8compiler21CommonOperatorBuilder13Int64ConstantEl@PLT
	movq	%r13, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rax, %rsi
	xorl	%edx, %edx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r13
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L2426
	movq	40(%rbx), %rdi
	cmpq	%rdi, %r13
	je	.L2429
	leaq	40(%rbx), %r15
	movq	%rbx, %rsi
.L2428:
	leaq	-48(%rsi), %r14
	testq	%rdi, %rdi
	je	.L2430
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L2430:
	movq	%r13, (%r15)
	testq	%r13, %r13
	je	.L2429
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L2429:
	movq	16(%r12), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Int64AddEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22MachineOperatorReducer14ReduceInt64AddEPNS1_4NodeE
	testq	%rax, %rax
	cmove	%rbx, %rax
	jmp	.L2424
	.p2align 4,,10
	.p2align 3
.L2426:
	movq	32(%rbx), %rsi
	movq	24(%rsi), %rdi
	cmpq	%rdi, %r13
	je	.L2429
	leaq	24(%rsi), %r15
	jmp	.L2428
.L2452:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19916:
	.size	_ZN2v88internal8compiler22MachineOperatorReducer14ReduceInt64SubEPNS1_4NodeE, .-_ZN2v88internal8compiler22MachineOperatorReducer14ReduceInt64SubEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler22MachineOperatorReducer15ReduceWord64ShlEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22MachineOperatorReducer15ReduceWord64ShlEPNS1_4NodeE
	.type	_ZN2v88internal8compiler22MachineOperatorReducer15ReduceWord64ShlEPNS1_4NodeE, @function
_ZN2v88internal8compiler22MachineOperatorReducer15ReduceWord64ShlEPNS1_4NodeE:
.LFB19926:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	leaq	-80(%rbp), %rdi
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES6_EC1EPNS1_4NodeE
	cmpb	$0, -32(%rbp)
	je	.L2455
	movq	-40(%rbp), %rcx
	movq	-72(%rbp), %rax
	testq	%rcx, %rcx
	je	.L2457
	cmpb	$0, -56(%rbp)
	je	.L2455
	movq	16(%rbx), %rax
	movq	-64(%rbp), %rsi
	movq	8(%rax), %rdi
	salq	%cl, %rsi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder13Int64ConstantEl@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
.L2457:
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L2462
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2455:
	.cfi_restore_state
	xorl	%eax, %eax
	jmp	.L2457
.L2462:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19926:
	.size	_ZN2v88internal8compiler22MachineOperatorReducer15ReduceWord64ShlEPNS1_4NodeE, .-_ZN2v88internal8compiler22MachineOperatorReducer15ReduceWord64ShlEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler22MachineOperatorReducer15ReduceWord64SarEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22MachineOperatorReducer15ReduceWord64SarEPNS1_4NodeE
	.type	_ZN2v88internal8compiler22MachineOperatorReducer15ReduceWord64SarEPNS1_4NodeE, @function
_ZN2v88internal8compiler22MachineOperatorReducer15ReduceWord64SarEPNS1_4NodeE:
.LFB19930:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	leaq	-80(%rbp), %rdi
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES6_EC1EPNS1_4NodeE
	cmpb	$0, -32(%rbp)
	je	.L2464
	movq	-40(%rbp), %rcx
	movq	-72(%rbp), %rax
	testq	%rcx, %rcx
	je	.L2466
	cmpb	$0, -56(%rbp)
	je	.L2464
	movq	16(%rbx), %rax
	movq	-64(%rbp), %rsi
	movq	8(%rax), %rdi
	sarq	%cl, %rsi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder13Int64ConstantEl@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
.L2466:
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L2471
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2464:
	.cfi_restore_state
	xorl	%eax, %eax
	jmp	.L2466
.L2471:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19930:
	.size	_ZN2v88internal8compiler22MachineOperatorReducer15ReduceWord64SarEPNS1_4NodeE, .-_ZN2v88internal8compiler22MachineOperatorReducer15ReduceWord64SarEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler12BinopMatcherINS1_12FloatMatcherIfLNS1_8IrOpcode5ValueE25EEES6_EC2EPNS1_4NodeE,"axG",@progbits,_ZN2v88internal8compiler12BinopMatcherINS1_12FloatMatcherIfLNS1_8IrOpcode5ValueE25EEES6_EC5EPNS1_4NodeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler12BinopMatcherINS1_12FloatMatcherIfLNS1_8IrOpcode5ValueE25EEES6_EC2EPNS1_4NodeE
	.type	_ZN2v88internal8compiler12BinopMatcherINS1_12FloatMatcherIfLNS1_8IrOpcode5ValueE25EEES6_EC2EPNS1_4NodeE, @function
_ZN2v88internal8compiler12BinopMatcherINS1_12FloatMatcherIfLNS1_8IrOpcode5ValueE25EEES6_EC2EPNS1_4NodeE:
.LFB21928:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	leaq	32(%rsi), %r12
	pushq	%rbx
	movq	%r12, %rdx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	%rsi, (%rdi)
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2473
	movq	32(%rsi), %rax
	leaq	16(%rax), %rdx
.L2473:
	movq	(%rdx), %rax
	movl	$0x00000000, 16(%rbx)
	movq	%rax, 8(%rbx)
	movq	(%rax), %rdx
	movzwl	16(%rdx), %eax
	cmpw	$25, %ax
	sete	%cl
	movb	%cl, 20(%rbx)
	jne	.L2474
	movss	44(%rdx), %xmm0
	movss	%xmm0, 16(%rbx)
.L2474:
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L2475
	leaq	8(%r12), %rax
.L2476:
	movq	(%rax), %r13
	movl	$0x00000000, 32(%rbx)
	movq	%r13, 24(%rbx)
	movq	0(%r13), %rdx
	movzwl	16(%rdx), %eax
	cmpw	$25, %ax
	sete	%dil
	movb	%dil, 36(%rbx)
	jne	.L2477
	movss	44(%rdx), %xmm0
	movss	%xmm0, 32(%rbx)
.L2472:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2477:
	.cfi_restore_state
	movq	(%rsi), %rax
	testb	$1, 18(%rax)
	je	.L2472
	testb	%cl, %cl
	je	.L2472
	movdqu	8(%rbx), %xmm1
	movl	32(%rbx), %edx
	movb	%dil, 20(%rbx)
	movq	8(%rbx), %rax
	movq	(%r12), %rdi
	movq	%r13, 8(%rbx)
	movaps	%xmm1, -48(%rbp)
	movl	%edx, 16(%rbx)
	movl	-40(%rbp), %edx
	movq	%rax, 24(%rbx)
	movl	%edx, 32(%rbx)
	movzbl	-36(%rbp), %edx
	movb	%dl, 36(%rbx)
	movzbl	23(%rsi), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L2481
	cmpq	%rdi, %r13
	je	.L2507
.L2482:
	leaq	-24(%rsi), %r14
	testq	%rdi, %rdi
	je	.L2485
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L2485:
	movq	%r13, (%r12)
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movq	(%rbx), %rsi
	movq	24(%rbx), %r13
	movzbl	23(%rsi), %eax
	leaq	32(%rsi), %r12
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L2490
.L2489:
	movq	8(%r12), %rdi
	addq	$8, %r12
	cmpq	%rdi, %r13
	je	.L2472
.L2487:
	leaq	-48(%rsi), %r14
	testq	%rdi, %rdi
	je	.L2488
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L2488:
	movq	%r13, (%r12)
	testq	%r13, %r13
	je	.L2472
	addq	$16, %rsp
	movq	%r14, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	.p2align 4,,10
	.p2align 3
.L2475:
	.cfi_restore_state
	movq	(%r12), %rax
	addq	$24, %rax
	jmp	.L2476
.L2484:
	movq	%rax, %r13
.L2490:
	movq	(%r12), %rsi
	movq	24(%rsi), %rdi
	cmpq	%rdi, %r13
	je	.L2472
	leaq	24(%rsi), %r12
	jmp	.L2487
	.p2align 4,,10
	.p2align 3
.L2507:
	movq	%rax, %r13
	jmp	.L2489
	.p2align 4,,10
	.p2align 3
.L2481:
	movq	16(%rdi), %rdx
	leaq	16(%rdi), %rcx
	cmpq	%rdx, %r13
	je	.L2484
	movq	%rdi, %rsi
	movq	%rcx, %r12
	movq	%rdx, %rdi
	jmp	.L2482
	.cfi_endproc
.LFE21928:
	.size	_ZN2v88internal8compiler12BinopMatcherINS1_12FloatMatcherIfLNS1_8IrOpcode5ValueE25EEES6_EC2EPNS1_4NodeE, .-_ZN2v88internal8compiler12BinopMatcherINS1_12FloatMatcherIfLNS1_8IrOpcode5ValueE25EEES6_EC2EPNS1_4NodeE
	.weak	_ZN2v88internal8compiler12BinopMatcherINS1_12FloatMatcherIfLNS1_8IrOpcode5ValueE25EEES6_EC1EPNS1_4NodeE
	.set	_ZN2v88internal8compiler12BinopMatcherINS1_12FloatMatcherIfLNS1_8IrOpcode5ValueE25EEES6_EC1EPNS1_4NodeE,_ZN2v88internal8compiler12BinopMatcherINS1_12FloatMatcherIfLNS1_8IrOpcode5ValueE25EEES6_EC2EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler12BinopMatcherINS1_12FloatMatcherIdLNS1_8IrOpcode5ValueE26EEES6_EC2EPNS1_4NodeE,"axG",@progbits,_ZN2v88internal8compiler12BinopMatcherINS1_12FloatMatcherIdLNS1_8IrOpcode5ValueE26EEES6_EC5EPNS1_4NodeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler12BinopMatcherINS1_12FloatMatcherIdLNS1_8IrOpcode5ValueE26EEES6_EC2EPNS1_4NodeE
	.type	_ZN2v88internal8compiler12BinopMatcherINS1_12FloatMatcherIdLNS1_8IrOpcode5ValueE26EEES6_EC2EPNS1_4NodeE, @function
_ZN2v88internal8compiler12BinopMatcherINS1_12FloatMatcherIdLNS1_8IrOpcode5ValueE26EEES6_EC2EPNS1_4NodeE:
.LFB21939:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	leaq	32(%rsi), %r12
	pushq	%rbx
	movq	%r12, %rdx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	%rsi, (%rdi)
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2509
	movq	32(%rsi), %rax
	leaq	16(%rax), %rdx
.L2509:
	movq	(%rdx), %rax
	movq	$0x000000000, 16(%rbx)
	movq	%rax, 8(%rbx)
	movq	(%rax), %rdx
	movzwl	16(%rdx), %eax
	cmpw	$26, %ax
	sete	%cl
	movb	%cl, 24(%rbx)
	jne	.L2510
	movsd	48(%rdx), %xmm0
	movsd	%xmm0, 16(%rbx)
.L2510:
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L2511
	leaq	8(%r12), %rax
.L2512:
	movq	(%rax), %r13
	movq	$0x000000000, 40(%rbx)
	movq	%r13, 32(%rbx)
	movq	0(%r13), %rdx
	movzwl	16(%rdx), %eax
	cmpw	$26, %ax
	sete	%dil
	movb	%dil, 48(%rbx)
	jne	.L2513
	movsd	48(%rdx), %xmm0
	movsd	%xmm0, 40(%rbx)
.L2508:
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2513:
	.cfi_restore_state
	movq	(%rsi), %rax
	testb	$1, 18(%rax)
	je	.L2508
	testb	%cl, %cl
	je	.L2508
	movq	24(%rbx), %rax
	movdqu	8(%rbx), %xmm0
	movb	%dil, 24(%rbx)
	movdqu	32(%rbx), %xmm1
	movq	(%r12), %rdi
	movq	%rax, -48(%rbp)
	movb	%al, 48(%rbx)
	movzbl	23(%rsi), %eax
	movaps	%xmm0, -64(%rbp)
	andl	$15, %eax
	movups	%xmm1, 8(%rbx)
	movups	%xmm0, 32(%rbx)
	cmpl	$15, %eax
	je	.L2517
	cmpq	%rdi, %r13
	je	.L2543
.L2518:
	leaq	-24(%rsi), %r14
	testq	%rdi, %rdi
	je	.L2521
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L2521:
	movq	%r13, (%r12)
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movq	(%rbx), %rsi
	movq	32(%rbx), %r13
	movzbl	23(%rsi), %eax
	leaq	32(%rsi), %r12
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L2526
.L2525:
	movq	8(%r12), %rdi
	addq	$8, %r12
	cmpq	%rdi, %r13
	je	.L2508
.L2523:
	leaq	-48(%rsi), %r14
	testq	%rdi, %rdi
	je	.L2524
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L2524:
	movq	%r13, (%r12)
	testq	%r13, %r13
	je	.L2508
	addq	$32, %rsp
	movq	%r14, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	.p2align 4,,10
	.p2align 3
.L2511:
	.cfi_restore_state
	movq	(%r12), %rax
	addq	$24, %rax
	jmp	.L2512
.L2520:
	movq	32(%rbx), %r13
.L2526:
	movq	(%r12), %rsi
	movq	24(%rsi), %rdi
	cmpq	%rdi, %r13
	je	.L2508
	leaq	24(%rsi), %r12
	jmp	.L2523
	.p2align 4,,10
	.p2align 3
.L2543:
	movq	32(%rbx), %r13
	jmp	.L2525
	.p2align 4,,10
	.p2align 3
.L2517:
	movq	16(%rdi), %rax
	leaq	16(%rdi), %rdx
	cmpq	%rax, %r13
	je	.L2520
	movq	%rdi, %rsi
	movq	%rdx, %r12
	movq	%rax, %rdi
	jmp	.L2518
	.cfi_endproc
.LFE21939:
	.size	_ZN2v88internal8compiler12BinopMatcherINS1_12FloatMatcherIdLNS1_8IrOpcode5ValueE26EEES6_EC2EPNS1_4NodeE, .-_ZN2v88internal8compiler12BinopMatcherINS1_12FloatMatcherIdLNS1_8IrOpcode5ValueE26EEES6_EC2EPNS1_4NodeE
	.weak	_ZN2v88internal8compiler12BinopMatcherINS1_12FloatMatcherIdLNS1_8IrOpcode5ValueE26EEES6_EC1EPNS1_4NodeE
	.set	_ZN2v88internal8compiler12BinopMatcherINS1_12FloatMatcherIdLNS1_8IrOpcode5ValueE26EEES6_EC1EPNS1_4NodeE,_ZN2v88internal8compiler12BinopMatcherINS1_12FloatMatcherIdLNS1_8IrOpcode5ValueE26EEES6_EC2EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherImLNS1_8IrOpcode5ValueE24EEES6_EC2EPNS1_4NodeE,"axG",@progbits,_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherImLNS1_8IrOpcode5ValueE24EEES6_EC5EPNS1_4NodeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherImLNS1_8IrOpcode5ValueE24EEES6_EC2EPNS1_4NodeE
	.type	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherImLNS1_8IrOpcode5ValueE24EEES6_EC2EPNS1_4NodeE, @function
_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherImLNS1_8IrOpcode5ValueE24EEES6_EC2EPNS1_4NodeE:
.LFB21983:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	leaq	32(%rsi), %r12
	pushq	%rbx
	movq	%r12, %rdx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	%rsi, (%rdi)
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2545
	movq	32(%rsi), %rax
	leaq	16(%rax), %rdx
.L2545:
	movq	(%rdx), %rax
	movq	$0, 16(%rbx)
	movb	$0, 24(%rbx)
	movq	%rax, 8(%rbx)
	movq	(%rax), %rdx
	movzwl	16(%rdx), %eax
	cmpl	$23, %eax
	je	.L2578
	cmpl	$24, %eax
	je	.L2579
.L2547:
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L2548
.L2583:
	leaq	8(%r12), %rax
.L2549:
	movq	(%rax), %r13
	movq	$0, 40(%rbx)
	movb	$0, 48(%rbx)
	movq	%r13, 32(%rbx)
	movq	0(%r13), %rdx
	movzwl	16(%rdx), %eax
	cmpl	$23, %eax
	je	.L2580
	xorl	%ecx, %ecx
	cmpl	$24, %eax
	je	.L2581
.L2551:
	movq	(%rsi), %rax
	testb	$1, 18(%rax)
	je	.L2544
	cmpb	$0, 24(%rbx)
	je	.L2544
	testb	%cl, %cl
	je	.L2582
.L2544:
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2581:
	.cfi_restore_state
	movq	48(%rdx), %rax
	movb	$1, 48(%rbx)
	movl	$1, %ecx
	movq	%rax, 40(%rbx)
	jmp	.L2551
	.p2align 4,,10
	.p2align 3
.L2579:
	movq	48(%rdx), %rax
	movb	$1, 24(%rbx)
	movq	%rax, 16(%rbx)
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2583
.L2548:
	movq	(%r12), %rax
	addq	$24, %rax
	jmp	.L2549
	.p2align 4,,10
	.p2align 3
.L2578:
	movl	44(%rdx), %eax
	movb	$1, 24(%rbx)
	movq	%rax, 16(%rbx)
	jmp	.L2547
	.p2align 4,,10
	.p2align 3
.L2580:
	movl	44(%rdx), %eax
	movb	$1, 48(%rbx)
	movl	$1, %ecx
	movq	%rax, 40(%rbx)
	jmp	.L2551
	.p2align 4,,10
	.p2align 3
.L2582:
	movq	24(%rbx), %rax
	movzbl	48(%rbx), %edx
	movdqu	8(%rbx), %xmm0
	movdqu	32(%rbx), %xmm1
	movq	%rax, -48(%rbp)
	movq	(%r12), %rdi
	movb	%al, 48(%rbx)
	movzbl	23(%rsi), %eax
	movb	%dl, 24(%rbx)
	andl	$15, %eax
	movaps	%xmm0, -64(%rbp)
	movups	%xmm1, 8(%rbx)
	movups	%xmm0, 32(%rbx)
	cmpl	$15, %eax
	je	.L2555
	cmpq	%rdi, %r13
	je	.L2584
.L2556:
	leaq	-24(%rsi), %r14
	testq	%rdi, %rdi
	je	.L2559
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L2559:
	movq	%r13, (%r12)
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movq	(%rbx), %rsi
	movq	32(%rbx), %r13
	movzbl	23(%rsi), %eax
	leaq	32(%rsi), %r12
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L2563
.L2562:
	movq	8(%r12), %rdi
	addq	$8, %r12
	cmpq	%rdi, %r13
	je	.L2544
.L2560:
	leaq	-48(%rsi), %r14
	testq	%rdi, %rdi
	je	.L2561
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L2561:
	movq	%r13, (%r12)
	testq	%r13, %r13
	je	.L2544
	addq	$32, %rsp
	movq	%r14, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L2558:
	.cfi_restore_state
	movq	32(%rbx), %r13
.L2563:
	movq	(%r12), %rsi
	movq	24(%rsi), %rdi
	cmpq	%rdi, %r13
	je	.L2544
	leaq	24(%rsi), %r12
	jmp	.L2560
	.p2align 4,,10
	.p2align 3
.L2584:
	movq	32(%rbx), %r13
	jmp	.L2562
	.p2align 4,,10
	.p2align 3
.L2555:
	movq	16(%rdi), %rax
	leaq	16(%rdi), %rdx
	cmpq	%rax, %r13
	je	.L2558
	movq	%rdi, %rsi
	movq	%rdx, %r12
	movq	%rax, %rdi
	jmp	.L2556
	.cfi_endproc
.LFE21983:
	.size	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherImLNS1_8IrOpcode5ValueE24EEES6_EC2EPNS1_4NodeE, .-_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherImLNS1_8IrOpcode5ValueE24EEES6_EC2EPNS1_4NodeE
	.weak	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherImLNS1_8IrOpcode5ValueE24EEES6_EC1EPNS1_4NodeE
	.set	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherImLNS1_8IrOpcode5ValueE24EEES6_EC1EPNS1_4NodeE,_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherImLNS1_8IrOpcode5ValueE24EEES6_EC2EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler22MachineOperatorReducer15ReduceWord64ShrEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22MachineOperatorReducer15ReduceWord64ShrEPNS1_4NodeE
	.type	_ZN2v88internal8compiler22MachineOperatorReducer15ReduceWord64ShrEPNS1_4NodeE, @function
_ZN2v88internal8compiler22MachineOperatorReducer15ReduceWord64ShrEPNS1_4NodeE:
.LFB19928:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	leaq	-80(%rbp), %rdi
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherImLNS1_8IrOpcode5ValueE24EEES6_EC1EPNS1_4NodeE
	cmpb	$0, -32(%rbp)
	je	.L2586
	movq	-40(%rbp), %rcx
	movq	-72(%rbp), %rax
	testq	%rcx, %rcx
	je	.L2588
	cmpb	$0, -56(%rbp)
	je	.L2586
	movq	16(%rbx), %rax
	movq	-64(%rbp), %rsi
	movq	8(%rax), %rdi
	shrq	%cl, %rsi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder13Int64ConstantEl@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
.L2588:
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L2593
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2586:
	.cfi_restore_state
	xorl	%eax, %eax
	jmp	.L2588
.L2593:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19928:
	.size	_ZN2v88internal8compiler22MachineOperatorReducer15ReduceWord64ShrEPNS1_4NodeE, .-_ZN2v88internal8compiler22MachineOperatorReducer15ReduceWord64ShrEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler22MachineOperatorReducer6ReduceEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22MachineOperatorReducer6ReduceEPNS1_4NodeE
	.type	_ZN2v88internal8compiler22MachineOperatorReducer6ReduceEPNS1_4NodeE, @function
_ZN2v88internal8compiler22MachineOperatorReducer6ReduceEPNS1_4NodeE:
.LFB19912:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$200, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzwl	16(%rdi), %eax
	subl	$55, %eax
	cmpw	$440, %ax
	ja	.L2595
	leaq	.L2597(%rip), %rdx
	movzwl	%ax, %eax
	movq	%rsi, %r12
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler22MachineOperatorReducer6ReduceEPNS1_4NodeE,"a",@progbits
	.align 4
	.align 4
.L2597:
	.long	.L2670-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2669-.L2597
	.long	.L2668-.L2597
	.long	.L2667-.L2597
	.long	.L2666-.L2597
	.long	.L2665-.L2597
	.long	.L2664-.L2597
	.long	.L2663-.L2597
	.long	.L2662-.L2597
	.long	.L2595-.L2597
	.long	.L2661-.L2597
	.long	.L2595-.L2597
	.long	.L2660-.L2597
	.long	.L2659-.L2597
	.long	.L2595-.L2597
	.long	.L2658-.L2597
	.long	.L2657-.L2597
	.long	.L2656-.L2597
	.long	.L2655-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2654-.L2597
	.long	.L2653-.L2597
	.long	.L2652-.L2597
	.long	.L2595-.L2597
	.long	.L2651-.L2597
	.long	.L2595-.L2597
	.long	.L2650-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2649-.L2597
	.long	.L2648-.L2597
	.long	.L2647-.L2597
	.long	.L2646-.L2597
	.long	.L2645-.L2597
	.long	.L2644-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2643-.L2597
	.long	.L2643-.L2597
	.long	.L2643-.L2597
	.long	.L2595-.L2597
	.long	.L2642-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2641-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2640-.L2597
	.long	.L2639-.L2597
	.long	.L2638-.L2597
	.long	.L2637-.L2597
	.long	.L2636-.L2597
	.long	.L2635-.L2597
	.long	.L2595-.L2597
	.long	.L2634-.L2597
	.long	.L2633-.L2597
	.long	.L2632-.L2597
	.long	.L2631-.L2597
	.long	.L2630-.L2597
	.long	.L2629-.L2597
	.long	.L2628-.L2597
	.long	.L2627-.L2597
	.long	.L2626-.L2597
	.long	.L2625-.L2597
	.long	.L2624-.L2597
	.long	.L2623-.L2597
	.long	.L2622-.L2597
	.long	.L2621-.L2597
	.long	.L2620-.L2597
	.long	.L2595-.L2597
	.long	.L2619-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2618-.L2597
	.long	.L2617-.L2597
	.long	.L2595-.L2597
	.long	.L2616-.L2597
	.long	.L2615-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2596-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2614-.L2597
	.long	.L2614-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2613-.L2597
	.long	.L2612-.L2597
	.long	.L2611-.L2597
	.long	.L2610-.L2597
	.long	.L2609-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2608-.L2597
	.long	.L2607-.L2597
	.long	.L2606-.L2597
	.long	.L2605-.L2597
	.long	.L2604-.L2597
	.long	.L2603-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2602-.L2597
	.long	.L2601-.L2597
	.long	.L2600-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2599-.L2597
	.long	.L2598-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2595-.L2597
	.long	.L2596-.L2597
	.section	.text._ZN2v88internal8compiler22MachineOperatorReducer6ReduceEPNS1_4NodeE
.L2929:
	testq	%rax, %rax
	jne	.L2930
	.p2align 4,,10
	.p2align 3
.L2595:
	xorl	%eax, %eax
.L2672:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L3315
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2643:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler22MachineOperatorReducer20ReduceFloat64CompareEPNS1_4NodeE
	jmp	.L2672
.L2614:
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %rbx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L3052
	movq	16(%rbx), %rbx
.L3052:
	movq	(%rbx), %rax
	cmpw	$443, 16(%rax)
	jne	.L2595
	movq	8(%r13), %rdi
	movq	%r12, %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	movzbl	23(%rbx), %eax
	leaq	32(%rbx), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L3053
	movq	32(%rbx), %rdx
	addq	$16, %rdx
.L3053:
	movq	(%rdx), %rax
	jmp	.L2672
.L2596:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler22MachineOperatorReducer11ReduceStoreEPNS1_4NodeE
	jmp	.L2672
.L2631:
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2940
	movq	16(%rdx), %rdx
.L2940:
	movq	(%rdx), %rax
	cmpw	$26, 16(%rax)
	jne	.L2595
	movsd	48(%rax), %xmm0
	call	_ZN2v84base7ieee7545asinhEd@PLT
	movq	16(%r13), %rdi
	call	_ZN2v88internal8compiler12MachineGraph15Float64ConstantEd@PLT
	jmp	.L2672
.L2632:
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2939
	movq	16(%rdx), %rdx
.L2939:
	movq	(%rdx), %rax
	cmpw	$26, 16(%rax)
	jne	.L2595
	movsd	48(%rax), %xmm0
	call	_ZN2v84base7ieee7544asinEd@PLT
	movq	16(%r13), %rdi
	call	_ZN2v88internal8compiler12MachineGraph15Float64ConstantEd@PLT
	jmp	.L2672
.L2633:
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2938
	movq	16(%rdx), %rdx
.L2938:
	movq	(%rdx), %rax
	cmpw	$26, 16(%rax)
	jne	.L2595
	movsd	48(%rax), %xmm0
	call	_ZN2v84base7ieee7545acoshEd@PLT
	movq	16(%r13), %rdi
	call	_ZN2v88internal8compiler12MachineGraph15Float64ConstantEd@PLT
	jmp	.L2672
.L2634:
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2937
	movq	16(%rdx), %rdx
.L2937:
	movq	(%rdx), %rax
	cmpw	$26, 16(%rax)
	jne	.L2595
	movsd	48(%rax), %xmm0
	call	_ZN2v84base7ieee7544acosEd@PLT
	movq	16(%r13), %rdi
	call	_ZN2v88internal8compiler12MachineGraph15Float64ConstantEd@PLT
	jmp	.L2672
.L2635:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal8compiler12BinopMatcherINS1_12FloatMatcherIdLNS1_8IrOpcode5ValueE26EEES6_EC1EPNS1_4NodeE
	cmpb	$0, -120(%rbp)
	je	.L2956
	cmpb	$0, -96(%rbp)
	je	.L2595
	movsd	-104(%rbp), %xmm1
	movsd	-128(%rbp), %xmm0
	call	_ZN2v84base7ieee7543powEdd@PLT
	movq	16(%r13), %rdi
	call	_ZN2v88internal8compiler12MachineGraph15Float64ConstantEd@PLT
	jmp	.L2672
.L2636:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal8compiler12BinopMatcherINS1_12FloatMatcherIdLNS1_8IrOpcode5ValueE26EEES6_EC1EPNS1_4NodeE
	movzbl	-96(%rbp), %eax
	testb	%al, %al
	je	.L2931
	movsd	-104(%rbp), %xmm0
	ucomisd	.LC1(%rip), %xmm0
	jp	.L2932
	jne	.L2932
	movq	16(%r13), %rdi
	movsd	.LC13(%rip), %xmm0
	call	_ZN2v88internal8compiler12MachineGraph15Float64ConstantEd@PLT
	jmp	.L2672
.L2637:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal8compiler12BinopMatcherINS1_12FloatMatcherIdLNS1_8IrOpcode5ValueE26EEES6_EC1EPNS1_4NodeE
	movzbl	24(%r13), %eax
	testb	%al, %al
	je	.L2911
	cmpb	$0, -96(%rbp)
	je	.L2912
	movsd	-104(%rbp), %xmm0
	ucomisd	.LC11(%rip), %xmm0
	jp	.L2913
	jne	.L2913
	movq	-136(%rbp), %rax
	jmp	.L2672
.L2638:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal8compiler12BinopMatcherINS1_12FloatMatcherIdLNS1_8IrOpcode5ValueE26EEES6_EC1EPNS1_4NodeE
	cmpb	$0, 24(%r13)
	je	.L2902
	cmpb	$0, -96(%rbp)
	je	.L2595
	movsd	-104(%rbp), %xmm0
	ucomisd	.LC11(%rip), %xmm0
	jp	.L2904
	jne	.L2904
	movq	-136(%rbp), %rax
	jmp	.L2672
.L2630:
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2941
	movq	16(%rdx), %rdx
.L2941:
	movq	(%rdx), %rax
	cmpw	$26, 16(%rax)
	jne	.L2595
	movsd	48(%rax), %xmm0
	call	_ZN2v84base7ieee7544atanEd@PLT
	movq	16(%r13), %rdi
	call	_ZN2v88internal8compiler12MachineGraph15Float64ConstantEd@PLT
	jmp	.L2672
.L2598:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler22MachineOperatorReducer29ReduceFloat64InsertHighWord32EPNS1_4NodeE
	jmp	.L2672
.L2599:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler22MachineOperatorReducer28ReduceFloat64InsertLowWord32EPNS1_4NodeE
	jmp	.L2672
.L2645:
	movzbl	23(%rsi), %edx
	movq	32(%rsi), %rcx
	leaq	32(%rsi), %r14
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L2804
	movq	(%rcx), %r8
	movzwl	16(%r8), %eax
	cmpw	$23, %ax
	sete	%sil
	je	.L2805
	xorl	%r10d, %r10d
	xorl	%r8d, %r8d
.L2806:
	leaq	8(%r14), %rax
	movq	%rcx, %r15
	movq	%r14, %r9
.L2809:
	movq	(%rax), %rbx
	movq	(%rbx), %rax
	movzwl	16(%rax), %r11d
	cmpw	$23, %r11w
	jne	.L2810
	movl	44(%rax), %r14d
	testb	%sil, %sil
	je	.L3316
.L2811:
	cmpl	$-1, %r8d
	je	.L3317
	cmpw	$23, %r11w
	je	.L3318
.L2823:
	cmpq	%r15, %rbx
	jne	.L2595
.L2825:
	movq	16(%r13), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	jmp	.L2672
.L2646:
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %rcx
	leaq	32(%rsi), %r14
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L2786
	movq	(%rcx), %rsi
	movzwl	16(%rsi), %edx
	cmpw	$23, %dx
	sete	%r8b
	je	.L2787
	xorl	%esi, %esi
	xorl	%r10d, %r10d
.L2788:
	leaq	8(%r14), %rdx
	movq	%rcx, %r15
	movq	%r14, %r9
.L2791:
	movq	(%rdx), %rbx
	movq	(%rbx), %rdx
	cmpw	$23, 16(%rdx)
	jne	.L2792
	movl	44(%rdx), %eax
	testb	%r8b, %r8b
	je	.L2794
	xorl	%esi, %esi
	movq	16(%r13), %rdi
	cmpl	%eax, %r10d
	setle	%sil
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	jmp	.L2672
.L2639:
	leaq	-208(%rbp), %rdi
	call	_ZN2v88internal8compiler12BinopMatcherINS1_12FloatMatcherIdLNS1_8IrOpcode5ValueE26EEES6_EC1EPNS1_4NodeE
	cmpb	$0, 24(%r13)
	je	.L2884
	cmpb	$0, -160(%rbp)
	je	.L2885
	movsd	-168(%rbp), %xmm0
	ucomisd	.LC1(%rip), %xmm0
	jp	.L2886
	jne	.L2886
	movq	%xmm0, %rax
	testq	%rax, %rax
	js	.L2886
	movq	-200(%rbp), %rax
	jmp	.L2672
	.p2align 4,,10
	.p2align 3
.L2640:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal8compiler12BinopMatcherINS1_12FloatMatcherIdLNS1_8IrOpcode5ValueE26EEES6_EC1EPNS1_4NodeE
	cmpb	$0, -120(%rbp)
	je	.L2595
	cmpb	$0, -96(%rbp)
	je	.L2595
	movsd	-128(%rbp), %xmm0
	addsd	-104(%rbp), %xmm0
	movq	16(%r13), %rdi
	call	_ZN2v88internal8compiler12MachineGraph15Float64ConstantEd@PLT
	jmp	.L2672
.L2641:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal8compiler12BinopMatcherINS1_12FloatMatcherIdLNS1_8IrOpcode5ValueE26EEES6_EC1EPNS1_4NodeE
	movzbl	-96(%rbp), %eax
	testb	%al, %al
	je	.L2943
	movsd	-104(%rbp), %xmm0
	ucomisd	%xmm0, %xmm0
	jp	.L3319
.L2943:
	cmpb	$0, -120(%rbp)
	je	.L2595
	movsd	-128(%rbp), %xmm0
	ucomisd	%xmm0, %xmm0
	jp	.L3320
	testb	%al, %al
	je	.L2595
	movsd	-104(%rbp), %xmm1
	call	_ZN2v84base7ieee7545atan2Edd@PLT
	movq	16(%r13), %rdi
	call	_ZN2v88internal8compiler12MachineGraph15Float64ConstantEd@PLT
	jmp	.L2672
.L2642:
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %r9
	leaq	32(%rsi), %r15
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L2851
	movq	(%r9), %rcx
	movzwl	16(%rcx), %edx
	cmpw	$25, %dx
	sete	%sil
	je	.L2852
	pxor	%xmm0, %xmm0
	xorl	%edx, %edx
.L2853:
	leaq	8(%r15), %rcx
	movq	%r9, %rbx
	movq	%r15, %r8
.L2856:
	movq	(%rcx), %r14
	movq	(%r14), %rcx
	cmpw	$25, 16(%rcx)
	jne	.L2857
	movzbl	24(%r13), %eax
	movss	44(%rcx), %xmm1
	pxor	%xmm2, %xmm2
	testb	%al, %al
	jne	.L2858
.L2859:
	ucomiss	%xmm1, %xmm1
	movl	$1, %edx
	jp	.L3321
.L2871:
	testb	%sil, %sil
	je	.L2595
	ucomiss	%xmm0, %xmm0
	jp	.L3322
	testb	%dl, %dl
	je	.L2874
	movq	16(%r13), %rax
	subss	%xmm1, %xmm0
	movq	8(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder15Float32ConstantEf@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	jmp	.L2672
.L2623:
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2952
	movq	16(%rdx), %rdx
.L2952:
	movq	(%rdx), %rax
	cmpw	$26, 16(%rax)
	jne	.L2595
	movsd	48(%rax), %xmm0
	call	_ZN2v84base7ieee7543logEd@PLT
	movq	16(%r13), %rdi
	call	_ZN2v88internal8compiler12MachineGraph15Float64ConstantEd@PLT
	jmp	.L2672
.L2625:
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2950
	movq	16(%rdx), %rdx
.L2950:
	movq	(%rdx), %rax
	cmpw	$26, 16(%rax)
	jne	.L2595
	movsd	48(%rax), %xmm0
	call	_ZN2v84base7ieee7543expEd@PLT
	movq	16(%r13), %rdi
	call	_ZN2v88internal8compiler12MachineGraph15Float64ConstantEd@PLT
	jmp	.L2672
.L2626:
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2949
	movq	16(%rdx), %rdx
.L2949:
	movq	(%rdx), %rax
	cmpw	$26, 16(%rax)
	jne	.L2595
	movsd	48(%rax), %xmm0
	call	_ZN2v84base7ieee7544coshEd@PLT
	movq	16(%r13), %rdi
	call	_ZN2v88internal8compiler12MachineGraph15Float64ConstantEd@PLT
	jmp	.L2672
.L2627:
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2948
	movq	16(%rdx), %rdx
.L2948:
	movq	(%rdx), %rax
	cmpw	$26, 16(%rax)
	jne	.L2595
	movsd	48(%rax), %xmm0
	call	_ZN2v84base7ieee7543cosEd@PLT
	movq	16(%r13), %rdi
	call	_ZN2v88internal8compiler12MachineGraph15Float64ConstantEd@PLT
	jmp	.L2672
.L2628:
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2947
	movq	16(%rdx), %rdx
.L2947:
	movq	(%rdx), %rax
	cmpw	$26, 16(%rax)
	jne	.L2595
	movsd	48(%rax), %xmm0
	call	_ZN2v84base7ieee7544cbrtEd@PLT
	movq	16(%r13), %rdi
	call	_ZN2v88internal8compiler12MachineGraph15Float64ConstantEd@PLT
	jmp	.L2672
.L2629:
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2942
	movq	16(%rdx), %rdx
.L2942:
	movq	(%rdx), %rax
	cmpw	$26, 16(%rax)
	jne	.L2595
	movsd	48(%rax), %xmm0
	call	_ZN2v84base7ieee7545atanhEd@PLT
	movq	16(%r13), %rdi
	call	_ZN2v88internal8compiler12MachineGraph15Float64ConstantEd@PLT
	jmp	.L2672
.L2611:
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2975
	movq	16(%rdx), %rdx
.L2975:
	movq	(%rdx), %rcx
	movzwl	16(%rcx), %eax
	cmpw	$26, %ax
	jne	.L2976
	movsd	48(%rcx), %xmm0
	comisd	.LC28(%rip), %xmm0
	jb	.L3299
	comisd	.LC29(%rip), %xmm0
	movl	$2147483647, %esi
	ja	.L2977
	cvttsd2sil	%xmm0, %esi
.L2977:
	movq	16(%r13), %rdi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	jmp	.L2672
.L2655:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler22MachineOperatorReducer15ReduceUint32ModEPNS1_4NodeE
	jmp	.L2672
.L2656:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler22MachineOperatorReducer15ReduceUint32DivEPNS1_4NodeE
	jmp	.L2672
.L2657:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler22MachineOperatorReducer14ReduceInt32ModEPNS1_4NodeE
	jmp	.L2672
.L2658:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler22MachineOperatorReducer14ReduceInt32DivEPNS1_4NodeE
	jmp	.L2672
.L2659:
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %rcx
	leaq	32(%rsi), %rbx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L2746
	movq	(%rcx), %rdx
	cmpw	$23, 16(%rdx)
	je	.L2747
	xorl	%r8d, %r8d
	xorl	%esi, %esi
.L2748:
	leaq	8(%rbx), %rdx
	movq	%rcx, %r15
	movq	%rbx, %r9
.L2751:
	movq	(%rdx), %r14
	movq	(%r14), %rdx
	cmpw	$23, 16(%rdx)
	jne	.L2752
	movl	44(%rdx), %r8d
.L2753:
	cmpl	$2, %r8d
	je	.L3323
	cmpl	$-1, %r8d
	jne	.L2595
	movq	16(%r13), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.0
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.1
	movq	16(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder20Int32SubWithOverflowEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	%r12, %rax
	jmp	.L2672
.L2660:
	movzbl	23(%rsi), %ecx
	leaq	32(%rsi), %rbx
	movq	32(%rsi), %rsi
	andl	$15, %ecx
	cmpl	$15, %ecx
	je	.L2715
	movq	(%rsi), %rdx
	movzwl	16(%rdx), %eax
	cmpw	$23, %ax
	sete	%r9b
	je	.L2716
	xorl	%r8d, %r8d
	xorl	%r10d, %r10d
.L2717:
	leaq	8(%rbx), %rax
	movq	%rsi, %r14
	movq	%rbx, %r15
.L2720:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	cmpw	$23, 16(%rdx)
	jne	.L2721
	movl	44(%rdx), %esi
.L2722:
	testl	%esi, %esi
	je	.L2672
	movq	%r14, %rax
	cmpl	$1, %esi
	je	.L2672
	testb	%r9b, %r9b
	je	.L3324
	imull	%r8d, %esi
	movq	16(%r13), %rdi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	jmp	.L2672
.L2661:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler22MachineOperatorReducer14ReduceInt32SubEPNS1_4NodeE
	jmp	.L2672
.L2607:
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2992
	movq	16(%rdx), %rdx
.L2992:
	movq	(%rdx), %rax
	cmpw	$23, 16(%rax)
	jne	.L2595
	movq	16(%r13), %rdx
	movslq	44(%rax), %rsi
	movq	8(%rdx), %rdi
	movq	(%rdx), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder13Int64ConstantEl@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	jmp	.L2672
.L2615:
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2971
	movq	16(%rdx), %rdx
.L2971:
	movq	(%rdx), %rax
	cmpw	$26, 16(%rax)
	jne	.L2595
	movsd	48(%rax), %xmm0
	call	_ZN2v84base7ieee7544tanhEd@PLT
	movq	16(%r13), %rdi
	call	_ZN2v88internal8compiler12MachineGraph15Float64ConstantEd@PLT
	jmp	.L2672
.L2616:
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2970
	movq	16(%rdx), %rdx
.L2970:
	movq	(%rdx), %rax
	cmpw	$26, 16(%rax)
	jne	.L2595
	movsd	48(%rax), %xmm0
	call	_ZN2v84base7ieee7543tanEd@PLT
	movq	16(%r13), %rdi
	call	_ZN2v88internal8compiler12MachineGraph15Float64ConstantEd@PLT
	jmp	.L2672
.L2617:
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2969
	movq	16(%rdx), %rdx
.L2969:
	movq	(%rdx), %rax
	cmpw	$26, 16(%rax)
	jne	.L2595
	movsd	48(%rax), %xmm0
	call	_ZN2v84base7ieee7544sinhEd@PLT
	movq	16(%r13), %rdi
	call	_ZN2v88internal8compiler12MachineGraph15Float64ConstantEd@PLT
	jmp	.L2672
.L2618:
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2968
	movq	16(%rdx), %rdx
.L2968:
	movq	(%rdx), %rax
	cmpw	$26, 16(%rax)
	jne	.L2595
	movsd	48(%rax), %xmm0
	call	_ZN2v84base7ieee7543sinEd@PLT
	movq	16(%r13), %rdi
	call	_ZN2v88internal8compiler12MachineGraph15Float64ConstantEd@PLT
	jmp	.L2672
.L2619:
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L3049
	movq	16(%rdx), %rdx
.L3049:
	movq	(%rdx), %rdx
	xorl	%eax, %eax
	cmpw	$26, 16(%rdx)
	jne	.L2672
	movsd	48(%rdx), %xmm3
	movsd	.LC10(%rip), %xmm2
	movsd	.LC9(%rip), %xmm4
	movapd	%xmm3, %xmm1
	movapd	%xmm3, %xmm0
	andpd	%xmm2, %xmm1
	ucomisd	%xmm1, %xmm4
	jbe	.L3051
	cvttsd2siq	%xmm3, %rax
	pxor	%xmm1, %xmm1
	movsd	.LC11(%rip), %xmm4
	andnpd	%xmm3, %xmm2
	cvtsi2sdq	%rax, %xmm1
	movapd	%xmm1, %xmm5
	cmpnlesd	%xmm3, %xmm5
	movapd	%xmm5, %xmm0
	andpd	%xmm4, %xmm0
	subsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	orpd	%xmm2, %xmm0
.L3051:
	movq	16(%r13), %rdi
	call	_ZN2v88internal8compiler12MachineGraph15Float64ConstantEd@PLT
	jmp	.L2672
.L2620:
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2955
	movq	16(%rdx), %rdx
.L2955:
	movq	(%rdx), %rax
	cmpw	$26, 16(%rax)
	jne	.L2595
	movsd	48(%rax), %xmm0
	call	_ZN2v84base7ieee7544log2Ed@PLT
	movq	16(%r13), %rdi
	call	_ZN2v88internal8compiler12MachineGraph15Float64ConstantEd@PLT
	jmp	.L2672
.L2621:
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2954
	movq	16(%rdx), %rdx
.L2954:
	movq	(%rdx), %rax
	cmpw	$26, 16(%rax)
	jne	.L2595
	movsd	48(%rax), %xmm0
	call	_ZN2v84base7ieee7545log10Ed@PLT
	movq	16(%r13), %rdi
	call	_ZN2v88internal8compiler12MachineGraph15Float64ConstantEd@PLT
	jmp	.L2672
.L2622:
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2953
	movq	16(%rdx), %rdx
.L2953:
	movq	(%rdx), %rax
	cmpw	$26, 16(%rax)
	jne	.L2595
	movsd	48(%rax), %xmm0
	call	_ZN2v84base7ieee7545log1pEd@PLT
	movq	16(%r13), %rdi
	call	_ZN2v88internal8compiler12MachineGraph15Float64ConstantEd@PLT
	jmp	.L2672
.L2647:
	movzbl	23(%rsi), %edx
	movq	32(%rsi), %rcx
	leaq	32(%rsi), %r15
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L2764
	movq	(%rcx), %r8
	movzwl	16(%r8), %eax
	cmpw	$23, %ax
	sete	%sil
	je	.L2765
	xorl	%r10d, %r10d
	xorl	%r9d, %r9d
.L2766:
	leaq	8(%r15), %rax
	movq	%rcx, %r14
	movq	%r15, %r8
.L2769:
	movq	(%rax), %rbx
	movq	(%rbx), %rax
	cmpw	$23, 16(%rax)
	jne	.L2770
	movl	44(%rax), %eax
	movl	$1, %r10d
	testb	%sil, %sil
	je	.L2772
	xorl	%esi, %esi
	movq	16(%r13), %rdi
	cmpl	%eax, %r9d
	setl	%sil
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	jmp	.L2672
.L2648:
	leaq	-208(%rbp), %rdi
	call	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES6_EC1EPNS1_4NodeE
	cmpb	$0, -184(%rbp)
	je	.L2710
	cmpb	$0, -160(%rbp)
	je	.L2711
	movq	-168(%rbp), %rax
	xorl	%esi, %esi
	movq	16(%r13), %rdi
	cmpq	%rax, -192(%rbp)
	sete	%sil
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	jmp	.L2672
.L2649:
	movzbl	23(%rsi), %edx
	movq	32(%rsi), %rcx
	leaq	32(%rsi), %r15
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L2691
	movq	(%rcx), %r8
	movzwl	16(%r8), %eax
	cmpw	$23, %ax
	sete	%sil
	je	.L2692
	xorl	%r9d, %r9d
	xorl	%r10d, %r10d
.L2693:
	leaq	8(%r15), %rax
	movq	%rcx, %r14
	movq	%r15, %r8
.L2696:
	movq	(%rax), %rbx
	movq	(%rbx), %rax
	cmpw	$23, 16(%rax)
	jne	.L2697
	movl	44(%rax), %eax
	testb	%sil, %sil
	je	.L3325
	xorl	%esi, %esi
	movq	16(%r13), %rdi
	cmpl	%r9d, %eax
	sete	%sil
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	jmp	.L2672
.L2650:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler22MachineOperatorReducer14ReduceInt64SubEPNS1_4NodeE
	jmp	.L2672
.L2651:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler22MachineOperatorReducer14ReduceInt64AddEPNS1_4NodeE
	jmp	.L2672
.L2652:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler22MachineOperatorReducer15ReduceWord64SarEPNS1_4NodeE
	jmp	.L2672
.L2653:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler22MachineOperatorReducer15ReduceWord64ShrEPNS1_4NodeE
	jmp	.L2672
.L2654:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler22MachineOperatorReducer15ReduceWord64ShlEPNS1_4NodeE
	jmp	.L2672
.L2608:
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2991
	movq	16(%rdx), %rdx
.L2991:
	movq	(%rdx), %rax
	cmpw	$23, 16(%rax)
	jne	.L2595
	pxor	%xmm0, %xmm0
	movq	16(%r13), %rdi
	cvtsi2sdl	44(%rax), %xmm0
	call	_ZN2v88internal8compiler12MachineGraph15Float64ConstantEd@PLT
	jmp	.L2672
.L2609:
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2983
	movq	16(%rdx), %rdx
.L2983:
	movq	(%rdx), %rcx
	movzwl	16(%rcx), %eax
	cmpw	$26, %ax
	jne	.L2984
	movsd	48(%rcx), %xmm0
	pxor	%xmm1, %xmm1
	comisd	%xmm0, %xmm1
	ja	.L3326
	movsd	.LC9(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jbe	.L3301
	addsd	%xmm1, %xmm0
	movd	%xmm0, %esi
.L2988:
	movq	16(%r13), %rdi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	jmp	.L2672
.L2610:
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2980
	movq	16(%rdx), %rdx
.L2980:
	movq	(%rdx), %rcx
	movzwl	16(%rcx), %eax
	cmpw	$26, %ax
	jne	.L2981
	movq	16(%r13), %rax
	cvttsd2siq	48(%rcx), %rsi
	movq	8(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder13Int64ConstantEl@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	jmp	.L2672
.L2600:
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L3038
	movq	16(%rdx), %rdx
.L3038:
	movq	(%rdx), %rcx
	movzwl	16(%rcx), %eax
	cmpw	$26, %ax
	jne	.L3039
	movsd	48(%rcx), %xmm0
	movsd	.LC23(%rip), %xmm2
	movapd	%xmm0, %xmm1
	andpd	.LC10(%rip), %xmm1
	ucomisd	%xmm1, %xmm2
	jb	.L3040
	movsd	.LC29(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L3040
	comisd	.LC28(%rip), %xmm0
	jb	.L3040
	cvttsd2sil	%xmm0, %esi
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%esi, %xmm1
	ucomisd	%xmm1, %xmm0
	jnp	.L3327
.L3040:
	movabsq	$9218868437227405312, %rax
	movq	%xmm0, %rdi
	testq	%rax, %rdi
	je	.L3111
	movq	%xmm0, %rdx
	xorl	%esi, %esi
	shrq	$52, %rdx
	andl	$2047, %edx
	movl	%edx, %ecx
	subl	$1075, %ecx
	js	.L3328
	cmpl	$31, %ecx
	jg	.L3043
	movabsq	$4503599627370495, %rax
	movabsq	$4503599627370496, %rdx
	andq	%rdi, %rax
	addq	%rdx, %rax
	salq	%cl, %rax
	movl	%eax, %esi
.L3046:
	movq	%rdi, %rax
	sarq	$63, %rax
	orl	$1, %eax
	imull	%eax, %esi
.L3043:
	movq	16(%r13), %rdi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	jmp	.L2672
.L2664:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler22MachineOperatorReducer15ReduceWord32SarEPNS1_4NodeE
	jmp	.L2672
.L2665:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler22MachineOperatorReducer15ReduceWord32ShrEPNS1_4NodeE
	jmp	.L2672
.L2666:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler22MachineOperatorReducer15ReduceWord32ShlEPNS1_4NodeE
	jmp	.L2672
.L2667:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler22MachineOperatorReducer15ReduceWord32XorEPNS1_4NodeE
	jmp	.L2672
.L2668:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler22MachineOperatorReducer14ReduceWord32OrEPNS1_4NodeE
	jmp	.L2672
.L2669:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler22MachineOperatorReducer15ReduceWord32AndEPNS1_4NodeE
	jmp	.L2672
.L2612:
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2972
	movq	16(%rdx), %rdx
.L2972:
	movq	(%rdx), %rax
	cmpw	$25, 16(%rax)
	jne	.L2595
	cmpb	$1, 24(%r13)
	movss	44(%rax), %xmm0
	movq	16(%r13), %rdi
	je	.L3139
	ucomiss	%xmm0, %xmm0
	jp	.L2973
.L3139:
	cvtss2sd	%xmm0, %xmm0
	call	_ZN2v88internal8compiler12MachineGraph15Float64ConstantEd@PLT
	jmp	.L2672
.L2644:
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %rcx
	leaq	32(%rsi), %r14
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L2828
	movq	(%rcx), %r8
	movzwl	16(%r8), %edx
	cmpw	$23, %dx
	sete	%sil
	je	.L2829
	xorl	%r10d, %r10d
	xorl	%r8d, %r8d
.L2830:
	leaq	8(%r14), %rdx
	movq	%rcx, %r15
	movq	%r14, %r9
.L2833:
	movq	(%rdx), %rbx
	movq	(%rbx), %rdx
	movzwl	16(%rdx), %r11d
	cmpw	$23, %r11w
	jne	.L2834
	movl	44(%rdx), %eax
	testb	%sil, %sil
	je	.L3329
.L2835:
	testl	%r8d, %r8d
	je	.L3330
	cmpw	$23, %r11w
	je	.L3331
.L2847:
	cmpq	%rbx, %r15
	jne	.L2595
.L2849:
	movq	16(%r13), %rdi
	movl	$1, %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	jmp	.L2672
.L2662:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler22MachineOperatorReducer14ReduceInt32AddEPNS1_4NodeE
	jmp	.L2672
.L2663:
	movzbl	23(%rsi), %ecx
	leaq	32(%rsi), %rbx
	movq	32(%rsi), %rsi
	andl	$15, %ecx
	cmpl	$15, %ecx
	je	.L2673
	movq	(%rsi), %rdx
	movzwl	16(%rdx), %eax
	cmpw	$23, %ax
	sete	%r9b
	je	.L2674
	xorl	%r15d, %r15d
	xorl	%r10d, %r10d
.L2675:
	leaq	8(%rbx), %rdx
	movq	%rsi, %rax
	movq	%rbx, %r8
.L2678:
	movq	(%rdx), %r14
	movq	(%r14), %rdx
	cmpw	$23, 16(%rdx)
	jne	.L2679
	movl	44(%rdx), %ecx
	testl	%ecx, %ecx
	je	.L2672
	testb	%r9b, %r9b
	je	.L2595
	movl	%r15d, %esi
	movq	16(%r13), %rdi
	rorl	%cl, %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	jmp	.L2672
.L2670:
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2671
	movq	16(%r14), %r14
.L2671:
	call	_ZN2v88internal8compiler17ProjectionIndexOfEPKNS1_8OperatorE@PLT
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler22MachineOperatorReducer16ReduceProjectionEmPNS1_4NodeE
	jmp	.L2672
.L2624:
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2951
	movq	16(%rdx), %rdx
.L2951:
	movq	(%rdx), %rax
	cmpw	$26, 16(%rax)
	jne	.L2595
	movsd	48(%rax), %xmm0
	call	_ZN2v84base7ieee7545expm1Ed@PLT
	movq	16(%r13), %rdi
	call	_ZN2v88internal8compiler12MachineGraph15Float64ConstantEd@PLT
	jmp	.L2672
.L2604:
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L3001
	movq	16(%rdx), %rdx
.L3001:
	movq	(%rdx), %rax
	cmpw	$23, 16(%rax)
	jne	.L2595
	movq	16(%r13), %rdx
	movl	44(%rax), %esi
	movq	8(%rdx), %rdi
	movq	(%rdx), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder13Int64ConstantEl@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	jmp	.L2672
.L2605:
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2998
	movq	16(%rdx), %rdx
.L2998:
	movq	(%rdx), %rax
	cmpw	$23, 16(%rax)
	jne	.L2595
	movl	44(%rax), %eax
	pxor	%xmm0, %xmm0
	movq	16(%r13), %rdi
	cvtsi2sdq	%rax, %xmm0
	call	_ZN2v88internal8compiler12MachineGraph15Float64ConstantEd@PLT
	jmp	.L2672
.L2606:
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2993
	movq	16(%rdx), %rdx
.L2993:
	movq	(%rdx), %rcx
	movzwl	16(%rcx), %esi
	cmpw	$23, %si
	je	.L3332
	cmpl	$24, %esi
	je	.L3333
	cmpw	$449, %si
	jne	.L2595
	movq	32(%rdx), %rax
	movzbl	23(%rdx), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L2672
	movq	16(%rax), %rax
	jmp	.L2672
	.p2align 4,,10
	.p2align 3
.L2613:
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L3005
	movq	16(%rdx), %rdx
.L3005:
	movq	(%rdx), %rcx
	movzwl	16(%rcx), %eax
	cmpw	$26, %ax
	jne	.L3006
	movsd	48(%rcx), %xmm0
	movsd	.LC23(%rip), %xmm2
	movapd	%xmm0, %xmm1
	andpd	.LC10(%rip), %xmm1
	ucomisd	%xmm1, %xmm2
	jb	.L3007
	movsd	.LC29(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L3007
	comisd	.LC28(%rip), %xmm0
	jb	.L3007
	cvttsd2sil	%xmm0, %esi
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%esi, %xmm1
	ucomisd	%xmm1, %xmm0
	jnp	.L3334
.L3007:
	movabsq	$9218868437227405312, %rax
	movq	%xmm0, %rdi
	testq	%rax, %rdi
	je	.L3103
	movq	%xmm0, %rdx
	xorl	%esi, %esi
	shrq	$52, %rdx
	andl	$2047, %edx
	movl	%edx, %ecx
	subl	$1075, %ecx
	js	.L3335
	cmpl	$31, %ecx
	jg	.L3010
	movabsq	$4503599627370495, %rax
	movabsq	$4503599627370496, %rdx
	andq	%rdi, %rax
	addq	%rdx, %rax
	salq	%cl, %rax
	movl	%eax, %esi
.L3013:
	movq	%rdi, %rax
	sarq	$63, %rax
	orl	$1, %eax
	imull	%eax, %esi
.L3010:
	movq	16(%r13), %rdi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	jmp	.L2672
.L2602:
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L3023
	movq	16(%rdx), %rdx
.L3023:
	movq	(%rdx), %rcx
	movzbl	24(%r13), %esi
	movzwl	16(%rcx), %eax
	cmpw	$26, %ax
	jne	.L3024
	movsd	48(%rcx), %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.L3145
	cmpb	$1, %sil
	jne	.L3025
.L3145:
	comisd	.LC5(%rip), %xmm0
	jbe	.L3336
	movsd	.LC6(%rip), %xmm2
	movss	.LC14(%rip), %xmm1
	comisd	%xmm0, %xmm2
	jnb	.L3034
	movss	.LC15(%rip), %xmm1
.L3034:
	movq	16(%r13), %rax
	movaps	%xmm1, %xmm0
	movq	8(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder15Float32ConstantEf@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	jmp	.L2672
.L2603:
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L3002
	movq	16(%rdx), %rdx
.L3002:
	movq	(%rdx), %rax
	cmpw	$443, 16(%rax)
	jne	.L2595
	movzbl	23(%rdx), %eax
	movq	32(%rdx), %rcx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L3003
	movq	16(%rcx), %rcx
.L3003:
	movq	(%rcx), %rax
	cmpw	$462, 16(%rax)
	jne	.L2595
	movzbl	23(%rcx), %eax
	movq	32(%rcx), %rbx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L3004
	movq	16(%rbx), %rbx
.L3004:
	movq	16(%r13), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder31BitcastWord32ToCompressedSignedEv@PLT
	leaq	-80(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%rbx, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	jmp	.L2672
.L2601:
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L3016
	movq	16(%rdx), %rdx
.L3016:
	movq	(%rdx), %rcx
	movzwl	16(%rcx), %esi
	movl	%esi, %eax
	cmpw	$23, %si
	je	.L3337
	cmpl	$24, %esi
	je	.L3338
	cmpw	$462, %si
	je	.L3339
	cmpw	$441, %ax
	jne	.L2595
	movq	32(%rdx), %rax
	movzbl	23(%rdx), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L3021
	movq	16(%rax), %rax
.L3021:
	movq	(%rax), %rdx
	cmpw	$469, 16(%rdx)
	jne	.L2595
	movq	32(%rax), %rcx
	leaq	32(%rax), %rdx
	movzbl	23(%rax), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L3022
	leaq	16(%rcx), %rdx
.L3022:
	movq	(%rdx), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler22MachineOperatorReducer31BitcastCompressedSignedToWord32EPNS1_4NodeE
	jmp	.L2672
.L2770:
	xorl	%eax, %eax
	andb	18(%rdi), %r10b
	je	.L2772
	cmpl	$15, %edx
	je	.L2773
	movq	%r12, %rcx
	cmpq	%r14, %rbx
	je	.L2775
.L2774:
	leaq	-24(%rcx), %rsi
	movq	%r14, %rdi
	movb	%r10b, -233(%rbp)
	movl	%r9d, -224(%rbp)
	movq	%r8, -232(%rbp)
	movq	%rsi, -216(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-232(%rbp), %r8
	movq	-216(%rbp), %rsi
	movq	%rbx, %rdi
	movq	%rbx, (%r8)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movzbl	23(%r12), %eax
	movl	-224(%rbp), %r9d
	movzbl	-233(%rbp), %r10d
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L2776
.L2775:
	movq	8(%r15), %rdi
	cmpq	%r14, %rdi
	je	.L3061
	addq	$8, %r15
.L2778:
	subq	$48, %r12
	testq	%rdi, %rdi
	je	.L2779
	movq	%r12, %rsi
	movb	%r10b, -224(%rbp)
	movl	%r9d, -216(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movl	-216(%rbp), %r9d
	movzbl	-224(%rbp), %r10d
.L2779:
	movq	%r14, (%r15)
	movq	%r12, %rsi
	movq	%r14, %rdi
	movb	%r10b, -224(%rbp)
	movl	%r9d, -216(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movl	-216(%rbp), %r9d
	movzbl	-224(%rbp), %r10d
.L3061:
	movq	%rbx, %rdx
	movl	%r9d, %eax
	movq	%r14, %rbx
	movq	%rdx, %r14
.L2772:
	cmpq	%rbx, %r14
	je	.L3340
	movq	(%r14), %rdx
	cmpw	$300, 16(%rdx)
	jne	.L2595
	testb	%r10b, %r10b
	je	.L2595
	testl	%eax, %eax
	jne	.L2595
	leaq	-144(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES6_EC1EPNS1_4NodeE
	cmpb	$0, -124(%rbp)
	je	.L2783
	cmpl	$0, -128(%rbp)
	js	.L2784
.L2783:
	cmpb	$0, -108(%rbp)
	je	.L2595
	cmpl	$0, -112(%rbp)
	jns	.L2595
.L2784:
	movq	16(%r13), %rdi
	movl	$1, %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	jmp	.L2672
	.p2align 4,,10
	.p2align 3
.L2792:
	testb	$1, 18(%rdi)
	je	.L2794
	testb	%sil, %sil
	je	.L2794
	cmpl	$15, %eax
	je	.L2795
	movq	%r12, %rcx
	cmpq	%r15, %rbx
	je	.L2797
.L2796:
	leaq	-24(%rcx), %rsi
	movq	%r15, %rdi
	movq	%r9, -224(%rbp)
	movq	%rsi, -216(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-224(%rbp), %r9
	movq	-216(%rbp), %rsi
	movq	%rbx, %rdi
	movq	%rbx, (%r9)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L2798
.L2797:
	movq	8(%r14), %rdi
	cmpq	%r15, %rdi
	je	.L3062
	addq	$8, %r14
.L2800:
	subq	$48, %r12
	testq	%rdi, %rdi
	je	.L2801
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L2801:
	movq	%r15, (%r14)
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L3062:
	movq	%r15, %rax
	movq	%rbx, %r15
	movq	%rax, %rbx
.L2794:
	cmpq	%rbx, %r15
	jne	.L2595
	movq	16(%r13), %rdi
	movl	$1, %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	jmp	.L2672
	.p2align 4,,10
	.p2align 3
.L2931:
	cmpb	$0, -120(%rbp)
	je	.L2595
	movsd	-128(%rbp), %xmm0
	ucomisd	%xmm0, %xmm0
	jp	.L3341
	testb	%al, %al
	je	.L2595
	movsd	-104(%rbp), %xmm1
	call	fmod@PLT
	movq	16(%r13), %rdi
	call	_ZN2v88internal8compiler12MachineGraph15Float64ConstantEd@PLT
	jmp	.L2672
.L3006:
	cmpw	$461, %ax
	je	.L3342
	xorl	%eax, %eax
	jmp	.L2672
.L3039:
	cmpw	$461, %ax
	jne	.L2595
	movq	32(%rdx), %rax
	movzbl	23(%rdx), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L2672
	movq	16(%rax), %rax
	jmp	.L2672
.L2981:
	cmpw	$463, %ax
	jne	.L2595
	movq	32(%rdx), %rax
	movzbl	23(%rdx), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L2672
	movq	16(%rax), %rax
	jmp	.L2672
.L2810:
	testb	$1, 18(%rdi)
	je	.L2813
	testb	%r10b, %r10b
	jne	.L3343
.L2813:
	xorl	%r14d, %r14d
	testb	%sil, %sil
	jne	.L2811
	jmp	.L2823
.L2752:
	testb	$1, 18(%rdi)
	je	.L2595
	testb	%sil, %sil
	je	.L2595
	cmpl	$15, %eax
	je	.L2755
	movq	%r12, %rcx
	cmpq	%r14, %r15
	je	.L2757
.L2756:
	leaq	-24(%rcx), %rsi
	movq	%r15, %rdi
	movl	%r8d, -224(%rbp)
	movq	%r9, -232(%rbp)
	movq	%rsi, -216(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-232(%rbp), %r9
	movq	-216(%rbp), %rsi
	movq	%r14, %rdi
	movq	%r14, (%r9)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movzbl	23(%r12), %eax
	movl	-224(%rbp), %r8d
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L2758
.L2757:
	movq	8(%rbx), %rdi
	cmpq	%rdi, %r15
	je	.L3059
	leaq	8(%rbx), %rdx
	movq	%r12, %rax
.L2760:
	leaq	-48(%rax), %rbx
	testq	%rdi, %rdi
	je	.L2761
	movq	%rbx, %rsi
	movq	%rdx, -224(%rbp)
	movl	%r8d, -216(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movl	-216(%rbp), %r8d
	movq	-224(%rbp), %rdx
.L2761:
	movq	%r15, (%rdx)
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movl	%r8d, -216(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movl	-216(%rbp), %r8d
.L3059:
	movq	%r14, %r15
	jmp	.L2753
.L2721:
	testb	$1, 18(%rdi)
	je	.L2595
	testb	%r10b, %r10b
	je	.L2595
	cmpl	$15, %ecx
	je	.L2724
	movq	%r12, %rsi
	cmpq	%r14, %rax
	je	.L2726
.L2725:
	subq	$24, %rsi
	movq	%r14, %rdi
	movl	%r8d, -224(%rbp)
	movq	%rax, -232(%rbp)
	movq	%rsi, -216(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-232(%rbp), %rax
	movq	-216(%rbp), %rsi
	movq	%rax, (%r15)
	movq	%rax, %rdi
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movzbl	23(%r12), %edx
	movq	-216(%rbp), %rax
	movl	-224(%rbp), %r8d
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L2727
.L2726:
	movq	8(%rbx), %rdi
	cmpq	%rdi, %r14
	je	.L3058
	addq	$8, %rbx
	movq	%r12, %rdx
.L2729:
	leaq	-48(%rdx), %r15
	testq	%rdi, %rdi
	je	.L2730
	movq	%r15, %rsi
	movq	%rax, -224(%rbp)
	movl	%r8d, -216(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movl	-216(%rbp), %r8d
	movq	-224(%rbp), %rax
.L2730:
	movq	%r14, (%rbx)
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rax, -224(%rbp)
	movl	%r8d, -216(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movl	-216(%rbp), %r8d
	movq	-224(%rbp), %rax
.L3058:
	movq	%r14, %rdx
	movl	%r8d, %esi
	movq	%rax, %r14
	xorl	%r9d, %r9d
	movq	%rdx, %rax
	xorl	%r8d, %r8d
	jmp	.L2722
.L2984:
	cmpw	$464, %ax
	jne	.L2595
	movq	32(%rdx), %rax
	movzbl	23(%rdx), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L2672
	movq	16(%rax), %rax
	jmp	.L2672
.L2857:
	andb	18(%rdi), %dl
	jne	.L3344
	movzbl	24(%r13), %eax
	pxor	%xmm1, %xmm1
	jmp	.L2871
.L2956:
	cmpb	$0, -96(%rbp)
	je	.L2595
	movsd	-104(%rbp), %xmm0
	ucomisd	.LC1(%rip), %xmm0
	jp	.L2959
	jne	.L2959
	movq	16(%r13), %rdi
	movsd	.LC11(%rip), %xmm0
	call	_ZN2v88internal8compiler12MachineGraph15Float64ConstantEd@PLT
	jmp	.L2672
.L2911:
	cmpb	$0, -96(%rbp)
	je	.L2912
	movsd	-104(%rbp), %xmm0
.L2913:
	ucomisd	%xmm0, %xmm0
	jp	.L3345
	cmpb	$0, -120(%rbp)
	jne	.L3346
	testb	%al, %al
	jne	.L3347
.L2922:
	movsd	.LC23(%rip), %xmm2
	movapd	%xmm0, %xmm1
	andpd	.LC10(%rip), %xmm1
	ucomisd	%xmm1, %xmm2
	setb	%al
	ucomisd	.LC24(%rip), %xmm1
	setb	%dl
	orb	%dl, %al
	jne	.L2595
	pxor	%xmm1, %xmm1
	ucomisd	%xmm1, %xmm0
	setnp	%dl
	cmove	%edx, %eax
	testb	%al, %al
	jne	.L2595
	movq	%xmm0, %rax
	movl	$2047, %ecx
	movq	%xmm0, %rdx
	btrq	$63, %rax
	salq	$52, %rcx
	cmpq	%rcx, %rax
	je	.L2595
	movabsq	$4503599627370495, %rax
	andq	%rdx, %rax
	testq	%rcx, %rdx
	je	.L2929
	movl	$1, %edx
	salq	$52, %rdx
	addq	%rdx, %rax
.L2930:
	leaq	-1(%rax), %rdx
	testq	%rax, %rdx
	jne	.L2595
	movsd	.LC11(%rip), %xmm1
	movq	16(%r13), %rdi
	divsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	call	_ZN2v88internal8compiler12MachineGraph15Float64ConstantEd@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.1
	movq	16(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder10Float64MulEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	%r12, %rax
	jmp	.L2672
	.p2align 4,,10
	.p2align 3
.L2902:
	cmpb	$0, -96(%rbp)
	je	.L2595
	movsd	-104(%rbp), %xmm0
.L2904:
	ucomisd	.LC20(%rip), %xmm0
	jp	.L2907
	jne	.L2907
	movsd	.LC21(%rip), %xmm0
	movq	16(%r13), %rdi
	call	_ZN2v88internal8compiler12MachineGraph15Float64ConstantEd@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.0
	movq	-136(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.1
	movq	16(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder10Float64SubEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	%r12, %rax
	jmp	.L2672
.L2834:
	testb	$1, 18(%rdi)
	je	.L2837
	testb	%r10b, %r10b
	jne	.L3348
.L2837:
	xorl	%eax, %eax
	testb	%sil, %sil
	jne	.L2835
	jmp	.L2847
.L2976:
	cmpw	$461, %ax
	jne	.L2595
	movq	32(%rdx), %rax
	movzbl	23(%rdx), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L2672
	movq	16(%rax), %rax
	jmp	.L2672
.L2884:
	cmpb	$0, -160(%rbp)
	je	.L2889
	movsd	-168(%rbp), %xmm0
.L2886:
	ucomisd	%xmm0, %xmm0
	jp	.L3349
	cmpb	$0, -184(%rbp)
	je	.L2595
	movsd	-192(%rbp), %xmm0
	ucomisd	%xmm0, %xmm0
	jp	.L2892
	subsd	-168(%rbp), %xmm0
	movq	16(%r13), %rdi
	call	_ZN2v88internal8compiler12MachineGraph15Float64ConstantEd@PLT
	jmp	.L2672
.L3024:
	cmpw	$447, %ax
	jne	.L2595
	testb	%sil, %sil
	je	.L2595
	movzbl	23(%rdx), %esi
	movq	32(%rdx), %rcx
	leaq	32(%rdx), %rax
	andl	$15, %esi
	leaq	16(%rcx), %rdx
	cmpl	$15, %esi
	cmove	%rdx, %rax
	movq	(%rax), %rax
	jmp	.L2672
.L2679:
	testb	$1, 18(%rdi)
	je	.L2595
	testb	%r10b, %r10b
	je	.L2595
	cmpl	$15, %ecx
	je	.L2683
	movq	%r12, %rsi
	cmpq	%r14, %rax
	je	.L2685
.L2684:
	leaq	-24(%rsi), %r13
	movq	%rax, %rdi
	movq	%rax, -216(%rbp)
	movq	%r13, %rsi
	movq	%r8, -224(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-224(%rbp), %r8
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%r14, (%r8)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movzbl	23(%r12), %edx
	movq	-216(%rbp), %rax
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L2686
.L2685:
	movq	8(%rbx), %rdi
	cmpq	%rdi, %rax
	je	.L3055
	addq	$8, %rbx
.L2688:
	subq	$48, %r12
	testq	%rdi, %rdi
	je	.L2689
	movq	%r12, %rsi
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-216(%rbp), %rax
.L2689:
	movq	%rax, (%rbx)
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L3055:
	testl	%r15d, %r15d
	jne	.L2595
	movq	%r14, %rax
	jmp	.L2672
	.p2align 4,,10
	.p2align 3
.L2710:
	movq	-200(%rbp), %rsi
	movq	(%rsi), %rax
	cmpw	$327, 16(%rax)
	je	.L3350
.L2713:
	cmpq	%rsi, -176(%rbp)
	jne	.L2595
	movq	16(%r13), %rdi
	movl	$1, %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	jmp	.L2672
.L2697:
	testb	$1, 18(%rdi)
	je	.L2700
	testb	%r10b, %r10b
	jne	.L3351
.L2700:
	cmpq	%rbx, %r14
	jne	.L2595
	movq	16(%r13), %rdi
	movl	$1, %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	jmp	.L2672
.L2912:
	cmpb	$0, -120(%rbp)
	je	.L2595
	movsd	-128(%rbp), %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.L2595
.L2919:
	subsd	%xmm1, %xmm1
	movq	16(%r13), %rdi
	movapd	%xmm1, %xmm0
	call	_ZN2v88internal8compiler12MachineGraph15Float64ConstantEd@PLT
	jmp	.L2672
.L2932:
	ucomisd	%xmm0, %xmm0
	jnp	.L2931
	movq	-112(%rbp), %rax
	jmp	.L2672
.L2673:
	movq	16(%rsi), %rax
	leaq	16(%rsi), %r8
	movq	(%rax), %r10
	movzwl	16(%r10), %edx
	cmpw	$23, %dx
	sete	%r9b
	je	.L2676
	xorl	%r15d, %r15d
	xorl	%r10d, %r10d
.L2677:
	leaq	8(%r8), %rdx
	jmp	.L2678
.L2828:
	movq	16(%rcx), %r15
	leaq	16(%rcx), %r9
	movq	(%r15), %r8
	movzwl	16(%r8), %edx
	cmpw	$23, %dx
	sete	%sil
	je	.L2831
	xorl	%r10d, %r10d
	xorl	%r8d, %r8d
.L2832:
	leaq	8(%r9), %rdx
	jmp	.L2833
.L3332:
	movslq	44(%rcx), %rax
.L2995:
	pxor	%xmm0, %xmm0
	movq	16(%r13), %rdi
	cvtsi2sdq	%rax, %xmm0
	call	_ZN2v88internal8compiler12MachineGraph15Float64ConstantEd@PLT
	jmp	.L2672
.L3337:
	movl	44(%rcx), %esi
.L3018:
	movq	16(%r13), %rdi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	jmp	.L2672
.L2851:
	movq	16(%r9), %rbx
	leaq	16(%r9), %r8
	movq	(%rbx), %rcx
	movzwl	16(%rcx), %edx
	cmpw	$25, %dx
	sete	%sil
	je	.L2854
	pxor	%xmm0, %xmm0
	xorl	%edx, %edx
.L2855:
	leaq	8(%r8), %rcx
	jmp	.L2856
.L2691:
	movq	16(%rcx), %r14
	leaq	16(%rcx), %r8
	movq	(%r14), %r9
	movzwl	16(%r9), %eax
	cmpw	$23, %ax
	sete	%sil
	je	.L2694
	xorl	%r9d, %r9d
	xorl	%r10d, %r10d
.L2695:
	leaq	8(%r8), %rax
	jmp	.L2696
.L2764:
	movq	16(%rcx), %r14
	leaq	16(%rcx), %r8
	movq	(%r14), %r9
	movzwl	16(%r9), %eax
	cmpw	$23, %ax
	sete	%sil
	je	.L2767
	xorl	%r10d, %r10d
	xorl	%r9d, %r9d
.L2768:
	leaq	8(%r8), %rax
	jmp	.L2769
.L2715:
	movq	16(%rsi), %r14
	leaq	16(%rsi), %r15
	movq	(%r14), %rdx
	movzwl	16(%rdx), %eax
	cmpw	$23, %ax
	sete	%r9b
	je	.L2718
	xorl	%r8d, %r8d
	xorl	%r10d, %r10d
.L2719:
	leaq	8(%r15), %rax
	jmp	.L2720
.L2786:
	movq	16(%rcx), %r15
	leaq	16(%rcx), %r9
	movq	(%r15), %rsi
	movzwl	16(%rsi), %edx
	cmpw	$23, %dx
	sete	%r8b
	je	.L2789
	xorl	%esi, %esi
	xorl	%r10d, %r10d
.L2790:
	leaq	8(%r9), %rdx
	jmp	.L2791
.L2804:
	movq	16(%rcx), %r15
	leaq	16(%rcx), %r9
	movq	(%r15), %r8
	movzwl	16(%r8), %eax
	cmpw	$23, %ax
	sete	%sil
	je	.L2807
	xorl	%r10d, %r10d
	xorl	%r8d, %r8d
.L2808:
	leaq	8(%r9), %rax
	jmp	.L2809
.L2746:
	movq	16(%rcx), %r15
	leaq	16(%rcx), %r9
	movq	(%r15), %rdx
	cmpw	$23, 16(%rdx)
	je	.L2749
	xorl	%r8d, %r8d
	xorl	%esi, %esi
.L2750:
	leaq	8(%r9), %rdx
	jmp	.L2751
.L2852:
	movss	44(%rcx), %xmm0
	movl	$1, %edx
	jmp	.L2853
.L2674:
	movl	44(%rdx), %r15d
	movl	$1, %r10d
	jmp	.L2675
.L2907:
	ucomisd	%xmm0, %xmm0
	jp	.L3352
	cmpb	$0, -120(%rbp)
	je	.L3353
	mulsd	-128(%rbp), %xmm0
	movq	16(%r13), %rdi
	call	_ZN2v88internal8compiler12MachineGraph15Float64ConstantEd@PLT
	jmp	.L2672
.L2829:
	movl	44(%r8), %r8d
	movl	$1, %r10d
	jmp	.L2830
.L2692:
	movl	44(%r8), %r9d
	movl	$1, %r10d
	jmp	.L2693
.L2765:
	movl	44(%r8), %r9d
	movl	$1, %r10d
	jmp	.L2766
.L2787:
	movl	44(%rsi), %r10d
	movl	$1, %esi
	jmp	.L2788
.L2805:
	movl	44(%r8), %r8d
	movl	$1, %r10d
	jmp	.L2806
.L2716:
	movl	44(%rdx), %r8d
	movl	$1, %r10d
	jmp	.L2717
.L2747:
	movl	44(%rdx), %r8d
	movl	$1, %esi
	jmp	.L2748
.L2959:
	ucomisd	.LC25(%rip), %xmm0
	jp	.L2961
	jne	.L2961
	movsd	.LC11(%rip), %xmm0
	movq	16(%r13), %rdi
	call	_ZN2v88internal8compiler12MachineGraph15Float64ConstantEd@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.0
	movq	16(%r13), %rax
	movq	-136(%rbp), %rbx
	movq	16(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler22MachineOperatorBuilder10Float64MulEv@PLT
	movq	%rbx, %xmm0
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	punpcklqdq	%xmm0, %xmm0
	leaq	-80(%rbp), %rcx
	movl	$2, %edx
	movq	%rax, %rsi
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.1
	movq	16(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder10Float64DivEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	%r12, %rax
	jmp	.L2672
.L3324:
	cmpl	$-1, %esi
	je	.L3354
	testl	%esi, %esi
	jle	.L2595
	leal	-1(%rsi), %r8d
	andl	%esi, %r8d
	jne	.L2595
	cmpl	$65535, %esi
	jle	.L2735
	sarl	$16, %esi
	movl	$16, %r8d
.L2735:
	cmpl	$255, %esi
	jle	.L2736
	addl	$8, %r8d
	sarl	$8, %esi
.L2736:
	cmpl	$15, %esi
	jle	.L2737
	addl	$4, %r8d
	sarl	$4, %esi
.L2737:
	cmpl	$4, %esi
	je	.L2738
	jg	.L2739
	cmpl	$1, %esi
	je	.L2740
	cmpl	$2, %esi
	jne	.L2742
.L2741:
	addl	$1, %r8d
.L2740:
	movq	16(%r13), %rdi
	movl	%r8d, %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.1
	movq	16(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32ShlEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler22MachineOperatorReducer15ReduceWord32ShlEPNS1_4NodeE
	testq	%rax, %rax
	cmove	%r12, %rax
	jmp	.L2672
.L3301:
	movl	$-2147483648, %esi
	jmp	.L2988
.L3325:
	movq	(%r14), %rdx
	cmpw	$308, 16(%rdx)
	jne	.L2700
.L3080:
	testl	%eax, %eax
	jne	.L2700
	leaq	-144(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES6_EC1EPNS1_4NodeE
	movq	-136(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.0
	movq	-120(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.1
	movq	%r12, %rax
	jmp	.L2672
.L2711:
	movq	-200(%rbp), %rsi
	jmp	.L2713
.L2885:
	cmpb	$0, -184(%rbp)
	je	.L2595
	movsd	-192(%rbp), %xmm0
	ucomisd	%xmm0, %xmm0
	jp	.L2892
	pxor	%xmm2, %xmm2
	ucomisd	%xmm2, %xmm0
	jp	.L2595
	jne	.L2595
	movmskpd	%xmm0, %eax
	movsd	%xmm2, -216(%rbp)
	testb	$1, %al
	je	.L2595
	movq	16(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder14Float64RoundUpEv@PLT
	testb	%al, %al
	je	.L2898
	movq	-176(%rbp), %rax
	movsd	-216(%rbp), %xmm2
	movq	(%rax), %rdx
	cmpw	$389, 16(%rdx)
	je	.L3355
.L2898:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node11RemoveInputEi@PLT
	movq	16(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder10Float64NegEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	%r12, %rax
	jmp	.L2672
.L3118:
	movq	%r14, %rax
	pxor	%xmm0, %xmm0
	movq	%rbx, %r14
	xorl	%esi, %esi
	movq	%rax, %rbx
	movaps	%xmm0, %xmm2
.L2858:
	ucomiss	%xmm2, %xmm1
	jp	.L3295
	jne	.L3295
	pxor	%xmm2, %xmm2
	movq	%rbx, %rax
	cvtss2sd	%xmm1, %xmm2
	andpd	.LC19(%rip), %xmm2
	orpd	.LC18(%rip), %xmm2
	comisd	.LC1(%rip), %xmm2
	ja	.L2672
.L3295:
	movl	$1, %eax
	jmp	.L2859
.L3336:
	movsd	.LC7(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jbe	.L3305
	comisd	.LC8(%rip), %xmm0
	movss	.LC16(%rip), %xmm1
	jnb	.L3034
	movss	.LC17(%rip), %xmm1
	jmp	.L3034
.L3316:
	testl	%r14d, %r14d
	je	.L2825
.L3081:
	cmpq	%r15, %rbx
	je	.L2825
	movq	(%r15), %rax
	cmpw	$304, 16(%rax)
	jne	.L2595
	leaq	-144(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES6_EC1EPNS1_4NodeE
	cmpb	$0, -108(%rbp)
	je	.L2595
	movl	-112(%rbp), %ecx
	movl	$2147483647, %eax
	movl	%ecx, %ebx
	sarl	%cl, %eax
	andl	$31, %ebx
	cmpl	%r14d, %eax
	jbe	.L2595
	movq	-136(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.0
	movl	%ebx, %ecx
	movq	16(%r13), %rdi
	movl	%r14d, %esi
	sall	%cl, %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.1
	movq	%r12, %rax
	jmp	.L2672
	.p2align 4,,10
	.p2align 3
.L3329:
	cmpl	$-1, %eax
	jne	.L2847
	jmp	.L2849
.L3353:
	ucomisd	.LC22(%rip), %xmm0
	jp	.L2595
	jne	.L2595
	movq	-136(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.1
	movq	16(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder10Float64AddEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	%r12, %rax
	jmp	.L2672
.L3338:
	movl	48(%rcx), %esi
	jmp	.L3018
.L3333:
	movq	48(%rcx), %rax
	jmp	.L2995
.L3342:
	movq	32(%rdx), %rax
	movzbl	23(%rdx), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L2672
	movq	16(%rax), %rax
	jmp	.L2672
.L3339:
	movq	32(%rdx), %rax
	movzbl	23(%rdx), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L2672
	movq	16(%rax), %rax
	jmp	.L2672
.L2961:
	ucomisd	.LC22(%rip), %xmm0
	jp	.L2963
	jne	.L2963
	movq	-136(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.1
	movq	16(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder10Float64MulEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	%r12, %rax
	jmp	.L2672
.L2874:
	testb	%al, %al
	je	.L2595
	pxor	%xmm2, %xmm2
	ucomiss	%xmm2, %xmm0
	movss	%xmm2, -216(%rbp)
	jp	.L2595
	jne	.L2595
	movd	%xmm0, %eax
	testl	%eax, %eax
	jns	.L2595
	movq	16(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder14Float32RoundUpEv@PLT
	testb	%al, %al
	je	.L2879
	movq	(%r14), %rax
	movss	-216(%rbp), %xmm2
	cmpw	$358, 16(%rax)
	je	.L3356
.L2879:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node11RemoveInputEi@PLT
	movq	16(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder10Float32NegEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	%r12, %rax
	jmp	.L2672
.L2889:
	cmpb	$0, -184(%rbp)
	je	.L2595
	movsd	-192(%rbp), %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.L2595
.L2892:
	subsd	%xmm0, %xmm0
	movq	16(%r13), %rdi
	call	_ZN2v88internal8compiler12MachineGraph15Float64ConstantEd@PLT
	jmp	.L2672
.L3326:
	comisd	.LC30(%rip), %xmm0
	jbe	.L3301
	movsd	.LC9(%rip), %xmm1
	subsd	%xmm0, %xmm1
	movq	%xmm1, %rsi
	negl	%esi
	jmp	.L2988
.L3350:
	cmpb	$0, -160(%rbp)
	je	.L2713
	cmpq	$0, -168(%rbp)
	jne	.L2713
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES6_EC1EPNS1_4NodeE
	movq	-136(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.0
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.1
	movq	%r12, %rax
	jmp	.L2672
.L2854:
	movss	44(%rcx), %xmm0
	movl	$1, %edx
	jmp	.L2855
.L2694:
	movl	44(%r9), %r9d
	movl	$1, %r10d
	jmp	.L2695
.L3330:
	movq	16(%r13), %rdi
	movl	$1, %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	jmp	.L2672
.L3323:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.1
	movq	16(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder20Int32AddWithOverflowEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	%r12, %rax
	jmp	.L2672
.L2718:
	movl	44(%rdx), %r8d
	movl	$1, %r10d
	jmp	.L2719
.L2789:
	movl	44(%rsi), %r10d
	movl	$1, %esi
	jmp	.L2790
.L3317:
	movq	16(%r13), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	jmp	.L2672
.L2749:
	movl	44(%rdx), %r8d
	movl	$1, %esi
	jmp	.L2750
.L2831:
	movl	44(%r8), %r8d
	movl	$1, %r10d
	jmp	.L2832
.L2676:
	movl	44(%r10), %r15d
	movl	$1, %r10d
	jmp	.L2677
.L2767:
	movl	44(%r9), %r9d
	movl	$1, %r10d
	jmp	.L2768
.L2807:
	movl	44(%r8), %r8d
	movl	$1, %r10d
	jmp	.L2808
.L2963:
	ucomisd	.LC26(%rip), %xmm0
	jp	.L2965
	jne	.L2965
	movsd	.LC11(%rip), %xmm0
	movq	16(%r13), %rdi
	call	_ZN2v88internal8compiler12MachineGraph15Float64ConstantEd@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.0
	movq	-136(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler22MachineOperatorReducer14Float64PowHalfEPNS1_4NodeE
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.1
	movq	16(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder10Float64DivEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	%r12, %rax
	jmp	.L2672
.L3347:
	movsd	.LC20(%rip), %xmm1
	ucomisd	-104(%rbp), %xmm1
	jp	.L2922
	jne	.L2922
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node11RemoveInputEi@PLT
	movq	16(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder10Float64NegEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	%r12, %rax
	jmp	.L2672
.L3340:
	movq	16(%r13), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	jmp	.L2672
.L3318:
	testl	%r14d, %r14d
	je	.L2825
	xorl	%esi, %esi
	movq	16(%r13), %rdi
	cmpl	%r14d, %r8d
	setb	%sil
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	jmp	.L2672
.L3331:
	cmpl	$-1, %eax
	je	.L2849
	xorl	%esi, %esi
	movq	16(%r13), %rdi
	cmpl	%eax, %r8d
	setbe	%sil
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	jmp	.L2672
.L3328:
	cmpl	$-52, %ecx
	jl	.L3043
	movabsq	$4503599627370495, %rsi
	movl	$1075, %ecx
	movabsq	$4503599627370496, %rax
	andq	%rdi, %rsi
	subl	%edx, %ecx
	addq	%rax, %rsi
	shrq	%cl, %rsi
	jmp	.L3046
.L3335:
	cmpl	$-52, %ecx
	jl	.L3010
	movabsq	$4503599627370495, %rsi
	movl	$1075, %ecx
	movabsq	$4503599627370496, %rax
	andq	%rdi, %rsi
	subl	%edx, %ecx
	addq	%rax, %rsi
	shrq	%cl, %rsi
	jmp	.L3013
.L2965:
	ucomisd	.LC27(%rip), %xmm0
	jp	.L2595
	jne	.L2595
	movq	-136(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler22MachineOperatorReducer14Float64PowHalfEPNS1_4NodeE
	jmp	.L2672
.L3305:
	pxor	%xmm1, %xmm1
	cvtsd2ss	%xmm0, %xmm1
	jmp	.L3034
.L3354:
	movq	16(%r13), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.0
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.1
	movq	16(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Int32SubEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	%r12, %rax
	jmp	.L2672
.L3343:
	cmpl	$15, %edx
	je	.L2814
	movq	%r12, %rcx
	cmpq	%r15, %rbx
	je	.L2816
.L2815:
	leaq	-24(%rcx), %rsi
	movq	%r15, %rdi
	movl	%r8d, -224(%rbp)
	movq	%r9, -232(%rbp)
	movq	%rsi, -216(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-232(%rbp), %r9
	movq	-216(%rbp), %rsi
	movq	%rbx, %rdi
	movq	%rbx, (%r9)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movzbl	23(%r12), %eax
	movl	-224(%rbp), %r8d
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L2817
.L2816:
	movq	8(%r14), %rdi
	cmpq	%r15, %rdi
	je	.L3063
	leaq	8(%r14), %rdx
	movq	%r12, %rax
.L2819:
	leaq	-48(%rax), %r14
	testq	%rdi, %rdi
	je	.L2820
	movq	%r14, %rsi
	movq	%rdx, -224(%rbp)
	movl	%r8d, -216(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movl	-216(%rbp), %r8d
	movq	-224(%rbp), %rdx
.L2820:
	movq	%r15, (%rdx)
	movq	%r14, %rsi
	movq	%r15, %rdi
	movl	%r8d, -216(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movl	-216(%rbp), %r8d
.L3063:
	testl	%r8d, %r8d
	je	.L2825
	movq	%r15, %rax
	movl	%r8d, %r14d
	movq	%rbx, %r15
	movq	%rax, %rbx
	jmp	.L3081
.L3351:
	cmpl	$15, %edx
	je	.L2701
	movq	%r12, %rcx
	cmpq	%r14, %rbx
	je	.L2703
.L2702:
	leaq	-24(%rcx), %rsi
	movq	%r14, %rdi
	movl	%r9d, -224(%rbp)
	movq	%r8, -232(%rbp)
	movq	%rsi, -216(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-232(%rbp), %r8
	movq	-216(%rbp), %rsi
	movq	%rbx, %rdi
	movq	%rbx, (%r8)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movzbl	23(%r12), %eax
	movl	-224(%rbp), %r9d
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L2704
.L2703:
	movq	8(%r15), %rdi
	cmpq	%r14, %rdi
	je	.L3060
	leaq	8(%r15), %rdx
	movq	%r12, %rax
.L2706:
	leaq	-48(%rax), %r15
	testq	%rdi, %rdi
	je	.L2707
	movq	%r15, %rsi
	movq	%rdx, -224(%rbp)
	movl	%r9d, -216(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movl	-216(%rbp), %r9d
	movq	-224(%rbp), %rdx
.L2707:
	movq	%r14, (%rdx)
	movq	%r15, %rsi
	movq	%r14, %rdi
	movl	%r9d, -216(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movl	-216(%rbp), %r9d
.L3060:
	movq	(%rbx), %rax
	cmpw	$308, 16(%rax)
	je	.L3117
	movq	%r14, %rax
	movq	%rbx, %r14
	movq	%rax, %rbx
	jmp	.L2700
.L3348:
	cmpl	$15, %eax
	je	.L2838
	movq	%r12, %rcx
	cmpq	%r15, %rbx
	je	.L2840
.L2839:
	leaq	-24(%rcx), %rsi
	movq	%r15, %rdi
	movl	%r8d, -224(%rbp)
	movq	%r9, -232(%rbp)
	movq	%rsi, -216(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-232(%rbp), %r9
	movq	-216(%rbp), %rsi
	movq	%rbx, %rdi
	movq	%rbx, (%r9)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movzbl	23(%r12), %eax
	movl	-224(%rbp), %r8d
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L2841
.L2840:
	movq	8(%r14), %rdi
	cmpq	%r15, %rdi
	je	.L3064
	addq	$8, %r14
.L2843:
	subq	$48, %r12
	testq	%rdi, %rdi
	je	.L2844
	movq	%r12, %rsi
	movl	%r8d, -216(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movl	-216(%rbp), %r8d
.L2844:
	movq	%r15, (%r14)
	movq	%r12, %rsi
	movq	%r15, %rdi
	movl	%r8d, -216(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movl	-216(%rbp), %r8d
.L3064:
	addl	$1, %r8d
	je	.L2849
	movq	%r15, %rax
	movq	%rbx, %r15
	movq	%rax, %rbx
	jmp	.L2847
.L3344:
	cmpl	$15, %eax
	je	.L2861
	movq	%r12, %r9
	cmpq	%rbx, %r14
	je	.L2863
.L2862:
	leaq	-24(%r9), %rsi
	movq	%rbx, %rdi
	movss	%xmm0, -232(%rbp)
	movq	%r8, -224(%rbp)
	movq	%rsi, -216(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-224(%rbp), %r8
	movq	-216(%rbp), %rsi
	movq	%r14, %rdi
	movq	%r14, (%r8)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movzbl	23(%r12), %eax
	movss	-232(%rbp), %xmm0
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L2864
.L2863:
	movq	8(%r15), %rdi
	cmpq	%rbx, %rdi
	je	.L3066
	leaq	8(%r15), %rdx
	movq	%r12, %rax
.L2866:
	leaq	-48(%rax), %r15
	testq	%rdi, %rdi
	je	.L2867
	movq	%r15, %rsi
	movq	%rdx, -216(%rbp)
	movss	%xmm0, -224(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-216(%rbp), %rdx
	movss	-224(%rbp), %xmm0
.L2867:
	movq	%rbx, (%rdx)
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movss	%xmm0, -216(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movss	-216(%rbp), %xmm0
.L3066:
	movzbl	24(%r13), %eax
	movaps	%xmm0, %xmm1
	testb	%al, %al
	jne	.L3118
	movq	%rbx, %r14
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	jmp	.L2859
.L2739:
	addl	$1, %r8d
	cmpl	$8, %esi
	jne	.L2742
.L2738:
	addl	$1, %r8d
	jmp	.L2741
.L3319:
	movq	-112(%rbp), %rax
	jmp	.L2672
.L3320:
	movq	-136(%rbp), %rax
	jmp	.L2672
.L3345:
	subsd	%xmm0, %xmm0
	movq	16(%r13), %rdi
	call	_ZN2v88internal8compiler12MachineGraph15Float64ConstantEd@PLT
	jmp	.L2672
.L3322:
	subss	%xmm0, %xmm0
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler22MachineOperatorReducer15Float32ConstantEf
	jmp	.L2672
.L3321:
	subss	%xmm1, %xmm1
	movq	%r13, %rdi
	movaps	%xmm1, %xmm0
	call	_ZN2v88internal8compiler22MachineOperatorReducer15Float32ConstantEf
	jmp	.L2672
.L3349:
	subsd	%xmm0, %xmm0
	movq	16(%r13), %rdi
	call	_ZN2v88internal8compiler12MachineGraph15Float64ConstantEd@PLT
	jmp	.L2672
.L3346:
	movsd	-128(%rbp), %xmm1
	ucomisd	%xmm1, %xmm1
	jp	.L2919
	pxor	%xmm2, %xmm2
	ucomisd	%xmm2, %xmm0
	jp	.L3131
	jne	.L3131
	ucomisd	%xmm2, %xmm1
	movl	$0, %edx
	setnp	%al
	cmovne	%edx, %eax
	testb	%al, %al
	jne	.L3098
	ucomisd	%xmm1, %xmm1
	jp	.L3098
	jne	.L3098
	comisd	%xmm2, %xmm1
	movmskpd	%xmm0, %eax
	setnb	%dl
	testb	$1, %al
	sete	%al
	cmpb	%al, %dl
	je	.L3099
	movsd	.LC2(%rip), %xmm0
.L2925:
	movq	16(%r13), %rdi
	call	_ZN2v88internal8compiler12MachineGraph15Float64ConstantEd@PLT
	jmp	.L2672
.L3131:
	divsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	jmp	.L2925
.L3098:
	movsd	.LC13(%rip), %xmm0
	jmp	.L2925
.L3341:
	movq	-136(%rbp), %rax
	jmp	.L2672
.L2795:
	cmpq	%r15, %rbx
	jne	.L2796
.L2798:
	movq	(%r14), %r12
	movq	24(%r12), %rdi
	cmpq	%r15, %rdi
	je	.L3062
	leaq	24(%r12), %r14
	jmp	.L2800
.L2838:
	cmpq	%r15, %rbx
	jne	.L2839
.L2841:
	movq	(%r14), %r12
	movq	24(%r12), %rdi
	cmpq	%r15, %rdi
	je	.L3064
	leaq	24(%r12), %r14
	jmp	.L2843
.L2773:
	cmpq	%r14, %rbx
	jne	.L2774
.L2776:
	movq	(%r15), %r12
	movq	24(%r12), %rdi
	cmpq	%rdi, %r14
	je	.L3061
	leaq	24(%r12), %r15
	jmp	.L2778
.L3315:
	call	__stack_chk_fail@PLT
.L3352:
	subsd	%xmm0, %xmm0
	movq	16(%r13), %rdi
	call	_ZN2v88internal8compiler12MachineGraph15Float64ConstantEd@PLT
	jmp	.L2672
.L2755:
	cmpq	%r14, %r15
	jne	.L2756
.L2758:
	movq	(%rbx), %rax
	movq	24(%rax), %rdi
	cmpq	%rdi, %r15
	je	.L3059
	leaq	24(%rax), %rdx
	jmp	.L2760
.L2861:
	cmpq	%rbx, %r14
	jne	.L2862
.L2864:
	movq	(%r15), %rax
	movq	24(%rax), %rdi
	cmpq	%rbx, %rdi
	je	.L3066
	leaq	24(%rax), %rdx
	jmp	.L2866
.L2814:
	cmpq	%r15, %rbx
	jne	.L2815
.L2817:
	movq	(%r14), %rax
	movq	24(%rax), %rdi
	cmpq	%r15, %rdi
	je	.L3063
	leaq	24(%rax), %rdx
	jmp	.L2819
.L2724:
	cmpq	%r14, %rax
	jne	.L2725
.L2727:
	movq	(%rbx), %rdx
	movq	24(%rdx), %rdi
	cmpq	%rdi, %r14
	je	.L3058
	leaq	24(%rdx), %rbx
	jmp	.L2729
.L2683:
	cmpq	%r14, %rax
	jne	.L2684
.L2686:
	movq	(%rbx), %r12
	movq	24(%r12), %rdi
	cmpq	%rdi, %rax
	je	.L3055
	leaq	24(%r12), %rbx
	jmp	.L2688
.L2701:
	cmpq	%r14, %rbx
	jne	.L2702
.L2704:
	movq	(%r15), %rax
	movq	24(%rax), %rdi
	cmpq	%rdi, %r14
	je	.L3060
	leaq	24(%rax), %rdx
	jmp	.L2706
.L3025:
	addsd	%xmm0, %xmm0
	comisd	.LC5(%rip), %xmm0
	jbe	.L3303
	movsd	.LC6(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jnb	.L3107
	movss	.LC15(%rip), %xmm0
.L3031:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler22MachineOperatorReducer15Float32ConstantEf
	jmp	.L2672
.L3099:
	movsd	.LC3(%rip), %xmm0
	jmp	.L2925
.L3117:
	movq	%r14, %rdx
	movl	%r9d, %eax
	movq	%rbx, %r14
	movq	%rdx, %rbx
	jmp	.L3080
.L3334:
	je	.L3010
	jmp	.L3007
.L3327:
	je	.L3043
	jmp	.L3040
.L3303:
	movsd	.LC7(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jbe	.L3304
	comisd	.LC8(%rip), %xmm0
	jnb	.L3108
	movss	.LC17(%rip), %xmm0
	jmp	.L3031
.L2973:
	addss	%xmm0, %xmm0
	cvtss2sd	%xmm0, %xmm0
	call	_ZN2v88internal8compiler12MachineGraph15Float64ConstantEd@PLT
	jmp	.L2672
.L3107:
	movss	.LC14(%rip), %xmm0
	jmp	.L3031
.L3356:
	movzbl	23(%r14), %eax
	movq	32(%r14), %rcx
	leaq	32(%r14), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2880
	leaq	16(%rcx), %rdx
.L2880:
	movq	(%rdx), %rsi
	movq	(%rsi), %rax
	cmpw	$351, 16(%rax)
	jne	.L2879
	leaq	-144(%rbp), %rdi
	movss	%xmm2, -216(%rbp)
	call	_ZN2v88internal8compiler12BinopMatcherINS1_12FloatMatcherIfLNS1_8IrOpcode5ValueE25EEES6_EC1EPNS1_4NodeE
	cmpb	$0, -124(%rbp)
	je	.L2879
	movss	-128(%rbp), %xmm0
	movss	-216(%rbp), %xmm2
	ucomiss	%xmm2, %xmm0
	jp	.L2879
	jne	.L2879
	movd	%xmm0, %eax
	testl	%eax, %eax
	jns	.L2879
	movq	16(%r13), %rax
	movq	-120(%rbp), %rbx
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder14Float32RoundUpEv@PLT
	leaq	-80(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movq	%rdx, %rsi
	movl	$1, %edx
	movq	%rbx, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	jmp	.L2672
.L3299:
	movl	$-2147483648, %esi
	jmp	.L2977
.L3355:
	movq	32(%rax), %rcx
	leaq	32(%rax), %rdx
	movzbl	23(%rax), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2899
	leaq	16(%rcx), %rdx
.L2899:
	movq	(%rdx), %rsi
	movq	(%rsi), %rax
	cmpw	$367, 16(%rax)
	jne	.L2898
	leaq	-144(%rbp), %rdi
	movsd	%xmm2, -216(%rbp)
	call	_ZN2v88internal8compiler12BinopMatcherINS1_12FloatMatcherIdLNS1_8IrOpcode5ValueE26EEES6_EC1EPNS1_4NodeE
	cmpb	$0, -120(%rbp)
	je	.L2898
	movsd	-128(%rbp), %xmm0
	movsd	-216(%rbp), %xmm2
	ucomisd	%xmm2, %xmm0
	jp	.L2898
	jne	.L2898
	movmskpd	%xmm0, %eax
	testb	$1, %al
	je	.L2898
	movq	16(%r13), %rax
	movq	-112(%rbp), %rbx
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder14Float64RoundUpEv@PLT
	leaq	-80(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movq	%rdx, %rsi
	movl	$1, %edx
	movq	%rbx, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	jmp	.L2672
.L3304:
	cvtsd2ss	%xmm0, %xmm0
	jmp	.L3031
.L2742:
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L3108:
	movss	.LC16(%rip), %xmm0
	jmp	.L3031
.L3111:
	xorl	%esi, %esi
	jmp	.L3043
.L3103:
	xorl	%esi, %esi
	jmp	.L3010
	.cfi_endproc
.LFE19912:
	.size	_ZN2v88internal8compiler22MachineOperatorReducer6ReduceEPNS1_4NodeE, .-_ZN2v88internal8compiler22MachineOperatorReducer6ReduceEPNS1_4NodeE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler22MachineOperatorReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_12MachineGraphEb,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler22MachineOperatorReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_12MachineGraphEb, @function
_GLOBAL__sub_I__ZN2v88internal8compiler22MachineOperatorReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_12MachineGraphEb:
.LFB23932:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE23932:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler22MachineOperatorReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_12MachineGraphEb, .-_GLOBAL__sub_I__ZN2v88internal8compiler22MachineOperatorReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_12MachineGraphEb
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler22MachineOperatorReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_12MachineGraphEb
	.weak	_ZTVN2v88internal8compiler22MachineOperatorReducerE
	.section	.data.rel.ro._ZTVN2v88internal8compiler22MachineOperatorReducerE,"awG",@progbits,_ZTVN2v88internal8compiler22MachineOperatorReducerE,comdat
	.align 8
	.type	_ZTVN2v88internal8compiler22MachineOperatorReducerE, @object
	.size	_ZTVN2v88internal8compiler22MachineOperatorReducerE, 56
_ZTVN2v88internal8compiler22MachineOperatorReducerE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal8compiler22MachineOperatorReducerD1Ev
	.quad	_ZN2v88internal8compiler22MachineOperatorReducerD0Ev
	.quad	_ZNK2v88internal8compiler22MachineOperatorReducer12reducer_nameEv
	.quad	_ZN2v88internal8compiler22MachineOperatorReducer6ReduceEPNS1_4NodeE
	.quad	_ZN2v88internal8compiler7Reducer8FinalizeEv
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	0
	.long	0
	.align 8
.LC2:
	.long	0
	.long	-1048576
	.align 8
.LC3:
	.long	0
	.long	2146435072
	.align 8
.LC5:
	.long	3758096384
	.long	1206910975
	.align 8
.LC6:
	.long	4026531839
	.long	1206910975
	.align 8
.LC7:
	.long	3758096384
	.long	-940572673
	.align 8
.LC8:
	.long	4026531839
	.long	-940572673
	.align 8
.LC9:
	.long	0
	.long	1127219200
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC10:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC11:
	.long	0
	.long	1072693248
	.align 8
.LC13:
	.long	0
	.long	2146959360
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC14:
	.long	2139095039
	.align 4
.LC15:
	.long	2139095040
	.align 4
.LC16:
	.long	4286578687
	.align 4
.LC17:
	.long	4286578688
	.section	.rodata.cst16
	.align 16
.LC18:
	.long	0
	.long	1072693248
	.long	0
	.long	0
	.align 16
.LC19:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC20:
	.long	0
	.long	-1074790400
	.align 8
.LC21:
	.long	0
	.long	-2147483648
	.align 8
.LC22:
	.long	0
	.long	1073741824
	.align 8
.LC23:
	.long	4294967295
	.long	2146435071
	.align 8
.LC24:
	.long	0
	.long	1048576
	.align 8
.LC25:
	.long	0
	.long	-1073741824
	.align 8
.LC26:
	.long	0
	.long	-1075838976
	.align 8
.LC27:
	.long	0
	.long	1071644672
	.align 8
.LC28:
	.long	0
	.long	-1042284544
	.align 8
.LC29:
	.long	4290772992
	.long	1105199103
	.align 8
.LC30:
	.long	0
	.long	-1020264448
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
