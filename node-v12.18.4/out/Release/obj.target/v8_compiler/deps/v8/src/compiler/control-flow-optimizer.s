	.file	"control-flow-optimizer.cc"
	.text
	.section	.text._ZNSt8_Rb_treeIiiSt9_IdentityIiESt4lessIiEN2v88internal13ZoneAllocatorIiEEE8_M_eraseEPSt13_Rb_tree_nodeIiE,"axG",@progbits,_ZNSt8_Rb_treeIiiSt9_IdentityIiESt4lessIiEN2v88internal13ZoneAllocatorIiEEE8_M_eraseEPSt13_Rb_tree_nodeIiE,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIiiSt9_IdentityIiESt4lessIiEN2v88internal13ZoneAllocatorIiEEE8_M_eraseEPSt13_Rb_tree_nodeIiE
	.type	_ZNSt8_Rb_treeIiiSt9_IdentityIiESt4lessIiEN2v88internal13ZoneAllocatorIiEEE8_M_eraseEPSt13_Rb_tree_nodeIiE, @function
_ZNSt8_Rb_treeIiiSt9_IdentityIiESt4lessIiEN2v88internal13ZoneAllocatorIiEEE8_M_eraseEPSt13_Rb_tree_nodeIiE:
.LFB12659:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L9
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L3:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIiiSt9_IdentityIiESt4lessIiEN2v88internal13ZoneAllocatorIiEEE8_M_eraseEPSt13_Rb_tree_nodeIiE
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L3
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE12659:
	.size	_ZNSt8_Rb_treeIiiSt9_IdentityIiESt4lessIiEN2v88internal13ZoneAllocatorIiEEE8_M_eraseEPSt13_Rb_tree_nodeIiE, .-_ZNSt8_Rb_treeIiiSt9_IdentityIiESt4lessIiEN2v88internal13ZoneAllocatorIiEEE8_M_eraseEPSt13_Rb_tree_nodeIiE
	.section	.text._ZNSt8_Rb_treeIiiSt9_IdentityIiESt4lessIiEN2v88internal13ZoneAllocatorIiEEE16_M_insert_uniqueIRKiEESt4pairISt17_Rb_tree_iteratorIiEbEOT_,"axG",@progbits,_ZNSt8_Rb_treeIiiSt9_IdentityIiESt4lessIiEN2v88internal13ZoneAllocatorIiEEE16_M_insert_uniqueIRKiEESt4pairISt17_Rb_tree_iteratorIiEbEOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIiiSt9_IdentityIiESt4lessIiEN2v88internal13ZoneAllocatorIiEEE16_M_insert_uniqueIRKiEESt4pairISt17_Rb_tree_iteratorIiEbEOT_
	.type	_ZNSt8_Rb_treeIiiSt9_IdentityIiESt4lessIiEN2v88internal13ZoneAllocatorIiEEE16_M_insert_uniqueIRKiEESt4pairISt17_Rb_tree_iteratorIiEbEOT_, @function
_ZNSt8_Rb_treeIiiSt9_IdentityIiESt4lessIiEN2v88internal13ZoneAllocatorIiEEE16_M_insert_uniqueIRKiEESt4pairISt17_Rb_tree_iteratorIiEbEOT_:
.LFB12661:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	16(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	24(%rdi), %r12
	testq	%r12, %r12
	je	.L27
	movl	(%rsi), %esi
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L34:
	movq	16(%r12), %rax
	movl	$1, %edx
	testq	%rax, %rax
	je	.L16
.L35:
	movq	%rax, %r12
.L15:
	movl	32(%r12), %ecx
	cmpl	%ecx, %esi
	jl	.L34
	movq	24(%r12), %rax
	xorl	%edx, %edx
	testq	%rax, %rax
	jne	.L35
.L16:
	testb	%dl, %dl
	jne	.L14
	cmpl	%ecx, %esi
	jle	.L20
.L25:
	movl	$1, %r8d
	cmpq	%r12, %r15
	jne	.L36
.L21:
	movq	(%rbx), %rdi
	movq	16(%rdi), %r13
	movq	24(%rdi), %rax
	subq	%r13, %rax
	cmpq	$39, %rax
	jbe	.L37
	leaq	40(%r13), %rax
	movq	%rax, 16(%rdi)
.L23:
	movl	(%r14), %eax
	movq	%r12, %rdx
	movq	%r15, %rcx
	movq	%r13, %rsi
	movl	%r8d, %edi
	movl	%eax, 32(%r13)
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 48(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	movl	$1, %edx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	.cfi_restore_state
	movq	%r15, %r12
.L14:
	cmpq	%r12, 32(%rbx)
	je	.L25
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movl	(%r14), %ecx
	cmpl	%ecx, 32(%rax)
	jl	.L25
	movq	%rax, %r12
.L20:
	addq	$24, %rsp
	movq	%r12, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	.cfi_restore_state
	xorl	%r8d, %r8d
	movl	32(%r12), %eax
	cmpl	%eax, (%r14)
	setl	%r8b
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L37:
	movl	$40, %esi
	movl	%r8d, -52(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-52(%rbp), %r8d
	movq	%rax, %r13
	jmp	.L23
	.cfi_endproc
.LFE12661:
	.size	_ZNSt8_Rb_treeIiiSt9_IdentityIiESt4lessIiEN2v88internal13ZoneAllocatorIiEEE16_M_insert_uniqueIRKiEESt4pairISt17_Rb_tree_iteratorIiEbEOT_, .-_ZNSt8_Rb_treeIiiSt9_IdentityIiESt4lessIiEN2v88internal13ZoneAllocatorIiEEE16_M_insert_uniqueIRKiEESt4pairISt17_Rb_tree_iteratorIiEbEOT_
	.section	.rodata._ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"cannot create std::deque larger than max_size()"
	.section	.text._ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_,"axG",@progbits,_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	.type	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_, @function
_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_:
.LFB12950:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	88(%rdi), %r13
	movq	56(%rdi), %rsi
	movq	64(%rbx), %rdx
	subq	72(%rbx), %rdx
	movq	%r13, %r14
	movq	%rdx, %rcx
	subq	%rsi, %r14
	sarq	$3, %rcx
	movq	%r14, %rdi
	sarq	$3, %rdi
	leaq	-1(%rdi), %rax
	salq	$6, %rax
	leaq	(%rcx,%rax), %rdx
	movq	48(%rbx), %rax
	subq	32(%rbx), %rax
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpq	$268435455, %rax
	je	.L56
	movq	16(%rbx), %rcx
	movq	24(%rbx), %rdx
	movq	%r13, %rax
	subq	%rcx, %rax
	movq	%rdx, %r8
	sarq	$3, %rax
	subq	%rax, %r8
	cmpq	$1, %r8
	jbe	.L57
.L40:
	movq	8(%rbx), %rax
	testq	%rax, %rax
	je	.L48
	cmpq	$63, 8(%rax)
	ja	.L58
.L48:
	movq	(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L59
	leaq	512(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L49:
	movq	%rax, 8(%r13)
	movq	(%r12), %rdx
	movq	64(%rbx), %rax
	movq	%rdx, (%rax)
	movq	88(%rbx), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 88(%rbx)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 72(%rbx)
	movq	%rdx, 80(%rbx)
	movq	%rax, 64(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	.cfi_restore_state
	leaq	2(%rdi), %r15
	leaq	(%r15,%r15), %rax
	cmpq	%rax, %rdx
	ja	.L60
	testq	%rdx, %rdx
	movl	$1, %eax
	movq	(%rbx), %rdi
	cmovne	%rdx, %rax
	movq	16(%rdi), %r13
	leaq	2(%rdx,%rax), %rcx
	movq	24(%rdi), %rax
	leaq	0(,%rcx,8), %rsi
	subq	%r13, %rax
	cmpq	%rax, %rsi
	ja	.L61
	addq	%r13, %rsi
	movq	%rsi, 16(%rdi)
.L45:
	movq	%rcx, %rax
	movq	56(%rbx), %rsi
	subq	%r15, %rax
	shrq	%rax
	leaq	0(%r13,%rax,8), %r15
	movq	88(%rbx), %rax
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L46
	subq	%rsi, %rdx
	movq	%r15, %rdi
	movq	%rcx, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %rcx
.L46:
	movq	24(%rbx), %rax
	leaq	0(,%rax,8), %rdx
	cmpq	$15, %rdx
	jbe	.L47
	movq	16(%rbx), %rdx
	movq	%rax, 8(%rdx)
	movq	$0, (%rdx)
.L47:
	movq	%r13, 16(%rbx)
	movq	%rcx, 24(%rbx)
.L43:
	movq	%r15, 56(%rbx)
	movq	(%r15), %rax
	leaq	(%r15,%r14), %r13
	movq	(%r15), %xmm0
	movq	%r13, 88(%rbx)
	addq	$512, %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 40(%rbx)
	movq	0(%r13), %rax
	movq	%rax, 72(%rbx)
	addq	$512, %rax
	movq	%rax, 80(%rbx)
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L58:
	movq	(%rax), %rdx
	movq	%rdx, 8(%rbx)
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L60:
	subq	%r15, %rdx
	addq	$8, %r13
	shrq	%rdx
	leaq	(%rcx,%rdx,8), %r15
	movq	%r13, %rdx
	subq	%rsi, %rdx
	cmpq	%r15, %rsi
	jbe	.L42
	cmpq	%r13, %rsi
	je	.L43
	movq	%r15, %rdi
	call	memmove@PLT
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L42:
	cmpq	%r13, %rsi
	je	.L43
	leaq	8(%r14), %rdi
	subq	%rdx, %rdi
	addq	%r15, %rdi
	call	memmove@PLT
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L59:
	movl	$512, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L61:
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	movq	%rax, %r13
	jmp	.L45
.L56:
	leaq	.LC0(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE12950:
	.size	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_, .-_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	.section	.text._ZN2v88internal8compiler20ControlFlowOptimizer7EnqueueEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20ControlFlowOptimizer7EnqueueEPNS1_4NodeE
	.type	_ZN2v88internal8compiler20ControlFlowOptimizer7EnqueueEPNS1_4NodeE, @function
_ZN2v88internal8compiler20ControlFlowOptimizer7EnqueueEPNS1_4NodeE:
.LFB11422:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	leaq	32(%rsi), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movzbl	23(%rsi), %edx
	movq	%rsi, -8(%rbp)
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L64
	movq	32(%rsi), %rcx
	movl	8(%rcx), %edx
	addq	$16, %rcx
.L64:
	testl	%edx, %edx
	jle	.L65
	cmpq	$0, (%rcx)
	je	.L62
.L65:
	movl	120(%rdi), %edx
	cmpl	%edx, 16(%rax)
	ja	.L62
	addl	$1, %edx
	movl	%edx, 16(%rax)
	movq	104(%rdi), %rsi
	movq	88(%rdi), %rcx
	leaq	-8(%rsi), %rdx
	cmpq	%rdx, %rcx
	je	.L67
	movq	%rax, (%rcx)
	addq	$8, 88(%rdi)
.L62:
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	.cfi_restore_state
	leaq	-8(%rbp), %rsi
	addq	$24, %rdi
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11422:
	.size	_ZN2v88internal8compiler20ControlFlowOptimizer7EnqueueEPNS1_4NodeE, .-_ZN2v88internal8compiler20ControlFlowOptimizer7EnqueueEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler20ControlFlowOptimizer9VisitNodeEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20ControlFlowOptimizer9VisitNodeEPNS1_4NodeE
	.type	_ZN2v88internal8compiler20ControlFlowOptimizer9VisitNodeEPNS1_4NodeE, @function
_ZN2v88internal8compiler20ControlFlowOptimizer9VisitNodeEPNS1_4NodeE:
.LFB11423:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	24(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%r13, %r13
	je	.L71
	movq	0(%r13), %r14
	movq	%rdi, %rbx
	leaq	-48(%rbp), %r12
	.p2align 4,,10
	.p2align 3
.L81:
	movl	16(%r13), %ecx
	movq	%r13, %rdi
	movl	%ecx, %edx
	shrl	%edx
	leaq	3(%rdx,%rdx,2), %rax
	leaq	0(%r13,%rax,8), %rax
	leaq	32(%rax), %rsi
	addq	$16, %rax
	andl	$1, %ecx
	cmovne	%rsi, %rax
	leaq	(%rax,%rdx,8), %rsi
	call	_ZN2v88internal8compiler14NodeProperties13IsControlEdgeENS1_4EdgeE@PLT
	testb	%al, %al
	je	.L74
	movl	16(%r13), %edx
	movl	%edx, %eax
	shrl	%eax
	andl	$1, %edx
	leaq	3(%rax,%rax,2), %rax
	leaq	0(%r13,%rax,8), %rax
	jne	.L75
	movq	(%rax), %rax
.L75:
	movzbl	23(%rax), %edx
	movq	%rax, -48(%rbp)
	leaq	32(%rax), %rcx
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L77
	movq	32(%rax), %rcx
	movl	8(%rcx), %edx
	addq	$16, %rcx
.L77:
	testl	%edx, %edx
	jle	.L78
	cmpq	$0, (%rcx)
	je	.L74
.L78:
	movl	120(%rbx), %edx
	cmpl	%edx, 16(%rax)
	ja	.L74
	addl	$1, %edx
	movl	%edx, 16(%rax)
	movq	104(%rbx), %rdi
	movq	88(%rbx), %rcx
	leaq	-8(%rdi), %rdx
	cmpq	%rdx, %rcx
	je	.L80
	movq	%rax, (%rcx)
	addq	$8, 88(%rbx)
.L74:
	testq	%r14, %r14
	je	.L71
	movq	%r14, %r13
	movq	(%r14), %r14
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L71:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L93
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L80:
	.cfi_restore_state
	leaq	24(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	jmp	.L74
.L93:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11423:
	.size	_ZN2v88internal8compiler20ControlFlowOptimizer9VisitNodeEPNS1_4NodeE, .-_ZN2v88internal8compiler20ControlFlowOptimizer9VisitNodeEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler20ControlFlowOptimizer14TryBuildSwitchEPNS1_4NodeE.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler20ControlFlowOptimizer14TryBuildSwitchEPNS1_4NodeE.part.0, @function
_ZN2v88internal8compiler20ControlFlowOptimizer14TryBuildSwitchEPNS1_4NodeE.part.0:
.LFB13486:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	pushq	%rbx
	subq	$264, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -192(%rbp)
	xorl	%esi, %esi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	(%rax), %rdx
	cmpw	$334, 16(%rdx)
	je	.L95
.L105:
	xorl	%r14d, %r14d
.L94:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L239
	addq	$264, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L95:
	.cfi_restore_state
	movzbl	23(%rax), %ecx
	movq	32(%rax), %rsi
	leaq	32(%rax), %rbx
	andl	$15, %ecx
	cmpl	$15, %ecx
	je	.L97
	movq	(%rsi), %rdi
	cmpw	$23, 16(%rdi)
	jne	.L240
	movl	44(%rdi), %r14d
	movl	$1, %r8d
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L240:
	xorl	%r8d, %r8d
	xorl	%r14d, %r14d
.L99:
	movq	%rsi, -248(%rbp)
	leaq	8(%rbx), %rdi
	movq	%rbx, %r15
.L102:
	movq	(%rdi), %r12
	movq	(%r12), %rdi
	cmpw	$23, 16(%rdi)
	jne	.L103
	movl	44(%rdi), %r14d
.L104:
	movq	128(%r13), %rax
	leaq	-112(%rbp), %rdi
	movl	%r14d, -164(%rbp)
	movl	$0, -96(%rbp)
	movq	%rax, -112(%rbp)
	leaq	-96(%rbp), %rax
	movq	%rax, -216(%rbp)
	movq	%rax, -80(%rbp)
	movq	%rax, -72(%rbp)
	leaq	-164(%rbp), %rax
	movq	%rax, %rsi
	movq	$0, -88(%rbp)
	movq	%rdi, -240(%rbp)
	movq	$0, -64(%rbp)
	movq	%rax, -256(%rbp)
	call	_ZNSt8_Rb_treeIiiSt9_IdentityIiESt4lessIiEN2v88internal13ZoneAllocatorIiEEE16_M_insert_uniqueIRKiEESt4pairISt17_Rb_tree_iteratorIiEbEOT_
	movq	-192(%rbp), %rax
	movl	$1, -204(%rbp)
	movq	%rax, -184(%rbp)
	leaq	-144(%rbp), %rax
	movq	-184(%rbp), %rsi
	movq	%rax, -224(%rbp)
	leaq	-160(%rbp), %rax
	movq	%rax, -232(%rbp)
	.p2align 4,,10
	.p2align 3
.L146:
	movq	-224(%rbp), %rdi
	call	_ZN2v88internal8compiler13BranchMatcherC1EPNS1_4NodeE@PLT
	movq	-128(%rbp), %rax
	movq	-136(%rbp), %r12
	movq	%rax, -200(%rbp)
	movq	24(%rax), %rax
	movq	%rax, -160(%rbp)
	testq	%rax, %rax
	je	.L113
	movq	-232(%rbp), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler4Node4Uses14const_iteratorppEi@PLT
	movl	16(%rax), %ecx
	movl	%ecx, %edx
	shrl	%edx
	andl	$1, %ecx
	leaq	3(%rdx,%rdx,2), %rdx
	leaq	(%rax,%rdx,8), %r14
	jne	.L114
	movq	(%r14), %r14
.L114:
	movq	(%r14), %rdi
	cmpw	$2, 16(%rdi)
	jne	.L113
	call	_ZN2v88internal8compiler12BranchHintOfEPKNS1_8OperatorE@PLT
	testb	%al, %al
	jne	.L113
	cmpq	$0, -160(%rbp)
	jne	.L113
	movzbl	23(%r14), %eax
	movq	32(%r14), %r15
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L115
	movq	16(%r15), %r15
.L115:
	movq	(%r15), %rcx
	cmpw	$334, 16(%rcx)
	jne	.L113
	movzbl	23(%r15), %eax
	movq	32(%r15), %rdx
	leaq	32(%r15), %r8
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L116
	movq	(%rdx), %rsi
	cmpw	$23, 16(%rsi)
	je	.L117
	xorl	%ebx, %ebx
	xorl	%edi, %edi
.L118:
	leaq	8(%r8), %rsi
	movq	%rdx, %r10
	movq	%r8, %r11
.L121:
	movq	(%rsi), %r9
	movq	(%r9), %rsi
	cmpw	$23, 16(%rsi)
	jne	.L122
	movl	44(%rsi), %ebx
	cmpq	-248(%rbp), %r10
	jne	.L113
.L123:
	movq	-88(%rbp), %rax
	testq	%rax, %rax
	je	.L131
	movq	-216(%rbp), %rdx
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L241:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L133
.L132:
	cmpl	%ebx, 32(%rax)
	jge	.L241
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L132
.L133:
	cmpq	-216(%rbp), %rdx
	je	.L131
	cmpl	%ebx, 32(%rdx)
	jg	.L131
.L113:
	movq	-192(%rbp), %rax
	movq	-184(%rbp), %rcx
	xorl	%r14d, %r14d
	cmpq	%rcx, %rax
	je	.L147
	movq	32(%rax), %rdi
	movq	%rax, %r14
	leaq	32(%rax), %rbx
	movzbl	23(%rax), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L148
	cmpq	-248(%rbp), %rdi
	je	.L150
.L149:
	subq	$24, %r14
	testq	%rdi, %rdi
	je	.L151
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L151:
	movq	-248(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L150:
	movq	-64(%rbp), %rax
	movq	8(%r13), %rdi
	leaq	32(%r12), %rbx
	leaq	1(%rax), %rsi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6SwitchEm@PLT
	movq	-192(%rbp), %r14
	movq	%rax, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L152
	movq	32(%r12), %rdi
	movq	%rbx, %r15
	movq	%r12, %rax
	cmpq	%rdi, %r14
	je	.L154
.L153:
	leaq	-24(%rax), %r14
	testq	%rdi, %rdi
	je	.L155
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L155:
	movq	-192(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, (%r15)
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L154:
	movq	8(%r13), %rdi
	movl	-204(%rbp), %edx
	xorl	%ecx, %ecx
	movl	-164(%rbp), %esi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfValueEiiNS1_10BranchHintE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	%r12, -144(%rbp)
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L157
	movq	32(%r12), %rbx
	movl	8(%rbx), %eax
	addq	$16, %rbx
.L157:
	testl	%eax, %eax
	jle	.L158
	cmpq	$0, (%rbx)
	je	.L159
.L158:
	movl	120(%r13), %eax
	cmpl	%eax, 16(%r12)
	ja	.L159
	addl	$1, %eax
	movl	%eax, 16(%r12)
	movq	104(%r13), %rax
	movq	88(%r13), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L160
	movq	%r12, (%rdx)
	addq	$8, 88(%r13)
.L159:
	movq	-200(%rbp), %rcx
	movzbl	23(%rcx), %eax
	leaq	32(%rcx), %rbx
	andl	$15, %eax
	cmpl	$15, %eax
	movq	%rcx, %rax
	je	.L161
	movq	32(%rcx), %rdi
	movq	%rbx, %r14
	cmpq	%rdi, -192(%rbp)
	je	.L163
.L162:
	leaq	-24(%rax), %r12
	testq	%rdi, %rdi
	je	.L164
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L164:
	movq	-192(%rbp), %rax
	movq	%r12, %rsi
	movq	%rax, (%r14)
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L163:
	movq	8(%r13), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9IfDefaultENS1_10BranchHintE@PLT
	movq	-200(%rbp), %r15
	movq	%rax, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	%r15, -144(%rbp)
	movzbl	23(%r15), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L166
	movq	-200(%rbp), %rax
	movq	32(%rax), %rbx
	movl	8(%rbx), %eax
	addq	$16, %rbx
.L166:
	testl	%eax, %eax
	jle	.L167
	cmpq	$0, (%rbx)
	je	.L168
.L167:
	movq	-200(%rbp), %rcx
	movl	120(%r13), %eax
	cmpl	%eax, 16(%rcx)
	ja	.L168
	addl	$1, %eax
	movl	%eax, 16(%rcx)
	movq	104(%r13), %rax
	movq	88(%r13), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L169
	movq	%rcx, (%rdx)
	addq	$8, 88(%r13)
.L168:
	movq	-184(%rbp), %rdi
	movl	$1, %r14d
	call	_ZN2v88internal8compiler4Node13NullAllInputsEv@PLT
.L147:
	movq	-88(%rbp), %r12
	testq	%r12, %r12
	je	.L94
	movq	-240(%rbp), %r13
.L172:
	movq	24(%r12), %rbx
	testq	%rbx, %rbx
	je	.L170
.L171:
	movq	24(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIiiSt9_IdentityIiESt4lessIiEN2v88internal13ZoneAllocatorIiEEE8_M_eraseEPSt13_Rb_tree_nodeIiE
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L171
.L170:
	movq	16(%r12), %r12
	testq	%r12, %r12
	jne	.L172
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L131:
	movq	-184(%rbp), %rcx
	cmpq	%rcx, -192(%rbp)
	je	.L138
	movq	%rcx, %rdi
	call	_ZN2v88internal8compiler4Node13NullAllInputsEv@PLT
	movzbl	23(%r12), %eax
	leaq	32(%r12), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L136
	movq	32(%r12), %rdi
	movq	%r12, %rsi
	cmpq	%rdi, -192(%rbp)
	je	.L138
.L137:
	leaq	-24(%rsi), %r15
	testq	%rdi, %rdi
	je	.L140
	movq	%r15, %rsi
	movq	%rdx, -184(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-184(%rbp), %rdx
.L140:
	movq	-192(%rbp), %rax
	movq	%rax, (%rdx)
	testq	%rax, %rax
	je	.L138
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L138:
	movl	-204(%rbp), %edx
	movq	8(%r13), %rdi
	xorl	%ecx, %ecx
	movl	-164(%rbp), %esi
	leal	1(%rdx), %r15d
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfValueEiiNS1_10BranchHintE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	-200(%rbp), %rdi
	call	_ZN2v88internal8compiler4Node13NullAllInputsEv@PLT
	movzbl	23(%r12), %eax
	leaq	32(%r12), %rdx
	movq	%r12, -152(%rbp)
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L142
	movq	32(%r12), %rdx
	movl	8(%rdx), %eax
	addq	$16, %rdx
.L142:
	testl	%eax, %eax
	jle	.L143
	cmpq	$0, (%rdx)
	je	.L144
.L143:
	movl	120(%r13), %eax
	cmpl	%eax, 16(%r12)
	ja	.L144
	addl	$1, %eax
	movl	%eax, 16(%r12)
	movq	104(%r13), %rax
	movq	88(%r13), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L145
	movq	%r12, (%rdx)
	addq	$8, 88(%r13)
.L144:
	movq	-256(%rbp), %rsi
	movq	-240(%rbp), %rdi
	movl	%ebx, -164(%rbp)
	call	_ZNSt8_Rb_treeIiiSt9_IdentityIiESt4lessIiEN2v88internal13ZoneAllocatorIiEEE16_M_insert_uniqueIRKiEESt4pairISt17_Rb_tree_iteratorIiEbEOT_
	movl	%r15d, -204(%rbp)
	movq	%r14, %rsi
	movq	%r14, -184(%rbp)
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L122:
	testb	$1, 18(%rcx)
	je	.L113
	testb	%dil, %dil
	je	.L113
	cmpl	$15, %eax
	je	.L124
	movq	%r15, %rdx
	cmpq	%r10, %r9
	je	.L126
.L125:
	leaq	-24(%rdx), %rsi
	movq	%r10, %rdi
	movq	%r8, -296(%rbp)
	movq	%r10, -272(%rbp)
	movq	%r9, -288(%rbp)
	movq	%r11, -280(%rbp)
	movq	%rsi, -264(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-288(%rbp), %r9
	movq	-280(%rbp), %r11
	movq	-264(%rbp), %rsi
	movq	%r9, (%r11)
	movq	%r9, %rdi
	movq	%r9, -264(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movzbl	23(%r15), %eax
	movq	-264(%rbp), %r9
	movq	-272(%rbp), %r10
	movq	-296(%rbp), %r8
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L242
.L126:
	movq	8(%r8), %rdi
	addq	$8, %r8
	cmpq	%r10, %rdi
	je	.L174
.L129:
	subq	$48, %r15
	testq	%rdi, %rdi
	je	.L130
	movq	%r15, %rsi
	movq	%r8, -280(%rbp)
	movq	%r9, -272(%rbp)
	movq	%r10, -264(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-264(%rbp), %r10
	movq	-272(%rbp), %r9
	movq	-280(%rbp), %r8
.L130:
	movq	%r10, (%r8)
	movq	%r15, %rsi
	movq	%r10, %rdi
	movq	%r9, -264(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movq	-264(%rbp), %r9
.L174:
	cmpq	-248(%rbp), %r9
	je	.L123
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L116:
	movq	16(%rdx), %r10
	leaq	16(%rdx), %r11
	movq	(%r10), %rsi
	cmpw	$23, 16(%rsi)
	je	.L119
	xorl	%ebx, %ebx
	xorl	%edi, %edi
.L120:
	leaq	8(%r11), %rsi
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L117:
	movl	44(%rsi), %ebx
	movl	$1, %edi
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L136:
	movq	32(%r12), %rsi
	movq	16(%rsi), %rdi
	leaq	16(%rsi), %rdx
	cmpq	%rdi, -192(%rbp)
	jne	.L137
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L119:
	movl	44(%rsi), %ebx
	movl	$1, %edi
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L103:
	testb	$1, 18(%rdx)
	je	.L105
	testb	%r8b, %r8b
	je	.L105
	cmpl	$15, %ecx
	je	.L106
	movq	%rax, %rsi
	cmpq	-248(%rbp), %r12
	je	.L108
.L107:
	movq	-248(%rbp), %rdi
	subq	$24, %rsi
	movq	%rax, -200(%rbp)
	movq	%rsi, -184(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	%r12, (%r15)
	movq	-184(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movq	-200(%rbp), %rax
	movzbl	23(%rax), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L243
.L108:
	movq	8(%rbx), %rdi
	addq	$8, %rbx
	cmpq	-248(%rbp), %rdi
	je	.L175
.L111:
	leaq	-48(%rax), %r15
	testq	%rdi, %rdi
	je	.L112
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L112:
	movq	-248(%rbp), %rax
	movq	%r15, %rsi
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L175:
	movq	%r12, -248(%rbp)
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L97:
	movq	16(%rsi), %rdi
	leaq	16(%rsi), %r15
	movq	%rdi, -248(%rbp)
	movq	(%rdi), %rdi
	cmpw	$23, 16(%rdi)
	je	.L100
	xorl	%r8d, %r8d
	xorl	%r14d, %r14d
.L101:
	leaq	8(%r15), %rdi
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L145:
	leaq	-152(%rbp), %rsi
	leaq	24(%r13), %rdi
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	jmp	.L144
.L100:
	movl	44(%rdi), %r14d
	movl	$1, %r8d
	jmp	.L101
.L242:
	movq	(%r8), %r15
.L127:
	movq	24(%r15), %rdi
	cmpq	%rdi, %r10
	je	.L174
	leaq	24(%r15), %r8
	jmp	.L129
.L148:
	movq	16(%rdi), %rax
	leaq	16(%rdi), %rbx
	cmpq	%rax, -248(%rbp)
	je	.L150
	movq	%rdi, %r14
	movq	%rax, %rdi
	jmp	.L149
.L152:
	movq	32(%r12), %rax
	movq	16(%rax), %rdi
	leaq	16(%rax), %r15
	cmpq	%rdi, -192(%rbp)
	jne	.L153
	jmp	.L154
.L161:
	movq	32(%rcx), %rax
	movq	16(%rax), %rdi
	leaq	16(%rax), %r14
	cmpq	%rdi, -192(%rbp)
	jne	.L162
	jmp	.L163
.L124:
	cmpq	%r10, %r9
	jne	.L125
	movq	%rdx, %r15
	jmp	.L127
.L160:
	movq	-224(%rbp), %rsi
	leaq	24(%r13), %rdi
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	jmp	.L159
.L169:
	movq	-224(%rbp), %rsi
	leaq	24(%r13), %rdi
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	jmp	.L168
.L106:
	cmpq	-248(%rbp), %r12
	jne	.L107
	movq	%rsi, %rax
.L109:
	movq	24(%rax), %rdi
	cmpq	%rdi, -248(%rbp)
	je	.L175
	leaq	24(%rax), %rbx
	jmp	.L111
.L243:
	movq	(%rbx), %rax
	jmp	.L109
.L239:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13486:
	.size	_ZN2v88internal8compiler20ControlFlowOptimizer14TryBuildSwitchEPNS1_4NodeE.part.0, .-_ZN2v88internal8compiler20ControlFlowOptimizer14TryBuildSwitchEPNS1_4NodeE.part.0
	.section	.text._ZN2v88internal8compiler20ControlFlowOptimizer14TryBuildSwitchEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20ControlFlowOptimizer14TryBuildSwitchEPNS1_4NodeE
	.type	_ZN2v88internal8compiler20ControlFlowOptimizer14TryBuildSwitchEPNS1_4NodeE, @function
_ZN2v88internal8compiler20ControlFlowOptimizer14TryBuildSwitchEPNS1_4NodeE:
.LFB11425:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	(%rsi), %rdi
	movq	%rsi, %r12
	call	_ZN2v88internal8compiler12BranchHintOfEPKNS1_8OperatorE@PLT
	testb	%al, %al
	je	.L247
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L247:
	.cfi_restore_state
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20ControlFlowOptimizer14TryBuildSwitchEPNS1_4NodeE.part.0
	.cfi_endproc
.LFE11425:
	.size	_ZN2v88internal8compiler20ControlFlowOptimizer14TryBuildSwitchEPNS1_4NodeE, .-_ZN2v88internal8compiler20ControlFlowOptimizer14TryBuildSwitchEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler20ControlFlowOptimizer11VisitBranchEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20ControlFlowOptimizer11VisitBranchEPNS1_4NodeE
	.type	_ZN2v88internal8compiler20ControlFlowOptimizer11VisitBranchEPNS1_4NodeE, @function
_ZN2v88internal8compiler20ControlFlowOptimizer11VisitBranchEPNS1_4NodeE:
.LFB11424:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	(%rsi), %rdi
	movq	%rsi, %r12
	call	_ZN2v88internal8compiler12BranchHintOfEPKNS1_8OperatorE@PLT
	testb	%al, %al
	je	.L249
.L250:
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20ControlFlowOptimizer9VisitNodeEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L249:
	.cfi_restore_state
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler20ControlFlowOptimizer14TryBuildSwitchEPNS1_4NodeE.part.0
	testb	%al, %al
	je	.L250
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11424:
	.size	_ZN2v88internal8compiler20ControlFlowOptimizer11VisitBranchEPNS1_4NodeE, .-_ZN2v88internal8compiler20ControlFlowOptimizer11VisitBranchEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler20ControlFlowOptimizer8OptimizeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20ControlFlowOptimizer8OptimizeEv
	.type	_ZN2v88internal8compiler20ControlFlowOptimizer8OptimizeEv, @function
_ZN2v88internal8compiler20ControlFlowOptimizer8OptimizeEv:
.LFB11421:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	8(%rax), %rdx
	movzbl	23(%rdx), %eax
	movq	%rdx, -32(%rbp)
	leaq	32(%rdx), %rcx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L257
	movq	32(%rdx), %rcx
	movl	8(%rcx), %eax
	addq	$16, %rcx
.L257:
	testl	%eax, %eax
	jle	.L258
	cmpq	$0, (%rcx)
	je	.L274
.L258:
	movl	120(%rbx), %eax
	cmpl	%eax, 16(%rdx)
	ja	.L274
	addl	$1, %eax
	movl	%eax, 16(%rdx)
	movq	104(%rbx), %rax
	movq	88(%rbx), %rcx
	subq	$8, %rax
	cmpq	%rax, %rcx
	je	.L261
	movq	%rdx, (%rcx)
	movq	88(%rbx), %rax
	addq	$8, %rax
	movq	%rax, 88(%rbx)
	cmpq	%rax, 56(%rbx)
	jne	.L285
	.p2align 4,,10
	.p2align 3
.L255:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L286
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L261:
	.cfi_restore_state
	leaq	-32(%rbp), %rsi
	leaq	24(%rbx), %rdi
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	.p2align 4,,10
	.p2align 3
.L274:
	movq	88(%rbx), %rax
.L284:
	cmpq	%rax, 56(%rbx)
	je	.L255
.L285:
	movq	136(%rbx), %rdi
	call	_ZN2v88internal11TickCounter6DoTickEv@PLT
	movq	72(%rbx), %rcx
	movq	56(%rbx), %rax
	leaq	-8(%rcx), %rdx
	movq	(%rax), %r12
	cmpq	%rdx, %rax
	je	.L264
	addq	$8, %rax
	movq	%rax, 56(%rbx)
.L265:
	movzbl	23(%r12), %eax
	leaq	32(%r12), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L269
	movq	32(%r12), %rdx
	movl	8(%rdx), %eax
	addq	$16, %rdx
.L269:
	testl	%eax, %eax
	jle	.L270
	cmpq	$0, (%rdx)
	je	.L274
.L270:
	movq	(%r12), %rdi
	cmpw	$2, 16(%rdi)
	je	.L287
.L272:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler20ControlFlowOptimizer9VisitNodeEPNS1_4NodeE
	movq	88(%rbx), %rax
	jmp	.L284
	.p2align 4,,10
	.p2align 3
.L287:
	call	_ZN2v88internal8compiler12BranchHintOfEPKNS1_8OperatorE@PLT
	testb	%al, %al
	jne	.L272
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler20ControlFlowOptimizer14TryBuildSwitchEPNS1_4NodeE.part.0
	testb	%al, %al
	jne	.L274
	jmp	.L272
	.p2align 4,,10
	.p2align 3
.L264:
	movq	32(%rbx), %rax
	testq	%rax, %rax
	je	.L266
	cmpq	$64, 8(%rax)
	ja	.L267
.L266:
	movq	64(%rbx), %rax
	movq	$64, 8(%rax)
	movq	32(%rbx), %rdx
	movq	%rdx, (%rax)
	movq	%rax, 32(%rbx)
.L267:
	movq	80(%rbx), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 80(%rbx)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 64(%rbx)
	movq	%rdx, 72(%rbx)
	movq	%rax, 56(%rbx)
	jmp	.L265
.L286:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11421:
	.size	_ZN2v88internal8compiler20ControlFlowOptimizer8OptimizeEv, .-_ZN2v88internal8compiler20ControlFlowOptimizer8OptimizeEv
	.section	.text._ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm,"axG",@progbits,_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	.type	_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm, @function
_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm:
.LFB13222:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$8, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	shrq	$6, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	1(%rsi), %rbx
	addq	$3, %rsi
	subq	$8, %rsp
	cmpq	$8, %rsi
	cmovb	%rax, %rsi
	movq	%rsi, 24(%rdi)
	movq	(%rdi), %rdi
	salq	$3, %rsi
	movq	16(%rdi), %rdx
	movq	24(%rdi), %rax
	subq	%rdx, %rax
	cmpq	%rax, %rsi
	ja	.L302
	addq	%rdx, %rsi
	movq	%rsi, 16(%rdi)
.L290:
	movq	24(%r12), %rax
	movq	%rdx, 16(%r12)
	subq	%rbx, %rax
	shrq	%rax
	leaq	(%rdx,%rax,8), %r15
	leaq	(%r15,%rbx,8), %r14
	cmpq	%r14, %r15
	jnb	.L291
	movq	%r15, %rbx
	jmp	.L296
	.p2align 4,,10
	.p2align 3
.L292:
	movq	(%r12), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L303
	leaq	512(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L293:
	movq	%rax, (%rbx)
	addq	$8, %rbx
	cmpq	%rbx, %r14
	jbe	.L291
.L296:
	movq	8(%r12), %rax
	testq	%rax, %rax
	je	.L292
	cmpq	$63, 8(%rax)
	jbe	.L292
	movq	(%rax), %rdx
	addq	$8, %rbx
	movq	%rdx, 8(%r12)
	movq	%rax, -8(%rbx)
	cmpq	%rbx, %r14
	ja	.L296
.L291:
	movq	%r15, 56(%r12)
	movq	(%r15), %rdx
	andl	$63, %r13d
	leaq	512(%rdx), %rax
	movq	%rdx, 40(%r12)
	movq	%rax, 48(%r12)
	leaq	-8(%r14), %rax
	movq	%rax, 88(%r12)
	movq	-8(%r14), %rax
	movq	%rdx, 32(%r12)
	movq	%rax, 72(%r12)
	leaq	512(%rax), %rcx
	leaq	(%rax,%r13,8), %rax
	movq	%rcx, 80(%r12)
	movq	%rax, 64(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L303:
	.cfi_restore_state
	movl	$512, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L302:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L290
	.cfi_endproc
.LFE13222:
	.size	_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm, .-_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	.section	.text._ZN2v88internal8compiler20ControlFlowOptimizerC2EPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS1_22MachineOperatorBuilderEPNS0_11TickCounterEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20ControlFlowOptimizerC2EPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS1_22MachineOperatorBuilderEPNS0_11TickCounterEPNS0_4ZoneE
	.type	_ZN2v88internal8compiler20ControlFlowOptimizerC2EPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS1_22MachineOperatorBuilderEPNS0_11TickCounterEPNS0_4ZoneE, @function
_ZN2v88internal8compiler20ControlFlowOptimizerC2EPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS1_22MachineOperatorBuilderEPNS0_11TickCounterEPNS0_4ZoneE:
.LFB11419:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %xmm0
	movq	%rdx, %xmm6
	punpcklqdq	%xmm6, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$240, %rsp
	movq	%r8, -240(%rbp)
	movq	%r9, -248(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	%rcx, 16(%rdi)
	movups	%xmm0, (%rdi)
	pxor	%xmm0, %xmm0
	leaq	-224(%rbp), %rdi
	movq	%r9, -224(%rbp)
	movq	$0, -216(%rbp)
	movq	$0, -208(%rbp)
	movq	$0, -200(%rbp)
	movaps	%xmm0, -192(%rbp)
	movaps	%xmm0, -176(%rbp)
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm0, -144(%rbp)
	call	_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	cmpq	$0, -208(%rbp)
	je	.L321
	movdqa	-224(%rbp), %xmm7
	pxor	%xmm0, %xmm0
	leaq	-128(%rbp), %rdi
	xorl	%esi, %esi
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm7, -128(%rbp)
	movaps	%xmm0, -80(%rbp)
	movaps	%xmm0, -64(%rbp)
	movaps	%xmm0, -48(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	call	_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	movdqa	-96(%rbp), %xmm7
	movq	-112(%rbp), %rsi
	movq	-192(%rbp), %xmm3
	movq	-184(%rbp), %r9
	movaps	%xmm7, -192(%rbp)
	movdqa	-64(%rbp), %xmm7
	movq	-168(%rbp), %r8
	movq	-160(%rbp), %xmm1
	movq	-152(%rbp), %rcx
	movaps	%xmm7, -160(%rbp)
	movq	%r9, %xmm7
	movq	-104(%rbp), %rdi
	movq	-176(%rbp), %xmm2
	punpcklqdq	%xmm7, %xmm3
	movq	-136(%rbp), %rdx
	movdqa	-80(%rbp), %xmm6
	movups	%xmm3, 56(%rbx)
	movq	%r8, %xmm3
	movq	-216(%rbp), %rax
	movq	-144(%rbp), %xmm0
	punpcklqdq	%xmm3, %xmm2
	movq	-208(%rbp), %r11
	movq	-224(%rbp), %xmm4
	movaps	%xmm6, -176(%rbp)
	movups	%xmm2, 72(%rbx)
	movq	%rcx, %xmm2
	movdqa	-48(%rbp), %xmm5
	movq	-200(%rbp), %r10
	punpcklqdq	%xmm2, %xmm1
	movq	%rax, %xmm6
	movq	%rsi, -208(%rbp)
	movups	%xmm1, 88(%rbx)
	movq	%rdx, %xmm1
	punpcklqdq	%xmm6, %xmm4
	punpcklqdq	%xmm1, %xmm0
	movq	%rdi, -200(%rbp)
	movq	%r11, 40(%rbx)
	movq	%r10, 48(%rbx)
	movaps	%xmm5, -144(%rbp)
	movups	%xmm4, 24(%rbx)
	movups	%xmm0, 104(%rbx)
	testq	%rsi, %rsi
	je	.L306
	movq	-136(%rbp), %rcx
	movq	-168(%rbp), %rdx
	addq	$8, %rcx
	cmpq	%rdx, %rcx
	jbe	.L307
	.p2align 4,,10
	.p2align 3
.L310:
	testq	%rax, %rax
	je	.L308
	cmpq	$64, 8(%rax)
	ja	.L309
.L308:
	movq	(%rdx), %rax
	movq	$64, 8(%rax)
	movq	-216(%rbp), %rsi
	movq	%rsi, (%rax)
	movq	%rax, -216(%rbp)
.L309:
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	ja	.L310
	movq	-200(%rbp), %rdi
	movq	-208(%rbp), %rsi
.L307:
	leaq	0(,%rdi,8), %rax
	cmpq	$15, %rax
	jbe	.L306
	movq	%rdi, 8(%rsi)
	movq	$0, (%rsi)
.L306:
	movq	-248(%rbp), %xmm0
	leaq	120(%rbx), %rdi
	movl	$2, %edx
	movq	%r12, %rsi
	movhps	-240(%rbp), %xmm0
	movaps	%xmm0, -240(%rbp)
	call	_ZN2v88internal8compiler14NodeMarkerBaseC2EPNS1_5GraphEj@PLT
	movdqa	-240(%rbp), %xmm0
	movups	%xmm0, 128(%rbx)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L322
	addq	$240, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L321:
	.cfi_restore_state
	movdqa	-224(%rbp), %xmm1
	movq	-200(%rbp), %rax
	movq	$0, 40(%rbx)
	movdqa	-192(%rbp), %xmm2
	movdqa	-176(%rbp), %xmm3
	movdqa	-160(%rbp), %xmm5
	movdqa	-144(%rbp), %xmm6
	movq	%rax, 48(%rbx)
	movups	%xmm1, 24(%rbx)
	movups	%xmm2, 56(%rbx)
	movups	%xmm3, 72(%rbx)
	movups	%xmm5, 88(%rbx)
	movups	%xmm6, 104(%rbx)
	jmp	.L306
.L322:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11419:
	.size	_ZN2v88internal8compiler20ControlFlowOptimizerC2EPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS1_22MachineOperatorBuilderEPNS0_11TickCounterEPNS0_4ZoneE, .-_ZN2v88internal8compiler20ControlFlowOptimizerC2EPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS1_22MachineOperatorBuilderEPNS0_11TickCounterEPNS0_4ZoneE
	.globl	_ZN2v88internal8compiler20ControlFlowOptimizerC1EPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS1_22MachineOperatorBuilderEPNS0_11TickCounterEPNS0_4ZoneE
	.set	_ZN2v88internal8compiler20ControlFlowOptimizerC1EPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS1_22MachineOperatorBuilderEPNS0_11TickCounterEPNS0_4ZoneE,_ZN2v88internal8compiler20ControlFlowOptimizerC2EPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS1_22MachineOperatorBuilderEPNS0_11TickCounterEPNS0_4ZoneE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler20ControlFlowOptimizerC2EPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS1_22MachineOperatorBuilderEPNS0_11TickCounterEPNS0_4ZoneE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler20ControlFlowOptimizerC2EPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS1_22MachineOperatorBuilderEPNS0_11TickCounterEPNS0_4ZoneE, @function
_GLOBAL__sub_I__ZN2v88internal8compiler20ControlFlowOptimizerC2EPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS1_22MachineOperatorBuilderEPNS0_11TickCounterEPNS0_4ZoneE:
.LFB13406:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE13406:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler20ControlFlowOptimizerC2EPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS1_22MachineOperatorBuilderEPNS0_11TickCounterEPNS0_4ZoneE, .-_GLOBAL__sub_I__ZN2v88internal8compiler20ControlFlowOptimizerC2EPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS1_22MachineOperatorBuilderEPNS0_11TickCounterEPNS0_4ZoneE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler20ControlFlowOptimizerC2EPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS1_22MachineOperatorBuilderEPNS0_11TickCounterEPNS0_4ZoneE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
