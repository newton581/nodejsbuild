	.file	"branch-elimination.cc"
	.text
	.section	.rodata._ZNK2v88internal8compiler17BranchElimination12reducer_nameEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"BranchElimination"
	.section	.text._ZNK2v88internal8compiler17BranchElimination12reducer_nameEv,"axG",@progbits,_ZNK2v88internal8compiler17BranchElimination12reducer_nameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal8compiler17BranchElimination12reducer_nameEv
	.type	_ZNK2v88internal8compiler17BranchElimination12reducer_nameEv, @function
_ZNK2v88internal8compiler17BranchElimination12reducer_nameEv:
.LFB10194:
	.cfi_startproc
	endbr64
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE10194:
	.size	_ZNK2v88internal8compiler17BranchElimination12reducer_nameEv, .-_ZNK2v88internal8compiler17BranchElimination12reducer_nameEv
	.section	.text._ZN2v88internal8compiler17BranchEliminationD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17BranchEliminationD2Ev
	.type	_ZN2v88internal8compiler17BranchEliminationD2Ev, @function
_ZN2v88internal8compiler17BranchEliminationD2Ev:
.LFB10992:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE10992:
	.size	_ZN2v88internal8compiler17BranchEliminationD2Ev, .-_ZN2v88internal8compiler17BranchEliminationD2Ev
	.globl	_ZN2v88internal8compiler17BranchEliminationD1Ev
	.set	_ZN2v88internal8compiler17BranchEliminationD1Ev,_ZN2v88internal8compiler17BranchEliminationD2Ev
	.section	.text._ZN2v88internal8compiler17BranchEliminationD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17BranchEliminationD0Ev
	.type	_ZN2v88internal8compiler17BranchEliminationD0Ev, @function
_ZN2v88internal8compiler17BranchEliminationD0Ev:
.LFB10994:
	.cfi_startproc
	endbr64
	movl	$128, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10994:
	.size	_ZN2v88internal8compiler17BranchEliminationD0Ev, .-_ZN2v88internal8compiler17BranchEliminationD0Ev
	.section	.rodata._ZN2v88internal8compiler17BranchEliminationC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS0_4ZoneENS2_5PhaseE.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"cannot create std::vector larger than max_size()"
	.section	.text._ZN2v88internal8compiler17BranchEliminationC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS0_4ZoneENS2_5PhaseE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17BranchEliminationC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS0_4ZoneENS2_5PhaseE
	.type	_ZN2v88internal8compiler17BranchEliminationC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS0_4ZoneENS2_5PhaseE, @function
_ZN2v88internal8compiler17BranchEliminationC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS0_4ZoneENS2_5PhaseE:
.LFB10989:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal8compiler17BranchEliminationE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%rsi, 8(%rdi)
	movq	%rax, (%rdi)
	movq	(%rdx), %rax
	movq	%rdx, 16(%rdi)
	movl	28(%rax), %eax
	cmpq	$268435455, %rax
	ja	.L24
	movq	%rdx, %r13
	movq	%rcx, 24(%rdi)
	movq	%rcx, %r12
	movl	%r8d, %r14d
	movq	$0, 32(%rdi)
	leaq	0(,%rax,8), %rdx
	xorl	%r15d, %r15d
	movq	$0, 40(%rdi)
	movq	$0, 48(%rdi)
	testq	%rax, %rax
	je	.L15
	movq	16(%rcx), %rdi
	movq	24(%rcx), %rax
	movq	%rdx, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rdx
	ja	.L25
	addq	%rdi, %rsi
	movq	%rsi, 16(%rcx)
.L9:
	leaq	(%rdi,%rdx), %r15
	movq	%rdi, 32(%rbx)
	xorl	%esi, %esi
	movq	%r15, 48(%rbx)
	call	memset@PLT
.L15:
	movq	%r15, 40(%rbx)
	movq	0(%r13), %rax
	movl	28(%rax), %r15d
	movq	%r12, 56(%rbx)
	movq	$0, 64(%rbx)
	movl	$0, 72(%rbx)
	movq	%r15, %rcx
	movq	$0, 80(%rbx)
	movl	$0, 88(%rbx)
	movq	$0, 96(%rbx)
	testq	%r15, %r15
	jne	.L26
.L13:
	movq	352(%r13), %rax
	movq	%r12, 104(%rbx)
	testq	%rax, %rax
	je	.L27
.L14:
	movl	%r14d, 120(%rbx)
	movq	%rax, 112(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	movq	16(%r12), %rdi
	movq	24(%r12), %rax
	leaq	63(%r15), %rdx
	shrq	$6, %rdx
	salq	$3, %rdx
	subq	%rdi, %rax
	movq	%rdx, %rsi
	cmpq	%rax, %rdx
	ja	.L28
	addq	%rdi, %rsi
	movq	%rsi, 16(%r12)
.L12:
	leaq	(%rdi,%rdx), %rax
	sarq	$6, %r15
	andl	$63, %ecx
	movq	%rdi, 64(%rbx)
	movq	%rax, 96(%rbx)
	leaq	(%rdi,%r15,8), %rax
	movl	$0, 72(%rbx)
	movq	%rax, 80(%rbx)
	movl	%ecx, 88(%rbx)
	testq	%rdi, %rdi
	je	.L13
	xorl	%esi, %esi
	call	memset@PLT
	movq	352(%r13), %rax
	movq	%r12, 104(%rbx)
	testq	%rax, %rax
	jne	.L14
.L27:
	movq	8(%r13), %rdi
	movq	0(%r13), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4DeadEv@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 352(%r13)
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L25:
	movq	%rcx, %rdi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %rdi
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L28:
	movq	%r12, %rdi
	movq	%rdx, -64(%rbp)
	movl	%r15d, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-56(%rbp), %ecx
	movq	-64(%rbp), %rdx
	movq	%rax, %rdi
	jmp	.L12
.L24:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE10989:
	.size	_ZN2v88internal8compiler17BranchEliminationC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS0_4ZoneENS2_5PhaseE, .-_ZN2v88internal8compiler17BranchEliminationC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS0_4ZoneENS2_5PhaseE
	.globl	_ZN2v88internal8compiler17BranchEliminationC1EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS0_4ZoneENS2_5PhaseE
	.set	_ZN2v88internal8compiler17BranchEliminationC1EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS0_4ZoneENS2_5PhaseE,_ZN2v88internal8compiler17BranchEliminationC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS0_4ZoneENS2_5PhaseE
	.section	.rodata._ZN2v88internal8compiler17BranchElimination21ControlPathConditions12AddConditionEPNS0_4ZoneEPNS1_4NodeES7_bS3_.str1.1,"aMS",@progbits,1
.LC2:
	.string	"Size() > 0"
.LC3:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal8compiler17BranchElimination21ControlPathConditions12AddConditionEPNS0_4ZoneEPNS1_4NodeES7_bS3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17BranchElimination21ControlPathConditions12AddConditionEPNS0_4ZoneEPNS1_4NodeES7_bS3_
	.type	_ZN2v88internal8compiler17BranchElimination21ControlPathConditions12AddConditionEPNS0_4ZoneEPNS1_4NodeES7_bS3_, @function
_ZN2v88internal8compiler17BranchElimination21ControlPathConditions12AddConditionEPNS0_4ZoneEPNS1_4NodeES7_bS3_:
.LFB11013:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdx, %rbx
	subq	$16, %rsp
	movq	%rcx, -24(%rbp)
	movq	(%r12), %rax
	testq	%r9, %r9
	je	.L30
	movq	32(%r9), %rdx
	testq	%rax, %rax
	je	.L43
.L42:
	movq	32(%rax), %rsi
	leaq	1(%rsi), %rcx
.L31:
	cmpq	%rcx, %rdx
	jne	.L32
	cmpq	(%r9), %rbx
	je	.L48
.L32:
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$39, %rdx
	jbe	.L49
	leaq	40(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L39:
	movq	(%r12), %rcx
	movq	%rbx, %xmm0
	movb	%r8b, 16(%rax)
	movl	$1, %edx
	movhps	-24(%rbp), %xmm0
	movq	%rcx, 24(%rax)
	movups	%xmm0, (%rax)
	testq	%rcx, %rcx
	je	.L40
	movq	32(%rcx), %rdx
	addq	$1, %rdx
.L40:
	movq	%rdx, 32(%rax)
	movq	%rax, (%r12)
.L29:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	.cfi_restore_state
	movl	$1, %ecx
	xorl	%esi, %esi
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L30:
	xorl	%edx, %edx
	testq	%rax, %rax
	jne	.L42
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L48:
	movq	-24(%rbp), %rcx
	cmpq	8(%r9), %rcx
	jne	.L32
	cmpb	16(%r9), %r8b
	jne	.L32
	cmpq	$0, 32(%r9)
	jne	.L50
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L49:
	movl	$40, %esi
	movl	%r8d, -28(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-28(%rbp), %r8d
	jmp	.L39
.L50:
	movq	24(%r9), %rdx
	xorl	%ecx, %ecx
	testq	%rdx, %rdx
	je	.L35
	movq	32(%rdx), %rcx
.L35:
	cmpq	%rcx, %rsi
	jne	.L32
	cmpq	%rdx, %rax
	je	.L36
.L37:
	movq	(%rdx), %rsi
	cmpq	%rsi, (%rax)
	jne	.L32
	movq	8(%rdx), %rsi
	cmpq	%rsi, 8(%rax)
	jne	.L32
	movzbl	16(%rdx), %esi
	cmpb	%sil, 16(%rax)
	jne	.L32
	movq	24(%rdx), %rdx
	movq	24(%rax), %rax
	cmpq	%rax, %rdx
	jne	.L37
.L36:
	movq	%r9, (%r12)
	jmp	.L29
	.cfi_endproc
.LFE11013:
	.size	_ZN2v88internal8compiler17BranchElimination21ControlPathConditions12AddConditionEPNS0_4ZoneEPNS1_4NodeES7_bS3_, .-_ZN2v88internal8compiler17BranchElimination21ControlPathConditions12AddConditionEPNS0_4ZoneEPNS1_4NodeES7_bS3_
	.section	.text._ZNK2v88internal8compiler17BranchElimination21ControlPathConditions15LookupConditionEPNS1_4NodeEPS5_Pb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler17BranchElimination21ControlPathConditions15LookupConditionEPNS1_4NodeEPS5_Pb
	.type	_ZNK2v88internal8compiler17BranchElimination21ControlPathConditions15LookupConditionEPNS1_4NodeEPS5_Pb, @function
_ZNK2v88internal8compiler17BranchElimination21ControlPathConditions15LookupConditionEPNS1_4NodeEPS5_Pb:
.LFB11014:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	testq	%rax, %rax
	jne	.L54
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L53:
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L57
.L54:
	cmpq	%rsi, (%rax)
	jne	.L53
	movq	8(%rax), %rsi
	movzbl	16(%rax), %eax
	movb	%al, (%rcx)
	movl	$1, %eax
	movq	%rsi, (%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	ret
.L55:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE11014:
	.size	_ZNK2v88internal8compiler17BranchElimination21ControlPathConditions15LookupConditionEPNS1_4NodeEPS5_Pb, .-_ZNK2v88internal8compiler17BranchElimination21ControlPathConditions15LookupConditionEPNS1_4NodeEPS5_Pb
	.section	.text._ZNK2v88internal8compiler17BranchElimination5graphEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler17BranchElimination5graphEv
	.type	_ZNK2v88internal8compiler17BranchElimination5graphEv, @function
_ZNK2v88internal8compiler17BranchElimination5graphEv:
.LFB11015:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	(%rax), %rax
	ret
	.cfi_endproc
.LFE11015:
	.size	_ZNK2v88internal8compiler17BranchElimination5graphEv, .-_ZNK2v88internal8compiler17BranchElimination5graphEv
	.section	.text._ZNK2v88internal8compiler17BranchElimination7isolateEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler17BranchElimination7isolateEv
	.type	_ZNK2v88internal8compiler17BranchElimination7isolateEv, @function
_ZNK2v88internal8compiler17BranchElimination7isolateEv:
.LFB11016:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	360(%rax), %rax
	ret
	.cfi_endproc
.LFE11016:
	.size	_ZNK2v88internal8compiler17BranchElimination7isolateEv, .-_ZNK2v88internal8compiler17BranchElimination7isolateEv
	.section	.text._ZNK2v88internal8compiler17BranchElimination6commonEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler17BranchElimination6commonEv
	.type	_ZNK2v88internal8compiler17BranchElimination6commonEv, @function
_ZNK2v88internal8compiler17BranchElimination6commonEv:
.LFB11017:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	8(%rax), %rax
	ret
	.cfi_endproc
.LFE11017:
	.size	_ZNK2v88internal8compiler17BranchElimination6commonEv, .-_ZNK2v88internal8compiler17BranchElimination6commonEv
	.section	.text._ZN2v84base11SmallVectorIPNS_8internal8compiler4NodeELm2EE4GrowEm,"axG",@progbits,_ZN2v84base11SmallVectorIPNS_8internal8compiler4NodeELm2EE4GrowEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v84base11SmallVectorIPNS_8internal8compiler4NodeELm2EE4GrowEm
	.type	_ZN2v84base11SmallVectorIPNS_8internal8compiler4NodeELm2EE4GrowEm, @function
_ZN2v84base11SmallVectorIPNS_8internal8compiler4NodeELm2EE4GrowEm:
.LFB12505:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rax
	movq	8(%rdi), %r12
	movq	16(%rdi), %rdi
	subq	%rax, %r12
	subq	%rax, %rdi
	sarq	$3, %rdi
	addq	%rdi, %rdi
	cmpq	%rsi, %rdi
	cmovb	%rsi, %rdi
	call	_ZN2v84base4bits21RoundUpToPowerOfTwo64Em@PLT
	leaq	0(,%rax,8), %r14
	movq	%r14, %rdi
	call	malloc@PLT
	movq	(%rbx), %r15
	movq	%r12, %rdx
	movq	%rax, %rdi
	movq	%rax, %r13
	movq	%r15, %rsi
	call	memcpy@PLT
	leaq	24(%rbx), %rax
	cmpq	%rax, %r15
	je	.L62
	movq	%r15, %rdi
	call	free@PLT
.L62:
	leaq	0(%r13,%r12), %rax
	movq	%r13, (%rbx)
	addq	%r14, %r13
	movq	%r13, 16(%rbx)
	movq	%rax, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE12505:
	.size	_ZN2v84base11SmallVectorIPNS_8internal8compiler4NodeELm2EE4GrowEm, .-_ZN2v84base11SmallVectorIPNS_8internal8compiler4NodeELm2EE4GrowEm
	.section	.text._ZN2v84base11SmallVectorIPNS_8internal8compiler4NodeELm2EE4GrowEv,"axG",@progbits,_ZN2v84base11SmallVectorIPNS_8internal8compiler4NodeELm2EE4GrowEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v84base11SmallVectorIPNS_8internal8compiler4NodeELm2EE4GrowEv
	.type	_ZN2v84base11SmallVectorIPNS_8internal8compiler4NodeELm2EE4GrowEv, @function
_ZN2v84base11SmallVectorIPNS_8internal8compiler4NodeELm2EE4GrowEv:
.LFB12224:
	.cfi_startproc
	endbr64
	xorl	%esi, %esi
	jmp	_ZN2v84base11SmallVectorIPNS_8internal8compiler4NodeELm2EE4GrowEm
	.cfi_endproc
.LFE12224:
	.size	_ZN2v84base11SmallVectorIPNS_8internal8compiler4NodeELm2EE4GrowEv, .-_ZN2v84base11SmallVectorIPNS_8internal8compiler4NodeELm2EE4GrowEv
	.section	.text._ZN2v88internal8compiler17BranchElimination23SimplifyBranchConditionEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17BranchElimination23SimplifyBranchConditionEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17BranchElimination23SimplifyBranchConditionEPNS1_4NodeE, @function
_ZN2v88internal8compiler17BranchElimination23SimplifyBranchConditionEPNS1_4NodeE:
.LFB10996:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movq	%r15, %rdi
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -120(%rbp)
	xorl	%esi, %esi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %rbx
	movq	(%rax), %rax
	cmpw	$10, 16(%rax)
	jne	.L65
	movzbl	23(%r15), %eax
	movq	32(%r15), %r12
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L103
.L67:
	movq	16(%r14), %rax
	leaq	-72(%rbp), %rcx
	leaq	32(%rbx), %r11
	movq	%rcx, %xmm0
	movq	%rcx, -104(%rbp)
	movq	(%rax), %r15
	leaq	-56(%rbp), %rax
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, -80(%rbp)
	movzbl	23(%rbx), %eax
	movaps	%xmm0, -96(%rbp)
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L68
	movl	%eax, -108(%rbp)
.L69:
	testl	%eax, %eax
	je	.L87
	leaq	-96(%rbp), %rsi
	subl	$1, %eax
	movq	%rbx, -136(%rbp)
	movq	%r11, %r13
	movq	%rsi, -128(%rbp)
	leaq	8(%r11,%rax,8), %rax
	movq	%r14, %rbx
	movq	%r12, %r14
	movq	%rax, %r12
	.p2align 4,,10
	.p2align 3
.L84:
	movq	0(%r13), %rax
	movq	32(%rbx), %rcx
	movl	20(%rax), %edx
	movq	40(%rbx), %rax
	subq	%rcx, %rax
	andl	$16777215, %edx
	sarq	$3, %rax
	cmpq	%rax, %rdx
	jnb	.L100
	movq	(%rcx,%rdx,8), %rax
	testq	%rax, %rax
	jne	.L75
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L72:
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L100
.L75:
	cmpq	%r14, (%rax)
	jne	.L72
	movl	120(%rbx), %edx
	movzbl	16(%rax), %eax
	movq	16(%rbx), %rdi
	testl	%edx, %edx
	jne	.L104
	testb	%al, %al
	je	.L77
	call	_ZN2v88internal8compiler7JSGraph12TrueConstantEv@PLT
	movq	%rax, %rdx
.L102:
	movq	-88(%rbp), %rax
	cmpq	-80(%rbp), %rax
	je	.L105
.L83:
	movq	%rdx, (%rax)
	addq	$8, %r13
	addq	$8, %rax
	movq	%rax, -88(%rbp)
	cmpq	%r12, %r13
	jne	.L84
	movq	%rbx, %r14
	movq	-136(%rbp), %rbx
	cmpq	%rax, -80(%rbp)
	je	.L106
.L70:
	movq	%rbx, (%rax)
	addq	$8, %rax
	cmpl	$1, 120(%r14)
	sbbl	%esi, %esi
	movl	-108(%rbp), %edx
	movq	-96(%rbp), %rbx
	movq	%rax, -88(%rbp)
	movq	16(%r14), %rax
	andl	$4, %esi
	addl	$4, %esi
	leal	1(%rdx), %r13d
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	movl	%r13d, %edx
	movq	%r15, %rdi
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movq	%rbx, %rcx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-120(%rbp), %rdi
	xorl	%edx, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties17ReplaceValueInputEPNS1_4NodeES4_i@PLT
	.p2align 4,,10
	.p2align 3
.L100:
	movq	-96(%rbp), %rdi
	cmpq	-104(%rbp), %rdi
	je	.L65
	call	free@PLT
	.p2align 4,,10
	.p2align 3
.L65:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L107
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L104:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	movl	$1, %esi
	testb	%al, %al
	jne	.L96
	xorl	%esi, %esi
.L96:
	call	_ZN2v88internal8compiler21CommonOperatorBuilder13Int32ConstantEi@PLT
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rax, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %rdx
	movq	-88(%rbp), %rax
	cmpq	-80(%rbp), %rax
	jne	.L83
.L105:
	movq	-128(%rbp), %rdi
	movq	%rdx, -144(%rbp)
	call	_ZN2v84base11SmallVectorIPNS_8internal8compiler4NodeELm2EE4GrowEv
	movq	-144(%rbp), %rdx
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L77:
	call	_ZN2v88internal8compiler7JSGraph13FalseConstantEv@PLT
	movq	%rax, %rdx
	jmp	.L102
.L103:
	movq	16(%r12), %r12
	jmp	.L67
.L68:
	movq	32(%rbx), %r11
	movl	8(%r11), %eax
	addq	$16, %r11
	movl	%eax, -108(%rbp)
	jmp	.L69
.L87:
	movq	-104(%rbp), %rax
	jmp	.L70
.L106:
	leaq	-96(%rbp), %rdi
	call	_ZN2v84base11SmallVectorIPNS_8internal8compiler4NodeELm2EE4GrowEv
	jmp	.L70
.L107:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10996:
	.size	_ZN2v88internal8compiler17BranchElimination23SimplifyBranchConditionEPNS1_4NodeE, .-_ZN2v88internal8compiler17BranchElimination23SimplifyBranchConditionEPNS1_4NodeE
	.section	.rodata._ZNSt6vectorIN2v88internal8compiler17BranchElimination21ControlPathConditionsENS1_13ZoneAllocatorIS4_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS4_S7_EEmRKS4_.str1.1,"aMS",@progbits,1
.LC4:
	.string	"vector::_M_fill_insert"
	.section	.text._ZNSt6vectorIN2v88internal8compiler17BranchElimination21ControlPathConditionsENS1_13ZoneAllocatorIS4_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS4_S7_EEmRKS4_,"axG",@progbits,_ZNSt6vectorIN2v88internal8compiler17BranchElimination21ControlPathConditionsENS1_13ZoneAllocatorIS4_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS4_S7_EEmRKS4_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal8compiler17BranchElimination21ControlPathConditionsENS1_13ZoneAllocatorIS4_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS4_S7_EEmRKS4_
	.type	_ZNSt6vectorIN2v88internal8compiler17BranchElimination21ControlPathConditionsENS1_13ZoneAllocatorIS4_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS4_S7_EEmRKS4_, @function
_ZNSt6vectorIN2v88internal8compiler17BranchElimination21ControlPathConditionsENS1_13ZoneAllocatorIS4_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS4_S7_EEmRKS4_:
.LFB12514:
	.cfi_startproc
	endbr64
	testq	%rdx, %rdx
	je	.L218
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	16(%rdi), %rdi
	movq	24(%r12), %rax
	subq	%rdi, %rax
	sarq	$3, %rax
	cmpq	%rdx, %rax
	jb	.L111
	movq	%rdi, %r8
	movq	(%rcx), %xmm0
	subq	%rsi, %r8
	movq	%r8, %rax
	sarq	$3, %rax
	cmpq	%rax, %rdx
	jnb	.L112
	leaq	0(,%rdx,8), %r14
	movq	%rdi, %rdx
	subq	%r14, %rdx
	cmpq	%rdx, %rdi
	je	.L159
	leaq	-8(%rdi), %rax
	movl	$16, %ecx
	subq	%rdx, %rax
	subq	%r14, %rcx
	shrq	$3, %rax
	testq	%rcx, %rcx
	leaq	16(%rdi), %rcx
	setle	%sil
	cmpq	%rcx, %rdx
	setnb	%cl
	orb	%cl, %sil
	je	.L160
	movabsq	$2305843009213693948, %rcx
	testq	%rcx, %rax
	je	.L160
	leaq	1(%rax), %r8
	xorl	%eax, %eax
	movq	%r8, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L115:
	movdqu	(%rdx,%rax), %xmm2
	movups	%xmm2, (%rdi,%rax)
	addq	$16, %rax
	cmpq	%rax, %rcx
	jne	.L115
	movq	%r8, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rcx
	leaq	(%rdx,%rcx), %rsi
	addq	%rdi, %rcx
	cmpq	%rax, %r8
	je	.L117
	movq	(%rsi), %rax
	movq	%rax, (%rcx)
.L117:
	movq	16(%r12), %rax
.L113:
	addq	%r14, %rax
	movq	%rax, 16(%r12)
	cmpq	%rdx, %rbx
	je	.L118
	subq	%rbx, %rdx
	movq	%rbx, %rsi
	movq	%xmm0, -40(%rbp)
	subq	%rdx, %rdi
	call	memmove@PLT
	movq	-40(%rbp), %xmm0
.L118:
	leaq	(%rbx,%r14), %rdx
	cmpq	%rdx, %rbx
	je	.L108
	subq	%rbx, %rdx
	movq	%rbx, %rax
	subq	$8, %rdx
	movq	%rdx, %rcx
	shrq	$3, %rcx
	addq	$1, %rcx
	testq	%rdx, %rdx
	je	.L158
	movq	%rcx, %rdx
	movdqa	%xmm0, %xmm1
	shrq	%rdx
	punpcklqdq	%xmm1, %xmm1
	salq	$4, %rdx
	addq	%rbx, %rdx
	.p2align 4,,10
	.p2align 3
.L122:
	movups	%xmm1, (%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L122
	movq	%rcx, %rax
	andq	$-2, %rax
	leaq	(%rbx,%rax,8), %r13
	cmpq	%rax, %rcx
	je	.L108
.L158:
	movq	%xmm0, 0(%r13)
.L108:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L218:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	ret
	.p2align 4,,10
	.p2align 3
.L112:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	subq	%rax, %rdx
	je	.L161
	cmpq	$1, %rdx
	je	.L162
	movq	%rdx, %rsi
	movdqa	%xmm0, %xmm1
	xorl	%eax, %eax
	shrq	%rsi
	punpcklqdq	%xmm1, %xmm1
	.p2align 4,,10
	.p2align 3
.L126:
	movq	%rax, %rcx
	addq	$1, %rax
	salq	$4, %rcx
	movups	%xmm1, (%rdi,%rcx)
	cmpq	%rax, %rsi
	jne	.L126
	movq	%rdx, %rcx
	andq	$-2, %rcx
	leaq	(%rdi,%rcx,8), %rax
	cmpq	%rcx, %rdx
	je	.L127
.L125:
	movq	%xmm0, (%rax)
.L127:
	leaq	(%rdi,%rdx,8), %rsi
.L124:
	movq	%rsi, 16(%r12)
	cmpq	%rbx, %rdi
	je	.L128
	subq	%rbx, %rdi
	leaq	15(%rbx), %rcx
	movq	%rbx, %rdx
	leaq	-8(%rdi), %r9
	subq	%rsi, %rcx
	movq	%r9, %rax
	shrq	$3, %rax
	cmpq	$30, %rcx
	jbe	.L163
	movabsq	$2305843009213693948, %rcx
	testq	%rcx, %rax
	je	.L163
	leaq	1(%rax), %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	shrq	%rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L130:
	movdqu	(%rbx,%rax), %xmm3
	movups	%xmm3, (%rsi,%rax)
	addq	$16, %rax
	cmpq	%rdi, %rax
	jne	.L130
	movq	%rcx, %r9
	andq	$-2, %r9
	leaq	0(,%r9,8), %rax
	leaq	(%rbx,%rax), %rdi
	addq	%rax, %rsi
	cmpq	%r9, %rcx
	je	.L131
	movq	(%rdi), %rax
	movq	%rax, (%rsi)
.L131:
	addq	%r8, 16(%r12)
	movq	%rcx, %rax
.L157:
	movq	%rax, %rcx
	movdqa	%xmm0, %xmm1
	shrq	%rcx
	punpcklqdq	%xmm1, %xmm1
	salq	$4, %rcx
	addq	%rbx, %rcx
	.p2align 4,,10
	.p2align 3
.L135:
	movups	%xmm1, (%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rcx
	jne	.L135
	movq	%rax, %rdx
	andq	$-2, %rdx
	leaq	(%rbx,%rdx,8), %r13
	cmpq	%rax, %rdx
	jne	.L158
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L111:
	movq	8(%r12), %rsi
	movl	$268435455, %r8d
	movq	%r8, %rax
	subq	%rsi, %rdi
	sarq	$3, %rdi
	subq	%rdi, %rax
	cmpq	%rax, %rdx
	ja	.L221
	cmpq	%rdi, %rdx
	movq	%rdi, %rax
	cmovnb	%rdx, %rax
	addq	%rax, %rdi
	setc	%al
	subq	%rsi, %r13
	movzbl	%al, %eax
	testq	%rax, %rax
	jne	.L164
	testq	%rdi, %rdi
	jne	.L222
	xorl	%r8d, %r8d
	xorl	%eax, %eax
.L140:
	leaq	0(,%rdx,8), %r9
	leaq	(%rax,%r13), %r11
	leaq	0(%r13,%r9), %rsi
	addq	%rax, %rsi
	cmpq	%rsi, %rcx
	leaq	8(%rcx), %rsi
	setnb	%dil
	cmpq	%rsi, %r11
	setnb	%sil
	orb	%sil, %dil
	je	.L166
	leaq	-1(%rdx), %rsi
	cmpq	$3, %rsi
	jbe	.L166
	movq	(%rcx), %xmm0
	leaq	-2(%rdx), %rdi
	xorl	%esi, %esi
	shrq	%rdi
	addq	$1, %rdi
	punpcklqdq	%xmm0, %xmm0
	.p2align 4,,10
	.p2align 3
.L144:
	movq	%rsi, %r10
	addq	$1, %rsi
	salq	$4, %r10
	movups	%xmm0, (%r11,%r10)
	cmpq	%rsi, %rdi
	ja	.L144
	leaq	(%rdi,%rdi), %rsi
	salq	$4, %rdi
	addq	%r11, %rdi
	cmpq	%rsi, %rdx
	je	.L146
	movq	(%rcx), %rdx
	movq	%rdx, (%rdi)
.L146:
	movq	8(%r12), %rcx
	cmpq	%rcx, %rbx
	je	.L167
	leaq	-8(%rbx), %r10
	leaq	15(%rcx), %rdx
	subq	%rcx, %r10
	subq	%rax, %rdx
	movq	%r10, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rdx
	jbe	.L168
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rsi
	je	.L168
	leaq	1(%rsi), %r11
	xorl	%edx, %edx
	movq	%r11, %rdi
	shrq	%rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L149:
	movdqu	(%rcx,%rdx), %xmm4
	movups	%xmm4, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rdi, %rdx
	jne	.L149
	movq	%r11, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rsi
	addq	%rsi, %rcx
	addq	%rax, %rsi
	cmpq	%rdx, %r11
	je	.L151
	movq	(%rcx), %rdx
	movq	%rdx, (%rsi)
.L151:
	leaq	8(%rax,%r10), %rdi
.L147:
	movq	16(%r12), %rdx
	leaq	(%rdi,%r9), %rcx
	cmpq	%rdx, %rbx
	je	.L152
	subq	%rbx, %rdx
	leaq	-8(%rdx), %r10
	leaq	16(%rdi,%r9), %rdx
	movq	%r10, %rsi
	shrq	$3, %rsi
	cmpq	%rdx, %rbx
	leaq	16(%rbx), %rdx
	setnb	%dil
	cmpq	%rdx, %rcx
	setnb	%dl
	orb	%dl, %dil
	je	.L169
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rsi
	je	.L169
	addq	$1, %rsi
	xorl	%edx, %edx
	movq	%rsi, %rdi
	shrq	%rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L154:
	movdqu	(%rbx,%rdx), %xmm5
	movups	%xmm5, (%rcx,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rdi
	jne	.L154
	movq	%rsi, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rdi
	addq	%rdi, %rbx
	addq	%rcx, %rdi
	cmpq	%rdx, %rsi
	je	.L156
	movq	(%rbx), %rdx
	movq	%rdx, (%rdi)
.L156:
	leaq	8(%rcx,%r10), %rcx
.L152:
	movq	%rax, %xmm0
	movq	%rcx, %xmm6
	movq	%r8, 24(%r12)
	punpcklqdq	%xmm6, %xmm0
	movups	%xmm0, 8(%r12)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L164:
	.cfi_restore_state
	movl	$2147483640, %esi
	movl	$2147483640, %r14d
.L139:
	movq	(%r12), %r8
	movq	16(%r8), %rax
	movq	24(%r8), %rdi
	subq	%rax, %rdi
	cmpq	%rsi, %rdi
	jb	.L223
	addq	%rax, %rsi
	movq	%rsi, 16(%r8)
.L142:
	leaq	(%rax,%r14), %r8
	jmp	.L140
	.p2align 4,,10
	.p2align 3
.L166:
	xorl	%esi, %esi
	.p2align 4,,10
	.p2align 3
.L143:
	movq	(%rcx), %rdi
	movq	%rdi, (%r11,%rsi,8)
	addq	$1, %rsi
	cmpq	%rsi, %rdx
	jne	.L143
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L160:
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L114:
	movq	(%rdx,%rcx,8), %rsi
	movq	%rsi, (%rdi,%rcx,8)
	movq	%rcx, %rsi
	addq	$1, %rcx
	cmpq	%rax, %rsi
	jne	.L114
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L163:
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L129:
	movq	(%rbx,%rcx,8), %rdi
	movq	%rdi, (%rsi,%rcx,8)
	movq	%rcx, %rdi
	addq	$1, %rcx
	cmpq	%rax, %rdi
	jne	.L129
	addq	%r8, 16(%r12)
	addq	$1, %rax
	testq	%r9, %r9
	je	.L158
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L168:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L148:
	movq	(%rcx,%rdx,8), %rdi
	movq	%rdi, (%rax,%rdx,8)
	movq	%rdx, %rdi
	addq	$1, %rdx
	cmpq	%rsi, %rdi
	jne	.L148
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L169:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L153:
	movq	(%rbx,%rdx,8), %rdi
	movq	%rdi, (%rcx,%rdx,8)
	movq	%rdx, %rdi
	addq	$1, %rdx
	cmpq	%rsi, %rdi
	jne	.L153
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L161:
	movq	%rdi, %rsi
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L159:
	movq	%rdi, %rax
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L128:
	addq	%r8, %rsi
	movq	%rsi, 16(%r12)
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L167:
	movq	%rax, %rdi
	jmp	.L147
.L162:
	movq	%rdi, %rax
	jmp	.L125
.L223:
	movq	%r8, %rdi
	movq	%rcx, -48(%rbp)
	movq	%rdx, -40(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-40(%rbp), %rdx
	movq	-48(%rbp), %rcx
	jmp	.L142
.L222:
	cmpq	$268435455, %rdi
	cmova	%r8, %rdi
	leaq	0(,%rdi,8), %r14
	movq	%r14, %rsi
	jmp	.L139
.L221:
	leaq	.LC4(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE12514:
	.size	_ZNSt6vectorIN2v88internal8compiler17BranchElimination21ControlPathConditionsENS1_13ZoneAllocatorIS4_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS4_S7_EEmRKS4_, .-_ZNSt6vectorIN2v88internal8compiler17BranchElimination21ControlPathConditionsENS1_13ZoneAllocatorIS4_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS4_S7_EEmRKS4_
	.section	.rodata._ZNSt6vectorIbN2v88internal13ZoneAllocatorIbEEE14_M_fill_insertESt13_Bit_iteratormb.str1.1,"aMS",@progbits,1
.LC5:
	.string	"vector<bool>::_M_fill_insert"
	.section	.text._ZNSt6vectorIbN2v88internal13ZoneAllocatorIbEEE14_M_fill_insertESt13_Bit_iteratormb,"axG",@progbits,_ZNSt6vectorIbN2v88internal13ZoneAllocatorIbEEE14_M_fill_insertESt13_Bit_iteratormb,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIbN2v88internal13ZoneAllocatorIbEEE14_M_fill_insertESt13_Bit_iteratormb
	.type	_ZNSt6vectorIbN2v88internal13ZoneAllocatorIbEEE14_M_fill_insertESt13_Bit_iteratormb, @function
_ZNSt6vectorIbN2v88internal13ZoneAllocatorIbEEE14_M_fill_insertESt13_Bit_iteratormb:
.LFB12635:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%r8d, -72(%rbp)
	movq	%rsi, -64(%rbp)
	movq	%rdx, -56(%rbp)
	testq	%rcx, %rcx
	je	.L224
	movq	%rdi, %rbx
	movq	8(%rdi), %rax
	movq	%rsi, -80(%rbp)
	movq	%rsi, %r12
	movq	24(%rbx), %r9
	movq	40(%rbx), %rsi
	movq	%rcx, %r13
	movl	32(%rdi), %edx
	movl	-56(%rbp), %r14d
	movq	%r9, %rcx
	subq	%rax, %rsi
	subq	%rax, %rcx
	movq	%rsi, %rax
	movq	%rdx, %rdi
	leaq	(%rdx,%rcx,8), %rcx
	salq	$3, %rax
	subq	%rcx, %rax
	cmpq	%r13, %rax
	jb	.L226
	leaq	0(%r13,%rdx), %rax
	testq	%rax, %rax
	leaq	63(%rax), %rcx
	cmovns	%rax, %rcx
	sarq	$6, %rcx
	leaq	(%r9,%rcx,8), %r8
	movq	%rax, %rcx
	sarq	$63, %rcx
	shrq	$58, %rcx
	addq	%rcx, %rax
	andl	$63, %eax
	subq	%rcx, %rax
	jns	.L227
	addq	$64, %rax
	subq	$8, %r8
.L227:
	movabsq	$-9223372036854775808, %r15
	movq	%r9, %rcx
	movl	%r14d, %r11d
	subq	%r12, %rcx
	movq	%r11, -88(%rbp)
	leaq	(%rdx,%rcx,8), %rsi
	subq	%r11, %rsi
	movl	$1, %r11d
	testq	%rsi, %rsi
	jle	.L236
	.p2align 4,,10
	.p2align 3
.L228:
	testl	%edi, %edi
	je	.L231
.L308:
	subl	$1, %edi
	movq	%r11, %r10
	movl	%edi, %ecx
	salq	%cl, %r10
	testl	%eax, %eax
	je	.L233
.L309:
	subl	$1, %eax
	movq	%r11, %rdx
	movl	%eax, %ecx
	salq	%cl, %rdx
.L234:
	movq	(%r8), %rcx
	testq	%r10, (%r9)
	je	.L235
	orq	%rcx, %rdx
	movq	%rdx, (%r8)
	subq	$1, %rsi
	jne	.L228
.L236:
	movq	-88(%rbp), %rax
	addq	%r13, %rax
	leaq	63(%rax), %rdx
	cmovns	%rax, %rdx
	sarq	$6, %rdx
	leaq	(%r12,%rdx,8), %r8
	cqto
	shrq	$58, %rdx
	leaq	(%rax,%rdx), %r15
	andl	$63, %r15d
	subq	%rdx, %r15
	jns	.L230
	addq	$64, %r15
	subq	$8, %r8
.L230:
	movl	%r15d, %r9d
	cmpq	%r12, %r8
	je	.L238
	testl	%r14d, %r14d
	jne	.L302
	movq	%r8, %r10
	subq	%r12, %r10
	cmpb	$0, -72(%rbp)
	je	.L242
.L241:
	movq	-80(%rbp), %rdi
	movq	%r10, %rdx
	movl	$-1, %esi
	movl	%r9d, -88(%rbp)
	movq	%r8, -72(%rbp)
	call	memset@PLT
	testq	%r15, %r15
	movq	-72(%rbp), %r8
	movl	-88(%rbp), %r9d
	je	.L244
	movl	$64, %ecx
	movq	$-1, %rax
	subl	%r9d, %ecx
	shrq	%cl, %rax
	orq	%rax, (%r8)
	.p2align 4,,10
	.p2align 3
.L244:
	movl	32(%rbx), %ecx
	movq	24(%rbx), %rdx
	addq	%rcx, %r13
	leaq	63(%r13), %rax
	cmovns	%r13, %rax
	sarq	$6, %rax
	leaq	(%rdx,%rax,8), %rdx
	movq	%r13, %rax
	sarq	$63, %rax
	shrq	$58, %rax
	addq	%rax, %r13
	andl	$63, %r13d
	subq	%rax, %r13
	jns	.L299
	addq	$64, %r13
	subq	$8, %rdx
.L299:
	movq	%rdx, 24(%rbx)
.L300:
	movl	%r13d, 32(%rbx)
.L224:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L226:
	.cfi_restore_state
	movabsq	$17179869120, %rax
	movq	%rax, %rdx
	subq	%rcx, %rdx
	cmpq	%rdx, %r13
	ja	.L303
	cmpq	%rcx, %r13
	movq	%rcx, %rdx
	cmovnb	%r13, %rdx
	addq	%rdx, %rcx
	jc	.L252
	cmpq	%rax, %rcx
	cmova	%rax, %rcx
	addq	$63, %rcx
	shrq	$6, %rcx
	leaq	0(,%rcx,8), %rax
	movq	%rax, -88(%rbp)
	movq	%rax, %rsi
.L253:
	movq	(%rbx), %rdi
	movq	16(%rdi), %r15
	movq	24(%rdi), %rax
	subq	%r15, %rax
	cmpq	%rax, %rsi
	ja	.L304
	addq	%r15, %rsi
	movq	%rsi, 16(%rdi)
.L255:
	movq	8(%rbx), %rsi
	movq	%r12, %rdx
	subq	%rsi, %rdx
	cmpq	%r12, %rsi
	je	.L256
	movq	%r15, %rdi
	movq	%rdx, -96(%rbp)
	call	memmove@PLT
	movq	-96(%rbp), %rdx
.L256:
	movl	%r14d, %esi
	leaq	(%r15,%rdx), %rdi
	movq	%rsi, -96(%rbp)
	testq	%rsi, %rsi
	je	.L284
	xorl	%edx, %edx
	movq	%r12, %r9
	movl	$1, %r10d
	movl	%edx, %ecx
	jmp	.L262
	.p2align 4,,10
	.p2align 3
.L260:
	addl	$1, %ecx
	subq	$1, %rsi
	je	.L305
.L262:
	movq	%r10, %rdx
	movq	(%rdi), %r11
	salq	%cl, %rdx
	movq	%rdx, %rax
	movq	%r11, %r8
	notq	%rax
	orq	%rdx, %r8
	andq	%r11, %rax
	testq	%rdx, (%r9)
	cmovne	%r8, %rax
	movq	%rax, (%rdi)
	cmpl	$63, %ecx
	jne	.L260
	addq	$8, %r9
	addq	$8, %rdi
	xorl	%ecx, %ecx
	subq	$1, %rsi
	jne	.L262
.L305:
	movl	%ecx, %ecx
	movq	%rcx, %rdx
.L257:
	addq	%rcx, %r13
	leaq	63(%r13), %rax
	cmovns	%r13, %rax
	sarq	$6, %rax
	leaq	(%rdi,%rax,8), %r8
	movq	%r13, %rax
	sarq	$63, %rax
	shrq	$58, %rax
	leaq	0(%r13,%rax), %r9
	andl	$63, %r9d
	subq	%rax, %r9
	jns	.L263
	addq	$64, %r9
	subq	$8, %r8
.L263:
	movl	%r9d, %r13d
	cmpq	%rdi, %r8
	je	.L264
	testl	%edx, %edx
	je	.L265
	movl	%edx, %ecx
	leaq	8(%rdi), %rsi
	movq	$-1, %rax
	movq	%r8, %rdx
	salq	%cl, %rax
	subq	%rsi, %rdx
	cmpb	$0, -72(%rbp)
	movq	(%rdi), %rcx
	je	.L266
	orq	%rcx, %rax
	movq	%rax, (%rdi)
	movq	%rsi, %rdi
.L267:
	movl	$-1, %esi
	movq	%r8, -104(%rbp)
	movq	%r9, -72(%rbp)
	call	memset@PLT
	movq	-72(%rbp), %r9
	movq	-104(%rbp), %r8
	testq	%r9, %r9
	jne	.L306
	.p2align 4,,10
	.p2align 3
.L270:
	movq	24(%rbx), %rax
	movl	32(%rbx), %edx
	subq	%r12, %rax
	leaq	(%rdx,%rax,8), %rdx
	subq	-96(%rbp), %rdx
	testq	%rdx, %rdx
	jle	.L274
	movq	-80(%rbp), %r9
	movl	$1, %edi
	jmp	.L281
	.p2align 4,,10
	.p2align 3
.L277:
	addl	$1, %r14d
	cmpl	$63, %r13d
	je	.L307
.L279:
	addl	$1, %r13d
	subq	$1, %rdx
	je	.L274
.L281:
	movq	(%r8), %rsi
	movl	%r13d, %ecx
	movq	%rdi, %rax
	movq	%rdi, %r10
	salq	%cl, %rax
	movl	%r14d, %ecx
	salq	%cl, %r10
	movq	%rsi, %rcx
	orq	%rax, %rcx
	notq	%rax
	andq	%rsi, %rax
	testq	%r10, (%r9)
	cmovne	%rcx, %rax
	movq	%rax, (%r8)
	cmpl	$63, %r14d
	jne	.L277
	addq	$8, %r9
	xorl	%r14d, %r14d
	cmpl	$63, %r13d
	jne	.L279
.L307:
	addq	$8, %r8
	xorl	%r13d, %r13d
	subq	$1, %rdx
	jne	.L281
.L274:
	cmpq	$0, 8(%rbx)
	je	.L282
	movq	$0, 24(%rbx)
	movl	$0, 32(%rbx)
	movq	$0, 8(%rbx)
	movl	$0, 16(%rbx)
.L282:
	movq	-88(%rbp), %rax
	movq	%r15, 8(%rbx)
	movl	$0, 16(%rbx)
	addq	%r15, %rax
	movq	%r8, 24(%rbx)
	movq	%rax, 40(%rbx)
	jmp	.L300
	.p2align 4,,10
	.p2align 3
.L235:
	notq	%rdx
	andq	%rcx, %rdx
	movq	%rdx, (%r8)
	subq	$1, %rsi
	je	.L236
	testl	%edi, %edi
	jne	.L308
.L231:
	subq	$8, %r9
	movq	%r15, %r10
	movl	$63, %edi
	testl	%eax, %eax
	jne	.L309
.L233:
	subq	$8, %r8
	movq	%r15, %rdx
	movl	$63, %eax
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L302:
	movl	%r14d, %ecx
	leaq	8(%r12), %rdx
	movq	$-1, %rax
	movq	%r8, %r10
	salq	%cl, %rax
	subq	%rdx, %r10
	cmpb	$0, -72(%rbp)
	movq	(%r12), %rcx
	je	.L240
	orq	%rcx, %rax
	movq	%rdx, -80(%rbp)
	movq	%rax, (%r12)
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L240:
	notq	%rax
	movq	%rdx, -80(%rbp)
	andq	%rcx, %rax
	movq	%rax, (%r12)
.L242:
	movq	-80(%rbp), %rdi
	xorl	%esi, %esi
	movq	%r10, %rdx
	movl	%r9d, -88(%rbp)
	movq	%r8, -72(%rbp)
	call	memset@PLT
	testq	%r15, %r15
	movq	-72(%rbp), %r8
	movl	-88(%rbp), %r9d
	je	.L244
	movl	$64, %ecx
	movq	$-1, %rax
	subl	%r9d, %ecx
	shrq	%cl, %rax
	notq	%rax
	andq	%rax, (%r8)
	jmp	.L244
	.p2align 4,,10
	.p2align 3
.L238:
	cmpl	%r14d, %r15d
	je	.L244
	movq	$-1, %rdx
	movl	$64, %ecx
	subl	%r15d, %ecx
	movq	%rdx, %rax
	shrq	%cl, %rax
	movl	%r14d, %ecx
	salq	%cl, %rdx
	movq	(%r8), %rcx
	andq	%rdx, %rax
	movq	%rax, %rdx
	orq	%rcx, %rax
	notq	%rdx
	andq	%rcx, %rdx
	cmpb	$0, -72(%rbp)
	cmove	%rdx, %rax
	movq	%rax, (%r8)
	jmp	.L244
	.p2align 4,,10
	.p2align 3
.L265:
	movq	%r8, %rdx
	subq	%rdi, %rdx
	cmpb	$0, -72(%rbp)
	jne	.L267
.L268:
	xorl	%esi, %esi
	movq	%r8, -104(%rbp)
	movq	%r9, -72(%rbp)
	call	memset@PLT
	movq	-72(%rbp), %r9
	movq	-104(%rbp), %r8
	testq	%r9, %r9
	je	.L270
	movl	$64, %ecx
	movq	$-1, %rax
	subl	%r13d, %ecx
	shrq	%cl, %rax
	notq	%rax
	andq	%rax, (%r8)
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L264:
	cmpl	%edx, %r9d
	je	.L270
	movq	$-1, %rsi
	movl	$64, %ecx
	subl	%r9d, %ecx
	movq	%rsi, %rax
	shrq	%cl, %rax
	movl	%edx, %ecx
	salq	%cl, %rsi
	movq	(%r8), %rcx
	andq	%rsi, %rax
	movq	%rax, %rdx
	orq	%rcx, %rax
	notq	%rdx
	andq	%rcx, %rdx
	cmpb	$0, -72(%rbp)
	cmove	%rdx, %rax
	movq	%rax, (%r8)
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L306:
	movl	$64, %ecx
	movq	$-1, %rax
	subl	%r13d, %ecx
	shrq	%cl, %rax
	orq	%rax, (%r8)
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L266:
	notq	%rax
	andq	%rcx, %rax
	movq	%rax, (%rdi)
	movq	%rsi, %rdi
	jmp	.L268
	.p2align 4,,10
	.p2align 3
.L304:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r15
	jmp	.L255
	.p2align 4,,10
	.p2align 3
.L284:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	jmp	.L257
.L303:
	leaq	.LC5(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L252:
	movq	$2147483640, -88(%rbp)
	movl	$2147483640, %esi
	jmp	.L253
	.cfi_endproc
.LFE12635:
	.size	_ZNSt6vectorIbN2v88internal13ZoneAllocatorIbEEE14_M_fill_insertESt13_Bit_iteratormb, .-_ZNSt6vectorIbN2v88internal13ZoneAllocatorIbEEE14_M_fill_insertESt13_Bit_iteratormb
	.section	.text._ZN2v88internal8compiler17BranchElimination16UpdateConditionsEPNS1_4NodeENS2_21ControlPathConditionsE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17BranchElimination16UpdateConditionsEPNS1_4NodeENS2_21ControlPathConditionsE
	.type	_ZN2v88internal8compiler17BranchElimination16UpdateConditionsEPNS1_4NodeENS2_21ControlPathConditionsE, @function
_ZN2v88internal8compiler17BranchElimination16UpdateConditionsEPNS1_4NodeENS2_21ControlPathConditionsE:
.LFB11011:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$64, %rsp
	movq	80(%rbx), %r9
	movl	20(%rsi), %r13d
	movq	%fs:40, %rdi
	movq	%rdi, -40(%rbp)
	xorl	%edi, %edi
	movq	64(%rbx), %rsi
	movl	88(%rbx), %edx
	movq	%r9, %rcx
	movl	%r13d, %r14d
	subq	%rsi, %rcx
	movq	%rdx, %rdi
	andl	$16777215, %r14d
	leaq	(%rdx,%rcx,8), %rdx
	cmpq	%rdx, %r14
	jnb	.L332
.L311:
	movq	%r14, %rdx
	movl	%r13d, %ecx
	xorl	%r14d, %r14d
	shrq	$6, %rdx
	leaq	(%rsi,%rdx,8), %rsi
	movl	$1, %edx
	salq	%cl, %rdx
	movq	(%rsi), %rcx
	testq	%rcx, %rdx
	jne	.L313
	orq	%rcx, %rdx
	movl	$1, %r14d
	movq	%rdx, (%rsi)
.L313:
	movq	40(%rbx), %r8
	movq	32(%rbx), %rsi
	movl	20(%rax), %r13d
	movq	%r8, %rcx
	subq	%rsi, %rcx
	andl	$16777215, %r13d
	sarq	$3, %rcx
	cmpq	%rcx, %r13
	jnb	.L333
.L314:
	leaq	(%rsi,%r13,8), %rcx
	movq	%r12, %rdi
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	je	.L317
	movq	32(%rdx), %rsi
	testq	%r12, %r12
	je	.L318
	cmpq	32(%r12), %rsi
	je	.L334
.L319:
	movq	%r12, (%rcx)
.L322:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L335
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L334:
	.cfi_restore_state
	cmpq	%rdx, %r12
	je	.L320
.L321:
	movq	(%rdx), %rbx
	cmpq	%rbx, (%rdi)
	jne	.L319
	movq	8(%rdx), %rbx
	cmpq	%rbx, 8(%rdi)
	jne	.L319
	movzbl	16(%rdx), %ebx
	cmpb	%bl, 16(%rdi)
	jne	.L319
	movq	24(%rdx), %rdx
	movq	24(%rdi), %rdi
	cmpq	%rdi, %rdx
	jne	.L321
	.p2align 4,,10
	.p2align 3
.L320:
	testb	%r14b, %r14b
	movl	$0, %edx
	cmove	%rdx, %rax
	jmp	.L322
	.p2align 4,,10
	.p2align 3
.L318:
	testq	%rsi, %rsi
	jne	.L319
	jmp	.L321
	.p2align 4,,10
	.p2align 3
.L332:
	leaq	1(%r14), %rcx
	cmpq	%rcx, %rdx
	jbe	.L312
	movq	%rcx, %rdx
	andl	$63, %ecx
	sarq	$6, %rdx
	movl	%ecx, 88(%rbx)
	leaq	(%rsi,%rdx,8), %rdx
	movq	%rdx, 80(%rbx)
	jmp	.L311
	.p2align 4,,10
	.p2align 3
.L333:
	leaq	1(%r13), %rdx
	movq	$0, -72(%rbp)
	cmpq	%rdx, %rcx
	jb	.L336
	jbe	.L314
	leaq	(%rsi,%rdx,8), %rdx
	cmpq	%rdx, %r8
	je	.L314
	movq	%rdx, 40(%rbx)
	jmp	.L314
	.p2align 4,,10
	.p2align 3
.L317:
	xorl	%esi, %esi
	testq	%r12, %r12
	je	.L320
	cmpq	32(%r12), %rsi
	jne	.L319
	jmp	.L334
	.p2align 4,,10
	.p2align 3
.L336:
	leaq	-72(%rbp), %r9
	subq	%rcx, %rdx
	movq	%r8, %rsi
	movq	%rax, -88(%rbp)
	leaq	24(%rbx), %rdi
	movq	%r9, %rcx
	call	_ZNSt6vectorIN2v88internal8compiler17BranchElimination21ControlPathConditionsENS1_13ZoneAllocatorIS4_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS4_S7_EEmRKS4_
	movq	32(%rbx), %rsi
	movq	-88(%rbp), %rax
	jmp	.L314
	.p2align 4,,10
	.p2align 3
.L312:
	movl	%edi, -56(%rbp)
	subq	%rdx, %rcx
	movq	-56(%rbp), %rdx
	movq	%r9, %rsi
	leaq	56(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%rax, -88(%rbp)
	movq	%r9, -64(%rbp)
	call	_ZNSt6vectorIbN2v88internal13ZoneAllocatorIbEEE14_M_fill_insertESt13_Bit_iteratormb
	movq	64(%rbx), %rsi
	movq	-88(%rbp), %rax
	jmp	.L311
.L335:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11011:
	.size	_ZN2v88internal8compiler17BranchElimination16UpdateConditionsEPNS1_4NodeENS2_21ControlPathConditionsE, .-_ZN2v88internal8compiler17BranchElimination16UpdateConditionsEPNS1_4NodeENS2_21ControlPathConditionsE
	.section	.text._ZN2v88internal8compiler17BranchElimination11ReduceMergeEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17BranchElimination11ReduceMergeEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17BranchElimination11ReduceMergeEPNS1_4NodeE, @function
_ZN2v88internal8compiler17BranchElimination11ReduceMergeEPNS1_4NodeE:
.LFB11004:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	32(%rsi), %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 14, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %r9
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L339
	movl	8(%r9), %eax
	leaq	16(%r9), %r8
	movq	16(%r9), %r9
.L339:
	cltq
	leaq	(%r8,%rax,8), %r10
	cmpq	%r10, %r8
	je	.L340
	movq	64(%rdi), %r11
	movq	80(%rdi), %rax
	movl	$1, %r12d
	movl	88(%rdi), %edx
	subq	%r11, %rax
	leaq	(%rdx,%rax,8), %rbx
	movq	%r8, %rdx
.L344:
	movq	(%rdx), %rax
	movl	20(%rax), %ecx
	movl	%ecx, %eax
	andl	$16777215, %eax
	cmpq	%rbx, %rax
	jnb	.L370
	movq	%r12, %r14
	shrq	$6, %rax
	salq	%cl, %r14
	testq	%r14, (%r11,%rax,8)
	je	.L370
	addq	$8, %rdx
	cmpq	%rdx, %r10
	jne	.L344
.L340:
	movl	20(%r9), %eax
	movq	32(%rdi), %rbx
	xorl	%edx, %edx
	movq	40(%rdi), %r9
	andl	$16777215, %eax
	subq	%rbx, %r9
	sarq	$3, %r9
	cmpq	%r9, %rax
	jb	.L388
.L345:
	addq	$8, %r8
	cmpq	%r8, %r10
	je	.L346
	movq	(%r8), %rax
	movl	20(%rax), %eax
	andl	$16777215, %eax
	cmpq	%rax, %r9
	jbe	.L347
	movq	(%rbx,%rax,8), %rax
	jmp	.L352
	.p2align 4,,10
	.p2align 3
.L360:
	movq	24(%rax), %rax
.L352:
	testq	%rax, %rax
	je	.L347
	movq	32(%rax), %rcx
	testq	%rdx, %rdx
	je	.L389
	cmpq	%rcx, 32(%rdx)
	jnb	.L350
	cmpq	$0, 32(%rax)
	jne	.L360
.L351:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L370:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L347:
	.cfi_restore_state
	xorl	%eax, %eax
	xorl	%ecx, %ecx
	testq	%rdx, %rdx
	jne	.L350
	jmp	.L345
	.p2align 4,,10
	.p2align 3
.L392:
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L390
.L350:
	movq	32(%rdx), %r11
	cmpq	%rcx, %r11
	jbe	.L391
	testq	%r11, %r11
	je	.L351
	jmp	.L392
	.p2align 4,,10
	.p2align 3
.L388:
	movq	(%rbx,%rax,8), %rdx
	jmp	.L345
	.p2align 4,,10
	.p2align 3
.L389:
	testq	%rcx, %rcx
	jne	.L360
	jmp	.L351
	.p2align 4,,10
	.p2align 3
.L390:
	testq	%rax, %rax
	jne	.L351
	jmp	.L345
.L391:
	cmpq	%rdx, %rax
	je	.L345
.L355:
	cmpq	$0, 32(%rdx)
	je	.L351
	movq	24(%rdx), %rdx
	testq	%rax, %rax
	je	.L351
	cmpq	$0, 32(%rax)
	je	.L351
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L345
	testq	%rdx, %rdx
	jne	.L355
	jmp	.L351
	.p2align 4,,10
	.p2align 3
.L346:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler17BranchElimination16UpdateConditionsEPNS1_4NodeENS2_21ControlPathConditionsE
	.cfi_endproc
.LFE11004:
	.size	_ZN2v88internal8compiler17BranchElimination11ReduceMergeEPNS1_4NodeE, .-_ZN2v88internal8compiler17BranchElimination11ReduceMergeEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17BranchElimination30TakeConditionsFromFirstControlEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17BranchElimination30TakeConditionsFromFirstControlEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17BranchElimination30TakeConditionsFromFirstControlEPNS1_4NodeE, @function
_ZN2v88internal8compiler17BranchElimination30TakeConditionsFromFirstControlEPNS1_4NodeE:
.LFB11010:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	xorl	%esi, %esi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	64(%r12), %rsi
	movl	88(%r12), %edi
	movl	20(%rax), %ecx
	movq	80(%r12), %rax
	movl	%ecx, %edx
	subq	%rsi, %rax
	andl	$16777215, %edx
	leaq	(%rdi,%rax,8), %rax
	cmpq	%rax, %rdx
	jnb	.L399
	movq	%rdx, %rdi
	movl	$1, %eax
	shrq	$6, %rdi
	salq	%cl, %rax
	testq	%rax, (%rsi,%rdi,8)
	jne	.L401
.L399:
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L401:
	.cfi_restore_state
	movq	32(%r12), %rcx
	movq	40(%r12), %rax
	xorl	%r8d, %r8d
	subq	%rcx, %rax
	sarq	$3, %rax
	cmpq	%rax, %rdx
	jnb	.L397
	movq	(%rcx,%rdx,8), %r8
.L397:
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r8, %rdx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler17BranchElimination16UpdateConditionsEPNS1_4NodeENS2_21ControlPathConditionsE
	.cfi_endproc
.LFE11010:
	.size	_ZN2v88internal8compiler17BranchElimination30TakeConditionsFromFirstControlEPNS1_4NodeE, .-_ZN2v88internal8compiler17BranchElimination30TakeConditionsFromFirstControlEPNS1_4NodeE
	.section	.rodata._ZN2v88internal8compiler17BranchElimination12ReduceBranchEPNS1_4NodeE.str1.1,"aMS",@progbits,1
.LC6:
	.string	"unreachable code"
	.section	.text._ZN2v88internal8compiler17BranchElimination12ReduceBranchEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17BranchElimination12ReduceBranchEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17BranchElimination12ReduceBranchEPNS1_4NodeE, @function
_ZN2v88internal8compiler17BranchElimination12ReduceBranchEPNS1_4NodeE:
.LFB11000:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %rbx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L403
	movq	16(%rbx), %rbx
.L403:
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	32(%r13), %rcx
	movl	20(%rax), %edx
	movq	%rax, %r12
	movq	40(%r13), %rax
	subq	%rcx, %rax
	andl	$16777215, %edx
	sarq	$3, %rax
	cmpq	%rax, %rdx
	jnb	.L404
	movq	(%rcx,%rdx,8), %rax
	testq	%rax, %rax
	jne	.L408
.L404:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler17BranchElimination23SimplifyBranchConditionEPNS1_4NodeE
	addq	$24, %rsp
	movq	%r14, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler17BranchElimination30TakeConditionsFromFirstControlEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L405:
	.cfi_restore_state
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L404
.L408:
	cmpq	%rbx, (%rax)
	jne	.L405
	movq	8(%rax), %r15
	movzbl	16(%rax), %ecx
	movzbl	23(%r15), %eax
	leaq	32(%r15), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L406
	testl	%eax, %eax
	jle	.L409
.L435:
	cmpq	$0, (%rdx)
	je	.L410
.L409:
	movq	(%r15), %rdi
	cmpw	$61, 16(%rdi)
	jne	.L434
.L410:
	movq	24(%r14), %rbx
	testq	%rbx, %rbx
	je	.L420
	testb	%cl, %cl
	jne	.L414
	jmp	.L419
	.p2align 4,,10
	.p2align 3
.L416:
	movq	8(%r13), %rdi
	movq	112(%r13), %rdx
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L420
.L419:
	movl	16(%rbx), %edx
	movl	%edx, %eax
	shrl	%eax
	andl	$1, %edx
	leaq	3(%rax,%rax,2), %rax
	leaq	(%rbx,%rax,8), %rsi
	movq	(%rsi), %rax
	jne	.L415
	movq	%rax, %rsi
	movq	(%rax), %rax
.L415:
	movzwl	16(%rax), %eax
	cmpw	$4, %ax
	je	.L416
	cmpw	$5, %ax
	jne	.L417
	movq	8(%r13), %rdi
	movq	%r12, %rdx
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L419
.L420:
	movq	112(%r13), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L422:
	.cfi_restore_state
	movq	8(%r13), %rdi
	movq	%r12, %rdx
	movq	(%rdi), %rax
	call	*16(%rax)
.L424:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L420
.L414:
	movl	16(%rbx), %edx
	movl	%edx, %eax
	shrl	%eax
	andl	$1, %edx
	leaq	3(%rax,%rax,2), %rax
	leaq	(%rbx,%rax,8), %rsi
	movq	(%rsi), %rax
	jne	.L421
	movq	%rax, %rsi
	movq	(%rax), %rax
.L421:
	movzwl	16(%rax), %eax
	cmpw	$4, %ax
	je	.L422
	cmpw	$5, %ax
	jne	.L417
	movq	8(%r13), %rdi
	movq	112(%r13), %rdx
	movq	(%rdi), %rax
	call	*16(%rax)
	jmp	.L424
	.p2align 4,,10
	.p2align 3
.L434:
	movb	%cl, -49(%rbp)
	call	_ZN2v88internal8compiler15IsSafetyCheckOfEPKNS1_8OperatorE@PLT
	movq	(%r14), %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler15IsSafetyCheckOfEPKNS1_8OperatorE@PLT
	movl	%ebx, %edi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler19CombineSafetyChecksENS1_13IsSafetyCheckES2_@PLT
	movzbl	-49(%rbp), %ecx
	cmpb	%al, %bl
	movl	%eax, %edx
	je	.L410
	movq	16(%r13), %rax
	movq	(%r15), %rsi
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder17MarkAsSafetyCheckEPKNS1_8OperatorENS1_13IsSafetyCheckE@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movzbl	-49(%rbp), %ecx
	jmp	.L410
	.p2align 4,,10
	.p2align 3
.L406:
	movq	32(%r15), %rdx
	movl	8(%rdx), %eax
	addq	$16, %rdx
	testl	%eax, %eax
	jg	.L435
	jmp	.L409
.L417:
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE11000:
	.size	_ZN2v88internal8compiler17BranchElimination12ReduceBranchEPNS1_4NodeE, .-_ZN2v88internal8compiler17BranchElimination12ReduceBranchEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17BranchElimination16UpdateConditionsEPNS1_4NodeENS2_21ControlPathConditionsES4_S4_b,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17BranchElimination16UpdateConditionsEPNS1_4NodeENS2_21ControlPathConditionsES4_S4_b
	.type	_ZN2v88internal8compiler17BranchElimination16UpdateConditionsEPNS1_4NodeENS2_21ControlPathConditionsES4_S4_b, @function
_ZN2v88internal8compiler17BranchElimination16UpdateConditionsEPNS1_4NodeENS2_21ControlPathConditionsES4_S4_b:
.LFB11012:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	subq	$16, %rsp
	movq	32(%rdi), %rcx
	movl	20(%rsi), %edx
	movq	%r8, -40(%rbp)
	movq	40(%r12), %rax
	movq	104(%rdi), %rdi
	andl	$16777215, %edx
	subq	%rcx, %rax
	sarq	$3, %rax
	cmpq	%rax, %rdx
	jb	.L458
.L437:
	xorl	%edx, %edx
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.L439
.L449:
	movq	32(%rbx), %rsi
	leaq	1(%rsi), %rcx
.L438:
	cmpq	%rcx, %rax
	jne	.L439
	cmpq	(%rdx), %r13
	je	.L459
.L439:
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$39, %rdx
	jbe	.L460
	leaq	40(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L446:
	movq	%r13, %xmm0
	movb	%r9b, 16(%rax)
	movq	%rax, %rdx
	movl	$1, %ecx
	movhps	-40(%rbp), %xmm0
	movq	%rbx, 24(%rax)
	movups	%xmm0, (%rax)
	testq	%rbx, %rbx
	je	.L447
	movq	32(%rbx), %rcx
	addq	$1, %rcx
.L447:
	movq	%rcx, 32(%rax)
.L448:
	addq	$16, %rsp
	movq	%r14, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler17BranchElimination16UpdateConditionsEPNS1_4NodeENS2_21ControlPathConditionsE
	.p2align 4,,10
	.p2align 3
.L458:
	.cfi_restore_state
	movq	(%rcx,%rdx,8), %rdx
	testq	%rdx, %rdx
	je	.L437
	movq	32(%rdx), %rax
	testq	%rbx, %rbx
	jne	.L449
	movl	$1, %ecx
	xorl	%esi, %esi
	jmp	.L438
	.p2align 4,,10
	.p2align 3
.L459:
	movq	-40(%rbp), %rax
	cmpq	8(%rdx), %rax
	jne	.L439
	cmpb	16(%rdx), %r9b
	jne	.L439
	cmpq	$0, 32(%rdx)
	jne	.L461
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L460:
	movl	$40, %esi
	movl	%r9d, -44(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-44(%rbp), %r9d
	jmp	.L446
.L461:
	movq	24(%rdx), %rax
	xorl	%ecx, %ecx
	testq	%rax, %rax
	je	.L442
	movq	32(%rax), %rcx
.L442:
	cmpq	%rcx, %rsi
	jne	.L439
	cmpq	%rbx, %rax
	je	.L448
	movq	%rbx, %rcx
.L444:
	movq	(%rax), %rsi
	cmpq	%rsi, (%rcx)
	jne	.L439
	movq	8(%rax), %rsi
	cmpq	%rsi, 8(%rcx)
	jne	.L439
	movzbl	16(%rax), %esi
	cmpb	%sil, 16(%rcx)
	jne	.L439
	movq	24(%rax), %rax
	movq	24(%rcx), %rcx
	cmpq	%rcx, %rax
	jne	.L444
	jmp	.L448
	.cfi_endproc
.LFE11012:
	.size	_ZN2v88internal8compiler17BranchElimination16UpdateConditionsEPNS1_4NodeENS2_21ControlPathConditionsES4_S4_b, .-_ZN2v88internal8compiler17BranchElimination16UpdateConditionsEPNS1_4NodeENS2_21ControlPathConditionsES4_S4_b
	.section	.text._ZN2v88internal8compiler17BranchElimination27ReduceDeoptimizeConditionalEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17BranchElimination27ReduceDeoptimizeConditionalEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17BranchElimination27ReduceDeoptimizeConditionalEPNS1_4NodeE, @function
_ZN2v88internal8compiler17BranchElimination27ReduceDeoptimizeConditionalEPNS1_4NodeE:
.LFB11001:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzwl	16(%rdi), %r15d
	call	_ZN2v88internal8compiler22DeoptimizeParametersOfEPKNS1_8OperatorE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movdqu	(%rax), %xmm1
	movaps	%xmm1, -112(%rbp)
	movdqu	16(%rax), %xmm2
	movaps	%xmm2, -96(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	64(%r13), %rsi
	movl	88(%r13), %edi
	movl	20(%rax), %ecx
	movq	%rax, %r8
	movq	80(%r13), %rax
	movl	%ecx, %edx
	subq	%rsi, %rax
	andl	$16777215, %edx
	leaq	(%rdi,%rax,8), %rax
	cmpq	%rax, %rdx
	jnb	.L463
	movq	%rdx, %rdi
	movl	$1, %eax
	shrq	$6, %rdi
	salq	%cl, %rax
	testq	%rax, (%rsi,%rdi,8)
	jne	.L464
.L463:
	xorl	%eax, %eax
.L465:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L481
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L464:
	.cfi_restore_state
	movq	32(%r13), %rcx
	movq	40(%r13), %rax
	cmpw	$13, %r15w
	sete	%r15b
	subq	%rcx, %rax
	sarq	$3, %rax
	cmpq	%rax, %rdx
	jnb	.L474
	movq	(%rcx,%rdx,8), %rdx
	testq	%rdx, %rdx
	je	.L466
	movq	%rdx, %rax
	jmp	.L470
	.p2align 4,,10
	.p2align 3
.L467:
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L466
.L470:
	cmpq	(%rax), %rbx
	jne	.L467
	movq	8(%rax), %r10
	movzbl	16(%rax), %eax
	movq	%r8, -144(%rbp)
	movq	(%r10), %rdi
	movq	%r10, -152(%rbp)
	movb	%al, -136(%rbp)
	call	_ZN2v88internal8compiler15IsSafetyCheckOfEPKNS1_8OperatorE@PLT
	movzbl	-88(%rbp), %esi
	movl	%eax, %edi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19CombineSafetyChecksENS1_13IsSafetyCheckES2_@PLT
	movq	-144(%rbp), %r8
	cmpb	%al, %bl
	movl	%eax, %edx
	je	.L469
	movq	-152(%rbp), %r10
	movq	16(%r13), %rax
	movq	%r8, -152(%rbp)
	movq	(%r10), %rsi
	movq	8(%rax), %rdi
	movq	%r10, -144(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder17MarkAsSafetyCheckEPKNS1_8OperatorENS1_13IsSafetyCheckE@PLT
	movq	-144(%rbp), %r10
	movq	%rax, %rsi
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	-152(%rbp), %r8
.L469:
	cmpb	-136(%rbp), %r15b
	jne	.L471
	movq	8(%r13), %rdi
	movq	112(%r13), %rdx
	movq	%r14, %rcx
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
.L472:
	movq	112(%r13), %rax
	jmp	.L465
	.p2align 4,,10
	.p2align 3
.L474:
	xorl	%edx, %edx
.L466:
	movl	%r15d, %r9d
	movq	%r12, %r8
	movq	%rbx, %rcx
	movq	%r12, %rsi
	andl	$1, %r9d
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler17BranchElimination16UpdateConditionsEPNS1_4NodeENS2_21ControlPathConditionsES4_S4_b
	jmp	.L465
	.p2align 4,,10
	.p2align 3
.L471:
	movq	16(%r13), %rax
	movq	-128(%rbp), %xmm0
	movq	%r14, %xmm3
	leaq	-104(%rbp), %rcx
	movzbl	-111(%rbp), %edx
	movzbl	-112(%rbp), %esi
	movq	%r8, -136(%rbp)
	movq	8(%rax), %rdi
	punpcklqdq	%xmm3, %xmm0
	movq	(%rax), %r12
	movaps	%xmm0, -128(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder10DeoptimizeENS0_14DeoptimizeKindENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceE@PLT
	movq	-136(%rbp), %r8
	movdqa	-128(%rbp), %xmm0
	movq	%r12, %rdi
	leaq	-80(%rbp), %rcx
	movq	%rax, %rsi
	movl	$3, %edx
	movq	%r8, -64(%rbp)
	xorl	%r8d, %r8d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %rdx
	movq	16(%r13), %rax
	movq	8(%rax), %rsi
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler14NodeProperties17MergeControlToEndEPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS1_4NodeE@PLT
	movq	16(%r13), %rax
	movq	8(%r13), %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rsi
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L472
.L481:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11001:
	.size	_ZN2v88internal8compiler17BranchElimination27ReduceDeoptimizeConditionalEPNS1_4NodeE, .-_ZN2v88internal8compiler17BranchElimination27ReduceDeoptimizeConditionalEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17BranchElimination8ReduceIfEPNS1_4NodeEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17BranchElimination8ReduceIfEPNS1_4NodeEb
	.type	_ZN2v88internal8compiler17BranchElimination8ReduceIfEPNS1_4NodeEb, @function
_ZN2v88internal8compiler17BranchElimination8ReduceIfEPNS1_4NodeEb:
.LFB11002:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	xorl	%esi, %esi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%edx, %ebx
	subq	$8, %rsp
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	40(%r12), %rdx
	xorl	%r10d, %r10d
	movl	20(%rax), %ecx
	movq	%rax, %r8
	movq	32(%r12), %rax
	movl	%ecx, %esi
	subq	%rax, %rdx
	andl	$16777215, %esi
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L483
	movq	(%rax,%rsi,8), %r10
.L483:
	movq	64(%r12), %rdx
	movq	80(%r12), %rax
	movl	88(%r12), %edi
	subq	%rdx, %rax
	leaq	(%rdi,%rax,8), %rax
	cmpq	%rsi, %rax
	jbe	.L489
	movl	$1, %eax
	shrq	$6, %rsi
	salq	%cl, %rax
	testq	%rax, (%rdx,%rsi,8)
	jne	.L492
.L489:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L492:
	.cfi_restore_state
	xorl	$251658240, %ecx
	movq	32(%r8), %r11
	andl	$251658240, %ecx
	je	.L493
.L487:
	addq	$8, %rsp
	movzbl	%bl, %r9d
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	movq	%r11, %rcx
	popq	%r12
	movq	%r10, %rdx
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler17BranchElimination16UpdateConditionsEPNS1_4NodeENS2_21ControlPathConditionsES4_S4_b
	.p2align 4,,10
	.p2align 3
.L493:
	.cfi_restore_state
	movq	16(%r11), %r11
	jmp	.L487
	.cfi_endproc
.LFE11002:
	.size	_ZN2v88internal8compiler17BranchElimination8ReduceIfEPNS1_4NodeEb, .-_ZN2v88internal8compiler17BranchElimination8ReduceIfEPNS1_4NodeEb
	.section	.text._ZN2v88internal8compiler17BranchElimination6ReduceEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17BranchElimination6ReduceEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17BranchElimination6ReduceEPNS1_4NodeE, @function
_ZN2v88internal8compiler17BranchElimination6ReduceEPNS1_4NodeE:
.LFB10995:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	cmpw	$61, 16(%rax)
	ja	.L495
	movzwl	16(%rax), %edx
	leaq	.L497(%rip), %rcx
	movslq	(%rcx,%rdx,4), %rdx
	addq	%rcx, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal8compiler17BranchElimination6ReduceEPNS1_4NodeE,"a",@progbits
	.align 4
	.align 4
.L497:
	.long	.L504-.L497
	.long	.L507-.L497
	.long	.L502-.L497
	.long	.L495-.L497
	.long	.L501-.L497
	.long	.L500-.L497
	.long	.L495-.L497
	.long	.L495-.L497
	.long	.L495-.L497
	.long	.L495-.L497
	.long	.L499-.L497
	.long	.L495-.L497
	.long	.L498-.L497
	.long	.L498-.L497
	.long	.L495-.L497
	.long	.L495-.L497
	.long	.L495-.L497
	.long	.L495-.L497
	.long	.L495-.L497
	.long	.L495-.L497
	.long	.L495-.L497
	.long	.L495-.L497
	.long	.L495-.L497
	.long	.L495-.L497
	.long	.L495-.L497
	.long	.L495-.L497
	.long	.L495-.L497
	.long	.L495-.L497
	.long	.L495-.L497
	.long	.L495-.L497
	.long	.L495-.L497
	.long	.L495-.L497
	.long	.L495-.L497
	.long	.L495-.L497
	.long	.L495-.L497
	.long	.L495-.L497
	.long	.L495-.L497
	.long	.L495-.L497
	.long	.L495-.L497
	.long	.L495-.L497
	.long	.L495-.L497
	.long	.L495-.L497
	.long	.L495-.L497
	.long	.L495-.L497
	.long	.L495-.L497
	.long	.L495-.L497
	.long	.L495-.L497
	.long	.L495-.L497
	.long	.L495-.L497
	.long	.L495-.L497
	.long	.L495-.L497
	.long	.L495-.L497
	.long	.L495-.L497
	.long	.L495-.L497
	.long	.L495-.L497
	.long	.L495-.L497
	.long	.L495-.L497
	.long	.L495-.L497
	.long	.L495-.L497
	.long	.L495-.L497
	.long	.L495-.L497
	.long	.L505-.L497
	.section	.text._ZN2v88internal8compiler17BranchElimination6ReduceEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L495:
	movl	40(%rax), %eax
	testl	%eax, %eax
	jg	.L507
.L505:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L498:
	jmp	_ZN2v88internal8compiler17BranchElimination27ReduceDeoptimizeConditionalEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L507:
	jmp	_ZN2v88internal8compiler17BranchElimination30TakeConditionsFromFirstControlEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L504:
	xorl	%edx, %edx
	jmp	_ZN2v88internal8compiler17BranchElimination16UpdateConditionsEPNS1_4NodeENS2_21ControlPathConditionsE
	.p2align 4,,10
	.p2align 3
.L502:
	jmp	_ZN2v88internal8compiler17BranchElimination12ReduceBranchEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L501:
	movl	$1, %edx
	jmp	_ZN2v88internal8compiler17BranchElimination8ReduceIfEPNS1_4NodeEb
	.p2align 4,,10
	.p2align 3
.L500:
	xorl	%edx, %edx
	jmp	_ZN2v88internal8compiler17BranchElimination8ReduceIfEPNS1_4NodeEb
	.p2align 4,,10
	.p2align 3
.L499:
	jmp	_ZN2v88internal8compiler17BranchElimination11ReduceMergeEPNS1_4NodeE
	.cfi_endproc
.LFE10995:
	.size	_ZN2v88internal8compiler17BranchElimination6ReduceEPNS1_4NodeE, .-_ZN2v88internal8compiler17BranchElimination6ReduceEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17BranchElimination18ReduceOtherControlEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17BranchElimination18ReduceOtherControlEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17BranchElimination18ReduceOtherControlEPNS1_4NodeE, @function
_ZN2v88internal8compiler17BranchElimination18ReduceOtherControlEPNS1_4NodeE:
.LFB13028:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	xorl	%esi, %esi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	64(%r12), %rsi
	movl	88(%r12), %edi
	movl	20(%rax), %ecx
	movq	80(%r12), %rax
	movl	%ecx, %edx
	subq	%rsi, %rax
	leaq	(%rdi,%rax,8), %rdi
	andl	$16777215, %edx
	xorl	%eax, %eax
	cmpq	%rdi, %rdx
	jnb	.L509
	movq	%rdx, %r8
	movl	$1, %edi
	shrq	$6, %r8
	salq	%cl, %rdi
	testq	%rdi, (%rsi,%r8,8)
	jne	.L516
.L509:
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L516:
	.cfi_restore_state
	movq	32(%r12), %rcx
	movq	40(%r12), %rax
	xorl	%r8d, %r8d
	subq	%rcx, %rax
	sarq	$3, %rax
	cmpq	%rax, %rdx
	jnb	.L510
	movq	(%rcx,%rdx,8), %r8
.L510:
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler17BranchElimination16UpdateConditionsEPNS1_4NodeENS2_21ControlPathConditionsE
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE13028:
	.size	_ZN2v88internal8compiler17BranchElimination18ReduceOtherControlEPNS1_4NodeE, .-_ZN2v88internal8compiler17BranchElimination18ReduceOtherControlEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17BranchElimination10ReduceLoopEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17BranchElimination10ReduceLoopEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17BranchElimination10ReduceLoopEPNS1_4NodeE, @function
_ZN2v88internal8compiler17BranchElimination10ReduceLoopEPNS1_4NodeE:
.LFB11003:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	xorl	%esi, %esi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	64(%r12), %rsi
	movl	88(%r12), %edi
	movl	20(%rax), %ecx
	movq	80(%r12), %rax
	movl	%ecx, %edx
	subq	%rsi, %rax
	leaq	(%rdi,%rax,8), %rdi
	andl	$16777215, %edx
	xorl	%eax, %eax
	cmpq	%rdi, %rdx
	jnb	.L518
	movq	%rdx, %r8
	movl	$1, %edi
	shrq	$6, %r8
	salq	%cl, %rdi
	testq	%rdi, (%rsi,%r8,8)
	jne	.L525
.L518:
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L525:
	.cfi_restore_state
	movq	32(%r12), %rcx
	movq	40(%r12), %rax
	xorl	%r8d, %r8d
	subq	%rcx, %rax
	sarq	$3, %rax
	cmpq	%rax, %rdx
	jnb	.L519
	movq	(%rcx,%rdx,8), %r8
.L519:
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler17BranchElimination16UpdateConditionsEPNS1_4NodeENS2_21ControlPathConditionsE
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11003:
	.size	_ZN2v88internal8compiler17BranchElimination10ReduceLoopEPNS1_4NodeE, .-_ZN2v88internal8compiler17BranchElimination10ReduceLoopEPNS1_4NodeE
	.section	.text.unlikely._ZN2v88internal8compiler17BranchElimination11ReduceStartEPNS1_4NodeE,"ax",@progbits
	.align 2
.LCOLDB7:
	.section	.text._ZN2v88internal8compiler17BranchElimination11ReduceStartEPNS1_4NodeE,"ax",@progbits
.LHOTB7:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17BranchElimination11ReduceStartEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17BranchElimination11ReduceStartEPNS1_4NodeE, @function
_ZN2v88internal8compiler17BranchElimination11ReduceStartEPNS1_4NodeE:
.LFB11005:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$64, %rsp
	movl	20(%rsi), %r13d
	movq	80(%rbx), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	64(%rdi), %r8
	movl	88(%rdi), %edx
	movq	%rsi, %rcx
	movl	%r13d, %eax
	subq	%r8, %rcx
	movq	%rdx, %rdi
	andl	$16777215, %eax
	leaq	(%rdx,%rcx,8), %rdx
	cmpq	%rdx, %rax
	jnb	.L542
.L527:
	shrq	$6, %rax
	movl	%r13d, %ecx
	xorl	%r14d, %r14d
	leaq	(%r8,%rax,8), %rdx
	movl	$1, %eax
	salq	%cl, %rax
	movq	(%rdx), %rcx
	testq	%rcx, %rax
	jne	.L529
	orq	%rcx, %rax
	movl	$1, %r14d
	movq	%rax, (%rdx)
.L529:
	movq	40(%rbx), %rsi
	movq	32(%rbx), %rcx
	movl	20(%r12), %r13d
	movq	%rsi, %rax
	subq	%rcx, %rax
	andl	$16777215, %r13d
	sarq	$3, %rax
	cmpq	%rax, %r13
	jnb	.L543
.L530:
	leaq	(%rcx,%r13,8), %rdx
	movq	(%rdx), %rax
	testq	%rax, %rax
	je	.L533
	cmpq	$0, 32(%rax)
	je	.L534
	movq	$0, (%rdx)
	movq	%r12, %rax
.L535:
	movq	-40(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L544
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L533:
	.cfi_restore_state
	testb	%r14b, %r14b
	cmovne	%r12, %rax
	jmp	.L535
	.p2align 4,,10
	.p2align 3
.L542:
	leaq	1(%rax), %rcx
	cmpq	%rcx, %rdx
	jbe	.L528
	movq	%rcx, %rdx
	andl	$63, %ecx
	sarq	$6, %rdx
	movl	%ecx, 88(%rbx)
	leaq	(%r8,%rdx,8), %rdx
	movq	%rdx, 80(%rbx)
	jmp	.L527
	.p2align 4,,10
	.p2align 3
.L543:
	leaq	1(%r13), %rdx
	movq	$0, -72(%rbp)
	cmpq	%rdx, %rax
	jb	.L545
	jbe	.L530
	leaq	(%rcx,%rdx,8), %rax
	cmpq	%rax, %rsi
	je	.L530
	movq	%rax, 40(%rbx)
	jmp	.L530
	.p2align 4,,10
	.p2align 3
.L545:
	leaq	-72(%rbp), %rcx
	subq	%rax, %rdx
	leaq	24(%rbx), %rdi
	call	_ZNSt6vectorIN2v88internal8compiler17BranchElimination21ControlPathConditionsENS1_13ZoneAllocatorIS4_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS4_S7_EEmRKS4_
	movq	32(%rbx), %rcx
	jmp	.L530
	.p2align 4,,10
	.p2align 3
.L528:
	movl	%edi, -56(%rbp)
	subq	%rdx, %rcx
	movq	-56(%rbp), %rdx
	xorl	%r8d, %r8d
	leaq	56(%rbx), %rdi
	movq	%rax, -88(%rbp)
	movq	%rsi, -64(%rbp)
	call	_ZNSt6vectorIbN2v88internal13ZoneAllocatorIbEEE14_M_fill_insertESt13_Bit_iteratormb
	movq	64(%rbx), %r8
	movq	-88(%rbp), %rax
	jmp	.L527
.L544:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal8compiler17BranchElimination11ReduceStartEPNS1_4NodeE
	.cfi_startproc
	.type	_ZN2v88internal8compiler17BranchElimination11ReduceStartEPNS1_4NodeE.cold, @function
_ZN2v88internal8compiler17BranchElimination11ReduceStartEPNS1_4NodeE.cold:
.LFSB11005:
.L534:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movq	0, %rax
	ud2
	.cfi_endproc
.LFE11005:
	.section	.text._ZN2v88internal8compiler17BranchElimination11ReduceStartEPNS1_4NodeE
	.size	_ZN2v88internal8compiler17BranchElimination11ReduceStartEPNS1_4NodeE, .-_ZN2v88internal8compiler17BranchElimination11ReduceStartEPNS1_4NodeE
	.section	.text.unlikely._ZN2v88internal8compiler17BranchElimination11ReduceStartEPNS1_4NodeE
	.size	_ZN2v88internal8compiler17BranchElimination11ReduceStartEPNS1_4NodeE.cold, .-_ZN2v88internal8compiler17BranchElimination11ReduceStartEPNS1_4NodeE.cold
.LCOLDE7:
	.section	.text._ZN2v88internal8compiler17BranchElimination11ReduceStartEPNS1_4NodeE
.LHOTE7:
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler17BranchEliminationC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS0_4ZoneENS2_5PhaseE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler17BranchEliminationC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS0_4ZoneENS2_5PhaseE, @function
_GLOBAL__sub_I__ZN2v88internal8compiler17BranchEliminationC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS0_4ZoneENS2_5PhaseE:
.LFB12915:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE12915:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler17BranchEliminationC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS0_4ZoneENS2_5PhaseE, .-_GLOBAL__sub_I__ZN2v88internal8compiler17BranchEliminationC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS0_4ZoneENS2_5PhaseE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler17BranchEliminationC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS0_4ZoneENS2_5PhaseE
	.weak	_ZTVN2v88internal8compiler17BranchEliminationE
	.section	.data.rel.ro._ZTVN2v88internal8compiler17BranchEliminationE,"awG",@progbits,_ZTVN2v88internal8compiler17BranchEliminationE,comdat
	.align 8
	.type	_ZTVN2v88internal8compiler17BranchEliminationE, @object
	.size	_ZTVN2v88internal8compiler17BranchEliminationE, 56
_ZTVN2v88internal8compiler17BranchEliminationE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal8compiler17BranchEliminationD1Ev
	.quad	_ZN2v88internal8compiler17BranchEliminationD0Ev
	.quad	_ZNK2v88internal8compiler17BranchElimination12reducer_nameEv
	.quad	_ZN2v88internal8compiler17BranchElimination6ReduceEPNS1_4NodeE
	.quad	_ZN2v88internal8compiler7Reducer8FinalizeEv
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
