	.file	"typed-optimization.cc"
	.text
	.section	.rodata._ZNK2v88internal8compiler17TypedOptimization12reducer_nameEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"TypedOptimization"
	.section	.text._ZNK2v88internal8compiler17TypedOptimization12reducer_nameEv,"axG",@progbits,_ZNK2v88internal8compiler17TypedOptimization12reducer_nameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal8compiler17TypedOptimization12reducer_nameEv
	.type	_ZNK2v88internal8compiler17TypedOptimization12reducer_nameEv, @function
_ZNK2v88internal8compiler17TypedOptimization12reducer_nameEv:
.LFB10163:
	.cfi_startproc
	endbr64
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE10163:
	.size	_ZNK2v88internal8compiler17TypedOptimization12reducer_nameEv, .-_ZNK2v88internal8compiler17TypedOptimization12reducer_nameEv
	.section	.text._ZN2v88internal8compiler17TypedOptimizationD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17TypedOptimizationD2Ev
	.type	_ZN2v88internal8compiler17TypedOptimizationD2Ev, @function
_ZN2v88internal8compiler17TypedOptimizationD2Ev:
.LFB20235:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE20235:
	.size	_ZN2v88internal8compiler17TypedOptimizationD2Ev, .-_ZN2v88internal8compiler17TypedOptimizationD2Ev
	.globl	_ZN2v88internal8compiler17TypedOptimizationD1Ev
	.set	_ZN2v88internal8compiler17TypedOptimizationD1Ev,_ZN2v88internal8compiler17TypedOptimizationD2Ev
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_123ResolveSameValueRenamesEPNS1_4NodeE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_123ResolveSameValueRenamesEPNS1_4NodeE, @function
_ZN2v88internal8compiler12_GLOBAL__N_123ResolveSameValueRenamesEPNS1_4NodeE:
.LFB20255:
	.cfi_startproc
	movq	%rdi, %rax
	movq	(%rax), %rdx
	movzwl	16(%rdx), %edx
	cmpw	$221, %dx
	je	.L5
	ja	.L6
.L31:
	cmpw	$40, %dx
	je	.L5
	cmpw	$58, %dx
	jne	.L29
.L5:
	movzbl	23(%rax), %edx
	movq	32(%rax), %rcx
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L8
	testl	%edx, %edx
	je	.L10
	testq	%rcx, %rcx
	je	.L30
.L10:
	movq	%rcx, %rax
	movq	(%rax), %rdx
	movzwl	16(%rdx), %edx
	cmpw	$221, %dx
	je	.L5
	jbe	.L31
.L6:
	subw	$227, %dx
	cmpw	$1, %dx
	jbe	.L5
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	movl	8(%rcx), %edx
	movq	16(%rcx), %rcx
	testl	%edx, %edx
	jle	.L10
	testq	%rcx, %rcx
	jne	.L10
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	ret
.L29:
	ret
	.cfi_endproc
.LFE20255:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_123ResolveSameValueRenamesEPNS1_4NodeE, .-_ZN2v88internal8compiler12_GLOBAL__N_123ResolveSameValueRenamesEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17TypedOptimizationD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17TypedOptimizationD0Ev
	.type	_ZN2v88internal8compiler17TypedOptimizationD0Ev, @function
_ZN2v88internal8compiler17TypedOptimizationD0Ev:
.LFB20237:
	.cfi_startproc
	endbr64
	movl	$64, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE20237:
	.size	_ZN2v88internal8compiler17TypedOptimizationD0Ev, .-_ZN2v88internal8compiler17TypedOptimizationD0Ev
	.section	.rodata._ZN2v88internal8compiler12_GLOBAL__N_131NumberOpFromSpeculativeNumberOpEPNS1_25SimplifiedOperatorBuilderEPKNS1_8OperatorE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"unreachable code"
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_131NumberOpFromSpeculativeNumberOpEPNS1_25SimplifiedOperatorBuilderEPKNS1_8OperatorE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_131NumberOpFromSpeculativeNumberOpEPNS1_25SimplifiedOperatorBuilderEPKNS1_8OperatorE, @function
_ZN2v88internal8compiler12_GLOBAL__N_131NumberOpFromSpeculativeNumberOpEPNS1_25SimplifiedOperatorBuilderEPKNS1_8OperatorE:
.LFB20284:
	.cfi_startproc
	movzwl	16(%rsi), %eax
	subl	$122, %eax
	cmpw	$31, %ax
	ja	.L34
	leaq	.L36(%rip), %rdx
	movzwl	%ax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler12_GLOBAL__N_131NumberOpFromSpeculativeNumberOpEPNS1_25SimplifiedOperatorBuilderEPKNS1_8OperatorE,"a",@progbits
	.align 4
	.align 4
.L36:
	.long	.L42-.L36
	.long	.L41-.L36
	.long	.L40-.L36
	.long	.L34-.L36
	.long	.L34-.L36
	.long	.L34-.L36
	.long	.L34-.L36
	.long	.L34-.L36
	.long	.L34-.L36
	.long	.L34-.L36
	.long	.L34-.L36
	.long	.L34-.L36
	.long	.L34-.L36
	.long	.L34-.L36
	.long	.L34-.L36
	.long	.L34-.L36
	.long	.L34-.L36
	.long	.L34-.L36
	.long	.L34-.L36
	.long	.L34-.L36
	.long	.L34-.L36
	.long	.L34-.L36
	.long	.L34-.L36
	.long	.L34-.L36
	.long	.L34-.L36
	.long	.L34-.L36
	.long	.L34-.L36
	.long	.L34-.L36
	.long	.L39-.L36
	.long	.L38-.L36
	.long	.L37-.L36
	.long	.L35-.L36
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_131NumberOpFromSpeculativeNumberOpEPNS1_25SimplifiedOperatorBuilderEPKNS1_8OperatorE
	.p2align 4,,10
	.p2align 3
.L42:
	jmp	_ZN2v88internal8compiler25SimplifiedOperatorBuilder11NumberEqualEv@PLT
	.p2align 4,,10
	.p2align 3
.L41:
	jmp	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14NumberLessThanEv@PLT
	.p2align 4,,10
	.p2align 3
.L40:
	jmp	_ZN2v88internal8compiler25SimplifiedOperatorBuilder21NumberLessThanOrEqualEv@PLT
	.p2align 4,,10
	.p2align 3
.L39:
	jmp	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14NumberSubtractEv@PLT
	.p2align 4,,10
	.p2align 3
.L38:
	jmp	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14NumberMultiplyEv@PLT
	.p2align 4,,10
	.p2align 3
.L37:
	jmp	_ZN2v88internal8compiler25SimplifiedOperatorBuilder12NumberDivideEv@PLT
	.p2align 4,,10
	.p2align 3
.L35:
	jmp	_ZN2v88internal8compiler25SimplifiedOperatorBuilder13NumberModulusEv@PLT
	.p2align 4,,10
	.p2align 3
.L34:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE20284:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_131NumberOpFromSpeculativeNumberOpEPNS1_25SimplifiedOperatorBuilderEPKNS1_8OperatorE, .-_ZN2v88internal8compiler12_GLOBAL__N_131NumberOpFromSpeculativeNumberOpEPNS1_25SimplifiedOperatorBuilderEPKNS1_8OperatorE
	.section	.text._ZN2v88internal8compiler17TypedOptimizationC2EPNS1_15AdvancedReducer6EditorEPNS1_23CompilationDependenciesEPNS1_7JSGraphEPNS1_12JSHeapBrokerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17TypedOptimizationC2EPNS1_15AdvancedReducer6EditorEPNS1_23CompilationDependenciesEPNS1_7JSGraphEPNS1_12JSHeapBrokerE
	.type	_ZN2v88internal8compiler17TypedOptimizationC2EPNS1_15AdvancedReducer6EditorEPNS1_23CompilationDependenciesEPNS1_7JSGraphEPNS1_12JSHeapBrokerE, @function
_ZN2v88internal8compiler17TypedOptimizationC2EPNS1_15AdvancedReducer6EditorEPNS1_23CompilationDependenciesEPNS1_7JSGraphEPNS1_12JSHeapBrokerE:
.LFB20232:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal8compiler17TypedOptimizationE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rsi, 8(%rdi)
	movq	360(%rcx), %rsi
	movq	%rax, (%rdi)
	movq	(%rcx), %rax
	movq	%rcx, 24(%rdi)
	addq	$112, %rsi
	movq	%r8, 32(%rdi)
	movq	%rdx, 16(%rdi)
	movq	(%rax), %rdx
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler4Type12HeapConstantEPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEPNS0_4ZoneE@PLT
	movq	%r12, %rdi
	movq	%rax, 40(%rbx)
	movq	24(%rbx), %rax
	movq	(%rax), %rdx
	movq	360(%rax), %rsi
	movq	(%rdx), %rdx
	addq	$120, %rsi
	call	_ZN2v88internal8compiler4Type12HeapConstantEPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEPNS0_4ZoneE@PLT
	movq	%rax, 48(%rbx)
	call	_ZN2v88internal8compiler9TypeCache3GetEv@PLT
	movq	%rax, 56(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20232:
	.size	_ZN2v88internal8compiler17TypedOptimizationC2EPNS1_15AdvancedReducer6EditorEPNS1_23CompilationDependenciesEPNS1_7JSGraphEPNS1_12JSHeapBrokerE, .-_ZN2v88internal8compiler17TypedOptimizationC2EPNS1_15AdvancedReducer6EditorEPNS1_23CompilationDependenciesEPNS1_7JSGraphEPNS1_12JSHeapBrokerE
	.globl	_ZN2v88internal8compiler17TypedOptimizationC1EPNS1_15AdvancedReducer6EditorEPNS1_23CompilationDependenciesEPNS1_7JSGraphEPNS1_12JSHeapBrokerE
	.set	_ZN2v88internal8compiler17TypedOptimizationC1EPNS1_15AdvancedReducer6EditorEPNS1_23CompilationDependenciesEPNS1_7JSGraphEPNS1_12JSHeapBrokerE,_ZN2v88internal8compiler17TypedOptimizationC2EPNS1_15AdvancedReducer6EditorEPNS1_23CompilationDependenciesEPNS1_7JSGraphEPNS1_12JSHeapBrokerE
	.section	.text._ZN2v88internal8compiler17TypedOptimization21ReduceConvertReceiverEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17TypedOptimization21ReduceConvertReceiverEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17TypedOptimization21ReduceConvertReceiverEPNS1_4NodeE, @function
_ZN2v88internal8compiler17TypedOptimization21ReduceConvertReceiverEPNS1_4NodeE:
.LFB20256:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	xorl	%esi, %esi
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r14, %rdi
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movl	$1, %esi
	movq	%r14, %rdi
	movq	%rax, %rbx
	movq	8(%rax), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	cmpq	$75431937, -64(%rbp)
	jne	.L55
.L48:
	movq	8(%r12), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%rbx, %rax
.L51:
	movq	-56(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L56
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	.cfi_restore_state
	leaq	-64(%rbp), %r15
	movl	$75431937, %esi
	movq	%rax, %r13
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L48
	cmpq	$385, -64(%rbp)
	jne	.L57
.L49:
	movq	8(%r12), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r13, %rax
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L57:
	movl	$385, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L49
	xorl	%eax, %eax
	jmp	.L51
.L56:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20256:
	.size	_ZN2v88internal8compiler17TypedOptimization21ReduceConvertReceiverEPNS1_4NodeE, .-_ZN2v88internal8compiler17TypedOptimization21ReduceConvertReceiverEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17TypedOptimization21ReduceCheckHeapObjectEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17TypedOptimization21ReduceCheckHeapObjectEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17TypedOptimization21ReduceCheckHeapObjectEPNS1_4NodeE, @function
_ZN2v88internal8compiler17TypedOptimization21ReduceCheckHeapObjectEPNS1_4NodeE:
.LFB20257:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	movq	%r12, %rdi
	subq	$24, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rax, %rbx
	movq	8(%rax), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler10BitsetType11SignedSmallEv@PLT
	leaq	-48(%rbp), %rdi
	movl	%eax, %esi
	orl	$1, %esi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	testb	%r8b, %r8b
	jne	.L60
	movq	8(%r13), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%rbx, %rax
.L60:
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L64
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L64:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20257:
	.size	_ZN2v88internal8compiler17TypedOptimization21ReduceCheckHeapObjectEPNS1_4NodeE, .-_ZN2v88internal8compiler17TypedOptimization21ReduceCheckHeapObjectEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17TypedOptimization24ReduceCheckNotTaggedHoleEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17TypedOptimization24ReduceCheckNotTaggedHoleEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17TypedOptimization24ReduceCheckNotTaggedHoleEPNS1_4NodeE, @function
_ZN2v88internal8compiler17TypedOptimization24ReduceCheckNotTaggedHoleEPNS1_4NodeE:
.LFB20258:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	movq	%r12, %rdi
	subq	$24, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	leaq	-48(%rbp), %rdi
	movl	$8388609, %esi
	movq	%rax, %rbx
	movq	8(%rax), %rax
	movq	%rax, -48(%rbp)
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	testb	%r8b, %r8b
	jne	.L67
	movq	8(%r13), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%rbx, %rax
.L67:
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L71
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L71:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20258:
	.size	_ZN2v88internal8compiler17TypedOptimization24ReduceCheckNotTaggedHoleEPNS1_4NodeE, .-_ZN2v88internal8compiler17TypedOptimization24ReduceCheckNotTaggedHoleEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17TypedOptimization15ReduceCheckMapsEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17TypedOptimization15ReduceCheckMapsEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17TypedOptimization15ReduceCheckMapsEPNS1_4NodeE, @function
_ZN2v88internal8compiler17TypedOptimization15ReduceCheckMapsEPNS1_4NodeE:
.LFB20259:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	xorl	%esi, %esi
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -136(%rbp)
	movq	%r13, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	8(%rax), %rbx
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	%rbx, -120(%rbp)
	testb	$1, %bl
	jne	.L76
	movl	(%rbx), %edx
	testl	%edx, %edx
	je	.L95
.L76:
	xorl	%eax, %eax
.L80:
	movq	-56(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L96
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L95:
	.cfi_restore_state
	leaq	-120(%rbp), %rdi
	movq	%rax, %r12
	leaq	-96(%rbp), %r15
	call	_ZNK2v88internal8compiler4Type14AsHeapConstantEv@PLT
	leaq	-112(%rbp), %rdi
	movdqu	8(%rax), %xmm1
	movaps	%xmm1, -112(%rbp)
	call	_ZNK2v88internal8compiler13HeapObjectRef3mapEv@PLT
	movq	%r15, %rdi
	movq	%rax, -96(%rbp)
	movq	%rdx, -88(%rbp)
	call	_ZNK2v88internal8compiler6MapRef9is_stableEv@PLT
	testb	%al, %al
	je	.L76
	movdqa	-96(%rbp), %xmm0
	movq	0(%r13), %rax
	movb	$1, -80(%rbp)
	movl	$1, %ebx
	leaq	-72(%rbp), %r14
	movups	%xmm0, -72(%rbp)
	cmpl	$1, 20(%rax)
	jg	.L75
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L78:
	movq	0(%r13), %rax
	addl	$1, %ebx
	cmpl	%ebx, 20(%rax)
	jle	.L76
.L75:
	movl	%ebx, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	8(%rax), %rax
	movq	%rax, -96(%rbp)
	testb	$1, %al
	jne	.L78
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L78
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type14AsHeapConstantEv@PLT
	movq	%r14, %rsi
	leaq	8(%rax), %rdi
	call	_ZNK2v88internal8compiler9ObjectRef6equalsERKS2_@PLT
	testb	%al, %al
	je	.L78
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler6MapRef13CanTransitionEv@PLT
	testb	%al, %al
	jne	.L97
.L79:
	movq	%r12, %rax
	jmp	.L80
.L97:
	movq	-136(%rbp), %rax
	movq	%r14, %rsi
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler23CompilationDependencies17DependOnStableMapERKNS1_6MapRefE@PLT
	jmp	.L79
.L96:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20259:
	.size	_ZN2v88internal8compiler17TypedOptimization15ReduceCheckMapsEPNS1_4NodeE, .-_ZN2v88internal8compiler17TypedOptimization15ReduceCheckMapsEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17TypedOptimization17ReduceCheckNumberEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17TypedOptimization17ReduceCheckNumberEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17TypedOptimization17ReduceCheckNumberEPNS1_4NodeE, @function
_ZN2v88internal8compiler17TypedOptimization17ReduceCheckNumberEPNS1_4NodeE:
.LFB20261:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	movq	%r12, %rdi
	subq	$24, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rax, %rbx
	movq	8(%rax), %rax
	movq	%rax, -48(%rbp)
	cmpq	$7263, %rax
	jne	.L106
.L99:
	movq	8(%r13), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%rbx, %rax
.L100:
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L107
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L106:
	.cfi_restore_state
	leaq	-48(%rbp), %rdi
	movl	$7263, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L99
	xorl	%eax, %eax
	jmp	.L100
.L107:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20261:
	.size	_ZN2v88internal8compiler17TypedOptimization17ReduceCheckNumberEPNS1_4NodeE, .-_ZN2v88internal8compiler17TypedOptimization17ReduceCheckNumberEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17TypedOptimization17ReduceCheckStringEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17TypedOptimization17ReduceCheckStringEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17TypedOptimization17ReduceCheckStringEPNS1_4NodeE, @function
_ZN2v88internal8compiler17TypedOptimization17ReduceCheckStringEPNS1_4NodeE:
.LFB20262:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	movq	%r12, %rdi
	subq	$24, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rax, %rbx
	movq	8(%rax), %rax
	movq	%rax, -48(%rbp)
	cmpq	$16417, %rax
	jne	.L116
.L109:
	movq	8(%r13), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%rbx, %rax
.L110:
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L117
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L116:
	.cfi_restore_state
	leaq	-48(%rbp), %rdi
	movl	$16417, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L109
	xorl	%eax, %eax
	jmp	.L110
.L117:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20262:
	.size	_ZN2v88internal8compiler17TypedOptimization17ReduceCheckStringEPNS1_4NodeE, .-_ZN2v88internal8compiler17TypedOptimization17ReduceCheckStringEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17TypedOptimization35ReduceCheckEqualsInternalizedStringEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17TypedOptimization35ReduceCheckEqualsInternalizedStringEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17TypedOptimization35ReduceCheckEqualsInternalizedStringEPNS1_4NodeE, @function
_ZN2v88internal8compiler17TypedOptimization35ReduceCheckEqualsInternalizedStringEPNS1_4NodeE:
.LFB20263:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	xorl	%esi, %esi
	movq	%r12, %rdi
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	8(%rax), %r13
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	8(%rax), %rax
	movq	%rax, -32(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	%rax, %r12
	cmpq	%r13, -32(%rbp)
	je	.L119
	leaq	-32(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	movl	$0, %eax
	cmove	%rax, %r12
.L119:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L123
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L123:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20263:
	.size	_ZN2v88internal8compiler17TypedOptimization35ReduceCheckEqualsInternalizedStringEPNS1_4NodeE, .-_ZN2v88internal8compiler17TypedOptimization35ReduceCheckEqualsInternalizedStringEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17TypedOptimization23ReduceCheckEqualsSymbolEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17TypedOptimization23ReduceCheckEqualsSymbolEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17TypedOptimization23ReduceCheckEqualsSymbolEPNS1_4NodeE, @function
_ZN2v88internal8compiler17TypedOptimization23ReduceCheckEqualsSymbolEPNS1_4NodeE:
.LFB24273:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	xorl	%esi, %esi
	movq	%r12, %rdi
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	8(%rax), %r13
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	8(%rax), %rax
	movq	%rax, -32(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	%rax, %r12
	cmpq	-32(%rbp), %r13
	je	.L125
	leaq	-32(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	movl	$0, %eax
	cmove	%rax, %r12
.L125:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L129
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L129:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24273:
	.size	_ZN2v88internal8compiler17TypedOptimization23ReduceCheckEqualsSymbolEPNS1_4NodeE, .-_ZN2v88internal8compiler17TypedOptimization23ReduceCheckEqualsSymbolEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17TypedOptimization15ReduceLoadFieldEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17TypedOptimization15ReduceLoadFieldEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17TypedOptimization15ReduceLoadFieldEPNS1_4NodeE, @function
_ZN2v88internal8compiler17TypedOptimization15ReduceLoadFieldEPNS1_4NodeE:
.LFB20265:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	(%r12), %rdi
	movq	8(%rax), %r13
	call	_ZN2v88internal8compiler13FieldAccessOfEPKNS1_8OperatorE@PLT
	movq	%rax, %r8
	movabsq	$-4294967041, %rax
	andq	(%r8), %rax
	cmpq	$1, %rax
	je	.L140
.L131:
	xorl	%eax, %eax
.L134:
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L141
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L140:
	.cfi_restore_state
	movq	%r13, -104(%rbp)
	testb	$1, %r13b
	jne	.L131
	movl	0(%r13), %eax
	testl	%eax, %eax
	jne	.L131
	leaq	-104(%rbp), %rdi
	call	_ZNK2v88internal8compiler4Type14AsHeapConstantEv@PLT
	leaq	-96(%rbp), %rdi
	movdqu	8(%rax), %xmm1
	movaps	%xmm1, -96(%rbp)
	call	_ZNK2v88internal8compiler13HeapObjectRef3mapEv@PLT
	leaq	-80(%rbp), %rdi
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZNK2v88internal8compiler6MapRef9is_stableEv@PLT
	testb	%al, %al
	je	.L131
	movdqa	-80(%rbp), %xmm0
	leaq	-56(%rbp), %r13
	movq	16(%rbx), %rdi
	movb	$1, -64(%rbp)
	movq	%r13, %rsi
	movups	%xmm0, -56(%rbp)
	call	_ZN2v88internal8compiler23CompilationDependencies17DependOnStableMapERKNS1_6MapRefE@PLT
	movq	24(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	8(%rbx), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rax, %r13
	movq	%r12, %rsi
	movq	(%rdi), %rax
	movq	%r13, %rdx
	call	*32(%rax)
	movq	%r13, %rax
	jmp	.L134
.L141:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20265:
	.size	_ZN2v88internal8compiler17TypedOptimization15ReduceLoadFieldEPNS1_4NodeE, .-_ZN2v88internal8compiler17TypedOptimization15ReduceLoadFieldEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17TypedOptimization17ReduceNumberFloorEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17TypedOptimization17ReduceNumberFloorEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17TypedOptimization17ReduceNumberFloorEPNS1_4NodeE, @function
_ZN2v88internal8compiler17TypedOptimization17ReduceNumberFloorEPNS1_4NodeE:
.LFB20266:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	56(%r13), %rdx
	movq	%rax, %r12
	movq	8(%rax), %rax
	movq	336(%rdx), %rsi
	movq	%rax, -64(%rbp)
	cmpq	%rsi, %rax
	jne	.L162
.L143:
	movq	%r12, %rax
.L147:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L163
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L162:
	.cfi_restore_state
	leaq	-64(%rbp), %r14
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L143
	cmpq	$1119, -64(%rbp)
	jne	.L144
.L148:
	movq	(%r12), %rax
	movzwl	16(%rax), %eax
	cmpl	$152, %eax
	je	.L145
	cmpl	$135, %eax
	je	.L145
.L151:
	xorl	%eax, %eax
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L144:
	movl	$1119, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L148
	xorl	%eax, %eax
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L145:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	8(%rax), %rax
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	cmpq	$1031, -56(%rbp)
	movq	8(%rax), %rax
	movq	%rax, -48(%rbp)
	jne	.L164
.L149:
	cmpq	$1031, %rax
	jne	.L165
.L152:
	movq	24(%r13), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14NumberToUint32Ev@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	24(%r13), %rax
	leaq	-56(%rbp), %rdi
	movq	(%rax), %rax
	movq	(%rax), %r12
	call	_ZNK2v88internal8compiler4Type3MaxEv@PLT
	movapd	%xmm0, %xmm1
	movq	%r12, %rdi
	pxor	%xmm0, %xmm0
	call	_ZN2v88internal8compiler4Type5RangeEddPNS0_4ZoneE@PLT
	movq	%rax, 8(%rbx)
	movq	%rbx, %rax
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L164:
	leaq	-56(%rbp), %rdi
	movl	$1031, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L151
	movq	-48(%rbp), %rax
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L165:
	leaq	-48(%rbp), %rdi
	movl	$1031, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L151
	jmp	.L152
.L163:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20266:
	.size	_ZN2v88internal8compiler17TypedOptimization17ReduceNumberFloorEPNS1_4NodeE, .-_ZN2v88internal8compiler17TypedOptimization17ReduceNumberFloorEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17TypedOptimization19ReduceNumberRoundopEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17TypedOptimization19ReduceNumberRoundopEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17TypedOptimization19ReduceNumberRoundopEPNS1_4NodeE, @function
_ZN2v88internal8compiler17TypedOptimization19ReduceNumberRoundopEPNS1_4NodeE:
.LFB20267:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	xorl	%esi, %esi
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	56(%rbx), %rdx
	movq	%rax, %r12
	movq	8(%rax), %rax
	movq	336(%rdx), %rsi
	movq	%rax, -32(%rbp)
	cmpq	%rsi, %rax
	je	.L167
	leaq	-32(%rbp), %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	movl	$0, %eax
	cmove	%rax, %r12
.L167:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L171
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L171:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20267:
	.size	_ZN2v88internal8compiler17TypedOptimization19ReduceNumberRoundopEPNS1_4NodeE, .-_ZN2v88internal8compiler17TypedOptimization19ReduceNumberRoundopEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17TypedOptimization22ReduceNumberSilenceNaNEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17TypedOptimization22ReduceNumberSilenceNaNEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17TypedOptimization22ReduceNumberSilenceNaNEPNS1_4NodeE, @function
_ZN2v88internal8compiler17TypedOptimization22ReduceNumberSilenceNaNEPNS1_4NodeE:
.LFB20268:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rax, %r12
	movq	8(%rax), %rax
	movq	%rax, -32(%rbp)
	cmpq	$3167, %rax
	je	.L173
	leaq	-32(%rbp), %rdi
	movl	$3167, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	movl	$0, %eax
	cmove	%rax, %r12
.L173:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L180
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L180:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20268:
	.size	_ZN2v88internal8compiler17TypedOptimization22ReduceNumberSilenceNaNEPNS1_4NodeE, .-_ZN2v88internal8compiler17TypedOptimization22ReduceNumberSilenceNaNEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17TypedOptimization26ReduceNumberToUint8ClampedEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17TypedOptimization26ReduceNumberToUint8ClampedEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17TypedOptimization26ReduceNumberToUint8ClampedEPNS1_4NodeE, @function
_ZN2v88internal8compiler17TypedOptimization26ReduceNumberToUint8ClampedEPNS1_4NodeE:
.LFB20269:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	xorl	%esi, %esi
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	56(%rbx), %rdx
	movq	%rax, %r12
	movq	8(%rax), %rax
	movq	96(%rdx), %rsi
	movq	%rax, -32(%rbp)
	cmpq	%rsi, %rax
	je	.L182
	leaq	-32(%rbp), %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	movl	$0, %eax
	cmove	%rax, %r12
.L182:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L186
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L186:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20269:
	.size	_ZN2v88internal8compiler17TypedOptimization26ReduceNumberToUint8ClampedEPNS1_4NodeE, .-_ZN2v88internal8compiler17TypedOptimization26ReduceNumberToUint8ClampedEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17TypedOptimization9ReducePhiEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17TypedOptimization9ReducePhiEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17TypedOptimization9ReducePhiEPNS1_4NodeE, @function
_ZN2v88internal8compiler17TypedOptimization9ReducePhiEPNS1_4NodeE:
.LFB20270:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	(%rax), %rax
	cmpw	$1, 16(%rax)
	je	.L200
	movq	(%rbx), %rax
	movq	32(%rbx), %rsi
	leaq	32(%rbx), %r14
	movl	20(%rax), %edx
	movl	20(%rbx), %eax
	movl	%eax, %edi
	xorl	$251658240, %edi
	andl	$251658240, %edi
	je	.L201
.L190:
	movq	8(%rsi), %r12
	cmpl	$1, %edx
	jle	.L191
	subl	$2, %edx
	movl	$8, %r15d
	leaq	16(,%rdx,8), %rcx
	movq	%rcx, -72(%rbp)
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L202:
	movq	(%r15,%r14), %rax
	movq	%r12, %rdi
	addq	$8, %r15
	movq	8(%rax), %rsi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, %r12
	cmpq	-72(%rbp), %r15
	je	.L191
.L194:
	movl	20(%rbx), %eax
.L195:
	movq	24(%r13), %rdx
	xorl	$251658240, %eax
	movq	(%rdx), %rdx
	movq	(%rdx), %rdx
	testl	$251658240, %eax
	jne	.L202
	movq	(%r14), %rax
	movq	%r12, %rdi
	movq	16(%rax,%r15), %rax
	addq	$8, %r15
	movq	8(%rax), %rsi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, %r12
	cmpq	%r15, -72(%rbp)
	jne	.L194
.L191:
	movq	8(%rbx), %rax
	movq	%rax, -64(%rbp)
	cmpq	%r12, %rax
	jne	.L203
.L196:
	xorl	%ebx, %ebx
.L197:
	movq	%rbx, %rax
.L189:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L204
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L203:
	.cfi_restore_state
	leaq	-64(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L196
	movq	24(%r13), %rax
	movq	-64(%rbp), %rdi
	movq	%r12, %rsi
	movq	(%rax), %rax
	movq	(%rax), %rdx
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, 8(%rbx)
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L201:
	movq	16(%rsi), %rsi
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L200:
	xorl	%eax, %eax
	jmp	.L189
.L204:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20270:
	.size	_ZN2v88internal8compiler17TypedOptimization9ReducePhiEPNS1_4NodeE, .-_ZN2v88internal8compiler17TypedOptimization9ReducePhiEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17TypedOptimization20ReduceReferenceEqualEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17TypedOptimization20ReduceReferenceEqualEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17TypedOptimization20ReduceReferenceEqualEPNS1_4NodeE, @function
_ZN2v88internal8compiler17TypedOptimization20ReduceReferenceEqualEPNS1_4NodeE:
.LFB20271:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movl	$1, %esi
	movq	%rbx, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	8(%r12), %rdx
	leaq	-56(%rbp), %rdi
	movq	8(%rax), %rsi
	movq	%rdx, -56(%rbp)
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	je	.L212
.L206:
	xorl	%eax, %eax
.L208:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L213
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L212:
	.cfi_restore_state
	movq	24(%r13), %rdi
	call	_ZN2v88internal8compiler7JSGraph13FalseConstantEv@PLT
	movq	8(%rbx), %rsi
	movq	8(%rax), %rax
	movq	%rax, -48(%rbp)
	cmpq	%rax, %rsi
	jne	.L207
.L209:
	movq	24(%r13), %rdi
	call	_ZN2v88internal8compiler7JSGraph13FalseConstantEv@PLT
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L207:
	leaq	-48(%rbp), %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L209
	jmp	.L206
.L213:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20271:
	.size	_ZN2v88internal8compiler17TypedOptimization20ReduceReferenceEqualEPNS1_4NodeE, .-_ZN2v88internal8compiler17TypedOptimization20ReduceReferenceEqualEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17TypedOptimization19NumberComparisonForEPKNS1_8OperatorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17TypedOptimization19NumberComparisonForEPKNS1_8OperatorE
	.type	_ZN2v88internal8compiler17TypedOptimization19NumberComparisonForEPKNS1_8OperatorE, @function
_ZN2v88internal8compiler17TypedOptimization19NumberComparisonForEPKNS1_8OperatorE:
.LFB20272:
	.cfi_startproc
	endbr64
	movzwl	16(%rsi), %eax
	cmpw	$130, %ax
	je	.L215
	cmpw	$131, %ax
	je	.L216
	cmpw	$129, %ax
	je	.L220
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L215:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	movq	24(%rdi), %rax
	movq	376(%rax), %rdi
	jmp	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14NumberLessThanEv@PLT
	.p2align 4,,10
	.p2align 3
.L220:
	movq	24(%rdi), %rax
	movq	376(%rax), %rdi
	jmp	_ZN2v88internal8compiler25SimplifiedOperatorBuilder11NumberEqualEv@PLT
	.p2align 4,,10
	.p2align 3
.L216:
	movq	24(%rdi), %rax
	movq	376(%rax), %rdi
	jmp	_ZN2v88internal8compiler25SimplifiedOperatorBuilder21NumberLessThanOrEqualEv@PLT
	.cfi_endproc
.LFE20272:
	.size	_ZN2v88internal8compiler17TypedOptimization19NumberComparisonForEPKNS1_8OperatorE, .-_ZN2v88internal8compiler17TypedOptimization19NumberComparisonForEPKNS1_8OperatorE
	.section	.text._ZN2v88internal8compiler17TypedOptimization61TryReduceStringComparisonOfStringFromSingleCharCodeToConstantEPNS1_4NodeERKNS1_9StringRefEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17TypedOptimization61TryReduceStringComparisonOfStringFromSingleCharCodeToConstantEPNS1_4NodeERKNS1_9StringRefEb
	.type	_ZN2v88internal8compiler17TypedOptimization61TryReduceStringComparisonOfStringFromSingleCharCodeToConstantEPNS1_4NodeERKNS1_9StringRefEb, @function
_ZN2v88internal8compiler17TypedOptimization61TryReduceStringComparisonOfStringFromSingleCharCodeToConstantEPNS1_4NodeERKNS1_9StringRefEb:
.LFB20273:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rsi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movzwl	16(%rax), %eax
	movq	%rdi, %rbx
	movq	%rdx, %rdi
	cmpw	$129, %ax
	je	.L222
	subw	$130, %ax
	cmpw	$1, %ax
	ja	.L223
	movl	%ecx, %r12d
	call	_ZNK2v88internal8compiler9StringRef6lengthEv@PLT
	testl	%eax, %eax
	jne	.L225
	movq	24(%rbx), %rdi
	testb	%r12b, %r12b
	je	.L227
	call	_ZN2v88internal8compiler7JSGraph12TrueConstantEv@PLT
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L225:
	.cfi_restore_state
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L222:
	.cfi_restore_state
	call	_ZNK2v88internal8compiler9StringRef6lengthEv@PLT
	cmpl	$1, %eax
	je	.L225
	movq	24(%rbx), %rdi
	call	_ZN2v88internal8compiler7JSGraph13FalseConstantEv@PLT
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L227:
	.cfi_restore_state
	call	_ZN2v88internal8compiler7JSGraph13FalseConstantEv@PLT
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L223:
	.cfi_restore_state
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE20273:
	.size	_ZN2v88internal8compiler17TypedOptimization61TryReduceStringComparisonOfStringFromSingleCharCodeToConstantEPNS1_4NodeERKNS1_9StringRefEb, .-_ZN2v88internal8compiler17TypedOptimization61TryReduceStringComparisonOfStringFromSingleCharCodeToConstantEPNS1_4NodeERKNS1_9StringRefEb
	.section	.text._ZN2v88internal8compiler17TypedOptimization51TryReduceStringComparisonOfStringFromSingleCharCodeEPNS1_4NodeES4_NS1_4TypeEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17TypedOptimization51TryReduceStringComparisonOfStringFromSingleCharCodeEPNS1_4NodeES4_NS1_4TypeEb
	.type	_ZN2v88internal8compiler17TypedOptimization51TryReduceStringComparisonOfStringFromSingleCharCodeEPNS1_4NodeES4_NS1_4TypeEb, @function
_ZN2v88internal8compiler17TypedOptimization51TryReduceStringComparisonOfStringFromSingleCharCodeEPNS1_4NodeES4_NS1_4TypeEb:
.LFB20274:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, -136(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	testb	$1, %al
	jne	.L234
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L235
.L234:
	xorl	%eax, %eax
.L236:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L269
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L235:
	.cfi_restore_state
	movq	%rdi, %r14
	leaq	-136(%rbp), %rdi
	movq	%rsi, %r12
	movq	%rdx, %r15
	movl	%r8d, %ebx
	leaq	-112(%rbp), %r13
	call	_ZNK2v88internal8compiler4Type14AsHeapConstantEv@PLT
	movq	%r13, %rdi
	movdqu	8(%rax), %xmm1
	movaps	%xmm1, -112(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef8IsStringEv@PLT
	testb	%al, %al
	je	.L234
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef8AsStringEv@PLT
	movq	%rax, -96(%rbp)
	movq	(%r12), %rax
	movq	%rdx, -88(%rbp)
	movzwl	16(%rax), %eax
	cmpw	$129, %ax
	je	.L237
	subw	$130, %ax
	cmpw	$1, %ax
	ja	.L238
	leaq	-96(%rbp), %r13
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler9StringRef6lengthEv@PLT
	testl	%eax, %eax
	je	.L239
.L247:
	movq	(%r12), %rax
	movzwl	16(%rax), %eax
	cmpw	$130, %ax
	je	.L244
	cmpw	$131, %ax
	jne	.L270
	movq	24(%r14), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder21NumberLessThanOrEqualEv@PLT
	movq	%rax, -152(%rbp)
.L248:
	xorl	%esi, %esi
	movq	%r15, %rdi
	leaq	-80(%rbp), %r15
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	56(%r14), %rdx
	movq	%rax, -144(%rbp)
	movq	8(%rax), %rax
	movq	128(%rdx), %rsi
	movq	%rax, -120(%rbp)
	cmpq	%rax, %rsi
	je	.L249
	leaq	-120(%rbp), %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L249
	movq	24(%r14), %rax
	movq	(%rax), %r11
	movq	376(%rax), %rdi
	movq	%r11, -160(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder13NumberToInt32Ev@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$1, %edx
	movq	-160(%rbp), %r11
	movq	%rax, %rsi
	movq	-144(%rbp), %rax
	movq	%r11, %rdi
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	24(%r14), %rdi
	movsd	.LC3(%rip), %xmm0
	movq	%rax, -160(%rbp)
	movq	(%rdi), %r9
	movq	%r9, -168(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%rax, -144(%rbp)
	movq	24(%r14), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder16NumberBitwiseAndEv@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$2, %edx
	movq	-160(%rbp), %xmm0
	movq	-168(%rbp), %r9
	movq	%rax, %rsi
	movhps	-144(%rbp), %xmm0
	movq	%r9, %rdi
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -144(%rbp)
.L249:
	movq	24(%r14), %r8
	movq	%r13, %rdi
	movq	%r8, -160(%rbp)
	call	_ZN2v88internal8compiler9StringRef12GetFirstCharEv@PLT
	pxor	%xmm0, %xmm0
	movq	-160(%rbp), %r8
	movzwl	%ax, %eax
	cvtsi2sdl	%eax, %xmm0
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%r13, %rdi
	movq	%rax, -160(%rbp)
	testb	%bl, %bl
	je	.L250
	call	_ZNK2v88internal8compiler9StringRef6lengthEv@PLT
	cmpl	$1, %eax
	jle	.L251
	movq	(%r12), %rax
	cmpw	$131, 16(%rax)
	je	.L271
.L251:
	movq	-160(%rbp), %xmm0
	movq	24(%r14), %rax
	movq	(%rax), %rdi
	movhps	-144(%rbp), %xmm0
.L268:
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$2, %edx
	movaps	%xmm0, -80(%rbp)
	movq	-152(%rbp), %rsi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%r14), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rax, %rbx
	movq	%r12, %rsi
	movq	(%rdi), %rax
	movq	%rbx, %rdx
	call	*32(%rax)
	movq	%rbx, %rax
	jmp	.L236
	.p2align 4,,10
	.p2align 3
.L237:
	leaq	-96(%rbp), %r13
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler9StringRef6lengthEv@PLT
	cmpl	$1, %eax
	je	.L247
	movq	24(%r14), %rdi
	call	_ZN2v88internal8compiler7JSGraph13FalseConstantEv@PLT
.L241:
	testq	%rax, %rax
	jne	.L236
	jmp	.L247
	.p2align 4,,10
	.p2align 3
.L239:
	movq	24(%r14), %rdi
	testb	%bl, %bl
	je	.L242
	call	_ZN2v88internal8compiler7JSGraph12TrueConstantEv@PLT
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L270:
	cmpw	$129, %ax
	je	.L272
.L238:
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L242:
	call	_ZN2v88internal8compiler7JSGraph13FalseConstantEv@PLT
	jmp	.L241
.L250:
	call	_ZNK2v88internal8compiler9StringRef6lengthEv@PLT
	cmpl	$1, %eax
	movq	24(%r14), %rax
	jle	.L254
	movq	(%r12), %rdx
	cmpw	$130, 16(%rdx)
	je	.L273
.L254:
	movq	-144(%rbp), %xmm0
	movq	(%rax), %rdi
	movhps	-160(%rbp), %xmm0
	jmp	.L268
	.p2align 4,,10
	.p2align 3
.L244:
	movq	24(%r14), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14NumberLessThanEv@PLT
	movq	%rax, -152(%rbp)
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L272:
	movq	24(%r14), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder11NumberEqualEv@PLT
	movq	%rax, -152(%rbp)
	jmp	.L248
.L273:
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder21NumberLessThanOrEqualEv@PLT
	movq	%rax, -152(%rbp)
	movq	24(%r14), %rax
	jmp	.L254
.L271:
	movq	24(%r14), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14NumberLessThanEv@PLT
	movq	%rax, -152(%rbp)
	jmp	.L251
.L269:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20274:
	.size	_ZN2v88internal8compiler17TypedOptimization51TryReduceStringComparisonOfStringFromSingleCharCodeEPNS1_4NodeES4_NS1_4TypeEb, .-_ZN2v88internal8compiler17TypedOptimization51TryReduceStringComparisonOfStringFromSingleCharCodeEPNS1_4NodeES4_NS1_4TypeEb
	.section	.text._ZN2v88internal8compiler17TypedOptimization22ReduceStringComparisonEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17TypedOptimization22ReduceStringComparisonEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17TypedOptimization22ReduceStringComparisonEPNS1_4NodeE, @function
_ZN2v88internal8compiler17TypedOptimization22ReduceStringComparisonEPNS1_4NodeE:
.LFB20275:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	movq	%r12, %rdi
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	8(%r13), %r9
	movq	8(%rax), %rcx
	movq	%rax, %r14
	movq	0(%r13), %rax
	cmpw	$210, 16(%rax)
	je	.L289
	movq	(%r14), %rdx
	xorl	%eax, %eax
	cmpw	$210, 16(%rdx)
	je	.L290
.L284:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L291
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L289:
	.cfi_restore_state
	movq	(%r14), %rax
	cmpw	$210, 16(%rax)
	je	.L292
	xorl	%r8d, %r8d
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler17TypedOptimization51TryReduceStringComparisonOfStringFromSingleCharCodeEPNS1_4NodeES4_NS1_4TypeEb
	jmp	.L284
	.p2align 4,,10
	.p2align 3
.L290:
	movl	$1, %r8d
	movq	%r9, %rcx
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler17TypedOptimization51TryReduceStringComparisonOfStringFromSingleCharCodeEPNS1_4NodeES4_NS1_4TypeEb
	jmp	.L284
	.p2align 4,,10
	.p2align 3
.L292:
	movq	%r13, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	56(%r15), %rdx
	movq	8(%r13), %rsi
	movq	%rax, %r14
	movq	8(%rax), %rax
	movq	128(%rdx), %r8
	movq	%rsi, -96(%rbp)
	movq	%rax, -88(%rbp)
	cmpq	%rsi, %r8
	je	.L277
	leaq	-96(%rbp), %rdi
	movq	%r8, %rsi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L278
.L288:
	movq	56(%r15), %rdx
	movq	-88(%rbp), %rax
	movq	128(%rdx), %rsi
.L277:
	cmpq	%rax, %rsi
	je	.L279
	leaq	-88(%rbp), %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L293
.L279:
	movq	(%r12), %rax
	movq	24(%r15), %rdx
	movzwl	16(%rax), %eax
	movq	(%rdx), %rbx
	cmpw	$130, %ax
	je	.L280
	cmpw	$131, %ax
	jne	.L294
	movq	376(%rdx), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder21NumberLessThanOrEqualEv@PLT
	movq	%rax, %rsi
.L283:
	movq	%r14, %xmm1
	movq	%r13, %xmm0
	xorl	%r8d, %r8d
	movq	%rbx, %rdi
	punpcklqdq	%xmm1, %xmm0
	leaq	-80(%rbp), %rcx
	movl	$2, %edx
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%r15), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rax, %r13
	movq	%r12, %rsi
	movq	(%rdi), %rax
	movq	%r13, %rdx
	call	*32(%rax)
	movq	%r13, %rax
	jmp	.L284
	.p2align 4,,10
	.p2align 3
.L294:
	cmpw	$129, %ax
	je	.L295
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L293:
	movq	24(%r15), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder13NumberToInt32Ev@PLT
	movq	%r14, -80(%rbp)
	leaq	-80(%rbp), %r14
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	24(%r15), %rdi
	movsd	.LC3(%rip), %xmm0
	movq	%rax, -112(%rbp)
	movq	(%rdi), %rbx
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%rax, -104(%rbp)
	movq	24(%r15), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder16NumberBitwiseAndEv@PLT
	movq	%r14, %rcx
	xorl	%r8d, %r8d
	movl	$2, %edx
	movq	-112(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%rbx, %rdi
	movhps	-104(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r14
	jmp	.L279
	.p2align 4,,10
	.p2align 3
.L278:
	movq	24(%r15), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder13NumberToInt32Ev@PLT
	movq	%r13, -80(%rbp)
	leaq	-80(%rbp), %r13
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	24(%r15), %rdi
	movsd	.LC3(%rip), %xmm0
	movq	%rax, -112(%rbp)
	movq	(%rdi), %rbx
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%rax, -104(%rbp)
	movq	24(%r15), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder16NumberBitwiseAndEv@PLT
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$2, %edx
	movq	-112(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%rbx, %rdi
	movhps	-104(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r13
	jmp	.L288
	.p2align 4,,10
	.p2align 3
.L295:
	movq	376(%rdx), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder11NumberEqualEv@PLT
	movq	%rax, %rsi
	jmp	.L283
	.p2align 4,,10
	.p2align 3
.L280:
	movq	376(%rdx), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14NumberLessThanEv@PLT
	movq	%rax, %rsi
	jmp	.L283
.L291:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20275:
	.size	_ZN2v88internal8compiler17TypedOptimization22ReduceStringComparisonEPNS1_4NodeE, .-_ZN2v88internal8compiler17TypedOptimization22ReduceStringComparisonEPNS1_4NodeE
	.section	.rodata._ZN2v88internal8compiler17TypedOptimization18ReduceStringLengthEPNS1_4NodeE.str1.1,"aMS",@progbits,1
.LC4:
	.string	"IsHeapObject()"
.LC5:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal8compiler17TypedOptimization18ReduceStringLengthEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17TypedOptimization18ReduceStringLengthEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17TypedOptimization18ReduceStringLengthEPNS1_4NodeE, @function
_ZN2v88internal8compiler17TypedOptimization18ReduceStringLengthEPNS1_4NodeE:
.LFB20276:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	xorl	%esi, %esi
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	(%rax), %rcx
	movzwl	16(%rcx), %edx
	cmpw	$30, %dx
	je	.L297
	cmpw	$206, %dx
	jne	.L299
	movq	32(%rax), %rdx
	movzbl	23(%rax), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L314
	movq	%rdx, %rax
	jmp	.L304
	.p2align 4,,10
	.p2align 3
.L297:
	movq	48(%rcx), %r14
	leaq	-80(%rbp), %r12
	movq	32(%rbx), %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	%r14, %rdx
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L301
	movdqa	-80(%rbp), %xmm1
	leaq	-64(%rbp), %r13
	movq	%r13, %rdi
	movaps	%xmm1, -64(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef8IsStringEv@PLT
	testb	%al, %al
	jne	.L315
.L299:
	xorl	%eax, %eax
.L304:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L316
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L314:
	.cfi_restore_state
	movq	16(%rdx), %rdx
	movq	%rdx, %rax
	jmp	.L304
	.p2align 4,,10
	.p2align 3
.L315:
	movq	32(%rbx), %rsi
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	movq	%r14, %rdx
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L301
	movdqa	-64(%rbp), %xmm2
	movq	%r12, %rdi
	movaps	%xmm2, -80(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef8AsStringEv@PLT
	movq	%r13, %rdi
	movq	%rax, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZNK2v88internal8compiler9StringRef6lengthEv@PLT
	pxor	%xmm0, %xmm0
	movq	24(%rbx), %rdi
	movl	%eax, %eax
	cvtsi2sdq	%rax, %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	jmp	.L304
	.p2align 4,,10
	.p2align 3
.L301:
	leaq	.LC4(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L316:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20276:
	.size	_ZN2v88internal8compiler17TypedOptimization18ReduceStringLengthEPNS1_4NodeE, .-_ZN2v88internal8compiler17TypedOptimization18ReduceStringLengthEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17TypedOptimization15ReduceSameValueEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17TypedOptimization15ReduceSameValueEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17TypedOptimization15ReduceSameValueEPNS1_4NodeE, @function
_ZN2v88internal8compiler17TypedOptimization15ReduceSameValueEPNS1_4NodeE:
.LFB20277:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movl	$1, %esi
	movq	%rbx, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	8(%r12), %r9
	movq	%r12, %rdi
	movq	8(%rax), %r10
	movq	%rax, %r8
	movq	%r9, -56(%rbp)
	movq	%r10, -48(%rbp)
	call	_ZN2v88internal8compiler12_GLOBAL__N_123ResolveSameValueRenamesEPNS1_4NodeE
	movq	%r8, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_123ResolveSameValueRenamesEPNS1_4NodeE
	cmpq	%rax, %rsi
	je	.L367
	cmpq	$75457409, %r9
	jne	.L368
	cmpq	$75457409, %r10
	jne	.L369
.L325:
	movq	24(%r13), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14ReferenceEqualEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	%rbx, %rax
.L320:
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L370
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L368:
	.cfi_restore_state
	leaq	-56(%rbp), %rdi
	movl	$75457409, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L326
	movq	-48(%rbp), %r10
	cmpq	$75457409, %r10
	je	.L325
.L369:
	leaq	-48(%rbp), %rdi
	movl	$75457409, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L325
.L326:
	cmpq	$16417, -56(%rbp)
	jne	.L371
.L323:
	cmpq	$16417, -48(%rbp)
	jne	.L372
.L327:
	movq	24(%r13), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder11StringEqualEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	%rbx, %rax
	jmp	.L320
	.p2align 4,,10
	.p2align 3
.L367:
	cmpq	$1, 8(%rbx)
	jne	.L319
.L342:
	xorl	%eax, %eax
	jmp	.L320
	.p2align 4,,10
	.p2align 3
.L371:
	leaq	-56(%rbp), %rdi
	movl	$16417, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L323
.L331:
	cmpq	$2049, -56(%rbp)
	je	.L329
	leaq	-56(%rbp), %r12
	movl	$2049, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L329
	cmpq	$2049, -48(%rbp)
	je	.L332
	leaq	-48(%rbp), %r14
	movl	$2049, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L332
	cmpq	$4097, -56(%rbp)
	je	.L334
	movl	$4097, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L334
	cmpq	$4097, -48(%rbp)
	je	.L336
	movl	$4097, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L336
	cmpq	$1119, -56(%rbp)
	je	.L341
	movl	$1119, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L342
.L341:
	cmpq	$1119, -48(%rbp)
	je	.L339
	movl	$1119, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L342
.L339:
	movq	24(%r13), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder11NumberEqualEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	%rbx, %rax
	jmp	.L320
	.p2align 4,,10
	.p2align 3
.L319:
	movq	24(%r13), %rdi
	call	_ZN2v88internal8compiler7JSGraph12TrueConstantEv@PLT
	jmp	.L320
	.p2align 4,,10
	.p2align 3
.L372:
	leaq	-48(%rbp), %rdi
	movl	$16417, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L331
	jmp	.L327
	.p2align 4,,10
	.p2align 3
.L329:
	xorl	%esi, %esi
.L365:
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler4Node11RemoveInputEi@PLT
	movq	24(%r13), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder17ObjectIsMinusZeroEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	%rbx, %rax
	jmp	.L320
	.p2align 4,,10
	.p2align 3
.L332:
	movl	$1, %esi
	jmp	.L365
.L334:
	xorl	%esi, %esi
.L366:
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler4Node11RemoveInputEi@PLT
	movq	24(%r13), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder11ObjectIsNaNEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	%rbx, %rax
	jmp	.L320
.L336:
	movl	$1, %esi
	jmp	.L366
.L370:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20277:
	.size	_ZN2v88internal8compiler17TypedOptimization15ReduceSameValueEPNS1_4NodeE, .-_ZN2v88internal8compiler17TypedOptimization15ReduceSameValueEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17TypedOptimization12ReduceSelectEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17TypedOptimization12ReduceSelectEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17TypedOptimization12ReduceSelectEPNS1_4NodeE, @function
_ZN2v88internal8compiler17TypedOptimization12ReduceSelectEPNS1_4NodeE:
.LFB20278:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	movq	%r12, %rdi
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	movq	8(%rax), %rax
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	movq	8(%rax), %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	40(%r13), %rsi
	movq	%rax, %r15
	movq	8(%rax), %rax
	movq	%rax, -72(%rbp)
	cmpq	%rsi, -88(%rbp)
	jne	.L401
.L374:
	movq	%rbx, %rax
.L377:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L402
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L401:
	.cfi_restore_state
	leaq	-88(%rbp), %rdi
	movq	%rdi, -104(%rbp)
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L374
	movq	48(%r13), %rsi
	cmpq	%rsi, -88(%rbp)
	jne	.L403
.L375:
	movq	%r15, %rax
	jmp	.L377
	.p2align 4,,10
	.p2align 3
.L403:
	movq	-104(%rbp), %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L375
	movq	40(%r13), %rsi
	cmpq	%rsi, -80(%rbp)
	jne	.L378
.L381:
	movq	48(%r13), %rsi
	cmpq	%rsi, -72(%rbp)
	je	.L379
	leaq	-72(%rbp), %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L379
.L384:
	movq	48(%r13), %rsi
	cmpq	-80(%rbp), %rsi
	je	.L382
	leaq	-80(%rbp), %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L382
.L389:
	movq	24(%r13), %rax
	movq	-72(%rbp), %rsi
	movq	-80(%rbp), %rdi
	movq	(%rax), %rax
	movq	(%rax), %rdx
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, %r14
	movq	8(%r12), %rax
	movq	%rax, -64(%rbp)
	cmpq	%r14, %rax
	je	.L387
	leaq	-64(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L387
	movq	24(%r13), %rax
	movq	-64(%rbp), %rdi
	movq	%r14, %rsi
	movq	(%rax), %rax
	movq	(%rax), %rdx
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, 8(%r12)
	movq	%r12, %rax
	jmp	.L377
	.p2align 4,,10
	.p2align 3
.L378:
	leaq	-80(%rbp), %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L381
	jmp	.L384
	.p2align 4,,10
	.p2align 3
.L379:
	movq	%r14, %rax
	jmp	.L377
	.p2align 4,,10
	.p2align 3
.L382:
	movq	40(%r13), %rsi
	cmpq	-72(%rbp), %rsi
	je	.L385
	leaq	-72(%rbp), %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L389
.L385:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node14TrimInputCountEi@PLT
	movq	24(%r13), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10BooleanNotEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	%r12, %rax
	jmp	.L377
.L387:
	xorl	%eax, %eax
	jmp	.L377
.L402:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20278:
	.size	_ZN2v88internal8compiler17TypedOptimization12ReduceSelectEPNS1_4NodeE, .-_ZN2v88internal8compiler17TypedOptimization12ReduceSelectEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17TypedOptimization25ReduceSpeculativeToNumberEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17TypedOptimization25ReduceSpeculativeToNumberEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17TypedOptimization25ReduceSpeculativeToNumberEPNS1_4NodeE, @function
_ZN2v88internal8compiler17TypedOptimization25ReduceSpeculativeToNumberEPNS1_4NodeE:
.LFB24271:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	xorl	%esi, %esi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rax, %r12
	movq	8(%rax), %rax
	movq	%rax, -48(%rbp)
	cmpq	$7263, %rax
	jne	.L413
.L405:
	movq	8(%rbx), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
.L406:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L414
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L413:
	.cfi_restore_state
	leaq	-48(%rbp), %rdi
	movl	$7263, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L405
	xorl	%r12d, %r12d
	jmp	.L406
.L414:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24271:
	.size	_ZN2v88internal8compiler17TypedOptimization25ReduceSpeculativeToNumberEPNS1_4NodeE, .-_ZN2v88internal8compiler17TypedOptimization25ReduceSpeculativeToNumberEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17TypedOptimization12ReduceTypeOfEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17TypedOptimization12ReduceTypeOfEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17TypedOptimization12ReduceTypeOfEPNS1_4NodeE, @function
_ZN2v88internal8compiler17TypedOptimization12ReduceTypeOfEPNS1_4NodeE:
.LFB20280:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	32(%rsi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L416
	movq	16(%rdx), %rdx
.L416:
	movq	8(%rdx), %rax
	movq	24(%rbx), %r13
	movq	%rax, -72(%rbp)
	movq	360(%r13), %r12
	cmpq	$513, %rax
	jne	.L447
.L417:
	movq	32(%rbx), %rsi
	leaq	-64(%rbp), %r14
	movl	$1, %ecx
	leaq	2168(%r12), %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler9ObjectRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
.L421:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L448
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L447:
	.cfi_restore_state
	leaq	-72(%rbp), %r13
	movl	$513, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L418
	movq	24(%rbx), %r13
	jmp	.L417
	.p2align 4,,10
	.p2align 3
.L418:
	cmpq	$7263, -72(%rbp)
	jne	.L449
.L419:
	movq	24(%rbx), %r14
	leaq	2984(%r12), %rdx
.L446:
	movq	32(%rbx), %rsi
	leaq	-64(%rbp), %r13
	movl	$1, %ecx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler9ObjectRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	jmp	.L421
	.p2align 4,,10
	.p2align 3
.L449:
	movl	$7263, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L419
	cmpq	$16417, -72(%rbp)
	jne	.L450
.L422:
	movq	24(%rbx), %r14
	leaq	3352(%r12), %rdx
	jmp	.L446
	.p2align 4,,10
	.p2align 3
.L450:
	movl	$16417, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L422
	cmpq	$134217729, -72(%rbp)
	je	.L424
	movl	$134217729, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L424
	cmpq	$8193, -72(%rbp)
	je	.L426
	movl	$8193, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L426
	cmpq	$262401, -72(%rbp)
	je	.L428
	movl	$262401, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L428
	cmpq	$68288641, -72(%rbp)
	je	.L430
	movl	$68288641, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L430
	cmpq	$2097153, -72(%rbp)
	je	.L432
	movl	$2097153, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L432
	xorl	%eax, %eax
	jmp	.L421
	.p2align 4,,10
	.p2align 3
.L424:
	movq	24(%rbx), %r14
	leaq	2128(%r12), %rdx
	jmp	.L446
.L426:
	movq	24(%rbx), %r14
	leaq	3384(%r12), %rdx
	jmp	.L446
.L428:
	movq	24(%rbx), %r14
	leaq	3512(%r12), %rdx
	jmp	.L446
.L448:
	call	__stack_chk_fail@PLT
.L430:
	movq	24(%rbx), %r14
	leaq	3008(%r12), %rdx
	jmp	.L446
.L432:
	movq	24(%rbx), %r13
	movq	32(%rbx), %rsi
	movl	$1, %ecx
	leaq	2576(%r12), %rdx
	leaq	-64(%rbp), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler9ObjectRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	jmp	.L421
	.cfi_endproc
.LFE20280:
	.size	_ZN2v88internal8compiler17TypedOptimization12ReduceTypeOfEPNS1_4NodeE, .-_ZN2v88internal8compiler17TypedOptimization12ReduceTypeOfEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17TypedOptimization15ReduceToBooleanEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17TypedOptimization15ReduceToBooleanEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17TypedOptimization15ReduceToBooleanEPNS1_4NodeE, @function
_ZN2v88internal8compiler17TypedOptimization15ReduceToBooleanEPNS1_4NodeE:
.LFB20281:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	32(%rsi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$72, %rsp
	movq	32(%rsi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L452
	movq	16(%r12), %r12
.L452:
	movq	8(%r12), %rax
	movq	%rax, -88(%rbp)
	cmpq	$513, %rax
	jne	.L537
.L453:
	movq	%r12, %rax
.L456:
	movq	-56(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L538
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L537:
	.cfi_restore_state
	leaq	-88(%rbp), %r15
	movl	$513, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L453
	cmpq	$3167, -88(%rbp)
	jne	.L539
.L454:
	movq	24(%r13), %rdi
	movq	(%rdi), %r15
	call	_ZN2v88internal8compiler7JSGraph12ZeroConstantEv@PLT
	movq	%rax, -104(%rbp)
	movq	24(%r13), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder11NumberEqualEv@PLT
	movq	%r12, %xmm0
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movhps	-104(%rbp), %xmm0
	movq	%rax, %rsi
	leaq	-80(%rbp), %rcx
	movl	$2, %edx
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r12
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L459
	movq	32(%rbx), %rdi
	movq	%rbx, %rsi
	cmpq	%rdi, %r12
	je	.L482
.L460:
	leaq	-24(%rsi), %r15
	testq	%rdi, %rdi
	je	.L484
.L527:
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L484:
	movq	%r12, (%r14)
	testq	%r12, %r12
	je	.L482
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L482:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler4Node14TrimInputCountEi@PLT
	movq	24(%r13), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10BooleanNotEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	%rbx, %rax
	jmp	.L456
	.p2align 4,,10
	.p2align 3
.L539:
	movl	$3167, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L454
	cmpq	$7263, -88(%rbp)
	jne	.L540
.L457:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler4Node14TrimInputCountEi@PLT
	movq	24(%r13), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder15NumberToBooleanEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	%rbx, %rax
	jmp	.L456
	.p2align 4,,10
	.p2align 3
.L459:
	movq	32(%rbx), %rsi
	movq	16(%rsi), %rdi
	leaq	16(%rsi), %r14
	cmpq	%rdi, %r12
	jne	.L460
	jmp	.L482
	.p2align 4,,10
	.p2align 3
.L540:
	movl	$7263, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L457
	cmpq	$75169921, -88(%rbp)
	je	.L464
	movl	$75169921, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L464
	cmpq	$75432321, -88(%rbp)
	je	.L466
	movl	$75432321, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L466
	cmpq	$16417, -88(%rbp)
	je	.L473
	movl	$16417, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L473
	xorl	%eax, %eax
	jmp	.L456
	.p2align 4,,10
	.p2align 3
.L464:
	movq	24(%r13), %rdi
	movq	(%rdi), %r15
	call	_ZN2v88internal8compiler7JSGraph12NullConstantEv@PLT
.L534:
	movq	%rax, -104(%rbp)
	movq	24(%r13), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14ReferenceEqualEv@PLT
	movq	%r12, %xmm0
	leaq	-80(%rbp), %rcx
	xorl	%r8d, %r8d
	movhps	-104(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movaps	%xmm0, -80(%rbp)
.L536:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r12
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L480
	movq	32(%rbx), %rdi
	movq	%rbx, %rax
	cmpq	%rdi, %r12
	je	.L482
.L481:
	leaq	-24(%rax), %r15
	testq	%rdi, %rdi
	jne	.L527
	jmp	.L484
.L480:
	movq	32(%rbx), %rax
	movq	16(%rax), %rdi
	leaq	16(%rax), %r14
	cmpq	%rdi, %r12
	jne	.L481
	jmp	.L482
.L466:
	movq	24(%r13), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder20ObjectIsUndetectableEv@PLT
	movq	%r12, -80(%rbp)
	leaq	-80(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movl	$1, %edx
	jmp	.L536
.L473:
	movq	24(%r13), %rdi
	movq	(%rdi), %r15
	call	_ZN2v88internal8compiler7JSGraph19EmptyStringConstantEv@PLT
	jmp	.L534
.L538:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20281:
	.size	_ZN2v88internal8compiler17TypedOptimization15ReduceToBooleanEPNS1_4NodeE, .-_ZN2v88internal8compiler17TypedOptimization15ReduceToBooleanEPNS1_4NodeE
	.section	.rodata._ZN2v88internal8compiler17TypedOptimization21ReduceJSToNumberInputEPNS1_4NodeE.str1.1,"aMS",@progbits,1
.LC6:
	.string	"ReduceJSToNumberInput"
	.section	.text._ZN2v88internal8compiler17TypedOptimization21ReduceJSToNumberInputEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17TypedOptimization21ReduceJSToNumberInputEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17TypedOptimization21ReduceJSToNumberInputEPNS1_4NodeE, @function
_ZN2v88internal8compiler17TypedOptimization21ReduceJSToNumberInputEPNS1_4NodeE:
.LFB20286:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rsi), %rax
	movq	%rax, -104(%rbp)
	cmpq	$16417, %rax
	jne	.L542
.L545:
	movq	(%rbx), %rax
	cmpw	$30, 16(%rax)
	jne	.L546
	movq	48(%rax), %r14
	leaq	-96(%rbp), %r13
	movq	32(%r12), %rsi
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	movq	%r14, %rdx
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L548
	movdqa	-96(%rbp), %xmm1
	leaq	-80(%rbp), %r15
	movq	%r15, %rdi
	movaps	%xmm1, -80(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef8IsStringEv@PLT
	testb	%al, %al
	jne	.L573
	.p2align 4,,10
	.p2align 3
.L546:
	movq	-104(%rbp), %rax
	testb	$1, %al
	jne	.L552
	movl	(%rax), %edx
	testl	%edx, %edx
	je	.L574
.L552:
	cmpq	$7263, %rax
	jne	.L575
.L554:
	movq	%rbx, %rax
.L551:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L576
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L575:
	.cfi_restore_state
	leaq	-104(%rbp), %r13
	movl	$7263, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L554
	cmpq	$257, -104(%rbp)
	jne	.L577
.L555:
	movq	24(%r12), %rdi
	call	_ZN2v88internal8compiler7JSGraph11NaNConstantEv@PLT
	jmp	.L551
	.p2align 4,,10
	.p2align 3
.L542:
	leaq	-104(%rbp), %rdi
	movl	$16417, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L545
	jmp	.L546
	.p2align 4,,10
	.p2align 3
.L574:
	leaq	-104(%rbp), %rdi
	call	_ZNK2v88internal8compiler4Type14AsHeapConstantEv@PLT
	leaq	-80(%rbp), %rdi
	movdqu	8(%rax), %xmm2
	movaps	%xmm2, -80(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef15OddballToNumberEv@PLT
	testb	%al, %al
	je	.L553
	movq	24(%r12), %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	jmp	.L551
	.p2align 4,,10
	.p2align 3
.L577:
	movl	$257, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L555
	cmpq	$129, -104(%rbp)
	je	.L557
	movl	$129, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L557
	xorl	%eax, %eax
	jmp	.L551
	.p2align 4,,10
	.p2align 3
.L573:
	movq	32(%r12), %rsi
	xorl	%ecx, %ecx
	movq	%r15, %rdi
	movq	%r14, %rdx
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L548
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef8AsStringEv@PLT
	movq	%r13, %rdi
	movq	%rax, -96(%rbp)
	movq	%rdx, -88(%rbp)
	call	_ZN2v88internal8compiler9StringRef8ToNumberEv@PLT
	testb	%al, %al
	jne	.L549
	movq	32(%r12), %rdi
	movl	$773, %edx
	leaq	.LC6(%rip), %rsi
	call	_ZN2v88internal8compiler28NoChangeBecauseOfMissingDataEPNS1_12JSHeapBrokerEPKci@PLT
	jmp	.L551
	.p2align 4,,10
	.p2align 3
.L553:
	movq	-104(%rbp), %rax
	cmpq	$7263, %rax
	je	.L554
	jmp	.L575
	.p2align 4,,10
	.p2align 3
.L549:
	movq	24(%r12), %rdi
	movq	%rdx, %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	jmp	.L551
	.p2align 4,,10
	.p2align 3
.L557:
	movq	24(%r12), %rdi
	call	_ZN2v88internal8compiler7JSGraph12ZeroConstantEv@PLT
	jmp	.L551
	.p2align 4,,10
	.p2align 3
.L548:
	leaq	.LC4(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L576:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20286:
	.size	_ZN2v88internal8compiler17TypedOptimization21ReduceJSToNumberInputEPNS1_4NodeE, .-_ZN2v88internal8compiler17TypedOptimization21ReduceJSToNumberInputEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17TypedOptimization26ReduceSpeculativeNumberAddEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17TypedOptimization26ReduceSpeculativeNumberAddEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17TypedOptimization26ReduceSpeculativeNumberAddEPNS1_4NodeE, @function
_ZN2v88internal8compiler17TypedOptimization26ReduceSpeculativeNumberAddEPNS1_4NodeE:
.LFB20285:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -104(%rbp)
	movq	%r12, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	(%r12), %rdi
	movq	8(%r13), %r15
	movq	8(%rax), %r14
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler21NumberOperationHintOfEPKNS1_8OperatorE@PLT
	subl	$3, %eax
	cmpb	$1, %al
	jbe	.L608
.L579:
	xorl	%eax, %eax
.L591:
	movq	-56(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L609
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L608:
	.cfi_restore_state
	movq	%r15, -96(%rbp)
	movq	%r14, %rax
	movq	%r14, -88(%rbp)
	cmpq	$24575, %r15
	jne	.L610
.L580:
	cmpq	$24575, %rax
	je	.L582
	leaq	-88(%rbp), %rdi
	movl	$24575, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L579
.L582:
	movq	%r14, -88(%rbp)
	leaq	-96(%rbp), %r14
	movl	$75448353, %esi
	movq	%r14, %rdi
	movq	%r15, -96(%rbp)
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L579
	leaq	-88(%rbp), %r15
	movl	$75448353, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L579
	movq	-104(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler17TypedOptimization21ReduceJSToNumberInputEPNS1_4NodeE
	leaq	-80(%rbp), %rcx
	movq	%rax, -112(%rbp)
	movq	%rcx, -120(%rbp)
	testq	%rax, %rax
	je	.L611
.L585:
	movq	-104(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler17TypedOptimization21ReduceJSToNumberInputEPNS1_4NodeE
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L612
.L588:
	movq	-104(%rbp), %r15
	movq	24(%r15), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9NumberAddEv@PLT
	movq	%r13, %xmm1
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movq	-112(%rbp), %xmm0
	movq	-120(%rbp), %rcx
	movq	%rax, %rsi
	movl	$2, %edx
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%r15), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rax, %rbx
	movq	%r12, %rsi
	movq	(%rdi), %rax
	movq	%rbx, %rdx
	call	*32(%rax)
	movq	%rbx, %rax
	jmp	.L591
	.p2align 4,,10
	.p2align 3
.L610:
	leaq	-96(%rbp), %rdi
	movl	$24575, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L579
	movq	-88(%rbp), %rax
	jmp	.L580
.L611:
	movq	8(%r13), %rax
	movq	%rax, -88(%rbp)
	cmpq	$7263, %rax
	je	.L606
	movl	$7263, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L606
	movq	-104(%rbp), %rax
	movq	24(%rax), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder22PlainPrimitiveToNumberEv@PLT
	movq	-120(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r13, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -112(%rbp)
	jmp	.L585
.L612:
	movq	8(%rbx), %rax
	movq	%rax, -96(%rbp)
	cmpq	$7263, %rax
	je	.L607
	movl	$7263, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L607
	movq	-104(%rbp), %rax
	movq	24(%rax), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder22PlainPrimitiveToNumberEv@PLT
	movq	-120(%rbp), %rcx
	movq	%r13, %rdi
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%rbx, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r13
	jmp	.L588
.L606:
	movq	%r13, -112(%rbp)
	jmp	.L585
.L607:
	movq	%rbx, %r13
	jmp	.L588
.L609:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20285:
	.size	_ZN2v88internal8compiler17TypedOptimization26ReduceSpeculativeNumberAddEPNS1_4NodeE, .-_ZN2v88internal8compiler17TypedOptimization26ReduceSpeculativeNumberAddEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17TypedOptimization29ConvertPlainPrimitiveToNumberEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17TypedOptimization29ConvertPlainPrimitiveToNumberEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17TypedOptimization29ConvertPlainPrimitiveToNumberEPNS1_4NodeE, @function
_ZN2v88internal8compiler17TypedOptimization29ConvertPlainPrimitiveToNumberEPNS1_4NodeE:
.LFB20288:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler17TypedOptimization21ReduceJSToNumberInputEPNS1_4NodeE
	testq	%rax, %rax
	je	.L619
.L613:
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L620
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L619:
	.cfi_restore_state
	movq	8(%rbx), %rax
	movq	%rax, -40(%rbp)
	cmpq	$7263, %rax
	jne	.L615
.L616:
	movq	%rbx, %rax
	jmp	.L613
	.p2align 4,,10
	.p2align 3
.L615:
	leaq	-40(%rbp), %rdi
	movl	$7263, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L616
	movq	24(%r12), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder22PlainPrimitiveToNumberEv@PLT
	leaq	-32(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%rbx, -32(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	jmp	.L613
.L620:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20288:
	.size	_ZN2v88internal8compiler17TypedOptimization29ConvertPlainPrimitiveToNumberEPNS1_4NodeE, .-_ZN2v88internal8compiler17TypedOptimization29ConvertPlainPrimitiveToNumberEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17TypedOptimization28ReduceSpeculativeNumberBinopEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17TypedOptimization28ReduceSpeculativeNumberBinopEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17TypedOptimization28ReduceSpeculativeNumberBinopEPNS1_4NodeE, @function
_ZN2v88internal8compiler17TypedOptimization28ReduceSpeculativeNumberBinopEPNS1_4NodeE:
.LFB20289:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	movq	%r12, %rdi
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	(%r12), %rdi
	movq	%rax, %r13
	movq	8(%rbx), %rax
	movq	8(%r13), %r15
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal8compiler21NumberOperationHintOfEPKNS1_8OperatorE@PLT
	subl	$3, %eax
	cmpb	$1, %al
	jbe	.L650
.L622:
	xorl	%eax, %eax
.L632:
	movq	-56(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L651
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L650:
	.cfi_restore_state
	movq	-104(%rbp), %rax
	movq	%r15, -88(%rbp)
	movq	%rax, -96(%rbp)
	cmpq	$8159, %rax
	jne	.L652
.L623:
	cmpq	$8159, %r15
	je	.L625
	leaq	-88(%rbp), %rdi
	movl	$8159, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L622
.L625:
	movq	%rbx, %rsi
	movq	%r14, %rdi
	leaq	-80(%rbp), %r15
	call	_ZN2v88internal8compiler17TypedOptimization21ReduceJSToNumberInputEPNS1_4NodeE
	movq	%rax, -104(%rbp)
	testq	%rax, %rax
	je	.L653
.L626:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler17TypedOptimization21ReduceJSToNumberInputEPNS1_4NodeE
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L654
.L629:
	movq	24(%r14), %rax
	movq	(%r12), %rsi
	movq	376(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler12_GLOBAL__N_131NumberOpFromSpeculativeNumberOpEPNS1_25SimplifiedOperatorBuilderEPKNS1_8OperatorE
	movq	%rbx, %xmm1
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	-104(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movq	%r13, %rdi
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%r14), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rax, %rbx
	movq	%r12, %rsi
	movq	(%rdi), %rax
	movq	%rbx, %rdx
	call	*32(%rax)
	movq	%rbx, %rax
	jmp	.L632
	.p2align 4,,10
	.p2align 3
.L652:
	leaq	-96(%rbp), %rdi
	movl	$8159, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L622
	movq	-88(%rbp), %r15
	jmp	.L623
	.p2align 4,,10
	.p2align 3
.L653:
	movq	8(%rbx), %rax
	movq	%rax, -88(%rbp)
	cmpq	$7263, %rax
	je	.L648
	leaq	-88(%rbp), %rdi
	movl	$7263, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L648
	movq	24(%r14), %rax
	movq	(%rax), %r9
	movq	376(%rax), %rdi
	movq	%r9, -104(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder22PlainPrimitiveToNumberEv@PLT
	movq	-104(%rbp), %r9
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%rbx, -80(%rbp)
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -104(%rbp)
	jmp	.L626
	.p2align 4,,10
	.p2align 3
.L654:
	movq	8(%r13), %rax
	movq	%rax, -96(%rbp)
	cmpq	$7263, %rax
	je	.L649
	leaq	-96(%rbp), %rdi
	movl	$7263, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L649
	movq	24(%r14), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder22PlainPrimitiveToNumberEv@PLT
	movq	%rbx, %rdi
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r13, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %rbx
	jmp	.L629
	.p2align 4,,10
	.p2align 3
.L648:
	movq	%rbx, -104(%rbp)
	jmp	.L626
	.p2align 4,,10
	.p2align 3
.L649:
	movq	%r13, %rbx
	jmp	.L629
.L651:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20289:
	.size	_ZN2v88internal8compiler17TypedOptimization28ReduceSpeculativeNumberBinopEPNS1_4NodeE, .-_ZN2v88internal8compiler17TypedOptimization28ReduceSpeculativeNumberBinopEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17TypedOptimization33ReduceSpeculativeNumberComparisonEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17TypedOptimization33ReduceSpeculativeNumberComparisonEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17TypedOptimization33ReduceSpeculativeNumberComparisonEPNS1_4NodeE, @function
_ZN2v88internal8compiler17TypedOptimization33ReduceSpeculativeNumberComparisonEPNS1_4NodeE:
.LFB20290:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	movq	%r12, %rdi
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	8(%rbx), %r15
	movq	8(%rax), %r14
	movq	%rax, -112(%rbp)
	movq	%r15, -96(%rbp)
	movq	%r14, -88(%rbp)
	movq	%r14, %rax
	cmpq	$1099, %r15
	jne	.L692
.L656:
	cmpq	$1099, %rax
	jne	.L693
.L661:
	movq	24(%r13), %rax
	movq	(%r12), %rsi
	movq	%rbx, %xmm0
	movhps	-112(%rbp), %xmm0
	movq	376(%rax), %rdi
	movq	(%rax), %r14
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler12_GLOBAL__N_131NumberOpFromSpeculativeNumberOpEPNS1_25SimplifiedOperatorBuilderEPKNS1_8OperatorE
	movdqa	-112(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movq	%rax, %rsi
	leaq	-80(%rbp), %rcx
	movl	$2, %edx
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%r13), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rax, %rbx
	movq	%r12, %rsi
	movq	(%rdi), %rax
	movq	%rbx, %rdx
	call	*32(%rax)
	movq	%rbx, %rax
.L664:
	movq	-56(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L694
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L693:
	.cfi_restore_state
	leaq	-88(%rbp), %rdi
	movl	$1099, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L661
.L657:
	movq	%r15, -96(%rbp)
	movq	%r14, -88(%rbp)
	cmpq	$1031, %r15
	je	.L658
	leaq	-96(%rbp), %rdi
	movl	$1031, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L662
	movq	-88(%rbp), %r14
.L658:
	cmpq	$1031, %r14
	je	.L661
	leaq	-88(%rbp), %rdi
	movl	$1031, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L661
.L662:
	xorl	%eax, %eax
	jmp	.L664
	.p2align 4,,10
	.p2align 3
.L692:
	leaq	-96(%rbp), %rdi
	movl	$1099, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L657
	movq	-88(%rbp), %rax
	jmp	.L656
.L694:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20290:
	.size	_ZN2v88internal8compiler17TypedOptimization33ReduceSpeculativeNumberComparisonEPNS1_4NodeE, .-_ZN2v88internal8compiler17TypedOptimization33ReduceSpeculativeNumberComparisonEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17TypedOptimization6ReduceEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17TypedOptimization6ReduceEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17TypedOptimization6ReduceEPNS1_4NodeE, @function
_ZN2v88internal8compiler17TypedOptimization6ReduceEPNS1_4NodeE:
.LFB20238:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movzwl	16(%rax), %eax
	subl	$34, %eax
	cmpw	$220, %ax
	ja	.L696
	leaq	.L698(%rip), %rdx
	movzwl	%ax, %eax
	movq	%rdi, %r13
	movq	%rsi, %r12
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler17TypedOptimization6ReduceEPNS1_4NodeE,"a",@progbits
	.align 4
	.align 4
.L698:
	.long	.L722-.L698
	.long	.L721-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L720-.L698
	.long	.L720-.L698
	.long	.L720-.L698
	.long	.L719-.L698
	.long	.L718-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L717-.L698
	.long	.L717-.L698
	.long	.L717-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L716-.L698
	.long	.L715-.L698
	.long	.L715-.L698
	.long	.L715-.L698
	.long	.L715-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L713-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L714-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L713-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L713-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L712-.L698
	.long	.L711-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L710-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L709-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L708-.L698
	.long	.L710-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L706-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L705-.L698
	.long	.L696-.L698
	.long	.L704-.L698
	.long	.L702-.L698
	.long	.L702-.L698
	.long	.L696-.L698
	.long	.L701-.L698
	.long	.L696-.L698
	.long	.L700-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L699-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L696-.L698
	.long	.L697-.L698
	.section	.text._ZN2v88internal8compiler17TypedOptimization6ReduceEPNS1_4NodeE
.L702:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	8(%rax), %r13
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	8(%rax), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	%rax, %rbx
	cmpq	-48(%rbp), %r13
	jne	.L765
	.p2align 4,,10
	.p2align 3
.L736:
	movq	%rbx, %rax
.L723:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L766
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L710:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rax, %rbx
	movq	8(%rax), %rax
	movq	%rax, -48(%rbp)
	cmpq	$7263, %rax
	jne	.L767
.L735:
	movq	8(%r13), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	jmp	.L736
	.p2align 4,,10
	.p2align 3
.L696:
	xorl	%eax, %eax
	jmp	.L723
	.p2align 4,,10
	.p2align 3
.L767:
	leaq	-48(%rbp), %rdi
	movl	$7263, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L735
.L747:
	xorl	%ebx, %ebx
	jmp	.L736
	.p2align 4,,10
	.p2align 3
.L765:
	leaq	-48(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	movl	$0, %eax
	cmove	%rax, %rbx
	jmp	.L736
	.p2align 4,,10
	.p2align 3
.L715:
	call	_ZN2v88internal8compiler17TypedOptimization28ReduceSpeculativeNumberBinopEPNS1_4NodeE
	jmp	.L723
	.p2align 4,,10
	.p2align 3
.L717:
	call	_ZN2v88internal8compiler17TypedOptimization22ReduceStringComparisonEPNS1_4NodeE
	jmp	.L723
	.p2align 4,,10
	.p2align 3
.L720:
	call	_ZN2v88internal8compiler17TypedOptimization33ReduceSpeculativeNumberComparisonEPNS1_4NodeE
	jmp	.L723
	.p2align 4,,10
	.p2align 3
.L713:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	56(%r13), %rdx
	movq	%rax, %rbx
	movq	8(%rax), %rax
	movq	%rax, -48(%rbp)
	movq	336(%rdx), %rsi
	cmpq	%rsi, %rax
	je	.L736
.L762:
	leaq	-48(%rbp), %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	movl	$0, %eax
	cmove	%rax, %rbx
	jmp	.L736
.L706:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rax, %rbx
	movq	8(%rax), %rax
	movq	%rax, -48(%rbp)
	cmpq	$16417, %rax
	je	.L735
	leaq	-48(%rbp), %rdi
	movl	$16417, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L747
	jmp	.L735
.L712:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	56(%r13), %rdx
	movq	%rax, %rbx
	movq	8(%rax), %rax
	movq	%rax, -48(%rbp)
	movq	96(%rdx), %rsi
	cmpq	%rsi, %rax
	je	.L736
	jmp	.L762
.L700:
	call	_ZN2v88internal8compiler17TypedOptimization12ReduceTypeOfEPNS1_4NodeE
	jmp	.L723
.L716:
	call	_ZN2v88internal8compiler17TypedOptimization26ReduceSpeculativeNumberAddEPNS1_4NodeE
	jmp	.L723
.L718:
	call	_ZN2v88internal8compiler17TypedOptimization15ReduceSameValueEPNS1_4NodeE
	jmp	.L723
.L719:
	call	_ZN2v88internal8compiler17TypedOptimization20ReduceReferenceEqualEPNS1_4NodeE
	jmp	.L723
.L721:
	call	_ZN2v88internal8compiler17TypedOptimization9ReducePhiEPNS1_4NodeE
	jmp	.L723
.L722:
	call	_ZN2v88internal8compiler17TypedOptimization12ReduceSelectEPNS1_4NodeE
	jmp	.L723
.L714:
	call	_ZN2v88internal8compiler17TypedOptimization17ReduceNumberFloorEPNS1_4NodeE
	jmp	.L723
.L709:
	call	_ZN2v88internal8compiler17TypedOptimization18ReduceStringLengthEPNS1_4NodeE
	jmp	.L723
.L699:
	call	_ZN2v88internal8compiler17TypedOptimization15ReduceLoadFieldEPNS1_4NodeE
	jmp	.L723
.L708:
	call	_ZN2v88internal8compiler17TypedOptimization15ReduceCheckMapsEPNS1_4NodeE
	jmp	.L723
.L697:
	call	_ZN2v88internal8compiler17TypedOptimization15ReduceToBooleanEPNS1_4NodeE
	jmp	.L723
.L701:
	call	_ZN2v88internal8compiler17TypedOptimization21ReduceConvertReceiverEPNS1_4NodeE
	jmp	.L723
.L711:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rax, %rbx
	movq	8(%rax), %rax
	movq	%rax, -48(%rbp)
	cmpq	$3167, %rax
	je	.L736
	leaq	-48(%rbp), %rdi
	movl	$3167, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	movl	$0, %eax
	cmove	%rax, %rbx
	jmp	.L736
.L704:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	leaq	-48(%rbp), %rdi
	movl	$8388609, %esi
	movq	%rax, %rbx
	movq	8(%rax), %rax
	movq	%rax, -48(%rbp)
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L747
	jmp	.L735
.L705:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rax, %rbx
	movq	8(%rax), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler10BitsetType11SignedSmallEv@PLT
	leaq	-48(%rbp), %rdi
	movl	%eax, %esi
	orl	$1, %esi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L747
	jmp	.L735
.L766:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20238:
	.size	_ZN2v88internal8compiler17TypedOptimization6ReduceEPNS1_4NodeE, .-_ZN2v88internal8compiler17TypedOptimization6ReduceEPNS1_4NodeE
	.section	.text._ZNK2v88internal8compiler17TypedOptimization7factoryEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler17TypedOptimization7factoryEv
	.type	_ZNK2v88internal8compiler17TypedOptimization7factoryEv, @function
_ZNK2v88internal8compiler17TypedOptimization7factoryEv:
.LFB20291:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	movq	360(%rax), %rax
	ret
	.cfi_endproc
.LFE20291:
	.size	_ZNK2v88internal8compiler17TypedOptimization7factoryEv, .-_ZNK2v88internal8compiler17TypedOptimization7factoryEv
	.section	.text._ZNK2v88internal8compiler17TypedOptimization5graphEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler17TypedOptimization5graphEv
	.type	_ZNK2v88internal8compiler17TypedOptimization5graphEv, @function
_ZNK2v88internal8compiler17TypedOptimization5graphEv:
.LFB20292:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	movq	(%rax), %rax
	ret
	.cfi_endproc
.LFE20292:
	.size	_ZNK2v88internal8compiler17TypedOptimization5graphEv, .-_ZNK2v88internal8compiler17TypedOptimization5graphEv
	.section	.text._ZNK2v88internal8compiler17TypedOptimization10simplifiedEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler17TypedOptimization10simplifiedEv
	.type	_ZNK2v88internal8compiler17TypedOptimization10simplifiedEv, @function
_ZNK2v88internal8compiler17TypedOptimization10simplifiedEv:
.LFB20293:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	movq	376(%rax), %rax
	ret
	.cfi_endproc
.LFE20293:
	.size	_ZNK2v88internal8compiler17TypedOptimization10simplifiedEv, .-_ZNK2v88internal8compiler17TypedOptimization10simplifiedEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler17TypedOptimizationC2EPNS1_15AdvancedReducer6EditorEPNS1_23CompilationDependenciesEPNS1_7JSGraphEPNS1_12JSHeapBrokerE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler17TypedOptimizationC2EPNS1_15AdvancedReducer6EditorEPNS1_23CompilationDependenciesEPNS1_7JSGraphEPNS1_12JSHeapBrokerE, @function
_GLOBAL__sub_I__ZN2v88internal8compiler17TypedOptimizationC2EPNS1_15AdvancedReducer6EditorEPNS1_23CompilationDependenciesEPNS1_7JSGraphEPNS1_12JSHeapBrokerE:
.LFB24237:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE24237:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler17TypedOptimizationC2EPNS1_15AdvancedReducer6EditorEPNS1_23CompilationDependenciesEPNS1_7JSGraphEPNS1_12JSHeapBrokerE, .-_GLOBAL__sub_I__ZN2v88internal8compiler17TypedOptimizationC2EPNS1_15AdvancedReducer6EditorEPNS1_23CompilationDependenciesEPNS1_7JSGraphEPNS1_12JSHeapBrokerE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler17TypedOptimizationC2EPNS1_15AdvancedReducer6EditorEPNS1_23CompilationDependenciesEPNS1_7JSGraphEPNS1_12JSHeapBrokerE
	.weak	_ZTVN2v88internal8compiler17TypedOptimizationE
	.section	.data.rel.ro._ZTVN2v88internal8compiler17TypedOptimizationE,"awG",@progbits,_ZTVN2v88internal8compiler17TypedOptimizationE,comdat
	.align 8
	.type	_ZTVN2v88internal8compiler17TypedOptimizationE, @object
	.size	_ZTVN2v88internal8compiler17TypedOptimizationE, 56
_ZTVN2v88internal8compiler17TypedOptimizationE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal8compiler17TypedOptimizationD1Ev
	.quad	_ZN2v88internal8compiler17TypedOptimizationD0Ev
	.quad	_ZNK2v88internal8compiler17TypedOptimization12reducer_nameEv
	.quad	_ZN2v88internal8compiler17TypedOptimization6ReduceEPNS1_4NodeE
	.quad	_ZN2v88internal8compiler7Reducer8FinalizeEv
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC3:
	.long	0
	.long	1089470432
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
