	.file	"js-create-lowering.cc"
	.text
	.section	.rodata._ZNK2v88internal8compiler16JSCreateLowering12reducer_nameEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"JSCreateLowering"
	.section	.text._ZNK2v88internal8compiler16JSCreateLowering12reducer_nameEv,"axG",@progbits,_ZNK2v88internal8compiler16JSCreateLowering12reducer_nameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal8compiler16JSCreateLowering12reducer_nameEv
	.type	_ZNK2v88internal8compiler16JSCreateLowering12reducer_nameEv, @function
_ZNK2v88internal8compiler16JSCreateLowering12reducer_nameEv:
.LFB10170:
	.cfi_startproc
	endbr64
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE10170:
	.size	_ZNK2v88internal8compiler16JSCreateLowering12reducer_nameEv, .-_ZNK2v88internal8compiler16JSCreateLowering12reducer_nameEv
	.section	.text._ZN2v88internal8compiler16JSCreateLoweringD2Ev,"axG",@progbits,_ZN2v88internal8compiler16JSCreateLoweringD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler16JSCreateLoweringD2Ev
	.type	_ZN2v88internal8compiler16JSCreateLoweringD2Ev, @function
_ZN2v88internal8compiler16JSCreateLoweringD2Ev:
.LFB27301:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE27301:
	.size	_ZN2v88internal8compiler16JSCreateLoweringD2Ev, .-_ZN2v88internal8compiler16JSCreateLoweringD2Ev
	.weak	_ZN2v88internal8compiler16JSCreateLoweringD1Ev
	.set	_ZN2v88internal8compiler16JSCreateLoweringD1Ev,_ZN2v88internal8compiler16JSCreateLoweringD2Ev
	.section	.text._ZN2v88internal8compiler16JSCreateLoweringD0Ev,"axG",@progbits,_ZN2v88internal8compiler16JSCreateLoweringD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler16JSCreateLoweringD0Ev
	.type	_ZN2v88internal8compiler16JSCreateLoweringD0Ev, @function
_ZN2v88internal8compiler16JSCreateLoweringD0Ev:
.LFB27303:
	.cfi_startproc
	endbr64
	movl	$48, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE27303:
	.size	_ZN2v88internal8compiler16JSCreateLoweringD0Ev, .-_ZN2v88internal8compiler16JSCreateLoweringD0Ev
	.section	.rodata._ZN2v88internal8compiler15FeedbackCellRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb.str1.1,"aMS",@progbits,1
.LC1:
	.string	"IsFeedbackCell()"
.LC2:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal8compiler15FeedbackCellRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,"axG",@progbits,_ZN2v88internal8compiler15FeedbackCellRefC5EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler15FeedbackCellRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.type	_ZN2v88internal8compiler15FeedbackCellRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, @function
_ZN2v88internal8compiler15FeedbackCellRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb:
.LFB9672:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%ecx, %ebx
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	testb	%bl, %bl
	jne	.L8
.L5:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef14IsFeedbackCellEv@PLT
	testb	%al, %al
	jne	.L5
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE9672:
	.size	_ZN2v88internal8compiler15FeedbackCellRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, .-_ZN2v88internal8compiler15FeedbackCellRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.weak	_ZN2v88internal8compiler15FeedbackCellRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.set	_ZN2v88internal8compiler15FeedbackCellRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,_ZN2v88internal8compiler15FeedbackCellRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.section	.rodata._ZN2v88internal8compiler17FeedbackVectorRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb.str1.1,"aMS",@progbits,1
.LC3:
	.string	"IsFeedbackVector()"
	.section	.text._ZN2v88internal8compiler17FeedbackVectorRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,"axG",@progbits,_ZN2v88internal8compiler17FeedbackVectorRefC5EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler17FeedbackVectorRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.type	_ZN2v88internal8compiler17FeedbackVectorRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, @function
_ZN2v88internal8compiler17FeedbackVectorRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb:
.LFB9678:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%ecx, %ebx
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	testb	%bl, %bl
	jne	.L12
.L9:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef16IsFeedbackVectorEv@PLT
	testb	%al, %al
	jne	.L9
	leaq	.LC3(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE9678:
	.size	_ZN2v88internal8compiler17FeedbackVectorRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, .-_ZN2v88internal8compiler17FeedbackVectorRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.weak	_ZN2v88internal8compiler17FeedbackVectorRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.set	_ZN2v88internal8compiler17FeedbackVectorRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,_ZN2v88internal8compiler17FeedbackVectorRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.section	.rodata._ZN2v88internal8compiler17AllocationSiteRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb.str1.1,"aMS",@progbits,1
.LC4:
	.string	"IsAllocationSite()"
	.section	.text._ZN2v88internal8compiler17AllocationSiteRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,"axG",@progbits,_ZN2v88internal8compiler17AllocationSiteRefC5EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler17AllocationSiteRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.type	_ZN2v88internal8compiler17AllocationSiteRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, @function
_ZN2v88internal8compiler17AllocationSiteRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb:
.LFB9696:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%ecx, %ebx
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	testb	%bl, %bl
	jne	.L16
.L13:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef16IsAllocationSiteEv@PLT
	testb	%al, %al
	jne	.L13
	leaq	.LC4(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE9696:
	.size	_ZN2v88internal8compiler17AllocationSiteRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, .-_ZN2v88internal8compiler17AllocationSiteRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.weak	_ZN2v88internal8compiler17AllocationSiteRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.set	_ZN2v88internal8compiler17AllocationSiteRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,_ZN2v88internal8compiler17AllocationSiteRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.section	.rodata._ZN2v88internal8compiler6MapRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb.str1.1,"aMS",@progbits,1
.LC5:
	.string	"IsMap()"
	.section	.text._ZN2v88internal8compiler6MapRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,"axG",@progbits,_ZN2v88internal8compiler6MapRefC5EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler6MapRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.type	_ZN2v88internal8compiler6MapRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, @function
_ZN2v88internal8compiler6MapRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb:
.LFB9708:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%ecx, %ebx
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	testb	%bl, %bl
	jne	.L20
.L17:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef5IsMapEv@PLT
	testb	%al, %al
	jne	.L17
	leaq	.LC5(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE9708:
	.size	_ZN2v88internal8compiler6MapRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, .-_ZN2v88internal8compiler6MapRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.weak	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.set	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,_ZN2v88internal8compiler6MapRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.section	.rodata._ZN2v88internal8compiler12ScopeInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb.str1.1,"aMS",@progbits,1
.LC6:
	.string	"IsScopeInfo()"
	.section	.text._ZN2v88internal8compiler12ScopeInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,"axG",@progbits,_ZN2v88internal8compiler12ScopeInfoRefC5EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler12ScopeInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.type	_ZN2v88internal8compiler12ScopeInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, @function
_ZN2v88internal8compiler12ScopeInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb:
.LFB9753:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%ecx, %ebx
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	testb	%bl, %bl
	jne	.L24
.L21:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef11IsScopeInfoEv@PLT
	testb	%al, %al
	jne	.L21
	leaq	.LC6(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE9753:
	.size	_ZN2v88internal8compiler12ScopeInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, .-_ZN2v88internal8compiler12ScopeInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.weak	_ZN2v88internal8compiler12ScopeInfoRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.set	_ZN2v88internal8compiler12ScopeInfoRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,_ZN2v88internal8compiler12ScopeInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.section	.rodata._ZN2v88internal8compiler21SharedFunctionInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb.str1.1,"aMS",@progbits,1
.LC7:
	.string	"IsSharedFunctionInfo()"
	.section	.text._ZN2v88internal8compiler21SharedFunctionInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,"axG",@progbits,_ZN2v88internal8compiler21SharedFunctionInfoRefC5EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler21SharedFunctionInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.type	_ZN2v88internal8compiler21SharedFunctionInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, @function
_ZN2v88internal8compiler21SharedFunctionInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb:
.LFB9759:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%ecx, %ebx
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	testb	%bl, %bl
	jne	.L28
.L25:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef20IsSharedFunctionInfoEv@PLT
	testb	%al, %al
	jne	.L25
	leaq	.LC7(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE9759:
	.size	_ZN2v88internal8compiler21SharedFunctionInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, .-_ZN2v88internal8compiler21SharedFunctionInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.weak	_ZN2v88internal8compiler21SharedFunctionInfoRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.set	_ZN2v88internal8compiler21SharedFunctionInfoRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,_ZN2v88internal8compiler21SharedFunctionInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.section	.rodata._ZN2v88internal8compiler7CellRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb.str1.1,"aMS",@progbits,1
.LC8:
	.string	"IsCell()"
	.section	.text._ZN2v88internal8compiler7CellRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,"axG",@progbits,_ZN2v88internal8compiler7CellRefC5EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler7CellRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.type	_ZN2v88internal8compiler7CellRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, @function
_ZN2v88internal8compiler7CellRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb:
.LFB9789:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%ecx, %ebx
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	testb	%bl, %bl
	jne	.L32
.L29:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef6IsCellEv@PLT
	testb	%al, %al
	jne	.L29
	leaq	.LC8(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE9789:
	.size	_ZN2v88internal8compiler7CellRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, .-_ZN2v88internal8compiler7CellRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.weak	_ZN2v88internal8compiler7CellRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.set	_ZN2v88internal8compiler7CellRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,_ZN2v88internal8compiler7CellRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.section	.text._ZN2v88internal8compiler17AllocationBuilder8AllocateEiNS0_14AllocationTypeENS1_4TypeE,"axG",@progbits,_ZN2v88internal8compiler17AllocationBuilder8AllocateEiNS0_14AllocationTypeENS1_4TypeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler17AllocationBuilder8AllocateEiNS0_14AllocationTypeENS1_4TypeE
	.type	_ZN2v88internal8compiler17AllocationBuilder8AllocateEiNS0_14AllocationTypeENS1_4TypeE, @function
_ZN2v88internal8compiler17AllocationBuilder8AllocateEiNS0_14AllocationTypeENS1_4TypeE:
.LFB13394:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%esi, %r12d
	movl	$1, %esi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	16(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%rdx, -88(%rbp)
	movq	8(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler21CommonOperatorBuilder11BeginRegionENS1_19RegionObservabilityE@PLT
	movq	-88(%rbp), %rdx
	leaq	-80(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movq	%r13, %rdi
	movq	%rcx, -112(%rbp)
	movq	%rdx, -80(%rbp)
	movl	$1, %edx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	pxor	%xmm0, %xmm0
	movq	24(%rbx), %r8
	movq	(%rbx), %rdi
	cvtsi2sdl	%r12d, %xmm0
	movq	%rax, 16(%rbx)
	movq	%r8, -104(%rbp)
	movq	(%rdi), %r13
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movl	%r15d, %edx
	movq	%r14, %rsi
	movq	%rax, -96(%rbp)
	movq	(%rbx), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder8AllocateENS1_4TypeENS0_14AllocationTypeE@PLT
	movq	-104(%rbp), %r8
	movq	-112(%rbp), %rcx
	movq	%r13, %rdi
	movq	-96(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$3, %edx
	movq	%r8, -64(%rbp)
	xorl	%r8d, %r8d
	movhps	-88(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 8(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L36
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L36:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13394:
	.size	_ZN2v88internal8compiler17AllocationBuilder8AllocateEiNS0_14AllocationTypeENS1_4TypeE, .-_ZN2v88internal8compiler17AllocationBuilder8AllocateEiNS0_14AllocationTypeENS1_4TypeE
	.section	.text._ZN2v88internal8compiler17AllocationBuilder5StoreERKNS1_11FieldAccessEPNS1_4NodeE,"axG",@progbits,_ZN2v88internal8compiler17AllocationBuilder5StoreERKNS1_11FieldAccessEPNS1_4NodeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler17AllocationBuilder5StoreERKNS1_11FieldAccessEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17AllocationBuilder5StoreERKNS1_11FieldAccessEPNS1_4NodeE, @function
_ZN2v88internal8compiler17AllocationBuilder5StoreERKNS1_11FieldAccessEPNS1_4NodeE:
.LFB13395:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	24(%rdi), %r14
	movq	16(%rdi), %r12
	movq	%rdx, -104(%rbp)
	movq	8(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	movq	%r13, %xmm0
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movhps	-104(%rbp), %xmm0
	movq	%r14, %xmm1
	leaq	-96(%rbp), %rcx
	movq	%rax, %rsi
	movaps	%xmm0, -96(%rbp)
	movq	%r12, %xmm0
	movl	$4, %edx
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 16(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L40
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L40:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13395:
	.size	_ZN2v88internal8compiler17AllocationBuilder5StoreERKNS1_11FieldAccessEPNS1_4NodeE, .-_ZN2v88internal8compiler17AllocationBuilder5StoreERKNS1_11FieldAccessEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17AllocationBuilder15FinishAndChangeEPNS1_4NodeE,"axG",@progbits,_ZN2v88internal8compiler17AllocationBuilder15FinishAndChangeEPNS1_4NodeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler17AllocationBuilder15FinishAndChangeEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17AllocationBuilder15FinishAndChangeEPNS1_4NodeE, @function
_ZN2v88internal8compiler17AllocationBuilder15FinishAndChangeEPNS1_4NodeE:
.LFB13398:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	8(%rsi), %rdx
	movq	8(%rdi), %rax
	movq	%rdx, 8(%rax)
	movzbl	23(%rsi), %eax
	leaq	32(%rsi), %rdx
	movq	8(%rdi), %r15
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L42
	movq	32(%rsi), %rdi
	movq	%rdx, %r14
	cmpq	%rdi, %r15
	je	.L70
.L43:
	leaq	-24(%rsi), %r13
	testq	%rdi, %rdi
	je	.L46
	movq	%r13, %rsi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-56(%rbp), %rdx
.L46:
	movq	%r15, (%r14)
	testq	%r15, %r15
	je	.L47
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movq	-56(%rbp), %rdx
.L47:
	movzbl	23(%r12), %eax
	movq	16(%rbx), %r15
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L48
	movq	8(%rdx), %rdi
	cmpq	%rdi, %r15
	je	.L51
.L71:
	leaq	8(%rdx), %r14
	movq	%r12, %rsi
.L50:
	leaq	-48(%rsi), %r13
	testq	%rdi, %rdi
	je	.L52
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L52:
	movq	%r15, (%r14)
	testq	%r15, %r15
	je	.L51
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L51:
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node14TrimInputCountEi@PLT
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12FinishRegionEv@PLT
	addq	$24, %rsp
	movq	%r12, %rdi
	popq	%rbx
	movq	%rax, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore_state
	movq	32(%r12), %rsi
	leaq	16(%rsi), %r14
.L54:
	movq	8(%r14), %rdi
	cmpq	%r15, %rdi
	je	.L51
	addq	$8, %r14
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L42:
	movq	32(%rsi), %rsi
	movq	16(%rsi), %rdi
	leaq	16(%rsi), %r14
	cmpq	%rdi, %r15
	jne	.L43
	movq	16(%rbx), %r15
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L70:
	movq	16(%rbx), %r15
	movq	8(%rdx), %rdi
	cmpq	%rdi, %r15
	jne	.L71
	jmp	.L51
	.cfi_endproc
.LFE13398:
	.size	_ZN2v88internal8compiler17AllocationBuilder15FinishAndChangeEPNS1_4NodeE, .-_ZN2v88internal8compiler17AllocationBuilder15FinishAndChangeEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17AllocationBuilder13AllocateArrayEiNS1_6MapRefENS0_14AllocationTypeE,"axG",@progbits,_ZN2v88internal8compiler17AllocationBuilder13AllocateArrayEiNS1_6MapRefENS0_14AllocationTypeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler17AllocationBuilder13AllocateArrayEiNS1_6MapRefENS0_14AllocationTypeE
	.type	_ZN2v88internal8compiler17AllocationBuilder13AllocateArrayEiNS1_6MapRefENS0_14AllocationTypeE, @function
_ZN2v88internal8compiler17AllocationBuilder13AllocateArrayEiNS1_6MapRefENS0_14AllocationTypeE:
.LFB21721:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-176(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-96(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r14, %rdi
	subq	$168, %rsp
	movq	%rcx, -168(%rbp)
	movl	%r8d, -208(%rbp)
	movl	%esi, -192(%rbp)
	movq	%rdx, -176(%rbp)
	movq	%fs:40, %rdx
	movq	%rdx, -56(%rbp)
	xorl	%edx, %edx
	call	_ZNK2v88internal8compiler6MapRef13instance_typeEv@PLT
	movq	(%rbx), %rax
	movl	$1, %esi
	movq	16(%rbx), %r15
	movq	8(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler21CommonOperatorBuilder11BeginRegionENS1_19RegionObservabilityE@PLT
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	movq	%r15, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	pxor	%xmm0, %xmm0
	movq	(%rbx), %rdi
	movq	24(%rbx), %r13
	movq	%rax, 16(%rbx)
	movq	%rax, -184(%rbp)
	movl	-192(%rbp), %eax
	movq	(%rdi), %r15
	leal	16(,%rax,8), %eax
	cvtsi2sdl	%eax, %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movl	-208(%rbp), %r9d
	movl	$16777217, %esi
	movq	%rax, -200(%rbp)
	movq	(%rbx), %rax
	movl	%r9d, %edx
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder8AllocateENS1_4TypeENS0_14AllocationTypeE@PLT
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movl	$3, %edx
	movq	-200(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movq	%r13, -80(%rbp)
	leaq	-160(%rbp), %r13
	movhps	-184(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r13, %rdi
	movq	%rax, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 8(%rbx)
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	24(%rbx), %rdx
	movq	16(%rbx), %rcx
	movq	%r13, %rsi
	movq	%rax, -184(%rbp)
	movq	(%rbx), %rax
	movq	8(%rbx), %r14
	movq	%rdx, -200(%rbp)
	movq	376(%rax), %rdi
	movq	(%rax), %r15
	movq	%rcx, -208(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	movq	%r14, %xmm0
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movhps	-184(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$4, %edx
	movq	%r15, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	-208(%rbp), %xmm0
	movhps	-200(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	pxor	%xmm0, %xmm0
	movq	(%rbx), %rdi
	cvtsi2sdl	-192(%rbp), %xmm0
	movq	%rax, 16(%rbx)
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%r13, %rdi
	movq	%rax, -184(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder19ForFixedArrayLengthEv@PLT
	movq	(%rbx), %rax
	movq	24(%rbx), %rdx
	movq	%r13, %rsi
	movq	8(%rbx), %rcx
	movq	16(%rbx), %r14
	movq	376(%rax), %rdi
	movq	(%rax), %r15
	movq	%rdx, -192(%rbp)
	movq	%rcx, -200(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movl	$4, %edx
	movq	-200(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movhps	-184(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	%r14, %xmm0
	movhps	-192(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 16(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L75
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L75:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21721:
	.size	_ZN2v88internal8compiler17AllocationBuilder13AllocateArrayEiNS1_6MapRefENS0_14AllocationTypeE, .-_ZN2v88internal8compiler17AllocationBuilder13AllocateArrayEiNS1_6MapRefENS0_14AllocationTypeE
	.section	.rodata._ZN2v88internal8compiler16JSCreateLowering14ReduceJSCreateEPNS1_4NodeE.str1.1,"aMS",@progbits,1
.LC9:
	.string	"IsHeapObject()"
	.section	.text._ZN2v88internal8compiler16JSCreateLowering14ReduceJSCreateEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16JSCreateLowering14ReduceJSCreateEPNS1_4NodeE
	.type	_ZN2v88internal8compiler16JSCreateLowering14ReduceJSCreateEPNS1_4NodeE, @function
_ZN2v88internal8compiler16JSCreateLowering14ReduceJSCreateEPNS1_4NodeE:
.LFB22936:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movl	$1, %esi
	movq	%rbx, %rdi
	subq	$232, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	32(%r14), %rsi
	leaq	-192(%rbp), %rdi
	movq	%rbx, %rdx
	movq	%rax, -248(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties14GetJSCreateMapEPNS1_12JSHeapBrokerEPNS1_4NodeE@PLT
	xorl	%eax, %eax
	cmpb	$0, -192(%rbp)
	je	.L78
	movq	(%r12), %rax
	xorl	%edx, %edx
	cmpw	$30, 16(%rax)
	jne	.L79
	movq	48(%rax), %rdx
.L79:
	leaq	-160(%rbp), %r13
	movq	32(%r14), %rsi
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L107
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12AsJSFunctionEv@PLT
	movq	16(%r14), %rdi
	leaq	-208(%rbp), %rsi
	movq	%rdx, -200(%rbp)
	movq	%rax, -208(%rbp)
	call	_ZN2v88internal8compiler23CompilationDependencies40DependOnInitialMapInstanceSizePredictionERKNS1_13JSFunctionRefE@PLT
	movq	24(%r14), %r12
	movl	$1, %esi
	movq	%rax, %r10
	movq	%rax, -224(%rbp)
	movq	(%r12), %r11
	movq	8(%r12), %rdi
	sarq	$32, %r10
	movl	%r10d, -260(%rbp)
	movq	%r10, -240(%rbp)
	movq	%r11, -216(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder11BeginRegionENS1_19RegionObservabilityE@PLT
	movq	-216(%rbp), %r11
	xorl	%r8d, %r8d
	movq	%r15, -96(%rbp)
	movq	%rax, %rsi
	leaq	-96(%rbp), %rax
	movl	$1, %edx
	movq	%r11, %rdi
	movq	%rax, %rcx
	movq	%rax, -256(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movq	(%r12), %r15
	movq	-224(%rbp), %r9
	movq	%rax, -216(%rbp)
	cvtsi2sdl	%r9d, %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	376(%r12), %rdi
	xorl	%edx, %edx
	movl	$4294967295, %esi
	movq	%rax, -224(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder8AllocateENS1_4TypeENS0_14AllocationTypeE@PLT
	movq	-248(%rbp), %rdx
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movq	-224(%rbp), %xmm0
	movq	-256(%rbp), %rcx
	movq	%rax, %rsi
	movq	%rdx, -80(%rbp)
	movl	$3, %edx
	movhps	-216(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r13, %rdi
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	leaq	-184(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rdx, %rsi
	movq	%rdx, -272(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	376(%r12), %rdi
	movq	%r13, %rsi
	movq	(%r12), %r15
	movq	%rax, -224(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movl	$4, %edx
	movq	%r15, %rdi
	movq	-216(%rbp), %xmm1
	movq	-256(%rbp), %rcx
	movq	%rax, %rsi
	movdqa	%xmm1, %xmm0
	movhps	-224(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movdqa	%xmm1, %xmm0
	movhps	-248(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	24(%r14), %rdi
	movq	%rax, -232(%rbp)
	call	_ZN2v88internal8compiler7JSGraph23EmptyFixedArrayConstantEv@PLT
	movq	%r13, %rdi
	movq	%rax, -224(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder39ForJSObjectPropertiesOrHashKnownPointerEv@PLT
	movq	376(%r12), %rdi
	movq	%r13, %rsi
	movq	(%r12), %r15
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movl	$4, %edx
	movq	%r15, %rdi
	movq	-216(%rbp), %xmm0
	movq	-256(%rbp), %rcx
	movq	%rax, %rsi
	movhps	-224(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-232(%rbp), %xmm0
	movhps	-248(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	24(%r14), %rdi
	movq	%rax, -232(%rbp)
	call	_ZN2v88internal8compiler7JSGraph23EmptyFixedArrayConstantEv@PLT
	movq	%r13, %rdi
	movq	%rax, -224(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder19ForJSObjectElementsEv@PLT
	movq	376(%r12), %rdi
	movq	%r13, %rsi
	movq	(%r12), %r15
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movl	$4, %edx
	movq	%r15, %rdi
	movq	-216(%rbp), %xmm0
	movq	-256(%rbp), %rcx
	movq	%rax, %rsi
	movhps	-224(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-232(%rbp), %xmm0
	movhps	-248(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-240(%rbp), %r10
	movq	%rax, %r9
	testl	%r10d, %r10d
	jle	.L81
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L82:
	movq	24(%r14), %rdi
	movq	%r9, -240(%rbp)
	call	_ZN2v88internal8compiler7JSGraph17UndefinedConstantEv@PLT
	movq	-272(%rbp), %rsi
	movl	%r15d, %edx
	movq	%r13, %rdi
	movq	%rax, -224(%rbp)
	addl	$1, %r15d
	call	_ZN2v88internal8compiler13AccessBuilder27ForJSObjectInObjectPropertyERKNS1_6MapRefEi@PLT
	movq	(%r12), %r11
	movq	376(%r12), %rdi
	movq	%r13, %rsi
	movq	%r11, -232(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	movq	-240(%rbp), %r9
	xorl	%r8d, %r8d
	movl	$4, %edx
	movq	-216(%rbp), %xmm0
	movq	-232(%rbp), %r11
	movq	%rax, %rsi
	movq	-256(%rbp), %rcx
	movhps	-224(%rbp), %xmm0
	movq	%r11, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	%r9, %xmm0
	movhps	-248(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r9
	cmpl	-260(%rbp), %r15d
	jne	.L82
.L81:
	movq	8(%r14), %rdi
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movq	%rbx, %rdx
	movq	%r9, -224(%rbp)
	movq	%rbx, %rsi
	leaq	32(%rbx), %r15
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	8(%rbx), %rax
	movq	-216(%rbp), %rcx
	movq	-224(%rbp), %r9
	movq	%rax, 8(%rcx)
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L83
	movq	32(%rbx), %rdi
	movq	%r15, %r14
	movq	%rbx, %rsi
	cmpq	%rdi, %rcx
	je	.L85
.L84:
	leaq	-24(%rsi), %r13
	testq	%rdi, %rdi
	je	.L87
	movq	%r13, %rsi
	movq	%r9, -224(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-224(%rbp), %r9
.L87:
	movq	-216(%rbp), %rax
	movq	%r13, %rsi
	movq	%r9, -224(%rbp)
	movq	%rax, (%r14)
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movzbl	23(%rbx), %eax
	movq	-224(%rbp), %r9
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L108
.L85:
	movq	8(%r15), %rdi
	cmpq	%rdi, %r9
	je	.L90
	addq	$8, %r15
	movq	%rbx, %rsi
.L89:
	leaq	-48(%rsi), %r13
	testq	%rdi, %rdi
	je	.L91
	movq	%r13, %rsi
	movq	%r9, -216(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-216(%rbp), %r9
.L91:
	movq	%r9, (%r15)
	testq	%r9, %r9
	je	.L90
	movq	%r13, %rsi
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L90:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler4Node14TrimInputCountEi@PLT
	movq	8(%r12), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12FinishRegionEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	%rbx, %rax
.L78:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L109
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L107:
	.cfi_restore_state
	leaq	.LC9(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L83:
	movq	32(%rbx), %rsi
	movq	16(%rsi), %rdi
	leaq	16(%rsi), %r14
	cmpq	%rdi, -216(%rbp)
	jne	.L84
.L86:
	movq	8(%r14), %rdi
	cmpq	%rdi, %r9
	je	.L90
	leaq	8(%r14), %r15
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L108:
	movq	32(%rbx), %rsi
	leaq	16(%rsi), %r14
	jmp	.L86
.L109:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22936:
	.size	_ZN2v88internal8compiler16JSCreateLowering14ReduceJSCreateEPNS1_4NodeE, .-_ZN2v88internal8compiler16JSCreateLowering14ReduceJSCreateEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler16JSCreateLowering29ReduceJSCreateGeneratorObjectEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16JSCreateLowering29ReduceJSCreateGeneratorObjectEPNS1_4NodeE
	.type	_ZN2v88internal8compiler16JSCreateLowering29ReduceJSCreateGeneratorObjectEPNS1_4NodeE, @function
_ZN2v88internal8compiler16JSCreateLowering29ReduceJSCreateGeneratorObjectEPNS1_4NodeE:
.LFB22939:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	xorl	%esi, %esi
	subq	$344, %rsp
	movq	%rdi, -272(%rbp)
	movq	%rbx, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movl	$1, %esi
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rbx, %rdi
	movq	%rax, -328(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetContextInputEPNS1_4NodeE@PLT
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	%rax, -336(%rbp)
	movq	8(%r15), %rax
	movq	%rax, -248(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, -264(%rbp)
	movq	-248(%rbp), %rax
	testb	$1, %al
	jne	.L111
	movl	(%rax), %edx
	testl	%edx, %edx
	je	.L144
.L111:
	xorl	%eax, %eax
.L129:
	movq	-56(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L145
	addq	$344, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L144:
	.cfi_restore_state
	leaq	-248(%rbp), %rdi
	leaq	-240(%rbp), %r12
	call	_ZNK2v88internal8compiler4Type14AsHeapConstantEv@PLT
	leaq	8(%rax), %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12AsJSFunctionEv@PLT
	movq	%r12, %rdi
	movq	%rax, -240(%rbp)
	movq	%rdx, -232(%rbp)
	call	_ZNK2v88internal8compiler13JSFunctionRef15has_initial_mapEv@PLT
	testb	%al, %al
	je	.L111
	movq	-272(%rbp), %rcx
	movq	%r12, %rsi
	movq	16(%rcx), %rdi
	call	_ZN2v88internal8compiler23CompilationDependencies40DependOnInitialMapInstanceSizePredictionERKNS1_13JSFunctionRefE@PLT
	movq	%r12, %rdi
	movq	%rax, -360(%rbp)
	sarq	$32, %rax
	movq	%rax, -368(%rbp)
	movl	%eax, -340(%rbp)
	call	_ZNK2v88internal8compiler13JSFunctionRef11initial_mapEv@PLT
	movq	%r12, %rdi
	leaq	-208(%rbp), %r12
	movq	%rax, -224(%rbp)
	movq	%rdx, -216(%rbp)
	call	_ZNK2v88internal8compiler13JSFunctionRef6sharedEv@PLT
	movq	%r12, %rdi
	movq	%rdx, -200(%rbp)
	movq	%rax, -208(%rbp)
	call	_ZNK2v88internal8compiler21SharedFunctionInfoRef31internal_formal_parameter_countEv@PLT
	movq	%r12, %rdi
	leaq	-160(%rbp), %r12
	movl	%eax, %r14d
	call	_ZNK2v88internal8compiler21SharedFunctionInfoRef16GetBytecodeArrayEv@PLT
	movq	%r12, %rdi
	movq	%rdx, -152(%rbp)
	movq	%rax, -160(%rbp)
	call	_ZNK2v88internal8compiler16BytecodeArrayRef14register_countEv@PLT
	movq	-272(%rbp), %rcx
	movq	%r12, %rdi
	movq	-264(%rbp), %rdx
	addl	%eax, %r14d
	movq	%r13, -176(%rbp)
	movq	24(%rcx), %rax
	movq	32(%rcx), %rsi
	movq	%rdx, -168(%rbp)
	movl	$1, %ecx
	movq	$0, -184(%rbp)
	movq	360(%rax), %rdx
	movq	%rax, -192(%rbp)
	addq	$152, %rdx
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	-160(%rbp), %rdx
	xorl	%r8d, %r8d
	movq	-152(%rbp), %rcx
	leaq	-192(%rbp), %rdi
	movl	%r14d, %esi
	call	_ZN2v88internal8compiler17AllocationBuilder13AllocateArrayEiNS1_6MapRefENS0_14AllocationTypeE
	testl	%r14d, %r14d
	jle	.L114
	leal	-1(%r14), %eax
	xorl	%r13d, %r13d
	leaq	-96(%rbp), %r14
	movq	%r15, -376(%rbp)
	movq	%rax, -352(%rbp)
	movq	%rbx, -384(%rbp)
	movq	%r13, %rbx
	movq	-272(%rbp), %r13
	.p2align 4,,10
	.p2align 3
.L116:
	movq	24(%r13), %rdi
	call	_ZN2v88internal8compiler7JSGraph17UndefinedConstantEv@PLT
	movl	$5, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder17ForFixedArraySlotEmNS1_16WriteBarrierKindE@PLT
	movq	-192(%rbp), %rax
	movq	-168(%rbp), %rcx
	movq	%r12, %rsi
	movq	-184(%rbp), %xmm0
	movq	%rcx, -304(%rbp)
	movq	376(%rax), %rdi
	movq	-176(%rbp), %rcx
	movq	(%rax), %r15
	movq	%xmm0, -288(%rbp)
	movq	%rcx, -320(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	movl	$4, %edx
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	-288(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movhps	-280(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-320(%rbp), %xmm0
	movhps	-304(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rbx, %rdx
	addq	$1, %rbx
	movq	%rax, -176(%rbp)
	cmpq	%rdx, -352(%rbp)
	jne	.L116
	movq	-376(%rbp), %r15
	movq	-384(%rbp), %rbx
	movq	%rax, %rdx
.L115:
	movq	-192(%rbp), %rax
	movq	-184(%rbp), %rcx
	movq	%rdx, -304(%rbp)
	movq	8(%rax), %rdi
	movq	(%rax), %r13
	movq	%rcx, -280(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12FinishRegionEv@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	%r13, %rdi
	movq	-304(%rbp), %rdx
	movq	%rax, %rsi
	movq	-280(%rbp), %xmm0
	movq	%rdx, %xmm4
	movl	$2, %edx
	punpcklqdq	%xmm4, %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$1, %esi
	movq	%rax, -320(%rbp)
	movq	-272(%rbp), %rax
	movq	24(%rax), %r13
	movq	0(%r13), %r9
	movq	8(%r13), %rdi
	movq	%r9, -280(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder11BeginRegionENS1_19RegionObservabilityE@PLT
	movq	-320(%rbp), %rdx
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	-280(%rbp), %r9
	movq	%rax, %rsi
	movq	%rdx, -96(%rbp)
	movl	$1, %edx
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	pxor	%xmm0, %xmm0
	movq	0(%r13), %r9
	movq	%r13, %rdi
	cvtsi2sdl	-360(%rbp), %xmm0
	movq	%rax, -280(%rbp)
	movq	%r9, -288(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	376(%r13), %rdi
	xorl	%edx, %edx
	movl	$4294967295, %esi
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder8AllocateENS1_4TypeENS0_14AllocationTypeE@PLT
	movq	-288(%rbp), %r9
	xorl	%r8d, %r8d
	movq	-264(%rbp), %rcx
	movq	-304(%rbp), %xmm0
	movl	$3, %edx
	movq	%rax, %rsi
	movq	%r9, %rdi
	movq	%rcx, -80(%rbp)
	movq	%r14, %rcx
	movhps	-280(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -280(%rbp)
	movq	-272(%rbp), %rax
	movq	24(%rax), %rdi
	call	_ZN2v88internal8compiler7JSGraph17UndefinedConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, -352(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	leaq	-224(%rbp), %rcx
	movq	%r13, %rdi
	movq	%rcx, %rsi
	movq	%rcx, -288(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	0(%r13), %r9
	movq	376(%r13), %rdi
	movq	%r12, %rsi
	movq	%rax, -304(%rbp)
	movq	%r9, -360(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$4, %edx
	movq	-280(%rbp), %xmm2
	movq	-360(%rbp), %r9
	movq	%rax, %rsi
	movdqa	%xmm2, %xmm0
	movq	%r9, %rdi
	movhps	-304(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movdqa	%xmm2, %xmm0
	movhps	-264(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -360(%rbp)
	movq	-272(%rbp), %rax
	movq	24(%rax), %rdi
	call	_ZN2v88internal8compiler7JSGraph23EmptyFixedArrayConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder39ForJSObjectPropertiesOrHashKnownPointerEv@PLT
	movq	0(%r13), %r9
	movq	376(%r13), %rdi
	movq	%r12, %rsi
	movq	%r9, -376(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$4, %edx
	movq	-280(%rbp), %xmm0
	movq	-376(%rbp), %r9
	movq	%rax, %rsi
	movhps	-304(%rbp), %xmm0
	movq	%r9, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	-360(%rbp), %xmm0
	movhps	-264(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -360(%rbp)
	movq	-272(%rbp), %rax
	movq	24(%rax), %rdi
	call	_ZN2v88internal8compiler7JSGraph23EmptyFixedArrayConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder19ForJSObjectElementsEv@PLT
	movq	0(%r13), %r9
	movq	376(%r13), %rdi
	movq	%r12, %rsi
	movq	%r9, -376(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$4, %edx
	movq	-280(%rbp), %xmm0
	movq	-376(%rbp), %r9
	movq	%rax, %rsi
	movhps	-304(%rbp), %xmm0
	movq	%r9, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	-360(%rbp), %xmm0
	movhps	-264(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r12, %rdi
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder27ForJSGeneratorObjectContextEv@PLT
	movq	0(%r13), %r9
	movq	376(%r13), %rdi
	movq	%r12, %rsi
	movq	%r9, -360(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$4, %edx
	movq	-280(%rbp), %xmm0
	movq	-360(%rbp), %r9
	movq	%rax, %rsi
	movhps	-336(%rbp), %xmm0
	movq	%r9, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	-304(%rbp), %xmm0
	movhps	-264(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r12, %rdi
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder28ForJSGeneratorObjectFunctionEv@PLT
	movq	0(%r13), %r9
	movq	376(%r13), %rdi
	movq	%r12, %rsi
	movq	%r9, -336(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	movq	%r15, %xmm5
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	-336(%rbp), %r9
	movl	$4, %edx
	movq	%rax, %rsi
	movq	-280(%rbp), %xmm0
	punpcklqdq	%xmm5, %xmm0
	movq	%r9, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	-304(%rbp), %xmm0
	movhps	-264(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r12, %rdi
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder28ForJSGeneratorObjectReceiverEv@PLT
	movq	376(%r13), %rdi
	movq	%r12, %rsi
	movq	0(%r13), %r15
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$4, %edx
	movq	-280(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movhps	-328(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-304(%rbp), %xmm0
	movhps	-264(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r12, %rdi
	movq	%rax, -328(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder35ForJSGeneratorObjectInputOrDebugPosEv@PLT
	movq	376(%r13), %rdi
	movq	%r12, %rsi
	movq	0(%r13), %r15
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$4, %edx
	movq	-280(%rbp), %xmm3
	movq	%rax, %rsi
	movq	%r15, %rdi
	movq	-328(%rbp), %xmm0
	movhps	-352(%rbp), %xmm3
	movhps	-264(%rbp), %xmm0
	movaps	%xmm3, -304(%rbp)
	movaps	%xmm3, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	pxor	%xmm0, %xmm0
	movq	%rax, -336(%rbp)
	movq	-272(%rbp), %rax
	movq	24(%rax), %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%r12, %rdi
	movq	%rax, -328(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder30ForJSGeneratorObjectResumeModeEv@PLT
	movq	376(%r13), %rdi
	movq	%r12, %rsi
	movq	0(%r13), %r15
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$4, %edx
	movq	-280(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movhps	-328(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-336(%rbp), %xmm0
	movhps	-264(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movsd	.LC11(%rip), %xmm0
	movq	%rax, -336(%rbp)
	movq	-272(%rbp), %rax
	movq	24(%rax), %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%r12, %rdi
	movq	%rax, -328(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder32ForJSGeneratorObjectContinuationEv@PLT
	movq	376(%r13), %rdi
	movq	%r12, %rsi
	movq	0(%r13), %r15
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$4, %edx
	movq	-280(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movhps	-328(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-336(%rbp), %xmm0
	movhps	-264(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r12, %rdi
	movq	%rax, -328(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder42ForJSGeneratorObjectParametersAndRegistersEv@PLT
	movq	376(%r13), %rdi
	movq	%r12, %rsi
	movq	0(%r13), %r15
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	movq	%r15, %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	-280(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$4, %edx
	movhps	-320(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-328(%rbp), %xmm0
	movhps	-264(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-288(%rbp), %rdi
	movq	%rax, %r15
	call	_ZNK2v88internal8compiler6MapRef13instance_typeEv@PLT
	cmpw	$1064, %ax
	je	.L146
.L117:
	movl	-368(%rbp), %eax
	testl	%eax, %eax
	jle	.L118
	movq	%r14, -320(%rbp)
	xorl	%r9d, %r9d
	movq	%rbx, -328(%rbp)
	movq	-288(%rbp), %rbx
	movq	%r15, -272(%rbp)
	movl	%r9d, %r15d
	.p2align 4,,10
	.p2align 3
.L119:
	movl	%r15d, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	addl	$1, %r15d
	call	_ZN2v88internal8compiler13AccessBuilder27ForJSObjectInObjectPropertyERKNS1_6MapRefEi@PLT
	movq	376(%r13), %rdi
	movq	%r12, %rsi
	movq	0(%r13), %r14
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movl	$4, %edx
	movq	%r14, %rdi
	movq	-272(%rbp), %xmm0
	movdqa	-304(%rbp), %xmm1
	movq	%rax, %rsi
	movq	-320(%rbp), %rcx
	movhps	-264(%rbp), %xmm0
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -272(%rbp)
	cmpl	-340(%rbp), %r15d
	jne	.L119
	movq	-328(%rbp), %rbx
	movq	%rax, %r15
.L118:
	movq	8(%rbx), %rax
	movq	-280(%rbp), %rdx
	movq	%rax, 8(%rdx)
	movzbl	23(%rbx), %eax
	leaq	32(%rbx), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L120
	movq	32(%rbx), %rdi
	movq	%rdx, %r14
	movq	%rbx, %rsi
	cmpq	%rdi, -280(%rbp)
	je	.L122
.L121:
	leaq	-24(%rsi), %r12
	testq	%rdi, %rdi
	je	.L124
	movq	%r12, %rsi
	movq	%rdx, -264(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-264(%rbp), %rdx
.L124:
	movq	-280(%rbp), %rax
	movq	%r12, %rsi
	movq	%rdx, -264(%rbp)
	movq	%rax, (%r14)
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movzbl	23(%rbx), %eax
	movq	-264(%rbp), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L147
.L122:
	movq	8(%rdx), %rdi
	cmpq	%rdi, %r15
	je	.L127
	leaq	8(%rdx), %r14
	movq	%rbx, %rsi
.L126:
	leaq	-48(%rsi), %r12
	testq	%rdi, %rdi
	je	.L128
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L128:
	movq	%r15, (%r14)
	testq	%r15, %r15
	je	.L127
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L127:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler4Node14TrimInputCountEi@PLT
	movq	8(%r13), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12FinishRegionEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	%rbx, %rax
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L120:
	movq	32(%rbx), %rsi
	movq	16(%rsi), %rdi
	leaq	16(%rsi), %r14
	cmpq	-280(%rbp), %rdi
	jne	.L121
.L123:
	movq	8(%r14), %rdi
	cmpq	%rdi, %r15
	je	.L127
	addq	$8, %r14
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L147:
	movq	32(%rbx), %rsi
	leaq	16(%rsi), %r14
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L146:
	movq	%r15, %xmm0
	movq	%r12, %rdi
	movhps	-264(%rbp), %xmm0
	movaps	%xmm0, -320(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder30ForJSAsyncGeneratorObjectQueueEv@PLT
	movq	376(%r13), %rdi
	movq	0(%r13), %r15
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$4, %edx
	movdqa	-304(%rbp), %xmm6
	movdqa	-320(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movaps	%xmm6, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -320(%rbp)
	movq	-272(%rbp), %rax
	movq	24(%rax), %rdi
	call	_ZN2v88internal8compiler7JSGraph12ZeroConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, -272(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder35ForJSAsyncGeneratorObjectIsAwaitingEv@PLT
	movq	376(%r13), %rdi
	movq	%r12, %rsi
	movq	0(%r13), %r15
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	movq	%r15, %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	-280(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$4, %edx
	movhps	-272(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-320(%rbp), %xmm0
	movhps	-264(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r15
	jmp	.L117
.L114:
	movq	-176(%rbp), %rdx
	leaq	-96(%rbp), %r14
	jmp	.L115
.L145:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22939:
	.size	_ZN2v88internal8compiler16JSCreateLowering29ReduceJSCreateGeneratorObjectEPNS1_4NodeE, .-_ZN2v88internal8compiler16JSCreateLowering29ReduceJSCreateGeneratorObjectEPNS1_4NodeE
	.section	.rodata._ZN2v88internal8compiler16JSCreateLowering14ReduceNewArrayEPNS1_4NodeES4_NS1_6MapRefENS0_12ElementsKindENS0_14AllocationTypeERKNS1_23SlackTrackingPredictionE.str1.1,"aMS",@progbits,1
.LC12:
	.string	"ReduceNewArray"
	.section	.text._ZN2v88internal8compiler16JSCreateLowering14ReduceNewArrayEPNS1_4NodeES4_NS1_6MapRefENS0_12ElementsKindENS0_14AllocationTypeERKNS1_23SlackTrackingPredictionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16JSCreateLowering14ReduceNewArrayEPNS1_4NodeES4_NS1_6MapRefENS0_12ElementsKindENS0_14AllocationTypeERKNS1_23SlackTrackingPredictionE
	.type	_ZN2v88internal8compiler16JSCreateLowering14ReduceNewArrayEPNS1_4NodeES4_NS1_6MapRefENS0_12ElementsKindENS0_14AllocationTypeERKNS1_23SlackTrackingPredictionE, @function
_ZN2v88internal8compiler16JSCreateLowering14ReduceNewArrayEPNS1_4NodeES4_NS1_6MapRefENS0_12ElementsKindENS0_14AllocationTypeERKNS1_23SlackTrackingPredictionE:
.LFB22940:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movq	%rax, %rdi
	pushq	%r13
	.cfi_offset 13, -40
	movl	%r9d, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$248, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	24(%rbp), %rbx
	movl	16(%rbp), %r12d
	movq	%r8, -200(%rbp)
	movq	%rcx, -208(%rbp)
	movq	%rbx, -264(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	-280(%rbp), %rdi
	xorl	%esi, %esi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movl	$1, %edx
	movq	%rax, -224(%rbp)
	testb	%r13b, %r13b
	je	.L149
	cmpb	$4, %r13b
	je	.L167
	cmpb	$2, %r13b
	je	.L168
	movzbl	%r13b, %edx
	cmpb	$6, %r13b
	movl	$7, %eax
	cmove	%eax, %edx
.L149:
	leaq	-208(%rbp), %rax
	leaq	-192(%rbp), %rdi
	movq	%rax, %rsi
	movq	%rax, -256(%rbp)
	call	_ZNK2v88internal8compiler6MapRef14AsElementsKindENS0_12ElementsKindE@PLT
	cmpb	$0, -192(%rbp)
	jne	.L150
	movq	32(%r14), %rdi
	movl	$466, %edx
	leaq	.LC12(%rip), %rsi
	call	_ZN2v88internal8compiler28NoChangeBecauseOfMissingDataEPNS1_12JSHeapBrokerEPKci@PLT
.L151:
	movq	-56(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L183
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L150:
	.cfi_restore_state
	movq	24(%r14), %rdi
	movdqu	-184(%rbp), %xmm3
	movq	%rbx, %xmm1
	leaq	-160(%rbp), %r13
	movsd	.LC13(%rip), %xmm0
	movhps	-224(%rbp), %xmm1
	movaps	%xmm3, -208(%rbp)
	movq	(%rdi), %rbx
	movaps	%xmm1, -240(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%r13, %rsi
	movq	%rax, -216(%rbp)
	movq	24(%r14), %rax
	movq	376(%rax), %rdi
	movq	$0, -160(%rbp)
	movl	$-1, -152(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder11CheckBoundsERKNS1_14FeedbackSourceE@PLT
	movq	%r15, %xmm0
	xorl	%r8d, %r8d
	movq	%rbx, %rdi
	movdqa	-240(%rbp), %xmm1
	leaq	-96(%rbp), %r15
	movq	%rax, %rsi
	movhps	-216(%rbp), %xmm0
	movq	%r15, %rcx
	movl	$4, %edx
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm1, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-256(%rbp), %rdi
	movq	%rax, -216(%rbp)
	movq	24(%r14), %rax
	movq	(%rax), %rbx
	call	_ZNK2v88internal8compiler6MapRef13elements_kindEv@PLT
	movl	%r12d, %esi
	subl	$4, %eax
	cmpb	$1, %al
	movq	24(%r14), %rax
	movq	376(%rax), %rdi
	jbe	.L184
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder22NewSmiOrObjectElementsENS0_14AllocationTypeE@PLT
	movq	%rax, %rsi
.L153:
	movq	-224(%rbp), %rax
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%rbx, %rdi
	movq	-216(%rbp), %xmm0
	movl	$3, %edx
	movq	%rax, -80(%rbp)
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	24(%r14), %rbx
	movl	$1, %esi
	movq	%rax, -248(%rbp)
	movq	-264(%rbp), %rax
	movq	(%rbx), %r9
	movq	8(%rbx), %rdi
	movl	(%rax), %r10d
	movq	%r9, -240(%rbp)
	movl	%r10d, -272(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder11BeginRegionENS1_19RegionObservabilityE@PLT
	movq	-248(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	-240(%rbp), %r9
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r9, %rdi
	movq	%rcx, -96(%rbp)
	movq	%r15, %rcx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	pxor	%xmm0, %xmm0
	movq	(%rbx), %r9
	movq	%rbx, %rdi
	movl	-272(%rbp), %r10d
	movq	%rax, -240(%rbp)
	movq	%r9, -288(%rbp)
	cvtsi2sdl	%r10d, %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	376(%rbx), %rdi
	movl	%r12d, %edx
	movl	$4294967295, %esi
	movq	%rax, -272(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder8AllocateENS1_4TypeENS0_14AllocationTypeE@PLT
	movq	-288(%rbp), %r9
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	-224(%rbp), %rdx
	movq	%rax, %rsi
	movq	-272(%rbp), %xmm0
	movq	%r9, %rdi
	movhps	-240(%rbp), %xmm0
	movq	%rdx, -80(%rbp)
	movl	$3, %edx
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r13, %rdi
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	-256(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	376(%rbx), %rdi
	movq	%r13, %rsi
	movq	(%rbx), %r12
	movq	%rax, -272(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$4, %edx
	movq	-240(%rbp), %xmm2
	movq	%rax, %rsi
	movq	%r12, %rdi
	movdqa	%xmm2, %xmm0
	movhps	-272(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movdqa	%xmm2, %xmm0
	movhps	-224(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	24(%r14), %rdi
	movq	%rax, -288(%rbp)
	call	_ZN2v88internal8compiler7JSGraph23EmptyFixedArrayConstantEv@PLT
	movq	%r13, %rdi
	movq	%rax, -272(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder39ForJSObjectPropertiesOrHashKnownPointerEv@PLT
	movq	376(%rbx), %rdi
	movq	%r13, %rsi
	movq	(%rbx), %r12
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$4, %edx
	movq	-240(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r12, %rdi
	movhps	-272(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-288(%rbp), %xmm0
	movhps	-224(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r13, %rdi
	movq	%rax, -272(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder19ForJSObjectElementsEv@PLT
	movq	376(%rbx), %rdi
	movq	%r13, %rsi
	movq	(%rbx), %r12
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$4, %edx
	movq	-240(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r12, %rdi
	movhps	-248(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-272(%rbp), %xmm0
	movhps	-224(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-256(%rbp), %rdi
	movq	%rax, -248(%rbp)
	call	_ZNK2v88internal8compiler6MapRef13elements_kindEv@PLT
	movq	%r13, %rdi
	movzbl	%al, %esi
	call	_ZN2v88internal8compiler13AccessBuilder16ForJSArrayLengthENS0_12ElementsKindE@PLT
	movq	376(%rbx), %rdi
	movq	%r13, %rsi
	movq	(%rbx), %r12
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$4, %edx
	movq	-240(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r12, %rdi
	movhps	-216(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-248(%rbp), %xmm0
	movhps	-224(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r9
	movq	-264(%rbp), %rax
	movl	4(%rax), %eax
	testl	%eax, %eax
	jle	.L154
	movq	%r9, -216(%rbp)
	xorl	%r12d, %r12d
	movq	%r15, -272(%rbp)
	.p2align 4,,10
	.p2align 3
.L155:
	movq	24(%r14), %rdi
	call	_ZN2v88internal8compiler7JSGraph17UndefinedConstantEv@PLT
	movq	-256(%rbp), %rsi
	movl	%r12d, %edx
	movq	%r13, %rdi
	movq	%rax, -248(%rbp)
	addl	$1, %r12d
	call	_ZN2v88internal8compiler13AccessBuilder27ForJSObjectInObjectPropertyERKNS1_6MapRefEi@PLT
	movq	376(%rbx), %rdi
	movq	%r13, %rsi
	movq	(%rbx), %r15
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movl	$4, %edx
	movq	%r15, %rdi
	movq	-240(%rbp), %xmm0
	movq	-272(%rbp), %rcx
	movq	%rax, %rsi
	movhps	-248(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-216(%rbp), %xmm0
	movhps	-224(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -216(%rbp)
	movq	-264(%rbp), %rax
	cmpl	4(%rax), %r12d
	jl	.L155
	movq	-216(%rbp), %r9
.L154:
	movq	8(%r14), %rdi
	movq	-280(%rbp), %r14
	movq	%r9, -216(%rbp)
	xorl	%r8d, %r8d
	movq	(%rdi), %rax
	movq	%r14, %rcx
	movq	%r14, %rdx
	movq	%r14, %rsi
	leaq	32(%r14), %r15
	call	*32(%rax)
	movq	8(%r14), %rax
	movq	-240(%rbp), %rdx
	movq	%r14, %rcx
	movq	-216(%rbp), %r9
	movq	%rax, 8(%rdx)
	movzbl	23(%r14), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L156
	movq	32(%r14), %rdi
	movq	%rcx, %rsi
	movq	%r15, %r14
	cmpq	%rdi, %rdx
	je	.L158
.L157:
	leaq	-24(%rsi), %r13
	testq	%rdi, %rdi
	je	.L160
	movq	%r13, %rsi
	movq	%r9, -216(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-216(%rbp), %r9
.L160:
	movq	-240(%rbp), %rax
	movq	%r13, %rsi
	movq	%r9, -216(%rbp)
	movq	%rax, (%r14)
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movq	-280(%rbp), %rax
	movq	-216(%rbp), %r9
	movzbl	23(%rax), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L185
.L158:
	movq	8(%r15), %rdi
	cmpq	%rdi, %r9
	je	.L163
	movq	-280(%rbp), %rsi
	addq	$8, %r15
.L162:
	leaq	-48(%rsi), %r13
	testq	%rdi, %rdi
	je	.L164
	movq	%r13, %rsi
	movq	%r9, -216(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-216(%rbp), %r9
.L164:
	movq	%r9, (%r15)
	testq	%r9, %r9
	je	.L163
	movq	%r13, %rsi
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L163:
	movq	-280(%rbp), %r14
	movl	$2, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node14TrimInputCountEi@PLT
	movq	8(%rbx), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12FinishRegionEv@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	%r14, %rax
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L184:
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder17NewDoubleElementsENS0_14AllocationTypeE@PLT
	movq	%rax, %rsi
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L156:
	movq	-280(%rbp), %rax
	movq	32(%rax), %rsi
	movq	16(%rsi), %rdi
	leaq	16(%rsi), %r14
	cmpq	%rdi, -240(%rbp)
	jne	.L157
.L159:
	movq	8(%r14), %rdi
	cmpq	%rdi, %r9
	je	.L163
	leaq	8(%r14), %r15
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L185:
	movq	-280(%rbp), %rax
	movq	32(%rax), %rsi
	leaq	16(%rsi), %r14
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L167:
	movl	$5, %edx
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L168:
	movl	$3, %edx
	jmp	.L149
.L183:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22940:
	.size	_ZN2v88internal8compiler16JSCreateLowering14ReduceNewArrayEPNS1_4NodeES4_NS1_6MapRefENS0_12ElementsKindENS0_14AllocationTypeERKNS1_23SlackTrackingPredictionE, .-_ZN2v88internal8compiler16JSCreateLowering14ReduceNewArrayEPNS1_4NodeES4_NS1_6MapRefENS0_12ElementsKindENS0_14AllocationTypeERKNS1_23SlackTrackingPredictionE
	.section	.rodata._ZN2v88internal8compiler16JSCreateLowering27ReduceJSCreateArrayIteratorEPNS1_4NodeE.str1.1,"aMS",@progbits,1
.LC15:
	.string	"storage_.is_populated_"
	.section	.text._ZN2v88internal8compiler16JSCreateLowering27ReduceJSCreateArrayIteratorEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16JSCreateLowering27ReduceJSCreateArrayIteratorEPNS1_4NodeE
	.type	_ZN2v88internal8compiler16JSCreateLowering27ReduceJSCreateArrayIteratorEPNS1_4NodeE, @function
_ZN2v88internal8compiler16JSCreateLowering27ReduceJSCreateArrayIteratorEPNS1_4NodeE:
.LFB22973:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler31CreateArrayIteratorParametersOfEPKNS1_8OperatorE@PLT
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rax, -224(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rax, -232(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	24(%r13), %rbx
	movl	$1, %esi
	movq	%rax, -208(%rbp)
	movq	8(%rbx), %rdi
	movq	(%rbx), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder11BeginRegionENS1_19RegionObservabilityE@PLT
	movq	%r14, -96(%rbp)
	leaq	-96(%rbp), %r14
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movsd	.LC14(%rip), %xmm0
	movq	%rbx, %rdi
	movq	(%rbx), %r12
	movq	%rax, -200(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	376(%rbx), %rdi
	xorl	%edx, %edx
	movl	$131073, %esi
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder8AllocateENS1_4TypeENS0_14AllocationTypeE@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$3, %edx
	movq	-216(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r12, %rdi
	movq	-208(%rbp), %rax
	movhps	-200(%rbp), %xmm0
	movq	%rax, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -200(%rbp)
	movq	32(%r13), %rax
	cmpb	$0, 24(%rax)
	je	.L210
	movq	-200(%rbp), %xmm3
	movdqu	32(%rax), %xmm4
	leaq	-160(%rbp), %r12
	leaq	-192(%rbp), %rdi
	movdqa	%xmm3, %xmm1
	movdqa	%xmm3, %xmm0
	movaps	%xmm4, -192(%rbp)
	movhps	-232(%rbp), %xmm1
	movhps	-208(%rbp), %xmm0
	movaps	%xmm1, -272(%rbp)
	movaps	%xmm0, -256(%rbp)
	call	_ZNK2v88internal8compiler16NativeContextRef26initial_array_iterator_mapEv@PLT
	movq	%r12, %rdi
	movq	%rdx, -168(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	leaq	-176(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	(%rbx), %r10
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	%rax, -216(%rbp)
	movq	%r10, -232(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$4, %edx
	movq	-232(%rbp), %r10
	movdqa	-256(%rbp), %xmm0
	movq	%rax, %rsi
	movq	-200(%rbp), %xmm2
	movq	%r10, %rdi
	movaps	%xmm0, -80(%rbp)
	movhps	-216(%rbp), %xmm2
	movaps	%xmm2, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	24(%r13), %rdi
	movq	%rax, -232(%rbp)
	call	_ZN2v88internal8compiler7JSGraph23EmptyFixedArrayConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder39ForJSObjectPropertiesOrHashKnownPointerEv@PLT
	movq	(%rbx), %r10
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	%r10, -256(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$4, %edx
	movq	-200(%rbp), %xmm0
	movq	-256(%rbp), %r10
	movq	%rax, %rsi
	movhps	-216(%rbp), %xmm0
	movq	%r10, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	-232(%rbp), %xmm0
	movhps	-208(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	24(%r13), %rdi
	movq	%rax, -232(%rbp)
	call	_ZN2v88internal8compiler7JSGraph23EmptyFixedArrayConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder19ForJSObjectElementsEv@PLT
	movq	(%rbx), %r10
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	%r10, -256(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$4, %edx
	movq	-200(%rbp), %xmm0
	movq	-256(%rbp), %r10
	movq	%rax, %rsi
	movhps	-216(%rbp), %xmm0
	movq	%r10, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	-232(%rbp), %xmm0
	movhps	-208(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r12, %rdi
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder32ForJSArrayIteratorIteratedObjectEv@PLT
	movq	(%rbx), %r10
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	%r10, -232(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$4, %edx
	movq	-232(%rbp), %r10
	movdqa	-272(%rbp), %xmm1
	movq	%rax, %rsi
	movq	-216(%rbp), %xmm0
	movq	%r10, %rdi
	movaps	%xmm1, -96(%rbp)
	movhps	-208(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	24(%r13), %rdi
	movq	%rax, -232(%rbp)
	call	_ZN2v88internal8compiler7JSGraph12ZeroConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder27ForJSArrayIteratorNextIndexEv@PLT
	movq	(%rbx), %r10
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	%r10, -256(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$4, %edx
	movq	-200(%rbp), %xmm0
	movq	-256(%rbp), %r10
	movq	%rax, %rsi
	movhps	-216(%rbp), %xmm0
	movq	%r10, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	-232(%rbp), %xmm0
	movhps	-208(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	pxor	%xmm0, %xmm0
	movq	24(%r13), %rdi
	movq	%rax, -232(%rbp)
	movq	-224(%rbp), %rax
	cvtsi2sdl	(%rax), %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%r12, %rdi
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder22ForJSArrayIteratorKindEv@PLT
	movq	(%rbx), %r9
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	%r9, -224(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$4, %edx
	movq	-200(%rbp), %xmm0
	movq	-224(%rbp), %r9
	movq	%rax, %rsi
	movhps	-216(%rbp), %xmm0
	movq	%r9, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	-232(%rbp), %xmm0
	movhps	-208(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%r13), %rdi
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%rax, %r12
	movq	%r15, %rdx
	movq	%r15, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	8(%r15), %rax
	movq	-200(%rbp), %rcx
	leaq	32(%r15), %rdx
	movq	%rax, 8(%rcx)
	movzbl	23(%r15), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L188
	movq	32(%r15), %rdi
	movq	%rdx, %r14
	movq	%r15, %rsi
	cmpq	%rdi, %rcx
	je	.L190
	leaq	-24(%rsi), %r13
	testq	%rdi, %rdi
	je	.L192
.L213:
	movq	%r13, %rsi
	movq	%rdx, -208(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-208(%rbp), %rdx
.L192:
	movq	-200(%rbp), %rax
	movq	%r13, %rsi
	movq	%rdx, -208(%rbp)
	movq	%rax, (%r14)
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movzbl	23(%r15), %eax
	movq	-208(%rbp), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L211
.L190:
	movq	8(%rdx), %rdi
	cmpq	%rdi, %r12
	je	.L195
	leaq	8(%rdx), %r14
	movq	%r15, %rsi
.L194:
	leaq	-48(%rsi), %r13
	testq	%rdi, %rdi
	je	.L196
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L196:
	movq	%r12, (%r14)
	testq	%r12, %r12
	je	.L195
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L195:
	movl	$2, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler4Node14TrimInputCountEi@PLT
	movq	8(%rbx), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12FinishRegionEv@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L212
	addq	$232, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L211:
	.cfi_restore_state
	movq	32(%r15), %rsi
	leaq	16(%rsi), %r14
.L191:
	movq	8(%r14), %rdi
	cmpq	%rdi, %r12
	je	.L195
	addq	$8, %r14
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L188:
	movq	32(%r15), %rsi
	movq	16(%rsi), %rdi
	leaq	16(%rsi), %r14
	cmpq	%rdi, -200(%rbp)
	je	.L191
	leaq	-24(%rsi), %r13
	testq	%rdi, %rdi
	jne	.L213
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L210:
	leaq	.LC15(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L212:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22973:
	.size	_ZN2v88internal8compiler16JSCreateLowering27ReduceJSCreateArrayIteratorEPNS1_4NodeE, .-_ZN2v88internal8compiler16JSCreateLowering27ReduceJSCreateArrayIteratorEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler16JSCreateLowering33ReduceJSCreateAsyncFunctionObjectEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16JSCreateLowering33ReduceJSCreateAsyncFunctionObjectEPNS1_4NodeE
	.type	_ZN2v88internal8compiler16JSCreateLowering33ReduceJSCreateAsyncFunctionObjectEPNS1_4NodeE, @function
_ZN2v88internal8compiler16JSCreateLowering33ReduceJSCreateAsyncFunctionObjectEPNS1_4NodeE:
.LFB22974:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$312, %rsp
	movq	%rsi, -224(%rbp)
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler15RegisterCountOfEPKNS1_8OperatorE@PLT
	movq	%rbx, %rdi
	xorl	%esi, %esi
	movl	%eax, %r15d
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rbx, %rdi
	movl	$1, %esi
	movq	%rax, -288(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movl	$2, %esi
	movq	%rbx, %rdi
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rbx, %rdi
	movq	%rax, -320(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetContextInputEPNS1_4NodeE@PLT
	movq	%rbx, %rdi
	xorl	%esi, %esi
	movq	%rax, -328(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	%rbx, %rdi
	xorl	%esi, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	24(%r14), %rbx
	movq	32(%r14), %rsi
	movl	$1, %ecx
	movq	%rax, -200(%rbp)
	movq	360(%rbx), %rax
	leaq	152(%rax), %rdx
	leaq	-192(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -336(%rbp)
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movdqa	-192(%rbp), %xmm1
	leaq	-176(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -280(%rbp)
	movaps	%xmm1, -176(%rbp)
	call	_ZNK2v88internal8compiler6MapRef13instance_typeEv@PLT
	movq	8(%rbx), %rdi
	movq	(%rbx), %r12
	movl	$1, %esi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder11BeginRegionENS1_19RegionObservabilityE@PLT
	movq	%r13, -96(%rbp)
	leaq	-96(%rbp), %r13
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdi
	movq	(%rbx), %r12
	movq	%rax, -208(%rbp)
	leal	16(,%r15,8), %eax
	cvtsi2sdl	%eax, %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	376(%rbx), %rdi
	xorl	%edx, %edx
	movl	$16777217, %esi
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder8AllocateENS1_4TypeENS0_14AllocationTypeE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$3, %edx
	movq	-216(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r12, %rdi
	movq	-200(%rbp), %rax
	leaq	-160(%rbp), %r12
	movhps	-208(%rbp), %xmm0
	movq	%rax, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r12, %rdi
	movq	%rax, -256(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	-280(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	(%rbx), %r9
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	%rax, -208(%rbp)
	movq	%r9, -216(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$4, %edx
	movq	-256(%rbp), %xmm6
	movq	-216(%rbp), %r9
	movq	%rax, %rsi
	movdqa	%xmm6, %xmm0
	movq	%r9, %rdi
	movhps	-208(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movdqa	%xmm6, %xmm0
	movhps	-200(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdi
	cvtsi2sdl	%r15d, %xmm0
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%r12, %rdi
	movq	%rax, -208(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder19ForFixedArrayLengthEv@PLT
	movq	(%rbx), %r9
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	%r9, -240(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$4, %edx
	movq	-256(%rbp), %xmm0
	movq	-240(%rbp), %r9
	movq	%rax, %rsi
	movhps	-208(%rbp), %xmm0
	movq	%r9, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	-216(%rbp), %xmm0
	movhps	-200(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -208(%rbp)
	testl	%r15d, %r15d
	jle	.L215
	leal	-1(%r15), %eax
	xorl	%r15d, %r15d
	movq	%rax, -272(%rbp)
	.p2align 4,,10
	.p2align 3
.L216:
	movq	24(%r14), %rdi
	call	_ZN2v88internal8compiler7JSGraph17UndefinedConstantEv@PLT
	movl	$5, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder17ForFixedArraySlotEmNS1_16WriteBarrierKindE@PLT
	movq	(%rbx), %r10
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	%r10, -240(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$4, %edx
	movq	-256(%rbp), %xmm0
	movq	-240(%rbp), %r10
	movq	%rax, %rsi
	movhps	-216(%rbp), %xmm0
	movq	%r10, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	-208(%rbp), %xmm0
	movhps	-200(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -208(%rbp)
	movq	%r15, %rax
	addq	$1, %r15
	cmpq	%rax, -272(%rbp)
	jne	.L216
.L215:
	movq	(%rbx), %r9
	movq	8(%rbx), %rdi
	movq	%r9, -216(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12FinishRegionEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$2, %edx
	movq	-216(%rbp), %r9
	movq	%rax, %rsi
	movq	-256(%rbp), %xmm0
	movq	%r9, %rdi
	movhps	-208(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	24(%r14), %rbx
	movl	$1, %esi
	movq	%rax, %r15
	movq	(%rbx), %r10
	movq	8(%rbx), %rdi
	movq	%r10, -208(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder11BeginRegionENS1_19RegionObservabilityE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$1, %edx
	movq	-208(%rbp), %r10
	movq	%rax, %rsi
	movq	%r15, -96(%rbp)
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	(%rbx), %r10
	movsd	.LC16(%rip), %xmm0
	movq	%rbx, %rdi
	movq	%rax, -208(%rbp)
	movq	%r10, -240(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	376(%rbx), %rdi
	xorl	%edx, %edx
	movl	$4294967295, %esi
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder8AllocateENS1_4TypeENS0_14AllocationTypeE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$3, %edx
	movq	-216(%rbp), %xmm0
	movq	-240(%rbp), %r10
	movq	%rax, %rsi
	movq	-200(%rbp), %rax
	movhps	-208(%rbp), %xmm0
	movq	%r10, %rdi
	movq	%rax, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -208(%rbp)
	movq	32(%r14), %rax
	cmpb	$0, 24(%rax)
	je	.L241
	movq	-208(%rbp), %xmm7
	movq	%r15, %xmm3
	movq	-336(%rbp), %rdi
	movdqa	%xmm7, %xmm2
	movdqa	%xmm7, %xmm1
	movdqa	%xmm7, %xmm4
	punpcklqdq	%xmm3, %xmm2
	movhps	-320(%rbp), %xmm1
	movdqa	%xmm7, %xmm3
	movdqa	%xmm7, %xmm5
	movdqa	%xmm7, %xmm0
	movaps	%xmm2, -320(%rbp)
	movdqu	32(%rax), %xmm2
	movhps	-304(%rbp), %xmm3
	movhps	-288(%rbp), %xmm4
	movhps	-328(%rbp), %xmm5
	movaps	%xmm1, -352(%rbp)
	movhps	-200(%rbp), %xmm0
	movaps	%xmm2, -192(%rbp)
	movaps	%xmm3, -304(%rbp)
	movaps	%xmm4, -272(%rbp)
	movaps	%xmm5, -256(%rbp)
	movaps	%xmm0, -240(%rbp)
	call	_ZNK2v88internal8compiler16NativeContextRef25async_function_object_mapEv@PLT
	movq	%r12, %rdi
	movq	%rdx, -168(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	-280(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	(%rbx), %r15
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$4, %edx
	movq	-208(%rbp), %xmm6
	movdqa	-240(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movhps	-216(%rbp), %xmm6
	movaps	%xmm0, -80(%rbp)
	movaps	%xmm6, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	24(%r14), %rdi
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal8compiler7JSGraph23EmptyFixedArrayConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder39ForJSObjectPropertiesOrHashKnownPointerEv@PLT
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	(%rbx), %r15
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$4, %edx
	movq	-208(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movhps	-216(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-240(%rbp), %xmm0
	movhps	-200(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	24(%r14), %rdi
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal8compiler7JSGraph23EmptyFixedArrayConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder19ForJSObjectElementsEv@PLT
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	(%rbx), %r15
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$4, %edx
	movq	-208(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movhps	-216(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-240(%rbp), %xmm0
	movhps	-200(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r12, %rdi
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder27ForJSGeneratorObjectContextEv@PLT
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	(%rbx), %r15
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$4, %edx
	movq	-216(%rbp), %xmm0
	movdqa	-256(%rbp), %xmm5
	movq	%rax, %rsi
	movq	%r15, %rdi
	movhps	-200(%rbp), %xmm0
	movaps	%xmm5, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r12, %rdi
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder28ForJSGeneratorObjectFunctionEv@PLT
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	(%rbx), %r15
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$4, %edx
	movq	-216(%rbp), %xmm0
	movdqa	-272(%rbp), %xmm4
	movq	%rax, %rsi
	movq	%r15, %rdi
	movhps	-200(%rbp), %xmm0
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r12, %rdi
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder28ForJSGeneratorObjectReceiverEv@PLT
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	(%rbx), %r15
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$4, %edx
	movq	-216(%rbp), %xmm0
	movdqa	-304(%rbp), %xmm3
	movq	%rax, %rsi
	movq	%r15, %rdi
	movhps	-200(%rbp), %xmm0
	movaps	%xmm3, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	24(%r14), %rdi
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal8compiler7JSGraph17UndefinedConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder35ForJSGeneratorObjectInputOrDebugPosEv@PLT
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	(%rbx), %r15
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$4, %edx
	movq	-208(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movhps	-216(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-240(%rbp), %xmm0
	movhps	-200(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	24(%r14), %rdi
	pxor	%xmm0, %xmm0
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%r12, %rdi
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder30ForJSGeneratorObjectResumeModeEv@PLT
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	(%rbx), %r15
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$4, %edx
	movq	-208(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movhps	-216(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-240(%rbp), %xmm0
	movhps	-200(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movsd	.LC11(%rip), %xmm0
	movq	24(%r14), %rdi
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%r12, %rdi
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder32ForJSGeneratorObjectContinuationEv@PLT
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	(%rbx), %r14
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$4, %edx
	movq	-208(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r14, %rdi
	movhps	-216(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-240(%rbp), %xmm0
	movhps	-200(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r12, %rdi
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder42ForJSGeneratorObjectParametersAndRegistersEv@PLT
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	(%rbx), %r14
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$4, %edx
	movq	-216(%rbp), %xmm0
	movdqa	-320(%rbp), %xmm2
	movq	%rax, %rsi
	movq	%r14, %rdi
	movhps	-200(%rbp), %xmm0
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r12, %rdi
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder31ForJSAsyncFunctionObjectPromiseEv@PLT
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	(%rbx), %r14
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	movq	%r13, %rcx
	movl	$4, %edx
	movq	%r14, %rdi
	movq	-216(%rbp), %xmm0
	movdqa	-352(%rbp), %xmm1
	movq	%rax, %rsi
	xorl	%r8d, %r8d
	movhps	-200(%rbp), %xmm0
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-224(%rbp), %rdx
	movq	-208(%rbp), %rcx
	movq	%rax, %r13
	movq	8(%rdx), %rax
	leaq	32(%rdx), %r14
	movq	%rax, 8(%rcx)
	movzbl	23(%rdx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L218
	movq	32(%rdx), %rdi
	movq	%r14, %r15
	movq	%rdx, %rsi
	cmpq	%rdi, %rcx
	je	.L220
	leaq	-24(%rsi), %r12
	testq	%rdi, %rdi
	je	.L222
.L244:
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L222:
	movq	-208(%rbp), %rax
	movq	%r12, %rsi
	movq	%rax, (%r15)
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movq	-224(%rbp), %rax
	movzbl	23(%rax), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L242
.L220:
	movq	8(%r14), %rdi
	cmpq	%r13, %rdi
	je	.L225
	movq	-224(%rbp), %rsi
	addq	$8, %r14
.L224:
	leaq	-48(%rsi), %r12
	testq	%rdi, %rdi
	je	.L226
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L226:
	movq	%r13, (%r14)
	testq	%r13, %r13
	je	.L225
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L225:
	movq	-224(%rbp), %r14
	movl	$2, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node14TrimInputCountEi@PLT
	movq	8(%rbx), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12FinishRegionEv@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L243
	movq	-224(%rbp), %rax
	addq	$312, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L242:
	.cfi_restore_state
	movq	-224(%rbp), %rax
	movq	32(%rax), %rsi
	leaq	16(%rsi), %r15
.L221:
	movq	8(%r15), %rdi
	cmpq	%r13, %rdi
	je	.L225
	leaq	8(%r15), %r14
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L218:
	movq	32(%rdx), %rsi
	movq	16(%rsi), %rdi
	leaq	16(%rsi), %r15
	cmpq	%rdi, -208(%rbp)
	je	.L221
	leaq	-24(%rsi), %r12
	testq	%rdi, %rdi
	jne	.L244
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L241:
	leaq	.LC15(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L243:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22974:
	.size	_ZN2v88internal8compiler16JSCreateLowering33ReduceJSCreateAsyncFunctionObjectEPNS1_4NodeE, .-_ZN2v88internal8compiler16JSCreateLowering33ReduceJSCreateAsyncFunctionObjectEPNS1_4NodeE
	.section	.rodata._ZN2v88internal8compiler16JSCreateLowering32ReduceJSCreateCollectionIteratorEPNS1_4NodeE.str1.1,"aMS",@progbits,1
.LC18:
	.string	"unreachable code"
	.section	.text._ZN2v88internal8compiler16JSCreateLowering32ReduceJSCreateCollectionIteratorEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16JSCreateLowering32ReduceJSCreateCollectionIteratorEPNS1_4NodeE
	.type	_ZN2v88internal8compiler16JSCreateLowering32ReduceJSCreateCollectionIteratorEPNS1_4NodeE, @function
_ZN2v88internal8compiler16JSCreateLowering32ReduceJSCreateCollectionIteratorEPNS1_4NodeE:
.LFB22976:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-160(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$216, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler36CreateCollectionIteratorParametersOfEPKNS1_8OperatorE@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%rax, -256(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%rax, -200(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%r12, %rdi
	movq	%rax, -208(%rbp)
	movq	24(%rbx), %rax
	movq	376(%rax), %r14
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler13AccessBuilder20ForJSCollectionTableEv@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	leaq	-96(%rbp), %r14
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9LoadFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$3, %edx
	movq	-200(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movq	-208(%rbp), %rax
	movhps	-216(%rbp), %xmm0
	movq	%rax, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rbx, -216(%rbp)
	movq	24(%rbx), %rbx
	movl	$1, %esi
	movq	%rax, -224(%rbp)
	movq	8(%rbx), %rdi
	movq	(%rbx), %r15
	call	_ZN2v88internal8compiler21CommonOperatorBuilder11BeginRegionENS1_19RegionObservabilityE@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	-224(%rbp), %rax
	movq	%r15, %rdi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movsd	.LC17(%rip), %xmm0
	movq	%rbx, %rdi
	movq	(%rbx), %r15
	movq	%rax, -200(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	376(%rbx), %rdi
	xorl	%edx, %edx
	movl	$131073, %esi
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder8AllocateENS1_4TypeENS0_14AllocationTypeE@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$3, %edx
	movq	-240(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movq	-208(%rbp), %rax
	movhps	-200(%rbp), %xmm0
	movq	%rax, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-216(%rbp), %rdx
	movq	-256(%rbp), %r10
	movq	%rax, -200(%rbp)
	movq	32(%rdx), %rdx
	movl	4(%r10), %eax
	movl	(%r10), %ecx
	cmpb	$0, 24(%rdx)
	je	.L276
	movdqu	32(%rdx), %xmm4
	movaps	%xmm4, -192(%rbp)
	testl	%ecx, %ecx
	je	.L247
	cmpl	$1, %ecx
	jne	.L248
	cmpl	$1, %eax
	je	.L249
	cmpl	$2, %eax
	jne	.L248
	leaq	-192(%rbp), %rdi
	call	_ZNK2v88internal8compiler16NativeContextRef26set_key_value_iterator_mapEv@PLT
	movq	%rax, -240(%rbp)
	movq	%rdx, -232(%rbp)
	movdqa	-240(%rbp), %xmm0
.L251:
	movq	-200(%rbp), %xmm3
	movq	%r12, %rdi
	movaps	%xmm0, -176(%rbp)
	movdqa	%xmm3, %xmm1
	movdqa	%xmm3, %xmm2
	movhps	-224(%rbp), %xmm1
	movhps	-208(%rbp), %xmm2
	movaps	%xmm1, -256(%rbp)
	movaps	%xmm2, -240(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	leaq	-176(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	(%rbx), %r15
	movq	%rax, -224(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$4, %edx
	movq	-200(%rbp), %xmm0
	movdqa	-240(%rbp), %xmm2
	movq	%rax, %rsi
	movq	%r15, %rdi
	movhps	-224(%rbp), %xmm0
	movaps	%xmm2, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -240(%rbp)
	movq	-216(%rbp), %rax
	movq	24(%rax), %rdi
	call	_ZN2v88internal8compiler7JSGraph23EmptyFixedArrayConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, -224(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder39ForJSObjectPropertiesOrHashKnownPointerEv@PLT
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	(%rbx), %r15
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$4, %edx
	movq	-200(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movhps	-224(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-240(%rbp), %xmm0
	movhps	-208(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -240(%rbp)
	movq	-216(%rbp), %rax
	movq	24(%rax), %rdi
	call	_ZN2v88internal8compiler7JSGraph23EmptyFixedArrayConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, -224(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder19ForJSObjectElementsEv@PLT
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	(%rbx), %r15
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$4, %edx
	movq	-200(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movhps	-224(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-240(%rbp), %xmm0
	movhps	-208(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r12, %rdi
	movq	%rax, -224(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder28ForJSCollectionIteratorTableEv@PLT
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	(%rbx), %r15
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$4, %edx
	movq	-224(%rbp), %xmm0
	movdqa	-256(%rbp), %xmm1
	movq	%rax, %rsi
	movq	%r15, %rdi
	movhps	-208(%rbp), %xmm0
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -240(%rbp)
	movq	-216(%rbp), %rax
	movq	24(%rax), %rdi
	call	_ZN2v88internal8compiler7JSGraph12ZeroConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, -224(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder28ForJSCollectionIteratorIndexEv@PLT
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	(%rbx), %r15
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	movq	%r15, %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	-200(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$4, %edx
	leaq	32(%r13), %r15
	movhps	-224(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-240(%rbp), %xmm0
	movhps	-208(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movq	%r13, %rdx
	movq	%rax, %r12
	movq	-216(%rbp), %rax
	movq	%r13, %rsi
	movq	8(%rax), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	8(%r13), %rax
	movq	-200(%rbp), %rcx
	movq	%rax, 8(%rcx)
	movzbl	23(%r13), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L254
	movq	32(%r13), %rdi
	movq	%r15, %r14
	movq	%r13, %rsi
	cmpq	%rdi, %rcx
	je	.L256
.L255:
	subq	$24, %rsi
	testq	%rdi, %rdi
	je	.L258
	movq	%rsi, -208(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-208(%rbp), %rsi
.L258:
	movq	-200(%rbp), %rax
	movq	%rax, (%r14)
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movzbl	23(%r13), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L277
.L256:
	movq	8(%r15), %rdi
	cmpq	%r12, %rdi
	je	.L261
	leaq	8(%r15), %r14
	movq	%r13, %rsi
.L260:
	leaq	-48(%rsi), %r15
	testq	%rdi, %rdi
	je	.L262
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L262:
	movq	%r12, (%r14)
	testq	%r12, %r12
	je	.L261
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L261:
	movl	$2, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node14TrimInputCountEi@PLT
	movq	8(%rbx), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12FinishRegionEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L278
	addq	$216, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L247:
	.cfi_restore_state
	cmpl	$1, %eax
	je	.L252
	cmpl	$2, %eax
	jne	.L279
	leaq	-192(%rbp), %rdi
	call	_ZNK2v88internal8compiler16NativeContextRef26map_key_value_iterator_mapEv@PLT
	movq	%rax, -240(%rbp)
	movq	%rdx, -232(%rbp)
	movdqa	-240(%rbp), %xmm0
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L249:
	leaq	-192(%rbp), %rdi
	call	_ZNK2v88internal8compiler16NativeContextRef22set_value_iterator_mapEv@PLT
	movq	%rax, -240(%rbp)
	movq	%rdx, -232(%rbp)
	movdqa	-240(%rbp), %xmm0
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L279:
	testl	%eax, %eax
	je	.L280
.L248:
	leaq	.LC18(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L254:
	movq	32(%r13), %rsi
	movq	16(%rsi), %rdi
	leaq	16(%rsi), %r14
	cmpq	%rdi, -200(%rbp)
	jne	.L255
.L257:
	movq	8(%r14), %rdi
	cmpq	%rdi, %r12
	je	.L261
	addq	$8, %r14
	jmp	.L260
	.p2align 4,,10
	.p2align 3
.L277:
	movq	32(%r13), %rsi
	leaq	16(%rsi), %r14
	jmp	.L257
	.p2align 4,,10
	.p2align 3
.L252:
	leaq	-192(%rbp), %rdi
	call	_ZNK2v88internal8compiler16NativeContextRef22map_value_iterator_mapEv@PLT
	movq	%rax, -240(%rbp)
	movq	%rdx, -232(%rbp)
	movdqa	-240(%rbp), %xmm0
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L280:
	leaq	-192(%rbp), %rdi
	call	_ZNK2v88internal8compiler16NativeContextRef20map_key_iterator_mapEv@PLT
	movq	%rax, -240(%rbp)
	movq	%rdx, -232(%rbp)
	movdqa	-240(%rbp), %xmm0
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L276:
	leaq	.LC15(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L278:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22976:
	.size	_ZN2v88internal8compiler16JSCreateLowering32ReduceJSCreateCollectionIteratorEPNS1_4NodeE, .-_ZN2v88internal8compiler16JSCreateLowering32ReduceJSCreateCollectionIteratorEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler16JSCreateLowering27ReduceJSCreateBoundFunctionEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16JSCreateLowering27ReduceJSCreateBoundFunctionEPNS1_4NodeE
	.type	_ZN2v88internal8compiler16JSCreateLowering27ReduceJSCreateBoundFunctionEPNS1_4NodeE, @function
_ZN2v88internal8compiler16JSCreateLowering27ReduceJSCreateBoundFunctionEPNS1_4NodeE:
.LFB22977:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-96(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-160(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$248, %rsp
	movq	%rdi, -256(%rbp)
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler31CreateBoundFunctionParametersOfEPKNS1_8OperatorE@PLT
	movq	32(%rbx), %rsi
	movl	$1, %ecx
	movq	8(%rax), %rdx
	movq	(%rax), %r15
	leaq	-208(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -288(%rbp)
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movl	$1, %esi
	movq	%r14, %rdi
	movq	%rax, -272(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	%rax, -224(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	24(%rbx), %rdi
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal8compiler7JSGraph23EmptyFixedArrayConstantEv@PLT
	movq	%rax, -232(%rbp)
	testl	%r15d, %r15d
	jg	.L308
.L282:
	movq	-256(%rbp), %rax
	movl	$1, %esi
	movq	24(%rax), %rbx
	movq	(%rbx), %r10
	movq	8(%rbx), %rdi
	movq	%r10, -240(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder11BeginRegionENS1_19RegionObservabilityE@PLT
	movq	-224(%rbp), %rdx
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	-240(%rbp), %r10
	movq	%rax, %rsi
	movq	%rdx, -96(%rbp)
	movl	$1, %edx
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	(%rbx), %r10
	movsd	.LC14(%rip), %xmm0
	movq	%rbx, %rdi
	movq	%rax, -224(%rbp)
	movq	%r10, -248(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	376(%rbx), %rdi
	xorl	%edx, %edx
	movl	$4194305, %esi
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder8AllocateENS1_4TypeENS0_14AllocationTypeE@PLT
	movq	-248(%rbp), %r10
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	-216(%rbp), %rdx
	movq	%rax, %rsi
	movq	-240(%rbp), %xmm0
	movq	%r10, %rdi
	movhps	-224(%rbp), %xmm0
	movq	%rdx, -80(%rbp)
	movl	$3, %edx
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r12, %rdi
	movq	%rax, -224(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	-288(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	(%rbx), %r15
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$4, %edx
	movq	-224(%rbp), %xmm1
	movq	%rax, %rsi
	movq	%r15, %rdi
	movdqa	%xmm1, %xmm0
	movhps	-240(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movdqa	%xmm1, %xmm0
	movhps	-216(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -248(%rbp)
	movq	-256(%rbp), %rax
	movq	24(%rax), %rdi
	call	_ZN2v88internal8compiler7JSGraph23EmptyFixedArrayConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder39ForJSObjectPropertiesOrHashKnownPointerEv@PLT
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	(%rbx), %r15
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$4, %edx
	movq	-224(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movhps	-240(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-248(%rbp), %xmm0
	movhps	-216(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -248(%rbp)
	movq	-256(%rbp), %rax
	movq	24(%rax), %rdi
	call	_ZN2v88internal8compiler7JSGraph23EmptyFixedArrayConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder19ForJSObjectElementsEv@PLT
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	(%rbx), %r15
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$4, %edx
	movq	-224(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movhps	-240(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-248(%rbp), %xmm0
	movhps	-216(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r12, %rdi
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder37ForJSBoundFunctionBoundTargetFunctionEv@PLT
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	(%rbx), %r15
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$4, %edx
	movq	-224(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movhps	-272(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-240(%rbp), %xmm0
	movhps	-216(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r12, %rdi
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder27ForJSBoundFunctionBoundThisEv@PLT
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	(%rbx), %r15
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$4, %edx
	movq	-224(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movhps	-280(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-240(%rbp), %xmm0
	movhps	-216(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r12, %rdi
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder32ForJSBoundFunctionBoundArgumentsEv@PLT
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	(%rbx), %r15
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	movq	%r15, %rdi
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	-224(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$4, %edx
	leaq	32(%r14), %r15
	movhps	-232(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-240(%rbp), %xmm0
	movhps	-216(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r14, %rcx
	xorl	%r8d, %r8d
	movq	%r14, %rdx
	movq	%rax, %r12
	movq	-256(%rbp), %rax
	movq	%r14, %rsi
	movq	8(%rax), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	8(%r14), %rax
	movq	-224(%rbp), %rcx
	movq	%rax, 8(%rcx)
	movzbl	23(%r14), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L284
	movq	32(%r14), %rdi
	movq	%r15, %rax
	movq	%r14, %rsi
	cmpq	%rdi, %rcx
	je	.L286
	leaq	-24(%rsi), %r13
	testq	%rdi, %rdi
	je	.L288
.L311:
	movq	%r13, %rsi
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-216(%rbp), %rax
.L288:
	movq	-224(%rbp), %rdx
	movq	%r13, %rsi
	movq	%rdx, (%rax)
	movq	%rdx, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movzbl	23(%r14), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L309
.L286:
	movq	8(%r15), %rdi
	cmpq	%rdi, %r12
	je	.L291
	addq	$8, %r15
	movq	%r14, %rsi
.L290:
	leaq	-48(%rsi), %r13
	testq	%rdi, %rdi
	je	.L292
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L292:
	movq	%r12, (%r15)
	testq	%r12, %r12
	je	.L291
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L291:
	movl	$2, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node14TrimInputCountEi@PLT
	movq	8(%rbx), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12FinishRegionEv@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L310
	addq	$248, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L309:
	.cfi_restore_state
	movq	32(%r14), %rsi
	leaq	16(%rsi), %rax
.L287:
	movq	8(%rax), %rdi
	cmpq	%rdi, %r12
	je	.L291
	leaq	8(%rax), %r15
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L308:
	movq	%rbx, %rcx
	movq	24(%rbx), %rbx
	leaq	-192(%rbp), %rdi
	movq	32(%rcx), %rsi
	movl	$1, %ecx
	movq	360(%rbx), %rdx
	addq	$152, %rdx
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movdqa	-192(%rbp), %xmm3
	leaq	-176(%rbp), %r10
	movq	%r10, %rdi
	movq	%r10, -264(%rbp)
	movaps	%xmm3, -176(%rbp)
	call	_ZNK2v88internal8compiler6MapRef13instance_typeEv@PLT
	movq	(%rbx), %r11
	movq	8(%rbx), %rdi
	movl	$1, %esi
	movq	%r11, -232(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder11BeginRegionENS1_19RegionObservabilityE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$1, %edx
	movq	-232(%rbp), %r11
	movq	%rax, %rsi
	movq	-224(%rbp), %rax
	movq	%r11, %rdi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	pxor	%xmm0, %xmm0
	movq	(%rbx), %r11
	movq	%rbx, %rdi
	movq	%rax, -224(%rbp)
	leal	16(,%r15,8), %eax
	cvtsi2sdl	%eax, %xmm0
	movq	%r11, -240(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	376(%rbx), %rdi
	xorl	%edx, %edx
	movl	$16777217, %esi
	movq	%rax, -232(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder8AllocateENS1_4TypeENS0_14AllocationTypeE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$3, %edx
	movq	-240(%rbp), %r11
	movq	%rax, %rsi
	movq	-216(%rbp), %rax
	movq	-232(%rbp), %xmm0
	movq	%r11, %rdi
	movq	%rax, -80(%rbp)
	movhps	-224(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r12, %rdi
	movq	%rax, -248(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	-264(%rbp), %r10
	movq	%rbx, %rdi
	movq	%r10, %rsi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	(%rbx), %r10
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	%rax, -224(%rbp)
	movq	%r10, -232(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$4, %edx
	movq	-248(%rbp), %xmm2
	movq	-232(%rbp), %r10
	movq	%rax, %rsi
	movdqa	%xmm2, %xmm0
	movq	%r10, %rdi
	movhps	-224(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movdqa	%xmm2, %xmm0
	movhps	-216(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdi
	cvtsi2sdl	%r15d, %xmm0
	movq	%rax, -232(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%r12, %rdi
	movq	%rax, -224(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder19ForFixedArrayLengthEv@PLT
	movq	(%rbx), %r10
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	%r10, -240(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$4, %edx
	movq	-248(%rbp), %xmm0
	movq	-240(%rbp), %r10
	movq	%rax, %rsi
	movhps	-224(%rbp), %xmm0
	movq	%r10, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	-232(%rbp), %xmm0
	movhps	-216(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -224(%rbp)
	leal	-1(%r15), %eax
	xorl	%r15d, %r15d
	movq	%rax, -264(%rbp)
	.p2align 4,,10
	.p2align 3
.L283:
	leal	2(%r15), %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movl	$5, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, -232(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder17ForFixedArraySlotEmNS1_16WriteBarrierKindE@PLT
	movq	(%rbx), %r11
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	%r11, -240(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$4, %edx
	movq	-248(%rbp), %xmm0
	movq	-240(%rbp), %r11
	movq	%rax, %rsi
	movhps	-232(%rbp), %xmm0
	movq	%r11, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	-224(%rbp), %xmm0
	movhps	-216(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -224(%rbp)
	movq	%r15, %rax
	addq	$1, %r15
	cmpq	%rax, -264(%rbp)
	jne	.L283
	movq	(%rbx), %r10
	movq	8(%rbx), %rdi
	movq	%r10, -232(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12FinishRegionEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$2, %edx
	movq	-248(%rbp), %xmm0
	movq	-232(%rbp), %r10
	movq	%rax, %rsi
	movhps	-224(%rbp), %xmm0
	movq	%r10, %rdi
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -224(%rbp)
	movq	%rax, -232(%rbp)
	jmp	.L282
	.p2align 4,,10
	.p2align 3
.L284:
	movq	32(%r14), %rsi
	movq	16(%rsi), %rdi
	leaq	16(%rsi), %rax
	cmpq	-224(%rbp), %rdi
	je	.L287
	leaq	-24(%rsi), %r13
	testq	%rdi, %rdi
	jne	.L311
	jmp	.L288
.L310:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22977:
	.size	_ZN2v88internal8compiler16JSCreateLowering27ReduceJSCreateBoundFunctionEPNS1_4NodeE, .-_ZN2v88internal8compiler16JSCreateLowering27ReduceJSCreateBoundFunctionEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler16JSCreateLowering21ReduceJSCreateClosureEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16JSCreateLowering21ReduceJSCreateClosureEPNS1_4NodeE
	.type	_ZN2v88internal8compiler16JSCreateLowering21ReduceJSCreateClosureEPNS1_4NodeE, @function
_ZN2v88internal8compiler16JSCreateLowering21ReduceJSCreateClosureEPNS1_4NodeE:
.LFB22978:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$280, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -288(%rbp)
	movq	(%rsi), %rdi
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler25CreateClosureParametersOfEPKNS1_8OperatorE@PLT
	movq	32(%r14), %rsi
	movl	$1, %ecx
	movq	%rax, %rbx
	movq	(%rax), %rdx
	leaq	-224(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -272(%rbp)
	call	_ZN2v88internal8compiler21SharedFunctionInfoRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	leaq	-208(%rbp), %rax
	movq	8(%rbx), %rdx
	movq	32(%r14), %rsi
	movq	%rax, %rdi
	movl	$1, %ecx
	movq	%rax, -256(%rbp)
	call	_ZN2v88internal8compiler15FeedbackCellRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	16(%rbx), %rdx
	movq	32(%r14), %rsi
	xorl	%ecx, %ecx
	leaq	-192(%rbp), %rax
	movq	%rax, %rbx
	movq	%rax, %rdi
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L345
	movq	-288(%rbp), %rbx
	xorl	%esi, %esi
	leaq	-160(%rbp), %r12
	leaq	-176(%rbp), %r15
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rbx, %rdi
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetContextInputEPNS1_4NodeE@PLT
	movq	-256(%rbp), %rdi
	movq	%rax, -296(%rbp)
	call	_ZNK2v88internal8compiler13HeapObjectRef3mapEv@PLT
	movq	32(%r14), %rsi
	movq	%r12, %rdi
	movl	$1, %ecx
	movq	%rax, -176(%rbp)
	movq	24(%r14), %rax
	movq	%rdx, -168(%rbp)
	movq	360(%rax), %rdx
	addq	$504, %rdx
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef6equalsERKS2_@PLT
	testb	%al, %al
	je	.L346
	movq	32(%r14), %rax
	cmpb	$0, 24(%rax)
	je	.L347
	movdqu	32(%rax), %xmm3
	movq	-272(%rbp), %rdi
	movaps	%xmm3, -160(%rbp)
	call	_ZNK2v88internal8compiler21SharedFunctionInfoRef18function_map_indexEv@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZNK2v88internal8compiler16NativeContextRef23GetFunctionMapFromIndexEi@PLT
	movq	24(%r14), %rbx
	movq	%r15, %rdi
	movq	%rdx, -168(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZNK2v88internal8compiler6MapRef13instance_sizeEv@PLT
	movq	(%rbx), %r10
	movq	8(%rbx), %rdi
	movl	$1, %esi
	movl	%eax, -304(%rbp)
	movq	%r10, -232(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder11BeginRegionENS1_19RegionObservabilityE@PLT
	movq	-232(%rbp), %r10
	xorl	%r8d, %r8d
	movq	%r13, -96(%rbp)
	movq	%rax, %rsi
	leaq	-96(%rbp), %rax
	movl	$1, %edx
	movq	%r10, %rdi
	movq	%rax, %rcx
	movq	%rax, -248(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdi
	movq	(%rbx), %r13
	movl	-304(%rbp), %r9d
	movq	%rax, -232(%rbp)
	cvtsi2sdl	%r9d, %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	376(%rbx), %rdi
	xorl	%edx, %edx
	movl	$2097153, %esi
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder8AllocateENS1_4TypeENS0_14AllocationTypeE@PLT
	movq	-240(%rbp), %rdx
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	movq	-304(%rbp), %xmm0
	movq	-248(%rbp), %rcx
	movq	%rax, %rsi
	movq	%rdx, -80(%rbp)
	movl	$3, %edx
	movhps	-232(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r12, %rdi
	movq	%rax, -232(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	(%rbx), %r13
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movl	$4, %edx
	movq	%r13, %rdi
	movq	-232(%rbp), %xmm2
	movq	-248(%rbp), %rcx
	movq	%rax, %rsi
	movdqa	%xmm2, %xmm0
	movhps	-304(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movdqa	%xmm2, %xmm0
	movhps	-240(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	24(%r14), %rdi
	movq	%rax, -312(%rbp)
	call	_ZN2v88internal8compiler7JSGraph23EmptyFixedArrayConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder39ForJSObjectPropertiesOrHashKnownPointerEv@PLT
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	(%rbx), %r13
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movl	$4, %edx
	movq	%r13, %rdi
	movq	-232(%rbp), %xmm0
	movq	-248(%rbp), %rcx
	movq	%rax, %rsi
	movhps	-304(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-312(%rbp), %xmm0
	movhps	-240(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	24(%r14), %rdi
	movq	%rax, -312(%rbp)
	call	_ZN2v88internal8compiler7JSGraph23EmptyFixedArrayConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder19ForJSObjectElementsEv@PLT
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	(%rbx), %r13
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movl	$4, %edx
	movq	%r13, %rdi
	movq	-232(%rbp), %xmm0
	movq	-248(%rbp), %rcx
	movq	%rax, %rsi
	movhps	-304(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-312(%rbp), %xmm0
	movhps	-240(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r12, %rdi
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder31ForJSFunctionSharedFunctionInfoEv@PLT
	movq	-272(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	(%rbx), %r13
	movq	%rax, -272(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movl	$4, %edx
	movq	%r13, %rdi
	movq	-232(%rbp), %xmm0
	movq	-248(%rbp), %rcx
	movq	%rax, %rsi
	movhps	-272(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-304(%rbp), %xmm0
	movhps	-240(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r12, %rdi
	movq	%rax, -272(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder20ForJSFunctionContextEv@PLT
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	(%rbx), %r13
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movl	$4, %edx
	movq	%r13, %rdi
	movq	-232(%rbp), %xmm0
	movq	-248(%rbp), %rcx
	movq	%rax, %rsi
	movhps	-296(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-272(%rbp), %xmm0
	movhps	-240(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r12, %rdi
	movq	%rax, -272(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder25ForJSFunctionFeedbackCellEv@PLT
	movq	-256(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	(%rbx), %r13
	movq	%rax, -256(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movl	$4, %edx
	movq	%r13, %rdi
	movq	-232(%rbp), %xmm0
	movq	-248(%rbp), %rcx
	movq	%rax, %rsi
	movhps	-256(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-272(%rbp), %xmm0
	movhps	-240(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r12, %rdi
	movq	%rax, -272(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder17ForJSFunctionCodeEv@PLT
	movq	-280(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	(%rbx), %r13
	movq	%rax, -256(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movl	$4, %edx
	movq	%r13, %rdi
	movq	-232(%rbp), %xmm0
	movq	-248(%rbp), %rcx
	movq	%rax, %rsi
	movhps	-256(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-272(%rbp), %xmm0
	movhps	-240(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r15, %rdi
	movq	%rax, -256(%rbp)
	call	_ZNK2v88internal8compiler6MapRef18has_prototype_slotEv@PLT
	movq	-256(%rbp), %r9
	testb	%al, %al
	jne	.L348
.L317:
	movq	%r14, -280(%rbp)
	xorl	%r13d, %r13d
	movq	%r9, %r14
	jmp	.L319
	.p2align 4,,10
	.p2align 3
.L349:
	movq	-280(%rbp), %rax
	movq	%r14, %xmm0
	movhps	-240(%rbp), %xmm0
	movq	24(%rax), %rdi
	movaps	%xmm0, -272(%rbp)
	call	_ZN2v88internal8compiler7JSGraph17UndefinedConstantEv@PLT
	movl	%r13d, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, -256(%rbp)
	addl	$1, %r13d
	call	_ZN2v88internal8compiler13AccessBuilder27ForJSObjectInObjectPropertyERKNS1_6MapRefEi@PLT
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	(%rbx), %r14
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	movq	%r14, %rdi
	xorl	%r8d, %r8d
	movl	$4, %edx
	movq	-232(%rbp), %xmm1
	movdqa	-272(%rbp), %xmm0
	movq	%rax, %rsi
	movq	-248(%rbp), %rcx
	movhps	-256(%rbp), %xmm1
	movaps	%xmm0, -80(%rbp)
	movaps	%xmm1, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r14
.L319:
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler6MapRef21GetInObjectPropertiesEv@PLT
	cmpl	%r13d, %eax
	jg	.L349
	movq	%r14, -240(%rbp)
	movq	-280(%rbp), %r14
	xorl	%r8d, %r8d
	movq	-288(%rbp), %r15
	movq	8(%r14), %rdi
	movq	%r15, %rdx
	movq	%r15, %rcx
	leaq	32(%r15), %r14
	movq	%r15, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	8(%r15), %rax
	movq	-232(%rbp), %rdx
	movq	-240(%rbp), %r9
	movq	%rax, 8(%rdx)
	movzbl	23(%r15), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L320
	movq	32(%r15), %rdi
	movq	%r14, %r13
	movq	%r15, %rsi
	cmpq	%rdi, %rdx
	je	.L322
.L321:
	leaq	-24(%rsi), %r12
	testq	%rdi, %rdi
	je	.L324
	movq	%r12, %rsi
	movq	%r9, -240(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-240(%rbp), %r9
.L324:
	movq	-232(%rbp), %rax
	movq	%r12, %rsi
	movq	%r9, -240(%rbp)
	movq	%rax, 0(%r13)
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movq	-288(%rbp), %rax
	movq	-240(%rbp), %r9
	movzbl	23(%rax), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L350
.L322:
	movq	8(%r14), %rdi
	cmpq	%rdi, %r9
	je	.L327
	movq	-288(%rbp), %rsi
	addq	$8, %r14
.L326:
	leaq	-48(%rsi), %r12
	testq	%rdi, %rdi
	je	.L328
	movq	%r12, %rsi
	movq	%r9, -232(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-232(%rbp), %r9
.L328:
	movq	%r9, (%r14)
	testq	%r9, %r9
	je	.L327
	movq	%r12, %rsi
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L327:
	movq	-288(%rbp), %r15
	movl	$2, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler4Node14TrimInputCountEi@PLT
	movq	8(%rbx), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12FinishRegionEv@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	%r15, %rax
	jmp	.L315
	.p2align 4,,10
	.p2align 3
.L346:
	xorl	%eax, %eax
.L315:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L351
	addq	$280, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L350:
	.cfi_restore_state
	movq	-288(%rbp), %rax
	movq	32(%rax), %rsi
	leaq	16(%rsi), %r13
.L323:
	movq	8(%r13), %rdi
	cmpq	%rdi, %r9
	je	.L327
	leaq	8(%r13), %r14
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L320:
	movq	-288(%rbp), %rax
	movq	32(%rax), %rsi
	movq	16(%rsi), %rdi
	leaq	16(%rsi), %r13
	cmpq	%rdi, -232(%rbp)
	jne	.L321
	jmp	.L323
	.p2align 4,,10
	.p2align 3
.L348:
	movq	%r9, %xmm0
	movq	24(%r14), %rdi
	movhps	-240(%rbp), %xmm0
	movaps	%xmm0, -272(%rbp)
	call	_ZN2v88internal8compiler7JSGraph15TheHoleConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, -256(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder34ForJSFunctionPrototypeOrInitialMapEv@PLT
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	(%rbx), %r13
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movl	$4, %edx
	movq	%r13, %rdi
	movq	-232(%rbp), %xmm1
	movdqa	-272(%rbp), %xmm0
	movq	%rax, %rsi
	movq	-248(%rbp), %rcx
	movhps	-256(%rbp), %xmm1
	movaps	%xmm0, -80(%rbp)
	movaps	%xmm1, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r9
	jmp	.L317
	.p2align 4,,10
	.p2align 3
.L345:
	leaq	.LC9(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L347:
	leaq	.LC15(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L351:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22978:
	.size	_ZN2v88internal8compiler16JSCreateLowering21ReduceJSCreateClosureEPNS1_4NodeE, .-_ZN2v88internal8compiler16JSCreateLowering21ReduceJSCreateClosureEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler16JSCreateLowering30ReduceJSCreateIterResultObjectEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16JSCreateLowering30ReduceJSCreateIterResultObjectEPNS1_4NodeE
	.type	_ZN2v88internal8compiler16JSCreateLowering30ReduceJSCreateIterResultObjectEPNS1_4NodeE, @function
_ZN2v88internal8compiler16JSCreateLowering30ReduceJSCreateIterResultObjectEPNS1_4NodeE:
.LFB22979:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	movq	%r12, %rdi
	subq	$200, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -200(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -208(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	24(%r14), %rbx
	movq	%rax, %r15
	movq	32(%r14), %rax
	cmpb	$0, 24(%rax)
	je	.L376
	movdqu	32(%rax), %xmm2
	leaq	-160(%rbp), %r13
	leaq	-176(%rbp), %rdi
	movaps	%xmm2, -176(%rbp)
	call	_ZNK2v88internal8compiler16NativeContextRef19iterator_result_mapEv@PLT
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movq	%rdx, -152(%rbp)
	movq	%rax, -160(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	24(%r14), %rbx
	movl	$1, %esi
	movq	%rax, -216(%rbp)
	movq	(%rbx), %r9
	movq	8(%rbx), %rdi
	movq	8(%r9), %rax
	movq	%r9, -184(%rbp)
	movq	%rax, -192(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder11BeginRegionENS1_19RegionObservabilityE@PLT
	movq	-184(%rbp), %r9
	movq	%r15, -96(%rbp)
	leaq	-96(%rbp), %r15
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	(%rbx), %r9
	movsd	.LC17(%rip), %xmm0
	movq	%rbx, %rdi
	movq	%rax, -184(%rbp)
	movq	%r9, -232(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	376(%rbx), %rdi
	xorl	%edx, %edx
	movl	$4294967295, %esi
	movq	%rax, -224(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder8AllocateENS1_4TypeENS0_14AllocationTypeE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$3, %edx
	movq	-232(%rbp), %r9
	movq	%rax, %rsi
	movq	-192(%rbp), %rax
	movq	-224(%rbp), %xmm0
	movq	%r9, %rdi
	movq	%rax, -80(%rbp)
	movhps	-184(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r13, %rdi
	movq	%rax, -184(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	(%rbx), %r9
	movq	376(%rbx), %rdi
	movq	%r13, %rsi
	movq	%r9, -224(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$4, %edx
	movq	-184(%rbp), %xmm1
	movq	-224(%rbp), %r9
	movq	%rax, %rsi
	movdqa	%xmm1, %xmm0
	movq	%r9, %rdi
	movhps	-216(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movdqa	%xmm1, %xmm0
	movhps	-192(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	24(%r14), %rdi
	movq	%rax, -224(%rbp)
	call	_ZN2v88internal8compiler7JSGraph23EmptyFixedArrayConstantEv@PLT
	movq	%r13, %rdi
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder39ForJSObjectPropertiesOrHashKnownPointerEv@PLT
	movq	(%rbx), %r9
	movq	376(%rbx), %rdi
	movq	%r13, %rsi
	movq	%r9, -232(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$4, %edx
	movq	-184(%rbp), %xmm0
	movq	-232(%rbp), %r9
	movq	%rax, %rsi
	movhps	-216(%rbp), %xmm0
	movq	%r9, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	-224(%rbp), %xmm0
	movhps	-192(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	24(%r14), %rdi
	movq	%rax, -224(%rbp)
	call	_ZN2v88internal8compiler7JSGraph23EmptyFixedArrayConstantEv@PLT
	movq	%r13, %rdi
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder19ForJSObjectElementsEv@PLT
	movq	376(%rbx), %rdi
	movq	%r13, %rsi
	movq	(%rbx), %r14
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$4, %edx
	movq	-184(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r14, %rdi
	movhps	-216(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-224(%rbp), %xmm0
	movhps	-192(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r13, %rdi
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder24ForJSIteratorResultValueEv@PLT
	movq	376(%rbx), %rdi
	movq	%r13, %rsi
	movq	(%rbx), %r14
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$4, %edx
	movq	-184(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r14, %rdi
	movhps	-200(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-216(%rbp), %xmm0
	movhps	-192(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r13, %rdi
	movq	%rax, -200(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder23ForJSIteratorResultDoneEv@PLT
	movq	376(%rbx), %rdi
	movq	%r13, %rsi
	movq	(%rbx), %r14
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$4, %edx
	movq	-184(%rbp), %xmm0
	movq	%r14, %rdi
	movq	%rax, %rsi
	movhps	-208(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-200(%rbp), %xmm0
	movhps	-192(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-184(%rbp), %rcx
	leaq	32(%r12), %rdx
	movq	%rax, %r14
	movq	8(%r12), %rax
	movq	%rax, 8(%rcx)
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L354
	movq	32(%r12), %rdi
	movq	%rdx, %r15
	movq	%r12, %rsi
	cmpq	%rdi, %rcx
	je	.L356
	leaq	-24(%rsi), %r13
	testq	%rdi, %rdi
	je	.L358
.L379:
	movq	%r13, %rsi
	movq	%rdx, -192(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-192(%rbp), %rdx
.L358:
	movq	-184(%rbp), %rax
	movq	%r13, %rsi
	movq	%rdx, -192(%rbp)
	movq	%rax, (%r15)
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movzbl	23(%r12), %eax
	movq	-192(%rbp), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L377
.L356:
	movq	8(%rdx), %rdi
	cmpq	%rdi, %r14
	je	.L361
	leaq	8(%rdx), %r15
	movq	%r12, %rsi
.L360:
	leaq	-48(%rsi), %r13
	testq	%rdi, %rdi
	je	.L362
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L362:
	movq	%r14, (%r15)
	testq	%r14, %r14
	je	.L361
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L361:
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node14TrimInputCountEi@PLT
	movq	8(%rbx), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12FinishRegionEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L378
	addq	$200, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L377:
	.cfi_restore_state
	movq	32(%r12), %rsi
	leaq	16(%rsi), %r15
.L357:
	movq	8(%r15), %rdi
	cmpq	%rdi, %r14
	je	.L361
	addq	$8, %r15
	jmp	.L360
	.p2align 4,,10
	.p2align 3
.L354:
	movq	32(%r12), %rsi
	movq	16(%rsi), %rdi
	leaq	16(%rsi), %r15
	cmpq	-184(%rbp), %rdi
	je	.L357
	leaq	-24(%rsi), %r13
	testq	%rdi, %rdi
	jne	.L379
	jmp	.L358
	.p2align 4,,10
	.p2align 3
.L376:
	leaq	.LC15(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L378:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22979:
	.size	_ZN2v88internal8compiler16JSCreateLowering30ReduceJSCreateIterResultObjectEPNS1_4NodeE, .-_ZN2v88internal8compiler16JSCreateLowering30ReduceJSCreateIterResultObjectEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler16JSCreateLowering28ReduceJSCreateStringIteratorEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16JSCreateLowering28ReduceJSCreateStringIteratorEPNS1_4NodeE
	.type	_ZN2v88internal8compiler16JSCreateLowering28ReduceJSCreateStringIteratorEPNS1_4NodeE, @function
_ZN2v88internal8compiler16JSCreateLowering28ReduceJSCreateStringIteratorEPNS1_4NodeE:
.LFB22980:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	movq	%r12, %rdi
	subq	$184, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -200(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	24(%r14), %rbx
	movq	%rax, %r15
	movq	32(%r14), %rax
	cmpb	$0, 24(%rax)
	je	.L404
	movdqu	32(%rax), %xmm2
	leaq	-160(%rbp), %r13
	leaq	-176(%rbp), %rdi
	movaps	%xmm2, -176(%rbp)
	call	_ZNK2v88internal8compiler16NativeContextRef27initial_string_iterator_mapEv@PLT
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movq	%rdx, -152(%rbp)
	movq	%rax, -160(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	24(%r14), %rbx
	movl	$1, %esi
	movq	%rax, -208(%rbp)
	movq	(%rbx), %r9
	movq	8(%rbx), %rdi
	movq	8(%r9), %rax
	movq	%r9, -184(%rbp)
	movq	%rax, -192(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder11BeginRegionENS1_19RegionObservabilityE@PLT
	movq	-184(%rbp), %r9
	movq	%r15, -96(%rbp)
	leaq	-96(%rbp), %r15
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	(%rbx), %r9
	movsd	.LC17(%rip), %xmm0
	movq	%rbx, %rdi
	movq	%rax, -184(%rbp)
	movq	%r9, -224(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	376(%rbx), %rdi
	xorl	%edx, %edx
	movl	$131073, %esi
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder8AllocateENS1_4TypeENS0_14AllocationTypeE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$3, %edx
	movq	-224(%rbp), %r9
	movq	%rax, %rsi
	movq	-192(%rbp), %rax
	movq	-216(%rbp), %xmm0
	movq	%r9, %rdi
	movq	%rax, -80(%rbp)
	movhps	-184(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r13, %rdi
	movq	%rax, -184(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	(%rbx), %r9
	movq	376(%rbx), %rdi
	movq	%r13, %rsi
	movq	%r9, -216(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$4, %edx
	movq	-184(%rbp), %xmm1
	movq	-216(%rbp), %r9
	movq	%rax, %rsi
	movdqa	%xmm1, %xmm0
	movq	%r9, %rdi
	movhps	-208(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movdqa	%xmm1, %xmm0
	movhps	-192(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	24(%r14), %rdi
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal8compiler7JSGraph23EmptyFixedArrayConstantEv@PLT
	movq	%r13, %rdi
	movq	%rax, -208(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder39ForJSObjectPropertiesOrHashKnownPointerEv@PLT
	movq	(%rbx), %r9
	movq	376(%rbx), %rdi
	movq	%r13, %rsi
	movq	%r9, -224(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$4, %edx
	movq	-184(%rbp), %xmm0
	movq	-224(%rbp), %r9
	movq	%rax, %rsi
	movhps	-208(%rbp), %xmm0
	movq	%r9, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	-216(%rbp), %xmm0
	movhps	-192(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	24(%r14), %rdi
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal8compiler7JSGraph23EmptyFixedArrayConstantEv@PLT
	movq	%r13, %rdi
	movq	%rax, -208(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder19ForJSObjectElementsEv@PLT
	movq	(%rbx), %r9
	movq	376(%rbx), %rdi
	movq	%r13, %rsi
	movq	%r9, -224(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$4, %edx
	movq	-184(%rbp), %xmm0
	movq	-224(%rbp), %r9
	movq	%rax, %rsi
	movhps	-208(%rbp), %xmm0
	movq	%r9, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	-216(%rbp), %xmm0
	movhps	-192(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r13, %rdi
	movq	%rax, -208(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder25ForJSStringIteratorStringEv@PLT
	movq	(%rbx), %r9
	movq	376(%rbx), %rdi
	movq	%r13, %rsi
	movq	%r9, -216(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$4, %edx
	movq	-184(%rbp), %xmm0
	movq	-216(%rbp), %r9
	movq	%rax, %rsi
	movhps	-200(%rbp), %xmm0
	movq	%r9, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	-208(%rbp), %xmm0
	movhps	-192(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	24(%r14), %rdi
	pxor	%xmm0, %xmm0
	movq	%rax, -208(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%r13, %rdi
	movq	%rax, -200(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder24ForJSStringIteratorIndexEv@PLT
	movq	376(%rbx), %rdi
	movq	%r13, %rsi
	movq	(%rbx), %r14
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	movq	%r15, %rcx
	movl	$4, %edx
	movq	%r14, %rdi
	movq	-184(%rbp), %xmm0
	movq	%rax, %rsi
	xorl	%r8d, %r8d
	movhps	-200(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-208(%rbp), %xmm0
	movhps	-192(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-184(%rbp), %rcx
	leaq	32(%r12), %rdx
	movq	%rax, %r14
	movq	8(%r12), %rax
	movq	%rax, 8(%rcx)
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L382
	movq	32(%r12), %rdi
	movq	%rdx, %r15
	movq	%r12, %rsi
	cmpq	%rdi, %rcx
	je	.L384
	leaq	-24(%rsi), %r13
	testq	%rdi, %rdi
	je	.L386
.L407:
	movq	%r13, %rsi
	movq	%rdx, -192(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-192(%rbp), %rdx
.L386:
	movq	-184(%rbp), %rax
	movq	%r13, %rsi
	movq	%rdx, -192(%rbp)
	movq	%rax, (%r15)
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movzbl	23(%r12), %eax
	movq	-192(%rbp), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L405
.L384:
	movq	8(%rdx), %rdi
	cmpq	%rdi, %r14
	je	.L389
	leaq	8(%rdx), %r15
	movq	%r12, %rsi
.L388:
	leaq	-48(%rsi), %r13
	testq	%rdi, %rdi
	je	.L390
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L390:
	movq	%r14, (%r15)
	testq	%r14, %r14
	je	.L389
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L389:
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node14TrimInputCountEi@PLT
	movq	8(%rbx), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12FinishRegionEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L406
	addq	$184, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L405:
	.cfi_restore_state
	movq	32(%r12), %rsi
	leaq	16(%rsi), %r15
.L385:
	movq	8(%r15), %rdi
	cmpq	%rdi, %r14
	je	.L389
	addq	$8, %r15
	jmp	.L388
	.p2align 4,,10
	.p2align 3
.L382:
	movq	32(%r12), %rsi
	movq	16(%rsi), %rdi
	leaq	16(%rsi), %r15
	cmpq	-184(%rbp), %rdi
	je	.L385
	leaq	-24(%rsi), %r13
	testq	%rdi, %rdi
	jne	.L407
	jmp	.L386
	.p2align 4,,10
	.p2align 3
.L404:
	leaq	.LC15(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L406:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22980:
	.size	_ZN2v88internal8compiler16JSCreateLowering28ReduceJSCreateStringIteratorEPNS1_4NodeE, .-_ZN2v88internal8compiler16JSCreateLowering28ReduceJSCreateStringIteratorEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler16JSCreateLowering27ReduceJSCreateKeyValueArrayEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16JSCreateLowering27ReduceJSCreateKeyValueArrayEPNS1_4NodeE
	.type	_ZN2v88internal8compiler16JSCreateLowering27ReduceJSCreateKeyValueArrayEPNS1_4NodeE, @function
_ZN2v88internal8compiler16JSCreateLowering27ReduceJSCreateKeyValueArrayEPNS1_4NodeE:
.LFB22981:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	xorl	%esi, %esi
	pushq	%r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -200(%rbp)
	movq	%r13, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%rax, -224(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	-200(%rbp), %r10
	movq	%rax, %r14
	movq	32(%r10), %rax
	movq	24(%r10), %rbx
	cmpb	$0, 24(%rax)
	je	.L432
	movdqu	32(%rax), %xmm4
	leaq	-176(%rbp), %r15
	leaq	-160(%rbp), %r12
	movq	%r10, -200(%rbp)
	movq	%r15, %rdi
	movaps	%xmm4, -176(%rbp)
	call	_ZNK2v88internal8compiler16NativeContextRef28js_array_packed_elements_mapEv@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rdx, -152(%rbp)
	movq	%rax, -160(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	-200(%rbp), %r10
	movsd	.LC19(%rip), %xmm2
	movq	%rax, -240(%rbp)
	movq	24(%r10), %rdi
	movapd	%xmm2, %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	-200(%rbp), %r10
	movl	$1, %ecx
	leaq	-192(%rbp), %rdi
	movq	%rax, -248(%rbp)
	movq	24(%r10), %rbx
	movq	32(%r10), %rsi
	movq	%r10, -264(%rbp)
	movq	(%rbx), %rax
	movq	8(%rax), %rax
	movq	%rax, -208(%rbp)
	movq	360(%rbx), %rax
	leaq	152(%rax), %rdx
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movdqa	-192(%rbp), %xmm5
	movq	%r15, %rdi
	movaps	%xmm5, -176(%rbp)
	call	_ZNK2v88internal8compiler6MapRef13instance_typeEv@PLT
	movq	(%rbx), %r9
	movq	8(%rbx), %rdi
	movl	$1, %esi
	movq	%r9, -200(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder11BeginRegionENS1_19RegionObservabilityE@PLT
	movq	-200(%rbp), %r9
	movq	%r14, -96(%rbp)
	leaq	-96(%rbp), %r14
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movsd	.LC20(%rip), %xmm1
	movq	(%rbx), %r9
	movq	%rbx, %rdi
	movq	%rax, -200(%rbp)
	movapd	%xmm1, %xmm0
	movq	%r9, -256(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	376(%rbx), %rdi
	xorl	%edx, %edx
	movl	$16777217, %esi
	movq	%rax, -232(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder8AllocateENS1_4TypeENS0_14AllocationTypeE@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$3, %edx
	movq	-256(%rbp), %r9
	movq	%rax, %rsi
	movq	-208(%rbp), %rax
	movq	-232(%rbp), %xmm0
	movq	%r9, %rdi
	movq	%rax, -80(%rbp)
	movhps	-200(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r12, %rdi
	movq	%rax, -200(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	(%rbx), %r15
	movq	%rax, -232(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$4, %edx
	movq	-200(%rbp), %xmm3
	movq	%rax, %rsi
	movq	%r15, %rdi
	movdqa	%xmm3, %xmm0
	movhps	-232(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movdqa	%xmm3, %xmm0
	movhps	-208(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rbx, %rdi
	movq	%rax, -256(%rbp)
	movq	.LC19(%rip), %rax
	movq	%rax, %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%r12, %rdi
	movq	%rax, -232(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder19ForFixedArrayLengthEv@PLT
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	(%rbx), %r15
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$4, %edx
	movq	-200(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movhps	-232(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-256(%rbp), %xmm0
	movhps	-208(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-264(%rbp), %r10
	movq	%rax, -256(%rbp)
	movq	24(%r10), %rdi
	call	_ZN2v88internal8compiler7JSGraph12ZeroConstantEv@PLT
	movl	$1, %edx
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, -232(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder20ForFixedArrayElementENS0_12ElementsKindENS0_15LoadSensitivityE@PLT
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	(%rbx), %r15
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder12StoreElementERKNS1_13ElementAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$5, %edx
	movq	-200(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movq	-208(%rbp), %rax
	movhps	-232(%rbp), %xmm0
	movq	%rax, -64(%rbp)
	movaps	%xmm0, -96(%rbp)
	movq	-216(%rbp), %xmm0
	movhps	-256(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-264(%rbp), %r10
	movq	%rax, -232(%rbp)
	movq	24(%r10), %rdi
	movq	%r10, -256(%rbp)
	call	_ZN2v88internal8compiler7JSGraph11OneConstantEv@PLT
	movl	$1, %edx
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder20ForFixedArrayElementENS0_12ElementsKindENS0_15LoadSensitivityE@PLT
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	(%rbx), %r15
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder12StoreElementERKNS1_13ElementAccessE@PLT
	movq	%r14, %rcx
	xorl	%r8d, %r8d
	movl	$5, %edx
	movq	-200(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movq	-208(%rbp), %rax
	movhps	-216(%rbp), %xmm0
	movq	%rax, -64(%rbp)
	movaps	%xmm0, -96(%rbp)
	movq	-224(%rbp), %xmm0
	movhps	-232(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%rbx), %rdi
	movq	(%rbx), %r15
	movq	%rax, -208(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12FinishRegionEv@PLT
	movq	%r14, %rcx
	xorl	%r8d, %r8d
	movl	$2, %edx
	movq	-200(%rbp), %xmm0
	movq	%r15, %rdi
	movq	%rax, %rsi
	movhps	-208(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-256(%rbp), %r10
	movl	$1, %esi
	movq	%rax, -216(%rbp)
	movq	24(%r10), %rbx
	movq	(%rbx), %r15
	movq	8(%rbx), %rdi
	movq	8(%r15), %rax
	movq	%rax, -208(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder11BeginRegionENS1_19RegionObservabilityE@PLT
	movq	%r14, %rcx
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%rax, %rsi
	movq	-216(%rbp), %rax
	movq	%r15, %rdi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rbx, %rdi
	movq	(%rbx), %r15
	movq	%rax, -200(%rbp)
	movq	.LC20(%rip), %rax
	movq	%rax, %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	376(%rbx), %rdi
	xorl	%edx, %edx
	movl	$4294967295, %esi
	movq	%rax, -224(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder8AllocateENS1_4TypeENS0_14AllocationTypeE@PLT
	movq	%r14, %rcx
	xorl	%r8d, %r8d
	movl	$3, %edx
	movq	-224(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movq	-208(%rbp), %rax
	movhps	-200(%rbp), %xmm0
	movq	%rax, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r12, %rdi
	movq	%rax, -200(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	(%rbx), %r15
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	movq	%r14, %rcx
	xorl	%r8d, %r8d
	movl	$4, %edx
	movq	-200(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movhps	-240(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-200(%rbp), %xmm0
	movhps	-208(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-256(%rbp), %r10
	movq	%rax, -232(%rbp)
	movq	24(%r10), %rdi
	call	_ZN2v88internal8compiler7JSGraph23EmptyFixedArrayConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, -224(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder39ForJSObjectPropertiesOrHashKnownPointerEv@PLT
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	(%rbx), %r15
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	movq	%r14, %rcx
	xorl	%r8d, %r8d
	movl	$4, %edx
	movq	-200(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movhps	-224(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-232(%rbp), %xmm0
	movhps	-208(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r12, %rdi
	movq	%rax, -224(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder19ForJSObjectElementsEv@PLT
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	(%rbx), %r15
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	movq	%r14, %rcx
	xorl	%r8d, %r8d
	movl	$4, %edx
	movq	-200(%rbp), %xmm0
	movq	%r15, %rdi
	movq	%rax, %rsi
	movhps	-216(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-224(%rbp), %xmm0
	movhps	-208(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder16ForJSArrayLengthENS0_12ElementsKindE@PLT
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	(%rbx), %r15
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	movq	%r14, %rcx
	movl	$4, %edx
	xorl	%r8d, %r8d
	movq	-200(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movhps	-248(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-216(%rbp), %xmm0
	movhps	-208(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-200(%rbp), %rcx
	leaq	32(%r13), %rdx
	movq	%rax, %r14
	movq	8(%r13), %rax
	movq	%rax, 8(%rcx)
	movzbl	23(%r13), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L410
	movq	32(%r13), %rdi
	movq	%rdx, %r15
	movq	%r13, %rsi
	cmpq	%rdi, %rcx
	je	.L412
	leaq	-24(%rsi), %r12
	testq	%rdi, %rdi
	je	.L414
.L435:
	movq	%r12, %rsi
	movq	%rdx, -208(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-208(%rbp), %rdx
.L414:
	movq	-200(%rbp), %rax
	movq	%r12, %rsi
	movq	%rdx, -208(%rbp)
	movq	%rax, (%r15)
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movzbl	23(%r13), %eax
	movq	-208(%rbp), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L433
.L412:
	movq	8(%rdx), %rdi
	cmpq	%rdi, %r14
	je	.L417
	leaq	8(%rdx), %r15
	movq	%r13, %rsi
.L416:
	leaq	-48(%rsi), %r12
	testq	%rdi, %rdi
	je	.L418
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L418:
	movq	%r14, (%r15)
	testq	%r14, %r14
	je	.L417
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L417:
	movl	$2, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node14TrimInputCountEi@PLT
	movq	8(%rbx), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12FinishRegionEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L434
	addq	$232, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L433:
	.cfi_restore_state
	movq	32(%r13), %rsi
	leaq	16(%rsi), %r15
.L413:
	movq	8(%r15), %rdi
	cmpq	%rdi, %r14
	je	.L417
	addq	$8, %r15
	jmp	.L416
	.p2align 4,,10
	.p2align 3
.L410:
	movq	32(%r13), %rsi
	movq	16(%rsi), %rdi
	leaq	16(%rsi), %r15
	cmpq	-200(%rbp), %rdi
	je	.L413
	leaq	-24(%rsi), %r12
	testq	%rdi, %rdi
	jne	.L435
	jmp	.L414
	.p2align 4,,10
	.p2align 3
.L432:
	leaq	.LC15(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L434:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22981:
	.size	_ZN2v88internal8compiler16JSCreateLowering27ReduceJSCreateKeyValueArrayEPNS1_4NodeE, .-_ZN2v88internal8compiler16JSCreateLowering27ReduceJSCreateKeyValueArrayEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler16JSCreateLowering21ReduceJSCreatePromiseEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16JSCreateLowering21ReduceJSCreatePromiseEPNS1_4NodeE
	.type	_ZN2v88internal8compiler16JSCreateLowering21ReduceJSCreatePromiseEPNS1_4NodeE, @function
_ZN2v88internal8compiler16JSCreateLowering21ReduceJSCreatePromiseEPNS1_4NodeE:
.LFB22982:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	xorl	%esi, %esi
	pushq	%r12
	movq	%r13, %rdi
	pushq	%rbx
	subq	$200, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	%rax, %r15
	movq	32(%r14), %rax
	cmpb	$0, 24(%rax)
	je	.L460
	movdqu	32(%rax), %xmm2
	leaq	-160(%rbp), %r12
	leaq	-176(%rbp), %rdi
	movaps	%xmm2, -176(%rbp)
	call	_ZNK2v88internal8compiler16NativeContextRef16promise_functionEv@PLT
	movq	%r12, %rdi
	movq	%rax, -160(%rbp)
	movq	%rdx, -152(%rbp)
	call	_ZNK2v88internal8compiler13JSFunctionRef11initial_mapEv@PLT
	movq	24(%r14), %rbx
	leaq	-192(%rbp), %r10
	movq	%rdx, -184(%rbp)
	movq	%r10, %rdi
	movq	%rax, -192(%rbp)
	movq	(%rbx), %rax
	movq	%r10, -232(%rbp)
	movq	8(%rax), %rax
	movq	%rax, -208(%rbp)
	call	_ZNK2v88internal8compiler6MapRef13instance_sizeEv@PLT
	movq	(%rbx), %r11
	movq	8(%rbx), %rdi
	movl	$1, %esi
	movl	%eax, -216(%rbp)
	movq	%r11, -200(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder11BeginRegionENS1_19RegionObservabilityE@PLT
	movq	-200(%rbp), %r11
	movq	%r15, -96(%rbp)
	leaq	-96(%rbp), %r15
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r11, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	pxor	%xmm0, %xmm0
	movq	(%rbx), %r11
	movq	%rbx, %rdi
	movl	-216(%rbp), %r9d
	movq	%rax, -200(%rbp)
	movq	%r11, -224(%rbp)
	cvtsi2sdl	%r9d, %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	376(%rbx), %rdi
	xorl	%edx, %edx
	movl	$4294967295, %esi
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder8AllocateENS1_4TypeENS0_14AllocationTypeE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$3, %edx
	movq	-224(%rbp), %r11
	movq	%rax, %rsi
	movq	-208(%rbp), %rax
	movq	-216(%rbp), %xmm0
	movq	%r11, %rdi
	movq	%rax, -80(%rbp)
	movhps	-200(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r12, %rdi
	movq	%rax, -200(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	-232(%rbp), %r10
	movq	%rbx, %rdi
	movq	%r10, %rsi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	(%rbx), %r9
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	%rax, -216(%rbp)
	movq	%r9, -224(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$4, %edx
	movq	-200(%rbp), %xmm1
	movq	-224(%rbp), %r9
	movq	%rax, %rsi
	movdqa	%xmm1, %xmm0
	movq	%r9, %rdi
	movhps	-216(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movdqa	%xmm1, %xmm0
	movhps	-208(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	24(%r14), %rdi
	movq	%rax, -224(%rbp)
	call	_ZN2v88internal8compiler7JSGraph23EmptyFixedArrayConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder39ForJSObjectPropertiesOrHashKnownPointerEv@PLT
	movq	(%rbx), %r9
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	%r9, -232(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$4, %edx
	movq	-200(%rbp), %xmm0
	movq	-232(%rbp), %r9
	movq	%rax, %rsi
	movhps	-216(%rbp), %xmm0
	movq	%r9, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	-224(%rbp), %xmm0
	movhps	-208(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	24(%r14), %rdi
	movq	%rax, -224(%rbp)
	call	_ZN2v88internal8compiler7JSGraph23EmptyFixedArrayConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder19ForJSObjectElementsEv@PLT
	movq	(%rbx), %r9
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	%r9, -232(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$4, %edx
	movq	-200(%rbp), %xmm0
	movq	-232(%rbp), %r9
	movq	%rax, %rsi
	movhps	-216(%rbp), %xmm0
	movq	%r9, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	-224(%rbp), %xmm0
	movhps	-208(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	24(%r14), %rdi
	movq	%rax, -224(%rbp)
	call	_ZN2v88internal8compiler7JSGraph12ZeroConstantEv@PLT
	movl	$5, %edx
	movl	$24, %esi
	movq	%r12, %rdi
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder17ForJSObjectOffsetEiNS1_16WriteBarrierKindE@PLT
	movq	(%rbx), %r9
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	%r9, -232(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$4, %edx
	movq	-200(%rbp), %xmm0
	movq	-232(%rbp), %r9
	movq	%rax, %rsi
	movhps	-216(%rbp), %xmm0
	movq	%r9, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	-224(%rbp), %xmm0
	movhps	-208(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	24(%r14), %rdi
	movq	%rax, -224(%rbp)
	call	_ZN2v88internal8compiler7JSGraph12ZeroConstantEv@PLT
	movl	$5, %edx
	movl	$32, %esi
	movq	%r12, %rdi
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder17ForJSObjectOffsetEiNS1_16WriteBarrierKindE@PLT
	movq	(%rbx), %r9
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	%r9, -232(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$4, %edx
	movq	-200(%rbp), %xmm0
	movq	-232(%rbp), %r9
	movq	%rax, %rsi
	movhps	-216(%rbp), %xmm0
	movq	%r9, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	-224(%rbp), %xmm0
	movhps	-208(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	24(%r14), %rdi
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal8compiler7JSGraph12ZeroConstantEv@PLT
	movl	$5, %edx
	movl	$40, %esi
	movq	%r12, %rdi
	movq	%rax, -224(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder17ForJSObjectOffsetEiNS1_16WriteBarrierKindE@PLT
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	(%rbx), %r14
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	movq	%r15, %rcx
	movl	$4, %edx
	movq	%r14, %rdi
	movq	-200(%rbp), %xmm0
	movq	%rax, %rsi
	xorl	%r8d, %r8d
	movhps	-224(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-216(%rbp), %xmm0
	movhps	-208(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-200(%rbp), %rcx
	leaq	32(%r13), %rdx
	movq	%rax, %r14
	movq	8(%r13), %rax
	movq	%rax, 8(%rcx)
	movzbl	23(%r13), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L438
	movq	32(%r13), %rdi
	movq	%rdx, %r15
	movq	%r13, %rsi
	cmpq	%rdi, %rcx
	je	.L440
	leaq	-24(%rsi), %r12
	testq	%rdi, %rdi
	je	.L442
.L463:
	movq	%r12, %rsi
	movq	%rdx, -208(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-208(%rbp), %rdx
.L442:
	movq	-200(%rbp), %rax
	movq	%r12, %rsi
	movq	%rdx, -208(%rbp)
	movq	%rax, (%r15)
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movzbl	23(%r13), %eax
	movq	-208(%rbp), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L461
.L440:
	movq	8(%rdx), %rdi
	cmpq	%rdi, %r14
	je	.L445
	leaq	8(%rdx), %r15
	movq	%r13, %rsi
.L444:
	leaq	-48(%rsi), %r12
	testq	%rdi, %rdi
	je	.L446
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L446:
	movq	%r14, (%r15)
	testq	%r14, %r14
	je	.L445
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L445:
	movl	$2, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node14TrimInputCountEi@PLT
	movq	8(%rbx), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12FinishRegionEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L462
	addq	$200, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L461:
	.cfi_restore_state
	movq	32(%r13), %rsi
	leaq	16(%rsi), %r15
.L441:
	movq	8(%r15), %rdi
	cmpq	%rdi, %r14
	je	.L445
	addq	$8, %r15
	jmp	.L444
	.p2align 4,,10
	.p2align 3
.L438:
	movq	32(%r13), %rsi
	movq	16(%rsi), %rdi
	leaq	16(%rsi), %r15
	cmpq	-200(%rbp), %rdi
	je	.L441
	leaq	-24(%rsi), %r12
	testq	%rdi, %rdi
	jne	.L463
	jmp	.L442
	.p2align 4,,10
	.p2align 3
.L460:
	leaq	.LC15(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L462:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22982:
	.size	_ZN2v88internal8compiler16JSCreateLowering21ReduceJSCreatePromiseEPNS1_4NodeE, .-_ZN2v88internal8compiler16JSCreateLowering21ReduceJSCreatePromiseEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler16JSCreateLowering32ReduceJSCreateEmptyLiteralObjectEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16JSCreateLowering32ReduceJSCreateEmptyLiteralObjectEPNS1_4NodeE
	.type	_ZN2v88internal8compiler16JSCreateLowering32ReduceJSCreateEmptyLiteralObjectEPNS1_4NodeE, @function
_ZN2v88internal8compiler16JSCreateLowering32ReduceJSCreateEmptyLiteralObjectEPNS1_4NodeE:
.LFB22985:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	movq	%r14, %rdi
	pushq	%r12
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -256(%rbp)
	xorl	%esi, %esi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, -224(%rbp)
	movq	32(%r15), %rax
	cmpb	$0, 24(%rax)
	je	.L490
	movdqu	32(%rax), %xmm3
	leaq	-160(%rbp), %r13
	leaq	-176(%rbp), %rdi
	leaq	-192(%rbp), %r14
	movaps	%xmm3, -176(%rbp)
	call	_ZNK2v88internal8compiler16NativeContextRef15object_functionEv@PLT
	movq	%r13, %rdi
	movq	%rax, -160(%rbp)
	movq	%rdx, -152(%rbp)
	call	_ZNK2v88internal8compiler13JSFunctionRef11initial_mapEv@PLT
	movq	24(%r15), %rdi
	movq	%r14, %rsi
	movq	%rdx, -184(%rbp)
	movq	%rax, -192(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	24(%r15), %rdi
	movq	%rax, -200(%rbp)
	call	_ZN2v88internal8compiler7JSGraph23EmptyFixedArrayConstantEv@PLT
	movq	24(%r15), %r12
	movq	%r14, %rdi
	movq	%rax, -216(%rbp)
	call	_ZNK2v88internal8compiler6MapRef13instance_sizeEv@PLT
	movq	(%r12), %r10
	movq	8(%r12), %rdi
	movl	$1, %esi
	movl	%eax, -240(%rbp)
	movq	%r10, -208(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder11BeginRegionENS1_19RegionObservabilityE@PLT
	movq	-208(%rbp), %r10
	xorl	%r8d, %r8d
	movq	%rbx, -96(%rbp)
	movq	%rax, %rsi
	leaq	-96(%rbp), %rax
	movl	$1, %edx
	movq	%r10, %rdi
	movq	%rax, %rcx
	movq	%rax, -248(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movq	(%r12), %rbx
	movl	-240(%rbp), %r9d
	movq	%rax, -208(%rbp)
	cvtsi2sdl	%r9d, %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	376(%r12), %rdi
	xorl	%edx, %edx
	movl	$4294967295, %esi
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder8AllocateENS1_4TypeENS0_14AllocationTypeE@PLT
	movq	-224(%rbp), %rdx
	xorl	%r8d, %r8d
	movq	%rbx, %rdi
	movq	-240(%rbp), %xmm0
	movq	-248(%rbp), %rcx
	movq	%rax, %rsi
	movq	%rdx, -80(%rbp)
	movl	$3, %edx
	movhps	-208(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r13, %rdi
	movq	%rax, -208(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	376(%r12), %rdi
	movq	%r13, %rsi
	movq	(%r12), %rbx
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movl	$4, %edx
	movq	%rbx, %rdi
	movq	-208(%rbp), %xmm2
	movq	-248(%rbp), %rcx
	movq	%rax, %rsi
	movdqa	%xmm2, %xmm0
	movhps	-200(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movdqa	%xmm2, %xmm0
	movhps	-224(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	24(%r15), %rdi
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal8compiler7JSGraph23EmptyFixedArrayConstantEv@PLT
	movq	%r13, %rdi
	movq	%rax, -200(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder39ForJSObjectPropertiesOrHashKnownPointerEv@PLT
	movq	376(%r12), %rdi
	movq	%r13, %rsi
	movq	(%r12), %rbx
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movl	$4, %edx
	movq	%rbx, %rdi
	movq	-208(%rbp), %xmm0
	movq	-248(%rbp), %rcx
	movq	%rax, %rsi
	movhps	-200(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-240(%rbp), %xmm0
	movhps	-224(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r13, %rdi
	movq	%rax, -200(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder19ForJSObjectElementsEv@PLT
	movq	376(%r12), %rdi
	movq	%r13, %rsi
	movq	(%r12), %rbx
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	movq	%rbx, %rdi
	xorl	%r8d, %r8d
	movl	$4, %edx
	movq	-208(%rbp), %xmm0
	movq	-248(%rbp), %rcx
	movq	%rax, %rsi
	xorl	%ebx, %ebx
	movhps	-216(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-200(%rbp), %xmm0
	movhps	-224(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r9
	jmp	.L467
	.p2align 4,,10
	.p2align 3
.L491:
	movq	%r9, %xmm0
	movq	24(%r15), %rdi
	movhps	-224(%rbp), %xmm0
	movaps	%xmm0, -240(%rbp)
	call	_ZN2v88internal8compiler7JSGraph17UndefinedConstantEv@PLT
	movl	%ebx, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, -200(%rbp)
	addl	$1, %ebx
	call	_ZN2v88internal8compiler13AccessBuilder27ForJSObjectInObjectPropertyERKNS1_6MapRefEi@PLT
	movq	(%r12), %r9
	movq	376(%r12), %rdi
	movq	%r13, %rsi
	movq	%r9, -216(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	movq	-216(%rbp), %r9
	xorl	%r8d, %r8d
	movl	$4, %edx
	movq	-208(%rbp), %xmm1
	movdqa	-240(%rbp), %xmm0
	movq	%rax, %rsi
	movq	-248(%rbp), %rcx
	movq	%r9, %rdi
	movhps	-200(%rbp), %xmm1
	movaps	%xmm0, -80(%rbp)
	movaps	%xmm1, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r9
.L467:
	movq	%r14, %rdi
	movq	%r9, -200(%rbp)
	call	_ZNK2v88internal8compiler6MapRef21GetInObjectPropertiesEv@PLT
	movq	-200(%rbp), %r9
	cmpl	%ebx, %eax
	jg	.L491
	movq	8(%r15), %rdi
	movq	-256(%rbp), %rbx
	movq	%r9, -200(%rbp)
	xorl	%r8d, %r8d
	movq	(%rdi), %rax
	movq	%rbx, %rcx
	movq	%rbx, %rdx
	movq	%rbx, %rsi
	leaq	32(%rbx), %r14
	call	*32(%rax)
	movq	8(%rbx), %rax
	movq	-208(%rbp), %rdx
	movq	%rbx, %rcx
	movq	-200(%rbp), %r9
	movq	%rax, 8(%rdx)
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L468
	movq	32(%rbx), %rdi
	movq	%rcx, %rsi
	movq	%r14, %rbx
	cmpq	%rdi, %rdx
	je	.L470
	leaq	-24(%rsi), %r13
	testq	%rdi, %rdi
	je	.L472
.L494:
	movq	%r13, %rsi
	movq	%r9, -200(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-200(%rbp), %r9
.L472:
	movq	-208(%rbp), %rax
	movq	%r13, %rsi
	movq	%r9, -200(%rbp)
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movq	-256(%rbp), %rax
	movq	-200(%rbp), %r9
	movzbl	23(%rax), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L492
.L470:
	movq	8(%r14), %rdi
	cmpq	%rdi, %r9
	je	.L475
	movq	-256(%rbp), %rsi
	addq	$8, %r14
.L474:
	leaq	-48(%rsi), %r13
	testq	%rdi, %rdi
	je	.L476
	movq	%r13, %rsi
	movq	%r9, -200(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-200(%rbp), %r9
.L476:
	movq	%r9, (%r14)
	testq	%r9, %r9
	je	.L475
	movq	%r13, %rsi
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L475:
	movq	-256(%rbp), %rbx
	movl	$2, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler4Node14TrimInputCountEi@PLT
	movq	8(%r12), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12FinishRegionEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L493
	movq	-256(%rbp), %rax
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L492:
	.cfi_restore_state
	movq	-256(%rbp), %rax
	movq	32(%rax), %rsi
	leaq	16(%rsi), %rbx
.L471:
	movq	8(%rbx), %rdi
	cmpq	%rdi, %r9
	je	.L475
	leaq	8(%rbx), %r14
	jmp	.L474
	.p2align 4,,10
	.p2align 3
.L468:
	movq	-256(%rbp), %rax
	movq	32(%rax), %rsi
	movq	16(%rsi), %rdi
	leaq	16(%rsi), %rbx
	cmpq	%rdi, -208(%rbp)
	je	.L471
	leaq	-24(%rsi), %r13
	testq	%rdi, %rdi
	jne	.L494
	jmp	.L472
	.p2align 4,,10
	.p2align 3
.L490:
	leaq	.LC15(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L493:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22985:
	.size	_ZN2v88internal8compiler16JSCreateLowering32ReduceJSCreateEmptyLiteralObjectEPNS1_4NodeE, .-_ZN2v88internal8compiler16JSCreateLowering32ReduceJSCreateEmptyLiteralObjectEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler16JSCreateLowering29ReduceJSCreateFunctionContextEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16JSCreateLowering29ReduceJSCreateFunctionContextEPNS1_4NodeE
	.type	_ZN2v88internal8compiler16JSCreateLowering29ReduceJSCreateFunctionContextEPNS1_4NodeE, @function
_ZN2v88internal8compiler16JSCreateLowering29ReduceJSCreateFunctionContextEPNS1_4NodeE:
.LFB22987:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-208(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	subq	$264, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -256(%rbp)
	movq	(%rsi), %rdi
	movq	%fs:40, %rdx
	movq	%rdx, -56(%rbp)
	xorl	%edx, %edx
	call	_ZN2v88internal8compiler33CreateFunctionContextParametersOfEPKNS1_8OperatorE@PLT
	movq	32(%r14), %rsi
	movl	$1, %ecx
	movq	%r13, %rdi
	movq	(%rax), %rdx
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler12ScopeInfoRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movl	8(%rbx), %r10d
	xorl	%eax, %eax
	cmpl	$15, %r10d
	jle	.L530
.L513:
	movq	-56(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L531
	addq	$264, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L530:
	.cfi_restore_state
	movzbl	12(%rbx), %r15d
	movq	-256(%rbp), %rbx
	xorl	%esi, %esi
	movl	%r10d, -216(%rbp)
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rbx, %rdi
	movq	%rax, -224(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetContextInputEPNS1_4NodeE@PLT
	movq	24(%r14), %rdi
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal8compiler7JSGraph15TheHoleConstantEv@PLT
	movl	-216(%rbp), %r10d
	movq	24(%r14), %rbx
	movq	%rax, -248(%rbp)
	leal	4(%r10), %eax
	movl	%eax, -232(%rbp)
	cmpb	$1, %r15b
	je	.L497
	cmpb	$2, %r15b
	jne	.L532
	movq	360(%rbx), %rax
	leaq	224(%rax), %rdx
.L500:
	movq	32(%r14), %rsi
	movl	$1, %ecx
	leaq	-96(%rbp), %r15
	leaq	-192(%rbp), %rdi
	movl	%r10d, -272(%rbp)
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movdqa	-192(%rbp), %xmm3
	movl	$1, %esi
	movaps	%xmm3, -176(%rbp)
	movq	(%rbx), %r9
	movq	8(%rbx), %rdi
	movq	%r9, -216(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder11BeginRegionENS1_19RegionObservabilityE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$1, %edx
	movq	-216(%rbp), %r9
	movq	%rax, %rsi
	movq	%r12, -96(%rbp)
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdi
	movq	(%rbx), %r12
	movl	-272(%rbp), %r10d
	movq	%rax, -216(%rbp)
	leal	48(,%r10,8), %eax
	movl	%r10d, -300(%rbp)
	cvtsi2sdl	%eax, %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	376(%rbx), %rdi
	xorl	%edx, %edx
	movl	$16777217, %esi
	movq	%rax, -272(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder8AllocateENS1_4TypeENS0_14AllocationTypeE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$3, %edx
	movq	-272(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r12, %rdi
	movq	-224(%rbp), %rax
	leaq	-160(%rbp), %r12
	movhps	-216(%rbp), %xmm0
	movq	%rax, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r12, %rdi
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	leaq	-176(%rbp), %r9
	movq	%rbx, %rdi
	movq	%r9, %rsi
	movq	%r9, -296(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	(%rbx), %r11
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	%rax, -272(%rbp)
	movq	%r11, -280(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$4, %edx
	movq	-216(%rbp), %xmm2
	movq	-280(%rbp), %r11
	movq	%rax, %rsi
	movdqa	%xmm2, %xmm0
	movq	%r11, %rdi
	movhps	-272(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movdqa	%xmm2, %xmm0
	movhps	-224(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdi
	cvtsi2sdl	-232(%rbp), %xmm0
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%r12, %rdi
	movq	%rax, -272(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder19ForFixedArrayLengthEv@PLT
	movq	(%rbx), %r11
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	%r11, -288(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$4, %edx
	movq	-216(%rbp), %xmm0
	movq	-288(%rbp), %r11
	movq	%rax, %rsi
	movhps	-272(%rbp), %xmm0
	movq	%r11, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	-280(%rbp), %xmm0
	movhps	-224(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder14ForContextSlotEm@PLT
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	(%rbx), %r11
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	%rax, -272(%rbp)
	movq	%r11, -288(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$4, %edx
	movq	-216(%rbp), %xmm0
	movq	-288(%rbp), %r11
	movq	%rax, %rsi
	movhps	-272(%rbp), %xmm0
	movq	%r11, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	-280(%rbp), %xmm0
	movhps	-224(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -272(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder14ForContextSlotEm@PLT
	movq	(%rbx), %r11
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	%r11, -280(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$4, %edx
	movq	-216(%rbp), %xmm0
	movq	-280(%rbp), %r11
	movq	%rax, %rsi
	movhps	-240(%rbp), %xmm0
	movq	%r11, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	-272(%rbp), %xmm0
	movhps	-224(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder14ForContextSlotEm@PLT
	movq	(%rbx), %r11
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	%r11, -272(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$4, %edx
	movq	-216(%rbp), %xmm0
	movq	-272(%rbp), %r11
	movq	%rax, %rsi
	movhps	-248(%rbp), %xmm0
	movq	%r11, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	-240(%rbp), %xmm0
	movhps	-224(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	24(%r14), %rdi
	movq	-296(%rbp), %r9
	movq	%rax, %xmm0
	movq	32(%r14), %rax
	movl	-300(%rbp), %r10d
	cmpb	$0, 24(%rax)
	je	.L533
	movdqu	32(%rax), %xmm4
	movhps	-224(%rbp), %xmm0
	movq	%r9, %rsi
	movl	%r10d, -280(%rbp)
	movaps	%xmm0, -272(%rbp)
	movaps	%xmm4, -176(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder14ForContextSlotEm@PLT
	movq	(%rbx), %r9
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	%r9, -248(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$4, %edx
	movq	-248(%rbp), %r9
	movdqa	-272(%rbp), %xmm0
	movq	%rax, %rsi
	movq	-216(%rbp), %xmm1
	movq	%r9, %rdi
	movaps	%xmm0, -80(%rbp)
	movhps	-240(%rbp), %xmm1
	movaps	%xmm1, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	cmpl	$4, -232(%rbp)
	movq	%rax, %r9
	jle	.L502
	movl	-280(%rbp), %r10d
	movl	$4, %r13d
	leal	-1(%r10), %eax
	addq	$5, %rax
	movq	%rax, -272(%rbp)
	.p2align 4,,10
	.p2align 3
.L503:
	movq	24(%r14), %rdi
	movq	%r9, -248(%rbp)
	call	_ZN2v88internal8compiler7JSGraph17UndefinedConstantEv@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	addq	$1, %r13
	movq	%rax, -232(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder14ForContextSlotEm@PLT
	movq	(%rbx), %r11
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	%r11, -240(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$4, %edx
	movq	-216(%rbp), %xmm0
	movq	-248(%rbp), %r9
	movq	%rax, %rsi
	movq	-240(%rbp), %r11
	movhps	-232(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	%r9, %xmm0
	movq	%r11, %rdi
	movhps	-224(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r9
	cmpq	%r13, -272(%rbp)
	jne	.L503
.L502:
	movq	8(%r14), %rdi
	movq	-256(%rbp), %r14
	movq	%r9, -224(%rbp)
	xorl	%r8d, %r8d
	movq	(%rdi), %rax
	movq	%r14, %rcx
	movq	%r14, %rdx
	movq	%r14, %rsi
	leaq	32(%r14), %r15
	call	*32(%rax)
	movq	8(%r14), %rax
	movq	-216(%rbp), %rdx
	movq	%r14, %rcx
	movq	-224(%rbp), %r9
	movq	%rax, 8(%rdx)
	movzbl	23(%r14), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L504
	movq	32(%r14), %rdi
	movq	%rcx, %rsi
	movq	%r15, %r14
	cmpq	%rdi, %rdx
	je	.L506
.L505:
	leaq	-24(%rsi), %r12
	testq	%rdi, %rdi
	je	.L508
	movq	%r12, %rsi
	movq	%r9, -224(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-224(%rbp), %r9
.L508:
	movq	-216(%rbp), %rax
	movq	%r12, %rsi
	movq	%r9, -224(%rbp)
	movq	%rax, (%r14)
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movq	-256(%rbp), %rax
	movq	-224(%rbp), %r9
	movzbl	23(%rax), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L534
.L506:
	movq	8(%r15), %rdi
	cmpq	%rdi, %r9
	je	.L511
	movq	-256(%rbp), %rsi
	addq	$8, %r15
.L510:
	leaq	-48(%rsi), %r12
	testq	%rdi, %rdi
	je	.L512
	movq	%r12, %rsi
	movq	%r9, -216(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-216(%rbp), %r9
.L512:
	movq	%r9, (%r15)
	testq	%r9, %r9
	je	.L511
	movq	%r12, %rsi
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L511:
	movq	-256(%rbp), %r15
	movl	$2, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler4Node14TrimInputCountEi@PLT
	movq	8(%rbx), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12FinishRegionEv@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	%r15, %rax
	jmp	.L513
	.p2align 4,,10
	.p2align 3
.L497:
	movq	360(%rbx), %rax
	leaq	360(%rax), %rdx
	jmp	.L500
	.p2align 4,,10
	.p2align 3
.L504:
	movq	-256(%rbp), %rax
	movq	32(%rax), %rsi
	movq	16(%rsi), %rdi
	leaq	16(%rsi), %r14
	cmpq	%rdi, -216(%rbp)
	jne	.L505
.L507:
	movq	8(%r14), %rdi
	cmpq	%rdi, %r9
	je	.L511
	leaq	8(%r14), %r15
	jmp	.L510
	.p2align 4,,10
	.p2align 3
.L534:
	movq	-256(%rbp), %rax
	movq	32(%rax), %rsi
	leaq	16(%rsi), %r14
	jmp	.L507
	.p2align 4,,10
	.p2align 3
.L533:
	leaq	.LC15(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L531:
	call	__stack_chk_fail@PLT
.L532:
	leaq	.LC18(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE22987:
	.size	_ZN2v88internal8compiler16JSCreateLowering29ReduceJSCreateFunctionContextEPNS1_4NodeE, .-_ZN2v88internal8compiler16JSCreateLowering29ReduceJSCreateFunctionContextEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler16JSCreateLowering25ReduceJSCreateWithContextEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16JSCreateLowering25ReduceJSCreateWithContextEPNS1_4NodeE
	.type	_ZN2v88internal8compiler16JSCreateLowering25ReduceJSCreateWithContextEPNS1_4NodeE, @function
_ZN2v88internal8compiler16JSCreateLowering25ReduceJSCreateWithContextEPNS1_4NodeE:
.LFB22988:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$248, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler11ScopeInfoOfEPKNS1_8OperatorE@PLT
	leaq	-208(%rbp), %r10
	movq	32(%r14), %rsi
	movl	$1, %ecx
	movq	%r10, %rdi
	movq	%rax, %rdx
	movq	%r10, -288(%rbp)
	call	_ZN2v88internal8compiler12ScopeInfoRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -232(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%r12, %rdi
	movq	%rax, -224(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetContextInputEPNS1_4NodeE@PLT
	movq	24(%r14), %rbx
	movq	32(%r14), %rsi
	movl	$1, %ecx
	movq	%rax, -256(%rbp)
	leaq	-192(%rbp), %rdi
	movq	360(%rbx), %rax
	leaq	400(%rax), %rdx
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movdqa	-192(%rbp), %xmm3
	movl	$1, %esi
	movaps	%xmm3, -176(%rbp)
	movq	8(%rbx), %rdi
	movq	(%rbx), %r13
	call	_ZN2v88internal8compiler21CommonOperatorBuilder11BeginRegionENS1_19RegionObservabilityE@PLT
	movq	%r15, -96(%rbp)
	leaq	-96(%rbp), %r15
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movsd	.LC14(%rip), %xmm0
	movq	%rbx, %rdi
	movq	(%rbx), %r13
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	376(%rbx), %rdi
	xorl	%edx, %edx
	movl	$16777217, %esi
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder8AllocateENS1_4TypeENS0_14AllocationTypeE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$3, %edx
	movq	-240(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r13, %rdi
	movq	-224(%rbp), %rax
	leaq	-160(%rbp), %r13
	movhps	-216(%rbp), %xmm0
	movq	%rax, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r13, %rdi
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	leaq	-176(%rbp), %r9
	movq	%rbx, %rdi
	movq	%r9, %rsi
	movq	%r9, -280(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	(%rbx), %r11
	movq	376(%rbx), %rdi
	movq	%r13, %rsi
	movq	%rax, -240(%rbp)
	movq	%r11, -264(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$4, %edx
	movq	-216(%rbp), %xmm2
	movq	-264(%rbp), %r11
	movq	%rax, %rsi
	movdqa	%xmm2, %xmm0
	movq	%r11, %rdi
	movhps	-240(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movdqa	%xmm2, %xmm0
	movhps	-224(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movsd	.LC21(%rip), %xmm0
	movq	%rbx, %rdi
	movq	%rax, -264(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%r13, %rdi
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder19ForFixedArrayLengthEv@PLT
	movq	(%rbx), %r11
	movq	376(%rbx), %rdi
	movq	%r13, %rsi
	movq	%r11, -272(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$4, %edx
	movq	-216(%rbp), %xmm0
	movq	-272(%rbp), %r11
	movq	%rax, %rsi
	movhps	-240(%rbp), %xmm0
	movq	%r11, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	-264(%rbp), %xmm0
	movhps	-224(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%rax, -264(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder14ForContextSlotEm@PLT
	movq	-288(%rbp), %r10
	movq	%rbx, %rdi
	movq	%r10, %rsi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	(%rbx), %r10
	movq	376(%rbx), %rdi
	movq	%r13, %rsi
	movq	%rax, -240(%rbp)
	movq	%r10, -272(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$4, %edx
	movq	-216(%rbp), %xmm0
	movq	-272(%rbp), %r10
	movq	%rax, %rsi
	movhps	-240(%rbp), %xmm0
	movq	%r10, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	-264(%rbp), %xmm0
	movhps	-224(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder14ForContextSlotEm@PLT
	movq	(%rbx), %r10
	movq	376(%rbx), %rdi
	movq	%r13, %rsi
	movq	%r10, -264(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$4, %edx
	movq	-216(%rbp), %xmm0
	movq	-264(%rbp), %r10
	movq	%rax, %rsi
	movhps	-256(%rbp), %xmm0
	movq	%r10, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	-240(%rbp), %xmm0
	movhps	-224(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$2, %esi
	movq	%r13, %rdi
	movq	%rax, -256(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder14ForContextSlotEm@PLT
	movq	(%rbx), %r10
	movq	376(%rbx), %rdi
	movq	%r13, %rsi
	movq	%r10, -240(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$4, %edx
	movq	-216(%rbp), %xmm0
	movq	-240(%rbp), %r10
	movq	%rax, %rsi
	movhps	-232(%rbp), %xmm0
	movq	%r10, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	-256(%rbp), %xmm0
	movhps	-224(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	24(%r14), %rdi
	movq	-280(%rbp), %r9
	movq	%rax, %xmm0
	movq	32(%r14), %rax
	cmpb	$0, 24(%rax)
	je	.L559
	movdqu	32(%rax), %xmm4
	movhps	-224(%rbp), %xmm0
	movq	%r9, %rsi
	movaps	%xmm0, -256(%rbp)
	movaps	%xmm4, -176(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	%r13, %rdi
	movl	$3, %esi
	movq	%rax, -224(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder14ForContextSlotEm@PLT
	movq	(%rbx), %r9
	movq	376(%rbx), %rdi
	movq	%r13, %rsi
	movq	%r9, -232(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$4, %edx
	movq	-232(%rbp), %r9
	movdqa	-256(%rbp), %xmm0
	movq	%rax, %rsi
	movq	-216(%rbp), %xmm1
	movq	%r9, %rdi
	movaps	%xmm0, -80(%rbp)
	movhps	-224(%rbp), %xmm1
	movaps	%xmm1, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%r14), %rdi
	movq	%r12, %rcx
	movq	%r12, %rdx
	movq	%rax, %r13
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	8(%r12), %rax
	movq	-216(%rbp), %rcx
	leaq	32(%r12), %rdx
	movq	%rax, 8(%rcx)
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L537
	movq	32(%r12), %rdi
	movq	%rdx, %r15
	movq	%r12, %rsi
	cmpq	%rdi, %rcx
	je	.L539
	leaq	-24(%rsi), %r14
	testq	%rdi, %rdi
	je	.L541
.L562:
	movq	%r14, %rsi
	movq	%rdx, -224(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-224(%rbp), %rdx
.L541:
	movq	-216(%rbp), %rax
	movq	%r14, %rsi
	movq	%rdx, -224(%rbp)
	movq	%rax, (%r15)
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movzbl	23(%r12), %eax
	movq	-224(%rbp), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L560
.L539:
	movq	8(%rdx), %rdi
	cmpq	%r13, %rdi
	je	.L544
	leaq	8(%rdx), %r15
	movq	%r12, %rsi
.L543:
	leaq	-48(%rsi), %r14
	testq	%rdi, %rdi
	je	.L545
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L545:
	movq	%r13, (%r15)
	testq	%r13, %r13
	je	.L544
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L544:
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node14TrimInputCountEi@PLT
	movq	8(%rbx), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12FinishRegionEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L561
	addq	$248, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L560:
	.cfi_restore_state
	movq	32(%r12), %rsi
	leaq	16(%rsi), %r15
.L540:
	movq	8(%r15), %rdi
	cmpq	%r13, %rdi
	je	.L544
	addq	$8, %r15
	jmp	.L543
	.p2align 4,,10
	.p2align 3
.L537:
	movq	32(%r12), %rsi
	movq	16(%rsi), %rdi
	leaq	16(%rsi), %r15
	cmpq	-216(%rbp), %rdi
	je	.L540
	leaq	-24(%rsi), %r14
	testq	%rdi, %rdi
	jne	.L562
	jmp	.L541
	.p2align 4,,10
	.p2align 3
.L559:
	leaq	.LC15(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L561:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22988:
	.size	_ZN2v88internal8compiler16JSCreateLowering25ReduceJSCreateWithContextEPNS1_4NodeE, .-_ZN2v88internal8compiler16JSCreateLowering25ReduceJSCreateWithContextEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler16JSCreateLowering26ReduceJSCreateCatchContextEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16JSCreateLowering26ReduceJSCreateCatchContextEPNS1_4NodeE
	.type	_ZN2v88internal8compiler16JSCreateLowering26ReduceJSCreateCatchContextEPNS1_4NodeE, @function
_ZN2v88internal8compiler16JSCreateLowering26ReduceJSCreateCatchContextEPNS1_4NodeE:
.LFB22989:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$264, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler11ScopeInfoOfEPKNS1_8OperatorE@PLT
	leaq	-208(%rbp), %r10
	movq	32(%r14), %rsi
	movl	$1, %ecx
	movq	%r10, %rdi
	movq	%rax, %rdx
	movq	%r10, -296(%rbp)
	call	_ZN2v88internal8compiler12ScopeInfoRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%r12, %rdi
	movq	%rax, -224(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetContextInputEPNS1_4NodeE@PLT
	movq	24(%r14), %rdi
	movq	%rax, -232(%rbp)
	call	_ZN2v88internal8compiler7JSGraph15TheHoleConstantEv@PLT
	movq	24(%r14), %rbx
	movq	32(%r14), %rsi
	movl	$1, %ecx
	movq	%rax, -240(%rbp)
	leaq	-192(%rbp), %rdi
	movq	360(%rbx), %rax
	leaq	392(%rax), %rdx
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movdqa	-192(%rbp), %xmm4
	movl	$1, %esi
	movaps	%xmm4, -176(%rbp)
	movq	8(%rbx), %rdi
	movq	(%rbx), %r13
	call	_ZN2v88internal8compiler21CommonOperatorBuilder11BeginRegionENS1_19RegionObservabilityE@PLT
	movq	%r15, -96(%rbp)
	leaq	-96(%rbp), %r15
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movsd	.LC22(%rip), %xmm0
	movq	%rbx, %rdi
	movq	(%rbx), %r13
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	376(%rbx), %rdi
	xorl	%edx, %edx
	movl	$16777217, %esi
	movq	%rax, -256(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder8AllocateENS1_4TypeENS0_14AllocationTypeE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$3, %edx
	movq	-256(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r13, %rdi
	movq	-224(%rbp), %rax
	leaq	-160(%rbp), %r13
	movhps	-216(%rbp), %xmm0
	movq	%rax, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r13, %rdi
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	leaq	-176(%rbp), %r9
	movq	%rbx, %rdi
	movq	%r9, %rsi
	movq	%r9, -288(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	(%rbx), %r11
	movq	376(%rbx), %rdi
	movq	%r13, %rsi
	movq	%rax, -256(%rbp)
	movq	%r11, -272(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$4, %edx
	movq	-216(%rbp), %xmm3
	movq	-272(%rbp), %r11
	movq	%rax, %rsi
	movdqa	%xmm3, %xmm0
	movq	%r11, %rdi
	movhps	-256(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movdqa	%xmm3, %xmm0
	movhps	-224(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movsd	.LC23(%rip), %xmm0
	movq	%rbx, %rdi
	movq	%rax, -272(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%r13, %rdi
	movq	%rax, -256(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder19ForFixedArrayLengthEv@PLT
	movq	(%rbx), %r11
	movq	376(%rbx), %rdi
	movq	%r13, %rsi
	movq	%r11, -280(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$4, %edx
	movq	-216(%rbp), %xmm0
	movq	-280(%rbp), %r11
	movq	%rax, %rsi
	movhps	-256(%rbp), %xmm0
	movq	%r11, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	-272(%rbp), %xmm0
	movhps	-224(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%rax, -272(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder14ForContextSlotEm@PLT
	movq	-296(%rbp), %r10
	movq	%rbx, %rdi
	movq	%r10, %rsi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	(%rbx), %r10
	movq	376(%rbx), %rdi
	movq	%r13, %rsi
	movq	%rax, -256(%rbp)
	movq	%r10, -280(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$4, %edx
	movq	-216(%rbp), %xmm0
	movq	-280(%rbp), %r10
	movq	%rax, %rsi
	movhps	-256(%rbp), %xmm0
	movq	%r10, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	-272(%rbp), %xmm0
	movhps	-224(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	movq	%rax, -256(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder14ForContextSlotEm@PLT
	movq	(%rbx), %r10
	movq	376(%rbx), %rdi
	movq	%r13, %rsi
	movq	%r10, -272(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$4, %edx
	movq	-216(%rbp), %xmm0
	movq	-272(%rbp), %r10
	movq	%rax, %rsi
	movhps	-232(%rbp), %xmm0
	movq	%r10, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	-256(%rbp), %xmm0
	movhps	-224(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$2, %esi
	movq	%r13, %rdi
	movq	%rax, -232(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder14ForContextSlotEm@PLT
	movq	(%rbx), %r10
	movq	376(%rbx), %rdi
	movq	%r13, %rsi
	movq	%r10, -256(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$4, %edx
	movq	-216(%rbp), %xmm0
	movq	-256(%rbp), %r10
	movq	%rax, %rsi
	movhps	-240(%rbp), %xmm0
	movq	%r10, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	-232(%rbp), %xmm0
	movhps	-224(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	24(%r14), %rdi
	movq	-288(%rbp), %r9
	movq	%rax, %xmm0
	movq	32(%r14), %rax
	cmpb	$0, 24(%rax)
	je	.L587
	movdqu	32(%rax), %xmm5
	movhps	-224(%rbp), %xmm0
	movq	%r9, %rsi
	movq	-216(%rbp), %xmm1
	movaps	%xmm0, -256(%rbp)
	movhps	-304(%rbp), %xmm1
	movaps	%xmm5, -176(%rbp)
	movaps	%xmm1, -272(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	%r13, %rdi
	movl	$3, %esi
	movq	%rax, -232(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder14ForContextSlotEm@PLT
	movq	(%rbx), %r9
	movq	376(%rbx), %rdi
	movq	%r13, %rsi
	movq	%r9, -240(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$4, %edx
	movq	-240(%rbp), %r9
	movdqa	-256(%rbp), %xmm0
	movq	%rax, %rsi
	movq	-216(%rbp), %xmm2
	movq	%r9, %rdi
	movaps	%xmm0, -80(%rbp)
	movhps	-232(%rbp), %xmm2
	movaps	%xmm2, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r13, %rdi
	movl	$4, %esi
	movq	%rax, -232(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder14ForContextSlotEm@PLT
	movq	(%rbx), %r9
	movq	376(%rbx), %rdi
	movq	%r13, %rsi
	movq	%r9, -240(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$4, %edx
	movq	-240(%rbp), %r9
	movdqa	-272(%rbp), %xmm1
	movq	%rax, %rsi
	movq	-232(%rbp), %xmm0
	movq	%r9, %rdi
	movaps	%xmm1, -96(%rbp)
	movhps	-224(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%r14), %rdi
	movq	%r12, %rcx
	movq	%r12, %rdx
	movq	%rax, %r13
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	8(%r12), %rax
	movq	-216(%rbp), %rcx
	leaq	32(%r12), %rdx
	movq	%rax, 8(%rcx)
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L565
	movq	32(%r12), %rdi
	movq	%rdx, %r15
	movq	%r12, %rsi
	cmpq	%rdi, %rcx
	je	.L567
	leaq	-24(%rsi), %r14
	testq	%rdi, %rdi
	je	.L569
.L590:
	movq	%r14, %rsi
	movq	%rdx, -224(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-224(%rbp), %rdx
.L569:
	movq	-216(%rbp), %rax
	movq	%r14, %rsi
	movq	%rdx, -224(%rbp)
	movq	%rax, (%r15)
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movzbl	23(%r12), %eax
	movq	-224(%rbp), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L588
.L567:
	movq	8(%rdx), %rdi
	cmpq	%r13, %rdi
	je	.L572
	leaq	8(%rdx), %r15
	movq	%r12, %rsi
.L571:
	leaq	-48(%rsi), %r14
	testq	%rdi, %rdi
	je	.L573
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L573:
	movq	%r13, (%r15)
	testq	%r13, %r13
	je	.L572
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L572:
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node14TrimInputCountEi@PLT
	movq	8(%rbx), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12FinishRegionEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L589
	addq	$264, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L588:
	.cfi_restore_state
	movq	32(%r12), %rsi
	leaq	16(%rsi), %r15
.L568:
	movq	8(%r15), %rdi
	cmpq	%r13, %rdi
	je	.L572
	addq	$8, %r15
	jmp	.L571
	.p2align 4,,10
	.p2align 3
.L565:
	movq	32(%r12), %rsi
	movq	16(%rsi), %rdi
	leaq	16(%rsi), %r15
	cmpq	-216(%rbp), %rdi
	je	.L568
	leaq	-24(%rsi), %r14
	testq	%rdi, %rdi
	jne	.L590
	jmp	.L569
	.p2align 4,,10
	.p2align 3
.L587:
	leaq	.LC15(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L589:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22989:
	.size	_ZN2v88internal8compiler16JSCreateLowering26ReduceJSCreateCatchContextEPNS1_4NodeE, .-_ZN2v88internal8compiler16JSCreateLowering26ReduceJSCreateCatchContextEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler16JSCreateLowering26ReduceJSCreateBlockContextEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16JSCreateLowering26ReduceJSCreateBlockContextEPNS1_4NodeE
	.type	_ZN2v88internal8compiler16JSCreateLowering26ReduceJSCreateBlockContextEPNS1_4NodeE, @function
_ZN2v88internal8compiler16JSCreateLowering26ReduceJSCreateBlockContextEPNS1_4NodeE:
.LFB22990:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-208(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	subq	$264, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -256(%rbp)
	movq	(%rsi), %rdi
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler11ScopeInfoOfEPKNS1_8OperatorE@PLT
	movq	32(%r14), %rsi
	movl	$1, %ecx
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler12ScopeInfoRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler12ScopeInfoRef13ContextLengthEv@PLT
	movl	%eax, %ecx
	movl	%eax, -232(%rbp)
	xorl	%eax, %eax
	cmpl	$15, %ecx
	jle	.L621
.L605:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L622
	addq	$264, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L621:
	.cfi_restore_state
	movq	-256(%rbp), %rbx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rbx, %rdi
	movq	%rax, -224(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetContextInputEPNS1_4NodeE@PLT
	movq	24(%r14), %rdi
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal8compiler7JSGraph15TheHoleConstantEv@PLT
	movq	24(%r14), %rbx
	movq	32(%r14), %rsi
	movl	$1, %ecx
	movq	%rax, -248(%rbp)
	leaq	-192(%rbp), %rdi
	movq	360(%rbx), %rax
	leaq	384(%rax), %rdx
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movdqa	-192(%rbp), %xmm3
	movl	$1, %esi
	movaps	%xmm3, -176(%rbp)
	movq	8(%rbx), %rdi
	movq	(%rbx), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder11BeginRegionENS1_19RegionObservabilityE@PLT
	movq	%r15, -96(%rbp)
	leaq	-96(%rbp), %r15
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdi
	movq	(%rbx), %r12
	movq	%rax, -216(%rbp)
	movl	-232(%rbp), %eax
	leal	16(,%rax,8), %eax
	cvtsi2sdl	%eax, %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	376(%rbx), %rdi
	xorl	%edx, %edx
	movl	$16777217, %esi
	movq	%rax, -272(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder8AllocateENS1_4TypeENS0_14AllocationTypeE@PLT
	xorl	%r8d, %r8d
	movl	$3, %edx
	movq	%r12, %rdi
	movq	-224(%rbp), %rcx
	leaq	-160(%rbp), %r12
	movq	%rax, %rsi
	movq	-272(%rbp), %xmm0
	movq	%rcx, -80(%rbp)
	movhps	-216(%rbp), %xmm0
	movq	%r15, %rcx
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r12, %rdi
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	leaq	-176(%rbp), %r11
	movq	%rbx, %rdi
	movq	%r11, %rsi
	movq	%r11, -296(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	(%rbx), %r9
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	%rax, -272(%rbp)
	movq	%r9, -280(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$4, %edx
	movq	-216(%rbp), %xmm2
	movq	-280(%rbp), %r9
	movq	%rax, %rsi
	movdqa	%xmm2, %xmm0
	movq	%r9, %rdi
	movhps	-272(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movdqa	%xmm2, %xmm0
	movhps	-224(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdi
	cvtsi2sdl	-232(%rbp), %xmm0
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%r12, %rdi
	movq	%rax, -272(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder19ForFixedArrayLengthEv@PLT
	movq	(%rbx), %r9
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	%r9, -288(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$4, %edx
	movq	-216(%rbp), %xmm0
	movq	-288(%rbp), %r9
	movq	%rax, %rsi
	movhps	-272(%rbp), %xmm0
	movq	%r9, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	-280(%rbp), %xmm0
	movhps	-224(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder14ForContextSlotEm@PLT
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	(%rbx), %r10
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	%rax, -272(%rbp)
	movq	%r10, -288(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$4, %edx
	movq	-216(%rbp), %xmm0
	movq	-288(%rbp), %r10
	movq	%rax, %rsi
	movhps	-272(%rbp), %xmm0
	movq	%r10, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	-280(%rbp), %xmm0
	movhps	-224(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -272(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder14ForContextSlotEm@PLT
	movq	(%rbx), %r10
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	%r10, -280(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$4, %edx
	movq	-216(%rbp), %xmm0
	movq	-280(%rbp), %r10
	movq	%rax, %rsi
	movhps	-240(%rbp), %xmm0
	movq	%r10, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	-272(%rbp), %xmm0
	movhps	-224(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder14ForContextSlotEm@PLT
	movq	(%rbx), %r10
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	%r10, -272(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$4, %edx
	movq	-216(%rbp), %xmm0
	movq	-272(%rbp), %r10
	movq	%rax, %rsi
	movhps	-248(%rbp), %xmm0
	movq	%r10, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	-240(%rbp), %xmm0
	movhps	-224(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	24(%r14), %rdi
	movq	-296(%rbp), %r11
	movq	%rax, %xmm0
	movq	32(%r14), %rax
	cmpb	$0, 24(%rax)
	je	.L623
	movdqu	32(%rax), %xmm4
	movhps	-224(%rbp), %xmm0
	movq	%r11, %rsi
	movaps	%xmm0, -272(%rbp)
	movaps	%xmm4, -176(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder14ForContextSlotEm@PLT
	movq	(%rbx), %r10
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	%r10, -248(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$4, %edx
	movq	-248(%rbp), %r10
	movdqa	-272(%rbp), %xmm0
	movq	%rax, %rsi
	movq	-216(%rbp), %xmm1
	movq	%r10, %rdi
	movaps	%xmm0, -80(%rbp)
	movhps	-240(%rbp), %xmm1
	movaps	%xmm1, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r10
	movl	-232(%rbp), %eax
	cmpl	$4, %eax
	jle	.L594
	subl	$5, %eax
	movl	$4, %r13d
	addq	$5, %rax
	movq	%rax, -272(%rbp)
	.p2align 4,,10
	.p2align 3
.L595:
	movq	24(%r14), %rdi
	movq	%r10, -248(%rbp)
	call	_ZN2v88internal8compiler7JSGraph17UndefinedConstantEv@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	addq	$1, %r13
	movq	%rax, -232(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder14ForContextSlotEm@PLT
	movq	(%rbx), %r11
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	%r11, -240(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$4, %edx
	movq	-216(%rbp), %xmm0
	movq	-248(%rbp), %r10
	movq	%rax, %rsi
	movq	-240(%rbp), %r11
	movhps	-232(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	%r10, %xmm0
	movq	%r11, %rdi
	movhps	-224(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r10
	cmpq	%r13, -272(%rbp)
	jne	.L595
.L594:
	movq	8(%r14), %rdi
	movq	-256(%rbp), %r14
	movq	%r10, -224(%rbp)
	xorl	%r8d, %r8d
	movq	(%rdi), %rax
	movq	%r14, %rcx
	movq	%r14, %rdx
	movq	%r14, %rsi
	leaq	32(%r14), %r15
	call	*32(%rax)
	movq	8(%r14), %rax
	movq	-216(%rbp), %rcx
	movq	%r14, %rdx
	movq	-224(%rbp), %r10
	movq	%rax, 8(%rcx)
	movzbl	23(%r14), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L596
	movq	32(%r14), %rdi
	movq	%rdx, %rsi
	movq	%r15, %r14
	cmpq	%rdi, %rcx
	je	.L598
.L597:
	leaq	-24(%rsi), %r12
	testq	%rdi, %rdi
	je	.L600
	movq	%r12, %rsi
	movq	%r10, -224(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-224(%rbp), %r10
.L600:
	movq	-216(%rbp), %rax
	movq	%r12, %rsi
	movq	%r10, -224(%rbp)
	movq	%rax, (%r14)
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movq	-256(%rbp), %rax
	movq	-224(%rbp), %r10
	movzbl	23(%rax), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L624
.L598:
	movq	8(%r15), %rdi
	cmpq	%rdi, %r10
	je	.L603
	movq	-256(%rbp), %rsi
	addq	$8, %r15
.L602:
	leaq	-48(%rsi), %r12
	testq	%rdi, %rdi
	je	.L604
	movq	%r12, %rsi
	movq	%r10, -216(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-216(%rbp), %r10
.L604:
	movq	%r10, (%r15)
	testq	%r10, %r10
	je	.L603
	movq	%r12, %rsi
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L603:
	movq	-256(%rbp), %r15
	movl	$2, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler4Node14TrimInputCountEi@PLT
	movq	8(%rbx), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12FinishRegionEv@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	%r15, %rax
	jmp	.L605
	.p2align 4,,10
	.p2align 3
.L596:
	movq	-256(%rbp), %rax
	movq	32(%rax), %rsi
	movq	16(%rsi), %rdi
	leaq	16(%rsi), %r14
	cmpq	-216(%rbp), %rdi
	jne	.L597
.L599:
	movq	8(%r14), %rdi
	cmpq	%rdi, %r10
	je	.L603
	leaq	8(%r14), %r15
	jmp	.L602
	.p2align 4,,10
	.p2align 3
.L624:
	movq	-256(%rbp), %rax
	movq	32(%rax), %rsi
	leaq	16(%rsi), %r14
	jmp	.L599
	.p2align 4,,10
	.p2align 3
.L623:
	leaq	.LC15(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L622:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22990:
	.size	_ZN2v88internal8compiler16JSCreateLowering26ReduceJSCreateBlockContextEPNS1_4NodeE, .-_ZN2v88internal8compiler16JSCreateLowering26ReduceJSCreateBlockContextEPNS1_4NodeE
	.section	.rodata._ZN2v88internal8compiler16JSCreateLowering20ReduceJSCreateObjectEPNS1_4NodeE.str1.8,"aMS",@progbits,1
	.align 8
.LC25:
	.string	"!instance_map.IsInobjectSlackTrackingInProgress()"
	.section	.text._ZN2v88internal8compiler16JSCreateLowering20ReduceJSCreateObjectEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16JSCreateLowering20ReduceJSCreateObjectEPNS1_4NodeE
	.type	_ZN2v88internal8compiler16JSCreateLowering20ReduceJSCreateObjectEPNS1_4NodeE, @function
_ZN2v88internal8compiler16JSCreateLowering20ReduceJSCreateObjectEPNS1_4NodeE:
.LFB23004:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$280, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -248(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	8(%rax), %rax
	movq	%rax, -232(%rbp)
	testb	$1, %al
	jne	.L627
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L628
.L627:
	xorl	%eax, %eax
.L630:
	movq	-56(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L653
	addq	$280, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L628:
	.cfi_restore_state
	leaq	-232(%rbp), %rdi
	call	_ZNK2v88internal8compiler4Type14AsHeapConstantEv@PLT
	movq	32(%rbx), %rcx
	movdqu	8(%rax), %xmm0
	movaps	%xmm0, -224(%rbp)
	cmpb	$0, 24(%rcx)
	je	.L635
	movdqu	32(%rcx), %xmm3
	leaq	-192(%rbp), %r15
	leaq	-160(%rbp), %r13
	movq	%rcx, -264(%rbp)
	movq	%r15, %rdi
	movaps	%xmm3, -192(%rbp)
	call	_ZNK2v88internal8compiler16NativeContextRef15object_functionEv@PLT
	movq	%r13, %rdi
	movq	%rax, -160(%rbp)
	movq	%rdx, -152(%rbp)
	call	_ZNK2v88internal8compiler13JSFunctionRef11initial_mapEv@PLT
	movq	%rax, -208(%rbp)
	leaq	-208(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -200(%rbp)
	movq	%rax, -272(%rbp)
	call	_ZNK2v88internal8compiler6MapRef9prototypeEv@PLT
	movq	%r13, %rsi
	movq	%rax, -160(%rbp)
	leaq	-224(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -152(%rbp)
	movq	%rax, -256(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef6equalsERKS2_@PLT
	movq	-264(%rbp), %rcx
	testb	%al, %al
	je	.L632
	movdqa	-208(%rbp), %xmm5
	movzbl	-208(%rbp), %eax
	movups	%xmm5, -184(%rbp)
.L637:
	movb	%al, -184(%rbp)
	movq	24(%rbx), %rdi
	movdqu	-184(%rbp), %xmm6
	movaps	%xmm6, -224(%rbp)
	call	_ZN2v88internal8compiler7JSGraph23EmptyFixedArrayConstantEv@PLT
	movq	-256(%rbp), %rdi
	movq	%rax, -264(%rbp)
	call	_ZNK2v88internal8compiler6MapRef17is_dictionary_mapEv@PLT
	testb	%al, %al
	jne	.L654
.L638:
	movq	-256(%rbp), %rdi
	call	_ZNK2v88internal8compiler6MapRef13instance_sizeEv@PLT
	movl	%eax, -272(%rbp)
	cmpl	$131072, %eax
	jg	.L627
	movq	-256(%rbp), %rdi
	call	_ZNK2v88internal8compiler6MapRef33IsInobjectSlackTrackingInProgressEv@PLT
	testb	%al, %al
	jne	.L655
	movq	24(%rbx), %r15
	movl	$1, %esi
	movq	(%r15), %r9
	movq	8(%r15), %rdi
	movq	%r9, -280(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder11BeginRegionENS1_19RegionObservabilityE@PLT
	movq	-280(%rbp), %r9
	movq	%r14, -96(%rbp)
	leaq	-96(%rbp), %r14
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	pxor	%xmm0, %xmm0
	movq	(%r15), %r9
	movq	%r15, %rdi
	cvtsi2sdl	-272(%rbp), %xmm0
	movq	%rax, -280(%rbp)
	movq	%r9, -288(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	376(%r15), %rdi
	xorl	%edx, %edx
	movl	$4294967295, %esi
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder8AllocateENS1_4TypeENS0_14AllocationTypeE@PLT
	movq	-288(%rbp), %r9
	xorl	%r8d, %r8d
	movq	-248(%rbp), %rcx
	movq	-304(%rbp), %xmm0
	movl	$3, %edx
	movq	%rax, %rsi
	movq	%r9, %rdi
	movq	%rcx, -80(%rbp)
	movq	%r14, %rcx
	movhps	-280(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r13, %rdi
	movq	%rax, -288(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	-256(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	%r13, %rsi
	movq	%rax, -256(%rbp)
	movq	%r15, %rax
	movq	(%r15), %r15
	movq	376(%rax), %rdi
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$4, %edx
	movq	-288(%rbp), %xmm7
	movq	%rax, %rsi
	movq	%r15, %rdi
	movdqa	%xmm7, %xmm0
	movhps	-256(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movdqa	%xmm7, %xmm0
	movhps	-248(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r13, %rdi
	movq	%rax, -256(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder27ForJSObjectPropertiesOrHashEv@PLT
	movq	-280(%rbp), %rax
	movq	%r13, %rsi
	movq	376(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$4, %edx
	movq	-288(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movhps	-264(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-256(%rbp), %xmm0
	movhps	-248(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	24(%rbx), %rdi
	movq	%rax, -264(%rbp)
	call	_ZN2v88internal8compiler7JSGraph23EmptyFixedArrayConstantEv@PLT
	movq	%r13, %rdi
	movq	%rax, -256(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder19ForJSObjectElementsEv@PLT
	movq	-280(%rbp), %rax
	movq	%r13, %rsi
	movq	376(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$4, %edx
	movq	-288(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movhps	-256(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-264(%rbp), %xmm0
	movhps	-248(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	24(%rbx), %rdi
	movq	%rax, -256(%rbp)
	call	_ZN2v88internal8compiler7JSGraph17UndefinedConstantEv@PLT
	cmpl	$24, -272(%rbp)
	movq	%rax, -264(%rbp)
	jle	.L641
	movq	-288(%rbp), %xmm4
	movl	$24, %r15d
	movhps	-264(%rbp), %xmm4
	movaps	%xmm4, -304(%rbp)
	.p2align 4,,10
	.p2align 3
.L642:
	xorl	%edx, %edx
	movl	%r15d, %esi
	movq	%r13, %rdi
	addl	$8, %r15d
	call	_ZN2v88internal8compiler13AccessBuilder17ForJSObjectOffsetEiNS1_16WriteBarrierKindE@PLT
	movq	-280(%rbp), %rax
	movq	%r13, %rsi
	movq	(%rax), %r9
	movq	376(%rax), %rdi
	movq	%r9, -264(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$4, %edx
	movq	-256(%rbp), %xmm0
	movq	-264(%rbp), %r9
	movq	%rax, %rsi
	movdqa	-304(%rbp), %xmm2
	movhps	-248(%rbp), %xmm0
	movq	%r9, %rdi
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -256(%rbp)
	cmpl	%r15d, -272(%rbp)
	jg	.L642
.L641:
	movq	-280(%rbp), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12FinishRegionEv@PLT
	movq	%r13, %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	-288(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movhps	-256(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%rbx), %rdi
	movq	-248(%rbp), %r8
	movq	%r12, %rsi
	movq	%rax, %r13
	movq	(%rdi), %rax
	movq	%r13, %rcx
	movq	%r13, %rdx
	call	*32(%rax)
	movq	%r13, %rax
	jmp	.L630
	.p2align 4,,10
	.p2align 3
.L632:
	movq	-256(%rbp), %rdi
	movq	%rcx, -264(%rbp)
	call	_ZNK2v88internal8compiler13HeapObjectRef3mapEv@PLT
	movq	%r13, %rdi
	movq	%rax, -160(%rbp)
	movq	%rdx, -152(%rbp)
	call	_ZNK2v88internal8compiler6MapRef12oddball_typeEv@PLT
	cmpb	$3, %al
	jne	.L634
	movq	-264(%rbp), %rcx
	cmpb	$0, 24(%rcx)
	je	.L635
	movdqu	32(%rcx), %xmm6
	movq	%r13, %rdi
	movaps	%xmm6, -160(%rbp)
	call	_ZNK2v88internal8compiler16NativeContextRef35slow_object_with_null_prototype_mapEv@PLT
	movq	%rax, -184(%rbp)
	movq	%rdx, -176(%rbp)
	jmp	.L637
	.p2align 4,,10
	.p2align 3
.L634:
	movq	-256(%rbp), %rdi
	call	_ZNK2v88internal8compiler9ObjectRef10IsJSObjectEv@PLT
	testb	%al, %al
	je	.L627
	movq	-256(%rbp), %rdi
	call	_ZNK2v88internal8compiler9ObjectRef10AsJSObjectEv@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, -160(%rbp)
	movq	%rdx, -152(%rbp)
	call	_ZNK2v88internal8compiler11JSObjectRef18GetObjectCreateMapEv@PLT
	cmpb	$0, -192(%rbp)
	movzbl	-184(%rbp), %eax
	je	.L627
	jmp	.L637
	.p2align 4,,10
	.p2align 3
.L635:
	leaq	.LC15(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L654:
	movq	24(%rbx), %rax
	movq	32(%rbx), %rsi
	movl	$1, %ecx
	movq	-272(%rbp), %rdi
	movq	360(%rax), %rdx
	addq	$520, %rdx
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movl	$3, %edi
	call	_ZN2v84base4bits21RoundUpToPowerOfTwo32Ej@PLT
	movq	24(%rbx), %r15
	movl	$4, %r10d
	movl	$1, %esi
	cmpl	$4, %eax
	cmovge	%eax, %r10d
	movq	(%r15), %r11
	movq	8(%r15), %rdi
	leal	(%r10,%r10,2), %eax
	movl	%r10d, -320(%rbp)
	movq	%r11, -264(%rbp)
	movl	%eax, -280(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder11BeginRegionENS1_19RegionObservabilityE@PLT
	movq	-264(%rbp), %r11
	movq	%r14, -96(%rbp)
	leaq	-96(%rbp), %r14
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r11, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	pxor	%xmm0, %xmm0
	movq	(%r15), %r11
	movq	%r15, %rdi
	movq	%rax, -264(%rbp)
	movl	-280(%rbp), %eax
	movq	%r11, -288(%rbp)
	leal	56(,%rax,8), %eax
	cvtsi2sdl	%eax, %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	376(%r15), %rdi
	xorl	%edx, %edx
	movl	$4294967295, %esi
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder8AllocateENS1_4TypeENS0_14AllocationTypeE@PLT
	movq	-288(%rbp), %r11
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	-248(%rbp), %rdx
	movq	%rax, %rsi
	movq	-304(%rbp), %xmm0
	movq	%r11, %rdi
	movhps	-264(%rbp), %xmm0
	movq	%rdx, -80(%rbp)
	movl	$3, %edx
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r13, %rdi
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	-272(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	(%r15), %r11
	movq	376(%r15), %rdi
	movq	%r13, %rsi
	movq	%rax, -264(%rbp)
	movq	%r11, -272(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$4, %edx
	movq	-304(%rbp), %xmm4
	movq	-272(%rbp), %r11
	movq	%rax, %rsi
	movdqa	%xmm4, %xmm0
	movq	%r11, %rdi
	movhps	-264(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movdqa	%xmm4, %xmm0
	movhps	-248(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	pxor	%xmm0, %xmm0
	movq	24(%rbx), %rdi
	movq	%rax, -272(%rbp)
	movl	-280(%rbp), %eax
	addl	$5, %eax
	cvtsi2sdl	%eax, %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%r13, %rdi
	movq	%rax, -264(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder19ForFixedArrayLengthEv@PLT
	movq	(%r15), %r11
	movq	376(%r15), %rdi
	movq	%r13, %rsi
	movq	%r11, -288(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$4, %edx
	movq	-304(%rbp), %xmm0
	movq	-288(%rbp), %r11
	movq	%rax, %rsi
	movhps	-264(%rbp), %xmm0
	movq	%r11, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	-272(%rbp), %xmm0
	movhps	-248(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	24(%rbx), %rdi
	pxor	%xmm0, %xmm0
	movq	%rax, -272(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%r13, %rdi
	movq	%rax, -264(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder32ForHashTableBaseNumberOfElementsEv@PLT
	movq	(%r15), %r11
	movq	376(%r15), %rdi
	movq	%r13, %rsi
	movq	%r11, -288(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$4, %edx
	movq	-304(%rbp), %xmm0
	movq	-288(%rbp), %r11
	movq	%rax, %rsi
	movhps	-264(%rbp), %xmm0
	movq	%r11, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	-272(%rbp), %xmm0
	movhps	-248(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	24(%rbx), %rdi
	pxor	%xmm0, %xmm0
	movq	%rax, -272(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%r13, %rdi
	movq	%rax, -264(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder38ForHashTableBaseNumberOfDeletedElementEv@PLT
	movq	(%r15), %r11
	movq	376(%r15), %rdi
	movq	%r13, %rsi
	movq	%r11, -288(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$4, %edx
	movq	-304(%rbp), %xmm0
	movq	-288(%rbp), %r11
	movq	%rax, %rsi
	movhps	-264(%rbp), %xmm0
	movq	%r11, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	-272(%rbp), %xmm0
	movhps	-248(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	-320(%rbp), %r10d
	pxor	%xmm0, %xmm0
	movq	24(%rbx), %rdi
	movq	%rax, -272(%rbp)
	cvtsi2sdl	%r10d, %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%r13, %rdi
	movq	%rax, -264(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder24ForHashTableBaseCapacityEv@PLT
	movq	(%r15), %r10
	movq	376(%r15), %rdi
	movq	%r13, %rsi
	movq	%r10, -288(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$4, %edx
	movq	-304(%rbp), %xmm0
	movq	-288(%rbp), %r10
	movq	%rax, %rsi
	movhps	-264(%rbp), %xmm0
	movq	%r10, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	-272(%rbp), %xmm0
	movhps	-248(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movsd	.LC24(%rip), %xmm0
	movq	24(%rbx), %rdi
	movq	%rax, -272(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%r13, %rdi
	movq	%rax, -264(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder33ForDictionaryNextEnumerationIndexEv@PLT
	movq	(%r15), %r10
	movq	376(%r15), %rdi
	movq	%r13, %rsi
	movq	%r10, -288(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$4, %edx
	movq	-304(%rbp), %xmm0
	movq	-288(%rbp), %r10
	movq	%rax, %rsi
	movhps	-264(%rbp), %xmm0
	movq	%r10, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	-272(%rbp), %xmm0
	movhps	-248(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	24(%rbx), %rdi
	pxor	%xmm0, %xmm0
	movq	%rax, -272(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%r13, %rdi
	movq	%rax, -264(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder28ForDictionaryObjectHashIndexEv@PLT
	movq	(%r15), %r10
	movq	376(%r15), %rdi
	movq	%r13, %rsi
	movq	%r10, -288(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	movq	%r14, %rcx
	movl	$4, %edx
	xorl	%r8d, %r8d
	movq	-304(%rbp), %xmm0
	movq	-288(%rbp), %r10
	movq	%rax, %rsi
	movhps	-264(%rbp), %xmm0
	movq	%r10, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	-272(%rbp), %xmm0
	movhps	-248(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	24(%rbx), %rdi
	movq	%rax, -264(%rbp)
	call	_ZN2v88internal8compiler7JSGraph17UndefinedConstantEv@PLT
	movl	-280(%rbp), %ecx
	movq	-304(%rbp), %xmm7
	movl	$5, %r10d
	movq	%rax, %xmm4
	leal	-1(%rcx), %edx
	punpcklqdq	%xmm4, %xmm7
	leaq	6(%rdx), %rcx
	movaps	%xmm7, -320(%rbp)
	movq	%rcx, -288(%rbp)
	.p2align 4,,10
	.p2align 3
.L639:
	movq	%r10, %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%r10, -280(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder17ForFixedArraySlotEmNS1_16WriteBarrierKindE@PLT
	movq	(%r15), %r11
	movq	376(%r15), %rdi
	movq	%r13, %rsi
	movq	%r11, -272(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$4, %edx
	movq	-264(%rbp), %xmm0
	movq	-272(%rbp), %r11
	movq	%rax, %rsi
	movdqa	-320(%rbp), %xmm1
	movhps	-248(%rbp), %xmm0
	movq	%r11, %rdi
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-280(%rbp), %r10
	movq	%rax, -264(%rbp)
	addq	$1, %r10
	cmpq	%r10, -288(%rbp)
	jne	.L639
	movq	(%r15), %r10
	movq	8(%r15), %rdi
	movq	%r10, -272(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12FinishRegionEv@PLT
	movq	%r14, %rcx
	xorl	%r8d, %r8d
	movl	$2, %edx
	movq	-304(%rbp), %xmm0
	movq	-272(%rbp), %r10
	movq	%rax, %rsi
	movhps	-264(%rbp), %xmm0
	movq	%r10, %rdi
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -264(%rbp)
	movq	%rax, %r14
	jmp	.L638
.L655:
	leaq	.LC25(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L653:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23004:
	.size	_ZN2v88internal8compiler16JSCreateLowering20ReduceJSCreateObjectEPNS1_4NodeE, .-_ZN2v88internal8compiler16JSCreateLowering20ReduceJSCreateObjectEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler16JSCreateLowering17AllocateArgumentsEPNS1_4NodeES4_S4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16JSCreateLowering17AllocateArgumentsEPNS1_4NodeES4_S4_
	.type	_ZN2v88internal8compiler16JSCreateLowering17AllocateArgumentsEPNS1_4NodeES4_S4_, @function
_ZN2v88internal8compiler16JSCreateLowering17AllocateArgumentsEPNS1_4NodeES4_S4_:
.LFB23005:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$584, %rsp
	movq	%rdi, -616(%rbp)
	movq	(%rcx), %rdi
	movq	%rdx, -608(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler16FrameStateInfoOfEPKNS1_8OperatorE@PLT
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L664
	movl	4(%rax), %eax
	movl	%eax, %ecx
	subl	$1, %ecx
	movl	%ecx, -620(%rbp)
	je	.L667
	leal	8(,%rax,8), %eax
	movl	%eax, -576(%rbp)
.L657:
	movzbl	23(%r12), %eax
	movq	32(%r12), %rsi
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L660
	movq	16(%rsi), %rsi
.L660:
	leaq	-256(%rbp), %r15
	leaq	-544(%rbp), %r14
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler17StateValuesAccess8iteratorC1EPNS1_4NodeE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler17StateValuesAccess8iteratorppEv@PLT
	movl	$1, %ecx
	leaq	-560(%rbp), %rdi
	movdqu	(%rax), %xmm2
	movaps	%xmm2, -464(%rbp)
	movdqu	16(%rax), %xmm3
	movaps	%xmm3, -448(%rbp)
	movdqu	32(%rax), %xmm4
	movaps	%xmm4, -432(%rbp)
	movdqu	48(%rax), %xmm5
	movaps	%xmm5, -416(%rbp)
	movdqu	64(%rax), %xmm6
	movaps	%xmm6, -400(%rbp)
	movdqu	80(%rax), %xmm7
	movaps	%xmm7, -384(%rbp)
	movdqu	96(%rax), %xmm2
	movaps	%xmm2, -368(%rbp)
	movdqu	112(%rax), %xmm3
	movaps	%xmm3, -352(%rbp)
	movdqu	128(%rax), %xmm4
	movaps	%xmm4, -336(%rbp)
	movdqu	144(%rax), %xmm5
	movaps	%xmm5, -320(%rbp)
	movdqu	160(%rax), %xmm6
	movaps	%xmm6, -304(%rbp)
	movdqu	176(%rax), %xmm7
	movaps	%xmm7, -288(%rbp)
	movq	192(%rax), %rax
	movq	%rax, -272(%rbp)
	movq	-616(%rbp), %rax
	movq	24(%rax), %r12
	movq	32(%rax), %rsi
	movq	360(%r12), %rdx
	addq	$152, %rdx
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movdqa	-560(%rbp), %xmm1
	movq	%r14, %rdi
	movaps	%xmm1, -544(%rbp)
	call	_ZNK2v88internal8compiler6MapRef13instance_typeEv@PLT
	movq	8(%r12), %rdi
	movq	(%r12), %r13
	movl	$1, %esi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder11BeginRegionENS1_19RegionObservabilityE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	movq	%rbx, -256(%rbp)
	leaq	-528(%rbp), %r13
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movq	(%r12), %rbx
	cvtsi2sdl	-576(%rbp), %xmm0
	movq	%rax, -568(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	376(%r12), %rdi
	xorl	%edx, %edx
	movl	$16777217, %esi
	movq	%rax, -576(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder8AllocateENS1_4TypeENS0_14AllocationTypeE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$3, %edx
	movq	-576(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%rbx, %rdi
	movq	-608(%rbp), %rax
	movhps	-568(%rbp), %xmm0
	movq	%rax, -240(%rbp)
	movaps	%xmm0, -256(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r13, %rdi
	movq	%rax, -600(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	376(%r12), %rdi
	movq	%r13, %rsi
	movq	(%r12), %r14
	movq	%rax, -568(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$4, %edx
	movq	-600(%rbp), %xmm1
	movq	%rax, %rsi
	movq	%r14, %rdi
	movdqa	%xmm1, %xmm0
	movhps	-568(%rbp), %xmm0
	movaps	%xmm0, -256(%rbp)
	movdqa	%xmm1, %xmm0
	movhps	-608(%rbp), %xmm0
	movaps	%xmm0, -240(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	-620(%rbp), %ebx
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movq	%rax, -576(%rbp)
	cvtsi2sdl	%ebx, %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%r13, %rdi
	movq	%rax, -568(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder19ForFixedArrayLengthEv@PLT
	movq	376(%r12), %rdi
	movq	%r13, %rsi
	movq	(%r12), %r14
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$4, %edx
	movq	-600(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r14, %rdi
	movhps	-568(%rbp), %xmm0
	movaps	%xmm0, -256(%rbp)
	movq	-576(%rbp), %xmm0
	movhps	-608(%rbp), %xmm0
	movaps	%xmm0, -240(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -568(%rbp)
	testl	%ebx, %ebx
	jle	.L661
	xorl	%ebx, %ebx
	leaq	-464(%rbp), %r14
	.p2align 4,,10
	.p2align 3
.L662:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler17StateValuesAccess8iteratordeEv@PLT
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%ebx, %xmm0
	movq	%rax, -584(%rbp)
	addl	$1, %ebx
	movq	-616(%rbp), %rax
	movq	24(%rax), %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%r13, %rdi
	movq	%rax, -576(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder20ForFixedArrayElementEv@PLT
	movq	(%r12), %r9
	movq	376(%r12), %rdi
	movq	%r13, %rsi
	movq	%r9, -592(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder12StoreElementERKNS1_13ElementAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$5, %edx
	movq	-600(%rbp), %xmm0
	movq	-592(%rbp), %r9
	movq	%rax, %rsi
	movq	-608(%rbp), %rax
	movhps	-576(%rbp), %xmm0
	movq	%r9, %rdi
	movaps	%xmm0, -256(%rbp)
	movq	-584(%rbp), %xmm0
	movq	%rax, -224(%rbp)
	movhps	-568(%rbp), %xmm0
	movaps	%xmm0, -240(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r14, %rdi
	movq	%rax, -568(%rbp)
	call	_ZN2v88internal8compiler17StateValuesAccess8iteratorppEv@PLT
	cmpl	-620(%rbp), %ebx
	jne	.L662
.L661:
	movq	8(%r12), %rdi
	movq	(%r12), %r13
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12FinishRegionEv@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$2, %edx
	movq	-600(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r13, %rdi
	movhps	-568(%rbp), %xmm0
	movaps	%xmm0, -256(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
.L656:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L668
	addq	$584, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L667:
	.cfi_restore_state
	movq	24(%r15), %rdi
	call	_ZN2v88internal8compiler7JSGraph23EmptyFixedArrayConstantEv@PLT
	jmp	.L656
	.p2align 4,,10
	.p2align 3
.L664:
	movl	$8, -576(%rbp)
	movl	$-1, -620(%rbp)
	jmp	.L657
.L668:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23005:
	.size	_ZN2v88internal8compiler16JSCreateLowering17AllocateArgumentsEPNS1_4NodeES4_S4_, .-_ZN2v88internal8compiler16JSCreateLowering17AllocateArgumentsEPNS1_4NodeES4_S4_
	.section	.text._ZN2v88internal8compiler16JSCreateLowering21AllocateRestArgumentsEPNS1_4NodeES4_S4_i,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16JSCreateLowering21AllocateRestArgumentsEPNS1_4NodeES4_S4_i
	.type	_ZN2v88internal8compiler16JSCreateLowering21AllocateRestArgumentsEPNS1_4NodeES4_S4_i, @function
_ZN2v88internal8compiler16JSCreateLowering21AllocateRestArgumentsEPNS1_4NodeES4_S4_i:
.LFB23006:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	subq	$584, %rsp
	movq	%rdi, -624(%rbp)
	movq	(%rcx), %rdi
	movq	%rdx, -608(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler16FrameStateInfoOfEPKNS1_8OperatorE@PLT
	movq	16(%rax), %rdx
	movl	$-1, %eax
	testq	%rdx, %rdx
	je	.L670
	movl	4(%rdx), %eax
	subl	$1, %eax
.L670:
	subl	%ebx, %eax
	movl	%eax, -612(%rbp)
	testl	%eax, %eax
	jg	.L671
	movq	-624(%rbp), %rax
	movq	24(%rax), %rdi
	call	_ZN2v88internal8compiler7JSGraph23EmptyFixedArrayConstantEv@PLT
.L669:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L685
	addq	$584, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L671:
	.cfi_restore_state
	movzbl	23(%r12), %eax
	movq	32(%r12), %rsi
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L686
.L673:
	leaq	-256(%rbp), %r15
	xorl	%r12d, %r12d
	leaq	-464(%rbp), %r13
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler17StateValuesAccess8iteratorC1EPNS1_4NodeE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler17StateValuesAccess8iteratorppEv@PLT
	movdqu	(%rax), %xmm2
	movaps	%xmm2, -464(%rbp)
	movdqu	16(%rax), %xmm3
	movaps	%xmm3, -448(%rbp)
	movdqu	32(%rax), %xmm4
	movaps	%xmm4, -432(%rbp)
	movdqu	48(%rax), %xmm5
	movaps	%xmm5, -416(%rbp)
	movdqu	64(%rax), %xmm6
	movaps	%xmm6, -400(%rbp)
	movdqu	80(%rax), %xmm7
	movaps	%xmm7, -384(%rbp)
	movdqu	96(%rax), %xmm2
	movaps	%xmm2, -368(%rbp)
	movdqu	112(%rax), %xmm3
	movaps	%xmm3, -352(%rbp)
	movdqu	128(%rax), %xmm4
	movaps	%xmm4, -336(%rbp)
	movdqu	144(%rax), %xmm5
	movaps	%xmm5, -320(%rbp)
	movdqu	160(%rax), %xmm6
	movaps	%xmm6, -304(%rbp)
	movdqu	176(%rax), %xmm7
	movaps	%xmm7, -288(%rbp)
	movq	192(%rax), %rax
	movq	%rax, -272(%rbp)
	testl	%ebx, %ebx
	jle	.L676
	.p2align 4,,10
	.p2align 3
.L674:
	movq	%r13, %rdi
	addl	$1, %r12d
	call	_ZN2v88internal8compiler17StateValuesAccess8iteratorppEv@PLT
	cmpl	%r12d, %ebx
	jne	.L674
.L676:
	movq	-624(%rbp), %rax
	leaq	-560(%rbp), %rdi
	leaq	-544(%rbp), %rbx
	movq	24(%rax), %r12
	movq	32(%rax), %rsi
	movq	360(%r12), %rcx
	leaq	152(%rcx), %rdx
	movl	$1, %ecx
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movdqa	-560(%rbp), %xmm1
	movq	%rbx, %rdi
	movaps	%xmm1, -544(%rbp)
	call	_ZNK2v88internal8compiler6MapRef13instance_typeEv@PLT
	movq	(%r12), %r9
	movq	8(%r12), %rdi
	movl	$1, %esi
	movq	%r9, -568(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder11BeginRegionENS1_19RegionObservabilityE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$1, %edx
	movq	-568(%rbp), %r9
	movq	%rax, %rsi
	movq	%r14, -256(%rbp)
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movq	(%r12), %r14
	movq	%rax, -568(%rbp)
	movl	-612(%rbp), %eax
	leal	16(,%rax,8), %eax
	cvtsi2sdl	%eax, %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	376(%r12), %rdi
	xorl	%edx, %edx
	movl	$16777217, %esi
	movq	%rax, -576(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder8AllocateENS1_4TypeENS0_14AllocationTypeE@PLT
	xorl	%r8d, %r8d
	movl	$3, %edx
	movq	%r14, %rdi
	movq	-608(%rbp), %rcx
	leaq	-528(%rbp), %r14
	movq	%rax, %rsi
	movq	-576(%rbp), %xmm0
	movq	%rcx, -240(%rbp)
	movhps	-568(%rbp), %xmm0
	movq	%r15, %rcx
	movaps	%xmm0, -256(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r14, %rdi
	movq	%rax, -600(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	376(%r12), %rdi
	movq	%r14, %rsi
	movq	(%r12), %rbx
	movq	%rax, -568(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$4, %edx
	movq	-600(%rbp), %xmm1
	movq	%rax, %rsi
	movq	%rbx, %rdi
	movdqa	%xmm1, %xmm0
	movhps	-568(%rbp), %xmm0
	movaps	%xmm0, -256(%rbp)
	movdqa	%xmm1, %xmm0
	movhps	-608(%rbp), %xmm0
	movaps	%xmm0, -240(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	cvtsi2sdl	-612(%rbp), %xmm0
	movq	%rax, -576(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%r14, %rdi
	movq	%rax, -568(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder19ForFixedArrayLengthEv@PLT
	movq	376(%r12), %rdi
	movq	%r14, %rsi
	movq	(%r12), %rbx
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	movq	%rbx, %rdi
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	-600(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$4, %edx
	xorl	%ebx, %ebx
	movhps	-568(%rbp), %xmm0
	movaps	%xmm0, -256(%rbp)
	movq	-576(%rbp), %xmm0
	movhps	-608(%rbp), %xmm0
	movaps	%xmm0, -240(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -568(%rbp)
	.p2align 4,,10
	.p2align 3
.L675:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler17StateValuesAccess8iteratordeEv@PLT
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%ebx, %xmm0
	movq	%rax, -584(%rbp)
	addl	$1, %ebx
	movq	-624(%rbp), %rax
	movq	24(%rax), %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%r14, %rdi
	movq	%rax, -576(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder20ForFixedArrayElementEv@PLT
	movq	(%r12), %r9
	movq	376(%r12), %rdi
	movq	%r14, %rsi
	movq	%r9, -592(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder12StoreElementERKNS1_13ElementAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$5, %edx
	movq	-600(%rbp), %xmm0
	movq	-592(%rbp), %r9
	movq	%rax, %rsi
	movq	-608(%rbp), %rax
	movhps	-576(%rbp), %xmm0
	movq	%r9, %rdi
	movaps	%xmm0, -256(%rbp)
	movq	-584(%rbp), %xmm0
	movq	%rax, -224(%rbp)
	movhps	-568(%rbp), %xmm0
	movaps	%xmm0, -240(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r13, %rdi
	movq	%rax, -568(%rbp)
	call	_ZN2v88internal8compiler17StateValuesAccess8iteratorppEv@PLT
	cmpl	%ebx, -612(%rbp)
	jne	.L675
	movq	8(%r12), %rdi
	movq	(%r12), %r13
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12FinishRegionEv@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$2, %edx
	movq	-600(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r13, %rdi
	movhps	-568(%rbp), %xmm0
	movaps	%xmm0, -256(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	jmp	.L669
	.p2align 4,,10
	.p2align 3
.L686:
	movq	16(%rsi), %rsi
	jmp	.L673
.L685:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23006:
	.size	_ZN2v88internal8compiler16JSCreateLowering21AllocateRestArgumentsEPNS1_4NodeES4_S4_i, .-_ZN2v88internal8compiler16JSCreateLowering21AllocateRestArgumentsEPNS1_4NodeES4_S4_i
	.section	.text._ZN2v88internal8compiler16JSCreateLowering24AllocateAliasedArgumentsEPNS1_4NodeES4_S4_S4_RKNS1_21SharedFunctionInfoRefEPb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16JSCreateLowering24AllocateAliasedArgumentsEPNS1_4NodeES4_S4_S4_RKNS1_21SharedFunctionInfoRefEPb
	.type	_ZN2v88internal8compiler16JSCreateLowering24AllocateAliasedArgumentsEPNS1_4NodeES4_S4_S4_RKNS1_21SharedFunctionInfoRefEPb, @function
_ZN2v88internal8compiler16JSCreateLowering24AllocateAliasedArgumentsEPNS1_4NodeES4_S4_S4_RKNS1_21SharedFunctionInfoRefEPb:
.LFB23007:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r9, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$616, %rsp
	.cfi_offset 3, -56
	movq	(%rcx), %rdi
	movq	16(%rbp), %rbx
	movq	%rdx, -568(%rbp)
	movq	%r8, -640(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler16FrameStateInfoOfEPKNS1_8OperatorE@PLT
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L699
	movl	4(%rax), %r9d
	movl	%r9d, %eax
	subl	$1, %eax
	movl	%eax, -616(%rbp)
	jne	.L688
	movq	24(%r12), %rdi
	call	_ZN2v88internal8compiler7JSGraph23EmptyFixedArrayConstantEv@PLT
.L687:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L706
	addq	$616, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L699:
	.cfi_restore_state
	movl	$-1, -616(%rbp)
	xorl	%r9d, %r9d
.L688:
	movq	%r13, %rdi
	movl	%r9d, -576(%rbp)
	call	_ZNK2v88internal8compiler21SharedFunctionInfoRef31internal_formal_parameter_countEv@PLT
	movl	-576(%rbp), %r9d
	testl	%eax, %eax
	movl	%eax, -644(%rbp)
	je	.L707
	movl	%eax, %ecx
	movl	-616(%rbp), %eax
	movb	$1, (%rbx)
	leaq	32(%r15), %rdx
	cmpl	%eax, %ecx
	cmovle	%ecx, %eax
	movl	%eax, -612(%rbp)
	movzbl	23(%r15), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L691
	movq	32(%r15), %rdx
	addq	$16, %rdx
.L691:
	leaq	-256(%rbp), %r15
	movq	(%rdx), %rsi
	movl	%r9d, -584(%rbp)
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler17StateValuesAccess8iteratorC1EPNS1_4NodeE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler17StateValuesAccess8iteratorppEv@PLT
	movq	24(%r12), %r13
	movq	32(%r12), %rsi
	movl	$1, %ecx
	movdqu	(%rax), %xmm3
	movaps	%xmm3, -464(%rbp)
	movdqu	16(%rax), %xmm4
	movaps	%xmm4, -448(%rbp)
	movdqu	32(%rax), %xmm5
	movaps	%xmm5, -432(%rbp)
	movdqu	48(%rax), %xmm6
	movaps	%xmm6, -416(%rbp)
	movdqu	64(%rax), %xmm7
	movaps	%xmm7, -400(%rbp)
	movdqu	80(%rax), %xmm3
	movaps	%xmm3, -384(%rbp)
	movdqu	96(%rax), %xmm4
	movaps	%xmm4, -368(%rbp)
	movdqu	112(%rax), %xmm5
	movaps	%xmm5, -352(%rbp)
	movdqu	128(%rax), %xmm6
	movaps	%xmm6, -336(%rbp)
	movdqu	144(%rax), %xmm7
	movaps	%xmm7, -320(%rbp)
	movdqu	160(%rax), %xmm1
	movaps	%xmm1, -304(%rbp)
	movdqu	176(%rax), %xmm2
	movaps	%xmm2, -288(%rbp)
	movq	192(%rax), %rax
	movq	%rax, -272(%rbp)
	movq	360(%r13), %rax
	leaq	152(%rax), %rdx
	leaq	-560(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -656(%rbp)
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movdqa	-560(%rbp), %xmm3
	leaq	-544(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -632(%rbp)
	movaps	%xmm3, -544(%rbp)
	call	_ZNK2v88internal8compiler6MapRef13instance_typeEv@PLT
	movq	8(%r13), %rdi
	movq	0(%r13), %rbx
	movl	$1, %esi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder11BeginRegionENS1_19RegionObservabilityE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%rbx, %rdi
	movq	%r14, -256(%rbp)
	leaq	-528(%rbp), %rbx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rdi
	movq	0(%r13), %r14
	movl	-584(%rbp), %r9d
	movq	%rax, -576(%rbp)
	leal	8(,%r9,8), %eax
	cvtsi2sdl	%eax, %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	376(%r13), %rdi
	xorl	%edx, %edx
	movl	$16777217, %esi
	movq	%rax, -584(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder8AllocateENS1_4TypeENS0_14AllocationTypeE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$3, %edx
	movq	-584(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r14, %rdi
	movq	-568(%rbp), %rax
	movhps	-576(%rbp), %xmm0
	movq	%rax, -240(%rbp)
	movaps	%xmm0, -256(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rbx, %rdi
	movq	%rax, -584(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	-632(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	376(%r13), %rdi
	movq	%rbx, %rsi
	movq	0(%r13), %r14
	movq	%rax, -576(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$4, %edx
	movq	-584(%rbp), %xmm2
	movq	%rax, %rsi
	movq	%r14, %rdi
	movdqa	%xmm2, %xmm0
	movhps	-576(%rbp), %xmm0
	movaps	%xmm0, -256(%rbp)
	movdqa	%xmm2, %xmm0
	movhps	-568(%rbp), %xmm0
	movaps	%xmm0, -240(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rdi
	cvtsi2sdl	-616(%rbp), %xmm0
	movq	%rax, -592(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%rbx, %rdi
	movq	%rax, -576(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder19ForFixedArrayLengthEv@PLT
	movq	376(%r13), %rdi
	movq	%rbx, %rsi
	movq	0(%r13), %r14
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	movq	%r14, %rdi
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	-584(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$4, %edx
	xorl	%r14d, %r14d
	movhps	-576(%rbp), %xmm0
	movaps	%xmm0, -256(%rbp)
	movq	-592(%rbp), %xmm0
	movhps	-568(%rbp), %xmm0
	movaps	%xmm0, -240(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -576(%rbp)
	leaq	-464(%rbp), %rax
	movq	%rax, -624(%rbp)
	movl	-612(%rbp), %eax
	testl	%eax, %eax
	jle	.L695
	movq	%r15, -608(%rbp)
	.p2align 4,,10
	.p2align 3
.L692:
	movq	24(%r12), %rdi
	call	_ZN2v88internal8compiler7JSGraph15TheHoleConstantEv@PLT
	pxor	%xmm0, %xmm0
	movq	24(%r12), %rdi
	cvtsi2sdl	%r14d, %xmm0
	movq	%rax, -592(%rbp)
	addl	$1, %r14d
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%rbx, %rdi
	movq	%rax, -600(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder20ForFixedArrayElementEv@PLT
	movq	376(%r13), %rdi
	movq	%rbx, %rsi
	movq	0(%r13), %r15
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder12StoreElementERKNS1_13ElementAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movl	$5, %edx
	movq	-584(%rbp), %xmm0
	movq	%rax, %rsi
	movq	-608(%rbp), %rcx
	movq	-568(%rbp), %rax
	movhps	-600(%rbp), %xmm0
	movaps	%xmm0, -256(%rbp)
	movq	-592(%rbp), %xmm0
	movq	%rax, -224(%rbp)
	movhps	-576(%rbp), %xmm0
	movaps	%xmm0, -240(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-624(%rbp), %rdi
	movq	%rax, -576(%rbp)
	call	_ZN2v88internal8compiler17StateValuesAccess8iteratorppEv@PLT
	cmpl	-612(%rbp), %r14d
	jne	.L692
	movq	-608(%rbp), %r15
.L695:
	movl	-612(%rbp), %eax
	movl	-616(%rbp), %ecx
	cmpl	%ecx, %eax
	jge	.L693
	movl	%eax, %r14d
	leaq	-464(%rbp), %rax
	movq	%r15, -624(%rbp)
	movq	%rax, %r15
	.p2align 4,,10
	.p2align 3
.L694:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler17StateValuesAccess8iteratordeEv@PLT
	pxor	%xmm0, %xmm0
	movq	24(%r12), %rdi
	cvtsi2sdl	%r14d, %xmm0
	movq	%rax, -600(%rbp)
	addl	$1, %r14d
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%rbx, %rdi
	movq	%rax, -592(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder20ForFixedArrayElementEv@PLT
	movq	0(%r13), %r9
	movq	376(%r13), %rdi
	movq	%rbx, %rsi
	movq	%r9, -608(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder12StoreElementERKNS1_13ElementAccessE@PLT
	movq	-608(%rbp), %r9
	xorl	%r8d, %r8d
	movq	-584(%rbp), %xmm0
	movq	%rax, %rsi
	movq	-624(%rbp), %rcx
	movq	-568(%rbp), %rax
	movl	$5, %edx
	movhps	-592(%rbp), %xmm0
	movq	%r9, %rdi
	movaps	%xmm0, -256(%rbp)
	movq	-600(%rbp), %xmm0
	movq	%rax, -224(%rbp)
	movhps	-576(%rbp), %xmm0
	movaps	%xmm0, -240(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r15, %rdi
	movq	%rax, -576(%rbp)
	call	_ZN2v88internal8compiler17StateValuesAccess8iteratorppEv@PLT
	cmpl	-616(%rbp), %r14d
	jne	.L694
	movq	-624(%rbp), %r15
.L693:
	movq	8(%r13), %rdi
	movq	0(%r13), %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12FinishRegionEv@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movq	%r15, %rcx
	movq	-584(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movhps	-576(%rbp), %xmm0
	movaps	%xmm0, -256(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	24(%r12), %r13
	movq	32(%r12), %rsi
	movl	$1, %ecx
	movq	%rax, -576(%rbp)
	movq	-656(%rbp), %rdi
	movq	360(%r13), %rax
	leaq	624(%rax), %rdx
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movdqa	-560(%rbp), %xmm4
	movq	-632(%rbp), %rdi
	movaps	%xmm4, -544(%rbp)
	call	_ZNK2v88internal8compiler6MapRef13instance_typeEv@PLT
	movq	8(%r13), %rdi
	movl	$1, %esi
	movq	0(%r13), %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder11BeginRegionENS1_19RegionObservabilityE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	-576(%rbp), %rax
	movq	%r14, %rdi
	movq	%rax, -256(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rdi
	movq	0(%r13), %r14
	movq	%rax, -584(%rbp)
	movl	-612(%rbp), %eax
	leal	32(,%rax,8), %eax
	cvtsi2sdl	%eax, %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	376(%r13), %rdi
	xorl	%edx, %edx
	movl	$16777217, %esi
	movq	%rax, -592(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder8AllocateENS1_4TypeENS0_14AllocationTypeE@PLT
	xorl	%r8d, %r8d
	movl	$3, %edx
	movq	%r14, %rdi
	movq	-568(%rbp), %rcx
	movq	%rax, %rsi
	movq	-592(%rbp), %xmm0
	movq	%rcx, -240(%rbp)
	movhps	-584(%rbp), %xmm0
	movq	%r15, %rcx
	movaps	%xmm0, -256(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rbx, %rdi
	movq	%rax, -600(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	-632(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	376(%r13), %rdi
	movq	%rbx, %rsi
	movq	0(%r13), %r14
	movq	%rax, -584(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$4, %edx
	movq	-600(%rbp), %xmm1
	movq	%rax, %rsi
	movq	%r14, %rdi
	movdqa	%xmm1, %xmm0
	movhps	-584(%rbp), %xmm0
	movaps	%xmm0, -256(%rbp)
	movdqa	%xmm1, %xmm0
	movhps	-568(%rbp), %xmm0
	movaps	%xmm0, -240(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rdi
	movq	%rax, -592(%rbp)
	movl	-612(%rbp), %eax
	addl	$2, %eax
	cvtsi2sdl	%eax, %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%rbx, %rdi
	movq	%rax, -584(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder19ForFixedArrayLengthEv@PLT
	movq	376(%r13), %rdi
	movq	%rbx, %rsi
	movq	0(%r13), %r14
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$4, %edx
	movq	-600(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r14, %rdi
	movhps	-584(%rbp), %xmm0
	movaps	%xmm0, -256(%rbp)
	movq	-592(%rbp), %xmm0
	movhps	-568(%rbp), %xmm0
	movaps	%xmm0, -240(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	24(%r12), %rdi
	pxor	%xmm0, %xmm0
	movq	%rax, -592(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%rbx, %rdi
	movq	%rax, -584(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder20ForFixedArrayElementEv@PLT
	movq	376(%r13), %rdi
	movq	%rbx, %rsi
	movq	0(%r13), %r14
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder12StoreElementERKNS1_13ElementAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%r14, %rdi
	movq	-600(%rbp), %xmm0
	movq	-568(%rbp), %rdx
	movq	%rax, %rsi
	movhps	-584(%rbp), %xmm0
	movq	%rdx, -224(%rbp)
	movl	$5, %edx
	movaps	%xmm0, -256(%rbp)
	movq	-640(%rbp), %xmm0
	movhps	-592(%rbp), %xmm0
	movaps	%xmm0, -240(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movsd	.LC24(%rip), %xmm0
	movq	24(%r12), %rdi
	movq	%rax, -592(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%rbx, %rdi
	movq	%rax, -584(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder20ForFixedArrayElementEv@PLT
	movq	376(%r13), %rdi
	movq	%rbx, %rsi
	movq	0(%r13), %r14
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder12StoreElementERKNS1_13ElementAccessE@PLT
	xorl	%r8d, %r8d
	movl	$5, %edx
	movq	%r14, %rdi
	movq	-600(%rbp), %xmm0
	movq	-568(%rbp), %rcx
	movq	%rax, %rsi
	movhps	-584(%rbp), %xmm0
	movq	%rcx, -224(%rbp)
	movq	%r15, %rcx
	movaps	%xmm0, -256(%rbp)
	movq	-576(%rbp), %xmm0
	movhps	-592(%rbp), %xmm0
	movaps	%xmm0, -240(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -576(%rbp)
	movl	-612(%rbp), %eax
	testl	%eax, %eax
	jle	.L696
	movl	-644(%rbp), %edx
	movq	%r15, -608(%rbp)
	leal	3(%rdx), %r14d
	movl	%r14d, %ecx
	subl	%eax, %ecx
	leal	5(%rdx), %eax
	movl	%ecx, -612(%rbp)
	movl	%eax, -616(%rbp)
	movq	%r12, %rax
	movl	%r14d, %r12d
	movq	%rax, %r14
	.p2align 4,,10
	.p2align 3
.L697:
	pxor	%xmm0, %xmm0
	movq	24(%r14), %rdi
	cvtsi2sdl	%r12d, %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	pxor	%xmm0, %xmm0
	movq	24(%r14), %rdi
	movq	%rax, -584(%rbp)
	movl	-616(%rbp), %eax
	subl	%r12d, %eax
	subl	$1, %r12d
	cvtsi2sdl	%eax, %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%rbx, %rdi
	movq	%rax, -592(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder20ForFixedArrayElementEv@PLT
	movq	376(%r13), %rdi
	movq	%rbx, %rsi
	movq	0(%r13), %r15
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder12StoreElementERKNS1_13ElementAccessE@PLT
	xorl	%r8d, %r8d
	movl	$5, %edx
	movq	%r15, %rdi
	movq	-600(%rbp), %xmm0
	movq	%rax, %rsi
	movq	-608(%rbp), %rcx
	movq	-568(%rbp), %rax
	movhps	-592(%rbp), %xmm0
	movaps	%xmm0, -256(%rbp)
	movq	-584(%rbp), %xmm0
	movq	%rax, -224(%rbp)
	movhps	-576(%rbp), %xmm0
	movaps	%xmm0, -240(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -576(%rbp)
	cmpl	%r12d, -612(%rbp)
	jne	.L697
	movq	-608(%rbp), %r15
.L696:
	movq	8(%r13), %rdi
	movq	0(%r13), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12FinishRegionEv@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$2, %edx
	movq	-600(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r12, %rdi
	movhps	-576(%rbp), %xmm0
	movaps	%xmm0, -256(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	jmp	.L687
	.p2align 4,,10
	.p2align 3
.L707:
	movq	-568(%rbp), %rdx
	movq	%r15, %rcx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler16JSCreateLowering17AllocateArgumentsEPNS1_4NodeES4_S4_
	jmp	.L687
.L706:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23007:
	.size	_ZN2v88internal8compiler16JSCreateLowering24AllocateAliasedArgumentsEPNS1_4NodeES4_S4_S4_RKNS1_21SharedFunctionInfoRefEPb, .-_ZN2v88internal8compiler16JSCreateLowering24AllocateAliasedArgumentsEPNS1_4NodeES4_S4_S4_RKNS1_21SharedFunctionInfoRefEPb
	.section	.text._ZN2v88internal8compiler16JSCreateLowering24AllocateAliasedArgumentsEPNS1_4NodeES4_S4_S4_S4_RKNS1_21SharedFunctionInfoRefEPb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16JSCreateLowering24AllocateAliasedArgumentsEPNS1_4NodeES4_S4_S4_S4_RKNS1_21SharedFunctionInfoRefEPb
	.type	_ZN2v88internal8compiler16JSCreateLowering24AllocateAliasedArgumentsEPNS1_4NodeES4_S4_S4_S4_RKNS1_21SharedFunctionInfoRefEPb, @function
_ZN2v88internal8compiler16JSCreateLowering24AllocateAliasedArgumentsEPNS1_4NodeES4_S4_S4_S4_RKNS1_21SharedFunctionInfoRefEPb:
.LFB23008:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$248, %rsp
	movq	%rdx, -264(%rbp)
	movq	16(%rbp), %rdi
	movq	%rcx, -216(%rbp)
	movq	24(%rbp), %r13
	movq	%r8, -208(%rbp)
	movq	%r9, -280(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal8compiler21SharedFunctionInfoRef31internal_formal_parameter_countEv@PLT
	movl	%eax, -268(%rbp)
	testl	%eax, %eax
	jne	.L709
	movq	-208(%rbp), %xmm0
	movq	24(%rbx), %rax
	xorl	%esi, %esi
	movq	376(%rax), %rdi
	movhps	-280(%rbp), %xmm0
	movq	(%rax), %r13
	movaps	%xmm0, -208(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder20NewArgumentsElementsEi@PLT
	leaq	-96(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	movdqa	-208(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$3, %edx
	movq	%r12, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
.L708:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L716
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L709:
	.cfi_restore_state
	movb	$1, 0(%r13)
	movl	%eax, %esi
	movq	24(%rbx), %rax
	leaq	-96(%rbp), %r14
	movq	-208(%rbp), %xmm0
	movq	376(%rax), %rdi
	movq	(%rax), %r13
	movhps	-280(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder20NewArgumentsElementsEi@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	movq	%r14, %rcx
	movdqa	-208(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$3, %edx
	movq	%r12, -80(%rbp)
	leaq	-176(%rbp), %r12
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	24(%rbx), %r13
	movq	32(%rbx), %rsi
	leaq	-192(%rbp), %rdi
	movq	%rax, -208(%rbp)
	movq	360(%r13), %rcx
	leaq	624(%rcx), %rdx
	movl	$1, %ecx
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movdqa	-192(%rbp), %xmm2
	movq	%r12, %rdi
	movaps	%xmm2, -176(%rbp)
	call	_ZNK2v88internal8compiler6MapRef13instance_typeEv@PLT
	movq	8(%r13), %rdi
	movl	$1, %esi
	movq	0(%r13), %r15
	call	_ZN2v88internal8compiler21CommonOperatorBuilder11BeginRegionENS1_19RegionObservabilityE@PLT
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%r15, %rdi
	movq	-208(%rbp), %rcx
	movq	%rax, %rsi
	movq	%rcx, -96(%rbp)
	movq	%r14, %rcx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rdi
	movq	0(%r13), %r15
	movq	%rax, -224(%rbp)
	movl	-268(%rbp), %eax
	leal	32(,%rax,8), %eax
	cvtsi2sdl	%eax, %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	376(%r13), %rdi
	xorl	%edx, %edx
	movl	$16777217, %esi
	movq	%rax, -232(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder8AllocateENS1_4TypeENS0_14AllocationTypeE@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	%r15, %rdi
	movq	-264(%rbp), %rdx
	leaq	-160(%rbp), %r15
	movq	%rax, %rsi
	movq	-232(%rbp), %xmm0
	movq	%rdx, -80(%rbp)
	movhps	-224(%rbp), %xmm0
	movl	$3, %edx
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r15, %rdi
	movq	%rax, -256(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	376(%r13), %rdi
	movq	%r15, %rsi
	movq	0(%r13), %r12
	movq	%rax, -224(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$4, %edx
	movq	-256(%rbp), %xmm1
	movq	%rax, %rsi
	movq	%r12, %rdi
	movdqa	%xmm1, %xmm0
	movhps	-224(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movdqa	%xmm1, %xmm0
	movhps	-264(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rdi
	movq	%rax, -232(%rbp)
	movl	-268(%rbp), %eax
	addl	$2, %eax
	cvtsi2sdl	%eax, %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%r15, %rdi
	movq	%rax, -224(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder19ForFixedArrayLengthEv@PLT
	movq	376(%r13), %rdi
	movq	%r15, %rsi
	movq	0(%r13), %r12
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$4, %edx
	movq	-256(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r12, %rdi
	movhps	-224(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-232(%rbp), %xmm0
	movhps	-264(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	24(%rbx), %rdi
	pxor	%xmm0, %xmm0
	movq	%rax, -232(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%r15, %rdi
	movq	%rax, -224(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder20ForFixedArrayElementEv@PLT
	movq	376(%r13), %rdi
	movq	%r15, %rsi
	movq	0(%r13), %r12
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder12StoreElementERKNS1_13ElementAccessE@PLT
	xorl	%r8d, %r8d
	movl	$5, %edx
	movq	%r12, %rdi
	movq	-256(%rbp), %xmm0
	movq	-264(%rbp), %rcx
	movq	%rax, %rsi
	movhps	-224(%rbp), %xmm0
	movq	%rcx, -64(%rbp)
	movq	%r14, %rcx
	movaps	%xmm0, -96(%rbp)
	movq	-216(%rbp), %xmm0
	movhps	-232(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movsd	.LC24(%rip), %xmm0
	movq	24(%rbx), %rdi
	movq	%rax, -224(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%r15, %rdi
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder20ForFixedArrayElementEv@PLT
	movq	376(%r13), %rdi
	movq	%r15, %rsi
	movq	0(%r13), %r12
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder12StoreElementERKNS1_13ElementAccessE@PLT
	xorl	%r8d, %r8d
	movl	$5, %edx
	movq	%r12, %rdi
	movq	-256(%rbp), %xmm0
	movq	-264(%rbp), %rcx
	movq	%rax, %rsi
	movhps	-216(%rbp), %xmm0
	movq	%rcx, -64(%rbp)
	movq	%r14, %rcx
	movaps	%xmm0, -96(%rbp)
	movq	-208(%rbp), %xmm0
	movhps	-224(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -208(%rbp)
	movl	-268(%rbp), %eax
	testl	%eax, %eax
	jle	.L711
	addl	$3, %eax
	xorl	%r12d, %r12d
	movl	%eax, -272(%rbp)
	.p2align 4,,10
	.p2align 3
.L712:
	movq	24(%rbx), %rdi
	movq	(%rdi), %r10
	movq	%r10, -248(%rbp)
	call	_ZN2v88internal8compiler7JSGraph15TheHoleConstantEv@PLT
	pxor	%xmm0, %xmm0
	movq	24(%rbx), %rdi
	movq	%rax, -240(%rbp)
	movl	-272(%rbp), %eax
	subl	%r12d, %eax
	cvtsi2sdl	%eax, %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	pxor	%xmm0, %xmm0
	movq	24(%rbx), %rdi
	cvtsi2sdl	%r12d, %xmm0
	movq	%rax, -216(%rbp)
	movq	(%rdi), %r11
	movq	%r11, -232(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%rax, -224(%rbp)
	movq	24(%rbx), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14NumberLessThanEv@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$2, %edx
	movq	-232(%rbp), %r11
	movq	%rax, %rsi
	movq	-224(%rbp), %xmm0
	movq	%r11, %rdi
	movhps	-280(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	xorl	%edx, %edx
	movl	$8, %esi
	movq	%rax, -224(%rbp)
	movq	24(%rbx), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6SelectENS0_21MachineRepresentationENS1_10BranchHintE@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$3, %edx
	movq	-248(%rbp), %r10
	movq	-240(%rbp), %r9
	movq	%rax, %rsi
	movq	-224(%rbp), %xmm0
	movq	%r10, %rdi
	movq	%r9, -80(%rbp)
	movhps	-216(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	pxor	%xmm0, %xmm0
	movq	24(%rbx), %rdi
	movq	%rax, -224(%rbp)
	leal	2(%r12), %eax
	addl	$1, %r12d
	cvtsi2sdl	%eax, %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%r15, %rdi
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder20ForFixedArrayElementEv@PLT
	movq	0(%r13), %r9
	movq	376(%r13), %rdi
	movq	%r15, %rsi
	movq	%r9, -232(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder12StoreElementERKNS1_13ElementAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$5, %edx
	movq	-256(%rbp), %xmm0
	movq	-232(%rbp), %r9
	movq	%rax, %rsi
	movq	-264(%rbp), %rax
	movhps	-216(%rbp), %xmm0
	movq	%r9, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	-224(%rbp), %xmm0
	movq	%rax, -64(%rbp)
	movhps	-208(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -208(%rbp)
	cmpl	%r12d, -268(%rbp)
	jne	.L712
.L711:
	movq	8(%r13), %rdi
	movq	0(%r13), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12FinishRegionEv@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$2, %edx
	movq	-256(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r12, %rdi
	movhps	-208(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	jmp	.L708
.L716:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23008:
	.size	_ZN2v88internal8compiler16JSCreateLowering24AllocateAliasedArgumentsEPNS1_4NodeES4_S4_S4_S4_RKNS1_21SharedFunctionInfoRefEPb, .-_ZN2v88internal8compiler16JSCreateLowering24AllocateAliasedArgumentsEPNS1_4NodeES4_S4_S4_S4_RKNS1_21SharedFunctionInfoRefEPb
	.section	.rodata._ZN2v88internal8compiler16JSCreateLowering23ReduceJSCreateArgumentsEPNS1_4NodeE.str1.1,"aMS",@progbits,1
.LC26:
	.string	"(location_) != nullptr"
	.section	.text._ZN2v88internal8compiler16JSCreateLowering23ReduceJSCreateArgumentsEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16JSCreateLowering23ReduceJSCreateArgumentsEPNS1_4NodeE
	.type	_ZN2v88internal8compiler16JSCreateLowering23ReduceJSCreateArgumentsEPNS1_4NodeE, @function
_ZN2v88internal8compiler16JSCreateLowering23ReduceJSCreateArgumentsEPNS1_4NodeE:
.LFB22938:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$264, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler21CreateArgumentsTypeOfEPKNS1_8OperatorE@PLT
	movq	%rbx, %rdi
	movzbl	(%rax), %r13d
	call	_ZN2v88internal8compiler14NodeProperties18GetFrameStateInputEPNS1_4NodeE@PLT
	movq	%rax, %r15
	addq	$32, %rax
	movq	%rax, -264(%rbp)
	movzbl	23(%r15), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L718
	leaq	72(%r15), %rax
.L719:
	movq	(%rax), %rax
	movq	(%r15), %rdi
	movq	%rax, -256(%rbp)
	movq	24(%r12), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rax
	movq	%rax, -248(%rbp)
	call	_ZN2v88internal8compiler16FrameStateInfoOfEPKNS1_8OperatorE@PLT
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L720
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L720
	movq	32(%r12), %rsi
	leaq	-224(%rbp), %r14
	movl	$1, %ecx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21SharedFunctionInfoRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	-256(%rbp), %rax
	movq	(%rax), %rax
	cmpw	$41, 16(%rax)
	je	.L722
	cmpb	$1, %r13b
	je	.L723
	cmpb	$2, %r13b
	je	.L724
	testb	%r13b, %r13b
	je	.L831
	leaq	.LC18(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L718:
	movq	32(%r15), %rax
	addq	$56, %rax
	jmp	.L719
	.p2align 4,,10
	.p2align 3
.L722:
	testb	%r13b, %r13b
	je	.L832
	cmpb	$1, %r13b
	je	.L833
	xorl	%eax, %eax
	cmpb	$2, %r13b
	jne	.L732
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21SharedFunctionInfoRef31internal_formal_parameter_countEv@PLT
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movl	%eax, -256(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14NodeProperties18GetFrameStateInputEPNS1_4NodeE@PLT
	movq	(%rax), %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler16FrameStateInfoOfEPKNS1_8OperatorE@PLT
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L769
	cmpl	$1, (%rax)
	jne	.L769
	leaq	32(%r14), %rax
	movq	%r14, %r15
	movq	%rax, -264(%rbp)
.L769:
	movq	-264(%rbp), %rax
	movq	(%rax), %rdx
	movzbl	23(%r15), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L770
	movq	16(%rdx), %rdx
.L770:
	movq	(%rdx), %rax
	cmpw	$60, 16(%rax)
	je	.L824
	movq	(%r15), %rdi
	call	_ZN2v88internal8compiler16FrameStateInfoOfEPKNS1_8OperatorE@PLT
	movq	%r13, %rsi
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	16(%rax), %rax
	movl	-256(%rbp), %r8d
	movq	-248(%rbp), %rdx
	movq	%rax, -264(%rbp)
	call	_ZN2v88internal8compiler16JSCreateLowering21AllocateRestArgumentsEPNS1_4NodeES4_S4_i
	movq	24(%r12), %r10
	movq	%rax, %r8
	movq	(%rax), %rax
	movq	%r8, -280(%rbp)
	cmpb	$0, 36(%rax)
	movq	32(%r12), %rax
	movq	%r10, -272(%rbp)
	cmovne	%r8, %r13
	cmpb	$0, 24(%rax)
	je	.L731
	movdqu	32(%rax), %xmm7
	leaq	-192(%rbp), %r15
	leaq	-160(%rbp), %r14
	movq	%r15, %rdi
	movaps	%xmm7, -192(%rbp)
	call	_ZNK2v88internal8compiler16NativeContextRef28js_array_packed_elements_mapEv@PLT
	movq	-272(%rbp), %r10
	movq	%r14, %rsi
	movq	%rax, -160(%rbp)
	movq	%r10, %rdi
	movq	%rdx, -152(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	%r13, -176(%rbp)
	movq	-280(%rbp), %r8
	movl	$-1, %r13d
	movq	$0, -184(%rbp)
	movq	%rax, %r10
	movq	24(%r12), %rax
	movq	%rax, -192(%rbp)
	movq	-248(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	-264(%rbp), %rax
	testq	%rax, %rax
	je	.L774
	movl	4(%rax), %eax
	leal	-1(%rax), %r13d
.L774:
	movl	$4294967295, %ecx
	xorl	%edx, %edx
	movl	$32, %esi
	movq	%r15, %rdi
	movq	%r8, -264(%rbp)
	movq	%r10, -248(%rbp)
	call	_ZN2v88internal8compiler17AllocationBuilder8AllocateEiNS0_14AllocationTypeENS1_4TypeE
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	-248(%rbp), %r10
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%r10, %rdx
	call	_ZN2v88internal8compiler17AllocationBuilder5StoreERKNS1_11FieldAccessEPNS1_4NodeE
	movq	24(%r12), %rdi
	call	_ZN2v88internal8compiler7JSGraph23EmptyFixedArrayConstantEv@PLT
	movq	%r14, %rdi
	movq	%rax, -248(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder39ForJSObjectPropertiesOrHashKnownPointerEv@PLT
	movq	-248(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler17AllocationBuilder5StoreERKNS1_11FieldAccessEPNS1_4NodeE
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder19ForJSObjectElementsEv@PLT
	movq	-264(%rbp), %r8
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler17AllocationBuilder5StoreERKNS1_11FieldAccessEPNS1_4NodeE
	movl	$0, %edx
	movl	%r13d, %eax
	subl	-256(%rbp), %eax
	cmovs	%edx, %eax
	pxor	%xmm0, %xmm0
	movq	24(%r12), %rdi
	cvtsi2sdl	%eax, %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movl	$2, %esi
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13AccessBuilder16ForJSArrayLengthENS0_12ElementsKindE@PLT
	movq	%r13, %rdx
	jmp	.L823
	.p2align 4,,10
	.p2align 3
.L831:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21SharedFunctionInfoRef24has_duplicate_parametersEv@PLT
	testb	%al, %al
	je	.L726
.L824:
	xorl	%eax, %eax
	jmp	.L732
	.p2align 4,,10
	.p2align 3
.L724:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	%rax, -272(%rbp)
	movq	24(%r12), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14ArgumentsFrameEv@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r14, %rdi
	movq	%rax, -256(%rbp)
	movq	24(%r12), %rax
	movq	376(%rax), %r13
	movq	(%rax), %r15
	call	_ZNK2v88internal8compiler21SharedFunctionInfoRef31internal_formal_parameter_countEv@PLT
	movl	$1, %edx
	movq	%r13, %rdi
	movl	%eax, %esi
	leaq	-96(%rbp), %r13
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder15ArgumentsLengthEib@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	-256(%rbp), %rax
	movq	%r15, %rdi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	xorl	%esi, %esi
	movq	%rax, -264(%rbp)
	movq	24(%r12), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder20NewArgumentsElementsEi@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movq	%r13, %rcx
	movq	-272(%rbp), %r9
	movq	%rax, %rsi
	movl	$3, %edx
	movq	-256(%rbp), %xmm0
	movhps	-264(%rbp), %xmm0
	movq	%r9, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	24(%r12), %r15
	movq	%rax, -272(%rbp)
	movq	32(%r12), %rax
	cmpb	$0, 24(%rax)
	je	.L731
	movdqu	32(%rax), %xmm4
	leaq	-160(%rbp), %r14
	leaq	-192(%rbp), %rdi
	movaps	%xmm4, -192(%rbp)
	call	_ZNK2v88internal8compiler16NativeContextRef28js_array_packed_elements_mapEv@PLT
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%rdx, -152(%rbp)
	movq	%rax, -160(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	24(%r12), %r15
	movl	$1, %esi
	movq	%rax, -280(%rbp)
	movq	(%r15), %r10
	movq	8(%r15), %rdi
	movq	%r10, -256(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder11BeginRegionENS1_19RegionObservabilityE@PLT
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	-256(%rbp), %r10
	movq	%rax, %rsi
	movq	-272(%rbp), %rax
	movq	%r10, %rdi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	(%r15), %r10
	movsd	.LC20(%rip), %xmm0
	movq	%r15, %rdi
	movq	%rax, -256(%rbp)
	movq	%r10, -296(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	376(%r15), %rdi
	xorl	%edx, %edx
	movl	$4294967295, %esi
	movq	%rax, -288(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder8AllocateENS1_4TypeENS0_14AllocationTypeE@PLT
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$3, %edx
	movq	-296(%rbp), %r10
	movq	%rax, %rsi
	movq	-248(%rbp), %rax
	movq	-288(%rbp), %xmm0
	movq	%r10, %rdi
	movq	%rax, -80(%rbp)
	movhps	-256(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r14, %rdi
	movq	%rax, -256(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	(%r15), %r10
	movq	376(%r15), %rdi
	movq	%r14, %rsi
	movq	%r10, -288(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$4, %edx
	movq	-256(%rbp), %xmm2
	movq	-288(%rbp), %r10
	movq	%rax, %rsi
	movdqa	%xmm2, %xmm0
	movq	%r10, %rdi
	movhps	-280(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movdqa	%xmm2, %xmm0
	movhps	-248(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	24(%r12), %rdi
	movq	%rax, -288(%rbp)
	call	_ZN2v88internal8compiler7JSGraph23EmptyFixedArrayConstantEv@PLT
	movq	%r14, %rdi
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder39ForJSObjectPropertiesOrHashKnownPointerEv@PLT
	movq	(%r15), %r10
	movq	376(%r15), %rdi
	movq	%r14, %rsi
	movq	%r10, -296(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$4, %edx
	movq	-256(%rbp), %xmm0
	movq	-296(%rbp), %r10
	movq	%rax, %rsi
	movhps	-280(%rbp), %xmm0
	movq	%r10, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	-288(%rbp), %xmm0
	movhps	-248(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r14, %rdi
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder19ForJSObjectElementsEv@PLT
	movq	(%r15), %r10
	movq	376(%r15), %rdi
	movq	%r14, %rsi
	movq	%r10, -288(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$4, %edx
	movq	-256(%rbp), %xmm0
	movq	-288(%rbp), %r10
	movq	%rax, %rsi
	movhps	-272(%rbp), %xmm0
	movq	%r10, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	-280(%rbp), %xmm0
	movhps	-248(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$2, %esi
	movq	%r14, %rdi
	movq	%rax, -272(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder16ForJSArrayLengthENS0_12ElementsKindE@PLT
	movq	(%r15), %r9
	movq	376(%r15), %rdi
	movq	%r14, %rsi
	movq	%r9, -280(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$4, %edx
	movq	-256(%rbp), %xmm0
	movq	-280(%rbp), %r9
	movq	%rax, %rsi
	movhps	-264(%rbp), %xmm0
	movq	%r9, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	-272(%rbp), %xmm0
	movhps	-248(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%r12), %rdi
	movq	%rbx, %rcx
	movq	%rbx, %rdx
	movq	%rax, %r13
	xorl	%r8d, %r8d
	movq	%rbx, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	8(%rbx), %rax
	movq	-256(%rbp), %rcx
	leaq	32(%rbx), %rdx
	movq	%rax, 8(%rcx)
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L742
	movq	32(%rbx), %rdi
	movq	%rdx, %r14
	movq	%rbx, %rsi
	cmpq	%rdi, %rcx
	je	.L744
.L743:
	leaq	-24(%rsi), %r12
	testq	%rdi, %rdi
	je	.L746
	movq	%r12, %rsi
	movq	%rdx, -248(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-248(%rbp), %rdx
.L746:
	movq	-256(%rbp), %rax
	movq	%r12, %rsi
	movq	%rdx, -248(%rbp)
	movq	%rax, (%r14)
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movzbl	23(%rbx), %eax
	movq	-248(%rbp), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L834
.L744:
	movq	8(%rdx), %rdi
	cmpq	%r13, %rdi
	je	.L749
.L830:
	leaq	8(%rdx), %r14
	movq	%rbx, %rsi
.L748:
	leaq	-48(%rsi), %r12
	testq	%rdi, %rdi
	je	.L750
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L750:
	movq	%r13, (%r14)
	testq	%r13, %r13
	je	.L749
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L749:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler4Node14TrimInputCountEi@PLT
	movq	8(%r15), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12FinishRegionEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	%rbx, %rax
.L732:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L835
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L723:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	%rax, -272(%rbp)
	movq	24(%r12), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14ArgumentsFrameEv@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r14, %rdi
	movq	%rax, -256(%rbp)
	movq	24(%r12), %rax
	movq	376(%rax), %r13
	movq	(%rax), %r15
	call	_ZNK2v88internal8compiler21SharedFunctionInfoRef31internal_formal_parameter_countEv@PLT
	xorl	%edx, %edx
	movq	%r13, %rdi
	movl	%eax, %esi
	leaq	-96(%rbp), %r13
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder15ArgumentsLengthEib@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	-256(%rbp), %rax
	movq	%r15, %rdi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	xorl	%esi, %esi
	movq	%rax, -264(%rbp)
	movq	24(%r12), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder20NewArgumentsElementsEi@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movq	%r13, %rcx
	movq	-272(%rbp), %r9
	movq	%rax, %rsi
	movl	$3, %edx
	movq	-256(%rbp), %xmm0
	movhps	-264(%rbp), %xmm0
	movq	%r9, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	24(%r12), %r15
	movq	%rax, -272(%rbp)
	movq	32(%r12), %rax
	cmpb	$0, 24(%rax)
	je	.L731
	movdqu	32(%rax), %xmm3
	leaq	-160(%rbp), %r14
	leaq	-192(%rbp), %rdi
	movaps	%xmm3, -192(%rbp)
	call	_ZNK2v88internal8compiler16NativeContextRef20strict_arguments_mapEv@PLT
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%rdx, -152(%rbp)
	movq	%rax, -160(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	24(%r12), %r15
	movl	$1, %esi
	movq	%rax, -280(%rbp)
	movq	(%r15), %r10
	movq	8(%r15), %rdi
	movq	%r10, -256(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder11BeginRegionENS1_19RegionObservabilityE@PLT
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	-256(%rbp), %r10
	movq	%rax, %rsi
	movq	-272(%rbp), %rax
	movq	%r10, %rdi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	(%r15), %r10
	movsd	.LC20(%rip), %xmm0
	movq	%r15, %rdi
	movq	%rax, -256(%rbp)
	movq	%r10, -296(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	376(%r15), %rdi
	xorl	%edx, %edx
	movl	$4294967295, %esi
	movq	%rax, -288(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder8AllocateENS1_4TypeENS0_14AllocationTypeE@PLT
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$3, %edx
	movq	-296(%rbp), %r10
	movq	%rax, %rsi
	movq	-248(%rbp), %rax
	movq	-288(%rbp), %xmm0
	movq	%r10, %rdi
	movq	%rax, -80(%rbp)
	movhps	-256(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r14, %rdi
	movq	%rax, -256(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	(%r15), %r10
	movq	376(%r15), %rdi
	movq	%r14, %rsi
	movq	%r10, -288(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$4, %edx
	movq	-256(%rbp), %xmm1
	movq	-288(%rbp), %r10
	movq	%rax, %rsi
	movdqa	%xmm1, %xmm0
	movq	%r10, %rdi
	movhps	-280(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movdqa	%xmm1, %xmm0
	movhps	-248(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	24(%r12), %rdi
	movq	%rax, -288(%rbp)
	call	_ZN2v88internal8compiler7JSGraph23EmptyFixedArrayConstantEv@PLT
	movq	%r14, %rdi
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder39ForJSObjectPropertiesOrHashKnownPointerEv@PLT
	movq	(%r15), %r10
	movq	376(%r15), %rdi
	movq	%r14, %rsi
	movq	%r10, -296(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$4, %edx
	movq	-256(%rbp), %xmm0
	movq	-296(%rbp), %r10
	movq	%rax, %rsi
	movhps	-280(%rbp), %xmm0
	movq	%r10, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	-288(%rbp), %xmm0
	movhps	-248(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r14, %rdi
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder19ForJSObjectElementsEv@PLT
	movq	(%r15), %r10
	movq	376(%r15), %rdi
	movq	%r14, %rsi
	movq	%r10, -288(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$4, %edx
	movq	-256(%rbp), %xmm0
	movq	-288(%rbp), %r10
	movq	%rax, %rsi
	movhps	-272(%rbp), %xmm0
	movq	%r10, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	-280(%rbp), %xmm0
	movhps	-248(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r14, %rdi
	movq	%rax, -272(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder18ForArgumentsLengthEv@PLT
	movq	(%r15), %r9
	movq	376(%r15), %rdi
	movq	%r14, %rsi
	movq	%r9, -280(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$4, %edx
	movq	-256(%rbp), %xmm0
	movq	-280(%rbp), %r9
	movq	%rax, %rsi
	movhps	-264(%rbp), %xmm0
	movq	%r9, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	-272(%rbp), %xmm0
	movhps	-248(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%r12), %rdi
	movq	%rbx, %rcx
	movq	%rbx, %rdx
	movq	%rax, %r13
	xorl	%r8d, %r8d
	movq	%rbx, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	8(%rbx), %rax
	movq	-256(%rbp), %rcx
	leaq	32(%rbx), %rdx
	movq	%rax, 8(%rcx)
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L733
	movq	32(%rbx), %rdi
	movq	%rdx, %r14
	movq	%rbx, %rsi
	cmpq	%rcx, %rdi
	je	.L735
.L734:
	leaq	-24(%rsi), %r12
	testq	%rdi, %rdi
	je	.L737
	movq	%r12, %rsi
	movq	%rdx, -248(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-248(%rbp), %rdx
.L737:
	movq	-256(%rbp), %rax
	movq	%r12, %rsi
	movq	%rdx, -248(%rbp)
	movq	%rax, (%r14)
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movzbl	23(%rbx), %eax
	movq	-248(%rbp), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L836
.L735:
	movq	8(%rdx), %rdi
	cmpq	%rdi, %r13
	jne	.L830
	jmp	.L749
	.p2align 4,,10
	.p2align 3
.L726:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rbx, %rdi
	movq	%rax, -288(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetContextInputEPNS1_4NodeE@PLT
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	%rax, -272(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	%rax, -264(%rbp)
	movq	24(%r12), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14ArgumentsFrameEv@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r14, %rdi
	movq	%rax, %r13
	movq	24(%r12), %rax
	movq	376(%rax), %r8
	movq	(%rax), %r15
	movq	%r8, -256(%rbp)
	call	_ZNK2v88internal8compiler21SharedFunctionInfoRef31internal_formal_parameter_countEv@PLT
	movq	-256(%rbp), %r8
	xorl	%edx, %edx
	movl	%eax, %esi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder15ArgumentsLengthEib@PLT
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%r15, %rdi
	movq	%rax, %rsi
	leaq	-96(%rbp), %rax
	movq	%r13, -96(%rbp)
	movq	%rax, %rcx
	movq	%rax, -256(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-272(%rbp), %r11
	movq	%r12, %rdi
	movq	%r13, %r8
	movq	%rax, -280(%rbp)
	movq	%rax, %r9
	leaq	-226(%rbp), %rax
	movq	-264(%rbp), %r10
	pushq	%rax
	movq	-248(%rbp), %rdx
	movq	%r11, %rcx
	pushq	%r14
	movq	%r10, %rsi
	movb	$0, -226(%rbp)
	call	_ZN2v88internal8compiler16JSCreateLowering24AllocateAliasedArgumentsEPNS1_4NodeES4_S4_S4_S4_RKNS1_21SharedFunctionInfoRefEPb
	cmpb	$0, -226(%rbp)
	popq	%rsi
	movq	24(%r12), %r14
	popq	%rdi
	movq	%rax, -264(%rbp)
	movq	32(%r12), %rax
	jne	.L837
	cmpb	$0, 24(%rax)
	je	.L731
	movdqu	32(%rax), %xmm6
	leaq	-192(%rbp), %r15
	movq	%r15, %rdi
	movaps	%xmm6, -192(%rbp)
	call	_ZNK2v88internal8compiler16NativeContextRef20sloppy_arguments_mapEv@PLT
	movq	%rax, -160(%rbp)
	movq	%rdx, -152(%rbp)
.L730:
	leaq	-160(%rbp), %r13
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	-264(%rbp), %rcx
	movl	$1, %esi
	movq	-248(%rbp), %rdx
	movq	%rax, -272(%rbp)
	movq	24(%r12), %rax
	movq	%rcx, -176(%rbp)
	movq	%rdx, -168(%rbp)
	movq	%rax, -192(%rbp)
	movq	$0, -184(%rbp)
	movq	8(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder11BeginRegionENS1_19RegionObservabilityE@PLT
	movq	-264(%rbp), %rcx
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r14, %rdi
	movq	%rcx, -96(%rbp)
	movq	-256(%rbp), %rcx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-168(%rbp), %rcx
	movq	-192(%rbp), %rdi
	movsd	.LC17(%rip), %xmm0
	movq	%rax, -176(%rbp)
	movq	%rcx, -304(%rbp)
	movq	(%rdi), %r14
	movq	%rax, -248(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	xorl	%edx, %edx
	movl	$4294967295, %esi
	movq	%rax, -296(%rbp)
	movq	-192(%rbp), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder8AllocateENS1_4TypeENS0_14AllocationTypeE@PLT
	xorl	%r8d, %r8d
	movl	$3, %edx
	movq	%r14, %rdi
	movq	-304(%rbp), %rcx
	movq	%rax, %rsi
	movq	-296(%rbp), %xmm0
	movq	%rcx, -80(%rbp)
	movq	-256(%rbp), %rcx
	movhps	-248(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r13, %rdi
	movq	%rax, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, -184(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	-192(%rbp), %rax
	movq	-168(%rbp), %rdx
	movq	-184(%rbp), %rsi
	movq	%rdx, -248(%rbp)
	movq	376(%rax), %rdi
	movq	-176(%rbp), %rdx
	movq	(%rax), %r14
	movq	%rsi, -304(%rbp)
	movq	%r13, %rsi
	movq	%rdx, -296(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movl	$4, %edx
	movq	%r14, %rdi
	movq	-304(%rbp), %xmm0
	movq	-256(%rbp), %rcx
	movq	%rax, %rsi
	movhps	-272(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-296(%rbp), %xmm0
	movhps	-248(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	24(%r12), %rdi
	movq	%rax, -176(%rbp)
	call	_ZN2v88internal8compiler7JSGraph23EmptyFixedArrayConstantEv@PLT
	movq	%r13, %rdi
	movq	%rax, -248(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder39ForJSObjectPropertiesOrHashKnownPointerEv@PLT
	movq	-192(%rbp), %rax
	movq	-168(%rbp), %rdx
	movq	-184(%rbp), %rsi
	movq	%rdx, -272(%rbp)
	movq	376(%rax), %rdi
	movq	-176(%rbp), %rdx
	movq	(%rax), %r14
	movq	%rsi, -304(%rbp)
	movq	%r13, %rsi
	movq	%rdx, -296(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movl	$4, %edx
	movq	%r14, %rdi
	movq	-304(%rbp), %xmm0
	movq	-256(%rbp), %rcx
	movq	%rax, %rsi
	movhps	-248(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-296(%rbp), %xmm0
	movhps	-272(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r13, %rdi
	movq	%rax, -176(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder19ForJSObjectElementsEv@PLT
	movq	-192(%rbp), %rax
	movq	-168(%rbp), %rdx
	movq	-184(%rbp), %rsi
	movq	%rdx, -248(%rbp)
	movq	376(%rax), %rdi
	movq	-176(%rbp), %rdx
	movq	(%rax), %r14
	movq	%rsi, -296(%rbp)
	movq	%r13, %rsi
	movq	%rdx, -272(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movl	$4, %edx
	movq	%r14, %rdi
	movq	-296(%rbp), %xmm0
	movq	-256(%rbp), %rcx
	movq	%rax, %rsi
	movhps	-264(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-272(%rbp), %xmm0
	movhps	-248(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r13, %rdi
	movq	%rax, -176(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder18ForArgumentsLengthEv@PLT
	movq	-192(%rbp), %rax
	movq	-168(%rbp), %rdx
	movq	-184(%rbp), %rsi
	movq	%rdx, -248(%rbp)
	movq	376(%rax), %rdi
	movq	-176(%rbp), %rdx
	movq	(%rax), %r14
	movq	%rsi, -272(%rbp)
	movq	%r13, %rsi
	movq	%rdx, -264(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movl	$4, %edx
	movq	%r14, %rdi
	movq	-272(%rbp), %xmm0
	movq	-256(%rbp), %rcx
	movq	%rax, %rsi
	movhps	-280(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-264(%rbp), %xmm0
	movhps	-248(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r13, %rdi
	movq	%rax, -176(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder18ForArgumentsCalleeEv@PLT
	movq	-192(%rbp), %rax
	movq	-168(%rbp), %rdx
	movq	-184(%rbp), %rsi
	movq	%rdx, -248(%rbp)
	movq	376(%rax), %rdi
	movq	-176(%rbp), %rdx
	movq	(%rax), %r14
	movq	%rsi, -272(%rbp)
	movq	%r13, %rsi
	movq	%rdx, -264(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movl	$4, %edx
	movq	%r14, %rdi
	movq	-272(%rbp), %xmm0
	movq	-256(%rbp), %rcx
	movq	%rax, %rsi
	movhps	-288(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-264(%rbp), %xmm0
	movhps	-248(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -176(%rbp)
	jmp	.L822
	.p2align 4,,10
	.p2align 3
.L832:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rbx, %rdi
	movq	%rax, -272(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetContextInputEPNS1_4NodeE@PLT
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	%rax, -256(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZNK2v88internal8compiler21SharedFunctionInfoRef24has_duplicate_parametersEv@PLT
	testb	%al, %al
	jne	.L824
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler14NodeProperties18GetFrameStateInputEPNS1_4NodeE@PLT
	movq	(%rax), %rdi
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal8compiler16FrameStateInfoOfEPKNS1_8OperatorE@PLT
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L754
	cmpl	$1, (%rax)
	movq	-280(%rbp), %rdx
	jne	.L754
	leaq	32(%rdx), %rax
	movq	%rdx, %r15
	movq	%rax, -264(%rbp)
.L754:
	movq	-264(%rbp), %rax
	movq	(%rax), %rdx
	movzbl	23(%r15), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L755
	movq	16(%rdx), %rdx
.L755:
	movq	(%rdx), %rax
	cmpw	$60, 16(%rax)
	je	.L824
	movq	(%r15), %rdi
	call	_ZN2v88internal8compiler16FrameStateInfoOfEPKNS1_8OperatorE@PLT
	subq	$8, %rsp
	movq	%r15, %rcx
	movq	%r13, %rsi
	movq	16(%rax), %rax
	movq	-256(%rbp), %r8
	movq	%r14, %r9
	movq	%r12, %rdi
	movq	-248(%rbp), %rdx
	movb	$0, -225(%rbp)
	movq	%rax, -264(%rbp)
	leaq	-225(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler16JSCreateLowering24AllocateAliasedArgumentsEPNS1_4NodeES4_S4_S4_RKNS1_21SharedFunctionInfoRefEPb
	movq	24(%r12), %r10
	popq	%rdx
	movq	%rax, %r8
	movq	(%rax), %rax
	popq	%rcx
	movq	%r10, -280(%rbp)
	cmpb	$0, 36(%rax)
	movq	32(%r12), %rax
	movq	%r8, -256(%rbp)
	cmovne	%r8, %r13
	cmpb	$0, -225(%rbp)
	movzbl	24(%rax), %edx
	je	.L758
	testb	%dl, %dl
	je	.L731
	movdqu	32(%rax), %xmm5
	leaq	-208(%rbp), %rdi
	leaq	-192(%rbp), %r15
	movaps	%xmm5, -208(%rbp)
	call	_ZNK2v88internal8compiler16NativeContextRef26fast_aliased_arguments_mapEv@PLT
	movq	-256(%rbp), %r8
	movq	-280(%rbp), %r10
	movq	%rax, -160(%rbp)
	movq	%rdx, -152(%rbp)
.L759:
	leaq	-160(%rbp), %r14
	movq	%r10, %rdi
	movq	%r8, -256(%rbp)
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	%r13, -176(%rbp)
	movq	-256(%rbp), %r8
	movl	$-1, %r13d
	movq	$0, -184(%rbp)
	movq	%rax, %r10
	movq	24(%r12), %rax
	movq	%rax, -192(%rbp)
	movq	-248(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	-264(%rbp), %rax
	testq	%rax, %rax
	je	.L760
	movl	4(%rax), %r13d
	subl	$1, %r13d
.L760:
	movl	$4294967295, %ecx
	xorl	%edx, %edx
	movl	$40, %esi
	movq	%r15, %rdi
	movq	%r8, -256(%rbp)
	movq	%r10, -248(%rbp)
	call	_ZN2v88internal8compiler17AllocationBuilder8AllocateEiNS0_14AllocationTypeENS1_4TypeE
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	-248(%rbp), %r10
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%r10, %rdx
	call	_ZN2v88internal8compiler17AllocationBuilder5StoreERKNS1_11FieldAccessEPNS1_4NodeE
	movq	24(%r12), %rdi
	call	_ZN2v88internal8compiler7JSGraph23EmptyFixedArrayConstantEv@PLT
	movq	%r14, %rdi
	movq	%rax, -248(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder39ForJSObjectPropertiesOrHashKnownPointerEv@PLT
	movq	-248(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler17AllocationBuilder5StoreERKNS1_11FieldAccessEPNS1_4NodeE
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder19ForJSObjectElementsEv@PLT
	movq	-256(%rbp), %r8
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler17AllocationBuilder5StoreERKNS1_11FieldAccessEPNS1_4NodeE
	pxor	%xmm0, %xmm0
	movq	24(%r12), %rdi
	cvtsi2sdl	%r13d, %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13AccessBuilder18ForArgumentsLengthEv@PLT
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler17AllocationBuilder5StoreERKNS1_11FieldAccessEPNS1_4NodeE
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder18ForArgumentsCalleeEv@PLT
	movq	-272(%rbp), %rdx
.L823:
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler17AllocationBuilder5StoreERKNS1_11FieldAccessEPNS1_4NodeE
	jmp	.L822
	.p2align 4,,10
	.p2align 3
.L720:
	leaq	.LC26(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L733:
	movq	32(%rbx), %rsi
	movq	16(%rsi), %rdi
	leaq	16(%rsi), %r14
	cmpq	-256(%rbp), %rdi
	jne	.L734
.L736:
	movq	8(%r14), %rdi
	cmpq	%rdi, %r13
	je	.L749
.L829:
	addq	$8, %r14
	jmp	.L748
	.p2align 4,,10
	.p2align 3
.L742:
	movq	32(%rbx), %rsi
	movq	16(%rsi), %rdi
	leaq	16(%rsi), %r14
	cmpq	-256(%rbp), %rdi
	jne	.L743
.L745:
	movq	8(%r14), %rdi
	cmpq	%r13, %rdi
	jne	.L829
	jmp	.L749
	.p2align 4,,10
	.p2align 3
.L731:
	leaq	.LC15(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L833:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14NodeProperties18GetFrameStateInputEPNS1_4NodeE@PLT
	movq	(%rax), %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler16FrameStateInfoOfEPKNS1_8OperatorE@PLT
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L762
	cmpl	$1, (%rax)
	jne	.L762
	leaq	32(%r14), %rax
	movq	%r14, %r15
	movq	%rax, -264(%rbp)
.L762:
	movq	-264(%rbp), %rax
	movq	(%rax), %rdx
	movzbl	23(%r15), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L763
	movq	16(%rdx), %rdx
.L763:
	movq	(%rdx), %rax
	cmpw	$60, 16(%rax)
	je	.L824
	movq	(%r15), %rdi
	call	_ZN2v88internal8compiler16FrameStateInfoOfEPKNS1_8OperatorE@PLT
	movq	%r13, %rsi
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	16(%rax), %rax
	movq	-248(%rbp), %rdx
	movq	%rax, -256(%rbp)
	call	_ZN2v88internal8compiler16JSCreateLowering17AllocateArgumentsEPNS1_4NodeES4_S4_
	movq	24(%r12), %r8
	movq	%rax, %r10
	movq	(%rax), %rax
	movq	%r10, -272(%rbp)
	cmpb	$0, 36(%rax)
	movq	32(%r12), %rax
	movq	%r8, -264(%rbp)
	cmovne	%r10, %r13
	cmpb	$0, 24(%rax)
	je	.L731
	movdqu	32(%rax), %xmm7
	leaq	-192(%rbp), %r15
	leaq	-160(%rbp), %r14
	movq	%r15, %rdi
	movaps	%xmm7, -192(%rbp)
	call	_ZNK2v88internal8compiler16NativeContextRef20strict_arguments_mapEv@PLT
	movq	-264(%rbp), %r8
	movq	%r14, %rsi
	movq	%rax, -160(%rbp)
	movq	%r8, %rdi
	movq	%rdx, -152(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	%r13, -176(%rbp)
	movq	-272(%rbp), %r10
	movl	$-1, %r9d
	movq	%rax, -264(%rbp)
	movq	24(%r12), %rax
	movq	$0, -184(%rbp)
	movq	%rax, -192(%rbp)
	movq	-248(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	-256(%rbp), %rax
	testq	%rax, %rax
	je	.L767
	movl	4(%rax), %eax
	leal	-1(%rax), %r9d
.L767:
	movl	$4294967295, %ecx
	xorl	%edx, %edx
	movl	$32, %esi
	movq	%r15, %rdi
	movl	%r9d, -296(%rbp)
	movq	%r10, -288(%rbp)
	call	_ZN2v88internal8compiler17AllocationBuilder8AllocateEiNS0_14AllocationTypeENS1_4TypeE
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	-192(%rbp), %rax
	movq	-168(%rbp), %rcx
	movq	%r14, %rsi
	movq	-184(%rbp), %rdx
	movq	%rcx, -248(%rbp)
	movq	376(%rax), %rdi
	movq	-176(%rbp), %rcx
	movq	(%rax), %r13
	movq	%rdx, -272(%rbp)
	movq	%rcx, -256(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	leaq	-96(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	movq	%rax, %rsi
	movl	$4, %edx
	movq	-272(%rbp), %xmm0
	movq	%rcx, -280(%rbp)
	movhps	-264(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-256(%rbp), %xmm0
	movhps	-248(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	24(%r12), %rdi
	movq	%rax, -176(%rbp)
	call	_ZN2v88internal8compiler7JSGraph23EmptyFixedArrayConstantEv@PLT
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13AccessBuilder39ForJSObjectPropertiesOrHashKnownPointerEv@PLT
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler17AllocationBuilder5StoreERKNS1_11FieldAccessEPNS1_4NodeE
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder19ForJSObjectElementsEv@PLT
	movq	-288(%rbp), %r10
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%r10, %rdx
	call	_ZN2v88internal8compiler17AllocationBuilder5StoreERKNS1_11FieldAccessEPNS1_4NodeE
	movl	-296(%rbp), %r9d
	pxor	%xmm0, %xmm0
	movq	24(%r12), %rdi
	cvtsi2sdl	%r9d, %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%r14, %rdi
	movq	%rax, -248(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder18ForArgumentsLengthEv@PLT
	movq	-192(%rbp), %rax
	movq	-168(%rbp), %rcx
	movq	%r14, %rsi
	movq	-184(%rbp), %rdx
	movq	%rcx, -256(%rbp)
	movq	376(%rax), %rdi
	movq	-176(%rbp), %rcx
	movq	(%rax), %r13
	movq	%rdx, -272(%rbp)
	movq	%rcx, -264(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movl	$4, %edx
	movq	%r13, %rdi
	movq	-272(%rbp), %xmm0
	movq	-280(%rbp), %rcx
	movq	%rax, %rsi
	movhps	-248(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-264(%rbp), %xmm0
	movhps	-256(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -176(%rbp)
	.p2align 4,,10
	.p2align 3
.L822:
	movq	8(%r12), %rdi
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movq	%rbx, %rdx
	movq	%rbx, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler17AllocationBuilder15FinishAndChangeEPNS1_4NodeE
	movq	%rbx, %rax
	jmp	.L732
	.p2align 4,,10
	.p2align 3
.L837:
	cmpb	$0, 24(%rax)
	je	.L731
	movdqu	32(%rax), %xmm5
	leaq	-208(%rbp), %rdi
	leaq	-192(%rbp), %r15
	movaps	%xmm5, -208(%rbp)
	call	_ZNK2v88internal8compiler16NativeContextRef26fast_aliased_arguments_mapEv@PLT
	movq	%rax, -160(%rbp)
	movq	%rdx, -152(%rbp)
	jmp	.L730
	.p2align 4,,10
	.p2align 3
.L834:
	movq	32(%rbx), %rsi
	leaq	16(%rsi), %r14
	jmp	.L745
	.p2align 4,,10
	.p2align 3
.L836:
	movq	32(%rbx), %rsi
	leaq	16(%rsi), %r14
	jmp	.L736
.L758:
	testb	%dl, %dl
	je	.L731
	movdqu	32(%rax), %xmm6
	leaq	-192(%rbp), %r15
	movq	%r15, %rdi
	movaps	%xmm6, -192(%rbp)
	call	_ZNK2v88internal8compiler16NativeContextRef20sloppy_arguments_mapEv@PLT
	movq	-280(%rbp), %r10
	movq	-256(%rbp), %r8
	movq	%rax, -160(%rbp)
	movq	%rdx, -152(%rbp)
	jmp	.L759
.L835:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22938:
	.size	_ZN2v88internal8compiler16JSCreateLowering23ReduceJSCreateArgumentsEPNS1_4NodeE, .-_ZN2v88internal8compiler16JSCreateLowering23ReduceJSCreateArgumentsEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler16JSCreateLowering16AllocateElementsEPNS1_4NodeES4_NS0_12ElementsKindEiNS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16JSCreateLowering16AllocateElementsEPNS1_4NodeES4_NS0_12ElementsKindEiNS0_14AllocationTypeE
	.type	_ZN2v88internal8compiler16JSCreateLowering16AllocateElementsEPNS1_4NodeES4_NS0_12ElementsKindEiNS0_14AllocationTypeE, @function
_ZN2v88internal8compiler16JSCreateLowering16AllocateElementsEPNS1_4NodeES4_NS0_12ElementsKindEiNS0_14AllocationTypeE:
.LFB23009:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	subl	$4, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%r9d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$248, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -256(%rbp)
	movl	%r8d, -260(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	movq	360(%rax), %r15
	cmpb	$1, %cl
	jbe	.L846
	leaq	-192(%rbp), %rax
	addq	$152, %r15
	movq	%rax, %rdi
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder20ForFixedArrayElementEv@PLT
.L840:
	movq	24(%r12), %rdi
	call	_ZN2v88internal8compiler7JSGraph15TheHoleConstantEv@PLT
	movq	32(%r12), %rsi
	movq	%r15, %rdx
	movl	$1, %ecx
	leaq	-224(%rbp), %rdi
	movq	24(%r12), %rbx
	movq	%rax, -272(%rbp)
	leaq	-96(%rbp), %r15
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movdqa	-224(%rbp), %xmm2
	leaq	-208(%rbp), %r9
	movq	%r9, %rdi
	movq	%r9, -288(%rbp)
	movaps	%xmm2, -208(%rbp)
	call	_ZNK2v88internal8compiler6MapRef13instance_typeEv@PLT
	movq	(%rbx), %r11
	movq	8(%rbx), %rdi
	movl	$1, %esi
	movq	%r11, -232(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder11BeginRegionENS1_19RegionObservabilityE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$1, %edx
	movq	-232(%rbp), %r11
	movq	%rax, %rsi
	movq	%r13, -96(%rbp)
	movq	%r11, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	pxor	%xmm0, %xmm0
	movq	(%rbx), %r11
	movq	%rbx, %rdi
	movl	-260(%rbp), %r13d
	movq	%rax, -232(%rbp)
	movq	%r11, -248(%rbp)
	leal	16(,%r13,8), %eax
	cvtsi2sdl	%eax, %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	376(%rbx), %rdi
	movl	%r14d, %edx
	movl	$16777217, %esi
	movq	%rax, -240(%rbp)
	leaq	-160(%rbp), %r14
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder8AllocateENS1_4TypeENS0_14AllocationTypeE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$3, %edx
	movq	-248(%rbp), %r11
	movq	%rax, %rsi
	movq	-256(%rbp), %rax
	movq	-240(%rbp), %xmm0
	movq	%r11, %rdi
	movq	%rax, -80(%rbp)
	movhps	-232(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r14, %rdi
	movq	%rax, -248(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	-288(%rbp), %r9
	movq	%rbx, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	(%rbx), %r9
	movq	376(%rbx), %rdi
	movq	%r14, %rsi
	movq	%rax, -232(%rbp)
	movq	%r9, -240(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$4, %edx
	movq	-248(%rbp), %xmm1
	movq	-240(%rbp), %r9
	movq	%rax, %rsi
	movdqa	%xmm1, %xmm0
	movq	%r9, %rdi
	movhps	-232(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movdqa	%xmm1, %xmm0
	movhps	-256(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdi
	cvtsi2sdl	%r13d, %xmm0
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%r14, %rdi
	movq	%rax, -232(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder19ForFixedArrayLengthEv@PLT
	movq	(%rbx), %r9
	movq	376(%rbx), %rdi
	movq	%r14, %rsi
	movq	%r9, -288(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$4, %edx
	movq	-248(%rbp), %xmm0
	movq	-288(%rbp), %r9
	movq	%rax, %rsi
	movhps	-232(%rbp), %xmm0
	movq	%r9, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	-240(%rbp), %xmm0
	movhps	-256(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -232(%rbp)
	testl	%r13d, %r13d
	jle	.L841
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L842:
	pxor	%xmm0, %xmm0
	movq	24(%r12), %rdi
	cvtsi2sdl	%r14d, %xmm0
	addl	$1, %r14d
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	376(%rbx), %rdi
	movq	-280(%rbp), %rsi
	movq	(%rbx), %r13
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder12StoreElementERKNS1_13ElementAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$5, %edx
	movq	-248(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r13, %rdi
	movq	-256(%rbp), %rax
	movhps	-240(%rbp), %xmm0
	movq	%rax, -64(%rbp)
	movaps	%xmm0, -96(%rbp)
	movq	-272(%rbp), %xmm0
	movhps	-232(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -232(%rbp)
	cmpl	%r14d, -260(%rbp)
	jne	.L842
.L841:
	movq	8(%rbx), %rdi
	movq	(%rbx), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12FinishRegionEv@PLT
	xorl	%r8d, %r8d
	movl	$2, %edx
	movq	%r15, %rcx
	movq	-248(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r12, %rdi
	movhps	-232(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-56(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L847
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L846:
	.cfi_restore_state
	leaq	-192(%rbp), %rax
	addq	$488, %r15
	movq	%rax, %rdi
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder26ForFixedDoubleArrayElementEv@PLT
	jmp	.L840
.L847:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23009:
	.size	_ZN2v88internal8compiler16JSCreateLowering16AllocateElementsEPNS1_4NodeES4_NS0_12ElementsKindEiNS0_14AllocationTypeE, .-_ZN2v88internal8compiler16JSCreateLowering16AllocateElementsEPNS1_4NodeES4_NS0_12ElementsKindEiNS0_14AllocationTypeE
	.section	.text._ZN2v88internal8compiler16JSCreateLowering14ReduceNewArrayEPNS1_4NodeES4_iNS1_6MapRefENS0_12ElementsKindENS0_14AllocationTypeERKNS1_23SlackTrackingPredictionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16JSCreateLowering14ReduceNewArrayEPNS1_4NodeES4_iNS1_6MapRefENS0_12ElementsKindENS0_14AllocationTypeERKNS1_23SlackTrackingPredictionE
	.type	_ZN2v88internal8compiler16JSCreateLowering14ReduceNewArrayEPNS1_4NodeES4_iNS1_6MapRefENS0_12ElementsKindENS0_14AllocationTypeERKNS1_23SlackTrackingPredictionE, @function
_ZN2v88internal8compiler16JSCreateLowering14ReduceNewArrayEPNS1_4NodeES4_iNS1_6MapRefENS0_12ElementsKindENS0_14AllocationTypeERKNS1_23SlackTrackingPredictionE:
.LFB22942:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$248, %rsp
	movq	%rdx, -232(%rbp)
	movl	16(%rbp), %edx
	movl	24(%rbp), %ecx
	movq	%r8, -208(%rbp)
	movl	%edx, -224(%rbp)
	movq	32(%rbp), %rdx
	movq	%r9, -200(%rbp)
	movq	%rdx, -248(%rbp)
	movq	%rsi, -272(%rbp)
	xorl	%esi, %esi
	movl	%ecx, -216(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, -240(%rbp)
	movq	8(%r12), %rax
	leaq	-160(%rbp), %r12
	movq	%r12, %rdi
	movq	%rax, -160(%rbp)
	call	_ZNK2v88internal8compiler4Type3MaxEv@PLT
	comisd	.LC10(%rip), %xmm0
	movl	-224(%rbp), %edx
	jbe	.L851
	movl	$1, %r13d
	testb	%dl, %dl
	je	.L850
	cmpb	$4, %dl
	je	.L869
	cmpb	$2, %dl
	je	.L870
	movl	$7, %r13d
	cmpb	$6, %dl
	je	.L850
.L851:
	movzbl	%dl, %r13d
.L850:
	leaq	-208(%rbp), %rax
	leaq	-192(%rbp), %rdi
	movl	%r13d, %edx
	movq	%rax, %rsi
	movq	%rax, -256(%rbp)
	call	_ZNK2v88internal8compiler6MapRef14AsElementsKindENS0_12ElementsKindE@PLT
	cmpb	$0, -192(%rbp)
	je	.L884
	movdqu	-184(%rbp), %xmm2
	movaps	%xmm2, -208(%rbp)
	testl	%ebx, %ebx
	je	.L885
	movq	-240(%rbp), %rdx
	movq	%r15, %rsi
	movl	%ebx, %r8d
	movl	%r13d, %ecx
	movzbl	-216(%rbp), %r9d
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler16JSCreateLowering16AllocateElementsEPNS1_4NodeES4_NS0_12ElementsKindEiNS0_14AllocationTypeE
	movq	%rax, -264(%rbp)
	movq	%rax, %r15
.L855:
	movq	24(%r14), %rbx
	movq	-248(%rbp), %rax
	movl	$1, %esi
	movl	(%rax), %r11d
	movq	(%rbx), %r9
	movq	8(%rbx), %rdi
	movl	%r11d, -280(%rbp)
	movq	%r9, -224(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder11BeginRegionENS1_19RegionObservabilityE@PLT
	movq	-224(%rbp), %r9
	movq	%r15, -96(%rbp)
	leaq	-96(%rbp), %r15
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	pxor	%xmm0, %xmm0
	movq	(%rbx), %r9
	movq	%rbx, %rdi
	movl	-280(%rbp), %r11d
	movq	%rax, -224(%rbp)
	movq	%r9, -288(%rbp)
	cvtsi2sdl	%r11d, %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	376(%rbx), %rdi
	movl	$4294967295, %esi
	movzbl	-216(%rbp), %edx
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder8AllocateENS1_4TypeENS0_14AllocationTypeE@PLT
	movq	-288(%rbp), %r9
	xorl	%r8d, %r8d
	movq	-240(%rbp), %rcx
	movq	-280(%rbp), %xmm0
	movl	$3, %edx
	movq	%rax, %rsi
	movq	%r9, %rdi
	movq	%rcx, -80(%rbp)
	movq	%r15, %rcx
	movhps	-224(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r12, %rdi
	movq	%rax, -224(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	-256(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	(%rbx), %r9
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	%rax, -216(%rbp)
	movq	%r9, -280(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$4, %edx
	movq	-224(%rbp), %xmm1
	movq	-280(%rbp), %r9
	movq	%rax, %rsi
	movdqa	%xmm1, %xmm0
	movq	%r9, %rdi
	movhps	-216(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movdqa	%xmm1, %xmm0
	movhps	-240(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	24(%r14), %rdi
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal8compiler7JSGraph23EmptyFixedArrayConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder39ForJSObjectPropertiesOrHashKnownPointerEv@PLT
	movq	(%rbx), %r9
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	%r9, -288(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$4, %edx
	movq	-224(%rbp), %xmm0
	movq	-288(%rbp), %r9
	movq	%rax, %rsi
	movhps	-216(%rbp), %xmm0
	movq	%r9, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	-280(%rbp), %xmm0
	movhps	-240(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r12, %rdi
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder19ForJSObjectElementsEv@PLT
	movq	(%rbx), %r9
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	%r9, -280(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$4, %edx
	movq	-224(%rbp), %xmm0
	movq	-280(%rbp), %r9
	movq	%rax, %rsi
	movhps	-264(%rbp), %xmm0
	movq	%r9, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	-216(%rbp), %xmm0
	movhps	-240(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder16ForJSArrayLengthENS0_12ElementsKindE@PLT
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	(%rbx), %r13
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$4, %edx
	movq	-224(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r13, %rdi
	movhps	-232(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-216(%rbp), %xmm0
	movhps	-240(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r9
	movq	-248(%rbp), %rax
	movl	4(%rax), %eax
	testl	%eax, %eax
	jle	.L856
	movq	%r9, -216(%rbp)
	xorl	%r13d, %r13d
	movq	%r12, %rax
	movq	%r15, -264(%rbp)
	movl	%r13d, %r12d
	movq	%r14, %r15
	movq	%rax, %r13
	.p2align 4,,10
	.p2align 3
.L857:
	movq	24(%r15), %rdi
	call	_ZN2v88internal8compiler7JSGraph17UndefinedConstantEv@PLT
	movq	-256(%rbp), %rsi
	movl	%r12d, %edx
	movq	%r13, %rdi
	movq	%rax, -232(%rbp)
	addl	$1, %r12d
	call	_ZN2v88internal8compiler13AccessBuilder27ForJSObjectInObjectPropertyERKNS1_6MapRefEi@PLT
	movq	376(%rbx), %rdi
	movq	%r13, %rsi
	movq	(%rbx), %r14
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movl	$4, %edx
	movq	%r14, %rdi
	movq	-224(%rbp), %xmm0
	movq	-264(%rbp), %rcx
	movq	%rax, %rsi
	movhps	-232(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-216(%rbp), %xmm0
	movhps	-240(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -216(%rbp)
	movq	-248(%rbp), %rax
	cmpl	4(%rax), %r12d
	jl	.L857
	movq	-216(%rbp), %r9
	movq	%r15, %r14
.L856:
	movq	8(%r14), %rdi
	movq	-272(%rbp), %r15
	movq	%r9, -216(%rbp)
	xorl	%r8d, %r8d
	movq	(%rdi), %rax
	movq	%r15, %rcx
	movq	%r15, %rdx
	movq	%r15, %rsi
	call	*32(%rax)
	movq	8(%r15), %rax
	movq	%r15, %rcx
	movq	-224(%rbp), %rdx
	movq	-216(%rbp), %r9
	leaq	32(%r15), %r15
	movq	%rax, 8(%rdx)
	movzbl	23(%rcx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L858
	movq	32(%rcx), %rdi
	movq	%r15, %r14
	movq	%rcx, %rsi
	cmpq	%rdi, %rdx
	je	.L860
.L859:
	leaq	-24(%rsi), %r12
	testq	%rdi, %rdi
	je	.L862
	movq	%r12, %rsi
	movq	%r9, -216(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-216(%rbp), %r9
.L862:
	movq	-224(%rbp), %rax
	movq	%r12, %rsi
	movq	%r9, -216(%rbp)
	movq	%rax, (%r14)
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movq	-272(%rbp), %rax
	movq	-216(%rbp), %r9
	movzbl	23(%rax), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L886
.L860:
	movq	8(%r15), %rdi
	cmpq	%rdi, %r9
	je	.L865
	movq	-272(%rbp), %rsi
	addq	$8, %r15
.L864:
	leaq	-48(%rsi), %r12
	testq	%rdi, %rdi
	je	.L866
	movq	%r12, %rsi
	movq	%r9, -216(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-216(%rbp), %r9
.L866:
	movq	%r9, (%r15)
	testq	%r9, %r9
	je	.L865
	movq	%r12, %rsi
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L865:
	movq	-272(%rbp), %r14
	movl	$2, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node14TrimInputCountEi@PLT
	movq	8(%rbx), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12FinishRegionEv@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	%r14, %rax
.L853:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L887
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L885:
	.cfi_restore_state
	movq	24(%r14), %rdi
	call	_ZN2v88internal8compiler7JSGraph23EmptyFixedArrayConstantEv@PLT
	movq	%rax, -264(%rbp)
	jmp	.L855
	.p2align 4,,10
	.p2align 3
.L884:
	movq	32(%r14), %rdi
	movl	$518, %edx
	leaq	.LC12(%rip), %rsi
	call	_ZN2v88internal8compiler28NoChangeBecauseOfMissingDataEPNS1_12JSHeapBrokerEPKci@PLT
	jmp	.L853
	.p2align 4,,10
	.p2align 3
.L858:
	movq	-272(%rbp), %rax
	movq	32(%rax), %rsi
	movq	16(%rsi), %rdi
	leaq	16(%rsi), %r14
	cmpq	%rdi, -224(%rbp)
	jne	.L859
	movq	8(%r14), %rdi
	cmpq	%rdi, %r9
	je	.L865
.L888:
	leaq	8(%r14), %r15
	jmp	.L864
	.p2align 4,,10
	.p2align 3
.L886:
	movq	-272(%rbp), %rax
	movq	32(%rax), %rsi
	movq	24(%rsi), %rdi
	leaq	16(%rsi), %r14
	cmpq	%rdi, %r9
	jne	.L888
	jmp	.L865
.L869:
	movl	$5, %r13d
	jmp	.L850
.L870:
	movl	$3, %r13d
	jmp	.L850
.L887:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22942:
	.size	_ZN2v88internal8compiler16JSCreateLowering14ReduceNewArrayEPNS1_4NodeES4_iNS1_6MapRefENS0_12ElementsKindENS0_14AllocationTypeERKNS1_23SlackTrackingPredictionE, .-_ZN2v88internal8compiler16JSCreateLowering14ReduceNewArrayEPNS1_4NodeES4_iNS1_6MapRefENS0_12ElementsKindENS0_14AllocationTypeERKNS1_23SlackTrackingPredictionE
	.section	.text._ZN2v88internal8compiler16JSCreateLowering31ReduceJSCreateEmptyLiteralArrayEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16JSCreateLowering31ReduceJSCreateEmptyLiteralArrayEPNS1_4NodeE
	.type	_ZN2v88internal8compiler16JSCreateLowering31ReduceJSCreateEmptyLiteralArrayEPNS1_4NodeE, @function
_ZN2v88internal8compiler16JSCreateLowering31ReduceJSCreateEmptyLiteralArrayEPNS1_4NodeE:
.LFB22984:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-144(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler19FeedbackParameterOfEPKNS1_8OperatorE@PLT
	movq	32(%r12), %rsi
	movl	$1, %ecx
	movq	%r14, %rdi
	movq	(%rax), %rdx
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler17FeedbackVectorRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movl	8(%rbx), %esi
	movq	%r14, %rdi
	leaq	-128(%rbp), %r14
	call	_ZNK2v88internal8compiler17FeedbackVectorRef3getENS0_12FeedbackSlotE@PLT
	movq	%r14, %rdi
	movq	%rax, -128(%rbp)
	movq	%rdx, -120(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef16IsAllocationSiteEv@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	testb	%r8b, %r8b
	jne	.L897
.L892:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L898
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L897:
	.cfi_restore_state
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef16AsAllocationSiteEv@PLT
	movq	%rax, -112(%rbp)
	movq	32(%r12), %rax
	movq	%rdx, -104(%rbp)
	cmpb	$0, 24(%rax)
	je	.L899
	movdqu	32(%rax), %xmm0
	leaq	-112(%rbp), %r14
	leaq	-80(%rbp), %r15
	movq	%r14, %rdi
	movaps	%xmm0, -80(%rbp)
	call	_ZNK2v88internal8compiler17AllocationSiteRef15GetElementsKindEv@PLT
	movq	%r15, %rdi
	movzbl	%al, %esi
	call	_ZNK2v88internal8compiler16NativeContextRef20GetInitialJSArrayMapENS0_12ElementsKindE@PLT
	movq	16(%r12), %rdi
	movq	%r14, %rsi
	movq	%rdx, -88(%rbp)
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler23CompilationDependencies21DependOnPretenureModeERKNS1_17AllocationSiteRefE@PLT
	movq	16(%r12), %rdi
	movq	%r14, %rsi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler23CompilationDependencies20DependOnElementsKindERKNS1_17AllocationSiteRefE@PLT
	movq	24(%r12), %rdi
	call	_ZN2v88internal8compiler7JSGraph12ZeroConstantEv@PLT
	leaq	-96(%rbp), %r8
	movq	%r8, %rdi
	movq	%r8, -152(%rbp)
	movq	%rax, %r14
	call	_ZNK2v88internal8compiler6MapRef13instance_sizeEv@PLT
	movq	-96(%rbp), %rsi
	movq	-88(%rbp), %rdx
	movq	%r15, %rdi
	movl	%eax, %ecx
	call	_ZN2v88internal8compiler23SlackTrackingPredictionC1ENS1_6MapRefEi@PLT
	movq	-152(%rbp), %r8
	movq	%r8, %rdi
	call	_ZNK2v88internal8compiler6MapRef13elements_kindEv@PLT
	subq	$8, %rsp
	movq	-96(%rbp), %r8
	xorl	%ecx, %ecx
	movzbl	%al, %eax
	pushq	%r15
	movq	-88(%rbp), %r9
	movq	%r14, %rdx
	pushq	%rbx
	movq	%r13, %rsi
	movq	%r12, %rdi
	pushq	%rax
	call	_ZN2v88internal8compiler16JSCreateLowering14ReduceNewArrayEPNS1_4NodeES4_iNS1_6MapRefENS0_12ElementsKindENS0_14AllocationTypeERKNS1_23SlackTrackingPredictionE
	addq	$32, %rsp
	jmp	.L892
	.p2align 4,,10
	.p2align 3
.L899:
	leaq	.LC15(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L898:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22984:
	.size	_ZN2v88internal8compiler16JSCreateLowering31ReduceJSCreateEmptyLiteralArrayEPNS1_4NodeE, .-_ZN2v88internal8compiler16JSCreateLowering31ReduceJSCreateEmptyLiteralArrayEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler16JSCreateLowering16AllocateElementsEPNS1_4NodeES4_NS0_12ElementsKindERKSt6vectorIS4_SaIS4_EENS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16JSCreateLowering16AllocateElementsEPNS1_4NodeES4_NS0_12ElementsKindERKSt6vectorIS4_SaIS4_EENS0_14AllocationTypeE
	.type	_ZN2v88internal8compiler16JSCreateLowering16AllocateElementsEPNS1_4NodeES4_NS0_12ElementsKindERKSt6vectorIS4_SaIS4_EENS0_14AllocationTypeE, @function
_ZN2v88internal8compiler16JSCreateLowering16AllocateElementsEPNS1_4NodeES4_NS0_12ElementsKindERKSt6vectorIS4_SaIS4_EENS0_14AllocationTypeE:
.LFB23010:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	subl	$4, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$248, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -272(%rbp)
	movq	8(%r8), %rbx
	movl	%r9d, -248(%rbp)
	subq	(%r8), %rbx
	sarq	$3, %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	movq	360(%rax), %rdx
	cmpb	$1, %cl
	jbe	.L908
	leaq	-192(%rbp), %rax
	addq	$152, %rdx
	movq	%rax, %rdi
	movq	%rdx, -232(%rbp)
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder20ForFixedArrayElementEv@PLT
	movq	-232(%rbp), %rdx
.L902:
	movq	32(%r15), %rsi
	movl	$1, %ecx
	leaq	-224(%rbp), %rdi
	movq	24(%r15), %r12
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movdqa	-224(%rbp), %xmm2
	leaq	-208(%rbp), %r11
	movq	%r11, %rdi
	movq	%r11, -288(%rbp)
	movaps	%xmm2, -208(%rbp)
	call	_ZNK2v88internal8compiler6MapRef13instance_typeEv@PLT
	movq	(%r12), %r9
	movq	8(%r12), %rdi
	movl	$1, %esi
	movq	%r9, -232(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder11BeginRegionENS1_19RegionObservabilityE@PLT
	movq	-232(%rbp), %r9
	movq	%r13, -96(%rbp)
	leaq	-96(%rbp), %r13
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	pxor	%xmm0, %xmm0
	movq	(%r12), %r9
	movq	%r12, %rdi
	movq	%rax, -232(%rbp)
	leal	16(,%rbx,8), %eax
	cvtsi2sdl	%eax, %xmm0
	movq	%r9, -256(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	376(%r12), %rdi
	movl	$16777217, %esi
	movzbl	-248(%rbp), %edx
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder8AllocateENS1_4TypeENS0_14AllocationTypeE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$3, %edx
	movq	-256(%rbp), %r9
	movq	%rax, %rsi
	movq	-272(%rbp), %rax
	movq	-240(%rbp), %xmm0
	movq	%r9, %rdi
	movq	%rax, -80(%rbp)
	movhps	-232(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	leaq	-160(%rbp), %r9
	movq	%r9, %rdi
	movq	%r9, -248(%rbp)
	movq	%rax, -264(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	-288(%rbp), %r11
	movq	%r12, %rdi
	movq	%r11, %rsi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	-248(%rbp), %r9
	movq	(%r12), %r11
	movq	376(%r12), %rdi
	movq	%rax, -232(%rbp)
	movq	%r9, %rsi
	movq	%r11, -240(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$4, %edx
	movq	-264(%rbp), %xmm1
	movq	-240(%rbp), %r11
	movq	%rax, %rsi
	movdqa	%xmm1, %xmm0
	movq	%r11, %rdi
	movhps	-232(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movdqa	%xmm1, %xmm0
	movhps	-272(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	cvtsi2sdl	%ebx, %xmm0
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	-248(%rbp), %r9
	movq	%rax, -232(%rbp)
	movq	%r9, %rdi
	movq	%r9, -256(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder19ForFixedArrayLengthEv@PLT
	movq	-256(%rbp), %r9
	movq	(%r12), %r11
	movq	376(%r12), %rdi
	movq	%r9, %rsi
	movq	%r11, -248(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$4, %edx
	movq	-264(%rbp), %xmm0
	movq	-248(%rbp), %r11
	movq	%rax, %rsi
	movhps	-232(%rbp), %xmm0
	movq	%r11, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	-240(%rbp), %xmm0
	movhps	-272(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -232(%rbp)
	testl	%ebx, %ebx
	jle	.L903
	leal	-1(%rbx), %eax
	xorl	%ebx, %ebx
	movq	%rax, -288(%rbp)
	.p2align 4,,10
	.p2align 3
.L904:
	pxor	%xmm0, %xmm0
	movq	24(%r15), %rdi
	cvtsi2sdl	%ebx, %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	(%r12), %r11
	movq	376(%r12), %rdi
	movq	%rax, -248(%rbp)
	movq	(%r14), %rax
	movq	-280(%rbp), %rsi
	movq	%r11, -256(%rbp)
	movq	(%rax,%rbx,8), %rax
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder12StoreElementERKNS1_13ElementAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$5, %edx
	movq	-264(%rbp), %xmm0
	movq	-256(%rbp), %r11
	movq	%rax, %rsi
	movq	-272(%rbp), %rax
	movhps	-248(%rbp), %xmm0
	movq	%r11, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	-240(%rbp), %xmm0
	movq	%rax, -64(%rbp)
	movhps	-232(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -232(%rbp)
	movq	%rbx, %rax
	addq	$1, %rbx
	cmpq	%rax, -288(%rbp)
	jne	.L904
.L903:
	movq	8(%r12), %rdi
	movq	(%r12), %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12FinishRegionEv@PLT
	xorl	%r8d, %r8d
	movl	$2, %edx
	movq	%r13, %rcx
	movq	-264(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r14, %rdi
	movhps	-232(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-56(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L909
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L908:
	.cfi_restore_state
	leaq	-192(%rbp), %rax
	addq	$488, %rdx
	movq	%rax, %rdi
	movq	%rdx, -232(%rbp)
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder26ForFixedDoubleArrayElementEv@PLT
	movq	-232(%rbp), %rdx
	jmp	.L902
.L909:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23010:
	.size	_ZN2v88internal8compiler16JSCreateLowering16AllocateElementsEPNS1_4NodeES4_NS0_12ElementsKindERKSt6vectorIS4_SaIS4_EENS0_14AllocationTypeE, .-_ZN2v88internal8compiler16JSCreateLowering16AllocateElementsEPNS1_4NodeES4_NS0_12ElementsKindERKSt6vectorIS4_SaIS4_EENS0_14AllocationTypeE
	.section	.text._ZN2v88internal8compiler16JSCreateLowering14ReduceNewArrayEPNS1_4NodeESt6vectorIS4_SaIS4_EENS1_6MapRefENS0_12ElementsKindENS0_14AllocationTypeERKNS1_23SlackTrackingPredictionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16JSCreateLowering14ReduceNewArrayEPNS1_4NodeESt6vectorIS4_SaIS4_EENS1_6MapRefENS0_12ElementsKindENS0_14AllocationTypeERKNS1_23SlackTrackingPredictionE
	.type	_ZN2v88internal8compiler16JSCreateLowering14ReduceNewArrayEPNS1_4NodeESt6vectorIS4_SaIS4_EENS1_6MapRefENS0_12ElementsKindENS0_14AllocationTypeERKNS1_23SlackTrackingPredictionE, @function
_ZN2v88internal8compiler16JSCreateLowering14ReduceNewArrayEPNS1_4NodeESt6vectorIS4_SaIS4_EENS1_6MapRefENS0_12ElementsKindENS0_14AllocationTypeERKNS1_23SlackTrackingPredictionE:
.LFB22943:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movq	%r15, %rdi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%r9d, %ebx
	subq	$264, %rsp
	movq	%rcx, -208(%rbp)
	movq	24(%rbp), %rdx
	movl	16(%rbp), %ecx
	movq	%r8, -200(%rbp)
	movq	%rdx, -248(%rbp)
	movq	%rsi, -272(%rbp)
	xorl	%esi, %esi
	movl	%ecx, -224(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movzbl	%bl, %edx
	leaq	-192(%rbp), %rdi
	movq	%rax, -232(%rbp)
	leaq	-208(%rbp), %rax
	movq	%rax, %rsi
	movl	%edx, -240(%rbp)
	movq	%rax, -256(%rbp)
	call	_ZNK2v88internal8compiler6MapRef14AsElementsKindENS0_12ElementsKindE@PLT
	cmpb	$0, -192(%rbp)
	je	.L957
	movdqu	-184(%rbp), %xmm2
	movaps	%xmm2, -208(%rbp)
	cmpb	$1, %bl
	jbe	.L958
	subl	$4, %ebx
	leaq	-160(%rbp), %r12
	leaq	-96(%rbp), %r15
	cmpb	$1, %bl
	jbe	.L959
.L914:
	movl	-240(%rbp), %ecx
	movq	%r13, %r8
	movq	%r14, %rdi
	movq	-232(%rbp), %rdx
	movzbl	-224(%rbp), %r9d
	movq	-216(%rbp), %rsi
	call	_ZN2v88internal8compiler16JSCreateLowering16AllocateElementsEPNS1_4NodeES4_NS0_12ElementsKindERKSt6vectorIS4_SaIS4_EENS0_14AllocationTypeE
	pxor	%xmm0, %xmm0
	movq	24(%r14), %rdi
	movq	%rax, -216(%rbp)
	movq	8(%r13), %rax
	subq	0(%r13), %rax
	sarq	$3, %rax
	cvtsi2sdl	%eax, %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	24(%r14), %rbx
	movl	$1, %esi
	movq	%rax, -280(%rbp)
	movq	-248(%rbp), %rax
	movq	(%rbx), %r9
	movq	8(%rbx), %rdi
	movl	(%rax), %r10d
	movq	%r9, -264(%rbp)
	movl	%r10d, -288(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder11BeginRegionENS1_19RegionObservabilityE@PLT
	movq	-216(%rbp), %rdx
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	-264(%rbp), %r9
	movq	%rax, %rsi
	movq	%rdx, -96(%rbp)
	movl	$1, %edx
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	pxor	%xmm0, %xmm0
	movq	(%rbx), %r9
	movq	%rbx, %rdi
	movl	-288(%rbp), %r10d
	movq	%rax, -264(%rbp)
	movq	%r9, -296(%rbp)
	cvtsi2sdl	%r10d, %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	376(%rbx), %rdi
	movl	$4294967295, %esi
	movzbl	-224(%rbp), %edx
	movq	%rax, -288(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder8AllocateENS1_4TypeENS0_14AllocationTypeE@PLT
	movq	-296(%rbp), %r9
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	-232(%rbp), %rdx
	movq	%rax, %rsi
	movq	-288(%rbp), %xmm0
	movq	%r9, %rdi
	movhps	-264(%rbp), %xmm0
	movq	%rdx, -80(%rbp)
	movl	$3, %edx
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r12, %rdi
	movq	%rax, -224(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	-256(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	(%rbx), %r9
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	%rax, -264(%rbp)
	movq	%r9, -288(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$4, %edx
	movq	-224(%rbp), %xmm1
	movq	-288(%rbp), %r9
	movq	%rax, %rsi
	movdqa	%xmm1, %xmm0
	movq	%r9, %rdi
	movhps	-264(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movdqa	%xmm1, %xmm0
	movhps	-232(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	24(%r14), %rdi
	movq	%rax, -288(%rbp)
	call	_ZN2v88internal8compiler7JSGraph23EmptyFixedArrayConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, -264(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder39ForJSObjectPropertiesOrHashKnownPointerEv@PLT
	movq	(%rbx), %r9
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	%r9, -296(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$4, %edx
	movq	-224(%rbp), %xmm0
	movq	-296(%rbp), %r9
	movq	%rax, %rsi
	movhps	-264(%rbp), %xmm0
	movq	%r9, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	-288(%rbp), %xmm0
	movhps	-232(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r12, %rdi
	movq	%rax, -264(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder19ForJSObjectElementsEv@PLT
	movq	(%rbx), %r9
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	%r9, -288(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$4, %edx
	movq	-224(%rbp), %xmm0
	movq	-288(%rbp), %r9
	movq	%rax, %rsi
	movhps	-216(%rbp), %xmm0
	movq	%r9, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	-264(%rbp), %xmm0
	movhps	-232(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	-240(%rbp), %esi
	movq	%r12, %rdi
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder16ForJSArrayLengthENS0_12ElementsKindE@PLT
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	(%rbx), %r13
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$4, %edx
	movq	-224(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r13, %rdi
	movhps	-280(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-216(%rbp), %xmm0
	movhps	-232(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r9
	movq	-248(%rbp), %rax
	movl	4(%rax), %eax
	testl	%eax, %eax
	jle	.L923
	movq	%r9, -216(%rbp)
	xorl	%r13d, %r13d
	movq	%r12, %rax
	movq	%r15, -264(%rbp)
	movl	%r13d, %r12d
	movq	%rax, %r13
	.p2align 4,,10
	.p2align 3
.L924:
	movq	24(%r14), %rdi
	call	_ZN2v88internal8compiler7JSGraph17UndefinedConstantEv@PLT
	movq	-256(%rbp), %rsi
	movl	%r12d, %edx
	movq	%r13, %rdi
	movq	%rax, -240(%rbp)
	addl	$1, %r12d
	call	_ZN2v88internal8compiler13AccessBuilder27ForJSObjectInObjectPropertyERKNS1_6MapRefEi@PLT
	movq	376(%rbx), %rdi
	movq	%r13, %rsi
	movq	(%rbx), %r15
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movl	$4, %edx
	movq	%r15, %rdi
	movq	-224(%rbp), %xmm0
	movq	-264(%rbp), %rcx
	movq	%rax, %rsi
	movhps	-240(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-216(%rbp), %xmm0
	movhps	-232(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -216(%rbp)
	movq	-248(%rbp), %rax
	cmpl	4(%rax), %r12d
	jl	.L924
	movq	-216(%rbp), %r9
.L923:
	movq	8(%r14), %rdi
	movq	-272(%rbp), %r14
	movq	%r9, -216(%rbp)
	xorl	%r8d, %r8d
	movq	(%rdi), %rax
	movq	%r14, %rcx
	movq	%r14, %rdx
	movq	%r14, %rsi
	leaq	32(%r14), %r15
	call	*32(%rax)
	movq	8(%r14), %rax
	movq	-224(%rbp), %rdx
	movq	%r14, %rcx
	movq	-216(%rbp), %r9
	movq	%rax, 8(%rdx)
	movzbl	23(%r14), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L925
	movq	32(%r14), %rdi
	movq	%rcx, %rsi
	movq	%r15, %r14
	cmpq	%rdi, %rdx
	je	.L927
.L926:
	leaq	-24(%rsi), %r12
	testq	%rdi, %rdi
	je	.L929
	movq	%r12, %rsi
	movq	%r9, -216(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-216(%rbp), %r9
.L929:
	movq	-224(%rbp), %rax
	movq	%r12, %rsi
	movq	%r9, -216(%rbp)
	movq	%rax, (%r14)
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movq	-272(%rbp), %rax
	movq	-216(%rbp), %r9
	movzbl	23(%rax), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L960
.L927:
	movq	8(%r15), %rdi
	cmpq	%rdi, %r9
	je	.L932
	movq	-272(%rbp), %rsi
	addq	$8, %r15
.L931:
	leaq	-48(%rsi), %r12
	testq	%rdi, %rdi
	je	.L933
	movq	%r12, %rsi
	movq	%r9, -216(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-216(%rbp), %r9
.L933:
	movq	%r9, (%r15)
	testq	%r9, %r9
	je	.L932
	movq	%r12, %rsi
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L932:
	movq	-272(%rbp), %r14
	movl	$2, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node14TrimInputCountEi@PLT
	movq	8(%rbx), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12FinishRegionEv@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	%r14, %rax
.L912:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L961
	addq	$264, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L958:
	.cfi_restore_state
	movq	0(%r13), %r9
	movq	8(%r13), %rax
	leaq	-160(%rbp), %r12
	leaq	-96(%rbp), %r15
	cmpq	%rax, %r9
	je	.L914
	movq	%r13, -288(%rbp)
	leaq	-160(%rbp), %r12
	movq	%r9, %r13
	movq	%rax, %rbx
	leaq	-96(%rbp), %r15
	.p2align 4,,10
	.p2align 3
.L918:
	movq	0(%r13), %rax
	movq	8(%rax), %rax
	movq	%rax, -160(%rbp)
	call	_ZN2v88internal8compiler10BitsetType11SignedSmallEv@PLT
	movl	%eax, %esi
	orl	$1, %esi
	cmpq	%rsi, -160(%rbp)
	je	.L916
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L916
	movq	24(%r14), %rax
	movq	0(%r13), %xmm0
	movq	%r12, %rsi
	movq	(%rax), %r10
	movq	376(%rax), %rdi
	movq	%xmm0, -264(%rbp)
	movq	$0, -160(%rbp)
	movq	%r10, -280(%rbp)
	movl	$-1, -152(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder8CheckSmiERKNS1_14FeedbackSourceE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$3, %edx
	movq	-264(%rbp), %xmm0
	movq	-280(%rbp), %r10
	movq	%rax, %rsi
	movq	-232(%rbp), %rax
	movhps	-216(%rbp), %xmm0
	movq	%r10, %rdi
	movq	%rax, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -216(%rbp)
	movq	%rax, 0(%r13)
.L916:
	addq	$8, %r13
	cmpq	%r13, %rbx
	jne	.L918
	movq	-288(%rbp), %r13
	jmp	.L914
	.p2align 4,,10
	.p2align 3
.L957:
	movq	32(%r14), %rdi
	movl	$559, %edx
	leaq	.LC12(%rip), %rsi
	call	_ZN2v88internal8compiler28NoChangeBecauseOfMissingDataEPNS1_12JSHeapBrokerEPKci@PLT
	jmp	.L912
	.p2align 4,,10
	.p2align 3
.L925:
	movq	-272(%rbp), %rax
	movq	32(%rax), %rsi
	movq	16(%rsi), %rdi
	leaq	16(%rsi), %r14
	cmpq	%rdi, -224(%rbp)
	jne	.L926
	movq	8(%r14), %rdi
	cmpq	%rdi, %r9
	je	.L932
.L962:
	leaq	8(%r14), %r15
	jmp	.L931
	.p2align 4,,10
	.p2align 3
.L959:
	movq	8(%r13), %rax
	movq	0(%r13), %r9
	movq	%rax, -264(%rbp)
	cmpq	%r9, %rax
	je	.L914
	movq	%r12, -280(%rbp)
	movq	%r13, -288(%rbp)
	movq	%r9, %r13
	.p2align 4,,10
	.p2align 3
.L922:
	movq	0(%r13), %rax
	movq	8(%rax), %rax
	movq	%rax, -160(%rbp)
	cmpq	$7263, %rax
	je	.L921
	movq	-280(%rbp), %rdi
	movl	$7263, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L921
	movq	24(%r14), %rax
	movq	-280(%rbp), %rsi
	movq	0(%r13), %rbx
	movq	376(%rax), %rdi
	movq	(%rax), %r12
	movq	$0, -160(%rbp)
	movl	$-1, -152(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder11CheckNumberERKNS1_14FeedbackSourceE@PLT
	movq	%rbx, %xmm0
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%rax, %rsi
	movq	-232(%rbp), %rax
	movhps	-216(%rbp), %xmm0
	movq	%r12, %rdi
	movl	$3, %edx
	movaps	%xmm0, -96(%rbp)
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -216(%rbp)
	movq	%rax, 0(%r13)
.L921:
	movq	24(%r14), %rax
	movq	0(%r13), %r12
	addq	$8, %r13
	movq	376(%rax), %rdi
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder16NumberSilenceNaNEv@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%rbx, %rdi
	movq	%r12, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -8(%r13)
	cmpq	%r13, -264(%rbp)
	jne	.L922
	movq	-288(%rbp), %r13
	movq	-280(%rbp), %r12
	jmp	.L914
	.p2align 4,,10
	.p2align 3
.L960:
	movq	-272(%rbp), %rax
	movq	32(%rax), %rsi
	movq	24(%rsi), %rdi
	leaq	16(%rsi), %r14
	cmpq	%rdi, %r9
	jne	.L962
	jmp	.L932
.L961:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22943:
	.size	_ZN2v88internal8compiler16JSCreateLowering14ReduceNewArrayEPNS1_4NodeESt6vectorIS4_SaIS4_EENS1_6MapRefENS0_12ElementsKindENS0_14AllocationTypeERKNS1_23SlackTrackingPredictionE, .-_ZN2v88internal8compiler16JSCreateLowering14ReduceNewArrayEPNS1_4NodeESt6vectorIS4_SaIS4_EENS1_6MapRefENS0_12ElementsKindENS0_14AllocationTypeERKNS1_23SlackTrackingPredictionE
	.section	.text._ZN2v88internal8compiler16JSCreateLowering21AllocateLiteralRegExpEPNS1_4NodeES4_NS1_11JSRegExpRefE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16JSCreateLowering21AllocateLiteralRegExpEPNS1_4NodeES4_NS1_11JSRegExpRefE
	.type	_ZN2v88internal8compiler16JSCreateLowering21AllocateLiteralRegExpEPNS1_4NodeES4_NS1_11JSRegExpRefE, @function
_ZN2v88internal8compiler16JSCreateLowering21AllocateLiteralRegExpEPNS1_4NodeES4_NS1_11JSRegExpRefE:
.LFB23034:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-208(%rbp), %r14
	leaq	-192(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r14, %rdi
	subq	$216, %rsp
	movq	%rcx, -208(%rbp)
	movq	%r8, -200(%rbp)
	movq	%rdx, -224(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal8compiler13HeapObjectRef3mapEv@PLT
	movq	%r15, %rdi
	movq	24(%rbx), %rbx
	movq	%rdx, -184(%rbp)
	movq	%rax, -192(%rbp)
	call	_ZN2v88internal8compiler10BitsetType3LubINS1_6MapRefEEEjRKT_@PLT
	movl	%eax, %edi
	call	_ZN2v88internal8compiler10BitsetType15ExpandInternalsEj@PLT
	movq	(%rbx), %r9
	movq	8(%rbx), %rdi
	movl	$1, %esi
	movl	%eax, %r12d
	movq	%r9, -216(%rbp)
	orl	$1, %r12d
	call	_ZN2v88internal8compiler21CommonOperatorBuilder11BeginRegionENS1_19RegionObservabilityE@PLT
	movq	-216(%rbp), %r9
	movq	%r13, -96(%rbp)
	leaq	-96(%rbp), %r13
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	(%rbx), %r9
	movsd	.LC22(%rip), %xmm0
	movq	%rbx, %rdi
	movq	%rax, -216(%rbp)
	movq	%r9, -240(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	376(%rbx), %rdi
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rax, -232(%rbp)
	leaq	-160(%rbp), %r12
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder8AllocateENS1_4TypeENS0_14AllocationTypeE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$3, %edx
	movq	-240(%rbp), %r9
	movq	%rax, %rsi
	movq	-224(%rbp), %rax
	movq	-232(%rbp), %xmm0
	movq	%r9, %rdi
	movq	%rax, -80(%rbp)
	movhps	-216(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r12, %rdi
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	(%rbx), %r15
	movq	%rax, -232(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	%r15, %rdi
	movq	%rax, %rsi
	movl	$4, %edx
	movq	-216(%rbp), %xmm1
	leaq	-176(%rbp), %r15
	movdqa	%xmm1, %xmm0
	movhps	-232(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movdqa	%xmm1, %xmm0
	movhps	-224(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r14, %rdi
	movq	%rax, -240(%rbp)
	call	_ZNK2v88internal8compiler11JSRegExpRef22raw_properties_or_hashEv@PLT
	movq	%r12, %rdi
	movq	%rdx, -168(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder27ForJSObjectPropertiesOrHashEv@PLT
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	(%rbx), %r9
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	%rax, -232(%rbp)
	movq	%r9, -248(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$4, %edx
	movq	-216(%rbp), %xmm0
	movq	-248(%rbp), %r9
	movq	%rax, %rsi
	movhps	-232(%rbp), %xmm0
	movq	%r9, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	-240(%rbp), %xmm0
	movhps	-224(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r14, %rdi
	movq	%rax, -240(%rbp)
	call	_ZNK2v88internal8compiler11JSObjectRef8elementsEv@PLT
	movq	%r12, %rdi
	movq	%rdx, -168(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder19ForJSObjectElementsEv@PLT
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	(%rbx), %r9
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	%rax, -232(%rbp)
	movq	%r9, -248(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$4, %edx
	movq	-216(%rbp), %xmm0
	movq	-248(%rbp), %r9
	movq	%rax, %rsi
	movhps	-232(%rbp), %xmm0
	movq	%r9, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	-240(%rbp), %xmm0
	movhps	-224(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r14, %rdi
	movq	%rax, -240(%rbp)
	call	_ZNK2v88internal8compiler11JSRegExpRef4dataEv@PLT
	movq	%r12, %rdi
	movq	%rdx, -168(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder15ForJSRegExpDataEv@PLT
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	(%rbx), %r9
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	%rax, -232(%rbp)
	movq	%r9, -248(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$4, %edx
	movq	-216(%rbp), %xmm0
	movq	-248(%rbp), %r9
	movq	%rax, %rsi
	movhps	-232(%rbp), %xmm0
	movq	%r9, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	-240(%rbp), %xmm0
	movhps	-224(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r14, %rdi
	movq	%rax, -240(%rbp)
	call	_ZNK2v88internal8compiler11JSRegExpRef6sourceEv@PLT
	movq	%r12, %rdi
	movq	%rdx, -168(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder17ForJSRegExpSourceEv@PLT
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	(%rbx), %r9
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	%rax, -232(%rbp)
	movq	%r9, -248(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$4, %edx
	movq	-216(%rbp), %xmm0
	movq	-248(%rbp), %r9
	movq	%rax, %rsi
	movhps	-232(%rbp), %xmm0
	movq	%r9, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	-240(%rbp), %xmm0
	movhps	-224(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r14, %rdi
	movq	%rax, -240(%rbp)
	call	_ZNK2v88internal8compiler11JSRegExpRef5flagsEv@PLT
	movq	%r12, %rdi
	movq	%rdx, -168(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder16ForJSRegExpFlagsEv@PLT
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	(%rbx), %r9
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	%rax, -232(%rbp)
	movq	%r9, -248(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$4, %edx
	movq	-216(%rbp), %xmm0
	movq	-248(%rbp), %r9
	movq	%rax, %rsi
	movhps	-232(%rbp), %xmm0
	movq	%r9, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	-240(%rbp), %xmm0
	movhps	-224(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r14, %rdi
	movq	%rax, -240(%rbp)
	call	_ZNK2v88internal8compiler11JSRegExpRef10last_indexEv@PLT
	movq	%r12, %rdi
	movq	%rdx, -168(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder20ForJSRegExpLastIndexEv@PLT
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	(%rbx), %r14
	movq	%rax, -232(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$4, %edx
	movq	-216(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r14, %rdi
	movhps	-232(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-240(%rbp), %xmm0
	movhps	-224(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%rbx), %rdi
	movq	(%rbx), %r12
	movq	%rax, -224(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12FinishRegionEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$2, %edx
	movq	-216(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r12, %rdi
	movhps	-224(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L966
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L966:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23034:
	.size	_ZN2v88internal8compiler16JSCreateLowering21AllocateLiteralRegExpEPNS1_4NodeES4_NS1_11JSRegExpRefE, .-_ZN2v88internal8compiler16JSCreateLowering21AllocateLiteralRegExpEPNS1_4NodeES4_NS1_11JSRegExpRefE
	.section	.text._ZN2v88internal8compiler16JSCreateLowering27ReduceJSCreateLiteralRegExpEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16JSCreateLowering27ReduceJSCreateLiteralRegExpEPNS1_4NodeE
	.type	_ZN2v88internal8compiler16JSCreateLowering27ReduceJSCreateLiteralRegExpEPNS1_4NodeE, @function
_ZN2v88internal8compiler16JSCreateLowering27ReduceJSCreateLiteralRegExpEPNS1_4NodeE:
.LFB22986:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler25CreateLiteralParametersOfEPKNS1_8OperatorE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	8(%rbx), %rdx
	movq	32(%r13), %rsi
	leaq	-96(%rbp), %rdi
	movl	$1, %ecx
	movq	%rdi, -104(%rbp)
	movq	%rax, %r14
	call	_ZN2v88internal8compiler17FeedbackVectorRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movl	16(%rbx), %esi
	movq	-104(%rbp), %rdi
	leaq	-80(%rbp), %rbx
	call	_ZNK2v88internal8compiler17FeedbackVectorRef3getENS0_12FeedbackSlotE@PLT
	movq	%rbx, %rdi
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef10IsJSRegExpEv@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	testb	%r8b, %r8b
	jne	.L974
.L969:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L975
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L974:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef10AsJSRegExpEv@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rdx, %r8
	movq	%rax, %rcx
	movq	%r14, %rdx
	call	_ZN2v88internal8compiler16JSCreateLowering21AllocateLiteralRegExpEPNS1_4NodeES4_NS1_11JSRegExpRefE
	movq	8(%r13), %rdi
	movq	%r14, %r8
	movq	%r12, %rsi
	movq	%rax, %rbx
	movq	(%rdi), %rax
	movq	%rbx, %rcx
	movq	%rbx, %rdx
	call	*32(%rax)
	movq	%rbx, %rax
	jmp	.L969
.L975:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22986:
	.size	_ZN2v88internal8compiler16JSCreateLowering27ReduceJSCreateLiteralRegExpEPNS1_4NodeE, .-_ZN2v88internal8compiler16JSCreateLowering27ReduceJSCreateLiteralRegExpEPNS1_4NodeE
	.section	.text._ZNK2v88internal8compiler16JSCreateLowering7factoryEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler16JSCreateLowering7factoryEv
	.type	_ZNK2v88internal8compiler16JSCreateLowering7factoryEv, @function
_ZNK2v88internal8compiler16JSCreateLowering7factoryEv:
.LFB23035:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	movq	360(%rax), %rax
	ret
	.cfi_endproc
.LFE23035:
	.size	_ZNK2v88internal8compiler16JSCreateLowering7factoryEv, .-_ZNK2v88internal8compiler16JSCreateLowering7factoryEv
	.section	.text._ZNK2v88internal8compiler16JSCreateLowering5graphEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler16JSCreateLowering5graphEv
	.type	_ZNK2v88internal8compiler16JSCreateLowering5graphEv, @function
_ZNK2v88internal8compiler16JSCreateLowering5graphEv:
.LFB23036:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	movq	(%rax), %rax
	ret
	.cfi_endproc
.LFE23036:
	.size	_ZNK2v88internal8compiler16JSCreateLowering5graphEv, .-_ZNK2v88internal8compiler16JSCreateLowering5graphEv
	.section	.text._ZNK2v88internal8compiler16JSCreateLowering6commonEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler16JSCreateLowering6commonEv
	.type	_ZNK2v88internal8compiler16JSCreateLowering6commonEv, @function
_ZNK2v88internal8compiler16JSCreateLowering6commonEv:
.LFB23037:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	movq	8(%rax), %rax
	ret
	.cfi_endproc
.LFE23037:
	.size	_ZNK2v88internal8compiler16JSCreateLowering6commonEv, .-_ZNK2v88internal8compiler16JSCreateLowering6commonEv
	.section	.text._ZNK2v88internal8compiler16JSCreateLowering10simplifiedEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler16JSCreateLowering10simplifiedEv
	.type	_ZNK2v88internal8compiler16JSCreateLowering10simplifiedEv, @function
_ZNK2v88internal8compiler16JSCreateLowering10simplifiedEv:
.LFB23038:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	movq	376(%rax), %rax
	ret
	.cfi_endproc
.LFE23038:
	.size	_ZNK2v88internal8compiler16JSCreateLowering10simplifiedEv, .-_ZNK2v88internal8compiler16JSCreateLowering10simplifiedEv
	.section	.text._ZNK2v88internal8compiler16JSCreateLowering14native_contextEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler16JSCreateLowering14native_contextEv
	.type	_ZNK2v88internal8compiler16JSCreateLowering14native_contextEv, @function
_ZNK2v88internal8compiler16JSCreateLowering14native_contextEv:
.LFB23039:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	cmpb	$0, 24(%rax)
	je	.L985
	movq	40(%rax), %rdx
	movq	32(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L985:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC15(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE23039:
	.size	_ZNK2v88internal8compiler16JSCreateLowering14native_contextEv, .-_ZNK2v88internal8compiler16JSCreateLowering14native_contextEv
	.section	.rodata._ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_.str1.1,"aMS",@progbits,1
.LC27:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.type	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, @function
_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_:
.LFB26248:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L1000
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L996
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L1001
.L988:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L995:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L1002
	testq	%r13, %r13
	jg	.L991
	testq	%r9, %r9
	jne	.L994
.L992:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1002:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L991
.L994:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L992
	.p2align 4,,10
	.p2align 3
.L1001:
	testq	%rsi, %rsi
	jne	.L989
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L995
	.p2align 4,,10
	.p2align 3
.L991:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L992
	jmp	.L994
	.p2align 4,,10
	.p2align 3
.L996:
	movl	$8, %r14d
	jmp	.L988
.L1000:
	leaq	.LC27(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L989:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L988
	.cfi_endproc
.LFE26248:
	.size	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, .-_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.section	.rodata._ZN2v88internal8compiler16JSCreateLowering19ReduceJSCreateArrayEPNS1_4NodeE.str1.1,"aMS",@progbits,1
.LC29:
	.string	"vector::reserve"
	.section	.text._ZN2v88internal8compiler16JSCreateLowering19ReduceJSCreateArrayEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16JSCreateLowering19ReduceJSCreateArrayEPNS1_4NodeE
	.type	_ZN2v88internal8compiler16JSCreateLowering19ReduceJSCreateArrayEPNS1_4NodeE, @function
_ZN2v88internal8compiler16JSCreateLowering19ReduceJSCreateArrayEPNS1_4NodeE:
.LFB22944:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$200, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler23CreateArrayParametersOfEPKNS1_8OperatorE@PLT
	pxor	%xmm0, %xmm0
	movq	(%rax), %r15
	movaps	%xmm0, -176(%rbp)
	movq	%rax, %rbx
	movq	$0, -160(%rbp)
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1004
	movq	32(%r12), %rsi
	leaq	-80(%rbp), %rdi
	movl	$1, %ecx
	call	_ZN2v88internal8compiler17AllocationSiteRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	cmpb	$0, -176(%rbp)
	je	.L1005
	movdqa	-80(%rbp), %xmm2
	movups	%xmm2, -168(%rbp)
.L1004:
	movq	32(%r12), %rsi
	leaq	-144(%rbp), %rdi
	movq	%r13, %rdx
	call	_ZN2v88internal8compiler14NodeProperties14GetJSCreateMapEPNS1_12JSHeapBrokerEPNS1_4NodeE@PLT
	cmpb	$0, -144(%rbp)
	jne	.L1007
.L1030:
	xorl	%eax, %eax
.L1008:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1112
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1005:
	.cfi_restore_state
	movdqa	-80(%rbp), %xmm3
	movb	$1, -176(%rbp)
	movups	%xmm3, -168(%rbp)
	jmp	.L1004
	.p2align 4,,10
	.p2align 3
.L1007:
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	xorl	%edx, %edx
	movq	(%rax), %rax
	cmpw	$30, 16(%rax)
	je	.L1113
.L1009:
	movq	32(%r12), %rsi
	leaq	-80(%rbp), %r14
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	movb	%al, -216(%rbp)
	testb	%al, %al
	je	.L1114
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12AsJSFunctionEv@PLT
	movq	16(%r12), %rdi
	leaq	-192(%rbp), %rsi
	movq	%rdx, -184(%rbp)
	movq	%rax, -192(%rbp)
	call	_ZN2v88internal8compiler23CompilationDependencies40DependOnInitialMapInstanceSizePredictionERKNS1_13JSFunctionRefE@PLT
	leaq	-136(%rbp), %rdi
	movq	%rax, -200(%rbp)
	call	_ZNK2v88internal8compiler6MapRef13elements_kindEv@PLT
	cmpb	$0, -176(%rbp)
	movb	%al, -225(%rbp)
	jne	.L1115
	movq	24(%r12), %rax
	movq	32(%r12), %rsi
	leaq	-112(%rbp), %rdi
	movl	$1, %ecx
	movq	%rdi, -224(%rbp)
	movq	360(%rax), %rdx
	addq	$4496, %rdx
	call	_ZN2v88internal8compiler7CellRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	-224(%rbp), %rdi
	call	_ZNK2v88internal8compiler7CellRef5valueEv@PLT
	movq	%r14, %rdi
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef5AsSmiEv@PLT
	movb	$0, -226(%rbp)
	cmpl	$1, %eax
	sete	-227(%rbp)
.L1012:
	movl	%r15d, -224(%rbp)
	testl	%r15d, %r15d
	je	.L1116
	cmpl	$1, %r15d
	je	.L1117
	cmpl	$16376, %r15d
	jg	.L1030
	pxor	%xmm0, %xmm0
	movq	$0, -96(%rbp)
	movabsq	$1152921504606846975, %rax
	movaps	%xmm0, -112(%rbp)
	movq	(%rbx), %rbx
	cmpq	%rax, %rbx
	ja	.L1118
	testq	%rbx, %rbx
	jne	.L1032
.L1111:
	testl	%r15d, %r15d
	jle	.L1119
	movzbl	-216(%rbp), %eax
	xorl	%ebx, %ebx
	xorl	%r15d, %r15d
	movb	%al, -240(%rbp)
	movb	%al, -228(%rbp)
	.p2align 4,,10
	.p2align 3
.L1034:
	leal	2(%rbx), %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rax, -208(%rbp)
	movq	8(%rax), %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal8compiler10BitsetType11SignedSmallEv@PLT
	movl	%eax, %esi
	orl	$1, %esi
	cmpq	%rsi, -80(%rbp)
	je	.L1039
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	movq	-80(%rbp), %rsi
	testb	%al, %al
	jne	.L1039
	movb	$0, -228(%rbp)
.L1039:
	cmpq	$7263, %rsi
	je	.L1041
	movl	$7263, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	movl	$0, %ecx
	testb	%al, %al
	movzbl	-240(%rbp), %eax
	cmove	%ecx, %eax
	movb	%al, -240(%rbp)
.L1041:
	movl	$7263, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	movq	-104(%rbp), %rsi
	testb	%al, %al
	movzbl	-216(%rbp), %eax
	cmove	%eax, %r15d
	cmpq	-96(%rbp), %rsi
	je	.L1043
	movq	-208(%rbp), %rax
	addl	$1, %ebx
	movq	%rax, (%rsi)
	addq	$8, -104(%rbp)
	cmpl	%ebx, -224(%rbp)
	jg	.L1034
.L1044:
	cmpb	$0, -228(%rbp)
	jne	.L1046
	cmpb	$0, -240(%rbp)
	je	.L1047
	movzbl	-225(%rbp), %r15d
	movl	%r15d, %eax
	cmpb	$5, %r15b
	ja	.L1070
	movl	$5, %ebx
	testb	$1, %al
	je	.L1070
.L1052:
	movl	%r15d, %edi
	movl	%ebx, %esi
	call	_ZN2v88internal35IsMoreGeneralElementsKindTransitionENS0_12ElementsKindES1_@PLT
	testb	%al, %al
	cmovne	%ebx, %r15d
.L1050:
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	pxor	%xmm0, %xmm0
	movq	$0, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	%rdx, %rax
	subq	%rsi, %rax
	movq	%rax, %rcx
	sarq	$3, %rcx
	je	.L1120
	movabsq	$1152921504606846975, %rdx
	cmpq	%rdx, %rcx
	ja	.L1121
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_Znwm@PLT
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%rax, %rcx
	movq	%rdx, %r8
	subq	%rsi, %r8
.L1057:
	movq	%rcx, %xmm0
	leaq	(%rcx,%rbx), %rax
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	cmpq	%rsi, %rdx
	je	.L1059
	movq	%r8, %rdx
	movq	%rcx, %rdi
	movq	%r8, -216(%rbp)
	call	memmove@PLT
	movq	-216(%rbp), %r8
	movq	%rax, %rcx
.L1059:
	leaq	-200(%rbp), %rax
	addq	%r8, %rcx
	movq	%r14, %rdx
	movq	%r12, %rdi
	pushq	%rax
	movzbl	-226(%rbp), %eax
	movl	%r15d, %r9d
	movq	%r13, %rsi
	movq	%rcx, -72(%rbp)
	movq	-128(%rbp), %r8
	pushq	%rax
	movq	-136(%rbp), %rcx
	call	_ZN2v88internal8compiler16JSCreateLowering14ReduceNewArrayEPNS1_4NodeESt6vectorIS4_SaIS4_EENS1_6MapRefENS0_12ElementsKindENS0_14AllocationTypeERKNS1_23SlackTrackingPredictionE
	movq	-80(%rbp), %rdi
	popq	%rdx
	popq	%rcx
	testq	%rdi, %rdi
	je	.L1055
	movq	%rax, -216(%rbp)
	call	_ZdlPv@PLT
	movq	-216(%rbp), %rax
.L1055:
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1008
.L1110:
	movq	%rax, -216(%rbp)
	call	_ZdlPv@PLT
	movq	-216(%rbp), %rax
	jmp	.L1008
	.p2align 4,,10
	.p2align 3
.L1113:
	movq	48(%rax), %rdx
	jmp	.L1009
	.p2align 4,,10
	.p2align 3
.L1115:
	leaq	-168(%rbp), %rsi
	movq	%rsi, %rdi
	movq	%rsi, -224(%rbp)
	call	_ZNK2v88internal8compiler17AllocationSiteRef15GetElementsKindEv@PLT
	movq	-224(%rbp), %rsi
	movb	%al, -225(%rbp)
	movq	%rsi, %rdi
	call	_ZNK2v88internal8compiler17AllocationSiteRef13CanInlineCallEv@PLT
	movq	16(%r12), %rdi
	movq	-224(%rbp), %rsi
	movb	%al, -227(%rbp)
	call	_ZN2v88internal8compiler23CompilationDependencies21DependOnPretenureModeERKNS1_17AllocationSiteRefE@PLT
	movq	16(%r12), %rdi
	movq	-224(%rbp), %rsi
	movb	%al, -226(%rbp)
	call	_ZN2v88internal8compiler23CompilationDependencies20DependOnElementsKindERKNS1_17AllocationSiteRefE@PLT
	jmp	.L1012
	.p2align 4,,10
	.p2align 3
.L1116:
	movq	24(%r12), %rdi
	call	_ZN2v88internal8compiler7JSGraph12ZeroConstantEv@PLT
	subq	$8, %rsp
	movq	-128(%rbp), %r9
	movq	%r13, %rsi
	movq	%rax, %rdx
	leaq	-200(%rbp), %rax
	movq	-136(%rbp), %r8
	movq	%r12, %rdi
	pushq	%rax
	movzbl	-226(%rbp), %eax
	movl	$4, %ecx
	pushq	%rax
	movzbl	-225(%rbp), %eax
	pushq	%rax
	call	_ZN2v88internal8compiler16JSCreateLowering14ReduceNewArrayEPNS1_4NodeES4_iNS1_6MapRefENS0_12ElementsKindENS0_14AllocationTypeERKNS1_23SlackTrackingPredictionE
	addq	$32, %rsp
	jmp	.L1008
	.p2align 4,,10
	.p2align 3
.L1032:
	salq	$3, %rbx
	movq	%rbx, %rdi
	call	_Znwm@PLT
	movq	-112(%rbp), %r8
	movq	-104(%rbp), %rdx
	movq	%rax, %rcx
	subq	%r8, %rdx
	testq	%rdx, %rdx
	jg	.L1122
	testq	%r8, %r8
	jne	.L1036
.L1037:
	movq	%rcx, %xmm0
	addq	%rcx, %rbx
	punpcklqdq	%xmm0, %xmm0
	movq	%rbx, -96(%rbp)
	movaps	%xmm0, -112(%rbp)
	jmp	.L1111
.L1119:
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	xorl	%ecx, %ecx
	movzbl	-225(%rbp), %r15d
	movq	%rdx, %rbx
	subq	%rsi, %rbx
	movq	%rbx, %r8
	jmp	.L1057
	.p2align 4,,10
	.p2align 3
.L1043:
	leaq	-208(%rbp), %rdx
	leaq	-112(%rbp), %rdi
	addl	$1, %ebx
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	cmpl	%ebx, -224(%rbp)
	jg	.L1034
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L1117:
	movl	$2, %esi
	movq	%r13, %rdi
	leaq	-112(%rbp), %rbx
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movl	$7263, %esi
	movq	%rbx, %rdi
	movq	%rax, %r15
	movq	8(%rax), %rax
	movq	%rax, -112(%rbp)
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1015
	movzbl	-225(%rbp), %ebx
	movl	%ebx, %eax
	cmpb	$5, %bl
	ja	.L1067
	movl	$3, %edx
	testb	$1, %al
	je	.L1067
.L1016:
	movl	%edx, %esi
	movl	%ebx, %edi
	movl	%edx, -216(%rbp)
	call	_ZN2v88internal35IsMoreGeneralElementsKindTransitionENS0_12ElementsKindES1_@PLT
	movl	-216(%rbp), %edx
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	testb	%al, %al
	movaps	%xmm0, -80(%rbp)
	cmovne	%edx, %ebx
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	-128(%rbp), %r8
	movq	%r12, %rdi
	movq	%r13, %rsi
	movq	%r15, (%rax)
	leaq	8(%rax), %rdx
	movl	%ebx, %r9d
	movq	-136(%rbp), %rcx
	movq	%rax, -80(%rbp)
	leaq	-200(%rbp), %rax
	pushq	%rax
	movzbl	-226(%rbp), %eax
	movq	%rdx, -64(%rbp)
	pushq	%rax
	movq	%rdx, -72(%rbp)
	movq	%r14, %rdx
	call	_ZN2v88internal8compiler16JSCreateLowering14ReduceNewArrayEPNS1_4NodeESt6vectorIS4_SaIS4_EENS1_6MapRefENS0_12ElementsKindENS0_14AllocationTypeERKNS1_23SlackTrackingPredictionE
	movq	-80(%rbp), %rdi
	popq	%r9
	popq	%r10
	testq	%rdi, %rdi
	jne	.L1110
	jmp	.L1008
	.p2align 4,,10
	.p2align 3
.L1114:
	leaq	.LC9(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1047:
	testb	%r15b, %r15b
	je	.L1051
	movzbl	-225(%rbp), %r15d
	movl	%r15d, %eax
	cmpb	$5, %r15b
	ja	.L1071
	movl	$3, %ebx
	testb	$1, %al
	jne	.L1052
.L1071:
	movl	$2, %ebx
	jmp	.L1052
	.p2align 4,,10
	.p2align 3
.L1046:
	movzbl	-225(%rbp), %r15d
	jmp	.L1050
	.p2align 4,,10
	.p2align 3
.L1015:
	call	_ZN2v88internal8compiler10BitsetType11SignedSmallEv@PLT
	movl	%eax, %esi
	orl	$1, %esi
	cmpq	%rsi, -112(%rbp)
	je	.L1024
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L1023
.L1024:
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	comisd	.LC10(%rip), %xmm0
	jnb	.L1123
.L1023:
	call	_ZN2v88internal8compiler10BitsetType13UnsignedSmallEv@PLT
	movq	%rbx, %rdi
	movl	%eax, %esi
	orl	$1, %esi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	je	.L1030
	cmpb	$0, -227(%rbp)
	je	.L1030
	leaq	-200(%rbp), %rax
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r15, %rdx
	movzbl	-225(%rbp), %r9d
	movq	-136(%rbp), %rcx
	pushq	%rax
	movzbl	-226(%rbp), %eax
	movq	-128(%rbp), %r8
	pushq	%rax
	call	_ZN2v88internal8compiler16JSCreateLowering14ReduceNewArrayEPNS1_4NodeES4_NS1_6MapRefENS0_12ElementsKindENS0_14AllocationTypeERKNS1_23SlackTrackingPredictionE
	popq	%rsi
	popq	%rdi
	jmp	.L1008
.L1122:
	movq	%r8, %rsi
	movq	%rax, %rdi
	movq	%r8, -240(%rbp)
	call	memmove@PLT
	movq	-240(%rbp), %r8
	movq	%rax, %rcx
.L1036:
	movq	%r8, %rdi
	movq	%rcx, -240(%rbp)
	call	_ZdlPv@PLT
	movq	-240(%rbp), %rcx
	jmp	.L1037
.L1051:
	cmpb	$0, -227(%rbp)
	jne	.L1046
	xorl	%eax, %eax
	jmp	.L1055
.L1120:
	movq	%rax, %rbx
	movq	%rax, %r8
	xorl	%ecx, %ecx
	jmp	.L1057
.L1067:
	movl	$2, %edx
	jmp	.L1016
.L1070:
	movl	$4, %ebx
	jmp	.L1052
.L1123:
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler4Type3MaxEv@PLT
	movsd	.LC28(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L1023
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	movq	%rbx, %rdi
	movsd	%xmm0, -216(%rbp)
	call	_ZNK2v88internal8compiler4Type3MaxEv@PLT
	movsd	-216(%rbp), %xmm1
	ucomisd	%xmm0, %xmm1
	jp	.L1023
	jne	.L1023
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler4Type3MaxEv@PLT
	movq	-128(%rbp), %r9
	movq	%r15, %rdx
	movq	%r13, %rsi
	cvttsd2sil	%xmm0, %ecx
	leaq	-200(%rbp), %rax
	pushq	%r8
	movq	%r12, %rdi
	pushq	%rax
	movzbl	-226(%rbp), %eax
	movq	-136(%rbp), %r8
	pushq	%rax
	movzbl	-225(%rbp), %eax
	pushq	%rax
	call	_ZN2v88internal8compiler16JSCreateLowering14ReduceNewArrayEPNS1_4NodeES4_iNS1_6MapRefENS0_12ElementsKindENS0_14AllocationTypeERKNS1_23SlackTrackingPredictionE
	addq	$32, %rsp
	jmp	.L1008
.L1112:
	call	__stack_chk_fail@PLT
.L1121:
	call	_ZSt17__throw_bad_allocv@PLT
.L1118:
	leaq	.LC29(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE22944:
	.size	_ZN2v88internal8compiler16JSCreateLowering19ReduceJSCreateArrayEPNS1_4NodeE, .-_ZN2v88internal8compiler16JSCreateLowering19ReduceJSCreateArrayEPNS1_4NodeE
	.section	.text._ZNSt6vectorISt4pairIN2v88internal8compiler11FieldAccessEPNS3_4NodeEENS2_13ZoneAllocatorIS7_EEE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_SA_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt4pairIN2v88internal8compiler11FieldAccessEPNS3_4NodeEENS2_13ZoneAllocatorIS7_EEE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_SA_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt4pairIN2v88internal8compiler11FieldAccessEPNS3_4NodeEENS2_13ZoneAllocatorIS7_EEE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_SA_EEDpOT_
	.type	_ZNSt6vectorISt4pairIN2v88internal8compiler11FieldAccessEPNS3_4NodeEENS2_13ZoneAllocatorIS7_EEE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_SA_EEDpOT_, @function
_ZNSt6vectorISt4pairIN2v88internal8compiler11FieldAccessEPNS3_4NodeEENS2_13ZoneAllocatorIS7_EEE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_SA_EEDpOT_:
.LFB26886:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r12
	movq	8(%rdi), %r15
	movq	%r12, %rax
	subq	%r15, %rax
	sarq	$6, %rax
	cmpq	$33554431, %rax
	je	.L1140
	movq	%rdx, %r14
	movq	%rsi, %rdx
	movq	%rdi, %r13
	movq	%rsi, %rbx
	subq	%r15, %rdx
	testq	%rax, %rax
	je	.L1134
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L1141
	movl	$2147483584, %esi
	movl	$2147483584, %ecx
.L1126:
	movq	0(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %r8
	subq	%rax, %r8
	cmpq	%rsi, %r8
	jb	.L1142
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1129:
	leaq	(%rax,%rcx), %rdi
	leaq	64(%rax), %rsi
	jmp	.L1127
	.p2align 4,,10
	.p2align 3
.L1141:
	testq	%rcx, %rcx
	jne	.L1143
	movl	$64, %esi
	xorl	%edi, %edi
	xorl	%eax, %eax
.L1127:
	movdqu	(%r14), %xmm1
	movups	%xmm1, (%rax,%rdx)
	movdqu	16(%r14), %xmm2
	movups	%xmm2, 16(%rax,%rdx)
	movdqu	32(%r14), %xmm3
	movups	%xmm3, 32(%rax,%rdx)
	movdqu	48(%r14), %xmm4
	movups	%xmm4, 48(%rax,%rdx)
	cmpq	%r15, %rbx
	je	.L1130
	movq	%r15, %rdx
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L1131:
	movdqu	(%rdx), %xmm1
	addq	$64, %rdx
	addq	$64, %rcx
	movups	%xmm1, -64(%rcx)
	movdqu	-48(%rdx), %xmm2
	movups	%xmm2, -48(%rcx)
	movdqu	-32(%rdx), %xmm3
	movups	%xmm3, -32(%rcx)
	movdqu	-16(%rdx), %xmm4
	movups	%xmm4, -16(%rcx)
	cmpq	%rdx, %rbx
	jne	.L1131
	movq	%rbx, %rdx
	subq	%r15, %rdx
	leaq	64(%rax,%rdx), %rsi
.L1130:
	cmpq	%r12, %rbx
	je	.L1132
	movq	%rbx, %rdx
	movq	%rsi, %rcx
	.p2align 4,,10
	.p2align 3
.L1133:
	movdqu	(%rdx), %xmm5
	addq	$64, %rdx
	addq	$64, %rcx
	movups	%xmm5, -64(%rcx)
	movdqu	-48(%rdx), %xmm6
	movups	%xmm6, -48(%rcx)
	movdqu	-32(%rdx), %xmm7
	movups	%xmm7, -32(%rcx)
	movdqu	-16(%rdx), %xmm0
	movups	%xmm0, -16(%rcx)
	cmpq	%rdx, %r12
	jne	.L1133
	subq	%rbx, %r12
	addq	%r12, %rsi
.L1132:
	movq	%rax, %xmm0
	movq	%rsi, %xmm5
	movq	%rdi, 24(%r13)
	punpcklqdq	%xmm5, %xmm0
	movups	%xmm0, 8(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1134:
	.cfi_restore_state
	movl	$64, %esi
	movl	$64, %ecx
	jmp	.L1126
.L1142:
	movq	%rdx, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rdx
	jmp	.L1129
.L1143:
	cmpq	$33554431, %rcx
	movl	$33554431, %eax
	cmova	%rax, %rcx
	salq	$6, %rcx
	movq	%rcx, %rsi
	jmp	.L1126
.L1140:
	leaq	.LC27(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE26886:
	.size	_ZNSt6vectorISt4pairIN2v88internal8compiler11FieldAccessEPNS3_4NodeEENS2_13ZoneAllocatorIS7_EEE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_SA_EEDpOT_, .-_ZNSt6vectorISt4pairIN2v88internal8compiler11FieldAccessEPNS3_4NodeEENS2_13ZoneAllocatorIS7_EEE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_SA_EEDpOT_
	.section	.text._ZN2v88internal8compiler16JSCreateLowering19AllocateFastLiteralEPNS1_4NodeES4_NS1_11JSObjectRefENS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16JSCreateLowering19AllocateFastLiteralEPNS1_4NodeES4_NS1_11JSObjectRefENS0_14AllocationTypeE
	.type	_ZN2v88internal8compiler16JSCreateLowering19AllocateFastLiteralEPNS1_4NodeES4_NS1_11JSObjectRefENS0_14AllocationTypeE, @function
_ZN2v88internal8compiler16JSCreateLowering19AllocateFastLiteralEPNS1_4NodeES4_NS1_11JSObjectRefENS0_14AllocationTypeE:
.LFB23011:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-304(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	subq	$408, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -328(%rbp)
	movq	%rcx, -320(%rbp)
	movq	%r8, -312(%rbp)
	movl	%r9d, -400(%rbp)
	movb	%r9b, -417(%rbp)
	movq	%rdx, -344(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-320(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -368(%rbp)
	call	_ZNK2v88internal8compiler13HeapObjectRef3mapEv@PLT
	movq	%r13, %rdi
	movq	$0, -248(%rbp)
	movq	%rax, -304(%rbp)
	movq	40(%r15), %rax
	movq	%rdx, -296(%rbp)
	movq	%rax, -256(%rbp)
	movq	$0, -240(%rbp)
	movq	$0, -232(%rbp)
	call	_ZNK2v88internal8compiler6MapRef21GetInObjectPropertiesEv@PLT
	cltq
	cmpq	$33554431, %rax
	ja	.L1199
	movq	-248(%rbp), %rbx
	movq	-232(%rbp), %rdx
	subq	%rbx, %rdx
	sarq	$6, %rdx
	cmpq	%rdx, %rax
	ja	.L1200
.L1146:
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler6MapRef22NumberOfOwnDescriptorsEv@PLT
	movl	%eax, -352(%rbp)
	testl	%eax, %eax
	jle	.L1201
	leaq	-160(%rbp), %rax
	movq	%r15, -384(%rbp)
	xorl	%r14d, %r14d
	movq	%rax, -336(%rbp)
	leaq	-96(%rbp), %rax
	movq	%rax, -360(%rbp)
	leaq	-288(%rbp), %rax
	movq	%rax, -408(%rbp)
.L1168:
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler6MapRef18GetPropertyDetailsEi@PLT
	movl	%eax, %ebx
	testb	$2, %al
	jne	.L1155
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler6MapRef14GetPropertyKeyEi@PLT
	movl	%r14d, %esi
	movq	%r13, %rdi
	movq	%rdx, -280(%rbp)
	movq	%rax, -288(%rbp)
	call	_ZNK2v88internal8compiler6MapRef16GetFieldIndexForEi@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZNK2v88internal8compiler6MapRef6objectEv@PLT
	movq	-408(%rbp), %rdi
	movq	%rax, %r15
	movl	%r12d, %eax
	andl	$16383, %eax
	movl	%eax, -396(%rbp)
	call	_ZNK2v88internal8compiler7NameRef6objectEv@PLT
	movl	%r14d, %esi
	movq	%r13, %rdi
	movq	%rax, -392(%rbp)
	call	_ZNK2v88internal8compiler6MapRef20IsUnboxedDoubleFieldEi@PLT
	testb	%al, %al
	je	.L1156
	movq	-368(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZNK2v88internal8compiler11JSObjectRef29RawFastDoublePropertyAsBitsAtENS0_10FieldIndexE@PLT
	movl	$0, %edx
	movabsq	$-2251799814209537, %rcx
	cmpq	%rcx, %rax
	movq	-384(%rbp), %rcx
	movq	%rax, %xmm0
	cmove	%rdx, %r15
	movq	24(%rcx), %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movl	$6, %edx
	movl	$13, %ecx
	movl	$7263, %esi
.L1158:
	movl	-396(%rbp), %ebx
	movb	%cl, -128(%rbp)
	movq	%rsi, -136(%rbp)
	movq	-240(%rbp), %rsi
	movl	%ebx, -156(%rbp)
	movq	-392(%rbp), %rbx
	movb	$1, -160(%rbp)
	movq	%rbx, -152(%rbp)
	movq	$0, -144(%rbp)
	movb	%dl, -127(%rbp)
	movb	$5, -126(%rbp)
	movl	$1, -124(%rbp)
	movq	%r15, -120(%rbp)
	movb	$0, -112(%rbp)
	movq	%rax, -104(%rbp)
	cmpq	-232(%rbp), %rsi
	je	.L1166
	movdqa	-160(%rbp), %xmm1
	movups	%xmm1, (%rsi)
	movdqa	-144(%rbp), %xmm4
	movups	%xmm4, 16(%rsi)
	movdqa	-128(%rbp), %xmm5
	addq	$64, -240(%rbp)
	movups	%xmm5, 32(%rsi)
	movdqa	-112(%rbp), %xmm6
	movups	%xmm6, 48(%rsi)
.L1155:
	addl	$1, %r14d
	cmpl	%r14d, -352(%rbp)
	jne	.L1168
	movq	-384(%rbp), %r15
.L1153:
	movq	%r13, %rdi
	leaq	-224(%rbp), %r12
	call	_ZNK2v88internal8compiler6MapRef21GetInObjectPropertiesEv@PLT
	leaq	-256(%rbp), %rcx
	movl	%eax, %ebx
	movq	-240(%rbp), %rax
	subq	-248(%rbp), %rax
	movq	%rcx, -352(%rbp)
	sarq	$6, %rax
	movl	%eax, %r14d
	cmpl	%eax, %ebx
	jle	.L1173
	.p2align 4,,10
	.p2align 3
.L1169:
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder27ForJSObjectInObjectPropertyERKNS1_6MapRefEi@PLT
	movq	24(%r15), %rdi
	movq	360(%rdi), %rax
	leaq	64(%rax), %rsi
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movdqa	-224(%rbp), %xmm0
	movdqa	-208(%rbp), %xmm2
	movdqa	-192(%rbp), %xmm3
	movq	-176(%rbp), %rdx
	movq	%rax, -104(%rbp)
	movq	-240(%rbp), %rsi
	movaps	%xmm0, -160(%rbp)
	movq	%rdx, -112(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm3, -128(%rbp)
	cmpq	-232(%rbp), %rsi
	je	.L1172
	movups	%xmm0, (%rsi)
	movdqa	-144(%rbp), %xmm4
	addl	$1, %r14d
	movups	%xmm4, 16(%rsi)
	movdqa	-128(%rbp), %xmm5
	addq	$64, -240(%rbp)
	movups	%xmm5, 32(%rsi)
	movdqa	-112(%rbp), %xmm6
	movups	%xmm6, 48(%rsi)
	cmpl	%r14d, %ebx
	jne	.L1169
.L1173:
	movq	-328(%rbp), %rbx
	movq	-320(%rbp), %rcx
	movq	%r15, %rdi
	movq	-312(%rbp), %r8
	movzbl	-400(%rbp), %r9d
	movq	-344(%rbp), %rdx
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler16JSCreateLowering27AllocateFastLiteralElementsEPNS1_4NodeES4_NS1_11JSObjectRefENS0_14AllocationTypeE
	movq	%r13, %rdi
	movq	%rax, %r12
	movq	(%rax), %rax
	cmpb	$0, 36(%rax)
	cmovne	%r12, %rbx
	movq	%rbx, -328(%rbp)
	movq	24(%r15), %rbx
	call	_ZN2v88internal8compiler10BitsetType3LubINS1_6MapRefEEEjRKT_@PLT
	movl	%eax, %edi
	call	_ZN2v88internal8compiler10BitsetType15ExpandInternalsEj@PLT
	movq	%r13, %rdi
	movl	%eax, -352(%rbp)
	call	_ZNK2v88internal8compiler6MapRef13instance_sizeEv@PLT
	movl	-352(%rbp), %edx
	movq	(%rbx), %r9
	movl	$1, %esi
	movq	8(%rbx), %rdi
	movl	%eax, %r14d
	movl	%edx, %r10d
	movq	%r9, -352(%rbp)
	orl	$1, %r10d
	movq	%r10, -392(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder11BeginRegionENS1_19RegionObservabilityE@PLT
	movq	-352(%rbp), %r9
	xorl	%r8d, %r8d
	movq	-360(%rbp), %rcx
	movq	%rax, %rsi
	movq	-328(%rbp), %rax
	movl	$1, %edx
	movq	%r9, %rdi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	pxor	%xmm0, %xmm0
	movq	(%rbx), %r9
	movq	%rbx, %rdi
	cvtsi2sdl	%r14d, %xmm0
	movq	%rax, -328(%rbp)
	movq	%r9, -384(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	-392(%rbp), %r10
	movq	376(%rbx), %rdi
	movzbl	-400(%rbp), %edx
	movq	%rax, -352(%rbp)
	movq	%r10, %rsi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder8AllocateENS1_4TypeENS0_14AllocationTypeE@PLT
	movq	-384(%rbp), %r9
	xorl	%r8d, %r8d
	movq	-352(%rbp), %xmm0
	movq	-360(%rbp), %rcx
	movq	%rax, %rsi
	movl	$3, %edx
	movq	-344(%rbp), %rax
	movq	%r9, %rdi
	movhps	-328(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-336(%rbp), %r14
	movq	%rax, -352(%rbp)
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	376(%rbx), %rdi
	movq	%r14, %rsi
	movq	(%rbx), %r13
	movq	%rax, -328(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movl	$4, %edx
	movq	%r13, %rdi
	movq	-352(%rbp), %xmm7
	movq	-360(%rbp), %rcx
	movq	%rax, %rsi
	movdqa	%xmm7, %xmm0
	movhps	-328(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movdqa	%xmm7, %xmm0
	movhps	-344(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	24(%r15), %rdi
	movq	%r14, %r15
	movq	%rax, -384(%rbp)
	call	_ZN2v88internal8compiler7JSGraph23EmptyFixedArrayConstantEv@PLT
	movq	%r14, %rdi
	movq	%rax, -328(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder39ForJSObjectPropertiesOrHashKnownPointerEv@PLT
	movq	376(%rbx), %rdi
	movq	%r14, %rsi
	movq	(%rbx), %r13
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movl	$4, %edx
	movq	%r13, %rdi
	movq	-352(%rbp), %xmm0
	movq	-360(%rbp), %r14
	movq	%rax, %rsi
	movhps	-328(%rbp), %xmm0
	movq	%r14, %rcx
	movaps	%xmm0, -96(%rbp)
	movq	-384(%rbp), %xmm0
	movhps	-344(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r15, %rdi
	movq	%rax, -328(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder19ForJSObjectElementsEv@PLT
	movq	376(%rbx), %rdi
	movq	%r15, %rsi
	movq	(%rbx), %r13
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	movq	%r12, %xmm7
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	movq	-352(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r14, %rcx
	movl	$4, %edx
	punpcklqdq	%xmm7, %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-328(%rbp), %xmm0
	movhps	-344(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-368(%rbp), %rdi
	movq	%rax, -328(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef9IsJSArrayEv@PLT
	testb	%al, %al
	jne	.L1202
.L1175:
	movq	-248(%rbp), %r12
	movq	-240(%rbp), %r14
	cmpq	%r12, %r14
	je	.L1176
	movq	-360(%rbp), %r15
	.p2align 4,,10
	.p2align 3
.L1177:
	movq	56(%r12), %rax
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	addq	$64, %r12
	movq	(%rbx), %r13
	movq	%rax, -336(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$4, %edx
	movq	-352(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r13, %rdi
	movhps	-336(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-328(%rbp), %xmm0
	movhps	-344(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -328(%rbp)
	cmpq	%r12, %r14
	jne	.L1177
.L1176:
	movq	8(%rbx), %rdi
	movq	(%rbx), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12FinishRegionEv@PLT
	xorl	%r8d, %r8d
	movl	$2, %edx
	movq	%r12, %rdi
	movq	-352(%rbp), %xmm0
	movq	-360(%rbp), %rcx
	movq	%rax, %rsi
	movhps	-328(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1203
	addq	$408, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1172:
	.cfi_restore_state
	movq	-336(%rbp), %rdx
	movq	-352(%rbp), %rdi
	addl	$1, %r14d
	call	_ZNSt6vectorISt4pairIN2v88internal8compiler11FieldAccessEPNS3_4NodeEENS2_13ZoneAllocatorIS7_EEE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_SA_EEDpOT_
	cmpl	%r14d, %ebx
	jne	.L1169
	jmp	.L1173
	.p2align 4,,10
	.p2align 3
.L1156:
	movq	-368(%rbp), %rdi
	movq	%r12, %rsi
	movb	%al, -416(%rbp)
	leaq	-272(%rbp), %r12
	call	_ZNK2v88internal8compiler11JSObjectRef17RawFastPropertyAtENS0_10FieldIndexE@PLT
	movq	%r12, %rdi
	movq	%rax, -272(%rbp)
	movq	%rdx, -264(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	movzbl	-416(%rbp), %ecx
	testb	%al, %al
	jne	.L1204
.L1160:
	movq	%r12, %rdi
	movb	%cl, -416(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef10IsJSObjectEv@PLT
	movzbl	-416(%rbp), %ecx
	testb	%al, %al
	jne	.L1205
	shrl	$6, %ebx
	andl	$7, %ebx
	cmpl	$2, %ebx
	je	.L1206
	movq	-384(%rbp), %rax
	movq	24(%rax), %r8
	cmpl	$1, %ebx
	jne	.L1164
	testb	%cl, %cl
	je	.L1165
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler7JSGraph12ZeroConstantEv@PLT
	jmp	.L1162
	.p2align 4,,10
	.p2align 3
.L1202:
	movq	-328(%rbp), %xmm0
	movq	-368(%rbp), %rdi
	leaq	-272(%rbp), %r12
	movhps	-344(%rbp), %xmm0
	movaps	%xmm0, -384(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef9AsJSArrayEv@PLT
	movq	%r12, %rdi
	movq	%rax, -272(%rbp)
	movq	%rdx, -264(%rbp)
	call	_ZNK2v88internal8compiler10JSArrayRef6lengthEv@PLT
	movq	%r12, %rdi
	movq	%rdx, -216(%rbp)
	movq	%rax, -224(%rbp)
	call	_ZNK2v88internal8compiler11JSObjectRef15GetElementsKindEv@PLT
	movq	-336(%rbp), %r15
	movzbl	%al, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder16ForJSArrayLengthENS0_12ElementsKindE@PLT
	leaq	-224(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	376(%rbx), %rdi
	movq	%r15, %rsi
	movq	(%rbx), %r12
	movq	%rax, -328(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movl	$4, %edx
	movq	%r12, %rdi
	movq	-352(%rbp), %xmm1
	movdqa	-384(%rbp), %xmm0
	movq	%rax, %rsi
	movq	-360(%rbp), %rcx
	movhps	-328(%rbp), %xmm1
	movaps	%xmm0, -80(%rbp)
	movaps	%xmm1, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -328(%rbp)
	jmp	.L1175
	.p2align 4,,10
	.p2align 3
.L1200:
	movq	-240(%rbp), %r12
	movq	%rax, %r14
	xorl	%ecx, %ecx
	salq	$6, %r14
	movq	%r12, %r8
	subq	%rbx, %r8
	testq	%rax, %rax
	je	.L1147
	movq	-256(%rbp), %rdi
	movq	%r14, %rsi
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	subq	%rcx, %rax
	cmpq	%rax, %r14
	ja	.L1207
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
.L1147:
	movq	%rbx, %rax
	movq	%rcx, %rdx
	cmpq	%r12, %rbx
	je	.L1152
	.p2align 4,,10
	.p2align 3
.L1151:
	movdqu	(%rax), %xmm1
	addq	$64, %rax
	addq	$64, %rdx
	movups	%xmm1, -64(%rdx)
	movdqu	-48(%rax), %xmm7
	movups	%xmm7, -48(%rdx)
	movdqu	-32(%rax), %xmm1
	movups	%xmm1, -32(%rdx)
	movdqu	-16(%rax), %xmm7
	movups	%xmm7, -16(%rdx)
	cmpq	%rax, %r12
	jne	.L1151
.L1152:
	leaq	(%rcx,%r8), %rax
	addq	%rcx, %r14
	movq	%rcx, -248(%rbp)
	movq	%rax, -240(%rbp)
	movq	%r14, -232(%rbp)
	jmp	.L1146
	.p2align 4,,10
	.p2align 3
.L1166:
	movq	-336(%rbp), %rdx
	leaq	-256(%rbp), %rdi
	call	_ZNSt6vectorISt4pairIN2v88internal8compiler11FieldAccessEPNS3_4NodeEENS2_13ZoneAllocatorIS7_EEE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_SA_EEDpOT_
	jmp	.L1155
	.p2align 4,,10
	.p2align 3
.L1205:
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef10AsJSObjectEv@PLT
	movzbl	-417(%rbp), %r9d
	movq	-328(%rbp), %rsi
	movq	%rdx, %r8
	movq	-384(%rbp), %rdi
	movq	-344(%rbp), %rdx
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler16JSCreateLowering19AllocateFastLiteralEPNS1_4NodeES4_NS1_11JSObjectRefENS0_14AllocationTypeE
	movq	%rax, -328(%rbp)
.L1162:
	movl	$7, %edx
	movl	$8, %ecx
	movl	$4294967295, %esi
	jmp	.L1158
	.p2align 4,,10
	.p2align 3
.L1204:
	movq	%r12, %rdi
	movb	%cl, -432(%rbp)
	movb	%al, -416(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef12AsHeapObjectEv@PLT
	leaq	-224(%rbp), %r9
	movq	%r9, %rdi
	movq	%rax, -224(%rbp)
	movq	%rdx, -216(%rbp)
	call	_ZNK2v88internal8compiler13HeapObjectRef3mapEv@PLT
	movq	-336(%rbp), %rdi
	movq	%rax, -160(%rbp)
	movq	%rdx, -152(%rbp)
	call	_ZNK2v88internal8compiler6MapRef12oddball_typeEv@PLT
	movzbl	-416(%rbp), %esi
	movzbl	-432(%rbp), %ecx
	cmpb	$5, %al
	movl	$0, %eax
	cmove	%esi, %ecx
	cmove	%rax, %r15
	jmp	.L1160
	.p2align 4,,10
	.p2align 3
.L1164:
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	jmp	.L1162
	.p2align 4,,10
	.p2align 3
.L1201:
	leaq	-160(%rbp), %rax
	movq	%rax, -336(%rbp)
	leaq	-96(%rbp), %rax
	movq	%rax, -360(%rbp)
	jmp	.L1153
	.p2align 4,,10
	.p2align 3
.L1165:
	movq	%r12, %rdi
	movq	%r8, -416(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef5AsSmiEv@PLT
	pxor	%xmm0, %xmm0
	movq	-416(%rbp), %r8
	cvtsi2sdl	%eax, %xmm0
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	jmp	.L1162
	.p2align 4,,10
	.p2align 3
.L1206:
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12AsHeapNumberEv@PLT
	movq	-336(%rbp), %rdi
	movq	%rdx, -152(%rbp)
	movq	%rax, -160(%rbp)
	call	_ZNK2v88internal8compiler13HeapNumberRef5valueEv@PLT
	movq	-384(%rbp), %rsi
	movsd	%xmm0, -440(%rbp)
	movq	24(%rsi), %rbx
	movl	$1, %esi
	movq	8(%rbx), %rdi
	movq	(%rbx), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder11BeginRegionENS1_19RegionObservabilityE@PLT
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%r12, %rdi
	movq	-360(%rbp), %rcx
	movq	%rax, %rsi
	movq	-328(%rbp), %rax
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rbx, %rdi
	movq	(%rbx), %r12
	movq	%rax, -328(%rbp)
	movq	.LC28(%rip), %rax
	movq	%rax, %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	376(%rbx), %rdi
	movl	$4294967295, %esi
	movzbl	-417(%rbp), %edx
	movq	%rax, -416(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder8AllocateENS1_4TypeENS0_14AllocationTypeE@PLT
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movl	$3, %edx
	movq	-416(%rbp), %xmm0
	movq	%rax, %rsi
	movq	-360(%rbp), %rcx
	movq	-344(%rbp), %rax
	movhps	-328(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-384(%rbp), %rsi
	movl	$1, %ecx
	leaq	-224(%rbp), %r9
	movq	%rax, -328(%rbp)
	movq	%r9, %rdi
	movq	24(%rsi), %rax
	movq	32(%rsi), %rsi
	movq	%r9, -416(%rbp)
	movq	360(%rax), %rdx
	addq	$256, %rdx
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	-336(%rbp), %rdi
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	-416(%rbp), %r9
	movq	%rbx, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	376(%rbx), %rdi
	movq	-336(%rbp), %rsi
	movq	(%rbx), %r12
	movq	%rax, -416(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	movq	-360(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	-328(%rbp), %xmm7
	movl	$4, %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	movdqa	%xmm7, %xmm0
	movhps	-416(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movdqa	%xmm7, %xmm0
	movhps	-344(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movsd	-440(%rbp), %xmm1
	movq	%rax, -432(%rbp)
	movq	-384(%rbp), %rax
	movapd	%xmm1, %xmm0
	movq	24(%rax), %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	-336(%rbp), %rdi
	movq	%rax, -416(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder18ForHeapNumberValueEv@PLT
	movq	376(%rbx), %rdi
	movq	-336(%rbp), %rsi
	movq	(%rbx), %r12
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	movq	-360(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	-328(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$4, %edx
	movq	%r12, %rdi
	movhps	-416(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-432(%rbp), %xmm0
	movhps	-344(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%rbx), %rdi
	movq	(%rbx), %r12
	movq	%rax, -416(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12FinishRegionEv@PLT
	xorl	%r8d, %r8d
	movl	$2, %edx
	movq	%r12, %rdi
	movq	-328(%rbp), %xmm0
	movq	-360(%rbp), %rcx
	movq	%rax, %rsi
	movhps	-416(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -328(%rbp)
	jmp	.L1162
.L1207:
	movq	%r8, -336(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-336(%rbp), %r8
	movq	%rax, %rcx
	jmp	.L1147
.L1203:
	call	__stack_chk_fail@PLT
.L1199:
	leaq	.LC29(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE23011:
	.size	_ZN2v88internal8compiler16JSCreateLowering19AllocateFastLiteralEPNS1_4NodeES4_NS1_11JSObjectRefENS0_14AllocationTypeE, .-_ZN2v88internal8compiler16JSCreateLowering19AllocateFastLiteralEPNS1_4NodeES4_NS1_11JSObjectRefENS0_14AllocationTypeE
	.section	.rodata._ZN2v88internal8compiler16JSCreateLowering27AllocateFastLiteralElementsEPNS1_4NodeES4_NS1_11JSObjectRefENS0_14AllocationTypeE.str1.8,"aMS",@progbits,1
	.align 8
.LC30:
	.string	"cannot create std::vector larger than max_size()"
	.section	.text._ZN2v88internal8compiler16JSCreateLowering27AllocateFastLiteralElementsEPNS1_4NodeES4_NS1_11JSObjectRefENS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16JSCreateLowering27AllocateFastLiteralElementsEPNS1_4NodeES4_NS1_11JSObjectRefENS0_14AllocationTypeE
	.type	_ZN2v88internal8compiler16JSCreateLowering27AllocateFastLiteralElementsEPNS1_4NodeES4_NS1_11JSObjectRefENS0_14AllocationTypeE, @function
_ZN2v88internal8compiler16JSCreateLowering27AllocateFastLiteralElementsEPNS1_4NodeES4_NS1_11JSObjectRefENS0_14AllocationTypeE:
.LFB23030:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-208(%rbp), %r15
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-224(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%r9d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$264, %rsp
	movq	%rcx, -224(%rbp)
	movq	%r8, -216(%rbp)
	movb	%r9b, -248(%rbp)
	movq	%rdx, -232(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal8compiler11JSObjectRef8elementsEv@PLT
	movq	%r15, %rdi
	movq	%rdx, -200(%rbp)
	movq	%rax, -208(%rbp)
	call	_ZNK2v88internal8compiler17FixedArrayBaseRef6lengthEv@PLT
	movq	%r15, %rdi
	movl	%eax, -240(%rbp)
	call	_ZNK2v88internal8compiler13HeapObjectRef3mapEv@PLT
	movq	%r15, %rdi
	movq	%rax, -192(%rbp)
	movq	%rdx, -184(%rbp)
	call	_ZNK2v88internal8compiler17FixedArrayBaseRef6lengthEv@PLT
	testl	%eax, %eax
	jne	.L1209
.L1212:
	cmpb	$1, %r12b
	je	.L1249
.L1211:
	movq	24(%rbx), %r12
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler17FixedArrayBaseRef6objectEv@PLT
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
.L1208:
	movq	-56(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L1250
	addq	$264, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1209:
	.cfi_restore_state
	leaq	-192(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -280(%rbp)
	call	_ZNK2v88internal8compiler6MapRef18IsFixedCowArrayMapEv@PLT
	testb	%al, %al
	jne	.L1212
	movslq	-240(%rbp), %rdx
	movq	40(%rbx), %rdi
	cmpq	$268435455, %rdx
	ja	.L1251
	testq	%rdx, %rdx
	je	.L1214
	movq	16(%rdi), %rax
	salq	$3, %rdx
	movq	%rdx, %rsi
	movq	%rax, %rcx
	movq	%rax, -256(%rbp)
	movq	24(%rdi), %rax
	subq	%rcx, %rax
	cmpq	%rax, %rdx
	ja	.L1252
	addq	-256(%rbp), %rsi
	movq	%rsi, 16(%rdi)
	movq	%rcx, %rdi
.L1216:
	xorl	%esi, %esi
	movq	%rdi, -272(%rbp)
	call	memset@PLT
	movq	-280(%rbp), %rdi
	call	_ZNK2v88internal8compiler6MapRef13instance_typeEv@PLT
	cmpw	$74, %ax
	je	.L1253
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12AsFixedArrayEv@PLT
	movq	%rdx, -168(%rbp)
	movl	-240(%rbp), %edx
	movq	%rax, -176(%rbp)
	testl	%edx, %edx
	jle	.L1248
	movl	-240(%rbp), %eax
	movl	%r12d, -292(%rbp)
	xorl	%r13d, %r13d
	leaq	-160(%rbp), %r15
	subl	$1, %eax
	movq	%rax, -264(%rbp)
	leaq	-176(%rbp), %rax
	movq	%rax, -288(%rbp)
	movq	%rax, %r12
	jmp	.L1229
	.p2align 4,,10
	.p2align 3
.L1254:
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef10AsJSObjectEv@PLT
	movzbl	-248(%rbp), %r9d
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	%rdx, %r8
	movq	-232(%rbp), %rdx
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler16JSCreateLowering19AllocateFastLiteralEPNS1_4NodeES4_NS1_11JSObjectRefENS0_14AllocationTypeE
	movq	%rax, %r14
	movq	-256(%rbp), %rax
	movq	%r14, (%rax,%r13,8)
	leaq	1(%r13), %rax
	cmpq	%r13, -264(%rbp)
	je	.L1245
.L1228:
	movq	%rax, %r13
.L1229:
	movq	%r12, %rdi
	movl	%r13d, %esi
	call	_ZNK2v88internal8compiler13FixedArrayRef3getEi@PLT
	movq	%r15, %rdi
	movq	%rax, -160(%rbp)
	movq	%rdx, -152(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef10IsJSObjectEv@PLT
	testb	%al, %al
	jne	.L1254
	movq	24(%rbx), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	-256(%rbp), %rcx
	movq	%rax, (%rcx,%r13,8)
	leaq	1(%r13), %rax
	cmpq	%r13, -264(%rbp)
	jne	.L1228
.L1245:
	movl	-292(%rbp), %r12d
.L1224:
	movdqa	-192(%rbp), %xmm2
	movq	24(%rbx), %r13
	movq	-288(%rbp), %rdi
	movaps	%xmm2, -176(%rbp)
	call	_ZNK2v88internal8compiler6MapRef13instance_typeEv@PLT
	movq	0(%r13), %r9
	movq	8(%r13), %rdi
	movl	$1, %esi
	movq	%r9, -248(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder11BeginRegionENS1_19RegionObservabilityE@PLT
	movq	-248(%rbp), %r9
	movq	%r14, -96(%rbp)
	leaq	-96(%rbp), %r14
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	pxor	%xmm0, %xmm0
	movq	0(%r13), %r9
	movq	%r13, %rdi
	movq	%rax, -248(%rbp)
	movl	-240(%rbp), %eax
	movq	%r9, -264(%rbp)
	leal	16(,%rax,8), %eax
	cvtsi2sdl	%eax, %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	376(%r13), %rdi
	movl	%r12d, %edx
	movl	$16777217, %esi
	movq	%rax, -256(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder8AllocateENS1_4TypeENS0_14AllocationTypeE@PLT
	movq	-264(%rbp), %r9
	xorl	%r8d, %r8d
	movq	-232(%rbp), %rcx
	movq	-256(%rbp), %xmm0
	movl	$3, %edx
	movq	%rax, %rsi
	movq	%r9, %rdi
	movq	%rcx, -80(%rbp)
	movq	%r14, %rcx
	movhps	-248(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r15, %rdi
	movq	%rax, -264(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	-288(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	376(%r13), %rdi
	movq	%r15, %rsi
	movq	0(%r13), %r12
	movq	%rax, -248(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$4, %edx
	movq	-264(%rbp), %xmm1
	movq	%rax, %rsi
	movq	%r12, %rdi
	movdqa	%xmm1, %xmm0
	movhps	-248(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movdqa	%xmm1, %xmm0
	movhps	-232(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rdi
	cvtsi2sdl	-240(%rbp), %xmm0
	movq	%rax, -256(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%r15, %rdi
	movq	%rax, -248(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder19ForFixedArrayLengthEv@PLT
	movq	376(%r13), %rdi
	movq	%r15, %rsi
	movq	0(%r13), %r12
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$4, %edx
	movq	-264(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r12, %rdi
	movhps	-248(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-256(%rbp), %xmm0
	movhps	-232(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-280(%rbp), %rdi
	movq	%rax, -248(%rbp)
	call	_ZNK2v88internal8compiler6MapRef13instance_typeEv@PLT
	movq	%r15, %rdi
	cmpw	$74, %ax
	je	.L1255
	call	_ZN2v88internal8compiler13AccessBuilder20ForFixedArrayElementEv@PLT
.L1233:
	movl	-240(%rbp), %eax
	testl	%eax, %eax
	jle	.L1231
	movl	-240(%rbp), %eax
	movq	%r15, -288(%rbp)
	xorl	%r12d, %r12d
	subl	$1, %eax
	movq	%rax, -280(%rbp)
	movq	%rbx, %rax
	movq	%r12, %rbx
	movq	%rax, %r12
	.p2align 4,,10
	.p2align 3
.L1234:
	pxor	%xmm0, %xmm0
	movq	-272(%rbp), %rax
	movq	24(%r12), %rdi
	cvtsi2sdl	%ebx, %xmm0
	movq	(%rax,%rbx,8), %rax
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	376(%r13), %rdi
	movq	-288(%rbp), %rsi
	movq	0(%r13), %r15
	movq	%rax, -256(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder12StoreElementERKNS1_13ElementAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$5, %edx
	movq	-264(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movq	-232(%rbp), %rax
	movhps	-256(%rbp), %xmm0
	movq	%rax, -64(%rbp)
	movaps	%xmm0, -96(%rbp)
	movq	-240(%rbp), %xmm0
	movhps	-248(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -248(%rbp)
	movq	%rbx, %rax
	addq	$1, %rbx
	cmpq	%rax, -280(%rbp)
	jne	.L1234
.L1231:
	movq	8(%r13), %rdi
	movq	0(%r13), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12FinishRegionEv@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$2, %edx
	movq	-264(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r12, %rdi
	movhps	-248(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	jmp	.L1208
	.p2align 4,,10
	.p2align 3
.L1249:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler11JSObjectRef21EnsureElementsTenuredEv@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler11JSObjectRef8elementsEv@PLT
	movq	%rax, -208(%rbp)
	movq	%rdx, -200(%rbp)
	jmp	.L1211
	.p2align 4,,10
	.p2align 3
.L1255:
	call	_ZN2v88internal8compiler13AccessBuilder26ForFixedDoubleArrayElementEv@PLT
	jmp	.L1233
	.p2align 4,,10
	.p2align 3
.L1253:
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef18AsFixedDoubleArrayEv@PLT
	movl	-240(%rbp), %ecx
	movq	%rax, -160(%rbp)
	movq	%rdx, -152(%rbp)
	testl	%ecx, %ecx
	jle	.L1248
	xorl	%r13d, %r13d
	movq	%r14, -248(%rbp)
	leaq	-160(%rbp), %r15
	movl	%r13d, %r14d
	movq	-256(%rbp), %r13
	movl	%r12d, -256(%rbp)
	jmp	.L1223
	.p2align 4,,10
	.p2align 3
.L1257:
	movq	24(%rbx), %rdi
	call	_ZN2v88internal8compiler7JSGraph15TheHoleConstantEv@PLT
	movq	%rax, 0(%r13)
.L1222:
	addl	$1, %r14d
	addq	$8, %r13
	cmpl	%r14d, -240(%rbp)
	je	.L1256
.L1223:
	movl	%r14d, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler19FixedDoubleArrayRef11is_the_holeEi@PLT
	testb	%al, %al
	jne	.L1257
	movq	24(%rbx), %r12
	movl	%r14d, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler19FixedDoubleArrayRef10get_scalarEi@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%rax, 0(%r13)
	jmp	.L1222
	.p2align 4,,10
	.p2align 3
.L1256:
	leaq	-176(%rbp), %rax
	movq	-248(%rbp), %r14
	movl	-256(%rbp), %r12d
	movq	%rax, -288(%rbp)
	jmp	.L1224
	.p2align 4,,10
	.p2align 3
.L1214:
	movq	-280(%rbp), %rdi
	call	_ZNK2v88internal8compiler6MapRef13instance_typeEv@PLT
	movq	%r15, %rdi
	cmpw	$74, %ax
	je	.L1235
	call	_ZNK2v88internal8compiler9ObjectRef12AsFixedArrayEv@PLT
	movq	$0, -272(%rbp)
.L1248:
	leaq	-176(%rbp), %rax
	leaq	-160(%rbp), %r15
	movq	%rax, -288(%rbp)
	jmp	.L1224
.L1252:
	movq	%rdx, -264(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-264(%rbp), %rdx
	movq	%rax, -256(%rbp)
	movq	%rax, %rdi
	jmp	.L1216
.L1235:
	call	_ZNK2v88internal8compiler9ObjectRef18AsFixedDoubleArrayEv@PLT
	movq	$0, -272(%rbp)
	movq	%rax, -160(%rbp)
	movq	%rdx, -152(%rbp)
	jmp	.L1248
.L1250:
	call	__stack_chk_fail@PLT
.L1251:
	leaq	.LC30(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE23030:
	.size	_ZN2v88internal8compiler16JSCreateLowering27AllocateFastLiteralElementsEPNS1_4NodeES4_NS1_11JSObjectRefENS0_14AllocationTypeE, .-_ZN2v88internal8compiler16JSCreateLowering27AllocateFastLiteralElementsEPNS1_4NodeES4_NS1_11JSObjectRefENS0_14AllocationTypeE
	.section	.text._ZN2v88internal8compiler16JSCreateLowering34ReduceJSCreateLiteralArrayOrObjectEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16JSCreateLowering34ReduceJSCreateLiteralArrayOrObjectEPNS1_4NodeE
	.type	_ZN2v88internal8compiler16JSCreateLowering34ReduceJSCreateLiteralArrayOrObjectEPNS1_4NodeE, @function
_ZN2v88internal8compiler16JSCreateLowering34ReduceJSCreateLiteralArrayOrObjectEPNS1_4NodeE:
.LFB22983:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-128(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler25CreateLiteralParametersOfEPKNS1_8OperatorE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	8(%rbx), %rdx
	movq	32(%r13), %rsi
	movq	%r15, %rdi
	movl	$1, %ecx
	movq	%rax, %r14
	call	_ZN2v88internal8compiler17FeedbackVectorRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movl	16(%rbx), %esi
	movq	%r15, %rdi
	leaq	-112(%rbp), %rbx
	call	_ZNK2v88internal8compiler17FeedbackVectorRef3getENS0_12FeedbackSlotE@PLT
	movq	%rbx, %rdi
	movq	%rax, -112(%rbp)
	movq	%rdx, -104(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef16IsAllocationSiteEv@PLT
	testb	%al, %al
	jne	.L1273
.L1259:
	xorl	%eax, %eax
.L1263:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1274
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1273:
	.cfi_restore_state
	movq	%rbx, %rdi
	leaq	-96(%rbp), %rbx
	call	_ZNK2v88internal8compiler9ObjectRef16AsAllocationSiteEv@PLT
	movq	%rbx, %rdi
	movq	%rax, -96(%rbp)
	movq	%rdx, -88(%rbp)
	call	_ZNK2v88internal8compiler17AllocationSiteRef13IsFastLiteralEv@PLT
	testb	%al, %al
	je	.L1259
	xorl	%r9d, %r9d
	cmpb	$0, _ZN2v88internal32FLAG_allocation_site_pretenuringE(%rip)
	movq	16(%r13), %rdi
	jne	.L1275
.L1261:
	movq	%rbx, %rsi
	movb	%r9b, -137(%rbp)
	call	_ZN2v88internal8compiler23CompilationDependencies21DependOnElementsKindsERKNS1_17AllocationSiteRefE@PLT
	leaq	-80(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZNK2v88internal8compiler17AllocationSiteRef11boilerplateEv@PLT
	cmpb	$0, -80(%rbp)
	movzbl	-137(%rbp), %r9d
	je	.L1276
	movq	-64(%rbp), %r8
	movq	-72(%rbp), %rcx
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	-136(%rbp), %rsi
	call	_ZN2v88internal8compiler16JSCreateLowering19AllocateFastLiteralEPNS1_4NodeES4_NS1_11JSObjectRefENS0_14AllocationTypeE
	movq	8(%r13), %rdi
	movq	%r14, %r8
	movq	%r12, %rsi
	movq	%rax, %rbx
	movq	(%rdi), %rax
	movq	%rbx, %rcx
	movq	%rbx, %rdx
	call	*32(%rax)
	movq	%rbx, %rax
	jmp	.L1263
	.p2align 4,,10
	.p2align 3
.L1275:
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler23CompilationDependencies21DependOnPretenureModeERKNS1_17AllocationSiteRefE@PLT
	movq	16(%r13), %rdi
	movl	%eax, %r9d
	jmp	.L1261
	.p2align 4,,10
	.p2align 3
.L1276:
	leaq	.LC15(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1274:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22983:
	.size	_ZN2v88internal8compiler16JSCreateLowering34ReduceJSCreateLiteralArrayOrObjectEPNS1_4NodeE, .-_ZN2v88internal8compiler16JSCreateLowering34ReduceJSCreateLiteralArrayOrObjectEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler16JSCreateLowering6ReduceEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16JSCreateLowering6ReduceEPNS1_4NodeE
	.type	_ZN2v88internal8compiler16JSCreateLowering6ReduceEPNS1_4NodeE, @function
_ZN2v88internal8compiler16JSCreateLowering6ReduceEPNS1_4NodeE:
.LFB22932:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	movzwl	16(%rax), %eax
	subw	$716, %ax
	cmpw	$38, %ax
	ja	.L1278
	leaq	.L1280(%rip), %rdx
	movzwl	%ax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler16JSCreateLowering6ReduceEPNS1_4NodeE,"a",@progbits
	.align 4
	.align 4
.L1280:
	.long	.L1301-.L1280
	.long	.L1300-.L1280
	.long	.L1299-.L1280
	.long	.L1278-.L1280
	.long	.L1298-.L1280
	.long	.L1297-.L1280
	.long	.L1296-.L1280
	.long	.L1295-.L1280
	.long	.L1294-.L1280
	.long	.L1293-.L1280
	.long	.L1292-.L1280
	.long	.L1291-.L1280
	.long	.L1290-.L1280
	.long	.L1289-.L1280
	.long	.L1288-.L1280
	.long	.L1288-.L1280
	.long	.L1287-.L1280
	.long	.L1286-.L1280
	.long	.L1285-.L1280
	.long	.L1284-.L1280
	.long	.L1278-.L1280
	.long	.L1278-.L1280
	.long	.L1278-.L1280
	.long	.L1278-.L1280
	.long	.L1278-.L1280
	.long	.L1278-.L1280
	.long	.L1278-.L1280
	.long	.L1278-.L1280
	.long	.L1278-.L1280
	.long	.L1278-.L1280
	.long	.L1278-.L1280
	.long	.L1278-.L1280
	.long	.L1278-.L1280
	.long	.L1278-.L1280
	.long	.L1278-.L1280
	.long	.L1283-.L1280
	.long	.L1282-.L1280
	.long	.L1281-.L1280
	.long	.L1279-.L1280
	.section	.text._ZN2v88internal8compiler16JSCreateLowering6ReduceEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L1278:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1288:
	jmp	_ZN2v88internal8compiler16JSCreateLowering34ReduceJSCreateLiteralArrayOrObjectEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L1289:
	jmp	_ZN2v88internal8compiler16JSCreateLowering27ReduceJSCreateKeyValueArrayEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L1290:
	jmp	_ZN2v88internal8compiler16JSCreateLowering30ReduceJSCreateIterResultObjectEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L1291:
	jmp	_ZN2v88internal8compiler16JSCreateLowering29ReduceJSCreateGeneratorObjectEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L1292:
	jmp	_ZN2v88internal8compiler16JSCreateLowering32ReduceJSCreateEmptyLiteralObjectEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L1293:
	jmp	_ZN2v88internal8compiler16JSCreateLowering31ReduceJSCreateEmptyLiteralArrayEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L1286:
	jmp	_ZN2v88internal8compiler16JSCreateLowering20ReduceJSCreateObjectEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L1287:
	jmp	_ZN2v88internal8compiler16JSCreateLowering27ReduceJSCreateLiteralRegExpEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L1298:
	jmp	_ZN2v88internal8compiler16JSCreateLowering27ReduceJSCreateArrayIteratorEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L1299:
	jmp	_ZN2v88internal8compiler16JSCreateLowering19ReduceJSCreateArrayEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L1300:
	jmp	_ZN2v88internal8compiler16JSCreateLowering23ReduceJSCreateArgumentsEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L1301:
	jmp	_ZN2v88internal8compiler16JSCreateLowering14ReduceJSCreateEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L1296:
	jmp	_ZN2v88internal8compiler16JSCreateLowering27ReduceJSCreateBoundFunctionEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L1297:
	jmp	_ZN2v88internal8compiler16JSCreateLowering33ReduceJSCreateAsyncFunctionObjectEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L1294:
	jmp	_ZN2v88internal8compiler16JSCreateLowering32ReduceJSCreateCollectionIteratorEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L1295:
	jmp	_ZN2v88internal8compiler16JSCreateLowering21ReduceJSCreateClosureEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L1284:
	jmp	_ZN2v88internal8compiler16JSCreateLowering28ReduceJSCreateStringIteratorEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L1285:
	jmp	_ZN2v88internal8compiler16JSCreateLowering21ReduceJSCreatePromiseEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L1279:
	jmp	_ZN2v88internal8compiler16JSCreateLowering26ReduceJSCreateBlockContextEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L1281:
	jmp	_ZN2v88internal8compiler16JSCreateLowering25ReduceJSCreateWithContextEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L1282:
	jmp	_ZN2v88internal8compiler16JSCreateLowering26ReduceJSCreateCatchContextEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L1283:
	jmp	_ZN2v88internal8compiler16JSCreateLowering29ReduceJSCreateFunctionContextEPNS1_4NodeE
	.cfi_endproc
.LFE22932:
	.size	_ZN2v88internal8compiler16JSCreateLowering6ReduceEPNS1_4NodeE, .-_ZN2v88internal8compiler16JSCreateLowering6ReduceEPNS1_4NodeE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler16JSCreateLowering6ReduceEPNS1_4NodeE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler16JSCreateLowering6ReduceEPNS1_4NodeE, @function
_GLOBAL__sub_I__ZN2v88internal8compiler16JSCreateLowering6ReduceEPNS1_4NodeE:
.LFB28121:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE28121:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler16JSCreateLowering6ReduceEPNS1_4NodeE, .-_GLOBAL__sub_I__ZN2v88internal8compiler16JSCreateLowering6ReduceEPNS1_4NodeE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler16JSCreateLowering6ReduceEPNS1_4NodeE
	.weak	_ZTVN2v88internal8compiler16JSCreateLoweringE
	.section	.data.rel.ro._ZTVN2v88internal8compiler16JSCreateLoweringE,"awG",@progbits,_ZTVN2v88internal8compiler16JSCreateLoweringE,comdat
	.align 8
	.type	_ZTVN2v88internal8compiler16JSCreateLoweringE, @object
	.size	_ZTVN2v88internal8compiler16JSCreateLoweringE, 56
_ZTVN2v88internal8compiler16JSCreateLoweringE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal8compiler16JSCreateLoweringD1Ev
	.quad	_ZN2v88internal8compiler16JSCreateLoweringD0Ev
	.quad	_ZNK2v88internal8compiler16JSCreateLowering12reducer_nameEv
	.quad	_ZN2v88internal8compiler16JSCreateLowering6ReduceEPNS1_4NodeE
	.quad	_ZN2v88internal8compiler7Reducer8FinalizeEv
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC10:
	.long	0
	.long	0
	.align 8
.LC11:
	.long	0
	.long	-1073741824
	.align 8
.LC13:
	.long	0
	.long	1087372288
	.align 8
.LC14:
	.long	0
	.long	1078460416
	.align 8
.LC16:
	.long	0
	.long	1079377920
	.align 8
.LC17:
	.long	0
	.long	1078198272
	.align 8
.LC19:
	.long	0
	.long	1073741824
	.align 8
.LC20:
	.long	0
	.long	1077936128
	.align 8
.LC21:
	.long	0
	.long	1074790400
	.align 8
.LC22:
	.long	0
	.long	1078722560
	.align 8
.LC23:
	.long	0
	.long	1075052544
	.align 8
.LC24:
	.long	0
	.long	1072693248
	.align 8
.LC28:
	.long	0
	.long	1076887552
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
