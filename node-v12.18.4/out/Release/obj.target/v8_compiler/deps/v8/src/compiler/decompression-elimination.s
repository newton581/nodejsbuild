	.file	"decompression-elimination.cc"
	.text
	.section	.rodata._ZNK2v88internal8compiler24DecompressionElimination12reducer_nameEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"DecompressionElimination"
	.section	.text._ZNK2v88internal8compiler24DecompressionElimination12reducer_nameEv,"axG",@progbits,_ZNK2v88internal8compiler24DecompressionElimination12reducer_nameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal8compiler24DecompressionElimination12reducer_nameEv
	.type	_ZNK2v88internal8compiler24DecompressionElimination12reducer_nameEv, @function
_ZNK2v88internal8compiler24DecompressionElimination12reducer_nameEv:
.LFB10575:
	.cfi_startproc
	endbr64
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE10575:
	.size	_ZNK2v88internal8compiler24DecompressionElimination12reducer_nameEv, .-_ZNK2v88internal8compiler24DecompressionElimination12reducer_nameEv
	.section	.text._ZN2v88internal8compiler24DecompressionEliminationD2Ev,"axG",@progbits,_ZN2v88internal8compiler24DecompressionEliminationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler24DecompressionEliminationD2Ev
	.type	_ZN2v88internal8compiler24DecompressionEliminationD2Ev, @function
_ZN2v88internal8compiler24DecompressionEliminationD2Ev:
.LFB12205:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE12205:
	.size	_ZN2v88internal8compiler24DecompressionEliminationD2Ev, .-_ZN2v88internal8compiler24DecompressionEliminationD2Ev
	.weak	_ZN2v88internal8compiler24DecompressionEliminationD1Ev
	.set	_ZN2v88internal8compiler24DecompressionEliminationD1Ev,_ZN2v88internal8compiler24DecompressionEliminationD2Ev
	.section	.text._ZN2v88internal8compiler24DecompressionEliminationD0Ev,"axG",@progbits,_ZN2v88internal8compiler24DecompressionEliminationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler24DecompressionEliminationD0Ev
	.type	_ZN2v88internal8compiler24DecompressionEliminationD0Ev, @function
_ZN2v88internal8compiler24DecompressionEliminationD0Ev:
.LFB12207:
	.cfi_startproc
	endbr64
	movl	$40, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12207:
	.size	_ZN2v88internal8compiler24DecompressionEliminationD0Ev, .-_ZN2v88internal8compiler24DecompressionEliminationD0Ev
	.section	.text._ZN2v88internal8compiler24DecompressionEliminationC2EPNS1_15AdvancedReducer6EditorEPNS1_5GraphEPNS1_22MachineOperatorBuilderEPNS1_21CommonOperatorBuilderE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler24DecompressionEliminationC2EPNS1_15AdvancedReducer6EditorEPNS1_5GraphEPNS1_22MachineOperatorBuilderEPNS1_21CommonOperatorBuilderE
	.type	_ZN2v88internal8compiler24DecompressionEliminationC2EPNS1_15AdvancedReducer6EditorEPNS1_5GraphEPNS1_22MachineOperatorBuilderEPNS1_21CommonOperatorBuilderE, @function
_ZN2v88internal8compiler24DecompressionEliminationC2EPNS1_15AdvancedReducer6EditorEPNS1_5GraphEPNS1_22MachineOperatorBuilderEPNS1_21CommonOperatorBuilderE:
.LFB10597:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal8compiler24DecompressionEliminationE(%rip), %rax
	movq	%rsi, 8(%rdi)
	movq	%rax, (%rdi)
	movq	%rdx, 16(%rdi)
	movq	%rcx, 24(%rdi)
	movq	%r8, 32(%rdi)
	ret
	.cfi_endproc
.LFE10597:
	.size	_ZN2v88internal8compiler24DecompressionEliminationC2EPNS1_15AdvancedReducer6EditorEPNS1_5GraphEPNS1_22MachineOperatorBuilderEPNS1_21CommonOperatorBuilderE, .-_ZN2v88internal8compiler24DecompressionEliminationC2EPNS1_15AdvancedReducer6EditorEPNS1_5GraphEPNS1_22MachineOperatorBuilderEPNS1_21CommonOperatorBuilderE
	.globl	_ZN2v88internal8compiler24DecompressionEliminationC1EPNS1_15AdvancedReducer6EditorEPNS1_5GraphEPNS1_22MachineOperatorBuilderEPNS1_21CommonOperatorBuilderE
	.set	_ZN2v88internal8compiler24DecompressionEliminationC1EPNS1_15AdvancedReducer6EditorEPNS1_5GraphEPNS1_22MachineOperatorBuilderEPNS1_21CommonOperatorBuilderE,_ZN2v88internal8compiler24DecompressionEliminationC2EPNS1_15AdvancedReducer6EditorEPNS1_5GraphEPNS1_22MachineOperatorBuilderEPNS1_21CommonOperatorBuilderE
	.section	.text._ZN2v88internal8compiler24DecompressionElimination25IsReducibleConstantOpcodeENS1_8IrOpcode5ValueE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler24DecompressionElimination25IsReducibleConstantOpcodeENS1_8IrOpcode5ValueE
	.type	_ZN2v88internal8compiler24DecompressionElimination25IsReducibleConstantOpcodeENS1_8IrOpcode5ValueE, @function
_ZN2v88internal8compiler24DecompressionElimination25IsReducibleConstantOpcodeENS1_8IrOpcode5ValueE:
.LFB10599:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	cmpl	$24, %esi
	je	.L6
	cmpl	$30, %esi
	sete	%al
.L6:
	ret
	.cfi_endproc
.LFE10599:
	.size	_ZN2v88internal8compiler24DecompressionElimination25IsReducibleConstantOpcodeENS1_8IrOpcode5ValueE, .-_ZN2v88internal8compiler24DecompressionElimination25IsReducibleConstantOpcodeENS1_8IrOpcode5ValueE
	.section	.rodata._ZN2v88internal8compiler24DecompressionElimination17IsValidDecompressENS1_8IrOpcode5ValueES4_.str1.1,"aMS",@progbits,1
.LC1:
	.string	"unreachable code"
	.section	.text._ZN2v88internal8compiler24DecompressionElimination17IsValidDecompressENS1_8IrOpcode5ValueES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler24DecompressionElimination17IsValidDecompressENS1_8IrOpcode5ValueES4_
	.type	_ZN2v88internal8compiler24DecompressionElimination17IsValidDecompressENS1_8IrOpcode5ValueES4_, @function
_ZN2v88internal8compiler24DecompressionElimination17IsValidDecompressENS1_8IrOpcode5ValueES4_:
.LFB10600:
	.cfi_startproc
	endbr64
	cmpl	$467, %esi
	je	.L11
	cmpl	$468, %esi
	je	.L12
	cmpl	$466, %esi
	je	.L17
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	subl	$469, %edx
	cmpl	$1, %edx
	setbe	%al
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	subl	$469, %edx
	cmpl	$2, %edx
	setbe	%al
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	andl	$-3, %edx
	cmpl	$469, %edx
	sete	%al
	ret
	.cfi_endproc
.LFE10600:
	.size	_ZN2v88internal8compiler24DecompressionElimination17IsValidDecompressENS1_8IrOpcode5ValueES4_, .-_ZN2v88internal8compiler24DecompressionElimination17IsValidDecompressENS1_8IrOpcode5ValueES4_
	.section	.text._ZN2v88internal8compiler24DecompressionElimination21GetCompressedConstantEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler24DecompressionElimination21GetCompressedConstantEPNS1_4NodeE
	.type	_ZN2v88internal8compiler24DecompressionElimination21GetCompressedConstantEPNS1_4NodeE, @function
_ZN2v88internal8compiler24DecompressionElimination21GetCompressedConstantEPNS1_4NodeE:
.LFB10601:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	(%rsi), %r8
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movzwl	16(%r8), %eax
	cmpw	$24, %ax
	je	.L19
	cmpw	$30, %ax
	jne	.L27
	movq	32(%rdi), %r13
	movq	16(%rdi), %r12
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler14HeapConstantOfEPKNS1_8OperatorE@PLT
	leaq	-32(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rax, -32(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder22CompressedHeapConstantERKNS0_6HandleINS0_10HeapObjectEEE@PLT
.L26:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L28
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	movq	16(%rdi), %r12
	movq	48(%r8), %rsi
	movq	32(%rdi), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder13Int32ConstantEi@PLT
	jmp	.L26
.L27:
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L28:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10601:
	.size	_ZN2v88internal8compiler24DecompressionElimination21GetCompressedConstantEPNS1_4NodeE, .-_ZN2v88internal8compiler24DecompressionElimination21GetCompressedConstantEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler24DecompressionElimination14ReduceCompressEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler24DecompressionElimination14ReduceCompressEPNS1_4NodeE
	.type	_ZN2v88internal8compiler24DecompressionElimination14ReduceCompressEPNS1_4NodeE, @function
_ZN2v88internal8compiler24DecompressionElimination14ReduceCompressEPNS1_4NodeE:
.LFB10602:
	.cfi_startproc
	endbr64
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %r8
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L30
	movq	16(%r8), %r8
.L30:
	movq	(%r8), %rax
	movzwl	16(%rax), %eax
	movl	%eax, %edx
	subl	$469, %eax
	cmpl	$2, %eax
	ja	.L31
	movzbl	23(%r8), %edx
	movq	32(%r8), %rax
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L42
.L40:
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	cmpw	$24, %dx
	je	.L34
	xorl	%eax, %eax
	cmpw	$30, %dx
	jne	.L40
.L34:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%r8, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal8compiler24DecompressionElimination21GetCompressedConstantEPNS1_4NodeE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	.cfi_restore 6
	movq	16(%rax), %rax
	ret
	.cfi_endproc
.LFE10602:
	.size	_ZN2v88internal8compiler24DecompressionElimination14ReduceCompressEPNS1_4NodeE, .-_ZN2v88internal8compiler24DecompressionElimination14ReduceCompressEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler24DecompressionElimination16ReduceDecompressEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler24DecompressionElimination16ReduceDecompressEPNS1_4NodeE
	.type	_ZN2v88internal8compiler24DecompressionElimination16ReduceDecompressEPNS1_4NodeE, @function
_ZN2v88internal8compiler24DecompressionElimination16ReduceDecompressEPNS1_4NodeE:
.LFB10603:
	.cfi_startproc
	endbr64
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %rcx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L44
	movq	16(%rcx), %rcx
.L44:
	movq	(%rcx), %rax
	movzwl	16(%rax), %edx
	xorl	%eax, %eax
	subl	$466, %edx
	cmpl	$2, %edx
	ja	.L47
	movzbl	23(%rcx), %edx
	movq	32(%rcx), %rax
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L48
.L47:
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	movq	16(%rax), %rax
	ret
	.cfi_endproc
.LFE10603:
	.size	_ZN2v88internal8compiler24DecompressionElimination16ReduceDecompressEPNS1_4NodeE, .-_ZN2v88internal8compiler24DecompressionElimination16ReduceDecompressEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler24DecompressionElimination9ReducePhiEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler24DecompressionElimination9ReducePhiEPNS1_4NodeE
	.type	_ZN2v88internal8compiler24DecompressionElimination9ReducePhiEPNS1_4NodeE, @function
_ZN2v88internal8compiler24DecompressionElimination9ReducePhiEPNS1_4NodeE:
.LFB10604:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$72, %rsp
	movq	%rdi, -104(%rbp)
	movl	20(%rsi), %ecx
	movl	%ecx, %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	xorl	$251658240, %edx
	andl	$251658240, %edx
	movl	20(%rax), %eax
	movl	%eax, -84(%rbp)
	movq	32(%rsi), %rax
	je	.L85
	movq	(%rax), %rax
	movq	%rax, -96(%rbp)
	movzwl	16(%rax), %r8d
	movl	-84(%rbp), %eax
	testl	%eax, %eax
	jle	.L51
	movl	-84(%rbp), %eax
	movq	%rbx, %rdx
	movl	$1, %esi
	subl	$1, %eax
	movq	%rax, %r15
	leaq	40(%r14,%rax,8), %r10
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L86:
	cmpw	%r8w, %di
	sete	%al
	addq	$8, %rdx
	andl	%eax, %esi
	cmpq	%rdx, %r10
	je	.L53
.L55:
	movq	(%rdx), %rax
	movq	(%rax), %rax
	movzwl	16(%rax), %eax
	movl	%eax, %edi
	subl	$469, %eax
	cmpl	$2, %eax
	jbe	.L86
.L52:
	xorl	%eax, %eax
.L56:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L87
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L88:
	.cfi_restore_state
	movq	-96(%rbp), %rax
	movzwl	16(%rax), %r8d
.L51:
	leal	-469(%r8), %eax
	cmpw	$2, %ax
	ja	.L67
	movq	-104(%rbp), %r15
	movl	-84(%rbp), %edx
	movl	$-32, %esi
	subl	%r8d, %esi
	movq	32(%r15), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	16(%r15), %rdi
	movq	-96(%rbp), %rsi
	xorl	%r8d, %r8d
	leaq	-64(%rbp), %rcx
	movl	$1, %edx
	movq	%r14, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%r15), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rax, %rbx
	movq	%r14, %rsi
	movq	(%rdi), %rax
	movq	%rbx, %rdx
	call	*32(%rax)
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L68
	movq	32(%rbx), %rdi
	leaq	32(%rbx), %r13
	cmpq	%rdi, %r14
	je	.L70
.L69:
	leaq	-24(%rbx), %r12
	testq	%rdi, %rdi
	je	.L71
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L71:
	movq	%r14, 0(%r13)
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L70:
	movq	%r14, %rax
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L53:
	testb	%sil, %sil
	jne	.L57
	movq	-104(%rbp), %rax
	movq	24(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder24ChangeCompressedToTaggedEv@PLT
	movl	20(%r14), %ecx
	movq	%rax, -96(%rbp)
.L57:
	leaq	0(,%r15,8), %rax
	xorl	%r13d, %r13d
	movq	%rax, -80(%rbp)
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L89:
	leaq	(%rbx,%r13), %r12
	movq	(%r12), %rdi
	movzbl	23(%rdi), %ecx
	movq	32(%rdi), %r15
	andl	$15, %ecx
	cmpl	$15, %ecx
	jne	.L59
	movq	16(%r15), %r15
.L59:
	movq	%r14, %rcx
	cmpq	%rdi, %r15
	je	.L64
.L63:
	leaq	0(,%r13,4), %rsi
	movq	%r13, %rax
	subq	%rsi, %rax
	leaq	-24(%rcx,%rax), %rsi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	testq	%r15, %r15
	movq	%r15, (%r12)
	movq	-72(%rbp), %rsi
	je	.L64
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L64:
	cmpq	%r13, -80(%rbp)
	je	.L88
	movl	20(%r14), %ecx
	addq	$8, %r13
.L73:
	xorl	$251658240, %ecx
	andl	$251658240, %ecx
	jne	.L89
	movq	(%rbx), %rcx
	leaq	16(%rcx,%r13), %r12
	movq	(%r12), %rdi
	movzbl	23(%rdi), %esi
	movq	32(%rdi), %r15
	andl	$15, %esi
	cmpl	$15, %esi
	jne	.L61
	movq	16(%r15), %r15
.L61:
	cmpq	%rdi, %r15
	jne	.L63
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L85:
	movq	16(%rax), %rdx
	movl	-84(%rbp), %esi
	movq	(%rdx), %rdx
	movq	%rdx, -96(%rbp)
	movzwl	16(%rdx), %r8d
	testl	%esi, %esi
	jle	.L51
	subl	$1, %esi
	leaq	16(%rax), %rdx
	movq	%rsi, %r15
	leaq	24(%rax,%rsi,8), %r10
	movl	$1, %esi
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L90:
	cmpw	%di, %r8w
	sete	%al
	addq	$8, %rdx
	andl	%eax, %esi
	cmpq	%rdx, %r10
	je	.L53
.L54:
	movq	(%rdx), %rax
	movq	(%rax), %rax
	movzwl	16(%rax), %eax
	movl	%eax, %edi
	subl	$469, %eax
	cmpl	$2, %eax
	jbe	.L90
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L68:
	movq	32(%rbx), %rbx
	movq	16(%rbx), %rdi
	leaq	16(%rbx), %r13
	cmpq	%rdi, %r14
	jne	.L69
	jmp	.L70
.L67:
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L87:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10604:
	.size	_ZN2v88internal8compiler24DecompressionElimination9ReducePhiEPNS1_4NodeE, .-_ZN2v88internal8compiler24DecompressionElimination9ReducePhiEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler24DecompressionElimination22ReduceTypedStateValuesEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler24DecompressionElimination22ReduceTypedStateValuesEPNS1_4NodeE
	.type	_ZN2v88internal8compiler24DecompressionElimination22ReduceTypedStateValuesEPNS1_4NodeE, @function
_ZN2v88internal8compiler24DecompressionElimination22ReduceTypedStateValuesEPNS1_4NodeE:
.LFB10605:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r9d, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	32(%r15), %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	20(%rsi), %r8d
	xorl	%esi, %esi
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L111:
	cmpl	%esi, %edx
	jle	.L94
	leaq	(%r12,%rsi,8), %r13
.L96:
	movq	0(%r13), %rdi
	leaq	1(%rsi), %rbx
	movq	(%rdi), %rax
	movzwl	16(%rax), %ecx
	subl	$469, %ecx
	cmpl	$2, %ecx
	ja	.L97
	movzbl	23(%rdi), %eax
	movq	32(%rdi), %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L98
	movq	16(%r14), %r14
.L98:
	cmpl	$15, %edx
	je	.L99
	movq	%r15, %rax
	cmpq	%r14, %rdi
	je	.L105
.L100:
	leaq	1(%rsi), %rbx
	leaq	0(,%rbx,4), %rdx
	movq	%rbx, %rcx
	subq	%rdx, %rcx
	leaq	(%rax,%rcx,8), %rsi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	testq	%r14, %r14
	movq	%r14, 0(%r13)
	movq	-56(%rbp), %rsi
	je	.L110
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movl	20(%r15), %r8d
	movl	$1, %r9d
	.p2align 4,,10
	.p2align 3
.L97:
	movq	%rbx, %rsi
.L101:
	movl	%r8d, %edx
	shrl	$24, %edx
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L111
	movq	32(%r15), %rax
	cmpl	%esi, 8(%rax)
	jle	.L94
	leaq	16(%rax,%rsi,8), %r13
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L99:
	cmpq	%r14, %rdi
	je	.L105
	movq	32(%r15), %rax
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L110:
	movl	20(%r15), %r8d
	movl	$1, %r9d
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L105:
	movl	$1, %r9d
	leaq	1(%rsi), %rbx
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L94:
	testb	%r9b, %r9b
	movl	$0, %eax
	cmove	%rax, %r15
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	movq	%r15, %rax
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10605:
	.size	_ZN2v88internal8compiler24DecompressionElimination22ReduceTypedStateValuesEPNS1_4NodeE, .-_ZN2v88internal8compiler24DecompressionElimination22ReduceTypedStateValuesEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler24DecompressionElimination17ReduceWord64EqualEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler24DecompressionElimination17ReduceWord64EqualEPNS1_4NodeE
	.type	_ZN2v88internal8compiler24DecompressionElimination17ReduceWord64EqualEPNS1_4NodeE, @function
_ZN2v88internal8compiler24DecompressionElimination17ReduceWord64EqualEPNS1_4NodeE:
.LFB10606:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	32(%rsi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movzbl	23(%rsi), %edx
	movq	32(%rsi), %rsi
	andl	$15, %edx
	movq	%rsi, %r8
	cmpl	$15, %edx
	je	.L113
	leaq	40(%rbx), %rax
	movq	%rsi, %rdi
	movq	%r14, %r9
.L114:
	movq	(%rax), %r15
	movq	(%rdi), %rax
	movzwl	16(%rax), %r12d
	movq	(%r15), %rax
	movzwl	16(%rax), %eax
	movl	%r12d, %r11d
	subl	$469, %r12d
	movl	%eax, %r10d
	subl	$469, %eax
	cmpl	$2, %eax
	setbe	%cl
	cmpl	$2, %r12d
	setbe	%al
	andb	%cl, %al
	je	.L115
	movzbl	23(%rdi), %eax
	movq	32(%rdi), %r12
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L201
	cmpl	$15, %edx
	je	.L117
.L206:
	movq	%rbx, %r8
	cmpq	%r12, %rdi
	je	.L202
.L118:
	leaq	-24(%r8), %rsi
	movq	%r9, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-64(%rbp), %r9
	testq	%r12, %r12
	movq	-56(%rbp), %rsi
	movq	%r12, (%r9)
	je	.L190
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L190:
	movzbl	23(%r15), %eax
	movzbl	23(%rbx), %edx
	movq	32(%r15), %r12
	andl	$15, %eax
	andl	$15, %edx
	cmpl	$15, %eax
	je	.L153
.L121:
	cmpl	$15, %edx
	je	.L145
.L198:
	movq	8(%r14), %rdi
	cmpq	%rdi, %r12
	je	.L148
	addq	$8, %r14
	movq	%rbx, %rsi
.L147:
	leaq	-48(%rsi), %r15
	testq	%rdi, %rdi
	je	.L149
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L149:
	movq	%r12, (%r14)
	testq	%r12, %r12
	je	.L148
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L148:
	movq	24(%r13), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder11Word32EqualEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	addq	$40, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L115:
	.cfi_restore_state
	cmpw	$24, %r11w
	je	.L128
	cmpw	$30, %r11w
	je	.L128
	cmpw	$24, %r10w
	je	.L130
	cmpw	$30, %r10w
	je	.L130
.L134:
	addq	$40, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L128:
	.cfi_restore_state
	cmpw	$24, %r10w
	je	.L155
	cmpw	$30, %r10w
	je	.L155
.L131:
	testb	%cl, %cl
	je	.L134
	movq	%rdi, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler24DecompressionElimination21GetCompressedConstantEPNS1_4NodeE
	movzbl	23(%rbx), %edx
	movq	32(%rbx), %r8
	movq	%rax, %r9
	andl	$15, %edx
.L136:
	cmpl	$15, %edx
	je	.L137
	movq	%r14, %rax
	movq	%rbx, %rsi
	cmpq	%r8, %r9
	je	.L139
.L138:
	subq	$24, %rsi
	testq	%r8, %r8
	je	.L141
	movq	%r8, %rdi
	movq	%r9, -72(%rbp)
	movq	%rax, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-72(%rbp), %r9
	movq	-64(%rbp), %rax
	movq	-56(%rbp), %rsi
.L141:
	movq	%r9, (%rax)
	testq	%r9, %r9
	je	.L139
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L139:
	cmpl	$2, %r12d
	jbe	.L203
	movzbl	23(%r15), %eax
	movq	32(%r15), %r12
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L204
.L143:
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L198
.L145:
	movq	32(%rbx), %rsi
.L200:
	movq	24(%rsi), %rdi
	cmpq	%rdi, %r12
	je	.L148
	leaq	24(%rsi), %r14
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L155:
	movl	$1, %eax
.L130:
	cmpl	$2, %r12d
	ja	.L205
	movzbl	23(%rdi), %eax
	movq	32(%rdi), %r9
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L136
	movq	16(%r9), %r9
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L113:
	movq	16(%rsi), %rdi
	leaq	16(%rsi), %r9
	leaq	24(%rsi), %rax
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L201:
	movq	16(%r12), %r12
	cmpl	$15, %edx
	jne	.L206
.L117:
	cmpq	%r12, %rdi
	jne	.L118
	movzbl	23(%r15), %edx
	movq	32(%r15), %r12
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L200
	.p2align 4,,10
	.p2align 3
.L153:
	movq	16(%r12), %r12
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L204:
	movq	16(%r12), %r12
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L137:
	movq	16(%r8), %rdx
	leaq	16(%r8), %rax
	cmpq	%rdx, %r9
	je	.L139
	movq	%r8, %rsi
	movq	%rdx, %r8
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L203:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler24DecompressionElimination21GetCompressedConstantEPNS1_4NodeE
	movq	%rax, %r12
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L198
	jmp	.L145
	.p2align 4,,10
	.p2align 3
.L202:
	movzbl	23(%r15), %eax
	movq	32(%r15), %r12
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L198
	jmp	.L153
.L205:
	andl	%eax, %ecx
	jmp	.L131
	.cfi_endproc
.LFE10606:
	.size	_ZN2v88internal8compiler24DecompressionElimination17ReduceWord64EqualEPNS1_4NodeE, .-_ZN2v88internal8compiler24DecompressionElimination17ReduceWord64EqualEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler24DecompressionElimination6ReduceEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler24DecompressionElimination6ReduceEPNS1_4NodeE
	.type	_ZN2v88internal8compiler24DecompressionElimination6ReduceEPNS1_4NodeE, @function
_ZN2v88internal8compiler24DecompressionElimination6ReduceEPNS1_4NodeE:
.LFB10607:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	movzwl	16(%rax), %eax
	cmpw	$335, %ax
	je	.L208
	jbe	.L222
	cmpw	$468, %ax
	jbe	.L223
	subw	$469, %ax
	cmpw	$2, %ax
	ja	.L212
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %rcx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L224
.L216:
	movq	(%rcx), %rax
	movzwl	16(%rax), %edx
	xorl	%eax, %eax
	subl	$466, %edx
	cmpl	$2, %edx
	ja	.L219
	movzbl	23(%rcx), %edx
	movq	32(%rcx), %rax
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L225
.L219:
	ret
	.p2align 4,,10
	.p2align 3
.L222:
	cmpw	$35, %ax
	je	.L210
	cmpw	$43, %ax
	jne	.L212
	jmp	_ZN2v88internal8compiler24DecompressionElimination22ReduceTypedStateValuesEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L223:
	cmpw	$465, %ax
	ja	.L226
.L212:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L226:
	jmp	_ZN2v88internal8compiler24DecompressionElimination14ReduceCompressEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L224:
	movq	16(%rcx), %rcx
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L210:
	jmp	_ZN2v88internal8compiler24DecompressionElimination9ReducePhiEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L208:
	jmp	_ZN2v88internal8compiler24DecompressionElimination17ReduceWord64EqualEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L225:
	movq	16(%rax), %rax
	ret
	.cfi_endproc
.LFE10607:
	.size	_ZN2v88internal8compiler24DecompressionElimination6ReduceEPNS1_4NodeE, .-_ZN2v88internal8compiler24DecompressionElimination6ReduceEPNS1_4NodeE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler24DecompressionEliminationC2EPNS1_15AdvancedReducer6EditorEPNS1_5GraphEPNS1_22MachineOperatorBuilderEPNS1_21CommonOperatorBuilderE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler24DecompressionEliminationC2EPNS1_15AdvancedReducer6EditorEPNS1_5GraphEPNS1_22MachineOperatorBuilderEPNS1_21CommonOperatorBuilderE, @function
_GLOBAL__sub_I__ZN2v88internal8compiler24DecompressionEliminationC2EPNS1_15AdvancedReducer6EditorEPNS1_5GraphEPNS1_22MachineOperatorBuilderEPNS1_21CommonOperatorBuilderE:
.LFB12245:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE12245:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler24DecompressionEliminationC2EPNS1_15AdvancedReducer6EditorEPNS1_5GraphEPNS1_22MachineOperatorBuilderEPNS1_21CommonOperatorBuilderE, .-_GLOBAL__sub_I__ZN2v88internal8compiler24DecompressionEliminationC2EPNS1_15AdvancedReducer6EditorEPNS1_5GraphEPNS1_22MachineOperatorBuilderEPNS1_21CommonOperatorBuilderE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler24DecompressionEliminationC2EPNS1_15AdvancedReducer6EditorEPNS1_5GraphEPNS1_22MachineOperatorBuilderEPNS1_21CommonOperatorBuilderE
	.weak	_ZTVN2v88internal8compiler24DecompressionEliminationE
	.section	.data.rel.ro._ZTVN2v88internal8compiler24DecompressionEliminationE,"awG",@progbits,_ZTVN2v88internal8compiler24DecompressionEliminationE,comdat
	.align 8
	.type	_ZTVN2v88internal8compiler24DecompressionEliminationE, @object
	.size	_ZTVN2v88internal8compiler24DecompressionEliminationE, 56
_ZTVN2v88internal8compiler24DecompressionEliminationE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal8compiler24DecompressionEliminationD1Ev
	.quad	_ZN2v88internal8compiler24DecompressionEliminationD0Ev
	.quad	_ZNK2v88internal8compiler24DecompressionElimination12reducer_nameEv
	.quad	_ZN2v88internal8compiler24DecompressionElimination6ReduceEPNS1_4NodeE
	.quad	_ZN2v88internal8compiler7Reducer8FinalizeEv
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
