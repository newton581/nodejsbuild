	.file	"escape-analysis.cc"
	.text
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal8compiler14EscapeAnalysisC4EPNS3_7JSGraphEPNS2_11TickCounterEPNS2_4ZoneEEUlPNS3_4NodeEPNS3_18EffectGraphReducer9ReductionEE_E10_M_managerERSt9_Any_dataRKSI_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal8compiler14EscapeAnalysisC4EPNS3_7JSGraphEPNS2_11TickCounterEPNS2_4ZoneEEUlPNS3_4NodeEPNS3_18EffectGraphReducer9ReductionEE_E10_M_managerERSt9_Any_dataRKSI_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal8compiler14EscapeAnalysisC4EPNS3_7JSGraphEPNS2_11TickCounterEPNS2_4ZoneEEUlPNS3_4NodeEPNS3_18EffectGraphReducer9ReductionEE_E10_M_managerERSt9_Any_dataRKSI_St18_Manager_operation:
.LFB26432:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L2
	cmpl	$3, %edx
	je	.L3
	cmpl	$1, %edx
	je	.L8
.L3:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE26432:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal8compiler14EscapeAnalysisC4EPNS3_7JSGraphEPNS2_11TickCounterEPNS2_4ZoneEEUlPNS3_4NodeEPNS3_18EffectGraphReducer9ReductionEE_E10_M_managerERSt9_Any_dataRKSI_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal8compiler14EscapeAnalysisC4EPNS3_7JSGraphEPNS2_11TickCounterEPNS2_4ZoneEEUlPNS3_4NodeEPNS3_18EffectGraphReducer9ReductionEE_E10_M_managerERSt9_Any_dataRKSI_St18_Manager_operation
	.section	.rodata._ZN2v88internal8compiler12_GLOBAL__N_122OffsetOfElementsAccessEPKNS1_8OperatorEPNS1_4NodeE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_122OffsetOfElementsAccessEPKNS1_8OperatorEPNS1_4NodeE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_122OffsetOfElementsAccessEPKNS1_8OperatorEPNS1_4NodeE, @function
_ZN2v88internal8compiler12_GLOBAL__N_122OffsetOfElementsAccessEPKNS1_8OperatorEPNS1_4NodeE:
.LFB22994:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-32(%rbp), %r12
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	8(%rsi), %rax
	movq	%rax, -32(%rbp)
	cmpq	$3167, %rax
	jne	.L21
.L10:
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type3MaxEv@PLT
	movq	%r12, %rdi
	movsd	%xmm0, -40(%rbp)
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	cvttsd2sil	%xmm0, %r12d
	testl	%r12d, %r12d
	js	.L12
	pxor	%xmm1, %xmm1
	movl	$1, %edx
	cvtsi2sdl	%r12d, %xmm1
	ucomisd	%xmm0, %xmm1
	setp	%al
	cmovne	%edx, %eax
	testb	%al, %al
	jne	.L12
	movsd	-40(%rbp), %xmm2
	ucomisd	%xmm2, %xmm1
	setp	%al
	cmove	%eax, %edx
	testb	%dl, %dl
	je	.L13
.L12:
	xorl	%eax, %eax
.L11:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L22
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	.cfi_restore_state
	movl	$3167, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L10
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L13:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler15ElementAccessOfEPKNS1_8OperatorE@PLT
	movl	4(%rax), %edx
	movzbl	16(%rax), %eax
	subl	$1, %eax
	cmpb	$13, %al
	ja	.L15
	movzbl	%al, %eax
	leaq	CSWTCH.372(%rip), %rcx
	movl	(%rcx,%rax,4), %ecx
	sall	%cl, %r12d
	leal	(%r12,%rdx), %eax
	salq	$32, %rax
	orq	$1, %rax
	jmp	.L11
.L22:
	call	__stack_chk_fail@PLT
.L15:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE22994:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_122OffsetOfElementsAccessEPKNS1_8OperatorEPNS1_4NodeE, .-_ZN2v88internal8compiler12_GLOBAL__N_122OffsetOfElementsAccessEPKNS1_8OperatorEPNS1_4NodeE
	.section	.rodata._ZNK2v88internal8compiler13VirtualObject7FieldAtEi.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"IsAligned(offset, kTaggedSize)"
	.section	.rodata._ZNK2v88internal8compiler13VirtualObject7FieldAtEi.str1.1,"aMS",@progbits,1
.LC2:
	.string	"Check failed: %s."
.LC3:
	.string	"!HasEscaped()"
	.section	.rodata._ZNK2v88internal8compiler13VirtualObject7FieldAtEi.str1.8
	.align 8
.LC4:
	.string	"vector::_M_range_check: __n (which is %zu) >= this->size() (which is %zu)"
	.section	.text._ZNK2v88internal8compiler13VirtualObject7FieldAtEi,"axG",@progbits,_ZNK2v88internal8compiler13VirtualObject7FieldAtEi,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal8compiler13VirtualObject7FieldAtEi
	.type	_ZNK2v88internal8compiler13VirtualObject7FieldAtEi, @function
_ZNK2v88internal8compiler13VirtualObject7FieldAtEi:
.LFB10862:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testb	$7, %sil
	jne	.L30
	cmpb	$0, 32(%rdi)
	jne	.L31
	movq	48(%rdi), %rcx
	movq	56(%rdi), %rdx
	subq	%rcx, %rdx
	sarq	$2, %rdx
	leal	0(,%rdx,8), %eax
	cmpl	%eax, %esi
	jge	.L32
	testl	%esi, %esi
	leal	7(%rsi), %eax
	cmovs	%eax, %esi
	sarl	$3, %esi
	movslq	%esi, %rsi
	cmpq	%rdx, %rsi
	jnb	.L33
	movl	(%rcx,%rsi,4), %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	salq	$32, %rax
	orq	$1, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore_state
	movabsq	$-4294967296, %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_restore_state
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L31:
	leaq	.LC3(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L33:
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE10862:
	.size	_ZNK2v88internal8compiler13VirtualObject7FieldAtEi, .-_ZNK2v88internal8compiler13VirtualObject7FieldAtEi
	.section	.rodata._ZN2v88internal8compiler15VariableTracker5Scope3GetENS1_8VariableE.str1.1,"aMS",@progbits,1
.LC5:
	.string	"var != Variable::Invalid()"
	.section	.text._ZN2v88internal8compiler15VariableTracker5Scope3GetENS1_8VariableE,"axG",@progbits,_ZN2v88internal8compiler15VariableTracker5Scope3GetENS1_8VariableE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler15VariableTracker5Scope3GetENS1_8VariableE
	.type	_ZN2v88internal8compiler15VariableTracker5Scope3GetENS1_8VariableE, @function
_ZN2v88internal8compiler15VariableTracker5Scope3GetENS1_8VariableE:
.LFB22904:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	cmpl	$-1, %esi
	je	.L67
	movq	%rdi, %r12
	movl	%esi, %edi
	movl	%esi, %ebx
	call	_ZN2v84base10hash_valueEj@PLT
	movq	24(%r12), %rdx
	testq	%rdx, %rdx
	je	.L36
	movl	20(%rdx), %esi
	xorl	%ecx, %ecx
	movl	$-2147483648, %edi
	cmpl	%esi, %eax
	je	.L37
	.p2align 4,,10
	.p2align 3
.L69:
	movl	%edi, %r8d
	xorl	%eax, %esi
	shrl	%cl, %r8d
	testl	%esi, %r8d
	jne	.L38
.L39:
	addl	$1, %ecx
	movl	%edi, %r8d
	shrl	%cl, %r8d
	testl	%esi, %r8d
	je	.L39
.L38:
	movsbl	16(%rdx), %esi
	cmpl	%ecx, %esi
	jg	.L68
.L36:
	leaq	32(%r12), %rdx
.L47:
	movq	(%rdx), %rdx
	movl	$1, %ecx
	testq	%rdx, %rdx
	je	.L48
	movq	(%rdx), %rax
	cmpw	$61, 16(%rax)
	setne	%cl
.L48:
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	movb	%cl, %al
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	.cfi_restore_state
	movslq	%ecx, %rsi
	addl	$1, %ecx
	movq	32(%rdx,%rsi,8), %rdx
	testq	%rdx, %rdx
	je	.L36
	movl	20(%rdx), %esi
	cmpl	%esi, %eax
	jne	.L69
.L37:
	movq	24(%rdx), %rax
	testq	%rax, %rax
	je	.L70
	leaq	16(%rax), %rdx
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L36
	movq	%rdx, %rcx
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L71:
	movq	%rax, %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L43
.L42:
	cmpl	%ebx, 32(%rax)
	jge	.L71
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L42
.L43:
	cmpq	%rcx, %rdx
	je	.L36
	leaq	40(%rcx), %rdx
	cmpl	%ebx, 32(%rcx)
	jle	.L47
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L70:
	cmpl	%ebx, (%rdx)
	jne	.L36
	addq	$8, %rdx
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L67:
	leaq	.LC5(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE22904:
	.size	_ZN2v88internal8compiler15VariableTracker5Scope3GetENS1_8VariableE, .-_ZN2v88internal8compiler15VariableTracker5Scope3GetENS1_8VariableE
	.section	.text._ZN2v88internal8compiler21EscapeAnalysisTracker5Scope14SetReplacementEPNS1_4NodeE,"axG",@progbits,_ZN2v88internal8compiler21EscapeAnalysisTracker5Scope14SetReplacementEPNS1_4NodeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler21EscapeAnalysisTracker5Scope14SetReplacementEPNS1_4NodeE
	.type	_ZN2v88internal8compiler21EscapeAnalysisTracker5Scope14SetReplacementEPNS1_4NodeE, @function
_ZN2v88internal8compiler21EscapeAnalysisTracker5Scope14SetReplacementEPNS1_4NodeE:
.LFB22942:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rsi, 72(%rdi)
	testq	%rsi, %rsi
	je	.L73
	movl	20(%rsi), %r13d
	movq	48(%rdi), %r12
	andl	$16777215, %r13d
	movl	%r13d, %edi
	call	_ZN2v84base10hash_valueEj@PLT
	movq	24(%r12), %r8
	xorl	%edx, %edx
	movq	%rax, %rdi
	divq	%r8
	movq	16(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L74
	movq	(%rax), %rcx
	movq	24(%rcx), %rsi
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L75:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L74
	movq	24(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%r8
	cmpq	%rdx, %r9
	jne	.L74
.L77:
	cmpq	%rsi, %rdi
	jne	.L75
	cmpl	8(%rcx), %r13d
	jne	.L75
	movq	16(%rcx), %rsi
.L73:
	movq	%rsi, 64(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L74:
	.cfi_restore_state
	movq	(%r12), %rsi
	movq	%rsi, 64(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22942:
	.size	_ZN2v88internal8compiler21EscapeAnalysisTracker5Scope14SetReplacementEPNS1_4NodeE, .-_ZN2v88internal8compiler21EscapeAnalysisTracker5Scope14SetReplacementEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler21EscapeAnalysisTracker5Scope15MarkForDeletionEv,"axG",@progbits,_ZN2v88internal8compiler21EscapeAnalysisTracker5Scope15MarkForDeletionEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler21EscapeAnalysisTracker5Scope15MarkForDeletionEv
	.type	_ZN2v88internal8compiler21EscapeAnalysisTracker5Scope15MarkForDeletionEv, @function
_ZN2v88internal8compiler21EscapeAnalysisTracker5Scope15MarkForDeletionEv:
.LFB22943:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	48(%rdi), %r12
	movq	272(%r12), %r13
	movq	352(%r13), %rax
	testq	%rax, %rax
	je	.L110
	movq	%rax, 72(%rdi)
.L97:
	movl	20(%rax), %r13d
	andl	$16777215, %r13d
	movl	%r13d, %edi
	call	_ZN2v84base10hash_valueEj@PLT
	movq	24(%r12), %r8
	xorl	%edx, %edx
	movq	%rax, %rdi
	divq	%r8
	movq	16(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L93
	movq	(%rax), %rcx
	movq	24(%rcx), %rsi
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L94:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L93
	movq	24(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%r8
	cmpq	%rdx, %r9
	jne	.L93
.L96:
	cmpq	%rsi, %rdi
	jne	.L94
	cmpl	8(%rcx), %r13d
	jne	.L94
	movq	16(%rcx), %rax
.L92:
	movq	%rax, 64(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L93:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	%rax, 64(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L110:
	.cfi_restore_state
	movq	8(%r13), %rdi
	movq	0(%r13), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4DeadEv@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 352(%r13)
	movq	%rax, 72(%rbx)
	testq	%rax, %rax
	je	.L92
	movq	48(%rbx), %r12
	jmp	.L97
	.cfi_endproc
.LFE22943:
	.size	_ZN2v88internal8compiler21EscapeAnalysisTracker5Scope15MarkForDeletionEv, .-_ZN2v88internal8compiler21EscapeAnalysisTracker5Scope15MarkForDeletionEv
	.section	.text._ZN2v88internal8compiler15VariableTrackerC2EPNS1_7JSGraphEPNS1_18EffectGraphReducerEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15VariableTrackerC2EPNS1_7JSGraphEPNS1_18EffectGraphReducerEPNS0_4ZoneE
	.type	_ZN2v88internal8compiler15VariableTrackerC2EPNS1_7JSGraphEPNS1_18EffectGraphReducerEPNS0_4ZoneE, @function
_ZN2v88internal8compiler15VariableTrackerC2EPNS1_7JSGraphEPNS1_18EffectGraphReducerEPNS0_4ZoneE:
.LFB22981:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %xmm2
	movq	%rcx, %xmm0
	movl	$100, %esi
	punpcklqdq	%xmm2, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	96(%rdi), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	addq	$80, %rdi
	subq	$16, %rsp
	movups	%xmm0, -80(%rdi)
	pxor	%xmm0, %xmm0
	movups	%xmm0, -64(%rdi)
	movq	%rcx, %xmm0
	movq	%r13, -32(%rdi)
	punpcklqdq	%xmm0, %xmm0
	movq	$1, -24(%rdi)
	movq	$0, -16(%rdi)
	movq	$0, -8(%rdi)
	movl	$0x3f800000, (%rdi)
	movq	$0, 8(%rdi)
	movq	$0, 16(%rdi)
	movups	%xmm0, -48(%rdi)
	movq	%rcx, -40(%rbp)
	call	_ZNKSt8__detail20_Prime_rehash_policy11_M_next_bktEm@PLT
	cmpq	56(%rbx), %rax
	jbe	.L112
	movq	%rax, %r12
	cmpq	$1, %rax
	je	.L118
	movq	40(%rbx), %rdi
	leaq	0(,%rax,8), %rdx
	movq	%rdx, %rsi
	movq	16(%rdi), %r13
	movq	24(%rdi), %rax
	subq	%r13, %rax
	cmpq	%rax, %rdx
	ja	.L119
	addq	%r13, %rsi
	movq	%rsi, 16(%rdi)
.L116:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	memset@PLT
.L114:
	movq	%r13, 48(%rbx)
	movq	%r12, 56(%rbx)
.L112:
	movq	-40(%rbp), %rax
	movq	$0, 112(%rbx)
	movq	$0, 120(%rbx)
	movq	%rax, 104(%rbx)
	movq	$0, 128(%rbx)
	movq	248(%r14), %rax
	movq	%r14, 136(%rbx)
	movl	$0, 144(%rbx)
	movq	%rax, 152(%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L118:
	.cfi_restore_state
	movq	$0, 96(%rbx)
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L119:
	movq	%rdx, -48(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-48(%rbp), %rdx
	movq	%rax, %r13
	jmp	.L116
	.cfi_endproc
.LFE22981:
	.size	_ZN2v88internal8compiler15VariableTrackerC2EPNS1_7JSGraphEPNS1_18EffectGraphReducerEPNS0_4ZoneE, .-_ZN2v88internal8compiler15VariableTrackerC2EPNS1_7JSGraphEPNS1_18EffectGraphReducerEPNS0_4ZoneE
	.globl	_ZN2v88internal8compiler15VariableTrackerC1EPNS1_7JSGraphEPNS1_18EffectGraphReducerEPNS0_4ZoneE
	.set	_ZN2v88internal8compiler15VariableTrackerC1EPNS1_7JSGraphEPNS1_18EffectGraphReducerEPNS0_4ZoneE,_ZN2v88internal8compiler15VariableTrackerC2EPNS1_7JSGraphEPNS1_18EffectGraphReducerEPNS0_4ZoneE
	.section	.text._ZN2v88internal8compiler20EscapeAnalysisResult21GetVirtualObjectFieldEPKNS1_13VirtualObjectEiPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20EscapeAnalysisResult21GetVirtualObjectFieldEPKNS1_13VirtualObjectEiPNS1_4NodeE
	.type	_ZN2v88internal8compiler20EscapeAnalysisResult21GetVirtualObjectFieldEPKNS1_13VirtualObjectEiPNS1_4NodeE, @function
_ZN2v88internal8compiler20EscapeAnalysisResult21GetVirtualObjectFieldEPKNS1_13VirtualObjectEiPNS1_4NodeE:
.LFB23006:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	andl	$7, %r12d
	jne	.L168
	cmpb	$0, 32(%rsi)
	jne	.L169
	movq	48(%rsi), %rax
	movq	56(%rsi), %r8
	movq	%rcx, %r14
	movq	(%rdi), %rbx
	subq	%rax, %r8
	sarq	$2, %r8
	leal	0(,%r8,8), %ecx
	cmpl	%ecx, %edx
	jge	.L170
	testl	%edx, %edx
	leal	7(%rdx), %esi
	cmovns	%edx, %esi
	movl	%esi, %edx
	sarl	$3, %edx
	movslq	%edx, %rsi
	cmpq	%r8, %rsi
	jnb	.L171
	movl	(%rax,%rsi,4), %r13d
.L124:
	movl	20(%r14), %r14d
	andl	$16777215, %r14d
	movl	%r14d, %edi
	call	_ZN2v84base10hash_valueEj@PLT
	movq	160(%rbx), %r8
	xorl	%edx, %edx
	movq	%rax, %rcx
	divq	%r8
	movq	152(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L126
	movq	(%rax), %rsi
	movq	40(%rsi), %rdi
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L127:
	movq	(%rsi), %rsi
	testq	%rsi, %rsi
	je	.L126
	movq	40(%rsi), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%r8
	cmpq	%rdx, %r9
	jne	.L126
.L129:
	cmpq	%rcx, %rdi
	jne	.L127
	cmpl	8(%rsi), %r14d
	jne	.L127
	leaq	16(%rsi), %rbx
.L146:
	cmpl	$-1, %r13d
	je	.L172
	movl	%r13d, %edi
	call	_ZN2v84base10hash_valueEj@PLT
	movq	(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L131
	movl	20(%rdx), %esi
	movl	$-2147483648, %edi
	cmpl	%esi, %eax
	je	.L132
	.p2align 4,,10
	.p2align 3
.L174:
	movl	%edi, %r10d
	movl	%r12d, %ecx
	xorl	%eax, %esi
	shrl	%cl, %r10d
	testl	%esi, %r10d
	jne	.L133
.L134:
	addl	$1, %r12d
	movl	%edi, %r11d
	movl	%r12d, %ecx
	shrl	%cl, %r11d
	testl	%esi, %r11d
	je	.L134
.L133:
	movsbl	16(%rdx), %ecx
	cmpl	%r12d, %ecx
	jg	.L173
.L131:
	leaq	8(%rbx), %rsi
	movq	(%rsi), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L170:
	.cfi_restore_state
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movl	$-1, %r13d
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L173:
	movslq	%r12d, %rcx
	addl	$1, %r12d
	movq	32(%rdx,%rcx,8), %rdx
	testq	%rdx, %rdx
	je	.L131
	movl	20(%rdx), %esi
	cmpl	%esi, %eax
	jne	.L174
.L132:
	movq	24(%rdx), %rax
	testq	%rax, %rax
	je	.L175
	leaq	16(%rax), %rcx
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L131
	movq	%rcx, %rdx
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L176:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L138
.L137:
	cmpl	32(%rax), %r13d
	jle	.L176
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L137
.L138:
	cmpq	%rdx, %rcx
	je	.L131
	leaq	40(%rdx), %rsi
	cmpl	32(%rdx), %r13d
	jl	.L131
	popq	%rbx
	movq	(%rsi), %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L126:
	.cfi_restore_state
	addq	$120, %rbx
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L175:
	leaq	8(%rdx), %rax
	leaq	8(%rbx), %rsi
	cmpl	(%rdx), %r13d
	popq	%rbx
	cmove	%rax, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	(%rsi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L168:
	.cfi_restore_state
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L169:
	leaq	.LC3(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L172:
	leaq	.LC5(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L171:
	movq	%r8, %rdx
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE23006:
	.size	_ZN2v88internal8compiler20EscapeAnalysisResult21GetVirtualObjectFieldEPKNS1_13VirtualObjectEiPNS1_4NodeE, .-_ZN2v88internal8compiler20EscapeAnalysisResult21GetVirtualObjectFieldEPKNS1_13VirtualObjectEiPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler20EscapeAnalysisResult16GetVirtualObjectEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20EscapeAnalysisResult16GetVirtualObjectEPNS1_4NodeE
	.type	_ZN2v88internal8compiler20EscapeAnalysisResult16GetVirtualObjectEPNS1_4NodeE, @function
_ZN2v88internal8compiler20EscapeAnalysisResult16GetVirtualObjectEPNS1_4NodeE:
.LFB23007:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movl	20(%rsi), %ebx
	movq	(%rdi), %r12
	andl	$16777215, %ebx
	movl	%ebx, %edi
	call	_ZN2v84base10hash_valueEj@PLT
	movq	24(%r12), %rdi
	xorl	%edx, %edx
	movq	%rax, %rsi
	divq	%rdi
	movq	16(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L178
	movq	(%rax), %rcx
	movq	%rdx, %r9
	movq	24(%rcx), %r8
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L179:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L178
	movq	24(%rcx), %r8
	xorl	%edx, %edx
	movq	%r8, %rax
	divq	%rdi
	cmpq	%rdx, %r9
	jne	.L178
.L181:
	cmpq	%r8, %rsi
	jne	.L179
	cmpl	8(%rcx), %ebx
	jne	.L179
	popq	%rbx
	movq	16(%rcx), %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L178:
	.cfi_restore_state
	movq	(%r12), %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23007:
	.size	_ZN2v88internal8compiler20EscapeAnalysisResult16GetVirtualObjectEPNS1_4NodeE, .-_ZN2v88internal8compiler20EscapeAnalysisResult16GetVirtualObjectEPNS1_4NodeE
	.section	.rodata._ZN2v88internal8compiler13VirtualObjectC2EPNS1_15VariableTrackerEji.str1.1,"aMS",@progbits,1
.LC7:
	.string	"vector::reserve"
.LC8:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZN2v88internal8compiler13VirtualObjectC2EPNS1_15VariableTrackerEji,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13VirtualObjectC2EPNS1_15VariableTrackerEji
	.type	_ZN2v88internal8compiler13VirtualObjectC2EPNS1_15VariableTrackerEji, @function
_ZN2v88internal8compiler13VirtualObjectC2EPNS1_15VariableTrackerEji:
.LFB23015:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leal	7(%rcx), %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	testl	%ecx, %ecx
	movq	(%rsi), %rax
	movb	$0, 32(%rdi)
	cmovns	%ecx, %r9d
	movq	$0, 8(%rdi)
	movq	%rax, (%rdi)
	movq	$0, 16(%rdi)
	sarl	$3, %r9d
	movq	$0, 24(%rdi)
	movslq	%r9d, %r15
	movl	%edx, 36(%rdi)
	movq	(%rsi), %rdi
	movq	$0, 48(%r12)
	movq	%rdi, 40(%r12)
	movq	$0, 56(%r12)
	movq	$0, 64(%r12)
	cmpq	$536870911, %r15
	ja	.L224
	testq	%r15, %r15
	jne	.L225
.L191:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L225:
	.cfi_restore_state
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	salq	$2, %r15
	movq	%rsi, %r8
	leaq	7(%r15), %rsi
	andq	$-8, %rsi
	subq	%rbx, %rax
	cmpq	%rax, %rsi
	ja	.L226
	addq	%rbx, %rsi
	movq	%rsi, 16(%rdi)
.L195:
	addq	%rbx, %r15
	movq	%rbx, 48(%r12)
	movq	%rbx, 56(%r12)
	movq	%r15, 64(%r12)
	cmpl	$7, %ecx
	jle	.L191
	xorl	%r13d, %r13d
	movq	$-4, %r10
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L227:
	movl	%r14d, (%rbx)
	addq	$4, %rbx
	movq	%rbx, 56(%r12)
.L198:
	addl	$1, %r13d
	cmpl	%r13d, %r9d
	jle	.L191
.L209:
	movl	144(%r8), %r14d
	leal	1(%r14), %eax
	movl	%eax, 144(%r8)
	cmpq	%rbx, %r15
	jne	.L227
	movq	48(%r12), %rcx
	movq	%r15, %rdx
	subq	%rcx, %rdx
	movq	%rdx, %rax
	sarq	$2, %rax
	cmpq	$536870911, %rax
	je	.L228
	testq	%rax, %rax
	je	.L210
	leaq	(%rax,%rax), %rdi
	movl	$2147483648, %esi
	movl	$2147483644, %r15d
	cmpq	%rdi, %rax
	jbe	.L229
.L200:
	movq	40(%r12), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %r11
	subq	%rax, %r11
	cmpq	%r11, %rsi
	ja	.L230
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L203:
	movl	%r14d, (%rax,%rdx)
	addq	%rax, %r15
	leaq	4(%rax), %rsi
	cmpq	%rbx, %rcx
	je	.L213
.L232:
	movq	%r10, %r11
	leaq	15(%rax), %rdx
	subq	%rcx, %r11
	subq	%rcx, %rdx
	addq	%rbx, %r11
	movq	%r11, %rsi
	shrq	$2, %rsi
	cmpq	$30, %rdx
	jbe	.L214
	movabsq	$4611686018427387900, %rdi
	testq	%rdi, %rsi
	je	.L214
	addq	$1, %rsi
	xorl	%edx, %edx
	movq	%rsi, %rdi
	shrq	$2, %rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L206:
	movdqu	(%rcx,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rdi
	jne	.L206
	movq	%rsi, %rdi
	andq	$-4, %rdi
	leaq	0(,%rdi,4), %rdx
	addq	%rdx, %rcx
	addq	%rax, %rdx
	cmpq	%rsi, %rdi
	je	.L208
	movl	(%rcx), %esi
	movl	%esi, (%rdx)
	leaq	4(%rcx), %rsi
	cmpq	%rsi, %rbx
	je	.L208
	movl	4(%rcx), %esi
	movl	%esi, 4(%rdx)
	leaq	8(%rcx), %rsi
	cmpq	%rsi, %rbx
	je	.L208
	movl	8(%rcx), %ecx
	movl	%ecx, 8(%rdx)
.L208:
	leaq	8(%rax,%r11), %rbx
.L204:
	movq	%rax, %xmm0
	movq	%rbx, %xmm2
	movq	%r15, 64(%r12)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 48(%r12)
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L229:
	testq	%rdi, %rdi
	jne	.L231
	xorl	%eax, %eax
	movl	$4, %esi
	xorl	%r15d, %r15d
	movl	%r14d, (%rax,%rdx)
	cmpq	%rbx, %rcx
	jne	.L232
.L213:
	movq	%rsi, %rbx
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L210:
	movl	$8, %esi
	movl	$4, %r15d
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L214:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L205:
	movl	(%rcx,%rdx,4), %edi
	movl	%edi, (%rax,%rdx,4)
	movq	%rdx, %rdi
	addq	$1, %rdx
	cmpq	%rdi, %rsi
	jne	.L205
	jmp	.L208
.L230:
	movl	%r9d, -76(%rbp)
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rdx
	movq	$-4, %r10
	movq	-72(%rbp), %r8
	movl	-76(%rbp), %r9d
	jmp	.L203
.L226:
	movl	%r9d, -72(%rbp)
	movl	%ecx, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %r8
	movl	-64(%rbp), %ecx
	movl	-72(%rbp), %r9d
	movq	%rax, %rbx
	jmp	.L195
.L228:
	leaq	.LC8(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L231:
	cmpq	$536870911, %rdi
	movl	$536870911, %r15d
	cmovbe	%rdi, %r15
	salq	$2, %r15
	leaq	7(%r15), %rsi
	andq	$-8, %rsi
	jmp	.L200
.L224:
	leaq	.LC7(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE23015:
	.size	_ZN2v88internal8compiler13VirtualObjectC2EPNS1_15VariableTrackerEji, .-_ZN2v88internal8compiler13VirtualObjectC2EPNS1_15VariableTrackerEji
	.globl	_ZN2v88internal8compiler13VirtualObjectC1EPNS1_15VariableTrackerEji
	.set	_ZN2v88internal8compiler13VirtualObjectC1EPNS1_15VariableTrackerEji,_ZN2v88internal8compiler13VirtualObjectC2EPNS1_15VariableTrackerEji
	.section	.text._ZNK2v88internal13ZoneHandleSetINS0_3MapEE8containsENS0_6HandleIS2_EE,"axG",@progbits,_ZNK2v88internal13ZoneHandleSetINS0_3MapEE8containsENS0_6HandleIS2_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal13ZoneHandleSetINS0_3MapEE8containsENS0_6HandleIS2_EE
	.type	_ZNK2v88internal13ZoneHandleSetINS0_3MapEE8containsENS0_6HandleIS2_EE, @function
_ZNK2v88internal13ZoneHandleSetINS0_3MapEE8containsENS0_6HandleIS2_EE:
.LFB25147:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	xorl	%r8d, %r8d
	cmpq	$1, %rax
	je	.L233
	testb	$3, %al
	jne	.L235
	cmpq	%rax, %rsi
	sete	%r8b
.L233:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L235:
	movq	14(%rax), %rcx
	subq	$2, %rax
	movq	8(%rax), %rax
	movq	%rcx, %rdx
	subq	%rax, %rdx
	movq	%rdx, %rdi
	sarq	$5, %rdx
	sarq	$3, %rdi
	testq	%rdx, %rdx
	jle	.L236
	salq	$5, %rdx
	addq	%rax, %rdx
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L237:
	cmpq	8(%rax), %rsi
	je	.L259
	cmpq	16(%rax), %rsi
	je	.L260
	cmpq	24(%rax), %rsi
	je	.L261
	addq	$32, %rax
	cmpq	%rdx, %rax
	je	.L262
.L242:
	cmpq	(%rax), %rsi
	jne	.L237
.L258:
	cmpq	%rax, %rcx
	setne	%r8b
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L262:
	movq	%rcx, %rdi
	subq	%rax, %rdi
	sarq	$3, %rdi
.L236:
	cmpq	$2, %rdi
	je	.L243
	cmpq	$3, %rdi
	je	.L244
	xorl	%r8d, %r8d
	cmpq	$1, %rdi
	jne	.L233
.L245:
	xorl	%r8d, %r8d
	cmpq	(%rax), %rsi
	jne	.L233
	jmp	.L258
	.p2align 4,,10
	.p2align 3
.L259:
	addq	$8, %rax
	cmpq	%rax, %rcx
	setne	%r8b
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L260:
	addq	$16, %rax
	cmpq	%rax, %rcx
	setne	%r8b
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L244:
	cmpq	(%rax), %rsi
	je	.L258
	addq	$8, %rax
.L243:
	cmpq	(%rax), %rsi
	je	.L258
	addq	$8, %rax
	jmp	.L245
	.p2align 4,,10
	.p2align 3
.L261:
	addq	$24, %rax
	cmpq	%rax, %rcx
	setne	%r8b
	jmp	.L233
	.cfi_endproc
.LFE25147:
	.size	_ZNK2v88internal13ZoneHandleSetINS0_3MapEE8containsENS0_6HandleIS2_EE, .-_ZNK2v88internal13ZoneHandleSetINS0_3MapEE8containsENS0_6HandleIS2_EE
	.section	.text._ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	.type	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_, @function
_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_:
.LFB25510:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r14
	movq	8(%rdi), %rbx
	movq	%r14, %rax
	subq	%rbx, %rax
	sarq	$3, %rax
	cmpq	$268435455, %rax
	je	.L301
	movq	%rsi, %r8
	movq	%rdi, %r13
	movq	%rsi, %r12
	movq	%rdx, %r15
	subq	%rbx, %r8
	testq	%rax, %rax
	je	.L279
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L302
	movl	$2147483640, %esi
	movl	$2147483640, %edx
.L265:
	movq	0(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	%rsi, %rcx
	jb	.L303
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L268:
	leaq	(%rax,%rdx), %rdi
	leaq	8(%rax), %rcx
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L302:
	testq	%rdx, %rdx
	jne	.L304
	movl	$8, %ecx
	xorl	%edi, %edi
	xorl	%eax, %eax
.L266:
	movq	(%r15), %rdx
	movq	%rdx, (%rax,%r8)
	cmpq	%rbx, %r12
	je	.L269
	leaq	-8(%r12), %r8
	leaq	15(%rax), %rdx
	subq	%rbx, %r8
	subq	%rbx, %rdx
	movq	%r8, %rcx
	shrq	$3, %rcx
	cmpq	$30, %rdx
	jbe	.L282
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rcx
	je	.L282
	leaq	1(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L271:
	movdqu	(%rbx,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L271
	movq	%rsi, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rcx
	addq	%rcx, %rbx
	addq	%rax, %rcx
	cmpq	%rdx, %rsi
	je	.L273
	movq	(%rbx), %rdx
	movq	%rdx, (%rcx)
.L273:
	leaq	16(%rax,%r8), %rcx
.L269:
	cmpq	%r14, %r12
	je	.L274
	subq	%r12, %r14
	leaq	15(%r12), %rdx
	leaq	-8(%r14), %r9
	subq	%rcx, %rdx
	movq	%r9, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rdx
	jbe	.L283
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rsi
	je	.L283
	leaq	1(%rsi), %r8
	xorl	%edx, %edx
	movq	%r8, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L276:
	movdqu	(%r12,%rdx), %xmm2
	movups	%xmm2, (%rcx,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rsi
	jne	.L276
	movq	%r8, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rsi
	leaq	(%rcx,%rsi), %r10
	addq	%rsi, %r12
	cmpq	%rdx, %r8
	je	.L278
	movq	(%r12), %rdx
	movq	%rdx, (%r10)
.L278:
	leaq	8(%rcx,%r9), %rcx
.L274:
	movq	%rax, %xmm0
	movq	%rcx, %xmm3
	movq	%rdi, 24(%r13)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 8(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L279:
	.cfi_restore_state
	movl	$8, %esi
	movl	$8, %edx
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L283:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L275:
	movq	(%r12,%rdx,8), %r8
	movq	%r8, (%rcx,%rdx,8)
	movq	%rdx, %r8
	addq	$1, %rdx
	cmpq	%rsi, %r8
	jne	.L275
	jmp	.L278
	.p2align 4,,10
	.p2align 3
.L282:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L270:
	movq	(%rbx,%rdx,8), %rsi
	movq	%rsi, (%rax,%rdx,8)
	movq	%rdx, %rsi
	addq	$1, %rdx
	cmpq	%rsi, %rcx
	jne	.L270
	jmp	.L273
.L303:
	movq	%r8, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %r8
	jmp	.L268
.L301:
	leaq	.LC8(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L304:
	cmpq	$268435455, %rdx
	movl	$268435455, %eax
	cmova	%rax, %rdx
	salq	$3, %rdx
	movq	%rdx, %rsi
	jmp	.L265
	.cfi_endproc
.LFE25510:
	.size	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_, .-_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	.section	.text._ZN2v88internal8compiler13PersistentMapINS1_8VariableEPNS1_4NodeENS_4base4hashIS3_EEE12FindLeftmostEPKNS9_11FocusedTreeEPiPSt5arrayISC_Lm32EE,"axG",@progbits,_ZN2v88internal8compiler13PersistentMapINS1_8VariableEPNS1_4NodeENS_4base4hashIS3_EEE12FindLeftmostEPKNS9_11FocusedTreeEPiPSt5arrayISC_Lm32EE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler13PersistentMapINS1_8VariableEPNS1_4NodeENS_4base4hashIS3_EEE12FindLeftmostEPKNS9_11FocusedTreeEPiPSt5arrayISC_Lm32EE
	.type	_ZN2v88internal8compiler13PersistentMapINS1_8VariableEPNS1_4NodeENS_4base4hashIS3_EEE12FindLeftmostEPKNS9_11FocusedTreeEPiPSt5arrayISC_Lm32EE, @function
_ZN2v88internal8compiler13PersistentMapINS1_8VariableEPNS1_4NodeENS_4base4hashIS3_EEE12FindLeftmostEPKNS9_11FocusedTreeEPiPSt5arrayISC_Lm32EE:
.LFB26395:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	movl	(%rsi), %ecx
	movsbl	16(%rdi), %edi
	cmpl	%ecx, %edi
	jle	.L312
	movl	$-2147483648, %r9d
	jmp	.L307
	.p2align 4,,10
	.p2align 3
.L316:
	testq	%r8, %r8
	jne	.L315
	movq	$0, (%rdx,%rdi,8)
	movl	(%rsi), %edi
	leal	1(%rdi), %ecx
	movl	%ecx, (%rsi)
	movsbl	16(%rax), %edi
	cmpl	%ecx, %edi
	jle	.L312
.L307:
	movslq	%ecx, %rdi
	movl	%r9d, %r10d
	movq	32(%rax,%rdi,8), %r8
	shrl	%cl, %r10d
	testl	%r10d, 20(%rax)
	jne	.L316
	movq	%r8, (%rdx,%rdi,8)
	movl	(%rsi), %edi
	leal	1(%rdi), %ecx
	movl	%ecx, (%rsi)
.L317:
	movsbl	16(%rax), %edi
	cmpl	%ecx, %edi
	jg	.L307
.L312:
	ret
	.p2align 4,,10
	.p2align 3
.L315:
	movq	%r8, %rcx
	movq	%rax, %r8
	movq	%r8, (%rdx,%rdi,8)
	movl	(%rsi), %edi
	movq	%rcx, %rax
	leal	1(%rdi), %ecx
	movl	%ecx, (%rsi)
	jmp	.L317
	.cfi_endproc
.LFE26395:
	.size	_ZN2v88internal8compiler13PersistentMapINS1_8VariableEPNS1_4NodeENS_4base4hashIS3_EEE12FindLeftmostEPKNS9_11FocusedTreeEPiPSt5arrayISC_Lm32EE, .-_ZN2v88internal8compiler13PersistentMapINS1_8VariableEPNS1_4NodeENS_4base4hashIS3_EEE12FindLeftmostEPKNS9_11FocusedTreeEPiPSt5arrayISC_Lm32EE
	.section	.text._ZNK2v88internal8compiler13PersistentMapINS1_8VariableEPNS1_4NodeENS_4base4hashIS3_EEEeqERKS9_.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK2v88internal8compiler13PersistentMapINS1_8VariableEPNS1_4NodeENS_4base4hashIS3_EEEeqERKS9_.part.0, @function
_ZNK2v88internal8compiler13PersistentMapINS1_8VariableEPNS1_4NodeENS_4base4hashIS3_EEEeqERKS9_.part.0:
.LFB29247:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-2096(%rbp), %r12
	pushq	%rbx
	subq	$2376, %rsp
	.cfi_offset 3, -56
	movq	8(%rsi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%rax, -2408(%rbp)
	movq	8(%rdi), %rax
	movq	(%rsi), %rdi
	movq	%r14, -1816(%rbp)
	movq	%rax, -2392(%rbp)
	movl	$0, -2096(%rbp)
	movups	%xmm0, -2088(%rbp)
	testq	%rdi, %rdi
	je	.L320
	leaq	-2072(%rbp), %rax
	movq	%r12, %rsi
	movq	%rax, %rdx
	movq	%rax, -2400(%rbp)
	call	_ZN2v88internal8compiler13PersistentMapINS1_8VariableEPNS1_4NodeENS_4base4hashIS3_EEE12FindLeftmostEPKNS9_11FocusedTreeEPiPSt5arrayISC_Lm32EE
	movq	%rax, -2080(%rbp)
	movq	%rax, %r15
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L322
	movq	32(%rax), %rcx
	movq	%rcx, -2088(%rbp)
	testq	%rax, %rax
	jne	.L489
	.p2align 4,,10
	.p2align 3
.L322:
	movq	8(%r15), %rax
.L323:
	cmpq	%r14, %rax
	jne	.L320
	movl	$-2147483648, %ebx
.L336:
	movq	24(%r15), %r13
	testq	%r13, %r13
	je	.L329
	movq	-2088(%rbp), %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	leaq	16(%r13), %rcx
	movq	%rax, -2088(%rbp)
	cmpq	%rcx, %rax
	jne	.L328
.L329:
	movl	-2096(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L326
	subl	$1, %ecx
	movl	20(%r15), %r8d
	xorl	%eax, %eax
	movl	%ecx, -2096(%rbp)
	movslq	%ecx, %rcx
	jmp	.L334
	.p2align 4,,10
	.p2align 3
.L333:
	subq	$1, %rcx
	movl	$1, %eax
.L334:
	movl	%ebx, %edx
	movl	%ecx, %esi
	shrl	%cl, %edx
	testl	%r8d, %edx
	jne	.L330
	movq	24(%r12,%rcx,8), %rdi
	testq	%rdi, %rdi
	jne	.L490
.L330:
	testl	%ecx, %ecx
	jne	.L333
	testb	%al, %al
	je	.L326
	movl	%esi, -2096(%rbp)
.L326:
	pxor	%xmm0, %xmm0
	movups	%xmm0, -2088(%rbp)
.L320:
	movq	-2392(%rbp), %rax
	pxor	%xmm0, %xmm0
	cmpq	$0, -2408(%rbp)
	movl	$0, -2384(%rbp)
	movups	%xmm0, -2376(%rbp)
	leaq	-2384(%rbp), %rbx
	movq	%rax, -2104(%rbp)
	je	.L338
	leaq	-2360(%rbp), %rax
	movq	-2408(%rbp), %rdi
	movq	%rbx, %rsi
	movq	%rax, %rdx
	movq	%rax, -2400(%rbp)
	call	_ZN2v88internal8compiler13PersistentMapINS1_8VariableEPNS1_4NodeENS_4base4hashIS3_EEE12FindLeftmostEPKNS9_11FocusedTreeEPiPSt5arrayISC_Lm32EE
	movq	%rax, -2368(%rbp)
	movq	%rax, %r15
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L340
	movq	32(%rax), %rdx
	movq	%rdx, -2376(%rbp)
	testq	%rax, %rax
	jne	.L491
	.p2align 4,,10
	.p2align 3
.L340:
	movq	8(%r15), %rax
.L341:
	cmpq	-2392(%rbp), %rax
	jne	.L338
	movl	$-2147483648, %r14d
.L354:
	movq	24(%r15), %r13
	testq	%r13, %r13
	je	.L347
	movq	-2376(%rbp), %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	leaq	16(%r13), %rdx
	movq	%rax, -2376(%rbp)
	cmpq	%rdx, %rax
	jne	.L346
.L347:
	movl	-2384(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L344
	subl	$1, %ecx
	movl	20(%r15), %r9d
	xorl	%eax, %eax
	movl	%ecx, -2384(%rbp)
	movslq	%ecx, %rcx
	jmp	.L352
	.p2align 4,,10
	.p2align 3
.L351:
	subq	$1, %rcx
	movl	$1, %eax
.L352:
	movl	%r14d, %edx
	movl	%ecx, %r8d
	shrl	%cl, %edx
	testl	%r9d, %edx
	jne	.L348
	movq	24(%rbx,%rcx,8), %rdi
	testq	%rdi, %rdi
	jne	.L492
.L348:
	testl	%ecx, %ecx
	jne	.L351
	testb	%al, %al
	je	.L344
	movl	%r8d, -2384(%rbp)
.L344:
	pxor	%xmm0, %xmm0
	movups	%xmm0, -2376(%rbp)
.L338:
	movq	%rbx, %rsi
	leaq	-1520(%rbp), %rdi
	movl	$36, %ecx
	rep movsq
	movq	%r12, %rsi
	leaq	-1232(%rbp), %r13
	leaq	-1520(%rbp), %r14
	leaq	-640(%rbp), %rdi
	movl	$36, %ecx
	leaq	-944(%rbp), %r12
	rep movsq
	movl	$36, %ecx
	movq	%r13, %rdi
	movq	%r14, %rsi
	rep movsq
	leaq	-640(%rbp), %rbx
	movl	$36, %ecx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	rep movsq
	movq	-1216(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L493
	movq	-928(%rbp), %rax
	testq	%rax, %rax
	je	.L358
	movl	20(%rax), %edi
	cmpl	%edi, 20(%rdx)
	jne	.L359
	cmpq	$0, 24(%rax)
	je	.L360
	movq	-936(%rbp), %rcx
	movl	32(%rcx), %esi
.L361:
	cmpq	$0, 24(%rdx)
	je	.L362
	movq	-1224(%rbp), %rcx
	movl	32(%rcx), %ecx
.L363:
	cmpl	%ecx, %esi
	jne	.L364
.L356:
	movl	$257, %esi
	movw	%si, -656(%rbp)
.L365:
	movl	$-2147483648, %r15d
	testq	%rdx, %rdx
	je	.L494
	.p2align 4,,10
	.p2align 3
.L371:
	movzbl	-656(%rbp), %r8d
	testb	%r8b, %r8b
	je	.L373
	movq	24(%rdx), %rcx
	testq	%rcx, %rcx
	je	.L374
	movq	-1224(%rbp), %rax
	movq	40(%rax), %rax
.L375:
	cmpb	$0, -655(%rbp)
	je	.L376
	movq	-928(%rbp), %rsi
	cmpq	$0, 24(%rsi)
	je	.L377
	movq	-936(%rbp), %rsi
	movq	40(%rsi), %rsi
.L378:
	cmpq	%rax, %rsi
	jne	.L382
.L395:
	testq	%rcx, %rcx
	je	.L389
	movq	-1224(%rbp), %rdi
	movq	%rdx, -2408(%rbp)
	movb	%r8b, -2400(%rbp)
	movq	%rcx, -2392(%rbp)
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	-2392(%rbp), %rcx
	movzbl	-2400(%rbp), %r8d
	movq	%rax, -1224(%rbp)
	movq	-2408(%rbp), %rdx
	addq	$16, %rcx
	cmpq	%rcx, %rax
	jne	.L384
.L389:
	movl	-1232(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L386
	subl	$1, %ecx
	movl	20(%rdx), %r9d
	xorl	%edx, %edx
	movl	%ecx, -1232(%rbp)
	movslq	%ecx, %rcx
	jmp	.L394
	.p2align 4,,10
	.p2align 3
.L393:
	subq	$1, %rcx
	movl	%r8d, %edx
.L394:
	movl	%r15d, %eax
	movl	%ecx, %esi
	shrl	%cl, %eax
	testl	%r9d, %eax
	jne	.L390
	movq	24(%r13,%rcx,8), %rdi
	testq	%rdi, %rdi
	jne	.L495
.L390:
	testl	%ecx, %ecx
	jne	.L393
	testb	%dl, %dl
	je	.L386
	movl	%esi, -1232(%rbp)
.L386:
	pxor	%xmm0, %xmm0
	movups	%xmm0, -1224(%rbp)
.L384:
	cmpb	$0, -655(%rbp)
	jne	.L496
.L397:
	leaq	-1808(%rbp), %rdi
	movl	$36, %ecx
	movq	%r13, %rsi
	rep movsq
	movq	%r14, %rdi
	movl	$36, %ecx
	movq	%r12, %rsi
	rep movsq
	leaq	-1520(%rbp), %r14
	leaq	-1808(%rbp), %rsi
	movq	%rbx, %rdi
	movq	-1792(%rbp), %r8
	movl	$36, %ecx
	movq	-1800(%rbp), %rdx
	rep movsq
	movq	-1512(%rbp), %rax
	leaq	-352(%rbp), %rdi
	movq	%r14, %rsi
	movl	$36, %ecx
	rep movsq
	movq	-1504(%rbp), %rcx
	testq	%r8, %r8
	je	.L497
	testq	%rcx, %rcx
	je	.L428
	movl	20(%rcx), %edi
	cmpl	%edi, 20(%r8)
	je	.L498
	setb	%sil
.L420:
	movl	%esi, %edi
	xorl	$1, %edi
.L410:
	movq	%rdx, %xmm0
	movq	%r8, %xmm1
	movq	%rcx, %xmm2
	movb	%sil, -64(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movb	%dil, -63(%rbp)
	movl	$72, %ecx
	movq	%r13, %rdi
	movups	%xmm0, -632(%rbp)
	movq	%rax, %xmm0
	movq	%rbx, %rsi
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, -344(%rbp)
	rep movsq
	movq	-1216(%rbp), %rdx
	movzwl	(%rsi), %eax
	movw	%ax, (%rdi)
	testq	%rdx, %rdx
	jne	.L371
.L494:
	cmpq	$0, -928(%rbp)
	jne	.L371
	movl	$1, %eax
.L318:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L499
	addq	$2376, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L490:
	.cfi_restore_state
	addl	$1, %esi
	movq	-2400(%rbp), %rdx
	movl	%esi, -2096(%rbp)
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler13PersistentMapINS1_8VariableEPNS1_4NodeENS_4base4hashIS3_EEE12FindLeftmostEPKNS9_11FocusedTreeEPiPSt5arrayISC_Lm32EE
	movq	%rax, -2080(%rbp)
	movq	%rax, %r15
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L500
	movq	32(%rax), %rax
	movq	-1816(%rbp), %rdx
	movq	%rax, -2088(%rbp)
	cmpq	%rdx, 40(%rax)
	je	.L336
.L328:
	movq	24(%r15), %rax
	testq	%rax, %rax
	je	.L322
.L489:
	movq	-2088(%rbp), %rax
	movq	40(%rax), %rax
	jmp	.L323
	.p2align 4,,10
	.p2align 3
.L492:
	movq	-2400(%rbp), %rdx
	addl	$1, %r8d
	movq	%rbx, %rsi
	movl	%r8d, -2384(%rbp)
	call	_ZN2v88internal8compiler13PersistentMapINS1_8VariableEPNS1_4NodeENS_4base4hashIS3_EEE12FindLeftmostEPKNS9_11FocusedTreeEPiPSt5arrayISC_Lm32EE
	movq	%rax, -2368(%rbp)
	movq	%rax, %r15
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L501
	movq	32(%rax), %rax
	movq	%rax, -2376(%rbp)
	movq	40(%rax), %rax
	cmpq	%rax, -2104(%rbp)
	je	.L354
.L346:
	movq	24(%r15), %rax
	testq	%rax, %rax
	je	.L340
.L491:
	movq	-2376(%rbp), %rax
	movq	40(%rax), %rax
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L501:
	movq	-2104(%rbp), %rax
	cmpq	%rax, 8(%r15)
	je	.L354
	jmp	.L346
	.p2align 4,,10
	.p2align 3
.L500:
	movq	-1816(%rbp), %rax
	cmpq	%rax, 8(%r15)
	je	.L336
	jmp	.L328
	.p2align 4,,10
	.p2align 3
.L359:
	setb	%al
.L370:
	testb	%al, %al
	je	.L357
.L358:
	movl	$1, %ecx
	movw	%cx, -656(%rbp)
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L493:
	cmpq	$0, -928(%rbp)
	je	.L356
.L357:
	movl	$256, %eax
	movw	%ax, -656(%rbp)
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L373:
	movq	-928(%rbp), %rax
	cmpq	$0, 24(%rax)
	je	.L380
	movq	-936(%rbp), %rax
	movq	40(%rax), %rax
.L381:
	cmpq	-952(%rbp), %rax
	je	.L384
	.p2align 4,,10
	.p2align 3
.L382:
	xorl	%eax, %eax
	jmp	.L318
	.p2align 4,,10
	.p2align 3
.L496:
	movq	-928(%rbp), %rdx
.L408:
	testq	%rdx, %rdx
	je	.L397
	cmpq	$0, 24(%rdx)
	je	.L402
	movq	-936(%rbp), %rdi
	movq	%rdx, -2392(%rbp)
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	-2392(%rbp), %rdx
	movq	%rax, -936(%rbp)
	movq	24(%rdx), %rdi
	leaq	16(%rdi), %rcx
	cmpq	%rcx, %rax
	jne	.L397
.L402:
	movslq	-944(%rbp), %rax
	testl	%eax, %eax
	je	.L400
	leal	-1(%rax), %esi
	subq	$2, %rax
	movl	%esi, -944(%rbp)
	movslq	%esi, %rcx
	movl	%esi, %esi
	subq	%rsi, %rax
	jmp	.L406
	.p2align 4,,10
	.p2align 3
.L503:
	movl	%ecx, -944(%rbp)
.L406:
	movl	%r15d, %esi
	movl	%ecx, %r8d
	shrl	%cl, %esi
	testl	%esi, 20(%rdx)
	jne	.L403
	movq	312(%r13,%rcx,8), %rdi
	testq	%rdi, %rdi
	jne	.L502
.L403:
	subq	$1, %rcx
	cmpq	%rax, %rcx
	jne	.L503
.L400:
	pxor	%xmm0, %xmm0
	movups	%xmm0, -936(%rbp)
	jmp	.L397
	.p2align 4,,10
	.p2align 3
.L497:
	testq	%rcx, %rcx
	movl	$1, %edi
	sete	%sil
	jmp	.L410
	.p2align 4,,10
	.p2align 3
.L498:
	cmpq	$0, 24(%rcx)
	je	.L412
	movl	32(%rax), %edi
.L413:
	cmpq	$0, 24(%r8)
	je	.L414
	movl	32(%rdx), %esi
.L415:
	cmpl	%esi, %edi
	jne	.L504
	movl	$1, %edi
	movl	$1, %esi
	jmp	.L410
	.p2align 4,,10
	.p2align 3
.L376:
	movq	-664(%rbp), %rsi
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L380:
	movq	8(%rax), %rax
	jmp	.L381
	.p2align 4,,10
	.p2align 3
.L374:
	movq	8(%rdx), %rax
	jmp	.L375
	.p2align 4,,10
	.p2align 3
.L495:
	addl	$1, %esi
	leaq	-1208(%rbp), %rdx
	movb	%r8b, -2392(%rbp)
	movl	%esi, -1232(%rbp)
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler13PersistentMapINS1_8VariableEPNS1_4NodeENS_4base4hashIS3_EEE12FindLeftmostEPKNS9_11FocusedTreeEPiPSt5arrayISC_Lm32EE
	movzbl	-2392(%rbp), %r8d
	movq	24(%rax), %rcx
	movq	%rax, -1216(%rbp)
	movq	%rax, %rdx
	testq	%rcx, %rcx
	je	.L505
	movq	32(%rcx), %rax
	movq	%rax, -1224(%rbp)
	movq	40(%rax), %rax
.L422:
	cmpq	%rax, -952(%rbp)
	je	.L395
	jmp	.L384
	.p2align 4,,10
	.p2align 3
.L502:
	addl	$1, %r8d
	leaq	-920(%rbp), %rdx
	movq	%r12, %rsi
	movl	%r8d, -944(%rbp)
	call	_ZN2v88internal8compiler13PersistentMapINS1_8VariableEPNS1_4NodeENS_4base4hashIS3_EEE12FindLeftmostEPKNS9_11FocusedTreeEPiPSt5arrayISC_Lm32EE
	movq	%rax, -928(%rbp)
	movq	%rax, %rdx
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L405
	movq	32(%rax), %rax
	movq	%rax, -936(%rbp)
	cmpq	$0, 24(%rdx)
	je	.L405
	movq	40(%rax), %rax
.L407:
	cmpq	%rax, -664(%rbp)
	je	.L408
	jmp	.L397
	.p2align 4,,10
	.p2align 3
.L428:
	xorl	%edi, %edi
	movl	$1, %esi
	jmp	.L410
.L504:
	cmpq	$0, 24(%rcx)
	je	.L416
	movl	32(%rax), %edi
.L417:
	cmpq	$0, 24(%r8)
	je	.L418
	movl	32(%rdx), %esi
.L419:
	cmpl	%esi, %edi
	setg	%sil
	jmp	.L420
	.p2align 4,,10
	.p2align 3
.L414:
	movl	(%r8), %esi
	jmp	.L415
	.p2align 4,,10
	.p2align 3
.L412:
	movl	(%rcx), %edi
	jmp	.L413
	.p2align 4,,10
	.p2align 3
.L377:
	movq	8(%rsi), %rsi
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L505:
	movq	8(%rax), %rax
	jmp	.L422
	.p2align 4,,10
	.p2align 3
.L405:
	movq	8(%rdx), %rax
	jmp	.L407
	.p2align 4,,10
	.p2align 3
.L418:
	movl	(%r8), %esi
	jmp	.L419
	.p2align 4,,10
	.p2align 3
.L416:
	movl	(%rcx), %edi
	jmp	.L417
.L364:
	cmpq	$0, 24(%rax)
	je	.L366
	movq	-936(%rbp), %rax
	movl	32(%rax), %ecx
.L367:
	cmpq	$0, 24(%rdx)
	je	.L368
	movq	-1224(%rbp), %rax
	movl	32(%rax), %eax
.L369:
	cmpl	%eax, %ecx
	setg	%al
	jmp	.L370
.L362:
	movl	(%rdx), %ecx
	jmp	.L363
.L360:
	movl	(%rax), %esi
	jmp	.L361
.L366:
	movl	(%rax), %ecx
	jmp	.L367
.L368:
	movl	(%rdx), %eax
	jmp	.L369
.L499:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29247:
	.size	_ZNK2v88internal8compiler13PersistentMapINS1_8VariableEPNS1_4NodeENS_4base4hashIS3_EEEeqERKS9_.part.0, .-_ZNK2v88internal8compiler13PersistentMapINS1_8VariableEPNS1_4NodeENS_4base4hashIS3_EEEeqERKS9_.part.0
	.section	.rodata._ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_.str1.8,"aMS",@progbits,1
	.align 8
.LC9:
	.string	"cannot create std::deque larger than max_size()"
	.section	.text._ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_,"axG",@progbits,_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	.type	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_, @function
_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_:
.LFB26696:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	88(%rdi), %r13
	movq	56(%rdi), %rsi
	movq	64(%rbx), %rdx
	subq	72(%rbx), %rdx
	movq	%r13, %r14
	movq	%rdx, %rcx
	subq	%rsi, %r14
	sarq	$3, %rcx
	movq	%r14, %rdi
	sarq	$3, %rdi
	leaq	-1(%rdi), %rax
	salq	$6, %rax
	leaq	(%rcx,%rax), %rdx
	movq	48(%rbx), %rax
	subq	32(%rbx), %rax
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpq	$268435455, %rax
	je	.L524
	movq	16(%rbx), %rcx
	movq	24(%rbx), %rdx
	movq	%r13, %rax
	subq	%rcx, %rax
	movq	%rdx, %r8
	sarq	$3, %rax
	subq	%rax, %r8
	cmpq	$1, %r8
	jbe	.L525
.L508:
	movq	8(%rbx), %rax
	testq	%rax, %rax
	je	.L516
	cmpq	$63, 8(%rax)
	ja	.L526
.L516:
	movq	(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L527
	leaq	512(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L517:
	movq	%rax, 8(%r13)
	movq	(%r12), %rdx
	movq	64(%rbx), %rax
	movq	%rdx, (%rax)
	movq	88(%rbx), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 88(%rbx)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 72(%rbx)
	movq	%rdx, 80(%rbx)
	movq	%rax, 64(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L525:
	.cfi_restore_state
	leaq	2(%rdi), %r15
	leaq	(%r15,%r15), %rax
	cmpq	%rax, %rdx
	ja	.L528
	testq	%rdx, %rdx
	movl	$1, %eax
	movq	(%rbx), %rdi
	cmovne	%rdx, %rax
	movq	16(%rdi), %r13
	leaq	2(%rdx,%rax), %rcx
	movq	24(%rdi), %rax
	leaq	0(,%rcx,8), %rsi
	subq	%r13, %rax
	cmpq	%rax, %rsi
	ja	.L529
	addq	%r13, %rsi
	movq	%rsi, 16(%rdi)
.L513:
	movq	%rcx, %rax
	movq	56(%rbx), %rsi
	subq	%r15, %rax
	shrq	%rax
	leaq	0(%r13,%rax,8), %r15
	movq	88(%rbx), %rax
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L514
	subq	%rsi, %rdx
	movq	%r15, %rdi
	movq	%rcx, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %rcx
.L514:
	movq	24(%rbx), %rax
	leaq	0(,%rax,8), %rdx
	cmpq	$15, %rdx
	jbe	.L515
	movq	16(%rbx), %rdx
	movq	%rax, 8(%rdx)
	movq	$0, (%rdx)
.L515:
	movq	%r13, 16(%rbx)
	movq	%rcx, 24(%rbx)
.L511:
	movq	%r15, 56(%rbx)
	movq	(%r15), %rax
	leaq	(%r15,%r14), %r13
	movq	(%r15), %xmm0
	movq	%r13, 88(%rbx)
	addq	$512, %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 40(%rbx)
	movq	0(%r13), %rax
	movq	%rax, 72(%rbx)
	addq	$512, %rax
	movq	%rax, 80(%rbx)
	jmp	.L508
	.p2align 4,,10
	.p2align 3
.L526:
	movq	(%rax), %rdx
	movq	%rdx, 8(%rbx)
	jmp	.L517
	.p2align 4,,10
	.p2align 3
.L528:
	subq	%r15, %rdx
	addq	$8, %r13
	shrq	%rdx
	leaq	(%rcx,%rdx,8), %r15
	movq	%r13, %rdx
	subq	%rsi, %rdx
	cmpq	%r15, %rsi
	jbe	.L510
	cmpq	%r13, %rsi
	je	.L511
	movq	%r15, %rdi
	call	memmove@PLT
	jmp	.L511
	.p2align 4,,10
	.p2align 3
.L510:
	cmpq	%r13, %rsi
	je	.L511
	leaq	8(%r14), %rdi
	subq	%rdx, %rdi
	addq	%r15, %rdi
	call	memmove@PLT
	jmp	.L511
	.p2align 4,,10
	.p2align 3
.L527:
	movl	$512, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L517
	.p2align 4,,10
	.p2align 3
.L529:
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	movq	%rax, %r13
	jmp	.L513
.L524:
	leaq	.LC9(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE26696:
	.size	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_, .-_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	.section	.text._ZN2v88internal8compiler18EffectGraphReducer7RevisitEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18EffectGraphReducer7RevisitEPNS1_4NodeE
	.type	_ZN2v88internal8compiler18EffectGraphReducer7RevisitEPNS1_4NodeE, @function
_ZN2v88internal8compiler18EffectGraphReducer7RevisitEPNS1_4NodeE:
.LFB22979:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movl	16(%rsi), %edx
	movl	16(%rdi), %ecx
	movq	%rsi, -8(%rbp)
	cmpl	%ecx, %edx
	jb	.L530
	subl	%ecx, %edx
	cmpb	$3, %dl
	je	.L534
.L530:
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L534:
	.cfi_restore_state
	addl	$1, %ecx
	movq	%rsi, %rax
	movl	%ecx, 16(%rsi)
	movq	104(%rdi), %rsi
	movq	88(%rdi), %rcx
	leaq	-8(%rsi), %rdx
	cmpq	%rdx, %rcx
	je	.L532
	movq	%rax, (%rcx)
	addq	$8, 88(%rdi)
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L532:
	.cfi_restore_state
	leaq	-8(%rbp), %rsi
	addq	$24, %rdi
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22979:
	.size	_ZN2v88internal8compiler18EffectGraphReducer7RevisitEPNS1_4NodeE, .-_ZN2v88internal8compiler18EffectGraphReducer7RevisitEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler21EscapeAnalysisTracker5Scope10SetEscapedEPNS1_4NodeE.isra.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler21EscapeAnalysisTracker5Scope10SetEscapedEPNS1_4NodeE.isra.0, @function
_ZN2v88internal8compiler21EscapeAnalysisTracker5Scope10SetEscapedEPNS1_4NodeE.isra.0:
.LFB29348:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movl	20(%rdx), %r13d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	andl	$16777215, %r13d
	movl	%r13d, %edi
	call	_ZN2v84base10hash_valueEj@PLT
	movq	24(%r12), %r8
	xorl	%edx, %edx
	movq	%rax, %rsi
	divq	%r8
	movq	16(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L536
	movq	(%rax), %rcx
	movq	%rdx, %r9
	movq	24(%rcx), %rdi
	jmp	.L539
	.p2align 4,,10
	.p2align 3
.L537:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L536
	movq	24(%rcx), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%r8
	cmpq	%rdx, %r9
	jne	.L536
.L539:
	cmpq	%rdi, %rsi
	jne	.L537
	cmpl	8(%rcx), %r13d
	jne	.L537
	movq	16(%rcx), %r12
.L546:
	testq	%r12, %r12
	je	.L535
	cmpb	$0, 32(%r12)
	je	.L557
.L535:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L558
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L557:
	.cfi_restore_state
	movb	$1, 32(%r12)
	movq	16(%r12), %r14
	movq	(%rbx), %r13
	movq	8(%r12), %rbx
	cmpq	%r14, %rbx
	je	.L535
	leaq	-64(%rbp), %r15
	jmp	.L545
	.p2align 4,,10
	.p2align 3
.L543:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	je	.L559
.L545:
	movq	(%rbx), %rcx
	movl	16(%r13), %edx
	movl	16(%rcx), %eax
	movq	%rcx, -64(%rbp)
	cmpl	%edx, %eax
	jb	.L543
	subl	%edx, %eax
	cmpb	$3, %al
	jne	.L543
	addl	$1, %edx
	movl	%edx, 16(%rcx)
	movq	104(%r13), %rax
	movq	88(%r13), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L544
	addq	$8, %rbx
	movq	%rcx, (%rdx)
	addq	$8, 88(%r13)
	cmpq	%rbx, %r14
	jne	.L545
	.p2align 4,,10
	.p2align 3
.L559:
	movq	8(%r12), %rax
	cmpq	16(%r12), %rax
	je	.L535
	movq	%rax, 16(%r12)
	jmp	.L535
	.p2align 4,,10
	.p2align 3
.L536:
	movq	(%r12), %r12
	jmp	.L546
	.p2align 4,,10
	.p2align 3
.L544:
	leaq	24(%r13), %rdi
	movq	%r15, %rsi
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	jmp	.L543
.L558:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29348:
	.size	_ZN2v88internal8compiler21EscapeAnalysisTracker5Scope10SetEscapedEPNS1_4NodeE.isra.0, .-_ZN2v88internal8compiler21EscapeAnalysisTracker5Scope10SetEscapedEPNS1_4NodeE.isra.0
	.section	.text._ZNSt8_Rb_treeIN2v88internal8compiler8VariableESt4pairIKS3_PNS2_4NodeEESt10_Select1stIS8_ESt4lessIS3_ENS1_13ZoneAllocatorIS8_EEE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS5_EESK_IJEEEEESt17_Rb_tree_iteratorIS8_ESt23_Rb_tree_const_iteratorIS8_EDpOT_,"axG",@progbits,_ZNSt8_Rb_treeIN2v88internal8compiler8VariableESt4pairIKS3_PNS2_4NodeEESt10_Select1stIS8_ESt4lessIS3_ENS1_13ZoneAllocatorIS8_EEE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS5_EESK_IJEEEEESt17_Rb_tree_iteratorIS8_ESt23_Rb_tree_const_iteratorIS8_EDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN2v88internal8compiler8VariableESt4pairIKS3_PNS2_4NodeEESt10_Select1stIS8_ESt4lessIS3_ENS1_13ZoneAllocatorIS8_EEE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS5_EESK_IJEEEEESt17_Rb_tree_iteratorIS8_ESt23_Rb_tree_const_iteratorIS8_EDpOT_
	.type	_ZNSt8_Rb_treeIN2v88internal8compiler8VariableESt4pairIKS3_PNS2_4NodeEESt10_Select1stIS8_ESt4lessIS3_ENS1_13ZoneAllocatorIS8_EEE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS5_EESK_IJEEEEESt17_Rb_tree_iteratorIS8_ESt23_Rb_tree_const_iteratorIS8_EDpOT_, @function
_ZNSt8_Rb_treeIN2v88internal8compiler8VariableESt4pairIKS3_PNS2_4NodeEESt10_Select1stIS8_ESt4lessIS3_ENS1_13ZoneAllocatorIS8_EEE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS5_EESK_IJEEEEESt17_Rb_tree_iteratorIS8_ESt23_Rb_tree_const_iteratorIS8_EDpOT_:
.LFB27038:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	$47, %rax
	jbe	.L622
	leaq	48(%rbx), %rax
	movq	%rax, 16(%rdi)
.L562:
	movq	(%rcx), %rax
	leaq	16(%r13), %rcx
	movq	%r14, %r12
	movq	$0, 40(%rbx)
	movl	(%rax), %r15d
	movl	%r15d, 32(%rbx)
	cmpq	%r14, %rcx
	je	.L623
	movl	32(%r14), %r14d
	cmpl	%r14d, %r15d
	jge	.L575
	movq	32(%r13), %r14
	cmpq	%r12, %r14
	je	.L602
	movq	%r12, %rdi
	movq	%rcx, -56(%rbp)
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	-56(%rbp), %rcx
	cmpl	32(%rax), %r15d
	jle	.L577
	cmpq	$0, 24(%rax)
	je	.L565
.L602:
	movq	%r12, %rax
.L597:
	testq	%rax, %rax
	setne	%al
.L596:
	cmpq	%r12, %rcx
	je	.L610
	testb	%al, %al
	je	.L624
.L610:
	movl	$1, %edi
.L587:
	movq	%r12, %rdx
	movq	%rbx, %rsi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 48(%r13)
	addq	$24, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L575:
	.cfi_restore_state
	movl	%r15d, -56(%rbp)
	jle	.L573
	cmpq	%r12, 40(%r13)
	je	.L619
	movq	%r12, %rdi
	movq	%rcx, -64(%rbp)
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	-64(%rbp), %rcx
	cmpl	32(%rax), %r15d
	jge	.L585
	cmpq	$0, 24(%r12)
	movl	-56(%rbp), %edx
	je	.L586
	movq	%rax, %r12
	movl	$1, %edi
	jmp	.L587
	.p2align 4,,10
	.p2align 3
.L568:
	movq	%r15, %r12
	testb	%sil, %sil
	jne	.L566
.L571:
	cmpl	%edi, %edx
	jl	.L574
	movq	%r15, %r12
.L573:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L623:
	.cfi_restore_state
	cmpq	$0, 48(%r13)
	je	.L564
	movq	40(%r13), %rax
	cmpl	%r15d, 32(%rax)
	jl	.L565
.L564:
	movq	24(%r13), %r15
	testq	%r15, %r15
	je	.L598
	movl	32(%rbx), %edi
	jmp	.L567
	.p2align 4,,10
	.p2align 3
.L625:
	movq	16(%r15), %rax
	movl	$1, %esi
.L570:
	testq	%rax, %rax
	je	.L568
	movq	%rax, %r15
.L567:
	movl	32(%r15), %edx
	cmpl	%edx, %edi
	jl	.L625
	movq	24(%r15), %rax
	xorl	%esi, %esi
	jmp	.L570
	.p2align 4,,10
	.p2align 3
.L565:
	movq	%rax, %r12
	xorl	%eax, %eax
	jmp	.L596
	.p2align 4,,10
	.p2align 3
.L624:
	movl	32(%rbx), %edx
	movl	32(%r12), %r14d
.L586:
	xorl	%edi, %edi
	cmpl	%edx, %r14d
	setg	%dil
	jmp	.L587
	.p2align 4,,10
	.p2align 3
.L577:
	movq	24(%r13), %r12
	testq	%r12, %r12
	jne	.L579
	jmp	.L626
	.p2align 4,,10
	.p2align 3
.L627:
	movq	16(%r12), %rax
	movl	$1, %esi
.L582:
	testq	%rax, %rax
	je	.L580
	movq	%rax, %r12
.L579:
	movl	32(%r12), %edx
	cmpl	%edx, %r15d
	jl	.L627
	movq	24(%r12), %rax
	xorl	%esi, %esi
	jmp	.L582
.L626:
	movq	%rcx, %r12
.L578:
	cmpq	%r12, %r14
	je	.L605
.L621:
	movq	%r12, %rdi
	movq	%rcx, -56(%rbp)
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	-56(%rbp), %rcx
	movq	%r12, %rdi
	movl	32(%rax), %edx
	movq	%rax, %r12
.L593:
	cmpl	%edx, %r15d
	jle	.L573
.L594:
	movq	%rdi, %r12
.L574:
	testq	%r12, %r12
	je	.L573
.L619:
	xorl	%eax, %eax
	jmp	.L597
	.p2align 4,,10
	.p2align 3
.L622:
	movl	$48, %esi
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	movq	%rax, %rbx
	jmp	.L562
	.p2align 4,,10
	.p2align 3
.L598:
	movq	%r14, %r15
.L566:
	cmpq	%r15, 32(%r13)
	je	.L600
	movq	%r15, %rdi
	movq	%rcx, -56(%rbp)
	movq	%r15, %r12
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movl	32(%rbx), %edi
	movq	-56(%rbp), %rcx
	movl	32(%rax), %edx
	movq	%rax, %r15
	jmp	.L571
	.p2align 4,,10
	.p2align 3
.L580:
	movq	%r12, %rdi
	testb	%sil, %sil
	je	.L593
	jmp	.L578
	.p2align 4,,10
	.p2align 3
.L585:
	movq	24(%r13), %r12
	testq	%r12, %r12
	jne	.L589
	jmp	.L628
	.p2align 4,,10
	.p2align 3
.L629:
	movq	16(%r12), %rax
	movl	$1, %esi
.L592:
	testq	%rax, %rax
	je	.L590
	movq	%rax, %r12
.L589:
	movl	32(%r12), %edx
	cmpl	%edx, %r15d
	jl	.L629
	movq	24(%r12), %rax
	xorl	%esi, %esi
	jmp	.L592
	.p2align 4,,10
	.p2align 3
.L590:
	movq	%r12, %rdi
	testb	%sil, %sil
	je	.L593
.L588:
	cmpq	%r12, 32(%r13)
	jne	.L621
	movq	%r12, %rdi
	jmp	.L594
	.p2align 4,,10
	.p2align 3
.L600:
	movq	%r15, %r12
	jmp	.L574
	.p2align 4,,10
	.p2align 3
.L605:
	movq	%r14, %rdi
	jmp	.L594
.L628:
	movq	%rcx, %r12
	jmp	.L588
	.cfi_endproc
.LFE27038:
	.size	_ZNSt8_Rb_treeIN2v88internal8compiler8VariableESt4pairIKS3_PNS2_4NodeEESt10_Select1stIS8_ESt4lessIS3_ENS1_13ZoneAllocatorIS8_EEE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS5_EESK_IJEEEEESt17_Rb_tree_iteratorIS8_ESt23_Rb_tree_const_iteratorIS8_EDpOT_, .-_ZNSt8_Rb_treeIN2v88internal8compiler8VariableESt4pairIKS3_PNS2_4NodeEESt10_Select1stIS8_ESt4lessIS3_ENS1_13ZoneAllocatorIS8_EEE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS5_EESK_IJEEEEESt17_Rb_tree_iteratorIS8_ESt23_Rb_tree_const_iteratorIS8_EDpOT_
	.section	.text._ZNKSt10_HashtableIjSt4pairIKjPN2v88internal8compiler13VirtualObjectEENS3_13ZoneAllocatorIS7_EENSt8__detail10_Select1stESt8equal_toIjENS2_4base4hashIjEENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS1_,"axG",@progbits,_ZNKSt10_HashtableIjSt4pairIKjPN2v88internal8compiler13VirtualObjectEENS3_13ZoneAllocatorIS7_EENSt8__detail10_Select1stESt8equal_toIjENS2_4base4hashIjEENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZNKSt10_HashtableIjSt4pairIKjPN2v88internal8compiler13VirtualObjectEENS3_13ZoneAllocatorIS7_EENSt8__detail10_Select1stESt8equal_toIjENS2_4base4hashIjEENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS1_
	.type	_ZNKSt10_HashtableIjSt4pairIKjPN2v88internal8compiler13VirtualObjectEENS3_13ZoneAllocatorIS7_EENSt8__detail10_Select1stESt8equal_toIjENS2_4base4hashIjEENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS1_, @function
_ZNKSt10_HashtableIjSt4pairIKjPN2v88internal8compiler13VirtualObjectEENS3_13ZoneAllocatorIS7_EENSt8__detail10_Select1stESt8equal_toIjENS2_4base4hashIjEENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS1_:
.LFB27112:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	(%rsi), %edi
	movq	%rsi, %rbx
	call	_ZN2v84base10hash_valueEj@PLT
	movq	16(%r12), %r8
	xorl	%edx, %edx
	movq	%rax, %rdi
	divq	%r8
	movq	8(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L631
	movq	(%rax), %rcx
	movq	%rdx, %r9
	movq	24(%rcx), %rsi
	jmp	.L634
	.p2align 4,,10
	.p2align 3
.L632:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L631
	movq	24(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%r8
	cmpq	%rdx, %r9
	jne	.L631
.L634:
	cmpq	%rsi, %rdi
	jne	.L632
	movl	8(%rcx), %eax
	cmpl	%eax, (%rbx)
	jne	.L632
	popq	%rbx
	movq	%rcx, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L631:
	.cfi_restore_state
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE27112:
	.size	_ZNKSt10_HashtableIjSt4pairIKjPN2v88internal8compiler13VirtualObjectEENS3_13ZoneAllocatorIS7_EENSt8__detail10_Select1stESt8equal_toIjENS2_4base4hashIjEENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS1_, .-_ZNKSt10_HashtableIjSt4pairIKjPN2v88internal8compiler13VirtualObjectEENS3_13ZoneAllocatorIS7_EENSt8__detail10_Select1stESt8equal_toIjENS2_4base4hashIjEENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS1_
	.section	.rodata._ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm.str1.1,"aMS",@progbits,1
.LC10:
	.string	"vector::_M_default_append"
	.section	.text._ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm,"axG",@progbits,_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm
	.type	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm, @function
_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm:
.LFB27115:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L673
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movl	$268435455, %esi
	subq	$24, %rsp
	movq	16(%rdi), %rcx
	movq	24(%r13), %rax
	movq	%rcx, %rdx
	subq	8(%rdi), %rdx
	subq	%rcx, %rax
	movq	%rsi, %rdi
	movq	%rdx, %r14
	sarq	$3, %rax
	sarq	$3, %r14
	subq	%r14, %rdi
	cmpq	%rbx, %rax
	jb	.L647
	salq	$3, %rbx
	movq	%rcx, %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	call	memset@PLT
	movq	%rax, %rcx
	addq	%rbx, %rcx
	movq	%rcx, 16(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L673:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L647:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	cmpq	%rbx, %rdi
	jb	.L676
	cmpq	%r14, %rbx
	movq	%r14, %r15
	movq	0(%r13), %rdi
	cmovnb	%rbx, %r15
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	addq	%r14, %r15
	cmpq	$268435455, %r15
	cmova	%rsi, %r15
	subq	%r12, %rax
	salq	$3, %r15
	movq	%r15, %rsi
	cmpq	%r15, %rax
	jb	.L677
	addq	%r12, %rsi
	movq	%rsi, 16(%rdi)
.L651:
	leaq	0(,%rbx,8), %r8
	leaq	(%r12,%rdx), %rdi
	xorl	%esi, %esi
	movq	%r8, %rdx
	call	memset@PLT
	movq	16(%r13), %rax
	movq	8(%r13), %rdx
	cmpq	%rdx, %rax
	je	.L656
	subq	$8, %rax
	leaq	15(%rdx), %rcx
	subq	%rdx, %rax
	subq	%r12, %rcx
	movq	%rax, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rcx
	jbe	.L653
	movabsq	$2305843009213693948, %rcx
	testq	%rcx, %rsi
	je	.L653
	addq	$1, %rsi
	movq	%rdx, %rdi
	movq	%r12, %rax
	movq	%rsi, %rcx
	subq	%r12, %rdi
	shrq	%rcx
	salq	$4, %rcx
	addq	%r12, %rcx
	.p2align 4,,10
	.p2align 3
.L654:
	movdqu	(%rax,%rdi), %xmm0
	addq	$16, %rax
	movups	%xmm0, -16(%rax)
	cmpq	%rax, %rcx
	jne	.L654
	movq	%rsi, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rcx
	addq	%rcx, %rdx
	addq	%r12, %rcx
	cmpq	%rax, %rsi
	je	.L656
	movq	(%rdx), %rax
	movq	%rax, (%rcx)
.L656:
	addq	%r14, %rbx
	movq	%r12, 8(%r13)
	leaq	(%r12,%rbx,8), %rax
	addq	%r15, %r12
	movq	%rax, 16(%r13)
	movq	%r12, 24(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L653:
	.cfi_restore_state
	leaq	8(%rax,%r12), %rsi
	subq	%r12, %rdx
	movq	%r12, %rax
	.p2align 4,,10
	.p2align 3
.L658:
	movq	(%rax,%rdx), %rcx
	addq	$8, %rax
	movq	%rcx, -8(%rax)
	cmpq	%rax, %rsi
	jne	.L658
	jmp	.L656
.L677:
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %r12
	jmp	.L651
.L676:
	leaq	.LC10(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE27115:
	.size	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm, .-_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm
	.section	.text._ZN2v88internal8compiler20EscapeAnalysisResult16GetReplacementOfEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20EscapeAnalysisResult16GetReplacementOfEPNS1_4NodeE
	.type	_ZN2v88internal8compiler20EscapeAnalysisResult16GetReplacementOfEPNS1_4NodeE, @function
_ZN2v88internal8compiler20EscapeAnalysisResult16GetReplacementOfEPNS1_4NodeE:
.LFB23005:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %rbx
	movl	20(%rsi), %esi
	movq	88(%rbx), %rcx
	movq	80(%rbx), %rdx
	andl	$16777215, %esi
	movq	%rcx, %rax
	movl	%esi, %r12d
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rax, %r12
	jnb	.L682
.L679:
	movq	(%rdx,%r12,8), %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L682:
	.cfi_restore_state
	addl	$1, %esi
	cmpq	%rsi, %rax
	jb	.L683
	jbe	.L679
	leaq	(%rdx,%rsi,8), %rax
	cmpq	%rax, %rcx
	je	.L679
	movq	%rax, 88(%rbx)
	jmp	.L679
	.p2align 4,,10
	.p2align 3
.L683:
	subq	%rax, %rsi
	leaq	72(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm
	movq	80(%rbx), %rdx
	popq	%rbx
	movq	(%rdx,%r12,8), %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23005:
	.size	_ZN2v88internal8compiler20EscapeAnalysisResult16GetReplacementOfEPNS1_4NodeE, .-_ZN2v88internal8compiler20EscapeAnalysisResult16GetReplacementOfEPNS1_4NodeE
	.section	.text._ZNSt5dequeIN2v88internal8compiler18EffectGraphReducer9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_,"axG",@progbits,_ZNSt5dequeIN2v88internal8compiler18EffectGraphReducer9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt5dequeIN2v88internal8compiler18EffectGraphReducer9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_
	.type	_ZNSt5dequeIN2v88internal8compiler18EffectGraphReducer9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_, @function
_ZNSt5dequeIN2v88internal8compiler18EffectGraphReducer9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_:
.LFB27157:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	80(%rdi), %rcx
	movq	64(%rdi), %rax
	leaq	-16(%rcx), %rdx
	cmpq	%rdx, %rax
	je	.L685
	movq	(%rsi), %rcx
	movl	8(%rsi), %edx
	movq	%rcx, (%rax)
	movl	%edx, 8(%rax)
	addq	$16, 64(%rdi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L685:
	.cfi_restore_state
	movq	88(%rdi), %r13
	movq	56(%rdi), %rsi
	subq	72(%rdi), %rax
	movq	%r13, %r14
	sarq	$4, %rax
	subq	%rsi, %r14
	movq	%r14, %rcx
	sarq	$3, %rcx
	leaq	-1(%rcx), %rdx
	salq	$5, %rdx
	addq	%rax, %rdx
	movq	48(%rdi), %rax
	subq	32(%rdi), %rax
	sarq	$4, %rax
	addq	%rdx, %rax
	cmpq	$134217727, %rax
	je	.L704
	movq	24(%rdi), %rdx
	movq	16(%rdi), %rdi
	movq	%r13, %rax
	subq	%rdi, %rax
	movq	%rdx, %r8
	sarq	$3, %rax
	subq	%rax, %r8
	cmpq	$1, %r8
	jbe	.L705
.L688:
	movq	8(%rbx), %rax
	testq	%rax, %rax
	je	.L696
	cmpq	$31, 8(%rax)
	ja	.L706
.L696:
	movq	(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L707
	leaq	512(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L697:
	movq	%rax, 8(%r13)
	movl	8(%r12), %edx
	movq	(%r12), %rcx
	movq	64(%rbx), %rax
	movl	%edx, 8(%rax)
	movq	%rcx, (%rax)
	movq	88(%rbx), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 88(%rbx)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 72(%rbx)
	movq	%rdx, 80(%rbx)
	movq	%rax, 64(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L705:
	.cfi_restore_state
	leaq	2(%rcx), %r15
	leaq	(%r15,%r15), %rax
	cmpq	%rax, %rdx
	ja	.L708
	testq	%rdx, %rdx
	movl	$1, %eax
	movq	(%rbx), %rdi
	cmovne	%rdx, %rax
	movq	16(%rdi), %r13
	leaq	2(%rdx,%rax), %rcx
	movq	24(%rdi), %rax
	leaq	0(,%rcx,8), %rsi
	subq	%r13, %rax
	cmpq	%rax, %rsi
	ja	.L709
	addq	%r13, %rsi
	movq	%rsi, 16(%rdi)
.L693:
	movq	%rcx, %rax
	movq	56(%rbx), %rsi
	subq	%r15, %rax
	shrq	%rax
	leaq	0(%r13,%rax,8), %r15
	movq	88(%rbx), %rax
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L694
	subq	%rsi, %rdx
	movq	%r15, %rdi
	movq	%rcx, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %rcx
.L694:
	movq	24(%rbx), %rax
	leaq	0(,%rax,8), %rdx
	cmpq	$15, %rdx
	jbe	.L695
	movq	16(%rbx), %rdx
	movq	%rax, 8(%rdx)
	movq	$0, (%rdx)
.L695:
	movq	%r13, 16(%rbx)
	movq	%rcx, 24(%rbx)
.L691:
	movq	%r15, 56(%rbx)
	movq	(%r15), %rax
	leaq	(%r15,%r14), %r13
	movq	(%r15), %xmm0
	movq	%r13, 88(%rbx)
	addq	$512, %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 40(%rbx)
	movq	0(%r13), %rax
	movq	%rax, 72(%rbx)
	addq	$512, %rax
	movq	%rax, 80(%rbx)
	jmp	.L688
	.p2align 4,,10
	.p2align 3
.L708:
	subq	%r15, %rdx
	addq	$8, %r13
	shrq	%rdx
	leaq	(%rdi,%rdx,8), %r15
	movq	%r13, %rdx
	subq	%rsi, %rdx
	cmpq	%r15, %rsi
	jbe	.L690
	cmpq	%r13, %rsi
	je	.L691
	movq	%r15, %rdi
	call	memmove@PLT
	jmp	.L691
	.p2align 4,,10
	.p2align 3
.L706:
	movq	(%rax), %rdx
	movq	%rdx, 8(%rbx)
	jmp	.L697
	.p2align 4,,10
	.p2align 3
.L690:
	cmpq	%r13, %rsi
	je	.L691
	leaq	8(%r14), %rdi
	subq	%rdx, %rdi
	addq	%r15, %rdi
	call	memmove@PLT
	jmp	.L691
	.p2align 4,,10
	.p2align 3
.L707:
	movl	$512, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L697
.L709:
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	movq	%rax, %r13
	jmp	.L693
.L704:
	leaq	.LC9(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE27157:
	.size	_ZNSt5dequeIN2v88internal8compiler18EffectGraphReducer9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_, .-_ZNSt5dequeIN2v88internal8compiler18EffectGraphReducer9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_
	.section	.text._ZN2v88internal8compiler18EffectGraphReducer10ReduceFromEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18EffectGraphReducer10ReduceFromEPNS1_4NodeE
	.type	_ZN2v88internal8compiler18EffectGraphReducer10ReduceFromEPNS1_4NodeE, @function
_ZN2v88internal8compiler18EffectGraphReducer10ReduceFromEPNS1_4NodeE:
.LFB22975:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-80(%rbp), %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	120(%rdi), %rax
	movq	%rsi, -80(%rbp)
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -104(%rbp)
	movl	$0, -72(%rbp)
	call	_ZNSt5dequeIN2v88internal8compiler18EffectGraphReducer9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_
	leaq	-90(%rbp), %rax
	movq	%rax, -112(%rbp)
	leaq	-88(%rbp), %rax
	movq	%rax, -120(%rbp)
	leaq	24(%r15), %rax
	movq	%rax, -128(%rbp)
	movq	184(%r15), %rax
	cmpq	%rax, 152(%r15)
	jne	.L711
	jmp	.L710
	.p2align 4,,10
	.p2align 3
.L768:
	cmpl	%edx, %eax
	jge	.L716
	movslq	%eax, %rdx
	leaq	32(%rbx,%rdx,8), %rdx
.L718:
	movq	(%rdx), %rdi
	addl	$1, %eax
	movl	%eax, -8(%rsi)
	movl	16(%r15), %edx
	movl	16(%rdi), %eax
	cmpl	%eax, %edx
	ja	.L719
	subl	%edx, %eax
	cmpb	$1, %al
	jbe	.L719
.L720:
	movq	152(%r15), %rax
	cmpq	%rax, 184(%r15)
	je	.L710
.L711:
	movq	248(%r15), %rdi
	call	_ZN2v88internal11TickCounter6DoTickEv@PLT
	movq	184(%r15), %rdi
	movq	192(%r15), %r8
	movq	208(%r15), %r9
	movq	%rdi, %rsi
	cmpq	%r8, %rdi
	je	.L767
.L713:
	movq	-16(%rsi), %rbx
	movl	-8(%rsi), %eax
	movzbl	23(%rbx), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L768
	movq	32(%rbx), %rdx
	cmpl	8(%rdx), %eax
	jge	.L716
	movslq	%eax, %rdi
	leaq	16(%rdx,%rdi,8), %rdx
	jmp	.L718
	.p2align 4,,10
	.p2align 3
.L767:
	movq	-8(%r9), %rax
	leaq	512(%rax), %rsi
	jmp	.L713
	.p2align 4,,10
	.p2align 3
.L716:
	cmpq	%r8, %rdi
	je	.L721
	subq	$16, %rdi
	movq	%rdi, 184(%r15)
.L722:
	xorl	%eax, %eax
	cmpq	$0, 232(%r15)
	movq	%rbx, -88(%rbp)
	movw	%ax, -90(%rbp)
	movq	-112(%rbp), %rax
	movq	%rax, -80(%rbp)
	je	.L769
	leaq	216(%r15), %rdi
	movq	-120(%rbp), %rsi
	movq	%r12, %rdx
	call	*240(%r15)
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L726
	movq	(%rdi), %r14
	.p2align 4,,10
	.p2align 3
.L735:
	movl	16(%rdi), %esi
	movl	%esi, %eax
	shrl	%eax
	leaq	3(%rax,%rax,2), %r8
	salq	$3, %rax
	andl	$1, %esi
	leaq	(%rdi,%r8,8), %r13
	leaq	32(%r13,%rax), %rsi
	jne	.L728
	leaq	16(%r13,%rax), %rsi
	movq	0(%r13), %r13
.L728:
	call	_ZN2v88internal8compiler14NodeProperties12IsEffectEdgeENS1_4EdgeE@PLT
	testb	%al, %al
	je	.L729
	cmpb	$0, -89(%rbp)
	jne	.L766
.L730:
	testq	%r14, %r14
	je	.L726
.L770:
	movq	%r14, %rdi
	movq	(%r14), %r14
	jmp	.L735
	.p2align 4,,10
	.p2align 3
.L729:
	cmpb	$0, -90(%rbp)
	je	.L730
.L766:
	movq	%r13, -80(%rbp)
	movl	16(%r15), %esi
	movl	16(%r13), %eax
	cmpl	%esi, %eax
	jb	.L730
	subl	%esi, %eax
	cmpb	$3, %al
	jne	.L730
	addl	$1, %esi
	movl	%esi, 16(%r13)
	movq	104(%r15), %rax
	movq	88(%r15), %rsi
	subq	$8, %rax
	cmpq	%rax, %rsi
	je	.L734
	movq	%r13, (%rsi)
	addq	$8, 88(%r15)
	testq	%r14, %r14
	jne	.L770
	.p2align 4,,10
	.p2align 3
.L726:
	movl	16(%r15), %eax
	addl	$3, %eax
	movl	%eax, 16(%rbx)
	movq	88(%r15), %rax
	cmpq	%rax, 56(%r15)
	je	.L720
	movq	96(%r15), %rdx
	jmp	.L737
	.p2align 4,,10
	.p2align 3
.L738:
	movq	-8(%rax), %rdi
	movl	16(%r15), %esi
	movl	16(%rdi), %eax
	cmpl	%esi, %eax
	jb	.L739
	subl	%esi, %eax
	cmpb	$1, %al
	je	.L771
.L739:
	movq	88(%r15), %rax
	cmpq	%rdx, %rax
	je	.L740
.L772:
	subq	$8, %rax
	movq	%rax, 88(%r15)
.L741:
	cmpq	%rax, 56(%r15)
	je	.L720
.L737:
	cmpq	%rdx, %rax
	jne	.L738
	movq	112(%r15), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L738
	.p2align 4,,10
	.p2align 3
.L771:
	addl	$2, %esi
	movl	%esi, 16(%rdi)
	movq	%r12, %rsi
	movq	%rdi, -80(%rbp)
	movq	-104(%rbp), %rdi
	movl	$0, -72(%rbp)
	call	_ZNSt5dequeIN2v88internal8compiler18EffectGraphReducer9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_
	movq	96(%r15), %rdx
	movq	88(%r15), %rax
	cmpq	%rdx, %rax
	jne	.L772
.L740:
	movq	32(%r15), %rax
	testq	%rax, %rax
	je	.L742
	cmpq	$64, 8(%rax)
	ja	.L743
.L742:
	movq	$64, 8(%rdx)
	movq	32(%r15), %rax
	movq	%rax, (%rdx)
	movq	%rdx, 32(%r15)
.L743:
	movq	112(%r15), %rax
	leaq	-8(%rax), %rdx
	movq	%rdx, 112(%r15)
	movq	-8(%rax), %rdx
	leaq	512(%rdx), %rax
	movq	%rdx, 96(%r15)
	movq	%rax, 104(%r15)
	leaq	504(%rdx), %rax
	movq	%rax, 88(%r15)
	jmp	.L741
	.p2align 4,,10
	.p2align 3
.L719:
	addl	$2, %edx
	movq	%r12, %rsi
	movl	%edx, 16(%rdi)
	movq	%rdi, -80(%rbp)
	movq	-104(%rbp), %rdi
	movl	$0, -72(%rbp)
	call	_ZNSt5dequeIN2v88internal8compiler18EffectGraphReducer9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_
	movq	152(%r15), %rax
	cmpq	%rax, 184(%r15)
	jne	.L711
	.p2align 4,,10
	.p2align 3
.L710:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L773
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L721:
	.cfi_restore_state
	movq	128(%r15), %rax
	testq	%rax, %rax
	je	.L723
	cmpq	$32, 8(%rax)
	ja	.L724
.L723:
	movq	$32, 8(%r8)
	movq	128(%r15), %rax
	movq	%rax, (%r8)
	movq	208(%r15), %r9
	movq	%r8, 128(%r15)
.L724:
	leaq	-8(%r9), %rax
	movq	%rax, 208(%r15)
	movq	-8(%r9), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 192(%r15)
	addq	$496, %rax
	movq	%rdx, 200(%r15)
	movq	%rax, 184(%r15)
	jmp	.L722
	.p2align 4,,10
	.p2align 3
.L734:
	movq	-128(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	jmp	.L730
.L769:
	call	_ZSt25__throw_bad_function_callv@PLT
.L773:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22975:
	.size	_ZN2v88internal8compiler18EffectGraphReducer10ReduceFromEPNS1_4NodeE, .-_ZN2v88internal8compiler18EffectGraphReducer10ReduceFromEPNS1_4NodeE
	.section	.text._ZNSt8_Rb_treeIN2v88internal8compiler8VariableESt4pairIKS3_PNS2_4NodeEESt10_Select1stIS8_ESt4lessIS3_ENS1_13ZoneAllocatorIS8_EEE8_M_eraseEPSt13_Rb_tree_nodeIS8_E,"axG",@progbits,_ZNSt8_Rb_treeIN2v88internal8compiler8VariableESt4pairIKS3_PNS2_4NodeEESt10_Select1stIS8_ESt4lessIS3_ENS1_13ZoneAllocatorIS8_EEE8_M_eraseEPSt13_Rb_tree_nodeIS8_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN2v88internal8compiler8VariableESt4pairIKS3_PNS2_4NodeEESt10_Select1stIS8_ESt4lessIS3_ENS1_13ZoneAllocatorIS8_EEE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	.type	_ZNSt8_Rb_treeIN2v88internal8compiler8VariableESt4pairIKS3_PNS2_4NodeEESt10_Select1stIS8_ESt4lessIS3_ENS1_13ZoneAllocatorIS8_EEE8_M_eraseEPSt13_Rb_tree_nodeIS8_E, @function
_ZNSt8_Rb_treeIN2v88internal8compiler8VariableESt4pairIKS3_PNS2_4NodeEESt10_Select1stIS8_ESt4lessIS3_ENS1_13ZoneAllocatorIS8_EEE8_M_eraseEPSt13_Rb_tree_nodeIS8_E:
.LFB27384:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L782
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L776:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal8compiler8VariableESt4pairIKS3_PNS2_4NodeEESt10_Select1stIS8_ESt4lessIS3_ENS1_13ZoneAllocatorIS8_EEE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L776
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L782:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE27384:
	.size	_ZNSt8_Rb_treeIN2v88internal8compiler8VariableESt4pairIKS3_PNS2_4NodeEESt10_Select1stIS8_ESt4lessIS3_ENS1_13ZoneAllocatorIS8_EEE8_M_eraseEPSt13_Rb_tree_nodeIS8_E, .-_ZNSt8_Rb_treeIN2v88internal8compiler8VariableESt4pairIKS3_PNS2_4NodeEESt10_Select1stIS8_ESt4lessIS3_ENS1_13ZoneAllocatorIS8_EEE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	.section	.text._ZNSt8_Rb_treeIN2v88internal8compiler8VariableESt4pairIKS3_PNS2_4NodeEESt10_Select1stIS8_ESt4lessIS3_ENS1_13ZoneAllocatorIS8_EEE7_M_copyINSF_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS8_EPKSJ_PSt18_Rb_tree_node_baseRT_,"axG",@progbits,_ZNSt8_Rb_treeIN2v88internal8compiler8VariableESt4pairIKS3_PNS2_4NodeEESt10_Select1stIS8_ESt4lessIS3_ENS1_13ZoneAllocatorIS8_EEE7_M_copyINSF_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS8_EPKSJ_PSt18_Rb_tree_node_baseRT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN2v88internal8compiler8VariableESt4pairIKS3_PNS2_4NodeEESt10_Select1stIS8_ESt4lessIS3_ENS1_13ZoneAllocatorIS8_EEE7_M_copyINSF_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS8_EPKSJ_PSt18_Rb_tree_node_baseRT_
	.type	_ZNSt8_Rb_treeIN2v88internal8compiler8VariableESt4pairIKS3_PNS2_4NodeEESt10_Select1stIS8_ESt4lessIS3_ENS1_13ZoneAllocatorIS8_EEE7_M_copyINSF_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS8_EPKSJ_PSt18_Rb_tree_node_baseRT_, @function
_ZNSt8_Rb_treeIN2v88internal8compiler8VariableESt4pairIKS3_PNS2_4NodeEESt10_Select1stIS8_ESt4lessIS3_ENS1_13ZoneAllocatorIS8_EEE7_M_copyINSF_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS8_EPKSJ_PSt18_Rb_tree_node_baseRT_:
.LFB27390:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	8(%rcx), %r15
	testq	%r15, %r15
	je	.L786
	movq	8(%r15), %rax
	movq	%rax, 8(%rcx)
	testq	%rax, %rax
	je	.L787
	cmpq	24(%rax), %r15
	je	.L848
	movq	$0, 16(%rax)
.L793:
	movq	40(%rbx), %rax
	movl	32(%rbx), %ecx
	movq	%rax, 40(%r15)
	movl	%ecx, 32(%r15)
	movl	(%rbx), %eax
	movq	$0, 16(%r15)
	movl	%eax, (%r15)
	movq	$0, 24(%r15)
	movq	%rdx, 8(%r15)
	movq	24(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L794
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal8compiler8VariableESt4pairIKS3_PNS2_4NodeEESt10_Select1stIS8_ESt4lessIS3_ENS1_13ZoneAllocatorIS8_EEE7_M_copyINSF_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS8_EPKSJ_PSt18_Rb_tree_node_baseRT_
	movq	%rax, 24(%r15)
.L794:
	movq	16(%rbx), %r12
	testq	%r12, %r12
	je	.L785
	movq	8(%r14), %rbx
	movq	%r15, %rdx
	testq	%rbx, %rbx
	je	.L796
.L850:
	movq	8(%rbx), %rax
	movq	%rax, 8(%r14)
	testq	%rax, %rax
	je	.L797
	cmpq	24(%rax), %rbx
	je	.L849
	movq	$0, 16(%rax)
.L803:
	movq	40(%r12), %rax
	movl	32(%r12), %ecx
	movq	%rax, 40(%rbx)
	movl	%ecx, 32(%rbx)
	movl	(%r12), %eax
	movq	$0, 16(%rbx)
	movl	%eax, (%rbx)
	movq	$0, 24(%rbx)
	movq	%rbx, 16(%rdx)
	movq	%rdx, 8(%rbx)
	movq	24(%r12), %rsi
	testq	%rsi, %rsi
	je	.L804
	movq	%r14, %rcx
	movq	%rbx, %rdx
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal8compiler8VariableESt4pairIKS3_PNS2_4NodeEESt10_Select1stIS8_ESt4lessIS3_ENS1_13ZoneAllocatorIS8_EEE7_M_copyINSF_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS8_EPKSJ_PSt18_Rb_tree_node_baseRT_
	movq	%rax, 24(%rbx)
	movq	16(%r12), %r12
	testq	%r12, %r12
	je	.L785
.L805:
	movq	%rbx, %rdx
	movq	8(%r14), %rbx
	testq	%rbx, %rbx
	jne	.L850
.L796:
	movq	16(%r14), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	$47, %rax
	jbe	.L851
	leaq	48(%rbx), %rax
	movq	%rax, 16(%rdi)
	jmp	.L803
	.p2align 4,,10
	.p2align 3
.L804:
	movq	16(%r12), %r12
	testq	%r12, %r12
	jne	.L805
.L785:
	addq	$24, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L797:
	.cfi_restore_state
	movq	$0, (%r14)
	jmp	.L803
	.p2align 4,,10
	.p2align 3
.L849:
	movq	$0, 24(%rax)
	movq	8(%r14), %rax
	movq	16(%rax), %rcx
	testq	%rcx, %rcx
	je	.L803
	movq	%rcx, 8(%r14)
	movq	24(%rcx), %rax
	testq	%rax, %rax
	je	.L800
	.p2align 4,,10
	.p2align 3
.L801:
	movq	%rax, 8(%r14)
	movq	%rax, %rcx
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L801
.L800:
	movq	16(%rcx), %rax
	testq	%rax, %rax
	je	.L803
	movq	%rax, 8(%r14)
	jmp	.L803
	.p2align 4,,10
	.p2align 3
.L786:
	movq	16(%rcx), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %r15
	movq	24(%rdi), %rax
	subq	%r15, %rax
	cmpq	$47, %rax
	jbe	.L852
	leaq	48(%r15), %rax
	movq	%rax, 16(%rdi)
	jmp	.L793
	.p2align 4,,10
	.p2align 3
.L787:
	movq	$0, (%rcx)
	jmp	.L793
	.p2align 4,,10
	.p2align 3
.L848:
	movq	$0, 24(%rax)
	movq	8(%rcx), %rax
	movq	16(%rax), %rcx
	testq	%rcx, %rcx
	je	.L793
	movq	%rcx, 8(%r14)
	movq	24(%rcx), %rax
	testq	%rax, %rax
	je	.L790
	.p2align 4,,10
	.p2align 3
.L791:
	movq	%rax, 8(%r14)
	movq	%rax, %rcx
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L791
.L790:
	movq	16(%rcx), %rax
	testq	%rax, %rax
	je	.L793
	movq	%rax, 8(%r14)
	jmp	.L793
	.p2align 4,,10
	.p2align 3
.L851:
	movl	$48, %esi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %rbx
	jmp	.L803
.L852:
	movl	$48, %esi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %r15
	jmp	.L793
	.cfi_endproc
.LFE27390:
	.size	_ZNSt8_Rb_treeIN2v88internal8compiler8VariableESt4pairIKS3_PNS2_4NodeEESt10_Select1stIS8_ESt4lessIS3_ENS1_13ZoneAllocatorIS8_EEE7_M_copyINSF_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS8_EPKSJ_PSt18_Rb_tree_node_baseRT_, .-_ZNSt8_Rb_treeIN2v88internal8compiler8VariableESt4pairIKS3_PNS2_4NodeEESt10_Select1stIS8_ESt4lessIS3_ENS1_13ZoneAllocatorIS8_EEE7_M_copyINSF_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS8_EPKSJ_PSt18_Rb_tree_node_baseRT_
	.section	.text._ZN2v88internal8compiler13PersistentMapINS1_8VariableEPNS1_4NodeENS_4base4hashIS3_EEE3SetES3_S5_,"axG",@progbits,_ZN2v88internal8compiler13PersistentMapINS1_8VariableEPNS1_4NodeENS_4base4hashIS3_EEE3SetES3_S5_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler13PersistentMapINS1_8VariableEPNS1_4NodeENS_4base4hashIS3_EEE3SetES3_S5_
	.type	_ZN2v88internal8compiler13PersistentMapINS1_8VariableEPNS1_4NodeENS_4base4hashIS3_EEE3SetES3_S5_, @function
_ZN2v88internal8compiler13PersistentMapINS1_8VariableEPNS1_4NodeENS_4base4hashIS3_EEE3SetES3_S5_:
.LFB25024:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$408, %rsp
	movq	%rdi, -384(%rbp)
	movl	%esi, %edi
	movl	%esi, -372(%rbp)
	movq	%rdx, -392(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v84base10hash_valueEj@PLT
	movq	(%rbx), %r14
	movq	%rax, -408(%rbp)
	testq	%r14, %r14
	je	.L904
	movl	20(%r14), %edx
	xorl	%r13d, %r13d
	movl	%eax, %edi
	movsbl	16(%r14), %r12d
	movl	$-2147483648, %esi
	movl	%r13d, %ecx
	cmpl	%edx, %edi
	je	.L855
	.p2align 4,,10
	.p2align 3
.L933:
	movl	%esi, %eax
	xorl	%edi, %edx
	shrl	%cl, %eax
	testl	%edx, %eax
	jne	.L856
	addl	$1, %ecx
	leaq	-320(%rbp), %r8
	movslq	%ecx, %rax
.L862:
	leal	-1(%rax), %ecx
	cmpl	%r12d, %ecx
	jge	.L932
	movq	24(%r14,%rax,8), %rcx
	movq	%rcx, -8(%r8,%rax,8)
.L930:
	movl	%eax, %ecx
	movl	%esi, %r9d
	addq	$1, %rax
	shrl	%cl, %r9d
	testl	%edx, %r9d
	je	.L862
.L856:
	movslq	%ecx, %rax
	leal	1(%rcx), %r15d
	movq	%r14, -320(%rbp,%rax,8)
	cmpl	%ecx, %r12d
	jle	.L931
	movq	32(%r14,%rax,8), %r14
	testq	%r14, %r14
	je	.L931
	movl	20(%r14), %edx
	movsbl	16(%r14), %r12d
	movl	%r15d, %ecx
	cmpl	%edx, %edi
	jne	.L933
.L855:
	cmpl	%r12d, %ecx
	jge	.L906
	movslq	%ecx, %rax
	leal	-1(%r12), %edx
	salq	$3, %rax
	subl	%ecx, %edx
	leaq	-320(%rbp,%rax), %rdi
	leaq	8(,%rdx,8), %rdx
	leaq	32(%r14,%rax), %rsi
	call	memcpy@PLT
.L864:
	movq	24(%r14), %rax
	movq	%rax, -400(%rbp)
	testq	%rax, %rax
	je	.L934
	leaq	16(%rax), %rsi
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L865
	movl	-372(%rbp), %edx
	movq	%rsi, %rcx
	jmp	.L866
	.p2align 4,,10
	.p2align 3
.L935:
	movq	%rax, %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L867
.L866:
	cmpl	%edx, 32(%rax)
	jge	.L935
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L866
.L867:
	cmpq	%rcx, %rsi
	je	.L865
	cmpl	32(%rcx), %edx
	jge	.L870
.L865:
	movq	-384(%rbp), %rax
	movq	8(%rax), %rax
	.p2align 4,,10
	.p2align 3
.L871:
	leal	-1(%r12), %r13d
	cmpq	%rax, -392(%rbp)
	je	.L853
.L902:
	movq	-384(%rbp), %rax
	movq	16(%rax), %rdi
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	$55, %rax
	jbe	.L936
	leaq	56(%rbx), %rax
	movq	%rax, 16(%rdi)
.L878:
	movq	-384(%rbp), %rax
	leaq	16(%rbx), %rdx
	movq	%rbx, -400(%rbp)
	movq	%rdx, -416(%rbp)
	movq	16(%rax), %rax
	movl	$0, 16(%rbx)
	movq	$0, 24(%rbx)
	movq	%rax, (%rbx)
	movq	%rdx, 32(%rbx)
	movq	%rdx, 40(%rbx)
	movq	$0, 48(%rbx)
	movq	24(%r14), %r15
	testq	%r15, %r15
	je	.L879
	cmpq	%rbx, %r15
	je	.L907
	pxor	%xmm0, %xmm0
	movq	%rbx, -336(%rbp)
	movaps	%xmm0, -352(%rbp)
	movq	24(%r15), %rsi
	testq	%rsi, %rsi
	je	.L907
	leaq	-352(%rbp), %rax
	movq	%rbx, %rdi
	movq	%rax, %rcx
	movq	%rax, -424(%rbp)
	call	_ZNSt8_Rb_treeIN2v88internal8compiler8VariableESt4pairIKS3_PNS2_4NodeEESt10_Select1stIS8_ESt4lessIS3_ENS1_13ZoneAllocatorIS8_EEE7_M_copyINSF_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS8_EPKSJ_PSt18_Rb_tree_node_baseRT_
	movq	%rax, %r14
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L882:
	movq	%rdx, %rcx
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L882
	movq	%rcx, 32(%rbx)
	movq	%r14, %rdx
	.p2align 4,,10
	.p2align 3
.L883:
	movq	%rdx, %rcx
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L883
	movq	%rcx, 40(%rbx)
	movq	48(%r15), %rdx
	movq	%r14, 24(%rbx)
	movq	%rdx, 48(%rbx)
	movq	-352(%rbp), %rax
	movq	-336(%rbp), %r15
	testq	%rax, %rax
	je	.L888
	movq	%rbx, -440(%rbp)
	movq	%r14, -432(%rbp)
	movq	%rax, %r14
.L887:
	movq	24(%r14), %rbx
	testq	%rbx, %rbx
	je	.L885
.L886:
	movq	24(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal8compiler8VariableESt4pairIKS3_PNS2_4NodeEESt10_Select1stIS8_ESt4lessIS3_ENS1_13ZoneAllocatorIS8_EEE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L886
.L885:
	movq	16(%r14), %r14
	testq	%r14, %r14
	jne	.L887
	movq	-432(%rbp), %r14
	movq	-440(%rbp), %rbx
.L888:
	testq	%r14, %r14
	je	.L908
	movl	-372(%rbp), %edx
	movq	-416(%rbp), %rsi
	jmp	.L889
	.p2align 4,,10
	.p2align 3
.L937:
	movq	%r14, %rsi
	movq	16(%r14), %r14
	testq	%r14, %r14
	je	.L890
.L889:
	cmpl	%edx, 32(%r14)
	jge	.L937
	movq	24(%r14), %r14
	testq	%r14, %r14
	jne	.L889
.L890:
	cmpq	%rsi, -416(%rbp)
	je	.L880
	cmpl	32(%rsi), %edx
	jge	.L893
	jmp	.L880
	.p2align 4,,10
	.p2align 3
.L931:
	movl	%ecx, %r13d
.L854:
	movq	-384(%rbp), %rax
	movq	-392(%rbp), %rbx
	cmpq	8(%rax), %rbx
	je	.L853
	movq	$0, -400(%rbp)
.L900:
	movq	-384(%rbp), %rax
	testl	%r13d, %r13d
	movq	16(%rax), %rdi
	movl	$0, %eax
	cmovns	%r13d, %eax
	movq	16(%rdi), %r14
	movslq	%eax, %r9
	movq	24(%rdi), %rax
	leaq	40(,%r9,8), %rsi
	subq	%r14, %rax
	cmpq	%rax, %rsi
	ja	.L938
	addq	%r14, %rsi
	movq	%rsi, 16(%rdi)
.L895:
	pxor	%xmm0, %xmm0
	movq	$0, 32(%r14)
	movups	%xmm0, (%r14)
	movl	-372(%rbp), %eax
	movups	%xmm0, 16(%r14)
	movl	%eax, (%r14)
	movq	-392(%rbp), %rax
	movb	%r15b, 16(%r14)
	movq	%rax, 8(%r14)
	movl	-408(%rbp), %eax
	movl	%eax, 20(%r14)
	movq	-400(%rbp), %rax
	movq	%rax, 24(%r14)
	testl	%r15d, %r15d
	je	.L897
	leal	-1(%r15), %eax
	leaq	32(%r14), %rdi
	leaq	8(,%rax,8), %rdx
	leaq	-320(%rbp), %rsi
	call	memcpy@PLT
.L897:
	movq	-384(%rbp), %rax
	movq	%r14, (%rax)
.L853:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L939
	addq	$408, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L932:
	.cfi_restore_state
	movq	$0, -8(%r8,%rax,8)
	jmp	.L930
	.p2align 4,,10
	.p2align 3
.L907:
	leaq	-352(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, -424(%rbp)
.L880:
	movq	-424(%rbp), %rcx
	leaq	-372(%rbp), %rax
	movq	%rbx, %rdi
	leaq	-353(%rbp), %r8
	leaq	_ZStL19piecewise_construct(%rip), %rdx
	movq	%rax, -352(%rbp)
	call	_ZNSt8_Rb_treeIN2v88internal8compiler8VariableESt4pairIKS3_PNS2_4NodeEESt10_Select1stIS8_ESt4lessIS3_ENS1_13ZoneAllocatorIS8_EEE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS5_EESK_IJEEEEESt17_Rb_tree_iteratorIS8_ESt23_Rb_tree_const_iteratorIS8_EDpOT_
	movq	%rax, %rsi
.L893:
	movq	-392(%rbp), %rax
	movl	%r12d, %r15d
	movq	%rax, 40(%rsi)
	jmp	.L900
	.p2align 4,,10
	.p2align 3
.L870:
	movq	40(%rcx), %rax
	jmp	.L871
	.p2align 4,,10
	.p2align 3
.L934:
	movl	(%r14), %eax
	cmpl	%eax, -372(%rbp)
	jne	.L872
	movq	-392(%rbp), %rax
	cmpq	8(%r14), %rax
	je	.L853
	leal	-1(%r12), %r13d
	movl	%r12d, %r15d
	jmp	.L900
	.p2align 4,,10
	.p2align 3
.L872:
	movq	-384(%rbp), %rax
	movq	-392(%rbp), %rbx
	leal	-1(%r12), %r13d
	cmpq	%rbx, 8(%rax)
	jne	.L902
	jmp	.L853
	.p2align 4,,10
	.p2align 3
.L879:
	leaq	-352(%rbp), %rax
	movq	%rdx, %rsi
	leaq	-353(%rbp), %r8
	movq	%rbx, %rdi
	leaq	_ZStL19piecewise_construct(%rip), %rdx
	movq	%rax, %rcx
	movq	%r14, -352(%rbp)
	movq	%rax, -424(%rbp)
	call	_ZNSt8_Rb_treeIN2v88internal8compiler8VariableESt4pairIKS3_PNS2_4NodeEESt10_Select1stIS8_ESt4lessIS3_ENS1_13ZoneAllocatorIS8_EEE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS5_EESK_IJEEEEESt17_Rb_tree_iteratorIS8_ESt23_Rb_tree_const_iteratorIS8_EDpOT_
	movq	8(%r14), %rdx
	movq	%rdx, 40(%rax)
	movq	24(%rbx), %r14
	jmp	.L888
	.p2align 4,,10
	.p2align 3
.L938:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r14
	jmp	.L895
.L906:
	movl	%ecx, %r12d
	jmp	.L864
.L936:
	movl	$56, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rbx
	jmp	.L878
.L904:
	movl	$-1, %r13d
	xorl	%r15d, %r15d
	jmp	.L854
.L908:
	movq	-416(%rbp), %rsi
	jmp	.L880
.L939:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25024:
	.size	_ZN2v88internal8compiler13PersistentMapINS1_8VariableEPNS1_4NodeENS_4base4hashIS3_EEE3SetES3_S5_, .-_ZN2v88internal8compiler13PersistentMapINS1_8VariableEPNS1_4NodeENS_4base4hashIS3_EEE3SetES3_S5_
	.section	.text._ZN2v88internal8compiler15VariableTracker11MergeInputsEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15VariableTracker11MergeInputsEPNS1_4NodeE
	.type	_ZN2v88internal8compiler15VariableTracker11MergeInputsEPNS1_4NodeE, @function
_ZN2v88internal8compiler15VariableTracker11MergeInputsEPNS1_4NodeE:
.LFB22989:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	subq	$440, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -432(%rbp)
	movq	%rdx, %rdi
	movq	%rdx, -408(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	movl	24(%rax), %ebx
	movl	%ebx, -396(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, -376(%rbp)
	movq	(%rax), %rax
	cmpw	$1, 16(%rax)
	leaq	104(%r12), %rax
	movq	%rax, -440(%rbp)
	leal	1(%rbx), %eax
	movslq	%eax, %rcx
	movl	%eax, -444(%rbp)
	sete	-397(%rbp)
	cmpq	$268435455, %rcx
	ja	.L1169
	movq	112(%r12), %rbx
	movq	128(%r12), %rax
	subq	%rbx, %rax
	sarq	$3, %rax
	cmpq	%rax, %rcx
	ja	.L1170
.L942:
	movq	-408(%rbp), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movl	20(%rax), %ebx
	andl	$16777215, %ebx
	movl	%ebx, %edi
	call	_ZN2v84base10hash_valueEj@PLT
	movq	56(%r12), %rdi
	xorl	%edx, %edx
	movq	%rax, %r8
	divq	%rdi
	movq	48(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L953
	movq	(%rax), %rcx
	movq	40(%rcx), %rsi
	jmp	.L956
	.p2align 4,,10
	.p2align 3
.L954:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L953
	movq	40(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r9
	jne	.L953
.L956:
	cmpq	%rsi, %r8
	jne	.L954
	cmpl	8(%rcx), %ebx
	jne	.L954
	addq	$16, %rcx
.L1060:
	movq	8(%rcx), %rsi
	movq	(%rcx), %r13
	movdqu	(%rcx), %xmm0
	movq	-432(%rbp), %rbx
	movq	16(%rcx), %rax
	movq	%rsi, -392(%rbp)
	movups	%xmm0, (%rbx)
	movq	%rax, 16(%rbx)
	testq	%r13, %r13
	je	.L1171
	leaq	-328(%rbp), %rax
	movq	%rsi, -72(%rbp)
	pxor	%xmm0, %xmm0
	movq	%r13, %rdi
	leaq	-352(%rbp), %r15
	movq	%rax, %rdx
	movl	$0, -352(%rbp)
	movq	%r15, %rsi
	movq	%rax, -416(%rbp)
	movups	%xmm0, -344(%rbp)
	call	_ZN2v88internal8compiler13PersistentMapINS1_8VariableEPNS1_4NodeENS_4base4hashIS3_EEE12FindLeftmostEPKNS9_11FocusedTreeEPiPSt5arrayISC_Lm32EE
	movq	%rax, -336(%rbp)
	movq	%rax, %r13
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L960
	movq	32(%rax), %rdx
	movq	%rdx, -344(%rbp)
	testq	%rax, %rax
	jne	.L1172
	.p2align 4,,10
	.p2align 3
.L960:
	movq	8(%r13), %rax
.L961:
	cmpq	%rax, -392(%rbp)
	jne	.L958
	movl	$-2147483648, %r14d
.L972:
	movq	24(%r13), %rbx
	testq	%rbx, %rbx
	je	.L966
	movq	-344(%rbp), %rdi
	addq	$16, %rbx
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, -344(%rbp)
	cmpq	%rbx, %rax
	jne	.L965
.L966:
	movl	-352(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L963
	subl	$1, %ecx
	movl	20(%r13), %r8d
	xorl	%eax, %eax
	movl	%ecx, -352(%rbp)
	movslq	%ecx, %rcx
	jmp	.L971
	.p2align 4,,10
	.p2align 3
.L970:
	subq	$1, %rcx
	movl	$1, %eax
.L971:
	movl	%r14d, %edx
	movl	%ecx, %esi
	shrl	%cl, %edx
	testl	%r8d, %edx
	jne	.L967
	movq	24(%r15,%rcx,8), %rdi
	testq	%rdi, %rdi
	jne	.L1173
.L967:
	testl	%ecx, %ecx
	jne	.L970
	testb	%al, %al
	je	.L963
	movl	%esi, -352(%rbp)
.L963:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movups	%xmm0, -344(%rbp)
.L958:
	movl	-396(%rbp), %eax
	movl	$-2147483648, %r14d
	subl	$1, %eax
	movl	%eax, -448(%rbp)
.L973:
	testq	%r13, %r13
	je	.L940
	cmpq	$0, 24(%r13)
	je	.L1152
	movq	-344(%rbp), %rax
	movl	32(%rax), %r15d
	movq	40(%rax), %rbx
.L977:
	movq	152(%r12), %rdi
	call	_ZN2v88internal11TickCounter6DoTickEv@PLT
	movq	%rbx, -368(%rbp)
	testq	%rbx, %rbx
	jne	.L1174
.L978:
	movq	-336(%rbp), %r13
.L1051:
	testq	%r13, %r13
	je	.L940
	movq	24(%r13), %rbx
	testq	%rbx, %rbx
	je	.L1046
	movq	-344(%rbp), %rdi
	addq	$16, %rbx
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, -344(%rbp)
	cmpq	%rbx, %rax
	jne	.L973
.L1046:
	movl	-352(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L940
	subl	$1, %ecx
	movl	20(%r13), %esi
	movl	%ecx, -352(%rbp)
	movslq	%ecx, %rcx
	jmp	.L1050
	.p2align 4,,10
	.p2align 3
.L1176:
	subq	$1, %rcx
.L1050:
	movl	%r14d, %eax
	movl	%ecx, %edx
	shrl	%cl, %eax
	testl	%esi, %eax
	jne	.L1047
	movq	-328(%rbp,%rcx,8), %rdi
	leaq	-352(%rbp), %r8
	testq	%rdi, %rdi
	jne	.L1175
.L1047:
	testl	%ecx, %ecx
	jne	.L1176
.L940:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1177
	movq	-432(%rbp), %rax
	addq	$440, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1173:
	.cfi_restore_state
	addl	$1, %esi
	movq	-416(%rbp), %rdx
	movl	%esi, -352(%rbp)
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler13PersistentMapINS1_8VariableEPNS1_4NodeENS_4base4hashIS3_EEE12FindLeftmostEPKNS9_11FocusedTreeEPiPSt5arrayISC_Lm32EE
	movq	%rax, -336(%rbp)
	movq	%rax, %r13
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L1178
	movq	32(%rax), %rax
	movq	-72(%rbp), %rbx
	movq	%rax, -344(%rbp)
	cmpq	%rbx, 40(%rax)
	je	.L972
.L965:
	movq	24(%r13), %rax
	testq	%rax, %rax
	je	.L960
.L1172:
	movq	-344(%rbp), %rax
	movq	40(%rax), %rax
	jmp	.L961
	.p2align 4,,10
	.p2align 3
.L1174:
	movq	112(%r12), %rsi
	cmpq	120(%r12), %rsi
	je	.L979
	movq	%rsi, 120(%r12)
.L979:
	cmpq	%rsi, 128(%r12)
	je	.L980
	movq	%rbx, (%rsi)
	addq	$8, 120(%r12)
	cmpl	$1, -396(%rbp)
	jle	.L1065
.L1191:
	leaq	16(%r12), %rax
	movl	$1, %ebx
	movl	$1, -416(%rbp)
	movl	$1, %r13d
	movq	%rax, -424(%rbp)
	.p2align 4,,10
	.p2align 3
.L1005:
	movq	-408(%rbp), %rdi
	movl	%ebx, %esi
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movl	20(%rax), %r10d
	andl	$16777215, %r10d
	movl	%r10d, %edi
	movl	%r10d, -392(%rbp)
	call	_ZN2v84base10hash_valueEj@PLT
	movq	56(%r12), %rdi
	xorl	%edx, %edx
	movq	%rax, %r8
	divq	%rdi
	movq	48(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L983
	movq	(%rax), %rcx
	movl	-392(%rbp), %r10d
	movq	40(%rcx), %rsi
	jmp	.L986
	.p2align 4,,10
	.p2align 3
.L984:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L983
	movq	40(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r9
	jne	.L983
.L986:
	cmpq	%rsi, %r8
	jne	.L984
	cmpl	8(%rcx), %r10d
	jne	.L984
	leaq	16(%rcx), %rsi
	cmpl	$-1, %r15d
	je	.L1010
.L987:
	movl	%r15d, %edi
	movq	%rsi, -392(%rbp)
	call	_ZN2v84base10hash_valueEj@PLT
	movq	-392(%rbp), %rsi
	movq	(%rsi), %rdi
	testq	%rdi, %rdi
	je	.L988
	movl	20(%rdi), %edx
	xorl	%ecx, %ecx
	cmpl	%edx, %eax
	je	.L989
	.p2align 4,,10
	.p2align 3
.L1183:
	movl	%r14d, %r8d
	xorl	%eax, %edx
	shrl	%cl, %r8d
	testl	%edx, %r8d
	jne	.L990
.L991:
	addl	$1, %ecx
	movl	%r14d, %r8d
	shrl	%cl, %r8d
	testl	%edx, %r8d
	je	.L991
.L990:
	movsbl	16(%rdi), %edx
	cmpl	%ecx, %edx
	jg	.L1179
.L988:
	leaq	8(%rsi), %rcx
.L999:
	movq	(%rcx), %rax
	movl	$0, %esi
	cmpq	%rax, -368(%rbp)
	cmovne	%esi, %r13d
	movq	120(%r12), %rsi
	cmpq	$1, %rax
	movq	%rax, -360(%rbp)
	sbbl	$-1, -416(%rbp)
	cmpq	128(%r12), %rsi
	je	.L1003
	movq	%rax, (%rsi)
	addl	$1, %ebx
	addq	$8, 120(%r12)
	cmpl	%ebx, -396(%rbp)
	jne	.L1005
.L982:
	movq	-408(%rbp), %rax
	movl	20(%rax), %eax
	movl	%eax, %ebx
	movl	%eax, -392(%rbp)
	andl	$16777215, %ebx
	movl	%ebx, %edi
	call	_ZN2v84base10hash_valueEj@PLT
	movq	56(%r12), %rdi
	xorl	%edx, %edx
	movq	%rax, %r8
	divq	%rdi
	movq	48(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L1006
	movq	(%rax), %rcx
	movq	40(%rcx), %rsi
	jmp	.L1009
	.p2align 4,,10
	.p2align 3
.L1007:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L1006
	movq	40(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r9
	jne	.L1006
.L1009:
	cmpq	%r8, %rsi
	jne	.L1007
	cmpl	8(%rcx), %ebx
	jne	.L1007
	leaq	16(%rcx), %rbx
.L1055:
	cmpl	$-1, %r15d
	je	.L1010
	movl	%r15d, %edi
	call	_ZN2v84base10hash_valueEj@PLT
	movq	(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L1011
	movl	20(%rsi), %edx
	xorl	%ecx, %ecx
	cmpl	%edx, %eax
	je	.L1012
	.p2align 4,,10
	.p2align 3
.L1186:
	movl	%r14d, %edi
	xorl	%eax, %edx
	shrl	%cl, %edi
	testl	%edx, %edi
	jne	.L1013
.L1014:
	addl	$1, %ecx
	movl	%r14d, %edi
	shrl	%cl, %edi
	testl	%edx, %edi
	je	.L1014
.L1013:
	movsbl	16(%rsi), %edx
	cmpl	%edx, %ecx
	jl	.L1180
.L1011:
	leaq	8(%rbx), %rax
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	je	.L1028
.L1024:
	movq	(%rbx), %rax
	cmpw	$35, 16(%rax)
	je	.L1181
.L1028:
	cmpl	$1, -416(%rbp)
	jne	.L1027
	cmpb	$0, -397(%rbp)
	jne	.L1165
.L1027:
	movl	-416(%rbp), %ebx
	cmpl	%ebx, -396(%rbp)
	jg	.L1182
	testb	%r13b, %r13b
	jne	.L1165
	movq	120(%r12), %rsi
	cmpq	128(%r12), %rsi
	je	.L1038
	movq	-376(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 120(%r12)
.L1039:
	movq	8(%r12), %rax
	movl	-396(%rbp), %edx
	movl	$8, %esi
	movq	112(%r12), %rbx
	movq	8(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	movq	%rbx, %rcx
	movq	%r13, %rdi
	xorl	%r8d, %r8d
	movl	-444(%rbp), %edx
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r13
	movl	$4294967295, %eax
	movq	%rax, 8(%r13)
	movq	136(%r12), %rax
	movq	%r13, -360(%rbp)
	movl	16(%rax), %ebx
	leal	1(%rbx), %edx
	movl	%edx, 16(%r13)
	movq	104(%rax), %rbx
	movq	88(%rax), %rcx
	leaq	-8(%rbx), %rdx
	cmpq	%rdx, %rcx
	je	.L1040
	movq	%r13, (%rcx)
	addq	$8, 88(%rax)
.L1041:
	movq	-432(%rbp), %rdi
	movq	%r13, %rdx
	movl	%r15d, %esi
	call	_ZN2v88internal8compiler13PersistentMapINS1_8VariableEPNS1_4NodeENS_4base4hashIS3_EEE3SetES3_S5_
	jmp	.L978
	.p2align 4,,10
	.p2align 3
.L1179:
	movslq	%ecx, %rdx
	addl	$1, %ecx
	movq	32(%rdi,%rdx,8), %rdi
	testq	%rdi, %rdi
	je	.L988
	movl	20(%rdi), %edx
	cmpl	%edx, %eax
	jne	.L1183
.L989:
	movq	24(%rdi), %rax
	testq	%rax, %rax
	je	.L1184
	leaq	16(%rax), %rcx
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L988
	movq	%rcx, %rdx
	jmp	.L994
	.p2align 4,,10
	.p2align 3
.L1185:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L995
.L994:
	cmpl	32(%rax), %r15d
	jle	.L1185
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L994
.L995:
	cmpq	%rdx, %rcx
	je	.L988
	leaq	40(%rdx), %rcx
	cmpl	32(%rdx), %r15d
	jge	.L999
	jmp	.L988
	.p2align 4,,10
	.p2align 3
.L983:
	movq	-424(%rbp), %rsi
	cmpl	$-1, %r15d
	jne	.L987
.L1010:
	leaq	.LC5(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1180:
	movslq	%ecx, %rdx
	addl	$1, %ecx
	movq	32(%rsi,%rdx,8), %rsi
	testq	%rsi, %rsi
	je	.L1011
	movl	20(%rsi), %edx
	cmpl	%edx, %eax
	jne	.L1186
.L1012:
	movq	24(%rsi), %rax
	testq	%rax, %rax
	je	.L1187
	leaq	16(%rax), %rcx
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L1011
	movq	%rcx, %rdx
	jmp	.L1017
	.p2align 4,,10
	.p2align 3
.L1188:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L1018
.L1017:
	cmpl	32(%rax), %r15d
	jle	.L1188
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L1017
.L1018:
	cmpq	%rdx, %rcx
	je	.L1011
	leaq	40(%rdx), %rax
	cmpl	32(%rdx), %r15d
	jl	.L1011
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	jne	.L1024
	jmp	.L1028
	.p2align 4,,10
	.p2align 3
.L1184:
	leaq	8(%rdi), %rax
	leaq	8(%rsi), %rcx
	cmpl	(%rdi), %r15d
	cmove	%rax, %rcx
	jmp	.L999
	.p2align 4,,10
	.p2align 3
.L1175:
	addl	$1, %edx
	movq	%r8, %rsi
	movl	%edx, -352(%rbp)
	leaq	-328(%rbp), %rdx
	call	_ZN2v88internal8compiler13PersistentMapINS1_8VariableEPNS1_4NodeENS_4base4hashIS3_EEE12FindLeftmostEPKNS9_11FocusedTreeEPiPSt5arrayISC_Lm32EE
	movq	%rax, -336(%rbp)
	movq	%rax, %r13
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L1189
	movq	32(%rax), %rax
	movq	-72(%rbp), %rbx
	movq	%rax, -344(%rbp)
	cmpq	%rbx, 40(%rax)
	je	.L1051
	jmp	.L973
	.p2align 4,,10
	.p2align 3
.L1003:
	movq	-440(%rbp), %rdi
	leaq	-360(%rbp), %rdx
	addl	$1, %ebx
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	cmpl	-396(%rbp), %ebx
	jne	.L1005
	jmp	.L982
	.p2align 4,,10
	.p2align 3
.L1152:
	movl	0(%r13), %r15d
	movq	8(%r13), %rbx
	jmp	.L977
	.p2align 4,,10
	.p2align 3
.L1178:
	movq	-72(%rbp), %rax
	cmpq	%rax, 8(%r13)
	je	.L972
	jmp	.L965
	.p2align 4,,10
	.p2align 3
.L1165:
	movq	-368(%rbp), %rdx
	movq	-432(%rbp), %rdi
	movl	%r15d, %esi
	call	_ZN2v88internal8compiler13PersistentMapINS1_8VariableEPNS1_4NodeENS_4base4hashIS3_EEE3SetES3_S5_
	jmp	.L978
	.p2align 4,,10
	.p2align 3
.L953:
	leaq	16(%r12), %rcx
	jmp	.L1060
	.p2align 4,,10
	.p2align 3
.L1171:
	pxor	%xmm0, %xmm0
	movq	%rsi, -72(%rbp)
	movl	$0, -352(%rbp)
	movups	%xmm0, -344(%rbp)
	jmp	.L958
.L1170:
	movq	120(%r12), %r14
	leaq	0(,%rcx,8), %r13
	xorl	%edx, %edx
	movq	%r14, %r15
	subq	%rbx, %r15
	testq	%rcx, %rcx
	je	.L943
	movq	104(%r12), %rdi
	movq	%r13, %rsi
	movq	16(%rdi), %rdx
	movq	24(%rdi), %rax
	subq	%rdx, %rax
	cmpq	%rax, %r13
	ja	.L1190
	addq	%rdx, %rsi
	movq	%rsi, 16(%rdi)
.L943:
	cmpq	%r14, %rbx
	je	.L950
	subq	$8, %r14
	leaq	15(%rbx), %rax
	subq	%rbx, %r14
	subq	%rdx, %rax
	shrq	$3, %r14
	cmpq	$30, %rax
	jbe	.L1064
	movabsq	$2305843009213693948, %rax
	testq	%rax, %r14
	je	.L1064
	addq	$1, %r14
	xorl	%eax, %eax
	movq	%r14, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L948:
	movdqu	(%rbx,%rax), %xmm1
	movups	%xmm1, (%rdx,%rax)
	addq	$16, %rax
	cmpq	%rax, %rcx
	jne	.L948
	movq	%r14, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rcx
	addq	%rcx, %rbx
	addq	%rdx, %rcx
	cmpq	%r14, %rax
	je	.L950
	movq	(%rbx), %rax
	movq	%rax, (%rcx)
.L950:
	leaq	(%rdx,%r15), %rax
	movq	%rdx, 112(%r12)
	addq	%r13, %rdx
	movq	%rax, 120(%r12)
	movq	%rdx, 128(%r12)
	jmp	.L942
.L1006:
	leaq	16(%r12), %rbx
	jmp	.L1055
.L1189:
	movq	-72(%rbp), %rax
	cmpq	%rax, 8(%r13)
	je	.L1051
	jmp	.L973
	.p2align 4,,10
	.p2align 3
.L980:
	movq	-440(%rbp), %rdi
	leaq	-368(%rbp), %rdx
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	cmpl	$1, -396(%rbp)
	jg	.L1191
.L1065:
	movl	$1, -416(%rbp)
	movl	$1, %r13d
	jmp	.L982
.L1181:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	cmpq	%rax, -376(%rbp)
	jne	.L1028
	movl	-448(%rbp), %eax
	xorl	%r13d, %r13d
	movq	%rax, -392(%rbp)
	leaq	-360(%rbp), %rax
	movq	%rax, -416(%rbp)
	movl	-396(%rbp), %eax
	testl	%eax, %eax
	jle	.L1034
	movl	%r15d, -424(%rbp)
	jmp	.L1035
.L1030:
	cmpq	%r9, %rsi
	je	.L1031
	movl	%r15d, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties17ReplaceValueInputEPNS1_4NodeES4_i@PLT
	movq	136(%r12), %rax
	movq	%rbx, -360(%rbp)
	movl	16(%rbx), %ecx
	movl	16(%rax), %edx
	cmpl	%edx, %ecx
	jb	.L1031
	subl	%edx, %ecx
	cmpb	$3, %cl
	je	.L1192
.L1031:
	leaq	1(%r13), %rax
	cmpq	-392(%rbp), %r13
	je	.L1163
	movq	%rax, %r13
.L1035:
	movl	%r13d, %esi
	movq	%rbx, %rdi
	movl	%r13d, %r15d
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rax, %r9
	movq	112(%r12), %rax
	movq	(%rax,%r13,8), %rsi
	testq	%rsi, %rsi
	jne	.L1030
	movq	8(%r12), %r11
	movq	352(%r11), %rsi
	testq	%rsi, %rsi
	jne	.L1030
	movq	(%r11), %r10
	movq	8(%r11), %rdi
	movq	%r9, -472(%rbp)
	movq	%r11, -456(%rbp)
	movq	%r10, -464(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4DeadEv@PLT
	movq	-464(%rbp), %r10
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	xorl	%r8d, %r8d
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-456(%rbp), %r11
	movq	-472(%rbp), %r9
	movq	%rax, %rsi
	movq	%rax, 352(%r11)
	jmp	.L1030
	.p2align 4,,10
	.p2align 3
.L1187:
	leaq	8(%rbx), %rax
	leaq	8(%rsi), %rdx
	cmpl	(%rsi), %r15d
	cmove	%rdx, %rax
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	jne	.L1024
	jmp	.L1028
.L1182:
	movq	-432(%rbp), %rdi
	xorl	%edx, %edx
	movl	%r15d, %esi
	call	_ZN2v88internal8compiler13PersistentMapINS1_8VariableEPNS1_4NodeENS_4base4hashIS3_EEE3SetES3_S5_
	jmp	.L978
.L1064:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L947:
	movq	(%rbx,%rax,8), %rcx
	movq	%rcx, (%rdx,%rax,8)
	movq	%rax, %rcx
	addq	$1, %rax
	cmpq	%r14, %rcx
	jne	.L947
	jmp	.L950
.L1038:
	movq	-440(%rbp), %rdi
	leaq	-376(%rbp), %rdx
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1039
.L1040:
	leaq	-360(%rbp), %rsi
	leaq	24(%rax), %rdi
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	jmp	.L1041
.L1190:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L943
.L1192:
	addl	$1, %edx
	movl	%edx, 16(%rbx)
	movq	104(%rax), %rsi
	movq	88(%rax), %rcx
	leaq	-8(%rsi), %rdx
	cmpq	%rdx, %rcx
	je	.L1033
	movq	%rbx, (%rcx)
	addq	$8, 88(%rax)
	jmp	.L1031
.L1163:
	movl	-424(%rbp), %r15d
.L1034:
	movq	-432(%rbp), %rdi
	movq	%rbx, %rdx
	movl	%r15d, %esi
	call	_ZN2v88internal8compiler13PersistentMapINS1_8VariableEPNS1_4NodeENS_4base4hashIS3_EEE3SetES3_S5_
	jmp	.L978
.L1033:
	movq	-416(%rbp), %rsi
	leaq	24(%rax), %rdi
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	jmp	.L1031
.L1169:
	leaq	.LC7(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1177:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22989:
	.size	_ZN2v88internal8compiler15VariableTracker11MergeInputsEPNS1_4NodeE, .-_ZN2v88internal8compiler15VariableTracker11MergeInputsEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler15VariableTracker5ScopeC2EPS2_PNS1_4NodeEPNS1_18EffectGraphReducer9ReductionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15VariableTracker5ScopeC2EPS2_PNS1_4NodeEPNS1_18EffectGraphReducer9ReductionE
	.type	_ZN2v88internal8compiler15VariableTracker5ScopeC2EPS2_PNS1_4NodeEPNS1_18EffectGraphReducer9ReductionE, @function
_ZN2v88internal8compiler15VariableTracker5ScopeC2EPS2_PNS1_4NodeEPNS1_18EffectGraphReducer9ReductionE:
.LFB22984:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm0
	movq	%rcx, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movups	%xmm0, (%rdi)
	movq	(%rsi), %rax
	movq	$0, 24(%rdi)
	movq	$0, 32(%rdi)
	movq	%rax, 40(%rdi)
	movq	(%rdx), %rax
	movq	%rsi, 16(%rdi)
	cmpw	$36, 16(%rax)
	je	.L1210
	cmpl	$1, 24(%rax)
	je	.L1211
.L1193:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1212
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1210:
	.cfi_restore_state
	leaq	-80(%rbp), %rdi
	call	_ZN2v88internal8compiler15VariableTracker11MergeInputsEPNS1_4NodeE
	movdqu	-80(%rbp), %xmm2
	movq	-64(%rbp), %rax
	movups	%xmm2, 24(%rbx)
	movq	%rax, 40(%rbx)
	jmp	.L1193
	.p2align 4,,10
	.p2align 3
.L1211:
	movq	%rdx, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movl	20(%rax), %r13d
	andl	$16777215, %r13d
	movl	%r13d, %edi
	call	_ZN2v84base10hash_valueEj@PLT
	movq	56(%r12), %rdi
	xorl	%edx, %edx
	movq	%rax, %r8
	divq	%rdi
	movq	48(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L1196
	movq	(%rax), %rsi
	movq	40(%rsi), %rcx
	jmp	.L1199
	.p2align 4,,10
	.p2align 3
.L1197:
	movq	(%rsi), %rsi
	testq	%rsi, %rsi
	je	.L1196
	movq	40(%rsi), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%rdi
	cmpq	%rdx, %r9
	jne	.L1196
.L1199:
	cmpq	%rcx, %r8
	jne	.L1197
	cmpl	8(%rsi), %r13d
	jne	.L1197
	addq	$16, %rsi
.L1200:
	movdqu	(%rsi), %xmm3
	movq	16(%rsi), %rax
	movups	%xmm3, 24(%rbx)
	movq	%rax, 40(%rbx)
	jmp	.L1193
	.p2align 4,,10
	.p2align 3
.L1196:
	leaq	16(%r12), %rsi
	jmp	.L1200
.L1212:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22984:
	.size	_ZN2v88internal8compiler15VariableTracker5ScopeC2EPS2_PNS1_4NodeEPNS1_18EffectGraphReducer9ReductionE, .-_ZN2v88internal8compiler15VariableTracker5ScopeC2EPS2_PNS1_4NodeEPNS1_18EffectGraphReducer9ReductionE
	.globl	_ZN2v88internal8compiler15VariableTracker5ScopeC1EPS2_PNS1_4NodeEPNS1_18EffectGraphReducer9ReductionE
	.set	_ZN2v88internal8compiler15VariableTracker5ScopeC1EPS2_PNS1_4NodeEPNS1_18EffectGraphReducer9ReductionE,_ZN2v88internal8compiler15VariableTracker5ScopeC2EPS2_PNS1_4NodeEPNS1_18EffectGraphReducer9ReductionE
	.section	.rodata._ZN2v88internal8compiler12_GLOBAL__N_110ReduceNodeEPKNS1_8OperatorEPNS1_21EscapeAnalysisTracker5ScopeEPNS1_7JSGraphE.constprop.0.str1.1,"aMS",@progbits,1
.LC11:
	.string	"vobject->size() == size"
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_110ReduceNodeEPKNS1_8OperatorEPNS1_21EscapeAnalysisTracker5ScopeEPNS1_7JSGraphE.constprop.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_110ReduceNodeEPKNS1_8OperatorEPNS1_21EscapeAnalysisTracker5ScopeEPNS1_7JSGraphE.constprop.0, @function
_ZN2v88internal8compiler12_GLOBAL__N_110ReduceNodeEPKNS1_8OperatorEPNS1_21EscapeAnalysisTracker5ScopeEPNS1_7JSGraphE.constprop.0:
.LFB29387:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzwl	16(%rdi), %eax
	cmpw	$246, %ax
	ja	.L1214
	movq	%rdx, %r14
	cmpw	$219, %ax
	jbe	.L1848
	subw	$220, %ax
	cmpw	$26, %ax
	ja	.L1214
	leaq	.L1221(%rip), %rdx
	movzwl	%ax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler12_GLOBAL__N_110ReduceNodeEPKNS1_8OperatorEPNS1_21EscapeAnalysisTracker5ScopeEPNS1_7JSGraphE.constprop.0,"a",@progbits
	.align 4
	.align 4
.L1221:
	.long	.L1228-.L1221
	.long	.L1214-.L1221
	.long	.L1214-.L1221
	.long	.L1214-.L1221
	.long	.L1214-.L1221
	.long	.L1214-.L1221
	.long	.L1214-.L1221
	.long	.L1214-.L1221
	.long	.L1227-.L1221
	.long	.L1214-.L1221
	.long	.L1214-.L1221
	.long	.L1214-.L1221
	.long	.L1214-.L1221
	.long	.L1226-.L1221
	.long	.L1214-.L1221
	.long	.L1214-.L1221
	.long	.L1214-.L1221
	.long	.L1225-.L1221
	.long	.L1214-.L1221
	.long	.L1214-.L1221
	.long	.L1224-.L1221
	.long	.L1223-.L1221
	.long	.L1214-.L1221
	.long	.L1214-.L1221
	.long	.L1214-.L1221
	.long	.L1222-.L1221
	.long	.L1220-.L1221
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_110ReduceNodeEPKNS1_8OperatorEPNS1_21EscapeAnalysisTracker5ScopeEPNS1_7JSGraphE.constprop.0
	.p2align 4,,10
	.p2align 3
.L1848:
	cmpw	$57, %ax
	je	.L1216
	jbe	.L1849
	cmpw	$58, %ax
	je	.L1229
	cmpw	$125, %ax
	jne	.L1214
	movq	48(%rsi), %rbx
	movq	(%r12), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	88(%rbx), %rdi
	movq	80(%rbx), %rcx
	movq	%rax, %r13
	movl	20(%rax), %eax
	movq	%rdi, %rdx
	andl	$16777215, %eax
	subq	%rcx, %rdx
	movl	%eax, %r15d
	sarq	$3, %rdx
	cmpq	%rdx, %r15
	jnb	.L1850
.L1376:
	movq	(%rcx,%r15,8), %rax
	movq	48(%r12), %rbx
	movl	$1, %esi
	movq	(%r12), %rdi
	testq	%rax, %rax
	cmovne	%rax, %r13
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	88(%rbx), %r8
	movq	80(%rbx), %rdi
	movq	%rax, %r15
	movl	20(%rax), %eax
	movq	%r8, %rdx
	andl	$16777215, %eax
	subq	%rdi, %rdx
	movl	%eax, %ecx
	sarq	$3, %rdx
	cmpq	%rdx, %rcx
	jnb	.L1851
.L1379:
	movq	(%rdi,%rcx,8), %rax
	movl	20(%r13), %r10d
	movq	48(%r12), %rbx
	testq	%rax, %rax
	cmovne	%rax, %r15
	andl	$16777215, %r10d
	movl	%r10d, %edi
	movl	%r10d, -152(%rbp)
	call	_ZN2v84base10hash_valueEj@PLT
	movq	24(%rbx), %rdi
	xorl	%edx, %edx
	movq	%rax, %r8
	divq	%rdi
	movq	16(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L1382
	movq	(%rax), %rcx
	movl	-152(%rbp), %r10d
	movq	24(%rcx), %rsi
	jmp	.L1385
	.p2align 4,,10
	.p2align 3
.L1383:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L1382
	movq	24(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r9
	jne	.L1382
.L1385:
	cmpq	%rsi, %r8
	jne	.L1383
	cmpl	8(%rcx), %r10d
	jne	.L1383
	movq	16(%rcx), %rbx
.L1528:
	testq	%rbx, %rbx
	je	.L1386
	movq	(%r12), %rax
	movq	%rax, -112(%rbp)
	movq	16(%rbx), %rsi
	cmpq	24(%rbx), %rsi
	je	.L1387
	movq	%rax, (%rsi)
	addq	$8, 16(%rbx)
.L1386:
	movl	20(%r15), %r11d
	movq	48(%r12), %r9
	andl	$16777215, %r11d
	movq	%r9, -152(%rbp)
	movl	%r11d, %edi
	movl	%r11d, -160(%rbp)
	call	_ZN2v84base10hash_valueEj@PLT
	movq	-152(%rbp), %r9
	xorl	%edx, %edx
	movq	%rax, %r8
	movq	24(%r9), %rdi
	divq	%rdi
	movq	16(%r9), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r10
	testq	%rax, %rax
	je	.L1389
	movq	(%rax), %rcx
	movl	-160(%rbp), %r11d
	movq	24(%rcx), %rsi
	jmp	.L1392
	.p2align 4,,10
	.p2align 3
.L1390:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L1389
	movq	24(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r10
	jne	.L1389
.L1392:
	cmpq	%rsi, %r8
	jne	.L1390
	cmpl	8(%rcx), %r11d
	jne	.L1390
	movq	16(%rcx), %rax
.L1527:
	testq	%rax, %rax
	je	.L1393
	movq	(%r12), %rdx
	movq	%rdx, -112(%rbp)
	movq	16(%rax), %rsi
	cmpq	24(%rax), %rsi
	je	.L1394
	movq	%rdx, (%rsi)
	addq	$8, 16(%rax)
.L1395:
	testq	%rbx, %rbx
	je	.L1397
	cmpb	$0, 32(%rbx)
	jne	.L1397
	cmpb	$0, 32(%rax)
	jne	.L1401
	movl	36(%rax), %eax
	cmpl	%eax, 36(%rbx)
	je	.L1852
.L1401:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler7JSGraph13FalseConstantEv@PLT
.L1399:
	movq	48(%r12), %r8
	testq	%rax, %rax
	je	.L1402
	cmpq	$1, 8(%r13)
	je	.L1402
	cmpq	$1, 8(%r15)
	je	.L1402
	movq	%rax, 72(%r12)
	movl	20(%rax), %ebx
	movq	%r8, -152(%rbp)
	andl	$16777215, %ebx
	movl	%ebx, %edi
	call	_ZN2v84base10hash_valueEj@PLT
	movq	-152(%rbp), %r8
	xorl	%edx, %edx
	movq	%rax, %rdi
	movq	24(%r8), %r9
	divq	%r9
	movq	16(%r8), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r10
	testq	%rax, %rax
	je	.L1404
	movq	(%rax), %rcx
	movq	24(%rcx), %rsi
	jmp	.L1407
	.p2align 4,,10
	.p2align 3
.L1405:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L1404
	movq	24(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%r9
	cmpq	%rdx, %r10
	jne	.L1404
.L1407:
	cmpq	%rsi, %rdi
	jne	.L1405
	cmpl	8(%rcx), %ebx
	jne	.L1405
	.p2align 4,,10
	.p2align 3
.L1305:
	movq	16(%rcx), %rax
.L1496:
	movq	%rax, 64(%r12)
	.p2align 4,,10
	.p2align 3
.L1213:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1853
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1849:
	.cfi_restore_state
	cmpw	$40, %ax
	je	.L1218
	subl	$41, %eax
	cmpw	$1, %ax
	jbe	.L1213
.L1214:
	movl	20(%r13), %eax
	leaq	-112(%rbp), %rdi
	xorl	%ebx, %ebx
	movq	%rdi, -168(%rbp)
	movl	%eax, -160(%rbp)
	testl	%eax, %eax
	jle	.L1516
	.p2align 4,,10
	.p2align 3
.L1501:
	movq	48(%r12), %rdx
	movq	(%r12), %rdi
	movl	%ebx, %esi
	movq	%rdx, -152(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	-152(%rbp), %rdx
	movq	%rax, %r15
	movl	20(%rax), %eax
	movq	88(%rdx), %r9
	movq	80(%rdx), %r8
	andl	$16777215, %eax
	movq	%r9, %rdi
	movl	%eax, %r14d
	subq	%r8, %rdi
	sarq	$3, %rdi
	cmpq	%rdi, %r14
	jnb	.L1854
.L1503:
	movq	(%r8,%r14,8), %rax
	movq	48(%r12), %r14
	testq	%rax, %rax
	cmovne	%rax, %r15
	movl	20(%r15), %r15d
	andl	$16777215, %r15d
	movl	%r15d, %edi
	call	_ZN2v84base10hash_valueEj@PLT
	movq	24(%r14), %r8
	xorl	%edx, %edx
	movq	%rax, %rdi
	divq	%r8
	movq	16(%r14), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L1506
	movq	(%rax), %rcx
	movq	24(%rcx), %rsi
	jmp	.L1509
	.p2align 4,,10
	.p2align 3
.L1507:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L1506
	movq	24(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%r8
	cmpq	%rdx, %r9
	jne	.L1506
.L1509:
	cmpq	%rsi, %rdi
	jne	.L1507
	cmpl	8(%rcx), %r15d
	jne	.L1507
	movq	16(%rcx), %rcx
.L1520:
	testq	%rcx, %rcx
	je	.L1511
	cmpb	$0, 32(%rcx)
	je	.L1855
.L1511:
	addl	$1, %ebx
	cmpl	%ebx, -160(%rbp)
	jne	.L1501
.L1516:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18OperatorProperties15HasContextInputEPKNS1_8OperatorE@PLT
	testb	%al, %al
	je	.L1213
	movq	(%r12), %rdi
	movq	48(%r12), %r14
	call	_ZN2v88internal8compiler14NodeProperties15GetContextInputEPNS1_4NodeE@PLT
	movq	80(%r14), %rdi
	movl	20(%rax), %edx
	movq	%rax, %r13
	movq	88(%r14), %rax
	movq	%rax, %rcx
	andl	$16777215, %edx
	subq	%rdi, %rcx
	movl	%edx, %ebx
	sarq	$3, %rcx
	cmpq	%rcx, %rbx
	jnb	.L1856
.L1517:
	movq	(%rdi,%rbx,8), %rdx
	movq	48(%r12), %rdi
	leaq	56(%r12), %rsi
	testq	%rdx, %rdx
	cmovne	%rdx, %r13
	movq	%r13, %rdx
	call	_ZN2v88internal8compiler21EscapeAnalysisTracker5Scope10SetEscapedEPNS1_4NodeE.isra.0
	jmp	.L1213
	.p2align 4,,10
	.p2align 3
.L1506:
	movq	(%r14), %rcx
	jmp	.L1520
.L1220:
	movq	48(%rsi), %rbx
	movq	(%r12), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	88(%rbx), %rdi
	movq	80(%rbx), %rcx
	movq	%rax, %r14
	movl	20(%rax), %eax
	movq	%rdi, %rdx
	andl	$16777215, %eax
	subq	%rcx, %rdx
	movl	%eax, %r15d
	sarq	$3, %rdx
	cmpq	%rdx, %r15
	jnb	.L1857
.L1274:
	movq	(%rcx,%r15,8), %rax
	movq	(%r12), %rdi
	movl	$1, %esi
	movq	48(%r12), %r15
	testq	%rax, %rax
	cmovne	%rax, %r14
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	88(%r15), %r8
	movq	80(%r15), %rdi
	movq	%rax, %rbx
	movl	20(%rax), %eax
	movq	%r8, %rdx
	andl	$16777215, %eax
	subq	%rdi, %rdx
	movl	%eax, %ecx
	sarq	$3, %rdx
	cmpq	%rdx, %rcx
	jnb	.L1858
.L1277:
	movq	(%rdi,%rcx,8), %rax
	movq	48(%r12), %rcx
	movl	$2, %esi
	movq	(%r12), %rdi
	testq	%rax, %rax
	movq	%rcx, -152(%rbp)
	cmovne	%rax, %rbx
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	-152(%rbp), %rcx
	movq	%rax, %r15
	movl	20(%rax), %eax
	movq	88(%rcx), %r9
	movq	80(%rcx), %rdi
	andl	$16777215, %eax
	movq	%r9, %rdx
	movl	%eax, %r8d
	subq	%rdi, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %r8
	jnb	.L1859
.L1280:
	movq	(%rdi,%r8,8), %rax
	movl	20(%r14), %r11d
	movq	48(%r12), %r9
	testq	%rax, %rax
	cmovne	%rax, %r15
	andl	$16777215, %r11d
	movq	%r9, -152(%rbp)
	movl	%r11d, %edi
	movl	%r11d, -160(%rbp)
	call	_ZN2v84base10hash_valueEj@PLT
	movq	-152(%rbp), %r9
	xorl	%edx, %edx
	movq	%rax, %r8
	movq	24(%r9), %rdi
	divq	%rdi
	movq	16(%r9), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r10
	testq	%rax, %rax
	je	.L1283
	movq	(%rax), %rcx
	movl	-160(%rbp), %r11d
	movq	24(%rcx), %rsi
	jmp	.L1286
	.p2align 4,,10
	.p2align 3
.L1284:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L1283
	movq	24(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r10
	jne	.L1283
.L1286:
	cmpq	%rsi, %r8
	jne	.L1284
	cmpl	8(%rcx), %r11d
	jne	.L1284
	movq	16(%rcx), %r8
.L1550:
	testq	%r8, %r8
	je	.L1287
	movq	(%r12), %rax
	movq	%rax, -112(%rbp)
	movq	16(%r8), %rsi
	cmpq	24(%r8), %rsi
	je	.L1288
	movq	%rax, (%rsi)
	addq	$8, 16(%r8)
.L1289:
	cmpb	$0, 32(%r8)
	jne	.L1287
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%r8, -152(%rbp)
	call	_ZN2v88internal8compiler12_GLOBAL__N_122OffsetOfElementsAccessEPKNS1_8OperatorEPNS1_4NodeE
	movq	%rax, %rsi
	testb	%al, %al
	je	.L1287
	movq	-152(%rbp), %r8
	sarq	$32, %rsi
	movq	%r8, %rdi
	call	_ZNK2v88internal8compiler13VirtualObject7FieldAtEi
	testb	%al, %al
	je	.L1287
	sarq	$32, %rax
	cmpl	$-1, %eax
	je	.L1267
	movl	%eax, %esi
	leaq	24(%r12), %rdi
	movq	%r15, %rdx
	call	_ZN2v88internal8compiler13PersistentMapINS1_8VariableEPNS1_4NodeENS_4base4hashIS3_EEE3SetES3_S5_
	movq	48(%r12), %rbx
	movq	272(%rbx), %r13
	movq	352(%r13), %rax
	testq	%rax, %rax
	je	.L1860
	movq	%rax, 72(%r12)
.L1548:
	movl	20(%rax), %r13d
	andl	$16777215, %r13d
	movl	%r13d, %edi
	call	_ZN2v84base10hash_valueEj@PLT
	movq	24(%rbx), %rdi
	xorl	%edx, %edx
	movq	%rax, %r8
	divq	%rdi
	movq	16(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L1497
	movq	(%rax), %rcx
	movq	24(%rcx), %rsi
	jmp	.L1306
.L1304:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L1497
	movq	24(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r9
	jne	.L1497
.L1306:
	cmpq	%rsi, %r8
	jne	.L1304
	cmpl	8(%rcx), %r13d
	jne	.L1304
	jmp	.L1305
.L1222:
	movq	48(%rsi), %rbx
	movq	(%r12), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	88(%rbx), %rdi
	movq	80(%rbx), %rcx
	movq	%rax, %r14
	movl	20(%rax), %eax
	movq	%rdi, %rdx
	andl	$16777215, %eax
	subq	%rcx, %rdx
	movl	%eax, %r15d
	sarq	$3, %rdx
	cmpq	%rdx, %r15
	jnb	.L1861
.L1254:
	movq	(%rcx,%r15,8), %rax
	movq	48(%r12), %rbx
	movl	$1, %esi
	movq	(%r12), %rdi
	testq	%rax, %rax
	cmovne	%rax, %r14
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	88(%rbx), %r8
	movq	80(%rbx), %rdi
	movq	%rax, %r15
	movl	20(%rax), %eax
	movq	%r8, %rdx
	andl	$16777215, %eax
	subq	%rdi, %rdx
	movl	%eax, %ecx
	sarq	$3, %rdx
	cmpq	%rdx, %rcx
	jnb	.L1862
.L1257:
	movq	(%rdi,%rcx,8), %rax
	movl	20(%r14), %r10d
	movq	48(%r12), %rbx
	testq	%rax, %rax
	cmovne	%rax, %r15
	andl	$16777215, %r10d
	movl	%r10d, %edi
	movl	%r10d, -152(%rbp)
	call	_ZN2v84base10hash_valueEj@PLT
	movq	24(%rbx), %rdi
	xorl	%edx, %edx
	movq	%rax, %r8
	divq	%rdi
	movq	16(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L1260
	movq	(%rax), %rcx
	movl	-152(%rbp), %r10d
	movq	24(%rcx), %rsi
	jmp	.L1263
	.p2align 4,,10
	.p2align 3
.L1261:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L1260
	movq	24(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r9
	jne	.L1260
.L1263:
	cmpq	%rsi, %r8
	jne	.L1261
	cmpl	8(%rcx), %r10d
	jne	.L1261
	movq	16(%rcx), %rbx
.L1547:
	testq	%rbx, %rbx
	je	.L1264
	movq	(%r12), %rax
	movq	%rax, -112(%rbp)
	movq	16(%rbx), %rsi
	cmpq	24(%rbx), %rsi
	je	.L1265
	movq	%rax, (%rsi)
	addq	$8, 16(%rbx)
.L1266:
	cmpb	$0, 32(%rbx)
	jne	.L1264
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13FieldAccessOfEPKNS1_8OperatorE@PLT
	movq	%rbx, %rdi
	movl	4(%rax), %esi
	call	_ZNK2v88internal8compiler13VirtualObject7FieldAtEi
	testb	%al, %al
	je	.L1264
	sarq	$32, %rax
	cmpl	$-1, %eax
	je	.L1267
	movl	%eax, %esi
	leaq	24(%r12), %rdi
	movq	%r15, %rdx
	call	_ZN2v88internal8compiler13PersistentMapINS1_8VariableEPNS1_4NodeENS_4base4hashIS3_EEE3SetES3_S5_
	movq	48(%r12), %rbx
	movq	272(%rbx), %r13
	movq	352(%r13), %rax
	testq	%rax, %rax
	je	.L1863
	movq	%rax, 72(%r12)
.L1546:
	movl	20(%rax), %r13d
	andl	$16777215, %r13d
	movl	%r13d, %edi
	call	_ZN2v84base10hash_valueEj@PLT
	movq	24(%rbx), %rdi
	xorl	%edx, %edx
	movq	%rax, %r8
	divq	%rdi
	movq	16(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L1497
	movq	(%rax), %rcx
	movq	24(%rcx), %rsi
	jmp	.L1273
	.p2align 4,,10
	.p2align 3
.L1271:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L1497
	movq	24(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r9
	jne	.L1497
.L1273:
	cmpq	%rsi, %r8
	jne	.L1271
	cmpl	8(%rcx), %r13d
	jne	.L1271
	jmp	.L1305
.L1228:
	call	_ZN2v88internal8compiler21CheckMapsParametersOfEPKNS1_8OperatorE@PLT
	movq	48(%r12), %r13
	movq	(%r12), %rdi
	xorl	%esi, %esi
	movdqu	(%rax), %xmm2
	movaps	%xmm2, -112(%rbp)
	movdqu	16(%rax), %xmm3
	movaps	%xmm3, -96(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	88(%r13), %rdi
	movq	80(%r13), %rcx
	movq	%rax, %rbx
	movl	20(%rax), %eax
	movq	%rdi, %rdx
	andl	$16777215, %eax
	subq	%rcx, %rdx
	movl	%eax, %r14d
	sarq	$3, %rdx
	cmpq	%rdx, %r14
	jnb	.L1864
.L1408:
	movq	(%rcx,%r14,8), %rax
	movq	48(%r12), %r13
	testq	%rax, %rax
	cmovne	%rax, %rbx
	movl	20(%rbx), %r14d
	andl	$16777215, %r14d
	movl	%r14d, %edi
	call	_ZN2v84base10hash_valueEj@PLT
	movq	24(%r13), %rdi
	xorl	%edx, %edx
	movq	%rax, %r8
	divq	%rdi
	movq	16(%r13), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L1411
	movq	(%rax), %rcx
	movq	24(%rcx), %rsi
	jmp	.L1414
	.p2align 4,,10
	.p2align 3
.L1412:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L1411
	movq	24(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r9
	jne	.L1411
.L1414:
	cmpq	%rsi, %r8
	jne	.L1412
	cmpl	8(%rcx), %r14d
	jne	.L1412
	movq	16(%rcx), %r13
.L1532:
	testq	%r13, %r13
	je	.L1416
	movq	(%r12), %rax
	movq	%rax, -128(%rbp)
	movq	16(%r13), %rsi
	cmpq	24(%r13), %rsi
	je	.L1417
	movq	%rax, (%rsi)
	addq	$8, 16(%r13)
.L1418:
	cmpb	$0, 32(%r13)
	jne	.L1416
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler13VirtualObject7FieldAtEi
	testb	%al, %al
	je	.L1416
	sarq	$32, %rax
	movq	%rax, %r13
	movl	%eax, %r14d
	cmpl	$-1, %eax
	je	.L1267
	movl	%eax, %edi
	call	_ZN2v84base10hash_valueEj@PLT
	movq	24(%r12), %rsi
	movl	%eax, %edi
	testq	%rsi, %rsi
	je	.L1420
	xorl	%ecx, %ecx
	movl	$-2147483648, %r8d
	.p2align 4,,10
	.p2align 3
.L1424:
	movl	20(%rsi), %eax
	cmpl	%eax, %edi
	je	.L1421
	movl	%r8d, %edx
	xorl	%edi, %eax
	shrl	%cl, %edx
	testl	%eax, %edx
	jne	.L1422
.L1423:
	addl	$1, %ecx
	movl	%r8d, %edx
	shrl	%cl, %edx
	testl	%eax, %edx
	je	.L1423
.L1422:
	movsbl	16(%rsi), %eax
	cmpl	%ecx, %eax
	jg	.L1865
.L1420:
	leaq	32(%r12), %rsi
.L1431:
	movq	(%rsi), %rax
	testq	%rax, %rax
	je	.L1213
	movq	(%rax), %rdx
	cmpw	$61, 16(%rdx)
	je	.L1416
	movq	8(%rax), %rax
	movq	%rax, -136(%rbp)
	testb	$1, %al
	jne	.L1416
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L1416
	leaq	-136(%rbp), %rdi
	call	_ZNK2v88internal8compiler4Type14AsHeapConstantEv@PLT
	leaq	8(%rax), %rdi
	call	_ZNK2v88internal8compiler9ObjectRef5AsMapEv@PLT
	leaq	-128(%rbp), %rdi
	movq	%rax, -128(%rbp)
	movq	%rdx, -120(%rbp)
	call	_ZNK2v88internal8compiler6MapRef6objectEv@PLT
	leaq	-104(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZNK2v88internal13ZoneHandleSetINS0_3MapEE8containsENS0_6HandleIS2_EE
	testb	%al, %al
	je	.L1416
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler21EscapeAnalysisTracker5Scope15MarkForDeletionEv
	jmp	.L1213
.L1224:
	movq	48(%rsi), %rbx
	movq	(%r12), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	88(%rbx), %rdi
	movq	80(%rbx), %rcx
	movq	%rax, %r14
	movl	20(%rax), %eax
	movq	%rdi, %rdx
	andl	$16777215, %eax
	subq	%rcx, %rdx
	movl	%eax, %r15d
	sarq	$3, %rdx
	cmpq	%rdx, %r15
	jnb	.L1866
.L1307:
	movq	(%rcx,%r15,8), %rax
	movq	48(%r12), %r15
	testq	%rax, %rax
	cmovne	%rax, %r14
	movl	20(%r14), %ebx
	andl	$16777215, %ebx
	movl	%ebx, %edi
	call	_ZN2v84base10hash_valueEj@PLT
	movq	24(%r15), %rdi
	xorl	%edx, %edx
	movq	%rax, %r8
	divq	%rdi
	movq	16(%r15), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L1310
	movq	(%rax), %rcx
	movq	24(%rcx), %rsi
	jmp	.L1313
	.p2align 4,,10
	.p2align 3
.L1311:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L1310
	movq	24(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r9
	jne	.L1310
.L1313:
	cmpq	%rsi, %r8
	jne	.L1311
	cmpl	8(%rcx), %ebx
	jne	.L1311
	movq	16(%rcx), %r15
.L1542:
	testq	%r15, %r15
	je	.L1317
	movq	(%r12), %rax
	movq	%rax, -112(%rbp)
	movq	16(%r15), %rsi
	cmpq	24(%r15), %rsi
	je	.L1315
	movq	%rax, (%rsi)
	addq	$8, 16(%r15)
.L1316:
	cmpb	$0, 32(%r15)
	jne	.L1317
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13FieldAccessOfEPKNS1_8OperatorE@PLT
	movq	%r15, %rdi
	movl	4(%rax), %esi
	call	_ZNK2v88internal8compiler13VirtualObject7FieldAtEi
	testb	%al, %al
	je	.L1317
	sarq	$32, %rax
	movq	%rax, %rbx
	movl	%eax, %r13d
	cmpl	$-1, %eax
	je	.L1267
	movl	%eax, %edi
	call	_ZN2v84base10hash_valueEj@PLT
	movq	24(%r12), %rsi
	movl	%eax, %edi
	testq	%rsi, %rsi
	je	.L1318
	xorl	%ecx, %ecx
	movl	$-2147483648, %r8d
	.p2align 4,,10
	.p2align 3
.L1322:
	movl	20(%rsi), %eax
	cmpl	%eax, %edi
	je	.L1319
	movl	%r8d, %edx
	xorl	%edi, %eax
	shrl	%cl, %edx
	testl	%eax, %edx
	jne	.L1320
.L1321:
	addl	$1, %ecx
	movl	%r8d, %edx
	shrl	%cl, %edx
	testl	%eax, %edx
	je	.L1321
.L1320:
	movsbl	16(%rsi), %eax
	cmpl	%eax, %ecx
	jl	.L1867
.L1318:
	leaq	32(%r12), %rsi
.L1329:
	movq	(%rsi), %rsi
	testq	%rsi, %rsi
	je	.L1330
	movq	(%rsi), %rax
	cmpw	$61, 16(%rax)
	je	.L1317
.L1330:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler21EscapeAnalysisTracker5Scope14SetReplacementEPNS1_4NodeE
	jmp	.L1213
.L1226:
	movq	48(%rsi), %rbx
	movq	(%r12), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	88(%rbx), %r8
	movq	80(%rbx), %rdi
	movq	%rax, %r15
	movl	20(%rax), %eax
	movq	%r8, %rdx
	andl	$16777215, %eax
	subq	%rdi, %rdx
	movl	%eax, %ecx
	sarq	$3, %rdx
	cmpq	%rdx, %rcx
	jnb	.L1868
.L1445:
	movq	(%rdi,%rcx,8), %rax
	movq	48(%r12), %rbx
	testq	%rax, %rax
	cmovne	%rax, %r15
	movl	20(%r15), %r10d
	andl	$16777215, %r10d
	movl	%r10d, %edi
	movl	%r10d, -152(%rbp)
	call	_ZN2v84base10hash_valueEj@PLT
	movq	24(%rbx), %rdi
	xorl	%edx, %edx
	movq	%rax, %r8
	divq	%rdi
	movq	16(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L1448
	movq	(%rax), %rcx
	movl	-152(%rbp), %r10d
	movq	24(%rcx), %rsi
	jmp	.L1451
	.p2align 4,,10
	.p2align 3
.L1449:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L1448
	movq	24(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r9
	jne	.L1448
.L1451:
	cmpq	%rsi, %r8
	jne	.L1449
	cmpl	8(%rcx), %r10d
	jne	.L1449
	movq	16(%rcx), %rbx
.L1539:
	testq	%rbx, %rbx
	je	.L1455
	movq	(%r12), %rax
	movq	%rax, -112(%rbp)
	movq	16(%rbx), %rsi
	cmpq	24(%rbx), %rsi
	je	.L1453
	movq	%rax, (%rsi)
	addq	$8, 16(%rbx)
.L1454:
	cmpb	$0, 32(%rbx)
	jne	.L1455
	movq	56(%rbx), %rcx
	movq	48(%rbx), %rdx
	movq	%rcx, %rax
	subq	%rdx, %rax
	sarq	$2, %rax
	sall	$3, %eax
	testl	%eax, %eax
	jle	.L1455
	cmpq	%rcx, %rdx
	je	.L1869
	movl	(%rdx), %ebx
	cmpl	$-1, %ebx
	je	.L1267
	movl	%ebx, %edi
	call	_ZN2v84base10hash_valueEj@PLT
	movq	24(%r12), %rsi
	movl	%eax, %edi
	testq	%rsi, %rsi
	je	.L1457
	xorl	%ecx, %ecx
	movl	$-2147483648, %r8d
	.p2align 4,,10
	.p2align 3
.L1461:
	movl	20(%rsi), %eax
	cmpl	%eax, %edi
	je	.L1458
	movl	%r8d, %edx
	xorl	%edi, %eax
	shrl	%cl, %edx
	testl	%eax, %edx
	jne	.L1459
.L1460:
	addl	$1, %ecx
	movl	%r8d, %edx
	shrl	%cl, %edx
	testl	%eax, %edx
	je	.L1460
.L1459:
	movsbl	16(%rsi), %eax
	cmpl	%eax, %ecx
	jl	.L1870
.L1457:
	leaq	32(%r12), %rsi
.L1468:
	movq	(%rsi), %rbx
	testq	%rbx, %rbx
	je	.L1213
	movq	(%rbx), %rax
	cmpw	$61, 16(%rax)
	je	.L1455
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler23CompareMapsParametersOfEPKNS1_8OperatorE@PLT
	movq	%r14, %rdi
	movq	%rax, %r15
	movq	%rax, -200(%rbp)
	call	_ZN2v88internal8compiler7JSGraph12TrueConstantEv@PLT
	movq	%r14, %rdi
	movq	%rax, -208(%rbp)
	call	_ZN2v88internal8compiler7JSGraph13FalseConstantEv@PLT
	movq	%rax, %r13
	movq	(%r15), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	je	.L1476
	testq	%rdx, %rdx
	je	.L1556
	movq	14(%rax), %rdx
	subq	6(%rax), %rdx
	sarq	$3, %rdx
	movq	%rdx, -152(%rbp)
	jne	.L1471
.L1476:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler21EscapeAnalysisTracker5Scope14SetReplacementEPNS1_4NodeE
	jmp	.L1213
.L1223:
	movq	48(%rsi), %rbx
	movq	(%r12), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	88(%rbx), %r8
	movq	80(%rbx), %rdi
	movq	%rax, %r15
	movl	20(%rax), %eax
	movq	%r8, %rdx
	andl	$16777215, %eax
	subq	%rdi, %rdx
	movl	%eax, %ecx
	sarq	$3, %rdx
	cmpq	%rdx, %rcx
	jnb	.L1871
.L1331:
	movq	(%rdi,%rcx,8), %rax
	movq	48(%r12), %rcx
	movl	$1, %esi
	movq	(%r12), %rdi
	testq	%rax, %rax
	movq	%rcx, -152(%rbp)
	cmovne	%rax, %r15
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	-152(%rbp), %rcx
	movq	%rax, %rbx
	movl	20(%rax), %eax
	movq	88(%rcx), %r9
	movq	80(%rcx), %rdi
	andl	$16777215, %eax
	movq	%r9, %rdx
	movl	%eax, %r8d
	subq	%rdi, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %r8
	jnb	.L1872
.L1334:
	movq	(%rdi,%r8,8), %rax
	movl	20(%r15), %r11d
	movq	48(%r12), %r9
	testq	%rax, %rax
	cmovne	%rax, %rbx
	andl	$16777215, %r11d
	movq	%r9, -152(%rbp)
	movl	%r11d, %edi
	movl	%r11d, -160(%rbp)
	call	_ZN2v84base10hash_valueEj@PLT
	movq	-152(%rbp), %r9
	xorl	%edx, %edx
	movq	%rax, %r8
	movq	24(%r9), %rdi
	divq	%rdi
	movq	16(%r9), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r10
	testq	%rax, %rax
	je	.L1337
	movq	(%rax), %rcx
	movl	-160(%rbp), %r11d
	movq	24(%rcx), %rsi
	jmp	.L1340
	.p2align 4,,10
	.p2align 3
.L1338:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L1337
	movq	24(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r10
	jne	.L1337
.L1340:
	cmpq	%r8, %rsi
	jne	.L1338
	cmpl	8(%rcx), %r11d
	jne	.L1338
	movq	16(%rcx), %r8
.L1545:
	testq	%r8, %r8
	je	.L1455
	movq	(%r12), %rax
	movq	%rax, -112(%rbp)
	movq	16(%r8), %rsi
	cmpq	24(%r8), %rsi
	je	.L1342
	movq	%rax, (%rsi)
	addq	$8, 16(%r8)
.L1343:
	cmpb	$0, 32(%r8)
	jne	.L1455
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%r8, -152(%rbp)
	call	_ZN2v88internal8compiler12_GLOBAL__N_122OffsetOfElementsAccessEPKNS1_8OperatorEPNS1_4NodeE
	movq	-152(%rbp), %r8
	testb	%al, %al
	movq	%rax, %rsi
	je	.L1847
	sarq	$32, %rsi
	movq	%r8, %rdi
	movq	%r8, -152(%rbp)
	call	_ZNK2v88internal8compiler13VirtualObject7FieldAtEi
	movq	-152(%rbp), %r8
	testb	%al, %al
	je	.L1847
	sarq	$32, %rax
	movq	%r12, %rdi
	movq	%r8, -152(%rbp)
	movl	%eax, %esi
	call	_ZN2v88internal8compiler15VariableTracker5Scope3GetENS1_8VariableE
	movq	-152(%rbp), %r8
	testb	%al, %al
	je	.L1847
	movq	%rdx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler21EscapeAnalysisTracker5Scope14SetReplacementEPNS1_4NodeE
	jmp	.L1213
.L1227:
	movq	48(%rsi), %rbx
	movq	(%r12), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	88(%rbx), %r9
	movq	80(%rbx), %r8
	movl	20(%rax), %ecx
	movq	%rax, %rdx
	movq	%r9, %rdi
	andl	$16777215, %ecx
	subq	%r8, %rdi
	movl	%ecx, %r13d
	sarq	$3, %rdi
	cmpq	%rdi, %r13
	jnb	.L1873
.L1478:
	movq	(%r8,%r13,8), %rcx
	movq	48(%r12), %r13
	testq	%rcx, %rcx
	cmovne	%rcx, %rdx
	movq	(%rdx), %rax
	movzwl	16(%rax), %ecx
	cmpw	$40, %cx
	je	.L1535
	cmpw	$237, %cx
	je	.L1535
	cmpw	$30, %cx
	je	.L1535
	leaq	56(%r12), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21EscapeAnalysisTracker5Scope10SetEscapedEPNS1_4NodeE.isra.0
	jmp	.L1213
.L1225:
	movq	48(%rsi), %r13
	movq	(%r12), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	88(%r13), %rdi
	movq	80(%r13), %rcx
	movq	%rax, %rbx
	movl	20(%rax), %eax
	movq	%rdi, %rdx
	andl	$16777215, %eax
	subq	%rcx, %rdx
	movl	%eax, %r15d
	sarq	$3, %rdx
	cmpq	%rdx, %r15
	jnb	.L1874
.L1231:
	movq	(%rcx,%r15,8), %rax
	testq	%rax, %rax
	cmovne	%rax, %rbx
	movq	(%rbx), %rax
	cmpw	$28, 16(%rax)
	jne	.L1213
	movsd	48(%rax), %xmm0
	pxor	%xmm1, %xmm1
	cvttsd2sil	%xmm0, %r15d
	cvtsi2sdl	%r15d, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L1213
	jne	.L1213
	movq	(%r12), %rax
	movq	48(%r12), %rbx
	leaq	-112(%rbp), %r13
	movq	%r13, %rsi
	movl	20(%rax), %eax
	leaq	8(%rbx), %rdi
	andl	$16777215, %eax
	movl	%eax, -112(%rbp)
	call	_ZNKSt10_HashtableIjSt4pairIKjPN2v88internal8compiler13VirtualObjectEENS3_13ZoneAllocatorIS7_EENSt8__detail10_Select1stESt8equal_toIjENS2_4base4hashIjEENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS1_
	testq	%rax, %rax
	je	.L1235
	movq	16(%rax), %rbx
.L1236:
	testq	%rbx, %rbx
	je	.L1237
	movq	56(%rbx), %rax
	subq	48(%rbx), %rax
	sarq	$2, %rax
	sall	$3, %eax
	cmpl	%eax, %r15d
	jne	.L1875
.L1238:
	movq	(%r12), %rax
	movq	%rax, -112(%rbp)
	movq	16(%rbx), %rsi
	cmpq	24(%rbx), %rsi
	je	.L1242
	movq	%rax, (%rsi)
	addq	$8, 16(%rbx)
.L1243:
	movq	%rbx, 64(%r12)
	movq	48(%rbx), %r13
	movq	56(%rbx), %rbx
	cmpq	%rbx, %r13
	je	.L1213
	addq	$24, %r12
	jmp	.L1246
	.p2align 4,,10
	.p2align 3
.L1244:
	cmpl	$-1, %r15d
	je	.L1267
	movl	%r15d, %esi
	movq	%r12, %rdi
	addq	$4, %r13
	call	_ZN2v88internal8compiler13PersistentMapINS1_8VariableEPNS1_4NodeENS_4base4hashIS3_EEE3SetES3_S5_
	cmpq	%r13, %rbx
	je	.L1213
.L1246:
	movq	352(%r14), %rdx
	movl	0(%r13), %r15d
	testq	%rdx, %rdx
	jne	.L1244
	movq	(%r14), %r9
	movq	8(%r14), %rdi
	movq	%r9, -152(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4DeadEv@PLT
	movq	-152(%rbp), %r9
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%rax, %rsi
	xorl	%r8d, %r8d
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 352(%r14)
	movq	%rax, %rdx
	jmp	.L1244
	.p2align 4,,10
	.p2align 3
.L1854:
	leal	1(%rax), %esi
	cmpq	%rsi, %rdi
	jb	.L1876
	jbe	.L1503
	leaq	(%r8,%rsi,8), %rax
	cmpq	%rax, %r9
	je	.L1503
	movq	%rax, 88(%rdx)
	jmp	.L1503
.L1908:
	movq	%r8, %rdi
	movq	%r9, -152(%rbp)
	call	_ZNK2v88internal8compiler13VirtualObject7FieldAtEi
	testb	%al, %al
	je	.L1455
	sarq	$32, %rax
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler15VariableTracker5Scope3GetENS1_8VariableE
	movq	%rdx, %r13
	testb	%al, %al
	je	.L1455
	testq	%rdx, %rdx
	movq	-152(%rbp), %r9
	je	.L1476
	movq	8(%rdx), %rax
	movq	%rax, -112(%rbp)
	movq	8(%r9), %rsi
	cmpq	%rsi, %rax
	je	.L1476
	leaq	-112(%rbp), %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1476
	.p2align 4,,10
	.p2align 3
.L1455:
	movq	48(%r12), %rdi
	leaq	56(%r12), %rsi
	movq	%r15, %rdx
	call	_ZN2v88internal8compiler21EscapeAnalysisTracker5Scope10SetEscapedEPNS1_4NodeE.isra.0
	jmp	.L1213
	.p2align 4,,10
	.p2align 3
.L1287:
	movl	20(%r15), %r15d
	movq	48(%r12), %rbx
	leaq	56(%r12), %r13
	andl	$16777215, %r15d
	movl	%r15d, %edi
	call	_ZN2v84base10hash_valueEj@PLT
	movq	24(%rbx), %rdi
	xorl	%edx, %edx
	movq	%rax, %r8
	divq	%rdi
	movq	16(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L1291
	movq	(%rax), %rcx
	movq	24(%rcx), %rsi
	jmp	.L1294
	.p2align 4,,10
	.p2align 3
.L1292:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L1291
	movq	24(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r9
	jne	.L1291
.L1294:
	cmpq	%rsi, %r8
	jne	.L1292
	cmpl	8(%rcx), %r15d
	jne	.L1292
	movq	16(%rcx), %rcx
.L1549:
	testq	%rcx, %rcx
	je	.L1296
	cmpb	$0, 32(%rcx)
	jne	.L1296
	movq	8(%rcx), %rbx
	movq	16(%rcx), %r8
	movb	$1, 32(%rcx)
	movq	56(%r12), %r15
	cmpq	%r8, %rbx
	je	.L1296
	leaq	-112(%rbp), %rsi
	jmp	.L1300
	.p2align 4,,10
	.p2align 3
.L1298:
	addq	$8, %rbx
	cmpq	%rbx, %r8
	je	.L1877
.L1300:
	movq	(%rbx), %rdi
	movq	%rdi, -112(%rbp)
	movl	16(%rdi), %eax
	movl	16(%r15), %edx
	cmpl	%edx, %eax
	jb	.L1298
	subl	%edx, %eax
	cmpb	$3, %al
	jne	.L1298
	addl	$1, %edx
	movl	%edx, 16(%rdi)
	movq	104(%r15), %rax
	movq	88(%r15), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L1299
	movq	%rdi, (%rdx)
	addq	$8, 88(%r15)
	jmp	.L1298
	.p2align 4,,10
	.p2align 3
.L1317:
	movq	48(%r12), %rdi
	leaq	56(%r12), %rsi
	movq	%r14, %rdx
	call	_ZN2v88internal8compiler21EscapeAnalysisTracker5Scope10SetEscapedEPNS1_4NodeE.isra.0
	jmp	.L1213
	.p2align 4,,10
	.p2align 3
.L1264:
	movq	48(%r12), %rdi
	leaq	56(%r12), %r13
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler21EscapeAnalysisTracker5Scope10SetEscapedEPNS1_4NodeE.isra.0
	movq	48(%r12), %rdi
	movq	%r15, %rdx
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler21EscapeAnalysisTracker5Scope10SetEscapedEPNS1_4NodeE.isra.0
	jmp	.L1213
	.p2align 4,,10
	.p2align 3
.L1416:
	movl	20(%rbx), %ebx
	movq	48(%r12), %r13
	andl	$16777215, %ebx
	movl	%ebx, %edi
	call	_ZN2v84base10hash_valueEj@PLT
	movq	24(%r13), %rdi
	xorl	%edx, %edx
	movq	%rax, %r8
	divq	%rdi
	movq	16(%r13), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L1436
	movq	(%rax), %rcx
	movq	24(%rcx), %rsi
	jmp	.L1439
	.p2align 4,,10
	.p2align 3
.L1437:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L1436
	movq	24(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r9
	jne	.L1436
.L1439:
	cmpq	%r8, %rsi
	jne	.L1437
	cmpl	8(%rcx), %ebx
	jne	.L1437
	movq	16(%rcx), %r13
.L1529:
	testq	%r13, %r13
	je	.L1213
	cmpb	$0, 32(%r13)
	jne	.L1213
	movq	8(%r13), %rbx
	movq	16(%r13), %r14
	movb	$1, 32(%r13)
	movq	56(%r12), %r12
	cmpq	%r14, %rbx
	je	.L1213
	leaq	-128(%rbp), %r15
	jmp	.L1444
	.p2align 4,,10
	.p2align 3
.L1442:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	je	.L1878
.L1444:
	movq	(%rbx), %rdx
	movq	%rdx, -128(%rbp)
	movl	16(%rdx), %eax
	movl	16(%r12), %ecx
	cmpl	%ecx, %eax
	jb	.L1442
	subl	%ecx, %eax
	cmpb	$3, %al
	jne	.L1442
	addl	$1, %ecx
	movl	%ecx, 16(%rdx)
	movq	104(%r12), %rax
	movq	88(%r12), %rcx
	subq	$8, %rax
	cmpq	%rax, %rcx
	je	.L1443
	movq	%rdx, (%rcx)
	addq	$8, 88(%r12)
	jmp	.L1442
	.p2align 4,,10
	.p2align 3
.L1877:
	movq	8(%rcx), %rax
	cmpq	%rax, 16(%rcx)
	je	.L1296
	movq	%rax, 16(%rcx)
.L1296:
	movq	48(%r12), %rdi
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler21EscapeAnalysisTracker5Scope10SetEscapedEPNS1_4NodeE.isra.0
	jmp	.L1213
	.p2align 4,,10
	.p2align 3
.L1855:
	movq	8(%rcx), %r15
	movq	16(%rcx), %r9
	movb	$1, 32(%rcx)
	movq	56(%r12), %r8
	cmpq	%r9, %r15
	jne	.L1515
	jmp	.L1511
	.p2align 4,,10
	.p2align 3
.L1513:
	addq	$8, %r15
	cmpq	%r15, %r9
	je	.L1879
.L1515:
	movq	(%r15), %rsi
	movq	%rsi, -112(%rbp)
	movl	16(%rsi), %eax
	movl	16(%r8), %edx
	cmpl	%edx, %eax
	jb	.L1513
	subl	%edx, %eax
	cmpb	$3, %al
	jne	.L1513
	addl	$1, %edx
	movl	%edx, 16(%rsi)
	movq	104(%r8), %rax
	movq	88(%r8), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L1514
	addq	$8, %r15
	movq	%rsi, (%rdx)
	addq	$8, 88(%r8)
	cmpq	%r15, %r9
	jne	.L1515
	.p2align 4,,10
	.p2align 3
.L1879:
	movq	8(%rcx), %rax
	cmpq	%rax, 16(%rcx)
	je	.L1511
	movq	%rax, 16(%rcx)
	jmp	.L1511
	.p2align 4,,10
	.p2align 3
.L1216:
	movq	48(%rsi), %rbx
	movq	(%r12), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	88(%rbx), %r8
	movq	80(%rbx), %rdi
	movl	20(%rax), %edx
	movq	%rax, %r13
	movq	%r8, %rcx
	andl	$16777215, %edx
	subq	%rdi, %rcx
	movl	%edx, %r14d
	sarq	$3, %rcx
	cmpq	%rcx, %r14
	jnb	.L1880
.L1486:
	movq	(%rdi,%r14,8), %rdx
	movq	48(%r12), %rbx
	testq	%rdx, %rdx
	cmovne	%rdx, %r13
	movl	20(%r13), %eax
	andl	$16777215, %eax
	movl	%eax, %edi
	movl	%eax, %r13d
	call	_ZN2v84base10hash_valueEj@PLT
	movq	24(%rbx), %rdi
	xorl	%edx, %edx
	movq	%rax, %r8
	divq	%rdi
	movq	16(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L1489
	movq	(%rax), %rcx
	movq	24(%rcx), %rsi
	jmp	.L1492
	.p2align 4,,10
	.p2align 3
.L1490:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L1489
	movq	24(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r9
	jne	.L1489
.L1492:
	cmpq	%rsi, %r8
	jne	.L1490
	cmpl	8(%rcx), %r13d
	jne	.L1490
	movq	16(%rcx), %rbx
.L1523:
	testq	%rbx, %rbx
	je	.L1213
	movq	(%r12), %rax
	movq	%rax, -112(%rbp)
	movq	16(%rbx), %rsi
	cmpq	24(%rbx), %rsi
	je	.L1493
	movq	%rax, (%rsi)
	addq	$8, 16(%rbx)
.L1494:
	cmpb	$0, 32(%rbx)
	jne	.L1213
	movq	48(%r12), %rbx
	movq	272(%rbx), %r13
	movq	352(%r13), %rax
	testq	%rax, %rax
	je	.L1881
	movq	%rax, 72(%r12)
.L1522:
	movl	20(%rax), %eax
	andl	$16777215, %eax
	movl	%eax, %edi
	movl	%eax, %r13d
	call	_ZN2v84base10hash_valueEj@PLT
	movq	24(%rbx), %rdi
	xorl	%edx, %edx
	movq	%rax, %r8
	divq	%rdi
	movq	16(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L1497
	movq	(%rax), %rcx
	movq	24(%rcx), %rsi
	jmp	.L1500
	.p2align 4,,10
	.p2align 3
.L1498:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L1497
	movq	24(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r9
	jne	.L1497
.L1500:
	cmpq	%rsi, %r8
	jne	.L1498
	cmpl	8(%rcx), %r13d
	jne	.L1498
	jmp	.L1305
	.p2align 4,,10
	.p2align 3
.L1229:
	movq	48(%rsi), %rbx
	movq	(%r12), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	88(%rbx), %r8
	movq	80(%rbx), %rdi
	movl	20(%rax), %edx
	movq	%rax, %r13
	movq	%r8, %rcx
	andl	$16777215, %edx
	subq	%rdi, %rcx
	movl	%edx, %r14d
	sarq	$3, %rcx
	cmpq	%rcx, %r14
	jnb	.L1882
.L1369:
	movq	(%rdi,%r14,8), %rdx
	movq	48(%r12), %rbx
	testq	%rdx, %rdx
	cmovne	%rdx, %r13
	movl	20(%r13), %eax
	andl	$16777215, %eax
	movl	%eax, %edi
	movl	%eax, %r13d
	call	_ZN2v84base10hash_valueEj@PLT
	movq	24(%rbx), %rdi
	xorl	%edx, %edx
	movq	%rax, %r8
	divq	%rdi
	movq	16(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L1497
	movq	(%rax), %rcx
	movq	24(%rcx), %rsi
	jmp	.L1375
	.p2align 4,,10
	.p2align 3
.L1373:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L1497
	movq	24(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r9
	jne	.L1497
.L1375:
	cmpq	%rsi, %r8
	jne	.L1373
	cmpl	8(%rcx), %r13d
	jne	.L1373
	jmp	.L1305
	.p2align 4,,10
	.p2align 3
.L1497:
	movq	(%rbx), %rax
	jmp	.L1496
	.p2align 4,,10
	.p2align 3
.L1218:
	movq	48(%rsi), %rbx
	movq	(%r12), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	88(%rbx), %r8
	movq	80(%rbx), %rdi
	movl	20(%rax), %edx
	movq	%rax, %r13
	movq	%r8, %rcx
	andl	$16777215, %edx
	subq	%rdi, %rcx
	movl	%edx, %r14d
	sarq	$3, %rcx
	cmpq	%rcx, %r14
	jnb	.L1883
.L1247:
	movq	(%rdi,%r14,8), %rdx
	movq	48(%r12), %rbx
	testq	%rdx, %rdx
	cmovne	%rdx, %r13
	movl	20(%r13), %eax
	andl	$16777215, %eax
	movl	%eax, %edi
	movl	%eax, %r13d
	call	_ZN2v84base10hash_valueEj@PLT
	movq	24(%rbx), %rdi
	xorl	%edx, %edx
	movq	%rax, %r8
	divq	%rdi
	movq	16(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L1497
	movq	(%rax), %rcx
	movq	24(%rcx), %rsi
	jmp	.L1253
	.p2align 4,,10
	.p2align 3
.L1251:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L1497
	movq	24(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r9
	jne	.L1497
.L1253:
	cmpq	%rsi, %r8
	jne	.L1251
	cmpl	8(%rcx), %r13d
	jne	.L1251
	jmp	.L1305
	.p2align 4,,10
	.p2align 3
.L1876:
	subq	%rdi, %rsi
	leaq	72(%rdx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm
	movq	-152(%rbp), %rdx
	movq	80(%rdx), %r8
	jmp	.L1503
	.p2align 4,,10
	.p2align 3
.L1514:
	movq	-168(%rbp), %rsi
	leaq	24(%r8), %rdi
	movq	%r9, -176(%rbp)
	movq	%rcx, -192(%rbp)
	movq	%r8, -152(%rbp)
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	movq	-176(%rbp), %r9
	movq	-192(%rbp), %rcx
	movq	-152(%rbp), %r8
	jmp	.L1513
	.p2align 4,,10
	.p2align 3
.L1393:
	testq	%rbx, %rbx
	je	.L1552
	cmpb	$0, 32(%rbx)
	je	.L1401
.L1552:
	movq	48(%r12), %r8
.L1402:
	leaq	56(%r12), %r14
	movq	%r13, %rdx
	movq	%r8, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler21EscapeAnalysisTracker5Scope10SetEscapedEPNS1_4NodeE.isra.0
	movq	48(%r12), %rdi
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler21EscapeAnalysisTracker5Scope10SetEscapedEPNS1_4NodeE.isra.0
	jmp	.L1213
	.p2align 4,,10
	.p2align 3
.L1880:
	leal	1(%rdx), %esi
	cmpq	%rsi, %rcx
	jb	.L1884
	jbe	.L1486
	leaq	(%rdi,%rsi,8), %rax
	cmpq	%rax, %r8
	je	.L1486
	movq	%rax, 88(%rbx)
	jmp	.L1486
	.p2align 4,,10
	.p2align 3
.L1850:
	leal	1(%rax), %esi
	cmpq	%rsi, %rdx
	jb	.L1885
	jbe	.L1376
	leaq	(%rcx,%rsi,8), %rax
	cmpq	%rax, %rdi
	je	.L1376
	movq	%rax, 88(%rbx)
	jmp	.L1376
	.p2align 4,,10
	.p2align 3
.L1868:
	leal	1(%rax), %esi
	cmpq	%rsi, %rdx
	jb	.L1886
	jbe	.L1445
	leaq	(%rdi,%rsi,8), %rax
	cmpq	%rax, %r8
	je	.L1445
	movq	%rax, 88(%rbx)
	jmp	.L1445
	.p2align 4,,10
	.p2align 3
.L1851:
	leal	1(%rax), %esi
	cmpq	%rsi, %rdx
	jb	.L1887
	jbe	.L1379
	leaq	(%rdi,%rsi,8), %rax
	cmpq	%rax, %r8
	je	.L1379
	movq	%rax, 88(%rbx)
	jmp	.L1379
	.p2align 4,,10
	.p2align 3
.L1862:
	leal	1(%rax), %esi
	cmpq	%rsi, %rdx
	jb	.L1888
	jbe	.L1257
	leaq	(%rdi,%rsi,8), %rax
	cmpq	%rax, %r8
	je	.L1257
	movq	%rax, 88(%rbx)
	jmp	.L1257
	.p2align 4,,10
	.p2align 3
.L1857:
	leal	1(%rax), %esi
	cmpq	%rsi, %rdx
	jb	.L1889
	jbe	.L1274
	leaq	(%rcx,%rsi,8), %rax
	cmpq	%rax, %rdi
	je	.L1274
	movq	%rax, 88(%rbx)
	jmp	.L1274
	.p2align 4,,10
	.p2align 3
.L1872:
	leal	1(%rax), %esi
	cmpq	%rsi, %rdx
	jb	.L1890
	jbe	.L1334
	leaq	(%rdi,%rsi,8), %rax
	cmpq	%rax, %r9
	je	.L1334
	movq	%rax, 88(%rcx)
	jmp	.L1334
	.p2align 4,,10
	.p2align 3
.L1873:
	leal	1(%rcx), %esi
	cmpq	%rsi, %rdi
	jb	.L1891
	jbe	.L1478
	leaq	(%r8,%rsi,8), %rax
	cmpq	%rax, %r9
	je	.L1478
	movq	%rax, 88(%rbx)
	jmp	.L1478
	.p2align 4,,10
	.p2align 3
.L1859:
	leal	1(%rax), %esi
	cmpq	%rsi, %rdx
	jb	.L1892
	jbe	.L1280
	leaq	(%rdi,%rsi,8), %rax
	cmpq	%rax, %r9
	je	.L1280
	movq	%rax, 88(%rcx)
	jmp	.L1280
	.p2align 4,,10
	.p2align 3
.L1883:
	leal	1(%rdx), %esi
	cmpq	%rsi, %rcx
	jb	.L1893
	jbe	.L1247
	leaq	(%rdi,%rsi,8), %rax
	cmpq	%rax, %r8
	je	.L1247
	movq	%rax, 88(%rbx)
	jmp	.L1247
	.p2align 4,,10
	.p2align 3
.L1858:
	leal	1(%rax), %esi
	cmpq	%rsi, %rdx
	jb	.L1894
	jbe	.L1277
	leaq	(%rdi,%rsi,8), %rax
	cmpq	%rax, %r8
	je	.L1277
	movq	%rax, 88(%r15)
	jmp	.L1277
	.p2align 4,,10
	.p2align 3
.L1864:
	leal	1(%rax), %esi
	cmpq	%rsi, %rdx
	jb	.L1895
	jbe	.L1408
	leaq	(%rcx,%rsi,8), %rax
	cmpq	%rax, %rdi
	je	.L1408
	movq	%rax, 88(%r13)
	jmp	.L1408
	.p2align 4,,10
	.p2align 3
.L1882:
	leal	1(%rdx), %esi
	cmpq	%rsi, %rcx
	jb	.L1896
	jbe	.L1369
	leaq	(%rdi,%rsi,8), %rax
	cmpq	%rax, %r8
	je	.L1369
	movq	%rax, 88(%rbx)
	jmp	.L1369
	.p2align 4,,10
	.p2align 3
.L1874:
	leal	1(%rax), %esi
	cmpq	%rsi, %rdx
	jb	.L1897
	jbe	.L1231
	leaq	(%rcx,%rsi,8), %rax
	cmpq	%rax, %rdi
	je	.L1231
	movq	%rax, 88(%r13)
	jmp	.L1231
	.p2align 4,,10
	.p2align 3
.L1866:
	leal	1(%rax), %esi
	cmpq	%rsi, %rdx
	jb	.L1898
	jbe	.L1307
	leaq	(%rcx,%rsi,8), %rax
	cmpq	%rax, %rdi
	je	.L1307
	movq	%rax, 88(%rbx)
	jmp	.L1307
	.p2align 4,,10
	.p2align 3
.L1871:
	leal	1(%rax), %esi
	cmpq	%rsi, %rdx
	jb	.L1899
	jbe	.L1331
	leaq	(%rdi,%rsi,8), %rax
	cmpq	%rax, %r8
	je	.L1331
	movq	%rax, 88(%rbx)
	jmp	.L1331
	.p2align 4,,10
	.p2align 3
.L1861:
	leal	1(%rax), %esi
	cmpq	%rsi, %rdx
	jb	.L1900
	jbe	.L1254
	leaq	(%rcx,%rsi,8), %rax
	cmpq	%rax, %rdi
	je	.L1254
	movq	%rax, 88(%rbx)
	jmp	.L1254
	.p2align 4,,10
	.p2align 3
.L1489:
	movq	(%rbx), %rbx
	jmp	.L1523
	.p2align 4,,10
	.p2align 3
.L1337:
	movq	(%r9), %r8
	jmp	.L1545
	.p2align 4,,10
	.p2align 3
.L1310:
	movq	(%r15), %r15
	jmp	.L1542
	.p2align 4,,10
	.p2align 3
.L1448:
	movq	(%rbx), %rbx
	jmp	.L1539
	.p2align 4,,10
	.p2align 3
.L1411:
	movq	0(%r13), %r13
	jmp	.L1532
	.p2align 4,,10
	.p2align 3
.L1260:
	movq	(%rbx), %rbx
	jmp	.L1547
	.p2align 4,,10
	.p2align 3
.L1283:
	movq	(%r9), %r8
	jmp	.L1550
	.p2align 4,,10
	.p2align 3
.L1389:
	movq	(%r9), %rax
	jmp	.L1527
	.p2align 4,,10
	.p2align 3
.L1382:
	movq	(%rbx), %rbx
	jmp	.L1528
	.p2align 4,,10
	.p2align 3
.L1436:
	movq	0(%r13), %r13
	jmp	.L1529
	.p2align 4,,10
	.p2align 3
.L1291:
	movq	(%rbx), %rcx
	jmp	.L1549
	.p2align 4,,10
	.p2align 3
.L1397:
	cmpb	$0, 32(%rax)
	jne	.L1552
	jmp	.L1401
	.p2align 4,,10
	.p2align 3
.L1867:
	movslq	%ecx, %rax
	addl	$1, %ecx
	movq	32(%rsi,%rax,8), %rsi
	testq	%rsi, %rsi
	jne	.L1322
	jmp	.L1318
	.p2align 4,,10
	.p2align 3
.L1878:
	movq	8(%r13), %rax
	cmpq	16(%r13), %rax
	je	.L1213
	movq	%rax, 16(%r13)
	jmp	.L1213
	.p2align 4,,10
	.p2align 3
.L1865:
	movslq	%ecx, %rax
	addl	$1, %ecx
	movq	32(%rsi,%rax,8), %rsi
	testq	%rsi, %rsi
	jne	.L1424
	jmp	.L1420
	.p2align 4,,10
	.p2align 3
.L1870:
	movslq	%ecx, %rax
	addl	$1, %ecx
	movq	32(%rsi,%rax,8), %rsi
	testq	%rsi, %rsi
	jne	.L1461
	jmp	.L1457
	.p2align 4,,10
	.p2align 3
.L1535:
	movq	%rdx, 72(%r12)
	movl	20(%rdx), %ebx
	andl	$16777215, %ebx
	movl	%ebx, %edi
	call	_ZN2v84base10hash_valueEj@PLT
	movq	24(%r13), %rdi
	xorl	%edx, %edx
	movq	%rax, %r8
	divq	%rdi
	movq	16(%r13), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L1484
	movq	(%rax), %rcx
	movq	24(%rcx), %rsi
	jmp	.L1485
	.p2align 4,,10
	.p2align 3
.L1482:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L1484
	movq	24(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r9
	jne	.L1484
.L1485:
	cmpq	%r8, %rsi
	jne	.L1482
	cmpl	8(%rcx), %ebx
	jne	.L1482
	jmp	.L1305
.L1484:
	movq	0(%r13), %rax
	jmp	.L1496
.L1900:
	subq	%rdx, %rsi
	leaq	72(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm
	movq	80(%rbx), %rcx
	jmp	.L1254
.L1889:
	subq	%rdx, %rsi
	leaq	72(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm
	movq	80(%rbx), %rcx
	jmp	.L1274
.L1887:
	leaq	72(%rbx), %rdi
	subq	%rdx, %rsi
	movq	%rcx, -152(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm
	movq	80(%rbx), %rdi
	movq	-152(%rbp), %rcx
	jmp	.L1379
.L1885:
	subq	%rdx, %rsi
	leaq	72(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm
	movq	80(%rbx), %rcx
	jmp	.L1376
.L1897:
	subq	%rdx, %rsi
	leaq	72(%r13), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm
	movq	80(%r13), %rcx
	jmp	.L1231
.L1894:
	leaq	72(%r15), %rdi
	subq	%rdx, %rsi
	movq	%rcx, -152(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm
	movq	80(%r15), %rdi
	movq	-152(%rbp), %rcx
	jmp	.L1277
.L1899:
	leaq	72(%rbx), %rdi
	subq	%rdx, %rsi
	movq	%rcx, -152(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm
	movq	80(%rbx), %rdi
	movq	-152(%rbp), %rcx
	jmp	.L1331
.L1888:
	leaq	72(%rbx), %rdi
	subq	%rdx, %rsi
	movq	%rcx, -152(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm
	movq	80(%rbx), %rdi
	movq	-152(%rbp), %rcx
	jmp	.L1257
.L1892:
	leaq	72(%rcx), %rdi
	subq	%rdx, %rsi
	movq	%r8, -160(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm
	movq	-152(%rbp), %rcx
	movq	-160(%rbp), %r8
	movq	80(%rcx), %rdi
	jmp	.L1280
.L1856:
	leal	1(%rdx), %esi
	cmpq	%rsi, %rcx
	jb	.L1901
	jbe	.L1517
	leaq	(%rdi,%rsi,8), %rdx
	cmpq	%rdx, %rax
	je	.L1517
	movq	%rdx, 88(%r14)
	jmp	.L1517
.L1890:
	leaq	72(%rcx), %rdi
	subq	%rdx, %rsi
	movq	%r8, -160(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm
	movq	-152(%rbp), %rcx
	movq	-160(%rbp), %r8
	movq	80(%rcx), %rdi
	jmp	.L1334
.L1884:
	leaq	72(%rbx), %rdi
	subq	%rcx, %rsi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm
	movq	80(%rbx), %rdi
	jmp	.L1486
.L1886:
	leaq	72(%rbx), %rdi
	subq	%rdx, %rsi
	movq	%rcx, -152(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm
	movq	80(%rbx), %rdi
	movq	-152(%rbp), %rcx
	jmp	.L1445
.L1891:
	subq	%rdi, %rsi
	leaq	72(%rbx), %rdi
	movq	%rax, -152(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm
	movq	80(%rbx), %r8
	movq	-152(%rbp), %rdx
	jmp	.L1478
.L1896:
	leaq	72(%rbx), %rdi
	subq	%rcx, %rsi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm
	movq	80(%rbx), %rdi
	jmp	.L1369
.L1893:
	leaq	72(%rbx), %rdi
	subq	%rcx, %rsi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm
	movq	80(%rbx), %rdi
	jmp	.L1247
.L1895:
	subq	%rdx, %rsi
	leaq	72(%r13), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm
	movq	80(%r13), %rcx
	jmp	.L1408
.L1898:
	subq	%rdx, %rsi
	leaq	72(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm
	movq	80(%rbx), %rcx
	jmp	.L1307
.L1881:
	movq	8(%r13), %rdi
	movq	0(%r13), %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4DeadEv@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 352(%r13)
	movq	%rax, 72(%r12)
	testq	%rax, %rax
	je	.L1496
	movq	48(%r12), %rbx
	jmp	.L1522
	.p2align 4,,10
	.p2align 3
.L1299:
	leaq	24(%r15), %rdi
	movq	%r8, -168(%rbp)
	movq	%rcx, -160(%rbp)
	movq	%rsi, -152(%rbp)
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	movq	-168(%rbp), %r8
	movq	-160(%rbp), %rcx
	movq	-152(%rbp), %rsi
	jmp	.L1298
.L1443:
	leaq	24(%r12), %rdi
	movq	%r15, %rsi
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	jmp	.L1442
.L1394:
	movq	%rax, %rdi
	leaq	-112(%rbp), %rdx
	movq	%rax, -152(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	movq	-152(%rbp), %rax
	jmp	.L1395
.L1493:
	leaq	-112(%rbp), %rdx
	movq	%rbx, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1494
.L1387:
	leaq	-112(%rbp), %rdx
	movq	%rbx, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1386
.L1315:
	leaq	-112(%rbp), %rdx
	movq	%r15, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1316
.L1417:
	leaq	-128(%rbp), %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1418
.L1453:
	leaq	-112(%rbp), %rdx
	movq	%rbx, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1454
.L1265:
	leaq	-112(%rbp), %rdx
	movq	%rbx, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1266
.L1342:
	movq	%r8, %rdi
	leaq	-112(%rbp), %rdx
	movq	%r8, -152(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	movq	-152(%rbp), %r8
	jmp	.L1343
.L1288:
	movq	%r8, %rdi
	leaq	-112(%rbp), %rdx
	movq	%r8, -152(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	movq	-152(%rbp), %r8
	jmp	.L1289
.L1267:
	leaq	.LC5(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1319:
	movq	24(%rsi), %rax
	testq	%rax, %rax
	je	.L1902
	leaq	16(%rax), %rcx
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L1318
	movq	%rcx, %rdx
	jmp	.L1324
	.p2align 4,,10
	.p2align 3
.L1903:
	movq	%rax, %rdx
	movq	16(%rax), %rax
.L1327:
	testq	%rax, %rax
	je	.L1325
.L1324:
	cmpl	32(%rax), %r13d
	jle	.L1903
	movq	24(%rax), %rax
	jmp	.L1327
.L1421:
	movq	24(%rsi), %rax
	testq	%rax, %rax
	je	.L1904
	leaq	16(%rax), %rcx
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L1420
	movq	%rcx, %rdx
	jmp	.L1426
	.p2align 4,,10
	.p2align 3
.L1905:
	movq	%rax, %rdx
	movq	16(%rax), %rax
.L1429:
	testq	%rax, %rax
	je	.L1427
.L1426:
	cmpl	32(%rax), %r14d
	jle	.L1905
	movq	24(%rax), %rax
	jmp	.L1429
.L1458:
	movq	24(%rsi), %rax
	testq	%rax, %rax
	je	.L1906
	leaq	16(%rax), %rcx
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L1457
	movq	%rcx, %rdx
	jmp	.L1463
	.p2align 4,,10
	.p2align 3
.L1907:
	movq	%rax, %rdx
	movq	16(%rax), %rax
.L1466:
	testq	%rax, %rax
	je	.L1464
.L1463:
	cmpl	32(%rax), %ebx
	jle	.L1907
	movq	24(%rax), %rax
	jmp	.L1466
.L1847:
	movzbl	32(%r8), %eax
	testb	%al, %al
	jne	.L1455
	movq	%r13, %rdi
	movq	%r8, -152(%rbp)
	call	_ZN2v88internal8compiler15ElementAccessOfEPKNS1_8OperatorE@PLT
	movq	-152(%rbp), %r8
	movq	%rax, %r9
	movl	4(%rax), %esi
	movzbl	16(%r9), %edi
	movq	56(%r8), %rax
	subq	48(%r8), %rax
	sarq	$2, %rax
	leal	-1(%rdi), %edx
	sall	$3, %eax
	subl	%esi, %eax
	cmpb	$13, %dl
	ja	.L1350
	leaq	CSWTCH.372(%rip), %r13
	movzbl	%dl, %edx
	movl	0(%r13,%rdx,4), %ecx
	sarl	%cl, %eax
	cmpl	$1, %eax
	je	.L1908
	movq	%r9, -152(%rbp)
	cmpl	$2, %eax
	jne	.L1455
	movq	%r8, %rdi
	movq	%r8, -168(%rbp)
	call	_ZNK2v88internal8compiler13VirtualObject7FieldAtEi
	testb	%al, %al
	je	.L1455
	sarq	$32, %rax
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler15VariableTracker5Scope3GetENS1_8VariableE
	movq	%rdx, -160(%rbp)
	testb	%al, %al
	je	.L1455
	testq	%rdx, %rdx
	movq	-168(%rbp), %r8
	movq	-152(%rbp), %r9
	je	.L1360
	movq	-160(%rbp), %rax
	movq	8(%rax), %rax
	movq	%rax, -112(%rbp)
	movq	8(%r9), %rsi
	cmpq	%rsi, %rax
	je	.L1360
	leaq	-112(%rbp), %rdi
	movq	%r8, -168(%rbp)
	movq	%r9, -152(%rbp)
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	movq	-152(%rbp), %r9
	movq	-168(%rbp), %r8
	testb	%al, %al
	je	.L1455
.L1360:
	movzbl	16(%r9), %eax
	subl	$1, %eax
	cmpb	$13, %al
	ja	.L1350
	movzbl	%al, %eax
	movq	%r8, %rdi
	movq	%r9, -152(%rbp)
	movl	0(%r13,%rax,4), %ecx
	movl	$1, %eax
	sall	%cl, %eax
	addl	4(%r9), %eax
	movl	%eax, %esi
	call	_ZNK2v88internal8compiler13VirtualObject7FieldAtEi
	testb	%al, %al
	je	.L1455
	sarq	$32, %rax
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler15VariableTracker5Scope3GetENS1_8VariableE
	movq	%rdx, %r13
	testb	%al, %al
	je	.L1455
	testq	%rdx, %rdx
	je	.L1213
	movq	8(%rdx), %rax
	movq	-152(%rbp), %r9
	movq	%rax, -128(%rbp)
	movq	8(%r9), %rsi
	cmpq	%rsi, %rax
	je	.L1362
	leaq	-128(%rbp), %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	movq	-152(%rbp), %r9
	testb	%al, %al
	je	.L1455
.L1362:
	cmpq	$0, -160(%rbp)
	movq	%r9, -192(%rbp)
	je	.L1213
	movq	%r14, %rdi
	movq	(%r14), %r15
	call	_ZN2v88internal8compiler7JSGraph12ZeroConstantEv@PLT
	movq	376(%r14), %rdi
	movq	%rax, -152(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder11NumberEqualEv@PLT
	movq	%rbx, %xmm0
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movhps	-152(%rbp), %xmm0
	leaq	-80(%rbp), %rcx
	movq	%rax, %rsi
	movl	$2, %edx
	movq	%rcx, -168(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-192(%rbp), %r9
	xorl	%edx, %edx
	movq	$513, 8(%rax)
	movq	8(%r14), %rdi
	movq	%rax, %rbx
	movzbl	16(%r9), %esi
	movq	(%r14), %r15
	movq	%r9, -152(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6SelectENS0_21MachineRepresentationENS1_10BranchHintE@PLT
	movq	%rbx, %xmm0
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movq	-168(%rbp), %rcx
	movhps	-160(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r13, -64(%rbp)
	movl	$3, %edx
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-152(%rbp), %r9
	movq	8(%r9), %rdx
	movq	%rdx, 8(%rax)
	movq	48(%r12), %r14
	movq	%rax, 72(%r12)
	movl	20(%rax), %ebx
	andl	$16777215, %ebx
	movl	%ebx, %edi
	call	_ZN2v84base10hash_valueEj@PLT
	movq	24(%r14), %rdi
	xorl	%edx, %edx
	movq	%rax, %r8
	divq	%rdi
	movq	16(%r14), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L1365
	movq	(%rax), %rcx
	movq	24(%rcx), %rsi
	jmp	.L1368
.L1366:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L1365
	movq	24(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r9
	jne	.L1365
.L1368:
	cmpq	%rsi, %r8
	jne	.L1366
	cmpl	8(%rcx), %ebx
	jne	.L1366
	movq	16(%rcx), %rax
.L1543:
	movq	%rax, 64(%r12)
	movq	48(%r12), %rdi
	leaq	56(%r12), %r14
	movq	-160(%rbp), %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler21EscapeAnalysisTracker5Scope10SetEscapedEPNS1_4NodeE.isra.0
	movq	48(%r12), %rdi
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler21EscapeAnalysisTracker5Scope10SetEscapedEPNS1_4NodeE.isra.0
	jmp	.L1213
.L1235:
	movq	(%rbx), %rbx
	jmp	.L1236
.L1325:
	cmpq	%rcx, %rdx
	je	.L1318
	leaq	40(%rdx), %rsi
	cmpl	32(%rdx), %ebx
	jge	.L1329
	jmp	.L1318
.L1863:
	movq	8(%r13), %rdi
	movq	0(%r13), %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4DeadEv@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 352(%r13)
	movq	%rax, 72(%r12)
	testq	%rax, %rax
	je	.L1496
	movq	48(%r12), %rbx
	jmp	.L1546
	.p2align 4,,10
	.p2align 3
.L1901:
	leaq	72(%r14), %rdi
	subq	%rcx, %rsi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm
	movq	80(%r14), %rdi
	jmp	.L1517
.L1427:
	cmpq	%rdx, %rcx
	je	.L1420
	leaq	40(%rdx), %rsi
	cmpl	32(%rdx), %r13d
	jge	.L1431
	jmp	.L1420
.L1464:
	cmpq	%rdx, %rcx
	je	.L1457
	leaq	40(%rdx), %rsi
	cmpl	32(%rdx), %ebx
	jge	.L1468
	jmp	.L1457
.L1852:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler7JSGraph12TrueConstantEv@PLT
	jmp	.L1399
.L1404:
	movq	(%r8), %rax
	jmp	.L1496
.L1237:
	movq	48(%r12), %rdx
	movl	264(%rdx), %r8d
	cmpl	$99, %r8d
	ja	.L1239
	movq	280(%rdx), %rdi
	leal	1(%r8), %eax
	movl	%eax, 264(%rdx)
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	$71, %rax
	jbe	.L1909
	leaq	72(%rbx), %rax
	movq	%rax, 16(%rdi)
.L1241:
	leaq	104(%rdx), %rsi
	movl	%r15d, %ecx
	movl	%r8d, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13VirtualObjectC1EPNS1_15VariableTrackerEji
	testq	%rbx, %rbx
	jne	.L1238
.L1239:
	movq	$0, 64(%r12)
	jmp	.L1213
.L1902:
	cmpl	(%rsi), %ebx
	jne	.L1318
	addq	$8, %rsi
	jmp	.L1329
.L1904:
	cmpl	(%rsi), %r13d
	jne	.L1420
	addq	$8, %rsi
	jmp	.L1431
.L1906:
	cmpl	(%rsi), %ebx
	jne	.L1457
	addq	$8, %rsi
	jmp	.L1468
.L1242:
	movq	%r13, %rdx
	movq	%rbx, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1243
.L1875:
	leaq	.LC11(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1365:
	movq	(%r14), %rax
	jmp	.L1543
.L1860:
	movq	8(%r13), %rdi
	movq	0(%r13), %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4DeadEv@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 352(%r13)
	movq	%rax, 72(%r12)
	testq	%rax, %rax
	je	.L1496
	movq	48(%r12), %rbx
	jmp	.L1548
.L1556:
	movq	$1, -152(%rbp)
.L1471:
	movq	%r13, %r9
	xorl	%r10d, %r10d
	leaq	-80(%rbp), %r15
	movq	%rax, %rsi
	testb	$3, %al
	je	.L1473
.L1911:
	movq	6(%rax), %rcx
	movq	14(%rax), %rdx
	movslq	%r10d, %rsi
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L1910
	movq	(%rcx,%rsi,8), %rsi
.L1473:
	movq	%r14, %rdi
	movq	%r10, -176(%rbp)
	movq	%r9, -192(%rbp)
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	$58720257, 8(%rax)
	movq	(%r14), %r11
	movq	376(%r14), %rdi
	movq	%rax, -160(%rbp)
	movq	%r11, -168(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14ReferenceEqualEv@PLT
	movq	%rbx, %xmm0
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	-168(%rbp), %r11
	movhps	-160(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movaps	%xmm0, -80(%rbp)
	movq	%r11, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-192(%rbp), %r9
	movq	-176(%rbp), %r10
	movq	$513, 8(%rax)
	cmpq	%r9, %r13
	movq	%r9, -160(%rbp)
	je	.L1557
	movq	(%r14), %r11
	movq	8(%r14), %rdi
	movq	%rax, %xmm0
	xorl	%edx, %edx
	movhps	-208(%rbp), %xmm0
	movl	$7, %esi
	movq	%r11, -168(%rbp)
	movaps	%xmm0, -192(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6SelectENS0_21MachineRepresentationENS1_10BranchHintE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$3, %edx
	movq	-168(%rbp), %r11
	movq	-160(%rbp), %r9
	movq	%rax, %rsi
	movdqa	-192(%rbp), %xmm0
	movq	%r11, %rdi
	movq	%r9, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-176(%rbp), %r10
	movq	$513, 8(%rax)
	movq	%rax, %r9
.L1475:
	addq	$1, %r10
	cmpq	%r10, -152(%rbp)
	je	.L1558
	movq	-200(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, %rsi
	testb	$3, %al
	je	.L1473
	jmp	.L1911
.L1557:
	movq	%rax, %r9
	jmp	.L1475
.L1558:
	movq	%r9, %r13
	jmp	.L1476
.L1853:
	call	__stack_chk_fail@PLT
.L1909:
	movl	$72, %esi
	movl	%r8d, -160(%rbp)
	movq	%rdx, -152(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-152(%rbp), %rdx
	movl	-160(%rbp), %r8d
	movq	%rax, %rbx
	jmp	.L1241
.L1869:
	xorl	%edx, %edx
	xorl	%esi, %esi
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L1350:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1910:
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE29387:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_110ReduceNodeEPKNS1_8OperatorEPNS1_21EscapeAnalysisTracker5ScopeEPNS1_7JSGraphE.constprop.0, .-_ZN2v88internal8compiler12_GLOBAL__N_110ReduceNodeEPKNS1_8OperatorEPNS1_21EscapeAnalysisTracker5ScopeEPNS1_7JSGraphE.constprop.0
	.section	.text._ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm,"axG",@progbits,_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	.type	_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm, @function
_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm:
.LFB27770:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$8, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	shrq	$6, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	1(%rsi), %rbx
	addq	$3, %rsi
	subq	$8, %rsp
	cmpq	$8, %rsi
	cmovb	%rax, %rsi
	movq	%rsi, 24(%rdi)
	movq	(%rdi), %rdi
	salq	$3, %rsi
	movq	16(%rdi), %rdx
	movq	24(%rdi), %rax
	subq	%rdx, %rax
	cmpq	%rax, %rsi
	ja	.L1926
	addq	%rdx, %rsi
	movq	%rsi, 16(%rdi)
.L1914:
	movq	24(%r12), %rax
	movq	%rdx, 16(%r12)
	subq	%rbx, %rax
	shrq	%rax
	leaq	(%rdx,%rax,8), %r15
	leaq	(%r15,%rbx,8), %r14
	cmpq	%r14, %r15
	jnb	.L1915
	movq	%r15, %rbx
	jmp	.L1920
	.p2align 4,,10
	.p2align 3
.L1916:
	movq	(%r12), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L1927
	leaq	512(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L1917:
	movq	%rax, (%rbx)
	addq	$8, %rbx
	cmpq	%rbx, %r14
	jbe	.L1915
.L1920:
	movq	8(%r12), %rax
	testq	%rax, %rax
	je	.L1916
	cmpq	$63, 8(%rax)
	jbe	.L1916
	movq	(%rax), %rdx
	addq	$8, %rbx
	movq	%rdx, 8(%r12)
	movq	%rax, -8(%rbx)
	cmpq	%rbx, %r14
	ja	.L1920
.L1915:
	movq	%r15, 56(%r12)
	movq	(%r15), %rdx
	andl	$63, %r13d
	leaq	512(%rdx), %rax
	movq	%rdx, 40(%r12)
	movq	%rax, 48(%r12)
	leaq	-8(%r14), %rax
	movq	%rax, 88(%r12)
	movq	-8(%r14), %rax
	movq	%rdx, 32(%r12)
	movq	%rax, 72(%r12)
	leaq	512(%rax), %rcx
	leaq	(%rax,%r13,8), %rax
	movq	%rcx, 80(%r12)
	movq	%rax, 64(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1927:
	.cfi_restore_state
	movl	$512, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1917
	.p2align 4,,10
	.p2align 3
.L1926:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L1914
	.cfi_endproc
.LFE27770:
	.size	_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm, .-_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	.section	.text._ZNSt11_Deque_baseIN2v88internal8compiler18EffectGraphReducer9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm,"axG",@progbits,_ZNSt11_Deque_baseIN2v88internal8compiler18EffectGraphReducer9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt11_Deque_baseIN2v88internal8compiler18EffectGraphReducer9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	.type	_ZNSt11_Deque_baseIN2v88internal8compiler18EffectGraphReducer9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm, @function
_ZNSt11_Deque_baseIN2v88internal8compiler18EffectGraphReducer9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm:
.LFB27784:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$8, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	shrq	$5, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	1(%rsi), %rbx
	addq	$3, %rsi
	subq	$8, %rsp
	cmpq	$8, %rsi
	cmovb	%rax, %rsi
	movq	%rsi, 24(%rdi)
	movq	(%rdi), %rdi
	salq	$3, %rsi
	movq	16(%rdi), %rdx
	movq	24(%rdi), %rax
	subq	%rdx, %rax
	cmpq	%rax, %rsi
	ja	.L1942
	addq	%rdx, %rsi
	movq	%rsi, 16(%rdi)
.L1930:
	movq	24(%r12), %rax
	movq	%rdx, 16(%r12)
	subq	%rbx, %rax
	shrq	%rax
	leaq	(%rdx,%rax,8), %r15
	leaq	(%r15,%rbx,8), %r14
	cmpq	%r14, %r15
	jnb	.L1931
	movq	%r15, %rbx
	jmp	.L1936
	.p2align 4,,10
	.p2align 3
.L1932:
	movq	(%r12), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L1943
	leaq	512(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L1933:
	movq	%rax, (%rbx)
	addq	$8, %rbx
	cmpq	%rbx, %r14
	jbe	.L1931
.L1936:
	movq	8(%r12), %rax
	testq	%rax, %rax
	je	.L1932
	cmpq	$31, 8(%rax)
	jbe	.L1932
	movq	(%rax), %rdx
	addq	$8, %rbx
	movq	%rdx, 8(%r12)
	movq	%rax, -8(%rbx)
	cmpq	%rbx, %r14
	ja	.L1936
.L1931:
	movq	%r15, 56(%r12)
	movq	(%r15), %rdx
	andl	$31, %r13d
	salq	$4, %r13
	leaq	512(%rdx), %rax
	movq	%rdx, 40(%r12)
	movq	%rax, 48(%r12)
	leaq	-8(%r14), %rax
	movq	%rax, 88(%r12)
	movq	-8(%r14), %rax
	movq	%rdx, 32(%r12)
	addq	%rax, %r13
	leaq	512(%rax), %rcx
	movq	%rax, 72(%r12)
	movq	%r13, 64(%r12)
	movq	%rcx, 80(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1943:
	.cfi_restore_state
	movl	$512, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1933
	.p2align 4,,10
	.p2align 3
.L1942:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L1930
	.cfi_endproc
.LFE27784:
	.size	_ZNSt11_Deque_baseIN2v88internal8compiler18EffectGraphReducer9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm, .-_ZNSt11_Deque_baseIN2v88internal8compiler18EffectGraphReducer9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	.section	.text._ZN2v88internal8compiler18EffectGraphReducerC2EPNS1_5GraphESt8functionIFvPNS1_4NodeEPNS2_9ReductionEEEPNS0_11TickCounterEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18EffectGraphReducerC2EPNS1_5GraphESt8functionIFvPNS1_4NodeEPNS2_9ReductionEEEPNS0_11TickCounterEPNS0_4ZoneE
	.type	_ZN2v88internal8compiler18EffectGraphReducerC2EPNS1_5GraphESt8functionIFvPNS1_4NodeEPNS2_9ReductionEEEPNS0_11TickCounterEPNS0_4ZoneE, @function
_ZN2v88internal8compiler18EffectGraphReducerC2EPNS1_5GraphESt8functionIFvPNS1_4NodeEPNS2_9ReductionEEEPNS0_11TickCounterEPNS0_4ZoneE:
.LFB22973:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-256(%rbp), %r15
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	movl	$4, %edx
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$16, %rdi
	subq	$216, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rsi, -8(%rdi)
	movb	$4, -16(%rdi)
	call	_ZN2v88internal8compiler14NodeMarkerBaseC2EPNS1_5GraphEj@PLT
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%r14, -256(%rbp)
	movq	$0, -248(%rbp)
	movq	$0, -240(%rbp)
	movq	$0, -232(%rbp)
	movaps	%xmm0, -224(%rbp)
	movaps	%xmm0, -208(%rbp)
	movaps	%xmm0, -192(%rbp)
	movaps	%xmm0, -176(%rbp)
	call	_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	cmpq	$0, -240(%rbp)
	je	.L1975
	movdqa	-256(%rbp), %xmm6
	pxor	%xmm0, %xmm0
	leaq	-160(%rbp), %rdi
	xorl	%esi, %esi
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm6, -160(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	$0, -144(%rbp)
	movq	$0, -136(%rbp)
	call	_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	movdqa	-128(%rbp), %xmm7
	movq	-224(%rbp), %xmm3
	movq	-216(%rbp), %r9
	movdqa	-112(%rbp), %xmm6
	movq	-248(%rbp), %rax
	movaps	%xmm7, -224(%rbp)
	movdqa	-96(%rbp), %xmm7
	movq	-200(%rbp), %r8
	movq	-184(%rbp), %rcx
	movq	-168(%rbp), %rdx
	movdqa	-80(%rbp), %xmm5
	movq	-256(%rbp), %xmm4
	movq	-240(%rbp), %r11
	movq	-208(%rbp), %xmm2
	movaps	%xmm6, -208(%rbp)
	movq	-192(%rbp), %xmm1
	movq	%rax, %xmm6
	movq	-232(%rbp), %r10
	movaps	%xmm7, -192(%rbp)
	movq	%r9, %xmm7
	movq	-176(%rbp), %xmm0
	punpcklqdq	%xmm6, %xmm4
	punpcklqdq	%xmm7, %xmm3
	movq	-144(%rbp), %rsi
	movq	-136(%rbp), %rdi
	movq	%rcx, %xmm6
	movaps	%xmm5, -176(%rbp)
	movq	%rdx, %xmm7
	movq	%r8, %xmm5
	punpcklqdq	%xmm5, %xmm2
	punpcklqdq	%xmm6, %xmm1
	punpcklqdq	%xmm7, %xmm0
	movq	%r11, 40(%rbx)
	movq	%rsi, -240(%rbp)
	movq	%rdi, -232(%rbp)
	movq	%r10, 48(%rbx)
	movups	%xmm4, 24(%rbx)
	movups	%xmm3, 56(%rbx)
	movups	%xmm2, 72(%rbx)
	movups	%xmm1, 88(%rbx)
	movups	%xmm0, 104(%rbx)
	testq	%rsi, %rsi
	je	.L1946
	movq	-168(%rbp), %rcx
	movq	-200(%rbp), %rdx
	addq	$8, %rcx
	cmpq	%rdx, %rcx
	jbe	.L1947
	.p2align 4,,10
	.p2align 3
.L1950:
	testq	%rax, %rax
	je	.L1948
	cmpq	$64, 8(%rax)
	ja	.L1949
.L1948:
	movq	(%rdx), %rax
	movq	$64, 8(%rax)
	movq	-248(%rbp), %rsi
	movq	%rsi, (%rax)
	movq	%rax, -248(%rbp)
.L1949:
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	ja	.L1950
	movq	-232(%rbp), %rdi
	movq	-240(%rbp), %rsi
.L1947:
	leaq	0(,%rdi,8), %rax
	cmpq	$15, %rax
	jbe	.L1946
	movq	%rdi, 8(%rsi)
	movq	$0, (%rsi)
.L1946:
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%r14, -256(%rbp)
	movaps	%xmm0, -224(%rbp)
	movaps	%xmm0, -208(%rbp)
	movaps	%xmm0, -192(%rbp)
	movaps	%xmm0, -176(%rbp)
	movq	$0, -248(%rbp)
	movq	$0, -240(%rbp)
	movq	$0, -232(%rbp)
	call	_ZNSt11_Deque_baseIN2v88internal8compiler18EffectGraphReducer9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	cmpq	$0, -240(%rbp)
	pxor	%xmm0, %xmm0
	je	.L1976
	movdqa	-256(%rbp), %xmm5
	leaq	-160(%rbp), %rdi
	xorl	%esi, %esi
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm5, -160(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	$0, -144(%rbp)
	movq	$0, -136(%rbp)
	call	_ZNSt11_Deque_baseIN2v88internal8compiler18EffectGraphReducer9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	movdqa	-96(%rbp), %xmm5
	movq	-216(%rbp), %r9
	movq	-224(%rbp), %xmm3
	movq	-200(%rbp), %r8
	movq	-192(%rbp), %xmm1
	movq	-184(%rbp), %rcx
	movaps	%xmm5, -192(%rbp)
	movq	%r9, %xmm5
	punpcklqdq	%xmm5, %xmm3
	movdqa	-128(%rbp), %xmm6
	movq	-208(%rbp), %xmm2
	movups	%xmm3, 152(%rbx)
	movq	%r8, %xmm3
	movq	-168(%rbp), %rdx
	movdqa	-112(%rbp), %xmm7
	punpcklqdq	%xmm3, %xmm2
	movq	-248(%rbp), %rax
	movq	-176(%rbp), %xmm0
	movaps	%xmm6, -224(%rbp)
	movups	%xmm2, 168(%rbx)
	movq	%rcx, %xmm2
	movq	-256(%rbp), %xmm4
	movq	-240(%rbp), %r11
	punpcklqdq	%xmm2, %xmm1
	movq	-232(%rbp), %r10
	movdqa	-80(%rbp), %xmm6
	movaps	%xmm7, -208(%rbp)
	movq	-144(%rbp), %rsi
	movq	-136(%rbp), %rdi
	movq	%rax, %xmm7
	movups	%xmm1, 184(%rbx)
	movq	%rdx, %xmm1
	punpcklqdq	%xmm7, %xmm4
	movq	%r11, 136(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movq	%rsi, -240(%rbp)
	movq	%rdi, -232(%rbp)
	movq	%r10, 144(%rbx)
	movaps	%xmm6, -176(%rbp)
	movups	%xmm4, 120(%rbx)
	movups	%xmm0, 200(%rbx)
	testq	%rsi, %rsi
	je	.L1953
	movq	-168(%rbp), %rcx
	movq	-200(%rbp), %rdx
	addq	$8, %rcx
	cmpq	%rdx, %rcx
	jbe	.L1954
	.p2align 4,,10
	.p2align 3
.L1957:
	testq	%rax, %rax
	je	.L1955
	cmpq	$32, 8(%rax)
	ja	.L1956
.L1955:
	movq	(%rdx), %rax
	movq	$32, 8(%rax)
	movq	-248(%rbp), %rsi
	movq	%rsi, (%rax)
	movq	%rax, -248(%rbp)
.L1956:
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	ja	.L1957
	movq	-232(%rbp), %rdi
	movq	-240(%rbp), %rsi
.L1954:
	leaq	0(,%rdi,8), %rax
	cmpq	$15, %rax
	jbe	.L1953
	movq	%rdi, 8(%rsi)
	movq	$0, (%rsi)
.L1953:
	movq	16(%r12), %rax
	movq	240(%rbx), %rdx
	movq	$0, 16(%r12)
	movdqu	(%r12), %xmm0
	movdqu	216(%rbx), %xmm5
	movq	%r13, 248(%rbx)
	movq	%rax, 232(%rbx)
	movq	24(%r12), %rax
	movq	%rdx, 24(%r12)
	movq	%rax, 240(%rbx)
	movups	%xmm5, (%r12)
	movups	%xmm0, 216(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1977
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1976:
	.cfi_restore_state
	movdqa	-256(%rbp), %xmm3
	movq	-232(%rbp), %rax
	movq	$0, 136(%rbx)
	movdqa	-224(%rbp), %xmm6
	movdqa	-208(%rbp), %xmm7
	movdqa	-192(%rbp), %xmm1
	movdqa	-176(%rbp), %xmm2
	movq	%rax, 144(%rbx)
	movups	%xmm3, 120(%rbx)
	movups	%xmm6, 152(%rbx)
	movups	%xmm7, 168(%rbx)
	movups	%xmm1, 184(%rbx)
	movups	%xmm2, 200(%rbx)
	jmp	.L1953
	.p2align 4,,10
	.p2align 3
.L1975:
	movdqa	-256(%rbp), %xmm1
	movdqa	-224(%rbp), %xmm2
	movq	$0, 40(%rbx)
	movq	-232(%rbp), %rax
	movdqa	-208(%rbp), %xmm3
	movups	%xmm1, 24(%rbx)
	movdqa	-192(%rbp), %xmm1
	movups	%xmm2, 56(%rbx)
	movdqa	-176(%rbp), %xmm2
	movq	%rax, 48(%rbx)
	movups	%xmm3, 72(%rbx)
	movups	%xmm1, 88(%rbx)
	movups	%xmm2, 104(%rbx)
	jmp	.L1946
.L1977:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22973:
	.size	_ZN2v88internal8compiler18EffectGraphReducerC2EPNS1_5GraphESt8functionIFvPNS1_4NodeEPNS2_9ReductionEEEPNS0_11TickCounterEPNS0_4ZoneE, .-_ZN2v88internal8compiler18EffectGraphReducerC2EPNS1_5GraphESt8functionIFvPNS1_4NodeEPNS2_9ReductionEEEPNS0_11TickCounterEPNS0_4ZoneE
	.globl	_ZN2v88internal8compiler18EffectGraphReducerC1EPNS1_5GraphESt8functionIFvPNS1_4NodeEPNS2_9ReductionEEEPNS0_11TickCounterEPNS0_4ZoneE
	.set	_ZN2v88internal8compiler18EffectGraphReducerC1EPNS1_5GraphESt8functionIFvPNS1_4NodeEPNS2_9ReductionEEEPNS0_11TickCounterEPNS0_4ZoneE,_ZN2v88internal8compiler18EffectGraphReducerC2EPNS1_5GraphESt8functionIFvPNS1_4NodeEPNS2_9ReductionEEEPNS0_11TickCounterEPNS0_4ZoneE
	.section	.text._ZN2v88internal8compiler14EscapeAnalysisC2EPNS1_7JSGraphEPNS0_11TickCounterEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14EscapeAnalysisC2EPNS1_7JSGraphEPNS0_11TickCounterEPNS0_4ZoneE
	.type	_ZN2v88internal8compiler14EscapeAnalysisC2EPNS1_7JSGraphEPNS0_11TickCounterEPNS0_4ZoneE, @function
_ZN2v88internal8compiler14EscapeAnalysisC2EPNS1_7JSGraphEPNS0_11TickCounterEPNS0_4ZoneE:
.LFB23003:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-96(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal8compiler14EscapeAnalysisC4EPNS3_7JSGraphEPNS2_11TickCounterEPNS2_4ZoneEEUlPNS3_4NodeEPNS3_18EffectGraphReducer9ReductionEE_E10_M_managerERSt9_Any_dataRKSI_St18_Manager_operation(%rip), %rcx
	pushq	%rbx
	movq	%rcx, %xmm0
	movq	%r12, %r8
	movq	%rdx, %rcx
	movq	%r15, %rdx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -104(%rbp)
	movq	(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEPNS2_18EffectGraphReducer9ReductionEEZNS2_14EscapeAnalysisC4EPNS2_7JSGraphEPNS1_11TickCounterEPNS1_4ZoneEEUlS4_S7_E_E9_M_invokeERKSt9_Any_dataOS4_OS7_(%rip), %rax
	movq	%rdi, -96(%rbp)
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler18EffectGraphReducerC2EPNS1_5GraphESt8functionIFvPNS1_4NodeEPNS2_9ReductionEEEPNS0_11TickCounterEPNS0_4ZoneE
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L1979
	movl	$3, %edx
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	*%rax
.L1979:
	movq	16(%r12), %rbx
	movq	24(%r12), %rax
	subq	%rbx, %rax
	cmpq	$287, %rax
	jbe	.L1997
	leaq	288(%rbx), %rax
	movq	%rax, 16(%r12)
.L1981:
	leaq	64(%rbx), %r14
	movq	%r12, 8(%rbx)
	leaq	48(%rbx), %rdi
	movl	$100, %esi
	movq	$0, (%rbx)
	movq	%r14, 16(%rbx)
	movq	$1, 24(%rbx)
	movq	$0, 32(%rbx)
	movq	$0, 40(%rbx)
	movl	$0x3f800000, 48(%rbx)
	movq	$0, 56(%rbx)
	movq	$0, 64(%rbx)
	call	_ZNKSt8__detail20_Prime_rehash_policy11_M_next_bktEm@PLT
	movq	%rax, %r15
	cmpq	24(%rbx), %rax
	jbe	.L1982
	cmpq	$1, %rax
	je	.L1998
	movq	8(%rbx), %rdi
	leaq	0(,%rax,8), %rdx
	movq	%rdx, %rsi
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	subq	%rcx, %rax
	cmpq	%rax, %rdx
	ja	.L1999
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
.L1986:
	xorl	%esi, %esi
	movq	%rcx, %rdi
	movq	%rcx, %r14
	call	memset@PLT
.L1984:
	movq	%r14, 16(%rbx)
	movq	%r15, 24(%rbx)
.L1982:
	movq	-104(%rbp), %rax
	leaq	200(%rbx), %r14
	movq	%r12, 72(%rbx)
	leaq	184(%rbx), %rdi
	movq	$0, 80(%rbx)
	movl	$100, %esi
	movq	$0, 88(%rbx)
	movq	$0, 96(%rbx)
	movq	%r12, 104(%rbx)
	movq	%rax, 112(%rbx)
	movq	$0, 120(%rbx)
	movq	$0, 128(%rbx)
	movq	%r12, 136(%rbx)
	movq	%r12, 144(%rbx)
	movq	%r14, 152(%rbx)
	movq	$1, 160(%rbx)
	movq	$0, 168(%rbx)
	movq	$0, 176(%rbx)
	movl	$0x3f800000, 184(%rbx)
	movq	$0, 192(%rbx)
	movq	$0, 200(%rbx)
	call	_ZNKSt8__detail20_Prime_rehash_policy11_M_next_bktEm@PLT
	movq	%rax, %r15
	cmpq	160(%rbx), %rax
	jbe	.L1987
	cmpq	$1, %rax
	je	.L2000
	movq	144(%rbx), %rdi
	leaq	0(,%rax,8), %rdx
	movq	%rdx, %rsi
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	subq	%rcx, %rax
	cmpq	%rax, %rdx
	ja	.L2001
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
.L1991:
	xorl	%esi, %esi
	movq	%rcx, %rdi
	movq	%rcx, %r14
	call	memset@PLT
.L1989:
	movq	%r14, 152(%rbx)
	movq	%r15, 160(%rbx)
.L1987:
	movq	-104(%rbp), %xmm0
	movq	%r12, %xmm2
	movq	%r12, 208(%rbx)
	movq	$0, 216(%rbx)
	movq	$0, 224(%rbx)
	punpcklqdq	%xmm2, %xmm0
	movq	$0, 232(%rbx)
	movq	248(%r13), %rax
	movq	%r13, 240(%rbx)
	movups	%xmm0, 272(%rbx)
	movq	%rbx, %xmm0
	movl	$0, 248(%rbx)
	movhps	-104(%rbp), %xmm0
	movq	%rax, 256(%rbx)
	movl	$0, 264(%rbx)
	movups	%xmm0, 256(%r13)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2002
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2000:
	.cfi_restore_state
	movq	$0, 200(%rbx)
	jmp	.L1989
	.p2align 4,,10
	.p2align 3
.L1998:
	movq	$0, 64(%rbx)
	jmp	.L1984
	.p2align 4,,10
	.p2align 3
.L1997:
	movl	$288, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rbx
	jmp	.L1981
	.p2align 4,,10
	.p2align 3
.L1999:
	movq	%rdx, -112(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-112(%rbp), %rdx
	movq	%rax, %rcx
	jmp	.L1986
	.p2align 4,,10
	.p2align 3
.L2001:
	movq	%rdx, -112(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-112(%rbp), %rdx
	movq	%rax, %rcx
	jmp	.L1991
.L2002:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23003:
	.size	_ZN2v88internal8compiler14EscapeAnalysisC2EPNS1_7JSGraphEPNS0_11TickCounterEPNS0_4ZoneE, .-_ZN2v88internal8compiler14EscapeAnalysisC2EPNS1_7JSGraphEPNS0_11TickCounterEPNS0_4ZoneE
	.globl	_ZN2v88internal8compiler14EscapeAnalysisC1EPNS1_7JSGraphEPNS0_11TickCounterEPNS0_4ZoneE
	.set	_ZN2v88internal8compiler14EscapeAnalysisC1EPNS1_7JSGraphEPNS0_11TickCounterEPNS0_4ZoneE,_ZN2v88internal8compiler14EscapeAnalysisC2EPNS1_7JSGraphEPNS0_11TickCounterEPNS0_4ZoneE
	.section	.text._ZNSt10_HashtableIjSt4pairIKjPN2v88internal8compiler13VirtualObjectEENS3_13ZoneAllocatorIS7_EENSt8__detail10_Select1stESt8equal_toIjENS2_4base4hashIjEENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS7_Lb1EEEm,"axG",@progbits,_ZNSt10_HashtableIjSt4pairIKjPN2v88internal8compiler13VirtualObjectEENS3_13ZoneAllocatorIS7_EENSt8__detail10_Select1stESt8equal_toIjENS2_4base4hashIjEENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS7_Lb1EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIjSt4pairIKjPN2v88internal8compiler13VirtualObjectEENS3_13ZoneAllocatorIS7_EENSt8__detail10_Select1stESt8equal_toIjENS2_4base4hashIjEENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS7_Lb1EEEm
	.type	_ZNSt10_HashtableIjSt4pairIKjPN2v88internal8compiler13VirtualObjectEENS3_13ZoneAllocatorIS7_EENSt8__detail10_Select1stESt8equal_toIjENS2_4base4hashIjEENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS7_Lb1EEEm, @function
_ZNSt10_HashtableIjSt4pairIKjPN2v88internal8compiler13VirtualObjectEENS3_13ZoneAllocatorIS7_EENSt8__detail10_Select1stESt8equal_toIjENS2_4base4hashIjEENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS7_Lb1EEEm:
.LFB27945:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	movq	%r8, %rcx
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$40, %rdi
	subq	$24, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L2027
.L2004:
	movq	%r14, 24(%r12)
	movq	8(%rbx), %rax
	leaq	0(,%r15,8), %rcx
	movq	(%rax,%r15,8), %rax
	testq	%rax, %rax
	je	.L2013
	movq	(%rax), %rax
	movq	%rax, (%r12)
	movq	8(%rbx), %rax
	movq	(%rax,%r15,8), %rax
	movq	%r12, (%rax)
.L2014:
	addq	$1, 32(%rbx)
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2027:
	.cfi_restore_state
	movq	%rdx, %r13
	cmpq	$1, %rdx
	je	.L2028
	movq	(%rbx), %r8
	leaq	0(,%rdx,8), %rdx
	movq	%rdx, %rsi
	movq	16(%r8), %rdi
	movq	24(%r8), %rax
	subq	%rdi, %rax
	cmpq	%rax, %rdx
	ja	.L2029
	addq	%rdi, %rsi
	movq	%rsi, 16(%r8)
.L2008:
	xorl	%esi, %esi
	call	memset@PLT
	movq	%rax, %rdi
.L2006:
	movq	24(%rbx), %rsi
	movq	$0, 24(%rbx)
	testq	%rsi, %rsi
	je	.L2009
	xorl	%r9d, %r9d
	leaq	24(%rbx), %r10
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2011:
	movq	(%r8), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L2012:
	testq	%rsi, %rsi
	je	.L2009
.L2010:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	24(%rcx), %rax
	divq	%r13
	leaq	(%rdi,%rdx,8), %rax
	movq	(%rax), %r8
	testq	%r8, %r8
	jne	.L2011
	movq	24(%rbx), %r8
	movq	%r8, (%rcx)
	movq	%rcx, 24(%rbx)
	movq	%r10, (%rax)
	cmpq	$0, (%rcx)
	je	.L2016
	movq	%rcx, (%rdi,%r9,8)
	movq	%rdx, %r9
	testq	%rsi, %rsi
	jne	.L2010
	.p2align 4,,10
	.p2align 3
.L2009:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%r13, 16(%rbx)
	divq	%r13
	movq	%rdi, 8(%rbx)
	movq	%rdx, %r15
	jmp	.L2004
	.p2align 4,,10
	.p2align 3
.L2013:
	movq	24(%rbx), %rax
	movq	%rax, (%r12)
	movq	%r12, 24(%rbx)
	movq	(%r12), %rax
	testq	%rax, %rax
	je	.L2015
	movq	24(%rax), %rax
	xorl	%edx, %edx
	divq	16(%rbx)
	movq	8(%rbx), %rax
	movq	%r12, (%rax,%rdx,8)
.L2015:
	movq	8(%rbx), %rax
	leaq	24(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L2014
	.p2align 4,,10
	.p2align 3
.L2016:
	movq	%rdx, %r9
	jmp	.L2012
	.p2align 4,,10
	.p2align 3
.L2028:
	movq	$0, 56(%rbx)
	leaq	56(%rbx), %rdi
	jmp	.L2006
	.p2align 4,,10
	.p2align 3
.L2029:
	movq	%r8, %rdi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %rdi
	jmp	.L2008
	.cfi_endproc
.LFE27945:
	.size	_ZNSt10_HashtableIjSt4pairIKjPN2v88internal8compiler13VirtualObjectEENS3_13ZoneAllocatorIS7_EENSt8__detail10_Select1stESt8equal_toIjENS2_4base4hashIjEENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS7_Lb1EEEm, .-_ZNSt10_HashtableIjSt4pairIKjPN2v88internal8compiler13VirtualObjectEENS3_13ZoneAllocatorIS7_EENSt8__detail10_Select1stESt8equal_toIjENS2_4base4hashIjEENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS7_Lb1EEEm
	.section	.text._ZNSt10_HashtableIjSt4pairIKjN2v88internal8compiler15VariableTracker5StateEENS3_13ZoneAllocatorIS7_EENSt8__detail10_Select1stESt8equal_toIjENS2_4base4hashIjEENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS7_Lb1EEEm,"axG",@progbits,_ZNSt10_HashtableIjSt4pairIKjN2v88internal8compiler15VariableTracker5StateEENS3_13ZoneAllocatorIS7_EENSt8__detail10_Select1stESt8equal_toIjENS2_4base4hashIjEENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS7_Lb1EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIjSt4pairIKjN2v88internal8compiler15VariableTracker5StateEENS3_13ZoneAllocatorIS7_EENSt8__detail10_Select1stESt8equal_toIjENS2_4base4hashIjEENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS7_Lb1EEEm
	.type	_ZNSt10_HashtableIjSt4pairIKjN2v88internal8compiler15VariableTracker5StateEENS3_13ZoneAllocatorIS7_EENSt8__detail10_Select1stESt8equal_toIjENS2_4base4hashIjEENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS7_Lb1EEEm, @function
_ZNSt10_HashtableIjSt4pairIKjN2v88internal8compiler15VariableTracker5StateEENS3_13ZoneAllocatorIS7_EENSt8__detail10_Select1stESt8equal_toIjENS2_4base4hashIjEENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS7_Lb1EEEm:
.LFB27998:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	movq	%r8, %rcx
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$40, %rdi
	subq	$24, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L2054
.L2031:
	movq	%r14, 40(%r12)
	movq	8(%rbx), %rax
	leaq	0(,%r15,8), %rcx
	movq	(%rax,%r15,8), %rax
	testq	%rax, %rax
	je	.L2040
	movq	(%rax), %rax
	movq	%rax, (%r12)
	movq	8(%rbx), %rax
	movq	(%rax,%r15,8), %rax
	movq	%r12, (%rax)
.L2041:
	addq	$1, 32(%rbx)
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2054:
	.cfi_restore_state
	movq	%rdx, %r13
	cmpq	$1, %rdx
	je	.L2055
	movq	(%rbx), %r8
	leaq	0(,%rdx,8), %rdx
	movq	%rdx, %rsi
	movq	16(%r8), %rdi
	movq	24(%r8), %rax
	subq	%rdi, %rax
	cmpq	%rax, %rdx
	ja	.L2056
	addq	%rdi, %rsi
	movq	%rsi, 16(%r8)
.L2035:
	xorl	%esi, %esi
	call	memset@PLT
	movq	%rax, %rdi
.L2033:
	movq	24(%rbx), %rsi
	movq	$0, 24(%rbx)
	testq	%rsi, %rsi
	je	.L2036
	xorl	%r9d, %r9d
	leaq	24(%rbx), %r10
	jmp	.L2037
	.p2align 4,,10
	.p2align 3
.L2038:
	movq	(%r8), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L2039:
	testq	%rsi, %rsi
	je	.L2036
.L2037:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	40(%rcx), %rax
	divq	%r13
	leaq	(%rdi,%rdx,8), %rax
	movq	(%rax), %r8
	testq	%r8, %r8
	jne	.L2038
	movq	24(%rbx), %r8
	movq	%r8, (%rcx)
	movq	%rcx, 24(%rbx)
	movq	%r10, (%rax)
	cmpq	$0, (%rcx)
	je	.L2043
	movq	%rcx, (%rdi,%r9,8)
	movq	%rdx, %r9
	testq	%rsi, %rsi
	jne	.L2037
	.p2align 4,,10
	.p2align 3
.L2036:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%r13, 16(%rbx)
	divq	%r13
	movq	%rdi, 8(%rbx)
	movq	%rdx, %r15
	jmp	.L2031
	.p2align 4,,10
	.p2align 3
.L2040:
	movq	24(%rbx), %rax
	movq	%rax, (%r12)
	movq	%r12, 24(%rbx)
	movq	(%r12), %rax
	testq	%rax, %rax
	je	.L2042
	movq	40(%rax), %rax
	xorl	%edx, %edx
	divq	16(%rbx)
	movq	8(%rbx), %rax
	movq	%r12, (%rax,%rdx,8)
.L2042:
	movq	8(%rbx), %rax
	leaq	24(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L2041
	.p2align 4,,10
	.p2align 3
.L2043:
	movq	%rdx, %r9
	jmp	.L2039
	.p2align 4,,10
	.p2align 3
.L2055:
	movq	$0, 56(%rbx)
	leaq	56(%rbx), %rdi
	jmp	.L2033
	.p2align 4,,10
	.p2align 3
.L2056:
	movq	%r8, %rdi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %rdi
	jmp	.L2035
	.cfi_endproc
.LFE27998:
	.size	_ZNSt10_HashtableIjSt4pairIKjN2v88internal8compiler15VariableTracker5StateEENS3_13ZoneAllocatorIS7_EENSt8__detail10_Select1stESt8equal_toIjENS2_4base4hashIjEENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS7_Lb1EEEm, .-_ZNSt10_HashtableIjSt4pairIKjN2v88internal8compiler15VariableTracker5StateEENS3_13ZoneAllocatorIS7_EENSt8__detail10_Select1stESt8equal_toIjENS2_4base4hashIjEENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS7_Lb1EEEm
	.section	.text._ZN2v88internal8compiler15VariableTracker5ScopeD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15VariableTracker5ScopeD2Ev
	.type	_ZN2v88internal8compiler15VariableTracker5ScopeD2Ev, @function
_ZN2v88internal8compiler15VariableTracker5ScopeD2Ev:
.LFB22987:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$80, %rsp
	movq	16(%rdi), %r12
	movq	(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	cmpb	$0, 1(%rax)
	je	.L2110
.L2058:
	movdqu	24(%rbx), %xmm0
	movq	40(%rbx), %rax
	movaps	%xmm0, -112(%rbp)
	movq	%rax, -96(%rbp)
	movl	20(%r13), %ebx
	andl	$16777215, %ebx
	movl	%ebx, %edi
	call	_ZN2v84base10hash_valueEj@PLT
	movq	56(%r12), %r8
	xorl	%edx, %edx
	movq	%rax, %rdi
	divq	%r8
	movq	48(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L2065
	movq	(%rax), %rcx
	movq	40(%rcx), %rsi
	jmp	.L2068
	.p2align 4,,10
	.p2align 3
.L2066:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L2065
	movq	40(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%r8
	cmpq	%rdx, %r9
	jne	.L2065
.L2068:
	cmpq	%rsi, %rdi
	jne	.L2066
	cmpl	8(%rcx), %ebx
	jne	.L2066
	movdqa	-112(%rbp), %xmm1
	movups	%xmm1, 16(%rcx)
	movq	-96(%rbp), %rax
	movq	%rax, 32(%rcx)
.L2057:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2111
	addq	$80, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2065:
	.cfi_restore_state
	movq	16(%r12), %rax
	cmpq	%rax, -112(%rbp)
	je	.L2057
	movq	24(%r12), %rax
	cmpq	%rax, -104(%rbp)
	je	.L2071
.L2075:
	movq	-96(%rbp), %rax
	movq	40(%r12), %rdi
	movl	20(%r13), %r14d
	movdqa	-112(%rbp), %xmm2
	movq	%rax, -56(%rbp)
	movq	16(%rdi), %r13
	movq	24(%rdi), %rax
	andl	$16777215, %r14d
	movups	%xmm2, -72(%rbp)
	subq	%r13, %rax
	cmpq	$47, %rax
	jbe	.L2112
	leaq	48(%r13), %rax
	movq	%rax, 16(%rdi)
.L2076:
	movq	-56(%rbp), %rax
	movdqu	-72(%rbp), %xmm3
	movl	%r14d, 8(%r13)
	movl	%r14d, %edi
	movq	$0, 0(%r13)
	movq	%rax, 32(%r13)
	movups	%xmm3, 16(%r13)
	call	_ZN2v84base10hash_valueEj@PLT
	movq	56(%r12), %rdi
	xorl	%edx, %edx
	movq	%rax, %r9
	divq	%rdi
	movq	48(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r10
	testq	%rax, %rax
	je	.L2077
	movq	(%rax), %rcx
	movq	40(%rcx), %rsi
	jmp	.L2080
	.p2align 4,,10
	.p2align 3
.L2078:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L2077
	movq	40(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r10
	jne	.L2077
.L2080:
	cmpq	%rsi, %r9
	jne	.L2078
	movl	8(%rcx), %eax
	cmpl	%eax, 8(%r13)
	jne	.L2078
	jmp	.L2057
	.p2align 4,,10
	.p2align 3
.L2110:
	movl	20(%r13), %r13d
	andl	$16777215, %r13d
	movl	%r13d, %edi
	call	_ZN2v84base10hash_valueEj@PLT
	movq	56(%r12), %rsi
	xorl	%edx, %edx
	movq	%rax, %r8
	divq	%rsi
	movq	48(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L2059
	movq	(%rax), %rdi
	movq	40(%rdi), %rcx
	jmp	.L2062
	.p2align 4,,10
	.p2align 3
.L2060:
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L2059
	movq	40(%rdi), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%rsi
	cmpq	%rdx, %r9
	jne	.L2059
.L2062:
	cmpq	%rcx, %r8
	jne	.L2060
	cmpl	8(%rdi), %r13d
	jne	.L2060
	addq	$16, %rdi
.L2081:
	movq	24(%rbx), %rax
	cmpq	%rax, (%rdi)
	je	.L2109
	movq	32(%rbx), %rax
	cmpq	%rax, 8(%rdi)
	je	.L2113
.L2064:
	movq	8(%rbx), %rax
	movb	$1, 1(%rax)
.L2109:
	movq	16(%rbx), %r12
	movq	(%rbx), %r13
	jmp	.L2058
	.p2align 4,,10
	.p2align 3
.L2113:
	leaq	24(%rbx), %rsi
	call	_ZNK2v88internal8compiler13PersistentMapINS1_8VariableEPNS1_4NodeENS_4base4hashIS3_EEEeqERKS9_.part.0
	testb	%al, %al
	je	.L2064
	jmp	.L2109
	.p2align 4,,10
	.p2align 3
.L2071:
	leaq	16(%r12), %rsi
	leaq	-112(%rbp), %rdi
	call	_ZNK2v88internal8compiler13PersistentMapINS1_8VariableEPNS1_4NodeENS_4base4hashIS3_EEEeqERKS9_.part.0
	testb	%al, %al
	jne	.L2057
	jmp	.L2075
	.p2align 4,,10
	.p2align 3
.L2059:
	leaq	16(%r12), %rdi
	jmp	.L2081
	.p2align 4,,10
	.p2align 3
.L2077:
	leaq	40(%r12), %rdi
	movq	%r13, %rcx
	movq	%r9, %rdx
	movq	%r10, %rsi
	movl	$1, %r8d
	call	_ZNSt10_HashtableIjSt4pairIKjN2v88internal8compiler15VariableTracker5StateEENS3_13ZoneAllocatorIS7_EENSt8__detail10_Select1stESt8equal_toIjENS2_4base4hashIjEENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS7_Lb1EEEm
	jmp	.L2057
	.p2align 4,,10
	.p2align 3
.L2112:
	movl	$48, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r13
	jmp	.L2076
.L2111:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22987:
	.size	_ZN2v88internal8compiler15VariableTracker5ScopeD2Ev, .-_ZN2v88internal8compiler15VariableTracker5ScopeD2Ev
	.globl	_ZN2v88internal8compiler15VariableTracker5ScopeD1Ev
	.set	_ZN2v88internal8compiler15VariableTracker5ScopeD1Ev,_ZN2v88internal8compiler15VariableTracker5ScopeD2Ev
	.section	.text._ZN2v88internal8compiler14EscapeAnalysis6ReduceEPNS1_4NodeEPNS1_18EffectGraphReducer9ReductionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14EscapeAnalysis6ReduceEPNS1_4NodeEPNS1_18EffectGraphReducer9ReductionE
	.type	_ZN2v88internal8compiler14EscapeAnalysis6ReduceEPNS1_4NodeEPNS1_18EffectGraphReducer9ReductionE, @function
_ZN2v88internal8compiler14EscapeAnalysis6ReduceEPNS1_4NodeEPNS1_18EffectGraphReducer9ReductionE:
.LFB22997:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	movq	%rdi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	256(%r12), %rbx
	movq	(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -120(%rbp)
	movq	104(%rbx), %rax
	leaq	104(%rbx), %rsi
	movq	$0, -112(%rbp)
	movq	%rsi, -128(%rbp)
	movq	%rax, -104(%rbp)
	cmpw	$36, 16(%r13)
	movaps	%xmm0, -144(%rbp)
	je	.L2176
	cmpl	$1, 24(%r13)
	je	.L2177
.L2116:
	movq	%r12, %xmm2
	movq	%rbx, %xmm0
	movq	264(%r12), %rdx
	leaq	-144(%rbp), %r12
	punpcklqdq	%xmm2, %xmm0
	movq	%r12, %rsi
	movq	%r13, %rdi
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler12_GLOBAL__N_110ReduceNodeEPKNS1_8OperatorEPNS1_21EscapeAnalysisTracker5ScopeEPNS1_7JSGraphE.constprop.0
	movq	-96(%rbp), %rbx
	movq	-144(%rbp), %rax
	movq	-72(%rbp), %r14
	movq	88(%rbx), %rcx
	movl	20(%rax), %esi
	movq	80(%rbx), %rdx
	movq	%rcx, %rax
	andl	$16777215, %esi
	subq	%rdx, %rax
	movl	%esi, %r13d
	sarq	$3, %rax
	cmpq	%rax, %r13
	jnb	.L2178
.L2121:
	cmpq	(%rdx,%r13,8), %r14
	je	.L2123
.L2129:
	movq	-136(%rbp), %rax
	movb	$1, (%rax)
.L2124:
	movq	-96(%rbp), %r13
	movq	-144(%rbp), %rax
	movq	88(%r13), %rcx
	movl	20(%rax), %esi
	movq	80(%r13), %rdx
	movq	%rcx, %rax
	andl	$16777215, %esi
	subq	%rdx, %rax
	movl	%esi, %ebx
	sarq	$3, %rax
	cmpq	%rax, %rbx
	jnb	.L2179
.L2130:
	movq	-72(%rbp), %rax
	movq	%rax, (%rdx,%rbx,8)
	movq	-144(%rbp), %r15
	movq	-96(%rbp), %rbx
	movq	-80(%rbp), %r14
	movl	20(%r15), %r13d
	andl	$16777215, %r13d
	movl	%r13d, %edi
	call	_ZN2v84base10hash_valueEj@PLT
	movq	24(%rbx), %r8
	xorl	%edx, %edx
	movq	%rax, %rdi
	divq	%r8
	movq	16(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L2132
	movq	(%rax), %rcx
	movq	24(%rcx), %rsi
	jmp	.L2135
	.p2align 4,,10
	.p2align 3
.L2133:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L2132
	movq	24(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%r8
	cmpq	%rdx, %r9
	jne	.L2132
.L2135:
	cmpq	%rsi, %rdi
	jne	.L2133
	cmpl	8(%rcx), %r13d
	jne	.L2133
	movq	%r14, 16(%rcx)
.L2136:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler15VariableTracker5ScopeD2Ev
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2180
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2132:
	.cfi_restore_state
	cmpq	(%rbx), %r14
	je	.L2136
	movq	8(%rbx), %rdi
	movl	20(%r15), %r15d
	movq	16(%rdi), %r13
	movq	24(%rdi), %rax
	andl	$16777215, %r15d
	subq	%r13, %rax
	cmpq	$31, %rax
	jbe	.L2181
	leaq	32(%r13), %rax
	movq	%rax, 16(%rdi)
.L2138:
	movq	$0, 0(%r13)
	movl	%r15d, %edi
	movl	%r15d, 8(%r13)
	movq	%r14, 16(%r13)
	call	_ZN2v84base10hash_valueEj@PLT
	movq	24(%rbx), %rdi
	xorl	%edx, %edx
	movq	%rax, %r9
	divq	%rdi
	movq	16(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r10
	testq	%rax, %rax
	je	.L2139
	movq	(%rax), %rcx
	movq	24(%rcx), %rsi
	jmp	.L2142
	.p2align 4,,10
	.p2align 3
.L2140:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L2139
	movq	24(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r10
	jne	.L2139
.L2142:
	cmpq	%rsi, %r9
	jne	.L2140
	movl	8(%rcx), %eax
	cmpl	%eax, 8(%r13)
	jne	.L2140
	jmp	.L2136
	.p2align 4,,10
	.p2align 3
.L2179:
	addl	$1, %esi
	cmpq	%rsi, %rax
	jb	.L2182
	jbe	.L2130
	leaq	(%rdx,%rsi,8), %rax
	cmpq	%rax, %rcx
	je	.L2130
	movq	%rax, 88(%r13)
	jmp	.L2130
	.p2align 4,,10
	.p2align 3
.L2178:
	addl	$1, %esi
	cmpq	%rsi, %rax
	jb	.L2183
	jbe	.L2121
	leaq	(%rdx,%rsi,8), %rax
	cmpq	%rax, %rcx
	je	.L2121
	movq	%rax, 88(%rbx)
	jmp	.L2121
	.p2align 4,,10
	.p2align 3
.L2176:
	leaq	-176(%rbp), %r8
	movq	%rdi, %rdx
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler15VariableTracker11MergeInputsEPNS1_4NodeE
	movdqu	-176(%rbp), %xmm3
	movq	-160(%rbp), %rax
	movups	%xmm3, -120(%rbp)
	movq	%rax, -104(%rbp)
	jmp	.L2116
	.p2align 4,,10
	.p2align 3
.L2123:
	movq	-144(%rbp), %rax
	movq	-96(%rbp), %r14
	movq	-80(%rbp), %rbx
	movl	20(%rax), %r13d
	andl	$16777215, %r13d
	movl	%r13d, %edi
	call	_ZN2v84base10hash_valueEj@PLT
	movq	24(%r14), %rdi
	xorl	%edx, %edx
	movq	%rax, %r8
	divq	%rdi
	movq	16(%r14), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L2125
	movq	(%rax), %rcx
	movq	24(%rcx), %rsi
	jmp	.L2128
	.p2align 4,,10
	.p2align 3
.L2126:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L2125
	movq	24(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r9
	jne	.L2125
.L2128:
	cmpq	%rsi, %r8
	jne	.L2126
	cmpl	8(%rcx), %r13d
	jne	.L2126
	movq	16(%rcx), %rax
.L2143:
	cmpq	%rax, %rbx
	jne	.L2129
	jmp	.L2124
	.p2align 4,,10
	.p2align 3
.L2177:
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movl	20(%rax), %r14d
	andl	$16777215, %r14d
	movl	%r14d, %edi
	call	_ZN2v84base10hash_valueEj@PLT
	movq	160(%rbx), %rdi
	xorl	%edx, %edx
	movq	%rax, %r8
	divq	%rdi
	movq	152(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L2117
	movq	(%rax), %rcx
	movq	40(%rcx), %rsi
	jmp	.L2120
	.p2align 4,,10
	.p2align 3
.L2118:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L2117
	movq	40(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r9
	jne	.L2117
.L2120:
	cmpq	%r8, %rsi
	jne	.L2118
	cmpl	8(%rcx), %r14d
	jne	.L2118
	addq	$16, %rcx
.L2144:
	movdqu	(%rcx), %xmm4
	movups	%xmm4, -120(%rbp)
	movq	16(%rcx), %rax
	movq	%rax, -104(%rbp)
	jmp	.L2116
	.p2align 4,,10
	.p2align 3
.L2183:
	subq	%rax, %rsi
	leaq	72(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm
	movq	80(%rbx), %rdx
	jmp	.L2121
	.p2align 4,,10
	.p2align 3
.L2182:
	subq	%rax, %rsi
	leaq	72(%r13), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm
	movq	80(%r13), %rdx
	jmp	.L2130
	.p2align 4,,10
	.p2align 3
.L2125:
	movq	(%r14), %rax
	jmp	.L2143
	.p2align 4,,10
	.p2align 3
.L2117:
	leaq	120(%rbx), %rcx
	jmp	.L2144
	.p2align 4,,10
	.p2align 3
.L2139:
	leaq	8(%rbx), %rdi
	movl	$1, %r8d
	movq	%r13, %rcx
	movq	%r9, %rdx
	movq	%r10, %rsi
	call	_ZNSt10_HashtableIjSt4pairIKjPN2v88internal8compiler13VirtualObjectEENS3_13ZoneAllocatorIS7_EENSt8__detail10_Select1stESt8equal_toIjENS2_4base4hashIjEENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS7_Lb1EEEm
	jmp	.L2136
	.p2align 4,,10
	.p2align 3
.L2181:
	movl	$32, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r13
	jmp	.L2138
.L2180:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22997:
	.size	_ZN2v88internal8compiler14EscapeAnalysis6ReduceEPNS1_4NodeEPNS1_18EffectGraphReducer9ReductionE, .-_ZN2v88internal8compiler14EscapeAnalysis6ReduceEPNS1_4NodeEPNS1_18EffectGraphReducer9ReductionE
	.section	.text._ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEPNS2_18EffectGraphReducer9ReductionEEZNS2_14EscapeAnalysisC4EPNS2_7JSGraphEPNS1_11TickCounterEPNS1_4ZoneEEUlS4_S7_E_E9_M_invokeERKSt9_Any_dataOS4_OS7_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEPNS2_18EffectGraphReducer9ReductionEEZNS2_14EscapeAnalysisC4EPNS2_7JSGraphEPNS1_11TickCounterEPNS1_4ZoneEEUlS4_S7_E_E9_M_invokeERKSt9_Any_dataOS4_OS7_, @function
_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEPNS2_18EffectGraphReducer9ReductionEEZNS2_14EscapeAnalysisC4EPNS2_7JSGraphEPNS1_11TickCounterEPNS1_4ZoneEEUlS4_S7_E_E9_M_invokeERKSt9_Any_dataOS4_OS7_:
.LFB26431:
	.cfi_startproc
	endbr64
	movq	(%rdx), %rdx
	movq	(%rsi), %rsi
	movq	(%rdi), %rdi
	jmp	_ZN2v88internal8compiler14EscapeAnalysis6ReduceEPNS1_4NodeEPNS1_18EffectGraphReducer9ReductionE
	.cfi_endproc
.LFE26431:
	.size	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEPNS2_18EffectGraphReducer9ReductionEEZNS2_14EscapeAnalysisC4EPNS2_7JSGraphEPNS1_11TickCounterEPNS1_4ZoneEEUlS4_S7_E_E9_M_invokeERKSt9_Any_dataOS4_OS7_, .-_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEPNS2_18EffectGraphReducer9ReductionEEZNS2_14EscapeAnalysisC4EPNS2_7JSGraphEPNS1_11TickCounterEPNS1_4ZoneEEUlS4_S7_E_E9_M_invokeERKSt9_Any_dataOS4_OS7_
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler18EffectGraphReducerC2EPNS1_5GraphESt8functionIFvPNS1_4NodeEPNS2_9ReductionEEEPNS0_11TickCounterEPNS0_4ZoneE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler18EffectGraphReducerC2EPNS1_5GraphESt8functionIFvPNS1_4NodeEPNS2_9ReductionEEEPNS0_11TickCounterEPNS0_4ZoneE, @function
_GLOBAL__sub_I__ZN2v88internal8compiler18EffectGraphReducerC2EPNS1_5GraphESt8functionIFvPNS1_4NodeEPNS2_9ReductionEEEPNS0_11TickCounterEPNS0_4ZoneE:
.LFB29069:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE29069:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler18EffectGraphReducerC2EPNS1_5GraphESt8functionIFvPNS1_4NodeEPNS2_9ReductionEEEPNS0_11TickCounterEPNS0_4ZoneE, .-_GLOBAL__sub_I__ZN2v88internal8compiler18EffectGraphReducerC2EPNS1_5GraphESt8functionIFvPNS1_4NodeEPNS2_9ReductionEEEPNS0_11TickCounterEPNS0_4ZoneE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler18EffectGraphReducerC2EPNS1_5GraphESt8functionIFvPNS1_4NodeEPNS2_9ReductionEEEPNS0_11TickCounterEPNS0_4ZoneE
	.section	.rodata.CSWTCH.372,"a"
	.align 32
	.type	CSWTCH.372, @object
	.size	CSWTCH.372, 56
CSWTCH.372:
	.long	0
	.long	0
	.long	1
	.long	2
	.long	3
	.long	3
	.long	3
	.long	3
	.long	3
	.long	3
	.long	3
	.long	2
	.long	3
	.long	4
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata._ZStL19piecewise_construct,"a"
	.type	_ZStL19piecewise_construct, @object
	.size	_ZStL19piecewise_construct, 1
_ZStL19piecewise_construct:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
