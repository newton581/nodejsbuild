	.file	"jump-threading.cc"
	.text
	.section	.rodata._ZN2v88internal8compiler13JumpThreading15ApplyForwardingEPNS0_4ZoneERNS0_10ZoneVectorINS1_9RpoNumberEEEPNS1_19InstructionSequenceE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"jt-fw nop @%d\n"
	.section	.text._ZN2v88internal8compiler13JumpThreading15ApplyForwardingEPNS0_4ZoneERNS0_10ZoneVectorINS1_9RpoNumberEEEPNS1_19InstructionSequenceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13JumpThreading15ApplyForwardingEPNS0_4ZoneERNS0_10ZoneVectorINS1_9RpoNumberEEEPNS1_19InstructionSequenceE
	.type	_ZN2v88internal8compiler13JumpThreading15ApplyForwardingEPNS0_4ZoneERNS0_10ZoneVectorINS1_9RpoNumberEEEPNS1_19InstructionSequenceE, @function
_ZN2v88internal8compiler13JumpThreading15ApplyForwardingEPNS0_4ZoneERNS0_10ZoneVectorINS1_9RpoNumberEEEPNS1_19InstructionSequenceE:
.LFB14991:
	.cfi_startproc
	endbr64
	movzbl	_ZN2v88internal13FLAG_turbo_jtE(%rip), %r10d
	testb	%r10b, %r10b
	jne	.L50
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r9
	xorl	%r8d, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rsi), %rax
	subq	8(%rsi), %rax
	sarq	$2, %rax
	cltq
	testq	%rax, %rax
	jne	.L51
.L6:
	movq	16(%r13), %rax
	movl	%r10d, %esi
	movq	16(%rax), %rbx
	movq	8(%rax), %rdx
	movq	%rbx, -56(%rbp)
	cmpq	%rbx, %rdx
	je	.L22
	.p2align 4,,10
	.p2align 3
.L21:
	movq	(%rdx), %r14
	movl	$1, %r11d
	movslq	100(%r14), %rdi
	movq	%rdi, %rax
	movq	%rdi, %rcx
	shrq	$6, %rax
	salq	%cl, %r11
	leaq	(%r8,%rax,8), %r15
	movq	(%r15), %rax
	testb	%sil, %sil
	jne	.L11
	movq	8(%r9), %rsi
	cmpl	(%rsi,%rdi,4), %edi
	je	.L11
	orq	%r11, %rax
	movq	%rax, (%r15)
	movl	112(%r14), %r12d
	cmpl	116(%r14), %r12d
	jge	.L30
.L55:
	movslq	%r12d, %rbx
	movl	%r10d, %esi
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L53:
	cmpq	$63, %rax
	jg	.L15
	leaq	(%rcx,%rbx,8), %rax
.L16:
	movq	(%rax), %rcx
	movl	(%rcx), %edi
	movl	%edi, %eax
	shrl	$14, %eax
	andl	$7, %eax
	subl	$1, %eax
	cmpl	$1, %eax
	jbe	.L31
	andl	$511, %edi
	cmpl	$13, %edi
	je	.L52
.L18:
	addl	$1, %r12d
	addq	$1, %rbx
	cmpl	116(%r14), %r12d
	jge	.L13
.L20:
	movq	208(%r13), %rcx
	movq	%rcx, %rax
	subq	216(%r13), %rax
	sarq	$3, %rax
	addq	%rbx, %rax
	jns	.L53
	movq	%rax, %rcx
	notq	%rcx
	shrq	$6, %rcx
	notq	%rcx
.L17:
	movq	%rcx, %rdi
	salq	$6, %rdi
	subq	%rdi, %rax
	movq	232(%r13), %rdi
	movq	(%rdi,%rcx,8), %rcx
	leaq	(%rcx,%rax,8), %rax
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L15:
	movq	%rax, %rcx
	sarq	$6, %rcx
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L31:
	xorl	%esi, %esi
	addl	$1, %r12d
	addq	$1, %rbx
	cmpl	116(%r14), %r12d
	jl	.L20
.L13:
	addq	$8, %rdx
	cmpq	%rdx, -56(%rbp)
	jne	.L21
.L22:
	movq	152(%r13), %rcx
	movq	160(%r13), %rsi
	xorl	%edx, %edx
	cmpq	%rcx, %rsi
	jne	.L8
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L23:
	movq	%rsi, %rax
	addq	$1, %rdx
	subq	%rcx, %rax
	sarq	$4, %rax
	cmpq	%rax, %rdx
	jnb	.L24
.L8:
	movq	%rdx, %rax
	salq	$4, %rax
	addq	%rcx, %rax
	cmpl	$7, (%rax)
	jne	.L23
	movq	8(%rax), %r10
	movq	8(%r9), %rdi
	movslq	%r10d, %r11
	movslq	(%rdi,%r11,4), %rdi
	cmpl	%edi, %r10d
	je	.L23
	movb	$19, 4(%rax)
	movq	%rdi, 8(%rax)
	movq	152(%r13), %rcx
	movq	160(%r13), %rsi
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L52:
	xorl	%esi, %esi
	testq	%r11, (%r15)
	je	.L18
	cmpb	$0, _ZN2v88internal19FLAG_trace_turbo_jtE(%rip)
	jne	.L54
.L19:
	movq	$17, (%rcx)
	xorl	%esi, %esi
	movq	$0, 24(%rcx)
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L11:
	movq	%r11, %rcx
	notq	%rcx
	andq	%rcx, %rax
	movq	%rax, (%r15)
	movl	112(%r14), %r12d
	cmpl	116(%r14), %r12d
	jl	.L55
.L30:
	movl	%r10d, %esi
	addq	$8, %rdx
	cmpq	%rdx, -56(%rbp)
	jne	.L21
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L54:
	movl	%r12d, %esi
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%r9, -104(%rbp)
	movq	%r11, -96(%rbp)
	movq	%rdx, -88(%rbp)
	movq	%r8, -80(%rbp)
	movb	%r10b, -72(%rbp)
	movq	%rcx, -64(%rbp)
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-104(%rbp), %r9
	movq	-96(%rbp), %r11
	movq	-88(%rbp), %rdx
	movq	-80(%rbp), %r8
	movzbl	-72(%rbp), %r10d
	movq	-64(%rbp), %rcx
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L24:
	movq	24(%r13), %rax
	xorl	%edi, %edi
	movl	$1, %r10d
	movq	16(%rax), %r9
	movq	8(%rax), %rax
	cmpq	%r9, %rax
	je	.L1
	.p2align 4,,10
	.p2align 3
.L27:
	movq	(%rax), %rdx
	movslq	100(%rdx), %rsi
	movl	%edi, 96(%rdx)
	movq	%r10, %rdx
	movq	%rsi, %rcx
	shrq	$6, %rsi
	salq	%cl, %rdx
	andq	(%r8,%rsi,8), %rdx
	cmpq	$1, %rdx
	adcl	$0, %edi
	addq	$8, %rax
	cmpq	%rax, %r9
	jne	.L27
.L1:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L51:
	.cfi_restore_state
	addq	$63, %rax
	movq	16(%rdi), %r8
	shrq	$6, %rax
	leaq	0(,%rax,8), %rdx
	movq	24(%rdi), %rax
	movq	%rdx, %rsi
	subq	%r8, %rax
	cmpq	%rax, %rdx
	ja	.L56
	addq	%r8, %rsi
	movq	%rsi, 16(%rdi)
.L5:
	testq	%r8, %r8
	je	.L6
	movq	%r8, %rdi
	xorl	%esi, %esi
	movq	%r9, -64(%rbp)
	movb	%r10b, -56(%rbp)
	call	memset@PLT
	movq	-64(%rbp), %r9
	movzbl	-56(%rbp), %r10d
	movq	%rax, %r8
	jmp	.L6
.L56:
	movq	%r9, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movb	%r10b, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movzbl	-56(%rbp), %r10d
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r9
	movq	%rax, %r8
	jmp	.L5
	.cfi_endproc
.LFE14991:
	.size	_ZN2v88internal8compiler13JumpThreading15ApplyForwardingEPNS0_4ZoneERNS0_10ZoneVectorINS1_9RpoNumberEEEPNS1_19InstructionSequenceE, .-_ZN2v88internal8compiler13JumpThreading15ApplyForwardingEPNS0_4ZoneERNS0_10ZoneVectorINS1_9RpoNumberEEEPNS1_19InstructionSequenceE
	.section	.rodata._ZNSt6vectorIN2v88internal8compiler9RpoNumberENS1_13ZoneAllocatorIS3_EEE14_M_fill_assignEmRKS3_.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"cannot create std::vector larger than max_size()"
	.section	.text._ZNSt6vectorIN2v88internal8compiler9RpoNumberENS1_13ZoneAllocatorIS3_EEE14_M_fill_assignEmRKS3_,"axG",@progbits,_ZNSt6vectorIN2v88internal8compiler9RpoNumberENS1_13ZoneAllocatorIS3_EEE14_M_fill_assignEmRKS3_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal8compiler9RpoNumberENS1_13ZoneAllocatorIS3_EEE14_M_fill_assignEmRKS3_
	.type	_ZNSt6vectorIN2v88internal8compiler9RpoNumberENS1_13ZoneAllocatorIS3_EEE14_M_fill_assignEmRKS3_, @function
_ZNSt6vectorIN2v88internal8compiler9RpoNumberENS1_13ZoneAllocatorIS3_EEE14_M_fill_assignEmRKS3_:
.LFB16603:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	8(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	sarq	$2, %rcx
	cmpq	%rsi, %rcx
	jb	.L115
	movq	16(%rdi), %rsi
	movq	%rsi, %rcx
	subq	%rax, %rcx
	sarq	$2, %rcx
	cmpq	%rcx, %rbx
	ja	.L116
	testq	%rbx, %rbx
	je	.L78
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L79:
	movl	(%rdx), %esi
	movl	%esi, (%rax,%rcx,4)
	addq	$1, %rcx
	cmpq	%rcx, %rbx
	jne	.L79
	movq	16(%r12), %rsi
	leaq	(%rax,%rbx,4), %rax
.L78:
	cmpq	%rax, %rsi
	je	.L57
	movq	%rax, 16(%r12)
.L57:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L116:
	.cfi_restore_state
	cmpq	%rax, %rsi
	je	.L70
	.p2align 4,,10
	.p2align 3
.L71:
	movl	(%rdx), %ecx
	addq	$4, %rax
	movl	%ecx, -4(%rax)
	cmpq	%rax, %rsi
	jne	.L71
	movq	16(%r12), %rax
	movq	%rax, %rcx
	subq	8(%r12), %rcx
	sarq	$2, %rcx
.L70:
	subq	%rcx, %rbx
	je	.L81
	leaq	4(%rdx), %rcx
	leaq	(%rax,%rbx,4), %r9
	cmpq	%rcx, %rax
	setnb	%sil
	cmpq	%r9, %rdx
	setnb	%cl
	orb	%cl, %sil
	je	.L82
	leaq	-1(%rbx), %rcx
	cmpq	$3, %rcx
	jbe	.L82
	movd	(%rdx), %xmm2
	leaq	-4(%rbx), %rdi
	xorl	%ecx, %ecx
	shrq	$2, %rdi
	addq	$1, %rdi
	pshufd	$0, %xmm2, %xmm0
	.p2align 4,,10
	.p2align 3
.L75:
	movq	%rcx, %r8
	addq	$1, %rcx
	salq	$4, %r8
	movups	%xmm0, (%rax,%r8)
	cmpq	%rcx, %rdi
	ja	.L75
	leaq	0(,%rdi,4), %rcx
	movq	%rbx, %rsi
	salq	$4, %rdi
	subq	%rcx, %rsi
	addq	%rdi, %rax
	cmpq	%rcx, %rbx
	je	.L72
	movl	(%rdx), %ecx
	movl	%ecx, (%rax)
	cmpq	$1, %rsi
	je	.L72
	movl	(%rdx), %ecx
	movl	%ecx, 4(%rax)
	cmpq	$2, %rsi
	je	.L72
	movl	(%rdx), %edx
	movl	%edx, 8(%rax)
.L72:
	movq	%r9, 16(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L115:
	.cfi_restore_state
	cmpq	$536870911, %rsi
	ja	.L117
	movq	(%rdi), %rdi
	leaq	0(,%rsi,4), %r13
	testq	%rsi, %rsi
	je	.L80
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	leaq	7(%r13), %rsi
	andq	$-8, %rsi
	subq	%rax, %rcx
	cmpq	%rcx, %rsi
	ja	.L118
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L62:
	leaq	4(%rdx), %r8
	leaq	(%rax,%r13), %rcx
	movq	%rax, %rsi
	cmpq	%r8, %rax
	movq	%rcx, %rdi
	setnb	%r9b
	cmpq	%rcx, %rdx
	setnb	%r8b
	orb	%r8b, %r9b
	je	.L67
	leaq	-1(%rbx), %r8
	cmpq	$3, %r8
	jbe	.L67
	movq	%rbx, %rcx
	movd	(%rdx), %xmm3
	shrq	$2, %rcx
	salq	$4, %rcx
	pshufd	$0, %xmm3, %xmm0
	addq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L64:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rcx, %rax
	jne	.L64
	movq	%rbx, %rax
	movq	%rbx, %r8
	andq	$-4, %rax
	andl	$3, %r8d
	leaq	(%rsi,%rax,4), %rcx
	cmpq	%rax, %rbx
	je	.L60
	movl	(%rdx), %eax
	movl	%eax, (%rcx)
	cmpq	$1, %r8
	je	.L60
	movl	(%rdx), %eax
	movl	%eax, 4(%rcx)
	cmpq	$2, %r8
	je	.L60
	movl	(%rdx), %eax
	movl	%eax, 8(%rcx)
.L60:
	movq	%rsi, %xmm0
	movq	%rdi, %xmm1
	movq	%rdi, 24(%r12)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 8(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	.cfi_restore_state
	movl	(%rdx), %r8d
	addq	$4, %rax
	movl	%r8d, -4(%rax)
	cmpq	%rcx, %rax
	je	.L60
	movl	(%rdx), %r8d
	addq	$4, %rax
	movl	%r8d, -4(%rax)
	cmpq	%rcx, %rax
	jne	.L67
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L80:
	xorl	%esi, %esi
	xorl	%edi, %edi
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L82:
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L73:
	movl	(%rdx), %esi
	movl	%esi, (%rax,%rcx,4)
	addq	$1, %rcx
	cmpq	%rcx, %rbx
	jne	.L73
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L81:
	movq	%rax, %r9
	jmp	.L72
.L118:
	movq	%rdx, -40(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-40(%rbp), %rdx
	jmp	.L62
.L117:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE16603:
	.size	_ZNSt6vectorIN2v88internal8compiler9RpoNumberENS1_13ZoneAllocatorIS3_EEE14_M_fill_assignEmRKS3_, .-_ZNSt6vectorIN2v88internal8compiler9RpoNumberENS1_13ZoneAllocatorIS3_EEE14_M_fill_assignEmRKS3_
	.section	.rodata._ZNSt5dequeIN2v88internal8compiler9RpoNumberENS1_22RecyclingZoneAllocatorIS3_EEE16_M_push_back_auxIJRKS3_EEEvDpOT_.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"cannot create std::deque larger than max_size()"
	.section	.text._ZNSt5dequeIN2v88internal8compiler9RpoNumberENS1_22RecyclingZoneAllocatorIS3_EEE16_M_push_back_auxIJRKS3_EEEvDpOT_,"axG",@progbits,_ZNSt5dequeIN2v88internal8compiler9RpoNumberENS1_22RecyclingZoneAllocatorIS3_EEE16_M_push_back_auxIJRKS3_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt5dequeIN2v88internal8compiler9RpoNumberENS1_22RecyclingZoneAllocatorIS3_EEE16_M_push_back_auxIJRKS3_EEEvDpOT_
	.type	_ZNSt5dequeIN2v88internal8compiler9RpoNumberENS1_22RecyclingZoneAllocatorIS3_EEE16_M_push_back_auxIJRKS3_EEEvDpOT_, @function
_ZNSt5dequeIN2v88internal8compiler9RpoNumberENS1_22RecyclingZoneAllocatorIS3_EEE16_M_push_back_auxIJRKS3_EEEvDpOT_:
.LFB17109:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	88(%rdi), %r13
	movq	56(%rdi), %rsi
	movq	64(%rbx), %rdx
	subq	72(%rbx), %rdx
	movq	%r13, %r14
	movq	%rdx, %rcx
	subq	%rsi, %r14
	sarq	$2, %rcx
	movq	%r14, %rdi
	sarq	$3, %rdi
	leaq	-1(%rdi), %rax
	salq	$7, %rax
	leaq	(%rcx,%rax), %rdx
	movq	48(%rbx), %rax
	subq	32(%rbx), %rax
	sarq	$2, %rax
	addq	%rdx, %rax
	cmpq	$536870911, %rax
	je	.L137
	movq	16(%rbx), %rcx
	movq	24(%rbx), %rdx
	movq	%r13, %rax
	subq	%rcx, %rax
	movq	%rdx, %r8
	sarq	$3, %rax
	subq	%rax, %r8
	cmpq	$1, %r8
	jbe	.L138
.L121:
	movq	8(%rbx), %rax
	testq	%rax, %rax
	je	.L129
	cmpq	$127, 8(%rax)
	ja	.L139
.L129:
	movq	(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L140
	leaq	512(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L130:
	movq	%rax, 8(%r13)
	movl	(%r12), %edx
	movq	64(%rbx), %rax
	movl	%edx, (%rax)
	movq	88(%rbx), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 88(%rbx)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 72(%rbx)
	movq	%rdx, 80(%rbx)
	movq	%rax, 64(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L138:
	.cfi_restore_state
	leaq	2(%rdi), %r15
	leaq	(%r15,%r15), %rax
	cmpq	%rax, %rdx
	ja	.L141
	testq	%rdx, %rdx
	movl	$1, %eax
	movq	(%rbx), %rdi
	cmovne	%rdx, %rax
	movq	16(%rdi), %r13
	leaq	2(%rdx,%rax), %rcx
	movq	24(%rdi), %rax
	leaq	0(,%rcx,8), %rsi
	subq	%r13, %rax
	cmpq	%rax, %rsi
	ja	.L142
	addq	%r13, %rsi
	movq	%rsi, 16(%rdi)
.L126:
	movq	%rcx, %rax
	movq	56(%rbx), %rsi
	subq	%r15, %rax
	shrq	%rax
	leaq	0(%r13,%rax,8), %r15
	movq	88(%rbx), %rax
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L127
	subq	%rsi, %rdx
	movq	%r15, %rdi
	movq	%rcx, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %rcx
.L127:
	movq	24(%rbx), %rax
	leaq	0(,%rax,8), %rdx
	cmpq	$15, %rdx
	jbe	.L128
	movq	16(%rbx), %rdx
	movq	%rax, 8(%rdx)
	movq	$0, (%rdx)
.L128:
	movq	%r13, 16(%rbx)
	movq	%rcx, 24(%rbx)
.L124:
	movq	%r15, 56(%rbx)
	movq	(%r15), %rax
	leaq	(%r15,%r14), %r13
	movq	(%r15), %xmm0
	movq	%r13, 88(%rbx)
	addq	$512, %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 40(%rbx)
	movq	0(%r13), %rax
	movq	%rax, 72(%rbx)
	addq	$512, %rax
	movq	%rax, 80(%rbx)
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L139:
	movq	(%rax), %rdx
	movq	%rdx, 8(%rbx)
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L141:
	subq	%r15, %rdx
	addq	$8, %r13
	shrq	%rdx
	leaq	(%rcx,%rdx,8), %r15
	movq	%r13, %rdx
	subq	%rsi, %rdx
	cmpq	%r15, %rsi
	jbe	.L123
	cmpq	%r13, %rsi
	je	.L124
	movq	%r15, %rdi
	call	memmove@PLT
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L123:
	cmpq	%r13, %rsi
	je	.L124
	leaq	8(%r14), %rdi
	subq	%rdx, %rdi
	addq	%r15, %rdi
	call	memmove@PLT
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L140:
	movl	$512, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L142:
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	movq	%rax, %r13
	jmp	.L126
.L137:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE17109:
	.size	_ZNSt5dequeIN2v88internal8compiler9RpoNumberENS1_22RecyclingZoneAllocatorIS3_EEE16_M_push_back_auxIJRKS3_EEEvDpOT_, .-_ZNSt5dequeIN2v88internal8compiler9RpoNumberENS1_22RecyclingZoneAllocatorIS3_EEE16_M_push_back_auxIJRKS3_EEEvDpOT_
	.section	.text._ZNSt11_Deque_baseIN2v88internal8compiler9RpoNumberENS1_22RecyclingZoneAllocatorIS3_EEE17_M_initialize_mapEm,"axG",@progbits,_ZNSt11_Deque_baseIN2v88internal8compiler9RpoNumberENS1_22RecyclingZoneAllocatorIS3_EEE17_M_initialize_mapEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt11_Deque_baseIN2v88internal8compiler9RpoNumberENS1_22RecyclingZoneAllocatorIS3_EEE17_M_initialize_mapEm
	.type	_ZNSt11_Deque_baseIN2v88internal8compiler9RpoNumberENS1_22RecyclingZoneAllocatorIS3_EEE17_M_initialize_mapEm, @function
_ZNSt11_Deque_baseIN2v88internal8compiler9RpoNumberENS1_22RecyclingZoneAllocatorIS3_EEE17_M_initialize_mapEm:
.LFB17665:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$8, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	shrq	$7, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	1(%rsi), %rbx
	addq	$3, %rsi
	subq	$8, %rsp
	cmpq	$8, %rsi
	cmovb	%rax, %rsi
	movq	%rsi, 24(%rdi)
	movq	(%rdi), %rdi
	salq	$3, %rsi
	movq	16(%rdi), %rdx
	movq	24(%rdi), %rax
	subq	%rdx, %rax
	cmpq	%rax, %rsi
	ja	.L157
	addq	%rdx, %rsi
	movq	%rsi, 16(%rdi)
.L145:
	movq	24(%r12), %rax
	movq	%rdx, 16(%r12)
	subq	%rbx, %rax
	shrq	%rax
	leaq	(%rdx,%rax,8), %r15
	leaq	(%r15,%rbx,8), %r14
	cmpq	%r14, %r15
	jnb	.L146
	movq	%r15, %rbx
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L147:
	movq	(%r12), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L158
	leaq	512(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L148:
	movq	%rax, (%rbx)
	addq	$8, %rbx
	cmpq	%rbx, %r14
	jbe	.L146
.L151:
	movq	8(%r12), %rax
	testq	%rax, %rax
	je	.L147
	cmpq	$127, 8(%rax)
	jbe	.L147
	movq	(%rax), %rdx
	addq	$8, %rbx
	movq	%rdx, 8(%r12)
	movq	%rax, -8(%rbx)
	cmpq	%rbx, %r14
	ja	.L151
.L146:
	movq	%r15, 56(%r12)
	movq	(%r15), %rdx
	andl	$127, %r13d
	leaq	512(%rdx), %rax
	movq	%rdx, 40(%r12)
	movq	%rax, 48(%r12)
	leaq	-8(%r14), %rax
	movq	%rax, 88(%r12)
	movq	-8(%r14), %rax
	movq	%rdx, 32(%r12)
	movq	%rax, 72(%r12)
	leaq	512(%rax), %rcx
	leaq	(%rax,%r13,4), %rax
	movq	%rcx, 80(%r12)
	movq	%rax, 64(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L158:
	.cfi_restore_state
	movl	$512, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L157:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L145
	.cfi_endproc
.LFE17665:
	.size	_ZNSt11_Deque_baseIN2v88internal8compiler9RpoNumberENS1_22RecyclingZoneAllocatorIS3_EEE17_M_initialize_mapEm, .-_ZNSt11_Deque_baseIN2v88internal8compiler9RpoNumberENS1_22RecyclingZoneAllocatorIS3_EEE17_M_initialize_mapEm
	.section	.rodata._ZN2v88internal8compiler13JumpThreading17ComputeForwardingEPNS0_4ZoneERNS0_10ZoneVectorINS1_9RpoNumberEEEPNS1_19InstructionSequenceEb.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"vector::_M_range_check: __n (which is %zu) >= this->size() (which is %zu)"
	.section	.rodata._ZN2v88internal8compiler13JumpThreading17ComputeForwardingEPNS0_4ZoneERNS0_10ZoneVectorINS1_9RpoNumberEEEPNS1_19InstructionSequenceEb.str1.1,"aMS",@progbits,1
.LC4:
	.string	"jt [%d] B%d\n"
.LC5:
	.string	"  parallel move\n"
.LC6:
	.string	"  flags\n"
.LC7:
	.string	"  nop\n"
.LC8:
	.string	"  jmp\n"
.LC9:
	.string	"  other\n"
.LC10:
	.string	"  xx %d\n"
.LC11:
	.string	"  fw %d -> %d (recurse)\n"
.LC12:
	.string	"  fw %d -> %d (cycle)\n"
.LC13:
	.string	"  fw %d -> %d (forward)\n"
.LC14:
	.string	"B%d "
.LC15:
	.string	"-> B%d\n"
.LC16:
	.string	"\n"
	.section	.text._ZN2v88internal8compiler13JumpThreading17ComputeForwardingEPNS0_4ZoneERNS0_10ZoneVectorINS1_9RpoNumberEEEPNS1_19InstructionSequenceEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13JumpThreading17ComputeForwardingEPNS0_4ZoneERNS0_10ZoneVectorINS1_9RpoNumberEEEPNS1_19InstructionSequenceEb
	.type	_ZN2v88internal8compiler13JumpThreading17ComputeForwardingEPNS0_4ZoneERNS0_10ZoneVectorINS1_9RpoNumberEEEPNS1_19InstructionSequenceEb, @function
_ZN2v88internal8compiler13JumpThreading17ComputeForwardingEPNS0_4ZoneERNS0_10ZoneVectorINS1_9RpoNumberEEEPNS1_19InstructionSequenceEb:
.LFB14981:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	xorl	%esi, %esi
	subq	$360, %rsp
	movb	%cl, -370(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -256(%rbp)
	leaq	-256(%rbp), %rdi
	movq	$0, -248(%rbp)
	movq	$0, -240(%rbp)
	movq	$0, -232(%rbp)
	movaps	%xmm0, -224(%rbp)
	movaps	%xmm0, -208(%rbp)
	movaps	%xmm0, -192(%rbp)
	movaps	%xmm0, -176(%rbp)
	call	_ZNSt11_Deque_baseIN2v88internal8compiler9RpoNumberENS1_22RecyclingZoneAllocatorIS3_EEE17_M_initialize_mapEm
	cmpq	$0, -240(%rbp)
	je	.L288
	leaq	-160(%rbp), %rax
	movdqa	-256(%rbp), %xmm5
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, -392(%rbp)
	movaps	%xmm5, -160(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	$0, -144(%rbp)
	movq	$0, -136(%rbp)
	call	_ZNSt11_Deque_baseIN2v88internal8compiler9RpoNumberENS1_22RecyclingZoneAllocatorIS3_EEE17_M_initialize_mapEm
	movdqa	-96(%rbp), %xmm5
	movq	-216(%rbp), %r10
	movq	-224(%rbp), %xmm3
	movq	-200(%rbp), %r9
	movq	-192(%rbp), %xmm1
	movq	-184(%rbp), %rcx
	movaps	%xmm5, -192(%rbp)
	movq	%r10, %xmm5
	punpcklqdq	%xmm5, %xmm3
	movdqa	-128(%rbp), %xmm6
	movq	-208(%rbp), %xmm2
	movaps	%xmm3, -320(%rbp)
	movq	%r9, %xmm3
	movq	-168(%rbp), %rdx
	movdqa	-112(%rbp), %xmm7
	punpcklqdq	%xmm3, %xmm2
	movq	-248(%rbp), %rax
	movq	-176(%rbp), %xmm0
	movaps	%xmm6, -224(%rbp)
	movaps	%xmm2, -304(%rbp)
	movq	%rcx, %xmm2
	movq	-256(%rbp), %xmm4
	movq	-240(%rbp), %r12
	punpcklqdq	%xmm2, %xmm1
	movq	-232(%rbp), %r11
	movdqa	-80(%rbp), %xmm6
	movaps	%xmm7, -208(%rbp)
	movq	-144(%rbp), %rsi
	movq	-136(%rbp), %rdi
	movq	%rax, %xmm7
	movaps	%xmm1, -288(%rbp)
	movq	%rdx, %xmm1
	punpcklqdq	%xmm7, %xmm4
	movq	%r12, -336(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movq	%rsi, -240(%rbp)
	movq	%rdi, -232(%rbp)
	movq	%r11, -328(%rbp)
	movaps	%xmm6, -176(%rbp)
	movaps	%xmm4, -352(%rbp)
	movaps	%xmm0, -272(%rbp)
	testq	%rsi, %rsi
	je	.L161
	movq	-168(%rbp), %rcx
	movq	-200(%rbp), %rdx
	addq	$8, %rcx
	cmpq	%rdx, %rcx
	jbe	.L162
	.p2align 4,,10
	.p2align 3
.L165:
	testq	%rax, %rax
	je	.L163
	cmpq	$128, 8(%rax)
	ja	.L164
.L163:
	movq	(%rdx), %rax
	movq	$128, 8(%rax)
	movq	-248(%rbp), %rsi
	movq	%rsi, (%rax)
	movq	%rax, -248(%rbp)
.L164:
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	ja	.L165
	movq	-232(%rbp), %rdi
	movq	-240(%rbp), %rsi
.L162:
	leaq	0(,%rdi,8), %rax
	cmpq	$15, %rax
	jbe	.L161
	movq	%rdi, 8(%rsi)
	movq	$0, (%rsi)
.L161:
	movq	16(%r15), %rax
	movq	-392(%rbp), %rdx
	movq	%rbx, %rdi
	movq	16(%rax), %rsi
	subq	8(%rax), %rsi
	movl	$-1, -160(%rbp)
	sarq	$3, %rsi
	movslq	%esi, %rsi
	call	_ZNSt6vectorIN2v88internal8compiler9RpoNumberENS1_13ZoneAllocatorIS3_EEE14_M_fill_assignEmRKS3_
	movq	16(%r15), %rax
	movq	8(%rax), %r12
	movq	16(%rax), %rax
	movq	%rax, -384(%rbp)
	cmpq	%rax, %r12
	je	.L230
	leaq	-352(%rbp), %rdi
	movq	%r12, -368(%rbp)
	movq	-288(%rbp), %rax
	movb	$0, -369(%rbp)
	movq	%rdi, -400(%rbp)
	.p2align 4,,10
	.p2align 3
.L214:
	movq	-368(%rbp), %rdi
	movq	8(%rbx), %rcx
	movq	(%rdi), %rdx
	movslq	100(%rdx), %rsi
	movl	%esi, -160(%rbp)
	cmpl	$-1, (%rcx,%rsi,4)
	movq	%rsi, %rdx
	je	.L289
.L168:
	movq	-320(%rbp), %rdi
	cmpq	%rax, %rdi
	je	.L171
.L304:
	movq	-280(%rbp), %r8
	movq	-264(%rbp), %rcx
	movq	%rax, %rsi
	cmpq	%rax, %r8
	je	.L290
.L172:
	movq	16(%r15), %rdx
	movslq	-4(%rsi), %r9
	movq	8(%rdx), %rsi
	movq	16(%rdx), %rdx
	subq	%rsi, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %r9
	jnb	.L291
	movq	(%rsi,%r9,8), %r12
	cmpb	$0, _ZN2v88internal19FLAG_trace_turbo_jtE(%rip)
	movl	100(%r12), %r14d
	jne	.L292
.L174:
	movq	40(%r12), %rdx
	movq	48(%r12), %rax
	subq	%rdx, %rax
	cmpq	$4, %rax
	je	.L293
.L175:
	movl	112(%r12), %eax
	cmpl	116(%r12), %eax
	jge	.L232
	movslq	%eax, %rdx
	movq	%rdx, %r13
.L193:
	movq	208(%r15), %rdx
	movq	%rdx, %rax
	subq	216(%r15), %rax
	sarq	$3, %rax
	addq	%r13, %rax
	js	.L183
	cmpq	$63, %rax
	jg	.L184
	leaq	(%rdx,%r13,8), %rax
.L185:
	movq	(%rax), %rdx
	movq	%rdx, %rdi
	movq	%rdx, -360(%rbp)
	call	_ZNK2v88internal8compiler11Instruction17AreMovesRedundantEv@PLT
	movq	-360(%rbp), %rdx
	movl	$0, %esi
	testb	%al, %al
	je	.L294
	movzbl	_ZN2v88internal19FLAG_trace_turbo_jtE(%rip), %ecx
	movl	(%rdx), %eax
	movl	%ecx, %edi
	testl	$114688, %eax
	jne	.L295
	andl	$511, %eax
	cmpl	$17, %eax
	je	.L296
	movq	%rdx, %r13
	cmpl	$13, %eax
	je	.L297
	testb	%cl, %cl
	jne	.L298
.L181:
	movq	-288(%rbp), %rax
	movl	%r14d, -160(%rbp)
	movq	%rax, %rdx
	cmpq	-280(%rbp), %rax
	je	.L299
.L198:
	movslq	-4(%rdx), %r12
	movq	8(%rbx), %rdx
	cmpl	%r12d, %r14d
	je	.L300
	movslq	%r14d, %rsi
	movl	(%rdx,%rsi,4), %r8d
	cmpl	$-1, %r8d
	je	.L301
	cmpl	$-2, %r8d
	je	.L302
	testb	%cl, %cl
	jne	.L303
.L209:
	movl	%r8d, (%rdx,%r12,4)
	movb	$1, -369(%rbp)
.L201:
	movq	-288(%rbp), %rax
	movq	-280(%rbp), %rdx
	cmpq	%rdx, %rax
	je	.L210
	movq	-320(%rbp), %rdi
	subq	$4, %rax
	movq	%rax, -288(%rbp)
	cmpq	%rax, %rdi
	jne	.L304
.L171:
	addq	$8, -368(%rbp)
	movq	-368(%rbp), %rdi
	cmpq	%rdi, -384(%rbp)
	jne	.L214
	movzbl	-369(%rbp), %r13d
.L167:
	movzbl	_ZN2v88internal19FLAG_trace_turbo_jtE(%rip), %eax
	testb	%al, %al
	jne	.L305
.L215:
	movq	-336(%rbp), %rax
	testq	%rax, %rax
	je	.L159
	movq	-264(%rbp), %rbx
	movq	-296(%rbp), %rdx
	leaq	8(%rbx), %rcx
	cmpq	%rdx, %rcx
	jbe	.L223
	movq	-344(%rbp), %rax
	.p2align 4,,10
	.p2align 3
.L226:
	testq	%rax, %rax
	je	.L224
	cmpq	$128, 8(%rax)
	ja	.L225
.L224:
	movq	(%rdx), %rax
	movq	$128, 8(%rax)
	movq	-344(%rbp), %rsi
	movq	%rsi, (%rax)
	movq	%rax, -344(%rbp)
.L225:
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	ja	.L226
	movq	-336(%rbp), %rax
.L223:
	movq	-328(%rbp), %rdx
	leaq	0(,%rdx,8), %rcx
	cmpq	$15, %rcx
	jbe	.L159
	movq	%rdx, 8(%rax)
	movq	$0, (%rax)
.L159:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L306
	addq	$360, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L300:
	.cfi_restore_state
	testb	%cl, %cl
	jne	.L307
.L200:
	movslq	%r12d, %rax
	movl	%r12d, (%rdx,%rax,4)
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L293:
	movq	16(%r15), %rcx
	movslq	(%rdx), %rsi
	movq	8(%rcx), %rax
	movq	16(%rcx), %r9
	subq	%rax, %r9
	sarq	$3, %r9
	cmpq	%r9, %rsi
	jnb	.L308
	movq	(%rax,%rsi,8), %rdx
	movl	116(%rdx), %eax
	cmpl	%eax, 112(%rdx)
	je	.L175
	movq	208(%r15), %rcx
	leal	-1(%rax), %edx
	movslq	%edx, %rdx
	movq	%rcx, %rax
	subq	216(%r15), %rax
	sarq	$3, %rax
	addq	%rdx, %rax
	js	.L177
	cmpq	$63, %rax
	jg	.L178
	leaq	(%rcx,%rdx,8), %rax
.L179:
	movq	(%rax), %rax
	movl	(%rax), %eax
	shrl	$14, %eax
	andl	$7, %eax
	cmpl	$2, %eax
	jne	.L175
.L285:
	movzbl	_ZN2v88internal19FLAG_trace_turbo_jtE(%rip), %ecx
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L292:
	subq	-296(%rbp), %rcx
	movq	-304(%rbp), %rsi
	subq	%r8, %rax
	movl	%r14d, %edx
	sarq	$3, %rcx
	sarq	$2, %rax
	subq	$1, %rcx
	subq	%rdi, %rsi
	leaq	.LC4(%rip), %rdi
	salq	$7, %rcx
	sarq	$2, %rsi
	addq	%rcx, %rax
	addq	%rax, %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movl	100(%r12), %r14d
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L290:
	movq	-8(%rcx), %rsi
	addq	$512, %rsi
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L299:
	movq	-264(%rbp), %rdx
	movq	-8(%rdx), %rdx
	addq	$512, %rdx
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L301:
	testb	%cl, %cl
	jne	.L309
	movq	-272(%rbp), %rdi
	leaq	-4(%rdi), %rdx
	cmpq	%rdx, %rax
	je	.L169
.L312:
	movl	-160(%rbp), %edx
.L287:
	movl	%edx, (%rax)
	addq	$4, -288(%rbp)
.L170:
	movq	8(%rbx), %rax
	movslq	-160(%rbp), %rdx
	movl	$-2, (%rax,%rdx,4)
	movq	-288(%rbp), %rax
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L302:
	testb	%cl, %cl
	jne	.L310
.L208:
	movl	-160(%rbp), %eax
	movb	$1, -369(%rbp)
	movl	%eax, (%rdx,%r12,4)
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L210:
	movq	-344(%rbp), %rax
	testq	%rax, %rax
	je	.L211
	cmpq	$128, 8(%rax)
	ja	.L212
.L211:
	movq	$128, 8(%rdx)
	movq	-344(%rbp), %rax
	movq	%rax, (%rdx)
	movq	%rdx, -344(%rbp)
.L212:
	movq	-264(%rbp), %rax
	leaq	-8(%rax), %rdx
	movq	%rdx, -264(%rbp)
	movq	-8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, -280(%rbp)
	addq	$508, %rax
	movq	%rdx, -272(%rbp)
	movq	%rax, -288(%rbp)
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L184:
	movq	%rax, %rdx
	sarq	$6, %rdx
.L186:
	movq	232(%r15), %rcx
	movq	%rdx, %rdi
	salq	$6, %rdi
	movq	(%rcx,%rdx,8), %rdx
	subq	%rdi, %rax
	leaq	(%rdx,%rax,8), %rax
	jmp	.L185
	.p2align 4,,10
	.p2align 3
.L307:
	movl	%r12d, %esi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	8(%rbx), %rdx
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L303:
	movl	%r14d, %edx
	movl	%r12d, %esi
	leaq	.LC13(%rip), %rdi
	xorl	%eax, %eax
	movl	%r8d, -360(%rbp)
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	8(%rbx), %rdx
	movl	-360(%rbp), %r8d
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L183:
	movq	%rax, %rdx
	notq	%rdx
	shrq	$6, %rdx
	notq	%rdx
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L178:
	movq	%rax, %rdx
	sarq	$6, %rdx
.L180:
	movq	232(%r15), %rcx
	movq	%rdx, %rsi
	salq	$6, %rsi
	movq	(%rcx,%rdx,8), %rdx
	subq	%rsi, %rax
	leaq	(%rdx,%rax,8), %rax
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L297:
	testb	%cl, %cl
	jne	.L311
.L195:
	cmpb	$0, -370(%rbp)
	jne	.L196
	cmpb	$0, 126(%r12)
	jne	.L285
	cmpb	$0, 125(%r12)
	jne	.L285
.L196:
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler19InstructionSequence8InputRpoEPNS1_11InstructionEm@PLT
	movl	%eax, %r14d
	jmp	.L285
	.p2align 4,,10
	.p2align 3
.L309:
	movl	%r14d, %edx
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	movl	%r12d, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-272(%rbp), %rdi
	movq	-288(%rbp), %rax
	leaq	-4(%rdi), %rdx
	cmpq	%rdx, %rax
	jne	.L312
.L169:
	movq	-392(%rbp), %rsi
	movq	-400(%rbp), %rdi
	call	_ZNSt5dequeIN2v88internal8compiler9RpoNumberENS1_22RecyclingZoneAllocatorIS3_EEE16_M_push_back_auxIJRKS3_EEEvDpOT_
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L177:
	movq	%rax, %rdx
	notq	%rdx
	shrq	$6, %rdx
	notq	%rdx
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L296:
	testb	%cl, %cl
	jne	.L313
	addq	$1, %r13
	leal	(%rsi,%r13), %eax
	cmpl	%eax, 116(%r12)
	jg	.L193
.L192:
	movl	100(%r12), %eax
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L298:
	leaq	.LC9(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movzbl	_ZN2v88internal19FLAG_trace_turbo_jtE(%rip), %ecx
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L310:
	movl	%r14d, %edx
	movl	%r12d, %esi
	leaq	.LC12(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	8(%rbx), %rdx
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L311:
	leaq	.LC8(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L289:
	movq	-272(%rbp), %rdi
	leaq	-4(%rdi), %rcx
	cmpq	%rcx, %rax
	jne	.L287
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L232:
	movzbl	_ZN2v88internal19FLAG_trace_turbo_jtE(%rip), %edi
	movl	%r14d, %eax
.L182:
	movq	16(%r15), %rsi
	addl	$1, %eax
	movl	%edi, %ecx
	movq	16(%rsi), %rdx
	subq	8(%rsi), %rdx
	sarq	$3, %rdx
	cmpl	%edx, %eax
	cmovl	%eax, %r14d
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L313:
	xorl	%eax, %eax
	leaq	.LC7(%rip), %rdi
	addq	$1, %r13
	call	_ZN2v88internal6PrintFEPKcz@PLT
	xorl	%esi, %esi
	leal	(%rsi,%r13), %eax
	cmpl	%eax, 116(%r12)
	jg	.L193
	movzbl	_ZN2v88internal19FLAG_trace_turbo_jtE(%rip), %edi
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L294:
	movzbl	_ZN2v88internal19FLAG_trace_turbo_jtE(%rip), %ecx
	testb	%cl, %cl
	je	.L181
	leaq	.LC5(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L285
.L295:
	testb	%cl, %cl
	je	.L181
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movzbl	_ZN2v88internal19FLAG_trace_turbo_jtE(%rip), %ecx
	jmp	.L181
.L305:
	movq	16(%rbx), %rcx
	movq	8(%rbx), %rdx
	movq	%rcx, %rsi
	subq	%rdx, %rsi
	sarq	$2, %rsi
	testl	%esi, %esi
	jle	.L215
	xorl	%r12d, %r12d
	leaq	.LC14(%rip), %r14
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L219:
	movq	%rcx, %rax
	addq	$1, %r12
	subq	%rdx, %rax
	sarq	$2, %rax
	cmpl	%r12d, %eax
	jle	.L215
	movzbl	_ZN2v88internal19FLAG_trace_turbo_jtE(%rip), %eax
.L221:
	testb	%al, %al
	je	.L219
	movl	%r12d, %esi
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	8(%rbx), %rdx
	movl	(%rdx,%r12,4), %esi
	cmpl	%r12d, %esi
	je	.L217
	cmpb	$0, _ZN2v88internal19FLAG_trace_turbo_jtE(%rip)
	je	.L286
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	8(%rbx), %rdx
	movq	16(%rbx), %rcx
	jmp	.L219
.L288:
	movdqa	-256(%rbp), %xmm6
	movdqa	-224(%rbp), %xmm7
	movq	$0, -336(%rbp)
	movq	-232(%rbp), %rax
	movdqa	-176(%rbp), %xmm1
	movaps	%xmm6, -352(%rbp)
	movdqa	-208(%rbp), %xmm6
	movaps	%xmm7, -320(%rbp)
	movdqa	-192(%rbp), %xmm7
	movq	%rax, -328(%rbp)
	leaq	-160(%rbp), %rax
	movq	%rax, -392(%rbp)
	movaps	%xmm6, -304(%rbp)
	movaps	%xmm7, -288(%rbp)
	movaps	%xmm1, -272(%rbp)
	jmp	.L161
.L217:
	cmpb	$0, _ZN2v88internal19FLAG_trace_turbo_jtE(%rip)
	je	.L286
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	8(%rbx), %rdx
.L286:
	movq	16(%rbx), %rcx
	jmp	.L219
.L230:
	xorl	%r13d, %r13d
	jmp	.L167
.L291:
	movq	%r9, %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L308:
	movq	%r9, %rdx
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L306:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE14981:
	.size	_ZN2v88internal8compiler13JumpThreading17ComputeForwardingEPNS0_4ZoneERNS0_10ZoneVectorINS1_9RpoNumberEEEPNS1_19InstructionSequenceEb, .-_ZN2v88internal8compiler13JumpThreading17ComputeForwardingEPNS0_4ZoneERNS0_10ZoneVectorINS1_9RpoNumberEEEPNS1_19InstructionSequenceEb
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler13JumpThreading17ComputeForwardingEPNS0_4ZoneERNS0_10ZoneVectorINS1_9RpoNumberEEEPNS1_19InstructionSequenceEb,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler13JumpThreading17ComputeForwardingEPNS0_4ZoneERNS0_10ZoneVectorINS1_9RpoNumberEEEPNS1_19InstructionSequenceEb, @function
_GLOBAL__sub_I__ZN2v88internal8compiler13JumpThreading17ComputeForwardingEPNS0_4ZoneERNS0_10ZoneVectorINS1_9RpoNumberEEEPNS1_19InstructionSequenceEb:
.LFB18792:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE18792:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler13JumpThreading17ComputeForwardingEPNS0_4ZoneERNS0_10ZoneVectorINS1_9RpoNumberEEEPNS1_19InstructionSequenceEb, .-_GLOBAL__sub_I__ZN2v88internal8compiler13JumpThreading17ComputeForwardingEPNS0_4ZoneERNS0_10ZoneVectorINS1_9RpoNumberEEEPNS1_19InstructionSequenceEb
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler13JumpThreading17ComputeForwardingEPNS0_4ZoneERNS0_10ZoneVectorINS1_9RpoNumberEEEPNS1_19InstructionSequenceEb
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
