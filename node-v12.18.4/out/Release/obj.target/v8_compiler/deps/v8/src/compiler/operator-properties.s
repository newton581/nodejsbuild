	.file	"operator-properties.cc"
	.text
	.section	.text._ZN2v88internal8compiler18OperatorProperties15HasContextInputEPKNS1_8OperatorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18OperatorProperties15HasContextInputEPKNS1_8OperatorE
	.type	_ZN2v88internal8compiler18OperatorProperties15HasContextInputEPKNS1_8OperatorE, @function
_ZN2v88internal8compiler18OperatorProperties15HasContextInputEPKNS1_8OperatorE:
.LFB12728:
	.cfi_startproc
	endbr64
	movzwl	16(%rdi), %eax
	subl	$682, %eax
	cmpl	$106, %eax
	setbe	%al
	ret
	.cfi_endproc
.LFE12728:
	.size	_ZN2v88internal8compiler18OperatorProperties15HasContextInputEPKNS1_8OperatorE, .-_ZN2v88internal8compiler18OperatorProperties15HasContextInputEPKNS1_8OperatorE
	.section	.rodata._ZN2v88internal8compiler18OperatorProperties17NeedsExactContextEPKNS1_8OperatorE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal8compiler18OperatorProperties17NeedsExactContextEPKNS1_8OperatorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18OperatorProperties17NeedsExactContextEPKNS1_8OperatorE
	.type	_ZN2v88internal8compiler18OperatorProperties17NeedsExactContextEPKNS1_8OperatorE, @function
_ZN2v88internal8compiler18OperatorProperties17NeedsExactContextEPKNS1_8OperatorE:
.LFB12729:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movzwl	16(%rdi), %eax
	subw	$682, %ax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpw	$106, %ax
	ja	.L4
	leaq	.L6(%rip), %rdx
	movzwl	%ax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler18OperatorProperties17NeedsExactContextEPKNS1_8OperatorE,"a",@progbits
	.align 4
	.align 4
.L6:
	.long	.L10-.L6
	.long	.L10-.L6
	.long	.L10-.L6
	.long	.L10-.L6
	.long	.L10-.L6
	.long	.L10-.L6
	.long	.L10-.L6
	.long	.L10-.L6
	.long	.L10-.L6
	.long	.L10-.L6
	.long	.L10-.L6
	.long	.L10-.L6
	.long	.L10-.L6
	.long	.L10-.L6
	.long	.L10-.L6
	.long	.L10-.L6
	.long	.L10-.L6
	.long	.L10-.L6
	.long	.L10-.L6
	.long	.L10-.L6
	.long	.L10-.L6
	.long	.L10-.L6
	.long	.L10-.L6
	.long	.L10-.L6
	.long	.L10-.L6
	.long	.L10-.L6
	.long	.L10-.L6
	.long	.L10-.L6
	.long	.L10-.L6
	.long	.L10-.L6
	.long	.L10-.L6
	.long	.L10-.L6
	.long	.L10-.L6
	.long	.L10-.L6
	.long	.L10-.L6
	.long	.L9-.L6
	.long	.L4-.L6
	.long	.L10-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L5-.L6
	.long	.L4-.L6
	.long	.L10-.L6
	.long	.L10-.L6
	.long	.L5-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L10-.L6
	.long	.L10-.L6
	.long	.L10-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L5-.L6
	.long	.L5-.L6
	.long	.L10-.L6
	.long	.L5-.L6
	.long	.L5-.L6
	.long	.L5-.L6
	.long	.L10-.L6
	.long	.L5-.L6
	.long	.L5-.L6
	.long	.L5-.L6
	.long	.L5-.L6
	.long	.L10-.L6
	.long	.L5-.L6
	.long	.L5-.L6
	.long	.L5-.L6
	.long	.L5-.L6
	.long	.L5-.L6
	.long	.L5-.L6
	.long	.L10-.L6
	.long	.L10-.L6
	.long	.L10-.L6
	.long	.L10-.L6
	.long	.L10-.L6
	.long	.L10-.L6
	.long	.L10-.L6
	.long	.L10-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L8-.L6
	.long	.L10-.L6
	.long	.L10-.L6
	.long	.L10-.L6
	.long	.L10-.L6
	.long	.L10-.L6
	.long	.L10-.L6
	.long	.L5-.L6
	.long	.L5-.L6
	.long	.L5-.L6
	.long	.L10-.L6
	.long	.L10-.L6
	.long	.L10-.L6
	.long	.L10-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L10-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L5-.L6
	.section	.text._ZN2v88internal8compiler18OperatorProperties17NeedsExactContextEPKNS1_8OperatorE
	.p2align 4,,10
	.p2align 3
.L10:
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L9:
	.cfi_restore_state
	call	_ZN2v88internal8compiler21CreateArgumentsTypeOfEPKNS1_8OperatorE@PLT
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	cmpb	$0, (%rax)
	sete	%al
	ret
.L8:
	.cfi_restore_state
	call	_ZN2v88internal8compiler23CallRuntimeParametersOfEPKNS1_8OperatorE@PLT
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movl	(%rax), %edi
	jmp	_ZN2v88internal7Runtime17NeedsExactContextENS1_10FunctionIdE@PLT
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	movl	$1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE12729:
	.size	_ZN2v88internal8compiler18OperatorProperties17NeedsExactContextEPKNS1_8OperatorE, .-_ZN2v88internal8compiler18OperatorProperties17NeedsExactContextEPKNS1_8OperatorE
	.section	.text._ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE
	.type	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE, @function
_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE:
.LFB12730:
	.cfi_startproc
	endbr64
	movzwl	16(%rdi), %eax
	cmpw	$788, %ax
	ja	.L21
	cmpw	$681, %ax
	jbe	.L24
	subw	$682, %ax
	cmpw	$106, %ax
	ja	.L21
	leaq	.L16(%rip), %rdx
	movzwl	%ax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE,"a",@progbits
	.align 4
	.align 4
.L16:
	.long	.L19-.L16
	.long	.L21-.L16
	.long	.L19-.L16
	.long	.L19-.L16
	.long	.L19-.L16
	.long	.L19-.L16
	.long	.L19-.L16
	.long	.L19-.L16
	.long	.L19-.L16
	.long	.L19-.L16
	.long	.L19-.L16
	.long	.L19-.L16
	.long	.L19-.L16
	.long	.L19-.L16
	.long	.L19-.L16
	.long	.L19-.L16
	.long	.L19-.L16
	.long	.L19-.L16
	.long	.L19-.L16
	.long	.L19-.L16
	.long	.L19-.L16
	.long	.L19-.L16
	.long	.L19-.L16
	.long	.L19-.L16
	.long	.L19-.L16
	.long	.L19-.L16
	.long	.L19-.L16
	.long	.L19-.L16
	.long	.L19-.L16
	.long	.L19-.L16
	.long	.L19-.L16
	.long	.L19-.L16
	.long	.L19-.L16
	.long	.L19-.L16
	.long	.L19-.L16
	.long	.L19-.L16
	.long	.L19-.L16
	.long	.L19-.L16
	.long	.L21-.L16
	.long	.L21-.L16
	.long	.L21-.L16
	.long	.L21-.L16
	.long	.L21-.L16
	.long	.L21-.L16
	.long	.L21-.L16
	.long	.L21-.L16
	.long	.L21-.L16
	.long	.L21-.L16
	.long	.L19-.L16
	.long	.L19-.L16
	.long	.L19-.L16
	.long	.L19-.L16
	.long	.L21-.L16
	.long	.L21-.L16
	.long	.L19-.L16
	.long	.L19-.L16
	.long	.L19-.L16
	.long	.L19-.L16
	.long	.L19-.L16
	.long	.L19-.L16
	.long	.L19-.L16
	.long	.L19-.L16
	.long	.L19-.L16
	.long	.L21-.L16
	.long	.L19-.L16
	.long	.L19-.L16
	.long	.L19-.L16
	.long	.L21-.L16
	.long	.L21-.L16
	.long	.L21-.L16
	.long	.L21-.L16
	.long	.L21-.L16
	.long	.L21-.L16
	.long	.L19-.L16
	.long	.L19-.L16
	.long	.L19-.L16
	.long	.L19-.L16
	.long	.L19-.L16
	.long	.L19-.L16
	.long	.L19-.L16
	.long	.L19-.L16
	.long	.L19-.L16
	.long	.L19-.L16
	.long	.L19-.L16
	.long	.L17-.L16
	.long	.L19-.L16
	.long	.L19-.L16
	.long	.L21-.L16
	.long	.L19-.L16
	.long	.L21-.L16
	.long	.L21-.L16
	.long	.L21-.L16
	.long	.L21-.L16
	.long	.L21-.L16
	.long	.L21-.L16
	.long	.L21-.L16
	.long	.L21-.L16
	.long	.L21-.L16
	.long	.L21-.L16
	.long	.L19-.L16
	.long	.L19-.L16
	.long	.L19-.L16
	.long	.L19-.L16
	.long	.L19-.L16
	.long	.L19-.L16
	.long	.L19-.L16
	.long	.L19-.L16
	.section	.text._ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE
.L19:
	movl	$1, %eax
	ret
.L21:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	cmpw	$38, %ax
	je	.L19
	cmpw	$41, %ax
	sete	%al
	ret
.L17:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal8compiler23CallRuntimeParametersOfEPKNS1_8OperatorE@PLT
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	movl	(%rax), %edi
	jmp	_ZN2v88internal8compiler7Linkage20NeedsFrameStateInputENS0_7Runtime10FunctionIdE@PLT
	.cfi_endproc
.LFE12730:
	.size	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE, .-_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE
	.section	.text._ZN2v88internal8compiler18OperatorProperties18GetTotalInputCountEPKNS1_8OperatorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18OperatorProperties18GetTotalInputCountEPKNS1_8OperatorE
	.type	_ZN2v88internal8compiler18OperatorProperties18GetTotalInputCountEPKNS1_8OperatorE, @function
_ZN2v88internal8compiler18OperatorProperties18GetTotalInputCountEPKNS1_8OperatorE:
.LFB12731:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movzwl	16(%rdi), %edx
	xorl	%ebx, %ebx
	movl	%edx, %eax
	subl	$682, %edx
	cmpl	$106, %edx
	setbe	%bl
	addl	20(%rdi), %ebx
	cmpw	$788, %ax
	ja	.L26
	cmpw	$681, %ax
	jbe	.L38
	subw	$682, %ax
	cmpw	$106, %ax
	ja	.L26
	leaq	.L29(%rip), %rdx
	movzwl	%ax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler18OperatorProperties18GetTotalInputCountEPKNS1_8OperatorE,"a",@progbits
	.align 4
	.align 4
.L29:
	.long	.L28-.L29
	.long	.L26-.L29
	.long	.L28-.L29
	.long	.L28-.L29
	.long	.L28-.L29
	.long	.L28-.L29
	.long	.L28-.L29
	.long	.L28-.L29
	.long	.L28-.L29
	.long	.L28-.L29
	.long	.L28-.L29
	.long	.L28-.L29
	.long	.L28-.L29
	.long	.L28-.L29
	.long	.L28-.L29
	.long	.L28-.L29
	.long	.L28-.L29
	.long	.L28-.L29
	.long	.L28-.L29
	.long	.L28-.L29
	.long	.L28-.L29
	.long	.L28-.L29
	.long	.L28-.L29
	.long	.L28-.L29
	.long	.L28-.L29
	.long	.L28-.L29
	.long	.L28-.L29
	.long	.L28-.L29
	.long	.L28-.L29
	.long	.L28-.L29
	.long	.L28-.L29
	.long	.L28-.L29
	.long	.L28-.L29
	.long	.L28-.L29
	.long	.L28-.L29
	.long	.L28-.L29
	.long	.L28-.L29
	.long	.L28-.L29
	.long	.L26-.L29
	.long	.L26-.L29
	.long	.L26-.L29
	.long	.L26-.L29
	.long	.L26-.L29
	.long	.L26-.L29
	.long	.L26-.L29
	.long	.L26-.L29
	.long	.L26-.L29
	.long	.L26-.L29
	.long	.L28-.L29
	.long	.L28-.L29
	.long	.L28-.L29
	.long	.L28-.L29
	.long	.L26-.L29
	.long	.L26-.L29
	.long	.L28-.L29
	.long	.L28-.L29
	.long	.L28-.L29
	.long	.L28-.L29
	.long	.L28-.L29
	.long	.L28-.L29
	.long	.L28-.L29
	.long	.L28-.L29
	.long	.L28-.L29
	.long	.L26-.L29
	.long	.L28-.L29
	.long	.L28-.L29
	.long	.L28-.L29
	.long	.L26-.L29
	.long	.L26-.L29
	.long	.L26-.L29
	.long	.L26-.L29
	.long	.L26-.L29
	.long	.L26-.L29
	.long	.L28-.L29
	.long	.L28-.L29
	.long	.L28-.L29
	.long	.L28-.L29
	.long	.L28-.L29
	.long	.L28-.L29
	.long	.L28-.L29
	.long	.L28-.L29
	.long	.L28-.L29
	.long	.L28-.L29
	.long	.L28-.L29
	.long	.L30-.L29
	.long	.L28-.L29
	.long	.L28-.L29
	.long	.L26-.L29
	.long	.L28-.L29
	.long	.L26-.L29
	.long	.L26-.L29
	.long	.L26-.L29
	.long	.L26-.L29
	.long	.L26-.L29
	.long	.L26-.L29
	.long	.L26-.L29
	.long	.L26-.L29
	.long	.L26-.L29
	.long	.L26-.L29
	.long	.L28-.L29
	.long	.L28-.L29
	.long	.L28-.L29
	.long	.L28-.L29
	.long	.L28-.L29
	.long	.L28-.L29
	.long	.L28-.L29
	.long	.L28-.L29
	.section	.text._ZN2v88internal8compiler18OperatorProperties18GetTotalInputCountEPKNS1_8OperatorE
.L30:
	call	_ZN2v88internal8compiler23CallRuntimeParametersOfEPKNS1_8OperatorE@PLT
	movl	(%rax), %edi
	call	_ZN2v88internal8compiler7Linkage20NeedsFrameStateInputENS0_7Runtime10FunctionIdE@PLT
	movzbl	%al, %eax
	addl	%eax, %ebx
.L26:
	movl	24(%r12), %eax
	addl	%ebx, %eax
	popq	%rbx
	addl	28(%r12), %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	.cfi_restore_state
	cmpw	$38, %ax
	je	.L28
	cmpw	$41, %ax
	jne	.L26
.L28:
	addl	$1, %ebx
	jmp	.L26
	.cfi_endproc
.LFE12731:
	.size	_ZN2v88internal8compiler18OperatorProperties18GetTotalInputCountEPKNS1_8OperatorE, .-_ZN2v88internal8compiler18OperatorProperties18GetTotalInputCountEPKNS1_8OperatorE
	.section	.text._ZN2v88internal8compiler18OperatorProperties17IsBasicBlockBeginEPKNS1_8OperatorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18OperatorProperties17IsBasicBlockBeginEPKNS1_8OperatorE
	.type	_ZN2v88internal8compiler18OperatorProperties17IsBasicBlockBeginEPKNS1_8OperatorE, @function
_ZN2v88internal8compiler18OperatorProperties17IsBasicBlockBeginEPKNS1_8OperatorE:
.LFB12732:
	.cfi_startproc
	endbr64
	movzwl	16(%rdi), %eax
	cmpw	$61, %ax
	ja	.L40
	movabsq	$2305843009217889395, %rdx
	movl	$1, %r8d
	btq	%rax, %rdx
	jnc	.L40
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	subl	$7, %eax
	cmpw	$2, %ax
	setbe	%r8b
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE12732:
	.size	_ZN2v88internal8compiler18OperatorProperties17IsBasicBlockBeginEPKNS1_8OperatorE, .-_ZN2v88internal8compiler18OperatorProperties17IsBasicBlockBeginEPKNS1_8OperatorE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler18OperatorProperties15HasContextInputEPKNS1_8OperatorE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler18OperatorProperties15HasContextInputEPKNS1_8OperatorE, @function
_GLOBAL__sub_I__ZN2v88internal8compiler18OperatorProperties15HasContextInputEPKNS1_8OperatorE:
.LFB15124:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE15124:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler18OperatorProperties15HasContextInputEPKNS1_8OperatorE, .-_GLOBAL__sub_I__ZN2v88internal8compiler18OperatorProperties15HasContextInputEPKNS1_8OperatorE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler18OperatorProperties15HasContextInputEPKNS1_8OperatorE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
