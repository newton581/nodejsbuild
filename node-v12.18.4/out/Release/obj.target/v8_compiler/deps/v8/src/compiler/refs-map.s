	.file	"refs-map.cc"
	.text
	.section	.rodata._ZN2v88internal8compiler7RefsMapC2EjNS1_14AddressMatcherEPNS0_4ZoneE.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"Out of memory: HashMap::Initialize"
	.section	.text._ZN2v88internal8compiler7RefsMapC2EjNS1_14AddressMatcherEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler7RefsMapC2EjNS1_14AddressMatcherEPNS0_4ZoneE
	.type	_ZN2v88internal8compiler7RefsMapC2EjNS1_14AddressMatcherEPNS0_4ZoneE, @function
_ZN2v88internal8compiler7RefsMapC2EjNS1_14AddressMatcherEPNS0_4ZoneE:
.LFB3812:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%esi, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movq	%r14, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%rdx, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	16(%rdx), %rax
	leaq	(%r14,%r14,2), %rbx
	movq	24(%rdx), %rdx
	salq	$3, %rbx
	movq	%rbx, %rsi
	subq	%rax, %rdx
	cmpq	%rdx, %rbx
	ja	.L13
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L3:
	movq	%rax, (%r12)
	testq	%rax, %rax
	je	.L14
	movl	%r13d, 8(%r12)
	testq	%r14, %r14
	je	.L5
	addq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L6:
	movb	$0, 20(%rax)
	addq	$24, %rax
	cmpq	%rax, %rbx
	jne	.L6
.L5:
	movl	$0, 12(%r12)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	.cfi_restore_state
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L3
.L14:
	leaq	.LC0(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE3812:
	.size	_ZN2v88internal8compiler7RefsMapC2EjNS1_14AddressMatcherEPNS0_4ZoneE, .-_ZN2v88internal8compiler7RefsMapC2EjNS1_14AddressMatcherEPNS0_4ZoneE
	.globl	_ZN2v88internal8compiler7RefsMapC1EjNS1_14AddressMatcherEPNS0_4ZoneE
	.set	_ZN2v88internal8compiler7RefsMapC1EjNS1_14AddressMatcherEPNS0_4ZoneE,_ZN2v88internal8compiler7RefsMapC2EjNS1_14AddressMatcherEPNS0_4ZoneE
	.section	.text._ZN2v88internal8compiler7RefsMapC2EPKS2_PNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler7RefsMapC2EPKS2_PNS0_4ZoneE
	.type	_ZN2v88internal8compiler7RefsMapC2EPKS2_PNS0_4ZoneE, @function
_ZN2v88internal8compiler7RefsMapC2EPKS2_PNS0_4ZoneE:
.LFB3815:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	8(%rsi), %rax
	movq	%rsi, %rbx
	movq	%rax, 8(%rdi)
	movl	8(%rsi), %eax
	movq	16(%rdx), %r8
	leaq	(%rax,%rax,2), %rsi
	movq	24(%rdx), %rax
	salq	$3, %rsi
	subq	%r8, %rax
	cmpq	%rax, %rsi
	ja	.L19
	addq	%r8, %rsi
	movq	%rsi, 16(%rdx)
.L17:
	movl	8(%r12), %eax
	movq	(%rbx), %rsi
	movq	%r8, (%r12)
	movq	%r8, %rdi
	popq	%rbx
	popq	%r12
	leaq	(%rax,%rax,2), %rdx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	salq	$3, %rdx
	jmp	memcpy@PLT
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	movq	%rdx, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r8
	jmp	.L17
	.cfi_endproc
.LFE3815:
	.size	_ZN2v88internal8compiler7RefsMapC2EPKS2_PNS0_4ZoneE, .-_ZN2v88internal8compiler7RefsMapC2EPKS2_PNS0_4ZoneE
	.globl	_ZN2v88internal8compiler7RefsMapC1EPKS2_PNS0_4ZoneE
	.set	_ZN2v88internal8compiler7RefsMapC1EPKS2_PNS0_4ZoneE,_ZN2v88internal8compiler7RefsMapC2EPKS2_PNS0_4ZoneE
	.section	.text._ZNK2v88internal8compiler7RefsMap6LookupERKm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler7RefsMap6LookupERKm
	.type	_ZNK2v88internal8compiler7RefsMap6LookupERKm, @function
_ZNK2v88internal8compiler7RefsMap6LookupERKm:
.LFB3817:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %ecx
	movq	(%rsi), %rsi
	movq	(%rdi), %r9
	subl	$1, %ecx
	movl	%ecx, %eax
	andl	%esi, %eax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%r9,%rdx,8), %r8
	cmpb	$0, 20(%r8)
	jne	.L22
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L25:
	addq	$1, %rax
	andq	%rcx, %rax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%r9,%rdx,8), %r8
	cmpb	$0, 20(%r8)
	je	.L23
.L22:
	cmpq	(%r8), %rsi
	jne	.L25
	movq	%r8, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	xorl	%r8d, %r8d
	movq	%r8, %rax
	ret
	.cfi_endproc
.LFE3817:
	.size	_ZNK2v88internal8compiler7RefsMap6LookupERKm, .-_ZNK2v88internal8compiler7RefsMap6LookupERKm
	.section	.text._ZN2v88internal8compiler7RefsMap4HashEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler7RefsMap4HashEm
	.type	_ZN2v88internal8compiler7RefsMap4HashEm, @function
_ZN2v88internal8compiler7RefsMap4HashEm:
.LFB3822:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE3822:
	.size	_ZN2v88internal8compiler7RefsMap4HashEm, .-_ZN2v88internal8compiler7RefsMap4HashEm
	.section	.text._ZN2v84base19TemplateHashMapImplImPNS_8internal8compiler10ObjectDataENS3_14AddressMatcherENS2_20ZoneAllocationPolicyEE6ResizeES7_,"axG",@progbits,_ZN2v84base19TemplateHashMapImplImPNS_8internal8compiler10ObjectDataENS3_14AddressMatcherENS2_20ZoneAllocationPolicyEE6ResizeES7_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v84base19TemplateHashMapImplImPNS_8internal8compiler10ObjectDataENS3_14AddressMatcherENS2_20ZoneAllocationPolicyEE6ResizeES7_
	.type	_ZN2v84base19TemplateHashMapImplImPNS_8internal8compiler10ObjectDataENS3_14AddressMatcherENS2_20ZoneAllocationPolicyEE6ResizeES7_, @function
_ZN2v84base19TemplateHashMapImplImPNS_8internal8compiler10ObjectDataENS3_14AddressMatcherENS2_20ZoneAllocationPolicyEE6ResizeES7_:
.LFB4263:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movl	8(%rdi), %eax
	movq	24(%r15), %rdx
	movq	(%rdi), %r13
	movl	12(%rdi), %r14d
	addl	%eax, %eax
	leaq	(%rax,%rax,2), %rsi
	movq	%rax, %rbx
	movq	16(%r15), %rax
	salq	$3, %rsi
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L55
	addq	%rax, %rsi
	movq	%rsi, 16(%r15)
.L29:
	movq	%rax, (%r12)
	testq	%rax, %rax
	je	.L56
	movl	%ebx, 8(%r12)
	testl	%ebx, %ebx
	je	.L31
	movb	$0, 20(%rax)
	cmpl	$1, 8(%r12)
	movl	$24, %edx
	movl	$1, %eax
	jbe	.L31
	.p2align 4,,10
	.p2align 3
.L32:
	movq	(%r12), %rcx
	addq	$1, %rax
	movb	$0, 20(%rcx,%rdx)
	movl	8(%r12), %ecx
	addq	$24, %rdx
	cmpq	%rax, %rcx
	ja	.L32
.L31:
	movl	$0, 12(%r12)
	testl	%r14d, %r14d
	je	.L27
.L34:
	cmpb	$0, 20(%r13)
	jne	.L57
.L35:
	addq	$24, %r13
	cmpb	$0, 20(%r13)
	je	.L35
.L57:
	movl	8(%r12), %eax
	movl	16(%r13), %ebx
	movq	(%r12), %rdi
	movq	0(%r13), %rsi
	leal	-1(%rax), %ecx
	movl	%ebx, %eax
	andl	%ecx, %eax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rdi,%rdx,8), %rdx
	cmpb	$0, 20(%rdx)
	jne	.L37
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L58:
	addq	$1, %rax
	andq	%rcx, %rax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rdi,%rdx,8), %rdx
	cmpb	$0, 20(%rdx)
	je	.L36
.L37:
	cmpq	%rsi, (%rdx)
	jne	.L58
.L36:
	movq	8(%r13), %rax
	movq	%rsi, (%rdx)
	movl	%ebx, 16(%rdx)
	movq	%rax, 8(%rdx)
	movb	$1, 20(%rdx)
	movl	12(%r12), %eax
	addl	$1, %eax
	movl	%eax, %edx
	movl	%eax, 12(%r12)
	shrl	$2, %edx
	addl	%edx, %eax
	cmpl	8(%r12), %eax
	jnb	.L38
.L41:
	addq	$24, %r13
	subl	$1, %r14d
	jne	.L34
.L27:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	.cfi_restore_state
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v84base19TemplateHashMapImplImPNS_8internal8compiler10ObjectDataENS3_14AddressMatcherENS2_20ZoneAllocationPolicyEE6ResizeES7_
	movl	8(%r12), %eax
	movq	(%r12), %rsi
	leal	-1(%rax), %ecx
	movl	%ebx, %eax
	andl	%ecx, %eax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rsi,%rdx,8), %rdx
	cmpb	$0, 20(%rdx)
	je	.L41
	movq	0(%r13), %rdi
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L59:
	addq	$1, %rax
	andq	%rcx, %rax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rsi,%rdx,8), %rdx
	cmpb	$0, 20(%rdx)
	je	.L41
.L42:
	cmpq	(%rdx), %rdi
	jne	.L59
	jmp	.L41
.L55:
	movq	%r15, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L29
.L56:
	leaq	.LC0(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE4263:
	.size	_ZN2v84base19TemplateHashMapImplImPNS_8internal8compiler10ObjectDataENS3_14AddressMatcherENS2_20ZoneAllocationPolicyEE6ResizeES7_, .-_ZN2v84base19TemplateHashMapImplImPNS_8internal8compiler10ObjectDataENS3_14AddressMatcherENS2_20ZoneAllocationPolicyEE6ResizeES7_
	.section	.text._ZN2v88internal8compiler7RefsMap14LookupOrInsertERKmPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler7RefsMap14LookupOrInsertERKmPNS0_4ZoneE
	.type	_ZN2v88internal8compiler7RefsMap14LookupOrInsertERKmPNS0_4ZoneE, @function
_ZN2v88internal8compiler7RefsMap14LookupOrInsertERKmPNS0_4ZoneE:
.LFB3818:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movl	8(%rdi), %eax
	movq	(%rsi), %rbx
	movq	(%rdi), %rdi
	leal	-1(%rax), %r8d
	movl	%r8d, %ecx
	andl	%ebx, %ecx
	leaq	(%rcx,%rcx,2), %rax
	leaq	(%rdi,%rax,8), %rax
	cmpb	$0, 20(%rax)
	jne	.L63
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L70:
	addq	$1, %rcx
	andq	%r8, %rcx
	leaq	(%rcx,%rcx,2), %rax
	leaq	(%rdi,%rax,8), %rax
	cmpb	$0, 20(%rax)
	je	.L61
.L63:
	cmpq	(%rax), %rbx
	jne	.L70
.L60:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L61:
	.cfi_restore_state
	movq	%rbx, (%rax)
	movq	$0, 8(%rax)
	movl	%ebx, 16(%rax)
	movb	$1, 20(%rax)
	movl	12(%r12), %esi
	leal	1(%rsi), %ecx
	movl	%ecx, %esi
	movl	%ecx, 12(%r12)
	shrl	$2, %esi
	addl	%esi, %ecx
	cmpl	8(%r12), %ecx
	jb	.L60
	movq	%rdx, %rsi
	movq	%r12, %rdi
	call	_ZN2v84base19TemplateHashMapImplImPNS_8internal8compiler10ObjectDataENS3_14AddressMatcherENS2_20ZoneAllocationPolicyEE6ResizeES7_
	movl	8(%r12), %eax
	movl	%ebx, %edx
	movq	(%r12), %rsi
	leal	-1(%rax), %ecx
	andl	%ecx, %edx
	leaq	(%rdx,%rdx,2), %rax
	leaq	(%rsi,%rax,8), %rax
	cmpb	$0, 20(%rax)
	je	.L60
	movq	0(%r13), %rdi
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L71:
	addq	$1, %rdx
	andq	%rcx, %rdx
	leaq	(%rdx,%rdx,2), %rax
	leaq	(%rsi,%rax,8), %rax
	cmpb	$0, 20(%rax)
	je	.L60
.L65:
	cmpq	(%rax), %rdi
	jne	.L71
	jmp	.L60
	.cfi_endproc
.LFE3818:
	.size	_ZN2v88internal8compiler7RefsMap14LookupOrInsertERKmPNS0_4ZoneE, .-_ZN2v88internal8compiler7RefsMap14LookupOrInsertERKmPNS0_4ZoneE
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
