	.file	"schedule.cc"
	.text
	.section	.text._ZN2v88internal12StdoutStreamD1Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD1Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StdoutStreamD1Ev
	.type	_ZN2v88internal12StdoutStreamD1Ev, @function
_ZN2v88internal12StdoutStreamD1Ev:
.LFB12088:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE12088:
	.size	_ZN2v88internal12StdoutStreamD1Ev, .-_ZN2v88internal12StdoutStreamD1Ev
	.section	.text._ZN2v88internal12StdoutStreamD0Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD0Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.type	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev, @function
_ZTv0_n24_N2v88internal12StdoutStreamD0Ev:
.LFB12216:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %rax
	addq	-24(%rax), %rdi
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	movq	%rax, 80(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12216:
	.size	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev, .-_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StdoutStreamD0Ev
	.type	_ZN2v88internal12StdoutStreamD0Ev, @function
_ZN2v88internal12StdoutStreamD0Ev:
.LFB12089:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12089:
	.size	_ZN2v88internal12StdoutStreamD0Ev, .-_ZN2v88internal12StdoutStreamD0Ev
	.section	.text._ZN2v88internal12StdoutStreamD1Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD1Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.type	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev, @function
_ZTv0_n24_N2v88internal12StdoutStreamD1Ev:
.LFB12217:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rax
	movq	-24(%rax), %rbx
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	addq	%rdi, %rbx
	movq	%rax, 80(%rbx)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	64(%rbx), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE12217:
	.size	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev, .-_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.section	.text._ZN2v88internal8compiler10BasicBlockC2EPNS0_4ZoneENS2_2IdE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler10BasicBlockC2EPNS0_4ZoneENS2_2IdE
	.type	_ZN2v88internal8compiler10BasicBlockC2EPNS0_4ZoneENS2_2IdE, @function
_ZN2v88internal8compiler10BasicBlockC2EPNS0_4ZoneENS2_2IdE:
.LFB10235:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movq	$-1, (%rdi)
	movb	$0, 8(%rdi)
	movl	$-1, 12(%rdi)
	movq	$0, 48(%rdi)
	movq	$0, 56(%rdi)
	movq	%rsi, 64(%rdi)
	movq	$0, 72(%rdi)
	movq	$0, 80(%rdi)
	movq	$0, 88(%rdi)
	movq	%rsi, 96(%rdi)
	movq	$0, 104(%rdi)
	movq	$0, 112(%rdi)
	movq	$0, 120(%rdi)
	movq	%rsi, 128(%rdi)
	movq	$0, 136(%rdi)
	movq	$0, 144(%rdi)
	movq	$0, 152(%rdi)
	movq	%rdx, 160(%rdi)
	movups	%xmm0, 16(%rdi)
	movups	%xmm0, 32(%rdi)
	ret
	.cfi_endproc
.LFE10235:
	.size	_ZN2v88internal8compiler10BasicBlockC2EPNS0_4ZoneENS2_2IdE, .-_ZN2v88internal8compiler10BasicBlockC2EPNS0_4ZoneENS2_2IdE
	.globl	_ZN2v88internal8compiler10BasicBlockC1EPNS0_4ZoneENS2_2IdE
	.set	_ZN2v88internal8compiler10BasicBlockC1EPNS0_4ZoneENS2_2IdE,_ZN2v88internal8compiler10BasicBlockC2EPNS0_4ZoneENS2_2IdE
	.section	.text._ZNK2v88internal8compiler10BasicBlock12LoopContainsEPS2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler10BasicBlock12LoopContainsEPS2_
	.type	_ZNK2v88internal8compiler10BasicBlock12LoopContainsEPS2_, @function
_ZNK2v88internal8compiler10BasicBlock12LoopContainsEPS2_:
.LFB10237:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	testq	%rax, %rax
	je	.L13
	movl	4(%rsi), %edx
	xorl	%r8d, %r8d
	cmpl	4(%rdi), %edx
	jl	.L11
	cmpl	4(%rax), %edx
	setl	%r8b
.L11:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	xorl	%r8d, %r8d
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE10237:
	.size	_ZNK2v88internal8compiler10BasicBlock12LoopContainsEPS2_, .-_ZNK2v88internal8compiler10BasicBlock12LoopContainsEPS2_
	.section	.text._ZN2v88internal8compiler10BasicBlock11set_controlENS2_7ControlE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler10BasicBlock11set_controlENS2_7ControlE
	.type	_ZN2v88internal8compiler10BasicBlock11set_controlENS2_7ControlE, @function
_ZN2v88internal8compiler10BasicBlock11set_controlENS2_7ControlE:
.LFB10241:
	.cfi_startproc
	endbr64
	movl	%esi, 52(%rdi)
	ret
	.cfi_endproc
.LFE10241:
	.size	_ZN2v88internal8compiler10BasicBlock11set_controlENS2_7ControlE, .-_ZN2v88internal8compiler10BasicBlock11set_controlENS2_7ControlE
	.section	.text._ZN2v88internal8compiler10BasicBlock17set_control_inputEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler10BasicBlock17set_control_inputEPNS1_4NodeE
	.type	_ZN2v88internal8compiler10BasicBlock17set_control_inputEPNS1_4NodeE, @function
_ZN2v88internal8compiler10BasicBlock17set_control_inputEPNS1_4NodeE:
.LFB10242:
	.cfi_startproc
	endbr64
	movq	80(%rdi), %rax
	cmpq	%rax, 72(%rdi)
	je	.L17
	cmpq	%rsi, -8(%rax)
	je	.L18
.L17:
	movq	%rsi, 56(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	subq	$8, %rax
	movq	%rsi, 56(%rdi)
	movq	%rax, 80(%rdi)
	ret
	.cfi_endproc
.LFE10242:
	.size	_ZN2v88internal8compiler10BasicBlock17set_control_inputEPNS1_4NodeE, .-_ZN2v88internal8compiler10BasicBlock17set_control_inputEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler10BasicBlock14set_loop_depthEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler10BasicBlock14set_loop_depthEi
	.type	_ZN2v88internal8compiler10BasicBlock14set_loop_depthEi, @function
_ZN2v88internal8compiler10BasicBlock14set_loop_depthEi:
.LFB10243:
	.cfi_startproc
	endbr64
	movl	%esi, 48(%rdi)
	ret
	.cfi_endproc
.LFE10243:
	.size	_ZN2v88internal8compiler10BasicBlock14set_loop_depthEi, .-_ZN2v88internal8compiler10BasicBlock14set_loop_depthEi
	.section	.text._ZN2v88internal8compiler10BasicBlock14set_rpo_numberEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler10BasicBlock14set_rpo_numberEi
	.type	_ZN2v88internal8compiler10BasicBlock14set_rpo_numberEi, @function
_ZN2v88internal8compiler10BasicBlock14set_rpo_numberEi:
.LFB10244:
	.cfi_startproc
	endbr64
	movl	%esi, 4(%rdi)
	ret
	.cfi_endproc
.LFE10244:
	.size	_ZN2v88internal8compiler10BasicBlock14set_rpo_numberEi, .-_ZN2v88internal8compiler10BasicBlock14set_rpo_numberEi
	.section	.text._ZN2v88internal8compiler10BasicBlock12set_loop_endEPS2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler10BasicBlock12set_loop_endEPS2_
	.type	_ZN2v88internal8compiler10BasicBlock12set_loop_endEPS2_, @function
_ZN2v88internal8compiler10BasicBlock12set_loop_endEPS2_:
.LFB10245:
	.cfi_startproc
	endbr64
	movq	%rsi, 40(%rdi)
	ret
	.cfi_endproc
.LFE10245:
	.size	_ZN2v88internal8compiler10BasicBlock12set_loop_endEPS2_, .-_ZN2v88internal8compiler10BasicBlock12set_loop_endEPS2_
	.section	.text._ZN2v88internal8compiler10BasicBlock15set_loop_headerEPS2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler10BasicBlock15set_loop_headerEPS2_
	.type	_ZN2v88internal8compiler10BasicBlock15set_loop_headerEPS2_, @function
_ZN2v88internal8compiler10BasicBlock15set_loop_headerEPS2_:
.LFB10246:
	.cfi_startproc
	endbr64
	movq	%rsi, 32(%rdi)
	ret
	.cfi_endproc
.LFE10246:
	.size	_ZN2v88internal8compiler10BasicBlock15set_loop_headerEPS2_, .-_ZN2v88internal8compiler10BasicBlock15set_loop_headerEPS2_
	.section	.text._ZN2v88internal8compiler10BasicBlock18GetCommonDominatorEPS2_S3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler10BasicBlock18GetCommonDominatorEPS2_S3_
	.type	_ZN2v88internal8compiler10BasicBlock18GetCommonDominatorEPS2_S3_, @function
_ZN2v88internal8compiler10BasicBlock18GetCommonDominatorEPS2_S3_:
.LFB10247:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	cmpq	%rsi, %rdi
	je	.L23
	.p2align 4,,10
	.p2align 3
.L31:
	movl	12(%rax), %edx
.L25:
	cmpl	12(%rsi), %edx
	jge	.L26
	movq	16(%rsi), %rsi
	cmpq	%rax, %rsi
	jne	.L25
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	movq	16(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L31
.L23:
	ret
	.cfi_endproc
.LFE10247:
	.size	_ZN2v88internal8compiler10BasicBlock18GetCommonDominatorEPS2_S3_, .-_ZN2v88internal8compiler10BasicBlock18GetCommonDominatorEPS2_S3_
	.section	.text._ZN2v88internal8compiler10BasicBlock5PrintEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler10BasicBlock5PrintEv
	.type	_ZN2v88internal8compiler10BasicBlock5PrintEv, @function
_ZN2v88internal8compiler10BasicBlock5PrintEv:
.LFB10248:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-304(%rbp), %r13
	leaq	-384(%rbp), %r14
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rbx
	subq	$352, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNSt8ios_baseC2Ev@PLT
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	stdout(%rip), %rdx
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movups	%xmm0, -72(%rbp)
	movups	%xmm0, -56(%rbp)
	movw	%ax, -80(%rbp)
	movq	%rbx, -304(%rbp)
	movq	$0, -88(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, -384(%rbp)
	addq	$40, %rax
	movq	%rax, -304(%rbp)
	call	_ZNSo9_M_insertIPKvEERSoT_@PLT
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rcx
	movq	%rax, -304(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-320(%rbp), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%r13, %rdi
	movq	%rbx, -304(%rbp)
	movq	%rax, -384(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L35
	addq	$352, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L35:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10248:
	.size	_ZN2v88internal8compiler10BasicBlock5PrintEv, .-_ZN2v88internal8compiler10BasicBlock5PrintEv
	.section	.rodata._ZN2v88internal8compilerlsERSoRKNS1_10BasicBlockE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"B"
	.section	.text._ZN2v88internal8compilerlsERSoRKNS1_10BasicBlockE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal8compilerlsERSoRKNS1_10BasicBlockE
	.type	_ZN2v88internal8compilerlsERSoRKNS1_10BasicBlockE, @function
_ZN2v88internal8compilerlsERSoRKNS1_10BasicBlockE:
.LFB10249:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	leaq	.LC0(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	160(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10249:
	.size	_ZN2v88internal8compilerlsERSoRKNS1_10BasicBlockE, .-_ZN2v88internal8compilerlsERSoRKNS1_10BasicBlockE
	.section	.rodata._ZN2v88internal8compilerlsERSoRKNS1_10BasicBlock7ControlE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"none"
.LC2:
	.string	"goto"
.LC3:
	.string	"call"
.LC4:
	.string	"branch"
.LC5:
	.string	"switch"
.LC6:
	.string	"deoptimize"
.LC7:
	.string	"tailcall"
.LC8:
	.string	"return"
.LC9:
	.string	"throw"
.LC10:
	.string	"unreachable code"
	.section	.text._ZN2v88internal8compilerlsERSoRKNS1_10BasicBlock7ControlE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal8compilerlsERSoRKNS1_10BasicBlock7ControlE
	.type	_ZN2v88internal8compilerlsERSoRKNS1_10BasicBlock7ControlE, @function
_ZN2v88internal8compilerlsERSoRKNS1_10BasicBlock7ControlE:
.LFB10250:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	cmpl	$8, (%rsi)
	ja	.L39
	movl	(%rsi), %eax
	leaq	.L41(%rip), %rdx
	movq	%rdi, %r12
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compilerlsERSoRKNS1_10BasicBlock7ControlE,"a",@progbits
	.align 4
	.align 4
.L41:
	.long	.L49-.L41
	.long	.L48-.L41
	.long	.L47-.L41
	.long	.L46-.L41
	.long	.L45-.L41
	.long	.L44-.L41
	.long	.L43-.L41
	.long	.L42-.L41
	.long	.L40-.L41
	.section	.text._ZN2v88internal8compilerlsERSoRKNS1_10BasicBlock7ControlE
	.p2align 4,,10
	.p2align 3
.L42:
	movl	$6, %edx
	leaq	.LC8(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	.cfi_restore_state
	movl	$5, %edx
	leaq	.LC9(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	.cfi_restore_state
	movl	$4, %edx
	leaq	.LC1(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore_state
	movl	$4, %edx
	leaq	.LC2(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	.cfi_restore_state
	movl	$4, %edx
	leaq	.LC3(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	.cfi_restore_state
	movl	$6, %edx
	leaq	.LC4(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	.cfi_restore_state
	movl	$6, %edx
	leaq	.LC5(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	.cfi_restore_state
	movl	$10, %edx
	leaq	.LC6(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	.cfi_restore_state
	movl	$8, %edx
	leaq	.LC7(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L39:
	.cfi_restore_state
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE10250:
	.size	_ZN2v88internal8compilerlsERSoRKNS1_10BasicBlock7ControlE, .-_ZN2v88internal8compilerlsERSoRKNS1_10BasicBlock7ControlE
	.section	.text._ZN2v88internal8compilerlsERSoRKNS1_10BasicBlock2IdE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal8compilerlsERSoRKNS1_10BasicBlock2IdE
	.type	_ZN2v88internal8compilerlsERSoRKNS1_10BasicBlock2IdE, @function
_ZN2v88internal8compilerlsERSoRKNS1_10BasicBlock2IdE:
.LFB10251:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rsi
	jmp	_ZNSo9_M_insertImEERSoT_@PLT
	.cfi_endproc
.LFE10251:
	.size	_ZN2v88internal8compilerlsERSoRKNS1_10BasicBlock2IdE, .-_ZN2v88internal8compilerlsERSoRKNS1_10BasicBlock2IdE
	.section	.text._ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE
	.type	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE, @function
_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE:
.LFB10255:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %rcx
	movq	56(%rdi), %rdx
	xorl	%r8d, %r8d
	movl	20(%rsi), %eax
	subq	%rcx, %rdx
	andl	$16777215, %eax
	sarq	$3, %rdx
	cmpl	%edx, %eax
	jnb	.L53
	movq	(%rcx,%rax,8), %r8
.L53:
	movq	%r8, %rax
	ret
	.cfi_endproc
.LFE10255:
	.size	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE, .-_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler8Schedule11IsScheduledEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler8Schedule11IsScheduledEPNS1_4NodeE
	.type	_ZN2v88internal8compiler8Schedule11IsScheduledEPNS1_4NodeE, @function
_ZN2v88internal8compiler8Schedule11IsScheduledEPNS1_4NodeE:
.LFB10256:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %rcx
	movq	56(%rdi), %rax
	xorl	%r8d, %r8d
	movl	20(%rsi), %edx
	subq	%rcx, %rax
	andl	$16777215, %edx
	sarq	$3, %rax
	cmpq	%rax, %rdx
	jnb	.L56
	cmpq	$0, (%rcx,%rdx,8)
	setne	%r8b
.L56:
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE10256:
	.size	_ZN2v88internal8compiler8Schedule11IsScheduledEPNS1_4NodeE, .-_ZN2v88internal8compiler8Schedule11IsScheduledEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler8Schedule12GetBlockByIdENS1_10BasicBlock2IdE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler8Schedule12GetBlockByIdENS1_10BasicBlock2IdE
	.type	_ZN2v88internal8compiler8Schedule12GetBlockByIdENS1_10BasicBlock2IdE, @function
_ZN2v88internal8compiler8Schedule12GetBlockByIdENS1_10BasicBlock2IdE:
.LFB10257:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	(%rax,%rsi,8), %rax
	ret
	.cfi_endproc
.LFE10257:
	.size	_ZN2v88internal8compiler8Schedule12GetBlockByIdENS1_10BasicBlock2IdE, .-_ZN2v88internal8compiler8Schedule12GetBlockByIdENS1_10BasicBlock2IdE
	.section	.text._ZN2v88internal8compiler8Schedule14ClearBlockByIdENS1_10BasicBlock2IdE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler8Schedule14ClearBlockByIdENS1_10BasicBlock2IdE
	.type	_ZN2v88internal8compiler8Schedule14ClearBlockByIdENS1_10BasicBlock2IdE, @function
_ZN2v88internal8compiler8Schedule14ClearBlockByIdENS1_10BasicBlock2IdE:
.LFB10258:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	$0, (%rax,%rsi,8)
	ret
	.cfi_endproc
.LFE10258:
	.size	_ZN2v88internal8compiler8Schedule14ClearBlockByIdENS1_10BasicBlock2IdE, .-_ZN2v88internal8compiler8Schedule14ClearBlockByIdENS1_10BasicBlock2IdE
	.section	.text._ZNK2v88internal8compiler8Schedule14SameBasicBlockEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler8Schedule14SameBasicBlockEPNS1_4NodeES4_
	.type	_ZNK2v88internal8compiler8Schedule14SameBasicBlockEPNS1_4NodeES4_, @function
_ZNK2v88internal8compiler8Schedule14SameBasicBlockEPNS1_4NodeES4_:
.LFB10259:
	.cfi_startproc
	endbr64
	movl	20(%rsi), %ecx
	movq	56(%rdi), %rax
	xorl	%r8d, %r8d
	movq	48(%rdi), %rsi
	andl	$16777215, %ecx
	subq	%rsi, %rax
	sarq	$3, %rax
	cmpl	%eax, %ecx
	jnb	.L61
	movq	(%rsi,%rcx,8), %rcx
	testq	%rcx, %rcx
	je	.L61
	movl	20(%rdx), %edx
	andl	$16777215, %edx
	cmpl	%edx, %eax
	jbe	.L61
	cmpq	%rcx, (%rsi,%rdx,8)
	sete	%r8b
.L61:
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE10259:
	.size	_ZNK2v88internal8compiler8Schedule14SameBasicBlockEPNS1_4NodeES4_, .-_ZNK2v88internal8compiler8Schedule14SameBasicBlockEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler8Schedule26EliminateRedundantPhiNodesEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler8Schedule26EliminateRedundantPhiNodesEv
	.type	_ZN2v88internal8compiler8Schedule26EliminateRedundantPhiNodesEv, @function
_ZN2v88internal8compiler8Schedule26EliminateRedundantPhiNodesEv:
.LFB10277:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -88(%rbp)
	.p2align 4,,10
	.p2align 3
.L70:
	movq	-88(%rbp), %rax
	movq	16(%rax), %r15
	movq	24(%rax), %rax
	movq	%rax, -80(%rbp)
	cmpq	%rax, %r15
	je	.L67
	movl	$1, %r9d
	.p2align 4,,10
	.p2align 3
.L82:
	movq	(%r15), %r14
	movq	144(%r14), %rax
	movq	72(%r14), %rdx
	subq	136(%r14), %rax
	movq	80(%r14), %rsi
	sarq	$3, %rax
	movl	%eax, %r13d
	cmpq	%rsi, %rdx
	je	.L71
	leal	-2(%rax), %r8d
	xorl	%ebx, %ebx
	movl	%r13d, %r12d
	salq	$3, %r8
	leaq	48(%r8), %rax
	movq	%r8, -64(%rbp)
	movq	%rax, -56(%rbp)
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L76:
	movq	%rsi, %rax
	addq	$1, %rbx
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rbx, %rax
	jbe	.L71
.L81:
	movq	(%rdx,%rbx,8), %r13
	leaq	0(,%rbx,8), %r10
	movq	0(%r13), %rax
	cmpw	$35, 16(%rax)
	jne	.L76
	movzbl	23(%r13), %eax
	movq	32(%r13), %r11
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L105
	cmpl	$1, %r12d
	jle	.L75
	movq	-56(%rbp), %rdi
	leaq	40(%r13), %rax
	addq	%r13, %rdi
	.p2align 4,,10
	.p2align 3
.L80:
	movq	(%rax), %rcx
	cmpq	%r11, %rcx
	je	.L79
	cmpq	%rcx, %r13
	jne	.L76
.L79:
	addq	$8, %rax
	cmpq	%rax, %rdi
	jne	.L80
.L75:
	movq	%r11, %rsi
	movq	%r13, %rdi
	movq	%r10, -72(%rbp)
	call	_ZN2v88internal8compiler4Node11ReplaceUsesEPS2_@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node4KillEv@PLT
	movq	72(%r14), %rdx
	movq	-72(%rbp), %r10
	movq	80(%r14), %rax
	leaq	(%rdx,%r10), %rdi
	leaq	8(%rdi), %rsi
	cmpq	%rsi, %rax
	je	.L85
	subq	%rsi, %rax
	movq	%rax, %rdx
	call	memmove@PLT
	movq	80(%r14), %rsi
	movq	72(%r14), %rdx
.L85:
	subq	$8, %rsi
	xorl	%r9d, %r9d
	movq	%rsi, %rax
	movq	%rsi, 80(%r14)
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rbx, %rax
	ja	.L81
	.p2align 4,,10
	.p2align 3
.L71:
	addq	$8, %r15
	cmpq	%r15, -80(%rbp)
	jne	.L82
	testb	%r9b, %r9b
	je	.L70
.L67:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L105:
	.cfi_restore_state
	movq	16(%r11), %rdi
	cmpl	$1, %r12d
	jle	.L88
	movq	-64(%rbp), %rcx
	leaq	24(%r11), %rax
	leaq	32(%r11,%rcx), %r11
	.p2align 4,,10
	.p2align 3
.L78:
	movq	(%rax), %rcx
	cmpq	%rcx, %r13
	je	.L89
	cmpq	%rdi, %rcx
	jne	.L76
.L89:
	addq	$8, %rax
	cmpq	%r11, %rax
	jne	.L78
.L88:
	movq	%rdi, %r11
	jmp	.L75
	.cfi_endproc
.LFE10277:
	.size	_ZN2v88internal8compiler8Schedule26EliminateRedundantPhiNodesEv, .-_ZN2v88internal8compiler8Schedule26EliminateRedundantPhiNodesEv
	.section	.text._ZN2v88internal8compiler8Schedule23EnsureCFGWellFormednessEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler8Schedule23EnsureCFGWellFormednessEv
	.type	_ZN2v88internal8compiler8Schedule23EnsureCFGWellFormednessEv, @function
_ZN2v88internal8compiler8Schedule23EnsureCFGWellFormednessEv:
.LFB10273:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	24(%r12), %rdx
	movq	16(%r12), %rcx
	movq	8(%rdi), %rdi
	movq	%rdx, %rsi
	subq	%rcx, %rsi
	je	.L128
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	addq	$7, %rsi
	andq	$-8, %rsi
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L130
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L109:
	movq	24(%r12), %rdx
	movq	16(%r12), %rcx
.L107:
	cmpq	%rdx, %rcx
	je	.L114
	subq	$8, %rdx
	leaq	15(%rcx), %rsi
	subq	%rcx, %rdx
	subq	%rax, %rsi
	shrq	$3, %rdx
	cmpq	$30, %rsi
	jbe	.L118
	movabsq	$2305843009213693948, %rsi
	testq	%rsi, %rdx
	je	.L118
	addq	$1, %rdx
	xorl	%esi, %esi
	movq	%rdx, %rdi
	shrq	%rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L112:
	movdqu	(%rcx,%rsi), %xmm0
	movups	%xmm0, (%rax,%rsi)
	addq	$16, %rsi
	cmpq	%rdi, %rsi
	jne	.L112
	movq	%rdx, %rdi
	andq	$-2, %rdi
	leaq	0(,%rdi,8), %rsi
	addq	%rsi, %rcx
	addq	%rsi, %rax
	cmpq	%rdi, %rdx
	je	.L114
	movq	(%rcx), %rdx
	movq	%rdx, (%rax)
.L114:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler8Schedule26EliminateRedundantPhiNodesEv
	.p2align 4,,10
	.p2align 3
.L128:
	.cfi_restore_state
	xorl	%eax, %eax
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L130:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L118:
	xorl	%esi, %esi
	.p2align 4,,10
	.p2align 3
.L111:
	movq	(%rcx,%rsi,8), %rdi
	movq	%rdi, (%rax,%rsi,8)
	movq	%rsi, %rdi
	addq	$1, %rsi
	cmpq	%rdi, %rdx
	jne	.L111
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler8Schedule26EliminateRedundantPhiNodesEv
	.cfi_endproc
.LFE10273:
	.size	_ZN2v88internal8compiler8Schedule23EnsureCFGWellFormednessEv, .-_ZN2v88internal8compiler8Schedule23EnsureCFGWellFormednessEv
	.section	.text._ZN2v88internal8compiler8Schedule19EnsureSplitEdgeFormEPNS1_10BasicBlockE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler8Schedule19EnsureSplitEdgeFormEPNS1_10BasicBlockE
	.type	_ZN2v88internal8compiler8Schedule19EnsureSplitEdgeFormEPNS1_10BasicBlockE, @function
_ZN2v88internal8compiler8Schedule19EnsureSplitEdgeFormEPNS1_10BasicBlockE:
.LFB10278:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE10278:
	.size	_ZN2v88internal8compiler8Schedule19EnsureSplitEdgeFormEPNS1_10BasicBlockE, .-_ZN2v88internal8compiler8Schedule19EnsureSplitEdgeFormEPNS1_10BasicBlockE
	.section	.text._ZN2v88internal8compiler8Schedule21PropagateDeferredMarkEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler8Schedule21PropagateDeferredMarkEv
	.type	_ZN2v88internal8compiler8Schedule21PropagateDeferredMarkEv, @function
_ZN2v88internal8compiler8Schedule21PropagateDeferredMarkEv:
.LFB10280:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r10d, %r10d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	.p2align 4,,10
	.p2align 3
.L135:
	movq	16(%rbx), %rcx
	movq	24(%rbx), %r9
	cmpq	%r9, %rcx
	je	.L132
	movl	$1, %r11d
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L136:
	addq	$8, %rcx
	cmpq	%rcx, %r9
	je	.L149
.L139:
	movq	(%rcx), %rsi
	cmpb	$0, 8(%rsi)
	jne	.L136
	movq	136(%rsi), %rax
	movq	144(%rsi), %rdi
	cmpq	%rdi, %rax
	je	.L136
	movl	$1, %r8d
	.p2align 4,,10
	.p2align 3
.L138:
	movq	(%rax), %rdx
	cmpb	$0, 8(%rdx)
	jne	.L137
	movl	4(%rsi), %r14d
	cmpl	%r14d, 4(%rdx)
	cmovl	%r10d, %r8d
.L137:
	addq	$8, %rax
	cmpq	%rax, %rdi
	jne	.L138
	testb	%r8b, %r8b
	je	.L136
	addq	$8, %rcx
	movb	$1, 8(%rsi)
	xorl	%r11d, %r11d
	cmpq	%rcx, %r9
	jne	.L139
	.p2align 4,,10
	.p2align 3
.L149:
	testb	%r11b, %r11b
	je	.L135
.L132:
	popq	%rbx
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10280:
	.size	_ZN2v88internal8compiler8Schedule21PropagateDeferredMarkEv, .-_ZN2v88internal8compiler8Schedule21PropagateDeferredMarkEv
	.section	.rodata._ZN2v88internal8compilerlsERSoRKNS1_8ScheduleE.str1.1,"aMS",@progbits,1
.LC11:
	.string	"--- BLOCK id:"
.LC12:
	.string	"--- BLOCK B"
.LC13:
	.string	" (deferred)"
.LC14:
	.string	" <- "
.LC15:
	.string	"id:"
.LC16:
	.string	", "
.LC17:
	.string	" ---\n"
.LC18:
	.string	"  "
.LC19:
	.string	" : "
.LC20:
	.string	"\n"
.LC21:
	.string	"Goto"
.LC22:
	.string	" -> "
	.section	.text._ZN2v88internal8compilerlsERSoRKNS1_8ScheduleE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal8compilerlsERSoRKNS1_8ScheduleE
	.type	_ZN2v88internal8compilerlsERSoRKNS1_8ScheduleE, @function
_ZN2v88internal8compilerlsERSoRKNS1_8ScheduleE:
.LFB10285:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	88(%rsi), %rcx
	movq	80(%rsi), %rax
	movq	%rcx, -72(%rbp)
	cmpq	%rcx, %rax
	je	.L181
.L151:
	movq	%rax, -64(%rbp)
	leaq	.LC18(%rip), %r14
	.p2align 4,,10
	.p2align 3
.L174:
	movq	-64(%rbp), %rax
	movq	(%rax), %r13
	testq	%r13, %r13
	je	.L153
	cmpl	$-1, 4(%r13)
	je	.L182
	movl	$11, %edx
	leaq	.LC12(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	4(%r13), %esi
	movq	%r15, %rdi
	call	_ZNSolsEi@PLT
	cmpb	$0, 8(%r13)
	jne	.L183
.L156:
	movq	136(%r13), %rax
	cmpq	%rax, 144(%r13)
	je	.L157
	movl	$4, %edx
	leaq	.LC14(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	144(%r13), %rax
	movq	136(%r13), %rbx
	movq	%rax, -56(%rbp)
	cmpq	%rbx, %rax
	je	.L157
	movq	(%rbx), %r12
	addq	$8, %rbx
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L158:
	leaq	.LC0(%rip), %rsi
	movq	%r15, %rdi
	movl	$1, %edx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	4(%r12), %esi
	movq	%r15, %rdi
	call	_ZNSolsEi@PLT
	cmpq	-56(%rbp), %rbx
	je	.L157
.L184:
	movl	$2, %edx
	movq	%r15, %rdi
	movq	(%rbx), %r12
	addq	$8, %rbx
	leaq	.LC16(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L160:
	cmpl	$-1, 4(%r12)
	jne	.L158
	leaq	.LC15(%rip), %rsi
	movq	%r15, %rdi
	movl	$3, %edx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	160(%r12), %rsi
	movq	%r15, %rdi
	call	_ZNSolsEi@PLT
	cmpq	-56(%rbp), %rbx
	jne	.L184
.L157:
	movl	$5, %edx
	leaq	.LC17(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	80(%r13), %rax
	movq	72(%r13), %rbx
	movq	%rax, -56(%rbp)
	cmpq	%rbx, %rax
	jne	.L167
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L164:
	movl	$1, %edx
	leaq	.LC20(%rip), %rsi
	movq	%r15, %rdi
	addq	$8, %rbx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpq	%rbx, -56(%rbp)
	je	.L166
.L167:
	movq	(%rbx), %r12
	movl	$2, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r15, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compilerlsERSoRKNS1_4NodeE@PLT
	cmpq	$0, 8(%r12)
	je	.L164
	movq	%r15, %rdi
	movl	$3, %edx
	leaq	.LC19(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r12), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compilerlsERSoNS1_4TypeE@PLT
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L170:
	movl	$1, %edx
	leaq	.LC20(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	.p2align 4,,10
	.p2align 3
.L153:
	addq	$8, -64(%rbp)
	movq	-64(%rbp), %rax
	cmpq	%rax, -72(%rbp)
	jne	.L174
.L178:
	addq	$40, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L166:
	.cfi_restore_state
	movl	52(%r13), %eax
	testl	%eax, %eax
	je	.L153
	movq	%r14, %rsi
	movl	$2, %edx
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	56(%r13), %rsi
	testq	%rsi, %rsi
	je	.L168
	movq	%r15, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_4NodeE@PLT
.L169:
	movl	$4, %edx
	leaq	.LC22(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	104(%r13), %rbx
	movq	112(%r13), %r13
	cmpq	%rbx, %r13
	je	.L170
	movq	(%rbx), %r12
	addq	$8, %rbx
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L171:
	leaq	.LC0(%rip), %rsi
	movq	%r15, %rdi
	movl	$1, %edx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	4(%r12), %esi
	movq	%r15, %rdi
	call	_ZNSolsEi@PLT
.L172:
	cmpq	%rbx, %r13
	je	.L170
	movl	$2, %edx
	movq	%r15, %rdi
	movq	(%rbx), %r12
	addq	$8, %rbx
	leaq	.LC16(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L173:
	cmpl	$-1, 4(%r12)
	jne	.L171
	movq	%r15, %rdi
	movl	$3, %edx
	leaq	.LC15(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	160(%r12), %rsi
	movq	%r15, %rdi
	call	_ZNSolsEi@PLT
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L182:
	movl	$13, %edx
	leaq	.LC11(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	160(%r13), %esi
	movq	%r15, %rdi
	call	_ZNSolsEi@PLT
	cmpb	$0, 8(%r13)
	je	.L156
.L183:
	movl	$11, %edx
	leaq	.LC13(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L156
.L181:
	movq	24(%rsi), %rcx
	movq	16(%rsi), %rax
	movq	%rcx, -72(%rbp)
	cmpq	%rcx, %rax
	jne	.L151
	jmp	.L178
.L168:
	movl	$4, %edx
	leaq	.LC21(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L169
	.cfi_endproc
.LFE10285:
	.size	_ZN2v88internal8compilerlsERSoRKNS1_8ScheduleE, .-_ZN2v88internal8compilerlsERSoRKNS1_8ScheduleE
	.section	.rodata._ZNSt6vectorIPN2v88internal8compiler10BasicBlockENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_.str1.1,"aMS",@progbits,1
.LC23:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIPN2v88internal8compiler10BasicBlockENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPN2v88internal8compiler10BasicBlockENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal8compiler10BasicBlockENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	.type	_ZNSt6vectorIPN2v88internal8compiler10BasicBlockENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_, @function
_ZNSt6vectorIPN2v88internal8compiler10BasicBlockENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_:
.LFB11489:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r14
	movq	8(%rdi), %rbx
	movq	%r14, %rax
	subq	%rbx, %rax
	sarq	$3, %rax
	cmpq	$268435455, %rax
	je	.L223
	movq	%rsi, %r8
	movq	%rdi, %r13
	movq	%rsi, %r12
	movq	%rdx, %r15
	subq	%rbx, %r8
	testq	%rax, %rax
	je	.L201
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L224
	movl	$2147483640, %esi
	movl	$2147483640, %edx
.L187:
	movq	0(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	%rsi, %rcx
	jb	.L225
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L190:
	leaq	(%rax,%rdx), %rdi
	leaq	8(%rax), %rcx
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L224:
	testq	%rdx, %rdx
	jne	.L226
	movl	$8, %ecx
	xorl	%edi, %edi
	xorl	%eax, %eax
.L188:
	movq	(%r15), %rdx
	movq	%rdx, (%rax,%r8)
	cmpq	%rbx, %r12
	je	.L191
	leaq	-8(%r12), %r8
	leaq	15(%rax), %rdx
	subq	%rbx, %r8
	subq	%rbx, %rdx
	movq	%r8, %rcx
	shrq	$3, %rcx
	cmpq	$30, %rdx
	jbe	.L204
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rcx
	je	.L204
	leaq	1(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L193:
	movdqu	(%rbx,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L193
	movq	%rsi, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rcx
	addq	%rcx, %rbx
	addq	%rax, %rcx
	cmpq	%rdx, %rsi
	je	.L195
	movq	(%rbx), %rdx
	movq	%rdx, (%rcx)
.L195:
	leaq	16(%rax,%r8), %rcx
.L191:
	cmpq	%r14, %r12
	je	.L196
	subq	%r12, %r14
	leaq	15(%r12), %rdx
	leaq	-8(%r14), %r9
	subq	%rcx, %rdx
	movq	%r9, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rdx
	jbe	.L205
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rsi
	je	.L205
	leaq	1(%rsi), %r8
	xorl	%edx, %edx
	movq	%r8, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L198:
	movdqu	(%r12,%rdx), %xmm2
	movups	%xmm2, (%rcx,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rsi
	jne	.L198
	movq	%r8, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rsi
	leaq	(%rcx,%rsi), %r10
	addq	%rsi, %r12
	cmpq	%rdx, %r8
	je	.L200
	movq	(%r12), %rdx
	movq	%rdx, (%r10)
.L200:
	leaq	8(%rcx,%r9), %rcx
.L196:
	movq	%rax, %xmm0
	movq	%rcx, %xmm3
	movq	%rdi, 24(%r13)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 8(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L201:
	.cfi_restore_state
	movl	$8, %esi
	movl	$8, %edx
	jmp	.L187
	.p2align 4,,10
	.p2align 3
.L205:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L197:
	movq	(%r12,%rdx,8), %r8
	movq	%r8, (%rcx,%rdx,8)
	movq	%rdx, %r8
	addq	$1, %rdx
	cmpq	%rsi, %r8
	jne	.L197
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L204:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L192:
	movq	(%rbx,%rdx,8), %rsi
	movq	%rsi, (%rax,%rdx,8)
	movq	%rdx, %rsi
	addq	$1, %rdx
	cmpq	%rsi, %rcx
	jne	.L192
	jmp	.L195
.L225:
	movq	%r8, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %r8
	jmp	.L190
.L223:
	leaq	.LC23(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L226:
	cmpq	$268435455, %rdx
	movl	$268435455, %eax
	cmova	%rax, %rdx
	salq	$3, %rdx
	movq	%rdx, %rsi
	jmp	.L187
	.cfi_endproc
.LFE11489:
	.size	_ZNSt6vectorIPN2v88internal8compiler10BasicBlockENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_, .-_ZNSt6vectorIPN2v88internal8compiler10BasicBlockENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	.section	.text._ZN2v88internal8compiler8Schedule14MoveSuccessorsEPNS1_10BasicBlockES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler8Schedule14MoveSuccessorsEPNS1_10BasicBlockES4_
	.type	_ZN2v88internal8compiler8Schedule14MoveSuccessorsEPNS1_10BasicBlockES4_, @function
_ZN2v88internal8compiler8Schedule14MoveSuccessorsEPNS1_10BasicBlockES4_:
.LFB10282:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	104(%rsi), %r14
	movq	112(%rsi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	%r15, %r14
	je	.L227
	movq	%rsi, %rbx
	leaq	96(%rdx), %rdi
	leaq	-64(%rbp), %rcx
	movq	%rdx, %r12
	.p2align 4,,10
	.p2align 3
.L232:
	movq	(%r14), %r13
	movq	112(%r12), %rsi
	movq	%r13, -64(%rbp)
	cmpq	120(%r12), %rsi
	je	.L229
	movq	%r13, (%rsi)
	addq	$8, 112(%r12)
.L230:
	movq	144(%r13), %rdx
	movq	136(%r13), %rax
	cmpq	%rax, %rdx
	jne	.L235
	jmp	.L236
	.p2align 4,,10
	.p2align 3
.L234:
	addq	$8, %rax
	cmpq	%rax, %rdx
	je	.L236
.L235:
	cmpq	%rbx, (%rax)
	jne	.L234
	movq	%r12, (%rax)
	addq	$8, %rax
	cmpq	%rax, %rdx
	jne	.L235
.L236:
	addq	$8, %r14
	cmpq	%r14, %r15
	jne	.L232
	movq	104(%rbx), %rax
	cmpq	112(%rbx), %rax
	je	.L227
	movq	%rax, 112(%rbx)
.L227:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L243
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L229:
	.cfi_restore_state
	movq	%rcx, %rdx
	movq	%rcx, -80(%rbp)
	movq	%rdi, -72(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler10BasicBlockENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	movq	-80(%rbp), %rcx
	movq	-72(%rbp), %rdi
	jmp	.L230
.L243:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10282:
	.size	_ZN2v88internal8compiler8Schedule14MoveSuccessorsEPNS1_10BasicBlockES4_, .-_ZN2v88internal8compiler8Schedule14MoveSuccessorsEPNS1_10BasicBlockES4_
	.section	.text._ZN2v88internal8compiler10BasicBlock14AddPredecessorEPS2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler10BasicBlock14AddPredecessorEPS2_
	.type	_ZN2v88internal8compiler10BasicBlock14AddPredecessorEPS2_, @function
_ZN2v88internal8compiler10BasicBlock14AddPredecessorEPS2_:
.LFB10239:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rsi, -8(%rbp)
	movq	144(%rdi), %rsi
	cmpq	152(%rdi), %rsi
	je	.L245
	movq	-8(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 144(%rdi)
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L245:
	.cfi_restore_state
	leaq	-8(%rbp), %rdx
	subq	$-128, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler10BasicBlockENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10239:
	.size	_ZN2v88internal8compiler10BasicBlock14AddPredecessorEPS2_, .-_ZN2v88internal8compiler10BasicBlock14AddPredecessorEPS2_
	.section	.text._ZN2v88internal8compiler10BasicBlock12AddSuccessorEPS2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler10BasicBlock12AddSuccessorEPS2_
	.type	_ZN2v88internal8compiler10BasicBlock12AddSuccessorEPS2_, @function
_ZN2v88internal8compiler10BasicBlock12AddSuccessorEPS2_:
.LFB10238:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rsi, -8(%rbp)
	movq	112(%rdi), %rsi
	cmpq	120(%rdi), %rsi
	je	.L249
	movq	-8(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 112(%rdi)
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L249:
	.cfi_restore_state
	leaq	-8(%rbp), %rdx
	addq	$96, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler10BasicBlockENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10238:
	.size	_ZN2v88internal8compiler10BasicBlock12AddSuccessorEPS2_, .-_ZN2v88internal8compiler10BasicBlock12AddSuccessorEPS2_
	.section	.text._ZN2v88internal8compiler8Schedule12AddSuccessorEPNS1_10BasicBlockES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler8Schedule12AddSuccessorEPNS1_10BasicBlockES4_
	.type	_ZN2v88internal8compiler8Schedule12AddSuccessorEPNS1_10BasicBlockES4_, @function
_ZN2v88internal8compiler8Schedule12AddSuccessorEPNS1_10BasicBlockES4_:
.LFB10281:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdx, %rbx
	subq	$16, %rsp
	movq	112(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	%rdx, -32(%rbp)
	cmpq	120(%r12), %rsi
	je	.L253
	movq	%rdx, (%rsi)
	addq	$8, 112(%r12)
.L254:
	movq	%r12, -32(%rbp)
	movq	144(%rbx), %rsi
	cmpq	152(%rbx), %rsi
	je	.L255
	movq	%r12, (%rsi)
	addq	$8, 144(%rbx)
.L252:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L259
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L253:
	.cfi_restore_state
	leaq	-32(%rbp), %rdx
	leaq	96(%r12), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler10BasicBlockENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L255:
	leaq	-32(%rbp), %rdx
	leaq	128(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler10BasicBlockENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L252
.L259:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10281:
	.size	_ZN2v88internal8compiler8Schedule12AddSuccessorEPNS1_10BasicBlockES4_, .-_ZN2v88internal8compiler8Schedule12AddSuccessorEPNS1_10BasicBlockES4_
	.section	.text._ZN2v88internal8compiler8Schedule7AddGotoEPNS1_10BasicBlockES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler8Schedule7AddGotoEPNS1_10BasicBlockES4_
	.type	_ZN2v88internal8compiler8Schedule7AddGotoEPNS1_10BasicBlockES4_, @function
_ZN2v88internal8compiler8Schedule7AddGotoEPNS1_10BasicBlockES4_:
.LFB10263:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	$1, 52(%rsi)
	movq	112(%rsi), %rsi
	movq	%rdx, -32(%rbp)
	cmpq	120(%rbx), %rsi
	je	.L261
	movq	%rdx, (%rsi)
	addq	$8, 112(%rbx)
.L262:
	movq	%rbx, -32(%rbp)
	movq	144(%r12), %rsi
	cmpq	152(%r12), %rsi
	je	.L263
	movq	%rbx, (%rsi)
	addq	$8, 144(%r12)
.L260:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L267
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L261:
	.cfi_restore_state
	leaq	-32(%rbp), %rdx
	leaq	96(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler10BasicBlockENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L262
	.p2align 4,,10
	.p2align 3
.L263:
	leaq	-32(%rbp), %rdx
	leaq	128(%r12), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler10BasicBlockENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L260
.L267:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10263:
	.size	_ZN2v88internal8compiler8Schedule7AddGotoEPNS1_10BasicBlockES4_, .-_ZN2v88internal8compiler8Schedule7AddGotoEPNS1_10BasicBlockES4_
	.section	.text._ZN2v88internal8compiler8Schedule13NewBasicBlockEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler8Schedule13NewBasicBlockEv
	.type	_ZN2v88internal8compiler8Schedule13NewBasicBlockEv, @function
_ZN2v88internal8compiler8Schedule13NewBasicBlockEv:
.LFB10260:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	24(%rdi), %r12
	subq	16(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rdi
	sarq	$3, %r12
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$167, %rdx
	jbe	.L275
	leaq	168(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L270:
	movq	(%rbx), %rdx
	pxor	%xmm0, %xmm0
	movb	$0, 8(%rax)
	movq	$-1, (%rax)
	movl	$-1, 12(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	%rdx, 64(%rax)
	movq	$0, 72(%rax)
	movq	$0, 80(%rax)
	movq	$0, 88(%rax)
	movq	%rdx, 96(%rax)
	movq	$0, 104(%rax)
	movq	$0, 112(%rax)
	movq	$0, 120(%rax)
	movq	%rdx, 128(%rax)
	movq	$0, 136(%rax)
	movq	$0, 144(%rax)
	movq	$0, 152(%rax)
	movq	%r12, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	24(%rbx), %rsi
	movq	%rax, -32(%rbp)
	cmpq	32(%rbx), %rsi
	je	.L271
	movq	%rax, (%rsi)
	addq	$8, 24(%rbx)
.L272:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	movq	-32(%rbp), %rax
	jne	.L276
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L271:
	.cfi_restore_state
	leaq	-32(%rbp), %rdx
	leaq	8(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler10BasicBlockENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L272
	.p2align 4,,10
	.p2align 3
.L275:
	movl	$168, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L270
.L276:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10260:
	.size	_ZN2v88internal8compiler8Schedule13NewBasicBlockEv, .-_ZN2v88internal8compiler8Schedule13NewBasicBlockEv
	.section	.rodata._ZN2v88internal8compiler8ScheduleC2EPNS0_4ZoneEm.str1.1,"aMS",@progbits,1
.LC24:
	.string	"vector::reserve"
	.section	.text._ZN2v88internal8compiler8ScheduleC2EPNS0_4ZoneEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler8ScheduleC2EPNS0_4ZoneEm
	.type	_ZN2v88internal8compiler8ScheduleC2EPNS0_4ZoneEm, @function
_ZN2v88internal8compiler8ScheduleC2EPNS0_4ZoneEm:
.LFB10253:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %xmm1
	punpcklqdq	%xmm1, %xmm1
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%rsi, 40(%rdi)
	movq	%rsi, 72(%rdi)
	movq	$0, 32(%rdi)
	movq	$0, 64(%rdi)
	movq	$0, 96(%rdi)
	movups	%xmm1, (%rdi)
	pxor	%xmm1, %xmm1
	movups	%xmm1, 16(%rdi)
	movups	%xmm1, 48(%rdi)
	movups	%xmm1, 80(%rdi)
	call	_ZN2v88internal8compiler8Schedule13NewBasicBlockEv
	movq	%rbx, %rdi
	movq	%rax, 104(%rbx)
	call	_ZN2v88internal8compiler8Schedule13NewBasicBlockEv
	movq	%rax, 112(%rbx)
	cmpq	$268435455, %r13
	ja	.L302
	movq	48(%rbx), %r12
	movq	64(%rbx), %rax
	subq	%r12, %rax
	sarq	$3, %rax
	cmpq	%rax, %r13
	ja	.L303
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L303:
	.cfi_restore_state
	movq	56(%rbx), %r14
	leaq	0(,%r13,8), %r8
	xorl	%eax, %eax
	movq	%r14, %r15
	subq	%r12, %r15
	testq	%r13, %r13
	je	.L280
	movq	40(%rbx), %rdi
	movq	%r8, %rsi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	%rdx, %r8
	ja	.L304
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L280:
	cmpq	%r14, %r12
	je	.L287
	leaq	-8(%r14), %rdx
	leaq	15(%r12), %rcx
	subq	%r12, %rdx
	subq	%rax, %rcx
	shrq	$3, %rdx
	cmpq	$30, %rcx
	jbe	.L291
	movabsq	$2305843009213693948, %rcx
	testq	%rcx, %rdx
	je	.L291
	leaq	1(%rdx), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L285:
	movdqu	(%r12,%rdx), %xmm2
	movups	%xmm2, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rsi, %rdx
	jne	.L285
	movq	%rcx, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rsi
	addq	%rsi, %r12
	addq	%rax, %rsi
	cmpq	%rdx, %rcx
	je	.L287
	movq	(%r12), %rdx
	movq	%rdx, (%rsi)
.L287:
	movq	%rax, 48(%rbx)
	addq	%rax, %r15
	addq	%r8, %rax
	movq	%r15, 56(%rbx)
	movq	%rax, 64(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L291:
	.cfi_restore_state
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L284:
	movq	(%r12,%rcx,8), %rsi
	movq	%rsi, (%rax,%rcx,8)
	movq	%rcx, %rsi
	addq	$1, %rcx
	cmpq	%rdx, %rsi
	jne	.L284
	jmp	.L287
	.p2align 4,,10
	.p2align 3
.L304:
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %r8
	jmp	.L280
.L302:
	leaq	.LC24(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE10253:
	.size	_ZN2v88internal8compiler8ScheduleC2EPNS0_4ZoneEm, .-_ZN2v88internal8compiler8ScheduleC2EPNS0_4ZoneEm
	.globl	_ZN2v88internal8compiler8ScheduleC1EPNS0_4ZoneEm
	.set	_ZN2v88internal8compiler8ScheduleC1EPNS0_4ZoneEm,_ZN2v88internal8compiler8ScheduleC2EPNS0_4ZoneEm
	.section	.text._ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	.type	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_, @function
_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_:
.LFB11492:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r14
	movq	8(%rdi), %rbx
	movq	%r14, %rax
	subq	%rbx, %rax
	sarq	$3, %rax
	cmpq	$268435455, %rax
	je	.L343
	movq	%rsi, %r8
	movq	%rdi, %r13
	movq	%rsi, %r12
	movq	%rdx, %r15
	subq	%rbx, %r8
	testq	%rax, %rax
	je	.L321
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L344
	movl	$2147483640, %esi
	movl	$2147483640, %edx
.L307:
	movq	0(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	%rsi, %rcx
	jb	.L345
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L310:
	leaq	(%rax,%rdx), %rdi
	leaq	8(%rax), %rcx
	jmp	.L308
	.p2align 4,,10
	.p2align 3
.L344:
	testq	%rdx, %rdx
	jne	.L346
	movl	$8, %ecx
	xorl	%edi, %edi
	xorl	%eax, %eax
.L308:
	movq	(%r15), %rdx
	movq	%rdx, (%rax,%r8)
	cmpq	%rbx, %r12
	je	.L311
	leaq	-8(%r12), %r8
	leaq	15(%rax), %rdx
	subq	%rbx, %r8
	subq	%rbx, %rdx
	movq	%r8, %rcx
	shrq	$3, %rcx
	cmpq	$30, %rdx
	jbe	.L324
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rcx
	je	.L324
	leaq	1(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L313:
	movdqu	(%rbx,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L313
	movq	%rsi, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rcx
	addq	%rcx, %rbx
	addq	%rax, %rcx
	cmpq	%rdx, %rsi
	je	.L315
	movq	(%rbx), %rdx
	movq	%rdx, (%rcx)
.L315:
	leaq	16(%rax,%r8), %rcx
.L311:
	cmpq	%r14, %r12
	je	.L316
	subq	%r12, %r14
	leaq	15(%r12), %rdx
	leaq	-8(%r14), %r9
	subq	%rcx, %rdx
	movq	%r9, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rdx
	jbe	.L325
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rsi
	je	.L325
	leaq	1(%rsi), %r8
	xorl	%edx, %edx
	movq	%r8, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L318:
	movdqu	(%r12,%rdx), %xmm2
	movups	%xmm2, (%rcx,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rsi
	jne	.L318
	movq	%r8, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rsi
	leaq	(%rcx,%rsi), %r10
	addq	%rsi, %r12
	cmpq	%rdx, %r8
	je	.L320
	movq	(%r12), %rdx
	movq	%rdx, (%r10)
.L320:
	leaq	8(%rcx,%r9), %rcx
.L316:
	movq	%rax, %xmm0
	movq	%rcx, %xmm3
	movq	%rdi, 24(%r13)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 8(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L321:
	.cfi_restore_state
	movl	$8, %esi
	movl	$8, %edx
	jmp	.L307
	.p2align 4,,10
	.p2align 3
.L325:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L317:
	movq	(%r12,%rdx,8), %r8
	movq	%r8, (%rcx,%rdx,8)
	movq	%rdx, %r8
	addq	$1, %rdx
	cmpq	%rsi, %r8
	jne	.L317
	jmp	.L320
	.p2align 4,,10
	.p2align 3
.L324:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L312:
	movq	(%rbx,%rdx,8), %rsi
	movq	%rsi, (%rax,%rdx,8)
	movq	%rdx, %rsi
	addq	$1, %rdx
	cmpq	%rsi, %rcx
	jne	.L312
	jmp	.L315
.L345:
	movq	%r8, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %r8
	jmp	.L310
.L343:
	leaq	.LC23(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L346:
	cmpq	$268435455, %rdx
	movl	$268435455, %eax
	cmova	%rax, %rdx
	salq	$3, %rdx
	movq	%rdx, %rsi
	jmp	.L307
	.cfi_endproc
.LFE11492:
	.size	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_, .-_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	.section	.text._ZN2v88internal8compiler10BasicBlock7AddNodeEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler10BasicBlock7AddNodeEPNS1_4NodeE
	.type	_ZN2v88internal8compiler10BasicBlock7AddNodeEPNS1_4NodeE, @function
_ZN2v88internal8compiler10BasicBlock7AddNodeEPNS1_4NodeE:
.LFB10240:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rsi, -8(%rbp)
	movq	80(%rdi), %rsi
	cmpq	88(%rdi), %rsi
	je	.L348
	movq	-8(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 80(%rdi)
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L348:
	.cfi_restore_state
	leaq	-8(%rbp), %rdx
	addq	$64, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10240:
	.size	_ZN2v88internal8compiler10BasicBlock7AddNodeEPNS1_4NodeE, .-_ZN2v88internal8compiler10BasicBlock7AddNodeEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler8Schedule8MovePhisEPNS1_10BasicBlockES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler8Schedule8MovePhisEPNS1_10BasicBlockES4_
	.type	_ZN2v88internal8compiler8Schedule8MovePhisEPNS1_10BasicBlockES4_, @function
_ZN2v88internal8compiler8Schedule8MovePhisEPNS1_10BasicBlockES4_:
.LFB10279:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$40, %rsp
	movq	72(%rsi), %rdx
	movq	80(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-64(%rbp), %rax
	movq	%rax, -72(%rbp)
	cmpq	%rsi, %rdx
	jne	.L352
	jmp	.L351
	.p2align 4,,10
	.p2align 3
.L354:
	movq	%rsi, %rax
	addq	$1, %rbx
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rbx, %rax
	jbe	.L351
.L352:
	movq	(%rdx,%rbx,8), %r12
	leaq	0(,%rbx,8), %r8
	movq	(%r12), %rax
	cmpw	$35, 16(%rax)
	jne	.L354
	movq	%r12, -64(%rbp)
	movq	80(%r15), %rsi
	cmpq	88(%r15), %rsi
	je	.L355
	movq	%r12, (%rsi)
	addq	$8, 80(%r15)
.L356:
	movq	72(%r14), %rdi
	movq	80(%r14), %rdx
	addq	%r8, %rdi
	leaq	8(%rdi), %rsi
	cmpq	%rsi, %rdx
	je	.L357
	subq	%rsi, %rdx
	call	memmove@PLT
	movq	80(%r14), %rsi
.L357:
	subq	$8, %rsi
	movq	%rsi, 80(%r14)
	movl	20(%r12), %eax
	movq	48(%r13), %rdx
	andl	$16777215, %eax
	movq	%r15, (%rdx,%rax,8)
	movq	80(%r14), %rsi
	movq	72(%r14), %rdx
	movq	%rsi, %rax
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rbx, %rax
	ja	.L352
	.p2align 4,,10
	.p2align 3
.L351:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L364
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L355:
	.cfi_restore_state
	movq	-72(%rbp), %rdx
	leaq	64(%r15), %rdi
	movq	%r8, -80(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	movq	-80(%rbp), %r8
	jmp	.L356
.L364:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10279:
	.size	_ZN2v88internal8compiler8Schedule8MovePhisEPNS1_10BasicBlockES4_, .-_ZN2v88internal8compiler8Schedule8MovePhisEPNS1_10BasicBlockES4_
	.section	.rodata._ZNSt6vectorIPN2v88internal8compiler10BasicBlockENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm.str1.1,"aMS",@progbits,1
.LC25:
	.string	"vector::_M_default_append"
	.section	.text._ZNSt6vectorIPN2v88internal8compiler10BasicBlockENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm,"axG",@progbits,_ZNSt6vectorIPN2v88internal8compiler10BasicBlockENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal8compiler10BasicBlockENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm
	.type	_ZNSt6vectorIPN2v88internal8compiler10BasicBlockENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm, @function
_ZNSt6vectorIPN2v88internal8compiler10BasicBlockENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm:
.LFB11514:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L393
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movl	$268435455, %esi
	subq	$24, %rsp
	movq	16(%rdi), %rcx
	movq	24(%r13), %rax
	movq	%rcx, %rdx
	subq	8(%rdi), %rdx
	subq	%rcx, %rax
	movq	%rsi, %rdi
	movq	%rdx, %r14
	sarq	$3, %rax
	sarq	$3, %r14
	subq	%r14, %rdi
	cmpq	%rbx, %rax
	jb	.L367
	salq	$3, %rbx
	movq	%rcx, %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	call	memset@PLT
	movq	%rax, %rcx
	addq	%rbx, %rcx
	movq	%rcx, 16(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L393:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L367:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	cmpq	%rbx, %rdi
	jb	.L396
	cmpq	%r14, %rbx
	movq	%r14, %r15
	movq	0(%r13), %rdi
	cmovnb	%rbx, %r15
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	addq	%r14, %r15
	cmpq	$268435455, %r15
	cmova	%rsi, %r15
	subq	%r12, %rax
	salq	$3, %r15
	movq	%r15, %rsi
	cmpq	%r15, %rax
	jb	.L397
	addq	%r12, %rsi
	movq	%rsi, 16(%rdi)
.L371:
	leaq	0(,%rbx,8), %r8
	leaq	(%r12,%rdx), %rdi
	xorl	%esi, %esi
	movq	%r8, %rdx
	call	memset@PLT
	movq	16(%r13), %rax
	movq	8(%r13), %rdx
	cmpq	%rdx, %rax
	je	.L376
	subq	$8, %rax
	leaq	15(%rdx), %rcx
	subq	%rdx, %rax
	subq	%r12, %rcx
	movq	%rax, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rcx
	jbe	.L373
	movabsq	$2305843009213693948, %rcx
	testq	%rcx, %rsi
	je	.L373
	addq	$1, %rsi
	movq	%rdx, %rdi
	movq	%r12, %rax
	movq	%rsi, %rcx
	subq	%r12, %rdi
	shrq	%rcx
	salq	$4, %rcx
	addq	%r12, %rcx
	.p2align 4,,10
	.p2align 3
.L374:
	movdqu	(%rax,%rdi), %xmm0
	addq	$16, %rax
	movups	%xmm0, -16(%rax)
	cmpq	%rax, %rcx
	jne	.L374
	movq	%rsi, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rcx
	addq	%rcx, %rdx
	addq	%r12, %rcx
	cmpq	%rax, %rsi
	je	.L376
	movq	(%rdx), %rax
	movq	%rax, (%rcx)
.L376:
	addq	%r14, %rbx
	movq	%r12, 8(%r13)
	leaq	(%r12,%rbx,8), %rax
	addq	%r15, %r12
	movq	%rax, 16(%r13)
	movq	%r12, 24(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L373:
	.cfi_restore_state
	leaq	8(%rax,%r12), %rsi
	subq	%r12, %rdx
	movq	%r12, %rax
	.p2align 4,,10
	.p2align 3
.L378:
	movq	(%rax,%rdx), %rcx
	addq	$8, %rax
	movq	%rcx, -8(%rax)
	cmpq	%rax, %rsi
	jne	.L378
	jmp	.L376
.L397:
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %r12
	jmp	.L371
.L396:
	leaq	.LC25(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE11514:
	.size	_ZNSt6vectorIPN2v88internal8compiler10BasicBlockENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm, .-_ZNSt6vectorIPN2v88internal8compiler10BasicBlockENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm
	.section	.text._ZN2v88internal8compiler8Schedule15SetBlockForNodeEPNS1_10BasicBlockEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler8Schedule15SetBlockForNodeEPNS1_10BasicBlockEPNS1_4NodeE
	.type	_ZN2v88internal8compiler8Schedule15SetBlockForNodeEPNS1_10BasicBlockEPNS1_4NodeE, @function
_ZN2v88internal8compiler8Schedule15SetBlockForNodeEPNS1_10BasicBlockEPNS1_4NodeE:
.LFB10284:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	48(%rdi), %rcx
	movq	56(%rdi), %rdi
	movl	20(%rdx), %esi
	movq	%rdi, %rax
	andl	$16777215, %esi
	subq	%rcx, %rax
	movl	%esi, %edx
	sarq	$3, %rax
	cmpq	%rax, %rdx
	jnb	.L402
.L399:
	movq	%r13, (%rcx,%rdx,8)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L402:
	.cfi_restore_state
	addl	$1, %esi
	cmpq	%rax, %rsi
	ja	.L403
	jnb	.L399
	leaq	(%rcx,%rsi,8), %rax
	cmpq	%rax, %rdi
	je	.L399
	movq	%rax, 56(%rbx)
	movl	20(%r12), %edx
	andl	$16777215, %edx
	jmp	.L399
	.p2align 4,,10
	.p2align 3
.L403:
	leaq	40(%rbx), %rdi
	subq	%rax, %rsi
	call	_ZNSt6vectorIPN2v88internal8compiler10BasicBlockENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm
	movl	20(%r12), %edx
	movq	48(%rbx), %rcx
	andl	$16777215, %edx
	movq	%r13, (%rcx,%rdx,8)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10284:
	.size	_ZN2v88internal8compiler8Schedule15SetBlockForNodeEPNS1_10BasicBlockEPNS1_4NodeE, .-_ZN2v88internal8compiler8Schedule15SetBlockForNodeEPNS1_10BasicBlockEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler8Schedule15SetControlInputEPNS1_10BasicBlockEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler8Schedule15SetControlInputEPNS1_10BasicBlockEPNS1_4NodeE
	.type	_ZN2v88internal8compiler8Schedule15SetControlInputEPNS1_10BasicBlockEPNS1_4NodeE, @function
_ZN2v88internal8compiler8Schedule15SetControlInputEPNS1_10BasicBlockEPNS1_4NodeE:
.LFB10283:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	80(%rsi), %rax
	cmpq	72(%rsi), %rax
	je	.L405
	cmpq	-8(%rax), %rdx
	je	.L409
.L405:
	movq	%r12, 56(%rbx)
	movq	56(%r13), %rdi
	movl	20(%r12), %esi
	movq	48(%r13), %rcx
	movq	%rdi, %rax
	andl	$16777215, %esi
	subq	%rcx, %rax
	movl	%esi, %edx
	sarq	$3, %rax
	cmpq	%rax, %rdx
	jnb	.L410
.L406:
	movq	%rbx, (%rcx,%rdx,8)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L409:
	.cfi_restore_state
	subq	$8, %rax
	movq	%rax, 80(%rsi)
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L410:
	addl	$1, %esi
	cmpq	%rsi, %rax
	jb	.L411
	jbe	.L406
	leaq	(%rcx,%rsi,8), %rax
	cmpq	%rax, %rdi
	je	.L406
	movq	%rax, 56(%r13)
	movl	20(%r12), %edx
	andl	$16777215, %edx
	jmp	.L406
	.p2align 4,,10
	.p2align 3
.L411:
	leaq	40(%r13), %rdi
	subq	%rax, %rsi
	call	_ZNSt6vectorIPN2v88internal8compiler10BasicBlockENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm
	movl	20(%r12), %edx
	movq	48(%r13), %rcx
	andl	$16777215, %edx
	movq	%rbx, (%rcx,%rdx,8)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10283:
	.size	_ZN2v88internal8compiler8Schedule15SetControlInputEPNS1_10BasicBlockEPNS1_4NodeE, .-_ZN2v88internal8compiler8Schedule15SetControlInputEPNS1_10BasicBlockEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler8Schedule9AddSwitchEPNS1_10BasicBlockEPNS1_4NodeEPS4_m,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler8Schedule9AddSwitchEPNS1_10BasicBlockEPNS1_4NodeEPS4_m
	.type	_ZN2v88internal8compiler8Schedule9AddSwitchEPNS1_10BasicBlockEPNS1_4NodeEPS4_m, @function
_ZN2v88internal8compiler8Schedule9AddSwitchEPNS1_10BasicBlockEPNS1_4NodeEPS4_m:
.LFB10266:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -80(%rbp)
	movq	%rdx, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$4, 52(%rsi)
	testq	%r8, %r8
	je	.L417
	leaq	96(%rsi), %rax
	movq	%rcx, %r15
	leaq	(%rcx,%r8,8), %r13
	movq	%rax, -72(%rbp)
	leaq	-64(%rbp), %r14
	.p2align 4,,10
	.p2align 3
.L419:
	movq	(%r15), %rbx
	movq	112(%r12), %rsi
	movq	%rbx, -64(%rbp)
	cmpq	120(%r12), %rsi
	je	.L414
	movq	%rbx, (%rsi)
	addq	$8, 112(%r12)
.L415:
	movq	%r12, -64(%rbp)
	movq	144(%rbx), %rsi
	cmpq	152(%rbx), %rsi
	je	.L416
	addq	$8, %r15
	movq	%r12, (%rsi)
	addq	$8, 144(%rbx)
	cmpq	%r13, %r15
	jne	.L419
.L417:
	movq	-88(%rbp), %rdx
	movq	-80(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler8Schedule15SetControlInputEPNS1_10BasicBlockEPNS1_4NodeE
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L423
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L416:
	.cfi_restore_state
	leaq	128(%rbx), %rdi
	movq	%r14, %rdx
	addq	$8, %r15
	call	_ZNSt6vectorIPN2v88internal8compiler10BasicBlockENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	cmpq	%r15, %r13
	jne	.L419
	jmp	.L417
	.p2align 4,,10
	.p2align 3
.L414:
	movq	-72(%rbp), %rdi
	movq	%r14, %rdx
	call	_ZNSt6vectorIPN2v88internal8compiler10BasicBlockENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L415
.L423:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10266:
	.size	_ZN2v88internal8compiler8Schedule9AddSwitchEPNS1_10BasicBlockEPNS1_4NodeEPS4_m, .-_ZN2v88internal8compiler8Schedule9AddSwitchEPNS1_10BasicBlockEPNS1_4NodeEPS4_m
	.section	.text._ZN2v88internal8compiler8Schedule12InsertBranchEPNS1_10BasicBlockES4_PNS1_4NodeES4_S4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler8Schedule12InsertBranchEPNS1_10BasicBlockES4_PNS1_4NodeES4_S4_
	.type	_ZN2v88internal8compiler8Schedule12InsertBranchEPNS1_10BasicBlockES4_PNS1_4NodeES4_S4_, @function
_ZN2v88internal8compiler8Schedule12InsertBranchEPNS1_10BasicBlockES4_PNS1_4NodeES4_S4_:
.LFB10271:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	52(%rsi), %eax
	movl	%eax, 52(%rdx)
	movq	104(%rsi), %rax
	movq	112(%rsi), %r9
	movl	$3, 52(%rsi)
	cmpq	%r9, %rax
	je	.L425
	leaq	96(%rdx), %rdi
	leaq	-64(%rbp), %r10
	.p2align 4,,10
	.p2align 3
.L429:
	movq	(%rax), %rcx
	movq	112(%r13), %rsi
	movq	%rcx, -64(%rbp)
	cmpq	120(%r13), %rsi
	je	.L426
	movq	%rcx, (%rsi)
	addq	$8, 112(%r13)
.L427:
	movq	144(%rcx), %rsi
	movq	136(%rcx), %rdx
	cmpq	%rsi, %rdx
	jne	.L432
	jmp	.L433
	.p2align 4,,10
	.p2align 3
.L431:
	addq	$8, %rdx
	cmpq	%rdx, %rsi
	je	.L433
.L432:
	cmpq	(%rdx), %r12
	jne	.L431
	movq	%r13, (%rdx)
	addq	$8, %rdx
	cmpq	%rdx, %rsi
	jne	.L432
.L433:
	addq	$8, %rax
	cmpq	%rax, %r9
	jne	.L429
	movq	104(%r12), %r9
	cmpq	%r9, 112(%r12)
	je	.L425
	movq	%r9, 112(%r12)
.L425:
	movq	%r8, -64(%rbp)
	leaq	96(%r12), %r10
	cmpq	120(%r12), %r9
	je	.L434
	movq	%r8, (%r9)
	addq	$8, 112(%r12)
.L435:
	movq	%r12, -64(%rbp)
	movq	144(%r8), %rsi
	cmpq	152(%r8), %rsi
	je	.L436
	movq	%r12, (%rsi)
	movq	%rbx, -64(%rbp)
	addq	$8, 144(%r8)
	movq	112(%r12), %rsi
	cmpq	120(%r12), %rsi
	je	.L438
.L453:
	movq	%rbx, (%rsi)
	addq	$8, 112(%r12)
.L439:
	movq	%r12, -64(%rbp)
	movq	144(%rbx), %rsi
	cmpq	152(%rbx), %rsi
	je	.L440
	movq	%r12, (%rsi)
	addq	$8, 144(%rbx)
.L441:
	movq	56(%r12), %rdx
	testq	%rdx, %rdx
	je	.L442
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler8Schedule15SetControlInputEPNS1_10BasicBlockEPNS1_4NodeE
.L442:
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler8Schedule15SetControlInputEPNS1_10BasicBlockEPNS1_4NodeE
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L452
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L426:
	.cfi_restore_state
	movq	%r10, %rdx
	movq	%r8, -112(%rbp)
	movq	%rcx, -104(%rbp)
	movq	%rax, -96(%rbp)
	movq	%r9, -88(%rbp)
	movq	%r10, -80(%rbp)
	movq	%rdi, -72(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler10BasicBlockENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	movq	-112(%rbp), %r8
	movq	-104(%rbp), %rcx
	movq	-96(%rbp), %rax
	movq	-88(%rbp), %r9
	movq	-80(%rbp), %r10
	movq	-72(%rbp), %rdi
	jmp	.L427
	.p2align 4,,10
	.p2align 3
.L434:
	movq	%r10, %rdi
	leaq	-64(%rbp), %rdx
	movq	%r9, %rsi
	movq	%r8, -80(%rbp)
	movq	%r10, -72(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler10BasicBlockENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	movq	-80(%rbp), %r8
	movq	-72(%rbp), %r10
	jmp	.L435
	.p2align 4,,10
	.p2align 3
.L436:
	leaq	-64(%rbp), %rdx
	leaq	128(%r8), %rdi
	movq	%r10, -72(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler10BasicBlockENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	movq	%rbx, -64(%rbp)
	movq	-72(%rbp), %r10
	movq	112(%r12), %rsi
	cmpq	120(%r12), %rsi
	jne	.L453
	.p2align 4,,10
	.p2align 3
.L438:
	leaq	-64(%rbp), %rdx
	movq	%r10, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler10BasicBlockENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L439
	.p2align 4,,10
	.p2align 3
.L440:
	leaq	-64(%rbp), %rdx
	leaq	128(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler10BasicBlockENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L441
.L452:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10271:
	.size	_ZN2v88internal8compiler8Schedule12InsertBranchEPNS1_10BasicBlockES4_PNS1_4NodeES4_S4_, .-_ZN2v88internal8compiler8Schedule12InsertBranchEPNS1_10BasicBlockES4_PNS1_4NodeES4_S4_
	.section	.text._ZN2v88internal8compiler8Schedule12InsertSwitchEPNS1_10BasicBlockES4_PNS1_4NodeEPS4_m,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler8Schedule12InsertSwitchEPNS1_10BasicBlockES4_PNS1_4NodeEPS4_m
	.type	_ZN2v88internal8compiler8Schedule12InsertSwitchEPNS1_10BasicBlockES4_PNS1_4NodeEPS4_m, @function
_ZN2v88internal8compiler8Schedule12InsertSwitchEPNS1_10BasicBlockES4_PNS1_4NodeEPS4_m:
.LFB10272:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$72, %rsp
	movq	%rcx, -104(%rbp)
	movq	%rdi, -96(%rbp)
	movq	%r8, -112(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	52(%rsi), %eax
	movl	%eax, 52(%rdx)
	movq	104(%rsi), %rcx
	movq	112(%rsi), %r11
	movl	$4, 52(%rsi)
	cmpq	%r11, %rcx
	je	.L455
	leaq	96(%rdx), %rdi
	leaq	-64(%rbp), %r13
	.p2align 4,,10
	.p2align 3
.L459:
	movq	(%rcx), %r15
	movq	112(%r14), %rsi
	movq	%r15, -64(%rbp)
	cmpq	120(%r14), %rsi
	je	.L456
	movq	%r15, (%rsi)
	addq	$8, 112(%r14)
.L457:
	movq	144(%r15), %rsi
	movq	136(%r15), %rax
	cmpq	%rsi, %rax
	jne	.L462
	jmp	.L463
	.p2align 4,,10
	.p2align 3
.L461:
	addq	$8, %rax
	cmpq	%rax, %rsi
	je	.L463
.L462:
	cmpq	(%rax), %r12
	jne	.L461
	movq	%r14, (%rax)
	addq	$8, %rax
	cmpq	%rax, %rsi
	jne	.L462
.L463:
	addq	$8, %rcx
	cmpq	%rcx, %r11
	jne	.L459
	movq	104(%r12), %r11
	cmpq	112(%r12), %r11
	je	.L455
	movq	%r11, 112(%r12)
.L455:
	testq	%rbx, %rbx
	je	.L464
	movq	-112(%rbp), %r13
	leaq	96(%r12), %rax
	movq	%rax, -80(%rbp)
	leaq	0(%r13,%rbx,8), %r15
	leaq	-64(%rbp), %rbx
	jmp	.L469
	.p2align 4,,10
	.p2align 3
.L483:
	movq	%rax, (%r11)
	addq	$8, 112(%r12)
.L466:
	movq	%r12, -64(%rbp)
	movq	144(%rax), %rsi
	cmpq	152(%rax), %rsi
	je	.L467
	addq	$8, %r13
	movq	%r12, (%rsi)
	addq	$8, 144(%rax)
	cmpq	%r13, %r15
	je	.L464
.L468:
	movq	112(%r12), %r11
.L469:
	movq	0(%r13), %rax
	movq	%rax, -64(%rbp)
	cmpq	%r11, 120(%r12)
	jne	.L483
	movq	-80(%rbp), %rdi
	movq	%rbx, %rdx
	movq	%r11, %rsi
	movq	%rax, -72(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler10BasicBlockENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	movq	-72(%rbp), %rax
	jmp	.L466
	.p2align 4,,10
	.p2align 3
.L456:
	movq	%r13, %rdx
	movq	%r11, -88(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%rdi, -72(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler10BasicBlockENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	movq	-88(%rbp), %r11
	movq	-80(%rbp), %rcx
	movq	-72(%rbp), %rdi
	jmp	.L457
	.p2align 4,,10
	.p2align 3
.L467:
	leaq	128(%rax), %rdi
	movq	%rbx, %rdx
	addq	$8, %r13
	call	_ZNSt6vectorIPN2v88internal8compiler10BasicBlockENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	cmpq	%r13, %r15
	jne	.L468
	.p2align 4,,10
	.p2align 3
.L464:
	movq	56(%r12), %rdx
	testq	%rdx, %rdx
	je	.L470
	movq	-96(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler8Schedule15SetControlInputEPNS1_10BasicBlockEPNS1_4NodeE
.L470:
	movq	-104(%rbp), %rdx
	movq	-96(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler8Schedule15SetControlInputEPNS1_10BasicBlockEPNS1_4NodeE
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L484
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L484:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10272:
	.size	_ZN2v88internal8compiler8Schedule12InsertSwitchEPNS1_10BasicBlockES4_PNS1_4NodeEPS4_m, .-_ZN2v88internal8compiler8Schedule12InsertSwitchEPNS1_10BasicBlockES4_PNS1_4NodeEPS4_m
	.section	.rodata._ZN2v88internal8compiler8Schedule8PlanNodeEPNS1_10BasicBlockEPNS1_4NodeE.str1.1,"aMS",@progbits,1
.LC26:
	.string	"Planning #"
.LC27:
	.string	":"
.LC28:
	.string	" for future add to B"
	.section	.text._ZN2v88internal8compiler8Schedule8PlanNodeEPNS1_10BasicBlockEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler8Schedule8PlanNodeEPNS1_10BasicBlockEPNS1_4NodeE
	.type	_ZN2v88internal8compiler8Schedule8PlanNodeEPNS1_10BasicBlockEPNS1_4NodeE, @function
_ZN2v88internal8compiler8Schedule8PlanNodeEPNS1_10BasicBlockEPNS1_4NodeE:
.LFB10261:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$376, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L493
.L486:
	movq	56(%r12), %rdi
	movl	20(%r13), %esi
	movq	48(%r12), %rcx
	movq	%rdi, %rax
	andl	$16777215, %esi
	subq	%rcx, %rax
	movl	%esi, %edx
	sarq	$3, %rax
	cmpq	%rax, %rdx
	jnb	.L494
.L489:
	movq	%rbx, (%rcx,%rdx,8)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L495
	addq	$376, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L493:
	.cfi_restore_state
	leaq	-320(%rbp), %r14
	leaq	-400(%rbp), %r15
	movq	%r14, %rdi
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	stdout(%rip), %rdx
	pxor	%xmm0, %xmm0
	movq	%r15, %rdi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movups	%xmm0, -88(%rbp)
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movups	%xmm0, -72(%rbp)
	movw	%ax, -96(%rbp)
	movq	$0, -104(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movq	%r15, %rdi
	movl	$10, %edx
	movq	%rax, -400(%rbp)
	leaq	.LC26(%rip), %rsi
	addq	$40, %rax
	movq	%rax, -320(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	20(%r13), %esi
	movq	%r15, %rdi
	andl	$16777215, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	leaq	.LC27(%rip), %rsi
	movl	$1, %edx
	movq	%rax, %rdi
	movq	%rax, %r15
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	0(%r13), %rax
	movq	8(%rax), %rsi
	testq	%rsi, %rsi
	je	.L496
	movq	%rsi, %rdi
	movq	%rsi, -408(%rbp)
	call	strlen@PLT
	movq	-408(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L488:
	movl	$20, %edx
	movq	%r15, %rdi
	leaq	.LC28(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	160(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC20(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rcx
	movq	%rax, -320(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-336(%rbp), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -400(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -400(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L486
	.p2align 4,,10
	.p2align 3
.L494:
	addl	$1, %esi
	cmpq	%rsi, %rax
	jb	.L497
	jbe	.L489
	leaq	(%rcx,%rsi,8), %rax
	cmpq	%rax, %rdi
	je	.L489
	movq	%rax, 56(%r12)
	movl	20(%r13), %edx
	andl	$16777215, %edx
	jmp	.L489
	.p2align 4,,10
	.p2align 3
.L497:
	subq	%rax, %rsi
	leaq	40(%r12), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler10BasicBlockENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm
	movl	20(%r13), %edx
	movq	48(%r12), %rcx
	andl	$16777215, %edx
	jmp	.L489
	.p2align 4,,10
	.p2align 3
.L496:
	movq	(%r15), %rax
	movq	-24(%rax), %rdi
	addq	%r15, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L488
.L495:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10261:
	.size	_ZN2v88internal8compiler8Schedule8PlanNodeEPNS1_10BasicBlockEPNS1_4NodeE, .-_ZN2v88internal8compiler8Schedule8PlanNodeEPNS1_10BasicBlockEPNS1_4NodeE
	.section	.rodata._ZN2v88internal8compiler8Schedule7AddNodeEPNS1_10BasicBlockEPNS1_4NodeE.str1.1,"aMS",@progbits,1
.LC29:
	.string	"Adding #"
.LC30:
	.string	" to B"
	.section	.text._ZN2v88internal8compiler8Schedule7AddNodeEPNS1_10BasicBlockEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler8Schedule7AddNodeEPNS1_10BasicBlockEPNS1_4NodeE
	.type	_ZN2v88internal8compiler8Schedule7AddNodeEPNS1_10BasicBlockEPNS1_4NodeE, @function
_ZN2v88internal8compiler8Schedule7AddNodeEPNS1_10BasicBlockEPNS1_4NodeE:
.LFB10262:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$392, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L508
.L499:
	movq	%r12, -408(%rbp)
	movq	80(%rbx), %rsi
	cmpq	88(%rbx), %rsi
	je	.L502
	movq	%r12, (%rsi)
	addq	$8, 80(%rbx)
.L503:
	movq	56(%r13), %rdi
	movl	20(%r12), %esi
	movq	48(%r13), %rcx
	movq	%rdi, %rax
	andl	$16777215, %esi
	subq	%rcx, %rax
	movl	%esi, %edx
	sarq	$3, %rax
	cmpq	%rax, %rdx
	jnb	.L509
.L504:
	movq	%rbx, (%rcx,%rdx,8)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L510
	addq	$392, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L508:
	.cfi_restore_state
	leaq	-320(%rbp), %r14
	leaq	-400(%rbp), %r15
	movq	%r14, %rdi
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	stdout(%rip), %rdx
	pxor	%xmm0, %xmm0
	movq	%r15, %rdi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movups	%xmm0, -88(%rbp)
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movups	%xmm0, -72(%rbp)
	movw	%ax, -96(%rbp)
	movq	$0, -104(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movq	%r15, %rdi
	movl	$8, %edx
	movq	%rax, -400(%rbp)
	leaq	.LC29(%rip), %rsi
	addq	$40, %rax
	movq	%rax, -320(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	20(%r12), %esi
	movq	%r15, %rdi
	andl	$16777215, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	leaq	.LC27(%rip), %rsi
	movl	$1, %edx
	movq	%rax, %rdi
	movq	%rax, %r15
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r12), %rax
	movq	8(%rax), %rsi
	testq	%rsi, %rsi
	je	.L511
	movq	%rsi, %rdi
	movq	%rsi, -424(%rbp)
	call	strlen@PLT
	movq	-424(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L501:
	movl	$5, %edx
	movq	%r15, %rdi
	leaq	.LC30(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	160(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC20(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rcx
	movq	%rax, -320(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-336(%rbp), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -400(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -400(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L499
	.p2align 4,,10
	.p2align 3
.L509:
	addl	$1, %esi
	cmpq	%rsi, %rax
	jb	.L512
	jbe	.L504
	leaq	(%rcx,%rsi,8), %rax
	cmpq	%rax, %rdi
	je	.L504
	movq	%rax, 56(%r13)
	movl	20(%r12), %edx
	andl	$16777215, %edx
	jmp	.L504
	.p2align 4,,10
	.p2align 3
.L502:
	leaq	-408(%rbp), %rdx
	leaq	64(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L503
	.p2align 4,,10
	.p2align 3
.L512:
	subq	%rax, %rsi
	leaq	40(%r13), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler10BasicBlockENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm
	movl	20(%r12), %edx
	movq	48(%r13), %rcx
	andl	$16777215, %edx
	jmp	.L504
	.p2align 4,,10
	.p2align 3
.L511:
	movq	(%r15), %rax
	movq	-24(%rax), %rdi
	addq	%r15, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L501
.L510:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10262:
	.size	_ZN2v88internal8compiler8Schedule7AddNodeEPNS1_10BasicBlockEPNS1_4NodeE, .-_ZN2v88internal8compiler8Schedule7AddNodeEPNS1_10BasicBlockEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler8Schedule13AddDeoptimizeEPNS1_10BasicBlockEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler8Schedule13AddDeoptimizeEPNS1_10BasicBlockEPNS1_4NodeE
	.type	_ZN2v88internal8compiler8Schedule13AddDeoptimizeEPNS1_10BasicBlockEPNS1_4NodeE, @function
_ZN2v88internal8compiler8Schedule13AddDeoptimizeEPNS1_10BasicBlockEPNS1_4NodeE:
.LFB10269:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$5, 52(%rsi)
	movq	80(%rsi), %rax
	cmpq	72(%rsi), %rax
	je	.L514
	cmpq	-8(%rax), %rdx
	je	.L524
.L514:
	movq	%r13, 56(%rbx)
	movq	56(%r12), %rdi
	movl	20(%r13), %esi
	movq	48(%r12), %rcx
	movq	%rdi, %rax
	andl	$16777215, %esi
	subq	%rcx, %rax
	movl	%esi, %edx
	sarq	$3, %rax
	cmpq	%rax, %rdx
	jnb	.L525
.L515:
	movq	%rbx, (%rcx,%rdx,8)
	movq	112(%r12), %r12
	cmpq	%r12, %rbx
	je	.L513
	movq	%r12, -48(%rbp)
	movq	112(%rbx), %rsi
	cmpq	120(%rbx), %rsi
	je	.L518
	movq	%r12, (%rsi)
	addq	$8, 112(%rbx)
.L519:
	movq	%rbx, -48(%rbp)
	movq	144(%r12), %rsi
	cmpq	152(%r12), %rsi
	je	.L520
	movq	%rbx, (%rsi)
	addq	$8, 144(%r12)
.L513:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L526
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L524:
	.cfi_restore_state
	subq	$8, %rax
	movq	%rax, 80(%rsi)
	jmp	.L514
	.p2align 4,,10
	.p2align 3
.L525:
	addl	$1, %esi
	cmpq	%rsi, %rax
	jb	.L527
	jbe	.L515
	leaq	(%rcx,%rsi,8), %rax
	cmpq	%rax, %rdi
	je	.L515
	movq	%rax, 56(%r12)
	movl	20(%r13), %edx
	andl	$16777215, %edx
	jmp	.L515
	.p2align 4,,10
	.p2align 3
.L518:
	leaq	-48(%rbp), %rdx
	leaq	96(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler10BasicBlockENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L519
	.p2align 4,,10
	.p2align 3
.L527:
	subq	%rax, %rsi
	leaq	40(%r12), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler10BasicBlockENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm
	movl	20(%r13), %edx
	movq	48(%r12), %rcx
	andl	$16777215, %edx
	jmp	.L515
	.p2align 4,,10
	.p2align 3
.L520:
	leaq	-48(%rbp), %rdx
	leaq	128(%r12), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler10BasicBlockENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L513
.L526:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10269:
	.size	_ZN2v88internal8compiler8Schedule13AddDeoptimizeEPNS1_10BasicBlockEPNS1_4NodeE, .-_ZN2v88internal8compiler8Schedule13AddDeoptimizeEPNS1_10BasicBlockEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler8Schedule9AddReturnEPNS1_10BasicBlockEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler8Schedule9AddReturnEPNS1_10BasicBlockEPNS1_4NodeE
	.type	_ZN2v88internal8compiler8Schedule9AddReturnEPNS1_10BasicBlockEPNS1_4NodeE, @function
_ZN2v88internal8compiler8Schedule9AddReturnEPNS1_10BasicBlockEPNS1_4NodeE:
.LFB10268:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$7, 52(%rsi)
	movq	80(%rsi), %rax
	cmpq	72(%rsi), %rax
	je	.L529
	cmpq	-8(%rax), %rdx
	je	.L539
.L529:
	movq	%r13, 56(%rbx)
	movq	56(%r12), %rdi
	movl	20(%r13), %esi
	movq	48(%r12), %rcx
	movq	%rdi, %rax
	andl	$16777215, %esi
	subq	%rcx, %rax
	movl	%esi, %edx
	sarq	$3, %rax
	cmpq	%rax, %rdx
	jnb	.L540
.L530:
	movq	%rbx, (%rcx,%rdx,8)
	movq	112(%r12), %r12
	cmpq	%r12, %rbx
	je	.L528
	movq	%r12, -48(%rbp)
	movq	112(%rbx), %rsi
	cmpq	120(%rbx), %rsi
	je	.L533
	movq	%r12, (%rsi)
	addq	$8, 112(%rbx)
.L534:
	movq	%rbx, -48(%rbp)
	movq	144(%r12), %rsi
	cmpq	152(%r12), %rsi
	je	.L535
	movq	%rbx, (%rsi)
	addq	$8, 144(%r12)
.L528:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L541
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L539:
	.cfi_restore_state
	subq	$8, %rax
	movq	%rax, 80(%rsi)
	jmp	.L529
	.p2align 4,,10
	.p2align 3
.L540:
	addl	$1, %esi
	cmpq	%rsi, %rax
	jb	.L542
	jbe	.L530
	leaq	(%rcx,%rsi,8), %rax
	cmpq	%rax, %rdi
	je	.L530
	movq	%rax, 56(%r12)
	movl	20(%r13), %edx
	andl	$16777215, %edx
	jmp	.L530
	.p2align 4,,10
	.p2align 3
.L533:
	leaq	-48(%rbp), %rdx
	leaq	96(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler10BasicBlockENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L534
	.p2align 4,,10
	.p2align 3
.L542:
	subq	%rax, %rsi
	leaq	40(%r12), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler10BasicBlockENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm
	movl	20(%r13), %edx
	movq	48(%r12), %rcx
	andl	$16777215, %edx
	jmp	.L530
	.p2align 4,,10
	.p2align 3
.L535:
	leaq	-48(%rbp), %rdx
	leaq	128(%r12), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler10BasicBlockENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L528
.L541:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10268:
	.size	_ZN2v88internal8compiler8Schedule9AddReturnEPNS1_10BasicBlockEPNS1_4NodeE, .-_ZN2v88internal8compiler8Schedule9AddReturnEPNS1_10BasicBlockEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler8Schedule8AddThrowEPNS1_10BasicBlockEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler8Schedule8AddThrowEPNS1_10BasicBlockEPNS1_4NodeE
	.type	_ZN2v88internal8compiler8Schedule8AddThrowEPNS1_10BasicBlockEPNS1_4NodeE, @function
_ZN2v88internal8compiler8Schedule8AddThrowEPNS1_10BasicBlockEPNS1_4NodeE:
.LFB10270:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$8, 52(%rsi)
	movq	80(%rsi), %rax
	cmpq	72(%rsi), %rax
	je	.L544
	cmpq	-8(%rax), %rdx
	je	.L554
.L544:
	movq	%r13, 56(%rbx)
	movq	56(%r12), %rdi
	movl	20(%r13), %esi
	movq	48(%r12), %rcx
	movq	%rdi, %rax
	andl	$16777215, %esi
	subq	%rcx, %rax
	movl	%esi, %edx
	sarq	$3, %rax
	cmpq	%rax, %rdx
	jnb	.L555
.L545:
	movq	%rbx, (%rcx,%rdx,8)
	movq	112(%r12), %r12
	cmpq	%r12, %rbx
	je	.L543
	movq	%r12, -48(%rbp)
	movq	112(%rbx), %rsi
	cmpq	120(%rbx), %rsi
	je	.L548
	movq	%r12, (%rsi)
	addq	$8, 112(%rbx)
.L549:
	movq	%rbx, -48(%rbp)
	movq	144(%r12), %rsi
	cmpq	152(%r12), %rsi
	je	.L550
	movq	%rbx, (%rsi)
	addq	$8, 144(%r12)
.L543:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L556
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L554:
	.cfi_restore_state
	subq	$8, %rax
	movq	%rax, 80(%rsi)
	jmp	.L544
	.p2align 4,,10
	.p2align 3
.L555:
	addl	$1, %esi
	cmpq	%rsi, %rax
	jb	.L557
	jbe	.L545
	leaq	(%rcx,%rsi,8), %rax
	cmpq	%rax, %rdi
	je	.L545
	movq	%rax, 56(%r12)
	movl	20(%r13), %edx
	andl	$16777215, %edx
	jmp	.L545
	.p2align 4,,10
	.p2align 3
.L548:
	leaq	-48(%rbp), %rdx
	leaq	96(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler10BasicBlockENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L549
	.p2align 4,,10
	.p2align 3
.L557:
	subq	%rax, %rsi
	leaq	40(%r12), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler10BasicBlockENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm
	movl	20(%r13), %edx
	movq	48(%r12), %rcx
	andl	$16777215, %edx
	jmp	.L545
	.p2align 4,,10
	.p2align 3
.L550:
	leaq	-48(%rbp), %rdx
	leaq	128(%r12), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler10BasicBlockENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L543
.L556:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10270:
	.size	_ZN2v88internal8compiler8Schedule8AddThrowEPNS1_10BasicBlockEPNS1_4NodeE, .-_ZN2v88internal8compiler8Schedule8AddThrowEPNS1_10BasicBlockEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler8Schedule11AddTailCallEPNS1_10BasicBlockEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler8Schedule11AddTailCallEPNS1_10BasicBlockEPNS1_4NodeE
	.type	_ZN2v88internal8compiler8Schedule11AddTailCallEPNS1_10BasicBlockEPNS1_4NodeE, @function
_ZN2v88internal8compiler8Schedule11AddTailCallEPNS1_10BasicBlockEPNS1_4NodeE:
.LFB10267:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$6, 52(%rsi)
	movq	80(%rsi), %rax
	cmpq	72(%rsi), %rax
	je	.L559
	cmpq	-8(%rax), %rdx
	je	.L569
.L559:
	movq	%r13, 56(%rbx)
	movq	56(%r12), %rdi
	movl	20(%r13), %esi
	movq	48(%r12), %rcx
	movq	%rdi, %rax
	andl	$16777215, %esi
	subq	%rcx, %rax
	movl	%esi, %edx
	sarq	$3, %rax
	cmpq	%rax, %rdx
	jnb	.L570
.L560:
	movq	%rbx, (%rcx,%rdx,8)
	movq	112(%r12), %r12
	cmpq	%r12, %rbx
	je	.L558
	movq	%r12, -48(%rbp)
	movq	112(%rbx), %rsi
	cmpq	120(%rbx), %rsi
	je	.L563
	movq	%r12, (%rsi)
	addq	$8, 112(%rbx)
.L564:
	movq	%rbx, -48(%rbp)
	movq	144(%r12), %rsi
	cmpq	152(%r12), %rsi
	je	.L565
	movq	%rbx, (%rsi)
	addq	$8, 144(%r12)
.L558:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L571
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L569:
	.cfi_restore_state
	subq	$8, %rax
	movq	%rax, 80(%rsi)
	jmp	.L559
	.p2align 4,,10
	.p2align 3
.L570:
	addl	$1, %esi
	cmpq	%rsi, %rax
	jb	.L572
	jbe	.L560
	leaq	(%rcx,%rsi,8), %rax
	cmpq	%rax, %rdi
	je	.L560
	movq	%rax, 56(%r12)
	movl	20(%r13), %edx
	andl	$16777215, %edx
	jmp	.L560
	.p2align 4,,10
	.p2align 3
.L563:
	leaq	-48(%rbp), %rdx
	leaq	96(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler10BasicBlockENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L564
	.p2align 4,,10
	.p2align 3
.L572:
	subq	%rax, %rsi
	leaq	40(%r12), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler10BasicBlockENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm
	movl	20(%r13), %edx
	movq	48(%r12), %rcx
	andl	$16777215, %edx
	jmp	.L560
	.p2align 4,,10
	.p2align 3
.L565:
	leaq	-48(%rbp), %rdx
	leaq	128(%r12), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler10BasicBlockENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L558
.L571:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10267:
	.size	_ZN2v88internal8compiler8Schedule11AddTailCallEPNS1_10BasicBlockEPNS1_4NodeE, .-_ZN2v88internal8compiler8Schedule11AddTailCallEPNS1_10BasicBlockEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler8Schedule9AddBranchEPNS1_10BasicBlockEPNS1_4NodeES4_S4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler8Schedule9AddBranchEPNS1_10BasicBlockEPNS1_4NodeES4_S4_
	.type	_ZN2v88internal8compiler8Schedule9AddBranchEPNS1_10BasicBlockEPNS1_4NodeES4_S4_, @function
_ZN2v88internal8compiler8Schedule9AddBranchEPNS1_10BasicBlockEPNS1_4NodeES4_S4_:
.LFB10265:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	leaq	96(%rsi), %r8
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$3, 52(%rsi)
	movq	112(%rsi), %rsi
	movq	%rcx, -64(%rbp)
	cmpq	120(%rbx), %rsi
	je	.L574
	movq	%rcx, (%rsi)
	addq	$8, 112(%rbx)
.L575:
	movq	%rbx, -64(%rbp)
	movq	144(%r13), %rsi
	cmpq	152(%r13), %rsi
	je	.L576
	movq	%rbx, (%rsi)
	movq	%r12, -64(%rbp)
	addq	$8, 144(%r13)
	movq	112(%rbx), %rsi
	cmpq	120(%rbx), %rsi
	je	.L578
.L591:
	movq	%r12, (%rsi)
	addq	$8, 112(%rbx)
.L579:
	movq	%rbx, -64(%rbp)
	movq	144(%r12), %rsi
	cmpq	152(%r12), %rsi
	je	.L580
	movq	%rbx, (%rsi)
	addq	$8, 144(%r12)
.L581:
	movq	80(%rbx), %rax
	cmpq	72(%rbx), %rax
	je	.L582
	cmpq	-8(%rax), %r14
	je	.L587
.L582:
	movq	%r14, 56(%rbx)
	movq	56(%r15), %rdi
	movl	20(%r14), %esi
	movq	48(%r15), %rcx
	movq	%rdi, %rax
	andl	$16777215, %esi
	subq	%rcx, %rax
	movl	%esi, %edx
	sarq	$3, %rax
	cmpq	%rax, %rdx
	jnb	.L588
.L583:
	movq	%rbx, (%rcx,%rdx,8)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L589
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L587:
	.cfi_restore_state
	subq	$8, %rax
	movq	%rax, 80(%rbx)
	jmp	.L582
	.p2align 4,,10
	.p2align 3
.L588:
	addl	$1, %esi
	cmpq	%rsi, %rax
	jb	.L590
	jbe	.L583
	leaq	(%rcx,%rsi,8), %rax
	cmpq	%rax, %rdi
	je	.L583
	movq	%rax, 56(%r15)
	movl	20(%r14), %edx
	andl	$16777215, %edx
	jmp	.L583
	.p2align 4,,10
	.p2align 3
.L574:
	movq	%r8, %rdi
	leaq	-64(%rbp), %rdx
	movq	%r8, -72(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler10BasicBlockENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	movq	-72(%rbp), %r8
	jmp	.L575
	.p2align 4,,10
	.p2align 3
.L576:
	leaq	-64(%rbp), %rdx
	leaq	128(%r13), %rdi
	movq	%r8, -72(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler10BasicBlockENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	movq	%r12, -64(%rbp)
	movq	-72(%rbp), %r8
	movq	112(%rbx), %rsi
	cmpq	120(%rbx), %rsi
	jne	.L591
	.p2align 4,,10
	.p2align 3
.L578:
	leaq	-64(%rbp), %rdx
	movq	%r8, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler10BasicBlockENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L579
	.p2align 4,,10
	.p2align 3
.L580:
	leaq	-64(%rbp), %rdx
	leaq	128(%r12), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler10BasicBlockENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L581
	.p2align 4,,10
	.p2align 3
.L590:
	subq	%rax, %rsi
	leaq	40(%r15), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler10BasicBlockENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm
	movl	20(%r14), %edx
	movq	48(%r15), %rcx
	andl	$16777215, %edx
	jmp	.L583
.L589:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10265:
	.size	_ZN2v88internal8compiler8Schedule9AddBranchEPNS1_10BasicBlockEPNS1_4NodeES4_S4_, .-_ZN2v88internal8compiler8Schedule9AddBranchEPNS1_10BasicBlockEPNS1_4NodeES4_S4_
	.section	.text._ZN2v88internal8compiler8Schedule7AddCallEPNS1_10BasicBlockEPNS1_4NodeES4_S4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler8Schedule7AddCallEPNS1_10BasicBlockEPNS1_4NodeES4_S4_
	.type	_ZN2v88internal8compiler8Schedule7AddCallEPNS1_10BasicBlockEPNS1_4NodeES4_S4_, @function
_ZN2v88internal8compiler8Schedule7AddCallEPNS1_10BasicBlockEPNS1_4NodeES4_S4_:
.LFB10264:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	leaq	96(%rsi), %r8
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$2, 52(%rsi)
	movq	112(%rsi), %rsi
	movq	%rcx, -64(%rbp)
	cmpq	120(%rbx), %rsi
	je	.L593
	movq	%rcx, (%rsi)
	addq	$8, 112(%rbx)
.L594:
	movq	%rbx, -64(%rbp)
	movq	144(%r13), %rsi
	cmpq	152(%r13), %rsi
	je	.L595
	movq	%rbx, (%rsi)
	movq	%r12, -64(%rbp)
	addq	$8, 144(%r13)
	movq	112(%rbx), %rsi
	cmpq	120(%rbx), %rsi
	je	.L597
.L610:
	movq	%r12, (%rsi)
	addq	$8, 112(%rbx)
.L598:
	movq	%rbx, -64(%rbp)
	movq	144(%r12), %rsi
	cmpq	152(%r12), %rsi
	je	.L599
	movq	%rbx, (%rsi)
	addq	$8, 144(%r12)
.L600:
	movq	80(%rbx), %rax
	cmpq	72(%rbx), %rax
	je	.L601
	cmpq	-8(%rax), %r14
	je	.L606
.L601:
	movq	%r14, 56(%rbx)
	movq	56(%r15), %rdi
	movl	20(%r14), %esi
	movq	48(%r15), %rcx
	movq	%rdi, %rax
	andl	$16777215, %esi
	subq	%rcx, %rax
	movl	%esi, %edx
	sarq	$3, %rax
	cmpq	%rax, %rdx
	jnb	.L607
.L602:
	movq	%rbx, (%rcx,%rdx,8)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L608
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L606:
	.cfi_restore_state
	subq	$8, %rax
	movq	%rax, 80(%rbx)
	jmp	.L601
	.p2align 4,,10
	.p2align 3
.L607:
	addl	$1, %esi
	cmpq	%rsi, %rax
	jb	.L609
	jbe	.L602
	leaq	(%rcx,%rsi,8), %rax
	cmpq	%rax, %rdi
	je	.L602
	movq	%rax, 56(%r15)
	movl	20(%r14), %edx
	andl	$16777215, %edx
	jmp	.L602
	.p2align 4,,10
	.p2align 3
.L593:
	movq	%r8, %rdi
	leaq	-64(%rbp), %rdx
	movq	%r8, -72(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler10BasicBlockENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	movq	-72(%rbp), %r8
	jmp	.L594
	.p2align 4,,10
	.p2align 3
.L595:
	leaq	-64(%rbp), %rdx
	leaq	128(%r13), %rdi
	movq	%r8, -72(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler10BasicBlockENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	movq	%r12, -64(%rbp)
	movq	-72(%rbp), %r8
	movq	112(%rbx), %rsi
	cmpq	120(%rbx), %rsi
	jne	.L610
	.p2align 4,,10
	.p2align 3
.L597:
	leaq	-64(%rbp), %rdx
	movq	%r8, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler10BasicBlockENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L598
	.p2align 4,,10
	.p2align 3
.L599:
	leaq	-64(%rbp), %rdx
	leaq	128(%r12), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler10BasicBlockENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L600
	.p2align 4,,10
	.p2align 3
.L609:
	subq	%rax, %rsi
	leaq	40(%r15), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler10BasicBlockENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm
	movl	20(%r14), %edx
	movq	48(%r15), %rcx
	andl	$16777215, %edx
	jmp	.L602
.L608:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10264:
	.size	_ZN2v88internal8compiler8Schedule7AddCallEPNS1_10BasicBlockEPNS1_4NodeES4_S4_, .-_ZN2v88internal8compiler8Schedule7AddCallEPNS1_10BasicBlockEPNS1_4NodeES4_S4_
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler10BasicBlockC2EPNS0_4ZoneENS2_2IdE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler10BasicBlockC2EPNS0_4ZoneENS2_2IdE, @function
_GLOBAL__sub_I__ZN2v88internal8compiler10BasicBlockC2EPNS0_4ZoneENS2_2IdE:
.LFB12123:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE12123:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler10BasicBlockC2EPNS0_4ZoneENS2_2IdE, .-_GLOBAL__sub_I__ZN2v88internal8compiler10BasicBlockC2EPNS0_4ZoneENS2_2IdE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler10BasicBlockC2EPNS0_4ZoneENS2_2IdE
	.hidden	_ZTCN2v88internal12StdoutStreamE0_So
	.weak	_ZTCN2v88internal12StdoutStreamE0_So
	.section	.rodata._ZTCN2v88internal12StdoutStreamE0_So,"aG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTCN2v88internal12StdoutStreamE0_So, @object
	.size	_ZTCN2v88internal12StdoutStreamE0_So, 80
_ZTCN2v88internal12StdoutStreamE0_So:
	.quad	80
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-80
	.quad	-80
	.quad	0
	.quad	0
	.quad	0
	.hidden	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE
	.weak	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE
	.section	.rodata._ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE,"aG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE, @object
	.size	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE, 80
_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE:
	.quad	80
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-80
	.quad	-80
	.quad	0
	.quad	0
	.quad	0
	.weak	_ZTTN2v88internal12StdoutStreamE
	.section	.data.rel.ro.local._ZTTN2v88internal12StdoutStreamE,"awG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTTN2v88internal12StdoutStreamE, @object
	.size	_ZTTN2v88internal12StdoutStreamE, 48
_ZTTN2v88internal12StdoutStreamE:
	.quad	_ZTVN2v88internal12StdoutStreamE+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_So+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_So+64
	.quad	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE+64
	.quad	_ZTVN2v88internal12StdoutStreamE+64
	.weak	_ZTVN2v88internal12StdoutStreamE
	.section	.data.rel.ro.local._ZTVN2v88internal12StdoutStreamE,"awG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTVN2v88internal12StdoutStreamE, @object
	.size	_ZTVN2v88internal12StdoutStreamE, 80
_ZTVN2v88internal12StdoutStreamE:
	.quad	80
	.quad	0
	.quad	0
	.quad	_ZN2v88internal12StdoutStreamD1Ev
	.quad	_ZN2v88internal12StdoutStreamD0Ev
	.quad	-80
	.quad	-80
	.quad	0
	.quad	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.quad	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
