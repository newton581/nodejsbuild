	.file	"compilation-dependencies.cc"
	.text
	.section	.text._ZNK2v88internal8compiler21CompilationDependency14PrepareInstallEv,"axG",@progbits,_ZNK2v88internal8compiler21CompilationDependency14PrepareInstallEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal8compiler21CompilationDependency14PrepareInstallEv
	.type	_ZNK2v88internal8compiler21CompilationDependency14PrepareInstallEv, @function
_ZNK2v88internal8compiler21CompilationDependency14PrepareInstallEv:
.LFB10214:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE10214:
	.size	_ZNK2v88internal8compiler21CompilationDependency14PrepareInstallEv, .-_ZNK2v88internal8compiler21CompilationDependency14PrepareInstallEv
	.section	.text._ZNK2v88internal8compiler42InitialMapInstanceSizePredictionDependency7InstallERKNS0_17MaybeObjectHandleE,"axG",@progbits,_ZNK2v88internal8compiler42InitialMapInstanceSizePredictionDependency7InstallERKNS0_17MaybeObjectHandleE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal8compiler42InitialMapInstanceSizePredictionDependency7InstallERKNS0_17MaybeObjectHandleE
	.type	_ZNK2v88internal8compiler42InitialMapInstanceSizePredictionDependency7InstallERKNS0_17MaybeObjectHandleE, @function
_ZNK2v88internal8compiler42InitialMapInstanceSizePredictionDependency7InstallERKNS0_17MaybeObjectHandleE:
.LFB18595:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE18595:
	.size	_ZNK2v88internal8compiler42InitialMapInstanceSizePredictionDependency7InstallERKNS0_17MaybeObjectHandleE, .-_ZNK2v88internal8compiler42InitialMapInstanceSizePredictionDependency7InstallERKNS0_17MaybeObjectHandleE
	.section	.text._ZNK2v88internal8compiler20InitialMapDependency7InstallERKNS0_17MaybeObjectHandleE,"axG",@progbits,_ZNK2v88internal8compiler20InitialMapDependency7InstallERKNS0_17MaybeObjectHandleE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal8compiler20InitialMapDependency7InstallERKNS0_17MaybeObjectHandleE
	.type	_ZNK2v88internal8compiler20InitialMapDependency7InstallERKNS0_17MaybeObjectHandleE, @function
_ZNK2v88internal8compiler20InitialMapDependency7InstallERKNS0_17MaybeObjectHandleE:
.LFB18538:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	leaq	24(%rdi), %rdi
	subq	$8, %rsp
	call	_ZNK2v88internal8compiler6MapRef6objectEv@PLT
	leaq	8(%rbx), %rdi
	movq	%rax, %r13
	call	_ZNK2v88internal8compiler9ObjectRef7isolateEv@PLT
	addq	$8, %rsp
	movq	%r13, %rdx
	movq	%r12, %rsi
	popq	%rbx
	movq	%rax, %rdi
	popq	%r12
	movl	$4, %ecx
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal13DependentCode17InstallDependencyEPNS0_7IsolateERKNS0_17MaybeObjectHandleENS0_6HandleINS0_10HeapObjectEEENS1_15DependencyGroupE@PLT
	.cfi_endproc
.LFE18538:
	.size	_ZNK2v88internal8compiler20InitialMapDependency7InstallERKNS0_17MaybeObjectHandleE, .-_ZNK2v88internal8compiler20InitialMapDependency7InstallERKNS0_17MaybeObjectHandleE
	.section	.text._ZNK2v88internal8compiler19StableMapDependency7InstallERKNS0_17MaybeObjectHandleE,"axG",@progbits,_ZNK2v88internal8compiler19StableMapDependency7InstallERKNS0_17MaybeObjectHandleE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal8compiler19StableMapDependency7InstallERKNS0_17MaybeObjectHandleE
	.type	_ZNK2v88internal8compiler19StableMapDependency7InstallERKNS0_17MaybeObjectHandleE, @function
_ZNK2v88internal8compiler19StableMapDependency7InstallERKNS0_17MaybeObjectHandleE:
.LFB18549:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	8(%rdi), %r12
	movq	%r12, %rdi
	subq	$8, %rsp
	call	_ZNK2v88internal8compiler6MapRef6objectEv@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZNK2v88internal8compiler9ObjectRef7isolateEv@PLT
	addq	$8, %rsp
	movq	%r14, %rdx
	movq	%r13, %rsi
	popq	%r12
	movq	%rax, %rdi
	popq	%r13
	movl	$1, %ecx
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal13DependentCode17InstallDependencyEPNS0_7IsolateERKNS0_17MaybeObjectHandleENS0_6HandleINS0_10HeapObjectEEENS1_15DependencyGroupE@PLT
	.cfi_endproc
.LFE18549:
	.size	_ZNK2v88internal8compiler19StableMapDependency7InstallERKNS0_17MaybeObjectHandleE, .-_ZNK2v88internal8compiler19StableMapDependency7InstallERKNS0_17MaybeObjectHandleE
	.section	.text._ZNK2v88internal8compiler24FieldConstnessDependency7InstallERKNS0_17MaybeObjectHandleE,"axG",@progbits,_ZNK2v88internal8compiler24FieldConstnessDependency7InstallERKNS0_17MaybeObjectHandleE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal8compiler24FieldConstnessDependency7InstallERKNS0_17MaybeObjectHandleE
	.type	_ZNK2v88internal8compiler24FieldConstnessDependency7InstallERKNS0_17MaybeObjectHandleE, @function
_ZNK2v88internal8compiler24FieldConstnessDependency7InstallERKNS0_17MaybeObjectHandleE:
.LFB18574:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	8(%rdi), %r12
	movq	%r12, %rdi
	subq	$8, %rsp
	call	_ZNK2v88internal8compiler6MapRef6objectEv@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZNK2v88internal8compiler9ObjectRef7isolateEv@PLT
	addq	$8, %rsp
	movq	%r14, %rdx
	movq	%r13, %rsi
	popq	%r12
	movq	%rax, %rdi
	popq	%r13
	movl	$3, %ecx
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal13DependentCode17InstallDependencyEPNS0_7IsolateERKNS0_17MaybeObjectHandleENS0_6HandleINS0_10HeapObjectEEENS1_15DependencyGroupE@PLT
	.cfi_endproc
.LFE18574:
	.size	_ZNK2v88internal8compiler24FieldConstnessDependency7InstallERKNS0_17MaybeObjectHandleE, .-_ZNK2v88internal8compiler24FieldConstnessDependency7InstallERKNS0_17MaybeObjectHandleE
	.section	.text._ZNK2v88internal8compiler19FieldTypeDependency7InstallERKNS0_17MaybeObjectHandleE,"axG",@progbits,_ZNK2v88internal8compiler19FieldTypeDependency7InstallERKNS0_17MaybeObjectHandleE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal8compiler19FieldTypeDependency7InstallERKNS0_17MaybeObjectHandleE
	.type	_ZNK2v88internal8compiler19FieldTypeDependency7InstallERKNS0_17MaybeObjectHandleE, @function
_ZNK2v88internal8compiler19FieldTypeDependency7InstallERKNS0_17MaybeObjectHandleE:
.LFB18569:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	8(%rdi), %r12
	movq	%r12, %rdi
	subq	$8, %rsp
	call	_ZNK2v88internal8compiler6MapRef6objectEv@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZNK2v88internal8compiler9ObjectRef7isolateEv@PLT
	addq	$8, %rsp
	movq	%r14, %rdx
	movq	%r13, %rsi
	popq	%r12
	movq	%rax, %rdi
	popq	%r13
	movl	$3, %ecx
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal13DependentCode17InstallDependencyEPNS0_7IsolateERKNS0_17MaybeObjectHandleENS0_6HandleINS0_10HeapObjectEEENS1_15DependencyGroupE@PLT
	.cfi_endproc
.LFE18569:
	.size	_ZNK2v88internal8compiler19FieldTypeDependency7InstallERKNS0_17MaybeObjectHandleE, .-_ZNK2v88internal8compiler19FieldTypeDependency7InstallERKNS0_17MaybeObjectHandleE
	.section	.text._ZNK2v88internal8compiler29FieldRepresentationDependency7InstallERKNS0_17MaybeObjectHandleE,"axG",@progbits,_ZNK2v88internal8compiler29FieldRepresentationDependency7InstallERKNS0_17MaybeObjectHandleE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal8compiler29FieldRepresentationDependency7InstallERKNS0_17MaybeObjectHandleE
	.type	_ZNK2v88internal8compiler29FieldRepresentationDependency7InstallERKNS0_17MaybeObjectHandleE, @function
_ZNK2v88internal8compiler29FieldRepresentationDependency7InstallERKNS0_17MaybeObjectHandleE:
.LFB18564:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	8(%rdi), %r12
	movq	%r12, %rdi
	subq	$8, %rsp
	call	_ZNK2v88internal8compiler6MapRef6objectEv@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZNK2v88internal8compiler9ObjectRef7isolateEv@PLT
	addq	$8, %rsp
	movq	%r14, %rdx
	movq	%r13, %rsi
	popq	%r12
	movq	%rax, %rdi
	popq	%r13
	movl	$3, %ecx
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal13DependentCode17InstallDependencyEPNS0_7IsolateERKNS0_17MaybeObjectHandleENS0_6HandleINS0_10HeapObjectEEENS1_15DependencyGroupE@PLT
	.cfi_endproc
.LFE18564:
	.size	_ZNK2v88internal8compiler29FieldRepresentationDependency7InstallERKNS0_17MaybeObjectHandleE, .-_ZNK2v88internal8compiler29FieldRepresentationDependency7InstallERKNS0_17MaybeObjectHandleE
	.section	.text._ZNK2v88internal8compiler20TransitionDependency7InstallERKNS0_17MaybeObjectHandleE,"axG",@progbits,_ZNK2v88internal8compiler20TransitionDependency7InstallERKNS0_17MaybeObjectHandleE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal8compiler20TransitionDependency7InstallERKNS0_17MaybeObjectHandleE
	.type	_ZNK2v88internal8compiler20TransitionDependency7InstallERKNS0_17MaybeObjectHandleE, @function
_ZNK2v88internal8compiler20TransitionDependency7InstallERKNS0_17MaybeObjectHandleE:
.LFB18554:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	8(%rdi), %r12
	movq	%r12, %rdi
	subq	$8, %rsp
	call	_ZNK2v88internal8compiler6MapRef6objectEv@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZNK2v88internal8compiler9ObjectRef7isolateEv@PLT
	addq	$8, %rsp
	movq	%r14, %rdx
	movq	%r13, %rsi
	popq	%r12
	movq	%rax, %rdi
	popq	%r13
	xorl	%ecx, %ecx
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal13DependentCode17InstallDependencyEPNS0_7IsolateERKNS0_17MaybeObjectHandleENS0_6HandleINS0_10HeapObjectEEENS1_15DependencyGroupE@PLT
	.cfi_endproc
.LFE18554:
	.size	_ZNK2v88internal8compiler20TransitionDependency7InstallERKNS0_17MaybeObjectHandleE, .-_ZNK2v88internal8compiler20TransitionDependency7InstallERKNS0_17MaybeObjectHandleE
	.section	.text._ZNK2v88internal8compiler29FieldRepresentationDependency7IsValidEv,"axG",@progbits,_ZNK2v88internal8compiler29FieldRepresentationDependency7IsValidEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal8compiler29FieldRepresentationDependency7IsValidEv
	.type	_ZNK2v88internal8compiler29FieldRepresentationDependency7IsValidEv, @function
_ZNK2v88internal8compiler29FieldRepresentationDependency7IsValidEv:
.LFB18563:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	8(%rdi), %rdi
	subq	$8, %rsp
	call	_ZNK2v88internal8compiler6MapRef6objectEv@PLT
	movq	(%rax), %rdx
	movl	24(%rbx), %eax
	leal	3(%rax,%rax,2), %eax
	movq	39(%rdx), %rdx
	sall	$3, %eax
	cltq
	movq	7(%rax,%rdx), %rax
	shrq	$38, %rax
	andl	$7, %eax
	cmpb	%al, 28(%rbx)
	sete	%al
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18563:
	.size	_ZNK2v88internal8compiler29FieldRepresentationDependency7IsValidEv, .-_ZNK2v88internal8compiler29FieldRepresentationDependency7IsValidEv
	.section	.text._ZNK2v88internal8compiler27PrototypePropertyDependency7InstallERKNS0_17MaybeObjectHandleE,"axG",@progbits,_ZNK2v88internal8compiler27PrototypePropertyDependency7InstallERKNS0_17MaybeObjectHandleE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal8compiler27PrototypePropertyDependency7InstallERKNS0_17MaybeObjectHandleE
	.type	_ZNK2v88internal8compiler27PrototypePropertyDependency7InstallERKNS0_17MaybeObjectHandleE, @function
_ZNK2v88internal8compiler27PrototypePropertyDependency7InstallERKNS0_17MaybeObjectHandleE:
.LFB18544:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	leaq	8(%rdi), %r12
	pushq	%rbx
	movq	%r12, %rdi
	subq	$16, %rsp
	.cfi_offset 3, -48
	call	_ZNK2v88internal8compiler13JSFunctionRef6objectEv@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZNK2v88internal8compiler9ObjectRef7isolateEv@PLT
	movq	%rax, %rbx
	movq	0(%r13), %rax
	movq	41112(%rbx), %rdi
	movq	55(%rax), %rsi
	testq	%rdi, %rdi
	je	.L19
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
.L20:
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef7isolateEv@PLT
	addq	$16, %rsp
	movq	%r13, %rdx
	movq	%r14, %rsi
	popq	%rbx
	movq	%rax, %rdi
	popq	%r12
	movl	$4, %ecx
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal13DependentCode17InstallDependencyEPNS0_7IsolateERKNS0_17MaybeObjectHandleENS0_6HandleINS0_10HeapObjectEEENS1_15DependencyGroupE@PLT
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	movq	41088(%rbx), %r13
	cmpq	41096(%rbx), %r13
	je	.L23
.L21:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, 0(%r13)
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L23:
	movq	%rbx, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L21
	.cfi_endproc
.LFE18544:
	.size	_ZNK2v88internal8compiler27PrototypePropertyDependency7InstallERKNS0_17MaybeObjectHandleE, .-_ZNK2v88internal8compiler27PrototypePropertyDependency7InstallERKNS0_17MaybeObjectHandleE
	.section	.text._ZNK2v88internal8compiler20TransitionDependency7IsValidEv,"axG",@progbits,_ZNK2v88internal8compiler20TransitionDependency7IsValidEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal8compiler20TransitionDependency7IsValidEv
	.type	_ZNK2v88internal8compiler20TransitionDependency7IsValidEv, @function
_ZNK2v88internal8compiler20TransitionDependency7IsValidEv:
.LFB18553:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$8, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNK2v88internal8compiler6MapRef6objectEv@PLT
	movq	(%rax), %rax
	movl	15(%rax), %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	shrl	$24, %eax
	xorl	$1, %eax
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE18553:
	.size	_ZNK2v88internal8compiler20TransitionDependency7IsValidEv, .-_ZNK2v88internal8compiler20TransitionDependency7IsValidEv
	.section	.text._ZNK2v88internal8compiler23PretenureModeDependency7InstallERKNS0_17MaybeObjectHandleE,"axG",@progbits,_ZNK2v88internal8compiler23PretenureModeDependency7InstallERKNS0_17MaybeObjectHandleE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal8compiler23PretenureModeDependency7InstallERKNS0_17MaybeObjectHandleE
	.type	_ZNK2v88internal8compiler23PretenureModeDependency7InstallERKNS0_17MaybeObjectHandleE, @function
_ZNK2v88internal8compiler23PretenureModeDependency7InstallERKNS0_17MaybeObjectHandleE:
.LFB18559:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	8(%rdi), %r12
	movq	%r12, %rdi
	subq	$8, %rsp
	call	_ZNK2v88internal8compiler17AllocationSiteRef6objectEv@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZNK2v88internal8compiler9ObjectRef7isolateEv@PLT
	addq	$8, %rsp
	movq	%r14, %rdx
	movq	%r13, %rsi
	popq	%r12
	movq	%rax, %rdi
	popq	%r13
	movl	$5, %ecx
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal13DependentCode17InstallDependencyEPNS0_7IsolateERKNS0_17MaybeObjectHandleENS0_6HandleINS0_10HeapObjectEEENS1_15DependencyGroupE@PLT
	.cfi_endproc
.LFE18559:
	.size	_ZNK2v88internal8compiler23PretenureModeDependency7InstallERKNS0_17MaybeObjectHandleE, .-_ZNK2v88internal8compiler23PretenureModeDependency7InstallERKNS0_17MaybeObjectHandleE
	.section	.text._ZNK2v88internal8compiler22ElementsKindDependency7InstallERKNS0_17MaybeObjectHandleE,"axG",@progbits,_ZNK2v88internal8compiler22ElementsKindDependency7InstallERKNS0_17MaybeObjectHandleE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal8compiler22ElementsKindDependency7InstallERKNS0_17MaybeObjectHandleE
	.type	_ZNK2v88internal8compiler22ElementsKindDependency7InstallERKNS0_17MaybeObjectHandleE, @function
_ZNK2v88internal8compiler22ElementsKindDependency7InstallERKNS0_17MaybeObjectHandleE:
.LFB18589:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	8(%rdi), %r12
	movq	%r12, %rdi
	subq	$8, %rsp
	call	_ZNK2v88internal8compiler17AllocationSiteRef6objectEv@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZNK2v88internal8compiler9ObjectRef7isolateEv@PLT
	addq	$8, %rsp
	movq	%r14, %rdx
	movq	%r13, %rsi
	popq	%r12
	movq	%rax, %rdi
	popq	%r13
	movl	$6, %ecx
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal13DependentCode17InstallDependencyEPNS0_7IsolateERKNS0_17MaybeObjectHandleENS0_6HandleINS0_10HeapObjectEEENS1_15DependencyGroupE@PLT
	.cfi_endproc
.LFE18589:
	.size	_ZNK2v88internal8compiler22ElementsKindDependency7InstallERKNS0_17MaybeObjectHandleE, .-_ZNK2v88internal8compiler22ElementsKindDependency7InstallERKNS0_17MaybeObjectHandleE
	.section	.text._ZNK2v88internal8compiler22ElementsKindDependency7IsValidEv,"axG",@progbits,_ZNK2v88internal8compiler22ElementsKindDependency7IsValidEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal8compiler22ElementsKindDependency7IsValidEv
	.type	_ZNK2v88internal8compiler22ElementsKindDependency7IsValidEv, @function
_ZNK2v88internal8compiler22ElementsKindDependency7IsValidEv:
.LFB18588:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	addq	$8, %rdi
	subq	$8, %rsp
	call	_ZNK2v88internal8compiler17AllocationSiteRef6objectEv@PLT
	movq	(%rax), %rax
	movq	7(%rax), %rdx
	movq	%rdx, %rax
	sarq	$32, %rax
	andl	$31, %eax
	testb	$1, %dl
	jne	.L34
	cmpb	%al, 24(%rbx)
	sete	%al
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	.cfi_restore_state
	movq	-1(%rdx), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	cmpb	%al, 24(%rbx)
	sete	%al
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18588:
	.size	_ZNK2v88internal8compiler22ElementsKindDependency7IsValidEv, .-_ZNK2v88internal8compiler22ElementsKindDependency7IsValidEv
	.section	.text._ZNK2v88internal8compiler23PretenureModeDependency7IsValidEv,"axG",@progbits,_ZNK2v88internal8compiler23PretenureModeDependency7IsValidEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal8compiler23PretenureModeDependency7IsValidEv
	.type	_ZNK2v88internal8compiler23PretenureModeDependency7IsValidEv, @function
_ZNK2v88internal8compiler23PretenureModeDependency7IsValidEv:
.LFB18558:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$8, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movzbl	16(%rdi), %ebx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal8compiler17AllocationSiteRef6objectEv@PLT
	leaq	-32(%rbp), %rdi
	movq	(%rax), %rax
	movq	%rax, -32(%rbp)
	call	_ZNK2v88internal14AllocationSite17GetAllocationTypeEv@PLT
	cmpb	%al, %bl
	sete	%al
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L38
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L38:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18558:
	.size	_ZNK2v88internal8compiler23PretenureModeDependency7IsValidEv, .-_ZNK2v88internal8compiler23PretenureModeDependency7IsValidEv
	.section	.text._ZNK2v88internal8compiler24GlobalPropertyDependency7InstallERKNS0_17MaybeObjectHandleE,"axG",@progbits,_ZNK2v88internal8compiler24GlobalPropertyDependency7InstallERKNS0_17MaybeObjectHandleE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal8compiler24GlobalPropertyDependency7InstallERKNS0_17MaybeObjectHandleE
	.type	_ZNK2v88internal8compiler24GlobalPropertyDependency7InstallERKNS0_17MaybeObjectHandleE, @function
_ZNK2v88internal8compiler24GlobalPropertyDependency7InstallERKNS0_17MaybeObjectHandleE:
.LFB18579:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	8(%rdi), %r12
	movq	%r12, %rdi
	subq	$8, %rsp
	call	_ZNK2v88internal8compiler15PropertyCellRef6objectEv@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZNK2v88internal8compiler9ObjectRef7isolateEv@PLT
	addq	$8, %rsp
	movq	%r14, %rdx
	movq	%r13, %rsi
	popq	%r12
	movq	%rax, %rdi
	popq	%r13
	movl	$2, %ecx
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal13DependentCode17InstallDependencyEPNS0_7IsolateERKNS0_17MaybeObjectHandleENS0_6HandleINS0_10HeapObjectEEENS1_15DependencyGroupE@PLT
	.cfi_endproc
.LFE18579:
	.size	_ZNK2v88internal8compiler24GlobalPropertyDependency7InstallERKNS0_17MaybeObjectHandleE, .-_ZNK2v88internal8compiler24GlobalPropertyDependency7InstallERKNS0_17MaybeObjectHandleE
	.section	.text._ZNK2v88internal8compiler19ProtectorDependency7InstallERKNS0_17MaybeObjectHandleE,"axG",@progbits,_ZNK2v88internal8compiler19ProtectorDependency7InstallERKNS0_17MaybeObjectHandleE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal8compiler19ProtectorDependency7InstallERKNS0_17MaybeObjectHandleE
	.type	_ZNK2v88internal8compiler19ProtectorDependency7InstallERKNS0_17MaybeObjectHandleE, @function
_ZNK2v88internal8compiler19ProtectorDependency7InstallERKNS0_17MaybeObjectHandleE:
.LFB18584:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	8(%rdi), %r12
	movq	%r12, %rdi
	subq	$8, %rsp
	call	_ZNK2v88internal8compiler15PropertyCellRef6objectEv@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZNK2v88internal8compiler9ObjectRef7isolateEv@PLT
	addq	$8, %rsp
	movq	%r14, %rdx
	movq	%r13, %rsi
	popq	%r12
	movq	%rax, %rdi
	popq	%r13
	movl	$2, %ecx
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal13DependentCode17InstallDependencyEPNS0_7IsolateERKNS0_17MaybeObjectHandleENS0_6HandleINS0_10HeapObjectEEENS1_15DependencyGroupE@PLT
	.cfi_endproc
.LFE18584:
	.size	_ZNK2v88internal8compiler19ProtectorDependency7InstallERKNS0_17MaybeObjectHandleE, .-_ZNK2v88internal8compiler19ProtectorDependency7InstallERKNS0_17MaybeObjectHandleE
	.section	.text._ZNK2v88internal8compiler24GlobalPropertyDependency7IsValidEv,"axG",@progbits,_ZNK2v88internal8compiler24GlobalPropertyDependency7IsValidEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal8compiler24GlobalPropertyDependency7IsValidEv
	.type	_ZNK2v88internal8compiler24GlobalPropertyDependency7IsValidEv, @function
_ZNK2v88internal8compiler24GlobalPropertyDependency7IsValidEv:
.LFB18578:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	8(%rdi), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$8, %rsp
	call	_ZNK2v88internal8compiler15PropertyCellRef6objectEv@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZNK2v88internal8compiler9ObjectRef7isolateEv@PLT
	xorl	%r8d, %r8d
	movq	%rax, %r9
	movq	(%r12), %rax
	movq	23(%rax), %rcx
	cmpq	%rcx, 96(%r9)
	je	.L43
	movslq	19(%rax), %rdx
	movl	%edx, %eax
	shrl	$6, %eax
	andl	$3, %eax
	cmpl	%eax, 24(%rbx)
	jne	.L43
	shrl	$3, %edx
	andl	$1, %edx
	cmpb	28(%rbx), %dl
	sete	%r8b
.L43:
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18578:
	.size	_ZNK2v88internal8compiler24GlobalPropertyDependency7IsValidEv, .-_ZNK2v88internal8compiler24GlobalPropertyDependency7IsValidEv
	.section	.text._ZNK2v88internal8compiler19ProtectorDependency7IsValidEv,"axG",@progbits,_ZNK2v88internal8compiler19ProtectorDependency7IsValidEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal8compiler19ProtectorDependency7IsValidEv
	.type	_ZNK2v88internal8compiler19ProtectorDependency7IsValidEv, @function
_ZNK2v88internal8compiler19ProtectorDependency7IsValidEv:
.LFB18583:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$8, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNK2v88internal8compiler15PropertyCellRef6objectEv@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	(%rax), %rdx
	movabsq	$4294967296, %rax
	cmpq	%rax, 23(%rdx)
	sete	%al
	ret
	.cfi_endproc
.LFE18583:
	.size	_ZNK2v88internal8compiler19ProtectorDependency7IsValidEv, .-_ZNK2v88internal8compiler19ProtectorDependency7IsValidEv
	.section	.text._ZNK2v88internal8compiler19FieldTypeDependency7IsValidEv,"axG",@progbits,_ZNK2v88internal8compiler19FieldTypeDependency7IsValidEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal8compiler19FieldTypeDependency7IsValidEv
	.type	_ZNK2v88internal8compiler19FieldTypeDependency7IsValidEv, @function
_ZNK2v88internal8compiler19FieldTypeDependency7IsValidEv:
.LFB18568:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	leaq	8(%rdi), %rdi
	subq	$8, %rsp
	call	_ZNK2v88internal8compiler6MapRef6objectEv@PLT
	leaq	32(%rbx), %rdi
	movq	%rax, %r13
	call	_ZNK2v88internal8compiler9ObjectRef6objectEv@PLT
	movq	0(%r13), %rdx
	movq	%rax, %r12
	movl	24(%rbx), %eax
	movq	39(%rdx), %rdx
	leal	3(%rax,%rax,2), %eax
	sall	$3, %eax
	cltq
	movq	15(%rax,%rdx), %rdi
	call	_ZN2v88internal3Map15UnwrapFieldTypeENS0_11MaybeObjectE@PLT
	cmpq	%rax, (%r12)
	sete	%al
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18568:
	.size	_ZNK2v88internal8compiler19FieldTypeDependency7IsValidEv, .-_ZNK2v88internal8compiler19FieldTypeDependency7IsValidEv
	.section	.text._ZNK2v88internal8compiler19StableMapDependency7IsValidEv,"axG",@progbits,_ZNK2v88internal8compiler19StableMapDependency7IsValidEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal8compiler19StableMapDependency7IsValidEv
	.type	_ZNK2v88internal8compiler19StableMapDependency7IsValidEv, @function
_ZNK2v88internal8compiler19StableMapDependency7IsValidEv:
.LFB18548:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$8, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNK2v88internal8compiler6MapRef6objectEv@PLT
	movq	(%rax), %rax
	movl	15(%rax), %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	shrl	$25, %eax
	xorl	$1, %eax
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE18548:
	.size	_ZNK2v88internal8compiler19StableMapDependency7IsValidEv, .-_ZNK2v88internal8compiler19StableMapDependency7IsValidEv
	.section	.text._ZNK2v88internal8compiler24FieldConstnessDependency7IsValidEv,"axG",@progbits,_ZNK2v88internal8compiler24FieldConstnessDependency7IsValidEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal8compiler24FieldConstnessDependency7IsValidEv
	.type	_ZNK2v88internal8compiler24FieldConstnessDependency7IsValidEv, @function
_ZNK2v88internal8compiler24FieldConstnessDependency7IsValidEv:
.LFB18573:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	8(%rdi), %rdi
	subq	$8, %rsp
	call	_ZNK2v88internal8compiler6MapRef6objectEv@PLT
	movq	(%rax), %rdx
	movl	24(%rbx), %eax
	leal	3(%rax,%rax,2), %eax
	movq	39(%rdx), %rdx
	sall	$3, %eax
	cltq
	movq	7(%rax,%rdx), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	shrq	$34, %rax
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE18573:
	.size	_ZNK2v88internal8compiler24FieldConstnessDependency7IsValidEv, .-_ZNK2v88internal8compiler24FieldConstnessDependency7IsValidEv
	.section	.text._ZNK2v88internal8compiler27PrototypePropertyDependency14PrepareInstallEv,"axG",@progbits,_ZNK2v88internal8compiler27PrototypePropertyDependency14PrepareInstallEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal8compiler27PrototypePropertyDependency14PrepareInstallEv
	.type	_ZNK2v88internal8compiler27PrototypePropertyDependency14PrepareInstallEv, @function
_ZNK2v88internal8compiler27PrototypePropertyDependency14PrepareInstallEv:
.LFB18543:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$8, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNK2v88internal8compiler13JSFunctionRef6objectEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	55(%rax), %rax
	movq	-1(%rax), %rax
	cmpw	$68, 11(%rax)
	jne	.L59
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore_state
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal10JSFunction19EnsureHasInitialMapENS0_6HandleIS1_EE@PLT
	.cfi_endproc
.LFE18543:
	.size	_ZNK2v88internal8compiler27PrototypePropertyDependency14PrepareInstallEv, .-_ZNK2v88internal8compiler27PrototypePropertyDependency14PrepareInstallEv
	.section	.text._ZNK2v88internal8compiler20InitialMapDependency7IsValidEv,"axG",@progbits,_ZNK2v88internal8compiler20InitialMapDependency7IsValidEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal8compiler20InitialMapDependency7IsValidEv
	.type	_ZNK2v88internal8compiler20InitialMapDependency7IsValidEv, @function
_ZNK2v88internal8compiler20InitialMapDependency7IsValidEv:
.LFB18537:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	addq	$8, %rdi
	call	_ZNK2v88internal8compiler13JSFunctionRef6objectEv@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	movq	55(%rax), %rax
	movq	-1(%rax), %rdx
	xorl	%eax, %eax
	cmpw	$68, 11(%rdx)
	je	.L65
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	.cfi_restore_state
	leaq	24(%rbx), %rdi
	call	_ZNK2v88internal8compiler6MapRef6objectEv@PLT
	movq	(%r12), %rdx
	popq	%rbx
	movq	(%rax), %rax
	popq	%r12
	cmpq	%rax, 55(%rdx)
	popq	%rbp
	.cfi_def_cfa 7, 8
	sete	%al
	ret
	.cfi_endproc
.LFE18537:
	.size	_ZNK2v88internal8compiler20InitialMapDependency7IsValidEv, .-_ZNK2v88internal8compiler20InitialMapDependency7IsValidEv
	.section	.text._ZNK2v88internal8compiler42InitialMapInstanceSizePredictionDependency7IsValidEv,"axG",@progbits,_ZNK2v88internal8compiler42InitialMapInstanceSizePredictionDependency7IsValidEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal8compiler42InitialMapInstanceSizePredictionDependency7IsValidEv
	.type	_ZNK2v88internal8compiler42InitialMapInstanceSizePredictionDependency7IsValidEv, @function
_ZNK2v88internal8compiler42InitialMapInstanceSizePredictionDependency7IsValidEv:
.LFB18593:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	8(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal8compiler13JSFunctionRef6objectEv@PLT
	movq	(%rax), %rax
	movq	55(%rax), %rax
	movq	-1(%rax), %rdx
	xorl	%eax, %eax
	cmpw	$68, 11(%rdx)
	jne	.L66
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13JSFunctionRef6objectEv@PLT
	movq	%r12, %rdi
	movq	(%rax), %rax
	movq	%rax, -32(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef7isolateEv@PLT
	leaq	-32(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal10JSFunction31ComputeInstanceSizeWithMinSlackEPNS0_7IsolateE@PLT
	cmpl	%eax, 24(%rbx)
	sete	%al
.L66:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L72
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L72:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18593:
	.size	_ZNK2v88internal8compiler42InitialMapInstanceSizePredictionDependency7IsValidEv, .-_ZNK2v88internal8compiler42InitialMapInstanceSizePredictionDependency7IsValidEv
	.section	.text._ZNK2v88internal8compiler42InitialMapInstanceSizePredictionDependency14PrepareInstallEv,"axG",@progbits,_ZNK2v88internal8compiler42InitialMapInstanceSizePredictionDependency14PrepareInstallEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal8compiler42InitialMapInstanceSizePredictionDependency14PrepareInstallEv
	.type	_ZNK2v88internal8compiler42InitialMapInstanceSizePredictionDependency14PrepareInstallEv, @function
_ZNK2v88internal8compiler42InitialMapInstanceSizePredictionDependency14PrepareInstallEv:
.LFB18594:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$8, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal8compiler13JSFunctionRef6objectEv@PLT
	movq	(%rax), %rax
	movq	-1(%rax), %rdx
	movzbl	13(%rdx), %edx
	testb	%dl, %dl
	js	.L82
.L73:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L83
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L82:
	.cfi_restore_state
	movq	55(%rax), %rdx
	movq	-1(%rdx), %rcx
	cmpw	$68, 11(%rcx)
	jne	.L73
	movl	15(%rdx), %ecx
	shrl	$29, %ecx
	je	.L73
	andq	$-262144, %rax
	movq	%rdx, -16(%rbp)
	leaq	-16(%rbp), %rdi
	movq	24(%rax), %rsi
	subq	$37592, %rsi
	call	_ZN2v88internal3Map29CompleteInobjectSlackTrackingEPNS0_7IsolateE@PLT
	jmp	.L73
.L83:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18594:
	.size	_ZNK2v88internal8compiler42InitialMapInstanceSizePredictionDependency14PrepareInstallEv, .-_ZNK2v88internal8compiler42InitialMapInstanceSizePredictionDependency14PrepareInstallEv
	.section	.rodata._ZN2v88internal8compiler12_GLOBAL__N_128DependOnStablePrototypeChainEPNS1_23CompilationDependenciesENS1_6MapRefENS_4base8OptionalINS1_11JSObjectRefEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"proto.map().oddball_type() == OddballType::kNull"
	.section	.rodata._ZN2v88internal8compiler12_GLOBAL__N_128DependOnStablePrototypeChainEPNS1_23CompilationDependenciesENS1_6MapRefENS_4base8OptionalINS1_11JSObjectRefEEE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_128DependOnStablePrototypeChainEPNS1_23CompilationDependenciesENS1_6MapRefENS_4base8OptionalINS1_11JSObjectRefEEE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_128DependOnStablePrototypeChainEPNS1_23CompilationDependenciesENS1_6MapRefENS_4base8OptionalINS1_11JSObjectRefEEE, @function
_ZN2v88internal8compiler12_GLOBAL__N_128DependOnStablePrototypeChainEPNS1_23CompilationDependenciesENS1_6MapRefENS_4base8OptionalINS1_11JSObjectRefEEE:
.LFB18617:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	16+_ZTVN2v88internal8compiler19StableMapDependencyE(%rip), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-96(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-112(%rbp), %rbx
	subq	$72, %rsp
	movq	%rsi, -112(%rbp)
	movq	%rdx, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L93:
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler6MapRef9prototypeEv@PLT
	movq	%r12, %rdi
	movq	%rax, -96(%rbp)
	movq	%rdx, -88(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef10IsJSObjectEv@PLT
	movq	%r12, %rdi
	testb	%al, %al
	je	.L106
	call	_ZNK2v88internal8compiler13HeapObjectRef3mapEv@PLT
	movq	%rbx, %rdi
	movq	%rax, -112(%rbp)
	movq	%rdx, -104(%rbp)
	call	_ZNK2v88internal8compiler6MapRef13CanTransitionEv@PLT
	testb	%al, %al
	jne	.L107
.L87:
	cmpb	$0, 16(%rbp)
	je	.L93
	leaq	24(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef6equalsERKS2_@PLT
	testb	%al, %al
	je	.L93
.L84:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L108
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L107:
	.cfi_restore_state
	movq	0(%r13), %rdi
	movq	16(%rdi), %r15
	movq	24(%rdi), %rax
	subq	%r15, %rax
	cmpq	$23, %rax
	jbe	.L109
	leaq	24(%r15), %rax
	movq	%rax, 16(%rdi)
.L89:
	movq	%r14, (%r15)
	movdqu	-112(%rbp), %xmm0
	movups	%xmm0, 8(%r15)
	movq	16(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$15, %rdx
	jbe	.L110
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L91:
	movq	$0, (%rax)
	movq	%r15, 8(%rax)
	movq	24(%r13), %rdx
	movq	%rdx, (%rax)
	movq	%rax, 24(%r13)
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L106:
	call	_ZNK2v88internal8compiler13HeapObjectRef3mapEv@PLT
	leaq	-80(%rbp), %rdi
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZNK2v88internal8compiler6MapRef12oddball_typeEv@PLT
	cmpb	$3, %al
	je	.L84
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L110:
	movl	$16, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L109:
	movl	$24, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r15
	jmp	.L89
.L108:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18617:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_128DependOnStablePrototypeChainEPNS1_23CompilationDependenciesENS1_6MapRefENS_4base8OptionalINS1_11JSObjectRefEEE, .-_ZN2v88internal8compiler12_GLOBAL__N_128DependOnStablePrototypeChainEPNS1_23CompilationDependenciesENS1_6MapRefENS_4base8OptionalINS1_11JSObjectRefEEE
	.section	.text._ZNK2v88internal8compiler27PrototypePropertyDependency7IsValidEv,"axG",@progbits,_ZNK2v88internal8compiler27PrototypePropertyDependency7IsValidEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal8compiler27PrototypePropertyDependency7IsValidEv
	.type	_ZNK2v88internal8compiler27PrototypePropertyDependency7IsValidEv, @function
_ZNK2v88internal8compiler27PrototypePropertyDependency7IsValidEv:
.LFB18542:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	addq	$8, %rdi
	call	_ZNK2v88internal8compiler13JSFunctionRef6objectEv@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	movq	-1(%rax), %rdx
	movzbl	13(%rdx), %edx
	testb	%dl, %dl
	js	.L112
.L118:
	xorl	%eax, %eax
.L111:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L112:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	movzbl	13(%rdx), %edx
	andl	$1, %edx
	je	.L114
.L117:
	movq	-1(%rax), %rdx
	movzbl	13(%rdx), %edx
	testb	%dl, %dl
	jns	.L116
	movq	-1(%rax), %rdx
	testb	$64, 13(%rdx)
	je	.L116
.L119:
	movq	-1(%rax), %rax
	movzbl	13(%rax), %eax
	testb	$1, %al
	jne	.L118
	leaq	24(%rbx), %rdi
	call	_ZNK2v88internal8compiler9ObjectRef6objectEv@PLT
	movq	(%rax), %rdx
	movq	(%r12), %rax
	movq	-1(%rax), %rcx
	movzbl	13(%rcx), %ecx
	andl	$1, %ecx
	je	.L120
	movq	-1(%rax), %rax
.L133:
	movq	31(%rax), %rax
	testb	$1, %al
	jne	.L134
.L123:
	cmpq	%rax, %rdx
	sete	%al
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L116:
	movq	23(%rax), %rdx
	movl	47(%rdx), %edx
	andl	$31, %edx
	subl	$12, %edx
	cmpb	$3, %dl
	jbe	.L119
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L114:
	movq	55(%rax), %rdx
	movq	-1(%rdx), %rcx
	cmpw	$68, 11(%rcx)
	je	.L117
	movq	%rax, %rcx
	andq	$-262144, %rcx
	movq	24(%rcx), %rcx
	cmpq	-37496(%rcx), %rdx
	jne	.L117
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L120:
	movq	55(%rax), %rax
	movq	-1(%rax), %rcx
	cmpw	$68, 11(%rcx)
	jne	.L123
	movq	23(%rax), %rax
	jmp	.L123
.L134:
	movq	-1(%rax), %rcx
	cmpw	$68, 11(%rcx)
	jne	.L123
	jmp	.L133
	.cfi_endproc
.LFE18542:
	.size	_ZNK2v88internal8compiler27PrototypePropertyDependency7IsValidEv, .-_ZNK2v88internal8compiler27PrototypePropertyDependency7IsValidEv
	.section	.rodata._ZN2v88internal8compiler15PropertyCellRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb.str1.1,"aMS",@progbits,1
.LC2:
	.string	"IsPropertyCell()"
	.section	.text._ZN2v88internal8compiler15PropertyCellRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,"axG",@progbits,_ZN2v88internal8compiler15PropertyCellRefC5EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler15PropertyCellRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.type	_ZN2v88internal8compiler15PropertyCellRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, @function
_ZN2v88internal8compiler15PropertyCellRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb:
.LFB9613:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%ecx, %ebx
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	testb	%bl, %bl
	jne	.L138
.L135:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L138:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef14IsPropertyCellEv@PLT
	testb	%al, %al
	jne	.L135
	leaq	.LC2(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE9613:
	.size	_ZN2v88internal8compiler15PropertyCellRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, .-_ZN2v88internal8compiler15PropertyCellRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.weak	_ZN2v88internal8compiler15PropertyCellRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.set	_ZN2v88internal8compiler15PropertyCellRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,_ZN2v88internal8compiler15PropertyCellRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.section	.rodata._ZN2v88internal8compiler6MapRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb.str1.1,"aMS",@progbits,1
.LC3:
	.string	"IsMap()"
	.section	.text._ZN2v88internal8compiler6MapRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,"axG",@progbits,_ZN2v88internal8compiler6MapRefC5EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler6MapRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.type	_ZN2v88internal8compiler6MapRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, @function
_ZN2v88internal8compiler6MapRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb:
.LFB9727:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%ecx, %ebx
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	testb	%bl, %bl
	jne	.L142
.L139:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L142:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef5IsMapEv@PLT
	testb	%al, %al
	jne	.L139
	leaq	.LC3(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE9727:
	.size	_ZN2v88internal8compiler6MapRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, .-_ZN2v88internal8compiler6MapRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.weak	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.set	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,_ZN2v88internal8compiler6MapRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.section	.text._ZN2v88internal8compiler23CompilationDependenciesC2EPNS1_12JSHeapBrokerEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23CompilationDependenciesC2EPNS1_12JSHeapBrokerEPNS0_4ZoneE
	.type	_ZN2v88internal8compiler23CompilationDependenciesC2EPNS1_12JSHeapBrokerEPNS0_4ZoneE, @function
_ZN2v88internal8compiler23CompilationDependenciesC2EPNS1_12JSHeapBrokerEPNS0_4ZoneE:
.LFB18529:
	.cfi_startproc
	endbr64
	movq	%rdx, %xmm1
	movq	%rsi, %xmm2
	movq	%rdx, 16(%rdi)
	punpcklqdq	%xmm2, %xmm1
	movq	$0, 24(%rdi)
	movups	%xmm1, (%rdi)
	ret
	.cfi_endproc
.LFE18529:
	.size	_ZN2v88internal8compiler23CompilationDependenciesC2EPNS1_12JSHeapBrokerEPNS0_4ZoneE, .-_ZN2v88internal8compiler23CompilationDependenciesC2EPNS1_12JSHeapBrokerEPNS0_4ZoneE
	.globl	_ZN2v88internal8compiler23CompilationDependenciesC1EPNS1_12JSHeapBrokerEPNS0_4ZoneE
	.set	_ZN2v88internal8compiler23CompilationDependenciesC1EPNS1_12JSHeapBrokerEPNS0_4ZoneE,_ZN2v88internal8compiler23CompilationDependenciesC2EPNS1_12JSHeapBrokerEPNS0_4ZoneE
	.section	.text._ZN2v88internal8compiler23CompilationDependencies16RecordDependencyEPKNS1_21CompilationDependencyE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23CompilationDependencies16RecordDependencyEPKNS1_21CompilationDependencyE
	.type	_ZN2v88internal8compiler23CompilationDependencies16RecordDependencyEPKNS1_21CompilationDependencyE, @function
_ZN2v88internal8compiler23CompilationDependencies16RecordDependencyEPKNS1_21CompilationDependencyE:
.LFB18596:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L152
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	16(%rdi), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$15, %rdx
	jbe	.L155
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L147:
	movq	$0, (%rax)
	movq	%r12, 8(%rax)
	movq	24(%rbx), %rdx
	movq	%rdx, (%rax)
	movq	%rax, 24(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L152:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.p2align 4,,10
	.p2align 3
.L155:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movl	$16, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L147
	.cfi_endproc
.LFE18596:
	.size	_ZN2v88internal8compiler23CompilationDependencies16RecordDependencyEPKNS1_21CompilationDependencyE, .-_ZN2v88internal8compiler23CompilationDependencies16RecordDependencyEPKNS1_21CompilationDependencyE
	.section	.text._ZN2v88internal8compiler23CompilationDependencies18DependOnInitialMapERKNS1_13JSFunctionRefE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23CompilationDependencies18DependOnInitialMapERKNS1_13JSFunctionRefE
	.type	_ZN2v88internal8compiler23CompilationDependencies18DependOnInitialMapERKNS1_13JSFunctionRefE, @function
_ZN2v88internal8compiler23CompilationDependencies18DependOnInitialMapERKNS1_13JSFunctionRefE:
.LFB18597:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	call	_ZNK2v88internal8compiler13JSFunctionRef11initial_mapEv@PLT
	movq	(%r12), %rdi
	movq	%rax, %r14
	movq	%rdx, %r15
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	$39, %rax
	jbe	.L162
	leaq	40(%rbx), %rax
	movq	%rax, 16(%rdi)
.L158:
	movdqu	0(%r13), %xmm0
	leaq	16+_ZTVN2v88internal8compiler20InitialMapDependencyE(%rip), %rax
	movq	%r14, 24(%rbx)
	movq	%rax, (%rbx)
	movq	%r15, 32(%rbx)
	movups	%xmm0, 8(%rbx)
	movq	16(%r12), %rdi
	movq	16(%rdi), %rdx
	movq	24(%rdi), %rax
	subq	%rdx, %rax
	cmpq	$15, %rax
	jbe	.L163
	leaq	16(%rdx), %rax
	movq	%rax, 16(%rdi)
.L160:
	movq	$0, (%rdx)
	movq	%rbx, 8(%rdx)
	movq	24(%r12), %rax
	movq	%rax, (%rdx)
	movq	%r14, %rax
	movq	%rdx, 24(%r12)
	addq	$8, %rsp
	movq	%r15, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L162:
	.cfi_restore_state
	movl	$40, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rbx
	jmp	.L158
	.p2align 4,,10
	.p2align 3
.L163:
	movl	$16, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L160
	.cfi_endproc
.LFE18597:
	.size	_ZN2v88internal8compiler23CompilationDependencies18DependOnInitialMapERKNS1_13JSFunctionRefE, .-_ZN2v88internal8compiler23CompilationDependencies18DependOnInitialMapERKNS1_13JSFunctionRefE
	.section	.text._ZN2v88internal8compiler23CompilationDependencies25DependOnPrototypePropertyERKNS1_13JSFunctionRefE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23CompilationDependencies25DependOnPrototypePropertyERKNS1_13JSFunctionRefE
	.type	_ZN2v88internal8compiler23CompilationDependencies25DependOnPrototypePropertyERKNS1_13JSFunctionRefE, @function
_ZN2v88internal8compiler23CompilationDependencies25DependOnPrototypePropertyERKNS1_13JSFunctionRefE:
.LFB18598:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	call	_ZNK2v88internal8compiler13JSFunctionRef9prototypeEv@PLT
	movq	(%r12), %rdi
	movq	%rax, %r14
	movq	%rdx, %r15
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	$39, %rax
	jbe	.L170
	leaq	40(%rbx), %rax
	movq	%rax, 16(%rdi)
.L166:
	movdqu	0(%r13), %xmm0
	leaq	16+_ZTVN2v88internal8compiler27PrototypePropertyDependencyE(%rip), %rax
	movq	%r14, 24(%rbx)
	movq	%rax, (%rbx)
	movq	%r15, 32(%rbx)
	movups	%xmm0, 8(%rbx)
	movq	16(%r12), %rdi
	movq	16(%rdi), %rdx
	movq	24(%rdi), %rax
	subq	%rdx, %rax
	cmpq	$15, %rax
	jbe	.L171
	leaq	16(%rdx), %rax
	movq	%rax, 16(%rdi)
.L168:
	movq	$0, (%rdx)
	movq	%rbx, 8(%rdx)
	movq	24(%r12), %rax
	movq	%rax, (%rdx)
	movq	%r14, %rax
	movq	%rdx, 24(%r12)
	addq	$8, %rsp
	movq	%r15, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L170:
	.cfi_restore_state
	movl	$40, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rbx
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L171:
	movl	$16, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L168
	.cfi_endproc
.LFE18598:
	.size	_ZN2v88internal8compiler23CompilationDependencies25DependOnPrototypePropertyERKNS1_13JSFunctionRefE, .-_ZN2v88internal8compiler23CompilationDependencies25DependOnPrototypePropertyERKNS1_13JSFunctionRefE
	.section	.text._ZN2v88internal8compiler23CompilationDependencies17DependOnStableMapERKNS1_6MapRefE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23CompilationDependencies17DependOnStableMapERKNS1_6MapRefE
	.type	_ZN2v88internal8compiler23CompilationDependencies17DependOnStableMapERKNS1_6MapRefE, @function
_ZN2v88internal8compiler23CompilationDependencies17DependOnStableMapERKNS1_6MapRefE:
.LFB18599:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	_ZNK2v88internal8compiler6MapRef13CanTransitionEv@PLT
	testb	%al, %al
	jne	.L182
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L182:
	.cfi_restore_state
	movq	(%r12), %rdi
	movq	16(%rdi), %r13
	movq	24(%rdi), %rax
	subq	%r13, %rax
	cmpq	$23, %rax
	jbe	.L183
	leaq	24(%r13), %rax
	movq	%rax, 16(%rdi)
.L175:
	movdqu	(%rbx), %xmm0
	leaq	16+_ZTVN2v88internal8compiler19StableMapDependencyE(%rip), %rax
	movq	%rax, 0(%r13)
	movups	%xmm0, 8(%r13)
	movq	16(%r12), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$15, %rdx
	jbe	.L184
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L177:
	movq	$0, (%rax)
	movq	%r13, 8(%rax)
	movq	24(%r12), %rdx
	movq	%rdx, (%rax)
	movq	%rax, 24(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L183:
	.cfi_restore_state
	movl	$24, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r13
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L184:
	movl	$16, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L177
	.cfi_endproc
.LFE18599:
	.size	_ZN2v88internal8compiler23CompilationDependencies17DependOnStableMapERKNS1_6MapRefE, .-_ZN2v88internal8compiler23CompilationDependencies17DependOnStableMapERKNS1_6MapRefE
	.section	.text._ZN2v88internal8compiler23CompilationDependencies18DependOnTransitionERKNS1_6MapRefE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23CompilationDependencies18DependOnTransitionERKNS1_6MapRefE
	.type	_ZN2v88internal8compiler23CompilationDependencies18DependOnTransitionERKNS1_6MapRefE, @function
_ZN2v88internal8compiler23CompilationDependencies18DependOnTransitionERKNS1_6MapRefE:
.LFB18600:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$8, %rsp
	call	_ZNK2v88internal8compiler6MapRef15CanBeDeprecatedEv@PLT
	testb	%al, %al
	je	.L185
	movq	(%rbx), %rdi
	movq	16(%rdi), %r13
	movq	24(%rdi), %rax
	subq	%r13, %rax
	cmpq	$23, %rax
	jbe	.L197
	leaq	24(%r13), %rax
	movq	%rax, 16(%rdi)
.L188:
	movdqu	(%r12), %xmm0
	leaq	16+_ZTVN2v88internal8compiler20TransitionDependencyE(%rip), %rax
	movq	%rax, 0(%r13)
	movups	%xmm0, 8(%r13)
	movq	16(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$15, %rdx
	jbe	.L198
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L189:
	movq	$0, (%rax)
	movq	%r13, 8(%rax)
	movq	24(%rbx), %rdx
	movq	%rdx, (%rax)
	movq	%rax, 24(%rbx)
.L185:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L197:
	.cfi_restore_state
	movl	$24, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r13
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L198:
	movl	$16, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L189
	.cfi_endproc
.LFE18600:
	.size	_ZN2v88internal8compiler23CompilationDependencies18DependOnTransitionERKNS1_6MapRefE, .-_ZN2v88internal8compiler23CompilationDependencies18DependOnTransitionERKNS1_6MapRefE
	.section	.text._ZN2v88internal8compiler23CompilationDependencies21DependOnPretenureModeERKNS1_17AllocationSiteRefE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23CompilationDependencies21DependOnPretenureModeERKNS1_17AllocationSiteRefE
	.type	_ZN2v88internal8compiler23CompilationDependencies21DependOnPretenureModeERKNS1_17AllocationSiteRefE, @function
_ZN2v88internal8compiler23CompilationDependencies21DependOnPretenureModeERKNS1_17AllocationSiteRefE:
.LFB18601:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	call	_ZNK2v88internal8compiler17AllocationSiteRef17GetAllocationTypeEv@PLT
	movq	(%r12), %rdi
	movl	%eax, %r13d
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	$31, %rax
	jbe	.L205
	leaq	32(%rbx), %rax
	movq	%rax, 16(%rdi)
.L201:
	movdqu	(%r14), %xmm0
	leaq	16+_ZTVN2v88internal8compiler23PretenureModeDependencyE(%rip), %rax
	movb	%r13b, 24(%rbx)
	movq	%rax, (%rbx)
	movups	%xmm0, 8(%rbx)
	movq	16(%r12), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$15, %rdx
	jbe	.L206
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L203:
	movq	$0, (%rax)
	movq	%rbx, 8(%rax)
	movq	24(%r12), %rdx
	movq	%rdx, (%rax)
	popq	%rbx
	movq	%rax, 24(%r12)
	popq	%r12
	movl	%r13d, %eax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L205:
	.cfi_restore_state
	movl	$32, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rbx
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L206:
	movl	$16, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L203
	.cfi_endproc
.LFE18601:
	.size	_ZN2v88internal8compiler23CompilationDependencies21DependOnPretenureModeERKNS1_17AllocationSiteRefE, .-_ZN2v88internal8compiler23CompilationDependencies21DependOnPretenureModeERKNS1_17AllocationSiteRefE
	.section	.text._ZN2v88internal8compiler23CompilationDependencies22DependOnFieldConstnessERKNS1_6MapRefEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23CompilationDependencies22DependOnFieldConstnessERKNS1_6MapRefEi
	.type	_ZN2v88internal8compiler23CompilationDependencies22DependOnFieldConstnessERKNS1_6MapRefEi, @function
_ZN2v88internal8compiler23CompilationDependencies22DependOnFieldConstnessERKNS1_6MapRefEi:
.LFB18602:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	movl	%edx, %esi
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -48
	movl	%edx, %ebx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal8compiler6MapRef14FindFieldOwnerEi@PLT
	leaq	-64(%rbp), %rdi
	movl	%ebx, %esi
	movq	%rax, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZNK2v88internal8compiler6MapRef18GetPropertyDetailsEi@PLT
	testb	$4, %al
	jne	.L208
.L213:
	xorl	%eax, %eax
.L207:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L230
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L208:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler6MapRef13instance_typeEv@PLT
	subw	$1041, %ax
	cmpw	$20, %ax
	ja	.L212
	movl	$1179649, %edx
	btq	%rax, %rdx
	jnc	.L212
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler6MapRef9is_stableEv@PLT
	testb	%al, %al
	je	.L213
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler6MapRef13CanTransitionEv@PLT
	testb	%al, %al
	je	.L212
	movq	0(%r13), %rdi
	movq	16(%rdi), %r14
	movq	24(%rdi), %rax
	subq	%r14, %rax
	cmpq	$23, %rax
	jbe	.L231
	leaq	24(%r14), %rax
	movq	%rax, 16(%rdi)
.L215:
	movdqu	(%r12), %xmm1
	leaq	16+_ZTVN2v88internal8compiler19StableMapDependencyE(%rip), %rax
	movq	%rax, (%r14)
	movups	%xmm1, 8(%r14)
	movq	16(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$15, %rdx
	jbe	.L232
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L217:
	movq	$0, (%rax)
	movq	%r14, 8(%rax)
	movq	24(%r13), %rdx
	movq	%rdx, (%rax)
	movq	%rax, 24(%r13)
	.p2align 4,,10
	.p2align 3
.L212:
	movq	0(%r13), %rdi
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	subq	%r12, %rax
	cmpq	$31, %rax
	jbe	.L233
	leaq	32(%r12), %rax
	movq	%rax, 16(%rdi)
.L219:
	leaq	16+_ZTVN2v88internal8compiler24FieldConstnessDependencyE(%rip), %rax
	movl	%ebx, 24(%r12)
	movq	%rax, (%r12)
	movdqa	-64(%rbp), %xmm0
	movups	%xmm0, 8(%r12)
	movq	16(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$15, %rdx
	jbe	.L234
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L221:
	movq	$0, (%rax)
	movq	%r12, 8(%rax)
	movq	24(%r13), %rdx
	movq	%rdx, (%rax)
	movq	%rax, 24(%r13)
	movl	$1, %eax
	jmp	.L207
.L231:
	movl	$24, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r14
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L233:
	movl	$32, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L234:
	movl	$16, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L221
.L232:
	movl	$16, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L217
.L230:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18602:
	.size	_ZN2v88internal8compiler23CompilationDependencies22DependOnFieldConstnessERKNS1_6MapRefEi, .-_ZN2v88internal8compiler23CompilationDependencies22DependOnFieldConstnessERKNS1_6MapRefEi
	.section	.text._ZN2v88internal8compiler23CompilationDependencies27DependOnFieldRepresentationERKNS1_6MapRefEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23CompilationDependencies27DependOnFieldRepresentationERKNS1_6MapRefEi
	.type	_ZN2v88internal8compiler23CompilationDependencies27DependOnFieldRepresentationERKNS1_6MapRefEi, @function
_ZN2v88internal8compiler23CompilationDependencies27DependOnFieldRepresentationERKNS1_6MapRefEi:
.LFB18603:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	movq	%rsi, %rdi
	pushq	%r13
	movl	%edx, %esi
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal8compiler6MapRef14FindFieldOwnerEi@PLT
	leaq	-64(%rbp), %rdi
	movl	%r13d, %esi
	movq	%rax, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZNK2v88internal8compiler6MapRef18GetPropertyDetailsEi@PLT
	movq	(%r14), %rdi
	shrl	$6, %eax
	movl	%eax, %r12d
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	andl	$7, %r12d
	subq	%rbx, %rax
	cmpq	$31, %rax
	jbe	.L246
	leaq	32(%rbx), %rax
	movq	%rax, 16(%rdi)
.L237:
	leaq	16+_ZTVN2v88internal8compiler29FieldRepresentationDependencyE(%rip), %rax
	movl	%r13d, 24(%rbx)
	movq	%rax, (%rbx)
	movdqa	-64(%rbp), %xmm0
	movb	%r12b, 28(%rbx)
	movups	%xmm0, 8(%rbx)
	testq	%rbx, %rbx
	je	.L235
	movq	16(%r14), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$15, %rdx
	jbe	.L247
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L240:
	movq	$0, (%rax)
	movq	%rbx, 8(%rax)
	movq	24(%r14), %rdx
	movq	%rdx, (%rax)
	movq	%rax, 24(%r14)
.L235:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L248
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L247:
	.cfi_restore_state
	movl	$16, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L240
	.p2align 4,,10
	.p2align 3
.L246:
	movl	$32, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rbx
	jmp	.L237
.L248:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18603:
	.size	_ZN2v88internal8compiler23CompilationDependencies27DependOnFieldRepresentationERKNS1_6MapRefEi, .-_ZN2v88internal8compiler23CompilationDependencies27DependOnFieldRepresentationERKNS1_6MapRefEi
	.section	.text._ZN2v88internal8compiler23CompilationDependencies17DependOnFieldTypeERKNS1_6MapRefEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23CompilationDependencies17DependOnFieldTypeERKNS1_6MapRefEi
	.type	_ZN2v88internal8compiler23CompilationDependencies17DependOnFieldTypeERKNS1_6MapRefEi, @function
_ZN2v88internal8compiler23CompilationDependencies17DependOnFieldTypeERKNS1_6MapRefEi:
.LFB18604:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rsi, %rdi
	movl	%edx, %esi
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal8compiler6MapRef14FindFieldOwnerEi@PLT
	leaq	-80(%rbp), %rdi
	movl	%r12d, %esi
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZNK2v88internal8compiler6MapRef12GetFieldTypeEi@PLT
	movq	0(%r13), %rdi
	movq	%rax, %r14
	movq	%rdx, %r15
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	$47, %rax
	jbe	.L260
	leaq	48(%rbx), %rax
	movq	%rax, 16(%rdi)
.L251:
	leaq	16+_ZTVN2v88internal8compiler19FieldTypeDependencyE(%rip), %rax
	movl	%r12d, 24(%rbx)
	movq	%rax, (%rbx)
	movdqa	-80(%rbp), %xmm0
	movq	%r14, 32(%rbx)
	movq	%r15, 40(%rbx)
	movups	%xmm0, 8(%rbx)
	testq	%rbx, %rbx
	je	.L249
	movq	16(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$15, %rdx
	jbe	.L261
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L254:
	movq	$0, (%rax)
	movq	%rbx, 8(%rax)
	movq	24(%r13), %rdx
	movq	%rdx, (%rax)
	movq	%rax, 24(%r13)
.L249:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L262
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L261:
	.cfi_restore_state
	movl	$16, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L260:
	movl	$48, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rbx
	jmp	.L251
.L262:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18604:
	.size	_ZN2v88internal8compiler23CompilationDependencies17DependOnFieldTypeERKNS1_6MapRefEi, .-_ZN2v88internal8compiler23CompilationDependencies17DependOnFieldTypeERKNS1_6MapRefEi
	.section	.text._ZN2v88internal8compiler23CompilationDependencies22DependOnGlobalPropertyERKNS1_15PropertyCellRefE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23CompilationDependencies22DependOnGlobalPropertyERKNS1_15PropertyCellRefE
	.type	_ZN2v88internal8compiler23CompilationDependencies22DependOnGlobalPropertyERKNS1_15PropertyCellRefE, @function
_ZN2v88internal8compiler23CompilationDependencies22DependOnGlobalPropertyERKNS1_15PropertyCellRefE:
.LFB18605:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	call	_ZNK2v88internal8compiler15PropertyCellRef16property_detailsEv@PLT
	movq	%r15, %rdi
	shrl	$6, %eax
	movl	%eax, %r14d
	call	_ZNK2v88internal8compiler15PropertyCellRef16property_detailsEv@PLT
	movq	(%r12), %rdi
	andl	$3, %r14d
	shrl	$3, %eax
	movl	%eax, %r13d
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	andl	$1, %r13d
	subq	%rbx, %rax
	cmpq	$31, %rax
	jbe	.L269
	leaq	32(%rbx), %rax
	movq	%rax, 16(%rdi)
.L265:
	movdqu	(%r15), %xmm0
	leaq	16+_ZTVN2v88internal8compiler24GlobalPropertyDependencyE(%rip), %rax
	movl	%r14d, 24(%rbx)
	movq	%rax, (%rbx)
	movb	%r13b, 28(%rbx)
	movups	%xmm0, 8(%rbx)
	movq	16(%r12), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$15, %rdx
	jbe	.L270
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L267:
	movq	$0, (%rax)
	movq	%rbx, 8(%rax)
	movq	24(%r12), %rdx
	movq	%rdx, (%rax)
	movq	%rax, 24(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L269:
	.cfi_restore_state
	movl	$32, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rbx
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L270:
	movl	$16, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L267
	.cfi_endproc
.LFE18605:
	.size	_ZN2v88internal8compiler23CompilationDependencies22DependOnGlobalPropertyERKNS1_15PropertyCellRefE, .-_ZN2v88internal8compiler23CompilationDependencies22DependOnGlobalPropertyERKNS1_15PropertyCellRefE
	.section	.text._ZN2v88internal8compiler23CompilationDependencies17DependOnProtectorERKNS1_15PropertyCellRefE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23CompilationDependencies17DependOnProtectorERKNS1_15PropertyCellRefE
	.type	_ZN2v88internal8compiler23CompilationDependencies17DependOnProtectorERKNS1_15PropertyCellRefE, @function
_ZN2v88internal8compiler23CompilationDependencies17DependOnProtectorERKNS1_15PropertyCellRefE:
.LFB18606:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal8compiler15PropertyCellRef5valueEv@PLT
	leaq	-64(%rbp), %rdi
	movq	%rax, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef5AsSmiEv@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	cmpl	$1, %r8d
	je	.L281
.L271:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L282
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L281:
	.cfi_restore_state
	movq	(%r12), %rdi
	movq	16(%rdi), %r13
	movq	24(%rdi), %rax
	subq	%r13, %rax
	cmpq	$23, %rax
	jbe	.L283
	leaq	24(%r13), %rax
	movq	%rax, 16(%rdi)
.L274:
	movdqu	(%rbx), %xmm0
	leaq	16+_ZTVN2v88internal8compiler19ProtectorDependencyE(%rip), %rax
	movq	%rax, 0(%r13)
	movups	%xmm0, 8(%r13)
	movq	16(%r12), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$15, %rdx
	jbe	.L284
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L276:
	movq	$0, (%rax)
	movq	%r13, 8(%rax)
	movq	24(%r12), %rdx
	movq	%rdx, (%rax)
	movq	%rax, 24(%r12)
	movl	$1, %eax
	jmp	.L271
	.p2align 4,,10
	.p2align 3
.L283:
	movl	$24, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r13
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L284:
	movl	$16, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L276
.L282:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18606:
	.size	_ZN2v88internal8compiler23CompilationDependencies17DependOnProtectorERKNS1_15PropertyCellRefE, .-_ZN2v88internal8compiler23CompilationDependencies17DependOnProtectorERKNS1_15PropertyCellRefE
	.section	.text._ZN2v88internal8compiler23CompilationDependencies37DependOnArrayBufferDetachingProtectorEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23CompilationDependencies37DependOnArrayBufferDetachingProtectorEv
	.type	_ZN2v88internal8compiler23CompilationDependencies37DependOnArrayBufferDetachingProtectorEv, @function
_ZN2v88internal8compiler23CompilationDependencies37DependOnArrayBufferDetachingProtectorEv:
.LFB18607:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	leaq	-64(%rbp), %r12
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	8(%rdi), %rsi
	movq	%r12, %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	leaq	4560(%rax), %rdx
	call	_ZN2v88internal8compiler15PropertyCellRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler15PropertyCellRef5valueEv@PLT
	leaq	-48(%rbp), %rdi
	movq	%rax, -48(%rbp)
	movq	%rdx, -40(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef5AsSmiEv@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	cmpl	$1, %r8d
	je	.L295
.L285:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L296
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L295:
	.cfi_restore_state
	movq	(%rbx), %rdi
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	subq	%r12, %rax
	cmpq	$23, %rax
	jbe	.L297
	leaq	24(%r12), %rax
	movq	%rax, 16(%rdi)
.L288:
	leaq	16+_ZTVN2v88internal8compiler19ProtectorDependencyE(%rip), %rax
	movq	%rax, (%r12)
	movdqa	-64(%rbp), %xmm0
	movups	%xmm0, 8(%r12)
	movq	16(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$15, %rdx
	jbe	.L298
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L290:
	movq	$0, (%rax)
	movq	%r12, 8(%rax)
	movq	24(%rbx), %rdx
	movq	%rdx, (%rax)
	movq	%rax, 24(%rbx)
	movl	$1, %eax
	jmp	.L285
	.p2align 4,,10
	.p2align 3
.L297:
	movl	$24, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L288
	.p2align 4,,10
	.p2align 3
.L298:
	movl	$16, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L290
.L296:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18607:
	.size	_ZN2v88internal8compiler23CompilationDependencies37DependOnArrayBufferDetachingProtectorEv, .-_ZN2v88internal8compiler23CompilationDependencies37DependOnArrayBufferDetachingProtectorEv
	.section	.text._ZN2v88internal8compiler23CompilationDependencies30DependOnArrayIteratorProtectorEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23CompilationDependencies30DependOnArrayIteratorProtectorEv
	.type	_ZN2v88internal8compiler23CompilationDependencies30DependOnArrayIteratorProtectorEv, @function
_ZN2v88internal8compiler23CompilationDependencies30DependOnArrayIteratorProtectorEv:
.LFB18608:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	leaq	-64(%rbp), %r12
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	8(%rdi), %rsi
	movq	%r12, %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	leaq	4552(%rax), %rdx
	call	_ZN2v88internal8compiler15PropertyCellRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler15PropertyCellRef5valueEv@PLT
	leaq	-48(%rbp), %rdi
	movq	%rax, -48(%rbp)
	movq	%rdx, -40(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef5AsSmiEv@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	cmpl	$1, %r8d
	je	.L309
.L299:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L310
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L309:
	.cfi_restore_state
	movq	(%rbx), %rdi
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	subq	%r12, %rax
	cmpq	$23, %rax
	jbe	.L311
	leaq	24(%r12), %rax
	movq	%rax, 16(%rdi)
.L302:
	leaq	16+_ZTVN2v88internal8compiler19ProtectorDependencyE(%rip), %rax
	movq	%rax, (%r12)
	movdqa	-64(%rbp), %xmm0
	movups	%xmm0, 8(%r12)
	movq	16(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$15, %rdx
	jbe	.L312
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L304:
	movq	$0, (%rax)
	movq	%r12, 8(%rax)
	movq	24(%rbx), %rdx
	movq	%rdx, (%rax)
	movq	%rax, 24(%rbx)
	movl	$1, %eax
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L311:
	movl	$24, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L302
	.p2align 4,,10
	.p2align 3
.L312:
	movl	$16, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L304
.L310:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18608:
	.size	_ZN2v88internal8compiler23CompilationDependencies30DependOnArrayIteratorProtectorEv, .-_ZN2v88internal8compiler23CompilationDependencies30DependOnArrayIteratorProtectorEv
	.section	.text._ZN2v88internal8compiler23CompilationDependencies29DependOnArraySpeciesProtectorEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23CompilationDependencies29DependOnArraySpeciesProtectorEv
	.type	_ZN2v88internal8compiler23CompilationDependencies29DependOnArraySpeciesProtectorEv, @function
_ZN2v88internal8compiler23CompilationDependencies29DependOnArraySpeciesProtectorEv:
.LFB18609:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	leaq	-64(%rbp), %r12
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	8(%rdi), %rsi
	movq	%r12, %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	leaq	4520(%rax), %rdx
	call	_ZN2v88internal8compiler15PropertyCellRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler15PropertyCellRef5valueEv@PLT
	leaq	-48(%rbp), %rdi
	movq	%rax, -48(%rbp)
	movq	%rdx, -40(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef5AsSmiEv@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	cmpl	$1, %r8d
	je	.L323
.L313:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L324
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L323:
	.cfi_restore_state
	movq	(%rbx), %rdi
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	subq	%r12, %rax
	cmpq	$23, %rax
	jbe	.L325
	leaq	24(%r12), %rax
	movq	%rax, 16(%rdi)
.L316:
	leaq	16+_ZTVN2v88internal8compiler19ProtectorDependencyE(%rip), %rax
	movq	%rax, (%r12)
	movdqa	-64(%rbp), %xmm0
	movups	%xmm0, 8(%r12)
	movq	16(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$15, %rdx
	jbe	.L326
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L318:
	movq	$0, (%rax)
	movq	%r12, 8(%rax)
	movq	24(%rbx), %rdx
	movq	%rdx, (%rax)
	movq	%rax, 24(%rbx)
	movl	$1, %eax
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L325:
	movl	$24, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L326:
	movl	$16, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L318
.L324:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18609:
	.size	_ZN2v88internal8compiler23CompilationDependencies29DependOnArraySpeciesProtectorEv, .-_ZN2v88internal8compiler23CompilationDependencies29DependOnArraySpeciesProtectorEv
	.section	.text._ZN2v88internal8compiler23CompilationDependencies27DependOnNoElementsProtectorEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23CompilationDependencies27DependOnNoElementsProtectorEv
	.type	_ZN2v88internal8compiler23CompilationDependencies27DependOnNoElementsProtectorEv, @function
_ZN2v88internal8compiler23CompilationDependencies27DependOnNoElementsProtectorEv:
.LFB18610:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	leaq	-64(%rbp), %r12
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	8(%rdi), %rsi
	movq	%r12, %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	leaq	4504(%rax), %rdx
	call	_ZN2v88internal8compiler15PropertyCellRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler15PropertyCellRef5valueEv@PLT
	leaq	-48(%rbp), %rdi
	movq	%rax, -48(%rbp)
	movq	%rdx, -40(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef5AsSmiEv@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	cmpl	$1, %r8d
	je	.L337
.L327:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L338
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L337:
	.cfi_restore_state
	movq	(%rbx), %rdi
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	subq	%r12, %rax
	cmpq	$23, %rax
	jbe	.L339
	leaq	24(%r12), %rax
	movq	%rax, 16(%rdi)
.L330:
	leaq	16+_ZTVN2v88internal8compiler19ProtectorDependencyE(%rip), %rax
	movq	%rax, (%r12)
	movdqa	-64(%rbp), %xmm0
	movups	%xmm0, 8(%r12)
	movq	16(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$15, %rdx
	jbe	.L340
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L332:
	movq	$0, (%rax)
	movq	%r12, 8(%rax)
	movq	24(%rbx), %rdx
	movq	%rdx, (%rax)
	movq	%rax, 24(%rbx)
	movl	$1, %eax
	jmp	.L327
	.p2align 4,,10
	.p2align 3
.L339:
	movl	$24, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L340:
	movl	$16, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L332
.L338:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18610:
	.size	_ZN2v88internal8compiler23CompilationDependencies27DependOnNoElementsProtectorEv, .-_ZN2v88internal8compiler23CompilationDependencies27DependOnNoElementsProtectorEv
	.section	.text._ZN2v88internal8compiler23CompilationDependencies28DependOnPromiseHookProtectorEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23CompilationDependencies28DependOnPromiseHookProtectorEv
	.type	_ZN2v88internal8compiler23CompilationDependencies28DependOnPromiseHookProtectorEv, @function
_ZN2v88internal8compiler23CompilationDependencies28DependOnPromiseHookProtectorEv:
.LFB18611:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	leaq	-64(%rbp), %r12
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	8(%rdi), %rsi
	movq	%r12, %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	leaq	4568(%rax), %rdx
	call	_ZN2v88internal8compiler15PropertyCellRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler15PropertyCellRef5valueEv@PLT
	leaq	-48(%rbp), %rdi
	movq	%rax, -48(%rbp)
	movq	%rdx, -40(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef5AsSmiEv@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	cmpl	$1, %r8d
	je	.L351
.L341:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L352
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L351:
	.cfi_restore_state
	movq	(%rbx), %rdi
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	subq	%r12, %rax
	cmpq	$23, %rax
	jbe	.L353
	leaq	24(%r12), %rax
	movq	%rax, 16(%rdi)
.L344:
	leaq	16+_ZTVN2v88internal8compiler19ProtectorDependencyE(%rip), %rax
	movq	%rax, (%r12)
	movdqa	-64(%rbp), %xmm0
	movups	%xmm0, 8(%r12)
	movq	16(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$15, %rdx
	jbe	.L354
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L346:
	movq	$0, (%rax)
	movq	%r12, 8(%rax)
	movq	24(%rbx), %rdx
	movq	%rdx, (%rax)
	movq	%rax, 24(%rbx)
	movl	$1, %eax
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L353:
	movl	$24, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L344
	.p2align 4,,10
	.p2align 3
.L354:
	movl	$16, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L346
.L352:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18611:
	.size	_ZN2v88internal8compiler23CompilationDependencies28DependOnPromiseHookProtectorEv, .-_ZN2v88internal8compiler23CompilationDependencies28DependOnPromiseHookProtectorEv
	.section	.text._ZN2v88internal8compiler23CompilationDependencies31DependOnPromiseSpeciesProtectorEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23CompilationDependencies31DependOnPromiseSpeciesProtectorEv
	.type	_ZN2v88internal8compiler23CompilationDependencies31DependOnPromiseSpeciesProtectorEv, @function
_ZN2v88internal8compiler23CompilationDependencies31DependOnPromiseSpeciesProtectorEv:
.LFB18612:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	leaq	-64(%rbp), %r12
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	8(%rdi), %rsi
	movq	%r12, %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	leaq	4536(%rax), %rdx
	call	_ZN2v88internal8compiler15PropertyCellRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler15PropertyCellRef5valueEv@PLT
	leaq	-48(%rbp), %rdi
	movq	%rax, -48(%rbp)
	movq	%rdx, -40(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef5AsSmiEv@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	cmpl	$1, %r8d
	je	.L365
.L355:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L366
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L365:
	.cfi_restore_state
	movq	(%rbx), %rdi
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	subq	%r12, %rax
	cmpq	$23, %rax
	jbe	.L367
	leaq	24(%r12), %rax
	movq	%rax, 16(%rdi)
.L358:
	leaq	16+_ZTVN2v88internal8compiler19ProtectorDependencyE(%rip), %rax
	movq	%rax, (%r12)
	movdqa	-64(%rbp), %xmm0
	movups	%xmm0, 8(%r12)
	movq	16(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$15, %rdx
	jbe	.L368
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L360:
	movq	$0, (%rax)
	movq	%r12, 8(%rax)
	movq	24(%rbx), %rdx
	movq	%rdx, (%rax)
	movq	%rax, 24(%rbx)
	movl	$1, %eax
	jmp	.L355
	.p2align 4,,10
	.p2align 3
.L367:
	movl	$24, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L358
	.p2align 4,,10
	.p2align 3
.L368:
	movl	$16, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L360
.L366:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18612:
	.size	_ZN2v88internal8compiler23CompilationDependencies31DependOnPromiseSpeciesProtectorEv, .-_ZN2v88internal8compiler23CompilationDependencies31DependOnPromiseSpeciesProtectorEv
	.section	.text._ZN2v88internal8compiler23CompilationDependencies28DependOnPromiseThenProtectorEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23CompilationDependencies28DependOnPromiseThenProtectorEv
	.type	_ZN2v88internal8compiler23CompilationDependencies28DependOnPromiseThenProtectorEv, @function
_ZN2v88internal8compiler23CompilationDependencies28DependOnPromiseThenProtectorEv:
.LFB18613:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	leaq	-64(%rbp), %r12
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	8(%rdi), %rsi
	movq	%r12, %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	leaq	4592(%rax), %rdx
	call	_ZN2v88internal8compiler15PropertyCellRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler15PropertyCellRef5valueEv@PLT
	leaq	-48(%rbp), %rdi
	movq	%rax, -48(%rbp)
	movq	%rdx, -40(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef5AsSmiEv@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	cmpl	$1, %r8d
	je	.L379
.L369:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L380
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L379:
	.cfi_restore_state
	movq	(%rbx), %rdi
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	subq	%r12, %rax
	cmpq	$23, %rax
	jbe	.L381
	leaq	24(%r12), %rax
	movq	%rax, 16(%rdi)
.L372:
	leaq	16+_ZTVN2v88internal8compiler19ProtectorDependencyE(%rip), %rax
	movq	%rax, (%r12)
	movdqa	-64(%rbp), %xmm0
	movups	%xmm0, 8(%r12)
	movq	16(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$15, %rdx
	jbe	.L382
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L374:
	movq	$0, (%rax)
	movq	%r12, 8(%rax)
	movq	24(%rbx), %rdx
	movq	%rdx, (%rax)
	movq	%rax, 24(%rbx)
	movl	$1, %eax
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L381:
	movl	$24, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L372
	.p2align 4,,10
	.p2align 3
.L382:
	movl	$16, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L374
.L380:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18613:
	.size	_ZN2v88internal8compiler23CompilationDependencies28DependOnPromiseThenProtectorEv, .-_ZN2v88internal8compiler23CompilationDependencies28DependOnPromiseThenProtectorEv
	.section	.rodata._ZN2v88internal8compiler23CompilationDependencies20DependOnElementsKindERKNS1_17AllocationSiteRefE.str1.1,"aMS",@progbits,1
.LC4:
	.string	"storage_.is_populated_"
	.section	.text._ZN2v88internal8compiler23CompilationDependencies20DependOnElementsKindERKNS1_17AllocationSiteRefE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23CompilationDependencies20DependOnElementsKindERKNS1_17AllocationSiteRefE
	.type	_ZN2v88internal8compiler23CompilationDependencies20DependOnElementsKindERKNS1_17AllocationSiteRefE, @function
_ZN2v88internal8compiler23CompilationDependencies20DependOnElementsKindERKNS1_17AllocationSiteRefE:
.LFB18614:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal8compiler17AllocationSiteRef15PointsToLiteralEv@PLT
	testb	%al, %al
	je	.L384
	leaq	-64(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZNK2v88internal8compiler17AllocationSiteRef11boilerplateEv@PLT
	cmpb	$0, -64(%rbp)
	je	.L394
	leaq	-56(%rbp), %rdi
	call	_ZNK2v88internal8compiler11JSObjectRef15GetElementsKindEv@PLT
	movl	%eax, %r12d
	cmpb	$1, %r12b
	ja	.L383
.L397:
	movq	0(%r13), %rdi
	movq	16(%rdi), %r14
	movq	24(%rdi), %rax
	subq	%r14, %rax
	cmpq	$31, %rax
	jbe	.L395
	leaq	32(%r14), %rax
	movq	%rax, 16(%rdi)
.L389:
	movdqu	(%rbx), %xmm0
	leaq	16+_ZTVN2v88internal8compiler22ElementsKindDependencyE(%rip), %rax
	movb	%r12b, 24(%r14)
	movq	%rax, (%r14)
	movups	%xmm0, 8(%r14)
	movq	16(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$15, %rdx
	jbe	.L396
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L391:
	movq	$0, (%rax)
	movq	%r14, 8(%rax)
	movq	24(%r13), %rdx
	movq	%rdx, (%rax)
	movq	%rax, 24(%r13)
	jmp	.L383
	.p2align 4,,10
	.p2align 3
.L384:
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler17AllocationSiteRef15GetElementsKindEv@PLT
	movl	%eax, %r12d
	cmpb	$1, %r12b
	jbe	.L397
.L383:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L398
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L394:
	.cfi_restore_state
	leaq	.LC4(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L396:
	movl	$16, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L391
	.p2align 4,,10
	.p2align 3
.L395:
	movl	$32, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r14
	jmp	.L389
.L398:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18614:
	.size	_ZN2v88internal8compiler23CompilationDependencies20DependOnElementsKindERKNS1_17AllocationSiteRefE, .-_ZN2v88internal8compiler23CompilationDependencies20DependOnElementsKindERKNS1_17AllocationSiteRefE
	.section	.text._ZNK2v88internal8compiler23CompilationDependencies8AreValidEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler23CompilationDependencies8AreValidEv
	.type	_ZNK2v88internal8compiler23CompilationDependencies8AreValidEv, @function
_ZNK2v88internal8compiler23CompilationDependencies8AreValidEv:
.LFB18615:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	24(%rdi), %rbx
	testq	%rbx, %rbx
	jne	.L402
	jmp	.L400
	.p2align 4,,10
	.p2align 3
.L411:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L400
.L402:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	call	*(%rax)
	testb	%al, %al
	jne	.L411
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L400:
	.cfi_restore_state
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18615:
	.size	_ZNK2v88internal8compiler23CompilationDependencies8AreValidEv, .-_ZNK2v88internal8compiler23CompilationDependencies8AreValidEv
	.section	.text._ZN2v88internal8compiler23CompilationDependencies6CommitENS0_6HandleINS0_4CodeEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23CompilationDependencies6CommitENS0_6HandleINS0_4CodeEEE
	.type	_ZN2v88internal8compiler23CompilationDependencies6CommitENS0_6HandleINS0_4CodeEEE, @function
_ZN2v88internal8compiler23CompilationDependencies6CommitENS0_6HandleINS0_4CodeEEE:
.LFB18616:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	24(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.L413
	movq	%rsi, %r13
	jmp	.L418
	.p2align 4,,10
	.p2align 3
.L414:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L450
.L418:
	movq	8(%rbx), %r12
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*(%rax)
	testb	%al, %al
	jne	.L414
	movq	24(%r14), %rax
	testq	%rax, %rax
	je	.L415
	.p2align 4,,10
	.p2align 3
.L416:
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L416
.L415:
	movq	$0, 24(%r14)
	xorl	%eax, %eax
.L412:
	movq	-56(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L451
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L450:
	.cfi_restore_state
	movq	24(%r14), %rbx
	testq	%rbx, %rbx
	je	.L413
	leaq	-80(%rbp), %r15
	jmp	.L421
	.p2align 4,,10
	.p2align 3
.L419:
	movq	(%r12), %rax
	movq	%r15, %rsi
	movq	%r13, -72(%rbp)
	movq	%r12, %rdi
	movl	$0, -80(%rbp)
	call	*16(%rax)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L452
.L421:
	movq	8(%rbx), %r12
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*(%rax)
	testb	%al, %al
	jne	.L419
	movq	24(%r14), %rax
	testq	%rax, %rax
	je	.L415
	.p2align 4,,10
	.p2align 3
.L420:
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L420
	jmp	.L415
	.p2align 4,,10
	.p2align 3
.L452:
	cmpb	$0, _ZN2v88internal33FLAG_stress_gc_during_compilationE(%rip)
	jne	.L425
.L422:
	movq	24(%r14), %rax
	testq	%rax, %rax
	je	.L423
	.p2align 4,,10
	.p2align 3
.L424:
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L424
.L423:
	movq	$0, 24(%r14)
	movl	$1, %eax
	jmp	.L412
.L413:
	cmpb	$0, _ZN2v88internal33FLAG_stress_gc_during_compilationE(%rip)
	je	.L423
	.p2align 4,,10
	.p2align 3
.L425:
	movq	8(%r14), %rax
	movl	$4, %ecx
	movl	$21, %edx
	xorl	%esi, %esi
	movq	(%rax), %rdi
	addq	$37592, %rdi
	call	_ZN2v88internal4Heap24PreciseCollectAllGarbageEiNS0_23GarbageCollectionReasonENS_15GCCallbackFlagsE@PLT
	jmp	.L422
.L451:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18616:
	.size	_ZN2v88internal8compiler23CompilationDependencies6CommitENS0_6HandleINS0_4CodeEEE, .-_ZN2v88internal8compiler23CompilationDependencies6CommitENS0_6HandleINS0_4CodeEEE
	.section	.text._ZN2v88internal8compiler23CompilationDependencies29DependOnStablePrototypeChainsINS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEEEEvRKT_NS0_12WhereToStartENS_4base8OptionalINS1_11JSObjectRefEEE,"axG",@progbits,_ZN2v88internal8compiler23CompilationDependencies29DependOnStablePrototypeChainsINS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEEEEvRKT_NS0_12WhereToStartENS_4base8OptionalINS1_11JSObjectRefEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler23CompilationDependencies29DependOnStablePrototypeChainsINS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEEEEvRKT_NS0_12WhereToStartENS_4base8OptionalINS1_11JSObjectRefEEE
	.type	_ZN2v88internal8compiler23CompilationDependencies29DependOnStablePrototypeChainsINS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEEEEvRKT_NS0_12WhereToStartENS_4base8OptionalINS1_11JSObjectRefEEE, @function
_ZN2v88internal8compiler23CompilationDependencies29DependOnStablePrototypeChainsINS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEEEEvRKT_NS0_12WhereToStartENS_4base8OptionalINS1_11JSObjectRefEEE:
.LFB20593:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rsi), %rbx
	movq	16(%rsi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	%r15, %rbx
	je	.L453
	movq	%rdi, %r12
	movl	%edx, %r14d
	leaq	-112(%rbp), %r13
	jmp	.L465
	.p2align 4,,10
	.p2align 3
.L456:
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler6MapRef14IsPrimitiveMapEv@PLT
	testb	%al, %al
	je	.L462
	movq	8(%r12), %rax
	cmpb	$0, 24(%rax)
	je	.L475
	movdqu	32(%rax), %xmm0
	leaq	-80(%rbp), %rdi
	leaq	-96(%rbp), %rsi
	movq	%r13, %rdx
	movaps	%xmm0, -96(%rbp)
	call	_ZNK2v88internal8compiler16NativeContextRef22GetConstructorFunctionERKNS1_6MapRefE@PLT
	cmpb	$0, -80(%rbp)
	jne	.L476
.L462:
	subq	$8, %rsp
	pushq	32(%rbp)
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	movq	-104(%rbp), %rdx
	pushq	24(%rbp)
	addq	$8, %rbx
	pushq	16(%rbp)
	call	_ZN2v88internal8compiler12_GLOBAL__N_128DependOnStablePrototypeChainEPNS1_23CompilationDependenciesENS1_6MapRefENS_4base8OptionalINS1_11JSObjectRefEEE
	addq	$32, %rsp
	cmpq	%rbx, %r15
	je	.L453
.L465:
	movq	(%rbx), %rdx
	movq	8(%r12), %rsi
	movl	$1, %ecx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	testl	%r14d, %r14d
	jne	.L456
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler6MapRef13CanTransitionEv@PLT
	testb	%al, %al
	je	.L456
	movq	(%r12), %rdi
	movq	16(%rdi), %rdx
	movq	24(%rdi), %rax
	subq	%rdx, %rax
	cmpq	$23, %rax
	jbe	.L477
	leaq	24(%rdx), %rax
	movq	%rax, 16(%rdi)
.L459:
	leaq	16+_ZTVN2v88internal8compiler19StableMapDependencyE(%rip), %rax
	movq	%rax, (%rdx)
	movdqa	-112(%rbp), %xmm1
	movups	%xmm1, 8(%rdx)
	movq	16(%r12), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	$15, %rcx
	jbe	.L478
	leaq	16(%rax), %rcx
	movq	%rcx, 16(%rdi)
.L461:
	movq	$0, (%rax)
	movq	%rdx, 8(%rax)
	movq	24(%r12), %rdx
	movq	%rdx, (%rax)
	movq	%rax, 24(%r12)
	jmp	.L456
	.p2align 4,,10
	.p2align 3
.L476:
	leaq	-72(%rbp), %rdi
	call	_ZNK2v88internal8compiler13JSFunctionRef11initial_mapEv@PLT
	movq	%rax, -112(%rbp)
	movq	%rdx, -104(%rbp)
	jmp	.L462
	.p2align 4,,10
	.p2align 3
.L475:
	leaq	.LC4(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L453:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L479
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L478:
	.cfi_restore_state
	movl	$16, %esi
	movq	%rdx, -120(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-120(%rbp), %rdx
	jmp	.L461
	.p2align 4,,10
	.p2align 3
.L477:
	movl	$24, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L459
.L479:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20593:
	.size	_ZN2v88internal8compiler23CompilationDependencies29DependOnStablePrototypeChainsINS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEEEEvRKT_NS0_12WhereToStartENS_4base8OptionalINS1_11JSObjectRefEEE, .-_ZN2v88internal8compiler23CompilationDependencies29DependOnStablePrototypeChainsINS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEEEEvRKT_NS0_12WhereToStartENS_4base8OptionalINS1_11JSObjectRefEEE
	.section	.rodata._ZN2v88internal8compiler23CompilationDependencies29DependOnStablePrototypeChainsINS0_13ZoneHandleSetINS0_3MapEEEEEvRKT_NS0_12WhereToStartENS_4base8OptionalINS1_11JSObjectRefEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"vector::_M_range_check: __n (which is %zu) >= this->size() (which is %zu)"
	.section	.text._ZN2v88internal8compiler23CompilationDependencies29DependOnStablePrototypeChainsINS0_13ZoneHandleSetINS0_3MapEEEEEvRKT_NS0_12WhereToStartENS_4base8OptionalINS1_11JSObjectRefEEE,"axG",@progbits,_ZN2v88internal8compiler23CompilationDependencies29DependOnStablePrototypeChainsINS0_13ZoneHandleSetINS0_3MapEEEEEvRKT_NS0_12WhereToStartENS_4base8OptionalINS1_11JSObjectRefEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler23CompilationDependencies29DependOnStablePrototypeChainsINS0_13ZoneHandleSetINS0_3MapEEEEEvRKT_NS0_12WhereToStartENS_4base8OptionalINS1_11JSObjectRefEEE
	.type	_ZN2v88internal8compiler23CompilationDependencies29DependOnStablePrototypeChainsINS0_13ZoneHandleSetINS0_3MapEEEEEvRKT_NS0_12WhereToStartENS_4base8OptionalINS1_11JSObjectRefEEE, @function
_ZN2v88internal8compiler23CompilationDependencies29DependOnStablePrototypeChainsINS0_13ZoneHandleSetINS0_3MapEEEEEvRKT_NS0_12WhereToStartENS_4base8OptionalINS1_11JSObjectRefEEE:
.LFB20595:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rsi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdx, %rax
	andl	$3, %eax
	cmpq	$1, %rax
	je	.L480
	movq	%rdi, %rbx
	movq	%rsi, %r13
	testq	%rax, %rax
	je	.L499
	movq	14(%rdx), %r14
	subq	6(%rdx), %r14
	sarq	$3, %r14
	movq	%r14, -120(%rbp)
	jne	.L482
.L480:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L510
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L499:
	.cfi_restore_state
	movq	$1, -120(%rbp)
.L482:
	xorl	%r12d, %r12d
	testb	$3, %dl
	je	.L485
.L515:
	movq	6(%rdx), %rax
	subq	$2, %rdx
	movslq	%r12d, %rsi
	movq	16(%rdx), %rdx
	subq	%rax, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L511
	movq	(%rax,%rsi,8), %rdx
.L485:
	leaq	-112(%rbp), %r14
	movq	8(%rbx), %rsi
	movl	$1, %ecx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	testl	%r15d, %r15d
	je	.L512
.L488:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler6MapRef14IsPrimitiveMapEv@PLT
	testb	%al, %al
	je	.L494
	movq	8(%rbx), %rax
	cmpb	$0, 24(%rax)
	je	.L513
	movdqu	32(%rax), %xmm0
	leaq	-80(%rbp), %rdi
	leaq	-96(%rbp), %rsi
	movq	%r14, %rdx
	movaps	%xmm0, -96(%rbp)
	call	_ZNK2v88internal8compiler16NativeContextRef22GetConstructorFunctionERKNS1_6MapRefE@PLT
	cmpb	$0, -80(%rbp)
	jne	.L514
.L494:
	subq	$8, %rsp
	pushq	32(%rbp)
	movq	-112(%rbp), %rsi
	movq	%rbx, %rdi
	movq	-104(%rbp), %rdx
	pushq	24(%rbp)
	addq	$1, %r12
	pushq	16(%rbp)
	call	_ZN2v88internal8compiler12_GLOBAL__N_128DependOnStablePrototypeChainEPNS1_23CompilationDependenciesENS1_6MapRefENS_4base8OptionalINS1_11JSObjectRefEEE
	addq	$32, %rsp
	cmpq	-120(%rbp), %r12
	je	.L480
	movq	0(%r13), %rdx
	testb	$3, %dl
	je	.L485
	jmp	.L515
	.p2align 4,,10
	.p2align 3
.L512:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler6MapRef13CanTransitionEv@PLT
	testb	%al, %al
	je	.L488
	movq	(%rbx), %rdi
	movq	16(%rdi), %rdx
	movq	24(%rdi), %rax
	subq	%rdx, %rax
	cmpq	$23, %rax
	jbe	.L516
	leaq	24(%rdx), %rax
	movq	%rax, 16(%rdi)
.L491:
	leaq	16+_ZTVN2v88internal8compiler19StableMapDependencyE(%rip), %rax
	movq	%rax, (%rdx)
	movdqa	-112(%rbp), %xmm1
	movups	%xmm1, 8(%rdx)
	movq	16(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	$15, %rcx
	jbe	.L517
	leaq	16(%rax), %rcx
	movq	%rcx, 16(%rdi)
.L493:
	movq	$0, (%rax)
	movq	%rdx, 8(%rax)
	movq	24(%rbx), %rdx
	movq	%rdx, (%rax)
	movq	%rax, 24(%rbx)
	jmp	.L488
	.p2align 4,,10
	.p2align 3
.L514:
	leaq	-72(%rbp), %rdi
	call	_ZNK2v88internal8compiler13JSFunctionRef11initial_mapEv@PLT
	movq	%rax, -112(%rbp)
	movq	%rdx, -104(%rbp)
	jmp	.L494
	.p2align 4,,10
	.p2align 3
.L513:
	leaq	.LC4(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L516:
	movl	$24, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L491
.L517:
	movl	$16, %esi
	movq	%rdx, -128(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-128(%rbp), %rdx
	jmp	.L493
.L510:
	call	__stack_chk_fail@PLT
.L511:
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE20595:
	.size	_ZN2v88internal8compiler23CompilationDependencies29DependOnStablePrototypeChainsINS0_13ZoneHandleSetINS0_3MapEEEEEvRKT_NS0_12WhereToStartENS_4base8OptionalINS1_11JSObjectRefEEE, .-_ZN2v88internal8compiler23CompilationDependencies29DependOnStablePrototypeChainsINS0_13ZoneHandleSetINS0_3MapEEEEEvRKT_NS0_12WhereToStartENS_4base8OptionalINS1_11JSObjectRefEEE
	.section	.rodata._ZN2v88internal8compiler23CompilationDependencies21DependOnElementsKindsERKNS1_17AllocationSiteRefE.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"current.nested_site().AsSmi() == 0"
	.section	.text._ZN2v88internal8compiler23CompilationDependencies21DependOnElementsKindsERKNS1_17AllocationSiteRefE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23CompilationDependencies21DependOnElementsKindsERKNS1_17AllocationSiteRefE
	.type	_ZN2v88internal8compiler23CompilationDependencies21DependOnElementsKindsERKNS1_17AllocationSiteRefE, @function
_ZN2v88internal8compiler23CompilationDependencies21DependOnElementsKindsERKNS1_17AllocationSiteRefE:
.LFB18620:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	leaq	-80(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$56, %rsp
	movdqu	(%rsi), %xmm0
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	jmp	.L520
	.p2align 4,,10
	.p2align 3
.L524:
	call	_ZNK2v88internal8compiler17AllocationSiteRef11nested_siteEv@PLT
	movq	%r13, %rdi
	movq	%rax, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef16AsAllocationSiteEv@PLT
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
.L520:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler23CompilationDependencies20DependOnElementsKindERKNS1_17AllocationSiteRefE
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler17AllocationSiteRef11nested_siteEv@PLT
	movq	%r13, %rdi
	movq	%rax, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef16IsAllocationSiteEv@PLT
	movq	%r12, %rdi
	testb	%al, %al
	jne	.L524
	call	_ZNK2v88internal8compiler17AllocationSiteRef11nested_siteEv@PLT
	movq	%r13, %rdi
	movq	%rax, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef5AsSmiEv@PLT
	testl	%eax, %eax
	jne	.L525
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L526
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L525:
	.cfi_restore_state
	leaq	.LC6(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L526:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18620:
	.size	_ZN2v88internal8compiler23CompilationDependencies21DependOnElementsKindsERKNS1_17AllocationSiteRefE, .-_ZN2v88internal8compiler23CompilationDependencies21DependOnElementsKindsERKNS1_17AllocationSiteRefE
	.section	.text._ZN2v88internal8compiler23SlackTrackingPredictionC2ENS1_6MapRefEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23SlackTrackingPredictionC2ENS1_6MapRefEi
	.type	_ZN2v88internal8compiler23SlackTrackingPredictionC2ENS1_6MapRefEi, @function
_ZN2v88internal8compiler23SlackTrackingPredictionC2ENS1_6MapRefEi:
.LFB18622:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%ecx, %ebx
	sarl	$3, %ebx
	subq	$16, %rsp
	movl	%ecx, (%rdi)
	leaq	-32(%rbp), %rdi
	movq	%rsi, -32(%rbp)
	movq	%rdx, -24(%rbp)
	call	_ZNK2v88internal8compiler6MapRef33GetInObjectPropertiesStartInWordsEv@PLT
	subl	%eax, %ebx
	movl	%ebx, 4(%r12)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18622:
	.size	_ZN2v88internal8compiler23SlackTrackingPredictionC2ENS1_6MapRefEi, .-_ZN2v88internal8compiler23SlackTrackingPredictionC2ENS1_6MapRefEi
	.globl	_ZN2v88internal8compiler23SlackTrackingPredictionC1ENS1_6MapRefEi
	.set	_ZN2v88internal8compiler23SlackTrackingPredictionC1ENS1_6MapRefEi,_ZN2v88internal8compiler23SlackTrackingPredictionC2ENS1_6MapRefEi
	.section	.text._ZN2v88internal8compiler23CompilationDependencies40DependOnInitialMapInstanceSizePredictionERKNS1_13JSFunctionRefE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23CompilationDependencies40DependOnInitialMapInstanceSizePredictionERKNS1_13JSFunctionRefE
	.type	_ZN2v88internal8compiler23CompilationDependencies40DependOnInitialMapInstanceSizePredictionERKNS1_13JSFunctionRefE, @function
_ZN2v88internal8compiler23CompilationDependencies40DependOnInitialMapInstanceSizePredictionERKNS1_13JSFunctionRefE:
.LFB18624:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal8compiler13JSFunctionRef11initial_mapEv@PLT
	movq	(%rbx), %rdi
	movq	%rax, -88(%rbp)
	movq	%rdx, %r13
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	subq	%r12, %rax
	cmpq	$39, %rax
	jbe	.L540
	leaq	40(%r12), %rax
	movq	%rax, 16(%rdi)
.L531:
	leaq	16+_ZTVN2v88internal8compiler20InitialMapDependencyE(%rip), %rax
	movdqu	(%r15), %xmm1
	movq	%r13, 32(%r12)
	movq	%rax, (%r12)
	movq	-88(%rbp), %rax
	movups	%xmm1, 8(%r12)
	movq	%rax, 24(%r12)
	movq	16(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$15, %rdx
	jbe	.L541
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L533:
	movq	$0, (%rax)
	movq	%r15, %rdi
	movq	%r12, 8(%rax)
	movq	24(%rbx), %rdx
	movq	%rdx, (%rax)
	movq	%rax, 24(%rbx)
	call	_ZNK2v88internal8compiler13JSFunctionRef34InitialMapInstanceSizeWithMinSlackEv@PLT
	movq	(%rbx), %rdi
	movl	%eax, %r12d
	movq	16(%rdi), %r14
	movq	24(%rdi), %rax
	subq	%r14, %rax
	cmpq	$31, %rax
	jbe	.L542
	leaq	32(%r14), %rax
	movq	%rax, 16(%rdi)
.L535:
	movdqu	(%r15), %xmm2
	leaq	16+_ZTVN2v88internal8compiler42InitialMapInstanceSizePredictionDependencyE(%rip), %rax
	movl	%r12d, 24(%r14)
	movq	%rax, (%r14)
	movups	%xmm2, 8(%r14)
	movq	16(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$15, %rdx
	jbe	.L543
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L537:
	movq	$0, (%rax)
	movq	-88(%rbp), %xmm0
	movq	%r13, %xmm3
	leaq	-80(%rbp), %rdi
	movq	%r14, 8(%rax)
	movq	24(%rbx), %rdx
	punpcklqdq	%xmm3, %xmm0
	movq	%rdx, (%rax)
	movq	%rax, 24(%rbx)
	movaps	%xmm0, -80(%rbp)
	call	_ZNK2v88internal8compiler6MapRef33GetInObjectPropertiesStartInWordsEv@PLT
	movl	%eax, %r8d
	movl	%r12d, %eax
	sarl	$3, %eax
	subl	%r8d, %eax
	salq	$32, %rax
	movq	%rax, %rdx
	movl	%r12d, %eax
	orq	%rdx, %rax
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L544
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L540:
	.cfi_restore_state
	movl	$40, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L531
	.p2align 4,,10
	.p2align 3
.L541:
	movl	$16, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L533
	.p2align 4,,10
	.p2align 3
.L542:
	movl	$32, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r14
	jmp	.L535
	.p2align 4,,10
	.p2align 3
.L543:
	movl	$16, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L537
.L544:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18624:
	.size	_ZN2v88internal8compiler23CompilationDependencies40DependOnInitialMapInstanceSizePredictionERKNS1_13JSFunctionRefE, .-_ZN2v88internal8compiler23CompilationDependencies40DependOnInitialMapInstanceSizePredictionERKNS1_13JSFunctionRefE
	.section	.text._ZNK2v88internal8compiler23CompilationDependencies32TransitionDependencyOffTheRecordERKNS1_6MapRefE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler23CompilationDependencies32TransitionDependencyOffTheRecordERKNS1_6MapRefE
	.type	_ZNK2v88internal8compiler23CompilationDependencies32TransitionDependencyOffTheRecordERKNS1_6MapRefE, @function
_ZNK2v88internal8compiler23CompilationDependencies32TransitionDependencyOffTheRecordERKNS1_6MapRefE:
.LFB18625:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_ZNK2v88internal8compiler6MapRef15CanBeDeprecatedEv@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	testb	%r8b, %r8b
	je	.L545
	movq	(%r12), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$23, %rdx
	jbe	.L553
	leaq	24(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L548:
	leaq	16+_ZTVN2v88internal8compiler20TransitionDependencyE(%rip), %rcx
	movdqu	(%rbx), %xmm0
	movq	%rcx, (%rax)
	movups	%xmm0, 8(%rax)
.L545:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L553:
	.cfi_restore_state
	movl	$24, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L548
	.cfi_endproc
.LFE18625:
	.size	_ZNK2v88internal8compiler23CompilationDependencies32TransitionDependencyOffTheRecordERKNS1_6MapRefE, .-_ZNK2v88internal8compiler23CompilationDependencies32TransitionDependencyOffTheRecordERKNS1_6MapRefE
	.section	.text._ZNK2v88internal8compiler23CompilationDependencies41FieldRepresentationDependencyOffTheRecordERKNS1_6MapRefEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler23CompilationDependencies41FieldRepresentationDependencyOffTheRecordERKNS1_6MapRefEi
	.type	_ZNK2v88internal8compiler23CompilationDependencies41FieldRepresentationDependencyOffTheRecordERKNS1_6MapRefEi, @function
_ZNK2v88internal8compiler23CompilationDependencies41FieldRepresentationDependencyOffTheRecordERKNS1_6MapRefEi:
.LFB18626:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	movl	%edx, %esi
	.cfi_offset 12, -32
	movl	%edx, %r12d
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal8compiler6MapRef14FindFieldOwnerEi@PLT
	leaq	-64(%rbp), %rdi
	movl	%r12d, %esi
	movq	%rdx, -56(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal8compiler6MapRef18GetPropertyDetailsEi@PLT
	movq	0(%r13), %rdi
	shrl	$6, %eax
	movl	%eax, %ebx
	movq	24(%rdi), %rdx
	movq	16(%rdi), %rax
	andl	$7, %ebx
	subq	%rax, %rdx
	cmpq	$31, %rdx
	jbe	.L559
	leaq	32(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L556:
	leaq	16+_ZTVN2v88internal8compiler29FieldRepresentationDependencyE(%rip), %rcx
	movl	%r12d, 24(%rax)
	movq	%rcx, (%rax)
	movdqa	-64(%rbp), %xmm0
	movb	%bl, 28(%rax)
	movups	%xmm0, 8(%rax)
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L560
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L559:
	.cfi_restore_state
	movl	$32, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L556
.L560:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18626:
	.size	_ZNK2v88internal8compiler23CompilationDependencies41FieldRepresentationDependencyOffTheRecordERKNS1_6MapRefEi, .-_ZNK2v88internal8compiler23CompilationDependencies41FieldRepresentationDependencyOffTheRecordERKNS1_6MapRefEi
	.section	.text._ZNK2v88internal8compiler23CompilationDependencies31FieldTypeDependencyOffTheRecordERKNS1_6MapRefEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler23CompilationDependencies31FieldTypeDependencyOffTheRecordERKNS1_6MapRefEi
	.type	_ZNK2v88internal8compiler23CompilationDependencies31FieldTypeDependencyOffTheRecordERKNS1_6MapRefEi, @function
_ZNK2v88internal8compiler23CompilationDependencies31FieldTypeDependencyOffTheRecordERKNS1_6MapRefEi:
.LFB18627:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	movq	%rsi, %rdi
	pushq	%r13
	movl	%edx, %esi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	%edx, %ebx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal8compiler6MapRef14FindFieldOwnerEi@PLT
	leaq	-64(%rbp), %rdi
	movl	%ebx, %esi
	movq	%rax, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZNK2v88internal8compiler6MapRef12GetFieldTypeEi@PLT
	movq	(%r14), %rdi
	movq	%rax, %r12
	movq	%rdx, %r13
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$47, %rdx
	jbe	.L566
	leaq	48(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L563:
	leaq	16+_ZTVN2v88internal8compiler19FieldTypeDependencyE(%rip), %rcx
	movl	%ebx, 24(%rax)
	movq	%rcx, (%rax)
	movdqa	-64(%rbp), %xmm0
	movq	%r12, 32(%rax)
	movq	%r13, 40(%rax)
	movups	%xmm0, 8(%rax)
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L567
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L566:
	.cfi_restore_state
	movl	$48, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L563
.L567:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18627:
	.size	_ZNK2v88internal8compiler23CompilationDependencies31FieldTypeDependencyOffTheRecordERKNS1_6MapRefEi, .-_ZNK2v88internal8compiler23CompilationDependencies31FieldTypeDependencyOffTheRecordERKNS1_6MapRefEi
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler23CompilationDependenciesC2EPNS1_12JSHeapBrokerEPNS0_4ZoneE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler23CompilationDependenciesC2EPNS1_12JSHeapBrokerEPNS0_4ZoneE, @function
_GLOBAL__sub_I__ZN2v88internal8compiler23CompilationDependenciesC2EPNS1_12JSHeapBrokerEPNS0_4ZoneE:
.LFB22637:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE22637:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler23CompilationDependenciesC2EPNS1_12JSHeapBrokerEPNS0_4ZoneE, .-_GLOBAL__sub_I__ZN2v88internal8compiler23CompilationDependenciesC2EPNS1_12JSHeapBrokerEPNS0_4ZoneE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler23CompilationDependenciesC2EPNS1_12JSHeapBrokerEPNS0_4ZoneE
	.weak	_ZTVN2v88internal8compiler20InitialMapDependencyE
	.section	.data.rel.ro.local._ZTVN2v88internal8compiler20InitialMapDependencyE,"awG",@progbits,_ZTVN2v88internal8compiler20InitialMapDependencyE,comdat
	.align 8
	.type	_ZTVN2v88internal8compiler20InitialMapDependencyE, @object
	.size	_ZTVN2v88internal8compiler20InitialMapDependencyE, 40
_ZTVN2v88internal8compiler20InitialMapDependencyE:
	.quad	0
	.quad	0
	.quad	_ZNK2v88internal8compiler20InitialMapDependency7IsValidEv
	.quad	_ZNK2v88internal8compiler21CompilationDependency14PrepareInstallEv
	.quad	_ZNK2v88internal8compiler20InitialMapDependency7InstallERKNS0_17MaybeObjectHandleE
	.weak	_ZTVN2v88internal8compiler27PrototypePropertyDependencyE
	.section	.data.rel.ro.local._ZTVN2v88internal8compiler27PrototypePropertyDependencyE,"awG",@progbits,_ZTVN2v88internal8compiler27PrototypePropertyDependencyE,comdat
	.align 8
	.type	_ZTVN2v88internal8compiler27PrototypePropertyDependencyE, @object
	.size	_ZTVN2v88internal8compiler27PrototypePropertyDependencyE, 40
_ZTVN2v88internal8compiler27PrototypePropertyDependencyE:
	.quad	0
	.quad	0
	.quad	_ZNK2v88internal8compiler27PrototypePropertyDependency7IsValidEv
	.quad	_ZNK2v88internal8compiler27PrototypePropertyDependency14PrepareInstallEv
	.quad	_ZNK2v88internal8compiler27PrototypePropertyDependency7InstallERKNS0_17MaybeObjectHandleE
	.weak	_ZTVN2v88internal8compiler19StableMapDependencyE
	.section	.data.rel.ro.local._ZTVN2v88internal8compiler19StableMapDependencyE,"awG",@progbits,_ZTVN2v88internal8compiler19StableMapDependencyE,comdat
	.align 8
	.type	_ZTVN2v88internal8compiler19StableMapDependencyE, @object
	.size	_ZTVN2v88internal8compiler19StableMapDependencyE, 40
_ZTVN2v88internal8compiler19StableMapDependencyE:
	.quad	0
	.quad	0
	.quad	_ZNK2v88internal8compiler19StableMapDependency7IsValidEv
	.quad	_ZNK2v88internal8compiler21CompilationDependency14PrepareInstallEv
	.quad	_ZNK2v88internal8compiler19StableMapDependency7InstallERKNS0_17MaybeObjectHandleE
	.weak	_ZTVN2v88internal8compiler20TransitionDependencyE
	.section	.data.rel.ro.local._ZTVN2v88internal8compiler20TransitionDependencyE,"awG",@progbits,_ZTVN2v88internal8compiler20TransitionDependencyE,comdat
	.align 8
	.type	_ZTVN2v88internal8compiler20TransitionDependencyE, @object
	.size	_ZTVN2v88internal8compiler20TransitionDependencyE, 40
_ZTVN2v88internal8compiler20TransitionDependencyE:
	.quad	0
	.quad	0
	.quad	_ZNK2v88internal8compiler20TransitionDependency7IsValidEv
	.quad	_ZNK2v88internal8compiler21CompilationDependency14PrepareInstallEv
	.quad	_ZNK2v88internal8compiler20TransitionDependency7InstallERKNS0_17MaybeObjectHandleE
	.weak	_ZTVN2v88internal8compiler23PretenureModeDependencyE
	.section	.data.rel.ro.local._ZTVN2v88internal8compiler23PretenureModeDependencyE,"awG",@progbits,_ZTVN2v88internal8compiler23PretenureModeDependencyE,comdat
	.align 8
	.type	_ZTVN2v88internal8compiler23PretenureModeDependencyE, @object
	.size	_ZTVN2v88internal8compiler23PretenureModeDependencyE, 40
_ZTVN2v88internal8compiler23PretenureModeDependencyE:
	.quad	0
	.quad	0
	.quad	_ZNK2v88internal8compiler23PretenureModeDependency7IsValidEv
	.quad	_ZNK2v88internal8compiler21CompilationDependency14PrepareInstallEv
	.quad	_ZNK2v88internal8compiler23PretenureModeDependency7InstallERKNS0_17MaybeObjectHandleE
	.weak	_ZTVN2v88internal8compiler29FieldRepresentationDependencyE
	.section	.data.rel.ro.local._ZTVN2v88internal8compiler29FieldRepresentationDependencyE,"awG",@progbits,_ZTVN2v88internal8compiler29FieldRepresentationDependencyE,comdat
	.align 8
	.type	_ZTVN2v88internal8compiler29FieldRepresentationDependencyE, @object
	.size	_ZTVN2v88internal8compiler29FieldRepresentationDependencyE, 40
_ZTVN2v88internal8compiler29FieldRepresentationDependencyE:
	.quad	0
	.quad	0
	.quad	_ZNK2v88internal8compiler29FieldRepresentationDependency7IsValidEv
	.quad	_ZNK2v88internal8compiler21CompilationDependency14PrepareInstallEv
	.quad	_ZNK2v88internal8compiler29FieldRepresentationDependency7InstallERKNS0_17MaybeObjectHandleE
	.weak	_ZTVN2v88internal8compiler19FieldTypeDependencyE
	.section	.data.rel.ro.local._ZTVN2v88internal8compiler19FieldTypeDependencyE,"awG",@progbits,_ZTVN2v88internal8compiler19FieldTypeDependencyE,comdat
	.align 8
	.type	_ZTVN2v88internal8compiler19FieldTypeDependencyE, @object
	.size	_ZTVN2v88internal8compiler19FieldTypeDependencyE, 40
_ZTVN2v88internal8compiler19FieldTypeDependencyE:
	.quad	0
	.quad	0
	.quad	_ZNK2v88internal8compiler19FieldTypeDependency7IsValidEv
	.quad	_ZNK2v88internal8compiler21CompilationDependency14PrepareInstallEv
	.quad	_ZNK2v88internal8compiler19FieldTypeDependency7InstallERKNS0_17MaybeObjectHandleE
	.weak	_ZTVN2v88internal8compiler24FieldConstnessDependencyE
	.section	.data.rel.ro.local._ZTVN2v88internal8compiler24FieldConstnessDependencyE,"awG",@progbits,_ZTVN2v88internal8compiler24FieldConstnessDependencyE,comdat
	.align 8
	.type	_ZTVN2v88internal8compiler24FieldConstnessDependencyE, @object
	.size	_ZTVN2v88internal8compiler24FieldConstnessDependencyE, 40
_ZTVN2v88internal8compiler24FieldConstnessDependencyE:
	.quad	0
	.quad	0
	.quad	_ZNK2v88internal8compiler24FieldConstnessDependency7IsValidEv
	.quad	_ZNK2v88internal8compiler21CompilationDependency14PrepareInstallEv
	.quad	_ZNK2v88internal8compiler24FieldConstnessDependency7InstallERKNS0_17MaybeObjectHandleE
	.weak	_ZTVN2v88internal8compiler24GlobalPropertyDependencyE
	.section	.data.rel.ro.local._ZTVN2v88internal8compiler24GlobalPropertyDependencyE,"awG",@progbits,_ZTVN2v88internal8compiler24GlobalPropertyDependencyE,comdat
	.align 8
	.type	_ZTVN2v88internal8compiler24GlobalPropertyDependencyE, @object
	.size	_ZTVN2v88internal8compiler24GlobalPropertyDependencyE, 40
_ZTVN2v88internal8compiler24GlobalPropertyDependencyE:
	.quad	0
	.quad	0
	.quad	_ZNK2v88internal8compiler24GlobalPropertyDependency7IsValidEv
	.quad	_ZNK2v88internal8compiler21CompilationDependency14PrepareInstallEv
	.quad	_ZNK2v88internal8compiler24GlobalPropertyDependency7InstallERKNS0_17MaybeObjectHandleE
	.weak	_ZTVN2v88internal8compiler19ProtectorDependencyE
	.section	.data.rel.ro.local._ZTVN2v88internal8compiler19ProtectorDependencyE,"awG",@progbits,_ZTVN2v88internal8compiler19ProtectorDependencyE,comdat
	.align 8
	.type	_ZTVN2v88internal8compiler19ProtectorDependencyE, @object
	.size	_ZTVN2v88internal8compiler19ProtectorDependencyE, 40
_ZTVN2v88internal8compiler19ProtectorDependencyE:
	.quad	0
	.quad	0
	.quad	_ZNK2v88internal8compiler19ProtectorDependency7IsValidEv
	.quad	_ZNK2v88internal8compiler21CompilationDependency14PrepareInstallEv
	.quad	_ZNK2v88internal8compiler19ProtectorDependency7InstallERKNS0_17MaybeObjectHandleE
	.weak	_ZTVN2v88internal8compiler22ElementsKindDependencyE
	.section	.data.rel.ro.local._ZTVN2v88internal8compiler22ElementsKindDependencyE,"awG",@progbits,_ZTVN2v88internal8compiler22ElementsKindDependencyE,comdat
	.align 8
	.type	_ZTVN2v88internal8compiler22ElementsKindDependencyE, @object
	.size	_ZTVN2v88internal8compiler22ElementsKindDependencyE, 40
_ZTVN2v88internal8compiler22ElementsKindDependencyE:
	.quad	0
	.quad	0
	.quad	_ZNK2v88internal8compiler22ElementsKindDependency7IsValidEv
	.quad	_ZNK2v88internal8compiler21CompilationDependency14PrepareInstallEv
	.quad	_ZNK2v88internal8compiler22ElementsKindDependency7InstallERKNS0_17MaybeObjectHandleE
	.weak	_ZTVN2v88internal8compiler42InitialMapInstanceSizePredictionDependencyE
	.section	.data.rel.ro.local._ZTVN2v88internal8compiler42InitialMapInstanceSizePredictionDependencyE,"awG",@progbits,_ZTVN2v88internal8compiler42InitialMapInstanceSizePredictionDependencyE,comdat
	.align 8
	.type	_ZTVN2v88internal8compiler42InitialMapInstanceSizePredictionDependencyE, @object
	.size	_ZTVN2v88internal8compiler42InitialMapInstanceSizePredictionDependencyE, 40
_ZTVN2v88internal8compiler42InitialMapInstanceSizePredictionDependencyE:
	.quad	0
	.quad	0
	.quad	_ZNK2v88internal8compiler42InitialMapInstanceSizePredictionDependency7IsValidEv
	.quad	_ZNK2v88internal8compiler42InitialMapInstanceSizePredictionDependency14PrepareInstallEv
	.quad	_ZNK2v88internal8compiler42InitialMapInstanceSizePredictionDependency7InstallERKNS0_17MaybeObjectHandleE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
