	.file	"representation-change.cc"
	.text
	.section	.rodata._ZNK2v88internal8compiler10Truncation11descriptionEv.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"truncate-oddball&bigint-to-number (identify zeros)"
	.section	.rodata._ZNK2v88internal8compiler10Truncation11descriptionEv.str1.1,"aMS",@progbits,1
.LC1:
	.string	"truncate-to-bool"
	.section	.rodata._ZNK2v88internal8compiler10Truncation11descriptionEv.str1.8
	.align 8
.LC2:
	.string	"no-truncation (but identify zeros)"
	.section	.rodata._ZNK2v88internal8compiler10Truncation11descriptionEv.str1.1
.LC3:
	.string	"truncate-to-word64"
.LC4:
	.string	"no-value-use"
	.section	.rodata._ZNK2v88internal8compiler10Truncation11descriptionEv.str1.8
	.align 8
.LC5:
	.string	"truncate-oddball&bigint-to-number (distinguish zeros)"
	.section	.rodata._ZNK2v88internal8compiler10Truncation11descriptionEv.str1.1
.LC6:
	.string	"truncate-to-word32"
	.section	.rodata._ZNK2v88internal8compiler10Truncation11descriptionEv.str1.8
	.align 8
.LC7:
	.string	"no-truncation (but distinguish zeros)"
	.section	.rodata._ZNK2v88internal8compiler10Truncation11descriptionEv.str1.1
.LC8:
	.string	"unreachable code"
	.section	.text._ZNK2v88internal8compiler10Truncation11descriptionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler10Truncation11descriptionEv
	.type	_ZNK2v88internal8compiler10Truncation11descriptionEv, @function
_ZNK2v88internal8compiler10Truncation11descriptionEv:
.LFB22737:
	.cfi_startproc
	endbr64
	cmpb	$5, (%rdi)
	ja	.L2
	movzbl	(%rdi), %eax
	leaq	.L4(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZNK2v88internal8compiler10Truncation11descriptionEv,"a",@progbits
	.align 4
	.align 4
.L4:
	.long	.L9-.L4
	.long	.L8-.L4
	.long	.L10-.L4
	.long	.L6-.L4
	.long	.L5-.L4
	.long	.L3-.L4
	.section	.text._ZNK2v88internal8compiler10Truncation11descriptionEv
	.p2align 4,,10
	.p2align 3
.L3:
	movl	4(%rdi), %edx
	leaq	.LC2(%rip), %rax
	testl	%edx, %edx
	jne	.L19
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	cmpl	$1, %edx
	jne	.L2
	leaq	.LC7(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	leaq	.LC6(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	leaq	.LC1(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movl	4(%rdi), %eax
	testl	%eax, %eax
	je	.L11
	cmpl	$1, %eax
	je	.L12
.L2:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC8(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	leaq	.LC4(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	leaq	.LC3(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	leaq	.LC5(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE22737:
	.size	_ZNK2v88internal8compiler10Truncation11descriptionEv, .-_ZNK2v88internal8compiler10Truncation11descriptionEv
	.section	.text._ZN2v88internal8compiler10Truncation10GeneralizeENS2_14TruncationKindES3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler10Truncation10GeneralizeENS2_14TruncationKindES3_
	.type	_ZN2v88internal8compiler10Truncation10GeneralizeENS2_14TruncationKindES3_, @function
_ZN2v88internal8compiler10Truncation10GeneralizeENS2_14TruncationKindES3_:
.LFB22738:
	.cfi_startproc
	endbr64
	cmpb	$5, %dil
	ja	.L21
	leaq	.L23(%rip), %r9
	movzbl	%dil, %r8d
	movl	%edi, %eax
	movzbl	%sil, %ecx
	movslq	(%r9,%r8,4), %rdx
	addq	%r9, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal8compiler10Truncation10GeneralizeENS2_14TruncationKindES3_,"a",@progbits
	.align 4
	.align 4
.L23:
	.long	.L42-.L23
	.long	.L27-.L23
	.long	.L26-.L23
	.long	.L25-.L23
	.long	.L24-.L23
	.long	.L22-.L23
	.section	.text._ZN2v88internal8compiler10Truncation10GeneralizeENS2_14TruncationKindES3_
	.p2align 4,,10
	.p2align 3
.L42:
	movl	%esi, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	cmpb	$5, %sil
	sete	%dl
.L29:
	testb	%dl, %dl
	jne	.L42
	cmpb	$5, %sil
	ja	.L21
	leaq	.L31(%rip), %r8
	movslq	(%r8,%rcx,4), %rdx
	addq	%r8, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal8compiler10Truncation10GeneralizeENS2_14TruncationKindES3_
	.align 4
	.align 4
.L31:
	.long	.L28-.L31
	.long	.L35-.L31
	.long	.L34-.L31
	.long	.L33-.L31
	.long	.L32-.L31
	.long	.L30-.L31
	.section	.text._ZN2v88internal8compiler10Truncation10GeneralizeENS2_14TruncationKindES3_
	.p2align 4,,10
	.p2align 3
.L40:
	cmpb	$5, %sil
	jne	.L21
.L44:
	movl	$5, %eax
.L28:
	ret
.L30:
	cmpb	$5, %dil
	sete	%dl
.L36:
	testb	%dl, %dl
	jne	.L28
	cmpb	$4, %dil
	ja	.L37
	cmpb	$1, %dil
	je	.L39
	cmpb	$4, %sil
	ja	.L40
	cmpb	$1, %sil
	sete	%al
	addl	$4, %eax
	ret
.L32:
	leal	-4(%rdi), %edx
	cmpb	$1, %dl
	setbe	%dl
	jmp	.L36
.L34:
	leal	-2(%rdi), %edx
	cmpb	$3, %dl
	setbe	%dl
	jmp	.L36
.L35:
	movl	%edi, %edx
	andl	$-5, %edx
	cmpb	$1, %dl
	sete	%dl
	jmp	.L36
.L33:
	leal	-3(%rdi), %edx
	cmpb	$2, %dl
	setbe	%dl
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L27:
	movl	%esi, %edx
	andl	$-5, %edx
	cmpb	$1, %dl
	sete	%dl
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L26:
	leal	-2(%rsi), %edx
	cmpb	$3, %dl
	setbe	%dl
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L25:
	leal	-3(%rsi), %edx
	cmpb	$2, %dl
	setbe	%dl
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L24:
	leal	-4(%rsi), %edx
	cmpb	$1, %dl
	setbe	%dl
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L37:
	cmpb	$5, %dil
	jne	.L21
.L39:
	cmpb	$5, %sil
	jbe	.L44
.L21:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC8(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE22738:
	.size	_ZN2v88internal8compiler10Truncation10GeneralizeENS2_14TruncationKindES3_, .-_ZN2v88internal8compiler10Truncation10GeneralizeENS2_14TruncationKindES3_
	.section	.text._ZN2v88internal8compiler10Truncation23GeneralizeIdentifyZerosENS1_13IdentifyZerosES3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler10Truncation23GeneralizeIdentifyZerosENS1_13IdentifyZerosES3_
	.type	_ZN2v88internal8compiler10Truncation23GeneralizeIdentifyZerosENS1_13IdentifyZerosES3_, @function
_ZN2v88internal8compiler10Truncation23GeneralizeIdentifyZerosENS1_13IdentifyZerosES3_:
.LFB22739:
	.cfi_startproc
	endbr64
	cmpl	%esi, %edi
	movl	$1, %eax
	cmove	%edi, %eax
	ret
	.cfi_endproc
.LFE22739:
	.size	_ZN2v88internal8compiler10Truncation23GeneralizeIdentifyZerosENS1_13IdentifyZerosES3_, .-_ZN2v88internal8compiler10Truncation23GeneralizeIdentifyZerosENS1_13IdentifyZerosES3_
	.section	.text._ZN2v88internal8compiler10Truncation11LessGeneralENS2_14TruncationKindES3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler10Truncation11LessGeneralENS2_14TruncationKindES3_
	.type	_ZN2v88internal8compiler10Truncation11LessGeneralENS2_14TruncationKindES3_, @function
_ZN2v88internal8compiler10Truncation11LessGeneralENS2_14TruncationKindES3_:
.LFB22740:
	.cfi_startproc
	endbr64
	cmpb	$5, %dil
	ja	.L51
	leaq	.L53(%rip), %rcx
	movzbl	%dil, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler10Truncation11LessGeneralENS2_14TruncationKindES3_,"a",@progbits
	.align 4
	.align 4
.L53:
	.long	.L59-.L53
	.long	.L57-.L53
	.long	.L56-.L53
	.long	.L55-.L53
	.long	.L54-.L53
	.long	.L52-.L53
	.section	.text._ZN2v88internal8compiler10Truncation11LessGeneralENS2_14TruncationKindES3_
	.p2align 4,,10
	.p2align 3
.L59:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	andl	$-5, %esi
	cmpb	$1, %sil
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	subl	$2, %esi
	cmpb	$3, %sil
	setbe	%al
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	subl	$3, %esi
	cmpb	$2, %sil
	setbe	%al
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	subl	$4, %esi
	cmpb	$1, %sil
	setbe	%al
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	cmpb	$5, %sil
	sete	%al
	ret
.L51:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC8(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE22740:
	.size	_ZN2v88internal8compiler10Truncation11LessGeneralENS2_14TruncationKindES3_, .-_ZN2v88internal8compiler10Truncation11LessGeneralENS2_14TruncationKindES3_
	.section	.text._ZN2v88internal8compiler10Truncation24LessGeneralIdentifyZerosENS1_13IdentifyZerosES3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler10Truncation24LessGeneralIdentifyZerosENS1_13IdentifyZerosES3_
	.type	_ZN2v88internal8compiler10Truncation24LessGeneralIdentifyZerosENS1_13IdentifyZerosES3_, @function
_ZN2v88internal8compiler10Truncation24LessGeneralIdentifyZerosENS1_13IdentifyZerosES3_:
.LFB22741:
	.cfi_startproc
	endbr64
	cmpl	%esi, %edi
	sete	%al
	testl	%edi, %edi
	sete	%dl
	orl	%edx, %eax
	ret
	.cfi_endproc
.LFE22741:
	.size	_ZN2v88internal8compiler10Truncation24LessGeneralIdentifyZerosENS1_13IdentifyZerosES3_, .-_ZN2v88internal8compiler10Truncation24LessGeneralIdentifyZerosENS1_13IdentifyZerosES3_
	.section	.text._ZN2v88internal8compiler21RepresentationChangerC2EPNS1_7JSGraphEPNS1_12JSHeapBrokerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21RepresentationChangerC2EPNS1_7JSGraphEPNS1_12JSHeapBrokerE
	.type	_ZN2v88internal8compiler21RepresentationChangerC2EPNS1_7JSGraphEPNS1_12JSHeapBrokerE, @function
_ZN2v88internal8compiler21RepresentationChangerC2EPNS1_7JSGraphEPNS1_12JSHeapBrokerE:
.LFB22744:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal8compiler9TypeCache3GetEv@PLT
	movq	%r12, 16(%rbx)
	movq	%rax, %xmm0
	xorl	%eax, %eax
	movhps	-24(%rbp), %xmm0
	movw	%ax, 24(%rbx)
	movups	%xmm0, (%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22744:
	.size	_ZN2v88internal8compiler21RepresentationChangerC2EPNS1_7JSGraphEPNS1_12JSHeapBrokerE, .-_ZN2v88internal8compiler21RepresentationChangerC2EPNS1_7JSGraphEPNS1_12JSHeapBrokerE
	.globl	_ZN2v88internal8compiler21RepresentationChangerC1EPNS1_7JSGraphEPNS1_12JSHeapBrokerE
	.set	_ZN2v88internal8compiler21RepresentationChangerC1EPNS1_7JSGraphEPNS1_12JSHeapBrokerE,_ZN2v88internal8compiler21RepresentationChangerC2EPNS1_7JSGraphEPNS1_12JSHeapBrokerE
	.section	.text._ZN2v88internal8compiler21RepresentationChanger26MakeTruncatedInt32ConstantEd,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21RepresentationChanger26MakeTruncatedInt32ConstantEd
	.type	_ZN2v88internal8compiler21RepresentationChanger26MakeTruncatedInt32ConstantEd, @function
_ZN2v88internal8compiler21RepresentationChanger26MakeTruncatedInt32ConstantEd:
.LFB22755:
	.cfi_startproc
	endbr64
	movsd	.LC10(%rip), %xmm2
	movapd	%xmm0, %xmm1
	movq	8(%rdi), %rdi
	andpd	.LC9(%rip), %xmm1
	ucomisd	%xmm1, %xmm2
	jb	.L66
	movsd	.LC11(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jnb	.L80
.L66:
	movabsq	$9218868437227405312, %rdx
	movq	%xmm0, %rax
	testq	%rdx, %rax
	je	.L74
	movq	%xmm0, %rdx
	xorl	%esi, %esi
	shrq	$52, %rdx
	andl	$2047, %edx
	movl	%edx, %ecx
	subl	$1075, %ecx
	js	.L81
	cmpl	$31, %ecx
	jg	.L69
	movabsq	$4503599627370495, %rsi
	movabsq	$4503599627370496, %rdx
	andq	%rax, %rsi
	addq	%rdx, %rsi
	salq	%cl, %rsi
	movl	%esi, %esi
.L72:
	sarq	$63, %rax
	orl	$1, %eax
	imull	%eax, %esi
.L69:
	jmp	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	.p2align 4,,10
	.p2align 3
.L80:
	comisd	.LC12(%rip), %xmm0
	jb	.L66
	cvttsd2sil	%xmm0, %esi
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%esi, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L66
	je	.L69
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L81:
	cmpl	$-52, %ecx
	jl	.L69
	movabsq	$4503599627370495, %rsi
	movabsq	$4503599627370496, %rcx
	andq	%rax, %rsi
	addq	%rcx, %rsi
	movl	$1075, %ecx
	subl	%edx, %ecx
	shrq	%cl, %rsi
	jmp	.L72
.L74:
	xorl	%esi, %esi
	jmp	.L69
	.cfi_endproc
.LFE22755:
	.size	_ZN2v88internal8compiler21RepresentationChanger26MakeTruncatedInt32ConstantEd, .-_ZN2v88internal8compiler21RepresentationChanger26MakeTruncatedInt32ConstantEd
	.section	.text._ZN2v88internal8compiler21RepresentationChanger24InsertUnconditionalDeoptEPNS1_4NodeENS0_16DeoptimizeReasonE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21RepresentationChanger24InsertUnconditionalDeoptEPNS1_4NodeENS0_16DeoptimizeReasonE
	.type	_ZN2v88internal8compiler21RepresentationChanger24InsertUnconditionalDeoptEPNS1_4NodeENS0_16DeoptimizeReasonE, @function
_ZN2v88internal8compiler21RepresentationChanger24InsertUnconditionalDeoptEPNS1_4NodeENS0_16DeoptimizeReasonE:
.LFB22756:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	8(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rax, %r13
	movq	(%rdi), %r15
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movl	%r14d, %esi
	leaq	-96(%rbp), %rdx
	movq	$0, -96(%rbp)
	movq	%rax, -104(%rbp)
	movq	8(%rbx), %rax
	leaq	-80(%rbp), %r14
	movl	$-1, -88(%rbp)
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder7CheckIfENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceE@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$3, %edx
	movq	-104(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movq	%r13, -64(%rbp)
	movhps	-112(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -104(%rbp)
	movq	8(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler21CommonOperatorBuilder11UnreachableEv@PLT
	movq	%r13, %xmm1
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	-104(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movq	%r15, %rdi
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14NodeProperties18ReplaceEffectInputEPNS1_4NodeES4_i@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L85
	addq	$72, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L85:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22756:
	.size	_ZN2v88internal8compiler21RepresentationChanger24InsertUnconditionalDeoptEPNS1_4NodeENS0_16DeoptimizeReasonE, .-_ZN2v88internal8compiler21RepresentationChanger24InsertUnconditionalDeoptEPNS1_4NodeENS0_16DeoptimizeReasonE
	.section	.text._ZN2v88internal8compiler21RepresentationChanger16InsertConversionEPNS1_4NodeEPKNS1_8OperatorES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21RepresentationChanger16InsertConversionEPNS1_4NodeEPKNS1_8OperatorES4_
	.type	_ZN2v88internal8compiler21RepresentationChanger16InsertConversionEPNS1_4NodeEPKNS1_8OperatorES4_, @function
_ZN2v88internal8compiler21RepresentationChanger16InsertConversionEPNS1_4NodeEPKNS1_8OperatorES4_:
.LFB22758:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	28(%rdx), %eax
	testl	%eax, %eax
	jg	.L91
	movq	8(%rdi), %rax
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	(%rax), %rdi
	movq	%rsi, -64(%rbp)
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r12
.L86:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L92
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L91:
	.cfi_restore_state
	movq	%rcx, %r14
	movq	%rcx, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	8(%rbx), %rdx
	movq	%r12, %rsi
	leaq	-64(%rbp), %rcx
	movq	%r13, %xmm0
	xorl	%r8d, %r8d
	movq	(%rdx), %rdi
	movhps	-72(%rbp), %xmm0
	movl	$3, %edx
	movq	%rax, -48(%rbp)
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler14NodeProperties18ReplaceEffectInputEPNS1_4NodeES4_i@PLT
	jmp	.L86
.L92:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22758:
	.size	_ZN2v88internal8compiler21RepresentationChanger16InsertConversionEPNS1_4NodeEPKNS1_8OperatorES4_, .-_ZN2v88internal8compiler21RepresentationChanger16InsertConversionEPNS1_4NodeEPKNS1_8OperatorES4_
	.section	.text._ZN2v88internal8compiler21RepresentationChanger16Int32OperatorForENS1_8IrOpcode5ValueE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21RepresentationChanger16Int32OperatorForENS1_8IrOpcode5ValueE
	.type	_ZN2v88internal8compiler21RepresentationChanger16Int32OperatorForENS1_8IrOpcode5ValueE, @function
_ZN2v88internal8compiler21RepresentationChanger16Int32OperatorForENS1_8IrOpcode5ValueE:
.LFB22761:
	.cfi_startproc
	endbr64
	subl	$119, %esi
	cmpl	$42, %esi
	ja	.L94
	leaq	.L96(%rip), %rdx
	movslq	(%rdx,%rsi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler21RepresentationChanger16Int32OperatorForENS1_8IrOpcode5ValueE,"a",@progbits
	.align 4
	.align 4
.L96:
	.long	.L106-.L96
	.long	.L105-.L96
	.long	.L104-.L96
	.long	.L106-.L96
	.long	.L105-.L96
	.long	.L104-.L96
	.long	.L94-.L96
	.long	.L94-.L96
	.long	.L94-.L96
	.long	.L94-.L96
	.long	.L94-.L96
	.long	.L94-.L96
	.long	.L94-.L96
	.long	.L97-.L96
	.long	.L95-.L96
	.long	.L103-.L96
	.long	.L102-.L96
	.long	.L101-.L96
	.long	.L99-.L96
	.long	.L98-.L96
	.long	.L100-.L96
	.long	.L94-.L96
	.long	.L94-.L96
	.long	.L94-.L96
	.long	.L94-.L96
	.long	.L94-.L96
	.long	.L94-.L96
	.long	.L94-.L96
	.long	.L94-.L96
	.long	.L94-.L96
	.long	.L97-.L96
	.long	.L95-.L96
	.long	.L103-.L96
	.long	.L102-.L96
	.long	.L101-.L96
	.long	.L100-.L96
	.long	.L99-.L96
	.long	.L98-.L96
	.long	.L94-.L96
	.long	.L94-.L96
	.long	.L94-.L96
	.long	.L97-.L96
	.long	.L95-.L96
	.section	.text._ZN2v88internal8compiler21RepresentationChanger16Int32OperatorForENS1_8IrOpcode5ValueE
	.p2align 4,,10
	.p2align 3
.L97:
	movq	8(%rdi), %rax
	movq	16(%rax), %rdi
	jmp	_ZN2v88internal8compiler22MachineOperatorBuilder8Int32AddEv@PLT
	.p2align 4,,10
	.p2align 3
.L95:
	movq	8(%rdi), %rax
	movq	16(%rax), %rdi
	jmp	_ZN2v88internal8compiler22MachineOperatorBuilder8Int32SubEv@PLT
	.p2align 4,,10
	.p2align 3
.L102:
	movq	8(%rdi), %rax
	movq	16(%rax), %rdi
	jmp	_ZN2v88internal8compiler22MachineOperatorBuilder8Int32DivEv@PLT
	.p2align 4,,10
	.p2align 3
.L101:
	movq	8(%rdi), %rax
	movq	16(%rax), %rdi
	jmp	_ZN2v88internal8compiler22MachineOperatorBuilder8Int32ModEv@PLT
	.p2align 4,,10
	.p2align 3
.L103:
	movq	8(%rdi), %rax
	movq	16(%rax), %rdi
	jmp	_ZN2v88internal8compiler22MachineOperatorBuilder8Int32MulEv@PLT
	.p2align 4,,10
	.p2align 3
.L98:
	movq	8(%rdi), %rax
	movq	16(%rax), %rdi
	jmp	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32XorEv@PLT
	.p2align 4,,10
	.p2align 3
.L104:
	movq	8(%rdi), %rax
	movq	16(%rax), %rdi
	jmp	_ZN2v88internal8compiler22MachineOperatorBuilder20Int32LessThanOrEqualEv@PLT
	.p2align 4,,10
	.p2align 3
.L100:
	movq	8(%rdi), %rax
	movq	16(%rax), %rdi
	jmp	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32AndEv@PLT
	.p2align 4,,10
	.p2align 3
.L106:
	movq	8(%rdi), %rax
	movq	16(%rax), %rdi
	jmp	_ZN2v88internal8compiler22MachineOperatorBuilder11Word32EqualEv@PLT
	.p2align 4,,10
	.p2align 3
.L105:
	movq	8(%rdi), %rax
	movq	16(%rax), %rdi
	jmp	_ZN2v88internal8compiler22MachineOperatorBuilder13Int32LessThanEv@PLT
	.p2align 4,,10
	.p2align 3
.L99:
	movq	8(%rdi), %rax
	movq	16(%rax), %rdi
	jmp	_ZN2v88internal8compiler22MachineOperatorBuilder8Word32OrEv@PLT
.L94:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC8(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE22761:
	.size	_ZN2v88internal8compiler21RepresentationChanger16Int32OperatorForENS1_8IrOpcode5ValueE, .-_ZN2v88internal8compiler21RepresentationChanger16Int32OperatorForENS1_8IrOpcode5ValueE
	.section	.text._ZN2v88internal8compiler21RepresentationChanger24Int32OverflowOperatorForENS1_8IrOpcode5ValueE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21RepresentationChanger24Int32OverflowOperatorForENS1_8IrOpcode5ValueE
	.type	_ZN2v88internal8compiler21RepresentationChanger24Int32OverflowOperatorForENS1_8IrOpcode5ValueE, @function
_ZN2v88internal8compiler21RepresentationChanger24Int32OverflowOperatorForENS1_8IrOpcode5ValueE:
.LFB22762:
	.cfi_startproc
	endbr64
	cmpl	$160, %esi
	je	.L110
	ja	.L111
	cmpl	$152, %esi
	je	.L112
	cmpl	$153, %esi
	jne	.L114
	movq	8(%rdi), %rax
	movq	376(%rax), %rdi
	jmp	_ZN2v88internal8compiler25SimplifiedOperatorBuilder15CheckedInt32ModEv@PLT
	.p2align 4,,10
	.p2align 3
.L111:
	cmpl	$161, %esi
	jne	.L114
	movq	8(%rdi), %rax
	movq	376(%rax), %rdi
	jmp	_ZN2v88internal8compiler25SimplifiedOperatorBuilder15CheckedInt32SubEv@PLT
	.p2align 4,,10
	.p2align 3
.L110:
	movq	8(%rdi), %rax
	movq	376(%rax), %rdi
	jmp	_ZN2v88internal8compiler25SimplifiedOperatorBuilder15CheckedInt32AddEv@PLT
	.p2align 4,,10
	.p2align 3
.L112:
	movq	8(%rdi), %rax
	movq	376(%rax), %rdi
	jmp	_ZN2v88internal8compiler25SimplifiedOperatorBuilder15CheckedInt32DivEv@PLT
.L114:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC8(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE22762:
	.size	_ZN2v88internal8compiler21RepresentationChanger24Int32OverflowOperatorForENS1_8IrOpcode5ValueE, .-_ZN2v88internal8compiler21RepresentationChanger24Int32OverflowOperatorForENS1_8IrOpcode5ValueE
	.section	.text._ZN2v88internal8compiler21RepresentationChanger16Int64OperatorForENS1_8IrOpcode5ValueE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21RepresentationChanger16Int64OperatorForENS1_8IrOpcode5ValueE
	.type	_ZN2v88internal8compiler21RepresentationChanger16Int64OperatorForENS1_8IrOpcode5ValueE, @function
_ZN2v88internal8compiler21RepresentationChanger16Int64OperatorForENS1_8IrOpcode5ValueE:
.LFB22763:
	.cfi_startproc
	endbr64
	subl	$132, %esi
	cmpl	$29, %esi
	ja	.L119
	leaq	.L121(%rip), %rdx
	movslq	(%rdx,%rsi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler21RepresentationChanger16Int64OperatorForENS1_8IrOpcode5ValueE,"a",@progbits
	.align 4
	.align 4
.L121:
	.long	.L122-.L121
	.long	.L120-.L121
	.long	.L119-.L121
	.long	.L119-.L121
	.long	.L119-.L121
	.long	.L119-.L121
	.long	.L119-.L121
	.long	.L119-.L121
	.long	.L119-.L121
	.long	.L119-.L121
	.long	.L119-.L121
	.long	.L119-.L121
	.long	.L119-.L121
	.long	.L119-.L121
	.long	.L119-.L121
	.long	.L119-.L121
	.long	.L119-.L121
	.long	.L122-.L121
	.long	.L120-.L121
	.long	.L119-.L121
	.long	.L119-.L121
	.long	.L119-.L121
	.long	.L119-.L121
	.long	.L119-.L121
	.long	.L119-.L121
	.long	.L119-.L121
	.long	.L119-.L121
	.long	.L119-.L121
	.long	.L122-.L121
	.long	.L120-.L121
	.section	.text._ZN2v88internal8compiler21RepresentationChanger16Int64OperatorForENS1_8IrOpcode5ValueE
	.p2align 4,,10
	.p2align 3
.L120:
	movq	8(%rdi), %rax
	movq	16(%rax), %rdi
	jmp	_ZN2v88internal8compiler22MachineOperatorBuilder8Int64SubEv@PLT
	.p2align 4,,10
	.p2align 3
.L122:
	movq	8(%rdi), %rax
	movq	16(%rax), %rdi
	jmp	_ZN2v88internal8compiler22MachineOperatorBuilder8Int64AddEv@PLT
.L119:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC8(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE22763:
	.size	_ZN2v88internal8compiler21RepresentationChanger16Int64OperatorForENS1_8IrOpcode5ValueE, .-_ZN2v88internal8compiler21RepresentationChanger16Int64OperatorForENS1_8IrOpcode5ValueE
	.section	.text._ZN2v88internal8compiler21RepresentationChanger23TaggedSignedOperatorForENS1_8IrOpcode5ValueE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21RepresentationChanger23TaggedSignedOperatorForENS1_8IrOpcode5ValueE
	.type	_ZN2v88internal8compiler21RepresentationChanger23TaggedSignedOperatorForENS1_8IrOpcode5ValueE, @function
_ZN2v88internal8compiler21RepresentationChanger23TaggedSignedOperatorForENS1_8IrOpcode5ValueE:
.LFB22764:
	.cfi_startproc
	endbr64
	cmpl	$123, %esi
	je	.L126
	cmpl	$124, %esi
	je	.L127
	cmpl	$122, %esi
	je	.L136
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC8(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L126:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	movq	8(%rdi), %rax
	movq	16(%rax), %rdi
	cmpb	$4, 16(%rdi)
	je	.L137
	jmp	_ZN2v88internal8compiler22MachineOperatorBuilder13Int64LessThanEv@PLT
	.p2align 4,,10
	.p2align 3
.L136:
	movq	8(%rdi), %rax
	movq	16(%rax), %rdi
	cmpb	$4, 16(%rdi)
	je	.L138
	jmp	_ZN2v88internal8compiler22MachineOperatorBuilder11Word64EqualEv@PLT
	.p2align 4,,10
	.p2align 3
.L127:
	movq	8(%rdi), %rax
	movq	16(%rax), %rdi
	cmpb	$4, 16(%rdi)
	je	.L139
	jmp	_ZN2v88internal8compiler22MachineOperatorBuilder20Int64LessThanOrEqualEv@PLT
	.p2align 4,,10
	.p2align 3
.L137:
	jmp	_ZN2v88internal8compiler22MachineOperatorBuilder13Int32LessThanEv@PLT
	.p2align 4,,10
	.p2align 3
.L139:
	jmp	_ZN2v88internal8compiler22MachineOperatorBuilder20Int32LessThanOrEqualEv@PLT
	.p2align 4,,10
	.p2align 3
.L138:
	jmp	_ZN2v88internal8compiler22MachineOperatorBuilder11Word32EqualEv@PLT
	.cfi_endproc
.LFE22764:
	.size	_ZN2v88internal8compiler21RepresentationChanger23TaggedSignedOperatorForENS1_8IrOpcode5ValueE, .-_ZN2v88internal8compiler21RepresentationChanger23TaggedSignedOperatorForENS1_8IrOpcode5ValueE
	.section	.text._ZN2v88internal8compiler21RepresentationChanger17Uint32OperatorForENS1_8IrOpcode5ValueE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21RepresentationChanger17Uint32OperatorForENS1_8IrOpcode5ValueE
	.type	_ZN2v88internal8compiler21RepresentationChanger17Uint32OperatorForENS1_8IrOpcode5ValueE, @function
_ZN2v88internal8compiler21RepresentationChanger17Uint32OperatorForENS1_8IrOpcode5ValueE:
.LFB22765:
	.cfi_startproc
	endbr64
	subl	$119, %esi
	cmpl	$52, %esi
	ja	.L141
	leaq	.L143(%rip), %rdx
	movslq	(%rdx,%rsi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler21RepresentationChanger17Uint32OperatorForENS1_8IrOpcode5ValueE,"a",@progbits
	.align 4
	.align 4
.L143:
	.long	.L152-.L143
	.long	.L151-.L143
	.long	.L150-.L143
	.long	.L152-.L143
	.long	.L151-.L143
	.long	.L150-.L143
	.long	.L141-.L143
	.long	.L141-.L143
	.long	.L141-.L143
	.long	.L141-.L143
	.long	.L141-.L143
	.long	.L141-.L143
	.long	.L141-.L143
	.long	.L149-.L143
	.long	.L148-.L143
	.long	.L147-.L143
	.long	.L145-.L143
	.long	.L144-.L143
	.long	.L141-.L143
	.long	.L141-.L143
	.long	.L141-.L143
	.long	.L141-.L143
	.long	.L141-.L143
	.long	.L141-.L143
	.long	.L141-.L143
	.long	.L147-.L143
	.long	.L141-.L143
	.long	.L141-.L143
	.long	.L141-.L143
	.long	.L141-.L143
	.long	.L141-.L143
	.long	.L141-.L143
	.long	.L147-.L143
	.long	.L145-.L143
	.long	.L144-.L143
	.long	.L141-.L143
	.long	.L141-.L143
	.long	.L141-.L143
	.long	.L141-.L143
	.long	.L141-.L143
	.long	.L141-.L143
	.long	.L141-.L143
	.long	.L141-.L143
	.long	.L141-.L143
	.long	.L141-.L143
	.long	.L141-.L143
	.long	.L141-.L143
	.long	.L141-.L143
	.long	.L141-.L143
	.long	.L141-.L143
	.long	.L141-.L143
	.long	.L141-.L143
	.long	.L142-.L143
	.section	.text._ZN2v88internal8compiler21RepresentationChanger17Uint32OperatorForENS1_8IrOpcode5ValueE
	.p2align 4,,10
	.p2align 3
.L147:
	movq	8(%rdi), %rax
	movq	16(%rax), %rdi
	jmp	_ZN2v88internal8compiler22MachineOperatorBuilder8Int32MulEv@PLT
	.p2align 4,,10
	.p2align 3
.L144:
	movq	8(%rdi), %rax
	movq	16(%rax), %rdi
	jmp	_ZN2v88internal8compiler22MachineOperatorBuilder9Uint32ModEv@PLT
	.p2align 4,,10
	.p2align 3
.L150:
	movq	8(%rdi), %rax
	movq	16(%rax), %rdi
	jmp	_ZN2v88internal8compiler22MachineOperatorBuilder21Uint32LessThanOrEqualEv@PLT
	.p2align 4,,10
	.p2align 3
.L145:
	movq	8(%rdi), %rax
	movq	16(%rax), %rdi
	jmp	_ZN2v88internal8compiler22MachineOperatorBuilder9Uint32DivEv@PLT
	.p2align 4,,10
	.p2align 3
.L152:
	movq	8(%rdi), %rax
	movq	16(%rax), %rdi
	jmp	_ZN2v88internal8compiler22MachineOperatorBuilder11Word32EqualEv@PLT
	.p2align 4,,10
	.p2align 3
.L151:
	movq	8(%rdi), %rax
	movq	16(%rax), %rdi
	jmp	_ZN2v88internal8compiler22MachineOperatorBuilder14Uint32LessThanEv@PLT
	.p2align 4,,10
	.p2align 3
.L148:
	movq	8(%rdi), %rax
	movq	16(%rax), %rdi
	jmp	_ZN2v88internal8compiler22MachineOperatorBuilder8Int32SubEv@PLT
	.p2align 4,,10
	.p2align 3
.L142:
	movq	8(%rdi), %rax
	movq	16(%rax), %rdi
	jmp	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32ClzEv@PLT
	.p2align 4,,10
	.p2align 3
.L149:
	movq	8(%rdi), %rax
	movq	16(%rax), %rdi
	jmp	_ZN2v88internal8compiler22MachineOperatorBuilder8Int32AddEv@PLT
.L141:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC8(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE22765:
	.size	_ZN2v88internal8compiler21RepresentationChanger17Uint32OperatorForENS1_8IrOpcode5ValueE, .-_ZN2v88internal8compiler21RepresentationChanger17Uint32OperatorForENS1_8IrOpcode5ValueE
	.section	.text._ZN2v88internal8compiler21RepresentationChanger25Uint32OverflowOperatorForENS1_8IrOpcode5ValueE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21RepresentationChanger25Uint32OverflowOperatorForENS1_8IrOpcode5ValueE
	.type	_ZN2v88internal8compiler21RepresentationChanger25Uint32OverflowOperatorForENS1_8IrOpcode5ValueE, @function
_ZN2v88internal8compiler21RepresentationChanger25Uint32OverflowOperatorForENS1_8IrOpcode5ValueE:
.LFB22766:
	.cfi_startproc
	endbr64
	cmpl	$152, %esi
	je	.L156
	cmpl	$153, %esi
	jne	.L163
	movq	8(%rdi), %rax
	movq	376(%rax), %rdi
	jmp	_ZN2v88internal8compiler25SimplifiedOperatorBuilder16CheckedUint32ModEv@PLT
	.p2align 4,,10
	.p2align 3
.L156:
	movq	8(%rdi), %rax
	movq	376(%rax), %rdi
	jmp	_ZN2v88internal8compiler25SimplifiedOperatorBuilder16CheckedUint32DivEv@PLT
.L163:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC8(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE22766:
	.size	_ZN2v88internal8compiler21RepresentationChanger25Uint32OverflowOperatorForENS1_8IrOpcode5ValueE, .-_ZN2v88internal8compiler21RepresentationChanger25Uint32OverflowOperatorForENS1_8IrOpcode5ValueE
	.section	.text._ZN2v88internal8compiler21RepresentationChanger18Float64OperatorForENS1_8IrOpcode5ValueE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21RepresentationChanger18Float64OperatorForENS1_8IrOpcode5ValueE
	.type	_ZN2v88internal8compiler21RepresentationChanger18Float64OperatorForENS1_8IrOpcode5ValueE, @function
_ZN2v88internal8compiler21RepresentationChanger18Float64OperatorForENS1_8IrOpcode5ValueE:
.LFB22767:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	subl	$119, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpl	$76, %esi
	ja	.L165
	leaq	.L167(%rip), %rdx
	movslq	(%rdx,%rsi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler21RepresentationChanger18Float64OperatorForENS1_8IrOpcode5ValueE,"a",@progbits
	.align 4
	.align 4
.L167:
	.long	.L204-.L167
	.long	.L203-.L167
	.long	.L202-.L167
	.long	.L204-.L167
	.long	.L203-.L167
	.long	.L202-.L167
	.long	.L165-.L167
	.long	.L165-.L167
	.long	.L165-.L167
	.long	.L165-.L167
	.long	.L165-.L167
	.long	.L165-.L167
	.long	.L165-.L167
	.long	.L194-.L167
	.long	.L193-.L167
	.long	.L197-.L167
	.long	.L196-.L167
	.long	.L195-.L167
	.long	.L165-.L167
	.long	.L165-.L167
	.long	.L165-.L167
	.long	.L165-.L167
	.long	.L165-.L167
	.long	.L165-.L167
	.long	.L201-.L167
	.long	.L165-.L167
	.long	.L200-.L167
	.long	.L199-.L167
	.long	.L198-.L167
	.long	.L165-.L167
	.long	.L194-.L167
	.long	.L193-.L167
	.long	.L197-.L167
	.long	.L196-.L167
	.long	.L195-.L167
	.long	.L165-.L167
	.long	.L165-.L167
	.long	.L165-.L167
	.long	.L165-.L167
	.long	.L165-.L167
	.long	.L165-.L167
	.long	.L194-.L167
	.long	.L193-.L167
	.long	.L192-.L167
	.long	.L191-.L167
	.long	.L190-.L167
	.long	.L189-.L167
	.long	.L188-.L167
	.long	.L187-.L167
	.long	.L186-.L167
	.long	.L185-.L167
	.long	.L184-.L167
	.long	.L165-.L167
	.long	.L183-.L167
	.long	.L182-.L167
	.long	.L181-.L167
	.long	.L180-.L167
	.long	.L179-.L167
	.long	.L178-.L167
	.long	.L177-.L167
	.long	.L176-.L167
	.long	.L175-.L167
	.long	.L174-.L167
	.long	.L165-.L167
	.long	.L165-.L167
	.long	.L173-.L167
	.long	.L172-.L167
	.long	.L171-.L167
	.long	.L170-.L167
	.long	.L169-.L167
	.long	.L168-.L167
	.long	.L165-.L167
	.long	.L165-.L167
	.long	.L165-.L167
	.long	.L165-.L167
	.long	.L165-.L167
	.long	.L166-.L167
	.section	.text._ZN2v88internal8compiler21RepresentationChanger18Float64OperatorForENS1_8IrOpcode5ValueE
.L165:
	leaq	.LC8(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L193:
	movq	8(%rdi), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	16(%rax), %rdi
	jmp	_ZN2v88internal8compiler22MachineOperatorBuilder10Float64SubEv@PLT
	.p2align 4,,10
	.p2align 3
.L194:
	.cfi_restore_state
	movq	8(%rdi), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	16(%rax), %rdi
	jmp	_ZN2v88internal8compiler22MachineOperatorBuilder10Float64AddEv@PLT
	.p2align 4,,10
	.p2align 3
.L197:
	.cfi_restore_state
	movq	8(%rdi), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	16(%rax), %rdi
	jmp	_ZN2v88internal8compiler22MachineOperatorBuilder10Float64MulEv@PLT
	.p2align 4,,10
	.p2align 3
.L202:
	.cfi_restore_state
	movq	8(%rdi), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	16(%rax), %rdi
	jmp	_ZN2v88internal8compiler22MachineOperatorBuilder22Float64LessThanOrEqualEv@PLT
	.p2align 4,,10
	.p2align 3
.L203:
	.cfi_restore_state
	movq	8(%rdi), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	16(%rax), %rdi
	jmp	_ZN2v88internal8compiler22MachineOperatorBuilder15Float64LessThanEv@PLT
	.p2align 4,,10
	.p2align 3
.L204:
	.cfi_restore_state
	movq	8(%rdi), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	16(%rax), %rdi
	jmp	_ZN2v88internal8compiler22MachineOperatorBuilder12Float64EqualEv@PLT
	.p2align 4,,10
	.p2align 3
.L195:
	.cfi_restore_state
	movq	8(%rdi), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	16(%rax), %rdi
	jmp	_ZN2v88internal8compiler22MachineOperatorBuilder10Float64ModEv@PLT
	.p2align 4,,10
	.p2align 3
.L196:
	.cfi_restore_state
	movq	8(%rdi), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	16(%rax), %rdi
	jmp	_ZN2v88internal8compiler22MachineOperatorBuilder10Float64DivEv@PLT
	.p2align 4,,10
	.p2align 3
.L191:
	.cfi_restore_state
	movq	8(%rdi), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	16(%rax), %rdi
	jmp	_ZN2v88internal8compiler22MachineOperatorBuilder11Float64AcosEv@PLT
	.p2align 4,,10
	.p2align 3
.L192:
	.cfi_restore_state
	movq	8(%rdi), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	16(%rax), %rdi
	jmp	_ZN2v88internal8compiler22MachineOperatorBuilder10Float64AbsEv@PLT
	.p2align 4,,10
	.p2align 3
.L198:
	.cfi_restore_state
	movq	8(%rdi), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	16(%rax), %rdi
	jmp	_ZN2v88internal8compiler22MachineOperatorBuilder10Float64PowEv@PLT
	.p2align 4,,10
	.p2align 3
.L199:
	.cfi_restore_state
	movq	8(%rdi), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	16(%rax), %rdi
	jmp	_ZN2v88internal8compiler22MachineOperatorBuilder10Float64MinEv@PLT
	.p2align 4,,10
	.p2align 3
.L200:
	.cfi_restore_state
	movq	8(%rdi), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	16(%rax), %rdi
	jmp	_ZN2v88internal8compiler22MachineOperatorBuilder10Float64MaxEv@PLT
	.p2align 4,,10
	.p2align 3
.L201:
	.cfi_restore_state
	movq	8(%rdi), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	16(%rax), %rdi
	jmp	_ZN2v88internal8compiler22MachineOperatorBuilder12Float64Atan2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L189:
	.cfi_restore_state
	movq	8(%rdi), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	16(%rax), %rdi
	jmp	_ZN2v88internal8compiler22MachineOperatorBuilder11Float64AsinEv@PLT
	.p2align 4,,10
	.p2align 3
.L190:
	.cfi_restore_state
	movq	8(%rdi), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	16(%rax), %rdi
	jmp	_ZN2v88internal8compiler22MachineOperatorBuilder12Float64AcoshEv@PLT
	.p2align 4,,10
	.p2align 3
.L183:
	.cfi_restore_state
	movq	8(%rdi), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	16(%rax), %rdi
	jmp	_ZN2v88internal8compiler22MachineOperatorBuilder10Float64CosEv@PLT
	.p2align 4,,10
	.p2align 3
.L184:
	.cfi_restore_state
	movq	8(%rdi), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder14Float64RoundUpEv@PLT
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	%rdx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L185:
	.cfi_restore_state
	movq	8(%rdi), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	16(%rax), %rdi
	jmp	_ZN2v88internal8compiler22MachineOperatorBuilder11Float64CbrtEv@PLT
	.p2align 4,,10
	.p2align 3
.L186:
	.cfi_restore_state
	movq	8(%rdi), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	16(%rax), %rdi
	jmp	_ZN2v88internal8compiler22MachineOperatorBuilder12Float64AtanhEv@PLT
	.p2align 4,,10
	.p2align 3
.L187:
	.cfi_restore_state
	movq	8(%rdi), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	16(%rax), %rdi
	jmp	_ZN2v88internal8compiler22MachineOperatorBuilder11Float64AtanEv@PLT
	.p2align 4,,10
	.p2align 3
.L188:
	.cfi_restore_state
	movq	8(%rdi), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	16(%rax), %rdi
	jmp	_ZN2v88internal8compiler22MachineOperatorBuilder12Float64AsinhEv@PLT
	.p2align 4,,10
	.p2align 3
.L173:
	.cfi_restore_state
	movq	8(%rdi), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	16(%rax), %rdi
	jmp	_ZN2v88internal8compiler22MachineOperatorBuilder10Float64SinEv@PLT
	.p2align 4,,10
	.p2align 3
.L174:
	.cfi_restore_state
	movq	8(%rdi), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	16(%rax), %rdi
	jmp	_ZN2v88internal8compiler22MachineOperatorBuilder12Float64Log10Ev@PLT
	.p2align 4,,10
	.p2align 3
.L175:
	.cfi_restore_state
	movq	8(%rdi), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	16(%rax), %rdi
	jmp	_ZN2v88internal8compiler22MachineOperatorBuilder11Float64Log2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L176:
	.cfi_restore_state
	movq	8(%rdi), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	16(%rax), %rdi
	jmp	_ZN2v88internal8compiler22MachineOperatorBuilder12Float64Log1pEv@PLT
	.p2align 4,,10
	.p2align 3
.L177:
	.cfi_restore_state
	movq	8(%rdi), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	16(%rax), %rdi
	jmp	_ZN2v88internal8compiler22MachineOperatorBuilder10Float64LogEv@PLT
	.p2align 4,,10
	.p2align 3
.L178:
	.cfi_restore_state
	movq	8(%rdi), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	16(%rax), %rdi
	jmp	_ZN2v88internal8compiler22MachineOperatorBuilder24TruncateFloat64ToFloat32Ev@PLT
	.p2align 4,,10
	.p2align 3
.L179:
	.cfi_restore_state
	movq	8(%rdi), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder16Float64RoundDownEv@PLT
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	%rdx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L180:
	.cfi_restore_state
	movq	8(%rdi), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	16(%rax), %rdi
	jmp	_ZN2v88internal8compiler22MachineOperatorBuilder12Float64Expm1Ev@PLT
	.p2align 4,,10
	.p2align 3
.L181:
	.cfi_restore_state
	movq	8(%rdi), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	16(%rax), %rdi
	jmp	_ZN2v88internal8compiler22MachineOperatorBuilder10Float64ExpEv@PLT
	.p2align 4,,10
	.p2align 3
.L182:
	.cfi_restore_state
	movq	8(%rdi), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	16(%rax), %rdi
	jmp	_ZN2v88internal8compiler22MachineOperatorBuilder11Float64CoshEv@PLT
	.p2align 4,,10
	.p2align 3
.L166:
	.cfi_restore_state
	movq	8(%rdi), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	16(%rax), %rdi
	jmp	_ZN2v88internal8compiler22MachineOperatorBuilder17Float64SilenceNaNEv@PLT
	.p2align 4,,10
	.p2align 3
.L168:
	.cfi_restore_state
	movq	8(%rdi), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder20Float64RoundTruncateEv@PLT
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	%rdx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L169:
	.cfi_restore_state
	movq	8(%rdi), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	16(%rax), %rdi
	jmp	_ZN2v88internal8compiler22MachineOperatorBuilder11Float64TanhEv@PLT
	.p2align 4,,10
	.p2align 3
.L170:
	.cfi_restore_state
	movq	8(%rdi), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	16(%rax), %rdi
	jmp	_ZN2v88internal8compiler22MachineOperatorBuilder10Float64TanEv@PLT
	.p2align 4,,10
	.p2align 3
.L171:
	.cfi_restore_state
	movq	8(%rdi), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	16(%rax), %rdi
	jmp	_ZN2v88internal8compiler22MachineOperatorBuilder11Float64SqrtEv@PLT
	.p2align 4,,10
	.p2align 3
.L172:
	.cfi_restore_state
	movq	8(%rdi), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	16(%rax), %rdi
	jmp	_ZN2v88internal8compiler22MachineOperatorBuilder11Float64SinhEv@PLT
	.cfi_endproc
.LFE22767:
	.size	_ZN2v88internal8compiler21RepresentationChanger18Float64OperatorForENS1_8IrOpcode5ValueE, .-_ZN2v88internal8compiler21RepresentationChanger18Float64OperatorForENS1_8IrOpcode5ValueE
	.section	.rodata._ZN2v88internal8compiler21RepresentationChanger9TypeErrorEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES5_.str1.1,"aMS",@progbits,1
.LC13:
	.string	" ("
.LC14:
	.string	")"
	.section	.rodata._ZN2v88internal8compiler21RepresentationChanger9TypeErrorEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES5_.str1.8,"aMS",@progbits,1
	.align 8
.LC15:
	.string	"RepresentationChangerError: node #%d:%s of %s cannot be changed to %s"
	.section	.text._ZN2v88internal8compiler21RepresentationChanger9TypeErrorEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21RepresentationChanger9TypeErrorEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES5_
	.type	_ZN2v88internal8compiler21RepresentationChanger9TypeErrorEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES5_, @function
_ZN2v88internal8compiler21RepresentationChanger9TypeErrorEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES5_:
.LFB22768:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	subq	$848, %rsp
	movq	%rcx, -856(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 24(%rdi)
	movb	$1, 25(%rdi)
	je	.L211
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L212
	addq	$848, %rsp
	movq	%rsi, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L211:
	.cfi_restore_state
	leaq	-784(%rbp), %r13
	movl	%r8d, -864(%rbp)
	movq	%r13, %rdi
	movl	%edx, -860(%rbp)
	call	_ZNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
	movl	-860(%rbp), %edx
	movq	%r13, %rdi
	movl	%edx, %esi
	call	_ZN2v88internallsERSoNS0_21MachineRepresentationE@PLT
	leaq	.LC13(%rip), %rsi
	movq	%rax, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	%r13, %rsi
	leaq	-856(%rbp), %rdi
	call	_ZNK2v88internal8compiler4Type7PrintToERSo@PLT
	leaq	.LC14(%rip), %rsi
	movq	%r13, %rdi
	leaq	-400(%rbp), %r13
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	%r13, %rdi
	call	_ZNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
	movl	-864(%rbp), %r8d
	movq	%r13, %rdi
	movl	%r8d, %esi
	call	_ZN2v88internallsERSoNS0_21MachineRepresentationE@PLT
	leaq	-816(%rbp), %rdi
	leaq	-392(%rbp), %rsi
	call	_ZNKSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE3strEv@PLT
	leaq	-848(%rbp), %rdi
	leaq	-776(%rbp), %rsi
	movq	-816(%rbp), %r13
	call	_ZNKSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE3strEv@PLT
	movq	(%r12), %rax
	movl	20(%r12), %esi
	leaq	.LC15(%rip), %rdi
	movq	-848(%rbp), %rcx
	movq	%r13, %r8
	movq	8(%rax), %rdx
	andl	$16777215, %esi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L212:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22768:
	.size	_ZN2v88internal8compiler21RepresentationChanger9TypeErrorEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES5_, .-_ZN2v88internal8compiler21RepresentationChanger9TypeErrorEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES5_
	.section	.text._ZN2v88internal8compiler21RepresentationChanger27GetFloat32RepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeENS1_10TruncationE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21RepresentationChanger27GetFloat32RepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeENS1_10TruncationE
	.type	_ZN2v88internal8compiler21RepresentationChanger27GetFloat32RepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeENS1_10TruncationE, @function
_ZN2v88internal8compiler21RepresentationChanger27GetFloat32RepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeENS1_10TruncationE:
.LFB22753:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movzwl	16(%rax), %edx
	cmpw	$28, %dx
	je	.L214
	movq	%rsi, %r12
	movq	%r8, %rbx
	ja	.L215
	cmpw	$23, %dx
	je	.L216
	subl	$25, %edx
	cmpw	$1, %dx
	jbe	.L216
.L215:
	cmpq	$1, -72(%rbp)
	jne	.L267
.L223:
	movq	8(%r13), %rax
	movl	$12, %esi
	movq	8(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9DeadValueENS0_21MachineRepresentationE@PLT
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r12, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
.L213:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L268
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L267:
	.cfi_restore_state
	leaq	-72(%rbp), %r15
	movl	$1, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L223
	leal	-2(%r14), %eax
	cmpb	$2, %al
	ja	.L269
	cmpq	$1099, -72(%rbp)
	jne	.L270
.L226:
	movq	8(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder20ChangeInt32ToFloat64Ev@PLT
.L265:
	movq	%rax, %rsi
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L214:
	movsd	48(%rax), %xmm0
	comisd	.LC20(%rip), %xmm0
	movq	8(%rdi), %rdi
	jbe	.L257
	movsd	.LC21(%rip), %xmm2
	movss	.LC17(%rip), %xmm1
	comisd	%xmm0, %xmm2
	jb	.L271
.L219:
	movaps	%xmm1, %xmm0
	call	_ZN2v88internal8compiler12MachineGraph15Float32ConstantEf@PLT
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L257:
	movsd	.LC22(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jbe	.L258
	comisd	.LC23(%rip), %xmm0
	movss	.LC16(%rip), %xmm1
	jnb	.L219
	movss	.LC19(%rip), %xmm1
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L258:
	pxor	%xmm1, %xmm1
	cvtsd2ss	%xmm0, %xmm1
	movaps	%xmm1, %xmm0
	call	_ZN2v88internal8compiler12MachineGraph15Float32ConstantEf@PLT
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L271:
	movss	.LC18(%rip), %xmm1
	movaps	%xmm1, %xmm0
	call	_ZN2v88internal8compiler12MachineGraph15Float32ConstantEf@PLT
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L269:
	leal	-6(%r14), %eax
	cmpb	$2, %al
	ja	.L272
	cmpq	$8396767, -72(%rbp)
	jne	.L236
.L240:
	movl	$7263, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L237
	movq	8(%r13), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder23TruncateTaggedToFloat64Ev@PLT
	movq	%rax, %rsi
	.p2align 4,,10
	.p2align 3
.L266:
	movq	8(%r13), %rax
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	(%rax), %rdi
	movq	%r12, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r12
.L264:
	movq	8(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder24TruncateFloat64ToFloat32Ev@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L232
	movq	8(%r13), %rax
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	(%rax), %rdi
	movq	%r12, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L270:
	movl	$1099, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L226
	cmpq	$1031, -72(%rbp)
	je	.L230
	movl	$1031, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L230
	cmpb	$2, %bl
	je	.L230
	ja	.L231
	testb	%bl, %bl
	je	.L230
	.p2align 4,,10
	.p2align 3
.L232:
	movq	-72(%rbp), %rcx
	movl	$12, %r8d
	movl	%r14d, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21RepresentationChanger9TypeErrorEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES5_
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L272:
	cmpb	$11, %r14b
	je	.L273
	cmpb	$9, %r14b
	je	.L274
	cmpb	$10, %r14b
	je	.L275
	cmpb	$13, %r14b
	je	.L264
	cmpb	$5, %r14b
	jne	.L232
	movq	0(%r13), %rax
	movq	384(%rax), %rsi
	cmpq	-72(%rbp), %rsi
	je	.L247
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L232
.L247:
	movq	8(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder20ChangeInt64ToFloat64Ev@PLT
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L236:
	movl	$8396767, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L232
	cmpq	$7263, -72(%rbp)
	jne	.L240
.L237:
	movq	8(%r13), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder21ChangeTaggedToFloat64Ev@PLT
	movq	%rax, %rsi
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L230:
	movq	8(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder21ChangeUint32ToFloat64Ev@PLT
	jmp	.L265
.L273:
	movq	8(%r13), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler22MachineOperatorBuilder24ChangeCompressedToTaggedEv@PLT
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r12, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-72(%rbp), %rcx
	movq	%rbx, %r8
	movq	%r13, %rdi
	movq	%rax, %rsi
	movl	$8, %edx
	call	_ZN2v88internal8compiler21RepresentationChanger27GetFloat32RepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeENS1_10TruncationE
	jmp	.L213
.L274:
	movq	8(%r13), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler22MachineOperatorBuilder36ChangeCompressedSignedToTaggedSignedEv@PLT
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r12, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-72(%rbp), %rcx
	movq	%rbx, %r8
	movq	%r13, %rdi
	movq	%rax, %rsi
	movl	$6, %edx
	call	_ZN2v88internal8compiler21RepresentationChanger27GetFloat32RepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeENS1_10TruncationE
	jmp	.L213
.L231:
	leal	-3(%rbx), %r8d
	cmpb	$2, %r8b
	jbe	.L232
.L216:
	leaq	.LC8(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L275:
	movq	8(%r13), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler22MachineOperatorBuilder38ChangeCompressedPointerToTaggedPointerEv@PLT
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r12, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-72(%rbp), %rcx
	movq	%rbx, %r8
	movq	%r13, %rdi
	movq	%rax, %rsi
	movl	$7, %edx
	call	_ZN2v88internal8compiler21RepresentationChanger27GetFloat32RepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeENS1_10TruncationE
	jmp	.L213
.L268:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22753:
	.size	_ZN2v88internal8compiler21RepresentationChanger27GetFloat32RepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeENS1_10TruncationE, .-_ZN2v88internal8compiler21RepresentationChanger27GetFloat32RepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeENS1_10TruncationE
	.section	.rodata._ZN2v88internal8compiler21RepresentationChanger27GetFloat64RepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE.str1.8,"aMS",@progbits,1
	.align 8
.LC24:
	.string	"output_type.Is(Type::Boolean())"
	.section	.rodata._ZN2v88internal8compiler21RepresentationChanger27GetFloat64RepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE.str1.1,"aMS",@progbits,1
.LC25:
	.string	"Check failed: %s."
	.section	.rodata._ZN2v88internal8compiler21RepresentationChanger27GetFloat64RepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE.str1.8
	.align 8
.LC26:
	.string	"use_info.type_check() != TypeCheckKind::kNone"
	.section	.text._ZN2v88internal8compiler21RepresentationChanger27GetFloat64RepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21RepresentationChanger27GetFloat64RepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE
	.type	_ZN2v88internal8compiler21RepresentationChanger27GetFloat64RepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE, @function
_ZN2v88internal8compiler21RepresentationChanger27GetFloat64RepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE:
.LFB22754:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$40, %rsp
	movq	%rcx, -72(%rbp)
	movq	(%rsi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpw	$28, 16(%rdx)
	jne	.L277
	movzbl	28(%rbp), %eax
	testb	%al, %al
	je	.L278
	subl	$4, %eax
	cmpb	$1, %al
	jbe	.L278
.L277:
	cmpq	$1, -72(%rbp)
	jne	.L366
.L280:
	movq	8(%r13), %rax
	movl	$13, %esi
	movq	8(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9DeadValueENS0_21MachineRepresentationE@PLT
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r12, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
.L276:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L367
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L366:
	.cfi_restore_state
	leaq	-72(%rbp), %r15
	movl	$1, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L280
	leal	-2(%rbx), %eax
	cmpb	$2, %al
	ja	.L368
	cmpq	$1099, -72(%rbp)
	jne	.L283
.L365:
	movq	8(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder20ChangeInt32ToFloat64Ev@PLT
	movq	%rax, %rdx
.L284:
	testq	%rdx, %rdx
	je	.L294
	movq	%r14, %rcx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21RepresentationChanger16InsertConversionEPNS1_4NodeEPKNS1_8OperatorES4_
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L278:
	movsd	48(%rdx), %xmm0
	movq	8(%r13), %rdi
	call	_ZN2v88internal8compiler12MachineGraph15Float64ConstantEd@PLT
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L368:
	cmpb	$1, %bl
	je	.L369
	leal	-6(%rbx), %eax
	cmpb	$2, %al
	ja	.L370
	cmpq	$257, -72(%rbp)
	je	.L306
	movl	$257, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L306
	cmpb	$6, %bl
	je	.L371
	cmpq	$7263, -72(%rbp)
	je	.L309
	movl	$7263, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L309
	cmpq	$8396767, -72(%rbp)
	jne	.L372
	movzbl	20(%rbp), %eax
	cmpb	$4, %al
	ja	.L329
	cmpb	$1, %al
	je	.L315
.L316:
	movq	8(%r13), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder23TruncateTaggedToFloat64Ev@PLT
	movq	%rax, %rdx
	jmp	.L284
.L293:
	subl	$3, %eax
	cmpb	$2, %al
	ja	.L300
	.p2align 4,,10
	.p2align 3
.L294:
	movq	-72(%rbp), %rcx
	movl	$13, %r8d
	movl	%ebx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21RepresentationChanger9TypeErrorEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES5_
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L370:
	cmpb	$11, %bl
	je	.L373
	cmpb	$9, %bl
	je	.L374
	cmpb	$10, %bl
	je	.L375
	cmpb	$12, %bl
	je	.L376
	cmpb	$5, %bl
	jne	.L294
	movq	0(%r13), %rax
	movq	384(%rax), %rsi
	cmpq	-72(%rbp), %rsi
	je	.L326
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L294
.L326:
	movq	8(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder20ChangeInt64ToFloat64Ev@PLT
	movq	%rax, %rdx
	jmp	.L284
	.p2align 4,,10
	.p2align 3
.L283:
	movl	$1099, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L365
	cmpq	$3147, -72(%rbp)
	je	.L286
	movl	$3147, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L287
	movl	24(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L365
.L287:
	cmpq	$1031, -72(%rbp)
	jne	.L328
	.p2align 4,,10
	.p2align 3
.L298:
	movq	8(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder21ChangeUint32ToFloat64Ev@PLT
	movq	%rax, %rdx
	jmp	.L284
	.p2align 4,,10
	.p2align 3
.L369:
	cmpq	$513, -72(%rbp)
	je	.L296
	movl	$513, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L296
	leaq	.LC24(%rip), %rsi
	leaq	.LC25(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L296:
	movzbl	20(%rbp), %eax
	cmpb	$4, %al
	ja	.L297
	cmpb	$1, %al
	jne	.L298
.L299:
	movzbl	28(%rbp), %eax
	cmpb	$5, %al
	je	.L298
	testb	%al, %al
	je	.L377
	movl	$21, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21RepresentationChanger24InsertUnconditionalDeoptEPNS1_4NodeENS0_16DeoptimizeReasonE
	movl	$13, %esi
	movq	%rax, %rbx
	movq	8(%r13), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9DeadValueENS0_21MachineRepresentationE@PLT
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%rbx, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	jmp	.L276
.L297:
	cmpb	$5, %al
	je	.L299
.L300:
	leaq	.LC8(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L306:
	movq	8(%r13), %rdi
	movsd	.LC27(%rip), %xmm0
	call	_ZN2v88internal8compiler12MachineGraph15Float64ConstantEd@PLT
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L286:
	movl	24(%rbp), %eax
	testl	%eax, %eax
	je	.L365
.L328:
	movl	$1031, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L298
	cmpq	$3079, -72(%rbp)
	je	.L291
	movl	$3079, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L291
.L290:
	movzbl	20(%rbp), %eax
	cmpb	$2, %al
	je	.L298
	ja	.L293
	testb	%al, %al
	jne	.L294
	jmp	.L298
.L309:
	movq	8(%r13), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder21ChangeTaggedToFloat64Ev@PLT
	movq	%rax, %rdx
	jmp	.L284
.L373:
	movq	8(%r13), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler22MachineOperatorBuilder24ChangeCompressedToTaggedEv@PLT
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r12, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	pushq	40(%rbp)
	movq	-72(%rbp), %rcx
	movq	%r14, %r8
	pushq	32(%rbp)
	movq	%rax, %rsi
	movl	$8, %edx
	movq	%r13, %rdi
	pushq	24(%rbp)
	pushq	16(%rbp)
	call	_ZN2v88internal8compiler21RepresentationChanger27GetFloat64RepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE
	addq	$32, %rsp
	jmp	.L276
.L374:
	movq	8(%r13), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler22MachineOperatorBuilder36ChangeCompressedSignedToTaggedSignedEv@PLT
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r12, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	pushq	40(%rbp)
	movq	-72(%rbp), %rcx
	movq	%r14, %r8
	pushq	32(%rbp)
	movq	%rax, %rsi
	movl	$6, %edx
	movq	%r13, %rdi
	pushq	24(%rbp)
	pushq	16(%rbp)
	call	_ZN2v88internal8compiler21RepresentationChanger27GetFloat64RepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE
	addq	$32, %rsp
	jmp	.L276
.L375:
	movq	8(%r13), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler22MachineOperatorBuilder38ChangeCompressedPointerToTaggedPointerEv@PLT
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r12, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	pushq	40(%rbp)
	movq	-72(%rbp), %rcx
	movq	%r14, %r8
	pushq	32(%rbp)
	movq	%rax, %rsi
	movl	$7, %edx
	movq	%r13, %rdi
	pushq	24(%rbp)
	pushq	16(%rbp)
	call	_ZN2v88internal8compiler21RepresentationChanger27GetFloat64RepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE
	addq	$32, %rsp
	jmp	.L276
.L291:
	movl	24(%rbp), %edx
	testl	%edx, %edx
	je	.L298
	jmp	.L290
.L376:
	movq	8(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder22ChangeFloat32ToFloat64Ev@PLT
	movq	%rax, %rdx
	jmp	.L284
.L371:
	movq	8(%r13), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder25ChangeTaggedSignedToInt32Ev@PLT
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r12, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r12
	jmp	.L365
.L377:
	leaq	.LC26(%rip), %rsi
	leaq	.LC25(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L329:
	cmpb	$5, %al
	jne	.L300
.L315:
	movl	$8395871, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L316
	movzbl	28(%rbp), %eax
	cmpb	$4, %al
	je	.L320
	cmpb	$5, %al
	jne	.L294
	movl	$7903, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	je	.L320
	cmpb	$5, 28(%rbp)
	jne	.L294
	movq	8(%r13), %rax
	leaq	32(%rbp), %rdx
	movl	$1, %esi
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder22CheckedTaggedToFloat64ENS1_20CheckTaggedInputModeERKNS1_14FeedbackSourceE@PLT
	movq	%rax, %rdx
	jmp	.L284
.L372:
	movl	$8396767, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L312
	movzbl	20(%rbp), %eax
	cmpb	$4, %al
	ja	.L313
	subb	$1, %al
	jne	.L316
.L312:
	cmpq	$8395871, -72(%rbp)
	je	.L316
	jmp	.L315
.L313:
	cmpb	$5, %al
	je	.L312
	jmp	.L300
.L367:
	call	__stack_chk_fail@PLT
.L320:
	movq	8(%r13), %rax
	leaq	32(%rbp), %rdx
	xorl	%esi, %esi
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder22CheckedTaggedToFloat64ENS1_20CheckTaggedInputModeERKNS1_14FeedbackSourceE@PLT
	movq	%rax, %rdx
	jmp	.L284
	.cfi_endproc
.LFE22754:
	.size	_ZN2v88internal8compiler21RepresentationChanger27GetFloat64RepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE, .-_ZN2v88internal8compiler21RepresentationChanger27GetFloat64RepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE
	.section	.rodata._ZN2v88internal8compiler21RepresentationChanger26GetWord32RepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE.str1.8,"aMS",@progbits,1
	.align 8
.LC28:
	.string	"Truncation::Any(kIdentifyZeros) .IsLessGeneralThan(use_info.truncation())"
	.align 8
.LC29:
	.string	"use_info.type_check() != TypeCheckKind::kNumberOrOddball"
	.section	.text._ZN2v88internal8compiler21RepresentationChanger26GetWord32RepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21RepresentationChanger26GetWord32RepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE
	.type	_ZN2v88internal8compiler21RepresentationChanger26GetWord32RepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE, @function
_ZN2v88internal8compiler21RepresentationChanger26GetWord32RepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE:
.LFB22757:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$40, %rsp
	movq	%rcx, -72(%rbp)
	movq	(%rsi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzwl	16(%rdx), %eax
	cmpw	$26, %ax
	jbe	.L530
	cmpw	$28, %ax
	jne	.L381
	movzbl	28(%rbp), %eax
	movsd	48(%rdx), %xmm0
	testb	%al, %al
	je	.L383
	leal	-1(%rax), %edx
	cmpb	$1, %dl
	jbe	.L484
	subl	$4, %eax
	cmpb	$1, %al
	ja	.L381
.L484:
	comisd	.LC12(%rip), %xmm0
	jb	.L381
	movsd	.LC11(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L381
	movabsq	$-9223372036854775808, %rdx
	movq	%xmm0, %rax
	cmpq	%rdx, %rax
	je	.L381
	cvttsd2sil	%xmm0, %esi
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%esi, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L381
	jne	.L381
	andpd	.LC9(%rip), %xmm0
	movsd	.LC10(%rip), %xmm1
	movq	8(%r13), %rdi
	ucomisd	%xmm0, %xmm1
	jnb	.L394
	jmp	.L391
	.p2align 4,,10
	.p2align 3
.L530:
	cmpw	$22, %ax
	ja	.L380
.L381:
	cmpq	$1, -72(%rbp)
	jne	.L531
.L400:
	movq	8(%r13), %rax
	movl	$4, %esi
	movq	8(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9DeadValueENS0_21MachineRepresentationE@PLT
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r12, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
.L378:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L532
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L531:
	.cfi_restore_state
	leaq	-72(%rbp), %r15
	movl	$1, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L400
	cmpb	$1, %bl
	je	.L533
	cmpb	$13, %bl
	je	.L534
	cmpb	$12, %bl
	je	.L535
	leal	-6(%rbx), %eax
	cmpb	$2, %al
	ja	.L536
	cmpb	$6, %bl
	je	.L433
.L437:
	cmpq	$1099, -72(%rbp)
	je	.L434
	movl	$1099, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L434
	movzbl	28(%rbp), %eax
	cmpb	$1, %al
	je	.L537
	cmpb	$2, %al
	je	.L538
	cmpq	$1031, -72(%rbp)
	je	.L442
	movl	$1031, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L442
	movzbl	20(%rbp), %eax
	cmpb	$2, %al
	je	.L443
	ja	.L444
	testb	%al, %al
	je	.L443
	.p2align 4,,10
	.p2align 3
.L463:
	movq	-72(%rbp), %rcx
	movl	$4, %r8d
	movl	%ebx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21RepresentationChanger9TypeErrorEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES5_
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L383:
	movsd	.LC10(%rip), %xmm2
	movapd	%xmm0, %xmm1
	movq	8(%rdi), %rdi
	andpd	.LC9(%rip), %xmm1
	ucomisd	%xmm1, %xmm2
	jnb	.L510
.L488:
	movq	%xmm0, %rax
.L391:
	movabsq	$9218868437227405312, %rdx
	testq	%rdx, %rax
	je	.L476
	movq	%rax, %rdx
	xorl	%esi, %esi
	shrq	$52, %rdx
	andl	$2047, %edx
	movl	%edx, %ecx
	subl	$1075, %ecx
	js	.L539
	cmpl	$31, %ecx
	jg	.L394
	movabsq	$4503599627370495, %rsi
	movabsq	$4503599627370496, %rdx
	andq	%rax, %rsi
	addq	%rdx, %rsi
	salq	%cl, %rsi
	movl	%esi, %esi
.L397:
	sarq	$63, %rax
	orl	$1, %eax
	imull	%eax, %esi
.L394:
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L510:
	movsd	.LC11(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L488
	comisd	.LC12(%rip), %xmm0
	jb	.L488
	cvttsd2sil	%xmm0, %esi
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%esi, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L488
	jne	.L488
	jmp	.L394
	.p2align 4,,10
	.p2align 3
.L539:
	cmpl	$-52, %ecx
	jl	.L394
	movabsq	$4503599627370495, %rsi
	movabsq	$4503599627370496, %rcx
	andq	%rax, %rsi
	addq	%rcx, %rsi
	movl	$1075, %ecx
	subl	%edx, %ecx
	shrq	%cl, %rsi
	jmp	.L397
	.p2align 4,,10
	.p2align 3
.L533:
	cmpq	$513, -72(%rbp)
	jne	.L540
.L403:
	cmpb	$5, 20(%rbp)
	ja	.L380
	movzbl	20(%rbp), %eax
	leaq	.L405(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler21RepresentationChanger26GetWord32RepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE,"a",@progbits
	.align 4
	.align 4
.L405:
	.long	.L480-.L405
	.long	.L406-.L405
	.long	.L480-.L405
	.long	.L406-.L405
	.long	.L406-.L405
	.long	.L404-.L405
	.section	.text._ZN2v88internal8compiler21RepresentationChanger26GetWord32RepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE
	.p2align 4,,10
	.p2align 3
.L540:
	movl	$513, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L403
	leaq	.LC24(%rip), %rsi
	leaq	.LC25(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L406:
	leaq	.LC28(%rip), %rsi
	leaq	.LC25(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L536:
	cmpb	$11, %bl
	je	.L541
	cmpb	$9, %bl
	je	.L542
	cmpb	$10, %bl
	je	.L543
	cmpb	$4, %bl
	je	.L544
	leal	-2(%rbx), %eax
	cmpb	$1, %al
	jbe	.L480
	cmpb	$5, %bl
	jne	.L463
	cmpq	$1099, -72(%rbp)
	je	.L466
	movl	$1099, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L466
	cmpq	$1031, -72(%rbp)
	je	.L466
	movl	$1031, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L466
	movq	0(%r13), %rax
	movq	384(%rax), %rsi
	cmpq	%rsi, -72(%rbp)
	je	.L468
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L468
.L467:
	movzbl	28(%rbp), %eax
	subl	$1, %eax
	cmpb	$1, %al
	ja	.L527
	movq	0(%r13), %rax
	movq	408(%rax), %rsi
	cmpq	-72(%rbp), %rsi
	je	.L471
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L471
	movq	0(%r13), %rax
	movq	384(%rax), %rsi
	cmpq	%rsi, -72(%rbp)
	je	.L472
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L527
.L472:
	movq	8(%r13), %rax
	leaq	32(%rbp), %rsi
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder19CheckedInt64ToInt32ERKNS1_14FeedbackSourceE@PLT
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L414:
	testq	%rdx, %rdx
	je	.L463
	movq	%r14, %rcx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21RepresentationChanger16InsertConversionEPNS1_4NodeEPKNS1_8OperatorES4_
	jmp	.L378
.L455:
	subl	$4, %eax
	cmpb	$1, %al
	ja	.L463
	.p2align 4,,10
	.p2align 3
.L480:
	movq	%r12, %rax
	jmp	.L378
.L404:
	movzbl	28(%rbp), %eax
	testb	%al, %al
	je	.L545
	cmpb	$5, %al
	je	.L546
	movl	$25, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21RepresentationChanger24InsertUnconditionalDeoptEPNS1_4NodeENS0_16DeoptimizeReasonE
	movl	$4, %esi
	movq	%rax, %rbx
	movq	8(%r13), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9DeadValueENS0_21MachineRepresentationE@PLT
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%rbx, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L534:
	cmpq	$1099, -72(%rbp)
	je	.L422
	movl	$1099, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L422
	movzbl	28(%rbp), %eax
	subl	$1, %eax
	cmpb	$1, %al
	jbe	.L423
	cmpq	$1031, -72(%rbp)
	je	.L425
	movl	$1031, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L425
	movzbl	20(%rbp), %eax
	cmpb	$2, %al
	je	.L428
	ja	.L419
	testb	%al, %al
	je	.L428
.L420:
	movq	-72(%rbp), %rcx
	movl	$4, %r8d
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	$13, %edx
	call	_ZN2v88internal8compiler21RepresentationChanger9TypeErrorEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES5_
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L422:
	movq	8(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder20ChangeFloat64ToInt32Ev@PLT
	movq	%rax, %rdx
	jmp	.L414
	.p2align 4,,10
	.p2align 3
.L434:
	movq	8(%r13), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder19ChangeTaggedToInt32Ev@PLT
	movq	%rax, %rdx
	jmp	.L414
	.p2align 4,,10
	.p2align 3
.L535:
	movq	8(%r13), %rax
	movq	(%rax), %r9
	movq	16(%rax), %rdi
	movq	%r9, -80(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder22ChangeFloat32ToFloat64Ev@PLT
	movq	-80(%rbp), %r9
	xorl	%r8d, %r8d
	leaq	-64(%rbp), %rcx
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r12, -64(%rbp)
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	cmpq	$1099, -72(%rbp)
	movq	%rax, %r12
	je	.L422
	movl	$1099, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L422
	movzbl	28(%rbp), %eax
	subl	$1, %eax
	cmpb	$1, %al
	jbe	.L423
	cmpq	$1031, -72(%rbp)
	je	.L425
	movl	$1031, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L425
	movzbl	20(%rbp), %edi
	movl	$2, %esi
	call	_ZN2v88internal8compiler10Truncation11LessGeneralENS2_14TruncationKindES3_
	testb	%al, %al
	jne	.L428
	movq	-72(%rbp), %rcx
	movl	$4, %r8d
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	$12, %edx
	call	_ZN2v88internal8compiler21RepresentationChanger9TypeErrorEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES5_
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L433:
	call	_ZN2v88internal8compiler10BitsetType11SignedSmallEv@PLT
	orl	$1, %eax
	cmpq	%rax, -72(%rbp)
	je	.L436
	movq	%rax, %rsi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L437
.L436:
	movq	8(%r13), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder25ChangeTaggedSignedToInt32Ev@PLT
	movq	%rax, %rdx
	jmp	.L414
.L423:
	movq	8(%r13), %rax
	movl	$2049, %esi
	movq	%r15, %rdi
	movq	376(%rax), %r8
	movq	%r8, -80(%rbp)
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	movq	-80(%rbp), %r8
	movl	$1, %esi
	testb	%al, %al
	je	.L427
	movl	24(%rbp), %eax
	testl	%eax, %eax
	sete	%sil
.L427:
	leaq	32(%rbp), %rdx
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder21CheckedFloat64ToInt32ENS1_21CheckForMinusZeroModeERKNS1_14FeedbackSourceE@PLT
	movq	%rax, %rdx
	jmp	.L414
.L425:
	movq	8(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder21ChangeFloat64ToUint32Ev@PLT
	movq	%rax, %rdx
	jmp	.L414
.L541:
	movq	8(%r13), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler22MachineOperatorBuilder24ChangeCompressedToTaggedEv@PLT
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r12, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	pushq	40(%rbp)
	movq	-72(%rbp), %rcx
	movq	%r14, %r8
	pushq	32(%rbp)
	movq	%rax, %rsi
	movl	$8, %edx
	movq	%r13, %rdi
	pushq	24(%rbp)
	pushq	16(%rbp)
	call	_ZN2v88internal8compiler21RepresentationChanger26GetWord32RepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE
	addq	$32, %rsp
	jmp	.L378
.L544:
	movzbl	28(%rbp), %eax
	leal	-1(%rax), %edx
	cmpb	$1, %dl
	ja	.L455
	cmpq	$1099, -72(%rbp)
	je	.L480
	movl	24(%rbp), %eax
	movl	$1099, %esi
	movq	%r15, %rdi
	movl	%eax, -80(%rbp)
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L480
	cmpl	$0, -80(%rbp)
	jne	.L461
	cmpq	$3147, -72(%rbp)
	je	.L480
	movl	$3147, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L480
.L461:
	cmpq	$1031, -72(%rbp)
	je	.L459
	movl	$1031, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L459
	cmpl	$0, -80(%rbp)
	jne	.L462
	cmpq	$3079, -72(%rbp)
	je	.L459
	movl	$3079, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L459
.L462:
	movq	-72(%rbp), %rcx
	movl	$4, %r8d
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	$4, %edx
	call	_ZN2v88internal8compiler21RepresentationChanger9TypeErrorEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES5_
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L542:
	call	_ZN2v88internal8compiler10BitsetType11SignedSmallEv@PLT
	movl	%eax, %esi
	orl	$1, %esi
	cmpq	%rsi, -72(%rbp)
	je	.L452
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L452
	movq	8(%r13), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler22MachineOperatorBuilder36ChangeCompressedSignedToTaggedSignedEv@PLT
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r12, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	pushq	40(%rbp)
	movq	-72(%rbp), %rcx
	movq	%r14, %r8
	pushq	32(%rbp)
	movq	%rax, %rsi
	movl	$6, %edx
	movq	%r13, %rdi
	pushq	24(%rbp)
	pushq	16(%rbp)
	call	_ZN2v88internal8compiler21RepresentationChanger26GetWord32RepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE
	addq	$32, %rsp
	jmp	.L378
.L442:
	movq	8(%r13), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder20ChangeTaggedToUint32Ev@PLT
	movq	%rax, %rdx
	jmp	.L414
.L543:
	movq	8(%r13), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler22MachineOperatorBuilder38ChangeCompressedPointerToTaggedPointerEv@PLT
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r12, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	pushq	40(%rbp)
	movq	-72(%rbp), %rcx
	movq	%r14, %r8
	pushq	32(%rbp)
	movq	%rax, %rsi
	movl	$7, %edx
	movq	%r13, %rdi
	pushq	24(%rbp)
	pushq	16(%rbp)
	call	_ZN2v88internal8compiler21RepresentationChanger26GetWord32RepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE
	addq	$32, %rsp
	jmp	.L378
.L452:
	movq	8(%r13), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder29ChangeCompressedSignedToInt32Ev@PLT
	movq	%rax, %rdx
	jmp	.L414
.L545:
	leaq	.LC26(%rip), %rsi
	leaq	.LC25(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L546:
	leaq	.LC29(%rip), %rsi
	leaq	.LC25(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L476:
	xorl	%esi, %esi
	jmp	.L394
.L468:
	movzbl	20(%rbp), %edi
	movl	$2, %esi
	call	_ZN2v88internal8compiler10Truncation11LessGeneralENS2_14TruncationKindES3_
	testb	%al, %al
	je	.L467
.L466:
	movq	8(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder20TruncateInt64ToInt32Ev@PLT
	movq	%rax, %rdx
	jmp	.L414
.L537:
	movq	8(%r13), %rax
	leaq	32(%rbp), %rsi
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder26CheckedTaggedSignedToInt32ERKNS1_14FeedbackSourceE@PLT
	movq	%rax, %rdx
	jmp	.L414
.L538:
	movq	8(%r13), %rax
	movl	$2049, %esi
	movq	%r15, %rdi
	movq	376(%rax), %r8
	movq	%r8, -80(%rbp)
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	movq	-80(%rbp), %r8
	movl	$1, %esi
	testb	%al, %al
	je	.L441
	cmpl	$0, 24(%rbp)
	sete	%sil
.L441:
	leaq	32(%rbp), %rdx
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder20CheckedTaggedToInt32ENS1_21CheckForMinusZeroModeERKNS1_14FeedbackSourceE@PLT
	movq	%rax, %rdx
	jmp	.L414
.L444:
	subl	$3, %eax
	cmpb	$2, %al
	jbe	.L463
.L380:
	leaq	.LC8(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L419:
	subl	$3, %eax
	cmpb	$2, %al
	jbe	.L420
	jmp	.L380
.L443:
	cmpq	$8396767, -72(%rbp)
	je	.L446
	movl	$8396767, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L446
	movzbl	28(%rbp), %eax
	cmpb	$4, %al
	je	.L547
	cmpb	$5, %al
	jne	.L463
	movq	8(%r13), %rax
	leaq	32(%rbp), %rdx
	movl	$1, %esi
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder29CheckedTruncateTaggedToWord32ENS1_20CheckTaggedInputModeERKNS1_14FeedbackSourceE@PLT
	movq	%rax, %rdx
	jmp	.L414
.L428:
	movq	8(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder23TruncateFloat64ToWord32Ev@PLT
	movq	%rax, %rdx
	jmp	.L414
.L446:
	movq	8(%r13), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder22TruncateTaggedToWord32Ev@PLT
	movq	%rax, %rdx
	jmp	.L414
.L532:
	call	__stack_chk_fail@PLT
.L459:
	movq	8(%r13), %rax
	leaq	32(%rbp), %rsi
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder20CheckedUint32ToInt32ERKNS1_14FeedbackSourceE@PLT
	movq	%rax, %rdx
	jmp	.L414
.L547:
	movq	8(%r13), %rax
	leaq	32(%rbp), %rdx
	xorl	%esi, %esi
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder29CheckedTruncateTaggedToWord32ENS1_20CheckTaggedInputModeERKNS1_14FeedbackSourceE@PLT
	movq	%rax, %rdx
	jmp	.L414
.L471:
	movq	8(%r13), %rax
	leaq	32(%rbp), %rsi
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder20CheckedUint64ToInt32ERKNS1_14FeedbackSourceE@PLT
	movq	%rax, %rdx
	jmp	.L414
.L527:
	movq	-72(%rbp), %rcx
	movl	$4, %r8d
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	$5, %edx
	call	_ZN2v88internal8compiler21RepresentationChanger9TypeErrorEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES5_
	jmp	.L378
	.cfi_endproc
.LFE22757:
	.size	_ZN2v88internal8compiler21RepresentationChanger26GetWord32RepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE, .-_ZN2v88internal8compiler21RepresentationChanger26GetWord32RepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE
	.section	.text._ZN2v88internal8compiler21RepresentationChanger23GetBitRepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21RepresentationChanger23GetBitRepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeE
	.type	_ZN2v88internal8compiler21RepresentationChanger23GetBitRepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeE, @function
_ZN2v88internal8compiler21RepresentationChanger23GetBitRepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeE:
.LFB22759:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%edx, %ebx
	subq	$64, %rsp
	movq	%rcx, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	cmpw	$30, 16(%rax)
	je	.L578
.L549:
	cmpq	$1, -72(%rbp)
	jne	.L579
.L552:
	movq	8(%r13), %rax
	movl	$1, %esi
	movq	8(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9DeadValueENS0_21MachineRepresentationE@PLT
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r12, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
.L548:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L580
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L579:
	.cfi_restore_state
	leaq	-72(%rbp), %r14
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L552
	leal	-7(%rbx), %eax
	cmpb	$1, %al
	jbe	.L581
	cmpb	$6, %bl
	je	.L582
	cmpb	$11, %bl
	je	.L583
	cmpb	$9, %bl
	je	.L584
	cmpb	$10, %bl
	je	.L585
	leal	-2(%rbx), %eax
	cmpb	$2, %al
	jbe	.L586
	cmpb	$5, %bl
	je	.L587
	cmpb	$12, %bl
	je	.L588
	cmpb	$13, %bl
	je	.L589
	movq	-72(%rbp), %rcx
	movl	$1, %r8d
	movl	%ebx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21RepresentationChanger9TypeErrorEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES5_
	jmp	.L548
	.p2align 4,,10
	.p2align 3
.L578:
	movq	48(%rax), %rdx
	movq	16(%rdi), %rax
	movq	(%rax), %rax
	leaq	120(%rax), %rcx
	cmpq	%rdx, %rcx
	je	.L590
	addq	$112, %rax
	cmpq	%rax, %rdx
	jne	.L549
	movq	8(%rdi), %rdi
	movl	$1, %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	jmp	.L548
	.p2align 4,,10
	.p2align 3
.L590:
	movq	8(%rdi), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	jmp	.L548
	.p2align 4,,10
	.p2align 3
.L581:
	cmpq	$897, -72(%rbp)
	jne	.L591
.L555:
	movq	8(%r13), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder17ChangeTaggedToBitEv@PLT
	movq	%rax, %rsi
.L557:
	movq	8(%r13), %rax
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	(%rax), %rdi
	movq	%r12, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	jmp	.L548
	.p2align 4,,10
	.p2align 3
.L591:
	movl	$897, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L555
	cmpb	$8, %bl
	je	.L556
.L558:
	movq	8(%r13), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder26TruncateTaggedPointerToBitEv@PLT
	movq	%rax, %rsi
	jmp	.L557
	.p2align 4,,10
	.p2align 3
.L585:
	movq	8(%r13), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler22MachineOperatorBuilder38ChangeCompressedPointerToTaggedPointerEv@PLT
	leaq	-64(%rbp), %rcx
	movq	%r14, %rdi
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r12, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-72(%rbp), %rcx
	movl	$7, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21RepresentationChanger23GetBitRepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeE
	jmp	.L548
	.p2align 4,,10
	.p2align 3
.L582:
	movq	8(%r13), %rdi
	xorl	%esi, %esi
	movq	(%rdi), %r14
	call	_ZN2v88internal8compiler12MachineGraph14IntPtrConstantEl@PLT
	movq	%rax, -80(%rbp)
	movq	8(%r13), %rax
	movq	16(%rax), %rdi
	cmpb	$4, 16(%rdi)
	je	.L592
.L560:
	call	_ZN2v88internal8compiler22MachineOperatorBuilder11Word64EqualEv@PLT
.L577:
	movq	%rax, %rsi
.L561:
	movq	%r12, %xmm0
	leaq	-64(%rbp), %r12
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movhps	-80(%rbp), %xmm0
	movq	%r12, %rcx
	movl	$2, %edx
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%r13), %rdi
	xorl	%esi, %esi
	movq	%rax, -88(%rbp)
	movq	(%rdi), %r14
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%rax, -80(%rbp)
	movq	8(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder11Word32EqualEv@PLT
	movq	-88(%rbp), %xmm0
	movq	%rax, %rsi
	movhps	-80(%rbp), %xmm0
.L575:
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movl	$2, %edx
	movq	%r14, %rdi
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	jmp	.L548
	.p2align 4,,10
	.p2align 3
.L583:
	movq	8(%r13), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler22MachineOperatorBuilder24ChangeCompressedToTaggedEv@PLT
	leaq	-64(%rbp), %rcx
	movq	%r14, %rdi
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r12, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-72(%rbp), %rcx
	movl	$8, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21RepresentationChanger23GetBitRepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeE
	jmp	.L548
	.p2align 4,,10
	.p2align 3
.L586:
	movq	8(%r13), %rdi
	xorl	%esi, %esi
	movq	(%rdi), %r14
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%rax, -80(%rbp)
	movq	8(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder11Word32EqualEv@PLT
	jmp	.L577
	.p2align 4,,10
	.p2align 3
.L584:
	movq	8(%r13), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler22MachineOperatorBuilder36ChangeCompressedSignedToTaggedSignedEv@PLT
	leaq	-64(%rbp), %rcx
	movq	%r14, %rdi
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r12, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-72(%rbp), %rcx
	movl	$6, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21RepresentationChanger23GetBitRepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeE
	jmp	.L548
.L592:
	call	_ZN2v88internal8compiler22MachineOperatorBuilder11Word32EqualEv@PLT
	movq	%rax, %rsi
	jmp	.L561
.L589:
	movq	8(%r13), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler22MachineOperatorBuilder10Float64AbsEv@PLT
	movq	%r12, -64(%rbp)
	leaq	-64(%rbp), %r12
	movq	%r14, %rdi
	movq	%rax, %rsi
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movl	$1, %edx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%r13), %rdi
	pxor	%xmm0, %xmm0
	movq	%rax, -88(%rbp)
	movq	(%rdi), %r14
	call	_ZN2v88internal8compiler12MachineGraph15Float64ConstantEd@PLT
	movq	%rax, -80(%rbp)
	movq	8(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder15Float64LessThanEv@PLT
.L576:
	movq	-80(%rbp), %xmm0
	movq	%rax, %rsi
	movhps	-88(%rbp), %xmm0
	jmp	.L575
.L556:
	call	_ZN2v88internal8compiler10BitsetType11SignedSmallEv@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	orl	$1, %esi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	je	.L558
	movq	8(%r13), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder19TruncateTaggedToBitEv@PLT
	movq	%rax, %rsi
	jmp	.L557
.L587:
	movq	8(%r13), %rdi
	xorl	%esi, %esi
	movq	(%rdi), %r14
	call	_ZN2v88internal8compiler12MachineGraph13Int64ConstantEl@PLT
	movq	%rax, -80(%rbp)
	movq	8(%r13), %rax
	movq	16(%rax), %rdi
	jmp	.L560
.L588:
	movq	8(%r13), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler22MachineOperatorBuilder10Float32AbsEv@PLT
	movq	%r12, -64(%rbp)
	leaq	-64(%rbp), %r12
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movq	%r12, %rcx
	movl	$1, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%r13), %rdi
	pxor	%xmm0, %xmm0
	movq	%rax, -88(%rbp)
	movq	(%rdi), %r14
	call	_ZN2v88internal8compiler12MachineGraph15Float32ConstantEf@PLT
	movq	%rax, -80(%rbp)
	movq	8(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder15Float32LessThanEv@PLT
	jmp	.L576
.L580:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22759:
	.size	_ZN2v88internal8compiler21RepresentationChanger23GetBitRepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeE, .-_ZN2v88internal8compiler21RepresentationChanger23GetBitRepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeE
	.section	.text._ZN2v88internal8compiler21RepresentationChanger23InsertChangeBitToTaggedEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21RepresentationChanger23InsertChangeBitToTaggedEPNS1_4NodeE
	.type	_ZN2v88internal8compiler21RepresentationChanger23InsertChangeBitToTaggedEPNS1_4NodeE, @function
_ZN2v88internal8compiler21RepresentationChanger23InsertChangeBitToTaggedEPNS1_4NodeE:
.LFB22769:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder17ChangeBitToTaggedEv@PLT
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	leaq	-32(%rbp), %rcx
	movq	%rbx, -32(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L596
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L596:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22769:
	.size	_ZN2v88internal8compiler21RepresentationChanger23InsertChangeBitToTaggedEPNS1_4NodeE, .-_ZN2v88internal8compiler21RepresentationChanger23InsertChangeBitToTaggedEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler21RepresentationChanger28InsertChangeFloat32ToFloat64EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21RepresentationChanger28InsertChangeFloat32ToFloat64EPNS1_4NodeE
	.type	_ZN2v88internal8compiler21RepresentationChanger28InsertChangeFloat32ToFloat64EPNS1_4NodeE, @function
_ZN2v88internal8compiler21RepresentationChanger28InsertChangeFloat32ToFloat64EPNS1_4NodeE:
.LFB22770:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder22ChangeFloat32ToFloat64Ev@PLT
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	leaq	-32(%rbp), %rcx
	movq	%rbx, -32(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L600
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L600:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22770:
	.size	_ZN2v88internal8compiler21RepresentationChanger28InsertChangeFloat32ToFloat64EPNS1_4NodeE, .-_ZN2v88internal8compiler21RepresentationChanger28InsertChangeFloat32ToFloat64EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler21RepresentationChanger27InsertChangeFloat64ToUint32EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21RepresentationChanger27InsertChangeFloat64ToUint32EPNS1_4NodeE
	.type	_ZN2v88internal8compiler21RepresentationChanger27InsertChangeFloat64ToUint32EPNS1_4NodeE, @function
_ZN2v88internal8compiler21RepresentationChanger27InsertChangeFloat64ToUint32EPNS1_4NodeE:
.LFB22771:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder21ChangeFloat64ToUint32Ev@PLT
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	leaq	-32(%rbp), %rcx
	movq	%rbx, -32(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L604
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L604:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22771:
	.size	_ZN2v88internal8compiler21RepresentationChanger27InsertChangeFloat64ToUint32EPNS1_4NodeE, .-_ZN2v88internal8compiler21RepresentationChanger27InsertChangeFloat64ToUint32EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler21RepresentationChanger26InsertChangeFloat64ToInt32EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21RepresentationChanger26InsertChangeFloat64ToInt32EPNS1_4NodeE
	.type	_ZN2v88internal8compiler21RepresentationChanger26InsertChangeFloat64ToInt32EPNS1_4NodeE, @function
_ZN2v88internal8compiler21RepresentationChanger26InsertChangeFloat64ToInt32EPNS1_4NodeE:
.LFB22772:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder20ChangeFloat64ToInt32Ev@PLT
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	leaq	-32(%rbp), %rcx
	movq	%rbx, -32(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L608
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L608:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22772:
	.size	_ZN2v88internal8compiler21RepresentationChanger26InsertChangeFloat64ToInt32EPNS1_4NodeE, .-_ZN2v88internal8compiler21RepresentationChanger26InsertChangeFloat64ToInt32EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler21RepresentationChanger26InsertChangeInt32ToFloat64EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21RepresentationChanger26InsertChangeInt32ToFloat64EPNS1_4NodeE
	.type	_ZN2v88internal8compiler21RepresentationChanger26InsertChangeInt32ToFloat64EPNS1_4NodeE, @function
_ZN2v88internal8compiler21RepresentationChanger26InsertChangeInt32ToFloat64EPNS1_4NodeE:
.LFB22773:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder20ChangeInt32ToFloat64Ev@PLT
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	leaq	-32(%rbp), %rcx
	movq	%rbx, -32(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L612
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L612:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22773:
	.size	_ZN2v88internal8compiler21RepresentationChanger26InsertChangeInt32ToFloat64EPNS1_4NodeE, .-_ZN2v88internal8compiler21RepresentationChanger26InsertChangeInt32ToFloat64EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler21RepresentationChanger31InsertChangeTaggedSignedToInt32EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21RepresentationChanger31InsertChangeTaggedSignedToInt32EPNS1_4NodeE
	.type	_ZN2v88internal8compiler21RepresentationChanger31InsertChangeTaggedSignedToInt32EPNS1_4NodeE, @function
_ZN2v88internal8compiler21RepresentationChanger31InsertChangeTaggedSignedToInt32EPNS1_4NodeE:
.LFB22774:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder25ChangeTaggedSignedToInt32Ev@PLT
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	leaq	-32(%rbp), %rcx
	movq	%rbx, -32(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L616
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L616:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22774:
	.size	_ZN2v88internal8compiler21RepresentationChanger31InsertChangeTaggedSignedToInt32EPNS1_4NodeE, .-_ZN2v88internal8compiler21RepresentationChanger31InsertChangeTaggedSignedToInt32EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler21RepresentationChanger27InsertChangeTaggedToFloat64EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21RepresentationChanger27InsertChangeTaggedToFloat64EPNS1_4NodeE
	.type	_ZN2v88internal8compiler21RepresentationChanger27InsertChangeTaggedToFloat64EPNS1_4NodeE, @function
_ZN2v88internal8compiler21RepresentationChanger27InsertChangeTaggedToFloat64EPNS1_4NodeE:
.LFB22775:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder21ChangeTaggedToFloat64Ev@PLT
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	leaq	-32(%rbp), %rcx
	movq	%rbx, -32(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L620
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L620:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22775:
	.size	_ZN2v88internal8compiler21RepresentationChanger27InsertChangeTaggedToFloat64EPNS1_4NodeE, .-_ZN2v88internal8compiler21RepresentationChanger27InsertChangeTaggedToFloat64EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler21RepresentationChanger27InsertChangeUint32ToFloat64EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21RepresentationChanger27InsertChangeUint32ToFloat64EPNS1_4NodeE
	.type	_ZN2v88internal8compiler21RepresentationChanger27InsertChangeUint32ToFloat64EPNS1_4NodeE, @function
_ZN2v88internal8compiler21RepresentationChanger27InsertChangeUint32ToFloat64EPNS1_4NodeE:
.LFB22776:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder21ChangeUint32ToFloat64Ev@PLT
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	leaq	-32(%rbp), %rcx
	movq	%rbx, -32(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L624
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L624:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22776:
	.size	_ZN2v88internal8compiler21RepresentationChanger27InsertChangeUint32ToFloat64EPNS1_4NodeE, .-_ZN2v88internal8compiler21RepresentationChanger27InsertChangeUint32ToFloat64EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler21RepresentationChanger26InsertTruncateInt64ToInt32EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21RepresentationChanger26InsertTruncateInt64ToInt32EPNS1_4NodeE
	.type	_ZN2v88internal8compiler21RepresentationChanger26InsertTruncateInt64ToInt32EPNS1_4NodeE, @function
_ZN2v88internal8compiler21RepresentationChanger26InsertTruncateInt64ToInt32EPNS1_4NodeE:
.LFB22777:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder20TruncateInt64ToInt32Ev@PLT
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	leaq	-32(%rbp), %rcx
	movq	%rbx, -32(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L628
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L628:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22777:
	.size	_ZN2v88internal8compiler21RepresentationChanger26InsertTruncateInt64ToInt32EPNS1_4NodeE, .-_ZN2v88internal8compiler21RepresentationChanger26InsertTruncateInt64ToInt32EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler21RepresentationChanger26GetTaggedRepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeENS1_10TruncationE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21RepresentationChanger26GetTaggedRepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeENS1_10TruncationE
	.type	_ZN2v88internal8compiler21RepresentationChanger26GetTaggedRepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeENS1_10TruncationE, @function
_ZN2v88internal8compiler21RepresentationChanger26GetTaggedRepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeENS1_10TruncationE:
.LFB22749:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%rcx, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movzwl	16(%rax), %ecx
	cmpw	$30, %cx
	je	.L630
	movq	%rdi, %r13
	movl	%edx, %r15d
	ja	.L631
	cmpw	$28, %cx
	je	.L630
	cmpw	$29, %cx
	jne	.L712
.L632:
	leal	-6(%r15), %eax
	cmpb	$1, %al
	jbe	.L630
	cmpq	$1, -72(%rbp)
	jne	.L713
.L634:
	movq	8(%r13), %rax
	movl	$8, %esi
	movq	8(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9DeadValueENS0_21MachineRepresentationE@PLT
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r12, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r12
	.p2align 4,,10
	.p2align 3
.L630:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L714
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L712:
	.cfi_restore_state
	cmpw	$23, %cx
	je	.L633
	subl	$25, %ecx
	cmpw	$1, %cx
	ja	.L632
.L633:
	leaq	.LC8(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L631:
	cmpw	$284, %cx
	jne	.L632
	jmp	.L630
	.p2align 4,,10
	.p2align 3
.L713:
	leaq	-72(%rbp), %r14
	movq	%r8, %rbx
	movl	$1, %esi
	movq	%r8, -80(%rbp)
	movq	%r14, %rdi
	shrq	$32, %rbx
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L634
	cmpb	$1, %r15b
	movq	-80(%rbp), %r8
	je	.L715
	leal	-2(%r15), %eax
	cmpb	$2, %al
	ja	.L639
	cmpq	$1089, -72(%rbp)
	movq	%r8, -80(%rbp)
	je	.L640
	movl	$1089, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L640
	cmpq	$1099, -72(%rbp)
	movq	-80(%rbp), %r8
	je	.L709
	movl	$1099, %esi
	movq	%r14, %rdi
	movq	%r8, -80(%rbp)
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L709
	cmpq	$3147, -72(%rbp)
	movq	-80(%rbp), %r8
	je	.L643
	movl	$3147, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	movq	-80(%rbp), %r8
	testb	%al, %al
	je	.L644
	testl	%ebx, %ebx
	je	.L709
.L644:
	cmpq	$1031, -72(%rbp)
	jne	.L676
.L710:
	movq	8(%r13), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder20ChangeUint32ToTaggedEv@PLT
	leaq	-64(%rbp), %rcx
	movq	%rax, %rsi
	.p2align 4,,10
	.p2align 3
.L638:
	movq	8(%r13), %rax
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	(%rax), %rdi
	movq	%r12, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r12
	jmp	.L630
	.p2align 4,,10
	.p2align 3
.L639:
	cmpb	$5, %r15b
	je	.L716
	cmpb	$12, %r15b
	je	.L717
	cmpb	$13, %r15b
	je	.L718
	cmpb	$9, %r15b
	je	.L719
	cmpb	$10, %r15b
	je	.L720
	cmpb	$11, %r15b
	je	.L721
.L675:
	movq	-72(%rbp), %rcx
	movq	%r12, %rsi
	movl	$8, %r8d
	movl	%r15d, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21RepresentationChanger9TypeErrorEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES5_
	movq	%rax, %r12
	jmp	.L630
	.p2align 4,,10
	.p2align 3
.L715:
	cmpq	$513, -72(%rbp)
	je	.L637
	movl	$513, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L637
	movq	-72(%rbp), %rcx
	movq	%r12, %rsi
	movl	$8, %r8d
	movq	%r13, %rdi
	movl	$1, %edx
	call	_ZN2v88internal8compiler21RepresentationChanger9TypeErrorEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES5_
	movq	%rax, %r12
	jmp	.L630
	.p2align 4,,10
	.p2align 3
.L637:
	movq	8(%r13), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder17ChangeBitToTaggedEv@PLT
	leaq	-64(%rbp), %rcx
	movq	%rax, %rsi
	jmp	.L638
	.p2align 4,,10
	.p2align 3
.L640:
	movq	8(%r13), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder25ChangeInt31ToTaggedSignedEv@PLT
	leaq	-64(%rbp), %rcx
	movq	%rax, %rsi
	jmp	.L638
	.p2align 4,,10
	.p2align 3
.L716:
	cmpq	$1089, -72(%rbp)
	je	.L650
	movl	$1089, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L650
	cmpq	$1099, -72(%rbp)
	je	.L651
	movl	$1099, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L651
	cmpq	$1031, -72(%rbp)
	je	.L653
	movl	$1031, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L653
	movq	0(%r13), %rax
	movq	408(%rax), %rsi
	cmpq	%rsi, -72(%rbp)
	je	.L655
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L655
	movq	0(%r13), %rax
	movq	384(%rax), %rsi
	cmpq	%rsi, -72(%rbp)
	je	.L657
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L657
	cmpq	$134217729, -72(%rbp)
	je	.L659
	movl	$134217729, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L659
	movq	-72(%rbp), %rcx
	movq	%r12, %rsi
	movl	$8, %r8d
	movq	%r13, %rdi
	movl	$5, %edx
	call	_ZN2v88internal8compiler21RepresentationChanger9TypeErrorEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES5_
	movq	%rax, %r12
	jmp	.L630
	.p2align 4,,10
	.p2align 3
.L650:
	movq	8(%r13), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler22MachineOperatorBuilder20TruncateInt64ToInt32Ev@PLT
.L708:
	leaq	-64(%rbp), %rcx
	movq	%rax, %rsi
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%r14, %rdi
	movq	%rcx, -80(%rbp)
	movq	%r12, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r12
	movq	8(%r13), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder25ChangeInt31ToTaggedSignedEv@PLT
	movq	-80(%rbp), %rcx
	movq	%rax, %rsi
	jmp	.L638
.L664:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21RepresentationChanger26InsertChangeFloat64ToInt32EPNS1_4NodeE
	movq	%rax, %r12
.L709:
	movq	8(%r13), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder19ChangeInt32ToTaggedEv@PLT
	leaq	-64(%rbp), %rcx
	movq	%rax, %rsi
	jmp	.L638
.L718:
	cmpq	$1089, -72(%rbp)
	movq	%r8, -80(%rbp)
	je	.L663
	movl	$1089, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L663
	cmpq	$1099, -72(%rbp)
	je	.L664
	movl	$1099, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L664
	cmpq	$1031, -72(%rbp)
	je	.L666
	movl	$1031, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L666
	cmpq	$7263, -72(%rbp)
	movq	-80(%rbp), %r8
	je	.L669
	movl	$7263, %esi
	movq	%r14, %rdi
	movq	%r8, -80(%rbp)
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L669
	cmpq	$8396767, -72(%rbp)
	movq	-80(%rbp), %r8
	je	.L672
	movl	$8396767, %esi
	movq	%r14, %rdi
	movq	%r8, -80(%rbp)
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	movq	-80(%rbp), %r8
	testb	%al, %al
	jne	.L672
.L671:
	movq	-72(%rbp), %rcx
	movq	%r12, %rsi
	movl	$8, %r8d
	movq	%r13, %rdi
	movl	$13, %edx
	call	_ZN2v88internal8compiler21RepresentationChanger9TypeErrorEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES5_
	movq	%rax, %r12
	jmp	.L630
	.p2align 4,,10
	.p2align 3
.L717:
	movq	8(%r13), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler22MachineOperatorBuilder22ChangeFloat32ToFloat64Ev@PLT
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%rcx, -80(%rbp)
	movq	%r12, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$2049, %esi
	movq	%r14, %rdi
	movq	%rax, %r12
	movq	8(%r13), %rax
	movq	376(%rax), %r15
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	xorl	$1, %eax
	movq	%r15, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder21ChangeFloat64ToTaggedENS1_21CheckForMinusZeroModeE@PLT
	movq	-80(%rbp), %rcx
	movq	%rax, %rsi
	jmp	.L638
.L663:
	movq	8(%r13), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler22MachineOperatorBuilder20ChangeFloat64ToInt32Ev@PLT
	jmp	.L708
.L721:
	movq	8(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder24ChangeCompressedToTaggedEv@PLT
	leaq	-64(%rbp), %rcx
	movq	%rax, %rsi
	jmp	.L638
.L719:
	movq	8(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder36ChangeCompressedSignedToTaggedSignedEv@PLT
	leaq	-64(%rbp), %rcx
	movq	%rax, %rsi
	jmp	.L638
.L651:
	movq	8(%r13), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler22MachineOperatorBuilder20TruncateInt64ToInt32Ev@PLT
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%rcx, -80(%rbp)
	movq	%r12, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r12
	movq	8(%r13), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder19ChangeInt32ToTaggedEv@PLT
	movq	-80(%rbp), %rcx
	movq	%rax, %rsi
	jmp	.L638
.L720:
	movq	8(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder38ChangeCompressedPointerToTaggedPointerEv@PLT
	leaq	-64(%rbp), %rcx
	movq	%rax, %rsi
	jmp	.L638
.L643:
	testl	%ebx, %ebx
	je	.L709
.L676:
	movl	$1031, %esi
	movq	%r14, %rdi
	movq	%r8, -80(%rbp)
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L710
	cmpq	$3079, -72(%rbp)
	movq	-80(%rbp), %r8
	je	.L648
	movl	$3079, %esi
	movq	%r14, %rdi
	movq	%r8, -80(%rbp)
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	movq	-80(%rbp), %r8
	testb	%al, %al
	jne	.L648
.L647:
	movl	$2, %esi
	movl	%r8d, %edi
	call	_ZN2v88internal8compiler10Truncation11LessGeneralENS2_14TruncationKindES3_
	testb	%al, %al
	jne	.L710
	jmp	.L675
.L653:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21RepresentationChanger26InsertTruncateInt64ToInt32EPNS1_4NodeE
.L711:
	movq	%rax, %r12
	jmp	.L710
.L714:
	call	__stack_chk_fail@PLT
.L648:
	testl	%ebx, %ebx
	je	.L710
	jmp	.L647
.L666:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21RepresentationChanger27InsertChangeFloat64ToUint32EPNS1_4NodeE
	jmp	.L711
.L672:
	movl	$4, %esi
	movl	%r8d, %edi
	call	_ZN2v88internal8compiler10Truncation11LessGeneralENS2_14TruncationKindES3_
	testb	%al, %al
	je	.L671
.L669:
	movq	8(%r13), %rax
	movl	$2049, %esi
	movq	%r14, %rdi
	movq	376(%rax), %r15
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	xorl	$1, %eax
	movq	%r15, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder21ChangeFloat64ToTaggedENS1_21CheckForMinusZeroModeE@PLT
	leaq	-64(%rbp), %rcx
	movq	%rax, %rsi
	jmp	.L638
.L655:
	movq	8(%r13), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder20ChangeUint64ToTaggedEv@PLT
	leaq	-64(%rbp), %rcx
	movq	%rax, %rsi
	jmp	.L638
.L657:
	movq	8(%r13), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder19ChangeInt64ToTaggedEv@PLT
	leaq	-64(%rbp), %rcx
	movq	%rax, %rsi
	jmp	.L638
.L659:
	movq	8(%r13), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder20ChangeUint64ToBigIntEv@PLT
	leaq	-64(%rbp), %rcx
	movq	%rax, %rsi
	jmp	.L638
	.cfi_endproc
.LFE22749:
	.size	_ZN2v88internal8compiler21RepresentationChanger26GetTaggedRepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeENS1_10TruncationE, .-_ZN2v88internal8compiler21RepresentationChanger26GetTaggedRepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeENS1_10TruncationE
	.section	.text._ZN2v88internal8compiler21RepresentationChanger30GetCompressedRepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeENS1_10TruncationE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21RepresentationChanger30GetCompressedRepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeENS1_10TruncationE
	.type	_ZN2v88internal8compiler21RepresentationChanger30GetCompressedRepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeENS1_10TruncationE, @function
_ZN2v88internal8compiler21RepresentationChanger30GetCompressedRepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeENS1_10TruncationE:
.LFB22752:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leal	-9(%rdx), %eax
	cmpb	$1, %al
	jbe	.L722
	movq	%rdi, %r15
	movq	%rsi, %r12
	cmpq	$1, %rcx
	jne	.L738
.L724:
	movq	8(%r15), %rax
	movl	$11, %esi
	movq	8(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9DeadValueENS0_21MachineRepresentationE@PLT
	movq	%r13, %rdi
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r12, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r13
.L722:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L739
	addq	$40, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L738:
	.cfi_restore_state
	leaq	-72(%rbp), %rdi
	movl	$1, %esi
	movl	%edx, %ebx
	movq	%r8, %r14
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L724
	cmpb	$1, %bl
	je	.L740
	leal	-2(%rbx), %eax
	cmpb	$2, %al
	jbe	.L741
	cmpb	$5, %bl
	je	.L742
	cmpb	$12, %bl
	je	.L743
	cmpb	$13, %bl
	je	.L744
	leal	-6(%rbx), %eax
	cmpb	$2, %al
	ja	.L745
	movq	8(%r15), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder24ChangeTaggedToCompressedEv@PLT
	movq	%rax, %rsi
	.p2align 4,,10
	.p2align 3
.L727:
	movq	8(%r15), %rax
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	(%rax), %rdi
	movq	%r12, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r13
	jmp	.L722
	.p2align 4,,10
	.p2align 3
.L744:
	movq	-72(%rbp), %rcx
	movq	%r14, %r8
	movl	$13, %edx
	.p2align 4,,10
	.p2align 3
.L737:
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21RepresentationChanger26GetTaggedRepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeENS1_10TruncationE
	movq	%rax, %r12
	movq	8(%r15), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder24ChangeTaggedToCompressedEv@PLT
	movq	%rax, %rsi
	jmp	.L727
	.p2align 4,,10
	.p2align 3
.L741:
	movq	-72(%rbp), %rcx
	movq	%r14, %r8
	movl	%ebx, %edx
	jmp	.L737
	.p2align 4,,10
	.p2align 3
.L740:
	movq	-72(%rbp), %rcx
	movq	%r14, %r8
	movl	$1, %edx
	jmp	.L737
	.p2align 4,,10
	.p2align 3
.L742:
	movq	-72(%rbp), %rcx
	movq	%r14, %r8
	movl	$5, %edx
	jmp	.L737
	.p2align 4,,10
	.p2align 3
.L745:
	movq	-72(%rbp), %rcx
	movq	%r13, %rsi
	movl	$11, %r8d
	movl	%ebx, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21RepresentationChanger9TypeErrorEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES5_
	movq	%rax, %r13
	jmp	.L722
	.p2align 4,,10
	.p2align 3
.L743:
	movq	-72(%rbp), %rcx
	movq	%r14, %r8
	movl	$12, %edx
	jmp	.L737
.L739:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22752:
	.size	_ZN2v88internal8compiler21RepresentationChanger30GetCompressedRepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeENS1_10TruncationE, .-_ZN2v88internal8compiler21RepresentationChanger30GetCompressedRepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeENS1_10TruncationE
	.section	.text._ZN2v88internal8compiler21RepresentationChanger44InsertChangeCompressedPointerToTaggedPointerEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21RepresentationChanger44InsertChangeCompressedPointerToTaggedPointerEPNS1_4NodeE
	.type	_ZN2v88internal8compiler21RepresentationChanger44InsertChangeCompressedPointerToTaggedPointerEPNS1_4NodeE, @function
_ZN2v88internal8compiler21RepresentationChanger44InsertChangeCompressedPointerToTaggedPointerEPNS1_4NodeE:
.LFB22778:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder38ChangeCompressedPointerToTaggedPointerEv@PLT
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	leaq	-32(%rbp), %rcx
	movq	%rbx, -32(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L749
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L749:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22778:
	.size	_ZN2v88internal8compiler21RepresentationChanger44InsertChangeCompressedPointerToTaggedPointerEPNS1_4NodeE, .-_ZN2v88internal8compiler21RepresentationChanger44InsertChangeCompressedPointerToTaggedPointerEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler21RepresentationChanger42InsertChangeCompressedSignedToTaggedSignedEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21RepresentationChanger42InsertChangeCompressedSignedToTaggedSignedEPNS1_4NodeE
	.type	_ZN2v88internal8compiler21RepresentationChanger42InsertChangeCompressedSignedToTaggedSignedEPNS1_4NodeE, @function
_ZN2v88internal8compiler21RepresentationChanger42InsertChangeCompressedSignedToTaggedSignedEPNS1_4NodeE:
.LFB22779:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder36ChangeCompressedSignedToTaggedSignedEv@PLT
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	leaq	-32(%rbp), %rcx
	movq	%rbx, -32(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L753
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L753:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22779:
	.size	_ZN2v88internal8compiler21RepresentationChanger42InsertChangeCompressedSignedToTaggedSignedEPNS1_4NodeE, .-_ZN2v88internal8compiler21RepresentationChanger42InsertChangeCompressedSignedToTaggedSignedEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler21RepresentationChanger30InsertChangeCompressedToTaggedEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21RepresentationChanger30InsertChangeCompressedToTaggedEPNS1_4NodeE
	.type	_ZN2v88internal8compiler21RepresentationChanger30InsertChangeCompressedToTaggedEPNS1_4NodeE, @function
_ZN2v88internal8compiler21RepresentationChanger30InsertChangeCompressedToTaggedEPNS1_4NodeE:
.LFB22780:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder24ChangeCompressedToTaggedEv@PLT
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	leaq	-32(%rbp), %rcx
	movq	%rbx, -32(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L757
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L757:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22780:
	.size	_ZN2v88internal8compiler21RepresentationChanger30InsertChangeCompressedToTaggedEPNS1_4NodeE, .-_ZN2v88internal8compiler21RepresentationChanger30InsertChangeCompressedToTaggedEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler21RepresentationChanger33GetTaggedPointerRepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21RepresentationChanger33GetTaggedPointerRepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE
	.type	_ZN2v88internal8compiler21RepresentationChanger33GetTaggedPointerRepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE, @function
_ZN2v88internal8compiler21RepresentationChanger33GetTaggedPointerRepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE:
.LFB22748:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%rcx, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movzwl	16(%rax), %eax
	cmpw	$30, %ax
	je	.L759
	movq	%rdi, %r13
	movl	%edx, %ebx
	movq	%r8, %r14
	jbe	.L836
	cmpw	$284, %ax
	je	.L759
.L762:
	cmpq	$1, -72(%rbp)
	jne	.L837
.L763:
	movq	8(%r13), %rax
	movl	$7, %esi
	movq	8(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9DeadValueENS0_21MachineRepresentationE@PLT
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r12, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r12
.L759:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L838
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L836:
	.cfi_restore_state
	cmpw	$23, %ax
	je	.L761
	subl	$25, %eax
	cmpw	$1, %ax
	ja	.L762
.L761:
	leaq	.LC8(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L837:
	leaq	-72(%rbp), %r15
	movl	$1, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L763
	cmpb	$1, %bl
	je	.L839
	leal	-2(%rbx), %eax
	cmpb	$2, %al
	jbe	.L840
	cmpb	$5, %bl
	je	.L841
	cmpb	$12, %bl
	je	.L842
	cmpb	$13, %bl
	je	.L843
	leal	-6(%rbx), %eax
	testb	$-3, %al
	jne	.L781
	cmpb	$6, 28(%rbp)
	je	.L844
.L783:
	cmpb	$7, 28(%rbp)
	movq	-72(%rbp), %rax
	je	.L786
	cmpq	$134217729, %rax
	je	.L759
	movl	$134217729, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L788
	movq	-72(%rbp), %rax
.L786:
	cmpq	$134217729, %rax
	je	.L759
	movl	$134217729, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L759
.L832:
	movq	8(%r13), %rax
	leaq	32(%rbp), %rsi
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder11CheckBigIntERKNS1_14FeedbackSourceE@PLT
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L767:
	movq	%r12, %rsi
	movq	%r14, %rcx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21RepresentationChanger16InsertConversionEPNS1_4NodeEPKNS1_8OperatorES4_
	movq	%rax, %r12
	jmp	.L759
	.p2align 4,,10
	.p2align 3
.L840:
	cmpq	$1031, -72(%rbp)
	je	.L769
	movl	$1031, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L769
	cmpq	$1099, -72(%rbp)
	je	.L770
	movl	$1099, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L770
.L788:
	movq	-72(%rbp), %rcx
	movq	%r12, %rsi
	movl	$7, %r8d
	movl	%ebx, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21RepresentationChanger9TypeErrorEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES5_
	movq	%rax, %r12
	jmp	.L759
	.p2align 4,,10
	.p2align 3
.L839:
	cmpq	$513, -72(%rbp)
	je	.L766
	movl	$513, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L766
	movq	-72(%rbp), %rcx
	movq	%r12, %rsi
	movl	$8, %r8d
	movq	%r13, %rdi
	movl	$1, %edx
	call	_ZN2v88internal8compiler21RepresentationChanger9TypeErrorEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES5_
	movq	%rax, %r12
	jmp	.L759
	.p2align 4,,10
	.p2align 3
.L766:
	movq	8(%r13), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder17ChangeBitToTaggedEv@PLT
	movq	%rax, %rdx
	jmp	.L767
	.p2align 4,,10
	.p2align 3
.L769:
	movq	8(%r13), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler22MachineOperatorBuilder21ChangeUint32ToFloat64Ev@PLT
.L835:
	movq	%r12, -64(%rbp)
	movq	%rax, %rsi
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%r15, %rdi
.L830:
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r12
.L831:
	movq	8(%r13), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder28ChangeFloat64ToTaggedPointerEv@PLT
	movq	%rax, %rdx
	jmp	.L767
	.p2align 4,,10
	.p2align 3
.L841:
	movq	0(%r13), %rax
	movq	384(%rax), %rsi
	cmpq	%rsi, -72(%rbp)
	je	.L774
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L774
	cmpq	$134217729, -72(%rbp)
	je	.L775
	movl	$134217729, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L775
	movq	-72(%rbp), %rcx
	movq	%r12, %rsi
	movl	$7, %r8d
	movq	%r13, %rdi
	movl	$5, %edx
	call	_ZN2v88internal8compiler21RepresentationChanger9TypeErrorEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES5_
	movq	%rax, %r12
	jmp	.L759
	.p2align 4,,10
	.p2align 3
.L774:
	movq	8(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder20ChangeInt64ToFloat64Ev@PLT
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%rax, %rsi
	movq	8(%r13), %rax
	movq	(%rax), %rdi
	movq	%r12, -64(%rbp)
	jmp	.L830
.L842:
	cmpq	$7263, -72(%rbp)
	je	.L778
	movl	$7263, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L778
	movq	-72(%rbp), %rcx
	movq	%r12, %rsi
	movl	$7, %r8d
	movq	%r13, %rdi
	movl	$12, %edx
	call	_ZN2v88internal8compiler21RepresentationChanger9TypeErrorEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES5_
	movq	%rax, %r12
	jmp	.L759
.L770:
	movq	8(%r13), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler22MachineOperatorBuilder20ChangeInt32ToFloat64Ev@PLT
	jmp	.L835
.L778:
	movq	8(%r13), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler22MachineOperatorBuilder22ChangeFloat32ToFloat64Ev@PLT
	jmp	.L835
.L843:
	cmpq	$7263, -72(%rbp)
	je	.L831
	movl	$7263, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L831
	movq	-72(%rbp), %rcx
	movq	%r12, %rsi
	movl	$7, %r8d
	movq	%r13, %rdi
	movl	$13, %edx
	call	_ZN2v88internal8compiler21RepresentationChanger9TypeErrorEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES5_
	movq	%rax, %r12
	jmp	.L759
.L781:
	cmpb	$2, %al
	jbe	.L783
	cmpb	$10, %bl
	je	.L845
	cmpb	$11, %bl
	je	.L846
	cmpb	$9, %bl
	jne	.L788
.L800:
	cmpb	$6, 28(%rbp)
	jne	.L788
	call	_ZN2v88internal8compiler10BitsetType11SignedSmallEv@PLT
	movq	%r15, %rdi
	movl	%eax, %esi
	orl	$1, %esi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	je	.L834
	movq	8(%r13), %rax
	leaq	32(%rbp), %rsi
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder32CheckedCompressedToTaggedPointerERKNS1_14FeedbackSourceE@PLT
	movq	%rax, %rdx
	jmp	.L767
	.p2align 4,,10
	.p2align 3
.L775:
	movq	8(%r13), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder20ChangeUint64ToBigIntEv@PLT
	movq	%rax, %rdx
	jmp	.L767
.L834:
	movq	8(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder38ChangeCompressedPointerToTaggedPointerEv@PLT
	movq	%rax, %rdx
	jmp	.L767
.L845:
	cmpb	$7, 28(%rbp)
	jne	.L834
	cmpq	$134217729, -72(%rbp)
	je	.L834
	movl	$134217729, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L834
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21RepresentationChanger44InsertChangeCompressedPointerToTaggedPointerEPNS1_4NodeE
.L833:
	movq	%rax, %r12
	jmp	.L832
.L844:
	call	_ZN2v88internal8compiler10BitsetType11SignedSmallEv@PLT
	movq	%r15, %rdi
	movl	%eax, %esi
	orl	$1, %esi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	je	.L759
	movq	8(%r13), %rax
	leaq	32(%rbp), %rsi
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder28CheckedTaggedToTaggedPointerERKNS1_14FeedbackSourceE@PLT
	movq	%rax, %rdx
	jmp	.L767
.L846:
	cmpq	$134217729, -72(%rbp)
	je	.L834
	movl	$134217729, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L834
	cmpb	$7, 28(%rbp)
	jne	.L800
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21RepresentationChanger30InsertChangeCompressedToTaggedEPNS1_4NodeE
	jmp	.L833
.L838:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22748:
	.size	_ZN2v88internal8compiler21RepresentationChanger33GetTaggedPointerRepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE, .-_ZN2v88internal8compiler21RepresentationChanger33GetTaggedPointerRepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE
	.section	.text._ZN2v88internal8compiler21RepresentationChanger37GetCompressedPointerRepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21RepresentationChanger37GetCompressedPointerRepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE
	.type	_ZN2v88internal8compiler21RepresentationChanger37GetCompressedPointerRepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE, @function
_ZN2v88internal8compiler21RepresentationChanger37GetCompressedPointerRepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE:
.LFB22751:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$40, %rsp
	movq	%rcx, -72(%rbp)
	movq	%fs:40, %rdx
	movq	%rdx, -56(%rbp)
	xorl	%edx, %edx
	cmpq	$1, %rcx
	jne	.L869
.L848:
	movq	8(%r13), %rax
	movl	$10, %esi
	movq	8(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9DeadValueENS0_21MachineRepresentationE@PLT
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r12, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
.L847:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L870
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L869:
	.cfi_restore_state
	leaq	-72(%rbp), %r15
	movl	$1, %esi
	movq	%r8, %r14
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L848
	cmpb	$7, %bl
	je	.L867
	leal	-6(%rbx), %eax
	testb	$-3, %al
	jne	.L853
	cmpb	$6, 28(%rbp)
	je	.L854
.L853:
	cmpb	$1, %bl
	je	.L871
	leal	-2(%rbx), %eax
	cmpb	$2, %al
	jbe	.L872
	cmpb	$5, %bl
	je	.L873
	cmpb	$12, %bl
	je	.L874
	cmpb	$13, %bl
	je	.L875
	movq	-72(%rbp), %rcx
	movl	$10, %r8d
	movl	%ebx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21RepresentationChanger9TypeErrorEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES5_
	jmp	.L847
	.p2align 4,,10
	.p2align 3
.L875:
	pushq	40(%rbp)
	movq	-72(%rbp), %rcx
	movq	%r14, %r8
	movl	$13, %edx
	pushq	32(%rbp)
	pushq	24(%rbp)
	pushq	16(%rbp)
	.p2align 4,,10
	.p2align 3
.L868:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21RepresentationChanger33GetTaggedPointerRepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE
	addq	$32, %rsp
	movq	%rax, %r12
.L867:
	movq	8(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder38ChangeTaggedPointerToCompressedPointerEv@PLT
	movq	%rax, %rdx
.L852:
	movq	%r14, %rcx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21RepresentationChanger16InsertConversionEPNS1_4NodeEPKNS1_8OperatorES4_
	jmp	.L847
	.p2align 4,,10
	.p2align 3
.L872:
	pushq	40(%rbp)
	movq	-72(%rbp), %rcx
	movq	%r14, %r8
	movl	%ebx, %edx
	pushq	32(%rbp)
	pushq	24(%rbp)
	pushq	16(%rbp)
	jmp	.L868
	.p2align 4,,10
	.p2align 3
.L871:
	pushq	40(%rbp)
	movq	-72(%rbp), %rcx
	movq	%r14, %r8
	movl	$1, %edx
	pushq	32(%rbp)
	pushq	24(%rbp)
	pushq	16(%rbp)
	jmp	.L868
	.p2align 4,,10
	.p2align 3
.L854:
	call	_ZN2v88internal8compiler10BitsetType11SignedSmallEv@PLT
	movq	%r15, %rdi
	movl	%eax, %esi
	orl	$1, %esi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	je	.L867
	movq	8(%r13), %rax
	leaq	32(%rbp), %rsi
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder32CheckedTaggedToCompressedPointerERKNS1_14FeedbackSourceE@PLT
	movq	%rax, %rdx
	jmp	.L852
	.p2align 4,,10
	.p2align 3
.L873:
	pushq	40(%rbp)
	movq	-72(%rbp), %rcx
	movq	%r14, %r8
	movl	$5, %edx
	pushq	32(%rbp)
	pushq	24(%rbp)
	pushq	16(%rbp)
	jmp	.L868
	.p2align 4,,10
	.p2align 3
.L874:
	pushq	40(%rbp)
	movq	-72(%rbp), %rcx
	movq	%r14, %r8
	movl	$12, %edx
	pushq	32(%rbp)
	pushq	24(%rbp)
	pushq	16(%rbp)
	jmp	.L868
.L870:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22751:
	.size	_ZN2v88internal8compiler21RepresentationChanger37GetCompressedPointerRepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE, .-_ZN2v88internal8compiler21RepresentationChanger37GetCompressedPointerRepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE
	.section	.rodata._ZN2v88internal8compiler21RepresentationChanger26GetWord64RepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE.str1.1,"aMS",@progbits,1
.LC34:
	.string	"IsHeapObject()"
	.section	.rodata._ZN2v88internal8compiler21RepresentationChanger26GetWord64RepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE.str1.8,"aMS",@progbits,1
	.align 8
.LC35:
	.string	"output_type.Maybe(Type::MinusZero()) implies use_info.truncation().IdentifiesZeroAndMinusZero()"
	.section	.text._ZN2v88internal8compiler21RepresentationChanger26GetWord64RepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21RepresentationChanger26GetWord64RepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE
	.type	_ZN2v88internal8compiler21RepresentationChanger26GetWord64RepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE, @function
_ZN2v88internal8compiler21RepresentationChanger26GetWord64RepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE:
.LFB22760:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$88, %rsp
	movq	%rcx, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movzwl	16(%rax), %edx
	cmpw	$28, %dx
	je	.L877
	jbe	.L968
	cmpw	$30, %dx
	jne	.L879
	movq	48(%rax), %rax
	movq	16(%rdi), %rsi
	leaq	-96(%rbp), %r14
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	movq	%rax, %rdx
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L888
	movdqa	-96(%rbp), %xmm2
	leaq	-80(%rbp), %rdi
	movq	%rdi, -120(%rbp)
	movaps	%xmm2, -80(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef8IsBigIntEv@PLT
	movq	-120(%rbp), %rdi
	testb	%al, %al
	je	.L879
	movq	-112(%rbp), %rdx
	movq	16(%r15), %rsi
	xorl	%ecx, %ecx
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	-112(%rbp), %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L888
	movq	-112(%rbp), %rdi
	call	_ZNK2v88internal8compiler9ObjectRef8AsBigIntEv@PLT
	movq	8(%r15), %r12
	movq	%r14, %rdi
	movq	%rax, -96(%rbp)
	movq	%rdx, -88(%rbp)
	call	_ZNK2v88internal8compiler9BigIntRef8AsUint64Ev@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler12MachineGraph13Int64ConstantEl@PLT
	jmp	.L876
	.p2align 4,,10
	.p2align 3
.L968:
	subl	$23, %edx
	cmpw	$3, %dx
	jbe	.L880
.L879:
	cmpq	$1, -104(%rbp)
	jne	.L969
.L889:
	movq	8(%r15), %rax
	movl	$5, %esi
	movq	8(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9DeadValueENS0_21MachineRepresentationE@PLT
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r12, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
.L876:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L970
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L969:
	.cfi_restore_state
	leaq	-104(%rbp), %r14
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L889
	cmpb	$1, %bl
	je	.L971
	leal	-2(%rbx), %eax
	cmpb	$2, %al
	ja	.L895
	cmpq	$3079, -104(%rbp)
	jne	.L972
.L896:
	movl	$2049, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L973
.L899:
	movq	8(%r15), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder20ChangeUint32ToUint64Ev@PLT
	movq	%rax, %rdx
.L901:
	movq	%r13, %rcx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21RepresentationChanger16InsertConversionEPNS1_4NodeEPKNS1_8OperatorES4_
	jmp	.L876
	.p2align 4,,10
	.p2align 3
.L877:
	movsd	48(%rax), %xmm0
	movsd	.LC32(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L879
	comisd	.LC33(%rip), %xmm0
	jb	.L879
	cvttsd2siq	%xmm0, %rsi
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%rsi, %xmm1
	ucomisd	%xmm0, %xmm1
	jp	.L879
	jne	.L879
	movq	8(%rdi), %rdi
	call	_ZN2v88internal8compiler12MachineGraph13Int64ConstantEl@PLT
	jmp	.L876
	.p2align 4,,10
	.p2align 3
.L971:
	cmpq	$513, -104(%rbp)
	jne	.L974
.L892:
	movzbl	28(%rbp), %eax
	testb	%al, %al
	je	.L975
	cmpb	$5, %al
	je	.L976
	movl	$25, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21RepresentationChanger24InsertUnconditionalDeoptEPNS1_4NodeENS0_16DeoptimizeReasonE
	movl	$5, %esi
	movq	%rax, %rbx
	movq	8(%r15), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9DeadValueENS0_21MachineRepresentationE@PLT
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%rbx, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	jmp	.L876
	.p2align 4,,10
	.p2align 3
.L895:
	cmpb	$12, %bl
	je	.L977
	cmpb	$13, %bl
	je	.L978
	cmpb	$6, %bl
	je	.L979
	leal	-7(%rbx), %eax
	cmpb	$1, %al
	jbe	.L980
	cmpb	$11, %bl
	je	.L981
	cmpb	$9, %bl
	je	.L982
	cmpb	$10, %bl
	je	.L983
.L930:
	movq	-104(%rbp), %rcx
	movl	$5, %r8d
	movl	%ebx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21RepresentationChanger9TypeErrorEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES5_
	jmp	.L876
	.p2align 4,,10
	.p2align 3
.L974:
	movl	$513, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L892
	leaq	.LC24(%rip), %rsi
	leaq	.LC25(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L972:
	movl	$3079, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L896
	cmpq	$3147, -104(%rbp)
	je	.L897
	movl	$3147, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L930
.L897:
	movl	$2049, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L984
.L902:
	movq	8(%r15), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder18ChangeInt32ToInt64Ev@PLT
	movq	%rax, %rdx
	jmp	.L901
	.p2align 4,,10
	.p2align 3
.L977:
	movq	(%r15), %rax
	movq	152(%rax), %rsi
	cmpq	-104(%rbp), %rsi
	je	.L904
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L904
	movq	(%r15), %rax
	movq	160(%rax), %rsi
	cmpq	-104(%rbp), %rsi
	je	.L905
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L905
	cmpb	$3, 28(%rbp)
	je	.L985
	movq	-104(%rbp), %rcx
	movl	$5, %r8d
	movq	%r12, %rsi
	movq	%r15, %rdi
	movl	$12, %edx
	call	_ZN2v88internal8compiler21RepresentationChanger9TypeErrorEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES5_
	jmp	.L876
	.p2align 4,,10
	.p2align 3
.L888:
	leaq	.LC34(%rip), %rsi
	leaq	.LC25(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L904:
	movq	8(%r15), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler22MachineOperatorBuilder22ChangeFloat32ToFloat64Ev@PLT
	movl	$1, %edx
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movq	%rax, %rsi
	leaq	-64(%rbp), %rcx
	movq	%r12, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r12
	movq	8(%r15), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder20ChangeFloat64ToInt64Ev@PLT
	movq	%rax, %rdx
	jmp	.L901
	.p2align 4,,10
	.p2align 3
.L978:
	movq	(%r15), %rax
	movq	152(%rax), %rsi
	cmpq	-104(%rbp), %rsi
	je	.L911
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L911
	movq	(%r15), %rax
	movq	160(%rax), %rsi
	cmpq	-104(%rbp), %rsi
	je	.L912
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L912
	cmpb	$3, 28(%rbp)
	je	.L914
	movq	-104(%rbp), %rcx
	movl	$5, %r8d
	movq	%r12, %rsi
	movq	%r15, %rdi
	movl	$13, %edx
	call	_ZN2v88internal8compiler21RepresentationChanger9TypeErrorEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES5_
	jmp	.L876
	.p2align 4,,10
	.p2align 3
.L911:
	movq	8(%r15), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder20ChangeFloat64ToInt64Ev@PLT
	movq	%rax, %rdx
	jmp	.L901
	.p2align 4,,10
	.p2align 3
.L980:
	movzbl	20(%rbp), %eax
	cmpb	$3, %al
	ja	.L920
	cmpb	$1, %al
	je	.L922
	cmpb	$7, 28(%rbp)
	je	.L924
	cmpq	$134217729, -104(%rbp)
	je	.L924
	movl	$134217729, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L922
.L924:
	pushq	40(%rbp)
	movq	-104(%rbp), %rcx
	movl	%ebx, %edx
	movq	%r12, %rsi
	pushq	32(%rbp)
	movq	%r13, %r8
	movq	%r15, %rdi
	pushq	24(%rbp)
	pushq	16(%rbp)
	call	_ZN2v88internal8compiler21RepresentationChanger33GetTaggedPointerRepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE
	addq	$32, %rsp
	movq	%rax, %r12
	movq	8(%r15), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder22TruncateBigIntToUint64Ev@PLT
	movq	%rax, %rdx
	jmp	.L901
.L920:
	subl	$4, %eax
	cmpb	$1, %al
	ja	.L880
.L922:
	movq	(%r15), %rax
	movq	152(%rax), %rsi
	cmpq	-104(%rbp), %rsi
	je	.L925
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L925
	cmpb	$3, 28(%rbp)
	jne	.L930
	movq	8(%r15), %rax
	movl	$2049, %esi
	movq	%r14, %rdi
	movq	376(%rax), %rbx
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	movl	$1, %esi
	testb	%al, %al
	je	.L928
	cmpl	$0, 24(%rbp)
	sete	%sil
.L928:
	leaq	32(%rbp), %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder20CheckedTaggedToInt64ENS1_21CheckForMinusZeroModeERKNS1_14FeedbackSourceE@PLT
	movq	%rax, %rdx
	jmp	.L901
.L975:
	leaq	.LC26(%rip), %rsi
	leaq	.LC25(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L925:
	movq	8(%r15), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder19ChangeTaggedToInt64Ev@PLT
	movq	%rax, %rdx
	jmp	.L901
.L979:
	call	_ZN2v88internal8compiler10BitsetType11SignedSmallEv@PLT
	movl	%eax, %esi
	orl	$1, %esi
	cmpq	%rsi, -104(%rbp)
	je	.L918
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L918
	movq	-104(%rbp), %rcx
	movl	$5, %r8d
	movq	%r12, %rsi
	movq	%r15, %rdi
	movl	$6, %edx
	call	_ZN2v88internal8compiler21RepresentationChanger9TypeErrorEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES5_
	jmp	.L876
.L976:
	leaq	.LC29(%rip), %rsi
	leaq	.LC25(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L973:
	movl	24(%rbp), %edx
	testl	%edx, %edx
	je	.L899
.L900:
	leaq	.LC35(%rip), %rsi
	leaq	.LC25(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L918:
	movq	8(%r15), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder25ChangeTaggedSignedToInt64Ev@PLT
	movq	%rax, %rdx
	jmp	.L901
.L905:
	movq	8(%r15), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler22MachineOperatorBuilder22ChangeFloat32ToFloat64Ev@PLT
	movl	$1, %edx
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movq	%rax, %rsi
	leaq	-64(%rbp), %rcx
	movq	%r12, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r12
	movq	8(%r15), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder21ChangeFloat64ToUint64Ev@PLT
	movq	%rax, %rdx
	jmp	.L901
.L912:
	movq	8(%r15), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder21ChangeFloat64ToUint64Ev@PLT
	movq	%rax, %rdx
	jmp	.L901
.L983:
	movq	8(%r15), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler22MachineOperatorBuilder38ChangeCompressedPointerToTaggedPointerEv@PLT
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r12, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	pushq	40(%rbp)
	movq	-104(%rbp), %rcx
	movq	%r13, %r8
	pushq	32(%rbp)
	movq	%rax, %rsi
	movl	$7, %edx
	movq	%r15, %rdi
	pushq	24(%rbp)
	pushq	16(%rbp)
	call	_ZN2v88internal8compiler21RepresentationChanger26GetWord64RepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE
	addq	$32, %rsp
	jmp	.L876
.L981:
	movq	8(%r15), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler22MachineOperatorBuilder24ChangeCompressedToTaggedEv@PLT
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r12, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	pushq	40(%rbp)
	movq	-104(%rbp), %rcx
	movq	%r13, %r8
	pushq	32(%rbp)
	movq	%rax, %rsi
	movl	$8, %edx
	movq	%r15, %rdi
	pushq	24(%rbp)
	pushq	16(%rbp)
	call	_ZN2v88internal8compiler21RepresentationChanger26GetWord64RepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE
	addq	$32, %rsp
	jmp	.L876
.L982:
	movq	8(%r15), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler22MachineOperatorBuilder36ChangeCompressedSignedToTaggedSignedEv@PLT
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r12, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	pushq	40(%rbp)
	movq	-104(%rbp), %rcx
	movq	%r13, %r8
	pushq	32(%rbp)
	movq	%rax, %rsi
	movl	$6, %edx
	movq	%r15, %rdi
	pushq	24(%rbp)
	pushq	16(%rbp)
	call	_ZN2v88internal8compiler21RepresentationChanger26GetWord64RepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE
	addq	$32, %rsp
	jmp	.L876
.L984:
	movl	24(%rbp), %eax
	testl	%eax, %eax
	je	.L902
	jmp	.L900
.L985:
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21RepresentationChanger28InsertChangeFloat32ToFloat64EPNS1_4NodeE
	movq	%rax, %r12
.L914:
	movq	8(%r15), %rax
	movl	$2049, %esi
	movq	%r14, %rdi
	movq	376(%rax), %rbx
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	movl	$1, %esi
	testb	%al, %al
	je	.L916
	cmpl	$0, 24(%rbp)
	sete	%sil
.L916:
	leaq	32(%rbp), %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder21CheckedFloat64ToInt64ENS1_21CheckForMinusZeroModeERKNS1_14FeedbackSourceE@PLT
	movq	%rax, %rdx
	jmp	.L901
.L880:
	leaq	.LC8(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L970:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22760:
	.size	_ZN2v88internal8compiler21RepresentationChanger26GetWord64RepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE, .-_ZN2v88internal8compiler21RepresentationChanger26GetWord64RepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE
	.section	.text._ZN2v88internal8compiler21RepresentationChanger27InsertCheckedFloat64ToInt32EPNS1_4NodeENS1_21CheckForMinusZeroModeERKNS1_14FeedbackSourceES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21RepresentationChanger27InsertCheckedFloat64ToInt32EPNS1_4NodeENS1_21CheckForMinusZeroModeERKNS1_14FeedbackSourceES4_
	.type	_ZN2v88internal8compiler21RepresentationChanger27InsertCheckedFloat64ToInt32EPNS1_4NodeENS1_21CheckForMinusZeroModeERKNS1_14FeedbackSourceES4_, @function
_ZN2v88internal8compiler21RepresentationChanger27InsertCheckedFloat64ToInt32EPNS1_4NodeENS1_21CheckForMinusZeroModeERKNS1_14FeedbackSourceES4_:
.LFB22781:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	movl	%edx, %esi
	movq	%rcx, %rdx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder21CheckedFloat64ToInt32ENS1_21CheckForMinusZeroModeERKNS1_14FeedbackSourceE@PLT
	movq	%rax, %r12
	movl	28(%rax), %eax
	testl	%eax, %eax
	jg	.L991
	movq	8(%rbx), %rax
	movq	%r12, %rsi
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	(%rax), %rdi
	movq	%r13, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r12
.L986:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L992
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L991:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	8(%rbx), %rdx
	movq	%r12, %rsi
	leaq	-64(%rbp), %rcx
	movq	%r13, %xmm0
	xorl	%r8d, %r8d
	movq	(%rdx), %rdi
	movhps	-72(%rbp), %xmm0
	movl	$3, %edx
	movq	%rax, -48(%rbp)
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler14NodeProperties18ReplaceEffectInputEPNS1_4NodeES4_i@PLT
	jmp	.L986
.L992:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22781:
	.size	_ZN2v88internal8compiler21RepresentationChanger27InsertCheckedFloat64ToInt32EPNS1_4NodeENS1_21CheckForMinusZeroModeERKNS1_14FeedbackSourceES4_, .-_ZN2v88internal8compiler21RepresentationChanger27InsertCheckedFloat64ToInt32EPNS1_4NodeENS1_21CheckForMinusZeroModeERKNS1_14FeedbackSourceES4_
	.section	.text._ZN2v88internal8compiler21RepresentationChanger32GetTaggedSignedRepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21RepresentationChanger32GetTaggedSignedRepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE
	.type	_ZN2v88internal8compiler21RepresentationChanger32GetTaggedSignedRepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE, @function
_ZN2v88internal8compiler21RepresentationChanger32GetTaggedSignedRepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE:
.LFB22747:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$40, %rsp
	movq	%rcx, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	cmpw	$28, 16(%rax)
	je	.L994
.L1000:
	cmpq	$1, -72(%rbp)
	jne	.L1057
.L995:
	movq	8(%r13), %rax
	movl	$6, %esi
	movq	8(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9DeadValueENS0_21MachineRepresentationE@PLT
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r12, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
.L993:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1058
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1057:
	.cfi_restore_state
	leaq	-72(%rbp), %r15
	movl	$1, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L995
	leal	-2(%rbx), %eax
	cmpb	$2, %al
	ja	.L1059
	cmpq	$1089, -72(%rbp)
	je	.L1049
	movl	$1089, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1049
	cmpq	$1099, -72(%rbp)
	je	.L1054
	movl	$1099, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1054
	cmpq	$1031, -72(%rbp)
	je	.L1010
	movl	$1031, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L1037
.L1010:
	cmpb	$1, 28(%rbp)
	je	.L1055
.L1037:
	movq	-72(%rbp), %rcx
	movl	$6, %r8d
	movl	%ebx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21RepresentationChanger9TypeErrorEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES5_
	jmp	.L993
	.p2align 4,,10
	.p2align 3
.L994:
	call	_ZN2v88internal8compiler10BitsetType11SignedSmallEv@PLT
	movl	%eax, %esi
	orl	$1, %esi
	cmpq	%rsi, -72(%rbp)
	jne	.L997
.L999:
	movq	%r12, %rax
	jmp	.L993
	.p2align 4,,10
	.p2align 3
.L1059:
	cmpb	$5, %bl
	je	.L1060
	cmpb	$13, %bl
	je	.L1061
	cmpb	$12, %bl
	je	.L1062
	leal	-7(%rbx), %eax
	cmpb	$1, %al
	jbe	.L1063
	cmpb	$1, %bl
	je	.L1064
	cmpb	$9, %bl
	je	.L1065
	leal	-10(%rbx), %eax
	cmpb	$1, %al
	ja	.L1037
	cmpb	$1, 28(%rbp)
	je	.L1066
	call	_ZN2v88internal8compiler10BitsetType11SignedSmallEv@PLT
	orl	$1, %eax
	cmpq	%rax, -72(%rbp)
	je	.L1039
	movq	%rax, %rsi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L1037
.L1039:
	movq	8(%r13), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder30ChangeCompressedToTaggedSignedEv@PLT
	movq	%rax, %rdx
	jmp	.L1006
	.p2align 4,,10
	.p2align 3
.L997:
	leaq	-72(%rbp), %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L999
	jmp	.L1000
	.p2align 4,,10
	.p2align 3
.L1012:
	movq	8(%r13), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler22MachineOperatorBuilder20TruncateInt64ToInt32Ev@PLT
.L1050:
	movq	%rax, %rsi
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%r15, %rdi
	movq	%r12, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r12
.L1049:
	movq	8(%r13), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder25ChangeInt31ToTaggedSignedEv@PLT
	movq	%rax, %rdx
.L1006:
	movq	%r14, %rcx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21RepresentationChanger16InsertConversionEPNS1_4NodeEPKNS1_8OperatorES4_
	jmp	.L993
	.p2align 4,,10
	.p2align 3
.L1061:
	cmpq	$1089, -72(%rbp)
	je	.L1021
	movl	$1089, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1021
	cmpq	$1099, -72(%rbp)
	je	.L1022
	movl	$1099, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1022
	cmpq	$1031, -72(%rbp)
	je	.L1027
	movl	$1031, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1027
	cmpb	$1, 28(%rbp)
	jne	.L1026
.L1051:
	movl	$2049, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	leaq	32(%rbp), %rcx
	movq	%r14, %r8
	movq	%r12, %rsi
	xorl	$1, %eax
	movq	%r13, %rdi
	movl	%eax, %edx
	call	_ZN2v88internal8compiler21RepresentationChanger27InsertCheckedFloat64ToInt32EPNS1_4NodeENS1_21CheckForMinusZeroModeERKNS1_14FeedbackSourceES4_
.L1052:
	movq	%rax, %r12
.L1054:
	movq	8(%r13), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder19ChangeInt32ToTaggedEv@PLT
	movq	%rax, %rdx
	jmp	.L1006
	.p2align 4,,10
	.p2align 3
.L1060:
	cmpq	$1089, -72(%rbp)
	je	.L1012
	movl	$1089, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1012
	cmpq	$1099, -72(%rbp)
	je	.L1013
	movl	$1099, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1013
	cmpb	$1, 28(%rbp)
	jne	.L1045
	movq	0(%r13), %rax
	movq	408(%rax), %rsi
	cmpq	-72(%rbp), %rsi
	je	.L1017
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1017
	movq	0(%r13), %rax
	movq	384(%rax), %rsi
	cmpq	-72(%rbp), %rsi
	je	.L1018
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L1045
.L1018:
	movq	8(%r13), %rax
	leaq	32(%rbp), %rsi
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder26CheckedInt64ToTaggedSignedERKNS1_14FeedbackSourceE@PLT
	movq	%rax, %rdx
	jmp	.L1006
	.p2align 4,,10
	.p2align 3
.L1021:
	movq	8(%r13), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler22MachineOperatorBuilder20ChangeFloat64ToInt32Ev@PLT
	jmp	.L1050
	.p2align 4,,10
	.p2align 3
.L1062:
	cmpb	$1, 28(%rbp)
	je	.L1067
	movq	-72(%rbp), %rcx
	movl	$6, %r8d
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	$12, %edx
	call	_ZN2v88internal8compiler21RepresentationChanger9TypeErrorEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES5_
	jmp	.L993
	.p2align 4,,10
	.p2align 3
.L1063:
	cmpb	$1, 28(%rbp)
	je	.L1053
	call	_ZN2v88internal8compiler10BitsetType11SignedSmallEv@PLT
	orl	$1, %eax
	cmpq	%rax, -72(%rbp)
	je	.L1033
	movq	%rax, %rsi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L1037
.L1033:
	movq	8(%r13), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder26ChangeTaggedToTaggedSignedEv@PLT
	movq	%rax, %rdx
	jmp	.L1006
.L1013:
	movq	8(%r13), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler22MachineOperatorBuilder20TruncateInt64ToInt32Ev@PLT
.L1056:
	movq	%rax, %rsi
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%r15, %rdi
	movq	%r12, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	jmp	.L1052
.L1068:
	movq	8(%r13), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder17ChangeBitToTaggedEv@PLT
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r12, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r12
.L1053:
	movq	8(%r13), %rax
	leaq	32(%rbp), %rsi
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder27CheckedTaggedToTaggedSignedERKNS1_14FeedbackSourceE@PLT
	movq	%rax, %rdx
	jmp	.L1006
.L1064:
	cmpb	$1, 28(%rbp)
	je	.L1068
	movq	-72(%rbp), %rcx
	movl	$6, %r8d
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	$1, %edx
	call	_ZN2v88internal8compiler21RepresentationChanger9TypeErrorEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES5_
	jmp	.L993
.L1067:
	movq	8(%r13), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler22MachineOperatorBuilder22ChangeFloat32ToFloat64Ev@PLT
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%rbx, %rdi
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r12, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r12
	jmp	.L1051
.L1022:
	movq	8(%r13), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler22MachineOperatorBuilder20ChangeFloat64ToInt32Ev@PLT
	jmp	.L1056
.L1065:
	movq	8(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder36ChangeCompressedSignedToTaggedSignedEv@PLT
	movq	%rax, %rdx
	jmp	.L1006
.L1066:
	movq	8(%r13), %rax
	leaq	32(%rbp), %rsi
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder31CheckedCompressedToTaggedSignedERKNS1_14FeedbackSourceE@PLT
	movq	%rax, %rdx
	jmp	.L1006
.L1069:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21RepresentationChanger27InsertChangeFloat64ToUint32EPNS1_4NodeE
	movq	%rax, %r12
.L1055:
	movq	8(%r13), %rax
	leaq	32(%rbp), %rsi
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder27CheckedUint32ToTaggedSignedERKNS1_14FeedbackSourceE@PLT
	movq	%rax, %rdx
	jmp	.L1006
.L1045:
	movq	-72(%rbp), %rcx
	movl	$6, %r8d
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	$5, %edx
	call	_ZN2v88internal8compiler21RepresentationChanger9TypeErrorEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES5_
	jmp	.L993
.L1027:
	cmpb	$1, 28(%rbp)
	je	.L1069
.L1026:
	movq	-72(%rbp), %rcx
	movl	$6, %r8d
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	$13, %edx
	call	_ZN2v88internal8compiler21RepresentationChanger9TypeErrorEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES5_
	jmp	.L993
.L1017:
	movq	8(%r13), %rax
	leaq	32(%rbp), %rsi
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder27CheckedUint64ToTaggedSignedERKNS1_14FeedbackSourceE@PLT
	movq	%rax, %rdx
	jmp	.L1006
.L1058:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22747:
	.size	_ZN2v88internal8compiler21RepresentationChanger32GetTaggedSignedRepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE, .-_ZN2v88internal8compiler21RepresentationChanger32GetTaggedSignedRepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE
	.section	.text._ZN2v88internal8compiler21RepresentationChanger36GetCompressedSignedRepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21RepresentationChanger36GetCompressedSignedRepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE
	.type	_ZN2v88internal8compiler21RepresentationChanger36GetCompressedSignedRepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE, @function
_ZN2v88internal8compiler21RepresentationChanger36GetCompressedSignedRepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE:
.LFB22750:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$40, %rsp
	movq	%rcx, -72(%rbp)
	movq	%fs:40, %rdx
	movq	%rdx, -56(%rbp)
	xorl	%edx, %edx
	cmpq	$1, %rcx
	jne	.L1106
.L1071:
	movq	8(%r12), %rax
	movl	$9, %esi
	movq	8(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9DeadValueENS0_21MachineRepresentationE@PLT
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r13, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
.L1070:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1107
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1106:
	.cfi_restore_state
	leaq	-72(%rbp), %r15
	movl	$1, %esi
	movq	%r8, %r14
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1071
	cmpb	$6, %bl
	je	.L1104
	leal	-7(%rbx), %eax
	cmpb	$1, %al
	jbe	.L1108
	cmpb	$1, %bl
	je	.L1109
	leal	-2(%rbx), %eax
	cmpb	$2, %al
	jbe	.L1110
	cmpb	$5, %bl
	je	.L1111
	cmpb	$12, %bl
	je	.L1112
	cmpb	$13, %bl
	jne	.L1084
	cmpq	$1089, -72(%rbp)
	je	.L1089
	movl	$1089, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1089
	cmpq	$1099, -72(%rbp)
	je	.L1090
	movl	$1099, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1090
	cmpb	$1, 28(%rbp)
	je	.L1092
	pushq	40(%rbp)
	movq	-72(%rbp), %rcx
	movq	%r14, %r8
	movl	$13, %edx
	pushq	32(%rbp)
	pushq	24(%rbp)
	pushq	16(%rbp)
	.p2align 4,,10
	.p2align 3
.L1101:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler21RepresentationChanger32GetTaggedSignedRepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE
	addq	$32, %rsp
	movq	%rax, %r13
.L1104:
	movq	8(%r12), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder36ChangeTaggedSignedToCompressedSignedEv@PLT
	movq	%rax, %rdx
.L1075:
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler21RepresentationChanger16InsertConversionEPNS1_4NodeEPKNS1_8OperatorES4_
	jmp	.L1070
	.p2align 4,,10
	.p2align 3
.L1108:
	cmpb	$1, 28(%rbp)
	je	.L1113
	call	_ZN2v88internal8compiler10BitsetType11SignedSmallEv@PLT
	movl	%eax, %esi
	orl	$1, %esi
	cmpq	%rsi, -72(%rbp)
	jne	.L1114
.L1078:
	movq	8(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder30ChangeTaggedToCompressedSignedEv@PLT
	movq	%rax, %rdx
	jmp	.L1075
	.p2align 4,,10
	.p2align 3
.L1110:
	cmpq	$1089, -72(%rbp)
	je	.L1103
	movl	$1089, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1103
	cmpq	$1099, -72(%rbp)
	je	.L1082
	movl	$1099, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1082
	pushq	40(%rbp)
	movq	-72(%rbp), %rcx
	movq	%r14, %r8
	movl	%ebx, %edx
	pushq	32(%rbp)
	pushq	24(%rbp)
	pushq	16(%rbp)
	jmp	.L1101
	.p2align 4,,10
	.p2align 3
.L1114:
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	movl	$6, %r8d
	testb	%al, %al
	jne	.L1078
.L1105:
	movq	-72(%rbp), %rcx
	movl	%ebx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler21RepresentationChanger9TypeErrorEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES5_
	jmp	.L1070
.L1089:
	movq	8(%r12), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler22MachineOperatorBuilder20ChangeFloat64ToInt32Ev@PLT
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r13, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r13
.L1103:
	movq	8(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder29ChangeInt31ToCompressedSignedEv@PLT
	movq	%rax, %rdx
	jmp	.L1075
	.p2align 4,,10
	.p2align 3
.L1113:
	movq	8(%r12), %rax
	leaq	32(%rbp), %rsi
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder31CheckedTaggedToCompressedSignedERKNS1_14FeedbackSourceE@PLT
	movq	%rax, %rdx
	jmp	.L1075
	.p2align 4,,10
	.p2align 3
.L1109:
	pushq	40(%rbp)
	movq	-72(%rbp), %rcx
	movq	%r14, %r8
	movl	$1, %edx
	pushq	32(%rbp)
	pushq	24(%rbp)
	pushq	16(%rbp)
	jmp	.L1101
	.p2align 4,,10
	.p2align 3
.L1084:
	movl	$9, %r8d
	jmp	.L1105
	.p2align 4,,10
	.p2align 3
.L1112:
	cmpb	$1, 28(%rbp)
	je	.L1115
	movq	-72(%rbp), %rcx
	movl	$9, %r8d
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$12, %edx
	call	_ZN2v88internal8compiler21RepresentationChanger9TypeErrorEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES5_
	jmp	.L1070
.L1111:
	pushq	40(%rbp)
	movq	-72(%rbp), %rcx
	movq	%r14, %r8
	movl	$5, %edx
	pushq	32(%rbp)
	pushq	24(%rbp)
	pushq	16(%rbp)
	jmp	.L1101
.L1082:
	cmpb	$1, 28(%rbp)
	jne	.L1084
.L1102:
	movq	8(%r12), %rax
	leaq	32(%rbp), %rsi
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder30CheckedInt32ToCompressedSignedERKNS1_14FeedbackSourceE@PLT
	movq	%rax, %rdx
	jmp	.L1075
.L1115:
	movq	8(%r12), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler22MachineOperatorBuilder22ChangeFloat32ToFloat64Ev@PLT
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%rbx, %rdi
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r13, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r13
.L1092:
	movq	%r15, %rdi
	movl	$2049, %esi
	leaq	32(%rbp), %r15
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	movq	%r13, %rsi
	movq	%r14, %r8
	movq	%r15, %rcx
	xorl	$1, %eax
	movq	%r12, %rdi
	movl	%eax, %edx
	call	_ZN2v88internal8compiler21RepresentationChanger27InsertCheckedFloat64ToInt32EPNS1_4NodeENS1_21CheckForMinusZeroModeERKNS1_14FeedbackSourceES4_
	movq	%r15, %rsi
	movq	%rax, %r13
	movq	8(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder30CheckedInt32ToCompressedSignedERKNS1_14FeedbackSourceE@PLT
	movq	%rax, %rdx
	jmp	.L1075
.L1090:
	movq	8(%r12), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler22MachineOperatorBuilder20ChangeFloat64ToInt32Ev@PLT
	xorl	%r8d, %r8d
	leaq	-64(%rbp), %rcx
	movq	%r15, %rdi
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r13, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	cmpb	$1, 28(%rbp)
	movq	%rax, %r13
	je	.L1102
	movq	-72(%rbp), %rcx
	movl	$9, %r8d
	movq	%rax, %rsi
	movq	%r12, %rdi
	movl	$13, %edx
	call	_ZN2v88internal8compiler21RepresentationChanger9TypeErrorEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES5_
	jmp	.L1070
.L1107:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22750:
	.size	_ZN2v88internal8compiler21RepresentationChanger36GetCompressedSignedRepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE, .-_ZN2v88internal8compiler21RepresentationChanger36GetCompressedSignedRepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE
	.section	.text._ZN2v88internal8compiler21RepresentationChanger20GetRepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21RepresentationChanger20GetRepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE
	.type	_ZN2v88internal8compiler21RepresentationChanger20GetRepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE, @function
_ZN2v88internal8compiler21RepresentationChanger20GetRepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE:
.LFB22746:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movzbl	16(%rbp), %eax
	testb	%dl, %dl
	jne	.L1117
	cmpq	$1, %rcx
	jne	.L1145
.L1118:
	cmpb	%dl, %al
	je	.L1116
.L1146:
	leal	-2(%rax), %r9d
	cmpb	$2, %r9b
	ja	.L1119
	leal	-2(%rdx), %r9d
	cmpb	$2, %r9b
	jbe	.L1116
.L1119:
	movzbl	20(%rbp), %r11d
	movl	24(%rbp), %r10d
	cmpb	$14, %al
	ja	.L1122
	leaq	.L1123(%rip), %r9
	movslq	(%r9,%rax,4), %rax
	addq	%r9, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler21RepresentationChanger20GetRepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE,"a",@progbits
	.align 4
	.align 4
.L1123:
	.long	.L1116-.L1123
	.long	.L1134-.L1123
	.long	.L1133-.L1123
	.long	.L1133-.L1123
	.long	.L1133-.L1123
	.long	.L1132-.L1123
	.long	.L1131-.L1123
	.long	.L1130-.L1123
	.long	.L1129-.L1123
	.long	.L1128-.L1123
	.long	.L1127-.L1123
	.long	.L1126-.L1123
	.long	.L1125-.L1123
	.long	.L1124-.L1123
	.long	.L1116-.L1123
	.section	.text._ZN2v88internal8compiler21RepresentationChanger20GetRepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE
	.p2align 4,,10
	.p2align 3
.L1145:
	movl	%eax, %r8d
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler21RepresentationChanger9TypeErrorEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES5_
	.p2align 4,,10
	.p2align 3
.L1116:
	.cfi_restore_state
	movq	%rsi, %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1117:
	.cfi_restore_state
	cmpb	$0, 28(%rbp)
	je	.L1118
	cmpb	$4, %dl
	je	.L1119
	cmpb	%dl, %al
	jne	.L1146
	jmp	.L1116
	.p2align 4,,10
	.p2align 3
.L1133:
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler21RepresentationChanger26GetWord32RepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE
	.p2align 4,,10
	.p2align 3
.L1132:
	.cfi_restore_state
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler21RepresentationChanger26GetWord64RepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE
	.p2align 4,,10
	.p2align 3
.L1131:
	.cfi_restore_state
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler21RepresentationChanger32GetTaggedSignedRepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE
	.p2align 4,,10
	.p2align 3
.L1130:
	.cfi_restore_state
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler21RepresentationChanger33GetTaggedPointerRepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE
	.p2align 4,,10
	.p2align 3
.L1129:
	.cfi_restore_state
	salq	$32, %r10
	movzbl	%r11b, %r8d
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	orq	%r10, %r8
	jmp	_ZN2v88internal8compiler21RepresentationChanger26GetTaggedRepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeENS1_10TruncationE
	.p2align 4,,10
	.p2align 3
.L1128:
	.cfi_restore_state
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler21RepresentationChanger36GetCompressedSignedRepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE
	.p2align 4,,10
	.p2align 3
.L1127:
	.cfi_restore_state
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler21RepresentationChanger37GetCompressedPointerRepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE
	.p2align 4,,10
	.p2align 3
.L1126:
	.cfi_restore_state
	salq	$32, %r10
	movzbl	%r11b, %r8d
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	orq	%r10, %r8
	jmp	_ZN2v88internal8compiler21RepresentationChanger30GetCompressedRepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeENS1_10TruncationE
	.p2align 4,,10
	.p2align 3
.L1125:
	.cfi_restore_state
	salq	$32, %r10
	movzbl	%r11b, %r8d
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	orq	%r10, %r8
	jmp	_ZN2v88internal8compiler21RepresentationChanger27GetFloat32RepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeENS1_10TruncationE
	.p2align 4,,10
	.p2align 3
.L1124:
	.cfi_restore_state
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler21RepresentationChanger27GetFloat64RepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE
	.p2align 4,,10
	.p2align 3
.L1134:
	.cfi_restore_state
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler21RepresentationChanger23GetBitRepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeE
.L1122:
	.cfi_restore_state
	leaq	.LC8(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE22746:
	.size	_ZN2v88internal8compiler21RepresentationChanger20GetRepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE, .-_ZN2v88internal8compiler21RepresentationChanger20GetRepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE
	.section	.text._ZNK2v88internal8compiler21RepresentationChanger7isolateEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler21RepresentationChanger7isolateEv
	.type	_ZNK2v88internal8compiler21RepresentationChanger7isolateEv, @function
_ZNK2v88internal8compiler21RepresentationChanger7isolateEv:
.LFB22782:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	(%rax), %rax
	ret
	.cfi_endproc
.LFE22782:
	.size	_ZNK2v88internal8compiler21RepresentationChanger7isolateEv, .-_ZNK2v88internal8compiler21RepresentationChanger7isolateEv
	.section	.text.startup._GLOBAL__sub_I__ZNK2v88internal8compiler10Truncation11descriptionEv,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZNK2v88internal8compiler10Truncation11descriptionEv, @function
_GLOBAL__sub_I__ZNK2v88internal8compiler10Truncation11descriptionEv:
.LFB27564:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE27564:
	.size	_GLOBAL__sub_I__ZNK2v88internal8compiler10Truncation11descriptionEv, .-_GLOBAL__sub_I__ZNK2v88internal8compiler10Truncation11descriptionEv
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZNK2v88internal8compiler10Truncation11descriptionEv
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC9:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC10:
	.long	4294967295
	.long	2146435071
	.align 8
.LC11:
	.long	4290772992
	.long	1105199103
	.align 8
.LC12:
	.long	0
	.long	-1042284544
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC16:
	.long	4286578687
	.align 4
.LC17:
	.long	2139095039
	.align 4
.LC18:
	.long	2139095040
	.align 4
.LC19:
	.long	4286578688
	.section	.rodata.cst8
	.align 8
.LC20:
	.long	3758096384
	.long	1206910975
	.align 8
.LC21:
	.long	4026531839
	.long	1206910975
	.align 8
.LC22:
	.long	3758096384
	.long	-940572673
	.align 8
.LC23:
	.long	4026531839
	.long	-940572673
	.align 8
.LC27:
	.long	0
	.long	2146959360
	.align 8
.LC32:
	.long	0
	.long	1138753536
	.align 8
.LC33:
	.long	0
	.long	-1008730112
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
