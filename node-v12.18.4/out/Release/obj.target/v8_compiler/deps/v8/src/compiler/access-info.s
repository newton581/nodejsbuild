	.file	"access-info.cc"
	.text
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_123CanInlinePropertyAccessENS0_6HandleINS0_3MapEEE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_123CanInlinePropertyAccessENS0_6HandleINS0_3MapEEE, @function
_ZN2v88internal8compiler12_GLOBAL__N_123CanInlinePropertyAccessENS0_6HandleINS0_3MapEEE:
.LFB19264:
	.cfi_startproc
	movq	(%rdi), %rax
	movl	$1, %r8d
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	-36696(%rdx), %rax
	je	.L1
	movzwl	11(%rax), %edx
	cmpw	$66, %dx
	jbe	.L1
	cmpw	$1024, %dx
	ja	.L3
.L4:
	xorl	%r8d, %r8d
.L1:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movl	15(%rax), %edx
	andl	$2097152, %edx
	jne	.L4
	testb	$36, 13(%rax)
	sete	%r8b
	jmp	.L1
	.cfi_endproc
.LFE19264:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_123CanInlinePropertyAccessENS0_6HandleINS0_3MapEEE, .-_ZN2v88internal8compiler12_GLOBAL__N_123CanInlinePropertyAccessENS0_6HandleINS0_3MapEEE
	.section	.rodata._ZN2v88internal8compiler6MapRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb.str1.1,"aMS",@progbits,1
.LC0:
	.string	"IsMap()"
.LC1:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal8compiler6MapRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,"axG",@progbits,_ZN2v88internal8compiler6MapRefC5EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler6MapRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.type	_ZN2v88internal8compiler6MapRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, @function
_ZN2v88internal8compiler6MapRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb:
.LFB9727:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%ecx, %ebx
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	testb	%bl, %bl
	jne	.L11
.L8:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef5IsMapEv@PLT
	testb	%al, %al
	jne	.L8
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE9727:
	.size	_ZN2v88internal8compiler6MapRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, .-_ZN2v88internal8compiler6MapRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.weak	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.set	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,_ZN2v88internal8compiler6MapRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.section	.rodata._ZN2v88internal8compilerlsERSoNS1_10AccessModeE.str1.1,"aMS",@progbits,1
.LC2:
	.string	"Load"
.LC3:
	.string	"Store"
.LC4:
	.string	"StoreInLiteral"
.LC5:
	.string	"Has"
.LC6:
	.string	"unreachable code"
	.section	.text._ZN2v88internal8compilerlsERSoNS1_10AccessModeE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal8compilerlsERSoNS1_10AccessModeE
	.type	_ZN2v88internal8compilerlsERSoNS1_10AccessModeE, @function
_ZN2v88internal8compilerlsERSoNS1_10AccessModeE:
.LFB19265:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	cmpl	$2, %esi
	je	.L13
	jg	.L14
	testl	%esi, %esi
	je	.L15
	cmpl	$1, %esi
	jne	.L17
	movl	$5, %edx
	leaq	.LC3(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_restore_state
	cmpl	$3, %esi
	jne	.L17
	movl	$3, %edx
	leaq	.LC5(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	.cfi_restore_state
	movl	$14, %edx
	leaq	.LC4(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	.cfi_restore_state
	movl	$4, %edx
	leaq	.LC2(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L17:
	.cfi_restore_state
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19265:
	.size	_ZN2v88internal8compilerlsERSoNS1_10AccessModeE, .-_ZN2v88internal8compilerlsERSoNS1_10AccessModeE
	.section	.rodata._ZN2v88internal8compiler17ElementAccessInfoC2EONS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEENS0_12ElementsKindEPNS0_4ZoneE.str1.1,"aMS",@progbits,1
.LC7:
	.string	"!receiver_maps.empty()"
	.section	.text._ZN2v88internal8compiler17ElementAccessInfoC2EONS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEENS0_12ElementsKindEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17ElementAccessInfoC2EONS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEENS0_12ElementsKindEPNS0_4ZoneE
	.type	_ZN2v88internal8compiler17ElementAccessInfoC2EONS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEENS0_12ElementsKindEPNS0_4ZoneE, @function
_ZN2v88internal8compiler17ElementAccessInfoC2EONS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEENS0_12ElementsKindEPNS0_4ZoneE:
.LFB19273:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movb	%dl, (%rdi)
	movq	(%rsi), %rdi
	movq	16(%rsi), %rax
	subq	8(%rsi), %rax
	movq	$0, 16(%rbx)
	movq	%rdi, 8(%rbx)
	movq	$0, 24(%rbx)
	movq	$0, 32(%rbx)
	je	.L32
	leaq	7(%rax), %rsi
	movq	24(%rdi), %rdx
	movq	%rax, %r14
	movq	16(%rdi), %rax
	andq	$-8, %rsi
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L44
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L22:
	movq	%rax, %xmm0
	addq	%rax, %r14
	punpcklqdq	%xmm0, %xmm0
	movq	%r14, 32(%rbx)
	movups	%xmm0, 16(%rbx)
	movq	16(%r12), %rdx
	movq	8(%r12), %rcx
	cmpq	%rcx, %rdx
	je	.L25
	subq	$8, %rdx
	leaq	15(%rcx), %rsi
	movq	%rdx, %rdi
	subq	%rax, %rsi
	subq	%rcx, %rdi
	movq	%rdi, %rdx
	shrq	$3, %rdx
	cmpq	$30, %rsi
	jbe	.L33
	movabsq	$2305843009213693948, %rsi
	testq	%rsi, %rdx
	je	.L33
	addq	$1, %rdx
	xorl	%esi, %esi
	movq	%rdx, %r8
	shrq	%r8
	salq	$4, %r8
	.p2align 4,,10
	.p2align 3
.L27:
	movdqu	(%rcx,%rsi), %xmm1
	movups	%xmm1, (%rax,%rsi)
	addq	$16, %rsi
	cmpq	%r8, %rsi
	jne	.L27
	movq	%rdx, %r8
	andq	$-2, %r8
	leaq	0(,%r8,8), %rsi
	addq	%rsi, %rcx
	addq	%rax, %rsi
	cmpq	%r8, %rdx
	je	.L30
	movq	(%rcx), %rdx
	movq	%rdx, (%rsi)
.L30:
	leaq	8(%rax,%rdi), %rax
.L25:
	movq	%rax, 24(%rbx)
	movq	%r13, 40(%rbx)
	movq	$0, 48(%rbx)
	movq	$0, 56(%rbx)
	movq	$0, 64(%rbx)
	movq	8(%r12), %rax
	cmpq	%rax, 16(%r12)
	je	.L45
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore_state
	xorl	%r14d, %r14d
	xorl	%eax, %eax
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L44:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L33:
	xorl	%esi, %esi
	.p2align 4,,10
	.p2align 3
.L26:
	movq	(%rcx,%rsi,8), %r8
	movq	%r8, (%rax,%rsi,8)
	movq	%rsi, %r8
	addq	$1, %rsi
	cmpq	%r8, %rdx
	jne	.L26
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L45:
	leaq	.LC7(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19273:
	.size	_ZN2v88internal8compiler17ElementAccessInfoC2EONS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEENS0_12ElementsKindEPNS0_4ZoneE, .-_ZN2v88internal8compiler17ElementAccessInfoC2EONS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEENS0_12ElementsKindEPNS0_4ZoneE
	.globl	_ZN2v88internal8compiler17ElementAccessInfoC1EONS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEENS0_12ElementsKindEPNS0_4ZoneE
	.set	_ZN2v88internal8compiler17ElementAccessInfoC1EONS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEENS0_12ElementsKindEPNS0_4ZoneE,_ZN2v88internal8compiler17ElementAccessInfoC2EONS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEENS0_12ElementsKindEPNS0_4ZoneE
	.section	.text._ZN2v88internal8compiler18PropertyAccessInfo7InvalidEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18PropertyAccessInfo7InvalidEPNS0_4ZoneE
	.type	_ZN2v88internal8compiler18PropertyAccessInfo7InvalidEPNS0_4ZoneE, @function
_ZN2v88internal8compiler18PropertyAccessInfo7InvalidEPNS0_4ZoneE:
.LFB19275:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movq	%rsi, 8(%rdi)
	movq	%rdi, %rax
	movl	$0, (%rdi)
	movq	$0, 16(%rdi)
	movq	$0, 24(%rdi)
	movq	$0, 32(%rdi)
	movq	%rsi, 40(%rdi)
	movq	$0, 48(%rdi)
	movq	$0, 56(%rdi)
	movq	$0, 64(%rdi)
	movq	$0, 72(%rdi)
	movq	$0, 80(%rdi)
	movq	$0, 88(%rdi)
	movq	$0, 96(%rdi)
	movb	$0, 104(%rdi)
	movq	$1, 112(%rdi)
	movups	%xmm0, 120(%rdi)
	ret
	.cfi_endproc
.LFE19275:
	.size	_ZN2v88internal8compiler18PropertyAccessInfo7InvalidEPNS0_4ZoneE, .-_ZN2v88internal8compiler18PropertyAccessInfo7InvalidEPNS0_4ZoneE
	.section	.text._ZN2v88internal8compiler18PropertyAccessInfo8NotFoundEPNS0_4ZoneENS0_6HandleINS0_3MapEEENS0_11MaybeHandleINS0_8JSObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18PropertyAccessInfo8NotFoundEPNS0_4ZoneENS0_6HandleINS0_3MapEEENS0_11MaybeHandleINS0_8JSObjectEEE
	.type	_ZN2v88internal8compiler18PropertyAccessInfo8NotFoundEPNS0_4ZoneENS0_6HandleINS0_3MapEEENS0_11MaybeHandleINS0_8JSObjectEEE, @function
_ZN2v88internal8compiler18PropertyAccessInfo8NotFoundEPNS0_4ZoneENS0_6HandleINS0_3MapEEENS0_11MaybeHandleINS0_8JSObjectEEE:
.LFB19303:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	16(%rsi), %r13
	movq	24(%rsi), %rax
	subq	%r13, %rax
	cmpq	$7, %rax
	jbe	.L53
	leaq	8(%r13), %rax
	movq	%rax, 16(%rsi)
.L49:
	movq	%rdx, 0(%r13)
	movl	$1, (%r12)
	movq	%rbx, 8(%r12)
	movq	$0, 16(%r12)
	movq	$0, 24(%r12)
	movq	$0, 32(%r12)
	movq	16(%rbx), %rax
	movq	24(%rbx), %rdx
	subq	%rax, %rdx
	cmpq	$7, %rdx
	jbe	.L54
	leaq	8(%rax), %rdx
	movq	%rdx, 16(%rbx)
.L51:
	movq	%rax, 24(%r12)
	pxor	%xmm0, %xmm0
	movq	%rax, 16(%r12)
	movq	%rdx, 32(%r12)
	movq	0(%r13), %rsi
	movq	%rsi, (%rax)
	movq	%r12, %rax
	movq	%rbx, 40(%r12)
	movq	%rdx, 24(%r12)
	movq	$0, 48(%r12)
	movq	$0, 56(%r12)
	movq	$0, 64(%r12)
	movq	$0, 72(%r12)
	movq	$0, 80(%r12)
	movq	%rcx, 88(%r12)
	movq	$0, 96(%r12)
	movb	$0, 104(%r12)
	movq	$1, 112(%r12)
	movups	%xmm0, 120(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	.cfi_restore_state
	movl	$8, %esi
	movq	%rbx, %rdi
	movq	%rcx, -48(%rbp)
	movq	%rdx, -40(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-40(%rbp), %rdx
	movq	-48(%rbp), %rcx
	movq	%rax, %r13
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L54:
	movl	$8, %esi
	movq	%rbx, %rdi
	movq	%rcx, -40(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-40(%rbp), %rcx
	leaq	8(%rax), %rdx
	jmp	.L51
	.cfi_endproc
.LFE19303:
	.size	_ZN2v88internal8compiler18PropertyAccessInfo8NotFoundEPNS0_4ZoneENS0_6HandleINS0_3MapEEENS0_11MaybeHandleINS0_8JSObjectEEE, .-_ZN2v88internal8compiler18PropertyAccessInfo8NotFoundEPNS0_4ZoneENS0_6HandleINS0_3MapEEENS0_11MaybeHandleINS0_8JSObjectEEE
	.section	.text._ZN2v88internal8compiler18PropertyAccessInfo9DataFieldEPNS0_4ZoneENS0_6HandleINS0_3MapEEEONS0_10ZoneVectorIPKNS1_21CompilationDependencyEEENS0_10FieldIndexENS0_14RepresentationENS1_4TypeES7_NS0_11MaybeHandleIS6_EENSH_INS0_8JSObjectEEESI_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18PropertyAccessInfo9DataFieldEPNS0_4ZoneENS0_6HandleINS0_3MapEEEONS0_10ZoneVectorIPKNS1_21CompilationDependencyEEENS0_10FieldIndexENS0_14RepresentationENS1_4TypeES7_NS0_11MaybeHandleIS6_EENSH_INS0_8JSObjectEEESI_
	.type	_ZN2v88internal8compiler18PropertyAccessInfo9DataFieldEPNS0_4ZoneENS0_6HandleINS0_3MapEEEONS0_10ZoneVectorIPKNS1_21CompilationDependencyEEENS0_10FieldIndexENS0_14RepresentationENS1_4TypeES7_NS0_11MaybeHandleIS6_EENSH_INS0_8JSObjectEEESI_, @function
_ZN2v88internal8compiler18PropertyAccessInfo9DataFieldEPNS0_4ZoneENS0_6HandleINS0_3MapEEEONS0_10ZoneVectorIPKNS1_21CompilationDependencyEEENS0_10FieldIndexENS0_14RepresentationENS1_4TypeES7_NS0_11MaybeHandleIS6_EENSH_INS0_8JSObjectEEESI_:
.LFB19304:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$40, %rsp
	movq	16(%rsi), %r14
	movq	24(%rsi), %rax
	subq	%r14, %rax
	cmpq	$7, %rax
	jbe	.L61
	leaq	8(%r14), %rax
	movq	%rax, 16(%rsi)
.L57:
	movq	%rdx, (%r14)
	movl	$2, (%r12)
	movq	%r13, 8(%r12)
	movq	$0, 16(%r12)
	movq	$0, 24(%r12)
	movq	$0, 32(%r12)
	movq	16(%r13), %rax
	movq	24(%r13), %rdx
	subq	%rax, %rdx
	cmpq	$7, %rdx
	jbe	.L62
	leaq	8(%rax), %rdx
	movq	%rdx, 16(%r13)
.L59:
	movq	%rax, 24(%r12)
	pxor	%xmm0, %xmm0
	movq	%rax, 16(%r12)
	movq	%rdx, 32(%r12)
	movq	(%r14), %rsi
	movq	%rsi, (%rax)
	movq	%rdx, 24(%r12)
	movq	(%rcx), %rax
	movq	%rax, 40(%r12)
	movdqu	8(%rcx), %xmm1
	movups	%xmm0, 8(%rcx)
	movq	48(%rbp), %xmm0
	movq	24(%rcx), %rax
	movq	$0, 24(%rcx)
	movhps	40(%rbp), %xmm0
	movq	%rax, 64(%r12)
	movq	16(%rbp), %rax
	movups	%xmm0, 80(%r12)
	movq	24(%rbp), %xmm0
	movq	%rax, 112(%r12)
	movq	%r12, %rax
	movhps	32(%rbp), %xmm0
	movq	%r8, 96(%r12)
	movq	$0, 72(%r12)
	movb	%r9b, 104(%r12)
	movups	%xmm1, 48(%r12)
	movups	%xmm0, 120(%r12)
	addq	$40, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L61:
	.cfi_restore_state
	movl	$8, %esi
	movq	%r13, %rdi
	movb	%r9b, -57(%rbp)
	movq	%r8, -56(%rbp)
	movq	%rcx, -48(%rbp)
	movq	%rdx, -40(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-40(%rbp), %rdx
	movq	-48(%rbp), %rcx
	movq	-56(%rbp), %r8
	movzbl	-57(%rbp), %r9d
	movq	%rax, %r14
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L62:
	movl	$8, %esi
	movq	%r13, %rdi
	movb	%r9b, -56(%rbp)
	movq	%r8, -48(%rbp)
	movq	%rcx, -40(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-40(%rbp), %rcx
	movq	-48(%rbp), %r8
	movzbl	-56(%rbp), %r9d
	leaq	8(%rax), %rdx
	jmp	.L59
	.cfi_endproc
.LFE19304:
	.size	_ZN2v88internal8compiler18PropertyAccessInfo9DataFieldEPNS0_4ZoneENS0_6HandleINS0_3MapEEEONS0_10ZoneVectorIPKNS1_21CompilationDependencyEEENS0_10FieldIndexENS0_14RepresentationENS1_4TypeES7_NS0_11MaybeHandleIS6_EENSH_INS0_8JSObjectEEESI_, .-_ZN2v88internal8compiler18PropertyAccessInfo9DataFieldEPNS0_4ZoneENS0_6HandleINS0_3MapEEEONS0_10ZoneVectorIPKNS1_21CompilationDependencyEEENS0_10FieldIndexENS0_14RepresentationENS1_4TypeES7_NS0_11MaybeHandleIS6_EENSH_INS0_8JSObjectEEESI_
	.section	.text._ZN2v88internal8compiler18PropertyAccessInfo12DataConstantEPNS0_4ZoneENS0_6HandleINS0_3MapEEEONS0_10ZoneVectorIPKNS1_21CompilationDependencyEEENS0_10FieldIndexENS0_14RepresentationENS1_4TypeES7_NS0_11MaybeHandleIS6_EENSH_INS0_8JSObjectEEESI_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18PropertyAccessInfo12DataConstantEPNS0_4ZoneENS0_6HandleINS0_3MapEEEONS0_10ZoneVectorIPKNS1_21CompilationDependencyEEENS0_10FieldIndexENS0_14RepresentationENS1_4TypeES7_NS0_11MaybeHandleIS6_EENSH_INS0_8JSObjectEEESI_
	.type	_ZN2v88internal8compiler18PropertyAccessInfo12DataConstantEPNS0_4ZoneENS0_6HandleINS0_3MapEEEONS0_10ZoneVectorIPKNS1_21CompilationDependencyEEENS0_10FieldIndexENS0_14RepresentationENS1_4TypeES7_NS0_11MaybeHandleIS6_EENSH_INS0_8JSObjectEEESI_, @function
_ZN2v88internal8compiler18PropertyAccessInfo12DataConstantEPNS0_4ZoneENS0_6HandleINS0_3MapEEEONS0_10ZoneVectorIPKNS1_21CompilationDependencyEEENS0_10FieldIndexENS0_14RepresentationENS1_4TypeES7_NS0_11MaybeHandleIS6_EENSH_INS0_8JSObjectEEESI_:
.LFB19306:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$40, %rsp
	movq	16(%rsi), %r14
	movq	24(%rsi), %rax
	subq	%r14, %rax
	cmpq	$7, %rax
	jbe	.L69
	leaq	8(%r14), %rax
	movq	%rax, 16(%rsi)
.L65:
	movq	%rdx, (%r14)
	movl	$3, (%r12)
	movq	%r13, 8(%r12)
	movq	$0, 16(%r12)
	movq	$0, 24(%r12)
	movq	$0, 32(%r12)
	movq	16(%r13), %rax
	movq	24(%r13), %rdx
	subq	%rax, %rdx
	cmpq	$7, %rdx
	jbe	.L70
	leaq	8(%rax), %rdx
	movq	%rdx, 16(%r13)
.L67:
	movq	%rax, 24(%r12)
	pxor	%xmm0, %xmm0
	movq	%rax, 16(%r12)
	movq	%rdx, 32(%r12)
	movq	(%r14), %rsi
	movq	%rsi, (%rax)
	movq	%rdx, 24(%r12)
	movq	(%rcx), %rax
	movq	%rax, 40(%r12)
	movdqu	8(%rcx), %xmm1
	movups	%xmm0, 8(%rcx)
	movq	48(%rbp), %xmm0
	movq	24(%rcx), %rax
	movq	$0, 24(%rcx)
	movhps	40(%rbp), %xmm0
	movq	%rax, 64(%r12)
	movq	16(%rbp), %rax
	movups	%xmm0, 80(%r12)
	movq	24(%rbp), %xmm0
	movq	%rax, 112(%r12)
	movq	%r12, %rax
	movhps	32(%rbp), %xmm0
	movq	%r8, 96(%r12)
	movq	$0, 72(%r12)
	movb	%r9b, 104(%r12)
	movups	%xmm1, 48(%r12)
	movups	%xmm0, 120(%r12)
	addq	$40, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L69:
	.cfi_restore_state
	movl	$8, %esi
	movq	%r13, %rdi
	movb	%r9b, -57(%rbp)
	movq	%r8, -56(%rbp)
	movq	%rcx, -48(%rbp)
	movq	%rdx, -40(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-40(%rbp), %rdx
	movq	-48(%rbp), %rcx
	movq	-56(%rbp), %r8
	movzbl	-57(%rbp), %r9d
	movq	%rax, %r14
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L70:
	movl	$8, %esi
	movq	%r13, %rdi
	movb	%r9b, -56(%rbp)
	movq	%r8, -48(%rbp)
	movq	%rcx, -40(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-40(%rbp), %rcx
	movq	-48(%rbp), %r8
	movzbl	-56(%rbp), %r9d
	leaq	8(%rax), %rdx
	jmp	.L67
	.cfi_endproc
.LFE19306:
	.size	_ZN2v88internal8compiler18PropertyAccessInfo12DataConstantEPNS0_4ZoneENS0_6HandleINS0_3MapEEEONS0_10ZoneVectorIPKNS1_21CompilationDependencyEEENS0_10FieldIndexENS0_14RepresentationENS1_4TypeES7_NS0_11MaybeHandleIS6_EENSH_INS0_8JSObjectEEESI_, .-_ZN2v88internal8compiler18PropertyAccessInfo12DataConstantEPNS0_4ZoneENS0_6HandleINS0_3MapEEEONS0_10ZoneVectorIPKNS1_21CompilationDependencyEEENS0_10FieldIndexENS0_14RepresentationENS1_4TypeES7_NS0_11MaybeHandleIS6_EENSH_INS0_8JSObjectEEESI_
	.section	.text._ZN2v88internal8compiler18PropertyAccessInfo16AccessorConstantEPNS0_4ZoneENS0_6HandleINS0_3MapEEENS5_INS0_6ObjectEEENS0_11MaybeHandleINS0_8JSObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18PropertyAccessInfo16AccessorConstantEPNS0_4ZoneENS0_6HandleINS0_3MapEEENS5_INS0_6ObjectEEENS0_11MaybeHandleINS0_8JSObjectEEE
	.type	_ZN2v88internal8compiler18PropertyAccessInfo16AccessorConstantEPNS0_4ZoneENS0_6HandleINS0_3MapEEENS5_INS0_6ObjectEEENS0_11MaybeHandleINS0_8JSObjectEEE, @function
_ZN2v88internal8compiler18PropertyAccessInfo16AccessorConstantEPNS0_4ZoneENS0_6HandleINS0_3MapEEENS5_INS0_6ObjectEEENS0_11MaybeHandleINS0_8JSObjectEEE:
.LFB19307:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	16(%rsi), %r13
	movq	24(%rsi), %rax
	subq	%r13, %rax
	cmpq	$7, %rax
	jbe	.L77
	leaq	8(%r13), %rax
	movq	%rax, 16(%rsi)
.L73:
	movq	%rdx, 0(%r13)
	movl	$4, (%r12)
	movq	%rbx, 8(%r12)
	movq	$0, 16(%r12)
	movq	$0, 24(%r12)
	movq	$0, 32(%r12)
	movq	16(%rbx), %rax
	movq	24(%rbx), %rdx
	subq	%rax, %rdx
	cmpq	$7, %rdx
	jbe	.L78
	leaq	8(%rax), %rdx
	movq	%rdx, 16(%rbx)
.L75:
	movq	%rax, 24(%r12)
	pxor	%xmm0, %xmm0
	movq	%rax, 16(%r12)
	movq	%rdx, 32(%r12)
	movq	0(%r13), %rsi
	movq	%rsi, (%rax)
	movl	$4294967295, %eax
	movq	%rbx, 40(%r12)
	movq	%rax, 112(%r12)
	movq	%r12, %rax
	movq	%rdx, 24(%r12)
	movq	$0, 48(%r12)
	movq	$0, 56(%r12)
	movq	$0, 64(%r12)
	movq	%rcx, 72(%r12)
	movq	$0, 80(%r12)
	movq	%r8, 88(%r12)
	movq	$0, 96(%r12)
	movb	$0, 104(%r12)
	movups	%xmm0, 120(%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L77:
	.cfi_restore_state
	movl	$8, %esi
	movq	%rbx, %rdi
	movq	%r8, -56(%rbp)
	movq	%rcx, -48(%rbp)
	movq	%rdx, -40(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-40(%rbp), %rdx
	movq	-48(%rbp), %rcx
	movq	-56(%rbp), %r8
	movq	%rax, %r13
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L78:
	movl	$8, %esi
	movq	%rbx, %rdi
	movq	%r8, -48(%rbp)
	movq	%rcx, -40(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-40(%rbp), %rcx
	movq	-48(%rbp), %r8
	leaq	8(%rax), %rdx
	jmp	.L75
	.cfi_endproc
.LFE19307:
	.size	_ZN2v88internal8compiler18PropertyAccessInfo16AccessorConstantEPNS0_4ZoneENS0_6HandleINS0_3MapEEENS5_INS0_6ObjectEEENS0_11MaybeHandleINS0_8JSObjectEEE, .-_ZN2v88internal8compiler18PropertyAccessInfo16AccessorConstantEPNS0_4ZoneENS0_6HandleINS0_3MapEEENS5_INS0_6ObjectEEENS0_11MaybeHandleINS0_8JSObjectEEE
	.section	.text._ZN2v88internal8compiler18PropertyAccessInfo12ModuleExportEPNS0_4ZoneENS0_6HandleINS0_3MapEEENS5_INS0_4CellEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18PropertyAccessInfo12ModuleExportEPNS0_4ZoneENS0_6HandleINS0_3MapEEENS5_INS0_4CellEEE
	.type	_ZN2v88internal8compiler18PropertyAccessInfo12ModuleExportEPNS0_4ZoneENS0_6HandleINS0_3MapEEENS5_INS0_4CellEEE, @function
_ZN2v88internal8compiler18PropertyAccessInfo12ModuleExportEPNS0_4ZoneENS0_6HandleINS0_3MapEEENS5_INS0_4CellEEE:
.LFB19308:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	16(%rsi), %r13
	movq	24(%rsi), %rax
	subq	%r13, %rax
	cmpq	$7, %rax
	jbe	.L85
	leaq	8(%r13), %rax
	movq	%rax, 16(%rsi)
.L81:
	movq	%rdx, 0(%r13)
	movl	$5, (%r12)
	movq	%rbx, 8(%r12)
	movq	$0, 16(%r12)
	movq	$0, 24(%r12)
	movq	$0, 32(%r12)
	movq	16(%rbx), %rax
	movq	24(%rbx), %rdx
	subq	%rax, %rdx
	cmpq	$7, %rdx
	jbe	.L86
	leaq	8(%rax), %rdx
	movq	%rdx, 16(%rbx)
.L83:
	movq	%rax, 24(%r12)
	pxor	%xmm0, %xmm0
	movq	%rax, 16(%r12)
	movq	%rdx, 32(%r12)
	movq	0(%r13), %rsi
	movq	%rsi, (%rax)
	movl	$4294967295, %eax
	movq	%rbx, 40(%r12)
	movq	%rax, 112(%r12)
	movq	%r12, %rax
	movq	%rdx, 24(%r12)
	movq	$0, 48(%r12)
	movq	$0, 56(%r12)
	movq	$0, 64(%r12)
	movq	%rcx, 72(%r12)
	movq	$0, 80(%r12)
	movq	$0, 88(%r12)
	movq	$0, 96(%r12)
	movb	$0, 104(%r12)
	movups	%xmm0, 120(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L85:
	.cfi_restore_state
	movl	$8, %esi
	movq	%rbx, %rdi
	movq	%rcx, -48(%rbp)
	movq	%rdx, -40(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-40(%rbp), %rdx
	movq	-48(%rbp), %rcx
	movq	%rax, %r13
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L86:
	movl	$8, %esi
	movq	%rbx, %rdi
	movq	%rcx, -40(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-40(%rbp), %rcx
	leaq	8(%rax), %rdx
	jmp	.L83
	.cfi_endproc
.LFE19308:
	.size	_ZN2v88internal8compiler18PropertyAccessInfo12ModuleExportEPNS0_4ZoneENS0_6HandleINS0_3MapEEENS5_INS0_4CellEEE, .-_ZN2v88internal8compiler18PropertyAccessInfo12ModuleExportEPNS0_4ZoneENS0_6HandleINS0_3MapEEENS5_INS0_4CellEEE
	.section	.text._ZN2v88internal8compiler18PropertyAccessInfo12StringLengthEPNS0_4ZoneENS0_6HandleINS0_3MapEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18PropertyAccessInfo12StringLengthEPNS0_4ZoneENS0_6HandleINS0_3MapEEE
	.type	_ZN2v88internal8compiler18PropertyAccessInfo12StringLengthEPNS0_4ZoneENS0_6HandleINS0_3MapEEE, @function
_ZN2v88internal8compiler18PropertyAccessInfo12StringLengthEPNS0_4ZoneENS0_6HandleINS0_3MapEEE:
.LFB19309:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	16(%rsi), %r13
	movq	24(%rsi), %rax
	subq	%r13, %rax
	cmpq	$7, %rax
	jbe	.L93
	leaq	8(%r13), %rax
	movq	%rax, 16(%rsi)
.L89:
	movq	%rdx, 0(%r13)
	movl	$6, (%r12)
	movq	%rbx, 8(%r12)
	movq	$0, 16(%r12)
	movq	$0, 24(%r12)
	movq	$0, 32(%r12)
	movq	16(%rbx), %rax
	movq	24(%rbx), %rdx
	subq	%rax, %rdx
	cmpq	$7, %rdx
	jbe	.L94
	leaq	8(%rax), %rdx
	movq	%rdx, 16(%rbx)
.L91:
	movq	%rax, 24(%r12)
	pxor	%xmm0, %xmm0
	movq	%rax, 16(%r12)
	movq	%rdx, 32(%r12)
	movq	0(%r13), %rcx
	movq	%rcx, (%rax)
	movq	%r12, %rax
	movq	%rbx, 40(%r12)
	movq	%rdx, 24(%r12)
	movq	$0, 48(%r12)
	movq	$0, 56(%r12)
	movq	$0, 64(%r12)
	movq	$0, 72(%r12)
	movq	$0, 80(%r12)
	movq	$0, 88(%r12)
	movq	$0, 96(%r12)
	movb	$0, 104(%r12)
	movq	$1, 112(%r12)
	movups	%xmm0, 120(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L93:
	.cfi_restore_state
	movl	$8, %esi
	movq	%rbx, %rdi
	movq	%rdx, -40(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-40(%rbp), %rdx
	movq	%rax, %r13
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L94:
	movl	$8, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	leaq	8(%rax), %rdx
	jmp	.L91
	.cfi_endproc
.LFE19309:
	.size	_ZN2v88internal8compiler18PropertyAccessInfo12StringLengthEPNS0_4ZoneENS0_6HandleINS0_3MapEEE, .-_ZN2v88internal8compiler18PropertyAccessInfo12StringLengthEPNS0_4ZoneENS0_6HandleINS0_3MapEEE
	.section	.text._ZN2v88internal8compiler18PropertyAccessInfoC2EPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18PropertyAccessInfoC2EPNS0_4ZoneE
	.type	_ZN2v88internal8compiler18PropertyAccessInfoC2EPNS0_4ZoneE, @function
_ZN2v88internal8compiler18PropertyAccessInfoC2EPNS0_4ZoneE:
.LFB19311:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movl	$0, (%rdi)
	movq	%rsi, 8(%rdi)
	movq	$0, 16(%rdi)
	movq	$0, 24(%rdi)
	movq	$0, 32(%rdi)
	movq	%rsi, 40(%rdi)
	movq	$0, 48(%rdi)
	movq	$0, 56(%rdi)
	movq	$0, 64(%rdi)
	movq	$0, 72(%rdi)
	movq	$0, 80(%rdi)
	movq	$0, 88(%rdi)
	movq	$0, 96(%rdi)
	movb	$0, 104(%rdi)
	movq	$1, 112(%rdi)
	movups	%xmm0, 120(%rdi)
	ret
	.cfi_endproc
.LFE19311:
	.size	_ZN2v88internal8compiler18PropertyAccessInfoC2EPNS0_4ZoneE, .-_ZN2v88internal8compiler18PropertyAccessInfoC2EPNS0_4ZoneE
	.globl	_ZN2v88internal8compiler18PropertyAccessInfoC1EPNS0_4ZoneE
	.set	_ZN2v88internal8compiler18PropertyAccessInfoC1EPNS0_4ZoneE,_ZN2v88internal8compiler18PropertyAccessInfoC2EPNS0_4ZoneE
	.section	.text._ZN2v88internal8compiler18PropertyAccessInfoC2EPNS0_4ZoneENS2_4KindENS0_11MaybeHandleINS0_8JSObjectEEEONS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18PropertyAccessInfoC2EPNS0_4ZoneENS2_4KindENS0_11MaybeHandleINS0_8JSObjectEEEONS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEE
	.type	_ZN2v88internal8compiler18PropertyAccessInfoC2EPNS0_4ZoneENS2_4KindENS0_11MaybeHandleINS0_8JSObjectEEEONS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEE, @function
_ZN2v88internal8compiler18PropertyAccessInfoC2EPNS0_4ZoneENS2_4KindENS0_11MaybeHandleINS0_8JSObjectEEEONS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEE:
.LFB19314:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	movl	%edx, (%rdi)
	movq	(%r8), %rdi
	movq	16(%r8), %rax
	subq	8(%r8), %rax
	movq	$0, 16(%rbx)
	movq	%rdi, 8(%rbx)
	movq	$0, 24(%rbx)
	movq	$0, 32(%rbx)
	je	.L106
	leaq	7(%rax), %rsi
	movq	24(%rdi), %rdx
	movq	%rax, %r14
	movq	16(%rdi), %rax
	andq	$-8, %rsi
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L118
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L97:
	movq	%rax, %xmm0
	addq	%rax, %r14
	punpcklqdq	%xmm0, %xmm0
	movq	%r14, 32(%rbx)
	movups	%xmm0, 16(%rbx)
	movq	16(%r8), %rdx
	movq	8(%r8), %rcx
	cmpq	%rcx, %rdx
	je	.L100
	subq	$8, %rdx
	leaq	15(%rcx), %rsi
	movq	%rdx, %rdi
	subq	%rax, %rsi
	subq	%rcx, %rdi
	movq	%rdi, %rdx
	shrq	$3, %rdx
	cmpq	$30, %rsi
	jbe	.L107
	movabsq	$2305843009213693948, %rsi
	testq	%rsi, %rdx
	je	.L107
	addq	$1, %rdx
	xorl	%esi, %esi
	movq	%rdx, %r8
	shrq	%r8
	salq	$4, %r8
	.p2align 4,,10
	.p2align 3
.L102:
	movdqu	(%rcx,%rsi), %xmm1
	movups	%xmm1, (%rax,%rsi)
	addq	$16, %rsi
	cmpq	%r8, %rsi
	jne	.L102
	movq	%rdx, %r8
	andq	$-2, %r8
	leaq	0(,%r8,8), %rsi
	addq	%rsi, %rcx
	addq	%rax, %rsi
	cmpq	%r8, %rdx
	je	.L105
	movq	(%rcx), %rdx
	movq	%rdx, (%rsi)
.L105:
	leaq	8(%rax,%rdi), %rax
.L100:
	pxor	%xmm0, %xmm0
	movq	%r12, 40(%rbx)
	movq	%r13, 88(%rbx)
	movq	%rax, 24(%rbx)
	movq	$0, 48(%rbx)
	movq	$0, 56(%rbx)
	movq	$0, 64(%rbx)
	movq	$0, 72(%rbx)
	movq	$0, 80(%rbx)
	movq	$0, 96(%rbx)
	movb	$0, 104(%rbx)
	movq	$1, 112(%rbx)
	movups	%xmm0, 120(%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L106:
	.cfi_restore_state
	xorl	%r14d, %r14d
	xorl	%eax, %eax
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L118:
	movq	%r8, -40(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-40(%rbp), %r8
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L107:
	xorl	%esi, %esi
	.p2align 4,,10
	.p2align 3
.L101:
	movq	(%rcx,%rsi,8), %r8
	movq	%r8, (%rax,%rsi,8)
	movq	%rsi, %r8
	addq	$1, %rsi
	cmpq	%rdx, %r8
	jne	.L101
	jmp	.L105
	.cfi_endproc
.LFE19314:
	.size	_ZN2v88internal8compiler18PropertyAccessInfoC2EPNS0_4ZoneENS2_4KindENS0_11MaybeHandleINS0_8JSObjectEEEONS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEE, .-_ZN2v88internal8compiler18PropertyAccessInfoC2EPNS0_4ZoneENS2_4KindENS0_11MaybeHandleINS0_8JSObjectEEEONS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEE
	.globl	_ZN2v88internal8compiler18PropertyAccessInfoC1EPNS0_4ZoneENS2_4KindENS0_11MaybeHandleINS0_8JSObjectEEEONS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEE
	.set	_ZN2v88internal8compiler18PropertyAccessInfoC1EPNS0_4ZoneENS2_4KindENS0_11MaybeHandleINS0_8JSObjectEEEONS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEE,_ZN2v88internal8compiler18PropertyAccessInfoC2EPNS0_4ZoneENS2_4KindENS0_11MaybeHandleINS0_8JSObjectEEEONS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEE
	.section	.text._ZN2v88internal8compiler18PropertyAccessInfoC2EPNS0_4ZoneENS2_4KindENS0_11MaybeHandleINS0_8JSObjectEEENS0_6HandleINS0_6ObjectEEEONS0_10ZoneVectorINS9_INS0_3MapEEEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18PropertyAccessInfoC2EPNS0_4ZoneENS2_4KindENS0_11MaybeHandleINS0_8JSObjectEEENS0_6HandleINS0_6ObjectEEEONS0_10ZoneVectorINS9_INS0_3MapEEEEE
	.type	_ZN2v88internal8compiler18PropertyAccessInfoC2EPNS0_4ZoneENS2_4KindENS0_11MaybeHandleINS0_8JSObjectEEENS0_6HandleINS0_6ObjectEEEONS0_10ZoneVectorINS9_INS0_3MapEEEEE, @function
_ZN2v88internal8compiler18PropertyAccessInfoC2EPNS0_4ZoneENS2_4KindENS0_11MaybeHandleINS0_8JSObjectEEENS0_6HandleINS0_6ObjectEEEONS0_10ZoneVectorINS9_INS0_3MapEEEEE:
.LFB19317:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	movl	%edx, (%rdi)
	movq	(%r9), %rdi
	movq	16(%r9), %rax
	subq	8(%r9), %rax
	movq	$0, 16(%rbx)
	movq	%rdi, 8(%rbx)
	movq	$0, 24(%rbx)
	movq	$0, 32(%rbx)
	je	.L129
	leaq	7(%rax), %rsi
	movq	24(%rdi), %rdx
	movq	%rax, %r14
	movq	16(%rdi), %rax
	andq	$-8, %rsi
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L141
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L120:
	movq	%rax, %xmm0
	addq	%rax, %r14
	punpcklqdq	%xmm0, %xmm0
	movq	%r14, 32(%rbx)
	movups	%xmm0, 16(%rbx)
	movq	16(%r9), %rdx
	movq	8(%r9), %rcx
	cmpq	%rcx, %rdx
	je	.L123
	subq	$8, %rdx
	leaq	15(%rcx), %rsi
	movq	%rdx, %rdi
	subq	%rax, %rsi
	subq	%rcx, %rdi
	movq	%rdi, %rdx
	shrq	$3, %rdx
	cmpq	$30, %rsi
	jbe	.L130
	movabsq	$2305843009213693948, %rsi
	testq	%rsi, %rdx
	je	.L130
	addq	$1, %rdx
	xorl	%esi, %esi
	movq	%rdx, %r9
	shrq	%r9
	salq	$4, %r9
	.p2align 4,,10
	.p2align 3
.L125:
	movdqu	(%rcx,%rsi), %xmm1
	movups	%xmm1, (%rax,%rsi)
	addq	$16, %rsi
	cmpq	%r9, %rsi
	jne	.L125
	movq	%rdx, %r9
	andq	$-2, %r9
	leaq	0(,%r9,8), %rsi
	addq	%rsi, %rcx
	addq	%rax, %rsi
	cmpq	%r9, %rdx
	je	.L128
	movq	(%rcx), %rdx
	movq	%rdx, (%rsi)
.L128:
	leaq	8(%rax,%rdi), %rax
.L123:
	movq	%rax, 24(%rbx)
	pxor	%xmm0, %xmm0
	movl	$4294967295, %eax
	movq	%r12, 40(%rbx)
	movq	%r13, 88(%rbx)
	movq	$0, 48(%rbx)
	movq	$0, 56(%rbx)
	movq	$0, 64(%rbx)
	movq	%r8, 72(%rbx)
	movq	$0, 80(%rbx)
	movq	$0, 96(%rbx)
	movb	$0, 104(%rbx)
	movq	%rax, 112(%rbx)
	movups	%xmm0, 120(%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L129:
	.cfi_restore_state
	xorl	%r14d, %r14d
	xorl	%eax, %eax
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L141:
	movq	%r9, -48(%rbp)
	movq	%r8, -40(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-40(%rbp), %r8
	movq	-48(%rbp), %r9
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L130:
	xorl	%esi, %esi
	.p2align 4,,10
	.p2align 3
.L124:
	movq	(%rcx,%rsi,8), %r9
	movq	%r9, (%rax,%rsi,8)
	movq	%rsi, %r9
	addq	$1, %rsi
	cmpq	%rdx, %r9
	jne	.L124
	jmp	.L128
	.cfi_endproc
.LFE19317:
	.size	_ZN2v88internal8compiler18PropertyAccessInfoC2EPNS0_4ZoneENS2_4KindENS0_11MaybeHandleINS0_8JSObjectEEENS0_6HandleINS0_6ObjectEEEONS0_10ZoneVectorINS9_INS0_3MapEEEEE, .-_ZN2v88internal8compiler18PropertyAccessInfoC2EPNS0_4ZoneENS2_4KindENS0_11MaybeHandleINS0_8JSObjectEEENS0_6HandleINS0_6ObjectEEEONS0_10ZoneVectorINS9_INS0_3MapEEEEE
	.globl	_ZN2v88internal8compiler18PropertyAccessInfoC1EPNS0_4ZoneENS2_4KindENS0_11MaybeHandleINS0_8JSObjectEEENS0_6HandleINS0_6ObjectEEEONS0_10ZoneVectorINS9_INS0_3MapEEEEE
	.set	_ZN2v88internal8compiler18PropertyAccessInfoC1EPNS0_4ZoneENS2_4KindENS0_11MaybeHandleINS0_8JSObjectEEENS0_6HandleINS0_6ObjectEEEONS0_10ZoneVectorINS9_INS0_3MapEEEEE,_ZN2v88internal8compiler18PropertyAccessInfoC2EPNS0_4ZoneENS2_4KindENS0_11MaybeHandleINS0_8JSObjectEEENS0_6HandleINS0_6ObjectEEEONS0_10ZoneVectorINS9_INS0_3MapEEEEE
	.section	.text._ZN2v88internal8compiler18PropertyAccessInfoC2ENS2_4KindENS0_11MaybeHandleINS0_8JSObjectEEENS4_INS0_3MapEEENS0_10FieldIndexENS0_14RepresentationENS1_4TypeENS0_6HandleIS7_EES8_ONS0_10ZoneVectorISD_EEONSE_IPKNS1_21CompilationDependencyEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18PropertyAccessInfoC2ENS2_4KindENS0_11MaybeHandleINS0_8JSObjectEEENS4_INS0_3MapEEENS0_10FieldIndexENS0_14RepresentationENS1_4TypeENS0_6HandleIS7_EES8_ONS0_10ZoneVectorISD_EEONSE_IPKNS1_21CompilationDependencyEEE
	.type	_ZN2v88internal8compiler18PropertyAccessInfoC2ENS2_4KindENS0_11MaybeHandleINS0_8JSObjectEEENS4_INS0_3MapEEENS0_10FieldIndexENS0_14RepresentationENS1_4TypeENS0_6HandleIS7_EES8_ONS0_10ZoneVectorISD_EEONSE_IPKNS1_21CompilationDependencyEEE, @function
_ZN2v88internal8compiler18PropertyAccessInfoC2ENS2_4KindENS0_11MaybeHandleINS0_8JSObjectEEENS4_INS0_3MapEEENS0_10FieldIndexENS0_14RepresentationENS1_4TypeENS0_6HandleIS7_EES8_ONS0_10ZoneVectorISD_EEONSE_IPKNS1_21CompilationDependencyEEE:
.LFB19320:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	40(%rbp), %rdx
	movq	48(%rbp), %r12
	movl	%esi, (%rdi)
	movq	(%rdx), %rdi
	movq	16(%rdx), %rax
	subq	8(%rdx), %rax
	movq	$0, 16(%rbx)
	movq	%rdi, 8(%rbx)
	movq	$0, 24(%rbx)
	movq	$0, 32(%rbx)
	je	.L152
	leaq	7(%rax), %rsi
	movq	24(%rdi), %rcx
	movq	%rax, %r15
	movq	16(%rdi), %rax
	andq	$-8, %rsi
	subq	%rax, %rcx
	cmpq	%rcx, %rsi
	ja	.L164
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L143:
	movq	%rax, %xmm0
	addq	%rax, %r15
	punpcklqdq	%xmm0, %xmm0
	movq	%r15, 32(%rbx)
	movups	%xmm0, 16(%rbx)
	movq	16(%rdx), %rdi
	movq	8(%rdx), %rsi
	cmpq	%rsi, %rdi
	je	.L146
	leaq	-8(%rdi), %rdx
	leaq	15(%rsi), %rcx
	subq	%rsi, %rdx
	subq	%rax, %rcx
	movq	%rdx, %r10
	shrq	$3, %r10
	cmpq	$30, %rcx
	jbe	.L153
	movabsq	$2305843009213693948, %rcx
	testq	%rcx, %r10
	je	.L153
	addq	$1, %r10
	xorl	%edi, %edi
	movq	%r10, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L148:
	movdqu	(%rsi,%rdi), %xmm1
	movups	%xmm1, (%rax,%rdi)
	addq	$16, %rdi
	cmpq	%rcx, %rdi
	jne	.L148
	movq	%r10, %rdi
	andq	$-2, %rdi
	leaq	0(,%rdi,8), %rcx
	addq	%rcx, %rsi
	addq	%rax, %rcx
	cmpq	%rdi, %r10
	je	.L151
	movq	(%rsi), %rsi
	movq	%rsi, (%rcx)
.L151:
	leaq	8(%rax,%rdx), %rax
.L146:
	movq	%rax, 24(%rbx)
	movq	(%r12), %rax
	pxor	%xmm0, %xmm0
	movq	%rax, 40(%rbx)
	movq	24(%r12), %rax
	movq	$0, 24(%r12)
	movdqu	8(%r12), %xmm2
	movq	%rax, 64(%rbx)
	movq	16(%rbp), %rax
	movups	%xmm0, 8(%r12)
	movq	%rax, 112(%rbx)
	movq	24(%rbp), %rax
	movq	%r14, 80(%rbx)
	movq	%rax, 120(%rbx)
	movq	32(%rbp), %rax
	movq	%r13, 88(%rbx)
	movq	$0, 72(%rbx)
	movq	%r8, 96(%rbx)
	movb	%r9b, 104(%rbx)
	movq	%rax, 128(%rbx)
	movups	%xmm2, 48(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L152:
	.cfi_restore_state
	xorl	%r15d, %r15d
	xorl	%eax, %eax
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L164:
	movb	%r9b, -57(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %r8
	movzbl	-57(%rbp), %r9d
	movq	40(%rbp), %rdx
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L153:
	xorl	%edi, %edi
	.p2align 4,,10
	.p2align 3
.L147:
	movq	(%rsi,%rdi,8), %rcx
	movq	%rcx, (%rax,%rdi,8)
	movq	%rdi, %rcx
	addq	$1, %rdi
	cmpq	%r10, %rcx
	jne	.L147
	jmp	.L151
	.cfi_endproc
.LFE19320:
	.size	_ZN2v88internal8compiler18PropertyAccessInfoC2ENS2_4KindENS0_11MaybeHandleINS0_8JSObjectEEENS4_INS0_3MapEEENS0_10FieldIndexENS0_14RepresentationENS1_4TypeENS0_6HandleIS7_EES8_ONS0_10ZoneVectorISD_EEONSE_IPKNS1_21CompilationDependencyEEE, .-_ZN2v88internal8compiler18PropertyAccessInfoC2ENS2_4KindENS0_11MaybeHandleINS0_8JSObjectEEENS4_INS0_3MapEEENS0_10FieldIndexENS0_14RepresentationENS1_4TypeENS0_6HandleIS7_EES8_ONS0_10ZoneVectorISD_EEONSE_IPKNS1_21CompilationDependencyEEE
	.globl	_ZN2v88internal8compiler18PropertyAccessInfoC1ENS2_4KindENS0_11MaybeHandleINS0_8JSObjectEEENS4_INS0_3MapEEENS0_10FieldIndexENS0_14RepresentationENS1_4TypeENS0_6HandleIS7_EES8_ONS0_10ZoneVectorISD_EEONSE_IPKNS1_21CompilationDependencyEEE
	.set	_ZN2v88internal8compiler18PropertyAccessInfoC1ENS2_4KindENS0_11MaybeHandleINS0_8JSObjectEEENS4_INS0_3MapEEENS0_10FieldIndexENS0_14RepresentationENS1_4TypeENS0_6HandleIS7_EES8_ONS0_10ZoneVectorISD_EEONSE_IPKNS1_21CompilationDependencyEEE,_ZN2v88internal8compiler18PropertyAccessInfoC2ENS2_4KindENS0_11MaybeHandleINS0_8JSObjectEEENS4_INS0_3MapEEENS0_10FieldIndexENS0_14RepresentationENS1_4TypeENS0_6HandleIS7_EES8_ONS0_10ZoneVectorISD_EEONSE_IPKNS1_21CompilationDependencyEEE
	.section	.rodata._ZNK2v88internal8compiler18PropertyAccessInfo17GetConstFieldInfoEv.str1.1,"aMS",@progbits,1
.LC8:
	.string	"(location_) != nullptr"
	.section	.text._ZNK2v88internal8compiler18PropertyAccessInfo17GetConstFieldInfoEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler18PropertyAccessInfo17GetConstFieldInfoEv
	.type	_ZNK2v88internal8compiler18PropertyAccessInfo17GetConstFieldInfoEv, @function
_ZNK2v88internal8compiler18PropertyAccessInfo17GetConstFieldInfoEv:
.LFB19323:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpl	$3, (%rdi)
	je	.L173
.L168:
	ret
	.p2align 4,,10
	.p2align 3
.L173:
	movq	120(%rdi), %rax
	testq	%rax, %rax
	jne	.L168
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC8(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19323:
	.size	_ZNK2v88internal8compiler18PropertyAccessInfo17GetConstFieldInfoEv, .-_ZNK2v88internal8compiler18PropertyAccessInfo17GetConstFieldInfoEv
	.section	.text._ZN2v88internal8compiler17AccessInfoFactoryC2EPNS1_12JSHeapBrokerEPNS1_23CompilationDependenciesEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17AccessInfoFactoryC2EPNS1_12JSHeapBrokerEPNS1_23CompilationDependenciesEPNS0_4ZoneE
	.type	_ZN2v88internal8compiler17AccessInfoFactoryC2EPNS1_12JSHeapBrokerEPNS1_23CompilationDependenciesEPNS0_4ZoneE, @function
_ZN2v88internal8compiler17AccessInfoFactoryC2EPNS1_12JSHeapBrokerEPNS1_23CompilationDependenciesEPNS0_4ZoneE:
.LFB19325:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %xmm0
	movq	%rdx, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	movups	%xmm0, (%rdi)
	movq	%rcx, -24(%rbp)
	call	_ZN2v88internal8compiler9TypeCache3GetEv@PLT
	movq	%rax, %xmm0
	movhps	-24(%rbp), %xmm0
	movups	%xmm0, 16(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19325:
	.size	_ZN2v88internal8compiler17AccessInfoFactoryC2EPNS1_12JSHeapBrokerEPNS1_23CompilationDependenciesEPNS0_4ZoneE, .-_ZN2v88internal8compiler17AccessInfoFactoryC2EPNS1_12JSHeapBrokerEPNS1_23CompilationDependenciesEPNS0_4ZoneE
	.globl	_ZN2v88internal8compiler17AccessInfoFactoryC1EPNS1_12JSHeapBrokerEPNS1_23CompilationDependenciesEPNS0_4ZoneE
	.set	_ZN2v88internal8compiler17AccessInfoFactoryC1EPNS1_12JSHeapBrokerEPNS1_23CompilationDependenciesEPNS0_4ZoneE,_ZN2v88internal8compiler17AccessInfoFactoryC2EPNS1_12JSHeapBrokerEPNS1_23CompilationDependenciesEPNS0_4ZoneE
	.section	.text._ZNK2v88internal8compiler17AccessInfoFactory24ComputeElementAccessInfoENS0_6HandleINS0_3MapEEENS1_10AccessModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler17AccessInfoFactory24ComputeElementAccessInfoENS0_6HandleINS0_3MapEEENS1_10AccessModeE
	.type	_ZNK2v88internal8compiler17AccessInfoFactory24ComputeElementAccessInfoENS0_6HandleINS0_3MapEEENS1_10AccessModeE, @function
_ZNK2v88internal8compiler17AccessInfoFactory24ComputeElementAccessInfoENS0_6HandleINS0_3MapEEENS1_10AccessModeE:
.LFB19330:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r14, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler22CanInlineElementAccessERKNS1_6MapRefE@PLT
	testb	%al, %al
	jne	.L177
	movb	$0, (%r12)
	movb	$0, 8(%r12)
.L176:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L185
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L177:
	.cfi_restore_state
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler6MapRef13elements_kindEv@PLT
	movq	24(%rbx), %rbx
	movl	%eax, %r15d
	movq	16(%rbx), %r14
	movq	24(%rbx), %rax
	subq	%r14, %rax
	cmpq	$7, %rax
	jbe	.L186
	leaq	8(%r14), %rax
	movq	%rax, 16(%rbx)
.L180:
	movq	%r13, (%r14)
	movq	16(%rbx), %rax
	movq	24(%rbx), %rdx
	subq	%rax, %rdx
	cmpq	$7, %rdx
	jbe	.L187
	leaq	8(%rax), %rdx
	movq	%rdx, 16(%rbx)
.L182:
	movq	(%r14), %rcx
	pxor	%xmm0, %xmm0
	movq	%rcx, (%rax)
	movb	$1, (%r12)
	movb	%r15b, 8(%r12)
	movq	%rbx, 16(%r12)
	movq	%rax, 24(%r12)
	movq	%rdx, 32(%r12)
	movq	%rdx, 40(%r12)
	movq	%rbx, 48(%r12)
	movq	$0, 56(%r12)
	movups	%xmm0, 64(%r12)
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L186:
	movl	$8, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r14
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L187:
	movl	$8, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	leaq	8(%rax), %rdx
	jmp	.L182
.L185:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19330:
	.size	_ZNK2v88internal8compiler17AccessInfoFactory24ComputeElementAccessInfoENS0_6HandleINS0_3MapEEENS1_10AccessModeE, .-_ZNK2v88internal8compiler17AccessInfoFactory24ComputeElementAccessInfoENS0_6HandleINS0_3MapEEENS1_10AccessModeE
	.section	.text._ZN2v88internal8compiler18PropertyAccessInfo18RecordDependenciesEPNS1_23CompilationDependenciesE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18PropertyAccessInfo18RecordDependenciesEPNS1_23CompilationDependenciesE
	.type	_ZN2v88internal8compiler18PropertyAccessInfo18RecordDependenciesEPNS1_23CompilationDependenciesE, @function
_ZN2v88internal8compiler18PropertyAccessInfo18RecordDependenciesEPNS1_23CompilationDependenciesE:
.LFB19368:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	48(%rdi), %rbx
	movq	56(%rdi), %r14
	cmpq	%r14, %rbx
	je	.L188
	movq	%rdi, %r13
	movq	%rsi, %r12
	.p2align 4,,10
	.p2align 3
.L190:
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	addq	$8, %rbx
	call	_ZN2v88internal8compiler23CompilationDependencies16RecordDependencyEPKNS1_21CompilationDependencyE@PLT
	cmpq	%rbx, %r14
	jne	.L190
	movq	48(%r13), %rax
	cmpq	56(%r13), %rax
	je	.L188
	movq	%rax, 56(%r13)
.L188:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19368:
	.size	_ZN2v88internal8compiler18PropertyAccessInfo18RecordDependenciesEPNS1_23CompilationDependenciesE, .-_ZN2v88internal8compiler18PropertyAccessInfo18RecordDependenciesEPNS1_23CompilationDependenciesE
	.section	.text._ZNK2v88internal8compiler17AccessInfoFactory7isolateEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler17AccessInfoFactory7isolateEv
	.type	_ZNK2v88internal8compiler17AccessInfoFactory7isolateEv, @function
_ZNK2v88internal8compiler17AccessInfoFactory7isolateEv:
.LFB19371:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	(%rax), %rax
	ret
	.cfi_endproc
.LFE19371:
	.size	_ZNK2v88internal8compiler17AccessInfoFactory7isolateEv, .-_ZNK2v88internal8compiler17AccessInfoFactory7isolateEv
	.section	.rodata._ZNK2v88internal8compiler17AccessInfoFactory22ConsolidateElementLoadERKNS1_21ElementAccessFeedbackE.str1.1,"aMS",@progbits,1
.LC9:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNK2v88internal8compiler17AccessInfoFactory22ConsolidateElementLoadERKNS1_21ElementAccessFeedbackE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler17AccessInfoFactory22ConsolidateElementLoadERKNS1_21ElementAccessFeedbackE
	.type	_ZNK2v88internal8compiler17AccessInfoFactory22ConsolidateElementLoadERKNS1_21ElementAccessFeedbackE, @function
_ZNK2v88internal8compiler17AccessInfoFactory22ConsolidateElementLoadERKNS1_21ElementAccessFeedbackE:
.LFB19373:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$136, %rsp
	movq	%rdi, -136(%rbp)
	movq	%rdx, %rdi
	movq	%rsi, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal8compiler21ElementAccessFeedback17transition_groupsEv@PLT
	movq	16(%rax), %rsi
	cmpq	%rsi, 8(%rax)
	je	.L286
	movq	%r12, %rdi
	leaq	-96(%rbp), %r13
	call	_ZNK2v88internal8compiler21ElementAccessFeedback17transition_groupsEv@PLT
	movq	-104(%rbp), %rbx
	movl	$1, %ecx
	movq	%r13, %rdi
	movq	8(%rax), %rax
	movq	(%rbx), %rsi
	movq	8(%rax), %rax
	movq	(%rax), %rdx
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler6MapRef13instance_typeEv@PLT
	movq	%r13, %rdi
	movw	%ax, -114(%rbp)
	call	_ZNK2v88internal8compiler6MapRef13elements_kindEv@PLT
	movq	%r12, %rdi
	movl	%eax, %r14d
	movq	24(%rbx), %rax
	movq	%rax, -152(%rbp)
	call	_ZNK2v88internal8compiler21ElementAccessFeedback17transition_groupsEv@PLT
	movq	8(%rax), %rsi
	movq	16(%rax), %rax
	movq	%rsi, -144(%rbp)
	movq	%rax, -176(%rbp)
	cmpq	%rax, %rsi
	je	.L227
	movq	$0, -112(%rbp)
	xorl	%r12d, %r12d
	xorl	%r15d, %r15d
.L223:
	movq	-144(%rbp), %rax
	movq	8(%rax), %r13
	movq	16(%rax), %rax
	movq	%rax, -128(%rbp)
	cmpq	%r13, %rax
	je	.L198
	leaq	-80(%rbp), %rbx
	.p2align 4,,10
	.p2align 3
.L222:
	movq	-104(%rbp), %rax
	movq	0(%r13), %rdx
	movq	%rbx, %rdi
	movl	$1, %ecx
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler6MapRef13instance_typeEv@PLT
	cmpw	-114(%rbp), %ax
	jne	.L210
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler22CanInlineElementAccessERKNS1_6MapRefE@PLT
	testb	%al, %al
	je	.L210
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler6MapRef13elements_kindEv@PLT
	movl	%eax, %edx
	cmpb	$5, %r14b
	ja	.L202
	testb	$1, %r14b
	jne	.L287
.L202:
	cmpb	$5, %dl
	ja	.L203
	testb	$1, %dl
	jne	.L288
.L203:
	cmpb	%dl, %r14b
	je	.L208
.L234:
	leal	-4(%rdx), %eax
	cmpb	$1, %al
	leal	-4(%r14), %eax
	setbe	%cl
	cmpb	$1, %al
	setbe	%al
	cmpb	%al, %cl
	je	.L209
.L210:
	movq	-136(%rbp), %rax
	movb	$0, (%rax)
	movb	$0, 8(%rax)
.L194:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L289
	movq	-136(%rbp), %rax
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L209:
	.cfi_restore_state
	movzbl	%dl, %r9d
	movzbl	%r14b, %r8d
	movb	%dl, -115(%rbp)
	movl	%r8d, %esi
	movl	%r9d, %edi
	movl	%r8d, -168(%rbp)
	movl	%r9d, -160(%rbp)
	call	_ZN2v88internal35IsMoreGeneralElementsKindTransitionENS0_12ElementsKindES1_@PLT
	movl	-160(%rbp), %r9d
	movl	-168(%rbp), %r8d
	testb	%al, %al
	movzbl	-115(%rbp), %edx
	jne	.L208
	movl	%r9d, %esi
	movl	%r8d, %edi
	movb	%dl, -160(%rbp)
	call	_ZN2v88internal35IsMoreGeneralElementsKindTransitionENS0_12ElementsKindES1_@PLT
	movzbl	-160(%rbp), %edx
	testb	%al, %al
	je	.L210
	movl	%edx, %r14d
	.p2align 4,,10
	.p2align 3
.L208:
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler6MapRef6objectEv@PLT
	movq	%rax, %rdx
	cmpq	-112(%rbp), %r12
	je	.L290
	movq	%rdx, (%r12)
	addq	$8, %r12
.L211:
	addq	$8, %r13
	cmpq	%r13, -128(%rbp)
	jne	.L222
.L198:
	addq	$32, -144(%rbp)
	movq	-144(%rbp), %rax
	cmpq	%rax, -176(%rbp)
	jne	.L223
	movq	-104(%rbp), %rax
	movq	%r12, %r13
	movq	24(%rax), %rbx
	subq	%r15, %r13
	je	.L251
	movq	-152(%rbp), %rax
	leaq	7(%r13), %rsi
	andq	$-8, %rsi
	movq	16(%rax), %rdx
	movq	24(%rax), %rax
	movq	%rax, -104(%rbp)
	subq	%rdx, %rax
	cmpq	%rax, %rsi
	ja	.L291
	movq	-152(%rbp), %rax
	addq	%rdx, %rsi
	movq	%rsi, 16(%rax)
.L226:
	leaq	(%rdx,%r13), %rsi
.L224:
	cmpq	%r15, %r12
	je	.L227
	subq	$8, %r12
	leaq	15(%r15), %rax
	subq	%r15, %r12
	subq	%rdx, %rax
	movq	%r12, %rdi
	shrq	$3, %rdi
	cmpq	$30, %rax
	jbe	.L252
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rdi
	je	.L252
	addq	$1, %rdi
	xorl	%eax, %eax
	movq	%rdi, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L229:
	movdqu	(%r15,%rax), %xmm2
	movups	%xmm2, (%rdx,%rax)
	addq	$16, %rax
	cmpq	%rcx, %rax
	jne	.L229
	movq	%rdi, %r8
	andq	$-2, %r8
	leaq	0(,%r8,8), %rcx
	leaq	(%r15,%rcx), %rax
	addq	%rdx, %rcx
	cmpq	%rdi, %r8
	je	.L231
	movq	(%rax), %rax
	movq	%rax, (%rcx)
.L231:
	movq	-152(%rbp), %xmm0
	movq	%rdx, %xmm3
	leaq	8(%rdx,%r12), %rax
	movq	-136(%rbp), %rcx
	punpcklqdq	%xmm3, %xmm0
	movb	$1, (%rcx)
	movups	%xmm0, 16(%rcx)
	pxor	%xmm0, %xmm0
	movb	%r14b, 8(%rcx)
	movq	%rax, 32(%rcx)
	movq	%rsi, 40(%rcx)
	movq	%rbx, 48(%rcx)
	movq	$0, 56(%rcx)
	movups	%xmm0, 64(%rcx)
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L287:
	testb	%al, %al
	je	.L292
	cmpb	$4, %al
	je	.L237
	cmpb	$2, %al
	je	.L238
	cmpb	$6, %al
	jne	.L203
	movl	$7, %edx
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L288:
	testb	%r14b, %r14b
	je	.L240
	cmpb	$4, %r14b
	je	.L241
	cmpb	$2, %r14b
	je	.L242
	cmpb	$6, %r14b
	jne	.L203
	movl	$7, %r14d
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L290:
	movq	%r12, %r8
	subq	%r15, %r8
	movq	%r8, %rax
	sarq	$3, %rax
	cmpq	$268435455, %rax
	je	.L293
	testq	%rax, %rax
	je	.L246
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L294
	movl	$2147483640, %esi
	movl	$2147483640, %ecx
.L213:
	movq	-152(%rbp), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdi
	subq	%rax, %rdi
	cmpq	%rsi, %rdi
	jb	.L295
	movq	-152(%rbp), %rdi
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L216:
	addq	%rax, %rcx
	movq	%rdx, (%rax,%r8)
	movq	%rcx, -112(%rbp)
	leaq	8(%rax), %rcx
	cmpq	%r15, %r12
	jne	.L296
.L249:
	movq	%rcx, %r12
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L294:
	testq	%rcx, %rcx
	jne	.L297
	xorl	%eax, %eax
	movq	$0, -112(%rbp)
	movl	$8, %ecx
	movq	%rdx, (%rax,%r8)
	cmpq	%r15, %r12
	je	.L249
.L296:
	subq	$8, %r12
	leaq	15(%rax), %rdx
	subq	%r15, %r12
	subq	%r15, %rdx
	movq	%r12, %rcx
	shrq	$3, %rcx
	cmpq	$30, %rdx
	jbe	.L250
	movabsq	$2305843009213693948, %rsi
	testq	%rsi, %rcx
	je	.L250
	leaq	1(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L219:
	movdqu	(%r15,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rcx
	jne	.L219
	movq	%rsi, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rcx
	addq	%rcx, %r15
	addq	%rax, %rcx
	cmpq	%rdx, %rsi
	je	.L221
	movq	(%r15), %rdx
	movq	%rdx, (%rcx)
.L221:
	leaq	16(%rax,%r12), %r12
.L217:
	movq	%rax, %r15
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L292:
	movl	$1, %edx
	jmp	.L203
	.p2align 4,,10
	.p2align 3
.L286:
	movb	$0, (%rbx)
	movb	$0, 8(%rbx)
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L240:
	movl	$1, %r14d
	jmp	.L203
	.p2align 4,,10
	.p2align 3
.L246:
	movl	$8, %esi
	movl	$8, %ecx
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L250:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L218:
	movq	(%r15,%rdx,8), %rsi
	movq	%rsi, (%rax,%rdx,8)
	movq	%rdx, %rsi
	addq	$1, %rdx
	cmpq	%rsi, %rcx
	jne	.L218
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L237:
	movl	$5, %edx
	jmp	.L203
	.p2align 4,,10
	.p2align 3
.L238:
	movl	$3, %edx
	jmp	.L203
	.p2align 4,,10
	.p2align 3
.L241:
	movl	$5, %r14d
	jmp	.L203
	.p2align 4,,10
	.p2align 3
.L242:
	movl	$3, %r14d
	jmp	.L203
.L295:
	movq	-152(%rbp), %rdi
	movq	%rcx, -168(%rbp)
	movq	%rdx, -160(%rbp)
	movq	%r8, -112(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-112(%rbp), %r8
	movq	-160(%rbp), %rdx
	movq	-168(%rbp), %rcx
	jmp	.L216
.L252:
	xorl	%eax, %eax
.L228:
	movq	(%r15,%rax,8), %rcx
	movq	%rcx, (%rdx,%rax,8)
	movq	%rax, %rcx
	addq	$1, %rax
	cmpq	%rdi, %rcx
	jne	.L228
	jmp	.L231
.L227:
	leaq	.LC7(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L291:
	movq	-152(%rbp), %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L226
.L293:
	leaq	.LC9(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L289:
	call	__stack_chk_fail@PLT
.L251:
	xorl	%esi, %esi
	xorl	%edx, %edx
	jmp	.L224
.L297:
	cmpq	$268435455, %rcx
	movl	$268435455, %eax
	cmova	%rax, %rcx
	salq	$3, %rcx
	movq	%rcx, %rsi
	jmp	.L213
	.cfi_endproc
.LFE19373:
	.size	_ZNK2v88internal8compiler17AccessInfoFactory22ConsolidateElementLoadERKNS1_21ElementAccessFeedbackE, .-_ZNK2v88internal8compiler17AccessInfoFactory22ConsolidateElementLoadERKNS1_21ElementAccessFeedbackE
	.section	.text._ZNK2v88internal8compiler17AccessInfoFactory26LookupSpecialFieldAccessorENS0_6HandleINS0_3MapEEENS3_INS0_4NameEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler17AccessInfoFactory26LookupSpecialFieldAccessorENS0_6HandleINS0_3MapEEENS3_INS0_4NameEEE
	.type	_ZNK2v88internal8compiler17AccessInfoFactory26LookupSpecialFieldAccessorENS0_6HandleINS0_3MapEEENS3_INS0_4NameEEE, @function
_ZNK2v88internal8compiler17AccessInfoFactory26LookupSpecialFieldAccessorENS0_6HandleINS0_3MapEEENS3_INS0_4NameEEE:
.LFB19375:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	(%rdx), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	cmpw	$63, 11(%rdx)
	ja	.L299
	movq	(%rax), %rdi
	leaq	2768(%rdi), %rdx
	cmpq	%rcx, %rdx
	je	.L301
	movq	(%rcx), %rax
	cmpq	%rax, 2768(%rdi)
	je	.L301
	movq	-1(%rax), %rcx
	movzwl	11(%rcx), %ecx
	andl	$65504, %ecx
	jne	.L306
	movq	2768(%rdi), %rcx
	movq	-1(%rcx), %rcx
	movzwl	11(%rcx), %ecx
	andl	$65504, %ecx
	je	.L309
.L306:
	movq	-1(%rax), %rax
	cmpw	$64, 11(%rax)
	je	.L309
	movq	2768(%rdi), %rax
	movq	-1(%rax), %rax
	cmpw	$64, 11(%rax)
	je	.L309
	movq	%r8, %rsi
	call	_ZN2v88internal6String10SlowEqualsEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	testb	%al, %al
	jne	.L301
	.p2align 4,,10
	.p2align 3
.L309:
	movq	24(%rbx), %rax
	movl	$0, (%r12)
	pxor	%xmm0, %xmm0
	movq	$0, 16(%r12)
	movq	%rax, 8(%r12)
	movq	$0, 24(%r12)
	movq	$0, 32(%r12)
	movq	%rax, 40(%r12)
	movq	$0, 48(%r12)
	movq	$0, 56(%r12)
	movq	$0, 64(%r12)
	movq	$0, 72(%r12)
	movq	$0, 80(%r12)
	movq	$0, 88(%r12)
	movq	$0, 96(%r12)
	movb	$0, 104(%r12)
	movq	$1, 112(%r12)
	movups	%xmm0, 120(%r12)
.L298:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L324
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L299:
	.cfi_restore_state
	movq	$0, -64(%rbp)
	movq	(%rax), %rdi
	leaq	-64(%rbp), %rcx
	movq	%r8, %rdx
	movq	%r13, %rsi
	call	_ZN2v88internal9Accessors23IsJSObjectFieldAccessorEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_4NameEEEPNS0_10FieldIndexE@PLT
	testb	%al, %al
	je	.L309
	movq	0(%r13), %rax
	movl	$4, %ecx
	movl	$209682431, %r15d
	cmpw	$1061, 11(%rax)
	je	.L325
.L310:
	movq	24(%rbx), %rbx
	movq	-64(%rbp), %r8
	movq	16(%rbx), %r14
	movq	24(%rbx), %rax
	subq	%r14, %rax
	cmpq	$7, %rax
	jbe	.L326
	leaq	8(%r14), %rax
	movq	%rax, 16(%rbx)
.L314:
	movq	%r13, (%r14)
	movl	$2, (%r12)
	movq	%rbx, 8(%r12)
	movq	$0, 16(%r12)
	movq	$0, 24(%r12)
	movq	$0, 32(%r12)
	movq	16(%rbx), %rax
	movq	24(%rbx), %rdx
	subq	%rax, %rdx
	cmpq	$7, %rdx
	jbe	.L327
	leaq	8(%rax), %rdx
	movq	%rdx, 16(%rbx)
.L316:
	movq	%rax, 24(%r12)
	movq	%rax, 16(%r12)
	movq	%rdx, 32(%r12)
	movq	(%r14), %rsi
	movq	%rsi, (%rax)
	movq	%rdx, 24(%r12)
	movq	%rbx, 40(%r12)
	movq	$0, 48(%r12)
	movq	$0, 56(%r12)
	movq	$0, 64(%r12)
	movq	$0, 72(%r12)
	movq	$0, 80(%r12)
	movq	$0, 88(%r12)
	movq	%r8, 96(%r12)
	movb	%cl, 104(%r12)
	movq	%r15, 112(%r12)
	movq	%r13, 120(%r12)
	movq	$0, 128(%r12)
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L301:
	movq	24(%rbx), %rsi
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler18PropertyAccessInfo12StringLengthEPNS0_4ZoneENS0_6HandleINS0_3MapEEE
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L325:
	movzbl	14(%rax), %eax
	movq	16(%rbx), %rsi
	shrl	$3, %eax
	leal	-4(%rax), %edx
	cmpb	$1, %dl
	jbe	.L328
	cmpb	$5, %al
	ja	.L312
	movq	416(%rsi), %r15
	movl	$1, %ecx
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L312:
	movq	432(%rsi), %r15
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L328:
	movq	424(%rsi), %r15
	movl	$1, %ecx
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L327:
	movl	$8, %esi
	movq	%rbx, %rdi
	movq	%r8, -80(%rbp)
	movb	%cl, -65(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movzbl	-65(%rbp), %ecx
	movq	-80(%rbp), %r8
	leaq	8(%rax), %rdx
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L326:
	movl	$8, %esi
	movq	%rbx, %rdi
	movq	%r8, -80(%rbp)
	movb	%cl, -65(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movzbl	-65(%rbp), %ecx
	movq	-80(%rbp), %r8
	movq	%rax, %r14
	jmp	.L314
.L324:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19375:
	.size	_ZNK2v88internal8compiler17AccessInfoFactory26LookupSpecialFieldAccessorENS0_6HandleINS0_3MapEEENS3_INS0_4NameEEE, .-_ZNK2v88internal8compiler17AccessInfoFactory26LookupSpecialFieldAccessorENS0_6HandleINS0_3MapEEENS3_INS0_4NameEEE
	.section	.text._ZN2v88internal12BinarySearchILNS0_10SearchModeE1ENS0_15DescriptorArrayEEEiPT0_NS0_4NameEiPi,"axG",@progbits,_ZN2v88internal12BinarySearchILNS0_10SearchModeE1ENS0_15DescriptorArrayEEEiPT0_NS0_4NameEiPi,comdat
	.p2align 4
	.weak	_ZN2v88internal12BinarySearchILNS0_10SearchModeE1ENS0_15DescriptorArrayEEEiPT0_NS0_4NameEiPi
	.type	_ZN2v88internal12BinarySearchILNS0_10SearchModeE1ENS0_15DescriptorArrayEEEiPT0_NS0_4NameEiPi, @function
_ZN2v88internal12BinarySearchILNS0_10SearchModeE1ENS0_15DescriptorArrayEEEiPT0_NS0_4NameEiPi:
.LFB21983:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %rbx
	movswl	9(%rbx), %r10d
	movl	7(%rsi), %r9d
	subl	$1, %r10d
	je	.L340
	movl	%r10d, %r12d
	xorl	%r8d, %r8d
	.p2align 4,,10
	.p2align 3
.L331:
	movl	%r12d, %ecx
	subl	%r8d, %ecx
	movl	%ecx, %eax
	shrl	$31, %eax
	addl	%ecx, %eax
	sarl	%eax
	addl	%r8d, %eax
	leal	1(%rax), %r11d
	leal	(%r11,%r11,2), %ecx
	sall	$3, %ecx
	movslq	%ecx, %rcx
	movq	7(%rcx,%rbx), %rcx
	shrq	$41, %rcx
	andl	$1023, %ecx
	leal	3(%rcx,%rcx,2), %ecx
	sall	$3, %ecx
	movslq	%ecx, %rcx
	movq	-1(%rcx,%rbx), %rcx
	cmpl	7(%rcx), %r9d
	ja	.L345
	cmpl	%r8d, %eax
	je	.L330
	movl	%eax, %r12d
	jmp	.L331
	.p2align 4,,10
	.p2align 3
.L345:
	cmpl	%r12d, %r11d
	je	.L341
	movl	%r11d, %r8d
	jmp	.L331
	.p2align 4,,10
	.p2align 3
.L340:
	xorl	%r8d, %r8d
	.p2align 4,,10
	.p2align 3
.L330:
	leal	3(%r8,%r8,2), %r11d
	sall	$3, %r11d
	movslq	%r11d, %r11
	jmp	.L339
	.p2align 4,,10
	.p2align 3
.L336:
	movq	(%rdi), %rbx
	addl	$1, %r8d
	movq	7(%r11,%rbx), %rax
	shrq	$41, %rax
	andl	$1023, %eax
	leal	3(%rax,%rax,2), %ecx
	sall	$3, %ecx
	movslq	%ecx, %rcx
	movq	-1(%rcx,%rbx), %rcx
	cmpl	7(%rcx), %r9d
	jne	.L338
	addq	$24, %r11
	cmpq	%rcx, %rsi
	je	.L346
.L339:
	cmpl	%r10d, %r8d
	jle	.L336
.L338:
	popq	%rbx
	movl	$-1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L341:
	.cfi_restore_state
	movl	%r12d, %r8d
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L346:
	cmpl	%eax, %edx
	jle	.L338
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21983:
	.size	_ZN2v88internal12BinarySearchILNS0_10SearchModeE1ENS0_15DescriptorArrayEEEiPT0_NS0_4NameEiPi, .-_ZN2v88internal12BinarySearchILNS0_10SearchModeE1ENS0_15DescriptorArrayEEEiPT0_NS0_4NameEiPi
	.section	.text._ZNSt6vectorIN2v88internal8compiler17ElementAccessInfoENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal8compiler17ElementAccessInfoENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal8compiler17ElementAccessInfoENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal8compiler17ElementAccessInfoENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_, @function
_ZNSt6vectorIN2v88internal8compiler17ElementAccessInfoENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_:
.LFB22672:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	movabsq	$-8198552921648689607, %rdx
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %r8
	movq	8(%rdi), %r9
	movq	%r8, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	imulq	%rdx, %rax
	cmpq	$29826161, %rax
	je	.L399
	movq	%rsi, %rdx
	movq	%rdi, %r15
	movq	%rsi, %r14
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L373
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L400
	movl	$2147483592, %esi
	movl	$2147483592, %ebx
.L349:
	movq	(%r15), %rdi
	movq	16(%rdi), %r13
	movq	24(%rdi), %rax
	subq	%r13, %rax
	cmpq	%rax, %rsi
	ja	.L401
	addq	%r13, %rsi
	movq	%rsi, 16(%rdi)
.L352:
	leaq	0(%r13,%rbx), %rax
	leaq	72(%r13), %r10
	movq	%rax, -56(%rbp)
	jmp	.L350
	.p2align 4,,10
	.p2align 3
.L400:
	testq	%rcx, %rcx
	jne	.L402
	movq	$0, -56(%rbp)
	movl	$72, %r10d
	xorl	%r13d, %r13d
.L350:
	movzbl	(%r12), %eax
	leaq	0(%r13,%rdx), %rbx
	movb	%al, (%rbx)
	movq	8(%r12), %rdi
	movq	24(%r12), %rdx
	subq	16(%r12), %rdx
	movq	$0, 16(%rbx)
	movq	%rdi, 8(%rbx)
	movq	$0, 24(%rbx)
	movq	$0, 32(%rbx)
	je	.L376
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	leaq	7(%rdx), %rsi
	andq	$-8, %rsi
	subq	%rax, %rcx
	cmpq	%rcx, %rsi
	ja	.L403
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L353:
	movq	%rax, %xmm0
	addq	%rax, %rdx
	punpcklqdq	%xmm0, %xmm0
	movq	%rdx, 32(%rbx)
	movups	%xmm0, 16(%rbx)
	movq	24(%r12), %rcx
	movq	16(%r12), %rdx
	cmpq	%rdx, %rcx
	je	.L356
	subq	$8, %rcx
	leaq	15(%rdx), %rdi
	subq	%rdx, %rcx
	subq	%rax, %rdi
	movq	%rcx, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rdi
	jbe	.L377
	movabsq	$2305843009213693948, %rdi
	testq	%rdi, %rsi
	je	.L377
	addq	$1, %rsi
	xorl	%edi, %edi
	movq	%rsi, %r11
	shrq	%r11
	salq	$4, %r11
	.p2align 4,,10
	.p2align 3
.L358:
	movdqu	(%rdx,%rdi), %xmm1
	movups	%xmm1, (%rax,%rdi)
	addq	$16, %rdi
	cmpq	%r11, %rdi
	jne	.L358
	movq	%rsi, %r11
	andq	$-2, %r11
	leaq	0(,%r11,8), %rdi
	addq	%rdi, %rdx
	addq	%rax, %rdi
	cmpq	%r11, %rsi
	je	.L360
	movq	(%rdx), %rdx
	movq	%rdx, (%rdi)
.L360:
	leaq	8(%rax,%rcx), %rax
.L356:
	movq	%rax, 24(%rbx)
	movq	40(%r12), %rdi
	movq	56(%r12), %rdx
	subq	48(%r12), %rdx
	movq	$0, 48(%rbx)
	movq	%rdi, 40(%rbx)
	movq	$0, 56(%rbx)
	movq	$0, 64(%rbx)
	je	.L378
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	leaq	7(%rdx), %rsi
	andq	$-8, %rsi
	subq	%rax, %rcx
	cmpq	%rcx, %rsi
	ja	.L404
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L361:
	movq	%rax, %xmm0
	addq	%rax, %rdx
	punpcklqdq	%xmm0, %xmm0
	movq	%rdx, 64(%rbx)
	movups	%xmm0, 48(%rbx)
	movq	56(%r12), %rcx
	movq	48(%r12), %rdx
	cmpq	%rdx, %rcx
	je	.L364
	subq	$8, %rcx
	leaq	15(%rdx), %rsi
	movq	%rcx, %rdi
	subq	%rax, %rsi
	subq	%rdx, %rdi
	movq	%rdi, %rcx
	shrq	$3, %rcx
	cmpq	$30, %rsi
	jbe	.L379
	movabsq	$2305843009213693948, %rsi
	testq	%rsi, %rcx
	je	.L379
	addq	$1, %rcx
	xorl	%esi, %esi
	movq	%rcx, %r11
	shrq	%r11
	salq	$4, %r11
	.p2align 4,,10
	.p2align 3
.L366:
	movdqu	(%rdx,%rsi), %xmm2
	movups	%xmm2, (%rax,%rsi)
	addq	$16, %rsi
	cmpq	%r11, %rsi
	jne	.L366
	movq	%rcx, %r11
	andq	$-2, %r11
	leaq	0(,%r11,8), %rsi
	addq	%rsi, %rdx
	addq	%rax, %rsi
	cmpq	%r11, %rcx
	je	.L368
	movq	(%rdx), %rdx
	movq	%rdx, (%rsi)
.L368:
	leaq	8(%rax,%rdi), %rax
.L364:
	movq	%rax, 56(%rbx)
	cmpq	%r9, %r14
	je	.L369
	movq	%r9, %rax
	movq	%r13, %rdx
	pxor	%xmm0, %xmm0
	.p2align 4,,10
	.p2align 3
.L370:
	movzbl	(%rax), %ecx
	addq	$72, %rax
	addq	$72, %rdx
	movb	%cl, -72(%rdx)
	movq	-64(%rax), %rcx
	movq	%rcx, -64(%rdx)
	movdqu	-56(%rax), %xmm3
	movups	%xmm3, -56(%rdx)
	movq	-40(%rax), %rcx
	movq	%rcx, -40(%rdx)
	movq	-32(%rax), %rcx
	movq	$0, -40(%rax)
	movups	%xmm0, -56(%rax)
	movq	%rcx, -32(%rdx)
	movdqu	-24(%rax), %xmm4
	movups	%xmm4, -24(%rdx)
	movq	-8(%rax), %rcx
	movq	%rcx, -8(%rdx)
	movq	$0, -8(%rax)
	movups	%xmm0, -24(%rax)
	cmpq	%rax, %r14
	jne	.L370
	leaq	-72(%r14), %rax
	subq	%r9, %rax
	shrq	$3, %rax
	leaq	144(%r13,%rax,8), %r10
.L369:
	cmpq	%r8, %r14
	je	.L371
	movq	%r14, %rax
	movq	%r10, %rdx
	pxor	%xmm0, %xmm0
	.p2align 4,,10
	.p2align 3
.L372:
	movzbl	(%rax), %ecx
	addq	$72, %rax
	addq	$72, %rdx
	movb	%cl, -72(%rdx)
	movq	-64(%rax), %rcx
	movq	%rcx, -64(%rdx)
	movdqu	-56(%rax), %xmm5
	movups	%xmm5, -56(%rdx)
	movq	-40(%rax), %rcx
	movq	%rcx, -40(%rdx)
	movq	-32(%rax), %rcx
	movq	$0, -40(%rax)
	movups	%xmm0, -56(%rax)
	movq	%rcx, -32(%rdx)
	movdqu	-24(%rax), %xmm6
	movups	%xmm6, -24(%rdx)
	movq	-8(%rax), %rcx
	movq	%rcx, -8(%rdx)
	movq	$0, -8(%rax)
	movups	%xmm0, -24(%rax)
	cmpq	%rax, %r8
	jne	.L372
	subq	%r14, %r8
	leaq	-72(%r8), %rax
	shrq	$3, %rax
	leaq	72(%r10,%rax,8), %r10
.L371:
	movq	-56(%rbp), %rax
	movq	%r13, %xmm0
	movq	%r10, %xmm7
	punpcklqdq	%xmm7, %xmm0
	movq	%rax, 24(%r15)
	movups	%xmm0, 8(%r15)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L376:
	.cfi_restore_state
	xorl	%edx, %edx
	xorl	%eax, %eax
	jmp	.L353
	.p2align 4,,10
	.p2align 3
.L378:
	xorl	%edx, %edx
	xorl	%eax, %eax
	jmp	.L361
	.p2align 4,,10
	.p2align 3
.L373:
	movl	$72, %esi
	movl	$72, %ebx
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L403:
	movq	%rdx, -88(%rbp)
	movq	%r10, -80(%rbp)
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	movq	-80(%rbp), %r10
	movq	-88(%rbp), %rdx
	jmp	.L353
	.p2align 4,,10
	.p2align 3
.L379:
	xorl	%esi, %esi
	.p2align 4,,10
	.p2align 3
.L365:
	movq	(%rdx,%rsi,8), %r11
	movq	%r11, (%rax,%rsi,8)
	movq	%rsi, %r11
	addq	$1, %rsi
	cmpq	%r11, %rcx
	jne	.L365
	jmp	.L368
	.p2align 4,,10
	.p2align 3
.L377:
	xorl	%edi, %edi
	.p2align 4,,10
	.p2align 3
.L357:
	movq	(%rdx,%rdi,8), %r11
	movq	%r11, (%rax,%rdi,8)
	movq	%rdi, %r11
	addq	$1, %rdi
	cmpq	%r11, %rsi
	jne	.L357
	jmp	.L360
	.p2align 4,,10
	.p2align 3
.L401:
	movq	%rdx, -72(%rbp)
	movq	%r8, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %r8
	movq	-72(%rbp), %rdx
	movq	%rax, %r13
	jmp	.L352
	.p2align 4,,10
	.p2align 3
.L404:
	movq	%r10, -88(%rbp)
	movq	%r8, -80(%rbp)
	movq	%r9, -72(%rbp)
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r9
	movq	-80(%rbp), %r8
	movq	-88(%rbp), %r10
	jmp	.L361
.L399:
	leaq	.LC9(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L402:
	cmpq	$29826161, %rcx
	movl	$29826161, %ebx
	cmova	%rbx, %rcx
	imulq	$72, %rcx, %rbx
	movq	%rbx, %rsi
	jmp	.L349
	.cfi_endproc
.LFE22672:
	.size	_ZNSt6vectorIN2v88internal8compiler17ElementAccessInfoENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_, .-_ZNSt6vectorIN2v88internal8compiler17ElementAccessInfoENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	.section	.rodata._ZNK2v88internal8compiler17AccessInfoFactory25ComputeElementAccessInfosERKNS1_21ElementAccessFeedbackEPNS0_10ZoneVectorINS1_17ElementAccessInfoEEE.str1.1,"aMS",@progbits,1
.LC10:
	.string	"receiver_maps_.size() == 1"
	.section	.text._ZNK2v88internal8compiler17AccessInfoFactory25ComputeElementAccessInfosERKNS1_21ElementAccessFeedbackEPNS0_10ZoneVectorINS1_17ElementAccessInfoEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler17AccessInfoFactory25ComputeElementAccessInfosERKNS1_21ElementAccessFeedbackEPNS0_10ZoneVectorINS1_17ElementAccessInfoEEE
	.type	_ZNK2v88internal8compiler17AccessInfoFactory25ComputeElementAccessInfosERKNS1_21ElementAccessFeedbackEPNS0_10ZoneVectorINS1_17ElementAccessInfoEEE, @function
_ZNK2v88internal8compiler17AccessInfoFactory25ComputeElementAccessInfosERKNS1_21ElementAccessFeedbackEPNS0_10ZoneVectorINS1_17ElementAccessInfoEEE:
.LFB19349:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-144(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -152(%rbp)
	movq	%rsi, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal8compiler21ElementAccessFeedback10keyed_modeEv@PLT
	movq	%r15, %rdi
	movq	%rax, -144(%rbp)
	call	_ZNK2v88internal8compiler15KeyedAccessMode11access_modeEv@PLT
	movl	%eax, %r14d
	testl	%eax, %eax
	je	.L480
	cmpl	$3, %eax
	je	.L480
.L406:
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler21ElementAccessFeedback17transition_groupsEv@PLT
	movq	8(%rax), %r8
	movq	16(%rax), %rax
	movq	%rax, -168(%rbp)
	cmpq	%rax, %r8
	je	.L465
	movl	%r14d, -156(%rbp)
	movq	%r13, %r14
	movq	%r8, %r13
.L428:
	movq	8(%r13), %rax
	movl	-156(%rbp), %ecx
	movq	%r15, %rdi
	movq	-152(%rbp), %rsi
	movq	(%rax), %rdx
	call	_ZNK2v88internal8compiler17AccessInfoFactory24ComputeElementAccessInfoENS0_6HandleINS0_3MapEEENS1_10AccessModeE
	movzbl	-144(%rbp), %ebx
	testb	%bl, %bl
	je	.L405
	movq	8(%r13), %rdx
	movq	16(%r13), %rax
	movl	$1, %ebx
	subq	%rdx, %rax
	cmpq	$15, %rax
	jbe	.L447
	.p2align 4,,10
	.p2align 3
.L430:
	movq	-112(%rbp), %rax
	subq	-120(%rbp), %rax
	movq	(%rdx,%rbx,8), %r8
	cmpq	$8, %rax
	je	.L537
	leaq	.LC10(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L537:
	movq	-80(%rbp), %r12
	cmpq	-72(%rbp), %r12
	je	.L538
	movq	%r8, (%r12)
	addq	$8, -80(%rbp)
.L436:
	movq	8(%r13), %rdx
	movq	16(%r13), %rax
	addq	$1, %rbx
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rax, %rbx
	jb	.L430
.L447:
	movq	16(%r14), %r12
	cmpq	24(%r14), %r12
	je	.L539
	movzbl	-136(%rbp), %eax
	movb	%al, (%r12)
	movq	-128(%rbp), %rdi
	movq	-112(%rbp), %rax
	subq	-120(%rbp), %rax
	movq	$0, 16(%r12)
	movq	%rdi, 8(%r12)
	movq	$0, 24(%r12)
	movq	$0, 32(%r12)
	je	.L476
	leaq	7(%rax), %rsi
	movq	16(%rdi), %rdx
	movq	%rax, %rbx
	movq	24(%rdi), %rax
	andq	$-8, %rsi
	subq	%rdx, %rax
	cmpq	%rax, %rsi
	ja	.L540
	addq	%rdx, %rsi
	movq	%rsi, 16(%rdi)
.L448:
	movq	%rdx, %xmm0
	leaq	(%rdx,%rbx), %rax
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, 32(%r12)
	movups	%xmm0, 16(%r12)
	movq	-112(%rbp), %rax
	movq	-120(%rbp), %rcx
	cmpq	%rcx, %rax
	je	.L451
	leaq	-8(%rax), %rsi
	leaq	15(%rcx), %rax
	subq	%rcx, %rsi
	subq	%rdx, %rax
	movq	%rsi, %r9
	shrq	$3, %r9
	cmpq	$30, %rax
	jbe	.L477
	movabsq	$2305843009213693948, %rax
	testq	%rax, %r9
	je	.L477
	addq	$1, %r9
	xorl	%eax, %eax
	movq	%r9, %rdi
	shrq	%rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L453:
	movdqu	(%rcx,%rax), %xmm2
	movups	%xmm2, (%rdx,%rax)
	addq	$16, %rax
	cmpq	%rdi, %rax
	jne	.L453
	movq	%r9, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rdi
	addq	%rdi, %rcx
	addq	%rdx, %rdi
	cmpq	%rax, %r9
	je	.L455
	movq	(%rcx), %rax
	movq	%rax, (%rdi)
.L455:
	leaq	8(%rdx,%rsi), %rdx
.L451:
	movq	%rdx, 24(%r12)
	movq	-96(%rbp), %rdi
	movq	-80(%rbp), %rax
	subq	-88(%rbp), %rax
	movq	$0, 48(%r12)
	movq	%rdi, 40(%r12)
	movq	$0, 56(%r12)
	movq	$0, 64(%r12)
	je	.L478
	leaq	7(%rax), %rsi
	movq	16(%rdi), %rdx
	movq	%rax, %rbx
	movq	24(%rdi), %rax
	andq	$-8, %rsi
	subq	%rdx, %rax
	cmpq	%rax, %rsi
	ja	.L541
	addq	%rdx, %rsi
	movq	%rsi, 16(%rdi)
.L456:
	movq	%rdx, %xmm0
	leaq	(%rdx,%rbx), %rax
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, 64(%r12)
	movups	%xmm0, 48(%r12)
	movq	-80(%rbp), %rax
	movq	-88(%rbp), %rcx
	cmpq	%rcx, %rax
	je	.L459
	leaq	-8(%rax), %rsi
	leaq	15(%rcx), %rax
	subq	%rcx, %rsi
	subq	%rdx, %rax
	movq	%rsi, %r9
	shrq	$3, %r9
	cmpq	$30, %rax
	jbe	.L479
	movabsq	$2305843009213693948, %rax
	testq	%rax, %r9
	je	.L479
	addq	$1, %r9
	xorl	%eax, %eax
	movq	%r9, %rdi
	shrq	%rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L461:
	movdqu	(%rcx,%rax), %xmm3
	movups	%xmm3, (%rdx,%rax)
	addq	$16, %rax
	cmpq	%rdi, %rax
	jne	.L461
	movq	%r9, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rdi
	addq	%rdi, %rcx
	addq	%rdx, %rdi
	cmpq	%rax, %r9
	je	.L463
	movq	(%rcx), %rax
	movq	%rax, (%rdi)
.L463:
	leaq	8(%rdx,%rsi), %rdx
.L459:
	movq	%rdx, 56(%r12)
	addq	$72, 16(%r14)
.L464:
	addq	$32, %r13
	cmpq	%r13, -168(%rbp)
	jne	.L428
.L465:
	movl	$1, %ebx
.L405:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L542
	addq	$168, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L538:
	.cfi_restore_state
	movq	-88(%rbp), %rcx
	movq	%r12, %r9
	subq	%rcx, %r9
	movq	%r9, %rax
	sarq	$3, %rax
	cmpq	$268435455, %rax
	je	.L543
	testq	%rax, %rax
	je	.L472
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L544
	movl	$2147483640, %esi
	movl	$2147483640, %r10d
.L438:
	movq	-96(%rbp), %rdi
	movq	16(%rdi), %rdx
	movq	24(%rdi), %rax
	subq	%rdx, %rax
	cmpq	%rsi, %rax
	jb	.L545
	addq	%rdx, %rsi
	movq	%rsi, 16(%rdi)
.L441:
	leaq	(%rdx,%r10), %rsi
	leaq	8(%rdx), %rax
	jmp	.L439
.L544:
	testq	%rdx, %rdx
	jne	.L546
	movl	$8, %eax
	xorl	%esi, %esi
	xorl	%edx, %edx
.L439:
	movq	%r8, (%rdx,%r9)
	cmpq	%rcx, %r12
	je	.L442
	subq	$8, %r12
	leaq	15(%rdx), %rax
	subq	%rcx, %r12
	subq	%rcx, %rax
	movq	%r12, %rdi
	shrq	$3, %rdi
	cmpq	$30, %rax
	jbe	.L475
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rdi
	je	.L475
	leaq	1(%rdi), %r8
	xorl	%eax, %eax
	movq	%r8, %rdi
	shrq	%rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L444:
	movdqu	(%rcx,%rax), %xmm1
	movups	%xmm1, (%rdx,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdi
	jne	.L444
	movq	%r8, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rdi
	addq	%rdi, %rcx
	addq	%rdx, %rdi
	cmpq	%rax, %r8
	je	.L446
	movq	(%rcx), %rax
	movq	%rax, (%rdi)
.L446:
	leaq	16(%rdx,%r12), %rax
.L442:
	movq	%rdx, %xmm0
	movq	%rax, %xmm4
	movq	%rsi, -72(%rbp)
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, -88(%rbp)
	jmp	.L436
.L480:
	movq	-152(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler17AccessInfoFactory22ConsolidateElementLoadERKNS1_21ElementAccessFeedbackE
	movzbl	-144(%rbp), %ebx
	testb	%bl, %bl
	je	.L406
	movq	16(%r13), %r12
	cmpq	24(%r13), %r12
	je	.L409
	movzbl	-136(%rbp), %eax
	movb	%al, (%r12)
	movq	-128(%rbp), %rdi
	movq	-112(%rbp), %r14
	subq	-120(%rbp), %r14
	movq	$0, 16(%r12)
	movq	%rdi, 8(%r12)
	movq	$0, 24(%r12)
	movq	$0, 32(%r12)
	je	.L467
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	leaq	7(%r14), %rsi
	andq	$-8, %rsi
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L547
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L410:
	movq	%rax, %xmm0
	addq	%rax, %r14
	punpcklqdq	%xmm0, %xmm0
	movq	%r14, 32(%r12)
	movups	%xmm0, 16(%r12)
	movq	-112(%rbp), %rdx
	movq	-120(%rbp), %rcx
	cmpq	%rcx, %rdx
	je	.L413
	subq	$8, %rdx
	leaq	15(%rcx), %rsi
	subq	%rcx, %rdx
	subq	%rax, %rsi
	movq	%rdx, %r8
	shrq	$3, %rdx
	cmpq	$30, %rsi
	jbe	.L468
	movabsq	$2305843009213693948, %rsi
	testq	%rsi, %rdx
	je	.L468
	leaq	1(%rdx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rdi
	shrq	%rdi
	salq	$4, %rdi
.L415:
	movdqu	(%rcx,%rdx), %xmm5
	movups	%xmm5, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rdi, %rdx
	jne	.L415
	movq	%rsi, %rdi
	andq	$-2, %rdi
	leaq	0(,%rdi,8), %rdx
	addq	%rdx, %rcx
	addq	%rax, %rdx
	cmpq	%rdi, %rsi
	je	.L417
	movq	(%rcx), %rcx
	movq	%rcx, (%rdx)
.L417:
	leaq	8(%rax,%r8), %rax
.L413:
	movq	%rax, 24(%r12)
	movq	-96(%rbp), %rdi
	movq	-80(%rbp), %r14
	subq	-88(%rbp), %r14
	movq	$0, 48(%r12)
	movq	%rdi, 40(%r12)
	movq	$0, 56(%r12)
	movq	$0, 64(%r12)
	je	.L469
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	leaq	7(%r14), %rsi
	andq	$-8, %rsi
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L548
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L418:
	movq	%rax, %xmm0
	addq	%rax, %r14
	punpcklqdq	%xmm0, %xmm0
	movq	%r14, 64(%r12)
	movups	%xmm0, 48(%r12)
	movq	-80(%rbp), %rdx
	movq	-88(%rbp), %rcx
	cmpq	%rcx, %rdx
	je	.L421
	subq	$8, %rdx
	leaq	15(%rcx), %rsi
	subq	%rcx, %rdx
	subq	%rax, %rsi
	movq	%rdx, %r8
	shrq	$3, %rdx
	cmpq	$30, %rsi
	jbe	.L470
	movabsq	$2305843009213693948, %rsi
	testq	%rsi, %rdx
	je	.L470
	leaq	1(%rdx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rdi
	shrq	%rdi
	salq	$4, %rdi
.L423:
	movdqu	(%rcx,%rdx), %xmm6
	movups	%xmm6, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rdi, %rdx
	jne	.L423
	movq	%rsi, %rdi
	andq	$-2, %rdi
	leaq	0(,%rdi,8), %rdx
	addq	%rdx, %rcx
	addq	%rax, %rdx
	cmpq	%rdi, %rsi
	je	.L425
	movq	(%rcx), %rcx
	movq	%rcx, (%rdx)
.L425:
	leaq	8(%rax,%r8), %rax
.L421:
	movq	%rax, 56(%r12)
	addq	$72, 16(%r13)
	jmp	.L405
.L476:
	xorl	%ebx, %ebx
	xorl	%edx, %edx
	jmp	.L448
.L472:
	movl	$8, %esi
	movl	$8, %r10d
	jmp	.L438
.L478:
	xorl	%ebx, %ebx
	xorl	%edx, %edx
	jmp	.L456
.L539:
	leaq	-136(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZNSt6vectorIN2v88internal8compiler17ElementAccessInfoENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	jmp	.L464
.L475:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L443:
	movq	(%rcx,%rax,8), %r8
	movq	%r8, (%rdx,%rax,8)
	movq	%rax, %r8
	addq	$1, %rax
	cmpq	%rdi, %r8
	jne	.L443
	jmp	.L446
.L479:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L460:
	movq	(%rcx,%rax,8), %rdi
	movq	%rdi, (%rdx,%rax,8)
	movq	%rax, %rdi
	addq	$1, %rax
	cmpq	%r9, %rdi
	jne	.L460
	jmp	.L463
.L477:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L452:
	movq	(%rcx,%rax,8), %rdi
	movq	%rdi, (%rdx,%rax,8)
	movq	%rax, %rdi
	addq	$1, %rax
	cmpq	%r9, %rdi
	jne	.L452
	jmp	.L455
.L545:
	movq	%r8, -200(%rbp)
	movq	%r9, -192(%rbp)
	movq	%rcx, -184(%rbp)
	movq	%r10, -176(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-176(%rbp), %r10
	movq	-184(%rbp), %rcx
	movq	-192(%rbp), %r9
	movq	-200(%rbp), %r8
	movq	%rax, %rdx
	jmp	.L441
.L467:
	xorl	%r14d, %r14d
	xorl	%eax, %eax
	jmp	.L410
.L541:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L456
.L540:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L448
.L469:
	xorl	%r14d, %r14d
	xorl	%eax, %eax
	jmp	.L418
.L409:
	leaq	-136(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNSt6vectorIN2v88internal8compiler17ElementAccessInfoENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	jmp	.L405
.L468:
	xorl	%esi, %esi
.L414:
	movq	(%rcx,%rsi,8), %rdi
	movq	%rdi, (%rax,%rsi,8)
	movq	%rsi, %rdi
	addq	$1, %rsi
	cmpq	%rdi, %rdx
	jne	.L414
	jmp	.L417
.L470:
	xorl	%esi, %esi
.L422:
	movq	(%rcx,%rsi,8), %rdi
	movq	%rdi, (%rax,%rsi,8)
	movq	%rsi, %rdi
	addq	$1, %rsi
	cmpq	%rdi, %rdx
	jne	.L422
	jmp	.L425
.L548:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L418
.L547:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L410
.L546:
	movl	$268435455, %r10d
	cmpq	$268435455, %rdx
	cmova	%r10, %rdx
	leaq	0(,%rdx,8), %r10
	movq	%r10, %rsi
	jmp	.L438
.L543:
	leaq	.LC9(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L542:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19349:
	.size	_ZNK2v88internal8compiler17AccessInfoFactory25ComputeElementAccessInfosERKNS1_21ElementAccessFeedbackEPNS0_10ZoneVectorINS1_17ElementAccessInfoEEE, .-_ZNK2v88internal8compiler17AccessInfoFactory25ComputeElementAccessInfosERKNS1_21ElementAccessFeedbackEPNS0_10ZoneVectorINS1_17ElementAccessInfoEEE
	.section	.text._ZNSt6vectorIPKN2v88internal8compiler21CompilationDependencyENS1_13ZoneAllocatorIS5_EEE12emplace_backIJS5_EEEvDpOT_,"axG",@progbits,_ZNSt6vectorIPKN2v88internal8compiler21CompilationDependencyENS1_13ZoneAllocatorIS5_EEE12emplace_backIJS5_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPKN2v88internal8compiler21CompilationDependencyENS1_13ZoneAllocatorIS5_EEE12emplace_backIJS5_EEEvDpOT_
	.type	_ZNSt6vectorIPKN2v88internal8compiler21CompilationDependencyENS1_13ZoneAllocatorIS5_EEE12emplace_backIJS5_EEEvDpOT_, @function
_ZNSt6vectorIPKN2v88internal8compiler21CompilationDependencyENS1_13ZoneAllocatorIS5_EEE12emplace_backIJS5_EEEvDpOT_:
.LFB22683:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	16(%rdi), %r13
	cmpq	24(%rdi), %r13
	je	.L550
	movq	(%rsi), %rax
	movq	%rax, 0(%r13)
	addq	$8, 16(%rdi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L550:
	.cfi_restore_state
	movq	8(%rdi), %r14
	movq	%r13, %rcx
	subq	%r14, %rcx
	movq	%rcx, %rax
	sarq	$3, %rax
	cmpq	$268435455, %rax
	je	.L575
	testq	%rax, %rax
	je	.L562
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L576
	movl	$2147483640, %esi
	movl	$2147483640, %r15d
.L553:
	movq	(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L577
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L556:
	leaq	(%rax,%r15), %rdi
	leaq	8(%rax), %rdx
.L554:
	movq	(%r12), %rsi
	movq	%rsi, (%rax,%rcx)
	cmpq	%r14, %r13
	je	.L557
	leaq	-8(%r13), %rdx
	movq	%rdx, %rcx
	leaq	15(%rax), %rdx
	subq	%r14, %rcx
	subq	%r14, %rdx
	movq	%rcx, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rdx
	jbe	.L565
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rsi
	je	.L565
	leaq	1(%rsi), %r8
	xorl	%edx, %edx
	movq	%r8, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L559:
	movdqu	(%r14,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rsi, %rdx
	jne	.L559
	movq	%r8, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rsi
	addq	%rsi, %r14
	addq	%rax, %rsi
	cmpq	%rdx, %r8
	je	.L561
	movq	(%r14), %rdx
	movq	%rdx, (%rsi)
.L561:
	leaq	16(%rax,%rcx), %rdx
.L557:
	movq	%rax, %xmm0
	movq	%rdx, %xmm2
	movq	%rdi, 24(%rbx)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 8(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L576:
	.cfi_restore_state
	testq	%rdx, %rdx
	jne	.L578
	movl	$8, %edx
	xorl	%edi, %edi
	xorl	%eax, %eax
	jmp	.L554
	.p2align 4,,10
	.p2align 3
.L562:
	movl	$8, %esi
	movl	$8, %r15d
	jmp	.L553
	.p2align 4,,10
	.p2align 3
.L565:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L558:
	movq	(%r14,%rdx,8), %r8
	movq	%r8, (%rax,%rdx,8)
	movq	%rdx, %r8
	addq	$1, %rdx
	cmpq	%r8, %rsi
	jne	.L558
	jmp	.L561
.L577:
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	jmp	.L556
.L575:
	leaq	.LC9(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L578:
	movl	$268435455, %r15d
	cmpq	$268435455, %rdx
	cmova	%r15, %rdx
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rsi
	jmp	.L553
	.cfi_endproc
.LFE22683:
	.size	_ZNSt6vectorIPKN2v88internal8compiler21CompilationDependencyENS1_13ZoneAllocatorIS5_EEE12emplace_backIJS5_EEEvDpOT_, .-_ZNSt6vectorIPKN2v88internal8compiler21CompilationDependencyENS1_13ZoneAllocatorIS5_EEE12emplace_backIJS5_EEEvDpOT_
	.section	.rodata._ZNK2v88internal8compiler17AccessInfoFactory26ComputeDataFieldAccessInfoENS0_6HandleINS0_3MapEEES5_NS0_11MaybeHandleINS0_8JSObjectEEEiNS1_10AccessModeE.str1.8,"aMS",@progbits,1
	.align 8
.LC11:
	.string	"details_representation.IsTagged()"
	.section	.text._ZNK2v88internal8compiler17AccessInfoFactory26ComputeDataFieldAccessInfoENS0_6HandleINS0_3MapEEES5_NS0_11MaybeHandleINS0_8JSObjectEEEiNS1_10AccessModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler17AccessInfoFactory26ComputeDataFieldAccessInfoENS0_6HandleINS0_3MapEEES5_NS0_11MaybeHandleINS0_8JSObjectEEEiNS1_10AccessModeE
	.type	_ZNK2v88internal8compiler17AccessInfoFactory26ComputeDataFieldAccessInfoENS0_6HandleINS0_3MapEEES5_NS0_11MaybeHandleINS0_8JSObjectEEEiNS1_10AccessModeE, @function
_ZNK2v88internal8compiler17AccessInfoFactory26ComputeDataFieldAccessInfoENS0_6HandleINS0_3MapEEES5_NS0_11MaybeHandleINS0_8JSObjectEEEiNS1_10AccessModeE:
.LFB19351:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%r9d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -152(%rbp)
	movq	%r8, -160(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	(%rax), %rbx
	movq	(%rcx), %rax
	movq	41112(%rbx), %rdi
	movq	39(%rax), %rsi
	testq	%rdi, %rdi
	je	.L580
	movq	%rcx, -136(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-136(%rbp), %r10
	movq	(%rax), %rsi
.L581:
	leal	3(%r14,%r14,2), %r15d
	sall	$3, %r15d
	movslq	%r15d, %r15
	movq	7(%r15,%rsi), %rbx
	movq	(%rax), %rdx
	movq	%rbx, %rcx
	shrq	$38, %rbx
	sarq	$32, %rcx
	movq	%rcx, -144(%rbp)
	movq	7(%r15,%rdx), %rdi
	andl	$7, %ebx
	jne	.L583
.L632:
	movq	24(%r13), %rax
	movl	$0, (%r12)
	pxor	%xmm0, %xmm0
	movq	$0, 16(%r12)
	movq	%rax, 8(%r12)
	movq	$0, 24(%r12)
	movq	$0, 32(%r12)
	movq	%rax, 40(%r12)
	movq	$0, 48(%r12)
	movq	$0, 56(%r12)
	movq	$0, 64(%r12)
	movq	$0, 72(%r12)
	movq	$0, 80(%r12)
	movq	$0, 88(%r12)
	movq	$0, 96(%r12)
	movb	$0, 104(%r12)
	movq	$1, 112(%r12)
	movups	%xmm0, 120(%r12)
.L579:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L633
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L580:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L634
.L582:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L581
	.p2align 4,,10
	.p2align 3
.L583:
	movq	(%r10), %r8
	shrq	$51, %rdi
	andl	$1023, %edi
	movzbl	7(%r8), %edx
	movzbl	8(%r8), %ecx
	subl	%ecx, %edx
	cmpl	%edx, %edi
	setl	%sil
	jl	.L635
	subl	%edx, %edi
	movl	$16, %ecx
	leal	16(,%rdi,8), %edi
.L586:
	cmpl	$2, %ebx
	je	.L614
	cmpb	$2, %bl
	jle	.L636
	leal	-3(%rbx), %r8d
	cmpb	$1, %r8b
	ja	.L589
.L616:
	xorl	%r8d, %r8d
.L587:
	movzbl	%sil, %esi
	movslq	%edx, %rdx
	movslq	%edi, %rdi
	movslq	%ecx, %rcx
	salq	$14, %rsi
	salq	$17, %rdx
	movq	%rax, -176(%rbp)
	leaq	-128(%rbp), %rax
	orq	%rsi, %rdx
	salq	$27, %rcx
	movq	0(%r13), %rsi
	movq	%r10, -168(%rbp)
	orq	%rdi, %rdx
	movq	%rax, %rdi
	movq	%rax, -136(%rbp)
	orq	%rcx, %rdx
	movl	$1, %ecx
	orq	%r8, %rdx
	movq	%rdx, -184(%rbp)
	movq	%r10, %rdx
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	24(%r13), %rdx
	movl	%r14d, %esi
	movq	-136(%rbp), %rdi
	movq	$0, -88(%rbp)
	movq	%rdx, -96(%rbp)
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	call	_ZN2v88internal8compiler6MapRef22SerializeOwnDescriptorEi@PLT
	cmpl	$1, %ebx
	movq	-168(%rbp), %r10
	movq	-176(%rbp), %rax
	je	.L637
	cmpb	$2, %bl
	je	.L638
	cmpb	$3, %bl
	je	.L639
	leaq	-96(%rbp), %rax
	leaq	-112(%rbp), %r15
	movq	%rax, -168(%rbp)
	cmpb	$4, %bl
	jne	.L640
.L598:
	movq	$0, -176(%rbp)
	movq	$209682431, -200(%rbp)
.L591:
	movq	8(%r13), %rdi
	movq	-136(%rbp), %rsi
	movl	%r14d, %edx
	movq	%r10, -192(%rbp)
	call	_ZNK2v88internal8compiler23CompilationDependencies31FieldTypeDependencyOffTheRecordERKNS1_6MapRefEi@PLT
	movq	-168(%rbp), %rdi
	movq	%r15, %rsi
	movq	%rax, -112(%rbp)
	call	_ZNSt6vectorIPKN2v88internal8compiler21CompilationDependencyENS1_13ZoneAllocatorIS5_EEE12emplace_backIJS5_EEEvDpOT_
	movq	-144(%rbp), %rcx
	movq	-192(%rbp), %r10
	movl	%ecx, %eax
	shrl	$3, %eax
	andl	$8, %ecx
	je	.L602
	movl	$1, -144(%rbp)
	testb	$4, %al
	jne	.L603
.L602:
	movq	-136(%rbp), %rdi
	movl	%r14d, %esi
	movq	%r10, -192(%rbp)
	call	_ZN2v88internal8compiler6MapRef22SerializeOwnDescriptorEi@PLT
	movq	8(%r13), %rdi
	movq	-136(%rbp), %rsi
	movl	%r14d, %edx
	call	_ZN2v88internal8compiler23CompilationDependencies22DependOnFieldConstnessERKNS1_6MapRefEi@PLT
	movq	-192(%rbp), %r10
	movl	%eax, -144(%rbp)
.L603:
	movq	0(%r13), %rax
	movq	(%r10), %rdx
	movq	%r15, %rdi
	movq	(%rax), %rcx
	movq	%rdx, -112(%rbp)
	movl	%r14d, %edx
	movq	(%rax), %rsi
	movq	%rcx, -136(%rbp)
	call	_ZNK2v88internal3Map14FindFieldOwnerEPNS0_7IsolateEi@PLT
	movq	-136(%rbp), %rcx
	movq	%rax, %rsi
	movq	41112(%rcx), %rdi
	testq	%rdi, %rdi
	je	.L604
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L605:
	movl	-144(%rbp), %eax
	testl	%eax, %eax
	jne	.L641
	subq	$8, %rsp
	movq	24(%r13), %rsi
	movl	%ebx, %r9d
	movq	%r12, %rdi
	movq	-184(%rbp), %r8
	movq	-168(%rbp), %rcx
	pushq	$0
	pushq	-160(%rbp)
	movq	-152(%rbp), %rdx
	pushq	-176(%rbp)
	pushq	%r14
	pushq	-200(%rbp)
	call	_ZN2v88internal8compiler18PropertyAccessInfo9DataFieldEPNS0_4ZoneENS0_6HandleINS0_3MapEEEONS0_10ZoneVectorIPKNS1_21CompilationDependencyEEENS0_10FieldIndexENS0_14RepresentationENS1_4TypeES7_NS0_11MaybeHandleIS6_EENSH_INS0_8JSObjectEEESI_
	addq	$48, %rsp
	jmp	.L579
	.p2align 4,,10
	.p2align 3
.L636:
	cmpb	$1, %bl
	je	.L616
.L589:
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L614:
	movl	$32768, %r8d
	jmp	.L587
	.p2align 4,,10
	.p2align 3
.L638:
	movq	16(%r13), %rax
	leaq	-112(%rbp), %r15
	movq	$0, -176(%rbp)
	movq	176(%rax), %rax
	movq	%rax, -200(%rbp)
	leaq	-96(%rbp), %rax
	movq	%rax, -168(%rbp)
	jmp	.L591
	.p2align 4,,10
	.p2align 3
.L641:
	cmpl	$1, %eax
	jne	.L589
	movq	24(%r13), %r13
	movq	16(%r13), %r15
	movq	24(%r13), %rax
	subq	%r15, %rax
	cmpq	$7, %rax
	jbe	.L642
	leaq	8(%r15), %rax
	movq	%rax, 16(%r13)
.L610:
	movq	-152(%rbp), %rax
	movq	%rax, (%r15)
	movl	$3, (%r12)
	movq	%r13, 8(%r12)
	movq	$0, 16(%r12)
	movq	$0, 24(%r12)
	movq	$0, 32(%r12)
	movq	16(%r13), %rax
	movq	24(%r13), %rdx
	subq	%rax, %rdx
	cmpq	$7, %rdx
	jbe	.L643
	leaq	8(%rax), %rdx
	movq	%rdx, 16(%r13)
.L612:
	movq	%rax, 24(%r12)
	movq	%r14, %xmm0
	movq	%rax, 16(%r12)
	movhps	-176(%rbp), %xmm0
	movq	%rdx, 32(%r12)
	movq	(%r15), %rcx
	movq	%rcx, (%rax)
	movq	-96(%rbp), %rax
	movdqu	-88(%rbp), %xmm1
	movq	%rdx, 24(%r12)
	movq	%rax, 40(%r12)
	movq	-72(%rbp), %rax
	movq	$0, 72(%r12)
	movq	%rax, 64(%r12)
	movq	-160(%rbp), %rax
	movq	$0, 80(%r12)
	movq	%rax, 88(%r12)
	movq	-184(%rbp), %rax
	movb	%bl, 104(%r12)
	movq	%rax, 96(%r12)
	movq	-200(%rbp), %rax
	movups	%xmm1, 48(%r12)
	movq	%rax, 112(%r12)
	movups	%xmm0, 120(%r12)
	jmp	.L579
	.p2align 4,,10
	.p2align 3
.L604:
	movq	41088(%rcx), %r14
	cmpq	41096(%rcx), %r14
	je	.L644
.L606:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%rcx)
	movq	%rsi, (%r14)
	jmp	.L605
	.p2align 4,,10
	.p2align 3
.L635:
	movzbl	8(%r8), %ecx
	movzbl	8(%r8), %r8d
	addl	%r8d, %edi
	sall	$3, %ecx
	sall	$3, %edi
	jmp	.L586
	.p2align 4,,10
	.p2align 3
.L634:
	movq	%rbx, %rdi
	movq	%rsi, -136(%rbp)
	movq	%rcx, -144(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-144(%rbp), %r10
	movq	-136(%rbp), %rsi
	jmp	.L582
	.p2align 4,,10
	.p2align 3
.L637:
	movq	%r10, -192(%rbp)
	leaq	-112(%rbp), %r15
	call	_ZN2v88internal8compiler10BitsetType11SignedSmallEv@PLT
	movq	8(%r13), %rdi
	movl	%r14d, %edx
	movl	%eax, %esi
	orl	$1, %esi
	movq	%rsi, -200(%rbp)
	movq	-136(%rbp), %rsi
	call	_ZNK2v88internal8compiler23CompilationDependencies41FieldRepresentationDependencyOffTheRecordERKNS1_6MapRefEi@PLT
	movq	%r15, %rsi
	movq	%rax, -112(%rbp)
	leaq	-96(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -168(%rbp)
	call	_ZNSt6vectorIPKN2v88internal8compiler21CompilationDependencyENS1_13ZoneAllocatorIS5_EEE12emplace_backIJS5_EEEvDpOT_
	movq	-192(%rbp), %r10
	movq	$0, -176(%rbp)
	jmp	.L591
	.p2align 4,,10
	.p2align 3
.L639:
	movq	0(%r13), %rdx
	movq	(%rax), %rax
	movq	%r10, -176(%rbp)
	movq	(%rdx), %rdx
	movq	%rdx, -168(%rbp)
	movq	15(%r15,%rax), %rdi
	call	_ZN2v88internal3Map15UnwrapFieldTypeENS0_11MaybeObjectE@PLT
	movq	-168(%rbp), %rdx
	movq	-176(%rbp), %r10
	movq	%rax, %r15
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L594
	movq	%rax, %rsi
	movq	%r10, -168(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-168(%rbp), %r10
	movq	%rax, -176(%rbp)
	movq	(%rax), %r15
.L595:
	movq	%r10, -168(%rbp)
	call	_ZN2v88internal9FieldType4NoneEv@PLT
	cmpl	$1, 16(%rbp)
	movq	-168(%rbp), %r10
	jne	.L597
	cmpq	%rax, %r15
	je	.L632
.L597:
	movq	8(%r13), %rdi
	movl	%r14d, %edx
	leaq	-112(%rbp), %r15
	movq	-136(%rbp), %rsi
	movq	%r10, -192(%rbp)
	call	_ZNK2v88internal8compiler23CompilationDependencies41FieldRepresentationDependencyOffTheRecordERKNS1_6MapRefEi@PLT
	movq	%r15, %rsi
	movq	%rax, -112(%rbp)
	leaq	-96(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -168(%rbp)
	call	_ZNSt6vectorIPKN2v88internal8compiler21CompilationDependencyENS1_13ZoneAllocatorIS5_EEE12emplace_backIJS5_EEEvDpOT_
	movq	-176(%rbp), %rax
	movq	%r15, %rdi
	movq	(%rax), %rax
	movq	%rax, -112(%rbp)
	call	_ZNK2v88internal9FieldType7IsClassEv@PLT
	movq	-192(%rbp), %r10
	testb	%al, %al
	je	.L598
	movq	0(%r13), %rax
	movq	%r15, %rdi
	movq	%r10, -200(%rbp)
	movq	(%rax), %rdx
	movq	-176(%rbp), %rax
	movq	(%rax), %rax
	movq	%rdx, -192(%rbp)
	movq	%rax, -112(%rbp)
	call	_ZNK2v88internal9FieldType7AsClassEv@PLT
	movq	-192(%rbp), %rdx
	movq	-200(%rbp), %r10
	movq	%rax, %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L599
	movq	%r10, -192(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-192(%rbp), %r10
	movq	%rax, -176(%rbp)
	movq	%rax, %rdx
.L600:
	movq	0(%r13), %rsi
	movl	$1, %ecx
	movq	%r15, %rdi
	movq	%r10, -192(%rbp)
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler10BitsetType3LubINS1_6MapRefEEEjRKT_@PLT
	movl	%eax, %edi
	call	_ZN2v88internal8compiler10BitsetType15ExpandInternalsEj@PLT
	movq	-192(%rbp), %r10
	movl	%eax, %esi
	orl	$1, %esi
	movq	%rsi, -200(%rbp)
	jmp	.L591
	.p2align 4,,10
	.p2align 3
.L644:
	movq	%rcx, %rdi
	movq	%rcx, -136(%rbp)
	movq	%rax, -192(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-192(%rbp), %rsi
	movq	-136(%rbp), %rcx
	movq	%rax, %r14
	jmp	.L606
	.p2align 4,,10
	.p2align 3
.L594:
	movq	41088(%rdx), %rax
	movq	%rax, -176(%rbp)
	cmpq	41096(%rdx), %rax
	je	.L645
.L596:
	movq	-176(%rbp), %rsi
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rdx)
	movq	%r15, (%rsi)
	jmp	.L595
	.p2align 4,,10
	.p2align 3
.L640:
	leaq	.LC11(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L643:
	movl	$8, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	leaq	8(%rax), %rdx
	jmp	.L612
	.p2align 4,,10
	.p2align 3
.L642:
	movl	$8, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r15
	jmp	.L610
.L599:
	movq	41088(%rdx), %rax
	movq	%rax, -176(%rbp)
	cmpq	41096(%rdx), %rax
	je	.L646
.L601:
	movq	-176(%rbp), %rcx
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rdx)
	movq	%rcx, %rdx
	movq	%rsi, (%rcx)
	jmp	.L600
.L645:
	movq	%rdx, %rdi
	movq	%r10, -192(%rbp)
	movq	%rdx, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-192(%rbp), %r10
	movq	-168(%rbp), %rdx
	movq	%rax, -176(%rbp)
	jmp	.L596
.L646:
	movq	%rdx, %rdi
	movq	%r10, -208(%rbp)
	movq	%rsi, -200(%rbp)
	movq	%rdx, -192(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-208(%rbp), %r10
	movq	-200(%rbp), %rsi
	movq	%rax, -176(%rbp)
	movq	-192(%rbp), %rdx
	jmp	.L601
.L633:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19351:
	.size	_ZNK2v88internal8compiler17AccessInfoFactory26ComputeDataFieldAccessInfoENS0_6HandleINS0_3MapEEES5_NS0_11MaybeHandleINS0_8JSObjectEEEiNS1_10AccessModeE, .-_ZNK2v88internal8compiler17AccessInfoFactory26ComputeDataFieldAccessInfoENS0_6HandleINS0_3MapEEES5_NS0_11MaybeHandleINS0_8JSObjectEEEiNS1_10AccessModeE
	.section	.text._ZNK2v88internal8compiler17AccessInfoFactory16LookupTransitionENS0_6HandleINS0_3MapEEENS3_INS0_4NameEEENS0_11MaybeHandleINS0_8JSObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler17AccessInfoFactory16LookupTransitionENS0_6HandleINS0_3MapEEENS3_INS0_4NameEEENS0_11MaybeHandleINS0_8JSObjectEEE
	.type	_ZNK2v88internal8compiler17AccessInfoFactory16LookupTransitionENS0_6HandleINS0_3MapEEENS3_INS0_4NameEEENS0_11MaybeHandleINS0_8JSObjectEEE, @function
_ZNK2v88internal8compiler17AccessInfoFactory16LookupTransitionENS0_6HandleINS0_3MapEEENS3_INS0_4NameEEENS0_11MaybeHandleINS0_8JSObjectEEE:
.LFB19376:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$200, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	(%rax), %xmm0
	movq	(%rdx), %rax
	movq	$0, -72(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	movq	71(%rax), %rax
	movq	%rax, -72(%rbp)
	testb	$1, %al
	je	.L648
	cmpl	$3, %eax
	je	.L648
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$3, %rdx
	je	.L688
	cmpq	$1, %rdx
	je	.L689
.L667:
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L648:
	movl	$1, -64(%rbp)
.L650:
	movq	(%rcx), %rsi
	leaq	-96(%rbp), %r15
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal19TransitionsAccessor16SearchTransitionENS0_4NameENS0_12PropertyKindENS0_18PropertyAttributesE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L686
	movq	(%rbx), %rax
	movq	(%rax), %rdx
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L658
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r10
.L659:
	movl	15(%rsi), %eax
	movq	(%r10), %rcx
	shrl	$10, %eax
	movq	39(%rcx), %rdx
	andl	$1023, %eax
	leal	(%rax,%rax,2), %r8d
	sall	$3, %r8d
	movslq	%r8d, %r8
	movq	7(%r8,%rdx), %rdx
	movq	%rdx, %r11
	sarq	$32, %r11
	btq	$35, %rdx
	jc	.L686
	testb	$2, %r11b
	je	.L662
	.p2align 4,,10
	.p2align 3
.L686:
	movq	24(%rbx), %rax
	movl	$0, (%r12)
	pxor	%xmm0, %xmm0
	movq	$0, 16(%r12)
	movq	%rax, 8(%r12)
	movq	$0, 24(%r12)
	movq	$0, 32(%r12)
	movq	%rax, 40(%r12)
	movq	$0, 48(%r12)
	movq	$0, 56(%r12)
	movq	$0, 64(%r12)
	movq	$0, 72(%r12)
	movq	$0, 80(%r12)
	movq	$0, 88(%r12)
	movq	$0, 96(%r12)
	movb	$0, 104(%r12)
	movq	$1, 112(%r12)
	movups	%xmm0, 120(%r12)
.L647:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L690
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L662:
	.cfi_restore_state
	movl	%r11d, %r9d
	movl	%r11d, %esi
	shrl	$6, %r9d
	shrl	$19, %esi
	andl	$7, %r9d
	andl	$1023, %esi
	movl	%r9d, -152(%rbp)
	movzbl	7(%rcx), %edx
	movzbl	8(%rcx), %edi
	subl	%edi, %edx
	cmpl	%edx, %esi
	setl	%r9b
	jl	.L691
	subl	%edx, %esi
	movl	$16, %edi
	leal	16(,%rsi,8), %esi
.L664:
	movl	-152(%rbp), %ecx
	cmpl	$2, %ecx
	jne	.L692
	movl	$32768, %ecx
.L665:
	movzbl	%r9b, %r9d
	movslq	%edx, %rdx
	movslq	%esi, %rsi
	movslq	%edi, %rdi
	salq	$14, %r9
	salq	$17, %rdx
	subl	$1, %eax
	movq	%r11, -192(%rbp)
	orq	%r9, %rdx
	salq	$27, %rdi
	movq	%r8, -184(%rbp)
	orq	%rsi, %rdx
	movq	(%rbx), %rsi
	movl	%eax, -216(%rbp)
	orq	%rdi, %rdx
	leaq	-128(%rbp), %rdi
	movq	%r10, -176(%rbp)
	orq	%rcx, %rdx
	movl	$1, %ecx
	movq	$209682431, -168(%rbp)
	movq	%rdx, -208(%rbp)
	movq	%r10, %rdx
	movq	$0, -200(%rbp)
	movq	%rdi, -160(%rbp)
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	24(%rbx), %rax
	cmpl	$1, -152(%rbp)
	movq	$0, -88(%rbp)
	movq	$0, -80(%rbp)
	movq	-176(%rbp), %r10
	movq	%rax, -96(%rbp)
	movq	-184(%rbp), %r8
	movq	$0, -72(%rbp)
	movq	-192(%rbp), %r11
	je	.L693
	cmpl	$2, -152(%rbp)
	movq	(%rbx), %rsi
	je	.L694
	cmpb	$3, -152(%rbp)
	leaq	-112(%rbp), %r9
	je	.L695
.L669:
	movq	8(%rbx), %r8
	movq	%r10, %rdx
	movl	$1, %ecx
	movq	%r9, %rdi
	movq	%r11, -216(%rbp)
	movq	%r10, -184(%rbp)
	movq	%r8, -192(%rbp)
	movq	%r9, -176(%rbp)
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	-176(%rbp), %r9
	movq	-192(%rbp), %r8
	movq	%r9, %rsi
	movq	%r8, %rdi
	call	_ZNK2v88internal8compiler23CompilationDependencies32TransitionDependencyOffTheRecordERKNS1_6MapRefE@PLT
	leaq	-136(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, -136(%rbp)
	call	_ZNSt6vectorIPKN2v88internal8compiler21CompilationDependencyENS1_13ZoneAllocatorIS5_EEE12emplace_backIJS5_EEEvDpOT_
	movq	-160(%rbp), %rdi
	call	_ZN2v88internal8compiler6MapRef20SerializeBackPointerEv@PLT
	movq	-216(%rbp), %r11
	movq	24(%rbx), %rsi
	movq	-184(%rbp), %r10
	andl	$4, %r11d
	jne	.L679
	subq	$8, %rsp
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	pushq	%r10
	movzbl	-152(%rbp), %r9d
	pushq	%r14
	movq	-208(%rbp), %r8
	pushq	-200(%rbp)
	pushq	%r10
	pushq	-168(%rbp)
	call	_ZN2v88internal8compiler18PropertyAccessInfo9DataFieldEPNS0_4ZoneENS0_6HandleINS0_3MapEEEONS0_10ZoneVectorIPKNS1_21CompilationDependencyEEENS0_10FieldIndexENS0_14RepresentationENS1_4TypeES7_NS0_11MaybeHandleIS6_EENSH_INS0_8JSObjectEEESI_
	addq	$48, %rsp
	jmp	.L647
	.p2align 4,,10
	.p2align 3
.L658:
	movq	41088(%rdx), %r10
	cmpq	%r10, 41096(%rdx)
	je	.L696
.L660:
	leaq	8(%r10), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%r10)
	jmp	.L659
	.p2align 4,,10
	.p2align 3
.L688:
	movl	$3, -64(%rbp)
	jmp	.L650
	.p2align 4,,10
	.p2align 3
.L689:
	movq	-1(%rax), %rdx
	cmpw	$149, 11(%rdx)
	jne	.L697
	movl	$4, -64(%rbp)
	jmp	.L650
	.p2align 4,,10
	.p2align 3
.L691:
	movzbl	8(%rcx), %edi
	movzbl	8(%rcx), %ecx
	addl	%ecx, %esi
	sall	$3, %edi
	sall	$3, %esi
	jmp	.L664
	.p2align 4,,10
	.p2align 3
.L692:
	cmpb	$2, %cl
	jg	.L666
	je	.L667
	xorl	%ecx, %ecx
	jmp	.L665
	.p2align 4,,10
	.p2align 3
.L666:
	movzbl	-152(%rbp), %ecx
	subl	$3, %ecx
	cmpb	$1, %cl
	ja	.L667
	xorl	%ecx, %ecx
	jmp	.L665
	.p2align 4,,10
	.p2align 3
.L696:
	movq	%rdx, %rdi
	movq	%rsi, -160(%rbp)
	movq	%rdx, -152(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-160(%rbp), %rsi
	movq	-152(%rbp), %rdx
	movq	%rax, %r10
	jmp	.L660
	.p2align 4,,10
	.p2align 3
.L697:
	movq	-1(%rax), %rax
	cmpw	$95, 11(%rax)
	setne	%al
	movzbl	%al, %eax
	addl	%eax, %eax
	movl	%eax, -64(%rbp)
	jmp	.L650
	.p2align 4,,10
	.p2align 3
.L679:
	subq	$8, %rsp
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	pushq	%r10
	movzbl	-152(%rbp), %r9d
	pushq	%r14
	movq	-208(%rbp), %r8
	pushq	-200(%rbp)
	pushq	%r10
	pushq	-168(%rbp)
	call	_ZN2v88internal8compiler18PropertyAccessInfo12DataConstantEPNS0_4ZoneENS0_6HandleINS0_3MapEEEONS0_10ZoneVectorIPKNS1_21CompilationDependencyEEENS0_10FieldIndexENS0_14RepresentationENS1_4TypeES7_NS0_11MaybeHandleIS6_EENSH_INS0_8JSObjectEEESI_
	addq	$48, %rsp
	jmp	.L647
	.p2align 4,,10
	.p2align 3
.L694:
	movq	16(%rbx), %rax
	leaq	-112(%rbp), %r9
	movq	176(%rax), %rax
	movq	%rax, -168(%rbp)
	jmp	.L669
	.p2align 4,,10
	.p2align 3
.L693:
	movq	%r10, -184(%rbp)
	call	_ZN2v88internal8compiler10BitsetType11SignedSmallEv@PLT
	movl	-216(%rbp), %esi
	movl	%eax, %edi
	orl	$1, %edi
	movq	%rdi, -168(%rbp)
	movq	-160(%rbp), %rdi
	call	_ZN2v88internal8compiler6MapRef22SerializeOwnDescriptorEi@PLT
	movq	8(%rbx), %rdi
	movl	-216(%rbp), %edx
	movq	-160(%rbp), %rsi
	call	_ZNK2v88internal8compiler23CompilationDependencies41FieldRepresentationDependencyOffTheRecordERKNS1_6MapRefEi@PLT
	leaq	-112(%rbp), %r9
	movq	%r15, %rdi
	movq	%r9, %rsi
	movq	%r9, -176(%rbp)
	movq	%rax, -112(%rbp)
	call	_ZNSt6vectorIPKN2v88internal8compiler21CompilationDependencyENS1_13ZoneAllocatorIS5_EEE12emplace_backIJS5_EEEvDpOT_
	movq	(%rbx), %rsi
	movq	-176(%rbp), %r9
	movq	-184(%rbp), %r10
	movq	-192(%rbp), %r11
	jmp	.L669
.L695:
	movq	(%r10), %rax
	movq	(%rsi), %rdx
	movq	%r11, -192(%rbp)
	movq	%r10, -176(%rbp)
	movq	39(%rax), %rax
	movq	%rdx, -184(%rbp)
	movq	15(%r8,%rax), %rdi
	call	_ZN2v88internal3Map15UnwrapFieldTypeENS0_11MaybeObjectE@PLT
	movq	-184(%rbp), %rdx
	movq	-176(%rbp), %r10
	movq	-192(%rbp), %r11
	movq	%rax, %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L671
	movq	%r10, -184(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-184(%rbp), %r10
	movq	-192(%rbp), %r11
	movq	%rax, -176(%rbp)
	movq	(%rax), %rsi
.L672:
	movq	%r11, -224(%rbp)
	movq	%rsi, -192(%rbp)
	movq	%r10, -184(%rbp)
	call	_ZN2v88internal9FieldType4NoneEv@PLT
	movq	-192(%rbp), %rsi
	movq	-184(%rbp), %r10
	movq	-224(%rbp), %r11
	cmpq	%rsi, %rax
	je	.L686
	movl	-216(%rbp), %esi
	movq	-160(%rbp), %rdi
	movq	%r11, -224(%rbp)
	movq	%r10, -192(%rbp)
	call	_ZN2v88internal8compiler6MapRef22SerializeOwnDescriptorEi@PLT
	movl	-216(%rbp), %edx
	movq	8(%rbx), %rdi
	movq	-160(%rbp), %rsi
	call	_ZNK2v88internal8compiler23CompilationDependencies41FieldRepresentationDependencyOffTheRecordERKNS1_6MapRefEi@PLT
	leaq	-112(%rbp), %r9
	movq	%r15, %rdi
	movq	%r9, %rsi
	movq	%rax, -112(%rbp)
	movq	%r9, -184(%rbp)
	call	_ZNSt6vectorIPKN2v88internal8compiler21CompilationDependencyENS1_13ZoneAllocatorIS5_EEE12emplace_backIJS5_EEEvDpOT_
	movq	-176(%rbp), %rax
	movq	-184(%rbp), %r9
	movq	(%rax), %rax
	movq	%r9, %rdi
	movq	%rax, -112(%rbp)
	call	_ZNK2v88internal9FieldType7IsClassEv@PLT
	movq	-184(%rbp), %r9
	movq	-192(%rbp), %r10
	testb	%al, %al
	movq	-224(%rbp), %r11
	jne	.L675
	movq	(%rbx), %rsi
	jmp	.L669
.L671:
	movq	41088(%rdx), %rax
	movq	%rax, -176(%rbp)
	cmpq	%rax, 41096(%rdx)
	je	.L698
.L673:
	movq	-176(%rbp), %rdi
	leaq	8(%rdi), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%rdi)
	jmp	.L672
.L675:
	movl	-216(%rbp), %edx
	movq	8(%rbx), %rdi
	movq	%r11, -200(%rbp)
	movq	-160(%rbp), %rsi
	movq	%r10, -192(%rbp)
	movq	%r9, -168(%rbp)
	call	_ZNK2v88internal8compiler23CompilationDependencies31FieldTypeDependencyOffTheRecordERKNS1_6MapRefEi@PLT
	movq	-168(%rbp), %r9
	movq	%r15, %rdi
	movq	%rax, -112(%rbp)
	movq	%r9, %rsi
	call	_ZNSt6vectorIPKN2v88internal8compiler21CompilationDependencyENS1_13ZoneAllocatorIS5_EEE12emplace_backIJS5_EEEvDpOT_
	movq	(%rbx), %rax
	movq	-168(%rbp), %r9
	movq	(%rax), %rcx
	movq	-176(%rbp), %rax
	movq	%r9, %rdi
	movq	(%rax), %rax
	movq	%rcx, -184(%rbp)
	movq	%rax, -112(%rbp)
	call	_ZNK2v88internal9FieldType7AsClassEv@PLT
	movq	-184(%rbp), %rcx
	movq	-168(%rbp), %r9
	movq	-192(%rbp), %r10
	movq	-200(%rbp), %r11
	movq	%rax, %rsi
	movq	41112(%rcx), %rdi
	testq	%rdi, %rdi
	je	.L676
	movq	%r9, -184(%rbp)
	movq	%r11, -176(%rbp)
	movq	%r10, -168(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-168(%rbp), %r10
	movq	-176(%rbp), %r11
	movq	-184(%rbp), %r9
	movq	%rax, %rdx
.L677:
	movq	(%rbx), %rsi
	movl	$1, %ecx
	movq	%r9, %rdi
	movq	%r11, -216(%rbp)
	movq	%r10, -192(%rbp)
	movq	%rdx, -184(%rbp)
	movq	%r9, -168(%rbp)
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	-168(%rbp), %r9
	movq	%r9, %rdi
	movq	%r9, -176(%rbp)
	call	_ZN2v88internal8compiler10BitsetType3LubINS1_6MapRefEEEjRKT_@PLT
	movl	%eax, %edi
	call	_ZN2v88internal8compiler10BitsetType15ExpandInternalsEj@PLT
	movq	-184(%rbp), %rdx
	movq	(%rbx), %rsi
	movl	%eax, %edi
	movq	-216(%rbp), %r11
	movq	-192(%rbp), %r10
	orl	$1, %edi
	movq	%rdx, -200(%rbp)
	movq	-176(%rbp), %r9
	movq	%rdi, -168(%rbp)
	jmp	.L669
.L698:
	movq	%rdx, %rdi
	movq	%r11, -232(%rbp)
	movq	%rsi, -224(%rbp)
	movq	%r10, -192(%rbp)
	movq	%rdx, -184(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-232(%rbp), %r11
	movq	-224(%rbp), %rsi
	movq	%rax, -176(%rbp)
	movq	-192(%rbp), %r10
	movq	-184(%rbp), %rdx
	jmp	.L673
.L676:
	movq	41088(%rcx), %rdx
	cmpq	41096(%rcx), %rdx
	je	.L699
.L678:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%rcx)
	movq	%rsi, (%rdx)
	jmp	.L677
.L690:
	call	__stack_chk_fail@PLT
.L699:
	movq	%rcx, %rdi
	movq	%r9, -200(%rbp)
	movq	%r11, -192(%rbp)
	movq	%r10, -176(%rbp)
	movq	%rcx, -168(%rbp)
	movq	%rax, -184(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-200(%rbp), %r9
	movq	-192(%rbp), %r11
	movq	-184(%rbp), %rsi
	movq	-176(%rbp), %r10
	movq	%rax, %rdx
	movq	-168(%rbp), %rcx
	jmp	.L678
	.cfi_endproc
.LFE19376:
	.size	_ZNK2v88internal8compiler17AccessInfoFactory16LookupTransitionENS0_6HandleINS0_3MapEEENS3_INS0_4NameEEENS0_11MaybeHandleINS0_8JSObjectEEE, .-_ZNK2v88internal8compiler17AccessInfoFactory16LookupTransitionENS0_6HandleINS0_3MapEEENS3_INS0_4NameEEENS0_11MaybeHandleINS0_8JSObjectEEE
	.section	.rodata._ZNK2v88internal8compiler17AccessInfoFactory25ComputePropertyAccessInfoENS0_6HandleINS0_3MapEEENS3_INS0_4NameEEENS1_10AccessModeE.str1.1,"aMS",@progbits,1
.LC12:
	.string	"name->IsUniqueName()"
.LC13:
	.string	"storage_.is_populated_"
.LC14:
	.string	"map->is_stable()"
	.section	.text._ZNK2v88internal8compiler17AccessInfoFactory25ComputePropertyAccessInfoENS0_6HandleINS0_3MapEEENS3_INS0_4NameEEENS1_10AccessModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler17AccessInfoFactory25ComputePropertyAccessInfoENS0_6HandleINS0_3MapEEENS3_INS0_4NameEEENS1_10AccessModeE
	.type	_ZNK2v88internal8compiler17AccessInfoFactory25ComputePropertyAccessInfoENS0_6HandleINS0_3MapEEENS3_INS0_4NameEEENS1_10AccessModeE, @function
_ZNK2v88internal8compiler17AccessInfoFactory25ComputePropertyAccessInfoENS0_6HandleINS0_3MapEEENS3_INS0_4NameEEENS1_10AccessModeE:
.LFB19353:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$200, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -208(%rbp)
	movl	%r8d, -196(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rcx), %rax
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L785
	cmpl	$3, -196(%rbp)
	movq	%rdi, %r12
	movq	%rsi, %r13
	movq	%rdx, %rbx
	movq	%rcx, %r14
	jne	.L702
	movq	(%rdx), %rax
	cmpw	$1023, 11(%rax)
	ja	.L702
.L782:
	movq	24(%r13), %rax
	movl	$0, (%r12)
	pxor	%xmm0, %xmm0
	movq	$0, 16(%r12)
	movq	%rax, 8(%r12)
	movq	$0, 24(%r12)
	movq	$0, 32(%r12)
	movq	%rax, 40(%r12)
	movq	$0, 48(%r12)
	movq	$0, 56(%r12)
	movq	$0, 64(%r12)
	movq	$0, 72(%r12)
	movq	$0, 80(%r12)
	movq	$0, 88(%r12)
	movq	$0, 96(%r12)
	movb	$0, 104(%r12)
	movq	$1, 112(%r12)
	movups	%xmm0, 120(%r12)
.L700:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L786
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L702:
	.cfi_restore_state
	movq	-208(%rbp), %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_123CanInlinePropertyAccessENS0_6HandleINS0_3MapEEE
	testb	%al, %al
	je	.L782
	movl	-196(%rbp), %eax
	testl	%eax, %eax
	je	.L750
	cmpl	$3, %eax
	je	.L750
.L705:
	leaq	-192(%rbp), %rax
	movq	%r12, -224(%rbp)
	xorl	%r15d, %r15d
	movq	%rax, %r12
.L746:
	movq	0(%r13), %rax
	movq	(%rax), %rdx
	movq	(%rbx), %rax
	movq	41112(%rdx), %rdi
	movq	39(%rax), %rsi
	testq	%rdi, %rdi
	je	.L708
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r10
.L709:
	movq	%rsi, -192(%rbp)
	movq	(%rbx), %rdi
	movq	(%r14), %r8
	movl	15(%rdi), %edx
	shrl	$10, %edx
	andl	$1023, %edx
	je	.L712
	cmpl	$8, %edx
	jg	.L787
	movl	$24, %r11d
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L713:
	movq	-1(%r11,%rsi), %r9
	movl	%ecx, %eax
	addl	$1, %ecx
	cmpq	%r9, %r8
	je	.L717
	addq	$24, %r11
	cmpl	%ecx, %edx
	jne	.L713
.L712:
	cmpw	$1086, 11(%rdi)
	je	.L722
	.p2align 4,,10
	.p2align 3
.L725:
	cmpl	$2, -196(%rbp)
	je	.L788
	movq	(%r14), %rax
	movq	-1(%rax), %rdx
	cmpw	$64, 11(%rdx)
	je	.L726
.L729:
	movq	0(%r13), %rsi
	movl	$1, %ecx
	movq	%rbx, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler6MapRef18SerializePrototypeEv@PLT
	movq	(%rbx), %rsi
	movq	23(%rsi), %rax
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	ja	.L735
	movq	0(%r13), %rax
	cmpb	$0, 24(%rax)
	je	.L789
	movdqu	32(%rax), %xmm1
	movq	%r12, %rdi
	movaps	%xmm1, -192(%rbp)
	call	_ZNK2v88internal8compiler16NativeContextRef6objectEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal3Map22GetConstructorFunctionENS0_6HandleIS1_EENS2_INS0_7ContextEEE@PLT
	testq	%rax, %rax
	je	.L790
	movq	0(%r13), %rdx
	movq	(%rax), %rax
	movq	(%rdx), %rbx
	movq	55(%rax), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L734
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L735:
	movq	0(%r13), %rax
	movq	23(%rsi), %rsi
	movq	(%rax), %rbx
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L738
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r15
	movq	-1(%rsi), %rax
	movl	15(%rax), %eax
	testl	$16777216, %eax
	jne	.L791
.L741:
	movq	0(%r13), %rax
	movq	(%rax), %rdx
	movq	(%r15), %rax
	movq	-1(%rax), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L742
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rbx
.L743:
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_123CanInlinePropertyAccessENS0_6HandleINS0_3MapEEE
	testb	%al, %al
	je	.L783
	movq	(%rbx), %rax
	movl	15(%rax), %eax
	testl	$33554432, %eax
	je	.L746
	leaq	.LC14(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L787:
	xorl	%ecx, %ecx
	movq	%r8, %rsi
	movq	%r12, %rdi
	movq	%r10, -216(%rbp)
	call	_ZN2v88internal12BinarySearchILNS0_10SearchModeE1ENS0_15DescriptorArrayEEEiPT0_NS0_4NameEiPi
	movq	-216(%rbp), %r10
.L717:
	cmpl	$-1, %eax
	jne	.L777
	movq	(%rbx), %rdi
	cmpw	$1086, 11(%rdi)
	jne	.L725
.L722:
	movq	(%r14), %rdi
	movq	-1(%rdi), %rax
	cmpw	$63, 11(%rax)
	ja	.L725
	call	_ZN2v88internal14IsSpecialIndexENS0_6StringE@PLT
	testb	%al, %al
	je	.L725
	.p2align 4,,10
	.p2align 3
.L783:
	movq	-224(%rbp), %r12
	jmp	.L782
	.p2align 4,,10
	.p2align 3
.L708:
	movq	41088(%rdx), %r10
	cmpq	%r10, 41096(%rdx)
	je	.L792
.L710:
	leaq	8(%r10), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%r10)
	jmp	.L709
	.p2align 4,,10
	.p2align 3
.L742:
	movq	41088(%rdx), %rbx
	cmpq	41096(%rdx), %rbx
	je	.L793
.L744:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%rbx)
	jmp	.L743
	.p2align 4,,10
	.p2align 3
.L738:
	movq	41088(%rbx), %r15
	cmpq	41096(%rbx), %r15
	je	.L794
.L740:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r15)
	movq	-1(%rsi), %rax
	movl	15(%rax), %eax
	testl	$16777216, %eax
	je	.L741
.L791:
	movq	0(%r13), %rax
	movq	%r15, %rsi
	movq	(%rax), %rdi
	call	_ZN2v88internal8JSObject18TryMigrateInstanceEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	jmp	.L741
	.p2align 4,,10
	.p2align 3
.L726:
	testb	$1, 11(%rax)
	je	.L729
	movq	-224(%rbp), %r12
	jmp	.L782
	.p2align 4,,10
	.p2align 3
.L734:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L795
.L736:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L735
	.p2align 4,,10
	.p2align 3
.L750:
	movq	-208(%rbp), %rdx
	leaq	-192(%rbp), %rdi
	movq	%r14, %rcx
	movq	%r13, %rsi
	call	_ZNK2v88internal8compiler17AccessInfoFactory26LookupSpecialFieldAccessorENS0_6HandleINS0_3MapEEENS3_INS0_4NameEEE
	movl	-192(%rbp), %eax
	testl	%eax, %eax
	je	.L705
	movl	%eax, (%r12)
	movq	-184(%rbp), %rax
	movdqa	-176(%rbp), %xmm2
	movdqa	-144(%rbp), %xmm3
	movq	%rax, 8(%r12)
	movq	-160(%rbp), %rax
	movups	%xmm2, 16(%r12)
	movq	%rax, 32(%r12)
	movq	-152(%rbp), %rax
	movups	%xmm3, 48(%r12)
	movq	%rax, 40(%r12)
	movq	-128(%rbp), %rax
	movq	%rax, 64(%r12)
	movq	-120(%rbp), %rax
	movq	%rax, 72(%r12)
	movq	-112(%rbp), %rax
	movq	%rax, 80(%r12)
	movq	-104(%rbp), %rax
	movq	%rax, 88(%r12)
	movq	-96(%rbp), %rax
	movq	%rax, 96(%r12)
	movzbl	-88(%rbp), %eax
	movb	%al, 104(%r12)
	movq	-80(%rbp), %rax
	movq	%rax, 112(%r12)
	movq	-72(%rbp), %rax
	movq	%rax, 120(%r12)
	movq	-64(%rbp), %rax
	movq	%rax, 128(%r12)
	jmp	.L700
	.p2align 4,,10
	.p2align 3
.L785:
	leaq	.LC12(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L792:
	movq	%rdx, %rdi
	movq	%rsi, -232(%rbp)
	movq	%rdx, -216(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-232(%rbp), %rsi
	movq	-216(%rbp), %rdx
	movq	%rax, %r10
	jmp	.L710
	.p2align 4,,10
	.p2align 3
.L790:
	movq	0(%r13), %rax
	movq	-224(%rbp), %r12
	movq	(%rax), %rdx
	movq	(%rbx), %rax
	movq	23(%rax), %rax
	cmpq	%rax, 104(%rdx)
	jne	.L782
	cmpl	$1, -196(%rbp)
	je	.L784
	movq	24(%r13), %rsi
	movq	-208(%rbp), %rdx
	movq	%r15, %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler18PropertyAccessInfo8NotFoundEPNS0_4ZoneENS0_6HandleINS0_3MapEEENS0_11MaybeHandleINS0_8JSObjectEEE
	jmp	.L700
	.p2align 4,,10
	.p2align 3
.L794:
	movq	%rbx, %rdi
	movq	%rsi, -216(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-216(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L740
	.p2align 4,,10
	.p2align 3
.L793:
	movq	%rdx, %rdi
	movq	%rsi, -232(%rbp)
	movq	%rdx, -216(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-232(%rbp), %rsi
	movq	-216(%rbp), %rdx
	movq	%rax, %rbx
	jmp	.L744
	.p2align 4,,10
	.p2align 3
.L789:
	leaq	.LC13(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L788:
	movq	-224(%rbp), %r12
.L784:
	movq	-208(%rbp), %rdx
	movq	%r15, %r8
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler17AccessInfoFactory16LookupTransitionENS0_6HandleINS0_3MapEEENS3_INS0_4NameEEENS0_11MaybeHandleINS0_8JSObjectEEE
	jmp	.L700
	.p2align 4,,10
	.p2align 3
.L795:
	movq	%rbx, %rdi
	movq	%rsi, -216(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-216(%rbp), %rsi
	jmp	.L736
	.p2align 4,,10
	.p2align 3
.L777:
	movl	%eax, %r9d
	leal	3(%rax,%rax,2), %eax
	movq	(%r10), %rdx
	movq	-224(%rbp), %r12
	sall	$3, %eax
	cltq
	movq	7(%rax,%rdx), %rax
	movl	-196(%rbp), %ecx
	leal	-1(%rcx), %edx
	sarq	$32, %rax
	cmpl	$1, %edx
	jbe	.L796
.L718:
	testb	$2, %al
	jne	.L720
	testb	$1, %al
	jne	.L782
	movl	-196(%rbp), %eax
	subq	$8, %rsp
	movq	%rbx, %rcx
	movq	%r13, %rsi
	movq	-208(%rbp), %rdx
	movq	%r15, %r8
	movq	%r12, %rdi
	pushq	%rax
	call	_ZNK2v88internal8compiler17AccessInfoFactory26ComputeDataFieldAccessInfoENS0_6HandleINS0_3MapEEES5_NS0_11MaybeHandleINS0_8JSObjectEEEiNS1_10AccessModeE
	popq	%rcx
	popq	%rsi
	jmp	.L700
.L796:
	testb	$8, %al
	jne	.L782
	testq	%r15, %r15
	je	.L718
	testb	$1, %al
	jne	.L718
	jmp	.L784
.L720:
	movl	-196(%rbp), %eax
	movq	%rbx, %r8
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	-208(%rbp), %rdx
	movq	%r12, %rdi
	pushq	%rax
	pushq	%r9
	movq	%r15, %r9
	call	_ZNK2v88internal8compiler17AccessInfoFactory35ComputeAccessorDescriptorAccessInfoENS0_6HandleINS0_3MapEEENS3_INS0_4NameEEES5_NS0_11MaybeHandleINS0_8JSObjectEEEiNS1_10AccessModeE
	popq	%rax
	popq	%rdx
	jmp	.L700
.L786:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19353:
	.size	_ZNK2v88internal8compiler17AccessInfoFactory25ComputePropertyAccessInfoENS0_6HandleINS0_3MapEEENS3_INS0_4NameEEENS1_10AccessModeE, .-_ZNK2v88internal8compiler17AccessInfoFactory25ComputePropertyAccessInfoENS0_6HandleINS0_3MapEEENS3_INS0_4NameEEENS1_10AccessModeE
	.section	.text._ZNK2v88internal8compiler17AccessInfoFactory35ComputeAccessorDescriptorAccessInfoENS0_6HandleINS0_3MapEEENS3_INS0_4NameEEES5_NS0_11MaybeHandleINS0_8JSObjectEEEiNS1_10AccessModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler17AccessInfoFactory35ComputeAccessorDescriptorAccessInfoENS0_6HandleINS0_3MapEEENS3_INS0_4NameEEES5_NS0_11MaybeHandleINS0_8JSObjectEEEiNS1_10AccessModeE
	.type	_ZNK2v88internal8compiler17AccessInfoFactory35ComputeAccessorDescriptorAccessInfoENS0_6HandleINS0_3MapEEENS3_INS0_4NameEEES5_NS0_11MaybeHandleINS0_8JSObjectEEEiNS1_10AccessModeE, @function
_ZNK2v88internal8compiler17AccessInfoFactory35ComputeAccessorDescriptorAccessInfoENS0_6HandleINS0_3MapEEENS3_INS0_4NameEEES5_NS0_11MaybeHandleINS0_8JSObjectEEEiNS1_10AccessModeE:
.LFB19352:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$200, %rsp
	movq	%r9, -216(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	(%rax), %rdx
	movq	(%r8), %rax
	movq	41112(%rdx), %rdi
	movq	39(%rax), %rsi
	testq	%rdi, %rdi
	je	.L798
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%r15), %rdx
	cmpw	$1027, 11(%rdx)
	je	.L881
.L801:
	cmpl	$3, 24(%rbp)
	je	.L882
	movq	(%rbx), %rdx
	movl	16(%rbp), %ecx
	movq	(%rax), %rax
	movq	(%rdx), %r13
	leal	3(%rcx,%rcx,2), %edx
	sall	$3, %edx
	movslq	%edx, %rdx
	movq	15(%rdx,%rax), %rsi
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L839
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	testb	$1, %sil
	jne	.L842
.L855:
	movq	24(%rbx), %rax
	movl	$0, (%r12)
	movq	$0, 16(%r12)
	movq	%rax, 8(%r12)
	movq	$0, 24(%r12)
	movq	$0, 32(%r12)
	movq	%rax, 40(%r12)
.L876:
	movq	$0, 48(%r12)
	pxor	%xmm0, %xmm0
	movq	$0, 56(%r12)
	movq	$0, 64(%r12)
	movq	$0, 72(%r12)
	movq	$0, 80(%r12)
	movq	$0, 88(%r12)
	movq	$0, 96(%r12)
	movb	$0, 104(%r12)
	movq	$1, 112(%r12)
	movups	%xmm0, 120(%r12)
.L797:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L883
	addq	$200, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L882:
	.cfi_restore_state
	movq	24(%rbx), %rsi
	movq	-216(%rbp), %r8
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler18PropertyAccessInfo16AccessorConstantEPNS0_4ZoneENS0_6HandleINS0_3MapEEENS5_INS0_6ObjectEEENS0_11MaybeHandleINS0_8JSObjectEEE
	jmp	.L797
	.p2align 4,,10
	.p2align 3
.L798:
	movq	41088(%rdx), %rax
	cmpq	%rax, 41096(%rdx)
	je	.L884
.L800:
	leaq	8(%rax), %rcx
	movq	%rcx, 41088(%rdx)
	movq	%rsi, (%rax)
	movq	(%r15), %rdx
	cmpw	$1027, 11(%rdx)
	jne	.L801
.L881:
	movq	(%rbx), %rax
	movq	71(%rdx), %rsi
	movq	(%rax), %r15
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L802
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L803:
	movq	(%rbx), %rax
	movq	7(%rsi), %r8
	movq	(%rax), %r15
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L805
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r8
.L806:
	movq	23(%r8), %rax
	movq	(%rbx), %rsi
	movq	7(%rax), %rax
	movq	(%rsi), %r15
	movq	%rsi, %rdx
	movq	%rax, -208(%rbp)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L808
	sarq	$32, %rax
	movq	%rax, %rdx
	sall	$15, %eax
	subl	%edx, %eax
	subl	$1, %eax
	movl	%eax, %edx
	shrl	$12, %edx
	xorl	%edx, %eax
	leal	(%rax,%rax,4), %ecx
	movl	%ecx, %eax
	shrl	$4, %eax
	xorl	%eax, %ecx
	imull	$2057, %ecx, %ecx
	movl	%ecx, %eax
	shrl	$16, %eax
	xorl	%eax, %ecx
	andl	$1073741823, %ecx
.L832:
	movq	(%rsi), %rsi
	leaq	-208(%rbp), %rdi
	movq	%r13, %rdx
	addq	$56, %rsi
	call	_ZN2v88internal19ObjectHashTableBaseINS0_15ObjectHashTableENS0_20ObjectHashTableShapeEE6LookupENS0_13ReadOnlyRootsENS0_6HandleINS0_6ObjectEEEi@PLT
	movq	41112(%r15), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L833
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L834:
	movq	(%rbx), %rax
	movq	24(%rbx), %rsi
	movq	(%rax), %rdx
	movq	(%rcx), %rax
	movq	7(%rax), %rax
	cmpq	%rax, 96(%rdx)
	jne	.L836
	movl	$0, (%r12)
	movq	%rsi, 8(%r12)
	movq	$0, 16(%r12)
	movq	$0, 24(%r12)
	movq	$0, 32(%r12)
	movq	%rsi, 40(%r12)
	jmp	.L876
	.p2align 4,,10
	.p2align 3
.L839:
	movq	41088(%r13), %rax
	cmpq	41096(%r13), %rax
	je	.L885
.L841:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r13)
	movq	%rsi, (%rax)
	testb	$1, %sil
	je	.L855
.L842:
	movq	-1(%rsi), %rdx
	cmpw	$79, 11(%rdx)
	jne	.L855
	movq	(%rbx), %rdx
	movl	24(%rbp), %ecx
	movq	(%rax), %rax
	movq	(%rdx), %r13
	testl	%ecx, %ecx
	jne	.L844
	movq	7(%rax), %rsi
.L845:
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L846
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r11
.L847:
	testb	$1, %sil
	jne	.L886
.L849:
	movq	(%rbx), %rax
	leaq	-192(%rbp), %r13
	movq	%r11, %rdx
	movq	%r11, -216(%rbp)
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal16CallOptimizationC1EPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE@PLT
	cmpb	$0, -184(%rbp)
	je	.L855
	movq	(%rbx), %rax
	movq	(%r15), %rdx
	movq	-216(%rbp), %r11
	cmpb	$0, 24(%rax)
	je	.L887
	leaq	-208(%rbp), %r8
	movdqu	32(%rax), %xmm2
	movq	%rdx, -232(%rbp)
	movq	%r8, %rdi
	movq	%r8, -216(%rbp)
	movq	%r11, -224(%rbp)
	movaps	%xmm2, -208(%rbp)
	call	_ZNK2v88internal8compiler16NativeContextRef6objectEv@PLT
	movq	-232(%rbp), %rdx
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZNK2v88internal16CallOptimization30IsCrossContextLazyAccessorPairENS0_7ContextENS0_3MapE@PLT
	movq	-216(%rbp), %r8
	testb	%al, %al
	jne	.L855
	movq	%r8, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZNK2v88internal16CallOptimization26LookupHolderOfExpectedTypeENS0_6HandleINS0_3MapEEEPNS1_12HolderLookupE@PLT
	movl	-208(%rbp), %edx
	movq	%rax, -216(%rbp)
	testl	%edx, %edx
	je	.L855
	movq	-224(%rbp), %r11
.L853:
	movl	24(%rbp), %eax
	testl	%eax, %eax
	jne	.L858
	movq	(%rbx), %rax
	movq	%r11, %rsi
	movq	%r11, -224(%rbp)
	movq	(%rax), %rdi
	call	_ZN2v88internal20FunctionTemplateInfo24TryGetCachedPropertyNameEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE@PLT
	movq	-224(%rbp), %r11
	testq	%rax, %rax
	movq	%rax, %rcx
	je	.L858
	xorl	%r8d, %r8d
	leaq	-192(%rbp), %rdi
	movq	%r15, %rdx
	movq	%rbx, %rsi
	call	_ZNK2v88internal8compiler17AccessInfoFactory25ComputePropertyAccessInfoENS0_6HandleINS0_3MapEEENS3_INS0_4NameEEENS1_10AccessModeE
	movl	-192(%rbp), %eax
	movq	-224(%rbp), %r11
	testl	%eax, %eax
	jne	.L888
.L858:
	movq	24(%rbx), %rsi
	movq	%r11, %rcx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	-216(%rbp), %r8
	call	_ZN2v88internal8compiler18PropertyAccessInfo16AccessorConstantEPNS0_4ZoneENS0_6HandleINS0_3MapEEENS5_INS0_6ObjectEEENS0_11MaybeHandleINS0_8JSObjectEEE
	jmp	.L797
	.p2align 4,,10
	.p2align 3
.L802:
	movq	41088(%r15), %rax
	cmpq	41096(%r15), %rax
	je	.L889
.L804:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r15)
	movq	%rsi, (%rax)
	jmp	.L803
	.p2align 4,,10
	.p2align 3
.L836:
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler18PropertyAccessInfo12ModuleExportEPNS0_4ZoneENS0_6HandleINS0_3MapEEENS5_INS0_4CellEEE
	jmp	.L797
	.p2align 4,,10
	.p2align 3
.L833:
	movq	41088(%r15), %rcx
	cmpq	41096(%r15), %rcx
	je	.L890
.L835:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%rcx)
	jmp	.L834
	.p2align 4,,10
	.p2align 3
.L805:
	movq	41088(%r15), %rax
	cmpq	41096(%r15), %rax
	je	.L891
.L807:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r15)
	movq	%r8, (%rax)
	jmp	.L806
	.p2align 4,,10
	.p2align 3
.L808:
	movq	-1(%rax), %rcx
	cmpw	$65, 11(%rcx)
	je	.L810
	movq	-1(%rax), %rcx
	cmpw	$64, 11(%rcx)
	jbe	.L880
	movq	-1(%rax), %rcx
	cmpw	$67, 11(%rcx)
	je	.L892
	movq	-1(%rax), %rcx
	cmpw	$66, 11(%rcx)
	je	.L893
	movq	-1(%rax), %rdx
	cmpw	$160, 11(%rdx)
	je	.L894
	leaq	-192(%rbp), %rdi
	movq	%rax, -192(%rbp)
	call	_ZN2v88internal10JSReceiver15GetIdentityHashEv@PLT
	movq	(%rbx), %rsi
	shrq	$32, %rax
	movq	%rax, %rcx
	jmp	.L832
	.p2align 4,,10
	.p2align 3
.L884:
	movq	%rdx, %rdi
	movq	%rsi, -232(%rbp)
	movq	%rdx, -224(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-232(%rbp), %rsi
	movq	-224(%rbp), %rdx
	jmp	.L800
	.p2align 4,,10
	.p2align 3
.L892:
	movq	15(%rax), %rax
.L880:
	movl	7(%rax), %ecx
	testb	$1, %cl
	jne	.L827
	shrl	$2, %ecx
.L831:
	movq	%rdx, %rsi
	jmp	.L832
	.p2align 4,,10
	.p2align 3
.L885:
	movq	%r13, %rdi
	movq	%rsi, -224(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-224(%rbp), %rsi
	jmp	.L841
	.p2align 4,,10
	.p2align 3
.L810:
	movq	7(%rax), %rcx
	movq	%rcx, %xmm0
	ucomisd	%xmm0, %xmm0
	jp	.L895
	comisd	.LC15(%rip), %xmm0
	jb	.L877
	movsd	.LC16(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L877
	cvttsd2sil	%xmm0, %esi
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%esi, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L877
	jne	.L877
	movl	%esi, %eax
	sall	$15, %eax
	subl	%esi, %eax
	subl	$1, %eax
	movl	%eax, %ecx
	shrl	$12, %ecx
	xorl	%ecx, %eax
	leal	(%rax,%rax,4), %ecx
	movl	%ecx, %eax
	shrl	$4, %eax
	xorl	%eax, %ecx
	imull	$2057, %ecx, %ecx
	movl	%ecx, %eax
	shrl	$16, %eax
	xorl	%eax, %ecx
	andl	$1073741823, %ecx
	jmp	.L831
.L893:
	movl	7(%rax), %ecx
	shrl	%ecx
	andl	$1073741823, %ecx
	je	.L831
	movq	15(%rax), %rcx
	.p2align 4,,10
	.p2align 3
.L877:
	movq	%rcx, %rax
	salq	$18, %rax
	subq	%rcx, %rax
	subq	$1, %rax
	movq	%rax, %rcx
	shrq	$31, %rcx
	xorq	%rcx, %rax
	leaq	(%rax,%rax,4), %rcx
	leaq	(%rax,%rcx,4), %rcx
	movq	%rcx, %rax
	shrq	$11, %rax
	xorq	%rax, %rcx
	movq	%rcx, %rax
	salq	$6, %rax
	addq	%rax, %rcx
	movq	%rcx, %rax
	shrq	$22, %rax
	xorq	%rax, %rcx
	andl	$1073741823, %ecx
	jmp	.L831
	.p2align 4,,10
	.p2align 3
.L891:
	movq	%r15, %rdi
	movq	%r8, -216(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-216(%rbp), %r8
	jmp	.L807
	.p2align 4,,10
	.p2align 3
.L890:
	movq	%r15, %rdi
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-216(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L835
	.p2align 4,,10
	.p2align 3
.L889:
	movq	%r15, %rdi
	movq	%rsi, -216(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-216(%rbp), %rsi
	jmp	.L804
	.p2align 4,,10
	.p2align 3
.L844:
	movq	15(%rax), %rsi
	jmp	.L845
	.p2align 4,,10
	.p2align 3
.L827:
	leaq	-192(%rbp), %rdi
	movq	%rax, -192(%rbp)
	call	_ZN2v88internal6String17ComputeAndSetHashEv@PLT
	movq	(%rbx), %rdx
	movl	%eax, %ecx
	jmp	.L831
	.p2align 4,,10
	.p2align 3
.L846:
	movq	41088(%r13), %r11
	cmpq	41096(%r13), %r11
	je	.L896
.L848:
	leaq	8(%r11), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r11)
	jmp	.L847
	.p2align 4,,10
	.p2align 3
.L886:
	movq	-1(%rsi), %rax
	cmpw	$1105, 11(%rax)
	jne	.L849
	jmp	.L853
.L894:
	leaq	-192(%rbp), %rdi
	movq	%rax, -192(%rbp)
	call	_ZN2v88internal18SharedFunctionInfo4HashEv@PLT
	movq	(%rbx), %rsi
	andl	$2147483647, %eax
	movl	%eax, %ecx
	jmp	.L832
.L896:
	movq	%r13, %rdi
	movq	%rsi, -224(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-224(%rbp), %rsi
	movq	%rax, %r11
	jmp	.L848
.L895:
	movl	$2147483647, %ecx
	jmp	.L832
.L887:
	leaq	.LC13(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L888:
	movl	%eax, (%r12)
	movq	-184(%rbp), %rax
	movdqa	-176(%rbp), %xmm3
	movdqa	-144(%rbp), %xmm4
	movq	%rax, 8(%r12)
	movq	-160(%rbp), %rax
	movups	%xmm3, 16(%r12)
	movq	%rax, 32(%r12)
	movq	-152(%rbp), %rax
	movups	%xmm4, 48(%r12)
	movq	%rax, 40(%r12)
	movq	-128(%rbp), %rax
	movq	%rax, 64(%r12)
	movq	-120(%rbp), %rax
	movq	%rax, 72(%r12)
	movq	-112(%rbp), %rax
	movq	%rax, 80(%r12)
	movq	-104(%rbp), %rax
	movq	%rax, 88(%r12)
	movq	-96(%rbp), %rax
	movq	%rax, 96(%r12)
	movzbl	-88(%rbp), %eax
	movb	%al, 104(%r12)
	movq	-80(%rbp), %rax
	movq	%rax, 112(%r12)
	movq	-72(%rbp), %rax
	movq	%rax, 120(%r12)
	movq	-64(%rbp), %rax
	movq	%rax, 128(%r12)
	jmp	.L797
.L883:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19352:
	.size	_ZNK2v88internal8compiler17AccessInfoFactory35ComputeAccessorDescriptorAccessInfoENS0_6HandleINS0_3MapEEENS3_INS0_4NameEEES5_NS0_11MaybeHandleINS0_8JSObjectEEEiNS1_10AccessModeE, .-_ZNK2v88internal8compiler17AccessInfoFactory35ComputeAccessorDescriptorAccessInfoENS0_6HandleINS0_3MapEEENS3_INS0_4NameEEES5_NS0_11MaybeHandleINS0_8JSObjectEEEiNS1_10AccessModeE
	.section	.text._ZNK2v88internal8compiler17AccessInfoFactory26ComputePropertyAccessInfosERKSt6vectorINS0_6HandleINS0_3MapEEESaIS6_EENS4_INS0_4NameEEENS1_10AccessModeEPNS0_10ZoneVectorINS1_18PropertyAccessInfoEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler17AccessInfoFactory26ComputePropertyAccessInfosERKSt6vectorINS0_6HandleINS0_3MapEEESaIS6_EENS4_INS0_4NameEEENS1_10AccessModeEPNS0_10ZoneVectorINS1_18PropertyAccessInfoEEE
	.type	_ZNK2v88internal8compiler17AccessInfoFactory26ComputePropertyAccessInfosERKSt6vectorINS0_6HandleINS0_3MapEEESaIS6_EENS4_INS0_4NameEEENS1_10AccessModeEPNS0_10ZoneVectorINS1_18PropertyAccessInfoEEE, @function
_ZNK2v88internal8compiler17AccessInfoFactory26ComputePropertyAccessInfosERKSt6vectorINS0_6HandleINS0_3MapEEESaIS6_EENS4_INS0_4NameEEENS1_10AccessModeEPNS0_10ZoneVectorINS1_18PropertyAccessInfoEEE:
.LFB19367:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$200, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%ecx, -212(%rbp)
	movq	(%rsi), %r12
	movq	%rdx, -208(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rsi), %rax
	movq	%rax, -200(%rbp)
	cmpq	%rax, %r12
	je	.L897
	movq	%rdi, %r14
	movq	%r8, %r13
	leaq	-192(%rbp), %r15
	jmp	.L908
	.p2align 4,,10
	.p2align 3
.L918:
	movl	-192(%rbp), %eax
	addq	$8, %r12
	movl	%eax, (%rbx)
	movq	-184(%rbp), %rax
	movq	%rax, 8(%rbx)
	movq	-176(%rbp), %rax
	movq	%rax, 16(%rbx)
	movq	-168(%rbp), %rax
	movq	%rax, 24(%rbx)
	movq	-160(%rbp), %rax
	movq	%rax, 32(%rbx)
	movq	-152(%rbp), %rax
	movq	%rax, 40(%rbx)
	movq	-144(%rbp), %rax
	movq	%rax, 48(%rbx)
	movq	-136(%rbp), %rax
	movq	%rax, 56(%rbx)
	movq	-128(%rbp), %rax
	movq	%rax, 64(%rbx)
	movq	-120(%rbp), %rax
	movq	%rax, 72(%rbx)
	movq	-112(%rbp), %rax
	movq	%rax, 80(%rbx)
	movq	-104(%rbp), %rax
	movq	%rax, 88(%rbx)
	movq	-96(%rbp), %rax
	movq	%rax, 96(%rbx)
	movzbl	-88(%rbp), %eax
	movb	%al, 104(%rbx)
	movq	-80(%rbp), %rax
	movq	%rax, 112(%rbx)
	movq	-72(%rbp), %rax
	movq	%rax, 120(%rbx)
	movq	-64(%rbp), %rax
	movq	%rax, 128(%rbx)
	addq	$136, 16(%r13)
	cmpq	%r12, -200(%rbp)
	je	.L897
.L908:
	movq	(%r12), %rdx
	movl	-212(%rbp), %r8d
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	-208(%rbp), %rcx
	call	_ZNK2v88internal8compiler17AccessInfoFactory25ComputePropertyAccessInfoENS0_6HandleINS0_3MapEEENS3_INS0_4NameEEENS1_10AccessModeE
	movq	16(%r13), %rbx
	cmpq	24(%r13), %rbx
	pxor	%xmm0, %xmm0
	jne	.L918
	movq	8(%r13), %r8
	movq	%rbx, %rdx
	movabsq	$-1085102592571150095, %rcx
	subq	%r8, %rdx
	movq	%rdx, %rax
	sarq	$3, %rax
	imulq	%rcx, %rax
	cmpq	$15790320, %rax
	je	.L919
	testq	%rax, %rax
	je	.L911
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L920
	movl	$2147483520, %esi
	movl	$2147483520, %ecx
.L902:
	movq	0(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %r11
	subq	%rax, %r11
	cmpq	%r11, %rsi
	ja	.L921
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L905:
	addq	%rax, %rcx
	leaq	136(%rax), %rsi
.L903:
	movl	-192(%rbp), %edi
	addq	%rax, %rdx
	movl	%edi, (%rdx)
	movq	-184(%rbp), %rdi
	movq	%rdi, 8(%rdx)
	movq	-176(%rbp), %rdi
	movq	%rdi, 16(%rdx)
	movq	-168(%rbp), %rdi
	movq	%rdi, 24(%rdx)
	movq	-160(%rbp), %rdi
	movaps	%xmm0, -176(%rbp)
	movq	%rdi, 32(%rdx)
	movq	-152(%rbp), %rdi
	movq	$0, -160(%rbp)
	movq	%rdi, 40(%rdx)
	movq	-144(%rbp), %rdi
	movq	%rdi, 48(%rdx)
	movq	-136(%rbp), %rdi
	movq	%rdi, 56(%rdx)
	movq	-128(%rbp), %rdi
	movaps	%xmm0, -144(%rbp)
	movq	%rdi, 64(%rdx)
	movq	-120(%rbp), %rdi
	movq	$0, -128(%rbp)
	movq	%rdi, 72(%rdx)
	movq	-112(%rbp), %rdi
	movq	%rdi, 80(%rdx)
	movq	-104(%rbp), %rdi
	movq	%rdi, 88(%rdx)
	movq	-96(%rbp), %rdi
	movq	%rdi, 96(%rdx)
	movzbl	-88(%rbp), %edi
	movb	%dil, 104(%rdx)
	movq	-80(%rbp), %rdi
	movq	%rdi, 112(%rdx)
	movq	-72(%rbp), %rdi
	movq	%rdi, 120(%rdx)
	movq	-64(%rbp), %rdi
	movq	%rdi, 128(%rdx)
	cmpq	%r8, %rbx
	je	.L906
	movq	%r8, %rdx
	movq	%rax, %rsi
	.p2align 4,,10
	.p2align 3
.L907:
	movl	(%rdx), %edi
	addq	$136, %rdx
	addq	$136, %rsi
	movl	%edi, -136(%rsi)
	movq	-128(%rdx), %rdi
	movq	%rdi, -128(%rsi)
	movdqu	-120(%rdx), %xmm2
	movups	%xmm2, -120(%rsi)
	movq	-104(%rdx), %rdi
	movq	%rdi, -104(%rsi)
	movq	-96(%rdx), %rdi
	movq	$0, -104(%rdx)
	movups	%xmm0, -120(%rdx)
	movq	%rdi, -96(%rsi)
	movdqu	-88(%rdx), %xmm3
	movups	%xmm3, -88(%rsi)
	movq	-72(%rdx), %rdi
	movq	%rdi, -72(%rsi)
	movq	-64(%rdx), %rdi
	movq	$0, -72(%rdx)
	movups	%xmm0, -88(%rdx)
	movq	%rdi, -64(%rsi)
	movq	-56(%rdx), %rdi
	movq	%rdi, -56(%rsi)
	movq	-48(%rdx), %rdi
	movq	%rdi, -48(%rsi)
	movq	-40(%rdx), %rdi
	movq	%rdi, -40(%rsi)
	movzbl	-32(%rdx), %edi
	movb	%dil, -32(%rsi)
	movq	-24(%rdx), %rdi
	movq	%rdi, -24(%rsi)
	movq	-16(%rdx), %rdi
	movq	%rdi, -16(%rsi)
	movq	-8(%rdx), %rdi
	movq	%rdi, -8(%rsi)
	cmpq	%rdx, %rbx
	jne	.L907
	leaq	-136(%rbx), %rdx
	movabsq	$1220740416642543857, %rbx
	subq	%r8, %rdx
	shrq	$3, %rdx
	imulq	%rbx, %rdx
	movabsq	$2305843009213693951, %rbx
	andq	%rbx, %rdx
	addq	$2, %rdx
	movq	%rdx, %rsi
	salq	$4, %rsi
	addq	%rsi, %rdx
	leaq	(%rax,%rdx,8), %rsi
.L906:
	movq	%rax, %xmm1
	movq	%rsi, %xmm4
	movq	%rcx, 24(%r13)
	addq	$8, %r12
	punpcklqdq	%xmm4, %xmm1
	movups	%xmm1, 8(%r13)
	cmpq	%r12, -200(%rbp)
	jne	.L908
.L897:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L922
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L920:
	.cfi_restore_state
	testq	%rcx, %rcx
	jne	.L923
	movl	$136, %esi
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	jmp	.L903
	.p2align 4,,10
	.p2align 3
.L911:
	movl	$136, %esi
	movl	$136, %ecx
	jmp	.L902
	.p2align 4,,10
	.p2align 3
.L921:
	movq	%rcx, -240(%rbp)
	movq	%rdx, -232(%rbp)
	movq	%r8, -224(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-224(%rbp), %r8
	movq	-232(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movq	-240(%rbp), %rcx
	jmp	.L905
.L919:
	leaq	.LC9(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L922:
	call	__stack_chk_fail@PLT
.L923:
	cmpq	$15790320, %rcx
	movl	$15790320, %eax
	cmova	%rax, %rcx
	imulq	$136, %rcx, %rcx
	movq	%rcx, %rsi
	jmp	.L902
	.cfi_endproc
.LFE19367:
	.size	_ZNK2v88internal8compiler17AccessInfoFactory26ComputePropertyAccessInfosERKSt6vectorINS0_6HandleINS0_3MapEEESaIS6_EENS4_INS0_4NameEEENS1_10AccessModeEPNS0_10ZoneVectorINS1_18PropertyAccessInfoEEE, .-_ZNK2v88internal8compiler17AccessInfoFactory26ComputePropertyAccessInfosERKSt6vectorINS0_6HandleINS0_3MapEEESaIS6_EENS4_INS0_4NameEEENS1_10AccessModeEPNS0_10ZoneVectorINS1_18PropertyAccessInfoEEE
	.section	.rodata._ZNSt6vectorIPKN2v88internal8compiler21CompilationDependencyENS1_13ZoneAllocatorIS5_EEE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPKS5_S8_EEEEvNSB_IPS5_S8_EET_SH_St20forward_iterator_tag.str1.1,"aMS",@progbits,1
.LC17:
	.string	"vector::_M_range_insert"
	.section	.text._ZNSt6vectorIPKN2v88internal8compiler21CompilationDependencyENS1_13ZoneAllocatorIS5_EEE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPKS5_S8_EEEEvNSB_IPS5_S8_EET_SH_St20forward_iterator_tag,"axG",@progbits,_ZNSt6vectorIPKN2v88internal8compiler21CompilationDependencyENS1_13ZoneAllocatorIS5_EEE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPKS5_S8_EEEEvNSB_IPS5_S8_EET_SH_St20forward_iterator_tag,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPKN2v88internal8compiler21CompilationDependencyENS1_13ZoneAllocatorIS5_EEE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPKS5_S8_EEEEvNSB_IPS5_S8_EET_SH_St20forward_iterator_tag
	.type	_ZNSt6vectorIPKN2v88internal8compiler21CompilationDependencyENS1_13ZoneAllocatorIS5_EEE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPKS5_S8_EEEEvNSB_IPS5_S8_EET_SH_St20forward_iterator_tag, @function
_ZNSt6vectorIPKN2v88internal8compiler21CompilationDependencyENS1_13ZoneAllocatorIS5_EEE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPKS5_S8_EEEEvNSB_IPS5_S8_EET_SH_St20forward_iterator_tag:
.LFB23348:
	.cfi_startproc
	endbr64
	cmpq	%rdx, %rcx
	je	.L1026
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rcx, %r14
	pushq	%r13
	subq	%rdx, %r14
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	movq	%r14, %rax
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	sarq	$3, %rax
	subq	$16, %rsp
	movq	16(%rdi), %rdi
	movq	24(%rbx), %rdx
	subq	%rdi, %rdx
	sarq	$3, %rdx
	cmpq	%rax, %rdx
	jb	.L927
	movq	%rdi, %r8
	subq	%rsi, %r8
	movq	%r8, %r9
	sarq	$3, %r9
	cmpq	%r9, %rax
	jnb	.L928
	movl	$16, %ecx
	movq	%rdi, %rdx
	leaq	-8(%r14), %rax
	subq	%r14, %rcx
	subq	%r14, %rdx
	shrq	$3, %rax
	testq	%rcx, %rcx
	leaq	16(%rdi), %rcx
	setle	%sil
	cmpq	%rcx, %rdx
	setnb	%cl
	orb	%cl, %sil
	je	.L966
	movabsq	$2305843009213693948, %rcx
	testq	%rcx, %rax
	je	.L966
	leaq	1(%rax), %r8
	xorl	%eax, %eax
	movq	%r8, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L930:
	movdqu	(%rdx,%rax), %xmm1
	movups	%xmm1, (%rdi,%rax)
	addq	$16, %rax
	cmpq	%rcx, %rax
	jne	.L930
	movq	%r8, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rcx
	leaq	(%rdx,%rcx), %rsi
	addq	%rdi, %rcx
	cmpq	%rax, %r8
	je	.L932
	movq	(%rsi), %rax
	movq	%rax, (%rcx)
.L932:
	addq	%r14, 16(%rbx)
	cmpq	%r12, %rdx
	je	.L933
	subq	%r12, %rdx
	movq	%r12, %rsi
	subq	%rdx, %rdi
	call	memmove@PLT
.L933:
	movq	%r14, %rdx
.L1029:
	addq	$16, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	memmove@PLT
	.p2align 4,,10
	.p2align 3
.L927:
	.cfi_restore_state
	movq	8(%rbx), %rsi
	movl	$268435455, %r14d
	movq	%r14, %rdx
	subq	%rsi, %rdi
	sarq	$3, %rdi
	subq	%rdi, %rdx
	cmpq	%rdx, %rax
	ja	.L1030
	cmpq	%rdi, %rax
	cmovb	%rdi, %rax
	addq	%rdi, %rax
	jc	.L947
	testq	%rax, %rax
	jne	.L1031
	xorl	%edi, %edi
	xorl	%eax, %eax
.L949:
	cmpq	%rsi, %r12
	je	.L972
	leaq	-8(%r12), %r10
	leaq	15(%rax), %rdx
	subq	%rsi, %r10
	subq	%rsi, %rdx
	movq	%r10, %r9
	shrq	$3, %r9
	cmpq	$30, %rdx
	jbe	.L973
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %r9
	je	.L973
	addq	$1, %r9
	xorl	%edx, %edx
	movq	%r9, %r8
	shrq	%r8
	salq	$4, %r8
	.p2align 4,,10
	.p2align 3
.L954:
	movdqu	(%rsi,%rdx), %xmm5
	movups	%xmm5, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%r8, %rdx
	jne	.L954
	movq	%r9, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %r8
	addq	%r8, %rsi
	addq	%rax, %r8
	cmpq	%rdx, %r9
	je	.L956
	movq	(%rsi), %rdx
	movq	%rdx, (%r8)
.L956:
	leaq	8(%rax,%r10), %rsi
.L952:
	subq	$8, %rcx
	leaq	15(%r13), %rdx
	subq	%r13, %rcx
	subq	%rsi, %rdx
	movq	%rcx, %r8
	shrq	$3, %r8
	cmpq	$30, %rdx
	jbe	.L974
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %r8
	je	.L974
	leaq	1(%r8), %r10
	xorl	%edx, %edx
	movq	%r10, %r9
	shrq	%r9
	salq	$4, %r9
	.p2align 4,,10
	.p2align 3
.L958:
	movdqu	0(%r13,%rdx), %xmm2
	movups	%xmm2, (%rsi,%rdx)
	addq	$16, %rdx
	cmpq	%r9, %rdx
	jne	.L958
	movq	%r10, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %r8
	addq	%r8, %r13
	addq	%rsi, %r8
	cmpq	%rdx, %r10
	je	.L960
	movq	0(%r13), %rdx
	movq	%rdx, (%r8)
.L960:
	movq	16(%rbx), %rdx
	leaq	8(%rsi,%rcx), %r8
	cmpq	%rdx, %r12
	je	.L961
	subq	%r12, %rdx
	leaq	-8(%rdx), %r10
	leaq	24(%rsi,%rcx), %rdx
	movq	%r10, %r9
	shrq	$3, %r9
	cmpq	%rdx, %r12
	leaq	16(%r12), %rdx
	setnb	%cl
	cmpq	%rdx, %r8
	setnb	%dl
	orb	%dl, %cl
	je	.L975
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %r9
	je	.L975
	addq	$1, %r9
	xorl	%edx, %edx
	movq	%r9, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L963:
	movdqu	(%r12,%rdx), %xmm6
	movups	%xmm6, (%r8,%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L963
	movq	%r9, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rcx
	addq	%rcx, %r12
	addq	%r8, %rcx
	cmpq	%rdx, %r9
	je	.L965
	movq	(%r12), %rdx
	movq	%rdx, (%rcx)
.L965:
	leaq	8(%r8,%r10), %r8
.L961:
	movq	%rax, %xmm0
	movq	%r8, %xmm7
	movq	%rdi, 24(%rbx)
	punpcklqdq	%xmm7, %xmm0
	movups	%xmm0, 8(%rbx)
.L924:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L928:
	.cfi_restore_state
	leaq	0(%r13,%r8), %rsi
	cmpq	%rsi, %rcx
	je	.L967
	subq	$8, %rcx
	leaq	16(%r8,%r13), %rdx
	subq	%rsi, %rcx
	shrq	$3, %rcx
	cmpq	%rdx, %rdi
	leaq	16(%rdi), %rdx
	setnb	%r10b
	cmpq	%rdx, %rsi
	setnb	%dl
	orb	%dl, %r10b
	je	.L968
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rcx
	je	.L968
	addq	$1, %rcx
	xorl	%edx, %edx
	movq	%rcx, %r10
	shrq	%r10
	salq	$4, %r10
	.p2align 4,,10
	.p2align 3
.L936:
	movdqu	(%rsi,%rdx), %xmm3
	movups	%xmm3, (%rdi,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %r10
	jne	.L936
	movq	%rcx, %r11
	andq	$-2, %r11
	leaq	0(,%r11,8), %r10
	leaq	(%rsi,%r10), %rdx
	addq	%rdi, %r10
	cmpq	%r11, %rcx
	je	.L938
	movq	(%rdx), %rdx
	movq	%rdx, (%r10)
.L938:
	movq	16(%rbx), %r10
.L934:
	movq	%rax, %rdx
	subq	%r9, %rdx
	leaq	(%r10,%rdx,8), %rdx
	movq	%rdx, 16(%rbx)
	cmpq	%r12, %rdi
	je	.L939
	subq	%r12, %rdi
	subq	%r9, %rax
	leaq	-8(%rdi), %rcx
	leaq	16(%r10,%rax,8), %rax
	shrq	$3, %rcx
	cmpq	%rax, %r12
	leaq	16(%r12), %rax
	setnb	%dil
	cmpq	%rax, %rdx
	setnb	%al
	orb	%al, %dil
	je	.L969
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rcx
	je	.L969
	addq	$1, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	shrq	%rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L941:
	movdqu	(%r12,%rax), %xmm4
	movups	%xmm4, (%rdx,%rax)
	addq	$16, %rax
	cmpq	%rdi, %rax
	jne	.L941
	movq	%rcx, %r9
	andq	$-2, %r9
	leaq	0(,%r9,8), %rax
	leaq	(%r12,%rax), %rdi
	addq	%rax, %rdx
	cmpq	%r9, %rcx
	je	.L943
	movq	(%rdi), %rax
	movq	%rax, (%rdx)
.L943:
	movq	16(%rbx), %rdx
.L939:
	addq	%r8, %rdx
	movq	%rdx, 16(%rbx)
	cmpq	%rsi, %r13
	je	.L924
	movq	%r8, %rdx
	jmp	.L1029
	.p2align 4,,10
	.p2align 3
.L1026:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	ret
	.p2align 4,,10
	.p2align 3
.L966:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L929:
	movq	(%rdx,%rcx,8), %rsi
	movq	%rsi, (%rdi,%rcx,8)
	movq	%rcx, %rsi
	addq	$1, %rcx
	cmpq	%rsi, %rax
	jne	.L929
	jmp	.L932
	.p2align 4,,10
	.p2align 3
.L968:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L935:
	movq	(%rsi,%rdx,8), %r10
	movq	%r10, (%rdi,%rdx,8)
	movq	%rdx, %r10
	addq	$1, %rdx
	cmpq	%r10, %rcx
	jne	.L935
	jmp	.L938
	.p2align 4,,10
	.p2align 3
.L973:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L953:
	movq	(%rsi,%rdx,8), %r8
	movq	%r8, (%rax,%rdx,8)
	movq	%rdx, %r8
	addq	$1, %rdx
	cmpq	%r8, %r9
	jne	.L953
	jmp	.L956
	.p2align 4,,10
	.p2align 3
.L974:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L957:
	movq	0(%r13,%rdx,8), %r9
	movq	%r9, (%rsi,%rdx,8)
	movq	%rdx, %r9
	addq	$1, %rdx
	cmpq	%r9, %r8
	jne	.L957
	jmp	.L960
	.p2align 4,,10
	.p2align 3
.L969:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L940:
	movq	(%r12,%rax,8), %rdi
	movq	%rdi, (%rdx,%rax,8)
	movq	%rax, %rdi
	addq	$1, %rax
	cmpq	%rcx, %rdi
	jne	.L940
	jmp	.L943
	.p2align 4,,10
	.p2align 3
.L975:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L962:
	movq	(%r12,%rdx,8), %rcx
	movq	%rcx, (%r8,%rdx,8)
	movq	%rdx, %rcx
	addq	$1, %rdx
	cmpq	%r9, %rcx
	jne	.L962
	jmp	.L965
	.p2align 4,,10
	.p2align 3
.L967:
	movq	%rdi, %r10
	jmp	.L934
	.p2align 4,,10
	.p2align 3
.L972:
	movq	%rax, %rsi
	jmp	.L952
.L1031:
	cmpq	$268435455, %rax
	cmova	%r14, %rax
	leaq	0(,%rax,8), %r14
	movq	%r14, %rsi
.L948:
	movq	(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L1032
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L951:
	movq	8(%rbx), %rsi
	leaq	(%rax,%r14), %rdi
	jmp	.L949
.L1032:
	movq	%rcx, -40(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-40(%rbp), %rcx
	jmp	.L951
.L947:
	movl	$2147483640, %esi
	movl	$2147483640, %r14d
	jmp	.L948
.L1030:
	leaq	.LC17(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE23348:
	.size	_ZNSt6vectorIPKN2v88internal8compiler21CompilationDependencyENS1_13ZoneAllocatorIS5_EEE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPKS5_S8_EEEEvNSB_IPS5_S8_EET_SH_St20forward_iterator_tag, .-_ZNSt6vectorIPKN2v88internal8compiler21CompilationDependencyENS1_13ZoneAllocatorIS5_EEE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPKS5_S8_EEEEvNSB_IPS5_S8_EET_SH_St20forward_iterator_tag
	.section	.text._ZN2v88internal8compiler18PropertyAccessInfo5MergeEPKS2_NS1_10AccessModeEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18PropertyAccessInfo5MergeEPKS2_NS1_10AccessModeEPNS0_4ZoneE
	.type	_ZN2v88internal8compiler18PropertyAccessInfo5MergeEPKS2_NS1_10AccessModeEPNS0_4ZoneE, @function
_ZN2v88internal8compiler18PropertyAccessInfo5MergeEPKS2_NS1_10AccessModeEPNS0_4ZoneE:
.LFB19322:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	(%rsi), %edi
	cmpl	%edi, (%rbx)
	jne	.L1132
	movq	88(%rbx), %rax
	movq	%rsi, %r12
	cmpq	%rax, 88(%rsi)
	je	.L1262
.L1132:
	xorl	%eax, %eax
.L1033:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1262:
	.cfi_restore_state
	leaq	.L1037(%rip), %r8
	movl	%edi, %esi
	movslq	(%r8,%rsi,4), %rax
	addq	%r8, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler18PropertyAccessInfo5MergeEPKS2_NS1_10AccessModeEPNS0_4ZoneE,"a",@progbits
	.align 4
	.align 4
.L1037:
	.long	.L1040-.L1037
	.long	.L1036-.L1037
	.long	.L1039-.L1037
	.long	.L1039-.L1037
	.long	.L1038-.L1037
	.long	.L1132-.L1037
	.long	.L1036-.L1037
	.section	.text._ZN2v88internal8compiler18PropertyAccessInfo5MergeEPKS2_NS1_10AccessModeEPNS0_4ZoneE
.L1038:
	movq	72(%rbx), %rcx
	xorl	%eax, %eax
	cmpq	%rcx, 72(%r12)
	jne	.L1033
	movq	24(%r12), %r13
	movq	16(%r12), %r12
	movq	24(%rbx), %r14
	cmpq	%r12, %r13
	je	.L1102
	movq	32(%rbx), %rax
	movq	%r13, %rsi
	subq	%r12, %rsi
	movq	%rsi, %rdx
	subq	%r14, %rax
	sarq	$3, %rdx
	sarq	$3, %rax
	cmpq	%rax, %rdx
	ja	.L1076
	leaq	-8(%r13), %rax
	leaq	15(%r12), %rdx
	subq	%r12, %rax
	subq	%r14, %rdx
	shrq	$3, %rax
	cmpq	$30, %rdx
	jbe	.L1144
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rax
	je	.L1144
	leaq	1(%rax), %rdx
	xorl	%eax, %eax
	movq	%rdx, %rcx
	shrq	%rcx
	salq	$4, %rcx
.L1078:
	movdqu	(%r12,%rax), %xmm5
	movups	%xmm5, (%r14,%rax)
	addq	$16, %rax
	cmpq	%rcx, %rax
	jne	.L1078
	movq	%rdx, %rcx
	andq	$-2, %rcx
	leaq	0(,%rcx,8), %rax
	addq	%rax, %r12
	addq	%rax, %r14
	cmpq	%rcx, %rdx
	jne	.L1261
	jmp	.L1107
.L1040:
	testl	%edi, %edi
	sete	%al
	jmp	.L1033
.L1036:
	movq	24(%r12), %r13
	movq	16(%r12), %r12
	movq	24(%rbx), %r14
	cmpq	%r12, %r13
	je	.L1102
	movq	32(%rbx), %rax
	movq	%r13, %rsi
	subq	%r12, %rsi
	movq	%rsi, %rcx
	subq	%r14, %rax
	sarq	$3, %rcx
	sarq	$3, %rax
	cmpq	%rax, %rcx
	ja	.L1103
	subq	$8, %r13
	leaq	15(%r12), %rax
	subq	%r12, %r13
	subq	%r14, %rax
	shrq	$3, %r13
	cmpq	$30, %rax
	jbe	.L1151
	movabsq	$2305843009213693948, %rax
	testq	%rax, %r13
	je	.L1151
	addq	$1, %r13
	xorl	%eax, %eax
	movq	%r13, %rdx
	shrq	%rdx
	salq	$4, %rdx
.L1105:
	movdqu	(%r12,%rax), %xmm1
	movups	%xmm1, (%r14,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L1105
	movq	%r13, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rax
	addq	%rax, %r12
	addq	%rax, %r14
	cmpq	%rdx, %r13
	je	.L1107
.L1261:
	movq	(%r12), %rax
	movq	%rax, (%r14)
.L1107:
	addq	%rsi, 24(%rbx)
.L1102:
	movl	$1, %eax
	jmp	.L1033
.L1039:
	movl	96(%r12), %edi
	movl	96(%rbx), %esi
	xorl	%eax, %eax
	andl	$131071, %edi
	andl	$131071, %esi
	cmpl	%esi, %edi
	jne	.L1033
	cmpl	$2, %edx
	jg	.L1041
	testl	%edx, %edx
	jg	.L1042
	jne	.L1044
.L1043:
	movzbl	104(%rbx), %eax
	movzbl	104(%r12), %edx
	cmpb	%dl, %al
	je	.L1045
	cmpb	$2, %al
	je	.L1132
	cmpb	$2, %dl
	je	.L1132
	movb	$4, 104(%rbx)
.L1045:
	movq	128(%rbx), %rax
	cmpq	%rax, 128(%r12)
	je	.L1044
	movq	$0, 128(%rbx)
	.p2align 4,,10
	.p2align 3
.L1044:
	movq	112(%r12), %rsi
	movq	112(%rbx), %rdi
	movq	%rcx, %rdx
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	24(%rbx), %r15
	movq	%rax, 112(%rbx)
	movq	24(%r12), %r14
	movq	16(%r12), %r13
	cmpq	%r13, %r14
	je	.L1048
	movq	32(%rbx), %rax
	movq	%r14, %rcx
	subq	%r13, %rcx
	movq	%rcx, %rdx
	subq	%r15, %rax
	sarq	$3, %rdx
	sarq	$3, %rax
	cmpq	%rax, %rdx
	ja	.L1049
	leaq	-8(%r14), %rax
	leaq	15(%r13), %rdx
	subq	%r13, %rax
	subq	%r15, %rdx
	shrq	$3, %rax
	cmpq	$30, %rdx
	jbe	.L1136
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rax
	je	.L1136
	leaq	1(%rax), %rdx
	xorl	%eax, %eax
	movq	%rdx, %rsi
	shrq	%rsi
	salq	$4, %rsi
.L1051:
	movdqu	0(%r13,%rax), %xmm5
	movups	%xmm5, (%r15,%rax)
	addq	$16, %rax
	cmpq	%rsi, %rax
	jne	.L1051
	movq	%rdx, %rsi
	andq	$-2, %rsi
	leaq	0(,%rsi,8), %rax
	addq	%rax, %r13
	addq	%rax, %r15
	cmpq	%rsi, %rdx
	je	.L1053
	movq	0(%r13), %rax
	movq	%rax, (%r15)
.L1053:
	addq	%rcx, 24(%rbx)
.L1048:
	movq	56(%r12), %rcx
	movq	48(%r12), %rdx
	leaq	40(%rbx), %rdi
	movq	56(%rbx), %rsi
	call	_ZNSt6vectorIPKN2v88internal8compiler21CompilationDependencyENS1_13ZoneAllocatorIS5_EEE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPKS5_S8_EEEEvNSB_IPS5_S8_EET_SH_St20forward_iterator_tag
	movl	$1, %eax
	jmp	.L1033
.L1041:
	cmpl	$3, %edx
	jne	.L1044
	jmp	.L1043
	.p2align 4,,10
	.p2align 3
.L1103:
	movq	16(%rbx), %rdx
	movq	%r14, %rax
	movl	$268435455, %esi
	movq	%rsi, %rdi
	subq	%rdx, %rax
	sarq	$3, %rax
	subq	%rax, %rdi
	cmpq	%rdi, %rcx
	ja	.L1081
	cmpq	%rax, %rcx
	cmovb	%rax, %rcx
	addq	%rax, %rcx
	jc	.L1109
	testq	%rcx, %rcx
	jne	.L1263
	xorl	%ecx, %ecx
	xorl	%esi, %esi
.L1111:
	cmpq	%rdx, %r14
	je	.L1154
	leaq	-8(%r14), %r9
	leaq	15(%rsi), %rax
	subq	%rdx, %r9
	subq	%rdx, %rax
	movq	%r9, %r8
	shrq	$3, %r8
	cmpq	$30, %rax
	jbe	.L1155
	movabsq	$2305843009213693948, %rax
	testq	%rax, %r8
	je	.L1155
	addq	$1, %r8
	xorl	%eax, %eax
	movq	%r8, %rdi
	shrq	%rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L1116:
	movdqu	(%rdx,%rax), %xmm3
	movups	%xmm3, (%rsi,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdi
	jne	.L1116
	movq	%r8, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rdi
	addq	%rdi, %rdx
	addq	%rsi, %rdi
	cmpq	%rax, %r8
	je	.L1118
	movq	(%rdx), %rax
	movq	%rax, (%rdi)
.L1118:
	leaq	8(%rsi,%r9), %rdi
.L1114:
	subq	$8, %r13
	leaq	15(%r12), %rax
	subq	%r12, %r13
	subq	%rdi, %rax
	movq	%r13, %rdx
	shrq	$3, %rdx
	cmpq	$30, %rax
	jbe	.L1156
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rdx
	je	.L1156
	leaq	1(%rdx), %r8
	xorl	%eax, %eax
	movq	%r8, %rdx
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L1120:
	movdqu	(%r12,%rax), %xmm2
	movups	%xmm2, (%rdi,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L1120
	movq	%r8, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rdx
	addq	%rdx, %r12
	addq	%rdi, %rdx
	cmpq	%rax, %r8
	je	.L1122
	movq	(%r12), %rax
	movq	%rax, (%rdx)
.L1122:
	movq	24(%rbx), %rax
	leaq	8(%rdi,%r13), %rdx
	cmpq	%rax, %r14
	je	.L1123
	leaq	-8(%rax), %r8
	leaq	24(%rdi,%r13), %rax
	subq	%r14, %r8
	movq	%r8, %r9
	shrq	$3, %r9
	cmpq	%rax, %r14
	leaq	16(%r14), %rax
	setnb	%dil
	cmpq	%rax, %rdx
	setnb	%al
	orb	%al, %dil
	je	.L1157
	movabsq	$2305843009213693948, %rax
	testq	%rax, %r9
	je	.L1157
	addq	$1, %r9
	xorl	%eax, %eax
	movq	%r9, %rdi
	shrq	%rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L1125:
	movdqu	(%r14,%rax), %xmm4
	movups	%xmm4, (%rdx,%rax)
	addq	$16, %rax
	cmpq	%rdi, %rax
	jne	.L1125
	movq	%r9, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rdi
	addq	%rdi, %r14
	addq	%rdx, %rdi
	cmpq	%rax, %r9
	je	.L1127
	movq	(%r14), %rax
	movq	%rax, (%rdi)
.L1127:
	leaq	8(%rdx,%r8), %rdx
.L1123:
	movq	%rsi, %xmm0
	movq	%rdx, %xmm6
	movq	%rcx, 32(%rbx)
	punpcklqdq	%xmm6, %xmm0
	movups	%xmm0, 16(%rbx)
	jmp	.L1102
.L1042:
	movq	128(%r12), %rdx
	cmpq	%rdx, 128(%rbx)
	jne	.L1033
	movzbl	104(%r12), %edx
	cmpb	%dl, 104(%rbx)
	jne	.L1033
	movq	80(%r12), %rdx
	cmpq	%rdx, 80(%rbx)
	jne	.L1033
	jmp	.L1044
.L1151:
	xorl	%eax, %eax
.L1104:
	movq	(%r12,%rax,8), %rdx
	movq	%rdx, (%r14,%rax,8)
	movq	%rax, %rdx
	addq	$1, %rax
	cmpq	%rdx, %r13
	jne	.L1104
	jmp	.L1107
.L1156:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L1119:
	movq	(%r12,%rax,8), %r8
	movq	%r8, (%rdi,%rax,8)
	movq	%rax, %r8
	addq	$1, %rax
	cmpq	%r8, %rdx
	jne	.L1119
	jmp	.L1122
.L1049:
	movq	16(%rbx), %rcx
	movq	%r15, %rax
	movl	$268435455, %esi
	movq	%rsi, %rdi
	subq	%rcx, %rax
	sarq	$3, %rax
	subq	%rax, %rdi
	cmpq	%rdi, %rdx
	ja	.L1081
	cmpq	%rax, %rdx
	cmovb	%rax, %rdx
	addq	%rdx, %rax
	jc	.L1056
	testq	%rax, %rax
	jne	.L1264
	xorl	%edx, %edx
	xorl	%esi, %esi
.L1058:
	cmpq	%rcx, %r15
	je	.L1139
	leaq	-8(%r15), %r8
	leaq	15(%rcx), %rax
	subq	%rcx, %r8
	subq	%rsi, %rax
	movq	%r8, %r9
	shrq	$3, %r9
	cmpq	$30, %rax
	jbe	.L1140
	movabsq	$2305843009213693948, %rax
	testq	%rax, %r9
	je	.L1140
	addq	$1, %r9
	xorl	%eax, %eax
	movq	%r9, %rdi
	shrq	%rdi
	salq	$4, %rdi
.L1063:
	movdqu	(%rcx,%rax), %xmm7
	movups	%xmm7, (%rsi,%rax)
	addq	$16, %rax
	cmpq	%rdi, %rax
	jne	.L1063
	movq	%r9, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rdi
	addq	%rdi, %rcx
	addq	%rsi, %rdi
	cmpq	%rax, %r9
	je	.L1065
	movq	(%rcx), %rax
	movq	%rax, (%rdi)
.L1065:
	leaq	8(%rsi,%r8), %rdi
.L1061:
	subq	$8, %r14
	leaq	15(%r13), %rax
	subq	%r13, %r14
	subq	%rdi, %rax
	movq	%r14, %r8
	shrq	$3, %r8
	cmpq	$30, %rax
	jbe	.L1141
	movabsq	$2305843009213693948, %rax
	testq	%rax, %r8
	je	.L1141
	addq	$1, %r8
	xorl	%eax, %eax
	movq	%r8, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L1067:
	movdqu	0(%r13,%rax), %xmm7
	movups	%xmm7, (%rdi,%rax)
	addq	$16, %rax
	cmpq	%rcx, %rax
	jne	.L1067
	movq	%r8, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rcx
	addq	%rcx, %r13
	addq	%rdi, %rcx
	cmpq	%rax, %r8
	je	.L1069
	movq	0(%r13), %rax
	movq	%rax, (%rcx)
.L1069:
	movq	24(%rbx), %rax
	leaq	8(%rdi,%r14), %rcx
	cmpq	%rax, %r15
	je	.L1070
	leaq	-8(%rax), %r8
	leaq	24(%rdi,%r14), %rax
	subq	%r15, %r8
	movq	%r8, %r9
	shrq	$3, %r9
	cmpq	%rax, %r15
	leaq	16(%r15), %rax
	setnb	%dil
	cmpq	%rax, %rcx
	setnb	%al
	orb	%al, %dil
	je	.L1142
	movabsq	$2305843009213693948, %rax
	testq	%rax, %r9
	je	.L1142
	addq	$1, %r9
	xorl	%eax, %eax
	movq	%r9, %rdi
	shrq	%rdi
	salq	$4, %rdi
.L1072:
	movdqu	(%r15,%rax), %xmm6
	movups	%xmm6, (%rcx,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdi
	jne	.L1072
	movq	%r9, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rdi
	addq	%rdi, %r15
	addq	%rcx, %rdi
	cmpq	%rax, %r9
	je	.L1074
	movq	(%r15), %rax
	movq	%rax, (%rdi)
.L1074:
	leaq	8(%rcx,%r8), %rcx
.L1070:
	movq	%rsi, %xmm0
	movq	%rcx, %xmm6
	movq	%rdx, 32(%rbx)
	punpcklqdq	%xmm6, %xmm0
	movups	%xmm0, 16(%rbx)
	jmp	.L1048
.L1157:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L1124:
	movq	(%r14,%rax,8), %rdi
	movq	%rdi, (%rdx,%rax,8)
	movq	%rax, %rdi
	addq	$1, %rax
	cmpq	%rdi, %r9
	jne	.L1124
	jmp	.L1127
.L1155:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L1115:
	movq	(%rdx,%rax,8), %rdi
	movq	%rdi, (%rsi,%rax,8)
	movq	%rax, %rdi
	addq	$1, %rax
	cmpq	%r8, %rdi
	jne	.L1115
	jmp	.L1118
.L1154:
	movq	%rsi, %rdi
	jmp	.L1114
.L1076:
	movq	%r14, %r15
	movl	$268435455, %eax
	subq	16(%rbx), %r15
	sarq	$3, %r15
	movq	%rax, %rcx
	subq	%r15, %rcx
	cmpq	%rcx, %rdx
	ja	.L1081
	cmpq	%r15, %rdx
	cmovb	%r15, %rdx
	addq	%r15, %rdx
	movq	%rdx, %r15
	jc	.L1145
	xorl	%ecx, %ecx
	testq	%rdx, %rdx
	jne	.L1265
.L1085:
	movq	16(%rbx), %rdx
	cmpq	%rdx, %r14
	je	.L1147
	leaq	-8(%r14), %r8
	leaq	15(%rdx), %rax
	subq	%rdx, %r8
	subq	%rcx, %rax
	movq	%r8, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rax
	jbe	.L1148
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rsi
	je	.L1148
	addq	$1, %rsi
	xorl	%eax, %eax
	movq	%rsi, %rdi
	shrq	%rdi
	salq	$4, %rdi
.L1090:
	movdqu	(%rdx,%rax), %xmm6
	movups	%xmm6, (%rcx,%rax)
	addq	$16, %rax
	cmpq	%rdi, %rax
	jne	.L1090
	movq	%rsi, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rdi
	addq	%rdi, %rdx
	addq	%rcx, %rdi
	cmpq	%rax, %rsi
	je	.L1092
	movq	(%rdx), %rax
	movq	%rax, (%rdi)
.L1092:
	leaq	8(%rcx,%r8), %rsi
.L1088:
	leaq	-8(%r13), %rdx
	leaq	15(%r12), %rax
	subq	%r12, %rdx
	subq	%rsi, %rax
	movq	%rdx, %rdi
	shrq	$3, %rdi
	cmpq	$30, %rax
	jbe	.L1149
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rdi
	je	.L1149
	addq	$1, %rdi
	xorl	%eax, %eax
	movq	%rdi, %r8
	shrq	%r8
	salq	$4, %r8
.L1094:
	movdqu	(%r12,%rax), %xmm7
	movups	%xmm7, (%rsi,%rax)
	addq	$16, %rax
	cmpq	%r8, %rax
	jne	.L1094
	movq	%rdi, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %r8
	addq	%r8, %r12
	addq	%rsi, %r8
	cmpq	%rax, %rdi
	je	.L1096
	movq	(%r12), %rax
	movq	%rax, (%r8)
.L1096:
	movq	24(%rbx), %rax
	leaq	8(%rsi,%rdx), %rdi
	cmpq	%rax, %r14
	je	.L1097
	subq	$8, %rax
	subq	%r14, %rax
	movq	%rax, %r8
	movq	%rax, %r9
	leaq	24(%rsi,%rdx), %rax
	shrq	$3, %r8
	cmpq	%rax, %r14
	leaq	16(%r14), %rax
	setnb	%dl
	cmpq	%rax, %rdi
	setnb	%al
	orb	%al, %dl
	je	.L1150
	movabsq	$2305843009213693948, %rax
	testq	%rax, %r8
	je	.L1150
	addq	$1, %r8
	xorl	%eax, %eax
	movq	%r8, %rdx
	shrq	%rdx
	salq	$4, %rdx
.L1099:
	movdqu	(%r14,%rax), %xmm5
	movups	%xmm5, (%rdi,%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L1099
	movq	%r8, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rdx
	addq	%rdx, %r14
	addq	%rdi, %rdx
	cmpq	%rax, %r8
	je	.L1101
	movq	(%r14), %rax
	movq	%rax, (%rdx)
.L1101:
	leaq	8(%rdi,%r9), %rdi
.L1097:
	movq	%rcx, %xmm0
	movq	%rdi, %xmm7
	leaq	(%rcx,%r15,8), %rax
	punpcklqdq	%xmm7, %xmm0
	movq	%rax, 32(%rbx)
	movups	%xmm0, 16(%rbx)
	jmp	.L1102
.L1136:
	xorl	%edx, %edx
.L1050:
	movq	0(%r13,%rdx,8), %rsi
	movq	%rsi, (%r15,%rdx,8)
	movq	%rdx, %rsi
	addq	$1, %rdx
	cmpq	%rax, %rsi
	jne	.L1050
	jmp	.L1053
.L1145:
	movl	$268435455, %r15d
.L1084:
	movq	8(%rbx), %rdi
	leaq	0(,%r15,8), %rsi
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L1266
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
	jmp	.L1085
.L1144:
	xorl	%edx, %edx
.L1077:
	movq	(%r12,%rdx,8), %rcx
	movq	%rcx, (%r14,%rdx,8)
	movq	%rdx, %rcx
	addq	$1, %rdx
	cmpq	%rcx, %rax
	jne	.L1077
	jmp	.L1107
.L1142:
	xorl	%eax, %eax
.L1071:
	movq	(%r15,%rax,8), %rdi
	movq	%rdi, (%rcx,%rax,8)
	movq	%rax, %rdi
	addq	$1, %rax
	cmpq	%rdi, %r9
	jne	.L1071
	jmp	.L1074
.L1141:
	xorl	%eax, %eax
.L1066:
	movq	0(%r13,%rax,8), %rcx
	movq	%rcx, (%rdi,%rax,8)
	movq	%rax, %rcx
	addq	$1, %rax
	cmpq	%r8, %rcx
	jne	.L1066
	jmp	.L1069
.L1140:
	xorl	%eax, %eax
.L1062:
	movq	(%rcx,%rax,8), %rdi
	movq	%rdi, (%rsi,%rax,8)
	movq	%rax, %rdi
	addq	$1, %rax
	cmpq	%rdi, %r9
	jne	.L1062
	jmp	.L1065
.L1149:
	xorl	%eax, %eax
.L1093:
	movq	(%r12,%rax,8), %r8
	movq	%r8, (%rsi,%rax,8)
	movq	%rax, %r8
	addq	$1, %rax
	cmpq	%r8, %rdi
	jne	.L1093
	jmp	.L1096
.L1148:
	xorl	%eax, %eax
.L1089:
	movq	(%rdx,%rax,8), %rdi
	movq	%rdi, (%rcx,%rax,8)
	movq	%rax, %rdi
	addq	$1, %rax
	cmpq	%rdi, %rsi
	jne	.L1089
	jmp	.L1092
.L1150:
	xorl	%eax, %eax
.L1098:
	movq	(%r14,%rax,8), %rdx
	movq	%rdx, (%rdi,%rax,8)
	movq	%rax, %rdx
	addq	$1, %rax
	cmpq	%rdx, %r8
	jne	.L1098
	jmp	.L1101
.L1139:
	movq	%rsi, %rdi
	jmp	.L1061
.L1147:
	movq	%rcx, %rsi
	jmp	.L1088
.L1266:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L1085
.L1263:
	cmpq	$268435455, %rcx
	cmova	%rsi, %rcx
	leaq	0(,%rcx,8), %r15
	movq	%r15, %r8
.L1110:
	movq	8(%rbx), %rdi
	movq	16(%rdi), %rsi
	movq	24(%rdi), %rax
	subq	%rsi, %rax
	cmpq	%r8, %rax
	jb	.L1267
	addq	%rsi, %r8
	movq	%r8, 16(%rdi)
.L1113:
	movq	16(%rbx), %rdx
	leaq	(%rsi,%r15), %rcx
	jmp	.L1111
.L1267:
	movq	%r8, %rsi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rsi
	jmp	.L1113
.L1265:
	cmpq	$268435455, %rdx
	cmova	%rax, %r15
	jmp	.L1084
.L1056:
	movl	$268435455, %eax
.L1057:
	movq	8(%rbx), %rdi
	leaq	0(,%rax,8), %rdx
	movq	%rdx, %r8
	movq	16(%rdi), %rsi
	movq	24(%rdi), %rax
	subq	%rsi, %rax
	cmpq	%rax, %rdx
	ja	.L1268
	addq	%rsi, %r8
	movq	%r8, 16(%rdi)
.L1060:
	movq	16(%rbx), %rcx
	addq	%rsi, %rdx
	jmp	.L1058
.L1268:
	movq	%rdx, %rsi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %rsi
	jmp	.L1060
.L1264:
	cmpq	$268435455, %rax
	cmova	%rsi, %rax
	jmp	.L1057
.L1109:
	movl	$2147483640, %r8d
	movl	$2147483640, %r15d
	jmp	.L1110
.L1081:
	leaq	.LC17(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE19322:
	.size	_ZN2v88internal8compiler18PropertyAccessInfo5MergeEPKS2_NS1_10AccessModeEPNS0_4ZoneE, .-_ZN2v88internal8compiler18PropertyAccessInfo5MergeEPKS2_NS1_10AccessModeEPNS0_4ZoneE
	.section	.rodata._ZNK2v88internal8compiler17AccessInfoFactory24MergePropertyAccessInfosENS0_10ZoneVectorINS1_18PropertyAccessInfoEEENS1_10AccessModeEPS5_.str1.1,"aMS",@progbits,1
.LC18:
	.string	"!result->empty()"
	.section	.text._ZNK2v88internal8compiler17AccessInfoFactory24MergePropertyAccessInfosENS0_10ZoneVectorINS1_18PropertyAccessInfoEEENS1_10AccessModeEPS5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler17AccessInfoFactory24MergePropertyAccessInfosENS0_10ZoneVectorINS1_18PropertyAccessInfoEEENS1_10AccessModeEPS5_
	.type	_ZNK2v88internal8compiler17AccessInfoFactory24MergePropertyAccessInfosENS0_10ZoneVectorINS1_18PropertyAccessInfoEEENS1_10AccessModeEPS5_, @function
_ZNK2v88internal8compiler17AccessInfoFactory24MergePropertyAccessInfosENS0_10ZoneVectorINS1_18PropertyAccessInfoEEENS1_10AccessModeEPS5_:
.LFB19370:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	.L1278(%rip), %rbx
	subq	$88, %rsp
	movq	%rcx, -72(%rbp)
	movq	16(%rsi), %r13
	movq	8(%rsi), %rcx
	movq	-72(%rbp), %r9
	movq	%rdi, -56(%rbp)
	movl	%edx, -60(%rbp)
	cmpq	%rcx, %r13
	je	.L1591
	.p2align 4,,10
	.p2align 3
.L1270:
	leaq	136(%rcx), %r14
	cmpq	%r14, %r13
	je	.L1274
	movq	-56(%rbp), %rax
	movl	-136(%r14), %esi
	movq	%r14, %r12
	movq	24(%rax), %r8
	movq	%rsi, %rdx
	.p2align 4,,10
	.p2align 3
.L1369:
	cmpl	%edx, (%r12)
	jne	.L1275
	movq	-48(%r14), %rax
	cmpq	%rax, 88(%r12)
	jne	.L1275
	movslq	(%rbx,%rsi,4), %rax
	addq	%rbx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZNK2v88internal8compiler17AccessInfoFactory24MergePropertyAccessInfosENS0_10ZoneVectorINS1_18PropertyAccessInfoEEENS1_10AccessModeEPS5_,"a",@progbits
	.align 4
	.align 4
.L1278:
	.long	.L1281-.L1278
	.long	.L1277-.L1278
	.long	.L1280-.L1278
	.long	.L1280-.L1278
	.long	.L1279-.L1278
	.long	.L1275-.L1278
	.long	.L1277-.L1278
	.section	.text._ZNK2v88internal8compiler17AccessInfoFactory24MergePropertyAccessInfosENS0_10ZoneVectorINS1_18PropertyAccessInfoEEENS1_10AccessModeEPS5_
.L1281:
	testl	%edx, %edx
	je	.L1272
.L1275:
	addq	$136, %r12
	cmpq	%r12, %r13
	jne	.L1369
.L1274:
	movq	16(%r9), %r12
	cmpq	24(%r9), %r12
	je	.L1592
	movl	-136(%r14), %eax
	movl	%eax, (%r12)
	movq	-128(%r14), %rdi
	movq	-112(%r14), %r15
	subq	-120(%r14), %r15
	movq	$0, 16(%r12)
	movq	%rdi, 8(%r12)
	movq	$0, 24(%r12)
	movq	$0, 32(%r12)
	je	.L1434
	movq	16(%rdi), %rdx
	movq	24(%rdi), %rax
	leaq	7(%r15), %rsi
	andq	$-8, %rsi
	subq	%rdx, %rax
	cmpq	%rax, %rsi
	ja	.L1593
	addq	%rdx, %rsi
	movq	%rsi, 16(%rdi)
.L1370:
	movq	%rdx, %xmm0
	addq	%rdx, %r15
	punpcklqdq	%xmm0, %xmm0
	movq	%r15, 32(%r12)
	movups	%xmm0, 16(%r12)
	movq	-112(%r14), %rsi
	movq	-120(%r14), %rcx
	cmpq	%rcx, %rsi
	je	.L1373
	subq	$8, %rsi
	leaq	15(%rcx), %rax
	subq	%rcx, %rsi
	subq	%rdx, %rax
	movq	%rsi, %r8
	shrq	$3, %r8
	cmpq	$30, %rax
	jbe	.L1435
	movabsq	$2305843009213693948, %rax
	testq	%rax, %r8
	je	.L1435
	addq	$1, %r8
	xorl	%eax, %eax
	movq	%r8, %rdi
	shrq	%rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L1375:
	movdqu	(%rcx,%rax), %xmm1
	movups	%xmm1, (%rdx,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdi
	jne	.L1375
	movq	%r8, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rdi
	addq	%rdi, %rcx
	addq	%rdx, %rdi
	cmpq	%rax, %r8
	je	.L1377
	movq	(%rcx), %rax
	movq	%rax, (%rdi)
.L1377:
	leaq	8(%rdx,%rsi), %rdx
.L1373:
	movq	%rdx, 24(%r12)
	movq	-96(%r14), %rdi
	movq	-80(%r14), %r15
	subq	-88(%r14), %r15
	movq	$0, 48(%r12)
	movq	%rdi, 40(%r12)
	movq	$0, 56(%r12)
	movq	$0, 64(%r12)
	je	.L1436
	movq	16(%rdi), %rdx
	movq	24(%rdi), %rax
	leaq	7(%r15), %rsi
	andq	$-8, %rsi
	subq	%rdx, %rax
	cmpq	%rax, %rsi
	ja	.L1594
	addq	%rdx, %rsi
	movq	%rsi, 16(%rdi)
.L1378:
	movq	%rdx, %xmm0
	addq	%rdx, %r15
	punpcklqdq	%xmm0, %xmm0
	movq	%r15, 64(%r12)
	movups	%xmm0, 48(%r12)
	movq	-80(%r14), %rsi
	movq	-88(%r14), %rcx
	cmpq	%rcx, %rsi
	je	.L1381
	subq	$8, %rsi
	leaq	15(%rcx), %rax
	subq	%rcx, %rsi
	subq	%rdx, %rax
	movq	%rsi, %r8
	shrq	$3, %r8
	cmpq	$30, %rax
	jbe	.L1437
	movabsq	$2305843009213693948, %rax
	testq	%rax, %r8
	je	.L1437
	addq	$1, %r8
	xorl	%eax, %eax
	movq	%r8, %rdi
	shrq	%rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L1383:
	movdqu	(%rcx,%rax), %xmm2
	movups	%xmm2, (%rdx,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdi
	jne	.L1383
	movq	%r8, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rdi
	addq	%rdi, %rcx
	addq	%rdx, %rdi
	cmpq	%rax, %r8
	je	.L1385
	movq	(%rcx), %rax
	movq	%rax, (%rdi)
.L1385:
	leaq	8(%rdx,%rsi), %rdx
.L1381:
	movq	%rdx, 56(%r12)
	movq	-64(%r14), %rax
	movq	%rax, 72(%r12)
	movq	-56(%r14), %rax
	movq	%rax, 80(%r12)
	movq	-48(%r14), %rax
	movq	%rax, 88(%r12)
	movq	-40(%r14), %rax
	movq	%rax, 96(%r12)
	movzbl	-32(%r14), %eax
	movb	%al, 104(%r12)
	movq	-24(%r14), %rax
	movq	%rax, 112(%r12)
	movq	-16(%r14), %rax
	movq	%rax, 120(%r12)
	movq	-8(%r14), %rax
	movq	%rax, 128(%r12)
	movq	16(%r9), %rax
	leaq	136(%rax), %r8
	movq	%r8, 16(%r9)
.L1386:
	cmpq	%r14, %r13
	je	.L1273
.L1272:
	movq	%r14, %rcx
	jmp	.L1270
.L1279:
	movq	-64(%r14), %rax
	cmpq	%rax, 72(%r12)
	jne	.L1275
	movq	-112(%r14), %rcx
	movq	-120(%r14), %r15
	movq	24(%r12), %rdx
	cmpq	%r15, %rcx
	je	.L1272
	movq	32(%r12), %rsi
	movq	%rcx, %rdi
	subq	%r15, %rdi
	movq	%rdi, %rax
	subq	%rdx, %rsi
	sarq	$3, %rax
	sarq	$3, %rsi
	cmpq	%rsi, %rax
	ja	.L1317
	leaq	-8(%rcx), %rax
	leaq	15(%r15), %rcx
	subq	%r15, %rax
	subq	%rdx, %rcx
	shrq	$3, %rax
	cmpq	$30, %rcx
	jbe	.L1420
	movabsq	$2305843009213693948, %rcx
	testq	%rcx, %rax
	je	.L1420
	leaq	1(%rax), %rcx
	xorl	%eax, %eax
	movq	%rcx, %rsi
	shrq	%rsi
	salq	$4, %rsi
.L1319:
	movdqu	(%r15,%rax), %xmm7
	movups	%xmm7, (%rdx,%rax)
	addq	$16, %rax
	cmpq	%rsi, %rax
	jne	.L1319
	movq	%rcx, %rsi
	andq	$-2, %rsi
	leaq	0(,%rsi,8), %rax
	addq	%rax, %r15
	addq	%rax, %rdx
	cmpq	%rsi, %rcx
	je	.L1348
	movq	(%r15), %rax
	movq	%rax, (%rdx)
	jmp	.L1348
.L1280:
	movl	96(%r12), %edi
	movl	-40(%r14), %eax
	andl	$131071, %edi
	andl	$131071, %eax
	cmpl	%eax, %edi
	jne	.L1275
	movl	-60(%rbp), %eax
	cmpl	$2, %eax
	jg	.L1282
	testl	%eax, %eax
	jg	.L1283
	jne	.L1285
.L1284:
	movzbl	104(%r12), %edi
	movzbl	-32(%r14), %eax
	cmpb	%al, %dil
	je	.L1286
	cmpb	$2, %al
	je	.L1275
	cmpb	$2, %dil
	je	.L1275
	movb	$4, 104(%r12)
.L1286:
	movq	-8(%r14), %rax
	cmpq	%rax, 128(%r12)
	je	.L1285
	movq	$0, 128(%r12)
	jmp	.L1285
.L1277:
	movq	-112(%r14), %rdx
	movq	-120(%r14), %r15
	movq	24(%r12), %rcx
	cmpq	%r15, %rdx
	je	.L1272
	movq	32(%r12), %rsi
	movq	%rdx, %rdi
	subq	%r15, %rdi
	movq	%rdi, %rax
	subq	%rcx, %rsi
	sarq	$3, %rax
	sarq	$3, %rsi
	cmpq	%rsi, %rax
	ja	.L1344
	subq	$8, %rdx
	leaq	15(%r15), %rax
	subq	%r15, %rdx
	subq	%rcx, %rax
	shrq	$3, %rdx
	cmpq	$30, %rax
	jbe	.L1427
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rdx
	je	.L1427
	addq	$1, %rdx
	xorl	%eax, %eax
	movq	%rdx, %rsi
	shrq	%rsi
	salq	$4, %rsi
.L1346:
	movdqu	(%r15,%rax), %xmm5
	movups	%xmm5, (%rcx,%rax)
	addq	$16, %rax
	cmpq	%rsi, %rax
	jne	.L1346
	movq	%rdx, %rsi
	andq	$-2, %rsi
	leaq	0(,%rsi,8), %rax
	addq	%rax, %r15
	addq	%rax, %rcx
	cmpq	%rsi, %rdx
	je	.L1348
	movq	(%r15), %rax
	movq	%rax, (%rcx)
.L1348:
	addq	%rdi, 24(%r12)
	jmp	.L1272
.L1282:
	cmpl	$3, -60(%rbp)
	je	.L1284
.L1285:
	movq	112(%rcx), %rsi
	movq	112(%r12), %rdi
	movq	%r8, %rdx
	movq	%r9, -80(%rbp)
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	24(%r12), %rcx
	movq	-80(%rbp), %r9
	movq	%rax, 112(%r12)
	movq	-112(%r14), %rdx
	movq	-120(%r14), %r15
	cmpq	%r15, %rdx
	je	.L1289
	movq	32(%r12), %rax
	movq	%rdx, %rsi
	subq	%r15, %rsi
	movq	%rsi, %rdi
	subq	%rcx, %rax
	sarq	$3, %rdi
	sarq	$3, %rax
	cmpq	%rax, %rdi
	ja	.L1290
	leaq	-8(%rdx), %rax
	leaq	15(%r15), %rdx
	subq	%r15, %rax
	subq	%rcx, %rdx
	shrq	$3, %rax
	cmpq	$30, %rdx
	jbe	.L1413
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rax
	je	.L1413
	leaq	1(%rax), %rdx
	xorl	%eax, %eax
	movq	%rdx, %rdi
	shrq	%rdi
	salq	$4, %rdi
.L1292:
	movdqu	(%r15,%rax), %xmm3
	movups	%xmm3, (%rcx,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdi
	jne	.L1292
	movq	%rdx, %rdi
	andq	$-2, %rdi
	leaq	0(,%rdi,8), %rax
	addq	%rax, %r15
	addq	%rax, %rcx
	cmpq	%rdi, %rdx
	je	.L1294
	movq	(%r15), %rax
	movq	%rax, (%rcx)
.L1294:
	addq	%rsi, 24(%r12)
.L1289:
	movq	-80(%r14), %rcx
	movq	-88(%r14), %rdx
	leaq	40(%r12), %rdi
	movq	%r9, -80(%rbp)
	movq	56(%r12), %rsi
	call	_ZNSt6vectorIPKN2v88internal8compiler21CompilationDependencyENS1_13ZoneAllocatorIS5_EEE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPKS5_S8_EEEEvNSB_IPS5_S8_EET_SH_St20forward_iterator_tag
	movq	-80(%rbp), %r9
	jmp	.L1272
.L1591:
	movq	-72(%rbp), %rax
	movq	16(%rax), %r8
	.p2align 4,,10
	.p2align 3
.L1273:
	movq	-72(%rbp), %rax
	cmpq	%r8, 8(%rax)
	je	.L1595
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1434:
	.cfi_restore_state
	xorl	%r15d, %r15d
	xorl	%edx, %edx
	jmp	.L1370
	.p2align 4,,10
	.p2align 3
.L1436:
	xorl	%r15d, %r15d
	xorl	%edx, %edx
	jmp	.L1378
	.p2align 4,,10
	.p2align 3
.L1344:
	movq	16(%r12), %rsi
	movq	%rcx, %rdi
	movl	$268435455, %r8d
	movq	%r8, %r10
	subq	%rsi, %rdi
	sarq	$3, %rdi
	subq	%rdi, %r10
	cmpq	%r10, %rax
	ja	.L1322
	cmpq	%rdi, %rax
	cmovb	%rdi, %rax
	addq	%rdi, %rax
	jc	.L1350
	testq	%rax, %rax
	jne	.L1596
	movq	$0, -80(%rbp)
	xorl	%edi, %edi
.L1352:
	cmpq	%rsi, %rcx
	je	.L1430
	leaq	-8(%rcx), %r8
	leaq	15(%rdi), %rax
	subq	%rsi, %r8
	subq	%rsi, %rax
	movq	%r8, %r10
	shrq	$3, %r10
	cmpq	$30, %rax
	jbe	.L1431
	movabsq	$2305843009213693948, %rax
	testq	%rax, %r10
	je	.L1431
	addq	$1, %r10
	xorl	%eax, %eax
	movq	%r10, %r11
	shrq	%r11
	salq	$4, %r11
	.p2align 4,,10
	.p2align 3
.L1357:
	movdqu	(%rsi,%rax), %xmm7
	movups	%xmm7, (%rdi,%rax)
	addq	$16, %rax
	cmpq	%r11, %rax
	jne	.L1357
	movq	%r10, %r11
	andq	$-2, %r11
	leaq	0(,%r11,8), %rax
	addq	%rax, %rsi
	addq	%rdi, %rax
	cmpq	%r11, %r10
	je	.L1359
	movq	(%rsi), %rsi
	movq	%rsi, (%rax)
.L1359:
	leaq	8(%rdi,%r8), %rsi
.L1355:
	subq	$8, %rdx
	leaq	15(%r15), %rax
	subq	%r15, %rdx
	subq	%rsi, %rax
	movq	%rdx, %r8
	shrq	$3, %r8
	cmpq	$30, %rax
	jbe	.L1432
	movabsq	$2305843009213693948, %rax
	testq	%rax, %r8
	je	.L1432
	addq	$1, %r8
	xorl	%eax, %eax
	movq	%r8, %r10
	shrq	%r10
	salq	$4, %r10
	.p2align 4,,10
	.p2align 3
.L1361:
	movdqu	(%r15,%rax), %xmm6
	movups	%xmm6, (%rsi,%rax)
	addq	$16, %rax
	cmpq	%rax, %r10
	jne	.L1361
	movq	%r8, %r10
	andq	$-2, %r10
	leaq	0(,%r10,8), %rax
	addq	%rax, %r15
	addq	%rsi, %rax
	cmpq	%r10, %r8
	je	.L1363
	movq	(%r15), %r8
	movq	%r8, (%rax)
.L1363:
	movq	24(%r12), %rax
	leaq	8(%rsi,%rdx), %r8
	cmpq	%rax, %rcx
	je	.L1364
	leaq	-8(%rax), %r10
	leaq	24(%rsi,%rdx), %rdx
	subq	%rcx, %r10
	movq	%r10, %rax
	shrq	$3, %rax
	cmpq	%rdx, %rcx
	leaq	16(%rcx), %rdx
	setnb	%sil
	cmpq	%rdx, %r8
	setnb	%dl
	orb	%dl, %sil
	je	.L1433
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rax
	je	.L1433
	leaq	1(%rax), %rsi
	xorl	%eax, %eax
	movq	%rsi, %rdx
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L1366:
	movdqu	(%rcx,%rax), %xmm7
	movups	%xmm7, (%r8,%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L1366
	movq	%rsi, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rdx
	addq	%rdx, %rcx
	addq	%r8, %rdx
	cmpq	%rax, %rsi
	je	.L1368
	movq	(%rcx), %rax
	movq	%rax, (%rdx)
.L1368:
	leaq	8(%r8,%r10), %r8
.L1364:
	movq	-80(%rbp), %rax
	movq	%rdi, %xmm0
	movq	%r8, %xmm7
	punpcklqdq	%xmm7, %xmm0
	movq	%rax, 32(%r12)
	movups	%xmm0, 16(%r12)
	jmp	.L1272
	.p2align 4,,10
	.p2align 3
.L1592:
	movq	8(%r9), %rax
	movq	%r12, %r15
	movabsq	$-1085102592571150095, %rdi
	subq	%rax, %r15
	movq	%rax, -80(%rbp)
	movq	%r15, %rax
	sarq	$3, %rax
	imulq	%rdi, %rax
	cmpq	$15790320, %rax
	je	.L1597
	testq	%rax, %rax
	je	.L1438
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L1598
	movl	$2147483520, %esi
	movl	$2147483520, %edx
.L1388:
	movq	(%r9), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	%rcx, %rsi
	ja	.L1599
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1391:
	movq	%rax, %r11
	leaq	(%rax,%rdx), %rax
	movq	%rax, -88(%rbp)
	leaq	136(%r11), %r8
	jmp	.L1389
.L1598:
	testq	%rdx, %rdx
	jne	.L1600
	movq	$0, -88(%rbp)
	movl	$136, %r8d
	xorl	%r11d, %r11d
.L1389:
	movl	-136(%r14), %eax
	addq	%r11, %r15
	movl	%eax, (%r15)
	movq	-128(%r14), %rdi
	movq	-112(%r14), %rdx
	subq	-120(%r14), %rdx
	movq	$0, 16(%r15)
	movq	%rdi, 8(%r15)
	movq	$0, 24(%r15)
	movq	$0, 32(%r15)
	je	.L1441
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	leaq	7(%rdx), %rsi
	andq	$-8, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L1601
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
.L1392:
	movq	%rcx, %xmm0
	addq	%rcx, %rdx
	punpcklqdq	%xmm0, %xmm0
	movq	%rdx, 32(%r15)
	movups	%xmm0, 16(%r15)
	movq	-112(%r14), %rax
	movq	-120(%r14), %rdx
	cmpq	%rdx, %rax
	je	.L1395
	leaq	-8(%rax), %rsi
	leaq	15(%rdx), %rax
	subq	%rdx, %rsi
	subq	%rcx, %rax
	movq	%rsi, %rdi
	shrq	$3, %rdi
	cmpq	$30, %rax
	jbe	.L1442
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rdi
	je	.L1442
	addq	$1, %rdi
	xorl	%eax, %eax
	movq	%rdi, %r10
	shrq	%r10
	salq	$4, %r10
	.p2align 4,,10
	.p2align 3
.L1397:
	movdqu	(%rdx,%rax), %xmm6
	movups	%xmm6, (%rcx,%rax)
	addq	$16, %rax
	cmpq	%rax, %r10
	jne	.L1397
	movq	%rdi, %r10
	andq	$-2, %r10
	leaq	0(,%r10,8), %rax
	addq	%rax, %rdx
	addq	%rcx, %rax
	cmpq	%rdi, %r10
	je	.L1399
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
.L1399:
	leaq	8(%rcx,%rsi), %rcx
.L1395:
	movq	%rcx, 24(%r15)
	movq	-96(%r14), %rdi
	movq	-80(%r14), %rdx
	subq	-88(%r14), %rdx
	movq	$0, 48(%r15)
	movq	%rdi, 40(%r15)
	movq	$0, 56(%r15)
	movq	$0, 64(%r15)
	je	.L1443
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	leaq	7(%rdx), %rsi
	andq	$-8, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L1602
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
.L1400:
	movq	%rcx, %xmm0
	addq	%rcx, %rdx
	punpcklqdq	%xmm0, %xmm0
	movq	%rdx, 64(%r15)
	movups	%xmm0, 48(%r15)
	movq	-80(%r14), %rax
	movq	-88(%r14), %rdx
	cmpq	%rdx, %rax
	je	.L1403
	leaq	-8(%rax), %rsi
	leaq	15(%rdx), %rax
	subq	%rdx, %rsi
	subq	%rcx, %rax
	movq	%rsi, %rdi
	shrq	$3, %rdi
	cmpq	$30, %rax
	jbe	.L1444
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rdi
	je	.L1444
	addq	$1, %rdi
	xorl	%eax, %eax
	movq	%rdi, %r10
	shrq	%r10
	salq	$4, %r10
	.p2align 4,,10
	.p2align 3
.L1405:
	movdqu	(%rdx,%rax), %xmm5
	movups	%xmm5, (%rcx,%rax)
	addq	$16, %rax
	cmpq	%r10, %rax
	jne	.L1405
	movq	%rdi, %r10
	andq	$-2, %r10
	leaq	0(,%r10,8), %rax
	addq	%rax, %rdx
	addq	%rcx, %rax
	cmpq	%r10, %rdi
	je	.L1407
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
.L1407:
	leaq	8(%rcx,%rsi), %rcx
.L1403:
	movq	%rcx, 56(%r15)
	movq	-64(%r14), %rax
	movq	%rax, 72(%r15)
	movq	-56(%r14), %rax
	movq	%rax, 80(%r15)
	movq	-48(%r14), %rax
	movq	%rax, 88(%r15)
	movq	-40(%r14), %rax
	movq	%rax, 96(%r15)
	movzbl	-32(%r14), %eax
	movb	%al, 104(%r15)
	movq	-24(%r14), %rax
	movq	%rax, 112(%r15)
	movq	-16(%r14), %rax
	movq	%rax, 120(%r15)
	movq	-8(%r14), %rax
	movq	%rax, 128(%r15)
	movq	-80(%rbp), %rax
	cmpq	%rax, %r12
	je	.L1408
	movq	%r11, %rdx
	pxor	%xmm0, %xmm0
	.p2align 4,,10
	.p2align 3
.L1409:
	movl	(%rax), %ecx
	addq	$136, %rax
	addq	$136, %rdx
	movl	%ecx, -136(%rdx)
	movq	-128(%rax), %rcx
	movq	%rcx, -128(%rdx)
	movdqu	-120(%rax), %xmm3
	movups	%xmm3, -120(%rdx)
	movq	-104(%rax), %rcx
	movq	%rcx, -104(%rdx)
	movq	-96(%rax), %rcx
	movq	$0, -104(%rax)
	movups	%xmm0, -120(%rax)
	movq	%rcx, -96(%rdx)
	movdqu	-88(%rax), %xmm4
	movups	%xmm4, -88(%rdx)
	movq	-72(%rax), %rcx
	movq	%rcx, -72(%rdx)
	movq	-64(%rax), %rcx
	movq	$0, -72(%rax)
	movups	%xmm0, -88(%rax)
	movq	%rcx, -64(%rdx)
	movq	-56(%rax), %rcx
	movq	%rcx, -56(%rdx)
	movq	-48(%rax), %rcx
	movq	%rcx, -48(%rdx)
	movq	-40(%rax), %rcx
	movq	%rcx, -40(%rdx)
	movzbl	-32(%rax), %ecx
	movb	%cl, -32(%rdx)
	movq	-24(%rax), %rcx
	movq	%rcx, -24(%rdx)
	movq	-16(%rax), %rcx
	movq	%rcx, -16(%rdx)
	movq	-8(%rax), %rcx
	movq	%rcx, -8(%rdx)
	cmpq	%rax, %r12
	jne	.L1409
	leaq	-136(%r12), %rax
	subq	-80(%rbp), %rax
	movabsq	$1220740416642543857, %rdi
	movabsq	$2305843009213693951, %rdx
	shrq	$3, %rax
	imulq	%rdi, %rax
	andq	%rdx, %rax
	addq	$2, %rax
	movq	%rax, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	leaq	(%r11,%rax,8), %r8
.L1408:
	movq	-88(%rbp), %rax
	movq	%r11, %xmm0
	movq	%r8, %xmm4
	punpcklqdq	%xmm4, %xmm0
	movq	%rax, 24(%r9)
	movups	%xmm0, 8(%r9)
	jmp	.L1386
.L1441:
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	jmp	.L1392
.L1283:
	movq	-8(%r14), %rax
	cmpq	%rax, 128(%r12)
	jne	.L1275
	movzbl	-32(%r14), %eax
	cmpb	%al, 104(%r12)
	jne	.L1275
	movq	-56(%r14), %rax
	cmpq	%rax, 80(%r12)
	jne	.L1275
	jmp	.L1285
	.p2align 4,,10
	.p2align 3
.L1443:
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	jmp	.L1400
.L1437:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L1382:
	movq	(%rcx,%rax,8), %rdi
	movq	%rdi, (%rdx,%rax,8)
	movq	%rax, %rdi
	addq	$1, %rax
	cmpq	%rdi, %r8
	jne	.L1382
	jmp	.L1385
.L1435:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L1374:
	movq	(%rcx,%rax,8), %rdi
	movq	%rdi, (%rdx,%rax,8)
	movq	%rax, %rdi
	addq	$1, %rax
	cmpq	%rdi, %r8
	jne	.L1374
	jmp	.L1377
.L1427:
	xorl	%eax, %eax
.L1345:
	movq	(%r15,%rax,8), %rsi
	movq	%rsi, (%rcx,%rax,8)
	movq	%rax, %rsi
	addq	$1, %rax
	cmpq	%rdx, %rsi
	jne	.L1345
	jmp	.L1348
.L1438:
	movl	$136, %esi
	movl	$136, %edx
	jmp	.L1388
.L1317:
	movq	16(%r12), %rsi
	movq	%rdx, %rdi
	movl	$268435455, %r10d
	movq	%r10, %r8
	subq	%rsi, %rdi
	sarq	$3, %rdi
	subq	%rdi, %r8
	cmpq	%r8, %rax
	ja	.L1322
	cmpq	%rdi, %rax
	cmovb	%rdi, %rax
	addq	%rax, %rdi
	jc	.L1324
	testq	%rdi, %rdi
	jne	.L1603
	xorl	%r11d, %r11d
	xorl	%eax, %eax
.L1326:
	cmpq	%rsi, %rdx
	je	.L1423
	leaq	-8(%rdx), %rdi
	subq	%rsi, %rdi
	movq	%rdi, -80(%rbp)
	shrq	$3, %rdi
	movq	%rdi, %r10
	leaq	15(%rax), %rdi
	subq	%rsi, %rdi
	cmpq	$30, %rdi
	jbe	.L1424
	movabsq	$2305843009213693948, %rdi
	testq	%rdi, %r10
	je	.L1424
	addq	$1, %r10
	xorl	%edi, %edi
	movq	%r10, %r8
	shrq	%r8
	salq	$4, %r8
.L1331:
	movdqu	(%rsi,%rdi), %xmm2
	movups	%xmm2, (%rax,%rdi)
	addq	$16, %rdi
	cmpq	%r8, %rdi
	jne	.L1331
	movq	%r10, %r8
	andq	$-2, %r8
	leaq	0(,%r8,8), %rdi
	addq	%rdi, %rsi
	addq	%rax, %rdi
	cmpq	%r8, %r10
	je	.L1333
	movq	(%rsi), %rsi
	movq	%rsi, (%rdi)
.L1333:
	movq	-80(%rbp), %rdi
	leaq	8(%rax,%rdi), %rsi
.L1329:
	subq	$8, %rcx
	subq	%r15, %rcx
	movq	%rcx, %rdi
	movq	%rcx, %r10
	leaq	15(%r15), %rcx
	subq	%rsi, %rcx
	shrq	$3, %rdi
	cmpq	$30, %rcx
	jbe	.L1425
	movabsq	$2305843009213693948, %rcx
	testq	%rcx, %rdi
	je	.L1425
	leaq	1(%rdi), %r8
	xorl	%ecx, %ecx
	movq	%r8, %rdi
	shrq	%rdi
	salq	$4, %rdi
.L1335:
	movdqu	(%r15,%rcx), %xmm1
	movups	%xmm1, (%rsi,%rcx)
	addq	$16, %rcx
	cmpq	%rdi, %rcx
	jne	.L1335
	movq	%r8, %rdi
	andq	$-2, %rdi
	leaq	0(,%rdi,8), %rcx
	addq	%rcx, %r15
	addq	%rsi, %rcx
	cmpq	%rdi, %r8
	je	.L1337
	movq	(%r15), %rdi
	movq	%rdi, (%rcx)
.L1337:
	movq	24(%r12), %rcx
	leaq	8(%rsi,%r10), %rdi
	cmpq	%rcx, %rdx
	je	.L1338
	subq	$8, %rcx
	leaq	24(%rsi,%r10), %rsi
	subq	%rdx, %rcx
	movq	%rcx, %r15
	shrq	$3, %rcx
	cmpq	%rsi, %rdx
	leaq	16(%rdx), %rsi
	setnb	%r8b
	cmpq	%rsi, %rdi
	setnb	%sil
	orb	%sil, %r8b
	je	.L1426
	movabsq	$2305843009213693948, %rsi
	testq	%rsi, %rcx
	je	.L1426
	leaq	1(%rcx), %r8
	xorl	%ecx, %ecx
	movq	%r8, %rsi
	shrq	%rsi
	salq	$4, %rsi
.L1340:
	movdqu	(%rdx,%rcx), %xmm4
	movups	%xmm4, (%rdi,%rcx)
	addq	$16, %rcx
	cmpq	%rcx, %rsi
	jne	.L1340
	movq	%r8, %rcx
	andq	$-2, %rcx
	leaq	0(,%rcx,8), %rsi
	addq	%rsi, %rdx
	addq	%rdi, %rsi
	cmpq	%rcx, %r8
	je	.L1342
	movq	(%rdx), %rdx
	movq	%rdx, (%rsi)
.L1342:
	leaq	8(%rdi,%r15), %rdi
.L1338:
	movq	%rax, %xmm0
	movq	%rdi, %xmm3
	movq	%r11, 32(%r12)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 16(%r12)
	jmp	.L1272
.L1432:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L1360:
	movq	(%r15,%rax,8), %r10
	movq	%r10, (%rsi,%rax,8)
	movq	%rax, %r10
	addq	$1, %rax
	cmpq	%r8, %r10
	jne	.L1360
	jmp	.L1363
.L1290:
	movq	16(%r12), %rax
	movq	%rcx, %rsi
	movl	$268435455, %r8d
	movq	%r8, %r10
	subq	%rax, %rsi
	sarq	$3, %rsi
	subq	%rsi, %r10
	cmpq	%r10, %rdi
	ja	.L1322
	cmpq	%rsi, %rdi
	cmovb	%rsi, %rdi
	addq	%rsi, %rdi
	jc	.L1297
	testq	%rdi, %rdi
	jne	.L1604
	xorl	%r11d, %r11d
	xorl	%esi, %esi
.L1299:
	cmpq	%rax, %rcx
	je	.L1416
	leaq	-8(%rcx), %rdi
	subq	%rax, %rdi
	movq	%rdi, -80(%rbp)
	shrq	$3, %rdi
	movq	%rdi, %r10
	leaq	15(%rsi), %rdi
	subq	%rax, %rdi
	cmpq	$30, %rdi
	jbe	.L1417
	movabsq	$2305843009213693948, %rdi
	testq	%rdi, %r10
	je	.L1417
	addq	$1, %r10
	xorl	%edi, %edi
	movq	%r10, %r8
	shrq	%r8
	salq	$4, %r8
.L1304:
	movdqu	(%rax,%rdi), %xmm6
	movups	%xmm6, (%rsi,%rdi)
	addq	$16, %rdi
	cmpq	%r8, %rdi
	jne	.L1304
	movq	%r10, %r8
	andq	$-2, %r8
	leaq	0(,%r8,8), %rdi
	addq	%rdi, %rax
	addq	%rsi, %rdi
	cmpq	%r8, %r10
	je	.L1306
	movq	(%rax), %rax
	movq	%rax, (%rdi)
.L1306:
	movq	-80(%rbp), %rax
	leaq	8(%rsi,%rax), %rax
.L1302:
	leaq	-8(%rdx), %r10
	leaq	15(%r15), %rdx
	subq	%r15, %r10
	subq	%rax, %rdx
	movq	%r10, %rdi
	shrq	$3, %rdi
	cmpq	$30, %rdx
	jbe	.L1418
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rdi
	je	.L1418
	leaq	1(%rdi), %r8
	xorl	%edx, %edx
	movq	%r8, %rdi
	shrq	%rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L1308:
	movdqu	(%r15,%rdx), %xmm4
	movups	%xmm4, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rdi
	jne	.L1308
	movq	%r8, %rdi
	andq	$-2, %rdi
	leaq	0(,%rdi,8), %rdx
	addq	%rdx, %r15
	addq	%rax, %rdx
	cmpq	%rdi, %r8
	je	.L1310
	movq	(%r15), %rdi
	movq	%rdi, (%rdx)
.L1310:
	movq	24(%r12), %rdi
	leaq	8(%rax,%r10), %rdx
	cmpq	%rdi, %rcx
	je	.L1311
	subq	$8, %rdi
	leaq	24(%rax,%r10), %rax
	subq	%rcx, %rdi
	movq	%rdi, %r15
	shrq	$3, %rdi
	cmpq	%rax, %rcx
	leaq	16(%rcx), %rax
	setnb	%r8b
	cmpq	%rax, %rdx
	setnb	%al
	orb	%al, %r8b
	je	.L1419
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rdi
	je	.L1419
	addq	$1, %rdi
	xorl	%eax, %eax
	movq	%rdi, %r8
	shrq	%r8
	salq	$4, %r8
.L1313:
	movdqu	(%rcx,%rax), %xmm3
	movups	%xmm3, (%rdx,%rax)
	addq	$16, %rax
	cmpq	%r8, %rax
	jne	.L1313
	movq	%rdi, %r8
	andq	$-2, %r8
	leaq	0(,%r8,8), %rax
	addq	%rax, %rcx
	addq	%rdx, %rax
	cmpq	%r8, %rdi
	je	.L1315
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
.L1315:
	leaq	8(%rdx,%r15), %rdx
.L1311:
	movq	%rsi, %xmm0
	movq	%rdx, %xmm6
	movq	%r11, 32(%r12)
	punpcklqdq	%xmm6, %xmm0
	movups	%xmm0, 16(%r12)
	jmp	.L1289
.L1444:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L1404:
	movq	(%rdx,%rax,8), %r10
	movq	%r10, (%rcx,%rax,8)
	movq	%rax, %r10
	addq	$1, %rax
	cmpq	%r10, %rdi
	jne	.L1404
	jmp	.L1407
.L1433:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L1365:
	movq	(%rcx,%rdx,8), %rsi
	movq	%rsi, (%r8,%rdx,8)
	movq	%rdx, %rsi
	addq	$1, %rdx
	cmpq	%rax, %rsi
	jne	.L1365
	jmp	.L1368
.L1442:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L1396:
	movq	(%rdx,%rax,8), %r10
	movq	%r10, (%rcx,%rax,8)
	movq	%rax, %r10
	addq	$1, %rax
	cmpq	%r10, %rdi
	jne	.L1396
	jmp	.L1399
.L1431:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L1356:
	movq	(%rsi,%rax,8), %r11
	movq	%r11, (%rdi,%rax,8)
	movq	%rax, %r11
	addq	$1, %rax
	cmpq	%r10, %r11
	jne	.L1356
	jmp	.L1359
.L1593:
	movq	%r9, -80(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-80(%rbp), %r9
	movq	%rax, %rdx
	jmp	.L1370
.L1594:
	movq	%r9, -80(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-80(%rbp), %r9
	movq	%rax, %rdx
	jmp	.L1378
.L1595:
	leaq	.LC18(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1430:
	movq	%rdi, %rsi
	jmp	.L1355
.L1413:
	xorl	%edx, %edx
.L1291:
	movq	(%r15,%rdx,8), %rdi
	movq	%rdi, (%rcx,%rdx,8)
	movq	%rdx, %rdi
	addq	$1, %rdx
	cmpq	%rax, %rdi
	jne	.L1291
	jmp	.L1294
.L1601:
	movq	%r8, -120(%rbp)
	movq	%r9, -112(%rbp)
	movq	%rdx, -104(%rbp)
	movq	%r11, -96(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-96(%rbp), %r11
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %r9
	movq	-120(%rbp), %r8
	movq	%rax, %rcx
	jmp	.L1392
.L1599:
	movq	%r9, -96(%rbp)
	movq	%rdx, -88(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %r9
	jmp	.L1391
.L1602:
	movq	%r8, -120(%rbp)
	movq	%r9, -112(%rbp)
	movq	%r11, -104(%rbp)
	movq	%rdx, -96(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-96(%rbp), %rdx
	movq	-104(%rbp), %r11
	movq	-112(%rbp), %r9
	movq	-120(%rbp), %r8
	movq	%rax, %rcx
	jmp	.L1400
.L1420:
	xorl	%ecx, %ecx
.L1318:
	movq	(%r15,%rcx,8), %rsi
	movq	%rsi, (%rdx,%rcx,8)
	movq	%rcx, %rsi
	addq	$1, %rcx
	cmpq	%rax, %rsi
	jne	.L1318
	jmp	.L1348
.L1419:
	xorl	%eax, %eax
.L1312:
	movq	(%rcx,%rax,8), %r8
	movq	%r8, (%rdx,%rax,8)
	movq	%rax, %r8
	addq	$1, %rax
	cmpq	%rdi, %r8
	jne	.L1312
	jmp	.L1315
.L1418:
	xorl	%edx, %edx
.L1307:
	movq	(%r15,%rdx,8), %r8
	movq	%r8, (%rax,%rdx,8)
	movq	%rdx, %r8
	addq	$1, %rdx
	cmpq	%rdi, %r8
	jne	.L1307
	jmp	.L1310
.L1417:
	xorl	%edi, %edi
.L1303:
	movq	(%rax,%rdi,8), %r8
	movq	%r8, (%rsi,%rdi,8)
	movq	%rdi, %r8
	addq	$1, %rdi
	cmpq	%r10, %r8
	jne	.L1303
	jmp	.L1306
.L1425:
	xorl	%ecx, %ecx
.L1334:
	movq	(%r15,%rcx,8), %r8
	movq	%r8, (%rsi,%rcx,8)
	movq	%rcx, %r8
	addq	$1, %rcx
	cmpq	%rdi, %r8
	jne	.L1334
	jmp	.L1337
.L1426:
	xorl	%esi, %esi
.L1339:
	movq	(%rdx,%rsi,8), %r8
	movq	%r8, (%rdi,%rsi,8)
	movq	%rsi, %r8
	addq	$1, %rsi
	cmpq	%rcx, %r8
	jne	.L1339
	jmp	.L1342
.L1416:
	movq	%rsi, %rax
	jmp	.L1302
.L1424:
	xorl	%edi, %edi
.L1330:
	movq	(%rsi,%rdi,8), %r8
	movq	%r8, (%rax,%rdi,8)
	movq	%rdi, %r8
	addq	$1, %rdi
	cmpq	%r10, %r8
	jne	.L1330
	jmp	.L1333
.L1423:
	movq	%rax, %rsi
	jmp	.L1329
.L1596:
	cmpq	$268435455, %rax
	cmova	%r8, %rax
	leaq	0(,%rax,8), %r8
	movq	%r8, %rsi
.L1351:
	movq	8(%r12), %r10
	movq	16(%r10), %rdi
	movq	24(%r10), %rax
	subq	%rdi, %rax
	cmpq	%rsi, %rax
	jb	.L1605
	addq	%rdi, %rsi
	movq	%rsi, 16(%r10)
.L1354:
	leaq	(%rdi,%r8), %rax
	movq	16(%r12), %rsi
	movq	%rax, -80(%rbp)
	jmp	.L1352
.L1605:
	movq	%r10, %rdi
	movq	%r9, -104(%rbp)
	movq	%rcx, -96(%rbp)
	movq	%rdx, -88(%rbp)
	movq	%r8, -80(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-80(%rbp), %r8
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rcx
	movq	-104(%rbp), %r9
	movq	%rax, %rdi
	jmp	.L1354
.L1350:
	movl	$2147483640, %esi
	movl	$2147483640, %r8d
	jmp	.L1351
.L1322:
	leaq	.LC17(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1600:
	cmpq	$15790320, %rdx
	movl	$15790320, %eax
	cmova	%rax, %rdx
	imulq	$136, %rdx, %rdx
	movq	%rdx, %rsi
	jmp	.L1388
.L1597:
	leaq	.LC9(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1604:
	cmpq	$268435455, %rdi
	cmova	%r8, %rdi
	leaq	0(,%rdi,8), %r11
	movq	%r11, %r8
.L1298:
	movq	8(%r12), %rdi
	movq	16(%rdi), %rsi
	movq	24(%rdi), %rax
	subq	%rsi, %rax
	cmpq	%r8, %rax
	jb	.L1606
	addq	%rsi, %r8
	movq	%r8, 16(%rdi)
.L1301:
	movq	16(%r12), %rax
	addq	%rsi, %r11
	jmp	.L1299
.L1606:
	movq	%r8, %rsi
	movq	%r9, -104(%rbp)
	movq	%r11, -96(%rbp)
	movq	%rcx, -88(%rbp)
	movq	%rdx, -80(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-80(%rbp), %rdx
	movq	-88(%rbp), %rcx
	movq	-96(%rbp), %r11
	movq	-104(%rbp), %r9
	movq	%rax, %rsi
	jmp	.L1301
.L1297:
	movl	$2147483640, %r8d
	movl	$2147483640, %r11d
	jmp	.L1298
	.p2align 4,,10
	.p2align 3
.L1603:
	cmpq	$268435455, %rdi
	cmova	%r10, %rdi
	leaq	0(,%rdi,8), %r11
	movq	%r11, %rsi
.L1325:
	movq	8(%r12), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %r8
	subq	%rax, %r8
	cmpq	%r8, %rsi
	ja	.L1607
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1328:
	movq	16(%r12), %rsi
	addq	%rax, %r11
	jmp	.L1326
.L1607:
	movq	%r9, -104(%rbp)
	movq	%r11, -96(%rbp)
	movq	%rdx, -88(%rbp)
	movq	%rcx, -80(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-80(%rbp), %rcx
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %r11
	movq	-104(%rbp), %r9
	jmp	.L1328
.L1324:
	movl	$2147483640, %esi
	movl	$2147483640, %r11d
	jmp	.L1325
	.cfi_endproc
.LFE19370:
	.size	_ZNK2v88internal8compiler17AccessInfoFactory24MergePropertyAccessInfosENS0_10ZoneVectorINS1_18PropertyAccessInfoEEENS1_10AccessModeEPS5_, .-_ZNK2v88internal8compiler17AccessInfoFactory24MergePropertyAccessInfosENS0_10ZoneVectorINS1_18PropertyAccessInfoEEENS1_10AccessModeEPS5_
	.section	.text._ZNK2v88internal8compiler17AccessInfoFactory32FinalizePropertyAccessInfosAsOneENS0_10ZoneVectorINS1_18PropertyAccessInfoEEENS1_10AccessModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler17AccessInfoFactory32FinalizePropertyAccessInfosAsOneENS0_10ZoneVectorINS1_18PropertyAccessInfoEEENS1_10AccessModeE
	.type	_ZNK2v88internal8compiler17AccessInfoFactory32FinalizePropertyAccessInfosAsOneENS0_10ZoneVectorINS1_18PropertyAccessInfoEEENS1_10AccessModeE, @function
_ZNK2v88internal8compiler17AccessInfoFactory32FinalizePropertyAccessInfosAsOneENS0_10ZoneVectorINS1_18PropertyAccessInfoEEENS1_10AccessModeE:
.LFB19354:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	movl	%ecx, %edx
	subq	$120, %rsp
	movq	16(%rbx), %r14
	movq	(%rbx), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rbx), %r9
	movq	24(%rsi), %rax
	movq	$0, -120(%rbp)
	movq	%r14, %r12
	movq	$0, -112(%rbp)
	movq	%rax, -128(%rbp)
	movq	$0, -104(%rbp)
	movq	%rdi, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	subq	%r9, %r12
	je	.L1706
	movq	16(%rdi), %r15
	movq	24(%rdi), %rax
	leaq	7(%r12), %rsi
	andq	$-8, %rsi
	subq	%r15, %rax
	cmpq	%rax, %rsi
	ja	.L1708
	addq	%r15, %rsi
	movq	%rsi, 16(%rdi)
.L1611:
	movq	16(%rbx), %r14
	movq	8(%rbx), %r9
.L1609:
	movq	%r15, %xmm0
	addq	%r15, %r12
	punpcklqdq	%xmm0, %xmm0
	movq	%r12, -72(%rbp)
	movups	%xmm0, -88(%rbp)
	cmpq	%r9, %r14
	je	.L1612
	movq	%r9, %r12
	movq	%r15, %rbx
	movq	%r15, %rcx
	movq	%r9, %r15
	movq	%r13, %r9
	.p2align 4,,10
	.p2align 3
.L1631:
	movl	(%r12), %eax
	movl	%eax, (%rbx)
	movq	8(%r12), %rdi
	movq	24(%r12), %r13
	subq	16(%r12), %r13
	movq	$0, 16(%rbx)
	movq	%rdi, 8(%rbx)
	movq	$0, 24(%rbx)
	movq	$0, 32(%rbx)
	je	.L1656
	movq	16(%rdi), %rax
	movq	24(%rdi), %r10
	leaq	7(%r13), %rsi
	andq	$-8, %rsi
	subq	%rax, %r10
	cmpq	%r10, %rsi
	ja	.L1709
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1613:
	movq	%rax, %xmm0
	addq	%rax, %r13
	punpcklqdq	%xmm0, %xmm0
	movq	%r13, 32(%rbx)
	movups	%xmm0, 16(%rbx)
	movq	24(%r12), %rdi
	movq	16(%r12), %rsi
	cmpq	%rsi, %rdi
	je	.L1616
	subq	$8, %rdi
	leaq	15(%rsi), %r10
	subq	%rsi, %rdi
	subq	%rax, %r10
	movq	%rdi, %r11
	shrq	$3, %r11
	cmpq	$30, %r10
	jbe	.L1657
	movabsq	$2305843009213693948, %r10
	testq	%r10, %r11
	je	.L1657
	addq	$1, %r11
	xorl	%r10d, %r10d
	movq	%r11, %r13
	shrq	%r13
	salq	$4, %r13
	.p2align 4,,10
	.p2align 3
.L1618:
	movdqu	(%rsi,%r10), %xmm1
	movups	%xmm1, (%rax,%r10)
	addq	$16, %r10
	cmpq	%r13, %r10
	jne	.L1618
	movq	%r11, %r13
	andq	$-2, %r13
	leaq	0(,%r13,8), %r10
	addq	%r10, %rsi
	addq	%rax, %r10
	cmpq	%r13, %r11
	je	.L1621
	movq	(%rsi), %rsi
	movq	%rsi, (%r10)
.L1621:
	leaq	8(%rax,%rdi), %rax
.L1616:
	movq	%rax, 24(%rbx)
	movq	40(%r12), %rdi
	movq	56(%r12), %r13
	subq	48(%r12), %r13
	movq	$0, 48(%rbx)
	movq	%rdi, 40(%rbx)
	movq	$0, 56(%rbx)
	movq	$0, 64(%rbx)
	je	.L1658
	movq	16(%rdi), %rax
	movq	24(%rdi), %r10
	leaq	7(%r13), %rsi
	andq	$-8, %rsi
	subq	%rax, %r10
	cmpq	%r10, %rsi
	ja	.L1710
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1622:
	movq	%rax, %xmm0
	addq	%rax, %r13
	punpcklqdq	%xmm0, %xmm0
	movq	%r13, 64(%rbx)
	movups	%xmm0, 48(%rbx)
	movq	56(%r12), %rdi
	movq	48(%r12), %rsi
	cmpq	%rsi, %rdi
	je	.L1625
	subq	$8, %rdi
	leaq	15(%rsi), %r10
	subq	%rsi, %rdi
	subq	%rax, %r10
	movq	%rdi, %r11
	shrq	$3, %r11
	cmpq	$30, %r10
	jbe	.L1659
	movabsq	$2305843009213693948, %r10
	testq	%r10, %r11
	je	.L1659
	addq	$1, %r11
	xorl	%r10d, %r10d
	movq	%r11, %r13
	shrq	%r13
	salq	$4, %r13
	.p2align 4,,10
	.p2align 3
.L1627:
	movdqu	(%rsi,%r10), %xmm2
	movups	%xmm2, (%rax,%r10)
	addq	$16, %r10
	cmpq	%r13, %r10
	jne	.L1627
	movq	%r11, %r13
	andq	$-2, %r13
	leaq	0(,%r13,8), %r10
	addq	%r10, %rsi
	addq	%rax, %r10
	cmpq	%r13, %r11
	je	.L1630
	movq	(%rsi), %rsi
	movq	%rsi, (%r10)
.L1630:
	leaq	8(%rax,%rdi), %rax
.L1625:
	movq	%rax, 56(%rbx)
	movq	72(%r12), %rax
	addq	$136, %r12
	addq	$136, %rbx
	movq	%rax, -64(%rbx)
	movq	-56(%r12), %rax
	movq	%rax, -56(%rbx)
	movq	-48(%r12), %rax
	movq	%rax, -48(%rbx)
	movq	-40(%r12), %rax
	movq	%rax, -40(%rbx)
	movzbl	-32(%r12), %eax
	movb	%al, -32(%rbx)
	movq	-24(%r12), %rax
	movq	%rax, -24(%rbx)
	movq	-16(%r12), %rax
	movq	%rax, -16(%rbx)
	movq	-8(%r12), %rax
	movq	%rax, -8(%rbx)
	cmpq	%r12, %r14
	jne	.L1631
	leaq	-136(%r14), %rax
	movq	%r9, %r13
	movq	%r15, %r9
	movq	%rcx, %r15
	movabsq	$1220740416642543857, %r14
	subq	%r9, %rax
	shrq	$3, %rax
	imulq	%r14, %rax
	movabsq	$2305843009213693951, %r14
	andq	%r14, %rax
	addq	$1, %rax
	movq	%rax, %rcx
	salq	$4, %rcx
	addq	%rcx, %rax
	leaq	(%r15,%rax,8), %r15
.L1612:
	movq	%r8, %rdi
	leaq	-128(%rbp), %rcx
	leaq	-96(%rbp), %rsi
	movq	%r15, -80(%rbp)
	movq	%r8, -136(%rbp)
	call	_ZNK2v88internal8compiler17AccessInfoFactory24MergePropertyAccessInfosENS0_10ZoneVectorINS1_18PropertyAccessInfoEEENS1_10AccessModeEPS5_
	movq	-120(%rbp), %rbx
	movq	-112(%rbp), %rax
	movq	-136(%rbp), %r8
	subq	%rbx, %rax
	cmpq	$136, %rax
	je	.L1711
.L1632:
	movq	24(%r8), %rax
	movl	$0, 0(%r13)
	pxor	%xmm0, %xmm0
	movq	$0, 16(%r13)
	movq	%rax, 8(%r13)
	movq	$0, 24(%r13)
	movq	$0, 32(%r13)
	movq	%rax, 40(%r13)
	movq	$0, 48(%r13)
	movq	$0, 56(%r13)
	movq	$0, 64(%r13)
	movq	$0, 72(%r13)
	movq	$0, 80(%r13)
	movq	$0, 88(%r13)
	movq	$0, 96(%r13)
	movb	$0, 104(%r13)
	movq	$1, 112(%r13)
	movups	%xmm0, 120(%r13)
.L1608:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1712
	addq	$120, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1659:
	.cfi_restore_state
	xorl	%r10d, %r10d
	.p2align 4,,10
	.p2align 3
.L1626:
	movq	(%rsi,%r10,8), %r13
	movq	%r13, (%rax,%r10,8)
	movq	%r10, %r13
	addq	$1, %r10
	cmpq	%r11, %r13
	jne	.L1626
	jmp	.L1630
	.p2align 4,,10
	.p2align 3
.L1656:
	xorl	%r13d, %r13d
	xorl	%eax, %eax
	jmp	.L1613
	.p2align 4,,10
	.p2align 3
.L1658:
	xorl	%r13d, %r13d
	xorl	%eax, %eax
	jmp	.L1622
	.p2align 4,,10
	.p2align 3
.L1657:
	xorl	%r10d, %r10d
	.p2align 4,,10
	.p2align 3
.L1617:
	movq	(%rsi,%r10,8), %r13
	movq	%r13, (%rax,%r10,8)
	movq	%r10, %r13
	addq	$1, %r10
	cmpq	%r13, %r11
	jne	.L1617
	jmp	.L1621
	.p2align 4,,10
	.p2align 3
.L1706:
	xorl	%r15d, %r15d
	jmp	.L1609
	.p2align 4,,10
	.p2align 3
.L1710:
	movl	%edx, -156(%rbp)
	movq	%r8, -152(%rbp)
	movq	%r9, -144(%rbp)
	movq	%rcx, -136(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-136(%rbp), %rcx
	movq	-144(%rbp), %r9
	movq	-152(%rbp), %r8
	movl	-156(%rbp), %edx
	jmp	.L1622
	.p2align 4,,10
	.p2align 3
.L1709:
	movl	%edx, -156(%rbp)
	movq	%r8, -152(%rbp)
	movq	%r9, -144(%rbp)
	movq	%rcx, -136(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-136(%rbp), %rcx
	movq	-144(%rbp), %r9
	movq	-152(%rbp), %r8
	movl	-156(%rbp), %edx
	jmp	.L1613
	.p2align 4,,10
	.p2align 3
.L1711:
	movl	(%rbx), %eax
	testl	%eax, %eax
	je	.L1632
	movq	48(%rbx), %r12
	movq	56(%rbx), %r14
	movq	8(%r8), %r15
	cmpq	%r14, %r12
	je	.L1633
	.p2align 4,,10
	.p2align 3
.L1634:
	movq	(%r12), %rsi
	movq	%r15, %rdi
	addq	$8, %r12
	call	_ZN2v88internal8compiler23CompilationDependencies16RecordDependencyEPKNS1_21CompilationDependencyE@PLT
	cmpq	%r12, %r14
	jne	.L1634
	movl	(%rbx), %eax
	movq	48(%rbx), %rdx
	cmpq	%rdx, 56(%rbx)
	je	.L1633
	movq	%rdx, 56(%rbx)
.L1633:
	movl	%eax, 0(%r13)
	movq	8(%rbx), %rdi
	movq	24(%rbx), %rax
	subq	16(%rbx), %rax
	movq	$0, 16(%r13)
	movq	%rdi, 8(%r13)
	movq	$0, 24(%r13)
	movq	$0, 32(%r13)
	je	.L1660
	leaq	7(%rax), %rsi
	movq	24(%rdi), %rdx
	movq	%rax, %r12
	movq	16(%rdi), %rax
	andq	$-8, %rsi
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L1713
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1635:
	movq	%rax, %xmm0
	addq	%rax, %r12
	punpcklqdq	%xmm0, %xmm0
	movq	%r12, 32(%r13)
	movups	%xmm0, 16(%r13)
	movq	24(%rbx), %r8
	movq	16(%rbx), %rcx
	cmpq	%rcx, %r8
	je	.L1638
	subq	$8, %r8
	leaq	15(%rcx), %rdx
	subq	%rcx, %r8
	subq	%rax, %rdx
	movq	%r8, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rdx
	jbe	.L1661
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rsi
	je	.L1661
	leaq	1(%rsi), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L1640:
	movdqu	(%rcx,%rdx), %xmm3
	movups	%xmm3, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rsi, %rdx
	jne	.L1640
	movq	%rdi, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rsi
	addq	%rsi, %rcx
	addq	%rax, %rsi
	cmpq	%rdx, %rdi
	je	.L1643
	movq	(%rcx), %rdx
	movq	%rdx, (%rsi)
.L1643:
	leaq	8(%rax,%r8), %rax
.L1638:
	movq	%rax, 24(%r13)
	movq	40(%rbx), %rdi
	movq	56(%rbx), %rax
	subq	48(%rbx), %rax
	movq	$0, 48(%r13)
	movq	%rdi, 40(%r13)
	movq	$0, 56(%r13)
	movq	$0, 64(%r13)
	je	.L1662
	leaq	7(%rax), %rsi
	movq	24(%rdi), %rdx
	movq	%rax, %r12
	movq	16(%rdi), %rax
	andq	$-8, %rsi
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L1714
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1644:
	movq	%rax, %xmm0
	addq	%rax, %r12
	punpcklqdq	%xmm0, %xmm0
	movq	%r12, 64(%r13)
	movups	%xmm0, 48(%r13)
	movq	56(%rbx), %rsi
	movq	48(%rbx), %rcx
	cmpq	%rcx, %rsi
	je	.L1647
	subq	$8, %rsi
	leaq	15(%rcx), %rdx
	subq	%rcx, %rsi
	subq	%rax, %rdx
	movq	%rsi, %rdi
	shrq	$3, %rdi
	cmpq	$30, %rdx
	jbe	.L1663
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rdi
	je	.L1663
	leaq	1(%rdi), %r8
	xorl	%edx, %edx
	movq	%r8, %rdi
	shrq	%rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L1649:
	movdqu	(%rcx,%rdx), %xmm4
	movups	%xmm4, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rdi
	jne	.L1649
	movq	%r8, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rdi
	addq	%rdi, %rcx
	addq	%rax, %rdi
	cmpq	%rdx, %r8
	je	.L1652
	movq	(%rcx), %rdx
	movq	%rdx, (%rdi)
.L1652:
	leaq	8(%rax,%rsi), %rax
.L1647:
	movq	%rax, 56(%r13)
	movq	72(%rbx), %rax
	movq	%rax, 72(%r13)
	movq	80(%rbx), %rax
	movq	%rax, 80(%r13)
	movq	88(%rbx), %rax
	movq	%rax, 88(%r13)
	movq	96(%rbx), %rax
	movq	%rax, 96(%r13)
	movzbl	104(%rbx), %eax
	movb	%al, 104(%r13)
	movq	112(%rbx), %rax
	movq	%rax, 112(%r13)
	movq	120(%rbx), %rax
	movq	%rax, 120(%r13)
	movq	128(%rbx), %rax
	movq	%rax, 128(%r13)
	jmp	.L1608
	.p2align 4,,10
	.p2align 3
.L1708:
	movq	%r8, -136(%rbp)
	movl	%ecx, -144(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-136(%rbp), %r8
	movl	-144(%rbp), %edx
	movq	%rax, %r15
	jmp	.L1611
	.p2align 4,,10
	.p2align 3
.L1662:
	xorl	%r12d, %r12d
	xorl	%eax, %eax
	jmp	.L1644
	.p2align 4,,10
	.p2align 3
.L1660:
	xorl	%r12d, %r12d
	xorl	%eax, %eax
	jmp	.L1635
	.p2align 4,,10
	.p2align 3
.L1661:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L1639:
	movq	(%rcx,%rdx,8), %rdi
	movq	%rdi, (%rax,%rdx,8)
	movq	%rdx, %rdi
	addq	$1, %rdx
	cmpq	%rsi, %rdi
	jne	.L1639
	jmp	.L1643
	.p2align 4,,10
	.p2align 3
.L1663:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L1648:
	movq	(%rcx,%rdx,8), %r8
	movq	%r8, (%rax,%rdx,8)
	movq	%rdx, %r8
	addq	$1, %rdx
	cmpq	%rdi, %r8
	jne	.L1648
	jmp	.L1652
.L1714:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1644
.L1713:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1635
.L1712:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19354:
	.size	_ZNK2v88internal8compiler17AccessInfoFactory32FinalizePropertyAccessInfosAsOneENS0_10ZoneVectorINS1_18PropertyAccessInfoEEENS1_10AccessModeE, .-_ZNK2v88internal8compiler17AccessInfoFactory32FinalizePropertyAccessInfosAsOneENS0_10ZoneVectorINS1_18PropertyAccessInfoEEENS1_10AccessModeE
	.section	.text._ZNK2v88internal8compiler17AccessInfoFactory27FinalizePropertyAccessInfosENS0_10ZoneVectorINS1_18PropertyAccessInfoEEENS1_10AccessModeEPS5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler17AccessInfoFactory27FinalizePropertyAccessInfosENS0_10ZoneVectorINS1_18PropertyAccessInfoEEENS1_10AccessModeEPS5_
	.type	_ZNK2v88internal8compiler17AccessInfoFactory27FinalizePropertyAccessInfosENS0_10ZoneVectorINS1_18PropertyAccessInfoEEENS1_10AccessModeEPS5_, @function
_ZNK2v88internal8compiler17AccessInfoFactory27FinalizePropertyAccessInfosENS0_10ZoneVectorINS1_18PropertyAccessInfoEEENS1_10AccessModeEPS5_:
.LFB19369:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, -108(%rbp)
	movq	8(%rsi), %rdx
	movq	%rdi, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rsi), %rax
	cmpq	%rax, %rdx
	je	.L1741
	movq	(%rsi), %rdi
	subq	%rdx, %rax
	movq	%rsi, %rbx
	movq	$0, -88(%rbp)
	leaq	7(%rax), %rsi
	movq	$0, -80(%rbp)
	movq	%rax, %r12
	movq	16(%rdi), %r8
	movq	24(%rdi), %rdx
	andq	$-8, %rsi
	movq	%rdi, -96(%rbp)
	movq	$0, -72(%rbp)
	subq	%r8, %rdx
	cmpq	%rdx, %rsi
	ja	.L1777
	addq	%r8, %rsi
	movq	%rsi, 16(%rdi)
.L1719:
	movq	16(%rbx), %r14
	movq	8(%rbx), %r9
	leaq	(%r8,%r12), %rax
	movq	%r8, -88(%rbp)
	movq	%r8, -80(%rbp)
	movq	%r8, %rdx
	movq	%rax, -72(%rbp)
	cmpq	%r9, %r14
	je	.L1720
	movabsq	$2305843009213693948, %r15
	movq	%r8, %rbx
	movq	%r9, %r12
	.p2align 4,,10
	.p2align 3
.L1739:
	movl	(%r12), %eax
	movl	%eax, (%rbx)
	movq	8(%r12), %rdi
	movq	24(%r12), %r13
	subq	16(%r12), %r13
	movq	$0, 16(%rbx)
	movq	%rdi, 8(%rbx)
	movq	$0, 24(%rbx)
	movq	$0, 32(%rbx)
	je	.L1748
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	leaq	7(%r13), %rsi
	andq	$-8, %rsi
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L1778
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1721:
	movq	%rax, %xmm0
	addq	%rax, %r13
	punpcklqdq	%xmm0, %xmm0
	movq	%r13, 32(%rbx)
	movups	%xmm0, 16(%rbx)
	movq	24(%r12), %rdx
	movq	16(%r12), %rdi
	cmpq	%rdi, %rdx
	je	.L1724
	leaq	-8(%rdx), %rsi
	leaq	15(%rdi), %rdx
	subq	%rdi, %rsi
	subq	%rax, %rdx
	movq	%rsi, %r10
	shrq	$3, %r10
	cmpq	$30, %rdx
	jbe	.L1749
	testq	%r15, %r10
	je	.L1749
	addq	$1, %r10
	xorl	%edx, %edx
	movq	%r10, %r11
	shrq	%r11
	salq	$4, %r11
	.p2align 4,,10
	.p2align 3
.L1726:
	movdqu	(%rdi,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %r11
	jne	.L1726
	movq	%r10, %r11
	andq	$-2, %r11
	leaq	0(,%r11,8), %rdx
	addq	%rdx, %rdi
	addq	%rax, %rdx
	cmpq	%r11, %r10
	je	.L1729
	movq	(%rdi), %rdi
	movq	%rdi, (%rdx)
.L1729:
	leaq	8(%rax,%rsi), %rax
.L1724:
	movq	%rax, 24(%rbx)
	movq	40(%r12), %rdi
	movq	56(%r12), %r13
	subq	48(%r12), %r13
	movq	$0, 48(%rbx)
	movq	%rdi, 40(%rbx)
	movq	$0, 56(%rbx)
	movq	$0, 64(%rbx)
	je	.L1750
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	leaq	7(%r13), %rsi
	andq	$-8, %rsi
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L1779
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1730:
	movq	%rax, %xmm0
	addq	%rax, %r13
	punpcklqdq	%xmm0, %xmm0
	movq	%r13, 64(%rbx)
	movups	%xmm0, 48(%rbx)
	movq	56(%r12), %rdx
	movq	48(%r12), %rdi
	cmpq	%rdi, %rdx
	je	.L1733
	leaq	-8(%rdx), %rsi
	leaq	15(%rdi), %rdx
	subq	%rdi, %rsi
	subq	%rax, %rdx
	movq	%rsi, %r10
	shrq	$3, %r10
	cmpq	$30, %rdx
	jbe	.L1751
	testq	%r15, %r10
	je	.L1751
	addq	$1, %r10
	xorl	%edx, %edx
	movq	%r10, %r11
	shrq	%r11
	salq	$4, %r11
	.p2align 4,,10
	.p2align 3
.L1735:
	movdqu	(%rdi,%rdx), %xmm2
	movups	%xmm2, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%r11, %rdx
	jne	.L1735
	movq	%r10, %r11
	andq	$-2, %r11
	leaq	0(,%r11,8), %rdx
	addq	%rdx, %rdi
	addq	%rax, %rdx
	cmpq	%r11, %r10
	je	.L1738
	movq	(%rdi), %rdi
	movq	%rdi, (%rdx)
.L1738:
	leaq	8(%rax,%rsi), %rax
.L1733:
	movq	%rax, 56(%rbx)
	movq	72(%r12), %rax
	addq	$136, %r12
	addq	$136, %rbx
	movq	%rax, -64(%rbx)
	movq	-56(%r12), %rax
	movq	%rax, -56(%rbx)
	movq	-48(%r12), %rax
	movq	%rax, -48(%rbx)
	movq	-40(%r12), %rax
	movq	%rax, -40(%rbx)
	movzbl	-32(%r12), %eax
	movb	%al, -32(%rbx)
	movq	-24(%r12), %rax
	movq	%rax, -24(%rbx)
	movq	-16(%r12), %rax
	movq	%rax, -16(%rbx)
	movq	-8(%r12), %rax
	movq	%rax, -8(%rbx)
	cmpq	%r12, %r14
	jne	.L1739
	leaq	-136(%r14), %rax
	movabsq	$1220740416642543857, %r14
	movabsq	$2305843009213693951, %rdx
	subq	%r9, %rax
	shrq	$3, %rax
	imulq	%r14, %rax
	andq	%rdx, %rax
	addq	$1, %rax
	movq	%rax, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	leaq	(%r8,%rax,8), %rdx
.L1720:
	movq	%rdx, -80(%rbp)
	movq	-104(%rbp), %rdi
	leaq	-96(%rbp), %rsi
	movl	-108(%rbp), %edx
	movq	%rcx, -120(%rbp)
	call	_ZNK2v88internal8compiler17AccessInfoFactory24MergePropertyAccessInfosENS0_10ZoneVectorINS1_18PropertyAccessInfoEEENS1_10AccessModeEPS5_
	movq	-120(%rbp), %rcx
	movq	8(%rcx), %r15
	movq	16(%rcx), %r12
	movq	%r15, %rax
	cmpq	%r12, %r15
	jne	.L1742
	jmp	.L1746
	.p2align 4,,10
	.p2align 3
.L1780:
	addq	$136, %rax
	cmpq	%rax, %r12
	je	.L1745
.L1742:
	movl	(%rax), %edx
	testl	%edx, %edx
	jne	.L1780
.L1741:
	xorl	%eax, %eax
.L1715:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1781
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1782:
	.cfi_restore_state
	movq	48(%r15), %rax
	cmpq	%rax, 56(%r15)
	je	.L1743
	movq	%rax, 56(%r15)
.L1743:
	addq	$136, %r15
	cmpq	%r15, %r12
	je	.L1746
.L1745:
	movq	-104(%rbp), %rax
	movq	48(%r15), %rbx
	movq	56(%r15), %r13
	movq	8(%rax), %r14
	cmpq	%r13, %rbx
	je	.L1743
	.p2align 4,,10
	.p2align 3
.L1744:
	movq	(%rbx), %rsi
	movq	%r14, %rdi
	addq	$8, %rbx
	call	_ZN2v88internal8compiler23CompilationDependencies16RecordDependencyEPKNS1_21CompilationDependencyE@PLT
	cmpq	%rbx, %r13
	jne	.L1744
	jmp	.L1782
	.p2align 4,,10
	.p2align 3
.L1748:
	xorl	%r13d, %r13d
	xorl	%eax, %eax
	jmp	.L1721
	.p2align 4,,10
	.p2align 3
.L1750:
	xorl	%r13d, %r13d
	xorl	%eax, %eax
	jmp	.L1730
	.p2align 4,,10
	.p2align 3
.L1749:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L1725:
	movq	(%rdi,%rdx,8), %r11
	movq	%r11, (%rax,%rdx,8)
	movq	%rdx, %r11
	addq	$1, %rdx
	cmpq	%r10, %r11
	jne	.L1725
	jmp	.L1729
	.p2align 4,,10
	.p2align 3
.L1751:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L1734:
	movq	(%rdi,%rdx,8), %r11
	movq	%r11, (%rax,%rdx,8)
	movq	%rdx, %r11
	addq	$1, %rdx
	cmpq	%r11, %r10
	jne	.L1734
	jmp	.L1738
	.p2align 4,,10
	.p2align 3
.L1746:
	movl	$1, %eax
	jmp	.L1715
	.p2align 4,,10
	.p2align 3
.L1779:
	movq	%rcx, -136(%rbp)
	movq	%r8, -128(%rbp)
	movq	%r9, -120(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-120(%rbp), %r9
	movq	-128(%rbp), %r8
	movq	-136(%rbp), %rcx
	jmp	.L1730
	.p2align 4,,10
	.p2align 3
.L1778:
	movq	%rcx, -136(%rbp)
	movq	%r8, -128(%rbp)
	movq	%r9, -120(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-120(%rbp), %r9
	movq	-128(%rbp), %r8
	movq	-136(%rbp), %rcx
	jmp	.L1721
	.p2align 4,,10
	.p2align 3
.L1777:
	movq	%rcx, -120(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-120(%rbp), %rcx
	movq	%rax, %r8
	jmp	.L1719
.L1781:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19369:
	.size	_ZNK2v88internal8compiler17AccessInfoFactory27FinalizePropertyAccessInfosENS0_10ZoneVectorINS1_18PropertyAccessInfoEEENS1_10AccessModeEPS5_, .-_ZNK2v88internal8compiler17AccessInfoFactory27FinalizePropertyAccessInfosENS0_10ZoneVectorINS1_18PropertyAccessInfoEEENS1_10AccessModeEPS5_
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compilerlsERSoNS1_10AccessModeE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compilerlsERSoNS1_10AccessModeE, @function
_GLOBAL__sub_I__ZN2v88internal8compilerlsERSoNS1_10AccessModeE:
.LFB24025:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE24025:
	.size	_GLOBAL__sub_I__ZN2v88internal8compilerlsERSoNS1_10AccessModeE, .-_GLOBAL__sub_I__ZN2v88internal8compilerlsERSoNS1_10AccessModeE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compilerlsERSoNS1_10AccessModeE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC15:
	.long	0
	.long	-1042284544
	.align 8
.LC16:
	.long	4290772992
	.long	1105199103
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
