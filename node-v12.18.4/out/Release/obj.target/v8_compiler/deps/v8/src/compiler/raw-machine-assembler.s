	.file	"raw-machine-assembler.cc"
	.text
	.section	.text._ZN2v88internal12StdoutStreamD1Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD1Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StdoutStreamD1Ev
	.type	_ZN2v88internal12StdoutStreamD1Ev, @function
_ZN2v88internal12StdoutStreamD1Ev:
.LFB27397:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE27397:
	.size	_ZN2v88internal12StdoutStreamD1Ev, .-_ZN2v88internal12StdoutStreamD1Ev
	.section	.text._ZN2v88internal12StdoutStreamD0Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD0Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.type	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev, @function
_ZTv0_n24_N2v88internal12StdoutStreamD0Ev:
.LFB27607:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %rax
	addq	-24(%rax), %rdi
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	movq	%rax, 80(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE27607:
	.size	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev, .-_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StdoutStreamD0Ev
	.type	_ZN2v88internal12StdoutStreamD0Ev, @function
_ZN2v88internal12StdoutStreamD0Ev:
.LFB27398:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE27398:
	.size	_ZN2v88internal12StdoutStreamD0Ev, .-_ZN2v88internal12StdoutStreamD0Ev
	.section	.text._ZN2v88internal12StdoutStreamD1Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD1Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.type	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev, @function
_ZTv0_n24_N2v88internal12StdoutStreamD1Ev:
.LFB27608:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rax
	movq	-24(%rax), %rbx
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	addq	%rdi, %rbx
	movq	%rax, 80(%rbx)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	64(%rbx), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE27608:
	.size	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev, .-_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.section	.rodata._ZN2v88internal8compiler19RawMachineAssemblerC2EPNS0_7IsolateEPNS1_5GraphEPNS1_14CallDescriptorENS0_21MachineRepresentationENS_4base5FlagsINS1_22MachineOperatorBuilder4FlagEjEENSC_21AlignmentRequirementsENS0_24PoisoningMitigationLevelE.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"cannot create std::vector larger than max_size()"
	.section	.text._ZN2v88internal8compiler19RawMachineAssemblerC2EPNS0_7IsolateEPNS1_5GraphEPNS1_14CallDescriptorENS0_21MachineRepresentationENS_4base5FlagsINS1_22MachineOperatorBuilder4FlagEjEENSC_21AlignmentRequirementsENS0_24PoisoningMitigationLevelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19RawMachineAssemblerC2EPNS0_7IsolateEPNS1_5GraphEPNS1_14CallDescriptorENS0_21MachineRepresentationENS_4base5FlagsINS1_22MachineOperatorBuilder4FlagEjEENSC_21AlignmentRequirementsENS0_24PoisoningMitigationLevelE
	.type	_ZN2v88internal8compiler19RawMachineAssemblerC2EPNS0_7IsolateEPNS1_5GraphEPNS1_14CallDescriptorENS0_21MachineRepresentationENS_4base5FlagsINS1_22MachineOperatorBuilder4FlagEjEENSC_21AlignmentRequirementsENS0_24PoisoningMitigationLevelE, @function
_ZN2v88internal8compiler19RawMachineAssemblerC2EPNS0_7IsolateEPNS1_5GraphEPNS1_14CallDescriptorENS0_21MachineRepresentationENS_4base5FlagsINS1_22MachineOperatorBuilder4FlagEjEENSC_21AlignmentRequirementsENS0_24PoisoningMitigationLevelE:
.LFB22167:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%r9d, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%r8d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%rdx, -80(%rbp)
	movq	(%rdx), %r8
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movups	%xmm0, (%rdi)
	movq	16(%r8), %r14
	movq	24(%r8), %rdx
	subq	%r14, %rdx
	cmpq	$119, %rdx
	jbe	.L29
	leaq	120(%r14), %rdx
	movq	%rdx, 16(%r8)
.L12:
	movq	%r14, %rdi
	xorl	%edx, %edx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler8ScheduleC1EPNS0_4ZoneEm@PLT
	movq	8(%rbx), %rax
	movq	%r14, 16(%rbx)
	movq	(%rax), %rdi
	movq	16(%rdi), %r14
	movq	24(%rdi), %rdx
	subq	%r14, %rdx
	cmpq	$55, %rdx
	jbe	.L30
	leaq	56(%r14), %rdx
	movq	%rdx, 16(%rdi)
.L14:
	movq	-80(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler19SourcePositionTableC1EPNS1_5GraphE@PLT
	movq	8(%rbx), %rax
	movq	%r14, 24(%rbx)
	movl	%r15d, %ecx
	movq	16(%rbp), %r8
	movl	24(%rbp), %r9d
	movl	%r13d, %edx
	leaq	32(%rbx), %rdi
	movq	(%rax), %rsi
	leaq	72(%rbx), %r14
	call	_ZN2v88internal8compiler22MachineOperatorBuilderC1EPNS0_4ZoneENS0_21MachineRepresentationENS_4base5FlagsINS2_4FlagEjEENS2_21AlignmentRequirementsE@PLT
	movq	8(%rbx), %rax
	movq	%r14, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler21CommonOperatorBuilderC1EPNS0_4ZoneE@PLT
	movq	8(%rbx), %rax
	leaq	88(%rbx), %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilderC1EPNS0_4ZoneE@PLT
	movq	8(%rbx), %rax
	movq	%r12, 104(%rbx)
	movq	$0, 112(%rbx)
	movq	(%rax), %r8
	movq	24(%r12), %rax
	movq	8(%rax), %rax
	cmpq	$268435455, %rax
	ja	.L31
	movq	%r8, 120(%rbx)
	leaq	0(,%rax,8), %rdx
	movq	$0, 128(%rbx)
	movq	$0, 136(%rbx)
	movq	$0, 144(%rbx)
	testq	%rax, %rax
	je	.L16
	movq	16(%r8), %rdi
	movq	24(%r8), %rax
	movq	%rdx, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rdx
	ja	.L32
	addq	%rdi, %rsi
	movq	%rsi, 16(%r8)
.L18:
	leaq	(%rdi,%rdx), %r13
	movq	%rdi, 128(%rbx)
	xorl	%esi, %esi
	movq	%r13, 144(%rbx)
	call	memset@PLT
	movq	104(%rbx), %rdx
.L22:
	movq	16(%rbx), %rax
	movq	%r13, 136(%rbx)
	movq	%r14, %rdi
	movq	104(%rax), %rax
	movq	%rax, 152(%rbx)
	movl	32(%rbp), %eax
	movl	%eax, 160(%rbx)
	movq	24(%rdx), %rax
	movq	8(%rax), %rsi
	addl	$1, %esi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5StartEi@PLT
	movq	-80(%rbp), %r15
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 8(%r15)
	cmpl	$1, 8(%r12)
	movq	%rax, %r13
	je	.L33
.L19:
	movq	104(%rbx), %rax
	xorl	%r12d, %r12d
	leaq	-64(%rbp), %r15
	movq	24(%rax), %rax
	cmpq	$0, 8(%rax)
	je	.L21
	.p2align 4,,10
	.p2align 3
.L20:
	movq	-80(%rbp), %rax
	movl	%r12d, %esi
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	8(%rax), %r13
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9ParameterEiPKc@PLT
	movq	8(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%rax, %rsi
	movq	128(%rbx), %rax
	movl	$1, %edx
	movq	%r13, -64(%rbp)
	leaq	(%rax,%r12,8), %r9
	addq	$1, %r12
	movq	%r9, -72(%rbp)
	call	_ZN2v88internal8compiler5Graph16NewNodeUncheckedEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	152(%rbx), %rsi
	movq	16(%rbx), %rdi
	movq	%rax, %rdx
	movq	%rax, %r13
	call	_ZN2v88internal8compiler8Schedule7AddNodeEPNS1_10BasicBlockEPNS1_4NodeE@PLT
	movq	-72(%rbp), %r9
	movq	%r13, (%r9)
	movq	104(%rbx), %rax
	movq	24(%rax), %rax
	cmpq	8(%rax), %r12
	jb	.L20
.L21:
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3EndEm@PLT
	movq	-80(%rbp), %r15
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	24(%rbx), %rdi
	movq	%rax, 16(%r15)
	call	_ZN2v88internal8compiler19SourcePositionTable12AddDecoratorEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L34
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	.cfi_restore_state
	xorl	%edx, %edx
	movl	$-1, %esi
	leaq	-64(%rbp), %r15
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9ParameterEiPKc@PLT
	movq	8(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r13, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph16NewNodeUncheckedEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	152(%rbx), %rsi
	movq	16(%rbx), %rdi
	movq	%rax, %r12
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler8Schedule7AddNodeEPNS1_10BasicBlockEPNS1_4NodeE@PLT
	movq	%r12, 112(%rbx)
	jmp	.L19
.L32:
	movq	%r8, %rdi
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-72(%rbp), %rdx
	movq	%rax, %rdi
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L16:
	movq	%r12, %rdx
	xorl	%r13d, %r13d
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L30:
	movl	$56, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r14
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L29:
	movq	%r8, %rdi
	movl	$120, %esi
	movq	%r8, -72(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-72(%rbp), %r8
	movq	%rax, %r14
	jmp	.L12
.L34:
	call	__stack_chk_fail@PLT
.L31:
	leaq	.LC0(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE22167:
	.size	_ZN2v88internal8compiler19RawMachineAssemblerC2EPNS0_7IsolateEPNS1_5GraphEPNS1_14CallDescriptorENS0_21MachineRepresentationENS_4base5FlagsINS1_22MachineOperatorBuilder4FlagEjEENSC_21AlignmentRequirementsENS0_24PoisoningMitigationLevelE, .-_ZN2v88internal8compiler19RawMachineAssemblerC2EPNS0_7IsolateEPNS1_5GraphEPNS1_14CallDescriptorENS0_21MachineRepresentationENS_4base5FlagsINS1_22MachineOperatorBuilder4FlagEjEENSC_21AlignmentRequirementsENS0_24PoisoningMitigationLevelE
	.globl	_ZN2v88internal8compiler19RawMachineAssemblerC1EPNS0_7IsolateEPNS1_5GraphEPNS1_14CallDescriptorENS0_21MachineRepresentationENS_4base5FlagsINS1_22MachineOperatorBuilder4FlagEjEENSC_21AlignmentRequirementsENS0_24PoisoningMitigationLevelE
	.set	_ZN2v88internal8compiler19RawMachineAssemblerC1EPNS0_7IsolateEPNS1_5GraphEPNS1_14CallDescriptorENS0_21MachineRepresentationENS_4base5FlagsINS1_22MachineOperatorBuilder4FlagEjEENSC_21AlignmentRequirementsENS0_24PoisoningMitigationLevelE,_ZN2v88internal8compiler19RawMachineAssemblerC2EPNS0_7IsolateEPNS1_5GraphEPNS1_14CallDescriptorENS0_21MachineRepresentationENS_4base5FlagsINS1_22MachineOperatorBuilder4FlagEjEENSC_21AlignmentRequirementsENS0_24PoisoningMitigationLevelE
	.section	.text._ZN2v88internal8compiler19RawMachineAssembler17SetSourcePositionEPKci,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19RawMachineAssembler17SetSourcePositionEPKci
	.type	_ZN2v88internal8compiler19RawMachineAssembler17SetSourcePositionEPKci, @function
_ZN2v88internal8compiler19RawMachineAssembler17SetSourcePositionEPKci:
.LFB22169:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	(%rdi), %rdi
	movslq	%edx, %rbx
	addq	%rbx, %rbx
	andq	$-2145386497, %rbx
	call	_ZN2v88internal7Isolate37LookupOrAddExternallyCompiledFilenameEPKc@PLT
	movq	24(%r12), %rdx
	cltq
	salq	$21, %rax
	orq	%rax, %rbx
	movabsq	$-140735340871681, %rax
	andq	%rax, %rbx
	orq	$1, %rbx
	movq	%rbx, 16(%rdx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22169:
	.size	_ZN2v88internal8compiler19RawMachineAssembler17SetSourcePositionEPKci, .-_ZN2v88internal8compiler19RawMachineAssembler17SetSourcePositionEPKci
	.section	.text._ZN2v88internal8compiler19RawMachineAssembler12NullConstantEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19RawMachineAssembler12NullConstantEv
	.type	_ZN2v88internal8compiler19RawMachineAssembler12NullConstantEv, @function
_ZN2v88internal8compiler19RawMachineAssembler12NullConstantEv:
.LFB22170:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	leaq	-32(%rbp), %rsi
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	addq	$72, %rdi
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	-72(%rdi), %rax
	addq	$104, %rax
	movq	%rax, -32(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12HeapConstantERKNS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	8(%rbx), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler5Graph16NewNodeUncheckedEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	152(%rbx), %rsi
	movq	16(%rbx), %rdi
	movq	%rax, %rdx
	movq	%rax, %r12
	call	_ZN2v88internal8compiler8Schedule7AddNodeEPNS1_10BasicBlockEPNS1_4NodeE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L40
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L40:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22170:
	.size	_ZN2v88internal8compiler19RawMachineAssembler12NullConstantEv, .-_ZN2v88internal8compiler19RawMachineAssembler12NullConstantEv
	.section	.text._ZN2v88internal8compiler19RawMachineAssembler17UndefinedConstantEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19RawMachineAssembler17UndefinedConstantEv
	.type	_ZN2v88internal8compiler19RawMachineAssembler17UndefinedConstantEv, @function
_ZN2v88internal8compiler19RawMachineAssembler17UndefinedConstantEv:
.LFB22171:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	leaq	-32(%rbp), %rsi
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	addq	$72, %rdi
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	-72(%rdi), %rax
	addq	$88, %rax
	movq	%rax, -32(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12HeapConstantERKNS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	8(%rbx), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler5Graph16NewNodeUncheckedEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	152(%rbx), %rsi
	movq	16(%rbx), %rdi
	movq	%rax, %rdx
	movq	%rax, %r12
	call	_ZN2v88internal8compiler8Schedule7AddNodeEPNS1_10BasicBlockEPNS1_4NodeE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L44
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L44:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22171:
	.size	_ZN2v88internal8compiler19RawMachineAssembler17UndefinedConstantEv, .-_ZN2v88internal8compiler19RawMachineAssembler17UndefinedConstantEv
	.section	.text._ZN2v88internal8compiler19RawMachineAssembler25RelocatableIntPtrConstantElNS0_9RelocInfo4ModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19RawMachineAssembler25RelocatableIntPtrConstantElNS0_9RelocInfo4ModeE
	.type	_ZN2v88internal8compiler19RawMachineAssembler25RelocatableIntPtrConstantElNS0_9RelocInfo4ModeE, @function
_ZN2v88internal8compiler19RawMachineAssembler25RelocatableIntPtrConstantElNS0_9RelocInfo4ModeE:
.LFB22172:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movsbl	%dl, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	leaq	72(%rdi), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder24RelocatableInt64ConstantElNS0_9RelocInfo4ModeE@PLT
	movq	8(%rbx), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rax, %rsi
	xorl	%edx, %edx
	call	_ZN2v88internal8compiler5Graph16NewNodeUncheckedEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	152(%rbx), %rsi
	movq	16(%rbx), %rdi
	movq	%rax, %r12
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler8Schedule7AddNodeEPNS1_10BasicBlockEPNS1_4NodeE@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22172:
	.size	_ZN2v88internal8compiler19RawMachineAssembler25RelocatableIntPtrConstantElNS0_9RelocInfo4ModeE, .-_ZN2v88internal8compiler19RawMachineAssembler25RelocatableIntPtrConstantElNS0_9RelocInfo4ModeE
	.section	.text._ZN2v88internal8compiler19RawMachineAssembler17OptimizedAllocateEPNS1_4NodeENS0_14AllocationTypeENS0_17AllowLargeObjectsE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19RawMachineAssembler17OptimizedAllocateEPNS1_4NodeENS0_14AllocationTypeENS0_17AllowLargeObjectsE
	.type	_ZN2v88internal8compiler19RawMachineAssembler17OptimizedAllocateEPNS1_4NodeENS0_14AllocationTypeENS0_17AllowLargeObjectsE, @function
_ZN2v88internal8compiler19RawMachineAssembler17OptimizedAllocateEPNS1_4NodeENS0_14AllocationTypeENS0_17AllowLargeObjectsE:
.LFB22173:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	movl	$4294967295, %esi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	addq	$88, %rdi
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder11AllocateRawENS1_4TypeENS0_14AllocationTypeENS0_17AllowLargeObjectsE@PLT
	movq	8(%rbx), %rdi
	xorl	%r8d, %r8d
	leaq	-32(%rbp), %rcx
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r12, -32(%rbp)
	call	_ZN2v88internal8compiler5Graph16NewNodeUncheckedEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	152(%rbx), %rsi
	movq	16(%rbx), %rdi
	movq	%rax, %rdx
	movq	%rax, %r12
	call	_ZN2v88internal8compiler8Schedule7AddNodeEPNS1_10BasicBlockEPNS1_4NodeE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L50
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L50:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22173:
	.size	_ZN2v88internal8compiler19RawMachineAssembler17OptimizedAllocateEPNS1_4NodeENS0_14AllocationTypeENS0_17AllowLargeObjectsE, .-_ZN2v88internal8compiler19RawMachineAssembler17OptimizedAllocateEPNS1_4NodeENS0_14AllocationTypeENS0_17AllowLargeObjectsE
	.section	.rodata._ZN2v88internal8compiler19RawMachineAssembler13ExportForTestEv.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"--- RAW SCHEDULE -------------------------------------------\n"
	.align 8
.LC2:
	.string	"--- EDGE SPLIT AND PROPAGATED DEFERRED SCHEDULE ------------\n"
	.section	.text._ZN2v88internal8compiler19RawMachineAssembler13ExportForTestEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19RawMachineAssembler13ExportForTestEv
	.type	_ZN2v88internal8compiler19RawMachineAssembler13ExportForTestEv, @function
_ZN2v88internal8compiler19RawMachineAssembler13ExportForTestEv:
.LFB22174:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$352, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L56
.L52:
	movq	16(%rbx), %rdi
	call	_ZN2v88internal8compiler8Schedule23EnsureCFGWellFormednessEv@PLT
	movq	8(%rbx), %rax
	movq	16(%rbx), %rsi
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler9Scheduler17ComputeSpecialRPOEPNS0_4ZoneEPNS1_8ScheduleE@PLT
	movq	16(%rbx), %rdi
	call	_ZN2v88internal8compiler8Schedule21PropagateDeferredMarkEv@PLT
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L57
.L53:
	movq	24(%rbx), %rdi
	call	_ZN2v88internal8compiler19SourcePositionTable15RemoveDecoratorEv@PLT
	movq	16(%rbx), %rax
	movq	$0, 16(%rbx)
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L58
	addq	$352, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	.cfi_restore_state
	leaq	-304(%rbp), %r12
	leaq	.LC1(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	%r12, %rdi
	leaq	-384(%rbp), %r14
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %r13
	call	_ZNSt8ios_baseC2Ev@PLT
	xorl	%edx, %edx
	pxor	%xmm0, %xmm0
	movq	%r14, %rdi
	movw	%dx, -80(%rbp)
	movq	stdout(%rip), %rdx
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movups	%xmm0, -72(%rbp)
	movups	%xmm0, -56(%rbp)
	movq	%r13, -304(%rbp)
	movq	$0, -88(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	movq	16(%rbx), %rsi
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -384(%rbp)
	addq	$40, %rax
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal8compilerlsERSoRKNS1_8ScheduleE@PLT
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rax, -304(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rdx, %xmm0
	leaq	-320(%rbp), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%r12, %rdi
	movq	%r13, -304(%rbp)
	movq	%rax, -384(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L57:
	xorl	%eax, %eax
	leaq	-304(%rbp), %r12
	leaq	.LC2(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	%r12, %rdi
	leaq	-384(%rbp), %r14
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %r13
	call	_ZNSt8ios_baseC2Ev@PLT
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	stdout(%rip), %rdx
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movups	%xmm0, -72(%rbp)
	movups	%xmm0, -56(%rbp)
	movw	%ax, -80(%rbp)
	movq	%r13, -304(%rbp)
	movq	$0, -88(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	movq	16(%rbx), %rsi
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -384(%rbp)
	addq	$40, %rax
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal8compilerlsERSoRKNS1_8ScheduleE@PLT
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rcx
	movq	%rax, -304(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-320(%rbp), %rdi
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%r12, %rdi
	movq	%r13, -304(%rbp)
	movq	%rax, -384(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L53
.L58:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22174:
	.size	_ZN2v88internal8compiler19RawMachineAssembler13ExportForTestEv, .-_ZN2v88internal8compiler19RawMachineAssembler13ExportForTestEv
	.section	.text._ZN2v88internal8compiler19RawMachineAssembler19OptimizeControlFlowEPNS1_8ScheduleEPNS1_5GraphEPNS1_21CommonOperatorBuilderE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19RawMachineAssembler19OptimizeControlFlowEPNS1_8ScheduleEPNS1_5GraphEPNS1_21CommonOperatorBuilderE
	.type	_ZN2v88internal8compiler19RawMachineAssembler19OptimizeControlFlowEPNS1_8ScheduleEPNS1_5GraphEPNS1_21CommonOperatorBuilderE, @function
_ZN2v88internal8compiler19RawMachineAssembler19OptimizeControlFlowEPNS1_8ScheduleEPNS1_5GraphEPNS1_21CommonOperatorBuilderE:
.LFB22184:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$120, %rsp
	movq	%rdx, -96(%rbp)
	movq	24(%rdi), %rcx
	movq	%rsi, -88(%rbp)
	movq	16(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L60:
	movq	%rcx, %rax
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rax, %rbx
	jnb	.L63
.L100:
	leaq	(%rdx,%rbx,8), %rax
	movq	(%rax), %r12
	testq	%r12, %r12
	je	.L97
.L65:
	movl	52(%r12), %eax
	cmpl	$1, %eax
	je	.L99
	addq	$1, %rbx
	cmpl	$3, %eax
	jne	.L60
	movq	72(%r12), %rsi
	movq	80(%r12), %rax
	subq	%rsi, %rax
	cmpq	$8, %rax
	jne	.L60
	movq	(%rsi), %r14
	movq	(%r14), %rax
	cmpw	$35, 16(%rax)
	jne	.L60
	movq	56(%r12), %rax
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	cmpq	%r14, %rax
	je	.L76
.L96:
	movq	24(%r13), %rcx
	movq	16(%r13), %rdx
	movq	%rcx, %rax
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rax, %rbx
	jb	.L100
	.p2align 4,,10
	.p2align 3
.L63:
	testb	%r15b, %r15b
	je	.L59
	testq	%rax, %rax
	je	.L59
	movq	%rdx, %rax
	xorl	%ebx, %ebx
	xorl	%r15d, %r15d
	movq	(%rax), %r12
	testq	%r12, %r12
	jne	.L65
	.p2align 4,,10
	.p2align 3
.L97:
	addq	$1, %rbx
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L99:
	movq	104(%r12), %rax
	movq	(%rax), %r14
	movq	144(%r14), %rax
	subq	136(%r14), %rax
	cmpq	$8, %rax
	jne	.L97
	movq	80(%r14), %rax
	movq	72(%r14), %r15
	cmpq	%rax, %r15
	je	.L73
	movq	%r14, -72(%rbp)
	movq	%rbx, -80(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	.p2align 4,,10
	.p2align 3
.L72:
	movq	(%rbx), %r14
	xorl	%esi, %esi
	movq	%r13, %rdi
	addq	$8, %rbx
	movq	%r14, %rdx
	call	_ZN2v88internal8compiler8Schedule15SetBlockForNodeEPNS1_10BasicBlockEPNS1_4NodeE@PLT
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler8Schedule7AddNodeEPNS1_10BasicBlockEPNS1_4NodeE@PLT
	cmpq	%rbx, %r15
	jne	.L72
	movq	-72(%rbp), %r14
	movq	-80(%rbp), %rbx
.L73:
	movl	52(%r14), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler10BasicBlock11set_controlENS2_7ControlE@PLT
	movq	56(%r14), %r15
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler10BasicBlock17set_control_inputEPNS1_4NodeE@PLT
	testq	%r15, %r15
	je	.L71
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler8Schedule15SetBlockForNodeEPNS1_10BasicBlockEPNS1_4NodeE@PLT
.L71:
	cmpb	$0, 8(%r14)
	je	.L74
	movb	$1, 8(%r12)
.L74:
	movq	104(%r12), %rax
	cmpq	112(%r12), %rax
	je	.L75
	movq	%rax, 112(%r12)
.L75:
	movq	%r14, %rsi
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler8Schedule14MoveSuccessorsEPNS1_10BasicBlockES4_@PLT
	movq	160(%r14), %rsi
.L98:
	movq	%r13, %rdi
	movl	$1, %r15d
	call	_ZN2v88internal8compiler8Schedule14ClearBlockByIdENS1_10BasicBlock2IdE@PLT
	movq	24(%r13), %rcx
	movq	16(%r13), %rdx
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L59:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L101
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L76:
	.cfi_restore_state
	movq	%rax, %rdi
	call	_ZNK2v88internal8compiler4Node8UseCountEv@PLT
	cmpl	$1, %eax
	jne	.L96
	movq	104(%r12), %rax
	movq	(%rax), %r15
	movq	8(%rax), %rax
	movq	%rax, -120(%rbp)
	movq	72(%r15), %rax
	movq	%r15, -128(%rbp)
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler4Node4KillEv@PLT
	movq	72(%r15), %rdi
	movq	80(%r15), %rax
	leaq	8(%rdi), %rsi
	cmpq	%rsi, %rax
	je	.L78
	subq	%rsi, %rax
	movq	%rax, %rdx
	call	memmove@PLT
	movq	80(%r15), %rsi
.L78:
	movq	-120(%rbp), %r15
	movq	-128(%rbp), %rax
	subq	$8, %rsi
	movq	%rsi, 80(%rax)
	movq	72(%r15), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler4Node4KillEv@PLT
	movq	72(%r15), %rdi
	movq	80(%r15), %rax
	leaq	8(%rdi), %rsi
	cmpq	%rsi, %rax
	je	.L79
	subq	%rsi, %rax
	movq	%rax, %rdx
	call	memmove@PLT
	movq	80(%r15), %rsi
.L79:
	movq	-120(%rbp), %rax
	movq	-128(%rbp), %rdi
	subq	$8, %rsi
	movq	%rsi, 80(%rax)
	movq	136(%rdi), %rax
	cmpq	144(%rdi), %rax
	je	.L80
	movq	%rax, 144(%rdi)
.L80:
	movq	-120(%rbp), %rdi
	movq	136(%rdi), %rax
	cmpq	144(%rdi), %rax
	je	.L81
	movq	%rax, 144(%rdi)
.L81:
	movq	136(%r12), %rax
	movq	144(%r12), %rdx
	subq	%rax, %rdx
	sarq	$3, %rdx
	movq	%rdx, -136(%rbp)
	je	.L82
	leaq	-64(%rbp), %rdi
	movq	%r14, -144(%rbp)
	xorl	%r10d, %r10d
	movq	%r12, %r11
	movq	%rdi, -80(%rbp)
	movq	%rbx, -152(%rbp)
	jmp	.L85
.L102:
	movq	136(%r11), %rax
.L85:
	movq	(%rax,%r10,8), %r15
	movq	104(%r15), %rax
	cmpq	112(%r15), %rax
	je	.L83
	movq	%rax, 112(%r15)
.L83:
	cmpb	$0, 8(%r11)
	je	.L84
	movb	$1, 8(%r15)
.L84:
	movq	-72(%rbp), %rsi
	movq	-88(%rbp), %rdi
	movq	%r11, -112(%rbp)
	movq	%r10, -104(%rbp)
	call	_ZN2v88internal8compiler5Graph9CloneNodeEPKNS1_4NodeE@PLT
	movq	-104(%rbp), %r10
	movq	-144(%rbp), %rdi
	movq	%rax, %r14
	movl	%r10d, %esi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties17ReplaceValueInputEPNS1_4NodeES4_i@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler8Schedule13NewBasicBlockEv@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler8Schedule13NewBasicBlockEv@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	movq	-80(%rbp), %rcx
	movq	-88(%rbp), %rdi
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r14, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler10BasicBlock7AddNodeEPNS1_4NodeE@PLT
	movq	-96(%rbp), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	movq	-80(%rbp), %rcx
	movq	-88(%rbp), %rdi
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r14, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler10BasicBlock7AddNodeEPNS1_4NodeE@PLT
	movq	-128(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler8Schedule7AddGotoEPNS1_10BasicBlockES4_@PLT
	movq	-120(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler8Schedule7AddGotoEPNS1_10BasicBlockES4_@PLT
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler10BasicBlock11set_controlENS2_7ControlE@PLT
	movq	%rbx, %r8
	movq	%r12, %rcx
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler8Schedule9AddBranchEPNS1_10BasicBlockEPNS1_4NodeES4_S4_@PLT
	movq	-104(%rbp), %r10
	movq	-112(%rbp), %r11
	addq	$1, %r10
	cmpq	-136(%rbp), %r10
	jne	.L102
	movq	-152(%rbp), %rbx
	movq	%r11, %r12
.L82:
	movq	-72(%rbp), %rdi
	call	_ZN2v88internal8compiler4Node4KillEv@PLT
	movq	160(%r12), %rsi
	jmp	.L98
.L101:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22184:
	.size	_ZN2v88internal8compiler19RawMachineAssembler19OptimizeControlFlowEPNS1_8ScheduleEPNS1_5GraphEPNS1_21CommonOperatorBuilderE, .-_ZN2v88internal8compiler19RawMachineAssembler19OptimizeControlFlowEPNS1_8ScheduleEPNS1_5GraphEPNS1_21CommonOperatorBuilderE
	.section	.rodata._ZN2v88internal8compiler19RawMachineAssembler19MarkControlDeferredEPNS1_4NodeE.str1.1,"aMS",@progbits,1
.LC3:
	.string	"unreachable code"
	.section	.text._ZN2v88internal8compiler19RawMachineAssembler19MarkControlDeferredEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19RawMachineAssembler19MarkControlDeferredEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19RawMachineAssembler19MarkControlDeferredEPNS1_4NodeE, @function
_ZN2v88internal8compiler19RawMachineAssembler19MarkControlDeferredEPNS1_4NodeE:
.LFB22216:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	.L107(%rip), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
.L104:
	movq	(%r12), %rdi
	cmpw	$10, 16(%rdi)
	ja	.L105
	movzwl	16(%rdi), %eax
	movslq	0(%r13,%rax,4), %rax
	addq	%r13, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler19RawMachineAssembler19MarkControlDeferredEPNS1_4NodeE,"a",@progbits
	.align 4
	.align 4
.L107:
	.long	.L103-.L107
	.long	.L105-.L107
	.long	.L113-.L107
	.long	.L113-.L107
	.long	.L112-.L107
	.long	.L111-.L107
	.long	.L105-.L107
	.long	.L103-.L107
	.long	.L109-.L107
	.long	.L108-.L107
	.long	.L106-.L107
	.section	.text._ZN2v88internal8compiler19RawMachineAssembler19MarkControlDeferredEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L109:
	call	_ZN2v88internal8compiler19IfValueParametersOfEPKNS1_8OperatorE@PLT
	cmpb	$2, 8(%rax)
	jne	.L129
.L103:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L105:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r12
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L108:
	call	_ZN2v88internal8compiler12BranchHintOfEPKNS1_8OperatorE@PLT
	cmpb	$2, %al
	je	.L103
	leaq	72(%rbx), %rdi
	movl	$2, %esi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9IfDefaultENS1_10BranchHintE@PLT
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L111:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	(%rax), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler20BranchOperatorInfoOfEPKNS1_8OperatorE@PLT
	cmpb	$2, (%rax)
	je	.L105
	movl	$1, %r13d
.L116:
	movq	(%r12), %rdi
	call	_ZN2v88internal8compiler20BranchOperatorInfoOfEPKNS1_8OperatorE@PLT
	cmpb	(%rax), %r13b
	je	.L103
	movzbl	1(%rax), %edx
	leaq	72(%rbx), %rdi
	movl	%r13d, %esi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L112:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	(%rax), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler20BranchOperatorInfoOfEPKNS1_8OperatorE@PLT
	cmpb	$1, (%rax)
	je	.L105
	movl	$2, %r13d
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L106:
	movl	28(%rdi), %eax
	testl	%eax, %eax
	jle	.L103
	xorl	%r13d, %r13d
.L118:
	movl	%r13d, %esi
	movq	%r12, %rdi
	addl	$1, %r13d
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler19RawMachineAssembler19MarkControlDeferredEPNS1_4NodeE
	movq	(%r12), %rax
	cmpl	%r13d, 28(%rax)
	jg	.L118
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L129:
	.cfi_restore_state
	movl	4(%rax), %edx
	movl	(%rax), %esi
	leaq	72(%rbx), %rdi
	movl	$2, %ecx
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfValueEiiNS1_10BranchHintE@PLT
.L128:
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	.p2align 4,,10
	.p2align 3
.L113:
	.cfi_restore_state
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE22216:
	.size	_ZN2v88internal8compiler19RawMachineAssembler19MarkControlDeferredEPNS1_4NodeE, .-_ZN2v88internal8compiler19RawMachineAssembler19MarkControlDeferredEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19RawMachineAssembler15TargetParameterEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19RawMachineAssembler15TargetParameterEv
	.type	_ZN2v88internal8compiler19RawMachineAssembler15TargetParameterEv, @function
_ZN2v88internal8compiler19RawMachineAssembler15TargetParameterEv:
.LFB22217:
	.cfi_startproc
	endbr64
	movq	112(%rdi), %rax
	ret
	.cfi_endproc
.LFE22217:
	.size	_ZN2v88internal8compiler19RawMachineAssembler15TargetParameterEv, .-_ZN2v88internal8compiler19RawMachineAssembler15TargetParameterEv
	.section	.text._ZN2v88internal8compiler19RawMachineAssembler9ParameterEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19RawMachineAssembler9ParameterEm
	.type	_ZN2v88internal8compiler19RawMachineAssembler9ParameterEm, @function
_ZN2v88internal8compiler19RawMachineAssembler9ParameterEm:
.LFB22218:
	.cfi_startproc
	endbr64
	movq	128(%rdi), %rax
	movq	(%rax,%rsi,8), %rax
	ret
	.cfi_endproc
.LFE22218:
	.size	_ZN2v88internal8compiler19RawMachineAssembler9ParameterEm, .-_ZN2v88internal8compiler19RawMachineAssembler9ParameterEm
	.section	.text._ZN2v88internal8compiler19RawMachineAssembler4GotoEPNS1_15RawMachineLabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19RawMachineAssembler4GotoEPNS1_15RawMachineLabelE
	.type	_ZN2v88internal8compiler19RawMachineAssembler4GotoEPNS1_15RawMachineLabelE, @function
_ZN2v88internal8compiler19RawMachineAssembler4GotoEPNS1_15RawMachineLabelE:
.LFB22219:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rsi), %rdx
	movq	16(%rdi), %r13
	movb	$1, 8(%rsi)
	testq	%rdx, %rdx
	je	.L135
.L133:
	movq	152(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler8Schedule7AddGotoEPNS1_10BasicBlockES4_@PLT
	movq	$0, 152(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L135:
	.cfi_restore_state
	movq	16(%rdi), %rdi
	movq	%rsi, %r12
	call	_ZN2v88internal8compiler8Schedule13NewBasicBlockEv@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdx
	jmp	.L133
	.cfi_endproc
.LFE22219:
	.size	_ZN2v88internal8compiler19RawMachineAssembler4GotoEPNS1_15RawMachineLabelE, .-_ZN2v88internal8compiler19RawMachineAssembler4GotoEPNS1_15RawMachineLabelE
	.section	.text._ZN2v88internal8compiler19RawMachineAssembler6BranchEPNS1_4NodeEPNS1_15RawMachineLabelES6_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19RawMachineAssembler6BranchEPNS1_4NodeEPNS1_15RawMachineLabelES6_
	.type	_ZN2v88internal8compiler19RawMachineAssembler6BranchEPNS1_4NodeEPNS1_15RawMachineLabelES6_, @function
_ZN2v88internal8compiler19RawMachineAssembler6BranchEPNS1_4NodeEPNS1_15RawMachineLabelES6_:
.LFB22220:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	72(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-64(%rbp), %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r15, %rdi
	subq	$56, %rsp
	movq	%rsi, -72(%rbp)
	xorl	%esi, %esi
	movq	%rdx, -88(%rbp)
	movl	$2, %edx
	movq	%rcx, -80(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	movq	8(%rbx), %rdi
	leaq	-72(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movl	$1, %edx
	call	_ZN2v88internal8compiler5Graph16NewNodeUncheckedEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%rbx), %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler8Schedule13NewBasicBlockEv@PLT
	movq	16(%rbx), %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler8Schedule13NewBasicBlockEv@PLT
	movq	-64(%rbp), %rdx
	movq	16(%rbx), %rdi
	movq	%r13, %rcx
	movq	152(%rbx), %rsi
	movq	%rax, %r8
	movq	%rax, %r12
	call	_ZN2v88internal8compiler8Schedule9AddBranchEPNS1_10BasicBlockEPNS1_4NodeES4_S4_@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	movq	8(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler5Graph16NewNodeUncheckedEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler10BasicBlock7AddNodeEPNS1_4NodeE@PLT
	movq	-88(%rbp), %r9
	movq	16(%rbx), %r8
	movq	(%r9), %rdx
	movb	$1, 8(%r9)
	testq	%rdx, %rdx
	je	.L141
.L137:
	movq	%r13, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler8Schedule7AddGotoEPNS1_10BasicBlockES4_@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	movq	8(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler5Graph16NewNodeUncheckedEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler10BasicBlock7AddNodeEPNS1_4NodeE@PLT
	movq	-80(%rbp), %rax
	movq	16(%rbx), %r13
	movq	(%rax), %rdx
	movb	$1, 8(%rax)
	testq	%rdx, %rdx
	je	.L142
.L138:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler8Schedule7AddGotoEPNS1_10BasicBlockES4_@PLT
	movq	$0, 152(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L143
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L141:
	.cfi_restore_state
	movq	16(%rbx), %rdi
	movq	%r9, -96(%rbp)
	movq	%r8, -88(%rbp)
	call	_ZN2v88internal8compiler8Schedule13NewBasicBlockEv@PLT
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %r8
	movq	%rax, %rdx
	movq	%rax, (%r9)
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L142:
	movq	16(%rbx), %rdi
	call	_ZN2v88internal8compiler8Schedule13NewBasicBlockEv@PLT
	movq	%rax, %rdx
	movq	-80(%rbp), %rax
	movq	%rdx, (%rax)
	jmp	.L138
.L143:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22220:
	.size	_ZN2v88internal8compiler19RawMachineAssembler6BranchEPNS1_4NodeEPNS1_15RawMachineLabelES6_, .-_ZN2v88internal8compiler19RawMachineAssembler6BranchEPNS1_4NodeEPNS1_15RawMachineLabelES6_
	.section	.text._ZN2v88internal8compiler19RawMachineAssembler13ContinuationsEPNS1_4NodeEPNS1_15RawMachineLabelES6_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19RawMachineAssembler13ContinuationsEPNS1_4NodeEPNS1_15RawMachineLabelES6_
	.type	_ZN2v88internal8compiler19RawMachineAssembler13ContinuationsEPNS1_4NodeEPNS1_15RawMachineLabelES6_, @function
_ZN2v88internal8compiler19RawMachineAssembler13ContinuationsEPNS1_4NodeEPNS1_15RawMachineLabelES6_:
.LFB22221:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rcx), %r8
	movq	16(%rdi), %r15
	movb	$1, 8(%rcx)
	testq	%r8, %r8
	je	.L148
	movq	(%r12), %rcx
	movb	$1, 8(%r12)
	testq	%rcx, %rcx
	je	.L149
.L146:
	movq	152(%rbx), %rsi
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler8Schedule7AddCallEPNS1_10BasicBlockEPNS1_4NodeES4_S4_@PLT
	movq	$0, 152(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L148:
	.cfi_restore_state
	movq	16(%rdi), %rdi
	movq	%rcx, %r13
	call	_ZN2v88internal8compiler8Schedule13NewBasicBlockEv@PLT
	movq	%rax, 0(%r13)
	movq	(%r12), %rcx
	movq	%rax, %r8
	movb	$1, 8(%r12)
	testq	%rcx, %rcx
	jne	.L146
	.p2align 4,,10
	.p2align 3
.L149:
	movq	16(%rbx), %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal8compiler8Schedule13NewBasicBlockEv@PLT
	movq	-56(%rbp), %r8
	movq	%rax, (%r12)
	movq	%rax, %rcx
	jmp	.L146
	.cfi_endproc
.LFE22221:
	.size	_ZN2v88internal8compiler19RawMachineAssembler13ContinuationsEPNS1_4NodeEPNS1_15RawMachineLabelES6_, .-_ZN2v88internal8compiler19RawMachineAssembler13ContinuationsEPNS1_4NodeEPNS1_15RawMachineLabelES6_
	.section	.text._ZN2v88internal8compiler19RawMachineAssembler6SwitchEPNS1_4NodeEPNS1_15RawMachineLabelEPKiPS6_m,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19RawMachineAssembler6SwitchEPNS1_4NodeEPNS1_15RawMachineLabelEPKiPS6_m
	.type	_ZN2v88internal8compiler19RawMachineAssembler6SwitchEPNS1_4NodeEPNS1_15RawMachineLabelEPKiPS6_m, @function
_ZN2v88internal8compiler19RawMachineAssembler6SwitchEPNS1_4NodeEPNS1_15RawMachineLabelEPKiPS6_m:
.LFB22222:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	72(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r9, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r15, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$120, %rsp
	movq	%rsi, -72(%rbp)
	movq	%rdx, -144(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	1(%r9), %rax
	movq	%rax, %rsi
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6SwitchEm@PLT
	movq	8(%r12), %rdi
	leaq	-72(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movl	$1, %edx
	call	_ZN2v88internal8compiler5Graph16NewNodeUncheckedEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -96(%rbp)
	movq	8(%r12), %rax
	movq	(%rax), %rdi
	movq	-136(%rbp), %rax
	salq	$3, %rax
	movq	%rax, -152(%rbp)
	movq	%rax, %rsi
	movq	16(%rdi), %rax
	movq	%rax, %rcx
	movq	%rax, -128(%rbp)
	movq	24(%rdi), %rax
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L162
	addq	-128(%rbp), %rsi
	movq	%rsi, 16(%rdi)
.L152:
	testq	%r14, %r14
	je	.L163
	leaq	0(%r13,%r14,4), %rax
	leaq	-64(%rbp), %rcx
	movq	%rax, -120(%rbp)
	movq	-128(%rbp), %rax
	movq	%rcx, -104(%rbp)
	subq	%rax, %rbx
	movq	%rax, %r14
	movq	%rbx, -112(%rbp)
	.p2align 4,,10
	.p2align 3
.L159:
	movl	0(%r13), %esi
	movq	16(%r12), %rdi
	movl	%esi, -88(%rbp)
	call	_ZN2v88internal8compiler8Schedule13NewBasicBlockEv@PLT
	movq	8(%r12), %r10
	movl	-88(%rbp), %esi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	%rax, %rbx
	movq	%r10, -80(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfValueEiiNS1_10BranchHintE@PLT
	movq	-80(%rbp), %r10
	movq	-104(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movq	-96(%rbp), %rax
	movl	$1, %edx
	movq	%r10, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%r12), %rdi
	movq	%rbx, %rsi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler8Schedule7AddNodeEPNS1_10BasicBlockEPNS1_4NodeE@PLT
	movq	-112(%rbp), %rax
	movq	16(%r12), %r8
	movq	(%rax,%r14), %rcx
	movq	(%rcx), %rdx
	movb	$1, 8(%rcx)
	testq	%rdx, %rdx
	je	.L164
	movq	%rbx, %rsi
	movq	%r8, %rdi
	addq	$4, %r13
	addq	$8, %r14
	call	_ZN2v88internal8compiler8Schedule7AddGotoEPNS1_10BasicBlockES4_@PLT
	movq	%rbx, -8(%r14)
	cmpq	%r13, -120(%rbp)
	jne	.L159
.L157:
	movq	16(%r12), %rdi
	call	_ZN2v88internal8compiler8Schedule13NewBasicBlockEv@PLT
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	8(%r12), %r13
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9IfDefaultENS1_10BranchHintE@PLT
	movq	-104(%rbp), %rcx
	movq	%r13, %rdi
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movq	-96(%rbp), %rax
	movl	$1, %edx
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%r12), %rdi
	movq	%rbx, %rsi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler8Schedule7AddNodeEPNS1_10BasicBlockEPNS1_4NodeE@PLT
	movq	-144(%rbp), %rax
	movq	16(%r12), %r13
	movq	(%rax), %rdx
	movb	$1, 8(%rax)
	testq	%rdx, %rdx
	je	.L165
.L155:
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler8Schedule7AddGotoEPNS1_10BasicBlockES4_@PLT
	movq	-152(%rbp), %rax
	movq	-128(%rbp), %rcx
	movq	-136(%rbp), %r8
	movq	-96(%rbp), %rdx
	movq	%rbx, -8(%rcx,%rax)
	movq	152(%r12), %rsi
	movq	16(%r12), %rdi
	call	_ZN2v88internal8compiler8Schedule9AddSwitchEPNS1_10BasicBlockEPNS1_4NodeEPS4_m@PLT
	movq	$0, 152(%r12)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L166
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L164:
	.cfi_restore_state
	movq	16(%r12), %rdi
	movq	%r8, -88(%rbp)
	addq	$4, %r13
	addq	$8, %r14
	movq	%rcx, -80(%rbp)
	call	_ZN2v88internal8compiler8Schedule13NewBasicBlockEv@PLT
	movq	-80(%rbp), %rcx
	movq	-88(%rbp), %r8
	movq	%rbx, %rsi
	movq	%rax, %rdx
	movq	%rax, (%rcx)
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler8Schedule7AddGotoEPNS1_10BasicBlockES4_@PLT
	movq	%rbx, -8(%r14)
	cmpq	-120(%rbp), %r13
	jne	.L159
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L165:
	movq	16(%r12), %rdi
	call	_ZN2v88internal8compiler8Schedule13NewBasicBlockEv@PLT
	movq	%rax, %rdx
	movq	-144(%rbp), %rax
	movq	%rdx, (%rax)
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L163:
	leaq	-64(%rbp), %rax
	movq	%rax, -104(%rbp)
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L162:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, -128(%rbp)
	jmp	.L152
.L166:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22222:
	.size	_ZN2v88internal8compiler19RawMachineAssembler6SwitchEPNS1_4NodeEPNS1_15RawMachineLabelEPKiPS6_m, .-_ZN2v88internal8compiler19RawMachineAssembler6SwitchEPNS1_4NodeEPNS1_15RawMachineLabelEPKiPS6_m
	.section	.text._ZN2v88internal8compiler19RawMachineAssembler6ReturnEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19RawMachineAssembler6ReturnEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19RawMachineAssembler6ReturnEPNS1_4NodeE, @function
_ZN2v88internal8compiler19RawMachineAssembler6ReturnEPNS1_4NodeE:
.LFB22223:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	72(%rdi), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$56, %rsp
	movq	%rsi, -72(%rbp)
	xorl	%esi, %esi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler21CommonOperatorBuilder13Int32ConstantEi@PLT
	movq	8(%rbx), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler5Graph16NewNodeUncheckedEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	152(%rbx), %rsi
	movq	16(%rbx), %rdi
	movq	%rax, %r12
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler8Schedule7AddNodeEPNS1_10BasicBlockEPNS1_4NodeE@PLT
	movq	%r12, %xmm0
	movl	$1, %esi
	movq	%r13, %rdi
	movhps	-72(%rbp), %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6ReturnEi@PLT
	movq	8(%rbx), %rdi
	xorl	%r8d, %r8d
	leaq	-64(%rbp), %rcx
	movq	%rax, %rsi
	movl	$2, %edx
	call	_ZN2v88internal8compiler5Graph16NewNodeUncheckedEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	152(%rbx), %rsi
	movq	16(%rbx), %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler8Schedule9AddReturnEPNS1_10BasicBlockEPNS1_4NodeE@PLT
	movq	$0, 152(%rbx)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L170
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L170:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22223:
	.size	_ZN2v88internal8compiler19RawMachineAssembler6ReturnEPNS1_4NodeE, .-_ZN2v88internal8compiler19RawMachineAssembler6ReturnEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19RawMachineAssembler6ReturnEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19RawMachineAssembler6ReturnEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler19RawMachineAssembler6ReturnEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler19RawMachineAssembler6ReturnEPNS1_4NodeES4_:
.LFB22224:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	72(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%r14, %rdi
	subq	$48, %rsp
	movq	%rsi, -72(%rbp)
	xorl	%esi, %esi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler21CommonOperatorBuilder13Int32ConstantEi@PLT
	movq	8(%rbx), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler5Graph16NewNodeUncheckedEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	152(%rbx), %rsi
	movq	16(%rbx), %rdi
	movq	%rax, %r12
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler8Schedule7AddNodeEPNS1_10BasicBlockEPNS1_4NodeE@PLT
	movq	%r12, %xmm0
	movq	%r14, %rdi
	movq	%r13, -48(%rbp)
	movhps	-72(%rbp), %xmm0
	movl	$2, %esi
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6ReturnEi@PLT
	movq	8(%rbx), %rdi
	xorl	%r8d, %r8d
	leaq	-64(%rbp), %rcx
	movq	%rax, %rsi
	movl	$3, %edx
	call	_ZN2v88internal8compiler5Graph16NewNodeUncheckedEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	152(%rbx), %rsi
	movq	16(%rbx), %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler8Schedule9AddReturnEPNS1_10BasicBlockEPNS1_4NodeE@PLT
	movq	$0, 152(%rbx)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L174
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L174:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22224:
	.size	_ZN2v88internal8compiler19RawMachineAssembler6ReturnEPNS1_4NodeES4_, .-_ZN2v88internal8compiler19RawMachineAssembler6ReturnEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler19RawMachineAssembler6ReturnEPNS1_4NodeES4_S4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19RawMachineAssembler6ReturnEPNS1_4NodeES4_S4_
	.type	_ZN2v88internal8compiler19RawMachineAssembler6ReturnEPNS1_4NodeES4_S4_, @function
_ZN2v88internal8compiler19RawMachineAssembler6ReturnEPNS1_4NodeES4_S4_:
.LFB22225:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %xmm2
	movq	%rdx, %xmm1
	punpcklqdq	%xmm2, %xmm1
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	72(%rdi), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$88, %rsp
	movq	%rsi, -88(%rbp)
	xorl	%esi, %esi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movaps	%xmm1, -112(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder13Int32ConstantEi@PLT
	movq	8(%rbx), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler5Graph16NewNodeUncheckedEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	152(%rbx), %rsi
	movq	16(%rbx), %rdi
	movq	%rax, %r12
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler8Schedule7AddNodeEPNS1_10BasicBlockEPNS1_4NodeE@PLT
	movdqa	-112(%rbp), %xmm1
	movq	%r12, %xmm0
	movq	%r13, %rdi
	movhps	-88(%rbp), %xmm0
	movl	$3, %esi
	movaps	%xmm0, -80(%rbp)
	movaps	%xmm1, -64(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6ReturnEi@PLT
	movq	8(%rbx), %rdi
	xorl	%r8d, %r8d
	leaq	-80(%rbp), %rcx
	movq	%rax, %rsi
	movl	$4, %edx
	call	_ZN2v88internal8compiler5Graph16NewNodeUncheckedEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	152(%rbx), %rsi
	movq	16(%rbx), %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler8Schedule9AddReturnEPNS1_10BasicBlockEPNS1_4NodeE@PLT
	movq	$0, 152(%rbx)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L178
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L178:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22225:
	.size	_ZN2v88internal8compiler19RawMachineAssembler6ReturnEPNS1_4NodeES4_S4_, .-_ZN2v88internal8compiler19RawMachineAssembler6ReturnEPNS1_4NodeES4_S4_
	.section	.text._ZN2v88internal8compiler19RawMachineAssembler6ReturnEPNS1_4NodeES4_S4_S4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19RawMachineAssembler6ReturnEPNS1_4NodeES4_S4_S4_
	.type	_ZN2v88internal8compiler19RawMachineAssembler6ReturnEPNS1_4NodeES4_S4_S4_, @function
_ZN2v88internal8compiler19RawMachineAssembler6ReturnEPNS1_4NodeES4_S4_S4_:
.LFB22226:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %xmm2
	movq	%rdx, %xmm1
	punpcklqdq	%xmm2, %xmm1
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	72(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%r8, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%r14, %rdi
	subq	$80, %rsp
	movq	%rsi, -88(%rbp)
	xorl	%esi, %esi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movaps	%xmm1, -112(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder13Int32ConstantEi@PLT
	movq	8(%rbx), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler5Graph16NewNodeUncheckedEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	152(%rbx), %rsi
	movq	16(%rbx), %rdi
	movq	%rax, %r12
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler8Schedule7AddNodeEPNS1_10BasicBlockEPNS1_4NodeE@PLT
	movdqa	-112(%rbp), %xmm1
	movq	%r12, %xmm0
	movq	%r14, %rdi
	movhps	-88(%rbp), %xmm0
	movl	$4, %esi
	movq	%r13, -48(%rbp)
	movaps	%xmm0, -80(%rbp)
	movaps	%xmm1, -64(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6ReturnEi@PLT
	movq	8(%rbx), %rdi
	xorl	%r8d, %r8d
	leaq	-80(%rbp), %rcx
	movq	%rax, %rsi
	movl	$5, %edx
	call	_ZN2v88internal8compiler5Graph16NewNodeUncheckedEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	152(%rbx), %rsi
	movq	16(%rbx), %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler8Schedule9AddReturnEPNS1_10BasicBlockEPNS1_4NodeE@PLT
	movq	$0, 152(%rbx)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L182
	addq	$80, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L182:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22226:
	.size	_ZN2v88internal8compiler19RawMachineAssembler6ReturnEPNS1_4NodeES4_S4_S4_, .-_ZN2v88internal8compiler19RawMachineAssembler6ReturnEPNS1_4NodeES4_S4_S4_
	.section	.text._ZN2v88internal8compiler19RawMachineAssembler6ReturnEiPPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19RawMachineAssembler6ReturnEiPPNS1_4NodeE
	.type	_ZN2v88internal8compiler19RawMachineAssembler6ReturnEiPPNS1_4NodeE, @function
_ZN2v88internal8compiler19RawMachineAssembler6ReturnEiPPNS1_4NodeE:
.LFB22227:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leal	1(%rsi), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	72(%rbx), %r15
	subq	$24, %rsp
	movq	%rdx, -64(%rbp)
	movabsq	$1152921504606846975, %rdx
	movl	%eax, -52(%rbp)
	cltq
	cmpq	%rdx, %rax
	leaq	0(,%rax,8), %rdi
	movq	$-1, %rax
	cmova	%rax, %rdi
	call	_Znam@PLT
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder13Int32ConstantEi@PLT
	movq	8(%rbx), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rax, %rsi
	xorl	%edx, %edx
	call	_ZN2v88internal8compiler5Graph16NewNodeUncheckedEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	152(%rbx), %rsi
	movq	16(%rbx), %rdi
	movq	%rax, %r14
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler8Schedule7AddNodeEPNS1_10BasicBlockEPNS1_4NodeE@PLT
	movq	%r14, (%r12)
	testl	%r13d, %r13d
	jle	.L185
	movq	-64(%rbp), %r9
	leal	-1(%r13), %eax
	leaq	8(%r12), %rdi
	leaq	8(,%rax,8), %rdx
	movq	%r9, %rsi
	call	memcpy@PLT
.L185:
	movl	%r13d, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6ReturnEi@PLT
	movq	8(%rbx), %rdi
	movl	-52(%rbp), %edx
	movq	%r12, %rcx
	movq	%rax, %rsi
	xorl	%r8d, %r8d
	call	_ZN2v88internal8compiler5Graph16NewNodeUncheckedEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%rbx), %rdi
	movq	152(%rbx), %rsi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler8Schedule9AddReturnEPNS1_10BasicBlockEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	$0, 152(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdaPv@PLT
	.cfi_endproc
.LFE22227:
	.size	_ZN2v88internal8compiler19RawMachineAssembler6ReturnEiPPNS1_4NodeE, .-_ZN2v88internal8compiler19RawMachineAssembler6ReturnEiPPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19RawMachineAssembler12PopAndReturnEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19RawMachineAssembler12PopAndReturnEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler19RawMachineAssembler12PopAndReturnEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler19RawMachineAssembler12PopAndReturnEPNS1_4NodeES4_:
.LFB22228:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	movl	$1, %esi
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	addq	$72, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6ReturnEi@PLT
	movq	8(%rbx), %rdi
	xorl	%r8d, %r8d
	leaq	-48(%rbp), %rcx
	movq	%rax, %rsi
	movl	$2, %edx
	call	_ZN2v88internal8compiler5Graph16NewNodeUncheckedEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	152(%rbx), %rsi
	movq	16(%rbx), %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler8Schedule9AddReturnEPNS1_10BasicBlockEPNS1_4NodeE@PLT
	movq	$0, 152(%rbx)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L191
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L191:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22228:
	.size	_ZN2v88internal8compiler19RawMachineAssembler12PopAndReturnEPNS1_4NodeES4_, .-_ZN2v88internal8compiler19RawMachineAssembler12PopAndReturnEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler19RawMachineAssembler12PopAndReturnEPNS1_4NodeES4_S4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19RawMachineAssembler12PopAndReturnEPNS1_4NodeES4_S4_
	.type	_ZN2v88internal8compiler19RawMachineAssembler12PopAndReturnEPNS1_4NodeES4_S4_, @function
_ZN2v88internal8compiler19RawMachineAssembler12PopAndReturnEPNS1_4NodeES4_S4_:
.LFB22229:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	movl	$2, %esi
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	addq	$72, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -48(%rbp)
	movq	%rcx, -32(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6ReturnEi@PLT
	movq	8(%rbx), %rdi
	xorl	%r8d, %r8d
	leaq	-48(%rbp), %rcx
	movq	%rax, %rsi
	movl	$3, %edx
	call	_ZN2v88internal8compiler5Graph16NewNodeUncheckedEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	152(%rbx), %rsi
	movq	16(%rbx), %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler8Schedule9AddReturnEPNS1_10BasicBlockEPNS1_4NodeE@PLT
	movq	$0, 152(%rbx)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L195
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L195:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22229:
	.size	_ZN2v88internal8compiler19RawMachineAssembler12PopAndReturnEPNS1_4NodeES4_S4_, .-_ZN2v88internal8compiler19RawMachineAssembler12PopAndReturnEPNS1_4NodeES4_S4_
	.section	.text._ZN2v88internal8compiler19RawMachineAssembler12PopAndReturnEPNS1_4NodeES4_S4_S4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19RawMachineAssembler12PopAndReturnEPNS1_4NodeES4_S4_S4_
	.type	_ZN2v88internal8compiler19RawMachineAssembler12PopAndReturnEPNS1_4NodeES4_S4_S4_, @function
_ZN2v88internal8compiler19RawMachineAssembler12PopAndReturnEPNS1_4NodeES4_S4_S4_:
.LFB22230:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	movl	$3, %esi
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	addq	$72, %rdi
	subq	$72, %rsp
	movq	%r8, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -64(%rbp)
	movq	%rcx, %xmm0
	movhps	-72(%rbp), %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6ReturnEi@PLT
	movq	8(%rbx), %rdi
	xorl	%r8d, %r8d
	leaq	-64(%rbp), %rcx
	movq	%rax, %rsi
	movl	$4, %edx
	call	_ZN2v88internal8compiler5Graph16NewNodeUncheckedEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	152(%rbx), %rsi
	movq	16(%rbx), %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler8Schedule9AddReturnEPNS1_10BasicBlockEPNS1_4NodeE@PLT
	movq	$0, 152(%rbx)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L199
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L199:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22230:
	.size	_ZN2v88internal8compiler19RawMachineAssembler12PopAndReturnEPNS1_4NodeES4_S4_S4_, .-_ZN2v88internal8compiler19RawMachineAssembler12PopAndReturnEPNS1_4NodeES4_S4_S4_
	.section	.text._ZN2v88internal8compiler19RawMachineAssembler12PopAndReturnEPNS1_4NodeES4_S4_S4_S4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19RawMachineAssembler12PopAndReturnEPNS1_4NodeES4_S4_S4_S4_
	.type	_ZN2v88internal8compiler19RawMachineAssembler12PopAndReturnEPNS1_4NodeES4_S4_S4_S4_, @function
_ZN2v88internal8compiler19RawMachineAssembler12PopAndReturnEPNS1_4NodeES4_S4_S4_S4_:
.LFB22231:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	movl	$4, %esi
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	addq	$72, %rdi
	subq	$72, %rsp
	movq	%r8, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -64(%rbp)
	movq	%rcx, %xmm0
	movhps	-72(%rbp), %xmm0
	movq	%r9, -32(%rbp)
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6ReturnEi@PLT
	movq	8(%rbx), %rdi
	xorl	%r8d, %r8d
	leaq	-64(%rbp), %rcx
	movq	%rax, %rsi
	movl	$5, %edx
	call	_ZN2v88internal8compiler5Graph16NewNodeUncheckedEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	152(%rbx), %rsi
	movq	16(%rbx), %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler8Schedule9AddReturnEPNS1_10BasicBlockEPNS1_4NodeE@PLT
	movq	$0, 152(%rbx)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L203
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L203:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22231:
	.size	_ZN2v88internal8compiler19RawMachineAssembler12PopAndReturnEPNS1_4NodeES4_S4_S4_S4_, .-_ZN2v88internal8compiler19RawMachineAssembler12PopAndReturnEPNS1_4NodeES4_S4_S4_S4_
	.section	.text._ZN2v88internal8compiler19RawMachineAssembler14AbortCSAAssertEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19RawMachineAssembler14AbortCSAAssertEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19RawMachineAssembler14AbortCSAAssertEPNS1_4NodeE, @function
_ZN2v88internal8compiler19RawMachineAssembler14AbortCSAAssertEPNS1_4NodeE:
.LFB22232:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler22MachineOperatorBuilder14AbortCSAAssertEv@PLT
	movq	8(%rbx), %rdi
	xorl	%r8d, %r8d
	leaq	-32(%rbp), %rcx
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r12, -32(%rbp)
	call	_ZN2v88internal8compiler5Graph16NewNodeUncheckedEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	152(%rbx), %rsi
	movq	16(%rbx), %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler8Schedule7AddNodeEPNS1_10BasicBlockEPNS1_4NodeE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L207
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L207:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22232:
	.size	_ZN2v88internal8compiler19RawMachineAssembler14AbortCSAAssertEPNS1_4NodeE, .-_ZN2v88internal8compiler19RawMachineAssembler14AbortCSAAssertEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19RawMachineAssembler10DebugBreakEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19RawMachineAssembler10DebugBreakEv
	.type	_ZN2v88internal8compiler19RawMachineAssembler10DebugBreakEv, @function
_ZN2v88internal8compiler19RawMachineAssembler10DebugBreakEv:
.LFB22233:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	32(%rdi), %rdi
	subq	$8, %rsp
	call	_ZN2v88internal8compiler22MachineOperatorBuilder10DebugBreakEv@PLT
	movq	8(%rbx), %rdi
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler5Graph16NewNodeUncheckedEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	152(%rbx), %rsi
	movq	16(%rbx), %rdi
	addq	$8, %rsp
	popq	%rbx
	movq	%rax, %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler8Schedule7AddNodeEPNS1_10BasicBlockEPNS1_4NodeE@PLT
	.cfi_endproc
.LFE22233:
	.size	_ZN2v88internal8compiler19RawMachineAssembler10DebugBreakEv, .-_ZN2v88internal8compiler19RawMachineAssembler10DebugBreakEv
	.section	.text._ZN2v88internal8compiler19RawMachineAssembler11UnreachableEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19RawMachineAssembler11UnreachableEv
	.type	_ZN2v88internal8compiler19RawMachineAssembler11UnreachableEv, @function
_ZN2v88internal8compiler19RawMachineAssembler11UnreachableEv:
.LFB22234:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	72(%rdi), %rdi
	subq	$8, %rsp
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5ThrowEv@PLT
	movq	8(%rbx), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rax, %rsi
	xorl	%edx, %edx
	call	_ZN2v88internal8compiler5Graph16NewNodeUncheckedEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	152(%rbx), %rsi
	movq	16(%rbx), %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler8Schedule8AddThrowEPNS1_10BasicBlockEPNS1_4NodeE@PLT
	movq	$0, 152(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22234:
	.size	_ZN2v88internal8compiler19RawMachineAssembler11UnreachableEv, .-_ZN2v88internal8compiler19RawMachineAssembler11UnreachableEv
	.section	.text._ZN2v88internal8compiler19RawMachineAssembler7CommentERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19RawMachineAssembler7CommentERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN2v88internal8compiler19RawMachineAssembler7CommentERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN2v88internal8compiler19RawMachineAssembler7CommentERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB22235:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	8(%rdi), %rax
	movq	8(%rsi), %rsi
	movq	(%rax), %rdi
	leaq	1(%rsi), %rdx
	addq	$8, %rsi
	andq	$-8, %rsi
	movq	16(%rdi), %r8
	movq	24(%rdi), %rax
	subq	%r8, %rax
	cmpq	%rax, %rsi
	ja	.L216
	addq	%r8, %rsi
	movq	%rsi, 16(%rdi)
.L214:
	movq	(%r12), %rsi
	movq	%r8, %rdi
	call	memcpy@PLT
	leaq	32(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder7CommentEPKc@PLT
	movq	8(%rbx), %rdi
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler5Graph16NewNodeUncheckedEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	152(%rbx), %rsi
	movq	16(%rbx), %rdi
	addq	$16, %rsp
	popq	%rbx
	movq	%rax, %rdx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler8Schedule7AddNodeEPNS1_10BasicBlockEPNS1_4NodeE@PLT
	.p2align 4,,10
	.p2align 3
.L216:
	.cfi_restore_state
	movq	%rdx, -24(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-24(%rbp), %rdx
	movq	%rax, %r8
	jmp	.L214
	.cfi_endproc
.LFE22235:
	.size	_ZN2v88internal8compiler19RawMachineAssembler7CommentERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN2v88internal8compiler19RawMachineAssembler7CommentERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text._ZN2v88internal8compiler19RawMachineAssembler12StaticAssertEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19RawMachineAssembler12StaticAssertEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19RawMachineAssembler12StaticAssertEPNS1_4NodeE, @function
_ZN2v88internal8compiler19RawMachineAssembler12StaticAssertEPNS1_4NodeE:
.LFB22236:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	addq	$72, %rdi
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12StaticAssertEv@PLT
	movq	8(%rbx), %rdi
	xorl	%r8d, %r8d
	leaq	-32(%rbp), %rcx
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r12, -32(%rbp)
	call	_ZN2v88internal8compiler5Graph16NewNodeUncheckedEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	152(%rbx), %rsi
	movq	16(%rbx), %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler8Schedule7AddNodeEPNS1_10BasicBlockEPNS1_4NodeE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L220
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L220:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22236:
	.size	_ZN2v88internal8compiler19RawMachineAssembler12StaticAssertEPNS1_4NodeE, .-_ZN2v88internal8compiler19RawMachineAssembler12StaticAssertEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19RawMachineAssembler5CallNEPNS1_14CallDescriptorEiPKPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19RawMachineAssembler5CallNEPNS1_14CallDescriptorEiPKPNS1_4NodeE
	.type	_ZN2v88internal8compiler19RawMachineAssembler5CallNEPNS1_14CallDescriptorEiPKPNS1_4NodeE, @function
_ZN2v88internal8compiler19RawMachineAssembler5CallNEPNS1_14CallDescriptorEiPKPNS1_4NodeE:
.LFB22237:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	leaq	72(%rdi), %rdi
	subq	$8, %rsp
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	movq	8(%rbx), %rdi
	movq	%r13, %rcx
	movl	%r12d, %edx
	movq	%rax, %rsi
	xorl	%r8d, %r8d
	call	_ZN2v88internal8compiler5Graph16NewNodeUncheckedEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	152(%rbx), %rsi
	movq	16(%rbx), %rdi
	movq	%rax, %rdx
	movq	%rax, %r12
	call	_ZN2v88internal8compiler8Schedule7AddNodeEPNS1_10BasicBlockEPNS1_4NodeE@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22237:
	.size	_ZN2v88internal8compiler19RawMachineAssembler5CallNEPNS1_14CallDescriptorEiPKPNS1_4NodeE, .-_ZN2v88internal8compiler19RawMachineAssembler5CallNEPNS1_14CallDescriptorEiPKPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19RawMachineAssembler19CallNWithFrameStateEPNS1_14CallDescriptorEiPKPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19RawMachineAssembler19CallNWithFrameStateEPNS1_14CallDescriptorEiPKPNS1_4NodeE
	.type	_ZN2v88internal8compiler19RawMachineAssembler19CallNWithFrameStateEPNS1_14CallDescriptorEiPKPNS1_4NodeE, @function
_ZN2v88internal8compiler19RawMachineAssembler19CallNWithFrameStateEPNS1_14CallDescriptorEiPKPNS1_4NodeE:
.LFB27606:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	leaq	72(%rdi), %rdi
	subq	$8, %rsp
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	movq	8(%rbx), %rdi
	movq	%r13, %rcx
	movl	%r12d, %edx
	movq	%rax, %rsi
	xorl	%r8d, %r8d
	call	_ZN2v88internal8compiler5Graph16NewNodeUncheckedEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	152(%rbx), %rsi
	movq	16(%rbx), %rdi
	movq	%rax, %rdx
	movq	%rax, %r12
	call	_ZN2v88internal8compiler8Schedule7AddNodeEPNS1_10BasicBlockEPNS1_4NodeE@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE27606:
	.size	_ZN2v88internal8compiler19RawMachineAssembler19CallNWithFrameStateEPNS1_14CallDescriptorEiPKPNS1_4NodeE, .-_ZN2v88internal8compiler19RawMachineAssembler19CallNWithFrameStateEPNS1_14CallDescriptorEiPKPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19RawMachineAssembler9TailCallNEPNS1_14CallDescriptorEiPKPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19RawMachineAssembler9TailCallNEPNS1_14CallDescriptorEiPKPNS1_4NodeE
	.type	_ZN2v88internal8compiler19RawMachineAssembler9TailCallNEPNS1_14CallDescriptorEiPKPNS1_4NodeE, @function
_ZN2v88internal8compiler19RawMachineAssembler9TailCallNEPNS1_14CallDescriptorEiPKPNS1_4NodeE:
.LFB22239:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	leaq	72(%rdi), %rdi
	subq	$8, %rsp
	call	_ZN2v88internal8compiler21CommonOperatorBuilder8TailCallEPKNS1_14CallDescriptorE@PLT
	movq	8(%rbx), %rdi
	movq	%r13, %rcx
	movl	%r12d, %edx
	movq	%rax, %rsi
	xorl	%r8d, %r8d
	call	_ZN2v88internal8compiler5Graph16NewNodeUncheckedEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	152(%rbx), %rsi
	movq	16(%rbx), %rdi
	movq	%rax, %rdx
	movq	%rax, %r12
	call	_ZN2v88internal8compiler8Schedule11AddTailCallEPNS1_10BasicBlockEPNS1_4NodeE@PLT
	movq	%r12, %rax
	movq	$0, 152(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22239:
	.size	_ZN2v88internal8compiler19RawMachineAssembler9TailCallNEPNS1_14CallDescriptorEiPKPNS1_4NodeE, .-_ZN2v88internal8compiler19RawMachineAssembler9TailCallNEPNS1_14CallDescriptorEiPKPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19RawMachineAssembler3UseEPNS1_15RawMachineLabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19RawMachineAssembler3UseEPNS1_15RawMachineLabelE
	.type	_ZN2v88internal8compiler19RawMachineAssembler3UseEPNS1_15RawMachineLabelE, @function
_ZN2v88internal8compiler19RawMachineAssembler3UseEPNS1_15RawMachineLabelE:
.LFB22250:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	movb	$1, 8(%rsi)
	testq	%rax, %rax
	je	.L233
	ret
	.p2align 4,,10
	.p2align 3
.L233:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %rdi
	call	_ZN2v88internal8compiler8Schedule13NewBasicBlockEv@PLT
	movq	%rax, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22250:
	.size	_ZN2v88internal8compiler19RawMachineAssembler3UseEPNS1_15RawMachineLabelE, .-_ZN2v88internal8compiler19RawMachineAssembler3UseEPNS1_15RawMachineLabelE
	.section	.text._ZN2v88internal8compiler19RawMachineAssembler11EnsureBlockEPNS1_15RawMachineLabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19RawMachineAssembler11EnsureBlockEPNS1_15RawMachineLabelE
	.type	_ZN2v88internal8compiler19RawMachineAssembler11EnsureBlockEPNS1_15RawMachineLabelE, @function
_ZN2v88internal8compiler19RawMachineAssembler11EnsureBlockEPNS1_15RawMachineLabelE:
.LFB22251:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	testq	%rax, %rax
	je	.L240
	ret
	.p2align 4,,10
	.p2align 3
.L240:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %rdi
	call	_ZN2v88internal8compiler8Schedule13NewBasicBlockEv@PLT
	movq	%rax, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22251:
	.size	_ZN2v88internal8compiler19RawMachineAssembler11EnsureBlockEPNS1_15RawMachineLabelE, .-_ZN2v88internal8compiler19RawMachineAssembler11EnsureBlockEPNS1_15RawMachineLabelE
	.section	.text._ZN2v88internal8compiler19RawMachineAssembler4BindEPNS1_15RawMachineLabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19RawMachineAssembler4BindEPNS1_15RawMachineLabelE
	.type	_ZN2v88internal8compiler19RawMachineAssembler4BindEPNS1_15RawMachineLabelE, @function
_ZN2v88internal8compiler19RawMachineAssembler4BindEPNS1_15RawMachineLabelE:
.LFB22252:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rsi), %rax
	movb	$1, 9(%rsi)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	testq	%rax, %rax
	je	.L244
.L242:
	movq	%rax, 152(%r12)
	movzbl	10(%rbx), %edx
	movb	%dl, 8(%rax)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L244:
	.cfi_restore_state
	movq	16(%rdi), %rdi
	call	_ZN2v88internal8compiler8Schedule13NewBasicBlockEv@PLT
	movq	%rax, (%rbx)
	jmp	.L242
	.cfi_endproc
.LFE22252:
	.size	_ZN2v88internal8compiler19RawMachineAssembler4BindEPNS1_15RawMachineLabelE, .-_ZN2v88internal8compiler19RawMachineAssembler4BindEPNS1_15RawMachineLabelE
	.section	.text._ZN2v88internal8compiler19RawMachineAssembler11InsideBlockEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19RawMachineAssembler11InsideBlockEv
	.type	_ZN2v88internal8compiler19RawMachineAssembler11InsideBlockEv, @function
_ZN2v88internal8compiler19RawMachineAssembler11InsideBlockEv:
.LFB22253:
	.cfi_startproc
	endbr64
	cmpq	$0, 152(%rdi)
	setne	%al
	ret
	.cfi_endproc
.LFE22253:
	.size	_ZN2v88internal8compiler19RawMachineAssembler11InsideBlockEv, .-_ZN2v88internal8compiler19RawMachineAssembler11InsideBlockEv
	.section	.text._ZN2v88internal8compiler19RawMachineAssembler12CurrentBlockEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19RawMachineAssembler12CurrentBlockEv
	.type	_ZN2v88internal8compiler19RawMachineAssembler12CurrentBlockEv, @function
_ZN2v88internal8compiler19RawMachineAssembler12CurrentBlockEv:
.LFB22254:
	.cfi_startproc
	endbr64
	movq	152(%rdi), %rax
	ret
	.cfi_endproc
.LFE22254:
	.size	_ZN2v88internal8compiler19RawMachineAssembler12CurrentBlockEv, .-_ZN2v88internal8compiler19RawMachineAssembler12CurrentBlockEv
	.section	.text._ZN2v88internal8compiler19RawMachineAssembler3PhiENS0_21MachineRepresentationEiPKPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19RawMachineAssembler3PhiENS0_21MachineRepresentationEiPKPNS1_4NodeE
	.type	_ZN2v88internal8compiler19RawMachineAssembler3PhiENS0_21MachineRepresentationEiPKPNS1_4NodeE, @function
_ZN2v88internal8compiler19RawMachineAssembler3PhiENS0_21MachineRepresentationEiPKPNS1_4NodeE:
.LFB22255:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leal	1(%rdx), %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movslq	%r15d, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	salq	$3, %r12
	movq	%r12, %rsi
	subq	$24, %rsp
	movq	8(%rdi), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %r13
	movq	24(%rdi), %rax
	subq	%r13, %rax
	cmpq	%rax, %r12
	ja	.L255
	addq	%r13, %rsi
	movq	%rsi, 16(%rdi)
.L249:
	movq	%r12, %rdx
	subq	$8, %rdx
	je	.L250
	movq	%rcx, %rsi
	movq	%r13, %rdi
	movl	%r8d, -52(%rbp)
	call	memmove@PLT
	movl	-52(%rbp), %r8d
.L250:
	movq	8(%rbx), %rax
	leaq	72(%rbx), %rdi
	movl	%r14d, %edx
	movl	%r8d, %esi
	movq	8(%rax), %rax
	movq	%rax, -8(%r13,%r12)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	movq	8(%rbx), %rdi
	movq	%r13, %rcx
	movl	%r15d, %edx
	movq	%rax, %rsi
	xorl	%r8d, %r8d
	call	_ZN2v88internal8compiler5Graph16NewNodeUncheckedEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	152(%rbx), %rsi
	movq	16(%rbx), %rdi
	movq	%rax, %rdx
	movq	%rax, %r12
	call	_ZN2v88internal8compiler8Schedule7AddNodeEPNS1_10BasicBlockEPNS1_4NodeE@PLT
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L255:
	.cfi_restore_state
	movq	%rcx, -64(%rbp)
	movl	%r8d, -52(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-52(%rbp), %r8d
	movq	-64(%rbp), %rcx
	movq	%rax, %r13
	jmp	.L249
	.cfi_endproc
.LFE22255:
	.size	_ZN2v88internal8compiler19RawMachineAssembler3PhiENS0_21MachineRepresentationEiPKPNS1_4NodeE, .-_ZN2v88internal8compiler19RawMachineAssembler3PhiENS0_21MachineRepresentationEiPKPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19RawMachineAssembler14AppendPhiInputEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19RawMachineAssembler14AppendPhiInputEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler19RawMachineAssembler14AppendPhiInputEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler19RawMachineAssembler14AppendPhiInputEPNS1_4NodeES4_:
.LFB22256:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	movzbl	23(%r12), %edx
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	addq	$72, %rdi
	movq	(%rsi), %rsi
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L258
	movq	32(%r12), %rax
	movl	8(%rax), %edx
.L258:
	call	_ZN2v88internal8compiler21CommonOperatorBuilder16ResizeMergeOrPhiEPKNS1_8OperatorEi@PLT
	movzbl	23(%r12), %edx
	movq	%rax, %r14
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L260
	movq	32(%r12), %rax
	movl	8(%rax), %edx
.L260:
	movq	8(%rbx), %rax
	movq	%r12, %rdi
	movq	%r13, %rcx
	subl	$1, %edx
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	popq	%rbx
	movq	%r14, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	.cfi_endproc
.LFE22256:
	.size	_ZN2v88internal8compiler19RawMachineAssembler14AppendPhiInputEPNS1_4NodeES4_, .-_ZN2v88internal8compiler19RawMachineAssembler14AppendPhiInputEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler19RawMachineAssembler7AddNodeEPKNS1_8OperatorEiPKPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19RawMachineAssembler7AddNodeEPKNS1_8OperatorEiPKPNS1_4NodeE
	.type	_ZN2v88internal8compiler19RawMachineAssembler7AddNodeEPKNS1_8OperatorEiPKPNS1_4NodeE, @function
_ZN2v88internal8compiler19RawMachineAssembler7AddNodeEPKNS1_8OperatorEiPKPNS1_4NodeE:
.LFB22257:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	8(%rdi), %rdi
	call	_ZN2v88internal8compiler5Graph16NewNodeUncheckedEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	152(%rbx), %rsi
	movq	16(%rbx), %rdi
	movq	%rax, %r12
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler8Schedule7AddNodeEPNS1_10BasicBlockEPNS1_4NodeE@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22257:
	.size	_ZN2v88internal8compiler19RawMachineAssembler7AddNodeEPKNS1_8OperatorEiPKPNS1_4NodeE, .-_ZN2v88internal8compiler19RawMachineAssembler7AddNodeEPKNS1_8OperatorEiPKPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19RawMachineAssembler8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19RawMachineAssembler8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeE
	.type	_ZN2v88internal8compiler19RawMachineAssembler8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeE, @function
_ZN2v88internal8compiler19RawMachineAssembler8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeE:
.LFB22258:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	xorl	%r8d, %r8d
	jmp	_ZN2v88internal8compiler5Graph16NewNodeUncheckedEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	.cfi_endproc
.LFE22258:
	.size	_ZN2v88internal8compiler19RawMachineAssembler8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeE, .-_ZN2v88internal8compiler19RawMachineAssembler8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler15RawMachineLabelD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15RawMachineLabelD2Ev
	.type	_ZN2v88internal8compiler15RawMachineLabelD2Ev, @function
_ZN2v88internal8compiler15RawMachineLabelD2Ev:
.LFB22260:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE22260:
	.size	_ZN2v88internal8compiler15RawMachineLabelD2Ev, .-_ZN2v88internal8compiler15RawMachineLabelD2Ev
	.globl	_ZN2v88internal8compiler15RawMachineLabelD1Ev
	.set	_ZN2v88internal8compiler15RawMachineLabelD1Ev,_ZN2v88internal8compiler15RawMachineLabelD2Ev
	.section	.rodata._ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_.str1.1,"aMS",@progbits,1
.LC4:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.type	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, @function
_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_:
.LFB25529:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L280
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L276
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L281
.L268:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L275:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L282
	testq	%r13, %r13
	jg	.L271
	testq	%r9, %r9
	jne	.L274
.L272:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L282:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L271
.L274:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L272
	.p2align 4,,10
	.p2align 3
.L281:
	testq	%rsi, %rsi
	jne	.L269
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L275
	.p2align 4,,10
	.p2align 3
.L271:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L272
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L276:
	movl	$8, %r14d
	jmp	.L268
.L280:
	leaq	.LC4(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L269:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L268
	.cfi_endproc
.LFE25529:
	.size	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, .-_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.section	.text._ZN2v88internal8compiler19RawMachineAssembler26CreateNodeFromPredecessorsERKSt6vectorIPNS1_10BasicBlockESaIS5_EERKS3_IPNS1_4NodeESaISB_EEPKNS1_8OperatorESF_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19RawMachineAssembler26CreateNodeFromPredecessorsERKSt6vectorIPNS1_10BasicBlockESaIS5_EERKS3_IPNS1_4NodeESaISB_EEPKNS1_8OperatorESF_
	.type	_ZN2v88internal8compiler19RawMachineAssembler26CreateNodeFromPredecessorsERKSt6vectorIPNS1_10BasicBlockESaIS5_EERKS3_IPNS1_4NodeESaISB_EEPKNS1_8OperatorESF_, @function
_ZN2v88internal8compiler19RawMachineAssembler26CreateNodeFromPredecessorsERKSt6vectorIPNS1_10BasicBlockESaIS5_EERKS3_IPNS1_4NodeESaISB_EEPKNS1_8OperatorESF_:
.LFB22214:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rsi), %r8
	movq	(%rsi), %r13
	movq	%rdi, -112(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r8, %rax
	subq	%r13, %rax
	cmpq	$8, %rax
	je	.L301
	pxor	%xmm0, %xmm0
	movq	$0, -64(%rbp)
	movq	%rcx, %r12
	movaps	%xmm0, -80(%rbp)
	cmpq	%r8, %r13
	je	.L295
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	leaq	-80(%rbp), %rbx
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L302:
	movq	(%rdx), %rax
	addq	$8, %r13
	movq	%rax, (%rsi)
	movq	-72(%rbp), %rax
	leaq	8(%rax), %rsi
	movq	%rsi, -72(%rbp)
	cmpq	%r13, %r8
	je	.L286
.L303:
	movq	-64(%rbp), %rcx
.L289:
	movq	0(%r13), %rax
	movq	160(%rax), %rdx
	movq	(%r14), %rax
	leaq	(%rax,%rdx,8), %rdx
	cmpq	%rcx, %rsi
	jne	.L302
	movq	%rbx, %rdi
	movq	%r8, -104(%rbp)
	addq	$8, %r13
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	movq	-104(%rbp), %r8
	movq	-72(%rbp), %rsi
	cmpq	%r13, %r8
	jne	.L303
	.p2align 4,,10
	.p2align 3
.L286:
	movq	(%r15), %r13
	movq	8(%r15), %r14
	cmpq	%r14, %r13
	je	.L290
	leaq	-88(%rbp), %r15
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L304:
	movq	%rax, (%rsi)
	movq	-72(%rbp), %rax
	addq	$8, %r13
	leaq	8(%rax), %rsi
	movq	%rsi, -72(%rbp)
	cmpq	%r13, %r14
	je	.L290
.L293:
	movq	0(%r13), %rax
	movq	%rax, -88(%rbp)
	cmpq	%rsi, -64(%rbp)
	jne	.L304
	leaq	-80(%rbp), %rdi
	movq	%r15, %rdx
	addq	$8, %r13
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	movq	-72(%rbp), %rsi
	cmpq	%r13, %r14
	jne	.L293
	.p2align 4,,10
	.p2align 3
.L290:
	movq	-80(%rbp), %rcx
	movq	-112(%rbp), %rax
	movq	%rsi, %rdx
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	movq	8(%rax), %rdi
	subq	%rcx, %rdx
	sarq	$3, %rdx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L283
	movq	%rax, -104(%rbp)
	call	_ZdlPv@PLT
	movq	-104(%rbp), %rax
.L283:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L305
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L301:
	.cfi_restore_state
	movq	0(%r13), %rax
	movq	160(%rax), %rdx
	movq	(%r14), %rax
	movq	(%rax,%rdx,8), %rax
	jmp	.L283
	.p2align 4,,10
	.p2align 3
.L295:
	xorl	%esi, %esi
	jmp	.L286
.L305:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22214:
	.size	_ZN2v88internal8compiler19RawMachineAssembler26CreateNodeFromPredecessorsERKSt6vectorIPNS1_10BasicBlockESaIS5_EERKS3_IPNS1_4NodeESaISB_EEPKNS1_8OperatorESF_, .-_ZN2v88internal8compiler19RawMachineAssembler26CreateNodeFromPredecessorsERKSt6vectorIPNS1_10BasicBlockESaIS5_EERKS3_IPNS1_4NodeESaISB_EEPKNS1_8OperatorESF_
	.section	.text._ZN2v88internal8compiler19RawMachineAssembler13MakePhiBinaryEPNS1_4NodeEiS4_S4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19RawMachineAssembler13MakePhiBinaryEPNS1_4NodeEiS4_S4_
	.type	_ZN2v88internal8compiler19RawMachineAssembler13MakePhiBinaryEPNS1_4NodeEiS4_S4_, @function
_ZN2v88internal8compiler19RawMachineAssembler13MakePhiBinaryEPNS1_4NodeEiS4_S4_:
.LFB22215:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -144(%rbp)
	movq	(%rsi), %rdi
	movq	%rcx, -120(%rbp)
	movq	%r8, -128(%rbp)
	movl	20(%rdi), %r14d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$2, %r14d
	je	.L306
	movl	%edx, %r13d
	movq	%rsi, %r12
	call	_ZN2v88internal8compiler19PhiRepresentationOfEPKNS1_8OperatorE@PLT
	movb	%al, -129(%rbp)
	movl	%r14d, %eax
	subl	%r13d, %eax
	movl	%eax, -152(%rbp)
	cmpl	$1, %r13d
	je	.L397
	pxor	%xmm0, %xmm0
	movq	$0, -96(%rbp)
	movaps	%xmm0, -112(%rbp)
	testl	%r13d, %r13d
	jle	.L352
	movl	%r14d, -176(%rbp)
	xorl	%ebx, %ebx
	jmp	.L319
	.p2align 4,,10
	.p2align 3
.L399:
	movq	%rax, (%rdx)
	movq	-104(%rbp), %rax
	addl	$1, %ebx
	leaq	8(%rax), %r15
	movq	%r15, -104(%rbp)
	cmpl	%ebx, %r13d
	je	.L398
.L319:
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	-104(%rbp), %rdx
	movq	%rax, %r15
	cmpq	-96(%rbp), %rdx
	jne	.L399
	movabsq	$1152921504606846975, %rcx
	movq	-112(%rbp), %r8
	subq	%r8, %rdx
	movq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L327
	testq	%rax, %rax
	je	.L353
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L400
.L314:
	movq	%r14, %rdi
	movq	%rdx, -168(%rbp)
	movq	%r8, -160(%rbp)
	call	_Znwm@PLT
	movq	-160(%rbp), %r8
	movq	-168(%rbp), %rdx
	movq	%rax, %rcx
	addq	%rax, %r14
.L315:
	movq	%r15, (%rcx,%rdx)
	leaq	8(%rcx,%rdx), %r15
	testq	%rdx, %rdx
	jg	.L401
	testq	%r8, %r8
	jne	.L317
.L318:
	movq	%rcx, %xmm0
	movq	%r15, %xmm1
	addl	$1, %ebx
	movq	%r14, -96(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -112(%rbp)
	cmpl	%ebx, %r13d
	jne	.L319
	.p2align 4,,10
	.p2align 3
.L398:
	movl	-176(%rbp), %r14d
	cmpq	%r15, -96(%rbp)
	je	.L310
	movq	-120(%rbp), %rax
	movq	%rax, (%r15)
	movq	-104(%rbp), %rax
	leaq	8(%rax), %rbx
	movq	%rbx, -104(%rbp)
.L320:
	movq	-144(%rbp), %rax
	movq	-112(%rbp), %rcx
	movl	%r13d, %edx
	movzbl	-129(%rbp), %esi
	leaq	72(%rax), %rdi
	movq	%rcx, -160(%rbp)
	movq	8(%rax), %r15
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	movq	-160(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movq	%rax, %rsi
	subq	%rcx, %rbx
	movq	%rbx, %rdx
	sarq	$3, %rdx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L309
	call	_ZdlPv@PLT
.L309:
	cmpl	$1, -152(%rbp)
	je	.L402
.L322:
	pxor	%xmm0, %xmm0
	movq	$0, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	cmpl	%r14d, %r13d
	jge	.L356
	movq	%rbx, -176(%rbp)
	jmp	.L333
	.p2align 4,,10
	.p2align 3
.L404:
	movq	%rax, (%rdx)
	movq	-72(%rbp), %rax
	addl	$1, %r13d
	leaq	8(%rax), %r15
	movq	%r15, -72(%rbp)
	cmpl	%r14d, %r13d
	je	.L403
.L333:
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	-72(%rbp), %rdx
	movq	%rax, %r15
	cmpq	-64(%rbp), %rdx
	jne	.L404
	movabsq	$1152921504606846975, %rcx
	movq	-80(%rbp), %r8
	subq	%r8, %rdx
	movq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L327
	testq	%rax, %rax
	je	.L357
	movabsq	$9223372036854775800, %rbx
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L405
.L328:
	movq	%rbx, %rdi
	movq	%rdx, -168(%rbp)
	movq	%r8, -160(%rbp)
	call	_Znwm@PLT
	movq	-160(%rbp), %r8
	movq	-168(%rbp), %rdx
	movq	%rax, %rcx
	addq	%rax, %rbx
.L329:
	movq	%r15, (%rcx,%rdx)
	leaq	8(%rcx,%rdx), %r15
	testq	%rdx, %rdx
	jg	.L406
	testq	%r8, %r8
	jne	.L331
.L332:
	movq	%rcx, %xmm0
	movq	%r15, %xmm2
	addl	$1, %r13d
	movq	%rbx, -64(%rbp)
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -80(%rbp)
	cmpl	%r14d, %r13d
	jne	.L333
	.p2align 4,,10
	.p2align 3
.L403:
	movq	-176(%rbp), %rbx
	cmpq	-64(%rbp), %r15
	je	.L324
	movq	-128(%rbp), %rax
	movq	%rax, (%r15)
	movq	-72(%rbp), %rax
	leaq	8(%rax), %r14
	movq	%r14, -72(%rbp)
.L334:
	movq	-144(%rbp), %rax
	movq	-80(%rbp), %rcx
	movl	-152(%rbp), %edx
	movzbl	-129(%rbp), %esi
	leaq	72(%rax), %r13
	movq	8(%rax), %r15
	movq	%rcx, -144(%rbp)
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	movq	%r14, %rdx
	movq	%r15, %rdi
	xorl	%r8d, %r8d
	movq	-144(%rbp), %rcx
	movq	%rax, %rsi
	subq	%rcx, %rdx
	sarq	$3, %rdx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-80(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L323
	call	_ZdlPv@PLT
.L323:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler4Node14TrimInputCountEi@PLT
	movzbl	23(%r12), %edx
	leaq	32(%r12), %rax
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L336
	movq	32(%r12), %rdi
	movq	%rax, %rdx
	movq	%r12, %rsi
	cmpq	%rdi, %rbx
	je	.L338
.L337:
	subq	$24, %rsi
	testq	%rdi, %rdi
	je	.L340
	movq	%rax, -160(%rbp)
	movq	%rdx, -152(%rbp)
	movq	%rsi, -144(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-160(%rbp), %rax
	movq	-152(%rbp), %rdx
	movq	-144(%rbp), %rsi
.L340:
	movq	%rbx, (%rdx)
	testq	%rbx, %rbx
	je	.L341
	movq	%rbx, %rdi
	movq	%rax, -144(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movq	-144(%rbp), %rax
.L341:
	movzbl	23(%r12), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L407
.L338:
	movq	8(%rax), %rdi
	cmpq	%rdi, %r15
	je	.L342
	leaq	8(%rax), %rdx
	movq	%r12, %rsi
.L343:
	leaq	-48(%rsi), %rbx
	testq	%rdi, %rdi
	je	.L345
	movq	%rbx, %rsi
	movq	%rax, -152(%rbp)
	movq	%rdx, -144(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-152(%rbp), %rax
	movq	-144(%rbp), %rdx
.L345:
	movq	%r15, (%rdx)
	testq	%r15, %r15
	je	.L346
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	%rax, -144(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movq	-144(%rbp), %rax
.L346:
	movzbl	23(%r12), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L408
.L342:
	movq	16(%rax), %rdi
	cmpq	%rdi, %r14
	je	.L349
	leaq	16(%rax), %rbx
	movq	%r12, %rsi
.L348:
	leaq	-72(%rsi), %r15
	testq	%rdi, %rdi
	je	.L350
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L350:
	movq	%r14, (%rbx)
	testq	%r14, %r14
	je	.L349
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L349:
	movzbl	-129(%rbp), %esi
	movq	%r13, %rdi
	movl	$2, %edx
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
.L306:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L409
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L401:
	.cfi_restore_state
	movq	%r8, %rsi
	movq	%rcx, %rdi
	movq	%r8, -160(%rbp)
	call	memmove@PLT
	movq	-160(%rbp), %r8
	movq	%rax, %rcx
.L317:
	movq	%r8, %rdi
	movq	%rcx, -160(%rbp)
	call	_ZdlPv@PLT
	movq	-160(%rbp), %rcx
	jmp	.L318
	.p2align 4,,10
	.p2align 3
.L406:
	movq	%r8, %rsi
	movq	%rcx, %rdi
	movq	%r8, -160(%rbp)
	call	memmove@PLT
	movq	-160(%rbp), %r8
	movq	%rax, %rcx
.L331:
	movq	%r8, %rdi
	movq	%rcx, -160(%rbp)
	call	_ZdlPv@PLT
	movq	-160(%rbp), %rcx
	jmp	.L332
	.p2align 4,,10
	.p2align 3
.L400:
	testq	%rcx, %rcx
	jne	.L410
	xorl	%r14d, %r14d
	xorl	%ecx, %ecx
	jmp	.L315
	.p2align 4,,10
	.p2align 3
.L405:
	testq	%rcx, %rcx
	jne	.L411
	xorl	%ebx, %ebx
	xorl	%ecx, %ecx
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L353:
	movl	$8, %r14d
	jmp	.L314
	.p2align 4,,10
	.p2align 3
.L357:
	movl	$8, %ebx
	jmp	.L328
	.p2align 4,,10
	.p2align 3
.L336:
	movq	32(%r12), %rsi
	movq	16(%rsi), %rdi
	leaq	16(%rsi), %rdx
	cmpq	%rdi, %rbx
	jne	.L337
	movq	8(%rdx), %rdi
	cmpq	%rdi, %r15
	je	.L344
.L412:
	addq	$8, %rdx
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L408:
	movq	32(%r12), %rsi
	leaq	16(%rsi), %rdx
.L344:
	movq	16(%rdx), %rdi
	cmpq	%rdi, %r14
	je	.L349
	leaq	16(%rdx), %rbx
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L352:
	xorl	%r15d, %r15d
.L310:
	leaq	-120(%rbp), %rdx
	leaq	-112(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	movq	-104(%rbp), %rbx
	jmp	.L320
	.p2align 4,,10
	.p2align 3
.L407:
	movq	32(%r12), %rsi
	movq	24(%rsi), %rdi
	leaq	16(%rsi), %rdx
	cmpq	%rdi, %r15
	jne	.L412
	jmp	.L344
	.p2align 4,,10
	.p2align 3
.L397:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	cmpl	$1, -152(%rbp)
	movq	%rax, %rbx
	jne	.L322
.L402:
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	-144(%rbp), %r13
	movq	%rax, %r15
	addq	$72, %r13
	jmp	.L323
	.p2align 4,,10
	.p2align 3
.L356:
	xorl	%r15d, %r15d
.L324:
	leaq	-128(%rbp), %rdx
	leaq	-80(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	movq	-72(%rbp), %r14
	jmp	.L334
.L327:
	leaq	.LC4(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L409:
	call	__stack_chk_fail@PLT
.L410:
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rcx
	cmova	%rax, %rcx
	leaq	0(,%rcx,8), %r14
	jmp	.L314
.L411:
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rcx
	cmova	%rax, %rcx
	leaq	0(,%rcx,8), %rbx
	jmp	.L328
	.cfi_endproc
.LFE22215:
	.size	_ZN2v88internal8compiler19RawMachineAssembler13MakePhiBinaryEPNS1_4NodeEiS4_S4_, .-_ZN2v88internal8compiler19RawMachineAssembler13MakePhiBinaryEPNS1_4NodeEiS4_S4_
	.section	.text._ZNSt6vectorIPN2v88internal8compiler10BasicBlockESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPN2v88internal8compiler10BasicBlockESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal8compiler10BasicBlockESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.type	_ZNSt6vectorIPN2v88internal8compiler10BasicBlockESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, @function
_ZNSt6vectorIPN2v88internal8compiler10BasicBlockESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_:
.LFB25552:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L427
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L423
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L428
.L415:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L422:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L429
	testq	%r13, %r13
	jg	.L418
	testq	%r9, %r9
	jne	.L421
.L419:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L429:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L418
.L421:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L419
	.p2align 4,,10
	.p2align 3
.L428:
	testq	%rsi, %rsi
	jne	.L416
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L422
	.p2align 4,,10
	.p2align 3
.L418:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L419
	jmp	.L421
	.p2align 4,,10
	.p2align 3
.L423:
	movl	$8, %r14d
	jmp	.L415
.L427:
	leaq	.LC4(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L416:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L415
	.cfi_endproc
.LFE25552:
	.size	_ZNSt6vectorIPN2v88internal8compiler10BasicBlockESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, .-_ZNSt6vectorIPN2v88internal8compiler10BasicBlockESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.section	.text._ZN2v88internal8compiler19RawMachineAssembler17MakeReschedulableEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19RawMachineAssembler17MakeReschedulableEv
	.type	_ZN2v88internal8compiler19RawMachineAssembler17MakeReschedulableEv, @function
_ZN2v88internal8compiler19RawMachineAssembler17MakeReschedulableEv:
.LFB22185:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$360, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	24(%rax), %r12
	movq	%r12, %r13
	subq	16(%rax), %r13
	movq	%r13, %rdx
	sarq	$3, %rdx
	testq	%r13, %r13
	js	.L433
	pxor	%xmm0, %xmm0
	movq	%rdi, %rbx
	movq	$0, -288(%rbp)
	movaps	%xmm0, -304(%rbp)
	testq	%rdx, %rdx
	je	.L432
	movq	%r13, %rdi
	call	_Znwm@PLT
	movq	%r13, %rdx
	xorl	%esi, %esi
	leaq	(%rax,%r13), %r12
	movq	%rax, %rdi
	movq	%rax, -304(%rbp)
	movq	%r12, -288(%rbp)
	call	memset@PLT
	movq	16(%rbx), %rax
	movq	%r12, %rdx
	movq	24(%rax), %r12
.L539:
	subq	16(%rax), %r12
	movq	%rdx, -296(%rbp)
	movq	%r12, %rdx
	sarq	$3, %rdx
	testq	%r12, %r12
	js	.L433
	movq	$0, -256(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -272(%rbp)
	testq	%rdx, %rdx
	je	.L434
	movq	%r12, %rdi
	call	_Znwm@PLT
	movq	%r12, %rdx
	xorl	%esi, %esi
	leaq	(%rax,%r12), %r13
	movq	%rax, %rdi
	movq	%rax, -272(%rbp)
	movq	%r13, -256(%rbp)
	call	memset@PLT
	movq	16(%rbx), %rax
.L538:
	movq	88(%rax), %rcx
	movq	80(%rax), %r14
	pxor	%xmm0, %xmm0
	movq	%r13, -264(%rbp)
	movq	$0, -224(%rbp)
	movq	$0, -192(%rbp)
	movq	%rcx, -328(%rbp)
	movaps	%xmm0, -240(%rbp)
	movaps	%xmm0, -208(%rbp)
	cmpq	%rcx, %r14
	je	.L620
	leaq	-240(%rbp), %rcx
	movq	%r14, -312(%rbp)
	movq	%rcx, -352(%rbp)
	movq	-312(%rbp), %rcx
	movq	$0, -344(%rbp)
	movq	$0, -376(%rbp)
	movq	(%rcx), %r12
	movq	$0, -400(%rbp)
	cmpq	104(%rax), %r12
	je	.L621
	.p2align 4,,10
	.p2align 3
.L438:
	cmpq	112(%rax), %r12
	je	.L622
	cmpq	$0, 40(%r12)
	je	.L442
	movq	8(%rbx), %r13
	leaq	72(%rbx), %r14
	movl	$2, %esi
	movq	%r14, %rdi
	movq	8(%r13), %r15
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4LoopEi@PLT
	leaq	-80(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	movq	%r15, %xmm0
	movl	$2, %edx
	movq	%rax, %rsi
	movq	%rcx, -336(%rbp)
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%rbx), %r13
	movq	%r14, %rdi
	movl	$2, %esi
	movq	%rax, -112(%rbp)
	movq	%rax, %r15
	movq	8(%r13), %xmm0
	movq	%xmm0, -320(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9EffectPhiEi@PLT
	xorl	%r8d, %r8d
	movl	$3, %edx
	movq	%r13, %rdi
	movq	-320(%rbp), %xmm0
	movq	-336(%rbp), %rcx
	movq	%rax, %rsi
	movq	%r15, -64(%rbp)
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r14, %rdi
	movq	8(%rbx), %r13
	movq	%rax, %r15
	movq	-112(%rbp), %rax
	movq	%rax, -320(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9TerminateEv@PLT
	movq	%r15, %xmm0
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	movq	-336(%rbp), %rcx
	movhps	-320(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%rbx), %rdi
	movq	%r14, %rsi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14NodeProperties17MergeControlToEndEPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS1_4NodeE@PLT
	movq	-376(%rbp), %rax
	movq	-344(%rbp), %rcx
	movq	-112(%rbp), %r14
	cmpq	%rcx, %rax
	je	.L443
	movq	%r12, %xmm0
	movq	%r14, %xmm6
	movq	%r15, 16(%rax)
	addq	$24, %rax
	punpcklqdq	%xmm6, %xmm0
	movups	%xmm0, -24(%rax)
	movq	%rax, -376(%rbp)
.L439:
	movq	72(%r12), %r8
	movq	80(%r12), %rax
	cmpq	%r8, %rax
	je	.L463
	movq	%r12, -320(%rbp)
	movq	%rax, %r14
	movq	%r15, %r12
	movq	%r8, %r15
	jmp	.L473
	.p2align 4,,10
	.p2align 3
.L624:
	subl	$35, %edx
	cmpl	$1, %edx
	jbe	.L464
	testl	%esi, %esi
	jle	.L469
	movq	8(%rbx), %rax
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11AppendInputEPNS0_4ZoneEPS2_@PLT
	movq	0(%r13), %rax
.L469:
	movl	28(%rax), %esi
	testl	%esi, %esi
	jle	.L470
	movq	8(%rbx), %rax
	movq	-112(%rbp), %rdx
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11AppendInputEPNS0_4ZoneEPS2_@PLT
	movq	0(%r13), %rax
.L470:
	movl	40(%rax), %r9d
	cmpb	$0, 36(%rax)
	cmovne	%r13, %r12
	testl	%r9d, %r9d
	jle	.L472
	movq	%r13, -112(%rbp)
.L472:
	addq	$8, %r15
	cmpq	%r15, %r14
	je	.L623
.L473:
	movq	(%r15), %r13
	movq	0(%r13), %rax
	movzwl	16(%rax), %edx
	movl	24(%rax), %esi
	leal	-4(%rdx), %edi
	cmpl	$5, %edi
	ja	.L624
.L464:
	testl	%esi, %esi
	jle	.L467
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler14NodeProperties18ReplaceEffectInputEPNS1_4NodeES4_i@PLT
	movq	0(%r13), %rax
.L467:
	movl	28(%rax), %edi
	testl	%edi, %edi
	jle	.L470
	movq	-112(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler14NodeProperties19ReplaceControlInputEPNS1_4NodeES4_i@PLT
	movq	0(%r13), %rax
	jmp	.L470
	.p2align 4,,10
	.p2align 3
.L623:
	movq	%r12, %r15
	movq	-320(%rbp), %r12
.L463:
	cmpb	$0, 8(%r12)
	jne	.L625
.L474:
	movq	56(%r12), %r13
	testq	%r13, %r13
	je	.L475
	movq	0(%r13), %rax
	movzwl	16(%rax), %edx
	movl	24(%rax), %ecx
	leal	-4(%rdx), %esi
	cmpl	$5, %esi
	jbe	.L479
	subl	$35, %edx
	cmpl	$1, %edx
	jbe	.L479
	testl	%ecx, %ecx
	jle	.L481
	movq	8(%rbx), %rax
	movq	%r15, %rdx
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11AppendInputEPNS0_4ZoneEPS2_@PLT
	movq	0(%r13), %rax
.L481:
	movl	28(%rax), %edx
	testl	%edx, %edx
	jle	.L482
	movq	8(%rbx), %rax
	movq	-112(%rbp), %rdx
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11AppendInputEPNS0_4ZoneEPS2_@PLT
	movq	0(%r13), %rax
.L482:
	movl	40(%rax), %r8d
	cmpb	$0, 36(%rax)
	cmovne	%r13, %r15
	testl	%r8d, %r8d
	jle	.L475
	movq	%r13, -112(%rbp)
.L475:
	movq	160(%r12), %rdx
	movq	-272(%rbp), %rax
	addq	$8, -312(%rbp)
	movq	%r15, (%rax,%rdx,8)
	movq	-304(%rbp), %rax
	movq	160(%r12), %rdx
	movq	-112(%rbp), %rcx
	movq	%rcx, (%rax,%rdx,8)
	movq	-312(%rbp), %rax
	cmpq	%rax, -328(%rbp)
	je	.L484
	movq	-312(%rbp), %rcx
	movq	16(%rbx), %rax
	movq	(%rcx), %r12
	cmpq	104(%rax), %r12
	jne	.L438
.L621:
	movq	8(%rbx), %rax
	movq	8(%rax), %r15
	movq	%r15, -112(%rbp)
	jmp	.L439
	.p2align 4,,10
	.p2align 3
.L479:
	testl	%ecx, %ecx
	jle	.L478
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler14NodeProperties18ReplaceEffectInputEPNS1_4NodeES4_i@PLT
	movq	0(%r13), %rax
.L478:
	movl	28(%rax), %ecx
	testl	%ecx, %ecx
	jle	.L482
	movq	-112(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler14NodeProperties19ReplaceControlInputEPNS1_4NodeES4_i@PLT
	movq	0(%r13), %rax
	jmp	.L482
	.p2align 4,,10
	.p2align 3
.L625:
	movq	-112(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler19RawMachineAssembler19MarkControlDeferredEPNS1_4NodeE
	jmp	.L474
	.p2align 4,,10
	.p2align 3
.L442:
	movq	136(%r12), %rdx
	movq	144(%r12), %r13
	subq	%rdx, %r13
	cmpq	$8, %r13
	jne	.L451
	movq	(%rdx), %rax
	movq	-272(%rbp), %rdx
	movq	160(%rax), %rax
	movq	(%rdx,%rax,8), %r15
	movq	-304(%rbp), %rdx
	movq	(%rdx,%rax,8), %rax
	movq	%rax, -112(%rbp)
	jmp	.L439
	.p2align 4,,10
	.p2align 3
.L451:
	movq	-240(%rbp), %r8
	cmpq	%r8, -232(%rbp)
	je	.L452
	movq	%r8, -232(%rbp)
.L452:
	movq	-208(%rbp), %rax
	cmpq	%rax, -200(%rbp)
	je	.L453
	movq	%rax, -200(%rbp)
.L453:
	sarq	$3, %r13
	testl	%r13d, %r13d
	jle	.L544
	leal	-1(%r13), %r9d
	movq	%rbx, -320(%rbp)
	xorl	%r14d, %r14d
	movq	%r8, %rsi
	leaq	0(,%r9,8), %r15
	movq	%r15, %rbx
	movq	%r12, %r15
	jmp	.L460
	.p2align 4,,10
	.p2align 3
.L626:
	movq	(%rdx), %rax
	movq	%rax, (%rsi)
	addq	$8, -232(%rbp)
.L456:
	movq	160(%r12), %rdx
	movq	-272(%rbp), %rax
	movq	-200(%rbp), %rsi
	leaq	(%rax,%rdx,8), %rdx
	cmpq	-192(%rbp), %rsi
	je	.L457
	movq	(%rdx), %rax
	movq	%rax, (%rsi)
	movq	-232(%rbp), %rsi
	addq	$8, -200(%rbp)
	cmpq	%rbx, %r14
	je	.L614
.L459:
	movq	136(%r15), %rdx
	addq	$8, %r14
.L460:
	movq	(%rdx,%r14), %r12
	movq	-304(%rbp), %rax
	movq	160(%r12), %rdx
	leaq	(%rax,%rdx,8), %rdx
	cmpq	%rsi, -224(%rbp)
	jne	.L626
	movq	-352(%rbp), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	jmp	.L456
	.p2align 4,,10
	.p2align 3
.L457:
	leaq	-208(%rbp), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	movq	-232(%rbp), %rsi
	cmpq	%rbx, %r14
	jne	.L459
	.p2align 4,,10
	.p2align 3
.L614:
	movq	%r15, %r12
	movq	-320(%rbp), %rbx
	movq	-240(%rbp), %r15
	movq	%rsi, %r8
.L454:
	leaq	72(%rbx), %r9
	movl	%r13d, %esi
	movq	%r8, -336(%rbp)
	movq	8(%rbx), %r14
	movq	%r9, %rdi
	movq	%r9, -320(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
	movq	-336(%rbp), %r8
	movq	%r15, %rcx
	movq	%r14, %rdi
	movq	%rax, %rsi
	movq	%r8, %rdx
	xorl	%r8d, %r8d
	subq	%r15, %rdx
	sarq	$3, %rdx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-200(%rbp), %rsi
	cmpq	-192(%rbp), %rsi
	movq	%rax, -112(%rbp)
	movq	-320(%rbp), %r9
	je	.L461
	movq	%rax, (%rsi)
	movq	-200(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, -200(%rbp)
.L462:
	movq	%r9, %rdi
	movl	%r13d, %esi
	movq	8(%rbx), %r14
	movq	-208(%rbp), %r15
	movq	%rdx, -320(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9EffectPhiEi@PLT
	movq	%r15, %rcx
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movq	-320(%rbp), %rdx
	movq	%rax, %rsi
	subq	%r15, %rdx
	sarq	$3, %rdx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r15
	jmp	.L439
	.p2align 4,,10
	.p2align 3
.L484:
	movq	-400(%rbp), %rcx
	cmpq	%rcx, -376(%rbp)
	je	.L528
	leaq	72(%rbx), %rax
	movq	%rax, -336(%rbp)
	movq	-400(%rbp), %rax
	movq	%rax, -312(%rbp)
	leaq	-112(%rbp), %rax
	movq	%rax, -320(%rbp)
	leaq	-144(%rbp), %rax
	movq	%rax, -352(%rbp)
	leaq	-176(%rbp), %rax
	movq	%rax, -344(%rbp)
	leaq	-304(%rbp), %rax
	movq	%rax, -368(%rbp)
	leaq	-272(%rbp), %rax
	movq	%rax, -360(%rbp)
	.p2align 4,,10
	.p2align 3
.L529:
	movq	-312(%rbp), %rax
	pxor	%xmm1, %xmm1
	movq	$0, -160(%rbp)
	movq	$0, -128(%rbp)
	movq	(%rax), %r13
	movaps	%xmm1, -176(%rbp)
	movaps	%xmm1, -144(%rbp)
	movq	136(%r13), %rdx
	cmpq	144(%r13), %rdx
	je	.L545
	movq	-320(%rbp), %r14
	xorl	%r12d, %r12d
	jmp	.L493
	.p2align 4,,10
	.p2align 3
.L628:
	movq	-136(%rbp), %rsi
	cmpq	-128(%rbp), %rsi
	je	.L490
	movq	-112(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, -136(%rbp)
.L491:
	movq	136(%r13), %rdx
	movq	144(%r13), %rax
	addq	$1, %r12
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rax, %r12
	jnb	.L627
.L493:
	movq	(%rdx,%r12,8), %rsi
	movq	%r13, %rdi
	movq	%rsi, -112(%rbp)
	call	_ZNK2v88internal8compiler10BasicBlock12LoopContainsEPS2_@PLT
	testb	%al, %al
	jne	.L628
	movq	-168(%rbp), %rsi
	cmpq	-160(%rbp), %rsi
	je	.L492
	movq	-112(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, -168(%rbp)
	jmp	.L491
	.p2align 4,,10
	.p2align 3
.L627:
	movq	-168(%rbp), %r15
	movq	-136(%rbp), %r14
	subq	-176(%rbp), %r15
	subq	-144(%rbp), %r14
	sarq	$3, %r15
	sarq	$3, %r14
.L488:
	movq	-336(%rbp), %rdi
	pxor	%xmm2, %xmm2
	movl	%r15d, %esi
	movq	$0, -96(%rbp)
	movaps	%xmm2, -112(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
	movq	-320(%rbp), %r8
	movq	-368(%rbp), %rdx
	movq	%rbx, %rdi
	movq	-344(%rbp), %rsi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler19RawMachineAssembler26CreateNodeFromPredecessorsERKSt6vectorIPNS1_10BasicBlockESaIS5_EERKS3_IPNS1_4NodeESaISB_EEPKNS1_8OperatorESF_
	movq	-112(%rbp), %rdi
	movq	%rax, -328(%rbp)
	testq	%rdi, %rdi
	je	.L494
	call	_ZdlPv@PLT
.L494:
	movq	-336(%rbp), %rdi
	pxor	%xmm3, %xmm3
	movl	%r14d, %esi
	movq	$0, -96(%rbp)
	movaps	%xmm3, -112(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
	movq	-320(%rbp), %r8
	movq	-368(%rbp), %rdx
	movq	%rbx, %rdi
	movq	-352(%rbp), %rsi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler19RawMachineAssembler26CreateNodeFromPredecessorsERKSt6vectorIPNS1_10BasicBlockESaIS5_EERKS3_IPNS1_4NodeESaISB_EEPKNS1_8OperatorESF_
	movq	-112(%rbp), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L495
	call	_ZdlPv@PLT
.L495:
	pxor	%xmm4, %xmm4
	movl	$8, %edi
	movq	$0, -96(%rbp)
	movaps	%xmm4, -112(%rbp)
	call	_Znwm@PLT
	movq	-328(%rbp), %rcx
	movq	-336(%rbp), %rdi
	movl	%r15d, %esi
	leaq	8(%rax), %rdx
	movq	%rax, -112(%rbp)
	movq	%rcx, (%rax)
	movq	%rdx, -96(%rbp)
	movq	%rdx, -104(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9EffectPhiEi@PLT
	movq	-320(%rbp), %r8
	movq	-360(%rbp), %rdx
	movq	%rbx, %rdi
	movq	-344(%rbp), %rsi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler19RawMachineAssembler26CreateNodeFromPredecessorsERKSt6vectorIPNS1_10BasicBlockESaIS5_EERKS3_IPNS1_4NodeESaISB_EEPKNS1_8OperatorESF_
	movq	-112(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L496
	call	_ZdlPv@PLT
.L496:
	pxor	%xmm5, %xmm5
	movl	$8, %edi
	movq	$0, -96(%rbp)
	movaps	%xmm5, -112(%rbp)
	call	_Znwm@PLT
	movq	-336(%rbp), %rdi
	movl	%r14d, %esi
	movq	%r12, (%rax)
	leaq	8(%rax), %rdx
	movq	%rdx, -96(%rbp)
	movq	%rdx, -104(%rbp)
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9EffectPhiEi@PLT
	movq	-320(%rbp), %r8
	movq	-360(%rbp), %rdx
	movq	%rbx, %rdi
	movq	-352(%rbp), %rsi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler19RawMachineAssembler26CreateNodeFromPredecessorsERKSt6vectorIPNS1_10BasicBlockESaIS5_EERKS3_IPNS1_4NodeESaISB_EEPKNS1_8OperatorESF_
	movq	-112(%rbp), %rdi
	movq	%rax, %r14
	testq	%rdi, %rdi
	je	.L497
	call	_ZdlPv@PLT
.L497:
	movq	-312(%rbp), %rax
	movq	8(%rax), %rsi
	movzbl	23(%rsi), %edx
	leaq	32(%rsi), %rax
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L498
	movq	32(%rsi), %rdi
	cmpq	%rdi, -328(%rbp)
	je	.L501
.L499:
	subq	$24, %rsi
	testq	%rdi, %rdi
	je	.L502
	movq	%rax, -392(%rbp)
	movq	%rsi, -384(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-392(%rbp), %rax
	movq	-384(%rbp), %rsi
.L502:
	movq	-328(%rbp), %rcx
	movq	%rcx, (%rax)
	testq	%rcx, %rcx
	je	.L503
	movq	%rcx, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L503:
	movq	-312(%rbp), %rax
	movq	8(%rax), %rsi
	movzbl	23(%rsi), %edx
	leaq	32(%rsi), %rax
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L501
	movq	32(%rsi), %rsi
	leaq	16(%rsi), %rax
.L501:
	movq	8(%rax), %rdi
	cmpq	%rdi, %r12
	je	.L507
	addq	$8, %rax
	subq	$48, %rsi
	testq	%rdi, %rdi
	je	.L508
	movq	%rax, -392(%rbp)
	movq	%rsi, -384(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-392(%rbp), %rax
	movq	-384(%rbp), %rsi
.L508:
	movq	%r12, (%rax)
	testq	%r12, %r12
	je	.L507
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L507:
	movq	-312(%rbp), %rax
	movq	16(%rax), %rsi
	movzbl	23(%rsi), %edx
	movq	32(%rsi), %rdi
	leaq	32(%rsi), %rax
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L509
	cmpq	%rdi, %r15
	je	.L512
.L510:
	subq	$24, %rsi
	testq	%rdi, %rdi
	je	.L513
	movq	%rax, -392(%rbp)
	movq	%rsi, -384(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-392(%rbp), %rax
	movq	-384(%rbp), %rsi
.L513:
	movq	%r15, (%rax)
	testq	%r15, %r15
	je	.L514
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L514:
	movq	-312(%rbp), %rax
	movq	16(%rax), %rsi
	movzbl	23(%rsi), %edx
	leaq	32(%rsi), %rax
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L512
	movq	32(%rsi), %rsi
	leaq	16(%rsi), %rax
.L512:
	movq	8(%rax), %rdi
	cmpq	%rdi, %r14
	je	.L518
	addq	$8, %rax
	leaq	-48(%rsi), %r15
	testq	%rdi, %rdi
	je	.L519
	movq	%r15, %rsi
	movq	%rax, -384(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-384(%rbp), %rax
.L519:
	movq	%r14, (%rax)
	testq	%r14, %r14
	je	.L518
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L518:
	movq	80(%r13), %r15
	movq	72(%r13), %r14
	movq	-328(%rbp), %r13
	cmpq	%r14, %r15
	jne	.L524
	jmp	.L525
	.p2align 4,,10
	.p2align 3
.L523:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L525
.L524:
	movq	(%r14), %rsi
	movq	(%rsi), %rax
	cmpw	$35, 16(%rax)
	jne	.L523
	movq	-168(%rbp), %rdx
	movq	%r12, %r8
	movq	%r13, %rcx
	movq	%rbx, %rdi
	subq	-176(%rbp), %rdx
	addq	$8, %r14
	sarq	$3, %rdx
	call	_ZN2v88internal8compiler19RawMachineAssembler13MakePhiBinaryEPNS1_4NodeEiS4_S4_
	cmpq	%r14, %r15
	jne	.L524
	.p2align 4,,10
	.p2align 3
.L525:
	movq	-144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L522
	call	_ZdlPv@PLT
.L522:
	movq	-176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L526
	call	_ZdlPv@PLT
	addq	$24, -312(%rbp)
	movq	-312(%rbp), %rax
	cmpq	%rax, -376(%rbp)
	jne	.L529
.L528:
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L530
	call	_ZdlPv@PLT
.L530:
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L531
	call	_ZdlPv@PLT
.L531:
	movq	-400(%rbp), %rax
	testq	%rax, %rax
	je	.L437
	movq	%rax, %rdi
.L617:
	call	_ZdlPv@PLT
.L437:
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L532
	call	_ZdlPv@PLT
.L532:
	movq	-304(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L430
	call	_ZdlPv@PLT
.L430:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L629
	addq	$360, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L490:
	.cfi_restore_state
	movq	-352(%rbp), %rdi
	movq	%r14, %rdx
	call	_ZNSt6vectorIPN2v88internal8compiler10BasicBlockESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	jmp	.L491
	.p2align 4,,10
	.p2align 3
.L492:
	movq	-344(%rbp), %rdi
	movq	%r14, %rdx
	call	_ZNSt6vectorIPN2v88internal8compiler10BasicBlockESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	jmp	.L491
	.p2align 4,,10
	.p2align 3
.L526:
	addq	$24, -312(%rbp)
	movq	-312(%rbp), %rax
	cmpq	%rax, -376(%rbp)
	jne	.L529
	jmp	.L528
	.p2align 4,,10
	.p2align 3
.L498:
	movq	32(%rsi), %rsi
	movq	16(%rsi), %rdi
	leaq	16(%rsi), %rax
	cmpq	%rdi, -328(%rbp)
	jne	.L499
	jmp	.L501
	.p2align 4,,10
	.p2align 3
.L509:
	movq	16(%rdi), %rdx
	leaq	16(%rdi), %rax
	movq	%rdi, %rsi
	cmpq	%rdx, %r15
	je	.L512
	movq	%rdx, %rdi
	jmp	.L510
	.p2align 4,,10
	.p2align 3
.L545:
	xorl	%r14d, %r14d
	xorl	%r15d, %r15d
	jmp	.L488
	.p2align 4,,10
	.p2align 3
.L622:
	movq	136(%r12), %rdx
	cmpq	144(%r12), %rdx
	je	.L439
	leaq	72(%rbx), %r13
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L441:
	movq	(%rdx,%r14,8), %rax
	movq	8(%rbx), %rdi
	movq	%r13, %rsi
	addq	$1, %r14
	movq	56(%rax), %rdx
	call	_ZN2v88internal8compiler14NodeProperties17MergeControlToEndEPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS1_4NodeE@PLT
	movq	136(%r12), %rdx
	movq	144(%r12), %rax
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rax, %r14
	jb	.L441
	jmp	.L439
.L443:
	movabsq	$-6148914691236517205, %rcx
	movq	%rax, %rdx
	subq	-400(%rbp), %rdx
	movq	%rdx, %rax
	sarq	$3, %rax
	imulq	%rcx, %rax
	movabsq	$384307168202282325, %rcx
	cmpq	%rcx, %rax
	je	.L630
	testq	%rax, %rax
	je	.L541
	movabsq	$9223372036854775800, %r13
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L631
.L446:
	movq	%r13, %rdi
	movq	%rdx, -320(%rbp)
	call	_Znwm@PLT
	movq	-320(%rbp), %rdx
	movq	%rax, %rcx
	leaq	(%rax,%r13), %rax
	movq	%rax, -344(%rbp)
.L447:
	movq	%r12, %xmm0
	movq	%r14, %xmm7
	leaq	(%rcx,%rdx), %rax
	punpcklqdq	%xmm7, %xmm0
	movq	%r15, 16(%rax)
	movups	%xmm0, (%rax)
	leaq	24(%rcx,%rdx), %rax
	movq	%rax, -376(%rbp)
	testq	%rdx, %rdx
	jg	.L632
	cmpq	$0, -400(%rbp)
	jne	.L449
.L450:
	movq	%rcx, -400(%rbp)
	jmp	.L439
.L632:
	movq	-400(%rbp), %rsi
	movq	%rcx, %rdi
	call	memmove@PLT
	movq	%rax, %rcx
.L449:
	movq	-400(%rbp), %rdi
	movq	%rcx, -320(%rbp)
	call	_ZdlPv@PLT
	movq	-320(%rbp), %rcx
	jmp	.L450
.L631:
	testq	%rcx, %rcx
	jne	.L633
	movq	$0, -344(%rbp)
	xorl	%ecx, %ecx
	jmp	.L447
.L461:
	leaq	-112(%rbp), %rdx
	leaq	-208(%rbp), %rdi
	movq	%r9, -320(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	movq	-200(%rbp), %rdx
	movq	-320(%rbp), %r9
	jmp	.L462
.L541:
	movl	$24, %r13d
	jmp	.L446
.L434:
	movq	%r12, -256(%rbp)
	xorl	%r13d, %r13d
	jmp	.L538
.L620:
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L617
	jmp	.L437
.L432:
	movq	%r13, -288(%rbp)
	xorl	%edx, %edx
	jmp	.L539
.L544:
	movq	%r8, %r15
	jmp	.L454
.L633:
	movabsq	$384307168202282325, %rax
	cmpq	%rax, %rcx
	cmova	%rax, %rcx
	imulq	$24, %rcx, %r13
	jmp	.L446
.L433:
	leaq	.LC0(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L629:
	call	__stack_chk_fail@PLT
.L630:
	leaq	.LC4(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE22185:
	.size	_ZN2v88internal8compiler19RawMachineAssembler17MakeReschedulableEv, .-_ZN2v88internal8compiler19RawMachineAssembler17MakeReschedulableEv
	.section	.rodata._ZN2v88internal8compiler19RawMachineAssembler21ExportForOptimizationEv.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"--- SCHEDULE BEFORE GRAPH CREATION -------------------------\n"
	.section	.text._ZN2v88internal8compiler19RawMachineAssembler21ExportForOptimizationEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19RawMachineAssembler21ExportForOptimizationEv
	.type	_ZN2v88internal8compiler19RawMachineAssembler21ExportForOptimizationEv, @function
_ZN2v88internal8compiler19RawMachineAssembler21ExportForOptimizationEv:
.LFB22183:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$352, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L639
.L635:
	movq	16(%rbx), %rdi
	call	_ZN2v88internal8compiler8Schedule23EnsureCFGWellFormednessEv@PLT
	movq	8(%rbx), %rsi
	movq	16(%rbx), %rdi
	leaq	72(%rbx), %rdx
	call	_ZN2v88internal8compiler19RawMachineAssembler19OptimizeControlFlowEPNS1_8ScheduleEPNS1_5GraphEPNS1_21CommonOperatorBuilderE
	movq	8(%rbx), %rax
	movq	16(%rbx), %rsi
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler9Scheduler17ComputeSpecialRPOEPNS0_4ZoneEPNS1_8ScheduleE@PLT
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L640
.L636:
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler19RawMachineAssembler17MakeReschedulableEv
	movq	$0, 16(%rbx)
	movq	8(%rbx), %rax
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L641
	addq	$352, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L639:
	.cfi_restore_state
	leaq	-304(%rbp), %r12
	leaq	.LC1(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	%r12, %rdi
	leaq	-384(%rbp), %r14
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %r13
	call	_ZNSt8ios_baseC2Ev@PLT
	xorl	%edx, %edx
	pxor	%xmm0, %xmm0
	movq	%r14, %rdi
	movw	%dx, -80(%rbp)
	movq	stdout(%rip), %rdx
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movups	%xmm0, -72(%rbp)
	movups	%xmm0, -56(%rbp)
	movq	%r13, -304(%rbp)
	movq	$0, -88(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	movq	16(%rbx), %rsi
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -384(%rbp)
	addq	$40, %rax
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal8compilerlsERSoRKNS1_8ScheduleE@PLT
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rcx
	movq	%rax, -304(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-320(%rbp), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%r12, %rdi
	movq	%r13, -304(%rbp)
	movq	%rax, -384(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L635
	.p2align 4,,10
	.p2align 3
.L640:
	xorl	%eax, %eax
	leaq	-304(%rbp), %r12
	leaq	.LC5(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	%r12, %rdi
	leaq	-384(%rbp), %r14
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %r13
	call	_ZNSt8ios_baseC2Ev@PLT
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	stdout(%rip), %rdx
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movups	%xmm0, -72(%rbp)
	movups	%xmm0, -56(%rbp)
	movw	%ax, -80(%rbp)
	movq	%r13, -304(%rbp)
	movq	$0, -88(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	movq	16(%rbx), %rsi
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -384(%rbp)
	addq	$40, %rax
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal8compilerlsERSoRKNS1_8ScheduleE@PLT
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rcx
	movq	%rax, -304(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-320(%rbp), %rdi
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%r12, %rdi
	movq	%r13, -304(%rbp)
	movq	%rax, -384(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L636
.L641:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22183:
	.size	_ZN2v88internal8compiler19RawMachineAssembler21ExportForOptimizationEv, .-_ZN2v88internal8compiler19RawMachineAssembler21ExportForOptimizationEv
	.section	.text._ZN2v84base11SmallVectorIPNS_8internal8compiler4NodeELm10EE4GrowEm,"axG",@progbits,_ZN2v84base11SmallVectorIPNS_8internal8compiler4NodeELm10EE4GrowEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v84base11SmallVectorIPNS_8internal8compiler4NodeELm10EE4GrowEm
	.type	_ZN2v84base11SmallVectorIPNS_8internal8compiler4NodeELm10EE4GrowEm, @function
_ZN2v84base11SmallVectorIPNS_8internal8compiler4NodeELm10EE4GrowEm:
.LFB26174:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rax
	movq	8(%rdi), %r12
	movq	16(%rdi), %rdi
	subq	%rax, %r12
	subq	%rax, %rdi
	sarq	$3, %rdi
	addq	%rdi, %rdi
	cmpq	%rsi, %rdi
	cmovb	%rsi, %rdi
	call	_ZN2v84base4bits21RoundUpToPowerOfTwo64Em@PLT
	leaq	0(,%rax,8), %r14
	movq	%r14, %rdi
	call	malloc@PLT
	movq	(%rbx), %r15
	movq	%r12, %rdx
	movq	%rax, %rdi
	movq	%rax, %r13
	movq	%r15, %rsi
	call	memcpy@PLT
	leaq	24(%rbx), %rax
	cmpq	%rax, %r15
	je	.L643
	movq	%r15, %rdi
	call	free@PLT
.L643:
	leaq	0(%r13,%r12), %rax
	movq	%r13, (%rbx)
	addq	%r14, %r13
	movq	%r13, 16(%rbx)
	movq	%rax, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE26174:
	.size	_ZN2v84base11SmallVectorIPNS_8internal8compiler4NodeELm10EE4GrowEm, .-_ZN2v84base11SmallVectorIPNS_8internal8compiler4NodeELm10EE4GrowEm
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_117CallCFunctionImplEPNS1_19RawMachineAssemblerEPNS1_4NodeENS0_11MachineTypeESt16initializer_listISt4pairIS7_S6_EEbNS0_14SaveFPRegsModeEb,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_117CallCFunctionImplEPNS1_19RawMachineAssemblerEPNS1_4NodeENS0_11MachineTypeESt16initializer_listISt4pairIS7_S6_EEbNS0_14SaveFPRegsModeEb, @function
_ZN2v88internal8compiler12_GLOBAL__N_117CallCFunctionImplEPNS1_19RawMachineAssemblerEPNS1_4NodeENS0_11MachineTypeESt16initializer_listISt4pairIS7_S6_EEbNS0_14SaveFPRegsModeEb:
.LFB22240:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$168, %rsp
	movl	24(%rbp), %eax
	movq	%rsi, -168(%rbp)
	movl	%eax, -176(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	(%rax), %rdi
	leal	1(%r8), %eax
	cltq
	leaq	7(%rax,%rax), %rsi
	movq	16(%rdi), %r13
	movq	24(%rdi), %rax
	andq	$-8, %rsi
	subq	%r13, %rax
	cmpq	%rax, %rsi
	ja	.L685
	addq	%r13, %rsi
	movq	%rsi, 16(%rdi)
.L647:
	movq	%r14, %r12
	movw	%dx, 0(%r13)
	salq	$4, %r12
	addq	%rbx, %r12
	cmpq	%rbx, %r12
	je	.L653
	movq	%r12, %rdx
	movq	%rbx, %rax
	subq	%rbx, %rdx
	leaq	-16(%rdx), %rcx
	leaq	2(%r13), %rdx
	movq	%rcx, %rsi
	leaq	2(%rcx,%rbx), %r8
	shrq	$4, %rsi
	cmpq	%rdx, %r8
	leaq	4(%r13,%rsi,2), %r8
	setbe	%r11b
	cmpq	%r8, %rbx
	setnb	%r8b
	orb	%r8b, %r11b
	je	.L650
	movabsq	$1152921504606846960, %r8
	testq	%r8, %rsi
	je	.L650
	xorb	%cl, %cl
	movdqa	.LC6(%rip), %xmm0
	addq	%rbx, %rcx
	.p2align 4,,10
	.p2align 3
.L652:
	movdqu	(%rax), %xmm2
	movdqu	16(%rax), %xmm1
	addq	$256, %rax
	addq	$32, %rdx
	movdqu	-208(%rax), %xmm3
	movdqu	-144(%rax), %xmm4
	pand	%xmm0, %xmm1
	pand	%xmm0, %xmm2
	movdqu	-16(%rax), %xmm5
	movdqu	-16(%rax), %xmm6
	packuswb	%xmm1, %xmm2
	movdqu	-224(%rax), %xmm1
	pand	%xmm0, %xmm3
	pand	%xmm0, %xmm4
	pand	%xmm0, %xmm2
	psrlw	$8, %xmm6
	pand	%xmm0, %xmm5
	pand	%xmm0, %xmm1
	packuswb	%xmm3, %xmm1
	movdqu	-176(%rax), %xmm3
	pand	%xmm0, %xmm1
	packuswb	%xmm1, %xmm2
	movdqu	-192(%rax), %xmm1
	pand	%xmm0, %xmm3
	pand	%xmm0, %xmm2
	pand	%xmm0, %xmm1
	packuswb	%xmm3, %xmm1
	movdqu	-160(%rax), %xmm3
	pand	%xmm0, %xmm1
	pand	%xmm0, %xmm3
	packuswb	%xmm4, %xmm3
	movdqu	-80(%rax), %xmm4
	pand	%xmm0, %xmm3
	packuswb	%xmm3, %xmm1
	movdqu	-112(%rax), %xmm3
	pand	%xmm0, %xmm4
	pand	%xmm0, %xmm1
	packuswb	%xmm1, %xmm2
	movdqu	-128(%rax), %xmm1
	pand	%xmm0, %xmm3
	pand	%xmm0, %xmm2
	pand	%xmm0, %xmm1
	packuswb	%xmm3, %xmm1
	movdqu	-96(%rax), %xmm3
	pand	%xmm0, %xmm1
	pand	%xmm0, %xmm3
	packuswb	%xmm4, %xmm3
	movdqu	-48(%rax), %xmm4
	pand	%xmm0, %xmm3
	packuswb	%xmm3, %xmm1
	movdqu	-64(%rax), %xmm3
	pand	%xmm0, %xmm4
	pand	%xmm0, %xmm1
	pand	%xmm0, %xmm3
	packuswb	%xmm4, %xmm3
	movdqu	-32(%rax), %xmm4
	pand	%xmm0, %xmm3
	pand	%xmm0, %xmm4
	packuswb	%xmm5, %xmm4
	movdqu	-144(%rax), %xmm5
	pand	%xmm0, %xmm4
	packuswb	%xmm4, %xmm3
	movdqu	-208(%rax), %xmm4
	psrlw	$8, %xmm5
	pand	%xmm0, %xmm3
	packuswb	%xmm3, %xmm1
	movdqu	-240(%rax), %xmm3
	psrlw	$8, %xmm4
	pand	%xmm0, %xmm1
	packuswb	%xmm1, %xmm2
	movdqu	-256(%rax), %xmm1
	psrlw	$8, %xmm3
	psrlw	$8, %xmm1
	packuswb	%xmm3, %xmm1
	movdqu	-224(%rax), %xmm3
	pand	%xmm0, %xmm1
	psrlw	$8, %xmm3
	packuswb	%xmm4, %xmm3
	movdqu	-176(%rax), %xmm4
	pand	%xmm0, %xmm3
	packuswb	%xmm3, %xmm1
	movdqu	-192(%rax), %xmm3
	psrlw	$8, %xmm4
	pand	%xmm0, %xmm1
	psrlw	$8, %xmm3
	packuswb	%xmm4, %xmm3
	movdqu	-160(%rax), %xmm4
	pand	%xmm0, %xmm3
	psrlw	$8, %xmm4
	packuswb	%xmm5, %xmm4
	movdqu	-80(%rax), %xmm5
	pand	%xmm0, %xmm4
	packuswb	%xmm4, %xmm3
	movdqu	-112(%rax), %xmm4
	psrlw	$8, %xmm5
	pand	%xmm0, %xmm3
	packuswb	%xmm3, %xmm1
	movdqu	-128(%rax), %xmm3
	psrlw	$8, %xmm4
	pand	%xmm0, %xmm1
	psrlw	$8, %xmm3
	packuswb	%xmm4, %xmm3
	movdqu	-96(%rax), %xmm4
	pand	%xmm0, %xmm3
	psrlw	$8, %xmm4
	packuswb	%xmm5, %xmm4
	movdqu	-48(%rax), %xmm5
	pand	%xmm0, %xmm4
	packuswb	%xmm4, %xmm3
	movdqu	-64(%rax), %xmm4
	psrlw	$8, %xmm5
	pand	%xmm0, %xmm3
	psrlw	$8, %xmm4
	packuswb	%xmm5, %xmm4
	movdqu	-32(%rax), %xmm5
	pand	%xmm0, %xmm4
	psrlw	$8, %xmm5
	packuswb	%xmm6, %xmm5
	pand	%xmm0, %xmm5
	packuswb	%xmm5, %xmm4
	pand	%xmm0, %xmm4
	packuswb	%xmm4, %xmm3
	pand	%xmm0, %xmm3
	packuswb	%xmm3, %xmm1
	movdqa	%xmm2, %xmm3
	punpcklbw	%xmm1, %xmm3
	punpckhbw	%xmm1, %xmm2
	movups	%xmm3, -32(%rdx)
	movups	%xmm2, -16(%rdx)
	cmpq	%rcx, %rax
	jne	.L652
	andq	$-16, %rsi
	movq	%rsi, %rax
	addq	$1, %rsi
	salq	$4, %rax
	leaq	(%rsi,%rsi), %rcx
	addq	%rbx, %rax
	movzwl	(%rax), %edx
	movw	%dx, 0(%r13,%rsi,2)
	leaq	16(%rax), %rdx
	cmpq	%rdx, %r12
	je	.L653
	movzwl	16(%rax), %edx
	movw	%dx, 2(%r13,%rcx)
	leaq	32(%rax), %rdx
	cmpq	%rdx, %r12
	je	.L653
	movzwl	32(%rax), %edx
	movw	%dx, 4(%r13,%rcx)
	leaq	48(%rax), %rdx
	cmpq	%rdx, %r12
	je	.L653
	movzwl	48(%rax), %edx
	movw	%dx, 6(%r13,%rcx)
	leaq	64(%rax), %rdx
	cmpq	%rdx, %r12
	je	.L653
	movzwl	64(%rax), %edx
	movw	%dx, 8(%r13,%rcx)
	leaq	80(%rax), %rdx
	cmpq	%rdx, %r12
	je	.L653
	movzwl	80(%rax), %edx
	movw	%dx, 10(%r13,%rcx)
	leaq	96(%rax), %rdx
	cmpq	%rdx, %r12
	je	.L653
	movzwl	96(%rax), %edx
	movw	%dx, 12(%r13,%rcx)
	leaq	112(%rax), %rdx
	cmpq	%rdx, %r12
	je	.L653
	movzwl	112(%rax), %edx
	movw	%dx, 14(%r13,%rcx)
	leaq	128(%rax), %rdx
	cmpq	%rdx, %r12
	je	.L653
	movzwl	128(%rax), %edx
	movw	%dx, 16(%r13,%rcx)
	leaq	144(%rax), %rdx
	cmpq	%rdx, %r12
	je	.L653
	movzwl	144(%rax), %edx
	movw	%dx, 18(%r13,%rcx)
	leaq	160(%rax), %rdx
	cmpq	%rdx, %r12
	je	.L653
	movzwl	160(%rax), %edx
	movw	%dx, 20(%r13,%rcx)
	leaq	176(%rax), %rdx
	cmpq	%rdx, %r12
	je	.L653
	movzwl	176(%rax), %edx
	movw	%dx, 22(%r13,%rcx)
	leaq	192(%rax), %rdx
	cmpq	%rdx, %r12
	je	.L653
	movzwl	192(%rax), %edx
	movw	%dx, 24(%r13,%rcx)
	leaq	208(%rax), %rdx
	cmpq	%rdx, %r12
	je	.L653
	movzwl	208(%rax), %edx
	movw	%dx, 26(%r13,%rcx)
	leaq	224(%rax), %rdx
	cmpq	%rdx, %r12
	je	.L653
	movzwl	224(%rax), %edx
	movw	%dx, 28(%r13,%rcx)
	leaq	240(%rax), %rdx
	cmpq	%rdx, %r12
	je	.L653
	movzwl	240(%rax), %eax
	movw	%ax, 30(%r13,%rcx)
	.p2align 4,,10
	.p2align 3
.L653:
	movq	16(%rdi), %rsi
	movq	24(%rdi), %rax
	xorl	%edx, %edx
	testb	%r9b, %r9b
	setne	%dl
	subq	%rsi, %rax
	sall	$9, %edx
	cmpq	$23, %rax
	jbe	.L686
	leaq	24(%rsi), %rax
	movq	%rax, 16(%rdi)
.L657:
	movq	$1, (%rsi)
	movq	%r14, 8(%rsi)
	movq	%r13, 16(%rsi)
	movq	8(%r15), %rax
	movl	%r9d, -192(%rbp)
	movq	(%rax), %rdi
	movq	%r10, -184(%rbp)
	call	_ZN2v88internal8compiler7Linkage24GetSimplifiedCDescriptorEPNS0_4ZoneEPKNS0_9SignatureINS0_11MachineTypeEEENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE@PLT
	movl	-192(%rbp), %r9d
	movq	-184(%rbp), %r10
	movq	%rax, %r11
	testb	%r9b, %r9b
	je	.L658
	movl	16(%rbp), %eax
	movl	%eax, (%r11)
.L658:
	movzbl	-176(%rbp), %eax
	leaq	-136(%rbp), %r13
	addq	$1, %r14
	leaq	-160(%rbp), %rdi
	movq	%r13, %xmm0
	movb	%al, 4(%r11)
	punpcklqdq	%xmm0, %xmm0
	leaq	-56(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	%r13, %rax
	movaps	%xmm0, -160(%rbp)
	cmpq	$10, %r14
	ja	.L687
.L659:
	movq	-168(%rbp), %rdi
	leaq	(%rax,%r14,8), %rdx
	movq	%rdx, -152(%rbp)
	movq	%rdi, (%rax)
	movq	-160(%rbp), %r14
	leaq	8(%r14), %rcx
	cmpq	%rbx, %r12
	je	.L660
	movq	%r12, %rax
	leaq	8(%rbx), %rdi
	subq	%rbx, %rax
	leaq	-16(%rax), %rdx
	movq	%rdx, %rsi
	shrq	$4, %rsi
	leaq	16(%r14,%rsi,8), %rax
	cmpq	%rax, %rdi
	leaq	16(%rdx,%rbx), %rax
	setnb	%dil
	cmpq	%rax, %rcx
	setnb	%al
	orb	%al, %dil
	je	.L674
	cmpq	$80, %rdx
	jbe	.L674
	shrq	$5, %rdx
	xorl	%eax, %eax
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L662:
	movdqu	8(%rbx,%rax,2), %xmm0
	movdqu	24(%rbx,%rax,2), %xmm7
	punpcklqdq	%xmm7, %xmm0
	movups	%xmm0, 8(%r14,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L662
	andq	$-2, %rsi
	movq	%rsi, %rax
	salq	$4, %rax
	addq	%rax, %rbx
	leaq	(%rcx,%rsi,8), %rax
	movq	8(%rbx), %rdx
	movq	%rdx, (%rax)
	leaq	16(%rbx), %rdx
	cmpq	%rdx, %r12
	je	.L663
	movq	24(%rbx), %rdx
	movq	%rdx, 8(%rax)
.L663:
	movq	-160(%rbp), %r14
.L660:
	movq	-152(%rbp), %rbx
	movq	%r11, %rsi
	leaq	72(%r15), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	movq	8(%r15), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	subq	%r14, %rbx
	movq	%rax, %rsi
	sarq	$3, %rbx
	movl	%ebx, %edx
	call	_ZN2v88internal8compiler5Graph16NewNodeUncheckedEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%r15), %rdi
	movq	152(%r15), %rsi
	movq	%rax, %rdx
	movq	%rax, %r12
	call	_ZN2v88internal8compiler8Schedule7AddNodeEPNS1_10BasicBlockEPNS1_4NodeE@PLT
	movq	-160(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L645
	call	free@PLT
.L645:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L688
	addq	$168, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L674:
	.cfi_restore_state
	movq	8(%r10), %rax
	addq	$16, %r10
	addq	$8, %rcx
	movq	%rax, -8(%rcx)
	cmpq	%r10, %r12
	jne	.L674
	jmp	.L663
	.p2align 4,,10
	.p2align 3
.L687:
	movq	%r14, %rsi
	movq	%r10, -184(%rbp)
	movq	%r11, -176(%rbp)
	call	_ZN2v84base11SmallVectorIPNS_8internal8compiler4NodeELm10EE4GrowEm
	movq	-160(%rbp), %rax
	movq	-184(%rbp), %r10
	movq	-176(%rbp), %r11
	jmp	.L659
	.p2align 4,,10
	.p2align 3
.L650:
	movq	%rbx, %rax
	.p2align 4,,10
	.p2align 3
.L655:
	movzbl	(%rax), %esi
	movzbl	1(%rax), %ecx
	addq	$16, %rax
	addq	$2, %rdx
	movb	%sil, -2(%rdx)
	movb	%cl, -1(%rdx)
	cmpq	%rax, %r12
	jne	.L655
	jmp	.L653
	.p2align 4,,10
	.p2align 3
.L686:
	movl	$24, %esi
	movl	%r9d, -196(%rbp)
	movq	%r10, -192(%rbp)
	movl	%edx, -184(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-184(%rbp), %edx
	movq	-192(%rbp), %r10
	movl	-196(%rbp), %r9d
	movq	%rax, %rsi
	jmp	.L657
	.p2align 4,,10
	.p2align 3
.L685:
	movl	%r9d, -200(%rbp)
	movl	%edx, -196(%rbp)
	movq	%rdi, -184(%rbp)
	movq	%rcx, -192(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-184(%rbp), %rdi
	movq	-192(%rbp), %r10
	movl	-196(%rbp), %edx
	movl	-200(%rbp), %r9d
	movq	%rax, %r13
	jmp	.L647
.L688:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22240:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_117CallCFunctionImplEPNS1_19RawMachineAssemblerEPNS1_4NodeENS0_11MachineTypeESt16initializer_listISt4pairIS7_S6_EEbNS0_14SaveFPRegsModeEb, .-_ZN2v88internal8compiler12_GLOBAL__N_117CallCFunctionImplEPNS1_19RawMachineAssemblerEPNS1_4NodeENS0_11MachineTypeESt16initializer_listISt4pairIS7_S6_EEbNS0_14SaveFPRegsModeEb
	.section	.text._ZN2v88internal8compiler19RawMachineAssembler13CallCFunctionEPNS1_4NodeENS0_11MachineTypeESt16initializer_listISt4pairIS5_S4_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19RawMachineAssembler13CallCFunctionEPNS1_4NodeENS0_11MachineTypeESt16initializer_listISt4pairIS5_S4_EE
	.type	_ZN2v88internal8compiler19RawMachineAssembler13CallCFunctionEPNS1_4NodeENS0_11MachineTypeESt16initializer_listISt4pairIS5_S4_EE, @function
_ZN2v88internal8compiler19RawMachineAssembler13CallCFunctionEPNS1_4NodeENS0_11MachineTypeESt16initializer_listISt4pairIS5_S4_EE:
.LFB22247:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r9d, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	$1
	pushq	$0
	call	_ZN2v88internal8compiler12_GLOBAL__N_117CallCFunctionImplEPNS1_19RawMachineAssemblerEPNS1_4NodeENS0_11MachineTypeESt16initializer_listISt4pairIS7_S6_EEbNS0_14SaveFPRegsModeEb
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22247:
	.size	_ZN2v88internal8compiler19RawMachineAssembler13CallCFunctionEPNS1_4NodeENS0_11MachineTypeESt16initializer_listISt4pairIS5_S4_EE, .-_ZN2v88internal8compiler19RawMachineAssembler13CallCFunctionEPNS1_4NodeENS0_11MachineTypeESt16initializer_listISt4pairIS5_S4_EE
	.section	.text._ZN2v88internal8compiler19RawMachineAssembler38CallCFunctionWithoutFunctionDescriptorEPNS1_4NodeENS0_11MachineTypeESt16initializer_listISt4pairIS5_S4_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19RawMachineAssembler38CallCFunctionWithoutFunctionDescriptorEPNS1_4NodeENS0_11MachineTypeESt16initializer_listISt4pairIS5_S4_EE
	.type	_ZN2v88internal8compiler19RawMachineAssembler38CallCFunctionWithoutFunctionDescriptorEPNS1_4NodeENS0_11MachineTypeESt16initializer_listISt4pairIS5_S4_EE, @function
_ZN2v88internal8compiler19RawMachineAssembler38CallCFunctionWithoutFunctionDescriptorEPNS1_4NodeENS0_11MachineTypeESt16initializer_listISt4pairIS5_S4_EE:
.LFB22248:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r9d, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	$0
	pushq	$0
	call	_ZN2v88internal8compiler12_GLOBAL__N_117CallCFunctionImplEPNS1_19RawMachineAssemblerEPNS1_4NodeENS0_11MachineTypeESt16initializer_listISt4pairIS7_S6_EEbNS0_14SaveFPRegsModeEb
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22248:
	.size	_ZN2v88internal8compiler19RawMachineAssembler38CallCFunctionWithoutFunctionDescriptorEPNS1_4NodeENS0_11MachineTypeESt16initializer_listISt4pairIS5_S4_EE, .-_ZN2v88internal8compiler19RawMachineAssembler38CallCFunctionWithoutFunctionDescriptorEPNS1_4NodeENS0_11MachineTypeESt16initializer_listISt4pairIS5_S4_EE
	.section	.text._ZN2v88internal8compiler19RawMachineAssembler37CallCFunctionWithCallerSavedRegistersEPNS1_4NodeENS0_11MachineTypeENS0_14SaveFPRegsModeESt16initializer_listISt4pairIS5_S4_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19RawMachineAssembler37CallCFunctionWithCallerSavedRegistersEPNS1_4NodeENS0_11MachineTypeENS0_14SaveFPRegsModeESt16initializer_listISt4pairIS5_S4_EE
	.type	_ZN2v88internal8compiler19RawMachineAssembler37CallCFunctionWithCallerSavedRegistersEPNS1_4NodeENS0_11MachineTypeENS0_14SaveFPRegsModeESt16initializer_listISt4pairIS5_S4_EE, @function
_ZN2v88internal8compiler19RawMachineAssembler37CallCFunctionWithCallerSavedRegistersEPNS1_4NodeENS0_11MachineTypeENS0_14SaveFPRegsModeESt16initializer_listISt4pairIS5_S4_EE:
.LFB22249:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%ecx, %r10d
	movq	%r8, %rcx
	movq	%r9, %r8
	movl	$1, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	$1
	pushq	%r10
	call	_ZN2v88internal8compiler12_GLOBAL__N_117CallCFunctionImplEPNS1_19RawMachineAssemblerEPNS1_4NodeENS0_11MachineTypeESt16initializer_listISt4pairIS7_S6_EEbNS0_14SaveFPRegsModeEb
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22249:
	.size	_ZN2v88internal8compiler19RawMachineAssembler37CallCFunctionWithCallerSavedRegistersEPNS1_4NodeENS0_11MachineTypeENS0_14SaveFPRegsModeESt16initializer_listISt4pairIS5_S4_EE, .-_ZN2v88internal8compiler19RawMachineAssembler37CallCFunctionWithCallerSavedRegistersEPNS1_4NodeENS0_11MachineTypeENS0_14SaveFPRegsModeESt16initializer_listISt4pairIS5_S4_EE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler19RawMachineAssemblerC2EPNS0_7IsolateEPNS1_5GraphEPNS1_14CallDescriptorENS0_21MachineRepresentationENS_4base5FlagsINS1_22MachineOperatorBuilder4FlagEjEENSC_21AlignmentRequirementsENS0_24PoisoningMitigationLevelE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler19RawMachineAssemblerC2EPNS0_7IsolateEPNS1_5GraphEPNS1_14CallDescriptorENS0_21MachineRepresentationENS_4base5FlagsINS1_22MachineOperatorBuilder4FlagEjEENSC_21AlignmentRequirementsENS0_24PoisoningMitigationLevelE, @function
_GLOBAL__sub_I__ZN2v88internal8compiler19RawMachineAssemblerC2EPNS0_7IsolateEPNS1_5GraphEPNS1_14CallDescriptorENS0_21MachineRepresentationENS_4base5FlagsINS1_22MachineOperatorBuilder4FlagEjEENSC_21AlignmentRequirementsENS0_24PoisoningMitigationLevelE:
.LFB27432:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE27432:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler19RawMachineAssemblerC2EPNS0_7IsolateEPNS1_5GraphEPNS1_14CallDescriptorENS0_21MachineRepresentationENS_4base5FlagsINS1_22MachineOperatorBuilder4FlagEjEENSC_21AlignmentRequirementsENS0_24PoisoningMitigationLevelE, .-_GLOBAL__sub_I__ZN2v88internal8compiler19RawMachineAssemblerC2EPNS0_7IsolateEPNS1_5GraphEPNS1_14CallDescriptorENS0_21MachineRepresentationENS_4base5FlagsINS1_22MachineOperatorBuilder4FlagEjEENSC_21AlignmentRequirementsENS0_24PoisoningMitigationLevelE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler19RawMachineAssemblerC2EPNS0_7IsolateEPNS1_5GraphEPNS1_14CallDescriptorENS0_21MachineRepresentationENS_4base5FlagsINS1_22MachineOperatorBuilder4FlagEjEENSC_21AlignmentRequirementsENS0_24PoisoningMitigationLevelE
	.hidden	_ZTCN2v88internal12StdoutStreamE0_So
	.weak	_ZTCN2v88internal12StdoutStreamE0_So
	.section	.rodata._ZTCN2v88internal12StdoutStreamE0_So,"aG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTCN2v88internal12StdoutStreamE0_So, @object
	.size	_ZTCN2v88internal12StdoutStreamE0_So, 80
_ZTCN2v88internal12StdoutStreamE0_So:
	.quad	80
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-80
	.quad	-80
	.quad	0
	.quad	0
	.quad	0
	.hidden	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE
	.weak	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE
	.section	.rodata._ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE,"aG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE, @object
	.size	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE, 80
_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE:
	.quad	80
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-80
	.quad	-80
	.quad	0
	.quad	0
	.quad	0
	.weak	_ZTTN2v88internal12StdoutStreamE
	.section	.data.rel.ro.local._ZTTN2v88internal12StdoutStreamE,"awG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTTN2v88internal12StdoutStreamE, @object
	.size	_ZTTN2v88internal12StdoutStreamE, 48
_ZTTN2v88internal12StdoutStreamE:
	.quad	_ZTVN2v88internal12StdoutStreamE+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_So+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_So+64
	.quad	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE+64
	.quad	_ZTVN2v88internal12StdoutStreamE+64
	.weak	_ZTVN2v88internal12StdoutStreamE
	.section	.data.rel.ro.local._ZTVN2v88internal12StdoutStreamE,"awG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTVN2v88internal12StdoutStreamE, @object
	.size	_ZTVN2v88internal12StdoutStreamE, 80
_ZTVN2v88internal12StdoutStreamE:
	.quad	80
	.quad	0
	.quad	0
	.quad	_ZN2v88internal12StdoutStreamD1Ev
	.quad	_ZN2v88internal12StdoutStreamD0Ev
	.quad	-80
	.quad	-80
	.quad	0
	.quad	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.quad	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC6:
	.value	255
	.value	255
	.value	255
	.value	255
	.value	255
	.value	255
	.value	255
	.value	255
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
