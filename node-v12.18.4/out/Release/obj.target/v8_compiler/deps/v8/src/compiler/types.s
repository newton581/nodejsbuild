	.file	"types.cc"
	.text
	.section	.text._ZN2v88internal8compiler4Type14NormalizeUnionEPNS1_9UnionTypeEiPNS0_4ZoneE.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler4Type14NormalizeUnionEPNS1_9UnionTypeEiPNS0_4ZoneE.part.0, @function
_ZN2v88internal8compiler4Type14NormalizeUnionEPNS1_9UnionTypeEiPNS0_4ZoneE.part.0:
.LFB22447:
	.cfi_startproc
	movq	8(%rdi), %rcx
	movq	%rdi, %rax
	cmpl	$1, (%rcx)
	jne	.L2
	cmpl	$2, %esi
	jne	.L2
	movq	8(%rcx), %rcx
	testb	$1, %cl
	jne	.L2
	cmpl	$4, (%rcx)
	je	.L41
.L2:
	movl	%esi, 4(%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movsd	8(%rcx), %xmm0
	movsd	16(%rcx), %xmm1
	movsd	.LC0(%rip), %xmm2
	comisd	%xmm0, %xmm2
	jbe	.L35
	comisd	%xmm1, %xmm2
	ja	.L22
	movl	$16, %eax
.L3:
	movsd	.LC1(%rip), %xmm2
	comisd	%xmm0, %xmm2
	jbe	.L6
	orl	$8, %eax
	comisd	%xmm1, %xmm2
	movl	%eax, %ebx
	ja	.L17
.L6:
	pxor	%xmm2, %xmm2
	comisd	%xmm0, %xmm2
	jbe	.L8
	orl	$64, %eax
	comisd	%xmm1, %xmm2
	movl	%eax, %ebx
	ja	.L17
.L8:
	movsd	.LC3(%rip), %xmm2
	comisd	%xmm0, %xmm2
	jbe	.L10
	orb	$4, %ah
	comisd	%xmm1, %xmm2
	movl	%eax, %ebx
	ja	.L17
.L10:
	movsd	.LC4(%rip), %xmm2
	comisd	%xmm0, %xmm2
	jbe	.L12
	orl	$2, %eax
	comisd	%xmm1, %xmm2
	movl	%eax, %ebx
	ja	.L17
.L12:
	movsd	.LC5(%rip), %xmm2
	movl	%eax, %ebx
	comisd	%xmm0, %xmm2
	ja	.L42
.L14:
	orl	$16, %ebx
.L17:
	movq	16(%rdx), %rax
	movq	24(%rdx), %rcx
	subq	%rax, %rcx
	cmpq	$23, %rcx
	jbe	.L43
	leaq	24(%rax), %rcx
	movq	%rcx, 16(%rdx)
.L19:
	unpcklpd	%xmm1, %xmm0
	movl	$4, (%rax)
	movl	%ebx, 4(%rax)
	movups	%xmm0, 8(%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore_state
	xorl	%eax, %eax
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L42:
	orl	$4, %ebx
	comisd	%xmm1, %xmm2
	jbe	.L14
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L43:
	movl	$24, %esi
	movq	%rdx, %rdi
	movsd	%xmm0, -32(%rbp)
	movsd	%xmm1, -24(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movsd	-24(%rbp), %xmm1
	movsd	-32(%rbp), %xmm0
	jmp	.L19
.L22:
	movl	$16, %ebx
	jmp	.L17
	.cfi_endproc
.LFE22447:
	.size	_ZN2v88internal8compiler4Type14NormalizeUnionEPNS1_9UnionTypeEiPNS0_4ZoneE.part.0, .-_ZN2v88internal8compiler4Type14NormalizeUnionEPNS1_9UnionTypeEiPNS0_4ZoneE.part.0
	.section	.text._ZN2v88internal8compiler9RangeType3NewENS2_6LimitsEPNS0_4ZoneE,"axG",@progbits,_ZN2v88internal8compiler9RangeType3NewENS2_6LimitsEPNS0_4ZoneE,comdat
	.p2align 4
	.weak	_ZN2v88internal8compiler9RangeType3NewENS2_6LimitsEPNS0_4ZoneE
	.type	_ZN2v88internal8compiler9RangeType3NewENS2_6LimitsEPNS0_4ZoneE, @function
_ZN2v88internal8compiler9RangeType3NewENS2_6LimitsEPNS0_4ZoneE:
.LFB10021:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movsd	.LC0(%rip), %xmm2
	comisd	%xmm0, %xmm2
	jbe	.L70
	comisd	%xmm1, %xmm2
	ja	.L63
	movsd	.LC1(%rip), %xmm2
	movl	$16, %eax
	comisd	%xmm0, %xmm2
	jbe	.L48
	orl	$8, %eax
	comisd	%xmm1, %xmm2
	movl	%eax, %ebx
	ja	.L59
.L48:
	pxor	%xmm2, %xmm2
	comisd	%xmm0, %xmm2
	jbe	.L50
	orl	$64, %eax
	comisd	%xmm1, %xmm2
	movl	%eax, %ebx
	ja	.L59
.L50:
	movsd	.LC3(%rip), %xmm2
	comisd	%xmm0, %xmm2
	jbe	.L52
	orb	$4, %ah
	comisd	%xmm1, %xmm2
	movl	%eax, %ebx
	ja	.L59
.L52:
	movsd	.LC4(%rip), %xmm2
	comisd	%xmm0, %xmm2
	jbe	.L54
	orl	$2, %eax
	comisd	%xmm1, %xmm2
	movl	%eax, %ebx
	ja	.L59
.L54:
	movsd	.LC5(%rip), %xmm2
	movl	%eax, %ebx
	comisd	%xmm0, %xmm2
	ja	.L72
.L56:
	orl	$16, %ebx
.L59:
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$23, %rdx
	jbe	.L73
	leaq	24(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L61:
	unpcklpd	%xmm1, %xmm0
	movl	$4, (%rax)
	movl	%ebx, 4(%rax)
	movups	%xmm0, 8(%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L70:
	.cfi_restore_state
	movsd	.LC1(%rip), %xmm2
	xorl	%eax, %eax
	comisd	%xmm0, %xmm2
	jbe	.L48
	orl	$8, %eax
	comisd	%xmm1, %xmm2
	movl	%eax, %ebx
	jbe	.L48
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L72:
	orl	$4, %ebx
	comisd	%xmm1, %xmm2
	jbe	.L56
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L73:
	movl	$24, %esi
	movsd	%xmm1, -32(%rbp)
	movsd	%xmm0, -24(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movsd	-24(%rbp), %xmm0
	movsd	-32(%rbp), %xmm1
	jmp	.L61
.L63:
	movl	$16, %ebx
	jmp	.L59
	.cfi_endproc
.LFE10021:
	.size	_ZN2v88internal8compiler9RangeType3NewENS2_6LimitsEPNS0_4ZoneE, .-_ZN2v88internal8compiler9RangeType3NewENS2_6LimitsEPNS0_4ZoneE
	.section	.text._ZN2v88internal8compiler9RangeType6Limits7IsEmptyEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler9RangeType6Limits7IsEmptyEv
	.type	_ZN2v88internal8compiler9RangeType6Limits7IsEmptyEv, @function
_ZN2v88internal8compiler9RangeType6Limits7IsEmptyEv:
.LFB18567:
	.cfi_startproc
	endbr64
	movsd	(%rdi), %xmm0
	comisd	8(%rdi), %xmm0
	seta	%al
	ret
	.cfi_endproc
.LFE18567:
	.size	_ZN2v88internal8compiler9RangeType6Limits7IsEmptyEv, .-_ZN2v88internal8compiler9RangeType6Limits7IsEmptyEv
	.section	.text._ZN2v88internal8compiler9RangeType6Limits9IntersectES3_S3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler9RangeType6Limits9IntersectES3_S3_
	.type	_ZN2v88internal8compiler9RangeType6Limits9IntersectES3_S3_, @function
_ZN2v88internal8compiler9RangeType6Limits9IntersectES3_S3_:
.LFB18568:
	.cfi_startproc
	endbr64
	maxsd	%xmm0, %xmm2
	minsd	%xmm1, %xmm3
	movapd	%xmm2, %xmm0
	movapd	%xmm3, %xmm1
	ret
	.cfi_endproc
.LFE18568:
	.size	_ZN2v88internal8compiler9RangeType6Limits9IntersectES3_S3_, .-_ZN2v88internal8compiler9RangeType6Limits9IntersectES3_S3_
	.section	.text._ZN2v88internal8compiler9RangeType6Limits5UnionES3_S3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler9RangeType6Limits5UnionES3_S3_
	.type	_ZN2v88internal8compiler9RangeType6Limits5UnionES3_S3_, @function
_ZN2v88internal8compiler9RangeType6Limits5UnionES3_S3_:
.LFB18569:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	comisd	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	jbe	.L96
	movapd	%xmm2, %xmm5
	unpcklpd	%xmm3, %xmm5
	movaps	%xmm5, -16(%rbp)
.L85:
	movsd	-16(%rbp), %xmm0
	movsd	-8(%rbp), %xmm1
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L96:
	.cfi_restore_state
	comisd	%xmm3, %xmm2
	movapd	%xmm0, %xmm6
	unpcklpd	%xmm1, %xmm6
	movaps	%xmm6, -16(%rbp)
	ja	.L85
	movapd	%xmm2, %xmm4
	cmpnltsd	%xmm0, %xmm4
	andpd	%xmm4, %xmm0
	andnpd	%xmm2, %xmm4
	movapd	%xmm1, %xmm2
	cmpnltsd	%xmm3, %xmm2
	orpd	%xmm4, %xmm0
	movapd	%xmm0, %xmm7
	andpd	%xmm2, %xmm1
	andnpd	%xmm3, %xmm2
	orpd	%xmm2, %xmm1
	unpcklpd	%xmm1, %xmm7
	movaps	%xmm7, -16(%rbp)
	movsd	-16(%rbp), %xmm0
	movsd	-8(%rbp), %xmm1
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18569:
	.size	_ZN2v88internal8compiler9RangeType6Limits5UnionES3_S3_, .-_ZN2v88internal8compiler9RangeType6Limits5UnionES3_S3_
	.section	.text._ZN2v88internal8compiler4Type7OverlapEPKNS1_9RangeTypeES5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler4Type7OverlapEPKNS1_9RangeTypeES5_
	.type	_ZN2v88internal8compiler4Type7OverlapEPKNS1_9RangeTypeES5_, @function
_ZN2v88internal8compiler4Type7OverlapEPKNS1_9RangeTypeES5_:
.LFB18570:
	.cfi_startproc
	endbr64
	movsd	8(%rsi), %xmm1
	movsd	16(%rsi), %xmm0
	maxsd	8(%rdi), %xmm1
	minsd	16(%rdi), %xmm0
	comisd	%xmm0, %xmm1
	setbe	%al
	ret
	.cfi_endproc
.LFE18570:
	.size	_ZN2v88internal8compiler4Type7OverlapEPKNS1_9RangeTypeES5_, .-_ZN2v88internal8compiler4Type7OverlapEPKNS1_9RangeTypeES5_
	.section	.text._ZN2v88internal8compiler4Type8ContainsEPKNS1_9RangeTypeES5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler4Type8ContainsEPKNS1_9RangeTypeES5_
	.type	_ZN2v88internal8compiler4Type8ContainsEPKNS1_9RangeTypeES5_, @function
_ZN2v88internal8compiler4Type8ContainsEPKNS1_9RangeTypeES5_:
.LFB18571:
	.cfi_startproc
	endbr64
	movsd	8(%rsi), %xmm0
	comisd	8(%rdi), %xmm0
	jb	.L110
	movsd	16(%rdi), %xmm0
	comisd	16(%rsi), %xmm0
	setnb	%al
	ret
	.p2align 4,,10
	.p2align 3
.L110:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE18571:
	.size	_ZN2v88internal8compiler4Type8ContainsEPKNS1_9RangeTypeES5_, .-_ZN2v88internal8compiler4Type8ContainsEPKNS1_9RangeTypeES5_
	.section	.text._ZNK2v88internal8compiler4Type9BitsetGlbEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler4Type9BitsetGlbEv
	.type	_ZNK2v88internal8compiler4Type9BitsetGlbEv, @function
_ZNK2v88internal8compiler4Type9BitsetGlbEv:
.LFB18574:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testb	$1, %dl
	je	.L112
	movl	%edx, %eax
	xorl	$1, %eax
.L111:
	movq	-24(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L138
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L112:
	.cfi_restore_state
	movl	(%rdx), %ecx
	cmpl	$3, %ecx
	je	.L139
	xorl	%eax, %eax
	cmpl	$4, %ecx
	jne	.L111
	movsd	16(%rdx), %xmm1
	movsd	.LC6(%rip), %xmm0
	comisd	%xmm1, %xmm0
	ja	.L111
	movsd	8(%rdx), %xmm0
	pxor	%xmm2, %xmm2
	comisd	%xmm2, %xmm0
	ja	.L111
	movsd	.LC0(%rip), %xmm3
	comisd	%xmm0, %xmm3
	jnb	.L140
	movsd	.LC1(%rip), %xmm3
	xorl	%edx, %edx
.L115:
	comisd	%xmm0, %xmm3
	movl	%edx, %eax
	jb	.L118
	movsd	.LC7(%rip), %xmm3
	addsd	%xmm1, %xmm3
	comisd	%xmm3, %xmm2
	ja	.L117
	orl	$64, %edx
.L118:
	comisd	%xmm0, %xmm2
	movl	%edx, %eax
	movsd	.LC3(%rip), %xmm2
	jb	.L120
	movsd	.LC7(%rip), %xmm3
	addsd	%xmm1, %xmm3
	comisd	%xmm3, %xmm2
	ja	.L117
	orb	$4, %dh
.L120:
	comisd	%xmm0, %xmm2
	movl	%edx, %eax
	movsd	.LC4(%rip), %xmm2
	jb	.L122
	movsd	.LC7(%rip), %xmm3
	addsd	%xmm1, %xmm3
	comisd	%xmm3, %xmm2
	ja	.L117
	orl	$1026, %edx
.L122:
	comisd	%xmm0, %xmm2
	movl	%edx, %eax
	jb	.L117
	addsd	.LC7(%rip), %xmm1
	movsd	.LC5(%rip), %xmm0
	orl	$1030, %edx
	ucomisd	%xmm1, %xmm0
	cmovbe	%edx, %eax
.L117:
	andl	$-17, %eax
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L139:
	movq	8(%rdx), %rax
	movq	%rdi, %rbx
	leaq	-40(%rbp), %rdi
	movq	(%rax), %rax
	movq	%rax, -40(%rbp)
	call	_ZNK2v88internal8compiler4Type9BitsetGlbEv
	leaq	-32(%rbp), %rdi
	movl	%eax, %r12d
	movq	(%rbx), %rax
	movq	8(%rax), %rax
	movq	8(%rax), %rax
	movq	%rax, -32(%rbp)
	call	_ZNK2v88internal8compiler4Type9BitsetGlbEv
	orl	%r12d, %eax
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L140:
	movsd	.LC7(%rip), %xmm4
	movsd	.LC1(%rip), %xmm3
	addsd	%xmm1, %xmm4
	comisd	%xmm4, %xmm3
	ja	.L131
	movl	$72, %edx
	jmp	.L115
.L131:
	xorl	%eax, %eax
	jmp	.L117
.L138:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18574:
	.size	_ZNK2v88internal8compiler4Type9BitsetGlbEv, .-_ZNK2v88internal8compiler4Type9BitsetGlbEv
	.section	.rodata._ZNK2v88internal8compiler4Type9BitsetLubEv.str1.1,"aMS",@progbits,1
.LC8:
	.string	"unreachable code"
	.section	.text._ZNK2v88internal8compiler4Type9BitsetLubEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler4Type9BitsetLubEv
	.type	_ZNK2v88internal8compiler4Type9BitsetLubEv, @function
_ZNK2v88internal8compiler4Type9BitsetLubEv:
.LFB18575:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	testb	$1, %al
	je	.L142
	xorl	$1, %eax
.L141:
	movq	-56(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L154
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L142:
	.cfi_restore_state
	movl	(%rax), %edx
	cmpl	$3, %edx
	je	.L155
	testl	%edx, %edx
	je	.L153
	cmpl	$1, %edx
	je	.L150
	cmpl	$4, %edx
	je	.L153
	cmpl	$2, %edx
	jne	.L156
	movl	$16777216, %eax
	jmp	.L141
	.p2align 4,,10
	.p2align 3
.L153:
	movl	4(%rax), %eax
	jmp	.L141
	.p2align 4,,10
	.p2align 3
.L155:
	movq	8(%rax), %rax
	leaq	-64(%rbp), %r13
	movq	%rdi, %rbx
	movq	%r13, %rdi
	movq	(%rax), %rax
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal8compiler4Type9BitsetLubEv
	movq	(%rbx), %rdx
	movl	%eax, %r15d
	movl	4(%rdx), %ecx
	testl	%ecx, %ecx
	jle	.L141
	leal	-1(%rcx), %r12d
	xorl	%r14d, %r14d
	salq	$3, %r12
.L146:
	movq	8(%rdx), %rax
	movq	%r13, %rdi
	movq	(%rax,%r14), %rax
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal8compiler4Type9BitsetLubEv
	orl	%r15d, %eax
	movl	%eax, %r15d
	cmpq	%r14, %r12
	je	.L141
	movq	(%rbx), %rdx
	addq	$8, %r14
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L150:
	movl	$16, %eax
	jmp	.L141
.L154:
	call	__stack_chk_fail@PLT
.L156:
	leaq	.LC8(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18575:
	.size	_ZNK2v88internal8compiler4Type9BitsetLubEv, .-_ZNK2v88internal8compiler4Type9BitsetLubEv
	.section	.text._ZN2v88internal8compiler10BitsetType3LubINS1_6MapRefEEEjRKT_,"axG",@progbits,_ZN2v88internal8compiler10BitsetType3LubINS1_6MapRefEEEjRKT_,comdat
	.p2align 4
	.weak	_ZN2v88internal8compiler10BitsetType3LubINS1_6MapRefEEEjRKT_
	.type	_ZN2v88internal8compiler10BitsetType3LubINS1_6MapRefEEEjRKT_, @function
_ZN2v88internal8compiler10BitsetType3LubINS1_6MapRefEEEjRKT_:
.LFB19405:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZNK2v88internal8compiler6MapRef13instance_typeEv@PLT
	cmpw	$168, %ax
	ja	.L184
	leaq	.L169(%rip), %rdx
	movzwl	%ax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler10BitsetType3LubINS1_6MapRefEEEjRKT_,"aG",@progbits,_ZN2v88internal8compiler10BitsetType3LubINS1_6MapRefEEEjRKT_,comdat
	.align 4
	.align 4
.L169:
	.long	.L175-.L169
	.long	.L160-.L169
	.long	.L175-.L169
	.long	.L160-.L169
	.long	.L160-.L169
	.long	.L160-.L169
	.long	.L160-.L169
	.long	.L160-.L169
	.long	.L175-.L169
	.long	.L160-.L169
	.long	.L175-.L169
	.long	.L160-.L169
	.long	.L160-.L169
	.long	.L160-.L169
	.long	.L160-.L169
	.long	.L160-.L169
	.long	.L160-.L169
	.long	.L160-.L169
	.long	.L175-.L169
	.long	.L160-.L169
	.long	.L160-.L169
	.long	.L160-.L169
	.long	.L160-.L169
	.long	.L160-.L169
	.long	.L160-.L169
	.long	.L160-.L169
	.long	.L175-.L169
	.long	.L160-.L169
	.long	.L160-.L169
	.long	.L160-.L169
	.long	.L160-.L169
	.long	.L160-.L169
	.long	.L174-.L169
	.long	.L174-.L169
	.long	.L174-.L169
	.long	.L174-.L169
	.long	.L160-.L169
	.long	.L174-.L169
	.long	.L160-.L169
	.long	.L160-.L169
	.long	.L174-.L169
	.long	.L174-.L169
	.long	.L174-.L169
	.long	.L174-.L169
	.long	.L160-.L169
	.long	.L174-.L169
	.long	.L160-.L169
	.long	.L160-.L169
	.long	.L160-.L169
	.long	.L160-.L169
	.long	.L174-.L169
	.long	.L160-.L169
	.long	.L160-.L169
	.long	.L160-.L169
	.long	.L160-.L169
	.long	.L160-.L169
	.long	.L160-.L169
	.long	.L160-.L169
	.long	.L174-.L169
	.long	.L160-.L169
	.long	.L160-.L169
	.long	.L160-.L169
	.long	.L160-.L169
	.long	.L160-.L169
	.long	.L178-.L169
	.long	.L173-.L169
	.long	.L172-.L169
	.long	.L171-.L169
	.long	.L170-.L169
	.long	.L170-.L169
	.long	.L170-.L169
	.long	.L170-.L169
	.long	.L170-.L169
	.long	.L160-.L169
	.long	.L170-.L169
	.long	.L170-.L169
	.long	.L160-.L169
	.long	.L160-.L169
	.long	.L170-.L169
	.long	.L170-.L169
	.long	.L160-.L169
	.long	.L160-.L169
	.long	.L170-.L169
	.long	.L160-.L169
	.long	.L160-.L169
	.long	.L160-.L169
	.long	.L160-.L169
	.long	.L160-.L169
	.long	.L170-.L169
	.long	.L170-.L169
	.long	.L160-.L169
	.long	.L160-.L169
	.long	.L160-.L169
	.long	.L160-.L169
	.long	.L160-.L169
	.long	.L160-.L169
	.long	.L170-.L169
	.long	.L160-.L169
	.long	.L170-.L169
	.long	.L160-.L169
	.long	.L160-.L169
	.long	.L160-.L169
	.long	.L160-.L169
	.long	.L160-.L169
	.long	.L160-.L169
	.long	.L160-.L169
	.long	.L160-.L169
	.long	.L160-.L169
	.long	.L160-.L169
	.long	.L160-.L169
	.long	.L160-.L169
	.long	.L160-.L169
	.long	.L160-.L169
	.long	.L160-.L169
	.long	.L160-.L169
	.long	.L160-.L169
	.long	.L160-.L169
	.long	.L160-.L169
	.long	.L160-.L169
	.long	.L170-.L169
	.long	.L170-.L169
	.long	.L170-.L169
	.long	.L170-.L169
	.long	.L170-.L169
	.long	.L170-.L169
	.long	.L170-.L169
	.long	.L170-.L169
	.long	.L170-.L169
	.long	.L170-.L169
	.long	.L170-.L169
	.long	.L170-.L169
	.long	.L170-.L169
	.long	.L170-.L169
	.long	.L170-.L169
	.long	.L170-.L169
	.long	.L170-.L169
	.long	.L170-.L169
	.long	.L170-.L169
	.long	.L170-.L169
	.long	.L170-.L169
	.long	.L170-.L169
	.long	.L170-.L169
	.long	.L170-.L169
	.long	.L170-.L169
	.long	.L170-.L169
	.long	.L170-.L169
	.long	.L170-.L169
	.long	.L170-.L169
	.long	.L170-.L169
	.long	.L170-.L169
	.long	.L160-.L169
	.long	.L170-.L169
	.long	.L160-.L169
	.long	.L170-.L169
	.long	.L170-.L169
	.long	.L170-.L169
	.long	.L160-.L169
	.long	.L170-.L169
	.long	.L170-.L169
	.long	.L170-.L169
	.long	.L170-.L169
	.long	.L160-.L169
	.long	.L160-.L169
	.long	.L160-.L169
	.long	.L160-.L169
	.long	.L170-.L169
	.long	.L170-.L169
	.long	.L170-.L169
	.long	.L164-.L169
	.section	.text._ZN2v88internal8compiler10BitsetType3LubINS1_6MapRefEEEjRKT_,"axG",@progbits,_ZN2v88internal8compiler10BitsetType3LubINS1_6MapRefEEEjRKT_,comdat
.L164:
	movl	$131072, %eax
.L157:
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L178:
	.cfi_restore_state
	addq	$8, %rsp
	movl	$8192, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L175:
	.cfi_restore_state
	addq	$8, %rsp
	movl	$16384, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L184:
	.cfi_restore_state
	subw	$1024, %ax
	cmpw	$81, %ax
	ja	.L160
	leaq	.L162(%rip), %rdx
	movzwl	%ax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler10BitsetType3LubINS1_6MapRefEEEjRKT_,"aG",@progbits,_ZN2v88internal8compiler10BitsetType3LubINS1_6MapRefEEEjRKT_,comdat
	.align 4
	.align 4
.L162:
	.long	.L167-.L162
	.long	.L165-.L162
	.long	.L165-.L162
	.long	.L164-.L162
	.long	.L160-.L162
	.long	.L160-.L162
	.long	.L160-.L162
	.long	.L160-.L162
	.long	.L160-.L162
	.long	.L160-.L162
	.long	.L160-.L162
	.long	.L160-.L162
	.long	.L160-.L162
	.long	.L160-.L162
	.long	.L160-.L162
	.long	.L160-.L162
	.long	.L165-.L162
	.long	.L164-.L162
	.long	.L160-.L162
	.long	.L160-.L162
	.long	.L160-.L162
	.long	.L160-.L162
	.long	.L160-.L162
	.long	.L160-.L162
	.long	.L160-.L162
	.long	.L160-.L162
	.long	.L160-.L162
	.long	.L160-.L162
	.long	.L160-.L162
	.long	.L160-.L162
	.long	.L160-.L162
	.long	.L160-.L162
	.long	.L165-.L162
	.long	.L165-.L162
	.long	.L165-.L162
	.long	.L164-.L162
	.long	.L164-.L162
	.long	.L166-.L162
	.long	.L164-.L162
	.long	.L164-.L162
	.long	.L164-.L162
	.long	.L164-.L162
	.long	.L164-.L162
	.long	.L165-.L162
	.long	.L164-.L162
	.long	.L164-.L162
	.long	.L164-.L162
	.long	.L164-.L162
	.long	.L164-.L162
	.long	.L164-.L162
	.long	.L164-.L162
	.long	.L164-.L162
	.long	.L164-.L162
	.long	.L164-.L162
	.long	.L164-.L162
	.long	.L164-.L162
	.long	.L164-.L162
	.long	.L164-.L162
	.long	.L164-.L162
	.long	.L164-.L162
	.long	.L164-.L162
	.long	.L164-.L162
	.long	.L164-.L162
	.long	.L164-.L162
	.long	.L164-.L162
	.long	.L164-.L162
	.long	.L164-.L162
	.long	.L164-.L162
	.long	.L164-.L162
	.long	.L164-.L162
	.long	.L164-.L162
	.long	.L164-.L162
	.long	.L164-.L162
	.long	.L164-.L162
	.long	.L164-.L162
	.long	.L164-.L162
	.long	.L164-.L162
	.long	.L164-.L162
	.long	.L164-.L162
	.long	.L164-.L162
	.long	.L177-.L162
	.long	.L161-.L162
	.section	.text._ZN2v88internal8compiler10BitsetType3LubINS1_6MapRefEEEjRKT_,"axG",@progbits,_ZN2v88internal8compiler10BitsetType3LubINS1_6MapRefEEEjRKT_,comdat
.L177:
	addq	$8, %rsp
	movl	$4194304, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L166:
	.cfi_restore_state
	addq	$8, %rsp
	movl	$67108864, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L161:
	.cfi_restore_state
	movl	$2097152, %eax
	jmp	.L157
.L174:
	movl	$16416, %eax
	jmp	.L157
.L173:
	movl	$7262, %eax
	jmp	.L157
.L172:
	movl	$134217728, %eax
	jmp	.L157
.L171:
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler6MapRef12oddball_typeEv@PLT
	subl	$1, %eax
	cmpb	$5, %al
	ja	.L160
	movzbl	%al, %eax
	leaq	CSWTCH.309(%rip), %rdx
	movl	(%rdx,%rax,4), %eax
	jmp	.L157
.L170:
	movl	$16777216, %eax
	jmp	.L157
.L167:
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler6MapRef11is_callableEv@PLT
	cmpb	$1, %al
	sbbl	%eax, %eax
	andl	$524288, %eax
	addl	$524288, %eax
	jmp	.L157
.L165:
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler6MapRef15is_undetectableEv@PLT
	movl	%eax, %r8d
	movl	$262144, %eax
	testb	%r8b, %r8b
	jne	.L157
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler6MapRef11is_callableEv@PLT
	cmpb	$1, %al
	sbbl	%eax, %eax
	andl	$65536, %eax
	addl	$65536, %eax
	jmp	.L157
.L160:
	leaq	.LC8(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19405:
	.size	_ZN2v88internal8compiler10BitsetType3LubINS1_6MapRefEEEjRKT_, .-_ZN2v88internal8compiler10BitsetType3LubINS1_6MapRefEEEjRKT_
	.section	.text._ZN2v88internal8compiler10BitsetType3LubEd,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler10BitsetType3LubEd
	.type	_ZN2v88internal8compiler10BitsetType3LubEd, @function
_ZN2v88internal8compiler10BitsetType3LubEd:
.LFB18577:
	.cfi_startproc
	endbr64
	movabsq	$-9223372036854775808, %rax
	movq	%xmm0, %rcx
	cmpq	%rax, %rcx
	je	.L206
	ucomisd	%xmm0, %xmm0
	jp	.L207
	pxor	%xmm1, %xmm1
	comisd	%xmm1, %xmm0
	jb	.L187
	movsd	.LC9(%rip), %xmm2
	comisd	%xmm0, %xmm2
	jb	.L187
	comisd	%xmm0, %xmm1
	ja	.L231
	movsd	.LC11(%rip), %xmm2
	comisd	%xmm0, %xmm2
	jbe	.L227
	addsd	%xmm0, %xmm2
	movd	%xmm2, %eax
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rax, %xmm2
.L193:
	ucomisd	%xmm2, %xmm0
	jp	.L187
	jne	.L187
	movsd	.LC0(%rip), %xmm2
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L187:
	movsd	.LC0(%rip), %xmm2
	comisd	%xmm2, %xmm0
	jb	.L230
	movsd	.LC12(%rip), %xmm3
	comisd	%xmm0, %xmm3
	jb	.L230
	cvttsd2sil	%xmm0, %eax
	pxor	%xmm3, %xmm3
	cvtsi2sdl	%eax, %xmm3
	ucomisd	%xmm3, %xmm0
	jp	.L230
	jne	.L230
.L197:
	comisd	%xmm0, %xmm2
	ja	.L212
	movsd	.LC1(%rip), %xmm2
	comisd	%xmm0, %xmm2
	ja	.L213
	comisd	%xmm0, %xmm1
	ja	.L214
	movsd	.LC3(%rip), %xmm1
	comisd	%xmm0, %xmm1
	ja	.L215
	movsd	.LC4(%rip), %xmm1
	comisd	%xmm0, %xmm1
	ja	.L216
	movsd	.LC5(%rip), %xmm1
	movl	$96, %edx
	comisd	%xmm0, %xmm1
	ja	.L202
.L230:
	movl	$16, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L231:
	comisd	.LC10(%rip), %xmm0
	jbe	.L227
	movsd	.LC11(%rip), %xmm2
	subsd	%xmm0, %xmm2
	movq	%xmm2, %rax
	pxor	%xmm2, %xmm2
	negl	%eax
	movl	%eax, %eax
	cvtsi2sdq	%rax, %xmm2
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L227:
	movsd	.LC4(%rip), %xmm2
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L206:
	movl	$2048, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L212:
	movl	$16, %edx
.L202:
	leaq	-16+_ZN2v88internal8compiler10BitsetType15BoundariesArrayE(%rip), %rax
	movl	(%rax,%rdx), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L213:
	movl	$32, %edx
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L207:
	movl	$4096, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L214:
	movl	$48, %edx
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L215:
	movl	$64, %edx
	jmp	.L202
.L216:
	movl	$80, %edx
	jmp	.L202
	.cfi_endproc
.LFE18577:
	.size	_ZN2v88internal8compiler10BitsetType3LubEd, .-_ZN2v88internal8compiler10BitsetType3LubEd
	.section	.text._ZN2v88internal8compiler10BitsetType15ExpandInternalsEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler10BitsetType15ExpandInternalsEj
	.type	_ZN2v88internal8compiler10BitsetType15ExpandInternalsEj, @function
_ZN2v88internal8compiler10BitsetType15ExpandInternalsEj:
.LFB18580:
	.cfi_startproc
	endbr64
	movl	%edi, %eax
	testl	$1118, %eax
	je	.L232
	movl	%edi, %edx
	orl	$1118, %edx
	testb	$16, %al
	cmovne	%edx, %eax
	movl	%eax, %edx
	orl	$72, %edx
	testb	$8, %al
	cmovne	%edx, %eax
	movl	%eax, %edx
	orl	$1026, %edx
	testb	$2, %al
	cmovne	%edx, %eax
	movl	%eax, %edx
	orl	$1030, %edx
	testb	$4, %al
	cmovne	%edx, %eax
	movl	%eax, %edx
	orl	$1118, %edx
	testb	$16, %al
	cmovne	%edx, %eax
.L232:
	ret
	.cfi_endproc
.LFE18580:
	.size	_ZN2v88internal8compiler10BitsetType15ExpandInternalsEj, .-_ZN2v88internal8compiler10BitsetType15ExpandInternalsEj
	.section	.text._ZN2v88internal8compiler10BitsetType3LubEdd,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler10BitsetType3LubEdd
	.type	_ZN2v88internal8compiler10BitsetType3LubEdd, @function
_ZN2v88internal8compiler10BitsetType3LubEdd:
.LFB18581:
	.cfi_startproc
	endbr64
	movsd	.LC0(%rip), %xmm2
	comisd	%xmm0, %xmm2
	jbe	.L280
	comisd	%xmm1, %xmm2
	ja	.L273
	movsd	.LC1(%rip), %xmm2
	movl	$16, %edx
	comisd	%xmm0, %xmm2
	jbe	.L260
	movl	%edx, %eax
	orl	$8, %eax
	comisd	%xmm1, %xmm2
	movl	%eax, %edx
	ja	.L256
.L260:
	pxor	%xmm2, %xmm2
	comisd	%xmm0, %xmm2
	jbe	.L262
	movl	%edx, %eax
	orl	$64, %eax
	comisd	%xmm1, %xmm2
	movl	%eax, %edx
	ja	.L256
.L262:
	movsd	.LC3(%rip), %xmm2
	comisd	%xmm0, %xmm2
	jbe	.L264
	movl	%edx, %eax
	orb	$4, %ah
	comisd	%xmm1, %xmm2
	movl	%eax, %edx
	ja	.L256
.L264:
	movsd	.LC4(%rip), %xmm2
	comisd	%xmm0, %xmm2
	jbe	.L266
	movl	%edx, %eax
	orl	$2, %eax
	comisd	%xmm1, %xmm2
	movl	%eax, %edx
	ja	.L256
.L266:
	movsd	.LC5(%rip), %xmm2
	movl	%edx, %eax
	comisd	%xmm0, %xmm2
	jbe	.L268
	orl	$4, %eax
	comisd	%xmm1, %xmm2
	ja	.L281
.L268:
	orl	$16, %eax
.L256:
	ret
	.p2align 4,,10
	.p2align 3
.L280:
	movsd	.LC1(%rip), %xmm2
	xorl	%edx, %edx
	comisd	%xmm0, %xmm2
	jbe	.L260
	movl	%edx, %eax
	orl	$8, %eax
	comisd	%xmm1, %xmm2
	movl	%eax, %edx
	jbe	.L260
	jmp	.L256
.L273:
	movl	$16, %eax
	ret
.L281:
	ret
	.cfi_endproc
.LFE18581:
	.size	_ZN2v88internal8compiler10BitsetType3LubEdd, .-_ZN2v88internal8compiler10BitsetType3LubEdd
	.section	.text._ZN2v88internal8compiler10BitsetType10NumberBitsEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler10BitsetType10NumberBitsEj
	.type	_ZN2v88internal8compiler10BitsetType10NumberBitsEj, @function
_ZN2v88internal8compiler10BitsetType10NumberBitsEj:
.LFB18582:
	.cfi_startproc
	endbr64
	movl	%edi, %eax
	andl	$1118, %eax
	ret
	.cfi_endproc
.LFE18582:
	.size	_ZN2v88internal8compiler10BitsetType10NumberBitsEj, .-_ZN2v88internal8compiler10BitsetType10NumberBitsEj
	.section	.text._ZN2v88internal8compiler10BitsetType3GlbEdd,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler10BitsetType3GlbEdd
	.type	_ZN2v88internal8compiler10BitsetType3GlbEdd, @function
_ZN2v88internal8compiler10BitsetType3GlbEdd:
.LFB18583:
	.cfi_startproc
	endbr64
	movsd	.LC6(%rip), %xmm2
	xorl	%eax, %eax
	comisd	%xmm1, %xmm2
	ja	.L283
	pxor	%xmm2, %xmm2
	comisd	%xmm2, %xmm0
	ja	.L283
	movsd	.LC0(%rip), %xmm3
	comisd	%xmm0, %xmm3
	jb	.L304
	movsd	.LC7(%rip), %xmm4
	movsd	.LC1(%rip), %xmm3
	addsd	%xmm1, %xmm4
	comisd	%xmm4, %xmm3
	ja	.L299
	comisd	%xmm0, %xmm3
	movl	$72, %edx
	movl	%edx, %eax
	jnb	.L305
.L288:
	comisd	%xmm0, %xmm2
	movl	%edx, %eax
	movsd	.LC3(%rip), %xmm2
	jb	.L290
.L302:
	movsd	.LC7(%rip), %xmm3
	addsd	%xmm1, %xmm3
	comisd	%xmm3, %xmm2
	ja	.L287
	orb	$4, %dh
.L290:
	comisd	%xmm0, %xmm2
	movl	%edx, %eax
	movsd	.LC4(%rip), %xmm2
	jb	.L292
	movsd	.LC7(%rip), %xmm3
	addsd	%xmm1, %xmm3
	comisd	%xmm3, %xmm2
	ja	.L287
	orl	$1026, %edx
.L292:
	comisd	%xmm0, %xmm2
	movl	%edx, %eax
	jb	.L287
	addsd	.LC7(%rip), %xmm1
	movsd	.LC5(%rip), %xmm0
	orl	$1030, %edx
	ucomisd	%xmm1, %xmm0
	cmovbe	%edx, %eax
.L287:
	andl	$-17, %eax
.L283:
	ret
	.p2align 4,,10
	.p2align 3
.L304:
	movsd	.LC1(%rip), %xmm3
	xorl	%edx, %edx
	movl	%edx, %eax
	comisd	%xmm0, %xmm3
	jb	.L288
.L305:
	movsd	.LC7(%rip), %xmm3
	addsd	%xmm1, %xmm3
	comisd	%xmm3, %xmm2
	ja	.L287
	orl	$64, %edx
	comisd	%xmm0, %xmm2
	movsd	.LC3(%rip), %xmm2
	movl	%edx, %eax
	jnb	.L302
	jmp	.L290
.L299:
	xorl	%eax, %eax
	jmp	.L287
	.cfi_endproc
.LFE18583:
	.size	_ZN2v88internal8compiler10BitsetType3GlbEdd, .-_ZN2v88internal8compiler10BitsetType3GlbEdd
	.section	.text._ZN2v88internal8compiler10BitsetType3MinEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler10BitsetType3MinEj
	.type	_ZN2v88internal8compiler10BitsetType3MinEj, @function
_ZN2v88internal8compiler10BitsetType3MinEj:
.LFB18584:
	.cfi_startproc
	endbr64
	movl	%edi, %eax
	andl	$16, %edi
	jne	.L310
	movl	%eax, %edx
	orl	$8, %edx
	cmpl	%edx, %eax
	je	.L311
	movl	%eax, %edx
	orl	$64, %edx
	cmpl	%edx, %eax
	je	.L312
	movl	%eax, %edx
	orb	$4, %dh
	cmpl	%edx, %eax
	je	.L313
	movl	%eax, %edx
	orl	$2, %edx
	cmpl	%edx, %eax
	je	.L314
	movl	%eax, %edx
	pxor	%xmm0, %xmm0
	orl	$4, %edx
	cmpl	%edx, %eax
	je	.L319
.L306:
	ret
.L310:
	movsd	.LC13(%rip), %xmm0
	ret
.L311:
	leaq	16+_ZN2v88internal8compiler10BitsetType15BoundariesArrayE(%rip), %rdx
.L308:
	movsd	8(%rdx), %xmm0
	testb	$8, %ah
	je	.L306
	minsd	.LC2(%rip), %xmm0
	ret
.L319:
	leaq	80+_ZN2v88internal8compiler10BitsetType15BoundariesArrayE(%rip), %rdx
	jmp	.L308
.L312:
	leaq	32+_ZN2v88internal8compiler10BitsetType15BoundariesArrayE(%rip), %rdx
	jmp	.L308
.L313:
	leaq	48+_ZN2v88internal8compiler10BitsetType15BoundariesArrayE(%rip), %rdx
	jmp	.L308
.L314:
	leaq	64+_ZN2v88internal8compiler10BitsetType15BoundariesArrayE(%rip), %rdx
	jmp	.L308
	.cfi_endproc
.LFE18584:
	.size	_ZN2v88internal8compiler10BitsetType3MinEj, .-_ZN2v88internal8compiler10BitsetType3MinEj
	.section	.text._ZN2v88internal8compiler10BitsetType3MaxEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler10BitsetType3MaxEj
	.type	_ZN2v88internal8compiler10BitsetType3MaxEj, @function
_ZN2v88internal8compiler10BitsetType3MaxEj:
.LFB18585:
	.cfi_startproc
	endbr64
	movl	%edi, %eax
	andl	$16, %edi
	movsd	.LC14(%rip), %xmm0
	jne	.L320
	movl	%eax, %edx
	orl	$4, %edx
	cmpl	%eax, %edx
	je	.L325
	movl	%eax, %edx
	orl	$2, %edx
	cmpl	%edx, %eax
	je	.L326
	movl	%eax, %edx
	orb	$4, %dh
	cmpl	%edx, %eax
	je	.L327
	movl	%eax, %edx
	orl	$64, %edx
	cmpl	%edx, %eax
	je	.L328
	movl	%eax, %edx
	pxor	%xmm0, %xmm0
	orl	$8, %edx
	cmpl	%edx, %eax
	je	.L333
.L320:
	ret
.L333:
	movl	$2, %edx
	.p2align 4,,10
	.p2align 3
.L322:
	salq	$4, %rdx
	leaq	_ZN2v88internal8compiler10BitsetType15BoundariesArrayE(%rip), %rcx
	movsd	8(%rcx,%rdx), %xmm0
	subsd	.LC7(%rip), %xmm0
	testb	$8, %ah
	je	.L320
	maxsd	.LC2(%rip), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L325:
	movl	$6, %edx
	jmp	.L322
.L326:
	movl	$5, %edx
	jmp	.L322
.L327:
	movl	$4, %edx
	jmp	.L322
.L328:
	movl	$3, %edx
	jmp	.L322
	.cfi_endproc
.LFE18585:
	.size	_ZN2v88internal8compiler10BitsetType3MaxEj, .-_ZN2v88internal8compiler10BitsetType3MaxEj
	.section	.text._ZN2v88internal8compiler23OtherNumberConstantType21IsOtherNumberConstantEd,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23OtherNumberConstantType21IsOtherNumberConstantEd
	.type	_ZN2v88internal8compiler23OtherNumberConstantType21IsOtherNumberConstantEd, @function
_ZN2v88internal8compiler23OtherNumberConstantType21IsOtherNumberConstantEd:
.LFB18586:
	.cfi_startproc
	endbr64
	ucomisd	%xmm0, %xmm0
	jp	.L338
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movsd	%xmm0, -8(%rbp)
	call	nearbyint@PLT
	movsd	-8(%rbp), %xmm1
	ucomisd	%xmm0, %xmm1
	movq	%xmm1, %rdx
	jp	.L336
	jne	.L336
	movabsq	$-9223372036854775808, %rcx
	xorl	%eax, %eax
	cmpq	%rcx, %rdx
	je	.L336
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L336:
	.cfi_restore_state
	movabsq	$-9223372036854775808, %rax
	leave
	.cfi_def_cfa 7, 8
	cmpq	%rax, %rdx
	setne	%al
	ret
	.p2align 4,,10
	.p2align 3
.L338:
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE18586:
	.size	_ZN2v88internal8compiler23OtherNumberConstantType21IsOtherNumberConstantEd, .-_ZN2v88internal8compiler23OtherNumberConstantType21IsOtherNumberConstantEd
	.section	.text._ZN2v88internal8compiler16HeapConstantTypeC2EjRKNS1_13HeapObjectRefE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16HeapConstantTypeC2EjRKNS1_13HeapObjectRefE
	.type	_ZN2v88internal8compiler16HeapConstantTypeC2EjRKNS1_13HeapObjectRefE, @function
_ZN2v88internal8compiler16HeapConstantTypeC2EjRKNS1_13HeapObjectRefE:
.LFB18588:
	.cfi_startproc
	endbr64
	movl	$0, (%rdi)
	movdqu	(%rdx), %xmm0
	movl	%esi, 4(%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE18588:
	.size	_ZN2v88internal8compiler16HeapConstantTypeC2EjRKNS1_13HeapObjectRefE, .-_ZN2v88internal8compiler16HeapConstantTypeC2EjRKNS1_13HeapObjectRefE
	.globl	_ZN2v88internal8compiler16HeapConstantTypeC1EjRKNS1_13HeapObjectRefE
	.set	_ZN2v88internal8compiler16HeapConstantTypeC1EjRKNS1_13HeapObjectRefE,_ZN2v88internal8compiler16HeapConstantTypeC2EjRKNS1_13HeapObjectRefE
	.section	.text._ZNK2v88internal8compiler16HeapConstantType5ValueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler16HeapConstantType5ValueEv
	.type	_ZNK2v88internal8compiler16HeapConstantType5ValueEv, @function
_ZNK2v88internal8compiler16HeapConstantType5ValueEv:
.LFB18590:
	.cfi_startproc
	endbr64
	addq	$8, %rdi
	jmp	_ZNK2v88internal8compiler13HeapObjectRef6objectEv@PLT
	.cfi_endproc
.LFE18590:
	.size	_ZNK2v88internal8compiler16HeapConstantType5ValueEv, .-_ZNK2v88internal8compiler16HeapConstantType5ValueEv
	.section	.text._ZNK2v88internal8compiler4Type6SlowIsES2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler4Type6SlowIsES2_
	.type	_ZNK2v88internal8compiler4Type6SlowIsES2_, @function
_ZNK2v88internal8compiler4Type6SlowIsES2_:
.LFB18592:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testb	$1, %sil
	jne	.L372
	movq	(%rdi), %rdx
	movl	%edx, %r15d
	andl	$1, %r15d
	jne	.L373
	movl	(%rdx), %eax
	cmpl	$3, %eax
	jne	.L351
	movl	4(%rdx), %eax
	testl	%eax, %eax
	jle	.L360
	leal	-1(%rax), %r13d
	xorl	%ebx, %ebx
	leaq	-64(%rbp), %r14
	salq	$3, %r13
	.p2align 4,,10
	.p2align 3
.L353:
	movq	8(%rdx), %rax
	movq	(%rax,%rbx), %rax
	movq	%rax, -64(%rbp)
	cmpq	%rax, %rsi
	je	.L356
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_
	movl	%eax, %r15d
	testb	%al, %al
	jne	.L356
.L347:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L374
	addq	$40, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L373:
	.cfi_restore_state
	leaq	-72(%rbp), %rdi
	call	_ZNK2v88internal8compiler4Type9BitsetGlbEv
	movq	(%r12), %rdx
	xorl	$1, %edx
	orl	%eax, %edx
	cmpl	%eax, %edx
	sete	%r15b
	jmp	.L347
	.p2align 4,,10
	.p2align 3
.L356:
	cmpq	%rbx, %r13
	je	.L360
	movq	-72(%rbp), %rsi
	movq	(%r12), %rdx
	addq	$8, %rbx
	jmp	.L353
	.p2align 4,,10
	.p2align 3
.L351:
	movl	(%rsi), %ecx
	cmpl	$3, %ecx
	jne	.L357
	movl	4(%rsi), %eax
	testl	%eax, %eax
	jle	.L347
	leal	-1(%rax), %r13d
	xorl	%ebx, %ebx
	jmp	.L359
	.p2align 4,,10
	.p2align 3
.L375:
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_
	testb	%al, %al
	jne	.L360
	cmpq	$1, %rbx
	jbe	.L362
	movq	(%r12), %rax
	testb	$1, %al
	jne	.L362
	cmpl	$4, (%rax)
	je	.L347
.L362:
	leaq	1(%rbx), %rax
	cmpq	%rbx, %r13
	je	.L347
	movq	-72(%rbp), %rsi
	movq	(%r12), %rdx
	movq	%rax, %rbx
.L359:
	movq	8(%rsi), %rax
	movq	(%rax,%rbx,8), %rsi
	cmpq	%rsi, %rdx
	jne	.L375
.L360:
	movl	$1, %r15d
	jmp	.L347
	.p2align 4,,10
	.p2align 3
.L372:
	movl	%esi, %ebx
	call	_ZNK2v88internal8compiler4Type9BitsetLubEv
	xorl	$1, %ebx
	orl	%ebx, %eax
	cmpl	%ebx, %eax
	sete	%r15b
	jmp	.L347
	.p2align 4,,10
	.p2align 3
.L357:
	cmpl	$4, %ecx
	jne	.L363
	cmpl	$4, %eax
	jne	.L347
	movsd	8(%rdx), %xmm0
	comisd	8(%rsi), %xmm0
	jb	.L347
	movsd	16(%rsi), %xmm0
	comisd	16(%rdx), %xmm0
	setnb	%r15b
	jmp	.L347
	.p2align 4,,10
	.p2align 3
.L363:
	cmpl	$4, %eax
	je	.L347
	call	_ZNK2v88internal8compiler4Type12SimplyEqualsES2_
	movl	%eax, %r15d
	jmp	.L347
.L374:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18592:
	.size	_ZNK2v88internal8compiler4Type6SlowIsES2_, .-_ZNK2v88internal8compiler4Type6SlowIsES2_
	.section	.text._ZNK2v88internal8compiler4Type12SimplyEqualsES2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler4Type12SimplyEqualsES2_
	.type	_ZNK2v88internal8compiler4Type12SimplyEqualsES2_, @function
_ZNK2v88internal8compiler4Type12SimplyEqualsES2_:
.LFB18591:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	%r12d, %eax
	andl	$1, %eax
	jne	.L377
	movl	(%r12), %edx
	movq	%rsi, %rbx
	testl	%edx, %edx
	jne	.L378
	testb	$1, %sil
	jne	.L376
	movl	(%rsi), %edx
	testl	%edx, %edx
	je	.L398
	.p2align 4,,10
	.p2align 3
.L376:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L399
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L378:
	.cfi_restore_state
	cmpl	$1, %edx
	jne	.L380
	testb	$1, %sil
	jne	.L376
	cmpl	$1, (%rsi)
	jne	.L376
	movsd	8(%r12), %xmm0
	ucomisd	8(%rsi), %xmm0
	setnp	%dl
	cmove	%edx, %eax
	jmp	.L376
	.p2align 4,,10
	.p2align 3
.L380:
	cmpl	$4, %edx
	jne	.L381
	movl	%esi, %eax
	andl	$1, %eax
	jne	.L377
	cmpl	$1, (%rsi)
	jbe	.L376
	.p2align 4,,10
	.p2align 3
.L377:
	leaq	.LC8(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L381:
	cmpl	$2, %edx
	jne	.L377
	testb	$1, %sil
	jne	.L376
	cmpl	$2, (%rsi)
	jne	.L376
	movl	4(%r12), %edx
	cmpl	%edx, 4(%rsi)
	jne	.L376
	testl	%edx, %edx
	jle	.L386
	leal	-1(%rdx), %eax
	xorl	%r13d, %r13d
	leaq	-72(%rbp), %r15
	leaq	8(,%rax,8), %r14
	.p2align 4,,10
	.p2align 3
.L387:
	movq	8(%r12), %rax
	movq	8(%rbx), %rdx
	movq	(%rax,%r13), %rax
	movq	%rax, -72(%rbp)
	movq	(%rdx,%r13), %rsi
	movq	%rsi, -64(%rbp)
	cmpq	%rax, %rsi
	je	.L388
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_
	testb	%al, %al
	je	.L385
	movq	-72(%rbp), %rsi
	cmpq	-64(%rbp), %rsi
	je	.L388
	leaq	-64(%rbp), %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_
	testb	%al, %al
	je	.L385
.L388:
	addq	$8, %r13
	cmpq	%r13, %r14
	jne	.L387
.L386:
	movl	$1, %eax
	jmp	.L376
	.p2align 4,,10
	.p2align 3
.L385:
	xorl	%eax, %eax
	jmp	.L376
	.p2align 4,,10
	.p2align 3
.L398:
	leaq	8(%r12), %rdi
	call	_ZNK2v88internal8compiler13HeapObjectRef6objectEv@PLT
	leaq	8(%rbx), %rdi
	movq	%rax, %r12
	call	_ZNK2v88internal8compiler13HeapObjectRef6objectEv@PLT
	cmpq	%r12, %rax
	sete	%al
	jmp	.L376
.L399:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18591:
	.size	_ZNK2v88internal8compiler4Type12SimplyEqualsES2_, .-_ZNK2v88internal8compiler4Type12SimplyEqualsES2_
	.section	.text._ZNK2v88internal8compiler4Type3MinEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler4Type3MinEv
	.type	_ZNK2v88internal8compiler4Type3MinEv, @function
_ZNK2v88internal8compiler4Type3MinEv:
.LFB18572:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	testb	$1, %al
	je	.L401
	movl	%eax, %edx
	xorl	$1, %edx
	testb	$16, %al
	jne	.L412
	movl	%edx, %eax
	orl	$8, %eax
	cmpl	%eax, %edx
	je	.L413
	movl	%edx, %eax
	orl	$64, %eax
	cmpl	%eax, %edx
	je	.L414
	movl	%edx, %eax
	orb	$4, %ah
	cmpl	%eax, %edx
	je	.L415
	movl	%edx, %eax
	orl	$2, %eax
	cmpl	%eax, %edx
	je	.L416
	movl	%edx, %eax
	pxor	%xmm1, %xmm1
	orl	$4, %eax
	cmpl	%eax, %edx
	je	.L429
.L400:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L430
	addq	$32, %rsp
	movapd	%xmm1, %xmm0
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L401:
	.cfi_restore_state
	cmpl	$3, (%rax)
	je	.L431
	movsd	8(%rax), %xmm1
	jmp	.L400
	.p2align 4,,10
	.p2align 3
.L431:
	movl	4(%rax), %edx
	cmpl	$1, %edx
	jle	.L418
	subl	$2, %edx
	movq	%rdi, %rbx
	leaq	-48(%rbp), %r13
	movl	$8, %r12d
	movsd	.LC14(%rip), %xmm1
	leaq	16(,%rdx,8), %r14
.L408:
	movq	8(%rax), %rax
	movq	%r13, %rdi
	movsd	%xmm1, -56(%rbp)
	movq	(%rax,%r12), %rax
	addq	$8, %r12
	movq	%rax, -48(%rbp)
	call	_ZNK2v88internal8compiler4Type3MinEv
	movsd	-56(%rbp), %xmm1
	movq	(%rbx), %rax
	minsd	%xmm1, %xmm0
	movapd	%xmm0, %xmm1
	cmpq	%r14, %r12
	jne	.L408
.L406:
	movq	8(%rax), %rax
	movq	(%rax), %rax
	movq	%rax, -48(%rbp)
	cmpq	$4097, %rax
	je	.L400
	leaq	-48(%rbp), %r12
	movl	$4097, %esi
	movsd	%xmm1, -56(%rbp)
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_
	movsd	-56(%rbp), %xmm1
	testb	%al, %al
	jne	.L400
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type3MinEv
	movsd	-56(%rbp), %xmm1
	minsd	%xmm1, %xmm0
	movapd	%xmm0, %xmm1
	jmp	.L400
	.p2align 4,,10
	.p2align 3
.L412:
	movsd	.LC13(%rip), %xmm1
	jmp	.L400
	.p2align 4,,10
	.p2align 3
.L418:
	movsd	.LC14(%rip), %xmm1
	jmp	.L406
	.p2align 4,,10
	.p2align 3
.L413:
	leaq	16+_ZN2v88internal8compiler10BitsetType15BoundariesArrayE(%rip), %rax
.L403:
	movsd	8(%rax), %xmm1
	andb	$8, %dh
	je	.L400
	minsd	.LC2(%rip), %xmm1
	jmp	.L400
	.p2align 4,,10
	.p2align 3
.L414:
	leaq	32+_ZN2v88internal8compiler10BitsetType15BoundariesArrayE(%rip), %rax
	jmp	.L403
	.p2align 4,,10
	.p2align 3
.L415:
	leaq	48+_ZN2v88internal8compiler10BitsetType15BoundariesArrayE(%rip), %rax
	jmp	.L403
	.p2align 4,,10
	.p2align 3
.L416:
	leaq	64+_ZN2v88internal8compiler10BitsetType15BoundariesArrayE(%rip), %rax
	jmp	.L403
	.p2align 4,,10
	.p2align 3
.L429:
	leaq	80+_ZN2v88internal8compiler10BitsetType15BoundariesArrayE(%rip), %rax
	jmp	.L403
.L430:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18572:
	.size	_ZNK2v88internal8compiler4Type3MinEv, .-_ZNK2v88internal8compiler4Type3MinEv
	.section	.text._ZNK2v88internal8compiler4Type3MaxEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler4Type3MaxEv
	.type	_ZNK2v88internal8compiler4Type3MaxEv, @function
_ZNK2v88internal8compiler4Type3MaxEv:
.LFB18573:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	testb	$1, %al
	je	.L433
	movl	%eax, %edx
	movsd	.LC14(%rip), %xmm1
	xorl	$1, %edx
	testb	$16, %al
	jne	.L432
	movl	%edx, %eax
	orl	$4, %eax
	cmpl	%eax, %edx
	je	.L446
	movl	%edx, %eax
	orl	$2, %eax
	cmpl	%eax, %edx
	je	.L447
	movl	%edx, %eax
	orb	$4, %ah
	cmpl	%eax, %edx
	je	.L448
	movl	%edx, %eax
	orl	$64, %eax
	cmpl	%eax, %edx
	je	.L449
	movl	%edx, %eax
	pxor	%xmm1, %xmm1
	orl	$8, %eax
	cmpl	%eax, %edx
	je	.L462
.L432:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L463
	addq	$32, %rsp
	movapd	%xmm1, %xmm0
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L433:
	.cfi_restore_state
	movl	(%rax), %edx
	cmpl	$3, %edx
	je	.L464
	cmpl	$4, %edx
	jne	.L443
	movsd	16(%rax), %xmm1
	jmp	.L432
	.p2align 4,,10
	.p2align 3
.L443:
	movsd	8(%rax), %xmm1
	jmp	.L432
	.p2align 4,,10
	.p2align 3
.L464:
	movl	4(%rax), %edx
	cmpl	$1, %edx
	jle	.L451
	subl	$2, %edx
	movq	%rdi, %rbx
	leaq	-48(%rbp), %r13
	movl	$8, %r12d
	movsd	.LC13(%rip), %xmm1
	leaq	16(,%rdx,8), %r14
.L440:
	movq	8(%rax), %rax
	movq	%r13, %rdi
	movsd	%xmm1, -56(%rbp)
	movq	(%rax,%r12), %rax
	addq	$8, %r12
	movq	%rax, -48(%rbp)
	call	_ZNK2v88internal8compiler4Type3MaxEv
	movsd	-56(%rbp), %xmm1
	movq	(%rbx), %rax
	maxsd	%xmm1, %xmm0
	movapd	%xmm0, %xmm1
	cmpq	%r14, %r12
	jne	.L440
.L438:
	movq	8(%rax), %rax
	movq	(%rax), %rax
	movq	%rax, -48(%rbp)
	cmpq	$4097, %rax
	je	.L432
	leaq	-48(%rbp), %r12
	movl	$4097, %esi
	movsd	%xmm1, -56(%rbp)
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_
	movsd	-56(%rbp), %xmm1
	testb	%al, %al
	jne	.L432
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type3MaxEv
	movsd	-56(%rbp), %xmm1
	maxsd	%xmm1, %xmm0
	movapd	%xmm0, %xmm1
	jmp	.L432
	.p2align 4,,10
	.p2align 3
.L462:
	movl	$2, %eax
	.p2align 4,,10
	.p2align 3
.L435:
	salq	$4, %rax
	leaq	_ZN2v88internal8compiler10BitsetType15BoundariesArrayE(%rip), %rcx
	movsd	8(%rcx,%rax), %xmm1
	subsd	.LC7(%rip), %xmm1
	andb	$8, %dh
	je	.L432
	maxsd	.LC2(%rip), %xmm1
	jmp	.L432
	.p2align 4,,10
	.p2align 3
.L451:
	movsd	.LC13(%rip), %xmm1
	jmp	.L438
	.p2align 4,,10
	.p2align 3
.L446:
	movl	$6, %eax
	jmp	.L435
	.p2align 4,,10
	.p2align 3
.L447:
	movl	$5, %eax
	jmp	.L435
	.p2align 4,,10
	.p2align 3
.L448:
	movl	$4, %eax
	jmp	.L435
	.p2align 4,,10
	.p2align 3
.L449:
	movl	$3, %eax
	jmp	.L435
.L463:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18573:
	.size	_ZNK2v88internal8compiler4Type3MaxEv, .-_ZNK2v88internal8compiler4Type3MaxEv
	.section	.text._ZNK2v88internal8compiler4Type5MaybeES2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler4Type5MaybeES2_
	.type	_ZNK2v88internal8compiler4Type5MaybeES2_, @function
_ZNK2v88internal8compiler4Type5MaybeES2_:
.LFB18593:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal8compiler4Type9BitsetLubEv
	movl	%eax, %r14d
	movq	-72(%rbp), %rax
	testb	$1, %al
	je	.L466
	xorl	$1, %eax
.L467:
	testl	%eax, %r14d
	je	.L477
	movq	(%r12), %rax
	testb	$1, %al
	jne	.L475
	movl	(%rax), %ecx
	cmpl	$3, %ecx
	jne	.L476
	movl	4(%rax), %edx
	testl	%edx, %edx
	jle	.L477
	leal	-1(%rdx), %r14d
	xorl	%r13d, %r13d
	leaq	-64(%rbp), %rbx
	salq	$3, %r14
.L479:
	movq	8(%rax), %rax
	movq	-72(%rbp), %rsi
	movq	%rbx, %rdi
	movq	(%rax,%r13), %rax
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal8compiler4Type5MaybeES2_
	testb	%al, %al
	jne	.L483
	cmpq	%r14, %r13
	je	.L477
	movq	(%r12), %rax
	addq	$8, %r13
	jmp	.L479
	.p2align 4,,10
	.p2align 3
.L477:
	xorl	%eax, %eax
.L465:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L540
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L466:
	.cfi_restore_state
	movl	(%rax), %edx
	cmpl	$3, %edx
	je	.L541
	testl	%edx, %edx
	je	.L539
	cmpl	$1, %edx
	je	.L508
	cmpl	$4, %edx
	je	.L539
	cmpl	$2, %edx
	jne	.L542
	movl	$16777216, %eax
	jmp	.L467
	.p2align 4,,10
	.p2align 3
.L475:
	movq	-72(%rbp), %rdx
	testb	$1, %dl
	jne	.L483
	movl	(%rdx), %eax
	cmpl	$3, %eax
	je	.L503
	cmpl	$4, %eax
	jne	.L483
.L501:
	movq	(%r12), %rsi
	leaq	-72(%rbp), %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_
	jmp	.L465
	.p2align 4,,10
	.p2align 3
.L539:
	movl	4(%rax), %eax
	jmp	.L467
	.p2align 4,,10
	.p2align 3
.L476:
	movq	-72(%rbp), %rdx
	testb	$1, %dl
	je	.L533
	cmpl	$4, %ecx
	je	.L543
.L483:
	movl	$1, %eax
	jmp	.L465
	.p2align 4,,10
	.p2align 3
.L533:
	movl	(%rdx), %ecx
	cmpl	$3, %ecx
	jne	.L544
.L503:
	movl	4(%rdx), %eax
	testl	%eax, %eax
	jle	.L477
	leal	-1(%rax), %r13d
	xorl	%ebx, %ebx
	salq	$3, %r13
.L484:
	movq	8(%rdx), %rax
	movq	%r12, %rdi
	movq	(%rax,%rbx), %rsi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_
	testb	%al, %al
	jne	.L483
	cmpq	%rbx, %r13
	je	.L477
	movq	-72(%rbp), %rdx
	addq	$8, %rbx
	jmp	.L484
	.p2align 4,,10
	.p2align 3
.L541:
	movq	8(%rax), %rax
	leaq	-64(%rbp), %rbx
	movq	%rbx, %rdi
	movq	(%rax), %rax
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal8compiler4Type9BitsetLubEv
	movq	-72(%rbp), %rcx
	movl	%eax, %r13d
	movl	4(%rcx), %esi
	testl	%esi, %esi
	jle	.L467
	leal	-1(%rsi), %eax
	xorl	%r15d, %r15d
	salq	$3, %rax
	movq	%rax, -80(%rbp)
.L470:
	movq	8(%rcx), %rax
	movq	%rbx, %rdi
	movq	(%rax,%r15), %rax
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal8compiler4Type9BitsetLubEv
	orl	%r13d, %eax
	movl	%eax, %r13d
	cmpq	%r15, -80(%rbp)
	je	.L467
	movq	-72(%rbp), %rcx
	addq	$8, %r15
	jmp	.L470
	.p2align 4,,10
	.p2align 3
.L508:
	movl	$16, %eax
	jmp	.L467
	.p2align 4,,10
	.p2align 3
.L543:
	movl	%edx, %ebx
	andl	$1118, %ebx
	je	.L477
	movq	%r12, %rdi
	movl	%ebx, %r13d
	call	_ZNK2v88internal8compiler4Type3MinEv
	orl	$16, %r13d
	movapd	%xmm0, %xmm1
	cmpl	%ebx, %r13d
	je	.L510
	movl	%ebx, %eax
	orl	$8, %eax
	cmpl	%eax, %ebx
	je	.L511
	movl	%ebx, %eax
	orl	$64, %eax
	cmpl	%eax, %ebx
	je	.L512
	movl	%ebx, %eax
	orb	$4, %ah
	cmpl	%ebx, %eax
	je	.L513
	movl	%ebx, %eax
	orl	$2, %eax
	cmpl	%eax, %ebx
	je	.L514
	movl	%ebx, %eax
	orl	$4, %eax
	cmpl	%eax, %ebx
	je	.L515
	pxor	%xmm3, %xmm3
	comisd	%xmm3, %xmm0
	ja	.L545
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type3MaxEv
	pxor	%xmm1, %xmm1
.L505:
	movl	%ebx, %eax
	orl	$4, %eax
	cmpl	%eax, %ebx
	je	.L517
	movl	%ebx, %eax
	orl	$2, %eax
	cmpl	%eax, %ebx
	je	.L518
	movl	%ebx, %eax
	orb	$4, %ah
	cmpl	%eax, %ebx
	je	.L519
	movl	%ebx, %eax
	orl	$64, %eax
	cmpl	%eax, %ebx
	je	.L520
	movl	%ebx, %eax
	pxor	%xmm2, %xmm2
	orl	$8, %eax
	cmpl	%ebx, %eax
	je	.L546
.L494:
	minsd	%xmm2, %xmm0
	comisd	%xmm1, %xmm0
	setnb	%al
	jmp	.L465
	.p2align 4,,10
	.p2align 3
.L544:
	cmpl	$4, (%rax)
	jne	.L547
	cmpl	$4, %ecx
	jne	.L485
	movsd	8(%rdx), %xmm1
	movsd	16(%rdx), %xmm0
	maxsd	8(%rax), %xmm1
	minsd	16(%rax), %xmm0
	comisd	%xmm0, %xmm1
	setbe	%al
	jmp	.L465
.L547:
	cmpl	$4, (%rdx)
	je	.L501
	.p2align 4,,10
	.p2align 3
.L485:
	movq	-72(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type12SimplyEqualsES2_
	jmp	.L465
.L545:
	movq	%r12, %rdi
	movsd	%xmm0, -80(%rbp)
	call	_ZNK2v88internal8compiler4Type3MaxEv
	movsd	-80(%rbp), %xmm1
	jmp	.L505
.L510:
	leaq	_ZN2v88internal8compiler10BitsetType15BoundariesArrayE(%rip), %rax
.L490:
	maxsd	8(%rax), %xmm1
	movq	%r12, %rdi
	movsd	%xmm1, -80(%rbp)
	call	_ZNK2v88internal8compiler4Type3MaxEv
	cmpl	%ebx, %r13d
	movsd	-80(%rbp), %xmm1
	jne	.L505
	movsd	.LC14(%rip), %xmm2
	jmp	.L494
.L511:
	leaq	16+_ZN2v88internal8compiler10BitsetType15BoundariesArrayE(%rip), %rax
	jmp	.L490
.L540:
	call	__stack_chk_fail@PLT
.L515:
	leaq	80+_ZN2v88internal8compiler10BitsetType15BoundariesArrayE(%rip), %rax
	jmp	.L490
.L514:
	leaq	64+_ZN2v88internal8compiler10BitsetType15BoundariesArrayE(%rip), %rax
	jmp	.L490
.L513:
	leaq	48+_ZN2v88internal8compiler10BitsetType15BoundariesArrayE(%rip), %rax
	jmp	.L490
.L512:
	leaq	32+_ZN2v88internal8compiler10BitsetType15BoundariesArrayE(%rip), %rax
	jmp	.L490
.L517:
	movl	$6, %eax
.L495:
	salq	$4, %rax
	leaq	_ZN2v88internal8compiler10BitsetType15BoundariesArrayE(%rip), %rdx
	movsd	8(%rdx,%rax), %xmm2
	subsd	.LC7(%rip), %xmm2
	jmp	.L494
.L520:
	movl	$3, %eax
	jmp	.L495
.L519:
	movl	$4, %eax
	jmp	.L495
.L518:
	movl	$5, %eax
	jmp	.L495
.L546:
	movl	$2, %eax
	jmp	.L495
.L542:
	leaq	.LC8(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18593:
	.size	_ZNK2v88internal8compiler4Type5MaybeES2_, .-_ZNK2v88internal8compiler4Type5MaybeES2_
	.section	.text._ZNK2v88internal8compiler4Type8GetRangeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler4Type8GetRangeEv
	.type	_ZNK2v88internal8compiler4Type8GetRangeEv, @function
_ZNK2v88internal8compiler4Type8GetRangeEv:
.LFB18594:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	testb	$1, %al
	jne	.L549
	movl	(%rax), %edx
	cmpl	$4, %edx
	je	.L551
	cmpl	$3, %edx
	jne	.L549
	movq	8(%rax), %rax
	movq	8(%rax), %rax
	testb	$1, %al
	jne	.L549
	cmpl	$4, (%rax)
	jne	.L549
.L551:
	ret
	.p2align 4,,10
	.p2align 3
.L549:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE18594:
	.size	_ZNK2v88internal8compiler4Type8GetRangeEv, .-_ZNK2v88internal8compiler4Type8GetRangeEv
	.section	.text._ZNK2v88internal8compiler9UnionType10WellformedEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler9UnionType10WellformedEv
	.type	_ZNK2v88internal8compiler9UnionType10WellformedEv, @function
_ZNK2v88internal8compiler9UnionType10WellformedEv:
.LFB18595:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE18595:
	.size	_ZNK2v88internal8compiler9UnionType10WellformedEv, .-_ZNK2v88internal8compiler9UnionType10WellformedEv
	.section	.text._ZN2v88internal8compiler4Type11UpdateRangeES2_PNS1_9UnionTypeEiPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler4Type11UpdateRangeES2_PNS1_9UnionTypeEiPNS0_4ZoneE
	.type	_ZN2v88internal8compiler4Type11UpdateRangeES2_PNS1_9UnionTypeEiPNS0_4ZoneE, @function
_ZN2v88internal8compiler4Type11UpdateRangeES2_PNS1_9UnionTypeEiPNS0_4ZoneE:
.LFB18597:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rsi), %rax
	cmpl	$1, %edx
	je	.L564
	movq	8(%rax), %rcx
	leal	1(%rdx), %r12d
	movslq	%edx, %rdx
	movq	%rsi, %r13
	movq	%rcx, (%rax,%rdx,8)
	movq	8(%rsi), %rax
	movq	%rdi, 8(%rax)
	cmpl	$2, %r12d
	jle	.L554
	leaq	-64(%rbp), %rax
	movl	$2, %r14d
	movq	%rax, -72(%rbp)
	jmp	.L557
	.p2align 4,,10
	.p2align 3
.L565:
	movq	8(%r13), %rax
	leaq	(%rax,%rbx), %rcx
.L559:
	subl	$1, %r12d
	movslq	%r12d, %rdx
	movq	(%rax,%rdx,8), %rax
	movq	%rax, (%rcx)
	cmpl	%r12d, %r14d
	jge	.L554
.L557:
	movq	8(%r13), %rax
	movslq	%r14d, %rbx
	salq	$3, %rbx
	leaq	(%rax,%rbx), %rcx
	movq	(%rcx), %rdx
	movq	%rdx, -64(%rbp)
	cmpq	%r15, %rdx
	je	.L559
	movq	-72(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_
	testb	%al, %al
	jne	.L565
	addl	$1, %r14d
	cmpl	%r12d, %r14d
	jl	.L557
	.p2align 4,,10
	.p2align 3
.L554:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L566
	addq	$40, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L564:
	.cfi_restore_state
	movq	%rdi, 8(%rax)
	movl	$2, %r12d
	jmp	.L554
.L566:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18597:
	.size	_ZN2v88internal8compiler4Type11UpdateRangeES2_PNS1_9UnionTypeEiPNS0_4ZoneE, .-_ZN2v88internal8compiler4Type11UpdateRangeES2_PNS1_9UnionTypeEiPNS0_4ZoneE
	.section	.text._ZN2v88internal8compiler4Type8ToLimitsEjPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler4Type8ToLimitsEjPNS0_4ZoneE
	.type	_ZN2v88internal8compiler4Type8ToLimitsEjPNS0_4ZoneE, @function
_ZN2v88internal8compiler4Type8ToLimitsEjPNS0_4ZoneE:
.LFB18598:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	andl	$1118, %edi
	jne	.L568
	movabsq	$4607182418800017408, %rax
	xorl	%edx, %edx
	movq	%rax, -16(%rbp)
	movsd	-16(%rbp), %xmm0
	movq	%rdx, -8(%rbp)
	movsd	-8(%rbp), %xmm1
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L568:
	.cfi_restore_state
	movl	%edi, %eax
	orl	$16, %eax
	cmpl	%eax, %edi
	je	.L591
	movl	%edi, %eax
	orl	$4, %eax
	cmpl	%eax, %edi
	je	.L581
	movl	%edi, %eax
	orl	$2, %eax
	cmpl	%eax, %edi
	je	.L582
	movl	%edi, %eax
	orb	$4, %ah
	cmpl	%eax, %edi
	je	.L583
	movl	%edi, %eax
	orl	$64, %eax
	cmpl	%eax, %edi
	je	.L584
	movl	%edi, %eax
	orl	$8, %eax
	cmpl	%eax, %edi
	je	.L585
	pxor	%xmm0, %xmm0
.L572:
	movl	%edi, %eax
	orl	$8, %eax
	cmpl	%eax, %edi
	je	.L592
	movl	%edi, %eax
	orl	$64, %eax
	cmpl	%edi, %eax
	je	.L577
	movl	%edi, %eax
	orb	$4, %ah
	cmpl	%eax, %edi
	je	.L578
	movl	%edi, %eax
	orl	$2, %eax
	cmpl	%eax, %edi
	je	.L579
	movl	%edi, %eax
	pxor	%xmm1, %xmm1
	orl	$4, %eax
	cmpl	%eax, %edi
	je	.L593
.L573:
	movapd	%xmm1, %xmm2
	unpcklpd	%xmm0, %xmm2
	movaps	%xmm2, -16(%rbp)
	movsd	-16(%rbp), %xmm0
	movsd	-8(%rbp), %xmm1
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L591:
	.cfi_restore_state
	movsd	.LC14(%rip), %xmm0
	leaq	_ZN2v88internal8compiler10BitsetType15BoundariesArrayE(%rip), %rax
.L570:
	movsd	8(%rax), %xmm1
	jmp	.L573
	.p2align 4,,10
	.p2align 3
.L592:
	leaq	16+_ZN2v88internal8compiler10BitsetType15BoundariesArrayE(%rip), %rax
	jmp	.L570
	.p2align 4,,10
	.p2align 3
.L577:
	leaq	32+_ZN2v88internal8compiler10BitsetType15BoundariesArrayE(%rip), %rax
	jmp	.L570
	.p2align 4,,10
	.p2align 3
.L578:
	leaq	48+_ZN2v88internal8compiler10BitsetType15BoundariesArrayE(%rip), %rax
	jmp	.L570
	.p2align 4,,10
	.p2align 3
.L579:
	leaq	64+_ZN2v88internal8compiler10BitsetType15BoundariesArrayE(%rip), %rax
	jmp	.L570
	.p2align 4,,10
	.p2align 3
.L593:
	leaq	80+_ZN2v88internal8compiler10BitsetType15BoundariesArrayE(%rip), %rax
	jmp	.L570
	.p2align 4,,10
	.p2align 3
.L581:
	movl	$6, %eax
.L574:
	salq	$4, %rax
	leaq	_ZN2v88internal8compiler10BitsetType15BoundariesArrayE(%rip), %rdx
	movsd	8(%rdx,%rax), %xmm0
	subsd	.LC7(%rip), %xmm0
	jmp	.L572
	.p2align 4,,10
	.p2align 3
.L582:
	movl	$5, %eax
	jmp	.L574
	.p2align 4,,10
	.p2align 3
.L583:
	movl	$4, %eax
	jmp	.L574
	.p2align 4,,10
	.p2align 3
.L584:
	movl	$3, %eax
	jmp	.L574
	.p2align 4,,10
	.p2align 3
.L585:
	movl	$2, %eax
	jmp	.L574
	.cfi_endproc
.LFE18598:
	.size	_ZN2v88internal8compiler4Type8ToLimitsEjPNS0_4ZoneE, .-_ZN2v88internal8compiler4Type8ToLimitsEjPNS0_4ZoneE
	.section	.text._ZN2v88internal8compiler4Type23IntersectRangeAndBitsetES2_S2_PNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler4Type23IntersectRangeAndBitsetES2_S2_PNS0_4ZoneE
	.type	_ZN2v88internal8compiler4Type23IntersectRangeAndBitsetES2_S2_PNS0_4ZoneE, @function
_ZN2v88internal8compiler4Type23IntersectRangeAndBitsetES2_S2_PNS0_4ZoneE:
.LFB18599:
	.cfi_startproc
	endbr64
	movsd	8(%rdi), %xmm0
	movsd	16(%rdi), %xmm1
	andl	$1118, %esi
	je	.L606
	movl	%esi, %eax
	orl	$16, %eax
	cmpl	%eax, %esi
	je	.L623
	movl	%esi, %eax
	orl	$4, %eax
	cmpl	%eax, %esi
	je	.L612
	movl	%esi, %eax
	orl	$2, %eax
	cmpl	%eax, %esi
	je	.L613
	movl	%esi, %eax
	orb	$4, %ah
	cmpl	%eax, %esi
	je	.L614
	movl	%esi, %eax
	orl	$64, %eax
	cmpl	%eax, %esi
	je	.L615
	movl	%esi, %eax
	orl	$8, %eax
	cmpl	%eax, %esi
	je	.L616
	pxor	%xmm2, %xmm2
.L598:
	movl	%esi, %eax
	orl	$8, %eax
	cmpl	%eax, %esi
	je	.L624
	movl	%esi, %eax
	orl	$64, %eax
	cmpl	%eax, %esi
	je	.L608
	movl	%esi, %eax
	orb	$4, %ah
	cmpl	%eax, %esi
	je	.L609
	movl	%esi, %eax
	orl	$2, %eax
	cmpl	%eax, %esi
	je	.L610
	movl	%esi, %eax
	pxor	%xmm3, %xmm3
	orl	$4, %eax
	cmpl	%eax, %esi
	jne	.L595
	leaq	80+_ZN2v88internal8compiler10BitsetType15BoundariesArrayE(%rip), %rax
	jmp	.L596
	.p2align 4,,10
	.p2align 3
.L606:
	movsd	.LC7(%rip), %xmm3
	pxor	%xmm2, %xmm2
.L595:
	maxsd	%xmm0, %xmm3
	minsd	%xmm1, %xmm2
	movapd	%xmm3, %xmm0
	movapd	%xmm2, %xmm1
	ret
	.p2align 4,,10
	.p2align 3
.L623:
	movsd	.LC14(%rip), %xmm2
	leaq	_ZN2v88internal8compiler10BitsetType15BoundariesArrayE(%rip), %rax
.L596:
	movsd	8(%rax), %xmm3
	jmp	.L595
	.p2align 4,,10
	.p2align 3
.L624:
	leaq	16+_ZN2v88internal8compiler10BitsetType15BoundariesArrayE(%rip), %rax
	jmp	.L596
	.p2align 4,,10
	.p2align 3
.L608:
	leaq	32+_ZN2v88internal8compiler10BitsetType15BoundariesArrayE(%rip), %rax
	jmp	.L596
	.p2align 4,,10
	.p2align 3
.L609:
	leaq	48+_ZN2v88internal8compiler10BitsetType15BoundariesArrayE(%rip), %rax
	jmp	.L596
	.p2align 4,,10
	.p2align 3
.L610:
	leaq	64+_ZN2v88internal8compiler10BitsetType15BoundariesArrayE(%rip), %rax
	jmp	.L596
	.p2align 4,,10
	.p2align 3
.L612:
	movl	$6, %eax
.L604:
	salq	$4, %rax
	leaq	_ZN2v88internal8compiler10BitsetType15BoundariesArrayE(%rip), %rdx
	movsd	8(%rdx,%rax), %xmm2
	subsd	.LC7(%rip), %xmm2
	jmp	.L598
	.p2align 4,,10
	.p2align 3
.L613:
	movl	$5, %eax
	jmp	.L604
	.p2align 4,,10
	.p2align 3
.L614:
	movl	$4, %eax
	jmp	.L604
	.p2align 4,,10
	.p2align 3
.L615:
	movl	$3, %eax
	jmp	.L604
	.p2align 4,,10
	.p2align 3
.L616:
	movl	$2, %eax
	jmp	.L604
	.cfi_endproc
.LFE18599:
	.size	_ZN2v88internal8compiler4Type23IntersectRangeAndBitsetES2_S2_PNS0_4ZoneE, .-_ZN2v88internal8compiler4Type23IntersectRangeAndBitsetES2_S2_PNS0_4ZoneE
	.section	.text._ZN2v88internal8compiler4Type23NormalizeRangeAndBitsetES2_PjPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler4Type23NormalizeRangeAndBitsetES2_PjPNS0_4ZoneE
	.type	_ZN2v88internal8compiler4Type23NormalizeRangeAndBitsetES2_PjPNS0_4ZoneE, @function
_ZN2v88internal8compiler4Type23NormalizeRangeAndBitsetES2_PjPNS0_4ZoneE:
.LFB18601:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	(%rsi), %ebx
	movq	%rdi, -40(%rbp)
	andl	$1118, %ebx
	jne	.L626
.L636:
	movq	-40(%rbp), %rax
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L626:
	.cfi_restore_state
	leaq	-40(%rbp), %r14
	movq	%rsi, %r12
	movq	%rdx, %r13
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type9BitsetLubEv
	movl	(%r12), %edx
	orl	%edx, %eax
	cmpl	%eax, %edx
	jne	.L628
	addq	$32, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L628:
	.cfi_restore_state
	movl	%ebx, %eax
	orl	$16, %eax
	cmpl	%eax, %ebx
	je	.L658
	movl	%ebx, %edx
	orl	$8, %edx
	cmpl	%edx, %ebx
	je	.L659
	movl	%ebx, %edx
	orl	$64, %edx
	cmpl	%edx, %ebx
	je	.L660
	movl	%ebx, %edx
	orb	$4, %dh
	cmpl	%edx, %ebx
	je	.L661
	movl	%ebx, %edx
	orl	$2, %edx
	cmpl	%edx, %ebx
	je	.L662
	movl	%ebx, %edx
	pxor	%xmm2, %xmm2
	orl	$4, %edx
	cmpl	%edx, %ebx
	je	.L682
.L631:
	movl	%ebx, %eax
	orl	$4, %eax
	cmpl	%eax, %ebx
	je	.L664
	movl	%ebx, %eax
	orl	$2, %eax
	cmpl	%eax, %ebx
	je	.L665
	movl	%ebx, %eax
	orb	$4, %ah
	cmpl	%eax, %ebx
	je	.L666
	movl	%ebx, %eax
	orl	$64, %eax
	cmpl	%eax, %ebx
	je	.L667
	movl	%ebx, %eax
	pxor	%xmm3, %xmm3
	orl	$8, %eax
	cmpl	%eax, %ebx
	je	.L683
.L632:
	movq	%r14, %rdi
	movsd	%xmm2, -64(%rbp)
	notl	%ebx
	movsd	%xmm3, -56(%rbp)
	call	_ZNK2v88internal8compiler4Type3MinEv
	movq	%r14, %rdi
	movsd	%xmm0, -48(%rbp)
	call	_ZNK2v88internal8compiler4Type3MaxEv
	movsd	-48(%rbp), %xmm1
	movsd	-64(%rbp), %xmm2
	andl	%ebx, (%r12)
	movsd	-56(%rbp), %xmm3
	comisd	%xmm1, %xmm2
	jb	.L634
	comisd	%xmm3, %xmm0
	jnb	.L636
.L634:
	minsd	%xmm1, %xmm2
	maxsd	%xmm0, %xmm3
	movapd	%xmm2, %xmm1
	movsd	.LC0(%rip), %xmm2
	movapd	%xmm3, %xmm0
	comisd	%xmm1, %xmm2
	jbe	.L680
	comisd	%xmm3, %xmm2
	ja	.L670
	movl	$16, %eax
.L641:
	movsd	.LC1(%rip), %xmm2
	comisd	%xmm1, %xmm2
	jbe	.L644
	orl	$8, %eax
	comisd	%xmm0, %xmm2
	movl	%eax, %ebx
	ja	.L655
.L644:
	pxor	%xmm2, %xmm2
	comisd	%xmm1, %xmm2
	jbe	.L646
	orl	$64, %eax
	comisd	%xmm0, %xmm2
	movl	%eax, %ebx
	ja	.L655
.L646:
	movsd	.LC3(%rip), %xmm2
	comisd	%xmm1, %xmm2
	jbe	.L648
	orb	$4, %ah
	comisd	%xmm0, %xmm2
	movl	%eax, %ebx
	ja	.L655
.L648:
	movsd	.LC4(%rip), %xmm2
	comisd	%xmm1, %xmm2
	jbe	.L650
	orl	$2, %eax
	comisd	%xmm0, %xmm2
	movl	%eax, %ebx
	ja	.L655
.L650:
	movsd	.LC5(%rip), %xmm2
	movl	%eax, %ebx
	comisd	%xmm1, %xmm2
	jbe	.L652
	orl	$4, %ebx
	comisd	%xmm0, %xmm2
	ja	.L655
.L652:
	orl	$16, %ebx
.L655:
	movq	16(%r13), %rax
	movq	24(%r13), %rdx
	subq	%rax, %rdx
	cmpq	$23, %rdx
	jbe	.L684
	leaq	24(%rax), %rdx
	movq	%rdx, 16(%r13)
.L657:
	unpcklpd	%xmm0, %xmm1
	movl	$4, (%rax)
	movl	%ebx, 4(%rax)
	movups	%xmm1, 8(%rax)
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L680:
	.cfi_restore_state
	xorl	%eax, %eax
	jmp	.L641
	.p2align 4,,10
	.p2align 3
.L684:
	movl	$24, %esi
	movq	%r13, %rdi
	movsd	%xmm0, -56(%rbp)
	movsd	%xmm1, -48(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movsd	-48(%rbp), %xmm1
	movsd	-56(%rbp), %xmm0
	jmp	.L657
.L658:
	movsd	.LC13(%rip), %xmm2
	.p2align 4,,10
	.p2align 3
.L629:
	movsd	.LC14(%rip), %xmm3
	jmp	.L632
.L659:
	leaq	16+_ZN2v88internal8compiler10BitsetType15BoundariesArrayE(%rip), %rdx
.L630:
	movsd	8(%rdx), %xmm2
	cmpl	%eax, %ebx
	je	.L629
	jmp	.L631
	.p2align 4,,10
	.p2align 3
.L660:
	leaq	32+_ZN2v88internal8compiler10BitsetType15BoundariesArrayE(%rip), %rdx
	jmp	.L630
.L661:
	leaq	48+_ZN2v88internal8compiler10BitsetType15BoundariesArrayE(%rip), %rdx
	jmp	.L630
.L662:
	leaq	64+_ZN2v88internal8compiler10BitsetType15BoundariesArrayE(%rip), %rdx
	jmp	.L630
.L682:
	leaq	80+_ZN2v88internal8compiler10BitsetType15BoundariesArrayE(%rip), %rdx
	jmp	.L630
.L664:
	movl	$6, %eax
.L633:
	salq	$4, %rax
	leaq	_ZN2v88internal8compiler10BitsetType15BoundariesArrayE(%rip), %rdx
	movsd	8(%rdx,%rax), %xmm3
	subsd	.LC7(%rip), %xmm3
	jmp	.L632
.L665:
	movl	$5, %eax
	jmp	.L633
.L666:
	movl	$4, %eax
	jmp	.L633
.L667:
	movl	$3, %eax
	jmp	.L633
.L683:
	movl	$2, %eax
	jmp	.L633
.L670:
	movl	$16, %ebx
	jmp	.L655
	.cfi_endproc
.LFE18601:
	.size	_ZN2v88internal8compiler4Type23NormalizeRangeAndBitsetES2_PjPNS0_4ZoneE, .-_ZN2v88internal8compiler4Type23NormalizeRangeAndBitsetES2_PjPNS0_4ZoneE
	.section	.text._ZN2v88internal8compiler4Type11NewConstantEdPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler4Type11NewConstantEdPNS0_4ZoneE
	.type	_ZN2v88internal8compiler4Type11NewConstantEdPNS0_4ZoneE, @function
_ZN2v88internal8compiler4Type11NewConstantEdPNS0_4ZoneE:
.LFB18602:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -32
	movsd	%xmm0, -24(%rbp)
	call	nearbyint@PLT
	movsd	-24(%rbp), %xmm1
	movabsq	$-9223372036854775808, %rdx
	ucomisd	%xmm0, %xmm1
	movq	%xmm1, %rax
	jp	.L686
	jne	.L686
	cmpq	%rdx, %rax
	je	.L688
	movsd	.LC0(%rip), %xmm0
	comisd	%xmm1, %xmm0
	jbe	.L709
	movl	$16, %eax
	.p2align 4,,10
	.p2align 3
.L689:
	leaq	-16+_ZN2v88internal8compiler10BitsetType15BoundariesArrayE(%rip), %rdx
	movl	(%rdx,%rax), %ebx
.L690:
	movq	16(%r12), %rax
	movq	24(%r12), %rdx
	subq	%rax, %rdx
	cmpq	$23, %rdx
	jbe	.L710
	leaq	24(%rax), %rdx
	movq	%rdx, 16(%r12)
.L693:
	unpcklpd	%xmm1, %xmm1
	movl	$4, (%rax)
	movl	%ebx, 4(%rax)
	movups	%xmm1, 8(%rax)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L688:
	.cfi_restore_state
	movl	$2049, %eax
.L694:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L709:
	.cfi_restore_state
	movsd	.LC1(%rip), %xmm0
	comisd	%xmm1, %xmm0
	jbe	.L711
	movl	$32, %eax
	jmp	.L689
	.p2align 4,,10
	.p2align 3
.L711:
	pxor	%xmm0, %xmm0
	comisd	%xmm1, %xmm0
	jbe	.L712
	movl	$48, %eax
	jmp	.L689
	.p2align 4,,10
	.p2align 3
.L712:
	movsd	.LC3(%rip), %xmm0
	comisd	%xmm1, %xmm0
	jbe	.L713
	movl	$64, %eax
	jmp	.L689
	.p2align 4,,10
	.p2align 3
.L713:
	movsd	.LC4(%rip), %xmm0
	comisd	%xmm1, %xmm0
	jbe	.L714
	movl	$80, %eax
	jmp	.L689
	.p2align 4,,10
	.p2align 3
.L714:
	movsd	.LC5(%rip), %xmm0
	movl	$96, %eax
	comisd	%xmm1, %xmm0
	ja	.L689
	movl	$16, %ebx
	jmp	.L690
	.p2align 4,,10
	.p2align 3
.L686:
	cmpq	%rdx, %rax
	je	.L688
	ucomisd	%xmm1, %xmm1
	movl	$4097, %eax
	jp	.L694
	movq	16(%r12), %rax
	movq	24(%r12), %rdx
	subq	%rax, %rdx
	cmpq	$15, %rdx
	jbe	.L715
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%r12)
.L698:
	movl	$1, (%rax)
	movsd	%xmm1, 8(%rax)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L715:
	.cfi_restore_state
	movl	$16, %esi
	movq	%r12, %rdi
	movsd	%xmm1, -24(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movsd	-24(%rbp), %xmm1
	jmp	.L698
	.p2align 4,,10
	.p2align 3
.L710:
	movl	$24, %esi
	movq	%r12, %rdi
	movsd	%xmm1, -24(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movsd	-24(%rbp), %xmm1
	jmp	.L693
	.cfi_endproc
.LFE18602:
	.size	_ZN2v88internal8compiler4Type11NewConstantEdPNS0_4ZoneE, .-_ZN2v88internal8compiler4Type11NewConstantEdPNS0_4ZoneE
	.section	.text._ZN2v88internal8compiler4Type10AddToUnionES2_PNS1_9UnionTypeEiPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler4Type10AddToUnionES2_PNS1_9UnionTypeEiPNS0_4ZoneE
	.type	_ZN2v88internal8compiler4Type10AddToUnionES2_PNS1_9UnionTypeEiPNS0_4ZoneE, @function
_ZN2v88internal8compiler4Type10AddToUnionES2_PNS1_9UnionTypeEiPNS0_4ZoneE:
.LFB18605:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$24, %rsp
	movq	%rdi, -56(%rbp)
	testb	$1, %dil
	jne	.L719
	movl	(%rdi), %eax
	movq	%rsi, %r13
	movq	%rdi, %rsi
	cmpl	$4, %eax
	je	.L719
	cmpl	$3, %eax
	je	.L729
	testl	%edx, %edx
	jle	.L722
	leal	-1(%rdx), %eax
	xorl	%r12d, %r12d
	leaq	-56(%rbp), %r15
	leaq	8(,%rax,8), %r14
.L723:
	movq	8(%r13), %rax
	movq	(%rax,%r12), %r8
	cmpq	%rsi, %r8
	jne	.L730
.L719:
	movl	%ebx, %eax
.L716:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L730:
	.cfi_restore_state
	movq	%r8, %rsi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_
	testb	%al, %al
	jne	.L719
	addq	$8, %r12
	movq	-56(%rbp), %rsi
	cmpq	%r12, %r14
	jne	.L723
.L722:
	movq	8(%r13), %rcx
	movslq	%ebx, %rdx
	leal	1(%rbx), %eax
	movq	%rsi, (%rcx,%rdx,8)
	jmp	.L716
	.p2align 4,,10
	.p2align 3
.L729:
	movl	4(%rdi), %eax
	testl	%eax, %eax
	jle	.L719
	leal	-1(%rax), %r12d
	movq	%rcx, %r14
	movl	%edx, %eax
	xorl	%ebx, %ebx
	salq	$3, %r12
.L721:
	movq	8(%rsi), %rdx
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	(%rdx,%rbx), %rdi
	movl	%eax, %edx
	call	_ZN2v88internal8compiler4Type10AddToUnionES2_PNS1_9UnionTypeEiPNS0_4ZoneE
	cmpq	%rbx, %r12
	je	.L716
	movq	-56(%rbp), %rsi
	addq	$8, %rbx
	jmp	.L721
	.cfi_endproc
.LFE18605:
	.size	_ZN2v88internal8compiler4Type10AddToUnionES2_PNS1_9UnionTypeEiPNS0_4ZoneE, .-_ZN2v88internal8compiler4Type10AddToUnionES2_PNS1_9UnionTypeEiPNS0_4ZoneE
	.section	.text._ZN2v88internal8compiler4Type12IntersectAuxES2_S2_PNS1_9UnionTypeEiPNS1_9RangeType6LimitsEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler4Type12IntersectAuxES2_S2_PNS1_9UnionTypeEiPNS1_9RangeType6LimitsEPNS0_4ZoneE
	.type	_ZN2v88internal8compiler4Type12IntersectAuxES2_S2_PNS1_9UnionTypeEiPNS1_9RangeType6LimitsEPNS0_4ZoneE, @function
_ZN2v88internal8compiler4Type12IntersectAuxES2_S2_PNS1_9UnionTypeEiPNS1_9RangeType6LimitsEPNS0_4ZoneE:
.LFB18600:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%r9, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%ecx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$40, %rsp
	movq	%rdi, -56(%rbp)
	movq	%rsi, -64(%rbp)
	testb	$1, %al
	jne	.L732
	cmpl	$3, (%rdi)
	je	.L775
.L732:
	movq	-64(%rbp), %rax
	testb	$1, %al
	jne	.L736
	cmpl	$3, (%rax)
	je	.L776
.L736:
	leaq	-56(%rbp), %r8
	leaq	-64(%rbp), %r14
	movq	%r8, %rdi
	movq	%r8, -80(%rbp)
	call	_ZNK2v88internal8compiler4Type9BitsetLubEv
	movq	%r14, %rdi
	movl	%eax, -68(%rbp)
	call	_ZNK2v88internal8compiler4Type9BitsetLubEv
	testl	%eax, -68(%rbp)
	je	.L774
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	movl	%esi, %ecx
	andl	$1, %ecx
	testb	$1, %dil
	jne	.L738
	cmpl	$4, (%rdi)
	movq	-80(%rbp), %r8
	jne	.L739
	testb	%cl, %cl
	jne	.L777
	cmpl	$4, (%rsi)
	jne	.L774
	movsd	8(%rsi), %xmm0
	movsd	16(%rsi), %xmm1
	maxsd	8(%rdi), %xmm0
	minsd	16(%rdi), %xmm1
	comisd	%xmm1, %xmm0
	ja	.L774
	movsd	(%rbx), %xmm3
	movsd	8(%rbx), %xmm2
	comisd	%xmm2, %xmm3
	ja	.L741
	minsd	%xmm0, %xmm3
	maxsd	%xmm1, %xmm2
	movapd	%xmm3, %xmm0
	movapd	%xmm2, %xmm1
	.p2align 4,,10
	.p2align 3
.L741:
	unpcklpd	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	.p2align 4,,10
	.p2align 3
.L774:
	movl	%r12d, %ecx
.L731:
	addq	$40, %rsp
	movl	%ecx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L775:
	.cfi_restore_state
	movl	4(%rdi), %edx
	testl	%edx, %edx
	jle	.L774
	leal	-1(%rdx), %r14d
	xorl	%r12d, %r12d
	salq	$3, %r14
.L734:
	movq	8(%rax), %rax
	movq	-64(%rbp), %rsi
	movq	%r13, %r9
	movq	%rbx, %r8
	movq	%r15, %rdx
	movq	(%rax,%r12), %rdi
	call	_ZN2v88internal8compiler4Type12IntersectAuxES2_S2_PNS1_9UnionTypeEiPNS1_9RangeType6LimitsEPNS0_4ZoneE
	movl	%eax, %ecx
	cmpq	%r14, %r12
	je	.L731
	movq	-56(%rbp), %rax
	addq	$8, %r12
	jmp	.L734
	.p2align 4,,10
	.p2align 3
.L776:
	movl	4(%rax), %edx
	testl	%edx, %edx
	jle	.L774
	leal	-1(%rdx), %r14d
	movl	%r12d, %ecx
	xorl	%r12d, %r12d
	salq	$3, %r14
.L737:
	movq	8(%rax), %rax
	movq	-56(%rbp), %rdi
	movq	%r13, %r9
	movq	%rbx, %r8
	movq	%r15, %rdx
	movq	(%rax,%r12), %rsi
	call	_ZN2v88internal8compiler4Type12IntersectAuxES2_S2_PNS1_9UnionTypeEiPNS1_9RangeType6LimitsEPNS0_4ZoneE
	movl	%eax, %ecx
	cmpq	%r12, %r14
	je	.L731
	movq	-64(%rbp), %rax
	addq	$8, %r12
	jmp	.L737
	.p2align 4,,10
	.p2align 3
.L739:
	movq	%r8, %rax
	testb	%cl, %cl
	je	.L778
.L754:
	movq	(%rax), %rdi
	movq	%r13, %rcx
	movl	%r12d, %edx
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler4Type10AddToUnionES2_PNS1_9UnionTypeEiPNS0_4ZoneE
	movl	%eax, %ecx
	jmp	.L731
	.p2align 4,,10
	.p2align 3
.L738:
	testb	%cl, %cl
	jne	.L760
	cmpl	$4, (%rsi)
	je	.L757
.L760:
	movq	%r14, %rax
	jmp	.L754
	.p2align 4,,10
	.p2align 3
.L778:
	cmpl	$4, (%rsi)
	je	.L757
	movq	%r8, %rdi
	call	_ZNK2v88internal8compiler4Type12SimplyEqualsES2_
	testb	%al, %al
	je	.L774
	movq	-56(%rbp), %rdi
	movq	%r13, %rcx
	movl	%r12d, %edx
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler4Type10AddToUnionES2_PNS1_9UnionTypeEiPNS0_4ZoneE
	movl	%eax, %ecx
	jmp	.L731
	.p2align 4,,10
	.p2align 3
.L757:
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %rdi
	movl	%r12d, %ecx
	movq	%r13, %r9
	movq	%rbx, %r8
	movq	%r15, %rdx
	call	_ZN2v88internal8compiler4Type12IntersectAuxES2_S2_PNS1_9UnionTypeEiPNS1_9RangeType6LimitsEPNS0_4ZoneE
	movl	%eax, %ecx
	jmp	.L731
	.p2align 4,,10
	.p2align 3
.L777:
	movq	%r13, %rdx
	call	_ZN2v88internal8compiler4Type23IntersectRangeAndBitsetES2_S2_PNS0_4ZoneE
	comisd	%xmm1, %xmm0
	ja	.L774
	movsd	(%rbx), %xmm3
	movsd	8(%rbx), %xmm2
	comisd	%xmm2, %xmm3
	ja	.L741
	movapd	%xmm3, %xmm4
	cmpnltsd	%xmm0, %xmm4
	andpd	%xmm4, %xmm0
	andnpd	%xmm3, %xmm4
	movapd	%xmm1, %xmm3
	cmpnltsd	%xmm2, %xmm3
	orpd	%xmm4, %xmm0
	andpd	%xmm3, %xmm1
	andnpd	%xmm2, %xmm3
	orpd	%xmm3, %xmm1
	jmp	.L741
	.cfi_endproc
.LFE18600:
	.size	_ZN2v88internal8compiler4Type12IntersectAuxES2_S2_PNS1_9UnionTypeEiPNS1_9RangeType6LimitsEPNS0_4ZoneE, .-_ZN2v88internal8compiler4Type12IntersectAuxES2_S2_PNS1_9UnionTypeEiPNS1_9RangeType6LimitsEPNS0_4ZoneE
	.section	.text._ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE
	.type	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE, @function
_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE:
.LFB18596:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -88(%rbp)
	movq	%rsi, -96(%rbp)
	movq	%fs:40, %rbx
	movq	%rbx, -56(%rbp)
	xorl	%ebx, %ebx
	testb	$1, %al
	je	.L780
	movq	%rsi, %rdx
	testb	$1, %sil
	je	.L780
	xorl	$1, %eax
	xorl	$1, %edx
	andl	%edx, %eax
	orl	$1, %eax
	jmp	.L781
	.p2align 4,,10
	.p2align 3
.L780:
	cmpq	$1, %rax
	je	.L783
	movq	-96(%rbp), %rsi
	movl	$4294967295, %edx
	cmpq	%rdx, %rsi
	je	.L783
	cmpq	$1, %rsi
	je	.L784
	cmpq	%rdx, %rax
	je	.L784
	cmpq	%rsi, %rax
	je	.L783
	leaq	-88(%rbp), %r13
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_
	testb	%al, %al
	jne	.L783
	movq	-88(%rbp), %rsi
	cmpq	-96(%rbp), %rsi
	je	.L784
	leaq	-96(%rbp), %r14
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_
	testb	%al, %al
	jne	.L784
	movq	-96(%rbp), %rsi
	cmpq	%rsi, -88(%rbp)
	je	.L787
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_
	testb	%al, %al
	jne	.L787
	movq	-88(%rbp), %rsi
	cmpq	%rsi, -96(%rbp)
	je	.L791
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_
	testb	%al, %al
	je	.L790
.L791:
	movl	$4294967295, %eax
	movq	%rax, -88(%rbp)
.L790:
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type9BitsetGlbEv
	movq	%r14, %rdi
	movl	%eax, %ebx
	call	_ZNK2v88internal8compiler4Type9BitsetGlbEv
	movq	-96(%rbp), %rdx
	movl	%eax, %r13d
	movq	-88(%rbp), %rax
	movl	%edx, %esi
	andl	$1, %esi
	testb	$1, %al
	jne	.L793
	cmpl	$3, (%rax)
	jne	.L793
	movl	4(%rax), %ecx
	movl	$1, %eax
	testb	%sil, %sil
	jne	.L794
	cmpl	$3, (%rdx)
	jne	.L794
.L805:
	movl	4(%rdx), %eax
.L794:
	leal	(%rax,%rcx), %edx
	xorl	%edx, %ecx
	xorl	%edx, %eax
	testl	%eax, %ecx
	jns	.L795
.L796:
	movl	$4294967295, %eax
	jmp	.L781
	.p2align 4,,10
	.p2align 3
.L783:
	movq	-88(%rbp), %rax
.L781:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L821
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L784:
	.cfi_restore_state
	movq	-96(%rbp), %rax
	jmp	.L781
.L787:
	movl	$4294967295, %eax
	movq	%rax, -96(%rbp)
	jmp	.L790
.L793:
	testb	%sil, %sil
	jne	.L810
	cmpl	$3, (%rdx)
	movl	$4, %ecx
	je	.L822
.L806:
	movslq	%ecx, %r15
	salq	$3, %r15
.L804:
	movq	16(%r12), %r14
	movq	24(%r12), %rax
	subq	%r14, %rax
	cmpq	$15, %rax
	jbe	.L823
	leaq	16(%r14), %rax
	movq	%rax, 16(%r12)
.L798:
	movl	$3, (%r14)
	movl	%ecx, 4(%r14)
	movq	16(%r12), %rax
	movq	24(%r12), %rdx
	subq	%rax, %rdx
	cmpq	%r15, %rdx
	jb	.L824
	addq	%rax, %r15
	movq	%r15, 16(%r12)
.L800:
	andl	%r13d, %ebx
	movq	%rax, 8(%r14)
	movq	%r12, %r9
	leaq	-80(%rbp), %r8
	movl	%ebx, %edx
	movq	.LC15(%rip), %xmm0
	orl	$1, %edx
	movl	%edx, %ecx
	movq	%r14, %rdx
	movq	%rcx, (%rax)
	movq	-96(%rbp), %rsi
	movl	$1, %ecx
	movq	-88(%rbp), %rdi
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler4Type12IntersectAuxES2_S2_PNS1_9UnionTypeEiPNS1_9RangeType6LimitsEPNS0_4ZoneE
	movsd	-80(%rbp), %xmm0
	comisd	-72(%rbp), %xmm0
	movl	%eax, %r13d
	jbe	.L825
.L801:
	cmpl	$1, %r13d
	je	.L826
	movq	%r12, %rdx
	movl	%r13d, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Type14NormalizeUnionEPNS1_9UnionTypeEiPNS0_4ZoneE.part.0
	jmp	.L781
.L810:
	movl	$32, %r15d
	movl	$4, %ecx
	jmp	.L804
.L822:
	movl	$1, %ecx
	jmp	.L805
.L795:
	leal	2(%rdx), %ecx
	xorl	%ecx, %edx
	testl	%edx, %ecx
	js	.L796
	jmp	.L806
.L826:
	movq	8(%r14), %rax
	movq	(%rax), %rax
	jmp	.L781
.L825:
	movsd	-72(%rbp), %xmm1
	movq	%r12, %rdi
	andl	$-1119, %ebx
	call	_ZN2v88internal8compiler9RangeType3NewENS2_6LimitsEPNS0_4ZoneE
	movl	%r13d, %edx
	movq	%r12, %rcx
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Type11UpdateRangeES2_PNS1_9UnionTypeEiPNS0_4ZoneE
	movq	8(%r14), %rdx
	movl	%eax, %r13d
	movl	%ebx, %eax
	orl	$1, %eax
	movq	%rax, (%rdx)
	jmp	.L801
.L821:
	call	__stack_chk_fail@PLT
.L824:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L800
.L823:
	movl	$16, %esi
	movq	%r12, %rdi
	movl	%ecx, -100(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-100(%rbp), %ecx
	movq	%rax, %r14
	jmp	.L798
	.cfi_endproc
.LFE18596:
	.size	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE, .-_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE
	.section	.text._ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE
	.type	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE, @function
_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE:
.LFB18604:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -72(%rbp)
	movq	%rsi, -80(%rbp)
	movq	%fs:40, %rbx
	movq	%rbx, -56(%rbp)
	xorl	%ebx, %ebx
	testb	$1, %al
	je	.L828
	movq	%rsi, %rdx
	testb	$1, %sil
	je	.L828
	xorl	$1, %eax
	xorl	$1, %edx
	orl	%edx, %eax
	orl	$1, %eax
	jmp	.L829
	.p2align 4,,10
	.p2align 3
.L828:
	movl	$4294967295, %edx
	cmpq	%rdx, %rax
	je	.L831
	movq	-80(%rbp), %rsi
	cmpq	$1, %rsi
	je	.L831
	cmpq	%rdx, %rsi
	je	.L832
	cmpq	$1, %rax
	je	.L832
	cmpq	%rsi, %rax
	je	.L832
	leaq	-72(%rbp), %r13
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_
	testb	%al, %al
	jne	.L832
	movq	-72(%rbp), %rsi
	cmpq	-80(%rbp), %rsi
	je	.L831
	leaq	-80(%rbp), %r14
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_
	testb	%al, %al
	jne	.L831
	movq	-80(%rbp), %rdx
	movq	-72(%rbp), %rax
	movl	%edx, %esi
	andl	$1, %esi
	testb	$1, %al
	jne	.L836
	cmpl	$3, (%rax)
	jne	.L836
	movl	4(%rax), %ecx
	movl	$1, %eax
	testb	%sil, %sil
	jne	.L837
	cmpl	$3, (%rdx)
	jne	.L837
.L861:
	movl	4(%rdx), %eax
.L837:
	leal	(%rax,%rcx), %edx
	xorl	%edx, %ecx
	xorl	%edx, %eax
	testl	%eax, %ecx
	jns	.L838
.L839:
	movl	$4294967295, %eax
	jmp	.L829
	.p2align 4,,10
	.p2align 3
.L831:
	movq	-72(%rbp), %rax
.L829:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L900
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L832:
	.cfi_restore_state
	movq	-80(%rbp), %rax
	jmp	.L829
.L836:
	testb	%sil, %sil
	jne	.L869
	cmpl	$3, (%rdx)
	movl	$4, %ebx
	je	.L901
.L862:
	movslq	%ebx, %r8
	salq	$3, %r8
.L860:
	movq	16(%r12), %r15
	movq	24(%r12), %rax
	subq	%r15, %rax
	cmpq	$15, %rax
	jbe	.L902
	leaq	16(%r15), %rax
	movq	%rax, 16(%r12)
.L841:
	movl	$3, (%r15)
	movl	%ebx, 4(%r15)
	movq	16(%r12), %rax
	movq	24(%r12), %rdx
	subq	%rax, %rdx
	cmpq	%rdx, %r8
	ja	.L903
	addq	%rax, %r8
	movq	%r8, 16(%r12)
.L843:
	movq	%rax, 8(%r15)
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type9BitsetGlbEv
	movq	%r14, %rdi
	movl	%eax, %r13d
	call	_ZNK2v88internal8compiler4Type9BitsetGlbEv
	movq	-72(%rbp), %rdi
	orl	%r13d, %eax
	movl	%eax, -60(%rbp)
	testb	$1, %dil
	jne	.L844
	movl	(%rdi), %edx
	cmpl	$4, %edx
	jne	.L904
.L845:
	movq	-80(%rbp), %r8
	testb	$1, %r8b
	jne	.L846
	movl	(%r8), %edx
	cmpl	$4, %edx
	jne	.L863
.L847:
	testq	%rdi, %rdi
	je	.L850
	testq	%r8, %r8
	je	.L848
	movsd	8(%rdi), %xmm0
	movsd	16(%rdi), %xmm1
	movsd	8(%r8), %xmm2
	movsd	16(%r8), %xmm3
	comisd	%xmm1, %xmm0
	ja	.L867
	comisd	%xmm3, %xmm2
	ja	.L851
	minsd	%xmm0, %xmm2
	maxsd	%xmm1, %xmm3
	movapd	%xmm2, %xmm0
	movapd	%xmm3, %xmm1
.L851:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler9RangeType3NewENS2_6LimitsEPNS0_4ZoneE
	leaq	-60(%rbp), %rsi
	movq	%r12, %rdx
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Type23NormalizeRangeAndBitsetES2_PjPNS0_4ZoneE
.L855:
	movl	-60(%rbp), %edx
	movq	8(%r15), %rcx
	orl	$1, %edx
	movl	%edx, %ebx
	movq	%rbx, (%rcx)
	cmpq	$1, %rax
	je	.L868
	movq	8(%r15), %rdx
	movq	%rax, 8(%rdx)
	movl	$2, %edx
.L857:
	movq	-72(%rbp), %rdi
	movq	%r12, %rcx
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler4Type10AddToUnionES2_PNS1_9UnionTypeEiPNS0_4ZoneE
	movq	-80(%rbp), %rdi
	movq	%r15, %rsi
	movq	%r12, %rcx
	movl	%eax, %edx
	call	_ZN2v88internal8compiler4Type10AddToUnionES2_PNS1_9UnionTypeEiPNS0_4ZoneE
	movl	%eax, %esi
	cmpl	$1, %eax
	je	.L905
	movq	%r12, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler4Type14NormalizeUnionEPNS1_9UnionTypeEiPNS0_4ZoneE.part.0
	jmp	.L829
.L901:
	movl	$1, %ecx
	jmp	.L861
.L869:
	movl	$32, %r8d
	movl	$4, %ebx
	jmp	.L860
.L904:
	cmpl	$3, %edx
	jne	.L844
	movq	8(%rdi), %rdx
	movq	8(%rdx), %rdi
	testb	$1, %dil
	jne	.L844
	cmpl	$4, (%rdi)
	je	.L845
.L844:
	movq	-80(%rbp), %r8
	testb	$1, %r8b
	jne	.L849
	movl	(%r8), %edx
	cmpl	$4, %edx
	je	.L850
	xorl	%edi, %edi
.L863:
	cmpl	$3, %edx
	jne	.L846
	movq	8(%r8), %rdx
	movq	8(%rdx), %r8
	testb	$1, %r8b
	jne	.L846
	cmpl	$4, (%r8)
	je	.L847
.L846:
	testq	%rdi, %rdi
	je	.L849
.L848:
	leaq	-60(%rbp), %rsi
	movq	%r12, %rdx
	call	_ZN2v88internal8compiler4Type23NormalizeRangeAndBitsetES2_PjPNS0_4ZoneE
	jmp	.L855
.L838:
	leal	2(%rdx), %ebx
	xorl	%ebx, %edx
	testl	%edx, %ebx
	js	.L839
	jmp	.L862
.L849:
	movq	8(%r15), %rdx
	orl	$1, %eax
	movq	%rax, (%rdx)
	movl	$1, %edx
	jmp	.L857
.L905:
	movq	8(%r15), %rax
	movq	(%rax), %rax
	jmp	.L829
.L900:
	call	__stack_chk_fail@PLT
.L867:
	movapd	%xmm3, %xmm1
	movapd	%xmm2, %xmm0
	jmp	.L851
.L850:
	testq	%r8, %r8
	je	.L849
	leaq	-60(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler4Type23NormalizeRangeAndBitsetES2_PjPNS0_4ZoneE
	jmp	.L855
.L868:
	movl	$1, %edx
	jmp	.L857
.L903:
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L843
.L902:
	movl	$16, %esi
	movq	%r12, %rdi
	movq	%r8, -88(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-88(%rbp), %r8
	movq	%rax, %r15
	jmp	.L841
	.cfi_endproc
.LFE18604:
	.size	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE, .-_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE
	.section	.text._ZN2v88internal8compiler4Type14NormalizeUnionEPNS1_9UnionTypeEiPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler4Type14NormalizeUnionEPNS1_9UnionTypeEiPNS0_4ZoneE
	.type	_ZN2v88internal8compiler4Type14NormalizeUnionEPNS1_9UnionTypeEiPNS0_4ZoneE, @function
_ZN2v88internal8compiler4Type14NormalizeUnionEPNS1_9UnionTypeEiPNS0_4ZoneE:
.LFB18606:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rcx
	movq	(%rcx), %rax
	cmpl	$1, %esi
	je	.L944
	cmpl	$2, %esi
	jne	.L909
	cmpl	$1, %eax
	jne	.L909
	movq	8(%rcx), %rax
	testb	$1, %al
	jne	.L909
	cmpl	$4, (%rax)
	je	.L947
.L909:
	movl	%esi, 4(%rdi)
	movq	%rdi, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L944:
	ret
	.p2align 4,,10
	.p2align 3
.L947:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movsd	8(%rax), %xmm0
	movsd	16(%rax), %xmm1
	movsd	.LC0(%rip), %xmm2
	comisd	%xmm0, %xmm2
	ja	.L948
	xorl	%eax, %eax
.L910:
	movsd	.LC1(%rip), %xmm2
	comisd	%xmm0, %xmm2
	jbe	.L913
	orl	$8, %eax
	comisd	%xmm1, %xmm2
	movl	%eax, %ebx
	ja	.L924
.L913:
	pxor	%xmm2, %xmm2
	comisd	%xmm0, %xmm2
	jbe	.L915
	orl	$64, %eax
	comisd	%xmm1, %xmm2
	movl	%eax, %ebx
	ja	.L924
.L915:
	movsd	.LC3(%rip), %xmm2
	comisd	%xmm0, %xmm2
	jbe	.L917
	orb	$4, %ah
	comisd	%xmm1, %xmm2
	movl	%eax, %ebx
	ja	.L924
.L917:
	movsd	.LC4(%rip), %xmm2
	comisd	%xmm0, %xmm2
	jbe	.L919
	orl	$2, %eax
	comisd	%xmm1, %xmm2
	movl	%eax, %ebx
	ja	.L924
.L919:
	movsd	.LC5(%rip), %xmm2
	movl	%eax, %ebx
	comisd	%xmm0, %xmm2
	jbe	.L921
	orl	$4, %ebx
	comisd	%xmm1, %xmm2
	ja	.L924
.L921:
	orl	$16, %ebx
.L924:
	movq	16(%rdx), %rax
	movq	24(%rdx), %rcx
	subq	%rax, %rcx
	cmpq	$23, %rcx
	jbe	.L949
	leaq	24(%rax), %rcx
	movq	%rcx, 16(%rdx)
.L926:
	unpcklpd	%xmm1, %xmm0
	movl	$4, (%rax)
	movl	%ebx, 4(%rax)
	movups	%xmm0, 8(%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L948:
	.cfi_restore_state
	comisd	%xmm1, %xmm2
	ja	.L929
	movl	$16, %eax
	jmp	.L910
.L949:
	movl	$24, %esi
	movq	%rdx, %rdi
	movsd	%xmm0, -32(%rbp)
	movsd	%xmm1, -24(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movsd	-24(%rbp), %xmm1
	movsd	-32(%rbp), %xmm0
	jmp	.L926
.L929:
	movl	$16, %ebx
	jmp	.L924
	.cfi_endproc
.LFE18606:
	.size	_ZN2v88internal8compiler4Type14NormalizeUnionEPNS1_9UnionTypeEiPNS0_4ZoneE, .-_ZN2v88internal8compiler4Type14NormalizeUnionEPNS1_9UnionTypeEiPNS0_4ZoneE
	.section	.text._ZNK2v88internal8compiler4Type12NumConstantsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler4Type12NumConstantsEv
	.type	_ZNK2v88internal8compiler4Type12NumConstantsEv, @function
_ZNK2v88internal8compiler4Type12NumConstantsEv:
.LFB18607:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	xorl	%r8d, %r8d
	testb	$1, %al
	jne	.L950
	movl	(%rax), %edx
	movl	$1, %r8d
	cmpl	$1, %edx
	jbe	.L950
	xorl	%r8d, %r8d
	cmpl	$3, %edx
	jne	.L950
	movl	4(%rax), %ecx
	testl	%ecx, %ecx
	jle	.L950
	movq	8(%rax), %rdx
	subl	$1, %ecx
	leaq	8(%rdx), %rax
	leaq	(%rax,%rcx,8), %rcx
	jmp	.L953
	.p2align 4,,10
	.p2align 3
.L959:
	addq	$8, %rax
.L953:
	movq	(%rdx), %rdx
	testb	$1, %dl
	jne	.L952
	cmpl	$1, (%rdx)
	adcl	$0, %r8d
.L952:
	movq	%rax, %rdx
	cmpq	%rax, %rcx
	jne	.L959
.L950:
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE18607:
	.size	_ZNK2v88internal8compiler4Type12NumConstantsEv, .-_ZNK2v88internal8compiler4Type12NumConstantsEv
	.section	.rodata._ZN2v88internal8compiler10BitsetType4NameEj.str1.1,"aMS",@progbits,1
.LC16:
	.string	"Null"
.LC17:
	.string	"None"
.LC18:
	.string	"Negative32"
.LC19:
	.string	"Unsigned30"
.LC20:
	.string	"Signed31"
.LC21:
	.string	"Integral32"
.LC22:
	.string	"Integral32OrMinusZero"
.LC23:
	.string	"BooleanOrNullOrNumber"
.LC24:
	.string	"Symbol"
.LC25:
	.string	"OtherUndetectable"
.LC26:
	.string	"OtherCallable"
.LC27:
	.string	"CallableProxy"
.LC28:
	.string	"DetectableCallable"
.LC29:
	.string	"Function"
.LC30:
	.string	"Undetectable"
.LC31:
	.string	"Proxy"
.LC32:
	.string	"Callable"
.LC33:
	.string	"BoundFunction"
.LC34:
	.string	"Array"
.LC35:
	.string	"NumberOrOddball"
.LC36:
	.string	"NumericOrString"
.LC37:
	.string	"Undefined"
.LC38:
	.string	"PlainNumber"
.LC39:
	.string	"OrderedNumber"
.LC40:
	.string	"Number"
.LC41:
	.string	"Negative31"
.LC42:
	.string	"BooleanOrNullOrUndefined"
.LC43:
	.string	"Unsigned31"
.LC44:
	.string	"NonBigIntPrimitive"
.LC45:
	.string	"NumberOrUndefined"
.LC46:
	.string	"Signed32"
.LC47:
	.string	"Signed32OrMinusZero"
.LC48:
	.string	"Signed32OrMinusZeroOrNaN"
.LC49:
	.string	"Unsigned32"
.LC50:
	.string	"Unsigned32OrMinusZeroOrNaN"
.LC51:
	.string	"NaN"
.LC52:
	.string	"MinusZeroOrNaN"
.LC53:
	.string	"NonBigInt"
.LC54:
	.string	"NullOrNumber"
.LC55:
	.string	"InternalizedString"
.LC56:
	.string	"PlainPrimitive"
.LC57:
	.string	"UniqueName"
	.section	.rodata._ZN2v88internal8compiler10BitsetType4NameEj.str1.8,"aMS",@progbits,1
	.align 8
.LC58:
	.string	"NumberOrUndefinedOrNullOrBoolean"
	.section	.rodata._ZN2v88internal8compiler10BitsetType4NameEj.str1.1
.LC59:
	.string	"BooleanOrNumber"
.LC60:
	.string	"Boolean"
.LC61:
	.string	"OtherInternal"
.LC62:
	.string	"Integral32OrMinusZeroOrNaN"
.LC63:
	.string	"Unsigned32OrMinusZero"
.LC64:
	.string	"OtherUndetectableOrUndefined"
.LC65:
	.string	"Hole"
.LC66:
	.string	"NumberOrHole"
.LC67:
	.string	"SymbolOrReceiver"
.LC68:
	.string	"MinusZero"
.LC69:
	.string	"String"
.LC70:
	.string	"InternalizedStringOrNull"
.LC71:
	.string	"NullOrUndefined"
.LC72:
	.string	"NonNumber"
.LC73:
	.string	"OtherObject"
.LC74:
	.string	"OtherProxy"
.LC75:
	.string	"Internal"
.LC76:
	.string	"NonCallableOrNull"
.LC77:
	.string	"NonCallable"
.LC78:
	.string	"Oddball"
.LC79:
	.string	"Name"
.LC80:
	.string	"DetectableObject"
.LC81:
	.string	"DetectableReceiverOrNull"
.LC82:
	.string	"Object"
.LC83:
	.string	"StringOrReceiver"
.LC84:
	.string	"ArrayOrProxy"
.LC85:
	.string	"ReceiverOrNullOrUndefined"
.LC86:
	.string	"Receiver"
.LC87:
	.string	"DetectableReceiver"
.LC88:
	.string	"ReceiverOrUndefined"
.LC89:
	.string	"ArrayOrOtherObject"
.LC90:
	.string	"BigInt"
.LC91:
	.string	"ExternalPointer"
.LC92:
	.string	"Primitive"
.LC93:
	.string	"Unique"
.LC94:
	.string	"Numeric"
.LC95:
	.string	"NonInternal"
.LC96:
	.string	"OtherUnsigned31"
.LC97:
	.string	"OtherUnsigned32"
.LC98:
	.string	"OtherSigned32"
.LC99:
	.string	"OtherNumber"
.LC100:
	.string	"OtherString"
.LC101:
	.string	"Any"
	.section	.text._ZN2v88internal8compiler10BitsetType4NameEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler10BitsetType4NameEj
	.type	_ZN2v88internal8compiler10BitsetType4NameEj, @function
_ZN2v88internal8compiler10BitsetType4NameEj:
.LFB18608:
	.cfi_startproc
	endbr64
	cmpl	$32766, %edi
	je	.L999
	ja	.L962
	leaq	.LC63(%rip), %rax
	cmpl	$3078, %edi
	je	.L960
	jbe	.L1129
	leaq	.LC45(%rip), %rax
	cmpl	$7518, %edi
	je	.L960
	jbe	.L1130
	leaq	.LC69(%rip), %rax
	cmpl	$16416, %edi
	je	.L960
	jbe	.L1131
	leaq	.LC57(%rip), %rax
	cmpl	$24576, %edi
	je	.L960
	jbe	.L1132
	cmpl	$24608, %edi
	movl	$0, %edx
	leaq	.LC79(%rip), %rax
	cmovne	%rdx, %rax
	ret
.L1002:
	leaq	.LC96(%rip), %rax
.L960:
	ret
	.p2align 4,,10
	.p2align 3
.L962:
	leaq	.LC77(%rip), %rax
	cmpl	$68288512, %edi
	je	.L960
	jbe	.L1133
	leaq	.LC83(%rip), %rax
	cmpl	$75448352, %edi
	je	.L960
	jbe	.L1134
	leaq	.LC36(%rip), %rax
	cmpl	$134241406, %edi
	je	.L960
	jbe	.L1135
	leaq	.LC72(%rip), %rax
	cmpl	$268395424, %edi
	je	.L960
	jbe	.L1136
	cmpl	$-2, %edi
	movl	$0, %edx
	leaq	.LC101(%rip), %rax
	cmovne	%rdx, %rax
	ret
.L1132:
	leaq	.LC70(%rip), %rax
	cmpl	$16512, %edi
	je	.L960
	cmpl	$24574, %edi
	movl	$0, %edx
	leaq	.LC56(%rip), %rax
	cmovne	%rdx, %rax
	ret
.L1131:
	leaq	.LC58(%rip), %rax
	cmpl	$8158, %edi
	je	.L960
	jbe	.L1137
	leaq	.LC24(%rip), %rax
	cmpl	$8192, %edi
	je	.L960
	cmpl	$16384, %edi
	movl	$0, %edx
	leaq	.LC55(%rip), %rax
	cmovne	%rdx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1130:
	leaq	.LC50(%rip), %rax
	cmpl	$7174, %edi
	je	.L960
	jbe	.L1138
	leaq	.LC40(%rip), %rax
	cmpl	$7262, %edi
	je	.L960
	jbe	.L1139
	cmpl	$7390, %edi
	movl	$0, %edx
	leaq	.LC54(%rip), %rax
	cmovne	%rdx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1129:
	leaq	.LC71(%rip), %rax
	cmpl	$384, %edi
	je	.L960
	jbe	.L1140
	leaq	.LC20(%rip), %rax
	cmpl	$1088, %edi
	je	.L960
	jbe	.L1141
	leaq	.LC38(%rip), %rax
	cmpl	$1118, %edi
	je	.L960
	jbe	.L1142
	cmpl	$2048, %edi
	movl	$0, %edx
	leaq	.LC68(%rip), %rax
	cmovne	%rdx, %rax
	ret
.L1137:
	leaq	.LC59(%rip), %rax
	cmpl	$7774, %edi
	je	.L960
	cmpl	$7902, %edi
	movl	$0, %edx
	leaq	.LC23(%rip), %rax
	cmovne	%rdx, %rax
	ret
.L1142:
	leaq	.LC46(%rip), %rax
	cmpl	$1098, %edi
	je	.L960
	cmpl	$1102, %edi
	movl	$0, %edx
	leaq	.LC21(%rip), %rax
	cmovne	%rdx, %rax
	ret
.L1141:
	leaq	.LC19(%rip), %rax
	cmpl	$1024, %edi
	je	.L960
	jbe	.L1143
	leaq	.LC43(%rip), %rax
	cmpl	$1026, %edi
	je	.L960
	cmpl	$1030, %edi
	movl	$0, %edx
	leaq	.LC49(%rip), %rax
	cmovne	%rdx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1140:
	cmpl	$32, %edi
	ja	.L965
	leaq	.L968(%rip), %rdx
	movl	%edi, %edi
	movslq	(%rdx,%rdi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler10BitsetType4NameEj,"a",@progbits
	.align 4
	.align 4
.L968:
	.long	.L972-.L968
	.long	.L966-.L968
	.long	.L1002-.L968
	.long	.L966-.L968
	.long	.L971-.L968
	.long	.L966-.L968
	.long	.L966-.L968
	.long	.L966-.L968
	.long	.L970-.L968
	.long	.L966-.L968
	.long	.L966-.L968
	.long	.L966-.L968
	.long	.L966-.L968
	.long	.L966-.L968
	.long	.L966-.L968
	.long	.L966-.L968
	.long	.L969-.L968
	.long	.L966-.L968
	.long	.L966-.L968
	.long	.L966-.L968
	.long	.L966-.L968
	.long	.L966-.L968
	.long	.L966-.L968
	.long	.L966-.L968
	.long	.L966-.L968
	.long	.L966-.L968
	.long	.L966-.L968
	.long	.L966-.L968
	.long	.L966-.L968
	.long	.L966-.L968
	.long	.L966-.L968
	.long	.L966-.L968
	.long	.L967-.L968
	.section	.text._ZN2v88internal8compiler10BitsetType4NameEj
.L972:
	leaq	.LC17(%rip), %rax
	ret
.L966:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L965:
	leaq	.LC16(%rip), %rax
	cmpl	$128, %edi
	je	.L960
	jbe	.L1144
	cmpl	$256, %edi
	movl	$0, %edx
	leaq	.LC37(%rip), %rax
	cmovne	%rdx, %rax
	ret
.L1143:
	leaq	.LC60(%rip), %rax
	cmpl	$512, %edi
	je	.L960
	cmpl	$896, %edi
	movl	$0, %edx
	leaq	.LC42(%rip), %rax
	cmovne	%rdx, %rax
	ret
.L1144:
	leaq	.LC41(%rip), %rax
	cmpl	$64, %edi
	je	.L960
	cmpl	$72, %edi
	movl	$0, %edx
	leaq	.LC18(%rip), %rax
	cmovne	%rdx, %rax
	ret
.L1139:
	leaq	.LC48(%rip), %rax
	cmpl	$7242, %edi
	je	.L960
	cmpl	$7246, %edi
	movl	$0, %edx
	leaq	.LC62(%rip), %rax
	cmovne	%rdx, %rax
	ret
.L1138:
	leaq	.LC39(%rip), %rax
	cmpl	$3166, %edi
	je	.L960
	jbe	.L1145
	leaq	.LC51(%rip), %rax
	cmpl	$4096, %edi
	je	.L960
	cmpl	$6144, %edi
	movl	$0, %edx
	leaq	.LC52(%rip), %rax
	cmovne	%rdx, %rax
	ret
.L1136:
	leaq	.LC92(%rip), %rax
	cmpl	$134250494, %edi
	je	.L960
	cmpl	$209682430, %edi
	movl	$0, %edx
	leaq	.LC95(%rip), %rax
	cmovne	%rdx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1134:
	leaq	.LC81(%rip), %rax
	cmpl	$75169920, %edi
	je	.L960
	jbe	.L1146
	leaq	.LC85(%rip), %rax
	cmpl	$75432320, %edi
	je	.L960
	jbe	.L1147
	cmpl	$75440128, %edi
	movl	$0, %edx
	leaq	.LC67(%rip), %rax
	cmovne	%rdx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1133:
	leaq	.LC28(%rip), %rax
	cmpl	$6881280, %edi
	je	.L960
	jbe	.L1148
	leaq	.LC61(%rip), %rax
	cmpl	$16777216, %edi
	je	.L960
	jbe	.L1149
	leaq	.LC34(%rip), %rax
	cmpl	$67108864, %edi
	je	.L960
	jbe	.L1150
	cmpl	$67239936, %edi
	movl	$0, %edx
	leaq	.LC89(%rip), %rax
	cmovne	%rdx, %rax
	ret
.L1147:
	leaq	.LC86(%rip), %rax
	cmpl	$75431936, %edi
	je	.L960
	cmpl	$75432192, %edi
	movl	$0, %edx
	leaq	.LC88(%rip), %rax
	cmovne	%rdx, %rax
	ret
.L1135:
	leaq	.LC90(%rip), %rax
	cmpl	$134217728, %edi
	je	.L960
	jbe	.L1151
	cmpl	$134224990, %edi
	movl	$0, %edx
	leaq	.LC94(%rip), %rax
	cmovne	%rdx, %rax
	ret
.L1145:
	leaq	.LC47(%rip), %rax
	cmpl	$3146, %edi
	je	.L960
	cmpl	$3150, %edi
	movl	$0, %edx
	leaq	.LC22(%rip), %rax
	cmovne	%rdx, %rax
	ret
.L1151:
	leaq	.LC93(%rip), %rax
	cmpl	$75457408, %edi
	je	.L960
	cmpl	$75464702, %edi
	movl	$0, %edx
	leaq	.LC53(%rip), %rax
	cmovne	%rdx, %rax
	ret
.L1146:
	leaq	.LC80(%rip), %rax
	cmpl	$73596928, %edi
	je	.L960
	jbe	.L1152
	leaq	.LC82(%rip), %rax
	cmpl	$73859072, %edi
	je	.L960
	cmpl	$75169792, %edi
	movl	$0, %edx
	leaq	.LC87(%rip), %rax
	cmovne	%rdx, %rax
	ret
.L1150:
	leaq	.LC91(%rip), %rax
	cmpl	$33554432, %edi
	je	.L960
	cmpl	$58720256, %edi
	movl	$0, %edx
	leaq	.LC75(%rip), %rax
	cmovne	%rdx, %rax
	ret
.L1152:
	leaq	.LC76(%rip), %rax
	cmpl	$68288640, %edi
	je	.L960
	cmpl	$68681728, %edi
	movl	$0, %edx
	leaq	.LC84(%rip), %rax
	cmovne	%rdx, %rax
	ret
.L1149:
	leaq	.LC78(%rip), %rax
	cmpl	$8389504, %edi
	je	.L960
	jbe	.L1153
	leaq	.LC66(%rip), %rax
	cmpl	$8395870, %edi
	je	.L960
	cmpl	$8396766, %edi
	movl	$0, %edx
	leaq	.LC35(%rip), %rax
	cmovne	%rdx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1148:
	leaq	.LC27(%rip), %rax
	cmpl	$524288, %edi
	je	.L960
	jbe	.L1154
	leaq	.LC29(%rip), %rax
	cmpl	$2097152, %edi
	je	.L960
	jbe	.L1155
	cmpl	$4194304, %edi
	movl	$0, %edx
	leaq	.LC33(%rip), %rax
	cmovne	%rdx, %rax
	ret
.L1153:
	leaq	.LC32(%rip), %rax
	cmpl	$7143424, %edi
	je	.L960
	cmpl	$8388608, %edi
	movl	$0, %edx
	leaq	.LC65(%rip), %rax
	cmovne	%rdx, %rax
	ret
.L1155:
	leaq	.LC74(%rip), %rax
	cmpl	$1048576, %edi
	je	.L960
	cmpl	$1572864, %edi
	movl	$0, %edx
	leaq	.LC31(%rip), %rax
	cmovne	%rdx, %rax
	ret
.L1154:
	leaq	.LC25(%rip), %rax
	cmpl	$262144, %edi
	je	.L960
	jbe	.L1156
	leaq	.LC64(%rip), %rax
	cmpl	$262400, %edi
	je	.L960
	cmpl	$262528, %edi
	movl	$0, %edx
	leaq	.LC30(%rip), %rax
	cmovne	%rdx, %rax
	ret
.L1156:
	leaq	.LC26(%rip), %rax
	cmpl	$65536, %edi
	je	.L960
	cmpl	$131072, %edi
	movl	$0, %edx
	leaq	.LC73(%rip), %rax
	cmovne	%rdx, %rax
	ret
.L967:
	leaq	.LC100(%rip), %rax
	ret
.L969:
	leaq	.LC99(%rip), %rax
	ret
.L970:
	leaq	.LC98(%rip), %rax
	ret
.L971:
	leaq	.LC97(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L999:
	leaq	.LC44(%rip), %rax
	ret
	.cfi_endproc
.LFE18608:
	.size	_ZN2v88internal8compiler10BitsetType4NameEj, .-_ZN2v88internal8compiler10BitsetType4NameEj
	.section	.rodata._ZN2v88internal8compiler10BitsetType5PrintERSoj.str1.1,"aMS",@progbits,1
.LC102:
	.string	"("
.LC103:
	.string	" | "
.LC104:
	.string	")"
	.section	.text._ZN2v88internal8compiler10BitsetType5PrintERSoj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler10BitsetType5PrintERSoj
	.type	_ZN2v88internal8compiler10BitsetType5PrintERSoj, @function
_ZN2v88internal8compiler10BitsetType5PrintERSoj:
.LFB18609:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movl	%esi, %edi
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	call	_ZN2v88internal8compiler10BitsetType4NameEj
	testq	%rax, %rax
	je	.L1158
	movq	%rax, %rdi
	movq	%rax, %r13
	call	strlen@PLT
	movq	%r13, %rsi
	movq	%rax, %rdx
.L1171:
	addq	$24, %rsp
	movq	%r14, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	.p2align 4,,10
	.p2align 3
.L1158:
	.cfi_restore_state
	movl	%esi, %r12d
	movl	$1, %edx
	movq	%r14, %rdi
	leaq	.LC102(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	testl	%r12d, %r12d
	je	.L1159
	movl	$84, %ebx
	movl	$1, %ecx
	leaq	4+_ZZN2v88internal8compiler10BitsetType5PrintERSojE13named_bitsets(%rip), %r15
	jmp	.L1164
	.p2align 4,,10
	.p2align 3
.L1160:
	testl	%r12d, %r12d
	movl	%ebx, %eax
	sete	%dl
	shrl	$31, %eax
	subq	$1, %rbx
	orb	%al, %dl
	jne	.L1159
.L1164:
	movl	(%r15,%rbx,4), %r13d
	movl	%r12d, %eax
	andl	%r13d, %eax
	cmpl	%eax, %r13d
	jne	.L1160
	testb	%cl, %cl
	je	.L1172
	movl	%r13d, %edi
	call	_ZN2v88internal8compiler10BitsetType4NameEj
	testq	%rax, %rax
	je	.L1173
.L1162:
	movq	%rax, %rdi
	movq	%rax, -56(%rbp)
	call	strlen@PLT
	movq	-56(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L1163:
	subl	%r13d, %r12d
	xorl	%ecx, %ecx
	jmp	.L1160
	.p2align 4,,10
	.p2align 3
.L1159:
	movl	$1, %edx
	leaq	.LC104(%rip), %rsi
	jmp	.L1171
	.p2align 4,,10
	.p2align 3
.L1172:
	movq	%r14, %rdi
	movl	$3, %edx
	leaq	.LC103(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	%r13d, %edi
	call	_ZN2v88internal8compiler10BitsetType4NameEj
	testq	%rax, %rax
	jne	.L1162
.L1173:
	movq	(%r14), %rax
	movq	-24(%rax), %rdi
	addq	%r14, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L1163
	.cfi_endproc
.LFE18609:
	.size	_ZN2v88internal8compiler10BitsetType5PrintERSoj, .-_ZN2v88internal8compiler10BitsetType5PrintERSoj
	.section	.rodata._ZNK2v88internal8compiler4Type7PrintToERSo.str1.1,"aMS",@progbits,1
.LC105:
	.string	"HeapConstant("
.LC106:
	.string	"OtherNumberConstant("
.LC107:
	.string	"Range("
.LC108:
	.string	", "
.LC109:
	.string	"<"
.LC110:
	.string	">"
	.section	.text._ZNK2v88internal8compiler4Type7PrintToERSo,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler4Type7PrintToERSo
	.type	_ZNK2v88internal8compiler4Type7PrintToERSo, @function
_ZNK2v88internal8compiler4Type7PrintToERSo:
.LFB18610:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testb	$1, %sil
	jne	.L1194
	movl	(%rsi), %eax
	movq	%rdi, %rbx
	testl	%eax, %eax
	je	.L1195
	cmpl	$1, %eax
	je	.L1196
	cmpl	$4, %eax
	je	.L1197
	cmpl	$3, %eax
	je	.L1198
	cmpl	$2, %eax
	jne	.L1184
	movl	$1, %edx
	leaq	.LC109(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rdx
	movl	4(%rdx), %eax
	testl	%eax, %eax
	jle	.L1186
	movq	8(%rdx), %rdx
	subl	$1, %eax
	movl	$8, %r12d
	leaq	-64(%rbp), %r13
	leaq	8(,%rax,8), %rax
	movq	%rax, -72(%rbp)
	movq	(%rdx), %r14
	jmp	.L1189
	.p2align 4,,10
	.p2align 3
.L1192:
	movq	(%rbx), %rax
	movl	$2, %edx
	leaq	.LC108(%rip), %rsi
	movq	%r15, %rdi
	movq	8(%rax), %rax
	movq	(%rax,%r12), %r14
	addq	$8, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L1189:
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%r14, -64(%rbp)
	call	_ZNK2v88internal8compiler4Type7PrintToERSo
	cmpq	%r12, -72(%rbp)
	jne	.L1192
.L1186:
	movl	$1, %edx
	leaq	.LC110(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L1174
	.p2align 4,,10
	.p2align 3
.L1195:
	movl	$13, %edx
	leaq	.LC105(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rdi
	addq	$8, %rdi
	call	_ZNK2v88internal8compiler13HeapObjectRef6objectEv@PLT
	leaq	-64(%rbp), %rsi
	movq	%r15, %rdi
	movq	(%rax), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internallsERSoRKNS0_5BriefE@PLT
	movl	$1, %edx
	leaq	.LC104(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L1174:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1199
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1194:
	.cfi_restore_state
	xorl	$1, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler10BitsetType5PrintERSoj
	jmp	.L1174
	.p2align 4,,10
	.p2align 3
.L1198:
	movl	$1, %edx
	leaq	.LC102(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rdx
	movl	4(%rdx), %eax
	testl	%eax, %eax
	jle	.L1182
	movq	8(%rdx), %rdx
	subl	$1, %eax
	movl	$8, %r12d
	leaq	-64(%rbp), %r13
	leaq	8(,%rax,8), %rax
	movq	%rax, -72(%rbp)
	movq	(%rdx), %r14
	jmp	.L1188
	.p2align 4,,10
	.p2align 3
.L1191:
	movq	(%rbx), %rax
	movl	$3, %edx
	leaq	.LC103(%rip), %rsi
	movq	%r15, %rdi
	movq	8(%rax), %rax
	movq	(%rax,%r12), %r14
	addq	$8, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L1188:
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%r14, -64(%rbp)
	call	_ZNK2v88internal8compiler4Type7PrintToERSo
	cmpq	-72(%rbp), %r12
	jne	.L1191
.L1182:
	movl	$1, %edx
	leaq	.LC104(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L1174
	.p2align 4,,10
	.p2align 3
.L1196:
	movl	$20, %edx
	leaq	.LC106(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	movq	%r15, %rdi
	movsd	8(%rax), %xmm0
	call	_ZNSo9_M_insertIdEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC104(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L1174
	.p2align 4,,10
	.p2align 3
.L1197:
	movq	(%r15), %rax
	leaq	.LC107(%rip), %rsi
	movq	%r15, %rdi
	movq	-24(%rax), %rcx
	addq	%r15, %rcx
	movl	24(%rcx), %r13d
	movl	%r13d, %edx
	orl	$4, %edx
	movl	%edx, 24(%rcx)
	movq	(%r15), %rax
	movl	$6, %edx
	movq	-24(%rax), %rcx
	addq	%r15, %rcx
	movq	8(%rcx), %r14
	movq	$0, 8(%rcx)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	movq	%r15, %rdi
	movsd	8(%rax), %xmm0
	call	_ZNSo9_M_insertIdEERSoT_@PLT
	movl	$2, %edx
	leaq	.LC108(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	movq	%r12, %rdi
	movsd	16(%rax), %xmm0
	call	_ZNSo9_M_insertIdEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC104(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r15), %rax
	movq	-24(%rax), %rax
	movl	%r13d, 24(%r15,%rax)
	movq	(%r15), %rax
	movq	-24(%rax), %rax
	movq	%r14, 8(%r15,%rax)
	jmp	.L1174
.L1199:
	call	__stack_chk_fail@PLT
.L1184:
	leaq	.LC8(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18610:
	.size	_ZNK2v88internal8compiler4Type7PrintToERSo, .-_ZNK2v88internal8compiler4Type7PrintToERSo
	.section	.text._ZN2v88internal8compiler10BitsetType11SignedSmallEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler10BitsetType11SignedSmallEv
	.type	_ZN2v88internal8compiler10BitsetType11SignedSmallEv, @function
_ZN2v88internal8compiler10BitsetType11SignedSmallEv:
.LFB18611:
	.cfi_startproc
	endbr64
	movl	$1098, %eax
	ret
	.cfi_endproc
.LFE18611:
	.size	_ZN2v88internal8compiler10BitsetType11SignedSmallEv, .-_ZN2v88internal8compiler10BitsetType11SignedSmallEv
	.section	.text._ZN2v88internal8compiler10BitsetType13UnsignedSmallEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler10BitsetType13UnsignedSmallEv
	.type	_ZN2v88internal8compiler10BitsetType13UnsignedSmallEv, @function
_ZN2v88internal8compiler10BitsetType13UnsignedSmallEv:
.LFB18612:
	.cfi_startproc
	endbr64
	movl	$1026, %eax
	ret
	.cfi_endproc
.LFE18612:
	.size	_ZN2v88internal8compiler10BitsetType13UnsignedSmallEv, .-_ZN2v88internal8compiler10BitsetType13UnsignedSmallEv
	.section	.text._ZN2v88internal8compiler4Type5TupleES2_S2_S2_PNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler4Type5TupleES2_S2_S2_PNS0_4ZoneE
	.type	_ZN2v88internal8compiler4Type5TupleES2_S2_S2_PNS0_4ZoneE, @function
_ZN2v88internal8compiler4Type5TupleES2_S2_S2_PNS0_4ZoneE:
.LFB18613:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	subq	$16, %rsp
	movq	16(%rcx), %r13
	movq	24(%rcx), %rax
	subq	%r13, %rax
	cmpq	$15, %rax
	jbe	.L1208
	leaq	16(%r13), %rax
	movq	%rax, 16(%rcx)
.L1204:
	movabsq	$12884901890, %rax
	movq	%rax, 0(%r13)
	movq	16(%rcx), %rax
	movq	24(%rcx), %rdx
	subq	%rax, %rdx
	cmpq	$23, %rdx
	jbe	.L1209
	leaq	24(%rax), %rdx
	movq	%rdx, 16(%rcx)
.L1206:
	movq	%rax, 8(%r13)
	movq	%r15, (%rax)
	movq	8(%r13), %rax
	movq	%r14, 8(%rax)
	movq	8(%r13), %rax
	movq	%rbx, 16(%rax)
	addq	$16, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1208:
	.cfi_restore_state
	movq	%rcx, %rdi
	movl	$16, %esi
	movq	%rcx, -40(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-40(%rbp), %rcx
	movq	%rax, %r13
	jmp	.L1204
	.p2align 4,,10
	.p2align 3
.L1209:
	movl	$24, %esi
	movq	%rcx, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1206
	.cfi_endproc
.LFE18613:
	.size	_ZN2v88internal8compiler4Type5TupleES2_S2_S2_PNS0_4ZoneE, .-_ZN2v88internal8compiler4Type5TupleES2_S2_S2_PNS0_4ZoneE
	.section	.rodata._ZN2v88internal8compiler4Type19OtherNumberConstantEdPNS0_4ZoneE.str1.1,"aMS",@progbits,1
.LC111:
	.string	"IsOtherNumberConstant(value)"
.LC112:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal8compiler4Type19OtherNumberConstantEdPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler4Type19OtherNumberConstantEdPNS0_4ZoneE
	.type	_ZN2v88internal8compiler4Type19OtherNumberConstantEdPNS0_4ZoneE, @function
_ZN2v88internal8compiler4Type19OtherNumberConstantEdPNS0_4ZoneE:
.LFB18614:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movapd	%xmm0, %xmm1
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 12, -24
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	subq	%r12, %rax
	cmpq	$15, %rax
	jbe	.L1218
	leaq	16(%r12), %rax
	movq	%rax, 16(%rdi)
.L1212:
	ucomisd	%xmm1, %xmm1
	movl	$1, (%r12)
	movsd	%xmm1, 8(%r12)
	jp	.L1213
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -24(%rbp)
	call	nearbyint@PLT
	movsd	-24(%rbp), %xmm1
	ucomisd	%xmm0, %xmm1
	jp	.L1215
	jne	.L1215
.L1213:
	leaq	.LC111(%rip), %rsi
	leaq	.LC112(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1215:
	movabsq	$-9223372036854775808, %rax
	movq	%xmm1, %rdx
	cmpq	%rax, %rdx
	je	.L1213
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1218:
	.cfi_restore_state
	movl	$16, %esi
	movsd	%xmm0, -24(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movsd	-24(%rbp), %xmm1
	movq	%rax, %r12
	jmp	.L1212
	.cfi_endproc
.LFE18614:
	.size	_ZN2v88internal8compiler4Type19OtherNumberConstantEdPNS0_4ZoneE, .-_ZN2v88internal8compiler4Type19OtherNumberConstantEdPNS0_4ZoneE
	.section	.rodata._ZN2v88internal8compiler4Type12HeapConstantEPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEPNS0_4ZoneE.str1.1,"aMS",@progbits,1
.LC113:
	.string	"IsHeapObject()"
	.section	.text._ZN2v88internal8compiler4Type12HeapConstantEPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler4Type12HeapConstantEPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEPNS0_4ZoneE
	.type	_ZN2v88internal8compiler4Type12HeapConstantEPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEPNS0_4ZoneE, @function
_ZN2v88internal8compiler4Type12HeapConstantEPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEPNS0_4ZoneE:
.LFB18615:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	movq	%rdx, %r12
	movq	%rsi, %rdx
	pushq	%rbx
	movq	%rdi, %rsi
	movq	%r13, %rdi
	subq	$40, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L1250
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler13HeapObjectRef17GetHeapObjectTypeEv@PLT
	movq	%rax, %rcx
	sarq	$32, %rcx
	cmpw	$168, %ax
	ja	.L1251
	leaq	.L1232(%rip), %rsi
	movzwl	%ax, %ecx
	movslq	(%rsi,%rcx,4), %rdx
	addq	%rsi, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal8compiler4Type12HeapConstantEPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEPNS0_4ZoneE,"a",@progbits
	.align 4
	.align 4
.L1232:
	.long	.L1238-.L1232
	.long	.L1223-.L1232
	.long	.L1238-.L1232
	.long	.L1223-.L1232
	.long	.L1223-.L1232
	.long	.L1223-.L1232
	.long	.L1223-.L1232
	.long	.L1223-.L1232
	.long	.L1238-.L1232
	.long	.L1223-.L1232
	.long	.L1238-.L1232
	.long	.L1223-.L1232
	.long	.L1223-.L1232
	.long	.L1223-.L1232
	.long	.L1223-.L1232
	.long	.L1223-.L1232
	.long	.L1223-.L1232
	.long	.L1223-.L1232
	.long	.L1238-.L1232
	.long	.L1223-.L1232
	.long	.L1223-.L1232
	.long	.L1223-.L1232
	.long	.L1223-.L1232
	.long	.L1223-.L1232
	.long	.L1223-.L1232
	.long	.L1223-.L1232
	.long	.L1238-.L1232
	.long	.L1223-.L1232
	.long	.L1223-.L1232
	.long	.L1223-.L1232
	.long	.L1223-.L1232
	.long	.L1223-.L1232
	.long	.L1237-.L1232
	.long	.L1237-.L1232
	.long	.L1237-.L1232
	.long	.L1237-.L1232
	.long	.L1223-.L1232
	.long	.L1237-.L1232
	.long	.L1223-.L1232
	.long	.L1223-.L1232
	.long	.L1237-.L1232
	.long	.L1237-.L1232
	.long	.L1237-.L1232
	.long	.L1237-.L1232
	.long	.L1223-.L1232
	.long	.L1237-.L1232
	.long	.L1223-.L1232
	.long	.L1223-.L1232
	.long	.L1223-.L1232
	.long	.L1223-.L1232
	.long	.L1237-.L1232
	.long	.L1223-.L1232
	.long	.L1223-.L1232
	.long	.L1223-.L1232
	.long	.L1223-.L1232
	.long	.L1223-.L1232
	.long	.L1223-.L1232
	.long	.L1223-.L1232
	.long	.L1237-.L1232
	.long	.L1223-.L1232
	.long	.L1223-.L1232
	.long	.L1223-.L1232
	.long	.L1223-.L1232
	.long	.L1223-.L1232
	.long	.L1244-.L1232
	.long	.L1236-.L1232
	.long	.L1235-.L1232
	.long	.L1234-.L1232
	.long	.L1233-.L1232
	.long	.L1233-.L1232
	.long	.L1233-.L1232
	.long	.L1233-.L1232
	.long	.L1233-.L1232
	.long	.L1223-.L1232
	.long	.L1233-.L1232
	.long	.L1233-.L1232
	.long	.L1223-.L1232
	.long	.L1223-.L1232
	.long	.L1233-.L1232
	.long	.L1233-.L1232
	.long	.L1223-.L1232
	.long	.L1223-.L1232
	.long	.L1233-.L1232
	.long	.L1223-.L1232
	.long	.L1223-.L1232
	.long	.L1223-.L1232
	.long	.L1223-.L1232
	.long	.L1223-.L1232
	.long	.L1233-.L1232
	.long	.L1233-.L1232
	.long	.L1223-.L1232
	.long	.L1223-.L1232
	.long	.L1223-.L1232
	.long	.L1223-.L1232
	.long	.L1223-.L1232
	.long	.L1223-.L1232
	.long	.L1233-.L1232
	.long	.L1223-.L1232
	.long	.L1233-.L1232
	.long	.L1223-.L1232
	.long	.L1223-.L1232
	.long	.L1223-.L1232
	.long	.L1223-.L1232
	.long	.L1223-.L1232
	.long	.L1223-.L1232
	.long	.L1223-.L1232
	.long	.L1223-.L1232
	.long	.L1223-.L1232
	.long	.L1223-.L1232
	.long	.L1223-.L1232
	.long	.L1223-.L1232
	.long	.L1223-.L1232
	.long	.L1223-.L1232
	.long	.L1223-.L1232
	.long	.L1223-.L1232
	.long	.L1223-.L1232
	.long	.L1223-.L1232
	.long	.L1223-.L1232
	.long	.L1223-.L1232
	.long	.L1233-.L1232
	.long	.L1233-.L1232
	.long	.L1233-.L1232
	.long	.L1233-.L1232
	.long	.L1233-.L1232
	.long	.L1233-.L1232
	.long	.L1233-.L1232
	.long	.L1233-.L1232
	.long	.L1233-.L1232
	.long	.L1233-.L1232
	.long	.L1233-.L1232
	.long	.L1233-.L1232
	.long	.L1233-.L1232
	.long	.L1233-.L1232
	.long	.L1233-.L1232
	.long	.L1233-.L1232
	.long	.L1233-.L1232
	.long	.L1233-.L1232
	.long	.L1233-.L1232
	.long	.L1233-.L1232
	.long	.L1233-.L1232
	.long	.L1233-.L1232
	.long	.L1233-.L1232
	.long	.L1233-.L1232
	.long	.L1233-.L1232
	.long	.L1233-.L1232
	.long	.L1233-.L1232
	.long	.L1233-.L1232
	.long	.L1233-.L1232
	.long	.L1233-.L1232
	.long	.L1233-.L1232
	.long	.L1223-.L1232
	.long	.L1233-.L1232
	.long	.L1223-.L1232
	.long	.L1233-.L1232
	.long	.L1233-.L1232
	.long	.L1233-.L1232
	.long	.L1223-.L1232
	.long	.L1233-.L1232
	.long	.L1233-.L1232
	.long	.L1233-.L1232
	.long	.L1233-.L1232
	.long	.L1223-.L1232
	.long	.L1223-.L1232
	.long	.L1223-.L1232
	.long	.L1223-.L1232
	.long	.L1233-.L1232
	.long	.L1233-.L1232
	.long	.L1233-.L1232
	.long	.L1227-.L1232
	.section	.text._ZN2v88internal8compiler4Type12HeapConstantEPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEPNS0_4ZoneE
.L1227:
	movl	$131072, %ebx
.L1226:
	movq	16(%r12), %rax
	movq	24(%r12), %rdx
	subq	%rax, %rdx
	cmpq	$23, %rdx
	jbe	.L1252
	leaq	24(%rax), %rdx
	movq	%rdx, 16(%r12)
.L1241:
	movl	$0, (%rax)
	movl	%ebx, 4(%rax)
	movdqa	-64(%rbp), %xmm0
	movups	%xmm0, 8(%rax)
	movq	-40(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L1253
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1244:
	.cfi_restore_state
	movl	$8192, %ebx
	jmp	.L1226
.L1238:
	movl	$16384, %ebx
	jmp	.L1226
	.p2align 4,,10
	.p2align 3
.L1251:
	subw	$1024, %ax
	cmpw	$81, %ax
	ja	.L1223
	leaq	.L1225(%rip), %rdx
	movzwl	%ax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler4Type12HeapConstantEPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEPNS0_4ZoneE
	.align 4
	.align 4
.L1225:
	.long	.L1230-.L1225
	.long	.L1228-.L1225
	.long	.L1228-.L1225
	.long	.L1227-.L1225
	.long	.L1223-.L1225
	.long	.L1223-.L1225
	.long	.L1223-.L1225
	.long	.L1223-.L1225
	.long	.L1223-.L1225
	.long	.L1223-.L1225
	.long	.L1223-.L1225
	.long	.L1223-.L1225
	.long	.L1223-.L1225
	.long	.L1223-.L1225
	.long	.L1223-.L1225
	.long	.L1223-.L1225
	.long	.L1228-.L1225
	.long	.L1227-.L1225
	.long	.L1223-.L1225
	.long	.L1223-.L1225
	.long	.L1223-.L1225
	.long	.L1223-.L1225
	.long	.L1223-.L1225
	.long	.L1223-.L1225
	.long	.L1223-.L1225
	.long	.L1223-.L1225
	.long	.L1223-.L1225
	.long	.L1223-.L1225
	.long	.L1223-.L1225
	.long	.L1223-.L1225
	.long	.L1223-.L1225
	.long	.L1223-.L1225
	.long	.L1228-.L1225
	.long	.L1228-.L1225
	.long	.L1228-.L1225
	.long	.L1227-.L1225
	.long	.L1227-.L1225
	.long	.L1229-.L1225
	.long	.L1227-.L1225
	.long	.L1227-.L1225
	.long	.L1227-.L1225
	.long	.L1227-.L1225
	.long	.L1227-.L1225
	.long	.L1228-.L1225
	.long	.L1227-.L1225
	.long	.L1227-.L1225
	.long	.L1227-.L1225
	.long	.L1227-.L1225
	.long	.L1227-.L1225
	.long	.L1227-.L1225
	.long	.L1227-.L1225
	.long	.L1227-.L1225
	.long	.L1227-.L1225
	.long	.L1227-.L1225
	.long	.L1227-.L1225
	.long	.L1227-.L1225
	.long	.L1227-.L1225
	.long	.L1227-.L1225
	.long	.L1227-.L1225
	.long	.L1227-.L1225
	.long	.L1227-.L1225
	.long	.L1227-.L1225
	.long	.L1227-.L1225
	.long	.L1227-.L1225
	.long	.L1227-.L1225
	.long	.L1227-.L1225
	.long	.L1227-.L1225
	.long	.L1227-.L1225
	.long	.L1227-.L1225
	.long	.L1227-.L1225
	.long	.L1227-.L1225
	.long	.L1227-.L1225
	.long	.L1227-.L1225
	.long	.L1227-.L1225
	.long	.L1227-.L1225
	.long	.L1227-.L1225
	.long	.L1227-.L1225
	.long	.L1227-.L1225
	.long	.L1227-.L1225
	.long	.L1227-.L1225
	.long	.L1243-.L1225
	.long	.L1224-.L1225
	.section	.text._ZN2v88internal8compiler4Type12HeapConstantEPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEPNS0_4ZoneE
.L1243:
	movl	$4194304, %ebx
	jmp	.L1226
.L1229:
	movl	$67108864, %ebx
	jmp	.L1226
.L1237:
	movl	$16416, %ebx
	jmp	.L1226
.L1236:
	movl	$7262, %ebx
	jmp	.L1226
.L1235:
	movl	$134217728, %ebx
	jmp	.L1226
.L1234:
	shrq	$16, %rax
	subl	$1, %eax
	cmpb	$5, %al
	ja	.L1223
	movzbl	%al, %eax
	leaq	CSWTCH.658(%rip), %rdx
	movl	(%rdx,%rax,4), %ebx
	jmp	.L1226
.L1233:
	movl	$16777216, %ebx
	jmp	.L1226
.L1230:
	andl	$2, %ecx
	cmpl	$1, %ecx
	sbbl	%ebx, %ebx
	andl	$524288, %ebx
	addl	$524288, %ebx
	jmp	.L1226
.L1228:
	movl	$262144, %ebx
	testb	$1, %cl
	jne	.L1226
	andl	$2, %ecx
	cmpl	$1, %ecx
	sbbl	%ebx, %ebx
	andl	$65536, %ebx
	addl	$65536, %ebx
	jmp	.L1226
.L1224:
	movl	$2097152, %ebx
	jmp	.L1226
	.p2align 4,,10
	.p2align 3
.L1250:
	leaq	.LC113(%rip), %rsi
	leaq	.LC112(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1252:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1241
.L1223:
	leaq	.LC8(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1253:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18615:
	.size	_ZN2v88internal8compiler4Type12HeapConstantEPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEPNS0_4ZoneE, .-_ZN2v88internal8compiler4Type12HeapConstantEPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEPNS0_4ZoneE
	.section	.text._ZN2v88internal8compiler4Type12HeapConstantERKNS1_13HeapObjectRefEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler4Type12HeapConstantERKNS1_13HeapObjectRefEPNS0_4ZoneE
	.type	_ZN2v88internal8compiler4Type12HeapConstantERKNS1_13HeapObjectRefEPNS0_4ZoneE, @function
_ZN2v88internal8compiler4Type12HeapConstantERKNS1_13HeapObjectRefEPNS0_4ZoneE:
.LFB18616:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	call	_ZNK2v88internal8compiler13HeapObjectRef17GetHeapObjectTypeEv@PLT
	movq	%rax, %rcx
	sarq	$32, %rcx
	cmpw	$168, %ax
	ja	.L1283
	leaq	.L1266(%rip), %rsi
	movzwl	%ax, %ecx
	movslq	(%rsi,%rcx,4), %rdx
	addq	%rsi, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal8compiler4Type12HeapConstantERKNS1_13HeapObjectRefEPNS0_4ZoneE,"a",@progbits
	.align 4
	.align 4
.L1266:
	.long	.L1272-.L1266
	.long	.L1257-.L1266
	.long	.L1272-.L1266
	.long	.L1257-.L1266
	.long	.L1257-.L1266
	.long	.L1257-.L1266
	.long	.L1257-.L1266
	.long	.L1257-.L1266
	.long	.L1272-.L1266
	.long	.L1257-.L1266
	.long	.L1272-.L1266
	.long	.L1257-.L1266
	.long	.L1257-.L1266
	.long	.L1257-.L1266
	.long	.L1257-.L1266
	.long	.L1257-.L1266
	.long	.L1257-.L1266
	.long	.L1257-.L1266
	.long	.L1272-.L1266
	.long	.L1257-.L1266
	.long	.L1257-.L1266
	.long	.L1257-.L1266
	.long	.L1257-.L1266
	.long	.L1257-.L1266
	.long	.L1257-.L1266
	.long	.L1257-.L1266
	.long	.L1272-.L1266
	.long	.L1257-.L1266
	.long	.L1257-.L1266
	.long	.L1257-.L1266
	.long	.L1257-.L1266
	.long	.L1257-.L1266
	.long	.L1271-.L1266
	.long	.L1271-.L1266
	.long	.L1271-.L1266
	.long	.L1271-.L1266
	.long	.L1257-.L1266
	.long	.L1271-.L1266
	.long	.L1257-.L1266
	.long	.L1257-.L1266
	.long	.L1271-.L1266
	.long	.L1271-.L1266
	.long	.L1271-.L1266
	.long	.L1271-.L1266
	.long	.L1257-.L1266
	.long	.L1271-.L1266
	.long	.L1257-.L1266
	.long	.L1257-.L1266
	.long	.L1257-.L1266
	.long	.L1257-.L1266
	.long	.L1271-.L1266
	.long	.L1257-.L1266
	.long	.L1257-.L1266
	.long	.L1257-.L1266
	.long	.L1257-.L1266
	.long	.L1257-.L1266
	.long	.L1257-.L1266
	.long	.L1257-.L1266
	.long	.L1271-.L1266
	.long	.L1257-.L1266
	.long	.L1257-.L1266
	.long	.L1257-.L1266
	.long	.L1257-.L1266
	.long	.L1257-.L1266
	.long	.L1277-.L1266
	.long	.L1270-.L1266
	.long	.L1269-.L1266
	.long	.L1268-.L1266
	.long	.L1267-.L1266
	.long	.L1267-.L1266
	.long	.L1267-.L1266
	.long	.L1267-.L1266
	.long	.L1267-.L1266
	.long	.L1257-.L1266
	.long	.L1267-.L1266
	.long	.L1267-.L1266
	.long	.L1257-.L1266
	.long	.L1257-.L1266
	.long	.L1267-.L1266
	.long	.L1267-.L1266
	.long	.L1257-.L1266
	.long	.L1257-.L1266
	.long	.L1267-.L1266
	.long	.L1257-.L1266
	.long	.L1257-.L1266
	.long	.L1257-.L1266
	.long	.L1257-.L1266
	.long	.L1257-.L1266
	.long	.L1267-.L1266
	.long	.L1267-.L1266
	.long	.L1257-.L1266
	.long	.L1257-.L1266
	.long	.L1257-.L1266
	.long	.L1257-.L1266
	.long	.L1257-.L1266
	.long	.L1257-.L1266
	.long	.L1267-.L1266
	.long	.L1257-.L1266
	.long	.L1267-.L1266
	.long	.L1257-.L1266
	.long	.L1257-.L1266
	.long	.L1257-.L1266
	.long	.L1257-.L1266
	.long	.L1257-.L1266
	.long	.L1257-.L1266
	.long	.L1257-.L1266
	.long	.L1257-.L1266
	.long	.L1257-.L1266
	.long	.L1257-.L1266
	.long	.L1257-.L1266
	.long	.L1257-.L1266
	.long	.L1257-.L1266
	.long	.L1257-.L1266
	.long	.L1257-.L1266
	.long	.L1257-.L1266
	.long	.L1257-.L1266
	.long	.L1257-.L1266
	.long	.L1257-.L1266
	.long	.L1257-.L1266
	.long	.L1267-.L1266
	.long	.L1267-.L1266
	.long	.L1267-.L1266
	.long	.L1267-.L1266
	.long	.L1267-.L1266
	.long	.L1267-.L1266
	.long	.L1267-.L1266
	.long	.L1267-.L1266
	.long	.L1267-.L1266
	.long	.L1267-.L1266
	.long	.L1267-.L1266
	.long	.L1267-.L1266
	.long	.L1267-.L1266
	.long	.L1267-.L1266
	.long	.L1267-.L1266
	.long	.L1267-.L1266
	.long	.L1267-.L1266
	.long	.L1267-.L1266
	.long	.L1267-.L1266
	.long	.L1267-.L1266
	.long	.L1267-.L1266
	.long	.L1267-.L1266
	.long	.L1267-.L1266
	.long	.L1267-.L1266
	.long	.L1267-.L1266
	.long	.L1267-.L1266
	.long	.L1267-.L1266
	.long	.L1267-.L1266
	.long	.L1267-.L1266
	.long	.L1267-.L1266
	.long	.L1267-.L1266
	.long	.L1257-.L1266
	.long	.L1267-.L1266
	.long	.L1257-.L1266
	.long	.L1267-.L1266
	.long	.L1267-.L1266
	.long	.L1267-.L1266
	.long	.L1257-.L1266
	.long	.L1267-.L1266
	.long	.L1267-.L1266
	.long	.L1267-.L1266
	.long	.L1267-.L1266
	.long	.L1257-.L1266
	.long	.L1257-.L1266
	.long	.L1257-.L1266
	.long	.L1257-.L1266
	.long	.L1267-.L1266
	.long	.L1267-.L1266
	.long	.L1267-.L1266
	.long	.L1261-.L1266
	.section	.text._ZN2v88internal8compiler4Type12HeapConstantERKNS1_13HeapObjectRefEPNS0_4ZoneE
.L1261:
	movl	$131072, %ebx
.L1260:
	movq	16(%r13), %rax
	movq	24(%r13), %rdx
	subq	%rax, %rdx
	cmpq	$23, %rdx
	jbe	.L1284
	leaq	24(%rax), %rdx
	movq	%rdx, 16(%r13)
.L1275:
	movl	$0, (%rax)
	movdqu	(%r12), %xmm0
	movl	%ebx, 4(%rax)
	movups	%xmm0, 8(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1277:
	.cfi_restore_state
	movl	$8192, %ebx
	jmp	.L1260
.L1272:
	movl	$16384, %ebx
	jmp	.L1260
	.p2align 4,,10
	.p2align 3
.L1283:
	subw	$1024, %ax
	cmpw	$81, %ax
	ja	.L1257
	leaq	.L1259(%rip), %rdx
	movzwl	%ax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler4Type12HeapConstantERKNS1_13HeapObjectRefEPNS0_4ZoneE
	.align 4
	.align 4
.L1259:
	.long	.L1264-.L1259
	.long	.L1262-.L1259
	.long	.L1262-.L1259
	.long	.L1261-.L1259
	.long	.L1257-.L1259
	.long	.L1257-.L1259
	.long	.L1257-.L1259
	.long	.L1257-.L1259
	.long	.L1257-.L1259
	.long	.L1257-.L1259
	.long	.L1257-.L1259
	.long	.L1257-.L1259
	.long	.L1257-.L1259
	.long	.L1257-.L1259
	.long	.L1257-.L1259
	.long	.L1257-.L1259
	.long	.L1262-.L1259
	.long	.L1261-.L1259
	.long	.L1257-.L1259
	.long	.L1257-.L1259
	.long	.L1257-.L1259
	.long	.L1257-.L1259
	.long	.L1257-.L1259
	.long	.L1257-.L1259
	.long	.L1257-.L1259
	.long	.L1257-.L1259
	.long	.L1257-.L1259
	.long	.L1257-.L1259
	.long	.L1257-.L1259
	.long	.L1257-.L1259
	.long	.L1257-.L1259
	.long	.L1257-.L1259
	.long	.L1262-.L1259
	.long	.L1262-.L1259
	.long	.L1262-.L1259
	.long	.L1261-.L1259
	.long	.L1261-.L1259
	.long	.L1263-.L1259
	.long	.L1261-.L1259
	.long	.L1261-.L1259
	.long	.L1261-.L1259
	.long	.L1261-.L1259
	.long	.L1261-.L1259
	.long	.L1262-.L1259
	.long	.L1261-.L1259
	.long	.L1261-.L1259
	.long	.L1261-.L1259
	.long	.L1261-.L1259
	.long	.L1261-.L1259
	.long	.L1261-.L1259
	.long	.L1261-.L1259
	.long	.L1261-.L1259
	.long	.L1261-.L1259
	.long	.L1261-.L1259
	.long	.L1261-.L1259
	.long	.L1261-.L1259
	.long	.L1261-.L1259
	.long	.L1261-.L1259
	.long	.L1261-.L1259
	.long	.L1261-.L1259
	.long	.L1261-.L1259
	.long	.L1261-.L1259
	.long	.L1261-.L1259
	.long	.L1261-.L1259
	.long	.L1261-.L1259
	.long	.L1261-.L1259
	.long	.L1261-.L1259
	.long	.L1261-.L1259
	.long	.L1261-.L1259
	.long	.L1261-.L1259
	.long	.L1261-.L1259
	.long	.L1261-.L1259
	.long	.L1261-.L1259
	.long	.L1261-.L1259
	.long	.L1261-.L1259
	.long	.L1261-.L1259
	.long	.L1261-.L1259
	.long	.L1261-.L1259
	.long	.L1261-.L1259
	.long	.L1261-.L1259
	.long	.L1276-.L1259
	.long	.L1258-.L1259
	.section	.text._ZN2v88internal8compiler4Type12HeapConstantERKNS1_13HeapObjectRefEPNS0_4ZoneE
.L1276:
	movl	$4194304, %ebx
	jmp	.L1260
.L1263:
	movl	$67108864, %ebx
	jmp	.L1260
.L1258:
	movl	$2097152, %ebx
	jmp	.L1260
.L1271:
	movl	$16416, %ebx
	jmp	.L1260
.L1270:
	movl	$7262, %ebx
	jmp	.L1260
.L1269:
	movl	$134217728, %ebx
	jmp	.L1260
.L1268:
	shrq	$16, %rax
	subl	$1, %eax
	cmpb	$5, %al
	ja	.L1257
	movzbl	%al, %eax
	leaq	CSWTCH.658(%rip), %rdx
	movl	(%rdx,%rax,4), %ebx
	jmp	.L1260
.L1267:
	movl	$16777216, %ebx
	jmp	.L1260
.L1264:
	andl	$2, %ecx
	cmpl	$1, %ecx
	sbbl	%ebx, %ebx
	andl	$524288, %ebx
	addl	$524288, %ebx
	jmp	.L1260
.L1262:
	movl	$262144, %ebx
	testb	$1, %cl
	jne	.L1260
	andl	$2, %ecx
	cmpl	$1, %ecx
	sbbl	%ebx, %ebx
	andl	$65536, %ebx
	addl	$65536, %ebx
	jmp	.L1260
	.p2align 4,,10
	.p2align 3
.L1284:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1275
.L1257:
	leaq	.LC8(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18616:
	.size	_ZN2v88internal8compiler4Type12HeapConstantERKNS1_13HeapObjectRefEPNS0_4ZoneE, .-_ZN2v88internal8compiler4Type12HeapConstantERKNS1_13HeapObjectRefEPNS0_4ZoneE
	.section	.text._ZN2v88internal8compiler4Type5RangeEddPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler4Type5RangeEddPNS0_4ZoneE
	.type	_ZN2v88internal8compiler4Type5RangeEddPNS0_4ZoneE, @function
_ZN2v88internal8compiler4Type5RangeEddPNS0_4ZoneE:
.LFB18617:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movsd	.LC0(%rip), %xmm2
	comisd	%xmm0, %xmm2
	jbe	.L1311
	comisd	%xmm1, %xmm2
	ja	.L1304
	movsd	.LC1(%rip), %xmm2
	movl	$16, %eax
	comisd	%xmm0, %xmm2
	jbe	.L1289
	orl	$8, %eax
	comisd	%xmm1, %xmm2
	movl	%eax, %ebx
	ja	.L1300
.L1289:
	pxor	%xmm2, %xmm2
	comisd	%xmm0, %xmm2
	jbe	.L1291
	orl	$64, %eax
	comisd	%xmm1, %xmm2
	movl	%eax, %ebx
	ja	.L1300
.L1291:
	movsd	.LC3(%rip), %xmm2
	comisd	%xmm0, %xmm2
	jbe	.L1293
	orb	$4, %ah
	comisd	%xmm1, %xmm2
	movl	%eax, %ebx
	ja	.L1300
.L1293:
	movsd	.LC4(%rip), %xmm2
	comisd	%xmm0, %xmm2
	jbe	.L1295
	orl	$2, %eax
	comisd	%xmm1, %xmm2
	movl	%eax, %ebx
	ja	.L1300
.L1295:
	movsd	.LC5(%rip), %xmm2
	movl	%eax, %ebx
	comisd	%xmm0, %xmm2
	ja	.L1313
.L1297:
	orl	$16, %ebx
.L1300:
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$23, %rdx
	jbe	.L1314
	leaq	24(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L1302:
	unpcklpd	%xmm1, %xmm0
	movl	$4, (%rax)
	movl	%ebx, 4(%rax)
	movups	%xmm0, 8(%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1311:
	.cfi_restore_state
	movsd	.LC1(%rip), %xmm2
	xorl	%eax, %eax
	comisd	%xmm0, %xmm2
	jbe	.L1289
	orl	$8, %eax
	comisd	%xmm1, %xmm2
	movl	%eax, %ebx
	jbe	.L1289
	jmp	.L1300
	.p2align 4,,10
	.p2align 3
.L1313:
	orl	$4, %ebx
	comisd	%xmm1, %xmm2
	jbe	.L1297
	jmp	.L1300
	.p2align 4,,10
	.p2align 3
.L1314:
	movl	$24, %esi
	movsd	%xmm1, -32(%rbp)
	movsd	%xmm0, -24(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movsd	-24(%rbp), %xmm0
	movsd	-32(%rbp), %xmm1
	jmp	.L1302
	.p2align 4,,10
	.p2align 3
.L1304:
	movl	$16, %ebx
	jmp	.L1300
	.cfi_endproc
.LFE18617:
	.size	_ZN2v88internal8compiler4Type5RangeEddPNS0_4ZoneE, .-_ZN2v88internal8compiler4Type5RangeEddPNS0_4ZoneE
	.section	.text._ZN2v88internal8compiler4Type5RangeENS1_9RangeType6LimitsEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler4Type5RangeENS1_9RangeType6LimitsEPNS0_4ZoneE
	.type	_ZN2v88internal8compiler4Type5RangeENS1_9RangeType6LimitsEPNS0_4ZoneE, @function
_ZN2v88internal8compiler4Type5RangeENS1_9RangeType6LimitsEPNS0_4ZoneE:
.LFB18618:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movsd	.LC0(%rip), %xmm2
	comisd	%xmm0, %xmm2
	jbe	.L1341
	comisd	%xmm1, %xmm2
	ja	.L1334
	movsd	.LC1(%rip), %xmm2
	movl	$16, %eax
	comisd	%xmm0, %xmm2
	jbe	.L1319
	orl	$8, %eax
	comisd	%xmm1, %xmm2
	movl	%eax, %ebx
	ja	.L1330
.L1319:
	pxor	%xmm2, %xmm2
	comisd	%xmm0, %xmm2
	jbe	.L1321
	orl	$64, %eax
	comisd	%xmm1, %xmm2
	movl	%eax, %ebx
	ja	.L1330
.L1321:
	movsd	.LC3(%rip), %xmm2
	comisd	%xmm0, %xmm2
	jbe	.L1323
	orb	$4, %ah
	comisd	%xmm1, %xmm2
	movl	%eax, %ebx
	ja	.L1330
.L1323:
	movsd	.LC4(%rip), %xmm2
	comisd	%xmm0, %xmm2
	jbe	.L1325
	orl	$2, %eax
	comisd	%xmm1, %xmm2
	movl	%eax, %ebx
	ja	.L1330
.L1325:
	movsd	.LC5(%rip), %xmm2
	movl	%eax, %ebx
	comisd	%xmm0, %xmm2
	ja	.L1343
.L1327:
	orl	$16, %ebx
.L1330:
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$23, %rdx
	jbe	.L1344
	leaq	24(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L1332:
	unpcklpd	%xmm1, %xmm0
	movl	$4, (%rax)
	movl	%ebx, 4(%rax)
	movups	%xmm0, 8(%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1341:
	.cfi_restore_state
	movsd	.LC1(%rip), %xmm2
	xorl	%eax, %eax
	comisd	%xmm0, %xmm2
	jbe	.L1319
	orl	$8, %eax
	comisd	%xmm1, %xmm2
	movl	%eax, %ebx
	jbe	.L1319
	jmp	.L1330
	.p2align 4,,10
	.p2align 3
.L1343:
	orl	$4, %ebx
	comisd	%xmm1, %xmm2
	jbe	.L1327
	jmp	.L1330
	.p2align 4,,10
	.p2align 3
.L1344:
	movl	$24, %esi
	movsd	%xmm1, -32(%rbp)
	movsd	%xmm0, -24(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movsd	-24(%rbp), %xmm0
	movsd	-32(%rbp), %xmm1
	jmp	.L1332
	.p2align 4,,10
	.p2align 3
.L1334:
	movl	$16, %ebx
	jmp	.L1330
	.cfi_endproc
.LFE18618:
	.size	_ZN2v88internal8compiler4Type5RangeENS1_9RangeType6LimitsEPNS0_4ZoneE, .-_ZN2v88internal8compiler4Type5RangeENS1_9RangeType6LimitsEPNS0_4ZoneE
	.section	.text._ZN2v88internal8compiler4Type5UnionEiPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler4Type5UnionEiPNS0_4ZoneE
	.type	_ZN2v88internal8compiler4Type5UnionEiPNS0_4ZoneE, @function
_ZN2v88internal8compiler4Type5UnionEiPNS0_4ZoneE:
.LFB18619:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%edi, %ebx
	subq	$8, %rsp
	movq	16(%rsi), %r13
	movq	24(%rsi), %rax
	subq	%r13, %rax
	cmpq	$15, %rax
	jbe	.L1351
	leaq	16(%r13), %rax
	movq	%rax, 16(%rsi)
.L1347:
	movl	$3, 0(%r13)
	movslq	%ebx, %rsi
	movl	%ebx, 4(%r13)
	movq	16(%r12), %rax
	salq	$3, %rsi
	movq	24(%r12), %rdx
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L1352
	addq	%rax, %rsi
	movq	%rsi, 16(%r12)
.L1349:
	movq	%rax, 8(%r13)
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1351:
	.cfi_restore_state
	movl	$16, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r13
	jmp	.L1347
	.p2align 4,,10
	.p2align 3
.L1352:
	movq	%r12, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1349
	.cfi_endproc
.LFE18619:
	.size	_ZN2v88internal8compiler4Type5UnionEiPNS0_4ZoneE, .-_ZN2v88internal8compiler4Type5UnionEiPNS0_4ZoneE
	.section	.text._ZNK2v88internal8compiler4Type14AsHeapConstantEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler4Type14AsHeapConstantEv
	.type	_ZNK2v88internal8compiler4Type14AsHeapConstantEv, @function
_ZNK2v88internal8compiler4Type14AsHeapConstantEv:
.LFB18620:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE18620:
	.size	_ZNK2v88internal8compiler4Type14AsHeapConstantEv, .-_ZNK2v88internal8compiler4Type14AsHeapConstantEv
	.section	.text._ZNK2v88internal8compiler4Type21AsOtherNumberConstantEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler4Type21AsOtherNumberConstantEv
	.type	_ZNK2v88internal8compiler4Type21AsOtherNumberConstantEv, @function
_ZNK2v88internal8compiler4Type21AsOtherNumberConstantEv:
.LFB18621:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE18621:
	.size	_ZNK2v88internal8compiler4Type21AsOtherNumberConstantEv, .-_ZNK2v88internal8compiler4Type21AsOtherNumberConstantEv
	.section	.text._ZNK2v88internal8compiler4Type7AsRangeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler4Type7AsRangeEv
	.type	_ZNK2v88internal8compiler4Type7AsRangeEv, @function
_ZNK2v88internal8compiler4Type7AsRangeEv:
.LFB18622:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE18622:
	.size	_ZNK2v88internal8compiler4Type7AsRangeEv, .-_ZNK2v88internal8compiler4Type7AsRangeEv
	.section	.text._ZNK2v88internal8compiler4Type7AsTupleEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler4Type7AsTupleEv
	.type	_ZNK2v88internal8compiler4Type7AsTupleEv, @function
_ZNK2v88internal8compiler4Type7AsTupleEv:
.LFB18623:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE18623:
	.size	_ZNK2v88internal8compiler4Type7AsTupleEv, .-_ZNK2v88internal8compiler4Type7AsTupleEv
	.section	.text._ZNK2v88internal8compiler4Type7AsUnionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler4Type7AsUnionEv
	.type	_ZNK2v88internal8compiler4Type7AsUnionEv, @function
_ZNK2v88internal8compiler4Type7AsUnionEv:
.LFB18624:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE18624:
	.size	_ZNK2v88internal8compiler4Type7AsUnionEv, .-_ZNK2v88internal8compiler4Type7AsUnionEv
	.section	.text._ZN2v88internal8compilerlsERSoNS1_4TypeE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal8compilerlsERSoNS1_4TypeE
	.type	_ZN2v88internal8compilerlsERSoNS1_4TypeE, @function
_ZN2v88internal8compilerlsERSoNS1_4TypeE:
.LFB18625:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-24(%rbp), %rdi
	subq	$24, %rsp
	movq	%rsi, -24(%rbp)
	movq	%r12, %rsi
	call	_ZNK2v88internal8compiler4Type7PrintToERSo
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18625:
	.size	_ZN2v88internal8compilerlsERSoNS1_4TypeE, .-_ZN2v88internal8compilerlsERSoNS1_4TypeE
	.section	.text._ZN2v88internal8compiler10BitsetType3LubINS1_14HeapObjectTypeEEEjRKT_,"axG",@progbits,_ZN2v88internal8compiler10BitsetType3LubINS1_14HeapObjectTypeEEEjRKT_,comdat
	.p2align 4
	.weak	_ZN2v88internal8compiler10BitsetType3LubINS1_14HeapObjectTypeEEEjRKT_
	.type	_ZN2v88internal8compiler10BitsetType3LubINS1_14HeapObjectTypeEEEjRKT_, @function
_ZN2v88internal8compiler10BitsetType3LubINS1_14HeapObjectTypeEEEjRKT_:
.LFB19404:
	.cfi_startproc
	endbr64
	movzwl	(%rdi), %eax
	cmpw	$168, %ax
	ja	.L1388
	leaq	.L1372(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler10BitsetType3LubINS1_14HeapObjectTypeEEEjRKT_,"aG",@progbits,_ZN2v88internal8compiler10BitsetType3LubINS1_14HeapObjectTypeEEEjRKT_,comdat
	.align 4
	.align 4
.L1372:
	.long	.L1378-.L1372
	.long	.L1363-.L1372
	.long	.L1378-.L1372
	.long	.L1363-.L1372
	.long	.L1363-.L1372
	.long	.L1363-.L1372
	.long	.L1363-.L1372
	.long	.L1363-.L1372
	.long	.L1378-.L1372
	.long	.L1363-.L1372
	.long	.L1378-.L1372
	.long	.L1363-.L1372
	.long	.L1363-.L1372
	.long	.L1363-.L1372
	.long	.L1363-.L1372
	.long	.L1363-.L1372
	.long	.L1363-.L1372
	.long	.L1363-.L1372
	.long	.L1378-.L1372
	.long	.L1363-.L1372
	.long	.L1363-.L1372
	.long	.L1363-.L1372
	.long	.L1363-.L1372
	.long	.L1363-.L1372
	.long	.L1363-.L1372
	.long	.L1363-.L1372
	.long	.L1378-.L1372
	.long	.L1363-.L1372
	.long	.L1363-.L1372
	.long	.L1363-.L1372
	.long	.L1363-.L1372
	.long	.L1363-.L1372
	.long	.L1377-.L1372
	.long	.L1377-.L1372
	.long	.L1377-.L1372
	.long	.L1377-.L1372
	.long	.L1363-.L1372
	.long	.L1377-.L1372
	.long	.L1363-.L1372
	.long	.L1363-.L1372
	.long	.L1377-.L1372
	.long	.L1377-.L1372
	.long	.L1377-.L1372
	.long	.L1377-.L1372
	.long	.L1363-.L1372
	.long	.L1377-.L1372
	.long	.L1363-.L1372
	.long	.L1363-.L1372
	.long	.L1363-.L1372
	.long	.L1363-.L1372
	.long	.L1377-.L1372
	.long	.L1363-.L1372
	.long	.L1363-.L1372
	.long	.L1363-.L1372
	.long	.L1363-.L1372
	.long	.L1363-.L1372
	.long	.L1363-.L1372
	.long	.L1363-.L1372
	.long	.L1377-.L1372
	.long	.L1363-.L1372
	.long	.L1363-.L1372
	.long	.L1363-.L1372
	.long	.L1363-.L1372
	.long	.L1363-.L1372
	.long	.L1381-.L1372
	.long	.L1376-.L1372
	.long	.L1375-.L1372
	.long	.L1374-.L1372
	.long	.L1373-.L1372
	.long	.L1373-.L1372
	.long	.L1373-.L1372
	.long	.L1373-.L1372
	.long	.L1373-.L1372
	.long	.L1363-.L1372
	.long	.L1373-.L1372
	.long	.L1373-.L1372
	.long	.L1363-.L1372
	.long	.L1363-.L1372
	.long	.L1373-.L1372
	.long	.L1373-.L1372
	.long	.L1363-.L1372
	.long	.L1363-.L1372
	.long	.L1373-.L1372
	.long	.L1363-.L1372
	.long	.L1363-.L1372
	.long	.L1363-.L1372
	.long	.L1363-.L1372
	.long	.L1363-.L1372
	.long	.L1373-.L1372
	.long	.L1373-.L1372
	.long	.L1363-.L1372
	.long	.L1363-.L1372
	.long	.L1363-.L1372
	.long	.L1363-.L1372
	.long	.L1363-.L1372
	.long	.L1363-.L1372
	.long	.L1373-.L1372
	.long	.L1363-.L1372
	.long	.L1373-.L1372
	.long	.L1363-.L1372
	.long	.L1363-.L1372
	.long	.L1363-.L1372
	.long	.L1363-.L1372
	.long	.L1363-.L1372
	.long	.L1363-.L1372
	.long	.L1363-.L1372
	.long	.L1363-.L1372
	.long	.L1363-.L1372
	.long	.L1363-.L1372
	.long	.L1363-.L1372
	.long	.L1363-.L1372
	.long	.L1363-.L1372
	.long	.L1363-.L1372
	.long	.L1363-.L1372
	.long	.L1363-.L1372
	.long	.L1363-.L1372
	.long	.L1363-.L1372
	.long	.L1363-.L1372
	.long	.L1363-.L1372
	.long	.L1373-.L1372
	.long	.L1373-.L1372
	.long	.L1373-.L1372
	.long	.L1373-.L1372
	.long	.L1373-.L1372
	.long	.L1373-.L1372
	.long	.L1373-.L1372
	.long	.L1373-.L1372
	.long	.L1373-.L1372
	.long	.L1373-.L1372
	.long	.L1373-.L1372
	.long	.L1373-.L1372
	.long	.L1373-.L1372
	.long	.L1373-.L1372
	.long	.L1373-.L1372
	.long	.L1373-.L1372
	.long	.L1373-.L1372
	.long	.L1373-.L1372
	.long	.L1373-.L1372
	.long	.L1373-.L1372
	.long	.L1373-.L1372
	.long	.L1373-.L1372
	.long	.L1373-.L1372
	.long	.L1373-.L1372
	.long	.L1373-.L1372
	.long	.L1373-.L1372
	.long	.L1373-.L1372
	.long	.L1373-.L1372
	.long	.L1373-.L1372
	.long	.L1373-.L1372
	.long	.L1373-.L1372
	.long	.L1363-.L1372
	.long	.L1373-.L1372
	.long	.L1363-.L1372
	.long	.L1373-.L1372
	.long	.L1373-.L1372
	.long	.L1373-.L1372
	.long	.L1363-.L1372
	.long	.L1373-.L1372
	.long	.L1373-.L1372
	.long	.L1373-.L1372
	.long	.L1373-.L1372
	.long	.L1363-.L1372
	.long	.L1363-.L1372
	.long	.L1363-.L1372
	.long	.L1363-.L1372
	.long	.L1373-.L1372
	.long	.L1373-.L1372
	.long	.L1373-.L1372
	.long	.L1367-.L1372
	.section	.text._ZN2v88internal8compiler10BitsetType3LubINS1_14HeapObjectTypeEEEjRKT_,"axG",@progbits,_ZN2v88internal8compiler10BitsetType3LubINS1_14HeapObjectTypeEEEjRKT_,comdat
.L1367:
	movl	$131072, %eax
	ret
.L1381:
	movl	$8192, %eax
.L1360:
	ret
.L1378:
	movl	$16384, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1388:
	subw	$1024, %ax
	cmpw	$81, %ax
	ja	.L1363
	leaq	.L1365(%rip), %rdx
	movzwl	%ax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler10BitsetType3LubINS1_14HeapObjectTypeEEEjRKT_,"aG",@progbits,_ZN2v88internal8compiler10BitsetType3LubINS1_14HeapObjectTypeEEEjRKT_,comdat
	.align 4
	.align 4
.L1365:
	.long	.L1370-.L1365
	.long	.L1368-.L1365
	.long	.L1368-.L1365
	.long	.L1367-.L1365
	.long	.L1363-.L1365
	.long	.L1363-.L1365
	.long	.L1363-.L1365
	.long	.L1363-.L1365
	.long	.L1363-.L1365
	.long	.L1363-.L1365
	.long	.L1363-.L1365
	.long	.L1363-.L1365
	.long	.L1363-.L1365
	.long	.L1363-.L1365
	.long	.L1363-.L1365
	.long	.L1363-.L1365
	.long	.L1368-.L1365
	.long	.L1367-.L1365
	.long	.L1363-.L1365
	.long	.L1363-.L1365
	.long	.L1363-.L1365
	.long	.L1363-.L1365
	.long	.L1363-.L1365
	.long	.L1363-.L1365
	.long	.L1363-.L1365
	.long	.L1363-.L1365
	.long	.L1363-.L1365
	.long	.L1363-.L1365
	.long	.L1363-.L1365
	.long	.L1363-.L1365
	.long	.L1363-.L1365
	.long	.L1363-.L1365
	.long	.L1368-.L1365
	.long	.L1368-.L1365
	.long	.L1368-.L1365
	.long	.L1367-.L1365
	.long	.L1367-.L1365
	.long	.L1369-.L1365
	.long	.L1367-.L1365
	.long	.L1367-.L1365
	.long	.L1367-.L1365
	.long	.L1367-.L1365
	.long	.L1367-.L1365
	.long	.L1368-.L1365
	.long	.L1367-.L1365
	.long	.L1367-.L1365
	.long	.L1367-.L1365
	.long	.L1367-.L1365
	.long	.L1367-.L1365
	.long	.L1367-.L1365
	.long	.L1367-.L1365
	.long	.L1367-.L1365
	.long	.L1367-.L1365
	.long	.L1367-.L1365
	.long	.L1367-.L1365
	.long	.L1367-.L1365
	.long	.L1367-.L1365
	.long	.L1367-.L1365
	.long	.L1367-.L1365
	.long	.L1367-.L1365
	.long	.L1367-.L1365
	.long	.L1367-.L1365
	.long	.L1367-.L1365
	.long	.L1367-.L1365
	.long	.L1367-.L1365
	.long	.L1367-.L1365
	.long	.L1367-.L1365
	.long	.L1367-.L1365
	.long	.L1367-.L1365
	.long	.L1367-.L1365
	.long	.L1367-.L1365
	.long	.L1367-.L1365
	.long	.L1367-.L1365
	.long	.L1367-.L1365
	.long	.L1367-.L1365
	.long	.L1367-.L1365
	.long	.L1367-.L1365
	.long	.L1367-.L1365
	.long	.L1367-.L1365
	.long	.L1367-.L1365
	.long	.L1380-.L1365
	.long	.L1364-.L1365
	.section	.text._ZN2v88internal8compiler10BitsetType3LubINS1_14HeapObjectTypeEEEjRKT_,"axG",@progbits,_ZN2v88internal8compiler10BitsetType3LubINS1_14HeapObjectTypeEEEjRKT_,comdat
.L1380:
	movl	$4194304, %eax
	ret
.L1369:
	movl	$67108864, %eax
	ret
.L1364:
	movl	$2097152, %eax
	ret
.L1377:
	movl	$16416, %eax
	ret
.L1376:
	movl	$7262, %eax
	ret
.L1375:
	movl	$134217728, %eax
	ret
.L1374:
	movzbl	2(%rdi), %eax
	subl	$1, %eax
	cmpb	$5, %al
	ja	.L1363
	leaq	CSWTCH.658(%rip), %rdx
	movzbl	%al, %eax
	movl	(%rdx,%rax,4), %eax
	ret
.L1373:
	movl	$16777216, %eax
	ret
.L1370:
	movl	4(%rdi), %eax
	andl	$2, %eax
	cmpl	$1, %eax
	sbbl	%eax, %eax
	andl	$524288, %eax
	addl	$524288, %eax
	ret
.L1368:
	movl	4(%rdi), %edx
	movl	$262144, %eax
	testb	$1, %dl
	jne	.L1360
	andl	$2, %edx
	cmpl	$1, %edx
	sbbl	%eax, %eax
	andl	$65536, %eax
	addl	$65536, %eax
	ret
.L1363:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC8(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19404:
	.size	_ZN2v88internal8compiler10BitsetType3LubINS1_14HeapObjectTypeEEEjRKT_, .-_ZN2v88internal8compiler10BitsetType3LubINS1_14HeapObjectTypeEEEjRKT_
	.section	.text._ZN2v88internal8compiler4Type11NewConstantEPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler4Type11NewConstantEPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEPNS0_4ZoneE
	.type	_ZN2v88internal8compiler4Type11NewConstantEPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEPNS0_4ZoneE, @function
_ZN2v88internal8compiler4Type11NewConstantEPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEPNS0_4ZoneE:
.LFB18603:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	movq	%rsi, %rdx
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r12
	movq	%rdi, %rsi
	movq	%r12, %rdi
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler9ObjectRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef5IsSmiEv@PLT
	movq	%r12, %rdi
	testb	%al, %al
	je	.L1390
	call	_ZNK2v88internal8compiler9ObjectRef5AsSmiEv@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rdi
	cvtsi2sdl	%eax, %xmm0
	call	_ZN2v88internal8compiler4Type11NewConstantEdPNS0_4ZoneE
.L1391:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1401
	addq	$64, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1390:
	.cfi_restore_state
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapNumberEv@PLT
	movq	%r12, %rdi
	testb	%al, %al
	je	.L1392
	call	_ZNK2v88internal8compiler9ObjectRef12AsHeapNumberEv@PLT
	leaq	-48(%rbp), %rdi
	movq	%rax, -48(%rbp)
	movq	%rdx, -40(%rbp)
	call	_ZNK2v88internal8compiler13HeapNumberRef5valueEv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Type11NewConstantEdPNS0_4ZoneE
	jmp	.L1391
	.p2align 4,,10
	.p2align 3
.L1392:
	call	_ZNK2v88internal8compiler9ObjectRef8IsStringEv@PLT
	testb	%al, %al
	jne	.L1402
.L1393:
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12AsHeapObjectEv@PLT
	leaq	-48(%rbp), %rdi
	movq	%rdx, -40(%rbp)
	movq	%rax, -48(%rbp)
	call	_ZNK2v88internal8compiler13HeapObjectRef17GetHeapObjectTypeEv@PLT
	leaq	-72(%rbp), %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal8compiler10BitsetType3LubINS1_14HeapObjectTypeEEEjRKT_
	movq	24(%r13), %rdx
	movl	%eax, %r12d
	movq	16(%r13), %rax
	subq	%rax, %rdx
	cmpq	$23, %rdx
	jbe	.L1403
	leaq	24(%rax), %rdx
	movq	%rdx, 16(%r13)
.L1395:
	movl	$0, (%rax)
	movl	%r12d, 4(%rax)
	movdqa	-48(%rbp), %xmm1
	movups	%xmm1, 8(%rax)
	jmp	.L1391
	.p2align 4,,10
	.p2align 3
.L1402:
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef20IsInternalizedStringEv@PLT
	testb	%al, %al
	jne	.L1393
	movl	$16417, %eax
	jmp	.L1391
	.p2align 4,,10
	.p2align 3
.L1403:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1395
.L1401:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18603:
	.size	_ZN2v88internal8compiler4Type11NewConstantEPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEPNS0_4ZoneE, .-_ZN2v88internal8compiler4Type11NewConstantEPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEPNS0_4ZoneE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler9RangeType6Limits7IsEmptyEv,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler9RangeType6Limits7IsEmptyEv, @function
_GLOBAL__sub_I__ZN2v88internal8compiler9RangeType6Limits7IsEmptyEv:
.LFB22415:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE22415:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler9RangeType6Limits7IsEmptyEv, .-_GLOBAL__sub_I__ZN2v88internal8compiler9RangeType6Limits7IsEmptyEv
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler9RangeType6Limits7IsEmptyEv
	.section	.rodata.CSWTCH.309,"a"
	.align 16
	.type	CSWTCH.309, @object
	.size	CSWTCH.309, 24
CSWTCH.309:
	.long	512
	.long	256
	.long	128
	.long	8388608
	.long	16777216
	.long	16777216
	.set	CSWTCH.658,CSWTCH.309
	.section	.rodata._ZZN2v88internal8compiler10BitsetType5PrintERSojE13named_bitsets,"a"
	.align 32
	.type	_ZZN2v88internal8compiler10BitsetType5PrintERSojE13named_bitsets, @object
	.size	_ZZN2v88internal8compiler10BitsetType5PrintERSojE13named_bitsets, 344
_ZZN2v88internal8compiler10BitsetType5PrintERSojE13named_bitsets:
	.long	2
	.long	4
	.long	8
	.long	16
	.long	32
	.long	0
	.long	64
	.long	128
	.long	256
	.long	512
	.long	1024
	.long	2048
	.long	4096
	.long	8192
	.long	16384
	.long	65536
	.long	131072
	.long	262144
	.long	524288
	.long	1048576
	.long	2097152
	.long	4194304
	.long	8388608
	.long	16777216
	.long	33554432
	.long	67108864
	.long	134217728
	.long	1088
	.long	1098
	.long	3146
	.long	7242
	.long	72
	.long	1026
	.long	1030
	.long	3078
	.long	7174
	.long	1102
	.long	3150
	.long	7246
	.long	1118
	.long	3166
	.long	6144
	.long	7262
	.long	134224990
	.long	16416
	.long	24576
	.long	24608
	.long	16512
	.long	7774
	.long	7902
	.long	896
	.long	8389504
	.long	7390
	.long	384
	.long	262528
	.long	8395870
	.long	8396766
	.long	134241406
	.long	7518
	.long	8158
	.long	24574
	.long	32766
	.long	134250494
	.long	262400
	.long	1572864
	.long	67239936
	.long	68681728
	.long	6881280
	.long	7143424
	.long	68288512
	.long	68288640
	.long	73596928
	.long	75169792
	.long	75169920
	.long	73859072
	.long	75431936
	.long	75432192
	.long	75432320
	.long	75440128
	.long	75448352
	.long	75457408
	.long	58720256
	.long	209682430
	.long	75464702
	.long	268395424
	.long	-2
	.globl	_ZN2v88internal8compiler10BitsetType15BoundariesArrayE
	.section	.rodata._ZN2v88internal8compiler10BitsetType15BoundariesArrayE,"a"
	.align 32
	.type	_ZN2v88internal8compiler10BitsetType15BoundariesArrayE, @object
	.size	_ZN2v88internal8compiler10BitsetType15BoundariesArrayE, 112
_ZN2v88internal8compiler10BitsetType15BoundariesArrayE:
	.long	16
	.long	1118
	.long	0
	.long	-1048576
	.long	8
	.long	72
	.long	0
	.long	-1042284544
	.long	64
	.long	64
	.long	0
	.long	-1043333120
	.long	1024
	.long	1024
	.long	0
	.long	0
	.long	2
	.long	1026
	.long	0
	.long	1104150528
	.long	4
	.long	1030
	.long	0
	.long	1105199104
	.long	16
	.long	1118
	.long	0
	.long	1106247680
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	-1042284544
	.align 8
.LC1:
	.long	0
	.long	-1043333120
	.align 8
.LC2:
	.long	0
	.long	0
	.align 8
.LC3:
	.long	0
	.long	1104150528
	.align 8
.LC4:
	.long	0
	.long	1105199104
	.align 8
.LC5:
	.long	0
	.long	1106247680
	.align 8
.LC6:
	.long	0
	.long	-1074790400
	.align 8
.LC7:
	.long	0
	.long	1072693248
	.align 8
.LC9:
	.long	4292870144
	.long	1106247679
	.align 8
.LC10:
	.long	0
	.long	-1020264448
	.align 8
.LC11:
	.long	0
	.long	1127219200
	.align 8
.LC12:
	.long	4290772992
	.long	1105199103
	.align 8
.LC13:
	.long	0
	.long	-1048576
	.align 8
.LC14:
	.long	0
	.long	2146435072
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC15:
	.long	0
	.long	1072693248
	.long	0
	.long	0
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
