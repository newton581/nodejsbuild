	.file	"machine-graph.cc"
	.text
	.section	.text._ZN2v88internal8compiler12MachineGraph13Int32ConstantEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi
	.type	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi, @function
_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi:
.LFB10616:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%esi, %r13d
	pushq	%r12
	movl	%r13d, %edx
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	344(%rdi), %rsi
	movq	%rdi, %rbx
	addq	$24, %rdi
	call	_ZN2v88internal8compiler9NodeCacheIiNS_4base4hashIiEESt8equal_toIiEE4FindEPNS0_4ZoneEi@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L5
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	movq	(%rbx), %r14
	movl	%r13d, %esi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder13Int32ConstantEi@PLT
	movq	%r14, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rax, %rsi
	xorl	%edx, %edx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, (%r12)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10616:
	.size	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi, .-_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi
	.section	.text._ZN2v88internal8compiler12MachineGraph13Int64ConstantEl,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler12MachineGraph13Int64ConstantEl
	.type	_ZN2v88internal8compiler12MachineGraph13Int64ConstantEl, @function
_ZN2v88internal8compiler12MachineGraph13Int64ConstantEl:
.LFB10617:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	movq	%r13, %rdx
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	344(%rdi), %rsi
	movq	%rdi, %rbx
	addq	$56, %rdi
	call	_ZN2v88internal8compiler9NodeCacheIlNS_4base4hashIlEESt8equal_toIlEE4FindEPNS0_4ZoneEl@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L9
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	movq	(%rbx), %r14
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder13Int64ConstantEl@PLT
	movq	%r14, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rax, %rsi
	xorl	%edx, %edx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, (%r12)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10617:
	.size	_ZN2v88internal8compiler12MachineGraph13Int64ConstantEl, .-_ZN2v88internal8compiler12MachineGraph13Int64ConstantEl
	.section	.text._ZN2v88internal8compiler12MachineGraph14IntPtrConstantEl,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler12MachineGraph14IntPtrConstantEl
	.type	_ZN2v88internal8compiler12MachineGraph14IntPtrConstantEl, @function
_ZN2v88internal8compiler12MachineGraph14IntPtrConstantEl:
.LFB10618:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	16(%rdi), %rax
	movq	%rdi, %rbx
	movq	344(%rdi), %rsi
	cmpb	$4, 16(%rax)
	je	.L17
	leaq	56(%rdi), %rdi
	movq	%r12, %rdx
	call	_ZN2v88internal8compiler9NodeCacheIlNS_4base4hashIlEESt8equal_toIlEE4FindEPNS0_4ZoneEl@PLT
	movq	%rax, %r13
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L18
.L10:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	leaq	24(%rdi), %rdi
	movl	%r12d, %edx
	call	_ZN2v88internal8compiler9NodeCacheIiNS_4base4hashIiEESt8equal_toIiEE4FindEPNS0_4ZoneEi@PLT
	movq	%rax, %r13
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L10
	movq	8(%rbx), %rdi
	movl	%r12d, %esi
	movq	(%rbx), %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder13Int32ConstantEi@PLT
.L16:
	movq	%r14, %rdi
	movq	%rax, %rsi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 0(%r13)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	movq	%r12, %rsi
	movq	(%rbx), %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder13Int64ConstantEl@PLT
	jmp	.L16
	.cfi_endproc
.LFE10618:
	.size	_ZN2v88internal8compiler12MachineGraph14IntPtrConstantEl, .-_ZN2v88internal8compiler12MachineGraph14IntPtrConstantEl
	.section	.text._ZN2v88internal8compiler12MachineGraph24RelocatableInt32ConstantEiNS0_9RelocInfo4ModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler12MachineGraph24RelocatableInt32ConstantEiNS0_9RelocInfo4ModeE
	.type	_ZN2v88internal8compiler12MachineGraph24RelocatableInt32ConstantEiNS0_9RelocInfo4ModeE, @function
_ZN2v88internal8compiler12MachineGraph24RelocatableInt32ConstantEiNS0_9RelocInfo4ModeE:
.LFB10619:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%edx, %r12d
	movzbl	%dl, %edx
	pushq	%rbx
	movq	%rdx, %rax
	movl	%esi, %edx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	salq	$32, %rax
	addq	$280, %rdi
	orq	%rax, %rdx
	subq	$8, %rsp
	movq	64(%rdi), %rsi
	call	_ZN2v88internal8compiler9NodeCacheISt4pairIicENS_4base4hashIS4_EESt8equal_toIS4_EE4FindEPNS0_4ZoneES4_@PLT
	movq	%rax, %r13
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L22
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	movq	(%rbx), %r15
	movsbl	%r12b, %edx
	movl	%r14d, %esi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder24RelocatableInt32ConstantEiNS0_9RelocInfo4ModeE@PLT
	movq	%r15, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rax, %rsi
	xorl	%edx, %edx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 0(%r13)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10619:
	.size	_ZN2v88internal8compiler12MachineGraph24RelocatableInt32ConstantEiNS0_9RelocInfo4ModeE, .-_ZN2v88internal8compiler12MachineGraph24RelocatableInt32ConstantEiNS0_9RelocInfo4ModeE
	.section	.text._ZN2v88internal8compiler12MachineGraph24RelocatableInt64ConstantElNS0_9RelocInfo4ModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler12MachineGraph24RelocatableInt64ConstantElNS0_9RelocInfo4ModeE
	.type	_ZN2v88internal8compiler12MachineGraph24RelocatableInt64ConstantElNS0_9RelocInfo4ModeE, @function
_ZN2v88internal8compiler12MachineGraph24RelocatableInt64ConstantElNS0_9RelocInfo4ModeE:
.LFB10620:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	movb	%dl, %cl
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edx, %r12d
	movq	%r13, %rdx
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$312, %rdi
	subq	$8, %rsp
	movq	32(%rdi), %rsi
	call	_ZN2v88internal8compiler9NodeCacheISt4pairIlcENS_4base4hashIS4_EESt8equal_toIS4_EE4FindEPNS0_4ZoneES4_@PLT
	movq	%rax, %r14
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L26
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	movq	(%rbx), %r15
	movsbl	%r12b, %edx
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder24RelocatableInt64ConstantElNS0_9RelocInfo4ModeE@PLT
	movq	%r15, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rax, %rsi
	xorl	%edx, %edx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, (%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10620:
	.size	_ZN2v88internal8compiler12MachineGraph24RelocatableInt64ConstantElNS0_9RelocInfo4ModeE, .-_ZN2v88internal8compiler12MachineGraph24RelocatableInt64ConstantElNS0_9RelocInfo4ModeE
	.section	.text._ZN2v88internal8compiler12MachineGraph25RelocatableIntPtrConstantElNS0_9RelocInfo4ModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler12MachineGraph25RelocatableIntPtrConstantElNS0_9RelocInfo4ModeE
	.type	_ZN2v88internal8compiler12MachineGraph25RelocatableIntPtrConstantElNS0_9RelocInfo4ModeE, @function
_ZN2v88internal8compiler12MachineGraph25RelocatableIntPtrConstantElNS0_9RelocInfo4ModeE:
.LFB10621:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	movb	%dl, %cl
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edx, %r12d
	movq	%r13, %rdx
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$312, %rdi
	subq	$8, %rsp
	movq	32(%rdi), %rsi
	call	_ZN2v88internal8compiler9NodeCacheISt4pairIlcENS_4base4hashIS4_EESt8equal_toIS4_EE4FindEPNS0_4ZoneES4_@PLT
	movq	%rax, %r14
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L30
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	movq	(%rbx), %r15
	movsbl	%r12b, %edx
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder24RelocatableInt64ConstantElNS0_9RelocInfo4ModeE@PLT
	movq	%r15, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rax, %rsi
	xorl	%edx, %edx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, (%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10621:
	.size	_ZN2v88internal8compiler12MachineGraph25RelocatableIntPtrConstantElNS0_9RelocInfo4ModeE, .-_ZN2v88internal8compiler12MachineGraph25RelocatableIntPtrConstantElNS0_9RelocInfo4ModeE
	.section	.text._ZN2v88internal8compiler12MachineGraph15Float32ConstantEf,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler12MachineGraph15Float32ConstantEf
	.type	_ZN2v88internal8compiler12MachineGraph15Float32ConstantEf, @function
_ZN2v88internal8compiler12MachineGraph15Float32ConstantEf:
.LFB10622:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movd	%xmm0, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movd	%xmm0, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	344(%rdi), %rsi
	movq	%rdi, %rbx
	addq	$88, %rdi
	call	_ZN2v88internal8compiler9NodeCacheIiNS_4base4hashIiEESt8equal_toIiEE4FindEPNS0_4ZoneEi@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L34
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	movq	(%rbx), %r14
	movd	%r13d, %xmm0
	call	_ZN2v88internal8compiler21CommonOperatorBuilder15Float32ConstantEf@PLT
	movq	%r14, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rax, %rsi
	xorl	%edx, %edx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, (%r12)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10622:
	.size	_ZN2v88internal8compiler12MachineGraph15Float32ConstantEf, .-_ZN2v88internal8compiler12MachineGraph15Float32ConstantEf
	.section	.text._ZN2v88internal8compiler12MachineGraph15Float64ConstantEd,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler12MachineGraph15Float64ConstantEd
	.type	_ZN2v88internal8compiler12MachineGraph15Float64ConstantEd, @function
_ZN2v88internal8compiler12MachineGraph15Float64ConstantEd:
.LFB10623:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%xmm0, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%xmm0, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	344(%rdi), %rsi
	movq	%rdi, %rbx
	addq	$120, %rdi
	call	_ZN2v88internal8compiler9NodeCacheIlNS_4base4hashIlEESt8equal_toIlEE4FindEPNS0_4ZoneEl@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L38
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	movq	(%rbx), %r14
	movq	%r13, %xmm0
	call	_ZN2v88internal8compiler21CommonOperatorBuilder15Float64ConstantEd@PLT
	movq	%r14, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rax, %rsi
	xorl	%edx, %edx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, (%r12)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10623:
	.size	_ZN2v88internal8compiler12MachineGraph15Float64ConstantEd, .-_ZN2v88internal8compiler12MachineGraph15Float64ConstantEd
	.section	.text._ZN2v88internal8compiler12MachineGraph15PointerConstantEl,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler12MachineGraph15PointerConstantEl
	.type	_ZN2v88internal8compiler12MachineGraph15PointerConstantEl, @function
_ZN2v88internal8compiler12MachineGraph15PointerConstantEl:
.LFB10624:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	movq	%r13, %rdx
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	344(%rdi), %rsi
	movq	%rdi, %rbx
	addq	$184, %rdi
	call	_ZN2v88internal8compiler9NodeCacheIlNS_4base4hashIlEESt8equal_toIlEE4FindEPNS0_4ZoneEl@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L42
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	movq	(%rbx), %r14
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder15PointerConstantEl@PLT
	movq	%r14, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rax, %rsi
	xorl	%edx, %edx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, (%r12)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10624:
	.size	_ZN2v88internal8compiler12MachineGraph15PointerConstantEl, .-_ZN2v88internal8compiler12MachineGraph15PointerConstantEl
	.section	.text._ZN2v88internal8compiler12MachineGraph16ExternalConstantENS0_17ExternalReferenceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler12MachineGraph16ExternalConstantENS0_17ExternalReferenceE
	.type	_ZN2v88internal8compiler12MachineGraph16ExternalConstantENS0_17ExternalReferenceE, @function
_ZN2v88internal8compiler12MachineGraph16ExternalConstantENS0_17ExternalReferenceE:
.LFB10625:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	addq	$24, %rdi
	subq	$24, %rsp
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal8compiler15CommonNodeCache20FindExternalConstantENS0_17ExternalReferenceE@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L46
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	movq	(%rbx), %r13
	leaq	-40(%rbp), %rsi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder16ExternalConstantERKNS0_17ExternalReferenceE@PLT
	movq	%r13, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rax, %rsi
	xorl	%edx, %edx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, (%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10625:
	.size	_ZN2v88internal8compiler12MachineGraph16ExternalConstantENS0_17ExternalReferenceE, .-_ZN2v88internal8compiler12MachineGraph16ExternalConstantENS0_17ExternalReferenceE
	.section	.text._ZN2v88internal8compiler12MachineGraph16ExternalConstantENS0_7Runtime10FunctionIdE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler12MachineGraph16ExternalConstantENS0_7Runtime10FunctionIdE
	.type	_ZN2v88internal8compiler12MachineGraph16ExternalConstantENS0_7Runtime10FunctionIdE, @function
_ZN2v88internal8compiler12MachineGraph16ExternalConstantENS0_7Runtime10FunctionIdE:
.LFB10626:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movl	%esi, %edi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	leaq	24(%rbx), %rdi
	movq	%rax, %rsi
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler15CommonNodeCache20FindExternalConstantENS0_17ExternalReferenceE@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L51
.L47:
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L52
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L51:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	movq	(%rbx), %r13
	leaq	-48(%rbp), %rsi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder16ExternalConstantERKNS0_17ExternalReferenceE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, (%r12)
	jmp	.L47
.L52:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10626:
	.size	_ZN2v88internal8compiler12MachineGraph16ExternalConstantENS0_7Runtime10FunctionIdE, .-_ZN2v88internal8compiler12MachineGraph16ExternalConstantENS0_7Runtime10FunctionIdE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler12MachineGraph13Int32ConstantEi,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler12MachineGraph13Int32ConstantEi, @function
_GLOBAL__sub_I__ZN2v88internal8compiler12MachineGraph13Int32ConstantEi:
.LFB12267:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE12267:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler12MachineGraph13Int32ConstantEi, .-_GLOBAL__sub_I__ZN2v88internal8compiler12MachineGraph13Int32ConstantEi
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler12MachineGraph13Int32ConstantEi
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
