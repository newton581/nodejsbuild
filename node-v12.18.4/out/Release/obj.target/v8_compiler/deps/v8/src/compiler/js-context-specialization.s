	.file	"js-context-specialization.cc"
	.text
	.section	.text._ZNKSt5ctypeIcE8do_widenEc,"axG",@progbits,_ZNKSt5ctypeIcE8do_widenEc,comdat
	.align 2
	.p2align 4
	.weak	_ZNKSt5ctypeIcE8do_widenEc
	.type	_ZNKSt5ctypeIcE8do_widenEc, @function
_ZNKSt5ctypeIcE8do_widenEc:
.LFB1527:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	ret
	.cfi_endproc
.LFE1527:
	.size	_ZNKSt5ctypeIcE8do_widenEc, .-_ZNKSt5ctypeIcE8do_widenEc
	.section	.rodata._ZNK2v88internal8compiler23JSContextSpecialization12reducer_nameEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"JSContextSpecialization"
	.section	.text._ZNK2v88internal8compiler23JSContextSpecialization12reducer_nameEv,"axG",@progbits,_ZNK2v88internal8compiler23JSContextSpecialization12reducer_nameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal8compiler23JSContextSpecialization12reducer_nameEv
	.type	_ZNK2v88internal8compiler23JSContextSpecialization12reducer_nameEv, @function
_ZNK2v88internal8compiler23JSContextSpecialization12reducer_nameEv:
.LFB10173:
	.cfi_startproc
	endbr64
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE10173:
	.size	_ZNK2v88internal8compiler23JSContextSpecialization12reducer_nameEv, .-_ZNK2v88internal8compiler23JSContextSpecialization12reducer_nameEv
	.section	.text._ZN2v88internal8compiler23JSContextSpecializationD2Ev,"axG",@progbits,_ZN2v88internal8compiler23JSContextSpecializationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler23JSContextSpecializationD2Ev
	.type	_ZN2v88internal8compiler23JSContextSpecializationD2Ev, @function
_ZN2v88internal8compiler23JSContextSpecializationD2Ev:
.LFB26734:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE26734:
	.size	_ZN2v88internal8compiler23JSContextSpecializationD2Ev, .-_ZN2v88internal8compiler23JSContextSpecializationD2Ev
	.weak	_ZN2v88internal8compiler23JSContextSpecializationD1Ev
	.set	_ZN2v88internal8compiler23JSContextSpecializationD1Ev,_ZN2v88internal8compiler23JSContextSpecializationD2Ev
	.section	.text._ZN2v88internal8compiler23JSContextSpecializationD0Ev,"axG",@progbits,_ZN2v88internal8compiler23JSContextSpecializationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler23JSContextSpecializationD0Ev
	.type	_ZN2v88internal8compiler23JSContextSpecializationD0Ev, @function
_ZN2v88internal8compiler23JSContextSpecializationD0Ev:
.LFB26736:
	.cfi_startproc
	endbr64
	movl	$64, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE26736:
	.size	_ZN2v88internal8compiler23JSContextSpecializationD0Ev, .-_ZN2v88internal8compiler23JSContextSpecializationD0Ev
	.section	.rodata._ZN2v88internal8compiler10ContextRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb.str1.1,"aMS",@progbits,1
.LC1:
	.string	"IsContext()"
.LC2:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal8compiler10ContextRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,"axG",@progbits,_ZN2v88internal8compiler10ContextRefC5EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler10ContextRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.type	_ZN2v88internal8compiler10ContextRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, @function
_ZN2v88internal8compiler10ContextRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb:
.LFB9642:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%ecx, %ebx
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	testb	%bl, %bl
	jne	.L9
.L6:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef9IsContextEv@PLT
	testb	%al, %al
	jne	.L6
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE9642:
	.size	_ZN2v88internal8compiler10ContextRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, .-_ZN2v88internal8compiler10ContextRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.weak	_ZN2v88internal8compiler10ContextRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.set	_ZN2v88internal8compiler10ContextRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,_ZN2v88internal8compiler10ContextRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.section	.rodata._ZN2v88internal8compiler12_GLOBAL__N_124GetSpecializationContextEPNS1_12JSHeapBrokerEPNS1_4NodeEPmNS_5MaybeINS1_12OuterContextEEE.isra.0.str1.1,"aMS",@progbits,1
.LC3:
	.string	"IsHeapObject()"
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_124GetSpecializationContextEPNS1_12JSHeapBrokerEPNS1_4NodeEPmNS_5MaybeINS1_12OuterContextEEE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_124GetSpecializationContextEPNS1_12JSHeapBrokerEPNS1_4NodeEPmNS_5MaybeINS1_12OuterContextEEE.isra.0, @function
_ZN2v88internal8compiler12_GLOBAL__N_124GetSpecializationContextEPNS1_12JSHeapBrokerEPNS1_4NodeEPmNS_5MaybeINS1_12OuterContextEEE.isra.0:
.LFB27576:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$64, %rsp
	movq	(%rdx), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzwl	16(%rdi), %eax
	cmpw	$30, %ax
	je	.L11
	cmpw	$50, %ax
	jne	.L13
	testb	%r8b, %r8b
	je	.L13
	xorl	%esi, %esi
	movq	%rdx, %rdi
	movq	%r9, -88(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	-72(%rbp), %rdx
	movq	%rax, %r15
	movq	(%rdx), %rdi
	call	_ZN2v88internal8compiler16ParameterIndexOfEPKNS1_8OperatorE@PLT
	movq	-80(%rbp), %rcx
	movq	-88(%rbp), %r9
	movl	%eax, %r8d
	movq	(%r15), %rax
	movl	32(%rax), %eax
	subl	$2, %eax
	cmpl	%eax, %r8d
	jne	.L13
	movq	(%rcx), %rax
	cmpq	16(%rbp), %rax
	jnb	.L25
	.p2align 4,,10
	.p2align 3
.L13:
	movb	$0, (%r12)
	movb	$0, 8(%r12)
.L10:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L26
	addq	$64, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore_state
	call	_ZN2v88internal8compiler14HeapConstantOfEPKNS1_8OperatorE@PLT
	leaq	-64(%rbp), %r14
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L27
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef9IsContextEv@PLT
	testb	%al, %al
	je	.L13
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef9AsContextEv@PLT
	movb	$1, (%r12)
	movq	%rax, 8(%r12)
	movq	%rdx, 16(%r12)
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L25:
	subq	16(%rbp), %rax
	leaq	-64(%rbp), %rdi
	movq	%r9, %rdx
	movq	%r13, %rsi
	movq	%rax, (%rcx)
	movl	$1, %ecx
	call	_ZN2v88internal8compiler10ContextRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movdqa	-64(%rbp), %xmm0
	movb	$1, (%r12)
	movups	%xmm0, 8(%r12)
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L27:
	leaq	.LC3(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L26:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27576:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_124GetSpecializationContextEPNS1_12JSHeapBrokerEPNS1_4NodeEPmNS_5MaybeINS1_12OuterContextEEE.isra.0, .-_ZN2v88internal8compiler12_GLOBAL__N_124GetSpecializationContextEPNS1_12JSHeapBrokerEPNS1_4NodeEPmNS_5MaybeINS1_12OuterContextEEE.isra.0
	.section	.text._ZN2v88internal8compiler23JSContextSpecialization15ReduceParameterEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23JSContextSpecialization15ReduceParameterEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23JSContextSpecialization15ReduceParameterEPNS1_4NodeE, @function
_ZN2v88internal8compiler23JSContextSpecialization15ReduceParameterEPNS1_4NodeE:
.LFB22757:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rsi), %rdi
	call	_ZN2v88internal8compiler16ParameterIndexOfEPKNS1_8OperatorE@PLT
	cmpl	$-1, %eax
	je	.L35
.L29:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore_state
	movq	48(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L29
	movq	16(%rbx), %rdi
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22757:
	.size	_ZN2v88internal8compiler23JSContextSpecialization15ReduceParameterEPNS1_4NodeE, .-_ZN2v88internal8compiler23JSContextSpecialization15ReduceParameterEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23JSContextSpecialization21SimplifyJSLoadContextEPNS1_4NodeES4_m,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23JSContextSpecialization21SimplifyJSLoadContextEPNS1_4NodeES4_m
	.type	_ZN2v88internal8compiler23JSContextSpecialization21SimplifyJSLoadContextEPNS1_4NodeES4_m, @function
_ZN2v88internal8compiler23JSContextSpecialization21SimplifyJSLoadContextEPNS1_4NodeES4_m:
.LFB22758:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	(%rsi), %rdi
	call	_ZN2v88internal8compiler15ContextAccessOfEPKNS1_8OperatorE@PLT
	movq	%rax, %r12
	movzwl	2(%rax), %eax
	cmpq	%rax, %r15
	je	.L40
.L37:
	movq	16(%r14), %rax
	movzbl	(%r12), %ecx
	movq	%r15, %rsi
	movl	4(%r12), %edx
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder11LoadContextEmmb@PLT
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler14NodeProperties19ReplaceContextInputEPNS1_4NodeES4_@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties15GetContextInputEPNS1_4NodeE@PLT
	cmpq	%rax, %r13
	jne	.L37
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22758:
	.size	_ZN2v88internal8compiler23JSContextSpecialization21SimplifyJSLoadContextEPNS1_4NodeES4_m, .-_ZN2v88internal8compiler23JSContextSpecialization21SimplifyJSLoadContextEPNS1_4NodeES4_m
	.section	.text._ZN2v88internal8compiler23JSContextSpecialization22SimplifyJSStoreContextEPNS1_4NodeES4_m,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23JSContextSpecialization22SimplifyJSStoreContextEPNS1_4NodeES4_m
	.type	_ZN2v88internal8compiler23JSContextSpecialization22SimplifyJSStoreContextEPNS1_4NodeES4_m, @function
_ZN2v88internal8compiler23JSContextSpecialization22SimplifyJSStoreContextEPNS1_4NodeES4_m:
.LFB22759:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	(%rsi), %rdi
	call	_ZN2v88internal8compiler15ContextAccessOfEPKNS1_8OperatorE@PLT
	movq	%rax, %r15
	movzwl	2(%rax), %eax
	cmpq	%rax, %r12
	je	.L45
.L42:
	movq	16(%r14), %rax
	movl	4(%r15), %edx
	movq	%r12, %rsi
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder12StoreContextEmm@PLT
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler14NodeProperties19ReplaceContextInputEPNS1_4NodeES4_@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties15GetContextInputEPNS1_4NodeE@PLT
	cmpq	%rax, %r13
	jne	.L42
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22759:
	.size	_ZN2v88internal8compiler23JSContextSpecialization22SimplifyJSStoreContextEPNS1_4NodeES4_m, .-_ZN2v88internal8compiler23JSContextSpecialization22SimplifyJSStoreContextEPNS1_4NodeES4_m
	.section	.rodata._ZN2v88internal8compiler23JSContextSpecialization19ReduceJSLoadContextEPNS1_4NodeE.str1.1,"aMS",@progbits,1
.LC4:
	.string	"Missing "
.LC5:
	.string	"previous value for context "
.LC6:
	.string	" ("
	.section	.rodata._ZN2v88internal8compiler23JSContextSpecialization19ReduceJSLoadContextEPNS1_4NodeE.str1.8,"aMS",@progbits,1
	.align 8
.LC7:
	.string	"../deps/v8/src/compiler/js-context-specialization.cc"
	.section	.rodata._ZN2v88internal8compiler23JSContextSpecialization19ReduceJSLoadContextEPNS1_4NodeE.str1.1
.LC8:
	.string	":"
.LC9:
	.string	")"
.LC10:
	.string	"slot value "
.LC11:
	.string	" for context "
	.section	.text._ZN2v88internal8compiler23JSContextSpecialization19ReduceJSLoadContextEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23JSContextSpecialization19ReduceJSLoadContextEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23JSContextSpecialization19ReduceJSLoadContextEPNS1_4NodeE, @function
_ZN2v88internal8compiler23JSContextSpecialization19ReduceJSLoadContextEPNS1_4NodeE:
.LFB22777:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-184(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$168, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler15ContextAccessOfEPKNS1_8OperatorE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %r13
	movzwl	2(%rax), %eax
	movq	%rax, -184(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetOuterContextEPNS1_4NodeEPm@PLT
	subq	$8, %rsp
	pushq	40(%rbx)
	movq	56(%rbx), %rsi
	movq	32(%rbx), %r9
	movq	%rax, %rdx
	movq	%r15, %rcx
	movq	%rax, %r14
	movzbl	24(%rbx), %r8d
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_124GetSpecializationContextEPNS1_12JSHeapBrokerEPNS1_4NodeEPmNS_5MaybeINS1_12OuterContextEEE.isra.0
	cmpb	$0, -144(%rbp)
	popq	%rax
	popq	%rdx
	je	.L96
	movdqu	-136(%rbp), %xmm1
	leaq	-176(%rbp), %r14
	movq	%r15, %rsi
	xorl	%edx, %edx
	movq	%r14, %rdi
	movaps	%xmm1, -176(%rbp)
	call	_ZNK2v88internal8compiler10ContextRef8previousEPmNS1_19SerializationPolicyE@PLT
	movq	-184(%rbp), %r15
	movq	%rax, -176(%rbp)
	movq	%rdx, -168(%rbp)
	testq	%r15, %r15
	je	.L52
	movq	56(%rbx), %rdi
	cmpb	$0, 124(%rdi)
	jne	.L97
.L53:
	movq	16(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	(%r12), %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler15ContextAccessOfEPKNS1_8OperatorE@PLT
	movq	%rax, %r13
	movzwl	2(%rax), %eax
	cmpq	%r15, %rax
	je	.L75
.L77:
	movq	16(%rbx), %rax
	movzbl	0(%r13), %ecx
	movq	%r15, %rsi
	movl	4(%r13), %edx
	movq	368(%rax), %rdi
.L91:
	call	_ZN2v88internal8compiler17JSOperatorBuilder11LoadContextEmmb@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14NodeProperties19ReplaceContextInputEPNS1_4NodeES4_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
.L76:
	movq	%r12, %rax
.L51:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L98
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	.cfi_restore_state
	cmpb	$0, 0(%r13)
	jne	.L60
	movq	16(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	(%r12), %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler15ContextAccessOfEPKNS1_8OperatorE@PLT
	cmpw	$0, 2(%rax)
	movq	%rax, %r13
	je	.L61
.L63:
	movq	16(%rbx), %rax
	movzbl	0(%r13), %ecx
	xorl	%esi, %esi
	movl	4(%r13), %edx
	movq	368(%rax), %rdi
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L96:
	movq	-184(%rbp), %r15
.L95:
	movq	(%r12), %rdi
	call	_ZN2v88internal8compiler15ContextAccessOfEPKNS1_8OperatorE@PLT
	movq	%rax, %r13
	movzwl	2(%rax), %eax
	cmpq	%rax, %r15
	jne	.L77
.L75:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties15GetContextInputEPNS1_4NodeE@PLT
	cmpq	%rax, %r14
	jne	.L77
.L90:
	xorl	%r12d, %r12d
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L60:
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %r15
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	$0, -96(%rbp)
	movq	%r15, %rdi
	movaps	%xmm0, -112(%rbp)
	movl	4(%r13), %edx
	call	_ZNK2v88internal8compiler10ContextRef3getEiNS1_19SerializationPolicyE@PLT
	cmpb	$0, -80(%rbp)
	je	.L89
	cmpb	$0, -112(%rbp)
	jne	.L99
	movdqu	-72(%rbp), %xmm3
	movb	$1, -112(%rbp)
	movups	%xmm3, -104(%rbp)
.L66:
	leaq	-104(%rbp), %rsi
	movq	%rsi, %rdi
	movq	%rsi, -200(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef5IsSmiEv@PLT
	movq	-200(%rbp), %rsi
	testb	%al, %al
	je	.L100
.L69:
	cmpb	$0, -112(%rbp)
	je	.L70
	movq	16(%rbx), %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	8(%rbx), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rax, %r13
	movq	%r12, %rsi
	movq	(%rdi), %rax
	movq	%r13, %rdx
	call	*32(%rax)
	movq	%r13, %rax
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L97:
	call	_ZN2v88internal8compiler12JSHeapBroker5TraceEv@PLT
	movl	$8, %edx
	leaq	.LC4(%rip), %rsi
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$27, %edx
	movq	%r13, %rdi
	leaq	.LC5(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r13, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compilerlsERSoRKNS1_9ObjectRefE@PLT
	movl	$2, %edx
	leaq	.LC6(%rip), %rsi
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r13, %rdi
	movl	$52, %edx
	leaq	.LC7(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC8(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r13, %rdi
	movl	$150, %esi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC9(%rip), %rsi
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	0(%r13), %rax
	movq	-24(%rax), %rax
	movq	240(%r13,%rax), %r15
	testq	%r15, %r15
	je	.L72
	cmpb	$0, 56(%r15)
	je	.L55
	movsbl	67(%r15), %esi
.L56:
	movq	%r13, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movq	-184(%rbp), %r15
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L100:
	movq	%rsi, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12AsHeapObjectEv@PLT
	leaq	-160(%rbp), %rdi
	movq	%rax, -160(%rbp)
	movq	%rdx, -152(%rbp)
	call	_ZNK2v88internal8compiler13HeapObjectRef3mapEv@PLT
	movq	%r15, %rdi
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZNK2v88internal8compiler6MapRef12oddball_typeEv@PLT
	movq	-200(%rbp), %rsi
	subl	$2, %eax
	testb	$-3, %al
	jne	.L69
	.p2align 4,,10
	.p2align 3
.L89:
	cmpb	$0, -112(%rbp)
	je	.L70
	movb	$0, -112(%rbp)
.L70:
	movq	56(%rbx), %rdi
	cmpb	$0, 124(%rdi)
	jne	.L101
.L79:
	movq	16(%rbx), %rdi
	movq	%r14, %rsi
	movq	-184(%rbp), %r15
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	%rax, %r14
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L61:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties15GetContextInputEPNS1_4NodeE@PLT
	cmpq	%rax, %r14
	jne	.L63
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L99:
	movdqu	-72(%rbp), %xmm2
	movups	%xmm2, -104(%rbp)
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L55:
	movq	%r15, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%r15), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L56
	movq	%r15, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L101:
	call	_ZN2v88internal8compiler12JSHeapBroker5TraceEv@PLT
	movl	$8, %edx
	leaq	.LC4(%rip), %rsi
	movq	%rax, %r15
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$11, %edx
	movq	%r15, %rdi
	leaq	.LC10(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	4(%r13), %esi
	movq	%r15, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$13, %edx
	leaq	.LC11(%rip), %rsi
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r13, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compilerlsERSoRKNS1_9ObjectRefE@PLT
	movl	$2, %edx
	leaq	.LC6(%rip), %rsi
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r13, %rdi
	movl	$52, %edx
	leaq	.LC7(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC8(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r13, %rdi
	movl	$177, %esi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC9(%rip), %rsi
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	0(%r13), %rax
	movq	-24(%rax), %rax
	movq	240(%r13,%rax), %r15
	testq	%r15, %r15
	je	.L72
	cmpb	$0, 56(%r15)
	je	.L73
	movsbl	67(%r15), %esi
.L74:
	movq	%r13, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L73:
	movq	%r15, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%r15), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L74
	movq	%r15, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L74
.L98:
	call	__stack_chk_fail@PLT
.L72:
	call	_ZSt16__throw_bad_castv@PLT
	.cfi_endproc
.LFE22777:
	.size	_ZN2v88internal8compiler23JSContextSpecialization19ReduceJSLoadContextEPNS1_4NodeE, .-_ZN2v88internal8compiler23JSContextSpecialization19ReduceJSLoadContextEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23JSContextSpecialization20ReduceJSStoreContextEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23JSContextSpecialization20ReduceJSStoreContextEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23JSContextSpecialization20ReduceJSStoreContextEPNS1_4NodeE, @function
_ZN2v88internal8compiler23JSContextSpecialization20ReduceJSStoreContextEPNS1_4NodeE:
.LFB22793:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-104(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler15ContextAccessOfEPKNS1_8OperatorE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movzwl	2(%rax), %eax
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetOuterContextEPNS1_4NodeEPm@PLT
	subq	$8, %rsp
	pushq	40(%rbx)
	movq	56(%rbx), %rsi
	movq	32(%rbx), %r9
	movq	%rax, %rdx
	leaq	-80(%rbp), %rdi
	movq	%r14, %rcx
	movzbl	24(%rbx), %r8d
	movq	%rax, %r13
	call	_ZN2v88internal8compiler12_GLOBAL__N_124GetSpecializationContextEPNS1_12JSHeapBrokerEPNS1_4NodeEPmNS_5MaybeINS1_12OuterContextEEE.isra.0
	cmpb	$0, -80(%rbp)
	popq	%rax
	popq	%rdx
	je	.L124
	movdqu	-72(%rbp), %xmm0
	leaq	-96(%rbp), %r15
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	movaps	%xmm0, -96(%rbp)
	call	_ZNK2v88internal8compiler10ContextRef8previousEPmNS1_19SerializationPolicyE@PLT
	movq	-104(%rbp), %r13
	movq	%rax, -96(%rbp)
	movq	%rdx, -88(%rbp)
	testq	%r13, %r13
	je	.L108
	movq	56(%rbx), %rdi
	cmpb	$0, 124(%rdi)
	jne	.L125
.L109:
	movq	16(%rbx), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	(%r12), %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler15ContextAccessOfEPKNS1_8OperatorE@PLT
	movq	%rax, %r14
	movzwl	2(%rax), %eax
	cmpq	%rax, %r13
	je	.L113
.L115:
	movq	16(%rbx), %rax
	movl	4(%r14), %edx
	movq	%r13, %rsi
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder12StoreContextEmm@PLT
	movq	%r15, %rsi
	movq	%rax, %r13
.L122:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties19ReplaceContextInputEPNS1_4NodeES4_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
.L107:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L126
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L124:
	.cfi_restore_state
	movq	(%r12), %rdi
	movq	-104(%rbp), %r15
	call	_ZN2v88internal8compiler15ContextAccessOfEPKNS1_8OperatorE@PLT
	movq	%rax, %r14
	movzwl	2(%rax), %eax
	cmpq	%rax, %r15
	je	.L104
.L106:
	movq	16(%rbx), %rax
	movl	4(%r14), %edx
	movq	%r15, %rsi
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder12StoreContextEmm@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14NodeProperties19ReplaceContextInputEPNS1_4NodeES4_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L108:
	movq	16(%rbx), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	(%r12), %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler15ContextAccessOfEPKNS1_8OperatorE@PLT
	cmpw	$0, 2(%rax)
	movq	%rax, %r13
	je	.L116
.L118:
	movq	16(%rbx), %rax
	movl	4(%r13), %edx
	xorl	%esi, %esi
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder12StoreContextEmm@PLT
	movq	%r14, %rsi
	movq	%rax, %r13
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L113:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties15GetContextInputEPNS1_4NodeE@PLT
	cmpq	%rax, %r15
	jne	.L115
.L123:
	xorl	%r12d, %r12d
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L125:
	call	_ZN2v88internal8compiler12JSHeapBroker5TraceEv@PLT
	movl	$8, %edx
	leaq	.LC4(%rip), %rsi
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$27, %edx
	movq	%r13, %rdi
	leaq	.LC5(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compilerlsERSoRKNS1_9ObjectRefE@PLT
	movl	$2, %edx
	leaq	.LC6(%rip), %rsi
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r13, %rdi
	movl	$52, %edx
	leaq	.LC7(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC8(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r13, %rdi
	movl	$215, %esi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC9(%rip), %rsi
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	0(%r13), %rax
	movq	-24(%rax), %rax
	movq	240(%r13,%rax), %r14
	testq	%r14, %r14
	je	.L127
	cmpb	$0, 56(%r14)
	je	.L111
	movsbl	67(%r14), %esi
.L112:
	movq	%r13, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movq	-104(%rbp), %r13
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L104:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties15GetContextInputEPNS1_4NodeE@PLT
	cmpq	%rax, %r13
	jne	.L106
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L116:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties15GetContextInputEPNS1_4NodeE@PLT
	cmpq	%rax, %r14
	jne	.L118
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L111:
	movq	%r14, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%r14), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L112
	movq	%r14, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L112
.L126:
	call	__stack_chk_fail@PLT
.L127:
	call	_ZSt16__throw_bad_castv@PLT
	.cfi_endproc
.LFE22793:
	.size	_ZN2v88internal8compiler23JSContextSpecialization20ReduceJSStoreContextEPNS1_4NodeE, .-_ZN2v88internal8compiler23JSContextSpecialization20ReduceJSStoreContextEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23JSContextSpecialization6ReduceEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23JSContextSpecialization6ReduceEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23JSContextSpecialization6ReduceEPNS1_4NodeE, @function
_ZN2v88internal8compiler23JSContextSpecialization6ReduceEPNS1_4NodeE:
.LFB22756:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	(%rsi), %rdi
	movzwl	16(%rdi), %edx
	cmpw	$749, %dx
	je	.L129
	cmpw	$750, %dx
	je	.L130
	xorl	%eax, %eax
	cmpw	$50, %dx
	je	.L139
.L133:
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L129:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler23JSContextSpecialization19ReduceJSLoadContextEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L139:
	.cfi_restore_state
	call	_ZN2v88internal8compiler16ParameterIndexOfEPKNS1_8OperatorE@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	cmpl	$-1, %r8d
	jne	.L133
	movq	48(%r12), %rax
	testq	%rax, %rax
	je	.L133
	movq	16(%r12), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L130:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler23JSContextSpecialization20ReduceJSStoreContextEPNS1_4NodeE
	.cfi_endproc
.LFE22756:
	.size	_ZN2v88internal8compiler23JSContextSpecialization6ReduceEPNS1_4NodeE, .-_ZN2v88internal8compiler23JSContextSpecialization6ReduceEPNS1_4NodeE
	.section	.text._ZNK2v88internal8compiler23JSContextSpecialization7isolateEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler23JSContextSpecialization7isolateEv
	.type	_ZNK2v88internal8compiler23JSContextSpecialization7isolateEv, @function
_ZNK2v88internal8compiler23JSContextSpecialization7isolateEv:
.LFB22794:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	360(%rax), %rax
	ret
	.cfi_endproc
.LFE22794:
	.size	_ZNK2v88internal8compiler23JSContextSpecialization7isolateEv, .-_ZNK2v88internal8compiler23JSContextSpecialization7isolateEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler23JSContextSpecialization6ReduceEPNS1_4NodeE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler23JSContextSpecialization6ReduceEPNS1_4NodeE, @function
_GLOBAL__sub_I__ZN2v88internal8compiler23JSContextSpecialization6ReduceEPNS1_4NodeE:
.LFB27554:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE27554:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler23JSContextSpecialization6ReduceEPNS1_4NodeE, .-_GLOBAL__sub_I__ZN2v88internal8compiler23JSContextSpecialization6ReduceEPNS1_4NodeE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler23JSContextSpecialization6ReduceEPNS1_4NodeE
	.weak	_ZTVN2v88internal8compiler23JSContextSpecializationE
	.section	.data.rel.ro._ZTVN2v88internal8compiler23JSContextSpecializationE,"awG",@progbits,_ZTVN2v88internal8compiler23JSContextSpecializationE,comdat
	.align 8
	.type	_ZTVN2v88internal8compiler23JSContextSpecializationE, @object
	.size	_ZTVN2v88internal8compiler23JSContextSpecializationE, 56
_ZTVN2v88internal8compiler23JSContextSpecializationE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal8compiler23JSContextSpecializationD1Ev
	.quad	_ZN2v88internal8compiler23JSContextSpecializationD0Ev
	.quad	_ZNK2v88internal8compiler23JSContextSpecialization12reducer_nameEv
	.quad	_ZN2v88internal8compiler23JSContextSpecialization6ReduceEPNS1_4NodeE
	.quad	_ZN2v88internal8compiler7Reducer8FinalizeEv
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
