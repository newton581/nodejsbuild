	.file	"operation-typer.cc"
	.text
	.section	.text._ZN2v88internal8compiler14OperationTyperC2EPNS1_12JSHeapBrokerEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyperC2EPNS1_12JSHeapBrokerEPNS0_4ZoneE
	.type	_ZN2v88internal8compiler14OperationTyperC2EPNS1_12JSHeapBrokerEPNS0_4ZoneE, @function
_ZN2v88internal8compiler14OperationTyperC2EPNS1_12JSHeapBrokerEPNS0_4ZoneE:
.LFB18702:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%rdx, (%rdi)
	call	_ZN2v88internal8compiler9TypeCache3GetEv@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movq	%rax, 8(%rbx)
	movups	%xmm0, 16(%rbx)
	movups	%xmm0, 32(%rbx)
	movups	%xmm0, 48(%rbx)
	movups	%xmm0, 64(%rbx)
	movups	%xmm0, 80(%rbx)
	movups	%xmm0, 96(%rbx)
	movsd	.LC0(%rip), %xmm0
	movq	0(%r13), %r14
	call	_ZN2v88internal8compiler4Type11NewConstantEdPNS0_4ZoneE@PLT
	movsd	.LC1(%rip), %xmm0
	movq	%r12, %rdi
	movq	%rax, 16(%rbx)
	call	_ZN2v88internal8compiler4Type11NewConstantEdPNS0_4ZoneE@PLT
	leaq	128(%r14), %rsi
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%rax, 24(%rbx)
	call	_ZN2v88internal8compiler4Type12HeapConstantEPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEPNS0_4ZoneE@PLT
	leaq	2880(%r14), %rsi
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%rax, 88(%rbx)
	call	_ZN2v88internal8compiler4Type12HeapConstantEPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEPNS0_4ZoneE@PLT
	leaq	3608(%r14), %rsi
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%rax, 32(%rbx)
	call	_ZN2v88internal8compiler4Type12HeapConstantEPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEPNS0_4ZoneE@PLT
	leaq	120(%r14), %rsi
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%rax, 40(%rbx)
	call	_ZN2v88internal8compiler4Type12HeapConstantEPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEPNS0_4ZoneE@PLT
	leaq	112(%r14), %rsi
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%rax, 48(%rbx)
	call	_ZN2v88internal8compiler4Type12HeapConstantEPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEPNS0_4ZoneE@PLT
	leaq	96(%r14), %rsi
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%rax, 56(%rbx)
	call	_ZN2v88internal8compiler4Type12HeapConstantEPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEPNS0_4ZoneE@PLT
	movq	%r12, %rdx
	movl	$6145, %esi
	movl	$1099, %edi
	movq	%rax, 64(%rbx)
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%r12, %rdx
	movl	$6145, %esi
	movl	$1031, %edi
	movq	%rax, 72(%rbx)
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	88(%rbx), %rdi
	movq	%r12, %rdx
	movl	$8388609, %esi
	movq	%rax, 80(%rbx)
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	48(%rbx), %rdi
	movq	%r12, %rdx
	movq	%rax, %r13
	movq	8(%rbx), %rax
	movq	312(%rax), %rsi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%r12, %rdx
	movl	$262529, %edi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%r12, %rdx
	movl	$8193, %esi
	movl	$75169793, %edi
	movq	%rax, 104(%rbx)
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	56(%rbx), %rdi
	movq	%r12, %rdx
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, 96(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18702:
	.size	_ZN2v88internal8compiler14OperationTyperC2EPNS1_12JSHeapBrokerEPNS0_4ZoneE, .-_ZN2v88internal8compiler14OperationTyperC2EPNS1_12JSHeapBrokerEPNS0_4ZoneE
	.globl	_ZN2v88internal8compiler14OperationTyperC1EPNS1_12JSHeapBrokerEPNS0_4ZoneE
	.set	_ZN2v88internal8compiler14OperationTyperC1EPNS1_12JSHeapBrokerEPNS0_4ZoneE,_ZN2v88internal8compiler14OperationTyperC2EPNS1_12JSHeapBrokerEPNS0_4ZoneE
	.section	.text._ZN2v88internal8compiler14OperationTyper5MergeENS1_4TypeES3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper5MergeENS1_4TypeES3_
	.type	_ZN2v88internal8compiler14OperationTyper5MergeENS1_4TypeES3_, @function
_ZN2v88internal8compiler14OperationTyper5MergeENS1_4TypeES3_:
.LFB18704:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	movq	%rsi, %rdi
	movq	%rdx, %rsi
	movq	(%r8), %rdx
	jmp	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	.cfi_endproc
.LFE18704:
	.size	_ZN2v88internal8compiler14OperationTyper5MergeENS1_4TypeES3_, .-_ZN2v88internal8compiler14OperationTyper5MergeENS1_4TypeES3_
	.section	.text._ZN2v88internal8compiler14OperationTyper11WeakenRangeENS1_4TypeES3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper11WeakenRangeENS1_4TypeES3_
	.type	_ZN2v88internal8compiler14OperationTyper11WeakenRangeENS1_4TypeES3_, @function
_ZN2v88internal8compiler14OperationTyper11WeakenRangeENS1_4TypeES3_:
.LFB18705:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	leaq	-40(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$40, %rsp
	movq	%rsi, -40(%rbp)
	movq	%rdx, -48(%rbp)
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	movq	%r12, %rdi
	movsd	%xmm0, -56(%rbp)
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	movsd	-56(%rbp), %xmm2
	ucomisd	%xmm2, %xmm0
	jp	.L14
	jne	.L14
.L6:
	movq	%r13, %rdi
	movsd	%xmm2, -64(%rbp)
	call	_ZNK2v88internal8compiler4Type3MaxEv@PLT
	movq	%r12, %rdi
	movsd	%xmm0, -56(%rbp)
	call	_ZNK2v88internal8compiler4Type3MaxEv@PLT
	movsd	-56(%rbp), %xmm1
	movsd	-64(%rbp), %xmm2
	ucomisd	%xmm1, %xmm0
	jp	.L15
	jne	.L15
.L9:
	movq	(%rbx), %rdi
	movapd	%xmm2, %xmm0
	call	_ZN2v88internal8compiler4Type5RangeEddPNS0_4ZoneE@PLT
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_restore_state
	leaq	_ZZN2v88internal8compiler14OperationTyper11WeakenRangeENS1_4TypeES3_E16kWeakenMinLimits(%rip), %rax
	leaq	168(%rax), %rdx
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L20:
	addq	$8, %rax
	cmpq	%rdx, %rax
	je	.L19
.L8:
	movsd	(%rax), %xmm0
	comisd	%xmm0, %xmm2
	jb	.L20
	movapd	%xmm0, %xmm2
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L15:
	leaq	_ZZN2v88internal8compiler14OperationTyper11WeakenRangeENS1_4TypeES3_E16kWeakenMaxLimits(%rip), %rax
	leaq	168(%rax), %rdx
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L22:
	addq	$8, %rax
	cmpq	%rdx, %rax
	je	.L21
.L11:
	movsd	(%rax), %xmm0
	comisd	%xmm1, %xmm0
	jb	.L22
	movapd	%xmm0, %xmm1
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L19:
	movsd	.LC1(%rip), %xmm2
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L21:
	movsd	.LC0(%rip), %xmm1
	jmp	.L9
	.cfi_endproc
.LFE18705:
	.size	_ZN2v88internal8compiler14OperationTyper11WeakenRangeENS1_4TypeES3_, .-_ZN2v88internal8compiler14OperationTyper11WeakenRangeENS1_4TypeES3_
	.section	.text._ZN2v88internal8compiler14OperationTyper7RangifyENS1_4TypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper7RangifyENS1_4TypeE
	.type	_ZN2v88internal8compiler14OperationTyper7RangifyENS1_4TypeE, @function
_ZN2v88internal8compiler14OperationTyper7RangifyENS1_4TypeE:
.LFB18706:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%rsi, -40(%rbp)
	testb	$1, %al
	jne	.L24
	cmpl	$4, (%rsi)
	jne	.L24
	movq	-40(%rbp), %rax
.L35:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_restore_state
	movq	8(%rbx), %rdx
	leaq	-40(%rbp), %r12
	movq	320(%rdx), %rsi
	cmpq	%rsi, %rax
	jne	.L34
.L26:
	movq	%r12, %rdi
	movq	(%rbx), %r13
	call	_ZNK2v88internal8compiler4Type3MaxEv@PLT
	movq	%r12, %rdi
	movsd	%xmm0, -48(%rbp)
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	movsd	-48(%rbp), %xmm1
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Type5RangeEddPNS0_4ZoneE@PLT
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L26
	movq	-40(%rbp), %rax
	jmp	.L35
	.cfi_endproc
.LFE18706:
	.size	_ZN2v88internal8compiler14OperationTyper7RangifyENS1_4TypeE, .-_ZN2v88internal8compiler14OperationTyper7RangifyENS1_4TypeE
	.section	.text._ZN2v88internal8compiler14OperationTyper9AddRangerEdddd,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper9AddRangerEdddd
	.type	_ZN2v88internal8compiler14OperationTyper9AddRangerEdddd, @function
_ZN2v88internal8compiler14OperationTyper9AddRangerEdddd:
.LFB18709:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movapd	%xmm2, %xmm6
	unpcklpd	%xmm1, %xmm1
	addsd	%xmm0, %xmm2
	unpcklpd	%xmm3, %xmm6
	unpcklpd	%xmm0, %xmm0
	addpd	%xmm6, %xmm0
	addpd	%xmm6, %xmm1
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movdqa	.LC3(%rip), %xmm3
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -64(%rbp)
	cmpunordpd	%xmm0, %xmm0
	movl	$4097, %eax
	movaps	%xmm1, -48(%rbp)
	cmpunordpd	%xmm1, %xmm1
	pand	%xmm3, %xmm0
	pand	%xmm3, %xmm1
	shufps	$136, %xmm1, %xmm0
	movdqa	%xmm0, %xmm1
	psrldq	$8, %xmm1
	paddd	%xmm1, %xmm0
	movdqa	%xmm0, %xmm1
	psrldq	$4, %xmm1
	paddd	%xmm1, %xmm0
	movd	%xmm0, %ebx
	cmpl	$4, %ebx
	je	.L38
	ucomisd	%xmm2, %xmm2
	movq	%rdi, %r12
	movsd	.LC1(%rip), %xmm1
	movq	(%rdi), %rdi
	jp	.L39
	maxsd	%xmm2, %xmm1
.L39:
	movsd	-56(%rbp), %xmm4
	ucomisd	%xmm4, %xmm4
	jp	.L42
	maxsd	%xmm4, %xmm1
.L42:
	movsd	-48(%rbp), %xmm3
	ucomisd	%xmm3, %xmm3
	jp	.L45
	maxsd	%xmm3, %xmm1
.L45:
	movsd	-40(%rbp), %xmm0
	ucomisd	%xmm0, %xmm0
	jp	.L48
	maxsd	%xmm0, %xmm1
.L48:
	pxor	%xmm7, %xmm7
	ucomisd	%xmm7, %xmm1
	jp	.L51
	movq	%xmm1, %rcx
	movq	%xmm7, %rax
	cmovne	%rcx, %rax
	movq	%rax, %xmm1
.L51:
	ucomisd	%xmm2, %xmm2
	jp	.L76
	movsd	.LC0(%rip), %xmm5
	minsd	%xmm2, %xmm5
	movapd	%xmm5, %xmm2
.L53:
	ucomisd	%xmm4, %xmm4
	jp	.L77
	minsd	%xmm4, %xmm2
	movapd	%xmm2, %xmm4
.L56:
	ucomisd	%xmm3, %xmm3
	jp	.L78
	minsd	%xmm3, %xmm4
	movapd	%xmm4, %xmm3
.L59:
	ucomisd	%xmm0, %xmm0
	jp	.L79
	minsd	%xmm0, %xmm3
	movapd	%xmm3, %xmm0
.L62:
	pxor	%xmm7, %xmm7
	ucomisd	%xmm7, %xmm0
	jp	.L65
	movq	%xmm0, %rsi
	movq	%xmm7, %rax
	cmovne	%rsi, %rax
	movq	%rax, %xmm0
.L65:
	call	_ZN2v88internal8compiler4Type5RangeEddPNS0_4ZoneE@PLT
	movq	%rax, %rdi
	testl	%ebx, %ebx
	jne	.L89
.L38:
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L88
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L89:
	.cfi_restore_state
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	movq	(%r12), %rdx
	jne	.L88
	addq	$48, %rsp
	movl	$4097, %esi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	.p2align 4,,10
	.p2align 3
.L79:
	.cfi_restore_state
	movapd	%xmm3, %xmm0
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L76:
	movsd	.LC0(%rip), %xmm2
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L77:
	movapd	%xmm2, %xmm4
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L78:
	movapd	%xmm4, %xmm3
	jmp	.L59
.L88:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18709:
	.size	_ZN2v88internal8compiler14OperationTyper9AddRangerEdddd, .-_ZN2v88internal8compiler14OperationTyper9AddRangerEdddd
	.section	.text._ZN2v88internal8compiler14OperationTyper14SubtractRangerEdddd,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper14SubtractRangerEdddd
	.type	_ZN2v88internal8compiler14OperationTyper14SubtractRangerEdddd, @function
_ZN2v88internal8compiler14OperationTyper14SubtractRangerEdddd:
.LFB18710:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movapd	%xmm2, %xmm6
	movapd	%xmm0, %xmm7
	unpcklpd	%xmm1, %xmm1
	unpcklpd	%xmm3, %xmm6
	unpcklpd	%xmm0, %xmm0
	subsd	%xmm2, %xmm7
	subpd	%xmm6, %xmm0
	subpd	%xmm6, %xmm1
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movdqa	.LC3(%rip), %xmm3
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -64(%rbp)
	cmpunordpd	%xmm0, %xmm0
	movl	$4097, %eax
	movaps	%xmm1, -48(%rbp)
	cmpunordpd	%xmm1, %xmm1
	pand	%xmm3, %xmm0
	pand	%xmm3, %xmm1
	shufps	$136, %xmm1, %xmm0
	movdqa	%xmm0, %xmm1
	psrldq	$8, %xmm1
	paddd	%xmm1, %xmm0
	movdqa	%xmm0, %xmm1
	psrldq	$4, %xmm1
	paddd	%xmm1, %xmm0
	movd	%xmm0, %ebx
	cmpl	$4, %ebx
	je	.L92
	ucomisd	%xmm7, %xmm7
	movq	%rdi, %r12
	movapd	%xmm7, %xmm2
	movq	(%rdi), %rdi
	movsd	.LC1(%rip), %xmm1
	jp	.L93
	maxsd	%xmm7, %xmm1
.L93:
	movsd	-56(%rbp), %xmm4
	ucomisd	%xmm4, %xmm4
	jp	.L96
	maxsd	%xmm4, %xmm1
.L96:
	movsd	-48(%rbp), %xmm3
	ucomisd	%xmm3, %xmm3
	jp	.L99
	maxsd	%xmm3, %xmm1
.L99:
	movsd	-40(%rbp), %xmm0
	ucomisd	%xmm0, %xmm0
	jp	.L102
	maxsd	%xmm0, %xmm1
.L102:
	pxor	%xmm5, %xmm5
	ucomisd	%xmm5, %xmm1
	jp	.L105
	movq	%xmm1, %rcx
	movq	%xmm5, %rax
	cmovne	%rcx, %rax
	movq	%rax, %xmm1
.L105:
	ucomisd	%xmm2, %xmm2
	jp	.L130
	movsd	.LC0(%rip), %xmm5
	minsd	%xmm2, %xmm5
	movapd	%xmm5, %xmm2
.L107:
	ucomisd	%xmm4, %xmm4
	jp	.L131
	minsd	%xmm4, %xmm2
	movapd	%xmm2, %xmm4
.L110:
	ucomisd	%xmm3, %xmm3
	jp	.L132
	minsd	%xmm3, %xmm4
	movapd	%xmm4, %xmm3
.L113:
	ucomisd	%xmm0, %xmm0
	jp	.L133
	minsd	%xmm0, %xmm3
	movapd	%xmm3, %xmm0
.L116:
	pxor	%xmm7, %xmm7
	ucomisd	%xmm7, %xmm0
	jp	.L119
	movq	%xmm0, %rsi
	movq	%xmm7, %rax
	cmovne	%rsi, %rax
	movq	%rax, %xmm0
.L119:
	call	_ZN2v88internal8compiler4Type5RangeEddPNS0_4ZoneE@PLT
	testl	%ebx, %ebx
	jne	.L142
.L92:
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L141
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L142:
	.cfi_restore_state
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	movq	(%r12), %rdx
	jne	.L141
	addq	$48, %rsp
	movl	$4097, %esi
	movq	%rax, %rdi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	.p2align 4,,10
	.p2align 3
.L133:
	.cfi_restore_state
	movapd	%xmm3, %xmm0
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L130:
	movsd	.LC0(%rip), %xmm2
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L131:
	movapd	%xmm2, %xmm4
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L132:
	movapd	%xmm4, %xmm3
	jmp	.L113
.L141:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18710:
	.size	_ZN2v88internal8compiler14OperationTyper14SubtractRangerEdddd, .-_ZN2v88internal8compiler14OperationTyper14SubtractRangerEdddd
	.section	.text._ZN2v88internal8compiler14OperationTyper14MultiplyRangerEdddd,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper14MultiplyRangerEdddd
	.type	_ZN2v88internal8compiler14OperationTyper14MultiplyRangerEdddd, @function
_ZN2v88internal8compiler14OperationTyper14MultiplyRangerEdddd:
.LFB18711:
	.cfi_startproc
	endbr64
	movapd	%xmm1, %xmm9
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movapd	%xmm0, %xmm4
	movapd	%xmm0, %xmm1
	movapd	%xmm0, %xmm5
	movapd	%xmm2, %xmm6
	movapd	%xmm9, %xmm0
	unpcklpd	%xmm3, %xmm6
	unpcklpd	%xmm1, %xmm1
	unpcklpd	%xmm0, %xmm0
	mulpd	%xmm6, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	mulpd	%xmm6, %xmm1
	mulsd	%xmm2, %xmm4
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -48(%rbp)
	movaps	%xmm1, -64(%rbp)
	movsd	-64(%rbp), %xmm0
	ucomisd	%xmm0, %xmm0
	jp	.L144
	movsd	-56(%rbp), %xmm6
	ucomisd	%xmm6, %xmm6
	jp	.L144
	movsd	-48(%rbp), %xmm8
	ucomisd	%xmm8, %xmm8
	jp	.L144
	movsd	-40(%rbp), %xmm1
	ucomisd	%xmm1, %xmm1
	jp	.L144
	movsd	.LC0(%rip), %xmm11
	ucomisd	%xmm4, %xmm4
	movapd	%xmm11, %xmm0
	jp	.L147
	minsd	%xmm4, %xmm0
.L147:
	minsd	%xmm6, %xmm0
	pxor	%xmm7, %xmm7
	minsd	%xmm8, %xmm0
	minsd	%xmm1, %xmm0
	ucomisd	%xmm7, %xmm0
	jp	.L156
	movq	%xmm0, %rcx
	movq	%xmm7, %rax
	cmovne	%rcx, %rax
	movq	%rax, %xmm0
.L156:
	ucomisd	%xmm4, %xmm4
	jp	.L192
	movsd	.LC1(%rip), %xmm10
	movapd	%xmm10, %xmm12
	maxsd	%xmm4, %xmm12
	movapd	%xmm12, %xmm4
.L158:
	maxsd	%xmm6, %xmm4
	movq	(%rbx), %rdi
	movsd	%xmm3, -104(%rbp)
	movsd	%xmm10, -120(%rbp)
	movsd	%xmm11, -112(%rbp)
	movapd	%xmm4, %xmm6
	movsd	%xmm2, -96(%rbp)
	maxsd	%xmm8, %xmm6
	movsd	%xmm9, -88(%rbp)
	movsd	%xmm5, -80(%rbp)
	movapd	%xmm6, %xmm8
	maxsd	%xmm1, %xmm8
	ucomisd	%xmm7, %xmm8
	movapd	%xmm8, %xmm1
	jp	.L198
	jne	.L198
	movapd	%xmm7, %xmm1
	movsd	%xmm0, -72(%rbp)
	call	_ZN2v88internal8compiler4Type5RangeEddPNS0_4ZoneE@PLT
	movsd	-72(%rbp), %xmm0
	pxor	%xmm7, %xmm7
	movsd	-80(%rbp), %xmm5
	movsd	-88(%rbp), %xmm9
	movsd	-96(%rbp), %xmm2
	movq	%rax, %rdi
	comisd	%xmm0, %xmm7
	movsd	-104(%rbp), %xmm3
	movsd	-112(%rbp), %xmm11
	movsd	-120(%rbp), %xmm10
	jnb	.L184
.L170:
	ucomisd	.LC1(%rip), %xmm5
	movl	$0, %edx
	setnp	%al
	ucomisd	%xmm10, %xmm5
	cmovne	%edx, %eax
	testb	%al, %al
	jne	.L199
	ucomisd	.LC0(%rip), %xmm9
	setnp	%dl
	ucomisd	%xmm11, %xmm9
	cmove	%edx, %eax
	testb	%al, %al
	jne	.L199
.L175:
	ucomisd	.LC1(%rip), %xmm2
	movl	$0, %edx
	setnp	%al
	ucomisd	%xmm10, %xmm2
	cmovne	%edx, %eax
	testb	%al, %al
	jne	.L201
	ucomisd	.LC0(%rip), %xmm3
	setnp	%dl
	ucomisd	%xmm11, %xmm3
	cmove	%edx, %eax
	testb	%al, %al
	jne	.L201
.L179:
	movq	%rdi, %rax
.L146:
	movq	-24(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L210
	addq	$120, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L198:
	.cfi_restore_state
	movsd	%xmm0, -72(%rbp)
	movsd	%xmm1, -128(%rbp)
	call	_ZN2v88internal8compiler4Type5RangeEddPNS0_4ZoneE@PLT
	movsd	-72(%rbp), %xmm0
	pxor	%xmm7, %xmm7
	movsd	-80(%rbp), %xmm5
	movsd	-88(%rbp), %xmm9
	movsd	-96(%rbp), %xmm2
	movq	%rax, %rdi
	comisd	%xmm0, %xmm7
	movsd	-104(%rbp), %xmm3
	movsd	-112(%rbp), %xmm11
	movsd	-120(%rbp), %xmm10
	jb	.L170
	movsd	-128(%rbp), %xmm1
	comisd	%xmm7, %xmm1
	jb	.L170
	.p2align 4,,10
	.p2align 3
.L184:
	comisd	%xmm5, %xmm7
	ja	.L173
	comisd	%xmm2, %xmm7
	jbe	.L170
.L173:
	movq	(%rbx), %rdx
	movl	$2049, %esi
	movsd	%xmm3, -96(%rbp)
	movsd	%xmm10, -112(%rbp)
	movsd	%xmm11, -104(%rbp)
	movsd	%xmm2, -88(%rbp)
	movsd	%xmm9, -80(%rbp)
	movsd	%xmm5, -72(%rbp)
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movsd	-112(%rbp), %xmm10
	movsd	-104(%rbp), %xmm11
	pxor	%xmm7, %xmm7
	movsd	-96(%rbp), %xmm3
	movsd	-88(%rbp), %xmm2
	movq	%rax, %rdi
	movsd	-80(%rbp), %xmm9
	movsd	-72(%rbp), %xmm5
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L199:
	comisd	%xmm2, %xmm7
	jb	.L175
	comisd	%xmm7, %xmm3
	jb	.L175
.L178:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	movq	(%rbx), %rdx
	jne	.L210
	addq	$120, %rsp
	movl	$4097, %esi
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	.p2align 4,,10
	.p2align 3
.L201:
	.cfi_restore_state
	comisd	%xmm5, %xmm7
	jb	.L179
	comisd	%xmm7, %xmm9
	jnb	.L178
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L144:
	movq	8(%rbx), %rax
	movq	336(%rax), %rax
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L192:
	movsd	.LC1(%rip), %xmm4
	movapd	%xmm4, %xmm10
	jmp	.L158
.L210:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18711:
	.size	_ZN2v88internal8compiler14OperationTyper14MultiplyRangerEdddd, .-_ZN2v88internal8compiler14OperationTyper14MultiplyRangerEdddd
	.section	.text._ZN2v88internal8compiler14OperationTyper15ConvertReceiverENS1_4TypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper15ConvertReceiverENS1_4TypeE
	.type	_ZN2v88internal8compiler14OperationTyper15ConvertReceiverENS1_4TypeE, @function
_ZN2v88internal8compiler14OperationTyper15ConvertReceiverENS1_4TypeE:
.LFB18712:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rsi, -24(%rbp)
	cmpq	$75431937, %rsi
	jne	.L218
.L215:
	movq	-24(%rbp), %rax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L218:
	.cfi_restore_state
	leaq	-24(%rbp), %r12
	movq	%rdi, %rbx
	movl	$75431937, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L215
	movq	%r12, %rdi
	movl	$134250495, %esi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	movq	(%rbx), %rdx
	movq	-24(%rbp), %rdi
	movl	$75431937, %esi
	movl	%eax, %r12d
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -24(%rbp)
	testb	%r12b, %r12b
	je	.L215
	movq	(%rbx), %rdx
	movl	$131073, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -24(%rbp)
	jmp	.L215
	.cfi_endproc
.LFE18712:
	.size	_ZN2v88internal8compiler14OperationTyper15ConvertReceiverENS1_4TypeE, .-_ZN2v88internal8compiler14OperationTyper15ConvertReceiverENS1_4TypeE
	.section	.text._ZN2v88internal8compiler14OperationTyper8ToNumberENS1_4TypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper8ToNumberENS1_4TypeE
	.type	_ZN2v88internal8compiler14OperationTyper8ToNumberENS1_4TypeE, @function
_ZN2v88internal8compiler14OperationTyper8ToNumberENS1_4TypeE:
.LFB18713:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rsi, -24(%rbp)
	cmpq	$7263, %rsi
	jne	.L242
.L220:
	movq	-24(%rbp), %rax
.L223:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L242:
	.cfi_restore_state
	leaq	-24(%rbp), %r12
	movq	%rdi, %rbx
	movl	$7263, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L220
	movl	$75448353, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	movl	%eax, %r8d
	movl	$7263, %eax
	testb	%r8b, %r8b
	jne	.L223
	movq	-24(%rbp), %rdi
	movq	(%rbx), %rdx
	movl	$24575, %esi
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movl	$129, %esi
	movq	%r12, %rdi
	movq	%rax, -24(%rbp)
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L243
.L224:
	movl	$257, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L244
.L225:
	movq	48(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L245
.L226:
	movq	56(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L246
.L227:
	movq	(%rbx), %rdx
	movq	-24(%rbp), %rdi
	movl	$7263, %esi
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L243:
	movq	8(%rbx), %rax
	movq	(%rbx), %rdx
	movq	-24(%rbp), %rdi
	movq	208(%rax), %rsi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -24(%rbp)
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L246:
	movq	8(%rbx), %rax
	movq	(%rbx), %rdx
	movq	-24(%rbp), %rdi
	movq	216(%rax), %rsi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -24(%rbp)
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L245:
	movq	8(%rbx), %rax
	movq	(%rbx), %rdx
	movq	-24(%rbp), %rdi
	movq	208(%rax), %rsi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -24(%rbp)
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L244:
	movq	(%rbx), %rdx
	movq	-24(%rbp), %rdi
	movl	$4097, %esi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -24(%rbp)
	jmp	.L225
	.cfi_endproc
.LFE18713:
	.size	_ZN2v88internal8compiler14OperationTyper8ToNumberENS1_4TypeE, .-_ZN2v88internal8compiler14OperationTyper8ToNumberENS1_4TypeE
	.section	.text._ZN2v88internal8compiler14OperationTyper21ToNumberConvertBigIntENS1_4TypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper21ToNumberConvertBigIntENS1_4TypeE
	.type	_ZN2v88internal8compiler14OperationTyper21ToNumberConvertBigIntENS1_4TypeE, @function
_ZN2v88internal8compiler14OperationTyper21ToNumberConvertBigIntENS1_4TypeE:
.LFB18714:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-56(%rbp), %r13
	movq	%rdi, %r12
	pushq	%rbx
	movq	%r13, %rdi
	subq	$40, %rsp
	.cfi_offset 3, -40
	movq	%rsi, -56(%rbp)
	movl	$134217729, %esi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	movl	%eax, %ebx
	testb	%al, %al
	je	.L279
.L248:
	movq	(%r12), %rdx
	movq	-56(%rbp), %rdi
	movl	$75464703, %esi
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -48(%rbp)
	cmpq	$7263, %rax
	jne	.L280
.L251:
	movq	%rax, -56(%rbp)
	testb	%bl, %bl
	jne	.L281
.L258:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L282
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L280:
	.cfi_restore_state
	leaq	-48(%rbp), %r13
	movl	$7263, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L250
	movq	-48(%rbp), %rax
	movq	%rax, -56(%rbp)
	testb	%bl, %bl
	je	.L258
.L281:
	movq	8(%r12), %rdx
	movq	%rax, %rdi
	movq	320(%rdx), %rsi
	movq	(%r12), %rdx
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	jmp	.L258
	.p2align 4,,10
	.p2align 3
.L279:
	movl	$75431937, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	movl	%eax, %ebx
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L250:
	movl	$75448353, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	movl	%eax, %r8d
	movl	$7263, %eax
	testb	%r8b, %r8b
	jne	.L251
	movq	-48(%rbp), %rdi
	movq	(%r12), %rdx
	movl	$24575, %esi
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movl	$129, %esi
	movq	%r13, %rdi
	movq	%rax, -48(%rbp)
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L283
.L253:
	movl	$257, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L284
.L254:
	movq	48(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L285
.L255:
	movq	56(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L286
.L256:
	movq	(%r12), %rdx
	movq	-48(%rbp), %rdi
	movl	$7263, %esi
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L283:
	movq	8(%r12), %rax
	movq	(%r12), %rdx
	movq	-48(%rbp), %rdi
	movq	208(%rax), %rsi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -48(%rbp)
	jmp	.L253
	.p2align 4,,10
	.p2align 3
.L286:
	movq	8(%r12), %rax
	movq	(%r12), %rdx
	movq	-48(%rbp), %rdi
	movq	216(%rax), %rsi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -48(%rbp)
	jmp	.L256
	.p2align 4,,10
	.p2align 3
.L285:
	movq	8(%r12), %rax
	movq	(%r12), %rdx
	movq	-48(%rbp), %rdi
	movq	208(%rax), %rsi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -48(%rbp)
	jmp	.L255
	.p2align 4,,10
	.p2align 3
.L284:
	movq	(%r12), %rdx
	movq	-48(%rbp), %rdi
	movl	$4097, %esi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -48(%rbp)
	jmp	.L254
.L282:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18714:
	.size	_ZN2v88internal8compiler14OperationTyper21ToNumberConvertBigIntENS1_4TypeE, .-_ZN2v88internal8compiler14OperationTyper21ToNumberConvertBigIntENS1_4TypeE
	.section	.text._ZN2v88internal8compiler14OperationTyper9ToNumericENS1_4TypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper9ToNumericENS1_4TypeE
	.type	_ZN2v88internal8compiler14OperationTyper9ToNumericENS1_4TypeE, @function
_ZN2v88internal8compiler14OperationTyper9ToNumericENS1_4TypeE:
.LFB18715:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	leaq	-40(%rbp), %rdi
	subq	$24, %rsp
	movq	%rsi, -40(%rbp)
	movl	$75431937, %esi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L293
.L288:
	movq	(%r12), %r14
	movq	-40(%rbp), %rdi
	movl	$134217729, %esi
	movq	%r14, %rdx
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	(%r12), %rdx
	movq	-40(%rbp), %rdi
	movl	$75464703, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14OperationTyper8ToNumberENS1_4TypeE
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	addq	$24, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L293:
	.cfi_restore_state
	movq	(%r12), %rdx
	movq	-40(%rbp), %rdi
	movl	$134217729, %esi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -40(%rbp)
	jmp	.L288
	.cfi_endproc
.LFE18715:
	.size	_ZN2v88internal8compiler14OperationTyper9ToNumericENS1_4TypeE, .-_ZN2v88internal8compiler14OperationTyper9ToNumericENS1_4TypeE
	.section	.text._ZN2v88internal8compiler14OperationTyper9NumberAbsENS1_4TypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper9NumberAbsENS1_4TypeE
	.type	_ZN2v88internal8compiler14OperationTyper9NumberAbsENS1_4TypeE, @function
_ZN2v88internal8compiler14OperationTyper9NumberAbsENS1_4TypeE:
.LFB18716:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, -40(%rbp)
	cmpq	$1, %rsi
	jne	.L295
.L303:
	movq	-40(%rbp), %rax
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L295:
	.cfi_restore_state
	leaq	-40(%rbp), %r14
	movq	%rdi, %rbx
	movl	$4097, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	movl	$2049, %esi
	movq	%r14, %rdi
	movl	%eax, %r12d
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	movq	(%rbx), %rdx
	movq	-40(%rbp), %rdi
	movl	$1119, %esi
	movl	%eax, %r13d
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -40(%rbp)
	cmpq	$1, %rax
	je	.L297
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type3MaxEv@PLT
	movq	%r14, %rdi
	movsd	%xmm0, -48(%rbp)
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	pxor	%xmm1, %xmm1
	comisd	%xmm0, %xmm1
	ja	.L313
.L297:
	testb	%r13b, %r13b
	jne	.L314
.L302:
	testb	%r12b, %r12b
	je	.L303
	movq	(%rbx), %rdx
	movq	-40(%rbp), %rdi
	movl	$4097, %esi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -40(%rbp)
	jmp	.L303
	.p2align 4,,10
	.p2align 3
.L313:
	movq	8(%rbx), %rax
	movq	320(%rax), %rsi
	cmpq	-40(%rbp), %rsi
	jne	.L315
.L300:
	movq	.LC4(%rip), %xmm2
	movsd	-48(%rbp), %xmm1
	movq	(%rbx), %rdi
	andpd	%xmm2, %xmm0
	andpd	%xmm2, %xmm1
	maxsd	%xmm0, %xmm1
	pxor	%xmm0, %xmm0
	call	_ZN2v88internal8compiler4Type5RangeEddPNS0_4ZoneE@PLT
	movq	%rax, -40(%rbp)
	jmp	.L297
	.p2align 4,,10
	.p2align 3
.L314:
	movq	8(%rbx), %rax
	movq	(%rbx), %rdx
	movq	-40(%rbp), %rdi
	movq	208(%rax), %rsi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -40(%rbp)
	jmp	.L302
	.p2align 4,,10
	.p2align 3
.L315:
	movq	%r14, %rdi
	movsd	%xmm0, -56(%rbp)
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	movsd	-56(%rbp), %xmm0
	testb	%al, %al
	jne	.L300
	movq	$1119, -40(%rbp)
	jmp	.L297
	.cfi_endproc
.LFE18716:
	.size	_ZN2v88internal8compiler14OperationTyper9NumberAbsENS1_4TypeE, .-_ZN2v88internal8compiler14OperationTyper9NumberAbsENS1_4TypeE
	.section	.text._ZN2v88internal8compiler14OperationTyper10NumberAcosENS1_4TypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper10NumberAcosENS1_4TypeE
	.type	_ZN2v88internal8compiler14OperationTyper10NumberAcosENS1_4TypeE, @function
_ZN2v88internal8compiler14OperationTyper10NumberAcosENS1_4TypeE:
.LFB18717:
	.cfi_startproc
	endbr64
	movl	$7263, %eax
	ret
	.cfi_endproc
.LFE18717:
	.size	_ZN2v88internal8compiler14OperationTyper10NumberAcosENS1_4TypeE, .-_ZN2v88internal8compiler14OperationTyper10NumberAcosENS1_4TypeE
	.section	.text._ZN2v88internal8compiler14OperationTyper11NumberAcoshENS1_4TypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper11NumberAcoshENS1_4TypeE
	.type	_ZN2v88internal8compiler14OperationTyper11NumberAcoshENS1_4TypeE, @function
_ZN2v88internal8compiler14OperationTyper11NumberAcoshENS1_4TypeE:
.LFB22681:
	.cfi_startproc
	endbr64
	movl	$7263, %eax
	ret
	.cfi_endproc
.LFE22681:
	.size	_ZN2v88internal8compiler14OperationTyper11NumberAcoshENS1_4TypeE, .-_ZN2v88internal8compiler14OperationTyper11NumberAcoshENS1_4TypeE
	.section	.text._ZN2v88internal8compiler14OperationTyper10NumberAsinENS1_4TypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper10NumberAsinENS1_4TypeE
	.type	_ZN2v88internal8compiler14OperationTyper10NumberAsinENS1_4TypeE, @function
_ZN2v88internal8compiler14OperationTyper10NumberAsinENS1_4TypeE:
.LFB22683:
	.cfi_startproc
	endbr64
	movl	$7263, %eax
	ret
	.cfi_endproc
.LFE22683:
	.size	_ZN2v88internal8compiler14OperationTyper10NumberAsinENS1_4TypeE, .-_ZN2v88internal8compiler14OperationTyper10NumberAsinENS1_4TypeE
	.section	.text._ZN2v88internal8compiler14OperationTyper11NumberAsinhENS1_4TypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper11NumberAsinhENS1_4TypeE
	.type	_ZN2v88internal8compiler14OperationTyper11NumberAsinhENS1_4TypeE, @function
_ZN2v88internal8compiler14OperationTyper11NumberAsinhENS1_4TypeE:
.LFB22685:
	.cfi_startproc
	endbr64
	movl	$7263, %eax
	ret
	.cfi_endproc
.LFE22685:
	.size	_ZN2v88internal8compiler14OperationTyper11NumberAsinhENS1_4TypeE, .-_ZN2v88internal8compiler14OperationTyper11NumberAsinhENS1_4TypeE
	.section	.text._ZN2v88internal8compiler14OperationTyper10NumberAtanENS1_4TypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper10NumberAtanENS1_4TypeE
	.type	_ZN2v88internal8compiler14OperationTyper10NumberAtanENS1_4TypeE, @function
_ZN2v88internal8compiler14OperationTyper10NumberAtanENS1_4TypeE:
.LFB22687:
	.cfi_startproc
	endbr64
	movl	$7263, %eax
	ret
	.cfi_endproc
.LFE22687:
	.size	_ZN2v88internal8compiler14OperationTyper10NumberAtanENS1_4TypeE, .-_ZN2v88internal8compiler14OperationTyper10NumberAtanENS1_4TypeE
	.section	.text._ZN2v88internal8compiler14OperationTyper11NumberAtanhENS1_4TypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper11NumberAtanhENS1_4TypeE
	.type	_ZN2v88internal8compiler14OperationTyper11NumberAtanhENS1_4TypeE, @function
_ZN2v88internal8compiler14OperationTyper11NumberAtanhENS1_4TypeE:
.LFB22689:
	.cfi_startproc
	endbr64
	movl	$7263, %eax
	ret
	.cfi_endproc
.LFE22689:
	.size	_ZN2v88internal8compiler14OperationTyper11NumberAtanhENS1_4TypeE, .-_ZN2v88internal8compiler14OperationTyper11NumberAtanhENS1_4TypeE
	.section	.text._ZN2v88internal8compiler14OperationTyper10NumberCbrtENS1_4TypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper10NumberCbrtENS1_4TypeE
	.type	_ZN2v88internal8compiler14OperationTyper10NumberCbrtENS1_4TypeE, @function
_ZN2v88internal8compiler14OperationTyper10NumberCbrtENS1_4TypeE:
.LFB22691:
	.cfi_startproc
	endbr64
	movl	$7263, %eax
	ret
	.cfi_endproc
.LFE22691:
	.size	_ZN2v88internal8compiler14OperationTyper10NumberCbrtENS1_4TypeE, .-_ZN2v88internal8compiler14OperationTyper10NumberCbrtENS1_4TypeE
	.section	.text._ZN2v88internal8compiler14OperationTyper10NumberCeilENS1_4TypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper10NumberCeilENS1_4TypeE
	.type	_ZN2v88internal8compiler14OperationTyper10NumberCeilENS1_4TypeE, @function
_ZN2v88internal8compiler14OperationTyper10NumberCeilENS1_4TypeE:
.LFB18724:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	8(%rdi), %rdx
	movq	%rsi, -24(%rbp)
	movq	336(%rdx), %rsi
	cmpq	%rsi, %rax
	jne	.L327
.L324:
	movq	-24(%rbp), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L327:
	.cfi_restore_state
	movq	%rdi, %rbx
	leaq	-24(%rbp), %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L324
	movq	(%rbx), %rdx
	movq	-24(%rbp), %rdi
	movl	$4097, %esi
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	(%rbx), %rdx
	movq	%rax, -24(%rbp)
	movq	%rax, %rdi
	movq	8(%rbx), %rax
	movq	328(%rax), %rsi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18724:
	.size	_ZN2v88internal8compiler14OperationTyper10NumberCeilENS1_4TypeE, .-_ZN2v88internal8compiler14OperationTyper10NumberCeilENS1_4TypeE
	.section	.text._ZN2v88internal8compiler14OperationTyper11NumberClz32ENS1_4TypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper11NumberClz32ENS1_4TypeE
	.type	_ZN2v88internal8compiler14OperationTyper11NumberClz32ENS1_4TypeE, @function
_ZN2v88internal8compiler14OperationTyper11NumberClz32ENS1_4TypeE:
.LFB18725:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movq	304(%rax), %rax
	ret
	.cfi_endproc
.LFE18725:
	.size	_ZN2v88internal8compiler14OperationTyper11NumberClz32ENS1_4TypeE, .-_ZN2v88internal8compiler14OperationTyper11NumberClz32ENS1_4TypeE
	.section	.text._ZN2v88internal8compiler14OperationTyper9NumberCosENS1_4TypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper9NumberCosENS1_4TypeE
	.type	_ZN2v88internal8compiler14OperationTyper9NumberCosENS1_4TypeE, @function
_ZN2v88internal8compiler14OperationTyper9NumberCosENS1_4TypeE:
.LFB22693:
	.cfi_startproc
	endbr64
	movl	$7263, %eax
	ret
	.cfi_endproc
.LFE22693:
	.size	_ZN2v88internal8compiler14OperationTyper9NumberCosENS1_4TypeE, .-_ZN2v88internal8compiler14OperationTyper9NumberCosENS1_4TypeE
	.section	.text._ZN2v88internal8compiler14OperationTyper10NumberCoshENS1_4TypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper10NumberCoshENS1_4TypeE
	.type	_ZN2v88internal8compiler14OperationTyper10NumberCoshENS1_4TypeE, @function
_ZN2v88internal8compiler14OperationTyper10NumberCoshENS1_4TypeE:
.LFB22695:
	.cfi_startproc
	endbr64
	movl	$7263, %eax
	ret
	.cfi_endproc
.LFE22695:
	.size	_ZN2v88internal8compiler14OperationTyper10NumberCoshENS1_4TypeE, .-_ZN2v88internal8compiler14OperationTyper10NumberCoshENS1_4TypeE
	.section	.text._ZN2v88internal8compiler14OperationTyper9NumberExpENS1_4TypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper9NumberExpENS1_4TypeE
	.type	_ZN2v88internal8compiler14OperationTyper9NumberExpENS1_4TypeE, @function
_ZN2v88internal8compiler14OperationTyper9NumberExpENS1_4TypeE:
.LFB18728:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movl	$4097, %esi
	movl	$1119, %edi
	jmp	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	.cfi_endproc
.LFE18728:
	.size	_ZN2v88internal8compiler14OperationTyper9NumberExpENS1_4TypeE, .-_ZN2v88internal8compiler14OperationTyper9NumberExpENS1_4TypeE
	.section	.text._ZN2v88internal8compiler14OperationTyper11NumberExpm1ENS1_4TypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper11NumberExpm1ENS1_4TypeE
	.type	_ZN2v88internal8compiler14OperationTyper11NumberExpm1ENS1_4TypeE, @function
_ZN2v88internal8compiler14OperationTyper11NumberExpm1ENS1_4TypeE:
.LFB22697:
	.cfi_startproc
	endbr64
	movl	$7263, %eax
	ret
	.cfi_endproc
.LFE22697:
	.size	_ZN2v88internal8compiler14OperationTyper11NumberExpm1ENS1_4TypeE, .-_ZN2v88internal8compiler14OperationTyper11NumberExpm1ENS1_4TypeE
	.section	.text._ZN2v88internal8compiler14OperationTyper11NumberFloorENS1_4TypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper11NumberFloorENS1_4TypeE
	.type	_ZN2v88internal8compiler14OperationTyper11NumberFloorENS1_4TypeE, @function
_ZN2v88internal8compiler14OperationTyper11NumberFloorENS1_4TypeE:
.LFB18730:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	8(%rdi), %rdx
	movq	%rsi, -24(%rbp)
	movq	336(%rdx), %rsi
	cmpq	%rsi, %rax
	jne	.L337
.L334:
	movq	-24(%rbp), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L337:
	.cfi_restore_state
	movq	%rdi, %rbx
	leaq	-24(%rbp), %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L334
	movq	(%rbx), %rdx
	movq	-24(%rbp), %rdi
	movl	$6145, %esi
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	(%rbx), %rdx
	movq	%rax, -24(%rbp)
	movq	%rax, %rdi
	movq	8(%rbx), %rax
	movq	320(%rax), %rsi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18730:
	.size	_ZN2v88internal8compiler14OperationTyper11NumberFloorENS1_4TypeE, .-_ZN2v88internal8compiler14OperationTyper11NumberFloorENS1_4TypeE
	.section	.text._ZN2v88internal8compiler14OperationTyper12NumberFroundENS1_4TypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper12NumberFroundENS1_4TypeE
	.type	_ZN2v88internal8compiler14OperationTyper12NumberFroundENS1_4TypeE, @function
_ZN2v88internal8compiler14OperationTyper12NumberFroundENS1_4TypeE:
.LFB22699:
	.cfi_startproc
	endbr64
	movl	$7263, %eax
	ret
	.cfi_endproc
.LFE22699:
	.size	_ZN2v88internal8compiler14OperationTyper12NumberFroundENS1_4TypeE, .-_ZN2v88internal8compiler14OperationTyper12NumberFroundENS1_4TypeE
	.section	.text._ZN2v88internal8compiler14OperationTyper9NumberLogENS1_4TypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper9NumberLogENS1_4TypeE
	.type	_ZN2v88internal8compiler14OperationTyper9NumberLogENS1_4TypeE, @function
_ZN2v88internal8compiler14OperationTyper9NumberLogENS1_4TypeE:
.LFB22701:
	.cfi_startproc
	endbr64
	movl	$7263, %eax
	ret
	.cfi_endproc
.LFE22701:
	.size	_ZN2v88internal8compiler14OperationTyper9NumberLogENS1_4TypeE, .-_ZN2v88internal8compiler14OperationTyper9NumberLogENS1_4TypeE
	.section	.text._ZN2v88internal8compiler14OperationTyper11NumberLog1pENS1_4TypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper11NumberLog1pENS1_4TypeE
	.type	_ZN2v88internal8compiler14OperationTyper11NumberLog1pENS1_4TypeE, @function
_ZN2v88internal8compiler14OperationTyper11NumberLog1pENS1_4TypeE:
.LFB22703:
	.cfi_startproc
	endbr64
	movl	$7263, %eax
	ret
	.cfi_endproc
.LFE22703:
	.size	_ZN2v88internal8compiler14OperationTyper11NumberLog1pENS1_4TypeE, .-_ZN2v88internal8compiler14OperationTyper11NumberLog1pENS1_4TypeE
	.section	.text._ZN2v88internal8compiler14OperationTyper10NumberLog2ENS1_4TypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper10NumberLog2ENS1_4TypeE
	.type	_ZN2v88internal8compiler14OperationTyper10NumberLog2ENS1_4TypeE, @function
_ZN2v88internal8compiler14OperationTyper10NumberLog2ENS1_4TypeE:
.LFB22705:
	.cfi_startproc
	endbr64
	movl	$7263, %eax
	ret
	.cfi_endproc
.LFE22705:
	.size	_ZN2v88internal8compiler14OperationTyper10NumberLog2ENS1_4TypeE, .-_ZN2v88internal8compiler14OperationTyper10NumberLog2ENS1_4TypeE
	.section	.text._ZN2v88internal8compiler14OperationTyper11NumberLog10ENS1_4TypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper11NumberLog10ENS1_4TypeE
	.type	_ZN2v88internal8compiler14OperationTyper11NumberLog10ENS1_4TypeE, @function
_ZN2v88internal8compiler14OperationTyper11NumberLog10ENS1_4TypeE:
.LFB22707:
	.cfi_startproc
	endbr64
	movl	$7263, %eax
	ret
	.cfi_endproc
.LFE22707:
	.size	_ZN2v88internal8compiler14OperationTyper11NumberLog10ENS1_4TypeE, .-_ZN2v88internal8compiler14OperationTyper11NumberLog10ENS1_4TypeE
	.section	.text._ZN2v88internal8compiler14OperationTyper11NumberRoundENS1_4TypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper11NumberRoundENS1_4TypeE
	.type	_ZN2v88internal8compiler14OperationTyper11NumberRoundENS1_4TypeE, @function
_ZN2v88internal8compiler14OperationTyper11NumberRoundENS1_4TypeE:
.LFB22719:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	8(%rdi), %rdx
	movq	%fs:40, %rcx
	movq	%rcx, -24(%rbp)
	xorl	%ecx, %ecx
	movq	%rsi, -32(%rbp)
	movq	336(%rdx), %rsi
	cmpq	%rax, %rsi
	jne	.L349
.L346:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L350
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L349:
	.cfi_restore_state
	movq	%rdi, %rbx
	leaq	-32(%rbp), %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L345
	movq	-32(%rbp), %rax
	jmp	.L346
	.p2align 4,,10
	.p2align 3
.L345:
	movq	(%rbx), %rdx
	movq	-32(%rbp), %rdi
	movl	$4097, %esi
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	(%rbx), %rdx
	movq	%rax, -32(%rbp)
	movq	%rax, %rdi
	movq	8(%rbx), %rax
	movq	328(%rax), %rsi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -32(%rbp)
	jmp	.L346
.L350:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22719:
	.size	_ZN2v88internal8compiler14OperationTyper11NumberRoundENS1_4TypeE, .-_ZN2v88internal8compiler14OperationTyper11NumberRoundENS1_4TypeE
	.section	.text._ZN2v88internal8compiler14OperationTyper10NumberSignENS1_4TypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper10NumberSignENS1_4TypeE
	.type	_ZN2v88internal8compiler14OperationTyper10NumberSignENS1_4TypeE, @function
_ZN2v88internal8compiler14OperationTyper10NumberSignENS1_4TypeE:
.LFB18737:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	8(%rdi), %rdx
	movq	%rsi, -40(%rbp)
	movq	312(%rdx), %rsi
	cmpq	%rsi, %rax
	jne	.L382
.L364:
	movq	-40(%rbp), %rax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L382:
	.cfi_restore_state
	leaq	-40(%rbp), %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L364
	movl	$2049, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	movl	$4097, %esi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	movq	(%rbx), %rdx
	movq	-40(%rbp), %rdi
	movl	$1119, %esi
	movl	%eax, %r13d
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -40(%rbp)
	cmpq	$1, %rax
	jne	.L383
.L353:
	testb	%r14b, %r14b
	jne	.L384
.L363:
	testb	%r13b, %r13b
	je	.L364
	movq	(%rbx), %rdx
	movq	-40(%rbp), %rdi
	movl	$4097, %esi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -40(%rbp)
	jmp	.L364
	.p2align 4,,10
	.p2align 3
.L383:
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type3MaxEv@PLT
	pxor	%xmm2, %xmm2
	comisd	%xmm0, %xmm2
	jbe	.L377
	movq	8(%rbx), %rax
	movq	232(%rax), %rax
	movq	%rax, -40(%rbp)
	jmp	.L353
	.p2align 4,,10
	.p2align 3
.L384:
	movq	(%rbx), %rdx
	movq	-40(%rbp), %rdi
	movl	$2049, %esi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -40(%rbp)
	jmp	.L363
	.p2align 4,,10
	.p2align 3
.L377:
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type3MaxEv@PLT
	pxor	%xmm3, %xmm3
	comisd	%xmm0, %xmm3
	jb	.L378
	movq	8(%rbx), %rax
	movq	264(%rax), %rax
	movq	%rax, -40(%rbp)
	jmp	.L353
	.p2align 4,,10
	.p2align 3
.L378:
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	pxor	%xmm4, %xmm4
	comisd	%xmm4, %xmm0
	jbe	.L379
	movq	8(%rbx), %rax
	movq	216(%rax), %rax
	movq	%rax, -40(%rbp)
	jmp	.L353
.L379:
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	pxor	%xmm5, %xmm5
	comisd	%xmm5, %xmm0
	jb	.L380
	movq	8(%rbx), %rax
	movq	280(%rax), %rax
	movq	%rax, -40(%rbp)
	jmp	.L353
.L380:
	movq	(%rbx), %rdi
	movsd	.LC5(%rip), %xmm1
	movsd	.LC6(%rip), %xmm0
	call	_ZN2v88internal8compiler4Type5RangeEddPNS0_4ZoneE@PLT
	movq	%rax, -40(%rbp)
	jmp	.L353
	.cfi_endproc
.LFE18737:
	.size	_ZN2v88internal8compiler14OperationTyper10NumberSignENS1_4TypeE, .-_ZN2v88internal8compiler14OperationTyper10NumberSignENS1_4TypeE
	.section	.text._ZN2v88internal8compiler14OperationTyper9NumberSinENS1_4TypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper9NumberSinENS1_4TypeE
	.type	_ZN2v88internal8compiler14OperationTyper9NumberSinENS1_4TypeE, @function
_ZN2v88internal8compiler14OperationTyper9NumberSinENS1_4TypeE:
.LFB22709:
	.cfi_startproc
	endbr64
	movl	$7263, %eax
	ret
	.cfi_endproc
.LFE22709:
	.size	_ZN2v88internal8compiler14OperationTyper9NumberSinENS1_4TypeE, .-_ZN2v88internal8compiler14OperationTyper9NumberSinENS1_4TypeE
	.section	.text._ZN2v88internal8compiler14OperationTyper10NumberSinhENS1_4TypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper10NumberSinhENS1_4TypeE
	.type	_ZN2v88internal8compiler14OperationTyper10NumberSinhENS1_4TypeE, @function
_ZN2v88internal8compiler14OperationTyper10NumberSinhENS1_4TypeE:
.LFB22711:
	.cfi_startproc
	endbr64
	movl	$7263, %eax
	ret
	.cfi_endproc
.LFE22711:
	.size	_ZN2v88internal8compiler14OperationTyper10NumberSinhENS1_4TypeE, .-_ZN2v88internal8compiler14OperationTyper10NumberSinhENS1_4TypeE
	.section	.text._ZN2v88internal8compiler14OperationTyper10NumberSqrtENS1_4TypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper10NumberSqrtENS1_4TypeE
	.type	_ZN2v88internal8compiler14OperationTyper10NumberSqrtENS1_4TypeE, @function
_ZN2v88internal8compiler14OperationTyper10NumberSqrtENS1_4TypeE:
.LFB22713:
	.cfi_startproc
	endbr64
	movl	$7263, %eax
	ret
	.cfi_endproc
.LFE22713:
	.size	_ZN2v88internal8compiler14OperationTyper10NumberSqrtENS1_4TypeE, .-_ZN2v88internal8compiler14OperationTyper10NumberSqrtENS1_4TypeE
	.section	.text._ZN2v88internal8compiler14OperationTyper9NumberTanENS1_4TypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper9NumberTanENS1_4TypeE
	.type	_ZN2v88internal8compiler14OperationTyper9NumberTanENS1_4TypeE, @function
_ZN2v88internal8compiler14OperationTyper9NumberTanENS1_4TypeE:
.LFB22715:
	.cfi_startproc
	endbr64
	movl	$7263, %eax
	ret
	.cfi_endproc
.LFE22715:
	.size	_ZN2v88internal8compiler14OperationTyper9NumberTanENS1_4TypeE, .-_ZN2v88internal8compiler14OperationTyper9NumberTanENS1_4TypeE
	.section	.text._ZN2v88internal8compiler14OperationTyper10NumberTanhENS1_4TypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper10NumberTanhENS1_4TypeE
	.type	_ZN2v88internal8compiler14OperationTyper10NumberTanhENS1_4TypeE, @function
_ZN2v88internal8compiler14OperationTyper10NumberTanhENS1_4TypeE:
.LFB22717:
	.cfi_startproc
	endbr64
	movl	$7263, %eax
	ret
	.cfi_endproc
.LFE22717:
	.size	_ZN2v88internal8compiler14OperationTyper10NumberTanhENS1_4TypeE, .-_ZN2v88internal8compiler14OperationTyper10NumberTanhENS1_4TypeE
	.section	.text._ZN2v88internal8compiler14OperationTyper11NumberTruncENS1_4TypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper11NumberTruncENS1_4TypeE
	.type	_ZN2v88internal8compiler14OperationTyper11NumberTruncENS1_4TypeE, @function
_ZN2v88internal8compiler14OperationTyper11NumberTruncENS1_4TypeE:
.LFB22721:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	8(%rdi), %rdx
	movq	%fs:40, %rcx
	movq	%rcx, -24(%rbp)
	xorl	%ecx, %ecx
	movq	%rsi, -32(%rbp)
	movq	336(%rdx), %rsi
	cmpq	%rax, %rsi
	jne	.L396
.L393:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L397
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L396:
	.cfi_restore_state
	movq	%rdi, %rbx
	leaq	-32(%rbp), %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L392
	movq	-32(%rbp), %rax
	jmp	.L393
	.p2align 4,,10
	.p2align 3
.L392:
	movq	(%rbx), %rdx
	movq	-32(%rbp), %rdi
	movl	$4097, %esi
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	(%rbx), %rdx
	movq	%rax, -32(%rbp)
	movq	%rax, %rdi
	movq	8(%rbx), %rax
	movq	328(%rax), %rsi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -32(%rbp)
	jmp	.L393
.L397:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22721:
	.size	_ZN2v88internal8compiler14OperationTyper11NumberTruncENS1_4TypeE, .-_ZN2v88internal8compiler14OperationTyper11NumberTruncENS1_4TypeE
	.section	.text._ZN2v88internal8compiler14OperationTyper15NumberToBooleanENS1_4TypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper15NumberToBooleanENS1_4TypeE
	.type	_ZN2v88internal8compiler14OperationTyper15NumberToBooleanENS1_4TypeE, @function
_ZN2v88internal8compiler14OperationTyper15NumberToBooleanENS1_4TypeE:
.LFB18744:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rsi, -24(%rbp)
	cmpq	$1, %rsi
	je	.L400
	movq	8(%rdi), %rdx
	movq	%rdi, %rbx
	movq	312(%rdx), %rsi
	cmpq	%rsi, %rax
	jne	.L409
.L401:
	movq	48(%rbx), %rax
.L400:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L409:
	.cfi_restore_state
	leaq	-24(%rbp), %r12
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L401
	cmpq	$1119, -24(%rbp)
	je	.L405
	movl	$1119, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L406
.L405:
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type3MaxEv@PLT
	pxor	%xmm1, %xmm1
	comisd	%xmm0, %xmm1
	jbe	.L410
.L403:
	movq	56(%rbx), %rax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L410:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	pxor	%xmm2, %xmm2
	comisd	%xmm2, %xmm0
	ja	.L403
.L406:
	movl	$513, %eax
	jmp	.L400
	.cfi_endproc
.LFE18744:
	.size	_ZN2v88internal8compiler14OperationTyper15NumberToBooleanENS1_4TypeE, .-_ZN2v88internal8compiler14OperationTyper15NumberToBooleanENS1_4TypeE
	.section	.text._ZN2v88internal8compiler14OperationTyper13NumberToInt32ENS1_4TypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper13NumberToInt32ENS1_4TypeE
	.type	_ZN2v88internal8compiler14OperationTyper13NumberToInt32ENS1_4TypeE, @function
_ZN2v88internal8compiler14OperationTyper13NumberToInt32ENS1_4TypeE:
.LFB18745:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rsi, -24(%rbp)
	cmpq	$1099, %rsi
	jne	.L422
.L412:
	movq	-24(%rbp), %rax
.L415:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L422:
	.cfi_restore_state
	leaq	-24(%rbp), %r12
	movq	%rdi, %rbx
	movl	$1099, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L412
	movq	8(%rbx), %rax
	movq	312(%rax), %rsi
	cmpq	%rsi, -24(%rbp)
	jne	.L423
.L413:
	movq	208(%rax), %rax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L423:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L416
	movq	8(%rbx), %rax
	jmp	.L413
	.p2align 4,,10
	.p2align 3
.L416:
	movq	72(%rbx), %rsi
	cmpq	-24(%rbp), %rsi
	jne	.L424
.L417:
	movq	8(%rbx), %rax
	movq	(%rbx), %r12
	movq	-24(%rbp), %rdi
	movq	208(%rax), %rsi
	movq	%r12, %rdx
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%r12, %rdx
	movl	$1099, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	jmp	.L415
	.p2align 4,,10
	.p2align 3
.L424:
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L417
	movl	$1099, %eax
	jmp	.L415
	.cfi_endproc
.LFE18745:
	.size	_ZN2v88internal8compiler14OperationTyper13NumberToInt32ENS1_4TypeE, .-_ZN2v88internal8compiler14OperationTyper13NumberToInt32ENS1_4TypeE
	.section	.text._ZN2v88internal8compiler14OperationTyper14NumberToStringENS1_4TypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper14NumberToStringENS1_4TypeE
	.type	_ZN2v88internal8compiler14OperationTyper14NumberToStringENS1_4TypeE, @function
_ZN2v88internal8compiler14OperationTyper14NumberToStringENS1_4TypeE:
.LFB18746:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rsi, -24(%rbp)
	cmpq	$1, %rsi
	je	.L427
	movq	%rdi, %rbx
	cmpq	$4097, %rsi
	jne	.L436
.L428:
	movq	32(%rbx), %rax
.L427:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L436:
	.cfi_restore_state
	leaq	-24(%rbp), %r12
	movl	$4097, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L428
	movq	8(%rbx), %rax
	movq	240(%rax), %rsi
	cmpq	-24(%rbp), %rsi
	je	.L429
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L437
.L429:
	movq	40(%rbx), %rax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L437:
	.cfi_restore_state
	movl	$16417, %eax
	jmp	.L427
	.cfi_endproc
.LFE18746:
	.size	_ZN2v88internal8compiler14OperationTyper14NumberToStringENS1_4TypeE, .-_ZN2v88internal8compiler14OperationTyper14NumberToStringENS1_4TypeE
	.section	.text._ZN2v88internal8compiler14OperationTyper14NumberToUint32ENS1_4TypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper14NumberToUint32ENS1_4TypeE
	.type	_ZN2v88internal8compiler14OperationTyper14NumberToUint32ENS1_4TypeE, @function
_ZN2v88internal8compiler14OperationTyper14NumberToUint32ENS1_4TypeE:
.LFB18747:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rsi, -24(%rbp)
	cmpq	$1031, %rsi
	jne	.L449
.L439:
	movq	-24(%rbp), %rax
.L442:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L449:
	.cfi_restore_state
	leaq	-24(%rbp), %r12
	movq	%rdi, %rbx
	movl	$1031, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L439
	movq	8(%rbx), %rax
	movq	312(%rax), %rsi
	cmpq	%rsi, -24(%rbp)
	jne	.L450
.L440:
	movq	208(%rax), %rax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L450:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L443
	movq	8(%rbx), %rax
	jmp	.L440
	.p2align 4,,10
	.p2align 3
.L443:
	movq	80(%rbx), %rsi
	cmpq	-24(%rbp), %rsi
	jne	.L451
.L444:
	movq	8(%rbx), %rax
	movq	(%rbx), %r12
	movq	-24(%rbp), %rdi
	movq	208(%rax), %rsi
	movq	%r12, %rdx
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%r12, %rdx
	movl	$1031, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	jmp	.L442
	.p2align 4,,10
	.p2align 3
.L451:
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L444
	movl	$1031, %eax
	jmp	.L442
	.cfi_endproc
.LFE18747:
	.size	_ZN2v88internal8compiler14OperationTyper14NumberToUint32ENS1_4TypeE, .-_ZN2v88internal8compiler14OperationTyper14NumberToUint32ENS1_4TypeE
	.section	.text._ZN2v88internal8compiler14OperationTyper20NumberToUint8ClampedENS1_4TypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper20NumberToUint8ClampedENS1_4TypeE
	.type	_ZN2v88internal8compiler14OperationTyper20NumberToUint8ClampedENS1_4TypeE, @function
_ZN2v88internal8compiler14OperationTyper20NumberToUint8ClampedENS1_4TypeE:
.LFB18748:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	8(%rdi), %rdx
	movq	%rsi, -24(%rbp)
	movq	96(%rdx), %rsi
	cmpq	%rsi, %rax
	jne	.L456
.L453:
	movq	-24(%rbp), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L456:
	.cfi_restore_state
	movq	%rdi, %rbx
	leaq	-24(%rbp), %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L453
	movq	8(%rbx), %rax
	movq	96(%rax), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18748:
	.size	_ZN2v88internal8compiler14OperationTyper20NumberToUint8ClampedENS1_4TypeE, .-_ZN2v88internal8compiler14OperationTyper20NumberToUint8ClampedENS1_4TypeE
	.section	.text._ZN2v88internal8compiler14OperationTyper16NumberSilenceNaNENS1_4TypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper16NumberSilenceNaNENS1_4TypeE
	.type	_ZN2v88internal8compiler14OperationTyper16NumberSilenceNaNENS1_4TypeE, @function
_ZN2v88internal8compiler14OperationTyper16NumberSilenceNaNENS1_4TypeE:
.LFB18749:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	leaq	-8(%rbp), %rdi
	movq	%rsi, -8(%rbp)
	movl	$4097, %esi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	movl	$7263, %eax
	cmove	-8(%rbp), %rax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18749:
	.size	_ZN2v88internal8compiler14OperationTyper16NumberSilenceNaNENS1_4TypeE, .-_ZN2v88internal8compiler14OperationTyper16NumberSilenceNaNENS1_4TypeE
	.section	.text._ZN2v88internal8compiler14OperationTyper13BigIntAsUintNENS1_4TypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper13BigIntAsUintNENS1_4TypeE
	.type	_ZN2v88internal8compiler14OperationTyper13BigIntAsUintNENS1_4TypeE, @function
_ZN2v88internal8compiler14OperationTyper13BigIntAsUintNENS1_4TypeE:
.LFB18750:
	.cfi_startproc
	endbr64
	movl	$134217729, %eax
	ret
	.cfi_endproc
.LFE18750:
	.size	_ZN2v88internal8compiler14OperationTyper13BigIntAsUintNENS1_4TypeE, .-_ZN2v88internal8compiler14OperationTyper13BigIntAsUintNENS1_4TypeE
	.section	.text._ZN2v88internal8compiler14OperationTyper11CheckBigIntENS1_4TypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper11CheckBigIntENS1_4TypeE
	.type	_ZN2v88internal8compiler14OperationTyper11CheckBigIntENS1_4TypeE, @function
_ZN2v88internal8compiler14OperationTyper11CheckBigIntENS1_4TypeE:
.LFB22723:
	.cfi_startproc
	endbr64
	movl	$134217729, %eax
	ret
	.cfi_endproc
.LFE22723:
	.size	_ZN2v88internal8compiler14OperationTyper11CheckBigIntENS1_4TypeE, .-_ZN2v88internal8compiler14OperationTyper11CheckBigIntENS1_4TypeE
	.section	.text._ZN2v88internal8compiler14OperationTyper9NumberAddENS1_4TypeES3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper9NumberAddENS1_4TypeES3_
	.type	_ZN2v88internal8compiler14OperationTyper9NumberAddENS1_4TypeES3_, @function
_ZN2v88internal8compiler14OperationTyper9NumberAddENS1_4TypeES3_:
.LFB18752:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -56(%rbp)
	movq	%rdx, -64(%rbp)
	cmpq	$1, %rsi
	je	.L464
	cmpq	$1, %rdx
	jne	.L465
.L464:
	addq	$56, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L465:
	.cfi_restore_state
	leaq	-56(%rbp), %r14
	movq	%rdi, %rbx
	leaq	-64(%rbp), %r15
	movl	$4097, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	movl	%eax, %r12d
	testb	%al, %al
	je	.L503
.L467:
	movl	$2049, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	movb	%al, -72(%rbp)
	testb	%al, %al
	jne	.L504
.L468:
	movl	$2049, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	movl	%eax, %r13d
	testb	%al, %al
	jne	.L505
	movq	(%rbx), %rdx
	movq	-56(%rbp), %rdi
	movl	$1119, %esi
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	-64(%rbp), %rdi
	movq	(%rbx), %rdx
	movl	$1119, %esi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	-56(%rbp), %rdi
	movq	%rax, -64(%rbp)
	cmpq	$1, %rdi
	je	.L479
	cmpq	$1, %rax
	je	.L506
.L483:
	movq	8(%rbx), %rdx
	movq	320(%rdx), %rsi
	cmpq	%rdi, %rsi
	je	.L471
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L476
	movq	8(%rbx), %rax
	movq	320(%rax), %rdi
	movq	-64(%rbp), %rax
.L471:
	cmpq	%rdi, %rax
	je	.L475
	movq	%rdi, %rsi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L476
.L475:
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type3MaxEv@PLT
	movq	%r15, %rdi
	movsd	%xmm0, -88(%rbp)
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	movq	%r14, %rdi
	movsd	%xmm0, -80(%rbp)
	call	_ZNK2v88internal8compiler4Type3MaxEv@PLT
	movq	%r14, %rdi
	movsd	%xmm0, -72(%rbp)
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	movsd	-88(%rbp), %xmm3
	movsd	-80(%rbp), %xmm2
	movq	%rbx, %rdi
	movsd	-72(%rbp), %xmm1
	call	_ZN2v88internal8compiler14OperationTyper9AddRangerEdddd
	movq	%rax, %rdi
.L470:
	testb	%r13b, %r13b
	je	.L479
.L481:
	movq	(%rbx), %rdx
	movl	$2049, %esi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, %rdi
.L479:
	testb	%r12b, %r12b
	jne	.L482
.L480:
	addq	$56, %rsp
	movq	%rdi, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L476:
	.cfi_restore_state
	movq	24(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	je	.L474
	movq	16(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	je	.L474
.L478:
	movl	$1119, %edi
	testb	%r13b, %r13b
	je	.L482
	movl	%r13d, %r12d
	movl	$1119, %edi
	jmp	.L481
	.p2align 4,,10
	.p2align 3
.L505:
	movq	8(%rbx), %rax
	movq	(%rbx), %rdx
	movq	-64(%rbp), %rdi
	movq	208(%rax), %rsi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	(%rbx), %rdx
	movq	-56(%rbp), %rdi
	movl	$1119, %esi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	-64(%rbp), %rdi
	movq	(%rbx), %rdx
	movl	$1119, %esi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	-56(%rbp), %rdi
	movzbl	-72(%rbp), %r13d
	movq	%rax, -64(%rbp)
	cmpq	$1, %rdi
	je	.L470
	cmpq	$1, %rax
	jne	.L483
	movl	$1, %edi
	jmp	.L470
	.p2align 4,,10
	.p2align 3
.L504:
	movq	8(%rbx), %rax
	movq	(%rbx), %rdx
	movq	-56(%rbp), %rdi
	movq	208(%rax), %rsi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -56(%rbp)
	jmp	.L468
	.p2align 4,,10
	.p2align 3
.L503:
	movl	$4097, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	movl	%eax, %r12d
	jmp	.L467
	.p2align 4,,10
	.p2align 3
.L482:
	movq	(%rbx), %rdx
	movl	$4097, %esi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, %rdi
	jmp	.L480
	.p2align 4,,10
	.p2align 3
.L474:
	movq	24(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L477
.L502:
	movl	$1119, %edi
	jmp	.L470
	.p2align 4,,10
	.p2align 3
.L506:
	movl	$1, %edi
	jmp	.L479
	.p2align 4,,10
	.p2align 3
.L477:
	movq	16(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L478
	jmp	.L502
	.cfi_endproc
.LFE18752:
	.size	_ZN2v88internal8compiler14OperationTyper9NumberAddENS1_4TypeES3_, .-_ZN2v88internal8compiler14OperationTyper9NumberAddENS1_4TypeES3_
	.section	.text._ZN2v88internal8compiler14OperationTyper14NumberSubtractENS1_4TypeES3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper14NumberSubtractENS1_4TypeES3_
	.type	_ZN2v88internal8compiler14OperationTyper14NumberSubtractENS1_4TypeES3_, @function
_ZN2v88internal8compiler14OperationTyper14NumberSubtractENS1_4TypeES3_:
.LFB18753:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -56(%rbp)
	movq	%rdx, -64(%rbp)
	cmpq	$1, %rsi
	je	.L508
	cmpq	$1, %rdx
	jne	.L509
.L508:
	addq	$56, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L509:
	.cfi_restore_state
	leaq	-56(%rbp), %r14
	movq	%rdi, %rbx
	leaq	-64(%rbp), %r15
	movl	$4097, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	movl	%eax, %r13d
	testb	%al, %al
	je	.L548
.L511:
	movl	$2049, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	movl	%eax, %r12d
	testb	%al, %al
	jne	.L549
.L512:
	movl	$2049, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L550
.L513:
	movq	(%rbx), %rdx
	movq	-56(%rbp), %rdi
	movl	$1119, %esi
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	-64(%rbp), %rdi
	movq	(%rbx), %rdx
	movl	$1119, %esi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	-56(%rbp), %rdi
	movq	%rax, -64(%rbp)
	cmpq	$1, %rdi
	je	.L514
	cmpq	$1, %rax
	je	.L528
	movq	8(%rbx), %rdx
	movq	320(%rdx), %rsi
	cmpq	%rsi, %rdi
	je	.L515
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L520
	movq	8(%rbx), %rax
	movq	320(%rax), %rsi
	movq	-64(%rbp), %rax
.L515:
	cmpq	%rax, %rsi
	je	.L519
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L520
.L519:
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type3MaxEv@PLT
	movq	%r15, %rdi
	movsd	%xmm0, -88(%rbp)
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	movq	%r14, %rdi
	movsd	%xmm0, -80(%rbp)
	call	_ZNK2v88internal8compiler4Type3MaxEv@PLT
	movq	%r14, %rdi
	movsd	%xmm0, -72(%rbp)
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	movsd	-88(%rbp), %xmm3
	movsd	-80(%rbp), %xmm2
	movq	%rbx, %rdi
	movsd	-72(%rbp), %xmm1
	call	_ZN2v88internal8compiler14OperationTyper14SubtractRangerEdddd
	movq	%rax, %rdi
.L514:
	testb	%r12b, %r12b
	jne	.L526
.L524:
	testb	%r13b, %r13b
	jne	.L527
.L525:
	addq	$56, %rsp
	movq	%rdi, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L528:
	.cfi_restore_state
	movl	$1, %edi
	jmp	.L514
	.p2align 4,,10
	.p2align 3
.L520:
	movq	16(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	je	.L518
	movq	16(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	je	.L518
.L523:
	movl	$1119, %edi
	testb	%r12b, %r12b
	je	.L527
	movl	%r12d, %r13d
	movl	$1119, %edi
	.p2align 4,,10
	.p2align 3
.L526:
	movq	(%rbx), %rdx
	movl	$2049, %esi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, %rdi
	jmp	.L524
	.p2align 4,,10
	.p2align 3
.L550:
	movq	8(%rbx), %rax
	movq	(%rbx), %rdx
	movq	-64(%rbp), %rdi
	movq	208(%rax), %rsi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -64(%rbp)
	jmp	.L513
	.p2align 4,,10
	.p2align 3
.L549:
	movq	8(%rbx), %rax
	movq	(%rbx), %rdx
	movq	-56(%rbp), %rdi
	movq	208(%rax), %rsi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%r15, %rdi
	movq	%rax, -56(%rbp)
	movq	8(%rbx), %rax
	movq	208(%rax), %rsi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	movl	%eax, %r12d
	jmp	.L512
	.p2align 4,,10
	.p2align 3
.L548:
	movl	$4097, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	movl	%eax, %r13d
	jmp	.L511
	.p2align 4,,10
	.p2align 3
.L527:
	movq	(%rbx), %rdx
	movl	$4097, %esi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, %rdi
	jmp	.L525
	.p2align 4,,10
	.p2align 3
.L518:
	movq	24(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L551
.L522:
	movl	$1119, %edi
	jmp	.L514
	.p2align 4,,10
	.p2align 3
.L551:
	movq	24(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L523
	jmp	.L522
	.cfi_endproc
.LFE18753:
	.size	_ZN2v88internal8compiler14OperationTyper14NumberSubtractENS1_4TypeES3_, .-_ZN2v88internal8compiler14OperationTyper14NumberSubtractENS1_4TypeES3_
	.section	.text._ZN2v88internal8compiler14OperationTyper25SpeculativeSafeIntegerAddENS1_4TypeES3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper25SpeculativeSafeIntegerAddENS1_4TypeES3_
	.type	_ZN2v88internal8compiler14OperationTyper25SpeculativeSafeIntegerAddENS1_4TypeES3_, @function
_ZN2v88internal8compiler14OperationTyper25SpeculativeSafeIntegerAddENS1_4TypeES3_:
.LFB18754:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movl	$8396767, %esi
	subq	$8, %rsp
	movq	(%rbx), %rdx
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14OperationTyper8ToNumberENS1_4TypeE
	movq	(%rbx), %rdx
	movq	%r13, %rdi
	movl	$8396767, %esi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14OperationTyper8ToNumberENS1_4TypeE
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14OperationTyper9NumberAddENS1_4TypeES3_
	movq	(%rbx), %rdx
	movq	%rax, %rdi
	movq	8(%rbx), %rax
	movq	400(%rax), %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	.cfi_endproc
.LFE18754:
	.size	_ZN2v88internal8compiler14OperationTyper25SpeculativeSafeIntegerAddENS1_4TypeES3_, .-_ZN2v88internal8compiler14OperationTyper25SpeculativeSafeIntegerAddENS1_4TypeES3_
	.section	.text._ZN2v88internal8compiler14OperationTyper30SpeculativeSafeIntegerSubtractENS1_4TypeES3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper30SpeculativeSafeIntegerSubtractENS1_4TypeES3_
	.type	_ZN2v88internal8compiler14OperationTyper30SpeculativeSafeIntegerSubtractENS1_4TypeES3_, @function
_ZN2v88internal8compiler14OperationTyper30SpeculativeSafeIntegerSubtractENS1_4TypeES3_:
.LFB18755:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movl	$8396767, %esi
	subq	$8, %rsp
	movq	(%rbx), %rdx
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14OperationTyper8ToNumberENS1_4TypeE
	movq	(%rbx), %rdx
	movq	%r13, %rdi
	movl	$8396767, %esi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14OperationTyper8ToNumberENS1_4TypeE
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14OperationTyper14NumberSubtractENS1_4TypeES3_
	movq	(%rbx), %rdx
	movq	%rax, %rdi
	movq	8(%rbx), %rax
	movq	400(%rax), %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	.cfi_endproc
.LFE18755:
	.size	_ZN2v88internal8compiler14OperationTyper30SpeculativeSafeIntegerSubtractENS1_4TypeES3_, .-_ZN2v88internal8compiler14OperationTyper30SpeculativeSafeIntegerSubtractENS1_4TypeES3_
	.section	.text._ZN2v88internal8compiler14OperationTyper14NumberMultiplyENS1_4TypeES3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper14NumberMultiplyENS1_4TypeES3_
	.type	_ZN2v88internal8compiler14OperationTyper14NumberMultiplyENS1_4TypeES3_, @function
_ZN2v88internal8compiler14OperationTyper14NumberMultiplyENS1_4TypeES3_:
.LFB18756:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -56(%rbp)
	movq	%rdx, -64(%rbp)
	cmpq	$1, %rsi
	je	.L557
	cmpq	$1, %rdx
	je	.L557
	cmpq	$4097, %rsi
	je	.L560
	leaq	-56(%rbp), %r14
	movq	%rdi, %rbx
	movl	$4097, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L560
	cmpq	$4097, -64(%rbp)
	je	.L560
	leaq	-64(%rbp), %r15
	movl	$4097, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L560
	movl	$4097, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	je	.L561
.L563:
	movl	$1, %r12d
.L562:
	movq	(%rbx), %rdx
	movq	-56(%rbp), %rdi
	movl	$3167, %esi
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	-64(%rbp), %rdi
	movq	(%rbx), %rdx
	movl	$3167, %esi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movl	$2049, %esi
	movq	%r14, %rdi
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	je	.L570
.L572:
	movl	$1, %r13d
.L571:
	movl	$2049, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L607
.L576:
	movl	$2049, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L608
.L577:
	movq	8(%rbx), %rax
	movq	320(%rax), %rsi
	cmpq	%rsi, -56(%rbp)
	je	.L578
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L583
	movq	8(%rbx), %rax
	movq	320(%rax), %rsi
.L578:
	cmpq	-64(%rbp), %rsi
	je	.L582
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L582
.L583:
	movl	$3167, %eax
.L580:
	testb	%r13b, %r13b
	jne	.L609
.L584:
	testb	%r12b, %r12b
	je	.L559
	movq	(%rbx), %rdx
	movl	$4097, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	jmp	.L559
	.p2align 4,,10
	.p2align 3
.L557:
	movl	$1, %eax
.L559:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L560:
	.cfi_restore_state
	addq	$56, %rsp
	movl	$4097, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L582:
	.cfi_restore_state
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type3MaxEv@PLT
	movq	%r15, %rdi
	movsd	%xmm0, -88(%rbp)
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	movq	%r14, %rdi
	movsd	%xmm0, -80(%rbp)
	call	_ZNK2v88internal8compiler4Type3MaxEv@PLT
	movq	%r14, %rdi
	movsd	%xmm0, -72(%rbp)
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	movsd	-88(%rbp), %xmm3
	movsd	-80(%rbp), %xmm2
	movq	%rbx, %rdi
	movsd	-72(%rbp), %xmm1
	call	_ZN2v88internal8compiler14OperationTyper14MultiplyRangerEdddd
	jmp	.L580
.L608:
	movq	8(%rbx), %rax
	movq	(%rbx), %rdx
	movq	-64(%rbp), %rdi
	movq	208(%rax), %rsi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	(%rbx), %rdx
	movl	$1119, %esi
	movq	%rax, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -64(%rbp)
	jmp	.L577
.L607:
	movq	8(%rbx), %rax
	movq	(%rbx), %rdx
	movq	-56(%rbp), %rdi
	movq	208(%rax), %rsi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	(%rbx), %rdx
	movl	$1119, %esi
	movq	%rax, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -56(%rbp)
	jmp	.L576
.L570:
	movl	$2049, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L572
	movq	8(%rbx), %rax
	movq	%r14, %rdi
	movq	312(%rax), %rsi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	je	.L575
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	pxor	%xmm1, %xmm1
	comisd	%xmm0, %xmm1
	ja	.L572
.L575:
	movq	8(%rbx), %rax
	movq	%r15, %rdi
	movq	312(%rax), %rsi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	movl	%eax, %r13d
	testb	%al, %al
	je	.L571
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	pxor	%xmm1, %xmm1
	comisd	%xmm0, %xmm1
	seta	%r13b
	jmp	.L571
	.p2align 4,,10
	.p2align 3
.L561:
	movl	$4097, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L563
	movq	8(%rbx), %rax
	movq	%r14, %rdi
	movq	312(%rax), %rsi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	je	.L568
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	ucomisd	.LC1(%rip), %xmm0
	jp	.L586
	je	.L563
.L586:
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type3MaxEv@PLT
	ucomisd	.LC0(%rip), %xmm0
	jp	.L568
	je	.L563
.L568:
	movq	8(%rbx), %rax
	movq	%r15, %rdi
	movq	312(%rax), %rsi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	movl	%eax, %r12d
	testb	%al, %al
	je	.L562
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	ucomisd	.LC1(%rip), %xmm0
	jp	.L587
	je	.L563
.L587:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type3MaxEv@PLT
	ucomisd	.LC0(%rip), %xmm0
	movl	$0, %eax
	setnp	%r12b
	cmovne	%eax, %r12d
	jmp	.L562
	.p2align 4,,10
	.p2align 3
.L609:
	movq	(%rbx), %rdx
	movl	$2049, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	jmp	.L584
	.cfi_endproc
.LFE18756:
	.size	_ZN2v88internal8compiler14OperationTyper14NumberMultiplyENS1_4TypeES3_, .-_ZN2v88internal8compiler14OperationTyper14NumberMultiplyENS1_4TypeES3_
	.section	.text._ZN2v88internal8compiler14OperationTyper12NumberDivideENS1_4TypeES3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper12NumberDivideENS1_4TypeES3_
	.type	_ZN2v88internal8compiler14OperationTyper12NumberDivideENS1_4TypeES3_, @function
_ZN2v88internal8compiler14OperationTyper12NumberDivideENS1_4TypeES3_:
.LFB18757:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, -40(%rbp)
	movq	%rdx, -48(%rbp)
	cmpq	$1, %rsi
	je	.L611
	cmpq	$1, %rdx
	je	.L611
	cmpq	$4097, %rsi
	je	.L614
	leaq	-40(%rbp), %r12
	movq	%rdi, %rbx
	movl	$4097, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L614
	cmpq	$4097, -48(%rbp)
	je	.L614
	leaq	-48(%rbp), %r14
	movl	$4097, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L614
	movl	$4097, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	je	.L615
.L617:
	movl	$1, %r13d
.L616:
	movq	(%rbx), %rdx
	movq	-40(%rbp), %rdi
	movl	$3167, %esi
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	(%rbx), %rdx
	movq	-48(%rbp), %rdi
	movl	$3167, %esi
	movq	%rax, -40(%rbp)
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -48(%rbp)
	movq	8(%rbx), %rax
	movq	320(%rax), %rsi
	cmpq	-40(%rbp), %rsi
	je	.L624
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L628
	movq	8(%rbx), %rax
.L624:
	movq	312(%rax), %rsi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	je	.L631
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	pxor	%xmm1, %xmm1
	comisd	%xmm0, %xmm1
	ja	.L628
.L631:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	ucomisd	.LC1(%rip), %xmm0
	jp	.L630
	jne	.L630
.L628:
	movq	(%rbx), %rdx
	movl	$2049, %esi
	movl	$1119, %edi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
.L626:
	testb	%r13b, %r13b
	je	.L613
	movq	(%rbx), %rdx
	movl	$4097, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	jmp	.L613
	.p2align 4,,10
	.p2align 3
.L611:
	movl	$1, %eax
.L613:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L614:
	.cfi_restore_state
	addq	$16, %rsp
	movl	$4097, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L615:
	.cfi_restore_state
	movq	8(%rbx), %rax
	movq	%r14, %rdi
	movq	312(%rax), %rsi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	movl	%eax, %r13d
	testb	%al, %al
	jne	.L617
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	ucomisd	.LC1(%rip), %xmm0
	jp	.L618
	jne	.L618
.L622:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	ucomisd	.LC1(%rip), %xmm0
	jp	.L621
	je	.L617
.L621:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type3MaxEv@PLT
	ucomisd	.LC0(%rip), %xmm0
	movl	$0, %eax
	setnp	%r13b
	cmovne	%eax, %r13d
	jmp	.L616
.L630:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type3MaxEv@PLT
	ucomisd	.LC0(%rip), %xmm0
	jp	.L634
	je	.L628
.L634:
	movl	$1119, %eax
	jmp	.L626
.L618:
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type3MaxEv@PLT
	ucomisd	.LC0(%rip), %xmm0
	jp	.L616
	je	.L622
	jmp	.L616
	.cfi_endproc
.LFE18757:
	.size	_ZN2v88internal8compiler14OperationTyper12NumberDivideENS1_4TypeES3_, .-_ZN2v88internal8compiler14OperationTyper12NumberDivideENS1_4TypeES3_
	.section	.text._ZN2v88internal8compiler14OperationTyper13NumberModulusENS1_4TypeES3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper13NumberModulusENS1_4TypeES3_
	.type	_ZN2v88internal8compiler14OperationTyper13NumberModulusENS1_4TypeES3_, @function
_ZN2v88internal8compiler14OperationTyper13NumberModulusENS1_4TypeES3_:
.LFB18758:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -56(%rbp)
	movq	%rdx, -64(%rbp)
	cmpq	$1, %rsi
	je	.L646
	cmpq	$1, %rdx
	jne	.L647
.L646:
	addq	$56, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L647:
	.cfi_restore_state
	leaq	-56(%rbp), %r13
	movq	%rdi, %rbx
	movl	$4097, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	je	.L649
	leaq	-64(%rbp), %r14
.L651:
	movl	$1, %r15d
.L650:
	movl	$2049, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	movl	%eax, %r12d
	testb	%al, %al
	jne	.L694
.L653:
	movl	$2049, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L695
.L654:
	movq	(%rbx), %rdx
	movq	-56(%rbp), %rdi
	movl	$1119, %esi
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	(%rbx), %rdx
	movq	-64(%rbp), %rdi
	movl	$1119, %esi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	-56(%rbp), %r8
	movq	%rax, -64(%rbp)
	cmpq	$1, %r8
	je	.L655
	movq	8(%rbx), %rax
	movl	$1, %r8d
	movq	208(%rax), %rsi
	cmpq	%rsi, -64(%rbp)
	jne	.L696
.L655:
	testb	%r12b, %r12b
	jne	.L697
.L667:
	testb	%r15b, %r15b
	jne	.L698
.L668:
	addq	$56, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L696:
	.cfi_restore_state
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	movq	-72(%rbp), %r8
	testb	%al, %al
	jne	.L655
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	movq	%r13, %rdi
	movsd	%xmm0, -72(%rbp)
	call	_ZNK2v88internal8compiler4Type3MaxEv@PLT
	movq	%r14, %rdi
	movsd	%xmm0, -80(%rbp)
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	movq	%r14, %rdi
	movsd	%xmm0, -88(%rbp)
	call	_ZNK2v88internal8compiler4Type3MaxEv@PLT
	pxor	%xmm5, %xmm5
	ucomisd	-72(%rbp), %xmm5
	movl	$1, %eax
	movsd	%xmm0, -96(%rbp)
	cmova	%eax, %r12d
	movq	8(%rbx), %rax
	movq	320(%rax), %rsi
	cmpq	%rsi, -56(%rbp)
	jne	.L699
.L658:
	cmpq	%rsi, -64(%rbp)
	je	.L662
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	movl	$1119, %r8d
	testb	%al, %al
	je	.L655
.L662:
	movsd	-72(%rbp), %xmm4
	movq	.LC4(%rip), %xmm0
	movsd	-80(%rbp), %xmm2
	movsd	-88(%rbp), %xmm6
	movapd	%xmm4, %xmm1
	andpd	%xmm0, %xmm1
	andpd	%xmm0, %xmm2
	andpd	%xmm0, %xmm6
	maxsd	%xmm1, %xmm2
	movsd	-96(%rbp), %xmm1
	andpd	%xmm0, %xmm1
	maxsd	%xmm6, %xmm1
	subsd	.LC5(%rip), %xmm1
	minsd	%xmm2, %xmm1
	pxor	%xmm2, %xmm2
	comisd	%xmm2, %xmm4
	jnb	.L665
	movsd	-80(%rbp), %xmm0
	pxor	%xmm7, %xmm7
	subsd	%xmm1, %xmm2
	cmpnlesd	%xmm7, %xmm0
	andpd	%xmm0, %xmm1
.L665:
	movq	(%rbx), %rdi
	movapd	%xmm2, %xmm0
	call	_ZN2v88internal8compiler4Type5RangeEddPNS0_4ZoneE@PLT
	movq	%rax, %r8
	jmp	.L655
	.p2align 4,,10
	.p2align 3
.L695:
	movq	8(%rbx), %rax
	movq	(%rbx), %rdx
	movq	-64(%rbp), %rdi
	movq	208(%rax), %rsi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -64(%rbp)
	jmp	.L654
	.p2align 4,,10
	.p2align 3
.L694:
	movq	8(%rbx), %rax
	movq	(%rbx), %rdx
	movq	-56(%rbp), %rdi
	movq	208(%rax), %rsi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -56(%rbp)
	jmp	.L653
	.p2align 4,,10
	.p2align 3
.L649:
	movq	8(%rbx), %rax
	leaq	-64(%rbp), %r14
	movq	%r14, %rdi
	movq	312(%rax), %rsi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L651
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	ucomisd	.LC1(%rip), %xmm0
	jp	.L673
	je	.L651
	.p2align 4,,10
	.p2align 3
.L673:
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type3MaxEv@PLT
	ucomisd	.LC0(%rip), %xmm0
	movl	$0, %eax
	setnp	%r15b
	cmovne	%eax, %r15d
	jmp	.L650
	.p2align 4,,10
	.p2align 3
.L698:
	movq	(%rbx), %rdx
	movq	%r8, %rdi
	movl	$4097, %esi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, %r8
	jmp	.L668
	.p2align 4,,10
	.p2align 3
.L697:
	movq	(%rbx), %rdx
	movq	%r8, %rdi
	movl	$2049, %esi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, %r8
	jmp	.L667
	.p2align 4,,10
	.p2align 3
.L699:
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	movl	$1119, %r8d
	testb	%al, %al
	je	.L655
	movq	8(%rbx), %rax
	movq	320(%rax), %rsi
	jmp	.L658
	.cfi_endproc
.LFE18758:
	.size	_ZN2v88internal8compiler14OperationTyper13NumberModulusENS1_4TypeES3_, .-_ZN2v88internal8compiler14OperationTyper13NumberModulusENS1_4TypeES3_
	.section	.text._ZN2v88internal8compiler14OperationTyper15NumberBitwiseOrENS1_4TypeES3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper15NumberBitwiseOrENS1_4TypeES3_
	.type	_ZN2v88internal8compiler14OperationTyper15NumberBitwiseOrENS1_4TypeES3_, @function
_ZN2v88internal8compiler14OperationTyper15NumberBitwiseOrENS1_4TypeES3_:
.LFB18760:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%rsi, -56(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -40(%rbp)
	xorl	%ecx, %ecx
	movq	%rsi, -48(%rbp)
	cmpq	$1099, %rsi
	jne	.L771
.L705:
	movq	-64(%rbp), %rdx
	movq	%rax, -56(%rbp)
	movq	%rdx, -48(%rbp)
	cmpq	$1099, %rdx
	jne	.L772
.L770:
	cmpq	$1, %rax
	jne	.L716
.L715:
	movl	$1, %eax
.L720:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L773
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L717:
	.cfi_restore_state
	movq	72(%rbx), %rsi
	cmpq	-48(%rbp), %rsi
	je	.L718
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L718
	cmpq	$1, -56(%rbp)
	movq	$1099, -64(%rbp)
	je	.L715
	.p2align 4,,10
	.p2align 3
.L716:
	leaq	-56(%rbp), %r13
	leaq	-64(%rbp), %r12
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	movq	%r12, %rdi
	movsd	%xmm0, -88(%rbp)
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	movq	%r13, %rdi
	movsd	%xmm0, -80(%rbp)
	call	_ZNK2v88internal8compiler4Type3MaxEv@PLT
	movq	%r12, %rdi
	movsd	%xmm0, -72(%rbp)
	call	_ZNK2v88internal8compiler4Type3MaxEv@PLT
	movsd	-88(%rbp), %xmm3
	pxor	%xmm2, %xmm2
	movsd	-80(%rbp), %xmm4
	movsd	-72(%rbp), %xmm5
	comisd	%xmm2, %xmm3
	movapd	%xmm4, %xmm6
	jb	.L721
	comisd	%xmm2, %xmm4
	jb	.L721
	maxsd	%xmm3, %xmm6
	jmp	.L724
	.p2align 4,,10
	.p2align 3
.L721:
	minsd	%xmm3, %xmm6
.L724:
	ucomisd	%xmm2, %xmm0
	movl	$0, %edx
	setnp	%al
	cmovne	%edx, %eax
	testb	%al, %al
	je	.L736
	ucomisd	%xmm2, %xmm4
	setnp	%al
	cmove	%eax, %edx
	testb	%dl, %dl
	jne	.L774
.L736:
	movsd	.LC7(%rip), %xmm1
.L725:
	ucomisd	%xmm2, %xmm3
	movl	$0, %edx
	setnp	%al
	cmovne	%edx, %eax
	testb	%al, %al
	je	.L726
	ucomisd	%xmm2, %xmm5
	setnp	%al
	cmove	%eax, %edx
	testb	%dl, %dl
	jne	.L775
.L726:
	comisd	%xmm5, %xmm2
	ja	.L727
	comisd	%xmm0, %xmm2
	jbe	.L728
.L727:
	movsd	.LC6(%rip), %xmm0
	minsd	%xmm1, %xmm0
	movapd	%xmm0, %xmm1
.L728:
	movq	(%rbx), %rdi
	movapd	%xmm6, %xmm0
	call	_ZN2v88internal8compiler4Type5RangeEddPNS0_4ZoneE@PLT
	jmp	.L720
	.p2align 4,,10
	.p2align 3
.L771:
	leaq	-48(%rbp), %r12
	movl	$1099, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L702
	movq	-48(%rbp), %rax
	jmp	.L705
	.p2align 4,,10
	.p2align 3
.L772:
	leaq	-48(%rbp), %r12
.L732:
	movl	$1099, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L711
	movq	-48(%rbp), %rax
	movq	-56(%rbp), %rdx
.L712:
	movq	%rax, -64(%rbp)
	cmpq	$1, %rdx
	jne	.L770
	jmp	.L715
	.p2align 4,,10
	.p2align 3
.L702:
	movq	8(%rbx), %rax
	movq	312(%rax), %rsi
	cmpq	-48(%rbp), %rsi
	jne	.L776
.L703:
	movq	208(%rax), %rax
	jmp	.L705
	.p2align 4,,10
	.p2align 3
.L711:
	movq	8(%rbx), %rax
	movq	312(%rax), %rsi
	cmpq	-48(%rbp), %rsi
	jne	.L777
.L713:
	movq	208(%rax), %rax
	movq	-56(%rbp), %rdx
	jmp	.L712
	.p2align 4,,10
	.p2align 3
.L776:
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L706
	movq	8(%rbx), %rax
	jmp	.L703
	.p2align 4,,10
	.p2align 3
.L777:
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L717
	movq	8(%rbx), %rax
	jmp	.L713
	.p2align 4,,10
	.p2align 3
.L775:
	movapd	%xmm0, %xmm1
	movapd	%xmm4, %xmm6
	jmp	.L726
	.p2align 4,,10
	.p2align 3
.L774:
	movapd	%xmm5, %xmm1
	movapd	%xmm3, %xmm6
	jmp	.L725
	.p2align 4,,10
	.p2align 3
.L706:
	movq	72(%rbx), %rsi
	cmpq	-48(%rbp), %rsi
	jne	.L707
.L708:
	movq	8(%rbx), %rax
	movq	(%rbx), %r12
	movq	-48(%rbp), %rdi
	movq	208(%rax), %rsi
	movq	%r12, %rdx
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%r12, %rdx
	movl	$1099, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	jmp	.L705
	.p2align 4,,10
	.p2align 3
.L718:
	movq	8(%rbx), %rax
	movq	(%rbx), %r12
	movq	-48(%rbp), %rdi
	movq	208(%rax), %rsi
	movq	%r12, %rdx
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%r12, %rdx
	movl	$1099, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	-56(%rbp), %rdx
	jmp	.L712
	.p2align 4,,10
	.p2align 3
.L707:
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L708
	movq	-64(%rbp), %rax
	movq	$1099, -56(%rbp)
	movq	%rax, -48(%rbp)
	cmpq	$1099, %rax
	je	.L716
	jmp	.L732
.L773:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18760:
	.size	_ZN2v88internal8compiler14OperationTyper15NumberBitwiseOrENS1_4TypeES3_, .-_ZN2v88internal8compiler14OperationTyper15NumberBitwiseOrENS1_4TypeES3_
	.section	.text._ZN2v88internal8compiler14OperationTyper16NumberBitwiseAndENS1_4TypeES3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper16NumberBitwiseAndENS1_4TypeES3_
	.type	_ZN2v88internal8compiler14OperationTyper16NumberBitwiseAndENS1_4TypeES3_, @function
_ZN2v88internal8compiler14OperationTyper16NumberBitwiseAndENS1_4TypeES3_:
.LFB18762:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%rsi, -56(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -40(%rbp)
	xorl	%ecx, %ecx
	movq	%rsi, -48(%rbp)
	cmpq	$1099, %rsi
	jne	.L845
.L783:
	movq	-64(%rbp), %rdx
	movq	%rax, -56(%rbp)
	movq	%rdx, -48(%rbp)
	cmpq	$1099, %rdx
	jne	.L846
.L843:
	cmpq	$1, %rax
	jne	.L794
.L793:
	movl	$1, %eax
.L798:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L847
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L795:
	.cfi_restore_state
	movq	72(%rbx), %rsi
	cmpq	-48(%rbp), %rsi
	je	.L796
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L796
	cmpq	$1, -56(%rbp)
	movq	$1099, -64(%rbp)
	je	.L793
	.p2align 4,,10
	.p2align 3
.L794:
	leaq	-56(%rbp), %r13
	leaq	-64(%rbp), %r12
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	movq	%r12, %rdi
	movsd	%xmm0, -88(%rbp)
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	movq	%r13, %rdi
	movsd	%xmm0, -80(%rbp)
	call	_ZNK2v88internal8compiler4Type3MaxEv@PLT
	movq	%r12, %rdi
	movsd	%xmm0, -72(%rbp)
	call	_ZNK2v88internal8compiler4Type3MaxEv@PLT
	movsd	-88(%rbp), %xmm3
	pxor	%xmm2, %xmm2
	movsd	-72(%rbp), %xmm1
	movsd	-80(%rbp), %xmm4
	comisd	%xmm2, %xmm3
	jb	.L837
	comisd	%xmm2, %xmm4
	jb	.L838
.L844:
	minsd	%xmm1, %xmm0
	movapd	%xmm0, %xmm1
.L803:
	movapd	%xmm2, %xmm5
.L807:
	movq	(%rbx), %rdi
	movapd	%xmm5, %xmm0
	call	_ZN2v88internal8compiler4Type5RangeEddPNS0_4ZoneE@PLT
	jmp	.L798
	.p2align 4,,10
	.p2align 3
.L845:
	leaq	-48(%rbp), %r12
	movl	$1099, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L780
	movq	-48(%rbp), %rax
	jmp	.L783
	.p2align 4,,10
	.p2align 3
.L846:
	leaq	-48(%rbp), %r12
.L812:
	movl	$1099, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L789
	movq	-48(%rbp), %rax
	movq	-56(%rbp), %rdx
.L790:
	movq	%rax, -64(%rbp)
	cmpq	$1, %rdx
	jne	.L843
	jmp	.L793
	.p2align 4,,10
	.p2align 3
.L780:
	movq	8(%rbx), %rax
	movq	312(%rax), %rsi
	cmpq	-48(%rbp), %rsi
	jne	.L848
.L781:
	movq	208(%rax), %rax
	jmp	.L783
	.p2align 4,,10
	.p2align 3
.L789:
	movq	8(%rbx), %rax
	movq	312(%rax), %rsi
	cmpq	-48(%rbp), %rsi
	jne	.L849
.L791:
	movq	208(%rax), %rax
	movq	-56(%rbp), %rdx
	jmp	.L790
	.p2align 4,,10
	.p2align 3
.L837:
	comisd	%xmm1, %xmm0
	ja	.L850
.L806:
	comisd	%xmm2, %xmm3
	movapd	%xmm2, %xmm5
	jnb	.L809
	movsd	.LC8(%rip), %xmm5
.L809:
	comisd	%xmm2, %xmm4
	jb	.L807
	jmp	.L844
	.p2align 4,,10
	.p2align 3
.L848:
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L784
	movq	8(%rbx), %rax
	jmp	.L781
	.p2align 4,,10
	.p2align 3
.L849:
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L795
	movq	8(%rbx), %rax
	jmp	.L791
	.p2align 4,,10
	.p2align 3
.L838:
	comisd	%xmm1, %xmm0
	movapd	%xmm2, %xmm5
	jbe	.L806
	jmp	.L809
	.p2align 4,,10
	.p2align 3
.L784:
	movq	72(%rbx), %rsi
	cmpq	-48(%rbp), %rsi
	jne	.L785
.L786:
	movq	8(%rbx), %rax
	movq	(%rbx), %r12
	movq	-48(%rbp), %rdi
	movq	208(%rax), %rsi
	movq	%r12, %rdx
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%r12, %rdx
	movl	$1099, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	jmp	.L783
	.p2align 4,,10
	.p2align 3
.L796:
	movq	8(%rbx), %rax
	movq	(%rbx), %r12
	movq	-48(%rbp), %rdi
	movq	208(%rax), %rsi
	movq	%r12, %rdx
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%r12, %rdx
	movl	$1099, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	-56(%rbp), %rdx
	jmp	.L790
	.p2align 4,,10
	.p2align 3
.L785:
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L786
	movq	-64(%rbp), %rax
	movq	$1099, -56(%rbp)
	movq	%rax, -48(%rbp)
	cmpq	$1099, %rax
	je	.L794
	jmp	.L812
	.p2align 4,,10
	.p2align 3
.L850:
	comisd	%xmm2, %xmm3
	jnb	.L851
	comisd	%xmm2, %xmm4
	movsd	.LC8(%rip), %xmm5
	movapd	%xmm0, %xmm1
	jnb	.L803
	jmp	.L807
.L847:
	call	__stack_chk_fail@PLT
.L851:
	movapd	%xmm2, %xmm5
	jmp	.L809
	.cfi_endproc
.LFE18762:
	.size	_ZN2v88internal8compiler14OperationTyper16NumberBitwiseAndENS1_4TypeES3_, .-_ZN2v88internal8compiler14OperationTyper16NumberBitwiseAndENS1_4TypeES3_
	.section	.text._ZN2v88internal8compiler14OperationTyper16NumberBitwiseXorENS1_4TypeES3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper16NumberBitwiseXorENS1_4TypeES3_
	.type	_ZN2v88internal8compiler14OperationTyper16NumberBitwiseXorENS1_4TypeES3_, @function
_ZN2v88internal8compiler14OperationTyper16NumberBitwiseXorENS1_4TypeES3_:
.LFB18763:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%rsi, -56(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -40(%rbp)
	xorl	%ecx, %ecx
	movq	%rsi, -48(%rbp)
	cmpq	$1099, %rsi
	jne	.L913
.L857:
	movq	-64(%rbp), %rdx
	movq	%rax, -56(%rbp)
	movq	%rdx, -48(%rbp)
	cmpq	$1099, %rdx
	jne	.L914
.L912:
	cmpq	$1, %rax
	jne	.L868
.L867:
	movl	$1, %eax
.L872:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L915
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L869:
	.cfi_restore_state
	movq	72(%rbx), %rsi
	cmpq	-48(%rbp), %rsi
	je	.L870
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L870
	cmpq	$1, -56(%rbp)
	movq	$1099, -64(%rbp)
	je	.L867
	.p2align 4,,10
	.p2align 3
.L868:
	leaq	-56(%rbp), %r13
	leaq	-64(%rbp), %r12
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	movq	%r12, %rdi
	movsd	%xmm0, -72(%rbp)
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	movq	%r13, %rdi
	movsd	%xmm0, -80(%rbp)
	call	_ZNK2v88internal8compiler4Type3MaxEv@PLT
	movq	%r12, %rdi
	movsd	%xmm0, -88(%rbp)
	call	_ZNK2v88internal8compiler4Type3MaxEv@PLT
	movsd	-72(%rbp), %xmm1
	pxor	%xmm2, %xmm2
	comisd	%xmm2, %xmm1
	jb	.L906
	movsd	-80(%rbp), %xmm3
	comisd	%xmm2, %xmm3
	jb	.L916
.L875:
	movl	$1027, %eax
	jmp	.L872
	.p2align 4,,10
	.p2align 3
.L913:
	leaq	-48(%rbp), %r12
	movl	$1099, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L854
	movq	-48(%rbp), %rax
	jmp	.L857
	.p2align 4,,10
	.p2align 3
.L914:
	leaq	-48(%rbp), %r12
.L888:
	movl	$1099, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L863
	movq	-48(%rbp), %rax
	movq	-56(%rbp), %rdx
.L864:
	movq	%rax, -64(%rbp)
	cmpq	$1, %rdx
	jne	.L912
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L854:
	movq	8(%rbx), %rax
	movq	312(%rax), %rsi
	cmpq	-48(%rbp), %rsi
	jne	.L917
.L855:
	movq	208(%rax), %rax
	jmp	.L857
	.p2align 4,,10
	.p2align 3
.L863:
	movq	8(%rbx), %rax
	movq	312(%rax), %rsi
	cmpq	-48(%rbp), %rsi
	jne	.L918
.L865:
	movq	208(%rax), %rax
	movq	-56(%rbp), %rdx
	jmp	.L864
	.p2align 4,,10
	.p2align 3
.L906:
	comisd	-88(%rbp), %xmm2
	ja	.L885
.L877:
	comisd	%xmm2, %xmm1
	jnb	.L886
.L882:
	movl	$1099, %eax
	jmp	.L872
	.p2align 4,,10
	.p2align 3
.L918:
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L869
	movq	8(%rbx), %rax
	jmp	.L865
	.p2align 4,,10
	.p2align 3
.L917:
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L858
	movq	8(%rbx), %rax
	jmp	.L855
	.p2align 4,,10
	.p2align 3
.L916:
	comisd	-88(%rbp), %xmm2
	jbe	.L886
.L885:
	comisd	%xmm0, %xmm2
	ja	.L875
	movsd	-80(%rbp), %xmm6
	comisd	%xmm2, %xmm6
	jb	.L877
.L881:
	movl	$73, %eax
	jmp	.L872
	.p2align 4,,10
	.p2align 3
.L886:
	comisd	%xmm0, %xmm2
	jbe	.L882
	jmp	.L881
	.p2align 4,,10
	.p2align 3
.L858:
	movq	72(%rbx), %rsi
	cmpq	-48(%rbp), %rsi
	jne	.L859
.L860:
	movq	8(%rbx), %rax
	movq	(%rbx), %r12
	movq	-48(%rbp), %rdi
	movq	208(%rax), %rsi
	movq	%r12, %rdx
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%r12, %rdx
	movl	$1099, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	jmp	.L857
	.p2align 4,,10
	.p2align 3
.L870:
	movq	8(%rbx), %rax
	movq	(%rbx), %r12
	movq	-48(%rbp), %rdi
	movq	208(%rax), %rsi
	movq	%r12, %rdx
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%r12, %rdx
	movl	$1099, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	-56(%rbp), %rdx
	jmp	.L864
	.p2align 4,,10
	.p2align 3
.L859:
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L860
	movq	-64(%rbp), %rax
	movq	$1099, -56(%rbp)
	movq	%rax, -48(%rbp)
	cmpq	$1099, %rax
	je	.L868
	jmp	.L888
.L915:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18763:
	.size	_ZN2v88internal8compiler14OperationTyper16NumberBitwiseXorENS1_4TypeES3_, .-_ZN2v88internal8compiler14OperationTyper16NumberBitwiseXorENS1_4TypeES3_
	.section	.text._ZN2v88internal8compiler14OperationTyper15NumberShiftLeftENS1_4TypeES3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper15NumberShiftLeftENS1_4TypeES3_
	.type	_ZN2v88internal8compiler14OperationTyper15NumberShiftLeftENS1_4TypeES3_, @function
_ZN2v88internal8compiler14OperationTyper15NumberShiftLeftENS1_4TypeES3_:
.LFB18764:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%rsi, -56(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -40(%rbp)
	xorl	%ecx, %ecx
	movq	%rsi, -48(%rbp)
	cmpq	$1099, %rsi
	jne	.L971
.L924:
	movq	-64(%rbp), %rdx
	movq	%rax, -56(%rbp)
	movq	%rdx, -48(%rbp)
	cmpq	$1031, %rdx
	jne	.L972
.L970:
	cmpq	$1, %rax
	jne	.L935
.L934:
	movl	$1, %eax
.L939:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L973
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L936:
	.cfi_restore_state
	movq	80(%rbx), %rsi
	cmpq	-48(%rbp), %rsi
	je	.L937
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L937
	cmpq	$1, -56(%rbp)
	movq	$1031, -64(%rbp)
	je	.L934
	.p2align 4,,10
	.p2align 3
.L935:
	leaq	-56(%rbp), %r12
	leaq	-64(%rbp), %r13
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	movq	%r12, %rdi
	movsd	%xmm0, -72(%rbp)
	call	_ZNK2v88internal8compiler4Type3MaxEv@PLT
	movq	%r13, %rdi
	cvttsd2sil	%xmm0, %r12d
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	movq	%r13, %rdi
	movsd	%xmm0, -80(%rbp)
	call	_ZNK2v88internal8compiler4Type3MaxEv@PLT
	cvttsd2siq	%xmm0, %rcx
	movl	%ecx, %esi
	cmpl	$31, %ecx
	ja	.L947
	cvttsd2siq	-80(%rbp), %rdx
	movl	$2147483647, %eax
	sarl	%cl, %eax
.L940:
	cmpl	%r12d, %eax
	jl	.L941
	cvttsd2sil	-72(%rbp), %eax
	movl	$-2147483648, %edi
	movl	%esi, %ecx
	sarl	%cl, %edi
	cmpl	%eax, %edi
	jg	.L941
	movl	%eax, %edi
	sall	%cl, %edi
	movl	%edx, %ecx
	sall	%cl, %eax
	movl	%esi, %ecx
	cmpl	%eax, %edi
	cmovle	%edi, %eax
	movl	%r12d, %edi
	sall	%cl, %edi
	movl	%edx, %ecx
	sall	%cl, %r12d
	cmpl	%r12d, %edi
	cmovge	%edi, %r12d
	cmpl	$2147483647, %r12d
	jne	.L948
	cmpl	$-2147483648, %eax
	jne	.L948
	.p2align 4,,10
	.p2align 3
.L941:
	movl	$1099, %eax
	jmp	.L939
	.p2align 4,,10
	.p2align 3
.L971:
	leaq	-48(%rbp), %r12
	movl	$1099, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L921
	movq	-48(%rbp), %rax
	jmp	.L924
	.p2align 4,,10
	.p2align 3
.L972:
	leaq	-48(%rbp), %r12
.L945:
	movl	$1031, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L930
	movq	-48(%rbp), %rax
	movq	-56(%rbp), %rdx
.L931:
	movq	%rax, -64(%rbp)
	cmpq	$1, %rdx
	jne	.L970
	jmp	.L934
	.p2align 4,,10
	.p2align 3
.L921:
	movq	8(%rbx), %rax
	movq	312(%rax), %rsi
	cmpq	-48(%rbp), %rsi
	jne	.L974
.L922:
	movq	208(%rax), %rax
	jmp	.L924
	.p2align 4,,10
	.p2align 3
.L930:
	movq	8(%rbx), %rax
	movq	312(%rax), %rsi
	cmpq	-48(%rbp), %rsi
	jne	.L975
.L932:
	movq	208(%rax), %rax
	movq	-56(%rbp), %rdx
	jmp	.L931
	.p2align 4,,10
	.p2align 3
.L947:
	xorl	%eax, %eax
	movl	$31, %esi
	xorl	%edx, %edx
	jmp	.L940
	.p2align 4,,10
	.p2align 3
.L974:
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L925
	movq	8(%rbx), %rax
	jmp	.L922
	.p2align 4,,10
	.p2align 3
.L975:
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L936
	movq	8(%rbx), %rax
	jmp	.L932
	.p2align 4,,10
	.p2align 3
.L925:
	movq	72(%rbx), %rsi
	cmpq	-48(%rbp), %rsi
	jne	.L926
.L927:
	movq	8(%rbx), %rax
	movq	(%rbx), %r12
	movq	-48(%rbp), %rdi
	movq	208(%rax), %rsi
	movq	%r12, %rdx
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%r12, %rdx
	movl	$1099, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	jmp	.L924
	.p2align 4,,10
	.p2align 3
.L937:
	movq	8(%rbx), %rax
	movq	(%rbx), %r12
	movq	-48(%rbp), %rdi
	movq	208(%rax), %rsi
	movq	%r12, %rdx
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%r12, %rdx
	movl	$1031, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	-56(%rbp), %rdx
	jmp	.L931
	.p2align 4,,10
	.p2align 3
.L926:
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L927
	movq	-64(%rbp), %rax
	movq	$1099, -56(%rbp)
	movq	%rax, -48(%rbp)
	cmpq	$1031, %rax
	je	.L935
	jmp	.L945
	.p2align 4,,10
	.p2align 3
.L948:
	pxor	%xmm0, %xmm0
	pxor	%xmm1, %xmm1
	movq	(%rbx), %rdi
	cvtsi2sdl	%eax, %xmm0
	cvtsi2sdl	%r12d, %xmm1
	call	_ZN2v88internal8compiler4Type5RangeEddPNS0_4ZoneE@PLT
	jmp	.L939
.L973:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18764:
	.size	_ZN2v88internal8compiler14OperationTyper15NumberShiftLeftENS1_4TypeES3_, .-_ZN2v88internal8compiler14OperationTyper15NumberShiftLeftENS1_4TypeES3_
	.section	.text._ZN2v88internal8compiler14OperationTyper16NumberShiftRightENS1_4TypeES3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper16NumberShiftRightENS1_4TypeES3_
	.type	_ZN2v88internal8compiler14OperationTyper16NumberShiftRightENS1_4TypeES3_, @function
_ZN2v88internal8compiler14OperationTyper16NumberShiftRightENS1_4TypeES3_:
.LFB18765:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	%rsi, -56(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -40(%rbp)
	xorl	%ecx, %ecx
	movq	%rsi, -48(%rbp)
	cmpq	$1099, %rsi
	jne	.L1028
.L981:
	movq	-64(%rbp), %rdx
	movq	%rax, -56(%rbp)
	movq	%rdx, -48(%rbp)
	cmpq	$1031, %rdx
	jne	.L1029
.L1027:
	cmpq	$1, %rax
	jne	.L992
.L991:
	movl	$1, %eax
.L996:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1030
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L993:
	.cfi_restore_state
	movq	80(%rbx), %rsi
	cmpq	-48(%rbp), %rsi
	je	.L994
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L994
	cmpq	$1, -56(%rbp)
	movq	$1031, -64(%rbp)
	je	.L991
	.p2align 4,,10
	.p2align 3
.L992:
	leaq	-56(%rbp), %r13
	leaq	-64(%rbp), %r14
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	movq	%r13, %rdi
	cvttsd2sil	%xmm0, %r12d
	call	_ZNK2v88internal8compiler4Type3MaxEv@PLT
	movq	%r14, %rdi
	cvttsd2sil	%xmm0, %r13d
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	movq	%r14, %rdi
	movsd	%xmm0, -72(%rbp)
	call	_ZNK2v88internal8compiler4Type3MaxEv@PLT
	cvttsd2siq	%xmm0, %rax
	movl	%eax, %edx
	cmpl	$31, %eax
	ja	.L1002
	cvttsd2siq	-72(%rbp), %rcx
	movl	%r12d, %esi
	movl	%r13d, %eax
	sarl	%cl, %esi
	sarl	%cl, %eax
.L997:
	movl	%edx, %ecx
	sarl	%cl, %r12d
	cmpl	%esi, %r12d
	cmovg	%esi, %r12d
	sarl	%cl, %r13d
	cmpl	%eax, %r13d
	cmovl	%eax, %r13d
	cmpl	$2147483647, %r13d
	jne	.L998
	cmpl	$-2147483648, %r12d
	jne	.L998
	movl	$1099, %eax
	jmp	.L996
	.p2align 4,,10
	.p2align 3
.L1028:
	leaq	-48(%rbp), %r12
	movl	$1099, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L978
	movq	-48(%rbp), %rax
	jmp	.L981
	.p2align 4,,10
	.p2align 3
.L1029:
	leaq	-48(%rbp), %r12
.L1000:
	movl	$1031, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L987
	movq	-48(%rbp), %rax
	movq	-56(%rbp), %rdx
.L988:
	movq	%rax, -64(%rbp)
	cmpq	$1, %rdx
	jne	.L1027
	jmp	.L991
	.p2align 4,,10
	.p2align 3
.L978:
	movq	8(%rbx), %rax
	movq	312(%rax), %rsi
	cmpq	-48(%rbp), %rsi
	jne	.L1031
.L979:
	movq	208(%rax), %rax
	jmp	.L981
	.p2align 4,,10
	.p2align 3
.L987:
	movq	8(%rbx), %rax
	movq	312(%rax), %rsi
	cmpq	-48(%rbp), %rsi
	jne	.L1032
.L989:
	movq	208(%rax), %rax
	movq	-56(%rbp), %rdx
	jmp	.L988
	.p2align 4,,10
	.p2align 3
.L1002:
	movl	%r13d, %eax
	movl	%r12d, %esi
	movl	$31, %edx
	jmp	.L997
	.p2align 4,,10
	.p2align 3
.L1032:
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L993
	movq	8(%rbx), %rax
	jmp	.L989
	.p2align 4,,10
	.p2align 3
.L1031:
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L982
	movq	8(%rbx), %rax
	jmp	.L979
	.p2align 4,,10
	.p2align 3
.L998:
	pxor	%xmm0, %xmm0
	pxor	%xmm1, %xmm1
	movq	(%rbx), %rdi
	cvtsi2sdl	%r12d, %xmm0
	cvtsi2sdl	%r13d, %xmm1
	call	_ZN2v88internal8compiler4Type5RangeEddPNS0_4ZoneE@PLT
	jmp	.L996
	.p2align 4,,10
	.p2align 3
.L982:
	movq	72(%rbx), %rsi
	cmpq	-48(%rbp), %rsi
	jne	.L983
.L984:
	movq	8(%rbx), %rax
	movq	(%rbx), %r12
	movq	-48(%rbp), %rdi
	movq	208(%rax), %rsi
	movq	%r12, %rdx
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%r12, %rdx
	movl	$1099, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	jmp	.L981
	.p2align 4,,10
	.p2align 3
.L994:
	movq	8(%rbx), %rax
	movq	(%rbx), %r12
	movq	-48(%rbp), %rdi
	movq	208(%rax), %rsi
	movq	%r12, %rdx
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%r12, %rdx
	movl	$1031, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	-56(%rbp), %rdx
	jmp	.L988
	.p2align 4,,10
	.p2align 3
.L983:
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L984
	movq	-64(%rbp), %rax
	movq	$1099, -56(%rbp)
	movq	%rax, -48(%rbp)
	cmpq	$1031, %rax
	je	.L992
	jmp	.L1000
.L1030:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18765:
	.size	_ZN2v88internal8compiler14OperationTyper16NumberShiftRightENS1_4TypeES3_, .-_ZN2v88internal8compiler14OperationTyper16NumberShiftRightENS1_4TypeES3_
	.section	.text._ZN2v88internal8compiler14OperationTyper23NumberShiftRightLogicalENS1_4TypeES3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper23NumberShiftRightLogicalENS1_4TypeES3_
	.type	_ZN2v88internal8compiler14OperationTyper23NumberShiftRightLogicalENS1_4TypeES3_, @function
_ZN2v88internal8compiler14OperationTyper23NumberShiftRightLogicalENS1_4TypeES3_:
.LFB18766:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%rsi, -72(%rbp)
	movq	%rdx, -80(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	movq	%rsi, -64(%rbp)
	cmpq	$1031, %rsi
	jne	.L1096
.L1038:
	movq	-80(%rbp), %rdx
	movq	%rax, -72(%rbp)
	movq	%rdx, -64(%rbp)
	cmpq	$1031, %rdx
	jne	.L1097
.L1095:
	cmpq	$1, %rax
	jne	.L1049
.L1048:
	movl	$1, %eax
.L1053:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1098
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1050:
	.cfi_restore_state
	movq	80(%rbx), %rsi
	cmpq	-64(%rbp), %rsi
	je	.L1051
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1051
	cmpq	$1, -72(%rbp)
	movq	$1031, -80(%rbp)
	je	.L1048
	.p2align 4,,10
	.p2align 3
.L1049:
	leaq	-72(%rbp), %r13
	leaq	-80(%rbp), %r15
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	movq	%r13, %rdi
	cvttsd2siq	%xmm0, %r12
	call	_ZNK2v88internal8compiler4Type3MaxEv@PLT
	movq	%r15, %rdi
	cvttsd2siq	%xmm0, %r13
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	movq	%r15, %rdi
	movsd	%xmm0, -88(%rbp)
	call	_ZNK2v88internal8compiler4Type3MaxEv@PLT
	cvttsd2siq	%xmm0, %rax
	movl	%r13d, %r14d
	movl	%eax, %edx
	cmpl	$31, %eax
	ja	.L1064
	cvttsd2siq	-88(%rbp), %rcx
	shrl	%cl, %r14d
.L1054:
	movl	%edx, %ecx
	movl	%r12d, %eax
	shrl	%cl, %eax
	testl	%eax, %eax
	sete	%dl
	cmpl	$2147483647, %r14d
	jne	.L1055
	testb	%dl, %dl
	je	.L1055
	movl	$1027, %eax
	jmp	.L1053
	.p2align 4,,10
	.p2align 3
.L1096:
	leaq	-64(%rbp), %r12
	movl	$1031, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L1035
	movq	-64(%rbp), %rax
	jmp	.L1038
	.p2align 4,,10
	.p2align 3
.L1097:
	leaq	-64(%rbp), %r12
.L1062:
	movl	$1031, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L1044
	movq	-64(%rbp), %rax
	movq	-72(%rbp), %rdx
.L1045:
	movq	%rax, -80(%rbp)
	cmpq	$1, %rdx
	jne	.L1095
	jmp	.L1048
	.p2align 4,,10
	.p2align 3
.L1035:
	movq	8(%rbx), %rax
	movq	312(%rax), %rsi
	cmpq	-64(%rbp), %rsi
	jne	.L1099
.L1036:
	movq	208(%rax), %rax
	jmp	.L1038
	.p2align 4,,10
	.p2align 3
.L1044:
	movq	8(%rbx), %rax
	movq	312(%rax), %rsi
	cmpq	-64(%rbp), %rsi
	jne	.L1100
.L1046:
	movq	208(%rax), %rax
	movq	-72(%rbp), %rdx
	jmp	.L1045
	.p2align 4,,10
	.p2align 3
.L1064:
	movl	$31, %edx
	jmp	.L1054
	.p2align 4,,10
	.p2align 3
.L1100:
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L1050
	movq	8(%rbx), %rax
	jmp	.L1046
	.p2align 4,,10
	.p2align 3
.L1099:
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L1039
	movq	8(%rbx), %rax
	jmp	.L1036
	.p2align 4,,10
	.p2align 3
.L1055:
	cmpl	$-1, %r14d
	jne	.L1056
	testb	%dl, %dl
	je	.L1056
	movl	$1031, %eax
	jmp	.L1053
	.p2align 4,,10
	.p2align 3
.L1039:
	movq	80(%rbx), %rsi
	cmpq	-64(%rbp), %rsi
	jne	.L1040
.L1041:
	movq	8(%rbx), %rax
	movq	(%rbx), %r12
	movq	-64(%rbp), %rdi
	movq	208(%rax), %rsi
	movq	%r12, %rdx
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%r12, %rdx
	movl	$1031, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	jmp	.L1038
	.p2align 4,,10
	.p2align 3
.L1056:
	pxor	%xmm0, %xmm0
	pxor	%xmm1, %xmm1
	movq	(%rbx), %rdi
	cvtsi2sdq	%rax, %xmm0
	cvtsi2sdq	%r14, %xmm1
	call	_ZN2v88internal8compiler4Type5RangeEddPNS0_4ZoneE@PLT
	jmp	.L1053
	.p2align 4,,10
	.p2align 3
.L1051:
	movq	8(%rbx), %rax
	movq	(%rbx), %r12
	movq	-64(%rbp), %rdi
	movq	208(%rax), %rsi
	movq	%r12, %rdx
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%r12, %rdx
	movl	$1031, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	-72(%rbp), %rdx
	jmp	.L1045
	.p2align 4,,10
	.p2align 3
.L1040:
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1041
	movq	-80(%rbp), %rax
	movq	$1031, -72(%rbp)
	movq	%rax, -64(%rbp)
	cmpq	$1031, %rax
	je	.L1049
	jmp	.L1062
.L1098:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18766:
	.size	_ZN2v88internal8compiler14OperationTyper23NumberShiftRightLogicalENS1_4TypeES3_, .-_ZN2v88internal8compiler14OperationTyper23NumberShiftRightLogicalENS1_4TypeES3_
	.section	.text._ZN2v88internal8compiler14OperationTyper11NumberAtan2ENS1_4TypeES3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper11NumberAtan2ENS1_4TypeES3_
	.type	_ZN2v88internal8compiler14OperationTyper11NumberAtan2ENS1_4TypeES3_, @function
_ZN2v88internal8compiler14OperationTyper11NumberAtan2ENS1_4TypeES3_:
.LFB18767:
	.cfi_startproc
	endbr64
	movl	$7263, %eax
	ret
	.cfi_endproc
.LFE18767:
	.size	_ZN2v88internal8compiler14OperationTyper11NumberAtan2ENS1_4TypeES3_, .-_ZN2v88internal8compiler14OperationTyper11NumberAtan2ENS1_4TypeES3_
	.section	.text._ZN2v88internal8compiler14OperationTyper10NumberImulENS1_4TypeES3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper10NumberImulENS1_4TypeES3_
	.type	_ZN2v88internal8compiler14OperationTyper10NumberImulENS1_4TypeES3_, @function
_ZN2v88internal8compiler14OperationTyper10NumberImulENS1_4TypeES3_:
.LFB18768:
	.cfi_startproc
	endbr64
	movl	$1099, %eax
	ret
	.cfi_endproc
.LFE18768:
	.size	_ZN2v88internal8compiler14OperationTyper10NumberImulENS1_4TypeES3_, .-_ZN2v88internal8compiler14OperationTyper10NumberImulENS1_4TypeES3_
	.section	.text._ZN2v88internal8compiler14OperationTyper9NumberMaxENS1_4TypeES3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper9NumberMaxENS1_4TypeES3_
	.type	_ZN2v88internal8compiler14OperationTyper9NumberMaxENS1_4TypeES3_, @function
_ZN2v88internal8compiler14OperationTyper9NumberMaxENS1_4TypeES3_:
.LFB18769:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -56(%rbp)
	movq	%rdx, -64(%rbp)
	cmpq	$1, %rsi
	je	.L1104
	cmpq	$1, %rdx
	je	.L1104
	cmpq	$4097, %rsi
	je	.L1107
	leaq	-56(%rbp), %r13
	movq	%rdi, %r12
	movl	$4097, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1107
	cmpq	$4097, -64(%rbp)
	je	.L1107
	leaq	-64(%rbp), %r14
	movl	$4097, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1107
	movl	$4097, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1110
	movl	$4097, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1110
	movl	$1, %r15d
.L1109:
	movq	(%r12), %rdx
	movq	-56(%rbp), %rdi
	movl	$3167, %esi
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	(%r12), %rdx
	movq	-64(%rbp), %rdi
	movl	$3167, %esi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -64(%rbp)
	movq	8(%r12), %rax
	movq	328(%rax), %rsi
	cmpq	%rsi, -56(%rbp)
	je	.L1111
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L1115
	movq	8(%r12), %rax
	movq	328(%rax), %rsi
.L1111:
	cmpq	%rsi, -64(%rbp)
	je	.L1114
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1114
.L1115:
	movq	(%r12), %r12
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	movq	%r12, %rdx
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%r12, %rdx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, %rbx
.L1113:
	movq	%rbx, %rax
	jmp	.L1106
	.p2align 4,,10
	.p2align 3
.L1104:
	movl	$1, %eax
.L1106:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1107:
	.cfi_restore_state
	addq	$40, %rsp
	movl	$4097, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1110:
	.cfi_restore_state
	movq	(%r12), %rdx
	movl	$4097, %esi
	movl	$1, %edi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, %r15
	jmp	.L1109
	.p2align 4,,10
	.p2align 3
.L1114:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type3MaxEv@PLT
	movq	%r13, %rdi
	movsd	%xmm0, -72(%rbp)
	call	_ZNK2v88internal8compiler4Type3MaxEv@PLT
	movsd	-72(%rbp), %xmm3
	movq	%r14, %rdi
	maxsd	%xmm0, %xmm3
	movsd	%xmm3, -72(%rbp)
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	movq	%r13, %rdi
	movsd	%xmm0, -80(%rbp)
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	movsd	-80(%rbp), %xmm2
	movq	(%r12), %rbx
	movsd	-72(%rbp), %xmm1
	maxsd	%xmm0, %xmm2
	movq	%rbx, %rdi
	movapd	%xmm2, %xmm0
	movsd	%xmm2, -80(%rbp)
	call	_ZN2v88internal8compiler4Type5RangeEddPNS0_4ZoneE@PLT
	movq	%rbx, %rdx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movsd	-80(%rbp), %xmm2
	pxor	%xmm0, %xmm0
	movq	%rax, %r15
	movq	%rax, %rbx
	comisd	%xmm2, %xmm0
	jb	.L1113
	movsd	-72(%rbp), %xmm3
	comisd	%xmm0, %xmm3
	jb	.L1113
	movl	$2049, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1121
	movl	$2049, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	je	.L1113
.L1121:
	movq	(%r12), %rdx
	movl	$2049, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, %rbx
	jmp	.L1113
	.cfi_endproc
.LFE18769:
	.size	_ZN2v88internal8compiler14OperationTyper9NumberMaxENS1_4TypeES3_, .-_ZN2v88internal8compiler14OperationTyper9NumberMaxENS1_4TypeES3_
	.section	.text._ZN2v88internal8compiler14OperationTyper9NumberMinENS1_4TypeES3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper9NumberMinENS1_4TypeES3_
	.type	_ZN2v88internal8compiler14OperationTyper9NumberMinENS1_4TypeES3_, @function
_ZN2v88internal8compiler14OperationTyper9NumberMinENS1_4TypeES3_:
.LFB18770:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -56(%rbp)
	movq	%rdx, -64(%rbp)
	cmpq	$1, %rsi
	je	.L1135
	cmpq	$1, %rdx
	je	.L1135
	cmpq	$4097, %rsi
	je	.L1138
	leaq	-56(%rbp), %r13
	movq	%rdi, %r12
	movl	$4097, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1138
	cmpq	$4097, -64(%rbp)
	je	.L1138
	leaq	-64(%rbp), %r14
	movl	$4097, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1138
	movl	$4097, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1141
	movl	$4097, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1141
	movl	$1, %r15d
.L1140:
	movq	(%r12), %rdx
	movq	-56(%rbp), %rdi
	movl	$3167, %esi
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	(%r12), %rdx
	movq	-64(%rbp), %rdi
	movl	$3167, %esi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -64(%rbp)
	movq	8(%r12), %rax
	movq	328(%rax), %rsi
	cmpq	%rsi, -56(%rbp)
	je	.L1142
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L1146
	movq	8(%r12), %rax
	movq	328(%rax), %rsi
.L1142:
	cmpq	%rsi, -64(%rbp)
	je	.L1145
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1145
.L1146:
	movq	(%r12), %r12
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	movq	%r12, %rdx
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%r12, %rdx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, %rbx
.L1144:
	movq	%rbx, %rax
	jmp	.L1137
	.p2align 4,,10
	.p2align 3
.L1135:
	movl	$1, %eax
.L1137:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1138:
	.cfi_restore_state
	addq	$40, %rsp
	movl	$4097, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1141:
	.cfi_restore_state
	movq	(%r12), %rdx
	movl	$4097, %esi
	movl	$1, %edi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, %r15
	jmp	.L1140
	.p2align 4,,10
	.p2align 3
.L1145:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type3MaxEv@PLT
	movq	%r13, %rdi
	movsd	%xmm0, -72(%rbp)
	call	_ZNK2v88internal8compiler4Type3MaxEv@PLT
	movsd	-72(%rbp), %xmm3
	movq	%r14, %rdi
	minsd	%xmm0, %xmm3
	movsd	%xmm3, -72(%rbp)
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	movq	%r13, %rdi
	movsd	%xmm0, -80(%rbp)
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	movsd	-80(%rbp), %xmm2
	movq	(%r12), %rbx
	movsd	-72(%rbp), %xmm1
	minsd	%xmm0, %xmm2
	movq	%rbx, %rdi
	movapd	%xmm2, %xmm0
	movsd	%xmm2, -80(%rbp)
	call	_ZN2v88internal8compiler4Type5RangeEddPNS0_4ZoneE@PLT
	movq	%rbx, %rdx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movsd	-80(%rbp), %xmm2
	pxor	%xmm0, %xmm0
	movq	%rax, %r15
	movq	%rax, %rbx
	comisd	%xmm2, %xmm0
	jb	.L1144
	movsd	-72(%rbp), %xmm3
	comisd	%xmm0, %xmm3
	jb	.L1144
	movl	$2049, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1152
	movl	$2049, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	je	.L1144
.L1152:
	movq	(%r12), %rdx
	movl	$2049, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, %rbx
	jmp	.L1144
	.cfi_endproc
.LFE18770:
	.size	_ZN2v88internal8compiler14OperationTyper9NumberMinENS1_4TypeES3_, .-_ZN2v88internal8compiler14OperationTyper9NumberMinENS1_4TypeES3_
	.section	.text._ZN2v88internal8compiler14OperationTyper9NumberPowENS1_4TypeES3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper9NumberPowENS1_4TypeES3_
	.type	_ZN2v88internal8compiler14OperationTyper9NumberPowENS1_4TypeES3_, @function
_ZN2v88internal8compiler14OperationTyper9NumberPowENS1_4TypeES3_:
.LFB22727:
	.cfi_startproc
	endbr64
	movl	$7263, %eax
	ret
	.cfi_endproc
.LFE22727:
	.size	_ZN2v88internal8compiler14OperationTyper9NumberPowENS1_4TypeES3_, .-_ZN2v88internal8compiler14OperationTyper9NumberPowENS1_4TypeES3_
	.section	.text._ZN2v88internal8compiler14OperationTyper20SpeculativeNumberAddENS1_4TypeES3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper20SpeculativeNumberAddENS1_4TypeES3_
	.type	_ZN2v88internal8compiler14OperationTyper20SpeculativeNumberAddENS1_4TypeES3_, @function
_ZN2v88internal8compiler14OperationTyper20SpeculativeNumberAddENS1_4TypeES3_:
.LFB18772:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movq	%rsi, %rdi
	movl	$8396767, %esi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 12, -48
	movq	(%r14), %rdx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -48(%rbp)
	movq	%rax, %r12
	cmpq	$7263, %rax
	jne	.L1193
.L1169:
	movq	(%r14), %rdx
	movl	$8396767, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14OperationTyper8ToNumberENS1_4TypeE
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14OperationTyper9NumberAddENS1_4TypeES3_
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1194
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1193:
	.cfi_restore_state
	leaq	-48(%rbp), %r15
	movl	$7263, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L1168
	movq	-48(%rbp), %r12
	jmp	.L1169
	.p2align 4,,10
	.p2align 3
.L1168:
	movl	$75448353, %esi
	movq	%r15, %rdi
	movl	$7263, %r12d
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1169
	movq	-48(%rbp), %rdi
	movq	(%r14), %rdx
	movl	$24575, %esi
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movl	$129, %esi
	movq	%r15, %rdi
	movq	%rax, -48(%rbp)
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1195
.L1171:
	movl	$257, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1196
.L1172:
	movq	48(%r14), %rsi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1197
.L1173:
	movq	56(%r14), %rsi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1198
.L1174:
	movq	(%r14), %rdx
	movq	-48(%rbp), %rdi
	movl	$7263, %esi
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, %r12
	jmp	.L1169
	.p2align 4,,10
	.p2align 3
.L1195:
	movq	8(%r14), %rax
	movq	(%r14), %rdx
	movq	-48(%rbp), %rdi
	movq	208(%rax), %rsi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -48(%rbp)
	jmp	.L1171
	.p2align 4,,10
	.p2align 3
.L1198:
	movq	8(%r14), %rax
	movq	(%r14), %rdx
	movq	-48(%rbp), %rdi
	movq	216(%rax), %rsi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -48(%rbp)
	jmp	.L1174
	.p2align 4,,10
	.p2align 3
.L1197:
	movq	8(%r14), %rax
	movq	(%r14), %rdx
	movq	-48(%rbp), %rdi
	movq	208(%rax), %rsi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -48(%rbp)
	jmp	.L1173
	.p2align 4,,10
	.p2align 3
.L1196:
	movq	(%r14), %rdx
	movq	-48(%rbp), %rdi
	movl	$4097, %esi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -48(%rbp)
	jmp	.L1172
.L1194:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18772:
	.size	_ZN2v88internal8compiler14OperationTyper20SpeculativeNumberAddENS1_4TypeES3_, .-_ZN2v88internal8compiler14OperationTyper20SpeculativeNumberAddENS1_4TypeES3_
	.section	.text._ZN2v88internal8compiler14OperationTyper25SpeculativeNumberSubtractENS1_4TypeES3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper25SpeculativeNumberSubtractENS1_4TypeES3_
	.type	_ZN2v88internal8compiler14OperationTyper25SpeculativeNumberSubtractENS1_4TypeES3_, @function
_ZN2v88internal8compiler14OperationTyper25SpeculativeNumberSubtractENS1_4TypeES3_:
.LFB18773:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	movl	$8396767, %esi
	subq	$24, %rsp
	movq	(%rdi), %rdx
	movq	%rbx, %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14OperationTyper8ToNumberENS1_4TypeE
	movq	(%r12), %rdx
	movl	$8396767, %esi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -48(%rbp)
	movq	%rax, %rdx
	cmpq	$7263, %rax
	jne	.L1226
.L1202:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14OperationTyper14NumberSubtractENS1_4TypeES3_
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1227
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1226:
	.cfi_restore_state
	leaq	-48(%rbp), %r13
	movl	$7263, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L1201
	movq	-48(%rbp), %rdx
	jmp	.L1202
	.p2align 4,,10
	.p2align 3
.L1201:
	movl	$75448353, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	movl	$7263, %edx
	testb	%al, %al
	jne	.L1202
	movq	-48(%rbp), %rdi
	movq	(%r12), %rdx
	movl	$24575, %esi
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movl	$129, %esi
	movq	%r13, %rdi
	movq	%rax, -48(%rbp)
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1228
.L1204:
	movl	$257, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1229
.L1205:
	movq	48(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1230
.L1206:
	movq	56(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1231
.L1207:
	movq	(%r12), %rdx
	movq	-48(%rbp), %rdi
	movl	$7263, %esi
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, %rdx
	jmp	.L1202
	.p2align 4,,10
	.p2align 3
.L1228:
	movq	8(%r12), %rax
	movq	(%r12), %rdx
	movq	-48(%rbp), %rdi
	movq	208(%rax), %rsi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -48(%rbp)
	jmp	.L1204
	.p2align 4,,10
	.p2align 3
.L1231:
	movq	8(%r12), %rax
	movq	(%r12), %rdx
	movq	-48(%rbp), %rdi
	movq	216(%rax), %rsi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -48(%rbp)
	jmp	.L1207
	.p2align 4,,10
	.p2align 3
.L1230:
	movq	8(%r12), %rax
	movq	(%r12), %rdx
	movq	-48(%rbp), %rdi
	movq	208(%rax), %rsi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -48(%rbp)
	jmp	.L1206
	.p2align 4,,10
	.p2align 3
.L1229:
	movq	(%r12), %rdx
	movq	-48(%rbp), %rdi
	movl	$4097, %esi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -48(%rbp)
	jmp	.L1205
.L1227:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18773:
	.size	_ZN2v88internal8compiler14OperationTyper25SpeculativeNumberSubtractENS1_4TypeES3_, .-_ZN2v88internal8compiler14OperationTyper25SpeculativeNumberSubtractENS1_4TypeES3_
	.section	.text._ZN2v88internal8compiler14OperationTyper25SpeculativeNumberMultiplyENS1_4TypeES3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper25SpeculativeNumberMultiplyENS1_4TypeES3_
	.type	_ZN2v88internal8compiler14OperationTyper25SpeculativeNumberMultiplyENS1_4TypeES3_, @function
_ZN2v88internal8compiler14OperationTyper25SpeculativeNumberMultiplyENS1_4TypeES3_:
.LFB18774:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movl	$8396767, %esi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	(%rdi), %rdx
	movq	%r12, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14OperationTyper8ToNumberENS1_4TypeE
	movq	(%rbx), %rdx
	movl	$8396767, %esi
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -64(%rbp)
	cmpq	$7263, %rax
	jne	.L1310
	movq	%r12, -72(%rbp)
	movq	%r12, %rax
	cmpq	$1, %r12
	je	.L1237
.L1238:
	cmpq	$4097, %r12
	je	.L1243
	leaq	-72(%rbp), %r15
	movl	$4097, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1243
	cmpq	$4097, -64(%rbp)
	je	.L1243
	leaq	-64(%rbp), %r14
	movl	$4097, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1243
	movl	$4097, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	je	.L1244
.L1246:
	movl	$1, %r12d
.L1245:
	movq	(%rbx), %rdx
	movq	-72(%rbp), %rdi
	movl	$3167, %esi
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	-64(%rbp), %rdi
	movq	(%rbx), %rdx
	movl	$3167, %esi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movl	$2049, %esi
	movq	%r15, %rdi
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	je	.L1253
.L1255:
	movl	$1, %r13d
.L1254:
	movl	$2049, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1311
.L1259:
	movl	$2049, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1312
.L1260:
	movq	8(%rbx), %rax
	movq	-72(%rbp), %rsi
	movq	320(%rax), %r8
	cmpq	%rsi, %r8
	je	.L1261
	movq	%r8, %rsi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L1266
	movq	8(%rbx), %rax
	movq	320(%rax), %rsi
.L1261:
	cmpq	-64(%rbp), %rsi
	je	.L1265
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1265
.L1266:
	movl	$3167, %eax
.L1263:
	testb	%r13b, %r13b
	jne	.L1313
.L1267:
	testb	%r12b, %r12b
	je	.L1237
	movq	(%rbx), %rdx
	movl	$4097, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	.p2align 4,,10
	.p2align 3
.L1237:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1314
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1243:
	.cfi_restore_state
	movl	$4097, %eax
	jmp	.L1237
	.p2align 4,,10
	.p2align 3
.L1310:
	leaq	-64(%rbp), %r14
	movl	$7263, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L1234
	movq	-64(%rbp), %rax
.L1235:
	movq	%r12, -72(%rbp)
	movq	%rax, -64(%rbp)
	cmpq	$1, %r12
	je	.L1270
	cmpq	$1, %rax
	je	.L1237
	jmp	.L1238
	.p2align 4,,10
	.p2align 3
.L1234:
	movl	$75448353, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	je	.L1236
	movq	%r12, -72(%rbp)
	movq	%r12, %rax
	movq	$7263, -64(%rbp)
	cmpq	$1, %r12
	jne	.L1238
	jmp	.L1237
	.p2align 4,,10
	.p2align 3
.L1236:
	movq	-64(%rbp), %rdi
	movq	(%rbx), %rdx
	movl	$24575, %esi
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movl	$129, %esi
	movq	%r14, %rdi
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1315
.L1239:
	movl	$257, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1316
.L1240:
	movq	48(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1317
.L1241:
	movq	56(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1318
.L1242:
	movq	(%rbx), %rdx
	movq	-64(%rbp), %rdi
	movl	$7263, %esi
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	jmp	.L1235
	.p2align 4,,10
	.p2align 3
.L1270:
	movl	$1, %eax
	jmp	.L1237
	.p2align 4,,10
	.p2align 3
.L1318:
	movq	8(%rbx), %rax
	movq	(%rbx), %rdx
	movq	-64(%rbp), %rdi
	movq	216(%rax), %rsi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -64(%rbp)
	jmp	.L1242
	.p2align 4,,10
	.p2align 3
.L1316:
	movq	(%rbx), %rdx
	movq	-64(%rbp), %rdi
	movl	$4097, %esi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -64(%rbp)
	jmp	.L1240
	.p2align 4,,10
	.p2align 3
.L1315:
	movq	8(%rbx), %rax
	movq	(%rbx), %rdx
	movq	-64(%rbp), %rdi
	movq	208(%rax), %rsi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -64(%rbp)
	jmp	.L1239
	.p2align 4,,10
	.p2align 3
.L1317:
	movq	8(%rbx), %rax
	movq	(%rbx), %rdx
	movq	-64(%rbp), %rdi
	movq	208(%rax), %rsi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -64(%rbp)
	jmp	.L1241
	.p2align 4,,10
	.p2align 3
.L1265:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type3MaxEv@PLT
	movq	%r14, %rdi
	movsd	%xmm0, -104(%rbp)
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	movq	%r15, %rdi
	movsd	%xmm0, -96(%rbp)
	call	_ZNK2v88internal8compiler4Type3MaxEv@PLT
	movq	%r15, %rdi
	movsd	%xmm0, -88(%rbp)
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	movsd	-104(%rbp), %xmm3
	movsd	-96(%rbp), %xmm2
	movq	%rbx, %rdi
	movsd	-88(%rbp), %xmm1
	call	_ZN2v88internal8compiler14OperationTyper14MultiplyRangerEdddd
	jmp	.L1263
.L1313:
	movq	(%rbx), %rdx
	movl	$2049, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	jmp	.L1267
.L1312:
	movq	8(%rbx), %rax
	movq	(%rbx), %rdx
	movq	-64(%rbp), %rdi
	movq	208(%rax), %rsi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	(%rbx), %rdx
	movl	$1119, %esi
	movq	%rax, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -64(%rbp)
	jmp	.L1260
.L1311:
	movq	8(%rbx), %rax
	movq	(%rbx), %rdx
	movq	-72(%rbp), %rdi
	movq	208(%rax), %rsi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	(%rbx), %rdx
	movl	$1119, %esi
	movq	%rax, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -72(%rbp)
	jmp	.L1259
.L1253:
	movl	$2049, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1255
	movq	8(%rbx), %rax
	movq	%r15, %rdi
	movq	312(%rax), %rsi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	je	.L1258
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	pxor	%xmm1, %xmm1
	comisd	%xmm0, %xmm1
	ja	.L1255
.L1258:
	movq	8(%rbx), %rax
	movq	%r14, %rdi
	movq	312(%rax), %rsi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	movl	%eax, %r13d
	testb	%al, %al
	je	.L1254
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	pxor	%xmm1, %xmm1
	comisd	%xmm0, %xmm1
	seta	%r13b
	jmp	.L1254
	.p2align 4,,10
	.p2align 3
.L1244:
	movl	$4097, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1246
	movq	8(%rbx), %rax
	movq	%r15, %rdi
	movq	312(%rax), %rsi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	je	.L1251
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	ucomisd	.LC1(%rip), %xmm0
	jp	.L1271
	je	.L1246
.L1271:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type3MaxEv@PLT
	ucomisd	.LC0(%rip), %xmm0
	jp	.L1251
	je	.L1246
.L1251:
	movq	8(%rbx), %rax
	movq	%r14, %rdi
	movq	312(%rax), %rsi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	movl	%eax, %r12d
	testb	%al, %al
	je	.L1245
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	ucomisd	.LC1(%rip), %xmm0
	jp	.L1272
	je	.L1246
.L1272:
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type3MaxEv@PLT
	ucomisd	.LC0(%rip), %xmm0
	movl	$0, %eax
	setnp	%r12b
	cmovne	%eax, %r12d
	jmp	.L1245
.L1314:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18774:
	.size	_ZN2v88internal8compiler14OperationTyper25SpeculativeNumberMultiplyENS1_4TypeES3_, .-_ZN2v88internal8compiler14OperationTyper25SpeculativeNumberMultiplyENS1_4TypeES3_
	.section	.text._ZN2v88internal8compiler14OperationTyper23SpeculativeNumberDivideENS1_4TypeES3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper23SpeculativeNumberDivideENS1_4TypeES3_
	.type	_ZN2v88internal8compiler14OperationTyper23SpeculativeNumberDivideENS1_4TypeES3_, @function
_ZN2v88internal8compiler14OperationTyper23SpeculativeNumberDivideENS1_4TypeES3_:
.LFB18775:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	movl	$8396767, %esi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	(%rdi), %rdx
	movq	%r12, %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14OperationTyper8ToNumberENS1_4TypeE
	movq	(%rbx), %rdx
	movl	$8396767, %esi
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -48(%rbp)
	cmpq	$7263, %rax
	jne	.L1381
	movq	%r12, -56(%rbp)
	movq	%r12, %rax
	cmpq	$1, %r12
	je	.L1324
.L1325:
	cmpq	$4097, %r12
	je	.L1330
	leaq	-56(%rbp), %r14
	movl	$4097, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1330
	cmpq	$4097, -48(%rbp)
	je	.L1330
	leaq	-48(%rbp), %r13
	movl	$4097, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1330
	movl	$4097, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	je	.L1331
.L1333:
	movl	$1, %r12d
.L1332:
	movq	(%rbx), %rdx
	movq	-56(%rbp), %rdi
	movl	$3167, %esi
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	(%rbx), %rdx
	movq	-48(%rbp), %rdi
	movl	$3167, %esi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -48(%rbp)
	movq	8(%rbx), %rax
	movq	320(%rax), %rsi
	cmpq	-56(%rbp), %rsi
	je	.L1340
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L1344
	movq	8(%rbx), %rax
.L1340:
	movq	312(%rax), %rsi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	je	.L1347
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	pxor	%xmm1, %xmm1
	comisd	%xmm0, %xmm1
	ja	.L1344
.L1347:
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	ucomisd	.LC1(%rip), %xmm0
	jp	.L1346
	jne	.L1346
.L1344:
	movq	(%rbx), %rdx
	movl	$2049, %esi
	movl	$1119, %edi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
.L1342:
	testb	%r12b, %r12b
	je	.L1324
	movq	(%rbx), %rdx
	movl	$4097, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	.p2align 4,,10
	.p2align 3
.L1324:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1382
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1330:
	.cfi_restore_state
	movl	$4097, %eax
	jmp	.L1324
	.p2align 4,,10
	.p2align 3
.L1381:
	leaq	-48(%rbp), %r13
	movl	$7263, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L1321
	movq	-48(%rbp), %rax
.L1322:
	movq	%r12, -56(%rbp)
	movq	%rax, -48(%rbp)
	cmpq	$1, %r12
	je	.L1351
	cmpq	$1, %rax
	je	.L1324
	jmp	.L1325
	.p2align 4,,10
	.p2align 3
.L1321:
	movl	$75448353, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	je	.L1323
	movq	%r12, -56(%rbp)
	movq	%r12, %rax
	movq	$7263, -48(%rbp)
	cmpq	$1, %r12
	jne	.L1325
	jmp	.L1324
	.p2align 4,,10
	.p2align 3
.L1323:
	movq	-48(%rbp), %rdi
	movq	(%rbx), %rdx
	movl	$24575, %esi
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movl	$129, %esi
	movq	%r13, %rdi
	movq	%rax, -48(%rbp)
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1383
.L1326:
	movl	$257, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1384
.L1327:
	movq	48(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1385
.L1328:
	movq	56(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1386
.L1329:
	movq	(%rbx), %rdx
	movq	-48(%rbp), %rdi
	movl	$7263, %esi
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	jmp	.L1322
	.p2align 4,,10
	.p2align 3
.L1351:
	movl	$1, %eax
	jmp	.L1324
	.p2align 4,,10
	.p2align 3
.L1386:
	movq	8(%rbx), %rax
	movq	(%rbx), %rdx
	movq	-48(%rbp), %rdi
	movq	216(%rax), %rsi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -48(%rbp)
	jmp	.L1329
	.p2align 4,,10
	.p2align 3
.L1385:
	movq	8(%rbx), %rax
	movq	(%rbx), %rdx
	movq	-48(%rbp), %rdi
	movq	208(%rax), %rsi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -48(%rbp)
	jmp	.L1328
	.p2align 4,,10
	.p2align 3
.L1384:
	movq	(%rbx), %rdx
	movq	-48(%rbp), %rdi
	movl	$4097, %esi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -48(%rbp)
	jmp	.L1327
	.p2align 4,,10
	.p2align 3
.L1383:
	movq	8(%rbx), %rax
	movq	(%rbx), %rdx
	movq	-48(%rbp), %rdi
	movq	208(%rax), %rsi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -48(%rbp)
	jmp	.L1326
.L1331:
	movq	8(%rbx), %rax
	movq	%r13, %rdi
	movq	312(%rax), %rsi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	movl	%eax, %r12d
	testb	%al, %al
	jne	.L1333
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	ucomisd	.LC1(%rip), %xmm0
	jp	.L1334
	jne	.L1334
.L1338:
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	ucomisd	.LC1(%rip), %xmm0
	jp	.L1337
	je	.L1333
.L1337:
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type3MaxEv@PLT
	ucomisd	.LC0(%rip), %xmm0
	movl	$0, %eax
	setnp	%r12b
	cmovne	%eax, %r12d
	jmp	.L1332
.L1346:
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type3MaxEv@PLT
	ucomisd	.LC0(%rip), %xmm0
	jp	.L1352
	je	.L1344
.L1352:
	movl	$1119, %eax
	jmp	.L1342
.L1334:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type3MaxEv@PLT
	ucomisd	.LC0(%rip), %xmm0
	jp	.L1332
	je	.L1338
	jmp	.L1332
.L1382:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18775:
	.size	_ZN2v88internal8compiler14OperationTyper23SpeculativeNumberDivideENS1_4TypeES3_, .-_ZN2v88internal8compiler14OperationTyper23SpeculativeNumberDivideENS1_4TypeES3_
	.section	.text._ZN2v88internal8compiler14OperationTyper24SpeculativeNumberModulusENS1_4TypeES3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper24SpeculativeNumberModulusENS1_4TypeES3_
	.type	_ZN2v88internal8compiler14OperationTyper24SpeculativeNumberModulusENS1_4TypeES3_, @function
_ZN2v88internal8compiler14OperationTyper24SpeculativeNumberModulusENS1_4TypeES3_:
.LFB18776:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movq	%rsi, %rdi
	movl	$8396767, %esi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 12, -48
	movq	(%r14), %rdx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -48(%rbp)
	movq	%rax, %r12
	cmpq	$7263, %rax
	jne	.L1414
.L1390:
	movq	(%r14), %rdx
	movl	$8396767, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14OperationTyper8ToNumberENS1_4TypeE
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14OperationTyper13NumberModulusENS1_4TypeES3_
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1415
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1414:
	.cfi_restore_state
	leaq	-48(%rbp), %r15
	movl	$7263, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L1389
	movq	-48(%rbp), %r12
	jmp	.L1390
	.p2align 4,,10
	.p2align 3
.L1389:
	movl	$75448353, %esi
	movq	%r15, %rdi
	movl	$7263, %r12d
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1390
	movq	-48(%rbp), %rdi
	movq	(%r14), %rdx
	movl	$24575, %esi
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movl	$129, %esi
	movq	%r15, %rdi
	movq	%rax, -48(%rbp)
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1416
.L1392:
	movl	$257, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1417
.L1393:
	movq	48(%r14), %rsi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1418
.L1394:
	movq	56(%r14), %rsi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1419
.L1395:
	movq	(%r14), %rdx
	movq	-48(%rbp), %rdi
	movl	$7263, %esi
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, %r12
	jmp	.L1390
	.p2align 4,,10
	.p2align 3
.L1416:
	movq	8(%r14), %rax
	movq	(%r14), %rdx
	movq	-48(%rbp), %rdi
	movq	208(%rax), %rsi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -48(%rbp)
	jmp	.L1392
	.p2align 4,,10
	.p2align 3
.L1419:
	movq	8(%r14), %rax
	movq	(%r14), %rdx
	movq	-48(%rbp), %rdi
	movq	216(%rax), %rsi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -48(%rbp)
	jmp	.L1395
	.p2align 4,,10
	.p2align 3
.L1418:
	movq	8(%r14), %rax
	movq	(%r14), %rdx
	movq	-48(%rbp), %rdi
	movq	208(%rax), %rsi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -48(%rbp)
	jmp	.L1394
	.p2align 4,,10
	.p2align 3
.L1417:
	movq	(%r14), %rdx
	movq	-48(%rbp), %rdi
	movl	$4097, %esi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -48(%rbp)
	jmp	.L1393
.L1415:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18776:
	.size	_ZN2v88internal8compiler14OperationTyper24SpeculativeNumberModulusENS1_4TypeES3_, .-_ZN2v88internal8compiler14OperationTyper24SpeculativeNumberModulusENS1_4TypeES3_
	.section	.text._ZN2v88internal8compiler14OperationTyper26SpeculativeNumberBitwiseOrENS1_4TypeES3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper26SpeculativeNumberBitwiseOrENS1_4TypeES3_
	.type	_ZN2v88internal8compiler14OperationTyper26SpeculativeNumberBitwiseOrENS1_4TypeES3_, @function
_ZN2v88internal8compiler14OperationTyper26SpeculativeNumberBitwiseOrENS1_4TypeES3_:
.LFB18777:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movq	%rsi, %rdi
	movl	$8396767, %esi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 12, -48
	movq	(%r14), %rdx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -48(%rbp)
	movq	%rax, %r12
	cmpq	$7263, %rax
	jne	.L1447
.L1423:
	movq	(%r14), %rdx
	movl	$8396767, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14OperationTyper8ToNumberENS1_4TypeE
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14OperationTyper15NumberBitwiseOrENS1_4TypeES3_
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1448
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1447:
	.cfi_restore_state
	leaq	-48(%rbp), %r15
	movl	$7263, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L1422
	movq	-48(%rbp), %r12
	jmp	.L1423
	.p2align 4,,10
	.p2align 3
.L1422:
	movl	$75448353, %esi
	movq	%r15, %rdi
	movl	$7263, %r12d
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1423
	movq	-48(%rbp), %rdi
	movq	(%r14), %rdx
	movl	$24575, %esi
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movl	$129, %esi
	movq	%r15, %rdi
	movq	%rax, -48(%rbp)
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1449
.L1425:
	movl	$257, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1450
.L1426:
	movq	48(%r14), %rsi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1451
.L1427:
	movq	56(%r14), %rsi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1452
.L1428:
	movq	(%r14), %rdx
	movq	-48(%rbp), %rdi
	movl	$7263, %esi
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, %r12
	jmp	.L1423
	.p2align 4,,10
	.p2align 3
.L1449:
	movq	8(%r14), %rax
	movq	(%r14), %rdx
	movq	-48(%rbp), %rdi
	movq	208(%rax), %rsi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -48(%rbp)
	jmp	.L1425
	.p2align 4,,10
	.p2align 3
.L1452:
	movq	8(%r14), %rax
	movq	(%r14), %rdx
	movq	-48(%rbp), %rdi
	movq	216(%rax), %rsi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -48(%rbp)
	jmp	.L1428
	.p2align 4,,10
	.p2align 3
.L1451:
	movq	8(%r14), %rax
	movq	(%r14), %rdx
	movq	-48(%rbp), %rdi
	movq	208(%rax), %rsi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -48(%rbp)
	jmp	.L1427
	.p2align 4,,10
	.p2align 3
.L1450:
	movq	(%r14), %rdx
	movq	-48(%rbp), %rdi
	movl	$4097, %esi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -48(%rbp)
	jmp	.L1426
.L1448:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18777:
	.size	_ZN2v88internal8compiler14OperationTyper26SpeculativeNumberBitwiseOrENS1_4TypeES3_, .-_ZN2v88internal8compiler14OperationTyper26SpeculativeNumberBitwiseOrENS1_4TypeES3_
	.section	.text._ZN2v88internal8compiler14OperationTyper27SpeculativeNumberBitwiseAndENS1_4TypeES3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper27SpeculativeNumberBitwiseAndENS1_4TypeES3_
	.type	_ZN2v88internal8compiler14OperationTyper27SpeculativeNumberBitwiseAndENS1_4TypeES3_, @function
_ZN2v88internal8compiler14OperationTyper27SpeculativeNumberBitwiseAndENS1_4TypeES3_:
.LFB18778:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movq	%rsi, %rdi
	movl	$8396767, %esi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 12, -48
	movq	(%r14), %rdx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -48(%rbp)
	movq	%rax, %r12
	cmpq	$7263, %rax
	jne	.L1480
.L1456:
	movq	(%r14), %rdx
	movl	$8396767, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14OperationTyper8ToNumberENS1_4TypeE
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14OperationTyper16NumberBitwiseAndENS1_4TypeES3_
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1481
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1480:
	.cfi_restore_state
	leaq	-48(%rbp), %r15
	movl	$7263, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L1455
	movq	-48(%rbp), %r12
	jmp	.L1456
	.p2align 4,,10
	.p2align 3
.L1455:
	movl	$75448353, %esi
	movq	%r15, %rdi
	movl	$7263, %r12d
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1456
	movq	-48(%rbp), %rdi
	movq	(%r14), %rdx
	movl	$24575, %esi
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movl	$129, %esi
	movq	%r15, %rdi
	movq	%rax, -48(%rbp)
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1482
.L1458:
	movl	$257, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1483
.L1459:
	movq	48(%r14), %rsi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1484
.L1460:
	movq	56(%r14), %rsi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1485
.L1461:
	movq	(%r14), %rdx
	movq	-48(%rbp), %rdi
	movl	$7263, %esi
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, %r12
	jmp	.L1456
	.p2align 4,,10
	.p2align 3
.L1482:
	movq	8(%r14), %rax
	movq	(%r14), %rdx
	movq	-48(%rbp), %rdi
	movq	208(%rax), %rsi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -48(%rbp)
	jmp	.L1458
	.p2align 4,,10
	.p2align 3
.L1485:
	movq	8(%r14), %rax
	movq	(%r14), %rdx
	movq	-48(%rbp), %rdi
	movq	216(%rax), %rsi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -48(%rbp)
	jmp	.L1461
	.p2align 4,,10
	.p2align 3
.L1484:
	movq	8(%r14), %rax
	movq	(%r14), %rdx
	movq	-48(%rbp), %rdi
	movq	208(%rax), %rsi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -48(%rbp)
	jmp	.L1460
	.p2align 4,,10
	.p2align 3
.L1483:
	movq	(%r14), %rdx
	movq	-48(%rbp), %rdi
	movl	$4097, %esi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -48(%rbp)
	jmp	.L1459
.L1481:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18778:
	.size	_ZN2v88internal8compiler14OperationTyper27SpeculativeNumberBitwiseAndENS1_4TypeES3_, .-_ZN2v88internal8compiler14OperationTyper27SpeculativeNumberBitwiseAndENS1_4TypeES3_
	.section	.text._ZN2v88internal8compiler14OperationTyper27SpeculativeNumberBitwiseXorENS1_4TypeES3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper27SpeculativeNumberBitwiseXorENS1_4TypeES3_
	.type	_ZN2v88internal8compiler14OperationTyper27SpeculativeNumberBitwiseXorENS1_4TypeES3_, @function
_ZN2v88internal8compiler14OperationTyper27SpeculativeNumberBitwiseXorENS1_4TypeES3_:
.LFB18779:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%rsi, %rdi
	movl	$8396767, %esi
	subq	$72, %rsp
	movq	(%r12), %rdx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14OperationTyper8ToNumberENS1_4TypeE
	movq	(%r12), %rdx
	movl	$8396767, %esi
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14OperationTyper8ToNumberENS1_4TypeE
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r13, -56(%rbp)
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler14OperationTyper13NumberToInt32ENS1_4TypeE
	movq	-48(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal8compiler14OperationTyper13NumberToInt32ENS1_4TypeE
	movq	%rax, -48(%rbp)
	movq	%rax, %rdx
	movq	-56(%rbp), %rax
	cmpq	$1, %rax
	je	.L1487
	movq	%rdx, %rax
	cmpq	$1, %rdx
	jne	.L1516
.L1487:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1517
	addq	$72, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1516:
	.cfi_restore_state
	leaq	-56(%rbp), %r13
	leaq	-48(%rbp), %r12
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	movq	%r12, %rdi
	movsd	%xmm0, -72(%rbp)
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	movq	%r13, %rdi
	movsd	%xmm0, -80(%rbp)
	call	_ZNK2v88internal8compiler4Type3MaxEv@PLT
	movq	%r12, %rdi
	movsd	%xmm0, -88(%rbp)
	call	_ZNK2v88internal8compiler4Type3MaxEv@PLT
	movsd	-72(%rbp), %xmm1
	pxor	%xmm2, %xmm2
	comisd	%xmm2, %xmm1
	jb	.L1511
	movsd	-80(%rbp), %xmm3
	movl	$1027, %eax
	comisd	%xmm2, %xmm3
	jnb	.L1487
	comisd	-88(%rbp), %xmm2
	ja	.L1495
.L1496:
	comisd	%xmm0, %xmm2
	movl	$73, %eax
	movl	$1099, %edx
	cmovbe	%rdx, %rax
	jmp	.L1487
	.p2align 4,,10
	.p2align 3
.L1511:
	comisd	-88(%rbp), %xmm2
	ja	.L1495
.L1491:
	comisd	%xmm2, %xmm1
	movl	$1099, %eax
	jb	.L1487
	jmp	.L1496
	.p2align 4,,10
	.p2align 3
.L1495:
	comisd	%xmm0, %xmm2
	movl	$1027, %eax
	ja	.L1487
	movsd	-80(%rbp), %xmm6
	movl	$73, %eax
	comisd	%xmm2, %xmm6
	jnb	.L1487
	jmp	.L1491
.L1517:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18779:
	.size	_ZN2v88internal8compiler14OperationTyper27SpeculativeNumberBitwiseXorENS1_4TypeES3_, .-_ZN2v88internal8compiler14OperationTyper27SpeculativeNumberBitwiseXorENS1_4TypeES3_
	.section	.text._ZN2v88internal8compiler14OperationTyper26SpeculativeNumberShiftLeftENS1_4TypeES3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper26SpeculativeNumberShiftLeftENS1_4TypeES3_
	.type	_ZN2v88internal8compiler14OperationTyper26SpeculativeNumberShiftLeftENS1_4TypeES3_, @function
_ZN2v88internal8compiler14OperationTyper26SpeculativeNumberShiftLeftENS1_4TypeES3_:
.LFB18780:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movq	%rsi, %rdi
	movl	$8396767, %esi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 12, -48
	movq	(%r14), %rdx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -48(%rbp)
	movq	%rax, %r12
	cmpq	$7263, %rax
	jne	.L1545
.L1521:
	movq	(%r14), %rdx
	movl	$8396767, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14OperationTyper8ToNumberENS1_4TypeE
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14OperationTyper15NumberShiftLeftENS1_4TypeES3_
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1546
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1545:
	.cfi_restore_state
	leaq	-48(%rbp), %r15
	movl	$7263, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L1520
	movq	-48(%rbp), %r12
	jmp	.L1521
	.p2align 4,,10
	.p2align 3
.L1520:
	movl	$75448353, %esi
	movq	%r15, %rdi
	movl	$7263, %r12d
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1521
	movq	-48(%rbp), %rdi
	movq	(%r14), %rdx
	movl	$24575, %esi
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movl	$129, %esi
	movq	%r15, %rdi
	movq	%rax, -48(%rbp)
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1547
.L1523:
	movl	$257, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1548
.L1524:
	movq	48(%r14), %rsi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1549
.L1525:
	movq	56(%r14), %rsi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1550
.L1526:
	movq	(%r14), %rdx
	movq	-48(%rbp), %rdi
	movl	$7263, %esi
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, %r12
	jmp	.L1521
	.p2align 4,,10
	.p2align 3
.L1547:
	movq	8(%r14), %rax
	movq	(%r14), %rdx
	movq	-48(%rbp), %rdi
	movq	208(%rax), %rsi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -48(%rbp)
	jmp	.L1523
	.p2align 4,,10
	.p2align 3
.L1550:
	movq	8(%r14), %rax
	movq	(%r14), %rdx
	movq	-48(%rbp), %rdi
	movq	216(%rax), %rsi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -48(%rbp)
	jmp	.L1526
	.p2align 4,,10
	.p2align 3
.L1549:
	movq	8(%r14), %rax
	movq	(%r14), %rdx
	movq	-48(%rbp), %rdi
	movq	208(%rax), %rsi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -48(%rbp)
	jmp	.L1525
	.p2align 4,,10
	.p2align 3
.L1548:
	movq	(%r14), %rdx
	movq	-48(%rbp), %rdi
	movl	$4097, %esi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -48(%rbp)
	jmp	.L1524
.L1546:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18780:
	.size	_ZN2v88internal8compiler14OperationTyper26SpeculativeNumberShiftLeftENS1_4TypeES3_, .-_ZN2v88internal8compiler14OperationTyper26SpeculativeNumberShiftLeftENS1_4TypeES3_
	.section	.text._ZN2v88internal8compiler14OperationTyper27SpeculativeNumberShiftRightENS1_4TypeES3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper27SpeculativeNumberShiftRightENS1_4TypeES3_
	.type	_ZN2v88internal8compiler14OperationTyper27SpeculativeNumberShiftRightENS1_4TypeES3_, @function
_ZN2v88internal8compiler14OperationTyper27SpeculativeNumberShiftRightENS1_4TypeES3_:
.LFB18781:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	movl	$8396767, %esi
	subq	$24, %rsp
	movq	(%rdi), %rdx
	movq	%rbx, %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14OperationTyper8ToNumberENS1_4TypeE
	movq	(%r12), %rdx
	movl	$8396767, %esi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -48(%rbp)
	movq	%rax, %rdx
	cmpq	$7263, %rax
	jne	.L1578
.L1554:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14OperationTyper16NumberShiftRightENS1_4TypeES3_
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1579
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1578:
	.cfi_restore_state
	leaq	-48(%rbp), %r13
	movl	$7263, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L1553
	movq	-48(%rbp), %rdx
	jmp	.L1554
	.p2align 4,,10
	.p2align 3
.L1553:
	movl	$75448353, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	movl	$7263, %edx
	testb	%al, %al
	jne	.L1554
	movq	-48(%rbp), %rdi
	movq	(%r12), %rdx
	movl	$24575, %esi
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movl	$129, %esi
	movq	%r13, %rdi
	movq	%rax, -48(%rbp)
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1580
.L1556:
	movl	$257, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1581
.L1557:
	movq	48(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1582
.L1558:
	movq	56(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1583
.L1559:
	movq	(%r12), %rdx
	movq	-48(%rbp), %rdi
	movl	$7263, %esi
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, %rdx
	jmp	.L1554
	.p2align 4,,10
	.p2align 3
.L1580:
	movq	8(%r12), %rax
	movq	(%r12), %rdx
	movq	-48(%rbp), %rdi
	movq	208(%rax), %rsi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -48(%rbp)
	jmp	.L1556
	.p2align 4,,10
	.p2align 3
.L1583:
	movq	8(%r12), %rax
	movq	(%r12), %rdx
	movq	-48(%rbp), %rdi
	movq	216(%rax), %rsi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -48(%rbp)
	jmp	.L1559
	.p2align 4,,10
	.p2align 3
.L1582:
	movq	8(%r12), %rax
	movq	(%r12), %rdx
	movq	-48(%rbp), %rdi
	movq	208(%rax), %rsi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -48(%rbp)
	jmp	.L1558
	.p2align 4,,10
	.p2align 3
.L1581:
	movq	(%r12), %rdx
	movq	-48(%rbp), %rdi
	movl	$4097, %esi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -48(%rbp)
	jmp	.L1557
.L1579:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18781:
	.size	_ZN2v88internal8compiler14OperationTyper27SpeculativeNumberShiftRightENS1_4TypeES3_, .-_ZN2v88internal8compiler14OperationTyper27SpeculativeNumberShiftRightENS1_4TypeES3_
	.section	.text._ZN2v88internal8compiler14OperationTyper34SpeculativeNumberShiftRightLogicalENS1_4TypeES3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper34SpeculativeNumberShiftRightLogicalENS1_4TypeES3_
	.type	_ZN2v88internal8compiler14OperationTyper34SpeculativeNumberShiftRightLogicalENS1_4TypeES3_, @function
_ZN2v88internal8compiler14OperationTyper34SpeculativeNumberShiftRightLogicalENS1_4TypeES3_:
.LFB18782:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	movl	$8396767, %esi
	subq	$24, %rsp
	movq	(%rdi), %rdx
	movq	%rbx, %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14OperationTyper8ToNumberENS1_4TypeE
	movq	(%r12), %rdx
	movl	$8396767, %esi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -48(%rbp)
	movq	%rax, %rdx
	cmpq	$7263, %rax
	jne	.L1611
.L1587:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14OperationTyper23NumberShiftRightLogicalENS1_4TypeES3_
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1612
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1611:
	.cfi_restore_state
	leaq	-48(%rbp), %r13
	movl	$7263, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L1586
	movq	-48(%rbp), %rdx
	jmp	.L1587
	.p2align 4,,10
	.p2align 3
.L1586:
	movl	$75448353, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	movl	$7263, %edx
	testb	%al, %al
	jne	.L1587
	movq	-48(%rbp), %rdi
	movq	(%r12), %rdx
	movl	$24575, %esi
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movl	$129, %esi
	movq	%r13, %rdi
	movq	%rax, -48(%rbp)
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1613
.L1589:
	movl	$257, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1614
.L1590:
	movq	48(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1615
.L1591:
	movq	56(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1616
.L1592:
	movq	(%r12), %rdx
	movq	-48(%rbp), %rdi
	movl	$7263, %esi
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, %rdx
	jmp	.L1587
	.p2align 4,,10
	.p2align 3
.L1613:
	movq	8(%r12), %rax
	movq	(%r12), %rdx
	movq	-48(%rbp), %rdi
	movq	208(%rax), %rsi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -48(%rbp)
	jmp	.L1589
	.p2align 4,,10
	.p2align 3
.L1616:
	movq	8(%r12), %rax
	movq	(%r12), %rdx
	movq	-48(%rbp), %rdi
	movq	216(%rax), %rsi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -48(%rbp)
	jmp	.L1592
	.p2align 4,,10
	.p2align 3
.L1615:
	movq	8(%r12), %rax
	movq	(%r12), %rdx
	movq	-48(%rbp), %rdi
	movq	208(%rax), %rsi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -48(%rbp)
	jmp	.L1591
	.p2align 4,,10
	.p2align 3
.L1614:
	movq	(%r12), %rdx
	movq	-48(%rbp), %rdi
	movl	$4097, %esi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -48(%rbp)
	jmp	.L1590
.L1612:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18782:
	.size	_ZN2v88internal8compiler14OperationTyper34SpeculativeNumberShiftRightLogicalENS1_4TypeES3_, .-_ZN2v88internal8compiler14OperationTyper34SpeculativeNumberShiftRightLogicalENS1_4TypeES3_
	.section	.text._ZN2v88internal8compiler14OperationTyper9BigIntAddENS1_4TypeES3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper9BigIntAddENS1_4TypeES3_
	.type	_ZN2v88internal8compiler14OperationTyper9BigIntAddENS1_4TypeES3_, @function
_ZN2v88internal8compiler14OperationTyper9BigIntAddENS1_4TypeES3_:
.LFB18783:
	.cfi_startproc
	endbr64
	movq	%rsi, %rax
	cmpq	$1, %rsi
	je	.L1618
	cmpq	$1, %rdx
	movl	$134217729, %eax
	cmove	%rdx, %rax
.L1618:
	ret
	.cfi_endproc
.LFE18783:
	.size	_ZN2v88internal8compiler14OperationTyper9BigIntAddENS1_4TypeES3_, .-_ZN2v88internal8compiler14OperationTyper9BigIntAddENS1_4TypeES3_
	.section	.text._ZN2v88internal8compiler14OperationTyper12BigIntNegateENS1_4TypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper12BigIntNegateENS1_4TypeE
	.type	_ZN2v88internal8compiler14OperationTyper12BigIntNegateENS1_4TypeE, @function
_ZN2v88internal8compiler14OperationTyper12BigIntNegateENS1_4TypeE:
.LFB18784:
	.cfi_startproc
	endbr64
	cmpq	$1, %rsi
	movl	$1, %edx
	movl	$134217729, %eax
	cmove	%rdx, %rax
	ret
	.cfi_endproc
.LFE18784:
	.size	_ZN2v88internal8compiler14OperationTyper12BigIntNegateENS1_4TypeE, .-_ZN2v88internal8compiler14OperationTyper12BigIntNegateENS1_4TypeE
	.section	.text._ZN2v88internal8compiler14OperationTyper20SpeculativeBigIntAddENS1_4TypeES3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper20SpeculativeBigIntAddENS1_4TypeES3_
	.type	_ZN2v88internal8compiler14OperationTyper20SpeculativeBigIntAddENS1_4TypeES3_, @function
_ZN2v88internal8compiler14OperationTyper20SpeculativeBigIntAddENS1_4TypeES3_:
.LFB22729:
	.cfi_startproc
	endbr64
	movq	%rsi, %rax
	cmpq	$1, %rsi
	je	.L1628
	cmpq	$1, %rdx
	movl	$1, %eax
	movl	$134217729, %esi
	cmovne	%rsi, %rax
.L1628:
	ret
	.cfi_endproc
.LFE22729:
	.size	_ZN2v88internal8compiler14OperationTyper20SpeculativeBigIntAddENS1_4TypeES3_, .-_ZN2v88internal8compiler14OperationTyper20SpeculativeBigIntAddENS1_4TypeES3_
	.section	.text._ZN2v88internal8compiler14OperationTyper23SpeculativeBigIntNegateENS1_4TypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper23SpeculativeBigIntNegateENS1_4TypeE
	.type	_ZN2v88internal8compiler14OperationTyper23SpeculativeBigIntNegateENS1_4TypeE, @function
_ZN2v88internal8compiler14OperationTyper23SpeculativeBigIntNegateENS1_4TypeE:
.LFB22725:
	.cfi_startproc
	endbr64
	movq	%rsi, %rax
	cmpq	$1, %rsi
	movl	$134217729, %edx
	cmovne	%rdx, %rax
	ret
	.cfi_endproc
.LFE22725:
	.size	_ZN2v88internal8compiler14OperationTyper23SpeculativeBigIntNegateENS1_4TypeE, .-_ZN2v88internal8compiler14OperationTyper23SpeculativeBigIntNegateENS1_4TypeE
	.section	.text._ZN2v88internal8compiler14OperationTyper19SpeculativeToNumberENS1_4TypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper19SpeculativeToNumberENS1_4TypeE
	.type	_ZN2v88internal8compiler14OperationTyper19SpeculativeToNumberENS1_4TypeE, @function
_ZN2v88internal8compiler14OperationTyper19SpeculativeToNumberENS1_4TypeE:
.LFB18787:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movl	$8396767, %esi
	subq	$16, %rsp
	movq	(%rbx), %rdx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -32(%rbp)
	cmpq	$7263, %rax
	jne	.L1663
.L1639:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1664
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1663:
	.cfi_restore_state
	leaq	-32(%rbp), %r12
	movl	$7263, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L1638
	movq	-32(%rbp), %rax
	jmp	.L1639
	.p2align 4,,10
	.p2align 3
.L1638:
	movl	$75448353, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	movl	%eax, %r8d
	movl	$7263, %eax
	testb	%r8b, %r8b
	jne	.L1639
	movq	-32(%rbp), %rdi
	movq	(%rbx), %rdx
	movl	$24575, %esi
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movl	$129, %esi
	movq	%r12, %rdi
	movq	%rax, -32(%rbp)
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1665
.L1641:
	movl	$257, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1666
.L1642:
	movq	48(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1667
.L1643:
	movq	56(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1668
.L1644:
	movq	(%rbx), %rdx
	movq	-32(%rbp), %rdi
	movl	$7263, %esi
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	jmp	.L1639
	.p2align 4,,10
	.p2align 3
.L1665:
	movq	8(%rbx), %rax
	movq	(%rbx), %rdx
	movq	-32(%rbp), %rdi
	movq	208(%rax), %rsi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -32(%rbp)
	jmp	.L1641
	.p2align 4,,10
	.p2align 3
.L1668:
	movq	8(%rbx), %rax
	movq	(%rbx), %rdx
	movq	-32(%rbp), %rdi
	movq	216(%rax), %rsi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -32(%rbp)
	jmp	.L1644
	.p2align 4,,10
	.p2align 3
.L1667:
	movq	8(%rbx), %rax
	movq	(%rbx), %rdx
	movq	-32(%rbp), %rdi
	movq	208(%rax), %rsi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -32(%rbp)
	jmp	.L1643
	.p2align 4,,10
	.p2align 3
.L1666:
	movq	(%rbx), %rdx
	movq	-32(%rbp), %rdi
	movl	$4097, %esi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -32(%rbp)
	jmp	.L1642
.L1664:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18787:
	.size	_ZN2v88internal8compiler14OperationTyper19SpeculativeToNumberENS1_4TypeE, .-_ZN2v88internal8compiler14OperationTyper19SpeculativeToNumberENS1_4TypeE
	.section	.text._ZN2v88internal8compiler14OperationTyper11ToPrimitiveENS1_4TypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper11ToPrimitiveENS1_4TypeE
	.type	_ZN2v88internal8compiler14OperationTyper11ToPrimitiveENS1_4TypeE, @function
_ZN2v88internal8compiler14OperationTyper11ToPrimitiveENS1_4TypeE:
.LFB18788:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rsi, -8(%rbp)
	cmpq	$134250495, %rsi
	jne	.L1673
.L1670:
	movq	-8(%rbp), %rax
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1673:
	.cfi_restore_state
	leaq	-8(%rbp), %rdi
	movl	$134250495, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1670
	leave
	.cfi_def_cfa 7, 8
	movl	$134250495, %eax
	ret
	.cfi_endproc
.LFE18788:
	.size	_ZN2v88internal8compiler14OperationTyper11ToPrimitiveENS1_4TypeE, .-_ZN2v88internal8compiler14OperationTyper11ToPrimitiveENS1_4TypeE
	.section	.rodata._ZN2v88internal8compiler14OperationTyper6InvertENS1_4TypeE.str1.1,"aMS",@progbits,1
.LC9:
	.string	"!type.IsNone()"
.LC10:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal8compiler14OperationTyper6InvertENS1_4TypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper6InvertENS1_4TypeE
	.type	_ZN2v88internal8compiler14OperationTyper6InvertENS1_4TypeE, @function
_ZN2v88internal8compiler14OperationTyper6InvertENS1_4TypeE:
.LFB18789:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rsi, -24(%rbp)
	cmpq	$1, %rsi
	je	.L1682
	movq	%rsi, %rax
	movq	48(%rdi), %rsi
	movq	%rdi, %rbx
	cmpq	%rsi, %rax
	jne	.L1683
.L1676:
	movq	56(%rbx), %rax
.L1679:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1683:
	.cfi_restore_state
	leaq	-24(%rbp), %r12
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1676
	movq	56(%rbx), %rsi
	cmpq	%rsi, -24(%rbp)
	jne	.L1684
.L1677:
	movq	48(%rbx), %rax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1684:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1677
	movq	-24(%rbp), %rax
	jmp	.L1679
	.p2align 4,,10
	.p2align 3
.L1682:
	leaq	.LC9(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18789:
	.size	_ZN2v88internal8compiler14OperationTyper6InvertENS1_4TypeE, .-_ZN2v88internal8compiler14OperationTyper6InvertENS1_4TypeE
	.section	.text._ZN2v88internal8compiler14OperationTyper6InvertENS_4base5FlagsINS2_22ComparisonOutcomeFlagsEiEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper6InvertENS_4base5FlagsINS2_22ComparisonOutcomeFlagsEiEE
	.type	_ZN2v88internal8compiler14OperationTyper6InvertENS_4base5FlagsINS2_22ComparisonOutcomeFlagsEiEE, @function
_ZN2v88internal8compiler14OperationTyper6InvertENS_4base5FlagsINS2_22ComparisonOutcomeFlagsEiEE:
.LFB18790:
	.cfi_startproc
	endbr64
	movl	%esi, %edx
	movl	$2, %eax
	andl	$4, %edx
	je	.L1686
	movl	$6, %eax
	movl	$4, %edx
.L1686:
	testb	$1, %sil
	cmove	%edx, %eax
	movl	%eax, %edx
	orl	$1, %edx
	andl	$2, %esi
	cmovne	%edx, %eax
	ret
	.cfi_endproc
.LFE18790:
	.size	_ZN2v88internal8compiler14OperationTyper6InvertENS_4base5FlagsINS2_22ComparisonOutcomeFlagsEiEE, .-_ZN2v88internal8compiler14OperationTyper6InvertENS_4base5FlagsINS2_22ComparisonOutcomeFlagsEiEE
	.section	.text._ZN2v88internal8compiler14OperationTyper16FalsifyUndefinedENS_4base5FlagsINS2_22ComparisonOutcomeFlagsEiEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper16FalsifyUndefinedENS_4base5FlagsINS2_22ComparisonOutcomeFlagsEiEE
	.type	_ZN2v88internal8compiler14OperationTyper16FalsifyUndefinedENS_4base5FlagsINS2_22ComparisonOutcomeFlagsEiEE, @function
_ZN2v88internal8compiler14OperationTyper16FalsifyUndefinedENS_4base5FlagsINS2_22ComparisonOutcomeFlagsEiEE:
.LFB18800:
	.cfi_startproc
	endbr64
	testb	$6, %sil
	je	.L1694
	andl	$1, %esi
	movl	$513, %eax
	je	.L1699
	ret
	.p2align 4,,10
	.p2align 3
.L1699:
	movq	48(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1694:
	movq	56(%rdi), %rax
	ret
	.cfi_endproc
.LFE18800:
	.size	_ZN2v88internal8compiler14OperationTyper16FalsifyUndefinedENS_4base5FlagsINS2_22ComparisonOutcomeFlagsEiEE, .-_ZN2v88internal8compiler14OperationTyper16FalsifyUndefinedENS_4base5FlagsINS2_22ComparisonOutcomeFlagsEiEE
	.section	.text._ZN2v88internal8compiler14OperationTyper9SameValueENS1_4TypeES3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper9SameValueENS1_4TypeES3_
	.type	_ZN2v88internal8compiler14OperationTyper9SameValueENS1_4TypeES3_, @function
_ZN2v88internal8compiler14OperationTyper9SameValueENS1_4TypeES3_:
.LFB18802:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%rsi, -72(%rbp)
	movq	%rdx, -80(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -40(%rbp)
	xorl	%ecx, %ecx
	movq	%rsi, -48(%rbp)
	cmpq	$513, %rsi
	jne	.L1783
.L1701:
	movl	$513, %eax
.L1704:
	movq	%rax, -56(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -48(%rbp)
	cmpq	$513, %rax
	jne	.L1784
.L1717:
	movl	$513, %esi
.L1720:
	leaq	-56(%rbp), %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1733
.L1754:
	movq	48(%rbx), %rax
.L1734:
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L1785
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1733:
	.cfi_restore_state
	cmpq	$4097, -72(%rbp)
	je	.L1735
	leaq	-72(%rbp), %r13
	movl	$4097, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1735
	cmpq	$4097, -80(%rbp)
	je	.L1741
	leaq	-80(%rbp), %r12
	movl	$4097, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1741
.L1737:
	cmpq	$2049, -72(%rbp)
	je	.L1742
	leaq	-72(%rbp), %r13
	movl	$2049, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1742
	cmpq	$2049, -80(%rbp)
	je	.L1748
	leaq	-80(%rbp), %r12
	movl	$2049, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L1744
.L1748:
	movl	$2049, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1744
	jmp	.L1754
	.p2align 4,,10
	.p2align 3
.L1735:
	cmpq	$4097, -80(%rbp)
	jne	.L1786
.L1746:
	movq	56(%rbx), %rax
	jmp	.L1734
	.p2align 4,,10
	.p2align 3
.L1784:
	leaq	-48(%rbp), %r12
	movl	$513, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1717
	cmpq	$16417, -48(%rbp)
	jne	.L1787
.L1718:
	movl	$16417, %esi
	jmp	.L1720
	.p2align 4,,10
	.p2align 3
.L1783:
	leaq	-48(%rbp), %r12
	movl	$513, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1701
	cmpq	$16417, -48(%rbp)
	jne	.L1788
.L1702:
	movl	$16417, %eax
	jmp	.L1704
	.p2align 4,,10
	.p2align 3
.L1786:
	leaq	-80(%rbp), %r12
	movl	$4097, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1746
	movl	$4097, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1737
	jmp	.L1754
	.p2align 4,,10
	.p2align 3
.L1787:
	movl	$16417, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1718
	cmpq	$7263, -48(%rbp)
	jne	.L1789
.L1721:
	movl	$7263, %esi
	jmp	.L1720
	.p2align 4,,10
	.p2align 3
.L1788:
	movl	$16417, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1702
	cmpq	$7263, -48(%rbp)
	jne	.L1790
.L1705:
	movl	$7263, %eax
	jmp	.L1704
	.p2align 4,,10
	.p2align 3
.L1742:
	cmpq	$2049, -80(%rbp)
	je	.L1746
	leaq	-80(%rbp), %r12
	movl	$2049, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1746
	movl	$2049, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	je	.L1754
.L1744:
	cmpq	$3167, -72(%rbp)
	je	.L1752
	leaq	-72(%rbp), %rdi
	movl	$3167, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1752
.L1753:
	movl	$513, %eax
	jmp	.L1734
	.p2align 4,,10
	.p2align 3
.L1741:
	movl	$4097, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1737
	jmp	.L1754
	.p2align 4,,10
	.p2align 3
.L1790:
	movl	$7263, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1705
	cmpq	$134217729, -48(%rbp)
	je	.L1707
	movl	$134217729, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1707
	cmpq	$257, -48(%rbp)
	je	.L1709
	movl	$257, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1709
	cmpq	$129, -48(%rbp)
	je	.L1711
	movl	$129, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1711
	cmpq	$8193, -48(%rbp)
	je	.L1713
	movl	$8193, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1713
	cmpq	$75431937, -48(%rbp)
	je	.L1715
	movl	$75431937, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	movl	%eax, %r8d
	movl	$4294967295, %eax
	testb	%r8b, %r8b
	je	.L1704
.L1715:
	movl	$75431937, %eax
	jmp	.L1704
	.p2align 4,,10
	.p2align 3
.L1789:
	movl	$7263, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1721
	cmpq	$134217729, -48(%rbp)
	je	.L1723
	movl	$134217729, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1723
	cmpq	$257, -48(%rbp)
	je	.L1725
	movl	$257, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1725
	cmpq	$129, -48(%rbp)
	je	.L1727
	movl	$129, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1727
	cmpq	$8193, -48(%rbp)
	je	.L1729
	movl	$8193, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1729
	cmpq	$75431937, -48(%rbp)
	je	.L1731
	movl	$75431937, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	movl	$4294967295, %esi
	testb	%al, %al
	je	.L1720
.L1731:
	movl	$75431937, %esi
	jmp	.L1720
	.p2align 4,,10
	.p2align 3
.L1752:
	cmpq	$3167, -80(%rbp)
	leaq	-80(%rbp), %r12
	je	.L1750
	movl	$3167, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L1753
.L1750:
	leaq	-72(%rbp), %r13
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type3MaxEv@PLT
	movq	%r12, %rdi
	movsd	%xmm0, -88(%rbp)
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	comisd	-88(%rbp), %xmm0
	ja	.L1754
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	movq	%r12, %rdi
	movsd	%xmm0, -88(%rbp)
	call	_ZNK2v88internal8compiler4Type3MaxEv@PLT
	movsd	-88(%rbp), %xmm1
	comisd	%xmm0, %xmm1
	ja	.L1754
	jmp	.L1753
	.p2align 4,,10
	.p2align 3
.L1707:
	movl	$134217729, %eax
	jmp	.L1704
	.p2align 4,,10
	.p2align 3
.L1723:
	movl	$134217729, %esi
	jmp	.L1720
.L1725:
	movl	$257, %esi
	jmp	.L1720
.L1709:
	movl	$257, %eax
	jmp	.L1704
.L1711:
	movl	$129, %eax
	jmp	.L1704
.L1727:
	movl	$129, %esi
	jmp	.L1720
.L1785:
	call	__stack_chk_fail@PLT
.L1729:
	movl	$8193, %esi
	jmp	.L1720
.L1713:
	movl	$8193, %eax
	jmp	.L1704
	.cfi_endproc
.LFE18802:
	.size	_ZN2v88internal8compiler14OperationTyper9SameValueENS1_4TypeES3_, .-_ZN2v88internal8compiler14OperationTyper9SameValueENS1_4TypeES3_
	.section	.text._ZN2v88internal8compiler14OperationTyper20SameValueNumbersOnlyENS1_4TypeES3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper20SameValueNumbersOnlyENS1_4TypeES3_
	.type	_ZN2v88internal8compiler14OperationTyper20SameValueNumbersOnlyENS1_4TypeES3_, @function
_ZN2v88internal8compiler14OperationTyper20SameValueNumbersOnlyENS1_4TypeES3_:
.LFB18803:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rsi, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rsi, -48(%rbp)
	cmpq	$513, %rsi
	jne	.L1874
.L1792:
	movl	$513, %eax
.L1795:
	movq	%rax, -56(%rbp)
	movq	-64(%rbp), %rax
	movq	%rax, -48(%rbp)
	cmpq	$513, %rax
	jne	.L1875
.L1808:
	movl	$513, %esi
.L1811:
	leaq	-56(%rbp), %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1824
.L1845:
	movq	48(%rbx), %rax
.L1825:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1876
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1824:
	.cfi_restore_state
	cmpq	$4097, -72(%rbp)
	je	.L1826
	leaq	-72(%rbp), %r13
	movl	$4097, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1826
	cmpq	$4097, -64(%rbp)
	je	.L1832
	leaq	-64(%rbp), %r12
	movl	$4097, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1832
.L1828:
	cmpq	$2049, -72(%rbp)
	je	.L1833
	leaq	-72(%rbp), %r13
	movl	$2049, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1833
	cmpq	$2049, -64(%rbp)
	je	.L1839
	leaq	-64(%rbp), %r12
	movl	$2049, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L1835
.L1839:
	movl	$2049, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1835
	jmp	.L1845
	.p2align 4,,10
	.p2align 3
.L1826:
	cmpq	$4097, -64(%rbp)
	jne	.L1877
.L1837:
	movq	56(%rbx), %rax
	jmp	.L1825
	.p2align 4,,10
	.p2align 3
.L1875:
	leaq	-48(%rbp), %r12
	movl	$513, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1808
	cmpq	$16417, -48(%rbp)
	jne	.L1878
.L1809:
	movl	$16417, %esi
	jmp	.L1811
	.p2align 4,,10
	.p2align 3
.L1874:
	leaq	-48(%rbp), %r12
	movl	$513, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1792
	cmpq	$16417, -48(%rbp)
	jne	.L1879
.L1793:
	movl	$16417, %eax
	jmp	.L1795
	.p2align 4,,10
	.p2align 3
.L1877:
	leaq	-64(%rbp), %r12
	movl	$4097, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1837
	movl	$4097, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1828
	jmp	.L1845
	.p2align 4,,10
	.p2align 3
.L1878:
	movl	$16417, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1809
	cmpq	$7263, -48(%rbp)
	jne	.L1880
.L1812:
	movl	$7263, %esi
	jmp	.L1811
	.p2align 4,,10
	.p2align 3
.L1879:
	movl	$16417, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1793
	cmpq	$7263, -48(%rbp)
	jne	.L1881
.L1796:
	movl	$7263, %eax
	jmp	.L1795
	.p2align 4,,10
	.p2align 3
.L1833:
	cmpq	$2049, -64(%rbp)
	je	.L1837
	leaq	-64(%rbp), %r12
	movl	$2049, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1837
	movl	$2049, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	je	.L1845
.L1835:
	cmpq	$3167, -72(%rbp)
	je	.L1843
	leaq	-72(%rbp), %rdi
	movl	$3167, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1843
.L1844:
	movl	$513, %eax
	jmp	.L1825
	.p2align 4,,10
	.p2align 3
.L1832:
	movl	$4097, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1828
	jmp	.L1845
	.p2align 4,,10
	.p2align 3
.L1881:
	movl	$7263, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1796
	cmpq	$134217729, -48(%rbp)
	je	.L1798
	movl	$134217729, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1798
	cmpq	$257, -48(%rbp)
	je	.L1800
	movl	$257, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1800
	cmpq	$129, -48(%rbp)
	je	.L1802
	movl	$129, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1802
	cmpq	$8193, -48(%rbp)
	je	.L1804
	movl	$8193, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1804
	cmpq	$75431937, -48(%rbp)
	je	.L1806
	movl	$75431937, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	movl	%eax, %r8d
	movl	$4294967295, %eax
	testb	%r8b, %r8b
	je	.L1795
.L1806:
	movl	$75431937, %eax
	jmp	.L1795
	.p2align 4,,10
	.p2align 3
.L1880:
	movl	$7263, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1812
	cmpq	$134217729, -48(%rbp)
	je	.L1814
	movl	$134217729, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1814
	cmpq	$257, -48(%rbp)
	je	.L1816
	movl	$257, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1816
	cmpq	$129, -48(%rbp)
	je	.L1818
	movl	$129, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1818
	cmpq	$8193, -48(%rbp)
	je	.L1820
	movl	$8193, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1820
	cmpq	$75431937, -48(%rbp)
	je	.L1822
	movl	$75431937, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	movl	$4294967295, %esi
	testb	%al, %al
	je	.L1811
.L1822:
	movl	$75431937, %esi
	jmp	.L1811
	.p2align 4,,10
	.p2align 3
.L1843:
	cmpq	$3167, -64(%rbp)
	leaq	-64(%rbp), %r12
	je	.L1841
	movl	$3167, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L1844
.L1841:
	leaq	-72(%rbp), %r13
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type3MaxEv@PLT
	movq	%r12, %rdi
	movsd	%xmm0, -88(%rbp)
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	comisd	-88(%rbp), %xmm0
	ja	.L1845
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	movq	%r12, %rdi
	movsd	%xmm0, -88(%rbp)
	call	_ZNK2v88internal8compiler4Type3MaxEv@PLT
	movsd	-88(%rbp), %xmm1
	comisd	%xmm0, %xmm1
	ja	.L1845
	jmp	.L1844
	.p2align 4,,10
	.p2align 3
.L1798:
	movl	$134217729, %eax
	jmp	.L1795
	.p2align 4,,10
	.p2align 3
.L1814:
	movl	$134217729, %esi
	jmp	.L1811
.L1816:
	movl	$257, %esi
	jmp	.L1811
.L1800:
	movl	$257, %eax
	jmp	.L1795
.L1802:
	movl	$129, %eax
	jmp	.L1795
.L1818:
	movl	$129, %esi
	jmp	.L1811
.L1876:
	call	__stack_chk_fail@PLT
.L1820:
	movl	$8193, %esi
	jmp	.L1811
.L1804:
	movl	$8193, %eax
	jmp	.L1795
	.cfi_endproc
.LFE18803:
	.size	_ZN2v88internal8compiler14OperationTyper20SameValueNumbersOnlyENS1_4TypeES3_, .-_ZN2v88internal8compiler14OperationTyper20SameValueNumbersOnlyENS1_4TypeES3_
	.section	.rodata._ZN2v88internal8compiler14OperationTyper11StrictEqualENS1_4TypeES3_.str1.1,"aMS",@progbits,1
.LC11:
	.string	"!lhs.IsNone()"
.LC12:
	.string	"!rhs.IsNone()"
	.section	.text._ZN2v88internal8compiler14OperationTyper11StrictEqualENS1_4TypeES3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper11StrictEqualENS1_4TypeES3_
	.type	_ZN2v88internal8compiler14OperationTyper11StrictEqualENS1_4TypeES3_, @function
_ZN2v88internal8compiler14OperationTyper11StrictEqualENS1_4TypeES3_:
.LFB18804:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rsi, -72(%rbp)
	movq	%rdx, -80(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -40(%rbp)
	xorl	%ecx, %ecx
	cmpq	$1, %rsi
	je	.L1963
	cmpq	$1, -80(%rbp)
	je	.L1964
	movq	%rsi, -48(%rbp)
	movq	%rdi, %rbx
	cmpq	$513, %rsi
	jne	.L1965
.L1885:
	movl	$513, %eax
.L1888:
	movq	%rax, -56(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -48(%rbp)
	cmpq	$513, %rax
	jne	.L1966
.L1901:
	movl	$513, %esi
.L1904:
	leaq	-56(%rbp), %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1917
.L1961:
	movq	48(%rbx), %rax
.L1918:
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L1967
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1917:
	.cfi_restore_state
	cmpq	$4097, -72(%rbp)
	je	.L1961
	leaq	-72(%rbp), %r12
	movl	$4097, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1961
	cmpq	$4097, -80(%rbp)
	je	.L1961
	leaq	-80(%rbp), %r13
	movl	$4097, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1961
	cmpq	$7263, -72(%rbp)
	je	.L1923
	movl	$7263, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1923
.L1924:
	cmpq	$8388609, -72(%rbp)
	je	.L1930
	movl	$8388609, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1930
	cmpq	$8388609, -80(%rbp)
	je	.L1930
	movl	$8388609, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1930
.L1928:
	movq	-72(%rbp), %rsi
	testb	$1, %sil
	jne	.L1932
	movl	(%rsi), %eax
	testl	%eax, %eax
	jne	.L1932
	cmpq	-80(%rbp), %rsi
	je	.L1933
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L1932
.L1933:
	movq	56(%rbx), %rax
	jmp	.L1918
	.p2align 4,,10
	.p2align 3
.L1966:
	leaq	-48(%rbp), %r12
	movl	$513, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1901
	cmpq	$16417, -48(%rbp)
	je	.L1902
	movl	$16417, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1902
	cmpq	$7263, -48(%rbp)
	je	.L1905
	movl	$7263, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1905
	cmpq	$134217729, -48(%rbp)
	je	.L1907
	movl	$134217729, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1907
	cmpq	$257, -48(%rbp)
	je	.L1909
	movl	$257, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1909
	cmpq	$129, -48(%rbp)
	je	.L1911
	movl	$129, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1911
	cmpq	$8193, -48(%rbp)
	je	.L1913
	movl	$8193, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1913
	cmpq	$75431937, -48(%rbp)
	je	.L1915
	movl	$75431937, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	movl	$4294967295, %esi
	testb	%al, %al
	je	.L1904
.L1915:
	movl	$75431937, %esi
	jmp	.L1904
	.p2align 4,,10
	.p2align 3
.L1965:
	leaq	-48(%rbp), %r12
	movl	$513, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1885
	cmpq	$16417, -48(%rbp)
	je	.L1886
	movl	$16417, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1886
	cmpq	$7263, -48(%rbp)
	je	.L1889
	movl	$7263, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1889
	cmpq	$134217729, -48(%rbp)
	je	.L1891
	movl	$134217729, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1891
	cmpq	$257, -48(%rbp)
	je	.L1893
	movl	$257, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1893
	cmpq	$129, -48(%rbp)
	je	.L1895
	movl	$129, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1895
	cmpq	$8193, -48(%rbp)
	je	.L1897
	movl	$8193, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1897
	cmpq	$75431937, -48(%rbp)
	je	.L1899
	movl	$75431937, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	movl	%eax, %r8d
	movl	$4294967295, %eax
	testb	%r8b, %r8b
	je	.L1888
.L1899:
	movl	$75431937, %eax
	jmp	.L1888
	.p2align 4,,10
	.p2align 3
.L1886:
	movl	$16417, %eax
	jmp	.L1888
	.p2align 4,,10
	.p2align 3
.L1902:
	movl	$16417, %esi
	jmp	.L1904
	.p2align 4,,10
	.p2align 3
.L1963:
	leaq	.LC11(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1964:
	leaq	.LC12(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1889:
	movl	$7263, %eax
	jmp	.L1888
	.p2align 4,,10
	.p2align 3
.L1905:
	movl	$7263, %esi
	jmp	.L1904
	.p2align 4,,10
	.p2align 3
.L1923:
	cmpq	$7263, -80(%rbp)
	je	.L1921
	movl	$7263, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L1924
.L1921:
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type3MaxEv@PLT
	movq	%r13, %rdi
	movsd	%xmm0, -88(%rbp)
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	comisd	-88(%rbp), %xmm0
	ja	.L1961
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	movq	%r13, %rdi
	movsd	%xmm0, -88(%rbp)
	call	_ZNK2v88internal8compiler4Type3MaxEv@PLT
	movsd	-88(%rbp), %xmm1
	comisd	%xmm0, %xmm1
	ja	.L1961
	jmp	.L1924
	.p2align 4,,10
	.p2align 3
.L1930:
	movq	-80(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1928
	jmp	.L1961
.L1907:
	movl	$134217729, %esi
	jmp	.L1904
.L1891:
	movl	$134217729, %eax
	jmp	.L1888
.L1932:
	movl	$513, %eax
	jmp	.L1918
.L1909:
	movl	$257, %esi
	jmp	.L1904
.L1893:
	movl	$257, %eax
	jmp	.L1888
.L1911:
	movl	$129, %esi
	jmp	.L1904
.L1895:
	movl	$129, %eax
	jmp	.L1888
.L1967:
	call	__stack_chk_fail@PLT
.L1897:
	movl	$8193, %eax
	jmp	.L1888
.L1913:
	movl	$8193, %esi
	jmp	.L1904
	.cfi_endproc
.LFE18804:
	.size	_ZN2v88internal8compiler14OperationTyper11StrictEqualENS1_4TypeES3_, .-_ZN2v88internal8compiler14OperationTyper11StrictEqualENS1_4TypeES3_
	.section	.text._ZN2v88internal8compiler14OperationTyper11CheckBoundsENS1_4TypeES3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper11CheckBoundsENS1_4TypeES3_
	.type	_ZN2v88internal8compiler14OperationTyper11CheckBoundsENS1_4TypeES3_, @function
_ZN2v88internal8compiler14OperationTyper11CheckBoundsENS1_4TypeES3_:
.LFB18805:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdx, -48(%rbp)
	movq	8(%rdi), %rdx
	movq	%rsi, -40(%rbp)
	movq	208(%rdx), %rsi
	cmpq	%rsi, %rax
	jne	.L1974
.L1969:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1974:
	.cfi_restore_state
	leaq	-48(%rbp), %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1969
	movq	%r12, %rdi
	movq	(%rbx), %r13
	call	_ZNK2v88internal8compiler4Type3MaxEv@PLT
	movapd	%xmm0, %xmm1
	subsd	.LC5(%rip), %xmm1
	pxor	%xmm0, %xmm0
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Type5RangeEddPNS0_4ZoneE@PLT
	leaq	-40(%rbp), %rdi
	movl	$2049, %esi
	movq	%rax, %r12
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1975
.L1971:
	movq	(%rbx), %rdx
	movq	-40(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1975:
	.cfi_restore_state
	movq	8(%rbx), %rax
	movq	(%rbx), %rdx
	movq	-40(%rbp), %rdi
	movq	208(%rax), %rsi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -40(%rbp)
	jmp	.L1971
	.cfi_endproc
.LFE18805:
	.size	_ZN2v88internal8compiler14OperationTyper11CheckBoundsENS1_4TypeES3_, .-_ZN2v88internal8compiler14OperationTyper11CheckBoundsENS1_4TypeES3_
	.section	.text._ZN2v88internal8compiler14OperationTyper16CheckFloat64HoleENS1_4TypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper16CheckFloat64HoleENS1_4TypeE
	.type	_ZN2v88internal8compiler14OperationTyper16CheckFloat64HoleENS1_4TypeE, @function
_ZN2v88internal8compiler14OperationTyper16CheckFloat64HoleENS1_4TypeE:
.LFB18806:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	-24(%rbp), %rdi
	subq	$24, %rsp
	movq	%rsi, -24(%rbp)
	movl	$8388609, %esi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1982
	movq	-24(%rbp), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1982:
	.cfi_restore_state
	movq	(%rbx), %rdx
	movq	-24(%rbp), %rdi
	movl	$7263, %esi
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	(%rbx), %rdx
	movl	$257, %esi
	movq	%rax, %rdi
	movq	%rax, -24(%rbp)
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -24(%rbp)
	movq	-24(%rbp), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18806:
	.size	_ZN2v88internal8compiler14OperationTyper16CheckFloat64HoleENS1_4TypeE, .-_ZN2v88internal8compiler14OperationTyper16CheckFloat64HoleENS1_4TypeE
	.section	.text._ZN2v88internal8compiler14OperationTyper11CheckNumberENS1_4TypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper11CheckNumberENS1_4TypeE
	.type	_ZN2v88internal8compiler14OperationTyper11CheckNumberENS1_4TypeE, @function
_ZN2v88internal8compiler14OperationTyper11CheckNumberENS1_4TypeE:
.LFB18807:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	movq	%rsi, %rdi
	movl	$7263, %esi
	movq	(%r8), %rdx
	jmp	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	.cfi_endproc
.LFE18807:
	.size	_ZN2v88internal8compiler14OperationTyper11CheckNumberENS1_4TypeE, .-_ZN2v88internal8compiler14OperationTyper11CheckNumberENS1_4TypeE
	.section	.text._ZN2v88internal8compiler14OperationTyper13TypeTypeGuardEPKNS1_8OperatorENS1_4TypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper13TypeTypeGuardEPKNS1_8OperatorENS1_4TypeE
	.type	_ZN2v88internal8compiler14OperationTyper13TypeTypeGuardEPKNS1_8OperatorENS1_4TypeE, @function
_ZN2v88internal8compiler14OperationTyper13TypeTypeGuardEPKNS1_8OperatorENS1_4TypeE:
.LFB18808:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	(%r8), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	call	_ZN2v88internal8compiler15TypeGuardTypeOfEPKNS1_8OperatorE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	popq	%r12
	movq	%rax, %rsi
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	.cfi_endproc
.LFE18808:
	.size	_ZN2v88internal8compiler14OperationTyper13TypeTypeGuardEPKNS1_8OperatorENS1_4TypeE, .-_ZN2v88internal8compiler14OperationTyper13TypeTypeGuardEPKNS1_8OperatorENS1_4TypeE
	.section	.text._ZN2v88internal8compiler14OperationTyper28ConvertTaggedHoleToUndefinedENS1_4TypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper28ConvertTaggedHoleToUndefinedENS1_4TypeE
	.type	_ZN2v88internal8compiler14OperationTyper28ConvertTaggedHoleToUndefinedENS1_4TypeE, @function
_ZN2v88internal8compiler14OperationTyper28ConvertTaggedHoleToUndefinedENS1_4TypeE:
.LFB18809:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	-24(%rbp), %rdi
	subq	$24, %rsp
	movq	%rsi, -24(%rbp)
	movl	$8388609, %esi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	movl	%eax, %r8d
	movq	-24(%rbp), %rax
	testb	%r8b, %r8b
	jne	.L1991
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1991:
	.cfi_restore_state
	movq	(%rbx), %rdx
	movl	$209682431, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	(%rbx), %rdx
	movl	$257, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18809:
	.size	_ZN2v88internal8compiler14OperationTyper28ConvertTaggedHoleToUndefinedENS1_4TypeE, .-_ZN2v88internal8compiler14OperationTyper28ConvertTaggedHoleToUndefinedENS1_4TypeE
	.section	.text._ZN2v88internal8compiler14OperationTyper9ToBooleanENS1_4TypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14OperationTyper9ToBooleanENS1_4TypeE
	.type	_ZN2v88internal8compiler14OperationTyper9ToBooleanENS1_4TypeE, @function
_ZN2v88internal8compiler14OperationTyper9ToBooleanENS1_4TypeE:
.LFB18810:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, -24(%rbp)
	cmpq	$513, %rsi
	jne	.L2005
.L1993:
	movq	-24(%rbp), %rax
.L1996:
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2005:
	.cfi_restore_state
	leaq	-24(%rbp), %r13
	movq	%rdi, %r12
	movl	$513, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1993
	movq	104(%r12), %rsi
	cmpq	%rsi, -24(%rbp)
	jne	.L2006
.L1994:
	movq	48(%r12), %rax
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2006:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1994
	movq	96(%r12), %rsi
	cmpq	%rsi, -24(%rbp)
	jne	.L2007
.L1997:
	movq	56(%r12), %rax
	jmp	.L1996
	.p2align 4,,10
	.p2align 3
.L2007:
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1997
	cmpq	$7263, -24(%rbp)
	je	.L1999
	movl	$7263, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1999
	movl	$513, %eax
	jmp	.L1996
	.p2align 4,,10
	.p2align 3
.L1999:
	movq	-24(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14OperationTyper15NumberToBooleanENS1_4TypeE
	jmp	.L1996
	.cfi_endproc
.LFE18810:
	.size	_ZN2v88internal8compiler14OperationTyper9ToBooleanENS1_4TypeE, .-_ZN2v88internal8compiler14OperationTyper9ToBooleanENS1_4TypeE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler14OperationTyperC2EPNS1_12JSHeapBrokerEPNS0_4ZoneE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler14OperationTyperC2EPNS1_12JSHeapBrokerEPNS0_4ZoneE, @function
_GLOBAL__sub_I__ZN2v88internal8compiler14OperationTyperC2EPNS1_12JSHeapBrokerEPNS0_4ZoneE:
.LFB22668:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE22668:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler14OperationTyperC2EPNS1_12JSHeapBrokerEPNS0_4ZoneE, .-_GLOBAL__sub_I__ZN2v88internal8compiler14OperationTyperC2EPNS1_12JSHeapBrokerEPNS0_4ZoneE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler14OperationTyperC2EPNS1_12JSHeapBrokerEPNS0_4ZoneE
	.section	.rodata._ZZN2v88internal8compiler14OperationTyper11WeakenRangeENS1_4TypeES3_E16kWeakenMaxLimits,"a"
	.align 32
	.type	_ZZN2v88internal8compiler14OperationTyper11WeakenRangeENS1_4TypeES3_E16kWeakenMaxLimits, @object
	.size	_ZZN2v88internal8compiler14OperationTyper11WeakenRangeENS1_4TypeES3_E16kWeakenMaxLimits, 168
_ZZN2v88internal8compiler14OperationTyper11WeakenRangeENS1_4TypeES3_E16kWeakenMaxLimits:
	.long	0
	.long	0
	.long	4286578688
	.long	1104150527
	.long	4290772992
	.long	1105199103
	.long	4292870144
	.long	1106247679
	.long	4293918720
	.long	1107296255
	.long	4294443008
	.long	1108344831
	.long	4294705152
	.long	1109393407
	.long	4294836224
	.long	1110441983
	.long	4294901760
	.long	1111490559
	.long	4294934528
	.long	1112539135
	.long	4294950912
	.long	1113587711
	.long	4294959104
	.long	1114636287
	.long	4294963200
	.long	1115684863
	.long	4294965248
	.long	1116733439
	.long	4294966272
	.long	1117782015
	.long	4294966784
	.long	1118830591
	.long	4294967040
	.long	1119879167
	.long	4294967168
	.long	1120927743
	.long	4294967232
	.long	1121976319
	.long	4294967264
	.long	1123024895
	.long	4294967280
	.long	1124073471
	.section	.rodata._ZZN2v88internal8compiler14OperationTyper11WeakenRangeENS1_4TypeES3_E16kWeakenMinLimits,"a"
	.align 32
	.type	_ZZN2v88internal8compiler14OperationTyper11WeakenRangeENS1_4TypeES3_E16kWeakenMinLimits, @object
	.size	_ZZN2v88internal8compiler14OperationTyper11WeakenRangeENS1_4TypeES3_E16kWeakenMinLimits, 168
_ZZN2v88internal8compiler14OperationTyper11WeakenRangeENS1_4TypeES3_E16kWeakenMinLimits:
	.long	0
	.long	0
	.long	0
	.long	-1043333120
	.long	0
	.long	-1042284544
	.long	0
	.long	-1041235968
	.long	0
	.long	-1040187392
	.long	0
	.long	-1039138816
	.long	0
	.long	-1038090240
	.long	0
	.long	-1037041664
	.long	0
	.long	-1035993088
	.long	0
	.long	-1034944512
	.long	0
	.long	-1033895936
	.long	0
	.long	-1032847360
	.long	0
	.long	-1031798784
	.long	0
	.long	-1030750208
	.long	0
	.long	-1029701632
	.long	0
	.long	-1028653056
	.long	0
	.long	-1027604480
	.long	0
	.long	-1026555904
	.long	0
	.long	-1025507328
	.long	0
	.long	-1024458752
	.long	0
	.long	-1023410176
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	2146435072
	.align 8
.LC1:
	.long	0
	.long	-1048576
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC3:
	.quad	1
	.quad	1
	.align 16
.LC4:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC5:
	.long	0
	.long	1072693248
	.align 8
.LC6:
	.long	0
	.long	-1074790400
	.align 8
.LC7:
	.long	4290772992
	.long	1105199103
	.align 8
.LC8:
	.long	0
	.long	-1042284544
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
