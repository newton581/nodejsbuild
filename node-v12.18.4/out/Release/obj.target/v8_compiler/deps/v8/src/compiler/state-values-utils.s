	.file	"state-values-utils.cc"
	.text
	.section	.rodata._ZN2v88internal8compiler16StateValuesCacheC2EPNS1_7JSGraphE.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"Out of memory: HashMap::Initialize"
	.section	.text._ZN2v88internal8compiler16StateValuesCacheC2EPNS1_7JSGraphE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16StateValuesCacheC2EPNS1_7JSGraphE
	.type	_ZN2v88internal8compiler16StateValuesCacheC2EPNS1_7JSGraphE, @function
_ZN2v88internal8compiler16StateValuesCacheC2EPNS1_7JSGraphE:
.LFB10843:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rsi), %rax
	movq	%rsi, (%rdi)
	movq	(%rax), %rdi
	leaq	_ZN2v88internal8compiler16StateValuesCache12AreKeysEqualEPvS3_(%rip), %rax
	movq	%rax, 24(%rbx)
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$191, %rdx
	jbe	.L7
	leaq	192(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L3:
	movq	%rax, 8(%rbx)
	testq	%rax, %rax
	je	.L8
	movq	$0, (%rax)
	movq	$0, 24(%rax)
	movq	$0, 48(%rax)
	movq	$0, 72(%rax)
	movq	$0, 96(%rax)
	movq	$0, 120(%rax)
	movq	$0, 144(%rax)
	movq	$0, 168(%rax)
	movq	(%rbx), %rax
	movq	$8, 16(%rbx)
	movq	(%rax), %rax
	movq	$0, 64(%rbx)
	movq	(%rax), %rax
	movq	$0, 40(%rbx)
	movq	$0, 48(%rbx)
	movq	%rax, 32(%rbx)
	movq	$0, 56(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
	movl	$192, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L3
.L8:
	leaq	.LC0(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE10843:
	.size	_ZN2v88internal8compiler16StateValuesCacheC2EPNS1_7JSGraphE, .-_ZN2v88internal8compiler16StateValuesCacheC2EPNS1_7JSGraphE
	.globl	_ZN2v88internal8compiler16StateValuesCacheC1EPNS1_7JSGraphE
	.set	_ZN2v88internal8compiler16StateValuesCacheC1EPNS1_7JSGraphE,_ZN2v88internal8compiler16StateValuesCacheC2EPNS1_7JSGraphE
	.section	.text._ZN2v88internal8compiler16StateValuesCache17IsKeysEqualToNodeEPNS2_14StateValuesKeyEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16StateValuesCache17IsKeysEqualToNodeEPNS2_14StateValuesKeyEPNS1_4NodeE
	.type	_ZN2v88internal8compiler16StateValuesCache17IsKeysEqualToNodeEPNS2_14StateValuesKeyEPNS1_4NodeE, @function
_ZN2v88internal8compiler16StateValuesCache17IsKeysEqualToNodeEPNS2_14StateValuesKeyEPNS1_4NodeE:
.LFB10846:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	8(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L11
	movq	32(%rsi), %rax
	movl	8(%rax), %eax
.L11:
	cltq
	cmpq	%rax, %rdx
	je	.L12
.L14:
	xorl	%eax, %eax
.L9:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L24
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	_ZN2v88internal8compiler17SparseInputMaskOfEPKNS1_8OperatorE@PLT
	leaq	16(%r12), %rsi
	leaq	-28(%rbp), %rdi
	movl	%eax, -28(%rbp)
	call	_ZN2v88internal8compilerneERKNS1_15SparseInputMaskES4_@PLT
	testb	%al, %al
	jne	.L14
	movq	8(%r12), %rcx
	testq	%rcx, %rcx
	je	.L20
	movzbl	23(%rbx), %edx
	movq	24(%r12), %rsi
	leaq	32(%rbx), %rdi
	xorl	%eax, %eax
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L15
	movq	32(%rbx), %rdi
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L25:
	addq	$1, %rax
	cmpq	%rax, %rcx
	je	.L20
.L16:
	movslq	%eax, %rdx
	movq	16(%rdi,%rdx,8), %rbx
	cmpq	%rbx, (%rsi,%rax,8)
	je	.L25
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L26:
	addq	$1, %rax
	cmpq	%rax, %rcx
	je	.L20
.L15:
	movslq	%eax, %rdx
	movq	(%rdi,%rdx,8), %rbx
	cmpq	%rbx, (%rsi,%rax,8)
	je	.L26
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L20:
	movl	$1, %eax
	jmp	.L9
.L24:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10846:
	.size	_ZN2v88internal8compiler16StateValuesCache17IsKeysEqualToNodeEPNS2_14StateValuesKeyEPNS1_4NodeE, .-_ZN2v88internal8compiler16StateValuesCache17IsKeysEqualToNodeEPNS2_14StateValuesKeyEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler16StateValuesCache12AreKeysEqualEPvS3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16StateValuesCache12AreKeysEqualEPvS3_
	.type	_ZN2v88internal8compiler16StateValuesCache12AreKeysEqualEPvS3_, @function
_ZN2v88internal8compiler16StateValuesCache12AreKeysEqualEPvS3_:
.LFB10845:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	(%rdi), %r8
	movq	(%rsi), %rsi
	testq	%r8, %r8
	je	.L39
	cmpq	%rsi, %r8
	sete	%al
	testq	%rsi, %rsi
	je	.L40
.L27:
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	.cfi_restore_state
	popq	%r12
	movq	%r13, %rdi
	movq	%r8, %rsi
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler16StateValuesCache17IsKeysEqualToNodeEPNS2_14StateValuesKeyEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L39:
	.cfi_restore_state
	movq	%rdi, %r12
	testq	%rsi, %rsi
	je	.L41
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler16StateValuesCache17IsKeysEqualToNodeEPNS2_14StateValuesKeyEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L41:
	.cfi_restore_state
	movq	8(%r13), %rax
	cmpq	%rax, 8(%rdi)
	je	.L30
.L32:
	xorl	%eax, %eax
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L30:
	leaq	16(%r13), %rsi
	leaq	16(%rdi), %rdi
	call	_ZN2v88internal8compilerneERKNS1_15SparseInputMaskES4_@PLT
	testb	%al, %al
	jne	.L32
	movq	8(%r12), %rdx
	testq	%rdx, %rdx
	je	.L36
	movq	24(%r12), %rsi
	movq	24(%r13), %rcx
	xorl	%eax, %eax
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L42:
	addq	$1, %rax
	cmpq	%rax, %rdx
	je	.L36
.L33:
	movq	(%rcx,%rax,8), %rdi
	cmpq	%rdi, (%rsi,%rax,8)
	je	.L42
	xorl	%eax, %eax
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L36:
	movl	$1, %eax
	jmp	.L27
	.cfi_endproc
.LFE10845:
	.size	_ZN2v88internal8compiler16StateValuesCache12AreKeysEqualEPvS3_, .-_ZN2v88internal8compiler16StateValuesCache12AreKeysEqualEPvS3_
	.section	.text._ZN2v88internal8compiler16StateValuesCache17AreValueKeysEqualEPNS2_14StateValuesKeyES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16StateValuesCache17AreValueKeysEqualEPNS2_14StateValuesKeyES4_
	.type	_ZN2v88internal8compiler16StateValuesCache17AreValueKeysEqualEPNS2_14StateValuesKeyES4_, @function
_ZN2v88internal8compiler16StateValuesCache17AreValueKeysEqualEPNS2_14StateValuesKeyES4_:
.LFB10847:
	.cfi_startproc
	endbr64
	movq	8(%rsi), %rax
	cmpq	%rax, 8(%rdi)
	je	.L53
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	leaq	16(%rsi), %rsi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	leaq	16(%rdi), %rdi
	call	_ZN2v88internal8compilerneERKNS1_15SparseInputMaskES4_@PLT
	testb	%al, %al
	jne	.L44
	movq	8(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L48
	movq	24(%rbx), %rsi
	movq	24(%r12), %rcx
	xorl	%eax, %eax
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L54:
	addq	$1, %rax
	cmpq	%rax, %rdx
	je	.L48
.L46:
	movq	(%rcx,%rax,8), %rbx
	cmpq	%rbx, (%rsi,%rax,8)
	je	.L54
.L44:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore_state
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10847:
	.size	_ZN2v88internal8compiler16StateValuesCache17AreValueKeysEqualEPNS2_14StateValuesKeyES4_, .-_ZN2v88internal8compiler16StateValuesCache17AreValueKeysEqualEPNS2_14StateValuesKeyES4_
	.section	.text._ZN2v88internal8compiler16StateValuesCache19GetEmptyStateValuesEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16StateValuesCache19GetEmptyStateValuesEv
	.type	_ZN2v88internal8compiler16StateValuesCache19GetEmptyStateValuesEv, @function
_ZN2v88internal8compiler16StateValuesCache19GetEmptyStateValuesEv:
.LFB10848:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %rax
	testq	%rax, %rax
	je	.L61
	ret
	.p2align 4,,10
	.p2align 3
.L61:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	movq	8(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder11StateValuesEiNS1_15SparseInputMaskE@PLT
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rax, %rsi
	xorl	%edx, %edx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 64(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10848:
	.size	_ZN2v88internal8compiler16StateValuesCache19GetEmptyStateValuesEv, .-_ZN2v88internal8compiler16StateValuesCache19GetEmptyStateValuesEv
	.section	.text._ZN2v88internal8compiler16StateValuesCache20FillBufferWithValuesEPSt5arrayIPNS1_4NodeELm8EEPmS8_PS5_mPKNS0_9BitVectorEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16StateValuesCache20FillBufferWithValuesEPSt5arrayIPNS1_4NodeELm8EEPmS8_PS5_mPKNS0_9BitVectorEi
	.type	_ZN2v88internal8compiler16StateValuesCache20FillBufferWithValuesEPSt5arrayIPNS1_4NodeELm8EEPmS8_PS5_mPKNS0_9BitVectorEi, @function
_ZN2v88internal8compiler16StateValuesCache20FillBufferWithValuesEPSt5arrayIPNS1_4NodeELm8EEPmS8_PS5_mPKNS0_9BitVectorEi:
.LFB10852:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdx), %r11
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	24(%rbp), %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	16(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rcx), %rdi
	cmpq	%r9, %rdi
	jnb	.L76
	cmpq	$7, %r11
	ja	.L76
	movq	%rcx, %rbx
	xorl	%eax, %eax
	movq	%r11, %rcx
	testq	%r13, %r13
	jne	.L72
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L85:
	movq	(%rdx), %r11
	cmpq	$30, %rcx
	ja	.L63
	cmpq	$7, %r11
	ja	.L63
.L72:
	cmpl	$1, 4(%r13)
	movq	8(%r13), %r12
	leal	(%r14,%rdi), %r10d
	je	.L69
	testl	%r10d, %r10d
	leal	63(%r10), %r15d
	cmovns	%r10d, %r15d
	sarl	$6, %r15d
	movslq	%r15d, %r15
	movq	(%r12,%r15,8), %r12
.L69:
	movl	%r10d, %r15d
	sarl	$31, %r15d
	shrl	$26, %r15d
	addl	%r15d, %r10d
	andl	$63, %r10d
	subl	%r15d, %r10d
	btq	%r10, %r12
	jnc	.L67
	movl	$1, %r10d
	sall	%cl, %r10d
	orl	%r10d, %eax
	leaq	1(%r11), %r10
	movq	%r10, (%rdx)
	movq	(%r8,%rdi,8), %rdi
	movq	%rdi, (%rsi,%r11,8)
.L67:
	movq	(%rbx), %rdi
	addq	$1, %rcx
	addq	$1, %rdi
	movq	%rdi, (%rbx)
	cmpq	%rdi, %r9
	ja	.L85
.L63:
	popq	%rbx
	btsl	%ecx, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L77:
	.cfi_restore_state
	movl	$1, %r12d
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L74:
	movq	(%rdx), %r11
	cmpq	$7, %r11
	ja	.L63
	cmpq	$30, %rcx
	ja	.L63
.L64:
	movl	%r12d, %r10d
	sall	%cl, %r10d
	addq	$1, %rcx
	orl	%r10d, %eax
	leaq	1(%r11), %r10
	movq	%r10, (%rdx)
	movq	(%r8,%rdi,8), %rdi
	movq	%rdi, (%rsi,%r11,8)
	movq	(%rbx), %rdi
	addq	$1, %rdi
	movq	%rdi, (%rbx)
	cmpq	%r9, %rdi
	jb	.L74
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L76:
	movq	%r11, %rcx
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	btsl	%ecx, %eax
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10852:
	.size	_ZN2v88internal8compiler16StateValuesCache20FillBufferWithValuesEPSt5arrayIPNS1_4NodeELm8EEPmS8_PS5_mPKNS0_9BitVectorEi, .-_ZN2v88internal8compiler16StateValuesCache20FillBufferWithValuesEPSt5arrayIPNS1_4NodeELm8EEPmS8_PS5_mPKNS0_9BitVectorEi
	.section	.text._ZN2v88internal8compiler17StateValuesAccess8iterator3TopEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17StateValuesAccess8iterator3TopEv
	.type	_ZN2v88internal8compiler17StateValuesAccess8iterator3TopEv, @function
_ZN2v88internal8compiler17StateValuesAccess8iterator3TopEv:
.LFB10858:
	.cfi_startproc
	endbr64
	movslq	192(%rdi), %rax
	leaq	(%rax,%rax,2), %rax
	leaq	(%rdi,%rax,8), %rax
	ret
	.cfi_endproc
.LFE10858:
	.size	_ZN2v88internal8compiler17StateValuesAccess8iterator3TopEv, .-_ZN2v88internal8compiler17StateValuesAccess8iterator3TopEv
	.section	.rodata._ZN2v88internal8compiler17StateValuesAccess8iterator4PushEPNS1_4NodeE.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"kMaxInlineDepth > current_depth_"
	.section	.rodata._ZN2v88internal8compiler17StateValuesAccess8iterator4PushEPNS1_4NodeE.str1.1,"aMS",@progbits,1
.LC2:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal8compiler17StateValuesAccess8iterator4PushEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17StateValuesAccess8iterator4PushEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17StateValuesAccess8iterator4PushEPNS1_4NodeE, @function
_ZN2v88internal8compiler17StateValuesAccess8iterator4PushEPNS1_4NodeE:
.LFB10859:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	192(%rdi), %eax
	addl	$1, %eax
	movl	%eax, 192(%rdi)
	cmpl	$7, %eax
	jg	.L91
	movq	%rdi, %rbx
	movq	(%rsi), %rdi
	movq	%rsi, %r12
	call	_ZN2v88internal8compiler17SparseInputMaskOfEPKNS1_8OperatorE@PLT
	movq	%r12, %rdx
	leaq	-48(%rbp), %rdi
	leaq	-52(%rbp), %rsi
	movl	%eax, -52(%rbp)
	call	_ZN2v88internal8compiler15SparseInputMask17IterateOverInputsEPNS1_4NodeE@PLT
	movslq	192(%rbx), %rax
	movl	-32(%rbp), %edx
	movdqa	-48(%rbp), %xmm0
	leaq	(%rax,%rax,2), %rax
	leaq	(%rbx,%rax,8), %rax
	movl	%edx, 16(%rax)
	movups	%xmm0, (%rax)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L92
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L91:
	.cfi_restore_state
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L92:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10859:
	.size	_ZN2v88internal8compiler17StateValuesAccess8iterator4PushEPNS1_4NodeE, .-_ZN2v88internal8compiler17StateValuesAccess8iterator4PushEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17StateValuesAccess8iterator3PopEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17StateValuesAccess8iterator3PopEv
	.type	_ZN2v88internal8compiler17StateValuesAccess8iterator3PopEv, @function
_ZN2v88internal8compiler17StateValuesAccess8iterator3PopEv:
.LFB10860:
	.cfi_startproc
	endbr64
	subl	$1, 192(%rdi)
	ret
	.cfi_endproc
.LFE10860:
	.size	_ZN2v88internal8compiler17StateValuesAccess8iterator3PopEv, .-_ZN2v88internal8compiler17StateValuesAccess8iterator3PopEv
	.section	.text._ZNK2v88internal8compiler17StateValuesAccess8iterator4doneEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler17StateValuesAccess8iterator4doneEv
	.type	_ZNK2v88internal8compiler17StateValuesAccess8iterator4doneEv, @function
_ZNK2v88internal8compiler17StateValuesAccess8iterator4doneEv:
.LFB10861:
	.cfi_startproc
	endbr64
	movl	192(%rdi), %eax
	shrl	$31, %eax
	ret
	.cfi_endproc
.LFE10861:
	.size	_ZNK2v88internal8compiler17StateValuesAccess8iterator4doneEv, .-_ZNK2v88internal8compiler17StateValuesAccess8iterator4doneEv
	.section	.text._ZN2v88internal8compiler17StateValuesAccess8iterator7AdvanceEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17StateValuesAccess8iterator7AdvanceEv
	.type	_ZN2v88internal8compiler17StateValuesAccess8iterator7AdvanceEv, @function
_ZN2v88internal8compiler17StateValuesAccess8iterator7AdvanceEv:
.LFB10862:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movslq	192(%rdi), %rax
	leaq	(%rax,%rax,2), %rax
	leaq	(%rdi,%rax,8), %rdi
	call	_ZN2v88internal8compiler15SparseInputMask13InputIterator7AdvanceEv@PLT
.L96:
	movslq	192(%rbx), %rax
	leaq	(%rax,%rax,2), %rax
	leaq	(%rbx,%rax,8), %r12
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler15SparseInputMask13InputIterator6IsRealEv@PLT
	testb	%al, %al
	je	.L95
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler15SparseInputMask13InputIterator5IsEndEv@PLT
	testb	%al, %al
	je	.L98
	movl	192(%rbx), %eax
	subl	$1, %eax
	movl	%eax, 192(%rbx)
	jns	.L106
.L95:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L107
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L98:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler15SparseInputMask13InputIterator7GetRealEv@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	movzwl	16(%rax), %eax
	subl	$42, %eax
	cmpl	$1, %eax
	ja	.L95
	movl	192(%rbx), %eax
	addl	$1, %eax
	movl	%eax, 192(%rbx)
	cmpl	$7, %eax
	jg	.L108
	movq	(%r12), %rdi
	call	_ZN2v88internal8compiler17SparseInputMaskOfEPKNS1_8OperatorE@PLT
	movq	%r12, %rdx
	leaq	-68(%rbp), %rsi
	movq	%r13, %rdi
	movl	%eax, -68(%rbp)
	call	_ZN2v88internal8compiler15SparseInputMask17IterateOverInputsEPNS1_4NodeE@PLT
	movslq	192(%rbx), %rax
	movdqa	-64(%rbp), %xmm0
	movl	-48(%rbp), %edx
	leaq	(%rax,%rax,2), %rax
	leaq	(%rbx,%rax,8), %rax
	movl	%edx, 16(%rax)
	movups	%xmm0, (%rax)
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L106:
	cltq
	leaq	(%rax,%rax,2), %rax
	leaq	(%rbx,%rax,8), %rdi
	call	_ZN2v88internal8compiler15SparseInputMask13InputIterator7AdvanceEv@PLT
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L108:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L107:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10862:
	.size	_ZN2v88internal8compiler17StateValuesAccess8iterator7AdvanceEv, .-_ZN2v88internal8compiler17StateValuesAccess8iterator7AdvanceEv
	.section	.text._ZN2v88internal8compiler17StateValuesAccess8iterator11EnsureValidEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17StateValuesAccess8iterator11EnsureValidEv
	.type	_ZN2v88internal8compiler17StateValuesAccess8iterator11EnsureValidEv, @function
_ZN2v88internal8compiler17StateValuesAccess8iterator11EnsureValidEv:
.LFB10863:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
.L110:
	movslq	192(%rbx), %rax
	leaq	(%rax,%rax,2), %rax
	leaq	(%rbx,%rax,8), %r12
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler15SparseInputMask13InputIterator6IsRealEv@PLT
	testb	%al, %al
	je	.L109
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler15SparseInputMask13InputIterator5IsEndEv@PLT
	testb	%al, %al
	je	.L112
	movl	192(%rbx), %eax
	subl	$1, %eax
	movl	%eax, 192(%rbx)
	jns	.L120
.L109:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L121
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L112:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler15SparseInputMask13InputIterator7GetRealEv@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	movzwl	16(%rax), %eax
	subl	$42, %eax
	cmpl	$1, %eax
	ja	.L109
	movl	192(%rbx), %eax
	addl	$1, %eax
	movl	%eax, 192(%rbx)
	cmpl	$7, %eax
	jg	.L122
	movq	(%r12), %rdi
	call	_ZN2v88internal8compiler17SparseInputMaskOfEPKNS1_8OperatorE@PLT
	movq	%r12, %rdx
	leaq	-68(%rbp), %rsi
	movq	%r13, %rdi
	movl	%eax, -68(%rbp)
	call	_ZN2v88internal8compiler15SparseInputMask17IterateOverInputsEPNS1_4NodeE@PLT
	movslq	192(%rbx), %rax
	movdqa	-64(%rbp), %xmm0
	movl	-48(%rbp), %edx
	leaq	(%rax,%rax,2), %rax
	leaq	(%rbx,%rax,8), %rax
	movl	%edx, 16(%rax)
	movups	%xmm0, (%rax)
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L120:
	cltq
	leaq	(%rax,%rax,2), %rax
	leaq	(%rbx,%rax,8), %rdi
	call	_ZN2v88internal8compiler15SparseInputMask13InputIterator7AdvanceEv@PLT
	jmp	.L110
.L122:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L121:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10863:
	.size	_ZN2v88internal8compiler17StateValuesAccess8iterator11EnsureValidEv, .-_ZN2v88internal8compiler17StateValuesAccess8iterator11EnsureValidEv
	.section	.text._ZN2v88internal8compiler17StateValuesAccess8iteratorC2EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17StateValuesAccess8iteratorC2EPNS1_4NodeE
	.type	_ZN2v88internal8compiler17StateValuesAccess8iteratorC2EPNS1_4NodeE, @function
_ZN2v88internal8compiler17StateValuesAccess8iteratorC2EPNS1_4NodeE:
.LFB10856:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	$0, 192(%rdi)
	movq	(%rsi), %rdi
	call	_ZN2v88internal8compiler17SparseInputMaskOfEPKNS1_8OperatorE@PLT
	leaq	-48(%rbp), %rdi
	leaq	-52(%rbp), %rsi
	movq	%r13, %rdx
	movl	%eax, -52(%rbp)
	call	_ZN2v88internal8compiler15SparseInputMask17IterateOverInputsEPNS1_4NodeE@PLT
	movl	-32(%rbp), %edx
	movdqa	-48(%rbp), %xmm0
	movq	%r12, %rdi
	movslq	192(%r12), %rax
	leaq	(%rax,%rax,2), %rax
	leaq	(%r12,%rax,8), %rax
	movl	%edx, 16(%rax)
	movups	%xmm0, (%rax)
	call	_ZN2v88internal8compiler17StateValuesAccess8iterator11EnsureValidEv
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L126
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L126:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10856:
	.size	_ZN2v88internal8compiler17StateValuesAccess8iteratorC2EPNS1_4NodeE, .-_ZN2v88internal8compiler17StateValuesAccess8iteratorC2EPNS1_4NodeE
	.globl	_ZN2v88internal8compiler17StateValuesAccess8iteratorC1EPNS1_4NodeE
	.set	_ZN2v88internal8compiler17StateValuesAccess8iteratorC1EPNS1_4NodeE,_ZN2v88internal8compiler17StateValuesAccess8iteratorC2EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17StateValuesAccess8iterator4nodeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17StateValuesAccess8iterator4nodeEv
	.type	_ZN2v88internal8compiler17StateValuesAccess8iterator4nodeEv, @function
_ZN2v88internal8compiler17StateValuesAccess8iterator4nodeEv:
.LFB10864:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movslq	192(%rdi), %rax
	leaq	(%rax,%rax,2), %rax
	leaq	(%rdi,%rax,8), %r12
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler15SparseInputMask13InputIterator6IsRealEv@PLT
	testb	%al, %al
	jne	.L130
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L130:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNK2v88internal8compiler15SparseInputMask13InputIterator7GetRealEv@PLT
	.cfi_endproc
.LFE10864:
	.size	_ZN2v88internal8compiler17StateValuesAccess8iterator4nodeEv, .-_ZN2v88internal8compiler17StateValuesAccess8iterator4nodeEv
	.section	.text._ZN2v88internal8compiler17StateValuesAccess8iterator4typeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17StateValuesAccess8iterator4typeEv
	.type	_ZN2v88internal8compiler17StateValuesAccess8iterator4typeEv, @function
_ZN2v88internal8compiler17StateValuesAccess8iterator4typeEv:
.LFB10865:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movslq	192(%rdi), %rax
	movq	%rdi, %rbx
	leaq	(%rax,%rax,2), %rax
	leaq	(%rdi,%rax,8), %rdi
	movq	8(%rdi), %r12
	movq	(%r12), %rax
	cmpw	$42, 16(%rax)
	jne	.L132
	popq	%rbx
	movl	$1800, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L132:
	.cfi_restore_state
	call	_ZNK2v88internal8compiler15SparseInputMask13InputIterator6IsRealEv@PLT
	testb	%al, %al
	jne	.L134
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L134:
	.cfi_restore_state
	movq	(%r12), %rdi
	call	_ZN2v88internal8compiler14MachineTypesOfEPKNS1_8OperatorE@PLT
	movq	%rax, %r8
	movslq	192(%rbx), %rax
	leaq	(%rax,%rax,2), %rax
	movslq	16(%rbx,%rax,8), %rdx
	movq	8(%r8), %rax
	popq	%rbx
	popq	%r12
	movzwl	(%rax,%rdx,2), %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10865:
	.size	_ZN2v88internal8compiler17StateValuesAccess8iterator4typeEv, .-_ZN2v88internal8compiler17StateValuesAccess8iterator4typeEv
	.section	.rodata._ZN2v88internal8compiler17StateValuesAccess8iteratorneERKS3_.str1.1,"aMS",@progbits,1
.LC3:
	.string	"other.done()"
	.section	.text._ZN2v88internal8compiler17StateValuesAccess8iteratorneERKS3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17StateValuesAccess8iteratorneERKS3_
	.type	_ZN2v88internal8compiler17StateValuesAccess8iteratorneERKS3_, @function
_ZN2v88internal8compiler17StateValuesAccess8iteratorneERKS3_:
.LFB10866:
	.cfi_startproc
	endbr64
	movl	192(%rsi), %eax
	testl	%eax, %eax
	jns	.L141
	movl	192(%rdi), %eax
	notl	%eax
	shrl	$31, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L141:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC3(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE10866:
	.size	_ZN2v88internal8compiler17StateValuesAccess8iteratorneERKS3_, .-_ZN2v88internal8compiler17StateValuesAccess8iteratorneERKS3_
	.section	.text._ZN2v88internal8compiler17StateValuesAccess8iteratorppEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17StateValuesAccess8iteratorppEv
	.type	_ZN2v88internal8compiler17StateValuesAccess8iteratorppEv, @function
_ZN2v88internal8compiler17StateValuesAccess8iteratorppEv:
.LFB10867:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	leaq	-64(%rbp), %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movslq	192(%rdi), %rax
	leaq	(%rax,%rax,2), %rax
	leaq	(%rdi,%rax,8), %rdi
	call	_ZN2v88internal8compiler15SparseInputMask13InputIterator7AdvanceEv@PLT
.L143:
	movslq	192(%r12), %rax
	leaq	(%rax,%rax,2), %rax
	leaq	(%r12,%rax,8), %r13
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler15SparseInputMask13InputIterator6IsRealEv@PLT
	testb	%al, %al
	je	.L144
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler15SparseInputMask13InputIterator5IsEndEv@PLT
	testb	%al, %al
	je	.L145
	movl	192(%r12), %eax
	subl	$1, %eax
	movl	%eax, 192(%r12)
	jns	.L153
.L144:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L154
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L145:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler15SparseInputMask13InputIterator7GetRealEv@PLT
	movq	%rax, %r13
	movq	(%rax), %rax
	movzwl	16(%rax), %eax
	subl	$42, %eax
	cmpl	$1, %eax
	ja	.L144
	movl	192(%r12), %eax
	addl	$1, %eax
	movl	%eax, 192(%r12)
	cmpl	$7, %eax
	jg	.L155
	movq	0(%r13), %rdi
	call	_ZN2v88internal8compiler17SparseInputMaskOfEPKNS1_8OperatorE@PLT
	movq	%r13, %rdx
	leaq	-68(%rbp), %rsi
	movq	%rbx, %rdi
	movl	%eax, -68(%rbp)
	call	_ZN2v88internal8compiler15SparseInputMask17IterateOverInputsEPNS1_4NodeE@PLT
	movslq	192(%r12), %rax
	movdqa	-64(%rbp), %xmm0
	movl	-48(%rbp), %edx
	leaq	(%rax,%rax,2), %rax
	leaq	(%r12,%rax,8), %rax
	movl	%edx, 16(%rax)
	movups	%xmm0, (%rax)
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L153:
	cltq
	leaq	(%rax,%rax,2), %rax
	leaq	(%r12,%rax,8), %rdi
	call	_ZN2v88internal8compiler15SparseInputMask13InputIterator7AdvanceEv@PLT
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L155:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L154:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10867:
	.size	_ZN2v88internal8compiler17StateValuesAccess8iteratorppEv, .-_ZN2v88internal8compiler17StateValuesAccess8iteratorppEv
	.section	.text._ZN2v88internal8compiler17StateValuesAccess8iteratordeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17StateValuesAccess8iteratordeEv
	.type	_ZN2v88internal8compiler17StateValuesAccess8iteratordeEv, @function
_ZN2v88internal8compiler17StateValuesAccess8iteratordeEv:
.LFB10868:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movslq	192(%rdi), %rax
	leaq	(%rax,%rax,2), %rax
	leaq	(%rdi,%rax,8), %r12
	movq	8(%r12), %r13
	movq	0(%r13), %rax
	cmpw	$42, 16(%rax)
	jne	.L164
	movl	$7, %ebx
	movl	$8, %r13d
.L157:
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler15SparseInputMask13InputIterator6IsRealEv@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	testb	%r8b, %r8b
	jne	.L165
.L159:
	xorl	%edx, %edx
	addq	$8, %rsp
	movb	%r13b, %dl
	movb	%bl, %dh
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L164:
	.cfi_restore_state
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler15SparseInputMask13InputIterator6IsRealEv@PLT
	testb	%al, %al
	jne	.L158
	movslq	192(%rbx), %rax
	xorl	%r13d, %r13d
	leaq	(%rax,%rax,2), %rax
	leaq	(%rbx,%rax,8), %r12
	xorl	%ebx, %ebx
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L165:
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler15SparseInputMask13InputIterator7GetRealEv@PLT
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L158:
	movq	0(%r13), %rdi
	call	_ZN2v88internal8compiler14MachineTypesOfEPKNS1_8OperatorE@PLT
	movq	%rax, %r8
	movslq	192(%rbx), %rax
	leaq	(%rax,%rax,2), %rax
	leaq	(%rbx,%rax,8), %r12
	movq	8(%r8), %rax
	movslq	16(%r12), %rdx
	leaq	(%rax,%rdx,2), %rax
	movzbl	(%rax), %r13d
	movzbl	1(%rax), %ebx
	jmp	.L157
	.cfi_endproc
.LFE10868:
	.size	_ZN2v88internal8compiler17StateValuesAccess8iteratordeEv, .-_ZN2v88internal8compiler17StateValuesAccess8iteratordeEv
	.section	.text._ZN2v88internal8compiler17StateValuesAccess4sizeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17StateValuesAccess4sizeEv
	.type	_ZN2v88internal8compiler17StateValuesAccess4sizeEv, @function
_ZN2v88internal8compiler17StateValuesAccess4sizeEv:
.LFB10869:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	leaq	-72(%rbp), %r13
	pushq	%rbx
	.cfi_offset 3, -40
	leaq	-64(%rbp), %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler17SparseInputMaskOfEPKNS1_8OperatorE@PLT
	movq	(%r12), %rdx
	movq	%rbx, %rdi
	leaq	-76(%rbp), %rsi
	movl	%eax, -76(%rbp)
	xorl	%r12d, %r12d
	call	_ZN2v88internal8compiler15SparseInputMask17IterateOverInputsEPNS1_4NodeE@PLT
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler15SparseInputMask13InputIterator5IsEndEv@PLT
	testb	%al, %al
	jne	.L166
.L174:
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler15SparseInputMask13InputIterator6IsRealEv@PLT
	testb	%al, %al
	jne	.L168
.L170:
	addq	$1, %r12
.L169:
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler15SparseInputMask13InputIterator7AdvanceEv@PLT
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler15SparseInputMask13InputIterator5IsEndEv@PLT
	testb	%al, %al
	je	.L174
.L166:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L175
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L168:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler15SparseInputMask13InputIterator7GetRealEv@PLT
	movq	(%rax), %rdx
	movzwl	16(%rdx), %edx
	subl	$42, %edx
	cmpl	$1, %edx
	ja	.L170
	movq	%r13, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal8compiler17StateValuesAccess4sizeEv
	addq	%rax, %r12
	jmp	.L169
.L175:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10869:
	.size	_ZN2v88internal8compiler17StateValuesAccess4sizeEv, .-_ZN2v88internal8compiler17StateValuesAccess4sizeEv
	.section	.rodata._ZNSt6vectorISt5arrayIPN2v88internal8compiler4NodeELm8EENS2_13ZoneAllocatorIS6_EEE17_M_default_appendEm.str1.1,"aMS",@progbits,1
.LC4:
	.string	"vector::_M_default_append"
	.section	.text._ZNSt6vectorISt5arrayIPN2v88internal8compiler4NodeELm8EENS2_13ZoneAllocatorIS6_EEE17_M_default_appendEm,"axG",@progbits,_ZNSt6vectorISt5arrayIPN2v88internal8compiler4NodeELm8EENS2_13ZoneAllocatorIS6_EEE17_M_default_appendEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt5arrayIPN2v88internal8compiler4NodeELm8EENS2_13ZoneAllocatorIS6_EEE17_M_default_appendEm
	.type	_ZNSt6vectorISt5arrayIPN2v88internal8compiler4NodeELm8EENS2_13ZoneAllocatorIS6_EEE17_M_default_appendEm, @function
_ZNSt6vectorISt5arrayIPN2v88internal8compiler4NodeELm8EENS2_13ZoneAllocatorIS6_EEE17_M_default_appendEm:
.LFB12054:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L192
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movl	$33554431, %esi
	subq	$24, %rsp
	movq	16(%rdi), %rcx
	movq	24(%r12), %rax
	movq	%rcx, %rdx
	subq	8(%rdi), %rdx
	subq	%rcx, %rax
	movq	%rsi, %rdi
	movq	%rdx, %r13
	sarq	$6, %rax
	sarq	$6, %r13
	subq	%r13, %rdi
	cmpq	%rbx, %rax
	jb	.L178
	salq	$6, %rbx
	movq	%rcx, %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	call	memset@PLT
	movq	%rax, %rcx
	addq	%rbx, %rcx
	movq	%rcx, 16(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L192:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L178:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	cmpq	%rbx, %rdi
	jb	.L195
	cmpq	%r13, %rbx
	movq	%r13, %r15
	movq	(%r12), %rdi
	cmovnb	%rbx, %r15
	movq	16(%rdi), %r14
	movq	24(%rdi), %rax
	addq	%r13, %r15
	cmpq	$33554431, %r15
	cmova	%rsi, %r15
	subq	%r14, %rax
	salq	$6, %r15
	movq	%r15, %rsi
	cmpq	%r15, %rax
	jb	.L196
	addq	%r14, %rsi
	movq	%rsi, 16(%rdi)
.L182:
	movq	%rbx, %r8
	leaq	(%r14,%rdx), %rdi
	xorl	%esi, %esi
	salq	$6, %r8
	movq	%r8, %rdx
	call	memset@PLT
	movq	16(%r12), %rcx
	movq	8(%r12), %rdx
	cmpq	%rdx, %rcx
	je	.L185
	subq	%rdx, %rcx
	movq	%r14, %rax
	subq	%r14, %rdx
	addq	%r14, %rcx
	.p2align 4,,10
	.p2align 3
.L184:
	movdqu	(%rax,%rdx), %xmm0
	addq	$64, %rax
	movups	%xmm0, -64(%rax)
	movdqu	-48(%rax,%rdx), %xmm1
	movups	%xmm1, -48(%rax)
	movdqu	-32(%rax,%rdx), %xmm2
	movups	%xmm2, -32(%rax)
	movdqu	-16(%rax,%rdx), %xmm3
	movups	%xmm3, -16(%rax)
	cmpq	%rcx, %rax
	jne	.L184
.L185:
	addq	%r13, %rbx
	movq	%r14, 8(%r12)
	salq	$6, %rbx
	leaq	(%r14,%rbx), %rax
	addq	%r15, %r14
	movq	%rax, 16(%r12)
	movq	%r14, 24(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L196:
	.cfi_restore_state
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %r14
	jmp	.L182
.L195:
	leaq	.LC4(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE12054:
	.size	_ZNSt6vectorISt5arrayIPN2v88internal8compiler4NodeELm8EENS2_13ZoneAllocatorIS6_EEE17_M_default_appendEm, .-_ZNSt6vectorISt5arrayIPN2v88internal8compiler4NodeELm8EENS2_13ZoneAllocatorIS6_EEE17_M_default_appendEm
	.section	.text._ZN2v88internal8compiler16StateValuesCache15GetWorkingSpaceEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16StateValuesCache15GetWorkingSpaceEm
	.type	_ZN2v88internal8compiler16StateValuesCache15GetWorkingSpaceEm, @function
_ZN2v88internal8compiler16StateValuesCache15GetWorkingSpaceEm:
.LFB10849:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	48(%rdi), %rcx
	movq	%rsi, %rbx
	movq	40(%rdi), %rax
	movq	%rcx, %rdx
	subq	%rax, %rdx
	sarq	$6, %rdx
	cmpq	%rdx, %rsi
	jnb	.L201
.L198:
	salq	$6, %rbx
	addq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L201:
	.cfi_restore_state
	leaq	1(%rsi), %rsi
	movq	%rdi, %r12
	cmpq	%rdx, %rsi
	ja	.L202
	jnb	.L198
	salq	$6, %rsi
	addq	%rax, %rsi
	cmpq	%rsi, %rcx
	je	.L198
	movq	%rsi, 48(%rdi)
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L202:
	subq	%rdx, %rsi
	leaq	32(%rdi), %rdi
	salq	$6, %rbx
	call	_ZNSt6vectorISt5arrayIPN2v88internal8compiler4NodeELm8EENS2_13ZoneAllocatorIS6_EEE17_M_default_appendEm
	movq	40(%r12), %rax
	addq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10849:
	.size	_ZN2v88internal8compiler16StateValuesCache15GetWorkingSpaceEm, .-_ZN2v88internal8compiler16StateValuesCache15GetWorkingSpaceEm
	.section	.text._ZN2v84base19TemplateHashMapImplIPvS2_NS0_26HashEqualityThenKeyMatcherIS2_PFbS2_S2_EEENS_8internal20ZoneAllocationPolicyEE6ResizeES8_,"axG",@progbits,_ZN2v84base19TemplateHashMapImplIPvS2_NS0_26HashEqualityThenKeyMatcherIS2_PFbS2_S2_EEENS_8internal20ZoneAllocationPolicyEE6ResizeES8_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_26HashEqualityThenKeyMatcherIS2_PFbS2_S2_EEENS_8internal20ZoneAllocationPolicyEE6ResizeES8_
	.type	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_26HashEqualityThenKeyMatcherIS2_PFbS2_S2_EEENS_8internal20ZoneAllocationPolicyEE6ResizeES8_, @function
_ZN2v84base19TemplateHashMapImplIPvS2_NS0_26HashEqualityThenKeyMatcherIS2_PFbS2_S2_EEENS_8internal20ZoneAllocationPolicyEE6ResizeES8_:
.LFB12435:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	12(%rdi), %eax
	movq	(%rdi), %r13
	movq	%rsi, -64(%rbp)
	movl	%eax, -52(%rbp)
	movl	8(%rdi), %eax
	addl	%eax, %eax
	leaq	(%rax,%rax,2), %rsi
	movq	%rax, %rbx
	movq	16(%rdx), %rax
	movq	24(%rdx), %rdx
	salq	$3, %rsi
	movq	%rdx, %rcx
	movq	%rdx, -72(%rbp)
	subq	%rax, %rcx
	cmpq	%rcx, %rsi
	ja	.L239
	movq	-64(%rbp), %rdx
	addq	%rax, %rsi
	movq	%rsi, 16(%rdx)
.L205:
	movq	%rax, (%r14)
	testq	%rax, %rax
	je	.L240
	movl	%ebx, 8(%r14)
	testl	%ebx, %ebx
	je	.L207
	movq	$0, (%rax)
	cmpl	$1, 8(%r14)
	jbe	.L207
	movl	$24, %ecx
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L208:
	movq	(%r14), %rsi
	addq	$1, %rax
	movq	$0, (%rsi,%rcx)
	movl	8(%r14), %esi
	addq	$24, %rcx
	cmpq	%rax, %rsi
	ja	.L208
.L207:
	movl	-52(%rbp), %eax
	movl	$0, 12(%r14)
	testl	%eax, %eax
	je	.L203
	.p2align 4,,10
	.p2align 3
.L209:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L210
	movl	8(%r14), %esi
	movl	16(%r13), %r12d
	movq	(%r14), %r9
	subl	$1, %esi
	movl	%esi, %r15d
	andl	%r12d, %r15d
	leaq	(%r15,%r15,2), %rbx
	salq	$3, %rbx
	leaq	(%r9,%rbx), %rax
	movq	(%rax), %r8
	testq	%r8, %r8
	je	.L211
.L217:
	cmpl	16(%rax), %r12d
	je	.L241
.L212:
	addq	$1, %r15
	andl	%esi, %r15d
	leaq	(%r15,%r15,2), %rbx
	salq	$3, %rbx
	leaq	(%r9,%rbx), %rax
	movq	(%rax), %r8
	testq	%r8, %r8
	jne	.L217
	movl	16(%r13), %r12d
.L211:
	movq	%rdi, %xmm0
	movhps	8(%r13), %xmm0
	movl	%r12d, 16(%rax)
	movups	%xmm0, (%rax)
	movl	12(%r14), %eax
	addl	$1, %eax
	movl	%eax, %esi
	movl	%eax, 12(%r14)
	shrl	$2, %esi
	addl	%esi, %eax
	cmpl	8(%r14), %eax
	jnb	.L214
.L218:
	addq	$24, %r13
	subl	$1, -52(%rbp)
	jne	.L209
.L203:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L241:
	.cfi_restore_state
	movq	%r8, %rsi
	call	*16(%r14)
	testb	%al, %al
	jne	.L213
	movl	8(%r14), %esi
	movq	(%r14), %r9
	movq	0(%r13), %rdi
	subl	$1, %esi
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L210:
	addq	$24, %r13
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L213:
	movq	(%r14), %rax
	movl	16(%r13), %r12d
	movq	0(%r13), %rdi
	addq	%rbx, %rax
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L214:
	movq	-64(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_26HashEqualityThenKeyMatcherIS2_PFbS2_S2_EEENS_8internal20ZoneAllocationPolicyEE6ResizeES8_
	movl	8(%r14), %edi
	movq	(%r14), %r8
	subl	$1, %edi
	movl	%edi, %ebx
	andl	%r12d, %ebx
	leaq	(%rbx,%rbx,2), %rax
	leaq	(%r8,%rax,8), %rax
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	je	.L218
	cmpl	%r12d, 16(%rax)
	je	.L242
	.p2align 4,,10
	.p2align 3
.L219:
	addq	$1, %rbx
	andl	%edi, %ebx
	leaq	(%rbx,%rbx,2), %rax
	leaq	(%r8,%rax,8), %rax
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	je	.L218
	cmpl	%r12d, 16(%rax)
	jne	.L219
.L242:
	movq	0(%r13), %rdi
	call	*16(%r14)
	testb	%al, %al
	jne	.L218
	movl	8(%r14), %edi
	movq	(%r14), %r8
	subl	$1, %edi
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L239:
	movq	-64(%rbp), %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L205
.L240:
	leaq	.LC0(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE12435:
	.size	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_26HashEqualityThenKeyMatcherIS2_PFbS2_S2_EEENS_8internal20ZoneAllocationPolicyEE6ResizeES8_, .-_ZN2v84base19TemplateHashMapImplIPvS2_NS0_26HashEqualityThenKeyMatcherIS2_PFbS2_S2_EEENS_8internal20ZoneAllocationPolicyEE6ResizeES8_
	.section	.text._ZN2v88internal8compiler16StateValuesCache22GetValuesNodeFromCacheEPPNS1_4NodeEmNS1_15SparseInputMaskE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16StateValuesCache22GetValuesNodeFromCacheEPPNS1_4NodeEmNS1_15SparseInputMaskE
	.type	_ZN2v88internal8compiler16StateValuesCache22GetValuesNodeFromCacheEPPNS1_4NodeEmNS1_15SparseInputMaskE, @function
_ZN2v88internal8compiler16StateValuesCache22GetValuesNodeFromCacheEPPNS1_4NodeEmNS1_15SparseInputMaskE:
.LFB10851:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movl	%ecx, -116(%rbp)
	movq	%fs:40, %rbx
	movq	%rbx, -56(%rbp)
	xorl	%ebx, %ebx
	movq	$0, -96(%rbp)
	movq	%rdx, -88(%rbp)
	movl	%ecx, -80(%rbp)
	movq	%rsi, -72(%rbp)
	testq	%rdx, %rdx
	je	.L262
	movq	%rsi, %rdx
	movq	%r14, %rax
	leaq	(%rsi,%r14,8), %rsi
	.p2align 4,,10
	.p2align 3
.L246:
	leaq	(%rax,%rax,2), %rcx
	salq	$3, %rcx
	subq	%rax, %rcx
	movq	%rcx, %rax
	movq	(%rdx), %rcx
	testq	%rcx, %rcx
	je	.L245
	movl	20(%rcx), %ecx
	andl	$16777215, %ecx
	addq	%rcx, %rax
.L245:
	addq	$8, %rdx
	cmpq	%rdx, %rsi
	jne	.L246
	movl	%eax, %ebx
	andl	$2147483647, %ebx
.L244:
	movq	(%r12), %rax
	movq	8(%r12), %rdi
	leaq	-96(%rbp), %r8
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	%rax, -128(%rbp)
	movl	16(%r12), %eax
	subl	$1, %eax
	movl	%eax, %edx
	andl	%ebx, %edx
	leaq	(%rdx,%rdx,2), %rcx
	leaq	0(,%rcx,8), %r15
	leaq	(%rdi,%r15), %r10
	movq	(%r10), %rsi
	testq	%rsi, %rsi
	je	.L251
.L252:
	cmpl	%ebx, 16(%r10)
	je	.L277
.L248:
	addq	$1, %rdx
	andl	%eax, %edx
	leaq	(%rdx,%rdx,2), %rcx
	leaq	0(,%rcx,8), %r15
	leaq	(%rdi,%r15), %r10
	movq	(%r10), %rsi
	testq	%rsi, %rsi
	jne	.L252
.L251:
	movq	%r8, (%r10)
	movq	$0, 8(%r10)
	movl	%ebx, 16(%r10)
	movl	20(%r12), %eax
	addl	$1, %eax
	movl	%eax, %edx
	movl	%eax, 20(%r12)
	shrl	$2, %edx
	addl	%edx, %eax
	cmpl	16(%r12), %eax
	jb	.L250
	movq	-128(%rbp), %rsi
	leaq	8(%r12), %rdi
	movq	%r8, -104(%rbp)
	call	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_26HashEqualityThenKeyMatcherIS2_PFbS2_S2_EEENS_8internal20ZoneAllocationPolicyEE6ResizeES8_
	movl	16(%r12), %eax
	movq	8(%r12), %rcx
	movq	-104(%rbp), %r8
	subl	$1, %eax
	movl	%eax, %r15d
	andl	%ebx, %r15d
	leaq	(%r15,%r15,2), %rdx
	salq	$3, %rdx
	leaq	(%rcx,%rdx), %r10
	movq	(%r10), %rsi
	testq	%rsi, %rsi
	je	.L250
	cmpl	%ebx, 16(%r10)
	je	.L278
	.p2align 4,,10
	.p2align 3
.L255:
	addq	$1, %r15
	andl	%eax, %r15d
	leaq	(%r15,%r15,2), %rdx
	salq	$3, %rdx
	leaq	(%rcx,%rdx), %r10
	movq	(%r10), %rsi
	testq	%rsi, %rsi
	je	.L250
	cmpl	%ebx, 16(%r10)
	jne	.L255
.L278:
	movq	%rdx, -104(%rbp)
	movq	%r8, %rdi
	movq	%r8, -112(%rbp)
	call	*24(%r12)
	movq	-104(%rbp), %rdx
	testb	%al, %al
	jne	.L256
	movl	16(%r12), %eax
	movq	8(%r12), %rcx
	movq	-112(%rbp), %r8
	subl	$1, %eax
	jmp	.L255
	.p2align 4,,10
	.p2align 3
.L277:
	movq	%r8, -104(%rbp)
	movq	%r8, %rdi
	movq	%rdx, -112(%rbp)
	call	*24(%r12)
	movq	-104(%rbp), %r8
	testb	%al, %al
	jne	.L249
	movl	16(%r12), %eax
	movq	8(%r12), %rdi
	movq	-112(%rbp), %rdx
	subl	$1, %eax
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L249:
	addq	8(%r12), %r15
	cmpq	$0, (%r15)
	movq	%r15, %r10
	je	.L251
.L250:
	movq	8(%r10), %r15
	testq	%r15, %r15
	je	.L279
.L243:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L280
	addq	$88, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L279:
	.cfi_restore_state
	movq	(%r12), %rax
	movl	-116(%rbp), %edx
	movl	%r14d, %esi
	movq	%r10, -104(%rbp)
	movq	8(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler21CommonOperatorBuilder11StateValuesEiNS1_15SparseInputMaskE@PLT
	movq	%r15, %rdi
	xorl	%r8d, %r8d
	movl	%r14d, %edx
	movq	%rax, %rsi
	movq	%r13, %rcx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-104(%rbp), %r10
	movq	%rax, %r15
	movq	(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$7, %rdx
	jbe	.L281
	leaq	8(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L260:
	movq	%r15, (%rax)
	movq	%rax, (%r10)
	movq	%r15, 8(%r10)
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L262:
	xorl	%ebx, %ebx
	jmp	.L244
.L256:
	addq	8(%r12), %rdx
	movq	%rdx, %r10
	jmp	.L250
.L281:
	movl	$8, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-104(%rbp), %r10
	jmp	.L260
.L280:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10851:
	.size	_ZN2v88internal8compiler16StateValuesCache22GetValuesNodeFromCacheEPPNS1_4NodeEmNS1_15SparseInputMaskE, .-_ZN2v88internal8compiler16StateValuesCache22GetValuesNodeFromCacheEPPNS1_4NodeEmNS1_15SparseInputMaskE
	.section	.text._ZN2v88internal8compiler16StateValuesCache9BuildTreeEPmPPNS1_4NodeEmPKNS0_9BitVectorEim,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16StateValuesCache9BuildTreeEPmPPNS1_4NodeEmPKNS0_9BitVectorEim
	.type	_ZN2v88internal8compiler16StateValuesCache9BuildTreeEPmPPNS1_4NodeEmPKNS0_9BitVectorEim, @function
_ZN2v88internal8compiler16StateValuesCache9BuildTreeEPmPPNS1_4NodeEmPKNS0_9BitVectorEim:
.LFB10853:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r11
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	48(%rdi), %rcx
	movq	40(%rdi), %r10
	movq	16(%rbp), %r12
	movq	%rcx, %rax
	subq	%r10, %rax
	sarq	$6, %rax
	cmpq	%rax, %r12
	jnb	.L342
.L283:
	movq	%r12, %rax
	salq	$6, %rax
	addq	%rax, %r10
	movq	(%rbx), %rax
	testq	%r12, %r12
	je	.L343
	xorl	%r14d, %r14d
	cmpq	%rax, %r13
	jbe	.L341
	movq	%r10, -72(%rbp)
	movl	$8, %r15d
	movq	%rdx, -80(%rbp)
	movq	%r12, -88(%rbp)
	movq	%rbx, %r12
	movq	%r11, %rbx
.L299:
	movq	%r13, %rcx
	movq	%r15, %rdx
	subq	%rax, %rcx
	subq	%r14, %rdx
	cmpq	%rdx, %rcx
	jb	.L344
	movq	-88(%rbp), %rax
	subq	$8, %rsp
	movq	-80(%rbp), %rdx
	movq	%r13, %rcx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movl	%r9d, -64(%rbp)
	addq	$1, %r14
	subq	$1, %rax
	movq	%r8, -56(%rbp)
	pushq	%rax
	call	_ZN2v88internal8compiler16StateValuesCache9BuildTreeEPmPPNS1_4NodeEmPKNS0_9BitVectorEim
	movq	-72(%rbp), %rdi
	movq	-56(%rbp), %r8
	movl	-64(%rbp), %r9d
	movq	%rax, -8(%rdi,%r14,8)
	movq	(%r12), %rax
	popq	%rdx
	popq	%rcx
	cmpq	%r13, %rax
	jnb	.L345
	cmpq	$8, %r14
	jne	.L299
	movq	%rdi, %r10
	movq	%rbx, %r11
.L341:
	xorl	%ecx, %ecx
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L343:
	cmpq	%rax, %r13
	jbe	.L346
	testq	%r8, %r8
	je	.L316
	xorl	%ecx, %ecx
	xorl	%edi, %edi
	jmp	.L296
	.p2align 4,,10
	.p2align 3
.L348:
	cmpq	$7, %r12
	ja	.L294
	cmpq	$31, %rcx
	je	.L347
.L296:
	cmpl	$1, 4(%r8)
	movq	8(%r8), %r14
	leal	(%r9,%rax), %esi
	je	.L293
	testl	%esi, %esi
	leal	63(%rsi), %r15d
	cmovns	%esi, %r15d
	sarl	$6, %r15d
	movslq	%r15d, %r15
	movq	(%r14,%r15,8), %r14
.L293:
	movl	%esi, %r15d
	sarl	$31, %r15d
	shrl	$26, %r15d
	addl	%r15d, %esi
	andl	$63, %esi
	subl	%r15d, %esi
	btq	%rsi, %r14
	jnc	.L291
	movq	(%rdx,%rax,8), %rax
	movl	$1, %esi
	sall	%cl, %esi
	movq	%rax, (%r10,%r12,8)
	orl	%esi, %edi
	addq	$1, %r12
.L291:
	movq	(%rbx), %rax
	addq	$1, %rcx
	addq	$1, %rax
	movq	%rax, (%rbx)
	cmpq	%rax, %r13
	ja	.L348
.L294:
	btsl	%ecx, %edi
	movq	%r12, %r14
	movl	%edi, %ecx
.L298:
	leaq	-40(%rbp), %rsp
	movq	%r14, %rdx
	movq	%r10, %rsi
	movq	%r11, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler16StateValuesCache22GetValuesNodeFromCacheEPPNS1_4NodeEmNS1_15SparseInputMaskE
	.p2align 4,,10
	.p2align 3
.L342:
	.cfi_restore_state
	leaq	1(%r12), %rsi
	cmpq	%rsi, %rax
	jb	.L349
	jbe	.L283
	salq	$6, %rsi
	addq	%r10, %rsi
	cmpq	%rsi, %rcx
	je	.L283
	movq	%rsi, 48(%rdi)
	jmp	.L283
	.p2align 4,,10
	.p2align 3
.L344:
	movq	%rbx, %r11
	movq	-72(%rbp), %r10
	movq	-80(%rbp), %rdx
	movq	%r12, %rbx
	cmpq	%rax, %r13
	jbe	.L319
	testq	%r8, %r8
	je	.L320
	movl	$0, -56(%rbp)
	movq	%r14, %r12
	movq	%r14, %rsi
	.p2align 4,,10
	.p2align 3
.L309:
	cmpl	$1, 4(%r8)
	movq	8(%r8), %rcx
	leal	(%r9,%rax), %edi
	je	.L308
	testl	%edi, %edi
	leal	63(%rdi), %r15d
	cmovns	%edi, %r15d
	sarl	$6, %r15d
	movslq	%r15d, %r15
	movq	(%rcx,%r15,8), %rcx
.L308:
	movl	%edi, %r15d
	sarl	$31, %r15d
	shrl	$26, %r15d
	addl	%r15d, %edi
	andl	$63, %edi
	subl	%r15d, %edi
	btq	%rdi, %rcx
	jc	.L306
	movq	(%rbx), %rax
	addq	$1, %rsi
	addq	$1, %rax
	cmpq	$30, %rsi
	seta	%dil
	cmpq	%rax, %r13
	movq	%rax, (%rbx)
	setbe	%cl
	orb	%cl, %dil
	jne	.L321
	cmpq	$7, %r12
	jbe	.L309
.L321:
	movq	%r12, %r15
.L301:
	movl	%r14d, %ecx
	movl	$1, %eax
	movq	%r15, %r14
	sall	%cl, %eax
	leal	-1(%rax), %ecx
	orl	-56(%rbp), %ecx
	btsl	%esi, %ecx
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L306:
	movq	(%rdx,%rax,8), %rax
	movl	%esi, %ecx
	movl	$1, %edi
	addq	$1, %rsi
	sall	%cl, %edi
	orl	%edi, -56(%rbp)
	leaq	1(%r12), %r15
	movq	%rax, (%r10,%r12,8)
	movq	(%rbx), %rax
	addq	$1, %rax
	cmpq	%rax, %r13
	movq	%rax, (%rbx)
	setbe	%dil
	cmpq	$30, %rsi
	seta	%cl
	orb	%cl, %dil
	jne	.L301
	cmpq	$7, %r15
	ja	.L301
	movq	%r15, %r12
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L345:
	movq	%rdi, %r10
	movq	%rbx, %r11
	cmpq	$1, %r14
	jne	.L341
	movq	(%r10), %rax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L349:
	.cfi_restore_state
	subq	%rax, %rsi
	leaq	32(%rdi), %rdi
	movl	%r9d, -80(%rbp)
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r11, -56(%rbp)
	call	_ZNSt6vectorISt5arrayIPN2v88internal8compiler4NodeELm8EENS2_13ZoneAllocatorIS6_EEE17_M_default_appendEm
	movq	-56(%rbp), %r11
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movl	-80(%rbp), %r9d
	movq	40(%r11), %r10
	jmp	.L283
	.p2align 4,,10
	.p2align 3
.L347:
	orl	$-2147483648, %edi
	movq	%r12, %r14
	movl	%edi, %ecx
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L316:
	xorl	%edi, %edi
	movl	$1, %esi
	movq	%r12, %rcx
	jmp	.L288
	.p2align 4,,10
	.p2align 3
.L350:
	cmpq	$7, %rcx
	ja	.L322
.L288:
	movq	(%rdx,%rax,8), %rax
	movl	%esi, %r8d
	sall	%cl, %r8d
	addq	$1, %rcx
	movq	%rax, -8(%r10,%rcx,8)
	movq	(%rbx), %rax
	orl	%r8d, %edi
	addq	$1, %rax
	movq	%rax, (%rbx)
	cmpq	%rax, %r13
	ja	.L350
.L322:
	movq	%rcx, %r12
	jmp	.L294
	.p2align 4,,10
	.p2align 3
.L320:
	xorl	%r8d, %r8d
	movl	$1, %edi
	movq	%r14, %rcx
	jmp	.L302
	.p2align 4,,10
	.p2align 3
.L351:
	cmpq	%rax, %r13
	jbe	.L323
.L302:
	movl	%edi, %esi
	movq	(%rdx,%rax,8), %rax
	sall	%cl, %esi
	orl	%esi, %r8d
	movq	%rcx, %rsi
	addq	$1, %rcx
	movq	%rax, -8(%r10,%rcx,8)
	movq	(%rbx), %rax
	subq	$7, %rsi
	addq	$1, %rax
	movq	%rax, (%rbx)
	cmpq	$-9, %rsi
	ja	.L351
.L323:
	movl	%r8d, -56(%rbp)
	movq	%rcx, %rsi
	movq	%rcx, %r15
	jmp	.L301
.L319:
	movl	$0, -56(%rbp)
	movq	%r14, %r15
	movq	%r14, %rsi
	jmp	.L301
.L346:
	xorl	%r14d, %r14d
	movl	$1, %ecx
	jmp	.L298
	.cfi_endproc
.LFE10853:
	.size	_ZN2v88internal8compiler16StateValuesCache9BuildTreeEPmPPNS1_4NodeEmPKNS0_9BitVectorEim, .-_ZN2v88internal8compiler16StateValuesCache9BuildTreeEPmPPNS1_4NodeEmPKNS0_9BitVectorEim
	.section	.text._ZN2v88internal8compiler16StateValuesCache16GetNodeForValuesEPPNS1_4NodeEmPKNS0_9BitVectorEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16StateValuesCache16GetNodeForValuesEPPNS1_4NodeEmPKNS0_9BitVectorEi
	.type	_ZN2v88internal8compiler16StateValuesCache16GetNodeForValuesEPPNS1_4NodeEmPKNS0_9BitVectorEi, @function
_ZN2v88internal8compiler16StateValuesCache16GetNodeForValuesEPPNS1_4NodeEmPKNS0_9BitVectorEi:
.LFB10854:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L362
	movl	%r8d, %r9d
	cmpq	$8, %rdx
	jbe	.L359
	movl	$8, %eax
	xorl	%edi, %edi
	.p2align 4,,10
	.p2align 3
.L357:
	salq	$3, %rax
	addq	$1, %rdi
	cmpq	%rax, %rdx
	ja	.L357
.L356:
	subq	$8, %rsp
	leaq	-32(%rbp), %r11
	movq	%rcx, %r8
	movq	%rdx, %rcx
	pushq	%rdi
	movq	%rsi, %rdx
	movq	%r12, %rdi
	movq	%r11, %rsi
	movq	$0, -32(%rbp)
	call	_ZN2v88internal8compiler16StateValuesCache9BuildTreeEPmPPNS1_4NodeEmPKNS0_9BitVectorEim
	popq	%rdx
	popq	%rcx
.L352:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L363
	leaq	-16(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L362:
	.cfi_restore_state
	movq	64(%rdi), %rax
	testq	%rax, %rax
	jne	.L352
	movq	(%rdi), %rax
	xorl	%esi, %esi
	movq	8(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler21CommonOperatorBuilder11StateValuesEiNS1_15SparseInputMaskE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 64(%r12)
	jmp	.L352
	.p2align 4,,10
	.p2align 3
.L359:
	xorl	%edi, %edi
	jmp	.L356
.L363:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10854:
	.size	_ZN2v88internal8compiler16StateValuesCache16GetNodeForValuesEPPNS1_4NodeEmPKNS0_9BitVectorEi, .-_ZN2v88internal8compiler16StateValuesCache16GetNodeForValuesEPPNS1_4NodeEmPKNS0_9BitVectorEi
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler16StateValuesCacheC2EPNS1_7JSGraphE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler16StateValuesCacheC2EPNS1_7JSGraphE, @function
_GLOBAL__sub_I__ZN2v88internal8compiler16StateValuesCacheC2EPNS1_7JSGraphE:
.LFB12627:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE12627:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler16StateValuesCacheC2EPNS1_7JSGraphE, .-_GLOBAL__sub_I__ZN2v88internal8compiler16StateValuesCacheC2EPNS1_7JSGraphE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler16StateValuesCacheC2EPNS1_7JSGraphE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
