	.file	"serializer-for-background-compilation.cc"
	.text
	.section	.text._ZNKSt5ctypeIcE8do_widenEc,"axG",@progbits,_ZNKSt5ctypeIcE8do_widenEc,comdat
	.align 2
	.p2align 4
	.weak	_ZNKSt5ctypeIcE8do_widenEc
	.type	_ZNKSt5ctypeIcE8do_widenEc, @function
_ZNKSt5ctypeIcE8do_widenEc:
.LFB3330:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	ret
	.cfi_endproc
.LFE3330:
	.size	_ZNKSt5ctypeIcE8do_widenEc, .-_ZNKSt5ctypeIcE8do_widenEc
	.section	.rodata._ZN2v88internal8compiler13JSReceiverRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb.str1.1,"aMS",@progbits,1
.LC0:
	.string	"IsJSReceiver()"
.LC1:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal8compiler13JSReceiverRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,"axG",@progbits,_ZN2v88internal8compiler13JSReceiverRefC5EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler13JSReceiverRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.type	_ZN2v88internal8compiler13JSReceiverRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, @function
_ZN2v88internal8compiler13JSReceiverRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb:
.LFB9619:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%ecx, %ebx
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	testb	%bl, %bl
	jne	.L6
.L3:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsJSReceiverEv@PLT
	testb	%al, %al
	jne	.L3
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE9619:
	.size	_ZN2v88internal8compiler13JSReceiverRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, .-_ZN2v88internal8compiler13JSReceiverRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.weak	_ZN2v88internal8compiler13JSReceiverRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.set	_ZN2v88internal8compiler13JSReceiverRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,_ZN2v88internal8compiler13JSReceiverRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.section	.rodata._ZN2v88internal8compiler11JSObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb.str1.1,"aMS",@progbits,1
.LC2:
	.string	"IsJSObject()"
	.section	.text._ZN2v88internal8compiler11JSObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,"axG",@progbits,_ZN2v88internal8compiler11JSObjectRefC5EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler11JSObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.type	_ZN2v88internal8compiler11JSObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, @function
_ZN2v88internal8compiler11JSObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb:
.LFB9625:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%ecx, %ebx
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler13JSReceiverRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	testb	%bl, %bl
	jne	.L10
.L7:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef10IsJSObjectEv@PLT
	testb	%al, %al
	jne	.L7
	leaq	.LC2(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE9625:
	.size	_ZN2v88internal8compiler11JSObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, .-_ZN2v88internal8compiler11JSObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.weak	_ZN2v88internal8compiler11JSObjectRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.set	_ZN2v88internal8compiler11JSObjectRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,_ZN2v88internal8compiler11JSObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.section	.rodata._ZN2v88internal8compiler18JSBoundFunctionRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb.str1.1,"aMS",@progbits,1
.LC3:
	.string	"IsJSBoundFunction()"
	.section	.text._ZN2v88internal8compiler18JSBoundFunctionRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,"axG",@progbits,_ZN2v88internal8compiler18JSBoundFunctionRefC5EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler18JSBoundFunctionRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.type	_ZN2v88internal8compiler18JSBoundFunctionRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, @function
_ZN2v88internal8compiler18JSBoundFunctionRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb:
.LFB9637:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%ecx, %ebx
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler11JSObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	testb	%bl, %bl
	jne	.L14
.L11:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef17IsJSBoundFunctionEv@PLT
	testb	%al, %al
	jne	.L11
	leaq	.LC3(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE9637:
	.size	_ZN2v88internal8compiler18JSBoundFunctionRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, .-_ZN2v88internal8compiler18JSBoundFunctionRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.weak	_ZN2v88internal8compiler18JSBoundFunctionRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.set	_ZN2v88internal8compiler18JSBoundFunctionRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,_ZN2v88internal8compiler18JSBoundFunctionRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.section	.rodata._ZN2v88internal8compiler13JSFunctionRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb.str1.1,"aMS",@progbits,1
.LC4:
	.string	"IsJSFunction()"
	.section	.text._ZN2v88internal8compiler13JSFunctionRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,"axG",@progbits,_ZN2v88internal8compiler13JSFunctionRefC5EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler13JSFunctionRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.type	_ZN2v88internal8compiler13JSFunctionRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, @function
_ZN2v88internal8compiler13JSFunctionRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb:
.LFB9643:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%ecx, %ebx
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler11JSObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	testb	%bl, %bl
	jne	.L18
.L15:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsJSFunctionEv@PLT
	testb	%al, %al
	jne	.L15
	leaq	.LC4(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE9643:
	.size	_ZN2v88internal8compiler13JSFunctionRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, .-_ZN2v88internal8compiler13JSFunctionRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.weak	_ZN2v88internal8compiler13JSFunctionRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.set	_ZN2v88internal8compiler13JSFunctionRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,_ZN2v88internal8compiler13JSFunctionRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.section	.rodata._ZN2v88internal8compiler10ContextRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb.str1.1,"aMS",@progbits,1
.LC5:
	.string	"IsContext()"
	.section	.text._ZN2v88internal8compiler10ContextRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,"axG",@progbits,_ZN2v88internal8compiler10ContextRefC5EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler10ContextRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.type	_ZN2v88internal8compiler10ContextRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, @function
_ZN2v88internal8compiler10ContextRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb:
.LFB9661:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%ecx, %ebx
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	testb	%bl, %bl
	jne	.L22
.L19:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef9IsContextEv@PLT
	testb	%al, %al
	jne	.L19
	leaq	.LC5(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE9661:
	.size	_ZN2v88internal8compiler10ContextRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, .-_ZN2v88internal8compiler10ContextRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.weak	_ZN2v88internal8compiler10ContextRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.set	_ZN2v88internal8compiler10ContextRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,_ZN2v88internal8compiler10ContextRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.section	.rodata._ZN2v88internal8compiler7NameRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb.str1.1,"aMS",@progbits,1
.LC6:
	.string	"IsName()"
	.section	.text._ZN2v88internal8compiler7NameRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,"axG",@progbits,_ZN2v88internal8compiler7NameRefC5EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler7NameRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.type	_ZN2v88internal8compiler7NameRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, @function
_ZN2v88internal8compiler7NameRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb:
.LFB9673:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%ecx, %ebx
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	testb	%bl, %bl
	jne	.L26
.L23:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef6IsNameEv@PLT
	testb	%al, %al
	jne	.L23
	leaq	.LC6(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE9673:
	.size	_ZN2v88internal8compiler7NameRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, .-_ZN2v88internal8compiler7NameRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.weak	_ZN2v88internal8compiler7NameRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.set	_ZN2v88internal8compiler7NameRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,_ZN2v88internal8compiler7NameRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.section	.rodata._ZN2v88internal8compiler15FeedbackCellRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb.str1.1,"aMS",@progbits,1
.LC7:
	.string	"IsFeedbackCell()"
	.section	.text._ZN2v88internal8compiler15FeedbackCellRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,"axG",@progbits,_ZN2v88internal8compiler15FeedbackCellRefC5EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler15FeedbackCellRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.type	_ZN2v88internal8compiler15FeedbackCellRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, @function
_ZN2v88internal8compiler15FeedbackCellRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb:
.LFB9691:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%ecx, %ebx
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	testb	%bl, %bl
	jne	.L30
.L27:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef14IsFeedbackCellEv@PLT
	testb	%al, %al
	jne	.L27
	leaq	.LC7(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE9691:
	.size	_ZN2v88internal8compiler15FeedbackCellRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, .-_ZN2v88internal8compiler15FeedbackCellRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.weak	_ZN2v88internal8compiler15FeedbackCellRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.set	_ZN2v88internal8compiler15FeedbackCellRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,_ZN2v88internal8compiler15FeedbackCellRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.section	.rodata._ZN2v88internal8compiler17FeedbackVectorRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb.str1.1,"aMS",@progbits,1
.LC8:
	.string	"IsFeedbackVector()"
	.section	.text._ZN2v88internal8compiler17FeedbackVectorRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,"axG",@progbits,_ZN2v88internal8compiler17FeedbackVectorRefC5EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler17FeedbackVectorRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.type	_ZN2v88internal8compiler17FeedbackVectorRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, @function
_ZN2v88internal8compiler17FeedbackVectorRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb:
.LFB9697:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%ecx, %ebx
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	testb	%bl, %bl
	jne	.L34
.L31:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef16IsFeedbackVectorEv@PLT
	testb	%al, %al
	jne	.L31
	leaq	.LC8(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE9697:
	.size	_ZN2v88internal8compiler17FeedbackVectorRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, .-_ZN2v88internal8compiler17FeedbackVectorRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.weak	_ZN2v88internal8compiler17FeedbackVectorRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.set	_ZN2v88internal8compiler17FeedbackVectorRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,_ZN2v88internal8compiler17FeedbackVectorRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.section	.rodata._ZN2v88internal8compiler6MapRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb.str1.1,"aMS",@progbits,1
.LC9:
	.string	"IsMap()"
	.section	.text._ZN2v88internal8compiler6MapRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,"axG",@progbits,_ZN2v88internal8compiler6MapRefC5EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler6MapRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.type	_ZN2v88internal8compiler6MapRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, @function
_ZN2v88internal8compiler6MapRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb:
.LFB9727:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%ecx, %ebx
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	testb	%bl, %bl
	jne	.L38
.L35:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef5IsMapEv@PLT
	testb	%al, %al
	jne	.L35
	leaq	.LC9(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE9727:
	.size	_ZN2v88internal8compiler6MapRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, .-_ZN2v88internal8compiler6MapRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.weak	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.set	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,_ZN2v88internal8compiler6MapRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.section	.rodata._ZN2v88internal8compiler23FunctionTemplateInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb.str1.1,"aMS",@progbits,1
.LC10:
	.string	"IsFunctionTemplateInfo()"
	.section	.text._ZN2v88internal8compiler23FunctionTemplateInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,"axG",@progbits,_ZN2v88internal8compiler23FunctionTemplateInfoRefC5EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler23FunctionTemplateInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.type	_ZN2v88internal8compiler23FunctionTemplateInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, @function
_ZN2v88internal8compiler23FunctionTemplateInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb:
.LFB9736:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%ecx, %ebx
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	testb	%bl, %bl
	jne	.L42
.L39:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef22IsFunctionTemplateInfoEv@PLT
	testb	%al, %al
	jne	.L39
	leaq	.LC10(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE9736:
	.size	_ZN2v88internal8compiler23FunctionTemplateInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, .-_ZN2v88internal8compiler23FunctionTemplateInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.weak	_ZN2v88internal8compiler23FunctionTemplateInfoRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.set	_ZN2v88internal8compiler23FunctionTemplateInfoRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,_ZN2v88internal8compiler23FunctionTemplateInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.section	.rodata._ZN2v88internal8compiler17FixedArrayBaseRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb.str1.1,"aMS",@progbits,1
.LC11:
	.string	"IsFixedArrayBase()"
	.section	.text._ZN2v88internal8compiler17FixedArrayBaseRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,"axG",@progbits,_ZN2v88internal8compiler17FixedArrayBaseRefC5EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler17FixedArrayBaseRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.type	_ZN2v88internal8compiler17FixedArrayBaseRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, @function
_ZN2v88internal8compiler17FixedArrayBaseRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb:
.LFB9742:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%ecx, %ebx
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	testb	%bl, %bl
	jne	.L46
.L43:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef16IsFixedArrayBaseEv@PLT
	testb	%al, %al
	jne	.L43
	leaq	.LC11(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE9742:
	.size	_ZN2v88internal8compiler17FixedArrayBaseRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, .-_ZN2v88internal8compiler17FixedArrayBaseRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.weak	_ZN2v88internal8compiler17FixedArrayBaseRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.set	_ZN2v88internal8compiler17FixedArrayBaseRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,_ZN2v88internal8compiler17FixedArrayBaseRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.section	.rodata._ZN2v88internal8compiler16BytecodeArrayRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb.str1.1,"aMS",@progbits,1
.LC12:
	.string	"IsBytecodeArray()"
	.section	.text._ZN2v88internal8compiler16BytecodeArrayRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,"axG",@progbits,_ZN2v88internal8compiler16BytecodeArrayRefC5EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler16BytecodeArrayRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.type	_ZN2v88internal8compiler16BytecodeArrayRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, @function
_ZN2v88internal8compiler16BytecodeArrayRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb:
.LFB9760:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%ecx, %ebx
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler17FixedArrayBaseRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	testb	%bl, %bl
	jne	.L50
.L47:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef15IsBytecodeArrayEv@PLT
	testb	%al, %al
	jne	.L47
	leaq	.LC12(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE9760:
	.size	_ZN2v88internal8compiler16BytecodeArrayRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, .-_ZN2v88internal8compiler16BytecodeArrayRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.weak	_ZN2v88internal8compiler16BytecodeArrayRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.set	_ZN2v88internal8compiler16BytecodeArrayRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,_ZN2v88internal8compiler16BytecodeArrayRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.section	.rodata._ZN2v88internal8compiler12ScopeInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb.str1.1,"aMS",@progbits,1
.LC13:
	.string	"IsScopeInfo()"
	.section	.text._ZN2v88internal8compiler12ScopeInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,"axG",@progbits,_ZN2v88internal8compiler12ScopeInfoRefC5EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler12ScopeInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.type	_ZN2v88internal8compiler12ScopeInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, @function
_ZN2v88internal8compiler12ScopeInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb:
.LFB9772:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%ecx, %ebx
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	testb	%bl, %bl
	jne	.L54
.L51:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef11IsScopeInfoEv@PLT
	testb	%al, %al
	jne	.L51
	leaq	.LC13(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE9772:
	.size	_ZN2v88internal8compiler12ScopeInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, .-_ZN2v88internal8compiler12ScopeInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.weak	_ZN2v88internal8compiler12ScopeInfoRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.set	_ZN2v88internal8compiler12ScopeInfoRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,_ZN2v88internal8compiler12ScopeInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.section	.rodata._ZN2v88internal8compiler21SharedFunctionInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb.str1.1,"aMS",@progbits,1
.LC14:
	.string	"IsSharedFunctionInfo()"
	.section	.text._ZN2v88internal8compiler21SharedFunctionInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,"axG",@progbits,_ZN2v88internal8compiler21SharedFunctionInfoRefC5EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler21SharedFunctionInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.type	_ZN2v88internal8compiler21SharedFunctionInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, @function
_ZN2v88internal8compiler21SharedFunctionInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb:
.LFB9778:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%ecx, %ebx
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	testb	%bl, %bl
	jne	.L58
.L55:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L58:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef20IsSharedFunctionInfoEv@PLT
	testb	%al, %al
	jne	.L55
	leaq	.LC14(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE9778:
	.size	_ZN2v88internal8compiler21SharedFunctionInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, .-_ZN2v88internal8compiler21SharedFunctionInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.weak	_ZN2v88internal8compiler21SharedFunctionInfoRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.set	_ZN2v88internal8compiler21SharedFunctionInfoRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,_ZN2v88internal8compiler21SharedFunctionInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.section	.rodata._ZN2v88internal8compiler9StringRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb.str1.1,"aMS",@progbits,1
.LC15:
	.string	"IsString()"
	.section	.text._ZN2v88internal8compiler9StringRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,"axG",@progbits,_ZN2v88internal8compiler9StringRefC5EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler9StringRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.type	_ZN2v88internal8compiler9StringRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, @function
_ZN2v88internal8compiler9StringRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb:
.LFB9784:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%ecx, %ebx
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler7NameRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	testb	%bl, %bl
	jne	.L62
.L59:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef8IsStringEv@PLT
	testb	%al, %al
	jne	.L59
	leaq	.LC15(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE9784:
	.size	_ZN2v88internal8compiler9StringRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, .-_ZN2v88internal8compiler9StringRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.weak	_ZN2v88internal8compiler9StringRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.set	_ZN2v88internal8compiler9StringRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,_ZN2v88internal8compiler9StringRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.section	.text._ZNK2v88internal18SharedFunctionInfo16GetBytecodeArrayEv,"axG",@progbits,_ZNK2v88internal18SharedFunctionInfo16GetBytecodeArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal18SharedFunctionInfo16GetBytecodeArrayEv
	.type	_ZNK2v88internal18SharedFunctionInfo16GetBytecodeArrayEv, @function
_ZNK2v88internal18SharedFunctionInfo16GetBytecodeArrayEv:
.LFB16233:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movq	31(%rdx), %rax
	testb	$1, %al
	jne	.L69
.L64:
	movq	7(%rdx), %rax
	testb	$1, %al
	jne	.L70
.L68:
	movq	(%rdi), %rax
	movq	7(%rax), %rax
	movq	7(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L69:
	movq	-1(%rax), %rcx
	cmpw	$86, 11(%rcx)
	jne	.L64
	movq	39(%rax), %rcx
	testb	$1, %cl
	je	.L64
	movq	-1(%rcx), %rcx
	cmpw	$72, 11(%rcx)
	jne	.L64
	movq	31(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L70:
	movq	-1(%rax), %rax
	cmpw	$72, 11(%rax)
	jne	.L68
	movq	(%rdi), %rax
	movq	7(%rax), %rax
	ret
	.cfi_endproc
.LFE16233:
	.size	_ZNK2v88internal18SharedFunctionInfo16GetBytecodeArrayEv, .-_ZNK2v88internal18SharedFunctionInfo16GetBytecodeArrayEv
	.section	.text._ZN2v88internal8compiler5HintsC2EPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler5HintsC2EPNS0_4ZoneE
	.type	_ZN2v88internal8compiler5HintsC2EPNS0_4ZoneE, @function
_ZN2v88internal8compiler5HintsC2EPNS0_4ZoneE:
.LFB18733:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rax
	movq	%rsi, (%rdi)
	movq	%rax, 32(%rdi)
	movq	%rax, 40(%rdi)
	leaq	72(%rdi), %rax
	movq	%rax, 88(%rdi)
	movq	%rax, 96(%rdi)
	leaq	128(%rdi), %rax
	movq	%rax, 144(%rdi)
	movq	%rax, 152(%rdi)
	leaq	184(%rdi), %rax
	movl	$0, 16(%rdi)
	movq	$0, 24(%rdi)
	movq	$0, 48(%rdi)
	movq	%rsi, 56(%rdi)
	movl	$0, 72(%rdi)
	movq	$0, 80(%rdi)
	movq	$0, 104(%rdi)
	movq	%rsi, 112(%rdi)
	movl	$0, 128(%rdi)
	movq	$0, 136(%rdi)
	movq	$0, 160(%rdi)
	movq	%rsi, 168(%rdi)
	movl	$0, 184(%rdi)
	movq	$0, 192(%rdi)
	movq	%rax, 200(%rdi)
	movq	%rax, 208(%rdi)
	movq	$0, 216(%rdi)
	ret
	.cfi_endproc
.LFE18733:
	.size	_ZN2v88internal8compiler5HintsC2EPNS0_4ZoneE, .-_ZN2v88internal8compiler5HintsC2EPNS0_4ZoneE
	.globl	_ZN2v88internal8compiler5HintsC1EPNS0_4ZoneE
	.set	_ZN2v88internal8compiler5HintsC1EPNS0_4ZoneE,_ZN2v88internal8compiler5HintsC2EPNS0_4ZoneE
	.section	.text._ZN2v88internal8compiler5HintsC2EOS2_,"axG",@progbits,_ZN2v88internal8compiler5HintsC5EOS2_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler5HintsC2EOS2_
	.type	_ZN2v88internal8compiler5HintsC2EOS2_, @function
_ZN2v88internal8compiler5HintsC2EOS2_:
.LFB18785:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	leaq	16(%rdi), %rdx
	movq	%rax, (%rdi)
	movq	24(%rsi), %rax
	testq	%rax, %rax
	je	.L73
	movl	16(%rsi), %ecx
	movq	%rax, 24(%rdi)
	movl	%ecx, 16(%rdi)
	movq	32(%rsi), %rcx
	movq	%rcx, 32(%rdi)
	movq	40(%rsi), %rcx
	movq	%rcx, 40(%rdi)
	movq	%rdx, 8(%rax)
	movq	48(%rsi), %rax
	movq	$0, 24(%rsi)
	movq	%rax, 48(%rdi)
	leaq	16(%rsi), %rax
	movq	%rax, 32(%rsi)
	movq	%rax, 40(%rsi)
	movq	$0, 48(%rsi)
.L74:
	movq	56(%rsi), %rax
	leaq	72(%rdi), %rdx
	movq	%rax, 56(%rdi)
	movq	80(%rsi), %rax
	testq	%rax, %rax
	je	.L75
	movl	72(%rsi), %ecx
	movq	%rax, 80(%rdi)
	movl	%ecx, 72(%rdi)
	movq	88(%rsi), %rcx
	movq	%rcx, 88(%rdi)
	movq	96(%rsi), %rcx
	movq	%rcx, 96(%rdi)
	movq	%rdx, 8(%rax)
	movq	104(%rsi), %rax
	movq	$0, 80(%rsi)
	movq	%rax, 104(%rdi)
	leaq	72(%rsi), %rax
	movq	%rax, 88(%rsi)
	movq	%rax, 96(%rsi)
	movq	$0, 104(%rsi)
.L76:
	movq	112(%rsi), %rax
	leaq	128(%rdi), %rdx
	movq	%rax, 112(%rdi)
	movq	136(%rsi), %rax
	testq	%rax, %rax
	je	.L77
	movl	128(%rsi), %ecx
	movq	%rax, 136(%rdi)
	movl	%ecx, 128(%rdi)
	movq	144(%rsi), %rcx
	movq	%rcx, 144(%rdi)
	movq	152(%rsi), %rcx
	movq	%rcx, 152(%rdi)
	movq	%rdx, 8(%rax)
	movq	160(%rsi), %rax
	movq	$0, 136(%rsi)
	movq	%rax, 160(%rdi)
	leaq	128(%rsi), %rax
	movq	%rax, 144(%rsi)
	movq	%rax, 152(%rsi)
	movq	$0, 160(%rsi)
.L78:
	movq	168(%rsi), %rax
	leaq	184(%rdi), %rdx
	movq	%rax, 168(%rdi)
	movq	192(%rsi), %rax
	testq	%rax, %rax
	je	.L79
	movl	184(%rsi), %ecx
	movq	%rax, 192(%rdi)
	movl	%ecx, 184(%rdi)
	movq	200(%rsi), %rcx
	movq	%rcx, 200(%rdi)
	movq	208(%rsi), %rcx
	movq	%rcx, 208(%rdi)
	movq	%rdx, 8(%rax)
	movq	216(%rsi), %rax
	movq	$0, 192(%rsi)
	movq	%rax, 216(%rdi)
	leaq	184(%rsi), %rax
	movq	%rax, 200(%rsi)
	movq	%rax, 208(%rsi)
	movq	$0, 216(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	movl	$0, 16(%rdi)
	movq	$0, 24(%rdi)
	movq	%rdx, 32(%rdi)
	movq	%rdx, 40(%rdi)
	movq	$0, 48(%rdi)
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L79:
	movl	$0, 184(%rdi)
	movq	$0, 192(%rdi)
	movq	%rdx, 200(%rdi)
	movq	%rdx, 208(%rdi)
	movq	$0, 216(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L77:
	movl	$0, 128(%rdi)
	movq	$0, 136(%rdi)
	movq	%rdx, 144(%rdi)
	movq	%rdx, 152(%rdi)
	movq	$0, 160(%rdi)
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L75:
	movl	$0, 72(%rdi)
	movq	$0, 80(%rdi)
	movq	%rdx, 88(%rdi)
	movq	%rdx, 96(%rdi)
	movq	$0, 104(%rdi)
	jmp	.L76
	.cfi_endproc
.LFE18785:
	.size	_ZN2v88internal8compiler5HintsC2EOS2_, .-_ZN2v88internal8compiler5HintsC2EOS2_
	.weak	_ZN2v88internal8compiler5HintsC1EOS2_
	.set	_ZN2v88internal8compiler5HintsC1EOS2_,_ZN2v88internal8compiler5HintsC2EOS2_
	.section	.text._ZNK2v88internal8compiler5Hints9constantsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler5Hints9constantsEv
	.type	_ZNK2v88internal8compiler5Hints9constantsEv, @function
_ZNK2v88internal8compiler5Hints9constantsEv:
.LFB18787:
	.cfi_startproc
	endbr64
	leaq	56(%rdi), %rax
	ret
	.cfi_endproc
.LFE18787:
	.size	_ZNK2v88internal8compiler5Hints9constantsEv, .-_ZNK2v88internal8compiler5Hints9constantsEv
	.section	.text._ZNK2v88internal8compiler5Hints4mapsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler5Hints4mapsEv
	.type	_ZNK2v88internal8compiler5Hints4mapsEv, @function
_ZNK2v88internal8compiler5Hints4mapsEv:
.LFB18788:
	.cfi_startproc
	endbr64
	leaq	112(%rdi), %rax
	ret
	.cfi_endproc
.LFE18788:
	.size	_ZNK2v88internal8compiler5Hints4mapsEv, .-_ZNK2v88internal8compiler5Hints4mapsEv
	.section	.text._ZNK2v88internal8compiler5Hints19function_blueprintsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler5Hints19function_blueprintsEv
	.type	_ZNK2v88internal8compiler5Hints19function_blueprintsEv, @function
_ZNK2v88internal8compiler5Hints19function_blueprintsEv:
.LFB18789:
	.cfi_startproc
	endbr64
	leaq	168(%rdi), %rax
	ret
	.cfi_endproc
.LFE18789:
	.size	_ZNK2v88internal8compiler5Hints19function_blueprintsEv, .-_ZNK2v88internal8compiler5Hints19function_blueprintsEv
	.section	.text._ZNK2v88internal8compiler5Hints16virtual_contextsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler5Hints16virtual_contextsEv
	.type	_ZNK2v88internal8compiler5Hints16virtual_contextsEv, @function
_ZNK2v88internal8compiler5Hints16virtual_contextsEv:
.LFB18790:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE18790:
	.size	_ZNK2v88internal8compiler5Hints16virtual_contextsEv, .-_ZNK2v88internal8compiler5Hints16virtual_contextsEv
	.section	.text._ZN2v88internal8compiler5Hints6AddMapENS0_6HandleINS0_3MapEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler5Hints6AddMapENS0_6HandleINS0_3MapEEE
	.type	_ZN2v88internal8compiler5Hints6AddMapENS0_6HandleINS0_3MapEEE, @function
_ZN2v88internal8compiler5Hints6AddMapENS0_6HandleINS0_3MapEEE:
.LFB18793:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	128(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	136(%rdi), %r12
	testq	%r12, %r12
	jne	.L87
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L104:
	movq	16(%r12), %rax
	movl	$1, %edx
	testq	%rax, %rax
	je	.L88
.L105:
	movq	%rax, %r12
.L87:
	movq	32(%r12), %rcx
	cmpq	%r13, %rcx
	ja	.L104
	movq	24(%r12), %rax
	xorl	%edx, %edx
	testq	%rax, %rax
	jne	.L105
.L88:
	testb	%dl, %dl
	jne	.L86
	cmpq	%r13, %rcx
	jnb	.L85
.L96:
	movl	$1, %r15d
	cmpq	%r12, %r14
	jne	.L106
.L93:
	movq	112(%rbx), %rdi
	movq	16(%rdi), %rsi
	movq	24(%rdi), %rax
	subq	%rsi, %rax
	cmpq	$39, %rax
	jbe	.L107
	leaq	40(%rsi), %rax
	movq	%rax, 16(%rdi)
.L95:
	movq	%r13, 32(%rsi)
	movq	%r14, %rcx
	movq	%r12, %rdx
	movl	%r15d, %edi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 160(%rbx)
.L85:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L103:
	.cfi_restore_state
	movq	%r14, %r12
.L86:
	cmpq	144(%rbx), %r12
	je	.L96
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	cmpq	%r13, 32(%rax)
	jnb	.L85
	movl	$1, %r15d
	cmpq	%r12, %r14
	je	.L93
.L106:
	xorl	%r15d, %r15d
	cmpq	32(%r12), %r13
	setb	%r15b
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L107:
	movl	$40, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rsi
	jmp	.L95
	.cfi_endproc
.LFE18793:
	.size	_ZN2v88internal8compiler5Hints6AddMapENS0_6HandleINS0_3MapEEE, .-_ZN2v88internal8compiler5Hints6AddMapENS0_6HandleINS0_3MapEEE
	.section	.text._ZNK2v88internal8compiler5Hints7IsEmptyEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler5Hints7IsEmptyEv
	.type	_ZNK2v88internal8compiler5Hints7IsEmptyEv, @function
_ZNK2v88internal8compiler5Hints7IsEmptyEv:
.LFB18796:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpq	$0, 104(%rdi)
	jne	.L108
	cmpq	$0, 160(%rdi)
	jne	.L108
	cmpq	$0, 216(%rdi)
	jne	.L108
	cmpq	$0, 48(%rdi)
	sete	%al
.L108:
	ret
	.cfi_endproc
.LFE18796:
	.size	_ZNK2v88internal8compiler5Hints7IsEmptyEv, .-_ZNK2v88internal8compiler5Hints7IsEmptyEv
	.section	.rodata._ZN2v88internal8compilerlsERSoRKNS1_14VirtualContextE.str1.1,"aMS",@progbits,1
.LC16:
	.string	"Distance "
.LC17:
	.string	" from "
	.section	.text._ZN2v88internal8compilerlsERSoRKNS1_14VirtualContextE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal8compilerlsERSoRKNS1_14VirtualContextE
	.type	_ZN2v88internal8compilerlsERSoRKNS1_14VirtualContextE, @function
_ZN2v88internal8compilerlsERSoRKNS1_14VirtualContextE:
.LFB18797:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$9, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	leaq	.LC16(%rip), %rsi
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	(%rbx), %esi
	movq	%r12, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$6, %edx
	leaq	.LC17(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%rbx), %rax
	movq	%r13, %rdi
	leaq	-48(%rbp), %rsi
	movq	(%rax), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internallsERSoRKNS0_5BriefE@PLT
	movq	%rax, %r13
	movq	(%rax), %rax
	movq	-24(%rax), %rax
	movq	240(%r13,%rax), %r14
	testq	%r14, %r14
	je	.L120
	cmpb	$0, 56(%r14)
	je	.L115
	movsbl	67(%r14), %esi
.L116:
	movq	%r13, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L121
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L115:
	.cfi_restore_state
	movq	%r14, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%r14), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L116
	movq	%r14, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L116
.L121:
	call	__stack_chk_fail@PLT
.L120:
	call	_ZSt16__throw_bad_castv@PLT
	.cfi_endproc
.LFE18797:
	.size	_ZN2v88internal8compilerlsERSoRKNS1_14VirtualContextE, .-_ZN2v88internal8compilerlsERSoRKNS1_14VirtualContextE
	.section	.rodata._ZN2v88internal8compilerlsERSoRKNS1_5HintsE.str1.1,"aMS",@progbits,1
.LC18:
	.string	"  constant "
.LC19:
	.string	"  map "
.LC20:
	.string	"  blueprint "
.LC21:
	.string	"  virtual context "
	.section	.text._ZN2v88internal8compilerlsERSoRKNS1_5HintsE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal8compilerlsERSoRKNS1_5HintsE
	.type	_ZN2v88internal8compilerlsERSoRKNS1_5HintsE, @function
_ZN2v88internal8compilerlsERSoRKNS1_5HintsE:
.LFB18799:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	88(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	72(%rsi), %rax
	movq	%rax, -88(%rbp)
	cmpq	%r13, %rax
	jne	.L123
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L175:
	movsbl	67(%rdi), %esi
.L128:
	movq	%r14, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movq	%r13, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r13
	cmpq	%rax, -88(%rbp)
	je	.L129
.L123:
	movq	32(%r13), %r14
	movl	$11, %edx
	leaq	.LC18(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	movq	(%r14), %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internallsERSoRKNS0_5BriefE@PLT
	movq	%rax, %r14
	movq	(%rax), %rax
	movq	-24(%rax), %rax
	movq	240(%r14,%rax), %rdi
	testq	%rdi, %rdi
	je	.L132
	cmpb	$0, 56(%rdi)
	jne	.L175
	movq	%rdi, -96(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-96(%rbp), %rdi
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rcx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rcx, %rax
	je	.L128
	call	*%rax
	movsbl	%al, %esi
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L129:
	movq	144(%rbx), %r13
	leaq	128(%rbx), %rax
	leaq	-72(%rbp), %r15
	movq	%rax, -88(%rbp)
	cmpq	%r13, %rax
	jne	.L124
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L176:
	movsbl	67(%rdi), %esi
.L134:
	movq	%r14, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movq	%r13, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r13
	cmpq	%rax, -88(%rbp)
	je	.L125
.L124:
	movq	32(%r13), %r14
	movl	$6, %edx
	leaq	.LC19(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	movq	(%r14), %rax
	movq	%rax, -72(%rbp)
	call	_ZN2v88internallsERSoRKNS0_5BriefE@PLT
	movq	%rax, %r14
	movq	(%rax), %rax
	movq	-24(%rax), %rax
	movq	240(%r14,%rax), %rdi
	testq	%rdi, %rdi
	je	.L132
	cmpb	$0, 56(%rdi)
	jne	.L176
	movq	%rdi, -96(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-96(%rbp), %rdi
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rcx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rcx, %rax
	je	.L134
	call	*%rax
	movsbl	%al, %esi
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L125:
	movq	200(%rbx), %r13
	leaq	184(%rbx), %r14
	cmpq	%r13, %r14
	jne	.L130
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L177:
	movsbl	67(%rdi), %esi
.L138:
	movq	%r15, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movq	%r13, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r13
	cmpq	%rax, %r14
	je	.L131
.L130:
	movl	$12, %edx
	leaq	.LC20(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rdi
	leaq	32(%r13), %rsi
	call	_ZN2v88internal8compilerlsERSoRKNS1_17FunctionBlueprintE
	movq	%rax, %r15
	movq	(%rax), %rax
	movq	-24(%rax), %rax
	movq	240(%r15,%rax), %rdi
	testq	%rdi, %rdi
	je	.L132
	cmpb	$0, 56(%rdi)
	jne	.L177
	movq	%rdi, -88(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-88(%rbp), %rdi
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rcx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rcx, %rax
	je	.L138
	call	*%rax
	movsbl	%al, %esi
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L131:
	movq	32(%rbx), %r13
	addq	$16, %rbx
	leaq	-64(%rbp), %r14
	cmpq	%r13, %rbx
	jne	.L135
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L178:
	movsbl	67(%rdi), %esi
.L140:
	movq	%r15, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movq	(%r12), %rax
	movq	-24(%rax), %rax
	movq	240(%r12,%rax), %rdi
	testq	%rdi, %rdi
	je	.L132
	cmpb	$0, 56(%rdi)
	je	.L141
	movsbl	67(%rdi), %esi
.L142:
	movq	%r12, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movq	%r13, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r13
	cmpq	%rax, %rbx
	je	.L136
.L135:
	movl	$18, %edx
	leaq	.LC21(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$9, %edx
	leaq	.LC16(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	32(%r13), %esi
	movq	%r12, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$6, %edx
	leaq	.LC17(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	40(%r13), %rax
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	(%rax), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internallsERSoRKNS0_5BriefE@PLT
	movq	%rax, %r15
	movq	(%rax), %rax
	movq	-24(%rax), %rax
	movq	240(%r15,%rax), %rdi
	testq	%rdi, %rdi
	je	.L132
	cmpb	$0, 56(%rdi)
	jne	.L178
	movq	%rdi, -88(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-88(%rbp), %rdi
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rcx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rcx, %rax
	je	.L140
	call	*%rax
	movsbl	%al, %esi
	jmp	.L140
	.p2align 4,,10
	.p2align 3
.L141:
	movq	%rdi, -88(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-88(%rbp), %rdi
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rcx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rcx, %rax
	je	.L142
	call	*%rax
	movsbl	%al, %esi
	jmp	.L142
	.p2align 4,,10
	.p2align 3
.L136:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L179
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L132:
	.cfi_restore_state
	call	_ZSt16__throw_bad_castv@PLT
.L179:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18799:
	.size	_ZN2v88internal8compilerlsERSoRKNS1_5HintsE, .-_ZN2v88internal8compilerlsERSoRKNS1_5HintsE
	.section	.rodata._ZN2v88internal8compilerlsERSoRKNS1_17FunctionBlueprintE.str1.1,"aMS",@progbits,1
.LC22:
	.string	"):"
	.section	.text._ZN2v88internal8compilerlsERSoRKNS1_17FunctionBlueprintE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal8compilerlsERSoRKNS1_17FunctionBlueprintE
	.type	_ZN2v88internal8compilerlsERSoRKNS1_17FunctionBlueprintE, @function
_ZN2v88internal8compilerlsERSoRKNS1_17FunctionBlueprintE:
.LFB18798:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	leaq	-56(%rbp), %rsi
	movq	(%rax), %rax
	movq	%rax, -56(%rbp)
	call	_ZN2v88internallsERSoRKNS0_5BriefE@PLT
	movq	%rax, %r13
	movq	(%rax), %rax
	movq	-24(%rax), %rax
	movq	240(%r13,%rax), %r14
	testq	%r14, %r14
	je	.L184
	cmpb	$0, 56(%r14)
	je	.L182
	movsbl	67(%r14), %esi
.L183:
	movq	%r13, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movq	8(%rbx), %rax
	leaq	-48(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internallsERSoRKNS0_5BriefE@PLT
	movq	%rax, %r13
	movq	(%rax), %rax
	movq	-24(%rax), %rax
	movq	240(%r13,%rax), %r14
	testq	%r14, %r14
	je	.L184
	cmpb	$0, 56(%r14)
	je	.L185
	movsbl	67(%r14), %esi
.L186:
	movq	%r13, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	cmpq	$0, 120(%rbx)
	leaq	16(%rbx), %rsi
	jne	.L187
	cmpq	$0, 176(%rbx)
	jne	.L187
	cmpq	$0, 232(%rbx)
	je	.L202
	.p2align 4,,10
	.p2align 3
.L187:
	movq	%r12, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_5HintsE
	movl	$2, %edx
	leaq	.LC22(%rip), %rsi
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	0(%r13), %rax
	movq	-24(%rax), %rax
	movq	240(%r13,%rax), %r14
	testq	%r14, %r14
	je	.L184
	cmpb	$0, 56(%r14)
	je	.L189
	movsbl	67(%r14), %esi
.L190:
	movq	%r13, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
.L188:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L203
	addq	$32, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L189:
	.cfi_restore_state
	movq	%r14, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%r14), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L190
	movq	%r14, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L182:
	movq	%r14, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%r14), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L183
	movq	%r14, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L185:
	movq	%r14, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%r14), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L186
	movq	%r14, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L202:
	cmpq	$0, 64(%rbx)
	je	.L188
	jmp	.L187
.L184:
	call	_ZSt16__throw_bad_castv@PLT
.L203:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18798:
	.size	_ZN2v88internal8compilerlsERSoRKNS1_17FunctionBlueprintE, .-_ZN2v88internal8compilerlsERSoRKNS1_17FunctionBlueprintE
	.section	.rodata._ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment17accumulator_hintsEv.str1.8,"aMS",@progbits,1
	.align 8
.LC23:
	.string	"accumulator_index() < ephemeral_hints_.size()"
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment17accumulator_hintsEv,"axG",@progbits,_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment17accumulator_hintsEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment17accumulator_hintsEv
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment17accumulator_hintsEv, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment17accumulator_hintsEv:
.LFB18810:
	.cfi_startproc
	endbr64
	movl	248(%rdi), %edx
	addl	252(%rdi), %edx
	movq	936(%rdi), %rcx
	movslq	%edx, %rsi
	js	.L205
	movq	944(%rdi), %rdx
	movabsq	$7905747460161236407, %rax
	subq	%rcx, %rdx
	sarq	$5, %rdx
	imulq	%rax, %rdx
	cmpq	%rdx, %rsi
	jnb	.L209
.L205:
	leaq	0(,%rsi,8), %rax
	subq	%rsi, %rax
	salq	$5, %rax
	addq	%rcx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L209:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18810:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment17accumulator_hintsEv, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment17accumulator_hintsEv
	.section	.rodata._ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment14register_hintsENS0_11interpreter8RegisterE.str1.8,"aMS",@progbits,1
	.align 8
.LC24:
	.string	"local_index < ephemeral_hints_.size()"
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment14register_hintsENS0_11interpreter8RegisterE,"axG",@progbits,_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment14register_hintsENS0_11interpreter8RegisterE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment14register_hintsENS0_11interpreter8RegisterE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment14register_hintsENS0_11interpreter8RegisterE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment14register_hintsENS0_11interpreter8RegisterE:
.LFB18811:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	leaq	-36(%rbp), %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$32, %rsp
	movl	%esi, -36(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter8Register19is_function_closureEv@PLT
	testb	%al, %al
	je	.L211
	leaq	256(%rbx), %rax
.L210:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L219
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L211:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal11interpreter8Register18is_current_contextEv@PLT
	testb	%al, %al
	jne	.L220
	movl	-36(%rbp), %edx
	movl	248(%rbx), %esi
	movl	%edx, -28(%rbp)
	leal	(%rdx,%rsi), %eax
	testl	%edx, %edx
	js	.L221
.L215:
	movq	936(%rbx), %rdi
	movslq	%eax, %rcx
	testl	%eax, %eax
	js	.L216
	movq	944(%rbx), %rdx
	movabsq	$7905747460161236407, %rax
	subq	%rdi, %rdx
	sarq	$5, %rdx
	imulq	%rax, %rdx
	cmpq	%rcx, %rdx
	ja	.L216
	leaq	.LC24(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L216:
	leaq	0(,%rcx,8), %rbx
	subq	%rcx, %rbx
	salq	$5, %rbx
	leaq	(%rdi,%rbx), %rax
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L220:
	leaq	480(%rbx), %rax
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L221:
	leaq	-28(%rbp), %rdi
	call	_ZNK2v88internal11interpreter8Register16ToParameterIndexEi@PLT
	jmp	.L215
.L219:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18811:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment14register_hintsENS0_11interpreter8RegisterE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment14register_hintsENS0_11interpreter8RegisterE
	.section	.rodata._ZN2v88internal8compilerlsERSoRKNS1_34SerializerForBackgroundCompilation11EnvironmentE.str1.1,"aMS",@progbits,1
.LC25:
	.string	"Function "
.LC26:
	.string	"dead\n"
.LC27:
	.string	"alive\n"
.LC28:
	.string	"Hints for a"
.LC29:
	.string	":\n"
.LC30:
	.string	"Hints for r"
.LC31:
	.string	"Hints for <accumulator>:\n"
.LC32:
	.string	"unreachable code"
.LC33:
	.string	"Hints for <closure>:\n"
.LC34:
	.string	"Hints for <context>:\n"
.LC35:
	.string	"Hints for {return value}:\n"
	.section	.text._ZN2v88internal8compilerlsERSoRKNS1_34SerializerForBackgroundCompilation11EnvironmentE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal8compilerlsERSoRKNS1_34SerializerForBackgroundCompilation11EnvironmentE
	.type	_ZN2v88internal8compilerlsERSoRKNS1_34SerializerForBackgroundCompilation11EnvironmentE, @function
_ZN2v88internal8compilerlsERSoRKNS1_34SerializerForBackgroundCompilation11EnvironmentE:
.LFB18860:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-432(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-320(%rbp), %rbx
	subq	$488, %rsp
	movq	%rdi, -504(%rbp)
	movq	.LC36(%rip), %xmm1
	movq	%rbx, %rdi
	movhps	.LC37(%rip), %xmm1
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm1, -496(%rbp)
	movq	%rbx, -520(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	$0, -104(%rbp)
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movw	%ax, -96(%rbp)
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movups	%xmm0, -88(%rbp)
	movq	%rax, -432(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	-24(%rax), %rax
	movq	%rsi, -432(%rbp,%rax)
	movq	-432(%rbp), %rax
	xorl	%esi, %esi
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movdqa	-496(%rbp), %xmm1
	movq	%rax, -320(%rbp)
	leaq	-368(%rbp), %rax
	movq	%rax, %rdi
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -528(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rbx, %rdi
	leaq	-424(%rbp), %rsi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -512(%rbp)
	movq	%rax, -352(%rbp)
	movl	$16, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movl	$9, %edx
	leaq	.LC25(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r13), %rax
	movq	(%rax), %rbx
	movq	15(%rbx), %rax
	testb	$1, %al
	jne	.L256
.L223:
	cmpq	%rax, _ZN2v88internal18SharedFunctionInfo21kNoSharedNameSentinelE(%rip)
	leaq	-472(%rbp), %rdi
	setne	%al
.L224:
	testb	%al, %al
	je	.L228
	movq	15(%rbx), %r15
	testb	$1, %r15b
	jne	.L257
.L226:
	movq	%r12, %rsi
	movq	%r15, -472(%rbp)
	call	_ZNK2v88internal6Object10ShortPrintERSo@PLT
	movq	944(%r13), %rax
	cmpq	%rax, 936(%r13)
	je	.L258
	movl	$6, %edx
	movq	%r12, %rdi
	xorl	%r15d, %r15d
	movabsq	$7905747460161236407, %rbx
	leaq	.LC27(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	944(%r13), %rdx
	movq	936(%r13), %rax
	movq	%rdx, %rsi
	subq	%rax, %rsi
	sarq	$5, %rsi
	imulq	%rbx, %rsi
	testl	%esi, %esi
	jg	.L231
	jmp	.L230
	.p2align 4,,10
	.p2align 3
.L234:
	movl	252(%r13), %edx
	addl	%eax, %edx
	cmpl	%r15d, %edx
	jg	.L259
	cmpl	%r15d, %edx
	jne	.L237
	movl	$25, %edx
	leaq	.LC31(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L235:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_5HintsE
	movq	936(%r13), %rax
	movq	944(%r13), %rdx
.L233:
	movq	%rdx, %rcx
	addq	$1, %r15
	subq	%rax, %rcx
	sarq	$5, %rcx
	imulq	%rbx, %rcx
	cmpl	%r15d, %ecx
	jle	.L230
.L231:
	leaq	0(,%r15,8), %r14
	movl	%r15d, %r8d
	subq	%r15, %r14
	salq	$5, %r14
	addq	%rax, %r14
	cmpq	$0, 104(%r14)
	jne	.L232
	cmpq	$0, 160(%r14)
	jne	.L232
	cmpq	$0, 216(%r14)
	jne	.L232
	cmpq	$0, 48(%r14)
	je	.L233
	.p2align 4,,10
	.p2align 3
.L232:
	movl	248(%r13), %eax
	cmpl	%r15d, %eax
	jle	.L234
	movl	%r8d, -496(%rbp)
	movl	$11, %edx
	leaq	.LC28(%rip), %rsi
.L255:
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	-496(%rbp), %r8d
	movq	%r12, %rdi
	movl	%r8d, %esi
	call	_ZNSolsEi@PLT
	movl	$2, %edx
	leaq	.LC29(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L259:
	subl	%eax, %r8d
	movl	$11, %edx
	leaq	.LC30(%rip), %rsi
	movl	%r8d, -496(%rbp)
	jmp	.L255
	.p2align 4,,10
	.p2align 3
.L258:
	movl	$5, %edx
	leaq	.LC26(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L230:
	cmpq	$0, 360(%r13)
	leaq	256(%r13), %rbx
	jne	.L239
	cmpq	$0, 416(%r13)
	jne	.L239
	cmpq	$0, 472(%r13)
	je	.L260
	.p2align 4,,10
	.p2align 3
.L239:
	leaq	.LC33(%rip), %rsi
	movq	%r12, %rdi
	movl	$21, %edx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_5HintsE
.L240:
	cmpq	$0, 584(%r13)
	leaq	480(%r13), %rbx
	jne	.L241
	cmpq	$0, 640(%r13)
	jne	.L241
	cmpq	$0, 696(%r13)
	je	.L261
	.p2align 4,,10
	.p2align 3
.L241:
	leaq	.LC34(%rip), %rsi
	movq	%r12, %rdi
	movl	$21, %edx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_5HintsE
.L242:
	cmpq	$0, 808(%r13)
	leaq	704(%r13), %rbx
	je	.L262
.L243:
	leaq	.LC35(%rip), %rsi
	movq	%r12, %rdi
	movl	$26, %edx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_5HintsE
.L244:
	movq	-384(%rbp), %rax
	leaq	-448(%rbp), %rbx
	movq	$0, -456(%rbp)
	leaq	-464(%rbp), %rdi
	movq	%rbx, -464(%rbp)
	movb	$0, -448(%rbp)
	testq	%rax, %rax
	je	.L245
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L263
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L247:
	movq	-504(%rbp), %rdi
	movq	-456(%rbp), %rdx
	movq	-464(%rbp), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-464(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L248
	call	_ZdlPv@PLT
.L248:
	movq	.LC36(%rip), %xmm0
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -320(%rbp)
	movhps	.LC38(%rip), %xmm0
	movaps	%xmm0, -432(%rbp)
	cmpq	-512(%rbp), %rdi
	je	.L249
	call	_ZdlPv@PLT
.L249:
	movq	-528(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	-520(%rbp), %rdi
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rbx, -432(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L264
	movq	-504(%rbp), %rax
	addq	$488, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L262:
	.cfi_restore_state
	cmpq	$0, 864(%r13)
	jne	.L243
	cmpq	$0, 920(%r13)
	jne	.L243
	cmpq	$0, 752(%r13)
	je	.L244
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L263:
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L247
	.p2align 4,,10
	.p2align 3
.L257:
	movq	-1(%r15), %rax
	cmpw	$136, 11(%rax)
	jne	.L226
	movq	%rdi, -496(%rbp)
	movq	%r15, -472(%rbp)
	call	_ZNK2v88internal9ScopeInfo15HasFunctionNameEv@PLT
	movq	-496(%rbp), %rdi
	testb	%al, %al
	je	.L228
	movq	%r15, -472(%rbp)
	call	_ZNK2v88internal9ScopeInfo12FunctionNameEv@PLT
	movq	-496(%rbp), %rdi
	movq	%rax, %r15
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L228:
	andq	$-262144, %rbx
	movq	24(%rbx), %rax
	movq	-37464(%rax), %r15
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L256:
	movq	-1(%rax), %rdx
	cmpw	$136, 11(%rdx)
	jne	.L223
	leaq	-472(%rbp), %rdi
	movq	%rax, -472(%rbp)
	movq	%rdi, -496(%rbp)
	call	_ZNK2v88internal9ScopeInfo21HasSharedFunctionNameEv@PLT
	movq	-496(%rbp), %rdi
	jmp	.L224
.L260:
	cmpq	$0, 304(%r13)
	je	.L240
	jmp	.L239
.L261:
	cmpq	$0, 528(%r13)
	je	.L242
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L245:
	leaq	-352(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L247
.L237:
	leaq	.LC32(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L264:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18860:
	.size	_ZN2v88internal8compilerlsERSoRKNS1_34SerializerForBackgroundCompilation11EnvironmentE, .-_ZN2v88internal8compilerlsERSoRKNS1_34SerializerForBackgroundCompilation11EnvironmentE
	.section	.text._ZNK2v88internal8compiler34SerializerForBackgroundCompilation11Environment20RegisterToLocalIndexENS0_11interpreter8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler34SerializerForBackgroundCompilation11Environment20RegisterToLocalIndexENS0_11interpreter8RegisterE
	.type	_ZNK2v88internal8compiler34SerializerForBackgroundCompilation11Environment20RegisterToLocalIndexENS0_11interpreter8RegisterE, @function
_ZNK2v88internal8compiler34SerializerForBackgroundCompilation11Environment20RegisterToLocalIndexENS0_11interpreter8RegisterE:
.LFB18861:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movl	%esi, -4(%rbp)
	movl	248(%rdi), %esi
	leal	(%rdx,%rsi), %eax
	testl	%edx, %edx
	js	.L269
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L269:
	.cfi_restore_state
	leaq	-4(%rbp), %rdi
	call	_ZNK2v88internal11interpreter8Register16ToParameterIndexEi@PLT
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18861:
	.size	_ZNK2v88internal8compiler34SerializerForBackgroundCompilation11Environment20RegisterToLocalIndexENS0_11interpreter8RegisterE, .-_ZNK2v88internal8compiler34SerializerForBackgroundCompilation11Environment20RegisterToLocalIndexENS0_11interpreter8RegisterE
	.section	.rodata._ZN2v88internal8compiler34SerializerForBackgroundCompilation28ProcessReceiverMapForApiCallENS1_23FunctionTemplateInfoRefENS0_6HandleINS0_3MapEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC39:
	.string	"Serializing holder for target:"
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation28ProcessReceiverMapForApiCallENS1_23FunctionTemplateInfoRefENS0_6HandleINS0_3MapEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation28ProcessReceiverMapForApiCallENS1_23FunctionTemplateInfoRefENS0_6HandleINS0_3MapEEE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation28ProcessReceiverMapForApiCallENS1_23FunctionTemplateInfoRefENS0_6HandleINS0_3MapEEE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation28ProcessReceiverMapForApiCallENS1_23FunctionTemplateInfoRefENS0_6HandleINS0_3MapEEE:
.LFB18997:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$80, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, -112(%rbp)
	movq	%rdx, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rcx), %rax
	testb	$32, 13(%rax)
	je	.L278
.L270:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L279
	addq	$80, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L278:
	.cfi_restore_state
	movq	%rdi, %rbx
	leaq	-96(%rbp), %rdi
	leaq	-112(%rbp), %r13
	movq	%rcx, %rdx
	movq	(%rbx), %rsi
	movl	$1, %ecx
	leaq	-80(%rbp), %r12
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	(%rbx), %rdi
	cmpb	$0, 124(%rdi)
	je	.L272
	cmpb	$0, _ZN2v88internal30FLAG_trace_heap_broker_verboseE(%rip)
	je	.L272
	call	_ZN2v88internal8compiler12JSHeapBroker5TraceEv@PLT
	movl	$30, %edx
	leaq	.LC39(%rip), %rsi
	movq	%rax, %r14
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_9ObjectRefE@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -80(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	.p2align 4,,10
	.p2align 3
.L272:
	movq	-96(%rbp), %rdx
	movq	-88(%rbp), %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1, %r8d
	call	_ZN2v88internal8compiler23FunctionTemplateInfoRef26LookupHolderOfExpectedTypeENS1_6MapRefENS1_19SerializationPolicyE@PLT
	jmp	.L270
.L279:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18997:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation28ProcessReceiverMapForApiCallENS1_23FunctionTemplateInfoRefENS0_6HandleINS0_3MapEEE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation28ProcessReceiverMapForApiCallENS1_23FunctionTemplateInfoRefENS0_6HandleINS0_3MapEEE
	.section	.rodata._ZN2v88internal8compiler34SerializerForBackgroundCompilation14ProcessApiCallENS0_6HandleINS0_18SharedFunctionInfoEEERKNS0_10ZoneVectorINS1_5HintsEEE.str1.1,"aMS",@progbits,1
.LC40:
	.string	"arguments.size() >= 1"
.LC41:
	.string	"storage_.is_populated_"
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation14ProcessApiCallENS0_6HandleINS0_18SharedFunctionInfoEEERKNS0_10ZoneVectorINS1_5HintsEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation14ProcessApiCallENS0_6HandleINS0_18SharedFunctionInfoEEERKNS0_10ZoneVectorINS1_5HintsEEE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation14ProcessApiCallENS0_6HandleINS0_18SharedFunctionInfoEEERKNS0_10ZoneVectorINS1_5HintsEEE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation14ProcessApiCallENS0_6HandleINS0_18SharedFunctionInfoEEERKNS0_10ZoneVectorINS1_5HintsEEE:
.LFB18996:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movl	$17, %esi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	(%rax), %rdi
	addq	$41184, %rdi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	movq	(%rbx), %rsi
	movl	$1, %ecx
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler9ObjectRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	(%rbx), %rax
	movl	$18, %esi
	movq	(%rax), %rdi
	addq	$41184, %rdi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	movq	(%rbx), %rsi
	movl	$1, %ecx
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler9ObjectRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	(%rbx), %rax
	movl	$19, %esi
	movq	(%rax), %rdi
	addq	$41184, %rdi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	movq	(%rbx), %rsi
	movq	%r14, %rdi
	movl	$1, %ecx
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler9ObjectRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	(%rbx), %rax
	movq	(%rax), %r15
	movq	0(%r13), %rax
	movq	7(%rax), %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L281
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L282:
	leaq	-128(%rbp), %r15
	movq	(%rbx), %rsi
	movl	$1, %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler23FunctionTemplateInfoRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler23FunctionTemplateInfoRef13has_call_codeEv@PLT
	testb	%al, %al
	jne	.L318
.L280:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L319
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L318:
	.cfi_restore_state
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler23FunctionTemplateInfoRef17SerializeCallCodeEv@PLT
	movq	(%rbx), %rsi
	leaq	-112(%rbp), %rdi
	movq	%r13, %rdx
	movl	$1, %ecx
	movq	%rdi, -136(%rbp)
	call	_ZN2v88internal8compiler21SharedFunctionInfoRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal8compiler21SharedFunctionInfoRef29SerializeFunctionTemplateInfoEv@PLT
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler23FunctionTemplateInfoRef19accept_any_receiverEv@PLT
	testb	%al, %al
	jne	.L285
.L288:
	movq	8(%r12), %r13
	cmpq	16(%r12), %r13
	jne	.L286
	leaq	.LC40(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L281:
	movq	41088(%r15), %rdx
	cmpq	%rdx, 41096(%r15)
	je	.L320
.L283:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%rdx)
	jmp	.L282
	.p2align 4,,10
	.p2align 3
.L286:
	movq	88(%r13), %r15
	leaq	72(%r13), %r12
	cmpq	%r15, %r12
	je	.L289
	leaq	-96(%rbp), %rax
	movq	%rax, -136(%rbp)
	jmp	.L303
	.p2align 4,,10
	.p2align 3
.L296:
	movq	%r15, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r15
	cmpq	%rax, %r12
	je	.L289
.L303:
	movq	32(%r15), %rax
	movq	(%rax), %rax
	testb	$1, %al
	je	.L296
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	-37504(%rdx), %rax
	jne	.L321
	movq	(%rbx), %rax
	cmpb	$0, 24(%rax)
	je	.L322
	movdqu	32(%rax), %xmm0
	movq	-136(%rbp), %rdi
	movaps	%xmm0, -96(%rbp)
	call	_ZNK2v88internal8compiler16NativeContextRef19global_proxy_objectEv@PLT
	movq	%r14, %rdi
	movq	%rdx, -72(%rbp)
	movq	%rax, -80(%rbp)
	call	_ZNK2v88internal8compiler16JSGlobalProxyRef6objectEv@PLT
	movq	(%rbx), %rdx
	movq	(%rax), %rax
	movq	(%rdx), %rdx
	movq	-1(%rax), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L300
.L315:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L301:
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation28ProcessReceiverMapForApiCallENS1_23FunctionTemplateInfoRefENS0_6HandleINS0_3MapEEE
	movq	%r15, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r15
	cmpq	%rax, %r12
	jne	.L303
.L289:
	movq	144(%r13), %r12
	subq	$-128, %r13
	cmpq	%r12, %r13
	je	.L280
	.p2align 4,,10
	.p2align 3
.L305:
	movq	32(%r12), %rcx
	movq	-128(%rbp), %rsi
	movq	%rbx, %rdi
	movq	-120(%rbp), %rdx
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation28ProcessReceiverMapForApiCallENS1_23FunctionTemplateInfoRefENS0_6HandleINS0_3MapEEE
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rax, %r13
	jne	.L305
	jmp	.L280
	.p2align 4,,10
	.p2align 3
.L321:
	movq	-1(%rax), %rdx
	cmpw	$1023, 11(%rdx)
	jbe	.L296
	movq	(%rbx), %rdx
	movq	(%rdx), %rdx
	movq	-1(%rax), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	jne	.L315
.L300:
	movq	41088(%rdx), %rcx
	cmpq	41096(%rdx), %rcx
	je	.L323
.L302:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%rcx)
	jmp	.L301
	.p2align 4,,10
	.p2align 3
.L285:
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler23FunctionTemplateInfoRef22is_signature_undefinedEv@PLT
	testb	%al, %al
	je	.L288
	jmp	.L280
	.p2align 4,,10
	.p2align 3
.L323:
	movq	%rdx, %rdi
	movq	%rsi, -152(%rbp)
	movq	%rdx, -144(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-152(%rbp), %rsi
	movq	-144(%rbp), %rdx
	movq	%rax, %rcx
	jmp	.L302
	.p2align 4,,10
	.p2align 3
.L322:
	leaq	.LC41(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L320:
	movq	%r15, %rdi
	movq	%rsi, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L283
.L319:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18996:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation14ProcessApiCallENS0_6HandleINS0_18SharedFunctionInfoEEERKNS0_10ZoneVectorINS1_5HintsEEE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation14ProcessApiCallENS0_6HandleINS0_18SharedFunctionInfoEEERKNS0_10ZoneVectorINS1_5HintsEEE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation27ProcessHintsForObjectCreateERKNS1_5HintsE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation27ProcessHintsForObjectCreateERKNS1_5HintsE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation27ProcessHintsForObjectCreateERKNS1_5HintsE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation27ProcessHintsForObjectCreateERKNS1_5HintsE:
.LFB18998:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	72(%rsi), %r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	88(%rsi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	%r13, %r12
	je	.L324
	movq	%rdi, %r14
	leaq	-96(%rbp), %rbx
	leaq	-80(%rbp), %r15
	jmp	.L328
	.p2align 4,,10
	.p2align 3
.L326:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rax, %r13
	je	.L324
.L328:
	movq	32(%r12), %rdx
	movq	(%r14), %rsi
	movq	%rbx, %rdi
	movl	$1, %ecx
	call	_ZN2v88internal8compiler9ObjectRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef10IsJSObjectEv@PLT
	testb	%al, %al
	je	.L326
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef10AsJSObjectEv@PLT
	movq	%r15, %rdi
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler11JSObjectRef24SerializeObjectCreateMapEv@PLT
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rax, %r13
	jne	.L328
.L324:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L331
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L331:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18998:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation27ProcessHintsForObjectCreateERKNS1_5HintsE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation27ProcessHintsForObjectCreateERKNS1_5HintsE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation34ProcessHintsForHasInPrototypeChainERKNS1_5HintsE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation34ProcessHintsForHasInPrototypeChainERKNS1_5HintsE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation34ProcessHintsForHasInPrototypeChainERKNS1_5HintsE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation34ProcessHintsForHasInPrototypeChainERKNS1_5HintsE:
.LFB19001:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	72(%rax), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -104(%rbp)
	movq	88(%rax), %r12
	movq	%fs:40, %rsi
	movq	%rsi, -56(%rbp)
	xorl	%esi, %esi
	cmpq	%r12, %r14
	je	.L333
	leaq	-96(%rbp), %rbx
	leaq	-80(%rbp), %r15
	.p2align 4,,10
	.p2align 3
.L340:
	movq	32(%r12), %rax
	movq	(%rax), %rax
	testb	$1, %al
	je	.L334
	movq	0(%r13), %rdx
	movq	(%rdx), %rcx
	movq	-1(%rax), %rsi
	movq	41112(%rcx), %rdi
	testq	%rdi, %rdi
	je	.L335
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L336:
	movq	0(%r13), %rsi
	movl	$1, %ecx
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	jmp	.L339
	.p2align 4,,10
	.p2align 3
.L352:
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler6MapRef18SerializePrototypeEv@PLT
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler6MapRef9prototypeEv@PLT
	movq	%r15, %rdi
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZNK2v88internal8compiler13HeapObjectRef3mapEv@PLT
	movq	%rax, -96(%rbp)
	movq	%rdx, -88(%rbp)
.L339:
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler6MapRef13IsJSObjectMapEv@PLT
	testb	%al, %al
	jne	.L352
.L334:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rax, %r14
	jne	.L340
.L333:
	movq	-104(%rbp), %rax
	movq	144(%rax), %r12
	leaq	128(%rax), %r14
	cmpq	%r12, %r14
	je	.L332
	leaq	-96(%rbp), %rbx
	leaq	-80(%rbp), %r15
	.p2align 4,,10
	.p2align 3
.L344:
	movq	32(%r12), %rdx
	movq	0(%r13), %rsi
	movl	$1, %ecx
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L353:
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler6MapRef18SerializePrototypeEv@PLT
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler6MapRef9prototypeEv@PLT
	movq	%r15, %rdi
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZNK2v88internal8compiler13HeapObjectRef3mapEv@PLT
	movq	%rax, -96(%rbp)
	movq	%rdx, -88(%rbp)
.L343:
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler6MapRef13IsJSObjectMapEv@PLT
	testb	%al, %al
	jne	.L353
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rax, %r14
	jne	.L344
.L332:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L354
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L335:
	.cfi_restore_state
	movq	41088(%rcx), %rdx
	cmpq	%rdx, 41096(%rcx)
	je	.L355
.L337:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%rcx)
	movq	%rsi, (%rdx)
	jmp	.L336
	.p2align 4,,10
	.p2align 3
.L355:
	movq	%rcx, %rdi
	movq	%rsi, -120(%rbp)
	movq	%rcx, -112(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-120(%rbp), %rsi
	movq	-112(%rbp), %rcx
	movq	%rax, %rdx
	jmp	.L337
.L354:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19001:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation34ProcessHintsForHasInPrototypeChainERKNS1_5HintsE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation34ProcessHintsForHasInPrototypeChainERKNS1_5HintsE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation29ProcessHintsForPromiseResolveERKNS1_5HintsE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation29ProcessHintsForPromiseResolveERKNS1_5HintsE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation29ProcessHintsForPromiseResolveERKNS1_5HintsE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation29ProcessHintsForPromiseResolveERKNS1_5HintsE:
.LFB19003:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	72(%rsi), %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$216, %rsp
	movq	%rsi, -256(%rbp)
	movq	88(%rsi), %r12
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	cmpq	%r12, %r15
	je	.L357
	leaq	-224(%rbp), %rax
	movq	%rax, -248(%rbp)
	jmp	.L364
	.p2align 4,,10
	.p2align 3
.L359:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rax, %r15
	je	.L357
.L364:
	movq	32(%r12), %rax
	movq	(%rax), %rax
	testb	$1, %al
	je	.L359
	movq	-1(%rax), %rdx
	cmpw	$1023, 11(%rdx)
	jbe	.L359
	movq	(%rbx), %rdx
	movq	(%rdx), %r14
	movq	-1(%rax), %rsi
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L361
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
.L362:
	movq	(%rbx), %r11
	movq	8(%rbx), %rax
	movl	$1, %ecx
	leaq	-208(%rbp), %r14
	movq	-248(%rbp), %rdi
	movq	%rax, -240(%rbp)
	movq	(%r11), %rax
	movq	%r11, %rsi
	movq	%r11, -232(%rbp)
	leaq	3408(%rax), %rdx
	call	_ZN2v88internal8compiler7NameRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	(%rbx), %rsi
	movq	%r13, %rdx
	movl	$1, %ecx
	movq	%r14, %rdi
	leaq	-192(%rbp), %r13
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	-240(%rbp), %rax
	subq	$8, %rsp
	movq	-232(%rbp), %r11
	pushq	$1
	movq	-224(%rbp), %r8
	movq	%r13, %rdi
	pushq	%rax
	movq	-216(%rbp), %r9
	movq	%r11, %rsi
	movq	-208(%rbp), %rdx
	movq	-200(%rbp), %rcx
	pushq	$0
	call	_ZN2v88internal8compiler12JSHeapBroker21GetPropertyAccessInfoENS1_6MapRefENS1_7NameRefENS1_10AccessModeEPNS1_23CompilationDependenciesENS1_19SerializationPolicyE@PLT
	movq	%r12, %rdi
	addq	$32, %rsp
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rax, %r15
	jne	.L364
	.p2align 4,,10
	.p2align 3
.L357:
	movq	-256(%rbp), %rax
	movq	144(%rax), %r12
	subq	$-128, %rax
	movq	%rax, -248(%rbp)
	cmpq	%r12, %rax
	je	.L356
	leaq	-224(%rbp), %rax
	leaq	-208(%rbp), %r14
	movq	%rax, -256(%rbp)
	leaq	-192(%rbp), %r13
	.p2align 4,,10
	.p2align 3
.L366:
	movq	(%rbx), %r15
	movq	8(%rbx), %rax
	movl	$1, %ecx
	movq	32(%r12), %r8
	movq	-256(%rbp), %rdi
	movq	%rax, -232(%rbp)
	movq	(%r15), %rax
	movq	%r15, %rsi
	movq	%r8, -240(%rbp)
	leaq	3408(%rax), %rdx
	call	_ZN2v88internal8compiler7NameRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	(%rbx), %rsi
	movl	$1, %ecx
	movq	%r14, %rdi
	movq	-240(%rbp), %r8
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	-232(%rbp), %rax
	subq	$8, %rsp
	movq	-224(%rbp), %r8
	pushq	$1
	movq	-216(%rbp), %r9
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	-208(%rbp), %rdx
	movq	-200(%rbp), %rcx
	pushq	%rax
	pushq	$0
	call	_ZN2v88internal8compiler12JSHeapBroker21GetPropertyAccessInfoENS1_6MapRefENS1_7NameRefENS1_10AccessModeEPNS1_23CompilationDependenciesENS1_19SerializationPolicyE@PLT
	movq	%r12, %rdi
	addq	$32, %rsp
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rax, -248(%rbp)
	jne	.L366
.L356:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L371
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L361:
	.cfi_restore_state
	movq	41088(%r14), %r13
	cmpq	41096(%r14), %r13
	je	.L372
.L363:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, 0(%r13)
	jmp	.L362
	.p2align 4,,10
	.p2align 3
.L372:
	movq	%r14, %rdi
	movq	%rsi, -232(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-232(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L363
.L371:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19003:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation29ProcessHintsForPromiseResolveERKNS1_5HintsE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation29ProcessHintsForPromiseResolveERKNS1_5HintsE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation26ProcessMapHintsForPromisesERKNS1_5HintsE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation26ProcessMapHintsForPromisesERKNS1_5HintsE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation26ProcessMapHintsForPromisesERKNS1_5HintsE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation26ProcessMapHintsForPromisesERKNS1_5HintsE:
.LFB19014:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	72(%rsi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	88(%rsi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	%r12, %r14
	je	.L374
	leaq	-80(%rbp), %r15
	jmp	.L381
	.p2align 4,,10
	.p2align 3
.L376:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rax, %r14
	je	.L374
.L381:
	movq	32(%r12), %rax
	movq	(%rax), %rax
	testb	$1, %al
	je	.L376
	movq	-1(%rax), %rdx
	cmpw	$1074, 11(%rdx)
	jne	.L376
	movq	0(%r13), %rdx
	movq	(%rdx), %rcx
	movq	-1(%rax), %rsi
	movq	41112(%rcx), %rdi
	testq	%rdi, %rdi
	je	.L378
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L379:
	movq	0(%r13), %rsi
	movl	$1, %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler6MapRef18SerializePrototypeEv@PLT
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rax, %r14
	jne	.L381
	.p2align 4,,10
	.p2align 3
.L374:
	movq	144(%rbx), %r12
	subq	$-128, %rbx
	cmpq	%r12, %rbx
	je	.L373
	leaq	-80(%rbp), %r14
	.p2align 4,,10
	.p2align 3
.L385:
	movq	32(%r12), %rdx
	movq	(%rdx), %rax
	cmpw	$1074, 11(%rax)
	jne	.L383
	movq	0(%r13), %rsi
	movq	%r14, %rdi
	movl	$1, %ecx
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler6MapRef18SerializePrototypeEv@PLT
.L383:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rax, %rbx
	jne	.L385
.L373:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L389
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L378:
	.cfi_restore_state
	movq	41088(%rcx), %rdx
	cmpq	41096(%rcx), %rdx
	je	.L390
.L380:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%rcx)
	movq	%rsi, (%rdx)
	jmp	.L379
.L390:
	movq	%rcx, %rdi
	movq	%rsi, -96(%rbp)
	movq	%rcx, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %rsi
	movq	-88(%rbp), %rcx
	movq	%rax, %rdx
	jmp	.L380
.L389:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19014:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation26ProcessMapHintsForPromisesERKNS1_5HintsE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation26ProcessMapHintsForPromisesERKNS1_5HintsE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation23ProcessMapForRegExpTestENS1_6MapRefE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation23ProcessMapForRegExpTestENS1_6MapRefE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation23ProcessMapForRegExpTestENS1_6MapRefE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation23ProcessMapForRegExpTestENS1_6MapRefE:
.LFB19015:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	movl	$1, %ecx
	pushq	%r14
	leaq	-80(%rbp), %r10
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r10, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$72, %rsp
	movq	(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rsi), %rax
	movq	%r10, -104(%rbp)
	movq	%r13, %rsi
	movq	%rax, -112(%rbp)
	movq	0(%r13), %rax
	leaq	2512(%rax), %rdx
	call	_ZN2v88internal8compiler7NameRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	-112(%rbp), %rax
	subq	$8, %rsp
	movq	%r14, %rdx
	pushq	$1
	movq	-80(%rbp), %r8
	movq	%r15, %rcx
	movq	%r13, %rsi
	pushq	%rax
	movq	-72(%rbp), %r9
	movq	%r12, %rdi
	pushq	$0
	call	_ZN2v88internal8compiler12JSHeapBroker21GetPropertyAccessInfoENS1_6MapRefENS1_7NameRefENS1_10AccessModeEPNS1_23CompilationDependenciesENS1_19SerializationPolicyE@PLT
	addq	$32, %rsp
	cmpl	$3, (%r12)
	je	.L398
.L391:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L399
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L398:
	.cfi_restore_state
	movq	88(%r12), %rdx
	testq	%rdx, %rdx
	je	.L391
	movq	(%rbx), %rsi
	leaq	-96(%rbp), %r13
	movl	$1, %ecx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler11JSObjectRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	-104(%rbp), %r10
	movq	96(%r12), %rcx
	movl	$1, %r8d
	movzbl	104(%r12), %edx
	movq	%r13, %rsi
	movq	%r10, %rdi
	call	_ZNK2v88internal8compiler11JSObjectRef18GetOwnDataPropertyENS0_14RepresentationENS0_10FieldIndexENS1_19SerializationPolicyE@PLT
	jmp	.L391
.L399:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19015:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation23ProcessMapForRegExpTestENS1_6MapRefE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation23ProcessMapForRegExpTestENS1_6MapRefE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation25ProcessHintsForRegExpTestERKNS1_5HintsE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation25ProcessHintsForRegExpTestERKNS1_5HintsE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation25ProcessHintsForRegExpTestERKNS1_5HintsE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation25ProcessHintsForRegExpTestERKNS1_5HintsE:
.LFB19037:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	72(%rsi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$232, %rsp
	movq	88(%rsi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	%r12, %r14
	je	.L401
	leaq	-224(%rbp), %rax
	movq	%rax, -248(%rbp)
	jmp	.L407
	.p2align 4,,10
	.p2align 3
.L403:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rax, %r14
	je	.L401
.L407:
	movq	32(%r12), %r15
	movq	(%r15), %rax
	testb	$1, %al
	je	.L403
	movq	-1(%rax), %rdx
	cmpw	$1075, 11(%rdx)
	jne	.L403
	movq	0(%r13), %rdx
	movq	(%rdx), %rcx
	movq	-1(%rax), %rsi
	movq	41112(%rcx), %rdi
	testq	%rdi, %rdi
	je	.L418
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L408:
	movq	0(%r13), %rsi
	movq	-248(%rbp), %rdi
	movl	$1, %ecx
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	-224(%rbp), %rdx
	movq	-216(%rbp), %rcx
	movq	%r13, %rsi
	leaq	-192(%rbp), %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation23ProcessMapForRegExpTestENS1_6MapRefE
	cmpl	$3, -192(%rbp)
	jne	.L403
	cmpq	$0, -104(%rbp)
	jne	.L403
	leaq	-240(%rbp), %r9
	movq	0(%r13), %rsi
	movq	%r15, %rdx
	movl	$1, %ecx
	movq	%r9, %rdi
	movq	%r9, -256(%rbp)
	call	_ZN2v88internal8compiler11JSObjectRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	-256(%rbp), %r9
	movq	-96(%rbp), %rcx
	movl	$1, %r8d
	movzbl	-88(%rbp), %edx
	movq	-248(%rbp), %rdi
	movq	%r9, %rsi
	call	_ZNK2v88internal8compiler11JSObjectRef18GetOwnDataPropertyENS0_14RepresentationENS0_10FieldIndexENS1_19SerializationPolicyE@PLT
	jmp	.L403
	.p2align 4,,10
	.p2align 3
.L401:
	movq	144(%rbx), %r12
	subq	$-128, %rbx
	cmpq	%r12, %rbx
	je	.L400
	leaq	-224(%rbp), %r14
	.p2align 4,,10
	.p2align 3
.L413:
	movq	32(%r12), %rdx
	movq	(%rdx), %rax
	cmpw	$1075, 11(%rax)
	jne	.L411
	movq	0(%r13), %rsi
	movl	$1, %ecx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	-224(%rbp), %rdx
	movq	-216(%rbp), %rcx
	movq	%r13, %rsi
	leaq	-192(%rbp), %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation23ProcessMapForRegExpTestENS1_6MapRefE
.L411:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rax, %rbx
	jne	.L413
.L400:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L419
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L418:
	.cfi_restore_state
	movq	41088(%rcx), %rdx
	cmpq	%rdx, 41096(%rcx)
	je	.L420
.L409:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%rcx)
	movq	%rsi, (%rdx)
	jmp	.L408
.L420:
	movq	%rcx, %rdi
	movq	%rsi, -264(%rbp)
	movq	%rcx, -256(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-264(%rbp), %rsi
	movq	-256(%rbp), %rcx
	movq	%rax, %rdx
	jmp	.L409
.L419:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19037:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation25ProcessHintsForRegExpTestERKNS1_5HintsE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation25ProcessHintsForRegExpTestERKNS1_5HintsE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation27ProcessHintsForFunctionCallERKNS1_5HintsE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation27ProcessHintsForFunctionCallERKNS1_5HintsE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation27ProcessHintsForFunctionCallERKNS1_5HintsE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation27ProcessHintsForFunctionCallERKNS1_5HintsE:
.LFB19038:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	leaq	72(%rsi), %rbx
	subq	$32, %rsp
	movq	88(%rsi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpq	%r12, %rbx
	je	.L421
	movq	%rdi, %r13
	leaq	-64(%rbp), %r14
	jmp	.L426
	.p2align 4,,10
	.p2align 3
.L424:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rax, %rbx
	je	.L421
.L426:
	movq	32(%r12), %rdx
	movq	(%rdx), %rax
	testb	$1, %al
	je	.L424
	movq	-1(%rax), %rax
	cmpw	$1105, 11(%rax)
	jne	.L424
	movq	0(%r13), %rsi
	movl	$1, %ecx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13JSFunctionRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13JSFunctionRef9SerializeEv@PLT
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rax, %rbx
	jne	.L426
	.p2align 4,,10
	.p2align 3
.L421:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L430
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L430:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19038:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation27ProcessHintsForFunctionCallERKNS1_5HintsE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation27ProcessHintsForFunctionCallERKNS1_5HintsE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation27ProcessHintsForFunctionBindERKNS1_5HintsE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation27ProcessHintsForFunctionBindERKNS1_5HintsE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation27ProcessHintsForFunctionBindERKNS1_5HintsE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation27ProcessHintsForFunctionBindERKNS1_5HintsE:
.LFB19040:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	72(%rsi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$72, %rsp
	movq	88(%rsi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	%r12, %r14
	je	.L432
	leaq	-96(%rbp), %r15
	jmp	.L437
	.p2align 4,,10
	.p2align 3
.L434:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rax, %r14
	je	.L432
.L437:
	movq	32(%r12), %rdx
	movq	(%rdx), %rax
	testb	$1, %al
	je	.L434
	movq	-1(%rax), %rax
	cmpw	$1105, 11(%rax)
	jne	.L434
	movq	0(%r13), %rsi
	movl	$1, %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13JSFunctionRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13JSFunctionRef9SerializeEv@PLT
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler13HeapObjectRef3mapEv@PLT
	leaq	-80(%rbp), %rdi
	movq	%rdi, -104(%rbp)
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler6MapRef18SerializePrototypeEv@PLT
	movq	-104(%rbp), %rdi
	call	_ZNK2v88internal8compiler6MapRef22NumberOfOwnDescriptorsEv@PLT
	cmpl	$1, %eax
	jle	.L434
	movq	-104(%rbp), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler6MapRef22SerializeOwnDescriptorEi@PLT
	movq	-104(%rbp), %rdi
	movl	$1, %esi
	call	_ZN2v88internal8compiler6MapRef22SerializeOwnDescriptorEi@PLT
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rax, %r14
	jne	.L437
	.p2align 4,,10
	.p2align 3
.L432:
	movq	144(%rbx), %r12
	subq	$-128, %rbx
	cmpq	%r12, %rbx
	je	.L431
	leaq	-96(%rbp), %r14
	jmp	.L442
	.p2align 4,,10
	.p2align 3
.L440:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rax, %rbx
	je	.L431
.L442:
	movq	32(%r12), %rdx
	movq	(%rdx), %rax
	cmpw	$1105, 11(%rax)
	jne	.L440
	movq	0(%r13), %rsi
	movl	$1, %ecx
	movq	%r14, %rdi
	leaq	-80(%rbp), %r15
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movdqa	-96(%rbp), %xmm0
	movq	%r15, %rdi
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler6MapRef18SerializePrototypeEv@PLT
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler6MapRef22NumberOfOwnDescriptorsEv@PLT
	cmpl	$1, %eax
	jle	.L440
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler6MapRef22SerializeOwnDescriptorEi@PLT
	movl	$1, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler6MapRef22SerializeOwnDescriptorEi@PLT
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rax, %rbx
	jne	.L442
	.p2align 4,,10
	.p2align 3
.L431:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L447
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L447:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19040:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation27ProcessHintsForFunctionBindERKNS1_5HintsE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation27ProcessHintsForFunctionBindERKNS1_5HintsE
	.section	.rodata._ZN2v88internal8compiler34SerializerForBackgroundCompilation33ProcessHintsForObjectGetPrototypeERKNS1_5HintsE.str1.1,"aMS",@progbits,1
.LC42:
	.string	"IsHeapObject()"
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation33ProcessHintsForObjectGetPrototypeERKNS1_5HintsE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation33ProcessHintsForObjectGetPrototypeERKNS1_5HintsE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation33ProcessHintsForObjectGetPrototypeERKNS1_5HintsE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation33ProcessHintsForObjectGetPrototypeERKNS1_5HintsE:
.LFB19041:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	72(%rsi), %rbx
	subq	$72, %rsp
	movq	%rsi, -104(%rbp)
	movq	88(%rsi), %r12
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	cmpq	%rbx, %r12
	je	.L449
	leaq	-96(%rbp), %r14
	.p2align 4,,10
	.p2align 3
.L453:
	movq	32(%r12), %rdx
	testb	$1, (%rdx)
	je	.L451
	movq	0(%r13), %rsi
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L460
	movq	%r14, %rdi
	leaq	-80(%rbp), %r15
	call	_ZNK2v88internal8compiler13HeapObjectRef3mapEv@PLT
	movq	%r15, %rdi
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler6MapRef18SerializePrototypeEv@PLT
.L451:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rax, %rbx
	jne	.L453
.L449:
	movq	-104(%rbp), %rax
	movq	144(%rax), %r12
	leaq	128(%rax), %rbx
	cmpq	%r12, %rbx
	je	.L448
	leaq	-80(%rbp), %r15
	.p2align 4,,10
	.p2align 3
.L455:
	movq	32(%r12), %rdx
	movq	0(%r13), %rsi
	movl	$1, %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler6MapRef18SerializePrototypeEv@PLT
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rax, %rbx
	jne	.L455
.L448:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L461
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L460:
	.cfi_restore_state
	leaq	.LC42(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L461:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19041:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation33ProcessHintsForObjectGetPrototypeERKNS1_5HintsE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation33ProcessHintsForObjectGetPrototypeERKNS1_5HintsE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation20ProcessElementAccessENS1_5HintsES3_RKNS1_21ElementAccessFeedbackENS1_10AccessModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation20ProcessElementAccessENS1_5HintsES3_RKNS1_21ElementAccessFeedbackENS1_10AccessModeE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation20ProcessElementAccessENS1_5HintsES3_RKNS1_21ElementAccessFeedbackENS1_10AccessModeE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation20ProcessElementAccessENS1_5HintsES3_RKNS1_21ElementAccessFeedbackENS1_10AccessModeE:
.LFB19090:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	movq	%rcx, %rdi
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	subq	$168, %rsp
	movq	%rsi, -208(%rbp)
	movq	%rdx, -200(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal8compiler21ElementAccessFeedback17transition_groupsEv@PLT
	movq	8(%rax), %rsi
	movq	16(%rax), %rax
	movq	%rsi, -168(%rbp)
	movq	%rax, -176(%rbp)
	cmpq	%rax, %rsi
	je	.L463
	leaq	-80(%rbp), %r12
	.p2align 4,,10
	.p2align 3
.L469:
	movq	-168(%rbp), %rcx
	movq	8(%rcx), %rax
	movq	16(%rcx), %r13
	cmpq	%r13, %rax
	je	.L464
	movq	%rax, %r14
	jmp	.L468
	.p2align 4,,10
	.p2align 3
.L507:
	cmpl	$3, %ebx
	je	.L466
	testl	%ebx, %ebx
	je	.L466
.L467:
	addq	$8, %r14
	cmpq	%r14, %r13
	je	.L464
.L468:
	movq	(%r14), %rdx
	movq	(%r15), %rsi
	movl	$1, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	cmpl	$1, %ebx
	jne	.L507
	movq	%r12, %rdi
	addq	$8, %r14
	call	_ZN2v88internal8compiler6MapRef24SerializeForElementStoreEv@PLT
	cmpq	%r14, %r13
	jne	.L468
.L464:
	addq	$32, -168(%rbp)
	movq	-168(%rbp), %rax
	cmpq	-176(%rbp), %rax
	jne	.L469
.L463:
	movq	-208(%rbp), %rax
	movq	88(%rax), %r12
	addq	$72, %rax
	movq	%rax, -168(%rbp)
	cmpq	%r12, %rax
	je	.L470
	testl	%ebx, %ebx
	leaq	-160(%rbp), %r13
	sete	%dl
	cmpl	$3, %ebx
	sete	%al
	orl	%eax, %edx
	leaq	-112(%rbp), %rax
	movb	%dl, -176(%rbp)
	movq	%rax, -184(%rbp)
	jmp	.L482
	.p2align 4,,10
	.p2align 3
.L471:
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef14IsJSTypedArrayEv@PLT
	testb	%al, %al
	jne	.L508
.L472:
	cmpb	$0, -176(%rbp)
	jne	.L509
.L473:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rax, -168(%rbp)
	je	.L470
.L482:
	movq	32(%r12), %rdx
	movq	(%r15), %rsi
	movq	%r13, %rdi
	movl	$1, %ecx
	call	_ZN2v88internal8compiler9ObjectRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L471
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12AsHeapObjectEv@PLT
	movq	-184(%rbp), %rdi
	movq	%rax, -112(%rbp)
	movq	%rdx, -104(%rbp)
	call	_ZNK2v88internal8compiler13HeapObjectRef3mapEv@PLT
	leaq	-80(%rbp), %rdi
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler6MapRef16SerializeRootMapEv@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef14IsJSTypedArrayEv@PLT
	testb	%al, %al
	je	.L472
.L508:
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef14AsJSTypedArrayEv@PLT
	leaq	-80(%rbp), %rdi
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler15JSTypedArrayRef9SerializeEv@PLT
	cmpb	$0, -176(%rbp)
	je	.L473
.L509:
	movq	-200(%rbp), %rax
	movq	88(%rax), %r14
	addq	$72, %rax
	cmpq	%r14, %rax
	je	.L473
	movq	%r12, -192(%rbp)
	leaq	-144(%rbp), %rbx
	movq	%r14, %r12
	movq	%rax, %r14
	jmp	.L481
	.p2align 4,,10
	.p2align 3
.L476:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rax, %r14
	je	.L510
.L481:
	movq	32(%r12), %rdx
	movq	(%r15), %rsi
	movq	%rbx, %rdi
	movl	$1, %ecx
	call	_ZN2v88internal8compiler9ObjectRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef5IsSmiEv@PLT
	testb	%al, %al
	je	.L476
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef5AsSmiEv@PLT
	testl	%eax, %eax
	js	.L476
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef5AsSmiEv@PLT
	movq	-184(%rbp), %rdi
	movl	$1, %ecx
	movq	%r13, %rsi
	movl	%eax, %edx
	call	_ZNK2v88internal8compiler9ObjectRef21GetOwnConstantElementEjNS1_19SerializationPolicyE@PLT
	cmpb	$0, -112(%rbp)
	jne	.L476
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef9IsJSArrayEv@PLT
	testb	%al, %al
	je	.L476
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef9AsJSArrayEv@PLT
	movq	%rbx, %rdi
	movq	%rdx, -120(%rbp)
	movq	%rax, -128(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef5AsSmiEv@PLT
	leaq	-80(%rbp), %rdi
	leaq	-128(%rbp), %rsi
	movl	$1, %ecx
	movl	%eax, %edx
	call	_ZNK2v88internal8compiler10JSArrayRef16GetOwnCowElementEjNS1_19SerializationPolicyE@PLT
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rax, %r14
	jne	.L481
	.p2align 4,,10
	.p2align 3
.L510:
	movq	-192(%rbp), %r12
	jmp	.L473
	.p2align 4,,10
	.p2align 3
.L466:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler6MapRef23SerializeForElementLoadEv@PLT
	jmp	.L467
	.p2align 4,,10
	.p2align 3
.L470:
	movq	-208(%rbp), %rax
	movq	144(%rax), %r12
	leaq	128(%rax), %r13
	cmpq	%r12, %r13
	je	.L462
	leaq	-80(%rbp), %rbx
	.p2align 4,,10
	.p2align 3
.L484:
	movq	32(%r12), %rdx
	movq	(%r15), %rsi
	movl	$1, %ecx
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler6MapRef16SerializeRootMapEv@PLT
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rax, %r13
	jne	.L484
.L462:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L511
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L511:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19090:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation20ProcessElementAccessENS1_5HintsES3_RKNS1_21ElementAccessFeedbackENS1_10AccessModeE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation20ProcessElementAccessENS1_5HintsES3_RKNS1_21ElementAccessFeedbackENS1_10AccessModeE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation31VisitLdaNamedPropertyNoFeedbackEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation31VisitLdaNamedPropertyNoFeedbackEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation31VisitLdaNamedPropertyNoFeedbackEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation31VisitLdaNamedPropertyNoFeedbackEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB19092:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movl	$1, %esi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rbx), %rax
	movq	(%rax), %rdx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor26GetConstantForIndexOperandEiPNS0_7IsolateE@PLT
	movq	(%rbx), %rsi
	leaq	-48(%rbp), %rdi
	movl	$1, %ecx
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler7NameRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L515
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L515:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19092:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation31VisitLdaNamedPropertyNoFeedbackEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation31VisitLdaNamedPropertyNoFeedbackEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation31VisitStaNamedPropertyNoFeedbackEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation31VisitStaNamedPropertyNoFeedbackEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation31VisitStaNamedPropertyNoFeedbackEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation31VisitStaNamedPropertyNoFeedbackEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25607:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movl	$1, %esi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rbx), %rax
	movq	(%rax), %rdx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor26GetConstantForIndexOperandEiPNS0_7IsolateE@PLT
	movq	(%rbx), %rsi
	leaq	-48(%rbp), %rdi
	movl	$1, %ecx
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler7NameRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L519
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L519:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25607:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation31VisitStaNamedPropertyNoFeedbackEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation31VisitStaNamedPropertyNoFeedbackEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation37ProcessConstantForOrdinaryHasInstanceERKNS1_13HeapObjectRefEPb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation37ProcessConstantForOrdinaryHasInstanceERKNS1_13HeapObjectRefEPb
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation37ProcessConstantForOrdinaryHasInstanceERKNS1_13HeapObjectRefEPb, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation37ProcessConstantForOrdinaryHasInstanceERKNS1_13HeapObjectRefEPb:
.LFB19097:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal8compiler9ObjectRef17IsJSBoundFunctionEv@PLT
	movq	%r12, %rdi
	testb	%al, %al
	je	.L521
	call	_ZNK2v88internal8compiler9ObjectRef17AsJSBoundFunctionEv@PLT
	leaq	-64(%rbp), %r15
	movq	%r15, %rdi
	movq	%rax, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal8compiler18JSBoundFunctionRef9SerializeEv@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef17AsJSBoundFunctionEv@PLT
	leaq	-80(%rbp), %rdi
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZNK2v88internal8compiler18JSBoundFunctionRef21bound_target_functionEv@PLT
	movq	%r15, %rdi
	movq	%rax, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	jne	.L538
.L520:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L539
	addq	$64, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L521:
	.cfi_restore_state
	call	_ZNK2v88internal8compiler9ObjectRef12IsJSFunctionEv@PLT
	testb	%al, %al
	je	.L520
	movq	%r12, %rdi
	leaq	-64(%rbp), %r15
	call	_ZNK2v88internal8compiler9ObjectRef12AsJSFunctionEv@PLT
	movq	%r15, %rdi
	movq	%rax, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal8compiler13JSFunctionRef9SerializeEv@PLT
	movzbl	(%r14), %r13d
	testb	%r13b, %r13b
	je	.L540
.L524:
	movb	%r13b, (%r14)
	jmp	.L520
	.p2align 4,,10
	.p2align 3
.L538:
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation28ProcessConstantForInstanceOfERKNS1_9ObjectRefEPb.part.0
	jmp	.L520
	.p2align 4,,10
	.p2align 3
.L540:
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13HeapObjectRef3mapEv@PLT
	leaq	-96(%rbp), %rdi
	movq	%rax, -96(%rbp)
	movq	%rdx, -88(%rbp)
	call	_ZNK2v88internal8compiler6MapRef18has_prototype_slotEv@PLT
	testb	%al, %al
	je	.L524
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12AsJSFunctionEv@PLT
	leaq	-80(%rbp), %rdi
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZNK2v88internal8compiler13JSFunctionRef13has_prototypeEv@PLT
	testb	%al, %al
	je	.L524
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12AsJSFunctionEv@PLT
	movq	%r15, %rdi
	movq	%rax, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZNK2v88internal8compiler13JSFunctionRef30PrototypeRequiresRuntimeLookupEv@PLT
	xorl	$1, %eax
	movl	%eax, %r13d
	jmp	.L524
.L539:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19097:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation37ProcessConstantForOrdinaryHasInstanceERKNS1_13HeapObjectRefEPb, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation37ProcessConstantForOrdinaryHasInstanceERKNS1_13HeapObjectRefEPb
	.section	.rodata._ZN2v88internal8compiler34SerializerForBackgroundCompilation28ProcessConstantForInstanceOfERKNS1_9ObjectRefEPb.part.0.str1.1,"aMS",@progbits,1
.LC43:
	.string	"constant.has_value()"
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation28ProcessConstantForInstanceOfERKNS1_9ObjectRefEPb.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation28ProcessConstantForInstanceOfERKNS1_9ObjectRefEPb.part.0, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation28ProcessConstantForInstanceOfERKNS1_9ObjectRefEPb.part.0:
.LFB25312:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-224(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-304(%rbp), %rbx
	subq	$280, %rsp
	movq	%rdx, -320(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal8compiler9ObjectRef12AsHeapObjectEv@PLT
	movq	(%r12), %r14
	movq	8(%r12), %r8
	movq	%r15, %rdi
	movq	%rdx, -296(%rbp)
	movl	$1, %ecx
	movq	%rax, -304(%rbp)
	movq	(%r14), %rax
	movq	%r14, %rsi
	movq	%r8, -312(%rbp)
	leaq	3936(%rax), %rdx
	call	_ZN2v88internal8compiler7NameRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler13HeapObjectRef3mapEv@PLT
	movq	-312(%rbp), %r8
	subq	$8, %rsp
	movq	-216(%rbp), %r9
	pushq	$1
	movq	%rdx, %rcx
	leaq	-192(%rbp), %rdi
	movq	%rax, %rdx
	pushq	%r8
	movq	-224(%rbp), %r8
	movq	%r14, %rsi
	pushq	$0
	call	_ZN2v88internal8compiler12JSHeapBroker21GetPropertyAccessInfoENS1_6MapRefENS1_7NameRefENS1_10AccessModeEPNS1_23CompilationDependenciesENS1_19SerializationPolicyE@PLT
	movl	-192(%rbp), %eax
	addq	$32, %rsp
	cmpl	$1, %eax
	je	.L556
	cmpl	$3, %eax
	je	.L557
.L541:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L558
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L557:
	.cfi_restore_state
	movq	-104(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L559
	leaq	-288(%rbp), %r13
	movq	(%r12), %rsi
	movl	$1, %ecx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler11JSObjectRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
.L545:
	movq	-96(%rbp), %rcx
	movzbl	-88(%rbp), %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movl	$1, %r8d
	call	_ZNK2v88internal8compiler11JSObjectRef18GetOwnDataPropertyENS0_14RepresentationENS0_10FieldIndexENS1_19SerializationPolicyE@PLT
	cmpb	$0, -224(%rbp)
	je	.L560
	leaq	-216(%rbp), %r13
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsJSFunctionEv@PLT
	testb	%al, %al
	je	.L541
	movq	%r13, %rdi
	leaq	-272(%rbp), %r13
	call	_ZNK2v88internal8compiler9ObjectRef12AsJSFunctionEv@PLT
	movq	%r13, %rdi
	movq	%rax, -272(%rbp)
	movq	%rdx, -264(%rbp)
	call	_ZN2v88internal8compiler13JSFunctionRef9SerializeEv@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler13JSFunctionRef6sharedEv@PLT
	leaq	-240(%rbp), %rdi
	movq	%rax, -240(%rbp)
	movq	%rdx, -232(%rbp)
	call	_ZNK2v88internal8compiler21SharedFunctionInfoRef12HasBuiltinIdEv@PLT
	testb	%al, %al
	je	.L541
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler13JSFunctionRef6sharedEv@PLT
	leaq	-256(%rbp), %rdi
	movq	%rax, -256(%rbp)
	movq	%rdx, -248(%rbp)
	call	_ZNK2v88internal8compiler21SharedFunctionInfoRef10builtin_idEv@PLT
	cmpl	$348, %eax
	jne	.L541
	.p2align 4,,10
	.p2align 3
.L556:
	movq	-320(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation37ProcessConstantForOrdinaryHasInstanceERKNS1_13HeapObjectRefEPb
	jmp	.L541
	.p2align 4,,10
	.p2align 3
.L559:
	movq	%r13, %rdi
	leaq	-288(%rbp), %r13
	call	_ZNK2v88internal8compiler9ObjectRef10AsJSObjectEv@PLT
	movq	%rax, -288(%rbp)
	movq	%rdx, -280(%rbp)
	jmp	.L545
	.p2align 4,,10
	.p2align 3
.L560:
	leaq	.LC43(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L558:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25312:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation28ProcessConstantForInstanceOfERKNS1_9ObjectRefEPb.part.0, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation28ProcessConstantForInstanceOfERKNS1_9ObjectRefEPb.part.0
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation34ProcessHintsForOrdinaryHasInstanceERKNS1_5HintsES5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation34ProcessHintsForOrdinaryHasInstanceERKNS1_5HintsES5_
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation34ProcessHintsForOrdinaryHasInstanceERKNS1_5HintsES5_, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation34ProcessHintsForOrdinaryHasInstanceERKNS1_5HintsES5_:
.LFB19000:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	72(%rsi), %rbx
	subq	$56, %rsp
	movq	88(%rsi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movb	$0, -81(%rbp)
	cmpq	%rbx, %r12
	je	.L561
	movq	%rdi, %r13
	movq	%rdx, %r15
	leaq	-80(%rbp), %r14
	jmp	.L562
	.p2align 4,,10
	.p2align 3
.L564:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rax, %rbx
	je	.L571
.L562:
	movq	32(%r12), %rdx
	testb	$1, (%rdx)
	je	.L564
	movq	0(%r13), %rsi
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L572
	leaq	-81(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation37ProcessConstantForOrdinaryHasInstanceERKNS1_13HeapObjectRefEPb
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rax, %rbx
	jne	.L562
.L571:
	cmpb	$0, -81(%rbp)
	jne	.L573
.L561:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L574
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L573:
	.cfi_restore_state
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation34ProcessHintsForHasInPrototypeChainERKNS1_5HintsE
	jmp	.L561
	.p2align 4,,10
	.p2align 3
.L572:
	leaq	.LC42(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L574:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19000:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation34ProcessHintsForOrdinaryHasInstanceERKNS1_5HintsES5_, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation34ProcessHintsForOrdinaryHasInstanceERKNS1_5HintsES5_
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation28ProcessConstantForInstanceOfERKNS1_9ObjectRefEPb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation28ProcessConstantForInstanceOfERKNS1_9ObjectRefEPb
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation28ProcessConstantForInstanceOfERKNS1_9ObjectRefEPb, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation28ProcessConstantForInstanceOfERKNS1_9ObjectRefEPb:
.LFB19098:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	subq	$8, %rsp
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	jne	.L578
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L578:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler34SerializerForBackgroundCompilation28ProcessConstantForInstanceOfERKNS1_9ObjectRefEPb.part.0
	.cfi_endproc
.LFE19098:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation28ProcessConstantForInstanceOfERKNS1_9ObjectRefEPb, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation28ProcessConstantForInstanceOfERKNS1_9ObjectRefEPb
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation30VisitThrowReferenceErrorIfHoleEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation30VisitThrowReferenceErrorIfHoleEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation30VisitThrowReferenceErrorIfHoleEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation30VisitThrowReferenceErrorIfHoleEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB19102:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	xorl	%esi, %esi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rbx), %rax
	movq	(%rax), %rdx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor26GetConstantForIndexOperandEiPNS0_7IsolateE@PLT
	movq	(%rbx), %rsi
	leaq	-48(%rbp), %rdi
	movl	$1, %ecx
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler9ObjectRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L582
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L582:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19102:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation30VisitThrowReferenceErrorIfHoleEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation30VisitThrowReferenceErrorIfHoleEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation20VisitIncBlockCounterEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation20VisitIncBlockCounterEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation20VisitIncBlockCounterEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation20VisitIncBlockCounterEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB19157:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE19157:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation20VisitIncBlockCounterEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation20VisitIncBlockCounterEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation15VisitStackCheckEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation15VisitStackCheckEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation15VisitStackCheckEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation15VisitStackCheckEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25709:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE25709:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation15VisitStackCheckEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation15VisitStackCheckEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation37VisitThrowSuperAlreadyCalledIfNotHoleEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation37VisitThrowSuperAlreadyCalledIfNotHoleEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation37VisitThrowSuperAlreadyCalledIfNotHoleEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation37VisitThrowSuperAlreadyCalledIfNotHoleEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25711:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE25711:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation37VisitThrowSuperAlreadyCalledIfNotHoleEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation37VisitThrowSuperAlreadyCalledIfNotHoleEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation30VisitThrowSuperNotCalledIfHoleEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation30VisitThrowSuperNotCalledIfHoleEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation30VisitThrowSuperNotCalledIfHoleEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation30VisitThrowSuperNotCalledIfHoleEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25713:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE25713:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation30VisitThrowSuperNotCalledIfHoleEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation30VisitThrowSuperNotCalledIfHoleEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation14VisitExtraWideEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation14VisitExtraWideEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation14VisitExtraWideEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation14VisitExtraWideEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB19161:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC32(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19161:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation14VisitExtraWideEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation14VisitExtraWideEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation12VisitIllegalEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation12VisitIllegalEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation12VisitIllegalEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation12VisitIllegalEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25729:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC32(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE25729:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation12VisitIllegalEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation12VisitIllegalEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation9VisitWideEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation9VisitWideEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation9VisitWideEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation9VisitWideEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25731:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC32(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE25731:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation9VisitWideEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation9VisitWideEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZNSt8_Rb_treeIN2v88internal8compiler14VirtualContextES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE8_M_eraseEPSt13_Rb_tree_nodeIS3_E,"axG",@progbits,_ZNSt8_Rb_treeIN2v88internal8compiler14VirtualContextES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE8_M_eraseEPSt13_Rb_tree_nodeIS3_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN2v88internal8compiler14VirtualContextES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE8_M_eraseEPSt13_Rb_tree_nodeIS3_E
	.type	_ZNSt8_Rb_treeIN2v88internal8compiler14VirtualContextES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE8_M_eraseEPSt13_Rb_tree_nodeIS3_E, @function
_ZNSt8_Rb_treeIN2v88internal8compiler14VirtualContextES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE8_M_eraseEPSt13_Rb_tree_nodeIS3_E:
.LFB22715:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L601
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L595:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal8compiler14VirtualContextES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE8_M_eraseEPSt13_Rb_tree_nodeIS3_E
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L595
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L601:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE22715:
	.size	_ZNSt8_Rb_treeIN2v88internal8compiler14VirtualContextES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE8_M_eraseEPSt13_Rb_tree_nodeIS3_E, .-_ZNSt8_Rb_treeIN2v88internal8compiler14VirtualContextES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE8_M_eraseEPSt13_Rb_tree_nodeIS3_E
	.section	.text._ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E,"axG",@progbits,_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	.type	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E, @function
_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E:
.LFB22723:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L612
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L606:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L606
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L612:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE22723:
	.size	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E, .-_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	.section	.text._ZNSt8_Rb_treeIN2v88internal6HandleINS1_3MapEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E,"axG",@progbits,_ZNSt8_Rb_treeIN2v88internal6HandleINS1_3MapEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_3MapEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	.type	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_3MapEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E, @function
_ZNSt8_Rb_treeIN2v88internal6HandleINS1_3MapEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E:
.LFB22731:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L623
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L617:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_3MapEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L617
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L623:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE22731:
	.size	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_3MapEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E, .-_ZNSt8_Rb_treeIN2v88internal6HandleINS1_3MapEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	.section	.text._ZNSt8_Rb_treeIN2v88internal8compiler17FunctionBlueprintES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE8_M_eraseEPSt13_Rb_tree_nodeIS3_E,"axG",@progbits,_ZNSt8_Rb_treeIN2v88internal8compiler17FunctionBlueprintES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE8_M_eraseEPSt13_Rb_tree_nodeIS3_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN2v88internal8compiler17FunctionBlueprintES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE8_M_eraseEPSt13_Rb_tree_nodeIS3_E
	.type	_ZNSt8_Rb_treeIN2v88internal8compiler17FunctionBlueprintES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE8_M_eraseEPSt13_Rb_tree_nodeIS3_E, @function
_ZNSt8_Rb_treeIN2v88internal8compiler17FunctionBlueprintES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE8_M_eraseEPSt13_Rb_tree_nodeIS3_E:
.LFB22739:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L634
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L628:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal8compiler17FunctionBlueprintES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE8_M_eraseEPSt13_Rb_tree_nodeIS3_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	addq	$48, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	testq	%rbx, %rbx
	jne	.L628
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L634:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE22739:
	.size	_ZNSt8_Rb_treeIN2v88internal8compiler17FunctionBlueprintES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE8_M_eraseEPSt13_Rb_tree_nodeIS3_E, .-_ZNSt8_Rb_treeIN2v88internal8compiler17FunctionBlueprintES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE8_M_eraseEPSt13_Rb_tree_nodeIS3_E
	.section	.text._ZN2v88internal8compiler5HintsD2Ev,"axG",@progbits,_ZN2v88internal8compiler5HintsD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler5HintsD2Ev
	.type	_ZN2v88internal8compiler5HintsD2Ev, @function
_ZN2v88internal8compiler5HintsD2Ev:
.LFB18692:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	192(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L638
	leaq	168(%rdi), %r13
.L639:
	movq	24(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal8compiler17FunctionBlueprintES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE8_M_eraseEPSt13_Rb_tree_nodeIS3_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	addq	$48, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	testq	%rbx, %rbx
	jne	.L639
.L638:
	movq	136(%r12), %r13
	leaq	112(%r12), %r14
	testq	%r13, %r13
	je	.L640
.L643:
	movq	24(%r13), %rbx
	testq	%rbx, %rbx
	je	.L641
.L642:
	movq	24(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_3MapEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L642
.L641:
	movq	16(%r13), %r13
	testq	%r13, %r13
	jne	.L643
.L640:
	movq	80(%r12), %r13
	leaq	56(%r12), %r14
	testq	%r13, %r13
	je	.L644
.L647:
	movq	24(%r13), %rbx
	testq	%rbx, %rbx
	je	.L645
.L646:
	movq	24(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L646
.L645:
	movq	16(%r13), %r13
	testq	%r13, %r13
	jne	.L647
.L644:
	movq	24(%r12), %r13
	testq	%r13, %r13
	je	.L637
.L651:
	movq	24(%r13), %rbx
	testq	%rbx, %rbx
	je	.L649
.L650:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal8compiler14VirtualContextES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE8_M_eraseEPSt13_Rb_tree_nodeIS3_E
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L650
.L649:
	movq	16(%r13), %r13
	testq	%r13, %r13
	jne	.L651
.L637:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18692:
	.size	_ZN2v88internal8compiler5HintsD2Ev, .-_ZN2v88internal8compiler5HintsD2Ev
	.weak	_ZN2v88internal8compiler5HintsD1Ev
	.set	_ZN2v88internal8compiler5HintsD1Ev,_ZN2v88internal8compiler5HintsD2Ev
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation22BailoutOnUninitializedERKNS1_17ProcessedFeedbackE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation22BailoutOnUninitializedERKNS1_17ProcessedFeedbackE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation22BailoutOnUninitializedERKNS1_17ProcessedFeedbackE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation22BailoutOnUninitializedERKNS1_17ProcessedFeedbackE:
.LFB18889:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testb	$1, 96(%rdi)
	je	.L691
	cmpl	$-1, 100(%rdi)
	je	.L694
.L691:
	ret
	.p2align 4,,10
	.p2align 3
.L694:
	movl	(%rsi), %edx
	testl	%edx, %edx
	jne	.L691
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	24(%rdi), %r13
	movq	936(%r13), %r14
	movq	944(%r13), %r12
	cmpq	%r12, %r14
	je	.L681
	movq	%r14, %rbx
	.p2align 4,,10
	.p2align 3
.L683:
	movq	%rbx, %rdi
	addq	$224, %rbx
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	%rbx, %r12
	jne	.L683
	movq	%r14, 944(%r13)
	movl	$1, %eax
.L681:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18889:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation22BailoutOnUninitializedERKNS1_17ProcessedFeedbackE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation22BailoutOnUninitializedERKNS1_17ProcessedFeedbackE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation10VisitAbortEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation10VisitAbortEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation10VisitAbortEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation10VisitAbortEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB19164:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	24(%rdi), %r13
	movq	936(%r13), %r14
	movq	944(%r13), %r12
	cmpq	%r12, %r14
	je	.L695
	movq	%r14, %rbx
	.p2align 4,,10
	.p2align 3
.L697:
	movq	%rbx, %rdi
	addq	$224, %rbx
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	%rbx, %r12
	jne	.L697
	movq	%r14, 944(%r13)
.L695:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19164:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation10VisitAbortEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation10VisitAbortEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation12VisitReThrowEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation12VisitReThrowEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation12VisitReThrowEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation12VisitReThrowEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25715:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	24(%rdi), %r13
	movq	936(%r13), %r14
	movq	944(%r13), %r12
	cmpq	%r12, %r14
	je	.L700
	movq	%r14, %rbx
	.p2align 4,,10
	.p2align 3
.L702:
	movq	%rbx, %rdi
	addq	$224, %rbx
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	%rbx, %r12
	jne	.L702
	movq	%r14, 944(%r13)
.L700:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25715:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation12VisitReThrowEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation12VisitReThrowEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation10VisitThrowEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation10VisitThrowEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation10VisitThrowEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation10VisitThrowEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25717:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	24(%rdi), %r13
	movq	936(%r13), %r14
	movq	944(%r13), %r12
	cmpq	%r12, %r14
	je	.L705
	movq	%r14, %rbx
	.p2align 4,,10
	.p2align 3
.L707:
	movq	%rbx, %rdi
	addq	$224, %rbx
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	%rbx, %r12
	jne	.L707
	movq	%r14, 944(%r13)
.L705:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25717:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation10VisitThrowEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation10VisitThrowEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler5Hints5ClearEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler5Hints5ClearEv
	.type	_ZN2v88internal8compiler5Hints5ClearEv, @function
_ZN2v88internal8compiler5Hints5ClearEv:
.LFB18800:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	24(%rdi), %r12
	movq	%rdi, %rbx
	testq	%r12, %r12
	je	.L711
.L714:
	movq	24(%r12), %r13
	testq	%r13, %r13
	je	.L712
.L713:
	movq	24(%r13), %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal8compiler14VirtualContextES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE8_M_eraseEPSt13_Rb_tree_nodeIS3_E
	movq	16(%r13), %r13
	testq	%r13, %r13
	jne	.L713
.L712:
	movq	16(%r12), %r12
	testq	%r12, %r12
	jne	.L714
.L711:
	movq	80(%rbx), %r12
	leaq	16(%rbx), %rax
	leaq	56(%rbx), %r14
	movq	$0, 24(%rbx)
	movq	%rax, 32(%rbx)
	movq	%rax, 40(%rbx)
	movq	$0, 48(%rbx)
	testq	%r12, %r12
	je	.L715
.L718:
	movq	24(%r12), %r13
	testq	%r13, %r13
	je	.L716
.L717:
	movq	24(%r13), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	movq	16(%r13), %r13
	testq	%r13, %r13
	jne	.L717
.L716:
	movq	16(%r12), %r12
	testq	%r12, %r12
	jne	.L718
.L715:
	movq	136(%rbx), %r12
	leaq	72(%rbx), %rax
	leaq	112(%rbx), %r14
	movq	$0, 80(%rbx)
	movq	%rax, 88(%rbx)
	movq	%rax, 96(%rbx)
	movq	$0, 104(%rbx)
	testq	%r12, %r12
	je	.L719
.L722:
	movq	24(%r12), %r13
	testq	%r13, %r13
	je	.L720
.L721:
	movq	24(%r13), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_3MapEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	movq	16(%r13), %r13
	testq	%r13, %r13
	jne	.L721
.L720:
	movq	16(%r12), %r12
	testq	%r12, %r12
	jne	.L722
.L719:
	movq	192(%rbx), %r12
	leaq	128(%rbx), %rax
	movq	$0, 136(%rbx)
	leaq	168(%rbx), %r13
	movq	%rax, 144(%rbx)
	movq	%rax, 152(%rbx)
	movq	$0, 160(%rbx)
	testq	%r12, %r12
	je	.L723
.L724:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal8compiler17FunctionBlueprintES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE8_M_eraseEPSt13_Rb_tree_nodeIS3_E
	movq	%r12, %rdi
	movq	16(%r12), %r12
	addq	$48, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	testq	%r12, %r12
	jne	.L724
.L723:
	leaq	184(%rbx), %rax
	movq	$0, 192(%rbx)
	movq	%rax, 200(%rbx)
	movq	%rax, 208(%rbx)
	movq	$0, 216(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18800:
	.size	_ZN2v88internal8compiler5Hints5ClearEv, .-_ZN2v88internal8compiler5Hints5ClearEv
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation23VisitCallRuntimeForPairEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation23VisitCallRuntimeForPairEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation23VisitCallRuntimeForPairEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation23VisitCallRuntimeForPairEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB19106:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	24(%rdi), %rax
	movq	936(%rax), %rbx
	movq	944(%rax), %r12
	cmpq	%r12, %rbx
	je	.L754
	.p2align 4,,10
	.p2align 3
.L756:
	movq	%rbx, %rdi
	addq	$224, %rbx
	call	_ZN2v88internal8compiler5Hints5ClearEv
	cmpq	%rbx, %r12
	jne	.L756
.L754:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19106:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation23VisitCallRuntimeForPairEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation23VisitCallRuntimeForPairEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation24VisitCreateRegExpLiteralEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation24VisitCreateRegExpLiteralEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation24VisitCreateRegExpLiteralEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation24VisitCreateRegExpLiteralEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB18950:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	xorl	%esi, %esi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rbx), %rax
	movq	(%rax), %rdx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor26GetConstantForIndexOperandEiPNS0_7IsolateE@PLT
	movq	(%rbx), %rsi
	movl	$1, %ecx
	leaq	-48(%rbp), %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler9StringRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	24(%rbx), %rdx
	movl	252(%rdx), %eax
	addl	248(%rdx), %eax
	movq	936(%rdx), %rcx
	cltq
	js	.L761
	movq	944(%rdx), %rdx
	movabsq	$7905747460161236407, %rsi
	subq	%rcx, %rdx
	sarq	$5, %rdx
	imulq	%rsi, %rdx
	cmpq	%rax, %rdx
	jbe	.L764
.L761:
	leaq	0(,%rax,8), %rdi
	subq	%rax, %rdi
	salq	$5, %rdi
	addq	%rcx, %rdi
	call	_ZN2v88internal8compiler5Hints5ClearEv
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L765
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L764:
	.cfi_restore_state
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L765:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18950:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation24VisitCreateRegExpLiteralEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation24VisitCreateRegExpLiteralEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation23VisitCreateArrayLiteralEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation23VisitCreateArrayLiteralEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation23VisitCreateArrayLiteralEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation23VisitCreateArrayLiteralEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB18951:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	xorl	%esi, %esi
	leaq	-48(%rbp), %r12
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rbx), %rax
	movq	(%rax), %rdx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor26GetConstantForIndexOperandEiPNS0_7IsolateE@PLT
	movq	(%rbx), %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L771
	movq	24(%rbx), %rdx
	movl	252(%rdx), %eax
	addl	248(%rdx), %eax
	movq	936(%rdx), %rcx
	movslq	%eax, %rsi
	js	.L768
	movq	944(%rdx), %rax
	movabsq	$7905747460161236407, %rdx
	subq	%rcx, %rax
	sarq	$5, %rax
	imulq	%rdx, %rax
	cmpq	%rax, %rsi
	jnb	.L772
.L768:
	leaq	0(,%rsi,8), %rdi
	subq	%rsi, %rdi
	salq	$5, %rdi
	addq	%rcx, %rdi
	call	_ZN2v88internal8compiler5Hints5ClearEv
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L773
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L772:
	.cfi_restore_state
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L771:
	leaq	.LC42(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L773:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18951:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation23VisitCreateArrayLiteralEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation23VisitCreateArrayLiteralEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation24VisitCreateObjectLiteralEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation24VisitCreateObjectLiteralEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation24VisitCreateObjectLiteralEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation24VisitCreateObjectLiteralEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB18955:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	xorl	%esi, %esi
	leaq	-48(%rbp), %r12
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rbx), %rax
	movq	(%rax), %rdx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor26GetConstantForIndexOperandEiPNS0_7IsolateE@PLT
	movq	(%rbx), %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L779
	movq	24(%rbx), %rdx
	movl	252(%rdx), %eax
	addl	248(%rdx), %eax
	movq	936(%rdx), %rcx
	movslq	%eax, %rsi
	js	.L776
	movq	944(%rdx), %rax
	movabsq	$7905747460161236407, %rdx
	subq	%rcx, %rax
	sarq	$5, %rax
	imulq	%rdx, %rax
	cmpq	%rax, %rsi
	jnb	.L780
.L776:
	leaq	0(,%rsi,8), %rdi
	subq	%rsi, %rdi
	salq	$5, %rdi
	addq	%rcx, %rdi
	call	_ZN2v88internal8compiler5Hints5ClearEv
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L781
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L780:
	.cfi_restore_state
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L779:
	leaq	.LC42(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L781:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18955:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation24VisitCreateObjectLiteralEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation24VisitCreateObjectLiteralEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitLdaLookupSlotEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitLdaLookupSlotEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitLdaLookupSlotEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitLdaLookupSlotEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB19057:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	xorl	%esi, %esi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rbx), %rax
	movq	(%rax), %rdx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor26GetConstantForIndexOperandEiPNS0_7IsolateE@PLT
	movq	(%rbx), %rsi
	movl	$1, %ecx
	leaq	-48(%rbp), %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler9ObjectRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	24(%rbx), %rdx
	movl	252(%rdx), %eax
	addl	248(%rdx), %eax
	movq	936(%rdx), %rcx
	cltq
	js	.L784
	movq	944(%rdx), %rdx
	movabsq	$7905747460161236407, %rsi
	subq	%rcx, %rdx
	sarq	$5, %rdx
	imulq	%rsi, %rdx
	cmpq	%rax, %rdx
	jbe	.L787
.L784:
	leaq	0(,%rax,8), %rdi
	subq	%rax, %rdi
	salq	$5, %rdi
	addq	%rcx, %rdi
	call	_ZN2v88internal8compiler5Hints5ClearEv
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L788
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L787:
	.cfi_restore_state
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L788:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19057:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitLdaLookupSlotEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitLdaLookupSlotEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation16VisitCallRuntimeEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation16VisitCallRuntimeEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation16VisitCallRuntimeEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation16VisitCallRuntimeEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB19110:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rdx
	movl	252(%rdx), %eax
	addl	248(%rdx), %eax
	movq	936(%rdx), %rcx
	movslq	%eax, %rsi
	js	.L790
	movq	944(%rdx), %rax
	movabsq	$7905747460161236407, %rdx
	subq	%rcx, %rax
	sarq	$5, %rax
	imulq	%rdx, %rax
	cmpq	%rsi, %rax
	jbe	.L794
.L790:
	leaq	0(,%rsi,8), %rdi
	subq	%rsi, %rdi
	salq	$5, %rdi
	addq	%rcx, %rdi
	jmp	_ZN2v88internal8compiler5Hints5ClearEv
	.p2align 4,,10
	.p2align 3
.L794:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19110:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation16VisitCallRuntimeEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation16VisitCallRuntimeEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitTypeOfEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitTypeOfEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitTypeOfEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitTypeOfEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25701:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rdx
	movl	252(%rdx), %eax
	addl	248(%rdx), %eax
	movq	936(%rdx), %rcx
	movslq	%eax, %rsi
	js	.L796
	movq	944(%rdx), %rax
	movabsq	$7905747460161236407, %rdx
	subq	%rcx, %rax
	sarq	$5, %rax
	imulq	%rdx, %rax
	cmpq	%rsi, %rax
	jbe	.L800
.L796:
	leaq	0(,%rsi,8), %rdi
	subq	%rsi, %rdi
	salq	$5, %rdi
	addq	%rcx, %rdi
	jmp	_ZN2v88internal8compiler5Hints5ClearEv
	.p2align 4,,10
	.p2align 3
.L800:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE25701:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitTypeOfEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitTypeOfEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation13VisitDebuggerEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation13VisitDebuggerEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation13VisitDebuggerEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation13VisitDebuggerEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25703:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	24(%rdi), %rax
	movq	936(%rax), %rbx
	movq	944(%rax), %r12
	cmpq	%r12, %rbx
	je	.L801
	.p2align 4,,10
	.p2align 3
.L803:
	movq	%rbx, %rdi
	addq	$224, %rbx
	call	_ZN2v88internal8compiler5Hints5ClearEv
	cmpq	%rbx, %r12
	jne	.L803
.L801:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25703:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation13VisitDebuggerEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation13VisitDebuggerEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation20VisitResumeGeneratorEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation20VisitResumeGeneratorEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation20VisitResumeGeneratorEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation20VisitResumeGeneratorEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25705:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	24(%rdi), %rax
	movq	936(%rax), %rbx
	movq	944(%rax), %r12
	cmpq	%r12, %rbx
	je	.L806
	.p2align 4,,10
	.p2align 3
.L808:
	movq	%rbx, %rdi
	addq	$224, %rbx
	call	_ZN2v88internal8compiler5Hints5ClearEv
	cmpq	%rbx, %r12
	jne	.L808
.L806:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25705:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation20VisitResumeGeneratorEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation20VisitResumeGeneratorEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation21VisitSuspendGeneratorEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation21VisitSuspendGeneratorEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation21VisitSuspendGeneratorEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation21VisitSuspendGeneratorEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25707:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	24(%rdi), %rax
	movq	936(%rax), %rbx
	movq	944(%rax), %r12
	cmpq	%r12, %rbx
	je	.L811
	.p2align 4,,10
	.p2align 3
.L813:
	movq	%rbx, %rdi
	addq	$224, %rbx
	call	_ZN2v88internal8compiler5Hints5ClearEv
	cmpq	%rbx, %r12
	jne	.L813
.L811:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25707:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation21VisitSuspendGeneratorEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation21VisitSuspendGeneratorEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation16VisitCloneObjectEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation16VisitCloneObjectEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation16VisitCloneObjectEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation16VisitCloneObjectEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25657:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rdx
	movl	252(%rdx), %eax
	addl	248(%rdx), %eax
	movq	936(%rdx), %rcx
	movslq	%eax, %rsi
	js	.L817
	movq	944(%rdx), %rax
	movabsq	$7905747460161236407, %rdx
	subq	%rcx, %rax
	sarq	$5, %rax
	imulq	%rdx, %rax
	cmpq	%rsi, %rax
	jbe	.L821
.L817:
	leaq	0(,%rsi,8), %rdi
	subq	%rsi, %rdi
	salq	$5, %rdi
	addq	%rcx, %rdi
	jmp	_ZN2v88internal8compiler5Hints5ClearEv
	.p2align 4,,10
	.p2align 3
.L821:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE25657:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation16VisitCloneObjectEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation16VisitCloneObjectEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation28VisitCreateArrayFromIterableEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation28VisitCreateArrayFromIterableEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation28VisitCreateArrayFromIterableEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation28VisitCreateArrayFromIterableEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25659:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rdx
	movl	252(%rdx), %eax
	addl	248(%rdx), %eax
	movq	936(%rdx), %rcx
	movslq	%eax, %rsi
	js	.L823
	movq	944(%rdx), %rax
	movabsq	$7905747460161236407, %rdx
	subq	%rcx, %rax
	sarq	$5, %rax
	imulq	%rdx, %rax
	cmpq	%rsi, %rax
	jbe	.L827
.L823:
	leaq	0(,%rsi,8), %rdi
	subq	%rsi, %rdi
	salq	$5, %rdi
	addq	%rcx, %rdi
	jmp	_ZN2v88internal8compiler5Hints5ClearEv
	.p2align 4,,10
	.p2align 3
.L827:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE25659:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation28VisitCreateArrayFromIterableEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation28VisitCreateArrayFromIterableEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation28VisitCreateEmptyArrayLiteralEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation28VisitCreateEmptyArrayLiteralEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation28VisitCreateEmptyArrayLiteralEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation28VisitCreateEmptyArrayLiteralEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25661:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rdx
	movl	252(%rdx), %eax
	addl	248(%rdx), %eax
	movq	936(%rdx), %rcx
	movslq	%eax, %rsi
	js	.L829
	movq	944(%rdx), %rax
	movabsq	$7905747460161236407, %rdx
	subq	%rcx, %rax
	sarq	$5, %rax
	imulq	%rdx, %rax
	cmpq	%rsi, %rax
	jbe	.L833
.L829:
	leaq	0(,%rsi,8), %rdi
	subq	%rsi, %rdi
	salq	$5, %rdi
	addq	%rcx, %rdi
	jmp	_ZN2v88internal8compiler5Hints5ClearEv
	.p2align 4,,10
	.p2align 3
.L833:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE25661:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation28VisitCreateEmptyArrayLiteralEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation28VisitCreateEmptyArrayLiteralEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation29VisitCreateEmptyObjectLiteralEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation29VisitCreateEmptyObjectLiteralEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation29VisitCreateEmptyObjectLiteralEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation29VisitCreateEmptyObjectLiteralEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25663:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rdx
	movl	252(%rdx), %eax
	addl	248(%rdx), %eax
	movq	936(%rdx), %rcx
	movslq	%eax, %rsi
	js	.L835
	movq	944(%rdx), %rax
	movabsq	$7905747460161236407, %rdx
	subq	%rcx, %rax
	sarq	$5, %rax
	imulq	%rdx, %rax
	cmpq	%rsi, %rax
	jbe	.L839
.L835:
	leaq	0(,%rsi,8), %rdi
	subq	%rsi, %rdi
	salq	$5, %rdi
	addq	%rcx, %rdi
	jmp	_ZN2v88internal8compiler5Hints5ClearEv
	.p2align 4,,10
	.p2align 3
.L839:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE25663:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation29VisitCreateEmptyObjectLiteralEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation29VisitCreateEmptyObjectLiteralEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation24VisitCreateRestParameterEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation24VisitCreateRestParameterEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation24VisitCreateRestParameterEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation24VisitCreateRestParameterEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25667:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rdx
	movl	252(%rdx), %eax
	addl	248(%rdx), %eax
	movq	936(%rdx), %rcx
	movslq	%eax, %rsi
	js	.L841
	movq	944(%rdx), %rax
	movabsq	$7905747460161236407, %rdx
	subq	%rcx, %rax
	sarq	$5, %rax
	imulq	%rdx, %rax
	cmpq	%rsi, %rax
	jbe	.L845
.L841:
	leaq	0(,%rsi,8), %rdi
	subq	%rsi, %rdi
	salq	$5, %rdi
	addq	%rcx, %rdi
	jmp	_ZN2v88internal8compiler5Hints5ClearEv
	.p2align 4,,10
	.p2align 3
.L845:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE25667:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation24VisitCreateRestParameterEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation24VisitCreateRestParameterEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation26VisitCreateMappedArgumentsEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation26VisitCreateMappedArgumentsEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation26VisitCreateMappedArgumentsEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation26VisitCreateMappedArgumentsEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25665:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rdx
	movl	252(%rdx), %eax
	addl	248(%rdx), %eax
	movq	936(%rdx), %rcx
	movslq	%eax, %rsi
	js	.L847
	movq	944(%rdx), %rax
	movabsq	$7905747460161236407, %rdx
	subq	%rcx, %rax
	sarq	$5, %rax
	imulq	%rdx, %rax
	cmpq	%rsi, %rax
	jbe	.L851
.L847:
	leaq	0(,%rsi,8), %rdi
	subq	%rsi, %rdi
	salq	$5, %rdi
	addq	%rcx, %rdi
	jmp	_ZN2v88internal8compiler5Hints5ClearEv
	.p2align 4,,10
	.p2align 3
.L851:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE25665:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation26VisitCreateMappedArgumentsEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation26VisitCreateMappedArgumentsEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation28VisitCreateUnmappedArgumentsEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation28VisitCreateUnmappedArgumentsEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation28VisitCreateUnmappedArgumentsEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation28VisitCreateUnmappedArgumentsEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25669:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rdx
	movl	252(%rdx), %eax
	addl	248(%rdx), %eax
	movq	936(%rdx), %rcx
	movslq	%eax, %rsi
	js	.L853
	movq	944(%rdx), %rax
	movabsq	$7905747460161236407, %rdx
	subq	%rcx, %rax
	sarq	$5, %rax
	imulq	%rdx, %rax
	cmpq	%rsi, %rax
	jbe	.L857
.L853:
	leaq	0(,%rsi,8), %rdi
	subq	%rsi, %rdi
	salq	$5, %rdi
	addq	%rcx, %rdi
	jmp	_ZN2v88internal8compiler5Hints5ClearEv
	.p2align 4,,10
	.p2align 3
.L857:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE25669:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation28VisitCreateUnmappedArgumentsEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation28VisitCreateUnmappedArgumentsEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation25VisitDeletePropertySloppyEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation25VisitDeletePropertySloppyEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation25VisitDeletePropertySloppyEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation25VisitDeletePropertySloppyEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25671:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rdx
	movl	252(%rdx), %eax
	addl	248(%rdx), %eax
	movq	936(%rdx), %rcx
	movslq	%eax, %rsi
	js	.L859
	movq	944(%rdx), %rax
	movabsq	$7905747460161236407, %rdx
	subq	%rcx, %rax
	sarq	$5, %rax
	imulq	%rdx, %rax
	cmpq	%rsi, %rax
	jbe	.L863
.L859:
	leaq	0(,%rsi,8), %rdi
	subq	%rsi, %rdi
	salq	$5, %rdi
	addq	%rcx, %rdi
	jmp	_ZN2v88internal8compiler5Hints5ClearEv
	.p2align 4,,10
	.p2align 3
.L863:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE25671:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation25VisitDeletePropertySloppyEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation25VisitDeletePropertySloppyEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation23VisitTestReferenceEqualEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation23VisitTestReferenceEqualEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation23VisitTestReferenceEqualEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation23VisitTestReferenceEqualEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25687:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rdx
	movl	252(%rdx), %eax
	addl	248(%rdx), %eax
	movq	936(%rdx), %rcx
	movslq	%eax, %rsi
	js	.L865
	movq	944(%rdx), %rax
	movabsq	$7905747460161236407, %rdx
	subq	%rcx, %rax
	sarq	$5, %rax
	imulq	%rdx, %rax
	cmpq	%rsi, %rax
	jbe	.L869
.L865:
	leaq	0(,%rsi,8), %rdi
	subq	%rsi, %rdi
	salq	$5, %rdi
	addq	%rcx, %rdi
	jmp	_ZN2v88internal8compiler5Hints5ClearEv
	.p2align 4,,10
	.p2align 3
.L869:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE25687:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation23VisitTestReferenceEqualEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation23VisitTestReferenceEqualEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation14VisitForInStepEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation14VisitForInStepEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation14VisitForInStepEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation14VisitForInStepEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25679:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rdx
	movl	252(%rdx), %eax
	addl	248(%rdx), %eax
	movq	936(%rdx), %rcx
	movslq	%eax, %rsi
	js	.L871
	movq	944(%rdx), %rax
	movabsq	$7905747460161236407, %rdx
	subq	%rcx, %rax
	sarq	$5, %rax
	imulq	%rdx, %rax
	cmpq	%rsi, %rax
	jbe	.L875
.L871:
	leaq	0(,%rsi,8), %rdi
	subq	%rsi, %rdi
	salq	$5, %rdi
	addq	%rcx, %rdi
	jmp	_ZN2v88internal8compiler5Hints5ClearEv
	.p2align 4,,10
	.p2align 3
.L875:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE25679:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation14VisitForInStepEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation14VisitForInStepEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation24VisitToBooleanLogicalNotEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation24VisitToBooleanLogicalNotEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation24VisitToBooleanLogicalNotEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation24VisitToBooleanLogicalNotEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25695:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rdx
	movl	252(%rdx), %eax
	addl	248(%rdx), %eax
	movq	936(%rdx), %rcx
	movslq	%eax, %rsi
	js	.L877
	movq	944(%rdx), %rax
	movabsq	$7905747460161236407, %rdx
	subq	%rcx, %rax
	sarq	$5, %rax
	imulq	%rdx, %rax
	cmpq	%rsi, %rax
	jbe	.L881
.L877:
	leaq	0(,%rsi,8), %rdi
	subq	%rsi, %rdi
	salq	$5, %rdi
	addq	%rcx, %rdi
	jmp	_ZN2v88internal8compiler5Hints5ClearEv
	.p2align 4,,10
	.p2align 3
.L881:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE25695:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation24VisitToBooleanLogicalNotEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation24VisitToBooleanLogicalNotEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitForInContinueEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitForInContinueEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitForInContinueEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitForInContinueEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25675:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rdx
	movl	252(%rdx), %eax
	addl	248(%rdx), %eax
	movq	936(%rdx), %rcx
	movslq	%eax, %rsi
	js	.L883
	movq	944(%rdx), %rax
	movabsq	$7905747460161236407, %rdx
	subq	%rcx, %rax
	sarq	$5, %rax
	imulq	%rdx, %rax
	cmpq	%rsi, %rax
	jbe	.L887
.L883:
	leaq	0(,%rsi,8), %rdi
	subq	%rsi, %rdi
	salq	$5, %rdi
	addq	%rcx, %rdi
	jmp	_ZN2v88internal8compiler5Hints5ClearEv
	.p2align 4,,10
	.p2align 3
.L887:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE25675:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitForInContinueEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitForInContinueEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitTestUndefinedEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitTestUndefinedEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitTestUndefinedEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitTestUndefinedEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25691:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rdx
	movl	252(%rdx), %eax
	addl	248(%rdx), %eax
	movq	936(%rdx), %rcx
	movslq	%eax, %rsi
	js	.L889
	movq	944(%rdx), %rax
	movabsq	$7905747460161236407, %rdx
	subq	%rcx, %rax
	sarq	$5, %rax
	imulq	%rdx, %rax
	cmpq	%rsi, %rax
	jbe	.L893
.L889:
	leaq	0(,%rsi,8), %rdi
	subq	%rsi, %rdi
	salq	$5, %rdi
	addq	%rcx, %rdi
	jmp	_ZN2v88internal8compiler5Hints5ClearEv
	.p2align 4,,10
	.p2align 3
.L893:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE25691:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitTestUndefinedEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitTestUndefinedEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation22VisitSetPendingMessageEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation22VisitSetPendingMessageEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation22VisitSetPendingMessageEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation22VisitSetPendingMessageEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25683:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rdx
	movl	252(%rdx), %eax
	addl	248(%rdx), %eax
	movq	936(%rdx), %rcx
	movslq	%eax, %rsi
	js	.L895
	movq	944(%rdx), %rax
	movabsq	$7905747460161236407, %rdx
	subq	%rcx, %rax
	sarq	$5, %rax
	imulq	%rdx, %rax
	cmpq	%rsi, %rax
	jbe	.L899
.L895:
	leaq	0(,%rsi,8), %rdi
	subq	%rsi, %rdi
	salq	$5, %rdi
	addq	%rcx, %rdi
	jmp	_ZN2v88internal8compiler5Hints5ClearEv
	.p2align 4,,10
	.p2align 3
.L899:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE25683:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation22VisitSetPendingMessageEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation22VisitSetPendingMessageEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation13VisitToStringEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation13VisitToStringEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation13VisitToStringEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation13VisitToStringEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25699:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rdx
	movl	252(%rdx), %eax
	addl	248(%rdx), %eax
	movq	936(%rdx), %rcx
	movslq	%eax, %rsi
	js	.L901
	movq	944(%rdx), %rax
	movabsq	$7905747460161236407, %rdx
	subq	%rcx, %rax
	sarq	$5, %rax
	imulq	%rdx, %rax
	cmpq	%rsi, %rax
	jbe	.L905
.L901:
	leaq	0(,%rsi,8), %rdi
	subq	%rsi, %rdi
	salq	$5, %rdi
	addq	%rcx, %rdi
	jmp	_ZN2v88internal8compiler5Hints5ClearEv
	.p2align 4,,10
	.p2align 3
.L905:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE25699:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation13VisitToStringEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation13VisitToStringEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation25VisitDeletePropertyStrictEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation25VisitDeletePropertyStrictEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation25VisitDeletePropertyStrictEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation25VisitDeletePropertyStrictEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25673:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rdx
	movl	252(%rdx), %eax
	addl	248(%rdx), %eax
	movq	936(%rdx), %rcx
	movslq	%eax, %rsi
	js	.L907
	movq	944(%rdx), %rax
	movabsq	$7905747460161236407, %rdx
	subq	%rcx, %rax
	sarq	$5, %rax
	imulq	%rdx, %rax
	cmpq	%rsi, %rax
	jbe	.L911
.L907:
	leaq	0(,%rsi,8), %rdi
	subq	%rsi, %rdi
	salq	$5, %rdi
	addq	%rcx, %rdi
	jmp	_ZN2v88internal8compiler5Hints5ClearEv
	.p2align 4,,10
	.p2align 3
.L911:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE25673:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation25VisitDeletePropertyStrictEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation25VisitDeletePropertyStrictEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation15VisitTestTypeOfEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation15VisitTestTypeOfEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation15VisitTestTypeOfEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation15VisitTestTypeOfEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25689:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rdx
	movl	252(%rdx), %eax
	addl	248(%rdx), %eax
	movq	936(%rdx), %rcx
	movslq	%eax, %rsi
	js	.L913
	movq	944(%rdx), %rax
	movabsq	$7905747460161236407, %rdx
	subq	%rcx, %rax
	sarq	$5, %rax
	imulq	%rdx, %rax
	cmpq	%rsi, %rax
	jbe	.L917
.L913:
	leaq	0(,%rsi,8), %rdi
	subq	%rsi, %rdi
	salq	$5, %rdi
	addq	%rcx, %rdi
	jmp	_ZN2v88internal8compiler5Hints5ClearEv
	.p2align 4,,10
	.p2align 3
.L917:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE25689:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation15VisitTestTypeOfEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation15VisitTestTypeOfEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation15VisitLogicalNotEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation15VisitLogicalNotEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation15VisitLogicalNotEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation15VisitLogicalNotEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25681:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rdx
	movl	252(%rdx), %eax
	addl	248(%rdx), %eax
	movq	936(%rdx), %rcx
	movslq	%eax, %rsi
	js	.L919
	movq	944(%rdx), %rax
	movabsq	$7905747460161236407, %rdx
	subq	%rcx, %rax
	sarq	$5, %rax
	imulq	%rdx, %rax
	cmpq	%rsi, %rax
	jbe	.L923
.L919:
	leaq	0(,%rsi,8), %rdi
	subq	%rsi, %rdi
	salq	$5, %rdi
	addq	%rcx, %rdi
	jmp	_ZN2v88internal8compiler5Hints5ClearEv
	.p2align 4,,10
	.p2align 3
.L923:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE25681:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation15VisitLogicalNotEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation15VisitLogicalNotEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitToNameEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitToNameEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitToNameEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitToNameEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25697:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rdx
	movl	252(%rdx), %eax
	addl	248(%rdx), %eax
	movq	936(%rdx), %rcx
	movslq	%eax, %rsi
	js	.L925
	movq	944(%rdx), %rax
	movabsq	$7905747460161236407, %rdx
	subq	%rcx, %rax
	sarq	$5, %rax
	imulq	%rdx, %rax
	cmpq	%rsi, %rax
	jbe	.L929
.L925:
	leaq	0(,%rsi,8), %rdi
	subq	%rsi, %rdi
	salq	$5, %rdi
	addq	%rcx, %rdi
	jmp	_ZN2v88internal8compiler5Hints5ClearEv
	.p2align 4,,10
	.p2align 3
.L929:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE25697:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitToNameEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitToNameEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation19VisitForInEnumerateEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation19VisitForInEnumerateEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation19VisitForInEnumerateEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation19VisitForInEnumerateEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25677:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rdx
	movl	252(%rdx), %eax
	addl	248(%rdx), %eax
	movq	936(%rdx), %rcx
	movslq	%eax, %rsi
	js	.L931
	movq	944(%rdx), %rax
	movabsq	$7905747460161236407, %rdx
	subq	%rcx, %rax
	sarq	$5, %rax
	imulq	%rdx, %rax
	cmpq	%rsi, %rax
	jbe	.L935
.L931:
	leaq	0(,%rsi,8), %rdi
	subq	%rsi, %rdi
	salq	$5, %rdi
	addq	%rcx, %rdi
	jmp	_ZN2v88internal8compiler5Hints5ClearEv
	.p2align 4,,10
	.p2align 3
.L935:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE25677:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation19VisitForInEnumerateEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation19VisitForInEnumerateEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation21VisitTestUndetectableEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation21VisitTestUndetectableEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation21VisitTestUndetectableEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation21VisitTestUndetectableEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25693:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rdx
	movl	252(%rdx), %eax
	addl	248(%rdx), %eax
	movq	936(%rdx), %rcx
	movslq	%eax, %rsi
	js	.L937
	movq	944(%rdx), %rax
	movabsq	$7905747460161236407, %rdx
	subq	%rcx, %rax
	sarq	$5, %rax
	imulq	%rdx, %rax
	cmpq	%rsi, %rax
	jbe	.L941
.L937:
	leaq	0(,%rsi,8), %rdi
	subq	%rsi, %rdi
	salq	$5, %rdi
	addq	%rcx, %rdi
	jmp	_ZN2v88internal8compiler5Hints5ClearEv
	.p2align 4,,10
	.p2align 3
.L941:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE25693:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation21VisitTestUndetectableEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation21VisitTestUndetectableEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation13VisitTestNullEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation13VisitTestNullEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation13VisitTestNullEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation13VisitTestNullEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25685:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rdx
	movl	252(%rdx), %eax
	addl	248(%rdx), %eax
	movq	936(%rdx), %rcx
	movslq	%eax, %rsi
	js	.L943
	movq	944(%rdx), %rax
	movabsq	$7905747460161236407, %rdx
	subq	%rcx, %rax
	sarq	$5, %rax
	imulq	%rdx, %rax
	cmpq	%rsi, %rax
	jbe	.L947
.L943:
	leaq	0(,%rsi,8), %rdi
	subq	%rsi, %rdi
	salq	$5, %rdi
	addq	%rcx, %rdi
	jmp	_ZN2v88internal8compiler5Hints5ClearEv
	.p2align 4,,10
	.p2align 3
.L947:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE25685:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation13VisitTestNullEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation13VisitTestNullEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitStaLookupSlotEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitStaLookupSlotEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitStaLookupSlotEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitStaLookupSlotEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25605:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	xorl	%esi, %esi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rbx), %rax
	movq	(%rax), %rdx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor26GetConstantForIndexOperandEiPNS0_7IsolateE@PLT
	movq	(%rbx), %rsi
	movl	$1, %ecx
	leaq	-48(%rbp), %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler9ObjectRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	24(%rbx), %rdx
	movl	252(%rdx), %eax
	addl	248(%rdx), %eax
	movq	936(%rdx), %rcx
	cltq
	js	.L950
	movq	944(%rdx), %rdx
	movabsq	$7905747460161236407, %rsi
	subq	%rcx, %rdx
	sarq	$5, %rdx
	imulq	%rsi, %rdx
	cmpq	%rax, %rdx
	jbe	.L953
.L950:
	leaq	0(,%rax,8), %rdi
	subq	%rax, %rdi
	salq	$5, %rdi
	addq	%rcx, %rdi
	call	_ZN2v88internal8compiler5Hints5ClearEv
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L954
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L953:
	.cfi_restore_state
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L954:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25605:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitStaLookupSlotEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitStaLookupSlotEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation30VisitLdaLookupSlotInsideTypeofEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation30VisitLdaLookupSlotInsideTypeofEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation30VisitLdaLookupSlotInsideTypeofEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation30VisitLdaLookupSlotInsideTypeofEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25603:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	xorl	%esi, %esi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rbx), %rax
	movq	(%rax), %rdx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor26GetConstantForIndexOperandEiPNS0_7IsolateE@PLT
	movq	(%rbx), %rsi
	movl	$1, %ecx
	leaq	-48(%rbp), %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler9ObjectRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	24(%rbx), %rdx
	movl	252(%rdx), %eax
	addl	248(%rdx), %eax
	movq	936(%rdx), %rcx
	cltq
	js	.L957
	movq	944(%rdx), %rdx
	movabsq	$7905747460161236407, %rsi
	subq	%rcx, %rdx
	sarq	$5, %rdx
	imulq	%rsi, %rdx
	cmpq	%rax, %rdx
	jbe	.L960
.L957:
	leaq	0(,%rax,8), %rdi
	subq	%rax, %rdi
	salq	$5, %rdi
	addq	%rcx, %rdi
	call	_ZN2v88internal8compiler5Hints5ClearEv
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L961
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L960:
	.cfi_restore_state
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L961:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25603:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation30VisitLdaLookupSlotInsideTypeofEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation30VisitLdaLookupSlotInsideTypeofEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZNSt8_Rb_treeIN2v88internal8compiler14VirtualContextES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE16_M_insert_uniqueIRKS3_EESt4pairISt17_Rb_tree_iteratorIS3_EbEOT_,"axG",@progbits,_ZNSt8_Rb_treeIN2v88internal8compiler14VirtualContextES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE16_M_insert_uniqueIRKS3_EESt4pairISt17_Rb_tree_iteratorIS3_EbEOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN2v88internal8compiler14VirtualContextES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE16_M_insert_uniqueIRKS3_EESt4pairISt17_Rb_tree_iteratorIS3_EbEOT_
	.type	_ZNSt8_Rb_treeIN2v88internal8compiler14VirtualContextES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE16_M_insert_uniqueIRKS3_EESt4pairISt17_Rb_tree_iteratorIS3_EbEOT_, @function
_ZNSt8_Rb_treeIN2v88internal8compiler14VirtualContextES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE16_M_insert_uniqueIRKS3_EESt4pairISt17_Rb_tree_iteratorIS3_EbEOT_:
.LFB22774:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	16(%rdi), %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	24(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L976
	movq	8(%rsi), %rcx
	jmp	.L964
	.p2align 4,,10
	.p2align 3
.L984:
	movl	32(%rbx), %eax
	cmpl	%eax, 0(%r13)
	jnb	.L966
	movq	16(%rbx), %rax
	movl	$1, %esi
	testq	%rax, %rax
	je	.L965
.L985:
	movq	%rax, %rbx
.L964:
	movq	40(%rbx), %rdx
	cmpq	%rdx, %rcx
	jb	.L984
.L966:
	movq	24(%rbx), %rax
	xorl	%esi, %esi
	testq	%rax, %rax
	jne	.L985
.L965:
	movq	%rbx, %r14
	testb	%sil, %sil
	jne	.L963
.L968:
	cmpq	%rcx, %rdx
	jnb	.L969
	movl	0(%r13), %eax
	cmpl	%eax, 32(%rbx)
	jnb	.L969
	testq	%r14, %r14
	je	.L986
	movl	$1, %r8d
	cmpq	%r14, %r15
	jne	.L987
.L970:
	movq	(%r12), %rdi
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	$47, %rax
	jbe	.L988
	leaq	48(%rbx), %rax
	movq	%rax, 16(%rdi)
.L972:
	movl	0(%r13), %edx
	movq	8(%r13), %rax
	movq	%r15, %rcx
	movq	%rbx, %rsi
	movl	%r8d, %edi
	movl	%edx, 32(%rbx)
	movq	%r14, %rdx
	movq	%rax, 40(%rbx)
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 48(%r12)
	addq	$24, %rsp
	movq	%rbx, %rax
	movl	$1, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L986:
	.cfi_restore_state
	xorl	%ebx, %ebx
.L969:
	addq	$24, %rsp
	movq	%rbx, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L976:
	.cfi_restore_state
	movq	%r15, %rbx
.L963:
	cmpq	32(%r12), %rbx
	je	.L989
	movq	%rbx, %rdi
	movq	%rbx, %r14
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	8(%r13), %rcx
	movq	40(%rax), %rdx
	movq	%rax, %rbx
	jmp	.L968
	.p2align 4,,10
	.p2align 3
.L989:
	movq	%rbx, %r14
	movl	$1, %r8d
	cmpq	%r14, %r15
	je	.L970
.L987:
	movq	40(%r14), %rax
	xorl	%r8d, %r8d
	cmpq	%rax, 8(%r13)
	jnb	.L970
	xorl	%r8d, %r8d
	movl	32(%r14), %eax
	cmpl	%eax, 0(%r13)
	setb	%r8b
	jmp	.L970
	.p2align 4,,10
	.p2align 3
.L988:
	movl	$48, %esi
	movl	%r8d, -52(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-52(%rbp), %r8d
	movq	%rax, %rbx
	jmp	.L972
	.cfi_endproc
.LFE22774:
	.size	_ZNSt8_Rb_treeIN2v88internal8compiler14VirtualContextES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE16_M_insert_uniqueIRKS3_EESt4pairISt17_Rb_tree_iteratorIS3_EbEOT_, .-_ZNSt8_Rb_treeIN2v88internal8compiler14VirtualContextES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE16_M_insert_uniqueIRKS3_EESt4pairISt17_Rb_tree_iteratorIS3_EbEOT_
	.section	.text._ZN2v88internal8compiler5Hints17AddVirtualContextENS1_14VirtualContextE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler5Hints17AddVirtualContextENS1_14VirtualContextE
	.type	_ZN2v88internal8compiler5Hints17AddVirtualContextENS1_14VirtualContextE, @function
_ZN2v88internal8compiler5Hints17AddVirtualContextENS1_14VirtualContextE:
.LFB18791:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rsi, -16(%rbp)
	leaq	-16(%rbp), %rsi
	movq	%rdx, -8(%rbp)
	call	_ZNSt8_Rb_treeIN2v88internal8compiler14VirtualContextES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE16_M_insert_uniqueIRKS3_EESt4pairISt17_Rb_tree_iteratorIS3_EbEOT_
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18791:
	.size	_ZN2v88internal8compiler5Hints17AddVirtualContextENS1_14VirtualContextE, .-_ZN2v88internal8compiler5Hints17AddVirtualContextENS1_14VirtualContextE
	.section	.rodata._ZN2v88internal8compiler34SerializerForBackgroundCompilation20ProcessCreateContextEPNS0_11interpreter21BytecodeArrayIteratorEi.str1.1,"aMS",@progbits,1
.LC44:
	.string	"distance > 0"
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation20ProcessCreateContextEPNS0_11interpreter21BytecodeArrayIteratorEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation20ProcessCreateContextEPNS0_11interpreter21BytecodeArrayIteratorEi
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation20ProcessCreateContextEPNS0_11interpreter21BytecodeArrayIteratorEi, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation20ProcessCreateContextEPNS0_11interpreter21BytecodeArrayIteratorEi:
.LFB18966:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movl	%edx, %esi
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rbx), %rax
	movq	(%rax), %rdx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor26GetConstantForIndexOperandEiPNS0_7IsolateE@PLT
	movq	(%rbx), %rsi
	movl	$1, %ecx
	leaq	-96(%rbp), %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler12ScopeInfoRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	24(%rbx), %rbx
	movq	%rbx, %rdi
	leaq	552(%rbx), %r14
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment17accumulator_hintsEv
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler5Hints5ClearEv
	movq	568(%rbx), %r12
	cmpq	%r12, %r14
	je	.L993
	leaq	-80(%rbp), %r15
	jmp	.L997
	.p2align 4,,10
	.p2align 3
.L995:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rax, %r14
	je	.L993
.L997:
	movq	32(%r12), %rdx
	movq	(%rdx), %rax
	testb	$1, %al
	je	.L995
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	subw	$138, %ax
	cmpw	$9, %ax
	ja	.L995
	movq	%r15, %rsi
	movq	%r13, %rdi
	movl	$1, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZNSt8_Rb_treeIN2v88internal8compiler14VirtualContextES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE16_M_insert_uniqueIRKS3_EESt4pairISt17_Rb_tree_iteratorIS3_EbEOT_
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rax, %r14
	jne	.L997
	.p2align 4,,10
	.p2align 3
.L993:
	movq	512(%rbx), %r12
	addq	$496, %rbx
	cmpq	%r12, %rbx
	je	.L992
	leaq	-80(%rbp), %r14
	jmp	.L1000
	.p2align 4,,10
	.p2align 3
.L999:
	movq	%r14, %rsi
	movq	%r13, %rdi
	movl	%eax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZNSt8_Rb_treeIN2v88internal8compiler14VirtualContextES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE16_M_insert_uniqueIRKS3_EESt4pairISt17_Rb_tree_iteratorIS3_EbEOT_
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rax, %rbx
	je	.L992
.L1000:
	movl	32(%r12), %eax
	movq	40(%r12), %rdx
	addl	$1, %eax
	jne	.L999
	leaq	.LC44(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L992:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1005
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1005:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18966:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation20ProcessCreateContextEPNS0_11interpreter21BytecodeArrayIteratorEi, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation20ProcessCreateContextEPNS0_11interpreter21BytecodeArrayIteratorEi
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation23VisitCreateBlockContextEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation23VisitCreateBlockContextEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation23VisitCreateBlockContextEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation23VisitCreateBlockContextEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB18960:
	.cfi_startproc
	endbr64
	xorl	%edx, %edx
	jmp	_ZN2v88internal8compiler34SerializerForBackgroundCompilation20ProcessCreateContextEPNS0_11interpreter21BytecodeArrayIteratorEi
	.cfi_endproc
.LFE18960:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation23VisitCreateBlockContextEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation23VisitCreateBlockContextEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation23VisitCreateCatchContextEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation23VisitCreateCatchContextEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation23VisitCreateCatchContextEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation23VisitCreateCatchContextEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB18963:
	.cfi_startproc
	endbr64
	movl	$1, %edx
	jmp	_ZN2v88internal8compiler34SerializerForBackgroundCompilation20ProcessCreateContextEPNS0_11interpreter21BytecodeArrayIteratorEi
	.cfi_endproc
.LFE18963:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation23VisitCreateCatchContextEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation23VisitCreateCatchContextEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation22VisitCreateWithContextEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation22VisitCreateWithContextEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation22VisitCreateWithContextEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation22VisitCreateWithContextEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25547:
	.cfi_startproc
	endbr64
	movl	$1, %edx
	jmp	_ZN2v88internal8compiler34SerializerForBackgroundCompilation20ProcessCreateContextEPNS0_11interpreter21BytecodeArrayIteratorEi
	.cfi_endproc
.LFE25547:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation22VisitCreateWithContextEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation22VisitCreateWithContextEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation26VisitCreateFunctionContextEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation26VisitCreateFunctionContextEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation26VisitCreateFunctionContextEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation26VisitCreateFunctionContextEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25545:
	.cfi_startproc
	endbr64
	xorl	%edx, %edx
	jmp	_ZN2v88internal8compiler34SerializerForBackgroundCompilation20ProcessCreateContextEPNS0_11interpreter21BytecodeArrayIteratorEi
	.cfi_endproc
.LFE25545:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation26VisitCreateFunctionContextEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation26VisitCreateFunctionContextEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation22VisitCreateEvalContextEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation22VisitCreateEvalContextEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation22VisitCreateEvalContextEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation22VisitCreateEvalContextEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25543:
	.cfi_startproc
	endbr64
	xorl	%edx, %edx
	jmp	_ZN2v88internal8compiler34SerializerForBackgroundCompilation20ProcessCreateContextEPNS0_11interpreter21BytecodeArrayIteratorEi
	.cfi_endproc
.LFE25543:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation22VisitCreateEvalContextEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation22VisitCreateEvalContextEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_,"axG",@progbits,_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_
	.type	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_, @function
_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_:
.LFB22793:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	16(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	24(%rdi), %r12
	testq	%r12, %r12
	je	.L1025
	movq	(%rsi), %rsi
	jmp	.L1013
	.p2align 4,,10
	.p2align 3
.L1032:
	movq	16(%r12), %rax
	movl	$1, %edx
	testq	%rax, %rax
	je	.L1014
.L1033:
	movq	%rax, %r12
.L1013:
	movq	32(%r12), %rcx
	cmpq	%rcx, %rsi
	jb	.L1032
	movq	24(%r12), %rax
	xorl	%edx, %edx
	testq	%rax, %rax
	jne	.L1033
.L1014:
	testb	%dl, %dl
	jne	.L1012
	cmpq	%rcx, %rsi
	jbe	.L1018
.L1023:
	movl	$1, %r8d
	cmpq	%r12, %r15
	jne	.L1034
.L1019:
	movq	(%rbx), %rdi
	movq	16(%rdi), %r13
	movq	24(%rdi), %rax
	subq	%r13, %rax
	cmpq	$39, %rax
	jbe	.L1035
	leaq	40(%r13), %rax
	movq	%rax, 16(%rdi)
.L1021:
	movq	(%r14), %rax
	movq	%r12, %rdx
	movq	%r15, %rcx
	movq	%r13, %rsi
	movl	%r8d, %edi
	movq	%rax, 32(%r13)
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 48(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	movl	$1, %edx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1025:
	.cfi_restore_state
	movq	%r15, %r12
.L1012:
	cmpq	%r12, 32(%rbx)
	je	.L1023
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	(%r14), %rcx
	cmpq	%rcx, 32(%rax)
	jb	.L1023
	movq	%rax, %r12
.L1018:
	addq	$24, %rsp
	movq	%r12, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1034:
	.cfi_restore_state
	xorl	%r8d, %r8d
	movq	32(%r12), %rax
	cmpq	%rax, (%r14)
	setb	%r8b
	jmp	.L1019
	.p2align 4,,10
	.p2align 3
.L1035:
	movl	$40, %esi
	movl	%r8d, -52(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-52(%rbp), %r8d
	movq	%rax, %r13
	jmp	.L1021
	.cfi_endproc
.LFE22793:
	.size	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_, .-_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_
	.section	.text._ZN2v88internal8compiler5Hints11AddConstantENS0_6HandleINS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler5Hints11AddConstantENS0_6HandleINS0_6ObjectEEE
	.type	_ZN2v88internal8compiler5Hints11AddConstantENS0_6HandleINS0_6ObjectEEE, @function
_ZN2v88internal8compiler5Hints11AddConstantENS0_6HandleINS0_6ObjectEEE:
.LFB18792:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$56, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rsi, -8(%rbp)
	leaq	-8(%rbp), %rsi
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18792:
	.size	_ZN2v88internal8compiler5Hints11AddConstantENS0_6HandleINS0_6ObjectEEE, .-_ZN2v88internal8compiler5Hints11AddConstantENS0_6HandleINS0_6ObjectEEE
	.section	.text._ZN2v88internal8compiler17FunctionBlueprintC2ENS0_6HandleINS0_10JSFunctionEEEPNS0_7IsolateEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17FunctionBlueprintC2ENS0_6HandleINS0_10JSFunctionEEEPNS0_7IsolateEPNS0_4ZoneE
	.type	_ZN2v88internal8compiler17FunctionBlueprintC2ENS0_6HandleINS0_10JSFunctionEEEPNS0_7IsolateEPNS0_4ZoneE, @function
_ZN2v88internal8compiler17FunctionBlueprintC2ENS0_6HandleINS0_10JSFunctionEEEPNS0_7IsolateEPNS0_4ZoneE:
.LFB18727:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	23(%rax), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L1039
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L1040:
	movq	%rax, (%rbx)
	movq	(%r14), %rax
	movq	41112(%r12), %rdi
	movq	39(%rax), %rax
	movq	7(%rax), %rsi
	testq	%rdi, %rdi
	je	.L1042
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L1043:
	movq	%rax, %xmm0
	leaq	32(%rbx), %rax
	movq	%r13, %xmm1
	movl	$0, 32(%rbx)
	movq	%rax, 48(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, 56(%rbx)
	leaq	88(%rbx), %rax
	movq	%rax, 104(%rbx)
	movq	%rax, 112(%rbx)
	leaq	144(%rbx), %rax
	movq	%rax, 160(%rbx)
	movq	%rax, 168(%rbx)
	leaq	200(%rbx), %rax
	movq	$0, 40(%rbx)
	movq	$0, 64(%rbx)
	movq	%r13, 72(%rbx)
	movl	$0, 88(%rbx)
	movq	$0, 96(%rbx)
	movq	$0, 120(%rbx)
	movq	%r13, 128(%rbx)
	movl	$0, 144(%rbx)
	movq	$0, 152(%rbx)
	movq	$0, 176(%rbx)
	movq	%r13, 184(%rbx)
	movl	$0, 200(%rbx)
	movq	$0, 208(%rbx)
	movq	%rax, 216(%rbx)
	movq	%rax, 224(%rbx)
	movq	$0, 232(%rbx)
	movq	(%r14), %rax
	movups	%xmm0, 8(%rbx)
	movq	41112(%r12), %rdi
	movq	31(%rax), %rsi
	testq	%rdi, %rdi
	je	.L1045
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L1046:
	leaq	-48(%rbp), %rsi
	leaq	72(%rbx), %rdi
	movq	%rax, -48(%rbp)
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1050
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1039:
	.cfi_restore_state
	movq	41088(%rdx), %rax
	cmpq	41096(%rdx), %rax
	je	.L1051
.L1041:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L1040
	.p2align 4,,10
	.p2align 3
.L1045:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L1052
.L1047:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L1046
	.p2align 4,,10
	.p2align 3
.L1042:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L1053
.L1044:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L1043
	.p2align 4,,10
	.p2align 3
.L1051:
	movq	%rdx, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1041
	.p2align 4,,10
	.p2align 3
.L1053:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L1052:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1047
.L1050:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18727:
	.size	_ZN2v88internal8compiler17FunctionBlueprintC2ENS0_6HandleINS0_10JSFunctionEEEPNS0_7IsolateEPNS0_4ZoneE, .-_ZN2v88internal8compiler17FunctionBlueprintC2ENS0_6HandleINS0_10JSFunctionEEEPNS0_7IsolateEPNS0_4ZoneE
	.globl	_ZN2v88internal8compiler17FunctionBlueprintC1ENS0_6HandleINS0_10JSFunctionEEEPNS0_7IsolateEPNS0_4ZoneE
	.set	_ZN2v88internal8compiler17FunctionBlueprintC1ENS0_6HandleINS0_10JSFunctionEEEPNS0_7IsolateEPNS0_4ZoneE,_ZN2v88internal8compiler17FunctionBlueprintC2ENS0_6HandleINS0_10JSFunctionEEEPNS0_7IsolateEPNS0_4ZoneE
	.section	.rodata._ZN2v88internal8compiler18CompilationSubjectC2ENS0_6HandleINS0_10JSFunctionEEEPNS0_7IsolateEPNS0_4ZoneE.str1.8,"aMS",@progbits,1
	.align 8
.LC45:
	.string	"closure->has_feedback_vector()"
	.section	.text._ZN2v88internal8compiler18CompilationSubjectC2ENS0_6HandleINS0_10JSFunctionEEEPNS0_7IsolateEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18CompilationSubjectC2ENS0_6HandleINS0_10JSFunctionEEEPNS0_7IsolateEPNS0_4ZoneE
	.type	_ZN2v88internal8compiler18CompilationSubjectC2ENS0_6HandleINS0_10JSFunctionEEEPNS0_7IsolateEPNS0_4ZoneE, @function
_ZN2v88internal8compiler18CompilationSubjectC2ENS0_6HandleINS0_10JSFunctionEEEPNS0_7IsolateEPNS0_4ZoneE:
.LFB18730:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_ZN2v88internal8compiler17FunctionBlueprintC1ENS0_6HandleINS0_10JSFunctionEEEPNS0_7IsolateEPNS0_4ZoneE
	movq	%rbx, 240(%r12)
	movq	(%rbx), %rdx
	movabsq	$287762808832, %rcx
	movq	23(%rdx), %rax
	movq	7(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1058
	testb	$1, %al
	jne	.L1056
.L1060:
	movq	39(%rdx), %rax
	movq	7(%rax), %rax
	movq	-1(%rax), %rax
	cmpw	$155, 11(%rax)
	je	.L1062
.L1058:
	leaq	.LC45(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1062:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1056:
	.cfi_restore_state
	movq	-1(%rax), %rcx
	cmpw	$165, 11(%rcx)
	je	.L1058
	movq	-1(%rax), %rax
	cmpw	$166, 11(%rax)
	jne	.L1060
	jmp	.L1058
	.cfi_endproc
.LFE18730:
	.size	_ZN2v88internal8compiler18CompilationSubjectC2ENS0_6HandleINS0_10JSFunctionEEEPNS0_7IsolateEPNS0_4ZoneE, .-_ZN2v88internal8compiler18CompilationSubjectC2ENS0_6HandleINS0_10JSFunctionEEEPNS0_7IsolateEPNS0_4ZoneE
	.globl	_ZN2v88internal8compiler18CompilationSubjectC1ENS0_6HandleINS0_10JSFunctionEEEPNS0_7IsolateEPNS0_4ZoneE
	.set	_ZN2v88internal8compiler18CompilationSubjectC1ENS0_6HandleINS0_10JSFunctionEEEPNS0_7IsolateEPNS0_4ZoneE,_ZN2v88internal8compiler18CompilationSubjectC2ENS0_6HandleINS0_10JSFunctionEEEPNS0_7IsolateEPNS0_4ZoneE
	.section	.text._ZN2v88internal8compiler5Hints14SingleConstantENS0_6HandleINS0_6ObjectEEEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler5Hints14SingleConstantENS0_6HandleINS0_6ObjectEEEPNS0_4ZoneE
	.type	_ZN2v88internal8compiler5Hints14SingleConstantENS0_6HandleINS0_6ObjectEEEPNS0_4ZoneE, @function
_ZN2v88internal8compiler5Hints14SingleConstantENS0_6HandleINS0_6ObjectEEEPNS0_4ZoneE:
.LFB18735:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	16(%rdi), %rax
	movq	%rdx, (%rdi)
	movq	%rax, 32(%rdi)
	movq	%rax, 40(%rdi)
	leaq	72(%rdi), %rax
	movq	%rax, 88(%rdi)
	movq	%rax, 96(%rdi)
	leaq	128(%rdi), %rax
	movq	%rax, 144(%rdi)
	movq	%rax, 152(%rdi)
	leaq	184(%rdi), %rax
	addq	$56, %rdi
	movl	$0, -40(%rdi)
	movq	$0, -32(%rdi)
	movq	$0, -8(%rdi)
	movq	%rdx, (%rdi)
	movl	$0, 16(%rdi)
	movq	$0, 24(%rdi)
	movq	$0, 48(%rdi)
	movq	%rdx, 56(%rdi)
	movl	$0, 72(%rdi)
	movq	$0, 80(%rdi)
	movq	$0, 104(%rdi)
	movq	%rdx, 112(%rdi)
	movl	$0, 128(%rdi)
	movq	$0, 136(%rdi)
	movq	%rax, 144(%rdi)
	movq	%rax, 152(%rdi)
	movq	$0, 160(%rdi)
	movq	%rsi, -32(%rbp)
	leaq	-32(%rbp), %rsi
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1066
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1066:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18735:
	.size	_ZN2v88internal8compiler5Hints14SingleConstantENS0_6HandleINS0_6ObjectEEEPNS0_4ZoneE, .-_ZN2v88internal8compiler5Hints14SingleConstantENS0_6HandleINS0_6ObjectEEEPNS0_4ZoneE
	.section	.rodata._ZN2v88internal8compiler34SerializerForBackgroundCompilation18ProcessBuiltinCallENS0_6HandleINS0_18SharedFunctionInfoEEERKNS0_10ZoneVectorINS1_5HintsEEENS0_15SpeculationModeE.str1.8,"aMS",@progbits,1
	.align 8
.LC46:
	.string	"Serializing for call to builtin "
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation18ProcessBuiltinCallENS0_6HandleINS0_18SharedFunctionInfoEEERKNS0_10ZoneVectorINS1_5HintsEEENS0_15SpeculationModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation18ProcessBuiltinCallENS0_6HandleINS0_18SharedFunctionInfoEEERKNS0_10ZoneVectorINS1_5HintsEEENS0_15SpeculationModeE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation18ProcessBuiltinCallENS0_6HandleINS0_18SharedFunctionInfoEEERKNS0_10ZoneVectorINS1_5HintsEEENS0_15SpeculationModeE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation18ProcessBuiltinCallENS0_6HandleINS0_18SharedFunctionInfoEEERKNS0_10ZoneVectorINS1_5HintsEEENS0_15SpeculationModeE:
.LFB18999:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$264, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	7(%rax), %rbx
	sarq	$32, %rbx
	movl	%ebx, %edi
	call	_ZN2v88internal8Builtins4nameEi@PLT
	movq	(%r12), %rdi
	cmpb	$0, 124(%rdi)
	je	.L1068
	cmpb	$0, _ZN2v88internal30FLAG_trace_heap_broker_verboseE(%rip)
	jne	.L1137
.L1068:
	cmpq	$527, %rbx
	jg	.L1071
	cmpl	$459, %ebx
	jle	.L1138
	subl	$460, %ebx
	cmpl	$67, %ebx
	ja	.L1067
	leaq	.L1081(%rip), %rdx
	movslq	(%rdx,%rbx,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler34SerializerForBackgroundCompilation18ProcessBuiltinCallENS0_6HandleINS0_18SharedFunctionInfoEEERKNS0_10ZoneVectorINS1_5HintsEEENS0_15SpeculationModeE,"a",@progbits
	.align 4
	.align 4
.L1081:
	.long	.L1089-.L1081
	.long	.L1067-.L1081
	.long	.L1067-.L1081
	.long	.L1067-.L1081
	.long	.L1067-.L1081
	.long	.L1067-.L1081
	.long	.L1067-.L1081
	.long	.L1067-.L1081
	.long	.L1067-.L1081
	.long	.L1067-.L1081
	.long	.L1067-.L1081
	.long	.L1067-.L1081
	.long	.L1067-.L1081
	.long	.L1067-.L1081
	.long	.L1067-.L1081
	.long	.L1067-.L1081
	.long	.L1067-.L1081
	.long	.L1067-.L1081
	.long	.L1067-.L1081
	.long	.L1067-.L1081
	.long	.L1067-.L1081
	.long	.L1088-.L1081
	.long	.L1067-.L1081
	.long	.L1087-.L1081
	.long	.L1067-.L1081
	.long	.L1067-.L1081
	.long	.L1067-.L1081
	.long	.L1067-.L1081
	.long	.L1067-.L1081
	.long	.L1067-.L1081
	.long	.L1067-.L1081
	.long	.L1067-.L1081
	.long	.L1067-.L1081
	.long	.L1067-.L1081
	.long	.L1067-.L1081
	.long	.L1067-.L1081
	.long	.L1067-.L1081
	.long	.L1067-.L1081
	.long	.L1067-.L1081
	.long	.L1067-.L1081
	.long	.L1067-.L1081
	.long	.L1076-.L1081
	.long	.L1067-.L1081
	.long	.L1085-.L1081
	.long	.L1067-.L1081
	.long	.L1085-.L1081
	.long	.L1067-.L1081
	.long	.L1067-.L1081
	.long	.L1067-.L1081
	.long	.L1084-.L1081
	.long	.L1067-.L1081
	.long	.L1067-.L1081
	.long	.L1085-.L1081
	.long	.L1067-.L1081
	.long	.L1067-.L1081
	.long	.L1067-.L1081
	.long	.L1067-.L1081
	.long	.L1067-.L1081
	.long	.L1067-.L1081
	.long	.L1067-.L1081
	.long	.L1067-.L1081
	.long	.L1067-.L1081
	.long	.L1067-.L1081
	.long	.L1067-.L1081
	.long	.L1067-.L1081
	.long	.L1082-.L1081
	.long	.L1080-.L1081
	.long	.L1080-.L1081
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation18ProcessBuiltinCallENS0_6HandleINS0_18SharedFunctionInfoEEERKNS0_10ZoneVectorINS1_5HintsEEENS0_15SpeculationModeE
	.p2align 4,,10
	.p2align 3
.L1138:
	cmpl	$348, %ebx
	je	.L1073
	jle	.L1139
	cmpl	$407, %ebx
	jne	.L1067
	movq	(%r12), %rax
	movl	$56, %esi
	leaq	-288(%rbp), %r13
	movq	(%rax), %rdi
	addq	$41184, %rdi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	movq	(%r12), %rsi
	movl	$1, %ecx
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler9ObjectRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	(%r12), %rsi
	movl	$1, %ecx
	movq	%r13, %rdi
	movq	(%rsi), %rax
	leaq	1024(%rax), %rdx
	call	_ZN2v88internal8compiler9ObjectRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
.L1067:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1140
	addq	$264, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1071:
	.cfi_restore_state
	cmpl	$848, %ebx
	je	.L1091
	jle	.L1141
	cmpl	$863, %ebx
	je	.L1091
	cmpl	$872, %ebx
	jne	.L1067
	movq	8(%r13), %rsi
	cmpq	%rsi, 16(%r13)
	je	.L1067
	cmpl	$1, %r14d
	je	.L1067
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation25ProcessHintsForRegExpTestERKNS1_5HintsE
	jmp	.L1067
	.p2align 4,,10
	.p2align 3
.L1139:
	cmpl	$346, %ebx
	jne	.L1142
	movq	8(%r13), %rsi
	cmpq	%rsi, 16(%r13)
	je	.L1067
	cmpl	$1, %r14d
	je	.L1067
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation27ProcessHintsForFunctionBindERKNS1_5HintsE
	jmp	.L1067
	.p2align 4,,10
	.p2align 3
.L1141:
	cmpl	$571, %ebx
	je	.L1093
	subl	$730, %ebx
	cmpl	$52, %ebx
	ja	.L1067
	leaq	.L1079(%rip), %rdx
	movslq	(%rdx,%rbx,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler34SerializerForBackgroundCompilation18ProcessBuiltinCallENS0_6HandleINS0_18SharedFunctionInfoEEERKNS0_10ZoneVectorINS1_5HintsEEENS0_15SpeculationModeE
	.align 4
	.align 4
.L1079:
	.long	.L1078-.L1079
	.long	.L1067-.L1079
	.long	.L1067-.L1079
	.long	.L1067-.L1079
	.long	.L1078-.L1079
	.long	.L1067-.L1079
	.long	.L1067-.L1079
	.long	.L1067-.L1079
	.long	.L1067-.L1079
	.long	.L1078-.L1079
	.long	.L1067-.L1079
	.long	.L1067-.L1079
	.long	.L1067-.L1079
	.long	.L1067-.L1079
	.long	.L1078-.L1079
	.long	.L1067-.L1079
	.long	.L1067-.L1079
	.long	.L1067-.L1079
	.long	.L1078-.L1079
	.long	.L1067-.L1079
	.long	.L1067-.L1079
	.long	.L1067-.L1079
	.long	.L1067-.L1079
	.long	.L1067-.L1079
	.long	.L1067-.L1079
	.long	.L1067-.L1079
	.long	.L1067-.L1079
	.long	.L1067-.L1079
	.long	.L1067-.L1079
	.long	.L1067-.L1079
	.long	.L1067-.L1079
	.long	.L1067-.L1079
	.long	.L1067-.L1079
	.long	.L1067-.L1079
	.long	.L1078-.L1079
	.long	.L1067-.L1079
	.long	.L1067-.L1079
	.long	.L1067-.L1079
	.long	.L1067-.L1079
	.long	.L1067-.L1079
	.long	.L1078-.L1079
	.long	.L1067-.L1079
	.long	.L1067-.L1079
	.long	.L1067-.L1079
	.long	.L1067-.L1079
	.long	.L1078-.L1079
	.long	.L1067-.L1079
	.long	.L1067-.L1079
	.long	.L1067-.L1079
	.long	.L1067-.L1079
	.long	.L1067-.L1079
	.long	.L1067-.L1079
	.long	.L1078-.L1079
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation18ProcessBuiltinCallENS0_6HandleINS0_18SharedFunctionInfoEEERKNS0_10ZoneVectorINS1_5HintsEEENS0_15SpeculationModeE
	.p2align 4,,10
	.p2align 3
.L1142:
	cmpl	$347, %ebx
	jne	.L1143
.L1076:
	movq	8(%r13), %rsi
	cmpq	16(%r13), %rsi
	je	.L1067
.L1133:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation27ProcessHintsForFunctionCallERKNS1_5HintsE
	jmp	.L1067
	.p2align 4,,10
	.p2align 3
.L1143:
	cmpl	$344, %ebx
	je	.L1076
	jmp	.L1067
.L1085:
	cmpl	$1, %r14d
	je	.L1067
	movq	8(%r13), %rsi
	cmpq	16(%r13), %rsi
	jne	.L1131
	leaq	.LC40(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1080:
	movq	8(%r13), %rsi
	movq	16(%r13), %rax
	movabsq	$7905747460161236407, %rdx
	subq	%rsi, %rax
	sarq	$5, %rax
	imulq	%rdx, %rax
	cmpq	$1, %rax
	jbe	.L1067
	jmp	.L1132
.L1078:
	movq	8(%r13), %rsi
	movq	16(%r13), %rax
	movabsq	$7905747460161236407, %rdx
	subq	%rsi, %rax
	sarq	$5, %rax
	imulq	%rdx, %rax
	cmpq	$1, %rax
	jbe	.L1067
	cmpl	$1, %r14d
	je	.L1067
.L1132:
	addq	$224, %rsi
	jmp	.L1133
.L1089:
	movq	8(%r13), %rsi
	movq	16(%r13), %rax
	movabsq	$7905747460161236407, %rdx
	subq	%rsi, %rax
	sarq	$5, %rax
	imulq	%rdx, %rax
	cmpq	$1, %rax
	jbe	.L1096
	addq	$224, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation27ProcessHintsForObjectCreateERKNS1_5HintsE
	jmp	.L1067
.L1088:
	movq	8(%r13), %rsi
	movq	16(%r13), %rax
	movabsq	$7905747460161236407, %rdx
	subq	%rsi, %rax
	sarq	$5, %rax
	imulq	%rdx, %rax
	cmpq	$1, %rax
	jbe	.L1067
	addq	$224, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation34ProcessHintsForHasInPrototypeChainERKNS1_5HintsE
	jmp	.L1067
.L1087:
	movq	8(%r13), %rsi
	cmpq	16(%r13), %rsi
	je	.L1067
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation33ProcessHintsForObjectGetPrototypeERKNS1_5HintsE
	jmp	.L1067
.L1084:
	movq	8(%r13), %rsi
	movq	16(%r13), %rax
	movabsq	$7905747460161236407, %rdx
	subq	%rsi, %rax
	sarq	$5, %rax
	imulq	%rdx, %rax
	cmpq	$1, %rax
	jbe	.L1067
	addq	$224, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation29ProcessHintsForPromiseResolveERKNS1_5HintsE
	jmp	.L1067
.L1082:
	movq	8(%r13), %rsi
	movq	16(%r13), %rax
	movabsq	$7905747460161236407, %rdx
	subq	%rsi, %rax
	sarq	$5, %rax
	imulq	%rdx, %rax
	cmpq	$2, %rax
	jbe	.L1067
	addq	$448, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation29ProcessHintsForPromiseResolveERKNS1_5HintsE
	jmp	.L1067
	.p2align 4,,10
	.p2align 3
.L1137:
	movq	%rax, %r15
	call	_ZN2v88internal8compiler12JSHeapBroker5TraceEv@PLT
	movl	$32, %edx
	leaq	.LC46(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, -296(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	testq	%r15, %r15
	movq	-296(%rbp), %r8
	je	.L1144
	movq	%r15, %rdi
	movq	%r8, -296(%rbp)
	call	strlen@PLT
	movq	-296(%rbp), %r8
	movq	%r15, %rsi
	movq	%rax, %rdx
	movq	%r8, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-296(%rbp), %r8
.L1070:
	leaq	-288(%rbp), %rsi
	movl	$1, %edx
	movq	%r8, %rdi
	movb	$10, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L1068
	.p2align 4,,10
	.p2align 3
.L1131:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation26ProcessMapHintsForPromisesERKNS1_5HintsE
	jmp	.L1067
	.p2align 4,,10
	.p2align 3
.L1091:
	movq	8(%r13), %rsi
	movq	16(%r13), %rax
	movabsq	$7905747460161236407, %rdx
	subq	%rsi, %rax
	sarq	$5, %rax
	imulq	%rdx, %rax
	cmpq	$1, %rax
	jbe	.L1099
	addq	$224, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation33ProcessHintsForObjectGetPrototypeERKNS1_5HintsE
	jmp	.L1067
	.p2align 4,,10
	.p2align 3
.L1073:
	movq	8(%r13), %rsi
	movq	16(%r13), %rax
	movabsq	$7905747460161236407, %rdx
	subq	%rsi, %rax
	sarq	$5, %rax
	imulq	%rdx, %rax
	cmpq	$1, %rax
	jbe	.L1067
	leaq	224(%rsi), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation34ProcessHintsForOrdinaryHasInstanceERKNS1_5HintsES5_
	jmp	.L1067
	.p2align 4,,10
	.p2align 3
.L1093:
	movq	(%r12), %rax
	movl	$56, %esi
	leaq	-288(%rbp), %r13
	movq	(%rax), %rdi
	addq	$41184, %rdi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	movq	(%r12), %rsi
	movq	%r13, %rdi
	movl	$1, %ecx
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler9ObjectRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	(%r12), %rsi
	movl	$1, %ecx
	movq	%r13, %rdi
	movq	(%rsi), %rax
	leaq	1032(%rax), %rdx
	call	_ZN2v88internal8compiler9ObjectRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	jmp	.L1067
	.p2align 4,,10
	.p2align 3
.L1099:
	movq	(%r12), %rax
	leaq	-288(%rbp), %r13
	movq	16(%r12), %rdx
	movq	%r13, %rdi
	movq	(%rax), %rsi
	addq	$88, %rsi
	call	_ZN2v88internal8compiler5Hints14SingleConstantENS0_6HandleINS0_6ObjectEEEPNS0_4ZoneE
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation33ProcessHintsForObjectGetPrototypeERKNS1_5HintsE
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	jmp	.L1067
	.p2align 4,,10
	.p2align 3
.L1096:
	movq	(%r12), %rax
	leaq	-288(%rbp), %r13
	movq	16(%r12), %rdx
	movq	%r13, %rdi
	movq	(%rax), %rsi
	addq	$88, %rsi
	call	_ZN2v88internal8compiler5Hints14SingleConstantENS0_6HandleINS0_6ObjectEEEPNS0_4ZoneE
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation27ProcessHintsForObjectCreateERKNS1_5HintsE
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	jmp	.L1067
	.p2align 4,,10
	.p2align 3
.L1144:
	movq	(%r8), %rax
	movq	-24(%rax), %rdi
	addq	%r8, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	movq	-296(%rbp), %r8
	jmp	.L1070
.L1140:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18999:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation18ProcessBuiltinCallENS0_6HandleINS0_18SharedFunctionInfoEEERKNS0_10ZoneVectorINS1_5HintsEEENS0_15SpeculationModeE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation18ProcessBuiltinCallENS0_6HandleINS0_18SharedFunctionInfoEEERKNS0_10ZoneVectorINS1_5HintsEEENS0_15SpeculationModeE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation28ProcessSFIForCallOrConstructENS0_6HandleINS0_18SharedFunctionInfoEEERKNS0_10ZoneVectorINS1_5HintsEEENS0_15SpeculationModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation28ProcessSFIForCallOrConstructENS0_6HandleINS0_18SharedFunctionInfoEEERKNS0_10ZoneVectorINS1_5HintsEEENS0_15SpeculationModeE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation28ProcessSFIForCallOrConstructENS0_6HandleINS0_18SharedFunctionInfoEEERKNS0_10ZoneVectorINS1_5HintsEEENS0_15SpeculationModeE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation28ProcessSFIForCallOrConstructENS0_6HandleINS0_18SharedFunctionInfoEEERKNS0_10ZoneVectorINS1_5HintsEEENS0_15SpeculationModeE:
.LFB18990:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	7(%rax), %rax
	testb	$1, %al
	jne	.L1146
.L1149:
	movq	(%rbx), %rax
	movq	7(%rax), %rax
	testb	$1, %al
	je	.L1152
.L1148:
	movq	(%rbx), %rax
	leaq	-32(%rbp), %rdi
	movq	%rax, -32(%rbp)
	call	_ZN2v88internal18SharedFunctionInfo12IsInlineableEv@PLT
	movq	-24(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L1153
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1146:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$88, 11(%rax)
	jne	.L1149
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation14ProcessApiCallENS0_6HandleINS0_18SharedFunctionInfoEEERKNS0_10ZoneVectorINS1_5HintsEEE
	jmp	.L1148
	.p2align 4,,10
	.p2align 3
.L1152:
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation18ProcessBuiltinCallENS0_6HandleINS0_18SharedFunctionInfoEEERKNS0_10ZoneVectorINS1_5HintsEEENS0_15SpeculationModeE
	jmp	.L1148
.L1153:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18990:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation28ProcessSFIForCallOrConstructENS0_6HandleINS0_18SharedFunctionInfoEEERKNS0_10ZoneVectorINS1_5HintsEEENS0_15SpeculationModeE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation28ProcessSFIForCallOrConstructENS0_6HandleINS0_18SharedFunctionInfoEEERKNS0_10ZoneVectorINS1_5HintsEEENS0_15SpeculationModeE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation31ProcessCalleeForCallOrConstructENS0_6HandleINS0_10JSFunctionEEERKNS0_10ZoneVectorINS1_5HintsEEENS0_15SpeculationModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation31ProcessCalleeForCallOrConstructENS0_6HandleINS0_10JSFunctionEEERKNS0_10ZoneVectorINS1_5HintsEEENS0_15SpeculationModeE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation31ProcessCalleeForCallOrConstructENS0_6HandleINS0_10JSFunctionEEERKNS0_10ZoneVectorINS1_5HintsEEENS0_15SpeculationModeE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation31ProcessCalleeForCallOrConstructENS0_6HandleINS0_10JSFunctionEEERKNS0_10ZoneVectorINS1_5HintsEEENS0_15SpeculationModeE:
.LFB18991:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r15
	movl	%ecx, %r14d
	movl	$1, %ecx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	movq	%rsi, %rdx
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	(%rdi), %rsi
	movq	%r15, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13JSFunctionRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13JSFunctionRef9SerializeEv@PLT
	movq	(%r12), %rax
	movq	(%rax), %r15
	movq	(%rbx), %rax
	movq	23(%rax), %r8
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L1155
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1156:
	movl	%r14d, %ecx
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation28ProcessSFIForCallOrConstructENS0_6HandleINS0_18SharedFunctionInfoEEERKNS0_10ZoneVectorINS1_5HintsEEENS0_15SpeculationModeE
	testb	%al, %al
	jne	.L1170
.L1154:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1171
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1155:
	.cfi_restore_state
	movq	41088(%r15), %rsi
	cmpq	41096(%r15), %rsi
	je	.L1172
.L1157:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r15)
	movq	%r8, (%rsi)
	jmp	.L1156
	.p2align 4,,10
	.p2align 3
.L1170:
	movabsq	$287762808832, %rsi
	movq	(%rbx), %rcx
	movq	23(%rcx), %rdx
	movq	7(%rdx), %rdx
	cmpq	%rsi, %rdx
	je	.L1162
	testb	$1, %dl
	jne	.L1160
.L1164:
	movq	39(%rcx), %rdx
	movq	7(%rdx), %rdx
	movq	-1(%rdx), %rdx
	cmpw	$155, 11(%rdx)
	je	.L1154
.L1162:
	xorl	%eax, %eax
	jmp	.L1154
	.p2align 4,,10
	.p2align 3
.L1160:
	movq	-1(%rdx), %rsi
	cmpw	$165, 11(%rsi)
	je	.L1162
	movq	-1(%rdx), %rdx
	cmpw	$166, 11(%rdx)
	jne	.L1164
	jmp	.L1162
	.p2align 4,,10
	.p2align 3
.L1172:
	movq	%r15, %rdi
	movq	%r8, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L1157
.L1171:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18991:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation31ProcessCalleeForCallOrConstructENS0_6HandleINS0_10JSFunctionEEERKNS0_10ZoneVectorINS1_5HintsEEENS0_15SpeculationModeE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation31ProcessCalleeForCallOrConstructENS0_6HandleINS0_10JSFunctionEEERKNS0_10ZoneVectorINS1_5HintsEEENS0_15SpeculationModeE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation24VisitGetSuperConstructorEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation24VisitGetSuperConstructorEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation24VisitGetSuperConstructorEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation24VisitGetSuperConstructorEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB18921:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rsi, %rdi
	xorl	%esi, %esi
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	24(%r12), %rdi
	movl	%eax, %esi
	movl	%eax, -132(%rbp)
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment14register_hintsENS0_11interpreter8RegisterE
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler5Hints5ClearEv
	movq	24(%r12), %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment17accumulator_hintsEv
	movq	88(%rax), %r13
	leaq	72(%rax), %rbx
	cmpq	%r13, %rbx
	je	.L1173
	leaq	-128(%rbp), %r14
	jmp	.L1180
	.p2align 4,,10
	.p2align 3
.L1176:
	movq	%r13, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r13
	cmpq	%rax, %rbx
	je	.L1173
.L1180:
	movq	32(%r13), %rax
	movq	(%rax), %rax
	testb	$1, %al
	je	.L1176
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L1176
	movq	(%r12), %rdx
	movq	(%rdx), %r15
	movq	-1(%rax), %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L1192
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L1181:
	movq	(%r12), %rsi
	movl	$1, %ecx
	movq	%r14, %rdi
	leaq	-112(%rbp), %r15
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler6MapRef18SerializePrototypeEv@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler6MapRef9prototypeEv@PLT
	movq	%r15, %rdi
	movq	%rax, -112(%rbp)
	movq	%rdx, -104(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L1176
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12AsHeapObjectEv@PLT
	leaq	-96(%rbp), %rdi
	movq	%rax, -96(%rbp)
	movq	%rdx, -88(%rbp)
	call	_ZNK2v88internal8compiler13HeapObjectRef3mapEv@PLT
	leaq	-80(%rbp), %r8
	movq	%r8, %rdi
	movq	%r8, -144(%rbp)
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZNK2v88internal8compiler6MapRef14is_constructorEv@PLT
	movq	-144(%rbp), %r8
	testb	%al, %al
	je	.L1176
	movl	-132(%rbp), %esi
	movq	24(%r12), %rdi
	movq	%r8, -152(%rbp)
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment14register_hintsENS0_11interpreter8RegisterE
	movq	%r15, %rdi
	movq	%rax, -144(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef6objectEv@PLT
	movq	-144(%rbp), %rdi
	movq	-152(%rbp), %r8
	movq	%rax, -80(%rbp)
	addq	$56, %rdi
	movq	%r8, %rsi
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_
	jmp	.L1176
	.p2align 4,,10
	.p2align 3
.L1173:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1193
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1192:
	.cfi_restore_state
	movq	41088(%r15), %rdx
	cmpq	41096(%r15), %rdx
	je	.L1194
.L1182:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%rdx)
	jmp	.L1181
.L1194:
	movq	%r15, %rdi
	movq	%rsi, -144(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-144(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L1182
.L1193:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18921:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation24VisitGetSuperConstructorEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation24VisitGetSuperConstructorEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation12VisitLdaTrueEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation12VisitLdaTrueEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation12VisitLdaTrueEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation12VisitLdaTrueEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB18923:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	24(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	252(%rdx), %eax
	addl	248(%rdx), %eax
	movq	936(%rdx), %rcx
	movslq	%eax, %rsi
	js	.L1196
	movq	944(%rdx), %rax
	movabsq	$7905747460161236407, %rdx
	subq	%rcx, %rax
	sarq	$5, %rax
	imulq	%rdx, %rax
	cmpq	%rsi, %rax
	jbe	.L1199
.L1196:
	leaq	0(,%rsi,8), %rdi
	subq	%rsi, %rdi
	salq	$5, %rdi
	addq	%rcx, %rdi
	call	_ZN2v88internal8compiler5Hints5ClearEv
	movq	24(%rbx), %rdx
	movl	252(%rdx), %eax
	addl	248(%rdx), %eax
	movq	936(%rdx), %rcx
	cltq
	js	.L1198
	movq	944(%rdx), %rdx
	movabsq	$7905747460161236407, %rsi
	subq	%rcx, %rdx
	sarq	$5, %rdx
	imulq	%rsi, %rdx
	cmpq	%rax, %rdx
	jbe	.L1199
.L1198:
	movq	(%rbx), %rdx
	leaq	-32(%rbp), %rsi
	movq	(%rdx), %rdx
	addq	$112, %rdx
	movq	%rdx, -32(%rbp)
	leaq	0(,%rax,8), %rdx
	subq	%rax, %rdx
	movq	%rdx, %rax
	salq	$5, %rax
	leaq	56(%rcx,%rax), %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1202
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1199:
	.cfi_restore_state
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1202:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18923:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation12VisitLdaTrueEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation12VisitLdaTrueEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation13VisitLdaFalseEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation13VisitLdaFalseEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation13VisitLdaFalseEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation13VisitLdaFalseEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB18924:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	24(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	252(%rdx), %eax
	addl	248(%rdx), %eax
	movq	936(%rdx), %rcx
	movslq	%eax, %rsi
	js	.L1204
	movq	944(%rdx), %rax
	movabsq	$7905747460161236407, %rdx
	subq	%rcx, %rax
	sarq	$5, %rax
	imulq	%rdx, %rax
	cmpq	%rsi, %rax
	jbe	.L1207
.L1204:
	leaq	0(,%rsi,8), %rdi
	subq	%rsi, %rdi
	salq	$5, %rdi
	addq	%rcx, %rdi
	call	_ZN2v88internal8compiler5Hints5ClearEv
	movq	24(%rbx), %rdx
	movl	252(%rdx), %eax
	addl	248(%rdx), %eax
	movq	936(%rdx), %rcx
	cltq
	js	.L1206
	movq	944(%rdx), %rdx
	movabsq	$7905747460161236407, %rsi
	subq	%rcx, %rdx
	sarq	$5, %rdx
	imulq	%rsi, %rdx
	cmpq	%rax, %rdx
	jbe	.L1207
.L1206:
	movq	(%rbx), %rdx
	leaq	-32(%rbp), %rsi
	movq	(%rdx), %rdx
	addq	$120, %rdx
	movq	%rdx, -32(%rbp)
	leaq	0(,%rax,8), %rdx
	subq	%rax, %rdx
	movq	%rdx, %rax
	salq	$5, %rax
	leaq	56(%rcx,%rax), %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1210
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1207:
	.cfi_restore_state
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1210:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18924:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation13VisitLdaFalseEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation13VisitLdaFalseEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation15VisitLdaTheHoleEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation15VisitLdaTheHoleEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation15VisitLdaTheHoleEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation15VisitLdaTheHoleEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB18925:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	24(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	252(%rdx), %eax
	addl	248(%rdx), %eax
	movq	936(%rdx), %rcx
	movslq	%eax, %rsi
	js	.L1212
	movq	944(%rdx), %rax
	movabsq	$7905747460161236407, %rdx
	subq	%rcx, %rax
	sarq	$5, %rax
	imulq	%rdx, %rax
	cmpq	%rsi, %rax
	jbe	.L1215
.L1212:
	leaq	0(,%rsi,8), %rdi
	subq	%rsi, %rdi
	salq	$5, %rdi
	addq	%rcx, %rdi
	call	_ZN2v88internal8compiler5Hints5ClearEv
	movq	24(%rbx), %rdx
	movl	252(%rdx), %eax
	addl	248(%rdx), %eax
	movq	936(%rdx), %rcx
	cltq
	js	.L1214
	movq	944(%rdx), %rdx
	movabsq	$7905747460161236407, %rsi
	subq	%rcx, %rdx
	sarq	$5, %rdx
	imulq	%rsi, %rdx
	cmpq	%rax, %rdx
	jbe	.L1215
.L1214:
	movq	(%rbx), %rdx
	leaq	-32(%rbp), %rsi
	movq	(%rdx), %rdx
	addq	$96, %rdx
	movq	%rdx, -32(%rbp)
	leaq	0(,%rax,8), %rdx
	subq	%rax, %rdx
	movq	%rdx, %rax
	salq	$5, %rax
	leaq	56(%rcx,%rax), %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1218
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1215:
	.cfi_restore_state
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1218:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18925:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation15VisitLdaTheHoleEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation15VisitLdaTheHoleEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation17VisitLdaUndefinedEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation17VisitLdaUndefinedEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation17VisitLdaUndefinedEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation17VisitLdaUndefinedEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB18926:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	24(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	252(%rdx), %eax
	addl	248(%rdx), %eax
	movq	936(%rdx), %rcx
	movslq	%eax, %rsi
	js	.L1220
	movq	944(%rdx), %rax
	movabsq	$7905747460161236407, %rdx
	subq	%rcx, %rax
	sarq	$5, %rax
	imulq	%rdx, %rax
	cmpq	%rsi, %rax
	jbe	.L1223
.L1220:
	leaq	0(,%rsi,8), %rdi
	subq	%rsi, %rdi
	salq	$5, %rdi
	addq	%rcx, %rdi
	call	_ZN2v88internal8compiler5Hints5ClearEv
	movq	24(%rbx), %rdx
	movl	252(%rdx), %eax
	addl	248(%rdx), %eax
	movq	936(%rdx), %rcx
	cltq
	js	.L1222
	movq	944(%rdx), %rdx
	movabsq	$7905747460161236407, %rsi
	subq	%rcx, %rdx
	sarq	$5, %rdx
	imulq	%rsi, %rdx
	cmpq	%rax, %rdx
	jbe	.L1223
.L1222:
	movq	(%rbx), %rdx
	leaq	-32(%rbp), %rsi
	movq	(%rdx), %rdx
	addq	$88, %rdx
	movq	%rdx, -32(%rbp)
	leaq	0(,%rax,8), %rdx
	subq	%rax, %rdx
	movq	%rdx, %rax
	salq	$5, %rax
	leaq	56(%rcx,%rax), %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1226
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1223:
	.cfi_restore_state
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1226:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18926:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation17VisitLdaUndefinedEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation17VisitLdaUndefinedEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation12VisitLdaNullEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation12VisitLdaNullEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation12VisitLdaNullEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation12VisitLdaNullEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB18927:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	24(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	252(%rdx), %eax
	addl	248(%rdx), %eax
	movq	936(%rdx), %rcx
	movslq	%eax, %rsi
	js	.L1228
	movq	944(%rdx), %rax
	movabsq	$7905747460161236407, %rdx
	subq	%rcx, %rax
	sarq	$5, %rax
	imulq	%rdx, %rax
	cmpq	%rsi, %rax
	jbe	.L1231
.L1228:
	leaq	0(,%rsi,8), %rdi
	subq	%rsi, %rdi
	salq	$5, %rdi
	addq	%rcx, %rdi
	call	_ZN2v88internal8compiler5Hints5ClearEv
	movq	24(%rbx), %rdx
	movl	252(%rdx), %eax
	addl	248(%rdx), %eax
	movq	936(%rdx), %rcx
	cltq
	js	.L1230
	movq	944(%rdx), %rdx
	movabsq	$7905747460161236407, %rsi
	subq	%rcx, %rdx
	sarq	$5, %rdx
	imulq	%rsi, %rdx
	cmpq	%rax, %rdx
	jbe	.L1231
.L1230:
	movq	(%rbx), %rdx
	leaq	-32(%rbp), %rsi
	movq	(%rdx), %rdx
	addq	$104, %rdx
	movq	%rdx, -32(%rbp)
	leaq	0(,%rax,8), %rdx
	subq	%rax, %rdx
	movq	%rdx, %rax
	salq	$5, %rax
	leaq	56(%rcx,%rax), %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1234
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1231:
	.cfi_restore_state
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1234:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18927:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation12VisitLdaNullEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation12VisitLdaNullEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation12VisitLdaZeroEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation12VisitLdaZeroEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation12VisitLdaZeroEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation12VisitLdaZeroEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB18928:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	24(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	252(%rdx), %eax
	addl	248(%rdx), %eax
	movq	936(%rdx), %rcx
	movslq	%eax, %rsi
	js	.L1236
	movq	944(%rdx), %rax
	movabsq	$7905747460161236407, %rdx
	subq	%rcx, %rax
	sarq	$5, %rax
	imulq	%rdx, %rax
	cmpq	%rsi, %rax
	jbe	.L1239
.L1236:
	leaq	0(,%rsi,8), %rdi
	subq	%rsi, %rdi
	salq	$5, %rdi
	addq	%rcx, %rdi
	call	_ZN2v88internal8compiler5Hints5ClearEv
	movq	24(%rbx), %rdx
	movl	252(%rdx), %eax
	addl	248(%rdx), %eax
	movq	936(%rdx), %rdi
	cltq
	js	.L1238
	movq	944(%rdx), %rdx
	movabsq	$7905747460161236407, %rcx
	subq	%rdi, %rdx
	sarq	$5, %rdx
	imulq	%rcx, %rdx
	cmpq	%rax, %rdx
	jbe	.L1239
.L1238:
	leaq	0(,%rax,8), %r12
	subq	%rax, %r12
	movq	(%rbx), %rax
	salq	$5, %r12
	movq	(%rax), %rbx
	addq	%rdi, %r12
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1240
	xorl	%esi, %esi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L1241:
	leaq	-32(%rbp), %rsi
	leaq	56(%r12), %rdi
	movq	%rax, -32(%rbp)
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1245
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1239:
	.cfi_restore_state
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1240:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L1246
.L1242:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	$0, (%rax)
	jmp	.L1241
	.p2align 4,,10
	.p2align 3
.L1246:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L1242
.L1245:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18928:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation12VisitLdaZeroEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation12VisitLdaZeroEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitLdaSmiEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitLdaSmiEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitLdaSmiEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitLdaSmiEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB18929:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	24(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	252(%rdx), %eax
	addl	248(%rdx), %eax
	movq	936(%rdx), %rcx
	movslq	%eax, %rsi
	js	.L1248
	movq	944(%rdx), %rax
	movabsq	$7905747460161236407, %rdx
	subq	%rcx, %rax
	sarq	$5, %rax
	imulq	%rdx, %rax
	cmpq	%rsi, %rax
	jbe	.L1251
.L1248:
	leaq	0(,%rsi,8), %rdi
	subq	%rsi, %rdi
	salq	$5, %rdi
	addq	%rcx, %rdi
	call	_ZN2v88internal8compiler5Hints5ClearEv
	movq	24(%rbx), %rdx
	movl	252(%rdx), %eax
	addl	248(%rdx), %eax
	movq	936(%rdx), %rdi
	cltq
	js	.L1250
	movq	944(%rdx), %rdx
	movabsq	$7905747460161236407, %rcx
	subq	%rdi, %rdx
	sarq	$5, %rdx
	imulq	%rcx, %rdx
	cmpq	%rax, %rdx
	jbe	.L1251
.L1250:
	leaq	0(,%rax,8), %r12
	xorl	%esi, %esi
	subq	%rax, %r12
	movq	(%rbx), %rax
	salq	$5, %r12
	movq	(%rax), %rbx
	addq	%rdi, %r12
	movq	%r13, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor19GetImmediateOperandEi@PLT
	movq	41112(%rbx), %rdi
	movq	%rax, %rsi
	salq	$32, %rsi
	testq	%rdi, %rdi
	je	.L1252
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L1253:
	leaq	-48(%rbp), %rsi
	leaq	56(%r12), %rdi
	movq	%rax, -48(%rbp)
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1257
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1251:
	.cfi_restore_state
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1252:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L1258
.L1254:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L1253
	.p2align 4,,10
	.p2align 3
.L1258:
	movq	%rbx, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1254
.L1257:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18929:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitLdaSmiEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitLdaSmiEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation16VisitLdaConstantEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation16VisitLdaConstantEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation16VisitLdaConstantEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation16VisitLdaConstantEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB18931:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	xorl	%esi, %esi
	leaq	-48(%rbp), %r12
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rbx), %rax
	movq	(%rax), %rdx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor26GetConstantForIndexOperandEiPNS0_7IsolateE@PLT
	movq	(%rbx), %rsi
	movl	$1, %ecx
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler9ObjectRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	24(%rbx), %rdx
	movl	252(%rdx), %eax
	addl	248(%rdx), %eax
	movq	936(%rdx), %rcx
	cltq
	js	.L1261
	movq	944(%rdx), %rdx
	movabsq	$7905747460161236407, %rsi
	subq	%rcx, %rdx
	sarq	$5, %rdx
	imulq	%rsi, %rdx
	cmpq	%rax, %rdx
	jbe	.L1264
.L1261:
	leaq	0(,%rax,8), %rdi
	subq	%rax, %rdi
	salq	$5, %rdi
	addq	%rcx, %rdi
	call	_ZN2v88internal8compiler5Hints5ClearEv
	movq	24(%rbx), %rdx
	movl	252(%rdx), %eax
	addl	248(%rdx), %eax
	movq	936(%rdx), %rdi
	cltq
	js	.L1263
	movq	944(%rdx), %rdx
	movabsq	$7905747460161236407, %rcx
	subq	%rdi, %rdx
	sarq	$5, %rdx
	imulq	%rcx, %rdx
	cmpq	%rax, %rdx
	jbe	.L1264
.L1263:
	leaq	0(,%rax,8), %rbx
	subq	%rax, %rbx
	salq	$5, %rbx
	addq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef6objectEv@PLT
	leaq	-56(%rbp), %rsi
	leaq	56(%rbx), %rdi
	movq	%rax, -56(%rbp)
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1267
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1264:
	.cfi_restore_state
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1267:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18931:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation16VisitLdaConstantEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation16VisitLdaConstantEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation20ProcessImmutableLoadERKNS1_10ContextRefEiNS2_21ContextProcessingModeEPNS1_5HintsE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation20ProcessImmutableLoadERKNS1_10ContextRefEiNS2_21ContextProcessingModeEPNS1_5HintsE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation20ProcessImmutableLoadERKNS1_10ContextRefEiNS2_21ContextProcessingModeEPNS1_5HintsE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation20ProcessImmutableLoadERKNS1_10ContextRefEiNS2_21ContextProcessingModeEPNS1_5HintsE:
.LFB18934:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%r8, %rbx
	leaq	-48(%rbp), %rdi
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal8compiler10ContextRef3getEiNS1_19SerializationPolicyE@PLT
	testq	%rbx, %rbx
	je	.L1268
	cmpb	$0, -48(%rbp)
	jne	.L1275
.L1268:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1276
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1275:
	.cfi_restore_state
	leaq	-40(%rbp), %rdi
	call	_ZNK2v88internal8compiler9ObjectRef6objectEv@PLT
	leaq	-56(%rbp), %rsi
	leaq	56(%rbx), %rdi
	movq	%rax, -56(%rbp)
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_
	jmp	.L1268
.L1276:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18934:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation20ProcessImmutableLoadERKNS1_10ContextRefEiNS2_21ContextProcessingModeEPNS1_5HintsE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation20ProcessImmutableLoadERKNS1_10ContextRefEiNS2_21ContextProcessingModeEPNS1_5HintsE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation20ProcessContextAccessERKNS1_5HintsEiiNS2_21ContextProcessingModeEPS3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation20ProcessContextAccessERKNS1_5HintsEiiNS2_21ContextProcessingModeEPS3_
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation20ProcessContextAccessERKNS1_5HintsEiiNS2_21ContextProcessingModeEPS3_, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation20ProcessContextAccessERKNS1_5HintsEiiNS2_21ContextProcessingModeEPS3_:
.LFB18936:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r15
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	72(%rsi), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%ecx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$88, %rsp
	movq	%r9, -120(%rbp)
	movq	88(%rsi), %r9
	movl	%edx, -112(%rbp)
	movl	%r8d, -108(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	%r9, %r13
	jne	.L1278
	jmp	.L1285
	.p2align 4,,10
	.p2align 3
.L1282:
	movq	%r9, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r9
	cmpq	%rax, %r13
	je	.L1285
.L1278:
	movq	32(%r9), %rdx
	movq	(%rdx), %rax
	testb	$1, %al
	je	.L1282
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	subw	$138, %ax
	cmpw	$9, %ax
	ja	.L1282
	movq	(%r14), %rsi
	movl	$1, %ecx
	movq	%r15, %rdi
	movq	%r9, -104(%rbp)
	call	_ZN2v88internal8compiler10ContextRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movslq	%r12d, %rax
	leaq	-88(%rbp), %rsi
	movq	%r15, %rdi
	movl	$1, %edx
	movq	%rax, -88(%rbp)
	call	_ZNK2v88internal8compiler10ContextRef8previousEPmNS1_19SerializationPolicyE@PLT
	cmpq	$0, -88(%rbp)
	movq	-104(%rbp), %r9
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	jne	.L1282
	movl	-108(%rbp), %edx
	testl	%edx, %edx
	je	.L1282
	movq	-120(%rbp), %r8
	movl	-108(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movl	-112(%rbp), %edx
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation20ProcessImmutableLoadERKNS1_10ContextRefEiNS2_21ContextProcessingModeEPNS1_5HintsE
	movq	-104(%rbp), %r9
	movq	%r9, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r9
	cmpq	%rax, %r13
	jne	.L1278
	.p2align 4,,10
	.p2align 3
.L1285:
	movq	32(%rbx), %r15
	addq	$16, %rbx
	leaq	-80(%rbp), %r13
	cmpq	%r15, %rbx
	jne	.L1288
	jmp	.L1277
	.p2align 4,,10
	.p2align 3
.L1286:
	movq	%r15, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r15
	cmpq	%rax, %rbx
	je	.L1277
.L1288:
	movl	32(%r15), %eax
	cmpl	%r12d, %eax
	ja	.L1286
	movq	40(%r15), %rdx
	movq	(%r14), %rsi
	movl	$1, %ecx
	movq	%r13, %rdi
	movl	%eax, -104(%rbp)
	call	_ZN2v88internal8compiler10ContextRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movl	-104(%rbp), %eax
	movl	%r12d, %ecx
	leaq	-88(%rbp), %rsi
	movl	$1, %edx
	movq	%r13, %rdi
	subl	%eax, %ecx
	movl	%ecx, %eax
	movq	%rax, -88(%rbp)
	call	_ZNK2v88internal8compiler10ContextRef8previousEPmNS1_19SerializationPolicyE@PLT
	cmpq	$0, -88(%rbp)
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	jne	.L1286
	movl	-108(%rbp), %eax
	testl	%eax, %eax
	je	.L1286
	movq	-120(%rbp), %r8
	movl	-108(%rbp), %ecx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movl	-112(%rbp), %edx
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation20ProcessImmutableLoadERKNS1_10ContextRefEiNS2_21ContextProcessingModeEPNS1_5HintsE
	movq	%r15, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r15
	cmpq	%rax, %rbx
	jne	.L1288
	.p2align 4,,10
	.p2align 3
.L1277:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1308
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1308:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18936:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation20ProcessContextAccessERKNS1_5HintsEiiNS2_21ContextProcessingModeEPS3_, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation20ProcessContextAccessERKNS1_5HintsEiiNS2_21ContextProcessingModeEPS3_
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation19VisitStaContextSlotEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation19VisitStaContextSlotEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation19VisitStaContextSlotEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation19VisitStaContextSlotEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB18945:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movl	$1, %esi
	pushq	%rbx
	movq	%r12, %rdi
	subq	$8, %rsp
	.cfi_offset 3, -56
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor15GetIndexOperandEi@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	movl	%eax, %r14d
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor27GetUnsignedImmediateOperandEi@PLT
	movq	24(%r13), %rbx
	movq	%r12, %rdi
	xorl	%esi, %esi
	movl	%eax, %r15d
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%rbx, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment14register_hintsENS0_11interpreter8RegisterE
	addq	$8, %rsp
	movl	%r15d, %ecx
	movl	%r14d, %edx
	popq	%rbx
	movq	%r13, %rdi
	popq	%r12
	movq	%rax, %rsi
	popq	%r13
	xorl	%r9d, %r9d
	popq	%r14
	xorl	%r8d, %r8d
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler34SerializerForBackgroundCompilation20ProcessContextAccessERKNS1_5HintsEiiNS2_21ContextProcessingModeEPS3_
	.cfi_endproc
.LFE18945:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation19VisitStaContextSlotEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation19VisitStaContextSlotEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation26VisitStaCurrentContextSlotEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation26VisitStaCurrentContextSlotEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation26VisitStaCurrentContextSlotEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation26VisitStaCurrentContextSlotEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB18946:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	xorl	%esi, %esi
	subq	$8, %rsp
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor15GetIndexOperandEi@PLT
	movq	%r12, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	%eax, %edx
	movq	24(%r12), %rax
	addq	$8, %rsp
	xorl	%ecx, %ecx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	480(%rax), %rsi
	jmp	_ZN2v88internal8compiler34SerializerForBackgroundCompilation20ProcessContextAccessERKNS1_5HintsEiiNS2_21ContextProcessingModeEPS3_
	.cfi_endproc
.LFE18946:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation26VisitStaCurrentContextSlotEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation26VisitStaCurrentContextSlotEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation20ProcessContextAccessERKNS1_5HintsEiiNS2_21ContextProcessingModeEPS3_.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation20ProcessContextAccessERKNS1_5HintsEiiNS2_21ContextProcessingModeEPS3_.constprop.0, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation20ProcessContextAccessERKNS1_5HintsEiiNS2_21ContextProcessingModeEPS3_.constprop.0:
.LFB25738:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r15
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	72(%rsi), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%ecx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$88, %rsp
	movl	%edx, -108(%rbp)
	movq	88(%rsi), %r9
	movq	%r8, -120(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	%r13, %r9
	jne	.L1314
	jmp	.L1321
	.p2align 4,,10
	.p2align 3
.L1318:
	movq	%r9, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r9
	cmpq	%rax, %r13
	je	.L1321
.L1314:
	movq	32(%r9), %rdx
	movq	(%rdx), %rax
	testb	$1, %al
	je	.L1318
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	subw	$138, %ax
	cmpw	$9, %ax
	ja	.L1318
	movq	(%r14), %rsi
	movl	$1, %ecx
	movq	%r15, %rdi
	movq	%r9, -104(%rbp)
	call	_ZN2v88internal8compiler10ContextRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movslq	%r12d, %rax
	leaq	-88(%rbp), %rsi
	movq	%r15, %rdi
	movl	$1, %edx
	movq	%rax, -88(%rbp)
	call	_ZNK2v88internal8compiler10ContextRef8previousEPmNS1_19SerializationPolicyE@PLT
	cmpq	$0, -88(%rbp)
	movq	-104(%rbp), %r9
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	jne	.L1318
	movq	-120(%rbp), %r8
	movl	-108(%rbp), %edx
	movl	$1, %ecx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation20ProcessImmutableLoadERKNS1_10ContextRefEiNS2_21ContextProcessingModeEPNS1_5HintsE
	movq	-104(%rbp), %r9
	movq	%r9, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r9
	cmpq	%rax, %r13
	jne	.L1314
	.p2align 4,,10
	.p2align 3
.L1321:
	movq	32(%rbx), %r15
	addq	$16, %rbx
	leaq	-80(%rbp), %r13
	cmpq	%rbx, %r15
	jne	.L1324
	jmp	.L1313
	.p2align 4,,10
	.p2align 3
.L1322:
	movq	%r15, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r15
	cmpq	%rax, %rbx
	je	.L1313
.L1324:
	movl	32(%r15), %eax
	cmpl	%r12d, %eax
	ja	.L1322
	movq	40(%r15), %rdx
	movq	(%r14), %rsi
	movl	$1, %ecx
	movq	%r13, %rdi
	movl	%eax, -104(%rbp)
	call	_ZN2v88internal8compiler10ContextRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movl	-104(%rbp), %eax
	movl	%r12d, %ecx
	leaq	-88(%rbp), %rsi
	movl	$1, %edx
	movq	%r13, %rdi
	subl	%eax, %ecx
	movl	%ecx, %eax
	movq	%rax, -88(%rbp)
	call	_ZNK2v88internal8compiler10ContextRef8previousEPmNS1_19SerializationPolicyE@PLT
	cmpq	$0, -88(%rbp)
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	jne	.L1322
	movq	-120(%rbp), %r8
	movl	-108(%rbp), %edx
	movl	$1, %ecx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation20ProcessImmutableLoadERKNS1_10ContextRefEiNS2_21ContextProcessingModeEPNS1_5HintsE
	movq	%r15, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r15
	cmpq	%rax, %rbx
	jne	.L1324
	.p2align 4,,10
	.p2align 3
.L1313:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1332
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1332:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25738:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation20ProcessContextAccessERKNS1_5HintsEiiNS2_21ContextProcessingModeEPS3_.constprop.0, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation20ProcessContextAccessERKNS1_5HintsEiiNS2_21ContextProcessingModeEPS3_.constprop.0
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation27ProcessModuleVariableAccessEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation27ProcessModuleVariableAccessEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation27ProcessModuleVariableAccessEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation27ProcessModuleVariableAccessEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB18941:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-216(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rsi, %rdi
	movl	$1, %esi
	pushq	%rbx
	subq	$296, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor27GetUnsignedImmediateOperandEi@PLT
	movq	16(%r12), %rdx
	movq	%r12, %rdi
	leaq	-288(%rbp), %r8
	movl	%eax, %ecx
	movq	24(%r12), %rax
	movl	$0, -272(%rbp)
	movq	%rdx, -288(%rbp)
	leaq	480(%rax), %rsi
	leaq	-272(%rbp), %rax
	movq	%rdx, -232(%rbp)
	movq	%rax, -256(%rbp)
	movq	%rax, -248(%rbp)
	leaq	-160(%rbp), %rax
	movq	%rdx, -176(%rbp)
	movq	%rax, -144(%rbp)
	movq	%rax, -136(%rbp)
	leaq	-104(%rbp), %rax
	movq	%rdx, -120(%rbp)
	movl	$2, %edx
	movq	$0, -264(%rbp)
	movq	%r8, -328(%rbp)
	movq	$0, -240(%rbp)
	movl	$0, -216(%rbp)
	movq	$0, -208(%rbp)
	movq	%r13, -200(%rbp)
	movq	%r13, -192(%rbp)
	movq	$0, -184(%rbp)
	movl	$0, -160(%rbp)
	movq	$0, -152(%rbp)
	movq	$0, -128(%rbp)
	movl	$0, -104(%rbp)
	movq	$0, -96(%rbp)
	movq	%rax, -88(%rbp)
	movq	%rax, -80(%rbp)
	movq	$0, -72(%rbp)
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation20ProcessContextAccessERKNS1_5HintsEiiNS2_21ContextProcessingModeEPS3_.constprop.0
	movq	-200(%rbp), %r15
	cmpq	%r13, %r15
	je	.L1334
	leaq	-320(%rbp), %rbx
	leaq	-304(%rbp), %r14
	jmp	.L1337
	.p2align 4,,10
	.p2align 3
.L1335:
	movq	%r15, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r15
	cmpq	%r13, %rax
	je	.L1334
.L1337:
	movq	32(%r15), %rdx
	movq	(%r12), %rsi
	movq	%rbx, %rdi
	movl	$1, %ecx
	call	_ZN2v88internal8compiler9ObjectRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef18IsSourceTextModuleEv@PLT
	testb	%al, %al
	je	.L1335
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef18AsSourceTextModuleEv@PLT
	movq	%r14, %rdi
	movq	%rax, -304(%rbp)
	movq	%rdx, -296(%rbp)
	call	_ZN2v88internal8compiler19SourceTextModuleRef9SerializeEv@PLT
	movq	%r15, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r15
	cmpq	%r13, %rax
	jne	.L1337
.L1334:
	movq	-328(%rbp), %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1340
	addq	$296, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1340:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18941:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation27ProcessModuleVariableAccessEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation27ProcessModuleVariableAccessEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation22VisitLdaModuleVariableEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation22VisitLdaModuleVariableEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation22VisitLdaModuleVariableEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation22VisitLdaModuleVariableEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB18942:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal8compiler34SerializerForBackgroundCompilation27ProcessModuleVariableAccessEPNS0_11interpreter21BytecodeArrayIteratorE
	.cfi_endproc
.LFE18942:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation22VisitLdaModuleVariableEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation22VisitLdaModuleVariableEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation22VisitStaModuleVariableEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation22VisitStaModuleVariableEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation22VisitStaModuleVariableEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation22VisitStaModuleVariableEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25559:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal8compiler34SerializerForBackgroundCompilation27ProcessModuleVariableAccessEPNS0_11interpreter21BytecodeArrayIteratorE
	.cfi_endproc
.LFE25559:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation22VisitStaModuleVariableEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation22VisitStaModuleVariableEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation29ProcessCheckContextExtensionsEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation29ProcessCheckContextExtensionsEi
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation29ProcessCheckContextExtensionsEi, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation29ProcessCheckContextExtensionsEi:
.LFB19059:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	testl	%esi, %esi
	jle	.L1348
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	480(%rax), %r14
	pushq	%r13
	.cfi_offset 13, -32
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L1345:
	movl	%ebx, %ecx
	xorl	%r8d, %r8d
	movl	$2, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	addl	$1, %ebx
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation20ProcessContextAccessERKNS1_5HintsEiiNS2_21ContextProcessingModeEPS3_.constprop.0
	cmpl	%ebx, %r13d
	jne	.L1345
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1348:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	ret
	.cfi_endproc
.LFE19059:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation29ProcessCheckContextExtensionsEi, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation29ProcessCheckContextExtensionsEi
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation27ProcessLdaLookupContextSlotEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation27ProcessLdaLookupContextSlotEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation27ProcessLdaLookupContextSlotEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation27ProcessLdaLookupContextSlotEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB19064:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movl	$1, %esi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor15GetIndexOperandEi@PLT
	movl	$2, %esi
	movq	%r13, %rdi
	movl	%eax, %r15d
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor27GetUnsignedImmediateOperandEi@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	movl	%eax, %r14d
	movq	(%r12), %rax
	movq	(%rax), %rdx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor26GetConstantForIndexOperandEiPNS0_7IsolateE@PLT
	movq	(%r12), %rsi
	leaq	-80(%rbp), %rdi
	movl	$1, %ecx
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler7NameRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	24(%r12), %rax
	testl	%r14d, %r14d
	jle	.L1352
	leaq	480(%rax), %rbx
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L1353:
	movl	%r13d, %ecx
	xorl	%r8d, %r8d
	movl	$2, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	addl	$1, %r13d
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation20ProcessContextAccessERKNS1_5HintsEiiNS2_21ContextProcessingModeEPS3_.constprop.0
	cmpl	%r13d, %r14d
	jne	.L1353
	movq	24(%r12), %rax
.L1352:
	movl	252(%rax), %edx
	addl	248(%rax), %edx
	movq	936(%rax), %rcx
	movslq	%edx, %rsi
	js	.L1354
	movq	944(%rax), %rax
	movabsq	$7905747460161236407, %rdx
	subq	%rcx, %rax
	sarq	$5, %rax
	imulq	%rdx, %rax
	cmpq	%rsi, %rax
	jbe	.L1358
.L1354:
	leaq	0(,%rsi,8), %rdi
	subq	%rsi, %rdi
	salq	$5, %rdi
	addq	%rcx, %rdi
	call	_ZN2v88internal8compiler5Hints5ClearEv
	movq	24(%r12), %rax
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	%r14d, %ecx
	movl	%r15d, %edx
	movq	%r12, %rdi
	leaq	480(%rax), %rsi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation20ProcessContextAccessERKNS1_5HintsEiiNS2_21ContextProcessingModeEPS3_
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1359
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1358:
	.cfi_restore_state
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1359:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19064:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation27ProcessLdaLookupContextSlotEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation27ProcessLdaLookupContextSlotEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation25VisitLdaLookupContextSlotEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation25VisitLdaLookupContextSlotEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation25VisitLdaLookupContextSlotEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation25VisitLdaLookupContextSlotEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB19065:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal8compiler34SerializerForBackgroundCompilation27ProcessLdaLookupContextSlotEPNS0_11interpreter21BytecodeArrayIteratorE
	.cfi_endproc
.LFE19065:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation25VisitLdaLookupContextSlotEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation25VisitLdaLookupContextSlotEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation37VisitLdaLookupContextSlotInsideTypeofEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation37VisitLdaLookupContextSlotInsideTypeofEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation37VisitLdaLookupContextSlotInsideTypeofEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation37VisitLdaLookupContextSlotInsideTypeofEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25561:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal8compiler34SerializerForBackgroundCompilation27ProcessLdaLookupContextSlotEPNS0_11interpreter21BytecodeArrayIteratorE
	.cfi_endproc
.LFE25561:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation37VisitLdaLookupContextSlotInsideTypeofEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation37VisitLdaLookupContextSlotInsideTypeofEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.rodata._ZN2v88internal8compiler34SerializerForBackgroundCompilation32ProcessMapForNamedPropertyAccessENS1_6MapRefERKNS1_7NameRefENS1_10AccessModeENS_4base8OptionalINS1_11JSObjectRefEEEPNS1_5HintsE.str1.8,"aMS",@progbits,1
	.align 8
.LC47:
	.string	"receiver.has_value() implies receiver->map().equals(receiver_map)"
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation32ProcessMapForNamedPropertyAccessENS1_6MapRefERKNS1_7NameRefENS1_10AccessModeENS_4base8OptionalINS1_11JSObjectRefEEEPNS1_5HintsE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation32ProcessMapForNamedPropertyAccessENS1_6MapRefERKNS1_7NameRefENS1_10AccessModeENS_4base8OptionalINS1_11JSObjectRefEEEPNS1_5HintsE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation32ProcessMapForNamedPropertyAccessENS1_6MapRefERKNS1_7NameRefENS1_10AccessModeENS_4base8OptionalINS1_11JSObjectRefEEEPNS1_5HintsE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation32ProcessMapForNamedPropertyAccessENS1_6MapRefERKNS1_7NameRefENS1_10AccessModeENS_4base8OptionalINS1_11JSObjectRefEEEPNS1_5HintsE:
.LFB19071:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-144(%rbp), %r14
	pushq	%r13
	movq	%r14, %rdi
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%r9d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$136, %rsp
	movq	40(%rbp), %rax
	movq	%rdx, -144(%rbp)
	movq	%rcx, -136(%rbp)
	movq	%rax, -152(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler6MapRef16SerializeRootMapEv@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler6MapRef24IsMapOfTargetGlobalProxyEv@PLT
	testb	%al, %al
	je	.L1363
	movq	(%rbx), %rax
	cmpb	$0, 24(%rax)
	je	.L1407
	movdqu	32(%rax), %xmm1
	leaq	-128(%rbp), %rdi
	movaps	%xmm1, -128(%rbp)
	call	_ZNK2v88internal8compiler16NativeContextRef19global_proxy_objectEv@PLT
	leaq	-80(%rbp), %rdi
	leaq	-112(%rbp), %rsi
	movl	$1, %ecx
	movq	%rdx, -104(%rbp)
	movq	%r13, %rdx
	movq	%rax, -112(%rbp)
	call	_ZNK2v88internal8compiler16JSGlobalProxyRef15GetPropertyCellERKNS1_7NameRefENS1_19SerializationPolicyE@PLT
.L1363:
	subq	$8, %rsp
	movq	-144(%rbp), %rdx
	movq	(%rbx), %rsi
	movq	%r15, %rdi
	pushq	$1
	movq	-136(%rbp), %rcx
	pushq	8(%rbx)
	pushq	%r12
	movq	0(%r13), %r8
	movq	8(%r13), %r9
	call	_ZN2v88internal8compiler12JSHeapBroker21GetPropertyAccessInfoENS1_6MapRefENS1_7NameRefENS1_10AccessModeEPNS1_23CompilationDependenciesENS1_19SerializationPolicyE@PLT
	addq	$32, %rsp
	cmpl	$4, (%r15)
	je	.L1408
.L1365:
	testl	%r12d, %r12d
	je	.L1409
.L1362:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1410
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1409:
	.cfi_restore_state
	cmpl	$3, (%r15)
	jne	.L1362
	movq	88(%r15), %rdx
	pxor	%xmm0, %xmm0
	movq	$0, -96(%rbp)
	movaps	%xmm0, -112(%rbp)
	testq	%rdx, %rdx
	je	.L1411
	movq	(%rbx), %rsi
	leaq	-80(%rbp), %r13
	movl	$1, %ecx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler11JSObjectRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	cmpb	$0, -112(%rbp)
	je	.L1387
	movdqa	-80(%rbp), %xmm2
	movups	%xmm2, -104(%rbp)
.L1388:
	movq	96(%r15), %rcx
	movzbl	104(%r15), %edx
	leaq	-104(%rbp), %rsi
	movq	%r13, %rdi
	movl	$1, %r8d
	call	_ZNK2v88internal8compiler11JSObjectRef18GetOwnDataPropertyENS0_14RepresentationENS0_10FieldIndexENS1_19SerializationPolicyE@PLT
	cmpb	$0, -80(%rbp)
	je	.L1362
	leaq	-72(%rbp), %rdi
	call	_ZNK2v88internal8compiler9ObjectRef6objectEv@PLT
	movq	-152(%rbp), %rdi
	leaq	-128(%rbp), %rsi
	movq	%rax, -128(%rbp)
	addq	$56, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_
	jmp	.L1362
	.p2align 4,,10
	.p2align 3
.L1408:
	movq	72(%r15), %rdx
	testq	%rdx, %rdx
	je	.L1362
	movq	(%rdx), %rax
	testb	$1, %al
	jne	.L1412
.L1381:
	leaq	-80(%rbp), %r13
	movq	(%rbx), %rsi
	movl	$1, %ecx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler23FunctionTemplateInfoRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler23FunctionTemplateInfoRef13has_call_codeEv@PLT
	testb	%al, %al
	je	.L1365
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler23FunctionTemplateInfoRef17SerializeCallCodeEv@PLT
	jmp	.L1365
	.p2align 4,,10
	.p2align 3
.L1412:
	movq	-1(%rax), %rcx
	cmpw	$1105, 11(%rcx)
	je	.L1369
	movq	-1(%rax), %rax
	cmpw	$1104, 11(%rax)
	jne	.L1381
	leaq	-80(%rbp), %r13
	movq	(%rbx), %rsi
	movl	$1, %ecx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18JSBoundFunctionRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18JSBoundFunctionRef9SerializeEv@PLT
	jmp	.L1365
	.p2align 4,,10
	.p2align 3
.L1407:
	leaq	.LC41(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1387:
	movdqa	-80(%rbp), %xmm3
	movb	$1, -112(%rbp)
	movups	%xmm3, -104(%rbp)
	jmp	.L1388
	.p2align 4,,10
	.p2align 3
.L1369:
	movq	(%rbx), %rsi
	leaq	-112(%rbp), %r13
	movl	$1, %ecx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13JSFunctionRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13JSFunctionRef9SerializeEv@PLT
	movq	%r13, %rdi
	leaq	-80(%rbp), %r13
	call	_ZNK2v88internal8compiler13JSFunctionRef6sharedEv@PLT
	movq	%r13, %rdi
	movq	%rdx, -72(%rbp)
	movq	%rax, -80(%rbp)
	call	_ZNK2v88internal8compiler21SharedFunctionInfoRef6objectEv@PLT
	movq	(%rax), %rdx
	movq	7(%rdx), %rdx
	testb	$1, %dl
	je	.L1365
	movq	-1(%rdx), %rdx
	cmpw	$88, 11(%rdx)
	jne	.L1365
	movq	(%rbx), %rdx
	movq	(%rax), %rax
	movq	(%rdx), %rcx
	movq	7(%rax), %rsi
	movq	41112(%rcx), %rdi
	testq	%rdi, %rdi
	je	.L1405
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L1377:
	movq	(%rbx), %rsi
	movq	%r13, %rdi
	movl	$1, %ecx
	call	_ZN2v88internal8compiler23FunctionTemplateInfoRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler23FunctionTemplateInfoRef13has_call_codeEv@PLT
	testb	%al, %al
	jne	.L1413
.L1379:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler6MapRef6objectEv@PLT
	movq	-80(%rbp), %rsi
	movq	-72(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation28ProcessReceiverMapForApiCallENS1_23FunctionTemplateInfoRefENS0_6HandleINS0_3MapEEE
	jmp	.L1365
	.p2align 4,,10
	.p2align 3
.L1411:
	cmpb	$0, 16(%rbp)
	je	.L1362
	leaq	24(%rbp), %rdi
	leaq	-80(%rbp), %r13
	call	_ZNK2v88internal8compiler13HeapObjectRef3mapEv@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef6equalsERKS2_@PLT
	testb	%al, %al
	je	.L1390
	cmpb	$0, 16(%rbp)
	movzbl	-112(%rbp), %eax
	je	.L1362
	testb	%al, %al
	je	.L1392
	movdqu	24(%rbp), %xmm4
	movups	%xmm4, -104(%rbp)
	jmp	.L1388
.L1392:
	movdqu	24(%rbp), %xmm5
	movb	$1, -112(%rbp)
	movups	%xmm5, -104(%rbp)
	jmp	.L1388
.L1405:
	movq	41088(%rcx), %rdx
	cmpq	%rdx, 41096(%rcx)
	je	.L1414
.L1378:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%rcx)
	movq	%rsi, (%rdx)
	jmp	.L1377
.L1413:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler23FunctionTemplateInfoRef17SerializeCallCodeEv@PLT
	jmp	.L1379
.L1414:
	movq	%rcx, %rdi
	movq	%rsi, -168(%rbp)
	movq	%rcx, -160(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-168(%rbp), %rsi
	movq	-160(%rbp), %rcx
	movq	%rax, %rdx
	jmp	.L1378
.L1410:
	call	__stack_chk_fail@PLT
.L1390:
	leaq	.LC47(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19071:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation32ProcessMapForNamedPropertyAccessENS1_6MapRefERKNS1_7NameRefENS1_10AccessModeENS_4base8OptionalINS1_11JSObjectRefEEEPNS1_5HintsE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation32ProcessMapForNamedPropertyAccessENS1_6MapRefERKNS1_7NameRefENS1_10AccessModeENS_4base8OptionalINS1_11JSObjectRefEEEPNS1_5HintsE
	.section	.rodata._ZN2v88internal8compiler34SerializerForBackgroundCompilation18ProcessNamedAccessENS1_5HintsERKNS1_19NamedAccessFeedbackENS1_10AccessModeEPS3_.str1.1,"aMS",@progbits,1
.LC48:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation18ProcessNamedAccessENS1_5HintsERKNS1_19NamedAccessFeedbackENS1_10AccessModeEPS3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation18ProcessNamedAccessENS1_5HintsERKNS1_19NamedAccessFeedbackENS1_10AccessModeEPS3_
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation18ProcessNamedAccessENS1_5HintsERKNS1_19NamedAccessFeedbackENS1_10AccessModeEPS3_, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation18ProcessNamedAccessENS1_5HintsERKNS1_19NamedAccessFeedbackENS1_10AccessModeEPS3_:
.LFB19089:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movq	%rdx, %rdi
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$312, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -328(%rbp)
	movq	%rdx, -320(%rbp)
	movl	%ecx, -276(%rbp)
	movq	%r8, -288(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal8compiler17ProcessedFeedback13AsNamedAccessEv@PLT
	movq	32(%rax), %r15
	movq	40(%rax), %rax
	movq	%rax, -296(%rbp)
	cmpq	%rax, %r15
	je	.L1420
	movq	-320(%rbp), %rax
	leaq	-192(%rbp), %r13
	leaq	-240(%rbp), %rbx
	leaq	8(%rax), %r12
	movq	%r13, %rax
	movq	%rbx, %r13
	movq	%r15, %rbx
	movq	%rax, %r15
	.p2align 4,,10
	.p2align 3
.L1419:
	movq	(%rbx), %rdx
	movq	(%r14), %rsi
	movl	$1, %ecx
	movq	%r13, %rdi
	addq	$8, %rbx
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	pushq	-288(%rbp)
	movq	%r12, %r8
	movl	-276(%rbp), %r9d
	pushq	-208(%rbp)
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	-240(%rbp), %rdx
	movb	$0, -224(%rbp)
	movb	$0, -216(%rbp)
	movq	-232(%rbp), %rcx
	pushq	-216(%rbp)
	pushq	-224(%rbp)
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation32ProcessMapForNamedPropertyAccessENS1_6MapRefERKNS1_7NameRefENS1_10AccessModeENS_4base8OptionalINS1_11JSObjectRefEEEPNS1_5HintsE
	addq	$32, %rsp
	cmpq	%rbx, -296(%rbp)
	jne	.L1419
.L1420:
	movq	-328(%rbp), %rcx
	movq	(%r14), %rax
	movq	$0, -296(%rbp)
	xorl	%r13d, %r13d
	movq	$0, -304(%rbp)
	movq	144(%rcx), %r15
	leaq	128(%rcx), %r12
	movq	(%rax), %rbx
	cmpq	%r12, %r15
	je	.L1438
	movq	%r14, -336(%rbp)
	movq	%r15, %r14
	movq	%r13, %r15
	movq	%r12, %r13
	movq	%rbx, %r12
	xorl	%ebx, %ebx
	jmp	.L1417
	.p2align 4,,10
	.p2align 3
.L1423:
	cmpq	%rbx, %r15
	je	.L1425
	movq	%rdx, (%rbx)
	addq	$8, %rbx
.L1422:
	movq	%r14, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r14
	cmpq	%rax, %r13
	je	.L1503
.L1417:
	movq	32(%r14), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal3Map9TryUpdateEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L1422
	movq	(%rax), %rax
	movl	15(%rax), %ecx
	andl	$1048576, %ecx
	je	.L1423
	movl	15(%rax), %eax
	testl	$4194304, %eax
	jne	.L1423
	movq	%r14, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r14
	cmpq	%rax, %r13
	jne	.L1417
	.p2align 4,,10
	.p2align 3
.L1503:
	movq	-304(%rbp), %r15
	movq	%rbx, -296(%rbp)
	movq	-336(%rbp), %r14
	cmpq	%r15, %rbx
	je	.L1436
	movq	-320(%rbp), %rax
	leaq	-192(%rbp), %r13
	leaq	-240(%rbp), %rbx
	leaq	8(%rax), %r12
	movq	%r13, %rax
	movq	%rbx, %r13
	movq	%r15, %rbx
	movq	%rax, %r15
	.p2align 4,,10
	.p2align 3
.L1437:
	movq	(%rbx), %rdx
	movq	(%r14), %rsi
	movl	$1, %ecx
	movq	%r13, %rdi
	addq	$8, %rbx
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	pushq	-288(%rbp)
	movq	%r12, %r8
	movl	-276(%rbp), %r9d
	pushq	-208(%rbp)
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	-240(%rbp), %rdx
	movb	$0, -224(%rbp)
	movb	$0, -216(%rbp)
	movq	-232(%rbp), %rcx
	pushq	-216(%rbp)
	pushq	-224(%rbp)
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation32ProcessMapForNamedPropertyAccessENS1_6MapRefERKNS1_7NameRefENS1_10AccessModeENS_4base8OptionalINS1_11JSObjectRefEEEPNS1_5HintsE
	addq	$32, %rsp
	cmpq	%rbx, -296(%rbp)
	jne	.L1437
.L1436:
	cmpq	$0, -304(%rbp)
	je	.L1501
	movq	-304(%rbp), %rdi
	call	_ZdlPv@PLT
.L1501:
	movq	(%r14), %rax
.L1438:
	cmpb	$0, 24(%rax)
	je	.L1504
	movdqu	32(%rax), %xmm1
	leaq	-192(%rbp), %r13
	leaq	-240(%rbp), %rbx
	movq	%r13, %rdi
	leaq	-256(%rbp), %r15
	movaps	%xmm1, -192(%rbp)
	call	_ZNK2v88internal8compiler16NativeContextRef19global_proxy_objectEv@PLT
	movq	-320(%rbp), %rcx
	movq	%rax, -256(%rbp)
	movq	-328(%rbp), %rax
	addq	$8, %rcx
	movq	%rdx, -248(%rbp)
	movq	88(%rax), %r12
	addq	$72, %rax
	movq	%rcx, -304(%rbp)
	movq	%rax, -296(%rbp)
	cmpq	%r12, %rax
	jne	.L1440
	jmp	.L1415
	.p2align 4,,10
	.p2align 3
.L1441:
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef6equalsERKS2_@PLT
	testb	%al, %al
	jne	.L1505
.L1447:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rax, -296(%rbp)
	je	.L1415
.L1440:
	movq	32(%r12), %rdx
	movq	(%r14), %rsi
	movl	$1, %ecx
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler9ObjectRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movl	-276(%rbp), %eax
	testl	%eax, %eax
	jne	.L1441
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef10IsJSObjectEv@PLT
	testb	%al, %al
	jne	.L1506
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef6equalsERKS2_@PLT
	testb	%al, %al
	jne	.L1507
.L1454:
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsJSFunctionEv@PLT
	testb	%al, %al
	je	.L1447
	movq	(%r14), %rsi
	movq	%r13, %rdi
	movl	$1, %ecx
	movq	(%rsi), %rax
	leaq	3104(%rax), %rdx
	call	_ZN2v88internal8compiler9ObjectRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	-304(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZNK2v88internal8compiler9ObjectRef6equalsERKS2_@PLT
	testb	%al, %al
	je	.L1447
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12AsJSFunctionEv@PLT
	leaq	-224(%rbp), %rdi
	movq	%rax, -224(%rbp)
	movq	%rdx, -216(%rbp)
	movq	%rdi, -320(%rbp)
	call	_ZN2v88internal8compiler13JSFunctionRef9SerializeEv@PLT
	cmpq	$0, -288(%rbp)
	je	.L1447
	movq	-320(%rbp), %rdi
	call	_ZNK2v88internal8compiler13JSFunctionRef13has_prototypeEv@PLT
	movq	-320(%rbp), %rdi
	testb	%al, %al
	je	.L1447
	call	_ZNK2v88internal8compiler13JSFunctionRef9prototypeEv@PLT
	movq	%r13, %rdi
	movq	%rax, -192(%rbp)
	movq	%rdx, -184(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef6objectEv@PLT
	leaq	-264(%rbp), %rsi
	movq	%rax, -264(%rbp)
	movq	-288(%rbp), %rax
	leaq	56(%rax), %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_
	jmp	.L1447
	.p2align 4,,10
	.p2align 3
.L1505:
	movq	-304(%rbp), %rdx
	movl	$1, %ecx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler16JSGlobalProxyRef15GetPropertyCellERKNS1_7NameRefENS1_19SerializationPolicyE@PLT
	jmp	.L1447
	.p2align 4,,10
	.p2align 3
.L1415:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1508
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1506:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef10AsJSObjectEv@PLT
	movq	%r13, %rdi
	movq	%rax, -192(%rbp)
	movq	%rdx, -184(%rbp)
	call	_ZNK2v88internal8compiler13HeapObjectRef3mapEv@PLT
	movq	%rbx, %rdi
	movq	%rax, -320(%rbp)
	movq	%rdx, -312(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef10AsJSObjectEv@PLT
	pushq	-288(%rbp)
	xorl	%r9d, %r9d
	movq	-320(%rbp), %r10
	movq	-312(%rbp), %r11
	pushq	%rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	pushq	%rax
	movq	-304(%rbp), %r8
	movb	$1, -224(%rbp)
	movq	%r11, %rcx
	pushq	-224(%rbp)
	movq	%rdx, -208(%rbp)
	movq	%r10, %rdx
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation32ProcessMapForNamedPropertyAccessENS1_6MapRefERKNS1_7NameRefENS1_10AccessModeENS_4base8OptionalINS1_11JSObjectRefEEEPNS1_5HintsE
	addq	$32, %rsp
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef6equalsERKS2_@PLT
	testb	%al, %al
	je	.L1454
.L1507:
	movq	-304(%rbp), %rdx
	movl	$1, %ecx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler16JSGlobalProxyRef15GetPropertyCellERKNS1_7NameRefENS1_19SerializationPolicyE@PLT
	jmp	.L1454
	.p2align 4,,10
	.p2align 3
.L1425:
	movabsq	$1152921504606846975, %rcx
	movq	%r15, %rsi
	subq	-304(%rbp), %rsi
	movq	%rsi, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L1509
	testq	%rax, %rax
	je	.L1510
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	ja	.L1511
	testq	%rcx, %rcx
	jne	.L1512
	movl	$8, %ecx
	xorl	%r15d, %r15d
	xorl	%eax, %eax
.L1429:
	movq	-304(%rbp), %r8
	movq	%rdx, (%rax,%rsi)
	cmpq	%r8, %rbx
	je	.L1459
	leaq	-8(%rbx), %rsi
	leaq	15(%rax), %rdx
	subq	%r8, %rsi
	subq	%r8, %rdx
	movq	%rsi, %rdi
	shrq	$3, %rdi
	cmpq	$30, %rdx
	jbe	.L1460
	movabsq	$2305843009213693948, %rcx
	testq	%rcx, %rdi
	je	.L1460
	addq	$1, %rdi
	xorl	%edx, %edx
	movq	%rdi, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L1432:
	movdqu	(%r8,%rdx), %xmm0
	movups	%xmm0, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rcx
	jne	.L1432
	movq	%rdi, %r9
	movq	-304(%rbp), %rbx
	andq	$-2, %r9
	leaq	0(,%r9,8), %rcx
	leaq	(%rbx,%rcx), %rdx
	addq	%rax, %rcx
	cmpq	%rdi, %r9
	je	.L1434
	movq	(%rdx), %rdx
	movq	%rdx, (%rcx)
.L1434:
	leaq	16(%rax,%rsi), %rbx
.L1430:
	movq	-304(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1435
	movq	%rax, -296(%rbp)
	call	_ZdlPv@PLT
	movq	-296(%rbp), %rax
.L1435:
	movq	%rax, -304(%rbp)
	jmp	.L1422
	.p2align 4,,10
	.p2align 3
.L1511:
	movabsq	$9223372036854775800, %r15
.L1427:
	movq	%r15, %rdi
	movq	%rsi, -344(%rbp)
	movq	%rdx, -296(%rbp)
	call	_Znwm@PLT
	movq	-296(%rbp), %rdx
	movq	-344(%rbp), %rsi
	addq	%rax, %r15
	leaq	8(%rax), %rcx
	jmp	.L1429
	.p2align 4,,10
	.p2align 3
.L1510:
	movl	$8, %r15d
	jmp	.L1427
	.p2align 4,,10
	.p2align 3
.L1504:
	leaq	.LC41(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1460:
	movq	-304(%rbp), %rdx
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L1431:
	movq	(%rdx), %rdi
	addq	$8, %rdx
	addq	$8, %rcx
	movq	%rdi, -8(%rcx)
	cmpq	%rbx, %rdx
	jne	.L1431
	jmp	.L1434
.L1512:
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rcx
	movq	%rax, %r15
	cmovbe	%rcx, %r15
	salq	$3, %r15
	jmp	.L1427
.L1459:
	movq	%rcx, %rbx
	jmp	.L1430
.L1509:
	leaq	.LC48(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1508:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19089:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation18ProcessNamedAccessENS1_5HintsERKNS1_19NamedAccessFeedbackENS1_10AccessModeEPS3_, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation18ProcessNamedAccessENS1_5HintsERKNS1_19NamedAccessFeedbackENS1_10AccessModeEPS3_
	.section	.text._ZNSt8_Rb_treeIN2v88internal6HandleINS1_3MapEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_,"axG",@progbits,_ZNSt8_Rb_treeIN2v88internal6HandleINS1_3MapEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_3MapEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_
	.type	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_3MapEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_, @function
_ZNSt8_Rb_treeIN2v88internal6HandleINS1_3MapEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_:
.LFB22807:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	16(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	24(%rdi), %r12
	testq	%r12, %r12
	je	.L1527
	movq	(%rsi), %rsi
	jmp	.L1515
	.p2align 4,,10
	.p2align 3
.L1534:
	movq	16(%r12), %rax
	movl	$1, %edx
	testq	%rax, %rax
	je	.L1516
.L1535:
	movq	%rax, %r12
.L1515:
	movq	32(%r12), %rcx
	cmpq	%rcx, %rsi
	jb	.L1534
	movq	24(%r12), %rax
	xorl	%edx, %edx
	testq	%rax, %rax
	jne	.L1535
.L1516:
	testb	%dl, %dl
	jne	.L1514
	cmpq	%rcx, %rsi
	jbe	.L1520
.L1525:
	movl	$1, %r8d
	cmpq	%r12, %r15
	jne	.L1536
.L1521:
	movq	(%rbx), %rdi
	movq	16(%rdi), %r13
	movq	24(%rdi), %rax
	subq	%r13, %rax
	cmpq	$39, %rax
	jbe	.L1537
	leaq	40(%r13), %rax
	movq	%rax, 16(%rdi)
.L1523:
	movq	(%r14), %rax
	movq	%r12, %rdx
	movq	%r15, %rcx
	movq	%r13, %rsi
	movl	%r8d, %edi
	movq	%rax, 32(%r13)
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 48(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	movl	$1, %edx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1527:
	.cfi_restore_state
	movq	%r15, %r12
.L1514:
	cmpq	%r12, 32(%rbx)
	je	.L1525
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	(%r14), %rcx
	cmpq	%rcx, 32(%rax)
	jb	.L1525
	movq	%rax, %r12
.L1520:
	addq	$24, %rsp
	movq	%r12, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1536:
	.cfi_restore_state
	xorl	%r8d, %r8d
	movq	32(%r12), %rax
	cmpq	%rax, (%r14)
	setb	%r8b
	jmp	.L1521
	.p2align 4,,10
	.p2align 3
.L1537:
	movl	$40, %esi
	movl	%r8d, -52(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-52(%rbp), %r8d
	movq	%rax, %r13
	jmp	.L1523
	.cfi_endproc
.LFE22807:
	.size	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_3MapEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_, .-_ZNSt8_Rb_treeIN2v88internal6HandleINS1_3MapEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_
	.section	.text._ZNSt8_Rb_treeIiiSt9_IdentityIiESt4lessIiESaIiEE8_M_eraseEPSt13_Rb_tree_nodeIiE,"axG",@progbits,_ZNSt8_Rb_treeIiiSt9_IdentityIiESt4lessIiESaIiEE8_M_eraseEPSt13_Rb_tree_nodeIiE,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIiiSt9_IdentityIiESt4lessIiESaIiEE8_M_eraseEPSt13_Rb_tree_nodeIiE
	.type	_ZNSt8_Rb_treeIiiSt9_IdentityIiESt4lessIiESaIiEE8_M_eraseEPSt13_Rb_tree_nodeIiE, @function
_ZNSt8_Rb_treeIiiSt9_IdentityIiESt4lessIiESaIiEE8_M_eraseEPSt13_Rb_tree_nodeIiE:
.LFB22967:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L1546
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L1540:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIiiSt9_IdentityIiESt4lessIiESaIiEE8_M_eraseEPSt13_Rb_tree_nodeIiE
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1540
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1546:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE22967:
	.size	_ZNSt8_Rb_treeIiiSt9_IdentityIiESt4lessIiESaIiEE8_M_eraseEPSt13_Rb_tree_nodeIiE, .-_ZNSt8_Rb_treeIiiSt9_IdentityIiESt4lessIiESaIiEE8_M_eraseEPSt13_Rb_tree_nodeIiE
	.section	.text._ZNSt8_Rb_treeIN2v88internal8compiler14VirtualContextES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE7_M_copyINSA_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS3_EPKSE_PSt18_Rb_tree_node_baseRT_,"axG",@progbits,_ZNSt8_Rb_treeIN2v88internal8compiler14VirtualContextES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE7_M_copyINSA_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS3_EPKSE_PSt18_Rb_tree_node_baseRT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN2v88internal8compiler14VirtualContextES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE7_M_copyINSA_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS3_EPKSE_PSt18_Rb_tree_node_baseRT_
	.type	_ZNSt8_Rb_treeIN2v88internal8compiler14VirtualContextES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE7_M_copyINSA_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS3_EPKSE_PSt18_Rb_tree_node_baseRT_, @function
_ZNSt8_Rb_treeIN2v88internal8compiler14VirtualContextES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE7_M_copyINSA_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS3_EPKSE_PSt18_Rb_tree_node_baseRT_:
.LFB23860:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	8(%rcx), %r15
	testq	%r15, %r15
	je	.L1550
	movq	8(%r15), %rax
	movq	%rax, 8(%rcx)
	testq	%rax, %rax
	je	.L1551
	cmpq	24(%rax), %r15
	je	.L1612
	movq	$0, 16(%rax)
.L1557:
	movq	40(%rbx), %rax
	movl	32(%rbx), %ecx
	movq	%rax, 40(%r15)
	movl	%ecx, 32(%r15)
	movl	(%rbx), %eax
	movq	$0, 16(%r15)
	movl	%eax, (%r15)
	movq	$0, 24(%r15)
	movq	%rdx, 8(%r15)
	movq	24(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L1558
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal8compiler14VirtualContextES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE7_M_copyINSA_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS3_EPKSE_PSt18_Rb_tree_node_baseRT_
	movq	%rax, 24(%r15)
.L1558:
	movq	16(%rbx), %r12
	testq	%r12, %r12
	je	.L1549
	movq	8(%r14), %rbx
	movq	%r15, %rdx
	testq	%rbx, %rbx
	je	.L1560
.L1614:
	movq	8(%rbx), %rax
	movq	%rax, 8(%r14)
	testq	%rax, %rax
	je	.L1561
	cmpq	24(%rax), %rbx
	je	.L1613
	movq	$0, 16(%rax)
.L1567:
	movq	40(%r12), %rax
	movl	32(%r12), %ecx
	movq	%rax, 40(%rbx)
	movl	%ecx, 32(%rbx)
	movl	(%r12), %eax
	movq	$0, 16(%rbx)
	movl	%eax, (%rbx)
	movq	$0, 24(%rbx)
	movq	%rbx, 16(%rdx)
	movq	%rdx, 8(%rbx)
	movq	24(%r12), %rsi
	testq	%rsi, %rsi
	je	.L1568
	movq	%r14, %rcx
	movq	%rbx, %rdx
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal8compiler14VirtualContextES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE7_M_copyINSA_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS3_EPKSE_PSt18_Rb_tree_node_baseRT_
	movq	%rax, 24(%rbx)
	movq	16(%r12), %r12
	testq	%r12, %r12
	je	.L1549
.L1569:
	movq	%rbx, %rdx
	movq	8(%r14), %rbx
	testq	%rbx, %rbx
	jne	.L1614
.L1560:
	movq	16(%r14), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	$47, %rax
	jbe	.L1615
	leaq	48(%rbx), %rax
	movq	%rax, 16(%rdi)
	jmp	.L1567
	.p2align 4,,10
	.p2align 3
.L1568:
	movq	16(%r12), %r12
	testq	%r12, %r12
	jne	.L1569
.L1549:
	addq	$24, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1561:
	.cfi_restore_state
	movq	$0, (%r14)
	jmp	.L1567
	.p2align 4,,10
	.p2align 3
.L1613:
	movq	$0, 24(%rax)
	movq	8(%r14), %rax
	movq	16(%rax), %rcx
	testq	%rcx, %rcx
	je	.L1567
	movq	%rcx, 8(%r14)
	movq	24(%rcx), %rax
	testq	%rax, %rax
	je	.L1564
	.p2align 4,,10
	.p2align 3
.L1565:
	movq	%rax, 8(%r14)
	movq	%rax, %rcx
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L1565
.L1564:
	movq	16(%rcx), %rax
	testq	%rax, %rax
	je	.L1567
	movq	%rax, 8(%r14)
	jmp	.L1567
	.p2align 4,,10
	.p2align 3
.L1550:
	movq	16(%rcx), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %r15
	movq	24(%rdi), %rax
	subq	%r15, %rax
	cmpq	$47, %rax
	jbe	.L1616
	leaq	48(%r15), %rax
	movq	%rax, 16(%rdi)
	jmp	.L1557
	.p2align 4,,10
	.p2align 3
.L1551:
	movq	$0, (%rcx)
	jmp	.L1557
	.p2align 4,,10
	.p2align 3
.L1612:
	movq	$0, 24(%rax)
	movq	8(%rcx), %rax
	movq	16(%rax), %rcx
	testq	%rcx, %rcx
	je	.L1557
	movq	%rcx, 8(%r14)
	movq	24(%rcx), %rax
	testq	%rax, %rax
	je	.L1554
	.p2align 4,,10
	.p2align 3
.L1555:
	movq	%rax, 8(%r14)
	movq	%rax, %rcx
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L1555
.L1554:
	movq	16(%rcx), %rax
	testq	%rax, %rax
	je	.L1557
	movq	%rax, 8(%r14)
	jmp	.L1557
	.p2align 4,,10
	.p2align 3
.L1615:
	movl	$48, %esi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %rbx
	jmp	.L1567
.L1616:
	movl	$48, %esi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %r15
	jmp	.L1557
	.cfi_endproc
.LFE23860:
	.size	_ZNSt8_Rb_treeIN2v88internal8compiler14VirtualContextES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE7_M_copyINSA_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS3_EPKSE_PSt18_Rb_tree_node_baseRT_, .-_ZNSt8_Rb_treeIN2v88internal8compiler14VirtualContextES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE7_M_copyINSA_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS3_EPKSE_PSt18_Rb_tree_node_baseRT_
	.section	.text._ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE7_M_copyINSC_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS4_EPKSG_PSt18_Rb_tree_node_baseRT_,"axG",@progbits,_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE7_M_copyINSC_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS4_EPKSG_PSt18_Rb_tree_node_baseRT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE7_M_copyINSC_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS4_EPKSG_PSt18_Rb_tree_node_baseRT_
	.type	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE7_M_copyINSC_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS4_EPKSG_PSt18_Rb_tree_node_baseRT_, @function
_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE7_M_copyINSC_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS4_EPKSG_PSt18_Rb_tree_node_baseRT_:
.LFB23868:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	8(%rcx), %r15
	testq	%r15, %r15
	je	.L1618
	movq	8(%r15), %rax
	movq	%rax, 8(%rcx)
	testq	%rax, %rax
	je	.L1619
	cmpq	24(%rax), %r15
	je	.L1680
	movq	$0, 16(%rax)
.L1625:
	movq	32(%rbx), %rax
	movq	%rax, 32(%r15)
	movl	(%rbx), %eax
	movq	$0, 16(%r15)
	movl	%eax, (%r15)
	movq	$0, 24(%r15)
	movq	%rdx, 8(%r15)
	movq	24(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L1626
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE7_M_copyINSC_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS4_EPKSG_PSt18_Rb_tree_node_baseRT_
	movq	%rax, 24(%r15)
.L1626:
	movq	16(%rbx), %r12
	testq	%r12, %r12
	je	.L1617
	movq	8(%r14), %rbx
	movq	%r15, %rdx
	testq	%rbx, %rbx
	je	.L1628
.L1682:
	movq	8(%rbx), %rax
	movq	%rax, 8(%r14)
	testq	%rax, %rax
	je	.L1629
	cmpq	24(%rax), %rbx
	je	.L1681
	movq	$0, 16(%rax)
.L1635:
	movq	32(%r12), %rax
	movq	%rax, 32(%rbx)
	movl	(%r12), %eax
	movq	$0, 16(%rbx)
	movl	%eax, (%rbx)
	movq	$0, 24(%rbx)
	movq	%rbx, 16(%rdx)
	movq	%rdx, 8(%rbx)
	movq	24(%r12), %rsi
	testq	%rsi, %rsi
	je	.L1636
	movq	%r14, %rcx
	movq	%rbx, %rdx
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE7_M_copyINSC_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS4_EPKSG_PSt18_Rb_tree_node_baseRT_
	movq	%rax, 24(%rbx)
	movq	16(%r12), %r12
	testq	%r12, %r12
	je	.L1617
.L1637:
	movq	%rbx, %rdx
	movq	8(%r14), %rbx
	testq	%rbx, %rbx
	jne	.L1682
.L1628:
	movq	16(%r14), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	$39, %rax
	jbe	.L1683
	leaq	40(%rbx), %rax
	movq	%rax, 16(%rdi)
	jmp	.L1635
	.p2align 4,,10
	.p2align 3
.L1636:
	movq	16(%r12), %r12
	testq	%r12, %r12
	jne	.L1637
.L1617:
	addq	$24, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1629:
	.cfi_restore_state
	movq	$0, (%r14)
	jmp	.L1635
	.p2align 4,,10
	.p2align 3
.L1681:
	movq	$0, 24(%rax)
	movq	8(%r14), %rax
	movq	16(%rax), %rcx
	testq	%rcx, %rcx
	je	.L1635
	movq	%rcx, 8(%r14)
	movq	24(%rcx), %rax
	testq	%rax, %rax
	je	.L1632
	.p2align 4,,10
	.p2align 3
.L1633:
	movq	%rax, 8(%r14)
	movq	%rax, %rcx
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L1633
.L1632:
	movq	16(%rcx), %rax
	testq	%rax, %rax
	je	.L1635
	movq	%rax, 8(%r14)
	jmp	.L1635
	.p2align 4,,10
	.p2align 3
.L1618:
	movq	16(%rcx), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %r15
	movq	24(%rdi), %rax
	subq	%r15, %rax
	cmpq	$39, %rax
	jbe	.L1684
	leaq	40(%r15), %rax
	movq	%rax, 16(%rdi)
	jmp	.L1625
	.p2align 4,,10
	.p2align 3
.L1619:
	movq	$0, (%rcx)
	jmp	.L1625
	.p2align 4,,10
	.p2align 3
.L1680:
	movq	$0, 24(%rax)
	movq	8(%rcx), %rax
	movq	16(%rax), %rcx
	testq	%rcx, %rcx
	je	.L1625
	movq	%rcx, 8(%r14)
	movq	24(%rcx), %rax
	testq	%rax, %rax
	je	.L1622
	.p2align 4,,10
	.p2align 3
.L1623:
	movq	%rax, 8(%r14)
	movq	%rax, %rcx
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L1623
.L1622:
	movq	16(%rcx), %rax
	testq	%rax, %rax
	je	.L1625
	movq	%rax, 8(%r14)
	jmp	.L1625
	.p2align 4,,10
	.p2align 3
.L1683:
	movl	$40, %esi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %rbx
	jmp	.L1635
.L1684:
	movl	$40, %esi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %r15
	jmp	.L1625
	.cfi_endproc
.LFE23868:
	.size	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE7_M_copyINSC_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS4_EPKSG_PSt18_Rb_tree_node_baseRT_, .-_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE7_M_copyINSC_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS4_EPKSG_PSt18_Rb_tree_node_baseRT_
	.section	.text._ZNSt8_Rb_treeIN2v88internal6HandleINS1_3MapEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE7_M_copyINSC_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS4_EPKSG_PSt18_Rb_tree_node_baseRT_,"axG",@progbits,_ZNSt8_Rb_treeIN2v88internal6HandleINS1_3MapEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE7_M_copyINSC_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS4_EPKSG_PSt18_Rb_tree_node_baseRT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_3MapEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE7_M_copyINSC_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS4_EPKSG_PSt18_Rb_tree_node_baseRT_
	.type	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_3MapEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE7_M_copyINSC_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS4_EPKSG_PSt18_Rb_tree_node_baseRT_, @function
_ZNSt8_Rb_treeIN2v88internal6HandleINS1_3MapEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE7_M_copyINSC_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS4_EPKSG_PSt18_Rb_tree_node_baseRT_:
.LFB23876:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	8(%rcx), %r15
	testq	%r15, %r15
	je	.L1686
	movq	8(%r15), %rax
	movq	%rax, 8(%rcx)
	testq	%rax, %rax
	je	.L1687
	cmpq	24(%rax), %r15
	je	.L1748
	movq	$0, 16(%rax)
.L1693:
	movq	32(%rbx), %rax
	movq	%rax, 32(%r15)
	movl	(%rbx), %eax
	movq	$0, 16(%r15)
	movl	%eax, (%r15)
	movq	$0, 24(%r15)
	movq	%rdx, 8(%r15)
	movq	24(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L1694
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_3MapEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE7_M_copyINSC_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS4_EPKSG_PSt18_Rb_tree_node_baseRT_
	movq	%rax, 24(%r15)
.L1694:
	movq	16(%rbx), %r12
	testq	%r12, %r12
	je	.L1685
	movq	8(%r14), %rbx
	movq	%r15, %rdx
	testq	%rbx, %rbx
	je	.L1696
.L1750:
	movq	8(%rbx), %rax
	movq	%rax, 8(%r14)
	testq	%rax, %rax
	je	.L1697
	cmpq	24(%rax), %rbx
	je	.L1749
	movq	$0, 16(%rax)
.L1703:
	movq	32(%r12), %rax
	movq	%rax, 32(%rbx)
	movl	(%r12), %eax
	movq	$0, 16(%rbx)
	movl	%eax, (%rbx)
	movq	$0, 24(%rbx)
	movq	%rbx, 16(%rdx)
	movq	%rdx, 8(%rbx)
	movq	24(%r12), %rsi
	testq	%rsi, %rsi
	je	.L1704
	movq	%r14, %rcx
	movq	%rbx, %rdx
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_3MapEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE7_M_copyINSC_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS4_EPKSG_PSt18_Rb_tree_node_baseRT_
	movq	%rax, 24(%rbx)
	movq	16(%r12), %r12
	testq	%r12, %r12
	je	.L1685
.L1705:
	movq	%rbx, %rdx
	movq	8(%r14), %rbx
	testq	%rbx, %rbx
	jne	.L1750
.L1696:
	movq	16(%r14), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	$39, %rax
	jbe	.L1751
	leaq	40(%rbx), %rax
	movq	%rax, 16(%rdi)
	jmp	.L1703
	.p2align 4,,10
	.p2align 3
.L1704:
	movq	16(%r12), %r12
	testq	%r12, %r12
	jne	.L1705
.L1685:
	addq	$24, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1697:
	.cfi_restore_state
	movq	$0, (%r14)
	jmp	.L1703
	.p2align 4,,10
	.p2align 3
.L1749:
	movq	$0, 24(%rax)
	movq	8(%r14), %rax
	movq	16(%rax), %rcx
	testq	%rcx, %rcx
	je	.L1703
	movq	%rcx, 8(%r14)
	movq	24(%rcx), %rax
	testq	%rax, %rax
	je	.L1700
	.p2align 4,,10
	.p2align 3
.L1701:
	movq	%rax, 8(%r14)
	movq	%rax, %rcx
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L1701
.L1700:
	movq	16(%rcx), %rax
	testq	%rax, %rax
	je	.L1703
	movq	%rax, 8(%r14)
	jmp	.L1703
	.p2align 4,,10
	.p2align 3
.L1686:
	movq	16(%rcx), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %r15
	movq	24(%rdi), %rax
	subq	%r15, %rax
	cmpq	$39, %rax
	jbe	.L1752
	leaq	40(%r15), %rax
	movq	%rax, 16(%rdi)
	jmp	.L1693
	.p2align 4,,10
	.p2align 3
.L1687:
	movq	$0, (%rcx)
	jmp	.L1693
	.p2align 4,,10
	.p2align 3
.L1748:
	movq	$0, 24(%rax)
	movq	8(%rcx), %rax
	movq	16(%rax), %rcx
	testq	%rcx, %rcx
	je	.L1693
	movq	%rcx, 8(%r14)
	movq	24(%rcx), %rax
	testq	%rax, %rax
	je	.L1690
	.p2align 4,,10
	.p2align 3
.L1691:
	movq	%rax, 8(%r14)
	movq	%rax, %rcx
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L1691
.L1690:
	movq	16(%rcx), %rax
	testq	%rax, %rax
	je	.L1693
	movq	%rax, 8(%r14)
	jmp	.L1693
	.p2align 4,,10
	.p2align 3
.L1751:
	movl	$40, %esi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %rbx
	jmp	.L1703
.L1752:
	movl	$40, %esi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %r15
	jmp	.L1693
	.cfi_endproc
.LFE23876:
	.size	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_3MapEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE7_M_copyINSC_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS4_EPKSG_PSt18_Rb_tree_node_baseRT_, .-_ZNSt8_Rb_treeIN2v88internal6HandleINS1_3MapEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE7_M_copyINSC_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS4_EPKSG_PSt18_Rb_tree_node_baseRT_
	.section	.text._ZNSt10_HashtableIiSt4pairIKiPN2v88internal8compiler34SerializerForBackgroundCompilation11EnvironmentEENS3_13ZoneAllocatorIS8_EENSt8__detail10_Select1stESt8equal_toIiENS2_4base4hashIiEENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS8_Lb1EEEm,"axG",@progbits,_ZNSt10_HashtableIiSt4pairIKiPN2v88internal8compiler34SerializerForBackgroundCompilation11EnvironmentEENS3_13ZoneAllocatorIS8_EENSt8__detail10_Select1stESt8equal_toIiENS2_4base4hashIiEENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS8_Lb1EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIiSt4pairIKiPN2v88internal8compiler34SerializerForBackgroundCompilation11EnvironmentEENS3_13ZoneAllocatorIS8_EENSt8__detail10_Select1stESt8equal_toIiENS2_4base4hashIiEENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS8_Lb1EEEm
	.type	_ZNSt10_HashtableIiSt4pairIKiPN2v88internal8compiler34SerializerForBackgroundCompilation11EnvironmentEENS3_13ZoneAllocatorIS8_EENSt8__detail10_Select1stESt8equal_toIiENS2_4base4hashIiEENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS8_Lb1EEEm, @function
_ZNSt10_HashtableIiSt4pairIKiPN2v88internal8compiler34SerializerForBackgroundCompilation11EnvironmentEENS3_13ZoneAllocatorIS8_EENSt8__detail10_Select1stESt8equal_toIiENS2_4base4hashIiEENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS8_Lb1EEEm:
.LFB23998:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	movq	%r8, %rcx
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$40, %rdi
	subq	$24, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L1777
.L1754:
	movq	%r14, 24(%r12)
	movq	8(%rbx), %rax
	leaq	0(,%r15,8), %rcx
	movq	(%rax,%r15,8), %rax
	testq	%rax, %rax
	je	.L1763
	movq	(%rax), %rax
	movq	%rax, (%r12)
	movq	8(%rbx), %rax
	movq	(%rax,%r15,8), %rax
	movq	%r12, (%rax)
.L1764:
	addq	$1, 32(%rbx)
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1777:
	.cfi_restore_state
	movq	%rdx, %r13
	cmpq	$1, %rdx
	je	.L1778
	movq	(%rbx), %r8
	leaq	0(,%rdx,8), %rdx
	movq	%rdx, %rsi
	movq	16(%r8), %rdi
	movq	24(%r8), %rax
	subq	%rdi, %rax
	cmpq	%rax, %rdx
	ja	.L1779
	addq	%rdi, %rsi
	movq	%rsi, 16(%r8)
.L1758:
	xorl	%esi, %esi
	call	memset@PLT
	movq	%rax, %rdi
.L1756:
	movq	24(%rbx), %rsi
	movq	$0, 24(%rbx)
	testq	%rsi, %rsi
	je	.L1759
	xorl	%r9d, %r9d
	leaq	24(%rbx), %r10
	jmp	.L1760
	.p2align 4,,10
	.p2align 3
.L1761:
	movq	(%r8), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L1762:
	testq	%rsi, %rsi
	je	.L1759
.L1760:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	24(%rcx), %rax
	divq	%r13
	leaq	(%rdi,%rdx,8), %rax
	movq	(%rax), %r8
	testq	%r8, %r8
	jne	.L1761
	movq	24(%rbx), %r8
	movq	%r8, (%rcx)
	movq	%rcx, 24(%rbx)
	movq	%r10, (%rax)
	cmpq	$0, (%rcx)
	je	.L1766
	movq	%rcx, (%rdi,%r9,8)
	movq	%rdx, %r9
	testq	%rsi, %rsi
	jne	.L1760
	.p2align 4,,10
	.p2align 3
.L1759:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%r13, 16(%rbx)
	divq	%r13
	movq	%rdi, 8(%rbx)
	movq	%rdx, %r15
	jmp	.L1754
	.p2align 4,,10
	.p2align 3
.L1763:
	movq	24(%rbx), %rax
	movq	%rax, (%r12)
	movq	%r12, 24(%rbx)
	movq	(%r12), %rax
	testq	%rax, %rax
	je	.L1765
	movq	24(%rax), %rax
	xorl	%edx, %edx
	divq	16(%rbx)
	movq	8(%rbx), %rax
	movq	%r12, (%rax,%rdx,8)
.L1765:
	movq	8(%rbx), %rax
	leaq	24(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L1764
	.p2align 4,,10
	.p2align 3
.L1766:
	movq	%rdx, %r9
	jmp	.L1762
	.p2align 4,,10
	.p2align 3
.L1778:
	movq	$0, 56(%rbx)
	leaq	56(%rbx), %rdi
	jmp	.L1756
	.p2align 4,,10
	.p2align 3
.L1779:
	movq	%r8, %rdi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %rdi
	jmp	.L1758
	.cfi_endproc
.LFE23998:
	.size	_ZNSt10_HashtableIiSt4pairIKiPN2v88internal8compiler34SerializerForBackgroundCompilation11EnvironmentEENS3_13ZoneAllocatorIS8_EENSt8__detail10_Select1stESt8equal_toIiENS2_4base4hashIiEENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS8_Lb1EEEm, .-_ZNSt10_HashtableIiSt4pairIKiPN2v88internal8compiler34SerializerForBackgroundCompilation11EnvironmentEENS3_13ZoneAllocatorIS8_EENSt8__detail10_Select1stESt8equal_toIiENS2_4base4hashIiEENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS8_Lb1EEEm
	.section	.text._ZNSt8_Rb_treeIN2v88internal8compiler14VirtualContextES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE7_M_copyINSA_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS3_EPKSE_PSt18_Rb_tree_node_baseRT_,"axG",@progbits,_ZNSt8_Rb_treeIN2v88internal8compiler14VirtualContextES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE7_M_copyINSA_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS3_EPKSE_PSt18_Rb_tree_node_baseRT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN2v88internal8compiler14VirtualContextES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE7_M_copyINSA_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS3_EPKSE_PSt18_Rb_tree_node_baseRT_
	.type	_ZNSt8_Rb_treeIN2v88internal8compiler14VirtualContextES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE7_M_copyINSA_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS3_EPKSE_PSt18_Rb_tree_node_baseRT_, @function
_ZNSt8_Rb_treeIN2v88internal8compiler14VirtualContextES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE7_M_copyINSA_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS3_EPKSE_PSt18_Rb_tree_node_baseRT_:
.LFB24190:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	(%rcx), %rax
	movq	%rdi, -56(%rbp)
	movq	(%rax), %rdi
	movq	16(%rdi), %r14
	movq	24(%rdi), %rax
	subq	%r14, %rax
	cmpq	$47, %rax
	jbe	.L1803
	leaq	48(%r14), %rax
	movq	%rax, 16(%rdi)
.L1782:
	movq	40(%rbx), %rax
	movl	32(%rbx), %ecx
	movq	%rax, 40(%r14)
	movl	%ecx, 32(%r14)
	movl	(%rbx), %eax
	movq	$0, 16(%r14)
	movl	%eax, (%r14)
	movq	$0, 24(%r14)
	movq	%rdx, 8(%r14)
	movq	24(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L1783
	movq	-56(%rbp), %rdi
	movq	%r13, %rcx
	movq	%r14, %rdx
	call	_ZNSt8_Rb_treeIN2v88internal8compiler14VirtualContextES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE7_M_copyINSA_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS3_EPKSE_PSt18_Rb_tree_node_baseRT_
	movq	%rax, 24(%r14)
.L1783:
	movq	16(%rbx), %r12
	testq	%r12, %r12
	je	.L1780
	movq	%r14, %r15
.L1789:
	movq	0(%r13), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	$47, %rax
	jbe	.L1804
	leaq	48(%rbx), %rax
	movq	%rax, 16(%rdi)
.L1786:
	movq	40(%r12), %rax
	movl	32(%r12), %edx
	movq	%rax, 40(%rbx)
	movl	%edx, 32(%rbx)
	movl	(%r12), %eax
	movq	$0, 16(%rbx)
	movl	%eax, (%rbx)
	movq	$0, 24(%rbx)
	movq	%rbx, 16(%r15)
	movq	%r15, 8(%rbx)
	movq	24(%r12), %rsi
	testq	%rsi, %rsi
	je	.L1787
	movq	-56(%rbp), %rdi
	movq	%r13, %rcx
	movq	%rbx, %rdx
	call	_ZNSt8_Rb_treeIN2v88internal8compiler14VirtualContextES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE7_M_copyINSA_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS3_EPKSE_PSt18_Rb_tree_node_baseRT_
	movq	%rax, 24(%rbx)
	movq	16(%r12), %r12
	testq	%r12, %r12
	je	.L1780
.L1788:
	movq	%rbx, %r15
	jmp	.L1789
	.p2align 4,,10
	.p2align 3
.L1787:
	movq	16(%r12), %r12
	testq	%r12, %r12
	jne	.L1788
.L1780:
	addq	$24, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1804:
	.cfi_restore_state
	movl	$48, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rbx
	jmp	.L1786
	.p2align 4,,10
	.p2align 3
.L1803:
	movl	$48, %esi
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-64(%rbp), %rdx
	movq	%rax, %r14
	jmp	.L1782
	.cfi_endproc
.LFE24190:
	.size	_ZNSt8_Rb_treeIN2v88internal8compiler14VirtualContextES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE7_M_copyINSA_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS3_EPKSE_PSt18_Rb_tree_node_baseRT_, .-_ZNSt8_Rb_treeIN2v88internal8compiler14VirtualContextES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE7_M_copyINSA_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS3_EPKSE_PSt18_Rb_tree_node_baseRT_
	.section	.text._ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE7_M_copyINSC_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS4_EPKSG_PSt18_Rb_tree_node_baseRT_,"axG",@progbits,_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE7_M_copyINSC_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS4_EPKSG_PSt18_Rb_tree_node_baseRT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE7_M_copyINSC_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS4_EPKSG_PSt18_Rb_tree_node_baseRT_
	.type	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE7_M_copyINSC_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS4_EPKSG_PSt18_Rb_tree_node_baseRT_, @function
_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE7_M_copyINSC_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS4_EPKSG_PSt18_Rb_tree_node_baseRT_:
.LFB24194:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	(%rcx), %rax
	movq	%rdi, -56(%rbp)
	movq	(%rax), %rdi
	movq	16(%rdi), %r14
	movq	24(%rdi), %rax
	subq	%r14, %rax
	cmpq	$39, %rax
	jbe	.L1828
	leaq	40(%r14), %rax
	movq	%rax, 16(%rdi)
.L1807:
	movq	32(%rbx), %rax
	movq	%rax, 32(%r14)
	movl	(%rbx), %eax
	movq	$0, 16(%r14)
	movl	%eax, (%r14)
	movq	$0, 24(%r14)
	movq	%rdx, 8(%r14)
	movq	24(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L1808
	movq	-56(%rbp), %rdi
	movq	%r13, %rcx
	movq	%r14, %rdx
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE7_M_copyINSC_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS4_EPKSG_PSt18_Rb_tree_node_baseRT_
	movq	%rax, 24(%r14)
.L1808:
	movq	16(%rbx), %r12
	testq	%r12, %r12
	je	.L1805
	movq	%r14, %r15
.L1814:
	movq	0(%r13), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	$39, %rax
	jbe	.L1829
	leaq	40(%rbx), %rax
	movq	%rax, 16(%rdi)
.L1811:
	movq	32(%r12), %rax
	movq	%rax, 32(%rbx)
	movl	(%r12), %eax
	movq	$0, 16(%rbx)
	movl	%eax, (%rbx)
	movq	$0, 24(%rbx)
	movq	%rbx, 16(%r15)
	movq	%r15, 8(%rbx)
	movq	24(%r12), %rsi
	testq	%rsi, %rsi
	je	.L1812
	movq	-56(%rbp), %rdi
	movq	%r13, %rcx
	movq	%rbx, %rdx
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE7_M_copyINSC_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS4_EPKSG_PSt18_Rb_tree_node_baseRT_
	movq	%rax, 24(%rbx)
	movq	16(%r12), %r12
	testq	%r12, %r12
	je	.L1805
.L1813:
	movq	%rbx, %r15
	jmp	.L1814
	.p2align 4,,10
	.p2align 3
.L1812:
	movq	16(%r12), %r12
	testq	%r12, %r12
	jne	.L1813
.L1805:
	addq	$24, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1829:
	.cfi_restore_state
	movl	$40, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rbx
	jmp	.L1811
	.p2align 4,,10
	.p2align 3
.L1828:
	movl	$40, %esi
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-64(%rbp), %rdx
	movq	%rax, %r14
	jmp	.L1807
	.cfi_endproc
.LFE24194:
	.size	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE7_M_copyINSC_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS4_EPKSG_PSt18_Rb_tree_node_baseRT_, .-_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE7_M_copyINSC_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS4_EPKSG_PSt18_Rb_tree_node_baseRT_
	.section	.text._ZNSt8_Rb_treeIN2v88internal6HandleINS1_3MapEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE7_M_copyINSC_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS4_EPKSG_PSt18_Rb_tree_node_baseRT_,"axG",@progbits,_ZNSt8_Rb_treeIN2v88internal6HandleINS1_3MapEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE7_M_copyINSC_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS4_EPKSG_PSt18_Rb_tree_node_baseRT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_3MapEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE7_M_copyINSC_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS4_EPKSG_PSt18_Rb_tree_node_baseRT_
	.type	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_3MapEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE7_M_copyINSC_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS4_EPKSG_PSt18_Rb_tree_node_baseRT_, @function
_ZNSt8_Rb_treeIN2v88internal6HandleINS1_3MapEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE7_M_copyINSC_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS4_EPKSG_PSt18_Rb_tree_node_baseRT_:
.LFB24198:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	(%rcx), %rax
	movq	%rdi, -56(%rbp)
	movq	(%rax), %rdi
	movq	16(%rdi), %r14
	movq	24(%rdi), %rax
	subq	%r14, %rax
	cmpq	$39, %rax
	jbe	.L1853
	leaq	40(%r14), %rax
	movq	%rax, 16(%rdi)
.L1832:
	movq	32(%rbx), %rax
	movq	%rax, 32(%r14)
	movl	(%rbx), %eax
	movq	$0, 16(%r14)
	movl	%eax, (%r14)
	movq	$0, 24(%r14)
	movq	%rdx, 8(%r14)
	movq	24(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L1833
	movq	-56(%rbp), %rdi
	movq	%r13, %rcx
	movq	%r14, %rdx
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_3MapEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE7_M_copyINSC_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS4_EPKSG_PSt18_Rb_tree_node_baseRT_
	movq	%rax, 24(%r14)
.L1833:
	movq	16(%rbx), %r12
	testq	%r12, %r12
	je	.L1830
	movq	%r14, %r15
.L1839:
	movq	0(%r13), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	$39, %rax
	jbe	.L1854
	leaq	40(%rbx), %rax
	movq	%rax, 16(%rdi)
.L1836:
	movq	32(%r12), %rax
	movq	%rax, 32(%rbx)
	movl	(%r12), %eax
	movq	$0, 16(%rbx)
	movl	%eax, (%rbx)
	movq	$0, 24(%rbx)
	movq	%rbx, 16(%r15)
	movq	%r15, 8(%rbx)
	movq	24(%r12), %rsi
	testq	%rsi, %rsi
	je	.L1837
	movq	-56(%rbp), %rdi
	movq	%r13, %rcx
	movq	%rbx, %rdx
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_3MapEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE7_M_copyINSC_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS4_EPKSG_PSt18_Rb_tree_node_baseRT_
	movq	%rax, 24(%rbx)
	movq	16(%r12), %r12
	testq	%r12, %r12
	je	.L1830
.L1838:
	movq	%rbx, %r15
	jmp	.L1839
	.p2align 4,,10
	.p2align 3
.L1837:
	movq	16(%r12), %r12
	testq	%r12, %r12
	jne	.L1838
.L1830:
	addq	$24, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1854:
	.cfi_restore_state
	movl	$40, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rbx
	jmp	.L1836
	.p2align 4,,10
	.p2align 3
.L1853:
	movl	$40, %esi
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-64(%rbp), %rdx
	movq	%rax, %r14
	jmp	.L1832
	.cfi_endproc
.LFE24198:
	.size	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_3MapEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE7_M_copyINSC_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS4_EPKSG_PSt18_Rb_tree_node_baseRT_, .-_ZNSt8_Rb_treeIN2v88internal6HandleINS1_3MapEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE7_M_copyINSC_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS4_EPKSG_PSt18_Rb_tree_node_baseRT_
	.section	.text._ZNSt8_Rb_treeIN2v88internal8compiler17FunctionBlueprintES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE7_M_copyINSA_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS3_EPKSE_PSt18_Rb_tree_node_baseRT_,"axG",@progbits,_ZNSt8_Rb_treeIN2v88internal8compiler17FunctionBlueprintES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE7_M_copyINSA_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS3_EPKSE_PSt18_Rb_tree_node_baseRT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN2v88internal8compiler17FunctionBlueprintES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE7_M_copyINSA_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS3_EPKSE_PSt18_Rb_tree_node_baseRT_
	.type	_ZNSt8_Rb_treeIN2v88internal8compiler17FunctionBlueprintES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE7_M_copyINSA_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS3_EPKSE_PSt18_Rb_tree_node_baseRT_, @function
_ZNSt8_Rb_treeIN2v88internal8compiler17FunctionBlueprintES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE7_M_copyINSA_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS3_EPKSE_PSt18_Rb_tree_node_baseRT_:
.LFB24202:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	(%rcx), %rax
	movq	%rdi, -56(%rbp)
	movq	(%rax), %rdi
	movq	16(%rdi), %r13
	movq	24(%rdi), %rax
	subq	%r13, %rax
	cmpq	$271, %rax
	jbe	.L1878
	leaq	272(%r13), %rax
	movq	%rax, 16(%rdi)
.L1857:
	movq	32(%rbx), %rax
	leaq	48(%rbx), %rsi
	leaq	48(%r13), %rdi
	movq	%rax, 32(%r13)
	movq	40(%rbx), %rax
	movq	%rax, 40(%r13)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movl	(%rbx), %eax
	movq	$0, 16(%r13)
	movq	$0, 24(%r13)
	movl	%eax, 0(%r13)
	movq	%r12, 8(%r13)
	movq	24(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L1858
	movq	-56(%rbp), %rdi
	movq	%r14, %rcx
	movq	%r13, %rdx
	call	_ZNSt8_Rb_treeIN2v88internal8compiler17FunctionBlueprintES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE7_M_copyINSA_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS3_EPKSE_PSt18_Rb_tree_node_baseRT_
	movq	%rax, 24(%r13)
.L1858:
	movq	16(%rbx), %r12
	testq	%r12, %r12
	je	.L1855
	movq	%r13, %r15
.L1864:
	movq	(%r14), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	$271, %rax
	jbe	.L1879
	leaq	272(%rbx), %rax
	movq	%rax, 16(%rdi)
.L1861:
	movq	32(%r12), %rax
	leaq	48(%r12), %rsi
	leaq	48(%rbx), %rdi
	movq	%rax, 32(%rbx)
	movq	40(%r12), %rax
	movq	%rax, 40(%rbx)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movl	(%r12), %eax
	movq	$0, 16(%rbx)
	movq	$0, 24(%rbx)
	movl	%eax, (%rbx)
	movq	%rbx, 16(%r15)
	movq	%r15, 8(%rbx)
	movq	24(%r12), %rsi
	testq	%rsi, %rsi
	je	.L1862
	movq	-56(%rbp), %rdi
	movq	%r14, %rcx
	movq	%rbx, %rdx
	call	_ZNSt8_Rb_treeIN2v88internal8compiler17FunctionBlueprintES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE7_M_copyINSA_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS3_EPKSE_PSt18_Rb_tree_node_baseRT_
	movq	%rax, 24(%rbx)
	movq	16(%r12), %r12
	testq	%r12, %r12
	je	.L1855
.L1863:
	movq	%rbx, %r15
	jmp	.L1864
	.p2align 4,,10
	.p2align 3
.L1862:
	movq	16(%r12), %r12
	testq	%r12, %r12
	jne	.L1863
.L1855:
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1879:
	.cfi_restore_state
	movl	$272, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rbx
	jmp	.L1861
	.p2align 4,,10
	.p2align 3
.L1878:
	movl	$272, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r13
	jmp	.L1857
	.cfi_endproc
.LFE24202:
	.size	_ZNSt8_Rb_treeIN2v88internal8compiler17FunctionBlueprintES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE7_M_copyINSA_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS3_EPKSE_PSt18_Rb_tree_node_baseRT_, .-_ZNSt8_Rb_treeIN2v88internal8compiler17FunctionBlueprintES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE7_M_copyINSA_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS3_EPKSE_PSt18_Rb_tree_node_baseRT_
	.section	.text._ZN2v88internal8compiler5HintsC2ERKS2_,"axG",@progbits,_ZN2v88internal8compiler5HintsC5ERKS2_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler5HintsC2ERKS2_
	.type	_ZN2v88internal8compiler5HintsC2ERKS2_, @function
_ZN2v88internal8compiler5HintsC2ERKS2_:
.LFB18689:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16(%rdi), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movl	$0, 16(%rdi)
	movq	$0, 24(%rdi)
	movq	%rax, (%rdi)
	movq	%rdx, 32(%rdi)
	movq	%rdx, 40(%rdi)
	movq	$0, 48(%rdi)
	movq	24(%rsi), %rsi
	testq	%rsi, %rsi
	je	.L1881
	leaq	-32(%rbp), %rcx
	movq	%rdi, -32(%rbp)
	call	_ZNSt8_Rb_treeIN2v88internal8compiler14VirtualContextES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE7_M_copyINSA_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS3_EPKSE_PSt18_Rb_tree_node_baseRT_
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L1882:
	movq	%rdx, %rcx
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L1882
	movq	%rcx, 32(%rbx)
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L1883:
	movq	%rdx, %rcx
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L1883
	movq	48(%r12), %rdx
	movq	%rcx, 40(%rbx)
	movq	%rax, 24(%rbx)
	movq	%rdx, 48(%rbx)
.L1881:
	movq	56(%r12), %rax
	leaq	72(%rbx), %rdx
	movl	$0, 72(%rbx)
	movq	$0, 80(%rbx)
	movq	%rax, 56(%rbx)
	movq	%rdx, 88(%rbx)
	movq	%rdx, 96(%rbx)
	movq	$0, 104(%rbx)
	movq	80(%r12), %rsi
	testq	%rsi, %rsi
	je	.L1884
	leaq	56(%rbx), %rdi
	leaq	-32(%rbp), %rcx
	movq	%rdi, -32(%rbp)
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE7_M_copyINSC_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS4_EPKSG_PSt18_Rb_tree_node_baseRT_
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L1885:
	movq	%rdx, %rcx
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L1885
	movq	%rcx, 88(%rbx)
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L1886:
	movq	%rdx, %rcx
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L1886
	movq	104(%r12), %rdx
	movq	%rcx, 96(%rbx)
	movq	%rax, 80(%rbx)
	movq	%rdx, 104(%rbx)
.L1884:
	movq	112(%r12), %rax
	leaq	128(%rbx), %rdx
	movl	$0, 128(%rbx)
	movq	$0, 136(%rbx)
	movq	%rax, 112(%rbx)
	movq	%rdx, 144(%rbx)
	movq	%rdx, 152(%rbx)
	movq	$0, 160(%rbx)
	movq	136(%r12), %rsi
	testq	%rsi, %rsi
	je	.L1887
	leaq	112(%rbx), %rdi
	leaq	-32(%rbp), %rcx
	movq	%rdi, -32(%rbp)
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_3MapEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE7_M_copyINSC_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS4_EPKSG_PSt18_Rb_tree_node_baseRT_
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L1888:
	movq	%rdx, %rcx
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L1888
	movq	%rcx, 144(%rbx)
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L1889:
	movq	%rdx, %rcx
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L1889
	movq	160(%r12), %rdx
	movq	%rcx, 152(%rbx)
	movq	%rax, 136(%rbx)
	movq	%rdx, 160(%rbx)
.L1887:
	movq	168(%r12), %rax
	leaq	184(%rbx), %rdx
	movl	$0, 184(%rbx)
	movq	$0, 192(%rbx)
	movq	%rax, 168(%rbx)
	movq	%rdx, 200(%rbx)
	movq	%rdx, 208(%rbx)
	movq	$0, 216(%rbx)
	movq	192(%r12), %rsi
	testq	%rsi, %rsi
	je	.L1880
	leaq	168(%rbx), %rdi
	leaq	-32(%rbp), %rcx
	movq	%rdi, -32(%rbp)
	call	_ZNSt8_Rb_treeIN2v88internal8compiler17FunctionBlueprintES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE7_M_copyINSA_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS3_EPKSE_PSt18_Rb_tree_node_baseRT_
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L1891:
	movq	%rdx, %rcx
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L1891
	movq	%rcx, 200(%rbx)
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L1892:
	movq	%rdx, %rcx
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L1892
	movq	216(%r12), %rdx
	movq	%rcx, 208(%rbx)
	movq	%rax, 192(%rbx)
	movq	%rdx, 216(%rbx)
.L1880:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1915
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1915:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18689:
	.size	_ZN2v88internal8compiler5HintsC2ERKS2_, .-_ZN2v88internal8compiler5HintsC2ERKS2_
	.weak	_ZN2v88internal8compiler5HintsC1ERKS2_
	.set	_ZN2v88internal8compiler5HintsC1ERKS2_,_ZN2v88internal8compiler5HintsC2ERKS2_
	.section	.text._ZN2v88internal8compiler17FunctionBlueprintC2ENS0_6HandleINS0_18SharedFunctionInfoEEENS3_INS0_14FeedbackVectorEEERKNS1_5HintsE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17FunctionBlueprintC2ENS0_6HandleINS0_18SharedFunctionInfoEEENS3_INS0_14FeedbackVectorEEERKNS1_5HintsE
	.type	_ZN2v88internal8compiler17FunctionBlueprintC2ENS0_6HandleINS0_18SharedFunctionInfoEEENS3_INS0_14FeedbackVectorEEERKNS1_5HintsE, @function
_ZN2v88internal8compiler17FunctionBlueprintC2ENS0_6HandleINS0_18SharedFunctionInfoEEENS3_INS0_14FeedbackVectorEEERKNS1_5HintsE:
.LFB18724:
	.cfi_startproc
	endbr64
	movq	%rsi, %r8
	movq	%rdx, 8(%rdi)
	movq	%rcx, %rsi
	addq	$16, %rdi
	movq	%r8, -16(%rdi)
	jmp	_ZN2v88internal8compiler5HintsC1ERKS2_
	.cfi_endproc
.LFE18724:
	.size	_ZN2v88internal8compiler17FunctionBlueprintC2ENS0_6HandleINS0_18SharedFunctionInfoEEENS3_INS0_14FeedbackVectorEEERKNS1_5HintsE, .-_ZN2v88internal8compiler17FunctionBlueprintC2ENS0_6HandleINS0_18SharedFunctionInfoEEENS3_INS0_14FeedbackVectorEEERKNS1_5HintsE
	.globl	_ZN2v88internal8compiler17FunctionBlueprintC1ENS0_6HandleINS0_18SharedFunctionInfoEEENS3_INS0_14FeedbackVectorEEERKNS1_5HintsE
	.set	_ZN2v88internal8compiler17FunctionBlueprintC1ENS0_6HandleINS0_18SharedFunctionInfoEEENS3_INS0_14FeedbackVectorEEERKNS1_5HintsE,_ZN2v88internal8compiler17FunctionBlueprintC2ENS0_6HandleINS0_18SharedFunctionInfoEEENS3_INS0_14FeedbackVectorEEERKNS1_5HintsE
	.section	.text._ZNK2v88internal8compiler34SerializerForBackgroundCompilation15feedback_vectorEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler34SerializerForBackgroundCompilation15feedback_vectorEv
	.type	_ZNK2v88internal8compiler34SerializerForBackgroundCompilation15feedback_vectorEv, @function
_ZNK2v88internal8compiler34SerializerForBackgroundCompilation15feedback_vectorEv:
.LFB18907:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-256(%rbp), %r12
	subq	$256, %rsp
	movq	24(%rdi), %rsi
	movq	%r12, %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	8(%rsi), %rax
	addq	$24, %rsi
	movq	%rax, -272(%rbp)
	movq	-8(%rsi), %rax
	movq	%rax, -264(%rbp)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movq	%r12, %rdi
	movq	-264(%rbp), %r13
	call	_ZN2v88internal8compiler5HintsD1Ev
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1920
	addq	$256, %rsp
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1920:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18907:
	.size	_ZNK2v88internal8compiler34SerializerForBackgroundCompilation15feedback_vectorEv, .-_ZNK2v88internal8compiler34SerializerForBackgroundCompilation15feedback_vectorEv
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation19ProcessGlobalAccessENS0_12FeedbackSlotEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation19ProcessGlobalAccessENS0_12FeedbackSlotEb
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation19ProcessGlobalAccessENS0_12FeedbackSlotEb, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation19ProcessGlobalAccessENS0_12FeedbackSlotEb:
.LFB19054:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$64, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$-1, %esi
	je	.L1921
	movq	%rdi, %rbx
	movl	%esi, %r12d
	movl	%edx, %r13d
	call	_ZNK2v88internal8compiler34SerializerForBackgroundCompilation15feedback_vectorEv
	testq	%rax, %rax
	je	.L1921
	movq	%rbx, %rdi
	leaq	-80(%rbp), %r14
	call	_ZNK2v88internal8compiler34SerializerForBackgroundCompilation15feedback_vectorEv
	movl	%r12d, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS0_6HandleINS0_14FeedbackVectorEEENS0_12FeedbackSlotE@PLT
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler12JSHeapBroker30ProcessFeedbackForGlobalAccessERKNS1_14FeedbackSourceE@PLT
	movq	%rax, %r12
	testb	%r13b, %r13b
	jne	.L1934
.L1921:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1935
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1934:
	.cfi_restore_state
	movq	24(%rbx), %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment17accumulator_hintsEv
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler5Hints5ClearEv
	cmpl	$6, (%r12)
	jne	.L1921
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler17ProcessedFeedback14AsGlobalAccessEv@PLT
	leaq	-64(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZNK2v88internal8compiler20GlobalAccessFeedback15GetConstantHintEv@PLT
	cmpb	$0, -64(%rbp)
	je	.L1921
	movq	24(%rbx), %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment17accumulator_hintsEv
	leaq	-56(%rbp), %rdi
	movq	%rax, %rbx
	call	_ZNK2v88internal8compiler9ObjectRef6objectEv@PLT
	leaq	-88(%rbp), %rsi
	leaq	56(%rbx), %rdi
	movq	%rax, -88(%rbp)
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_
	jmp	.L1921
.L1935:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19054:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation19ProcessGlobalAccessENS0_12FeedbackSlotEb, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation19ProcessGlobalAccessENS0_12FeedbackSlotEb
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation14VisitLdaGlobalEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation14VisitLdaGlobalEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation14VisitLdaGlobalEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation14VisitLdaGlobalEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB19055:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	xorl	%esi, %esi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%r13, %rdi
	movq	(%rax), %rdx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor26GetConstantForIndexOperandEiPNS0_7IsolateE@PLT
	movq	(%r12), %rsi
	movl	$1, %ecx
	leaq	-48(%rbp), %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler7NameRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	movl	$1, %edx
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation19ProcessGlobalAccessENS0_12FeedbackSlotEb
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1939
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1939:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19055:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation14VisitLdaGlobalEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation14VisitLdaGlobalEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation14VisitStaGlobalEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation14VisitStaGlobalEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation14VisitStaGlobalEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation14VisitStaGlobalEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB19063:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	xorl	%esi, %esi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%r13, %rdi
	movq	(%rax), %rdx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor26GetConstantForIndexOperandEiPNS0_7IsolateE@PLT
	movq	(%r12), %rsi
	movl	$1, %ecx
	leaq	-48(%rbp), %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler7NameRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation19ProcessGlobalAccessENS0_12FeedbackSlotEb
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1943
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1943:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19063:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation14VisitStaGlobalEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation14VisitStaGlobalEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation36VisitLdaLookupGlobalSlotInsideTypeofEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation36VisitLdaLookupGlobalSlotInsideTypeofEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation36VisitLdaLookupGlobalSlotInsideTypeofEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation36VisitLdaLookupGlobalSlotInsideTypeofEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25563:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movl	$2, %esi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor27GetUnsignedImmediateOperandEi@PLT
	movl	%eax, %ebx
	movq	24(%r12), %rax
	testl	%ebx, %ebx
	jle	.L1945
	leaq	480(%rax), %r15
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L1946:
	movl	%r14d, %ecx
	xorl	%r8d, %r8d
	movl	$2, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	addl	$1, %r14d
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation20ProcessContextAccessERKNS1_5HintsEiiNS2_21ContextProcessingModeEPS3_.constprop.0
	cmpl	%r14d, %ebx
	jne	.L1946
.L1945:
	movq	(%r12), %rax
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	(%rax), %rdx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor26GetConstantForIndexOperandEiPNS0_7IsolateE@PLT
	movq	(%r12), %rsi
	movl	$1, %ecx
	leaq	-80(%rbp), %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler7NameRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	movl	$1, %edx
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation19ProcessGlobalAccessENS0_12FeedbackSlotEb
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1950
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1950:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25563:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation36VisitLdaLookupGlobalSlotInsideTypeofEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation36VisitLdaLookupGlobalSlotInsideTypeofEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation26VisitLdaGlobalInsideTypeofEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation26VisitLdaGlobalInsideTypeofEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation26VisitLdaGlobalInsideTypeofEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation26VisitLdaGlobalInsideTypeofEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB19056:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	xorl	%esi, %esi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%r13, %rdi
	movq	(%rax), %rdx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor26GetConstantForIndexOperandEiPNS0_7IsolateE@PLT
	movq	(%r12), %rsi
	movl	$1, %ecx
	leaq	-48(%rbp), %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler7NameRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	movl	$1, %edx
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation19ProcessGlobalAccessENS0_12FeedbackSlotEb
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1954
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1954:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19056:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation26VisitLdaGlobalInsideTypeofEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation26VisitLdaGlobalInsideTypeofEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation24VisitLdaLookupGlobalSlotEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation24VisitLdaLookupGlobalSlotEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation24VisitLdaLookupGlobalSlotEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation24VisitLdaLookupGlobalSlotEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB19061:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movl	$2, %esi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor27GetUnsignedImmediateOperandEi@PLT
	movl	%eax, %ebx
	movq	24(%r12), %rax
	testl	%ebx, %ebx
	jle	.L1956
	leaq	480(%rax), %r15
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L1957:
	movl	%r14d, %ecx
	xorl	%r8d, %r8d
	movl	$2, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	addl	$1, %r14d
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation20ProcessContextAccessERKNS1_5HintsEiiNS2_21ContextProcessingModeEPS3_.constprop.0
	cmpl	%r14d, %ebx
	jne	.L1957
.L1956:
	movq	(%r12), %rax
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	(%rax), %rdx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor26GetConstantForIndexOperandEiPNS0_7IsolateE@PLT
	movq	(%r12), %rsi
	movl	$1, %ecx
	leaq	-80(%rbp), %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler7NameRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	movl	$1, %edx
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation19ProcessGlobalAccessENS0_12FeedbackSlotEb
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1961
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1961:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19061:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation24VisitLdaLookupGlobalSlotEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation24VisitLdaLookupGlobalSlotEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation26ProcessLdaLookupGlobalSlotEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation26ProcessLdaLookupGlobalSlotEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation26ProcessLdaLookupGlobalSlotEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation26ProcessLdaLookupGlobalSlotEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB19060:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movl	$2, %esi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor27GetUnsignedImmediateOperandEi@PLT
	movl	%eax, %ebx
	movq	24(%r12), %rax
	testl	%ebx, %ebx
	jle	.L1963
	leaq	480(%rax), %r15
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L1964:
	movl	%r14d, %ecx
	xorl	%r8d, %r8d
	movl	$2, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	addl	$1, %r14d
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation20ProcessContextAccessERKNS1_5HintsEiiNS2_21ContextProcessingModeEPS3_.constprop.0
	cmpl	%r14d, %ebx
	jne	.L1964
.L1963:
	movq	(%r12), %rax
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	(%rax), %rdx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor26GetConstantForIndexOperandEiPNS0_7IsolateE@PLT
	movq	(%r12), %rsi
	movl	$1, %ecx
	leaq	-80(%rbp), %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler7NameRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	movl	$1, %edx
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation19ProcessGlobalAccessENS0_12FeedbackSlotEb
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1968
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1968:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19060:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation26ProcessLdaLookupGlobalSlotEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation26ProcessLdaLookupGlobalSlotEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation12ProcessForInENS0_12FeedbackSlotE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation12ProcessForInENS0_12FeedbackSlotE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation12ProcessForInENS0_12FeedbackSlotE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation12ProcessForInENS0_12FeedbackSlotE:
.LFB19069:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$-1, %esi
	je	.L1969
	movq	%rdi, %rbx
	movl	%esi, %r12d
	call	_ZNK2v88internal8compiler34SerializerForBackgroundCompilation15feedback_vectorEv
	testq	%rax, %rax
	je	.L1969
	movq	%rbx, %rdi
	leaq	-64(%rbp), %r13
	call	_ZNK2v88internal8compiler34SerializerForBackgroundCompilation15feedback_vectorEv
	movl	%r12d, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS0_6HandleINS0_14FeedbackVectorEEENS0_12FeedbackSlotE@PLT
	movq	(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler12JSHeapBroker23ProcessFeedbackForForInERKNS1_14FeedbackSourceE@PLT
	testb	$1, 96(%rbx)
	movq	24(%rbx), %r12
	je	.L1974
	cmpl	$-1, 100(%rbx)
	jne	.L1974
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L1974
	movq	936(%r12), %r14
	movq	944(%r12), %r13
	cmpq	%r13, %r14
	je	.L1969
	movq	%r14, %rbx
	.p2align 4,,10
	.p2align 3
.L1975:
	movq	%rbx, %rdi
	addq	$224, %rbx
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	%rbx, %r13
	jne	.L1975
	movq	%r14, 944(%r12)
	.p2align 4,,10
	.p2align 3
.L1969:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1983
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1974:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment17accumulator_hintsEv
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler5Hints5ClearEv
	jmp	.L1969
.L1983:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19069:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation12ProcessForInENS0_12FeedbackSlotE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation12ProcessForInENS0_12FeedbackSlotE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation14VisitForInNextEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation14VisitForInNextEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation14VisitForInNextEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation14VisitForInNextEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB18964:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	movl	$3, %esi
	subq	$8, %rsp
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	%eax, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler34SerializerForBackgroundCompilation12ProcessForInENS0_12FeedbackSlotE
	.cfi_endproc
.LFE18964:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation14VisitForInNextEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation14VisitForInNextEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation17VisitForInPrepareEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation17VisitForInPrepareEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation17VisitForInPrepareEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation17VisitForInPrepareEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB18965:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	movl	$1, %esi
	subq	$8, %rsp
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	%eax, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler34SerializerForBackgroundCompilation12ProcessForInENS0_12FeedbackSlotE
	.cfi_endproc
.LFE18965:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation17VisitForInPrepareEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation17VisitForInPrepareEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation29ProcessUnaryOrBinaryOperationENS0_12FeedbackSlotEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation29ProcessUnaryOrBinaryOperationENS0_12FeedbackSlotEb
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation29ProcessUnaryOrBinaryOperationENS0_12FeedbackSlotEb, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation29ProcessUnaryOrBinaryOperationENS0_12FeedbackSlotEb:
.LFB19070:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$-1, %esi
	je	.L1988
	movq	%rdi, %rbx
	movl	%esi, %r12d
	movl	%edx, %r13d
	call	_ZNK2v88internal8compiler34SerializerForBackgroundCompilation15feedback_vectorEv
	testq	%rax, %rax
	je	.L1988
	movq	%rbx, %rdi
	leaq	-64(%rbp), %r14
	call	_ZNK2v88internal8compiler34SerializerForBackgroundCompilation15feedback_vectorEv
	movl	%r12d, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS0_6HandleINS0_14FeedbackVectorEEENS0_12FeedbackSlotE@PLT
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler12JSHeapBroker33ProcessFeedbackForBinaryOperationERKNS1_14FeedbackSourceE@PLT
	movq	%rax, %rsi
	testb	%r13b, %r13b
	je	.L1994
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation22BailoutOnUninitializedERKNS1_17ProcessedFeedbackE
	testb	%al, %al
	jne	.L1988
.L1994:
	movq	24(%rbx), %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment17accumulator_hintsEv
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler5Hints5ClearEv
.L1988:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2003
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2003:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19070:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation29ProcessUnaryOrBinaryOperationENS0_12FeedbackSlotEb, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation29ProcessUnaryOrBinaryOperationENS0_12FeedbackSlotEb
	.section	.text._ZNK2v88internal8compiler34SerializerForBackgroundCompilation14bytecode_arrayEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler34SerializerForBackgroundCompilation14bytecode_arrayEv
	.type	_ZNK2v88internal8compiler34SerializerForBackgroundCompilation14bytecode_arrayEv, @function
_ZNK2v88internal8compiler34SerializerForBackgroundCompilation14bytecode_arrayEv:
.LFB18908:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-272(%rbp), %r13
	pushq	%rbx
	subq	$296, %rsp
	.cfi_offset 3, -40
	movq	24(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%r13, %rdi
	addq	$24, %rsi
	movq	(%rax), %rbx
	movq	-16(%rsi), %rax
	movq	%rax, -288(%rbp)
	movq	-8(%rsi), %rax
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movq	-288(%rbp), %rax
	leaq	-296(%rbp), %rdi
	movq	(%rax), %rax
	movq	%rax, -296(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo16GetBytecodeArrayEv
	movq	41112(%rbx), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L2005
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r12
.L2006:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2010
	addq	$296, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2005:
	.cfi_restore_state
	movq	41088(%rbx), %r12
	cmpq	41096(%rbx), %r12
	je	.L2011
.L2007:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r12)
	jmp	.L2006
	.p2align 4,,10
	.p2align 3
.L2011:
	movq	%rbx, %rdi
	movq	%rax, -312(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-312(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L2007
.L2010:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18908:
	.size	_ZNK2v88internal8compiler34SerializerForBackgroundCompilation14bytecode_arrayEv, .-_ZNK2v88internal8compiler34SerializerForBackgroundCompilation14bytecode_arrayEv
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation19GetBytecodeAnalysisENS1_19SerializationPolicyE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation19GetBytecodeAnalysisENS1_19SerializationPolicyE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation19GetBytecodeAnalysisENS1_19SerializationPolicyE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation19GetBytecodeAnalysisENS1_19SerializationPolicyE:
.LFB18909:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	96(%rdi), %r12d
	movl	%esi, %ebx
	movl	100(%rdi), %r14d
	movq	(%rdi), %r13
	shrl	$2, %r12d
	call	_ZNK2v88internal8compiler34SerializerForBackgroundCompilation14bytecode_arrayEv
	andl	$1, %r12d
	movl	%ebx, %r8d
	movl	%r14d, %edx
	popq	%rbx
	movl	%r12d, %ecx
	movq	%r13, %rdi
	popq	%r12
	movq	%rax, %rsi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler12JSHeapBroker19GetBytecodeAnalysisENS0_6HandleINS0_13BytecodeArrayEEENS0_9BailoutIdEbNS1_19SerializationPolicyE@PLT
	.cfi_endproc
.LFE18909:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation19GetBytecodeAnalysisENS1_19SerializationPolicyE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation19GetBytecodeAnalysisENS1_19SerializationPolicyE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation22VisitGetTemplateObjectEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation22VisitGetTemplateObjectEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation22VisitGetTemplateObjectEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation22VisitGetTemplateObjectEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB18922:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-288(%rbp), %r15
	leaq	-320(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$328, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%r12, %rdi
	movq	(%rax), %rdx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor26GetConstantForIndexOperandEiPNS0_7IsolateE@PLT
	movq	(%rbx), %rsi
	movl	$1, %ecx
	leaq	-352(%rbp), %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler9ObjectRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	leaq	-304(%rbp), %r12
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	movq	%rbx, %rdi
	movl	%eax, %r13d
	call	_ZNK2v88internal8compiler34SerializerForBackgroundCompilation15feedback_vectorEv
	movq	(%rbx), %rsi
	movl	$1, %ecx
	leaq	-336(%rbp), %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler17FeedbackVectorRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	24(%rbx), %rsi
	movq	%r15, %rdi
	movq	8(%rsi), %rax
	addq	$24, %rsi
	movq	%rax, -304(%rbp)
	movq	-8(%rsi), %rax
	movq	%rax, -296(%rbp)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movq	(%rbx), %rsi
	movl	$1, %ecx
	movq	%r14, %rdi
	movq	-304(%rbp), %rdx
	call	_ZN2v88internal8compiler21SharedFunctionInfoRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	subq	$8, %rsp
	movl	%r13d, %r9d
	movq	-336(%rbp), %rcx
	pushq	$1
	movq	-328(%rbp), %r8
	movq	%r14, %rdi
	movq	-352(%rbp), %rsi
	movq	-344(%rbp), %rdx
	call	_ZN2v88internal8compiler21SharedFunctionInfoRef17GetTemplateObjectENS1_9ObjectRefENS1_17FeedbackVectorRefENS0_12FeedbackSlotENS1_19SerializationPolicyE@PLT
	movq	24(%rbx), %rdi
	movq	%rdx, -296(%rbp)
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment17accumulator_hintsEv
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler5Hints5ClearEv
	movq	24(%rbx), %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment17accumulator_hintsEv
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZNK2v88internal8compiler10JSArrayRef6objectEv@PLT
	leaq	-360(%rbp), %rsi
	leaq	56(%rbx), %rdi
	movq	%rax, -360(%rbp)
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2017
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2017:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18922:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation22VisitGetTemplateObjectEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation22VisitGetTemplateObjectEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation23ProcessCompareOperationENS0_12FeedbackSlotE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation23ProcessCompareOperationENS0_12FeedbackSlotE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation23ProcessCompareOperationENS0_12FeedbackSlotE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation23ProcessCompareOperationENS0_12FeedbackSlotE:
.LFB19068:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$272, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$-1, %esi
	je	.L2018
	movq	%rdi, %rbx
	movl	%esi, %r12d
	call	_ZNK2v88internal8compiler34SerializerForBackgroundCompilation15feedback_vectorEv
	testq	%rax, %rax
	je	.L2018
	movq	24(%rbx), %rsi
	leaq	-272(%rbp), %r14
	leaq	-304(%rbp), %r13
	movq	%r14, %rdi
	movq	8(%rsi), %rax
	addq	$24, %rsi
	movq	%rax, -288(%rbp)
	movq	-8(%rsi), %rax
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movq	-280(%rbp), %rsi
	movl	%r12d, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS0_6HandleINS0_14FeedbackVectorEEENS0_12FeedbackSlotE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	movq	(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler12JSHeapBroker34ProcessFeedbackForCompareOperationERKNS1_14FeedbackSourceE@PLT
	testb	$1, 96(%rbx)
	movq	24(%rbx), %r12
	je	.L2023
	cmpl	$-1, 100(%rbx)
	jne	.L2023
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L2023
	movq	936(%r12), %r14
	movq	944(%r12), %r13
	cmpq	%r13, %r14
	je	.L2018
	movq	%r14, %rbx
	.p2align 4,,10
	.p2align 3
.L2024:
	movq	%rbx, %rdi
	addq	$224, %rbx
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	%rbx, %r13
	jne	.L2024
	movq	%r14, 944(%r12)
	.p2align 4,,10
	.p2align 3
.L2018:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2032
	addq	$272, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2023:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment17accumulator_hintsEv
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler5Hints5ClearEv
	jmp	.L2018
.L2032:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19068:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation23ProcessCompareOperationENS0_12FeedbackSlotE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation23ProcessCompareOperationENS0_12FeedbackSlotE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation14VisitTestEqualEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation14VisitTestEqualEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation14VisitTestEqualEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation14VisitTestEqualEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB19191:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	movl	$1, %esi
	subq	$8, %rsp
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	%eax, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler34SerializerForBackgroundCompilation23ProcessCompareOperationENS0_12FeedbackSlotE
	.cfi_endproc
.LFE19191:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation14VisitTestEqualEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation14VisitTestEqualEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation20VisitTestEqualStrictEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation20VisitTestEqualStrictEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation20VisitTestEqualStrictEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation20VisitTestEqualStrictEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25549:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	movl	$1, %esi
	subq	$8, %rsp
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	%eax, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler34SerializerForBackgroundCompilation23ProcessCompareOperationENS0_12FeedbackSlotE
	.cfi_endproc
.LFE25549:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation20VisitTestEqualStrictEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation20VisitTestEqualStrictEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation17VisitTestLessThanEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation17VisitTestLessThanEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation17VisitTestLessThanEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation17VisitTestLessThanEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25555:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	movl	$1, %esi
	subq	$8, %rsp
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	%eax, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler34SerializerForBackgroundCompilation23ProcessCompareOperationENS0_12FeedbackSlotE
	.cfi_endproc
.LFE25555:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation17VisitTestLessThanEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation17VisitTestLessThanEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation20VisitTestGreaterThanEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation20VisitTestGreaterThanEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation20VisitTestGreaterThanEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation20VisitTestGreaterThanEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25551:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	movl	$1, %esi
	subq	$8, %rsp
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	%eax, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler34SerializerForBackgroundCompilation23ProcessCompareOperationENS0_12FeedbackSlotE
	.cfi_endproc
.LFE25551:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation20VisitTestGreaterThanEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation20VisitTestGreaterThanEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation24VisitTestLessThanOrEqualEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation24VisitTestLessThanOrEqualEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation24VisitTestLessThanOrEqualEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation24VisitTestLessThanOrEqualEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25557:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	movl	$1, %esi
	subq	$8, %rsp
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	%eax, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler34SerializerForBackgroundCompilation23ProcessCompareOperationENS0_12FeedbackSlotE
	.cfi_endproc
.LFE25557:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation24VisitTestLessThanOrEqualEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation24VisitTestLessThanOrEqualEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation27VisitTestGreaterThanOrEqualEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation27VisitTestGreaterThanOrEqualEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation27VisitTestGreaterThanOrEqualEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation27VisitTestGreaterThanOrEqualEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25553:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	movl	$1, %esi
	subq	$8, %rsp
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	%eax, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler34SerializerForBackgroundCompilation23ProcessCompareOperationENS0_12FeedbackSlotE
	.cfi_endproc
.LFE25553:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation27VisitTestGreaterThanOrEqualEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation27VisitTestGreaterThanOrEqualEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZNSt6vectorIN2v88internal8compiler5HintsENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal8compiler5HintsENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal8compiler5HintsENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal8compiler5HintsENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_, @function
_ZNSt6vectorIN2v88internal8compiler5HintsENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_:
.LFB23029:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r9
	movabsq	$7905747460161236407, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %rbx
	movq	8(%rdi), %r15
	movq	%rbx, %rax
	subq	%r15, %rax
	sarq	$5, %rax
	imulq	%rdx, %rax
	cmpq	$9586980, %rax
	je	.L2084
	movq	%rsi, %rdx
	movq	%rdi, %r14
	movq	%rsi, %rcx
	subq	%r15, %rdx
	testq	%rax, %rax
	je	.L2074
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L2085
	movl	$2147483520, %esi
	movl	$2147483520, %r12d
.L2047:
	movq	(%r14), %rdi
	movq	16(%rdi), %r13
	movq	24(%rdi), %rax
	subq	%r13, %rax
	cmpq	%rsi, %rax
	jb	.L2086
	addq	%r13, %rsi
	movq	%rsi, 16(%rdi)
.L2050:
	leaq	0(%r13,%r12), %rax
	leaq	224(%r13), %r12
	movq	%rax, -56(%rbp)
	jmp	.L2048
	.p2align 4,,10
	.p2align 3
.L2085:
	testq	%rsi, %rsi
	jne	.L2087
	movq	$0, -56(%rbp)
	movl	$224, %r12d
	xorl	%r13d, %r13d
.L2048:
	leaq	0(%r13,%rdx), %rdi
	movq	%r9, %rsi
	movq	%rcx, -64(%rbp)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movq	-64(%rbp), %rcx
	cmpq	%r15, %rcx
	je	.L2051
	movq	%r15, %rax
	movq	%r13, %rdx
	jmp	.L2060
	.p2align 4,,10
	.p2align 3
.L2089:
	movl	16(%rax), %r9d
	movq	%rsi, 24(%rdx)
	movl	%r9d, 16(%rdx)
	movq	32(%rax), %r9
	movq	%r9, 32(%rdx)
	movq	40(%rax), %r9
	movq	%r9, 40(%rdx)
	movq	%rdi, 8(%rsi)
	movq	48(%rax), %rsi
	movq	%rsi, 48(%rdx)
	leaq	16(%rax), %rsi
	movq	$0, 24(%rax)
	movq	%rsi, 32(%rax)
	movq	%rsi, 40(%rax)
	movq	$0, 48(%rax)
.L2053:
	movq	56(%rax), %rsi
	leaq	72(%rdx), %rdi
	movq	%rsi, 56(%rdx)
	movq	80(%rax), %rsi
	testq	%rsi, %rsi
	je	.L2054
	movl	72(%rax), %r9d
	movq	%rsi, 80(%rdx)
	movl	%r9d, 72(%rdx)
	movq	88(%rax), %r9
	movq	%r9, 88(%rdx)
	movq	96(%rax), %r9
	movq	%r9, 96(%rdx)
	movq	%rdi, 8(%rsi)
	movq	104(%rax), %rsi
	movq	%rsi, 104(%rdx)
	leaq	72(%rax), %rsi
	movq	$0, 80(%rax)
	movq	%rsi, 88(%rax)
	movq	%rsi, 96(%rax)
	movq	$0, 104(%rax)
.L2055:
	movq	112(%rax), %rsi
	leaq	128(%rdx), %rdi
	movq	%rsi, 112(%rdx)
	movq	136(%rax), %rsi
	testq	%rsi, %rsi
	je	.L2056
	movl	128(%rax), %r9d
	movq	%rsi, 136(%rdx)
	movl	%r9d, 128(%rdx)
	movq	144(%rax), %r9
	movq	%r9, 144(%rdx)
	movq	152(%rax), %r9
	movq	%r9, 152(%rdx)
	movq	%rdi, 8(%rsi)
	movq	160(%rax), %rsi
	movq	%rsi, 160(%rdx)
	leaq	128(%rax), %rsi
	movq	$0, 136(%rax)
	movq	%rsi, 144(%rax)
	movq	%rsi, 152(%rax)
	movq	$0, 160(%rax)
.L2057:
	movq	168(%rax), %rsi
	leaq	184(%rdx), %rdi
	movq	%rsi, 168(%rdx)
	movq	192(%rax), %rsi
	testq	%rsi, %rsi
	je	.L2058
	movl	184(%rax), %r9d
	movq	%rsi, 192(%rdx)
	movl	%r9d, 184(%rdx)
	movq	200(%rax), %r9
	movq	%r9, 200(%rdx)
	movq	208(%rax), %r9
	movq	%r9, 208(%rdx)
	movq	%rdi, 8(%rsi)
	movq	216(%rax), %rsi
	movq	%rsi, 216(%rdx)
	leaq	184(%rax), %rsi
	movq	$0, 192(%rax)
	movq	%rsi, 200(%rax)
	movq	%rsi, 208(%rax)
	movq	$0, 216(%rax)
.L2059:
	addq	$224, %rax
	addq	$224, %rdx
	cmpq	%rax, %rcx
	je	.L2088
.L2060:
	movq	(%rax), %rsi
	leaq	16(%rdx), %rdi
	movq	%rsi, (%rdx)
	movq	24(%rax), %rsi
	testq	%rsi, %rsi
	jne	.L2089
	movl	$0, 16(%rdx)
	movq	$0, 24(%rdx)
	movq	%rdi, 32(%rdx)
	movq	%rdi, 40(%rdx)
	movq	$0, 48(%rdx)
	jmp	.L2053
	.p2align 4,,10
	.p2align 3
.L2058:
	movl	$0, 184(%rdx)
	movq	$0, 192(%rdx)
	movq	%rdi, 200(%rdx)
	movq	%rdi, 208(%rdx)
	movq	$0, 216(%rdx)
	jmp	.L2059
	.p2align 4,,10
	.p2align 3
.L2056:
	movl	$0, 128(%rdx)
	movq	$0, 136(%rdx)
	movq	%rdi, 144(%rdx)
	movq	%rdi, 152(%rdx)
	movq	$0, 160(%rdx)
	jmp	.L2057
	.p2align 4,,10
	.p2align 3
.L2054:
	movl	$0, 72(%rdx)
	movq	$0, 80(%rdx)
	movq	%rdi, 88(%rdx)
	movq	%rdi, 96(%rdx)
	movq	$0, 104(%rdx)
	jmp	.L2055
	.p2align 4,,10
	.p2align 3
.L2088:
	leaq	-224(%rcx), %rax
	movabsq	$411757680216731063, %rdx
	subq	%r15, %rax
	shrq	$5, %rax
	imulq	%rdx, %rax
	movabsq	$576460752303423487, %rdx
	andq	%rdx, %rax
	addq	$2, %rax
	leaq	0(,%rax,8), %r12
	subq	%rax, %r12
	salq	$5, %r12
	addq	%r13, %r12
.L2051:
	movq	%rcx, %rax
	movq	%r12, %rdx
	cmpq	%rbx, %rcx
	jne	.L2072
	jmp	.L2083
	.p2align 4,,10
	.p2align 3
.L2091:
	movl	16(%rax), %r9d
	movq	%rsi, 24(%rdx)
	movl	%r9d, 16(%rdx)
	movq	32(%rax), %r9
	movq	%r9, 32(%rdx)
	movq	40(%rax), %r9
	movq	%r9, 40(%rdx)
	movq	%rdi, 8(%rsi)
	movq	48(%rax), %rsi
	movq	%rsi, 48(%rdx)
	leaq	16(%rax), %rsi
	movq	$0, 24(%rax)
	movq	%rsi, 32(%rax)
	movq	%rsi, 40(%rax)
	movq	$0, 48(%rax)
.L2065:
	movq	56(%rax), %rsi
	leaq	72(%rdx), %rdi
	movq	%rsi, 56(%rdx)
	movq	80(%rax), %rsi
	testq	%rsi, %rsi
	je	.L2066
	movl	72(%rax), %r9d
	movq	%rsi, 80(%rdx)
	movl	%r9d, 72(%rdx)
	movq	88(%rax), %r9
	movq	%r9, 88(%rdx)
	movq	96(%rax), %r9
	movq	%r9, 96(%rdx)
	movq	%rdi, 8(%rsi)
	movq	104(%rax), %rsi
	movq	%rsi, 104(%rdx)
	leaq	72(%rax), %rsi
	movq	$0, 80(%rax)
	movq	%rsi, 88(%rax)
	movq	%rsi, 96(%rax)
	movq	$0, 104(%rax)
.L2067:
	movq	112(%rax), %rsi
	leaq	128(%rdx), %rdi
	movq	%rsi, 112(%rdx)
	movq	136(%rax), %rsi
	testq	%rsi, %rsi
	je	.L2068
	movl	128(%rax), %r9d
	movq	%rsi, 136(%rdx)
	movl	%r9d, 128(%rdx)
	movq	144(%rax), %r9
	movq	%r9, 144(%rdx)
	movq	152(%rax), %r9
	movq	%r9, 152(%rdx)
	movq	%rdi, 8(%rsi)
	movq	160(%rax), %rsi
	movq	%rsi, 160(%rdx)
	leaq	128(%rax), %rsi
	movq	$0, 136(%rax)
	movq	%rsi, 144(%rax)
	movq	%rsi, 152(%rax)
	movq	$0, 160(%rax)
.L2069:
	movq	168(%rax), %rsi
	leaq	184(%rdx), %rdi
	movq	%rsi, 168(%rdx)
	movq	192(%rax), %rsi
	testq	%rsi, %rsi
	je	.L2070
	movl	184(%rax), %r9d
	movq	%rsi, 192(%rdx)
	movl	%r9d, 184(%rdx)
	movq	200(%rax), %r9
	movq	%r9, 200(%rdx)
	movq	208(%rax), %r9
	movq	%r9, 208(%rdx)
	movq	%rdi, 8(%rsi)
	movq	216(%rax), %rsi
	movq	%rsi, 216(%rdx)
	leaq	184(%rax), %rsi
	movq	$0, 192(%rax)
	movq	%rsi, 200(%rax)
	movq	%rsi, 208(%rax)
	movq	$0, 216(%rax)
.L2071:
	addq	$224, %rax
	addq	$224, %rdx
	cmpq	%rax, %rbx
	je	.L2090
.L2072:
	movq	(%rax), %rsi
	leaq	16(%rdx), %rdi
	movq	%rsi, (%rdx)
	movq	24(%rax), %rsi
	testq	%rsi, %rsi
	jne	.L2091
	movl	$0, 16(%rdx)
	movq	$0, 24(%rdx)
	movq	%rdi, 32(%rdx)
	movq	%rdi, 40(%rdx)
	movq	$0, 48(%rdx)
	jmp	.L2065
	.p2align 4,,10
	.p2align 3
.L2063:
	movq	%r15, %rdi
	addq	$224, %r15
	call	_ZN2v88internal8compiler5HintsD1Ev
.L2083:
	cmpq	%rbx, %r15
	jne	.L2063
	movq	-56(%rbp), %rax
	movq	%r13, %xmm0
	movq	%r12, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, 24(%r14)
	movups	%xmm0, 8(%r14)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2066:
	.cfi_restore_state
	movl	$0, 72(%rdx)
	movq	$0, 80(%rdx)
	movq	%rdi, 88(%rdx)
	movq	%rdi, 96(%rdx)
	movq	$0, 104(%rdx)
	jmp	.L2067
	.p2align 4,,10
	.p2align 3
.L2070:
	movl	$0, 184(%rdx)
	movq	$0, 192(%rdx)
	movq	%rdi, 200(%rdx)
	movq	%rdi, 208(%rdx)
	movq	$0, 216(%rdx)
	jmp	.L2071
	.p2align 4,,10
	.p2align 3
.L2068:
	movl	$0, 128(%rdx)
	movq	$0, 136(%rdx)
	movq	%rdi, 144(%rdx)
	movq	%rdi, 152(%rdx)
	movq	$0, 160(%rdx)
	jmp	.L2069
	.p2align 4,,10
	.p2align 3
.L2090:
	movabsq	$411757680216731063, %rdx
	movq	%rbx, %rax
	subq	%rcx, %rax
	subq	$224, %rax
	shrq	$5, %rax
	imulq	%rdx, %rax
	movabsq	$576460752303423487, %rdx
	andq	%rdx, %rax
	addq	$1, %rax
	leaq	0(,%rax,8), %rdx
	subq	%rax, %rdx
	movq	%rdx, %rax
	salq	$5, %rax
	addq	%rax, %r12
	jmp	.L2083
	.p2align 4,,10
	.p2align 3
.L2074:
	movl	$224, %esi
	movl	$224, %r12d
	jmp	.L2047
.L2086:
	movq	%r9, -72(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rcx
	movq	-72(%rbp), %r9
	movq	%rax, %r13
	jmp	.L2050
.L2087:
	cmpq	$9586980, %rsi
	movl	$9586980, %eax
	cmovbe	%rsi, %rax
	imulq	$224, %rax, %r12
	movq	%r12, %rsi
	jmp	.L2047
.L2084:
	leaq	.LC48(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE23029:
	.size	_ZNSt6vectorIN2v88internal8compiler5HintsENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_, .-_ZNSt6vectorIN2v88internal8compiler5HintsENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation26ProcessKeyedPropertyAccessERKNS1_5HintsES5_NS0_12FeedbackSlotENS1_10AccessModeEb.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation26ProcessKeyedPropertyAccessERKNS1_5HintsES5_NS0_12FeedbackSlotENS1_10AccessModeEb.constprop.0, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation26ProcessKeyedPropertyAccessERKNS1_5HintsES5_NS0_12FeedbackSlotENS1_10AccessModeEb.constprop.0:
.LFB25737:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$728, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$-1, %ecx
	je	.L2092
	movq	%rdi, %r12
	movq	%rsi, %r14
	movq	%rdx, %r15
	movl	%ecx, %r13d
	movl	%r8d, %ebx
	call	_ZNK2v88internal8compiler34SerializerForBackgroundCompilation15feedback_vectorEv
	testq	%rax, %rax
	je	.L2092
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler34SerializerForBackgroundCompilation15feedback_vectorEv
	leaq	-752(%rbp), %r8
	movl	%r13d, %edx
	movq	%rax, %rsi
	movq	%r8, %rdi
	movq	%r8, -760(%rbp)
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS0_6HandleINS0_14FeedbackVectorEEENS0_12FeedbackSlotE@PLT
	subq	$8, %rsp
	movq	(%r12), %rdi
	movb	$0, -288(%rbp)
	pushq	-272(%rbp)
	movq	-760(%rbp), %r8
	movl	$2, %edx
	movb	$0, -280(%rbp)
	pushq	-280(%rbp)
	movq	%r8, %rsi
	pushq	-288(%rbp)
	call	_ZN2v88internal8compiler12JSHeapBroker32ProcessFeedbackForPropertyAccessERKNS1_14FeedbackSourceENS1_10AccessModeENS_4base8OptionalINS1_7NameRefEEE@PLT
	addq	$32, %rsp
	movq	%rax, %r13
	testb	%bl, %bl
	jne	.L2096
.L2101:
	movq	16(%r12), %rax
	leaq	-720(%rbp), %rdx
	movl	$0, -720(%rbp)
	leaq	-736(%rbp), %rbx
	movq	%rdx, -704(%rbp)
	movq	%rdx, -696(%rbp)
	leaq	-664(%rbp), %rdx
	movq	%rax, -736(%rbp)
	movq	%rax, -680(%rbp)
	movq	%rdx, -648(%rbp)
	movq	%rdx, -640(%rbp)
	leaq	-608(%rbp), %rdx
	movq	%rax, -624(%rbp)
	movq	%rax, -568(%rbp)
	leaq	-552(%rbp), %rax
	movq	$0, -712(%rbp)
	movq	$0, -688(%rbp)
	movl	$0, -664(%rbp)
	movq	$0, -656(%rbp)
	movq	$0, -632(%rbp)
	movl	$0, -608(%rbp)
	movq	$0, -600(%rbp)
	movq	%rdx, -592(%rbp)
	movq	%rdx, -584(%rbp)
	movq	$0, -576(%rbp)
	movl	$0, -552(%rbp)
	movq	$0, -544(%rbp)
	movq	%rax, -536(%rbp)
	movq	%rax, -528(%rbp)
	movq	$0, -520(%rbp)
	movl	0(%r13), %eax
	cmpl	$4, %eax
	je	.L2097
	cmpl	$8, %eax
	je	.L2098
	testl	%eax, %eax
	je	.L2099
	leaq	.LC32(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2097:
	movq	%r13, %rdi
	leaq	-288(%rbp), %r13
	call	_ZNK2v88internal8compiler17ProcessedFeedback15AsElementAccessEv@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	leaq	-512(%rbp), %r15
	movq	%rax, -760(%rbp)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	-760(%rbp), %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation20ProcessElementAccessENS1_5HintsES3_RKNS1_21ElementAccessFeedbackENS1_10AccessModeE
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
.L2099:
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
.L2092:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2112
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2098:
	.cfi_restore_state
	movq	%r13, %rdi
	leaq	-288(%rbp), %r13
	call	_ZNK2v88internal8compiler17ProcessedFeedback13AsNamedAccessEv@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movq	%rbx, %r8
	movl	$2, %ecx
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation18ProcessNamedAccessENS1_5HintsERKNS1_19NamedAccessFeedbackENS1_10AccessModeEPS3_
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	jmp	.L2092
	.p2align 4,,10
	.p2align 3
.L2096:
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation22BailoutOnUninitializedERKNS1_17ProcessedFeedbackE
	testb	%al, %al
	je	.L2101
	jmp	.L2092
.L2112:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25737:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation26ProcessKeyedPropertyAccessERKNS1_5HintsES5_NS0_12FeedbackSlotENS1_10AccessModeEb.constprop.0, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation26ProcessKeyedPropertyAccessERKNS1_5HintsES5_NS0_12FeedbackSlotENS1_10AccessModeEb.constprop.0
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation22VisitStaInArrayLiteralEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation22VisitStaInArrayLiteralEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation22VisitStaInArrayLiteralEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation22VisitStaInArrayLiteralEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB19104:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	xorl	%esi, %esi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	24(%rdi), %r14
	movq	%r13, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment14register_hintsENS0_11interpreter8RegisterE
	movq	24(%r12), %r15
	movq	%r13, %rdi
	movl	$1, %esi
	movq	%rax, %r14
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r15, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment14register_hintsENS0_11interpreter8RegisterE
	movq	%r13, %rdi
	movl	$2, %esi
	movq	%rax, %r15
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %ecx
	popq	%r12
	movl	$1, %r8d
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler34SerializerForBackgroundCompilation26ProcessKeyedPropertyAccessERKNS1_5HintsES5_NS0_12FeedbackSlotENS1_10AccessModeEb.constprop.0
	.cfi_endproc
.LFE19104:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation22VisitStaInArrayLiteralEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation22VisitStaInArrayLiteralEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation29VisitStaDataPropertyInLiteralEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation29VisitStaDataPropertyInLiteralEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation29VisitStaDataPropertyInLiteralEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation29VisitStaDataPropertyInLiteralEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB19105:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	xorl	%esi, %esi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	24(%rdi), %r14
	movq	%r13, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment14register_hintsENS0_11interpreter8RegisterE
	movq	24(%r12), %r15
	movq	%r13, %rdi
	movl	$1, %esi
	movq	%rax, %r14
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r15, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment14register_hintsENS0_11interpreter8RegisterE
	movq	%r13, %rdi
	movl	$3, %esi
	movq	%rax, %r15
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %ecx
	popq	%r12
	xorl	%r8d, %r8d
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler34SerializerForBackgroundCompilation26ProcessKeyedPropertyAccessERKNS1_5HintsES5_NS0_12FeedbackSlotENS1_10AccessModeEb.constprop.0
	.cfi_endproc
.LFE19105:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation29VisitStaDataPropertyInLiteralEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation29VisitStaDataPropertyInLiteralEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZNSt8_Rb_treeIN2v88internal8compiler17FunctionBlueprintES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE16_M_insert_uniqueIRKS3_EESt4pairISt17_Rb_tree_iteratorIS3_EbEOT_,"axG",@progbits,_ZNSt8_Rb_treeIN2v88internal8compiler17FunctionBlueprintES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE16_M_insert_uniqueIRKS3_EESt4pairISt17_Rb_tree_iteratorIS3_EbEOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN2v88internal8compiler17FunctionBlueprintES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE16_M_insert_uniqueIRKS3_EESt4pairISt17_Rb_tree_iteratorIS3_EbEOT_
	.type	_ZNSt8_Rb_treeIN2v88internal8compiler17FunctionBlueprintES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE16_M_insert_uniqueIRKS3_EESt4pairISt17_Rb_tree_iteratorIS3_EbEOT_, @function
_ZNSt8_Rb_treeIN2v88internal8compiler17FunctionBlueprintES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE16_M_insert_uniqueIRKS3_EESt4pairISt17_Rb_tree_iteratorIS3_EbEOT_:
.LFB22820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	16(%rdi), %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	24(%rdi), %r12
	testq	%r12, %r12
	je	.L2131
	movq	8(%rsi), %rsi
	jmp	.L2119
	.p2align 4,,10
	.p2align 3
.L2138:
	movq	16(%r12), %rax
	movl	$1, %edx
	testq	%rax, %rax
	je	.L2120
.L2139:
	movq	%rax, %r12
.L2119:
	movq	40(%r12), %rcx
	cmpq	%rcx, %rsi
	jb	.L2138
	movq	24(%r12), %rax
	xorl	%edx, %edx
	testq	%rax, %rax
	jne	.L2139
.L2120:
	testb	%dl, %dl
	jne	.L2118
	cmpq	%rcx, %rsi
	jbe	.L2124
.L2129:
	movl	$1, %r8d
	cmpq	%r12, %r15
	jne	.L2140
.L2125:
	movq	(%rbx), %rdi
	movq	16(%rdi), %r14
	movq	24(%rdi), %rax
	subq	%r14, %rax
	cmpq	$271, %rax
	jbe	.L2141
	leaq	272(%r14), %rax
	movq	%rax, 16(%rdi)
.L2127:
	movq	0(%r13), %rax
	leaq	16(%r13), %rsi
	leaq	48(%r14), %rdi
	movl	%r8d, -52(%rbp)
	movq	%rax, 32(%r14)
	movq	8(%r13), %rax
	movq	%rax, 40(%r14)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movl	-52(%rbp), %r8d
	movq	%r12, %rdx
	movq	%r15, %rcx
	movq	%r14, %rsi
	movl	%r8d, %edi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 48(%rbx)
	addq	$24, %rsp
	movq	%r14, %rax
	popq	%rbx
	movl	$1, %edx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2131:
	.cfi_restore_state
	movq	%r15, %r12
.L2118:
	cmpq	32(%rbx), %r12
	je	.L2129
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	8(%r13), %rcx
	cmpq	%rcx, 40(%rax)
	jb	.L2129
	movq	%rax, %r12
.L2124:
	addq	$24, %rsp
	movq	%r12, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2140:
	.cfi_restore_state
	xorl	%r8d, %r8d
	movq	40(%r12), %rax
	cmpq	%rax, 8(%r13)
	setb	%r8b
	jmp	.L2125
	.p2align 4,,10
	.p2align 3
.L2141:
	movl	$272, %esi
	movl	%r8d, -52(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-52(%rbp), %r8d
	movq	%rax, %r14
	jmp	.L2127
	.cfi_endproc
.LFE22820:
	.size	_ZNSt8_Rb_treeIN2v88internal8compiler17FunctionBlueprintES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE16_M_insert_uniqueIRKS3_EESt4pairISt17_Rb_tree_iteratorIS3_EbEOT_, .-_ZNSt8_Rb_treeIN2v88internal8compiler17FunctionBlueprintES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE16_M_insert_uniqueIRKS3_EESt4pairISt17_Rb_tree_iteratorIS3_EbEOT_
	.section	.text._ZN2v88internal8compiler5Hints20AddFunctionBlueprintENS1_17FunctionBlueprintE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler5Hints20AddFunctionBlueprintENS1_17FunctionBlueprintE
	.type	_ZN2v88internal8compiler5Hints20AddFunctionBlueprintENS1_17FunctionBlueprintE, @function
_ZN2v88internal8compiler5Hints20AddFunctionBlueprintENS1_17FunctionBlueprintE:
.LFB18794:
	.cfi_startproc
	endbr64
	addq	$168, %rdi
	jmp	_ZNSt8_Rb_treeIN2v88internal8compiler17FunctionBlueprintES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE16_M_insert_uniqueIRKS3_EESt4pairISt17_Rb_tree_iteratorIS3_EbEOT_
	.cfi_endproc
.LFE18794:
	.size	_ZN2v88internal8compiler5Hints20AddFunctionBlueprintENS1_17FunctionBlueprintE, .-_ZN2v88internal8compiler5Hints20AddFunctionBlueprintENS1_17FunctionBlueprintE
	.section	.text._ZN2v88internal8compiler5Hints3AddERKS2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler5Hints3AddERKS2_
	.type	_ZN2v88internal8compiler5Hints3AddERKS2_, @function
_ZN2v88internal8compiler5Hints3AddERKS2_:
.LFB18795:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	56(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	72(%rsi), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-304(%rbp), %rbx
	subq	$536, %rsp
	movq	%rsi, -568(%rbp)
	movq	88(%rsi), %r15
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	cmpq	%r12, %r15
	je	.L2148
	.p2align 4,,10
	.p2align 3
.L2147:
	movq	32(%r15), %rax
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, -304(%rbp)
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_
	movq	%r15, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r15
	cmpq	%r12, %rax
	jne	.L2147
.L2148:
	movq	-568(%rbp), %rax
	leaq	112(%r14), %r13
	leaq	-304(%rbp), %rbx
	movq	144(%rax), %r15
	leaq	128(%rax), %r12
	cmpq	%r15, %r12
	je	.L2146
	.p2align 4,,10
	.p2align 3
.L2151:
	movq	32(%r15), %rax
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, -304(%rbp)
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_3MapEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_
	movq	%r15, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r15
	cmpq	%rax, %r12
	jne	.L2151
.L2146:
	movq	-568(%rbp), %rax
	movq	200(%rax), %r15
	addq	$184, %rax
	movq	%rax, -552(%rbp)
	cmpq	%r15, %rax
	je	.L2150
	leaq	168(%r14), %rax
	leaq	-304(%rbp), %rbx
	movq	%rax, -560(%rbp)
	leaq	-528(%rbp), %r12
	leaq	-288(%rbp), %r13
	.p2align 4,,10
	.p2align 3
.L2154:
	movq	32(%r15), %rax
	leaq	48(%r15), %rsi
	movq	%r12, %rdi
	movq	%rax, -544(%rbp)
	movq	40(%r15), %rax
	movq	%rax, -536(%rbp)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movq	-544(%rbp), %rax
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, -304(%rbp)
	movq	-536(%rbp), %rax
	movq	%rax, -296(%rbp)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movq	-560(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZNSt8_Rb_treeIN2v88internal8compiler17FunctionBlueprintES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE16_M_insert_uniqueIRKS3_EESt4pairISt17_Rb_tree_iteratorIS3_EbEOT_
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	movq	%r15, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r15
	cmpq	%rax, -552(%rbp)
	jne	.L2154
.L2150:
	movq	-568(%rbp), %rax
	leaq	-304(%rbp), %rbx
	movq	32(%rax), %r12
	leaq	16(%rax), %r13
	cmpq	%r12, %r13
	je	.L2143
	.p2align 4,,10
	.p2align 3
.L2155:
	movq	40(%r12), %rax
	movl	32(%r12), %edx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movl	%edx, -304(%rbp)
	movq	%rax, -296(%rbp)
	call	_ZNSt8_Rb_treeIN2v88internal8compiler14VirtualContextES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE16_M_insert_uniqueIRKS3_EESt4pairISt17_Rb_tree_iteratorIS3_EbEOT_
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rax, %r13
	jne	.L2155
.L2143:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2166
	addq	$536, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2166:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18795:
	.size	_ZN2v88internal8compiler5Hints3AddERKS2_, .-_ZN2v88internal8compiler5Hints3AddERKS2_
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation16VisitPushContextEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation16VisitPushContextEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation16VisitPushContextEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation16VisitPushContextEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB18932:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	xorl	%esi, %esi
	subq	$8, %rsp
	movq	24(%rbx), %r13
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movl	%eax, %esi
	movq	%r13, %rdi
	leaq	480(%r13), %r12
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment14register_hintsENS0_11interpreter8RegisterE
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler5Hints5ClearEv
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5Hints3AddERKS2_
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler5Hints5ClearEv
	movq	24(%rbx), %rdx
	movl	252(%rdx), %eax
	addl	248(%rdx), %eax
	movq	936(%rdx), %rcx
	cltq
	js	.L2169
	movq	944(%rdx), %rdx
	movabsq	$7905747460161236407, %rsi
	subq	%rcx, %rdx
	sarq	$5, %rdx
	imulq	%rsi, %rdx
	cmpq	%rax, %rdx
	jbe	.L2171
.L2169:
	leaq	0(,%rax,8), %rsi
	addq	$8, %rsp
	movq	%r12, %rdi
	subq	%rax, %rsi
	popq	%rbx
	popq	%r12
	salq	$5, %rsi
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	addq	%rcx, %rsi
	jmp	_ZN2v88internal8compiler5Hints3AddERKS2_
	.p2align 4,,10
	.p2align 3
.L2171:
	.cfi_restore_state
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18932:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation16VisitPushContextEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation16VisitPushContextEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation15VisitPopContextEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation15VisitPopContextEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation15VisitPopContextEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation15VisitPopContextEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB18933:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	xorl	%esi, %esi
	movq	24(%rbx), %r12
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment14register_hintsENS0_11interpreter8RegisterE
	movq	%rax, %r12
	movq	24(%rbx), %rax
	leaq	480(%rax), %rdi
	call	_ZN2v88internal8compiler5Hints5ClearEv
	movq	24(%rbx), %rdi
	movq	%r12, %rsi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	addq	$480, %rdi
	jmp	_ZN2v88internal8compiler5Hints3AddERKS2_
	.cfi_endproc
.LFE18933:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation15VisitPopContextEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation15VisitPopContextEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation19VisitLdaContextSlotEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation19VisitLdaContextSlotEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation19VisitLdaContextSlotEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation19VisitLdaContextSlotEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB18937:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$240, %rsp
	movq	24(%rdi), %r13
	movq	%r12, %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r13, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment14register_hintsENS0_11interpreter8RegisterE
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r13
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor15GetIndexOperandEi@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	leaq	-272(%rbp), %r12
	movl	%eax, %r14d
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor27GetUnsignedImmediateOperandEi@PLT
	movq	16(%rbx), %rsi
	xorl	%r8d, %r8d
	movq	%r12, %r9
	movl	%eax, %ecx
	leaq	-256(%rbp), %rax
	movl	%r14d, %edx
	movq	%rbx, %rdi
	movq	%rax, -240(%rbp)
	movq	%rax, -232(%rbp)
	leaq	-200(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	%rax, -176(%rbp)
	leaq	-144(%rbp), %rax
	movq	%rsi, -272(%rbp)
	movq	%rsi, -216(%rbp)
	movq	%rsi, -160(%rbp)
	movq	%rax, -128(%rbp)
	movq	%rax, -120(%rbp)
	leaq	-88(%rbp), %rax
	movq	%rsi, -104(%rbp)
	movq	%r13, %rsi
	movq	%rax, -72(%rbp)
	movq	%rax, -64(%rbp)
	movl	$0, -256(%rbp)
	movq	$0, -248(%rbp)
	movq	$0, -224(%rbp)
	movl	$0, -200(%rbp)
	movq	$0, -192(%rbp)
	movq	$0, -168(%rbp)
	movl	$0, -144(%rbp)
	movq	$0, -136(%rbp)
	movq	$0, -112(%rbp)
	movl	$0, -88(%rbp)
	movq	$0, -80(%rbp)
	movq	$0, -56(%rbp)
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation20ProcessContextAccessERKNS1_5HintsEiiNS2_21ContextProcessingModeEPS3_
	movq	24(%rbx), %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment17accumulator_hintsEv
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler5Hints5ClearEv
	movq	24(%rbx), %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment17accumulator_hintsEv
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler5Hints3AddERKS2_
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2177
	addq	$240, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2177:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18937:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation19VisitLdaContextSlotEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation19VisitLdaContextSlotEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation26VisitLdaCurrentContextSlotEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation26VisitLdaCurrentContextSlotEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation26VisitLdaCurrentContextSlotEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation26VisitLdaCurrentContextSlotEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB18938:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	xorl	%esi, %esi
	leaq	-256(%rbp), %r12
	subq	$240, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor15GetIndexOperandEi@PLT
	movq	16(%rbx), %rcx
	xorl	%r8d, %r8d
	movq	%r12, %r9
	movl	%eax, %edx
	movq	24(%rbx), %rax
	movq	%rbx, %rdi
	movl	$0, -240(%rbp)
	movq	%rcx, -256(%rbp)
	leaq	480(%rax), %rsi
	leaq	-240(%rbp), %rax
	movq	%rcx, -200(%rbp)
	movq	%rax, -224(%rbp)
	movq	%rax, -216(%rbp)
	leaq	-184(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	%rax, -160(%rbp)
	leaq	-128(%rbp), %rax
	movq	%rcx, -144(%rbp)
	movq	%rax, -112(%rbp)
	movq	%rax, -104(%rbp)
	leaq	-72(%rbp), %rax
	movq	%rcx, -88(%rbp)
	xorl	%ecx, %ecx
	movq	%rax, -56(%rbp)
	movq	%rax, -48(%rbp)
	movq	$0, -232(%rbp)
	movq	$0, -208(%rbp)
	movl	$0, -184(%rbp)
	movq	$0, -176(%rbp)
	movq	$0, -152(%rbp)
	movl	$0, -128(%rbp)
	movq	$0, -120(%rbp)
	movq	$0, -96(%rbp)
	movl	$0, -72(%rbp)
	movq	$0, -64(%rbp)
	movq	$0, -40(%rbp)
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation20ProcessContextAccessERKNS1_5HintsEiiNS2_21ContextProcessingModeEPS3_
	movq	24(%rbx), %rdx
	movl	252(%rdx), %eax
	addl	248(%rdx), %eax
	movq	936(%rdx), %rcx
	cltq
	js	.L2180
	movq	944(%rdx), %rdx
	movabsq	$7905747460161236407, %rsi
	subq	%rcx, %rdx
	sarq	$5, %rdx
	imulq	%rsi, %rdx
	cmpq	%rax, %rdx
	jbe	.L2183
.L2180:
	leaq	0(,%rax,8), %rdi
	subq	%rax, %rdi
	salq	$5, %rdi
	addq	%rcx, %rdi
	call	_ZN2v88internal8compiler5Hints5ClearEv
	movq	24(%rbx), %rdx
	movl	252(%rdx), %eax
	addl	248(%rdx), %eax
	movq	936(%rdx), %rcx
	cltq
	js	.L2182
	movq	944(%rdx), %rdx
	movabsq	$7905747460161236407, %rsi
	subq	%rcx, %rdx
	sarq	$5, %rdx
	imulq	%rsi, %rdx
	cmpq	%rax, %rdx
	jbe	.L2183
.L2182:
	leaq	0(,%rax,8), %rdi
	movq	%r12, %rsi
	subq	%rax, %rdi
	salq	$5, %rdi
	addq	%rcx, %rdi
	call	_ZN2v88internal8compiler5Hints3AddERKS2_
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2186
	addq	$240, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2183:
	.cfi_restore_state
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2186:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18938:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation26VisitLdaCurrentContextSlotEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation26VisitLdaCurrentContextSlotEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation28VisitLdaImmutableContextSlotEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation28VisitLdaImmutableContextSlotEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation28VisitLdaImmutableContextSlotEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation28VisitLdaImmutableContextSlotEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB18939:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movl	$1, %esi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$248, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor15GetIndexOperandEi@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	movl	%eax, %r13d
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor27GetUnsignedImmediateOperandEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	24(%rbx), %r15
	movl	%eax, %r14d
	leaq	-288(%rbp), %r12
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r15, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment14register_hintsENS0_11interpreter8RegisterE
	movq	16(%rbx), %rdi
	movq	%r12, %r8
	movl	%r14d, %ecx
	movq	%rax, %rsi
	movl	%r13d, %edx
	leaq	-272(%rbp), %rax
	movl	$0, -272(%rbp)
	movq	%rax, -256(%rbp)
	movq	%rax, -248(%rbp)
	leaq	-216(%rbp), %rax
	movq	%rax, -200(%rbp)
	movq	%rax, -192(%rbp)
	leaq	-160(%rbp), %rax
	movq	%rdi, -288(%rbp)
	movq	%rdi, -232(%rbp)
	movq	%rdi, -176(%rbp)
	movq	%rax, -144(%rbp)
	movq	%rax, -136(%rbp)
	leaq	-104(%rbp), %rax
	movq	%rdi, -120(%rbp)
	movq	%rbx, %rdi
	movq	%rax, -88(%rbp)
	movq	%rax, -80(%rbp)
	movq	$0, -264(%rbp)
	movq	$0, -240(%rbp)
	movl	$0, -216(%rbp)
	movq	$0, -208(%rbp)
	movq	$0, -184(%rbp)
	movl	$0, -160(%rbp)
	movq	$0, -152(%rbp)
	movq	$0, -128(%rbp)
	movl	$0, -104(%rbp)
	movq	$0, -96(%rbp)
	movq	$0, -72(%rbp)
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation20ProcessContextAccessERKNS1_5HintsEiiNS2_21ContextProcessingModeEPS3_.constprop.0
	movq	24(%rbx), %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment17accumulator_hintsEv
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler5Hints5ClearEv
	movq	24(%rbx), %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment17accumulator_hintsEv
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler5Hints3AddERKS2_
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2190
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2190:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18939:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation28VisitLdaImmutableContextSlotEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation28VisitLdaImmutableContextSlotEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation35VisitLdaImmutableCurrentContextSlotEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation35VisitLdaImmutableCurrentContextSlotEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation35VisitLdaImmutableCurrentContextSlotEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation35VisitLdaImmutableCurrentContextSlotEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB18940:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	xorl	%esi, %esi
	leaq	-256(%rbp), %r12
	subq	$240, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor15GetIndexOperandEi@PLT
	movq	16(%rbx), %rcx
	movq	%r12, %r8
	movq	%rbx, %rdi
	movl	%eax, %edx
	movq	24(%rbx), %rax
	movl	$0, -240(%rbp)
	movq	%rcx, -256(%rbp)
	leaq	480(%rax), %rsi
	leaq	-240(%rbp), %rax
	movq	%rcx, -200(%rbp)
	movq	%rax, -224(%rbp)
	movq	%rax, -216(%rbp)
	leaq	-184(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	%rax, -160(%rbp)
	leaq	-128(%rbp), %rax
	movq	%rcx, -144(%rbp)
	movq	%rax, -112(%rbp)
	movq	%rax, -104(%rbp)
	leaq	-72(%rbp), %rax
	movq	%rcx, -88(%rbp)
	xorl	%ecx, %ecx
	movq	%rax, -56(%rbp)
	movq	%rax, -48(%rbp)
	movq	$0, -232(%rbp)
	movq	$0, -208(%rbp)
	movl	$0, -184(%rbp)
	movq	$0, -176(%rbp)
	movq	$0, -152(%rbp)
	movl	$0, -128(%rbp)
	movq	$0, -120(%rbp)
	movq	$0, -96(%rbp)
	movl	$0, -72(%rbp)
	movq	$0, -64(%rbp)
	movq	$0, -40(%rbp)
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation20ProcessContextAccessERKNS1_5HintsEiiNS2_21ContextProcessingModeEPS3_.constprop.0
	movq	24(%rbx), %rdx
	movl	252(%rdx), %eax
	addl	248(%rdx), %eax
	movq	936(%rdx), %rcx
	cltq
	js	.L2193
	movq	944(%rdx), %rdx
	movabsq	$7905747460161236407, %rsi
	subq	%rcx, %rdx
	sarq	$5, %rdx
	imulq	%rsi, %rdx
	cmpq	%rax, %rdx
	jbe	.L2196
.L2193:
	leaq	0(,%rax,8), %rdi
	subq	%rax, %rdi
	salq	$5, %rdi
	addq	%rcx, %rdi
	call	_ZN2v88internal8compiler5Hints5ClearEv
	movq	24(%rbx), %rdx
	movl	252(%rdx), %eax
	addl	248(%rdx), %eax
	movq	936(%rdx), %rcx
	cltq
	js	.L2195
	movq	944(%rdx), %rdx
	movabsq	$7905747460161236407, %rsi
	subq	%rcx, %rdx
	sarq	$5, %rdx
	imulq	%rsi, %rdx
	cmpq	%rax, %rdx
	jbe	.L2196
.L2195:
	leaq	0(,%rax,8), %rdi
	movq	%r12, %rsi
	subq	%rax, %rdi
	salq	$5, %rdi
	addq	%rcx, %rdi
	call	_ZN2v88internal8compiler5Hints3AddERKS2_
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2199
	addq	$240, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2196:
	.cfi_restore_state
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2199:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18940:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation35VisitLdaImmutableCurrentContextSlotEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation35VisitLdaImmutableCurrentContextSlotEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation9VisitLdarEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation9VisitLdarEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation9VisitLdarEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation9VisitLdarEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB18947:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	24(%rdi), %rdx
	movl	252(%rdx), %eax
	addl	248(%rdx), %eax
	movq	936(%rdx), %rcx
	movslq	%eax, %rsi
	js	.L2201
	movq	944(%rdx), %rax
	movabsq	$7905747460161236407, %rdx
	subq	%rcx, %rax
	sarq	$5, %rax
	imulq	%rdx, %rax
	cmpq	%rsi, %rax
	jbe	.L2204
.L2201:
	leaq	0(,%rsi,8), %rdi
	subq	%rsi, %rdi
	salq	$5, %rdi
	addq	%rcx, %rdi
	call	_ZN2v88internal8compiler5Hints5ClearEv
	movq	24(%r12), %r12
	movl	252(%r12), %eax
	addl	248(%r12), %eax
	movq	936(%r12), %rcx
	cltq
	js	.L2203
	movq	944(%r12), %rdx
	movabsq	$7905747460161236407, %rsi
	subq	%rcx, %rdx
	sarq	$5, %rdx
	imulq	%rsi, %rdx
	cmpq	%rax, %rdx
	jbe	.L2204
.L2203:
	leaq	0(,%rax,8), %rdx
	movq	%r13, %rdi
	xorl	%esi, %esi
	subq	%rax, %rdx
	movq	%rdx, %rax
	salq	$5, %rax
	leaq	(%rcx,%rax), %r14
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment14register_hintsENS0_11interpreter8RegisterE
	addq	$8, %rsp
	movq	%r14, %rdi
	popq	%r12
	movq	%rax, %rsi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler5Hints3AddERKS2_
	.p2align 4,,10
	.p2align 3
.L2204:
	.cfi_restore_state
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18947:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation9VisitLdarEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation9VisitLdarEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation9VisitStarEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation9VisitStarEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation9VisitStarEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation9VisitStarEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB18948:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	xorl	%esi, %esi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	24(%rbx), %rdi
	movl	%eax, %esi
	movl	%eax, %r12d
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment14register_hintsENS0_11interpreter8RegisterE
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler5Hints5ClearEv
	movq	24(%rbx), %rdi
	movl	%r12d, %esi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment14register_hintsENS0_11interpreter8RegisterE
	movq	24(%rbx), %rcx
	movq	%rax, %rdi
	movl	252(%rcx), %edx
	addl	248(%rcx), %edx
	movq	936(%rcx), %rax
	movslq	%edx, %rdx
	js	.L2208
	movq	944(%rcx), %rcx
	movabsq	$7905747460161236407, %rsi
	subq	%rax, %rcx
	sarq	$5, %rcx
	imulq	%rsi, %rcx
	cmpq	%rdx, %rcx
	jbe	.L2210
.L2208:
	leaq	0(,%rdx,8), %rsi
	popq	%rbx
	popq	%r12
	subq	%rdx, %rsi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	salq	$5, %rsi
	addq	%rax, %rsi
	jmp	_ZN2v88internal8compiler5Hints3AddERKS2_
	.p2align 4,,10
	.p2align 3
.L2210:
	.cfi_restore_state
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18948:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation9VisitStarEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation9VisitStarEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation8VisitMovEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation8VisitMovEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation8VisitMovEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation8VisitMovEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB18949:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$8, %rsp
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movl	%eax, %r13d
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	24(%rbx), %rdi
	movl	%eax, %esi
	movl	%eax, %r12d
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment14register_hintsENS0_11interpreter8RegisterE
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler5Hints5ClearEv
	movq	24(%rbx), %rdi
	movl	%r12d, %esi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment14register_hintsENS0_11interpreter8RegisterE
	movq	24(%rbx), %rdi
	movl	%r13d, %esi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment14register_hintsENS0_11interpreter8RegisterE
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	movq	%rax, %rsi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler5Hints3AddERKS2_
	.cfi_endproc
.LFE18949:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation8VisitMovEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation8VisitMovEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitReturnEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitReturnEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitReturnEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitReturnEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB19048:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	24(%rdi), %rax
	movq	%rdi, %rbx
	movl	252(%rax), %edx
	addl	248(%rax), %edx
	leaq	704(%rax), %rdi
	movq	936(%rax), %rcx
	movslq	%edx, %r8
	js	.L2214
	movq	944(%rax), %rax
	movabsq	$7905747460161236407, %rdx
	subq	%rcx, %rax
	sarq	$5, %rax
	imulq	%rdx, %rax
	cmpq	%rax, %r8
	jnb	.L2221
.L2214:
	leaq	0(,%r8,8), %rsi
	subq	%r8, %rsi
	salq	$5, %rsi
	addq	%rcx, %rsi
	call	_ZN2v88internal8compiler5Hints3AddERKS2_
	movq	24(%rbx), %rax
	movq	944(%rax), %r12
	movq	936(%rax), %rbx
	cmpq	%rbx, %r12
	je	.L2213
	.p2align 4,,10
	.p2align 3
.L2216:
	movq	%rbx, %rdi
	addq	$224, %rbx
	call	_ZN2v88internal8compiler5Hints5ClearEv
	cmpq	%rbx, %r12
	jne	.L2216
.L2213:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2221:
	.cfi_restore_state
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19048:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitReturnEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitReturnEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation19VisitTestInstanceOfEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation19VisitTestInstanceOfEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation19VisitTestInstanceOfEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation19VisitTestInstanceOfEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB19099:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-512(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$552, %rsp
	movq	24(%rdi), %r13
	movq	%r12, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r13, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment14register_hintsENS0_11interpreter8RegisterE
	movq	24(%rbx), %rdi
	movq	%rax, -592(%rbp)
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment17accumulator_hintsEv
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movq	%r12, %rdi
	movl	$1, %esi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	leaq	-272(%rbp), %rdx
	movl	$0, -272(%rbp)
	movl	%eax, %r12d
	movq	16(%rbx), %rax
	movq	%rdx, -256(%rbp)
	movq	%rdx, -248(%rbp)
	leaq	-216(%rbp), %rdx
	movq	%rax, -288(%rbp)
	movq	%rax, -232(%rbp)
	movq	%rdx, -200(%rbp)
	movq	%rdx, -192(%rbp)
	leaq	-160(%rbp), %rdx
	movq	%rax, -176(%rbp)
	movq	%rax, -120(%rbp)
	leaq	-104(%rbp), %rax
	movq	$0, -264(%rbp)
	movq	$0, -240(%rbp)
	movl	$0, -216(%rbp)
	movq	$0, -208(%rbp)
	movq	$0, -184(%rbp)
	movl	$0, -160(%rbp)
	movq	$0, -152(%rbp)
	movq	%rdx, -144(%rbp)
	movq	%rdx, -136(%rbp)
	movq	$0, -128(%rbp)
	movl	$0, -104(%rbp)
	movq	$0, -96(%rbp)
	movq	%rax, -88(%rbp)
	movq	%rax, -80(%rbp)
	movq	$0, -72(%rbp)
	cmpl	$-1, %r12d
	je	.L2225
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler34SerializerForBackgroundCompilation15feedback_vectorEv
	testq	%rax, %rax
	je	.L2225
	movq	%rbx, %rdi
	leaq	-560(%rbp), %r13
	call	_ZNK2v88internal8compiler34SerializerForBackgroundCompilation15feedback_vectorEv
	movl	%r12d, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS0_6HandleINS0_14FeedbackVectorEEENS0_12FeedbackSlotE@PLT
	movq	(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler12JSHeapBroker28ProcessFeedbackForInstanceOfERKNS1_14FeedbackSourceE@PLT
	movl	(%rax), %edx
	testl	%edx, %edx
	jne	.L2244
.L2227:
	movq	-424(%rbp), %r15
	leaq	-561(%rbp), %rax
	leaq	-440(%rbp), %r13
	movb	$0, -561(%rbp)
	movq	%rax, -584(%rbp)
	leaq	-544(%rbp), %r12
	cmpq	%r13, %r15
	je	.L2230
	.p2align 4,,10
	.p2align 3
.L2229:
	movq	32(%r15), %rdx
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	movl	$1, %ecx
	call	_ZN2v88internal8compiler9ObjectRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L2231
	movq	-584(%rbp), %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation28ProcessConstantForInstanceOfERKNS1_9ObjectRefEPb.part.0
.L2231:
	movq	%r15, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r15
	cmpq	%r13, %rax
	jne	.L2229
	cmpb	$0, -561(%rbp)
	jne	.L2245
.L2230:
	movq	24(%rbx), %rdi
	leaq	-288(%rbp), %r12
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment17accumulator_hintsEv
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler5Hints5ClearEv
	movq	24(%rbx), %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment17accumulator_hintsEv
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler5Hints3AddERKS2_
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
.L2222:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2246
	addq	$552, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2225:
	.cfi_restore_state
	leaq	-288(%rbp), %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	jmp	.L2222
	.p2align 4,,10
	.p2align 3
.L2245:
	movq	-592(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation34ProcessHintsForHasInPrototypeChainERKNS1_5HintsE
	jmp	.L2230
	.p2align 4,,10
	.p2align 3
.L2244:
	movq	%rax, %rdi
	call	_ZNK2v88internal8compiler17ProcessedFeedback12AsInstanceOfEv@PLT
	cmpb	$0, 8(%rax)
	je	.L2227
	movdqu	8(%rax), %xmm0
	leaq	-536(%rbp), %rdi
	leaq	-544(%rbp), %r12
	movaps	%xmm0, -544(%rbp)
	movq	24(%rax), %rax
	movq	%rax, -528(%rbp)
	call	_ZNK2v88internal8compiler11JSObjectRef6objectEv@PLT
	leaq	-456(%rbp), %rdi
	movq	%r12, %rsi
	movq	%rax, -544(%rbp)
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_
	jmp	.L2227
.L2246:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19099:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation19VisitTestInstanceOfEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation19VisitTestInstanceOfEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation26ProcessKeyedPropertyAccessERKNS1_5HintsES5_NS0_12FeedbackSlotENS1_10AccessModeEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation26ProcessKeyedPropertyAccessERKNS1_5HintsES5_NS0_12FeedbackSlotENS1_10AccessModeEb
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation26ProcessKeyedPropertyAccessERKNS1_5HintsES5_NS0_12FeedbackSlotENS1_10AccessModeEb, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation26ProcessKeyedPropertyAccessERKNS1_5HintsES5_NS0_12FeedbackSlotENS1_10AccessModeEb:
.LFB19087:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$728, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -760(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$-1, %ecx
	je	.L2247
	movq	%rdi, %rbx
	movq	%rsi, %r14
	movl	%ecx, %r15d
	movl	%r8d, %r12d
	movl	%r9d, %r13d
	call	_ZNK2v88internal8compiler34SerializerForBackgroundCompilation15feedback_vectorEv
	testq	%rax, %rax
	je	.L2247
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler34SerializerForBackgroundCompilation15feedback_vectorEv
	leaq	-752(%rbp), %r8
	movl	%r15d, %edx
	movq	%rax, %rsi
	movq	%r8, %rdi
	movq	%r8, -768(%rbp)
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS0_6HandleINS0_14FeedbackVectorEEENS0_12FeedbackSlotE@PLT
	subq	$8, %rsp
	movq	(%rbx), %rdi
	movl	%r12d, %edx
	pushq	-272(%rbp)
	movq	-768(%rbp), %r8
	movb	$0, -288(%rbp)
	movb	$0, -280(%rbp)
	movq	%r8, %rsi
	pushq	-280(%rbp)
	pushq	-288(%rbp)
	call	_ZN2v88internal8compiler12JSHeapBroker32ProcessFeedbackForPropertyAccessERKNS1_14FeedbackSourceENS1_10AccessModeENS_4base8OptionalINS1_7NameRefEEE@PLT
	addq	$32, %rsp
	movq	%rax, %r8
	testb	%r13b, %r13b
	jne	.L2251
.L2256:
	movq	16(%rbx), %rax
	leaq	-720(%rbp), %rdx
	movl	$0, -720(%rbp)
	leaq	-736(%rbp), %r13
	movq	%rdx, -704(%rbp)
	movq	%rdx, -696(%rbp)
	leaq	-664(%rbp), %rdx
	movq	%rax, -736(%rbp)
	movq	%rax, -680(%rbp)
	movq	%rdx, -648(%rbp)
	movq	%rdx, -640(%rbp)
	leaq	-608(%rbp), %rdx
	movq	%rax, -624(%rbp)
	movq	%rax, -568(%rbp)
	leaq	-552(%rbp), %rax
	movq	$0, -712(%rbp)
	movq	$0, -688(%rbp)
	movl	$0, -664(%rbp)
	movq	$0, -656(%rbp)
	movq	$0, -632(%rbp)
	movl	$0, -608(%rbp)
	movq	$0, -600(%rbp)
	movq	%rdx, -592(%rbp)
	movq	%rdx, -584(%rbp)
	movq	$0, -576(%rbp)
	movl	$0, -552(%rbp)
	movq	$0, -544(%rbp)
	movq	%rax, -536(%rbp)
	movq	%rax, -528(%rbp)
	movq	$0, -520(%rbp)
	movl	(%r8), %eax
	cmpl	$4, %eax
	je	.L2252
	cmpl	$8, %eax
	je	.L2253
	testl	%eax, %eax
	je	.L2254
	leaq	.LC32(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2252:
	movq	%r8, %rdi
	leaq	-512(%rbp), %r15
	call	_ZNK2v88internal8compiler17ProcessedFeedback15AsElementAccessEv@PLT
	leaq	-288(%rbp), %r9
	movq	-760(%rbp), %rsi
	movq	%r9, %rdi
	movq	%rax, -768(%rbp)
	movq	%r9, -760(%rbp)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movl	%r12d, %r8d
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	-760(%rbp), %r9
	movq	-768(%rbp), %rcx
	movq	%r9, %rdx
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation20ProcessElementAccessENS1_5HintsES3_RKNS1_21ElementAccessFeedbackENS1_10AccessModeE
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	movq	-760(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
.L2254:
	testl	%r12d, %r12d
	je	.L2268
.L2257:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
.L2247:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2269
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2253:
	.cfi_restore_state
	movq	%r8, %rdi
	leaq	-288(%rbp), %r15
	call	_ZNK2v88internal8compiler17ProcessedFeedback13AsNamedAccessEv@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, -760(%rbp)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movq	%rbx, %rdi
	movq	%r13, %r8
	movl	%r12d, %ecx
	movq	-760(%rbp), %rdx
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation18ProcessNamedAccessENS1_5HintsERKNS1_19NamedAccessFeedbackENS1_10AccessModeEPS3_
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	testl	%r12d, %r12d
	jne	.L2257
.L2268:
	movq	24(%rbx), %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment17accumulator_hintsEv
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler5Hints5ClearEv
	movq	24(%rbx), %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment17accumulator_hintsEv
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler5Hints3AddERKS2_
	jmp	.L2257
	.p2align 4,,10
	.p2align 3
.L2251:
	movq	%rax, %rsi
	movq	%rbx, %rdi
	movq	%rax, -768(%rbp)
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation22BailoutOnUninitializedERKNS1_17ProcessedFeedbackE
	movq	-768(%rbp), %r8
	testb	%al, %al
	je	.L2256
	jmp	.L2247
.L2269:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19087:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation26ProcessKeyedPropertyAccessERKNS1_5HintsES5_NS0_12FeedbackSlotENS1_10AccessModeEb, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation26ProcessKeyedPropertyAccessERKNS1_5HintsES5_NS0_12FeedbackSlotENS1_10AccessModeEb
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation21VisitLdaKeyedPropertyEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation21VisitLdaKeyedPropertyEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation21VisitLdaKeyedPropertyEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation21VisitLdaKeyedPropertyEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB19086:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	24(%rdi), %r12
	movl	252(%r12), %eax
	addl	248(%r12), %eax
	movq	936(%r12), %r15
	movslq	%eax, %rcx
	js	.L2271
	movq	944(%r12), %rax
	movabsq	$7905747460161236407, %rdx
	subq	%r15, %rax
	sarq	$5, %rax
	imulq	%rdx, %rax
	cmpq	%rcx, %rax
	jbe	.L2273
.L2271:
	leaq	0(,%rcx,8), %rdx
	movq	%r14, %rdi
	xorl	%esi, %esi
	subq	%rcx, %rdx
	salq	$5, %rdx
	addq	%rdx, %r15
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment14register_hintsENS0_11interpreter8RegisterE
	movq	%r14, %rdi
	movl	$1, %esi
	movq	%rax, %r12
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	movl	%eax, %ecx
	popq	%r13
	movl	$1, %r9d
	popq	%r14
	xorl	%r8d, %r8d
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler34SerializerForBackgroundCompilation26ProcessKeyedPropertyAccessERKNS1_5HintsES5_NS0_12FeedbackSlotENS1_10AccessModeEb
	.p2align 4,,10
	.p2align 3
.L2273:
	.cfi_restore_state
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19086:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation21VisitLdaKeyedPropertyEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation21VisitLdaKeyedPropertyEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitTestInEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitTestInEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitTestInEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitTestInEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB19096:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	24(%rdi), %r12
	movl	252(%r12), %edx
	addl	248(%r12), %edx
	movq	936(%r12), %r15
	movslq	%edx, %rcx
	js	.L2275
	movq	944(%r12), %rdx
	movabsq	$7905747460161236407, %rax
	subq	%r15, %rdx
	sarq	$5, %rdx
	imulq	%rax, %rdx
	cmpq	%rcx, %rdx
	jbe	.L2277
.L2275:
	leaq	0(,%rcx,8), %rax
	movq	%r14, %rdi
	xorl	%esi, %esi
	subq	%rcx, %rax
	salq	$5, %rax
	addq	%rax, %r15
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment14register_hintsENS0_11interpreter8RegisterE
	movq	%r14, %rdi
	movl	$1, %esi
	movq	%rax, %r12
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	movq	%r12, %rdx
	movq	%r15, %rsi
	popq	%r12
	movq	%r13, %rdi
	movl	%eax, %ecx
	popq	%r13
	xorl	%r9d, %r9d
	popq	%r14
	movl	$3, %r8d
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler34SerializerForBackgroundCompilation26ProcessKeyedPropertyAccessERKNS1_5HintsES5_NS0_12FeedbackSlotENS1_10AccessModeEb
	.p2align 4,,10
	.p2align 3
.L2277:
	.cfi_restore_state
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19096:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitTestInEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitTestInEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation21VisitStaKeyedPropertyEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation21VisitStaKeyedPropertyEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation21VisitStaKeyedPropertyEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation21VisitStaKeyedPropertyEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB19103:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	xorl	%esi, %esi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	24(%rdi), %r14
	movq	%r13, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment14register_hintsENS0_11interpreter8RegisterE
	movq	24(%r12), %r15
	movq	%r13, %rdi
	movl	$1, %esi
	movq	%rax, %r14
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r15, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment14register_hintsENS0_11interpreter8RegisterE
	movq	%r13, %rdi
	movl	$2, %esi
	movq	%rax, %r15
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %ecx
	popq	%r12
	movl	$1, %r9d
	popq	%r13
	movl	$1, %r8d
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler34SerializerForBackgroundCompilation26ProcessKeyedPropertyAccessERKNS1_5HintsES5_NS0_12FeedbackSlotENS1_10AccessModeEb
	.cfi_endproc
.LFE19103:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation21VisitStaKeyedPropertyEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation21VisitStaKeyedPropertyEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation26ProcessNamedPropertyAccessENS1_5HintsERKNS1_7NameRefENS0_12FeedbackSlotENS1_10AccessModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation26ProcessNamedPropertyAccessENS1_5HintsERKNS1_7NameRefENS0_12FeedbackSlotENS1_10AccessModeE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation26ProcessNamedPropertyAccessENS1_5HintsERKNS1_7NameRefENS0_12FeedbackSlotENS1_10AccessModeE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation26ProcessNamedPropertyAccessENS1_5HintsERKNS1_7NameRefENS0_12FeedbackSlotENS1_10AccessModeE:
.LFB19088:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$504, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$-1, %ecx
	je	.L2280
	movq	%rdi, %rbx
	movq	%rsi, %r14
	movq	%rdx, %r13
	movl	%ecx, %r12d
	movl	%r8d, %r15d
	call	_ZNK2v88internal8compiler34SerializerForBackgroundCompilation15feedback_vectorEv
	testq	%rax, %rax
	je	.L2280
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler34SerializerForBackgroundCompilation15feedback_vectorEv
	leaq	-528(%rbp), %r8
	movl	%r12d, %edx
	movq	%rax, %rsi
	movq	%r8, %rdi
	movq	%r8, -536(%rbp)
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS0_6HandleINS0_14FeedbackVectorEEENS0_12FeedbackSlotE@PLT
	movq	8(%r13), %rdx
	subq	$8, %rsp
	movq	(%rbx), %rdi
	movq	0(%r13), %rax
	movb	$1, -288(%rbp)
	pushq	%rdx
	movq	-536(%rbp), %r8
	pushq	%rax
	pushq	-288(%rbp)
	movq	%r8, %rsi
	movq	%rdx, -272(%rbp)
	movl	%r15d, %edx
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal8compiler12JSHeapBroker32ProcessFeedbackForPropertyAccessERKNS1_14FeedbackSourceENS1_10AccessModeENS_4base8OptionalINS1_7NameRefEEE@PLT
	addq	$32, %rsp
	movq	%rbx, %rdi
	movq	%rax, %rsi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation22BailoutOnUninitializedERKNS1_17ProcessedFeedbackE
	testb	%al, %al
	jne	.L2280
	movq	16(%rbx), %rax
	leaq	-496(%rbp), %rdx
	movl	$0, -496(%rbp)
	leaq	-512(%rbp), %r13
	movq	%rdx, -480(%rbp)
	movq	%rdx, -472(%rbp)
	leaq	-440(%rbp), %rdx
	movq	%rax, -512(%rbp)
	movq	%rax, -456(%rbp)
	movq	%rdx, -424(%rbp)
	movq	%rdx, -416(%rbp)
	leaq	-384(%rbp), %rdx
	movq	%rax, -400(%rbp)
	movq	%rax, -344(%rbp)
	leaq	-328(%rbp), %rax
	movq	$0, -488(%rbp)
	movq	$0, -464(%rbp)
	movl	$0, -440(%rbp)
	movq	$0, -432(%rbp)
	movq	$0, -408(%rbp)
	movl	$0, -384(%rbp)
	movq	$0, -376(%rbp)
	movq	%rdx, -368(%rbp)
	movq	%rdx, -360(%rbp)
	movq	$0, -352(%rbp)
	movl	$0, -328(%rbp)
	movq	$0, -320(%rbp)
	movq	%rax, -312(%rbp)
	movq	%rax, -304(%rbp)
	movq	$0, -296(%rbp)
	movl	(%r12), %eax
	testl	%eax, %eax
	je	.L2284
	cmpl	$8, %eax
	jne	.L2285
	movq	%r12, %rdi
	leaq	-288(%rbp), %r12
	call	_ZNK2v88internal8compiler17ProcessedFeedback13AsNamedAccessEv@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, -536(%rbp)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movq	%rbx, %rdi
	movq	%r13, %r8
	movl	%r15d, %ecx
	movq	-536(%rbp), %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation18ProcessNamedAccessENS1_5HintsERKNS1_19NamedAccessFeedbackENS1_10AccessModeEPS3_
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
.L2284:
	testl	%r15d, %r15d
	je	.L2296
.L2286:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
.L2280:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2297
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2296:
	.cfi_restore_state
	movq	24(%rbx), %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment17accumulator_hintsEv
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler5Hints5ClearEv
	movq	24(%rbx), %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment17accumulator_hintsEv
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler5Hints3AddERKS2_
	jmp	.L2286
.L2297:
	call	__stack_chk_fail@PLT
.L2285:
	leaq	.LC32(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19088:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation26ProcessNamedPropertyAccessENS1_5HintsERKNS1_7NameRefENS0_12FeedbackSlotENS1_10AccessModeE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation26ProcessNamedPropertyAccessENS1_5HintsERKNS1_7NameRefENS0_12FeedbackSlotENS1_10AccessModeE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation16VisitGetIteratorEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation16VisitGetIteratorEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation16VisitGetIteratorEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation16VisitGetIteratorEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB18920:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	xorl	%esi, %esi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$280, %rsp
	.cfi_offset 3, -56
	movq	24(%rdi), %r14
	movq	%r13, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment14register_hintsENS0_11interpreter8RegisterE
	movq	%r13, %rdi
	movl	$1, %esi
	leaq	-288(%rbp), %r13
	movq	%rax, %r15
	movq	(%r12), %rax
	movq	(%rax), %rbx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	leaq	-304(%rbp), %r9
	movq	(%r12), %rsi
	movl	$1, %ecx
	addq	$3856, %rbx
	movq	%r9, %rdi
	movl	%eax, %r14d
	movq	%r9, -312(%rbp)
	movq	%rbx, %rdx
	call	_ZN2v88internal8compiler7NameRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	xorl	%r8d, %r8d
	movl	%r14d, %ecx
	movq	%r13, %rsi
	movq	-312(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rdx
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation26ProcessNamedPropertyAccessENS1_5HintsERKNS1_7NameRefENS0_12FeedbackSlotENS1_10AccessModeE
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2301
	addq	$280, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2301:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18920:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation16VisitGetIteratorEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation16VisitGetIteratorEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation21VisitLdaNamedPropertyEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation21VisitLdaNamedPropertyEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation21VisitLdaNamedPropertyEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation21VisitLdaNamedPropertyEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB19091:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	xorl	%esi, %esi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-304(%rbp), %rbx
	subq	$264, %rsp
	movq	24(%rdi), %r14
	movq	%r13, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment14register_hintsENS0_11interpreter8RegisterE
	movq	%r13, %rdi
	movl	$1, %esi
	movq	%rax, %r15
	movq	(%r12), %rax
	movq	(%rax), %rdx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor26GetConstantForIndexOperandEiPNS0_7IsolateE@PLT
	movq	(%r12), %rsi
	movl	$1, %ecx
	movq	%rbx, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler7NameRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r13, %rdi
	movl	$2, %esi
	leaq	-288(%rbp), %r13
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	xorl	%r8d, %r8d
	movl	%r14d, %ecx
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation26ProcessNamedPropertyAccessENS1_5HintsERKNS1_7NameRefENS0_12FeedbackSlotENS1_10AccessModeE
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2305
	addq	$264, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2305:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19091:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation21VisitLdaNamedPropertyEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation21VisitLdaNamedPropertyEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation21VisitStaNamedPropertyEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation21VisitStaNamedPropertyEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation21VisitStaNamedPropertyEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation21VisitStaNamedPropertyEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB19093:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	xorl	%esi, %esi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-304(%rbp), %rbx
	subq	$264, %rsp
	movq	24(%rdi), %r14
	movq	%r13, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment14register_hintsENS0_11interpreter8RegisterE
	movq	%r13, %rdi
	movl	$1, %esi
	movq	%rax, %r15
	movq	(%r12), %rax
	movq	(%rax), %rdx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor26GetConstantForIndexOperandEiPNS0_7IsolateE@PLT
	movq	(%r12), %rsi
	movl	$1, %ecx
	movq	%rbx, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler7NameRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r13, %rdi
	movl	$2, %esi
	leaq	-288(%rbp), %r13
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movl	%r14d, %ecx
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movl	$1, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation26ProcessNamedPropertyAccessENS1_5HintsERKNS1_7NameRefENS0_12FeedbackSlotENS1_10AccessModeE
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2309
	addq	$264, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2309:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19093:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation21VisitStaNamedPropertyEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation21VisitStaNamedPropertyEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation24VisitStaNamedOwnPropertyEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation24VisitStaNamedOwnPropertyEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation24VisitStaNamedOwnPropertyEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation24VisitStaNamedOwnPropertyEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB19095:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	xorl	%esi, %esi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-304(%rbp), %rbx
	subq	$264, %rsp
	movq	24(%rdi), %r14
	movq	%r13, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment14register_hintsENS0_11interpreter8RegisterE
	movq	%r13, %rdi
	movl	$1, %esi
	movq	%rax, %r15
	movq	(%r12), %rax
	movq	(%rax), %rdx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor26GetConstantForIndexOperandEiPNS0_7IsolateE@PLT
	movq	(%r12), %rsi
	movl	$1, %ecx
	movq	%rbx, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler7NameRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r13, %rdi
	movl	$2, %esi
	leaq	-288(%rbp), %r13
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movl	%r14d, %ecx
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movl	$2, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation26ProcessNamedPropertyAccessENS1_5HintsERKNS1_7NameRefENS0_12FeedbackSlotENS1_10AccessModeE
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2313
	addq	$264, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2313:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19095:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation24VisitStaNamedOwnPropertyEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation24VisitStaNamedOwnPropertyEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.rodata._ZN2v88internal8compiler34SerializerForBackgroundCompilation11EnvironmentC2EPNS0_4ZoneENS1_18CompilationSubjectE.str1.8,"aMS",@progbits,1
	.align 8
.LC49:
	.string	"cannot create std::vector larger than max_size()"
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation11EnvironmentC2EPNS0_4ZoneENS1_18CompilationSubjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11EnvironmentC2EPNS0_4ZoneENS1_18CompilationSubjectE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11EnvironmentC2EPNS0_4ZoneENS1_18CompilationSubjectE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation11EnvironmentC2EPNS0_4ZoneENS1_18CompilationSubjectE:
.LFB18822:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-304(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	16(%rdx), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$24, %rdi
	subq	$296, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	movq	%rsi, -24(%rdi)
	movq	%r13, %rsi
	movq	%rax, -16(%rdi)
	movq	8(%rdx), %rax
	movq	%rax, -8(%rdi)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movq	8(%rbx), %rax
	movq	%r14, %rdi
	movq	(%rax), %rax
	movq	%rax, -304(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo16GetBytecodeArrayEv
	movq	%r14, %rdi
	movl	43(%rax), %eax
	sarl	$3, %eax
	movl	%eax, 248(%rbx)
	movq	8(%rbx), %rax
	movq	(%rax), %rax
	movq	%rax, -304(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo16GetBytecodeArrayEv
	movl	39(%rax), %eax
	movq	%r12, 256(%rbx)
	movl	$0, 272(%rbx)
	testl	%eax, %eax
	leal	7(%rax), %ecx
	movq	$0, 280(%rbx)
	cmovns	%eax, %ecx
	leaq	272(%rbx), %rax
	movq	$0, 304(%rbx)
	movq	%rax, 288(%rbx)
	movq	%rax, 296(%rbx)
	sarl	$3, %ecx
	leaq	328(%rbx), %rax
	movq	%rax, 344(%rbx)
	movq	%rax, 352(%rbx)
	leaq	384(%rbx), %rax
	movq	%rax, 400(%rbx)
	movq	%rax, 408(%rbx)
	leaq	440(%rbx), %rax
	movq	%rax, 456(%rbx)
	movq	%rax, 464(%rbx)
	leaq	480(%rbx), %rax
	movq	%rax, -328(%rbp)
	leaq	496(%rbx), %rax
	movq	%rax, 512(%rbx)
	movq	%rax, 520(%rbx)
	leaq	552(%rbx), %rax
	movl	%ecx, 252(%rbx)
	movq	%r12, 312(%rbx)
	movl	$0, 328(%rbx)
	movq	$0, 336(%rbx)
	movq	$0, 360(%rbx)
	movq	%r12, 368(%rbx)
	movl	$0, 384(%rbx)
	movq	$0, 392(%rbx)
	movq	$0, 416(%rbx)
	movq	%r12, 424(%rbx)
	movl	$0, 440(%rbx)
	movq	$0, 448(%rbx)
	movq	$0, 472(%rbx)
	movq	%r12, 480(%rbx)
	movl	$0, 496(%rbx)
	movq	$0, 504(%rbx)
	movq	$0, 528(%rbx)
	movq	%rax, 568(%rbx)
	movq	%rax, 576(%rbx)
	leaq	608(%rbx), %rax
	movq	%rax, 624(%rbx)
	movq	%rax, 632(%rbx)
	leaq	664(%rbx), %rax
	movq	%rax, 680(%rbx)
	movq	%rax, 688(%rbx)
	leaq	720(%rbx), %rax
	movq	%rax, 736(%rbx)
	movq	%rax, 744(%rbx)
	leaq	776(%rbx), %rax
	movq	%rax, 792(%rbx)
	movq	%rax, 800(%rbx)
	leaq	832(%rbx), %rax
	movq	%r12, 536(%rbx)
	movl	$0, 552(%rbx)
	movq	$0, 560(%rbx)
	movq	$0, 584(%rbx)
	movq	%r12, 592(%rbx)
	movl	$0, 608(%rbx)
	movq	$0, 616(%rbx)
	movq	$0, 640(%rbx)
	movq	%r12, 648(%rbx)
	movl	$0, 664(%rbx)
	movq	$0, 672(%rbx)
	movq	$0, 696(%rbx)
	movq	%r12, 704(%rbx)
	movl	$0, 720(%rbx)
	movq	$0, 728(%rbx)
	movq	$0, 752(%rbx)
	movq	%r12, 760(%rbx)
	movl	$0, 776(%rbx)
	movq	$0, 784(%rbx)
	movq	$0, 808(%rbx)
	movq	%r12, 816(%rbx)
	movl	$0, 832(%rbx)
	movq	$0, 840(%rbx)
	movq	%rax, 848(%rbx)
	movq	%rax, 856(%rbx)
	leaq	888(%rbx), %rax
	movq	%rax, 904(%rbx)
	movq	%rax, 912(%rbx)
	leaq	-288(%rbp), %rax
	movq	%rax, -336(%rbp)
	movq	%rax, -272(%rbp)
	movq	%rax, -264(%rbp)
	leaq	-232(%rbp), %rax
	movq	%rax, -216(%rbp)
	movq	%rax, -208(%rbp)
	leaq	-176(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	%rax, -152(%rbp)
	leaq	-120(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	$0, 864(%rbx)
	movq	%r12, 872(%rbx)
	movl	$0, 888(%rbx)
	movq	$0, 896(%rbx)
	movq	$0, 920(%rbx)
	movq	%r12, -304(%rbp)
	movl	$0, -288(%rbp)
	movq	$0, -280(%rbp)
	movq	$0, -256(%rbp)
	movq	%r12, -248(%rbp)
	movl	$0, -232(%rbp)
	movq	$0, -224(%rbp)
	movq	$0, -200(%rbp)
	movq	%r12, -192(%rbp)
	movl	$0, -176(%rbp)
	movq	$0, -168(%rbp)
	movq	$0, -144(%rbp)
	movq	%r12, -136(%rbp)
	movl	$0, -120(%rbp)
	movq	$0, -112(%rbp)
	movq	%rax, -96(%rbp)
	addl	248(%rbx), %ecx
	leal	1(%rcx), %eax
	movq	$0, -88(%rbp)
	cltq
	cmpq	$9586980, %rax
	ja	.L2327
	leaq	0(,%rax,8), %r10
	xorl	%r9d, %r9d
	movq	%r12, 928(%rbx)
	movq	$0, 936(%rbx)
	subq	%rax, %r10
	movq	$0, 944(%rbx)
	salq	$5, %r10
	movq	$0, 952(%rbx)
	testq	%rax, %rax
	je	.L2322
	movq	16(%r12), %rax
	movq	24(%r12), %rdi
	movq	%r10, %rsi
	subq	%rax, %rdi
	cmpq	%rdi, %r10
	ja	.L2328
	addq	%rax, %rsi
	movq	%rsi, 16(%r12)
.L2318:
	movslq	%ecx, %rcx
	leaq	(%rax,%r10), %r9
	movq	%rax, 936(%rbx)
	addq	$1, %rcx
	movq	%rax, 944(%rbx)
	leaq	0(,%rcx,8), %r12
	movq	%r9, 952(%rbx)
	subq	%rcx, %r12
	salq	$5, %r12
	addq	%rax, %r12
	.p2align 4,,10
	.p2align 3
.L2319:
	movq	%rax, %rdi
	movq	%r14, %rsi
	movq	%r9, -320(%rbp)
	movq	%rax, -312(%rbp)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movq	-312(%rbp), %rax
	movq	-320(%rbp), %r9
	addq	$224, %rax
	cmpq	%rax, %r12
	jne	.L2319
.L2322:
	movq	%r9, 944(%rbx)
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	movq	240(%r15), %rax
	testq	%rax, %rax
	je	.L2329
	leaq	312(%rbx), %rdi
	movq	%r14, %rsi
	movq	%rax, -304(%rbp)
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_
.L2321:
	movq	-328(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler5Hints3AddERKS2_
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2330
	addq	$296, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2328:
	.cfi_restore_state
	movq	%r12, %rdi
	movq	%r10, -320(%rbp)
	movl	%ecx, -312(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-312(%rbp), %ecx
	movq	-320(%rbp), %r10
	jmp	.L2318
	.p2align 4,,10
	.p2align 3
.L2329:
	movq	(%r15), %rax
	movq	%r13, %rsi
	movq	%rax, -304(%rbp)
	movq	8(%r15), %rax
	movq	-336(%rbp), %r15
	movq	%rax, -296(%rbp)
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	leaq	424(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZNSt8_Rb_treeIN2v88internal8compiler17FunctionBlueprintES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE16_M_insert_uniqueIRKS3_EESt4pairISt17_Rb_tree_iteratorIS3_EbEOT_
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	jmp	.L2321
.L2330:
	call	__stack_chk_fail@PLT
.L2327:
	leaq	.LC49(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE18822:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11EnvironmentC2EPNS0_4ZoneENS1_18CompilationSubjectE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation11EnvironmentC2EPNS0_4ZoneENS1_18CompilationSubjectE
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11EnvironmentC1EPNS0_4ZoneENS1_18CompilationSubjectE
	.set	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11EnvironmentC1EPNS0_4ZoneENS1_18CompilationSubjectE,_ZN2v88internal8compiler34SerializerForBackgroundCompilation11EnvironmentC2EPNS0_4ZoneENS1_18CompilationSubjectE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilationC2EPNS1_12JSHeapBrokerEPNS1_23CompilationDependenciesEPNS0_4ZoneENS0_6HandleINS0_10JSFunctionEEENS_4base5FlagsINS1_38SerializerForBackgroundCompilationFlagEiEENS0_9BailoutIdE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilationC2EPNS1_12JSHeapBrokerEPNS1_23CompilationDependenciesEPNS0_4ZoneENS0_6HandleINS0_10JSFunctionEEENS_4base5FlagsINS1_38SerializerForBackgroundCompilationFlagEiEENS0_9BailoutIdE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilationC2EPNS1_12JSHeapBrokerEPNS1_23CompilationDependenciesEPNS0_4ZoneENS0_6HandleINS0_10JSFunctionEEENS_4base5FlagsINS1_38SerializerForBackgroundCompilationFlagEiEENS0_9BailoutIdE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilationC2EPNS1_12JSHeapBrokerEPNS1_23CompilationDependenciesEPNS0_4ZoneENS0_6HandleINS0_10JSFunctionEEENS_4base5FlagsINS1_38SerializerForBackgroundCompilationFlagEiEENS0_9BailoutIdE:
.LFB18869:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-304(%rbp), %r15
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$280, %rsp
	movl	%r9d, -308(%rbp)
	movq	(%rsi), %rdx
	movq	%r8, %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rcx, 16(%rdi)
	movups	%xmm0, (%rdi)
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CompilationSubjectC1ENS0_6HandleINS0_10JSFunctionEEEPNS0_7IsolateEPNS0_4ZoneE
	movq	16(%r12), %rax
	movq	24(%r12), %rdx
	subq	%rax, %rdx
	cmpq	$959, %rdx
	jbe	.L2341
	leaq	960(%rax), %rdx
	movq	%rdx, 16(%r12)
.L2333:
	movq	%r12, %rsi
	movq	%r15, %rdx
	movq	%rax, %rdi
	movq	%rax, -320(%rbp)
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11EnvironmentC1EPNS0_4ZoneENS1_18CompilationSubjectE
	movq	-320(%rbp), %rax
	leaq	-288(%rbp), %rdi
	movq	%rax, 24(%rbx)
	call	_ZN2v88internal8compiler5HintsD1Ev
	leaq	88(%rbx), %rcx
	movq	%r12, 32(%rbx)
	leaq	72(%rbx), %rdi
	movq	%rcx, 40(%rbx)
	movl	$100, %esi
	movq	$1, 48(%rbx)
	movq	$0, 56(%rbx)
	movq	$0, 64(%rbx)
	movl	$0x3f800000, 72(%rbx)
	movq	$0, 80(%rbx)
	movq	$0, 88(%rbx)
	movq	%rcx, -320(%rbp)
	call	_ZNKSt8__detail20_Prime_rehash_policy11_M_next_bktEm@PLT
	movq	%rax, %r12
	cmpq	48(%rbx), %rax
	jbe	.L2334
	cmpq	$1, %rax
	movq	-320(%rbp), %rcx
	je	.L2342
	movq	32(%rbx), %rdi
	leaq	0(,%rax,8), %rdx
	movq	%rdx, %rsi
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	subq	%rcx, %rax
	cmpq	%rax, %rdx
	ja	.L2343
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
.L2338:
	movq	%rcx, %rdi
	xorl	%esi, %esi
	call	memset@PLT
	movq	%rax, %rcx
.L2336:
	movq	%rcx, 40(%rbx)
	movq	%r12, 48(%rbx)
.L2334:
	movl	-308(%rbp), %eax
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movl	$1, %ecx
	movl	%eax, 96(%rbx)
	movl	16(%rbp), %eax
	movl	%eax, 100(%rbx)
	call	_ZN2v88internal8compiler13JSFunctionRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13JSFunctionRef9SerializeEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2344
	addq	$280, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2342:
	.cfi_restore_state
	movq	$0, 88(%rbx)
	jmp	.L2336
	.p2align 4,,10
	.p2align 3
.L2341:
	movl	$960, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L2333
	.p2align 4,,10
	.p2align 3
.L2343:
	movq	%rdx, -320(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-320(%rbp), %rdx
	movq	%rax, %rcx
	jmp	.L2338
.L2344:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18869:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilationC2EPNS1_12JSHeapBrokerEPNS1_23CompilationDependenciesEPNS0_4ZoneENS0_6HandleINS0_10JSFunctionEEENS_4base5FlagsINS1_38SerializerForBackgroundCompilationFlagEiEENS0_9BailoutIdE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilationC2EPNS1_12JSHeapBrokerEPNS1_23CompilationDependenciesEPNS0_4ZoneENS0_6HandleINS0_10JSFunctionEEENS_4base5FlagsINS1_38SerializerForBackgroundCompilationFlagEiEENS0_9BailoutIdE
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilationC1EPNS1_12JSHeapBrokerEPNS1_23CompilationDependenciesEPNS0_4ZoneENS0_6HandleINS0_10JSFunctionEEENS_4base5FlagsINS1_38SerializerForBackgroundCompilationFlagEiEENS0_9BailoutIdE
	.set	_ZN2v88internal8compiler34SerializerForBackgroundCompilationC1EPNS1_12JSHeapBrokerEPNS1_23CompilationDependenciesEPNS0_4ZoneENS0_6HandleINS0_10JSFunctionEEENS_4base5FlagsINS1_38SerializerForBackgroundCompilationFlagEiEENS0_9BailoutIdE,_ZN2v88internal8compiler34SerializerForBackgroundCompilationC2EPNS1_12JSHeapBrokerEPNS1_23CompilationDependenciesEPNS0_4ZoneENS0_6HandleINS0_10JSFunctionEEENS_4base5FlagsINS1_38SerializerForBackgroundCompilationFlagEiEENS0_9BailoutIdE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitCreateClosureEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitCreateClosureEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitCreateClosureEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitCreateClosureEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB18967:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$544, %rsp
	movq	24(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment17accumulator_hintsEv
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler5Hints5ClearEv
	movq	(%rbx), %rax
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	(%rax), %rdx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor26GetConstantForIndexOperandEiPNS0_7IsolateE@PLT
	movq	%rbx, %rdi
	movq	%rax, %r13
	call	_ZNK2v88internal8compiler34SerializerForBackgroundCompilation15feedback_vectorEv
	movl	$1, %esi
	movq	%r12, %rdi
	movq	(%rax), %r14
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor15GetIndexOperandEi@PLT
	movq	23(%r14), %rdx
	leal	16(,%rax,8), %eax
	cltq
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	24(%rcx), %r14
	movq	-1(%rdx,%rax), %rsi
	movq	3520(%r14), %rdi
	subq	$37592, %r14
	testq	%rdi, %rdi
	je	.L2346
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r12
.L2347:
	movq	(%rbx), %rsi
	leaq	-560(%rbp), %rdi
	movl	$1, %ecx
	movq	%r12, %rdx
	call	_ZN2v88internal8compiler15FeedbackCellRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	(%rbx), %rax
	movq	(%rax), %r14
	movq	(%r12), %rax
	movq	41112(%r14), %rdi
	movq	7(%rax), %rsi
	testq	%rdi, %rdi
	je	.L2349
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r12
.L2350:
	movq	(%rbx), %rsi
	leaq	-544(%rbp), %rdi
	movl	$1, %ecx
	movq	%r12, %rdx
	call	_ZN2v88internal8compiler9ObjectRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	(%r12), %rax
	testb	$1, %al
	jne	.L2357
.L2345:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2358
	addq	$544, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2346:
	.cfi_restore_state
	movq	41088(%r14), %r12
	cmpq	41096(%r14), %r12
	je	.L2359
.L2348:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%r12)
	jmp	.L2347
	.p2align 4,,10
	.p2align 3
.L2349:
	movq	41088(%r14), %r12
	cmpq	41096(%r14), %r12
	je	.L2360
.L2351:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%r12)
	jmp	.L2350
	.p2align 4,,10
	.p2align 3
.L2357:
	movq	-1(%rax), %rax
	cmpw	$155, 11(%rax)
	jne	.L2345
	movq	24(%rbx), %rax
	movq	%r12, %xmm1
	movq	%r13, %xmm0
	leaq	-512(%rbp), %r12
	punpcklqdq	%xmm1, %xmm0
	movq	%r12, %rdi
	leaq	-272(%rbp), %r13
	leaq	480(%rax), %rsi
	movaps	%xmm0, -528(%rbp)
	leaq	-288(%rbp), %r14
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movq	24(%rbx), %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment17accumulator_hintsEv
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	movq	-528(%rbp), %rax
	movq	%rax, -288(%rbp)
	movq	-520(%rbp), %rax
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	leaq	168(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZNSt8_Rb_treeIN2v88internal8compiler17FunctionBlueprintES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE16_M_insert_uniqueIRKS3_EESt4pairISt17_Rb_tree_iteratorIS3_EbEOT_
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	jmp	.L2345
	.p2align 4,,10
	.p2align 3
.L2360:
	movq	%r14, %rdi
	movq	%rsi, -568(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-568(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L2351
	.p2align 4,,10
	.p2align 3
.L2359:
	movq	%r14, %rdi
	movq	%rsi, -568(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-568(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L2348
.L2358:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18967:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitCreateClosureEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitCreateClosureEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment19ExportRegisterHintsENS0_11interpreter8RegisterEmPNS0_10ZoneVectorINS1_5HintsEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment19ExportRegisterHintsENS0_11interpreter8RegisterEmPNS0_10ZoneVectorINS1_5HintsEEE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment19ExportRegisterHintsENS0_11interpreter8RegisterEmPNS0_10ZoneVectorINS1_5HintsEEE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment19ExportRegisterHintsENS0_11interpreter8RegisterEmPNS0_10ZoneVectorINS1_5HintsEEE:
.LFB19051:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leal	(%rsi,%rdx), %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	jle	.L2361
	movq	%rdi, %r13
	movq	%rcx, %r12
	leaq	-64(%rbp), %r14
	movl	%esi, %ebx
	.p2align 4,,10
	.p2align 3
.L2372:
	movq	%r14, %rdi
	movl	%ebx, -64(%rbp)
	call	_ZNK2v88internal11interpreter8Register19is_function_closureEv@PLT
	leaq	256(%r13), %rsi
	testb	%al, %al
	jne	.L2364
	movq	%r14, %rdi
	call	_ZNK2v88internal11interpreter8Register18is_current_contextEv@PLT
	leaq	480(%r13), %rsi
	testb	%al, %al
	jne	.L2364
	movl	-64(%rbp), %edx
	movl	248(%r13), %esi
	movl	%edx, -60(%rbp)
	leal	(%rdx,%rsi), %eax
	testl	%edx, %edx
	js	.L2376
.L2367:
	movq	936(%r13), %rdx
	movslq	%eax, %rcx
	testl	%eax, %eax
	js	.L2368
	movq	944(%r13), %rax
	movabsq	$7905747460161236407, %rsi
	subq	%rdx, %rax
	sarq	$5, %rax
	imulq	%rsi, %rax
	cmpq	%rcx, %rax
	ja	.L2368
	leaq	.LC24(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2368:
	leaq	0(,%rcx,8), %rsi
	subq	%rcx, %rsi
	salq	$5, %rsi
	addq	%rdx, %rsi
.L2364:
	movq	16(%r12), %rdi
	cmpq	24(%r12), %rdi
	je	.L2369
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	addq	$224, 16(%r12)
.L2370:
	addl	$1, %ebx
	cmpl	%ebx, %r15d
	jne	.L2372
.L2361:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2377
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2369:
	.cfi_restore_state
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	movq	%r12, %rdi
	call	_ZNSt6vectorIN2v88internal8compiler5HintsENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	jmp	.L2370
	.p2align 4,,10
	.p2align 3
.L2376:
	leaq	-60(%rbp), %rdi
	call	_ZNK2v88internal11interpreter8Register16ToParameterIndexEi@PLT
	jmp	.L2367
.L2377:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19051:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment19ExportRegisterHintsENS0_11interpreter8RegisterEmPNS0_10ZoneVectorINS1_5HintsEEE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment19ExportRegisterHintsENS0_11interpreter8RegisterEmPNS0_10ZoneVectorINS1_5HintsEEE
	.section	.rodata._ZN2v88internal8compiler34SerializerForBackgroundCompilation20VisitInvokeIntrinsicEPNS0_11interpreter21BytecodeArrayIteratorE.str1.1,"aMS",@progbits,1
.LC51:
	.string	"reg_count == 3"
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation20VisitInvokeIntrinsicEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation20VisitInvokeIntrinsicEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation20VisitInvokeIntrinsicEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation20VisitInvokeIntrinsicEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB18930:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor21GetIntrinsicIdOperandEi@PLT
	cmpl	$486, %eax
	jg	.L2379
	cmpl	$469, %eax
	jg	.L2380
	cmpl	$110, %eax
	jg	.L2381
	cmpl	$99, %eax
	jle	.L2379
	subl	$100, %eax
	cmpl	$10, %eax
	ja	.L2379
	leaq	.L2394(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler34SerializerForBackgroundCompilation20VisitInvokeIntrinsicEPNS0_11interpreter21BytecodeArrayIteratorE,"a",@progbits
	.align 4
	.align 4
.L2394:
	.long	.L2393-.L2394
	.long	.L2392-.L2394
	.long	.L2379-.L2394
	.long	.L2391-.L2394
	.long	.L2395-.L2394
	.long	.L2389-.L2394
	.long	.L2388-.L2394
	.long	.L2379-.L2394
	.long	.L2387-.L2394
	.long	.L2386-.L2394
	.long	.L2385-.L2394
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation20VisitInvokeIntrinsicEPNS0_11interpreter21BytecodeArrayIteratorE
	.p2align 4,,10
	.p2align 3
.L2381:
	cmpl	$210, %eax
	jne	.L2379
.L2383:
	movq	(%rbx), %rax
	movl	$161, %esi
	movq	(%rax), %rdi
	addq	$41184, %rdi
	.p2align 4,,10
	.p2align 3
.L2409:
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	movq	(%rbx), %rsi
	leaq	-80(%rbp), %rdi
	movl	$1, %ecx
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler9ObjectRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
.L2379:
	movq	24(%rbx), %rax
	movq	936(%rax), %rbx
	movq	944(%rax), %r12
	cmpq	%r12, %rbx
	je	.L2378
	.p2align 4,,10
	.p2align 3
.L2401:
	movq	%rbx, %rdi
	addq	$224, %rbx
	call	_ZN2v88internal8compiler5Hints5ClearEv
	cmpq	%rbx, %r12
	jne	.L2401
.L2378:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2410
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2380:
	.cfi_restore_state
	subl	$470, %eax
	cmpl	$16, %eax
	ja	.L2379
	leaq	.L2384(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler34SerializerForBackgroundCompilation20VisitInvokeIntrinsicEPNS0_11interpreter21BytecodeArrayIteratorE
	.align 4
	.align 4
.L2384:
	.long	.L2393-.L2384
	.long	.L2392-.L2384
	.long	.L2379-.L2384
	.long	.L2391-.L2384
	.long	.L2390-.L2384
	.long	.L2389-.L2384
	.long	.L2388-.L2384
	.long	.L2387-.L2384
	.long	.L2386-.L2384
	.long	.L2385-.L2384
	.long	.L2379-.L2384
	.long	.L2379-.L2384
	.long	.L2379-.L2384
	.long	.L2379-.L2384
	.long	.L2379-.L2384
	.long	.L2379-.L2384
	.long	.L2383-.L2384
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation20VisitInvokeIntrinsicEPNS0_11interpreter21BytecodeArrayIteratorE
.L2390:
	movq	(%rbx), %rax
	movl	$228, %esi
	leaq	-80(%rbp), %r14
	movq	(%rax), %rdi
	addq	$41184, %rdi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	movq	(%rbx), %rsi
	movl	$1, %ecx
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler9ObjectRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor23GetRegisterCountOperandEi@PLT
	cmpl	$3, %eax
	je	.L2411
	leaq	.LC51(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2393:
	movq	(%rbx), %rax
	movl	$230, %esi
	movq	(%rax), %rdi
	addq	$41184, %rdi
	jmp	.L2409
.L2392:
	movq	(%rbx), %rax
	movl	$231, %esi
	movq	(%rax), %rdi
	addq	$41184, %rdi
	jmp	.L2409
.L2391:
	movq	(%rbx), %rax
	movl	$227, %esi
	movq	(%rax), %rdi
	addq	$41184, %rdi
	jmp	.L2409
.L2395:
	movq	(%rbx), %rax
	movl	$228, %esi
	movq	(%rax), %rdi
	addq	$41184, %rdi
	jmp	.L2409
.L2389:
	movq	(%rbx), %rax
	movl	$683, %esi
	movq	(%rax), %rdi
	addq	$41184, %rdi
	jmp	.L2409
.L2388:
	movq	(%rbx), %rax
	movl	$684, %esi
	movq	(%rax), %rdi
	addq	$41184, %rdi
	jmp	.L2409
.L2387:
	movq	(%rbx), %rax
	movl	$675, %esi
	movq	(%rax), %rdi
	addq	$41184, %rdi
	jmp	.L2409
.L2386:
	movq	(%rbx), %rax
	movl	$674, %esi
	movq	(%rax), %rdi
	addq	$41184, %rdi
	jmp	.L2409
.L2385:
	movq	(%rbx), %rax
	movl	$676, %esi
	movq	(%rax), %rdi
	addq	$41184, %rdi
	jmp	.L2409
.L2411:
	movq	16(%rbx), %rax
	movq	24(%rbx), %rdi
	movq	%r14, %rcx
	movl	$3, %edx
	movl	%r13d, %esi
	movq	$0, -72(%rbp)
	movq	%rax, -80(%rbp)
	movq	$0, -64(%rbp)
	movq	$0, -56(%rbp)
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment19ExportRegisterHintsENS0_11interpreter8RegisterEmPNS0_10ZoneVectorINS1_5HintsEEE
	movq	-72(%rbp), %rax
	movq	%rbx, %rdi
	leaq	224(%rax), %rsi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation29ProcessHintsForPromiseResolveERKNS1_5HintsE
	movq	24(%rbx), %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment17accumulator_hintsEv
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler5Hints5ClearEv
	movq	-64(%rbp), %r12
	movq	-72(%rbp), %rbx
	cmpq	%r12, %rbx
	je	.L2378
	.p2align 4,,10
	.p2align 3
.L2398:
	movq	%rbx, %rdi
	addq	$224, %rbx
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	%rbx, %r12
	jne	.L2398
	jmp	.L2378
.L2410:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18930:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation20VisitInvokeIntrinsicEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation20VisitInvokeIntrinsicEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZNSt8_Rb_treeIN2v88internal8compiler17FunctionBlueprintES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE7_M_copyINSA_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS3_EPKSE_PSt18_Rb_tree_node_baseRT_,"axG",@progbits,_ZNSt8_Rb_treeIN2v88internal8compiler17FunctionBlueprintES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE7_M_copyINSA_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS3_EPKSE_PSt18_Rb_tree_node_baseRT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN2v88internal8compiler17FunctionBlueprintES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE7_M_copyINSA_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS3_EPKSE_PSt18_Rb_tree_node_baseRT_
	.type	_ZNSt8_Rb_treeIN2v88internal8compiler17FunctionBlueprintES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE7_M_copyINSA_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS3_EPKSE_PSt18_Rb_tree_node_baseRT_, @function
_ZNSt8_Rb_treeIN2v88internal8compiler17FunctionBlueprintES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE7_M_copyINSA_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS3_EPKSE_PSt18_Rb_tree_node_baseRT_:
.LFB23884:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	8(%rcx), %r12
	movq	%rdi, -64(%rbp)
	testq	%r12, %r12
	je	.L2413
	movq	8(%r12), %rax
	movq	%rax, 8(%rcx)
	testq	%rax, %rax
	je	.L2414
	cmpq	24(%rax), %r12
	je	.L2475
	movq	$0, 16(%rax)
.L2419:
	leaq	48(%r12), %rdi
	movq	%rdi, -56(%rbp)
	call	_ZN2v88internal8compiler5HintsD1Ev
	movq	32(%rbx), %rax
	movq	-56(%rbp), %rdi
	leaq	48(%rbx), %rsi
	movq	%rax, 32(%r12)
	movq	40(%rbx), %rax
	movq	%rax, 40(%r12)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
.L2437:
	movl	(%rbx), %eax
	movq	%r14, 8(%r12)
	movq	$0, 16(%r12)
	movl	%eax, (%r12)
	movq	$0, 24(%r12)
	movq	24(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L2421
	movq	-64(%rbp), %rdi
	movq	%r15, %rcx
	movq	%r12, %rdx
	call	_ZNSt8_Rb_treeIN2v88internal8compiler17FunctionBlueprintES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE7_M_copyINSA_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS3_EPKSE_PSt18_Rb_tree_node_baseRT_
	movq	%rax, 24(%r12)
.L2421:
	movq	16(%rbx), %r14
	testq	%r14, %r14
	je	.L2412
	movq	8(%r15), %rbx
	movq	%r12, %r13
	testq	%rbx, %rbx
	je	.L2423
.L2477:
	movq	8(%rbx), %rax
	movq	%rax, 8(%r15)
	testq	%rax, %rax
	je	.L2424
	cmpq	24(%rax), %rbx
	je	.L2476
	movq	$0, 16(%rax)
.L2429:
	leaq	48(%rbx), %rdi
	movq	%rdi, -56(%rbp)
	call	_ZN2v88internal8compiler5HintsD1Ev
	movq	32(%r14), %rax
	movq	-56(%rbp), %rdi
	leaq	48(%r14), %rsi
	movq	%rax, 32(%rbx)
	movq	40(%r14), %rax
	movq	%rax, 40(%rbx)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
.L2434:
	movl	(%r14), %eax
	movq	$0, 16(%rbx)
	movq	$0, 24(%rbx)
	movl	%eax, (%rbx)
	movq	%rbx, 16(%r13)
	movq	%r13, 8(%rbx)
	movq	24(%r14), %rsi
	testq	%rsi, %rsi
	je	.L2431
	movq	-64(%rbp), %rdi
	movq	%r15, %rcx
	movq	%rbx, %rdx
	call	_ZNSt8_Rb_treeIN2v88internal8compiler17FunctionBlueprintES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE7_M_copyINSA_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS3_EPKSE_PSt18_Rb_tree_node_baseRT_
	movq	%rax, 24(%rbx)
	movq	16(%r14), %r14
	testq	%r14, %r14
	je	.L2412
.L2432:
	movq	%rbx, %r13
	movq	8(%r15), %rbx
	testq	%rbx, %rbx
	jne	.L2477
.L2423:
	movq	16(%r15), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	$271, %rax
	jbe	.L2478
	leaq	272(%rbx), %rax
	movq	%rax, 16(%rdi)
.L2430:
	movq	32(%r14), %rax
	leaq	48(%r14), %rsi
	leaq	48(%rbx), %rdi
	movq	%rax, 32(%rbx)
	movq	40(%r14), %rax
	movq	%rax, 40(%rbx)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	jmp	.L2434
	.p2align 4,,10
	.p2align 3
.L2431:
	movq	16(%r14), %r14
	testq	%r14, %r14
	jne	.L2432
.L2412:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2424:
	.cfi_restore_state
	movq	$0, (%r15)
	jmp	.L2429
	.p2align 4,,10
	.p2align 3
.L2476:
	movq	$0, 24(%rax)
	movq	8(%r15), %rax
	movq	16(%rax), %rcx
	testq	%rcx, %rcx
	je	.L2429
	movq	%rcx, 8(%r15)
	movq	24(%rcx), %rax
	testq	%rax, %rax
	je	.L2427
	.p2align 4,,10
	.p2align 3
.L2428:
	movq	%rax, 8(%r15)
	movq	%rax, %rcx
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L2428
.L2427:
	movq	16(%rcx), %rax
	testq	%rax, %rax
	je	.L2429
	movq	%rax, 8(%r15)
	jmp	.L2429
	.p2align 4,,10
	.p2align 3
.L2413:
	movq	16(%rcx), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	subq	%r12, %rax
	cmpq	$271, %rax
	jbe	.L2479
	leaq	272(%r12), %rax
	movq	%rax, 16(%rdi)
.L2420:
	movq	32(%rbx), %rax
	leaq	48(%rbx), %rsi
	leaq	48(%r12), %rdi
	movq	%rax, 32(%r12)
	movq	40(%rbx), %rax
	movq	%rax, 40(%r12)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	jmp	.L2437
	.p2align 4,,10
	.p2align 3
.L2414:
	movq	$0, (%rcx)
	jmp	.L2419
	.p2align 4,,10
	.p2align 3
.L2475:
	movq	$0, 24(%rax)
	movq	8(%rcx), %rax
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2419
	movq	%rdx, 8(%rcx)
	movq	24(%rdx), %rax
	testq	%rax, %rax
	je	.L2417
	.p2align 4,,10
	.p2align 3
.L2418:
	movq	%rax, 8(%r15)
	movq	%rax, %rdx
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L2418
.L2417:
	movq	16(%rdx), %rax
	testq	%rax, %rax
	je	.L2419
	movq	%rax, 8(%r15)
	jmp	.L2419
	.p2align 4,,10
	.p2align 3
.L2478:
	movl	$272, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rbx
	jmp	.L2430
.L2479:
	movl	$272, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L2420
	.cfi_endproc
.LFE23884:
	.size	_ZNSt8_Rb_treeIN2v88internal8compiler17FunctionBlueprintES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE7_M_copyINSA_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS3_EPKSE_PSt18_Rb_tree_node_baseRT_, .-_ZNSt8_Rb_treeIN2v88internal8compiler17FunctionBlueprintES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE7_M_copyINSA_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS3_EPKSE_PSt18_Rb_tree_node_baseRT_
	.section	.text._ZN2v88internal8compiler5HintsaSERKS2_,"axG",@progbits,_ZN2v88internal8compiler5HintsaSERKS2_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler5HintsaSERKS2_
	.type	_ZN2v88internal8compiler5HintsaSERKS2_, @function
_ZN2v88internal8compiler5HintsaSERKS2_:
.LFB18846:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	%rdi, %rsi
	je	.L2481
	movq	24(%rdi), %r13
	movq	40(%rdi), %rax
	movq	%rdi, -64(%rbp)
	leaq	16(%rdi), %rdx
	movq	%r13, -80(%rbp)
	movq	%rax, -72(%rbp)
	testq	%r13, %r13
	je	.L2482
	movq	$0, 8(%r13)
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L2483
	movq	%rax, -72(%rbp)
.L2483:
	movq	$0, 24(%r12)
	movq	%rdx, 32(%r12)
	movq	%rdx, 40(%r12)
	movq	$0, 48(%r12)
	movq	24(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L2590
.L2484:
	leaq	-80(%rbp), %rcx
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal8compiler14VirtualContextES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE7_M_copyINSA_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS3_EPKSE_PSt18_Rb_tree_node_baseRT_
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L2485:
	movq	%rdx, %rcx
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L2485
	movq	%rcx, 32(%r12)
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L2486:
	movq	%rdx, %rcx
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L2486
	movq	-80(%rbp), %r13
	movq	-64(%rbp), %r15
	movq	%rcx, 40(%r12)
	movq	48(%rbx), %rdx
	movq	%rax, 24(%r12)
	movq	%rdx, 48(%r12)
	testq	%r13, %r13
	je	.L2481
	.p2align 4,,10
	.p2align 3
.L2490:
	movq	24(%r13), %r14
	testq	%r14, %r14
	je	.L2488
.L2489:
	movq	24(%r14), %rsi
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal8compiler14VirtualContextES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE8_M_eraseEPSt13_Rb_tree_nodeIS3_E
	movq	16(%r14), %r14
	testq	%r14, %r14
	jne	.L2489
.L2488:
	movq	16(%r13), %r13
	testq	%r13, %r13
	jne	.L2490
.L2481:
	leaq	56(%r12), %rdi
	leaq	56(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2491
	movq	80(%r12), %r13
	movq	96(%r12), %rax
	movq	%rdi, -64(%rbp)
	leaq	72(%r12), %rdx
	movq	%r13, -80(%rbp)
	movq	%rax, -72(%rbp)
	testq	%r13, %r13
	je	.L2492
	movq	$0, 8(%r13)
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L2493
	movq	%rax, -72(%rbp)
.L2493:
	movq	$0, 80(%r12)
	movq	%rdx, 88(%r12)
	movq	%rdx, 96(%r12)
	movq	$0, 104(%r12)
	movq	80(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L2591
.L2494:
	leaq	-80(%rbp), %rcx
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE7_M_copyINSC_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS4_EPKSG_PSt18_Rb_tree_node_baseRT_
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L2495:
	movq	%rdx, %rcx
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L2495
	movq	%rcx, 88(%r12)
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L2496:
	movq	%rdx, %rcx
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L2496
	movq	-80(%rbp), %r13
	movq	-64(%rbp), %r15
	movq	%rcx, 96(%r12)
	movq	104(%rbx), %rdx
	movq	%rax, 80(%r12)
	movq	%rdx, 104(%r12)
	testq	%r13, %r13
	je	.L2491
	.p2align 4,,10
	.p2align 3
.L2500:
	movq	24(%r13), %r14
	testq	%r14, %r14
	je	.L2498
.L2499:
	movq	24(%r14), %rsi
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	movq	16(%r14), %r14
	testq	%r14, %r14
	jne	.L2499
.L2498:
	movq	16(%r13), %r13
	testq	%r13, %r13
	jne	.L2500
.L2491:
	leaq	112(%r12), %rdi
	leaq	112(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2501
	movq	136(%r12), %r13
	movq	152(%r12), %rax
	movq	%rdi, -64(%rbp)
	leaq	128(%r12), %rdx
	movq	%r13, -80(%rbp)
	movq	%rax, -72(%rbp)
	testq	%r13, %r13
	je	.L2502
	movq	$0, 8(%r13)
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L2503
	movq	%rax, -72(%rbp)
.L2503:
	movq	$0, 136(%r12)
	movq	%rdx, 144(%r12)
	movq	%rdx, 152(%r12)
	movq	$0, 160(%r12)
	movq	136(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L2592
.L2504:
	leaq	-80(%rbp), %rcx
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_3MapEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE7_M_copyINSC_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS4_EPKSG_PSt18_Rb_tree_node_baseRT_
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L2505:
	movq	%rdx, %rcx
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L2505
	movq	%rcx, 144(%r12)
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L2506:
	movq	%rdx, %rcx
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L2506
	movq	-80(%rbp), %r13
	movq	-64(%rbp), %r15
	movq	%rcx, 152(%r12)
	movq	160(%rbx), %rdx
	movq	%rax, 136(%r12)
	movq	%rdx, 160(%r12)
	testq	%r13, %r13
	je	.L2501
	.p2align 4,,10
	.p2align 3
.L2510:
	movq	24(%r13), %r14
	testq	%r14, %r14
	je	.L2508
.L2509:
	movq	24(%r14), %rsi
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_3MapEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	movq	16(%r14), %r14
	testq	%r14, %r14
	jne	.L2509
.L2508:
	movq	16(%r13), %r13
	testq	%r13, %r13
	jne	.L2510
.L2501:
	leaq	168(%r12), %rdi
	leaq	168(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2511
	movq	192(%r12), %r13
	movq	208(%r12), %rax
	movq	%rdi, -64(%rbp)
	leaq	184(%r12), %rdx
	movq	%r13, -80(%rbp)
	movq	%rax, -72(%rbp)
	testq	%r13, %r13
	je	.L2512
	movq	$0, 8(%r13)
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L2513
	movq	%rax, -72(%rbp)
.L2513:
	movq	$0, 192(%r12)
	movq	%rdx, 200(%r12)
	movq	%rdx, 208(%r12)
	movq	$0, 216(%r12)
	movq	192(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L2593
.L2514:
	leaq	-80(%rbp), %rcx
	call	_ZNSt8_Rb_treeIN2v88internal8compiler17FunctionBlueprintES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE7_M_copyINSA_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS3_EPKSE_PSt18_Rb_tree_node_baseRT_
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L2515:
	movq	%rdx, %rcx
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L2515
	movq	%rcx, 200(%r12)
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L2516:
	movq	%rdx, %rcx
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L2516
	movq	%rcx, 208(%r12)
	movq	-80(%rbp), %r13
	movq	216(%rbx), %rdx
	movq	%rax, 192(%r12)
	movq	-64(%rbp), %rbx
	movq	%rdx, 216(%r12)
	testq	%r13, %r13
	je	.L2511
	.p2align 4,,10
	.p2align 3
.L2518:
	movq	24(%r13), %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal8compiler17FunctionBlueprintES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE8_M_eraseEPSt13_Rb_tree_nodeIS3_E
	movq	%r13, %rdi
	movq	16(%r13), %r13
	addq	$48, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	testq	%r13, %r13
	jne	.L2518
.L2511:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2594
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2512:
	.cfi_restore_state
	movq	%rdx, 200(%r12)
	movq	%rdx, 208(%r12)
	movq	$0, 216(%r12)
	movq	192(%rbx), %rsi
	movq	$0, -72(%rbp)
	testq	%rsi, %rsi
	jne	.L2514
	jmp	.L2511
	.p2align 4,,10
	.p2align 3
.L2492:
	movq	%rdx, 88(%r12)
	movq	%rdx, 96(%r12)
	movq	$0, 104(%r12)
	movq	80(%rbx), %rsi
	movq	$0, -72(%rbp)
	testq	%rsi, %rsi
	jne	.L2494
	jmp	.L2491
	.p2align 4,,10
	.p2align 3
.L2502:
	movq	%rdx, 144(%r12)
	movq	%rdx, 152(%r12)
	movq	$0, 160(%r12)
	movq	136(%rbx), %rsi
	movq	$0, -72(%rbp)
	testq	%rsi, %rsi
	jne	.L2504
	jmp	.L2501
	.p2align 4,,10
	.p2align 3
.L2482:
	movq	%rdx, 32(%rdi)
	movq	%rdx, 40(%rdi)
	movq	$0, 48(%rdi)
	movq	24(%rsi), %rsi
	movq	$0, -72(%rbp)
	testq	%rsi, %rsi
	jne	.L2484
	jmp	.L2481
	.p2align 4,,10
	.p2align 3
.L2591:
	movq	-64(%rbp), %r15
	jmp	.L2500
	.p2align 4,,10
	.p2align 3
.L2590:
	movq	-64(%rbp), %r15
	jmp	.L2490
	.p2align 4,,10
	.p2align 3
.L2592:
	movq	-64(%rbp), %r15
	jmp	.L2510
	.p2align 4,,10
	.p2align 3
.L2593:
	movq	-64(%rbp), %rbx
	jmp	.L2518
.L2594:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18846:
	.size	_ZN2v88internal8compiler5HintsaSERKS2_, .-_ZN2v88internal8compiler5HintsaSERKS2_
	.section	.text._ZNSt6vectorIN2v88internal8compiler5HintsENS1_13ZoneAllocatorIS3_EEEaSERKS6_,"axG",@progbits,_ZNSt6vectorIN2v88internal8compiler5HintsENS1_13ZoneAllocatorIS3_EEEaSERKS6_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal8compiler5HintsENS1_13ZoneAllocatorIS3_EEEaSERKS6_
	.type	_ZNSt6vectorIN2v88internal8compiler5HintsENS1_13ZoneAllocatorIS3_EEEaSERKS6_, @function
_ZNSt6vectorIN2v88internal8compiler5HintsENS1_13ZoneAllocatorIS3_EEEaSERKS6_:
.LFB21351:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -72(%rbp)
	cmpq	%rdi, %rsi
	je	.L2596
	movq	16(%rsi), %r13
	movq	8(%rsi), %r14
	movabsq	$7905747460161236407, %rdi
	movq	8(%r12), %r15
	movq	%r13, %rax
	subq	%r14, %rax
	movq	%rax, %rsi
	movq	%rax, -64(%rbp)
	movq	24(%r12), %rax
	sarq	$5, %rsi
	subq	%r15, %rax
	imulq	%rdi, %rsi
	sarq	$5, %rax
	imulq	%rdi, %rax
	movq	%rsi, %r9
	cmpq	%rsi, %rax
	jb	.L2629
	movq	16(%r12), %rbx
	movq	%rbx, %rax
	subq	%r15, %rax
	movq	%rax, %rcx
	sarq	$5, %rcx
	imulq	%rdi, %rcx
	cmpq	%rcx, %rsi
	ja	.L2607
	cmpq	$0, -64(%rbp)
	movq	%r15, %r13
	jle	.L2630
	.p2align 4,,10
	.p2align 3
.L2608:
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%r9, -56(%rbp)
	addq	$224, %r14
	call	_ZN2v88internal8compiler5HintsaSERKS2_
	movq	-56(%rbp), %r9
	addq	$224, %r13
	subq	$1, %r9
	jne	.L2608
	movq	-64(%rbp), %rdx
	movl	$224, %eax
	testq	%rdx, %rdx
	movq	%rdx, %r13
	cmovg	%rdx, %rax
	addq	%rax, %r15
	movq	8(%r12), %rax
.L2610:
	addq	%rax, %r13
	cmpq	%r15, %rbx
	je	.L2606
	.p2align 4,,10
	.p2align 3
.L2611:
	movq	%r15, %rdi
	addq	$224, %r15
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	%r15, %rbx
	jne	.L2611
.L2606:
	movq	%r13, 16(%r12)
.L2596:
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2629:
	.cfi_restore_state
	xorl	%ebx, %ebx
	testq	%rsi, %rsi
	je	.L2598
	movq	(%r12), %rdi
	movq	-64(%rbp), %rax
	movq	16(%rdi), %rbx
	leaq	7(%rax), %rsi
	movq	24(%rdi), %rax
	andq	$-8, %rsi
	subq	%rbx, %rax
	cmpq	%rax, %rsi
	ja	.L2631
	addq	%rbx, %rsi
	movq	%rsi, 16(%rdi)
.L2598:
	movq	%rbx, %r15
	cmpq	%r13, %r14
	je	.L2605
	.p2align 4,,10
	.p2align 3
.L2604:
	movq	%r14, %rsi
	movq	%r15, %rdi
	addq	$224, %r14
	addq	$224, %r15
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	cmpq	%r14, %r13
	jne	.L2604
.L2605:
	movq	16(%r12), %r14
	movq	8(%r12), %r13
	cmpq	%r13, %r14
	je	.L2602
	.p2align 4,,10
	.p2align 3
.L2603:
	movq	%r13, %rdi
	addq	$224, %r13
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	%r13, %r14
	jne	.L2603
.L2602:
	movq	-64(%rbp), %r13
	movq	%rbx, 8(%r12)
	addq	%rbx, %r13
	movq	%r13, 24(%r12)
	movq	%r13, 16(%r12)
	jmp	.L2596
	.p2align 4,,10
	.p2align 3
.L2607:
	testq	%rax, %rax
	jle	.L2612
	.p2align 4,,10
	.p2align 3
.L2613:
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rcx, -56(%rbp)
	addq	$224, %r14
	call	_ZN2v88internal8compiler5HintsaSERKS2_
	movq	-56(%rbp), %rcx
	addq	$224, %r15
	subq	$1, %rcx
	jne	.L2613
	movq	-72(%rbp), %rax
	movq	16(%r12), %rbx
	movq	8(%r12), %r15
	movq	16(%rax), %r13
	movq	8(%rax), %r14
	movq	%rbx, %rax
	subq	%r15, %rax
.L2612:
	addq	%rax, %r14
	cmpq	%r13, %r14
	je	.L2632
	.p2align 4,,10
	.p2align 3
.L2614:
	movq	%r14, %rsi
	movq	%rbx, %rdi
	addq	$224, %r14
	addq	$224, %rbx
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	cmpq	%r13, %r14
	jne	.L2614
	movq	-64(%rbp), %r13
	addq	8(%r12), %r13
	movq	%r13, 16(%r12)
	jmp	.L2596
	.p2align 4,,10
	.p2align 3
.L2630:
	movq	-64(%rbp), %r13
	movq	%r15, %rax
	jmp	.L2610
	.p2align 4,,10
	.p2align 3
.L2632:
	movq	-64(%rbp), %r13
	addq	%r15, %r13
	jmp	.L2606
.L2631:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rbx
	jmp	.L2598
	.cfi_endproc
.LFE21351:
	.size	_ZNSt6vectorIN2v88internal8compiler5HintsENS1_13ZoneAllocatorIS3_EEEaSERKS6_, .-_ZNSt6vectorIN2v88internal8compiler5HintsENS1_13ZoneAllocatorIS3_EEEaSERKS6_
	.section	.rodata._ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment5MergeEPS3_.str1.8,"aMS",@progbits,1
	.align 8
.LC52:
	.string	"parameter_count() == other->parameter_count()"
	.align 8
.LC53:
	.string	"register_count() == other->register_count()"
	.section	.rodata._ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment5MergeEPS3_.str1.1,"aMS",@progbits,1
.LC54:
	.string	"!IsDead()"
	.section	.rodata._ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment5MergeEPS3_.str1.8
	.align 8
.LC55:
	.string	"ephemeral_hints_.size() == other->ephemeral_hints_.size()"
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment5MergeEPS3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment5MergeEPS3_
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment5MergeEPS3_, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment5MergeEPS3_:
.LFB18858:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	248(%rdi), %eax
	cmpl	%eax, 248(%rsi)
	jne	.L2643
	movl	252(%rdi), %eax
	movq	%rdi, %r13
	movq	%rsi, %r14
	cmpl	%eax, 252(%rsi)
	jne	.L2644
	movq	944(%rdi), %rax
	movq	936(%rdi), %rdx
	cmpq	%rax, %rdx
	je	.L2645
	movq	936(%rsi), %rsi
	movq	944(%r14), %rcx
	subq	%rdx, %rax
	xorl	%r12d, %r12d
	movabsq	$7905747460161236407, %rbx
	subq	%rsi, %rcx
	cmpq	%rcx, %rax
	je	.L2638
	jmp	.L2646
	.p2align 4,,10
	.p2align 3
.L2647:
	movq	936(%r14), %rsi
.L2638:
	leaq	0(,%r12,8), %rdi
	subq	%r12, %rdi
	addq	$1, %r12
	salq	$5, %rdi
	addq	%rdi, %rsi
	addq	%rdx, %rdi
	call	_ZN2v88internal8compiler5Hints3AddERKS2_
	movq	936(%r13), %rdx
	movq	944(%r13), %rax
	subq	%rdx, %rax
	sarq	$5, %rax
	imulq	%rbx, %rax
	cmpq	%rax, %r12
	jb	.L2647
	popq	%rbx
	leaq	704(%r14), %rsi
	leaq	704(%r13), %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler5Hints3AddERKS2_
	.p2align 4,,10
	.p2align 3
.L2645:
	.cfi_restore_state
	leaq	928(%rsi), %rsi
	leaq	928(%rdi), %rdi
	call	_ZNSt6vectorIN2v88internal8compiler5HintsENS1_13ZoneAllocatorIS3_EEEaSERKS6_
	movq	944(%r13), %rax
	cmpq	%rax, 936(%r13)
	je	.L2648
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2643:
	.cfi_restore_state
	leaq	.LC52(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2644:
	leaq	.LC53(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2646:
	leaq	.LC55(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2648:
	leaq	.LC54(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18858:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment5MergeEPS3_, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment5MergeEPS3_
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation32IncorporateJumpTargetEnvironmentEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation32IncorporateJumpTargetEnvironmentEi
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation32IncorporateJumpTargetEnvironmentEi, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation32IncorporateJumpTargetEnvironmentEi:
.LFB19046:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movl	%esi, %edi
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	call	_ZN2v84base10hash_valueEj@PLT
	movq	48(%r12), %rdi
	xorl	%edx, %edx
	movq	%rax, %rcx
	divq	%rdi
	movq	40(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L2649
	movq	(%rax), %rbx
	movq	%rdx, %r8
	movq	24(%rbx), %rsi
	jmp	.L2653
	.p2align 4,,10
	.p2align 3
.L2651:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L2649
	movq	24(%rbx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r8
	jne	.L2649
.L2653:
	cmpq	%rsi, %rcx
	jne	.L2651
	cmpl	%r13d, 8(%rbx)
	jne	.L2651
	movq	16(%rbx), %rsi
	movq	24(%r12), %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment5MergeEPS3_
	movq	48(%r12), %rcx
	movq	24(%rbx), %rax
	xorl	%edx, %edx
	movq	40(%r12), %r9
	divq	%rcx
	leaq	0(,%rdx,8), %r11
	movq	%rdx, %r8
	leaq	(%r9,%r11), %r10
	movq	(%r10), %rax
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L2654:
	movq	%rdx, %rsi
	movq	(%rdx), %rdx
	cmpq	%rdx, %rbx
	jne	.L2654
	movq	(%rbx), %rdi
	cmpq	%rsi, %rax
	je	.L2671
	testq	%rdi, %rdi
	je	.L2657
	movq	24(%rdi), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r8
	je	.L2657
	movq	%rsi, (%r9,%rdx,8)
	movq	(%rbx), %rdi
.L2657:
	movq	%rdi, (%rsi)
	subq	$1, 64(%r12)
.L2649:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2671:
	.cfi_restore_state
	testq	%rdi, %rdi
	je	.L2659
	movq	24(%rdi), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r8
	je	.L2657
	movq	%rsi, (%r9,%rdx,8)
	addq	40(%r12), %r11
	movq	(%r11), %rax
	movq	%r11, %r10
.L2656:
	leaq	56(%r12), %rdx
	cmpq	%rdx, %rax
	je	.L2672
.L2658:
	movq	$0, (%r10)
	movq	(%rbx), %rdi
	jmp	.L2657
	.p2align 4,,10
	.p2align 3
.L2659:
	movq	%rsi, %rax
	jmp	.L2656
	.p2align 4,,10
	.p2align 3
.L2672:
	movq	%rdi, 56(%r12)
	jmp	.L2658
	.cfi_endproc
.LFE19046:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation32IncorporateJumpTargetEnvironmentEi, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation32IncorporateJumpTargetEnvironmentEi
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation11EnvironmentC2EPNS0_4ZoneEPNS0_7IsolateENS1_18CompilationSubjectENS_4base8OptionalINS1_5HintsEEERKNS0_10ZoneVectorISB_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11EnvironmentC2EPNS0_4ZoneEPNS0_7IsolateENS1_18CompilationSubjectENS_4base8OptionalINS1_5HintsEEERKNS0_10ZoneVectorISB_EE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11EnvironmentC2EPNS0_4ZoneEPNS0_7IsolateENS1_18CompilationSubjectENS_4base8OptionalINS1_5HintsEEERKNS0_10ZoneVectorISB_EE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation11EnvironmentC2EPNS0_4ZoneEPNS0_7IsolateENS1_18CompilationSubjectENS_4base8OptionalINS1_5HintsEEERKNS0_10ZoneVectorISB_EE:
.LFB18856:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-288(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-304(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r9, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$392, %rsp
	movq	%r9, -400(%rbp)
	movq	%r8, -424(%rbp)
	movq	%rdi, -344(%rbp)
	movq	%r14, %rdi
	movq	%rsi, -392(%rbp)
	leaq	16(%rcx), %rsi
	movq	%rdx, -408(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rcx), %rax
	movq	%r14, -416(%rbp)
	movq	%rax, -304(%rbp)
	movq	8(%rcx), %rax
	movq	%rax, -296(%rbp)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movq	240(%rbx), %rax
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	-392(%rbp), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11EnvironmentC1EPNS0_4ZoneENS1_18CompilationSubjectE
	movq	%r14, %rdi
	movabsq	$7905747460161236407, %r14
	call	_ZN2v88internal8compiler5HintsD1Ev
	movq	16(%r12), %rax
	movq	8(%r12), %r9
	movq	%r14, -368(%rbp)
	movslq	248(%r15), %rsi
	movq	%rax, -360(%rbp)
	subq	%r9, %rax
	sarq	$5, %rax
	movq	%rsi, -352(%rbp)
	imulq	%r14, %rax
	cmpq	%rsi, %rax
	cmova	%rsi, %rax
	testq	%rax, %rax
	je	.L2715
	movq	%r13, -360(%rbp)
	movq	-400(%rbp), %r14
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L2674:
	leaq	0(,%rbx,8), %r8
	movq	-344(%rbp), %rax
	subq	%rbx, %r8
	salq	$5, %r8
	leaq	(%r9,%r8), %r12
	addq	936(%rax), %r8
	movq	%r8, %r15
	cmpq	%r8, %r12
	je	.L2677
	movq	24(%r8), %rax
	leaq	16(%r8), %rdx
	movq	%rax, -304(%rbp)
	movq	40(%r8), %rcx
	movq	%r8, -288(%rbp)
	movq	%rcx, -296(%rbp)
	testq	%rax, %rax
	je	.L2678
	movq	$0, 8(%rax)
	movq	16(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L2679
	movq	%rcx, -296(%rbp)
.L2679:
	movq	$0, 24(%r15)
	movq	%rdx, 32(%r15)
	movq	%rdx, 40(%r15)
	movq	$0, 48(%r15)
	movq	24(%r12), %rsi
	testq	%rsi, %rsi
	je	.L2906
.L2680:
	movq	-360(%rbp), %rcx
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal8compiler14VirtualContextES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE7_M_copyINSA_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS3_EPKSE_PSt18_Rb_tree_node_baseRT_
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L2681:
	movq	%rdx, %rcx
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L2681
	movq	%rcx, 32(%r15)
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L2682:
	movq	%rdx, %rcx
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L2682
	movq	%rcx, 40(%r15)
	movq	48(%r12), %rdx
	movq	%rax, 24(%r15)
	movq	%rdx, 48(%r15)
	movq	-304(%rbp), %rax
	movq	-288(%rbp), %rcx
	testq	%rax, %rax
	je	.L2677
	movq	%rbx, -376(%rbp)
	movq	%rcx, %rbx
	movq	%r12, -384(%rbp)
	movq	%rax, %r12
	.p2align 4,,10
	.p2align 3
.L2686:
	movq	24(%r12), %r13
	testq	%r13, %r13
	je	.L2684
.L2685:
	movq	24(%r13), %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal8compiler14VirtualContextES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE8_M_eraseEPSt13_Rb_tree_nodeIS3_E
	movq	16(%r13), %r13
	testq	%r13, %r13
	jne	.L2685
.L2684:
	movq	16(%r12), %r12
	testq	%r12, %r12
	jne	.L2686
	movq	-376(%rbp), %rbx
	movq	-384(%rbp), %r12
.L2677:
	leaq	56(%r15), %rdi
	leaq	56(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2687
	movq	80(%r15), %rax
	leaq	72(%r15), %rdx
	movq	%rax, -304(%rbp)
	movq	96(%r15), %rcx
	movq	%rdi, -288(%rbp)
	movq	%rcx, -296(%rbp)
	testq	%rax, %rax
	je	.L2688
	movq	$0, 8(%rax)
	movq	16(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L2689
	movq	%rcx, -296(%rbp)
.L2689:
	movq	$0, 80(%r15)
	movq	%rdx, 88(%r15)
	movq	%rdx, 96(%r15)
	movq	$0, 104(%r15)
	movq	80(%r12), %rsi
	testq	%rsi, %rsi
	je	.L2907
.L2690:
	movq	-360(%rbp), %rcx
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE7_M_copyINSC_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS4_EPKSG_PSt18_Rb_tree_node_baseRT_
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L2691:
	movq	%rdx, %rcx
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L2691
	movq	%rcx, 88(%r15)
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L2692:
	movq	%rdx, %rcx
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L2692
	movq	%rcx, 96(%r15)
	movq	104(%r12), %rdx
	movq	%rax, 80(%r15)
	movq	%rdx, 104(%r15)
	movq	-304(%rbp), %rax
	movq	-288(%rbp), %rcx
	testq	%rax, %rax
	je	.L2687
	movq	%rbx, -376(%rbp)
	movq	%rcx, %rbx
	movq	%r12, -384(%rbp)
	movq	%rax, %r12
	.p2align 4,,10
	.p2align 3
.L2696:
	movq	24(%r12), %r13
	testq	%r13, %r13
	je	.L2694
.L2695:
	movq	24(%r13), %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	movq	16(%r13), %r13
	testq	%r13, %r13
	jne	.L2695
.L2694:
	movq	16(%r12), %r12
	testq	%r12, %r12
	jne	.L2696
	movq	-376(%rbp), %rbx
	movq	-384(%rbp), %r12
.L2687:
	leaq	112(%r15), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2697
	movq	136(%r15), %rax
	leaq	128(%r15), %rdx
	movq	%rax, -304(%rbp)
	movq	152(%r15), %rcx
	movq	%rdi, -288(%rbp)
	movq	%rcx, -296(%rbp)
	testq	%rax, %rax
	je	.L2698
	movq	$0, 8(%rax)
	movq	16(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L2699
	movq	%rcx, -296(%rbp)
.L2699:
	movq	$0, 136(%r15)
	movq	%rdx, 144(%r15)
	movq	%rdx, 152(%r15)
	movq	$0, 160(%r15)
	movq	136(%r12), %rsi
	testq	%rsi, %rsi
	je	.L2908
.L2700:
	movq	-360(%rbp), %rcx
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_3MapEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE7_M_copyINSC_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS4_EPKSG_PSt18_Rb_tree_node_baseRT_
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L2701:
	movq	%rdx, %rcx
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L2701
	movq	%rcx, 144(%r15)
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L2702:
	movq	%rdx, %rcx
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L2702
	movq	%rcx, 152(%r15)
	movq	160(%r12), %rdx
	movq	%rax, 136(%r15)
	movq	%rdx, 160(%r15)
	movq	-304(%rbp), %rax
	movq	-288(%rbp), %rcx
	testq	%rax, %rax
	je	.L2697
	movq	%rbx, -376(%rbp)
	movq	%rcx, %rbx
	movq	%r12, -384(%rbp)
	movq	%rax, %r12
	.p2align 4,,10
	.p2align 3
.L2706:
	movq	24(%r12), %r13
	testq	%r13, %r13
	je	.L2704
.L2705:
	movq	24(%r13), %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_3MapEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	movq	16(%r13), %r13
	testq	%r13, %r13
	jne	.L2705
.L2704:
	movq	16(%r12), %r12
	testq	%r12, %r12
	jne	.L2706
	movq	-376(%rbp), %rbx
	movq	-384(%rbp), %r12
.L2697:
	leaq	168(%r15), %rdi
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2707
	movq	192(%r15), %r13
	leaq	184(%r15), %rdx
	movq	%r13, -304(%rbp)
	movq	208(%r15), %rcx
	movq	%rdi, -288(%rbp)
	movq	%rcx, -296(%rbp)
	testq	%r13, %r13
	je	.L2708
	movq	$0, 8(%r13)
	movq	16(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L2709
	movq	%rcx, -296(%rbp)
.L2709:
	movq	$0, 192(%r15)
	movq	%rdx, 200(%r15)
	movq	%rdx, 208(%r15)
	movq	$0, 216(%r15)
	movq	192(%r12), %rsi
	testq	%rsi, %rsi
	je	.L2909
.L2710:
	movq	-360(%rbp), %rcx
	call	_ZNSt8_Rb_treeIN2v88internal8compiler17FunctionBlueprintES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE7_M_copyINSA_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS3_EPKSE_PSt18_Rb_tree_node_baseRT_
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L2711:
	movq	%rdx, %rcx
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L2711
	movq	%rcx, 200(%r15)
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L2712:
	movq	%rdx, %rcx
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L2712
	movq	%rcx, 208(%r15)
	movq	216(%r12), %rdx
	movq	%rax, 192(%r15)
	movq	%rdx, 216(%r15)
	movq	-304(%rbp), %r13
	movq	-288(%rbp), %r12
	testq	%r13, %r13
	je	.L2707
	.p2align 4,,10
	.p2align 3
.L2714:
	movq	24(%r13), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal8compiler17FunctionBlueprintES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE8_M_eraseEPSt13_Rb_tree_nodeIS3_E
	movq	%r13, %rdi
	movq	16(%r13), %r13
	addq	$48, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	testq	%r13, %r13
	jne	.L2714
.L2707:
	movq	8(%r14), %r9
	movq	16(%r14), %rax
	addq	$1, %rbx
	movq	-352(%rbp), %rdi
	subq	%r9, %rax
	sarq	$5, %rax
	imulq	-368(%rbp), %rax
	cmpq	%rdi, %rax
	cmova	%rdi, %rax
	cmpq	%rax, %rbx
	jb	.L2674
	movq	-360(%rbp), %r13
.L2715:
	movq	-392(%rbp), %rsi
	movq	-416(%rbp), %rdi
	leaq	-232(%rbp), %rax
	leaq	-336(%rbp), %r14
	movq	-408(%rbp), %rdx
	movq	%rax, -216(%rbp)
	leaq	-248(%rbp), %r15
	movq	%rax, -208(%rbp)
	leaq	-176(%rbp), %rax
	movq	%rsi, -304(%rbp)
	addq	$88, %rdx
	movq	%rdi, -272(%rbp)
	movq	%rdi, -264(%rbp)
	movq	%r15, %rdi
	movq	%rsi, -248(%rbp)
	movq	%rsi, -192(%rbp)
	movq	%rax, -160(%rbp)
	movq	%rax, -152(%rbp)
	leaq	-120(%rbp), %rax
	movq	%rsi, -136(%rbp)
	movq	%r14, %rsi
	movl	$0, -288(%rbp)
	movq	$0, -280(%rbp)
	movq	$0, -256(%rbp)
	movl	$0, -232(%rbp)
	movq	$0, -224(%rbp)
	movq	$0, -200(%rbp)
	movl	$0, -176(%rbp)
	movq	$0, -168(%rbp)
	movq	$0, -144(%rbp)
	movl	$0, -120(%rbp)
	movq	$0, -112(%rbp)
	movq	%rax, -104(%rbp)
	movq	%rax, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rdx, -336(%rbp)
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_
	movq	-400(%rbp), %rax
	movq	16(%rax), %rbx
	subq	8(%rax), %rbx
	movabsq	$7905747460161236407, %rax
	movq	%rbx, %r12
	sarq	$5, %r12
	imulq	%rax, %r12
	movq	%r12, %rdi
	cmpq	%r12, -352(%rbp)
	jbe	.L2675
	leaq	-192(%rbp), %rax
	movq	%r15, -384(%rbp)
	movq	%rbx, %r12
	movq	%rdi, %rbx
	movq	%rax, -368(%rbp)
	leaq	-136(%rbp), %rax
	movq	%rax, -376(%rbp)
	movq	%r13, -360(%rbp)
	.p2align 4,,10
	.p2align 3
.L2756:
	movq	-344(%rbp), %rax
	movq	936(%rax), %r15
	addq	%r12, %r15
	cmpq	-360(%rbp), %r15
	je	.L2718
	movq	24(%r15), %r13
	leaq	16(%r15), %rdx
	movq	%r13, -336(%rbp)
	movq	40(%r15), %rcx
	movq	%r15, -320(%rbp)
	movq	%rcx, -328(%rbp)
	testq	%r13, %r13
	je	.L2719
	movq	$0, 8(%r13)
	movq	16(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L2720
	movq	%rcx, -328(%rbp)
.L2720:
	movq	$0, 24(%r15)
	movq	%rdx, 32(%r15)
	movq	%rdx, 40(%r15)
	movq	$0, 48(%r15)
	movq	-280(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L2910
.L2721:
	movq	%r14, %rcx
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal8compiler14VirtualContextES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE7_M_copyINSA_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS3_EPKSE_PSt18_Rb_tree_node_baseRT_
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L2722:
	movq	%rdx, %rcx
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L2722
	movq	%rcx, 32(%r15)
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L2723:
	movq	%rdx, %rcx
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L2723
	movq	%rcx, 40(%r15)
	movq	-256(%rbp), %rdx
	movq	%rax, 24(%r15)
	movq	%rdx, 48(%r15)
	movq	-336(%rbp), %r13
	movq	-320(%rbp), %rcx
	testq	%r13, %r13
	je	.L2718
	movq	%rbx, -392(%rbp)
	movq	%rcx, %rbx
	movq	%r12, -400(%rbp)
	movq	%r13, %r12
	.p2align 4,,10
	.p2align 3
.L2727:
	movq	24(%r12), %r13
	testq	%r13, %r13
	je	.L2725
.L2726:
	movq	24(%r13), %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal8compiler14VirtualContextES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE8_M_eraseEPSt13_Rb_tree_nodeIS3_E
	movq	16(%r13), %r13
	testq	%r13, %r13
	jne	.L2726
.L2725:
	movq	16(%r12), %r12
	testq	%r12, %r12
	jne	.L2727
	movq	-392(%rbp), %rbx
	movq	-400(%rbp), %r12
.L2718:
	leaq	56(%r15), %rdi
	cmpq	-384(%rbp), %rdi
	je	.L2728
	movq	80(%r15), %r13
	leaq	72(%r15), %rdx
	movq	%r13, -336(%rbp)
	movq	96(%r15), %rcx
	movq	%rdi, -320(%rbp)
	movq	%rcx, -328(%rbp)
	testq	%r13, %r13
	je	.L2729
	movq	$0, 8(%r13)
	movq	16(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L2730
	movq	%rcx, -328(%rbp)
.L2730:
	movq	$0, 80(%r15)
	movq	%rdx, 88(%r15)
	movq	%rdx, 96(%r15)
	movq	$0, 104(%r15)
	movq	-224(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L2911
.L2731:
	movq	%r14, %rcx
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE7_M_copyINSC_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS4_EPKSG_PSt18_Rb_tree_node_baseRT_
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L2732:
	movq	%rdx, %rcx
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L2732
	movq	%rcx, 88(%r15)
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L2733:
	movq	%rdx, %rcx
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L2733
	movq	%rcx, 96(%r15)
	movq	-200(%rbp), %rdx
	movq	%rax, 80(%r15)
	movq	%rdx, 104(%r15)
	movq	-336(%rbp), %r13
	movq	-320(%rbp), %rcx
	testq	%r13, %r13
	je	.L2728
	movq	%rbx, -392(%rbp)
	movq	%rcx, %rbx
	movq	%r12, -400(%rbp)
	movq	%r13, %r12
	.p2align 4,,10
	.p2align 3
.L2737:
	movq	24(%r12), %r13
	testq	%r13, %r13
	je	.L2735
.L2736:
	movq	24(%r13), %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	movq	16(%r13), %r13
	testq	%r13, %r13
	jne	.L2736
.L2735:
	movq	16(%r12), %r12
	testq	%r12, %r12
	jne	.L2737
	movq	-392(%rbp), %rbx
	movq	-400(%rbp), %r12
.L2728:
	leaq	112(%r15), %rdi
	cmpq	-368(%rbp), %rdi
	je	.L2738
	movq	136(%r15), %r13
	leaq	128(%r15), %rdx
	movq	%r13, -336(%rbp)
	movq	152(%r15), %rcx
	movq	%rdi, -320(%rbp)
	movq	%rcx, -328(%rbp)
	testq	%r13, %r13
	je	.L2739
	movq	$0, 8(%r13)
	movq	16(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L2740
	movq	%rcx, -328(%rbp)
.L2740:
	movq	$0, 136(%r15)
	movq	%rdx, 144(%r15)
	movq	%rdx, 152(%r15)
	movq	$0, 160(%r15)
	movq	-168(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L2912
.L2741:
	movq	%r14, %rcx
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_3MapEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE7_M_copyINSC_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS4_EPKSG_PSt18_Rb_tree_node_baseRT_
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L2742:
	movq	%rdx, %rcx
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L2742
	movq	%rcx, 144(%r15)
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L2743:
	movq	%rdx, %rcx
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L2743
	movq	%rcx, 152(%r15)
	movq	-144(%rbp), %rdx
	movq	%rax, 136(%r15)
	movq	%rdx, 160(%r15)
	movq	-336(%rbp), %r13
	movq	-320(%rbp), %rcx
	testq	%r13, %r13
	je	.L2738
	movq	%rbx, -392(%rbp)
	movq	%rcx, %rbx
	movq	%r12, -400(%rbp)
	movq	%r13, %r12
	.p2align 4,,10
	.p2align 3
.L2747:
	movq	24(%r12), %r13
	testq	%r13, %r13
	je	.L2745
.L2746:
	movq	24(%r13), %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_3MapEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	movq	16(%r13), %r13
	testq	%r13, %r13
	jne	.L2746
.L2745:
	movq	16(%r12), %r12
	testq	%r12, %r12
	jne	.L2747
	movq	-392(%rbp), %rbx
	movq	-400(%rbp), %r12
.L2738:
	leaq	168(%r15), %rdi
	cmpq	-376(%rbp), %rdi
	je	.L2748
	movq	192(%r15), %r13
	leaq	184(%r15), %rdx
	movq	%r13, -336(%rbp)
	movq	208(%r15), %rcx
	movq	%rdi, -320(%rbp)
	movq	%rcx, -328(%rbp)
	testq	%r13, %r13
	je	.L2749
	movq	$0, 8(%r13)
	movq	16(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L2750
	movq	%rcx, -328(%rbp)
.L2750:
	movq	$0, 192(%r15)
	movq	%rdx, 200(%r15)
	movq	%rdx, 208(%r15)
	movq	$0, 216(%r15)
	movq	-112(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L2913
.L2751:
	movq	%r14, %rcx
	call	_ZNSt8_Rb_treeIN2v88internal8compiler17FunctionBlueprintES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE7_M_copyINSA_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS3_EPKSE_PSt18_Rb_tree_node_baseRT_
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L2752:
	movq	%rdx, %rcx
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L2752
	movq	%rcx, 200(%r15)
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L2753:
	movq	%rdx, %rcx
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L2753
	movq	%rcx, 208(%r15)
	movq	-88(%rbp), %rdx
	movq	%rax, 192(%r15)
	movq	%rdx, 216(%r15)
	movq	-336(%rbp), %r13
	movq	-320(%rbp), %r15
	testq	%r13, %r13
	je	.L2748
	.p2align 4,,10
	.p2align 3
.L2755:
	movq	24(%r13), %rsi
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal8compiler17FunctionBlueprintES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE8_M_eraseEPSt13_Rb_tree_nodeIS3_E
	movq	%r13, %rdi
	movq	16(%r13), %r13
	addq	$48, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	testq	%r13, %r13
	jne	.L2755
.L2748:
	addq	$1, %rbx
	addq	$224, %r12
	cmpq	%rbx, -352(%rbp)
	jne	.L2756
	movq	-360(%rbp), %r13
.L2675:
	movq	-344(%rbp), %rax
	movq	%r14, %rdi
	movq	8(%rax), %rax
	movq	(%rax), %rax
	movq	%rax, -336(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo16GetBytecodeArrayEv
	movl	47(%rax), %eax
	testl	%eax, %eax
	je	.L2757
	movq	-424(%rbp), %rsi
	cmpb	$0, (%rsi)
	jne	.L2914
.L2757:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2915
	addq	$392, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2913:
	.cfi_restore_state
	movq	-320(%rbp), %r15
	jmp	.L2755
	.p2align 4,,10
	.p2align 3
.L2912:
	movq	-320(%rbp), %rcx
	movq	%rbx, -392(%rbp)
	movq	%r12, -400(%rbp)
	movq	%r13, %r12
	movq	%rcx, %rbx
	jmp	.L2747
	.p2align 4,,10
	.p2align 3
.L2911:
	movq	-320(%rbp), %rcx
	movq	%rbx, -392(%rbp)
	movq	%r12, -400(%rbp)
	movq	%r13, %r12
	movq	%rcx, %rbx
	jmp	.L2737
	.p2align 4,,10
	.p2align 3
.L2910:
	movq	-320(%rbp), %rcx
	movq	%rbx, -392(%rbp)
	movq	%r12, -400(%rbp)
	movq	%r13, %r12
	movq	%rcx, %rbx
	jmp	.L2727
	.p2align 4,,10
	.p2align 3
.L2909:
	movq	-288(%rbp), %r12
	jmp	.L2714
	.p2align 4,,10
	.p2align 3
.L2908:
	movq	-288(%rbp), %rcx
	movq	%rbx, -376(%rbp)
	movq	%r12, -384(%rbp)
	movq	%rax, %r12
	movq	%rcx, %rbx
	jmp	.L2706
	.p2align 4,,10
	.p2align 3
.L2907:
	movq	-288(%rbp), %rcx
	movq	%rbx, -376(%rbp)
	movq	%r12, -384(%rbp)
	movq	%rax, %r12
	movq	%rcx, %rbx
	jmp	.L2696
	.p2align 4,,10
	.p2align 3
.L2906:
	movq	-288(%rbp), %rcx
	movq	%rbx, -376(%rbp)
	movq	%r12, -384(%rbp)
	movq	%rax, %r12
	movq	%rcx, %rbx
	jmp	.L2686
	.p2align 4,,10
	.p2align 3
.L2688:
	movq	$0, -296(%rbp)
	movq	$0, 80(%r15)
	movq	%rdx, 88(%r15)
	movq	%rdx, 96(%r15)
	movq	$0, 104(%r15)
	movq	80(%r12), %rsi
	testq	%rsi, %rsi
	jne	.L2690
	jmp	.L2687
	.p2align 4,,10
	.p2align 3
.L2678:
	movq	$0, -296(%rbp)
	movq	$0, 24(%r8)
	movq	%rdx, 32(%r8)
	movq	%rdx, 40(%r8)
	movq	$0, 48(%r8)
	movq	24(%r12), %rsi
	testq	%rsi, %rsi
	jne	.L2680
	jmp	.L2677
	.p2align 4,,10
	.p2align 3
.L2708:
	movq	$0, -296(%rbp)
	movq	$0, 192(%r15)
	movq	%rdx, 200(%r15)
	movq	%rdx, 208(%r15)
	movq	$0, 216(%r15)
	movq	192(%r12), %rsi
	testq	%rsi, %rsi
	jne	.L2710
	jmp	.L2707
	.p2align 4,,10
	.p2align 3
.L2698:
	movq	$0, -296(%rbp)
	movq	$0, 136(%r15)
	movq	%rdx, 144(%r15)
	movq	%rdx, 152(%r15)
	movq	$0, 160(%r15)
	movq	136(%r12), %rsi
	testq	%rsi, %rsi
	jne	.L2700
	jmp	.L2697
	.p2align 4,,10
	.p2align 3
.L2729:
	movq	$0, -328(%rbp)
	movq	$0, 80(%r15)
	movq	%rdx, 88(%r15)
	movq	%rdx, 96(%r15)
	movq	$0, 104(%r15)
	movq	-224(%rbp), %rsi
	testq	%rsi, %rsi
	jne	.L2731
	jmp	.L2728
	.p2align 4,,10
	.p2align 3
.L2719:
	movq	$0, -328(%rbp)
	movq	$0, 24(%r15)
	movq	%rdx, 32(%r15)
	movq	%rdx, 40(%r15)
	movq	$0, 48(%r15)
	movq	-280(%rbp), %rsi
	testq	%rsi, %rsi
	jne	.L2721
	jmp	.L2718
	.p2align 4,,10
	.p2align 3
.L2749:
	movq	$0, -328(%rbp)
	movq	$0, 192(%r15)
	movq	%rdx, 200(%r15)
	movq	%rdx, 208(%r15)
	movq	$0, 216(%r15)
	movq	-112(%rbp), %rsi
	testq	%rsi, %rsi
	jne	.L2751
	jmp	.L2748
	.p2align 4,,10
	.p2align 3
.L2739:
	movq	$0, -328(%rbp)
	movq	$0, 136(%r15)
	movq	%rdx, 144(%r15)
	movq	%rdx, 152(%r15)
	movq	$0, 160(%r15)
	movq	-168(%rbp), %rsi
	testq	%rsi, %rsi
	jne	.L2741
	jmp	.L2738
	.p2align 4,,10
	.p2align 3
.L2914:
	movq	-344(%rbp), %rdi
	movl	$-5, %esi
	subl	%eax, %esi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment14register_hintsENS0_11interpreter8RegisterE
	movq	-424(%rbp), %rsi
	movq	%rax, %rdi
	addq	$8, %rsi
	call	_ZN2v88internal8compiler5Hints3AddERKS2_
	jmp	.L2757
.L2915:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18856:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11EnvironmentC2EPNS0_4ZoneEPNS0_7IsolateENS1_18CompilationSubjectENS_4base8OptionalINS1_5HintsEEERKNS0_10ZoneVectorISB_EE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation11EnvironmentC2EPNS0_4ZoneEPNS0_7IsolateENS1_18CompilationSubjectENS_4base8OptionalINS1_5HintsEEERKNS0_10ZoneVectorISB_EE
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11EnvironmentC1EPNS0_4ZoneEPNS0_7IsolateENS1_18CompilationSubjectENS_4base8OptionalINS1_5HintsEEERKNS0_10ZoneVectorISB_EE
	.set	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11EnvironmentC1EPNS0_4ZoneEPNS0_7IsolateENS1_18CompilationSubjectENS_4base8OptionalINS1_5HintsEEERKNS0_10ZoneVectorISB_EE,_ZN2v88internal8compiler34SerializerForBackgroundCompilation11EnvironmentC2EPNS0_4ZoneEPNS0_7IsolateENS1_18CompilationSubjectENS_4base8OptionalINS1_5HintsEEERKNS0_10ZoneVectorISB_EE
	.section	.rodata._ZN2v88internal8compiler34SerializerForBackgroundCompilationC2EPNS1_12JSHeapBrokerEPNS1_23CompilationDependenciesEPNS0_4ZoneENS1_18CompilationSubjectENS_4base8OptionalINS1_5HintsEEERKNS0_10ZoneVectorISC_EENSA_5FlagsINS1_38SerializerForBackgroundCompilationFlagEiEE.str1.1,"aMS",@progbits,1
.LC56:
	.string	"Running "
	.section	.rodata._ZN2v88internal8compiler34SerializerForBackgroundCompilationC2EPNS1_12JSHeapBrokerEPNS1_23CompilationDependenciesEPNS0_4ZoneENS1_18CompilationSubjectENS_4base8OptionalINS1_5HintsEEERKNS0_10ZoneVectorISC_EENSA_5FlagsINS1_38SerializerForBackgroundCompilationFlagEiEE.str1.8,"aMS",@progbits,1
	.align 8
.LC57:
	.string	"SerializerForBackgroundCompilation::SerializerForBackgroundCompilation"
	.section	.rodata._ZN2v88internal8compiler34SerializerForBackgroundCompilationC2EPNS1_12JSHeapBrokerEPNS1_23CompilationDependenciesEPNS0_4ZoneENS1_18CompilationSubjectENS_4base8OptionalINS1_5HintsEEERKNS0_10ZoneVectorISC_EENSA_5FlagsINS1_38SerializerForBackgroundCompilationFlagEiEE.str1.1
.LC58:
	.string	" on "
.LC59:
	.string	"Initial environment:\n"
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilationC2EPNS1_12JSHeapBrokerEPNS1_23CompilationDependenciesEPNS0_4ZoneENS1_18CompilationSubjectENS_4base8OptionalINS1_5HintsEEERKNS0_10ZoneVectorISC_EENSA_5FlagsINS1_38SerializerForBackgroundCompilationFlagEiEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilationC2EPNS1_12JSHeapBrokerEPNS1_23CompilationDependenciesEPNS0_4ZoneENS1_18CompilationSubjectENS_4base8OptionalINS1_5HintsEEERKNS0_10ZoneVectorISC_EENSA_5FlagsINS1_38SerializerForBackgroundCompilationFlagEiEE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilationC2EPNS1_12JSHeapBrokerEPNS1_23CompilationDependenciesEPNS0_4ZoneENS1_18CompilationSubjectENS_4base8OptionalINS1_5HintsEEERKNS0_10ZoneVectorISC_EENSA_5FlagsINS1_38SerializerForBackgroundCompilationFlagEiEE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilationC2EPNS1_12JSHeapBrokerEPNS1_23CompilationDependenciesEPNS0_4ZoneENS1_18CompilationSubjectENS_4base8OptionalINS1_5HintsEEERKNS0_10ZoneVectorISC_EENSA_5FlagsINS1_38SerializerForBackgroundCompilationFlagEiEE:
.LFB18887:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r9, %r14
	leaq	-288(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$536, %rsp
	movq	16(%rbp), %r9
	movq	(%rsi), %rdx
	movq	%rsi, -568(%rbp)
	leaq	16(%r8), %rsi
	movq	%r9, -560(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%r8), %rax
	movq	%rcx, 16(%rdi)
	movups	%xmm0, (%rdi)
	movq	%r15, %rdi
	movq	%rax, -304(%rbp)
	movq	8(%r8), %rax
	movq	%rdx, -552(%rbp)
	movq	%rax, -296(%rbp)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	cmpb	$0, (%r14)
	movq	240(%r12), %rax
	movb	$0, -544(%rbp)
	movb	$0, -536(%rbp)
	movq	-552(%rbp), %rdx
	movq	%rax, -64(%rbp)
	movq	-560(%rbp), %r9
	jne	.L2934
.L2917:
	movq	16(%r13), %rax
	movq	24(%r13), %rcx
	subq	%rax, %rcx
	cmpq	$959, %rcx
	jbe	.L2935
	leaq	960(%rax), %rcx
	movq	%rcx, 16(%r13)
.L2919:
	leaq	-304(%rbp), %r14
	movq	%rax, %rdi
	leaq	-544(%rbp), %r8
	movq	%r13, %rsi
	movq	%r14, %rcx
	movq	%rax, -552(%rbp)
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11EnvironmentC1EPNS0_4ZoneEPNS0_7IsolateENS1_18CompilationSubjectENS_4base8OptionalINS1_5HintsEEERKNS0_10ZoneVectorISB_EE
	movq	-552(%rbp), %rax
	cmpb	$0, -544(%rbp)
	movq	%rax, 24(%rbx)
	jne	.L2936
.L2920:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	leaq	88(%rbx), %rcx
	movq	%r13, 32(%rbx)
	leaq	72(%rbx), %rdi
	movq	%rcx, 40(%rbx)
	movl	$100, %esi
	movq	$1, 48(%rbx)
	movq	$0, 56(%rbx)
	movq	$0, 64(%rbx)
	movl	$0x3f800000, 72(%rbx)
	movq	$0, 80(%rbx)
	movq	$0, 88(%rbx)
	movq	%rcx, -552(%rbp)
	call	_ZNKSt8__detail20_Prime_rehash_policy11_M_next_bktEm@PLT
	movq	%rax, %r15
	cmpq	48(%rbx), %rax
	jbe	.L2921
	cmpq	$1, %rax
	movq	-552(%rbp), %rcx
	je	.L2937
	movq	32(%rbx), %rdi
	leaq	0(,%rax,8), %rdx
	movq	%rdx, %rsi
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	subq	%rcx, %rax
	cmpq	%rax, %rdx
	ja	.L2938
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
.L2925:
	movq	%rcx, %rdi
	xorl	%esi, %esi
	call	memset@PLT
	movq	%rax, %rcx
.L2923:
	movq	%rcx, 40(%rbx)
	movq	%r15, 48(%rbx)
.L2921:
	movl	24(%rbp), %eax
	movq	(%rbx), %r15
	movl	$-1, 100(%rbx)
	movl	%eax, 96(%rbx)
	cmpb	$0, 124(%r15)
	je	.L2926
	cmpb	$0, _ZN2v88internal30FLAG_trace_heap_broker_verboseE(%rip)
	jne	.L2939
.L2926:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler12JSHeapBroker27IncrementTracingIndentationEv@PLT
	movq	(%rbx), %rdi
	cmpb	$0, 124(%rdi)
	je	.L2927
	cmpb	$0, _ZN2v88internal30FLAG_trace_heap_broker_verboseE(%rip)
	jne	.L2940
.L2927:
	movq	240(%r12), %rdx
	testq	%rdx, %rdx
	je	.L2928
	movq	-568(%rbp), %rsi
	movq	%r14, %rdi
	movl	$1, %ecx
	call	_ZN2v88internal8compiler13JSFunctionRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13JSFunctionRef9SerializeEv@PLT
.L2928:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler12JSHeapBroker27DecrementTracingIndentationEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2941
	addq	$536, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2936:
	.cfi_restore_state
	leaq	-536(%rbp), %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	jmp	.L2920
	.p2align 4,,10
	.p2align 3
.L2934:
	leaq	8(%r14), %rsi
	leaq	-536(%rbp), %rdi
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movb	$1, -544(%rbp)
	movq	-560(%rbp), %r9
	movq	-552(%rbp), %rdx
	jmp	.L2917
	.p2align 4,,10
	.p2align 3
.L2940:
	call	_ZN2v88internal8compiler12JSHeapBroker5TraceEv@PLT
	movl	$21, %edx
	leaq	.LC59(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, -552(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	24(%rbx), %rsi
	movq	-552(%rbp), %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_34SerializerForBackgroundCompilation11EnvironmentE
	movl	$1, %edx
	movq	%r14, %rsi
	movb	$10, -304(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L2927
	.p2align 4,,10
	.p2align 3
.L2939:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler12JSHeapBroker5TraceEv@PLT
	movl	$8, %edx
	leaq	.LC56(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, -552(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-552(%rbp), %rdi
	movl	$70, %edx
	leaq	.LC57(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-552(%rbp), %rdi
	movl	$4, %edx
	leaq	.LC58(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-552(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZNSo9_M_insertIPKvEERSoT_@PLT
	movl	$1, %edx
	movq	%r14, %rsi
	movb	$10, -304(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L2926
	.p2align 4,,10
	.p2align 3
.L2937:
	movq	$0, 88(%rbx)
	jmp	.L2923
	.p2align 4,,10
	.p2align 3
.L2935:
	movl	$960, %esi
	movq	%r13, %rdi
	movq	%r9, -560(%rbp)
	movq	%rdx, -552(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-552(%rbp), %rdx
	movq	-560(%rbp), %r9
	jmp	.L2919
	.p2align 4,,10
	.p2align 3
.L2938:
	movq	%rdx, -552(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-552(%rbp), %rdx
	movq	%rax, %rcx
	jmp	.L2925
.L2941:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18887:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilationC2EPNS1_12JSHeapBrokerEPNS1_23CompilationDependenciesEPNS0_4ZoneENS1_18CompilationSubjectENS_4base8OptionalINS1_5HintsEEERKNS0_10ZoneVectorISC_EENSA_5FlagsINS1_38SerializerForBackgroundCompilationFlagEiEE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilationC2EPNS1_12JSHeapBrokerEPNS1_23CompilationDependenciesEPNS0_4ZoneENS1_18CompilationSubjectENS_4base8OptionalINS1_5HintsEEERKNS0_10ZoneVectorISC_EENSA_5FlagsINS1_38SerializerForBackgroundCompilationFlagEiEE
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilationC1EPNS1_12JSHeapBrokerEPNS1_23CompilationDependenciesEPNS0_4ZoneENS1_18CompilationSubjectENS_4base8OptionalINS1_5HintsEEERKNS0_10ZoneVectorISC_EENSA_5FlagsINS1_38SerializerForBackgroundCompilationFlagEiEE
	.set	_ZN2v88internal8compiler34SerializerForBackgroundCompilationC1EPNS1_12JSHeapBrokerEPNS1_23CompilationDependenciesEPNS0_4ZoneENS1_18CompilationSubjectENS_4base8OptionalINS1_5HintsEEERKNS0_10ZoneVectorISC_EENSA_5FlagsINS1_38SerializerForBackgroundCompilationFlagEiEE,_ZN2v88internal8compiler34SerializerForBackgroundCompilationC2EPNS1_12JSHeapBrokerEPNS1_23CompilationDependenciesEPNS0_4ZoneENS1_18CompilationSubjectENS_4base8OptionalINS1_5HintsEEERKNS0_10ZoneVectorISC_EENSA_5FlagsINS1_38SerializerForBackgroundCompilationFlagEiEE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation33ContributeToJumpTargetEnvironmentEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation33ContributeToJumpTargetEnvironmentEi
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation33ContributeToJumpTargetEnvironmentEi, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation33ContributeToJumpTargetEnvironmentEi:
.LFB19042:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movl	%esi, %edi
	subq	$40, %rsp
	call	_ZN2v84base10hash_valueEj@PLT
	movq	48(%rbx), %rdi
	xorl	%edx, %edx
	movq	%rax, %rsi
	divq	%rdi
	movq	40(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L2943
	movq	(%rax), %rcx
	movq	%rdx, %r9
	movq	24(%rcx), %r8
	jmp	.L2949
	.p2align 4,,10
	.p2align 3
.L2944:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L2943
	movq	24(%rcx), %r8
	xorl	%edx, %edx
	movq	%r8, %rax
	divq	%rdi
	cmpq	%rdx, %r9
	jne	.L2943
.L2949:
	cmpq	%rsi, %r8
	jne	.L2944
	cmpl	%r12d, 8(%rcx)
	jne	.L2944
	movq	24(%rbx), %rsi
	movq	16(%rcx), %rdi
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment5MergeEPS3_
	.p2align 4,,10
	.p2align 3
.L2943:
	.cfi_restore_state
	movq	16(%rbx), %rdi
	movq	24(%rbx), %r15
	movq	16(%rdi), %r14
	movq	24(%rdi), %rax
	subq	%r14, %rax
	cmpq	$959, %rax
	jbe	.L2978
	leaq	960(%r14), %rax
	movq	%rax, 16(%rdi)
.L2950:
	movq	(%r15), %rax
	leaq	24(%r15), %rsi
	leaq	24(%r14), %rdi
	movq	%rax, (%r14)
	movq	8(%r15), %rax
	movq	%rax, 8(%r14)
	movq	16(%r15), %rax
	movq	%rax, 16(%r14)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movq	248(%r15), %rax
	leaq	256(%r15), %rsi
	leaq	256(%r14), %rdi
	movq	%rax, 248(%r14)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	leaq	480(%r15), %rsi
	leaq	480(%r14), %rdi
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	leaq	704(%r14), %rdi
	leaq	704(%r15), %rsi
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movq	928(%r15), %rdi
	movq	944(%r15), %r13
	subq	936(%r15), %r13
	movq	$0, 936(%r14)
	movq	%rdi, 928(%r14)
	movq	$0, 944(%r14)
	movq	$0, 952(%r14)
	je	.L2963
	movq	16(%rdi), %rdx
	movq	24(%rdi), %rax
	leaq	7(%r13), %rsi
	andq	$-8, %rsi
	subq	%rdx, %rax
	cmpq	%rax, %rsi
	ja	.L2979
	addq	%rdx, %rsi
	movq	%rsi, 16(%rdi)
.L2953:
	movq	%rdx, -64(%rbp)
.L2951:
	movq	-64(%rbp), %rax
	movq	%rax, %xmm0
	addq	%rax, %r13
	punpcklqdq	%xmm0, %xmm0
	movq	%r13, 952(%r14)
	movups	%xmm0, 936(%r14)
	movq	944(%r15), %rcx
	movq	936(%r15), %rdx
	movq	%rcx, -56(%rbp)
	movq	%rdx, -72(%rbp)
	cmpq	%rdx, %rcx
	je	.L2954
	movq	%rdx, %r13
	movq	%rax, %r15
	.p2align 4,,10
	.p2align 3
.L2955:
	movq	%r13, %rsi
	movq	%r15, %rdi
	addq	$224, %r13
	addq	$224, %r15
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	cmpq	%r13, -56(%rbp)
	jne	.L2955
	movabsq	$411757680216731063, %r13
	movq	-56(%rbp), %rax
	subq	$224, %rax
	subq	-72(%rbp), %rax
	shrq	$5, %rax
	imulq	%r13, %rax
	movabsq	$576460752303423487, %r13
	andq	%r13, %rax
	addq	$1, %rax
	leaq	0(,%rax,8), %rdx
	subq	%rax, %rdx
	movq	%rdx, %rax
	salq	$5, %rax
	addq	%rax, -64(%rbp)
.L2954:
	movq	-64(%rbp), %rax
	movl	%r12d, %edi
	movq	%rax, 944(%r14)
	call	_ZN2v84base10hash_valueEj@PLT
	movq	48(%rbx), %rdi
	xorl	%edx, %edx
	movq	%rax, %r13
	divq	%rdi
	movq	40(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r15
	testq	%rax, %rax
	je	.L2956
	movq	(%rax), %rcx
	movq	24(%rcx), %rsi
	jmp	.L2959
	.p2align 4,,10
	.p2align 3
.L2957:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L2956
	movq	24(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r15
	jne	.L2956
.L2959:
	cmpq	%rsi, %r13
	jne	.L2957
	cmpl	%r12d, 8(%rcx)
	jne	.L2957
	addq	$16, %rcx
.L2962:
	movq	%r14, (%rcx)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2963:
	.cfi_restore_state
	movq	$0, -64(%rbp)
	xorl	%r13d, %r13d
	jmp	.L2951
	.p2align 4,,10
	.p2align 3
.L2956:
	movq	32(%rbx), %rdi
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	subq	%rcx, %rax
	cmpq	$31, %rax
	jbe	.L2980
	leaq	32(%rcx), %rax
	movq	%rax, 16(%rdi)
.L2961:
	movl	%r12d, 8(%rcx)
	leaq	32(%rbx), %rdi
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	$0, (%rcx)
	movl	$1, %r8d
	movq	$0, 16(%rcx)
	call	_ZNSt10_HashtableIiSt4pairIKiPN2v88internal8compiler34SerializerForBackgroundCompilation11EnvironmentEENS3_13ZoneAllocatorIS8_EENSt8__detail10_Select1stESt8equal_toIiENS2_4base4hashIiEENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS8_Lb1EEEm
	leaq	16(%rax), %rcx
	jmp	.L2962
	.p2align 4,,10
	.p2align 3
.L2978:
	movl	$960, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r14
	jmp	.L2950
	.p2align 4,,10
	.p2align 3
.L2979:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L2953
	.p2align 4,,10
	.p2align 3
.L2980:
	movl	$32, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L2961
	.cfi_endproc
.LFE19042:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation33ContributeToJumpTargetEnvironmentEi, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation33ContributeToJumpTargetEnvironmentEi
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation11ProcessJumpEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11ProcessJumpEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11ProcessJumpEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation11ProcessJumpEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB19047:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor19GetJumpTargetOffsetEv@PLT
	cmpl	8(%rbx), %eax
	jg	.L2984
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2984:
	.cfi_restore_state
	popq	%rbx
	movq	%r12, %rdi
	movl	%eax, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler34SerializerForBackgroundCompilation33ContributeToJumpTargetEnvironmentEi
	.cfi_endproc
.LFE19047:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11ProcessJumpEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation11ProcessJumpEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation26VisitSwitchOnSmiNoFeedbackEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation26VisitSwitchOnSmiNoFeedbackEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation26VisitSwitchOnSmiNoFeedbackEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation26VisitSwitchOnSmiNoFeedbackEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB19049:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-144(%rbp), %r14
	movq	%rdi, %r13
	pushq	%r12
	movq	%r14, %rdi
	.cfi_offset 12, -40
	leaq	-80(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -48
	leaq	-112(%rbp), %rbx
	subq	$112, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor25GetJumpTableTargetOffsetsEv@PLT
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZNK2v88internal11interpreter22JumpTableTargetOffsets5beginEv@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZNK2v88internal11interpreter22JumpTableTargetOffsets3endEv@PLT
	jmp	.L2987
	.p2align 4,,10
	.p2align 3
.L2990:
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter22JumpTableTargetOffsets8iteratordeEv@PLT
	movq	%r13, %rdi
	sarq	$32, %rax
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation33ContributeToJumpTargetEnvironmentEi
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter22JumpTableTargetOffsets8iteratorppEv@PLT
.L2987:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter22JumpTableTargetOffsets8iteratorneERKS3_@PLT
	testb	%al, %al
	jne	.L2990
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2991
	addq	$112, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2991:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19049:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation26VisitSwitchOnSmiNoFeedbackEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation26VisitSwitchOnSmiNoFeedbackEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation27VisitSwitchOnGeneratorStateEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation27VisitSwitchOnGeneratorStateEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation27VisitSwitchOnGeneratorStateEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation27VisitSwitchOnGeneratorStateEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB19050:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movl	96(%rdi), %r13d
	movl	100(%rdi), %r15d
	movq	(%rdi), %r14
	call	_ZNK2v88internal8compiler34SerializerForBackgroundCompilation14bytecode_arrayEv
	shrl	$2, %r13d
	xorl	%r8d, %r8d
	movl	%r15d, %edx
	andl	$1, %r13d
	movq	%rax, %rsi
	movq	%r14, %rdi
	movl	%r13d, %ecx
	call	_ZN2v88internal8compiler12JSHeapBroker19GetBytecodeAnalysisENS0_6HandleINS0_13BytecodeArrayEEENS0_9BailoutIdEbNS1_19SerializationPolicyE@PLT
	movq	160(%rax), %rbx
	movq	168(%rax), %r13
	cmpq	%r13, %rbx
	je	.L2992
	.p2align 4,,10
	.p2align 3
.L2994:
	movl	4(%rbx), %esi
	movq	%r12, %rdi
	addq	$12, %rbx
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation33ContributeToJumpTargetEnvironmentEi
	cmpq	%rbx, %r13
	jne	.L2994
.L2992:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19050:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation27VisitSwitchOnGeneratorStateEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation27VisitSwitchOnGeneratorStateEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation20VisitJumpIfUndefinedEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation20VisitJumpIfUndefinedEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation20VisitJumpIfUndefinedEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation20VisitJumpIfUndefinedEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25595:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor19GetJumpTargetOffsetEv@PLT
	cmpl	8(%rbx), %eax
	jg	.L3000
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3000:
	.cfi_restore_state
	popq	%rbx
	movq	%r12, %rdi
	movl	%eax, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler34SerializerForBackgroundCompilation33ContributeToJumpTargetEnvironmentEi
	.cfi_endproc
.LFE25595:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation20VisitJumpIfUndefinedEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation20VisitJumpIfUndefinedEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation17VisitJumpConstantEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation17VisitJumpConstantEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation17VisitJumpConstantEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation17VisitJumpConstantEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25725:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor19GetJumpTargetOffsetEv@PLT
	cmpl	8(%rbx), %eax
	jg	.L3007
.L3002:
	movq	24(%r12), %rax
	movq	936(%rax), %rbx
	movq	944(%rax), %r12
	cmpq	%r12, %rbx
	je	.L3001
	.p2align 4,,10
	.p2align 3
.L3004:
	movq	%rbx, %rdi
	addq	$224, %rbx
	call	_ZN2v88internal8compiler5Hints5ClearEv
	cmpq	%rbx, %r12
	jne	.L3004
.L3001:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3007:
	.cfi_restore_state
	movl	%eax, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation33ContributeToJumpTargetEnvironmentEi
	jmp	.L3002
	.cfi_endproc
.LFE25725:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation17VisitJumpConstantEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation17VisitJumpConstantEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation16VisitJumpIfFalseEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation16VisitJumpIfFalseEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation16VisitJumpIfFalseEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation16VisitJumpIfFalseEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB19134:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor19GetJumpTargetOffsetEv@PLT
	cmpl	8(%rbx), %eax
	jg	.L3011
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3011:
	.cfi_restore_state
	popq	%rbx
	movq	%r12, %rdi
	movl	%eax, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler34SerializerForBackgroundCompilation33ContributeToJumpTargetEnvironmentEi
	.cfi_endproc
.LFE19134:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation16VisitJumpIfFalseEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation16VisitJumpIfFalseEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation34VisitJumpIfUndefinedOrNullConstantEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation34VisitJumpIfUndefinedOrNullConstantEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation34VisitJumpIfUndefinedOrNullConstantEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation34VisitJumpIfUndefinedOrNullConstantEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25601:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor19GetJumpTargetOffsetEv@PLT
	cmpl	8(%rbx), %eax
	jg	.L3015
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3015:
	.cfi_restore_state
	popq	%rbx
	movq	%r12, %rdi
	movl	%eax, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler34SerializerForBackgroundCompilation33ContributeToJumpTargetEnvironmentEi
	.cfi_endproc
.LFE25601:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation34VisitJumpIfUndefinedOrNullConstantEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation34VisitJumpIfUndefinedOrNullConstantEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation24VisitJumpIfToBooleanTrueEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation24VisitJumpIfToBooleanTrueEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation24VisitJumpIfToBooleanTrueEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation24VisitJumpIfToBooleanTrueEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25587:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor19GetJumpTargetOffsetEv@PLT
	cmpl	8(%rbx), %eax
	jg	.L3019
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3019:
	.cfi_restore_state
	popq	%rbx
	movq	%r12, %rdi
	movl	%eax, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler34SerializerForBackgroundCompilation33ContributeToJumpTargetEnvironmentEi
	.cfi_endproc
.LFE25587:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation24VisitJumpIfToBooleanTrueEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation24VisitJumpIfToBooleanTrueEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation15VisitJumpIfTrueEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation15VisitJumpIfTrueEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation15VisitJumpIfTrueEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation15VisitJumpIfTrueEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25591:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor19GetJumpTargetOffsetEv@PLT
	cmpl	8(%rbx), %eax
	jg	.L3023
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3023:
	.cfi_restore_state
	popq	%rbx
	movq	%r12, %rdi
	movl	%eax, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler34SerializerForBackgroundCompilation33ContributeToJumpTargetEnvironmentEi
	.cfi_endproc
.LFE25591:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation15VisitJumpIfTrueEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation15VisitJumpIfTrueEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation15VisitJumpIfNullEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation15VisitJumpIfNullEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation15VisitJumpIfNullEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation15VisitJumpIfNullEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25579:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor19GetJumpTargetOffsetEv@PLT
	cmpl	8(%rbx), %eax
	jg	.L3027
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3027:
	.cfi_restore_state
	popq	%rbx
	movq	%r12, %rdi
	movl	%eax, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler34SerializerForBackgroundCompilation33ContributeToJumpTargetEnvironmentEi
	.cfi_endproc
.LFE25579:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation15VisitJumpIfNullEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation15VisitJumpIfNullEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation23VisitJumpIfNullConstantEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation23VisitJumpIfNullConstantEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation23VisitJumpIfNullConstantEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation23VisitJumpIfNullConstantEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25581:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor19GetJumpTargetOffsetEv@PLT
	cmpl	8(%rbx), %eax
	jg	.L3031
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3031:
	.cfi_restore_state
	popq	%rbx
	movq	%r12, %rdi
	movl	%eax, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler34SerializerForBackgroundCompilation33ContributeToJumpTargetEnvironmentEi
	.cfi_endproc
.LFE25581:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation23VisitJumpIfNullConstantEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation23VisitJumpIfNullConstantEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation25VisitJumpIfToBooleanFalseEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation25VisitJumpIfToBooleanFalseEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation25VisitJumpIfToBooleanFalseEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation25VisitJumpIfToBooleanFalseEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25583:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor19GetJumpTargetOffsetEv@PLT
	cmpl	8(%rbx), %eax
	jg	.L3035
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3035:
	.cfi_restore_state
	popq	%rbx
	movq	%r12, %rdi
	movl	%eax, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler34SerializerForBackgroundCompilation33ContributeToJumpTargetEnvironmentEi
	.cfi_endproc
.LFE25583:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation25VisitJumpIfToBooleanFalseEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation25VisitJumpIfToBooleanFalseEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation32VisitJumpIfToBooleanTrueConstantEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation32VisitJumpIfToBooleanTrueConstantEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation32VisitJumpIfToBooleanTrueConstantEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation32VisitJumpIfToBooleanTrueConstantEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25589:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor19GetJumpTargetOffsetEv@PLT
	cmpl	8(%rbx), %eax
	jg	.L3039
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3039:
	.cfi_restore_state
	popq	%rbx
	movq	%r12, %rdi
	movl	%eax, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler34SerializerForBackgroundCompilation33ContributeToJumpTargetEnvironmentEi
	.cfi_endproc
.LFE25589:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation32VisitJumpIfToBooleanTrueConstantEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation32VisitJumpIfToBooleanTrueConstantEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation24VisitJumpIfFalseConstantEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation24VisitJumpIfFalseConstantEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation24VisitJumpIfFalseConstantEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation24VisitJumpIfFalseConstantEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25565:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor19GetJumpTargetOffsetEv@PLT
	cmpl	8(%rbx), %eax
	jg	.L3043
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3043:
	.cfi_restore_state
	popq	%rbx
	movq	%r12, %rdi
	movl	%eax, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler34SerializerForBackgroundCompilation33ContributeToJumpTargetEnvironmentEi
	.cfi_endproc
.LFE25565:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation24VisitJumpIfFalseConstantEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation24VisitJumpIfFalseConstantEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation29VisitJumpIfJSReceiverConstantEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation29VisitJumpIfJSReceiverConstantEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation29VisitJumpIfJSReceiverConstantEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation29VisitJumpIfJSReceiverConstantEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25569:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor19GetJumpTargetOffsetEv@PLT
	cmpl	8(%rbx), %eax
	jg	.L3047
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3047:
	.cfi_restore_state
	popq	%rbx
	movq	%r12, %rdi
	movl	%eax, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler34SerializerForBackgroundCompilation33ContributeToJumpTargetEnvironmentEi
	.cfi_endproc
.LFE25569:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation29VisitJumpIfJSReceiverConstantEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation29VisitJumpIfJSReceiverConstantEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation21VisitJumpIfJSReceiverEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation21VisitJumpIfJSReceiverEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation21VisitJumpIfJSReceiverEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation21VisitJumpIfJSReceiverEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25567:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor19GetJumpTargetOffsetEv@PLT
	cmpl	8(%rbx), %eax
	jg	.L3051
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3051:
	.cfi_restore_state
	popq	%rbx
	movq	%r12, %rdi
	movl	%eax, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler34SerializerForBackgroundCompilation33ContributeToJumpTargetEnvironmentEi
	.cfi_endproc
.LFE25567:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation21VisitJumpIfJSReceiverEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation21VisitJumpIfJSReceiverEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation31VisitJumpIfNotUndefinedConstantEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation31VisitJumpIfNotUndefinedConstantEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation31VisitJumpIfNotUndefinedConstantEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation31VisitJumpIfNotUndefinedConstantEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25577:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor19GetJumpTargetOffsetEv@PLT
	cmpl	8(%rbx), %eax
	jg	.L3055
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3055:
	.cfi_restore_state
	popq	%rbx
	movq	%r12, %rdi
	movl	%eax, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler34SerializerForBackgroundCompilation33ContributeToJumpTargetEnvironmentEi
	.cfi_endproc
.LFE25577:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation31VisitJumpIfNotUndefinedConstantEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation31VisitJumpIfNotUndefinedConstantEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitJumpIfNotNullEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitJumpIfNotNullEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitJumpIfNotNullEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitJumpIfNotNullEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25571:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor19GetJumpTargetOffsetEv@PLT
	cmpl	8(%rbx), %eax
	jg	.L3059
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3059:
	.cfi_restore_state
	popq	%rbx
	movq	%r12, %rdi
	movl	%eax, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler34SerializerForBackgroundCompilation33ContributeToJumpTargetEnvironmentEi
	.cfi_endproc
.LFE25571:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitJumpIfNotNullEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitJumpIfNotNullEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation26VisitJumpIfNotNullConstantEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation26VisitJumpIfNotNullConstantEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation26VisitJumpIfNotNullConstantEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation26VisitJumpIfNotNullConstantEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25573:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor19GetJumpTargetOffsetEv@PLT
	cmpl	8(%rbx), %eax
	jg	.L3063
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3063:
	.cfi_restore_state
	popq	%rbx
	movq	%r12, %rdi
	movl	%eax, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler34SerializerForBackgroundCompilation33ContributeToJumpTargetEnvironmentEi
	.cfi_endproc
.LFE25573:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation26VisitJumpIfNotNullConstantEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation26VisitJumpIfNotNullConstantEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation23VisitJumpIfNotUndefinedEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation23VisitJumpIfNotUndefinedEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation23VisitJumpIfNotUndefinedEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation23VisitJumpIfNotUndefinedEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25575:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor19GetJumpTargetOffsetEv@PLT
	cmpl	8(%rbx), %eax
	jg	.L3067
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3067:
	.cfi_restore_state
	popq	%rbx
	movq	%r12, %rdi
	movl	%eax, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler34SerializerForBackgroundCompilation33ContributeToJumpTargetEnvironmentEi
	.cfi_endproc
.LFE25575:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation23VisitJumpIfNotUndefinedEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation23VisitJumpIfNotUndefinedEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation33VisitJumpIfToBooleanFalseConstantEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation33VisitJumpIfToBooleanFalseConstantEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation33VisitJumpIfToBooleanFalseConstantEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation33VisitJumpIfToBooleanFalseConstantEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25585:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor19GetJumpTargetOffsetEv@PLT
	cmpl	8(%rbx), %eax
	jg	.L3071
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3071:
	.cfi_restore_state
	popq	%rbx
	movq	%r12, %rdi
	movl	%eax, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler34SerializerForBackgroundCompilation33ContributeToJumpTargetEnvironmentEi
	.cfi_endproc
.LFE25585:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation33VisitJumpIfToBooleanFalseConstantEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation33VisitJumpIfToBooleanFalseConstantEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation13VisitJumpLoopEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation13VisitJumpLoopEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation13VisitJumpLoopEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation13VisitJumpLoopEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25727:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor19GetJumpTargetOffsetEv@PLT
	cmpl	8(%rbx), %eax
	jg	.L3078
.L3073:
	movq	24(%r12), %rax
	movq	936(%rax), %rbx
	movq	944(%rax), %r12
	cmpq	%r12, %rbx
	je	.L3072
	.p2align 4,,10
	.p2align 3
.L3075:
	movq	%rbx, %rdi
	addq	$224, %rbx
	call	_ZN2v88internal8compiler5Hints5ClearEv
	cmpq	%rbx, %r12
	jne	.L3075
.L3072:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3078:
	.cfi_restore_state
	movl	%eax, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation33ContributeToJumpTargetEnvironmentEi
	jmp	.L3073
	.cfi_endproc
.LFE25727:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation13VisitJumpLoopEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation13VisitJumpLoopEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation23VisitJumpIfTrueConstantEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation23VisitJumpIfTrueConstantEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation23VisitJumpIfTrueConstantEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation23VisitJumpIfTrueConstantEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25593:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor19GetJumpTargetOffsetEv@PLT
	cmpl	8(%rbx), %eax
	jg	.L3082
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3082:
	.cfi_restore_state
	popq	%rbx
	movq	%r12, %rdi
	movl	%eax, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler34SerializerForBackgroundCompilation33ContributeToJumpTargetEnvironmentEi
	.cfi_endproc
.LFE25593:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation23VisitJumpIfTrueConstantEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation23VisitJumpIfTrueConstantEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation28VisitJumpIfUndefinedConstantEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation28VisitJumpIfUndefinedConstantEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation28VisitJumpIfUndefinedConstantEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation28VisitJumpIfUndefinedConstantEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25597:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor19GetJumpTargetOffsetEv@PLT
	cmpl	8(%rbx), %eax
	jg	.L3086
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3086:
	.cfi_restore_state
	popq	%rbx
	movq	%r12, %rdi
	movl	%eax, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler34SerializerForBackgroundCompilation33ContributeToJumpTargetEnvironmentEi
	.cfi_endproc
.LFE25597:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation28VisitJumpIfUndefinedConstantEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation28VisitJumpIfUndefinedConstantEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation26VisitJumpIfUndefinedOrNullEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation26VisitJumpIfUndefinedOrNullEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation26VisitJumpIfUndefinedOrNullEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation26VisitJumpIfUndefinedOrNullEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25599:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor19GetJumpTargetOffsetEv@PLT
	cmpl	8(%rbx), %eax
	jg	.L3090
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3090:
	.cfi_restore_state
	popq	%rbx
	movq	%r12, %rdi
	movl	%eax, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler34SerializerForBackgroundCompilation33ContributeToJumpTargetEnvironmentEi
	.cfi_endproc
.LFE25599:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation26VisitJumpIfUndefinedOrNullEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation26VisitJumpIfUndefinedOrNullEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation9VisitJumpEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation9VisitJumpEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation9VisitJumpEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation9VisitJumpEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB19154:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor19GetJumpTargetOffsetEv@PLT
	cmpl	8(%rbx), %eax
	jg	.L3097
.L3092:
	movq	24(%r12), %rax
	movq	936(%rax), %rbx
	movq	944(%rax), %r12
	cmpq	%r12, %rbx
	je	.L3091
	.p2align 4,,10
	.p2align 3
.L3094:
	movq	%rbx, %rdi
	addq	$224, %rbx
	call	_ZN2v88internal8compiler5Hints5ClearEv
	cmpq	%rbx, %r12
	jne	.L3094
.L3091:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3097:
	.cfi_restore_state
	movl	%eax, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation33ContributeToJumpTargetEnvironmentEi
	jmp	.L3092
	.cfi_endproc
.LFE19154:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation9VisitJumpEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation9VisitJumpEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitBitwiseXorSmiEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitBitwiseXorSmiEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitBitwiseXorSmiEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitBitwiseXorSmiEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25623:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movl	$1, %esi
	subq	$256, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	cmpl	$-1, %eax
	je	.L3098
	movq	24(%rbx), %rsi
	movl	%eax, %r12d
	leaq	-272(%rbp), %r13
	leaq	-288(%rbp), %r14
	movq	%r13, %rdi
	movq	8(%rsi), %rax
	addq	$24, %rsi
	movq	%rax, -288(%rbp)
	movq	-8(%rsi), %rax
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	$0, -280(%rbp)
	je	.L3098
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler34SerializerForBackgroundCompilation15feedback_vectorEv
	movl	%r12d, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS0_6HandleINS0_14FeedbackVectorEEENS0_12FeedbackSlotE@PLT
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler12JSHeapBroker33ProcessFeedbackForBinaryOperationERKNS1_14FeedbackSourceE@PLT
	movq	24(%rbx), %r13
	testb	$1, 96(%rbx)
	movq	936(%r13), %r14
	movq	944(%r13), %r12
	je	.L3103
	cmpl	$-1, 100(%rbx)
	jne	.L3103
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L3103
	cmpq	%r12, %r14
	je	.L3098
	movq	%r14, %rbx
	.p2align 4,,10
	.p2align 3
.L3104:
	movq	%rbx, %rdi
	addq	$224, %rbx
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	%r12, %rbx
	jne	.L3104
	movq	%r14, 944(%r13)
	.p2align 4,,10
	.p2align 3
.L3098:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3113
	addq	$256, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3103:
	.cfi_restore_state
	movl	252(%r13), %eax
	addl	248(%r13), %eax
	movslq	%eax, %rdx
	js	.L3105
	movabsq	$7905747460161236407, %rax
	subq	%r14, %r12
	sarq	$5, %r12
	imulq	%rax, %r12
	cmpq	%r12, %rdx
	jb	.L3105
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3105:
	leaq	0(,%rdx,8), %rdi
	subq	%rdx, %rdi
	salq	$5, %rdi
	addq	%r14, %rdi
	call	_ZN2v88internal8compiler5Hints5ClearEv
	jmp	.L3098
.L3113:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25623:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitBitwiseXorSmiEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitBitwiseXorSmiEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation22VisitShiftRightLogicalEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation22VisitShiftRightLogicalEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation22VisitShiftRightLogicalEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation22VisitShiftRightLogicalEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25649:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movl	$1, %esi
	subq	$256, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	cmpl	$-1, %eax
	je	.L3114
	movq	24(%rbx), %rsi
	movl	%eax, %r12d
	leaq	-272(%rbp), %r13
	leaq	-288(%rbp), %r14
	movq	%r13, %rdi
	movq	8(%rsi), %rax
	addq	$24, %rsi
	movq	%rax, -288(%rbp)
	movq	-8(%rsi), %rax
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	$0, -280(%rbp)
	je	.L3114
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler34SerializerForBackgroundCompilation15feedback_vectorEv
	movl	%r12d, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS0_6HandleINS0_14FeedbackVectorEEENS0_12FeedbackSlotE@PLT
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler12JSHeapBroker33ProcessFeedbackForBinaryOperationERKNS1_14FeedbackSourceE@PLT
	movq	24(%rbx), %r13
	testb	$1, 96(%rbx)
	movq	936(%r13), %r14
	movq	944(%r13), %r12
	je	.L3119
	cmpl	$-1, 100(%rbx)
	jne	.L3119
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L3119
	cmpq	%r12, %r14
	je	.L3114
	movq	%r14, %rbx
	.p2align 4,,10
	.p2align 3
.L3120:
	movq	%rbx, %rdi
	addq	$224, %rbx
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	%r12, %rbx
	jne	.L3120
	movq	%r14, 944(%r13)
	.p2align 4,,10
	.p2align 3
.L3114:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3129
	addq	$256, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3119:
	.cfi_restore_state
	movl	252(%r13), %eax
	addl	248(%r13), %eax
	movslq	%eax, %rdx
	js	.L3121
	movabsq	$7905747460161236407, %rax
	subq	%r14, %r12
	sarq	$5, %r12
	imulq	%rax, %r12
	cmpq	%r12, %rdx
	jb	.L3121
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3121:
	leaq	0(,%rdx,8), %rdi
	subq	%rdx, %rdi
	salq	$5, %rdi
	addq	%r14, %rdi
	call	_ZN2v88internal8compiler5Hints5ClearEv
	jmp	.L3114
.L3129:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25649:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation22VisitShiftRightLogicalEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation22VisitShiftRightLogicalEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation15VisitShiftRightEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation15VisitShiftRightEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation15VisitShiftRightEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation15VisitShiftRightEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25645:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movl	$1, %esi
	subq	$256, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	cmpl	$-1, %eax
	je	.L3130
	movq	24(%rbx), %rsi
	movl	%eax, %r12d
	leaq	-272(%rbp), %r13
	leaq	-288(%rbp), %r14
	movq	%r13, %rdi
	movq	8(%rsi), %rax
	addq	$24, %rsi
	movq	%rax, -288(%rbp)
	movq	-8(%rsi), %rax
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	$0, -280(%rbp)
	je	.L3130
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler34SerializerForBackgroundCompilation15feedback_vectorEv
	movl	%r12d, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS0_6HandleINS0_14FeedbackVectorEEENS0_12FeedbackSlotE@PLT
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler12JSHeapBroker33ProcessFeedbackForBinaryOperationERKNS1_14FeedbackSourceE@PLT
	movq	24(%rbx), %r13
	testb	$1, 96(%rbx)
	movq	936(%r13), %r14
	movq	944(%r13), %r12
	je	.L3135
	cmpl	$-1, 100(%rbx)
	jne	.L3135
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L3135
	cmpq	%r12, %r14
	je	.L3130
	movq	%r14, %rbx
	.p2align 4,,10
	.p2align 3
.L3136:
	movq	%rbx, %rdi
	addq	$224, %rbx
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	%r12, %rbx
	jne	.L3136
	movq	%r14, 944(%r13)
	.p2align 4,,10
	.p2align 3
.L3130:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3145
	addq	$256, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3135:
	.cfi_restore_state
	movl	252(%r13), %eax
	addl	248(%r13), %eax
	movslq	%eax, %rdx
	js	.L3137
	movabsq	$7905747460161236407, %rax
	subq	%r14, %r12
	sarq	$5, %r12
	imulq	%rax, %r12
	cmpq	%r12, %rdx
	jb	.L3137
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3137:
	leaq	0(,%rdx,8), %rdi
	subq	%rdx, %rdi
	salq	$5, %rdi
	addq	%r14, %rdi
	call	_ZN2v88internal8compiler5Hints5ClearEv
	jmp	.L3130
.L3145:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25645:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation15VisitShiftRightEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation15VisitShiftRightEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation8VisitDecEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation8VisitDecEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation8VisitDecEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation8VisitDecEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25719:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	xorl	%esi, %esi
	subq	$256, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	cmpl	$-1, %eax
	je	.L3146
	movq	24(%rbx), %rsi
	movl	%eax, %r12d
	leaq	-272(%rbp), %r13
	leaq	-288(%rbp), %r14
	movq	%r13, %rdi
	movq	8(%rsi), %rax
	addq	$24, %rsi
	movq	%rax, -288(%rbp)
	movq	-8(%rsi), %rax
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	$0, -280(%rbp)
	je	.L3146
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler34SerializerForBackgroundCompilation15feedback_vectorEv
	movl	%r12d, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS0_6HandleINS0_14FeedbackVectorEEENS0_12FeedbackSlotE@PLT
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler12JSHeapBroker33ProcessFeedbackForBinaryOperationERKNS1_14FeedbackSourceE@PLT
	movq	24(%rbx), %r13
	testb	$1, 96(%rbx)
	movq	936(%r13), %r14
	movq	944(%r13), %r12
	je	.L3151
	cmpl	$-1, 100(%rbx)
	jne	.L3151
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L3151
	cmpq	%r12, %r14
	je	.L3146
	movq	%r14, %rbx
	.p2align 4,,10
	.p2align 3
.L3152:
	movq	%rbx, %rdi
	addq	$224, %rbx
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	%r12, %rbx
	jne	.L3152
	movq	%r14, 944(%r13)
	.p2align 4,,10
	.p2align 3
.L3146:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3161
	addq	$256, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3151:
	.cfi_restore_state
	movl	252(%r13), %eax
	addl	248(%r13), %eax
	movslq	%eax, %rdx
	js	.L3153
	movabsq	$7905747460161236407, %rax
	subq	%r14, %r12
	sarq	$5, %r12
	imulq	%rax, %r12
	cmpq	%r12, %rdx
	jb	.L3153
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3153:
	leaq	0(,%rdx,8), %rdi
	subq	%rdx, %rdi
	salq	$5, %rdi
	addq	%r14, %rdi
	call	_ZN2v88internal8compiler5Hints5ClearEv
	jmp	.L3146
.L3161:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25719:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation8VisitDecEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation8VisitDecEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitMulSmiEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitMulSmiEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitMulSmiEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitMulSmiEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25639:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movl	$1, %esi
	subq	$256, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	cmpl	$-1, %eax
	je	.L3162
	movq	24(%rbx), %rsi
	movl	%eax, %r12d
	leaq	-272(%rbp), %r13
	leaq	-288(%rbp), %r14
	movq	%r13, %rdi
	movq	8(%rsi), %rax
	addq	$24, %rsi
	movq	%rax, -288(%rbp)
	movq	-8(%rsi), %rax
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	$0, -280(%rbp)
	je	.L3162
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler34SerializerForBackgroundCompilation15feedback_vectorEv
	movl	%r12d, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS0_6HandleINS0_14FeedbackVectorEEENS0_12FeedbackSlotE@PLT
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler12JSHeapBroker33ProcessFeedbackForBinaryOperationERKNS1_14FeedbackSourceE@PLT
	movq	24(%rbx), %r13
	testb	$1, 96(%rbx)
	movq	936(%r13), %r14
	movq	944(%r13), %r12
	je	.L3167
	cmpl	$-1, 100(%rbx)
	jne	.L3167
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L3167
	cmpq	%r12, %r14
	je	.L3162
	movq	%r14, %rbx
	.p2align 4,,10
	.p2align 3
.L3168:
	movq	%rbx, %rdi
	addq	$224, %rbx
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	%r12, %rbx
	jne	.L3168
	movq	%r14, 944(%r13)
	.p2align 4,,10
	.p2align 3
.L3162:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3177
	addq	$256, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3167:
	.cfi_restore_state
	movl	252(%r13), %eax
	addl	248(%r13), %eax
	movslq	%eax, %rdx
	js	.L3169
	movabsq	$7905747460161236407, %rax
	subq	%r14, %r12
	sarq	$5, %r12
	imulq	%rax, %r12
	cmpq	%r12, %rdx
	jb	.L3169
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3169:
	leaq	0(,%rdx,8), %rdi
	subq	%rdx, %rdi
	salq	$5, %rdi
	addq	%r14, %rdi
	call	_ZN2v88internal8compiler5Hints5ClearEv
	jmp	.L3162
.L3177:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25639:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitMulSmiEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitMulSmiEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitDivSmiEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitDivSmiEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitDivSmiEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitDivSmiEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25627:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movl	$1, %esi
	subq	$256, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	cmpl	$-1, %eax
	je	.L3178
	movq	24(%rbx), %rsi
	movl	%eax, %r12d
	leaq	-272(%rbp), %r13
	leaq	-288(%rbp), %r14
	movq	%r13, %rdi
	movq	8(%rsi), %rax
	addq	$24, %rsi
	movq	%rax, -288(%rbp)
	movq	-8(%rsi), %rax
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	$0, -280(%rbp)
	je	.L3178
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler34SerializerForBackgroundCompilation15feedback_vectorEv
	movl	%r12d, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS0_6HandleINS0_14FeedbackVectorEEENS0_12FeedbackSlotE@PLT
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler12JSHeapBroker33ProcessFeedbackForBinaryOperationERKNS1_14FeedbackSourceE@PLT
	movq	24(%rbx), %r13
	testb	$1, 96(%rbx)
	movq	936(%r13), %r14
	movq	944(%r13), %r12
	je	.L3183
	cmpl	$-1, 100(%rbx)
	jne	.L3183
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L3183
	cmpq	%r12, %r14
	je	.L3178
	movq	%r14, %rbx
	.p2align 4,,10
	.p2align 3
.L3184:
	movq	%rbx, %rdi
	addq	$224, %rbx
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	%r12, %rbx
	jne	.L3184
	movq	%r14, 944(%r13)
	.p2align 4,,10
	.p2align 3
.L3178:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3193
	addq	$256, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3183:
	.cfi_restore_state
	movl	252(%r13), %eax
	addl	248(%r13), %eax
	movslq	%eax, %rdx
	js	.L3185
	movabsq	$7905747460161236407, %rax
	subq	%r14, %r12
	sarq	$5, %r12
	imulq	%rax, %r12
	cmpq	%r12, %rdx
	jb	.L3185
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3185:
	leaq	0(,%rdx,8), %rdi
	subq	%rdx, %rdi
	salq	$5, %rdi
	addq	%r14, %rdi
	call	_ZN2v88internal8compiler5Hints5ClearEv
	jmp	.L3178
.L3193:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25627:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitDivSmiEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitDivSmiEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation8VisitSubEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation8VisitSubEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation8VisitSubEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation8VisitSubEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25653:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movl	$1, %esi
	subq	$256, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	cmpl	$-1, %eax
	je	.L3194
	movq	24(%rbx), %rsi
	movl	%eax, %r12d
	leaq	-272(%rbp), %r13
	leaq	-288(%rbp), %r14
	movq	%r13, %rdi
	movq	8(%rsi), %rax
	addq	$24, %rsi
	movq	%rax, -288(%rbp)
	movq	-8(%rsi), %rax
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	$0, -280(%rbp)
	je	.L3194
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler34SerializerForBackgroundCompilation15feedback_vectorEv
	movl	%r12d, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS0_6HandleINS0_14FeedbackVectorEEENS0_12FeedbackSlotE@PLT
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler12JSHeapBroker33ProcessFeedbackForBinaryOperationERKNS1_14FeedbackSourceE@PLT
	movq	24(%rbx), %r13
	testb	$1, 96(%rbx)
	movq	936(%r13), %r14
	movq	944(%r13), %r12
	je	.L3199
	cmpl	$-1, 100(%rbx)
	jne	.L3199
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L3199
	cmpq	%r12, %r14
	je	.L3194
	movq	%r14, %rbx
	.p2align 4,,10
	.p2align 3
.L3200:
	movq	%rbx, %rdi
	addq	$224, %rbx
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	%r12, %rbx
	jne	.L3200
	movq	%r14, 944(%r13)
	.p2align 4,,10
	.p2align 3
.L3194:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3209
	addq	$256, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3199:
	.cfi_restore_state
	movl	252(%r13), %eax
	addl	248(%r13), %eax
	movslq	%eax, %rdx
	js	.L3201
	movabsq	$7905747460161236407, %rax
	subq	%r14, %r12
	sarq	$5, %r12
	imulq	%rax, %r12
	cmpq	%r12, %rdx
	jb	.L3201
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3201:
	leaq	0(,%rdx,8), %rdi
	subq	%rdx, %rdi
	salq	$5, %rdi
	addq	%r14, %rdi
	call	_ZN2v88internal8compiler5Hints5ClearEv
	jmp	.L3194
.L3209:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25653:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation8VisitSubEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation8VisitSubEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitShiftRightSmiEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitShiftRightSmiEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitShiftRightSmiEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitShiftRightSmiEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25647:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movl	$1, %esi
	subq	$256, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	cmpl	$-1, %eax
	je	.L3210
	movq	24(%rbx), %rsi
	movl	%eax, %r12d
	leaq	-272(%rbp), %r13
	leaq	-288(%rbp), %r14
	movq	%r13, %rdi
	movq	8(%rsi), %rax
	addq	$24, %rsi
	movq	%rax, -288(%rbp)
	movq	-8(%rsi), %rax
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	$0, -280(%rbp)
	je	.L3210
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler34SerializerForBackgroundCompilation15feedback_vectorEv
	movl	%r12d, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS0_6HandleINS0_14FeedbackVectorEEENS0_12FeedbackSlotE@PLT
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler12JSHeapBroker33ProcessFeedbackForBinaryOperationERKNS1_14FeedbackSourceE@PLT
	movq	24(%rbx), %r13
	testb	$1, 96(%rbx)
	movq	936(%r13), %r14
	movq	944(%r13), %r12
	je	.L3215
	cmpl	$-1, 100(%rbx)
	jne	.L3215
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L3215
	cmpq	%r12, %r14
	je	.L3210
	movq	%r14, %rbx
	.p2align 4,,10
	.p2align 3
.L3216:
	movq	%rbx, %rdi
	addq	$224, %rbx
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	%r12, %rbx
	jne	.L3216
	movq	%r14, 944(%r13)
	.p2align 4,,10
	.p2align 3
.L3210:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3225
	addq	$256, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3215:
	.cfi_restore_state
	movl	252(%r13), %eax
	addl	248(%r13), %eax
	movslq	%eax, %rdx
	js	.L3217
	movabsq	$7905747460161236407, %rax
	subq	%r14, %r12
	sarq	$5, %r12
	imulq	%rax, %r12
	cmpq	%r12, %rdx
	jb	.L3217
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3217:
	leaq	0(,%rdx,8), %rdi
	subq	%rdx, %rdi
	salq	$5, %rdi
	addq	%r14, %rdi
	call	_ZN2v88internal8compiler5Hints5ClearEv
	jmp	.L3210
.L3225:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25647:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitShiftRightSmiEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitShiftRightSmiEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation15VisitBitwiseXorEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation15VisitBitwiseXorEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation15VisitBitwiseXorEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation15VisitBitwiseXorEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25621:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movl	$1, %esi
	subq	$256, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	cmpl	$-1, %eax
	je	.L3226
	movq	24(%rbx), %rsi
	movl	%eax, %r12d
	leaq	-272(%rbp), %r13
	leaq	-288(%rbp), %r14
	movq	%r13, %rdi
	movq	8(%rsi), %rax
	addq	$24, %rsi
	movq	%rax, -288(%rbp)
	movq	-8(%rsi), %rax
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	$0, -280(%rbp)
	je	.L3226
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler34SerializerForBackgroundCompilation15feedback_vectorEv
	movl	%r12d, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS0_6HandleINS0_14FeedbackVectorEEENS0_12FeedbackSlotE@PLT
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler12JSHeapBroker33ProcessFeedbackForBinaryOperationERKNS1_14FeedbackSourceE@PLT
	movq	24(%rbx), %r13
	testb	$1, 96(%rbx)
	movq	936(%r13), %r14
	movq	944(%r13), %r12
	je	.L3231
	cmpl	$-1, 100(%rbx)
	jne	.L3231
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L3231
	cmpq	%r12, %r14
	je	.L3226
	movq	%r14, %rbx
	.p2align 4,,10
	.p2align 3
.L3232:
	movq	%rbx, %rdi
	addq	$224, %rbx
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	%r12, %rbx
	jne	.L3232
	movq	%r14, 944(%r13)
	.p2align 4,,10
	.p2align 3
.L3226:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3241
	addq	$256, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3231:
	.cfi_restore_state
	movl	252(%r13), %eax
	addl	248(%r13), %eax
	movslq	%eax, %rdx
	js	.L3233
	movabsq	$7905747460161236407, %rax
	subq	%r14, %r12
	sarq	$5, %r12
	imulq	%rax, %r12
	cmpq	%r12, %rdx
	jb	.L3233
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3233:
	leaq	0(,%rdx,8), %rdi
	subq	%rdx, %rdi
	salq	$5, %rdi
	addq	%r14, %rdi
	call	_ZN2v88internal8compiler5Hints5ClearEv
	jmp	.L3226
.L3241:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25621:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation15VisitBitwiseXorEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation15VisitBitwiseXorEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation14VisitToNumericEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation14VisitToNumericEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation14VisitToNumericEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation14VisitToNumericEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25609:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	xorl	%esi, %esi
	subq	$256, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	cmpl	$-1, %eax
	je	.L3242
	movq	24(%rbx), %rsi
	movl	%eax, %r12d
	leaq	-272(%rbp), %r13
	leaq	-288(%rbp), %r14
	movq	%r13, %rdi
	movq	8(%rsi), %rax
	addq	$24, %rsi
	movq	%rax, -288(%rbp)
	movq	-8(%rsi), %rax
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	$0, -280(%rbp)
	je	.L3242
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler34SerializerForBackgroundCompilation15feedback_vectorEv
	movl	%r12d, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS0_6HandleINS0_14FeedbackVectorEEENS0_12FeedbackSlotE@PLT
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler12JSHeapBroker33ProcessFeedbackForBinaryOperationERKNS1_14FeedbackSourceE@PLT
	movq	24(%rbx), %rdx
	movl	252(%rdx), %eax
	addl	248(%rdx), %eax
	movq	936(%rdx), %rcx
	cltq
	js	.L3247
	movq	944(%rdx), %rdx
	movabsq	$7905747460161236407, %rsi
	subq	%rcx, %rdx
	sarq	$5, %rdx
	imulq	%rsi, %rdx
	cmpq	%rax, %rdx
	jbe	.L3253
.L3247:
	leaq	0(,%rax,8), %rdi
	subq	%rax, %rdi
	salq	$5, %rdi
	addq	%rcx, %rdi
	call	_ZN2v88internal8compiler5Hints5ClearEv
.L3242:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3254
	addq	$256, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3253:
	.cfi_restore_state
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L3254:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25609:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation14VisitToNumericEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation14VisitToNumericEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation8VisitModEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation8VisitModEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation8VisitModEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation8VisitModEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25633:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movl	$1, %esi
	subq	$256, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	cmpl	$-1, %eax
	je	.L3255
	movq	24(%rbx), %rsi
	movl	%eax, %r12d
	leaq	-272(%rbp), %r13
	leaq	-288(%rbp), %r14
	movq	%r13, %rdi
	movq	8(%rsi), %rax
	addq	$24, %rsi
	movq	%rax, -288(%rbp)
	movq	-8(%rsi), %rax
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	$0, -280(%rbp)
	je	.L3255
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler34SerializerForBackgroundCompilation15feedback_vectorEv
	movl	%r12d, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS0_6HandleINS0_14FeedbackVectorEEENS0_12FeedbackSlotE@PLT
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler12JSHeapBroker33ProcessFeedbackForBinaryOperationERKNS1_14FeedbackSourceE@PLT
	movq	24(%rbx), %r13
	testb	$1, 96(%rbx)
	movq	936(%r13), %r14
	movq	944(%r13), %r12
	je	.L3260
	cmpl	$-1, 100(%rbx)
	jne	.L3260
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L3260
	cmpq	%r12, %r14
	je	.L3255
	movq	%r14, %rbx
	.p2align 4,,10
	.p2align 3
.L3261:
	movq	%rbx, %rdi
	addq	$224, %rbx
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	%r12, %rbx
	jne	.L3261
	movq	%r14, 944(%r13)
	.p2align 4,,10
	.p2align 3
.L3255:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3270
	addq	$256, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3260:
	.cfi_restore_state
	movl	252(%r13), %eax
	addl	248(%r13), %eax
	movslq	%eax, %rdx
	js	.L3262
	movabsq	$7905747460161236407, %rax
	subq	%r14, %r12
	sarq	$5, %r12
	imulq	%rax, %r12
	cmpq	%r12, %rdx
	jb	.L3262
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3262:
	leaq	0(,%rdx,8), %rdi
	subq	%rdx, %rdi
	salq	$5, %rdi
	addq	%r14, %rdi
	call	_ZN2v88internal8compiler5Hints5ClearEv
	jmp	.L3255
.L3270:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25633:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation8VisitModEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation8VisitModEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitModSmiEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitModSmiEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitModSmiEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitModSmiEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25635:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movl	$1, %esi
	subq	$256, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	cmpl	$-1, %eax
	je	.L3271
	movq	24(%rbx), %rsi
	movl	%eax, %r12d
	leaq	-272(%rbp), %r13
	leaq	-288(%rbp), %r14
	movq	%r13, %rdi
	movq	8(%rsi), %rax
	addq	$24, %rsi
	movq	%rax, -288(%rbp)
	movq	-8(%rsi), %rax
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	$0, -280(%rbp)
	je	.L3271
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler34SerializerForBackgroundCompilation15feedback_vectorEv
	movl	%r12d, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS0_6HandleINS0_14FeedbackVectorEEENS0_12FeedbackSlotE@PLT
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler12JSHeapBroker33ProcessFeedbackForBinaryOperationERKNS1_14FeedbackSourceE@PLT
	movq	24(%rbx), %r13
	testb	$1, 96(%rbx)
	movq	936(%r13), %r14
	movq	944(%r13), %r12
	je	.L3276
	cmpl	$-1, 100(%rbx)
	jne	.L3276
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L3276
	cmpq	%r12, %r14
	je	.L3271
	movq	%r14, %rbx
	.p2align 4,,10
	.p2align 3
.L3277:
	movq	%rbx, %rdi
	addq	$224, %rbx
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	%r12, %rbx
	jne	.L3277
	movq	%r14, 944(%r13)
	.p2align 4,,10
	.p2align 3
.L3271:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3286
	addq	$256, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3276:
	.cfi_restore_state
	movl	252(%r13), %eax
	addl	248(%r13), %eax
	movslq	%eax, %rdx
	js	.L3278
	movabsq	$7905747460161236407, %rax
	subq	%r14, %r12
	sarq	$5, %r12
	imulq	%rax, %r12
	cmpq	%r12, %rdx
	jb	.L3278
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3278:
	leaq	0(,%rdx,8), %rdi
	subq	%rdx, %rdi
	salq	$5, %rdi
	addq	%r14, %rdi
	call	_ZN2v88internal8compiler5Hints5ClearEv
	jmp	.L3271
.L3286:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25635:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitModSmiEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitModSmiEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation8VisitIncEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation8VisitIncEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation8VisitIncEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation8VisitIncEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25721:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	xorl	%esi, %esi
	subq	$256, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	cmpl	$-1, %eax
	je	.L3287
	movq	24(%rbx), %rsi
	movl	%eax, %r12d
	leaq	-272(%rbp), %r13
	leaq	-288(%rbp), %r14
	movq	%r13, %rdi
	movq	8(%rsi), %rax
	addq	$24, %rsi
	movq	%rax, -288(%rbp)
	movq	-8(%rsi), %rax
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	$0, -280(%rbp)
	je	.L3287
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler34SerializerForBackgroundCompilation15feedback_vectorEv
	movl	%r12d, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS0_6HandleINS0_14FeedbackVectorEEENS0_12FeedbackSlotE@PLT
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler12JSHeapBroker33ProcessFeedbackForBinaryOperationERKNS1_14FeedbackSourceE@PLT
	movq	24(%rbx), %r13
	testb	$1, 96(%rbx)
	movq	936(%r13), %r14
	movq	944(%r13), %r12
	je	.L3292
	cmpl	$-1, 100(%rbx)
	jne	.L3292
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L3292
	cmpq	%r12, %r14
	je	.L3287
	movq	%r14, %rbx
	.p2align 4,,10
	.p2align 3
.L3293:
	movq	%rbx, %rdi
	addq	$224, %rbx
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	%r12, %rbx
	jne	.L3293
	movq	%r14, 944(%r13)
	.p2align 4,,10
	.p2align 3
.L3287:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3302
	addq	$256, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3292:
	.cfi_restore_state
	movl	252(%r13), %eax
	addl	248(%r13), %eax
	movslq	%eax, %rdx
	js	.L3294
	movabsq	$7905747460161236407, %rax
	subq	%r14, %r12
	sarq	$5, %r12
	imulq	%rax, %r12
	cmpq	%r12, %rdx
	jb	.L3294
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3294:
	leaq	0(,%rdx,8), %rdi
	subq	%rdx, %rdi
	salq	$5, %rdi
	addq	%r14, %rdi
	call	_ZN2v88internal8compiler5Hints5ClearEv
	jmp	.L3287
.L3302:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25721:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation8VisitIncEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation8VisitIncEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation15VisitBitwiseAndEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation15VisitBitwiseAndEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation15VisitBitwiseAndEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation15VisitBitwiseAndEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25613:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movl	$1, %esi
	subq	$256, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	cmpl	$-1, %eax
	je	.L3303
	movq	24(%rbx), %rsi
	movl	%eax, %r12d
	leaq	-272(%rbp), %r13
	leaq	-288(%rbp), %r14
	movq	%r13, %rdi
	movq	8(%rsi), %rax
	addq	$24, %rsi
	movq	%rax, -288(%rbp)
	movq	-8(%rsi), %rax
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	$0, -280(%rbp)
	je	.L3303
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler34SerializerForBackgroundCompilation15feedback_vectorEv
	movl	%r12d, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS0_6HandleINS0_14FeedbackVectorEEENS0_12FeedbackSlotE@PLT
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler12JSHeapBroker33ProcessFeedbackForBinaryOperationERKNS1_14FeedbackSourceE@PLT
	movq	24(%rbx), %r13
	testb	$1, 96(%rbx)
	movq	936(%r13), %r14
	movq	944(%r13), %r12
	je	.L3308
	cmpl	$-1, 100(%rbx)
	jne	.L3308
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L3308
	cmpq	%r12, %r14
	je	.L3303
	movq	%r14, %rbx
	.p2align 4,,10
	.p2align 3
.L3309:
	movq	%rbx, %rdi
	addq	$224, %rbx
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	%r12, %rbx
	jne	.L3309
	movq	%r14, 944(%r13)
	.p2align 4,,10
	.p2align 3
.L3303:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3318
	addq	$256, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3308:
	.cfi_restore_state
	movl	252(%r13), %eax
	addl	248(%r13), %eax
	movslq	%eax, %rdx
	js	.L3310
	movabsq	$7905747460161236407, %rax
	subq	%r14, %r12
	sarq	$5, %r12
	imulq	%rax, %r12
	cmpq	%r12, %rdx
	jb	.L3310
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3310:
	leaq	0(,%rdx,8), %rdi
	subq	%rdx, %rdi
	salq	$5, %rdi
	addq	%r14, %rdi
	call	_ZN2v88internal8compiler5Hints5ClearEv
	jmp	.L3303
.L3318:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25613:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation15VisitBitwiseAndEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation15VisitBitwiseAndEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitSubSmiEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitSubSmiEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitSubSmiEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitSubSmiEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25655:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movl	$1, %esi
	subq	$256, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	cmpl	$-1, %eax
	je	.L3319
	movq	24(%rbx), %rsi
	movl	%eax, %r12d
	leaq	-272(%rbp), %r13
	leaq	-288(%rbp), %r14
	movq	%r13, %rdi
	movq	8(%rsi), %rax
	addq	$24, %rsi
	movq	%rax, -288(%rbp)
	movq	-8(%rsi), %rax
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	$0, -280(%rbp)
	je	.L3319
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler34SerializerForBackgroundCompilation15feedback_vectorEv
	movl	%r12d, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS0_6HandleINS0_14FeedbackVectorEEENS0_12FeedbackSlotE@PLT
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler12JSHeapBroker33ProcessFeedbackForBinaryOperationERKNS1_14FeedbackSourceE@PLT
	movq	24(%rbx), %r13
	testb	$1, 96(%rbx)
	movq	936(%r13), %r14
	movq	944(%r13), %r12
	je	.L3324
	cmpl	$-1, 100(%rbx)
	jne	.L3324
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L3324
	cmpq	%r12, %r14
	je	.L3319
	movq	%r14, %rbx
	.p2align 4,,10
	.p2align 3
.L3325:
	movq	%rbx, %rdi
	addq	$224, %rbx
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	%r12, %rbx
	jne	.L3325
	movq	%r14, 944(%r13)
	.p2align 4,,10
	.p2align 3
.L3319:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3334
	addq	$256, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3324:
	.cfi_restore_state
	movl	252(%r13), %eax
	addl	248(%r13), %eax
	movslq	%eax, %rdx
	js	.L3326
	movabsq	$7905747460161236407, %rax
	subq	%r14, %r12
	sarq	$5, %r12
	imulq	%rax, %r12
	cmpq	%r12, %rdx
	jb	.L3326
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3326:
	leaq	0(,%rdx,8), %rdi
	subq	%rdx, %rdi
	salq	$5, %rdi
	addq	%r14, %rdi
	call	_ZN2v88internal8compiler5Hints5ClearEv
	jmp	.L3319
.L3334:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25655:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitSubSmiEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitSubSmiEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation17VisitShiftLeftSmiEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation17VisitShiftLeftSmiEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation17VisitShiftLeftSmiEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation17VisitShiftLeftSmiEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25643:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movl	$1, %esi
	subq	$256, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	cmpl	$-1, %eax
	je	.L3335
	movq	24(%rbx), %rsi
	movl	%eax, %r12d
	leaq	-272(%rbp), %r13
	leaq	-288(%rbp), %r14
	movq	%r13, %rdi
	movq	8(%rsi), %rax
	addq	$24, %rsi
	movq	%rax, -288(%rbp)
	movq	-8(%rsi), %rax
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	$0, -280(%rbp)
	je	.L3335
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler34SerializerForBackgroundCompilation15feedback_vectorEv
	movl	%r12d, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS0_6HandleINS0_14FeedbackVectorEEENS0_12FeedbackSlotE@PLT
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler12JSHeapBroker33ProcessFeedbackForBinaryOperationERKNS1_14FeedbackSourceE@PLT
	movq	24(%rbx), %r13
	testb	$1, 96(%rbx)
	movq	936(%r13), %r14
	movq	944(%r13), %r12
	je	.L3340
	cmpl	$-1, 100(%rbx)
	jne	.L3340
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L3340
	cmpq	%r12, %r14
	je	.L3335
	movq	%r14, %rbx
	.p2align 4,,10
	.p2align 3
.L3341:
	movq	%rbx, %rdi
	addq	$224, %rbx
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	%r12, %rbx
	jne	.L3341
	movq	%r14, 944(%r13)
	.p2align 4,,10
	.p2align 3
.L3335:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3350
	addq	$256, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3340:
	.cfi_restore_state
	movl	252(%r13), %eax
	addl	248(%r13), %eax
	movslq	%eax, %rdx
	js	.L3342
	movabsq	$7905747460161236407, %rax
	subq	%r14, %r12
	sarq	$5, %r12
	imulq	%rax, %r12
	cmpq	%r12, %rdx
	jb	.L3342
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3342:
	leaq	0(,%rdx,8), %rdi
	subq	%rdx, %rdi
	salq	$5, %rdi
	addq	%r14, %rdi
	call	_ZN2v88internal8compiler5Hints5ClearEv
	jmp	.L3335
.L3350:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25643:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation17VisitShiftLeftSmiEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation17VisitShiftLeftSmiEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation8VisitMulEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation8VisitMulEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation8VisitMulEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation8VisitMulEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25637:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movl	$1, %esi
	subq	$256, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	cmpl	$-1, %eax
	je	.L3351
	movq	24(%rbx), %rsi
	movl	%eax, %r12d
	leaq	-272(%rbp), %r13
	leaq	-288(%rbp), %r14
	movq	%r13, %rdi
	movq	8(%rsi), %rax
	addq	$24, %rsi
	movq	%rax, -288(%rbp)
	movq	-8(%rsi), %rax
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	$0, -280(%rbp)
	je	.L3351
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler34SerializerForBackgroundCompilation15feedback_vectorEv
	movl	%r12d, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS0_6HandleINS0_14FeedbackVectorEEENS0_12FeedbackSlotE@PLT
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler12JSHeapBroker33ProcessFeedbackForBinaryOperationERKNS1_14FeedbackSourceE@PLT
	movq	24(%rbx), %r13
	testb	$1, 96(%rbx)
	movq	936(%r13), %r14
	movq	944(%r13), %r12
	je	.L3356
	cmpl	$-1, 100(%rbx)
	jne	.L3356
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L3356
	cmpq	%r12, %r14
	je	.L3351
	movq	%r14, %rbx
	.p2align 4,,10
	.p2align 3
.L3357:
	movq	%rbx, %rdi
	addq	$224, %rbx
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	%r12, %rbx
	jne	.L3357
	movq	%r14, 944(%r13)
	.p2align 4,,10
	.p2align 3
.L3351:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3366
	addq	$256, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3356:
	.cfi_restore_state
	movl	252(%r13), %eax
	addl	248(%r13), %eax
	movslq	%eax, %rdx
	js	.L3358
	movabsq	$7905747460161236407, %rax
	subq	%r14, %r12
	sarq	$5, %r12
	imulq	%rax, %r12
	cmpq	%r12, %rdx
	jb	.L3358
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3358:
	leaq	0(,%rdx,8), %rdi
	subq	%rdx, %rdi
	salq	$5, %rdi
	addq	%r14, %rdi
	call	_ZN2v88internal8compiler5Hints5ClearEv
	jmp	.L3351
.L3366:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25637:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation8VisitMulEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation8VisitMulEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation25VisitShiftRightLogicalSmiEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation25VisitShiftRightLogicalSmiEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation25VisitShiftRightLogicalSmiEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation25VisitShiftRightLogicalSmiEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25651:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movl	$1, %esi
	subq	$256, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	cmpl	$-1, %eax
	je	.L3367
	movq	24(%rbx), %rsi
	movl	%eax, %r12d
	leaq	-272(%rbp), %r13
	leaq	-288(%rbp), %r14
	movq	%r13, %rdi
	movq	8(%rsi), %rax
	addq	$24, %rsi
	movq	%rax, -288(%rbp)
	movq	-8(%rsi), %rax
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	$0, -280(%rbp)
	je	.L3367
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler34SerializerForBackgroundCompilation15feedback_vectorEv
	movl	%r12d, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS0_6HandleINS0_14FeedbackVectorEEENS0_12FeedbackSlotE@PLT
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler12JSHeapBroker33ProcessFeedbackForBinaryOperationERKNS1_14FeedbackSourceE@PLT
	movq	24(%rbx), %r13
	testb	$1, 96(%rbx)
	movq	936(%r13), %r14
	movq	944(%r13), %r12
	je	.L3372
	cmpl	$-1, 100(%rbx)
	jne	.L3372
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L3372
	cmpq	%r12, %r14
	je	.L3367
	movq	%r14, %rbx
	.p2align 4,,10
	.p2align 3
.L3373:
	movq	%rbx, %rdi
	addq	$224, %rbx
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	%r12, %rbx
	jne	.L3373
	movq	%r14, 944(%r13)
	.p2align 4,,10
	.p2align 3
.L3367:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3382
	addq	$256, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3372:
	.cfi_restore_state
	movl	252(%r13), %eax
	addl	248(%r13), %eax
	movslq	%eax, %rdx
	js	.L3374
	movabsq	$7905747460161236407, %rax
	subq	%r14, %r12
	sarq	$5, %r12
	imulq	%rax, %r12
	cmpq	%r12, %rdx
	jb	.L3374
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3374:
	leaq	0(,%rdx,8), %rdi
	subq	%rdx, %rdi
	salq	$5, %rdi
	addq	%r14, %rdi
	call	_ZN2v88internal8compiler5Hints5ClearEv
	jmp	.L3367
.L3382:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25651:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation25VisitShiftRightLogicalSmiEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation25VisitShiftRightLogicalSmiEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitBitwiseAndSmiEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitBitwiseAndSmiEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitBitwiseAndSmiEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitBitwiseAndSmiEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25615:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movl	$1, %esi
	subq	$256, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	cmpl	$-1, %eax
	je	.L3383
	movq	24(%rbx), %rsi
	movl	%eax, %r12d
	leaq	-272(%rbp), %r13
	leaq	-288(%rbp), %r14
	movq	%r13, %rdi
	movq	8(%rsi), %rax
	addq	$24, %rsi
	movq	%rax, -288(%rbp)
	movq	-8(%rsi), %rax
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	$0, -280(%rbp)
	je	.L3383
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler34SerializerForBackgroundCompilation15feedback_vectorEv
	movl	%r12d, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS0_6HandleINS0_14FeedbackVectorEEENS0_12FeedbackSlotE@PLT
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler12JSHeapBroker33ProcessFeedbackForBinaryOperationERKNS1_14FeedbackSourceE@PLT
	movq	24(%rbx), %r13
	testb	$1, 96(%rbx)
	movq	936(%r13), %r14
	movq	944(%r13), %r12
	je	.L3388
	cmpl	$-1, 100(%rbx)
	jne	.L3388
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L3388
	cmpq	%r12, %r14
	je	.L3383
	movq	%r14, %rbx
	.p2align 4,,10
	.p2align 3
.L3389:
	movq	%rbx, %rdi
	addq	$224, %rbx
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	%r12, %rbx
	jne	.L3389
	movq	%r14, 944(%r13)
	.p2align 4,,10
	.p2align 3
.L3383:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3398
	addq	$256, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3388:
	.cfi_restore_state
	movl	252(%r13), %eax
	addl	248(%r13), %eax
	movslq	%eax, %rdx
	js	.L3390
	movabsq	$7905747460161236407, %rax
	subq	%r14, %r12
	sarq	$5, %r12
	imulq	%rax, %r12
	cmpq	%r12, %rdx
	jb	.L3390
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3390:
	leaq	0(,%rdx,8), %rdi
	subq	%rdx, %rdi
	salq	$5, %rdi
	addq	%r14, %rdi
	call	_ZN2v88internal8compiler5Hints5ClearEv
	jmp	.L3383
.L3398:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25615:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitBitwiseAndSmiEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitBitwiseAndSmiEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation14VisitShiftLeftEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation14VisitShiftLeftEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation14VisitShiftLeftEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation14VisitShiftLeftEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25641:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movl	$1, %esi
	subq	$256, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	cmpl	$-1, %eax
	je	.L3399
	movq	24(%rbx), %rsi
	movl	%eax, %r12d
	leaq	-272(%rbp), %r13
	leaq	-288(%rbp), %r14
	movq	%r13, %rdi
	movq	8(%rsi), %rax
	addq	$24, %rsi
	movq	%rax, -288(%rbp)
	movq	-8(%rsi), %rax
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	$0, -280(%rbp)
	je	.L3399
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler34SerializerForBackgroundCompilation15feedback_vectorEv
	movl	%r12d, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS0_6HandleINS0_14FeedbackVectorEEENS0_12FeedbackSlotE@PLT
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler12JSHeapBroker33ProcessFeedbackForBinaryOperationERKNS1_14FeedbackSourceE@PLT
	movq	24(%rbx), %r13
	testb	$1, 96(%rbx)
	movq	936(%r13), %r14
	movq	944(%r13), %r12
	je	.L3404
	cmpl	$-1, 100(%rbx)
	jne	.L3404
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L3404
	cmpq	%r12, %r14
	je	.L3399
	movq	%r14, %rbx
	.p2align 4,,10
	.p2align 3
.L3405:
	movq	%rbx, %rdi
	addq	$224, %rbx
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	%r12, %rbx
	jne	.L3405
	movq	%r14, 944(%r13)
	.p2align 4,,10
	.p2align 3
.L3399:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3414
	addq	$256, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3404:
	.cfi_restore_state
	movl	252(%r13), %eax
	addl	248(%r13), %eax
	movslq	%eax, %rdx
	js	.L3406
	movabsq	$7905747460161236407, %rax
	subq	%r14, %r12
	sarq	$5, %r12
	imulq	%rax, %r12
	cmpq	%r12, %rdx
	jb	.L3406
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3406:
	leaq	0(,%rdx,8), %rdi
	subq	%rdx, %rdi
	salq	$5, %rdi
	addq	%r14, %rdi
	call	_ZN2v88internal8compiler5Hints5ClearEv
	jmp	.L3399
.L3414:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25641:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation14VisitShiftLeftEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation14VisitShiftLeftEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitExpSmiEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitExpSmiEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitExpSmiEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitExpSmiEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25631:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movl	$1, %esi
	subq	$256, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	cmpl	$-1, %eax
	je	.L3415
	movq	24(%rbx), %rsi
	movl	%eax, %r12d
	leaq	-272(%rbp), %r13
	leaq	-288(%rbp), %r14
	movq	%r13, %rdi
	movq	8(%rsi), %rax
	addq	$24, %rsi
	movq	%rax, -288(%rbp)
	movq	-8(%rsi), %rax
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	$0, -280(%rbp)
	je	.L3415
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler34SerializerForBackgroundCompilation15feedback_vectorEv
	movl	%r12d, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS0_6HandleINS0_14FeedbackVectorEEENS0_12FeedbackSlotE@PLT
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler12JSHeapBroker33ProcessFeedbackForBinaryOperationERKNS1_14FeedbackSourceE@PLT
	movq	24(%rbx), %r13
	testb	$1, 96(%rbx)
	movq	936(%r13), %r14
	movq	944(%r13), %r12
	je	.L3420
	cmpl	$-1, 100(%rbx)
	jne	.L3420
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L3420
	cmpq	%r12, %r14
	je	.L3415
	movq	%r14, %rbx
	.p2align 4,,10
	.p2align 3
.L3421:
	movq	%rbx, %rdi
	addq	$224, %rbx
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	%r12, %rbx
	jne	.L3421
	movq	%r14, 944(%r13)
	.p2align 4,,10
	.p2align 3
.L3415:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3430
	addq	$256, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3420:
	.cfi_restore_state
	movl	252(%r13), %eax
	addl	248(%r13), %eax
	movslq	%eax, %rdx
	js	.L3422
	movabsq	$7905747460161236407, %rax
	subq	%r14, %r12
	sarq	$5, %r12
	imulq	%rax, %r12
	cmpq	%r12, %rdx
	jb	.L3422
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3422:
	leaq	0(,%rdx,8), %rdi
	subq	%rdx, %rdi
	salq	$5, %rdi
	addq	%r14, %rdi
	call	_ZN2v88internal8compiler5Hints5ClearEv
	jmp	.L3415
.L3430:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25631:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitExpSmiEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitExpSmiEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation8VisitDivEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation8VisitDivEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation8VisitDivEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation8VisitDivEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25625:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movl	$1, %esi
	subq	$256, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	cmpl	$-1, %eax
	je	.L3431
	movq	24(%rbx), %rsi
	movl	%eax, %r12d
	leaq	-272(%rbp), %r13
	leaq	-288(%rbp), %r14
	movq	%r13, %rdi
	movq	8(%rsi), %rax
	addq	$24, %rsi
	movq	%rax, -288(%rbp)
	movq	-8(%rsi), %rax
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	$0, -280(%rbp)
	je	.L3431
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler34SerializerForBackgroundCompilation15feedback_vectorEv
	movl	%r12d, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS0_6HandleINS0_14FeedbackVectorEEENS0_12FeedbackSlotE@PLT
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler12JSHeapBroker33ProcessFeedbackForBinaryOperationERKNS1_14FeedbackSourceE@PLT
	movq	24(%rbx), %r13
	testb	$1, 96(%rbx)
	movq	936(%r13), %r14
	movq	944(%r13), %r12
	je	.L3436
	cmpl	$-1, 100(%rbx)
	jne	.L3436
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L3436
	cmpq	%r12, %r14
	je	.L3431
	movq	%r14, %rbx
	.p2align 4,,10
	.p2align 3
.L3437:
	movq	%rbx, %rdi
	addq	$224, %rbx
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	%r12, %rbx
	jne	.L3437
	movq	%r14, 944(%r13)
	.p2align 4,,10
	.p2align 3
.L3431:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3446
	addq	$256, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3436:
	.cfi_restore_state
	movl	252(%r13), %eax
	addl	248(%r13), %eax
	movslq	%eax, %rdx
	js	.L3438
	movabsq	$7905747460161236407, %rax
	subq	%r14, %r12
	sarq	$5, %r12
	imulq	%rax, %r12
	cmpq	%r12, %rdx
	jb	.L3438
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3438:
	leaq	0(,%rdx,8), %rdi
	subq	%rdx, %rdi
	salq	$5, %rdi
	addq	%r14, %rdi
	call	_ZN2v88internal8compiler5Hints5ClearEv
	jmp	.L3431
.L3446:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25625:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation8VisitDivEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation8VisitDivEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation14VisitBitwiseOrEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation14VisitBitwiseOrEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation14VisitBitwiseOrEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation14VisitBitwiseOrEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25617:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movl	$1, %esi
	subq	$256, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	cmpl	$-1, %eax
	je	.L3447
	movq	24(%rbx), %rsi
	movl	%eax, %r12d
	leaq	-272(%rbp), %r13
	leaq	-288(%rbp), %r14
	movq	%r13, %rdi
	movq	8(%rsi), %rax
	addq	$24, %rsi
	movq	%rax, -288(%rbp)
	movq	-8(%rsi), %rax
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	$0, -280(%rbp)
	je	.L3447
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler34SerializerForBackgroundCompilation15feedback_vectorEv
	movl	%r12d, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS0_6HandleINS0_14FeedbackVectorEEENS0_12FeedbackSlotE@PLT
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler12JSHeapBroker33ProcessFeedbackForBinaryOperationERKNS1_14FeedbackSourceE@PLT
	movq	24(%rbx), %r13
	testb	$1, 96(%rbx)
	movq	936(%r13), %r14
	movq	944(%r13), %r12
	je	.L3452
	cmpl	$-1, 100(%rbx)
	jne	.L3452
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L3452
	cmpq	%r12, %r14
	je	.L3447
	movq	%r14, %rbx
	.p2align 4,,10
	.p2align 3
.L3453:
	movq	%rbx, %rdi
	addq	$224, %rbx
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	%r12, %rbx
	jne	.L3453
	movq	%r14, 944(%r13)
	.p2align 4,,10
	.p2align 3
.L3447:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3462
	addq	$256, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3452:
	.cfi_restore_state
	movl	252(%r13), %eax
	addl	248(%r13), %eax
	movslq	%eax, %rdx
	js	.L3454
	movabsq	$7905747460161236407, %rax
	subq	%r14, %r12
	sarq	$5, %r12
	imulq	%rax, %r12
	cmpq	%r12, %rdx
	jb	.L3454
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3454:
	leaq	0(,%rdx,8), %rdi
	subq	%rdx, %rdi
	salq	$5, %rdi
	addq	%r14, %rdi
	call	_ZN2v88internal8compiler5Hints5ClearEv
	jmp	.L3447
.L3462:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25617:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation14VisitBitwiseOrEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation14VisitBitwiseOrEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation17VisitBitwiseOrSmiEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation17VisitBitwiseOrSmiEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation17VisitBitwiseOrSmiEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation17VisitBitwiseOrSmiEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25619:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movl	$1, %esi
	subq	$256, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	cmpl	$-1, %eax
	je	.L3463
	movq	24(%rbx), %rsi
	movl	%eax, %r12d
	leaq	-272(%rbp), %r13
	leaq	-288(%rbp), %r14
	movq	%r13, %rdi
	movq	8(%rsi), %rax
	addq	$24, %rsi
	movq	%rax, -288(%rbp)
	movq	-8(%rsi), %rax
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	$0, -280(%rbp)
	je	.L3463
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler34SerializerForBackgroundCompilation15feedback_vectorEv
	movl	%r12d, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS0_6HandleINS0_14FeedbackVectorEEENS0_12FeedbackSlotE@PLT
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler12JSHeapBroker33ProcessFeedbackForBinaryOperationERKNS1_14FeedbackSourceE@PLT
	movq	24(%rbx), %r13
	testb	$1, 96(%rbx)
	movq	936(%r13), %r14
	movq	944(%r13), %r12
	je	.L3468
	cmpl	$-1, 100(%rbx)
	jne	.L3468
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L3468
	cmpq	%r12, %r14
	je	.L3463
	movq	%r14, %rbx
	.p2align 4,,10
	.p2align 3
.L3469:
	movq	%rbx, %rdi
	addq	$224, %rbx
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	%r12, %rbx
	jne	.L3469
	movq	%r14, 944(%r13)
	.p2align 4,,10
	.p2align 3
.L3463:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3478
	addq	$256, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3468:
	.cfi_restore_state
	movl	252(%r13), %eax
	addl	248(%r13), %eax
	movslq	%eax, %rdx
	js	.L3470
	movabsq	$7905747460161236407, %rax
	subq	%r14, %r12
	sarq	$5, %r12
	imulq	%rax, %r12
	cmpq	%r12, %rdx
	jb	.L3470
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3470:
	leaq	0(,%rdx,8), %rdi
	subq	%rdx, %rdi
	salq	$5, %rdi
	addq	%r14, %rdi
	call	_ZN2v88internal8compiler5Hints5ClearEv
	jmp	.L3463
.L3478:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25619:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation17VisitBitwiseOrSmiEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation17VisitBitwiseOrSmiEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation8VisitExpEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation8VisitExpEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation8VisitExpEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation8VisitExpEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25629:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movl	$1, %esi
	subq	$256, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	cmpl	$-1, %eax
	je	.L3479
	movq	24(%rbx), %rsi
	movl	%eax, %r12d
	leaq	-272(%rbp), %r13
	leaq	-288(%rbp), %r14
	movq	%r13, %rdi
	movq	8(%rsi), %rax
	addq	$24, %rsi
	movq	%rax, -288(%rbp)
	movq	-8(%rsi), %rax
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	$0, -280(%rbp)
	je	.L3479
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler34SerializerForBackgroundCompilation15feedback_vectorEv
	movl	%r12d, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS0_6HandleINS0_14FeedbackVectorEEENS0_12FeedbackSlotE@PLT
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler12JSHeapBroker33ProcessFeedbackForBinaryOperationERKNS1_14FeedbackSourceE@PLT
	movq	24(%rbx), %r13
	testb	$1, 96(%rbx)
	movq	936(%r13), %r14
	movq	944(%r13), %r12
	je	.L3484
	cmpl	$-1, 100(%rbx)
	jne	.L3484
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L3484
	cmpq	%r12, %r14
	je	.L3479
	movq	%r14, %rbx
	.p2align 4,,10
	.p2align 3
.L3485:
	movq	%rbx, %rdi
	addq	$224, %rbx
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	%r12, %rbx
	jne	.L3485
	movq	%r14, 944(%r13)
	.p2align 4,,10
	.p2align 3
.L3479:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3494
	addq	$256, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3484:
	.cfi_restore_state
	movl	252(%r13), %eax
	addl	248(%r13), %eax
	movslq	%eax, %rdx
	js	.L3486
	movabsq	$7905747460161236407, %rax
	subq	%r14, %r12
	sarq	$5, %r12
	imulq	%rax, %r12
	cmpq	%r12, %rdx
	jb	.L3486
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3486:
	leaq	0(,%rdx,8), %rdi
	subq	%rdx, %rdi
	salq	$5, %rdi
	addq	%r14, %rdi
	call	_ZN2v88internal8compiler5Hints5ClearEv
	jmp	.L3479
.L3494:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25629:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation8VisitExpEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation8VisitExpEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitAddSmiEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitAddSmiEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitAddSmiEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitAddSmiEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25611:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movl	$1, %esi
	subq	$256, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	cmpl	$-1, %eax
	je	.L3495
	movq	24(%rbx), %rsi
	movl	%eax, %r12d
	leaq	-272(%rbp), %r13
	leaq	-288(%rbp), %r14
	movq	%r13, %rdi
	movq	8(%rsi), %rax
	addq	$24, %rsi
	movq	%rax, -288(%rbp)
	movq	-8(%rsi), %rax
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	$0, -280(%rbp)
	je	.L3495
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler34SerializerForBackgroundCompilation15feedback_vectorEv
	movl	%r12d, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS0_6HandleINS0_14FeedbackVectorEEENS0_12FeedbackSlotE@PLT
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler12JSHeapBroker33ProcessFeedbackForBinaryOperationERKNS1_14FeedbackSourceE@PLT
	movq	24(%rbx), %r13
	testb	$1, 96(%rbx)
	movq	936(%r13), %r14
	movq	944(%r13), %r12
	je	.L3500
	cmpl	$-1, 100(%rbx)
	jne	.L3500
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L3500
	cmpq	%r12, %r14
	je	.L3495
	movq	%r14, %rbx
	.p2align 4,,10
	.p2align 3
.L3501:
	movq	%rbx, %rdi
	addq	$224, %rbx
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	%r12, %rbx
	jne	.L3501
	movq	%r14, 944(%r13)
	.p2align 4,,10
	.p2align 3
.L3495:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3510
	addq	$256, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3500:
	.cfi_restore_state
	movl	252(%r13), %eax
	addl	248(%r13), %eax
	movslq	%eax, %rdx
	js	.L3502
	movabsq	$7905747460161236407, %rax
	subq	%r14, %r12
	sarq	$5, %r12
	imulq	%rax, %r12
	cmpq	%r12, %rdx
	jb	.L3502
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3502:
	leaq	0(,%rdx,8), %rdi
	subq	%rdx, %rdi
	salq	$5, %rdi
	addq	%r14, %rdi
	call	_ZN2v88internal8compiler5Hints5ClearEv
	jmp	.L3495
.L3510:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25611:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitAddSmiEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitAddSmiEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitNegateEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitNegateEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitNegateEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitNegateEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB25723:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	xorl	%esi, %esi
	subq	$256, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	cmpl	$-1, %eax
	je	.L3511
	movq	24(%rbx), %rsi
	movl	%eax, %r12d
	leaq	-272(%rbp), %r13
	leaq	-288(%rbp), %r14
	movq	%r13, %rdi
	movq	8(%rsi), %rax
	addq	$24, %rsi
	movq	%rax, -288(%rbp)
	movq	-8(%rsi), %rax
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	$0, -280(%rbp)
	je	.L3511
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler34SerializerForBackgroundCompilation15feedback_vectorEv
	movl	%r12d, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS0_6HandleINS0_14FeedbackVectorEEENS0_12FeedbackSlotE@PLT
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler12JSHeapBroker33ProcessFeedbackForBinaryOperationERKNS1_14FeedbackSourceE@PLT
	movq	24(%rbx), %r13
	testb	$1, 96(%rbx)
	movq	936(%r13), %r14
	movq	944(%r13), %r12
	je	.L3516
	cmpl	$-1, 100(%rbx)
	jne	.L3516
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L3516
	cmpq	%r12, %r14
	je	.L3511
	movq	%r14, %rbx
	.p2align 4,,10
	.p2align 3
.L3517:
	movq	%rbx, %rdi
	addq	$224, %rbx
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	%r12, %rbx
	jne	.L3517
	movq	%r14, 944(%r13)
	.p2align 4,,10
	.p2align 3
.L3511:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3526
	addq	$256, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3516:
	.cfi_restore_state
	movl	252(%r13), %eax
	addl	248(%r13), %eax
	movslq	%eax, %rdx
	js	.L3518
	movabsq	$7905747460161236407, %rax
	subq	%r14, %r12
	sarq	$5, %r12
	imulq	%rax, %r12
	cmpq	%r12, %rdx
	jb	.L3518
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3518:
	leaq	0(,%rdx,8), %rdi
	subq	%rdx, %rdi
	salq	$5, %rdi
	addq	%r14, %rdi
	call	_ZN2v88internal8compiler5Hints5ClearEv
	jmp	.L3511
.L3526:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25723:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitNegateEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitNegateEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation8VisitAddEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation8VisitAddEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation8VisitAddEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation8VisitAddEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB19167:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movl	$1, %esi
	subq	$256, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	cmpl	$-1, %eax
	je	.L3527
	movq	24(%rbx), %rsi
	movl	%eax, %r12d
	leaq	-272(%rbp), %r13
	leaq	-288(%rbp), %r14
	movq	%r13, %rdi
	movq	8(%rsi), %rax
	addq	$24, %rsi
	movq	%rax, -288(%rbp)
	movq	-8(%rsi), %rax
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	$0, -280(%rbp)
	je	.L3527
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler34SerializerForBackgroundCompilation15feedback_vectorEv
	movl	%r12d, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS0_6HandleINS0_14FeedbackVectorEEENS0_12FeedbackSlotE@PLT
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler12JSHeapBroker33ProcessFeedbackForBinaryOperationERKNS1_14FeedbackSourceE@PLT
	movq	24(%rbx), %r13
	testb	$1, 96(%rbx)
	movq	936(%r13), %r14
	movq	944(%r13), %r12
	je	.L3532
	cmpl	$-1, 100(%rbx)
	jne	.L3532
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L3532
	cmpq	%r12, %r14
	je	.L3527
	movq	%r14, %rbx
	.p2align 4,,10
	.p2align 3
.L3533:
	movq	%rbx, %rdi
	addq	$224, %rbx
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	%r12, %rbx
	jne	.L3533
	movq	%r14, 944(%r13)
	.p2align 4,,10
	.p2align 3
.L3527:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3542
	addq	$256, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3532:
	.cfi_restore_state
	movl	252(%r13), %eax
	addl	248(%r13), %eax
	movslq	%eax, %rdx
	js	.L3534
	movabsq	$7905747460161236407, %rax
	subq	%r14, %r12
	sarq	$5, %r12
	imulq	%rax, %r12
	cmpq	%r12, %rdx
	jb	.L3534
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3534:
	leaq	0(,%rdx,8), %rdi
	subq	%rdx, %rdi
	salq	$5, %rdi
	addq	%r14, %rdi
	call	_ZN2v88internal8compiler5Hints5ClearEv
	jmp	.L3527
.L3542:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19167:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation8VisitAddEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation8VisitAddEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation13VisitToNumberEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation13VisitToNumberEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation13VisitToNumberEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation13VisitToNumberEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB19101:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	xorl	%esi, %esi
	subq	$256, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	cmpl	$-1, %eax
	je	.L3543
	movq	24(%rbx), %rsi
	movl	%eax, %r12d
	leaq	-272(%rbp), %r13
	leaq	-288(%rbp), %r14
	movq	%r13, %rdi
	movq	8(%rsi), %rax
	addq	$24, %rsi
	movq	%rax, -288(%rbp)
	movq	-8(%rsi), %rax
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	$0, -280(%rbp)
	je	.L3543
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler34SerializerForBackgroundCompilation15feedback_vectorEv
	movl	%r12d, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS0_6HandleINS0_14FeedbackVectorEEENS0_12FeedbackSlotE@PLT
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler12JSHeapBroker33ProcessFeedbackForBinaryOperationERKNS1_14FeedbackSourceE@PLT
	movq	24(%rbx), %rdx
	movl	252(%rdx), %eax
	addl	248(%rdx), %eax
	movq	936(%rdx), %rcx
	cltq
	js	.L3548
	movq	944(%rdx), %rdx
	movabsq	$7905747460161236407, %rsi
	subq	%rcx, %rdx
	sarq	$5, %rdx
	imulq	%rsi, %rdx
	cmpq	%rax, %rdx
	jbe	.L3554
.L3548:
	leaq	0(,%rax,8), %rdi
	subq	%rax, %rdi
	salq	$5, %rdi
	addq	%rcx, %rdi
	call	_ZN2v88internal8compiler5Hints5ClearEv
.L3543:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3555
	addq	$256, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3554:
	.cfi_restore_state
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L3555:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19101:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation13VisitToNumberEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation13VisitToNumberEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation15VisitBitwiseNotEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation15VisitBitwiseNotEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation15VisitBitwiseNotEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation15VisitBitwiseNotEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB19197:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	xorl	%esi, %esi
	subq	$256, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	cmpl	$-1, %eax
	je	.L3556
	movq	24(%rbx), %rsi
	movl	%eax, %r12d
	leaq	-272(%rbp), %r13
	leaq	-288(%rbp), %r14
	movq	%r13, %rdi
	movq	8(%rsi), %rax
	addq	$24, %rsi
	movq	%rax, -288(%rbp)
	movq	-8(%rsi), %rax
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	$0, -280(%rbp)
	je	.L3556
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler34SerializerForBackgroundCompilation15feedback_vectorEv
	movl	%r12d, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS0_6HandleINS0_14FeedbackVectorEEENS0_12FeedbackSlotE@PLT
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler12JSHeapBroker33ProcessFeedbackForBinaryOperationERKNS1_14FeedbackSourceE@PLT
	movq	24(%rbx), %r13
	testb	$1, 96(%rbx)
	movq	936(%r13), %r14
	movq	944(%r13), %r12
	je	.L3561
	cmpl	$-1, 100(%rbx)
	jne	.L3561
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L3561
	cmpq	%r12, %r14
	je	.L3556
	movq	%r14, %rbx
	.p2align 4,,10
	.p2align 3
.L3562:
	movq	%rbx, %rdi
	addq	$224, %rbx
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	%r12, %rbx
	jne	.L3562
	movq	%r14, 944(%r13)
	.p2align 4,,10
	.p2align 3
.L3556:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3571
	addq	$256, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3561:
	.cfi_restore_state
	movl	252(%r13), %eax
	addl	248(%r13), %eax
	movslq	%eax, %rdx
	js	.L3563
	movabsq	$7905747460161236407, %rax
	subq	%r14, %r12
	sarq	$5, %r12
	imulq	%rax, %r12
	cmpq	%r12, %rdx
	jb	.L3563
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3563:
	leaq	0(,%rdx,8), %rdi
	subq	%rdx, %rdi
	salq	$5, %rdi
	addq	%r14, %rdi
	call	_ZN2v88internal8compiler5Hints5ClearEv
	jmp	.L3556
.L3571:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19197:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation15VisitBitwiseNotEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation15VisitBitwiseNotEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZNSt8_Rb_treeIN2v88internal8compiler14VirtualContextES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE7_M_copyIZNSA_14_M_move_assignERSA_St17integral_constantIbLb0EEEUlRKS3_E_EEPSt13_Rb_tree_nodeIS3_EPKSJ_PSt18_Rb_tree_node_baseRT_,"axG",@progbits,_ZNSt8_Rb_treeIN2v88internal8compiler14VirtualContextES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE7_M_copyIZNSA_14_M_move_assignERSA_St17integral_constantIbLb0EEEUlRKS3_E_EEPSt13_Rb_tree_nodeIS3_EPKSJ_PSt18_Rb_tree_node_baseRT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN2v88internal8compiler14VirtualContextES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE7_M_copyIZNSA_14_M_move_assignERSA_St17integral_constantIbLb0EEEUlRKS3_E_EEPSt13_Rb_tree_nodeIS3_EPKSJ_PSt18_Rb_tree_node_baseRT_
	.type	_ZNSt8_Rb_treeIN2v88internal8compiler14VirtualContextES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE7_M_copyIZNSA_14_M_move_assignERSA_St17integral_constantIbLb0EEEUlRKS3_E_EEPSt13_Rb_tree_nodeIS3_EPKSJ_PSt18_Rb_tree_node_baseRT_, @function
_ZNSt8_Rb_treeIN2v88internal8compiler14VirtualContextES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE7_M_copyIZNSA_14_M_move_assignERSA_St17integral_constantIbLb0EEEUlRKS3_E_EEPSt13_Rb_tree_nodeIS3_EPKSJ_PSt18_Rb_tree_node_baseRT_:
.LFB24979:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	(%rcx), %rcx
	movq	8(%rcx), %r14
	testq	%r14, %r14
	je	.L3573
	movq	8(%r14), %rax
	movq	%rax, 8(%rcx)
	testq	%rax, %rax
	je	.L3574
	cmpq	24(%rax), %r14
	je	.L3635
	movq	$0, 16(%rax)
.L3580:
	movq	40(%rbx), %rax
	movl	32(%rbx), %ecx
	movq	%rax, 40(%r14)
	movl	%ecx, 32(%r14)
	movl	(%rbx), %eax
	movq	$0, 16(%r14)
	movl	%eax, (%r14)
	movq	$0, 24(%r14)
	movq	%rdx, 8(%r14)
	movq	24(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L3581
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal8compiler14VirtualContextES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE7_M_copyIZNSA_14_M_move_assignERSA_St17integral_constantIbLb0EEEUlRKS3_E_EEPSt13_Rb_tree_nodeIS3_EPKSJ_PSt18_Rb_tree_node_baseRT_
	movq	%rax, 24(%r14)
.L3581:
	movq	16(%rbx), %r12
	testq	%r12, %r12
	je	.L3572
	movq	(%r15), %rdx
	movq	%r14, %rcx
	movq	8(%rdx), %rbx
	testq	%rbx, %rbx
	je	.L3583
.L3637:
	movq	8(%rbx), %rax
	movq	%rax, 8(%rdx)
	testq	%rax, %rax
	je	.L3584
	cmpq	24(%rax), %rbx
	je	.L3636
	movq	$0, 16(%rax)
.L3590:
	movq	40(%r12), %rax
	movl	32(%r12), %edx
	movq	%rax, 40(%rbx)
	movl	%edx, 32(%rbx)
	movl	(%r12), %eax
	movq	$0, 16(%rbx)
	movl	%eax, (%rbx)
	movq	$0, 24(%rbx)
	movq	%rbx, 16(%rcx)
	movq	%rcx, 8(%rbx)
	movq	24(%r12), %rsi
	testq	%rsi, %rsi
	je	.L3591
	movq	%r15, %rcx
	movq	%rbx, %rdx
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal8compiler14VirtualContextES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE7_M_copyIZNSA_14_M_move_assignERSA_St17integral_constantIbLb0EEEUlRKS3_E_EEPSt13_Rb_tree_nodeIS3_EPKSJ_PSt18_Rb_tree_node_baseRT_
	movq	%rax, 24(%rbx)
	movq	16(%r12), %r12
	testq	%r12, %r12
	je	.L3572
.L3592:
	movq	(%r15), %rdx
	movq	%rbx, %rcx
	movq	8(%rdx), %rbx
	testq	%rbx, %rbx
	jne	.L3637
.L3583:
	movq	16(%rdx), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	$47, %rax
	jbe	.L3638
	leaq	48(%rbx), %rax
	movq	%rax, 16(%rdi)
	jmp	.L3590
	.p2align 4,,10
	.p2align 3
.L3591:
	movq	16(%r12), %r12
	testq	%r12, %r12
	jne	.L3592
.L3572:
	addq	$24, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3584:
	.cfi_restore_state
	movq	$0, (%rdx)
	jmp	.L3590
	.p2align 4,,10
	.p2align 3
.L3636:
	movq	$0, 24(%rax)
	movq	8(%rdx), %rax
	movq	16(%rax), %rsi
	testq	%rsi, %rsi
	je	.L3590
	movq	%rsi, 8(%rdx)
	movq	24(%rsi), %rax
	testq	%rax, %rax
	je	.L3587
	.p2align 4,,10
	.p2align 3
.L3588:
	movq	%rax, 8(%rdx)
	movq	%rax, %rsi
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L3588
.L3587:
	movq	16(%rsi), %rax
	testq	%rax, %rax
	je	.L3590
	movq	%rax, 8(%rdx)
	jmp	.L3590
	.p2align 4,,10
	.p2align 3
.L3573:
	movq	16(%rcx), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %r14
	movq	24(%rdi), %rax
	subq	%r14, %rax
	cmpq	$47, %rax
	jbe	.L3639
	leaq	48(%r14), %rax
	movq	%rax, 16(%rdi)
	jmp	.L3580
	.p2align 4,,10
	.p2align 3
.L3574:
	movq	$0, (%rcx)
	jmp	.L3580
	.p2align 4,,10
	.p2align 3
.L3635:
	movq	$0, 24(%rax)
	movq	8(%rcx), %rax
	movq	16(%rax), %rsi
	testq	%rsi, %rsi
	je	.L3580
	movq	%rsi, 8(%rcx)
	movq	24(%rsi), %rax
	testq	%rax, %rax
	je	.L3577
	.p2align 4,,10
	.p2align 3
.L3578:
	movq	%rax, 8(%rcx)
	movq	%rax, %rsi
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L3578
.L3577:
	movq	16(%rsi), %rax
	testq	%rax, %rax
	je	.L3580
	movq	%rax, 8(%rcx)
	jmp	.L3580
	.p2align 4,,10
	.p2align 3
.L3638:
	movl	$48, %esi
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	movq	%rax, %rbx
	jmp	.L3590
.L3639:
	movl	$48, %esi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %r14
	jmp	.L3580
	.cfi_endproc
.LFE24979:
	.size	_ZNSt8_Rb_treeIN2v88internal8compiler14VirtualContextES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE7_M_copyIZNSA_14_M_move_assignERSA_St17integral_constantIbLb0EEEUlRKS3_E_EEPSt13_Rb_tree_nodeIS3_EPKSJ_PSt18_Rb_tree_node_baseRT_, .-_ZNSt8_Rb_treeIN2v88internal8compiler14VirtualContextES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE7_M_copyIZNSA_14_M_move_assignERSA_St17integral_constantIbLb0EEEUlRKS3_E_EEPSt13_Rb_tree_nodeIS3_EPKSJ_PSt18_Rb_tree_node_baseRT_
	.section	.text._ZNSt8_Rb_treeIN2v88internal8compiler14VirtualContextES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE14_M_move_assignERSA_St17integral_constantIbLb0EE,"axG",@progbits,_ZNSt8_Rb_treeIN2v88internal8compiler14VirtualContextES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE14_M_move_assignERSA_St17integral_constantIbLb0EE,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN2v88internal8compiler14VirtualContextES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE14_M_move_assignERSA_St17integral_constantIbLb0EE
	.type	_ZNSt8_Rb_treeIN2v88internal8compiler14VirtualContextES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE14_M_move_assignERSA_St17integral_constantIbLb0EE, @function
_ZNSt8_Rb_treeIN2v88internal8compiler14VirtualContextES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE14_M_move_assignERSA_St17integral_constantIbLb0EE:
.LFB24898:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	16(%rdi), %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	24(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	cmpq	%rax, (%rdi)
	je	.L3700
	movq	40(%rdi), %rax
	movq	%r12, -80(%rbp)
	movq	%rdi, -64(%rbp)
	movq	%rax, -72(%rbp)
	testq	%r12, %r12
	je	.L3648
	movq	$0, 8(%r12)
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L3649
	movq	%rax, -72(%rbp)
.L3649:
	movq	$0, 24(%rbx)
	movq	%r15, 32(%rbx)
	movq	%r15, 40(%rbx)
	movq	$0, 48(%rbx)
	movq	24(%r13), %rsi
	testq	%rsi, %rsi
	je	.L3701
.L3661:
	leaq	-80(%rbp), %rax
	movq	%r15, %rdx
	leaq	-88(%rbp), %rcx
	movq	%rbx, %rdi
	movq	%rax, -88(%rbp)
	call	_ZNSt8_Rb_treeIN2v88internal8compiler14VirtualContextES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE7_M_copyIZNSA_14_M_move_assignERSA_St17integral_constantIbLb0EEEUlRKS3_E_EEPSt13_Rb_tree_nodeIS3_EPKSJ_PSt18_Rb_tree_node_baseRT_
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L3651:
	movq	%rdx, %rcx
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L3651
	movq	%rcx, 32(%rbx)
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L3652:
	movq	%rdx, %rcx
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L3652
	movq	%rcx, 40(%rbx)
	movq	48(%r13), %rdx
	movq	%rax, 24(%rbx)
	movq	%rdx, 48(%rbx)
	movq	24(%r13), %r12
	testq	%r12, %r12
	je	.L3653
.L3656:
	movq	24(%r12), %rbx
	testq	%rbx, %rbx
	je	.L3654
.L3655:
	movq	24(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal8compiler14VirtualContextES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE8_M_eraseEPSt13_Rb_tree_nodeIS3_E
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L3655
.L3654:
	movq	16(%r12), %r12
	testq	%r12, %r12
	jne	.L3656
.L3653:
	movq	-80(%rbp), %r12
	leaq	16(%r13), %rax
	movq	$0, 24(%r13)
	movq	%rax, 32(%r13)
	movq	%rax, 40(%r13)
	movq	$0, 48(%r13)
	movq	-64(%rbp), %r13
	testq	%r12, %r12
	je	.L3640
	.p2align 4,,10
	.p2align 3
.L3659:
	movq	24(%r12), %rbx
	testq	%rbx, %rbx
	je	.L3657
.L3658:
	movq	24(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal8compiler14VirtualContextES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE8_M_eraseEPSt13_Rb_tree_nodeIS3_E
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L3658
.L3657:
	movq	16(%r12), %r12
	testq	%r12, %r12
	jne	.L3659
.L3640:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3702
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3643:
	.cfi_restore_state
	movq	16(%r12), %r12
	.p2align 4,,10
	.p2align 3
.L3700:
	testq	%r12, %r12
	je	.L3703
	movq	24(%r12), %r14
	testq	%r14, %r14
	je	.L3643
.L3644:
	movq	24(%r14), %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal8compiler14VirtualContextES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE8_M_eraseEPSt13_Rb_tree_nodeIS3_E
	movq	16(%r14), %r14
	testq	%r14, %r14
	jne	.L3644
	movq	16(%r12), %r12
	jmp	.L3700
	.p2align 4,,10
	.p2align 3
.L3701:
	movq	-64(%rbp), %r13
	jmp	.L3659
	.p2align 4,,10
	.p2align 3
.L3648:
	movq	%r15, 32(%rdi)
	movq	%r15, 40(%rdi)
	movq	$0, 48(%rdi)
	movq	24(%rsi), %rsi
	movq	$0, -72(%rbp)
	testq	%rsi, %rsi
	jne	.L3661
	jmp	.L3640
	.p2align 4,,10
	.p2align 3
.L3703:
	movq	$0, 24(%rbx)
	movq	%r15, 32(%rbx)
	movq	%r15, 40(%rbx)
	movq	$0, 48(%rbx)
	cmpq	$0, 24(%r13)
	je	.L3640
	movl	16(%r13), %eax
	movl	%eax, 16(%rbx)
	movq	24(%r13), %rax
	movq	%rax, 24(%rbx)
	movq	32(%r13), %rdx
	movq	%rdx, 32(%rbx)
	movq	40(%r13), %rdx
	movq	%rdx, 40(%rbx)
	movq	%r15, 8(%rax)
	movq	48(%r13), %rax
	movq	%rax, 48(%rbx)
	leaq	16(%r13), %rax
	movq	$0, 24(%r13)
	movq	%rax, 32(%r13)
	movq	%rax, 40(%r13)
	movq	$0, 48(%r13)
	jmp	.L3640
.L3702:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24898:
	.size	_ZNSt8_Rb_treeIN2v88internal8compiler14VirtualContextES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE14_M_move_assignERSA_St17integral_constantIbLb0EE, .-_ZNSt8_Rb_treeIN2v88internal8compiler14VirtualContextES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE14_M_move_assignERSA_St17integral_constantIbLb0EE
	.section	.text._ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE7_M_copyIZNSC_14_M_move_assignERSC_St17integral_constantIbLb0EEEUlRKS4_E_EEPSt13_Rb_tree_nodeIS4_EPKSL_PSt18_Rb_tree_node_baseRT_,"axG",@progbits,_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE7_M_copyIZNSC_14_M_move_assignERSC_St17integral_constantIbLb0EEEUlRKS4_E_EEPSt13_Rb_tree_nodeIS4_EPKSL_PSt18_Rb_tree_node_baseRT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE7_M_copyIZNSC_14_M_move_assignERSC_St17integral_constantIbLb0EEEUlRKS4_E_EEPSt13_Rb_tree_nodeIS4_EPKSL_PSt18_Rb_tree_node_baseRT_
	.type	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE7_M_copyIZNSC_14_M_move_assignERSC_St17integral_constantIbLb0EEEUlRKS4_E_EEPSt13_Rb_tree_nodeIS4_EPKSL_PSt18_Rb_tree_node_baseRT_, @function
_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE7_M_copyIZNSC_14_M_move_assignERSC_St17integral_constantIbLb0EEEUlRKS4_E_EEPSt13_Rb_tree_nodeIS4_EPKSL_PSt18_Rb_tree_node_baseRT_:
.LFB24984:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	(%rcx), %rcx
	movq	8(%rcx), %r14
	testq	%r14, %r14
	je	.L3705
	movq	8(%r14), %rax
	movq	%rax, 8(%rcx)
	testq	%rax, %rax
	je	.L3706
	cmpq	24(%rax), %r14
	je	.L3767
	movq	$0, 16(%rax)
.L3712:
	movq	32(%rbx), %rax
	movq	%rax, 32(%r14)
	movl	(%rbx), %eax
	movq	$0, 16(%r14)
	movl	%eax, (%r14)
	movq	$0, 24(%r14)
	movq	%rdx, 8(%r14)
	movq	24(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L3713
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE7_M_copyIZNSC_14_M_move_assignERSC_St17integral_constantIbLb0EEEUlRKS4_E_EEPSt13_Rb_tree_nodeIS4_EPKSL_PSt18_Rb_tree_node_baseRT_
	movq	%rax, 24(%r14)
.L3713:
	movq	16(%rbx), %r12
	testq	%r12, %r12
	je	.L3704
	movq	(%r15), %rdx
	movq	%r14, %rcx
	movq	8(%rdx), %rbx
	testq	%rbx, %rbx
	je	.L3715
.L3769:
	movq	8(%rbx), %rax
	movq	%rax, 8(%rdx)
	testq	%rax, %rax
	je	.L3716
	cmpq	24(%rax), %rbx
	je	.L3768
	movq	$0, 16(%rax)
.L3722:
	movq	32(%r12), %rax
	movq	%rax, 32(%rbx)
	movl	(%r12), %eax
	movq	$0, 16(%rbx)
	movl	%eax, (%rbx)
	movq	$0, 24(%rbx)
	movq	%rbx, 16(%rcx)
	movq	%rcx, 8(%rbx)
	movq	24(%r12), %rsi
	testq	%rsi, %rsi
	je	.L3723
	movq	%r15, %rcx
	movq	%rbx, %rdx
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE7_M_copyIZNSC_14_M_move_assignERSC_St17integral_constantIbLb0EEEUlRKS4_E_EEPSt13_Rb_tree_nodeIS4_EPKSL_PSt18_Rb_tree_node_baseRT_
	movq	%rax, 24(%rbx)
	movq	16(%r12), %r12
	testq	%r12, %r12
	je	.L3704
.L3724:
	movq	(%r15), %rdx
	movq	%rbx, %rcx
	movq	8(%rdx), %rbx
	testq	%rbx, %rbx
	jne	.L3769
.L3715:
	movq	16(%rdx), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	$39, %rax
	jbe	.L3770
	leaq	40(%rbx), %rax
	movq	%rax, 16(%rdi)
	jmp	.L3722
	.p2align 4,,10
	.p2align 3
.L3723:
	movq	16(%r12), %r12
	testq	%r12, %r12
	jne	.L3724
.L3704:
	addq	$24, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3716:
	.cfi_restore_state
	movq	$0, (%rdx)
	jmp	.L3722
	.p2align 4,,10
	.p2align 3
.L3768:
	movq	$0, 24(%rax)
	movq	8(%rdx), %rax
	movq	16(%rax), %rsi
	testq	%rsi, %rsi
	je	.L3722
	movq	%rsi, 8(%rdx)
	movq	24(%rsi), %rax
	testq	%rax, %rax
	je	.L3719
	.p2align 4,,10
	.p2align 3
.L3720:
	movq	%rax, 8(%rdx)
	movq	%rax, %rsi
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L3720
.L3719:
	movq	16(%rsi), %rax
	testq	%rax, %rax
	je	.L3722
	movq	%rax, 8(%rdx)
	jmp	.L3722
	.p2align 4,,10
	.p2align 3
.L3705:
	movq	16(%rcx), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %r14
	movq	24(%rdi), %rax
	subq	%r14, %rax
	cmpq	$39, %rax
	jbe	.L3771
	leaq	40(%r14), %rax
	movq	%rax, 16(%rdi)
	jmp	.L3712
	.p2align 4,,10
	.p2align 3
.L3706:
	movq	$0, (%rcx)
	jmp	.L3712
	.p2align 4,,10
	.p2align 3
.L3767:
	movq	$0, 24(%rax)
	movq	8(%rcx), %rax
	movq	16(%rax), %rsi
	testq	%rsi, %rsi
	je	.L3712
	movq	%rsi, 8(%rcx)
	movq	24(%rsi), %rax
	testq	%rax, %rax
	je	.L3709
	.p2align 4,,10
	.p2align 3
.L3710:
	movq	%rax, 8(%rcx)
	movq	%rax, %rsi
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L3710
.L3709:
	movq	16(%rsi), %rax
	testq	%rax, %rax
	je	.L3712
	movq	%rax, 8(%rcx)
	jmp	.L3712
	.p2align 4,,10
	.p2align 3
.L3770:
	movl	$40, %esi
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	movq	%rax, %rbx
	jmp	.L3722
.L3771:
	movl	$40, %esi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %r14
	jmp	.L3712
	.cfi_endproc
.LFE24984:
	.size	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE7_M_copyIZNSC_14_M_move_assignERSC_St17integral_constantIbLb0EEEUlRKS4_E_EEPSt13_Rb_tree_nodeIS4_EPKSL_PSt18_Rb_tree_node_baseRT_, .-_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE7_M_copyIZNSC_14_M_move_assignERSC_St17integral_constantIbLb0EEEUlRKS4_E_EEPSt13_Rb_tree_nodeIS4_EPKSL_PSt18_Rb_tree_node_baseRT_
	.section	.text._ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE14_M_move_assignERSC_St17integral_constantIbLb0EE,"axG",@progbits,_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE14_M_move_assignERSC_St17integral_constantIbLb0EE,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE14_M_move_assignERSC_St17integral_constantIbLb0EE
	.type	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE14_M_move_assignERSC_St17integral_constantIbLb0EE, @function
_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE14_M_move_assignERSC_St17integral_constantIbLb0EE:
.LFB24900:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	16(%rdi), %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	24(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	cmpq	%rax, (%rdi)
	je	.L3832
	movq	40(%rdi), %rax
	movq	%r12, -80(%rbp)
	movq	%rdi, -64(%rbp)
	movq	%rax, -72(%rbp)
	testq	%r12, %r12
	je	.L3780
	movq	$0, 8(%r12)
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L3781
	movq	%rax, -72(%rbp)
.L3781:
	movq	$0, 24(%rbx)
	movq	%r15, 32(%rbx)
	movq	%r15, 40(%rbx)
	movq	$0, 48(%rbx)
	movq	24(%r13), %rsi
	testq	%rsi, %rsi
	je	.L3833
.L3793:
	leaq	-80(%rbp), %rax
	movq	%r15, %rdx
	leaq	-88(%rbp), %rcx
	movq	%rbx, %rdi
	movq	%rax, -88(%rbp)
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE7_M_copyIZNSC_14_M_move_assignERSC_St17integral_constantIbLb0EEEUlRKS4_E_EEPSt13_Rb_tree_nodeIS4_EPKSL_PSt18_Rb_tree_node_baseRT_
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L3783:
	movq	%rdx, %rcx
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L3783
	movq	%rcx, 32(%rbx)
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L3784:
	movq	%rdx, %rcx
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L3784
	movq	%rcx, 40(%rbx)
	movq	48(%r13), %rdx
	movq	%rax, 24(%rbx)
	movq	%rdx, 48(%rbx)
	movq	24(%r13), %r12
	testq	%r12, %r12
	je	.L3785
.L3788:
	movq	24(%r12), %rbx
	testq	%rbx, %rbx
	je	.L3786
.L3787:
	movq	24(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L3787
.L3786:
	movq	16(%r12), %r12
	testq	%r12, %r12
	jne	.L3788
.L3785:
	movq	-80(%rbp), %r12
	leaq	16(%r13), %rax
	movq	$0, 24(%r13)
	movq	%rax, 32(%r13)
	movq	%rax, 40(%r13)
	movq	$0, 48(%r13)
	movq	-64(%rbp), %r13
	testq	%r12, %r12
	je	.L3772
	.p2align 4,,10
	.p2align 3
.L3791:
	movq	24(%r12), %rbx
	testq	%rbx, %rbx
	je	.L3789
.L3790:
	movq	24(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L3790
.L3789:
	movq	16(%r12), %r12
	testq	%r12, %r12
	jne	.L3791
.L3772:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3834
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3775:
	.cfi_restore_state
	movq	16(%r12), %r12
	.p2align 4,,10
	.p2align 3
.L3832:
	testq	%r12, %r12
	je	.L3835
	movq	24(%r12), %r14
	testq	%r14, %r14
	je	.L3775
.L3776:
	movq	24(%r14), %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	movq	16(%r14), %r14
	testq	%r14, %r14
	jne	.L3776
	movq	16(%r12), %r12
	jmp	.L3832
	.p2align 4,,10
	.p2align 3
.L3833:
	movq	-64(%rbp), %r13
	jmp	.L3791
	.p2align 4,,10
	.p2align 3
.L3780:
	movq	%r15, 32(%rdi)
	movq	%r15, 40(%rdi)
	movq	$0, 48(%rdi)
	movq	24(%rsi), %rsi
	movq	$0, -72(%rbp)
	testq	%rsi, %rsi
	jne	.L3793
	jmp	.L3772
	.p2align 4,,10
	.p2align 3
.L3835:
	movq	$0, 24(%rbx)
	movq	%r15, 32(%rbx)
	movq	%r15, 40(%rbx)
	movq	$0, 48(%rbx)
	cmpq	$0, 24(%r13)
	je	.L3772
	movl	16(%r13), %eax
	movl	%eax, 16(%rbx)
	movq	24(%r13), %rax
	movq	%rax, 24(%rbx)
	movq	32(%r13), %rdx
	movq	%rdx, 32(%rbx)
	movq	40(%r13), %rdx
	movq	%rdx, 40(%rbx)
	movq	%r15, 8(%rax)
	movq	48(%r13), %rax
	movq	%rax, 48(%rbx)
	leaq	16(%r13), %rax
	movq	$0, 24(%r13)
	movq	%rax, 32(%r13)
	movq	%rax, 40(%r13)
	movq	$0, 48(%r13)
	jmp	.L3772
.L3834:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24900:
	.size	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE14_M_move_assignERSC_St17integral_constantIbLb0EE, .-_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE14_M_move_assignERSC_St17integral_constantIbLb0EE
	.section	.text._ZNSt8_Rb_treeIN2v88internal6HandleINS1_3MapEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE7_M_copyIZNSC_14_M_move_assignERSC_St17integral_constantIbLb0EEEUlRKS4_E_EEPSt13_Rb_tree_nodeIS4_EPKSL_PSt18_Rb_tree_node_baseRT_,"axG",@progbits,_ZNSt8_Rb_treeIN2v88internal6HandleINS1_3MapEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE7_M_copyIZNSC_14_M_move_assignERSC_St17integral_constantIbLb0EEEUlRKS4_E_EEPSt13_Rb_tree_nodeIS4_EPKSL_PSt18_Rb_tree_node_baseRT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_3MapEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE7_M_copyIZNSC_14_M_move_assignERSC_St17integral_constantIbLb0EEEUlRKS4_E_EEPSt13_Rb_tree_nodeIS4_EPKSL_PSt18_Rb_tree_node_baseRT_
	.type	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_3MapEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE7_M_copyIZNSC_14_M_move_assignERSC_St17integral_constantIbLb0EEEUlRKS4_E_EEPSt13_Rb_tree_nodeIS4_EPKSL_PSt18_Rb_tree_node_baseRT_, @function
_ZNSt8_Rb_treeIN2v88internal6HandleINS1_3MapEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE7_M_copyIZNSC_14_M_move_assignERSC_St17integral_constantIbLb0EEEUlRKS4_E_EEPSt13_Rb_tree_nodeIS4_EPKSL_PSt18_Rb_tree_node_baseRT_:
.LFB24989:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	(%rcx), %rcx
	movq	8(%rcx), %r14
	testq	%r14, %r14
	je	.L3837
	movq	8(%r14), %rax
	movq	%rax, 8(%rcx)
	testq	%rax, %rax
	je	.L3838
	cmpq	24(%rax), %r14
	je	.L3899
	movq	$0, 16(%rax)
.L3844:
	movq	32(%rbx), %rax
	movq	%rax, 32(%r14)
	movl	(%rbx), %eax
	movq	$0, 16(%r14)
	movl	%eax, (%r14)
	movq	$0, 24(%r14)
	movq	%rdx, 8(%r14)
	movq	24(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L3845
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_3MapEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE7_M_copyIZNSC_14_M_move_assignERSC_St17integral_constantIbLb0EEEUlRKS4_E_EEPSt13_Rb_tree_nodeIS4_EPKSL_PSt18_Rb_tree_node_baseRT_
	movq	%rax, 24(%r14)
.L3845:
	movq	16(%rbx), %r12
	testq	%r12, %r12
	je	.L3836
	movq	(%r15), %rdx
	movq	%r14, %rcx
	movq	8(%rdx), %rbx
	testq	%rbx, %rbx
	je	.L3847
.L3901:
	movq	8(%rbx), %rax
	movq	%rax, 8(%rdx)
	testq	%rax, %rax
	je	.L3848
	cmpq	24(%rax), %rbx
	je	.L3900
	movq	$0, 16(%rax)
.L3854:
	movq	32(%r12), %rax
	movq	%rax, 32(%rbx)
	movl	(%r12), %eax
	movq	$0, 16(%rbx)
	movl	%eax, (%rbx)
	movq	$0, 24(%rbx)
	movq	%rbx, 16(%rcx)
	movq	%rcx, 8(%rbx)
	movq	24(%r12), %rsi
	testq	%rsi, %rsi
	je	.L3855
	movq	%r15, %rcx
	movq	%rbx, %rdx
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_3MapEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE7_M_copyIZNSC_14_M_move_assignERSC_St17integral_constantIbLb0EEEUlRKS4_E_EEPSt13_Rb_tree_nodeIS4_EPKSL_PSt18_Rb_tree_node_baseRT_
	movq	%rax, 24(%rbx)
	movq	16(%r12), %r12
	testq	%r12, %r12
	je	.L3836
.L3856:
	movq	(%r15), %rdx
	movq	%rbx, %rcx
	movq	8(%rdx), %rbx
	testq	%rbx, %rbx
	jne	.L3901
.L3847:
	movq	16(%rdx), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	$39, %rax
	jbe	.L3902
	leaq	40(%rbx), %rax
	movq	%rax, 16(%rdi)
	jmp	.L3854
	.p2align 4,,10
	.p2align 3
.L3855:
	movq	16(%r12), %r12
	testq	%r12, %r12
	jne	.L3856
.L3836:
	addq	$24, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3848:
	.cfi_restore_state
	movq	$0, (%rdx)
	jmp	.L3854
	.p2align 4,,10
	.p2align 3
.L3900:
	movq	$0, 24(%rax)
	movq	8(%rdx), %rax
	movq	16(%rax), %rsi
	testq	%rsi, %rsi
	je	.L3854
	movq	%rsi, 8(%rdx)
	movq	24(%rsi), %rax
	testq	%rax, %rax
	je	.L3851
	.p2align 4,,10
	.p2align 3
.L3852:
	movq	%rax, 8(%rdx)
	movq	%rax, %rsi
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L3852
.L3851:
	movq	16(%rsi), %rax
	testq	%rax, %rax
	je	.L3854
	movq	%rax, 8(%rdx)
	jmp	.L3854
	.p2align 4,,10
	.p2align 3
.L3837:
	movq	16(%rcx), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %r14
	movq	24(%rdi), %rax
	subq	%r14, %rax
	cmpq	$39, %rax
	jbe	.L3903
	leaq	40(%r14), %rax
	movq	%rax, 16(%rdi)
	jmp	.L3844
	.p2align 4,,10
	.p2align 3
.L3838:
	movq	$0, (%rcx)
	jmp	.L3844
	.p2align 4,,10
	.p2align 3
.L3899:
	movq	$0, 24(%rax)
	movq	8(%rcx), %rax
	movq	16(%rax), %rsi
	testq	%rsi, %rsi
	je	.L3844
	movq	%rsi, 8(%rcx)
	movq	24(%rsi), %rax
	testq	%rax, %rax
	je	.L3841
	.p2align 4,,10
	.p2align 3
.L3842:
	movq	%rax, 8(%rcx)
	movq	%rax, %rsi
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L3842
.L3841:
	movq	16(%rsi), %rax
	testq	%rax, %rax
	je	.L3844
	movq	%rax, 8(%rcx)
	jmp	.L3844
	.p2align 4,,10
	.p2align 3
.L3902:
	movl	$40, %esi
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	movq	%rax, %rbx
	jmp	.L3854
.L3903:
	movl	$40, %esi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %r14
	jmp	.L3844
	.cfi_endproc
.LFE24989:
	.size	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_3MapEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE7_M_copyIZNSC_14_M_move_assignERSC_St17integral_constantIbLb0EEEUlRKS4_E_EEPSt13_Rb_tree_nodeIS4_EPKSL_PSt18_Rb_tree_node_baseRT_, .-_ZNSt8_Rb_treeIN2v88internal6HandleINS1_3MapEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE7_M_copyIZNSC_14_M_move_assignERSC_St17integral_constantIbLb0EEEUlRKS4_E_EEPSt13_Rb_tree_nodeIS4_EPKSL_PSt18_Rb_tree_node_baseRT_
	.section	.text._ZNSt8_Rb_treeIN2v88internal6HandleINS1_3MapEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE14_M_move_assignERSC_St17integral_constantIbLb0EE,"axG",@progbits,_ZNSt8_Rb_treeIN2v88internal6HandleINS1_3MapEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE14_M_move_assignERSC_St17integral_constantIbLb0EE,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_3MapEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE14_M_move_assignERSC_St17integral_constantIbLb0EE
	.type	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_3MapEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE14_M_move_assignERSC_St17integral_constantIbLb0EE, @function
_ZNSt8_Rb_treeIN2v88internal6HandleINS1_3MapEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE14_M_move_assignERSC_St17integral_constantIbLb0EE:
.LFB24902:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	16(%rdi), %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	24(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	cmpq	%rax, (%rdi)
	je	.L3964
	movq	40(%rdi), %rax
	movq	%r12, -80(%rbp)
	movq	%rdi, -64(%rbp)
	movq	%rax, -72(%rbp)
	testq	%r12, %r12
	je	.L3912
	movq	$0, 8(%r12)
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L3913
	movq	%rax, -72(%rbp)
.L3913:
	movq	$0, 24(%rbx)
	movq	%r15, 32(%rbx)
	movq	%r15, 40(%rbx)
	movq	$0, 48(%rbx)
	movq	24(%r13), %rsi
	testq	%rsi, %rsi
	je	.L3965
.L3925:
	leaq	-80(%rbp), %rax
	movq	%r15, %rdx
	leaq	-88(%rbp), %rcx
	movq	%rbx, %rdi
	movq	%rax, -88(%rbp)
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_3MapEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE7_M_copyIZNSC_14_M_move_assignERSC_St17integral_constantIbLb0EEEUlRKS4_E_EEPSt13_Rb_tree_nodeIS4_EPKSL_PSt18_Rb_tree_node_baseRT_
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L3915:
	movq	%rdx, %rcx
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L3915
	movq	%rcx, 32(%rbx)
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L3916:
	movq	%rdx, %rcx
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L3916
	movq	%rcx, 40(%rbx)
	movq	48(%r13), %rdx
	movq	%rax, 24(%rbx)
	movq	%rdx, 48(%rbx)
	movq	24(%r13), %r12
	testq	%r12, %r12
	je	.L3917
.L3920:
	movq	24(%r12), %rbx
	testq	%rbx, %rbx
	je	.L3918
.L3919:
	movq	24(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_3MapEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L3919
.L3918:
	movq	16(%r12), %r12
	testq	%r12, %r12
	jne	.L3920
.L3917:
	movq	-80(%rbp), %r12
	leaq	16(%r13), %rax
	movq	$0, 24(%r13)
	movq	%rax, 32(%r13)
	movq	%rax, 40(%r13)
	movq	$0, 48(%r13)
	movq	-64(%rbp), %r13
	testq	%r12, %r12
	je	.L3904
	.p2align 4,,10
	.p2align 3
.L3923:
	movq	24(%r12), %rbx
	testq	%rbx, %rbx
	je	.L3921
.L3922:
	movq	24(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_3MapEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L3922
.L3921:
	movq	16(%r12), %r12
	testq	%r12, %r12
	jne	.L3923
.L3904:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3966
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3907:
	.cfi_restore_state
	movq	16(%r12), %r12
	.p2align 4,,10
	.p2align 3
.L3964:
	testq	%r12, %r12
	je	.L3967
	movq	24(%r12), %r14
	testq	%r14, %r14
	je	.L3907
.L3908:
	movq	24(%r14), %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_3MapEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	movq	16(%r14), %r14
	testq	%r14, %r14
	jne	.L3908
	movq	16(%r12), %r12
	jmp	.L3964
	.p2align 4,,10
	.p2align 3
.L3965:
	movq	-64(%rbp), %r13
	jmp	.L3923
	.p2align 4,,10
	.p2align 3
.L3912:
	movq	%r15, 32(%rdi)
	movq	%r15, 40(%rdi)
	movq	$0, 48(%rdi)
	movq	24(%rsi), %rsi
	movq	$0, -72(%rbp)
	testq	%rsi, %rsi
	jne	.L3925
	jmp	.L3904
	.p2align 4,,10
	.p2align 3
.L3967:
	movq	$0, 24(%rbx)
	movq	%r15, 32(%rbx)
	movq	%r15, 40(%rbx)
	movq	$0, 48(%rbx)
	cmpq	$0, 24(%r13)
	je	.L3904
	movl	16(%r13), %eax
	movl	%eax, 16(%rbx)
	movq	24(%r13), %rax
	movq	%rax, 24(%rbx)
	movq	32(%r13), %rdx
	movq	%rdx, 32(%rbx)
	movq	40(%r13), %rdx
	movq	%rdx, 40(%rbx)
	movq	%r15, 8(%rax)
	movq	48(%r13), %rax
	movq	%rax, 48(%rbx)
	leaq	16(%r13), %rax
	movq	$0, 24(%r13)
	movq	%rax, 32(%r13)
	movq	%rax, 40(%r13)
	movq	$0, 48(%r13)
	jmp	.L3904
.L3966:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24902:
	.size	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_3MapEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE14_M_move_assignERSC_St17integral_constantIbLb0EE, .-_ZNSt8_Rb_treeIN2v88internal6HandleINS1_3MapEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE14_M_move_assignERSC_St17integral_constantIbLb0EE
	.section	.text._ZNSt8_Rb_treeIN2v88internal8compiler17FunctionBlueprintES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE7_M_copyIZNSA_14_M_move_assignERSA_St17integral_constantIbLb0EEEUlRKS3_E_EEPSt13_Rb_tree_nodeIS3_EPKSJ_PSt18_Rb_tree_node_baseRT_,"axG",@progbits,_ZNSt8_Rb_treeIN2v88internal8compiler17FunctionBlueprintES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE7_M_copyIZNSA_14_M_move_assignERSA_St17integral_constantIbLb0EEEUlRKS3_E_EEPSt13_Rb_tree_nodeIS3_EPKSJ_PSt18_Rb_tree_node_baseRT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN2v88internal8compiler17FunctionBlueprintES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE7_M_copyIZNSA_14_M_move_assignERSA_St17integral_constantIbLb0EEEUlRKS3_E_EEPSt13_Rb_tree_nodeIS3_EPKSJ_PSt18_Rb_tree_node_baseRT_
	.type	_ZNSt8_Rb_treeIN2v88internal8compiler17FunctionBlueprintES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE7_M_copyIZNSA_14_M_move_assignERSA_St17integral_constantIbLb0EEEUlRKS3_E_EEPSt13_Rb_tree_nodeIS3_EPKSJ_PSt18_Rb_tree_node_baseRT_, @function
_ZNSt8_Rb_treeIN2v88internal8compiler17FunctionBlueprintES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE7_M_copyIZNSA_14_M_move_assignERSA_St17integral_constantIbLb0EEEUlRKS3_E_EEPSt13_Rb_tree_nodeIS3_EPKSJ_PSt18_Rb_tree_node_baseRT_:
.LFB24994:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	(%rcx), %rdx
	movq	%rdi, -64(%rbp)
	movq	8(%rdx), %r12
	testq	%r12, %r12
	je	.L3969
	movq	8(%r12), %rax
	movq	%rax, 8(%rdx)
	testq	%rax, %rax
	je	.L3970
	cmpq	24(%rax), %r12
	je	.L4031
	movq	$0, 16(%rax)
.L3975:
	leaq	48(%r12), %rdi
	movq	%rdi, -56(%rbp)
	call	_ZN2v88internal8compiler5HintsD1Ev
	movq	32(%rbx), %rax
	movq	-56(%rbp), %rdi
	leaq	48(%rbx), %rsi
	movq	%rax, 32(%r12)
	movq	40(%rbx), %rax
	movq	%rax, 40(%r12)
	call	_ZN2v88internal8compiler5HintsC1EOS2_
.L3993:
	movl	(%rbx), %eax
	movq	%r15, 8(%r12)
	movq	$0, 16(%r12)
	movl	%eax, (%r12)
	movq	$0, 24(%r12)
	movq	24(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L3977
	movq	-64(%rbp), %rdi
	movq	%r13, %rcx
	movq	%r12, %rdx
	call	_ZNSt8_Rb_treeIN2v88internal8compiler17FunctionBlueprintES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE7_M_copyIZNSA_14_M_move_assignERSA_St17integral_constantIbLb0EEEUlRKS3_E_EEPSt13_Rb_tree_nodeIS3_EPKSJ_PSt18_Rb_tree_node_baseRT_
	movq	%rax, 24(%r12)
.L3977:
	movq	16(%rbx), %r15
	testq	%r15, %r15
	je	.L3968
	movq	0(%r13), %rdx
	movq	%r12, %r14
	movq	8(%rdx), %rbx
	testq	%rbx, %rbx
	je	.L3979
.L4033:
	movq	8(%rbx), %rax
	movq	%rax, 8(%rdx)
	testq	%rax, %rax
	je	.L3980
	cmpq	24(%rax), %rbx
	je	.L4032
	movq	$0, 16(%rax)
.L3985:
	leaq	48(%rbx), %rdi
	movq	%rdi, -56(%rbp)
	call	_ZN2v88internal8compiler5HintsD1Ev
	movq	32(%r15), %rax
	movq	-56(%rbp), %rdi
	leaq	48(%r15), %rsi
	movq	%rax, 32(%rbx)
	movq	40(%r15), %rax
	movq	%rax, 40(%rbx)
	call	_ZN2v88internal8compiler5HintsC1EOS2_
.L3990:
	movl	(%r15), %eax
	movq	$0, 16(%rbx)
	movq	$0, 24(%rbx)
	movl	%eax, (%rbx)
	movq	%rbx, 16(%r14)
	movq	%r14, 8(%rbx)
	movq	24(%r15), %rsi
	testq	%rsi, %rsi
	je	.L3987
	movq	-64(%rbp), %rdi
	movq	%r13, %rcx
	movq	%rbx, %rdx
	call	_ZNSt8_Rb_treeIN2v88internal8compiler17FunctionBlueprintES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE7_M_copyIZNSA_14_M_move_assignERSA_St17integral_constantIbLb0EEEUlRKS3_E_EEPSt13_Rb_tree_nodeIS3_EPKSJ_PSt18_Rb_tree_node_baseRT_
	movq	%rax, 24(%rbx)
	movq	16(%r15), %r15
	testq	%r15, %r15
	je	.L3968
.L3988:
	movq	0(%r13), %rdx
	movq	%rbx, %r14
	movq	8(%rdx), %rbx
	testq	%rbx, %rbx
	jne	.L4033
.L3979:
	movq	16(%rdx), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	$271, %rax
	jbe	.L4034
	leaq	272(%rbx), %rax
	movq	%rax, 16(%rdi)
.L3986:
	movq	32(%r15), %rax
	leaq	48(%r15), %rsi
	leaq	48(%rbx), %rdi
	movq	%rax, 32(%rbx)
	movq	40(%r15), %rax
	movq	%rax, 40(%rbx)
	call	_ZN2v88internal8compiler5HintsC1EOS2_
	jmp	.L3990
	.p2align 4,,10
	.p2align 3
.L3987:
	movq	16(%r15), %r15
	testq	%r15, %r15
	jne	.L3988
.L3968:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3980:
	.cfi_restore_state
	movq	$0, (%rdx)
	jmp	.L3985
	.p2align 4,,10
	.p2align 3
.L4032:
	movq	$0, 24(%rax)
	movq	8(%rdx), %rax
	movq	16(%rax), %rsi
	testq	%rsi, %rsi
	je	.L3985
	movq	%rsi, 8(%rdx)
	movq	24(%rsi), %rax
	testq	%rax, %rax
	je	.L3983
	.p2align 4,,10
	.p2align 3
.L3984:
	movq	%rax, 8(%rdx)
	movq	%rax, %rsi
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L3984
.L3983:
	movq	16(%rsi), %rax
	testq	%rax, %rax
	je	.L3985
	movq	%rax, 8(%rdx)
	jmp	.L3985
	.p2align 4,,10
	.p2align 3
.L3969:
	movq	16(%rdx), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	subq	%r12, %rax
	cmpq	$271, %rax
	jbe	.L4035
	leaq	272(%r12), %rax
	movq	%rax, 16(%rdi)
.L3976:
	movq	32(%rbx), %rax
	leaq	48(%rbx), %rsi
	leaq	48(%r12), %rdi
	movq	%rax, 32(%r12)
	movq	40(%rbx), %rax
	movq	%rax, 40(%r12)
	call	_ZN2v88internal8compiler5HintsC1EOS2_
	jmp	.L3993
	.p2align 4,,10
	.p2align 3
.L3970:
	movq	$0, (%rdx)
	jmp	.L3975
	.p2align 4,,10
	.p2align 3
.L4031:
	movq	$0, 24(%rax)
	movq	8(%rdx), %rax
	movq	16(%rax), %rcx
	testq	%rcx, %rcx
	je	.L3975
	movq	%rcx, 8(%rdx)
	movq	24(%rcx), %rax
	testq	%rax, %rax
	je	.L3973
	.p2align 4,,10
	.p2align 3
.L3974:
	movq	%rax, 8(%rdx)
	movq	%rax, %rcx
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L3974
.L3973:
	movq	16(%rcx), %rax
	testq	%rax, %rax
	je	.L3975
	movq	%rax, 8(%rdx)
	jmp	.L3975
	.p2align 4,,10
	.p2align 3
.L4034:
	movl	$272, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rbx
	jmp	.L3986
.L4035:
	movl	$272, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L3976
	.cfi_endproc
.LFE24994:
	.size	_ZNSt8_Rb_treeIN2v88internal8compiler17FunctionBlueprintES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE7_M_copyIZNSA_14_M_move_assignERSA_St17integral_constantIbLb0EEEUlRKS3_E_EEPSt13_Rb_tree_nodeIS3_EPKSJ_PSt18_Rb_tree_node_baseRT_, .-_ZNSt8_Rb_treeIN2v88internal8compiler17FunctionBlueprintES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE7_M_copyIZNSA_14_M_move_assignERSA_St17integral_constantIbLb0EEEUlRKS3_E_EEPSt13_Rb_tree_nodeIS3_EPKSJ_PSt18_Rb_tree_node_baseRT_
	.section	.rodata._ZNSt6vectorIN2v88internal8compiler5HintsENS1_13ZoneAllocatorIS3_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS3_S6_EEmRKS3_.str1.1,"aMS",@progbits,1
.LC60:
	.string	"vector::_M_fill_insert"
	.section	.text._ZNSt6vectorIN2v88internal8compiler5HintsENS1_13ZoneAllocatorIS3_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS3_S6_EEmRKS3_,"axG",@progbits,_ZNSt6vectorIN2v88internal8compiler5HintsENS1_13ZoneAllocatorIS3_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS3_S6_EEmRKS3_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal8compiler5HintsENS1_13ZoneAllocatorIS3_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS3_S6_EEmRKS3_
	.type	_ZNSt6vectorIN2v88internal8compiler5HintsENS1_13ZoneAllocatorIS3_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS3_S6_EEmRKS3_, @function
_ZNSt6vectorIN2v88internal8compiler5HintsENS1_13ZoneAllocatorIS3_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS3_S6_EEmRKS3_:
.LFB22859:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$344, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L4036
	movq	24(%rdi), %rax
	movq	%rdx, %r12
	movq	16(%rdi), %rdx
	movq	%rsi, %r13
	movq	%rsi, -352(%rbp)
	movq	%rdi, %r14
	movabsq	$7905747460161236407, %r8
	subq	%rdx, %rax
	movq	%rax, %rsi
	sarq	$5, %rsi
	imulq	%r8, %rsi
	cmpq	%r12, %rsi
	jnb	.L4116
	movq	8(%rdi), %rsi
	movq	%rdx, %rax
	movl	$9586980, %edi
	movq	%rdi, %rdx
	subq	%rsi, %rax
	sarq	$5, %rax
	imulq	%r8, %rax
	subq	%rax, %rdx
	cmpq	%rdx, %r12
	ja	.L4117
	cmpq	%rax, %r12
	movq	%rax, %rdx
	movq	-352(%rbp), %rbx
	cmovnb	%r12, %rdx
	addq	%rdx, %rax
	setc	%dl
	subq	%rsi, %rbx
	movzbl	%dl, %edx
	testq	%rdx, %rdx
	jne	.L4082
	testq	%rax, %rax
	jne	.L4118
	movq	$0, -360(%rbp)
	xorl	%r13d, %r13d
.L4066:
	leaq	0(%r13,%rbx), %r15
	movq	%r12, %rbx
	.p2align 4,,10
	.p2align 3
.L4069:
	movq	%rcx, %rsi
	movq	%r15, %rdi
	movq	%rcx, -344(%rbp)
	addq	$224, %r15
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	subq	$1, %rbx
	movq	-344(%rbp), %rcx
	jne	.L4069
	movq	8(%r14), %rbx
	movq	%rbx, -344(%rbp)
	cmpq	%rbx, -352(%rbp)
	je	.L4084
	movq	%r13, %r15
	.p2align 4,,10
	.p2align 3
.L4071:
	movq	%rbx, %rsi
	movq	%r15, %rdi
	addq	$224, %rbx
	addq	$224, %r15
	call	_ZN2v88internal8compiler5HintsC1EOS2_
	cmpq	%rbx, -352(%rbp)
	jne	.L4071
	movq	-352(%rbp), %rax
	movabsq	$411757680216731063, %rdx
	subq	$224, %rax
	subq	-344(%rbp), %rax
	shrq	$5, %rax
	imulq	%rdx, %rax
	movabsq	$576460752303423487, %rdx
	andq	%rdx, %rax
	addq	$1, %rax
	leaq	0(,%rax,8), %rdx
	subq	%rax, %rdx
	movq	%rdx, %rax
	salq	$5, %rax
	addq	%r13, %rax
.L4070:
	leaq	0(,%r12,8), %rdx
	movq	16(%r14), %rbx
	subq	%r12, %rdx
	movq	%rdx, %r12
	salq	$5, %r12
	leaq	(%rax,%r12), %r15
	movq	-352(%rbp), %rax
	movq	%r15, -344(%rbp)
	cmpq	%rbx, %rax
	je	.L4072
	movq	%rax, %r12
	.p2align 4,,10
	.p2align 3
.L4073:
	movq	%r12, %rsi
	movq	%r15, %rdi
	addq	$224, %r12
	addq	$224, %r15
	call	_ZN2v88internal8compiler5HintsC1EOS2_
	cmpq	%r12, %rbx
	jne	.L4073
	subq	-352(%rbp), %rbx
	movabsq	$411757680216731063, %rdx
	leaq	-224(%rbx), %rax
	movq	16(%r14), %rbx
	shrq	$5, %rax
	imulq	%rdx, %rax
	movabsq	$576460752303423487, %rdx
	andq	%rdx, %rax
	addq	$1, %rax
	leaq	0(,%rax,8), %rdx
	subq	%rax, %rdx
	movq	%rdx, %rax
	salq	$5, %rax
	addq	%rax, -344(%rbp)
.L4072:
	movq	8(%r14), %r15
	cmpq	%r15, %rbx
	je	.L4074
	.p2align 4,,10
	.p2align 3
.L4075:
	movq	%r15, %rdi
	addq	$224, %r15
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	%r15, %rbx
	jne	.L4075
.L4074:
	movq	-360(%rbp), %rax
	movq	%r13, %xmm0
	movhps	-344(%rbp), %xmm0
	movq	%rax, 24(%r14)
	movups	%xmm0, 8(%r14)
.L4036:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4119
	addq	$344, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4116:
	.cfi_restore_state
	leaq	-280(%rbp), %r15
	movq	%rdi, -288(%rbp)
	movq	%rcx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movq	16(%r14), %rbx
	movabsq	$7905747460161236407, %r8
	movq	%rbx, %rax
	subq	-352(%rbp), %rax
	movq	%rax, -360(%rbp)
	sarq	$5, %rax
	imulq	%r8, %rax
	cmpq	%rax, %r12
	jnb	.L4039
	leaq	0(,%r12,8), %rax
	subq	%r12, %rax
	movq	%rbx, %r12
	salq	$5, %rax
	subq	%rax, %r12
	movq	%rax, -376(%rbp)
	movq	%r12, %rax
	cmpq	%rbx, %r12
	je	.L4040
	movq	%rbx, %rdx
	.p2align 4,,10
	.p2align 3
.L4041:
	movq	%rax, %rsi
	movq	%rdx, %rdi
	movq	%rax, -360(%rbp)
	movq	%rdx, -344(%rbp)
	call	_ZN2v88internal8compiler5HintsC1EOS2_
	movq	-360(%rbp), %rax
	movq	-344(%rbp), %rdx
	addq	$224, %rax
	addq	$224, %rdx
	cmpq	%rax, %rbx
	jne	.L4041
	movq	16(%r14), %rax
.L4040:
	addq	-376(%rbp), %rax
	leaq	-320(%rbp), %rcx
	movq	%rax, 16(%r14)
	movq	%r12, %rax
	subq	-352(%rbp), %rax
	movabsq	$7905747460161236407, %r14
	movq	%rax, %rdx
	movq	%rcx, -368(%rbp)
	sarq	$5, %rdx
	imulq	%r14, %rdx
	movq	%rdx, -360(%rbp)
	testq	%rax, %rax
	jle	.L4048
	.p2align 4,,10
	.p2align 3
.L4042:
	subq	$224, %r12
	subq	$224, %rbx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal8compiler14VirtualContextES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE14_M_move_assignERSA_St17integral_constantIbLb0EE
	leaq	56(%r12), %rsi
	leaq	56(%rbx), %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE14_M_move_assignERSC_St17integral_constantIbLb0EE
	leaq	112(%rbx), %rdi
	leaq	112(%r12), %rsi
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_3MapEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE14_M_move_assignERSC_St17integral_constantIbLb0EE
	movq	168(%r12), %rax
	leaq	168(%rbx), %rdi
	cmpq	%rax, 168(%rbx)
	je	.L4120
	movq	192(%rbx), %r14
	movq	%r14, -320(%rbp)
	movq	208(%rbx), %rdx
	movq	%rdi, -304(%rbp)
	movq	%rdx, -312(%rbp)
	testq	%r14, %r14
	je	.L4049
	movq	$0, 8(%r14)
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L4050
	movq	%rdx, -312(%rbp)
.L4050:
	leaq	184(%rbx), %rdx
	movq	$0, 192(%rbx)
	movq	%rdx, 200(%rbx)
	movq	%rdx, 208(%rbx)
	movq	$0, 216(%rbx)
	movq	192(%r12), %rsi
	testq	%rsi, %rsi
	je	.L4121
.L4077:
	movq	-368(%rbp), %rax
	leaq	-328(%rbp), %rcx
	movq	%rax, -328(%rbp)
	call	_ZNSt8_Rb_treeIN2v88internal8compiler17FunctionBlueprintES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE7_M_copyIZNSA_14_M_move_assignERSA_St17integral_constantIbLb0EEEUlRKS3_E_EEPSt13_Rb_tree_nodeIS3_EPKSJ_PSt18_Rb_tree_node_baseRT_
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L4052:
	movq	%rdx, %rcx
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L4052
	movq	%rcx, 200(%rbx)
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L4053:
	movq	%rdx, %rcx
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L4053
	movq	%rcx, 208(%rbx)
	movq	216(%r12), %rdx
	leaq	168(%r12), %rdi
	movq	%rax, 192(%rbx)
	movq	%rdx, 216(%rbx)
	movq	192(%r12), %rsi
	call	_ZNSt8_Rb_treeIN2v88internal8compiler17FunctionBlueprintES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE8_M_eraseEPSt13_Rb_tree_nodeIS3_E
	leaq	184(%r12), %rax
	movq	$0, 192(%r12)
	movq	%rax, 200(%r12)
	movq	%rax, 208(%r12)
	movq	$0, 216(%r12)
	movq	-320(%rbp), %r14
	movq	-304(%rbp), %rdx
	testq	%r14, %r14
	je	.L4047
	.p2align 4,,10
	.p2align 3
.L4054:
	movq	24(%r14), %rsi
	movq	%rdx, %rdi
	movq	%rdx, -344(%rbp)
	call	_ZNSt8_Rb_treeIN2v88internal8compiler17FunctionBlueprintES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE8_M_eraseEPSt13_Rb_tree_nodeIS3_E
	movq	%r14, %rdi
	movq	16(%r14), %r14
	addq	$48, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	testq	%r14, %r14
	movq	-344(%rbp), %rdx
	jne	.L4054
.L4047:
	subq	$1, -360(%rbp)
	jne	.L4042
.L4048:
	movq	-376(%rbp), %rbx
	movq	-352(%rbp), %rax
	addq	%rax, %rbx
	cmpq	%rbx, %rax
	je	.L4055
	.p2align 4,,10
	.p2align 3
.L4044:
	movq	%r13, %rdi
	movq	%r15, %rsi
	addq	$224, %r13
	call	_ZN2v88internal8compiler5HintsaSERKS2_
	cmpq	%r13, %rbx
	jne	.L4044
.L4055:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	jmp	.L4036
	.p2align 4,,10
	.p2align 3
.L4039:
	subq	%rax, %r12
	movq	%r12, -368(%rbp)
	movq	%r12, %rax
	je	.L4081
	movq	%rax, -344(%rbp)
	movq	%rbx, %r12
	.p2align 4,,10
	.p2align 3
.L4057:
	movq	%r12, %rdi
	movq	%r15, %rsi
	addq	$224, %r12
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	subq	$1, -344(%rbp)
	jne	.L4057
	movq	-368(%rbp), %rcx
	leaq	0(,%rcx,8), %rax
	subq	%rcx, %rax
	salq	$5, %rax
	addq	%rbx, %rax
.L4056:
	movq	-352(%rbp), %rcx
	movq	%rax, 16(%r14)
	cmpq	%rbx, %rcx
	je	.L4058
	movq	%rcx, %r12
	.p2align 4,,10
	.p2align 3
.L4059:
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -344(%rbp)
	addq	$224, %r12
	call	_ZN2v88internal8compiler5HintsC1EOS2_
	movq	-344(%rbp), %rax
	addq	$224, %rax
	cmpq	%r12, %rbx
	jne	.L4059
	movq	-360(%rbp), %rax
	addq	%rax, 16(%r14)
	.p2align 4,,10
	.p2align 3
.L4061:
	movq	%r13, %rdi
	movq	%r15, %rsi
	addq	$224, %r13
	call	_ZN2v88internal8compiler5HintsaSERKS2_
	cmpq	%r13, %rbx
	jne	.L4061
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	jmp	.L4036
	.p2align 4,,10
	.p2align 3
.L4082:
	movl	$2147483520, %esi
	movl	$2147483520, %r15d
.L4065:
	movq	(%r14), %rdi
	movq	16(%rdi), %r13
	movq	24(%rdi), %rax
	subq	%r13, %rax
	cmpq	%rax, %rsi
	ja	.L4122
	addq	%r13, %rsi
	movq	%rsi, 16(%rdi)
.L4068:
	leaq	0(%r13,%r15), %rax
	movq	%rax, -360(%rbp)
	jmp	.L4066
	.p2align 4,,10
	.p2align 3
.L4121:
	movq	-304(%rbp), %rdx
	jmp	.L4054
	.p2align 4,,10
	.p2align 3
.L4049:
	leaq	184(%rbx), %rdx
	movq	$0, -312(%rbp)
	movq	$0, 192(%rbx)
	movq	%rdx, 200(%rbx)
	movq	%rdx, 208(%rbx)
	movq	$0, 216(%rbx)
	movq	192(%r12), %rsi
	testq	%rsi, %rsi
	jne	.L4077
	jmp	.L4047
	.p2align 4,,10
	.p2align 3
.L4120:
	movq	192(%rbx), %rsi
	call	_ZNSt8_Rb_treeIN2v88internal8compiler17FunctionBlueprintES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE8_M_eraseEPSt13_Rb_tree_nodeIS3_E
	leaq	184(%rbx), %rax
	movq	$0, 192(%rbx)
	movq	%rax, 200(%rbx)
	movq	%rax, 208(%rbx)
	movq	$0, 216(%rbx)
	cmpq	$0, 192(%r12)
	je	.L4047
	movl	184(%r12), %edx
	movl	%edx, 184(%rbx)
	movq	192(%r12), %rdx
	movq	%rdx, 192(%rbx)
	movq	200(%r12), %rcx
	movq	%rcx, 200(%rbx)
	movq	208(%r12), %rcx
	movq	%rcx, 208(%rbx)
	movq	%rax, 8(%rdx)
	movq	216(%r12), %rax
	movq	%rax, 216(%rbx)
	leaq	184(%r12), %rax
	movq	$0, 192(%r12)
	movq	%rax, 200(%r12)
	movq	%rax, 208(%r12)
	movq	$0, 216(%r12)
	jmp	.L4047
	.p2align 4,,10
	.p2align 3
.L4084:
	movq	%r13, %rax
	jmp	.L4070
.L4122:
	movq	%rcx, -344(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-344(%rbp), %rcx
	movq	%rax, %r13
	jmp	.L4068
.L4058:
	addq	-360(%rbp), %rax
	movq	%rax, 16(%r14)
	jmp	.L4055
.L4081:
	movq	%rbx, %rax
	jmp	.L4056
.L4118:
	cmpq	$9586980, %rax
	cmova	%rdi, %rax
	leaq	0(,%rax,8), %r15
	subq	%rax, %r15
	salq	$5, %r15
	movq	%r15, %rsi
	jmp	.L4065
.L4119:
	call	__stack_chk_fail@PLT
.L4117:
	leaq	.LC60(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE22859:
	.size	_ZNSt6vectorIN2v88internal8compiler5HintsENS1_13ZoneAllocatorIS3_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS3_S6_EEmRKS3_, .-_ZNSt6vectorIN2v88internal8compiler5HintsENS1_13ZoneAllocatorIS3_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS3_S6_EEmRKS3_
	.section	.rodata._ZNSt6vectorIN2v88internal8compiler5HintsENS1_13ZoneAllocatorIS3_EEE15_M_range_insertISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPS3_S6_EEEEEvSC_T_SE_St20forward_iterator_tag.str1.1,"aMS",@progbits,1
.LC61:
	.string	"vector::_M_range_insert"
	.section	.text._ZNSt6vectorIN2v88internal8compiler5HintsENS1_13ZoneAllocatorIS3_EEE15_M_range_insertISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPS3_S6_EEEEEvSC_T_SE_St20forward_iterator_tag,"axG",@progbits,_ZNSt6vectorIN2v88internal8compiler5HintsENS1_13ZoneAllocatorIS3_EEE15_M_range_insertISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPS3_S6_EEEEEvSC_T_SE_St20forward_iterator_tag,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal8compiler5HintsENS1_13ZoneAllocatorIS3_EEE15_M_range_insertISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPS3_S6_EEEEEvSC_T_SE_St20forward_iterator_tag
	.type	_ZNSt6vectorIN2v88internal8compiler5HintsENS1_13ZoneAllocatorIS3_EEE15_M_range_insertISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPS3_S6_EEEEEvSC_T_SE_St20forward_iterator_tag, @function
_ZNSt6vectorIN2v88internal8compiler5HintsENS1_13ZoneAllocatorIS3_EEE15_M_range_insertISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPS3_S6_EEEEEvSC_T_SE_St20forward_iterator_tag:
.LFB23963:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -120(%rbp)
	movq	(%rdx), %rdx
	movq	%rcx, -128(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	movq	(%rax), %rcx
	cmpq	%rcx, %rdx
	je	.L4123
	movq	%rdi, %r14
	movq	%rdx, %rdi
	movq	%rsi, %r12
	movq	%rsi, %r13
	movq	16(%r14), %r15
	movq	24(%r14), %rax
	subq	%rcx, %rdi
	movabsq	$7905747460161236407, %rsi
	movq	%rdi, %rbx
	movq	%rdi, -152(%rbp)
	subq	%r15, %rax
	sarq	$5, %rbx
	sarq	$5, %rax
	imulq	%rsi, %rbx
	imulq	%rsi, %rax
	movq	%rbx, -136(%rbp)
	cmpq	%rbx, %rax
	jb	.L4125
	movq	%r15, %rax
	subq	%r12, %rax
	movq	%rax, -144(%rbp)
	sarq	$5, %rax
	imulq	%rsi, %rax
	movq	%rax, -160(%rbp)
	cmpq	%rax, %rbx
	jnb	.L4126
	movq	%r15, %rbx
	movq	%r15, %rdx
	subq	%rdi, %rbx
	movq	%rbx, %rax
	.p2align 4,,10
	.p2align 3
.L4127:
	movq	%rax, %rsi
	movq	%rdx, %rdi
	movq	%rax, -112(%rbp)
	movq	%rdx, -104(%rbp)
	call	_ZN2v88internal8compiler5HintsC1EOS2_
	movq	-112(%rbp), %rax
	movq	-104(%rbp), %rdx
	addq	$224, %rax
	addq	$224, %rdx
	cmpq	%rax, %r15
	jne	.L4127
	movq	-152(%rbp), %rax
	addq	%rax, 16(%r14)
	movq	%rbx, %rax
	leaq	-80(%rbp), %rcx
	subq	%r12, %rax
	movq	%rcx, -112(%rbp)
	movabsq	$7905747460161236407, %rdx
	movq	%rax, %r12
	sarq	$5, %r12
	imulq	%rdx, %r12
	movq	%r12, -104(%rbp)
	testq	%rax, %rax
	jle	.L4134
	.p2align 4,,10
	.p2align 3
.L4128:
	subq	$224, %rbx
	subq	$224, %r15
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal8compiler14VirtualContextES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE14_M_move_assignERSA_St17integral_constantIbLb0EE
	leaq	56(%rbx), %rsi
	leaq	56(%r15), %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE14_M_move_assignERSC_St17integral_constantIbLb0EE
	leaq	112(%r15), %rdi
	leaq	112(%rbx), %rsi
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_3MapEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE14_M_move_assignERSC_St17integral_constantIbLb0EE
	movq	168(%rbx), %rax
	leaq	168(%r15), %rdi
	cmpq	%rax, 168(%r15)
	je	.L4216
	movq	192(%r15), %r14
	movq	%r14, -80(%rbp)
	movq	208(%r15), %rdx
	movq	%rdi, -64(%rbp)
	movq	%rdx, -72(%rbp)
	testq	%r14, %r14
	je	.L4135
	movq	$0, 8(%r14)
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L4136
	movq	%rdx, -72(%rbp)
.L4136:
	leaq	184(%r15), %rdx
	movq	$0, 192(%r15)
	movq	%rdx, 200(%r15)
	movq	%rdx, 208(%r15)
	movq	$0, 216(%r15)
	movq	192(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L4217
.L4168:
	movq	-112(%rbp), %rax
	leaq	-88(%rbp), %rcx
	movq	%rax, -88(%rbp)
	call	_ZNSt8_Rb_treeIN2v88internal8compiler17FunctionBlueprintES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE7_M_copyIZNSA_14_M_move_assignERSA_St17integral_constantIbLb0EEEUlRKS3_E_EEPSt13_Rb_tree_nodeIS3_EPKSJ_PSt18_Rb_tree_node_baseRT_
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L4138:
	movq	%rdx, %rcx
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L4138
	movq	%rcx, 200(%r15)
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L4139:
	movq	%rdx, %rcx
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L4139
	movq	%rcx, 208(%r15)
	movq	216(%rbx), %rdx
	leaq	168(%rbx), %r14
	movq	%rax, 192(%r15)
	movq	%rdx, 216(%r15)
	movq	192(%rbx), %r12
	testq	%r12, %r12
	je	.L4142
.L4143:
	movq	24(%r12), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal8compiler17FunctionBlueprintES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE8_M_eraseEPSt13_Rb_tree_nodeIS3_E
	movq	%r12, %rdi
	movq	16(%r12), %r12
	addq	$48, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	testq	%r12, %r12
	jne	.L4143
.L4142:
	leaq	184(%rbx), %rax
	movq	$0, 192(%rbx)
	movq	%rax, 200(%rbx)
	movq	%rax, 208(%rbx)
	movq	$0, 216(%rbx)
	movq	-80(%rbp), %r14
	movq	-64(%rbp), %r12
	testq	%r14, %r14
	je	.L4133
	.p2align 4,,10
	.p2align 3
.L4144:
	movq	24(%r14), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal8compiler17FunctionBlueprintES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE8_M_eraseEPSt13_Rb_tree_nodeIS3_E
	movq	%r14, %rdi
	movq	16(%r14), %r14
	addq	$48, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	testq	%r14, %r14
	jne	.L4144
.L4133:
	subq	$1, -104(%rbp)
	jne	.L4128
.L4134:
	movq	-120(%rbp), %rax
	movq	-128(%rbp), %rcx
	movabsq	$7905747460161236407, %rdx
	movq	(%rax), %rbx
	movq	%rbx, %rax
	subq	(%rcx), %rax
	subq	$224, %rbx
	movq	%rax, %r12
	sarq	$5, %r12
	imulq	%rdx, %r12
	testq	%rax, %rax
	jle	.L4123
	.p2align 4,,10
	.p2align 3
.L4145:
	movq	%rbx, %rsi
	movq	%r13, %rdi
	subq	$224, %rbx
	addq	$224, %r13
	call	_ZN2v88internal8compiler5HintsaSERKS2_
	subq	$1, %r12
	jne	.L4145
.L4123:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4218
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4125:
	.cfi_restore_state
	movq	8(%r14), %rax
	movq	%r15, %r8
	subq	%rax, %r8
	movq	%rax, -104(%rbp)
	movl	$9586980, %eax
	sarq	$5, %r8
	movq	%rax, %rdx
	imulq	%rsi, %r8
	subq	%r8, %rdx
	cmpq	%rdx, -136(%rbp)
	ja	.L4219
	cmpq	%r8, %rbx
	cmovb	%r8, %rbx
	addq	%r8, %rbx
	jc	.L4155
	testq	%rbx, %rbx
	jne	.L4220
	movq	$0, -144(%rbp)
	movq	$0, -136(%rbp)
.L4157:
	movq	-104(%rbp), %rbx
	cmpq	%rbx, %r12
	je	.L4174
	movq	-136(%rbp), %r15
	.p2align 4,,10
	.p2align 3
.L4161:
	movq	%rbx, %rsi
	movq	%r15, %rdi
	addq	$224, %rbx
	addq	$224, %r15
	call	_ZN2v88internal8compiler5HintsC1EOS2_
	cmpq	%rbx, %r12
	jne	.L4161
	leaq	-224(%r12), %rax
	subq	-104(%rbp), %rax
	movabsq	$411757680216731063, %rdx
	shrq	$5, %rax
	imulq	%rdx, %rax
	movabsq	$576460752303423487, %rdx
	andq	%rdx, %rax
	addq	$1, %rax
	leaq	0(,%rax,8), %rdx
	subq	%rax, %rdx
	movq	%rdx, %rax
	salq	$5, %rax
	addq	-136(%rbp), %rax
	movq	%rax, -112(%rbp)
.L4160:
	movq	-120(%rbp), %rcx
	movq	-128(%rbp), %rax
	movq	(%rcx), %r15
	movq	(%rax), %rax
	movq	%r15, -120(%rbp)
	movq	%rax, -104(%rbp)
	cmpq	%r15, %rax
	je	.L4162
	movq	-112(%rbp), %rbx
	.p2align 4,,10
	.p2align 3
.L4163:
	subq	$224, %r15
	movq	%rbx, %rdi
	addq	$224, %rbx
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	cmpq	-104(%rbp), %r15
	jne	.L4163
	movabsq	$411757680216731063, %rdx
	movq	-120(%rbp), %rax
	subq	%r15, %rax
	leaq	-224(%rax), %rax
	shrq	$5, %rax
	imulq	%rdx, %rax
	movabsq	$576460752303423487, %rdx
	andq	%rdx, %rax
	addq	$1, %rax
	leaq	0(,%rax,8), %rdx
	subq	%rax, %rdx
	movq	%rdx, %rax
	salq	$5, %rax
	addq	%rax, -112(%rbp)
.L4162:
	movq	16(%r14), %rbx
	cmpq	%r12, %rbx
	je	.L4164
	movq	-112(%rbp), %r15
	movq	%r12, %r13
	.p2align 4,,10
	.p2align 3
.L4165:
	movq	%r13, %rsi
	movq	%r15, %rdi
	addq	$224, %r13
	addq	$224, %r15
	call	_ZN2v88internal8compiler5HintsC1EOS2_
	cmpq	%r13, %rbx
	jne	.L4165
	subq	%r12, %rbx
	movq	16(%r14), %r13
	movabsq	$411757680216731063, %rdx
	leaq	-224(%rbx), %rax
	shrq	$5, %rax
	imulq	%rdx, %rax
	movabsq	$576460752303423487, %rdx
	andq	%rdx, %rax
	addq	$1, %rax
	leaq	0(,%rax,8), %rdx
	subq	%rax, %rdx
	movq	%rdx, %rax
	salq	$5, %rax
	addq	%rax, -112(%rbp)
.L4164:
	movq	8(%r14), %rbx
	cmpq	%r13, %rbx
	je	.L4166
	.p2align 4,,10
	.p2align 3
.L4167:
	movq	%rbx, %rdi
	addq	$224, %rbx
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	%r13, %rbx
	jne	.L4167
.L4166:
	movq	-136(%rbp), %xmm0
	movq	-144(%rbp), %rax
	movhps	-112(%rbp), %xmm0
	movq	%rax, 24(%r14)
	movups	%xmm0, 8(%r14)
	jmp	.L4123
	.p2align 4,,10
	.p2align 3
.L4217:
	movq	-64(%rbp), %r12
	jmp	.L4144
	.p2align 4,,10
	.p2align 3
.L4135:
	leaq	184(%r15), %rdx
	movq	$0, -72(%rbp)
	movq	$0, 192(%r15)
	movq	%rdx, 200(%r15)
	movq	%rdx, 208(%r15)
	movq	$0, 216(%r15)
	movq	192(%rbx), %rsi
	testq	%rsi, %rsi
	jne	.L4168
	subq	$1, -104(%rbp)
	jne	.L4128
	jmp	.L4134
	.p2align 4,,10
	.p2align 3
.L4126:
	subq	-144(%rbp), %rdx
	movq	%rdx, -128(%rbp)
	movq	%rdx, %rbx
	cmpq	%rdx, %rcx
	je	.L4171
	movq	%r15, %rdx
	.p2align 4,,10
	.p2align 3
.L4147:
	subq	$224, %rbx
	movq	%rdx, %rdi
	movq	%rcx, -112(%rbp)
	movq	%rbx, %rsi
	movq	%rdx, -104(%rbp)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rcx
	addq	$224, %rdx
	cmpq	%rbx, %rcx
	jne	.L4147
	movq	16(%r14), %rax
.L4146:
	movq	-136(%rbp), %rdx
	subq	-160(%rbp), %rdx
	leaq	0(,%rdx,8), %rbx
	subq	%rdx, %rbx
	salq	$5, %rbx
	addq	%rax, %rbx
	movq	%rbx, 16(%r14)
	cmpq	%r12, %r15
	je	.L4148
	.p2align 4,,10
	.p2align 3
.L4149:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	addq	$224, %r12
	addq	$224, %rbx
	call	_ZN2v88internal8compiler5HintsC1EOS2_
	cmpq	%r12, %r15
	jne	.L4149
	movq	16(%r14), %rbx
.L4148:
	movq	-120(%rbp), %rax
	addq	-144(%rbp), %rbx
	movabsq	$7905747460161236407, %rdx
	movq	%rbx, 16(%r14)
	movq	(%rax), %rbx
	movq	%rbx, %rax
	subq	-128(%rbp), %rax
	subq	$224, %rbx
	movq	%rax, %r12
	sarq	$5, %r12
	imulq	%rdx, %r12
	testq	%rax, %rax
	jle	.L4123
	.p2align 4,,10
	.p2align 3
.L4152:
	movq	%rbx, %rsi
	movq	%r13, %rdi
	subq	$224, %rbx
	addq	$224, %r13
	call	_ZN2v88internal8compiler5HintsaSERKS2_
	subq	$1, %r12
	jne	.L4152
	jmp	.L4123
	.p2align 4,,10
	.p2align 3
.L4216:
	movq	192(%r15), %rsi
	call	_ZNSt8_Rb_treeIN2v88internal8compiler17FunctionBlueprintES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE8_M_eraseEPSt13_Rb_tree_nodeIS3_E
	leaq	184(%r15), %rax
	movq	$0, 192(%r15)
	movq	%rax, 200(%r15)
	movq	%rax, 208(%r15)
	movq	$0, 216(%r15)
	cmpq	$0, 192(%rbx)
	je	.L4133
	movl	184(%rbx), %edx
	subq	$1, -104(%rbp)
	movl	%edx, 184(%r15)
	movq	192(%rbx), %rdx
	movq	%rdx, 192(%r15)
	movq	200(%rbx), %rcx
	movq	%rcx, 200(%r15)
	movq	208(%rbx), %rcx
	movq	%rcx, 208(%r15)
	movq	%rax, 8(%rdx)
	movq	216(%rbx), %rax
	movq	%rax, 216(%r15)
	leaq	184(%rbx), %rax
	movq	$0, 192(%rbx)
	movq	%rax, 200(%rbx)
	movq	%rax, 208(%rbx)
	movq	$0, 216(%rbx)
	jne	.L4128
	jmp	.L4134
	.p2align 4,,10
	.p2align 3
.L4171:
	movq	%r15, %rax
	jmp	.L4146
	.p2align 4,,10
	.p2align 3
.L4174:
	movq	-136(%rbp), %rax
	movq	%rax, -112(%rbp)
	jmp	.L4160
.L4220:
	cmpq	$9586980, %rbx
	cmova	%rax, %rbx
	leaq	0(,%rbx,8), %rax
	subq	%rbx, %rax
	movq	%rax, %rbx
	salq	$5, %rbx
	movq	%rbx, %rsi
.L4156:
	movq	(%r14), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L4221
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L4159:
	movq	8(%r14), %rcx
	addq	%rax, %rbx
	movq	%rax, -136(%rbp)
	movq	%rbx, -144(%rbp)
	movq	%rcx, -104(%rbp)
	jmp	.L4157
.L4221:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L4159
.L4218:
	call	__stack_chk_fail@PLT
.L4155:
	movl	$2147483520, %esi
	movl	$2147483520, %ebx
	jmp	.L4156
.L4219:
	leaq	.LC61(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE23963:
	.size	_ZNSt6vectorIN2v88internal8compiler5HintsENS1_13ZoneAllocatorIS3_EEE15_M_range_insertISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPS3_S6_EEEEEvSC_T_SE_St20forward_iterator_tag, .-_ZNSt6vectorIN2v88internal8compiler5HintsENS1_13ZoneAllocatorIS3_EEE15_M_range_insertISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPS3_S6_EEEEEvSC_T_SE_St20forward_iterator_tag
	.section	.text._ZNSt6vectorIN2v88internal8compiler5HintsENS1_13ZoneAllocatorIS3_EEE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPKS3_S6_EEEEvNS9_IPS3_S6_EET_SF_St20forward_iterator_tag,"axG",@progbits,_ZNSt6vectorIN2v88internal8compiler5HintsENS1_13ZoneAllocatorIS3_EEE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPKS3_S6_EEEEvNS9_IPS3_S6_EET_SF_St20forward_iterator_tag,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal8compiler5HintsENS1_13ZoneAllocatorIS3_EEE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPKS3_S6_EEEEvNS9_IPS3_S6_EET_SF_St20forward_iterator_tag
	.type	_ZNSt6vectorIN2v88internal8compiler5HintsENS1_13ZoneAllocatorIS3_EEE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPKS3_S6_EEEEvNS9_IPS3_S6_EET_SF_St20forward_iterator_tag, @function
_ZNSt6vectorIN2v88internal8compiler5HintsENS1_13ZoneAllocatorIS3_EEE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPKS3_S6_EEEEvNS9_IPS3_S6_EET_SF_St20forward_iterator_tag:
.LFB23964:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -120(%rbp)
	movq	%rdx, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	%rdx, %rcx
	je	.L4222
	movq	%rsi, %r12
	movq	%rsi, %r14
	movq	%rcx, %rsi
	movq	%rdx, %r13
	subq	%rdx, %rsi
	movq	16(%rdi), %r15
	movq	%rcx, %rbx
	movabsq	$7905747460161236407, %rdx
	movq	%rsi, %rax
	movq	%rsi, -152(%rbp)
	sarq	$5, %rax
	imulq	%rdx, %rax
	movq	%rax, -144(%rbp)
	movq	%rax, %rcx
	movq	%rax, -128(%rbp)
	movq	24(%rdi), %rax
	movq	%rax, -112(%rbp)
	subq	%r15, %rax
	sarq	$5, %rax
	imulq	%rdx, %rax
	cmpq	%rcx, %rax
	jb	.L4225
	movq	%r15, %rax
	subq	%r12, %rax
	movq	%rax, -160(%rbp)
	sarq	$5, %rax
	imulq	%rdx, %rax
	movq	%rax, -168(%rbp)
	movq	%rax, -136(%rbp)
	cmpq	%rax, %rcx
	jnb	.L4226
	movq	%r15, %rbx
	movq	%r15, %rdx
	subq	%rsi, %rbx
	movq	%rbx, %rax
	.p2align 4,,10
	.p2align 3
.L4227:
	movq	%rax, %rsi
	movq	%rdx, %rdi
	movq	%rax, -112(%rbp)
	movq	%rdx, -104(%rbp)
	call	_ZN2v88internal8compiler5HintsC1EOS2_
	movq	-112(%rbp), %rax
	movq	-104(%rbp), %rdx
	addq	$224, %rax
	addq	$224, %rdx
	cmpq	%rax, %r15
	jne	.L4227
	movq	-120(%rbp), %rax
	movq	-152(%rbp), %rcx
	movabsq	$7905747460161236407, %rdx
	addq	%rcx, 16(%rax)
	movq	%rbx, %rax
	leaq	-80(%rbp), %rcx
	subq	%r12, %rax
	movq	%rcx, -120(%rbp)
	movq	%rax, %r12
	sarq	$5, %r12
	imulq	%rdx, %r12
	movq	%r12, -112(%rbp)
	testq	%rax, %rax
	jle	.L4234
	.p2align 4,,10
	.p2align 3
.L4228:
	subq	$224, %rbx
	subq	$224, %r15
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal8compiler14VirtualContextES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE14_M_move_assignERSA_St17integral_constantIbLb0EE
	leaq	56(%rbx), %rsi
	leaq	56(%r15), %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE14_M_move_assignERSC_St17integral_constantIbLb0EE
	leaq	112(%r15), %rdi
	leaq	112(%rbx), %rsi
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_3MapEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE14_M_move_assignERSC_St17integral_constantIbLb0EE
	movq	168(%rbx), %rax
	leaq	168(%r15), %rdi
	cmpq	%rax, 168(%r15)
	je	.L4310
	movq	192(%r15), %r12
	movq	%r12, -80(%rbp)
	movq	208(%r15), %rdx
	movq	%rdi, -64(%rbp)
	movq	%rdx, -72(%rbp)
	testq	%r12, %r12
	je	.L4235
	movq	$0, 8(%r12)
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L4236
	movq	%rdx, -72(%rbp)
.L4236:
	leaq	184(%r15), %rdx
	movq	$0, 192(%r15)
	movq	%rdx, 200(%r15)
	movq	%rdx, 208(%r15)
	movq	$0, 216(%r15)
	movq	192(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L4311
.L4264:
	movq	-120(%rbp), %rax
	leaq	-88(%rbp), %rcx
	movq	%rax, -88(%rbp)
	call	_ZNSt8_Rb_treeIN2v88internal8compiler17FunctionBlueprintES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE7_M_copyIZNSA_14_M_move_assignERSA_St17integral_constantIbLb0EEEUlRKS3_E_EEPSt13_Rb_tree_nodeIS3_EPKSJ_PSt18_Rb_tree_node_baseRT_
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L4238:
	movq	%rdx, %rcx
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L4238
	movq	%rcx, 200(%r15)
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L4239:
	movq	%rdx, %rcx
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L4239
	movq	%rcx, 208(%r15)
	movq	216(%rbx), %rdx
	movq	%rax, 192(%r15)
	movq	%rdx, 216(%r15)
	movq	192(%rbx), %r12
	leaq	168(%rbx), %rdx
	testq	%r12, %r12
	je	.L4242
.L4243:
	movq	24(%r12), %rsi
	movq	%rdx, %rdi
	movq	%rdx, -104(%rbp)
	call	_ZNSt8_Rb_treeIN2v88internal8compiler17FunctionBlueprintES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE8_M_eraseEPSt13_Rb_tree_nodeIS3_E
	movq	%r12, %rdi
	movq	16(%r12), %r12
	addq	$48, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	testq	%r12, %r12
	movq	-104(%rbp), %rdx
	jne	.L4243
.L4242:
	leaq	184(%rbx), %rax
	movq	$0, 192(%rbx)
	movq	%rax, 200(%rbx)
	movq	%rax, 208(%rbx)
	movq	$0, 216(%rbx)
	movq	-80(%rbp), %r12
	movq	-64(%rbp), %rdx
	testq	%r12, %r12
	je	.L4233
	.p2align 4,,10
	.p2align 3
.L4244:
	movq	24(%r12), %rsi
	movq	%rdx, %rdi
	movq	%rdx, -104(%rbp)
	call	_ZNSt8_Rb_treeIN2v88internal8compiler17FunctionBlueprintES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE8_M_eraseEPSt13_Rb_tree_nodeIS3_E
	movq	%r12, %rdi
	movq	16(%r12), %r12
	addq	$48, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	testq	%r12, %r12
	movq	-104(%rbp), %rdx
	jne	.L4244
.L4233:
	subq	$1, -112(%rbp)
	jne	.L4228
.L4234:
	cmpq	$0, -152(%rbp)
	jle	.L4222
	.p2align 4,,10
	.p2align 3
.L4230:
	movq	%r13, %rsi
	movq	%r14, %rdi
	addq	$224, %r13
	addq	$224, %r14
	call	_ZN2v88internal8compiler5HintsaSERKS2_
	subq	$1, -128(%rbp)
	jne	.L4230
.L4222:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4312
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4225:
	.cfi_restore_state
	movq	8(%rdi), %rax
	movq	%r15, %r8
	subq	%rax, %r8
	movq	%rax, -128(%rbp)
	sarq	$5, %r8
	imulq	%rdx, %r8
	movl	$9586980, %edx
	movq	%rdx, %rax
	subq	%r8, %rax
	cmpq	%rax, -144(%rbp)
	ja	.L4313
	movq	%rcx, %rax
	cmpq	%r8, %rcx
	cmovb	%r8, %rax
	addq	%rax, %r8
	jc	.L4252
	testq	%r8, %r8
	jne	.L4314
	movq	$0, -136(%rbp)
	movq	$0, -112(%rbp)
.L4254:
	movq	-128(%rbp), %r13
	cmpq	%r13, %r12
	je	.L4270
	movq	-112(%rbp), %r14
	.p2align 4,,10
	.p2align 3
.L4258:
	movq	%r13, %rsi
	movq	%r14, %rdi
	addq	$224, %r13
	addq	$224, %r14
	call	_ZN2v88internal8compiler5HintsC1EOS2_
	cmpq	%r13, %r12
	jne	.L4258
	leaq	-224(%r12), %rax
	subq	-128(%rbp), %rax
	movabsq	$411757680216731063, %rdx
	shrq	$5, %rax
	imulq	%rdx, %rax
	movabsq	$576460752303423487, %rdx
	andq	%rdx, %rax
	addq	$1, %rax
	leaq	0(,%rax,8), %r13
	subq	%rax, %r13
	movq	-112(%rbp), %rax
	salq	$5, %r13
	leaq	(%rax,%r13), %r15
.L4257:
	movq	-104(%rbp), %r13
	movq	%r15, %r14
	.p2align 4,,10
	.p2align 3
.L4259:
	movq	%r13, %rsi
	movq	%r14, %rdi
	addq	$224, %r13
	addq	$224, %r14
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	cmpq	%r13, %rbx
	jne	.L4259
	leaq	-224(%rbx), %rax
	subq	-104(%rbp), %rax
	movabsq	$411757680216731063, %rbx
	movabsq	$576460752303423487, %rdx
	shrq	$5, %rax
	imulq	%rbx, %rax
	andq	%rdx, %rax
	addq	$1, %rax
	leaq	0(,%rax,8), %rdx
	subq	%rax, %rdx
	movq	%rdx, %rax
	salq	$5, %rax
	addq	%rax, %r15
	movq	-120(%rbp), %rax
	movq	16(%rax), %rbx
	cmpq	%rbx, %r12
	je	.L4260
	movq	%r12, %r14
	movq	%r15, %r13
	.p2align 4,,10
	.p2align 3
.L4261:
	movq	%r14, %rsi
	movq	%r13, %rdi
	addq	$224, %r14
	addq	$224, %r13
	call	_ZN2v88internal8compiler5HintsC1EOS2_
	cmpq	%r14, %rbx
	jne	.L4261
	movabsq	$411757680216731063, %rdx
	subq	%r12, %rbx
	leaq	-224(%rbx), %rax
	shrq	$5, %rax
	imulq	%rdx, %rax
	movabsq	$576460752303423487, %rdx
	andq	%rdx, %rax
	addq	$1, %rax
	leaq	0(,%rax,8), %rdx
	subq	%rax, %rdx
	movq	%rdx, %rax
	salq	$5, %rax
	addq	%rax, %r15
	movq	-120(%rbp), %rax
	movq	16(%rax), %rbx
.L4260:
	movq	-120(%rbp), %rax
	movq	8(%rax), %r12
	cmpq	%rbx, %r12
	je	.L4262
	.p2align 4,,10
	.p2align 3
.L4263:
	movq	%r12, %rdi
	addq	$224, %r12
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	%rbx, %r12
	jne	.L4263
.L4262:
	movq	-112(%rbp), %xmm0
	movq	-120(%rbp), %rax
	movq	%r15, %xmm1
	movq	-136(%rbp), %rcx
	punpcklqdq	%xmm1, %xmm0
	movq	%rcx, 24(%rax)
	movups	%xmm0, 8(%rax)
	jmp	.L4222
	.p2align 4,,10
	.p2align 3
.L4311:
	movq	-64(%rbp), %rdx
	jmp	.L4244
	.p2align 4,,10
	.p2align 3
.L4235:
	leaq	184(%r15), %rdx
	movq	$0, -72(%rbp)
	movq	$0, 192(%r15)
	movq	%rdx, 200(%r15)
	movq	%rdx, 208(%r15)
	movq	$0, 216(%r15)
	movq	192(%rbx), %rsi
	testq	%rsi, %rsi
	jne	.L4264
	subq	$1, -112(%rbp)
	jne	.L4228
	jmp	.L4234
	.p2align 4,,10
	.p2align 3
.L4226:
	movq	-160(%rbp), %rax
	addq	%r13, %rax
	cmpq	%rax, %rbx
	je	.L4267
	movq	%r15, %rdx
	.p2align 4,,10
	.p2align 3
.L4246:
	movq	%rax, %rsi
	movq	%rdx, %rdi
	movq	%rax, -112(%rbp)
	movq	%rdx, -104(%rbp)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movq	-112(%rbp), %rax
	movq	-104(%rbp), %rdx
	addq	$224, %rax
	addq	$224, %rdx
	cmpq	%rax, %rbx
	jne	.L4246
	movq	-120(%rbp), %rax
	movq	16(%rax), %rax
.L4245:
	movq	-144(%rbp), %rdx
	subq	-168(%rbp), %rdx
	leaq	0(,%rdx,8), %rbx
	subq	%rdx, %rbx
	salq	$5, %rbx
	addq	%rax, %rbx
	movq	-120(%rbp), %rax
	movq	%rbx, 16(%rax)
	cmpq	%r12, %r15
	je	.L4247
	.p2align 4,,10
	.p2align 3
.L4248:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	addq	$224, %r12
	addq	$224, %rbx
	call	_ZN2v88internal8compiler5HintsC1EOS2_
	cmpq	%r12, %r15
	jne	.L4248
	movq	-120(%rbp), %rax
	movq	16(%rax), %rbx
.L4247:
	movq	-160(%rbp), %rax
	movq	-120(%rbp), %rcx
	addq	%rax, %rbx
	movq	%rbx, 16(%rcx)
	testq	%rax, %rax
	jle	.L4222
	.p2align 4,,10
	.p2align 3
.L4249:
	movq	%r13, %rsi
	movq	%r14, %rdi
	addq	$224, %r13
	addq	$224, %r14
	call	_ZN2v88internal8compiler5HintsaSERKS2_
	subq	$1, -136(%rbp)
	jne	.L4249
	jmp	.L4222
	.p2align 4,,10
	.p2align 3
.L4310:
	movq	192(%r15), %rsi
	call	_ZNSt8_Rb_treeIN2v88internal8compiler17FunctionBlueprintES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE8_M_eraseEPSt13_Rb_tree_nodeIS3_E
	leaq	184(%r15), %rax
	movq	$0, 192(%r15)
	movq	%rax, 200(%r15)
	movq	%rax, 208(%r15)
	movq	$0, 216(%r15)
	cmpq	$0, 192(%rbx)
	je	.L4233
	movl	184(%rbx), %edx
	subq	$1, -112(%rbp)
	movl	%edx, 184(%r15)
	movq	192(%rbx), %rdx
	movq	%rdx, 192(%r15)
	movq	200(%rbx), %rcx
	movq	%rcx, 200(%r15)
	movq	208(%rbx), %rcx
	movq	%rcx, 208(%r15)
	movq	%rax, 8(%rdx)
	movq	216(%rbx), %rax
	movq	%rax, 216(%r15)
	leaq	184(%rbx), %rax
	movq	$0, 192(%rbx)
	movq	%rax, 200(%rbx)
	movq	%rax, 208(%rbx)
	movq	$0, 216(%rbx)
	jne	.L4228
	jmp	.L4234
	.p2align 4,,10
	.p2align 3
.L4267:
	movq	%r15, %rax
	jmp	.L4245
	.p2align 4,,10
	.p2align 3
.L4270:
	movq	-112(%rbp), %r15
	jmp	.L4257
.L4314:
	cmpq	$9586980, %r8
	cmova	%rdx, %r8
	leaq	0(,%r8,8), %rdx
	subq	%r8, %rdx
	salq	$5, %rdx
	movq	%rdx, %r13
	movq	%rdx, %rsi
.L4253:
	movq	-120(%rbp), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %r14
	movq	24(%rdi), %rax
	subq	%r14, %rax
	cmpq	%rax, %rsi
	ja	.L4315
	addq	%r14, %rsi
	movq	%rsi, 16(%rdi)
.L4256:
	movq	-120(%rbp), %rax
	addq	%r14, %r13
	movq	%r14, -112(%rbp)
	movq	%r13, -136(%rbp)
	movq	8(%rax), %rax
	movq	%rax, -128(%rbp)
	jmp	.L4254
.L4315:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r14
	jmp	.L4256
.L4312:
	call	__stack_chk_fail@PLT
.L4252:
	movl	$2147483520, %esi
	movl	$2147483520, %r13d
	jmp	.L4253
.L4313:
	leaq	.LC61(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE23964:
	.size	_ZNSt6vectorIN2v88internal8compiler5HintsENS1_13ZoneAllocatorIS3_EEE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPKS3_S6_EEEEvNS9_IPS3_S6_EET_SF_St20forward_iterator_tag, .-_ZNSt6vectorIN2v88internal8compiler5HintsENS1_13ZoneAllocatorIS3_EEE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPKS3_S6_EEEEvNS9_IPS3_S6_EET_SF_St20forward_iterator_tag
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation22ProcessCallOrConstructENS1_5HintsENS_4base8OptionalIS3_EERKNS0_10ZoneVectorIS3_EENS0_12FeedbackSlotEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation22ProcessCallOrConstructENS1_5HintsENS_4base8OptionalIS3_EERKNS0_10ZoneVectorIS3_EENS0_12FeedbackSlotEb
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation22ProcessCallOrConstructENS1_5HintsENS_4base8OptionalIS3_EERKNS0_10ZoneVectorIS3_EENS0_12FeedbackSlotEb, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation22ProcessCallOrConstructENS1_5HintsENS_4base8OptionalIS3_EERKNS0_10ZoneVectorIS3_EENS0_12FeedbackSlotEb:
.LFB18993:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$1432, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -1432(%rbp)
	movq	%rsi, -1464(%rbp)
	movq	%rdx, -1456(%rbp)
	movq	%rcx, -1472(%rbp)
	movb	%r9b, -1445(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$-1, %r8d
	je	.L4379
	movl	%r8d, %r12d
	movq	%rdi, %rbx
	leaq	-784(%rbp), %r13
	call	_ZNK2v88internal8compiler34SerializerForBackgroundCompilation15feedback_vectorEv
	movl	%r12d, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS0_6HandleINS0_14FeedbackVectorEEENS0_12FeedbackSlotE@PLT
	movq	(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler12JSHeapBroker22ProcessFeedbackForCallERKNS1_14FeedbackSourceE@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation22BailoutOnUninitializedERKNS1_17ProcessedFeedbackE
	testb	%al, %al
	jne	.L4316
	movl	$1, -1444(%rbp)
	movl	(%r12), %eax
	testl	%eax, %eax
	jne	.L4441
.L4317:
	movq	-1432(%rbp), %rax
	movq	24(%rax), %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment17accumulator_hintsEv
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler5Hints5ClearEv
	movq	-1464(%rbp), %rax
	leaq	-192(%rbp), %rcx
	movq	%rcx, -1400(%rbp)
	movq	88(%rax), %rsi
	addq	$72, %rax
	movq	%rax, -1440(%rbp)
	movq	%rsi, -1416(%rbp)
	cmpq	%rsi, %rax
	je	.L4370
	.p2align 4,,10
	.p2align 3
.L4325:
	movq	-1416(%rbp), %rax
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
	movq	32(%rax), %r8
	movq	-1432(%rbp), %rax
	movq	$0, -1016(%rbp)
	movq	$0, -1008(%rbp)
	movq	$0, -1000(%rbp)
	movq	16(%rax), %rax
	movq	%rax, -1024(%rbp)
	movq	(%r8), %rdx
	testb	$1, %dl
	je	.L4440
	jmp	.L4442
	.p2align 4,,10
	.p2align 3
.L4372:
	movq	%r13, %rdi
	addq	$224, %r13
	call	_ZN2v88internal8compiler5HintsD1Ev
.L4440:
	cmpq	%r12, %r13
	jne	.L4372
.L4371:
	movq	-1416(%rbp), %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, -1416(%rbp)
	cmpq	%rax, -1440(%rbp)
	jne	.L4325
.L4370:
	movq	-1464(%rbp), %rax
	leaq	-768(%rbp), %rbx
	movq	200(%rax), %r12
	addq	$184, %rax
	movq	%rax, -1352(%rbp)
	cmpq	%r12, %rax
	je	.L4316
	movq	-1432(%rbp), %r14
	movq	-1472(%rbp), %r15
	.p2align 4,,10
	.p2align 3
.L4326:
	movq	32(%r12), %rax
	leaq	48(%r12), %rsi
	movq	%rbx, %rdi
	movq	%rax, -784(%rbp)
	movq	40(%r12), %rax
	movq	%rax, -776(%rbp)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movl	-1444(%rbp), %ecx
	movq	%r15, %rdx
	movq	%r14, %rdi
	movq	-784(%rbp), %rsi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation28ProcessSFIForCallOrConstructENS0_6HandleINS0_18SharedFunctionInfoEEERKNS0_10ZoneVectorINS1_5HintsEEENS0_15SpeculationModeE
	testb	%al, %al
	je	.L4377
	movq	24(%r14), %rsi
	movl	252(%rsi), %eax
	addl	248(%rsi), %eax
	movq	936(%rsi), %rdx
	movslq	%eax, %rcx
	js	.L4375
	movq	944(%rsi), %rax
	movabsq	$7905747460161236407, %rsi
	subq	%rdx, %rax
	sarq	$5, %rax
	imulq	%rsi, %rax
	cmpq	%rax, %rcx
	jb	.L4375
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4375:
	leaq	0(,%rcx,8), %r13
	movb	$0, -1024(%rbp)
	movzbl	-1445(%rbp), %r9d
	subq	%rcx, %r13
	movb	$0, -1016(%rbp)
	salq	$5, %r13
	leaq	(%rdx,%r13), %rax
	movq	%rax, -1320(%rbp)
	movq	-1456(%rbp), %rax
	cmpb	$0, (%rax)
	jne	.L4443
.L4376:
	movq	-784(%rbp), %rax
	leaq	-528(%rbp), %r13
	movq	%rbx, %rsi
	movl	%r9d, -1344(%rbp)
	movq	%r13, %rdi
	movq	%rax, -544(%rbp)
	movq	-776(%rbp), %rax
	movq	%rax, -536(%rbp)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movq	-544(%rbp), %rax
	leaq	-288(%rbp), %r10
	movq	%r13, %rsi
	movq	%r10, %rdi
	leaq	-304(%rbp), %rdx
	movq	%r10, -1336(%rbp)
	movq	%rax, -304(%rbp)
	movq	-536(%rbp), %rax
	movq	%rdx, -1328(%rbp)
	movq	%rax, -296(%rbp)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movq	-1328(%rbp), %rdx
	movq	%r15, %r8
	movl	-1344(%rbp), %r9d
	leaq	-1248(%rbp), %r11
	leaq	-1024(%rbp), %rcx
	movq	%r14, %rsi
	movq	$0, -64(%rbp)
	movq	%r11, %rdi
	movq	%r11, -1328(%rbp)
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation18RunChildSerializerENS1_18CompilationSubjectENS_4base8OptionalINS1_5HintsEEERKNS0_10ZoneVectorIS6_EEb
	movq	-1328(%rbp), %r11
	movq	-1320(%rbp), %rdi
	movq	%r11, %rsi
	call	_ZN2v88internal8compiler5Hints3AddERKS2_
	movq	-1328(%rbp), %r11
	movq	%r11, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	movq	-1336(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpb	$0, -1024(%rbp)
	je	.L4377
	leaq	-1016(%rbp), %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
.L4377:
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rax, -1352(%rbp)
	jne	.L4326
.L4316:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4444
	addq	$1432, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4442:
	.cfi_restore_state
	movq	-1(%rdx), %rax
	cmpw	$1104, 11(%rax)
	je	.L4329
	movq	-1(%rdx), %rax
	movq	%r13, %r12
	cmpw	$1105, 11(%rax)
	jne	.L4440
	movq	-1472(%rbp), %r12
	movq	%r8, %r13
.L4364:
	movl	-1444(%rbp), %ecx
	movq	-1432(%rbp), %rdi
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation31ProcessCalleeForCallOrConstructENS0_6HandleINS0_10JSFunctionEEERKNS0_10ZoneVectorINS1_5HintsEEENS0_15SpeculationModeE
	testb	%al, %al
	jne	.L4445
.L4366:
	movq	-1008(%rbp), %rbx
	movq	-1016(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L4371
	.p2align 4,,10
	.p2align 3
.L4369:
	movq	%r12, %rdi
	addq	$224, %r12
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	%r12, %rbx
	jne	.L4369
	jmp	.L4371
	.p2align 4,,10
	.p2align 3
.L4443:
	leaq	8(%rax), %rsi
	leaq	-1016(%rbp), %rdi
	movl	%r9d, -1328(%rbp)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movb	$1, -1024(%rbp)
	movl	-1328(%rbp), %r9d
	jmp	.L4376
	.p2align 4,,10
	.p2align 3
.L4329:
	movq	-1432(%rbp), %rbx
	leaq	-1296(%rbp), %r12
	movl	$1, %ecx
	movq	%r8, %rdx
	movq	%r12, %rdi
	leaq	-1280(%rbp), %r14
	movq	(%rbx), %rsi
	movq	%r14, %r15
	call	_ZN2v88internal8compiler18JSBoundFunctionRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler18JSBoundFunctionRef9SerializeEv@PLT
	movq	(%rbx), %rbx
	movq	%r12, %rdi
	movq	%rbx, -1384(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef12AsJSReceiverEv@PLT
	movq	%r15, %rdi
	movq	%rax, -1280(%rbp)
	movq	%rdx, -1272(%rbp)
	movq	16(%rbx), %rax
	movq	$0, -536(%rbp)
	movq	%rax, -544(%rbp)
	leaq	-784(%rbp), %rax
	movq	$0, -528(%rbp)
	movq	$0, -520(%rbp)
	movq	%rax, -1320(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef17IsJSBoundFunctionEv@PLT
	testb	%al, %al
	je	.L4332
	.p2align 4,,10
	.p2align 3
.L4447:
	movq	%r15, %rdi
	leaq	-304(%rbp), %r12
	leaq	-248(%rbp), %r14
	call	_ZNK2v88internal8compiler9ObjectRef17AsJSBoundFunctionEv@PLT
	movq	-1320(%rbp), %rdi
	movq	%rax, -784(%rbp)
	movq	%rdx, -776(%rbp)
	call	_ZNK2v88internal8compiler18JSBoundFunctionRef15bound_argumentsEv@PLT
	movq	%r12, %rdi
	movq	%rax, -304(%rbp)
	movq	%rdx, -296(%rbp)
	call	_ZNK2v88internal8compiler17FixedArrayBaseRef6lengthEv@PLT
	subl	$1, %eax
	movl	%eax, %ebx
	leaq	-1248(%rbp), %rax
	movq	%rax, -1336(%rbp)
	leaq	-288(%rbp), %rax
	movq	%rax, -1368(%rbp)
	leaq	-232(%rbp), %rax
	movq	%rax, -1344(%rbp)
	leaq	-176(%rbp), %rax
	movq	%rax, -1352(%rbp)
	leaq	-120(%rbp), %rax
	movq	%rax, -1360(%rbp)
	leaq	-1264(%rbp), %rax
	movq	%rax, -1328(%rbp)
	js	.L4352
	leaq	-1304(%rbp), %rax
	movq	%r15, -1376(%rbp)
	movq	%rax, -1392(%rbp)
	leaq	-544(%rbp), %rax
	movq	%rax, -1424(%rbp)
	leaq	-136(%rbp), %rax
	movq	%rax, -1408(%rbp)
	.p2align 4,,10
	.p2align 3
.L4333:
	movq	-1384(%rbp), %rax
	movq	-1376(%rbp), %rdi
	movq	16(%rax), %r13
	call	_ZNK2v88internal8compiler9ObjectRef17AsJSBoundFunctionEv@PLT
	movq	-1328(%rbp), %rdi
	movq	%rax, -1264(%rbp)
	movq	%rdx, -1256(%rbp)
	call	_ZNK2v88internal8compiler18JSBoundFunctionRef15bound_argumentsEv@PLT
	movq	-1336(%rbp), %rdi
	movl	%ebx, %esi
	movq	%rax, -1248(%rbp)
	movq	%rdx, -1240(%rbp)
	call	_ZNK2v88internal8compiler13FixedArrayRef3getEi@PLT
	movq	-1320(%rbp), %rdi
	movq	%rdx, -776(%rbp)
	movq	%rax, -784(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef6objectEv@PLT
	movq	-1368(%rbp), %rcx
	movq	-1344(%rbp), %rdx
	movq	%r14, %rdi
	movq	-1392(%rbp), %rsi
	movq	%r13, -304(%rbp)
	movq	%rcx, -272(%rbp)
	movq	%rcx, -264(%rbp)
	movq	-1352(%rbp), %rcx
	movq	%rdx, -216(%rbp)
	movq	%rdx, -208(%rbp)
	movq	-1360(%rbp), %rdx
	movl	$0, -288(%rbp)
	movq	$0, -280(%rbp)
	movq	$0, -256(%rbp)
	movq	%r13, -248(%rbp)
	movl	$0, -232(%rbp)
	movq	$0, -224(%rbp)
	movq	$0, -200(%rbp)
	movq	%r13, -192(%rbp)
	movl	$0, -176(%rbp)
	movq	$0, -168(%rbp)
	movq	%rcx, -160(%rbp)
	movq	%rcx, -152(%rbp)
	movq	$0, -144(%rbp)
	movq	%r13, -136(%rbp)
	movl	$0, -120(%rbp)
	movq	$0, -112(%rbp)
	movq	%rdx, -104(%rbp)
	movq	%rdx, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rax, -1304(%rbp)
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_
	movq	-528(%rbp), %rdi
	cmpq	-520(%rbp), %rdi
	je	.L4336
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	addq	$224, -528(%rbp)
.L4337:
	movq	-112(%rbp), %r13
	movq	-1408(%rbp), %r15
	testq	%r13, %r13
	je	.L4341
.L4338:
	movq	24(%r13), %rsi
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal8compiler17FunctionBlueprintES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE8_M_eraseEPSt13_Rb_tree_nodeIS3_E
	movq	%r13, %rdi
	movq	16(%r13), %r13
	addq	$48, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	testq	%r13, %r13
	jne	.L4338
.L4341:
	movq	-168(%rbp), %r13
	testq	%r13, %r13
	je	.L4339
.L4340:
	movq	24(%r13), %r15
	testq	%r15, %r15
	je	.L4342
.L4343:
	movq	24(%r15), %rsi
	movq	-1400(%rbp), %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_3MapEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	movq	16(%r15), %r15
	testq	%r15, %r15
	jne	.L4343
.L4342:
	movq	16(%r13), %r13
	testq	%r13, %r13
	jne	.L4340
.L4339:
	movq	-224(%rbp), %r13
	testq	%r13, %r13
	je	.L4344
.L4347:
	movq	24(%r13), %r15
	testq	%r15, %r15
	je	.L4345
.L4346:
	movq	24(%r15), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	movq	16(%r15), %r15
	testq	%r15, %r15
	jne	.L4346
.L4345:
	movq	16(%r13), %r13
	testq	%r13, %r13
	jne	.L4347
.L4344:
	movq	-280(%rbp), %r13
	testq	%r13, %r13
	je	.L4348
.L4351:
	movq	24(%r13), %r15
	testq	%r15, %r15
	je	.L4349
.L4350:
	movq	24(%r15), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal8compiler14VirtualContextES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE8_M_eraseEPSt13_Rb_tree_nodeIS3_E
	movq	16(%r15), %r15
	testq	%r15, %r15
	jne	.L4350
.L4349:
	movq	16(%r13), %r13
	testq	%r13, %r13
	jne	.L4351
.L4348:
	subl	$1, %ebx
	cmpl	$-1, %ebx
	jne	.L4333
	movq	-1376(%rbp), %r15
.L4352:
	movq	-1384(%rbp), %rax
	movq	%r15, %rdi
	movq	16(%rax), %rbx
	call	_ZNK2v88internal8compiler9ObjectRef17AsJSBoundFunctionEv@PLT
	movq	-1336(%rbp), %rdi
	movq	%rax, -1248(%rbp)
	movq	%rdx, -1240(%rbp)
	call	_ZNK2v88internal8compiler18JSBoundFunctionRef10bound_thisEv@PLT
	movq	-1320(%rbp), %rdi
	movq	%rdx, -776(%rbp)
	movq	%rax, -784(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef6objectEv@PLT
	movq	-1368(%rbp), %rcx
	movq	-1344(%rbp), %rdx
	movq	%r14, %rdi
	movq	-1328(%rbp), %rsi
	movq	%rbx, -304(%rbp)
	movq	%rcx, -272(%rbp)
	movq	%rcx, -264(%rbp)
	movq	-1352(%rbp), %rcx
	movq	%rdx, -216(%rbp)
	movq	%rdx, -208(%rbp)
	movq	-1360(%rbp), %rdx
	movl	$0, -288(%rbp)
	movq	$0, -280(%rbp)
	movq	$0, -256(%rbp)
	movq	%rbx, -248(%rbp)
	movl	$0, -232(%rbp)
	movq	$0, -224(%rbp)
	movq	$0, -200(%rbp)
	movq	%rbx, -192(%rbp)
	movl	$0, -176(%rbp)
	movq	$0, -168(%rbp)
	movq	%rcx, -160(%rbp)
	movq	%rcx, -152(%rbp)
	movq	$0, -144(%rbp)
	movq	%rbx, -136(%rbp)
	movl	$0, -120(%rbp)
	movq	$0, -112(%rbp)
	movq	%rdx, -104(%rbp)
	movq	%rdx, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rax, -1264(%rbp)
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_
	movq	-528(%rbp), %rdi
	cmpq	-520(%rbp), %rdi
	je	.L4446
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	addq	$224, -528(%rbp)
.L4353:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef17AsJSBoundFunctionEv@PLT
	movq	%r12, %rdi
	movq	%rax, -304(%rbp)
	movq	%rdx, -296(%rbp)
	call	_ZNK2v88internal8compiler18JSBoundFunctionRef21bound_target_functionEv@PLT
	movq	%r15, %rdi
	movq	%rax, -1280(%rbp)
	movq	%rdx, -1272(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef17IsJSBoundFunctionEv@PLT
	testb	%al, %al
	jne	.L4447
.L4332:
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsJSFunctionEv@PLT
	testb	%al, %al
	jne	.L4355
	movq	-528(%rbp), %r12
	movq	-536(%rbp), %r13
	cmpq	%r12, %r13
	je	.L4360
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L4359:
	movq	%r13, %rdi
	addq	$224, %r13
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	%r12, %r13
	jne	.L4359
.L4358:
	movq	%rbx, %r13
	leaq	-1024(%rbp), %r12
	testq	%rbx, %rbx
	jne	.L4364
.L4360:
	movq	-1008(%rbp), %r12
	movq	-1016(%rbp), %r13
	jmp	.L4440
	.p2align 4,,10
	.p2align 3
.L4336:
	movq	%rdi, %rsi
	movq	-1424(%rbp), %rdi
	movq	%r12, %rdx
	call	_ZNSt6vectorIN2v88internal8compiler5HintsENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	jmp	.L4337
	.p2align 4,,10
	.p2align 3
.L4446:
	leaq	-544(%rbp), %r8
	movq	%rdi, %rsi
	movq	%r12, %rdx
	movq	%r8, %rdi
	call	_ZNSt6vectorIN2v88internal8compiler5HintsENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	jmp	.L4353
.L4445:
	movq	-1432(%rbp), %rax
	movq	24(%rax), %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment17accumulator_hintsEv
	movb	$0, -544(%rbp)
	movzbl	-1445(%rbp), %ebx
	movq	%rax, %r14
	movq	-1456(%rbp), %rax
	movb	$0, -536(%rbp)
	cmpb	$0, (%rax)
	jne	.L4448
.L4367:
	movq	-1432(%rbp), %rax
	leaq	-304(%rbp), %r15
	movq	%r13, %rsi
	leaq	-784(%rbp), %r13
	movq	%r15, %rdi
	movq	16(%rax), %rcx
	movq	(%rax), %rax
	movq	(%rax), %rdx
	call	_ZN2v88internal8compiler18CompilationSubjectC1ENS0_6HandleINS0_10JSFunctionEEEPNS0_7IsolateEPNS0_4ZoneE
	movl	%ebx, %r9d
	movq	%r12, %r8
	movq	%r15, %rdx
	movq	-1432(%rbp), %rsi
	leaq	-544(%rbp), %rcx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation18RunChildSerializerENS1_18CompilationSubjectENS_4base8OptionalINS1_5HintsEEERKNS0_10ZoneVectorIS6_EEb
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler5Hints3AddERKS2_
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	leaq	-288(%rbp), %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpb	$0, -544(%rbp)
	je	.L4366
	leaq	-536(%rbp), %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	jmp	.L4366
.L4379:
	movl	$1, -1444(%rbp)
	jmp	.L4317
.L4355:
	movq	-536(%rbp), %rax
	leaq	-304(%rbp), %r12
	leaq	-1024(%rbp), %r13
	movq	-1008(%rbp), %rsi
	movq	%r12, %rcx
	movq	%r13, %rdi
	leaq	-784(%rbp), %rdx
	movq	%rax, -304(%rbp)
	movq	-528(%rbp), %rax
	movq	%rax, -784(%rbp)
	call	_ZNSt6vectorIN2v88internal8compiler5HintsENS1_13ZoneAllocatorIS3_EEE15_M_range_insertISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPS3_S6_EEEEEvSC_T_SE_St20forward_iterator_tag
	movq	-1472(%rbp), %rax
	movq	-1008(%rbp), %rsi
	movq	%r13, %rdi
	movq	16(%rax), %rcx
	movq	8(%rax), %rdx
	call	_ZNSt6vectorIN2v88internal8compiler5HintsENS1_13ZoneAllocatorIS3_EEE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPKS3_S6_EEEEvNS9_IPS3_S6_EET_SF_St20forward_iterator_tag
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12AsJSFunctionEv@PLT
	movq	%r12, %rdi
	movq	%rax, -304(%rbp)
	movq	%rdx, -296(%rbp)
	call	_ZNK2v88internal8compiler13JSFunctionRef6objectEv@PLT
	movq	-528(%rbp), %r12
	movq	-536(%rbp), %r13
	movq	%rax, %rbx
	cmpq	%r13, %r12
	jne	.L4359
	jmp	.L4358
.L4441:
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler17ProcessedFeedback6AsCallEv@PLT
	movq	%r12, %rdi
	movl	36(%rax), %eax
	movl	%eax, -1444(%rbp)
	call	_ZNK2v88internal8compiler17ProcessedFeedback6AsCallEv@PLT
	movdqu	8(%rax), %xmm0
	movaps	%xmm0, -304(%rbp)
	movq	24(%rax), %rax
	cmpb	$0, -304(%rbp)
	movq	%rax, -288(%rbp)
	je	.L4317
	leaq	-296(%rbp), %r13
	leaq	-544(%rbp), %r12
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler13HeapObjectRef3mapEv@PLT
	movq	%r12, %rdi
	movq	%rax, -544(%rbp)
	movq	%rdx, -536(%rbp)
	call	_ZNK2v88internal8compiler6MapRef11is_callableEv@PLT
	testb	%al, %al
	je	.L4317
	movq	-1456(%rbp), %rbx
	movq	-1464(%rbp), %rax
	movq	%r13, %rdi
	cmpb	$0, (%rbx)
	leaq	56(%rax), %r14
	je	.L4430
	call	_ZNK2v88internal8compiler13HeapObjectRef6objectEv@PLT
	leaq	64(%rbx), %rdi
	movq	%r12, %rsi
	movq	%rax, -544(%rbp)
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_
	movq	%r13, %rdi
.L4430:
	call	_ZNK2v88internal8compiler13HeapObjectRef6objectEv@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, -544(%rbp)
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_
	jmp	.L4317
.L4448:
	leaq	8(%rax), %rsi
	leaq	-536(%rbp), %rdi
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movb	$1, -544(%rbp)
	jmp	.L4367
.L4444:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18993:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation22ProcessCallOrConstructENS1_5HintsENS_4base8OptionalIS3_EERKNS0_10ZoneVectorIS3_EENS0_12FeedbackSlotEb, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation22ProcessCallOrConstructENS1_5HintsENS_4base8OptionalIS3_EERKNS0_10ZoneVectorIS3_EENS0_12FeedbackSlotEb
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation27VisitCallUndefinedReceiver0EPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation27VisitCallUndefinedReceiver0EPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation27VisitCallUndefinedReceiver0EPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation27VisitCallUndefinedReceiver0EPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB18969:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-528(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	xorl	%esi, %esi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$984, %rsp
	.cfi_offset 3, -56
	movq	24(%rdi), %r14
	movq	%r13, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r14, %rdi
	leaq	-288(%rbp), %r14
	movl	%eax, %esi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment14register_hintsENS0_11interpreter8RegisterE
	movq	%r13, %rdi
	movl	$1, %esi
	leaq	-976(%rbp), %r13
	movq	%rax, -1016(%rbp)
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	movq	(%r12), %rdx
	leaq	-960(%rbp), %rcx
	movq	%r15, %rsi
	movl	%eax, %ebx
	movq	16(%r12), %rax
	leaq	-920(%rbp), %rdi
	movq	(%rdx), %rdx
	movq	%rcx, -944(%rbp)
	movq	%rcx, -936(%rbp)
	leaq	-904(%rbp), %rcx
	movq	%rax, -976(%rbp)
	addq	$88, %rdx
	movq	%rax, -920(%rbp)
	movq	%rcx, -888(%rbp)
	movq	%rcx, -880(%rbp)
	leaq	-848(%rbp), %rcx
	movq	%rax, -864(%rbp)
	movq	%rax, -808(%rbp)
	leaq	-792(%rbp), %rax
	movq	%rcx, -832(%rbp)
	movq	%rcx, -824(%rbp)
	movl	$0, -960(%rbp)
	movq	$0, -952(%rbp)
	movq	$0, -928(%rbp)
	movl	$0, -904(%rbp)
	movq	$0, -896(%rbp)
	movq	$0, -872(%rbp)
	movl	$0, -848(%rbp)
	movq	$0, -840(%rbp)
	movq	$0, -816(%rbp)
	movl	$0, -792(%rbp)
	movq	$0, -784(%rbp)
	movq	%rax, -776(%rbp)
	movq	%rax, -768(%rbp)
	movq	$0, -760(%rbp)
	movq	%rdx, -528(%rbp)
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_
	movq	16(%r12), %r8
	movq	%r14, %rdi
	movq	%r13, %rsi
	movq	%r8, -1024(%rbp)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movq	-1024(%rbp), %r8
	movq	$0, -984(%rbp)
	movq	$0, -992(%rbp)
	movq	24(%r8), %rax
	movq	16(%r8), %rdi
	movq	%r8, -1008(%rbp)
	movq	$0, -1000(%rbp)
	subq	%rdi, %rax
	cmpq	$223, %rax
	jbe	.L4458
	leaq	224(%rdi), %rax
	movq	%rax, 16(%r8)
.L4451:
	movq	%r14, %rsi
	movq	%rdi, -1000(%rbp)
	movq	%rax, -984(%rbp)
	movq	%rax, -1024(%rbp)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movq	-1024(%rbp), %rax
	movq	%r14, %rdi
	leaq	-752(%rbp), %r14
	movq	%rax, -992(%rbp)
	call	_ZN2v88internal8compiler5HintsD1Ev
	movq	-1016(%rbp), %rsi
	movq	%r14, %rdi
	movb	$0, -528(%rbp)
	movb	$0, -520(%rbp)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	xorl	%r9d, %r9d
	movl	%ebx, %r8d
	movq	%r15, %rdx
	leaq	-1008(%rbp), %rcx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation22ProcessCallOrConstructENS1_5HintsENS_4base8OptionalIS3_EERKNS0_10ZoneVectorIS3_EENS0_12FeedbackSlotEb
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpb	$0, -528(%rbp)
	jne	.L4459
.L4452:
	movq	-992(%rbp), %r12
	movq	-1000(%rbp), %rbx
	cmpq	%rbx, %r12
	je	.L4453
	.p2align 4,,10
	.p2align 3
.L4454:
	movq	%rbx, %rdi
	addq	$224, %rbx
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	%rbx, %r12
	jne	.L4454
.L4453:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4460
	addq	$984, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4459:
	.cfi_restore_state
	leaq	-520(%rbp), %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	jmp	.L4452
	.p2align 4,,10
	.p2align 3
.L4458:
	movq	%r8, %rdi
	movl	$224, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdi
	leaq	224(%rax), %rax
	jmp	.L4451
.L4460:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18969:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation27VisitCallUndefinedReceiver0EPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation27VisitCallUndefinedReceiver0EPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation27VisitCallUndefinedReceiver1EPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation27VisitCallUndefinedReceiver1EPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation27VisitCallUndefinedReceiver1EPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation27VisitCallUndefinedReceiver1EPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB18976:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	xorl	%esi, %esi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-512(%rbp), %rbx
	subq	$1224, %rsp
	movq	24(%rdi), %r14
	movq	%r13, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r14, %rdi
	leaq	-752(%rbp), %r14
	movl	%eax, %esi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment14register_hintsENS0_11interpreter8RegisterE
	movq	24(%r12), %r15
	movq	%r13, %rdi
	movl	$1, %esi
	movq	%rax, -1240(%rbp)
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r15, %rdi
	leaq	-288(%rbp), %r15
	movl	%eax, %esi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment14register_hintsENS0_11interpreter8RegisterE
	movq	%r13, %rdi
	movl	$2, %esi
	leaq	-1200(%rbp), %r13
	movq	%rax, -1264(%rbp)
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	movq	(%r12), %rdx
	leaq	-1184(%rbp), %rcx
	movq	%r14, %rsi
	movl	%eax, -1244(%rbp)
	movq	16(%r12), %rax
	leaq	-1144(%rbp), %rdi
	movq	(%rdx), %rdx
	movq	%rcx, -1168(%rbp)
	movq	%rcx, -1160(%rbp)
	leaq	-1128(%rbp), %rcx
	movq	%rax, -1200(%rbp)
	addq	$88, %rdx
	movq	%rax, -1144(%rbp)
	movq	%rcx, -1112(%rbp)
	movq	%rcx, -1104(%rbp)
	leaq	-1072(%rbp), %rcx
	movq	%rax, -1088(%rbp)
	movq	%rax, -1032(%rbp)
	leaq	-1016(%rbp), %rax
	movq	%rcx, -1056(%rbp)
	movq	%rcx, -1048(%rbp)
	movl	$0, -1184(%rbp)
	movq	$0, -1176(%rbp)
	movq	$0, -1152(%rbp)
	movl	$0, -1128(%rbp)
	movq	$0, -1120(%rbp)
	movq	$0, -1096(%rbp)
	movl	$0, -1072(%rbp)
	movq	$0, -1064(%rbp)
	movq	$0, -1040(%rbp)
	movl	$0, -1016(%rbp)
	movq	$0, -1008(%rbp)
	movq	%rax, -1000(%rbp)
	movq	%rax, -992(%rbp)
	movq	$0, -984(%rbp)
	movq	%rdx, -752(%rbp)
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_
	movq	16(%r12), %r11
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%r11, -1256(%rbp)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movq	-1264(%rbp), %r9
	movq	%r15, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movq	-1256(%rbp), %r11
	movq	$0, -1208(%rbp)
	movq	$0, -1216(%rbp)
	movq	24(%r11), %rax
	movq	16(%r11), %rdi
	movq	%r11, -1232(%rbp)
	movq	$0, -1224(%rbp)
	subq	%rdi, %rax
	cmpq	$447, %rax
	jbe	.L4470
	leaq	448(%rdi), %rax
	movq	%rax, 16(%r11)
.L4463:
	movq	%rbx, %rsi
	movq	%rax, -1208(%rbp)
	movq	%rax, -1264(%rbp)
	movq	%rdi, -1224(%rbp)
	movq	%rdi, -1256(%rbp)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movq	-1256(%rbp), %rdi
	movq	%r15, %rsi
	addq	$224, %rdi
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movq	-1264(%rbp), %rax
	movq	%r15, %rdi
	leaq	-976(%rbp), %r15
	movq	%rax, -1216(%rbp)
	call	_ZN2v88internal8compiler5HintsD1Ev
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	movq	-1240(%rbp), %rsi
	movq	%r15, %rdi
	movb	$0, -752(%rbp)
	movb	$0, -744(%rbp)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	xorl	%r9d, %r9d
	movq	%r14, %rdx
	movq	%r15, %rsi
	movl	-1244(%rbp), %r8d
	leaq	-1232(%rbp), %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation22ProcessCallOrConstructENS1_5HintsENS_4base8OptionalIS3_EERKNS0_10ZoneVectorIS3_EENS0_12FeedbackSlotEb
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpb	$0, -752(%rbp)
	jne	.L4471
.L4464:
	movq	-1216(%rbp), %r12
	movq	-1224(%rbp), %rbx
	cmpq	%rbx, %r12
	je	.L4465
	.p2align 4,,10
	.p2align 3
.L4466:
	movq	%rbx, %rdi
	addq	$224, %rbx
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	%rbx, %r12
	jne	.L4466
.L4465:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4472
	addq	$1224, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4471:
	.cfi_restore_state
	leaq	-744(%rbp), %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	jmp	.L4464
	.p2align 4,,10
	.p2align 3
.L4470:
	movq	%r11, %rdi
	movl	$448, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdi
	leaq	448(%rax), %rax
	jmp	.L4463
.L4472:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18976:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation27VisitCallUndefinedReceiver1EPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation27VisitCallUndefinedReceiver1EPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation27VisitCallUndefinedReceiver2EPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation27VisitCallUndefinedReceiver2EPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation27VisitCallUndefinedReceiver2EPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation27VisitCallUndefinedReceiver2EPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB18977:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	xorl	%esi, %esi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$1480, %rsp
	.cfi_offset 3, -56
	movq	24(%rdi), %r14
	movq	%r13, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment14register_hintsENS0_11interpreter8RegisterE
	movq	24(%r12), %r14
	movq	%r13, %rdi
	movl	$1, %esi
	movq	%rax, -1480(%rbp)
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment14register_hintsENS0_11interpreter8RegisterE
	movq	24(%r12), %r14
	movq	%r13, %rdi
	movl	$2, %esi
	movq	%rax, %r15
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r14, %rdi
	leaq	-736(%rbp), %r14
	movl	%eax, %esi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment14register_hintsENS0_11interpreter8RegisterE
	movq	%r13, %rdi
	movl	$3, %esi
	movq	%rax, %rbx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	movq	(%r12), %rdx
	leaq	-1424(%rbp), %rcx
	leaq	-1368(%rbp), %rdi
	movl	%eax, -1484(%rbp)
	movq	16(%r12), %rax
	movq	(%rdx), %rdx
	movq	%rcx, -1464(%rbp)
	leaq	-1408(%rbp), %rcx
	movq	%rax, -1424(%rbp)
	movq	%rax, -1368(%rbp)
	addq	$88, %rdx
	movq	%rax, -1312(%rbp)
	movq	%rax, -1256(%rbp)
	leaq	-1240(%rbp), %rax
	movq	%rcx, -1392(%rbp)
	movq	%rcx, -1384(%rbp)
	leaq	-1352(%rbp), %rcx
	movq	%rax, -1224(%rbp)
	movq	%rax, -1216(%rbp)
	leaq	-976(%rbp), %rax
	movq	%rcx, -1336(%rbp)
	movq	%rax, %rsi
	movq	%rcx, -1328(%rbp)
	leaq	-1296(%rbp), %rcx
	movq	%rcx, -1280(%rbp)
	movq	%rcx, -1272(%rbp)
	movl	$0, -1408(%rbp)
	movq	$0, -1400(%rbp)
	movq	$0, -1376(%rbp)
	movl	$0, -1352(%rbp)
	movq	$0, -1344(%rbp)
	movq	$0, -1320(%rbp)
	movl	$0, -1296(%rbp)
	movq	$0, -1288(%rbp)
	movq	$0, -1264(%rbp)
	movl	$0, -1240(%rbp)
	movq	$0, -1232(%rbp)
	movq	$0, -1208(%rbp)
	movq	%rdx, -976(%rbp)
	movq	%rax, -1496(%rbp)
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_
	movq	-1464(%rbp), %rsi
	movq	%r14, %rdi
	movq	16(%r12), %r13
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	leaq	-512(%rbp), %rax
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -1504(%rbp)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	leaq	-288(%rbp), %rax
	movq	%rbx, %rsi
	movq	%rax, %rdi
	movq	%rax, -1512(%rbp)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movq	%r13, -1456(%rbp)
	movq	$0, -1432(%rbp)
	movq	24(%r13), %rax
	movq	$0, -1440(%rbp)
	movq	16(%r13), %rbx
	movq	$0, -1448(%rbp)
	subq	%rbx, %rax
	cmpq	$671, %rax
	jbe	.L4484
	leaq	672(%rbx), %rax
	movq	%rax, -1472(%rbp)
	movq	%rax, 16(%r13)
.L4475:
	movq	%rax, -1432(%rbp)
	movq	%r14, %r15
	leaq	-64(%rbp), %r13
	movq	%rbx, -1448(%rbp)
	subq	%r14, %rbx
	.p2align 4,,10
	.p2align 3
.L4476:
	leaq	(%r15,%rbx), %rdi
	movq	%r15, %rsi
	addq	$224, %r15
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	cmpq	%r13, %r15
	jne	.L4476
	movq	-1472(%rbp), %rax
	movq	-1512(%rbp), %rdi
	leaq	-1200(%rbp), %r13
	movq	%rax, -1440(%rbp)
	call	_ZN2v88internal8compiler5HintsD1Ev
	movq	-1504(%rbp), %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	movq	-1480(%rbp), %rsi
	movq	%r13, %rdi
	movb	$0, -976(%rbp)
	movb	$0, -968(%rbp)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	xorl	%r9d, %r9d
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	-1484(%rbp), %r8d
	movq	-1496(%rbp), %rdx
	leaq	-1456(%rbp), %rcx
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation22ProcessCallOrConstructENS1_5HintsENS_4base8OptionalIS3_EERKNS0_10ZoneVectorIS3_EENS0_12FeedbackSlotEb
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpb	$0, -976(%rbp)
	jne	.L4485
.L4477:
	movq	-1440(%rbp), %r12
	movq	-1448(%rbp), %rbx
	cmpq	%rbx, %r12
	je	.L4478
	.p2align 4,,10
	.p2align 3
.L4479:
	movq	%rbx, %rdi
	addq	$224, %rbx
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	%rbx, %r12
	jne	.L4479
.L4478:
	movq	-1464(%rbp), %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4486
	addq	$1480, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4485:
	.cfi_restore_state
	leaq	-968(%rbp), %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	jmp	.L4477
	.p2align 4,,10
	.p2align 3
.L4484:
	movl	$672, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rbx
	leaq	672(%rax), %rax
	movq	%rax, -1472(%rbp)
	jmp	.L4475
.L4486:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18977:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation27VisitCallUndefinedReceiver2EPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation27VisitCallUndefinedReceiver2EPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitCallProperty0EPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitCallProperty0EPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitCallProperty0EPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitCallProperty0EPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB18981:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	xorl	%esi, %esi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$760, %rsp
	.cfi_offset 3, -56
	movq	24(%rdi), %r14
	movq	%r13, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment14register_hintsENS0_11interpreter8RegisterE
	movq	24(%r12), %r15
	movq	%r13, %rdi
	movl	$1, %esi
	movq	%rax, %r14
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r15, %rdi
	leaq	-288(%rbp), %r15
	movl	%eax, %esi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment14register_hintsENS0_11interpreter8RegisterE
	movq	%r13, %rdi
	movl	$2, %esi
	movq	%rax, -792(%rbp)
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	movq	-792(%rbp), %r8
	movq	16(%r12), %r13
	movq	%r15, %rdi
	movl	%eax, %ebx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movq	%r13, -784(%rbp)
	movq	$0, -760(%rbp)
	movq	24(%r13), %rax
	movq	$0, -768(%rbp)
	movq	16(%r13), %rdi
	movq	$0, -776(%rbp)
	subq	%rdi, %rax
	cmpq	$223, %rax
	jbe	.L4496
	leaq	224(%rdi), %rax
	movq	%rax, 16(%r13)
.L4489:
	movq	%r15, %rsi
	movq	%rdi, -776(%rbp)
	leaq	-752(%rbp), %r13
	movq	%rax, -760(%rbp)
	movq	%rax, -792(%rbp)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movq	-792(%rbp), %rax
	movq	%r15, %rdi
	movq	%rax, -768(%rbp)
	call	_ZN2v88internal8compiler5HintsD1Ev
	movq	%r14, %rsi
	movq	%r13, %rdi
	movb	$0, -528(%rbp)
	movb	$0, -520(%rbp)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	xorl	%r9d, %r9d
	movl	%ebx, %r8d
	movq	%r13, %rsi
	leaq	-784(%rbp), %rcx
	leaq	-528(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation22ProcessCallOrConstructENS1_5HintsENS_4base8OptionalIS3_EERKNS0_10ZoneVectorIS3_EENS0_12FeedbackSlotEb
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpb	$0, -528(%rbp)
	jne	.L4497
.L4490:
	movq	-768(%rbp), %r12
	movq	-776(%rbp), %rbx
	cmpq	%rbx, %r12
	je	.L4487
	.p2align 4,,10
	.p2align 3
.L4492:
	movq	%rbx, %rdi
	addq	$224, %rbx
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	%rbx, %r12
	jne	.L4492
.L4487:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4498
	addq	$760, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4497:
	.cfi_restore_state
	leaq	-520(%rbp), %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	jmp	.L4490
	.p2align 4,,10
	.p2align 3
.L4496:
	movq	%r13, %rdi
	movl	$224, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdi
	leaq	224(%rax), %rax
	jmp	.L4489
.L4498:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18981:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitCallProperty0EPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitCallProperty0EPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitCallProperty1EPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitCallProperty1EPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitCallProperty1EPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitCallProperty1EPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB18982:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	xorl	%esi, %esi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$984, %rsp
	.cfi_offset 3, -56
	movq	24(%rdi), %r14
	movq	%r13, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment14register_hintsENS0_11interpreter8RegisterE
	movq	24(%r12), %r15
	movq	%r13, %rdi
	movl	$1, %esi
	movq	%rax, %r14
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r15, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment14register_hintsENS0_11interpreter8RegisterE
	movq	24(%r12), %rbx
	movq	%r13, %rdi
	movl	$2, %esi
	movq	%rax, %r15
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%rbx, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment14register_hintsENS0_11interpreter8RegisterE
	movq	%r13, %rdi
	movl	$3, %esi
	leaq	-512(%rbp), %r13
	movq	%rax, %rbx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	movq	16(%r12), %r8
	movq	%r15, %rsi
	movq	%r13, %rdi
	leaq	-288(%rbp), %r15
	movl	%eax, -1012(%rbp)
	movq	%r8, -1024(%rbp)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movq	-1024(%rbp), %r8
	movq	$0, -984(%rbp)
	movq	$0, -992(%rbp)
	movq	24(%r8), %rax
	movq	16(%r8), %rbx
	movq	%r8, -1008(%rbp)
	movq	$0, -1000(%rbp)
	subq	%rbx, %rax
	cmpq	$447, %rax
	jbe	.L4508
	leaq	448(%rbx), %rax
	movq	%rax, 16(%r8)
.L4501:
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%rax, -984(%rbp)
	movq	%rax, -1024(%rbp)
	movq	%rbx, -1000(%rbp)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movq	%r15, %rsi
	leaq	224(%rbx), %rdi
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movq	-1024(%rbp), %rax
	movq	%r15, %rdi
	leaq	-976(%rbp), %r15
	movq	%rax, -992(%rbp)
	call	_ZN2v88internal8compiler5HintsD1Ev
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	movq	%r14, %rsi
	movq	%r15, %rdi
	movb	$0, -752(%rbp)
	movb	$0, -744(%rbp)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	xorl	%r9d, %r9d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	-1012(%rbp), %r8d
	leaq	-1008(%rbp), %rcx
	leaq	-752(%rbp), %rdx
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation22ProcessCallOrConstructENS1_5HintsENS_4base8OptionalIS3_EERKNS0_10ZoneVectorIS3_EENS0_12FeedbackSlotEb
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpb	$0, -752(%rbp)
	jne	.L4509
.L4502:
	movq	-992(%rbp), %r12
	movq	-1000(%rbp), %rbx
	cmpq	%rbx, %r12
	je	.L4499
	.p2align 4,,10
	.p2align 3
.L4504:
	movq	%rbx, %rdi
	addq	$224, %rbx
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	%rbx, %r12
	jne	.L4504
.L4499:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4510
	addq	$984, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4509:
	.cfi_restore_state
	leaq	-744(%rbp), %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	jmp	.L4502
	.p2align 4,,10
	.p2align 3
.L4508:
	movl	$448, %esi
	movq	%r8, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rbx
	leaq	448(%rax), %rax
	jmp	.L4501
.L4510:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18982:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitCallProperty1EPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitCallProperty1EPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitCallProperty2EPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitCallProperty2EPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitCallProperty2EPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitCallProperty2EPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB18983:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	subq	$1240, %rsp
	.cfi_offset 3, -56
	movq	24(%rdi), %r14
	movq	%r12, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment14register_hintsENS0_11interpreter8RegisterE
	movq	24(%r13), %r14
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, -1248(%rbp)
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment14register_hintsENS0_11interpreter8RegisterE
	movq	24(%r13), %r14
	movq	%r12, %rdi
	movl	$2, %esi
	movq	%rax, -1240(%rbp)
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment14register_hintsENS0_11interpreter8RegisterE
	movq	24(%r13), %r14
	movq	%r12, %rdi
	movl	$3, %esi
	movq	%rax, %r15
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r14, %rdi
	leaq	-736(%rbp), %r14
	movl	%eax, %esi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment14register_hintsENS0_11interpreter8RegisterE
	movq	%r12, %rdi
	movl	$4, %esi
	movq	%rax, %rbx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	movq	-1240(%rbp), %r8
	movq	%r14, %rdi
	movq	16(%r13), %r12
	movl	%eax, -1252(%rbp)
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	leaq	-512(%rbp), %rax
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -1264(%rbp)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	leaq	-288(%rbp), %rax
	movq	%rbx, %rsi
	movq	%rax, %rdi
	movq	%rax, -1272(%rbp)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movq	%r12, -1232(%rbp)
	movq	$0, -1208(%rbp)
	movq	24(%r12), %rax
	movq	$0, -1216(%rbp)
	movq	16(%r12), %rbx
	movq	$0, -1224(%rbp)
	subq	%rbx, %rax
	cmpq	$671, %rax
	jbe	.L4522
	leaq	672(%rbx), %rax
	movq	%rax, -1240(%rbp)
	movq	%rax, 16(%r12)
.L4513:
	movq	%rax, -1208(%rbp)
	movq	%r14, %r15
	leaq	-64(%rbp), %r12
	movq	%rbx, -1224(%rbp)
	subq	%r14, %rbx
	.p2align 4,,10
	.p2align 3
.L4514:
	leaq	(%r15,%rbx), %rdi
	movq	%r15, %rsi
	addq	$224, %r15
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	cmpq	%r12, %r15
	jne	.L4514
	movq	-1240(%rbp), %rax
	movq	-1272(%rbp), %rdi
	leaq	-1200(%rbp), %r12
	movq	%rax, -1216(%rbp)
	call	_ZN2v88internal8compiler5HintsD1Ev
	movq	-1264(%rbp), %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	movq	-1248(%rbp), %rsi
	movq	%r12, %rdi
	movb	$0, -976(%rbp)
	movb	$0, -968(%rbp)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	xorl	%r9d, %r9d
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	-1252(%rbp), %r8d
	leaq	-1232(%rbp), %rcx
	leaq	-976(%rbp), %rdx
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation22ProcessCallOrConstructENS1_5HintsENS_4base8OptionalIS3_EERKNS0_10ZoneVectorIS3_EENS0_12FeedbackSlotEb
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpb	$0, -976(%rbp)
	jne	.L4523
.L4515:
	movq	-1216(%rbp), %r12
	movq	-1224(%rbp), %rbx
	cmpq	%rbx, %r12
	je	.L4511
	.p2align 4,,10
	.p2align 3
.L4517:
	movq	%rbx, %rdi
	addq	$224, %rbx
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	%rbx, %r12
	jne	.L4517
.L4511:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4524
	addq	$1240, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4523:
	.cfi_restore_state
	leaq	-968(%rbp), %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	jmp	.L4515
	.p2align 4,,10
	.p2align 3
.L4522:
	movl	$672, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rbx
	leaq	672(%rax), %rax
	movq	%rax, -1240(%rbp)
	jmp	.L4513
.L4524:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18983:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitCallProperty2EPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitCallProperty2EPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation18ProcessCallVarArgsENS0_19ConvertReceiverModeERKNS1_5HintsENS0_11interpreter8RegisterEiNS0_12FeedbackSlotEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation18ProcessCallVarArgsENS0_19ConvertReceiverModeERKNS1_5HintsENS0_11interpreter8RegisterEiNS0_12FeedbackSlotEb
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation18ProcessCallVarArgsENS0_19ConvertReceiverModeERKNS1_5HintsENS0_11interpreter8RegisterEiNS0_12FeedbackSlotEb, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation18ProcessCallVarArgsENS0_19ConvertReceiverModeERKNS1_5HintsENS0_11interpreter8RegisterEiNS0_12FeedbackSlotEb:
.LFB18995:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$552, %rsp
	movq	%rdx, -560(%rbp)
	movq	16(%rdi), %rdx
	movl	%r8d, -548(%rbp)
	movl	%r9d, -552(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdx, -544(%rbp)
	movq	$0, -536(%rbp)
	movq	$0, -528(%rbp)
	movq	$0, -520(%rbp)
	testl	%esi, %esi
	je	.L4563
.L4526:
	movl	-548(%rbp), %eax
	movq	24(%r15), %r13
	leaq	-512(%rbp), %r12
	leal	(%rax,%rbx), %r14d
	testl	%eax, %eax
	jle	.L4549
	.p2align 4,,10
	.p2align 3
.L4550:
	movq	%r12, %rdi
	movl	%ebx, -512(%rbp)
	call	_ZNK2v88internal11interpreter8Register19is_function_closureEv@PLT
	leaq	256(%r13), %rsi
	testb	%al, %al
	jne	.L4542
	movq	%r12, %rdi
	call	_ZNK2v88internal11interpreter8Register18is_current_contextEv@PLT
	leaq	480(%r13), %rsi
	testb	%al, %al
	jne	.L4542
	movl	-512(%rbp), %edx
	movl	%edx, -288(%rbp)
	movl	248(%r13), %esi
	leal	(%rdx,%rsi), %eax
	testl	%edx, %edx
	js	.L4564
.L4545:
	movq	936(%r13), %rdx
	movslq	%eax, %rcx
	testl	%eax, %eax
	js	.L4546
	movq	944(%r13), %rax
	movabsq	$7905747460161236407, %rsi
	subq	%rdx, %rax
	sarq	$5, %rax
	imulq	%rsi, %rax
	cmpq	%rax, %rcx
	jb	.L4546
	leaq	.LC24(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4546:
	leaq	0(,%rcx,8), %rsi
	subq	%rcx, %rsi
	salq	$5, %rsi
	addq	%rdx, %rsi
.L4542:
	movq	-528(%rbp), %rdi
	cmpq	-520(%rbp), %rdi
	je	.L4547
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	addq	$224, -528(%rbp)
.L4548:
	addl	$1, %ebx
	cmpl	%ebx, %r14d
	jne	.L4550
.L4549:
	movq	-560(%rbp), %rsi
	movq	%r12, %rdi
	movb	$0, -288(%rbp)
	movb	$0, -280(%rbp)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	xorl	%r9d, %r9d
	movq	%r12, %rsi
	movq	%r15, %rdi
	movl	-552(%rbp), %r8d
	leaq	-544(%rbp), %rcx
	leaq	-288(%rbp), %rdx
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation22ProcessCallOrConstructENS1_5HintsENS_4base8OptionalIS3_EERKNS0_10ZoneVectorIS3_EENS0_12FeedbackSlotEb
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpb	$0, -288(%rbp)
	jne	.L4565
.L4540:
	movq	-528(%rbp), %r12
	movq	-536(%rbp), %rbx
	cmpq	%rbx, %r12
	je	.L4525
	.p2align 4,,10
	.p2align 3
.L4552:
	movq	%rbx, %rdi
	addq	$224, %rbx
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	%rbx, %r12
	jne	.L4552
.L4525:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4566
	addq	$552, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4547:
	.cfi_restore_state
	leaq	-544(%rbp), %r8
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	movq	%r8, %rdi
	call	_ZNSt6vectorIN2v88internal8compiler5HintsENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	jmp	.L4548
	.p2align 4,,10
	.p2align 3
.L4564:
	leaq	-288(%rbp), %rdi
	call	_ZNK2v88internal11interpreter8Register16ToParameterIndexEi@PLT
	jmp	.L4545
	.p2align 4,,10
	.p2align 3
.L4563:
	movq	(%rdi), %rax
	leaq	-288(%rbp), %r13
	movq	%r13, %rdi
	movq	(%rax), %rsi
	addq	$88, %rsi
	call	_ZN2v88internal8compiler5Hints14SingleConstantENS0_6HandleINS0_6ObjectEEEPNS0_4ZoneE
	movq	-528(%rbp), %r12
	cmpq	-520(%rbp), %r12
	je	.L4527
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler5HintsC1EOS2_
	addq	$224, -528(%rbp)
.L4528:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	jmp	.L4526
	.p2align 4,,10
	.p2align 3
.L4565:
	leaq	-280(%rbp), %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	jmp	.L4540
	.p2align 4,,10
	.p2align 3
.L4527:
	movq	-536(%rbp), %r14
	movq	%r12, %rcx
	movabsq	$7905747460161236407, %rsi
	subq	%r14, %rcx
	movq	%rcx, %rax
	sarq	$5, %rax
	imulq	%rsi, %rax
	cmpq	$9586980, %rax
	je	.L4567
	testq	%rax, %rax
	je	.L4554
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L4568
	movl	$2147483520, %esi
	movl	$2147483520, %r8d
.L4530:
	movq	-544(%rbp), %rdi
	movq	16(%rdi), %rdx
	movq	24(%rdi), %rax
	subq	%rdx, %rax
	cmpq	%rsi, %rax
	jb	.L4569
	addq	%rdx, %rsi
	movq	%rsi, 16(%rdi)
.L4533:
	movq	%rdx, %rax
	addq	%rdx, %r8
	movq	%rdx, -584(%rbp)
	addq	$224, %rax
	movq	%r8, -592(%rbp)
	movq	%rax, -568(%rbp)
	jmp	.L4531
.L4568:
	testq	%rsi, %rsi
	jne	.L4570
	movq	$224, -568(%rbp)
	movq	$0, -592(%rbp)
	movq	$0, -584(%rbp)
.L4531:
	movq	-584(%rbp), %rax
	movq	%r13, %rsi
	leaq	(%rax,%rcx), %rdi
	call	_ZN2v88internal8compiler5HintsC1EOS2_
	cmpq	%r14, %r12
	je	.L4534
	movq	-584(%rbp), %rcx
	movq	%r14, %rax
	.p2align 4,,10
	.p2align 3
.L4535:
	movq	%rax, %rsi
	movq	%rcx, %rdi
	movq	%rax, -576(%rbp)
	movq	%rcx, -568(%rbp)
	call	_ZN2v88internal8compiler5HintsC1EOS2_
	movq	-576(%rbp), %rax
	movq	-568(%rbp), %rcx
	addq	$224, %rax
	addq	$224, %rcx
	cmpq	%rax, %r12
	jne	.L4535
	leaq	-224(%r12), %rax
	movabsq	$411757680216731063, %rcx
	subq	%r14, %rax
	shrq	$5, %rax
	imulq	%rcx, %rax
	movabsq	$576460752303423487, %rcx
	andq	%rcx, %rax
	leaq	0(,%rax,8), %rcx
	subq	%rax, %rcx
	movq	%rcx, %rax
	movq	-584(%rbp), %rcx
	salq	$5, %rax
	leaq	448(%rcx,%rax), %rax
	movq	%rax, -568(%rbp)
	.p2align 4,,10
	.p2align 3
.L4537:
	movq	%r14, %rdi
	addq	$224, %r14
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	%r14, %r12
	jne	.L4537
.L4534:
	movq	-584(%rbp), %xmm0
	movq	-592(%rbp), %rax
	movhps	-568(%rbp), %xmm0
	movq	%rax, -520(%rbp)
	movups	%xmm0, -536(%rbp)
	jmp	.L4528
.L4554:
	movl	$224, %esi
	movl	$224, %r8d
	jmp	.L4530
.L4569:
	movq	%rcx, -576(%rbp)
	movq	%r8, -568(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-568(%rbp), %r8
	movq	-576(%rbp), %rcx
	movq	%rax, %rdx
	jmp	.L4533
.L4567:
	leaq	.LC48(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L4566:
	call	__stack_chk_fail@PLT
.L4570:
	cmpq	$9586980, %rsi
	movl	$9586980, %r8d
	cmova	%r8, %rsi
	imulq	$224, %rsi, %r8
	movq	%r8, %rsi
	jmp	.L4530
	.cfi_endproc
.LFE18995:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation18ProcessCallVarArgsENS0_19ConvertReceiverModeERKNS1_5HintsENS0_11interpreter8RegisterEiNS0_12FeedbackSlotEb, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation18ProcessCallVarArgsENS0_19ConvertReceiverModeERKNS1_5HintsENS0_11interpreter8RegisterEiNS0_12FeedbackSlotEb
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation26VisitCallUndefinedReceiverEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation26VisitCallUndefinedReceiverEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation26VisitCallUndefinedReceiverEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation26VisitCallUndefinedReceiverEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB18968:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	24(%rdi), %r14
	movq	%r12, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movl	%eax, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment14register_hintsENS0_11interpreter8RegisterE
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r14
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	movl	%eax, %r15d
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor23GetRegisterCountOperandEi@PLT
	movq	%r12, %rdi
	movl	$3, %esi
	movl	%eax, %ebx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	subq	$8, %rsp
	movq	%r14, %rdx
	movl	%ebx, %r8d
	pushq	$0
	movl	%eax, %r9d
	movl	%r15d, %ecx
	movq	%r13, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation18ProcessCallVarArgsENS0_19ConvertReceiverModeERKNS1_5HintsENS0_11interpreter8RegisterEiNS0_12FeedbackSlotEb
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18968:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation26VisitCallUndefinedReceiverEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation26VisitCallUndefinedReceiverEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation20VisitCallAnyReceiverEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation20VisitCallAnyReceiverEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation20VisitCallAnyReceiverEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation20VisitCallAnyReceiverEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB18978:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	24(%rdi), %r14
	movq	%r12, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movl	%eax, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment14register_hintsENS0_11interpreter8RegisterE
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r14
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	movl	%eax, %r15d
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor23GetRegisterCountOperandEi@PLT
	movq	%r12, %rdi
	movl	$3, %esi
	movl	%eax, %ebx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	subq	$8, %rsp
	movq	%r14, %rdx
	movl	%ebx, %r8d
	pushq	$0
	movl	%eax, %r9d
	movl	%r15d, %ecx
	movq	%r13, %rdi
	movl	$2, %esi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation18ProcessCallVarArgsENS0_19ConvertReceiverModeERKNS1_5HintsENS0_11interpreter8RegisterEiNS0_12FeedbackSlotEb
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18978:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation20VisitCallAnyReceiverEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation20VisitCallAnyReceiverEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation19VisitCallNoFeedbackEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation19VisitCallNoFeedbackEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation19VisitCallNoFeedbackEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation19VisitCallNoFeedbackEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB18979:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movq	24(%rdi), %r14
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment14register_hintsENS0_11interpreter8RegisterE
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r14
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	movl	%eax, %r15d
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor23GetRegisterCountOperandEi@PLT
	subq	$8, %rsp
	movq	%r14, %rdx
	movl	%r15d, %ecx
	pushq	$0
	movl	%eax, %r8d
	movq	%r13, %rdi
	movl	$-1, %r9d
	movl	$2, %esi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation18ProcessCallVarArgsENS0_19ConvertReceiverModeERKNS1_5HintsENS0_11interpreter8RegisterEiNS0_12FeedbackSlotEb
	popq	%rax
	popq	%rdx
	leaq	-32(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18979:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation19VisitCallNoFeedbackEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation19VisitCallNoFeedbackEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation17VisitCallPropertyEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation17VisitCallPropertyEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation17VisitCallPropertyEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation17VisitCallPropertyEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB18980:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	24(%rdi), %r14
	movq	%r12, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movl	%eax, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment14register_hintsENS0_11interpreter8RegisterE
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r14
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	movl	%eax, %r15d
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor23GetRegisterCountOperandEi@PLT
	movq	%r12, %rdi
	movl	$3, %esi
	movl	%eax, %ebx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	subq	$8, %rsp
	movq	%r14, %rdx
	movl	%ebx, %r8d
	pushq	$0
	movl	%eax, %r9d
	movl	%r15d, %ecx
	movq	%r13, %rdi
	movl	$1, %esi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation18ProcessCallVarArgsENS0_19ConvertReceiverModeERKNS1_5HintsENS0_11interpreter8RegisterEiNS0_12FeedbackSlotEb
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18980:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation17VisitCallPropertyEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation17VisitCallPropertyEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation19VisitCallWithSpreadEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation19VisitCallWithSpreadEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation19VisitCallWithSpreadEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation19VisitCallWithSpreadEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB18984:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	24(%rdi), %r14
	movq	%r12, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movl	%eax, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment14register_hintsENS0_11interpreter8RegisterE
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r14
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	movl	%eax, %r15d
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor23GetRegisterCountOperandEi@PLT
	movq	%r12, %rdi
	movl	$3, %esi
	movl	%eax, %ebx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	subq	$8, %rsp
	movq	%r14, %rdx
	movl	%ebx, %r8d
	pushq	$1
	movl	%eax, %r9d
	movl	%r15d, %ecx
	movq	%r13, %rdi
	movl	$2, %esi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation18ProcessCallVarArgsENS0_19ConvertReceiverModeERKNS1_5HintsENS0_11interpreter8RegisterEiNS0_12FeedbackSlotEb
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18984:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation19VisitCallWithSpreadEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation19VisitCallWithSpreadEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitCallJSRuntimeEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitCallJSRuntimeEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitCallJSRuntimeEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitCallJSRuntimeEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB18985:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	xorl	%esi, %esi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	subq	$280, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor28GetNativeContextIndexOperandEi@PLT
	movl	%eax, %edx
	movq	(%r12), %rax
	cmpb	$0, 24(%rax)
	je	.L4583
	movdqu	32(%rax), %xmm0
	leaq	-288(%rbp), %r14
	movl	$1, %ecx
	leaq	-304(%rbp), %r15
	movq	%r15, %rsi
	movq	%r14, %rdi
	movaps	%xmm0, -304(%rbp)
	call	_ZNK2v88internal8compiler10ContextRef3getEiNS1_19SerializationPolicyE@PLT
	cmpb	$0, -288(%rbp)
	je	.L4583
	movdqu	-280(%rbp), %xmm1
	leaq	-320(%rbp), %rdi
	movq	16(%r12), %rbx
	movaps	%xmm1, -320(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef6objectEv@PLT
	movq	%r15, %rsi
	movq	%rbx, -120(%rbp)
	leaq	-232(%rbp), %rdi
	movq	%rax, %r8
	leaq	-272(%rbp), %rax
	movq	%rbx, -288(%rbp)
	movq	%rax, -256(%rbp)
	movq	%rax, -248(%rbp)
	leaq	-216(%rbp), %rax
	movq	%rax, -200(%rbp)
	movq	%rax, -192(%rbp)
	leaq	-160(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	%rax, -136(%rbp)
	leaq	-104(%rbp), %rax
	movq	%r8, -304(%rbp)
	movl	$0, -272(%rbp)
	movq	$0, -264(%rbp)
	movq	$0, -240(%rbp)
	movq	%rbx, -232(%rbp)
	movl	$0, -216(%rbp)
	movq	$0, -208(%rbp)
	movq	$0, -184(%rbp)
	movq	%rbx, -176(%rbp)
	movl	$0, -160(%rbp)
	movq	$0, -152(%rbp)
	movq	$0, -128(%rbp)
	movl	$0, -104(%rbp)
	movq	$0, -96(%rbp)
	movq	%rax, -88(%rbp)
	movq	%rax, -80(%rbp)
	movq	$0, -72(%rbp)
	call	_ZNSt8_Rb_treeIN2v88internal6HandleINS1_6ObjectEEES4_St9_IdentityIS4_ENS1_8compiler16HandleComparatorIS3_EENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movl	$2, %esi
	movq	%r13, %rdi
	movl	%eax, %r15d
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor23GetRegisterCountOperandEi@PLT
	subq	$8, %rsp
	movq	%r14, %rdx
	xorl	%esi, %esi
	pushq	$0
	movl	%eax, %r8d
	movl	$-1, %r9d
	movl	%r15d, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation18ProcessCallVarArgsENS0_19ConvertReceiverModeERKNS1_5HintsENS0_11interpreter8RegisterEiNS0_12FeedbackSlotEb
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4586
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4583:
	.cfi_restore_state
	leaq	.LC41(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L4586:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18985:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitCallJSRuntimeEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitCallJSRuntimeEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation14VisitConstructEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation14VisitConstructEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation14VisitConstructEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation14VisitConstructEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB19052:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$536, %rsp
	.cfi_offset 3, -56
	movq	24(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment17accumulator_hintsEv
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, -560(%rbp)
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r13, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment14register_hintsENS0_11interpreter8RegisterE
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, -568(%rbp)
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	movl	%eax, %ebx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor23GetRegisterCountOperandEi@PLT
	movq	%r12, %rdi
	movl	$3, %esi
	leaq	-512(%rbp), %r12
	movl	%eax, -548(%rbp)
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	movl	-548(%rbp), %edx
	movq	24(%r15), %r13
	movq	$0, -536(%rbp)
	movl	%eax, -552(%rbp)
	movq	16(%r15), %rax
	movq	$0, -528(%rbp)
	leal	(%rdx,%rbx), %r14d
	movq	%rax, -544(%rbp)
	movq	$0, -520(%rbp)
	testl	%edx, %edx
	jle	.L4599
	.p2align 4,,10
	.p2align 3
.L4600:
	movq	%r12, %rdi
	movl	%ebx, -512(%rbp)
	call	_ZNK2v88internal11interpreter8Register19is_function_closureEv@PLT
	leaq	256(%r13), %rsi
	testb	%al, %al
	jne	.L4592
	movq	%r12, %rdi
	call	_ZNK2v88internal11interpreter8Register18is_current_contextEv@PLT
	leaq	480(%r13), %rsi
	testb	%al, %al
	jne	.L4592
	movl	-512(%rbp), %edx
	movl	%edx, -288(%rbp)
	movl	248(%r13), %esi
	leal	(%rdx,%rsi), %eax
	testl	%edx, %edx
	js	.L4607
.L4595:
	movq	936(%r13), %rdx
	movslq	%eax, %rcx
	testl	%eax, %eax
	js	.L4596
	movq	944(%r13), %rax
	movabsq	$7905747460161236407, %rsi
	subq	%rdx, %rax
	sarq	$5, %rax
	imulq	%rsi, %rax
	cmpq	%rcx, %rax
	ja	.L4596
	leaq	.LC24(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4596:
	leaq	0(,%rcx,8), %rsi
	subq	%rcx, %rsi
	salq	$5, %rsi
	addq	%rdx, %rsi
.L4592:
	movq	-528(%rbp), %rdi
	cmpq	-520(%rbp), %rdi
	je	.L4597
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	addq	$224, -528(%rbp)
.L4598:
	addl	$1, %ebx
	cmpl	%ebx, %r14d
	jne	.L4600
.L4599:
	movq	-560(%rbp), %rsi
	leaq	-280(%rbp), %r13
	movb	$1, -288(%rbp)
	leaq	-288(%rbp), %r14
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movq	-568(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	xorl	%r9d, %r9d
	movq	%r14, %rdx
	movq	%r12, %rsi
	movl	-552(%rbp), %r8d
	leaq	-544(%rbp), %rcx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation22ProcessCallOrConstructENS1_5HintsENS_4base8OptionalIS3_EERKNS0_10ZoneVectorIS3_EENS0_12FeedbackSlotEb
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpb	$0, -288(%rbp)
	jne	.L4608
.L4590:
	movq	-528(%rbp), %r12
	movq	-536(%rbp), %rbx
	cmpq	%r12, %rbx
	je	.L4587
	.p2align 4,,10
	.p2align 3
.L4602:
	movq	%rbx, %rdi
	addq	$224, %rbx
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	%r12, %rbx
	jne	.L4602
.L4587:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4609
	addq	$536, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4597:
	.cfi_restore_state
	leaq	-544(%rbp), %r8
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	movq	%r8, %rdi
	call	_ZNSt6vectorIN2v88internal8compiler5HintsENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	jmp	.L4598
	.p2align 4,,10
	.p2align 3
.L4607:
	leaq	-288(%rbp), %rdi
	call	_ZNK2v88internal11interpreter8Register16ToParameterIndexEi@PLT
	jmp	.L4595
	.p2align 4,,10
	.p2align 3
.L4608:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	jmp	.L4590
.L4609:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19052:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation14VisitConstructEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation14VisitConstructEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation24VisitConstructWithSpreadEPNS0_11interpreter21BytecodeArrayIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation24VisitConstructWithSpreadEPNS0_11interpreter21BytecodeArrayIteratorE
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation24VisitConstructWithSpreadEPNS0_11interpreter21BytecodeArrayIteratorE, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation24VisitConstructWithSpreadEPNS0_11interpreter21BytecodeArrayIteratorE:
.LFB19053:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$536, %rsp
	.cfi_offset 3, -56
	movq	24(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment17accumulator_hintsEv
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, -560(%rbp)
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r13, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment14register_hintsENS0_11interpreter8RegisterE
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, -568(%rbp)
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	movl	%eax, %ebx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor23GetRegisterCountOperandEi@PLT
	movq	%r12, %rdi
	movl	$3, %esi
	leaq	-512(%rbp), %r12
	movl	%eax, -548(%rbp)
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	movl	-548(%rbp), %edx
	movq	24(%r15), %r13
	movq	$0, -536(%rbp)
	movl	%eax, -552(%rbp)
	movq	16(%r15), %rax
	movq	$0, -528(%rbp)
	leal	(%rdx,%rbx), %r14d
	movq	%rax, -544(%rbp)
	movq	$0, -520(%rbp)
	testl	%edx, %edx
	jle	.L4622
	.p2align 4,,10
	.p2align 3
.L4623:
	movq	%r12, %rdi
	movl	%ebx, -512(%rbp)
	call	_ZNK2v88internal11interpreter8Register19is_function_closureEv@PLT
	leaq	256(%r13), %rsi
	testb	%al, %al
	jne	.L4615
	movq	%r12, %rdi
	call	_ZNK2v88internal11interpreter8Register18is_current_contextEv@PLT
	leaq	480(%r13), %rsi
	testb	%al, %al
	jne	.L4615
	movl	-512(%rbp), %edx
	movl	%edx, -288(%rbp)
	movl	248(%r13), %esi
	leal	(%rdx,%rsi), %eax
	testl	%edx, %edx
	js	.L4630
.L4618:
	movq	936(%r13), %rdx
	movslq	%eax, %rcx
	testl	%eax, %eax
	js	.L4619
	movq	944(%r13), %rax
	movabsq	$7905747460161236407, %rsi
	subq	%rdx, %rax
	sarq	$5, %rax
	imulq	%rsi, %rax
	cmpq	%rcx, %rax
	ja	.L4619
	leaq	.LC24(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4619:
	leaq	0(,%rcx,8), %rsi
	subq	%rcx, %rsi
	salq	$5, %rsi
	addq	%rdx, %rsi
.L4615:
	movq	-528(%rbp), %rdi
	cmpq	-520(%rbp), %rdi
	je	.L4620
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	addq	$224, -528(%rbp)
.L4621:
	addl	$1, %ebx
	cmpl	%ebx, %r14d
	jne	.L4623
.L4622:
	movq	-560(%rbp), %rsi
	leaq	-280(%rbp), %r13
	movb	$1, -288(%rbp)
	leaq	-288(%rbp), %r14
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movq	-568(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movl	-552(%rbp), %r8d
	leaq	-544(%rbp), %rcx
	movl	$1, %r9d
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation22ProcessCallOrConstructENS1_5HintsENS_4base8OptionalIS3_EERKNS0_10ZoneVectorIS3_EENS0_12FeedbackSlotEb
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpb	$0, -288(%rbp)
	jne	.L4631
.L4613:
	movq	-528(%rbp), %r12
	movq	-536(%rbp), %rbx
	cmpq	%r12, %rbx
	je	.L4610
	.p2align 4,,10
	.p2align 3
.L4625:
	movq	%rbx, %rdi
	addq	$224, %rbx
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	%r12, %rbx
	jne	.L4625
.L4610:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4632
	addq	$536, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4620:
	.cfi_restore_state
	leaq	-544(%rbp), %r8
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	movq	%r8, %rdi
	call	_ZNSt6vectorIN2v88internal8compiler5HintsENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	jmp	.L4621
	.p2align 4,,10
	.p2align 3
.L4630:
	leaq	-288(%rbp), %rdi
	call	_ZNK2v88internal11interpreter8Register16ToParameterIndexEi@PLT
	jmp	.L4618
	.p2align 4,,10
	.p2align 3
.L4631:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	jmp	.L4613
.L4632:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19053:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation24VisitConstructWithSpreadEPNS0_11interpreter21BytecodeArrayIteratorE, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation24VisitConstructWithSpreadEPNS0_11interpreter21BytecodeArrayIteratorE
	.section	.rodata._ZN2v88internal8compiler34SerializerForBackgroundCompilation16TraverseBytecodeEv.str1.1,"aMS",@progbits,1
.LC62:
	.string	"Handling bytecode: "
.LC63:
	.string	"  "
.LC64:
	.string	"Current environment: "
.LC65:
	.string	"!bytecode_iterator_.done()"
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation16TraverseBytecodeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation16TraverseBytecodeEv
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation16TraverseBytecodeEv, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation16TraverseBytecodeEv:
.LFB18910:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$424, %rsp
	movl	96(%rdi), %r12d
	movl	100(%rdi), %r14d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %r13
	call	_ZNK2v88internal8compiler34SerializerForBackgroundCompilation14bytecode_arrayEv
	shrl	$2, %r12d
	movl	$1, %r8d
	movl	%r14d, %edx
	andl	$1, %r12d
	movq	%rax, %rsi
	movq	%r13, %rdi
	movl	%r12d, %ecx
	leaq	-288(%rbp), %r14
	leaq	-384(%rbp), %r12
	call	_ZN2v88internal8compiler12JSHeapBroker19GetBytecodeAnalysisENS0_6HandleINS0_13BytecodeArrayEEENS0_9BailoutIdEbNS1_19SerializationPolicyE@PLT
	movq	%rbx, %rdi
	leaq	-336(%rbp), %r13
	movq	%rax, -416(%rbp)
	call	_ZNK2v88internal8compiler34SerializerForBackgroundCompilation14bytecode_arrayEv
	movq	(%rbx), %rsi
	movl	$1, %ecx
	movq	%r14, %rdi
	movq	%rax, %rdx
	movq	%r14, -424(%rbp)
	call	_ZN2v88internal8compiler16BytecodeArrayRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler16BytecodeArrayRef23SerializeForCompilationEv@PLT
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler34SerializerForBackgroundCompilation14bytecode_arrayEv
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter21BytecodeArrayIteratorC1ENS0_6HandleINS0_13BytecodeArrayEEE@PLT
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler34SerializerForBackgroundCompilation14bytecode_arrayEv
	movq	%r12, -352(%rbp)
	movq	%r14, %rdi
	movl	$0, -336(%rbp)
	movq	$0, -328(%rbp)
	movq	%r13, -320(%rbp)
	movq	%r13, -312(%rbp)
	movq	$0, -304(%rbp)
	movq	$0, -296(%rbp)
	movq	(%rax), %rsi
	call	_ZN2v88internal12HandlerTableC1ENS0_13BytecodeArrayE@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal12HandlerTable20NumberOfRangeEntriesEv@PLT
	movl	%eax, -404(%rbp)
	testl	%eax, %eax
	jle	.L4645
	xorl	%r14d, %r14d
	movq	%r12, -448(%rbp)
	movq	%rbx, -440(%rbp)
	movl	%r14d, %ebx
	movq	-424(%rbp), %r14
	.p2align 4,,10
	.p2align 3
.L4634:
	movl	%ebx, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal12HandlerTable15GetRangeHandlerEi@PLT
	movq	-328(%rbp), %r12
	movl	%eax, %r15d
	testq	%r12, %r12
	jne	.L4637
	jmp	.L4793
	.p2align 4,,10
	.p2align 3
.L4794:
	movq	16(%r12), %rax
	movl	$1, %esi
	testq	%rax, %rax
	je	.L4638
.L4795:
	movq	%rax, %r12
.L4637:
	movl	32(%r12), %ecx
	cmpl	%ecx, %r15d
	jl	.L4794
	movq	24(%r12), %rax
	xorl	%esi, %esi
	testq	%rax, %rax
	jne	.L4795
.L4638:
	testb	%sil, %sil
	jne	.L4796
	cmpl	%ecx, %r15d
	jle	.L4643
.L4761:
	movl	$1, %r10d
	cmpq	%r13, %r12
	jne	.L4797
.L4644:
	movl	$40, %edi
	movl	%r10d, -432(%rbp)
	call	_Znwm@PLT
	movl	-432(%rbp), %r10d
	movq	%r13, %rcx
	movq	%r12, %rdx
	movl	%r15d, 32(%rax)
	movq	%rax, %rsi
	movl	%r10d, %edi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, -304(%rbp)
.L4643:
	addl	$1, %ebx
	cmpl	%ebx, -404(%rbp)
	jne	.L4634
	movq	-440(%rbp), %rbx
	movq	-448(%rbp), %r12
.L4645:
	movq	-320(%rbp), %rax
	movq	%r12, %rdi
	movb	$0, -404(%rbp)
	movq	%rax, -296(%rbp)
	leaq	-385(%rbp), %rax
	movq	%rax, -432(%rbp)
	call	_ZNK2v88internal11interpreter21BytecodeArrayIterator4doneEv@PLT
	testb	%al, %al
	jne	.L4646
	.p2align 4,,10
	.p2align 3
.L4801:
	cmpb	$0, -404(%rbp)
	je	.L4798
.L4647:
	movl	-376(%rbp), %r14d
	movq	%rbx, %rdi
	movl	%r14d, %esi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation32IncorporateJumpTargetEnvironmentEi
	movq	(%rbx), %rdi
	cmpb	$0, 124(%rdi)
	je	.L4650
	cmpb	$0, _ZN2v88internal30FLAG_trace_heap_broker_verboseE(%rip)
	jne	.L4799
.L4650:
	movq	24(%rbx), %rax
	movq	944(%rax), %rcx
	cmpq	%rcx, 936(%rax)
	je	.L4800
.L4652:
	movq	-416(%rbp), %rdi
	movl	%r14d, %esi
	call	_ZNK2v88internal8compiler16BytecodeAnalysis12IsLoopHeaderEi@PLT
	testb	%al, %al
	jne	.L4662
.L4748:
	movq	%r12, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor16current_bytecodeEv@PLT
	cmpb	$-74, %al
	ja	.L4663
.L4807:
	leaq	.L4665(%rip), %rdx
	movzbl	%al, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler34SerializerForBackgroundCompilation16TraverseBytecodeEv,"a",@progbits
	.align 4
	.align 4
.L4665:
	.long	.L4664-.L4665
	.long	.L4664-.L4665
	.long	.L4663-.L4665
	.long	.L4663-.L4665
	.long	.L4663-.L4665
	.long	.L4663-.L4665
	.long	.L4663-.L4665
	.long	.L4663-.L4665
	.long	.L4663-.L4665
	.long	.L4663-.L4665
	.long	.L4663-.L4665
	.long	.L4747-.L4665
	.long	.L4746-.L4665
	.long	.L4745-.L4665
	.long	.L4744-.L4665
	.long	.L4743-.L4665
	.long	.L4742-.L4665
	.long	.L4741-.L4665
	.long	.L4740-.L4665
	.long	.L4739-.L4665
	.long	.L4739-.L4665
	.long	.L4738-.L4665
	.long	.L4737-.L4665
	.long	.L4736-.L4665
	.long	.L4735-.L4665
	.long	.L4734-.L4665
	.long	.L4733-.L4665
	.long	.L4732-.L4665
	.long	.L4731-.L4665
	.long	.L4730-.L4665
	.long	.L4727-.L4665
	.long	.L4729-.L4665
	.long	.L4728-.L4665
	.long	.L4727-.L4665
	.long	.L4729-.L4665
	.long	.L4728-.L4665
	.long	.L4727-.L4665
	.long	.L4726-.L4665
	.long	.L4725-.L4665
	.long	.L4724-.L4665
	.long	.L4723-.L4665
	.long	.L4719-.L4665
	.long	.L4722-.L4665
	.long	.L4721-.L4665
	.long	.L4721-.L4665
	.long	.L4720-.L4665
	.long	.L4719-.L4665
	.long	.L4718-.L4665
	.long	.L4717-.L4665
	.long	.L4716-.L4665
	.long	.L4715-.L4665
	.long	.L4663-.L4665
	.long	.L4714-.L4665
	.long	.L4714-.L4665
	.long	.L4714-.L4665
	.long	.L4714-.L4665
	.long	.L4714-.L4665
	.long	.L4714-.L4665
	.long	.L4714-.L4665
	.long	.L4714-.L4665
	.long	.L4714-.L4665
	.long	.L4714-.L4665
	.long	.L4714-.L4665
	.long	.L4714-.L4665
	.long	.L4714-.L4665
	.long	.L4714-.L4665
	.long	.L4714-.L4665
	.long	.L4714-.L4665
	.long	.L4714-.L4665
	.long	.L4714-.L4665
	.long	.L4714-.L4665
	.long	.L4714-.L4665
	.long	.L4714-.L4665
	.long	.L4714-.L4665
	.long	.L4714-.L4665
	.long	.L4714-.L4665
	.long	.L4713-.L4665
	.long	.L4713-.L4665
	.long	.L4713-.L4665
	.long	.L4713-.L4665
	.long	.L4674-.L4665
	.long	.L4674-.L4665
	.long	.L4674-.L4665
	.long	.L4674-.L4665
	.long	.L4674-.L4665
	.long	.L4712-.L4665
	.long	.L4711-.L4665
	.long	.L4710-.L4665
	.long	.L4709-.L4665
	.long	.L4708-.L4665
	.long	.L4707-.L4665
	.long	.L4706-.L4665
	.long	.L4705-.L4665
	.long	.L4704-.L4665
	.long	.L4703-.L4665
	.long	.L4702-.L4665
	.long	.L4701-.L4665
	.long	.L4674-.L4665
	.long	.L4700-.L4665
	.long	.L4699-.L4665
	.long	.L4698-.L4665
	.long	.L4697-.L4665
	.long	.L4696-.L4665
	.long	.L4691-.L4665
	.long	.L4691-.L4665
	.long	.L4691-.L4665
	.long	.L4691-.L4665
	.long	.L4691-.L4665
	.long	.L4691-.L4665
	.long	.L4674-.L4665
	.long	.L4689-.L4665
	.long	.L4688-.L4665
	.long	.L4674-.L4665
	.long	.L4674-.L4665
	.long	.L4674-.L4665
	.long	.L4674-.L4665
	.long	.L4674-.L4665
	.long	.L4687-.L4665
	.long	.L4687-.L4665
	.long	.L4663-.L4665
	.long	.L4674-.L4665
	.long	.L4686-.L4665
	.long	.L4685-.L4665
	.long	.L4674-.L4665
	.long	.L4674-.L4665
	.long	.L4684-.L4665
	.long	.L4674-.L4665
	.long	.L4674-.L4665
	.long	.L4683-.L4665
	.long	.L4682-.L4665
	.long	.L4681-.L4665
	.long	.L4680-.L4665
	.long	.L4681-.L4665
	.long	.L4681-.L4665
	.long	.L4680-.L4665
	.long	.L4674-.L4665
	.long	.L4674-.L4665
	.long	.L4674-.L4665
	.long	.L4679-.L4665
	.long	.L4679-.L4665
	.long	.L4679-.L4665
	.long	.L4678-.L4665
	.long	.L4678-.L4665
	.long	.L4678-.L4665
	.long	.L4678-.L4665
	.long	.L4678-.L4665
	.long	.L4678-.L4665
	.long	.L4678-.L4665
	.long	.L4678-.L4665
	.long	.L4678-.L4665
	.long	.L4678-.L4665
	.long	.L4678-.L4665
	.long	.L4678-.L4665
	.long	.L4678-.L4665
	.long	.L4678-.L4665
	.long	.L4678-.L4665
	.long	.L4678-.L4665
	.long	.L4678-.L4665
	.long	.L4678-.L4665
	.long	.L4678-.L4665
	.long	.L4678-.L4665
	.long	.L4677-.L4665
	.long	.L4674-.L4665
	.long	.L4676-.L4665
	.long	.L4674-.L4665
	.long	.L4675-.L4665
	.long	.L4674-.L4665
	.long	.L4655-.L4665
	.long	.L4674-.L4665
	.long	.L4666-.L4665
	.long	.L4666-.L4665
	.long	.L4673-.L4665
	.long	.L4672-.L4665
	.long	.L4655-.L4665
	.long	.L4655-.L4665
	.long	.L4671-.L4665
	.long	.L4670-.L4665
	.long	.L4669-.L4665
	.long	.L4668-.L4665
	.long	.L4667-.L4665
	.long	.L4655-.L4665
	.long	.L4666-.L4665
	.long	.L4664-.L4665
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation16TraverseBytecodeEv
	.p2align 4,,10
	.p2align 3
.L4674:
	movq	24(%rbx), %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11Environment17accumulator_hintsEv
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler5Hints5ClearEv
	.p2align 4,,10
	.p2align 3
.L4655:
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter21BytecodeArrayIterator7AdvanceEv@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayIterator4doneEv@PLT
	testb	%al, %al
	je	.L4801
.L4646:
	cmpb	$0, -404(%rbp)
	jne	.L4802
.L4755:
	movq	-328(%rbp), %rbx
	leaq	-344(%rbp), %r12
	testq	%rbx, %rbx
	je	.L4759
.L4756:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIiiSt9_IdentityIiESt4lessIiESaIiEE8_M_eraseEPSt13_Rb_tree_nodeIiE
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L4756
.L4759:
	movq	-384(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4633
	movq	(%rdi), %rax
	call	*72(%rax)
.L4633:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4803
	addq	$424, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4714:
	.cfi_restore_state
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation8VisitAddEPNS0_11interpreter21BytecodeArrayIteratorE
	jmp	.L4655
	.p2align 4,,10
	.p2align 3
.L4678:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11ProcessJumpEPNS0_11interpreter21BytecodeArrayIteratorE
	jmp	.L4655
	.p2align 4,,10
	.p2align 3
.L4691:
	movq	%r12, %rdi
	movl	$1, %esi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	movq	%rbx, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation23ProcessCompareOperationENS0_12FeedbackSlotE
	jmp	.L4655
	.p2align 4,,10
	.p2align 3
.L4798:
	movq	%r12, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor16current_bytecodeEv@PLT
	movzbl	-404(%rbp), %edx
	leal	-41(%rax), %ecx
	movabsq	$18014398509482017, %rax
	shrq	%cl, %rax
	andl	$1, %eax
	cmpb	$55, %cl
	cmovb	%eax, %edx
	movb	%dl, -404(%rbp)
	jmp	.L4647
	.p2align 4,,10
	.p2align 3
.L4800:
	movq	-352(%rbp), %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayIterator4doneEv@PLT
	testb	%al, %al
	jne	.L4804
	movq	-296(%rbp), %rdi
	movq	-352(%rbp), %r15
	cmpq	%r13, %rdi
	jne	.L4657
	jmp	.L4655
	.p2align 4,,10
	.p2align 3
.L4805:
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, -296(%rbp)
	movq	%rax, %rdi
	cmpq	%r13, %rax
	je	.L4655
.L4657:
	movl	8(%r15), %eax
	cmpl	%eax, 32(%rdi)
	jl	.L4805
	jne	.L4655
	movq	24(%rbx), %r15
	leaq	-272(%rbp), %rdx
	movabsq	$7905747460161236407, %rcx
	movq	(%r15), %rax
	movq	%rdx, -256(%rbp)
	movq	%rdx, -248(%rbp)
	leaq	-216(%rbp), %rdx
	movq	%rax, -288(%rbp)
	movq	%rax, -232(%rbp)
	movq	%rdx, -200(%rbp)
	movq	%rdx, -192(%rbp)
	leaq	-160(%rbp), %rdx
	movq	%rax, -176(%rbp)
	movq	%rax, -120(%rbp)
	leaq	-104(%rbp), %rax
	movq	%rax, -88(%rbp)
	movq	%rax, -80(%rbp)
	movl	$0, -272(%rbp)
	movq	$0, -264(%rbp)
	movq	$0, -240(%rbp)
	movl	$0, -216(%rbp)
	movq	$0, -208(%rbp)
	movq	$0, -184(%rbp)
	movl	$0, -160(%rbp)
	movq	$0, -152(%rbp)
	movq	%rdx, -144(%rbp)
	movq	%rdx, -136(%rbp)
	movq	$0, -128(%rbp)
	movl	$0, -104(%rbp)
	movq	$0, -96(%rbp)
	movq	$0, -72(%rbp)
	movq	944(%r15), %rsi
	movq	936(%r15), %rdi
	movl	252(%r15), %edx
	movq	%rsi, %rax
	addl	248(%r15), %edx
	subq	%rdi, %rax
	addl	$1, %edx
	sarq	$5, %rax
	movslq	%edx, %rdx
	imulq	%rcx, %rax
	cmpq	%rax, %rdx
	ja	.L4806
	jnb	.L4660
	leaq	0(,%rdx,8), %rax
	subq	%rdx, %rax
	salq	$5, %rax
	addq	%rdi, %rax
	movq	%rax, -440(%rbp)
	cmpq	%rax, %rsi
	je	.L4660
	movl	%r14d, -448(%rbp)
	movq	%rsi, %r14
	movq	%rbx, -456(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L4661:
	movq	%rbx, %rdi
	addq	$224, %rbx
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	%rbx, %r14
	jne	.L4661
	movq	-440(%rbp), %rax
	movl	-448(%rbp), %r14d
	movq	-456(%rbp), %rbx
	movq	%rax, 944(%r15)
.L4660:
	movq	-424(%rbp), %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	jmp	.L4652
	.p2align 4,,10
	.p2align 3
.L4796:
	cmpq	%r12, -320(%rbp)
	je	.L4761
.L4762:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	cmpl	%r15d, 32(%rax)
	jge	.L4643
	testq	%r12, %r12
	je	.L4643
	movl	$1, %r10d
	cmpq	%r13, %r12
	je	.L4644
.L4797:
	xorl	%r10d, %r10d
	cmpl	32(%r12), %r15d
	setl	%r10b
	jmp	.L4644
	.p2align 4,,10
	.p2align 3
.L4799:
	call	_ZN2v88internal8compiler12JSHeapBroker5TraceEv@PLT
	movl	$19, %edx
	leaq	.LC62(%rip), %rsi
	movq	%rax, %r15
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r15, %rdi
	movl	%r14d, %esi
	call	_ZNSolsEi@PLT
	movl	$2, %edx
	leaq	.LC63(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor16current_bytecodeEv@PLT
	movq	-432(%rbp), %rsi
	movq	%r15, %rdi
	movb	%al, -385(%rbp)
	call	_ZN2v88internal11interpreterlsERSoRKNS1_8BytecodeE@PLT
	movq	-424(%rbp), %rsi
	movl	$1, %edx
	movb	$10, -288(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rdi
	cmpb	$0, 124(%rdi)
	je	.L4650
	cmpb	$0, _ZN2v88internal30FLAG_trace_heap_broker_verboseE(%rip)
	je	.L4650
	call	_ZN2v88internal8compiler12JSHeapBroker5TraceEv@PLT
	movl	$21, %edx
	leaq	.LC64(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	24(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_34SerializerForBackgroundCompilation11EnvironmentE
	movq	-424(%rbp), %rsi
	movl	$1, %edx
	movb	$10, -288(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L4650
	.p2align 4,,10
	.p2align 3
.L4662:
	movq	-416(%rbp), %rdi
	movl	%r14d, %esi
	call	_ZNK2v88internal8compiler16BytecodeAnalysis14GetLoopInfoForEi@PLT
	movq	32(%rax), %r15
	movq	40(%rax), %r14
	cmpq	%r14, %r15
	je	.L4748
	.p2align 4,,10
	.p2align 3
.L4749:
	movl	4(%r15), %esi
	movq	%rbx, %rdi
	addq	$12, %r15
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation33ContributeToJumpTargetEnvironmentEi
	cmpq	%r15, %r14
	jne	.L4749
	movq	%r12, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor16current_bytecodeEv@PLT
	cmpb	$-74, %al
	jbe	.L4807
	.p2align 4,,10
	.p2align 3
.L4663:
	movq	24(%rbx), %rax
	movq	936(%rax), %r15
	movq	944(%rax), %r14
	cmpq	%r14, %r15
	je	.L4655
	.p2align 4,,10
	.p2align 3
.L4754:
	movq	%r15, %rdi
	addq	$224, %r15
	call	_ZN2v88internal8compiler5Hints5ClearEv
	cmpq	%r15, %r14
	jne	.L4754
	jmp	.L4655
	.p2align 4,,10
	.p2align 3
.L4793:
	movq	%r13, %r12
	cmpq	%r13, -320(%rbp)
	jne	.L4762
	movl	$1, %r10d
	jmp	.L4644
	.p2align 4,,10
	.p2align 3
.L4806:
	movq	-424(%rbp), %rcx
	subq	%rax, %rdx
	leaq	928(%r15), %rdi
	call	_ZNSt6vectorIN2v88internal8compiler5HintsENS1_13ZoneAllocatorIS3_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS3_S6_EEmRKS3_
	jmp	.L4660
	.p2align 4,,10
	.p2align 3
.L4804:
	leaq	.LC65(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L4713:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation15VisitBitwiseNotEPNS0_11interpreter21BytecodeArrayIteratorE
	jmp	.L4655
.L4666:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation10VisitAbortEPNS0_11interpreter21BytecodeArrayIteratorE
	jmp	.L4655
.L4679:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation9VisitJumpEPNS0_11interpreter21BytecodeArrayIteratorE
	jmp	.L4655
.L4681:
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation20ProcessCreateContextEPNS0_11interpreter21BytecodeArrayIteratorEi
	jmp	.L4655
.L4727:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitLdaLookupSlotEPNS0_11interpreter21BytecodeArrayIteratorE
	jmp	.L4655
.L4680:
	movl	$1, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation20ProcessCreateContextEPNS0_11interpreter21BytecodeArrayIteratorEi
	jmp	.L4655
.L4728:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation26ProcessLdaLookupGlobalSlotEPNS0_11interpreter21BytecodeArrayIteratorE
	jmp	.L4655
.L4739:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation14VisitLdaGlobalEPNS0_11interpreter21BytecodeArrayIteratorE
	jmp	.L4655
.L4687:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation13VisitToNumberEPNS0_11interpreter21BytecodeArrayIteratorE
	jmp	.L4655
.L4719:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation31VisitLdaNamedPropertyNoFeedbackEPNS0_11interpreter21BytecodeArrayIteratorE
	jmp	.L4655
.L4721:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation27ProcessModuleVariableAccessEPNS0_11interpreter21BytecodeArrayIteratorE
	jmp	.L4655
.L4729:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation27ProcessLdaLookupContextSlotEPNS0_11interpreter21BytecodeArrayIteratorE
	jmp	.L4655
.L4707:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitCallProperty2EPNS0_11interpreter21BytecodeArrayIteratorE
	jmp	.L4655
.L4706:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation26VisitCallUndefinedReceiverEPNS0_11interpreter21BytecodeArrayIteratorE
	jmp	.L4655
.L4705:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation27VisitCallUndefinedReceiver0EPNS0_11interpreter21BytecodeArrayIteratorE
	jmp	.L4655
.L4704:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation27VisitCallUndefinedReceiver1EPNS0_11interpreter21BytecodeArrayIteratorE
	jmp	.L4655
.L4703:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation27VisitCallUndefinedReceiver2EPNS0_11interpreter21BytecodeArrayIteratorE
	jmp	.L4655
.L4738:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation14VisitStaGlobalEPNS0_11interpreter21BytecodeArrayIteratorE
	jmp	.L4655
.L4737:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation16VisitPushContextEPNS0_11interpreter21BytecodeArrayIteratorE
	jmp	.L4655
.L4718:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation24VisitStaNamedOwnPropertyEPNS0_11interpreter21BytecodeArrayIteratorE
	jmp	.L4655
.L4717:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation21VisitStaKeyedPropertyEPNS0_11interpreter21BytecodeArrayIteratorE
	jmp	.L4655
.L4720:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation21VisitStaNamedPropertyEPNS0_11interpreter21BytecodeArrayIteratorE
	jmp	.L4655
.L4740:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation16VisitLdaConstantEPNS0_11interpreter21BytecodeArrayIteratorE
	jmp	.L4655
.L4724:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation8VisitMovEPNS0_11interpreter21BytecodeArrayIteratorE
	jmp	.L4655
.L4723:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation21VisitLdaNamedPropertyEPNS0_11interpreter21BytecodeArrayIteratorE
	jmp	.L4655
.L4675:
	movl	$3, %esi
.L4792:
	movq	%r12, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	movq	%rbx, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation12ProcessForInENS0_12FeedbackSlotE
	jmp	.L4655
.L4747:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation12VisitLdaZeroEPNS0_11interpreter21BytecodeArrayIteratorE
	jmp	.L4655
.L4736:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation15VisitPopContextEPNS0_11interpreter21BytecodeArrayIteratorE
	jmp	.L4655
.L4735:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation19VisitLdaContextSlotEPNS0_11interpreter21BytecodeArrayIteratorE
	jmp	.L4655
.L4734:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation28VisitLdaImmutableContextSlotEPNS0_11interpreter21BytecodeArrayIteratorE
	jmp	.L4655
.L4733:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation26VisitLdaCurrentContextSlotEPNS0_11interpreter21BytecodeArrayIteratorE
	jmp	.L4655
.L4677:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation26VisitSwitchOnSmiNoFeedbackEPNS0_11interpreter21BytecodeArrayIteratorE
	jmp	.L4655
.L4676:
	movl	$1, %esi
	jmp	.L4792
.L4716:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation22VisitStaInArrayLiteralEPNS0_11interpreter21BytecodeArrayIteratorE
	jmp	.L4655
.L4715:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation29VisitStaDataPropertyInLiteralEPNS0_11interpreter21BytecodeArrayIteratorE
	jmp	.L4655
.L4732:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation35VisitLdaImmutableCurrentContextSlotEPNS0_11interpreter21BytecodeArrayIteratorE
	jmp	.L4655
.L4731:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation19VisitStaContextSlotEPNS0_11interpreter21BytecodeArrayIteratorE
	jmp	.L4655
.L4730:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor15GetIndexOperandEi@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	%eax, %edx
	movq	24(%rbx), %rax
	movq	%rbx, %rdi
	leaq	480(%rax), %rsi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation20ProcessContextAccessERKNS1_5HintsEiiNS2_21ContextProcessingModeEPS3_
	jmp	.L4655
.L4682:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitCreateClosureEPNS0_11interpreter21BytecodeArrayIteratorE
	jmp	.L4655
.L4726:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation9VisitLdarEPNS0_11interpreter21BytecodeArrayIteratorE
	jmp	.L4655
.L4725:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation9VisitStarEPNS0_11interpreter21BytecodeArrayIteratorE
	jmp	.L4655
.L4702:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation19VisitCallNoFeedbackEPNS0_11interpreter21BytecodeArrayIteratorE
	jmp	.L4655
.L4701:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation19VisitCallWithSpreadEPNS0_11interpreter21BytecodeArrayIteratorE
	jmp	.L4655
.L4700:
	movq	24(%rbx), %rax
	movq	936(%rax), %r15
	movq	944(%rax), %r14
	cmpq	%r14, %r15
	je	.L4655
	.p2align 4,,10
	.p2align 3
.L4750:
	movq	%r15, %rdi
	addq	$224, %r15
	call	_ZN2v88internal8compiler5Hints5ClearEv
	cmpq	%r15, %r14
	jne	.L4750
	jmp	.L4655
.L4711:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation20VisitCallAnyReceiverEPNS0_11interpreter21BytecodeArrayIteratorE
	jmp	.L4655
.L4710:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation17VisitCallPropertyEPNS0_11interpreter21BytecodeArrayIteratorE
	jmp	.L4655
.L4709:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitCallProperty0EPNS0_11interpreter21BytecodeArrayIteratorE
	jmp	.L4655
.L4708:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitCallProperty1EPNS0_11interpreter21BytecodeArrayIteratorE
	jmp	.L4655
.L4673:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitReturnEPNS0_11interpreter21BytecodeArrayIteratorE
	jmp	.L4655
.L4672:
	movq	(%rbx), %rax
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	(%rax), %rdx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor26GetConstantForIndexOperandEiPNS0_7IsolateE@PLT
	movq	(%rbx), %rsi
	movl	$1, %ecx
	movq	-424(%rbp), %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler9ObjectRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	jmp	.L4655
.L4712:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation24VisitGetSuperConstructorEPNS0_11interpreter21BytecodeArrayIteratorE
	jmp	.L4655
.L4699:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation18VisitCallJSRuntimeEPNS0_11interpreter21BytecodeArrayIteratorE
	jmp	.L4655
.L4698:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation20VisitInvokeIntrinsicEPNS0_11interpreter21BytecodeArrayIteratorE
	jmp	.L4655
.L4697:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation14VisitConstructEPNS0_11interpreter21BytecodeArrayIteratorE
	jmp	.L4655
.L4696:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation24VisitConstructWithSpreadEPNS0_11interpreter21BytecodeArrayIteratorE
	jmp	.L4655
.L4667:
	movq	24(%rbx), %rax
	movq	936(%rax), %r15
	movq	944(%rax), %r14
	cmpq	%r14, %r15
	je	.L4655
	.p2align 4,,10
	.p2align 3
.L4751:
	movq	%r15, %rdi
	addq	$224, %r15
	call	_ZN2v88internal8compiler5Hints5ClearEv
	cmpq	%r15, %r14
	jne	.L4751
	jmp	.L4655
.L4668:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation16VisitGetIteratorEPNS0_11interpreter21BytecodeArrayIteratorE
	jmp	.L4655
.L4669:
	movq	24(%rbx), %rax
	movq	936(%rax), %r15
	movq	944(%rax), %r14
	cmpq	%r14, %r15
	je	.L4655
	.p2align 4,,10
	.p2align 3
.L4752:
	movq	%r15, %rdi
	addq	$224, %r15
	call	_ZN2v88internal8compiler5Hints5ClearEv
	cmpq	%r15, %r14
	jne	.L4752
	jmp	.L4655
.L4670:
	movq	24(%rbx), %rax
	movq	936(%rax), %r15
	movq	944(%rax), %r14
	cmpq	%r14, %r15
	je	.L4655
	.p2align 4,,10
	.p2align 3
.L4753:
	movq	%r15, %rdi
	addq	$224, %r15
	call	_ZN2v88internal8compiler5Hints5ClearEv
	cmpq	%r15, %r14
	jne	.L4753
	jmp	.L4655
.L4686:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation24VisitCreateRegExpLiteralEPNS0_11interpreter21BytecodeArrayIteratorE
	jmp	.L4655
.L4685:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation23VisitCreateArrayLiteralEPNS0_11interpreter21BytecodeArrayIteratorE
	jmp	.L4655
.L4684:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation24VisitCreateObjectLiteralEPNS0_11interpreter21BytecodeArrayIteratorE
	jmp	.L4655
.L4683:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation22VisitGetTemplateObjectEPNS0_11interpreter21BytecodeArrayIteratorE
	jmp	.L4655
.L4671:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation27VisitSwitchOnGeneratorStateEPNS0_11interpreter21BytecodeArrayIteratorE
	jmp	.L4655
.L4689:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation19VisitTestInstanceOfEPNS0_11interpreter21BytecodeArrayIteratorE
	jmp	.L4655
.L4688:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitTestInEPNS0_11interpreter21BytecodeArrayIteratorE
	jmp	.L4655
.L4722:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation21VisitLdaKeyedPropertyEPNS0_11interpreter21BytecodeArrayIteratorE
	jmp	.L4655
.L4744:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation12VisitLdaNullEPNS0_11interpreter21BytecodeArrayIteratorE
	jmp	.L4655
.L4743:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation15VisitLdaTheHoleEPNS0_11interpreter21BytecodeArrayIteratorE
	jmp	.L4655
.L4742:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation12VisitLdaTrueEPNS0_11interpreter21BytecodeArrayIteratorE
	jmp	.L4655
.L4741:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation13VisitLdaFalseEPNS0_11interpreter21BytecodeArrayIteratorE
	jmp	.L4655
.L4746:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation11VisitLdaSmiEPNS0_11interpreter21BytecodeArrayIteratorE
	jmp	.L4655
.L4745:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation17VisitLdaUndefinedEPNS0_11interpreter21BytecodeArrayIteratorE
	jmp	.L4655
.L4802:
	movq	(%rbx), %rax
	movl	$71, %esi
	movq	(%rax), %rdi
	call	_ZN2v88internal7Isolate10CountUsageENS_7Isolate17UseCounterFeatureE@PLT
	jmp	.L4755
.L4664:
	leaq	.LC32(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L4803:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18910:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation16TraverseBytecodeEv, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation16TraverseBytecodeEv
	.section	.rodata._ZN2v88internal8compiler34SerializerForBackgroundCompilation3RunEv.str1.8,"aMS",@progbits,1
	.align 8
.LC66:
	.string	"SerializerForBackgroundCompilation::Run"
	.align 8
.LC67:
	.string	"Already ran serializer for SharedFunctionInfo "
	.section	.rodata._ZN2v88internal8compiler34SerializerForBackgroundCompilation3RunEv.str1.1,"aMS",@progbits,1
.LC68:
	.string	", bailing out.\n"
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation3RunEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation3RunEv
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation3RunEv, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation3RunEv:
.LFB18890:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-304(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$312, %rsp
	movq	(%rsi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 124(%r14)
	je	.L4809
	cmpb	$0, _ZN2v88internal30FLAG_trace_heap_broker_verboseE(%rip)
	jne	.L4821
.L4809:
	movq	%r14, %rdi
	leaq	-320(%rbp), %r15
	call	_ZN2v88internal8compiler12JSHeapBroker27IncrementTracingIndentationEv@PLT
	movq	24(%rbx), %rsi
	leaq	-288(%rbp), %r8
	movq	%r8, %rdi
	movq	%r8, -344(%rbp)
	movq	8(%rsi), %rax
	addq	$24, %rsi
	movq	%rax, -304(%rbp)
	movq	-8(%rsi), %rax
	movq	%rax, -296(%rbp)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movq	(%rbx), %rsi
	movl	$1, %ecx
	movq	%r15, %rdi
	movq	-304(%rbp), %rdx
	call	_ZN2v88internal8compiler21SharedFunctionInfoRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	-344(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler34SerializerForBackgroundCompilation15feedback_vectorEv
	movq	(%rbx), %rsi
	movq	%r13, %rdi
	movl	$1, %ecx
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler17FeedbackVectorRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	-304(%rbp), %rsi
	movq	-296(%rbp), %rdx
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler21SharedFunctionInfoRef26IsSerializedForCompilationENS1_17FeedbackVectorRefE@PLT
	testb	%al, %al
	je	.L4810
	movq	(%rbx), %rdi
	cmpb	$0, 124(%rdi)
	jne	.L4822
.L4811:
	movq	16(%rbx), %rax
	leaq	16(%r12), %rdx
	movl	$0, 16(%r12)
	movq	%rdx, 32(%r12)
	movq	%rdx, 40(%r12)
	leaq	72(%r12), %rdx
	movq	%rax, (%r12)
	movq	%rax, 56(%r12)
	movq	%rdx, 88(%r12)
	movq	%rdx, 96(%r12)
	leaq	128(%r12), %rdx
	movq	%rax, 112(%r12)
	movq	%rax, 168(%r12)
	leaq	184(%r12), %rax
	movq	$0, 24(%r12)
	movq	$0, 48(%r12)
	movl	$0, 72(%r12)
	movq	$0, 80(%r12)
	movq	$0, 104(%r12)
	movl	$0, 128(%r12)
	movq	$0, 136(%r12)
	movq	%rdx, 144(%r12)
	movq	%rdx, 152(%r12)
	movq	$0, 160(%r12)
	movl	$0, 184(%r12)
	movq	$0, 192(%r12)
	movq	%rax, 200(%r12)
	movq	%rax, 208(%r12)
	movq	$0, 216(%r12)
.L4812:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler12JSHeapBroker27DecrementTracingIndentationEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4823
	addq	$312, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4822:
	.cfi_restore_state
	cmpb	$0, _ZN2v88internal30FLAG_trace_heap_broker_verboseE(%rip)
	je	.L4811
	call	_ZN2v88internal8compiler12JSHeapBroker5TraceEv@PLT
	movl	$46, %edx
	leaq	.LC67(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler21SharedFunctionInfoRef6objectEv@PLT
	leaq	-328(%rbp), %rsi
	movq	%r13, %rdi
	movq	(%rax), %rax
	movq	%rax, -328(%rbp)
	call	_ZN2v88internallsERSoRKNS0_5BriefE@PLT
	movl	$15, %edx
	leaq	.LC68(%rip), %rsi
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	-329(%rbp), %rsi
	movq	%r13, %rdi
	movl	$1, %edx
	movb	$10, -329(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L4811
	.p2align 4,,10
	.p2align 3
.L4810:
	movq	-304(%rbp), %rsi
	movq	-296(%rbp), %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21SharedFunctionInfoRef27SetSerializedForCompilationENS1_17FeedbackVectorRefE@PLT
	testb	$2, 96(%rbx)
	jne	.L4824
.L4813:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler17FeedbackVectorRef9SerializeEv@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation16TraverseBytecodeEv
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	addq	$704, %rsi
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	jmp	.L4812
	.p2align 4,,10
	.p2align 3
.L4821:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler12JSHeapBroker5TraceEv@PLT
	movl	$8, %edx
	leaq	.LC56(%rip), %rsi
	movq	%rax, %r15
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r15, %rdi
	movl	$39, %edx
	leaq	.LC66(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$4, %edx
	movq	%r15, %rdi
	leaq	.LC58(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZNSo9_M_insertIPKvEERSoT_@PLT
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$10, -304(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L4809
	.p2align 4,,10
	.p2align 3
.L4824:
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler21SharedFunctionInfoRef6objectEv@PLT
	movq	%rax, %rsi
	movq	(%rbx), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal18SharedFunctionInfo30EnsureSourcePositionsAvailableEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	jmp	.L4813
.L4823:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18890:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation3RunEv, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation3RunEv
	.section	.text._ZN2v88internal8compiler37RunSerializerForBackgroundCompilationEPNS1_12JSHeapBrokerEPNS1_23CompilationDependenciesEPNS0_4ZoneENS0_6HandleINS0_10JSFunctionEEENS_4base5FlagsINS1_38SerializerForBackgroundCompilationFlagEiEENS0_9BailoutIdE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal8compiler37RunSerializerForBackgroundCompilationEPNS1_12JSHeapBrokerEPNS1_23CompilationDependenciesEPNS0_4ZoneENS0_6HandleINS0_10JSFunctionEEENS_4base5FlagsINS1_38SerializerForBackgroundCompilationFlagEiEENS0_9BailoutIdE
	.type	_ZN2v88internal8compiler37RunSerializerForBackgroundCompilationEPNS1_12JSHeapBrokerEPNS1_23CompilationDependenciesEPNS0_4ZoneENS0_6HandleINS0_10JSFunctionEEENS_4base5FlagsINS1_38SerializerForBackgroundCompilationFlagEiEENS0_9BailoutIdE, @function
_ZN2v88internal8compiler37RunSerializerForBackgroundCompilationEPNS1_12JSHeapBrokerEPNS1_23CompilationDependenciesEPNS0_4ZoneENS0_6HandleINS0_10JSFunctionEEENS_4base5FlagsINS1_38SerializerForBackgroundCompilationFlagEiEENS0_9BailoutIdE:
.LFB18713:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-368(%rbp), %r13
	leaq	-256(%rbp), %r12
	subq	$360, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	pushq	%r9
	movl	%r8d, %r9d
	movq	%rcx, %r8
	movq	%rdx, %rcx
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilationC1EPNS1_12JSHeapBrokerEPNS1_23CompilationDependenciesEPNS0_4ZoneENS0_6HandleINS0_10JSFunctionEEENS_4base5FlagsINS1_38SerializerForBackgroundCompilationFlagEiEENS0_9BailoutIdE
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation3RunEv
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	movq	-312(%rbp), %rax
	popq	%rdx
	popq	%rcx
	testq	%rax, %rax
	je	.L4826
	.p2align 4,,10
	.p2align 3
.L4827:
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L4827
.L4826:
	movq	-320(%rbp), %rax
	movq	-328(%rbp), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4834
	leaq	-16(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4834:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18713:
	.size	_ZN2v88internal8compiler37RunSerializerForBackgroundCompilationEPNS1_12JSHeapBrokerEPNS1_23CompilationDependenciesEPNS0_4ZoneENS0_6HandleINS0_10JSFunctionEEENS_4base5FlagsINS1_38SerializerForBackgroundCompilationFlagEiEENS0_9BailoutIdE, .-_ZN2v88internal8compiler37RunSerializerForBackgroundCompilationEPNS1_12JSHeapBrokerEPNS1_23CompilationDependenciesEPNS0_4ZoneENS0_6HandleINS0_10JSFunctionEEENS_4base5FlagsINS1_38SerializerForBackgroundCompilationFlagEiEENS0_9BailoutIdE
	.section	.text._ZN2v88internal8compiler34SerializerForBackgroundCompilation18RunChildSerializerENS1_18CompilationSubjectENS_4base8OptionalINS1_5HintsEEERKNS0_10ZoneVectorIS6_EEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler34SerializerForBackgroundCompilation18RunChildSerializerENS1_18CompilationSubjectENS_4base8OptionalINS1_5HintsEEERKNS0_10ZoneVectorIS6_EEb
	.type	_ZN2v88internal8compiler34SerializerForBackgroundCompilation18RunChildSerializerENS1_18CompilationSubjectENS_4base8OptionalINS1_5HintsEEERKNS0_10ZoneVectorIS6_EEb, @function
_ZN2v88internal8compiler34SerializerForBackgroundCompilation18RunChildSerializerENS1_18CompilationSubjectENS_4base8OptionalINS1_5HintsEEERKNS0_10ZoneVectorIS6_EEb:
.LFB18986:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	movq	%r8, %rdx
	subq	$664, %rsp
	movq	%rcx, -672(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testb	%r9b, %r9b
	jne	.L4868
	movl	96(%rsi), %eax
	movb	$0, -544(%rbp)
	movb	$0, -536(%rbp)
	movl	%eax, -664(%rbp)
	movq	-672(%rbp), %rax
	cmpb	$0, (%rax)
	jne	.L4869
.L4851:
	movq	(%rbx), %rax
	leaq	-288(%rbp), %r14
	leaq	16(%rbx), %rsi
	movq	%rdx, -680(%rbp)
	leaq	-304(%rbp), %r8
	movq	%r14, %rdi
	leaq	-656(%rbp), %r15
	movq	%rax, -304(%rbp)
	movq	8(%rbx), %rax
	movq	%r8, -672(%rbp)
	movq	%rax, -296(%rbp)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movq	240(%rbx), %rax
	movq	8(%r12), %r11
	movq	%r15, %rdi
	movq	-680(%rbp), %rdx
	movq	16(%r12), %rcx
	leaq	-544(%rbp), %r9
	movq	%rax, -64(%rbp)
	movl	-664(%rbp), %eax
	movq	-672(%rbp), %r8
	pushq	%rax
	pushq	%rdx
	movq	(%r12), %rsi
	movq	%r11, %rdx
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilationC1EPNS1_12JSHeapBrokerEPNS1_23CompilationDependenciesEPNS0_4ZoneENS1_18CompilationSubjectENS_4base8OptionalINS1_5HintsEEERKNS0_10ZoneVectorISC_EENSA_5FlagsINS1_38SerializerForBackgroundCompilationFlagEiEE
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpb	$0, -544(%rbp)
	popq	%rax
	popq	%rdx
	jne	.L4870
.L4852:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation3RunEv
	movq	-600(%rbp), %rax
	testq	%rax, %rax
	je	.L4853
	.p2align 4,,10
	.p2align 3
.L4854:
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L4854
.L4853:
	movq	-608(%rbp), %rax
	movq	-616(%rbp), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
.L4835:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4871
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4870:
	.cfi_restore_state
	leaq	-536(%rbp), %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	jmp	.L4852
	.p2align 4,,10
	.p2align 3
.L4869:
	movq	%rax, %rsi
	leaq	-536(%rbp), %rdi
	movq	%r8, -680(%rbp)
	addq	$8, %rsi
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movb	$1, -544(%rbp)
	movq	-680(%rbp), %rdx
	jmp	.L4851
	.p2align 4,,10
	.p2align 3
.L4868:
	movq	(%r8), %rdi
	movq	8(%r8), %rax
	movq	$0, -648(%rbp)
	movq	$0, -640(%rbp)
	movq	16(%r8), %r14
	movq	%rax, -688(%rbp)
	movq	%r14, -664(%rbp)
	movq	%rdi, -656(%rbp)
	movq	$0, -632(%rbp)
	movq	$0, -680(%rbp)
	subq	%rax, %r14
	je	.L4837
	movq	16(%rdi), %rax
	movq	24(%rdi), %r8
	leaq	7(%r14), %rsi
	andq	$-8, %rsi
	subq	%rax, %r8
	cmpq	%r8, %rsi
	ja	.L4872
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L4839:
	movq	%rax, -680(%rbp)
	movq	16(%rdx), %rax
	movq	%rax, -664(%rbp)
	movq	8(%rdx), %rax
	movq	%rax, -688(%rbp)
.L4837:
	movq	-680(%rbp), %rax
	movq	-688(%rbp), %r15
	leaq	(%rax,%r14), %rcx
	movq	%rax, %xmm0
	movq	%rcx, -632(%rbp)
	movq	-664(%rbp), %rcx
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, -648(%rbp)
	cmpq	%rcx, %r15
	je	.L4840
	movq	%rax, %r14
	.p2align 4,,10
	.p2align 3
.L4841:
	movq	%r15, %rsi
	movq	%r14, %rdi
	addq	$224, %r15
	addq	$224, %r14
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	cmpq	%r15, -664(%rbp)
	jne	.L4841
	movq	-664(%rbp), %rax
	movabsq	$411757680216731063, %r15
	movabsq	$576460752303423487, %rdx
	subq	$224, %rax
	subq	-688(%rbp), %rax
	shrq	$5, %rax
	imulq	%r15, %rax
	andq	%rdx, %rax
	leaq	0(,%rax,8), %rdi
	subq	%rax, %rdi
	salq	$5, %rdi
	addq	-680(%rbp), %rdi
.L4842:
	movq	%rdi, -640(%rbp)
	leaq	-288(%rbp), %r14
	call	_ZN2v88internal8compiler5HintsD1Ev
	movq	16(%r12), %rax
	leaq	-232(%rbp), %rdx
	movl	$0, -288(%rbp)
	movq	%rdx, -216(%rbp)
	leaq	-304(%rbp), %rcx
	movq	%rax, -304(%rbp)
	movq	%rax, -248(%rbp)
	movq	%rax, -192(%rbp)
	movq	%rax, -136(%rbp)
	leaq	-120(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	%rax, -96(%rbp)
	movq	(%rbx), %rax
	movq	%rdx, -208(%rbp)
	leaq	-176(%rbp), %rdx
	movq	%rdx, -160(%rbp)
	movq	%rdx, -152(%rbp)
	movq	$0, -280(%rbp)
	movq	%r14, -272(%rbp)
	movq	%r14, -264(%rbp)
	movq	$0, -256(%rbp)
	movl	$0, -232(%rbp)
	movq	$0, -224(%rbp)
	movq	$0, -200(%rbp)
	movl	$0, -176(%rbp)
	movq	$0, -168(%rbp)
	movq	$0, -144(%rbp)
	movl	$0, -120(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -88(%rbp)
	movq	(%rax), %rax
	movq	%rcx, -680(%rbp)
	movq	%rax, -544(%rbp)
	leaq	-544(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -688(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo16GetBytecodeArrayEv
	movq	-640(%rbp), %rsi
	movq	-648(%rbp), %rcx
	movabsq	$7905747460161236407, %rdi
	movl	43(%rax), %edx
	movq	%rsi, %rax
	subq	%rcx, %rax
	sarl	$3, %edx
	sarq	$5, %rax
	movslq	%edx, %rdx
	imulq	%rdi, %rax
	cmpq	%rax, %rdx
	ja	.L4873
	leaq	-656(%rbp), %r15
	jb	.L4874
.L4844:
	movq	-680(%rbp), %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	movq	-672(%rbp), %rax
	movb	$0, -544(%rbp)
	movb	$0, -536(%rbp)
	cmpb	$0, (%rax)
	jne	.L4875
.L4846:
	movq	(%rbx), %rax
	leaq	16(%rbx), %rsi
	movq	%r14, %rdi
	movq	%rax, -304(%rbp)
	movq	8(%rbx), %rax
	movq	%rax, -296(%rbp)
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	xorl	%r9d, %r9d
	movq	%r15, %r8
	movq	%r12, %rsi
	movq	240(%rbx), %rax
	movq	-688(%rbp), %rcx
	movq	%r13, %rdi
	movq	-680(%rbp), %rdx
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler34SerializerForBackgroundCompilation18RunChildSerializerENS1_18CompilationSubjectENS_4base8OptionalINS1_5HintsEEERKNS0_10ZoneVectorIS6_EEb
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpb	$0, -544(%rbp)
	jne	.L4876
.L4847:
	movq	-640(%rbp), %r12
	movq	-648(%rbp), %rbx
	cmpq	%rbx, %r12
	je	.L4835
	.p2align 4,,10
	.p2align 3
.L4849:
	movq	%rbx, %rdi
	addq	$224, %rbx
	call	_ZN2v88internal8compiler5HintsD1Ev
	cmpq	%rbx, %r12
	jne	.L4849
	jmp	.L4835
	.p2align 4,,10
	.p2align 3
.L4874:
	leaq	0(,%rdx,8), %rax
	subq	%rdx, %rax
	salq	$5, %rax
	addq	%rcx, %rax
	movq	%rax, -696(%rbp)
	cmpq	%rax, %rsi
	je	.L4844
	movq	%rax, %r15
	.p2align 4,,10
	.p2align 3
.L4845:
	movq	%r15, %rdi
	movq	%rsi, -664(%rbp)
	addq	$224, %r15
	call	_ZN2v88internal8compiler5HintsD1Ev
	movq	-664(%rbp), %rsi
	cmpq	%r15, %rsi
	jne	.L4845
	movq	-696(%rbp), %rax
	leaq	-656(%rbp), %r15
	movq	%rax, -640(%rbp)
	jmp	.L4844
	.p2align 4,,10
	.p2align 3
.L4875:
	movq	%rax, %rsi
	leaq	-536(%rbp), %rdi
	addq	$8, %rsi
	call	_ZN2v88internal8compiler5HintsC1ERKS2_
	movb	$1, -544(%rbp)
	jmp	.L4846
	.p2align 4,,10
	.p2align 3
.L4876:
	leaq	-536(%rbp), %rdi
	call	_ZN2v88internal8compiler5HintsD1Ev
	jmp	.L4847
	.p2align 4,,10
	.p2align 3
.L4873:
	leaq	-656(%rbp), %r15
	movq	-680(%rbp), %rcx
	subq	%rax, %rdx
	movq	%r15, %rdi
	call	_ZNSt6vectorIN2v88internal8compiler5HintsENS1_13ZoneAllocatorIS3_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS3_S6_EEmRKS3_
	jmp	.L4844
	.p2align 4,,10
	.p2align 3
.L4840:
	movq	%rax, %rdi
	subq	$224, %rdi
	jmp	.L4842
.L4872:
	movq	%rdx, -664(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-664(%rbp), %rdx
	jmp	.L4839
.L4871:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18986:
	.size	_ZN2v88internal8compiler34SerializerForBackgroundCompilation18RunChildSerializerENS1_18CompilationSubjectENS_4base8OptionalINS1_5HintsEEERKNS0_10ZoneVectorIS6_EEb, .-_ZN2v88internal8compiler34SerializerForBackgroundCompilation18RunChildSerializerENS1_18CompilationSubjectENS_4base8OptionalINS1_5HintsEEERKNS0_10ZoneVectorIS6_EEb
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler37RunSerializerForBackgroundCompilationEPNS1_12JSHeapBrokerEPNS1_23CompilationDependenciesEPNS0_4ZoneENS0_6HandleINS0_10JSFunctionEEENS_4base5FlagsINS1_38SerializerForBackgroundCompilationFlagEiEENS0_9BailoutIdE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler37RunSerializerForBackgroundCompilationEPNS1_12JSHeapBrokerEPNS1_23CompilationDependenciesEPNS0_4ZoneENS0_6HandleINS0_10JSFunctionEEENS_4base5FlagsINS1_38SerializerForBackgroundCompilationFlagEiEENS0_9BailoutIdE, @function
_GLOBAL__sub_I__ZN2v88internal8compiler37RunSerializerForBackgroundCompilationEPNS1_12JSHeapBrokerEPNS1_23CompilationDependenciesEPNS0_4ZoneENS0_6HandleINS0_10JSFunctionEEENS_4base5FlagsINS1_38SerializerForBackgroundCompilationFlagEiEENS0_9BailoutIdE:
.LFB25049:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE25049:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler37RunSerializerForBackgroundCompilationEPNS1_12JSHeapBrokerEPNS1_23CompilationDependenciesEPNS0_4ZoneENS0_6HandleINS0_10JSFunctionEEENS_4base5FlagsINS1_38SerializerForBackgroundCompilationFlagEiEENS0_9BailoutIdE, .-_GLOBAL__sub_I__ZN2v88internal8compiler37RunSerializerForBackgroundCompilationEPNS1_12JSHeapBrokerEPNS1_23CompilationDependenciesEPNS0_4ZoneENS0_6HandleINS0_10JSFunctionEEENS_4base5FlagsINS1_38SerializerForBackgroundCompilationFlagEiEENS0_9BailoutIdE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler37RunSerializerForBackgroundCompilationEPNS1_12JSHeapBrokerEPNS1_23CompilationDependenciesEPNS0_4ZoneENS0_6HandleINS0_10JSFunctionEEENS_4base5FlagsINS1_38SerializerForBackgroundCompilationFlagEiEENS0_9BailoutIdE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.data.rel.ro,"aw"
	.align 8
.LC36:
	.quad	_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE+24
	.align 8
.LC37:
	.quad	_ZTVSt15basic_streambufIcSt11char_traitsIcEE+16
	.align 8
.LC38:
	.quad	_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE+16
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
