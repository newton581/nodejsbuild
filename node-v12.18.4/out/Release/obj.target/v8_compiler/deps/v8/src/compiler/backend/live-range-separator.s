	.file	"live-range-separator.cc"
	.text
	.section	.rodata._ZN2v88internal8compiler12_GLOBAL__N_114CreateSplinterEPNS1_17TopLevelLiveRangeEPNS1_22RegisterAllocationDataENS1_16LifetimePositionES7_b.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"creating splinter %d for range %d between %d and %d\n"
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_114CreateSplinterEPNS1_17TopLevelLiveRangeEPNS1_22RegisterAllocationDataENS1_16LifetimePositionES7_b,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_114CreateSplinterEPNS1_17TopLevelLiveRangeEPNS1_22RegisterAllocationDataENS1_16LifetimePositionES7_b, @function
_ZN2v88internal8compiler12_GLOBAL__N_114CreateSplinterEPNS1_17TopLevelLiveRangeEPNS1_22RegisterAllocationDataENS1_16LifetimePositionES7_b:
.LFB11016:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%r8d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	16(%rdi), %rax
	movq	8(%rdi), %rdx
	movl	(%rax), %eax
	movl	4(%rdx), %r12d
	cmpl	%r14d, %eax
	jl	.L2
	movl	%ecx, %edx
	movl	%eax, %r14d
	andl	$-4, %edx
	addl	$4, %edx
	cmpl	%r12d, %edx
	jge	.L1
.L2:
	cmpl	%r12d, %ecx
	cmovle	%ecx, %r12d
	cmpl	%r12d, %r14d
	jge	.L1
	movl	4(%r13), %eax
	shrl	$5, %eax
	andl	$3, %eax
	cmpl	$1, %eax
	je	.L5
	cmpq	$0, 104(%r13)
	je	.L21
.L5:
	cmpq	$0, 144(%r13)
	je	.L22
.L7:
	movq	(%rbx), %rbx
	testb	%r15b, %r15b
	jne	.L23
.L10:
	addq	$24, %rsp
	movq	%rbx, %rcx
	movl	%r12d, %edx
	movl	%r14d, %esi
	popq	%rbx
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler17TopLevelLiveRange8SplinterENS1_16LifetimePositionES3_PNS0_4ZoneE@PLT
	.p2align 4,,10
	.p2align 3
.L1:
	.cfi_restore_state
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	.cfi_restore_state
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler22RegisterAllocationData28CreateSpillRangeForLiveRangeEPNS1_17TopLevelLiveRangeE@PLT
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L23:
	testl	%r12d, %r12d
	leal	3(%r12), %r8d
	leal	3(%r14), %ecx
	movl	88(%r13), %edx
	cmovns	%r12d, %r8d
	movq	144(%r13), %rax
	testl	%r14d, %r14d
	leaq	.LC0(%rip), %rdi
	cmovns	%r14d, %ecx
	movl	88(%rax), %esi
	sarl	$2, %r8d
	xorl	%eax, %eax
	sarl	$2, %ecx
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L22:
	movl	4(%r13), %esi
	movq	%rbx, %rdi
	shrl	$13, %esi
	call	_ZN2v88internal8compiler22RegisterAllocationData13NextLiveRangeENS0_21MachineRepresentationE@PLT
	movq	168(%rbx), %rdx
	movslq	88(%rax), %rcx
	movq	%rax, (%rdx,%rcx,8)
	movq	%r13, %rdx
	movq	%rax, 144(%r13)
	.p2align 4,,10
	.p2align 3
.L8:
	movq	%rdx, %rcx
	movq	96(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L8
	movl	92(%rcx), %edi
	movq	%r13, %rsi
	movq	%rax, -56(%rbp)
	leal	1(%rdi), %edx
	movq	%rax, %rdi
	movl	%edx, 92(%rcx)
	movl	%edx, (%rax)
	movl	4(%rax), %edx
	movl	4(%r13), %ecx
	andl	$-97, %edx
	andl	$96, %ecx
	orl	%ecx, %edx
	movl	%edx, 4(%rax)
	call	_ZN2v88internal8compiler17TopLevelLiveRange17SetSplinteredFromEPS2_@PLT
	movq	80(%r13), %rdx
	testq	%rdx, %rdx
	je	.L7
	movq	-56(%rbp), %rax
	movq	%rdx, 80(%rax)
	jmp	.L7
	.cfi_endproc
.LFE11016:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_114CreateSplinterEPNS1_17TopLevelLiveRangeEPNS1_22RegisterAllocationDataENS1_16LifetimePositionES7_b, .-_ZN2v88internal8compiler12_GLOBAL__N_114CreateSplinterEPNS1_17TopLevelLiveRangeEPNS1_22RegisterAllocationDataENS1_16LifetimePositionES7_b
	.section	.rodata._ZN2v88internal8compiler18LiveRangeSeparator8SplinterEv.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"vector::_M_range_check: __n (which is %zu) >= this->size() (which is %zu)"
	.section	.text._ZN2v88internal8compiler18LiveRangeSeparator8SplinterEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18LiveRangeSeparator8SplinterEv
	.type	_ZN2v88internal8compiler18LiveRangeSeparator8SplinterEv, @function
_ZN2v88internal8compiler18LiveRangeSeparator8SplinterEv:
.LFB11019:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rax
	movq	%rdi, -88(%rbp)
	movq	168(%rax), %rdx
	movq	176(%rax), %rcx
	subq	%rdx, %rcx
	sarq	$3, %rcx
	movq	%rcx, -80(%rbp)
	je	.L24
	xorl	%r12d, %r12d
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L27:
	addq	$1, %r12
	cmpq	%r12, -80(%rbp)
	je	.L24
	movq	-88(%rbp), %rax
	movq	(%rax), %rax
	movq	168(%rax), %rdx
.L53:
	movq	(%rdx,%r12,8), %rbx
	testq	%rbx, %rbx
	je	.L27
	movq	16(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L27
	cmpq	$0, 96(%rbx)
	jne	.L27
	movl	(%rdx), %edx
	movq	16(%rax), %rdi
	testl	%edx, %edx
	leal	3(%rdx), %esi
	cmovns	%edx, %esi
	andl	$2, %edx
	sarl	$2, %esi
	cmpl	$1, %edx
	sbbl	$-1, %esi
	call	_ZNK2v88internal8compiler19InstructionSequence19GetInstructionBlockEi@PLT
	cmpb	$0, 120(%rax)
	jne	.L27
	movq	-88(%rbp), %rax
	movl	$-1, %ecx
	movq	(%rax), %r13
	movq	16(%rbx), %rax
	movq	16(%r13), %r14
	testq	%rax, %rax
	je	.L43
	movq	%r12, -96(%rbp)
	movl	%ecx, %r15d
	movq	%rax, %r12
	.p2align 4,,10
	.p2align 3
.L30:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	8(%r12), %r12
	movl	4(%rdx), %eax
	movq	%rdx, -56(%rbp)
	movl	%eax, -64(%rbp)
	movl	(%rdx), %eax
	testl	%eax, %eax
	leal	3(%rax), %esi
	cmovns	%eax, %esi
	andl	$2, %eax
	sarl	$2, %esi
	cmpl	$1, %eax
	sbbl	$-1, %esi
	call	_ZNK2v88internal8compiler19InstructionSequence19GetInstructionBlockEi@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %r8
	movl	4(%rdx), %eax
	testl	%eax, %eax
	leal	3(%rax), %esi
	cmovns	%eax, %esi
	sarl	$2, %esi
	testb	$2, %al
	jne	.L34
	leal	-1(%rsi), %edx
	testb	$1, %al
	cmove	%edx, %esi
.L34:
	movq	%r14, %rdi
	movq	%r8, -56(%rbp)
	call	_ZNK2v88internal8compiler19InstructionSequence19GetInstructionBlockEi@PLT
	movq	-56(%rbp), %r8
	movl	$-1, %edx
	movl	100(%rax), %r10d
	movslq	100(%r8), %r11
	cmpl	%r10d, %r11d
	jg	.L36
	movq	%r12, -72(%rbp)
	movl	%r15d, %ecx
	movq	%r14, %r12
	movq	%r11, %r15
	movq	%rbx, %r14
	movl	%r11d, -56(%rbp)
	movq	%r13, %rbx
	movq	%r11, %r13
	movl	%r10d, -60(%rbp)
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L116:
	cmpl	$-1, %edx
	jne	.L39
	movl	112(%rsi), %eax
	leal	0(,%rax,4), %edx
.L39:
	movl	116(%rsi), %ecx
	leal	-4(,%rcx,4), %ecx
.L40:
	movl	-56(%rbp), %esi
	addq	$1, %r15
	subl	%r13d, %esi
	addl	%r15d, %esi
	cmpl	%esi, -60(%rbp)
	jl	.L114
.L41:
	movq	16(%r12), %r8
	movq	8(%r8), %rsi
	movq	16(%r8), %r8
	subq	%rsi, %r8
	sarq	$3, %r8
	cmpq	%r15, %r8
	jbe	.L115
	movq	(%rsi,%r15,8), %rsi
	cmpb	$0, 120(%rsi)
	jne	.L116
	cmpl	$-1, %edx
	je	.L40
	movl	488(%rbx), %r8d
	movq	%rbx, %rsi
	movq	%r14, %rdi
	shrl	$2, %r8d
	andl	$1, %r8d
	call	_ZN2v88internal8compiler12_GLOBAL__N_114CreateSplinterEPNS1_17TopLevelLiveRangeEPNS1_22RegisterAllocationDataENS1_16LifetimePositionES7_b
	movl	$-1, %ecx
	movl	$-1, %edx
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L114:
	movq	%rbx, %r13
	movl	%ecx, %r15d
	movq	%r14, %rbx
	movq	%r12, %r14
	movq	-72(%rbp), %r12
	cmpl	$-1, %edx
	je	.L36
	movl	488(%r13), %r8d
	movl	-64(%rbp), %ecx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movl	$-1, %r15d
	shrl	$2, %r8d
	andl	$1, %r8d
	call	_ZN2v88internal8compiler12_GLOBAL__N_114CreateSplinterEPNS1_17TopLevelLiveRangeEPNS1_22RegisterAllocationDataENS1_16LifetimePositionES7_b
.L36:
	testq	%r12, %r12
	jne	.L30
	movq	-96(%rbp), %r12
.L43:
	movl	4(%rbx), %ecx
	testb	$6, %cl
	je	.L27
	movq	144(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L27
	andl	$-7, %ecx
	movq	24(%rbx), %rax
	movl	%ecx, 4(%rbx)
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L44:
	movzbl	28(%rax), %edx
	andl	$3, %edx
	cmpb	$3, %dl
	jne	.L46
	andl	$-7, %ecx
	orl	$4, %ecx
	movl	%ecx, 4(%rbx)
.L46:
	movl	%ecx, %edx
	movq	16(%rax), %rax
	shrl	%edx
	andl	$3, %edx
	jne	.L49
.L110:
	testq	%rax, %rax
	jne	.L44
.L49:
	movl	4(%rsi), %ecx
	movq	24(%rsi), %rax
	andl	$-7, %ecx
	movl	%ecx, 4(%rsi)
	testq	%rax, %rax
	jne	.L45
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L24:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L117:
	.cfi_restore_state
	testq	%rax, %rax
	je	.L27
.L45:
	movzbl	28(%rax), %edx
	andl	$3, %edx
	cmpb	$3, %dl
	jne	.L50
	andl	$-7, %ecx
	orl	$4, %ecx
	movl	%ecx, 4(%rsi)
.L50:
	movl	%ecx, %edx
	movq	16(%rax), %rax
	shrl	%edx
	andl	$3, %edx
	je	.L117
	jmp	.L27
.L115:
	movq	%r8, %rdx
	movq	%r15, %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE11019:
	.size	_ZN2v88internal8compiler18LiveRangeSeparator8SplinterEv, .-_ZN2v88internal8compiler18LiveRangeSeparator8SplinterEv
	.section	.text._ZN2v88internal8compiler15LiveRangeMerger33MarkRangesSpilledInDeferredBlocksEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15LiveRangeMerger33MarkRangesSpilledInDeferredBlocksEv
	.type	_ZN2v88internal8compiler15LiveRangeMerger33MarkRangesSpilledInDeferredBlocksEv, @function
_ZN2v88internal8compiler15LiveRangeMerger33MarkRangesSpilledInDeferredBlocksEv:
.LFB11020:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rax
	movq	16(%rax), %rdx
	movq	176(%rax), %r13
	movq	168(%rax), %r12
	movq	%rdx, -56(%rbp)
	cmpq	%r13, %r12
	je	.L118
	movq	%rdi, %r14
	.p2align 4,,10
	.p2align 3
.L134:
	movq	(%r12), %r15
	testq	%r15, %r15
	je	.L121
	cmpq	$0, 16(%r15)
	je	.L121
	movq	144(%r15), %rsi
	testq	%rsi, %rsi
	je	.L121
	movl	4(%r15), %eax
	movl	%eax, %ecx
	shrl	$5, %ecx
	andl	$3, %ecx
	cmpl	$1, %ecx
	je	.L121
	testb	$64, 4(%rsi)
	je	.L121
	movq	%r15, %rbx
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L153:
	movl	4(%rbx), %eax
.L124:
	testb	$1, %al
	jne	.L121
	movq	16(%rbx), %rax
	movq	%rbx, %rdi
	movl	(%rax), %esi
	call	_ZNK2v88internal8compiler9LiveRange16NextSlotPositionENS1_16LifetimePositionE@PLT
	testq	%rax, %rax
	jne	.L121
	movq	40(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L153
	movq	-56(%rbp), %rax
	movq	16(%rax), %rax
	movq	16(%rax), %rbx
	subq	8(%rax), %rbx
	movq	(%r14), %rax
	sarq	$3, %rbx
	movq	(%rax), %rdi
	movl	$-1, 124(%r15)
	movb	$1, 120(%r15)
	movq	$0, 112(%r15)
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	subq	%rcx, %rax
	cmpq	$15, %rax
	jbe	.L154
	leaq	16(%rcx), %rax
	movq	%rax, 16(%rdi)
.L125:
	movl	%ebx, (%rcx)
	cmpl	$64, %ebx
	jle	.L155
	subl	$1, %ebx
	movq	$0, 8(%rcx)
	sarl	$6, %ebx
	leal	1(%rbx), %eax
	movl	%eax, 4(%rcx)
	cltq
	movq	24(%rdi), %r8
	leaq	0(,%rax,8), %rsi
	movq	16(%rdi), %rax
	subq	%rax, %r8
	cmpq	%r8, %rsi
	ja	.L156
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L129:
	movl	4(%rcx), %esi
	movq	%rax, 8(%rcx)
	cmpl	$1, %esi
	je	.L157
	testl	%esi, %esi
	jle	.L127
	movq	$0, (%rax)
	cmpl	$1, 4(%rcx)
	movl	$8, %esi
	movl	$1, %eax
	jle	.L127
	.p2align 4,,10
	.p2align 3
.L132:
	movq	8(%rcx), %rdi
	addl	$1, %eax
	movq	$0, (%rdi,%rsi)
	addq	$8, %rsi
	cmpl	%eax, 4(%rcx)
	jg	.L132
.L127:
	movq	%rcx, 112(%r15)
	.p2align 4,,10
	.p2align 3
.L121:
	addq	$8, %r12
	cmpq	%r12, %r13
	jne	.L134
.L118:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L155:
	.cfi_restore_state
	movl	$1, 4(%rcx)
	movq	$0, 8(%rcx)
	movq	%rcx, 112(%r15)
	jmp	.L121
.L157:
	movq	$0, 8(%rcx)
	jmp	.L127
.L156:
	movq	%rcx, -64(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-64(%rbp), %rcx
	jmp	.L129
.L154:
	movl	$16, %esi
	movq	%rdi, -64(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-64(%rbp), %rdi
	movq	%rax, %rcx
	jmp	.L125
	.cfi_endproc
.LFE11020:
	.size	_ZN2v88internal8compiler15LiveRangeMerger33MarkRangesSpilledInDeferredBlocksEv, .-_ZN2v88internal8compiler15LiveRangeMerger33MarkRangesSpilledInDeferredBlocksEv
	.section	.text._ZN2v88internal8compiler15LiveRangeMerger5MergeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15LiveRangeMerger5MergeEv
	.type	_ZN2v88internal8compiler15LiveRangeMerger5MergeEv, @function
_ZN2v88internal8compiler15LiveRangeMerger5MergeEv:
.LFB11021:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	call	_ZN2v88internal8compiler15LiveRangeMerger33MarkRangesSpilledInDeferredBlocksEv
	movq	(%r14), %rax
	movq	168(%rax), %rdx
	movq	176(%rax), %rcx
	subq	%rdx, %rcx
	sarq	$3, %rcx
	testl	%ecx, %ecx
	jle	.L158
	leal	-1(%rcx), %r12d
	xorl	%ebx, %ebx
	salq	$3, %r12
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L169:
	movq	(%r14), %rax
	addq	$8, %rbx
	movq	168(%rax), %rdx
.L161:
	movq	(%rdx,%rbx), %rsi
	testq	%rsi, %rsi
	je	.L160
	cmpq	$0, 16(%rsi)
	je	.L160
	movq	96(%rsi), %rdi
	testq	%rdi, %rdi
	je	.L160
	movq	(%rax), %rdx
	movslq	88(%rsi), %r13
	call	_ZN2v88internal8compiler17TopLevelLiveRange5MergeEPS2_PNS0_4ZoneE@PLT
	movq	(%r14), %rax
	movq	168(%rax), %rax
	movq	$0, (%rax,%r13,8)
.L160:
	cmpq	%r12, %rbx
	jne	.L169
.L158:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11021:
	.size	_ZN2v88internal8compiler15LiveRangeMerger5MergeEv, .-_ZN2v88internal8compiler15LiveRangeMerger5MergeEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler18LiveRangeSeparator8SplinterEv,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler18LiveRangeSeparator8SplinterEv, @function
_GLOBAL__sub_I__ZN2v88internal8compiler18LiveRangeSeparator8SplinterEv:
.LFB13543:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE13543:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler18LiveRangeSeparator8SplinterEv, .-_GLOBAL__sub_I__ZN2v88internal8compiler18LiveRangeSeparator8SplinterEv
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler18LiveRangeSeparator8SplinterEv
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
