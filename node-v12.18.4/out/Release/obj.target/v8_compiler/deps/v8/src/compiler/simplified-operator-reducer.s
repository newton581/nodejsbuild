	.file	"simplified-operator-reducer.cc"
	.text
	.section	.rodata._ZNK2v88internal8compiler25SimplifiedOperatorReducer12reducer_nameEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"SimplifiedOperatorReducer"
	.section	.text._ZNK2v88internal8compiler25SimplifiedOperatorReducer12reducer_nameEv,"axG",@progbits,_ZNK2v88internal8compiler25SimplifiedOperatorReducer12reducer_nameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal8compiler25SimplifiedOperatorReducer12reducer_nameEv
	.type	_ZNK2v88internal8compiler25SimplifiedOperatorReducer12reducer_nameEv, @function
_ZNK2v88internal8compiler25SimplifiedOperatorReducer12reducer_nameEv:
.LFB10163:
	.cfi_startproc
	endbr64
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE10163:
	.size	_ZNK2v88internal8compiler25SimplifiedOperatorReducer12reducer_nameEv, .-_ZNK2v88internal8compiler25SimplifiedOperatorReducer12reducer_nameEv
	.section	.text._ZN2v88internal8compiler25SimplifiedOperatorReducerD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler25SimplifiedOperatorReducerD2Ev
	.type	_ZN2v88internal8compiler25SimplifiedOperatorReducerD2Ev, @function
_ZN2v88internal8compiler25SimplifiedOperatorReducerD2Ev:
.LFB20164:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE20164:
	.size	_ZN2v88internal8compiler25SimplifiedOperatorReducerD2Ev, .-_ZN2v88internal8compiler25SimplifiedOperatorReducerD2Ev
	.globl	_ZN2v88internal8compiler25SimplifiedOperatorReducerD1Ev
	.set	_ZN2v88internal8compiler25SimplifiedOperatorReducerD1Ev,_ZN2v88internal8compiler25SimplifiedOperatorReducerD2Ev
	.section	.text._ZN2v88internal8compiler25SimplifiedOperatorReducerD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler25SimplifiedOperatorReducerD0Ev
	.type	_ZN2v88internal8compiler25SimplifiedOperatorReducerD0Ev, @function
_ZN2v88internal8compiler25SimplifiedOperatorReducerD0Ev:
.LFB20166:
	.cfi_startproc
	endbr64
	movl	$32, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE20166:
	.size	_ZN2v88internal8compiler25SimplifiedOperatorReducerD0Ev, .-_ZN2v88internal8compiler25SimplifiedOperatorReducerD0Ev
	.section	.rodata._ZN2v88internal8compiler25SimplifiedOperatorReducer6ReduceEPNS1_4NodeE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"IsHeapObject()"
.LC2:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal8compiler25SimplifiedOperatorReducer6ReduceEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler25SimplifiedOperatorReducer6ReduceEPNS1_4NodeE
	.type	_ZN2v88internal8compiler25SimplifiedOperatorReducer6ReduceEPNS1_4NodeE, @function
_ZN2v88internal8compiler25SimplifiedOperatorReducer6ReduceEPNS1_4NodeE:
.LFB20167:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzwl	16(%rsi), %edx
	cmpw	$228, %dx
	ja	.L6
	cmpw	$62, %dx
	jbe	.L7
	subl	$63, %edx
	cmpw	$165, %dx
	ja	.L7
	leaq	.L9(%rip), %rcx
	movzwl	%dx, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler25SimplifiedOperatorReducer6ReduceEPNS1_4NodeE,"a",@progbits
	.align 4
	.align 4
.L9:
	.long	.L28-.L9
	.long	.L27-.L9
	.long	.L7-.L9
	.long	.L27-.L9
	.long	.L7-.L9
	.long	.L26-.L9
	.long	.L19-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L25-.L9
	.long	.L25-.L9
	.long	.L7-.L9
	.long	.L24-.L9
	.long	.L7-.L9
	.long	.L23-.L9
	.long	.L7-.L9
	.long	.L22-.L9
	.long	.L21-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L20-.L9
	.long	.L19-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L18-.L9
	.long	.L7-.L9
	.long	.L11-.L9
	.long	.L11-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L16-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L15-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L14-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L13-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L12-.L9
	.long	.L7-.L9
	.long	.L11-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L10-.L9
	.long	.L8-.L9
	.section	.text._ZN2v88internal8compiler25SimplifiedOperatorReducer6ReduceEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L168:
	cmpw	$82, %ax
	je	.L190
	cmpw	$237, %ax
	je	.L190
	cmpw	$74, %ax
	je	.L172
	cmpw	$30, %ax
	je	.L190
.L7:
	xorl	%eax, %eax
.L34:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L355
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	cmpw	$274, %dx
	je	.L29
	cmpw	$468, %dx
	jne	.L7
	movzbl	23(%r12), %eax
	movq	32(%r12), %r13
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L356
.L132:
	movq	0(%r13), %rax
	movzwl	16(%rax), %eax
	cmpw	$74, %ax
	je	.L357
	cmpw	$97, %ax
	jne	.L7
	movq	16(%rbx), %rax
	movq	%r13, %rsi
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler5Graph9CloneNodeEPKNS1_4NodeE@PLT
	movq	0(%r13), %rdi
	movq	%rax, %r12
	movq	16(%rbx), %rax
	movq	376(%rax), %r14
	call	_ZN2v88internal8compiler17CheckParametersOfEPKNS1_8OperatorE@PLT
	movq	%rax, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder30CheckedInt32ToCompressedSignedERKNS1_14FeedbackSourceE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	16(%rbx), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler22MachineOperatorBuilder36ChangeCompressedSignedToTaggedSignedEv@PLT
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r12, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%rbx), %rdi
	movq	%r12, %r8
	movq	%r12, %rcx
	movq	%rax, %rdx
	movq	%r13, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r12, %rax
	jmp	.L34
.L11:
	movzbl	23(%r12), %eax
	movq	32(%r12), %rdx
	leaq	32(%r12), %rbx
	andl	$15, %eax
	movq	%rdx, %rdi
	cmpl	$15, %eax
	jne	.L145
	movq	16(%rdx), %rdi
	leaq	16(%rdx), %rbx
.L145:
	movq	(%rdi), %rcx
	cmpw	$235, 16(%rcx)
	jne	.L7
	movzbl	23(%rdi), %ecx
	movq	32(%rdi), %r14
	andl	$15, %ecx
	cmpl	$15, %ecx
	jne	.L146
	movq	16(%r14), %r14
.L146:
	cmpl	$15, %eax
	je	.L310
	movq	%r12, %rdx
.L310:
	cmpq	%rdi, %r14
	je	.L300
	leaq	-24(%rdx), %r13
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	%r14, (%rbx)
	testq	%r14, %r14
	je	.L300
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	jmp	.L300
.L10:
	movzbl	23(%r12), %edx
	movq	32(%r12), %rax
	leaq	32(%r12), %r15
	andl	$15, %edx
	movq	%rax, %r13
	cmpl	$15, %edx
	jne	.L156
	movq	16(%rax), %r13
	leaq	16(%rax), %r15
.L156:
	movq	0(%r13), %rsi
	movzwl	16(%rsi), %ecx
	cmpw	$28, %cx
	jne	.L157
	movsd	48(%rsi), %xmm0
	comisd	.LC6(%rip), %xmm0
	jb	.L7
	movsd	.LC5(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L7
	movabsq	$-9223372036854775808, %rax
	movq	%xmm0, %rdi
	cmpq	%rax, %rdi
	je	.L7
	cvttsd2sil	%xmm0, %eax
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L7
	jne	.L7
	.p2align 4,,10
	.p2align 3
.L303:
	movq	8(%rbx), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r13, %rax
	jmp	.L34
.L12:
	movzbl	23(%r12), %eax
	movq	32(%r12), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L144
	movq	16(%rdx), %rdx
.L144:
	movq	(%rdx), %rax
	cmpw	$30, 16(%rax)
	jne	.L7
	movq	16(%rbx), %rdx
	movq	360(%rdx), %rdx
	addq	$112, %rdx
	cmpq	%rdx, 48(%rax)
	jne	.L7
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	jmp	.L34
.L13:
	movzbl	23(%r12), %eax
	movq	32(%r12), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L31
	movq	16(%rdx), %rdx
.L31:
	movq	(%rdx), %rcx
	movzwl	16(%rcx), %eax
	cmpw	$30, %ax
	jne	.L32
	movq	16(%rbx), %rdi
	movq	48(%rcx), %rdx
	movq	360(%rdi), %rax
	leaq	112(%rax), %rcx
	cmpq	%rcx, %rdx
	je	.L299
	addq	$120, %rax
	cmpq	%rax, %rdx
	jne	.L7
	.p2align 4,,10
	.p2align 3
.L301:
	call	_ZN2v88internal8compiler7JSGraph12TrueConstantEv@PLT
	jmp	.L34
.L14:
	movzbl	23(%r12), %eax
	movq	32(%r12), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L174
	movq	16(%rdx), %rdx
.L174:
	movq	(%rdx), %rax
	cmpw	$28, 16(%rax)
	jne	.L7
	movsd	48(%rax), %xmm0
	movq	16(%rbx), %rdi
	andpd	.LC3(%rip), %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	jmp	.L34
.L15:
	movzbl	23(%r12), %eax
	movq	32(%r12), %rdx
	leaq	32(%r12), %r8
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L175
	movq	(%rdx), %rcx
	movq	%rdx, %r15
	movq	%r8, %r9
	cmpw	$30, 16(%rcx)
	leaq	40(%r12), %rcx
	sete	%dil
.L176:
	movq	(%rcx), %r14
	movq	(%r14), %rcx
	cmpw	$30, 16(%rcx)
	je	.L178
	testb	$1, 18(%rsi)
	je	.L178
	testb	%dil, %dil
	jne	.L358
.L178:
	cmpq	%r15, %r14
	jne	.L7
.L172:
	movq	16(%rbx), %rdi
	jmp	.L301
.L16:
	movzbl	23(%r12), %edx
	movq	32(%r12), %r13
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L143
	movq	16(%r13), %r13
.L143:
	movq	0(%r13), %rdx
	cmpw	$470, 16(%rdx)
	jne	.L7
	movq	8(%rbx), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdx
	movq	%r12, %rsi
	movq	(%rdi), %r9
	call	*32(%r9)
	movq	%r13, %rax
	jmp	.L34
.L8:
	movzbl	23(%r12), %edx
	movq	32(%r12), %r13
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L149
	movq	16(%r13), %r13
.L149:
	movq	0(%r13), %rcx
	movzwl	16(%rcx), %edx
	cmpw	$28, %dx
	jne	.L150
	movsd	48(%rcx), %xmm0
	comisd	.LC6(%rip), %xmm0
	jb	.L302
	movsd	.LC5(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L302
	movabsq	$-9223372036854775808, %rdx
	movq	%xmm0, %rax
	cmpq	%rdx, %rax
	je	.L302
	cvttsd2sil	%xmm0, %edx
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%edx, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L302
	je	.L7
	.p2align 4,,10
	.p2align 3
.L302:
	movq	8(%rbx), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	(%rdi), %r9
	call	*32(%r9)
	movq	%r13, %rax
	jmp	.L34
.L18:
	movzbl	23(%r12), %eax
	movq	32(%r12), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L128
	movq	16(%rdx), %rdx
.L128:
	movq	(%rdx), %rax
	cmpw	$26, 16(%rax)
	jne	.L7
	movsd	48(%rax), %xmm0
	comisd	.LC6(%rip), %xmm0
	jb	.L7
	movsd	.LC5(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L7
	movabsq	$-9223372036854775808, %rax
	movq	%xmm0, %rdi
	cmpq	%rax, %rdi
	je	.L7
	cvttsd2sil	%xmm0, %esi
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%esi, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L7
	jne	.L7
	movq	16(%rbx), %rdi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%rax, %r13
	jmp	.L303
.L20:
	movzbl	23(%r12), %eax
	movq	32(%r12), %rdx
	leaq	32(%r12), %r13
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L109
	movq	16(%rdx), %rdx
.L109:
	movq	(%rdx), %rcx
	movzwl	16(%rcx), %eax
	cmpw	$28, %ax
	je	.L353
	leal	-74(%rax), %ecx
	cmpw	$1, %cx
	jbe	.L305
	cmpw	$77, %ax
	jne	.L119
	.p2align 4,,10
	.p2align 3
.L305:
	movq	32(%rdx), %rax
	movzbl	23(%rdx), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L34
	movq	16(%rax), %rax
	jmp	.L34
.L21:
	movzbl	23(%r12), %eax
	movq	32(%r12), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L36
	movq	16(%rdx), %rdx
.L36:
	movq	(%rdx), %rcx
	movzwl	16(%rcx), %eax
	cmpw	$23, %ax
	je	.L359
	cmpw	$81, %ax
	je	.L305
	jmp	.L7
.L22:
	movzbl	23(%r12), %eax
	movq	32(%r12), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L38
	movq	16(%rdx), %rdx
.L38:
	movq	(%rdx), %rcx
	movzwl	16(%rcx), %eax
	cmpw	$30, %ax
	jne	.L39
	movq	48(%rcx), %rdx
	leaq	-80(%rbp), %r12
	movq	24(%rbx), %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L360
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12BooleanValueEv@PLT
	movq	16(%rbx), %rdi
	movzbl	%al, %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	jmp	.L34
.L19:
	movzbl	23(%r12), %eax
	movq	32(%r12), %rdx
	leaq	32(%r12), %r13
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L48
	movq	16(%rdx), %rdx
.L48:
	movq	(%rdx), %rcx
	movzwl	16(%rcx), %eax
	cmpw	$28, %ax
	jne	.L49
	movsd	48(%rcx), %xmm0
	movq	16(%rbx), %rdi
	call	_ZN2v88internal8compiler12MachineGraph15Float64ConstantEd@PLT
	jmp	.L34
.L26:
	movzbl	23(%r12), %eax
	movq	32(%r12), %rdx
	leaq	32(%r12), %r13
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L87
	movq	16(%rdx), %rdx
.L87:
	movq	(%rdx), %rcx
	movzwl	16(%rcx), %eax
	cmpw	$28, %ax
	je	.L353
	leal	-79(%rax), %ecx
	cmpw	$1, %cx
	ja	.L361
	movzbl	23(%rdx), %eax
	movq	32(%rdx), %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L99
	movq	16(%r14), %r14
.L99:
	movq	16(%rbx), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder21ChangeFloat64ToUint32Ev@PLT
.L348:
	movq	%rax, %r15
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L123
	movq	0(%r13), %rdi
	movq	%r12, %rax
	cmpq	%rdi, %r14
	je	.L125
.L124:
	leaq	-24(%rax), %rbx
	testq	%rdi, %rdi
	je	.L127
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L127:
	movq	%r14, 0(%r13)
	testq	%r14, %r14
	je	.L125
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L125:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
.L300:
	movq	%r12, %rax
	jmp	.L34
.L24:
	movzbl	23(%r12), %eax
	movq	32(%r12), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L106
	movq	16(%rdx), %rdx
.L106:
	movq	(%rdx), %rax
	cmpw	$23, 16(%rax)
	jne	.L7
	movl	44(%rax), %eax
	pxor	%xmm0, %xmm0
	movq	16(%rbx), %rdi
	cvtsi2sdq	%rax, %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	jmp	.L34
.L25:
	movzbl	23(%r12), %eax
	movq	32(%r12), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L45
	movq	16(%rdx), %rdx
.L45:
	movq	(%rdx), %rcx
	movzwl	16(%rcx), %eax
	cmpw	$23, %ax
	jne	.L46
	pxor	%xmm0, %xmm0
	movq	16(%rbx), %rdi
	cvtsi2sdl	44(%rcx), %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	jmp	.L34
.L23:
	movzbl	23(%r12), %eax
	movq	32(%r12), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L42
	movq	16(%rdx), %rdx
.L42:
	movq	(%rdx), %rcx
	movzwl	16(%rcx), %eax
	cmpw	$26, %ax
	jne	.L43
	movsd	48(%rcx), %xmm0
	movq	16(%rbx), %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	jmp	.L34
.L27:
	movzbl	23(%r12), %eax
	movq	32(%r12), %rdx
	leaq	32(%r12), %r13
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L66
	movq	16(%rdx), %rdx
.L66:
	movq	(%rdx), %rcx
	movzwl	16(%rcx), %eax
	cmpw	$28, %ax
	je	.L353
	leal	-79(%rax), %ecx
	cmpw	$1, %cx
	ja	.L76
	movzbl	23(%rdx), %eax
	movq	32(%rdx), %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L77
	movq	16(%r14), %r14
.L77:
	movq	16(%rbx), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder20ChangeFloat64ToInt32Ev@PLT
	jmp	.L348
.L28:
	movzbl	23(%r12), %eax
	movq	32(%r12), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L135
	movq	16(%rdx), %rdx
.L135:
	movq	(%rdx), %rax
	cmpw	$96, 16(%rax)
	jne	.L7
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L356:
	movq	16(%r13), %r13
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L353:
	movsd	48(%rcx), %xmm0
	movsd	.LC4(%rip), %xmm2
	movapd	%xmm0, %xmm1
	andpd	.LC3(%rip), %xmm1
	ucomisd	%xmm1, %xmm2
	jb	.L111
	movsd	.LC5(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L111
	comisd	.LC6(%rip), %xmm0
	jb	.L111
	cvttsd2sil	%xmm0, %esi
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%esi, %xmm1
	ucomisd	%xmm1, %xmm0
	jnp	.L362
	.p2align 4,,10
	.p2align 3
.L111:
	movabsq	$9218868437227405312, %rdx
	movq	%xmm0, %rax
	testq	%rdx, %rax
	je	.L218
	movq	%xmm0, %rdx
	xorl	%esi, %esi
	shrq	$52, %rdx
	andl	$2047, %edx
	movl	%edx, %ecx
	subl	$1075, %ecx
	js	.L363
	cmpl	$31, %ecx
	jg	.L114
	movabsq	$4503599627370495, %rsi
	movabsq	$4503599627370496, %rdx
	andq	%rax, %rsi
	addq	%rdx, %rsi
	salq	%cl, %rsi
	movl	%esi, %esi
.L117:
	sarq	$63, %rax
	orl	$1, %eax
	imull	%eax, %esi
.L114:
	movq	16(%rbx), %rdi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L29:
	movzbl	23(%r12), %eax
	movq	32(%r12), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L167
	movq	16(%rdx), %rdx
.L167:
	movq	(%rdx), %rdx
	movzwl	16(%rdx), %eax
	cmpw	$28, %ax
	jne	.L168
	movsd	48(%rdx), %xmm0
	comisd	.LC6(%rip), %xmm0
	jb	.L190
	movsd	.LC5(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L190
	movabsq	$-9223372036854775808, %rax
	movq	%xmm0, %rdi
	cmpq	%rax, %rdi
	je	.L190
	cvttsd2sil	%xmm0, %eax
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L190
	je	.L172
	.p2align 4,,10
	.p2align 3
.L190:
	movq	16(%rbx), %rdi
.L299:
	call	_ZN2v88internal8compiler7JSGraph13FalseConstantEv@PLT
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L43:
	cmpw	$69, %ax
	jne	.L7
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L39:
	cmpw	$82, %ax
	jne	.L7
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L49:
	leal	-79(%rax), %ecx
	cmpw	$1, %cx
	jbe	.L305
	leal	-74(%rax), %ecx
	cmpw	$1, %cx
	ja	.L364
	movzbl	23(%rdx), %eax
	movq	32(%rdx), %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L54
	movq	16(%r14), %r14
.L54:
	movq	16(%rbx), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder20ChangeInt32ToFloat64Ev@PLT
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L46:
	andl	$-3, %eax
	cmpw	$64, %ax
	je	.L305
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L32:
	cmpw	$205, %ax
	je	.L305
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L157:
	cmpw	$74, %cx
	je	.L303
	cmpw	$227, %cx
	je	.L303
	cmpw	$235, %cx
	jne	.L7
	movzbl	23(%r13), %ecx
	movq	32(%r13), %rbx
	andl	$15, %ecx
	cmpl	$15, %ecx
	jne	.L164
	movq	16(%rbx), %rbx
.L164:
	cmpl	$15, %edx
	je	.L165
	movq	%r12, %rax
	cmpq	%rbx, %r13
	je	.L300
.L166:
	leaq	-24(%rax), %r14
	movq	%r13, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	%rbx, (%r15)
	testq	%rbx, %rbx
	je	.L300
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	jmp	.L300
	.p2align 4,,10
	.p2align 3
.L150:
	cmpw	$237, %dx
	je	.L302
	cmpw	$82, %dx
	je	.L302
	cmpw	$30, %dx
	je	.L302
	cmpw	$228, %dx
	jne	.L7
	jmp	.L302
	.p2align 4,,10
	.p2align 3
.L363:
	cmpl	$-52, %ecx
	jl	.L114
	movabsq	$4503599627370495, %rsi
	movabsq	$4503599627370496, %rcx
	andq	%rax, %rsi
	addq	%rcx, %rsi
	movl	$1075, %ecx
	subl	%edx, %ecx
	shrq	%cl, %rsi
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L123:
	movq	0(%r13), %rax
	movq	16(%rax), %rdi
	leaq	16(%rax), %r13
	cmpq	%rdi, %r14
	jne	.L124
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L175:
	movq	16(%rdx), %r15
	leaq	16(%rdx), %r9
	movq	(%r15), %rcx
	cmpw	$30, 16(%rcx)
	leaq	24(%rdx), %rcx
	sete	%dil
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L357:
	movzbl	23(%r13), %eax
	movq	16(%rbx), %rdx
	movq	32(%r13), %rbx
	andl	$15, %eax
	movq	(%rdx), %r12
	cmpl	$15, %eax
	jne	.L134
	movq	16(%rbx), %rbx
.L134:
	movq	376(%rdx), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder29ChangeInt31ToCompressedSignedEv@PLT
.L306:
	movq	%rax, %rsi
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rbx, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L76:
	leal	-74(%rax), %ecx
	cmpw	$1, %cx
	ja	.L365
.L98:
	movzbl	23(%rdx), %eax
	movq	32(%rdx), %r12
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L300
	movq	16(%r12), %r12
	jmp	.L300
	.p2align 4,,10
	.p2align 3
.L361:
	cmpw	$77, %ax
	jne	.L7
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L359:
	movl	44(%rcx), %eax
	testl	%eax, %eax
	je	.L190
	cmpl	$1, %eax
	jne	.L7
	jmp	.L172
.L119:
	subl	$79, %eax
	cmpw	$1, %ax
	ja	.L7
	movzbl	23(%rdx), %eax
	movq	32(%rdx), %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L122
	movq	16(%r14), %r14
.L122:
	movq	16(%rbx), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder23TruncateFloat64ToWord32Ev@PLT
	jmp	.L348
.L364:
	cmpw	$77, %ax
	jne	.L7
	movzbl	23(%rdx), %eax
	movq	32(%rdx), %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L60
	movq	16(%r14), %r14
.L60:
	movq	16(%rbx), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder21ChangeUint32ToFloat64Ev@PLT
	jmp	.L348
.L365:
	cmpw	$471, %ax
	jne	.L7
	movzbl	23(%rdx), %eax
	movq	16(%rbx), %rcx
	movq	32(%rdx), %rbx
	andl	$15, %eax
	movq	(%rcx), %r12
	cmpl	$15, %eax
	jne	.L86
	movq	16(%rbx), %rbx
.L86:
	movq	376(%rcx), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder29ChangeCompressedSignedToInt32Ev@PLT
	jmp	.L306
.L358:
	cmpl	$15, %eax
	je	.L179
	movq	%r12, %rdx
	cmpq	%r15, %r14
	je	.L366
.L180:
	leaq	-24(%rdx), %r13
	movq	%r15, %rdi
	movq	%r8, -96(%rbp)
	movq	%r13, %rsi
	movq	%r9, -88(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-88(%rbp), %r9
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%r14, (%r9)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movzbl	23(%r12), %eax
	movq	-96(%rbp), %r8
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L367
	movq	8(%r8), %rdi
	cmpq	%r15, %rdi
	je	.L7
.L201:
	leaq	8(%r8), %r13
.L184:
	subq	$48, %r12
	testq	%rdi, %rdi
	je	.L185
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L185:
	movq	%r15, 0(%r13)
	movq	%r15, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movq	%r15, %rax
	movq	%r14, %r15
	movq	%rax, %r14
	jmp	.L178
.L165:
	cmpq	%rbx, %r13
	jne	.L166
	jmp	.L300
.L362:
	je	.L114
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L360:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L179:
	cmpq	%r15, %r14
	jne	.L180
	movq	(%r8), %r12
	movq	24(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%r15, %rdi
	je	.L172
.L203:
	leaq	8(%rax), %r13
	jmp	.L184
.L367:
	movq	(%r8), %r12
	movq	24(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rdi, %r15
	jne	.L203
	movq	%r14, %r15
	movq	%rdi, %r14
	jmp	.L178
.L366:
	movq	8(%r8), %rdi
	cmpq	%r15, %rdi
	jne	.L201
	jmp	.L172
.L355:
	call	__stack_chk_fail@PLT
.L218:
	xorl	%esi, %esi
	jmp	.L114
	.cfi_endproc
.LFE20167:
	.size	_ZN2v88internal8compiler25SimplifiedOperatorReducer6ReduceEPNS1_4NodeE, .-_ZN2v88internal8compiler25SimplifiedOperatorReducer6ReduceEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler25SimplifiedOperatorReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler25SimplifiedOperatorReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerE
	.type	_ZN2v88internal8compiler25SimplifiedOperatorReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerE, @function
_ZN2v88internal8compiler25SimplifiedOperatorReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerE:
.LFB20161:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal8compiler25SimplifiedOperatorReducerE(%rip), %rax
	movq	%rsi, 8(%rdi)
	movq	%rax, (%rdi)
	movq	%rdx, 16(%rdi)
	movq	%rcx, 24(%rdi)
	ret
	.cfi_endproc
.LFE20161:
	.size	_ZN2v88internal8compiler25SimplifiedOperatorReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerE, .-_ZN2v88internal8compiler25SimplifiedOperatorReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerE
	.globl	_ZN2v88internal8compiler25SimplifiedOperatorReducerC1EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerE
	.set	_ZN2v88internal8compiler25SimplifiedOperatorReducerC1EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerE,_ZN2v88internal8compiler25SimplifiedOperatorReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerE
	.section	.text._ZN2v88internal8compiler25SimplifiedOperatorReducer6ChangeEPNS1_4NodeEPKNS1_8OperatorES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler25SimplifiedOperatorReducer6ChangeEPNS1_4NodeEPKNS1_8OperatorES4_
	.type	_ZN2v88internal8compiler25SimplifiedOperatorReducer6ChangeEPNS1_4NodeEPKNS1_8OperatorES4_, @function
_ZN2v88internal8compiler25SimplifiedOperatorReducer6ChangeEPNS1_4NodeEPKNS1_8OperatorES4_:
.LFB20171:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L370
	leaq	32(%rsi), %rbx
	cmpq	%rdi, %rcx
	je	.L372
	leaq	-24(%rsi), %r15
	testq	%rdi, %rdi
	je	.L374
.L383:
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L374:
	movq	%r13, (%rbx)
	testq	%r13, %r13
	je	.L372
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L372:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L370:
	.cfi_restore_state
	movq	16(%rdi), %rax
	leaq	16(%rdi), %rbx
	cmpq	%rax, %rcx
	je	.L372
	movq	%rdi, %rsi
	movq	%rax, %rdi
	leaq	-24(%rsi), %r15
	testq	%rdi, %rdi
	jne	.L383
	jmp	.L374
	.cfi_endproc
.LFE20171:
	.size	_ZN2v88internal8compiler25SimplifiedOperatorReducer6ChangeEPNS1_4NodeEPKNS1_8OperatorES4_, .-_ZN2v88internal8compiler25SimplifiedOperatorReducer6ChangeEPNS1_4NodeEPKNS1_8OperatorES4_
	.section	.text._ZN2v88internal8compiler25SimplifiedOperatorReducer14ReplaceBooleanEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler25SimplifiedOperatorReducer14ReplaceBooleanEb
	.type	_ZN2v88internal8compiler25SimplifiedOperatorReducer14ReplaceBooleanEb, @function
_ZN2v88internal8compiler25SimplifiedOperatorReducer14ReplaceBooleanEb:
.LFB20172:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	16(%rdi), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testb	%sil, %sil
	je	.L385
	call	_ZN2v88internal8compiler7JSGraph12TrueConstantEv@PLT
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L385:
	.cfi_restore_state
	call	_ZN2v88internal8compiler7JSGraph13FalseConstantEv@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20172:
	.size	_ZN2v88internal8compiler25SimplifiedOperatorReducer14ReplaceBooleanEb, .-_ZN2v88internal8compiler25SimplifiedOperatorReducer14ReplaceBooleanEb
	.section	.text._ZN2v88internal8compiler25SimplifiedOperatorReducer14ReplaceFloat64Ed,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler25SimplifiedOperatorReducer14ReplaceFloat64Ed
	.type	_ZN2v88internal8compiler25SimplifiedOperatorReducer14ReplaceFloat64Ed, @function
_ZN2v88internal8compiler25SimplifiedOperatorReducer14ReplaceFloat64Ed:
.LFB20173:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	16(%rdi), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal8compiler12MachineGraph15Float64ConstantEd@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20173:
	.size	_ZN2v88internal8compiler25SimplifiedOperatorReducer14ReplaceFloat64Ed, .-_ZN2v88internal8compiler25SimplifiedOperatorReducer14ReplaceFloat64Ed
	.section	.text._ZN2v88internal8compiler25SimplifiedOperatorReducer12ReplaceInt32Ei,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler25SimplifiedOperatorReducer12ReplaceInt32Ei
	.type	_ZN2v88internal8compiler25SimplifiedOperatorReducer12ReplaceInt32Ei, @function
_ZN2v88internal8compiler25SimplifiedOperatorReducer12ReplaceInt32Ei:
.LFB20174:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	16(%rdi), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20174:
	.size	_ZN2v88internal8compiler25SimplifiedOperatorReducer12ReplaceInt32Ei, .-_ZN2v88internal8compiler25SimplifiedOperatorReducer12ReplaceInt32Ei
	.section	.text._ZN2v88internal8compiler25SimplifiedOperatorReducer13ReplaceNumberEd,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler25SimplifiedOperatorReducer13ReplaceNumberEd
	.type	_ZN2v88internal8compiler25SimplifiedOperatorReducer13ReplaceNumberEd, @function
_ZN2v88internal8compiler25SimplifiedOperatorReducer13ReplaceNumberEd:
.LFB20175:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	16(%rdi), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20175:
	.size	_ZN2v88internal8compiler25SimplifiedOperatorReducer13ReplaceNumberEd, .-_ZN2v88internal8compiler25SimplifiedOperatorReducer13ReplaceNumberEd
	.section	.text._ZN2v88internal8compiler25SimplifiedOperatorReducer13ReplaceNumberEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler25SimplifiedOperatorReducer13ReplaceNumberEi
	.type	_ZN2v88internal8compiler25SimplifiedOperatorReducer13ReplaceNumberEi, @function
_ZN2v88internal8compiler25SimplifiedOperatorReducer13ReplaceNumberEi:
.LFB20176:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	16(%rdi), %rdi
	cvtsi2sdl	%esi, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20176:
	.size	_ZN2v88internal8compiler25SimplifiedOperatorReducer13ReplaceNumberEi, .-_ZN2v88internal8compiler25SimplifiedOperatorReducer13ReplaceNumberEi
	.section	.text._ZNK2v88internal8compiler25SimplifiedOperatorReducer7factoryEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler25SimplifiedOperatorReducer7factoryEv
	.type	_ZNK2v88internal8compiler25SimplifiedOperatorReducer7factoryEv, @function
_ZNK2v88internal8compiler25SimplifiedOperatorReducer7factoryEv:
.LFB20177:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	360(%rax), %rax
	ret
	.cfi_endproc
.LFE20177:
	.size	_ZNK2v88internal8compiler25SimplifiedOperatorReducer7factoryEv, .-_ZNK2v88internal8compiler25SimplifiedOperatorReducer7factoryEv
	.section	.text._ZNK2v88internal8compiler25SimplifiedOperatorReducer5graphEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler25SimplifiedOperatorReducer5graphEv
	.type	_ZNK2v88internal8compiler25SimplifiedOperatorReducer5graphEv, @function
_ZNK2v88internal8compiler25SimplifiedOperatorReducer5graphEv:
.LFB20178:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	(%rax), %rax
	ret
	.cfi_endproc
.LFE20178:
	.size	_ZNK2v88internal8compiler25SimplifiedOperatorReducer5graphEv, .-_ZNK2v88internal8compiler25SimplifiedOperatorReducer5graphEv
	.section	.text._ZNK2v88internal8compiler25SimplifiedOperatorReducer7machineEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler25SimplifiedOperatorReducer7machineEv
	.type	_ZNK2v88internal8compiler25SimplifiedOperatorReducer7machineEv, @function
_ZNK2v88internal8compiler25SimplifiedOperatorReducer7machineEv:
.LFB20179:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	16(%rax), %rax
	ret
	.cfi_endproc
.LFE20179:
	.size	_ZNK2v88internal8compiler25SimplifiedOperatorReducer7machineEv, .-_ZNK2v88internal8compiler25SimplifiedOperatorReducer7machineEv
	.section	.text._ZNK2v88internal8compiler25SimplifiedOperatorReducer10simplifiedEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler25SimplifiedOperatorReducer10simplifiedEv
	.type	_ZNK2v88internal8compiler25SimplifiedOperatorReducer10simplifiedEv, @function
_ZNK2v88internal8compiler25SimplifiedOperatorReducer10simplifiedEv:
.LFB20180:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	376(%rax), %rax
	ret
	.cfi_endproc
.LFE20180:
	.size	_ZNK2v88internal8compiler25SimplifiedOperatorReducer10simplifiedEv, .-_ZNK2v88internal8compiler25SimplifiedOperatorReducer10simplifiedEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler25SimplifiedOperatorReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler25SimplifiedOperatorReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerE, @function
_GLOBAL__sub_I__ZN2v88internal8compiler25SimplifiedOperatorReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerE:
.LFB24077:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE24077:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler25SimplifiedOperatorReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerE, .-_GLOBAL__sub_I__ZN2v88internal8compiler25SimplifiedOperatorReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler25SimplifiedOperatorReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerE
	.weak	_ZTVN2v88internal8compiler25SimplifiedOperatorReducerE
	.section	.data.rel.ro._ZTVN2v88internal8compiler25SimplifiedOperatorReducerE,"awG",@progbits,_ZTVN2v88internal8compiler25SimplifiedOperatorReducerE,comdat
	.align 8
	.type	_ZTVN2v88internal8compiler25SimplifiedOperatorReducerE, @object
	.size	_ZTVN2v88internal8compiler25SimplifiedOperatorReducerE, 56
_ZTVN2v88internal8compiler25SimplifiedOperatorReducerE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal8compiler25SimplifiedOperatorReducerD1Ev
	.quad	_ZN2v88internal8compiler25SimplifiedOperatorReducerD0Ev
	.quad	_ZNK2v88internal8compiler25SimplifiedOperatorReducer12reducer_nameEv
	.quad	_ZN2v88internal8compiler25SimplifiedOperatorReducer6ReduceEPNS1_4NodeE
	.quad	_ZN2v88internal8compiler7Reducer8FinalizeEv
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC3:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC4:
	.long	4294967295
	.long	2146435071
	.align 8
.LC5:
	.long	4290772992
	.long	1105199103
	.align 8
.LC6:
	.long	0
	.long	-1042284544
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
