	.file	"simplified-lowering.cc"
	.text
	.section	.text._ZNKSt5ctypeIcE8do_widenEc,"axG",@progbits,_ZNKSt5ctypeIcE8do_widenEc,comdat
	.align 2
	.p2align 4
	.weak	_ZNKSt5ctypeIcE8do_widenEc
	.type	_ZNKSt5ctypeIcE8do_widenEc, @function
_ZNKSt5ctypeIcE8do_widenEc:
.LFB1527:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	ret
	.cfi_endproc
.LFE1527:
	.size	_ZNKSt5ctypeIcE8do_widenEc, .-_ZNKSt5ctypeIcE8do_widenEc
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD2Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD2Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD2Ev:
.LFB11023:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11023:
	.size	_ZN2v88internal23CallInterfaceDescriptorD2Ev, .-_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.weak	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.set	_ZN2v88internal23CallInterfaceDescriptorD1Ev,_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD0Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD0Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD0Ev:
.LFB11025:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11025:
	.size	_ZN2v88internal23CallInterfaceDescriptorD0Ev, .-_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.section	.text._ZN2v88internal12StdoutStreamD1Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD1Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StdoutStreamD1Ev
	.type	_ZN2v88internal12StdoutStreamD1Ev, @function
_ZN2v88internal12StdoutStreamD1Ev:
.LFB28306:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE28306:
	.size	_ZN2v88internal12StdoutStreamD1Ev, .-_ZN2v88internal12StdoutStreamD1Ev
	.section	.rodata._ZN2v88internal8compiler12_GLOBAL__N_134MachineRepresentationFromArrayTypeENS0_17ExternalArrayTypeE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unimplemented code"
.LC1:
	.string	"unreachable code"
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_134MachineRepresentationFromArrayTypeENS0_17ExternalArrayTypeE,"axG",@progbits,_ZN2v88internal8compiler22RepresentationSelector9VisitNodeEPNS1_4NodeENS1_10TruncationEPNS1_18SimplifiedLoweringE,comdat
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_134MachineRepresentationFromArrayTypeENS0_17ExternalArrayTypeE, @function
_ZN2v88internal8compiler12_GLOBAL__N_134MachineRepresentationFromArrayTypeENS0_17ExternalArrayTypeE:
.LFB22932:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpl	$11, %edi
	ja	.L8
	leaq	.L10(%rip), %rdx
	movl	%edi, %edi
	movslq	(%rdx,%rdi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler12_GLOBAL__N_134MachineRepresentationFromArrayTypeENS0_17ExternalArrayTypeE,"aG",@progbits,_ZN2v88internal8compiler22RepresentationSelector9VisitNodeEPNS1_4NodeENS1_10TruncationEPNS1_18SimplifiedLoweringE,comdat
	.align 4
	.align 4
.L10:
	.long	.L8-.L10
	.long	.L11-.L10
	.long	.L11-.L10
	.long	.L15-.L10
	.long	.L15-.L10
	.long	.L16-.L10
	.long	.L16-.L10
	.long	.L13-.L10
	.long	.L12-.L10
	.long	.L11-.L10
	.long	.L9-.L10
	.long	.L9-.L10
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_134MachineRepresentationFromArrayTypeENS0_17ExternalArrayTypeE,"axG",@progbits,_ZN2v88internal8compiler22RepresentationSelector9VisitNodeEPNS1_4NodeENS1_10TruncationEPNS1_18SimplifiedLoweringE,comdat
	.p2align 4,,10
	.p2align 3
.L11:
	movl	$2, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	.cfi_restore_state
	movl	$3, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	.cfi_restore_state
	movl	$4, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	.cfi_restore_state
	movl	$13, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	.cfi_restore_state
	movl	$12, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L9:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L8:
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE22932:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_134MachineRepresentationFromArrayTypeENS0_17ExternalArrayTypeE, .-_ZN2v88internal8compiler12_GLOBAL__N_134MachineRepresentationFromArrayTypeENS0_17ExternalArrayTypeE
	.section	.text._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB11016:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE11016:
	.size	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_130CheckedUseInfoAsWord32FromHintENS1_19NumberOperationHintERKNS1_14FeedbackSourceENS1_13IdentifyZerosE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_130CheckedUseInfoAsWord32FromHintENS1_19NumberOperationHintERKNS1_14FeedbackSourceENS1_13IdentifyZerosE, @function
_ZN2v88internal8compiler12_GLOBAL__N_130CheckedUseInfoAsWord32FromHintENS1_19NumberOperationHintERKNS1_14FeedbackSourceENS1_13IdentifyZerosE:
.LFB22933:
	.cfi_startproc
	movq	%rdi, %rax
	cmpb	$3, %sil
	je	.L21
	ja	.L22
	cmpb	$2, %sil
	je	.L23
	movq	(%rdx), %rsi
	movl	8(%rdx), %edx
	movb	$4, (%rdi)
	movb	$5, 4(%rdi)
	movl	%ecx, 8(%rdi)
	movb	$1, 12(%rdi)
	movq	%rsi, 16(%rdi)
	movl	%edx, 24(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	cmpb	$4, %sil
	jne	.L33
	movq	(%rdx), %rcx
	movl	8(%rdx), %edx
	movb	$4, (%rdi)
	movb	$2, 4(%rdi)
	movl	$0, 8(%rdi)
	movb	$5, 12(%rdi)
	movq	%rcx, 16(%rdi)
	movl	%edx, 24(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	movq	(%rdx), %rcx
	movl	8(%rdx), %edx
	movb	$4, (%rdi)
	movb	$2, 4(%rdi)
	movl	$0, 8(%rdi)
	movb	$4, 12(%rdi)
	movq	%rcx, 16(%rdi)
	movl	%edx, 24(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	movq	(%rdx), %rsi
	movl	8(%rdx), %edx
	movb	$4, (%rdi)
	movb	$5, 4(%rdi)
	movl	%ecx, 8(%rdi)
	movb	$2, 12(%rdi)
	movq	%rsi, 16(%rdi)
	movl	%edx, 24(%rdi)
	ret
.L33:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE22933:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_130CheckedUseInfoAsWord32FromHintENS1_19NumberOperationHintERKNS1_14FeedbackSourceENS1_13IdentifyZerosE, .-_ZN2v88internal8compiler12_GLOBAL__N_130CheckedUseInfoAsWord32FromHintENS1_19NumberOperationHintERKNS1_14FeedbackSourceENS1_13IdentifyZerosE
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_135TruncatingUseInfoFromRepresentationENS0_21MachineRepresentationE,"axG",@progbits,_ZN2v88internal8compiler22RepresentationSelector9VisitNodeEPNS1_4NodeENS1_10TruncationEPNS1_18SimplifiedLoweringE,comdat
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_135TruncatingUseInfoFromRepresentationENS0_21MachineRepresentationE, @function
_ZN2v88internal8compiler12_GLOBAL__N_135TruncatingUseInfoFromRepresentationENS0_21MachineRepresentationE:
.LFB22935:
	.cfi_startproc
	cmpb	$13, %sil
	ja	.L35
	movzbl	%sil, %ecx
	leaq	.L37(%rip), %rsi
	movq	%rdi, %rax
	movslq	(%rsi,%rcx,4), %rdx
	addq	%rsi, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal8compiler12_GLOBAL__N_135TruncatingUseInfoFromRepresentationENS0_21MachineRepresentationE,"aG",@progbits,_ZN2v88internal8compiler22RepresentationSelector9VisitNodeEPNS1_4NodeENS1_10TruncationEPNS1_18SimplifiedLoweringE,comdat
	.align 4
	.align 4
.L37:
	.long	.L35-.L37
	.long	.L45-.L37
	.long	.L44-.L37
	.long	.L44-.L37
	.long	.L44-.L37
	.long	.L43-.L37
	.long	.L42-.L37
	.long	.L41-.L37
	.long	.L41-.L37
	.long	.L40-.L37
	.long	.L39-.L37
	.long	.L39-.L37
	.long	.L38-.L37
	.long	.L36-.L37
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_135TruncatingUseInfoFromRepresentationENS0_21MachineRepresentationE,"axG",@progbits,_ZN2v88internal8compiler22RepresentationSelector9VisitNodeEPNS1_4NodeENS1_10TruncationEPNS1_18SimplifiedLoweringE,comdat
	.p2align 4,,10
	.p2align 3
.L44:
	movb	$4, (%rdi)
	movb	$2, 4(%rdi)
	movl	$0, 8(%rdi)
	movb	$0, 12(%rdi)
	movq	$0, 16(%rdi)
	movl	$-1, 24(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	movb	$8, (%rdi)
.L49:
	movb	$5, 4(%rax)
	movl	$1, 8(%rax)
	movb	$0, 12(%rax)
	movq	$0, 16(%rax)
	movl	$-1, 24(%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	movb	$11, (%rdi)
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L42:
	movb	$6, (%rdi)
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L40:
	movb	$9, (%rdi)
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L36:
	movb	$13, (%rdi)
	movb	$4, 4(%rdi)
	movl	$1, 8(%rdi)
	movb	$0, 12(%rdi)
	movq	$0, 16(%rdi)
	movl	$-1, 24(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	movb	$12, (%rdi)
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L45:
	movb	$1, (%rdi)
	movb	$1, 4(%rdi)
	movl	$0, 8(%rdi)
	movb	$0, 12(%rdi)
	movq	$0, 16(%rdi)
	movl	$-1, 24(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	movb	$5, (%rdi)
	jmp	.L49
.L35:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE22935:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_135TruncatingUseInfoFromRepresentationENS0_21MachineRepresentationE, .-_ZN2v88internal8compiler12_GLOBAL__N_135TruncatingUseInfoFromRepresentationENS0_21MachineRepresentationE
	.section	.text._ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB11017:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	movq	%rsi, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %edx
	xorl	%esi, %esi
	jmp	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	.cfi_endproc
.LFE11017:
	.size	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal8compiler22RepresentationSelector7InputIsEPNS1_4NodeENS1_4TypeE.isra.0,"axG",@progbits,_ZN2v88internal8compiler22RepresentationSelector9VisitNodeEPNS1_4NodeENS1_10TruncationEPNS1_18SimplifiedLoweringE,comdat
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler22RepresentationSelector7InputIsEPNS1_4NodeENS1_4TypeE.isra.0, @function
_ZN2v88internal8compiler22RepresentationSelector7InputIsEPNS1_4NodeENS1_4TypeE.isra.0:
.LFB28477:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	32(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rdi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L52
	movq	16(%rdx), %rdx
.L52:
	movq	8(%rdx), %rdx
	movl	$1, %eax
	movq	%rdx, -16(%rbp)
	cmpq	%rsi, %rdx
	je	.L51
	leaq	-16(%rbp), %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
.L51:
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L57
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L57:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE28477:
	.size	_ZN2v88internal8compiler22RepresentationSelector7InputIsEPNS1_4NodeENS1_4TypeE.isra.0, .-_ZN2v88internal8compiler22RepresentationSelector7InputIsEPNS1_4NodeENS1_4TypeE.isra.0
	.section	.text._ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.0, @function
_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.0:
.LFB28620:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movzbl	23(%rdi), %eax
	movq	32(%rdi), %r8
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L59
	leaq	32(%rdi), %rbx
	cmpq	%r8, %rsi
	je	.L58
	leaq	-24(%rdi), %r12
	testq	%r8, %r8
	je	.L63
.L71:
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L63:
	movq	%r13, (%rbx)
	testq	%r13, %r13
	je	.L58
	addq	$8, %rsp
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	.p2align 4,,10
	.p2align 3
.L58:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore_state
	movq	16(%r8), %rax
	leaq	16(%r8), %rbx
	cmpq	%rax, %rsi
	je	.L58
	movq	%r8, %rdi
	movq	%rax, %r8
	leaq	-24(%rdi), %r12
	testq	%r8, %r8
	jne	.L71
	jmp	.L63
	.cfi_endproc
.LFE28620:
	.size	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.0, .-_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.0
	.section	.text._ZN2v88internal12StdoutStreamD0Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD0Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.type	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev, @function
_ZTv0_n24_N2v88internal12StdoutStreamD0Ev:
.LFB28618:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %rax
	addq	-24(%rax), %rdi
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	movq	%rax, 80(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE28618:
	.size	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev, .-_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StdoutStreamD0Ev
	.type	_ZN2v88internal12StdoutStreamD0Ev, @function
_ZN2v88internal12StdoutStreamD0Ev:
.LFB28307:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE28307:
	.size	_ZN2v88internal12StdoutStreamD0Ev, .-_ZN2v88internal12StdoutStreamD0Ev
	.section	.text._ZN2v88internal12StdoutStreamD1Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD1Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.type	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev, @function
_ZTv0_n24_N2v88internal12StdoutStreamD1Ev:
.LFB28619:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rax
	movq	-24(%rax), %rbx
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	addq	%rdi, %rbx
	movq	%rax, 80(%rbx)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	64(%rbx), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE28619:
	.size	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev, .-_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.section	.text._ZN2v88internal8compiler22RepresentationSelector15PrintOutputInfoEPNS2_8NodeInfoE.isra.0.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler22RepresentationSelector15PrintOutputInfoEPNS2_8NodeInfoE.isra.0.part.0, @function
_ZN2v88internal8compiler22RepresentationSelector15PrintOutputInfoEPNS2_8NodeInfoE.isra.0.part.0:
.LFB28610:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-384(%rbp), %r14
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-304(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$352, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNSt8ios_baseC2Ev@PLT
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	stdout(%rip), %rdx
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movups	%xmm0, -72(%rbp)
	movups	%xmm0, -56(%rbp)
	movw	%ax, -80(%rbp)
	movq	%r13, -304(%rbp)
	movq	$0, -88(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	movzbl	1(%rbx), %esi
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -384(%rbp)
	addq	$40, %rax
	movq	%rax, -304(%rbp)
	call	_ZN2v88internallsERSoNS0_21MachineRepresentationE@PLT
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rcx
	movq	%rax, -304(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-320(%rbp), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%r12, %rdi
	movq	%r13, -304(%rbp)
	movq	%rax, -384(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L81
	addq	$352, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L81:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE28610:
	.size	_ZN2v88internal8compiler22RepresentationSelector15PrintOutputInfoEPNS2_8NodeInfoE.isra.0.part.0, .-_ZN2v88internal8compiler22RepresentationSelector15PrintOutputInfoEPNS2_8NodeInfoE.isra.0.part.0
	.section	.text._ZN2v88internal8compiler22RepresentationSelector22LowerToCheckedInt32MulEPNS1_4NodeENS1_10TruncationENS1_4TypeES6_.isra.0,"axG",@progbits,_ZN2v88internal8compiler22RepresentationSelector9VisitNodeEPNS1_4NodeENS1_10TruncationEPNS1_18SimplifiedLoweringE,comdat
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler22RepresentationSelector22LowerToCheckedInt32MulEPNS1_4NodeENS1_10TruncationENS1_4TypeES6_.isra.0, @function
_ZN2v88internal8compiler22RepresentationSelector22LowerToCheckedInt32MulEPNS1_4NodeENS1_10TruncationENS1_4TypeES6_.isra.0:
.LFB28461:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	movl	$1, %esi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	jne	.L102
.L83:
	movq	(%rbx), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder15CheckedInt32MulENS1_21CheckForMinusZeroModeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L103
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L102:
	.cfi_restore_state
	movq	%rcx, -48(%rbp)
	movq	%r8, %r13
	leaq	-48(%rbp), %r14
	cmpq	$3167, %rcx
	jne	.L84
.L90:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	comisd	.LC2(%rip), %xmm0
	movl	$1, %esi
	ja	.L83
.L86:
	movq	%r13, -48(%rbp)
	cmpq	$3167, %r13
	jne	.L104
.L88:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	comisd	.LC2(%rip), %xmm0
	movl	$1, %esi
	ja	.L83
.L92:
	xorl	%esi, %esi
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L84:
	movl	$3167, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L86
	cmpq	$1, -48(%rbp)
	jne	.L90
	movq	%r13, -48(%rbp)
	cmpq	$3167, %r13
	je	.L88
.L104:
	movl	$3167, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L92
	cmpq	$1, -48(%rbp)
	jne	.L88
	xorl	%esi, %esi
	jmp	.L83
.L103:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE28461:
	.size	_ZN2v88internal8compiler22RepresentationSelector22LowerToCheckedInt32MulEPNS1_4NodeENS1_10TruncationENS1_4TypeES6_.isra.0, .-_ZN2v88internal8compiler22RepresentationSelector22LowerToCheckedInt32MulEPNS1_4NodeENS1_10TruncationENS1_4TypeES6_.isra.0
	.section	.text._ZN2v88internal8compiler22RepresentationSelector15PrintTruncationENS1_10TruncationE.isra.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler22RepresentationSelector15PrintTruncationENS1_10TruncationE.isra.0, @function
_ZN2v88internal8compiler22RepresentationSelector15PrintTruncationENS1_10TruncationE.isra.0:
.LFB28611:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$368, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, -392(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	jne	.L115
.L105:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L116
	addq	$368, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L115:
	.cfi_restore_state
	leaq	-304(%rbp), %r14
	leaq	-384(%rbp), %r13
	movq	%r14, %rdi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rbx
	call	_ZNSt8ios_baseC2Ev@PLT
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	movq	%r13, %rdi
	movq	stdout(%rip), %rdx
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movw	%ax, -80(%rbp)
	movups	%xmm0, -72(%rbp)
	movups	%xmm0, -56(%rbp)
	movq	%rbx, -304(%rbp)
	movq	$0, -88(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	leaq	-392(%rbp), %rdi
	movq	%rax, -384(%rbp)
	addq	$40, %rax
	movq	%rax, -304(%rbp)
	call	_ZNK2v88internal8compiler10Truncation11descriptionEv@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L117
	movq	%rax, %rdi
	call	strlen@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L108:
	movq	-384(%rbp), %rax
	movq	-24(%rax), %rax
	movq	-144(%rbp,%rax), %r12
	testq	%r12, %r12
	je	.L118
	cmpb	$0, 56(%r12)
	je	.L110
	movsbl	67(%r12), %esi
.L111:
	movq	%r13, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rcx
	movq	%rax, -304(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-320(%rbp), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%r14, %rdi
	movq	%rbx, -304(%rbp)
	movq	%rax, -384(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L110:
	movq	%r12, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%r12), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L111
	movq	%r12, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L117:
	movq	-384(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r13, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L108
.L116:
	call	__stack_chk_fail@PLT
.L118:
	call	_ZSt16__throw_bad_castv@PLT
	.cfi_endproc
.LFE28611:
	.size	_ZN2v88internal8compiler22RepresentationSelector15PrintTruncationENS1_10TruncationE.isra.0, .-_ZN2v88internal8compiler22RepresentationSelector15PrintTruncationENS1_10TruncationE.isra.0
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_124ReplaceEffectControlUsesEPNS1_4NodeES4_S4_.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_124ReplaceEffectControlUsesEPNS1_4NodeES4_S4_.isra.0, @function
_ZN2v88internal8compiler12_GLOBAL__N_124ReplaceEffectControlUsesEPNS1_4NodeES4_S4_.isra.0:
.LFB28482:
	.cfi_startproc
	testq	%rdi, %rdi
	je	.L152
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rbx
	.p2align 4,,10
	.p2align 3
.L129:
	movl	16(%r14), %ecx
	movq	%r14, %rdi
	movl	%ecx, %edx
	shrl	%edx
	leaq	3(%rdx,%rdx,2), %rax
	leaq	(%r14,%rax,8), %rax
	leaq	32(%rax), %rsi
	addq	$16, %rax
	andl	$1, %ecx
	cmovne	%rsi, %rax
	leaq	(%rax,%rdx,8), %r15
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler14NodeProperties13IsControlEdgeENS1_4EdgeE@PLT
	testb	%al, %al
	je	.L123
	movq	(%r15), %rdi
	cmpq	%rdi, %r12
	je	.L125
	testq	%rdi, %rdi
	je	.L126
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L126:
	movq	%r12, (%r15)
	testq	%r12, %r12
	je	.L125
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	.p2align 4,,10
	.p2align 3
.L125:
	testq	%rbx, %rbx
	je	.L120
.L155:
	movq	%rbx, %r14
	movq	(%rbx), %rbx
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L123:
	movq	%r14, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler14NodeProperties12IsEffectEdgeENS1_4EdgeE@PLT
	testb	%al, %al
	je	.L125
	movq	(%r15), %rdi
	cmpq	%rdi, %r13
	je	.L125
	testq	%rdi, %rdi
	je	.L128
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L128:
	movq	%r13, (%r15)
	testq	%r13, %r13
	je	.L125
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	testq	%rbx, %rbx
	jne	.L155
.L120:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L152:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.cfi_endproc
.LFE28482:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_124ReplaceEffectControlUsesEPNS1_4NodeES4_S4_.isra.0, .-_ZN2v88internal8compiler12_GLOBAL__N_124ReplaceEffectControlUsesEPNS1_4NodeES4_S4_.isra.0
	.section	.text._ZN2v88internal8compiler4Node12ReplaceInputEiPS2_,"axG",@progbits,_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_
	.type	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_, @function
_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_:
.LFB10633:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rax
	salq	$3, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movzbl	23(%rdi), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L157
	leaq	32(%rdi), %rbx
	addq	%rax, %rbx
	movq	(%rbx), %r8
	cmpq	%r8, %r12
	je	.L156
.L158:
	notl	%esi
	movslq	%esi, %rsi
	leaq	(%rsi,%rsi,2), %rax
	leaq	(%rdi,%rax,8), %r13
	testq	%r8, %r8
	je	.L161
	movq	%r13, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L161:
	movq	%r12, (%rbx)
	testq	%r12, %r12
	je	.L156
	addq	$8, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	.p2align 4,,10
	.p2align 3
.L157:
	.cfi_restore_state
	movq	32(%rdi), %rdi
	leaq	16(%rdi,%rax), %rbx
	movq	(%rbx), %r8
	cmpq	%r8, %r12
	jne	.L158
.L156:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10633:
	.size	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_, .-_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_
	.section	.rodata._ZN2v88internal8compiler22RepresentationSelector21PrintNodeFeedbackTypeEPNS1_4NodeE.str1.1,"aMS",@progbits,1
.LC3:
	.string	"#"
.LC4:
	.string	":"
.LC5:
	.string	"("
.LC6:
	.string	")"
.LC7:
	.string	", "
.LC8:
	.string	"  [Static type: "
.LC9:
	.string	", Feedback type: "
.LC10:
	.string	"]"
	.section	.text._ZN2v88internal8compiler22RepresentationSelector21PrintNodeFeedbackTypeEPNS1_4NodeE,"axG",@progbits,_ZN2v88internal8compiler22RepresentationSelector21PrintNodeFeedbackTypeEPNS1_4NodeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler22RepresentationSelector21PrintNodeFeedbackTypeEPNS1_4NodeE
	.type	_ZN2v88internal8compiler22RepresentationSelector21PrintNodeFeedbackTypeEPNS1_4NodeE, @function
_ZN2v88internal8compiler22RepresentationSelector21PrintNodeFeedbackTypeEPNS1_4NodeE:
.LFB22992:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-400(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	addq	$32, %rbx
	subq	$392, %rsp
	movq	%rdi, -432(%rbp)
	movq	%rsi, -416(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-320(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -424(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	stdout(%rip), %rdx
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movq	%rax, -320(%rbp)
	movq	%r12, %rdi
	xorl	%eax, %eax
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movw	%ax, -96(%rbp)
	movq	$0, -104(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, -400(%rbp)
	leaq	.LC3(%rip), %rsi
	addq	$40, %rax
	movq	%rax, -320(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	-12(%rbx), %esi
	movq	%r12, %rdi
	andl	$16777215, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC4(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-32(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_8OperatorE@PLT
	movl	$1, %edx
	leaq	.LC5(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movzbl	-9(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L171
	movq	-416(%rbp), %rax
	movq	32(%rax), %rbx
	movl	8(%rbx), %eax
	addq	$16, %rbx
.L171:
	cltq
	leaq	(%rbx,%rax,8), %rax
	movq	%rax, -408(%rbp)
	cmpq	%rax, %rbx
	je	.L178
	movq	(%rbx), %r13
	leaq	.LC3(%rip), %r15
	addq	$8, %rbx
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L175:
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpq	%rbx, -408(%rbp)
	je	.L178
.L177:
	movl	$2, %edx
	movq	%r12, %rdi
	movq	(%rbx), %r13
	addq	$8, %rbx
	leaq	.LC7(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L183:
	movl	$1, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	20(%r13), %esi
	movq	%r12, %rdi
	andl	$16777215, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC4(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r14
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	0(%r13), %rax
	movq	8(%rax), %r13
	testq	%r13, %r13
	jne	.L175
	movq	(%r14), %rax
	movq	-24(%rax), %rdi
	addq	%r14, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	cmpq	%rbx, -408(%rbp)
	jne	.L177
.L178:
	movl	$1, %edx
	leaq	.LC6(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-416(%rbp), %rax
	movq	8(%rax), %rbx
	testq	%rbx, %rbx
	jne	.L194
.L174:
	movq	-400(%rbp), %rax
	movq	-24(%rax), %rax
	movq	-160(%rbp,%rax), %r13
	testq	%r13, %r13
	je	.L195
	cmpb	$0, 56(%r13)
	je	.L181
	movsbl	67(%r13), %esi
.L182:
	movq	%r12, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rcx
	movq	%rax, -320(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-336(%rbp), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -400(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	-424(%rbp), %rdi
	movq	%rax, -400(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L196
	addq	$392, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L181:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	0(%r13), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L182
	movq	%r13, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L194:
	movl	$16, %edx
	leaq	.LC8(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compilerlsERSoNS1_4TypeE@PLT
	movq	-416(%rbp), %rax
	movq	-432(%rbp), %rcx
	movl	20(%rax), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rax
	salq	$3, %rax
	addq	32(%rcx), %rax
	movq	24(%rax), %r13
	cmpq	%r13, %rbx
	je	.L179
	testq	%r13, %r13
	jne	.L197
.L179:
	movl	$1, %edx
	leaq	.LC10(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L174
.L197:
	movq	%r12, %rdi
	movl	$17, %edx
	leaq	.LC9(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compilerlsERSoNS1_4TypeE@PLT
	jmp	.L179
.L196:
	call	__stack_chk_fail@PLT
.L195:
	call	_ZSt16__throw_bad_castv@PLT
	.cfi_endproc
.LFE22992:
	.size	_ZN2v88internal8compiler22RepresentationSelector21PrintNodeFeedbackTypeEPNS1_4NodeE, .-_ZN2v88internal8compiler22RepresentationSelector21PrintNodeFeedbackTypeEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler22RepresentationSelector18UpdateFeedbackTypeEPNS1_4NodeE,"axG",@progbits,_ZN2v88internal8compiler22RepresentationSelector18UpdateFeedbackTypeEPNS1_4NodeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler22RepresentationSelector18UpdateFeedbackTypeEPNS1_4NodeE
	.type	_ZN2v88internal8compiler22RepresentationSelector18UpdateFeedbackTypeEPNS1_4NodeE, @function
_ZN2v88internal8compiler22RepresentationSelector18UpdateFeedbackTypeEPNS1_4NodeE:
.LFB22991:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rsi), %r10
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	32(%r10), %ecx
	testl	%ecx, %ecx
	je	.L203
	movq	%rsi, %r12
	movl	20(%rsi), %esi
	movq	32(%rdi), %rcx
	movq	%rdi, %r15
	movzwl	16(%r10), %edi
	movl	%esi, %eax
	movl	%esi, %r9d
	andl	$16777215, %eax
	shrl	$24, %r9d
	leaq	(%rax,%rax,4), %rax
	andl	$15, %r9d
	leaq	(%rcx,%rax,8), %r11
	movq	24(%r11), %r14
	movq	%r14, -88(%rbp)
	cmpw	$35, %di
	je	.L201
	movl	20(%r10), %eax
	testl	%eax, %eax
	jle	.L201
	leaq	32(%r12), %rdx
	cmpl	$15, %r9d
	je	.L333
	subl	$1, %eax
	leaq	40(%r12,%rax,8), %rbx
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L335:
	addq	$8, %rdx
	cmpq	%rdx, %rbx
	je	.L334
.L206:
	movq	(%rdx), %rax
	movl	20(%rax), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rax
	cmpq	$0, 24(%rcx,%rax,8)
	jne	.L335
	.p2align 4,,10
	.p2align 3
.L203:
	xorl	%eax, %eax
.L198:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L336
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L201:
	.cfi_restore_state
	xorl	%r13d, %r13d
	cmpl	$15, %r9d
	je	.L208
	testl	%r9d, %r9d
	je	.L210
.L209:
	movq	32(%r12), %rax
	movl	$1, %r13d
	movl	20(%rax), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rax
	leaq	(%rcx,%rax,8), %rax
	cmpq	$0, 24(%rax)
	cmovne	24(%rax), %r13
	xorl	%edx, %edx
	cmpl	$1, %r9d
	je	.L213
	leaq	40(%r12), %rax
.L216:
	movq	(%rax), %rax
	movl	$1, %edx
	movl	20(%rax), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rax
	leaq	(%rcx,%rax,8), %rax
	cmpq	$0, 24(%rax)
	cmovne	24(%rax), %rdx
.L213:
	subl	$34, %edi
	cmpw	$201, %di
	ja	.L217
.L337:
	leaq	.L219(%rip), %rbx
	movzwl	%di, %edi
	movslq	(%rbx,%rdi,4), %rax
	addq	%rbx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler22RepresentationSelector18UpdateFeedbackTypeEPNS1_4NodeE,"aG",@progbits,_ZN2v88internal8compiler22RepresentationSelector18UpdateFeedbackTypeEPNS1_4NodeE,comdat
	.align 4
	.align 4
.L219:
	.long	.L293-.L219
	.long	.L292-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L291-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L290-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L289-.L219
	.long	.L288-.L219
	.long	.L287-.L219
	.long	.L286-.L219
	.long	.L285-.L219
	.long	.L284-.L219
	.long	.L283-.L219
	.long	.L282-.L219
	.long	.L281-.L219
	.long	.L280-.L219
	.long	.L279-.L219
	.long	.L278-.L219
	.long	.L277-.L219
	.long	.L276-.L219
	.long	.L275-.L219
	.long	.L274-.L219
	.long	.L217-.L219
	.long	.L273-.L219
	.long	.L272-.L219
	.long	.L271-.L219
	.long	.L270-.L219
	.long	.L269-.L219
	.long	.L268-.L219
	.long	.L267-.L219
	.long	.L266-.L219
	.long	.L265-.L219
	.long	.L264-.L219
	.long	.L263-.L219
	.long	.L262-.L219
	.long	.L261-.L219
	.long	.L260-.L219
	.long	.L259-.L219
	.long	.L258-.L219
	.long	.L257-.L219
	.long	.L256-.L219
	.long	.L255-.L219
	.long	.L254-.L219
	.long	.L253-.L219
	.long	.L252-.L219
	.long	.L251-.L219
	.long	.L250-.L219
	.long	.L249-.L219
	.long	.L248-.L219
	.long	.L247-.L219
	.long	.L246-.L219
	.long	.L245-.L219
	.long	.L244-.L219
	.long	.L243-.L219
	.long	.L242-.L219
	.long	.L241-.L219
	.long	.L240-.L219
	.long	.L239-.L219
	.long	.L238-.L219
	.long	.L237-.L219
	.long	.L236-.L219
	.long	.L235-.L219
	.long	.L234-.L219
	.long	.L233-.L219
	.long	.L232-.L219
	.long	.L231-.L219
	.long	.L230-.L219
	.long	.L229-.L219
	.long	.L228-.L219
	.long	.L227-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L226-.L219
	.long	.L217-.L219
	.long	.L225-.L219
	.long	.L224-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L223-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L222-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L221-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L217-.L219
	.long	.L220-.L219
	.long	.L218-.L219
	.section	.text._ZN2v88internal8compiler22RepresentationSelector18UpdateFeedbackTypeEPNS1_4NodeE,"axG",@progbits,_ZN2v88internal8compiler22RepresentationSelector18UpdateFeedbackTypeEPNS1_4NodeE,comdat
	.p2align 4,,10
	.p2align 3
.L210:
	subl	$34, %edi
	xorl	%edx, %edx
	cmpw	$201, %di
	jbe	.L337
.L217:
	testq	%r14, %r14
	jne	.L203
	movq	8(%r12), %rax
	movq	%rax, 24(%r11)
	movl	$1, %eax
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L338:
	xorl	%r13d, %r13d
.L208:
	movq	32(%r12), %rax
	movl	8(%rax), %ebx
	testl	%ebx, %ebx
	jle	.L210
	movq	16(%rax), %rdx
	movl	$1, %r13d
	movl	20(%rdx), %edx
	andl	$16777215, %edx
	leaq	(%rdx,%rdx,4), %rdx
	leaq	(%rcx,%rdx,8), %rdx
	cmpq	$0, 24(%rdx)
	cmovne	24(%rdx), %r13
	xorl	%edx, %edx
	cmpl	$1, %ebx
	je	.L213
	addq	$24, %rax
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L333:
	movq	32(%r12), %rbx
	subl	$1, %eax
	leaq	16(%rbx), %rdx
	leaq	24(%rbx,%rax,8), %rbx
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L339:
	addq	$8, %rdx
	cmpq	%rdx, %rbx
	je	.L338
.L204:
	movq	(%rdx), %rax
	movl	20(%rax), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rax
	cmpq	$0, 24(%rcx,%rax,8)
	jne	.L339
	jmp	.L203
	.p2align 4,,10
	.p2align 3
.L334:
	xorl	%r13d, %r13d
	testl	%r9d, %r9d
	jne	.L209
	jmp	.L210
.L218:
	movq	32(%r12), %rax
	leaq	352(%r15), %rdi
	cmpl	$15, %r9d
	jne	.L308
	movq	16(%rax), %rax
.L308:
	movl	20(%rax), %eax
	movl	$1, %esi
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rax
	leaq	(%rcx,%rax,8), %rax
	cmpq	$0, 24(%rax)
	cmovne	24(%rax), %rsi
	call	_ZN2v88internal8compiler14OperationTyper28ConvertTaggedHoleToUndefinedENS1_4TypeE@PLT
	movq	%rax, -88(%rbp)
	movq	%rax, %rsi
	.p2align 4,,10
	.p2align 3
.L294:
	movq	(%r15), %rax
	movq	8(%r12), %rdi
	movq	(%rax), %rax
	movq	(%rax), %rdx
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -88(%rbp)
	testq	%r14, %r14
	je	.L312
.L307:
	cmpq	-88(%rbp), %r14
	je	.L203
	leaq	-88(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L203
	.p2align 4,,10
	.p2align 3
.L312:
	movl	20(%r12), %eax
	movq	32(%r15), %rdx
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rax
	leaq	(%rdx,%rax,8), %rax
	movq	-88(%rbp), %rdx
	movq	%rdx, 24(%rax)
	movzbl	_ZN2v88internal25FLAG_trace_representationE(%rip), %eax
	testb	%al, %al
	jne	.L340
	movl	$1, %eax
	jmp	.L198
.L220:
	movq	%r13, %rsi
	leaq	352(%r15), %rdi
	call	_ZN2v88internal8compiler14OperationTyper15ConvertReceiverENS1_4TypeE@PLT
	movq	%rax, -88(%rbp)
	movq	%rax, %rsi
	jmp	.L294
.L221:
	movq	(%r15), %rax
	movq	16(%r11), %r8
	leaq	352(%r15), %rdi
	movq	%r13, %rsi
	movq	(%rax), %rax
	movq	%r8, -104(%rbp)
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler14OperationTyper16CheckFloat64HoleENS1_4TypeE@PLT
	.p2align 4,,10
	.p2align 3
.L332:
	movq	-104(%rbp), %r8
	movq	%rax, %rdi
	movq	%rbx, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -88(%rbp)
	movq	%rax, %rsi
	jmp	.L294
.L222:
	movq	(%r15), %rax
	movq	16(%r11), %r8
	leaq	352(%r15), %rdi
	movq	%r13, %rsi
	movq	(%rax), %rax
	movq	%r8, -104(%rbp)
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler14OperationTyper11CheckNumberENS1_4TypeE@PLT
	jmp	.L332
.L223:
	movq	(%r15), %rax
	movq	16(%r11), %r8
	leaq	352(%r15), %rdi
	movq	%r13, %rsi
	movq	(%rax), %rax
	movq	%r8, -104(%rbp)
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler14OperationTyper11CheckBoundsENS1_4TypeES3_@PLT
	jmp	.L332
.L224:
	movq	%r13, %rsi
	leaq	352(%r15), %rdi
	call	_ZN2v88internal8compiler14OperationTyper8ToNumberENS1_4TypeE@PLT
	movq	%rax, -88(%rbp)
	movq	%rax, %rsi
	jmp	.L294
.L225:
	movq	(%r15), %rax
	movq	16(%r11), %r8
	leaq	352(%r15), %rdi
	movq	%r13, %rsi
	movq	(%rax), %rax
	movq	%r8, -104(%rbp)
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler14OperationTyper20SpeculativeBigIntAddENS1_4TypeES3_@PLT
	jmp	.L332
.L226:
	movq	(%r15), %rax
	movq	16(%r11), %r8
	leaq	352(%r15), %rdi
	movq	%r13, %rsi
	movq	(%rax), %rax
	movq	%r8, -104(%rbp)
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler14OperationTyper19SpeculativeToNumberENS1_4TypeE@PLT
	jmp	.L332
.L227:
	movq	%r13, %rsi
	leaq	352(%r15), %rdi
	call	_ZN2v88internal8compiler14OperationTyper16NumberSilenceNaNENS1_4TypeE@PLT
	movq	%rax, -88(%rbp)
	movq	%rax, %rsi
	jmp	.L294
.L228:
	movq	%r13, %rsi
	leaq	352(%r15), %rdi
	call	_ZN2v88internal8compiler14OperationTyper20NumberToUint8ClampedENS1_4TypeE@PLT
	movq	%rax, -88(%rbp)
	movq	%rax, %rsi
	jmp	.L294
.L229:
	movq	%r13, %rsi
	leaq	352(%r15), %rdi
	call	_ZN2v88internal8compiler14OperationTyper14NumberToUint32ENS1_4TypeE@PLT
	movq	%rax, -88(%rbp)
	movq	%rax, %rsi
	jmp	.L294
.L230:
	movq	%r13, %rsi
	leaq	352(%r15), %rdi
	call	_ZN2v88internal8compiler14OperationTyper14NumberToStringENS1_4TypeE@PLT
	movq	%rax, -88(%rbp)
	movq	%rax, %rsi
	jmp	.L294
.L231:
	movq	%r13, %rsi
	leaq	352(%r15), %rdi
	call	_ZN2v88internal8compiler14OperationTyper13NumberToInt32ENS1_4TypeE@PLT
	movq	%rax, -88(%rbp)
	movq	%rax, %rsi
	jmp	.L294
.L232:
	movq	%r13, %rsi
	leaq	352(%r15), %rdi
	call	_ZN2v88internal8compiler14OperationTyper15NumberToBooleanENS1_4TypeE@PLT
	movq	%rax, -88(%rbp)
	movq	%rax, %rsi
	jmp	.L294
.L233:
	movq	%r13, %rsi
	leaq	352(%r15), %rdi
	call	_ZN2v88internal8compiler14OperationTyper11NumberTruncENS1_4TypeE@PLT
	movq	%rax, -88(%rbp)
	movq	%rax, %rsi
	jmp	.L294
.L234:
	movq	%r13, %rsi
	leaq	352(%r15), %rdi
	call	_ZN2v88internal8compiler14OperationTyper10NumberTanhENS1_4TypeE@PLT
	movq	%rax, -88(%rbp)
	movq	%rax, %rsi
	jmp	.L294
.L235:
	movq	%r13, %rsi
	leaq	352(%r15), %rdi
	call	_ZN2v88internal8compiler14OperationTyper9NumberTanENS1_4TypeE@PLT
	movq	%rax, -88(%rbp)
	movq	%rax, %rsi
	jmp	.L294
.L236:
	movq	%r13, %rsi
	leaq	352(%r15), %rdi
	call	_ZN2v88internal8compiler14OperationTyper10NumberSqrtENS1_4TypeE@PLT
	movq	%rax, -88(%rbp)
	movq	%rax, %rsi
	jmp	.L294
.L237:
	movq	%r13, %rsi
	leaq	352(%r15), %rdi
	call	_ZN2v88internal8compiler14OperationTyper10NumberSinhENS1_4TypeE@PLT
	movq	%rax, -88(%rbp)
	movq	%rax, %rsi
	jmp	.L294
.L238:
	movq	%r13, %rsi
	leaq	352(%r15), %rdi
	call	_ZN2v88internal8compiler14OperationTyper9NumberSinENS1_4TypeE@PLT
	movq	%rax, -88(%rbp)
	movq	%rax, %rsi
	jmp	.L294
.L239:
	movq	%r13, %rsi
	leaq	352(%r15), %rdi
	call	_ZN2v88internal8compiler14OperationTyper10NumberSignENS1_4TypeE@PLT
	movq	%rax, -88(%rbp)
	movq	%rax, %rsi
	jmp	.L294
.L240:
	movq	%r13, %rsi
	leaq	352(%r15), %rdi
	call	_ZN2v88internal8compiler14OperationTyper11NumberRoundENS1_4TypeE@PLT
	movq	%rax, -88(%rbp)
	movq	%rax, %rsi
	jmp	.L294
.L241:
	movq	%r13, %rsi
	leaq	352(%r15), %rdi
	call	_ZN2v88internal8compiler14OperationTyper11NumberLog10ENS1_4TypeE@PLT
	movq	%rax, -88(%rbp)
	movq	%rax, %rsi
	jmp	.L294
.L242:
	movq	%r13, %rsi
	leaq	352(%r15), %rdi
	call	_ZN2v88internal8compiler14OperationTyper10NumberLog2ENS1_4TypeE@PLT
	movq	%rax, -88(%rbp)
	movq	%rax, %rsi
	jmp	.L294
.L243:
	movq	%r13, %rsi
	leaq	352(%r15), %rdi
	call	_ZN2v88internal8compiler14OperationTyper11NumberLog1pENS1_4TypeE@PLT
	movq	%rax, -88(%rbp)
	movq	%rax, %rsi
	jmp	.L294
.L244:
	movq	%r13, %rsi
	leaq	352(%r15), %rdi
	call	_ZN2v88internal8compiler14OperationTyper9NumberLogENS1_4TypeE@PLT
	movq	%rax, -88(%rbp)
	movq	%rax, %rsi
	jmp	.L294
.L245:
	movq	%r13, %rsi
	leaq	352(%r15), %rdi
	call	_ZN2v88internal8compiler14OperationTyper12NumberFroundENS1_4TypeE@PLT
	movq	%rax, -88(%rbp)
	movq	%rax, %rsi
	jmp	.L294
.L246:
	movq	%r13, %rsi
	leaq	352(%r15), %rdi
	call	_ZN2v88internal8compiler14OperationTyper11NumberFloorENS1_4TypeE@PLT
	movq	%rax, -88(%rbp)
	movq	%rax, %rsi
	jmp	.L294
.L247:
	movq	%r13, %rsi
	leaq	352(%r15), %rdi
	call	_ZN2v88internal8compiler14OperationTyper11NumberExpm1ENS1_4TypeE@PLT
	movq	%rax, -88(%rbp)
	movq	%rax, %rsi
	jmp	.L294
.L248:
	movq	%r13, %rsi
	leaq	352(%r15), %rdi
	call	_ZN2v88internal8compiler14OperationTyper9NumberExpENS1_4TypeE@PLT
	movq	%rax, -88(%rbp)
	movq	%rax, %rsi
	jmp	.L294
.L249:
	movq	%r13, %rsi
	leaq	352(%r15), %rdi
	call	_ZN2v88internal8compiler14OperationTyper10NumberCoshENS1_4TypeE@PLT
	movq	%rax, -88(%rbp)
	movq	%rax, %rsi
	jmp	.L294
.L250:
	movq	%r13, %rsi
	leaq	352(%r15), %rdi
	call	_ZN2v88internal8compiler14OperationTyper9NumberCosENS1_4TypeE@PLT
	movq	%rax, -88(%rbp)
	movq	%rax, %rsi
	jmp	.L294
.L251:
	movq	%r13, %rsi
	leaq	352(%r15), %rdi
	call	_ZN2v88internal8compiler14OperationTyper11NumberClz32ENS1_4TypeE@PLT
	movq	%rax, -88(%rbp)
	movq	%rax, %rsi
	jmp	.L294
.L252:
	movq	%r13, %rsi
	leaq	352(%r15), %rdi
	call	_ZN2v88internal8compiler14OperationTyper10NumberCeilENS1_4TypeE@PLT
	movq	%rax, -88(%rbp)
	movq	%rax, %rsi
	jmp	.L294
.L253:
	movq	%r13, %rsi
	leaq	352(%r15), %rdi
	call	_ZN2v88internal8compiler14OperationTyper10NumberCbrtENS1_4TypeE@PLT
	movq	%rax, -88(%rbp)
	movq	%rax, %rsi
	jmp	.L294
.L254:
	movq	%r13, %rsi
	leaq	352(%r15), %rdi
	call	_ZN2v88internal8compiler14OperationTyper11NumberAtanhENS1_4TypeE@PLT
	movq	%rax, -88(%rbp)
	movq	%rax, %rsi
	jmp	.L294
.L255:
	movq	%r13, %rsi
	leaq	352(%r15), %rdi
	call	_ZN2v88internal8compiler14OperationTyper10NumberAtanENS1_4TypeE@PLT
	movq	%rax, -88(%rbp)
	movq	%rax, %rsi
	jmp	.L294
.L256:
	movq	%r13, %rsi
	leaq	352(%r15), %rdi
	call	_ZN2v88internal8compiler14OperationTyper11NumberAsinhENS1_4TypeE@PLT
	movq	%rax, -88(%rbp)
	movq	%rax, %rsi
	jmp	.L294
.L257:
	movq	%r13, %rsi
	leaq	352(%r15), %rdi
	call	_ZN2v88internal8compiler14OperationTyper10NumberAsinENS1_4TypeE@PLT
	movq	%rax, -88(%rbp)
	movq	%rax, %rsi
	jmp	.L294
.L258:
	movq	%r13, %rsi
	leaq	352(%r15), %rdi
	call	_ZN2v88internal8compiler14OperationTyper11NumberAcoshENS1_4TypeE@PLT
	movq	%rax, -88(%rbp)
	movq	%rax, %rsi
	jmp	.L294
.L259:
	movq	%r13, %rsi
	leaq	352(%r15), %rdi
	call	_ZN2v88internal8compiler14OperationTyper10NumberAcosENS1_4TypeE@PLT
	movq	%rax, -88(%rbp)
	movq	%rax, %rsi
	jmp	.L294
.L260:
	movq	%r13, %rsi
	leaq	352(%r15), %rdi
	call	_ZN2v88internal8compiler14OperationTyper9NumberAbsENS1_4TypeE@PLT
	movq	%rax, -88(%rbp)
	movq	%rax, %rsi
	jmp	.L294
.L261:
	movq	(%r15), %rax
	movq	16(%r11), %r8
	leaq	352(%r15), %rdi
	movq	%r13, %rsi
	movq	(%rax), %rax
	movq	%r8, -104(%rbp)
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler14OperationTyper30SpeculativeSafeIntegerSubtractENS1_4TypeES3_@PLT
	jmp	.L332
.L262:
	movq	(%r15), %rax
	movq	16(%r11), %r8
	leaq	352(%r15), %rdi
	movq	%r13, %rsi
	movq	(%rax), %rax
	movq	%r8, -104(%rbp)
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler14OperationTyper25SpeculativeSafeIntegerAddENS1_4TypeES3_@PLT
	jmp	.L332
.L263:
	movq	(%r15), %rax
	movq	16(%r11), %r8
	leaq	352(%r15), %rdi
	movq	%r13, %rsi
	movq	(%rax), %rax
	movq	%r8, -104(%rbp)
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler14OperationTyper34SpeculativeNumberShiftRightLogicalENS1_4TypeES3_@PLT
	jmp	.L332
.L264:
	movq	(%r15), %rax
	movq	16(%r11), %r8
	leaq	352(%r15), %rdi
	movq	%r13, %rsi
	movq	(%rax), %rax
	movq	%r8, -104(%rbp)
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler14OperationTyper27SpeculativeNumberShiftRightENS1_4TypeES3_@PLT
	jmp	.L332
.L265:
	movq	(%r15), %rax
	movq	16(%r11), %r8
	leaq	352(%r15), %rdi
	movq	%r13, %rsi
	movq	(%rax), %rax
	movq	%r8, -104(%rbp)
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler14OperationTyper26SpeculativeNumberShiftLeftENS1_4TypeES3_@PLT
	jmp	.L332
.L266:
	movq	(%r15), %rax
	movq	16(%r11), %r8
	leaq	352(%r15), %rdi
	movq	%r13, %rsi
	movq	(%rax), %rax
	movq	%r8, -104(%rbp)
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler14OperationTyper27SpeculativeNumberBitwiseXorENS1_4TypeES3_@PLT
	jmp	.L332
.L267:
	movq	(%r15), %rax
	movq	16(%r11), %r8
	leaq	352(%r15), %rdi
	movq	%r13, %rsi
	movq	(%rax), %rax
	movq	%r8, -104(%rbp)
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler14OperationTyper26SpeculativeNumberBitwiseOrENS1_4TypeES3_@PLT
	jmp	.L332
.L268:
	movq	(%r15), %rax
	movq	16(%r11), %r8
	leaq	352(%r15), %rdi
	movq	%r13, %rsi
	movq	(%rax), %rax
	movq	%r8, -104(%rbp)
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler14OperationTyper27SpeculativeNumberBitwiseAndENS1_4TypeES3_@PLT
	jmp	.L332
.L269:
	movq	(%r15), %rax
	movq	16(%r11), %r8
	leaq	352(%r15), %rdi
	movq	%r13, %rsi
	movq	(%rax), %rax
	movq	%r8, -104(%rbp)
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler14OperationTyper24SpeculativeNumberModulusENS1_4TypeES3_@PLT
	jmp	.L332
.L270:
	movq	(%r15), %rax
	movq	16(%r11), %r8
	leaq	352(%r15), %rdi
	movq	%r13, %rsi
	movq	(%rax), %rax
	movq	%r8, -104(%rbp)
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler14OperationTyper23SpeculativeNumberDivideENS1_4TypeES3_@PLT
	jmp	.L332
.L271:
	movq	(%r15), %rax
	movq	16(%r11), %r8
	leaq	352(%r15), %rdi
	movq	%r13, %rsi
	movq	(%rax), %rax
	movq	%r8, -104(%rbp)
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler14OperationTyper25SpeculativeNumberMultiplyENS1_4TypeES3_@PLT
	jmp	.L332
.L272:
	movq	(%r15), %rax
	movq	16(%r11), %r8
	leaq	352(%r15), %rdi
	movq	%r13, %rsi
	movq	(%rax), %rax
	movq	%r8, -104(%rbp)
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler14OperationTyper25SpeculativeNumberSubtractENS1_4TypeES3_@PLT
	jmp	.L332
.L273:
	movq	(%r15), %rax
	movq	16(%r11), %r8
	leaq	352(%r15), %rdi
	movq	%r13, %rsi
	movq	(%rax), %rax
	movq	%r8, -104(%rbp)
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler14OperationTyper20SpeculativeNumberAddENS1_4TypeES3_@PLT
	jmp	.L332
.L274:
	movq	%r13, %rsi
	leaq	352(%r15), %rdi
	call	_ZN2v88internal8compiler14OperationTyper9NumberPowENS1_4TypeES3_@PLT
	movq	%rax, -88(%rbp)
	movq	%rax, %rsi
	jmp	.L294
.L275:
	movq	%r13, %rsi
	leaq	352(%r15), %rdi
	call	_ZN2v88internal8compiler14OperationTyper9NumberMinENS1_4TypeES3_@PLT
	movq	%rax, -88(%rbp)
	movq	%rax, %rsi
	jmp	.L294
.L276:
	movq	%r13, %rsi
	leaq	352(%r15), %rdi
	call	_ZN2v88internal8compiler14OperationTyper9NumberMaxENS1_4TypeES3_@PLT
	movq	%rax, -88(%rbp)
	movq	%rax, %rsi
	jmp	.L294
.L277:
	movq	%r13, %rsi
	leaq	352(%r15), %rdi
	call	_ZN2v88internal8compiler14OperationTyper10NumberImulENS1_4TypeES3_@PLT
	movq	%rax, -88(%rbp)
	movq	%rax, %rsi
	jmp	.L294
.L278:
	movq	%r13, %rsi
	leaq	352(%r15), %rdi
	call	_ZN2v88internal8compiler14OperationTyper11NumberAtan2ENS1_4TypeES3_@PLT
	movq	%rax, -88(%rbp)
	movq	%rax, %rsi
	jmp	.L294
.L279:
	movq	%r13, %rsi
	leaq	352(%r15), %rdi
	call	_ZN2v88internal8compiler14OperationTyper23NumberShiftRightLogicalENS1_4TypeES3_@PLT
	movq	%rax, -88(%rbp)
	movq	%rax, %rsi
	jmp	.L294
.L280:
	movq	%r13, %rsi
	leaq	352(%r15), %rdi
	call	_ZN2v88internal8compiler14OperationTyper16NumberShiftRightENS1_4TypeES3_@PLT
	movq	%rax, -88(%rbp)
	movq	%rax, %rsi
	jmp	.L294
.L281:
	movq	%r13, %rsi
	leaq	352(%r15), %rdi
	call	_ZN2v88internal8compiler14OperationTyper15NumberShiftLeftENS1_4TypeES3_@PLT
	movq	%rax, -88(%rbp)
	movq	%rax, %rsi
	jmp	.L294
.L282:
	movq	%r13, %rsi
	leaq	352(%r15), %rdi
	call	_ZN2v88internal8compiler14OperationTyper16NumberBitwiseAndENS1_4TypeES3_@PLT
	movq	%rax, -88(%rbp)
	movq	%rax, %rsi
	jmp	.L294
.L283:
	movq	%r13, %rsi
	leaq	352(%r15), %rdi
	call	_ZN2v88internal8compiler14OperationTyper16NumberBitwiseXorENS1_4TypeES3_@PLT
	movq	%rax, -88(%rbp)
	movq	%rax, %rsi
	jmp	.L294
.L284:
	movq	%r13, %rsi
	leaq	352(%r15), %rdi
	call	_ZN2v88internal8compiler14OperationTyper15NumberBitwiseOrENS1_4TypeES3_@PLT
	movq	%rax, -88(%rbp)
	movq	%rax, %rsi
	jmp	.L294
.L285:
	movq	%r13, %rsi
	leaq	352(%r15), %rdi
	call	_ZN2v88internal8compiler14OperationTyper13NumberModulusENS1_4TypeES3_@PLT
	movq	%rax, -88(%rbp)
	movq	%rax, %rsi
	jmp	.L294
.L286:
	movq	%r13, %rsi
	leaq	352(%r15), %rdi
	call	_ZN2v88internal8compiler14OperationTyper12NumberDivideENS1_4TypeES3_@PLT
	movq	%rax, -88(%rbp)
	movq	%rax, %rsi
	jmp	.L294
.L287:
	movq	%r13, %rsi
	leaq	352(%r15), %rdi
	call	_ZN2v88internal8compiler14OperationTyper14NumberMultiplyENS1_4TypeES3_@PLT
	movq	%rax, -88(%rbp)
	movq	%rax, %rsi
	jmp	.L294
.L288:
	movq	%r13, %rsi
	leaq	352(%r15), %rdi
	call	_ZN2v88internal8compiler14OperationTyper14NumberSubtractENS1_4TypeES3_@PLT
	movq	%rax, -88(%rbp)
	movq	%rax, %rsi
	jmp	.L294
.L289:
	movq	%r13, %rsi
	leaq	352(%r15), %rdi
	call	_ZN2v88internal8compiler14OperationTyper9NumberAddENS1_4TypeES3_@PLT
	movq	%rax, -88(%rbp)
	movq	%rax, %rsi
	jmp	.L294
.L290:
	movq	%r13, %rsi
	leaq	352(%r15), %rdi
	call	_ZN2v88internal8compiler14OperationTyper9SameValueENS1_4TypeES3_@PLT
	movq	%rax, -88(%rbp)
	movq	%rax, %rsi
	jmp	.L294
.L291:
	movq	32(%r12), %rax
	leaq	352(%r15), %rdi
	cmpl	$15, %r9d
	jne	.L309
	movq	16(%rax), %rax
.L309:
	movl	20(%rax), %eax
	movl	$1, %edx
	movq	%r10, %rsi
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rax
	leaq	(%rcx,%rax,8), %rax
	cmpq	$0, 24(%rax)
	cmovne	24(%rax), %rdx
	call	_ZN2v88internal8compiler14OperationTyper13TypeTypeGuardEPKNS1_8OperatorENS1_4TypeE@PLT
	movq	%rax, -88(%rbp)
	movq	%rax, %rsi
	jmp	.L294
.L292:
	movl	20(%r10), %edx
	movq	32(%r12), %rax
	leaq	32(%r12), %rbx
	cmpl	$15, %r9d
	jne	.L295
	movq	16(%rax), %rax
.L295:
	movl	20(%rax), %eax
	movl	$1, %r9d
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rax
	leaq	(%rcx,%rax,8), %rax
	cmpq	$0, 24(%rax)
	cmovne	24(%rax), %r9
	cmpl	$1, %edx
	jle	.L296
	leaq	352(%r15), %rax
	movl	$8, %r13d
	movq	%rax, -104(%rbp)
	leal	-2(%rdx), %eax
	leaq	16(,%rax,8), %rax
	movq	%rax, -112(%rbp)
	jmp	.L300
.L341:
	movq	(%rbx,%r13), %rax
.L331:
	movl	20(%rax), %eax
	movq	-104(%rbp), %rdi
	movl	$1, %edx
	movq	%r9, %rsi
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rax
	leaq	(%rcx,%rax,8), %rax
	cmpq	$0, 24(%rax)
	cmovne	24(%rax), %rdx
	addq	$8, %r13
	call	_ZN2v88internal8compiler14OperationTyper5MergeENS1_4TypeES3_@PLT
	movq	%rax, %r9
	cmpq	-112(%rbp), %r13
	je	.L296
	movl	20(%r12), %esi
	movq	32(%r15), %rcx
.L300:
	xorl	$251658240, %esi
	andl	$251658240, %esi
	jne	.L341
	movq	(%rbx), %rax
	movq	16(%rax,%r13), %rax
	jmp	.L331
.L293:
	leaq	352(%r15), %rdi
	cmpl	$15, %r9d
	je	.L310
	movq	48(%r12), %rax
	movl	$1, %edx
	movl	20(%rax), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rax
	leaq	(%rcx,%rax,8), %rax
	cmpq	$0, 24(%rax)
	cmovne	24(%rax), %rdx
	leaq	40(%r12), %rax
.L311:
	movq	(%rax), %rax
	movl	$1, %esi
	movl	20(%rax), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rax
	leaq	(%rcx,%rax,8), %rax
	cmpq	$0, 24(%rax)
	cmovne	24(%rax), %rsi
	call	_ZN2v88internal8compiler14OperationTyper5MergeENS1_4TypeES3_@PLT
	movq	%rax, -88(%rbp)
	movq	-88(%rbp), %rsi
	jmp	.L294
	.p2align 4,,10
	.p2align 3
.L340:
	movq	%r12, %rsi
	movq	%r15, %rdi
	movb	%al, -104(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector21PrintNodeFeedbackTypeEPNS1_4NodeE
	movzbl	-104(%rbp), %eax
	jmp	.L198
.L296:
	movq	%r9, -88(%rbp)
	testq	%r14, %r14
	jne	.L342
	movq	(%r15), %rax
	movq	8(%r12), %rdi
	movq	%r9, %rsi
	movq	(%rax), %rax
	movq	(%rax), %rdx
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -88(%rbp)
	jmp	.L312
.L310:
	movq	32(%r12), %rax
	movq	32(%rax), %rdx
	movl	20(%rdx), %edx
	andl	$16777215, %edx
	leaq	(%rdx,%rdx,4), %rdx
	leaq	(%rcx,%rdx,8), %rsi
	movl	$1, %edx
	cmpq	$0, 24(%rsi)
	cmovne	24(%rsi), %rdx
	addq	$24, %rax
	jmp	.L311
.L342:
	movq	344(%r15), %rax
	leaq	-80(%rbp), %rdi
	movq	%r9, -104(%rbp)
	movq	%r14, -80(%rbp)
	movq	320(%rax), %r13
	movq	%r13, %rsi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	movq	-104(%rbp), %r9
	testb	%al, %al
	jne	.L343
.L303:
	movq	(%r15), %rax
	movq	8(%r12), %rdi
	movq	%r9, -88(%rbp)
	movq	%r9, %rsi
	movq	(%rax), %rax
	movq	(%rax), %rdx
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -88(%rbp)
	jmp	.L307
.L343:
	movq	(%r15), %rax
	movq	%r9, %rdi
	movq	%r13, %rsi
	movq	(%rax), %rax
	movq	(%rax), %rdx
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	-80(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rax, -72(%rbp)
	movq	(%r15), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdx
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	-104(%rbp), %r9
	movq	%rax, -64(%rbp)
	movl	20(%r12), %eax
	andl	$16777215, %eax
	imulq	$40, %rax, %rax
	addq	32(%r15), %rax
	cmpb	$0, 32(%rax)
	jne	.L304
	leaq	-64(%rbp), %rdi
	call	_ZNK2v88internal8compiler4Type8GetRangeEv@PLT
	leaq	-72(%rbp), %rdi
	movq	%rax, %rbx
	call	_ZNK2v88internal8compiler4Type8GetRangeEv@PLT
	movq	-104(%rbp), %r9
	testq	%rax, %rax
	je	.L303
	testq	%rbx, %rbx
	je	.L303
	movl	20(%r12), %eax
	andl	$16777215, %eax
	imulq	$40, %rax, %rax
	addq	32(%r15), %rax
	movb	$1, 32(%rax)
.L304:
	movq	(%r15), %rax
	movq	-72(%rbp), %rdx
	movq	%r9, -104(%rbp)
	leaq	352(%r15), %rdi
	movq	-64(%rbp), %rsi
	movq	(%rax), %rax
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler14OperationTyper11WeakenRangeENS1_4TypeES3_@PLT
	movq	-104(%rbp), %r9
	movq	%rax, %rsi
	movq	%r13, %rdx
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, %r9
	jmp	.L303
.L336:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22991:
	.size	_ZN2v88internal8compiler22RepresentationSelector18UpdateFeedbackTypeEPNS1_4NodeE, .-_ZN2v88internal8compiler22RepresentationSelector18UpdateFeedbackTypeEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler22RepresentationSelector13BothInputsAreEPNS1_4NodeENS1_4TypeE,"axG",@progbits,_ZN2v88internal8compiler22RepresentationSelector13BothInputsAreEPNS1_4NodeENS1_4TypeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler22RepresentationSelector13BothInputsAreEPNS1_4NodeENS1_4TypeE
	.type	_ZN2v88internal8compiler22RepresentationSelector13BothInputsAreEPNS1_4NodeENS1_4TypeE, @function
_ZN2v88internal8compiler22RepresentationSelector13BothInputsAreEPNS1_4NodeENS1_4TypeE:
.LFB23007:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	32(%rsi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	32(%rsi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L345
	movq	8(%rdx), %rax
	movq	%rax, -56(%rbp)
	cmpq	%rax, %r12
	jne	.L350
.L346:
	leaq	8(%r13), %rax
.L351:
	movq	(%rax), %rax
	movq	8(%rax), %rdx
	movl	$1, %eax
	movq	%rdx, -48(%rbp)
	cmpq	%rdx, %r12
	je	.L344
	leaq	-48(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
.L344:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L355
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L345:
	.cfi_restore_state
	leaq	16(%rdx), %rax
	movq	16(%rdx), %rdx
	movq	8(%rdx), %rdx
	movq	%rdx, -56(%rbp)
	cmpq	%rdx, %r12
	je	.L349
.L350:
	leaq	-56(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L344
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L346
	movq	32(%rbx), %rax
	addq	$16, %rax
.L349:
	addq	$8, %rax
	jmp	.L351
.L355:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23007:
	.size	_ZN2v88internal8compiler22RepresentationSelector13BothInputsAreEPNS1_4NodeENS1_4TypeE, .-_ZN2v88internal8compiler22RepresentationSelector13BothInputsAreEPNS1_4NodeENS1_4TypeE
	.section	.text._ZN2v88internal8compiler22RepresentationSelector16OneInputCannotBeEPNS1_4NodeENS1_4TypeE,"axG",@progbits,_ZN2v88internal8compiler22RepresentationSelector16OneInputCannotBeEPNS1_4NodeENS1_4TypeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler22RepresentationSelector16OneInputCannotBeEPNS1_4NodeENS1_4TypeE
	.type	_ZN2v88internal8compiler22RepresentationSelector16OneInputCannotBeEPNS1_4NodeENS1_4TypeE, @function
_ZN2v88internal8compiler22RepresentationSelector16OneInputCannotBeEPNS1_4NodeENS1_4TypeE:
.LFB23009:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	32(%rsi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	32(%rsi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L357
	movq	16(%rdx), %rdx
.L357:
	movq	8(%rdx), %rax
	leaq	-56(%rbp), %rdi
	movq	%r12, %rsi
	movq	%rax, -56(%rbp)
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	movl	%eax, %r8d
	movl	$1, %eax
	testb	%r8b, %r8b
	je	.L356
	movzbl	23(%rbx), %eax
	addq	$8, %r13
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L365
.L360:
	movq	0(%r13), %rax
	leaq	-48(%rbp), %rdi
	movq	%r12, %rsi
	movq	8(%rax), %rax
	movq	%rax, -48(%rbp)
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	xorl	$1, %eax
.L356:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L366
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L365:
	.cfi_restore_state
	movq	32(%rbx), %r13
	addq	$24, %r13
	jmp	.L360
.L366:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23009:
	.size	_ZN2v88internal8compiler22RepresentationSelector16OneInputCannotBeEPNS1_4NodeENS1_4TypeE, .-_ZN2v88internal8compiler22RepresentationSelector16OneInputCannotBeEPNS1_4NodeENS1_4TypeE
	.section	.text._ZN2v88internal8compiler22RepresentationSelector14ChangeToPureOpEPNS1_4NodeEPKNS1_8OperatorE,"axG",@progbits,_ZN2v88internal8compiler22RepresentationSelector14ChangeToPureOpEPNS1_4NodeEPKNS1_8OperatorE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler22RepresentationSelector14ChangeToPureOpEPNS1_4NodeEPKNS1_8OperatorE
	.type	_ZN2v88internal8compiler22RepresentationSelector14ChangeToPureOpEPNS1_4NodeEPKNS1_8OperatorE, @function
_ZN2v88internal8compiler22RepresentationSelector14ChangeToPureOpEPNS1_4NodeEPKNS1_8OperatorE:
.LFB23011:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movl	24(%rax), %esi
	testl	%esi, %esi
	jle	.L368
	movq	%rdi, %rbx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movl	20(%r12), %edx
	movq	32(%rbx), %rcx
	movq	%rax, %r15
	andl	$16777215, %edx
	leaq	(%rdx,%rdx,4), %rdx
	leaq	(%rcx,%rdx,8), %rdx
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L369
	movq	8(%r12), %rdx
.L369:
	cmpq	$1, %rdx
	je	.L386
	movl	20(%r13), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node14TrimInputCountEi@PLT
	movq	24(%r12), %rdi
	movq	%r14, %rdx
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_124ReplaceEffectControlUsesEPNS1_4NodeES4_S4_.isra.0
.L368:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
.L367:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L387
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L386:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	%r14, %xmm1
	movq	%r15, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	8(%rax), %rdi
	movq	(%rax), %r13
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder11UnreachableEv@PLT
	movdqa	-96(%rbp), %xmm0
	movq	%r13, %rdi
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	leaq	-80(%rbp), %rcx
	movl	$2, %edx
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	32(%rbx), %rdx
	movq	%rax, %r13
	movl	20(%r12), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rax
	leaq	(%rdx,%rax,8), %rax
	movzbl	1(%rax), %esi
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9DeadValueENS0_21MachineRepresentationE@PLT
	leaq	32(%r12), %rdx
	movq	%rax, %r15
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L371
	movq	32(%r12), %rdi
	movq	%r12, %rsi
	cmpq	%rdi, %r13
	je	.L373
.L372:
	leaq	-24(%rsi), %rbx
	testq	%rdi, %rdi
	je	.L375
	movq	%rbx, %rsi
	movq	%rdx, -96(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-96(%rbp), %rdx
.L375:
	movq	%r13, (%rdx)
	testq	%r13, %r13
	je	.L373
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L373:
	movl	20(%r15), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node14TrimInputCountEi@PLT
	movq	24(%r12), %rdi
	movq	%r13, %rsi
	movq	%r14, %rdx
	call	_ZN2v88internal8compiler12_GLOBAL__N_124ReplaceEffectControlUsesEPNS1_4NodeES4_S4_.isra.0
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	jmp	.L367
	.p2align 4,,10
	.p2align 3
.L371:
	movq	32(%r12), %rsi
	movq	16(%rsi), %rdi
	leaq	16(%rsi), %rdx
	cmpq	%rdi, %r13
	jne	.L372
	jmp	.L373
.L387:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23011:
	.size	_ZN2v88internal8compiler22RepresentationSelector14ChangeToPureOpEPNS1_4NodeEPKNS1_8OperatorE, .-_ZN2v88internal8compiler22RepresentationSelector14ChangeToPureOpEPNS1_4NodeEPKNS1_8OperatorE
	.section	.text._ZN2v88internal8compiler22RepresentationSelector25ChangeUnaryToPureBinaryOpEPNS1_4NodeEPKNS1_8OperatorEiS4_,"axG",@progbits,_ZN2v88internal8compiler22RepresentationSelector25ChangeUnaryToPureBinaryOpEPNS1_4NodeEPKNS1_8OperatorEiS4_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler22RepresentationSelector25ChangeUnaryToPureBinaryOpEPNS1_4NodeEPKNS1_8OperatorEiS4_
	.type	_ZN2v88internal8compiler22RepresentationSelector25ChangeUnaryToPureBinaryOpEPNS1_4NodeEPKNS1_8OperatorEiS4_, @function
_ZN2v88internal8compiler22RepresentationSelector25ChangeUnaryToPureBinaryOpEPNS1_4NodeEPKNS1_8OperatorEiS4_:
.LFB23012:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movl	24(%rax), %esi
	testl	%esi, %esi
	jle	.L389
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movl	20(%r12), %edx
	movq	32(%rbx), %rcx
	movq	%rax, %r8
	andl	$16777215, %edx
	leaq	(%rdx,%rdx,4), %rdx
	leaq	(%rcx,%rdx,8), %rdx
	movq	24(%rdx), %rax
	testq	%rax, %rax
	jne	.L390
	movq	8(%r12), %rax
.L390:
	cmpq	$1, %rax
	je	.L407
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	%r8, -112(%rbp)
	movl	20(%rax), %esi
	call	_ZN2v88internal8compiler4Node14TrimInputCountEi@PLT
	movq	-112(%rbp), %r8
	movq	24(%r12), %rdi
	movq	-88(%rbp), %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_124ReplaceEffectControlUsesEPNS1_4NodeES4_S4_.isra.0
.L389:
	movq	(%rbx), %rax
	movq	%r12, %rdi
	movq	%r15, %rcx
	movl	%r14d, %edx
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
.L388:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L408
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L407:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	%r8, %xmm0
	movhps	-88(%rbp), %xmm0
	movq	8(%rax), %rdi
	movq	(%rax), %r13
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder11UnreachableEv@PLT
	movdqa	-112(%rbp), %xmm0
	movq	%r13, %rdi
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	leaq	-80(%rbp), %rcx
	movl	$2, %edx
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	32(%rbx), %rdx
	movq	%rax, %r13
	movl	20(%r12), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rax
	leaq	(%rdx,%rax,8), %rax
	movzbl	1(%rax), %esi
	movq	(%rbx), %rax
	leaq	32(%r12), %rbx
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9DeadValueENS0_21MachineRepresentationE@PLT
	movq	%rax, %r14
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L392
	movq	32(%r12), %rdi
	movq	%r12, %rsi
	cmpq	%rdi, %r13
	je	.L394
.L393:
	leaq	-24(%rsi), %r15
	testq	%rdi, %rdi
	je	.L396
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L396:
	movq	%r13, (%rbx)
	testq	%r13, %r13
	je	.L394
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L394:
	movl	20(%r14), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node14TrimInputCountEi@PLT
	movq	24(%r12), %rdi
	movq	-88(%rbp), %rdx
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_124ReplaceEffectControlUsesEPNS1_4NodeES4_S4_.isra.0
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	jmp	.L388
	.p2align 4,,10
	.p2align 3
.L392:
	movq	32(%r12), %rsi
	movq	16(%rsi), %rdi
	leaq	16(%rsi), %rbx
	cmpq	%rdi, %r13
	jne	.L393
	jmp	.L394
.L408:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23012:
	.size	_ZN2v88internal8compiler22RepresentationSelector25ChangeUnaryToPureBinaryOpEPNS1_4NodeEPKNS1_8OperatorEiS4_, .-_ZN2v88internal8compiler22RepresentationSelector25ChangeUnaryToPureBinaryOpEPNS1_4NodeEPKNS1_8OperatorEiS4_
	.section	.text._ZN2v88internal8compiler22RepresentationSelector19GetOutputInfoForPhiEPNS1_4NodeENS1_4TypeENS1_10TruncationE,"axG",@progbits,_ZN2v88internal8compiler22RepresentationSelector19GetOutputInfoForPhiEPNS1_4NodeENS1_4TypeENS1_10TruncationE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler22RepresentationSelector19GetOutputInfoForPhiEPNS1_4NodeENS1_4TypeENS1_10TruncationE
	.type	_ZN2v88internal8compiler22RepresentationSelector19GetOutputInfoForPhiEPNS1_4NodeENS1_4TypeENS1_10TruncationE, @function
_ZN2v88internal8compiler22RepresentationSelector19GetOutputInfoForPhiEPNS1_4NodeENS1_4TypeENS1_10TruncationE:
.LFB23028:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdx, -40(%rbp)
	cmpq	$1, %rdx
	jne	.L410
.L412:
	xorl	%eax, %eax
.L409:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L410:
	.cfi_restore_state
	leaq	-40(%rbp), %r13
	movq	%rdi, %r12
	movl	$1, %esi
	movq	%rcx, %rbx
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L412
	cmpq	$1099, -40(%rbp)
	jne	.L413
.L414:
	addq	$24, %rsp
	movl	$4, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L413:
	.cfi_restore_state
	movl	$1099, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L414
	cmpq	$1031, -40(%rbp)
	je	.L414
	movl	$1031, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L414
	cmpq	$8396767, -40(%rbp)
	je	.L417
	movl	$8396767, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L416
.L417:
	movl	$2, %esi
	movl	%ebx, %edi
	call	_ZN2v88internal8compiler10Truncation11LessGeneralENS2_14TruncationKindES3_@PLT
	testb	%al, %al
	jne	.L414
.L416:
	cmpq	$513, -40(%rbp)
	je	.L419
	movl	$513, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L419
	cmpq	$8396767, -40(%rbp)
	je	.L423
	movl	$8396767, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L422
.L423:
	movl	$4, %esi
	movl	%ebx, %edi
	call	_ZN2v88internal8compiler10Truncation11LessGeneralENS2_14TruncationKindES3_@PLT
	testb	%al, %al
	jne	.L421
.L422:
	movq	8(%r12), %r12
	call	_ZN2v88internal8compiler10BitsetType11SignedSmallEv@PLT
	movl	$4097, %esi
	movl	%eax, %edi
	movq	%r12, %rdx
	orl	$1, %edi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, %rsi
	cmpq	%rax, -40(%rbp)
	je	.L424
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L424
	cmpq	$7263, -40(%rbp)
	je	.L421
	movl	$7263, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L421
	cmpq	$134217729, -40(%rbp)
	je	.L429
	movl	$134217729, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L428
.L429:
	movl	$3, %esi
	movl	%ebx, %edi
	call	_ZN2v88internal8compiler10Truncation11LessGeneralENS2_14TruncationKindES3_@PLT
	testb	%al, %al
	jne	.L436
.L428:
	cmpq	$33554433, -40(%rbp)
	je	.L436
	movl	$33554433, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L424
.L436:
	movl	$5, %eax
	jmp	.L409
	.p2align 4,,10
	.p2align 3
.L419:
	movl	$1, %eax
	jmp	.L409
.L424:
	movl	$8, %eax
	jmp	.L409
.L421:
	movl	$13, %eax
	jmp	.L409
	.cfi_endproc
.LFE23028:
	.size	_ZN2v88internal8compiler22RepresentationSelector19GetOutputInfoForPhiEPNS1_4NodeENS1_4TypeENS1_10TruncationE, .-_ZN2v88internal8compiler22RepresentationSelector19GetOutputInfoForPhiEPNS1_4NodeENS1_4TypeENS1_10TruncationE
	.section	.text._ZN2v88internal8compiler22RepresentationSelector16MaskShiftOperandEPNS1_4NodeENS1_4TypeE,"axG",@progbits,_ZN2v88internal8compiler22RepresentationSelector16MaskShiftOperandEPNS1_4NodeENS1_4TypeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler22RepresentationSelector16MaskShiftOperandEPNS1_4NodeENS1_4TypeE
	.type	_ZN2v88internal8compiler22RepresentationSelector16MaskShiftOperandEPNS1_4NodeENS1_4TypeE, @function
_ZN2v88internal8compiler22RepresentationSelector16MaskShiftOperandEPNS1_4NodeENS1_4TypeE:
.LFB23034:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$64, %rsp
	movq	%rdx, -72(%rbp)
	movq	344(%rdi), %rdx
	movq	296(%rdx), %rsi
	movq	%fs:40, %rcx
	movq	%rcx, -40(%rbp)
	xorl	%ecx, %ecx
	cmpq	%rsi, %rax
	jne	.L452
.L437:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L453
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L452:
	.cfi_restore_state
	movq	%rdi, %r12
	leaq	-72(%rbp), %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L437
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	(%r12), %rdi
	movl	$31, %esi
	movq	%rax, -80(%rbp)
	movq	(%rdi), %r13
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%rax, -88(%rbp)
	movq	(%r12), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32AndEv@PLT
	movq	%r13, %rdi
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	-80(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movhps	-88(%rbp), %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r13
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L441
	movq	40(%rbx), %rdi
	leaq	40(%rbx), %r14
	cmpq	%rdi, %r13
	je	.L437
.L442:
	leaq	-48(%rbx), %r12
	testq	%rdi, %rdi
	je	.L443
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L443:
	movq	%r13, (%r14)
	testq	%r13, %r13
	je	.L437
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	jmp	.L437
	.p2align 4,,10
	.p2align 3
.L441:
	movq	32(%rbx), %rbx
	movq	24(%rbx), %rdi
	cmpq	%rdi, %r13
	je	.L437
	leaq	24(%rbx), %r14
	jmp	.L442
.L453:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23034:
	.size	_ZN2v88internal8compiler22RepresentationSelector16MaskShiftOperandEPNS1_4NodeENS1_4TypeE, .-_ZN2v88internal8compiler22RepresentationSelector16MaskShiftOperandEPNS1_4NodeENS1_4TypeE
	.section	.text._ZN2v88internal8compiler22RepresentationSelector19WriteBarrierKindForENS1_14BaseTaggednessENS0_21MachineRepresentationENS1_4TypeES4_PNS1_4NodeE,"axG",@progbits,_ZN2v88internal8compiler22RepresentationSelector19WriteBarrierKindForENS1_14BaseTaggednessENS0_21MachineRepresentationENS1_4TypeES4_PNS1_4NodeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler22RepresentationSelector19WriteBarrierKindForENS1_14BaseTaggednessENS0_21MachineRepresentationENS1_4TypeES4_PNS1_4NodeE
	.type	_ZN2v88internal8compiler22RepresentationSelector19WriteBarrierKindForENS1_14BaseTaggednessENS0_21MachineRepresentationENS1_4TypeES4_PNS1_4NodeE, @function
_ZN2v88internal8compiler22RepresentationSelector19WriteBarrierKindForENS1_14BaseTaggednessENS0_21MachineRepresentationENS1_4TypeES4_PNS1_4NodeE:
.LFB23046:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rcx, -40(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpb	$1, %sil
	je	.L481
.L454:
	movq	-24(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L482
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L481:
	.cfi_restore_state
	leal	-7(%rdx), %ecx
	movq	%rdi, %rbx
	cmpb	$1, %cl
	jbe	.L456
	leal	-10(%rdx), %ecx
	cmpb	$1, %cl
	ja	.L454
.L456:
	movq	8(%r9), %rax
	movl	%edx, -52(%rbp)
	movq	%r9, -48(%rbp)
	movq	%rax, -32(%rbp)
	cmpb	$6, %r8b
	je	.L457
	cmpb	$9, %r8b
	movl	%r8d, -64(%rbp)
	je	.L457
	cmpq	$897, -40(%rbp)
	je	.L457
	leaq	-40(%rbp), %rdi
	movl	$897, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L457
	cmpq	$897, -32(%rbp)
	je	.L457
	leaq	-32(%rbp), %r12
	movl	$897, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L457
	movq	-32(%rbp), %rax
	movl	-64(%rbp), %r8d
	movq	-48(%rbp), %r9
	movl	-52(%rbp), %edx
	testb	$1, %al
	jne	.L461
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L483
.L461:
	cmpb	$7, %dl
	je	.L474
	cmpb	$7, %r8b
	je	.L474
	cmpb	$10, %dl
	je	.L474
	cmpb	$10, %r8b
	je	.L474
	movq	(%r9), %rdx
	movl	$5, %eax
	cmpw	$28, 16(%rdx)
	jne	.L454
	movsd	48(%rdx), %xmm0
	comisd	.LC11(%rip), %xmm0
	jb	.L474
	movsd	.LC12(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L474
	movl	$1, %eax
	movq	%xmm0, %rbx
	salq	$63, %rax
	cmpq	%rax, %rbx
	je	.L474
	cvttsd2sil	%xmm0, %eax
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L474
	je	.L457
.L474:
	movl	$3, %eax
	jmp	.L454
.L483:
	movq	(%rbx), %rax
	movq	%r12, %rdi
	movq	%r9, -64(%rbp)
	movl	%r8d, -52(%rbp)
	movl	%edx, -48(%rbp)
	movq	360(%rax), %rbx
	call	_ZNK2v88internal8compiler4Type14AsHeapConstantEv@PLT
	movq	%rax, %rdi
	call	_ZNK2v88internal8compiler16HeapConstantType5ValueEv@PLT
	leaq	56(%rbx), %rcx
	movl	-48(%rbp), %edx
	movl	-52(%rbp), %r8d
	cmpq	%rax, %rcx
	movq	-64(%rbp), %r9
	ja	.L461
	addq	$4856, %rbx
	cmpq	%rax, %rbx
	jbe	.L461
	subq	%rcx, %rax
	shrq	$3, %rax
	cmpw	$573, %ax
	ja	.L461
	.p2align 4,,10
	.p2align 3
.L457:
	xorl	%eax, %eax
	jmp	.L454
.L482:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23046:
	.size	_ZN2v88internal8compiler22RepresentationSelector19WriteBarrierKindForENS1_14BaseTaggednessENS0_21MachineRepresentationENS1_4TypeES4_PNS1_4NodeE, .-_ZN2v88internal8compiler22RepresentationSelector19WriteBarrierKindForENS1_14BaseTaggednessENS0_21MachineRepresentationENS1_4TypeES4_PNS1_4NodeE
	.section	.text._ZN2v88internal8compiler22RepresentationSelector23ChangeToInt32OverflowOpEPNS1_4NodeE,"axG",@progbits,_ZN2v88internal8compiler22RepresentationSelector23ChangeToInt32OverflowOpEPNS1_4NodeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler22RepresentationSelector23ChangeToInt32OverflowOpEPNS1_4NodeE
	.type	_ZN2v88internal8compiler22RepresentationSelector23ChangeToInt32OverflowOpEPNS1_4NodeE, @function
_ZN2v88internal8compiler22RepresentationSelector23ChangeToInt32OverflowOpEPNS1_4NodeE:
.LFB23052:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$8, %rsp
	movq	(%rsi), %rax
	movq	128(%rdi), %rdi
	movzwl	16(%rax), %esi
	call	_ZN2v88internal8compiler21RepresentationChanger24Int32OverflowOperatorForENS1_8IrOpcode5ValueE@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	.cfi_endproc
.LFE23052:
	.size	_ZN2v88internal8compiler22RepresentationSelector23ChangeToInt32OverflowOpEPNS1_4NodeE, .-_ZN2v88internal8compiler22RepresentationSelector23ChangeToInt32OverflowOpEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler22RepresentationSelector12PrintUseInfoENS1_7UseInfoE,"axG",@progbits,_ZN2v88internal8compiler22RepresentationSelector12PrintUseInfoENS1_7UseInfoE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler22RepresentationSelector12PrintUseInfoENS1_7UseInfoE
	.type	_ZN2v88internal8compiler22RepresentationSelector12PrintUseInfoENS1_7UseInfoE, @function
_ZN2v88internal8compiler22RepresentationSelector12PrintUseInfoENS1_7UseInfoE:
.LFB23065:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$368, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	jne	.L492
.L486:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L493
	addq	$368, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L492:
	.cfi_restore_state
	leaq	-304(%rbp), %r14
	leaq	-384(%rbp), %r12
	movq	%r14, %rdi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rbx
	call	_ZNSt8ios_baseC2Ev@PLT
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	stdout(%rip), %rdx
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movups	%xmm0, -72(%rbp)
	movups	%xmm0, -56(%rbp)
	movw	%ax, -80(%rbp)
	movq	%rbx, -304(%rbp)
	movq	$0, -88(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	movzbl	16(%rbp), %esi
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, -384(%rbp)
	addq	$40, %rax
	movq	%rax, -304(%rbp)
	call	_ZN2v88internallsERSoNS0_21MachineRepresentationE@PLT
	movl	$1, %edx
	leaq	.LC4(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movzbl	20(%rbp), %eax
	leaq	-392(%rbp), %rdi
	movb	%al, -392(%rbp)
	movl	24(%rbp), %eax
	movl	%eax, -388(%rbp)
	call	_ZNK2v88internal8compiler10Truncation11descriptionEv@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L494
	movq	%rax, %rdi
	call	strlen@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L489:
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rcx
	movq	%rax, -304(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-320(%rbp), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%r14, %rdi
	movq	%rbx, -304(%rbp)
	movq	%rax, -384(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L486
	.p2align 4,,10
	.p2align 3
.L494:
	movq	-384(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L489
.L493:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23065:
	.size	_ZN2v88internal8compiler22RepresentationSelector12PrintUseInfoENS1_7UseInfoE, .-_ZN2v88internal8compiler22RepresentationSelector12PrintUseInfoENS1_7UseInfoE
	.section	.rodata._ZN2v88internal8compiler22RepresentationSelector12ConvertInputEPNS1_4NodeEiNS1_7UseInfoENS1_4TypeE.str1.1,"aMS",@progbits,1
.LC13:
	.string	"  change: #%d:%s(@%d #%d:%s) "
.LC14:
	.string	" from "
.LC15:
	.string	" to "
.LC16:
	.string	"\n"
	.section	.text._ZN2v88internal8compiler22RepresentationSelector12ConvertInputEPNS1_4NodeEiNS1_7UseInfoENS1_4TypeE,"axG",@progbits,_ZN2v88internal8compiler22RepresentationSelector12ConvertInputEPNS1_4NodeEiNS1_7UseInfoENS1_4TypeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler22RepresentationSelector12ConvertInputEPNS1_4NodeEiNS1_7UseInfoENS1_4TypeE
	.type	_ZN2v88internal8compiler22RepresentationSelector12ConvertInputEPNS1_4NodeEiNS1_7UseInfoENS1_4TypeE, @function
_ZN2v88internal8compiler22RepresentationSelector12ConvertInputEPNS1_4NodeEiNS1_7UseInfoENS1_4TypeE:
.LFB23013:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movslq	%edx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r13, %r12
	salq	$3, %r13
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movl	20(%rsi), %esi
	movzbl	16(%rbp), %eax
	movq	%rcx, -56(%rbp)
	leaq	32(%rbx), %rcx
	movl	%esi, %edx
	movq	%rcx, -64(%rbp)
	xorl	$251658240, %edx
	andl	$251658240, %edx
	leaq	(%rcx,%r13), %rdx
	jne	.L497
	movq	32(%rbx), %rdx
	leaq	16(%rdx,%r13), %rdx
.L497:
	testb	%al, %al
	je	.L495
	movq	(%rdx), %r14
	movl	20(%r14), %r8d
	andl	$16777215, %r8d
	movl	%r8d, %edx
	leaq	(%rdx,%rdx,4), %rcx
	movq	32(%r15), %rdx
	leaq	(%rdx,%rcx,8), %rdi
	movq	%rdi, -80(%rbp)
	cmpb	1(%rdi), %al
	je	.L518
.L500:
	movq	-56(%rbp), %rax
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	movq	%rax, -72(%rbp)
	jne	.L519
.L510:
	pushq	40(%rbp)
	movq	%r15, %rdi
	pushq	32(%rbp)
	pushq	24(%rbp)
	pushq	16(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12PrintUseInfoENS1_7UseInfoE
	addq	$32, %rsp
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	jne	.L520
	cmpq	$0, -56(%rbp)
	jne	.L506
.L521:
	movl	20(%r14), %eax
	movq	32(%r15), %rdx
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rax
	leaq	(%rdx,%rax,8), %rax
	movq	24(%rax), %rax
	movq	%rax, -72(%rbp)
	testq	%rax, %rax
	jne	.L506
	movq	8(%r14), %rax
	movq	%rax, -72(%rbp)
.L506:
	movq	-80(%rbp), %rax
	movq	-72(%rbp), %rcx
	movq	%rbx, %r8
	movq	%r14, %rsi
	movq	128(%r15), %rdi
	movzbl	1(%rax), %edx
	pushq	40(%rbp)
	pushq	32(%rbp)
	pushq	24(%rbp)
	pushq	16(%rbp)
	call	_ZN2v88internal8compiler21RepresentationChanger20GetRepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE@PLT
	addq	$32, %rsp
	movq	%rax, %r15
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L507
	addq	-64(%rbp), %r13
	movq	0(%r13), %rdi
	cmpq	%rdi, %r15
	je	.L495
.L508:
	notl	%r12d
	movslq	%r12d, %r12
	leaq	(%r12,%r12,2), %rax
	leaq	(%rbx,%rax,8), %r12
	testq	%rdi, %rdi
	je	.L509
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L509:
	movq	%r15, 0(%r13)
	testq	%r15, %r15
	je	.L495
	leaq	-40(%rbp), %rsp
	movq	%r12, %rsi
	movq	%r15, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	.p2align 4,,10
	.p2align 3
.L518:
	.cfi_restore_state
	cmpb	$0, 28(%rbp)
	jne	.L500
.L495:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L507:
	.cfi_restore_state
	movq	-64(%rbp), %rax
	movq	(%rax), %rbx
	leaq	16(%rbx,%r13), %r13
	movq	0(%r13), %rdi
	cmpq	%rdi, %r15
	jne	.L508
	jmp	.L495
	.p2align 4,,10
	.p2align 3
.L519:
	movq	(%r14), %rax
	movq	(%rbx), %rdx
	andl	$16777215, %esi
	movl	%r12d, %ecx
	leaq	.LC13(%rip), %rdi
	movq	8(%rax), %r9
	movq	8(%rdx), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	je	.L510
	xorl	%eax, %eax
	leaq	.LC14(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	je	.L510
	movq	-80(%rbp), %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector15PrintOutputInfoEPNS2_8NodeInfoE.isra.0.part.0
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	je	.L510
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L510
	.p2align 4,,10
	.p2align 3
.L520:
	xorl	%eax, %eax
	leaq	.LC16(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	cmpq	$0, -56(%rbp)
	jne	.L506
	jmp	.L521
	.cfi_endproc
.LFE23013:
	.size	_ZN2v88internal8compiler22RepresentationSelector12ConvertInputEPNS1_4NodeEiNS1_7UseInfoENS1_4TypeE, .-_ZN2v88internal8compiler22RepresentationSelector12ConvertInputEPNS1_4NodeEiNS1_7UseInfoENS1_4TypeE
	.section	.text._ZN2v88internal8compiler18SimplifiedLoweringC2EPNS1_7JSGraphEPNS1_12JSHeapBrokerEPNS0_4ZoneEPNS1_19SourcePositionTableEPNS1_15NodeOriginTableENS0_24PoisoningMitigationLevelEPNS0_11TickCounterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18SimplifiedLoweringC2EPNS1_7JSGraphEPNS1_12JSHeapBrokerEPNS0_4ZoneEPNS1_19SourcePositionTableEPNS1_15NodeOriginTableENS0_24PoisoningMitigationLevelEPNS0_11TickCounterE
	.type	_ZN2v88internal8compiler18SimplifiedLoweringC2EPNS1_7JSGraphEPNS1_12JSHeapBrokerEPNS0_4ZoneEPNS1_19SourcePositionTableEPNS1_15NodeOriginTableENS0_24PoisoningMitigationLevelEPNS0_11TickCounterE, @function
_ZN2v88internal8compiler18SimplifiedLoweringC2EPNS1_7JSGraphEPNS1_12JSHeapBrokerEPNS0_4ZoneEPNS1_19SourcePositionTableEPNS1_15NodeOriginTableENS0_24PoisoningMitigationLevelEPNS0_11TickCounterE:
.LFB23076:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %xmm0
	movq	%rdx, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%r9, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rcx, 16(%rdi)
	movups	%xmm0, (%rdi)
	call	_ZN2v88internal8compiler9TypeCache3GetEv@PLT
	movq	%r13, 80(%rbx)
	movq	%rax, 24(%rbx)
	movl	16(%rbp), %eax
	movq	%r12, 88(%rbx)
	movl	%eax, 96(%rbx)
	movq	24(%rbp), %rax
	movq	$0, 32(%rbx)
	movq	$0, 40(%rbx)
	movq	$0, 48(%rbx)
	movq	$0, 56(%rbx)
	movq	$0, 64(%rbx)
	movq	$0, 72(%rbx)
	movq	%rax, 104(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23076:
	.size	_ZN2v88internal8compiler18SimplifiedLoweringC2EPNS1_7JSGraphEPNS1_12JSHeapBrokerEPNS0_4ZoneEPNS1_19SourcePositionTableEPNS1_15NodeOriginTableENS0_24PoisoningMitigationLevelEPNS0_11TickCounterE, .-_ZN2v88internal8compiler18SimplifiedLoweringC2EPNS1_7JSGraphEPNS1_12JSHeapBrokerEPNS0_4ZoneEPNS1_19SourcePositionTableEPNS1_15NodeOriginTableENS0_24PoisoningMitigationLevelEPNS0_11TickCounterE
	.globl	_ZN2v88internal8compiler18SimplifiedLoweringC1EPNS1_7JSGraphEPNS1_12JSHeapBrokerEPNS0_4ZoneEPNS1_19SourcePositionTableEPNS1_15NodeOriginTableENS0_24PoisoningMitigationLevelEPNS0_11TickCounterE
	.set	_ZN2v88internal8compiler18SimplifiedLoweringC1EPNS1_7JSGraphEPNS1_12JSHeapBrokerEPNS0_4ZoneEPNS1_19SourcePositionTableEPNS1_15NodeOriginTableENS0_24PoisoningMitigationLevelEPNS0_11TickCounterE,_ZN2v88internal8compiler18SimplifiedLoweringC2EPNS1_7JSGraphEPNS1_12JSHeapBrokerEPNS0_4ZoneEPNS1_19SourcePositionTableEPNS1_15NodeOriginTableENS0_24PoisoningMitigationLevelEPNS0_11TickCounterE
	.section	.text._ZN2v88internal8compiler18SimplifiedLowering12Float64RoundEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18SimplifiedLowering12Float64RoundEPNS1_4NodeE
	.type	_ZN2v88internal8compiler18SimplifiedLowering12Float64RoundEPNS1_4NodeE, @function
_ZN2v88internal8compiler18SimplifiedLowering12Float64RoundEPNS1_4NodeE:
.LFB23084:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movsd	.LC17(%rip), %xmm0
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler12MachineGraph15Float64ConstantEd@PLT
	movsd	.LC18(%rip), %xmm0
	movq	(%rbx), %rdi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler12MachineGraph15Float64ConstantEd@PLT
	leaq	32(%r12), %rdx
	movq	%rax, -104(%rbp)
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L525
	movq	32(%r12), %rdx
	addq	$16, %rdx
.L525:
	movq	(%rbx), %rax
	movq	(%rdx), %r14
	leaq	-80(%rbp), %r12
	movq	16(%rax), %rdi
	movq	(%rax), %r13
	movq	%r14, -112(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder14Float64RoundUpEv@PLT
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movq	%r13, %rdi
	movq	%rdx, %rsi
	movl	$1, %edx
	movq	%r14, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -88(%rbp)
	movq	(%rbx), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler22MachineOperatorBuilder10Float64SubEv@PLT
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movl	$2, %edx
	movq	-88(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movhps	-96(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r13
	movq	(%rbx), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler22MachineOperatorBuilder10Float64SubEv@PLT
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movl	$2, %edx
	movq	-88(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r14, %rdi
	movhps	-104(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -96(%rbp)
	movq	(%rbx), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder22Float64LessThanOrEqualEv@PLT
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movl	$2, %edx
	movq	-96(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r14, %rdi
	movhps	-112(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	xorl	%edx, %edx
	movl	$13, %esi
	movq	%rax, -96(%rbp)
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6SelectENS0_21MachineRepresentationENS1_10BranchHintE@PLT
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movl	$3, %edx
	movq	-96(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movq	%r13, -64(%rbp)
	movhps	-88(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L529
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L529:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23084:
	.size	_ZN2v88internal8compiler18SimplifiedLowering12Float64RoundEPNS1_4NodeE, .-_ZN2v88internal8compiler18SimplifiedLowering12Float64RoundEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler18SimplifiedLowering11Float64SignEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18SimplifiedLowering11Float64SignEPNS1_4NodeE
	.type	_ZN2v88internal8compiler18SimplifiedLowering11Float64SignEPNS1_4NodeE, @function
_ZN2v88internal8compiler18SimplifiedLowering11Float64SignEPNS1_4NodeE:
.LFB23085:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movsd	.LC19(%rip), %xmm0
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler12MachineGraph15Float64ConstantEd@PLT
	movq	(%rbx), %rdi
	pxor	%xmm0, %xmm0
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal8compiler12MachineGraph15Float64ConstantEd@PLT
	movsd	.LC17(%rip), %xmm0
	movq	(%rbx), %rdi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler12MachineGraph15Float64ConstantEd@PLT
	leaq	32(%r12), %rdx
	movq	%rax, -120(%rbp)
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L531
	movq	32(%r12), %rdx
	addq	$16, %rdx
.L531:
	movq	(%rdx), %rax
	leaq	-80(%rbp), %r12
	movq	%rax, -88(%rbp)
	movq	(%rbx), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler22MachineOperatorBuilder15Float64LessThanEv@PLT
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movl	$2, %edx
	movq	-96(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r13, %rdi
	movhps	-88(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	xorl	%edx, %edx
	movl	$13, %esi
	movq	%rax, -104(%rbp)
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6SelectENS0_21MachineRepresentationENS1_10BranchHintE@PLT
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movl	$3, %edx
	movq	-104(%rbp), %xmm0
	movq	%rax, %rsi
	movq	-88(%rbp), %rax
	movq	%r13, %rdi
	movhps	-120(%rbp), %xmm0
	movq	%rax, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r14
	movq	(%rbx), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler22MachineOperatorBuilder15Float64LessThanEv@PLT
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movl	$2, %edx
	movq	-88(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movhps	-96(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	xorl	%edx, %edx
	movl	$13, %esi
	movq	%rax, -88(%rbp)
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6SelectENS0_21MachineRepresentationENS1_10BranchHintE@PLT
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movl	$3, %edx
	movq	-88(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r13, %rdi
	movq	%r14, -64(%rbp)
	movhps	-112(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L535
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L535:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23085:
	.size	_ZN2v88internal8compiler18SimplifiedLowering11Float64SignEPNS1_4NodeE, .-_ZN2v88internal8compiler18SimplifiedLowering11Float64SignEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler18SimplifiedLowering8Int32AbsEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18SimplifiedLowering8Int32AbsEPNS1_4NodeE
	.type	_ZN2v88internal8compiler18SimplifiedLowering8Int32AbsEPNS1_4NodeE, @function
_ZN2v88internal8compiler18SimplifiedLowering8Int32AbsEPNS1_4NodeE:
.LFB23086:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	32(%rsi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L537
	movq	16(%r12), %r12
.L537:
	movq	(%rbx), %rdi
	movl	$31, %esi
	leaq	-64(%rbp), %r13
	movq	(%rdi), %r14
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%rax, -72(%rbp)
	movq	(%rbx), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32SarEv@PLT
	movq	%r12, %xmm0
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movhps	-72(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movq	%r14, %rdi
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -72(%rbp)
	movq	(%rbx), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32XorEv@PLT
	movq	%r12, %xmm0
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movhps	-72(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movq	%r14, %rdi
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -80(%rbp)
	movq	(%rbx), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Int32SubEv@PLT
	xorl	%r8d, %r8d
	movl	$2, %edx
	movq	%r13, %rcx
	movq	-80(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r14, %rdi
	movhps	-72(%rbp), %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L540
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L540:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23086:
	.size	_ZN2v88internal8compiler18SimplifiedLowering8Int32AbsEPNS1_4NodeE, .-_ZN2v88internal8compiler18SimplifiedLowering8Int32AbsEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler18SimplifiedLowering8Int32DivEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18SimplifiedLowering8Int32DivEPNS1_4NodeE
	.type	_ZN2v88internal8compiler18SimplifiedLowering8Int32DivEPNS1_4NodeE, @function
_ZN2v88internal8compiler18SimplifiedLowering8Int32DivEPNS1_4NodeE:
.LFB23087:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	32(%rsi), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$168, %rsp
	movq	32(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L542
	movq	0(%r13), %rax
	cmpw	$23, 16(%rax)
	je	.L543
	xorl	%esi, %esi
	xorl	%r14d, %r14d
.L544:
	leaq	8(%rcx), %rax
.L547:
	movq	(%rax), %r15
	movq	(%r15), %rdx
	cmpw	$23, 16(%rdx)
	jne	.L548
	movq	(%rbx), %rdi
	xorl	%esi, %esi
	movl	44(%rdx), %r14d
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	(%rbx), %rdi
	movl	$-1, %esi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
.L549:
	cmpl	$-1, %r14d
	je	.L576
	testl	%r14d, %r14d
	je	.L541
	movq	(%rbx), %rdx
	movq	16(%rdx), %rdi
.L558:
	movq	(%rdx), %r12
	movq	8(%r12), %rbx
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Int32DivEv@PLT
	movq	%r15, %xmm4
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movq	%r13, %xmm0
	movq	%rax, %rsi
	leaq	-80(%rbp), %rcx
	movq	%rbx, -64(%rbp)
	punpcklqdq	%xmm4, %xmm0
	movl	$3, %edx
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r15
.L541:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L577
	addq	$168, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L543:
	.cfi_restore_state
	movl	44(%rax), %r14d
	movl	$1, %esi
	jmp	.L544
	.p2align 4,,10
	.p2align 3
.L548:
	movq	(%r12), %rax
	testb	$1, 18(%rax)
	je	.L550
	testb	%sil, %sil
	jne	.L578
.L550:
	movq	(%rbx), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	(%rbx), %rdi
	movl	$-1, %esi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	(%rbx), %rdx
	movq	%rax, -112(%rbp)
	movq	16(%rdx), %rdi
	testb	$2, 21(%rdi)
	jne	.L558
	movq	-96(%rbp), %xmm6
	movq	%r13, %xmm7
	movq	8(%rdx), %rdi
	movq	%r15, %xmm2
	movq	%r13, %xmm5
	movq	%r15, %xmm3
	movl	$2, %esi
	movdqa	%xmm6, %xmm1
	movdqa	%xmm6, %xmm0
	punpcklqdq	%xmm6, %xmm2
	punpcklqdq	%xmm7, %xmm1
	movq	%r15, %xmm7
	movhps	-112(%rbp), %xmm3
	movaps	%xmm2, -192(%rbp)
	punpcklqdq	%xmm7, %xmm5
	punpcklqdq	%xmm7, %xmm0
	leaq	-80(%rbp), %r12
	movaps	%xmm1, -208(%rbp)
	movaps	%xmm3, -160(%rbp)
	movaps	%xmm5, -144(%rbp)
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
	movl	$2, %edx
	movl	$4, %esi
	movq	%rax, %r14
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	movq	%rax, %r13
	movq	(%rbx), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler22MachineOperatorBuilder13Int32LessThanEv@PLT
	movdqa	-112(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movq	%rax, %rsi
	movq	%r12, %rcx
	movl	$2, %edx
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$1, %edx
	movl	$1, %esi
	movq	%rax, -120(%rbp)
	movq	(%rbx), %rax
	movq	(%rax), %r15
	movq	8(%rax), %rdi
	movq	8(%r15), %rcx
	movq	%rcx, -112(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movl	$2, %edx
	movq	-120(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movhps	-112(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r15
	movq	(%rbx), %rax
	movq	(%rax), %r9
	movq	8(%rax), %rdi
	movq	%r9, -112(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	movq	-112(%rbp), %r9
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r15, -80(%rbp)
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -120(%rbp)
	movq	(%rbx), %rax
	movq	(%rax), %r9
	movq	16(%rax), %rdi
	movq	%r9, -112(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Int32DivEv@PLT
	movq	-112(%rbp), %r9
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movdqa	-144(%rbp), %xmm5
	movq	%rax, %rsi
	movq	-120(%rbp), %rax
	movl	$3, %edx
	movq	%r9, %rdi
	movaps	%xmm5, -80(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -176(%rbp)
	movq	(%rbx), %rax
	movq	(%rax), %r9
	movq	8(%rax), %rdi
	movq	%r9, -112(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	movq	-112(%rbp), %r9
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r15, -80(%rbp)
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -128(%rbp)
	movq	(%rbx), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler22MachineOperatorBuilder13Int32LessThanEv@PLT
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movq	%r15, %rdi
	movdqa	-160(%rbp), %xmm3
	movq	%rax, %rsi
	movl	$2, %edx
	movaps	%xmm3, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%rax, -112(%rbp)
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movl	$2, %edx
	movq	-112(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movhps	-128(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r15
	movq	(%rbx), %rax
	movq	(%rax), %r9
	movq	8(%rax), %rdi
	movq	%r9, -112(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	movq	-112(%rbp), %r9
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r15, -80(%rbp)
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -112(%rbp)
	movq	(%rbx), %rax
	movq	(%rax), %r9
	movq	16(%rax), %rdi
	movq	%r9, -128(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Int32DivEv@PLT
	movq	-128(%rbp), %r9
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movdqa	-144(%rbp), %xmm5
	movq	%rax, %rsi
	movq	-112(%rbp), %rax
	movl	$3, %edx
	movq	%r9, %rdi
	movaps	%xmm5, -80(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -168(%rbp)
	movq	(%rbx), %rax
	movq	(%rax), %r9
	movq	8(%rax), %rdi
	movq	%r9, -144(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movl	$1, %edx
	movq	-144(%rbp), %r9
	movq	%rax, %rsi
	movq	%r15, -80(%rbp)
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -128(%rbp)
	movq	(%rbx), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler22MachineOperatorBuilder11Word32EqualEv@PLT
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movq	%r15, %rdi
	movdqa	-192(%rbp), %xmm2
	movq	%rax, %rsi
	movl	$2, %edx
	movaps	%xmm2, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$1, %edx
	xorl	%esi, %esi
	movq	%rax, -144(%rbp)
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movl	$2, %edx
	movq	-144(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movhps	-128(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r15
	movq	(%rbx), %rax
	movq	(%rax), %r9
	movq	8(%rax), %rdi
	movq	%r9, -144(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movl	$1, %edx
	movq	-144(%rbp), %r9
	movq	%rax, %rsi
	movq	%r15, -80(%rbp)
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -160(%rbp)
	movq	(%rbx), %rax
	movq	(%rax), %r9
	movq	8(%rax), %rdi
	movq	%r9, -144(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movl	$1, %edx
	movq	-144(%rbp), %r9
	movq	%rax, %rsi
	movq	%r15, -80(%rbp)
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -144(%rbp)
	movq	(%rbx), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Int32SubEv@PLT
	movq	%r15, %rdi
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movdqa	-208(%rbp), %xmm1
	movq	%rax, %rsi
	movl	$2, %edx
	movaps	%xmm1, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movl	$2, %edx
	movq	-160(%rbp), %xmm0
	movq	%rax, -128(%rbp)
	movq	%r14, %rsi
	movq	(%rbx), %rax
	movhps	-144(%rbp), %xmm0
	movq	(%rax), %rdi
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movl	$3, %edx
	movq	%rax, %r15
	movq	(%rbx), %rax
	movq	-96(%rbp), %xmm0
	movq	%r13, %rsi
	movq	(%rax), %rdi
	movhps	-128(%rbp), %xmm0
	movq	%r15, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r15, %xmm6
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movq	%rax, -96(%rbp)
	movq	(%rbx), %rax
	movl	$2, %edx
	movq	%r14, %rsi
	movq	-112(%rbp), %xmm0
	movq	(%rax), %rdi
	punpcklqdq	%xmm6, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movl	$3, %edx
	movq	-168(%rbp), %xmm0
	movq	%rax, %r15
	movq	(%rbx), %rax
	movq	%r13, %rsi
	movq	(%rax), %rdi
	movhps	-96(%rbp), %xmm0
	movq	%r15, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r15, %xmm4
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movq	%rax, -96(%rbp)
	movq	(%rbx), %rax
	movl	$2, %edx
	movq	%r14, %rsi
	movq	-120(%rbp), %xmm0
	movq	(%rax), %rdi
	punpcklqdq	%xmm4, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	(%rbx), %rdx
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movq	-176(%rbp), %xmm0
	movq	%r13, %rsi
	movq	(%rdx), %rdi
	movl	$3, %edx
	movq	%rax, -64(%rbp)
	movhps	-96(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r15
	jmp	.L541
	.p2align 4,,10
	.p2align 3
.L542:
	leaq	16(%r13), %rax
	movq	16(%r13), %r13
	movq	0(%r13), %rdx
	cmpw	$23, 16(%rdx)
	je	.L545
	xorl	%esi, %esi
	xorl	%r14d, %r14d
.L546:
	addq	$8, %rax
	jmp	.L547
	.p2align 4,,10
	.p2align 3
.L545:
	movl	44(%rdx), %r14d
	movl	$1, %esi
	jmp	.L546
	.p2align 4,,10
	.p2align 3
.L576:
	movq	(%rbx), %rax
	movq	%r13, %xmm7
	movq	%r12, %xmm0
	punpcklqdq	%xmm7, %xmm0
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Int32SubEv@PLT
	movdqa	-96(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movq	%rax, %rsi
	leaq	-80(%rbp), %rcx
	movl	$2, %edx
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r15
	jmp	.L541
	.p2align 4,,10
	.p2align 3
.L578:
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rcx, -96(%rbp)
	call	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.0
	movzbl	23(%r12), %eax
	movq	-96(%rbp), %rcx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L551
	movq	8(%rcx), %rdi
	addq	$8, %rcx
	cmpq	%r13, %rdi
	je	.L552
.L553:
	subq	$48, %r12
	testq	%rdi, %rdi
	je	.L554
	movq	%r12, %rsi
	movq	%rcx, -96(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-96(%rbp), %rcx
.L554:
	movq	%r13, (%rcx)
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L552:
	movq	(%rbx), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	(%rbx), %rdi
	movl	$-1, %esi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%r15, %rax
	movq	%r13, %r15
	movq	%rax, %r13
	jmp	.L549
	.p2align 4,,10
	.p2align 3
.L551:
	movq	(%rcx), %r12
	movq	24(%r12), %rdi
	cmpq	%r13, %rdi
	je	.L552
	leaq	24(%r12), %rcx
	jmp	.L553
.L577:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23087:
	.size	_ZN2v88internal8compiler18SimplifiedLowering8Int32DivEPNS1_4NodeE, .-_ZN2v88internal8compiler18SimplifiedLowering8Int32DivEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler18SimplifiedLowering8Int32ModEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18SimplifiedLowering8Int32ModEPNS1_4NodeE
	.type	_ZN2v88internal8compiler18SimplifiedLowering8Int32ModEPNS1_4NodeE, @function
_ZN2v88internal8compiler18SimplifiedLowering8Int32ModEPNS1_4NodeE:
.LFB23088:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$184, %rsp
	movzbl	23(%rsi), %edx
	movq	32(%rsi), %rcx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	andl	$15, %edx
	movq	%rcx, -96(%rbp)
	cmpl	$15, %edx
	je	.L580
	movq	(%rcx), %rdx
	leaq	32(%rsi), %rax
	cmpw	$23, 16(%rdx)
	je	.L581
	xorl	%r14d, %r14d
	xorl	%edx, %edx
.L582:
	addq	$8, %rax
.L585:
	movq	(%rax), %rax
	movq	%rax, -104(%rbp)
	movq	(%rax), %rax
	cmpw	$23, 16(%rax)
	jne	.L586
	movq	(%rbx), %rdi
	xorl	%esi, %esi
	movl	44(%rax), %r14d
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	(%rbx), %rdi
	movl	$-1, %esi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
.L587:
	addl	$1, %r14d
	cmpl	$1, %r14d
	jbe	.L579
	movq	(%rbx), %rax
	movq	-96(%rbp), %xmm0
	movq	(%rax), %r12
	movq	16(%rax), %rdi
	movhps	-104(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	8(%r12), %rbx
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Int32ModEv@PLT
	movdqa	-96(%rbp), %xmm0
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	leaq	-80(%rbp), %rcx
	movl	$3, %edx
	movq	%rbx, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r12
.L579:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L598
	addq	$184, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L581:
	.cfi_restore_state
	movl	44(%rdx), %r14d
	movl	$1, %edx
	jmp	.L582
	.p2align 4,,10
	.p2align 3
.L586:
	movq	(%r12), %rax
	testb	$1, 18(%rax)
	je	.L588
	testb	%dl, %dl
	jne	.L599
.L588:
	movq	(%rbx), %rdi
	xorl	%esi, %esi
	leaq	-80(%rbp), %r12
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	(%rbx), %rdi
	movl	$-1, %esi
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	-104(%rbp), %rcx
	movq	-96(%rbp), %rdx
	movl	$2, %esi
	movq	-112(%rbp), %xmm5
	movq	%rcx, %xmm7
	movq	%rdx, %xmm3
	movq	%rdx, %xmm6
	punpcklqdq	%xmm7, %xmm3
	movq	%rax, %xmm7
	movq	(%rbx), %rax
	movdqa	%xmm5, %xmm1
	movdqa	%xmm5, %xmm0
	movq	%rcx, %xmm4
	movq	%rdx, %xmm2
	movaps	%xmm3, -160(%rbp)
	movq	8(%rax), %rdi
	punpcklqdq	%xmm7, %xmm4
	punpcklqdq	%xmm6, %xmm1
	punpcklqdq	%xmm5, %xmm2
	movhps	-104(%rbp), %xmm0
	movaps	%xmm1, -224(%rbp)
	movaps	%xmm2, -208(%rbp)
	movaps	%xmm4, -176(%rbp)
	movaps	%xmm0, -128(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
	movl	$2, %edx
	movl	$4, %esi
	movq	%rax, %r15
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	movq	%rax, %r14
	movq	(%rbx), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler22MachineOperatorBuilder13Int32LessThanEv@PLT
	movdqa	-128(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movq	%r13, %rdi
	movq	%rax, %rsi
	movl	$2, %edx
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$1, %edx
	movq	%rax, -136(%rbp)
	movq	(%rbx), %rax
	movq	(%rax), %r13
	movq	8(%rax), %rdi
	movq	8(%r13), %rsi
	movq	%rsi, -128(%rbp)
	movl	$1, %esi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movl	$2, %edx
	movq	-136(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r13, %rdi
	movhps	-128(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -144(%rbp)
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	-144(%rbp), %rax
	movq	%r13, %rdi
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -136(%rbp)
	movq	(%rbx), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Int32AddEv@PLT
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movl	$2, %edx
	movdqa	-176(%rbp), %xmm4
	movq	%rax, %rsi
	movq	%r13, %rdi
	movaps	%xmm4, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -128(%rbp)
	movq	(%rbx), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32AndEv@PLT
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movq	%r13, %rdi
	movq	-104(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movhps	-128(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%rax, -104(%rbp)
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movl	$2, %edx
	movq	-104(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r13, %rdi
	movhps	-136(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r13
	movq	(%rbx), %rax
	movq	(%rax), %r9
	movq	8(%rax), %rdi
	movq	%r9, -104(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	movq	-104(%rbp), %r9
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r13, -80(%rbp)
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -136(%rbp)
	movq	(%rbx), %rax
	movq	(%rax), %r9
	movq	16(%rax), %rdi
	movq	%r9, -104(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Int32ModEv@PLT
	movq	-136(%rbp), %rcx
	movq	-104(%rbp), %r9
	xorl	%r8d, %r8d
	movdqa	-160(%rbp), %xmm3
	movq	%rax, %rsi
	movl	$3, %edx
	movq	%r9, %rdi
	movq	%rcx, -64(%rbp)
	movq	%r12, %rcx
	movaps	%xmm3, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -192(%rbp)
	movq	(%rbx), %rax
	movq	(%rax), %r9
	movq	8(%rax), %rdi
	movq	%r9, -104(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	movq	-104(%rbp), %r9
	movq	%r12, %rcx
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r13, -80(%rbp)
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -184(%rbp)
	movq	(%rbx), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler22MachineOperatorBuilder13Int32LessThanEv@PLT
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movq	%r13, %rdi
	movdqa	-208(%rbp), %xmm2
	movq	%rax, %rsi
	movl	$2, %edx
	movaps	%xmm2, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$1, %edx
	movl	$2, %esi
	movq	%rax, -104(%rbp)
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movl	$2, %edx
	movq	-104(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r13, %rdi
	movhps	-184(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -104(%rbp)
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	movq	-104(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%rcx, -80(%rbp)
	movq	%r12, %rcx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -208(%rbp)
	movq	(%rbx), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Int32SubEv@PLT
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movl	$2, %edx
	movdqa	-224(%rbp), %xmm1
	movq	%rax, %rsi
	movq	%r13, %rdi
	movaps	%xmm1, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -184(%rbp)
	movq	(%rbx), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32AndEv@PLT
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movl	$2, %edx
	movq	-184(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r13, %rdi
	movhps	-128(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -184(%rbp)
	movq	(%rbx), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Int32SubEv@PLT
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movl	$2, %edx
	movq	-112(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r13, %rdi
	movhps	-184(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -184(%rbp)
	movq	(%rbx), %rax
	movq	(%rax), %r9
	movq	8(%rax), %rdi
	movq	%r9, -224(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	movq	-104(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	-224(%rbp), %r9
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r9, %rdi
	movq	%rcx, -80(%rbp)
	movq	%r12, %rcx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -104(%rbp)
	movq	(%rbx), %rax
	movq	(%rax), %r9
	movq	16(%rax), %rdi
	movq	%r9, -224(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32AndEv@PLT
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movl	$2, %edx
	movq	-224(%rbp), %r9
	movq	-96(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r9, %rdi
	movhps	-128(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movl	$2, %edx
	movq	-208(%rbp), %xmm0
	movq	%rax, -96(%rbp)
	movq	%r15, %rsi
	movq	(%rbx), %rax
	movhps	-104(%rbp), %xmm0
	movq	(%rax), %rdi
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movl	$3, %edx
	movq	-184(%rbp), %xmm0
	movq	%rax, %r13
	movq	(%rbx), %rax
	movq	%r14, %rsi
	movq	(%rax), %rdi
	movhps	-96(%rbp), %xmm0
	movq	%r13, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r13, %xmm7
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movq	%rax, -96(%rbp)
	movq	(%rbx), %rax
	movl	$2, %edx
	movq	%r15, %rsi
	movq	-136(%rbp), %xmm0
	movq	(%rax), %rdi
	punpcklqdq	%xmm7, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r12, %rcx
	xorl	%r8d, %r8d
	movl	$3, %edx
	movq	%rax, %xmm6
	movq	%rax, -104(%rbp)
	movq	(%rbx), %rax
	movq	%r14, %rsi
	movq	-192(%rbp), %xmm0
	movq	(%rax), %rdi
	movq	%xmm6, -64(%rbp)
	movhps	-96(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -128(%rbp)
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	movq	%r12, %rcx
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%rax, %rsi
	movq	-144(%rbp), %rax
	movq	%r13, %rdi
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -96(%rbp)
	movq	(%rbx), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler22MachineOperatorBuilder13Int32LessThanEv@PLT
	movq	%r12, %rcx
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	movdqa	-176(%rbp), %xmm4
	movq	%rax, %rsi
	movl	$2, %edx
	movaps	%xmm4, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$1, %edx
	movl	$1, %esi
	movq	%rax, -136(%rbp)
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	movq	%r12, %rcx
	xorl	%r8d, %r8d
	movl	$2, %edx
	movq	-136(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r13, %rdi
	movhps	-96(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r13
	movq	(%rbx), %rax
	movq	(%rax), %r9
	movq	8(%rax), %rdi
	movq	%r9, -96(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	movq	-96(%rbp), %r9
	movq	%r12, %rcx
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r13, -80(%rbp)
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -96(%rbp)
	movq	(%rbx), %rax
	movq	(%rax), %r9
	movq	16(%rax), %rdi
	movq	%r9, -136(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Int32ModEv@PLT
	movq	%r12, %rcx
	xorl	%r8d, %r8d
	movl	$3, %edx
	movq	-136(%rbp), %r9
	movdqa	-160(%rbp), %xmm3
	movq	%rax, %rsi
	movq	-96(%rbp), %rax
	movq	%r9, %rdi
	movaps	%xmm3, -80(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -136(%rbp)
	movq	(%rbx), %rax
	movq	(%rax), %r9
	movq	8(%rax), %rdi
	movq	%r9, -144(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	movq	%r12, %rcx
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	-144(%rbp), %r9
	movq	%rax, %rsi
	movq	%r13, -80(%rbp)
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	(%rbx), %rdx
	movq	%r12, %rcx
	xorl	%r8d, %r8d
	movq	-96(%rbp), %xmm0
	movq	%rax, %xmm6
	movq	%r15, %rsi
	movq	(%rdx), %rdi
	movl	$2, %edx
	punpcklqdq	%xmm6, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r12, %rcx
	xorl	%r8d, %r8d
	movl	$3, %edx
	movq	-136(%rbp), %xmm0
	movq	%rax, %r13
	movq	(%rbx), %rax
	movq	%r14, %rsi
	movq	(%rax), %rdi
	movhps	-112(%rbp), %xmm0
	movq	%r13, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r13, %xmm7
	movq	%r12, %rcx
	xorl	%r8d, %r8d
	movq	%rax, -96(%rbp)
	movq	(%rbx), %rax
	movl	$2, %edx
	movq	%r15, %rsi
	movq	-104(%rbp), %xmm0
	movq	(%rax), %rdi
	punpcklqdq	%xmm7, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	(%rbx), %rdx
	movq	%r12, %rcx
	xorl	%r8d, %r8d
	movq	-128(%rbp), %xmm0
	movq	%r14, %rsi
	movq	(%rdx), %rdi
	movl	$3, %edx
	movq	%rax, -64(%rbp)
	movhps	-96(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r12
	jmp	.L579
	.p2align 4,,10
	.p2align 3
.L580:
	movq	16(%rcx), %r13
	leaq	16(%rcx), %rax
	movq	0(%r13), %rdx
	cmpw	$23, 16(%rdx)
	je	.L583
	xorl	%r14d, %r14d
	xorl	%edx, %edx
.L584:
	movq	%r13, -96(%rbp)
	addq	$8, %rax
	jmp	.L585
	.p2align 4,,10
	.p2align 3
.L583:
	movl	44(%rdx), %r14d
	movl	$1, %edx
	jmp	.L584
	.p2align 4,,10
	.p2align 3
.L599:
	movq	-104(%rbp), %r15
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.0
	movq	-96(%rbp), %r13
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%r13, %rdx
	call	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_
	movq	(%rbx), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	(%rbx), %rdi
	movl	$-1, %esi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%r15, -96(%rbp)
	movq	%r13, -104(%rbp)
	jmp	.L587
.L598:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23088:
	.size	_ZN2v88internal8compiler18SimplifiedLowering8Int32ModEPNS1_4NodeE, .-_ZN2v88internal8compiler18SimplifiedLowering8Int32ModEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler18SimplifiedLowering9Int32SignEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18SimplifiedLowering9Int32SignEPNS1_4NodeE
	.type	_ZN2v88internal8compiler18SimplifiedLowering9Int32SignEPNS1_4NodeE, @function
_ZN2v88internal8compiler18SimplifiedLowering9Int32SignEPNS1_4NodeE:
.LFB23089:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movl	$-1, %esi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	(%rbx), %rdi
	movl	$1, %esi
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	leaq	32(%r12), %rdx
	movq	%rax, -120(%rbp)
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L601
	movq	32(%r12), %rdx
	addq	$16, %rdx
.L601:
	movq	(%rdx), %rax
	leaq	-80(%rbp), %r12
	movq	%rax, -96(%rbp)
	movq	(%rbx), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler22MachineOperatorBuilder13Int32LessThanEv@PLT
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movl	$2, %edx
	movq	-88(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r14, %rdi
	movhps	-96(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	xorl	%edx, %edx
	movl	$4, %esi
	movq	%rax, -104(%rbp)
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6SelectENS0_21MachineRepresentationENS1_10BranchHintE@PLT
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movl	$3, %edx
	movq	-104(%rbp), %xmm0
	movq	%rax, %rsi
	movq	-88(%rbp), %rax
	movq	%r14, %rdi
	movhps	-120(%rbp), %xmm0
	movq	%rax, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r15
	movq	(%rbx), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler22MachineOperatorBuilder13Int32LessThanEv@PLT
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movl	$2, %edx
	movq	-96(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r13, %rdi
	movhps	-88(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	xorl	%edx, %edx
	movl	$4, %esi
	movq	%rax, -88(%rbp)
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6SelectENS0_21MachineRepresentationENS1_10BranchHintE@PLT
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movl	$3, %edx
	movq	-88(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r14, %rdi
	movq	%r15, -64(%rbp)
	movhps	-112(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L605
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L605:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23089:
	.size	_ZN2v88internal8compiler18SimplifiedLowering9Int32SignEPNS1_4NodeE, .-_ZN2v88internal8compiler18SimplifiedLowering9Int32SignEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler18SimplifiedLowering9Uint32DivEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18SimplifiedLowering9Uint32DivEPNS1_4NodeE
	.type	_ZN2v88internal8compiler18SimplifiedLowering9Uint32DivEPNS1_4NodeE, @function
_ZN2v88internal8compiler18SimplifiedLowering9Uint32DivEPNS1_4NodeE:
.LFB23090:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	32(%rsi), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$88, %rsp
	movq	32(%rsi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L607
	movq	(%r12), %rax
	cmpw	$23, 16(%rax)
	je	.L608
	xorl	%r15d, %r15d
	xorl	%ecx, %ecx
.L609:
	leaq	8(%rdx), %rax
.L612:
	movq	(%rax), %r14
	movq	(%r14), %rax
	cmpw	$23, 16(%rax)
	jne	.L613
	movq	0(%r13), %rdi
	xorl	%esi, %esi
	movl	44(%rax), %r15d
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
.L614:
	testl	%r15d, %r15d
	je	.L606
	movq	0(%r13), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r15
.L622:
	movq	8(%r15), %rbx
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Uint32DivEv@PLT
	movq	%r12, %xmm0
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movq	%r14, %xmm2
	movq	%rax, %rsi
	leaq	-80(%rbp), %rcx
	movq	%rbx, -64(%rbp)
	punpcklqdq	%xmm2, %xmm0
	movl	$3, %edx
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
.L606:
	movq	-56(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L640
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L608:
	.cfi_restore_state
	movl	44(%rax), %r15d
	movl	$1, %ecx
	jmp	.L609
	.p2align 4,,10
	.p2align 3
.L613:
	movq	(%rbx), %rax
	testb	$1, 18(%rax)
	je	.L615
	testb	%cl, %cl
	jne	.L641
.L615:
	movq	0(%r13), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%rax, -88(%rbp)
	movq	0(%r13), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r15
	testb	$4, 21(%rdi)
	jne	.L622
	movq	%r14, %xmm3
	movq	%r12, %xmm1
	movq	%r14, %xmm0
	punpcklqdq	%xmm3, %xmm1
	movhps	-88(%rbp), %xmm0
	leaq	-80(%rbp), %r12
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder11Word32EqualEv@PLT
	movdqa	-112(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movq	%r15, %rdi
	movq	%rax, %rsi
	movl	$2, %edx
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$1, %edx
	movl	$2, %esi
	movq	%rax, -112(%rbp)
	movq	0(%r13), %rax
	movq	(%rax), %r14
	movq	8(%rax), %r15
	movq	8(%r14), %rbx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movl	$2, %edx
	movq	-112(%rbp), %xmm0
	movq	%rbx, %xmm4
	movq	%rax, %rsi
	movq	%r14, %rdi
	punpcklqdq	%xmm4, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r14, %rdi
	movq	%rbx, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r15, %rdi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movl	$1, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	movq	%rbx, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r15, %rdi
	movl	$2, %esi
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movl	$2, %edx
	movq	-96(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r14, %rdi
	movhps	-112(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %rbx
	movq	0(%r13), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Uint32DivEv@PLT
	movdqa	-128(%rbp), %xmm1
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movq	%rax, %rsi
	movq	-112(%rbp), %rax
	movq	%r13, %rdi
	movl	$3, %edx
	movaps	%xmm1, -80(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r15, %rdi
	movl	$2, %edx
	movl	$4, %esi
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movl	$3, %edx
	movq	-88(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r14, %rdi
	movq	%rbx, -64(%rbp)
	movhps	-112(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	jmp	.L606
	.p2align 4,,10
	.p2align 3
.L607:
	leaq	16(%r12), %rax
	movq	16(%r12), %r12
	movq	(%r12), %rcx
	cmpw	$23, 16(%rcx)
	je	.L610
	xorl	%r15d, %r15d
	xorl	%ecx, %ecx
.L611:
	addq	$8, %rax
	jmp	.L612
	.p2align 4,,10
	.p2align 3
.L610:
	movl	44(%rcx), %r15d
	movl	$1, %ecx
	jmp	.L611
	.p2align 4,,10
	.p2align 3
.L641:
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	%rdx, -88(%rbp)
	call	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.0
	movzbl	23(%rbx), %eax
	movq	-88(%rbp), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L616
	movq	8(%rdx), %rdi
	addq	$8, %rdx
	cmpq	%r12, %rdi
	je	.L617
.L618:
	subq	$48, %rbx
	testq	%rdi, %rdi
	je	.L619
	movq	%rbx, %rsi
	movq	%rdx, -88(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-88(%rbp), %rdx
.L619:
	movq	%r12, (%rdx)
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L617:
	movq	0(%r13), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%r14, %rdx
	movq	%r12, %r14
	movq	%rdx, %r12
	jmp	.L614
	.p2align 4,,10
	.p2align 3
.L616:
	movq	(%rdx), %rbx
	movq	24(%rbx), %rdi
	cmpq	%r12, %rdi
	je	.L617
	leaq	24(%rbx), %rdx
	jmp	.L618
.L640:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23090:
	.size	_ZN2v88internal8compiler18SimplifiedLowering9Uint32DivEPNS1_4NodeE, .-_ZN2v88internal8compiler18SimplifiedLowering9Uint32DivEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler18SimplifiedLowering9Uint32ModEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18SimplifiedLowering9Uint32ModEPNS1_4NodeE
	.type	_ZN2v88internal8compiler18SimplifiedLowering9Uint32ModEPNS1_4NodeE, @function
_ZN2v88internal8compiler18SimplifiedLowering9Uint32ModEPNS1_4NodeE:
.LFB23091:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	32(%rsi), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$152, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %rsi
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L643
	movq	(%rsi), %rdx
	cmpw	$23, 16(%rdx)
	je	.L644
	xorl	%r15d, %r15d
	xorl	%edi, %edi
.L645:
	movq	%rsi, -96(%rbp)
	leaq	8(%rcx), %rdx
	movq	%rcx, %r8
.L648:
	movq	(%rdx), %r13
	movq	0(%r13), %rdx
	cmpw	$23, 16(%rdx)
	jne	.L649
	movq	(%rbx), %rdi
	movl	$-1, %esi
	movl	44(%rdx), %r15d
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	(%rbx), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
.L650:
	testl	%r15d, %r15d
	je	.L642
	movq	(%rbx), %rax
	movq	-96(%rbp), %xmm0
	movq	%r13, %xmm6
	movq	(%rax), %r12
	movq	16(%rax), %rdi
	punpcklqdq	%xmm6, %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	8(%r12), %rbx
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Uint32ModEv@PLT
	movdqa	-96(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movq	%rax, %rsi
	leaq	-80(%rbp), %rcx
	movl	$3, %edx
	movq	%rbx, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
.L642:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L676
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L644:
	.cfi_restore_state
	movl	44(%rdx), %r15d
	movl	$1, %edi
	jmp	.L645
	.p2align 4,,10
	.p2align 3
.L649:
	movq	(%r12), %rdx
	testb	$1, 18(%rdx)
	je	.L651
	testb	%dil, %dil
	jne	.L677
.L651:
	movq	(%rbx), %rdi
	movl	$-1, %esi
	leaq	-80(%rbp), %r15
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	-96(%rbp), %xmm1
	movq	%r13, %xmm4
	movq	%r13, %xmm2
	movq	%rax, %xmm3
	movq	%rax, -128(%rbp)
	movq	(%rbx), %rax
	movhps	-112(%rbp), %xmm2
	movq	%r13, %xmm0
	punpcklqdq	%xmm4, %xmm1
	movl	$2, %esi
	movaps	%xmm2, -160(%rbp)
	movq	8(%rax), %rdi
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm1, -192(%rbp)
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
	movl	$2, %edx
	movl	$4, %esi
	movq	%rax, -168(%rbp)
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	movq	%rax, %r14
	movq	(%rbx), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder11Word32EqualEv@PLT
	movdqa	-112(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movl	$2, %edx
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$1, %edx
	movl	$2, %esi
	movq	%rax, -120(%rbp)
	movq	(%rbx), %rax
	movq	(%rax), %r12
	movq	8(%rax), %rdi
	movq	8(%r12), %rcx
	movq	%rcx, -112(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$2, %edx
	movq	-120(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r12, %rdi
	movhps	-112(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r12
	movq	(%rbx), %rax
	movq	(%rax), %r11
	movq	8(%rax), %rdi
	movq	%r11, -112(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	movq	-112(%rbp), %r11
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r12, -80(%rbp)
	movq	%r11, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -144(%rbp)
	movq	(%rbx), %rax
	movq	(%rax), %r11
	movq	8(%rax), %rdi
	movq	%r11, -112(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	movq	-112(%rbp), %r11
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r12, -80(%rbp)
	movq	%r11, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -136(%rbp)
	movq	(%rbx), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Int32AddEv@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$2, %edx
	movdqa	-160(%rbp), %xmm2
	movq	%rax, %rsi
	movq	%r12, %rdi
	movaps	%xmm2, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -120(%rbp)
	movq	(%rbx), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32AndEv@PLT
	movq	%r13, %xmm0
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movhps	-120(%rbp), %xmm0
	movq	%r12, %rdi
	movq	%rax, %rsi
	movl	$2, %edx
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%rax, -112(%rbp)
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$2, %edx
	movq	-112(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r12, %rdi
	movhps	-136(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r12
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	movq	%r12, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -112(%rbp)
	movq	(%rbx), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Uint32ModEv@PLT
	movq	%r15, %rcx
	xorl	%r8d, %r8d
	movl	$3, %edx
	movdqa	-192(%rbp), %xmm1
	movq	%rax, %rsi
	movq	-112(%rbp), %rax
	movq	%r13, %rdi
	movaps	%xmm1, -80(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -160(%rbp)
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	movq	%r12, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -136(%rbp)
	movq	(%rbx), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32AndEv@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	-96(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movhps	-120(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$2, %edx
	movq	%rax, -96(%rbp)
	movq	(%rbx), %rax
	movq	-168(%rbp), %r10
	movq	-112(%rbp), %xmm0
	movq	(%rax), %rdi
	movhps	-136(%rbp), %xmm0
	movq	%r10, %rsi
	movq	%r10, -112(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%r14, %rsi
	movq	%rax, %r12
	movq	(%rbx), %rax
	movl	$3, %edx
	movq	-160(%rbp), %xmm0
	movq	(%rax), %rdi
	movhps	-96(%rbp), %xmm0
	movq	%r12, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-112(%rbp), %r10
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%rax, -96(%rbp)
	movq	(%rbx), %rax
	movq	%r12, %xmm5
	movl	$2, %edx
	movq	-144(%rbp), %xmm0
	movq	%r10, %rsi
	movq	(%rax), %rdi
	punpcklqdq	%xmm5, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	(%rbx), %rdx
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	-128(%rbp), %xmm0
	movq	%r14, %rsi
	movq	(%rdx), %rdi
	movl	$3, %edx
	movq	%rax, -64(%rbp)
	movhps	-96(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	jmp	.L642
	.p2align 4,,10
	.p2align 3
.L643:
	movq	16(%rsi), %rdx
	leaq	16(%rsi), %r8
	movq	%rdx, -96(%rbp)
	movq	(%rdx), %rdx
	cmpw	$23, 16(%rdx)
	je	.L646
	xorl	%r15d, %r15d
	xorl	%edi, %edi
.L647:
	leaq	8(%r8), %rdx
	jmp	.L648
	.p2align 4,,10
	.p2align 3
.L646:
	movl	44(%rdx), %r15d
	movl	$1, %edi
	jmp	.L647
	.p2align 4,,10
	.p2align 3
.L677:
	cmpl	$15, %eax
	je	.L652
	movq	%r12, %rsi
	cmpq	-96(%rbp), %r13
	je	.L654
.L653:
	movq	-96(%rbp), %rdi
	leaq	-24(%rsi), %r14
	movq	%rcx, -120(%rbp)
	movq	%r14, %rsi
	movq	%r8, -112(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-112(%rbp), %r8
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%r13, (%r8)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movzbl	23(%r12), %eax
	movq	-120(%rbp), %rcx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L678
.L654:
	movq	8(%rcx), %rdi
	leaq	8(%rcx), %r14
	cmpq	-96(%rbp), %rdi
	je	.L660
.L657:
	subq	$48, %r12
	testq	%rdi, %rdi
	je	.L658
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L658:
	movq	-96(%rbp), %rax
	movq	%r12, %rsi
	movq	%rax, (%r14)
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L660:
	movq	(%rbx), %rdi
	movl	$-1, %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	(%rbx), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	-96(%rbp), %rdx
	movq	%r13, -96(%rbp)
	movq	%rdx, %r13
	jmp	.L650
	.p2align 4,,10
	.p2align 3
.L678:
	movq	(%rcx), %r12
.L655:
	movq	24(%r12), %rdi
	cmpq	-96(%rbp), %rdi
	je	.L660
	leaq	24(%r12), %r14
	jmp	.L657
	.p2align 4,,10
	.p2align 3
.L652:
	cmpq	-96(%rbp), %r13
	jne	.L653
	movq	%rsi, %r12
	jmp	.L655
.L676:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23091:
	.size	_ZN2v88internal8compiler18SimplifiedLowering9Uint32ModEPNS1_4NodeE, .-_ZN2v88internal8compiler18SimplifiedLowering9Uint32ModEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler18SimplifiedLowering5DoMaxEPNS1_4NodeEPKNS1_8OperatorENS0_21MachineRepresentationE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18SimplifiedLowering5DoMaxEPNS1_4NodeEPKNS1_8OperatorENS0_21MachineRepresentationE
	.type	_ZN2v88internal8compiler18SimplifiedLowering5DoMaxEPNS1_4NodeEPKNS1_8OperatorENS0_21MachineRepresentationE, @function
_ZN2v88internal8compiler18SimplifiedLowering5DoMaxEPNS1_4NodeEPKNS1_8OperatorENS0_21MachineRepresentationE:
.LFB23092:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movq	%rdx, %rsi
	pushq	%rbx
	leaq	32(%r12), %r15
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movl	%ecx, -84(%rbp)
	movq	32(%r12), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L680
	leaq	40(%r12), %rax
.L681:
	movq	(%rbx), %rdx
	movq	(%rax), %rax
	leaq	-80(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	(%rdx), %rdi
	movl	$2, %edx
	movq	%r14, -80(%rbp)
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r13
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L682
	movq	32(%r12), %rdi
	movq	%r12, %rsi
	cmpq	%rdi, %r13
	je	.L684
	subq	$24, %rsi
	testq	%rdi, %rdi
	je	.L686
.L697:
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-96(%rbp), %rsi
.L686:
	movq	%r13, (%r15)
	testq	%r13, %r13
	je	.L684
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L684:
	movq	(%rbx), %rax
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11AppendInputEPNS0_4ZoneEPS2_@PLT
	movq	(%rbx), %rax
	movzbl	-84(%rbp), %esi
	xorl	%edx, %edx
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6SelectENS0_21MachineRepresentationENS1_10BranchHintE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L696
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L682:
	.cfi_restore_state
	movq	32(%r12), %rsi
	movq	16(%rsi), %rdi
	leaq	16(%rsi), %r15
	cmpq	%rdi, %r13
	je	.L684
	subq	$24, %rsi
	testq	%rdi, %rdi
	jne	.L697
	jmp	.L686
	.p2align 4,,10
	.p2align 3
.L680:
	leaq	16(%r14), %rax
	movq	16(%r14), %r14
	addq	$8, %rax
	jmp	.L681
.L696:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23092:
	.size	_ZN2v88internal8compiler18SimplifiedLowering5DoMaxEPNS1_4NodeEPKNS1_8OperatorENS0_21MachineRepresentationE, .-_ZN2v88internal8compiler18SimplifiedLowering5DoMaxEPNS1_4NodeEPKNS1_8OperatorENS0_21MachineRepresentationE
	.section	.text._ZN2v88internal8compiler18SimplifiedLowering5DoMinEPNS1_4NodeEPKNS1_8OperatorENS0_21MachineRepresentationE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18SimplifiedLowering5DoMinEPNS1_4NodeEPKNS1_8OperatorENS0_21MachineRepresentationE
	.type	_ZN2v88internal8compiler18SimplifiedLowering5DoMinEPNS1_4NodeEPKNS1_8OperatorENS0_21MachineRepresentationE, @function
_ZN2v88internal8compiler18SimplifiedLowering5DoMinEPNS1_4NodeEPKNS1_8OperatorENS0_21MachineRepresentationE:
.LFB23093:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	movq	%rdx, %rsi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	32(%r12), %rdx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L699
	leaq	40(%r12), %rax
.L700:
	movq	(%rbx), %rcx
	movq	(%rax), %rax
	xorl	%r8d, %r8d
	movq	(%rcx), %rdi
	movq	%rdx, -64(%rbp)
	leaq	-64(%rbp), %rcx
	movl	$2, %edx
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	(%rbx), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	(%rbx), %rax
	xorl	%edx, %edx
	movl	%r13d, %esi
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6SelectENS0_21MachineRepresentationENS1_10BranchHintE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L703
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L699:
	.cfi_restore_state
	leaq	16(%rdx), %rax
	movq	16(%rdx), %rdx
	addq	$8, %rax
	jmp	.L700
.L703:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23093:
	.size	_ZN2v88internal8compiler18SimplifiedLowering5DoMinEPNS1_4NodeEPKNS1_8OperatorENS0_21MachineRepresentationE, .-_ZN2v88internal8compiler18SimplifiedLowering5DoMinEPNS1_4NodeEPKNS1_8OperatorENS0_21MachineRepresentationE
	.section	.text._ZN2v88internal8compiler18SimplifiedLowering17DoIntegral32ToBitEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18SimplifiedLowering17DoIntegral32ToBitEPNS1_4NodeE
	.type	_ZN2v88internal8compiler18SimplifiedLowering17DoIntegral32ToBitEPNS1_4NodeE, @function
_ZN2v88internal8compiler18SimplifiedLowering17DoIntegral32ToBitEPNS1_4NodeE:
.LFB23094:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	32(%rsi), %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	32(%rsi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L705
	movq	16(%r14), %r14
.L705:
	movq	(%rbx), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%rax, -88(%rbp)
	movq	(%rbx), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder11Word32EqualEv@PLT
	movq	%r14, %xmm0
	leaq	-80(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%rax, %r12
	movq	(%rbx), %rax
	movhps	-88(%rbp), %xmm0
	movl	$2, %edx
	movq	%r12, %rsi
	movq	(%rax), %rdi
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r14
	movzbl	23(%r13), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L706
	movq	32(%r13), %rdi
	movq	%r13, %rsi
	cmpq	%rdi, %r14
	je	.L708
	subq	$24, %rsi
	testq	%rdi, %rdi
	je	.L710
.L721:
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-96(%rbp), %rsi
.L710:
	movq	%r14, (%r15)
	testq	%r14, %r14
	je	.L708
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L708:
	movq	(%rbx), %rax
	movq	-88(%rbp), %rdx
	movq	%r13, %rdi
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11AppendInputEPNS0_4ZoneEPS2_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L720
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L706:
	.cfi_restore_state
	movq	32(%r13), %rsi
	movq	16(%rsi), %rdi
	leaq	16(%rsi), %r15
	cmpq	%rdi, %r14
	je	.L708
	subq	$24, %rsi
	testq	%rdi, %rdi
	jne	.L721
	jmp	.L710
.L720:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23094:
	.size	_ZN2v88internal8compiler18SimplifiedLowering17DoIntegral32ToBitEPNS1_4NodeE, .-_ZN2v88internal8compiler18SimplifiedLowering17DoIntegral32ToBitEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler18SimplifiedLowering20DoOrderedNumberToBitEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18SimplifiedLowering20DoOrderedNumberToBitEPNS1_4NodeE
	.type	_ZN2v88internal8compiler18SimplifiedLowering20DoOrderedNumberToBitEPNS1_4NodeE, @function
_ZN2v88internal8compiler18SimplifiedLowering20DoOrderedNumberToBitEPNS1_4NodeE:
.LFB23095:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	32(%rsi), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	32(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L723
	movq	16(%r13), %r13
.L723:
	movq	(%rbx), %rdi
	pxor	%xmm0, %xmm0
	movq	(%rdi), %r15
	call	_ZN2v88internal8compiler12MachineGraph15Float64ConstantEd@PLT
	movq	%rax, -88(%rbp)
	movq	(%rbx), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder12Float64EqualEv@PLT
	movq	%r13, %xmm0
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movhps	-88(%rbp), %xmm0
	movq	%rax, %rsi
	leaq	-80(%rbp), %rcx
	movl	$2, %edx
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r13
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L724
	movq	32(%r12), %rdi
	movq	%r12, %rsi
	cmpq	%rdi, %r13
	je	.L726
	leaq	-24(%rsi), %r15
	testq	%rdi, %rdi
	je	.L728
.L739:
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L728:
	movq	%r13, (%r14)
	testq	%r13, %r13
	je	.L726
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L726:
	movq	(%rbx), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	(%rbx), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11AppendInputEPNS0_4ZoneEPS2_@PLT
	movq	(%rbx), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder11Word32EqualEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L738
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L724:
	.cfi_restore_state
	movq	32(%r12), %rsi
	movq	16(%rsi), %rdi
	leaq	16(%rsi), %r14
	cmpq	%rdi, %r13
	je	.L726
	leaq	-24(%rsi), %r15
	testq	%rdi, %rdi
	jne	.L739
	jmp	.L728
.L738:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23095:
	.size	_ZN2v88internal8compiler18SimplifiedLowering20DoOrderedNumberToBitEPNS1_4NodeE, .-_ZN2v88internal8compiler18SimplifiedLowering20DoOrderedNumberToBitEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler18SimplifiedLowering13DoNumberToBitEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18SimplifiedLowering13DoNumberToBitEPNS1_4NodeE
	.type	_ZN2v88internal8compiler18SimplifiedLowering13DoNumberToBitEPNS1_4NodeE, @function
_ZN2v88internal8compiler18SimplifiedLowering13DoNumberToBitEPNS1_4NodeE:
.LFB23096:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	32(%rsi), %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	32(%rsi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L741
	movq	16(%r14), %r14
.L741:
	movq	(%rbx), %rdi
	pxor	%xmm0, %xmm0
	call	_ZN2v88internal8compiler12MachineGraph15Float64ConstantEd@PLT
	movq	%rax, %r13
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L742
	movq	32(%r12), %rdi
	movq	%r12, %rsi
	cmpq	%rdi, %r13
	je	.L744
	subq	$24, %rsi
	testq	%rdi, %rdi
	je	.L746
.L757:
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-72(%rbp), %rsi
.L746:
	movq	%r13, (%r15)
	testq	%r13, %r13
	je	.L744
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L744:
	movq	(%rbx), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler22MachineOperatorBuilder10Float64AbsEv@PLT
	xorl	%r8d, %r8d
	leaq	-64(%rbp), %rcx
	movq	%r13, %rdi
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r14, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	(%rbx), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11AppendInputEPNS0_4ZoneEPS2_@PLT
	movq	(%rbx), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder15Float64LessThanEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L756
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L742:
	.cfi_restore_state
	movq	32(%r12), %rsi
	movq	16(%rsi), %rdi
	leaq	16(%rsi), %r15
	cmpq	%rdi, %r13
	je	.L744
	subq	$24, %rsi
	testq	%rdi, %rdi
	jne	.L757
	jmp	.L746
.L756:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23096:
	.size	_ZN2v88internal8compiler18SimplifiedLowering13DoNumberToBitEPNS1_4NodeE, .-_ZN2v88internal8compiler18SimplifiedLowering13DoNumberToBitEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler18SimplifiedLowering23DoIntegerToUint8ClampedEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18SimplifiedLowering23DoIntegerToUint8ClampedEPNS1_4NodeE
	.type	_ZN2v88internal8compiler18SimplifiedLowering23DoIntegerToUint8ClampedEPNS1_4NodeE, @function
_ZN2v88internal8compiler18SimplifiedLowering23DoIntegerToUint8ClampedEPNS1_4NodeE:
.LFB23097:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	32(%rsi), %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rsi), %rax
	movq	%rax, -88(%rbp)
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L759
	movq	-88(%rbp), %rax
	movq	16(%rax), %rax
	movq	%rax, -88(%rbp)
.L759:
	movq	(%rbx), %rdi
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %r14
	call	_ZN2v88internal8compiler12MachineGraph15Float64ConstantEd@PLT
	movsd	.LC20(%rip), %xmm0
	movq	(%rbx), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler12MachineGraph15Float64ConstantEd@PLT
	movq	%rax, -96(%rbp)
	movq	(%rbx), %rax
	movq	(%rax), %r10
	movq	16(%rax), %rdi
	movq	%r10, -112(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder15Float64LessThanEv@PLT
	movq	-112(%rbp), %r10
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	%r12, %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movhps	-88(%rbp), %xmm0
	movq	%r10, %rdi
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r8
	movzbl	23(%r13), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L760
	movq	32(%r13), %rdi
	movq	%r13, %rsi
	cmpq	%rdi, %r8
	je	.L762
	subq	$24, %rsi
	testq	%rdi, %rdi
	je	.L764
.L775:
	movq	%r8, -120(%rbp)
	movq	%rsi, -112(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-120(%rbp), %r8
	movq	-112(%rbp), %rsi
.L764:
	movq	%r8, (%r15)
	testq	%r8, %r8
	je	.L762
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L762:
	movq	(%rbx), %rax
	movq	-88(%rbp), %xmm0
	movq	16(%rax), %rdi
	movq	(%rax), %r15
	movhps	-96(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder15Float64LessThanEv@PLT
	movdqa	-112(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	%rax, %rsi
	movl	$2, %edx
	movq	%r15, %rdi
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	xorl	%edx, %edx
	movl	$13, %esi
	movq	%rax, -112(%rbp)
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6SelectENS0_21MachineRepresentationENS1_10BranchHintE@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$3, %edx
	movq	-112(%rbp), %xmm0
	movq	%rax, %rsi
	movq	-96(%rbp), %rax
	movq	%r15, %rdi
	movhps	-88(%rbp), %xmm0
	movq	%rax, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r13, %rdi
	movq	%rax, %rdx
	movq	(%rbx), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11AppendInputEPNS0_4ZoneEPS2_@PLT
	movq	(%rbx), %rax
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11AppendInputEPNS0_4ZoneEPS2_@PLT
	movq	(%rbx), %rax
	xorl	%edx, %edx
	movl	$13, %esi
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6SelectENS0_21MachineRepresentationENS1_10BranchHintE@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L774
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L760:
	.cfi_restore_state
	movq	32(%r13), %rsi
	movq	16(%rsi), %rdi
	leaq	16(%rsi), %r15
	cmpq	%rdi, %r8
	je	.L762
	subq	$24, %rsi
	testq	%rdi, %rdi
	jne	.L775
	jmp	.L764
.L774:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23097:
	.size	_ZN2v88internal8compiler18SimplifiedLowering23DoIntegerToUint8ClampedEPNS1_4NodeE, .-_ZN2v88internal8compiler18SimplifiedLowering23DoIntegerToUint8ClampedEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler18SimplifiedLowering22DoNumberToUint8ClampedEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18SimplifiedLowering22DoNumberToUint8ClampedEPNS1_4NodeE
	.type	_ZN2v88internal8compiler18SimplifiedLowering22DoNumberToUint8ClampedEPNS1_4NodeE, @function
_ZN2v88internal8compiler18SimplifiedLowering22DoNumberToUint8ClampedEPNS1_4NodeE:
.LFB23098:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	32(%rsi), %rax
	movq	%rax, -104(%rbp)
	movq	32(%rsi), %rax
	movq	%rax, -88(%rbp)
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L777
	movq	-88(%rbp), %rax
	movq	16(%rax), %rax
	movq	%rax, -88(%rbp)
.L777:
	movq	(%rbx), %rdi
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %r14
	call	_ZN2v88internal8compiler12MachineGraph15Float64ConstantEd@PLT
	movsd	.LC20(%rip), %xmm0
	movq	(%rbx), %rdi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler12MachineGraph15Float64ConstantEd@PLT
	movq	%rax, %r13
	movq	(%rbx), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler22MachineOperatorBuilder15Float64LessThanEv@PLT
	movq	%r13, %xmm1
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	-88(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movq	%r15, %rdi
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	xorl	%edx, %edx
	movl	$13, %esi
	movq	%rax, -112(%rbp)
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6SelectENS0_21MachineRepresentationENS1_10BranchHintE@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$3, %edx
	movq	-112(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movq	%r13, -64(%rbp)
	movhps	-88(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -112(%rbp)
	movq	(%rbx), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler22MachineOperatorBuilder15Float64LessThanEv@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	%r13, %rdi
	movq	-96(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movhps	-88(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	xorl	%edx, %edx
	movl	$13, %esi
	movq	%rax, -88(%rbp)
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6SelectENS0_21MachineRepresentationENS1_10BranchHintE@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$3, %edx
	movq	-88(%rbp), %xmm0
	movq	%rax, %rsi
	movq	-96(%rbp), %rax
	movq	%r15, %rdi
	movhps	-112(%rbp), %xmm0
	movq	%rax, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r13
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L778
	movq	32(%r12), %rdi
	movq	%r12, %rsi
	cmpq	%rdi, %r13
	je	.L780
	leaq	-24(%rsi), %r15
	testq	%rdi, %rdi
	je	.L782
.L793:
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L782:
	movq	-104(%rbp), %rax
	movq	%r13, (%rax)
	testq	%r13, %r13
	je	.L780
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L780:
	movq	(%rbx), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder20Float64RoundTiesEvenEv@PLT
	movq	%r12, %rdi
	movq	%rdx, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L792
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L778:
	.cfi_restore_state
	movq	32(%r12), %rsi
	movq	16(%rsi), %rdi
	leaq	16(%rsi), %rax
	movq	%rax, -104(%rbp)
	cmpq	%rdi, %r13
	je	.L780
	leaq	-24(%rsi), %r15
	testq	%rdi, %rdi
	jne	.L793
	jmp	.L782
.L792:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23098:
	.size	_ZN2v88internal8compiler18SimplifiedLowering22DoNumberToUint8ClampedEPNS1_4NodeE, .-_ZN2v88internal8compiler18SimplifiedLowering22DoNumberToUint8ClampedEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler18SimplifiedLowering24DoSigned32ToUint8ClampedEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18SimplifiedLowering24DoSigned32ToUint8ClampedEPNS1_4NodeE
	.type	_ZN2v88internal8compiler18SimplifiedLowering24DoSigned32ToUint8ClampedEPNS1_4NodeE, @function
_ZN2v88internal8compiler18SimplifiedLowering24DoSigned32ToUint8ClampedEPNS1_4NodeE:
.LFB23099:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	32(%rsi), %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rsi), %rax
	movq	%rax, -88(%rbp)
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L795
	movq	-88(%rbp), %rax
	movq	16(%rax), %rax
	movq	%rax, -88(%rbp)
.L795:
	movq	(%rbx), %rdi
	xorl	%esi, %esi
	leaq	-80(%rbp), %r14
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	(%rbx), %rdi
	movl	$255, %esi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%rax, -104(%rbp)
	movq	(%rbx), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder20Int32LessThanOrEqualEv@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$2, %edx
	movq	-88(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r12, %rdi
	movhps	-104(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r8
	movzbl	23(%r13), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L796
	movq	32(%r13), %rdi
	movq	%r13, %rsi
	cmpq	%rdi, %r8
	je	.L798
	subq	$24, %rsi
	testq	%rdi, %rdi
	je	.L800
.L811:
	movq	%r8, -112(%rbp)
	movq	%rsi, -128(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-112(%rbp), %r8
	movq	-128(%rbp), %rsi
.L800:
	movq	%r8, (%r15)
	testq	%r8, %r8
	je	.L798
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L798:
	movq	(%rbx), %rax
	movq	-88(%rbp), %xmm0
	movq	16(%rax), %rdi
	movq	(%rax), %r15
	movhps	-96(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder13Int32LessThanEv@PLT
	movdqa	-128(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	%rax, %rsi
	movl	$2, %edx
	movq	%r15, %rdi
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	xorl	%edx, %edx
	movl	$4, %esi
	movq	%rax, -128(%rbp)
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6SelectENS0_21MachineRepresentationENS1_10BranchHintE@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$3, %edx
	movq	-128(%rbp), %xmm0
	movq	%rax, %rsi
	movq	-88(%rbp), %rax
	movq	%r15, %rdi
	movhps	-96(%rbp), %xmm0
	movq	%rax, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r13, %rdi
	movq	%rax, %rdx
	movq	(%rbx), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11AppendInputEPNS0_4ZoneEPS2_@PLT
	movq	(%rbx), %rax
	movq	-104(%rbp), %rdx
	movq	%r13, %rdi
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11AppendInputEPNS0_4ZoneEPS2_@PLT
	movq	(%rbx), %rax
	xorl	%edx, %edx
	movl	$4, %esi
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6SelectENS0_21MachineRepresentationENS1_10BranchHintE@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L810
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L796:
	.cfi_restore_state
	movq	32(%r13), %rsi
	movq	16(%rsi), %rdi
	leaq	16(%rsi), %r15
	cmpq	%rdi, %r8
	je	.L798
	subq	$24, %rsi
	testq	%rdi, %rdi
	jne	.L811
	jmp	.L800
.L810:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23099:
	.size	_ZN2v88internal8compiler18SimplifiedLowering24DoSigned32ToUint8ClampedEPNS1_4NodeE, .-_ZN2v88internal8compiler18SimplifiedLowering24DoSigned32ToUint8ClampedEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler18SimplifiedLowering26DoUnsigned32ToUint8ClampedEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18SimplifiedLowering26DoUnsigned32ToUint8ClampedEPNS1_4NodeE
	.type	_ZN2v88internal8compiler18SimplifiedLowering26DoUnsigned32ToUint8ClampedEPNS1_4NodeE, @function
_ZN2v88internal8compiler18SimplifiedLowering26DoUnsigned32ToUint8ClampedEPNS1_4NodeE:
.LFB23100:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	32(%rsi), %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	32(%rsi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L813
	movq	16(%r14), %r14
.L813:
	movq	(%rbx), %rdi
	movl	$255, %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%rax, -88(%rbp)
	movq	(%rbx), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler22MachineOperatorBuilder21Uint32LessThanOrEqualEv@PLT
	movq	%r14, %xmm0
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	movhps	-88(%rbp), %xmm0
	movq	%rax, %rsi
	leaq	-80(%rbp), %rcx
	movl	$2, %edx
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r8
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L814
	movq	32(%r12), %rdi
	movq	%r12, %rsi
	cmpq	%rdi, %r8
	je	.L816
	subq	$24, %rsi
	testq	%rdi, %rdi
	je	.L818
.L829:
	movq	%r8, -104(%rbp)
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-104(%rbp), %r8
	movq	-96(%rbp), %rsi
.L818:
	movq	%r8, (%r15)
	testq	%r8, %r8
	je	.L816
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L816:
	movq	(%rbx), %rax
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11AppendInputEPNS0_4ZoneEPS2_@PLT
	movq	(%rbx), %rax
	movq	-88(%rbp), %rdx
	movq	%r12, %rdi
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11AppendInputEPNS0_4ZoneEPS2_@PLT
	movq	(%rbx), %rax
	xorl	%edx, %edx
	movl	$4, %esi
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6SelectENS0_21MachineRepresentationENS1_10BranchHintE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L828
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L814:
	.cfi_restore_state
	movq	32(%r12), %rsi
	movq	16(%rsi), %rdi
	leaq	16(%rsi), %r15
	cmpq	%rdi, %r8
	je	.L816
	subq	$24, %rsi
	testq	%rdi, %rdi
	jne	.L829
	jmp	.L818
.L828:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23100:
	.size	_ZN2v88internal8compiler18SimplifiedLowering26DoUnsigned32ToUint8ClampedEPNS1_4NodeE, .-_ZN2v88internal8compiler18SimplifiedLowering26DoUnsigned32ToUint8ClampedEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler18SimplifiedLowering12ToNumberCodeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18SimplifiedLowering12ToNumberCodeEv
	.type	_ZN2v88internal8compiler18SimplifiedLowering12ToNumberCodeEv, @function
_ZN2v88internal8compiler18SimplifiedLowering12ToNumberCodeEv:
.LFB23101:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rax
	testq	%rax, %rax
	je	.L834
.L830:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L835
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L834:
	.cfi_restore_state
	movq	%rdi, %rbx
	movl	$102, %edx
	leaq	-48(%rbp), %rdi
	movq	(%rbx), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	(%rbx), %rdi
	movq	-48(%rbp), %rsi
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rax, 32(%rbx)
	jmp	.L830
.L835:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23101:
	.size	_ZN2v88internal8compiler18SimplifiedLowering12ToNumberCodeEv, .-_ZN2v88internal8compiler18SimplifiedLowering12ToNumberCodeEv
	.section	.text._ZN2v88internal8compiler18SimplifiedLowering25ToNumberConvertBigIntCodeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18SimplifiedLowering25ToNumberConvertBigIntCodeEv
	.type	_ZN2v88internal8compiler18SimplifiedLowering25ToNumberConvertBigIntCodeEv, @function
_ZN2v88internal8compiler18SimplifiedLowering25ToNumberConvertBigIntCodeEv:
.LFB23108:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	40(%rdi), %rax
	testq	%rax, %rax
	je	.L840
.L836:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L841
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L840:
	.cfi_restore_state
	movq	%rdi, %rbx
	movl	$103, %edx
	leaq	-48(%rbp), %rdi
	movq	(%rbx), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	(%rbx), %rdi
	movq	-48(%rbp), %rsi
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rax, 40(%rbx)
	jmp	.L836
.L841:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23108:
	.size	_ZN2v88internal8compiler18SimplifiedLowering25ToNumberConvertBigIntCodeEv, .-_ZN2v88internal8compiler18SimplifiedLowering25ToNumberConvertBigIntCodeEv
	.section	.text._ZN2v88internal8compiler18SimplifiedLowering13ToNumericCodeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18SimplifiedLowering13ToNumericCodeEv
	.type	_ZN2v88internal8compiler18SimplifiedLowering13ToNumericCodeEv, @function
_ZN2v88internal8compiler18SimplifiedLowering13ToNumericCodeEv:
.LFB23109:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	48(%rdi), %rax
	testq	%rax, %rax
	je	.L846
.L842:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L847
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L846:
	.cfi_restore_state
	movq	%rdi, %rbx
	movl	$104, %edx
	leaq	-48(%rbp), %rdi
	movq	(%rbx), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	(%rbx), %rdi
	movq	-48(%rbp), %rsi
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rax, 48(%rbx)
	jmp	.L842
.L847:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23109:
	.size	_ZN2v88internal8compiler18SimplifiedLowering13ToNumericCodeEv, .-_ZN2v88internal8compiler18SimplifiedLowering13ToNumericCodeEv
	.section	.text._ZN2v88internal8compiler18SimplifiedLowering16ToNumberOperatorEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18SimplifiedLowering16ToNumberOperatorEv
	.type	_ZN2v88internal8compiler18SimplifiedLowering16ToNumberOperatorEv, @function
_ZN2v88internal8compiler18SimplifiedLowering16ToNumberOperatorEv:
.LFB23110:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	56(%rdi), %rax
	testq	%rax, %rax
	je	.L852
.L848:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L853
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L852:
	.cfi_restore_state
	movq	%rdi, %rbx
	movl	$102, %edx
	leaq	-48(%rbp), %rdi
	movq	(%rbx), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-32(%rbp), %rax
	leaq	-64(%rbp), %rsi
	xorl	%r9d, %r9d
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	xorl	%r8d, %r8d
	movl	8(%rax), %edx
	subl	(%rax), %edx
	movq	%rax, -56(%rbp)
	movq	(%rbx), %rax
	movq	%rcx, -64(%rbp)
	movl	$1, %ecx
	movq	(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler7Linkage21GetStubCallDescriptorEPNS0_4ZoneERKNS0_23CallInterfaceDescriptorEiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEENS9_INS1_8Operator8PropertyEhEENS0_12StubCallModeE@PLT
	movq	%rax, %rsi
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	movq	%rax, 56(%rbx)
	jmp	.L848
.L853:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23110:
	.size	_ZN2v88internal8compiler18SimplifiedLowering16ToNumberOperatorEv, .-_ZN2v88internal8compiler18SimplifiedLowering16ToNumberOperatorEv
	.section	.text._ZN2v88internal8compiler18SimplifiedLowering29ToNumberConvertBigIntOperatorEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18SimplifiedLowering29ToNumberConvertBigIntOperatorEv
	.type	_ZN2v88internal8compiler18SimplifiedLowering29ToNumberConvertBigIntOperatorEv, @function
_ZN2v88internal8compiler18SimplifiedLowering29ToNumberConvertBigIntOperatorEv:
.LFB23111:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	64(%rdi), %rax
	testq	%rax, %rax
	je	.L858
.L854:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L859
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L858:
	.cfi_restore_state
	movq	%rdi, %rbx
	movl	$103, %edx
	leaq	-48(%rbp), %rdi
	movq	(%rbx), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-32(%rbp), %rax
	leaq	-64(%rbp), %rsi
	xorl	%r9d, %r9d
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	xorl	%r8d, %r8d
	movl	8(%rax), %edx
	subl	(%rax), %edx
	movq	%rax, -56(%rbp)
	movq	(%rbx), %rax
	movq	%rcx, -64(%rbp)
	movl	$1, %ecx
	movq	(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler7Linkage21GetStubCallDescriptorEPNS0_4ZoneERKNS0_23CallInterfaceDescriptorEiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEENS9_INS1_8Operator8PropertyEhEENS0_12StubCallModeE@PLT
	movq	%rax, %rsi
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	movq	%rax, 64(%rbx)
	jmp	.L854
.L859:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23111:
	.size	_ZN2v88internal8compiler18SimplifiedLowering29ToNumberConvertBigIntOperatorEv, .-_ZN2v88internal8compiler18SimplifiedLowering29ToNumberConvertBigIntOperatorEv
	.section	.text._ZN2v88internal8compiler18SimplifiedLowering17ToNumericOperatorEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18SimplifiedLowering17ToNumericOperatorEv
	.type	_ZN2v88internal8compiler18SimplifiedLowering17ToNumericOperatorEv, @function
_ZN2v88internal8compiler18SimplifiedLowering17ToNumericOperatorEv:
.LFB23112:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	72(%rdi), %rax
	testq	%rax, %rax
	je	.L864
.L860:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L865
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L864:
	.cfi_restore_state
	movq	%rdi, %rbx
	movl	$104, %edx
	leaq	-48(%rbp), %rdi
	movq	(%rbx), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-32(%rbp), %rax
	leaq	-64(%rbp), %rsi
	xorl	%r9d, %r9d
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	xorl	%r8d, %r8d
	movl	8(%rax), %edx
	subl	(%rax), %edx
	movq	%rax, -56(%rbp)
	movq	(%rbx), %rax
	movq	%rcx, -64(%rbp)
	movl	$1, %ecx
	movq	(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler7Linkage21GetStubCallDescriptorEPNS0_4ZoneERKNS0_23CallInterfaceDescriptorEiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEENS9_INS1_8Operator8PropertyEhEENS0_12StubCallModeE@PLT
	movq	%rax, %rsi
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	movq	%rax, 72(%rbx)
	jmp	.L860
.L865:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23112:
	.size	_ZN2v88internal8compiler18SimplifiedLowering17ToNumericOperatorEv, .-_ZN2v88internal8compiler18SimplifiedLowering17ToNumericOperatorEv
	.section	.rodata._ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_.str1.1,"aMS",@progbits,1
.LC21:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	.type	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_, @function
_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_:
.LFB26316:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r14
	movq	8(%rdi), %rbx
	movq	%r14, %rax
	subq	%rbx, %rax
	sarq	$3, %rax
	cmpq	$268435455, %rax
	je	.L904
	movq	%rsi, %r8
	movq	%rdi, %r13
	movq	%rsi, %r12
	movq	%rdx, %r15
	subq	%rbx, %r8
	testq	%rax, %rax
	je	.L882
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L905
	movl	$2147483640, %esi
	movl	$2147483640, %edx
.L868:
	movq	0(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	%rsi, %rcx
	jb	.L906
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L871:
	leaq	(%rax,%rdx), %rdi
	leaq	8(%rax), %rcx
	jmp	.L869
	.p2align 4,,10
	.p2align 3
.L905:
	testq	%rdx, %rdx
	jne	.L907
	movl	$8, %ecx
	xorl	%edi, %edi
	xorl	%eax, %eax
.L869:
	movq	(%r15), %rdx
	movq	%rdx, (%rax,%r8)
	cmpq	%rbx, %r12
	je	.L872
	leaq	-8(%r12), %r8
	leaq	15(%rax), %rdx
	subq	%rbx, %r8
	subq	%rbx, %rdx
	movq	%r8, %rcx
	shrq	$3, %rcx
	cmpq	$30, %rdx
	jbe	.L885
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rcx
	je	.L885
	leaq	1(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L874:
	movdqu	(%rbx,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L874
	movq	%rsi, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rcx
	addq	%rcx, %rbx
	addq	%rax, %rcx
	cmpq	%rdx, %rsi
	je	.L876
	movq	(%rbx), %rdx
	movq	%rdx, (%rcx)
.L876:
	leaq	16(%rax,%r8), %rcx
.L872:
	cmpq	%r14, %r12
	je	.L877
	subq	%r12, %r14
	leaq	15(%r12), %rdx
	leaq	-8(%r14), %r9
	subq	%rcx, %rdx
	movq	%r9, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rdx
	jbe	.L886
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rsi
	je	.L886
	leaq	1(%rsi), %r8
	xorl	%edx, %edx
	movq	%r8, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L879:
	movdqu	(%r12,%rdx), %xmm2
	movups	%xmm2, (%rcx,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rsi
	jne	.L879
	movq	%r8, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rsi
	leaq	(%rcx,%rsi), %r10
	addq	%rsi, %r12
	cmpq	%rdx, %r8
	je	.L881
	movq	(%r12), %rdx
	movq	%rdx, (%r10)
.L881:
	leaq	8(%rcx,%r9), %rcx
.L877:
	movq	%rax, %xmm0
	movq	%rcx, %xmm3
	movq	%rdi, 24(%r13)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 8(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L882:
	.cfi_restore_state
	movl	$8, %esi
	movl	$8, %edx
	jmp	.L868
	.p2align 4,,10
	.p2align 3
.L886:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L878:
	movq	(%r12,%rdx,8), %r8
	movq	%r8, (%rcx,%rdx,8)
	movq	%rdx, %r8
	addq	$1, %rdx
	cmpq	%rsi, %r8
	jne	.L878
	jmp	.L881
	.p2align 4,,10
	.p2align 3
.L885:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L873:
	movq	(%rbx,%rdx,8), %rsi
	movq	%rsi, (%rax,%rdx,8)
	movq	%rdx, %rsi
	addq	$1, %rdx
	cmpq	%rsi, %rcx
	jne	.L873
	jmp	.L876
.L906:
	movq	%r8, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %r8
	jmp	.L871
.L904:
	leaq	.LC21(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L907:
	cmpq	$268435455, %rdx
	movl	$268435455, %eax
	cmova	%rax, %rdx
	salq	$3, %rdx
	movq	%rdx, %rsi
	jmp	.L868
	.cfi_endproc
.LFE26316:
	.size	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_, .-_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	.section	.rodata._ZN2v88internal8compiler22RepresentationSelector16DeferReplacementEPNS1_4NodeES4_.str1.8,"aMS",@progbits,1
	.align 8
.LC22:
	.string	"defer replacement #%d:%s with #%d:%s\n"
	.section	.text._ZN2v88internal8compiler22RepresentationSelector16DeferReplacementEPNS1_4NodeES4_,"axG",@progbits,_ZN2v88internal8compiler22RepresentationSelector16DeferReplacementEPNS1_4NodeES4_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler22RepresentationSelector16DeferReplacementEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler22RepresentationSelector16DeferReplacementEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler22RepresentationSelector16DeferReplacementEPNS1_4NodeES4_:
.LFB23060:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$16, %rsp
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	movq	%rsi, -24(%rbp)
	movq	%rdx, -32(%rbp)
	movq	(%rsi), %rax
	jne	.L916
.L909:
	movl	24(%rax), %eax
	testl	%eax, %eax
	jle	.L910
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	-24(%rbp), %rdi
	xorl	%esi, %esi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	%r12, %rdx
	movq	%rax, %rsi
	movq	-24(%rbp), %rax
	movq	24(%rax), %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_124ReplaceEffectControlUsesEPNS1_4NodeES4_S4_.isra.0
.L910:
	movq	104(%rbx), %rsi
	leaq	88(%rbx), %r12
	cmpq	112(%rbx), %rsi
	je	.L911
	movq	-24(%rbp), %rax
	movq	%rax, (%rsi)
	movq	104(%rbx), %rax
	leaq	8(%rax), %rsi
	movq	%rsi, 104(%rbx)
.L912:
	cmpq	%rsi, 112(%rbx)
	je	.L913
	movq	-32(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 104(%rbx)
.L914:
	movq	-24(%rbp), %rdi
	call	_ZN2v88internal8compiler4Node13NullAllInputsEv@PLT
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L916:
	.cfi_restore_state
	movq	(%rdx), %r8
	movl	20(%rdx), %ecx
	leaq	.LC22(%rip), %rdi
	movl	20(%rsi), %esi
	movq	8(%rax), %rdx
	xorl	%eax, %eax
	movq	8(%r8), %r8
	andl	$16777215, %ecx
	andl	$16777215, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-24(%rbp), %rdi
	movq	(%rdi), %rax
	jmp	.L909
	.p2align 4,,10
	.p2align 3
.L913:
	leaq	-32(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L914
	.p2align 4,,10
	.p2align 3
.L911:
	leaq	-24(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	movq	104(%rbx), %rsi
	jmp	.L912
	.cfi_endproc
.LFE23060:
	.size	_ZN2v88internal8compiler22RepresentationSelector16DeferReplacementEPNS1_4NodeES4_, .-_ZN2v88internal8compiler22RepresentationSelector16DeferReplacementEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler18SimplifiedLowering39DoJSToNumberOrNumericTruncatesToFloat64EPNS1_4NodeEPNS1_22RepresentationSelectorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18SimplifiedLowering39DoJSToNumberOrNumericTruncatesToFloat64EPNS1_4NodeEPNS1_22RepresentationSelectorE
	.type	_ZN2v88internal8compiler18SimplifiedLowering39DoJSToNumberOrNumericTruncatesToFloat64EPNS1_4NodeEPNS1_22RepresentationSelectorE, @function
_ZN2v88internal8compiler18SimplifiedLowering39DoJSToNumberOrNumericTruncatesToFloat64EPNS1_4NodeEPNS1_22RepresentationSelectorE:
.LFB23082:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$248, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -208(%rbp)
	movq	32(%rsi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L918
	movq	40(%rsi), %rax
	movq	%rax, -256(%rbp)
	movq	48(%rsi), %rax
	movq	%rax, -248(%rbp)
	movq	56(%rsi), %rax
	movq	%rax, -224(%rbp)
	leaq	64(%rsi), %rax
.L919:
	movq	(%rax), %rax
	leaq	-112(%rbp), %r15
	movq	%rax, -200(%rbp)
	movq	(%r14), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder11ObjectIsSmiEv@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%rbx, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$1, %edx
	movl	$1, %esi
	movq	%rax, -216(%rbp)
	movq	(%r14), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$2, %edx
	movq	-216(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r12, %rdi
	movhps	-200(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r12
	movq	(%r14), %rax
	movq	(%rax), %r10
	movq	8(%rax), %rdi
	movq	%r10, -200(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$1, %edx
	movq	-200(%rbp), %r10
	movq	%rax, %rsi
	movq	%r12, -112(%rbp)
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -232(%rbp)
	movq	(%r14), %rax
	movq	(%rax), %r10
	movq	376(%rax), %rdi
	movq	%r10, -200(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder25ChangeTaggedSignedToInt32Ev@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$1, %edx
	movq	-200(%rbp), %r10
	movq	%rax, %rsi
	movq	%rbx, -112(%rbp)
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -216(%rbp)
	movq	(%r14), %rax
	movq	(%rax), %r10
	movq	16(%rax), %rdi
	movq	%r10, -200(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder20ChangeInt32ToFloat64Ev@PLT
	movq	-216(%rbp), %rdx
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	-200(%rbp), %r10
	movq	%rax, %rsi
	movq	%rdx, -112(%rbp)
	movl	$1, %edx
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -240(%rbp)
	movq	(%r14), %rax
	movq	(%rax), %r10
	movq	8(%rax), %rdi
	movq	%r10, -200(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$1, %edx
	movq	-200(%rbp), %r10
	movq	%rax, %rsi
	movq	%r12, -112(%rbp)
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r14, %rdi
	movq	%rax, -200(%rbp)
	movq	0(%r13), %rax
	cmpw	$705, 16(%rax)
	je	.L970
	call	_ZN2v88internal8compiler18SimplifiedLowering17ToNumericOperatorEv
	movq	%rax, %r10
.L921:
	movq	0(%r13), %rax
	movq	(%r14), %rdx
	movzwl	16(%rax), %eax
	cmpl	$705, %eax
	je	.L971
	cmpl	$706, %eax
	je	.L972
	movq	48(%r14), %rax
	leaq	-176(%rbp), %r12
	testq	%rax, %rax
	je	.L973
.L924:
	movq	%rbx, %xmm2
	movq	%rax, %xmm0
	movq	(%rdx), %rdi
	movq	%r10, %rsi
	punpcklqdq	%xmm2, %xmm0
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$6, %edx
	movaps	%xmm0, -112(%rbp)
	movq	-256(%rbp), %xmm0
	movhps	-248(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-224(%rbp), %xmm0
	movhps	-200(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	leaq	-184(%rbp), %rsi
	movq	%r13, %rdi
	movq	$0, -184(%rbp)
	movq	%rax, -200(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties17IsExceptionalCallEPNS1_4NodeEPS4_@PLT
	movq	-200(%rbp), %rbx
	movq	%rbx, -216(%rbp)
	testb	%al, %al
	jne	.L974
.L928:
	movq	-200(%rbp), %xmm1
	movq	(%r14), %rax
	movq	376(%rax), %rdi
	punpcklqdq	%xmm1, %xmm1
	movq	(%rax), %rbx
	movaps	%xmm1, -288(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder11ObjectIsSmiEv@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	movq	-200(%rbp), %rax
	movl	$1, %edx
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%rax, -248(%rbp)
	movq	(%r14), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$2, %edx
	movq	-248(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%rbx, %rdi
	movhps	-216(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %rbx
	movq	(%r14), %rax
	movq	(%rax), %r10
	movq	8(%rax), %rdi
	movq	%r10, -216(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$1, %edx
	movq	-216(%rbp), %r10
	movq	%rax, %rsi
	movq	%rbx, -112(%rbp)
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -248(%rbp)
	movq	(%r14), %rax
	movq	(%rax), %r10
	movq	376(%rax), %rdi
	movq	%r10, -216(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder25ChangeTaggedSignedToInt32Ev@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$1, %edx
	movq	-216(%rbp), %r10
	movq	%rax, %rsi
	movq	-200(%rbp), %rax
	movq	%r10, %rdi
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -256(%rbp)
	movq	(%r14), %rax
	movq	(%rax), %r10
	movq	16(%rax), %rdi
	movq	%r10, -216(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder20ChangeInt32ToFloat64Ev@PLT
	movq	-256(%rbp), %rdx
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	-216(%rbp), %r10
	movq	%rax, %rsi
	movq	%rdx, -112(%rbp)
	movl	$1, %edx
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -256(%rbp)
	movq	(%r14), %rax
	movq	(%rax), %r10
	movq	8(%rax), %rdi
	movq	%r10, -216(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	movq	%r15, %rcx
	movl	$1, %edx
	xorl	%r8d, %r8d
	movq	-216(%rbp), %r10
	movq	%rax, %rsi
	movq	%rbx, -112(%rbp)
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	movq	(%r14), %rax
	movq	(%rax), %r10
	movq	376(%rax), %r8
	movq	%r10, -264(%rbp)
	movq	%r8, -216(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder18ForHeapNumberValueEv@PLT
	movq	-216(%rbp), %r8
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9LoadFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$3, %edx
	movq	-264(%rbp), %r10
	movdqa	-288(%rbp), %xmm1
	movq	%rax, %rsi
	movq	%rbx, -96(%rbp)
	movq	%r10, %rdi
	movaps	%xmm1, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$2, %esi
	movq	%rax, -216(%rbp)
	movq	(%r14), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
	movq	%rbx, %xmm3
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	-248(%rbp), %xmm0
	movl	$2, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$2, %esi
	movq	%rax, %rbx
	movq	(%r14), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9EffectPhiEi@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	-200(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$3, %edx
	movq	%rbx, -96(%rbp)
	movhps	-216(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$2, %edx
	movl	$13, %esi
	movq	%rax, -248(%rbp)
	movq	(%r14), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$3, %edx
	movq	-256(%rbp), %xmm0
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rbx, -96(%rbp)
	movhps	-216(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$2, %esi
	movq	%rax, -200(%rbp)
	movq	(%r14), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
	movq	%rbx, %xmm4
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	-232(%rbp), %xmm0
	movl	$2, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	punpcklqdq	%xmm4, %xmm0
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$2, %esi
	movq	%rax, %r12
	movq	(%r14), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9EffectPhiEi@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$3, %edx
	movq	-224(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%rbx, %rdi
	movq	%r12, -96(%rbp)
	movhps	-248(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$2, %edx
	movl	$13, %esi
	movq	%rax, -216(%rbp)
	movq	(%r14), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	movq	%r14, %rdi
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	-240(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$3, %edx
	movq	%r12, -96(%rbp)
	movhps	-200(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	24(%r13), %r14
	movq	%rax, -200(%rbp)
	testq	%r14, %r14
	je	.L929
	movq	(%r14), %rbx
	.p2align 4,,10
	.p2align 3
.L941:
	movl	16(%r14), %ecx
	movq	%r14, %rdi
	movl	%ecx, %edx
	shrl	%edx
	leaq	3(%rdx,%rdx,2), %rax
	leaq	(%r14,%rax,8), %rax
	leaq	32(%rax), %rsi
	addq	$16, %rax
	andl	$1, %ecx
	cmovne	%rsi, %rax
	leaq	(%rax,%rdx,8), %r15
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler14NodeProperties13IsControlEdgeENS1_4EdgeE@PLT
	testb	%al, %al
	je	.L932
	movl	16(%r14), %ecx
	movl	%ecx, %eax
	shrl	%eax
	andl	$1, %ecx
	leaq	3(%rax,%rax,2), %rax
	leaq	(%r14,%rax,8), %rdi
	movq	(%rdi), %rax
	jne	.L933
	movq	%rax, %rdi
	movq	(%rax), %rax
.L933:
	cmpw	$6, 16(%rax)
	je	.L975
	movq	(%r15), %rdi
	cmpq	%rdi, %r12
	je	.L936
	testq	%rdi, %rdi
	je	.L938
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L938:
	movq	%r12, (%r15)
	testq	%r12, %r12
	je	.L936
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	.p2align 4,,10
	.p2align 3
.L936:
	testq	%rbx, %rbx
	je	.L929
	movq	%rbx, %r14
	movq	(%rbx), %rbx
	jmp	.L941
	.p2align 4,,10
	.p2align 3
.L929:
	movq	-200(%rbp), %rdx
	movq	-208(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler22RepresentationSelector16DeferReplacementEPNS1_4NodeES4_
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L976
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L932:
	.cfi_restore_state
	movq	%r14, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler14NodeProperties12IsEffectEdgeENS1_4EdgeE@PLT
	testb	%al, %al
	je	.L936
	movq	(%r15), %rdi
	cmpq	%rdi, -216(%rbp)
	je	.L936
	testq	%rdi, %rdi
	je	.L940
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L940:
	movq	-216(%rbp), %rax
	movq	%rax, (%r15)
	testq	%rax, %rax
	je	.L936
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	jmp	.L936
	.p2align 4,,10
	.p2align 3
.L975:
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler4Node11ReplaceUsesEPS2_@PLT
	movl	16(%r14), %edx
	movl	%edx, %eax
	shrl	%eax
	andl	$1, %edx
	leaq	3(%rax,%rax,2), %rax
	leaq	(%r14,%rax,8), %rdi
	jne	.L935
	movq	(%rdi), %rdi
.L935:
	call	_ZN2v88internal8compiler4Node4KillEv@PLT
	jmp	.L936
	.p2align 4,,10
	.p2align 3
.L970:
	call	_ZN2v88internal8compiler18SimplifiedLowering16ToNumberOperatorEv
	movq	%rax, %r10
	jmp	.L921
	.p2align 4,,10
	.p2align 3
.L918:
	movq	24(%rbx), %rcx
	leaq	16(%rbx), %rax
	movq	16(%rbx), %rbx
	movq	%rcx, -256(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -248(%rbp)
	movq	24(%rax), %rcx
	addq	$32, %rax
	movq	%rcx, -224(%rbp)
	jmp	.L919
	.p2align 4,,10
	.p2align 3
.L974:
	movq	-184(%rbp), %rdi
	movq	%rbx, %rsi
	xorl	%edx, %edx
	call	_ZN2v88internal8compiler14NodeProperties19ReplaceControlInputEPNS1_4NodeES4_i@PLT
	movq	-200(%rbp), %rsi
	movq	-184(%rbp), %rdi
	xorl	%edx, %edx
	call	_ZN2v88internal8compiler14NodeProperties18ReplaceEffectInputEPNS1_4NodeES4_i@PLT
	movq	(%r14), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9IfSuccessEv@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	-200(%rbp), %rax
	movq	%rbx, %rdi
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -216(%rbp)
	jmp	.L928
	.p2align 4,,10
	.p2align 3
.L972:
	movq	40(%r14), %rax
	leaq	-176(%rbp), %r12
	testq	%rax, %rax
	jne	.L924
	movq	360(%rdx), %rsi
	movq	%r12, %rdi
	movl	$103, %edx
	movq	%r10, -216(%rbp)
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	(%r14), %rdi
	movq	-176(%rbp), %rsi
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	(%r14), %rdx
	movq	-216(%rbp), %r10
	movq	%rax, 40(%r14)
	jmp	.L924
	.p2align 4,,10
	.p2align 3
.L971:
	movq	32(%r14), %rax
	leaq	-176(%rbp), %r12
	testq	%rax, %rax
	jne	.L924
	movq	360(%rdx), %rsi
	movq	%r12, %rdi
	movl	$102, %edx
	movq	%r10, -216(%rbp)
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	(%r14), %rdi
	movq	-176(%rbp), %rsi
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	(%r14), %rdx
	movq	-216(%rbp), %r10
	movq	%rax, 32(%r14)
	jmp	.L924
	.p2align 4,,10
	.p2align 3
.L973:
	movq	360(%rdx), %rsi
	movq	%r12, %rdi
	movl	$104, %edx
	movq	%r10, -216(%rbp)
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	(%r14), %rdi
	movq	-176(%rbp), %rsi
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	(%r14), %rdx
	movq	-216(%rbp), %r10
	movq	%rax, 48(%r14)
	jmp	.L924
.L976:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23082:
	.size	_ZN2v88internal8compiler18SimplifiedLowering39DoJSToNumberOrNumericTruncatesToFloat64EPNS1_4NodeEPNS1_22RepresentationSelectorE, .-_ZN2v88internal8compiler18SimplifiedLowering39DoJSToNumberOrNumericTruncatesToFloat64EPNS1_4NodeEPNS1_22RepresentationSelectorE
	.section	.text._ZN2v88internal8compiler18SimplifiedLowering38DoJSToNumberOrNumericTruncatesToWord32EPNS1_4NodeEPNS1_22RepresentationSelectorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18SimplifiedLowering38DoJSToNumberOrNumericTruncatesToWord32EPNS1_4NodeEPNS1_22RepresentationSelectorE
	.type	_ZN2v88internal8compiler18SimplifiedLowering38DoJSToNumberOrNumericTruncatesToWord32EPNS1_4NodeEPNS1_22RepresentationSelectorE, @function
_ZN2v88internal8compiler18SimplifiedLowering38DoJSToNumberOrNumericTruncatesToWord32EPNS1_4NodeEPNS1_22RepresentationSelectorE:
.LFB23083:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$248, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -208(%rbp)
	movq	32(%rsi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L978
	movq	40(%rsi), %rax
	movq	%rax, -256(%rbp)
	movq	48(%rsi), %rax
	movq	%rax, -248(%rbp)
	movq	56(%rsi), %rax
	movq	%rax, -224(%rbp)
	leaq	64(%rsi), %rax
.L979:
	movq	(%rax), %rax
	leaq	-112(%rbp), %r15
	movq	%rax, -200(%rbp)
	movq	(%r14), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder11ObjectIsSmiEv@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%rbx, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$1, %edx
	movl	$1, %esi
	movq	%rax, -216(%rbp)
	movq	(%r14), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$2, %edx
	movq	-216(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r12, %rdi
	movhps	-200(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r12
	movq	(%r14), %rax
	movq	(%rax), %r10
	movq	8(%rax), %rdi
	movq	%r10, -200(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$1, %edx
	movq	-200(%rbp), %r10
	movq	%rax, %rsi
	movq	%r12, -112(%rbp)
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -232(%rbp)
	movq	(%r14), %rax
	movq	(%rax), %r10
	movq	376(%rax), %rdi
	movq	%r10, -200(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder25ChangeTaggedSignedToInt32Ev@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$1, %edx
	movq	-200(%rbp), %r10
	movq	%rax, %rsi
	movq	%rbx, -112(%rbp)
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -240(%rbp)
	movq	(%r14), %rax
	movq	(%rax), %r10
	movq	8(%rax), %rdi
	movq	%r10, -200(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$1, %edx
	movq	-200(%rbp), %r10
	movq	%rax, %rsi
	movq	%r12, -112(%rbp)
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r14, %rdi
	movq	%rax, -200(%rbp)
	movq	0(%r13), %rax
	cmpw	$705, 16(%rax)
	je	.L1030
	call	_ZN2v88internal8compiler18SimplifiedLowering17ToNumericOperatorEv
	movq	%rax, %r10
.L981:
	movq	0(%r13), %rax
	movq	(%r14), %rdx
	movzwl	16(%rax), %eax
	cmpl	$705, %eax
	je	.L1031
	cmpl	$706, %eax
	je	.L1032
	movq	48(%r14), %rax
	leaq	-176(%rbp), %r12
	testq	%rax, %rax
	je	.L1033
.L984:
	movq	%rbx, %xmm2
	movq	%rax, %xmm0
	movq	(%rdx), %rdi
	movq	%r10, %rsi
	punpcklqdq	%xmm2, %xmm0
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$6, %edx
	movaps	%xmm0, -112(%rbp)
	movq	-256(%rbp), %xmm0
	movhps	-248(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-224(%rbp), %xmm0
	movhps	-200(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	leaq	-184(%rbp), %rsi
	movq	%r13, %rdi
	movq	$0, -184(%rbp)
	movq	%rax, -200(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties17IsExceptionalCallEPNS1_4NodeEPS4_@PLT
	movq	-200(%rbp), %rbx
	movq	%rbx, -216(%rbp)
	testb	%al, %al
	jne	.L1034
.L988:
	movq	-200(%rbp), %xmm1
	movq	(%r14), %rax
	movq	376(%rax), %rdi
	punpcklqdq	%xmm1, %xmm1
	movq	(%rax), %rbx
	movaps	%xmm1, -288(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder11ObjectIsSmiEv@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	movq	-200(%rbp), %rax
	movl	$1, %edx
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%rax, -248(%rbp)
	movq	(%r14), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$2, %edx
	movq	-248(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%rbx, %rdi
	movhps	-216(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %rbx
	movq	(%r14), %rax
	movq	(%rax), %r10
	movq	8(%rax), %rdi
	movq	%r10, -216(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$1, %edx
	movq	-216(%rbp), %r10
	movq	%rax, %rsi
	movq	%rbx, -112(%rbp)
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -264(%rbp)
	movq	(%r14), %rax
	movq	(%rax), %r10
	movq	376(%rax), %rdi
	movq	%r10, -216(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder25ChangeTaggedSignedToInt32Ev@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$1, %edx
	movq	-216(%rbp), %r10
	movq	%rax, %rsi
	movq	-200(%rbp), %rax
	movq	%r10, %rdi
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -256(%rbp)
	movq	(%r14), %rax
	movq	(%rax), %r10
	movq	8(%rax), %rdi
	movq	%r10, -216(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	movq	%r15, %rcx
	movl	$1, %edx
	xorl	%r8d, %r8d
	movq	-216(%rbp), %r10
	movq	%rax, %rsi
	movq	%rbx, -112(%rbp)
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	movq	(%r14), %rax
	movq	(%rax), %r10
	movq	376(%rax), %r8
	movq	%r10, -248(%rbp)
	movq	%r8, -216(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder18ForHeapNumberValueEv@PLT
	movq	-216(%rbp), %r8
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9LoadFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$3, %edx
	movq	-248(%rbp), %r10
	movdqa	-288(%rbp), %xmm1
	movq	%rax, %rsi
	movq	%rbx, -96(%rbp)
	movq	%r10, %rdi
	movaps	%xmm1, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -216(%rbp)
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder23TruncateFloat64ToWord32Ev@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	-216(%rbp), %rax
	movq	%r12, %rdi
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$2, %esi
	movq	%rax, -248(%rbp)
	movq	(%r14), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
	movq	%rbx, %xmm3
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	-264(%rbp), %xmm0
	movl	$2, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$2, %esi
	movq	%rax, %rbx
	movq	(%r14), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9EffectPhiEi@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	-200(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$3, %edx
	movq	%rbx, -96(%rbp)
	movhps	-216(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$2, %edx
	movl	$4, %esi
	movq	%rax, -216(%rbp)
	movq	(%r14), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$3, %edx
	movq	-256(%rbp), %xmm0
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rbx, -96(%rbp)
	movhps	-248(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$2, %esi
	movq	%rax, -200(%rbp)
	movq	(%r14), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
	movq	%rbx, %xmm4
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	-232(%rbp), %xmm0
	movl	$2, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	punpcklqdq	%xmm4, %xmm0
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$2, %esi
	movq	%rax, %r12
	movq	(%r14), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9EffectPhiEi@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$3, %edx
	movq	-224(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%rbx, %rdi
	movq	%r12, -96(%rbp)
	movhps	-216(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$2, %edx
	movl	$4, %esi
	movq	%rax, -216(%rbp)
	movq	(%r14), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	movq	%r14, %rdi
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	-240(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$3, %edx
	movq	%r12, -96(%rbp)
	movhps	-200(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	24(%r13), %r14
	movq	%rax, -200(%rbp)
	testq	%r14, %r14
	je	.L989
	movq	(%r14), %rbx
	.p2align 4,,10
	.p2align 3
.L1001:
	movl	16(%r14), %ecx
	movq	%r14, %rdi
	movl	%ecx, %edx
	shrl	%edx
	leaq	3(%rdx,%rdx,2), %rax
	leaq	(%r14,%rax,8), %rax
	leaq	32(%rax), %rsi
	addq	$16, %rax
	andl	$1, %ecx
	cmovne	%rsi, %rax
	leaq	(%rax,%rdx,8), %r15
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler14NodeProperties13IsControlEdgeENS1_4EdgeE@PLT
	testb	%al, %al
	je	.L992
	movl	16(%r14), %ecx
	movl	%ecx, %eax
	shrl	%eax
	andl	$1, %ecx
	leaq	3(%rax,%rax,2), %rax
	leaq	(%r14,%rax,8), %rdi
	movq	(%rdi), %rax
	jne	.L993
	movq	%rax, %rdi
	movq	(%rax), %rax
.L993:
	cmpw	$6, 16(%rax)
	je	.L1035
	movq	(%r15), %rdi
	cmpq	%rdi, %r12
	je	.L996
	testq	%rdi, %rdi
	je	.L998
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L998:
	movq	%r12, (%r15)
	testq	%r12, %r12
	je	.L996
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	.p2align 4,,10
	.p2align 3
.L996:
	testq	%rbx, %rbx
	je	.L989
	movq	%rbx, %r14
	movq	(%rbx), %rbx
	jmp	.L1001
	.p2align 4,,10
	.p2align 3
.L989:
	movq	-200(%rbp), %rdx
	movq	-208(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler22RepresentationSelector16DeferReplacementEPNS1_4NodeES4_
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1036
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L992:
	.cfi_restore_state
	movq	%r14, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler14NodeProperties12IsEffectEdgeENS1_4EdgeE@PLT
	testb	%al, %al
	je	.L996
	movq	(%r15), %rdi
	cmpq	%rdi, -216(%rbp)
	je	.L996
	testq	%rdi, %rdi
	je	.L1000
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L1000:
	movq	-216(%rbp), %rax
	movq	%rax, (%r15)
	testq	%rax, %rax
	je	.L996
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	jmp	.L996
	.p2align 4,,10
	.p2align 3
.L1035:
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler4Node11ReplaceUsesEPS2_@PLT
	movl	16(%r14), %edx
	movl	%edx, %eax
	shrl	%eax
	andl	$1, %edx
	leaq	3(%rax,%rax,2), %rax
	leaq	(%r14,%rax,8), %rdi
	jne	.L995
	movq	(%rdi), %rdi
.L995:
	call	_ZN2v88internal8compiler4Node4KillEv@PLT
	jmp	.L996
	.p2align 4,,10
	.p2align 3
.L1030:
	call	_ZN2v88internal8compiler18SimplifiedLowering16ToNumberOperatorEv
	movq	%rax, %r10
	jmp	.L981
	.p2align 4,,10
	.p2align 3
.L978:
	movq	24(%rbx), %rcx
	leaq	16(%rbx), %rax
	movq	16(%rbx), %rbx
	movq	%rcx, -256(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -248(%rbp)
	movq	24(%rax), %rcx
	addq	$32, %rax
	movq	%rcx, -224(%rbp)
	jmp	.L979
	.p2align 4,,10
	.p2align 3
.L1034:
	movq	-184(%rbp), %rdi
	movq	%rbx, %rsi
	xorl	%edx, %edx
	call	_ZN2v88internal8compiler14NodeProperties19ReplaceControlInputEPNS1_4NodeES4_i@PLT
	movq	-200(%rbp), %rsi
	movq	-184(%rbp), %rdi
	xorl	%edx, %edx
	call	_ZN2v88internal8compiler14NodeProperties18ReplaceEffectInputEPNS1_4NodeES4_i@PLT
	movq	(%r14), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9IfSuccessEv@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	-200(%rbp), %rax
	movq	%rbx, %rdi
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -216(%rbp)
	jmp	.L988
	.p2align 4,,10
	.p2align 3
.L1032:
	movq	40(%r14), %rax
	leaq	-176(%rbp), %r12
	testq	%rax, %rax
	jne	.L984
	movq	360(%rdx), %rsi
	movq	%r12, %rdi
	movl	$103, %edx
	movq	%r10, -216(%rbp)
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	(%r14), %rdi
	movq	-176(%rbp), %rsi
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	(%r14), %rdx
	movq	-216(%rbp), %r10
	movq	%rax, 40(%r14)
	jmp	.L984
	.p2align 4,,10
	.p2align 3
.L1031:
	movq	32(%r14), %rax
	leaq	-176(%rbp), %r12
	testq	%rax, %rax
	jne	.L984
	movq	360(%rdx), %rsi
	movq	%r12, %rdi
	movl	$102, %edx
	movq	%r10, -216(%rbp)
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	(%r14), %rdi
	movq	-176(%rbp), %rsi
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	(%r14), %rdx
	movq	-216(%rbp), %r10
	movq	%rax, 32(%r14)
	jmp	.L984
	.p2align 4,,10
	.p2align 3
.L1033:
	movq	360(%rdx), %rsi
	movq	%r12, %rdi
	movl	$104, %edx
	movq	%r10, -216(%rbp)
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	(%r14), %rdi
	movq	-176(%rbp), %rsi
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	(%r14), %rdx
	movq	-216(%rbp), %r10
	movq	%rax, 48(%r14)
	jmp	.L984
.L1036:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23083:
	.size	_ZN2v88internal8compiler18SimplifiedLowering38DoJSToNumberOrNumericTruncatesToWord32EPNS1_4NodeEPNS1_22RepresentationSelectorE, .-_ZN2v88internal8compiler18SimplifiedLowering38DoJSToNumberOrNumericTruncatesToWord32EPNS1_4NodeEPNS1_22RepresentationSelectorE
	.section	.rodata._ZNSt5dequeIN2v88internal8compiler22RepresentationSelector9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_.str1.8,"aMS",@progbits,1
	.align 8
.LC23:
	.string	"cannot create std::deque larger than max_size()"
	.section	.text._ZNSt5dequeIN2v88internal8compiler22RepresentationSelector9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_,"axG",@progbits,_ZNSt5dequeIN2v88internal8compiler22RepresentationSelector9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt5dequeIN2v88internal8compiler22RepresentationSelector9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_
	.type	_ZNSt5dequeIN2v88internal8compiler22RepresentationSelector9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_, @function
_ZNSt5dequeIN2v88internal8compiler22RepresentationSelector9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_:
.LFB26918:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	80(%rdi), %rcx
	movq	64(%rdi), %rax
	leaq	-16(%rcx), %rdx
	cmpq	%rdx, %rax
	je	.L1038
	movq	(%rsi), %rcx
	movl	8(%rsi), %edx
	movq	%rcx, (%rax)
	movl	%edx, 8(%rax)
	addq	$16, 64(%rdi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1038:
	.cfi_restore_state
	movq	88(%rdi), %r13
	movq	56(%rdi), %rsi
	subq	72(%rdi), %rax
	movq	%r13, %r14
	sarq	$4, %rax
	subq	%rsi, %r14
	movq	%r14, %rcx
	sarq	$3, %rcx
	leaq	-1(%rcx), %rdx
	salq	$5, %rdx
	addq	%rax, %rdx
	movq	48(%rdi), %rax
	subq	32(%rdi), %rax
	sarq	$4, %rax
	addq	%rdx, %rax
	cmpq	$134217727, %rax
	je	.L1057
	movq	24(%rdi), %rdx
	movq	16(%rdi), %rdi
	movq	%r13, %rax
	subq	%rdi, %rax
	movq	%rdx, %r8
	sarq	$3, %rax
	subq	%rax, %r8
	cmpq	$1, %r8
	jbe	.L1058
.L1041:
	movq	8(%rbx), %rax
	testq	%rax, %rax
	je	.L1049
	cmpq	$31, 8(%rax)
	ja	.L1059
.L1049:
	movq	(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L1060
	leaq	512(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L1050:
	movq	%rax, 8(%r13)
	movl	8(%r12), %edx
	movq	(%r12), %rcx
	movq	64(%rbx), %rax
	movl	%edx, 8(%rax)
	movq	%rcx, (%rax)
	movq	88(%rbx), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 88(%rbx)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 72(%rbx)
	movq	%rdx, 80(%rbx)
	movq	%rax, 64(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1058:
	.cfi_restore_state
	leaq	2(%rcx), %r15
	leaq	(%r15,%r15), %rax
	cmpq	%rax, %rdx
	ja	.L1061
	testq	%rdx, %rdx
	movl	$1, %eax
	movq	(%rbx), %rdi
	cmovne	%rdx, %rax
	movq	16(%rdi), %r13
	leaq	2(%rdx,%rax), %rcx
	movq	24(%rdi), %rax
	leaq	0(,%rcx,8), %rsi
	subq	%r13, %rax
	cmpq	%rax, %rsi
	ja	.L1062
	addq	%r13, %rsi
	movq	%rsi, 16(%rdi)
.L1046:
	movq	%rcx, %rax
	movq	56(%rbx), %rsi
	subq	%r15, %rax
	shrq	%rax
	leaq	0(%r13,%rax,8), %r15
	movq	88(%rbx), %rax
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L1047
	subq	%rsi, %rdx
	movq	%r15, %rdi
	movq	%rcx, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %rcx
.L1047:
	movq	24(%rbx), %rax
	leaq	0(,%rax,8), %rdx
	cmpq	$15, %rdx
	jbe	.L1048
	movq	16(%rbx), %rdx
	movq	%rax, 8(%rdx)
	movq	$0, (%rdx)
.L1048:
	movq	%r13, 16(%rbx)
	movq	%rcx, 24(%rbx)
.L1044:
	movq	%r15, 56(%rbx)
	movq	(%r15), %rax
	leaq	(%r15,%r14), %r13
	movq	(%r15), %xmm0
	movq	%r13, 88(%rbx)
	addq	$512, %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 40(%rbx)
	movq	0(%r13), %rax
	movq	%rax, 72(%rbx)
	addq	$512, %rax
	movq	%rax, 80(%rbx)
	jmp	.L1041
	.p2align 4,,10
	.p2align 3
.L1061:
	subq	%r15, %rdx
	addq	$8, %r13
	shrq	%rdx
	leaq	(%rdi,%rdx,8), %r15
	movq	%r13, %rdx
	subq	%rsi, %rdx
	cmpq	%r15, %rsi
	jbe	.L1043
	cmpq	%r13, %rsi
	je	.L1044
	movq	%r15, %rdi
	call	memmove@PLT
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L1059:
	movq	(%rax), %rdx
	movq	%rdx, 8(%rbx)
	jmp	.L1050
	.p2align 4,,10
	.p2align 3
.L1043:
	cmpq	%r13, %rsi
	je	.L1044
	leaq	8(%r14), %rdi
	subq	%rdx, %rdi
	addq	%r15, %rdi
	call	memmove@PLT
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L1060:
	movl	$512, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1050
.L1062:
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	movq	%rax, %r13
	jmp	.L1046
.L1057:
	leaq	.LC23(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE26918:
	.size	_ZNSt5dequeIN2v88internal8compiler22RepresentationSelector9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_, .-_ZNSt5dequeIN2v88internal8compiler22RepresentationSelector9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_
	.section	.text._ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_,"axG",@progbits,_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	.type	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_, @function
_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_:
.LFB26926:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	88(%rdi), %r13
	movq	56(%rdi), %rsi
	movq	64(%rbx), %rdx
	subq	72(%rbx), %rdx
	movq	%r13, %r14
	movq	%rdx, %rcx
	subq	%rsi, %r14
	sarq	$3, %rcx
	movq	%r14, %rdi
	sarq	$3, %rdi
	leaq	-1(%rdi), %rax
	salq	$6, %rax
	leaq	(%rcx,%rax), %rdx
	movq	48(%rbx), %rax
	subq	32(%rbx), %rax
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpq	$268435455, %rax
	je	.L1081
	movq	16(%rbx), %rcx
	movq	24(%rbx), %rdx
	movq	%r13, %rax
	subq	%rcx, %rax
	movq	%rdx, %r8
	sarq	$3, %rax
	subq	%rax, %r8
	cmpq	$1, %r8
	jbe	.L1082
.L1065:
	movq	8(%rbx), %rax
	testq	%rax, %rax
	je	.L1073
	cmpq	$63, 8(%rax)
	ja	.L1083
.L1073:
	movq	(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L1084
	leaq	512(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L1074:
	movq	%rax, 8(%r13)
	movq	(%r12), %rdx
	movq	64(%rbx), %rax
	movq	%rdx, (%rax)
	movq	88(%rbx), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 88(%rbx)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 72(%rbx)
	movq	%rdx, 80(%rbx)
	movq	%rax, 64(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1082:
	.cfi_restore_state
	leaq	2(%rdi), %r15
	leaq	(%r15,%r15), %rax
	cmpq	%rax, %rdx
	ja	.L1085
	testq	%rdx, %rdx
	movl	$1, %eax
	movq	(%rbx), %rdi
	cmovne	%rdx, %rax
	movq	16(%rdi), %r13
	leaq	2(%rdx,%rax), %rcx
	movq	24(%rdi), %rax
	leaq	0(,%rcx,8), %rsi
	subq	%r13, %rax
	cmpq	%rax, %rsi
	ja	.L1086
	addq	%r13, %rsi
	movq	%rsi, 16(%rdi)
.L1070:
	movq	%rcx, %rax
	movq	56(%rbx), %rsi
	subq	%r15, %rax
	shrq	%rax
	leaq	0(%r13,%rax,8), %r15
	movq	88(%rbx), %rax
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L1071
	subq	%rsi, %rdx
	movq	%r15, %rdi
	movq	%rcx, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %rcx
.L1071:
	movq	24(%rbx), %rax
	leaq	0(,%rax,8), %rdx
	cmpq	$15, %rdx
	jbe	.L1072
	movq	16(%rbx), %rdx
	movq	%rax, 8(%rdx)
	movq	$0, (%rdx)
.L1072:
	movq	%r13, 16(%rbx)
	movq	%rcx, 24(%rbx)
.L1068:
	movq	%r15, 56(%rbx)
	movq	(%r15), %rax
	leaq	(%r15,%r14), %r13
	movq	(%r15), %xmm0
	movq	%r13, 88(%rbx)
	addq	$512, %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 40(%rbx)
	movq	0(%r13), %rax
	movq	%rax, 72(%rbx)
	addq	$512, %rax
	movq	%rax, 80(%rbx)
	jmp	.L1065
	.p2align 4,,10
	.p2align 3
.L1083:
	movq	(%rax), %rdx
	movq	%rdx, 8(%rbx)
	jmp	.L1074
	.p2align 4,,10
	.p2align 3
.L1085:
	subq	%r15, %rdx
	addq	$8, %r13
	shrq	%rdx
	leaq	(%rcx,%rdx,8), %r15
	movq	%r13, %rdx
	subq	%rsi, %rdx
	cmpq	%r15, %rsi
	jbe	.L1067
	cmpq	%r13, %rsi
	je	.L1068
	movq	%r15, %rdi
	call	memmove@PLT
	jmp	.L1068
	.p2align 4,,10
	.p2align 3
.L1067:
	cmpq	%r13, %rsi
	je	.L1068
	leaq	8(%r14), %rdi
	subq	%rdx, %rdi
	addq	%r15, %rdi
	call	memmove@PLT
	jmp	.L1068
	.p2align 4,,10
	.p2align 3
.L1084:
	movl	$512, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1074
	.p2align 4,,10
	.p2align 3
.L1086:
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	movq	%rax, %r13
	jmp	.L1070
.L1081:
	leaq	.LC23(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE26926:
	.size	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_, .-_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	.section	.rodata._ZN2v88internal8compiler22RepresentationSelector12EnqueueInputEPNS1_4NodeEiNS1_7UseInfoE.str1.1,"aMS",@progbits,1
.LC24:
	.string	"  initial #%i: "
.LC25:
	.string	"   queue #%i?: "
.LC26:
	.string	"   added: "
.LC27:
	.string	" inqueue: "
	.section	.text._ZN2v88internal8compiler22RepresentationSelector12EnqueueInputEPNS1_4NodeEiNS1_7UseInfoE,"axG",@progbits,_ZN2v88internal8compiler22RepresentationSelector12EnqueueInputEPNS1_4NodeEiNS1_7UseInfoE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler22RepresentationSelector12EnqueueInputEPNS1_4NodeEiNS1_7UseInfoE
	.type	_ZN2v88internal8compiler22RepresentationSelector12EnqueueInputEPNS1_4NodeEiNS1_7UseInfoE, @function
_ZN2v88internal8compiler22RepresentationSelector12EnqueueInputEPNS1_4NodeEiNS1_7UseInfoE:
.LFB22997:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edx, %rdx
	salq	$3, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movzbl	20(%rbp), %r13d
	movl	24(%rbp), %r14d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1088
	leaq	32(%rsi), %rcx
	addq	%rcx, %rdx
.L1089:
	movq	(%rdx), %rax
	movl	120(%rbx), %edx
	movq	%rax, -64(%rbp)
	testl	%edx, %edx
	jne	.L1087
	movl	20(%rax), %esi
	andl	$16777215, %esi
	movl	%esi, %eax
	leaq	(%rax,%rax,4), %rdx
	movq	32(%rbx), %rax
	leaq	(%rax,%rdx,8), %r12
	cmpb	$0, (%r12)
	je	.L1112
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	jne	.L1113
.L1098:
	movl	8(%r12), %edi
	movq	%rdi, %rax
	movzbl	4(%r12), %edi
	salq	$32, %rax
	orq	%rax, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector15PrintTruncationENS1_10TruncationE.isra.0
	movl	8(%r12), %edx
	movzbl	4(%r12), %r15d
	movl	%r14d, %esi
	movl	%edx, %edi
	movl	%edx, -68(%rbp)
	call	_ZN2v88internal8compiler10Truncation23GeneralizeIdentifyZerosENS1_13IdentifyZerosES3_@PLT
	movl	%r13d, %esi
	movl	%r15d, %edi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler10Truncation10GeneralizeENS2_14TruncationKindES3_@PLT
	movl	-68(%rbp), %edx
	movl	%r14d, 8(%r12)
	movb	%al, 4(%r12)
	cmpl	%r14d, %edx
	jne	.L1106
	cmpb	%al, %r15b
	je	.L1087
.L1106:
	cmpb	$3, (%r12)
	jne	.L1114
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	jne	.L1115
.L1104:
	salq	$32, %r14
	movzbl	%al, %edi
	orq	%r14, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector15PrintTruncationENS1_10TruncationE.isra.0
.L1087:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1116
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1088:
	.cfi_restore_state
	movq	32(%rsi), %rax
	leaq	16(%rax,%rdx), %rdx
	jmp	.L1089
	.p2align 4,,10
	.p2align 3
.L1113:
	leaq	.LC25(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1098
	.p2align 4,,10
	.p2align 3
.L1114:
	movq	216(%rbx), %rax
	movq	200(%rbx), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L1101
	movq	-64(%rbp), %rax
	movq	%rax, (%rdx)
	addq	$8, 200(%rbx)
.L1102:
	movb	$3, (%r12)
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	jne	.L1103
.L1111:
	movzbl	4(%r12), %eax
	movl	8(%r12), %r14d
	jmp	.L1104
	.p2align 4,,10
	.p2align 3
.L1112:
	movb	$3, (%r12)
	movq	72(%rbx), %rsi
	cmpq	80(%rbx), %rsi
	je	.L1092
	movq	-64(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 72(%rbx)
.L1093:
	movq	216(%rbx), %rax
	movq	200(%rbx), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L1094
	movq	-64(%rbp), %rax
	movq	%rax, (%rdx)
	addq	$8, 200(%rbx)
.L1095:
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	jne	.L1117
.L1096:
	movzbl	4(%r12), %r15d
	movl	8(%r12), %edi
	movl	%r14d, %esi
	call	_ZN2v88internal8compiler10Truncation23GeneralizeIdentifyZerosENS1_13IdentifyZerosES3_@PLT
	movl	%r15d, %edi
	movl	%r13d, %esi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler10Truncation10GeneralizeENS2_14TruncationKindES3_@PLT
	movl	%ebx, 8(%r12)
	salq	$32, %rbx
	movb	%al, 4(%r12)
	movzbl	%al, %edi
	orq	%rbx, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector15PrintTruncationENS1_10TruncationE.isra.0
	jmp	.L1087
	.p2align 4,,10
	.p2align 3
.L1115:
	leaq	.LC27(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1111
	.p2align 4,,10
	.p2align 3
.L1117:
	movq	-64(%rbp), %rax
	leaq	.LC24(%rip), %rdi
	movl	20(%rax), %esi
	xorl	%eax, %eax
	andl	$16777215, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1096
	.p2align 4,,10
	.p2align 3
.L1103:
	xorl	%eax, %eax
	leaq	.LC26(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movzbl	4(%r12), %eax
	movl	8(%r12), %r14d
	jmp	.L1104
	.p2align 4,,10
	.p2align 3
.L1094:
	leaq	-64(%rbp), %rsi
	leaq	136(%rbx), %rdi
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	jmp	.L1095
	.p2align 4,,10
	.p2align 3
.L1092:
	leaq	-64(%rbp), %rdx
	leaq	56(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1093
	.p2align 4,,10
	.p2align 3
.L1101:
	leaq	-64(%rbp), %rsi
	leaq	136(%rbx), %rdi
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	jmp	.L1102
.L1116:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22997:
	.size	_ZN2v88internal8compiler22RepresentationSelector12EnqueueInputEPNS1_4NodeEiNS1_7UseInfoE, .-_ZN2v88internal8compiler22RepresentationSelector12EnqueueInputEPNS1_4NodeEiNS1_7UseInfoE
	.section	.text._ZN2v88internal8compiler22RepresentationSelector12ProcessInputEPNS1_4NodeEiNS1_7UseInfoE,"axG",@progbits,_ZN2v88internal8compiler22RepresentationSelector12ProcessInputEPNS1_4NodeEiNS1_7UseInfoE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler22RepresentationSelector12ProcessInputEPNS1_4NodeEiNS1_7UseInfoE
	.type	_ZN2v88internal8compiler22RepresentationSelector12ProcessInputEPNS1_4NodeEiNS1_7UseInfoE, @function
_ZN2v88internal8compiler22RepresentationSelector12ProcessInputEPNS1_4NodeEiNS1_7UseInfoE:
.LFB23014:
	.cfi_startproc
	endbr64
	movl	120(%rdi), %eax
	testl	%eax, %eax
	je	.L1119
	cmpl	$2, %eax
	jne	.L1123
	xorl	%ecx, %ecx
	jmp	_ZN2v88internal8compiler22RepresentationSelector12ConvertInputEPNS1_4NodeEiNS1_7UseInfoENS1_4TypeE
	.p2align 4,,10
	.p2align 3
.L1123:
	ret
	.p2align 4,,10
	.p2align 3
.L1119:
	jmp	_ZN2v88internal8compiler22RepresentationSelector12EnqueueInputEPNS1_4NodeEiNS1_7UseInfoE
	.cfi_endproc
.LFE23014:
	.size	_ZN2v88internal8compiler22RepresentationSelector12ProcessInputEPNS1_4NodeEiNS1_7UseInfoE, .-_ZN2v88internal8compiler22RepresentationSelector12ProcessInputEPNS1_4NodeEiNS1_7UseInfoE
	.section	.text._ZN2v88internal8compiler22RepresentationSelector15VisitFrameStateEPNS1_4NodeE,"axG",@progbits,_ZN2v88internal8compiler22RepresentationSelector15VisitFrameStateEPNS1_4NodeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler22RepresentationSelector15VisitFrameStateEPNS1_4NodeE
	.type	_ZN2v88internal8compiler22RepresentationSelector15VisitFrameStateEPNS1_4NodeE, @function
_ZN2v88internal8compiler22RepresentationSelector15VisitFrameStateEPNS1_4NodeE:
.LFB23038:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$168, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	120(%rdi), %eax
	movb	$8, -96(%rbp)
	movb	$5, -92(%rbp)
	movl	$1, -88(%rbp)
	movb	$0, -84(%rbp)
	movq	$0, -80(%rbp)
	movl	$-1, -72(%rbp)
	testl	%eax, %eax
	je	.L1125
	cmpl	$2, %eax
	jne	.L1131
	pushq	-72(%rbp)
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	-80(%rbp)
	pushq	-88(%rbp)
	pushq	-96(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12ConvertInputEPNS1_4NodeEiNS1_7UseInfoENS1_4TypeE
	movl	120(%rbx), %eax
	addq	$32, %rsp
.L1128:
	movb	$8, -96(%rbp)
	movb	$5, -92(%rbp)
	movl	$1, -88(%rbp)
	movb	$0, -84(%rbp)
	movq	$0, -80(%rbp)
	movl	$-1, -72(%rbp)
	testl	%eax, %eax
	jne	.L1199
	pushq	-72(%rbp)
	movl	$1, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	pushq	-80(%rbp)
	pushq	-88(%rbp)
	pushq	-96(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12EnqueueInputEPNS1_4NodeEiNS1_7UseInfoE
	movl	120(%rbx), %eax
	addq	$32, %rsp
.L1131:
	movl	20(%r12), %edx
	leaq	32(%r12), %r14
	movl	%edx, %ecx
	xorl	$251658240, %ecx
	andl	$251658240, %ecx
	leaq	48(%r12), %rcx
	jne	.L1133
	movq	32(%r12), %rsi
	leaq	32(%rsi), %rcx
.L1133:
	movq	(%rcx), %r13
	testl	%eax, %eax
	je	.L1200
	cmpl	$2, %eax
	je	.L1201
.L1176:
	andl	$16777215, %edx
	leaq	(%rdx,%rdx,4), %rcx
	movq	32(%rbx), %rdx
	leaq	(%rdx,%rcx,8), %rdx
.L1181:
	cmpl	$1, %eax
	jne	.L1124
	movb	$8, 1(%rdx)
.L1124:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1202
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1200:
	.cfi_restore_state
	movl	20(%r13), %eax
	movq	32(%rbx), %rdi
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rax
	leaq	(%rdi,%rax,8), %rax
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L1135
	movq	8(%r13), %rax
.L1135:
	movq	%rax, -96(%rbp)
	cmpq	$134217729, %rax
	je	.L1138
	leaq	-96(%rbp), %rdi
	movl	$134217729, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L1203
.L1138:
	movb	$8, -192(%rbp)
	movb	$5, -188(%rbp)
	movl	$1, -184(%rbp)
	movb	$0, -180(%rbp)
	movq	$0, -176(%rbp)
	movl	$-1, -168(%rbp)
	pushq	-168(%rbp)
	pushq	-176(%rbp)
	pushq	-184(%rbp)
	pushq	-192(%rbp)
.L1195:
	movl	$2, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector12EnqueueInputEPNS1_4NodeEiNS1_7UseInfoE
	movl	120(%rbx), %eax
	addq	$32, %rsp
.L1137:
	movb	$8, -96(%rbp)
	movb	$5, -92(%rbp)
	movl	$1, -88(%rbp)
	movb	$0, -84(%rbp)
	movq	$0, -80(%rbp)
	movl	$-1, -72(%rbp)
	testl	%eax, %eax
	jne	.L1204
	pushq	-72(%rbp)
	movl	$3, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	pushq	-80(%rbp)
	pushq	-88(%rbp)
	pushq	-96(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12EnqueueInputEPNS1_4NodeEiNS1_7UseInfoE
	movl	120(%rbx), %eax
	addq	$32, %rsp
.L1169:
	movb	$8, -96(%rbp)
	movb	$5, -92(%rbp)
	movl	$1, -88(%rbp)
	movb	$0, -84(%rbp)
	movq	$0, -80(%rbp)
	movl	$-1, -72(%rbp)
	testl	%eax, %eax
	jne	.L1205
	pushq	-72(%rbp)
	movl	$4, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	pushq	-80(%rbp)
	pushq	-88(%rbp)
	pushq	-96(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12EnqueueInputEPNS1_4NodeEiNS1_7UseInfoE
	movl	120(%rbx), %eax
	addq	$32, %rsp
.L1173:
	movb	$8, -96(%rbp)
	movb	$5, -92(%rbp)
	movl	$1, -88(%rbp)
	movb	$0, -84(%rbp)
	movq	$0, -80(%rbp)
	movl	$-1, -72(%rbp)
	testl	%eax, %eax
	jne	.L1206
	pushq	-72(%rbp)
	movl	$5, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	pushq	-80(%rbp)
	pushq	-88(%rbp)
	pushq	-96(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12EnqueueInputEPNS1_4NodeEiNS1_7UseInfoE
	movl	120(%rbx), %eax
	addq	$32, %rsp
.L1177:
	movl	20(%r12), %edx
	andl	$16777215, %edx
	leaq	(%rdx,%rdx,4), %rcx
	movq	32(%rbx), %rdx
	leaq	(%rdx,%rcx,8), %rdx
	testl	%eax, %eax
	jne	.L1181
	movl	$4294967295, %eax
	movq	%rax, 16(%rdx)
	jmp	.L1124
	.p2align 4,,10
	.p2align 3
.L1204:
	cmpl	$2, %eax
	jne	.L1198
	pushq	-72(%rbp)
	xorl	%ecx, %ecx
	movl	$3, %edx
	movq	%r12, %rsi
	pushq	-80(%rbp)
	movq	%rbx, %rdi
	pushq	-88(%rbp)
	pushq	-96(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12ConvertInputEPNS1_4NodeEiNS1_7UseInfoENS1_4TypeE
	movl	120(%rbx), %eax
	addq	$32, %rsp
	jmp	.L1169
	.p2align 4,,10
	.p2align 3
.L1199:
	cmpl	$2, %eax
	jne	.L1131
	pushq	-72(%rbp)
	xorl	%ecx, %ecx
	movl	$1, %edx
	movq	%r12, %rsi
	pushq	-80(%rbp)
	movq	%rbx, %rdi
	pushq	-88(%rbp)
	pushq	-96(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12ConvertInputEPNS1_4NodeEiNS1_7UseInfoENS1_4TypeE
	movl	120(%rbx), %eax
	addq	$32, %rsp
	jmp	.L1131
	.p2align 4,,10
	.p2align 3
.L1205:
	cmpl	$2, %eax
	jne	.L1198
	pushq	-72(%rbp)
	xorl	%ecx, %ecx
	movl	$4, %edx
	movq	%r12, %rsi
	pushq	-80(%rbp)
	movq	%rbx, %rdi
	pushq	-88(%rbp)
	pushq	-96(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12ConvertInputEPNS1_4NodeEiNS1_7UseInfoENS1_4TypeE
	movl	120(%rbx), %eax
	addq	$32, %rsp
	jmp	.L1173
	.p2align 4,,10
	.p2align 3
.L1206:
	cmpl	$2, %eax
	jne	.L1198
	pushq	-72(%rbp)
	xorl	%ecx, %ecx
	movl	$5, %edx
	movq	%r12, %rsi
	pushq	-80(%rbp)
	movq	%rbx, %rdi
	pushq	-88(%rbp)
	pushq	-96(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12ConvertInputEPNS1_4NodeEiNS1_7UseInfoENS1_4TypeE
	movl	120(%rbx), %eax
	addq	$32, %rsp
	jmp	.L1177
	.p2align 4,,10
	.p2align 3
.L1125:
	pushq	-72(%rbp)
	xorl	%edx, %edx
	pushq	-80(%rbp)
	pushq	-88(%rbp)
	pushq	-96(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12EnqueueInputEPNS1_4NodeEiNS1_7UseInfoE
	movl	120(%rbx), %eax
	addq	$32, %rsp
	jmp	.L1128
	.p2align 4,,10
	.p2align 3
.L1203:
	movb	$0, -160(%rbp)
	movb	$5, -156(%rbp)
	movl	$1, -152(%rbp)
	movb	$0, -148(%rbp)
	movq	$0, -144(%rbp)
	movl	$-1, -136(%rbp)
	pushq	-136(%rbp)
	pushq	-144(%rbp)
	pushq	-152(%rbp)
	pushq	-160(%rbp)
	jmp	.L1195
	.p2align 4,,10
	.p2align 3
.L1201:
	movl	20(%r13), %eax
	movq	32(%rbx), %rsi
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rax
	leaq	(%rsi,%rax,8), %rax
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L1140
	movq	8(%r13), %rax
.L1140:
	movq	%rax, -96(%rbp)
	cmpq	$134217729, %rax
	je	.L1141
	leaq	-96(%rbp), %rdi
	movl	$134217729, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1141
.L1143:
	movq	(%rbx), %rdi
	movq	(%rdi), %rax
	movq	(%rax), %r8
	movq	%r8, -200(%rbp)
	call	_ZN2v88internal8compiler7JSGraph20OptimizedOutConstantEv@PLT
	movq	-200(%rbp), %r8
	cmpq	%rax, %r13
	je	.L1207
	movq	16(%r8), %r15
	movq	24(%r8), %rax
	subq	%r15, %rax
	cmpq	$31, %rax
	jbe	.L1208
	leaq	32(%r15), %rax
	movq	%rax, 16(%r8)
.L1146:
	movq	%r8, (%r15)
	movq	$0, 8(%r15)
	movq	$0, 16(%r15)
	movq	$0, 24(%r15)
	movq	16(%r8), %rax
	movq	24(%r8), %rdx
	subq	%rax, %rdx
	cmpq	$7, %rdx
	jbe	.L1209
	leaq	8(%rax), %rdx
	movq	%rdx, 16(%r8)
.L1148:
	leaq	2(%rax), %rdx
	xorl	%ecx, %ecx
	movq	%rax, 8(%r15)
	movq	%rdx, 24(%r15)
	movw	%cx, (%rax)
	movq	%rdx, 16(%r15)
	movl	20(%r13), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rdx
	movq	32(%rbx), %rax
	leaq	(%rax,%rdx,8), %rdx
	movq	24(%rdx), %rax
	testq	%rax, %rax
	jne	.L1149
	movq	8(%r13), %rax
.L1149:
	movzbl	1(%rdx), %r13d
	movq	8(%r15), %rdx
	movq	%rax, -160(%rbp)
	cmpq	$1, %rax
	je	.L1183
	leal	-6(%r13), %ecx
	cmpb	$2, %cl
	ja	.L1210
.L1151:
	movl	$7, %eax
	movl	$8, %r13d
.L1150:
	movb	%r13b, (%rdx)
	movb	%al, 1(%rdx)
	movzbl	23(%r12), %eax
	movq	(%rbx), %rdx
	andl	$15, %eax
	movq	(%rdx), %r13
	cmpl	$15, %eax
	je	.L1160
	leaq	16(%r14), %rax
.L1161:
	movq	(%rax), %rcx
	movq	8(%rdx), %rdi
	movq	%r15, %rsi
	xorl	%edx, %edx
	movq	%rcx, -200(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder16TypedStateValuesEPKNS0_10ZoneVectorINS0_11MachineTypeEEENS1_15SparseInputMaskE@PLT
	movq	%r13, %rdi
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	-200(%rbp), %rcx
	movq	%rax, %rsi
	movq	%rcx, -64(%rbp)
	leaq	-64(%rbp), %rcx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r13
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1162
	movq	16(%r14), %rdi
	cmpq	%r13, %rdi
	je	.L1196
	addq	$16, %r14
	movq	%r12, %rsi
.L1164:
	leaq	-72(%rsi), %r15
	testq	%rdi, %rdi
	je	.L1165
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L1165:
	movq	%r13, (%r14)
	testq	%r13, %r13
	je	.L1196
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L1196:
	movl	120(%rbx), %eax
	jmp	.L1137
	.p2align 4,,10
	.p2align 3
.L1141:
	movb	$8, -128(%rbp)
	xorl	%ecx, %ecx
	movl	$2, %edx
	movq	%r12, %rsi
	movb	$5, -124(%rbp)
	movq	%rbx, %rdi
	movl	$1, -120(%rbp)
	movb	$0, -116(%rbp)
	movq	$0, -112(%rbp)
	movl	$-1, -104(%rbp)
	pushq	-104(%rbp)
	pushq	-112(%rbp)
	pushq	-120(%rbp)
	pushq	-128(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12ConvertInputEPNS1_4NodeEiNS1_7UseInfoENS1_4TypeE
	addq	$32, %rsp
	jmp	.L1143
	.p2align 4,,10
	.p2align 3
.L1198:
	movl	20(%r12), %edx
	jmp	.L1176
	.p2align 4,,10
	.p2align 3
.L1183:
	xorl	%eax, %eax
	xorl	%r13d, %r13d
	jmp	.L1150
	.p2align 4,,10
	.p2align 3
.L1162:
	movq	32(%r12), %rsi
	movq	32(%rsi), %rdi
	cmpq	%r13, %rdi
	je	.L1196
	leaq	32(%rsi), %r14
	jmp	.L1164
	.p2align 4,,10
	.p2align 3
.L1160:
	movq	32(%r12), %rax
	addq	$32, %rax
	jmp	.L1161
	.p2align 4,,10
	.p2align 3
.L1210:
	leal	-9(%r13), %ecx
	cmpb	$2, %cl
	ja	.L1211
	movl	$7, %eax
	movl	$11, %r13d
	jmp	.L1150
	.p2align 4,,10
	.p2align 3
.L1207:
	movq	(%rbx), %rdi
	call	_ZN2v88internal8compiler7JSGraph26SingleDeadTypedStateValuesEv@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_
	movl	120(%rbx), %eax
	jmp	.L1137
	.p2align 4,,10
	.p2align 3
.L1209:
	movl	$8, %esi
	movq	%r8, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1148
	.p2align 4,,10
	.p2align 3
.L1208:
	movq	%r8, %rdi
	movl	$32, %esi
	movq	%r8, -200(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-200(%rbp), %r8
	movq	%rax, %r15
	jmp	.L1146
	.p2align 4,,10
	.p2align 3
.L1211:
	cmpb	$5, %r13b
	jne	.L1152
	cmpq	$134217729, %rax
	je	.L1151
	leaq	-160(%rbp), %rdi
	movl	$134217729, %esi
	movq	%rdx, -200(%rbp)
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	movq	-200(%rbp), %rdx
	testb	%al, %al
	jne	.L1151
	movl	$4, %eax
	jmp	.L1150
.L1152:
	movq	%rax, -96(%rbp)
	cmpq	$1099, %rax
	je	.L1157
	leaq	-96(%rbp), %rdi
	movl	$1099, %esi
	movq	%rdx, -200(%rbp)
	movq	%rdi, -208(%rbp)
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	movq	-200(%rbp), %rdx
	testb	%al, %al
	jne	.L1157
	cmpq	$1031, -96(%rbp)
	movq	-208(%rbp), %rdi
	je	.L1159
	movl	$1031, %esi
	movq	%rdx, -200(%rbp)
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	movq	-200(%rbp), %rdx
	testb	%al, %al
	jne	.L1159
	movl	$7, %eax
	jmp	.L1150
	.p2align 4,,10
	.p2align 3
.L1157:
	movl	$2, %eax
	jmp	.L1150
.L1159:
	movl	$3, %eax
	jmp	.L1150
.L1202:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23038:
	.size	_ZN2v88internal8compiler22RepresentationSelector15VisitFrameStateEPNS1_4NodeE, .-_ZN2v88internal8compiler22RepresentationSelector15VisitFrameStateEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE,"axG",@progbits,_ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE
	.type	_ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE, @function
_ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE:
.LFB23020:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	%rsi, -104(%rbp)
	movq	40(%rbp), %rdi
	movl	%edx, -140(%rbp)
	movq	32(%rbp), %rsi
	movq	%rcx, -152(%rbp)
	movq	24(%rbp), %rdx
	movl	120(%rbx), %ecx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rbp), %rax
	movq	%rdx, -88(%rbp)
	movq	%rsi, -80(%rbp)
	movq	%rax, -96(%rbp)
	movq	%rdi, -72(%rbp)
	testl	%ecx, %ecx
	je	.L1213
	cmpl	$2, %ecx
	jne	.L1219
	pushq	%rdi
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	pushq	%rsi
	movq	-104(%rbp), %rsi
	pushq	%rdx
	xorl	%edx, %edx
	pushq	%rax
	call	_ZN2v88internal8compiler22RepresentationSelector12ConvertInputEPNS1_4NodeEiNS1_7UseInfoENS1_4TypeE
	movl	120(%rbx), %ecx
	addq	$32, %rsp
.L1216:
	movq	48(%rbp), %rax
	movq	56(%rbp), %rdx
	movq	64(%rbp), %rsi
	movq	72(%rbp), %rdi
	movq	%rax, -96(%rbp)
	movq	%rdx, -88(%rbp)
	movq	%rsi, -80(%rbp)
	movq	%rdi, -72(%rbp)
	testl	%ecx, %ecx
	jne	.L1253
	pushq	%rdi
	movq	%rbx, %rdi
	pushq	%rsi
	movq	-104(%rbp), %rsi
	pushq	%rdx
	movl	$1, %edx
	pushq	%rax
	call	_ZN2v88internal8compiler22RepresentationSelector12EnqueueInputEPNS1_4NodeEiNS1_7UseInfoE
	movl	120(%rbx), %ecx
	addq	$32, %rsp
.L1219:
	movq	-104(%rbp), %rax
	xorl	%r15d, %r15d
	movq	32(%rbx), %rdx
	movl	20(%rax), %esi
	addq	$48, %rax
	movq	%rax, -128(%rbp)
	movq	%rbx, %rax
	movq	%r12, %rbx
	movq	%r15, %r12
	movq	%rax, %r15
	jmp	.L1240
	.p2align 4,,10
	.p2align 3
.L1256:
	leal	2(%r12), %edi
	cmpl	%edi, %eax
	jle	.L1222
	movq	-128(%rbp), %rax
	leaq	(%rax,%r12,8), %rax
.L1244:
	movq	(%rax), %rax
	movq	%rax, -96(%rbp)
	testl	%ecx, %ecx
	jne	.L1239
	movl	20(%rax), %esi
	andl	$16777215, %esi
	movl	%esi, %eax
	leaq	(%rax,%rax,4), %rax
	leaq	(%rdx,%rax,8), %r13
	cmpb	$0, 0(%r13)
	je	.L1254
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	jne	.L1255
.L1231:
	movb	4(%r13), %bl
	movl	8(%r13), %eax
	movl	%ebx, %r9d
	salq	$32, %rax
	movq	%r9, %rbx
	orq	%rax, %rbx
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector15PrintTruncationENS1_10TruncationE.isra.0
	movl	8(%r13), %r11d
	movzbl	4(%r13), %r14d
	xorl	%esi, %esi
	movl	%r11d, %edi
	movl	%r11d, -112(%rbp)
	call	_ZN2v88internal8compiler10Truncation23GeneralizeIdentifyZerosENS1_13IdentifyZerosES3_@PLT
	xorl	%esi, %esi
	movl	%r14d, %edi
	movl	%eax, -108(%rbp)
	call	_ZN2v88internal8compiler10Truncation10GeneralizeENS2_14TruncationKindES3_@PLT
	movl	-108(%rbp), %edx
	movl	-112(%rbp), %r11d
	movb	%al, 4(%r13)
	movl	%edx, 8(%r13)
	cmpl	%edx, %r11d
	jne	.L1246
	cmpb	%al, %r14b
	je	.L1232
.L1246:
	cmpb	$3, 0(%r13)
	je	.L1234
	movq	216(%r15), %rax
	movq	200(%r15), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L1235
	movq	-96(%rbp), %rax
	movq	%rax, (%rdx)
	addq	$8, 200(%r15)
.L1236:
	movb	$3, 0(%r13)
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	jne	.L1237
.L1251:
	movzbl	4(%r13), %eax
	movl	8(%r13), %edx
.L1238:
	movb	%al, -120(%rbp)
	movl	-120(%rbp), %r13d
	salq	$32, %rdx
	orq	%rdx, %r13
	movq	%r13, -120(%rbp)
	movq	%r13, %rdi
.L1252:
	call	_ZN2v88internal8compiler22RepresentationSelector15PrintTruncationENS1_10TruncationE.isra.0
.L1232:
	movq	-104(%rbp), %rax
	movq	32(%r15), %rdx
	movl	120(%r15), %ecx
	movl	20(%rax), %esi
.L1239:
	addq	$1, %r12
.L1240:
	movl	%esi, %eax
	shrl	$24, %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1256
	movq	-104(%rbp), %rax
	leal	2(%r12), %edi
	movq	32(%rax), %rax
	cmpl	%edi, 8(%rax)
	jle	.L1222
	leaq	32(%rax,%r12,8), %rax
	jmp	.L1244
	.p2align 4,,10
	.p2align 3
.L1255:
	leaq	.LC25(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1231
	.p2align 4,,10
	.p2align 3
.L1234:
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	je	.L1238
	leaq	.LC27(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1251
	.p2align 4,,10
	.p2align 3
.L1254:
	movb	$3, 0(%r13)
	movq	72(%r15), %rsi
	cmpq	80(%r15), %rsi
	je	.L1226
	movq	-96(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 72(%r15)
.L1227:
	movq	216(%r15), %rax
	movq	200(%r15), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L1228
	movq	-96(%rbp), %rax
	movq	%rax, (%rdx)
	addq	$8, 200(%r15)
.L1229:
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	jne	.L1257
.L1230:
	movzbl	4(%r13), %r11d
	movl	8(%r13), %edi
	xorl	%esi, %esi
	movb	%r11b, -108(%rbp)
	call	_ZN2v88internal8compiler10Truncation23GeneralizeIdentifyZerosENS1_13IdentifyZerosES3_@PLT
	movzbl	-108(%rbp), %r11d
	xorl	%esi, %esi
	movl	%eax, %r14d
	movl	%r11d, %edi
	call	_ZN2v88internal8compiler10Truncation10GeneralizeENS2_14TruncationKindES3_@PLT
	movl	%r14d, 8(%r13)
	salq	$32, %r14
	movb	%al, -136(%rbp)
	movb	%al, 4(%r13)
	movl	-136(%rbp), %eax
	orq	%r14, %rax
	movq	%rax, -136(%rbp)
	movq	%rax, %rdi
	jmp	.L1252
	.p2align 4,,10
	.p2align 3
.L1222:
	andl	$16777215, %esi
	leaq	(%rsi,%rsi,4), %rax
	leaq	(%rdx,%rax,8), %rax
	testl	%ecx, %ecx
	je	.L1241
	cmpl	$1, %ecx
	je	.L1242
.L1212:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1258
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1242:
	.cfi_restore_state
	movzbl	-140(%rbp), %ecx
	movb	%cl, 1(%rax)
	jmp	.L1212
	.p2align 4,,10
	.p2align 3
.L1253:
	cmpl	$2, %ecx
	jne	.L1219
	pushq	%rdi
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	pushq	%rsi
	movq	-104(%rbp), %rsi
	pushq	%rdx
	movl	$1, %edx
	pushq	%rax
	call	_ZN2v88internal8compiler22RepresentationSelector12ConvertInputEPNS1_4NodeEiNS1_7UseInfoENS1_4TypeE
	movl	120(%rbx), %ecx
	addq	$32, %rsp
	jmp	.L1219
	.p2align 4,,10
	.p2align 3
.L1257:
	movq	-96(%rbp), %rax
	leaq	.LC24(%rip), %rdi
	movl	20(%rax), %esi
	xorl	%eax, %eax
	andl	$16777215, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1230
	.p2align 4,,10
	.p2align 3
.L1237:
	xorl	%eax, %eax
	leaq	.LC26(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movzbl	4(%r13), %eax
	movl	8(%r13), %edx
	jmp	.L1238
	.p2align 4,,10
	.p2align 3
.L1241:
	movq	-152(%rbp), %rcx
	movq	%rcx, 16(%rax)
	jmp	.L1212
	.p2align 4,,10
	.p2align 3
.L1213:
	pushq	%rdi
	movq	%rbx, %rdi
	pushq	%rsi
	movq	-104(%rbp), %rsi
	pushq	%rdx
	xorl	%edx, %edx
	pushq	%rax
	call	_ZN2v88internal8compiler22RepresentationSelector12EnqueueInputEPNS1_4NodeEiNS1_7UseInfoE
	movl	120(%rbx), %ecx
	addq	$32, %rsp
	jmp	.L1216
	.p2align 4,,10
	.p2align 3
.L1228:
	leaq	-96(%rbp), %rsi
	leaq	136(%r15), %rdi
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	jmp	.L1229
	.p2align 4,,10
	.p2align 3
.L1226:
	leaq	-96(%rbp), %rdx
	leaq	56(%r15), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1227
	.p2align 4,,10
	.p2align 3
.L1235:
	leaq	-96(%rbp), %rsi
	leaq	136(%r15), %rdi
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	jmp	.L1236
.L1258:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23020:
	.size	_ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE, .-_ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE
	.section	.text._ZN2v88internal8compiler22RepresentationSelector17VisitFloat64BinopEPNS1_4NodeE,"axG",@progbits,_ZN2v88internal8compiler22RepresentationSelector17VisitFloat64BinopEPNS1_4NodeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler22RepresentationSelector17VisitFloat64BinopEPNS1_4NodeE
	.type	_ZN2v88internal8compiler22RepresentationSelector17VisitFloat64BinopEPNS1_4NodeE, @function
_ZN2v88internal8compiler22RepresentationSelector17VisitFloat64BinopEPNS1_4NodeE:
.LFB23025:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$4294967295, %ecx
	movl	$13, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movb	$13, -32(%rbp)
	movb	$4, -28(%rbp)
	movl	$1, -24(%rbp)
	movb	$0, -20(%rbp)
	movq	$0, -16(%rbp)
	movl	$-1, -8(%rbp)
	pushq	-8(%rbp)
	pushq	-16(%rbp)
	pushq	-24(%rbp)
	pushq	-32(%rbp)
	pushq	-8(%rbp)
	pushq	-16(%rbp)
	pushq	-24(%rbp)
	pushq	-32(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE
	addq	$64, %rsp
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23025:
	.size	_ZN2v88internal8compiler22RepresentationSelector17VisitFloat64BinopEPNS1_4NodeE, .-_ZN2v88internal8compiler22RepresentationSelector17VisitFloat64BinopEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler22RepresentationSelector26VisitWord32TruncatingBinopEPNS1_4NodeE,"axG",@progbits,_ZN2v88internal8compiler22RepresentationSelector26VisitWord32TruncatingBinopEPNS1_4NodeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler22RepresentationSelector26VisitWord32TruncatingBinopEPNS1_4NodeE
	.type	_ZN2v88internal8compiler22RepresentationSelector26VisitWord32TruncatingBinopEPNS1_4NodeE, @function
_ZN2v88internal8compiler22RepresentationSelector26VisitWord32TruncatingBinopEPNS1_4NodeE:
.LFB23027:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$4294967295, %ecx
	movl	$4, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movb	$4, -32(%rbp)
	movb	$2, -28(%rbp)
	movl	$0, -24(%rbp)
	movb	$0, -20(%rbp)
	movq	$0, -16(%rbp)
	movl	$-1, -8(%rbp)
	pushq	-8(%rbp)
	pushq	-16(%rbp)
	pushq	-24(%rbp)
	pushq	-32(%rbp)
	pushq	-8(%rbp)
	pushq	-16(%rbp)
	pushq	-24(%rbp)
	pushq	-32(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE
	addq	$64, %rsp
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23027:
	.size	_ZN2v88internal8compiler22RepresentationSelector26VisitWord32TruncatingBinopEPNS1_4NodeE, .-_ZN2v88internal8compiler22RepresentationSelector26VisitWord32TruncatingBinopEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler22RepresentationSelector15VisitInt64BinopEPNS1_4NodeE,"axG",@progbits,_ZN2v88internal8compiler22RepresentationSelector15VisitInt64BinopEPNS1_4NodeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler22RepresentationSelector15VisitInt64BinopEPNS1_4NodeE
	.type	_ZN2v88internal8compiler22RepresentationSelector15VisitInt64BinopEPNS1_4NodeE, @function
_ZN2v88internal8compiler22RepresentationSelector15VisitInt64BinopEPNS1_4NodeE:
.LFB23026:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$4294967295, %ecx
	movl	$5, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movb	$5, -32(%rbp)
	movb	$5, -28(%rbp)
	movl	$1, -24(%rbp)
	movb	$0, -20(%rbp)
	movq	$0, -16(%rbp)
	movl	$-1, -8(%rbp)
	pushq	-8(%rbp)
	pushq	-16(%rbp)
	pushq	-24(%rbp)
	pushq	-32(%rbp)
	pushq	-8(%rbp)
	pushq	-16(%rbp)
	pushq	-24(%rbp)
	pushq	-32(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE
	addq	$64, %rsp
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23026:
	.size	_ZN2v88internal8compiler22RepresentationSelector15VisitInt64BinopEPNS1_4NodeE, .-_ZN2v88internal8compiler22RepresentationSelector15VisitInt64BinopEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler22RepresentationSelector16VisitCheckBoundsEPNS1_4NodeEPNS1_18SimplifiedLoweringE,"axG",@progbits,_ZN2v88internal8compiler22RepresentationSelector16VisitCheckBoundsEPNS1_4NodeEPNS1_18SimplifiedLoweringE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler22RepresentationSelector16VisitCheckBoundsEPNS1_4NodeEPNS1_18SimplifiedLoweringE
	.type	_ZN2v88internal8compiler22RepresentationSelector16VisitCheckBoundsEPNS1_4NodeEPNS1_18SimplifiedLoweringE, @function
_ZN2v88internal8compiler22RepresentationSelector16VisitCheckBoundsEPNS1_4NodeEPNS1_18SimplifiedLoweringE:
.LFB23058:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$176, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler17CheckParametersOfEPKNS1_8OperatorE@PLT
	movzbl	23(%r12), %edx
	movq	%rax, %r13
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L1266
	movq	32(%r12), %rsi
	leaq	32(%r12), %rax
.L1298:
	movl	20(%rsi), %edx
	movq	32(%rbx), %rcx
	andl	$16777215, %edx
	leaq	(%rdx,%rdx,4), %rdx
	movq	24(%rcx,%rdx,8), %rdx
	testq	%rdx, %rdx
	jne	.L1296
	movq	8(%rsi), %rdx
.L1296:
	addq	$8, %rax
	movq	%rdx, -192(%rbp)
	movq	(%rax), %rdx
	movl	20(%rdx), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rax
	movq	24(%rcx,%rax,8), %rax
	testq	%rax, %rax
	jne	.L1272
	movq	8(%rdx), %rax
.L1272:
	movq	%rax, -184(%rbp)
	cmpq	$1027, %rax
	jne	.L1299
.L1273:
	cmpq	$3151, -192(%rbp)
	jne	.L1300
.L1276:
	movb	$4, -80(%rbp)
	movl	$4294967295, %ecx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movb	$2, -76(%rbp)
	movl	$4, %edx
	movl	$0, -72(%rbp)
	movb	$0, -68(%rbp)
	movq	$0, -64(%rbp)
	movl	$-1, -56(%rbp)
	pushq	-56(%rbp)
	pushq	-64(%rbp)
	pushq	-72(%rbp)
	pushq	-80(%rbp)
	pushq	-56(%rbp)
	pushq	-64(%rbp)
	pushq	-72(%rbp)
	pushq	-80(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE
	addq	$64, %rsp
	cmpl	$2, 120(%rbx)
	je	.L1301
.L1265:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1302
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1300:
	.cfi_restore_state
	leaq	-192(%rbp), %rdi
	movl	$3151, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1276
	movq	0(%r13), %rdx
	movl	8(%r13), %eax
	movb	$4, -176(%rbp)
	movl	$4294967295, %ecx
	movb	$2, -172(%rbp)
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rdx, -128(%rbp)
	movl	$4, %edx
	movl	$0, -168(%rbp)
	movb	$0, -164(%rbp)
	movq	$0, -160(%rbp)
	movl	$-1, -152(%rbp)
	movb	$4, -144(%rbp)
	pushq	-152(%rbp)
	movb	$5, -140(%rbp)
	pushq	-160(%rbp)
	movl	$0, -136(%rbp)
	pushq	-168(%rbp)
	movb	$2, -132(%rbp)
	pushq	-176(%rbp)
	movl	%eax, -120(%rbp)
	pushq	-120(%rbp)
	pushq	-128(%rbp)
	pushq	-136(%rbp)
	pushq	-144(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE
	addq	$64, %rsp
	cmpl	$2, 120(%rbx)
	jne	.L1265
	movq	(%rbx), %rax
	movl	$1, %edx
	movq	376(%rax), %rdi
	jmp	.L1297
	.p2align 4,,10
	.p2align 3
.L1299:
	leaq	-184(%rbp), %rdi
	movl	$1027, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1273
	movq	0(%r13), %rdx
	movl	8(%r13), %eax
	movb	$5, -112(%rbp)
	movq	%r12, %rsi
	movb	$5, -108(%rbp)
	movl	$4294967295, %ecx
	movq	%rbx, %rdi
	movq	%rdx, -64(%rbp)
	movl	$5, %edx
	movl	$1, -104(%rbp)
	movb	$0, -100(%rbp)
	movq	$0, -96(%rbp)
	movl	$-1, -88(%rbp)
	movb	$5, -80(%rbp)
	pushq	-88(%rbp)
	movb	$5, -76(%rbp)
	pushq	-96(%rbp)
	movl	$0, -72(%rbp)
	pushq	-104(%rbp)
	movb	$3, -68(%rbp)
	pushq	-112(%rbp)
	movl	%eax, -56(%rbp)
	pushq	-56(%rbp)
	pushq	-64(%rbp)
	pushq	-72(%rbp)
	pushq	-80(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE
	addq	$64, %rsp
	cmpl	$2, 120(%rbx)
	jne	.L1265
	movq	(%rbx), %rax
	movq	%r13, %rsi
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder19CheckedUint64BoundsERKNS1_14FeedbackSourceE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	jmp	.L1265
	.p2align 4,,10
	.p2align 3
.L1266:
	movq	32(%r12), %rdx
	movq	16(%rdx), %rsi
	leaq	16(%rdx), %rax
	jmp	.L1298
	.p2align 4,,10
	.p2align 3
.L1301:
	cmpl	$1, 96(%r14)
	je	.L1279
.L1281:
	movl	$1, %edx
.L1280:
	movq	(%rbx), %rax
	movq	376(%rax), %rdi
.L1297:
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder19CheckedUint32BoundsERKNS1_14FeedbackSourceENS1_21CheckBoundsParameters4ModeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	jmp	.L1265
	.p2align 4,,10
	.p2align 3
.L1279:
	xorl	%edx, %edx
	cmpq	$1, -192(%rbp)
	je	.L1280
	cmpq	$1, -184(%rbp)
	je	.L1280
	leaq	-192(%rbp), %r14
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	comisd	.LC2(%rip), %xmm0
	jb	.L1281
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type3MaxEv@PLT
	leaq	-184(%rbp), %rdi
	movsd	%xmm0, -200(%rbp)
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	xorl	%edx, %edx
	comisd	-200(%rbp), %xmm0
	setbe	%dl
	jmp	.L1280
.L1302:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23058:
	.size	_ZN2v88internal8compiler22RepresentationSelector16VisitCheckBoundsEPNS1_4NodeEPNS1_18SimplifiedLoweringE, .-_ZN2v88internal8compiler22RepresentationSelector16VisitCheckBoundsEPNS1_4NodeEPNS1_18SimplifiedLoweringE
	.section	.text._ZN2v88internal8compiler22RepresentationSelector26VisitSpeculativeAdditiveOpEPNS1_4NodeENS1_10TruncationEPNS1_18SimplifiedLoweringE,"axG",@progbits,_ZN2v88internal8compiler22RepresentationSelector26VisitSpeculativeAdditiveOpEPNS1_4NodeENS1_10TruncationEPNS1_18SimplifiedLoweringE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler22RepresentationSelector26VisitSpeculativeAdditiveOpEPNS1_4NodeENS1_10TruncationEPNS1_18SimplifiedLoweringE
	.type	_ZN2v88internal8compiler22RepresentationSelector26VisitSpeculativeAdditiveOpEPNS1_4NodeENS1_10TruncationEPNS1_18SimplifiedLoweringE, @function
_ZN2v88internal8compiler22RepresentationSelector26VisitSpeculativeAdditiveOpEPNS1_4NodeENS1_10TruncationEPNS1_18SimplifiedLoweringE:
.LFB23055:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	32(%rsi), %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$72, %rsp
	movq	32(%rsi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	344(%rdi), %rax
	movq	392(%rax), %r14
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1304
	movq	8(%rdx), %rax
	movq	%rax, -104(%rbp)
	cmpq	%rax, %r14
	jne	.L1309
.L1305:
	leaq	8(%r15), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rax
	movq	%rax, -96(%rbp)
	cmpq	%r14, %rax
	je	.L1312
.L1329:
	leaq	-96(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1312
.L1310:
	movb	$13, -96(%rbp)
	movl	$7263, %ecx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movb	$5, -92(%rbp)
	movl	$13, %edx
	movl	$1, -88(%rbp)
	movb	$5, -84(%rbp)
	movq	$0, -80(%rbp)
	movl	$-1, -72(%rbp)
	pushq	-72(%rbp)
	pushq	-80(%rbp)
	pushq	-88(%rbp)
	pushq	-96(%rbp)
	pushq	-72(%rbp)
	pushq	-80(%rbp)
	pushq	-88(%rbp)
	pushq	-96(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE
	addq	$64, %rsp
	cmpl	$2, 120(%r13)
	je	.L1327
.L1303:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1328
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1304:
	.cfi_restore_state
	leaq	16(%rdx), %rax
	movq	16(%rdx), %rdx
	movq	8(%rdx), %rdx
	movq	%rdx, -104(%rbp)
	cmpq	%r14, %rdx
	je	.L1308
.L1309:
	leaq	-104(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L1310
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1305
	movq	32(%r12), %rax
	addq	$16, %rax
.L1308:
	addq	$8, %rax
	movq	(%rax), %rax
	movq	8(%rax), %rax
	movq	%rax, -96(%rbp)
	cmpq	%r14, %rax
	jne	.L1329
.L1312:
	movq	8(%r12), %rax
	movq	%rax, -104(%rbp)
	cmpq	$1099, %rax
	je	.L1316
	leaq	-104(%rbp), %rdi
	movl	$1099, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1316
	movq	8(%r12), %rax
	movq	%rax, -96(%rbp)
	cmpq	$1031, %rax
	je	.L1316
	leaq	-96(%rbp), %rdi
	movl	$1031, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L1330
	.p2align 4,,10
	.p2align 3
.L1316:
	movb	$4, -96(%rbp)
	movl	$4294967295, %ecx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movb	$2, -92(%rbp)
	movl	$4, %edx
	movl	$0, -88(%rbp)
	movb	$0, -84(%rbp)
	movq	$0, -80(%rbp)
	movl	$-1, -72(%rbp)
	pushq	-72(%rbp)
	pushq	-80(%rbp)
	pushq	-88(%rbp)
	pushq	-96(%rbp)
	pushq	-72(%rbp)
	pushq	-80(%rbp)
	pushq	-88(%rbp)
	pushq	-96(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE
	addq	$64, %rsp
	cmpl	$2, 120(%r13)
	jne	.L1303
	movq	(%r12), %rax
	movq	128(%r13), %rdi
	movzwl	16(%rax), %esi
	call	_ZN2v88internal8compiler21RepresentationChanger16Int32OperatorForENS1_8IrOpcode5ValueE@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler22RepresentationSelector14ChangeToPureOpEPNS1_4NodeEPKNS1_8OperatorE
	jmp	.L1303
	.p2align 4,,10
	.p2align 3
.L1327:
	movq	(%r12), %rax
	movq	128(%r13), %rdi
	movzwl	16(%rax), %esi
	call	_ZN2v88internal8compiler21RepresentationChanger18Float64OperatorForENS1_8IrOpcode5ValueE@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler22RepresentationSelector14ChangeToPureOpEPNS1_4NodeEPKNS1_8OperatorE
	jmp	.L1303
	.p2align 4,,10
	.p2align 3
.L1330:
	movl	$2, %esi
	movl	%ebx, %edi
	call	_ZN2v88internal8compiler10Truncation11LessGeneralENS2_14TruncationKindES3_@PLT
	testb	%al, %al
	je	.L1310
	jmp	.L1316
.L1328:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23055:
	.size	_ZN2v88internal8compiler22RepresentationSelector26VisitSpeculativeAdditiveOpEPNS1_4NodeENS1_10TruncationEPNS1_18SimplifiedLoweringE, .-_ZN2v88internal8compiler22RepresentationSelector26VisitSpeculativeAdditiveOpEPNS1_4NodeENS1_10TruncationEPNS1_18SimplifiedLoweringE
	.section	.text._ZN2v88internal8compiler22RepresentationSelector29VisitSpeculativeNumberModulusEPNS1_4NodeENS1_10TruncationEPNS1_18SimplifiedLoweringE,"axG",@progbits,_ZN2v88internal8compiler22RepresentationSelector29VisitSpeculativeNumberModulusEPNS1_4NodeENS1_10TruncationEPNS1_18SimplifiedLoweringE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler22RepresentationSelector29VisitSpeculativeNumberModulusEPNS1_4NodeENS1_10TruncationEPNS1_18SimplifiedLoweringE
	.type	_ZN2v88internal8compiler22RepresentationSelector29VisitSpeculativeNumberModulusEPNS1_4NodeENS1_10TruncationEPNS1_18SimplifiedLoweringE, @function
_ZN2v88internal8compiler22RepresentationSelector29VisitSpeculativeNumberModulusEPNS1_4NodeENS1_10TruncationEPNS1_18SimplifiedLoweringE:
.LFB23056:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	32(%rsi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdx, %rax
	movq	32(%rsi), %rdx
	shrq	$32, %rax
	movq	%rax, -152(%rbp)
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1332
	movq	8(%rdx), %rax
	movq	%rax, -128(%rbp)
	cmpq	$7175, %rax
	jne	.L1337
.L1333:
	leaq	8(%r14), %rax
.L1339:
	movq	(%rax), %rax
	movq	8(%rax), %rax
	movq	%rax, -96(%rbp)
	cmpq	$7175, %rax
	je	.L1340
	leaq	-96(%rbp), %rdi
	movl	$7175, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L1338
.L1340:
	movl	$2, %esi
	movl	%ebx, %edi
	call	_ZN2v88internal8compiler10Truncation11LessGeneralENS2_14TruncationKindES3_@PLT
	testb	%al, %al
	jne	.L1344
	movq	8(%r15), %rax
	movq	%rax, -96(%rbp)
	cmpq	$1031, %rax
	jne	.L1458
.L1344:
	movb	$4, -96(%rbp)
	movl	$4294967295, %ecx
	movb	$2, -92(%rbp)
	movl	$0, -88(%rbp)
	movb	$0, -84(%rbp)
	movq	$0, -80(%rbp)
	movl	$-1, -72(%rbp)
	pushq	-72(%rbp)
	pushq	-80(%rbp)
	pushq	-88(%rbp)
	pushq	-96(%rbp)
	pushq	-72(%rbp)
	pushq	-80(%rbp)
	pushq	-88(%rbp)
	pushq	-96(%rbp)
.L1453:
	movl	$4, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE
	addq	$64, %rsp
	cmpl	$2, 120(%r12)
	je	.L1459
.L1331:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1460
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1332:
	.cfi_restore_state
	leaq	16(%rdx), %rax
	movq	16(%rdx), %rdx
	movq	8(%rdx), %rdx
	movq	%rdx, -128(%rbp)
	cmpq	$7175, %rdx
	je	.L1336
.L1337:
	leaq	-128(%rbp), %rdi
	movl	$7175, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L1338
	movzbl	23(%r15), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1333
	movq	32(%r15), %rax
	addq	$16, %rax
.L1336:
	addq	$8, %rax
	jmp	.L1339
	.p2align 4,,10
	.p2align 3
.L1458:
	leaq	-96(%rbp), %rdi
	movl	$1031, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1344
	.p2align 4,,10
	.p2align 3
.L1338:
	movzbl	23(%r15), %eax
	movq	32(%r15), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1346
	movq	8(%rdx), %rax
	movq	%rax, -128(%rbp)
	cmpq	$7243, %rax
	jne	.L1351
.L1347:
	leaq	8(%r14), %rax
.L1352:
	movq	(%rax), %rax
	movq	8(%rax), %rax
	movq	%rax, -96(%rbp)
	cmpq	$7243, %rax
	je	.L1353
	leaq	-96(%rbp), %rdi
	movl	$7243, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L1356
.L1353:
	movl	$2, %esi
	movl	%ebx, %edi
	call	_ZN2v88internal8compiler10Truncation11LessGeneralENS2_14TruncationKindES3_@PLT
	testb	%al, %al
	jne	.L1354
	movq	8(%r15), %rax
	movq	%rax, -96(%rbp)
	cmpq	$1099, %rax
	jne	.L1461
.L1354:
	movb	$4, -96(%rbp)
	movl	$4294967295, %ecx
	movb	$2, -92(%rbp)
	movl	$0, -88(%rbp)
	movb	$0, -84(%rbp)
	movq	$0, -80(%rbp)
	movl	$-1, -72(%rbp)
	pushq	-72(%rbp)
	pushq	-80(%rbp)
	pushq	-88(%rbp)
	pushq	-96(%rbp)
	pushq	-72(%rbp)
	pushq	-80(%rbp)
	pushq	-88(%rbp)
	pushq	-96(%rbp)
.L1456:
	movl	$4, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE
	addq	$64, %rsp
	cmpl	$2, 120(%r12)
	jne	.L1331
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18SimplifiedLowering8Int32ModEPNS1_4NodeE
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler22RepresentationSelector16DeferReplacementEPNS1_4NodeES4_
	jmp	.L1331
	.p2align 4,,10
	.p2align 3
.L1461:
	leaq	-96(%rbp), %rdi
	movl	$1099, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1354
	.p2align 4,,10
	.p2align 3
.L1356:
	movq	(%r15), %rdi
	call	_ZN2v88internal8compiler21NumberOperationHintOfEPKNS1_8OperatorE@PLT
	movl	$1031, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movb	%al, -153(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector13BothInputsAreEPNS1_4NodeENS1_4TypeE
	movzbl	-153(%rbp), %ecx
	andl	$-3, %ecx
	testb	%al, %al
	je	.L1462
	testb	%cl, %cl
	je	.L1463
	movl	$1099, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector13BothInputsAreEPNS1_4NodeENS1_4TypeE
.L1362:
	movzbl	23(%r15), %eax
	movq	32(%r15), %rsi
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1368
	movq	16(%rsi), %rsi
.L1368:
	movl	20(%rsi), %edx
	movq	32(%r12), %rcx
	andl	$16777215, %edx
	leaq	(%rdx,%rdx,4), %rdx
	movq	24(%rcx,%rdx,8), %rdx
	testq	%rdx, %rdx
	jne	.L1369
	movq	8(%rsi), %rdx
.L1369:
	movq	%rdx, -144(%rbp)
	cmpq	$1031, %rdx
	je	.L1370
	leaq	-144(%rbp), %rdi
	movl	$1031, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L1371
	movzbl	23(%r15), %eax
	movq	32(%r12), %rcx
	andl	$15, %eax
.L1370:
	cmpl	$15, %eax
	je	.L1372
	leaq	8(%r14), %rax
.L1373:
	movq	(%rax), %rdx
	movl	20(%rdx), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rax
	movq	24(%rcx,%rax,8), %rax
	testq	%rax, %rax
	jne	.L1374
	movq	8(%rdx), %rax
.L1374:
	movq	%rax, -128(%rbp)
	cmpq	$1031, %rax
	je	.L1378
	leaq	-128(%rbp), %rdi
	movl	$1031, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L1371
.L1378:
	movl	$2, %esi
	movl	%ebx, %edi
	call	_ZN2v88internal8compiler10Truncation11LessGeneralENS2_14TruncationKindES3_@PLT
	testb	%al, %al
	jne	.L1376
	movq	8(%r15), %rax
	movq	%rax, -96(%rbp)
	cmpq	$1031, %rax
	jne	.L1464
.L1376:
	movb	$4, -96(%rbp)
	movl	$7263, %ecx
	movb	$2, -92(%rbp)
	movl	$0, -88(%rbp)
	movb	$0, -84(%rbp)
	movq	$0, -80(%rbp)
	movl	$-1, -72(%rbp)
	pushq	-72(%rbp)
	pushq	-80(%rbp)
	pushq	-88(%rbp)
	pushq	-96(%rbp)
	pushq	-72(%rbp)
	pushq	-80(%rbp)
	pushq	-88(%rbp)
	pushq	-96(%rbp)
	jmp	.L1453
	.p2align 4,,10
	.p2align 3
.L1346:
	leaq	16(%rdx), %rax
	movq	16(%rdx), %rdx
	movq	8(%rdx), %rdx
	movq	%rdx, -128(%rbp)
	cmpq	$7243, %rdx
	je	.L1350
.L1351:
	leaq	-128(%rbp), %rdi
	movl	$7243, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L1356
	movzbl	23(%r15), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1347
	movq	32(%r15), %rax
	addq	$16, %rax
.L1350:
	addq	$8, %rax
	jmp	.L1352
	.p2align 4,,10
	.p2align 3
.L1464:
	leaq	-96(%rbp), %rdi
	movl	$1031, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1376
.L1371:
	movzbl	23(%r15), %eax
	movq	32(%r15), %rsi
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1380
	movq	16(%rsi), %rsi
.L1380:
	movl	20(%rsi), %edx
	movq	32(%r12), %rcx
	andl	$16777215, %edx
	leaq	(%rdx,%rdx,4), %rdx
	movq	24(%rcx,%rdx,8), %rdx
	testq	%rdx, %rdx
	jne	.L1382
	movq	8(%rsi), %rdx
.L1382:
	movq	%rdx, -144(%rbp)
	cmpq	$1099, %rdx
	je	.L1383
	leaq	-144(%rbp), %rdi
	movl	$1099, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L1384
	movzbl	23(%r15), %eax
	movq	32(%r12), %rcx
	andl	$15, %eax
.L1383:
	addq	$8, %r14
	cmpl	$15, %eax
	jne	.L1386
	movq	32(%r15), %rax
	leaq	24(%rax), %r14
.L1386:
	movq	(%r14), %rdx
	movl	20(%rdx), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rax
	movq	24(%rcx,%rax,8), %rax
	testq	%rax, %rax
	jne	.L1387
	movq	8(%rdx), %rax
.L1387:
	movq	%rax, -128(%rbp)
	cmpq	$1099, %rax
	jne	.L1388
.L1391:
	movl	$2, %esi
	movl	%ebx, %edi
	call	_ZN2v88internal8compiler10Truncation11LessGeneralENS2_14TruncationKindES3_@PLT
	testb	%al, %al
	jne	.L1389
	movq	8(%r15), %rax
	movq	%rax, -96(%rbp)
	cmpq	$1099, %rax
	jne	.L1465
.L1389:
	movb	$4, -96(%rbp)
	movl	$7263, %ecx
	movb	$2, -92(%rbp)
	movl	$0, -88(%rbp)
	movb	$0, -84(%rbp)
	movq	$0, -80(%rbp)
	movl	$-1, -72(%rbp)
	pushq	-72(%rbp)
	pushq	-80(%rbp)
	pushq	-88(%rbp)
	pushq	-96(%rbp)
	pushq	-72(%rbp)
	pushq	-80(%rbp)
	pushq	-88(%rbp)
	pushq	-96(%rbp)
	jmp	.L1456
	.p2align 4,,10
	.p2align 3
.L1465:
	leaq	-96(%rbp), %rdi
	movl	$1099, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1389
.L1384:
	movl	-152(%rbp), %eax
	movb	$13, -128(%rbp)
	movq	%r15, %rsi
	movq	%r12, %rdi
	movb	$5, -124(%rbp)
	movl	$7263, %ecx
	movl	$13, %edx
	movl	%eax, -120(%rbp)
	movb	$5, -116(%rbp)
	movq	$0, -112(%rbp)
	movl	$-1, -104(%rbp)
	movb	$13, -96(%rbp)
	movb	$5, -92(%rbp)
	movl	$0, -88(%rbp)
	movb	$5, -84(%rbp)
	movq	$0, -80(%rbp)
	movl	$-1, -72(%rbp)
	pushq	-72(%rbp)
	pushq	-80(%rbp)
	pushq	-88(%rbp)
	pushq	-96(%rbp)
	pushq	-104(%rbp)
	pushq	-112(%rbp)
	pushq	-120(%rbp)
	pushq	-128(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE
	addq	$64, %rsp
	cmpl	$2, 120(%r12)
	jne	.L1331
	movq	(%r15), %rax
	movq	128(%r12), %rdi
	movzwl	16(%rax), %esi
	call	_ZN2v88internal8compiler21RepresentationChanger18Float64OperatorForENS1_8IrOpcode5ValueE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler22RepresentationSelector14ChangeToPureOpEPNS1_4NodeEPKNS1_8OperatorE
	jmp	.L1331
	.p2align 4,,10
	.p2align 3
.L1462:
	movl	$1099, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movb	%cl, -154(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector13BothInputsAreEPNS1_4NodeENS1_4TypeE
	movzbl	-154(%rbp), %ecx
	testb	%al, %al
	je	.L1361
	testb	%cl, %cl
	jne	.L1362
	movb	$4, -96(%rbp)
	movb	$2, -92(%rbp)
	movl	$0, -88(%rbp)
	movb	$0, -84(%rbp)
	movq	$0, -80(%rbp)
	movl	$-1, -72(%rbp)
	pushq	-72(%rbp)
	pushq	-80(%rbp)
	pushq	-88(%rbp)
	pushq	-96(%rbp)
	pushq	-72(%rbp)
	pushq	-80(%rbp)
	pushq	-88(%rbp)
	pushq	-96(%rbp)
.L1451:
	movl	$1099, %ecx
	movl	$4, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE
	addq	$64, %rsp
	cmpl	$2, 120(%r12)
	jne	.L1331
	movq	(%r15), %rax
	movq	128(%r12), %rdi
	movzwl	16(%rax), %esi
	call	_ZN2v88internal8compiler21RepresentationChanger24Int32OverflowOperatorForENS1_8IrOpcode5ValueE@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	jmp	.L1331
	.p2align 4,,10
	.p2align 3
.L1459:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18SimplifiedLowering9Uint32ModEPNS1_4NodeE
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler22RepresentationSelector16DeferReplacementEPNS1_4NodeES4_
	jmp	.L1331
	.p2align 4,,10
	.p2align 3
.L1361:
	testb	%cl, %cl
	jne	.L1362
	movl	-152(%rbp), %ecx
	movzbl	-153(%rbp), %esi
	leaq	-96(%rbp), %r14
	leaq	-128(%rbp), %rdi
	movq	%r14, %rdx
	movq	$0, -96(%rbp)
	movl	$-1, -88(%rbp)
	call	_ZN2v88internal8compiler12_GLOBAL__N_130CheckedUseInfoAsWord32FromHintENS1_19NumberOperationHintERKNS1_14FeedbackSourceENS1_13IdentifyZerosE
	movzbl	-153(%rbp), %esi
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	leaq	-144(%rbp), %rdx
	movq	$0, -144(%rbp)
	movl	$-1, -136(%rbp)
	call	_ZN2v88internal8compiler12_GLOBAL__N_130CheckedUseInfoAsWord32FromHintENS1_19NumberOperationHintERKNS1_14FeedbackSourceENS1_13IdentifyZerosE
	movl	$2, %esi
	movl	%ebx, %edi
	call	_ZN2v88internal8compiler10Truncation11LessGeneralENS2_14TruncationKindES3_@PLT
	testb	%al, %al
	jne	.L1466
	movl	$7175, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector13BothInputsAreEPNS1_4NodeENS1_4TypeE
	testb	%al, %al
	je	.L1366
	pushq	-72(%rbp)
	movl	$1031, %ecx
	pushq	-80(%rbp)
	pushq	-88(%rbp)
	pushq	-96(%rbp)
	pushq	-104(%rbp)
	pushq	-112(%rbp)
	pushq	-120(%rbp)
	pushq	-128(%rbp)
	jmp	.L1453
	.p2align 4,,10
	.p2align 3
.L1372:
	movq	32(%r15), %rax
	addq	$24, %rax
	jmp	.L1373
	.p2align 4,,10
	.p2align 3
.L1463:
	movb	$4, -96(%rbp)
	movl	$1031, %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movb	$2, -92(%rbp)
	movl	$4, %edx
	movl	$0, -88(%rbp)
	movb	$0, -84(%rbp)
	movq	$0, -80(%rbp)
	movl	$-1, -72(%rbp)
	pushq	-72(%rbp)
	pushq	-80(%rbp)
	pushq	-88(%rbp)
	pushq	-96(%rbp)
	pushq	-72(%rbp)
	pushq	-80(%rbp)
	pushq	-88(%rbp)
	pushq	-96(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE
	addq	$64, %rsp
	cmpl	$2, 120(%r12)
	jne	.L1331
	movq	(%r15), %rax
	movq	128(%r12), %rdi
	movzwl	16(%rax), %esi
	call	_ZN2v88internal8compiler21RepresentationChanger25Uint32OverflowOperatorForENS1_8IrOpcode5ValueE@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	jmp	.L1331
	.p2align 4,,10
	.p2align 3
.L1388:
	leaq	-128(%rbp), %rdi
	movl	$1099, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1391
	jmp	.L1384
	.p2align 4,,10
	.p2align 3
.L1466:
	pushq	-72(%rbp)
	movl	$4294967295, %ecx
	pushq	-80(%rbp)
	pushq	-88(%rbp)
	pushq	-96(%rbp)
	pushq	-104(%rbp)
	pushq	-112(%rbp)
	pushq	-120(%rbp)
	pushq	-128(%rbp)
	jmp	.L1456
	.p2align 4,,10
	.p2align 3
.L1366:
	pushq	-72(%rbp)
	pushq	-80(%rbp)
	pushq	-88(%rbp)
	pushq	-96(%rbp)
	pushq	-104(%rbp)
	pushq	-112(%rbp)
	pushq	-120(%rbp)
	pushq	-128(%rbp)
	jmp	.L1451
.L1460:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23056:
	.size	_ZN2v88internal8compiler22RepresentationSelector29VisitSpeculativeNumberModulusEPNS1_4NodeENS1_10TruncationEPNS1_18SimplifiedLoweringE, .-_ZN2v88internal8compiler22RepresentationSelector29VisitSpeculativeNumberModulusEPNS1_4NodeENS1_10TruncationEPNS1_18SimplifiedLoweringE
	.section	.text._ZN2v88internal8compiler22RepresentationSelector22ProcessRemainingInputsEPNS1_4NodeEi,"axG",@progbits,_ZN2v88internal8compiler22RepresentationSelector22ProcessRemainingInputsEPNS1_4NodeEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler22RepresentationSelector22ProcessRemainingInputsEPNS1_4NodeEi
	.type	_ZN2v88internal8compiler22RepresentationSelector22ProcessRemainingInputsEPNS1_4NodeEi, @function
_ZN2v88internal8compiler22RepresentationSelector22ProcessRemainingInputsEPNS1_4NodeEi:
.LFB23015:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$2232, %rsp
	movl	%edx, -2252(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties19PastFrameStateIndexEPNS1_4NodeE@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	cmpl	%eax, %r15d
	movq	%rdx, %xmm1
	cmovge	%r15d, %eax
	movslq	%eax, %r14
	leaq	0(,%r14,8), %rax
	movq	%r14, %r13
	movq	%rax, -2232(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm5
	punpcklqdq	%xmm5, %xmm1
	movaps	%xmm1, -2272(%rbp)
	jmp	.L1503
	.p2align 4,,10
	.p2align 3
.L1587:
	movq	-2232(%rbp), %rax
	addq	%rdx, %rax
.L1470:
	movq	(%rax), %rax
	movl	120(%r12), %r10d
	movq	%rax, -2224(%rbp)
	testl	%r10d, %r10d
	jne	.L1484
	movl	20(%rax), %esi
	andl	$16777215, %esi
	movl	%esi, %eax
	leaq	(%rax,%rax,4), %rdx
	movq	32(%r12), %rax
	leaq	(%rax,%rdx,8), %r14
	cmpb	$0, (%r14)
	je	.L1585
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	jne	.L1586
.L1581:
	movl	8(%r14), %r8d
.L1486:
	movzbl	4(%r14), %r15d
	movl	%r8d, %edi
	xorl	%esi, %esi
	movl	%r8d, -2248(%rbp)
	call	_ZN2v88internal8compiler10Truncation23GeneralizeIdentifyZerosENS1_13IdentifyZerosES3_@PLT
	xorl	%esi, %esi
	movl	%r15d, %edi
	movl	%eax, -2240(%rbp)
	call	_ZN2v88internal8compiler10Truncation10GeneralizeENS2_14TruncationKindES3_@PLT
	movl	-2240(%rbp), %edx
	movl	-2248(%rbp), %r8d
	movb	%al, 4(%r14)
	movl	%edx, 8(%r14)
	cmpl	%r8d, %edx
	jne	.L1545
	cmpb	%al, %r15b
	je	.L1484
.L1545:
	cmpb	$3, (%r14)
	je	.L1493
	movq	216(%r12), %rax
	movq	200(%r12), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L1494
	movq	-2224(%rbp), %rax
	movq	%rax, (%rdx)
	addq	$8, 200(%r12)
.L1495:
	movb	$3, (%r14)
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	leaq	.LC26(%rip), %rdi
	jne	.L1582
	.p2align 4,,10
	.p2align 3
.L1484:
	addq	$8, -2232(%rbp)
	addl	$1, %r13d
.L1503:
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties15PastEffectIndexEPNS1_4NodeE@PLT
	cmpl	%r13d, %eax
	jle	.L1468
	movzbl	23(%rbx), %eax
	leaq	32(%rbx), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1587
	movq	32(%rbx), %rax
	movq	-2232(%rbp), %rcx
	leaq	16(%rax,%rcx), %rax
	jmp	.L1470
	.p2align 4,,10
	.p2align 3
.L1586:
	xorl	%eax, %eax
	leaq	.LC25(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movzbl	4(%r14), %eax
	movl	8(%r14), %r8d
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	movb	%al, -2200(%rbp)
	movl	%r8d, -2196(%rbp)
	je	.L1486
	leaq	-1728(%rbp), %rax
	leaq	-1808(%rbp), %r15
	movq	%rax, %rdi
	movq	%rax, -2240(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	xorl	%r8d, %r8d
	pxor	%xmm0, %xmm0
	movq	%r15, %rdi
	movq	stdout(%rip), %rdx
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movq	$0, -1512(%rbp)
	movq	%rax, -1728(%rbp)
	movw	%r8w, -1504(%rbp)
	movups	%xmm0, -1496(%rbp)
	movups	%xmm0, -1480(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	leaq	-2200(%rbp), %rdi
	movq	%rax, -1808(%rbp)
	addq	$40, %rax
	movq	%rax, -1728(%rbp)
	call	_ZNK2v88internal8compiler10Truncation11descriptionEv@PLT
	testq	%rax, %rax
	je	.L1588
	movq	%rax, %rdi
	movq	%rax, -2248(%rbp)
	call	strlen@PLT
	movq	-2248(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L1488:
	movq	-1808(%rbp), %rax
	movq	-24(%rax), %rax
	movq	-1568(%rbp,%rax), %rdi
	testq	%rdi, %rdi
	je	.L1489
	cmpb	$0, 56(%rdi)
	je	.L1490
	movsbl	67(%rdi), %esi
.L1491:
	movq	%r15, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movdqa	-2272(%rbp), %xmm3
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	-1744(%rbp), %rdi
	movq	%rax, -1728(%rbp)
	movaps	%xmm3, -1808(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	-2240(%rbp), %rdi
	movq	%rax, -1808(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -1728(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L1581
	.p2align 4,,10
	.p2align 3
.L1493:
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	je	.L1484
	leaq	.LC27(%rip), %rdi
.L1582:
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movzbl	4(%r14), %esi
	movl	8(%r14), %edx
	movzbl	_ZN2v88internal25FLAG_trace_representationE(%rip), %eax
	movb	%sil, -2192(%rbp)
	movl	%edx, -2188(%rbp)
	testb	%al, %al
	je	.L1484
	leaq	-1376(%rbp), %r14
	leaq	-1456(%rbp), %r15
	movq	%r14, %rdi
	call	_ZNSt8ios_baseC2Ev@PLT
	xorl	%edi, %edi
	movq	stdout(%rip), %rdx
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movw	%di, -1152(%rbp)
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -1376(%rbp)
	movq	$0, -1160(%rbp)
	movups	%xmm0, -1144(%rbp)
	movups	%xmm0, -1128(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	leaq	-2192(%rbp), %rdi
	movq	%rax, -1456(%rbp)
	addq	$40, %rax
	movq	%rax, -1376(%rbp)
	call	_ZNK2v88internal8compiler10Truncation11descriptionEv@PLT
	testq	%rax, %rax
	je	.L1589
	movq	%rax, %rdi
	movq	%rax, -2240(%rbp)
	call	strlen@PLT
	movq	-2240(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L1500:
	movq	-1456(%rbp), %rax
	movq	-24(%rax), %rax
	movq	-1216(%rbp,%rax), %rdi
	testq	%rdi, %rdi
	je	.L1489
	cmpb	$0, 56(%rdi)
	je	.L1501
	movsbl	67(%rdi), %esi
.L1502:
	movq	%r15, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movdqa	-2272(%rbp), %xmm7
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	-1392(%rbp), %rdi
	movq	%rax, -1376(%rbp)
	movaps	%xmm7, -1456(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -1456(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -1376(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L1484
	.p2align 4,,10
	.p2align 3
.L1585:
	movb	$3, (%r14)
	movq	72(%r12), %rsi
	cmpq	80(%r12), %rsi
	je	.L1473
	movq	-2224(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 72(%r12)
.L1474:
	movq	216(%r12), %rax
	movq	200(%r12), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L1475
	movq	-2224(%rbp), %rax
	movq	%rax, (%rdx)
	addq	$8, 200(%r12)
.L1476:
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	jne	.L1590
.L1477:
	movzbl	4(%r14), %r8d
	movl	8(%r14), %edi
	xorl	%esi, %esi
	movb	%r8b, -2240(%rbp)
	call	_ZN2v88internal8compiler10Truncation23GeneralizeIdentifyZerosENS1_13IdentifyZerosES3_@PLT
	movzbl	-2240(%rbp), %r8d
	xorl	%esi, %esi
	movl	%eax, %r15d
	movl	%r8d, %edi
	call	_ZN2v88internal8compiler10Truncation10GeneralizeENS2_14TruncationKindES3_@PLT
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	movl	%r15d, 8(%r14)
	movb	%al, 4(%r14)
	movb	%al, -2208(%rbp)
	movl	%r15d, -2204(%rbp)
	je	.L1484
	leaq	-2080(%rbp), %r14
	leaq	-2160(%rbp), %r15
	movq	%r14, %rdi
	call	_ZNSt8ios_baseC2Ev@PLT
	xorl	%r9d, %r9d
	pxor	%xmm0, %xmm0
	movq	%r15, %rdi
	movq	stdout(%rip), %rdx
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movq	$0, -1864(%rbp)
	movq	%rax, -2080(%rbp)
	movw	%r9w, -1856(%rbp)
	movups	%xmm0, -1848(%rbp)
	movups	%xmm0, -1832(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	leaq	-2208(%rbp), %rdi
	movq	%rax, -2160(%rbp)
	addq	$40, %rax
	movq	%rax, -2080(%rbp)
	call	_ZNK2v88internal8compiler10Truncation11descriptionEv@PLT
	testq	%rax, %rax
	je	.L1591
	movq	%rax, %rdi
	movq	%rax, -2240(%rbp)
	call	strlen@PLT
	movq	-2240(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L1480:
	movq	-2160(%rbp), %rax
	movq	-24(%rax), %rax
	movq	-1920(%rbp,%rax), %rdi
	testq	%rdi, %rdi
	je	.L1489
	cmpb	$0, 56(%rdi)
	je	.L1482
	movsbl	67(%rdi), %esi
.L1483:
	movq	%r15, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movdqa	-2272(%rbp), %xmm6
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	-2096(%rbp), %rdi
	movq	%rax, -2080(%rbp)
	movaps	%xmm6, -2160(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -2160(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -2080(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L1484
	.p2align 4,,10
	.p2align 3
.L1468:
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties15PastEffectIndexEPNS1_4NodeE@PLT
	movl	-2252(%rbp), %ecx
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm2
	cmpl	%eax, %ecx
	movl	%ecx, %r13d
	cmovl	%eax, %r13d
	movslq	%r13d, %r14
	leaq	0(,%r14,8), %rax
	movq	%rax, -2232(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm6
	punpcklqdq	%xmm6, %xmm2
	movaps	%xmm2, -2272(%rbp)
	jmp	.L1537
	.p2align 4,,10
	.p2align 3
.L1594:
	movq	-2232(%rbp), %rax
	addq	%rdx, %rax
.L1506:
	movq	(%rax), %rax
	movl	120(%r12), %esi
	movq	%rax, -2216(%rbp)
	testl	%esi, %esi
	jne	.L1519
	movl	20(%rax), %esi
	andl	$16777215, %esi
	movl	%esi, %eax
	leaq	(%rax,%rax,4), %rdx
	movq	32(%r12), %rax
	leaq	(%rax,%rdx,8), %r14
	cmpb	$0, (%r14)
	je	.L1592
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	jne	.L1593
.L1583:
	movl	8(%r14), %r8d
.L1521:
	movzbl	4(%r14), %r15d
	movl	%r8d, %edi
	xorl	%esi, %esi
	movl	%r8d, -2248(%rbp)
	call	_ZN2v88internal8compiler10Truncation23GeneralizeIdentifyZerosENS1_13IdentifyZerosES3_@PLT
	xorl	%esi, %esi
	movl	%r15d, %edi
	movl	%eax, -2240(%rbp)
	call	_ZN2v88internal8compiler10Truncation10GeneralizeENS2_14TruncationKindES3_@PLT
	movl	-2240(%rbp), %edx
	movl	-2248(%rbp), %r8d
	movb	%al, 4(%r14)
	movl	%edx, 8(%r14)
	cmpl	%edx, %r8d
	jne	.L1546
	cmpb	%al, %r15b
	je	.L1519
.L1546:
	cmpb	$3, (%r14)
	je	.L1527
	movq	216(%r12), %rax
	movq	200(%r12), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L1528
	movq	-2216(%rbp), %rax
	movq	%rax, (%rdx)
	addq	$8, 200(%r12)
.L1529:
	movb	$3, (%r14)
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	leaq	.LC26(%rip), %rdi
	jne	.L1584
	.p2align 4,,10
	.p2align 3
.L1519:
	addq	$8, -2232(%rbp)
	addl	$1, %r13d
.L1537:
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties16PastControlIndexEPNS1_4NodeE@PLT
	cmpl	%r13d, %eax
	jle	.L1467
	movzbl	23(%rbx), %eax
	leaq	32(%rbx), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1594
	movq	32(%rbx), %rax
	movq	-2232(%rbp), %rcx
	leaq	16(%rax,%rcx), %rax
	jmp	.L1506
	.p2align 4,,10
	.p2align 3
.L1593:
	xorl	%eax, %eax
	leaq	.LC25(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movzbl	4(%r14), %eax
	movl	8(%r14), %r8d
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	movb	%al, -2176(%rbp)
	movl	%r8d, -2172(%rbp)
	je	.L1521
	leaq	-672(%rbp), %rax
	leaq	-752(%rbp), %r15
	movq	%rax, %rdi
	movq	%rax, -2240(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	xorl	%edx, %edx
	pxor	%xmm0, %xmm0
	movq	%r15, %rdi
	movw	%dx, -448(%rbp)
	movq	stdout(%rip), %rdx
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movq	%rax, -672(%rbp)
	movq	$0, -456(%rbp)
	movups	%xmm0, -440(%rbp)
	movups	%xmm0, -424(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	leaq	-2176(%rbp), %rdi
	movq	%rax, -752(%rbp)
	addq	$40, %rax
	movq	%rax, -672(%rbp)
	call	_ZNK2v88internal8compiler10Truncation11descriptionEv@PLT
	testq	%rax, %rax
	je	.L1595
	movq	%rax, %rdi
	movq	%rax, -2248(%rbp)
	call	strlen@PLT
	movq	-2248(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L1523:
	movq	-752(%rbp), %rax
	movq	-24(%rax), %rax
	movq	-512(%rbp,%rax), %rdi
	testq	%rdi, %rdi
	je	.L1489
	cmpb	$0, 56(%rdi)
	je	.L1524
	movsbl	67(%rdi), %esi
.L1525:
	movq	%r15, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movdqa	-2272(%rbp), %xmm4
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	-688(%rbp), %rdi
	movq	%rax, -672(%rbp)
	movaps	%xmm4, -752(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	-2240(%rbp), %rdi
	movq	%rax, -752(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -672(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L1583
	.p2align 4,,10
	.p2align 3
.L1527:
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	je	.L1519
	leaq	.LC27(%rip), %rdi
.L1584:
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movzbl	4(%r14), %esi
	movl	8(%r14), %edx
	movzbl	_ZN2v88internal25FLAG_trace_representationE(%rip), %eax
	movb	%sil, -2168(%rbp)
	movl	%edx, -2164(%rbp)
	testb	%al, %al
	je	.L1519
	leaq	-320(%rbp), %r14
	leaq	-400(%rbp), %r15
	movq	%r14, %rdi
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%r15, %rdi
	movq	stdout(%rip), %rdx
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movw	%ax, -96(%rbp)
	movq	$0, -104(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	leaq	-2168(%rbp), %rdi
	movq	%rax, -400(%rbp)
	addq	$40, %rax
	movq	%rax, -320(%rbp)
	call	_ZNK2v88internal8compiler10Truncation11descriptionEv@PLT
	testq	%rax, %rax
	je	.L1596
	movq	%rax, %rdi
	movq	%rax, -2240(%rbp)
	call	strlen@PLT
	movq	-2240(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L1534:
	movq	-400(%rbp), %rax
	movq	-24(%rax), %rax
	movq	-160(%rbp,%rax), %rdi
	testq	%rdi, %rdi
	je	.L1489
	cmpb	$0, 56(%rdi)
	je	.L1535
	movsbl	67(%rdi), %esi
.L1536:
	movq	%r15, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movdqa	-2272(%rbp), %xmm7
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	-336(%rbp), %rdi
	movq	%rax, -320(%rbp)
	movaps	%xmm7, -400(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -400(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L1519
	.p2align 4,,10
	.p2align 3
.L1592:
	movb	$3, (%r14)
	movq	72(%r12), %rsi
	cmpq	80(%r12), %rsi
	je	.L1509
	movq	-2216(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 72(%r12)
.L1510:
	movq	216(%r12), %rax
	movq	200(%r12), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L1511
	movq	-2216(%rbp), %rax
	movq	%rax, (%rdx)
	addq	$8, 200(%r12)
.L1512:
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	jne	.L1597
.L1513:
	movzbl	4(%r14), %r8d
	movl	8(%r14), %edi
	xorl	%esi, %esi
	movb	%r8b, -2240(%rbp)
	call	_ZN2v88internal8compiler10Truncation23GeneralizeIdentifyZerosENS1_13IdentifyZerosES3_@PLT
	movzbl	-2240(%rbp), %r8d
	xorl	%esi, %esi
	movl	%eax, %r15d
	movl	%r8d, %edi
	call	_ZN2v88internal8compiler10Truncation10GeneralizeENS2_14TruncationKindES3_@PLT
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	movl	%r15d, 8(%r14)
	movb	%al, 4(%r14)
	movb	%al, -2184(%rbp)
	movl	%r15d, -2180(%rbp)
	je	.L1519
	leaq	-1024(%rbp), %r14
	leaq	-1104(%rbp), %r15
	movq	%r14, %rdi
	call	_ZNSt8ios_baseC2Ev@PLT
	xorl	%ecx, %ecx
	pxor	%xmm0, %xmm0
	movq	%r15, %rdi
	movq	stdout(%rip), %rdx
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movq	$0, -808(%rbp)
	movq	%rax, -1024(%rbp)
	movw	%cx, -800(%rbp)
	movups	%xmm0, -792(%rbp)
	movups	%xmm0, -776(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	leaq	-2184(%rbp), %rdi
	movq	%rax, -1104(%rbp)
	addq	$40, %rax
	movq	%rax, -1024(%rbp)
	call	_ZNK2v88internal8compiler10Truncation11descriptionEv@PLT
	testq	%rax, %rax
	je	.L1598
	movq	%rax, %rdi
	movq	%rax, -2240(%rbp)
	call	strlen@PLT
	movq	-2240(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L1516:
	movq	-1104(%rbp), %rax
	movq	-24(%rax), %rax
	movq	-864(%rbp,%rax), %rdi
	testq	%rdi, %rdi
	je	.L1489
	cmpb	$0, 56(%rdi)
	je	.L1517
	movsbl	67(%rdi), %esi
.L1518:
	movq	%r15, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movdqa	-2272(%rbp), %xmm5
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	-1040(%rbp), %rdi
	movq	%rax, -1024(%rbp)
	movaps	%xmm5, -1104(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -1104(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -1024(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L1519
	.p2align 4,,10
	.p2align 3
.L1467:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1599
	addq	$2232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1490:
	.cfi_restore_state
	movq	%rdi, -2248(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-2248(%rbp), %rdi
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1491
	call	*%rax
	movsbl	%al, %esi
	jmp	.L1491
	.p2align 4,,10
	.p2align 3
.L1524:
	movq	%rdi, -2248(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-2248(%rbp), %rdi
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1525
	call	*%rax
	movsbl	%al, %esi
	jmp	.L1525
	.p2align 4,,10
	.p2align 3
.L1589:
	movq	-1456(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r15, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L1500
	.p2align 4,,10
	.p2align 3
.L1596:
	movq	-400(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r15, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L1534
	.p2align 4,,10
	.p2align 3
.L1588:
	movq	-1808(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r15, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L1488
	.p2align 4,,10
	.p2align 3
.L1595:
	movq	-752(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r15, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L1523
	.p2align 4,,10
	.p2align 3
.L1597:
	movq	-2216(%rbp), %rax
	leaq	.LC24(%rip), %rdi
	movl	20(%rax), %esi
	xorl	%eax, %eax
	andl	$16777215, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1513
	.p2align 4,,10
	.p2align 3
.L1590:
	movq	-2224(%rbp), %rax
	leaq	.LC24(%rip), %rdi
	movl	20(%rax), %esi
	xorl	%eax, %eax
	andl	$16777215, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1477
	.p2align 4,,10
	.p2align 3
.L1501:
	movq	%rdi, -2240(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-2240(%rbp), %rdi
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1502
	call	*%rax
	movsbl	%al, %esi
	jmp	.L1502
	.p2align 4,,10
	.p2align 3
.L1535:
	movq	%rdi, -2240(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-2240(%rbp), %rdi
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1536
	call	*%rax
	movsbl	%al, %esi
	jmp	.L1536
	.p2align 4,,10
	.p2align 3
.L1482:
	movq	%rdi, -2240(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-2240(%rbp), %rdi
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1483
	call	*%rax
	movsbl	%al, %esi
	jmp	.L1483
	.p2align 4,,10
	.p2align 3
.L1517:
	movq	%rdi, -2240(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-2240(%rbp), %rdi
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1518
	call	*%rax
	movsbl	%al, %esi
	jmp	.L1518
	.p2align 4,,10
	.p2align 3
.L1475:
	leaq	-2224(%rbp), %rsi
	leaq	136(%r12), %rdi
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	jmp	.L1476
	.p2align 4,,10
	.p2align 3
.L1511:
	leaq	-2216(%rbp), %rsi
	leaq	136(%r12), %rdi
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	jmp	.L1512
	.p2align 4,,10
	.p2align 3
.L1509:
	leaq	-2216(%rbp), %rdx
	leaq	56(%r12), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1510
	.p2align 4,,10
	.p2align 3
.L1473:
	leaq	-2224(%rbp), %rdx
	leaq	56(%r12), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1474
	.p2align 4,,10
	.p2align 3
.L1494:
	leaq	-2224(%rbp), %rsi
	leaq	136(%r12), %rdi
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	jmp	.L1495
	.p2align 4,,10
	.p2align 3
.L1528:
	leaq	-2216(%rbp), %rsi
	leaq	136(%r12), %rdi
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	jmp	.L1529
	.p2align 4,,10
	.p2align 3
.L1591:
	movq	-2160(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r15, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L1480
	.p2align 4,,10
	.p2align 3
.L1598:
	movq	-1104(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r15, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L1516
.L1489:
	call	_ZSt16__throw_bad_castv@PLT
.L1599:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23015:
	.size	_ZN2v88internal8compiler22RepresentationSelector22ProcessRemainingInputsEPNS1_4NodeEi, .-_ZN2v88internal8compiler22RepresentationSelector22ProcessRemainingInputsEPNS1_4NodeEi
	.section	.text._ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE,"axG",@progbits,_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	.type	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE, @function
_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE:
.LFB23023:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	16(%rbp), %r8
	movq	24(%rbp), %r9
	movq	32(%rbp), %rsi
	movq	40(%rbp), %rdi
	movl	120(%rbx), %eax
	movq	%r8, -112(%rbp)
	movq	%r9, -104(%rbp)
	movq	%rsi, -96(%rbp)
	movq	%rdi, -88(%rbp)
	testl	%eax, %eax
	je	.L1601
	cmpl	$2, %eax
	jne	.L1603
	movq	%rsi, -64(%rbp)
	movl	20(%r12), %esi
	leaq	32(%r12), %rax
	movq	%rax, -120(%rbp)
	movzbl	-112(%rbp), %r11d
	movl	%esi, %eax
	movq	%r8, -80(%rbp)
	movq	32(%r12), %r15
	xorl	$251658240, %eax
	movq	%r9, -72(%rbp)
	movq	%rdi, -56(%rbp)
	testl	$251658240, %eax
	je	.L1632
.L1604:
	testb	%r11b, %r11b
	je	.L1603
	movl	20(%r15), %r8d
	movzbl	-100(%rbp), %eax
	andl	$16777215, %r8d
	movb	%al, -128(%rbp)
	movl	%r8d, %eax
	leaq	(%rax,%rax,4), %rdx
	movq	32(%rbx), %rax
	leaq	(%rax,%rdx,8), %rax
	movq	%rax, -136(%rbp)
	cmpb	%r11b, 1(%rax)
	je	.L1633
.L1607:
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	jne	.L1634
.L1620:
	movzbl	-128(%rbp), %eax
	pushq	-56(%rbp)
	movq	%rbx, %rdi
	pushq	-64(%rbp)
	movb	%r11b, -80(%rbp)
	movb	%al, -68(%rbp)
	pushq	-72(%rbp)
	pushq	-80(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12PrintUseInfoENS1_7UseInfoE
	addq	$32, %rsp
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	jne	.L1635
.L1612:
	movl	20(%r15), %eax
	movq	32(%rbx), %rdx
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rax
	leaq	(%rdx,%rax,8), %rax
	movq	24(%rax), %rcx
	testq	%rcx, %rcx
	jne	.L1613
	movq	8(%r15), %rcx
.L1613:
	movq	-136(%rbp), %rax
	movq	128(%rbx), %rdi
	movq	%r15, %rsi
	movq	%r12, %r8
	movzbl	1(%rax), %edx
	pushq	-56(%rbp)
	pushq	-64(%rbp)
	pushq	-72(%rbp)
	pushq	-80(%rbp)
	call	_ZN2v88internal8compiler21RepresentationChanger20GetRepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE@PLT
	addq	$32, %rsp
	movq	%rax, %r15
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1614
	movq	32(%r12), %rdi
	movq	%r12, %rsi
	cmpq	%rdi, %r15
	je	.L1603
.L1615:
	subq	$24, %rsi
	testq	%rdi, %rdi
	je	.L1616
	movq	%rsi, -128(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-128(%rbp), %rsi
.L1616:
	movq	-120(%rbp), %rax
	movq	%r15, (%rax)
	testq	%r15, %r15
	je	.L1603
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	.p2align 4,,10
	.p2align 3
.L1603:
	movl	$1, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector22ProcessRemainingInputsEPNS1_4NodeEi
	movl	20(%r12), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rdx
	movq	32(%rbx), %rax
	leaq	(%rax,%rdx,8), %rdx
	movl	120(%rbx), %eax
	testl	%eax, %eax
	je	.L1617
	cmpl	$1, %eax
	je	.L1618
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1618:
	.cfi_restore_state
	movb	%r13b, 1(%rdx)
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1601:
	.cfi_restore_state
	pushq	%rdi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	pushq	%rsi
	movq	%r12, %rsi
	pushq	%r9
	pushq	%r8
	call	_ZN2v88internal8compiler22RepresentationSelector12EnqueueInputEPNS1_4NodeEiNS1_7UseInfoE
	addq	$32, %rsp
	jmp	.L1603
	.p2align 4,,10
	.p2align 3
.L1617:
	movq	%r14, 16(%rdx)
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1632:
	.cfi_restore_state
	movq	16(%r15), %r15
	jmp	.L1604
	.p2align 4,,10
	.p2align 3
.L1633:
	cmpb	$0, -128(%rbp)
	je	.L1603
	jmp	.L1607
	.p2align 4,,10
	.p2align 3
.L1614:
	movq	32(%r12), %rsi
	movq	16(%rsi), %rdi
	leaq	16(%rsi), %rax
	movq	%rax, -120(%rbp)
	cmpq	%rdi, %r15
	jne	.L1615
	jmp	.L1603
	.p2align 4,,10
	.p2align 3
.L1635:
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1612
	.p2align 4,,10
	.p2align 3
.L1634:
	movq	(%r15), %rax
	movq	(%r12), %rdx
	andl	$16777215, %esi
	xorl	%ecx, %ecx
	leaq	.LC13(%rip), %rdi
	movb	%r11b, -137(%rbp)
	movq	8(%rax), %r9
	movq	8(%rdx), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	movzbl	-137(%rbp), %r11d
	je	.L1620
	xorl	%eax, %eax
	leaq	.LC14(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	movzbl	-137(%rbp), %r11d
	je	.L1620
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector15PrintOutputInfoEPNS2_8NodeInfoE.isra.0.part.0
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	movzbl	-137(%rbp), %r11d
	je	.L1620
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movzbl	-137(%rbp), %r11d
	jmp	.L1620
	.cfi_endproc
.LFE23023:
	.size	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE, .-_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	.section	.text._ZN2v88internal8compiler22RepresentationSelector10VisitCheckEPNS1_4NodeENS1_4TypeEPNS1_18SimplifiedLoweringE,"axG",@progbits,_ZN2v88internal8compiler22RepresentationSelector10VisitCheckEPNS1_4NodeENS1_4TypeEPNS1_18SimplifiedLoweringE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler22RepresentationSelector10VisitCheckEPNS1_4NodeENS1_4TypeEPNS1_18SimplifiedLoweringE
	.type	_ZN2v88internal8compiler22RepresentationSelector10VisitCheckEPNS1_4NodeENS1_4TypeEPNS1_18SimplifiedLoweringE, @function
_ZN2v88internal8compiler22RepresentationSelector10VisitCheckEPNS1_4NodeENS1_4TypeEPNS1_18SimplifiedLoweringE:
.LFB23032:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	movq	%rdx, %rsi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$48, %rsp
	movq	32(%r13), %rdx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movzbl	23(%r13), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1637
	movq	16(%rdx), %rdx
.L1637:
	movq	8(%rdx), %rax
	movq	%rax, -64(%rbp)
	cmpq	%rax, %rsi
	jne	.L1638
.L1642:
	movl	120(%r12), %eax
	movb	$8, -64(%rbp)
	movb	$5, -60(%rbp)
	movl	$1, -56(%rbp)
	movb	$0, -52(%rbp)
	movq	$0, -48(%rbp)
	movl	$-1, -40(%rbp)
	testl	%eax, %eax
	je	.L1639
	cmpl	$2, %eax
	je	.L1640
.L1641:
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector22ProcessRemainingInputsEPNS1_4NodeEi
	movl	20(%r13), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rdx
	movq	32(%r12), %rax
	leaq	(%rax,%rdx,8), %rdx
	movl	120(%r12), %eax
	testl	%eax, %eax
	je	.L1646
	cmpl	$1, %eax
	jne	.L1648
	movb	$7, 1(%rdx)
	movl	120(%r12), %eax
.L1648:
	cmpl	$2, %eax
	je	.L1656
.L1636:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1657
	leaq	-16(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1638:
	.cfi_restore_state
	leaq	-64(%rbp), %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1642
	movl	120(%r12), %eax
	movb	$7, -64(%rbp)
	movb	$5, -60(%rbp)
	movl	$1, -56(%rbp)
	movb	$6, -52(%rbp)
	movq	$0, -48(%rbp)
	movl	$-1, -40(%rbp)
	testl	%eax, %eax
	je	.L1643
	cmpl	$2, %eax
	jne	.L1645
	pushq	-40(%rbp)
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	pushq	-48(%rbp)
	movq	%r12, %rdi
	pushq	-56(%rbp)
	pushq	-64(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12ConvertInputEPNS1_4NodeEiNS1_7UseInfoENS1_4TypeE
	addq	$32, %rsp
.L1645:
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector22ProcessRemainingInputsEPNS1_4NodeEi
	movl	20(%r13), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rdx
	movq	32(%r12), %rax
	leaq	(%rax,%rdx,8), %rdx
	movl	120(%r12), %eax
	testl	%eax, %eax
	je	.L1651
	cmpl	$1, %eax
	jne	.L1636
	movb	$7, 1(%rdx)
	jmp	.L1636
	.p2align 4,,10
	.p2align 3
.L1640:
	pushq	-40(%rbp)
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	pushq	-48(%rbp)
	movq	%r12, %rdi
	pushq	-56(%rbp)
	pushq	-64(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12ConvertInputEPNS1_4NodeEiNS1_7UseInfoENS1_4TypeE
	addq	$32, %rsp
	jmp	.L1641
	.p2align 4,,10
	.p2align 3
.L1656:
	movzbl	23(%r13), %eax
	movq	32(%r13), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1650
	movq	16(%rdx), %rdx
.L1650:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector16DeferReplacementEPNS1_4NodeES4_
	jmp	.L1636
	.p2align 4,,10
	.p2align 3
.L1646:
	movl	$4294967295, %eax
	movq	%rax, 16(%rdx)
	movl	120(%r12), %eax
	jmp	.L1648
	.p2align 4,,10
	.p2align 3
.L1639:
	pushq	-40(%rbp)
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	pushq	-48(%rbp)
	pushq	-56(%rbp)
	pushq	-64(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12EnqueueInputEPNS1_4NodeEiNS1_7UseInfoE
	addq	$32, %rsp
	jmp	.L1641
	.p2align 4,,10
	.p2align 3
.L1651:
	movl	$4294967295, %eax
	movq	%rax, 16(%rdx)
	jmp	.L1636
	.p2align 4,,10
	.p2align 3
.L1643:
	pushq	-40(%rbp)
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	pushq	-48(%rbp)
	pushq	-56(%rbp)
	pushq	-64(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12EnqueueInputEPNS1_4NodeEiNS1_7UseInfoE
	addq	$32, %rsp
	jmp	.L1645
.L1657:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23032:
	.size	_ZN2v88internal8compiler22RepresentationSelector10VisitCheckEPNS1_4NodeENS1_4TypeEPNS1_18SimplifiedLoweringE, .-_ZN2v88internal8compiler22RepresentationSelector10VisitCheckEPNS1_4NodeENS1_4TypeEPNS1_18SimplifiedLoweringE
	.section	.text._ZN2v88internal8compiler22RepresentationSelector13VisitObjectIsEPNS1_4NodeENS1_4TypeEPNS1_18SimplifiedLoweringE,"axG",@progbits,_ZN2v88internal8compiler22RepresentationSelector13VisitObjectIsEPNS1_4NodeENS1_4TypeEPNS1_18SimplifiedLoweringE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler22RepresentationSelector13VisitObjectIsEPNS1_4NodeENS1_4TypeEPNS1_18SimplifiedLoweringE
	.type	_ZN2v88internal8compiler22RepresentationSelector13VisitObjectIsEPNS1_4NodeENS1_4TypeEPNS1_18SimplifiedLoweringE, @function
_ZN2v88internal8compiler22RepresentationSelector13VisitObjectIsEPNS1_4NodeENS1_4TypeEPNS1_18SimplifiedLoweringE:
.LFB23031:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$72, %rsp
	movq	32(%rsi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1659
	movq	16(%rdx), %rdx
.L1659:
	movl	20(%rdx), %eax
	movq	32(%r12), %rcx
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rax
	leaq	(%rcx,%rax,8), %rax
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L1660
	movq	8(%rdx), %rax
.L1660:
	movq	%rax, -112(%rbp)
	cmpq	%rax, %r14
	jne	.L1683
.L1661:
	movl	120(%r12), %edx
	movb	$0, -96(%rbp)
	movb	$0, -92(%rbp)
	movl	$0, -88(%rbp)
	movb	$0, -84(%rbp)
	movq	$0, -80(%rbp)
	movl	$-1, -72(%rbp)
	testl	%edx, %edx
	je	.L1684
.L1670:
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector22ProcessRemainingInputsEPNS1_4NodeEi
	movl	20(%r13), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rdx
	movq	32(%r12), %rax
	leaq	(%rax,%rdx,8), %rdx
	movl	120(%r12), %eax
	testl	%eax, %eax
	je	.L1671
	cmpl	$1, %eax
	je	.L1672
.L1673:
	cmpl	$2, %eax
	je	.L1685
.L1658:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1686
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1672:
	.cfi_restore_state
	movb	$1, 1(%rdx)
	movl	120(%r12), %eax
	cmpl	$2, %eax
	jne	.L1658
.L1685:
	movq	(%rbx), %rdi
	movl	$1, %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler22RepresentationSelector16DeferReplacementEPNS1_4NodeES4_
	jmp	.L1658
	.p2align 4,,10
	.p2align 3
.L1683:
	leaq	-112(%rbp), %r15
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1661
	movl	120(%r12), %eax
	movb	$8, -96(%rbp)
	movb	$5, -92(%rbp)
	movl	$1, -88(%rbp)
	movb	$0, -84(%rbp)
	movq	$0, -80(%rbp)
	movl	$-1, -72(%rbp)
	testl	%eax, %eax
	je	.L1662
	cmpl	$2, %eax
	je	.L1663
.L1664:
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector22ProcessRemainingInputsEPNS1_4NodeEi
	movl	20(%r13), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rdx
	movq	32(%r12), %rax
	leaq	(%rax,%rdx,8), %rdx
	movl	120(%r12), %eax
	testl	%eax, %eax
	je	.L1665
	cmpl	$1, %eax
	je	.L1666
.L1667:
	cmpl	$2, %eax
	jne	.L1658
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L1658
	movq	(%rbx), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	movq	%r13, -104(%rbp)
	movq	%rax, -96(%rbp)
	jne	.L1687
.L1675:
	movq	0(%r13), %rax
	movl	24(%rax), %eax
	testl	%eax, %eax
	jle	.L1676
	movq	%r13, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	-104(%rbp), %rdi
	xorl	%esi, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	%r13, %rdx
	movq	%rax, %rsi
	movq	-104(%rbp), %rax
	movq	24(%rax), %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_124ReplaceEffectControlUsesEPNS1_4NodeES4_S4_.isra.0
.L1676:
	movq	104(%r12), %rsi
	leaq	88(%r12), %r13
	cmpq	112(%r12), %rsi
	je	.L1677
	movq	-104(%rbp), %rax
	movq	%rax, (%rsi)
	movq	104(%r12), %rax
	leaq	8(%rax), %rsi
	movq	%rsi, 104(%r12)
.L1678:
	cmpq	112(%r12), %rsi
	je	.L1679
	movq	-96(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 104(%r12)
.L1680:
	movq	-104(%rbp), %rdi
	call	_ZN2v88internal8compiler4Node13NullAllInputsEv@PLT
	jmp	.L1658
	.p2align 4,,10
	.p2align 3
.L1671:
	movl	$4294967295, %eax
	movq	%rax, 16(%rdx)
	movl	120(%r12), %eax
	jmp	.L1673
	.p2align 4,,10
	.p2align 3
.L1684:
	pushq	-72(%rbp)
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	pushq	-80(%rbp)
	pushq	-88(%rbp)
	pushq	-96(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12EnqueueInputEPNS1_4NodeEiNS1_7UseInfoE
	addq	$32, %rsp
	jmp	.L1670
	.p2align 4,,10
	.p2align 3
.L1666:
	movb	$1, 1(%rdx)
	movl	120(%r12), %eax
	jmp	.L1667
	.p2align 4,,10
	.p2align 3
.L1663:
	pushq	-72(%rbp)
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	pushq	-80(%rbp)
	movq	%r12, %rdi
	pushq	-88(%rbp)
	pushq	-96(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12ConvertInputEPNS1_4NodeEiNS1_7UseInfoENS1_4TypeE
	addq	$32, %rsp
	jmp	.L1664
	.p2align 4,,10
	.p2align 3
.L1665:
	movl	$4294967295, %eax
	movq	%rax, 16(%rdx)
	movl	120(%r12), %eax
	jmp	.L1667
	.p2align 4,,10
	.p2align 3
.L1662:
	pushq	-72(%rbp)
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	pushq	-80(%rbp)
	pushq	-88(%rbp)
	pushq	-96(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12EnqueueInputEPNS1_4NodeEiNS1_7UseInfoE
	addq	$32, %rsp
	jmp	.L1664
.L1687:
	movq	(%rax), %rdi
	movl	20(%rax), %ecx
	movq	0(%r13), %rax
	movl	20(%r13), %esi
	movq	8(%rdi), %r8
	andl	$16777215, %ecx
	leaq	.LC22(%rip), %rdi
	movq	8(%rax), %rdx
	andl	$16777215, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-104(%rbp), %r13
	jmp	.L1675
.L1679:
	leaq	-96(%rbp), %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1680
.L1677:
	leaq	-104(%rbp), %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	movq	104(%r12), %rsi
	jmp	.L1678
.L1686:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23031:
	.size	_ZN2v88internal8compiler22RepresentationSelector13VisitObjectIsEPNS1_4NodeENS1_4TypeEPNS1_18SimplifiedLoweringE, .-_ZN2v88internal8compiler22RepresentationSelector13VisitObjectIsEPNS1_4NodeENS1_4TypeEPNS1_18SimplifiedLoweringE
	.section	.rodata._ZN2v88internal8compiler22RepresentationSelector11VisitUnusedEPNS1_4NodeE.str1.1,"aMS",@progbits,1
.LC28:
	.string	"killing #%d:%s\n"
	.section	.text._ZN2v88internal8compiler22RepresentationSelector11VisitUnusedEPNS1_4NodeE,"axG",@progbits,_ZN2v88internal8compiler22RepresentationSelector11VisitUnusedEPNS1_4NodeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler22RepresentationSelector11VisitUnusedEPNS1_4NodeE
	.type	_ZN2v88internal8compiler22RepresentationSelector11VisitUnusedEPNS1_4NodeE, @function
_ZN2v88internal8compiler22RepresentationSelector11VisitUnusedEPNS1_4NodeE:
.LFB23018:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	20(%rdi), %r15d
	call	_ZN2v88internal8compiler18OperatorProperties15HasContextInputEPKNS1_8OperatorE@PLT
	movq	(%r14), %rdi
	movzbl	%al, %eax
	addl	%eax, %r15d
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	movzbl	%al, %eax
	addl	%r15d, %eax
	movl	%eax, -88(%rbp)
	testl	%eax, %eax
	jle	.L1709
	subl	$1, %eax
	movq	%r14, -80(%rbp)
	movq	%r14, %r15
	leaq	8(%r14,%rax,8), %rax
	movq	%rbx, %r14
	movq	%rax, -72(%rbp)
	leaq	-64(%rbp), %rax
	movq	%rax, -112(%rbp)
	jmp	.L1710
	.p2align 4,,10
	.p2align 3
.L1692:
	addq	$8, %r15
	cmpq	%r15, -72(%rbp)
	je	.L1724
.L1710:
	movl	120(%r14), %eax
	testl	%eax, %eax
	jne	.L1692
	movq	-80(%rbp), %rax
	movq	%r15, %rdx
	subq	%rax, %rdx
	movzbl	23(%rax), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1693
	leaq	32(%r15), %rax
.L1694:
	movq	(%rax), %rax
	movl	20(%rax), %esi
	movq	%rax, -64(%rbp)
	andl	$16777215, %esi
	movl	%esi, %eax
	leaq	(%rax,%rax,4), %rdx
	movq	32(%r14), %rax
	leaq	(%rax,%rdx,8), %rbx
	cmpb	$0, (%rbx)
	je	.L1725
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	jne	.L1726
.L1701:
	movb	4(%rbx), %r12b
	movl	8(%rbx), %eax
	movl	%r12d, %r10d
	salq	$32, %rax
	orq	%rax, %r10
	movq	%r10, %rdi
	movq	%r10, %r12
	call	_ZN2v88internal8compiler22RepresentationSelector15PrintTruncationENS1_10TruncationE.isra.0
	movl	8(%rbx), %r11d
	movzbl	4(%rbx), %eax
	xorl	%esi, %esi
	movl	%r11d, %edi
	movl	%r11d, -96(%rbp)
	movb	%al, -84(%rbp)
	call	_ZN2v88internal8compiler10Truncation23GeneralizeIdentifyZerosENS1_13IdentifyZerosES3_@PLT
	movzbl	-84(%rbp), %edi
	xorl	%esi, %esi
	movl	%eax, -92(%rbp)
	call	_ZN2v88internal8compiler10Truncation10GeneralizeENS2_14TruncationKindES3_@PLT
	movl	-92(%rbp), %edx
	movl	-96(%rbp), %r11d
	movb	%al, 4(%rbx)
	movl	%edx, 8(%rbx)
	cmpl	%edx, %r11d
	jne	.L1717
	cmpb	%al, -84(%rbp)
	je	.L1692
.L1717:
	cmpb	$3, (%rbx)
	jne	.L1727
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	jne	.L1728
.L1708:
	movb	%al, -104(%rbp)
	movl	-104(%rbp), %eax
	salq	$32, %rdx
	addq	$8, %r15
	orq	%rdx, %rax
	movq	%rax, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector15PrintTruncationENS1_10TruncationE.isra.0
	cmpq	%r15, -72(%rbp)
	jne	.L1710
	.p2align 4,,10
	.p2align 3
.L1724:
	movq	%r14, %rbx
	movq	-80(%rbp), %r14
.L1709:
	movl	-88(%rbp), %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector22ProcessRemainingInputsEPNS1_4NodeEi
	cmpl	$2, 120(%rbx)
	je	.L1729
.L1688:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1730
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1693:
	.cfi_restore_state
	movq	-80(%rbp), %rax
	movq	32(%rax), %rax
	leaq	16(%rax,%rdx), %rax
	jmp	.L1694
	.p2align 4,,10
	.p2align 3
.L1727:
	movq	216(%r14), %rax
	movq	200(%r14), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L1705
	movq	-64(%rbp), %rax
	movq	%rax, (%rdx)
	addq	$8, 200(%r14)
.L1706:
	movb	$3, (%rbx)
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	jne	.L1707
.L1723:
	movzbl	4(%rbx), %eax
	movl	8(%rbx), %edx
	jmp	.L1708
	.p2align 4,,10
	.p2align 3
.L1726:
	leaq	.LC25(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1701
	.p2align 4,,10
	.p2align 3
.L1725:
	movb	$3, (%rbx)
	movq	72(%r14), %rsi
	cmpq	80(%r14), %rsi
	je	.L1695
	movq	-64(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 72(%r14)
.L1696:
	movq	216(%r14), %rax
	movq	200(%r14), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L1697
	movq	-64(%rbp), %rax
	movq	%rax, (%rdx)
	addq	$8, 200(%r14)
.L1698:
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	jne	.L1731
.L1699:
	movzbl	4(%rbx), %r11d
	movl	8(%rbx), %edi
	xorl	%esi, %esi
	movb	%r11b, -92(%rbp)
	call	_ZN2v88internal8compiler10Truncation23GeneralizeIdentifyZerosENS1_13IdentifyZerosES3_@PLT
	movzbl	-92(%rbp), %r11d
	xorl	%esi, %esi
	movl	%eax, -84(%rbp)
	movl	%r11d, %edi
	call	_ZN2v88internal8compiler10Truncation10GeneralizeENS2_14TruncationKindES3_@PLT
	movl	-84(%rbp), %edx
	movb	%al, 4(%rbx)
	movb	%al, %r13b
	movl	%edx, 8(%rbx)
	movq	%rdx, %rbx
	movl	%r13d, %r13d
	salq	$32, %rbx
	orq	%rbx, %r13
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector15PrintTruncationENS1_10TruncationE.isra.0
	jmp	.L1692
	.p2align 4,,10
	.p2align 3
.L1729:
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	movq	(%r14), %rax
	jne	.L1732
.L1711:
	cmpl	$1, 24(%rax)
	je	.L1733
.L1712:
	movq	(%rbx), %rbx
	movq	352(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L1734
.L1713:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node11ReplaceUsesEPS2_@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node13NullAllInputsEv@PLT
	jmp	.L1688
.L1732:
	movl	20(%r14), %esi
	movq	8(%rax), %rdx
	leaq	.LC28(%rip), %rdi
	xorl	%eax, %eax
	andl	$16777215, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	(%r14), %rax
	jmp	.L1711
	.p2align 4,,10
	.p2align 3
.L1728:
	leaq	.LC27(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1723
	.p2align 4,,10
	.p2align 3
.L1731:
	movq	-64(%rbp), %rax
	leaq	.LC24(%rip), %rdi
	movl	20(%rax), %esi
	xorl	%eax, %eax
	andl	$16777215, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1699
	.p2align 4,,10
	.p2align 3
.L1707:
	xorl	%eax, %eax
	leaq	.LC26(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movzbl	4(%rbx), %eax
	movl	8(%rbx), %edx
	jmp	.L1708
.L1697:
	movq	-112(%rbp), %rsi
	leaq	136(%r14), %rdi
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	jmp	.L1698
.L1695:
	movq	-112(%rbp), %rdx
	leaq	56(%r14), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1696
.L1705:
	movq	-112(%rbp), %rsi
	leaq	136(%r14), %rdi
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	jmp	.L1706
.L1733:
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	24(%r14), %rdi
	movq	%r12, %rdx
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_124ReplaceEffectControlUsesEPNS1_4NodeES4_S4_.isra.0
	jmp	.L1712
.L1734:
	movq	8(%rbx), %rdi
	movq	(%rbx), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4DeadEv@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 352(%rbx)
	movq	%rax, %rsi
	jmp	.L1713
.L1730:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23018:
	.size	_ZN2v88internal8compiler22RepresentationSelector11VisitUnusedEPNS1_4NodeE, .-_ZN2v88internal8compiler22RepresentationSelector11VisitUnusedEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler22RepresentationSelector33VisitSpeculativeIntegerAdditiveOpEPNS1_4NodeENS1_10TruncationEPNS1_18SimplifiedLoweringE,"axG",@progbits,_ZN2v88internal8compiler22RepresentationSelector33VisitSpeculativeIntegerAdditiveOpEPNS1_4NodeENS1_10TruncationEPNS1_18SimplifiedLoweringE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler22RepresentationSelector33VisitSpeculativeIntegerAdditiveOpEPNS1_4NodeENS1_10TruncationEPNS1_18SimplifiedLoweringE
	.type	_ZN2v88internal8compiler22RepresentationSelector33VisitSpeculativeIntegerAdditiveOpEPNS1_4NodeENS1_10TruncationEPNS1_18SimplifiedLoweringE, @function
_ZN2v88internal8compiler22RepresentationSelector33VisitSpeculativeIntegerAdditiveOpEPNS1_4NodeENS1_10TruncationEPNS1_18SimplifiedLoweringE:
.LFB23054:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	32(%rsi), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$152, %rsp
	movzbl	23(%rsi), %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rsi), %rax
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L1736
	movq	8(%rax), %rsi
	leaq	40(%r14), %rax
.L1737:
	movq	%rsi, -168(%rbp)
	movq	344(%r12), %rdx
	movq	(%rax), %rax
	movq	392(%rdx), %r9
	movq	8(%rax), %rax
	movq	%rax, -160(%rbp)
	cmpq	%rsi, %r9
	jne	.L1791
.L1738:
	cmpq	%rsi, %rax
	jne	.L1740
.L1743:
	testb	%bl, %bl
	je	.L1792
	movq	8(%r14), %rax
	movq	%rax, -128(%rbp)
	cmpq	$1099, %rax
	je	.L1745
	leaq	-128(%rbp), %rdi
	movl	$1099, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L1793
.L1745:
	movb	$4, -96(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$2, -92(%rbp)
	movl	$4, %edx
	movl	$0, -88(%rbp)
	movb	$0, -84(%rbp)
	movq	$0, -80(%rbp)
	movl	$-1, -72(%rbp)
	pushq	-72(%rbp)
	pushq	-80(%rbp)
	pushq	-88(%rbp)
	pushq	-96(%rbp)
	pushq	-72(%rbp)
	pushq	-80(%rbp)
	pushq	-88(%rbp)
	pushq	-96(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE
	addq	$64, %rsp
	cmpl	$2, 120(%r12)
	je	.L1766
.L1735:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1794
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1740:
	.cfi_restore_state
	leaq	-160(%rbp), %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1743
.L1746:
	movq	(%r14), %rdi
	call	_ZN2v88internal8compiler21NumberOperationHintOfEPKNS1_8OperatorE@PLT
	movb	%al, -184(%rbp)
	movzbl	23(%r14), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1748
	movq	32(%r14), %rcx
.L1790:
	movl	20(%rcx), %eax
	movq	32(%r12), %rdx
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rax
	movq	24(%rdx,%rax,8), %r15
	testq	%r15, %r15
	jne	.L1752
	movq	8(%rcx), %r15
.L1752:
	movq	8(%r13), %rcx
	addq	$8, %r13
	movl	20(%rcx), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rax
	movq	24(%rdx,%rax,8), %rax
	testq	%rax, %rax
	jne	.L1754
	movq	8(%rcx), %rax
.L1754:
	movq	%rax, -152(%rbp)
	movq	(%r14), %rax
	movl	$3147, %esi
	cmpw	$160, 16(%rax)
	movl	$1099, %eax
	cmovne	%rax, %rsi
	cmpq	-168(%rbp), %rsi
	je	.L1759
	leaq	-168(%rbp), %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L1760
.L1759:
	cmpq	$3147, -160(%rbp)
	jne	.L1795
.L1757:
	cmpq	$1099, -168(%rbp)
	jne	.L1796
.L1761:
	movb	$4, -96(%rbp)
	movb	$2, -92(%rbp)
	movl	$0, -88(%rbp)
	movb	$0, -84(%rbp)
	movq	$0, -80(%rbp)
	movl	$-1, -72(%rbp)
	pushq	-72(%rbp)
	pushq	-80(%rbp)
	pushq	-88(%rbp)
	pushq	-96(%rbp)
	pushq	-72(%rbp)
	pushq	-80(%rbp)
	pushq	-88(%rbp)
	pushq	-96(%rbp)
.L1789:
	movl	$1099, %ecx
	movl	$4, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE
	addq	$64, %rsp
	cmpl	$2, 120(%r12)
	jne	.L1735
	movl	$2, %esi
	movl	%ebx, %edi
	call	_ZN2v88internal8compiler10Truncation11LessGeneralENS2_14TruncationKindES3_@PLT
	testb	%al, %al
	jne	.L1766
	movq	(%r12), %rax
	movl	$1099, %esi
	movq	%r15, %rdi
	movq	(%r14), %rbx
	movq	(%rax), %rax
	movq	(%rax), %r13
	movq	-152(%rbp), %rax
	movq	%r15, -128(%rbp)
	movq	%r13, %rdx
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	-96(%rbp), %rdi
	movq	%r13, %rdx
	movl	$1099, %esi
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	cmpq	$1, -128(%rbp)
	movq	%rax, -96(%rbp)
	je	.L1766
	cmpq	$1, %rax
	je	.L1766
	movzwl	16(%rbx), %eax
	cmpw	$160, %ax
	jne	.L1797
	leaq	-128(%rbp), %r15
	leaq	-96(%rbp), %r13
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type3MaxEv@PLT
	movq	%r13, %rdi
	movsd	%xmm0, -184(%rbp)
	call	_ZNK2v88internal8compiler4Type3MaxEv@PLT
	addsd	-184(%rbp), %xmm0
	comisd	.LC12(%rip), %xmm0
	jbe	.L1798
.L1771:
	movq	(%r14), %rax
	movq	128(%r12), %rdi
	movzwl	16(%rax), %esi
	call	_ZN2v88internal8compiler21RepresentationChanger24Int32OverflowOperatorForENS1_8IrOpcode5ValueE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	jmp	.L1735
	.p2align 4,,10
	.p2align 3
.L1796:
	leaq	-168(%rbp), %rdi
	movl	$1099, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1761
	cmpq	$1099, -160(%rbp)
	je	.L1761
	leaq	-160(%rbp), %rdi
	movl	$1099, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1761
	.p2align 4,,10
	.p2align 3
.L1760:
	movq	%rbx, %rax
	shrq	$32, %rax
	movq	%rax, %rcx
	movq	(%r14), %rax
	cmpw	$160, 16(%rax)
	je	.L1799
.L1764:
	movzbl	-184(%rbp), %esi
	leaq	-96(%rbp), %r13
	leaq	-128(%rbp), %rdi
	movq	$0, -96(%rbp)
	movq	%r13, %rdx
	movl	$-1, -88(%rbp)
	call	_ZN2v88internal8compiler12_GLOBAL__N_130CheckedUseInfoAsWord32FromHintENS1_19NumberOperationHintERKNS1_14FeedbackSourceENS1_13IdentifyZerosE
	movzbl	-184(%rbp), %esi
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	leaq	-144(%rbp), %rdx
	movq	$0, -144(%rbp)
	movl	$-1, -136(%rbp)
	call	_ZN2v88internal8compiler12_GLOBAL__N_130CheckedUseInfoAsWord32FromHintENS1_19NumberOperationHintERKNS1_14FeedbackSourceENS1_13IdentifyZerosE
	pushq	-72(%rbp)
	pushq	-80(%rbp)
	pushq	-88(%rbp)
	pushq	-96(%rbp)
	pushq	-104(%rbp)
	pushq	-112(%rbp)
	pushq	-120(%rbp)
	pushq	-128(%rbp)
	jmp	.L1789
	.p2align 4,,10
	.p2align 3
.L1791:
	leaq	-168(%rbp), %rdi
	movq	%r9, %rsi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L1746
	movq	344(%r12), %rdx
	movq	-160(%rbp), %rax
	movq	392(%rdx), %rsi
	jmp	.L1738
	.p2align 4,,10
	.p2align 3
.L1736:
	movq	16(%rax), %rdx
	addq	$24, %rax
	movq	8(%rdx), %rsi
	jmp	.L1737
	.p2align 4,,10
	.p2align 3
.L1795:
	leaq	-160(%rbp), %rdi
	movl	$3147, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1757
	jmp	.L1760
	.p2align 4,,10
	.p2align 3
.L1792:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector11VisitUnusedEPNS1_4NodeE
	jmp	.L1735
	.p2align 4,,10
	.p2align 3
.L1793:
	movq	8(%r14), %rax
	movq	%rax, -96(%rbp)
	cmpq	$1031, %rax
	je	.L1745
	leaq	-96(%rbp), %rdi
	movl	$1031, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1745
	movl	$2, %esi
	movl	%ebx, %edi
	call	_ZN2v88internal8compiler10Truncation11LessGeneralENS2_14TruncationKindES3_@PLT
	testb	%al, %al
	je	.L1746
	jmp	.L1745
	.p2align 4,,10
	.p2align 3
.L1797:
	cmpw	$161, %ax
	jne	.L1800
	leaq	-128(%rbp), %r15
	leaq	-96(%rbp), %r13
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type3MaxEv@PLT
	movq	%r13, %rdi
	movsd	%xmm0, -184(%rbp)
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	movsd	-184(%rbp), %xmm1
	subsd	%xmm0, %xmm1
	comisd	.LC12(%rip), %xmm1
	ja	.L1771
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	movq	%r13, %rdi
	movsd	%xmm0, -184(%rbp)
	call	_ZNK2v88internal8compiler4Type3MaxEv@PLT
	movsd	-184(%rbp), %xmm1
	subsd	%xmm0, %xmm1
	movsd	.LC11(%rip), %xmm0
	comisd	%xmm1, %xmm0
	ja	.L1771
	.p2align 4,,10
	.p2align 3
.L1766:
	movq	(%r14), %rax
	movq	128(%r12), %rdi
	movzwl	16(%rax), %esi
	call	_ZN2v88internal8compiler21RepresentationChanger16Int32OperatorForENS1_8IrOpcode5ValueE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler22RepresentationSelector14ChangeToPureOpEPNS1_4NodeEPKNS1_8OperatorE
	jmp	.L1735
	.p2align 4,,10
	.p2align 3
.L1748:
	movq	32(%r14), %rax
	movq	16(%rax), %rcx
	leaq	16(%rax), %r13
	jmp	.L1790
	.p2align 4,,10
	.p2align 3
.L1799:
	leaq	-152(%rbp), %rdi
	movl	$2049, %esi
	movl	%ecx, -188(%rbp)
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	movl	-188(%rbp), %ecx
	testb	%al, %al
	movl	$0, %eax
	cmove	%eax, %ecx
	jmp	.L1764
.L1798:
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	movq	%r13, %rdi
	movsd	%xmm0, -184(%rbp)
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	addsd	-184(%rbp), %xmm0
	movsd	.LC11(%rip), %xmm1
	comisd	%xmm0, %xmm1
	ja	.L1771
	jmp	.L1766
.L1794:
	call	__stack_chk_fail@PLT
.L1800:
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE23054:
	.size	_ZN2v88internal8compiler22RepresentationSelector33VisitSpeculativeIntegerAdditiveOpEPNS1_4NodeENS1_10TruncationEPNS1_18SimplifiedLoweringE, .-_ZN2v88internal8compiler22RepresentationSelector33VisitSpeculativeIntegerAdditiveOpEPNS1_4NodeENS1_10TruncationEPNS1_18SimplifiedLoweringE
	.section	.text._ZN2v88internal8compiler22RepresentationSelector9VisitNoopEPNS1_4NodeENS1_10TruncationE,"axG",@progbits,_ZN2v88internal8compiler22RepresentationSelector9VisitNoopEPNS1_4NodeENS1_10TruncationE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler22RepresentationSelector9VisitNoopEPNS1_4NodeENS1_10TruncationE
	.type	_ZN2v88internal8compiler22RepresentationSelector9VisitNoopEPNS1_4NodeENS1_10TruncationE, @function
_ZN2v88internal8compiler22RepresentationSelector9VisitNoopEPNS1_4NodeENS1_10TruncationE:
.LFB23019:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testb	%dl, %dl
	je	.L1843
	movl	20(%rsi), %eax
	movq	%rdx, %rbx
	movq	32(%rdi), %rdx
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rax
	leaq	(%rdx,%rax,8), %rax
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L1804
	movq	8(%rsi), %rax
.L1804:
	movq	%rax, -96(%rbp)
	cmpq	$1, %rax
	jne	.L1805
.L1807:
	xorl	%r14d, %r14d
.L1806:
	movq	%rbx, %rax
	movb	%r14b, -96(%rbp)
	shrq	$32, %rax
	movb	%bl, -92(%rbp)
	movl	%eax, -88(%rbp)
	movl	120(%r12), %eax
	movb	$0, -84(%rbp)
	movq	$0, -80(%rbp)
	movl	$-1, -72(%rbp)
	testl	%eax, %eax
	je	.L1826
	cmpl	$2, %eax
	je	.L1827
.L1828:
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector22ProcessRemainingInputsEPNS1_4NodeEi
	movl	20(%r13), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rdx
	movq	32(%r12), %rax
	leaq	(%rax,%rdx,8), %rdx
	movl	120(%r12), %eax
	testl	%eax, %eax
	je	.L1829
	cmpl	$1, %eax
	je	.L1830
.L1831:
	cmpl	$2, %eax
	je	.L1844
.L1801:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1845
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1830:
	.cfi_restore_state
	movb	%r14b, 1(%rdx)
	movl	120(%r12), %eax
	cmpl	$2, %eax
	jne	.L1801
.L1844:
	movl	20(%r13), %esi
	movq	32(%r13), %rax
	movl	%esi, %edx
	xorl	$251658240, %edx
	andl	$251658240, %edx
	je	.L1846
.L1832:
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	movq	0(%r13), %rdx
	movq	%r13, -104(%rbp)
	movq	%rax, -96(%rbp)
	jne	.L1847
.L1833:
	movl	24(%rdx), %eax
	testl	%eax, %eax
	jg	.L1848
.L1834:
	movq	104(%r12), %rsi
	leaq	88(%r12), %r13
	cmpq	112(%r12), %rsi
	je	.L1835
	movq	-104(%rbp), %rax
	movq	%rax, (%rsi)
	movq	104(%r12), %rax
	leaq	8(%rax), %rsi
	movq	%rsi, 104(%r12)
.L1836:
	cmpq	%rsi, 112(%r12)
	je	.L1837
	movq	-96(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 104(%r12)
.L1838:
	movq	-104(%rbp), %rdi
	call	_ZN2v88internal8compiler4Node13NullAllInputsEv@PLT
	jmp	.L1801
	.p2align 4,,10
	.p2align 3
.L1827:
	pushq	-72(%rbp)
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	pushq	-80(%rbp)
	movq	%r12, %rdi
	pushq	-88(%rbp)
	pushq	-96(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12ConvertInputEPNS1_4NodeEiNS1_7UseInfoENS1_4TypeE
	addq	$32, %rsp
	jmp	.L1828
	.p2align 4,,10
	.p2align 3
.L1805:
	leaq	-96(%rbp), %r14
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1807
	cmpq	$1099, -96(%rbp)
	je	.L1809
	movl	$1099, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1809
	cmpq	$1031, -96(%rbp)
	je	.L1809
	movl	$1031, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1809
	cmpq	$8396767, -96(%rbp)
	je	.L1812
	movl	$8396767, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L1811
.L1812:
	movl	$2, %esi
	movl	%ebx, %edi
	call	_ZN2v88internal8compiler10Truncation11LessGeneralENS2_14TruncationKindES3_@PLT
	testb	%al, %al
	jne	.L1809
.L1811:
	cmpq	$513, -96(%rbp)
	je	.L1814
	movl	$513, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1814
	cmpq	$8396767, -96(%rbp)
	je	.L1818
	movl	$8396767, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L1817
.L1818:
	movl	$4, %esi
	movl	%ebx, %edi
	call	_ZN2v88internal8compiler10Truncation11LessGeneralENS2_14TruncationKindES3_@PLT
	testb	%al, %al
	jne	.L1816
.L1817:
	movq	8(%r12), %r15
	call	_ZN2v88internal8compiler10BitsetType11SignedSmallEv@PLT
	movl	$4097, %esi
	movl	%eax, %edi
	movq	%r15, %rdx
	orl	$1, %edi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, %rsi
	cmpq	-96(%rbp), %rax
	je	.L1842
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1842
	cmpq	$7263, -96(%rbp)
	je	.L1816
	movl	$7263, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1816
	cmpq	$134217729, -96(%rbp)
	je	.L1824
	movl	$134217729, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L1823
.L1824:
	movl	$3, %esi
	movl	%ebx, %edi
	call	_ZN2v88internal8compiler10Truncation11LessGeneralENS2_14TruncationKindES3_@PLT
	testb	%al, %al
	jne	.L1822
.L1823:
	cmpq	$33554433, -96(%rbp)
	je	.L1822
	movl	$33554433, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L1842
.L1822:
	movl	$5, %r14d
	jmp	.L1806
	.p2align 4,,10
	.p2align 3
.L1826:
	pushq	-72(%rbp)
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	pushq	-80(%rbp)
	pushq	-88(%rbp)
	pushq	-96(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12EnqueueInputEPNS1_4NodeEiNS1_7UseInfoE
	addq	$32, %rsp
	jmp	.L1828
	.p2align 4,,10
	.p2align 3
.L1829:
	movl	$4294967295, %eax
	movq	%rax, 16(%rdx)
	movl	120(%r12), %eax
	jmp	.L1831
	.p2align 4,,10
	.p2align 3
.L1843:
	call	_ZN2v88internal8compiler22RepresentationSelector11VisitUnusedEPNS1_4NodeE
	jmp	.L1801
	.p2align 4,,10
	.p2align 3
.L1848:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	-104(%rbp), %rdi
	xorl	%esi, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	%r13, %rdx
	movq	%rax, %rsi
	movq	-104(%rbp), %rax
	movq	24(%rax), %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_124ReplaceEffectControlUsesEPNS1_4NodeES4_S4_.isra.0
	jmp	.L1834
	.p2align 4,,10
	.p2align 3
.L1846:
	movq	16(%rax), %rax
	jmp	.L1832
	.p2align 4,,10
	.p2align 3
.L1847:
	movq	(%rax), %rdi
	movl	20(%rax), %ecx
	xorl	%eax, %eax
	andl	$16777215, %esi
	movq	8(%rdx), %rdx
	movq	8(%rdi), %r8
	andl	$16777215, %ecx
	leaq	.LC22(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-104(%rbp), %r13
	movq	0(%r13), %rdx
	jmp	.L1833
	.p2align 4,,10
	.p2align 3
.L1809:
	movl	$4, %r14d
	jmp	.L1806
	.p2align 4,,10
	.p2align 3
.L1837:
	leaq	-96(%rbp), %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1838
	.p2align 4,,10
	.p2align 3
.L1835:
	leaq	-104(%rbp), %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	movq	104(%r12), %rsi
	jmp	.L1836
.L1814:
	movl	$1, %r14d
	jmp	.L1806
.L1842:
	movl	$8, %r14d
	jmp	.L1806
.L1816:
	movl	$13, %r14d
	jmp	.L1806
.L1845:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23019:
	.size	_ZN2v88internal8compiler22RepresentationSelector9VisitNoopEPNS1_4NodeENS1_10TruncationE, .-_ZN2v88internal8compiler22RepresentationSelector9VisitNoopEPNS1_4NodeENS1_10TruncationE
	.section	.rodata._ZN2v88internal8compiler22RepresentationSelector16VisitObjectStateEPNS1_4NodeE.str1.8,"aMS",@progbits,1
	.align 8
.LC29:
	.string	"cannot create std::vector larger than max_size()"
	.section	.text._ZN2v88internal8compiler22RepresentationSelector16VisitObjectStateEPNS1_4NodeE,"axG",@progbits,_ZN2v88internal8compiler22RepresentationSelector16VisitObjectStateEPNS1_4NodeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler22RepresentationSelector16VisitObjectStateEPNS1_4NodeE
	.type	_ZN2v88internal8compiler22RepresentationSelector16VisitObjectStateEPNS1_4NodeE, @function
_ZN2v88internal8compiler22RepresentationSelector16VisitObjectStateEPNS1_4NodeE:
.LFB23039:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$1128, %rsp
	.cfi_offset 3, -56
	movl	120(%rdi), %esi
	movl	20(%r12), %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%esi, %esi
	jne	.L1850
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	32(%rdi), %rcx
	xorl	%ebx, %ebx
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdi
	movq	%rax, %xmm3
	movl	%edx, %eax
	movq	%rdi, %xmm1
	shrl	$24, %eax
	punpcklqdq	%xmm3, %xmm1
	andl	$15, %eax
	movaps	%xmm1, -1168(%rbp)
	cmpl	$15, %eax
	je	.L1851
	.p2align 4,,10
	.p2align 3
.L1972:
	cmpl	%ebx, %eax
	jle	.L1853
	leaq	32(%r12,%rbx,8), %rax
.L1922:
	movq	(%rax), %rax
	movq	%rax, -1136(%rbp)
	testl	%esi, %esi
	jne	.L1887
	movl	20(%rax), %esi
	andl	$16777215, %esi
	movl	%esi, %eax
	leaq	(%rax,%rax,4), %rax
	leaq	(%rcx,%rax,8), %r15
	cmpb	$0, (%r15)
	je	.L1970
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	jne	.L1971
.L1968:
	movl	8(%r15), %edx
.L1869:
	movzbl	4(%r15), %r14d
	movl	%edx, %edi
	movl	$1, %esi
	movl	%edx, -1152(%rbp)
	call	_ZN2v88internal8compiler10Truncation23GeneralizeIdentifyZerosENS1_13IdentifyZerosES3_@PLT
	movl	$5, %esi
	movl	%r14d, %edi
	movl	%eax, -1144(%rbp)
	call	_ZN2v88internal8compiler10Truncation10GeneralizeENS2_14TruncationKindES3_@PLT
	movl	-1144(%rbp), %ecx
	movl	-1152(%rbp), %edx
	movb	%al, 4(%r15)
	movl	%ecx, 8(%r15)
	cmpl	%ecx, %edx
	jne	.L1931
	cmpb	%al, %r14b
	je	.L1875
.L1931:
	cmpb	$3, (%r15)
	je	.L1877
	movq	216(%r13), %rax
	movq	200(%r13), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L1878
	movq	-1136(%rbp), %rax
	movq	%rax, (%rdx)
	addq	$8, 200(%r13)
.L1879:
	movb	$3, (%r15)
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	leaq	.LC26(%rip), %rdi
	jne	.L1969
.L1875:
	movl	20(%r12), %edx
	movl	120(%r13), %esi
	movq	32(%r13), %rcx
.L1887:
	movl	%edx, %eax
	addq	$1, %rbx
	shrl	$24, %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1972
.L1851:
	movq	32(%r12), %rax
	cmpl	%ebx, 8(%rax)
	jle	.L1853
	leaq	16(%rax,%rbx,8), %rax
	jmp	.L1922
	.p2align 4,,10
	.p2align 3
.L1971:
	xorl	%eax, %eax
	leaq	.LC25(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movl	8(%r15), %edx
	movzbl	4(%r15), %eax
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	movb	%al, -1120(%rbp)
	movl	%edx, -1116(%rbp)
	je	.L1869
	leaq	-672(%rbp), %rax
	leaq	-752(%rbp), %r14
	movq	%rax, %rdi
	movq	%rax, -1144(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%r14, %rdi
	movq	stdout(%rip), %rdx
	movq	%rax, -672(%rbp)
	xorl	%eax, %eax
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movw	%ax, -448(%rbp)
	movq	$0, -456(%rbp)
	movups	%xmm0, -440(%rbp)
	movups	%xmm0, -424(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	leaq	-1120(%rbp), %rdi
	movq	%rax, -752(%rbp)
	addq	$40, %rax
	movq	%rax, -672(%rbp)
	call	_ZNK2v88internal8compiler10Truncation11descriptionEv@PLT
	testq	%rax, %rax
	je	.L1973
	movq	%rax, %rdi
	movq	%rax, -1152(%rbp)
	call	strlen@PLT
	movq	-1152(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L1871:
	movq	-752(%rbp), %rax
	movq	-24(%rax), %rax
	movq	-512(%rbp,%rax), %rdi
	testq	%rdi, %rdi
	je	.L1872
	cmpb	$0, 56(%rdi)
	je	.L1873
	movsbl	67(%rdi), %esi
.L1874:
	movq	%r14, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movdqa	-1168(%rbp), %xmm2
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	-688(%rbp), %rdi
	movq	%rax, -672(%rbp)
	movaps	%xmm2, -752(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	-1144(%rbp), %rdi
	movq	%rax, -752(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -672(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L1968
	.p2align 4,,10
	.p2align 3
.L1904:
	movq	0(%r13), %rax
	movq	(%r12), %rdi
	movq	8(%rax), %r15
	call	_ZN2v88internal8compiler10ObjectIdOfEPKNS1_8OperatorE@PLT
	movq	%r14, %rdx
	movl	%eax, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder16TypedObjectStateEjPKNS0_10ZoneVectorINS0_11MachineTypeEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movl	20(%r12), %edx
	movl	120(%r13), %esi
	movq	32(%r13), %rcx
.L1853:
	andl	$16777215, %edx
	leaq	(%rdx,%rdx,4), %rax
	leaq	(%rcx,%rax,8), %rax
	testl	%esi, %esi
	je	.L1919
	cmpl	$1, %esi
	je	.L1920
.L1849:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1974
	addq	$1128, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1877:
	.cfi_restore_state
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	je	.L1875
	leaq	.LC27(%rip), %rdi
.L1969:
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movzbl	4(%r15), %esi
	movl	8(%r15), %edx
	movzbl	_ZN2v88internal25FLAG_trace_representationE(%rip), %eax
	movb	%sil, -1112(%rbp)
	movl	%edx, -1108(%rbp)
	testb	%al, %al
	je	.L1875
	leaq	-320(%rbp), %r15
	leaq	-400(%rbp), %r14
	movq	%r15, %rdi
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%r14, %rdi
	movq	stdout(%rip), %rdx
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movw	%ax, -96(%rbp)
	movq	$0, -104(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	leaq	-1112(%rbp), %rdi
	movq	%rax, -400(%rbp)
	addq	$40, %rax
	movq	%rax, -320(%rbp)
	call	_ZNK2v88internal8compiler10Truncation11descriptionEv@PLT
	testq	%rax, %rax
	je	.L1975
	movq	%rax, %rdi
	movq	%rax, -1144(%rbp)
	call	strlen@PLT
	movq	-1144(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L1884:
	movq	-400(%rbp), %rax
	movq	-24(%rax), %rax
	movq	-160(%rbp,%rax), %rdi
	testq	%rdi, %rdi
	je	.L1872
	cmpb	$0, 56(%rdi)
	je	.L1885
	movsbl	67(%rdi), %esi
.L1886:
	movq	%r14, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movdqa	-1168(%rbp), %xmm4
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	-336(%rbp), %rdi
	movq	%rax, -320(%rbp)
	movaps	%xmm4, -400(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%r15, %rdi
	movq	%rax, -400(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movl	20(%r12), %edx
	movl	120(%r13), %esi
	movq	32(%r13), %rcx
	jmp	.L1887
	.p2align 4,,10
	.p2align 3
.L1970:
	movb	$3, (%r15)
	movq	72(%r13), %rsi
	cmpq	80(%r13), %rsi
	je	.L1857
	movq	-1136(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 72(%r13)
.L1858:
	movq	216(%r13), %rax
	movq	200(%r13), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L1859
	movq	-1136(%rbp), %rax
	movq	%rax, (%rdx)
	addq	$8, 200(%r13)
.L1860:
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	jne	.L1976
.L1861:
	movzbl	4(%r15), %r8d
	movl	8(%r15), %edi
	movl	$1, %esi
	movb	%r8b, -1144(%rbp)
	call	_ZN2v88internal8compiler10Truncation23GeneralizeIdentifyZerosENS1_13IdentifyZerosES3_@PLT
	movzbl	-1144(%rbp), %r8d
	movl	$5, %esi
	movl	%eax, %r14d
	movl	%r8d, %edi
	call	_ZN2v88internal8compiler10Truncation10GeneralizeENS2_14TruncationKindES3_@PLT
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	movl	%r14d, 8(%r15)
	movb	%al, 4(%r15)
	movb	%al, -1128(%rbp)
	movl	%r14d, -1124(%rbp)
	je	.L1875
	leaq	-1024(%rbp), %r15
	leaq	-1104(%rbp), %r14
	movq	%r15, %rdi
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%r14, %rdi
	movq	stdout(%rip), %rdx
	movq	%rax, -1024(%rbp)
	xorl	%eax, %eax
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movw	%ax, -800(%rbp)
	movq	$0, -808(%rbp)
	movups	%xmm0, -792(%rbp)
	movups	%xmm0, -776(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	leaq	-1128(%rbp), %rdi
	movq	%rax, -1104(%rbp)
	addq	$40, %rax
	movq	%rax, -1024(%rbp)
	call	_ZNK2v88internal8compiler10Truncation11descriptionEv@PLT
	testq	%rax, %rax
	je	.L1977
	movq	%rax, %rdi
	movq	%rax, -1144(%rbp)
	call	strlen@PLT
	movq	-1144(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L1864:
	movq	-1104(%rbp), %rax
	movq	-24(%rax), %rax
	movq	-864(%rbp,%rax), %rdi
	testq	%rdi, %rdi
	je	.L1872
	cmpb	$0, 56(%rdi)
	je	.L1866
	movsbl	67(%rdi), %esi
.L1867:
	movq	%r14, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movdqa	-1168(%rbp), %xmm5
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	-1040(%rbp), %rdi
	movq	%rax, -1024(%rbp)
	movaps	%xmm5, -1104(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%r15, %rdi
	movq	%rax, -1104(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -1024(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movl	20(%r12), %edx
	movl	120(%r13), %esi
	movq	32(%r13), %rcx
	jmp	.L1887
	.p2align 4,,10
	.p2align 3
.L1850:
	cmpl	$2, %esi
	je	.L1889
	movq	32(%rdi), %rax
	andl	$16777215, %edx
	leaq	(%rdx,%rdx,4), %rdx
	leaq	(%rax,%rdx,8), %rax
	cmpl	$1, %esi
	jne	.L1849
.L1920:
	movb	$8, 1(%rax)
	jmp	.L1849
	.p2align 4,,10
	.p2align 3
.L1919:
	movl	$4294967295, %ebx
	movq	%rbx, 16(%rax)
	jmp	.L1849
	.p2align 4,,10
	.p2align 3
.L1973:
	movq	-752(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r14, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L1871
	.p2align 4,,10
	.p2align 3
.L1873:
	movq	%rdi, -1152(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-1152(%rbp), %rdi
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1874
	call	*%rax
	movsbl	%al, %esi
	jmp	.L1874
	.p2align 4,,10
	.p2align 3
.L1885:
	movq	%rdi, -1144(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-1144(%rbp), %rdi
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1886
	call	*%rax
	movsbl	%al, %esi
	jmp	.L1886
	.p2align 4,,10
	.p2align 3
.L1889:
	movq	(%rdi), %rax
	shrl	$24, %edx
	andl	$15, %edx
	movq	(%rax), %rax
	movq	(%rax), %rdi
	cmpl	$15, %edx
	jne	.L1892
	movq	32(%r12), %rax
	movl	8(%rax), %edx
.L1892:
	movq	16(%rdi), %r14
	movq	24(%rdi), %rax
	movslq	%edx, %rbx
	subq	%r14, %rax
	cmpq	$31, %rax
	jbe	.L1978
	leaq	32(%r14), %rax
	movq	%rax, 16(%rdi)
.L1894:
	cmpq	$1073741823, %rbx
	ja	.L1979
	movq	%rdi, (%r14)
	leaq	(%rbx,%rbx), %r15
	xorl	%esi, %esi
	movq	$0, 8(%r14)
	movq	$0, 16(%r14)
	movq	$0, 24(%r14)
	testq	%rbx, %rbx
	je	.L1923
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	leaq	7(%r15), %rsi
	andq	$-8, %rsi
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L1980
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1898:
	leaq	(%rax,%r15), %rsi
	leaq	-1(%rbx), %rdx
	movq	%rax, 8(%r14)
	movq	%rax, %rcx
	movq	%rsi, 24(%r14)
	cmpq	$6, %rdx
	jbe	.L1928
	movq	%rbx, %rdx
	pxor	%xmm0, %xmm0
	shrq	$3, %rdx
	salq	$4, %rdx
	addq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L1900:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L1900
	movq	%rbx, %rdx
	movq	%rbx, %rax
	andq	$-8, %rdx
	andl	$7, %eax
	leaq	(%rcx,%rdx,2), %rcx
	cmpq	%rdx, %rbx
	je	.L1923
.L1899:
	xorl	%r11d, %r11d
	movw	%r11w, (%rcx)
	cmpq	$1, %rax
	je	.L1923
	xorl	%r10d, %r10d
	movw	%r10w, 2(%rcx)
	cmpq	$2, %rax
	je	.L1923
	xorl	%r9d, %r9d
	movw	%r9w, 4(%rcx)
	cmpq	$3, %rax
	je	.L1923
	xorl	%r8d, %r8d
	movw	%r8w, 6(%rcx)
	cmpq	$4, %rax
	je	.L1923
	xorl	%edi, %edi
	movw	%di, 8(%rcx)
	cmpq	$5, %rax
	je	.L1923
	xorl	%edx, %edx
	movw	%dx, 10(%rcx)
	cmpq	$6, %rax
	je	.L1923
	xorl	%eax, %eax
	movw	%ax, 12(%rcx)
.L1923:
	movq	%rsi, 16(%r14)
	xorl	%ebx, %ebx
	leaq	32(%r12), %r15
	jmp	.L1918
	.p2align 4,,10
	.p2align 3
.L1982:
	cmpl	%ebx, %eax
	jle	.L1904
	leaq	(%r15,%rbx,8), %rax
.L1906:
	movq	(%rax), %rcx
	movl	20(%rcx), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rdx
	movq	32(%r13), %rax
	leaq	(%rax,%rdx,8), %rdx
	movq	24(%rdx), %rax
	testq	%rax, %rax
	jne	.L1907
	movq	8(%rcx), %rax
.L1907:
	movzbl	1(%rdx), %r8d
	movq	8(%r14), %rdx
	movq	%rax, -1120(%rbp)
	leaq	(%rdx,%rbx,2), %rdx
	cmpq	$1, %rax
	je	.L1929
	leal	-6(%r8), %ecx
	cmpb	$2, %cl
	ja	.L1981
.L1909:
	movl	$7, %eax
	movl	$8, %r8d
.L1908:
	movb	%r8b, (%rdx)
	addq	$1, %rbx
	movb	%al, 1(%rdx)
.L1918:
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1982
	movq	32(%r12), %rax
	cmpl	%ebx, 8(%rax)
	jle	.L1904
	leaq	16(%rax,%rbx,8), %rax
	jmp	.L1906
	.p2align 4,,10
	.p2align 3
.L1857:
	leaq	-1136(%rbp), %rdx
	leaq	56(%r13), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1858
	.p2align 4,,10
	.p2align 3
.L1878:
	leaq	-1136(%rbp), %rsi
	leaq	136(%r13), %rdi
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	jmp	.L1879
	.p2align 4,,10
	.p2align 3
.L1929:
	xorl	%eax, %eax
	xorl	%r8d, %r8d
	jmp	.L1908
	.p2align 4,,10
	.p2align 3
.L1981:
	leal	-9(%r8), %ecx
	cmpb	$2, %cl
	ja	.L1983
	movl	$7, %eax
	movl	$11, %r8d
	jmp	.L1908
	.p2align 4,,10
	.p2align 3
.L1976:
	movq	-1136(%rbp), %rax
	leaq	.LC24(%rip), %rdi
	movl	20(%rax), %esi
	xorl	%eax, %eax
	andl	$16777215, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1861
	.p2align 4,,10
	.p2align 3
.L1983:
	cmpb	$5, %r8b
	jne	.L1910
	movb	%r8b, -1144(%rbp)
	cmpq	$134217729, %rax
	je	.L1909
	leaq	-1120(%rbp), %rdi
	movl	$134217729, %esi
	movq	%rdx, -1152(%rbp)
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	movq	-1152(%rbp), %rdx
	testb	%al, %al
	jne	.L1909
	movzbl	-1144(%rbp), %r8d
	movl	$4, %eax
	jmp	.L1908
	.p2align 4,,10
	.p2align 3
.L1975:
	movq	-400(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r14, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L1884
	.p2align 4,,10
	.p2align 3
.L1859:
	leaq	-1136(%rbp), %rsi
	leaq	136(%r13), %rdi
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	jmp	.L1860
	.p2align 4,,10
	.p2align 3
.L1866:
	movq	%rdi, -1144(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-1144(%rbp), %rdi
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1867
	call	*%rax
	movsbl	%al, %esi
	jmp	.L1867
	.p2align 4,,10
	.p2align 3
.L1910:
	movq	%rax, -1112(%rbp)
	cmpq	$1099, %rax
	jne	.L1913
.L1915:
	movl	$2, %eax
	jmp	.L1908
	.p2align 4,,10
	.p2align 3
.L1977:
	movq	-1104(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r14, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L1864
.L1913:
	leaq	-1112(%rbp), %rdi
	movl	$1099, %esi
	movq	%rdx, -1152(%rbp)
	movb	%r8b, -1144(%rbp)
	movq	%rdi, -1168(%rbp)
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	movzbl	-1144(%rbp), %r8d
	movq	-1152(%rbp), %rdx
	testb	%al, %al
	jne	.L1915
	cmpq	$1031, -1112(%rbp)
	movq	-1168(%rbp), %rdi
	je	.L1917
	movl	$1031, %esi
	movq	%rdx, -1152(%rbp)
	movb	%r8b, -1144(%rbp)
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	movzbl	-1144(%rbp), %r8d
	movq	-1152(%rbp), %rdx
	testb	%al, %al
	jne	.L1917
	movl	$7, %eax
	jmp	.L1908
.L1978:
	movl	$32, %esi
	movq	%rdi, -1144(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-1144(%rbp), %rdi
	movq	%rax, %r14
	jmp	.L1894
.L1928:
	movq	%rbx, %rax
	jmp	.L1899
.L1917:
	movl	$3, %eax
	jmp	.L1908
.L1980:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1898
.L1872:
	call	_ZSt16__throw_bad_castv@PLT
.L1974:
	call	__stack_chk_fail@PLT
.L1979:
	leaq	.LC29(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE23039:
	.size	_ZN2v88internal8compiler22RepresentationSelector16VisitObjectStateEPNS1_4NodeE, .-_ZN2v88internal8compiler22RepresentationSelector16VisitObjectStateEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler22RepresentationSelector11VisitInputsEPNS1_4NodeE,"axG",@progbits,_ZN2v88internal8compiler22RepresentationSelector11VisitInputsEPNS1_4NodeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler22RepresentationSelector11VisitInputsEPNS1_4NodeE
	.type	_ZN2v88internal8compiler22RepresentationSelector11VisitInputsEPNS1_4NodeE, @function
_ZN2v88internal8compiler22RepresentationSelector11VisitInputsEPNS1_4NodeE:
.LFB23016:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$2312, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -2264(%rbp)
	movq	(%rsi), %rdi
	movl	20(%rdi), %r13d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18OperatorProperties15HasContextInputEPKNS1_8OperatorE@PLT
	movq	(%r15), %rdi
	movzbl	%al, %ebx
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	addl	%r13d, %ebx
	movzbl	%al, %eax
	addl	%ebx, %eax
	movl	%eax, -2284(%rbp)
	testl	%eax, %eax
	jle	.L2146
	subl	$1, %eax
	movq	-2264(%rbp), %r13
	xorl	%ebx, %ebx
	movq	%r12, %r14
	movq	%rax, -2272(%rbp)
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rcx
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm2
	movq	%rax, %xmm5
	movl	20(%r13), %edx
	punpcklqdq	%xmm5, %xmm2
	movaps	%xmm2, -2320(%rbp)
	.p2align 4,,10
	.p2align 3
.L2039:
	movl	%edx, %eax
	movl	120(%r14), %ecx
	movl	%ebx, %r10d
	shrl	$24, %eax
	andl	$15, %eax
	testl	%ecx, %ecx
	je	.L1987
	cmpl	$2, %ecx
	jne	.L2142
	leaq	32(%r13), %rcx
	leaq	0(,%rbx,8), %rsi
	movb	$8, -2192(%rbp)
	movb	$5, -2188(%rbp)
	movl	$1, -2184(%rbp)
	movb	$0, -2180(%rbp)
	movq	$0, -2176(%rbp)
	movl	$-1, -2168(%rbp)
	movq	%rcx, -2304(%rbp)
	movq	%rsi, -2280(%rbp)
	cmpl	$15, %eax
	je	.L2023
	movq	%rcx, %rax
	addq	%rsi, %rax
.L2024:
	movq	(%rax), %r11
	leaq	1(%rbx), %r12
	movl	20(%r11), %eax
	movl	%eax, %r8d
	andl	$16777215, %r8d
	movl	%r8d, %ecx
	leaq	(%rcx,%rcx,4), %rsi
	movq	32(%r14), %rcx
	leaq	(%rcx,%rsi,8), %r15
	cmpb	$8, 1(%r15)
	je	.L1989
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	jne	.L2147
.L2032:
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rax
	movq	24(%rcx,%rax,8), %rcx
	testq	%rcx, %rcx
	jne	.L2033
	movq	8(%r11), %rcx
.L2033:
	movzbl	1(%r15), %edx
	movq	128(%r14), %rdi
	movq	%r13, %r8
	movq	%r11, %rsi
	pushq	-2168(%rbp)
	pushq	-2176(%rbp)
	pushq	-2184(%rbp)
	pushq	-2192(%rbp)
	call	_ZN2v88internal8compiler21RepresentationChanger20GetRepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE@PLT
	movl	20(%r13), %edx
	addq	$32, %rsp
	movq	%rax, %r15
	movl	%edx, %eax
	xorl	$251658240, %eax
	testl	$251658240, %eax
	je	.L2034
	movq	32(%r13,%rbx,8), %rdi
	movq	-2304(%rbp), %rcx
	movq	%r13, %rax
	addq	-2280(%rbp), %rcx
	cmpq	%r15, %rdi
	je	.L2142
.L2035:
	leaq	1(%rbx), %r12
	leaq	0(,%r12,4), %rdx
	movq	%r12, %rsi
	subq	%rdx, %rsi
	leaq	(%rax,%rsi,8), %rsi
	testq	%rdi, %rdi
	je	.L2036
	movq	%rcx, -2304(%rbp)
	movq	%rsi, -2280(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-2304(%rbp), %rcx
	movq	-2280(%rbp), %rsi
.L2036:
	movq	%r15, (%rcx)
	testq	%r15, %r15
	je	.L2143
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L2143:
	movl	20(%r13), %edx
.L1989:
	cmpq	-2272(%rbp), %rbx
	je	.L2137
.L2148:
	movq	%r12, %rbx
	jmp	.L2039
	.p2align 4,,10
	.p2align 3
.L2142:
	leaq	1(%rbx), %r12
	cmpq	-2272(%rbp), %rbx
	jne	.L2148
.L2137:
	movq	%r14, %r12
.L2038:
	movslq	-2284(%rbp), %rax
	movq	-2264(%rbp), %rbx
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rcx
	movq	%rcx, %xmm1
	movq	%rax, %r13
	leaq	(%rbx,%rax,8), %rbx
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm4
	punpcklqdq	%xmm4, %xmm1
	movaps	%xmm1, -2304(%rbp)
	jmp	.L1986
	.p2align 4,,10
	.p2align 3
.L2151:
	cmpl	%eax, %r13d
	jge	.L1984
	leaq	32(%rbx), %rax
.L2075:
	movq	(%rax), %rax
	movl	120(%r12), %esi
	movq	%rax, -2240(%rbp)
	testl	%esi, %esi
	jne	.L2074
	movl	20(%rax), %esi
	andl	$16777215, %esi
	movl	%esi, %eax
	leaq	(%rax,%rax,4), %rdx
	movq	32(%r12), %rax
	leaq	(%rax,%rdx,8), %r14
	cmpb	$0, (%r14)
	je	.L2149
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	jne	.L2150
.L2144:
	movl	8(%r14), %r8d
.L2057:
	movzbl	4(%r14), %r15d
	movl	%r8d, %edi
	xorl	%esi, %esi
	movl	%r8d, -2280(%rbp)
	call	_ZN2v88internal8compiler10Truncation23GeneralizeIdentifyZerosENS1_13IdentifyZerosES3_@PLT
	xorl	%esi, %esi
	movl	%r15d, %edi
	movl	%eax, -2272(%rbp)
	call	_ZN2v88internal8compiler10Truncation10GeneralizeENS2_14TruncationKindES3_@PLT
	movl	-2272(%rbp), %edx
	movb	%al, 4(%r14)
	movl	%edx, 8(%r14)
	cmpb	%al, %r15b
	jne	.L2087
	movl	-2280(%rbp), %r8d
	cmpl	%edx, %r8d
	je	.L2062
.L2087:
	cmpb	$3, (%r14)
	je	.L2064
	movq	216(%r12), %rax
	movq	200(%r12), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L2065
	movq	-2240(%rbp), %rax
	movq	%rax, (%rdx)
	addq	$8, 200(%r12)
.L2066:
	movb	$3, (%r14)
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	leaq	.LC26(%rip), %rdi
	jne	.L2145
	.p2align 4,,10
	.p2align 3
.L2062:
	movq	-2264(%rbp), %rax
	movl	20(%rax), %edx
.L2074:
	addl	$1, %r13d
	addq	$8, %rbx
.L1986:
	movl	%edx, %eax
	shrl	$24, %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2151
	movq	-2264(%rbp), %rax
	movq	32(%rax), %rax
	cmpl	8(%rax), %r13d
	jge	.L1984
	movq	%rbx, %rcx
	subq	-2264(%rbp), %rcx
	leaq	16(%rax,%rcx), %rax
	jmp	.L2075
	.p2align 4,,10
	.p2align 3
.L1987:
	leaq	32(%r13), %rdx
	leaq	0(,%rbx,8), %rcx
	addq	%rcx, %rdx
	cmpl	$15, %eax
	jne	.L1991
	movq	32(%r13), %rax
	leaq	16(%rax,%rcx), %rdx
.L1991:
	movq	(%rdx), %rax
	movl	20(%rax), %esi
	movq	%rax, -2248(%rbp)
	andl	$16777215, %esi
	movl	%esi, %eax
	leaq	(%rax,%rax,4), %rdx
	movq	32(%r14), %rax
	leaq	(%rax,%rdx,8), %r15
	cmpb	$0, (%r15)
	je	.L2152
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	jne	.L2153
.L2139:
	movl	8(%r15), %edx
.L2005:
	movzbl	4(%r15), %r12d
	movl	%edx, %edi
	movl	$1, %esi
	movl	%edx, -2304(%rbp)
	call	_ZN2v88internal8compiler10Truncation23GeneralizeIdentifyZerosENS1_13IdentifyZerosES3_@PLT
	movl	$5, %esi
	movl	%r12d, %edi
	movl	%eax, -2280(%rbp)
	call	_ZN2v88internal8compiler10Truncation10GeneralizeENS2_14TruncationKindES3_@PLT
	movl	-2280(%rbp), %ecx
	movl	-2304(%rbp), %edx
	movb	%al, 4(%r15)
	movl	%ecx, 8(%r15)
	cmpl	%edx, %ecx
	jne	.L2086
	cmpb	%al, %r12b
	je	.L2003
.L2086:
	cmpb	$3, (%r15)
	jne	.L2154
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	jne	.L2155
.L2003:
	movl	20(%r13), %edx
	leaq	1(%rbx), %r12
	jmp	.L1989
	.p2align 4,,10
	.p2align 3
.L2023:
	movq	32(%r13), %rax
	leaq	16(%rax,%rsi), %rax
	jmp	.L2024
	.p2align 4,,10
	.p2align 3
.L2150:
	xorl	%eax, %eax
	leaq	.LC25(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movl	8(%r14), %r8d
	movzbl	4(%r14), %eax
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	movb	%al, -2200(%rbp)
	movl	%r8d, -2196(%rbp)
	je	.L2057
	leaq	-672(%rbp), %rax
	leaq	-752(%rbp), %r15
	movq	%rax, %rdi
	movq	%rax, -2272(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	xorl	%edx, %edx
	pxor	%xmm0, %xmm0
	movq	%r15, %rdi
	movw	%dx, -448(%rbp)
	movq	stdout(%rip), %rdx
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movq	%rax, -672(%rbp)
	movq	$0, -456(%rbp)
	movups	%xmm0, -440(%rbp)
	movups	%xmm0, -424(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	leaq	-2200(%rbp), %rdi
	movq	%rax, -752(%rbp)
	addq	$40, %rax
	movq	%rax, -672(%rbp)
	call	_ZNK2v88internal8compiler10Truncation11descriptionEv@PLT
	testq	%rax, %rax
	je	.L2156
	movq	%rax, %rdi
	movq	%rax, -2280(%rbp)
	call	strlen@PLT
	movq	-2280(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L2059:
	movq	-752(%rbp), %rax
	movq	-24(%rax), %rax
	movq	-512(%rbp,%rax), %rdi
	testq	%rdi, %rdi
	je	.L2008
	cmpb	$0, 56(%rdi)
	je	.L2060
	movsbl	67(%rdi), %esi
.L2061:
	movq	%r15, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movdqa	-2304(%rbp), %xmm3
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	-688(%rbp), %rdi
	movq	%rax, -672(%rbp)
	movaps	%xmm3, -752(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	-2272(%rbp), %rdi
	movq	%rax, -752(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -672(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L2144
	.p2align 4,,10
	.p2align 3
.L2064:
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	je	.L2062
	leaq	.LC27(%rip), %rdi
.L2145:
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movzbl	4(%r14), %esi
	movl	8(%r14), %edx
	movzbl	_ZN2v88internal25FLAG_trace_representationE(%rip), %eax
	movb	%sil, -2192(%rbp)
	movl	%edx, -2188(%rbp)
	testb	%al, %al
	je	.L2062
	leaq	-320(%rbp), %r14
	leaq	-400(%rbp), %r15
	movq	%r14, %rdi
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%r15, %rdi
	movq	stdout(%rip), %rdx
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movw	%ax, -96(%rbp)
	movq	$0, -104(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	leaq	-2192(%rbp), %rdi
	movq	%rax, -400(%rbp)
	addq	$40, %rax
	movq	%rax, -320(%rbp)
	call	_ZNK2v88internal8compiler10Truncation11descriptionEv@PLT
	testq	%rax, %rax
	je	.L2157
	movq	%rax, %rdi
	movq	%rax, -2272(%rbp)
	call	strlen@PLT
	movq	-2272(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L2071:
	movq	-400(%rbp), %rax
	movq	-24(%rax), %rax
	movq	-160(%rbp,%rax), %rdi
	testq	%rdi, %rdi
	je	.L2008
	cmpb	$0, 56(%rdi)
	je	.L2072
	movsbl	67(%rdi), %esi
.L2073:
	movq	%r15, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movdqa	-2304(%rbp), %xmm5
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	-336(%rbp), %rdi
	movq	%rax, -320(%rbp)
	movaps	%xmm5, -400(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -400(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L2062
	.p2align 4,,10
	.p2align 3
.L2149:
	movb	$3, (%r14)
	movq	72(%r12), %rsi
	cmpq	80(%r12), %rsi
	je	.L2046
	movq	-2240(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 72(%r12)
.L2047:
	movq	216(%r12), %rax
	movq	200(%r12), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L2048
	movq	-2240(%rbp), %rax
	movq	%rax, (%rdx)
	addq	$8, 200(%r12)
.L2049:
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	jne	.L2158
.L2050:
	movzbl	4(%r14), %r8d
	movl	8(%r14), %edi
	xorl	%esi, %esi
	movb	%r8b, -2272(%rbp)
	call	_ZN2v88internal8compiler10Truncation23GeneralizeIdentifyZerosENS1_13IdentifyZerosES3_@PLT
	movzbl	-2272(%rbp), %r8d
	xorl	%esi, %esi
	movl	%eax, %r15d
	movl	%r8d, %edi
	call	_ZN2v88internal8compiler10Truncation10GeneralizeENS2_14TruncationKindES3_@PLT
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	movl	%r15d, 8(%r14)
	movb	%al, 4(%r14)
	movb	%al, -2208(%rbp)
	movl	%r15d, -2204(%rbp)
	je	.L2062
	leaq	-1024(%rbp), %r14
	leaq	-1104(%rbp), %r15
	movq	%r14, %rdi
	call	_ZNSt8ios_baseC2Ev@PLT
	xorl	%ecx, %ecx
	pxor	%xmm0, %xmm0
	movq	%r15, %rdi
	movq	stdout(%rip), %rdx
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movq	$0, -808(%rbp)
	movq	%rax, -1024(%rbp)
	movw	%cx, -800(%rbp)
	movups	%xmm0, -792(%rbp)
	movups	%xmm0, -776(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	leaq	-2208(%rbp), %rdi
	movq	%rax, -1104(%rbp)
	addq	$40, %rax
	movq	%rax, -1024(%rbp)
	call	_ZNK2v88internal8compiler10Truncation11descriptionEv@PLT
	testq	%rax, %rax
	je	.L2159
	movq	%rax, %rdi
	movq	%rax, -2272(%rbp)
	call	strlen@PLT
	movq	-2272(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L2053:
	movq	-1104(%rbp), %rax
	movq	-24(%rax), %rax
	movq	-864(%rbp,%rax), %rdi
	testq	%rdi, %rdi
	je	.L2008
	cmpb	$0, 56(%rdi)
	je	.L2054
	movsbl	67(%rdi), %esi
.L2055:
	movq	%r15, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movdqa	-2304(%rbp), %xmm5
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	-1040(%rbp), %rdi
	movq	%rax, -1024(%rbp)
	movaps	%xmm5, -1104(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -1104(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -1024(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L2062
	.p2align 4,,10
	.p2align 3
.L1984:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2160
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2060:
	.cfi_restore_state
	movq	%rdi, -2280(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-2280(%rbp), %rdi
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L2061
	call	*%rax
	movsbl	%al, %esi
	jmp	.L2061
	.p2align 4,,10
	.p2align 3
.L2034:
	movq	32(%r13), %rax
	movq	-2280(%rbp), %rcx
	leaq	1(%rbx), %r12
	leaq	16(%rax,%rcx), %rcx
	movq	(%rcx), %rdi
	cmpq	%r15, %rdi
	jne	.L2035
	jmp	.L1989
	.p2align 4,,10
	.p2align 3
.L2154:
	movq	216(%r14), %rax
	movq	200(%r14), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L2014
	movq	-2248(%rbp), %rax
	movq	%rax, (%rdx)
	addq	$8, 200(%r14)
.L2015:
	movb	$3, (%r15)
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	leaq	.LC26(%rip), %rdi
	je	.L2003
.L2140:
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movzbl	4(%r15), %ecx
	movl	8(%r15), %edx
	movzbl	_ZN2v88internal25FLAG_trace_representationE(%rip), %eax
	movb	%cl, -2216(%rbp)
	movl	%edx, -2212(%rbp)
	testb	%al, %al
	je	.L2003
	leaq	-1376(%rbp), %rax
	leaq	-1456(%rbp), %r12
	movq	%rax, %rdi
	movq	%rax, -2280(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	pxor	%xmm0, %xmm0
	xorl	%r9d, %r9d
	movq	%r12, %rdi
	movq	stdout(%rip), %rdx
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movw	%r9w, -1152(%rbp)
	movq	%rax, -1376(%rbp)
	movups	%xmm0, -1144(%rbp)
	movups	%xmm0, -1128(%rbp)
	movq	$0, -1160(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	leaq	-2216(%rbp), %rdi
	movq	%rax, -1456(%rbp)
	addq	$40, %rax
	movq	%rax, -1376(%rbp)
	call	_ZNK2v88internal8compiler10Truncation11descriptionEv@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L2161
	movq	%rax, %rdi
	call	strlen@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L2020:
	movq	-1456(%rbp), %rax
	movq	-24(%rax), %rax
	movq	-1216(%rbp,%rax), %r15
	testq	%r15, %r15
	je	.L2008
	cmpb	$0, 56(%r15)
	je	.L2021
	movsbl	67(%r15), %esi
.L2022:
	movq	%r12, %rdi
	leaq	1(%rbx), %r12
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movdqa	-2320(%rbp), %xmm6
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	-1392(%rbp), %rdi
	movq	%rax, -1376(%rbp)
	movaps	%xmm6, -1456(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	-2280(%rbp), %rdi
	movq	%rax, -1456(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -1376(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movl	20(%r13), %edx
	jmp	.L1989
	.p2align 4,,10
	.p2align 3
.L2157:
	movq	-400(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r15, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L2071
	.p2align 4,,10
	.p2align 3
.L2147:
	movq	0(%r13), %rcx
	movq	(%r11), %rax
	andl	$16777215, %edx
	leaq	.LC13(%rip), %rdi
	movl	%edx, %esi
	movq	%r11, -2328(%rbp)
	movq	8(%rcx), %r12
	movq	8(%rax), %r9
	movl	%r10d, %ecx
	xorl	%eax, %eax
	movq	%r12, %rdx
	call	_ZN2v88internal6PrintFEPKcz@PLT
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	movq	-2328(%rbp), %r11
	je	.L2141
	xorl	%eax, %eax
	leaq	.LC14(%rip), %rdi
	movq	%r11, -2328(%rbp)
	call	_ZN2v88internal6PrintFEPKcz@PLT
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	movq	-2328(%rbp), %r11
	je	.L2141
	leaq	-320(%rbp), %rax
	movq	%r11, -2344(%rbp)
	leaq	-400(%rbp), %r12
	movq	%rax, %rdi
	movq	%rax, -2328(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	xorl	%r8d, %r8d
	pxor	%xmm6, %xmm6
	movq	%r12, %rdi
	movq	stdout(%rip), %rdx
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rcx
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movw	%r8w, -96(%rbp)
	movq	%rcx, -320(%rbp)
	movups	%xmm6, -88(%rbp)
	movups	%xmm6, -72(%rbp)
	movq	$0, -104(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rdx
	movq	%r12, %rdi
	movq	%rdx, -400(%rbp)
	addq	$40, %rdx
	movq	%rdx, -320(%rbp)
	movzbl	1(%r15), %esi
	call	_ZN2v88internallsERSoNS0_21MachineRepresentationE@PLT
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movdqa	-2320(%rbp), %xmm4
	movq	%rdx, -320(%rbp)
	leaq	-336(%rbp), %rdx
	movq	%rdx, %rdi
	movq	%rdx, -2336(%rbp)
	movaps	%xmm4, -400(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	-2328(%rbp), %rdi
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rdx
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rcx
	movq	%rdx, -400(%rbp)
	movq	%rcx, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	movq	-2344(%rbp), %r11
	je	.L2141
	xorl	%eax, %eax
	leaq	.LC15(%rip), %rdi
	movq	%r11, -2344(%rbp)
	call	_ZN2v88internal6PrintFEPKcz@PLT
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	movq	-2344(%rbp), %r11
	je	.L2141
	movq	-2328(%rbp), %rdi
	movq	%r11, -2344(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	stdout(%rip), %rdx
	xorl	%edi, %edi
	pxor	%xmm7, %xmm7
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movw	%di, -96(%rbp)
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movq	%r12, %rdi
	movups	%xmm7, -88(%rbp)
	movups	%xmm7, -72(%rbp)
	movq	%rax, -320(%rbp)
	movq	$0, -104(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movl	$8, %esi
	movq	%r12, %rdi
	movq	%rax, -400(%rbp)
	addq	$40, %rax
	movq	%rax, -320(%rbp)
	call	_ZN2v88internallsERSoNS0_21MachineRepresentationE@PLT
	movl	$1, %edx
	leaq	.LC4(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	-2200(%rbp), %rdi
	movb	$5, -2200(%rbp)
	movl	$1, -2196(%rbp)
	call	_ZNK2v88internal8compiler10Truncation11descriptionEv@PLT
	movq	-2344(%rbp), %r11
	testq	%rax, %rax
	je	.L2162
	movq	%rax, %rdi
	movq	%r11, -2352(%rbp)
	movq	%rax, -2344(%rbp)
	call	strlen@PLT
	movq	-2344(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-2352(%rbp), %r11
.L2031:
	movdqa	-2320(%rbp), %xmm3
	movq	-2336(%rbp), %rdi
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	movq	%r11, -2344(%rbp)
	movq	%rax, -320(%rbp)
	movaps	%xmm3, -400(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	-2328(%rbp), %rdi
	movq	%rax, -400(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	movq	-2344(%rbp), %r11
	je	.L2141
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	movq	%r11, -2328(%rbp)
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-2328(%rbp), %r11
.L2141:
	movl	20(%r11), %eax
	movq	32(%r14), %rcx
	jmp	.L2032
	.p2align 4,,10
	.p2align 3
.L2153:
	xorl	%eax, %eax
	leaq	.LC25(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movzbl	4(%r15), %eax
	movl	8(%r15), %edx
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	movb	%al, -2224(%rbp)
	movl	%edx, -2220(%rbp)
	je	.L2005
	leaq	-1728(%rbp), %rax
	leaq	-1808(%rbp), %r12
	movq	%rax, %rdi
	movq	%rax, -2280(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	xorl	%r10d, %r10d
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movq	stdout(%rip), %rdx
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movq	$0, -1512(%rbp)
	movq	%rax, -1728(%rbp)
	movw	%r10w, -1504(%rbp)
	movups	%xmm0, -1496(%rbp)
	movups	%xmm0, -1480(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	leaq	-2224(%rbp), %rdi
	movq	%rax, -1808(%rbp)
	addq	$40, %rax
	movq	%rax, -1728(%rbp)
	call	_ZNK2v88internal8compiler10Truncation11descriptionEv@PLT
	testq	%rax, %rax
	je	.L2163
	movq	%rax, %rdi
	movq	%rax, -2304(%rbp)
	call	strlen@PLT
	movq	-2304(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L2007:
	movq	-1808(%rbp), %rax
	movq	-24(%rax), %rax
	movq	-1568(%rbp,%rax), %rdi
	testq	%rdi, %rdi
	je	.L2008
	cmpb	$0, 56(%rdi)
	je	.L2009
	movsbl	67(%rdi), %esi
.L2010:
	movq	%r12, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movdqa	-2320(%rbp), %xmm4
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	-1744(%rbp), %rdi
	movq	%rax, -1728(%rbp)
	movaps	%xmm4, -1808(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	-2280(%rbp), %rdi
	movq	%rax, -1808(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -1728(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L2139
	.p2align 4,,10
	.p2align 3
.L2156:
	movq	-752(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r15, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L2059
	.p2align 4,,10
	.p2align 3
.L2152:
	movb	$3, (%r15)
	movq	72(%r14), %rsi
	cmpq	80(%r14), %rsi
	je	.L1992
	movq	-2248(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 72(%r14)
.L1993:
	movq	216(%r14), %rax
	movq	200(%r14), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L1994
	movq	-2248(%rbp), %rax
	movq	%rax, (%rdx)
	addq	$8, 200(%r14)
.L1995:
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	jne	.L2164
.L1996:
	movzbl	4(%r15), %r8d
	movl	8(%r15), %edi
	movl	$1, %esi
	movb	%r8b, -2280(%rbp)
	call	_ZN2v88internal8compiler10Truncation23GeneralizeIdentifyZerosENS1_13IdentifyZerosES3_@PLT
	movzbl	-2280(%rbp), %r8d
	movl	$5, %esi
	movl	%eax, %r12d
	movl	%r8d, %edi
	call	_ZN2v88internal8compiler10Truncation10GeneralizeENS2_14TruncationKindES3_@PLT
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	movl	%r12d, 8(%r15)
	movb	%al, 4(%r15)
	movb	%al, -2232(%rbp)
	movl	%r12d, -2228(%rbp)
	je	.L2003
	leaq	-2080(%rbp), %rax
	leaq	-2160(%rbp), %r12
	movq	%rax, %rdi
	movq	%rax, -2280(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	pxor	%xmm0, %xmm0
	xorl	%r11d, %r11d
	movq	%r12, %rdi
	movq	stdout(%rip), %rdx
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movw	%r11w, -1856(%rbp)
	movq	%rax, -2080(%rbp)
	movups	%xmm0, -1848(%rbp)
	movups	%xmm0, -1832(%rbp)
	movq	$0, -1864(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	leaq	-2232(%rbp), %rdi
	movq	%rax, -2160(%rbp)
	addq	$40, %rax
	movq	%rax, -2080(%rbp)
	call	_ZNK2v88internal8compiler10Truncation11descriptionEv@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L2165
	movq	%rax, %rdi
	call	strlen@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L1999:
	movq	-2160(%rbp), %rax
	movq	-24(%rax), %rax
	movq	-1920(%rbp,%rax), %r15
	testq	%r15, %r15
	je	.L2008
	cmpb	$0, 56(%r15)
	je	.L2001
	movsbl	67(%r15), %esi
.L2002:
	movq	%r12, %rdi
	leaq	1(%rbx), %r12
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movdqa	-2320(%rbp), %xmm7
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	-2096(%rbp), %rdi
	movq	%rax, -2080(%rbp)
	movaps	%xmm7, -2160(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	-2280(%rbp), %rdi
	movq	%rax, -2160(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -2080(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movl	20(%r13), %edx
	jmp	.L1989
	.p2align 4,,10
	.p2align 3
.L2158:
	movq	-2240(%rbp), %rax
	leaq	.LC24(%rip), %rdi
	movl	20(%rax), %esi
	xorl	%eax, %eax
	andl	$16777215, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L2050
	.p2align 4,,10
	.p2align 3
.L2072:
	movq	%rdi, -2272(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-2272(%rbp), %rdi
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L2073
	call	*%rax
	movsbl	%al, %esi
	jmp	.L2073
	.p2align 4,,10
	.p2align 3
.L2009:
	movq	%rdi, -2304(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-2304(%rbp), %rdi
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L2010
	call	*%rax
	movsbl	%al, %esi
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2054:
	movq	%rdi, -2272(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-2272(%rbp), %rdi
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L2055
	call	*%rax
	movsbl	%al, %esi
	jmp	.L2055
	.p2align 4,,10
	.p2align 3
.L2155:
	leaq	.LC27(%rip), %rdi
	jmp	.L2140
.L2161:
	movq	-1456(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L2020
	.p2align 4,,10
	.p2align 3
.L2048:
	leaq	-2240(%rbp), %rsi
	leaq	136(%r12), %rdi
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	jmp	.L2049
	.p2align 4,,10
	.p2align 3
.L2046:
	leaq	-2240(%rbp), %rdx
	leaq	56(%r12), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L2047
	.p2align 4,,10
	.p2align 3
.L2065:
	leaq	-2240(%rbp), %rsi
	leaq	136(%r12), %rdi
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	jmp	.L2066
	.p2align 4,,10
	.p2align 3
.L2162:
	movq	-400(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	movq	-2344(%rbp), %r11
	jmp	.L2031
	.p2align 4,,10
	.p2align 3
.L2163:
	movq	-1808(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L2007
	.p2align 4,,10
	.p2align 3
.L2164:
	movq	-2248(%rbp), %rax
	leaq	.LC24(%rip), %rdi
	movl	20(%rax), %esi
	xorl	%eax, %eax
	andl	$16777215, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1996
	.p2align 4,,10
	.p2align 3
.L2021:
	movq	%r15, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%r15), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L2022
	movq	%r15, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L2022
	.p2align 4,,10
	.p2align 3
.L2146:
	movq	-2264(%rbp), %rax
	movl	20(%rax), %edx
	jmp	.L2038
	.p2align 4,,10
	.p2align 3
.L2159:
	movq	-1104(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r15, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L2053
.L1992:
	leaq	-2248(%rbp), %rdx
	leaq	56(%r14), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1993
.L1994:
	leaq	-2248(%rbp), %rsi
	leaq	136(%r14), %rdi
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	jmp	.L1995
.L2014:
	leaq	-2248(%rbp), %rsi
	leaq	136(%r14), %rdi
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	jmp	.L2015
.L2001:
	movq	%r15, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%r15), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L2002
	movq	%r15, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L2002
.L2165:
	movq	-2160(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L1999
.L2008:
	call	_ZSt16__throw_bad_castv@PLT
.L2160:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23016:
	.size	_ZN2v88internal8compiler22RepresentationSelector11VisitInputsEPNS1_4NodeE, .-_ZN2v88internal8compiler22RepresentationSelector11VisitInputsEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler22RepresentationSelector11VisitReturnEPNS1_4NodeE,"axG",@progbits,_ZN2v88internal8compiler22RepresentationSelector11VisitReturnEPNS1_4NodeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler22RepresentationSelector11VisitReturnEPNS1_4NodeE
	.type	_ZN2v88internal8compiler22RepresentationSelector11VisitReturnEPNS1_4NodeE, @function
_ZN2v88internal8compiler22RepresentationSelector11VisitReturnEPNS1_4NodeE:
.LFB23017:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$2360, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -2280(%rbp)
	movq	(%rsi), %rdi
	movl	20(%rdi), %r13d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18OperatorProperties15HasContextInputEPKNS1_8OperatorE@PLT
	movq	(%r15), %rdi
	movzbl	%al, %ebx
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	addl	%r13d, %ebx
	movb	$4, -2224(%rbp)
	movzbl	%al, %eax
	movb	$2, -2220(%rbp)
	addl	%ebx, %eax
	movl	$0, -2216(%rbp)
	movl	%eax, -2320(%rbp)
	movl	120(%r12), %eax
	movb	$0, -2212(%rbp)
	movq	$0, -2208(%rbp)
	movl	$-1, -2200(%rbp)
	testl	%eax, %eax
	je	.L2167
	cmpl	$2, %eax
	je	.L2168
	movl	20(%r15), %esi
.L2169:
	cmpl	$1, -2320(%rbp)
	jle	.L2237
	movl	-2320(%rbp), %eax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	-2280(%rbp), %rbx
	movq	%r12, %r13
	movq	%rdx, %xmm2
	movl	$1, %r15d
	subl	$2, %eax
	addq	$2, %rax
	movq	%rax, -2288(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm5
	punpcklqdq	%xmm5, %xmm2
	movaps	%xmm2, -2352(%rbp)
	.p2align 4,,10
	.p2align 3
.L2238:
	movl	%esi, %eax
	movl	120(%r13), %edx
	movl	%r15d, %ecx
	shrl	$24, %eax
	andl	$15, %eax
	testl	%edx, %edx
	je	.L2186
	cmpl	$2, %edx
	jne	.L2357
	leaq	32(%rbx), %rdx
	leaq	0(,%r15,8), %rdi
	movb	$8, -2192(%rbp)
	movb	$5, -2188(%rbp)
	movl	$1, -2184(%rbp)
	movb	$0, -2180(%rbp)
	movq	$0, -2176(%rbp)
	movl	$-1, -2168(%rbp)
	movq	%rdx, -2328(%rbp)
	movq	%rdi, -2304(%rbp)
	cmpl	$15, %eax
	je	.L2222
	movq	%rdi, %rax
	addq	%rdx, %rax
.L2223:
	movq	(%rax), %r11
	leaq	1(%r15), %r12
	movl	20(%r11), %eax
	movl	%eax, %r8d
	andl	$16777215, %r8d
	movl	%r8d, %edx
	leaq	(%rdx,%rdx,4), %rdi
	movq	32(%r13), %rdx
	leaq	(%rdx,%rdi,8), %r14
	cmpb	$8, 1(%r14)
	je	.L2188
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	jne	.L2361
.L2231:
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rax
	movq	24(%rdx,%rax,8), %rcx
	testq	%rcx, %rcx
	jne	.L2232
	movq	8(%r11), %rcx
.L2232:
	movzbl	1(%r14), %edx
	movq	128(%r13), %rdi
	movq	%r11, %rsi
	movq	%rbx, %r8
	pushq	-2168(%rbp)
	pushq	-2176(%rbp)
	pushq	-2184(%rbp)
	pushq	-2192(%rbp)
	call	_ZN2v88internal8compiler21RepresentationChanger20GetRepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE@PLT
	movl	20(%rbx), %esi
	addq	$32, %rsp
	movq	%rax, %r14
	movl	%esi, %eax
	xorl	$251658240, %eax
	testl	$251658240, %eax
	je	.L2233
	movq	32(%rbx,%r15,8), %rdi
	movq	-2304(%rbp), %rcx
	movq	%rbx, %rax
	addq	-2328(%rbp), %rcx
	cmpq	%rdi, %r14
	je	.L2357
.L2234:
	leaq	1(%r15), %r12
	leaq	0(,%r12,4), %rdx
	movq	%r12, %rsi
	subq	%rdx, %rsi
	leaq	(%rax,%rsi,8), %r15
	testq	%rdi, %rdi
	je	.L2235
	movq	%r15, %rsi
	movq	%rcx, -2304(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-2304(%rbp), %rcx
.L2235:
	movq	%r14, (%rcx)
	testq	%r14, %r14
	je	.L2358
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L2358:
	movl	20(%rbx), %esi
	.p2align 4,,10
	.p2align 3
.L2188:
	movq	%r12, %r15
	cmpq	%r12, -2288(%rbp)
	jne	.L2238
	movq	%r13, %r12
.L2237:
	movslq	-2320(%rbp), %rax
	movq	-2280(%rbp), %rbx
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm1
	movq	%rax, %r13
	leaq	(%rbx,%rax,8), %rbx
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm4
	punpcklqdq	%xmm4, %xmm1
	movaps	%xmm1, -2320(%rbp)
	jmp	.L2185
	.p2align 4,,10
	.p2align 3
.L2364:
	cmpl	%eax, %r13d
	jge	.L2166
	leaq	32(%rbx), %rax
.L2274:
	movq	(%rax), %rax
	movl	120(%r12), %edi
	movq	%rax, -2264(%rbp)
	testl	%edi, %edi
	jne	.L2273
	movl	20(%rax), %esi
	andl	$16777215, %esi
	movl	%esi, %eax
	leaq	(%rax,%rax,4), %rdx
	movq	32(%r12), %rax
	leaq	(%rax,%rdx,8), %r14
	cmpb	$0, (%r14)
	je	.L2362
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	jne	.L2363
.L2359:
	movl	8(%r14), %r8d
.L2256:
	movzbl	4(%r14), %r15d
	movl	%r8d, %edi
	xorl	%esi, %esi
	movl	%r8d, -2304(%rbp)
	call	_ZN2v88internal8compiler10Truncation23GeneralizeIdentifyZerosENS1_13IdentifyZerosES3_@PLT
	xorl	%esi, %esi
	movl	%r15d, %edi
	movl	%eax, -2288(%rbp)
	call	_ZN2v88internal8compiler10Truncation10GeneralizeENS2_14TruncationKindES3_@PLT
	movl	-2288(%rbp), %edx
	movl	-2304(%rbp), %r8d
	movb	%al, 4(%r14)
	movl	%edx, 8(%r14)
	cmpl	%edx, %r8d
	jne	.L2287
	cmpb	%al, %r15b
	je	.L2261
.L2287:
	cmpb	$3, (%r14)
	je	.L2263
	movq	216(%r12), %rax
	movq	200(%r12), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L2264
	movq	-2264(%rbp), %rax
	movq	%rax, (%rdx)
	addq	$8, 200(%r12)
.L2265:
	movb	$3, (%r14)
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	leaq	.LC26(%rip), %rdi
	jne	.L2360
	.p2align 4,,10
	.p2align 3
.L2261:
	movq	-2280(%rbp), %rax
	movl	20(%rax), %esi
.L2273:
	addl	$1, %r13d
	addq	$8, %rbx
.L2185:
	movl	%esi, %eax
	shrl	$24, %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2364
	movq	-2280(%rbp), %rax
	movq	32(%rax), %rax
	cmpl	8(%rax), %r13d
	jge	.L2166
	movq	%rbx, %rdx
	subq	-2280(%rbp), %rdx
	leaq	16(%rax,%rdx), %rax
	jmp	.L2274
	.p2align 4,,10
	.p2align 3
.L2357:
	leaq	1(%r15), %r12
	jmp	.L2188
	.p2align 4,,10
	.p2align 3
.L2186:
	leaq	32(%rbx), %rdx
	leaq	0(,%r15,8), %rcx
	addq	%rcx, %rdx
	cmpl	$15, %eax
	jne	.L2190
	movq	32(%rbx), %rax
	leaq	16(%rax,%rcx), %rdx
.L2190:
	movq	(%rdx), %rax
	movl	20(%rax), %esi
	movq	%rax, -2272(%rbp)
	andl	$16777215, %esi
	movl	%esi, %eax
	leaq	(%rax,%rax,4), %rdx
	movq	32(%r13), %rax
	leaq	(%rax,%rdx,8), %r14
	cmpb	$0, (%r14)
	je	.L2365
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	jne	.L2366
.L2353:
	movl	8(%r14), %edx
.L2204:
	movzbl	4(%r14), %r12d
	movl	%edx, %edi
	movl	$1, %esi
	movl	%edx, -2328(%rbp)
	call	_ZN2v88internal8compiler10Truncation23GeneralizeIdentifyZerosENS1_13IdentifyZerosES3_@PLT
	movl	$5, %esi
	movl	%r12d, %edi
	movl	%eax, -2304(%rbp)
	call	_ZN2v88internal8compiler10Truncation10GeneralizeENS2_14TruncationKindES3_@PLT
	movl	-2304(%rbp), %ecx
	movl	-2328(%rbp), %edx
	movb	%al, 4(%r14)
	movl	%ecx, 8(%r14)
	cmpl	%edx, %ecx
	jne	.L2286
	cmpb	%al, %r12b
	je	.L2202
.L2286:
	cmpb	$3, (%r14)
	jne	.L2367
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	jne	.L2368
.L2202:
	movl	20(%rbx), %esi
	leaq	1(%r15), %r12
	jmp	.L2188
	.p2align 4,,10
	.p2align 3
.L2363:
	xorl	%eax, %eax
	leaq	.LC25(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movl	8(%r14), %r8d
	movzbl	4(%r14), %eax
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	movb	%al, -2224(%rbp)
	movl	%r8d, -2220(%rbp)
	je	.L2256
	leaq	-672(%rbp), %rax
	leaq	-752(%rbp), %r15
	movq	%rax, %rdi
	movq	%rax, -2288(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	xorl	%edx, %edx
	pxor	%xmm0, %xmm0
	movq	%r15, %rdi
	movw	%dx, -448(%rbp)
	movq	stdout(%rip), %rdx
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movq	%rax, -672(%rbp)
	movq	$0, -456(%rbp)
	movups	%xmm0, -440(%rbp)
	movups	%xmm0, -424(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	leaq	-2224(%rbp), %rdi
	movq	%rax, -752(%rbp)
	addq	$40, %rax
	movq	%rax, -672(%rbp)
	call	_ZNK2v88internal8compiler10Truncation11descriptionEv@PLT
	testq	%rax, %rax
	je	.L2369
	movq	%rax, %rdi
	movq	%rax, -2304(%rbp)
	call	strlen@PLT
	movq	-2304(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L2258:
	movq	-752(%rbp), %rax
	movq	-24(%rax), %rax
	movq	-512(%rbp,%rax), %rdi
	testq	%rdi, %rdi
	je	.L2207
	cmpb	$0, 56(%rdi)
	je	.L2259
	movsbl	67(%rdi), %esi
.L2260:
	movq	%r15, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movdqa	-2320(%rbp), %xmm3
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	-688(%rbp), %rdi
	movq	%rax, -672(%rbp)
	movaps	%xmm3, -752(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	-2288(%rbp), %rdi
	movq	%rax, -752(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -672(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L2359
	.p2align 4,,10
	.p2align 3
.L2263:
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	je	.L2261
	leaq	.LC27(%rip), %rdi
.L2360:
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movzbl	4(%r14), %esi
	movl	8(%r14), %edx
	movzbl	_ZN2v88internal25FLAG_trace_representationE(%rip), %eax
	movb	%sil, -2192(%rbp)
	movl	%edx, -2188(%rbp)
	testb	%al, %al
	je	.L2261
	leaq	-320(%rbp), %r14
	leaq	-400(%rbp), %r15
	movq	%r14, %rdi
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%r15, %rdi
	movq	stdout(%rip), %rdx
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movw	%ax, -96(%rbp)
	movq	$0, -104(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	leaq	-2192(%rbp), %rdi
	movq	%rax, -400(%rbp)
	addq	$40, %rax
	movq	%rax, -320(%rbp)
	call	_ZNK2v88internal8compiler10Truncation11descriptionEv@PLT
	testq	%rax, %rax
	je	.L2370
	movq	%rax, %rdi
	movq	%rax, -2288(%rbp)
	call	strlen@PLT
	movq	-2288(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L2270:
	movq	-400(%rbp), %rax
	movq	-24(%rax), %rax
	movq	-160(%rbp,%rax), %rdi
	testq	%rdi, %rdi
	je	.L2207
	cmpb	$0, 56(%rdi)
	je	.L2271
	movsbl	67(%rdi), %esi
.L2272:
	movq	%r15, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movdqa	-2320(%rbp), %xmm5
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	-336(%rbp), %rdi
	movq	%rax, -320(%rbp)
	movaps	%xmm5, -400(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -400(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L2261
	.p2align 4,,10
	.p2align 3
.L2362:
	movb	$3, (%r14)
	movq	72(%r12), %rsi
	cmpq	80(%r12), %rsi
	je	.L2245
	movq	-2264(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 72(%r12)
.L2246:
	movq	216(%r12), %rax
	movq	200(%r12), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L2247
	movq	-2264(%rbp), %rax
	movq	%rax, (%rdx)
	addq	$8, 200(%r12)
.L2248:
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	jne	.L2371
.L2249:
	movzbl	4(%r14), %r8d
	movl	8(%r14), %edi
	xorl	%esi, %esi
	movb	%r8b, -2288(%rbp)
	call	_ZN2v88internal8compiler10Truncation23GeneralizeIdentifyZerosENS1_13IdentifyZerosES3_@PLT
	movzbl	-2288(%rbp), %r8d
	xorl	%esi, %esi
	movl	%eax, %r15d
	movl	%r8d, %edi
	call	_ZN2v88internal8compiler10Truncation10GeneralizeENS2_14TruncationKindES3_@PLT
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	movl	%r15d, 8(%r14)
	movb	%al, 4(%r14)
	movb	%al, -2232(%rbp)
	movl	%r15d, -2228(%rbp)
	je	.L2261
	leaq	-1024(%rbp), %r14
	leaq	-1104(%rbp), %r15
	movq	%r14, %rdi
	call	_ZNSt8ios_baseC2Ev@PLT
	xorl	%ecx, %ecx
	pxor	%xmm0, %xmm0
	movq	%r15, %rdi
	movq	stdout(%rip), %rdx
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movq	$0, -808(%rbp)
	movq	%rax, -1024(%rbp)
	movw	%cx, -800(%rbp)
	movups	%xmm0, -792(%rbp)
	movups	%xmm0, -776(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	leaq	-2232(%rbp), %rdi
	movq	%rax, -1104(%rbp)
	addq	$40, %rax
	movq	%rax, -1024(%rbp)
	call	_ZNK2v88internal8compiler10Truncation11descriptionEv@PLT
	testq	%rax, %rax
	je	.L2372
	movq	%rax, %rdi
	movq	%rax, -2288(%rbp)
	call	strlen@PLT
	movq	-2288(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L2252:
	movq	-1104(%rbp), %rax
	movq	-24(%rax), %rax
	movq	-864(%rbp,%rax), %rdi
	testq	%rdi, %rdi
	je	.L2207
	cmpb	$0, 56(%rdi)
	je	.L2253
	movsbl	67(%rdi), %esi
.L2254:
	movq	%r15, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movdqa	-2320(%rbp), %xmm5
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	-1040(%rbp), %rdi
	movq	%rax, -1024(%rbp)
	movaps	%xmm5, -1104(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -1104(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -1024(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L2261
	.p2align 4,,10
	.p2align 3
.L2166:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2373
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2222:
	.cfi_restore_state
	movq	32(%rbx), %rax
	leaq	16(%rax,%rdi), %rax
	jmp	.L2223
	.p2align 4,,10
	.p2align 3
.L2259:
	movq	%rdi, -2304(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-2304(%rbp), %rdi
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L2260
	call	*%rax
	movsbl	%al, %esi
	jmp	.L2260
	.p2align 4,,10
	.p2align 3
.L2233:
	movq	32(%rbx), %rax
	movq	-2304(%rbp), %rcx
	leaq	1(%r15), %r12
	leaq	16(%rax,%rcx), %rcx
	movq	(%rcx), %rdi
	cmpq	%rdi, %r14
	jne	.L2234
	jmp	.L2188
	.p2align 4,,10
	.p2align 3
.L2367:
	movq	216(%r13), %rax
	movq	200(%r13), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L2213
	movq	-2272(%rbp), %rax
	movq	%rax, (%rdx)
	addq	$8, 200(%r13)
.L2214:
	movb	$3, (%r14)
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	leaq	.LC26(%rip), %rdi
	je	.L2202
.L2354:
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movzbl	4(%r14), %ecx
	movl	8(%r14), %edx
	movzbl	_ZN2v88internal25FLAG_trace_representationE(%rip), %eax
	movb	%cl, -2240(%rbp)
	movl	%edx, -2236(%rbp)
	testb	%al, %al
	je	.L2202
	leaq	-1376(%rbp), %rax
	leaq	-1456(%rbp), %r12
	movq	%rax, %rdi
	movq	%rax, -2304(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	pxor	%xmm0, %xmm0
	xorl	%r10d, %r10d
	movq	%r12, %rdi
	movq	stdout(%rip), %rdx
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movw	%r10w, -1152(%rbp)
	movq	%rax, -1376(%rbp)
	movups	%xmm0, -1144(%rbp)
	movups	%xmm0, -1128(%rbp)
	movq	$0, -1160(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	leaq	-2240(%rbp), %rdi
	movq	%rax, -1456(%rbp)
	addq	$40, %rax
	movq	%rax, -1376(%rbp)
	call	_ZNK2v88internal8compiler10Truncation11descriptionEv@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2374
	movq	%rax, %rdi
	call	strlen@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L2219:
	movq	-1456(%rbp), %rax
	movq	-24(%rax), %rax
	movq	-1216(%rbp,%rax), %r14
	testq	%r14, %r14
	je	.L2207
	cmpb	$0, 56(%r14)
	je	.L2220
	movsbl	67(%r14), %esi
.L2221:
	movq	%r12, %rdi
	leaq	1(%r15), %r12
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movdqa	-2352(%rbp), %xmm6
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	-1392(%rbp), %rdi
	movq	%rax, -1376(%rbp)
	movaps	%xmm6, -1456(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	-2304(%rbp), %rdi
	movq	%rax, -1456(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -1376(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movl	20(%rbx), %esi
	jmp	.L2188
	.p2align 4,,10
	.p2align 3
.L2370:
	movq	-400(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r15, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L2270
	.p2align 4,,10
	.p2align 3
.L2361:
	movq	(%r11), %rax
	movq	(%rbx), %rdx
	andl	$16777215, %esi
	leaq	.LC13(%rip), %rdi
	movq	%r11, -2336(%rbp)
	movq	8(%rax), %r9
	movq	8(%rdx), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	movq	-2336(%rbp), %r11
	je	.L2356
	xorl	%eax, %eax
	leaq	.LC14(%rip), %rdi
	movq	%r11, -2336(%rbp)
	call	_ZN2v88internal6PrintFEPKcz@PLT
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	movq	-2336(%rbp), %r11
	je	.L2356
	leaq	-320(%rbp), %rax
	movq	%r11, -2384(%rbp)
	leaq	-400(%rbp), %r12
	movq	%rax, %rdi
	movq	%rax, -2336(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	xorl	%r9d, %r9d
	pxor	%xmm6, %xmm6
	movq	%r12, %rdi
	movq	stdout(%rip), %rdx
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rcx
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movw	%r9w, -96(%rbp)
	movq	%rcx, -320(%rbp)
	movups	%xmm6, -88(%rbp)
	movups	%xmm6, -72(%rbp)
	movq	$0, -104(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rsi
	movq	%r12, %rdi
	movq	%rsi, -400(%rbp)
	addq	$40, %rsi
	movq	%rsi, -320(%rbp)
	movzbl	1(%r14), %esi
	call	_ZN2v88internallsERSoNS0_21MachineRepresentationE@PLT
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rsi
	movdqa	-2352(%rbp), %xmm4
	movq	%rsi, -2376(%rbp)
	movq	%rsi, -320(%rbp)
	leaq	-336(%rbp), %rsi
	movq	%rsi, %rdi
	movq	%rsi, -2368(%rbp)
	movaps	%xmm4, -400(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	-2336(%rbp), %rdi
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rsi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rcx
	movq	%rsi, -2360(%rbp)
	movq	%rsi, -400(%rbp)
	movq	%rcx, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	movq	-2384(%rbp), %r11
	je	.L2356
	xorl	%eax, %eax
	leaq	.LC15(%rip), %rdi
	movq	%r11, -2384(%rbp)
	call	_ZN2v88internal6PrintFEPKcz@PLT
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	movq	-2384(%rbp), %r11
	je	.L2356
	movq	-2336(%rbp), %rdi
	movq	%r11, -2384(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	xorl	%r8d, %r8d
	pxor	%xmm7, %xmm7
	movq	%r12, %rdi
	movq	stdout(%rip), %rdx
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movw	%r8w, -96(%rbp)
	movups	%xmm7, -88(%rbp)
	movups	%xmm7, -72(%rbp)
	movq	%rax, -320(%rbp)
	movq	$0, -104(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movl	$8, %esi
	movq	%r12, %rdi
	movq	%rax, -400(%rbp)
	addq	$40, %rax
	movq	%rax, -320(%rbp)
	call	_ZN2v88internallsERSoNS0_21MachineRepresentationE@PLT
	movl	$1, %edx
	leaq	.LC4(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	-2224(%rbp), %rdi
	movb	$5, -2224(%rbp)
	movl	$1, -2220(%rbp)
	call	_ZNK2v88internal8compiler10Truncation11descriptionEv@PLT
	movq	-2384(%rbp), %r11
	testq	%rax, %rax
	je	.L2375
	movq	%rax, %rdi
	movq	%r11, -2392(%rbp)
	movq	%rax, -2384(%rbp)
	call	strlen@PLT
	movq	-2384(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-2392(%rbp), %r11
.L2230:
	movq	-2376(%rbp), %rax
	movdqa	-2352(%rbp), %xmm3
	movq	%r11, -2384(%rbp)
	movq	-2368(%rbp), %rdi
	movq	%rax, -320(%rbp)
	movaps	%xmm3, -400(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	-2360(%rbp), %rax
	movq	-2336(%rbp), %rdi
	movq	%rax, -400(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	movq	-2384(%rbp), %r11
	je	.L2356
	xorl	%eax, %eax
	leaq	.LC16(%rip), %rdi
	movq	%r11, -2336(%rbp)
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-2336(%rbp), %r11
	movq	32(%r13), %rdx
	movl	20(%r11), %eax
	jmp	.L2231
	.p2align 4,,10
	.p2align 3
.L2366:
	xorl	%eax, %eax
	leaq	.LC25(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movzbl	4(%r14), %eax
	movl	8(%r14), %edx
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	movb	%al, -2248(%rbp)
	movl	%edx, -2244(%rbp)
	je	.L2204
	leaq	-1728(%rbp), %rax
	leaq	-1808(%rbp), %r12
	movq	%rax, %rdi
	movq	%rax, -2304(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	xorl	%r11d, %r11d
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movq	stdout(%rip), %rdx
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movq	$0, -1512(%rbp)
	movq	%rax, -1728(%rbp)
	movw	%r11w, -1504(%rbp)
	movups	%xmm0, -1496(%rbp)
	movups	%xmm0, -1480(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	leaq	-2248(%rbp), %rdi
	movq	%rax, -1808(%rbp)
	addq	$40, %rax
	movq	%rax, -1728(%rbp)
	call	_ZNK2v88internal8compiler10Truncation11descriptionEv@PLT
	testq	%rax, %rax
	je	.L2376
	movq	%rax, %rdi
	movq	%rax, -2328(%rbp)
	call	strlen@PLT
	movq	-2328(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L2206:
	movq	-1808(%rbp), %rax
	movq	-24(%rax), %rax
	movq	-1568(%rbp,%rax), %rdi
	testq	%rdi, %rdi
	je	.L2207
	cmpb	$0, 56(%rdi)
	je	.L2208
	movsbl	67(%rdi), %esi
.L2209:
	movq	%r12, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movdqa	-2352(%rbp), %xmm4
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	-1744(%rbp), %rdi
	movq	%rax, -1728(%rbp)
	movaps	%xmm4, -1808(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	-2304(%rbp), %rdi
	movq	%rax, -1808(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -1728(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L2353
	.p2align 4,,10
	.p2align 3
.L2369:
	movq	-752(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r15, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L2258
	.p2align 4,,10
	.p2align 3
.L2365:
	movb	$3, (%r14)
	movq	72(%r13), %rsi
	cmpq	80(%r13), %rsi
	je	.L2191
	movq	-2272(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 72(%r13)
.L2192:
	movq	216(%r13), %rax
	movq	200(%r13), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L2193
	movq	-2272(%rbp), %rax
	movq	%rax, (%rdx)
	addq	$8, 200(%r13)
.L2194:
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	jne	.L2377
.L2195:
	movzbl	4(%r14), %r8d
	movl	8(%r14), %edi
	movl	$1, %esi
	movb	%r8b, -2304(%rbp)
	call	_ZN2v88internal8compiler10Truncation23GeneralizeIdentifyZerosENS1_13IdentifyZerosES3_@PLT
	movzbl	-2304(%rbp), %r8d
	movl	$5, %esi
	movl	%eax, %r12d
	movl	%r8d, %edi
	call	_ZN2v88internal8compiler10Truncation10GeneralizeENS2_14TruncationKindES3_@PLT
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	movl	%r12d, 8(%r14)
	movb	%al, 4(%r14)
	movb	%al, -2256(%rbp)
	movl	%r12d, -2252(%rbp)
	je	.L2202
	leaq	-2080(%rbp), %rax
	leaq	-2160(%rbp), %r12
	xorl	%r14d, %r14d
	movq	%rax, %rdi
	movq	%rax, -2304(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	stdout(%rip), %rdx
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -2080(%rbp)
	movw	%r14w, -1856(%rbp)
	movups	%xmm0, -1848(%rbp)
	movups	%xmm0, -1832(%rbp)
	movq	$0, -1864(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	leaq	-2256(%rbp), %rdi
	movq	%rax, -2160(%rbp)
	addq	$40, %rax
	movq	%rax, -2080(%rbp)
	call	_ZNK2v88internal8compiler10Truncation11descriptionEv@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2378
	movq	%rax, %rdi
	call	strlen@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L2198:
	movq	-2160(%rbp), %rax
	movq	-24(%rax), %rax
	movq	-1920(%rbp,%rax), %r14
	testq	%r14, %r14
	je	.L2207
	cmpb	$0, 56(%r14)
	je	.L2200
	movsbl	67(%r14), %esi
.L2201:
	movq	%r12, %rdi
	leaq	1(%r15), %r12
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movdqa	-2352(%rbp), %xmm7
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	-2096(%rbp), %rdi
	movq	%rax, -2080(%rbp)
	movaps	%xmm7, -2160(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	-2304(%rbp), %rdi
	movq	%rax, -2160(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -2080(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movl	20(%rbx), %esi
	jmp	.L2188
	.p2align 4,,10
	.p2align 3
.L2371:
	movq	-2264(%rbp), %rax
	leaq	.LC24(%rip), %rdi
	movl	20(%rax), %esi
	xorl	%eax, %eax
	andl	$16777215, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L2249
	.p2align 4,,10
	.p2align 3
.L2271:
	movq	%rdi, -2288(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-2288(%rbp), %rdi
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L2272
	call	*%rax
	movsbl	%al, %esi
	jmp	.L2272
	.p2align 4,,10
	.p2align 3
.L2168:
	movq	-2280(%rbp), %rax
	movb	$4, -2192(%rbp)
	movb	$2, -2188(%rbp)
	movl	20(%rax), %esi
	movq	32(%rax), %r15
	leaq	32(%rax), %r13
	movl	$0, -2184(%rbp)
	movb	$0, -2180(%rbp)
	movq	$0, -2176(%rbp)
	movl	%esi, %eax
	movl	$-1, -2168(%rbp)
	xorl	$251658240, %eax
	testl	$251658240, %eax
	jne	.L2170
	movq	16(%r15), %r15
.L2170:
	movl	20(%r15), %eax
	movl	%eax, %r8d
	andl	$16777215, %r8d
	movl	%r8d, %edx
	leaq	(%rdx,%rdx,4), %rcx
	movq	32(%r12), %rdx
	leaq	(%rdx,%rcx,8), %rbx
	cmpb	$4, 1(%rbx)
	je	.L2169
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	jne	.L2379
.L2178:
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rax
	movq	24(%rdx,%rax,8), %rcx
	testq	%rcx, %rcx
	jne	.L2179
	movq	8(%r15), %rcx
.L2179:
	movzbl	1(%rbx), %edx
	movq	128(%r12), %rdi
	pushq	-2168(%rbp)
	movq	%r15, %rsi
	movq	-2280(%rbp), %rbx
	pushq	-2176(%rbp)
	pushq	-2184(%rbp)
	pushq	-2192(%rbp)
	movq	%rbx, %r8
	call	_ZN2v88internal8compiler21RepresentationChanger20GetRepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE@PLT
	movl	20(%rbx), %esi
	addq	$32, %rsp
	movq	%rax, %r15
	movl	%esi, %eax
	xorl	$251658240, %eax
	testl	$251658240, %eax
	je	.L2180
	movq	32(%rbx), %rdi
	movq	%rbx, %rax
	cmpq	%rdi, %r15
	je	.L2169
.L2181:
	leaq	-24(%rax), %r14
	testq	%rdi, %rdi
	je	.L2182
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L2182:
	movq	%r15, 0(%r13)
	testq	%r15, %r15
	je	.L2352
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L2352:
	movq	-2280(%rbp), %rax
	movl	20(%rax), %esi
	jmp	.L2169
	.p2align 4,,10
	.p2align 3
.L2167:
	pushq	-2200(%rbp)
	movq	-2280(%rbp), %rbx
	xorl	%edx, %edx
	movq	%r12, %rdi
	pushq	-2208(%rbp)
	pushq	-2216(%rbp)
	movq	%rbx, %rsi
	pushq	-2224(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12EnqueueInputEPNS1_4NodeEiNS1_7UseInfoE
	movl	20(%rbx), %esi
	addq	$32, %rsp
	jmp	.L2169
	.p2align 4,,10
	.p2align 3
.L2208:
	movq	%rdi, -2328(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-2328(%rbp), %rdi
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L2209
	call	*%rax
	movsbl	%al, %esi
	jmp	.L2209
	.p2align 4,,10
	.p2align 3
.L2253:
	movq	%rdi, -2288(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-2288(%rbp), %rdi
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L2254
	call	*%rax
	movsbl	%al, %esi
	jmp	.L2254
	.p2align 4,,10
	.p2align 3
.L2368:
	leaq	.LC27(%rip), %rdi
	jmp	.L2354
.L2374:
	movq	-1456(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L2219
.L2379:
	movq	-2280(%rbp), %rcx
	movq	(%r15), %rax
	andl	$16777215, %esi
	leaq	.LC13(%rip), %rdi
	movq	(%rcx), %rdx
	movq	8(%rax), %r9
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	movq	8(%rdx), %rdx
	call	_ZN2v88internal6PrintFEPKcz@PLT
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	je	.L2351
	xorl	%eax, %eax
	leaq	.LC14(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	je	.L2351
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm4
	movq	%rdx, %xmm7
	leaq	-320(%rbp), %rax
	punpcklqdq	%xmm4, %xmm7
	movq	%rax, %rdi
	movq	%rax, -2288(%rbp)
	leaq	-400(%rbp), %r14
	movaps	%xmm7, -2304(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	pxor	%xmm3, %xmm3
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	stdout(%rip), %rdx
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rcx
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movups	%xmm3, -88(%rbp)
	movq	%rcx, -320(%rbp)
	movups	%xmm3, -72(%rbp)
	movw	%ax, -96(%rbp)
	movq	$0, -104(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rsi
	movq	%r14, %rdi
	movq	%rsi, -400(%rbp)
	addq	$40, %rsi
	movq	%rsi, -320(%rbp)
	movzbl	1(%rbx), %esi
	call	_ZN2v88internallsERSoNS0_21MachineRepresentationE@PLT
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rsi
	movdqa	-2304(%rbp), %xmm7
	movq	%rsi, -2352(%rbp)
	movq	%rsi, -320(%rbp)
	leaq	-336(%rbp), %rsi
	movq	%rsi, %rdi
	movq	%rsi, -2328(%rbp)
	movaps	%xmm7, -400(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	-2288(%rbp), %rdi
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rsi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rcx
	movq	%rsi, -2336(%rbp)
	movq	%rsi, -400(%rbp)
	movq	%rcx, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	je	.L2351
	xorl	%eax, %eax
	leaq	.LC15(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	je	.L2351
	movq	-2288(%rbp), %rdi
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	stdout(%rip), %rdx
	pxor	%xmm6, %xmm6
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movq	%rax, -320(%rbp)
	movq	%r14, %rdi
	xorl	%eax, %eax
	movups	%xmm6, -88(%rbp)
	movups	%xmm6, -72(%rbp)
	movw	%ax, -96(%rbp)
	movq	$0, -104(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movl	$4, %esi
	movq	%r14, %rdi
	movq	%rax, -400(%rbp)
	addq	$40, %rax
	movq	%rax, -320(%rbp)
	call	_ZN2v88internallsERSoNS0_21MachineRepresentationE@PLT
	movq	%r14, %rdi
	movl	$1, %edx
	leaq	.LC4(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	-2232(%rbp), %rdi
	movb	$2, -2232(%rbp)
	movl	$0, -2228(%rbp)
	call	_ZNK2v88internal8compiler10Truncation11descriptionEv@PLT
	testq	%rax, %rax
	je	.L2380
	movq	%rax, %rdi
	movq	%rax, -2360(%rbp)
	call	strlen@PLT
	movq	-2360(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L2177:
	movq	-2352(%rbp), %rax
	movdqa	-2304(%rbp), %xmm5
	movq	-2328(%rbp), %rdi
	movq	%rax, -320(%rbp)
	movaps	%xmm5, -400(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	-2336(%rbp), %rax
	movq	-2288(%rbp), %rdi
	movq	%rax, -400(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	je	.L2351
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
.L2351:
	movl	20(%r15), %eax
	movq	32(%r12), %rdx
	jmp	.L2178
	.p2align 4,,10
	.p2align 3
.L2247:
	leaq	-2264(%rbp), %rsi
	leaq	136(%r12), %rdi
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	jmp	.L2248
	.p2align 4,,10
	.p2align 3
.L2245:
	leaq	-2264(%rbp), %rdx
	leaq	56(%r12), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L2246
	.p2align 4,,10
	.p2align 3
.L2264:
	leaq	-2264(%rbp), %rsi
	leaq	136(%r12), %rdi
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	jmp	.L2265
	.p2align 4,,10
	.p2align 3
.L2375:
	movq	-400(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	movq	-2384(%rbp), %r11
	jmp	.L2230
	.p2align 4,,10
	.p2align 3
.L2376:
	movq	-1808(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L2206
	.p2align 4,,10
	.p2align 3
.L2377:
	movq	-2272(%rbp), %rax
	leaq	.LC24(%rip), %rdi
	movl	20(%rax), %esi
	xorl	%eax, %eax
	andl	$16777215, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L2195
	.p2align 4,,10
	.p2align 3
.L2220:
	movq	%r14, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%r14), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L2221
	movq	%r14, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L2221
	.p2align 4,,10
	.p2align 3
.L2372:
	movq	-1104(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r15, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L2252
.L2193:
	leaq	-2272(%rbp), %rsi
	leaq	136(%r13), %rdi
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	jmp	.L2194
.L2191:
	leaq	-2272(%rbp), %rdx
	leaq	56(%r13), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L2192
.L2180:
	movq	-2280(%rbp), %rax
	movq	32(%rax), %rax
	movq	16(%rax), %rdi
	leaq	16(%rax), %r13
	cmpq	%rdi, %r15
	jne	.L2181
	jmp	.L2169
.L2213:
	leaq	-2272(%rbp), %rsi
	leaq	136(%r13), %rdi
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	jmp	.L2214
.L2200:
	movq	%r14, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%r14), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L2201
	movq	%r14, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L2201
.L2380:
	movq	-400(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r14, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L2177
.L2378:
	movq	-2160(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L2198
.L2207:
	call	_ZSt16__throw_bad_castv@PLT
.L2373:
	call	__stack_chk_fail@PLT
.L2356:
	movq	32(%r13), %rdx
	movl	20(%r11), %eax
	jmp	.L2231
	.cfi_endproc
.LFE23017:
	.size	_ZN2v88internal8compiler22RepresentationSelector11VisitReturnEPNS1_4NodeE, .-_ZN2v88internal8compiler22RepresentationSelector11VisitReturnEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler22RepresentationSelector8VisitPhiEPNS1_4NodeENS1_10TruncationEPNS1_18SimplifiedLoweringE,"axG",@progbits,_ZN2v88internal8compiler22RepresentationSelector8VisitPhiEPNS1_4NodeENS1_10TruncationEPNS1_18SimplifiedLoweringE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler22RepresentationSelector8VisitPhiEPNS1_4NodeENS1_10TruncationEPNS1_18SimplifiedLoweringE
	.type	_ZN2v88internal8compiler22RepresentationSelector8VisitPhiEPNS1_4NodeENS1_10TruncationEPNS1_18SimplifiedLoweringE, @function
_ZN2v88internal8compiler22RepresentationSelector8VisitPhiEPNS1_4NodeENS1_10TruncationEPNS1_18SimplifiedLoweringE:
.LFB23030:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$136, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdx, %rax
	movb	%dl, -113(%rbp)
	shrq	$32, %rax
	movq	%rax, -112(%rbp)
	movl	20(%rsi), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rdx
	movq	32(%rdi), %rax
	leaq	(%rax,%rdx,8), %rdx
	movq	24(%rdx), %rax
	testq	%rax, %rax
	jne	.L2382
	movq	8(%rsi), %rax
.L2382:
	movq	%rax, -96(%rbp)
	movb	$0, -104(%rbp)
	cmpq	$1, %rax
	jne	.L2480
.L2383:
	movl	120(%r10), %eax
	testl	%eax, %eax
	je	.L2409
.L2490:
	cmpl	$1, %eax
	jne	.L2411
	movzbl	-104(%rbp), %eax
	movb	%al, 1(%rdx)
	movl	120(%r10), %eax
.L2411:
	movq	(%r12), %rdi
	movl	20(%rdi), %r13d
	cmpl	$2, %eax
	je	.L2481
.L2413:
	movl	20(%r12), %esi
	xorl	%ebx, %ebx
	movq	%r10, %r14
	.p2align 4,,10
	.p2align 3
.L2450:
	movl	%esi, %eax
	movl	%ebx, %ecx
	shrl	$24, %eax
	andl	$15, %eax
	movl	%eax, %edx
	cmpl	$15, %eax
	jne	.L2416
	movq	32(%r12), %rdx
	movl	8(%rdx), %edx
.L2416:
	cmpl	%ebx, %edx
	jle	.L2381
	movzbl	-113(%rbp), %r15d
	movzbl	-104(%rbp), %edi
	cmpl	%ebx, %r13d
	movl	$0, %edx
	movl	$0, %r8d
	cmovg	-112(%rbp), %r8d
	cmovle	%edx, %r15d
	cmovle	%edx, %edi
	movl	120(%r14), %edx
	testl	%edx, %edx
	je	.L2419
	cmpl	$2, %edx
	jne	.L2479
	leaq	32(%r12), %rdx
	leaq	0(,%rbx,8), %r11
	movb	%dil, -96(%rbp)
	movb	%r15b, -92(%rbp)
	movl	%r8d, -88(%rbp)
	movb	$0, -84(%rbp)
	movq	$0, -80(%rbp)
	movl	$-1, -72(%rbp)
	movq	%rdx, -160(%rbp)
	movq	%r11, -128(%rbp)
	cmpl	$15, %eax
	je	.L2438
	movq	%rdx, %rax
	addq	%r11, %rax
.L2439:
	leaq	1(%rbx), %rdx
	testb	%dil, %dil
	je	.L2421
	movq	(%rax), %r15
	movl	20(%r15), %r8d
	andl	$16777215, %r8d
	movl	%r8d, %eax
	leaq	(%rax,%rax,4), %r9
	movq	32(%r14), %rax
	leaq	(%rax,%r9,8), %rax
	movq	%rax, -152(%rbp)
	cmpb	1(%rax), %dil
	je	.L2421
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	jne	.L2482
.L2453:
	pushq	-72(%rbp)
	movq	%r14, %rdi
	pushq	-80(%rbp)
	pushq	-88(%rbp)
	pushq	-96(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12PrintUseInfoENS1_7UseInfoE
	addq	$32, %rsp
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	jne	.L2483
.L2444:
	movl	20(%r15), %eax
	movq	32(%r14), %rdx
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rax
	leaq	(%rdx,%rax,8), %rax
	movq	24(%rax), %rcx
	testq	%rcx, %rcx
	jne	.L2445
	movq	8(%r15), %rcx
.L2445:
	movq	-152(%rbp), %rax
	movq	128(%r14), %rdi
	movq	%r12, %r8
	movq	%r15, %rsi
	movzbl	1(%rax), %edx
	pushq	-72(%rbp)
	pushq	-80(%rbp)
	pushq	-88(%rbp)
	pushq	-96(%rbp)
	call	_ZN2v88internal8compiler21RepresentationChanger20GetRepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE@PLT
	movl	20(%r12), %esi
	addq	$32, %rsp
	movq	%rax, %r8
	movl	%esi, %eax
	xorl	$251658240, %eax
	testl	$251658240, %eax
	je	.L2446
	movq	32(%r12,%rbx,8), %rdi
	movq	-160(%rbp), %r15
	movq	%r12, %rax
	addq	-128(%rbp), %r15
	cmpq	%rdi, %r8
	je	.L2479
.L2447:
	leaq	1(%rbx), %rdx
	leaq	0(,%rdx,4), %rcx
	movq	%rdx, %rsi
	subq	%rcx, %rsi
	leaq	(%rax,%rsi,8), %rbx
	testq	%rdi, %rdi
	je	.L2448
	movq	%rbx, %rsi
	movq	%rdx, -152(%rbp)
	movq	%r8, -128(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-152(%rbp), %rdx
	movq	-128(%rbp), %r8
.L2448:
	movq	%r8, (%r15)
	testq	%r8, %r8
	je	.L2484
	movq	%rbx, %rsi
	movq	%r8, %rdi
	movq	%rdx, -128(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movl	20(%r12), %esi
	movq	-128(%rbp), %rdx
	.p2align 4,,10
	.p2align 3
.L2421:
	movq	%rdx, %rbx
	jmp	.L2450
	.p2align 4,,10
	.p2align 3
.L2479:
	leaq	1(%rbx), %rdx
	jmp	.L2421
	.p2align 4,,10
	.p2align 3
.L2419:
	leaq	32(%r12), %rdx
	leaq	0(,%rbx,8), %rcx
	addq	%rcx, %rdx
	cmpl	$15, %eax
	jne	.L2423
	movq	32(%r12), %rax
	leaq	16(%rax,%rcx), %rdx
.L2423:
	movq	(%rdx), %rax
	movq	%rax, -96(%rbp)
	movl	20(%rax), %esi
	andl	$16777215, %esi
	movl	%esi, %eax
	leaq	(%rax,%rax,4), %rdx
	movq	32(%r14), %rax
	leaq	(%rax,%rdx,8), %rcx
	cmpb	$0, (%rcx)
	je	.L2485
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	jne	.L2486
.L2430:
	movzbl	4(%rcx), %eax
	movl	%r8d, -152(%rbp)
	movq	%rcx, -128(%rbp)
	movb	%al, -136(%rbp)
	movl	8(%rcx), %eax
	movl	-136(%rbp), %edx
	salq	$32, %rax
	orq	%rax, %rdx
	movq	%rdx, %rdi
	movq	%rdx, -136(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector15PrintTruncationENS1_10TruncationE.isra.0
	movq	-128(%rbp), %rcx
	movl	-152(%rbp), %r8d
	movl	8(%rcx), %edx
	movzbl	4(%rcx), %eax
	movl	%r8d, %esi
	movq	%rcx, -168(%rbp)
	movl	%edx, %edi
	movl	%edx, -160(%rbp)
	movb	%al, -128(%rbp)
	call	_ZN2v88internal8compiler10Truncation23GeneralizeIdentifyZerosENS1_13IdentifyZerosES3_@PLT
	movl	%r15d, %esi
	movzbl	-128(%rbp), %r15d
	movl	%eax, -152(%rbp)
	movl	%r15d, %edi
	call	_ZN2v88internal8compiler10Truncation10GeneralizeENS2_14TruncationKindES3_@PLT
	movq	-168(%rbp), %rcx
	movl	-152(%rbp), %r8d
	movl	-160(%rbp), %edx
	movb	%al, 4(%rcx)
	movl	%r8d, 8(%rcx)
	cmpl	%r8d, %edx
	jne	.L2458
	cmpb	%al, %r15b
	je	.L2429
.L2458:
	cmpb	$3, (%rcx)
	jne	.L2487
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	jne	.L2488
.L2437:
	movb	%al, -176(%rbp)
	movl	-176(%rbp), %eax
	salq	$32, %r8
	orq	%r8, %rax
	movq	%rax, %rdi
	movq	%rax, -176(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector15PrintTruncationENS1_10TruncationE.isra.0
.L2429:
	movl	20(%r12), %esi
	leaq	1(%rbx), %rdx
	jmp	.L2421
	.p2align 4,,10
	.p2align 3
.L2381:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2489
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2438:
	.cfi_restore_state
	movq	32(%r12), %rax
	leaq	16(%rax,%r11), %rax
	jmp	.L2439
	.p2align 4,,10
	.p2align 3
.L2446:
	movq	32(%r12), %rax
	movq	-128(%rbp), %rcx
	leaq	1(%rbx), %rdx
	leaq	16(%rax,%rcx), %r15
	movq	(%r15), %rdi
	cmpq	%rdi, %r8
	jne	.L2447
	jmp	.L2421
	.p2align 4,,10
	.p2align 3
.L2487:
	movq	216(%r14), %rax
	movq	200(%r14), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L2434
	movq	-96(%rbp), %rax
	movq	%rax, (%rdx)
	addq	$8, 200(%r14)
.L2435:
	movb	$3, (%rcx)
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	jne	.L2436
.L2477:
	movzbl	4(%rcx), %eax
	movl	8(%rcx), %r8d
	jmp	.L2437
	.p2align 4,,10
	.p2align 3
.L2480:
	leaq	-96(%rbp), %r14
	movl	$1, %esi
	movq	%r10, -128(%rbp)
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	movq	-128(%rbp), %r10
	testb	%al, %al
	je	.L2384
	movl	20(%r12), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rdx
	movq	32(%r10), %rax
	leaq	(%rax,%rdx,8), %rdx
	movl	120(%r10), %eax
	testl	%eax, %eax
	jne	.L2490
.L2409:
	movl	$4294967295, %eax
	movq	%rax, 16(%rdx)
	movq	(%r12), %rdi
	movl	120(%r10), %eax
	movl	20(%rdi), %r13d
	cmpl	$2, %eax
	jne	.L2413
.L2481:
	movq	%r10, -128(%rbp)
	call	_ZN2v88internal8compiler19PhiRepresentationOfEPKNS1_8OperatorE@PLT
	movzbl	-104(%rbp), %esi
	movq	-128(%rbp), %r10
	cmpb	%sil, %al
	je	.L2413
	movq	(%rbx), %rax
	movl	%r13d, %edx
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	-128(%rbp), %r10
	jmp	.L2413
	.p2align 4,,10
	.p2align 3
.L2483:
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L2444
	.p2align 4,,10
	.p2align 3
.L2482:
	movq	(%r15), %rax
	movq	(%r12), %rdx
	andl	$16777215, %esi
	leaq	.LC13(%rip), %rdi
	movq	8(%rax), %r9
	movq	8(%rdx), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	je	.L2453
	xorl	%eax, %eax
	leaq	.LC14(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	je	.L2453
	movq	-152(%rbp), %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector15PrintOutputInfoEPNS2_8NodeInfoE.isra.0.part.0
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	je	.L2453
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L2453
	.p2align 4,,10
	.p2align 3
.L2484:
	movl	20(%r12), %esi
	jmp	.L2421
	.p2align 4,,10
	.p2align 3
.L2486:
	leaq	.LC25(%rip), %rdi
	xorl	%eax, %eax
	movl	%r8d, -152(%rbp)
	movq	%rcx, -128(%rbp)
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movl	-152(%rbp), %r8d
	movq	-128(%rbp), %rcx
	jmp	.L2430
	.p2align 4,,10
	.p2align 3
.L2485:
	movb	$3, (%rcx)
	movq	72(%r14), %rsi
	cmpq	80(%r14), %rsi
	je	.L2424
	movq	-96(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 72(%r14)
.L2425:
	movq	216(%r14), %rax
	movq	200(%r14), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L2426
	movq	-96(%rbp), %rax
	movq	%rax, (%rdx)
	addq	$8, 200(%r14)
.L2427:
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	jne	.L2491
.L2428:
	movzbl	4(%rcx), %r9d
	movl	8(%rcx), %edi
	movl	%r8d, %esi
	movq	%rcx, -152(%rbp)
	movb	%r9b, -160(%rbp)
	call	_ZN2v88internal8compiler10Truncation23GeneralizeIdentifyZerosENS1_13IdentifyZerosES3_@PLT
	movzbl	-160(%rbp), %r9d
	movl	%r15d, %esi
	movl	%eax, -128(%rbp)
	movl	%r9d, %edi
	call	_ZN2v88internal8compiler10Truncation10GeneralizeENS2_14TruncationKindES3_@PLT
	movq	-152(%rbp), %rcx
	movl	-128(%rbp), %edx
	movb	%al, -144(%rbp)
	movb	%al, 4(%rcx)
	movl	-144(%rbp), %eax
	movl	%edx, 8(%rcx)
	salq	$32, %rdx
	orq	%rdx, %rax
	movq	%rax, %rdi
	movq	%rax, -144(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector15PrintTruncationENS1_10TruncationE.isra.0
	movl	20(%r12), %esi
	leaq	1(%rbx), %rdx
	jmp	.L2421
	.p2align 4,,10
	.p2align 3
.L2384:
	cmpq	$1099, -96(%rbp)
	jne	.L2385
.L2390:
	movl	20(%r12), %eax
	movb	$4, -104(%rbp)
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rdx
	movq	32(%r10), %rax
	leaq	(%rax,%rdx,8), %rdx
	jmp	.L2383
	.p2align 4,,10
	.p2align 3
.L2385:
	movl	$1099, %esi
	movq	%r14, %rdi
	movq	%r10, -104(%rbp)
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	movq	-104(%rbp), %r10
	testb	%al, %al
	jne	.L2390
	cmpq	$1031, -96(%rbp)
	je	.L2390
	movl	$1031, %esi
	movq	%r14, %rdi
	movq	%r10, -104(%rbp)
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	movq	-104(%rbp), %r10
	testb	%al, %al
	jne	.L2390
	cmpq	$8396767, -96(%rbp)
	je	.L2392
	movl	$8396767, %esi
	movq	%r14, %rdi
	movq	%r10, -104(%rbp)
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	movq	-104(%rbp), %r10
	testb	%al, %al
	je	.L2391
.L2392:
	movl	$2, %esi
	movl	%r13d, %edi
	movq	%r10, -104(%rbp)
	call	_ZN2v88internal8compiler10Truncation11LessGeneralENS2_14TruncationKindES3_@PLT
	movq	-104(%rbp), %r10
	testb	%al, %al
	jne	.L2390
.L2391:
	cmpq	$513, -96(%rbp)
	je	.L2473
	movl	$513, %esi
	movq	%r14, %rdi
	movq	%r10, -104(%rbp)
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	movq	-104(%rbp), %r10
	testb	%al, %al
	jne	.L2473
	cmpq	$8396767, -96(%rbp)
	je	.L2398
	movl	$8396767, %esi
	movq	%r14, %rdi
	movq	%r10, -104(%rbp)
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	movq	-104(%rbp), %r10
	testb	%al, %al
	je	.L2397
.L2398:
	movl	$4, %esi
	movl	%r13d, %edi
	movq	%r10, -104(%rbp)
	call	_ZN2v88internal8compiler10Truncation11LessGeneralENS2_14TruncationKindES3_@PLT
	movq	-104(%rbp), %r10
	testb	%al, %al
	jne	.L2475
.L2397:
	movq	8(%r10), %r15
	movq	%r10, -104(%rbp)
	call	_ZN2v88internal8compiler10BitsetType11SignedSmallEv@PLT
	movl	$4097, %esi
	movl	%eax, %edi
	movq	%r15, %rdx
	orl	$1, %edi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	cmpq	-96(%rbp), %rax
	movq	-104(%rbp), %r10
	movq	%rax, %rsi
	jne	.L2399
	movl	20(%r12), %eax
	movb	$8, -104(%rbp)
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rdx
	movq	32(%r10), %rax
	leaq	(%rax,%rdx,8), %rdx
	jmp	.L2383
	.p2align 4,,10
	.p2align 3
.L2488:
	movq	%rcx, -128(%rbp)
	leaq	.LC27(%rip), %rdi
.L2478:
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-128(%rbp), %rcx
	jmp	.L2477
	.p2align 4,,10
	.p2align 3
.L2491:
	movq	-96(%rbp), %rax
	leaq	.LC24(%rip), %rdi
	movl	%r8d, -152(%rbp)
	movq	%rcx, -128(%rbp)
	movl	20(%rax), %esi
	xorl	%eax, %eax
	andl	$16777215, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movl	-152(%rbp), %r8d
	movq	-128(%rbp), %rcx
	jmp	.L2428
	.p2align 4,,10
	.p2align 3
.L2436:
	movq	%rcx, -128(%rbp)
	leaq	.LC26(%rip), %rdi
	jmp	.L2478
.L2426:
	leaq	-96(%rbp), %rsi
	leaq	136(%r14), %rdi
	movl	%r8d, -152(%rbp)
	movq	%rcx, -128(%rbp)
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	movl	-152(%rbp), %r8d
	movq	-128(%rbp), %rcx
	jmp	.L2427
.L2424:
	leaq	-96(%rbp), %rdx
	leaq	56(%r14), %rdi
	movl	%r8d, -152(%rbp)
	movq	%rcx, -128(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	movl	-152(%rbp), %r8d
	movq	-128(%rbp), %rcx
	jmp	.L2425
.L2434:
	leaq	-96(%rbp), %rsi
	leaq	136(%r14), %rdi
	movq	%rcx, -128(%rbp)
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	movq	-128(%rbp), %rcx
	jmp	.L2435
.L2473:
	movl	20(%r12), %eax
	movb	$1, -104(%rbp)
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rdx
	movq	32(%r10), %rax
	leaq	(%rax,%rdx,8), %rdx
	jmp	.L2383
.L2399:
	movq	%r14, %rdi
	movq	%r10, -104(%rbp)
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	movq	-104(%rbp), %r10
	testb	%al, %al
	je	.L2400
	movl	20(%r12), %edx
	movb	$8, -104(%rbp)
	andl	$16777215, %edx
	imulq	$40, %rdx, %rdx
	addq	32(%r10), %rdx
	jmp	.L2383
.L2475:
	movl	20(%r12), %edx
	movb	$13, -104(%rbp)
	andl	$16777215, %edx
	imulq	$40, %rdx, %rdx
	addq	32(%r10), %rdx
	jmp	.L2383
.L2400:
	cmpq	$7263, -96(%rbp)
	je	.L2475
	movl	$7263, %esi
	movq	%r14, %rdi
	movq	%r10, -104(%rbp)
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	movq	-104(%rbp), %r10
	testb	%al, %al
	jne	.L2475
	cmpq	$134217729, -96(%rbp)
	je	.L2406
	movl	$134217729, %esi
	movq	%r14, %rdi
	movq	%r10, -104(%rbp)
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	movq	-104(%rbp), %r10
	testb	%al, %al
	je	.L2405
.L2406:
	movl	$3, %esi
	movl	%r13d, %edi
	movq	%r10, -104(%rbp)
	call	_ZN2v88internal8compiler10Truncation11LessGeneralENS2_14TruncationKindES3_@PLT
	movq	-104(%rbp), %r10
	testb	%al, %al
	jne	.L2476
.L2405:
	cmpq	$33554433, -96(%rbp)
	je	.L2476
	movl	$33554433, %esi
	movq	%r14, %rdi
	movq	%r10, -104(%rbp)
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	movl	20(%r12), %edx
	movq	-104(%rbp), %r10
	andl	$16777215, %edx
	imulq	$40, %rdx, %rdx
	testb	%al, %al
	jne	.L2408
	movb	$8, -104(%rbp)
	addq	32(%r10), %rdx
	jmp	.L2383
.L2476:
	movl	20(%r12), %edx
	movb	$5, -104(%rbp)
	andl	$16777215, %edx
	imulq	$40, %rdx, %rdx
	addq	32(%r10), %rdx
	jmp	.L2383
.L2489:
	call	__stack_chk_fail@PLT
.L2408:
	movb	$5, -104(%rbp)
	addq	32(%r10), %rdx
	jmp	.L2383
	.cfi_endproc
.LFE23030:
	.size	_ZN2v88internal8compiler22RepresentationSelector8VisitPhiEPNS1_4NodeENS1_10TruncationEPNS1_18SimplifiedLoweringE, .-_ZN2v88internal8compiler22RepresentationSelector8VisitPhiEPNS1_4NodeENS1_10TruncationEPNS1_18SimplifiedLoweringE
	.section	.text._ZN2v88internal8compiler22RepresentationSelector16VisitStateValuesEPNS1_4NodeE,"axG",@progbits,_ZN2v88internal8compiler22RepresentationSelector16VisitStateValuesEPNS1_4NodeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler22RepresentationSelector16VisitStateValuesEPNS1_4NodeE
	.type	_ZN2v88internal8compiler22RepresentationSelector16VisitStateValuesEPNS1_4NodeE, @function
_ZN2v88internal8compiler22RepresentationSelector16VisitStateValuesEPNS1_4NodeE:
.LFB23037:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$1224, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	120(%rdi), %edx
	movl	20(%rsi), %edi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	jne	.L2493
	leaq	32(%r15), %rax
	xorl	%r12d, %r12d
	movq	32(%r13), %rsi
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rax, -1192(%rbp)
	movq	%rax, %rbx
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%r12, %r14
	movq	%rax, %xmm3
	movl	%edi, %eax
	movq	%rdx, %xmm1
	movl	%r14d, %r12d
	shrl	$24, %eax
	punpcklqdq	%xmm3, %xmm1
	movq	%r15, -1176(%rbp)
	andl	$15, %eax
	movaps	%xmm1, -1216(%rbp)
	cmpl	$15, %eax
	je	.L2494
	.p2align 4,,10
	.p2align 3
.L2666:
	cmpl	%r14d, %eax
	jle	.L2661
	movq	%rbx, %rdx
.L2498:
	movq	(%rdx), %r8
	movl	20(%r8), %edx
	andl	$16777215, %edx
	leaq	(%rdx,%rdx,4), %rdx
	movq	24(%rsi,%rdx,8), %rdx
	testq	%rdx, %rdx
	jne	.L2499
	movq	8(%r8), %rdx
.L2499:
	movq	%rdx, -1144(%rbp)
	cmpq	$134217729, %rdx
	je	.L2501
	leaq	-1144(%rbp), %rdi
	movl	$134217729, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L2502
	movq	-1176(%rbp), %rax
	movq	32(%r13), %rsi
	movl	20(%rax), %edi
	movl	%edi, %eax
	shrl	$24, %eax
	andl	$15, %eax
.L2501:
	leaq	0(,%r14,8), %rdx
	cmpl	$15, %eax
	je	.L2504
	movq	%rbx, %rax
.L2505:
	movq	(%rax), %rax
	movl	120(%r13), %edx
	movq	%rax, -1168(%rbp)
	testl	%edx, %edx
	jne	.L2503
	movl	20(%rax), %r9d
	andl	$16777215, %r9d
	movl	%r9d, %eax
	leaq	(%rax,%rax,4), %rax
	leaq	(%rsi,%rax,8), %r15
	cmpb	$0, (%r15)
	je	.L2662
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	jne	.L2663
.L2657:
	movl	8(%r15), %r9d
.L2520:
	movzbl	4(%r15), %r12d
	movl	%r9d, %edi
	movl	$1, %esi
	movl	%r9d, -1200(%rbp)
	call	_ZN2v88internal8compiler10Truncation23GeneralizeIdentifyZerosENS1_13IdentifyZerosES3_@PLT
	movl	$5, %esi
	movl	%r12d, %edi
	movl	%eax, -1184(%rbp)
	call	_ZN2v88internal8compiler10Truncation10GeneralizeENS2_14TruncationKindES3_@PLT
	movl	-1184(%rbp), %edx
	movb	%al, 4(%r15)
	movl	%edx, 8(%r15)
	cmpb	%al, %r12b
	jne	.L2603
	movl	-1200(%rbp), %r9d
	cmpl	%r9d, %edx
	je	.L2526
.L2603:
	cmpb	$3, (%r15)
	jne	.L2664
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	jne	.L2665
.L2526:
	movq	-1176(%rbp), %rax
	movq	32(%r13), %rsi
	movl	20(%rax), %edi
.L2503:
	addq	$1, %r14
	addq	$8, %rbx
.L2667:
	movl	%edi, %eax
	movl	%r14d, %r12d
	shrl	$24, %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2666
.L2494:
	movq	-1192(%rbp), %rcx
	movq	(%rcx), %rdx
	cmpl	%r14d, 8(%rdx)
	jle	.L2661
	leaq	16(%rdx,%r14,8), %rdx
	jmp	.L2498
	.p2align 4,,10
	.p2align 3
.L2504:
	movq	-1192(%rbp), %rax
	movq	(%rax), %rax
	leaq	16(%rax,%rdx), %rax
	jmp	.L2505
	.p2align 4,,10
	.p2align 3
.L2502:
	movq	-1176(%rbp), %r15
	movq	%r13, %rdi
	movb	$0, -1136(%rbp)
	movl	%r12d, %edx
	movb	$5, -1132(%rbp)
	addq	$1, %r14
	addq	$8, %rbx
	movl	$1, -1128(%rbp)
	movq	%r15, %rsi
	movb	$0, -1124(%rbp)
	movq	$0, -1120(%rbp)
	movl	$-1, -1112(%rbp)
	pushq	-1112(%rbp)
	pushq	-1120(%rbp)
	pushq	-1128(%rbp)
	pushq	-1136(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12EnqueueInputEPNS1_4NodeEiNS1_7UseInfoE
	movl	20(%r15), %edi
	movq	32(%r13), %rsi
	addq	$32, %rsp
	jmp	.L2667
	.p2align 4,,10
	.p2align 3
.L2663:
	xorl	%eax, %eax
	movl	%r9d, %esi
	leaq	.LC25(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movzbl	4(%r15), %eax
	movl	8(%r15), %r9d
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	movb	%al, -1152(%rbp)
	movl	%r9d, -1148(%rbp)
	je	.L2520
	leaq	-672(%rbp), %rax
	leaq	-752(%rbp), %r12
	movq	%rax, %rdi
	movq	%rax, -1184(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movq	stdout(%rip), %rdx
	movq	%rax, -672(%rbp)
	xorl	%eax, %eax
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movw	%ax, -448(%rbp)
	movq	$0, -456(%rbp)
	movups	%xmm0, -440(%rbp)
	movups	%xmm0, -424(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	leaq	-1152(%rbp), %rdi
	movq	%rax, -752(%rbp)
	addq	$40, %rax
	movq	%rax, -672(%rbp)
	call	_ZNK2v88internal8compiler10Truncation11descriptionEv@PLT
	testq	%rax, %rax
	je	.L2668
	movq	%rax, %rdi
	movq	%rax, -1200(%rbp)
	call	strlen@PLT
	movq	-1200(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L2522:
	movq	-752(%rbp), %rax
	movq	-24(%rax), %rax
	movq	-512(%rbp,%rax), %rdi
	testq	%rdi, %rdi
	je	.L2523
	cmpb	$0, 56(%rdi)
	je	.L2524
	movsbl	67(%rdi), %esi
.L2525:
	movq	%r12, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movdqa	-1216(%rbp), %xmm2
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	-688(%rbp), %rdi
	movq	%rax, -672(%rbp)
	movaps	%xmm2, -752(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	-1184(%rbp), %rdi
	movq	%rax, -752(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -672(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L2657
	.p2align 4,,10
	.p2align 3
.L2493:
	cmpl	$2, %edx
	je	.L2540
	movl	%edi, %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rcx
	movq	32(%r13), %rax
	leaq	(%rax,%rcx,8), %rax
.L2541:
	cmpl	$1, %edx
	jne	.L2492
	movb	$8, 1(%rax)
.L2492:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2669
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2664:
	.cfi_restore_state
	movq	216(%r13), %rax
	movq	200(%r13), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L2529
	movq	-1168(%rbp), %rax
	movq	%rax, (%rdx)
	addq	$8, 200(%r13)
.L2530:
	movb	$3, (%r15)
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	leaq	.LC26(%rip), %rdi
	je	.L2526
.L2658:
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movzbl	4(%r15), %esi
	movl	8(%r15), %edx
	movzbl	_ZN2v88internal25FLAG_trace_representationE(%rip), %eax
	movb	%sil, -1144(%rbp)
	movl	%edx, -1140(%rbp)
	testb	%al, %al
	je	.L2526
	leaq	-320(%rbp), %r15
	leaq	-400(%rbp), %r12
	movq	%r15, %rdi
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movq	stdout(%rip), %rdx
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movw	%ax, -96(%rbp)
	movq	$0, -104(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	leaq	-1144(%rbp), %rdi
	movq	%rax, -400(%rbp)
	addq	$40, %rax
	movq	%rax, -320(%rbp)
	call	_ZNK2v88internal8compiler10Truncation11descriptionEv@PLT
	testq	%rax, %rax
	je	.L2670
	movq	%rax, %rdi
	movq	%rax, -1184(%rbp)
	call	strlen@PLT
	movq	-1184(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L2535:
	movq	-400(%rbp), %rax
	movq	-24(%rax), %rax
	movq	-160(%rbp,%rax), %rdi
	testq	%rdi, %rdi
	je	.L2523
	cmpb	$0, 56(%rdi)
	je	.L2536
	movsbl	67(%rdi), %esi
.L2537:
	movq	%r12, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movdqa	-1216(%rbp), %xmm7
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	-336(%rbp), %rdi
	movq	%rax, -320(%rbp)
	movaps	%xmm7, -400(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%r15, %rdi
	movq	%rax, -400(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-1176(%rbp), %rax
	movq	32(%r13), %rsi
	movl	20(%rax), %edi
	jmp	.L2503
	.p2align 4,,10
	.p2align 3
.L2661:
	movl	120(%r13), %edx
.L2496:
	movl	%edi, %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rax
	leaq	(%rsi,%rax,8), %rax
	testl	%edx, %edx
	jne	.L2541
	movl	$4294967295, %ebx
	movq	%rbx, 16(%rax)
	jmp	.L2492
	.p2align 4,,10
	.p2align 3
.L2662:
	movb	$3, (%r15)
	movq	72(%r13), %rsi
	cmpq	80(%r13), %rsi
	je	.L2508
	movq	-1168(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 72(%r13)
.L2509:
	movq	216(%r13), %rax
	movq	200(%r13), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L2510
	movq	-1168(%rbp), %rax
	movq	%rax, (%rdx)
	addq	$8, 200(%r13)
.L2511:
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	jne	.L2671
.L2512:
	movzbl	4(%r15), %r9d
	movl	8(%r15), %edi
	movl	$1, %esi
	movb	%r9b, -1184(%rbp)
	call	_ZN2v88internal8compiler10Truncation23GeneralizeIdentifyZerosENS1_13IdentifyZerosES3_@PLT
	movzbl	-1184(%rbp), %r9d
	movl	$5, %esi
	movl	%eax, %r12d
	movl	%r9d, %edi
	call	_ZN2v88internal8compiler10Truncation10GeneralizeENS2_14TruncationKindES3_@PLT
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	movl	%r12d, 8(%r15)
	movb	%al, 4(%r15)
	movb	%al, -1160(%rbp)
	movl	%r12d, -1156(%rbp)
	je	.L2526
	leaq	-1024(%rbp), %r15
	leaq	-1104(%rbp), %r12
	movq	%r15, %rdi
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movq	stdout(%rip), %rdx
	movq	%rax, -1024(%rbp)
	xorl	%eax, %eax
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movw	%ax, -800(%rbp)
	movq	$0, -808(%rbp)
	movups	%xmm0, -792(%rbp)
	movups	%xmm0, -776(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	leaq	-1160(%rbp), %rdi
	movq	%rax, -1104(%rbp)
	addq	$40, %rax
	movq	%rax, -1024(%rbp)
	call	_ZNK2v88internal8compiler10Truncation11descriptionEv@PLT
	testq	%rax, %rax
	je	.L2672
	movq	%rax, %rdi
	movq	%rax, -1184(%rbp)
	call	strlen@PLT
	movq	-1184(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L2515:
	movq	-1104(%rbp), %rax
	movq	-24(%rax), %rax
	movq	-864(%rbp,%rax), %rdi
	testq	%rdi, %rdi
	je	.L2523
	cmpb	$0, 56(%rdi)
	je	.L2517
	movsbl	67(%rdi), %esi
.L2518:
	movq	%r12, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movdqa	-1216(%rbp), %xmm3
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	-1040(%rbp), %rdi
	movq	%rax, -1024(%rbp)
	movaps	%xmm3, -1104(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%r15, %rdi
	movq	%rax, -1104(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -1024(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-1176(%rbp), %rax
	movq	32(%r13), %rsi
	movl	20(%rax), %edi
	jmp	.L2503
	.p2align 4,,10
	.p2align 3
.L2524:
	movq	%rdi, -1200(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-1200(%rbp), %rdi
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L2525
	call	*%rax
	movsbl	%al, %esi
	jmp	.L2525
	.p2align 4,,10
	.p2align 3
.L2540:
	movq	0(%r13), %rax
	movq	(%rax), %rax
	movq	(%rax), %r12
	movl	%edi, %eax
	shrl	$24, %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2543
	movq	32(%rsi), %rax
	movl	8(%rax), %eax
.L2543:
	movslq	%eax, %rbx
	movq	16(%r12), %rax
	movq	%rax, %rcx
	movq	%rax, -1184(%rbp)
	movq	24(%r12), %rax
	subq	%rcx, %rax
	cmpq	$31, %rax
	jbe	.L2673
	movq	%rcx, %rax
	addq	$32, %rax
	movq	%rax, 16(%r12)
.L2545:
	cmpq	$1073741823, %rbx
	ja	.L2674
	movq	-1184(%rbp), %rax
	leaq	(%rbx,%rbx), %r14
	xorl	%esi, %esi
	movq	%r12, (%rax)
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	testq	%rbx, %rbx
	je	.L2594
	movq	16(%r12), %rax
	movq	24(%r12), %rdx
	leaq	7(%r14), %rsi
	andq	$-8, %rsi
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L2675
	addq	%rax, %rsi
	movq	%rsi, 16(%r12)
.L2549:
	movq	-1184(%rbp), %rdi
	leaq	(%rax,%r14), %rsi
	leaq	-1(%rbx), %rdx
	movq	%rax, %rcx
	movq	%rax, 8(%rdi)
	movq	%rsi, 24(%rdi)
	cmpq	$6, %rdx
	jbe	.L2599
	movq	%rbx, %rdx
	pxor	%xmm0, %xmm0
	shrq	$3, %rdx
	salq	$4, %rdx
	addq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L2551:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L2551
	movq	%rbx, %rdx
	movq	%rbx, %rax
	andq	$-8, %rdx
	andl	$7, %eax
	leaq	(%rcx,%rdx,2), %rcx
	cmpq	%rdx, %rbx
	je	.L2594
.L2550:
	xorl	%r12d, %r12d
	movw	%r12w, (%rcx)
	cmpq	$1, %rax
	je	.L2594
	xorl	%ebx, %ebx
	movw	%bx, 2(%rcx)
	cmpq	$2, %rax
	je	.L2594
	xorl	%r11d, %r11d
	movw	%r11w, 4(%rcx)
	cmpq	$3, %rax
	je	.L2594
	xorl	%r10d, %r10d
	movw	%r10w, 6(%rcx)
	cmpq	$4, %rax
	je	.L2594
	xorl	%r9d, %r9d
	movw	%r9w, 8(%rcx)
	cmpq	$5, %rax
	je	.L2594
	xorl	%r8d, %r8d
	movw	%r8w, 10(%rcx)
	cmpq	$6, %rax
	je	.L2594
	xorl	%edi, %edi
	movw	%di, 12(%rcx)
.L2594:
	movq	-1184(%rbp), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	xorl	%r12d, %r12d
	movq	%rdx, %xmm4
	movq	%rsi, 16(%rax)
	leaq	32(%r15), %rax
	movq	%rax, -1200(%rbp)
	movq	%rax, -1176(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm7
	punpcklqdq	%xmm7, %xmm4
	movaps	%xmm4, -1232(%rbp)
	jmp	.L2590
	.p2align 4,,10
	.p2align 3
.L2679:
	cmpl	%r12d, %eax
	jle	.L2555
	movq	-1176(%rbp), %rdx
.L2557:
	movq	(%rdx), %rbx
	movq	32(%r13), %r9
	movl	20(%rbx), %edx
	movq	%r9, %rdi
	andl	$16777215, %edx
	leaq	(%rdx,%rdx,4), %rdx
	movq	24(%r9,%rdx,8), %rdx
	testq	%rdx, %rdx
	jne	.L2558
	movq	8(%rbx), %rdx
.L2558:
	movq	%rdx, -1136(%rbp)
	cmpq	$134217729, %rdx
	je	.L2561
	leaq	-1136(%rbp), %rdi
	movl	$134217729, %esi
	movl	%ecx, -1192(%rbp)
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L2576
	movl	20(%r15), %esi
	movq	32(%r13), %r9
	movl	-1192(%rbp), %ecx
	movl	%esi, %eax
	movq	%r9, %rdi
	shrl	$24, %eax
	andl	$15, %eax
.L2561:
	leaq	0(,%r12,8), %rdx
	movb	$8, -1136(%rbp)
	movb	$5, -1132(%rbp)
	movl	$1, -1128(%rbp)
	movb	$0, -1124(%rbp)
	movq	$0, -1120(%rbp)
	movl	$-1, -1112(%rbp)
	movq	%rdx, -1192(%rbp)
	cmpl	$15, %eax
	je	.L2563
	movq	-1176(%rbp), %rax
.L2564:
	movq	(%rax), %r11
	movl	20(%r11), %eax
	movl	%eax, %r8d
	andl	$16777215, %r8d
	movl	%r8d, %edx
	leaq	(%rdx,%rdx,4), %rdx
	leaq	(%r9,%rdx,8), %r14
	leaq	1(%r12), %rdx
	cmpb	$8, 1(%r14)
	je	.L2562
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	jne	.L2676
.L2572:
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rax
	movq	24(%r9,%rax,8), %rcx
	testq	%rcx, %rcx
	jne	.L2573
	movq	8(%r11), %rcx
.L2573:
	movzbl	1(%r14), %edx
	pushq	-1112(%rbp)
	movq	%r15, %r8
	movq	%r11, %rsi
	movq	128(%r13), %rdi
	pushq	-1120(%rbp)
	pushq	-1128(%rbp)
	pushq	-1136(%rbp)
	call	_ZN2v88internal8compiler21RepresentationChanger20GetRepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE@PLT
	addq	$32, %rsp
	movq	%rax, %r14
	movzbl	23(%r15), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L2574
	movq	-1176(%rbp), %rcx
	movq	(%rcx), %rdi
	movq	%rcx, %rax
	movq	%r15, %rcx
	cmpq	%r14, %rdi
	je	.L2576
.L2575:
	leaq	1(%r12), %rdx
	leaq	0(,%rdx,4), %rsi
	movq	%rdx, %r10
	subq	%rsi, %r10
	leaq	(%rcx,%r10,8), %rsi
	testq	%rdi, %rdi
	je	.L2577
	movq	%rdx, -1240(%rbp)
	movq	%rax, -1216(%rbp)
	movq	%rsi, -1192(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-1240(%rbp), %rdx
	movq	-1216(%rbp), %rax
	movq	-1192(%rbp), %rsi
.L2577:
	movq	%r14, (%rax)
	testq	%r14, %r14
	je	.L2677
	movq	%r14, %rdi
	movq	%rdx, -1192(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movq	32(%r13), %rdi
	movq	-1192(%rbp), %rdx
	.p2align 4,,10
	.p2align 3
.L2562:
	movl	20(%rbx), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rax
	leaq	(%rdi,%rax,8), %rcx
	movq	24(%rcx), %rax
	testq	%rax, %rax
	jne	.L2579
	movq	8(%rbx), %rax
.L2579:
	movq	-1184(%rbp), %rbx
	movzbl	1(%rcx), %r14d
	movq	8(%rbx), %rcx
	movq	%rax, -1144(%rbp)
	leaq	(%rcx,%r12,2), %r12
	cmpq	$1, %rax
	je	.L2601
	leal	-6(%r14), %ecx
	cmpb	$2, %cl
	ja	.L2678
.L2581:
	movl	$7, %eax
	movl	$8, %r14d
.L2580:
	addq	$8, -1176(%rbp)
	movb	%r14b, (%r12)
	movb	%al, 1(%r12)
	movq	%rdx, %r12
.L2590:
	movl	20(%r15), %esi
	movl	%r12d, %ecx
	movl	%esi, %eax
	shrl	$24, %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2679
	movq	-1200(%rbp), %rbx
	movq	(%rbx), %rdx
	cmpl	%r12d, 8(%rdx)
	jle	.L2555
	leaq	16(%rdx,%r12,8), %rdx
	jmp	.L2557
	.p2align 4,,10
	.p2align 3
.L2668:
	movq	-752(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L2522
	.p2align 4,,10
	.p2align 3
.L2665:
	leaq	.LC27(%rip), %rdi
	jmp	.L2658
	.p2align 4,,10
	.p2align 3
.L2670:
	movq	-400(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L2535
	.p2align 4,,10
	.p2align 3
.L2574:
	movq	-1200(%rbp), %rax
	movq	(%rax), %rcx
	leaq	0(,%r12,8), %rax
	leaq	16(%rcx,%rax), %rax
	movq	(%rax), %rdi
	cmpq	%r14, %rdi
	jne	.L2575
	.p2align 4,,10
	.p2align 3
.L2576:
	movq	32(%r13), %rdi
	leaq	1(%r12), %rdx
	jmp	.L2562
	.p2align 4,,10
	.p2align 3
.L2601:
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	jmp	.L2580
	.p2align 4,,10
	.p2align 3
.L2563:
	movq	-1200(%rbp), %rax
	leaq	0(,%r12,8), %rdx
	movq	(%rax), %rax
	leaq	16(%rax,%rdx), %rax
	jmp	.L2564
	.p2align 4,,10
	.p2align 3
.L2678:
	leal	-9(%r14), %ecx
	cmpb	$2, %cl
	ja	.L2680
	movl	$7, %eax
	movl	$11, %r14d
	jmp	.L2580
	.p2align 4,,10
	.p2align 3
.L2676:
	movq	(%r11), %rax
	movq	(%r15), %rdx
	andl	$16777215, %esi
	leaq	.LC13(%rip), %rdi
	movq	%r11, -1192(%rbp)
	movq	8(%rax), %r9
	movq	8(%rdx), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	movq	-1192(%rbp), %r11
	je	.L2660
	xorl	%eax, %eax
	leaq	.LC14(%rip), %rdi
	movq	%r11, -1192(%rbp)
	call	_ZN2v88internal6PrintFEPKcz@PLT
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	movq	-1192(%rbp), %r11
	je	.L2660
	leaq	-320(%rbp), %rcx
	leaq	-400(%rbp), %rax
	movq	%r11, -1248(%rbp)
	movq	%rcx, %rdi
	movq	%rcx, -1216(%rbp)
	movq	%rax, -1192(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	xorl	%edx, %edx
	movq	-1192(%rbp), %rdi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rsi
	movw	%dx, -96(%rbp)
	movq	stdout(%rip), %rdx
	pxor	%xmm5, %xmm5
	movq	%rsi, -320(%rbp)
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movups	%xmm5, -88(%rbp)
	movups	%xmm5, -72(%rbp)
	movq	$0, -104(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rdx
	movq	-1192(%rbp), %rdi
	movq	%rdx, -400(%rbp)
	addq	$40, %rdx
	movq	%rdx, -320(%rbp)
	movzbl	1(%r14), %esi
	call	_ZN2v88internallsERSoNS0_21MachineRepresentationE@PLT
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	movdqa	-1232(%rbp), %xmm7
	movq	%rax, -320(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -1240(%rbp)
	movaps	%xmm7, -400(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	-1216(%rbp), %rdi
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rsi
	movq	%rax, -400(%rbp)
	movq	%rsi, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	movq	-1248(%rbp), %r11
	je	.L2660
	xorl	%eax, %eax
	leaq	.LC15(%rip), %rdi
	movq	%r11, -1248(%rbp)
	call	_ZN2v88internal6PrintFEPKcz@PLT
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	movq	-1248(%rbp), %r11
	je	.L2660
	movq	-1216(%rbp), %rdi
	movq	%r11, -1248(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	stdout(%rip), %rdx
	movq	-1192(%rbp), %rdi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	pxor	%xmm6, %xmm6
	movq	%rax, -320(%rbp)
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	xorl	%eax, %eax
	movups	%xmm6, -88(%rbp)
	movups	%xmm6, -72(%rbp)
	movw	%ax, -96(%rbp)
	movq	$0, -104(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	movq	-1192(%rbp), %rdi
	movl	$8, %esi
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movq	%rax, -400(%rbp)
	addq	$40, %rax
	movq	%rax, -320(%rbp)
	call	_ZN2v88internallsERSoNS0_21MachineRepresentationE@PLT
	movq	-1192(%rbp), %rdi
	movl	$1, %edx
	leaq	.LC4(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	-1144(%rbp), %rdi
	movb	$5, -1144(%rbp)
	movl	$1, -1140(%rbp)
	call	_ZNK2v88internal8compiler10Truncation11descriptionEv@PLT
	movq	-1248(%rbp), %r11
	testq	%rax, %rax
	je	.L2681
	movq	%rax, %rdi
	movq	%r11, -1256(%rbp)
	movq	%rax, -1248(%rbp)
	call	strlen@PLT
	movq	-1248(%rbp), %rsi
	movq	-1192(%rbp), %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1256(%rbp), %r11
.L2571:
	movdqa	-1232(%rbp), %xmm4
	movq	-1240(%rbp), %rdi
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	movq	%r11, -1192(%rbp)
	movq	%rax, -320(%rbp)
	movaps	%xmm4, -400(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	-1216(%rbp), %rdi
	movq	%rax, -400(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	movq	-1192(%rbp), %r11
	je	.L2660
	xorl	%eax, %eax
	leaq	.LC16(%rip), %rdi
	movq	%r11, -1192(%rbp)
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-1192(%rbp), %r11
	movq	32(%r13), %r9
	movl	20(%r11), %eax
	jmp	.L2572
	.p2align 4,,10
	.p2align 3
.L2677:
	movq	32(%r13), %rdi
	jmp	.L2562
	.p2align 4,,10
	.p2align 3
.L2555:
	movq	(%r15), %rdi
	call	_ZN2v88internal8compiler17SparseInputMaskOfEPKNS1_8OperatorE@PLT
	movq	-1184(%rbp), %rsi
	movl	%eax, %edx
	movq	0(%r13), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder16TypedStateValuesEPKNS0_10ZoneVectorINS0_11MachineTypeEEENS1_15SparseInputMaskE@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movl	20(%r15), %edi
	movl	120(%r13), %edx
	movq	32(%r13), %rsi
	jmp	.L2496
	.p2align 4,,10
	.p2align 3
.L2680:
	cmpb	$5, %r14b
	jne	.L2582
	cmpq	$134217729, %rax
	je	.L2581
	leaq	-1144(%rbp), %rdi
	movl	$134217729, %esi
	movq	%rdx, -1192(%rbp)
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	movq	-1192(%rbp), %rdx
	testb	%al, %al
	jne	.L2581
	movl	$4, %eax
	jmp	.L2580
	.p2align 4,,10
	.p2align 3
.L2671:
	movq	-1168(%rbp), %rax
	leaq	.LC24(%rip), %rdi
	movl	20(%rax), %esi
	xorl	%eax, %eax
	andl	$16777215, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L2512
	.p2align 4,,10
	.p2align 3
.L2536:
	movq	%rdi, -1184(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-1184(%rbp), %rdi
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L2537
	call	*%rax
	movsbl	%al, %esi
	jmp	.L2537
	.p2align 4,,10
	.p2align 3
.L2510:
	leaq	-1168(%rbp), %rsi
	leaq	136(%r13), %rdi
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	jmp	.L2511
	.p2align 4,,10
	.p2align 3
.L2508:
	leaq	-1168(%rbp), %rdx
	leaq	56(%r13), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L2509
	.p2align 4,,10
	.p2align 3
.L2529:
	leaq	-1168(%rbp), %rsi
	leaq	136(%r13), %rdi
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	jmp	.L2530
	.p2align 4,,10
	.p2align 3
.L2582:
	movq	%rax, -1136(%rbp)
	cmpq	$1099, %rax
	jne	.L2585
.L2587:
	movl	$2, %eax
	jmp	.L2580
	.p2align 4,,10
	.p2align 3
.L2517:
	movq	%rdi, -1184(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-1184(%rbp), %rdi
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L2518
	call	*%rax
	movsbl	%al, %esi
	jmp	.L2518
	.p2align 4,,10
	.p2align 3
.L2681:
	movq	-400(%rbp), %rax
	movq	-1192(%rbp), %rdi
	addq	-24(%rax), %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	movq	-1248(%rbp), %r11
	jmp	.L2571
.L2585:
	leaq	-1136(%rbp), %rdi
	movl	$1099, %esi
	movq	%rdx, -1192(%rbp)
	movq	%rdi, -1216(%rbp)
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	movq	-1192(%rbp), %rdx
	testb	%al, %al
	jne	.L2587
	cmpq	$1031, -1136(%rbp)
	movq	-1216(%rbp), %rdi
	je	.L2589
	movl	$1031, %esi
	movq	%rdx, -1192(%rbp)
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	movq	-1192(%rbp), %rdx
	testb	%al, %al
	jne	.L2589
	movl	$7, %eax
	jmp	.L2580
	.p2align 4,,10
	.p2align 3
.L2672:
	movq	-1104(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L2515
.L2673:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, -1184(%rbp)
	jmp	.L2545
.L2599:
	movq	%rbx, %rax
	jmp	.L2550
.L2589:
	movl	$3, %eax
	jmp	.L2580
.L2675:
	movq	%r12, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L2549
.L2669:
	call	__stack_chk_fail@PLT
.L2523:
	call	_ZSt16__throw_bad_castv@PLT
.L2660:
	movq	32(%r13), %r9
	movl	20(%r11), %eax
	jmp	.L2572
.L2674:
	leaq	.LC29(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE23037:
	.size	_ZN2v88internal8compiler22RepresentationSelector16VisitStateValuesEPNS1_4NodeE, .-_ZN2v88internal8compiler22RepresentationSelector16VisitStateValuesEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler22RepresentationSelector9VisitCallEPNS1_4NodeEPNS1_18SimplifiedLoweringE,"axG",@progbits,_ZN2v88internal8compiler22RepresentationSelector9VisitCallEPNS1_4NodeEPNS1_18SimplifiedLoweringE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler22RepresentationSelector9VisitCallEPNS1_4NodeEPNS1_18SimplifiedLoweringE
	.type	_ZN2v88internal8compiler22RepresentationSelector9VisitCallEPNS1_4NodeEPNS1_18SimplifiedLoweringE, @function
_ZN2v88internal8compiler22RepresentationSelector9VisitCallEPNS1_4NodeEPNS1_18SimplifiedLoweringE:
.LFB23033:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$2344, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler16CallDescriptorOfEPKNS1_8OperatorE@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rax, -2280(%rbp)
	movq	24(%rax), %rax
	movq	%rdx, %xmm1
	movl	8(%rax), %eax
	movl	%eax, -2268(%rbp)
	movq	(%rbx), %rax
	movl	20(%rax), %ecx
	leal	-1(%rcx), %eax
	movl	%ecx, -2284(%rbp)
	movq	%rax, -2264(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm1
	movaps	%xmm1, -2304(%rbp)
	testl	%ecx, %ecx
	jle	.L2800
	movl	%r13d, %ecx
	testq	%r13, %r13
	je	.L2905
	.p2align 4,,10
	.p2align 3
.L2686:
	cmpl	%r13d, -2268(%rbp)
	jl	.L2689
	movq	-2280(%rbp), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rdx
	movq	16(%rax), %rax
	addq	%r13, %rdx
	movzbl	-4(%rax,%rdx,8), %r14d
	cmpb	$13, %r14b
	ja	.L2690
	leaq	.L2692(%rip), %rsi
	movzbl	%r14b, %edx
	movslq	(%rsi,%rdx,4), %rax
	addq	%rsi, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler22RepresentationSelector9VisitCallEPNS1_4NodeEPNS1_18SimplifiedLoweringE,"aG",@progbits,_ZN2v88internal8compiler22RepresentationSelector9VisitCallEPNS1_4NodeEPNS1_18SimplifiedLoweringE,comdat
	.align 4
	.align 4
.L2692:
	.long	.L2690-.L2692
	.long	.L2698-.L2692
	.long	.L2697-.L2692
	.long	.L2697-.L2692
	.long	.L2697-.L2692
	.long	.L2696-.L2692
	.long	.L2812-.L2692
	.long	.L2695-.L2692
	.long	.L2695-.L2692
	.long	.L2812-.L2692
	.long	.L2694-.L2692
	.long	.L2694-.L2692
	.long	.L2812-.L2692
	.long	.L2691-.L2692
	.section	.text._ZN2v88internal8compiler22RepresentationSelector9VisitCallEPNS1_4NodeEPNS1_18SimplifiedLoweringE,"axG",@progbits,_ZN2v88internal8compiler22RepresentationSelector9VisitCallEPNS1_4NodeEPNS1_18SimplifiedLoweringE,comdat
	.p2align 4,,10
	.p2align 3
.L2812:
	movl	$1, %r10d
	movl	$5, %r12d
.L2693:
	movl	120(%r15), %eax
	testl	%eax, %eax
	je	.L2699
	cmpl	$2, %eax
	jne	.L2688
	movl	20(%rbx), %esi
	leaq	32(%rbx), %rdi
	leaq	0(,%r13,8), %rdx
	movb	%r14b, -2192(%rbp)
	movb	%r12b, -2188(%rbp)
	movl	%esi, %eax
	movl	%r10d, -2184(%rbp)
	xorl	$251658240, %eax
	movb	$0, -2180(%rbp)
	movq	$0, -2176(%rbp)
	movl	$-1, -2168(%rbp)
	movq	%rdi, -2336(%rbp)
	movq	%rdx, -2312(%rbp)
	testl	$251658240, %eax
	je	.L2735
	movq	%rdi, %rax
	addq	%rdx, %rax
.L2736:
	movq	(%rax), %r11
	movl	20(%r11), %edx
	movl	%edx, %r8d
	andl	$16777215, %r8d
	movl	%r8d, %eax
	leaq	(%rax,%rax,4), %rdi
	movq	32(%r15), %rax
	leaq	(%rax,%rdi,8), %rdi
	movq	%rdi, -2320(%rbp)
	cmpb	1(%rdi), %r14b
	je	.L2688
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	jne	.L2906
.L2745:
	andl	$16777215, %edx
	leaq	(%rdx,%rdx,4), %rdx
	movq	24(%rax,%rdx,8), %rcx
	testq	%rcx, %rcx
	jne	.L2746
	movq	8(%r11), %rcx
.L2746:
	movq	-2320(%rbp), %rax
	movq	128(%r15), %rdi
	movq	%rbx, %r8
	movq	%r11, %rsi
	movzbl	1(%rax), %edx
	pushq	-2168(%rbp)
	pushq	-2176(%rbp)
	pushq	-2184(%rbp)
	pushq	-2192(%rbp)
	call	_ZN2v88internal8compiler21RepresentationChanger20GetRepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE@PLT
	addq	$32, %rsp
	movq	%rax, %r12
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L2747
	movq	32(%rbx,%r13,8), %rdi
	movq	-2336(%rbp), %r14
	movq	%rbx, %rdx
	addq	-2312(%rbp), %r14
	cmpq	%rdi, %r12
	jne	.L2797
	.p2align 4,,10
	.p2align 3
.L2688:
	leaq	1(%r13), %rax
	cmpq	%r13, -2264(%rbp)
	je	.L2800
.L2907:
	movq	%rax, %r13
	movl	%r13d, %ecx
	testq	%r13, %r13
	jne	.L2686
.L2905:
	movl	120(%r15), %eax
	movb	$0, -2192(%rbp)
	movb	$5, -2188(%rbp)
	movl	$1, -2184(%rbp)
	movb	$0, -2180(%rbp)
	movq	$0, -2176(%rbp)
	movl	$-1, -2168(%rbp)
	testl	%eax, %eax
	jne	.L2688
	pushq	-2168(%rbp)
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	pushq	-2176(%rbp)
	pushq	-2184(%rbp)
	pushq	-2192(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12EnqueueInputEPNS1_4NodeEiNS1_7UseInfoE
	addq	$32, %rsp
	leaq	1(%r13), %rax
	cmpq	%r13, -2264(%rbp)
	jne	.L2907
	.p2align 4,,10
	.p2align 3
.L2800:
	movl	-2284(%rbp), %edx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector22ProcessRemainingInputsEPNS1_4NodeEi
	movq	-2280(%rbp), %rax
	movq	24(%rax), %rdx
	cmpq	$0, (%rdx)
	je	.L2908
	movl	20(%rbx), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rcx
	movq	32(%r15), %rax
	leaq	(%rax,%rcx,8), %rcx
	movl	120(%r15), %eax
	testl	%eax, %eax
	je	.L2802
	cmpl	$1, %eax
	jne	.L2682
	movq	16(%rdx), %rax
	movzbl	4(%rax), %eax
	movb	%al, 1(%rcx)
	.p2align 4,,10
	.p2align 3
.L2682:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2909
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2689:
	.cfi_restore_state
	movl	120(%r15), %eax
	testl	%eax, %eax
	je	.L2751
	cmpl	$2, %eax
	jne	.L2688
	movl	20(%rbx), %esi
	leaq	32(%rbx), %rdi
	leaq	0(,%r13,8), %rdx
	movb	$8, -2192(%rbp)
	movb	$5, -2188(%rbp)
	movl	%esi, %eax
	movl	$1, -2184(%rbp)
	xorl	$251658240, %eax
	movb	$0, -2180(%rbp)
	movq	$0, -2176(%rbp)
	movl	$-1, -2168(%rbp)
	movq	%rdi, -2320(%rbp)
	movq	%rdx, -2312(%rbp)
	testl	$251658240, %eax
	je	.L2784
	movq	%rdi, %rax
	addq	%rdx, %rax
.L2785:
	movq	(%rax), %r14
	movl	20(%r14), %edx
	movl	%edx, %r8d
	andl	$16777215, %r8d
	movl	%r8d, %eax
	leaq	(%rax,%rax,4), %rdi
	movq	32(%r15), %rax
	leaq	(%rax,%rdi,8), %r12
	cmpb	$8, 1(%r12)
	je	.L2688
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	jne	.L2910
.L2794:
	andl	$16777215, %edx
	leaq	(%rdx,%rdx,4), %rdx
	movq	24(%rax,%rdx,8), %rcx
	testq	%rcx, %rcx
	jne	.L2795
	movq	8(%r14), %rcx
.L2795:
	movzbl	1(%r12), %edx
	pushq	-2168(%rbp)
	movq	%rbx, %r8
	movq	%r14, %rsi
	movq	128(%r15), %rdi
	pushq	-2176(%rbp)
	pushq	-2184(%rbp)
	pushq	-2192(%rbp)
	call	_ZN2v88internal8compiler21RepresentationChanger20GetRepresentationForEPNS1_4NodeENS0_21MachineRepresentationENS1_4TypeES4_NS1_7UseInfoE@PLT
	addq	$32, %rsp
	movq	%rax, %r12
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L2796
	movq	32(%rbx,%r13,8), %rdi
	movq	-2320(%rbp), %r14
	movq	%rbx, %rdx
	addq	-2312(%rbp), %r14
	cmpq	%r12, %rdi
	je	.L2688
.L2797:
	leaq	1(%r13), %rax
	leaq	0(,%rax,4), %rcx
	subq	%rcx, %rax
	leaq	(%rdx,%rax,8), %rsi
	testq	%rdi, %rdi
	je	.L2799
	movq	%rsi, -2312(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-2312(%rbp), %rsi
.L2799:
	movq	%r12, (%r14)
	testq	%r12, %r12
	je	.L2688
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	jmp	.L2688
	.p2align 4,,10
	.p2align 3
.L2751:
	movzbl	23(%rbx), %eax
	leaq	32(%rbx), %rdx
	leaq	0(,%r13,8), %rcx
	addq	%rcx, %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2754
	movq	32(%rbx), %rax
	leaq	16(%rax,%rcx), %rdx
.L2754:
	movq	(%rdx), %rax
	movl	20(%rax), %esi
	movq	%rax, -2240(%rbp)
	andl	$16777215, %esi
	movl	%esi, %eax
	leaq	(%rax,%rax,4), %rdx
	movq	32(%r15), %rax
	leaq	(%rax,%rdx,8), %r14
	cmpb	$0, (%r14)
	je	.L2911
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	jne	.L2912
.L2898:
	movl	8(%r14), %edx
.L2767:
	movzbl	4(%r14), %r12d
	movl	%edx, %edi
	movl	$1, %esi
	movl	%edx, -2320(%rbp)
	call	_ZN2v88internal8compiler10Truncation23GeneralizeIdentifyZerosENS1_13IdentifyZerosES3_@PLT
	movl	$5, %esi
	movl	%r12d, %edi
	movl	%eax, -2312(%rbp)
	call	_ZN2v88internal8compiler10Truncation10GeneralizeENS2_14TruncationKindES3_@PLT
	movl	-2312(%rbp), %ecx
	movl	-2320(%rbp), %edx
	movb	%al, 4(%r14)
	movl	%ecx, 8(%r14)
	cmpl	%ecx, %edx
	jne	.L2822
	cmpb	%al, %r12b
	je	.L2688
.L2822:
	cmpb	$3, (%r14)
	jne	.L2913
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	je	.L2688
	leaq	.LC27(%rip), %rdi
.L2899:
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movzbl	4(%r14), %esi
	movl	8(%r14), %edx
	movzbl	_ZN2v88internal25FLAG_trace_representationE(%rip), %eax
	movb	%sil, -2192(%rbp)
	movl	%edx, -2188(%rbp)
	testb	%al, %al
	je	.L2688
	leaq	-320(%rbp), %rax
	leaq	-400(%rbp), %r12
	movq	%rax, %rdi
	movq	%rax, -2328(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	stdout(%rip), %rdx
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movw	%cx, -96(%rbp)
	movq	%rax, -320(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	$0, -104(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	leaq	-2192(%rbp), %rdi
	movq	%rax, -400(%rbp)
	addq	$40, %rax
	movq	%rax, -320(%rbp)
	call	_ZNK2v88internal8compiler10Truncation11descriptionEv@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2914
	movq	%rax, %rdi
	call	strlen@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L2781:
	movq	-400(%rbp), %rax
	movq	-24(%rax), %rax
	movq	-160(%rbp,%rax), %r14
	testq	%r14, %r14
	je	.L2720
	cmpb	$0, 56(%r14)
	je	.L2782
	movsbl	67(%r14), %esi
.L2783:
	movq	%r12, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movdqa	-2304(%rbp), %xmm6
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	-336(%rbp), %rdi
	movq	%rax, -320(%rbp)
	movaps	%xmm6, -400(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	-2328(%rbp), %rdi
	movq	%rax, -400(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L2688
	.p2align 4,,10
	.p2align 3
.L2699:
	movzbl	23(%rbx), %eax
	leaq	32(%rbx), %rcx
	leaq	0(,%r13,8), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L2702
	addq	%rcx, %rdx
.L2703:
	movq	(%rdx), %rax
	movl	20(%rax), %esi
	movq	%rax, -2248(%rbp)
	andl	$16777215, %esi
	movl	%esi, %eax
	leaq	(%rax,%rax,4), %rdx
	movq	32(%r15), %rax
	leaq	(%rax,%rdx,8), %r14
	cmpb	$0, (%r14)
	je	.L2915
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	jne	.L2916
	movl	8(%r14), %r8d
.L2717:
	movzbl	4(%r14), %eax
	movl	%r8d, %edi
	movl	%r10d, %esi
	movl	%r8d, -2328(%rbp)
	movb	%al, -2312(%rbp)
	call	_ZN2v88internal8compiler10Truncation23GeneralizeIdentifyZerosENS1_13IdentifyZerosES3_@PLT
	movl	%r12d, %esi
	movzbl	-2312(%rbp), %r12d
	movl	%eax, -2320(%rbp)
	movl	%r12d, %edi
	call	_ZN2v88internal8compiler10Truncation10GeneralizeENS2_14TruncationKindES3_@PLT
	movl	-2320(%rbp), %edx
	movl	-2328(%rbp), %r8d
	movb	%al, 4(%r14)
	movl	%edx, 8(%r14)
	cmpl	%r8d, %edx
	jne	.L2821
	cmpb	%al, %r12b
	je	.L2688
.L2821:
	cmpb	$3, (%r14)
	jne	.L2917
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	je	.L2688
	leaq	.LC27(%rip), %rdi
.L2896:
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movzbl	4(%r14), %esi
	movl	8(%r14), %edx
	movzbl	_ZN2v88internal25FLAG_trace_representationE(%rip), %eax
	movb	%sil, -2216(%rbp)
	movl	%edx, -2212(%rbp)
	testb	%al, %al
	je	.L2688
	leaq	-1376(%rbp), %r14
	leaq	-1456(%rbp), %r12
	movq	%r14, %rdi
	call	_ZNSt8ios_baseC2Ev@PLT
	xorl	%r10d, %r10d
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movq	stdout(%rip), %rdx
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movq	$0, -1160(%rbp)
	movq	%rax, -1376(%rbp)
	movw	%r10w, -1152(%rbp)
	movups	%xmm0, -1144(%rbp)
	movups	%xmm0, -1128(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	leaq	-2216(%rbp), %rdi
	movq	%rax, -1456(%rbp)
	addq	$40, %rax
	movq	%rax, -1376(%rbp)
	call	_ZNK2v88internal8compiler10Truncation11descriptionEv@PLT
	testq	%rax, %rax
	je	.L2918
	movq	%rax, %rdi
	movq	%rax, -2312(%rbp)
	call	strlen@PLT
	movq	-2312(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L2732:
	movq	-1456(%rbp), %rax
	movq	-24(%rax), %rax
	movq	-1216(%rbp,%rax), %rdi
	testq	%rdi, %rdi
	je	.L2720
	cmpb	$0, 56(%rdi)
	je	.L2733
	movsbl	67(%rdi), %esi
.L2734:
	movq	%r12, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movdqa	-2304(%rbp), %xmm5
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	-1392(%rbp), %rdi
	movq	%rax, -1376(%rbp)
	movaps	%xmm5, -1456(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -1456(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -1376(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L2688
	.p2align 4,,10
	.p2align 3
.L2697:
	xorl	%r10d, %r10d
	movl	$2, %r12d
	movl	$4, %r14d
	jmp	.L2693
	.p2align 4,,10
	.p2align 3
.L2908:
	movl	20(%rbx), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rdx
	movq	32(%r15), %rax
	leaq	(%rax,%rdx,8), %rdx
	movl	120(%r15), %eax
	testl	%eax, %eax
	je	.L2805
	cmpl	$1, %eax
	jne	.L2682
	movb	$8, 1(%rdx)
	jmp	.L2682
	.p2align 4,,10
	.p2align 3
.L2695:
	movl	$1, %r10d
	movl	$5, %r12d
	movl	$8, %r14d
	jmp	.L2693
	.p2align 4,,10
	.p2align 3
.L2694:
	movl	$1, %r10d
	movl	$5, %r12d
	movl	$11, %r14d
	jmp	.L2693
	.p2align 4,,10
	.p2align 3
.L2691:
	movl	$1, %r10d
	movl	$4, %r12d
	jmp	.L2693
	.p2align 4,,10
	.p2align 3
.L2696:
	movl	%r14d, %r12d
	movl	$1, %r10d
	jmp	.L2693
	.p2align 4,,10
	.p2align 3
.L2698:
	movl	%r14d, %r12d
	xorl	%r10d, %r10d
	jmp	.L2693
	.p2align 4,,10
	.p2align 3
.L2784:
	movq	32(%rbx), %rax
	leaq	16(%rax,%rdx), %rax
	jmp	.L2785
	.p2align 4,,10
	.p2align 3
.L2735:
	movq	32(%rbx), %rax
	leaq	16(%rax,%rdx), %rax
	jmp	.L2736
	.p2align 4,,10
	.p2align 3
.L2702:
	movq	32(%rbx), %rax
	leaq	16(%rax,%rdx), %rdx
	jmp	.L2703
	.p2align 4,,10
	.p2align 3
.L2796:
	movq	32(%rbx), %rdx
	movq	-2312(%rbp), %rax
	leaq	16(%rdx,%rax), %r14
	movq	(%r14), %rdi
	cmpq	%r12, %rdi
	jne	.L2797
	jmp	.L2688
	.p2align 4,,10
	.p2align 3
.L2747:
	movq	32(%rbx), %rdx
	movq	-2312(%rbp), %rax
	leaq	16(%rdx,%rax), %r14
	movq	(%r14), %rdi
	cmpq	%rdi, %r12
	jne	.L2797
	jmp	.L2688
	.p2align 4,,10
	.p2align 3
.L2906:
	movq	(%r11), %rax
	movq	(%rbx), %rdx
	andl	$16777215, %esi
	leaq	.LC13(%rip), %rdi
	movl	%r10d, -2344(%rbp)
	movq	8(%rax), %r9
	movq	8(%rdx), %rdx
	xorl	%eax, %eax
	movq	%r11, -2328(%rbp)
	call	_ZN2v88internal6PrintFEPKcz@PLT
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	movq	-2328(%rbp), %r11
	movl	-2344(%rbp), %r10d
	je	.L2897
	xorl	%eax, %eax
	leaq	.LC14(%rip), %rdi
	movq	%r11, -2344(%rbp)
	movl	%r10d, -2328(%rbp)
	call	_ZN2v88internal6PrintFEPKcz@PLT
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	movl	-2328(%rbp), %r10d
	movq	-2344(%rbp), %r11
	je	.L2897
	leaq	-320(%rbp), %rsi
	leaq	-400(%rbp), %rax
	movq	%r11, -2384(%rbp)
	movq	%rsi, %rdi
	movl	%r10d, -2376(%rbp)
	movq	%rax, -2344(%rbp)
	movq	%rsi, -2328(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	stdout(%rip), %rdx
	xorl	%r9d, %r9d
	pxor	%xmm3, %xmm3
	movq	-2344(%rbp), %rdi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rcx
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movw	%r9w, -96(%rbp)
	movq	%rcx, -320(%rbp)
	movups	%xmm3, -88(%rbp)
	movups	%xmm3, -72(%rbp)
	movq	$0, -104(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rdx
	movq	-2344(%rbp), %rdi
	movq	%rdx, -400(%rbp)
	addq	$40, %rdx
	movq	%rdx, -320(%rbp)
	movq	-2320(%rbp), %rdx
	movzbl	1(%rdx), %esi
	call	_ZN2v88internallsERSoNS0_21MachineRepresentationE@PLT
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	movdqa	-2304(%rbp), %xmm7
	movq	%rax, -2360(%rbp)
	movq	%rax, -320(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -2368(%rbp)
	movaps	%xmm7, -400(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	-2328(%rbp), %rdi
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rcx
	movq	%rax, -2352(%rbp)
	movq	%rax, -400(%rbp)
	movq	%rcx, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	movl	-2376(%rbp), %r10d
	movq	-2384(%rbp), %r11
	je	.L2897
	xorl	%eax, %eax
	leaq	.LC15(%rip), %rdi
	movq	%r11, -2384(%rbp)
	movl	%r10d, -2376(%rbp)
	call	_ZN2v88internal6PrintFEPKcz@PLT
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	movl	-2376(%rbp), %r10d
	movq	-2384(%rbp), %r11
	je	.L2897
	movq	-2328(%rbp), %rdi
	movq	%r11, -2384(%rbp)
	movl	%r10d, -2376(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	stdout(%rip), %rdx
	xorl	%r8d, %r8d
	pxor	%xmm4, %xmm4
	movq	-2344(%rbp), %rdi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movw	%r8w, -96(%rbp)
	movups	%xmm4, -88(%rbp)
	movups	%xmm4, -72(%rbp)
	movq	%rax, -320(%rbp)
	movq	$0, -104(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	movl	%r14d, %esi
	movq	-2344(%rbp), %r14
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movq	%rax, -400(%rbp)
	addq	$40, %rax
	movq	%r14, %rdi
	movq	%rax, -320(%rbp)
	call	_ZN2v88internallsERSoNS0_21MachineRepresentationE@PLT
	movl	$1, %edx
	leaq	.LC4(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	-2376(%rbp), %r10d
	leaq	-2200(%rbp), %rdi
	movb	%r12b, -2200(%rbp)
	movl	%r10d, -2196(%rbp)
	call	_ZNK2v88internal8compiler10Truncation11descriptionEv@PLT
	movq	-2384(%rbp), %r11
	testq	%rax, %rax
	movq	%rax, %r12
	movq	%r11, -2376(%rbp)
	je	.L2919
	movq	%rax, %rdi
	call	strlen@PLT
	movq	-2344(%rbp), %rdi
	movq	%r12, %rsi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-2376(%rbp), %r11
.L2744:
	movq	-2360(%rbp), %rax
	movdqa	-2304(%rbp), %xmm7
	movq	%r11, -2344(%rbp)
	movq	-2368(%rbp), %rdi
	movq	%rax, -320(%rbp)
	movaps	%xmm7, -400(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	-2352(%rbp), %rax
	movq	-2328(%rbp), %rdi
	movq	%rax, -400(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	movq	-2344(%rbp), %r11
	je	.L2897
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	movq	%r11, -2328(%rbp)
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-2328(%rbp), %r11
.L2897:
	movl	20(%r11), %edx
	movq	32(%r15), %rax
	jmp	.L2745
	.p2align 4,,10
	.p2align 3
.L2910:
	movq	(%r14), %rax
	movq	(%rbx), %rdx
	andl	$16777215, %esi
	leaq	.LC13(%rip), %rdi
	movq	8(%rax), %r9
	movq	8(%rdx), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	je	.L2900
	xorl	%eax, %eax
	leaq	.LC14(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	je	.L2900
	leaq	-320(%rbp), %rsi
	leaq	-400(%rbp), %rax
	movq	%rsi, %rdi
	movq	%rax, -2336(%rbp)
	movq	%rsi, -2328(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	xorl	%edx, %edx
	pxor	%xmm5, %xmm5
	movq	-2336(%rbp), %rdi
	movw	%dx, -96(%rbp)
	movq	stdout(%rip), %rdx
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rcx
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movq	%rcx, -320(%rbp)
	movups	%xmm5, -88(%rbp)
	movups	%xmm5, -72(%rbp)
	movq	$0, -104(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rdx
	movq	-2336(%rbp), %rdi
	movq	%rdx, -400(%rbp)
	addq	$40, %rdx
	movq	%rdx, -320(%rbp)
	movzbl	1(%r12), %esi
	call	_ZN2v88internallsERSoNS0_21MachineRepresentationE@PLT
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	movdqa	-2304(%rbp), %xmm7
	movq	%rax, -2360(%rbp)
	movq	%rax, -320(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -2368(%rbp)
	movaps	%xmm7, -400(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	-2328(%rbp), %rdi
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rcx
	movq	%rax, -2352(%rbp)
	movq	%rax, -400(%rbp)
	movq	%rcx, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	je	.L2900
	xorl	%eax, %eax
	leaq	.LC15(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	je	.L2900
	movq	-2328(%rbp), %rdi
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	stdout(%rip), %rdx
	movq	-2336(%rbp), %rdi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	pxor	%xmm6, %xmm6
	movq	%rax, -320(%rbp)
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	xorl	%eax, %eax
	movups	%xmm6, -88(%rbp)
	movups	%xmm6, -72(%rbp)
	movw	%ax, -96(%rbp)
	movq	$0, -104(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	movq	-2336(%rbp), %rdi
	movl	$8, %esi
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movq	%rax, -400(%rbp)
	addq	$40, %rax
	movq	%rax, -320(%rbp)
	call	_ZN2v88internallsERSoNS0_21MachineRepresentationE@PLT
	movq	-2336(%rbp), %rdi
	movl	$1, %edx
	leaq	.LC4(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	-2200(%rbp), %rdi
	movb	$5, -2200(%rbp)
	movl	$1, -2196(%rbp)
	call	_ZNK2v88internal8compiler10Truncation11descriptionEv@PLT
	testq	%rax, %rax
	je	.L2920
	movq	%rax, %rdi
	movq	%rax, -2344(%rbp)
	call	strlen@PLT
	movq	-2344(%rbp), %rsi
	movq	-2336(%rbp), %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L2793:
	movq	-2360(%rbp), %rax
	movdqa	-2304(%rbp), %xmm7
	movq	-2368(%rbp), %rdi
	movq	%rax, -320(%rbp)
	movaps	%xmm7, -400(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	-2352(%rbp), %rax
	movq	-2328(%rbp), %rdi
	movq	%rax, -400(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	je	.L2900
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
.L2900:
	movq	32(%r15), %rax
	movl	20(%r14), %edx
	jmp	.L2794
	.p2align 4,,10
	.p2align 3
.L2916:
	xorl	%eax, %eax
	leaq	.LC25(%rip), %rdi
	movl	%r10d, -2312(%rbp)
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movzbl	4(%r14), %eax
	movl	8(%r14), %r8d
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	movl	-2312(%rbp), %r10d
	movb	%al, -2224(%rbp)
	movl	%r8d, -2220(%rbp)
	je	.L2717
	leaq	-1728(%rbp), %rsi
	leaq	-1808(%rbp), %rax
	movl	%r10d, -2328(%rbp)
	movq	%rsi, %rdi
	movq	%rax, -2312(%rbp)
	movq	%rsi, -2320(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	stdout(%rip), %rdx
	xorl	%r11d, %r11d
	pxor	%xmm0, %xmm0
	movq	-2312(%rbp), %rdi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rsi
	movw	%r11w, -1504(%rbp)
	movq	%rsi, -1728(%rbp)
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movups	%xmm0, -1496(%rbp)
	movups	%xmm0, -1480(%rbp)
	movq	$0, -1512(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	leaq	-2224(%rbp), %rdi
	movq	%rax, -1808(%rbp)
	addq	$40, %rax
	movq	%rax, -1728(%rbp)
	call	_ZNK2v88internal8compiler10Truncation11descriptionEv@PLT
	movl	-2328(%rbp), %r10d
	testq	%rax, %rax
	je	.L2921
	movq	%rax, %rdi
	movl	%r10d, -2336(%rbp)
	movq	%rax, -2328(%rbp)
	call	strlen@PLT
	movq	-2328(%rbp), %rsi
	movq	-2312(%rbp), %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	-2336(%rbp), %r10d
.L2719:
	movq	-1808(%rbp), %rax
	movq	-24(%rax), %rax
	movq	-1568(%rbp,%rax), %rdi
	testq	%rdi, %rdi
	je	.L2720
	cmpb	$0, 56(%rdi)
	je	.L2721
	movsbl	67(%rdi), %esi
.L2722:
	movq	-2312(%rbp), %rdi
	movl	%r10d, -2328(%rbp)
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movdqa	-2304(%rbp), %xmm3
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	-1744(%rbp), %rdi
	movq	%rax, -1728(%rbp)
	movaps	%xmm3, -1808(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	-2320(%rbp), %rdi
	movq	%rax, -1808(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -1728(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movl	8(%r14), %r8d
	movl	-2328(%rbp), %r10d
	jmp	.L2717
	.p2align 4,,10
	.p2align 3
.L2912:
	xorl	%eax, %eax
	leaq	.LC25(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movl	8(%r14), %edx
	movzbl	4(%r14), %eax
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	movb	%al, -2200(%rbp)
	movl	%edx, -2196(%rbp)
	je	.L2767
	leaq	-672(%rbp), %rax
	leaq	-752(%rbp), %r12
	movq	%rax, %rdi
	movq	%rax, -2312(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movq	stdout(%rip), %rdx
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movw	%si, -448(%rbp)
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movq	%rax, -672(%rbp)
	movq	$0, -456(%rbp)
	movups	%xmm0, -440(%rbp)
	movups	%xmm0, -424(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	leaq	-2200(%rbp), %rdi
	movq	%rax, -752(%rbp)
	addq	$40, %rax
	movq	%rax, -672(%rbp)
	call	_ZNK2v88internal8compiler10Truncation11descriptionEv@PLT
	testq	%rax, %rax
	je	.L2922
	movq	%rax, %rdi
	movq	%rax, -2320(%rbp)
	call	strlen@PLT
	movq	-2320(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L2769:
	movq	-752(%rbp), %rax
	movq	-24(%rax), %rax
	movq	-512(%rbp,%rax), %rdi
	testq	%rdi, %rdi
	je	.L2720
	cmpb	$0, 56(%rdi)
	je	.L2770
	movsbl	67(%rdi), %esi
.L2771:
	movq	%r12, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movdqa	-2304(%rbp), %xmm4
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	-688(%rbp), %rdi
	movq	%rax, -672(%rbp)
	movaps	%xmm4, -752(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	-2312(%rbp), %rdi
	movq	%rax, -752(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -672(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L2898
	.p2align 4,,10
	.p2align 3
.L2913:
	movq	216(%r15), %rax
	movq	200(%r15), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L2775
	movq	-2240(%rbp), %rax
	movq	%rax, (%rdx)
	addq	$8, 200(%r15)
.L2776:
	movb	$3, (%r14)
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	leaq	.LC26(%rip), %rdi
	je	.L2688
	jmp	.L2899
	.p2align 4,,10
	.p2align 3
.L2917:
	movq	216(%r15), %rax
	movq	200(%r15), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L2726
	movq	-2248(%rbp), %rax
	movq	%rax, (%rdx)
	addq	$8, 200(%r15)
.L2727:
	movb	$3, (%r14)
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	leaq	.LC26(%rip), %rdi
	je	.L2688
	jmp	.L2896
	.p2align 4,,10
	.p2align 3
.L2802:
	movl	$4294967295, %eax
	movq	%rax, 16(%rcx)
	jmp	.L2682
	.p2align 4,,10
	.p2align 3
.L2805:
	movl	$4294967295, %eax
	movq	%rax, 16(%rdx)
	jmp	.L2682
	.p2align 4,,10
	.p2align 3
.L2911:
	movb	$3, (%r14)
	movq	72(%r15), %rsi
	cmpq	80(%r15), %rsi
	je	.L2755
	movq	-2240(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 72(%r15)
.L2756:
	movq	216(%r15), %rax
	movq	200(%r15), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L2757
	movq	-2240(%rbp), %rax
	movq	%rax, (%rdx)
	addq	$8, 200(%r15)
.L2758:
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	jne	.L2923
.L2759:
	movzbl	4(%r14), %r8d
	movl	8(%r14), %edi
	movl	$1, %esi
	movb	%r8b, -2312(%rbp)
	call	_ZN2v88internal8compiler10Truncation23GeneralizeIdentifyZerosENS1_13IdentifyZerosES3_@PLT
	movzbl	-2312(%rbp), %r8d
	movl	$5, %esi
	movl	%eax, %r12d
	movl	%r8d, %edi
	call	_ZN2v88internal8compiler10Truncation10GeneralizeENS2_14TruncationKindES3_@PLT
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	movl	%r12d, 8(%r14)
	movb	%al, 4(%r14)
	movb	%al, -2208(%rbp)
	movl	%r12d, -2204(%rbp)
	je	.L2688
	leaq	-1024(%rbp), %r14
	leaq	-1104(%rbp), %r12
	movq	%r14, %rdi
	call	_ZNSt8ios_baseC2Ev@PLT
	xorl	%edi, %edi
	movq	stdout(%rip), %rdx
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movw	%di, -800(%rbp)
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -1024(%rbp)
	movq	$0, -808(%rbp)
	movups	%xmm0, -792(%rbp)
	movups	%xmm0, -776(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	leaq	-2208(%rbp), %rdi
	movq	%rax, -1104(%rbp)
	addq	$40, %rax
	movq	%rax, -1024(%rbp)
	call	_ZNK2v88internal8compiler10Truncation11descriptionEv@PLT
	testq	%rax, %rax
	je	.L2924
	movq	%rax, %rdi
	movq	%rax, -2312(%rbp)
	call	strlen@PLT
	movq	-2312(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L2762:
	movq	-1104(%rbp), %rax
	movq	-24(%rax), %rax
	movq	-864(%rbp,%rax), %rdi
	testq	%rdi, %rdi
	je	.L2720
	cmpb	$0, 56(%rdi)
	je	.L2763
	movsbl	67(%rdi), %esi
.L2764:
	movq	%r12, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movdqa	-2304(%rbp), %xmm6
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	-1040(%rbp), %rdi
	movq	%rax, -1024(%rbp)
	movaps	%xmm6, -1104(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -1104(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -1024(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L2688
	.p2align 4,,10
	.p2align 3
.L2915:
	movb	$3, (%r14)
	movq	72(%r15), %rsi
	cmpq	80(%r15), %rsi
	je	.L2704
	movq	-2248(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 72(%r15)
.L2705:
	movq	216(%r15), %rax
	movq	200(%r15), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L2706
	movq	-2248(%rbp), %rax
	movq	%rax, (%rdx)
	addq	$8, 200(%r15)
.L2707:
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	jne	.L2925
.L2708:
	movzbl	4(%r14), %r8d
	movl	8(%r14), %edi
	movl	%r10d, %esi
	movb	%r8b, -2320(%rbp)
	call	_ZN2v88internal8compiler10Truncation23GeneralizeIdentifyZerosENS1_13IdentifyZerosES3_@PLT
	movzbl	-2320(%rbp), %r8d
	movl	%r12d, %esi
	movl	%eax, -2312(%rbp)
	movl	%r8d, %edi
	call	_ZN2v88internal8compiler10Truncation10GeneralizeENS2_14TruncationKindES3_@PLT
	movl	-2312(%rbp), %edx
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	movb	%al, 4(%r14)
	movl	%edx, 8(%r14)
	movb	%al, -2232(%rbp)
	movl	%edx, -2228(%rbp)
	je	.L2688
	leaq	-2080(%rbp), %r14
	leaq	-2160(%rbp), %r12
	movq	%r14, %rdi
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movq	stdout(%rip), %rdx
	movq	%rax, -2080(%rbp)
	xorl	%eax, %eax
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movw	%ax, -1856(%rbp)
	movq	$0, -1864(%rbp)
	movups	%xmm0, -1848(%rbp)
	movups	%xmm0, -1832(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	leaq	-2232(%rbp), %rdi
	movq	%rax, -2160(%rbp)
	addq	$40, %rax
	movq	%rax, -2080(%rbp)
	call	_ZNK2v88internal8compiler10Truncation11descriptionEv@PLT
	testq	%rax, %rax
	je	.L2926
	movq	%rax, %rdi
	movq	%rax, -2312(%rbp)
	call	strlen@PLT
	movq	-2312(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L2711:
	movq	-2160(%rbp), %rax
	movq	-24(%rax), %rax
	movq	-1920(%rbp,%rax), %rdi
	testq	%rdi, %rdi
	je	.L2720
	cmpb	$0, 56(%rdi)
	je	.L2713
	movsbl	67(%rdi), %esi
.L2714:
	movq	%r12, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movdqa	-2304(%rbp), %xmm5
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	-2096(%rbp), %rdi
	movq	%rax, -2080(%rbp)
	movaps	%xmm5, -2160(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -2160(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -2080(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L2688
.L2721:
	movl	%r10d, -2336(%rbp)
	movq	%rdi, -2328(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-2328(%rbp), %rdi
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movl	-2336(%rbp), %r10d
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L2722
	movl	%r10d, -2328(%rbp)
	call	*%rax
	movl	-2328(%rbp), %r10d
	movsbl	%al, %esi
	jmp	.L2722
.L2770:
	movq	%rdi, -2320(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-2320(%rbp), %rdi
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L2771
	call	*%rax
	movsbl	%al, %esi
	jmp	.L2771
.L2920:
	movq	-400(%rbp), %rax
	movq	-2336(%rbp), %rdi
	addq	-24(%rax), %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L2793
.L2919:
	movq	-400(%rbp), %rax
	movq	-2344(%rbp), %rdi
	addq	-24(%rax), %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	movq	-2376(%rbp), %r11
	jmp	.L2744
.L2921:
	movq	-1808(%rbp), %rax
	movq	-2312(%rbp), %rdi
	addq	-24(%rax), %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	movl	-2328(%rbp), %r10d
	jmp	.L2719
.L2922:
	movq	-752(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L2769
.L2733:
	movq	%rdi, -2312(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-2312(%rbp), %rdi
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L2734
	call	*%rax
	movsbl	%al, %esi
	jmp	.L2734
.L2782:
	movq	%r14, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%r14), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L2783
	movq	%r14, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L2783
.L2923:
	movq	-2240(%rbp), %rax
	leaq	.LC24(%rip), %rdi
	movl	20(%rax), %esi
	xorl	%eax, %eax
	andl	$16777215, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L2759
.L2925:
	movq	-2248(%rbp), %rax
	leaq	.LC24(%rip), %rdi
	movl	%r10d, -2312(%rbp)
	movl	20(%rax), %esi
	xorl	%eax, %eax
	andl	$16777215, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movl	-2312(%rbp), %r10d
	jmp	.L2708
.L2914:
	movq	-400(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L2781
.L2918:
	movq	-1456(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L2732
.L2755:
	leaq	-2240(%rbp), %rdx
	leaq	56(%r15), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L2756
.L2757:
	leaq	-2240(%rbp), %rsi
	leaq	136(%r15), %rdi
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	jmp	.L2758
.L2706:
	leaq	-2248(%rbp), %rsi
	leaq	136(%r15), %rdi
	movl	%r10d, -2312(%rbp)
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	movl	-2312(%rbp), %r10d
	jmp	.L2707
.L2704:
	leaq	-2248(%rbp), %rdx
	leaq	56(%r15), %rdi
	movl	%r10d, -2312(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	movl	-2312(%rbp), %r10d
	jmp	.L2705
.L2726:
	leaq	-2248(%rbp), %rsi
	leaq	136(%r15), %rdi
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	jmp	.L2727
.L2775:
	leaq	-2240(%rbp), %rsi
	leaq	136(%r15), %rdi
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	jmp	.L2776
.L2713:
	movq	%rdi, -2312(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-2312(%rbp), %rdi
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L2714
	call	*%rax
	movsbl	%al, %esi
	jmp	.L2714
.L2763:
	movq	%rdi, -2312(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-2312(%rbp), %rdi
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L2764
	call	*%rax
	movsbl	%al, %esi
	jmp	.L2764
.L2924:
	movq	-1104(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L2762
.L2926:
	movq	-2160(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L2711
.L2690:
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2720:
	call	_ZSt16__throw_bad_castv@PLT
.L2909:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23033:
	.size	_ZN2v88internal8compiler22RepresentationSelector9VisitCallEPNS1_4NodeEPNS1_18SimplifiedLoweringE, .-_ZN2v88internal8compiler22RepresentationSelector9VisitCallEPNS1_4NodeEPNS1_18SimplifiedLoweringE
	.section	.rodata._ZN2v88internal8compiler22RepresentationSelector9VisitNodeEPNS1_4NodeENS1_10TruncationEPNS1_18SimplifiedLoweringE.str1.8,"aMS",@progbits,1
	.align 8
.LC30:
	.string	"Representation inference: unsupported opcode %i (%s), node #%i\n."
	.section	.text._ZN2v88internal8compiler22RepresentationSelector9VisitNodeEPNS1_4NodeENS1_10TruncationEPNS1_18SimplifiedLoweringE,"axG",@progbits,_ZN2v88internal8compiler22RepresentationSelector9VisitNodeEPNS1_4NodeENS1_10TruncationEPNS1_18SimplifiedLoweringE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler22RepresentationSelector9VisitNodeEPNS1_4NodeENS1_10TruncationEPNS1_18SimplifiedLoweringE
	.type	_ZN2v88internal8compiler22RepresentationSelector9VisitNodeEPNS1_4NodeENS1_10TruncationEPNS1_18SimplifiedLoweringE, @function
_ZN2v88internal8compiler22RepresentationSelector9VisitNodeEPNS1_4NodeENS1_10TruncationEPNS1_18SimplifiedLoweringE:
.LFB23059:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$1912, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, %r12
	movq	%rdx, %rax
	movq	464(%rdi), %rdi
	movq	%rsi, %r14
	shrq	$32, %rax
	movq	%rdx, %r15
	movq	%rcx, %r13
	movq	%rax, -6008(%rbp)
	movl	%edx, %ebx
	call	_ZN2v88internal11TickCounter6DoTickEv@PLT
	movq	(%r14), %rdi
	movl	20(%rdi), %r10d
	testl	%r10d, %r10d
	jle	.L2928
	movzbl	18(%rdi), %eax
	andl	$124, %eax
	cmpb	$124, %al
	jne	.L2928
	testb	%r15b, %r15b
	je	.L3550
.L2928:
	cmpl	$2, 120(%r12)
	movzwl	16(%rdi), %eax
	je	.L4107
.L2930:
	cmpw	$788, %ax
	ja	.L2948
	leaq	.L2949(%rip), %rsi
	movzwl	%ax, %ecx
	movslq	(%rsi,%rcx,4), %rdx
	addq	%rsi, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal8compiler22RepresentationSelector9VisitNodeEPNS1_4NodeENS1_10TruncationEPNS1_18SimplifiedLoweringE,"aG",@progbits,_ZN2v88internal8compiler22RepresentationSelector9VisitNodeEPNS1_4NodeENS1_10TruncationEPNS1_18SimplifiedLoweringE,comdat
	.align 4
	.align 4
.L2949:
	.long	.L3095-.L2949
	.long	.L2931-.L2949
	.long	.L3094-.L2949
	.long	.L3093-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L3092-.L2949
	.long	.L2948-.L2949
	.long	.L2931-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L3091-.L2949
	.long	.L3090-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L3089-.L2949
	.long	.L3088-.L2949
	.long	.L3087-.L2949
	.long	.L2958-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L3086-.L2949
	.long	.L3085-.L2949
	.long	.L2931-.L2949
	.long	.L2948-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L3084-.L2949
	.long	.L3083-.L2949
	.long	.L3082-.L2949
	.long	.L2948-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L3081-.L2949
	.long	.L3080-.L2949
	.long	.L2948-.L2949
	.long	.L3079-.L2949
	.long	.L3078-.L2949
	.long	.L2931-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2931-.L2949
	.long	.L2948-.L2949
	.long	.L3077-.L2949
	.long	.L3076-.L2949
	.long	.L2931-.L2949
	.long	.L3075-.L2949
	.long	.L2948-.L2949
	.long	.L3074-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L3073-.L2949
	.long	.L3072-.L2949
	.long	.L3072-.L2949
	.long	.L3071-.L2949
	.long	.L3071-.L2949
	.long	.L3071-.L2949
	.long	.L3070-.L2949
	.long	.L3069-.L2949
	.long	.L3068-.L2949
	.long	.L2948-.L2949
	.long	.L3067-.L2949
	.long	.L3067-.L2949
	.long	.L3067-.L2949
	.long	.L3066-.L2949
	.long	.L3066-.L2949
	.long	.L3065-.L2949
	.long	.L3064-.L2949
	.long	.L3063-.L2949
	.long	.L3062-.L2949
	.long	.L3062-.L2949
	.long	.L3062-.L2949
	.long	.L3061-.L2949
	.long	.L3060-.L2949
	.long	.L3059-.L2949
	.long	.L3055-.L2949
	.long	.L3058-.L2949
	.long	.L3057-.L2949
	.long	.L3056-.L2949
	.long	.L3055-.L2949
	.long	.L2948-.L2949
	.long	.L3054-.L2949
	.long	.L3054-.L2949
	.long	.L3053-.L2949
	.long	.L3052-.L2949
	.long	.L3051-.L2949
	.long	.L3050-.L2949
	.long	.L3050-.L2949
	.long	.L3050-.L2949
	.long	.L3049-.L2949
	.long	.L3048-.L2949
	.long	.L3047-.L2949
	.long	.L3046-.L2949
	.long	.L3046-.L2949
	.long	.L3045-.L2949
	.long	.L3040-.L2949
	.long	.L3040-.L2949
	.long	.L3040-.L2949
	.long	.L3040-.L2949
	.long	.L3040-.L2949
	.long	.L3040-.L2949
	.long	.L3040-.L2949
	.long	.L3039-.L2949
	.long	.L3044-.L2949
	.long	.L3040-.L2949
	.long	.L3040-.L2949
	.long	.L3040-.L2949
	.long	.L3040-.L2949
	.long	.L3039-.L2949
	.long	.L3043-.L2949
	.long	.L3040-.L2949
	.long	.L3040-.L2949
	.long	.L3040-.L2949
	.long	.L3040-.L2949
	.long	.L3039-.L2949
	.long	.L3042-.L2949
	.long	.L3040-.L2949
	.long	.L3040-.L2949
	.long	.L3041-.L2949
	.long	.L3040-.L2949
	.long	.L3040-.L2949
	.long	.L3039-.L2949
	.long	.L3038-.L2949
	.long	.L3037-.L2949
	.long	.L3036-.L2949
	.long	.L3035-.L2949
	.long	.L3034-.L2949
	.long	.L3033-.L2949
	.long	.L3032-.L2949
	.long	.L2948-.L2949
	.long	.L3031-.L2949
	.long	.L3030-.L2949
	.long	.L3029-.L2949
	.long	.L3028-.L2949
	.long	.L3027-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L3026-.L2949
	.long	.L3025-.L2949
	.long	.L2948-.L2949
	.long	.L3024-.L2949
	.long	.L3023-.L2949
	.long	.L3022-.L2949
	.long	.L3021-.L2949
	.long	.L3020-.L2949
	.long	.L3019-.L2949
	.long	.L3018-.L2949
	.long	.L3017-.L2949
	.long	.L3017-.L2949
	.long	.L3016-.L2949
	.long	.L3015-.L2949
	.long	.L3014-.L2949
	.long	.L3013-.L2949
	.long	.L3012-.L2949
	.long	.L3011-.L2949
	.long	.L3010-.L2949
	.long	.L3009-.L2949
	.long	.L3008-.L2949
	.long	.L3007-.L2949
	.long	.L3006-.L2949
	.long	.L3005-.L2949
	.long	.L3004-.L2949
	.long	.L3003-.L2949
	.long	.L3002-.L2949
	.long	.L3002-.L2949
	.long	.L3001-.L2949
	.long	.L3000-.L2949
	.long	.L2999-.L2949
	.long	.L2998-.L2949
	.long	.L2997-.L2949
	.long	.L2948-.L2949
	.long	.L2996-.L2949
	.long	.L2995-.L2949
	.long	.L2994-.L2949
	.long	.L2993-.L2949
	.long	.L2948-.L2949
	.long	.L2992-.L2949
	.long	.L2991-.L2949
	.long	.L2990-.L2949
	.long	.L2989-.L2949
	.long	.L2948-.L2949
	.long	.L2988-.L2949
	.long	.L2948-.L2949
	.long	.L2987-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2986-.L2949
	.long	.L2985-.L2949
	.long	.L2984-.L2949
	.long	.L2983-.L2949
	.long	.L2982-.L2949
	.long	.L2981-.L2949
	.long	.L2980-.L2949
	.long	.L2979-.L2949
	.long	.L2978-.L2949
	.long	.L2977-.L2949
	.long	.L2976-.L2949
	.long	.L2975-.L2949
	.long	.L2974-.L2949
	.long	.L2973-.L2949
	.long	.L2948-.L2949
	.long	.L2972-.L2949
	.long	.L2971-.L2949
	.long	.L2970-.L2949
	.long	.L2969-.L2949
	.long	.L2968-.L2949
	.long	.L2967-.L2949
	.long	.L2966-.L2949
	.long	.L2965-.L2949
	.long	.L2964-.L2949
	.long	.L2963-.L2949
	.long	.L2962-.L2949
	.long	.L2961-.L2949
	.long	.L2961-.L2949
	.long	.L2960-.L2949
	.long	.L2959-.L2949
	.long	.L2958-.L2949
	.long	.L2957-.L2949
	.long	.L2956-.L2949
	.long	.L2955-.L2949
	.long	.L2954-.L2949
	.long	.L2948-.L2949
	.long	.L2953-.L2949
	.long	.L2931-.L2949
	.long	.L2952-.L2949
	.long	.L2951-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2948-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2950-.L2949
	.long	.L2950-.L2949
	.long	.L2950-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.long	.L2931-.L2949
	.section	.text._ZN2v88internal8compiler22RepresentationSelector9VisitNodeEPNS1_4NodeENS1_10TruncationEPNS1_18SimplifiedLoweringE,"axG",@progbits,_ZN2v88internal8compiler22RepresentationSelector9VisitNodeEPNS1_4NodeENS1_10TruncationEPNS1_18SimplifiedLoweringE,comdat
	.p2align 4,,10
	.p2align 3
.L4107:
	movl	32(%rdi), %r9d
	testl	%r9d, %r9d
	jle	.L2930
	cmpb	$0, 36(%rdi)
	je	.L2930
	cmpw	$59, %ax
	je	.L2931
	movl	20(%r14), %edx
	movq	32(%r12), %r8
	andl	$16777215, %edx
	leaq	(%rdx,%rdx,4), %rdx
	leaq	(%r8,%rdx,8), %rdx
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L2932
	movq	8(%r14), %rdx
.L2932:
	cmpq	$1, %rdx
	jne	.L2930
	movl	40(%rdi), %edi
	testl	%edi, %edi
	jne	.L2933
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, -6016(%rbp)
.L2934:
	movq	(%r12), %rax
	movq	(%rax), %r9
	movq	8(%rax), %rdi
	movq	%r9, -6024(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder11UnreachableEv@PLT
	movq	-6024(%rbp), %r9
	xorl	%r8d, %r8d
	leaq	-80(%rbp), %rcx
	movq	%r14, %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movhps	-6016(%rbp), %xmm0
	movq	%r9, %rdi
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	24(%r14), %r8
	movq	%rax, -6040(%rbp)
	testq	%r8, %r8
	je	.L2935
	movq	(%r8), %rdx
	testq	%rax, %rax
	je	.L2942
	.p2align 4,,10
	.p2align 3
.L2936:
	movl	16(%r8), %esi
	movq	%rdx, -6016(%rbp)
	movq	%r8, -6032(%rbp)
	movl	%esi, %ecx
	shrl	%ecx
	leaq	3(%rcx,%rcx,2), %rax
	leaq	(%r8,%rax,8), %rax
	leaq	32(%rax), %rdi
	addq	$16, %rax
	andl	$1, %esi
	cmovne	%rdi, %rax
	movq	%r8, %rdi
	leaq	(%rax,%rcx,8), %rcx
	movq	%rcx, %rsi
	movq	%rcx, -6024(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties12IsEffectEdgeENS1_4EdgeE@PLT
	movq	-6016(%rbp), %rdx
	testb	%al, %al
	je	.L2945
	movq	-6032(%rbp), %r8
	movq	-6024(%rbp), %rcx
	movl	16(%r8), %esi
	movl	%esi, %eax
	shrl	%eax
	andl	$1, %esi
	leaq	3(%rax,%rax,2), %rax
	leaq	(%r8,%rax,8), %rax
	jne	.L2946
	movq	(%rax), %rax
.L2946:
	movq	-6040(%rbp), %rsi
	cmpq	%rax, %rsi
	je	.L2945
	movq	(%rax), %rax
	cmpw	$7, 16(%rax)
	je	.L2945
	movq	(%rcx), %rdi
	cmpq	%rdi, %rsi
	je	.L2945
	testq	%rdi, %rdi
	je	.L2947
	movq	%r8, %rsi
	movq	%rcx, -6032(%rbp)
	movq	%rdx, -6024(%rbp)
	movq	%r8, -6016(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-6032(%rbp), %rcx
	movq	-6024(%rbp), %rdx
	movq	-6016(%rbp), %r8
.L2947:
	movq	-6040(%rbp), %rax
	movq	%r8, %rsi
	movq	%rdx, -6016(%rbp)
	movq	%rax, (%rcx)
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movq	-6016(%rbp), %rdx
.L2945:
	testq	%rdx, %rdx
	je	.L2935
	movq	%rdx, %r8
	movq	(%rdx), %rdx
	jmp	.L2936
.L2948:
	movl	20(%r14), %ecx
	movq	8(%rdi), %rdx
	movzwl	%ax, %esi
	leaq	.LC30(%rip), %rdi
	xorl	%eax, %eax
	andl	$16777215, %ecx
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2931:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector11VisitInputsEPNS1_4NodeE
	movl	20(%r14), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rdx
	movq	32(%r12), %rax
	leaq	(%rax,%rdx,8), %rdx
	movl	120(%r12), %eax
	testl	%eax, %eax
	je	.L3760
	cmpl	$1, %eax
	jne	.L2927
	movb	$8, 1(%rdx)
	.p2align 4,,10
	.p2align 3
.L2927:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4108
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3550:
	.cfi_restore_state
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector11VisitUnusedEPNS1_4NodeE
	jmp	.L2927
	.p2align 4,,10
	.p2align 3
.L2942:
	movl	16(%r8), %esi
	movq	%rdx, -6016(%rbp)
	movq	%r8, -6032(%rbp)
	movl	%esi, %ecx
	shrl	%ecx
	leaq	3(%rcx,%rcx,2), %rax
	leaq	(%r8,%rax,8), %rax
	leaq	16(%rax), %rdi
	addq	$32, %rax
	andl	$1, %esi
	cmove	%rdi, %rax
	movq	%r8, %rdi
	leaq	(%rax,%rcx,8), %r9
	movq	%r9, %rsi
	movq	%r9, -6024(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties12IsEffectEdgeENS1_4EdgeE@PLT
	movq	-6016(%rbp), %rdx
	testb	%al, %al
	je	.L2939
	movq	-6032(%rbp), %r8
	movq	-6024(%rbp), %r9
	movl	16(%r8), %ecx
	movl	%ecx, %eax
	shrl	%eax
	andl	$1, %ecx
	leaq	3(%rax,%rax,2), %rax
	leaq	(%r8,%rax,8), %rax
	jne	.L2941
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L2939
.L2941:
	movq	(%rax), %rax
	cmpw	$7, 16(%rax)
	je	.L2939
	movq	(%r9), %rdi
	movq	%r9, -6016(%rbp)
	testq	%rdi, %rdi
	je	.L2939
	movq	%r8, %rsi
	movq	%rdx, -6024(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-6016(%rbp), %r9
	movq	-6024(%rbp), %rdx
	movq	$0, (%r9)
.L2939:
	testq	%rdx, %rdx
	je	.L2935
	movq	%rdx, %r8
	movq	(%rdx), %rdx
	jmp	.L2942
	.p2align 4,,10
	.p2align 3
.L3040:
	movb	$13, -4752(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$4, -4748(%rbp)
	movl	$13, %edx
	movl	$1, -4744(%rbp)
	movb	$0, -4740(%rbp)
	movq	$0, -4736(%rbp)
	movl	$-1, -4728(%rbp)
	pushq	-4728(%rbp)
	pushq	-4736(%rbp)
	pushq	-4744(%rbp)
	pushq	-4752(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	(%r14), %rax
	movq	128(%r12), %rdi
	movzwl	16(%rax), %esi
	call	_ZN2v88internal8compiler21RepresentationChanger18Float64OperatorForENS1_8IrOpcode5ValueE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	jmp	.L2927
.L3039:
	movzbl	23(%r14), %eax
	movq	32(%r14), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L3505
	movq	16(%rdx), %rdx
.L3505:
	movl	20(%rdx), %eax
	movq	32(%r12), %rdi
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rax
	leaq	(%rdi,%rax,8), %rax
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L3506
	movq	8(%rdx), %rax
.L3506:
	movq	%rax, -144(%rbp)
	movl	-6008(%rbp), %eax
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$13, -4848(%rbp)
	movl	$4294967295, %ecx
	movl	$13, %edx
	movb	$4, -4844(%rbp)
	movl	%eax, -4840(%rbp)
	movb	$0, -4836(%rbp)
	movq	$0, -4832(%rbp)
	movl	$-1, -4824(%rbp)
	pushq	-4824(%rbp)
	pushq	-4832(%rbp)
	pushq	-4840(%rbp)
	pushq	-4848(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	344(%r12), %rax
	movq	336(%rax), %rsi
	cmpq	-144(%rbp), %rsi
	je	.L3508
	leaq	-144(%rbp), %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L3508
	movq	(%r14), %rax
	movzwl	16(%rax), %esi
	cmpl	$182, %esi
	je	.L4109
	movq	128(%r12), %rdi
	call	_ZN2v88internal8compiler21RepresentationChanger18Float64OperatorForENS1_8IrOpcode5ValueE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	jmp	.L2927
	.p2align 4,,10
	.p2align 3
.L3062:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector26VisitWord32TruncatingBinopEPNS1_4NodeE
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	(%r14), %rax
	movq	128(%r12), %rdi
	movzwl	16(%rax), %esi
	call	_ZN2v88internal8compiler21RepresentationChanger16Int32OperatorForENS1_8IrOpcode5ValueE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	jmp	.L2927
.L3067:
	movb	$8, -144(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$5, -140(%rbp)
	movl	$7, %edx
	movl	$1, -136(%rbp)
	movb	$0, -132(%rbp)
	movq	$0, -128(%rbp)
	movl	$-1, -120(%rbp)
	pushq	-120(%rbp)
	pushq	-128(%rbp)
	pushq	-136(%rbp)
	pushq	-144(%rbp)
	pushq	-120(%rbp)
	pushq	-128(%rbp)
	pushq	-136(%rbp)
	pushq	-144(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE
	addq	$64, %rsp
	jmp	.L2927
.L3050:
	movzbl	23(%r14), %eax
	movq	32(%r14), %rdx
	leaq	32(%r14), %rbx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L3361
	movq	8(%rdx), %rax
	movq	%rax, -176(%rbp)
	cmpq	$8396767, %rax
	je	.L3362
.L3366:
	leaq	-176(%rbp), %rdi
	movl	$8396767, %esi
	leaq	-144(%rbp), %r13
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L3364
	movzbl	23(%r14), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L4110
.L3362:
	leaq	8(%rbx), %rax
.L3367:
	movq	(%rax), %rax
	movq	8(%rax), %rax
	movq	%rax, -144(%rbp)
	cmpq	$8396767, %rax
	je	.L3368
	leaq	-144(%rbp), %r13
	movl	$8396767, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L3364
.L3368:
	movb	$4, -144(%rbp)
	movb	$2, -140(%rbp)
	movl	$0, -136(%rbp)
	movb	$0, -132(%rbp)
	movq	$0, -128(%rbp)
	movl	$-1, -120(%rbp)
	pushq	-120(%rbp)
	pushq	-128(%rbp)
	pushq	-136(%rbp)
	pushq	-144(%rbp)
	pushq	-120(%rbp)
	pushq	-128(%rbp)
	pushq	-136(%rbp)
	pushq	-144(%rbp)
.L4085:
	movl	$4294967295, %ecx
	movl	$4, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE
	addq	$64, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	(%r14), %rax
	movq	128(%r12), %rdi
	movzwl	16(%rax), %esi
	call	_ZN2v88internal8compiler21RepresentationChanger16Int32OperatorForENS1_8IrOpcode5ValueE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler22RepresentationSelector14ChangeToPureOpEPNS1_4NodeEPKNS1_8OperatorE
	jmp	.L2927
.L3071:
	movzbl	23(%r14), %eax
	movq	32(%r12), %rcx
	leaq	32(%r14), %rbx
	movq	32(%r14), %rsi
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L3216
	movl	20(%rsi), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rax
	movq	24(%rcx,%rax,8), %rdx
	testq	%rdx, %rdx
	jne	.L4078
	movq	8(%rsi), %rdx
.L4078:
	movq	%rdx, -240(%rbp)
	leaq	8(%rbx), %rax
.L3221:
	movq	(%rax), %rsi
	movl	20(%rsi), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rax
	movq	24(%rcx,%rax,8), %rax
	testq	%rax, %rax
	jne	.L3222
	movq	8(%rsi), %rax
.L3222:
	movq	%rax, -208(%rbp)
	cmpq	$3079, %rdx
	je	.L3223
	leaq	-240(%rbp), %rdi
	movl	$3079, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L3228
	movq	-208(%rbp), %rax
.L3223:
	cmpq	$3079, %rax
	je	.L3227
	leaq	-208(%rbp), %rdi
	movl	$3079, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L3227
.L3228:
	cmpq	$3147, -240(%rbp)
	je	.L3225
	leaq	-240(%rbp), %rdi
	movl	$3147, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L3225
.L3235:
	movq	(%r14), %rdi
	call	_ZN2v88internal8compiler21NumberOperationHintOfEPKNS1_8OperatorE@PLT
	movl	%eax, %esi
	cmpb	$4, %al
	ja	.L2980
	cmpb	$2, %al
	jbe	.L4111
	cmpb	$4, %al
	movb	$13, -144(%rbp)
	movq	%r14, %rsi
	movq	%r12, %rdi
	sete	%al
	movb	$5, -140(%rbp)
	movl	$4294967295, %ecx
	movl	$1, %edx
	addl	$4, %eax
	movq	$0, -128(%rbp)
	movl	$0, -136(%rbp)
	movb	%al, -132(%rbp)
	movl	$-1, -120(%rbp)
	pushq	-120(%rbp)
	pushq	-128(%rbp)
	pushq	-136(%rbp)
	pushq	-144(%rbp)
	pushq	-120(%rbp)
	pushq	-128(%rbp)
	pushq	-136(%rbp)
	pushq	-144(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE
	addq	$64, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	(%r14), %rax
	movq	128(%r12), %rdi
	movzwl	16(%rax), %esi
	call	_ZN2v88internal8compiler21RepresentationChanger18Float64OperatorForENS1_8IrOpcode5ValueE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler22RepresentationSelector14ChangeToPureOpEPNS1_4NodeEPKNS1_8OperatorE
	jmp	.L2927
	.p2align 4,,10
	.p2align 3
.L2950:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector11VisitInputsEPNS1_4NodeE
	movl	$2, %esi
	movl	%r15d, %edi
	call	_ZN2v88internal8compiler10Truncation11LessGeneralENS2_14TruncationKindES3_@PLT
	testb	%al, %al
	je	.L3145
	movl	20(%r14), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rdx
	movq	32(%r12), %rax
	leaq	(%rax,%rdx,8), %rdx
	movl	120(%r12), %eax
	testl	%eax, %eax
	je	.L3146
	cmpl	$1, %eax
	jne	.L3148
	movb	$4, 1(%rdx)
	movl	120(%r12), %eax
.L3148:
	cmpl	$2, %eax
	jne	.L2927
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18SimplifiedLowering38DoJSToNumberOrNumericTruncatesToWord32EPNS1_4NodeEPNS1_22RepresentationSelectorE
	jmp	.L2927
.L3055:
	movb	$13, -144(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$4, -140(%rbp)
	movl	$13, %edx
	movl	$1, -136(%rbp)
	movb	$0, -132(%rbp)
	movq	$0, -128(%rbp)
	movl	$-1, -120(%rbp)
	pushq	-120(%rbp)
	pushq	-128(%rbp)
	pushq	-136(%rbp)
	pushq	-144(%rbp)
	pushq	-120(%rbp)
	pushq	-128(%rbp)
	pushq	-136(%rbp)
	pushq	-144(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE
	addq	$64, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	(%r14), %rax
	movq	128(%r12), %rdi
	movzwl	16(%rax), %esi
	call	_ZN2v88internal8compiler21RepresentationChanger18Float64OperatorForENS1_8IrOpcode5ValueE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	jmp	.L2927
.L2961:
	movb	$5, -816(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$5, -812(%rbp)
	movl	$7, %edx
	movl	$1, -808(%rbp)
	movb	$0, -804(%rbp)
	movq	$0, -800(%rbp)
	movl	$-1, -792(%rbp)
	pushq	-792(%rbp)
	pushq	-800(%rbp)
	pushq	-808(%rbp)
	pushq	-816(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	jmp	.L2927
.L3002:
	movb	$8, -144(%rbp)
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$5, -140(%rbp)
	movl	$4294967295, %ecx
	movl	$1, -136(%rbp)
	movb	$0, -132(%rbp)
	movq	$0, -128(%rbp)
	movl	$-1, -120(%rbp)
	pushq	-120(%rbp)
	pushq	-128(%rbp)
	pushq	-136(%rbp)
	pushq	-144(%rbp)
	pushq	-120(%rbp)
	pushq	-128(%rbp)
	pushq	-136(%rbp)
	pushq	-144(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE
	addq	$64, %rsp
	jmp	.L2927
.L3066:
	movl	20(%r14), %ecx
	movq	32(%r14), %rsi
	leaq	32(%r14), %rbx
	movl	%ecx, %eax
	shrl	$24, %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L3243
	movq	16(%rsi), %rsi
.L3243:
	movl	20(%rsi), %edx
	movq	32(%r12), %rdi
	andl	$16777215, %edx
	leaq	(%rdx,%rdx,4), %rdx
	movq	24(%rdi,%rdx,8), %rdx
	testq	%rdx, %rdx
	jne	.L3244
	movq	8(%rsi), %rdx
.L3244:
	movq	344(%r12), %rsi
	movq	%rdx, -240(%rbp)
	movq	392(%rsi), %rsi
	cmpq	%rsi, %rdx
	je	.L3245
	leaq	-240(%rbp), %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L3246
	movq	344(%r12), %rax
	movl	20(%r14), %ecx
	movq	32(%r12), %rdi
	movq	392(%rax), %rsi
	movl	%ecx, %eax
	shrl	$24, %eax
	andl	$15, %eax
.L3245:
	addq	$8, %rbx
	cmpl	$15, %eax
	jne	.L3248
	movq	32(%r14), %rax
	leaq	24(%rax), %rbx
.L3248:
	movq	(%rbx), %rdx
	movl	20(%rdx), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rax
	movq	24(%rdi,%rax,8), %rax
	testq	%rax, %rax
	jne	.L3249
	movq	8(%rdx), %rax
.L3249:
	movq	%rax, -208(%rbp)
	cmpq	%rsi, %rax
	je	.L3250
	leaq	-208(%rbp), %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L3246
	movl	20(%r14), %ecx
	movq	32(%r12), %rdi
.L3250:
	andl	$16777215, %ecx
	leaq	(%rcx,%rcx,4), %rax
	movq	24(%rdi,%rax,8), %rax
	testq	%rax, %rax
	jne	.L3251
	movq	8(%r14), %rax
.L3251:
	movq	%rax, -176(%rbp)
	cmpq	$1099, %rax
	je	.L3254
	leaq	-176(%rbp), %rdi
	movl	$1099, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L3254
	movl	20(%r14), %eax
	andl	$16777215, %eax
	imulq	$40, %rax, %rax
	addq	32(%r12), %rax
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L3255
	movq	8(%r14), %rax
.L3255:
	movq	%rax, -144(%rbp)
	cmpq	$1031, %rax
	je	.L3254
	leaq	-144(%rbp), %rdi
	movl	$1031, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L3254
	movl	$2, %esi
	movl	%r15d, %edi
	call	_ZN2v88internal8compiler10Truncation11LessGeneralENS2_14TruncationKindES3_@PLT
	testb	%al, %al
	jne	.L3254
.L3246:
	movq	(%r12), %rax
	movq	16(%rax), %rax
	cmpb	$5, 16(%rax)
	je	.L3256
.L3258:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector17VisitFloat64BinopEPNS1_4NodeE
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	(%r14), %rax
	movq	128(%r12), %rdi
	movzwl	16(%rax), %esi
	call	_ZN2v88internal8compiler21RepresentationChanger18Float64OperatorForENS1_8IrOpcode5ValueE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler22RepresentationSelector14ChangeToPureOpEPNS1_4NodeEPKNS1_8OperatorE
	jmp	.L2927
	.p2align 4,,10
	.p2align 3
.L3072:
	movzbl	23(%r14), %edx
	movq	32(%r12), %rcx
	leaq	32(%r14), %rax
	movq	32(%r14), %rsi
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L4100
	leaq	16(%rsi), %rax
	movq	16(%rsi), %rsi
.L4100:
	movl	20(%rsi), %edx
	andl	$16777215, %edx
	leaq	(%rdx,%rdx,4), %rdx
	movq	24(%rcx,%rdx,8), %rdx
	testq	%rdx, %rdx
	jne	.L4077
	movq	8(%rsi), %rdx
.L4077:
	addq	$8, %rax
	movq	%rdx, -208(%rbp)
	movq	(%rax), %rsi
	movl	20(%rsi), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rax
	movq	24(%rcx,%rax,8), %rax
	testq	%rax, %rax
	jne	.L3202
	movq	8(%rsi), %rax
.L3202:
	movq	%rax, -176(%rbp)
	cmpq	$3079, %rdx
	je	.L3203
	leaq	-208(%rbp), %rdi
	movl	$3079, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L3208
	movq	-176(%rbp), %rax
.L3203:
	cmpq	$3079, %rax
	je	.L3207
	leaq	-176(%rbp), %rdi
	movl	$3079, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L3207
.L3208:
	cmpq	$3147, -208(%rbp)
	je	.L3205
	leaq	-208(%rbp), %rdi
	movl	$3147, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L3205
.L3215:
	movb	$13, -144(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$4, -140(%rbp)
	movl	$1, %edx
	movl	$0, -136(%rbp)
	movb	$0, -132(%rbp)
	movq	$0, -128(%rbp)
	movl	$-1, -120(%rbp)
	pushq	-120(%rbp)
	pushq	-128(%rbp)
	pushq	-136(%rbp)
	pushq	-144(%rbp)
	pushq	-120(%rbp)
	pushq	-128(%rbp)
	pushq	-136(%rbp)
	pushq	-144(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE
	addq	$64, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	(%r14), %rax
	movq	128(%r12), %rdi
	movzwl	16(%rax), %esi
	call	_ZN2v88internal8compiler21RepresentationChanger18Float64OperatorForENS1_8IrOpcode5ValueE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	jmp	.L2927
	.p2align 4,,10
	.p2align 3
.L3017:
	movb	$8, -3440(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$5, -3436(%rbp)
	movl	$7, %edx
	movl	$1, -3432(%rbp)
	movb	$0, -3428(%rbp)
	movq	$0, -3424(%rbp)
	movl	$-1, -3416(%rbp)
	pushq	-3416(%rbp)
	pushq	-3424(%rbp)
	pushq	-3432(%rbp)
	pushq	-3440(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	jmp	.L2927
.L2958:
	movl	20(%r14), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rdx
	movq	32(%r12), %rax
	leaq	(%rax,%rdx,8), %rdx
	movl	120(%r12), %eax
	testl	%eax, %eax
	je	.L3115
	cmpl	$1, %eax
	jne	.L2927
	movb	$7, 1(%rdx)
	jmp	.L2927
.L3054:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector26VisitSpeculativeAdditiveOpEPNS1_4NodeENS1_10TruncationEPNS1_18SimplifiedLoweringE
	jmp	.L2927
.L3046:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector33VisitSpeculativeIntegerAdditiveOpEPNS1_4NodeENS1_10TruncationEPNS1_18SimplifiedLoweringE
	jmp	.L2927
.L3019:
	movb	$8, -3664(%rbp)
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$5, -3660(%rbp)
	movl	$1, -3656(%rbp)
	movb	$0, -3652(%rbp)
	movq	$0, -3648(%rbp)
	movl	$-1, -3640(%rbp)
	pushq	-3640(%rbp)
	pushq	-3648(%rbp)
	pushq	-3656(%rbp)
	pushq	-3664(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12ProcessInputEPNS1_4NodeEiNS1_7UseInfoE
	addq	$32, %rsp
	movq	%r14, %rsi
	movb	$8, -3632(%rbp)
	movb	$5, -3628(%rbp)
	movl	$1, %edx
	movq	%r12, %rdi
	movl	$1, -3624(%rbp)
	movb	$0, -3620(%rbp)
	movq	$0, -3616(%rbp)
	movl	$-1, -3608(%rbp)
	pushq	-3608(%rbp)
	pushq	-3616(%rbp)
	pushq	-3624(%rbp)
	pushq	-3632(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12ProcessInputEPNS1_4NodeEiNS1_7UseInfoE
	addq	$32, %rsp
	movq	%r14, %rsi
	movb	$6, -3600(%rbp)
	movb	$5, -3596(%rbp)
	movl	$2, %edx
	movq	%r12, %rdi
	movl	$1, -3592(%rbp)
	movb	$0, -3588(%rbp)
	movq	$0, -3584(%rbp)
	movl	$-1, -3576(%rbp)
	pushq	-3576(%rbp)
	pushq	-3584(%rbp)
	pushq	-3592(%rbp)
	pushq	-3600(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12ProcessInputEPNS1_4NodeEiNS1_7UseInfoE
	movl	20(%r14), %eax
	addq	$32, %rsp
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rdx
	movq	32(%r12), %rax
	leaq	(%rax,%rdx,8), %rdx
	movl	120(%r12), %eax
	testl	%eax, %eax
	je	.L3558
	cmpl	$1, %eax
	jne	.L2927
	movb	$6, 1(%rdx)
	jmp	.L2927
.L3016:
	movb	$8, -3536(%rbp)
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$5, -3532(%rbp)
	movl	$1, -3528(%rbp)
	movb	$0, -3524(%rbp)
	movq	$0, -3520(%rbp)
	movl	$-1, -3512(%rbp)
	pushq	-3512(%rbp)
	pushq	-3520(%rbp)
	pushq	-3528(%rbp)
	pushq	-3536(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12ProcessInputEPNS1_4NodeEiNS1_7UseInfoE
	addq	$32, %rsp
	movq	%r14, %rsi
	movb	$4, -3504(%rbp)
	movb	$2, -3500(%rbp)
	movl	$1, %edx
	movq	%r12, %rdi
	movl	$0, -3496(%rbp)
	movb	$0, -3492(%rbp)
	movq	$0, -3488(%rbp)
	movl	$-1, -3480(%rbp)
	pushq	-3480(%rbp)
	pushq	-3488(%rbp)
	pushq	-3496(%rbp)
	pushq	-3504(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12ProcessInputEPNS1_4NodeEiNS1_7UseInfoE
	addq	$32, %rsp
	movq	%r14, %rsi
	movb	$4, -3472(%rbp)
	movb	$2, -3468(%rbp)
	movl	$2, %edx
	movq	%r12, %rdi
	movl	$0, -3464(%rbp)
	movb	$0, -3460(%rbp)
	movq	$0, -3456(%rbp)
	movl	$-1, -3448(%rbp)
	pushq	-3448(%rbp)
	pushq	-3456(%rbp)
	pushq	-3464(%rbp)
	pushq	-3472(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12ProcessInputEPNS1_4NodeEiNS1_7UseInfoE
	movl	$3, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	addq	$32, %rsp
	call	_ZN2v88internal8compiler22RepresentationSelector22ProcessRemainingInputsEPNS1_4NodeEi
	movl	20(%r14), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rdx
	movq	32(%r12), %rax
	leaq	(%rax,%rdx,8), %rdx
	movl	120(%r12), %eax
	testl	%eax, %eax
	je	.L3560
	cmpl	$1, %eax
	jne	.L2927
	movb	$7, 1(%rdx)
	jmp	.L2927
.L3058:
	movb	$4, -4944(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$2, -4940(%rbp)
	movl	$4, %edx
	movl	$0, -4936(%rbp)
	movb	$0, -4932(%rbp)
	movq	$0, -4928(%rbp)
	movl	$-1, -4920(%rbp)
	movb	$4, -4912(%rbp)
	pushq	-4920(%rbp)
	movb	$2, -4908(%rbp)
	pushq	-4928(%rbp)
	movl	$0, -4904(%rbp)
	pushq	-4936(%rbp)
	movb	$0, -4900(%rbp)
	pushq	-4944(%rbp)
	movq	$0, -4896(%rbp)
	movl	$-1, -4888(%rbp)
	pushq	-4888(%rbp)
	pushq	-4896(%rbp)
	pushq	-4904(%rbp)
	pushq	-4912(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE
	addq	$64, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	(%r14), %rax
	movq	128(%r12), %rdi
	movzwl	16(%rax), %esi
	call	_ZN2v88internal8compiler21RepresentationChanger17Uint32OperatorForENS1_8IrOpcode5ValueE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	jmp	.L2927
.L3083:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector15VisitFrameStateEPNS1_4NodeE
	jmp	.L2927
.L3018:
	movb	$8, -3568(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$5, -3564(%rbp)
	movl	$4, %edx
	movl	$1, -3560(%rbp)
	movb	$0, -3556(%rbp)
	movq	$0, -3552(%rbp)
	movl	$-1, -3544(%rbp)
	pushq	-3544(%rbp)
	pushq	-3552(%rbp)
	pushq	-3560(%rbp)
	pushq	-3568(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	jmp	.L2927
.L3082:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector16VisitStateValuesEPNS1_4NodeE
	jmp	.L2927
.L2995:
	testb	%r15b, %r15b
	je	.L3550
	call	_ZN2v88internal8compiler13FieldAccessOfEPKNS1_8OperatorE@PLT
	cmpb	$1, (%rax)
	movzbl	32(%rax), %edx
	je	.L4112
	movb	$5, -3024(%rbp)
.L4091:
	movb	$5, -3020(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$1, -3016(%rbp)
	movb	$0, -3012(%rbp)
	movq	$0, -3008(%rbp)
	movl	$-1, -3000(%rbp)
	pushq	-3000(%rbp)
	pushq	-3008(%rbp)
	pushq	-3016(%rbp)
	pushq	-3024(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	jmp	.L2927
.L2972:
	movzbl	23(%r14), %eax
	movq	32(%r14), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L3691
	movq	16(%rdx), %rdx
.L3691:
	movq	8(%rdx), %rax
	movq	%rax, -144(%rbp)
	cmpq	$4097, %rax
	je	.L3692
	leaq	-144(%rbp), %r15
	movl	$4097, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L3692
	movl	$4097, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	je	.L4113
	cmpq	$7263, -144(%rbp)
	je	.L3698
	movl	$7263, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L3698
	movb	$8, -944(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$5, -940(%rbp)
	movl	$1, %edx
	movl	$1, -936(%rbp)
	movb	$0, -932(%rbp)
	movq	$0, -928(%rbp)
	movl	$-1, -920(%rbp)
	pushq	-920(%rbp)
	pushq	-928(%rbp)
	pushq	-936(%rbp)
	pushq	-944(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	jmp	.L2927
	.p2align 4,,10
	.p2align 3
.L2979:
	movzbl	23(%r14), %eax
	movq	32(%r14), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L3675
	movq	16(%rdx), %rdx
.L3675:
	movq	8(%rdx), %rax
	movq	344(%r12), %rdx
	movq	%rax, -144(%rbp)
	movq	384(%rdx), %rsi
	cmpq	%rsi, %rax
	je	.L3676
	leaq	-144(%rbp), %r15
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L3676
	movl	$7263, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	je	.L4114
	cmpq	$7263, -144(%rbp)
	je	.L3682
	movl	$7263, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L3682
	movb	$8, -1232(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$5, -1228(%rbp)
	movl	$1, %edx
	movl	$1, -1224(%rbp)
	movb	$0, -1220(%rbp)
	movq	$0, -1216(%rbp)
	movl	$-1, -1208(%rbp)
	pushq	-1208(%rbp)
	pushq	-1216(%rbp)
	pushq	-1224(%rbp)
	pushq	-1232(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	jmp	.L2927
	.p2align 4,,10
	.p2align 3
.L2982:
	movb	$13, -1200(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$4, -1196(%rbp)
	movl	$1, %edx
	movl	$1, -1192(%rbp)
	movb	$0, -1188(%rbp)
	movq	$0, -1184(%rbp)
	movl	$-1, -1176(%rbp)
	pushq	-1176(%rbp)
	pushq	-1184(%rbp)
	pushq	-1192(%rbp)
	pushq	-1200(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	jmp	.L2927
.L2983:
	movzbl	23(%r14), %eax
	movq	32(%r14), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L3659
	movq	16(%rdx), %rdx
.L3659:
	movq	8(%rdx), %rax
	movq	344(%r12), %rdx
	movq	%rax, -144(%rbp)
	movq	384(%rdx), %rsi
	cmpq	%rsi, %rax
	je	.L3660
	leaq	-144(%rbp), %r15
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L3660
	movl	$7263, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	je	.L4115
	cmpq	$7263, -144(%rbp)
	je	.L3666
	movl	$7263, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L3666
	movb	$8, -1520(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$5, -1516(%rbp)
	movl	$1, %edx
	movl	$1, -1512(%rbp)
	movb	$0, -1508(%rbp)
	movq	$0, -1504(%rbp)
	movl	$-1, -1496(%rbp)
	pushq	-1496(%rbp)
	pushq	-1504(%rbp)
	pushq	-1512(%rbp)
	pushq	-1520(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	jmp	.L2927
	.p2align 4,,10
	.p2align 3
.L2975:
	movb	$8, -1648(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$5, -1644(%rbp)
	movl	$1, %edx
	movl	$1, -1640(%rbp)
	movb	$0, -1636(%rbp)
	movq	$0, -1632(%rbp)
	movl	$-1, -1624(%rbp)
	pushq	-1624(%rbp)
	pushq	-1632(%rbp)
	pushq	-1640(%rbp)
	pushq	-1648(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	jmp	.L2927
.L2977:
	movq	%r13, %rcx
	movl	$134217729, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector13VisitObjectIsEPNS1_4NodeENS1_4TypeEPNS1_18SimplifiedLoweringE
	jmp	.L2927
.L2978:
	movb	$8, -1680(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$5, -1676(%rbp)
	movl	$1, %edx
	movl	$1, -1672(%rbp)
	movb	$0, -1668(%rbp)
	movq	$0, -1664(%rbp)
	movl	$-1, -1656(%rbp)
	pushq	-1656(%rbp)
	pushq	-1664(%rbp)
	pushq	-1672(%rbp)
	pushq	-1680(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	jmp	.L2927
.L2984:
	movb	$13, -1488(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$4, -1484(%rbp)
	movl	$1, %edx
	movl	$1, -1480(%rbp)
	movb	$0, -1476(%rbp)
	movq	$0, -1472(%rbp)
	movl	$-1, -1464(%rbp)
	pushq	-1464(%rbp)
	pushq	-1472(%rbp)
	pushq	-1480(%rbp)
	pushq	-1488(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	jmp	.L2927
.L2985:
	movb	$13, -2768(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$4, -2764(%rbp)
	movl	$1, %edx
	movl	$1, -2760(%rbp)
	movb	$0, -2756(%rbp)
	movq	$0, -2752(%rbp)
	movl	$-1, -2744(%rbp)
	pushq	-2744(%rbp)
	pushq	-2752(%rbp)
	pushq	-2760(%rbp)
	pushq	-2768(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	jmp	.L2927
.L2986:
	movl	$1, %esi
	movl	%r15d, %edi
	call	_ZN2v88internal8compiler10Truncation11LessGeneralENS2_14TruncationKindES3_@PLT
	testb	%al, %al
	je	.L3138
	movb	$1, -5840(%rbp)
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$1, -5836(%rbp)
	movl	$0, -5832(%rbp)
	movb	$0, -5828(%rbp)
	movq	$0, -5824(%rbp)
	movl	$-1, -5816(%rbp)
	pushq	-5816(%rbp)
	pushq	-5824(%rbp)
	pushq	-5832(%rbp)
	pushq	-5840(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12ProcessInputEPNS1_4NodeEiNS1_7UseInfoE
	movl	20(%r14), %eax
	addq	$32, %rsp
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rdx
	movq	32(%r12), %rax
	leaq	(%rax,%rdx,8), %rdx
	movl	120(%r12), %eax
	testl	%eax, %eax
	je	.L3139
	cmpl	$1, %eax
	jne	.L3141
	movb	$1, 1(%rdx)
	movl	120(%r12), %eax
.L3141:
	cmpl	$2, %eax
	jne	.L2927
	movzbl	23(%r14), %ecx
	movq	32(%r14), %rax
	movq	%r14, %rsi
	movq	%r12, %rdi
	andl	$15, %ecx
	leaq	16(%rax), %rdx
	leaq	32(%r14), %rax
	cmpl	$15, %ecx
	cmove	%rdx, %rax
	movq	(%rax), %rdx
	call	_ZN2v88internal8compiler22RepresentationSelector16DeferReplacementEPNS1_4NodeES4_
	jmp	.L2927
.L2987:
	movzbl	23(%r14), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L3610
	leaq	48(%r14), %rax
.L3611:
	movq	(%rax), %rdx
	movq	32(%r12), %r10
	movl	20(%rdx), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rax
	leaq	(%r10,%rax,8), %rax
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L3612
	movq	8(%rdx), %rax
.L3612:
	movb	$8, -2736(%rbp)
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$5, -2732(%rbp)
	movl	$1, -2728(%rbp)
	movb	$0, -2724(%rbp)
	movq	$0, -2720(%rbp)
	movl	$-1, -2712(%rbp)
	pushq	-2712(%rbp)
	pushq	-2720(%rbp)
	pushq	-2728(%rbp)
	pushq	-2736(%rbp)
	movq	%rax, -144(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12ProcessInputEPNS1_4NodeEiNS1_7UseInfoE
	addq	$32, %rsp
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$5, -2704(%rbp)
	movl	$1, %edx
	movb	$5, -2700(%rbp)
	movl	$1, -2696(%rbp)
	movb	$0, -2692(%rbp)
	movq	$0, -2688(%rbp)
	movl	$-1, -2680(%rbp)
	pushq	-2680(%rbp)
	pushq	-2688(%rbp)
	pushq	-2696(%rbp)
	pushq	-2704(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12ProcessInputEPNS1_4NodeEiNS1_7UseInfoE
	addq	$32, %rsp
	call	_ZN2v88internal8compiler10BitsetType11SignedSmallEv@PLT
	movl	%eax, %esi
	orl	$1, %esi
	cmpq	%rsi, -144(%rbp)
	je	.L3613
	leaq	-144(%rbp), %r13
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L3613
	cmpq	$7263, -144(%rbp)
	je	.L3614
	movl	$7263, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L3614
	cmpq	$268395425, -144(%rbp)
	je	.L3618
	movl	$268395425, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L3618
	movb	$8, -2576(%rbp)
	movl	$2, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$5, -2572(%rbp)
	movl	$1, -2568(%rbp)
	movb	$0, -2564(%rbp)
	movq	$0, -2560(%rbp)
	movl	$-1, -2552(%rbp)
	pushq	-2552(%rbp)
	pushq	-2560(%rbp)
	pushq	-2568(%rbp)
	pushq	-2576(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12ProcessInputEPNS1_4NodeEiNS1_7UseInfoE
	addq	$32, %rsp
.L3617:
	movl	$3, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector22ProcessRemainingInputsEPNS1_4NodeEi
	movl	20(%r14), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rdx
	movq	32(%r12), %rax
	leaq	(%rax,%rdx,8), %rdx
	movl	120(%r12), %eax
	testl	%eax, %eax
	je	.L3621
	cmpl	$1, %eax
	jne	.L2927
	movb	$0, 1(%rdx)
	jmp	.L2927
.L2981:
	movzbl	23(%r14), %eax
	movq	32(%r14), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L3667
	movq	16(%rdx), %rdx
.L3667:
	movq	8(%rdx), %rax
	movq	344(%r12), %rdx
	movq	%rax, -144(%rbp)
	movq	384(%rdx), %rsi
	cmpq	%rsi, %rax
	je	.L3668
	leaq	-144(%rbp), %r15
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L3668
	movl	$7263, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	je	.L4116
	cmpq	$7263, -144(%rbp)
	je	.L3674
	movl	$7263, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L3674
	movb	$8, -1360(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$5, -1356(%rbp)
	movl	$1, %edx
	movl	$1, -1352(%rbp)
	movb	$0, -1348(%rbp)
	movq	$0, -1344(%rbp)
	movl	$-1, -1336(%rbp)
	pushq	-1336(%rbp)
	pushq	-1344(%rbp)
	pushq	-1352(%rbp)
	pushq	-1360(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	jmp	.L2927
	.p2align 4,,10
	.p2align 3
.L2973:
	movzbl	23(%r14), %eax
	movq	32(%r14), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L3683
	movq	16(%rdx), %rdx
.L3683:
	movq	8(%rdx), %rax
	movq	%rax, -144(%rbp)
	cmpq	$2049, %rax
	je	.L3684
	leaq	-144(%rbp), %r15
	movl	$2049, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L3684
	movl	$2049, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	je	.L4117
	cmpq	$7263, -144(%rbp)
	je	.L3690
	movl	$7263, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L3690
	movb	$8, -1072(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$5, -1068(%rbp)
	movl	$1, %edx
	movl	$1, -1064(%rbp)
	movb	$0, -1060(%rbp)
	movq	$0, -1056(%rbp)
	movl	$-1, -1048(%rbp)
	pushq	-1048(%rbp)
	pushq	-1056(%rbp)
	pushq	-1064(%rbp)
	pushq	-1072(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	jmp	.L2927
	.p2align 4,,10
	.p2align 3
.L2976:
	movq	%r13, %rcx
	movl	$7143425, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector13VisitObjectIsEPNS1_4NodeENS1_4TypeEPNS1_18SimplifiedLoweringE
	jmp	.L2927
.L2974:
	movq	%r13, %rcx
	movl	$6881281, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector13VisitObjectIsEPNS1_4NodeENS1_4TypeEPNS1_18SimplifiedLoweringE
	jmp	.L2927
.L3015:
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector16VisitCheckBoundsEPNS1_4NodeEPNS1_18SimplifiedLoweringE
	jmp	.L2927
.L3004:
	movzbl	23(%r14), %eax
	movq	32(%r14), %rdx
	leaq	32(%r14), %rbx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L3701
	movq	16(%rdx), %rdx
.L3701:
	movl	20(%rdx), %eax
	movq	32(%r12), %r8
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rax
	leaq	(%r8,%rax,8), %rax
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L3702
	movq	8(%rdx), %rax
.L3702:
	movq	%rax, -176(%rbp)
	call	_ZN2v88internal8compiler28CheckFloat64HoleParametersOfEPKNS1_8OperatorE@PLT
	cmpb	$1, (%rax)
	jne	.L3703
	testb	%r15b, %r15b
	je	.L4118
	movl	$4, %esi
	movl	%r15d, %edi
	call	_ZN2v88internal8compiler10Truncation11LessGeneralENS2_14TruncationKindES3_@PLT
	testb	%al, %al
	jne	.L4119
.L3703:
	movb	$13, -144(%rbp)
	movl	$7263, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$5, -140(%rbp)
	movl	$13, %edx
	movl	$1, -136(%rbp)
	movb	$0, -132(%rbp)
	movq	$0, -128(%rbp)
	movl	$-1, -120(%rbp)
	pushq	-120(%rbp)
	pushq	-128(%rbp)
	pushq	-136(%rbp)
	pushq	-144(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	cmpq	$7263, -176(%rbp)
	je	.L3712
	leaq	-176(%rbp), %rdi
	movl	$7263, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L2927
.L3712:
	movzbl	23(%r14), %edx
	movq	32(%r14), %rax
	movq	%r14, %rsi
	movq	%r12, %rdi
	andl	$15, %edx
	addq	$16, %rax
	cmpl	$15, %edx
	cmove	%rax, %rbx
	movq	(%rbx), %rdx
	call	_ZN2v88internal8compiler22RepresentationSelector16DeferReplacementEPNS1_4NodeES4_
	jmp	.L2927
.L3005:
	call	_ZN2v88internal8compiler10BitsetType11SignedSmallEv@PLT
	leaq	32(%r14), %rdx
	movl	%eax, %esi
	movzbl	23(%r14), %eax
	orl	$1, %esi
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L3562
	movq	32(%r14), %rax
	leaq	16(%rax), %rdx
.L3562:
	movq	(%rdx), %rax
	leaq	-144(%rbp), %rdi
	movq	8(%rax), %rax
	movq	%rax, -144(%rbp)
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L3563
	movb	$8, -3376(%rbp)
	movb	$5, -3372(%rbp)
	movl	$1, -3368(%rbp)
	movb	$0, -3364(%rbp)
	movq	$0, -3360(%rbp)
	movl	$-1, -3352(%rbp)
	pushq	-3352(%rbp)
	pushq	-3360(%rbp)
	pushq	-3368(%rbp)
	pushq	-3376(%rbp)
.L4090:
	movl	$4294967295, %ecx
	movl	$7, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movzbl	23(%r14), %eax
	movq	32(%r14), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L3565
	movq	16(%rdx), %rdx
.L3565:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector16DeferReplacementEPNS1_4NodeES4_
	jmp	.L2927
.L3006:
	call	_ZN2v88internal8compiler17CheckParametersOfEPKNS1_8OperatorE@PLT
	movl	$2, %esi
	movl	%r15d, %edi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler10Truncation11LessGeneralENS2_14TruncationKindES3_@PLT
	movq	(%rbx), %rdx
	testb	%al, %al
	movl	8(%rbx), %eax
	je	.L3572
	movq	%rdx, -3232(%rbp)
	movq	%r14, %rsi
	movl	$4294967295, %ecx
	movl	$4, %edx
	movb	$4, -3248(%rbp)
	movq	%r12, %rdi
	movb	$5, -3244(%rbp)
	movl	$1, -3240(%rbp)
	movb	$1, -3236(%rbp)
	movl	%eax, -3224(%rbp)
	pushq	-3224(%rbp)
	pushq	-3232(%rbp)
	pushq	-3240(%rbp)
	pushq	-3248(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
.L3573:
	cmpl	$2, 120(%r12)
	jne	.L2927
	movzbl	23(%r14), %eax
	movq	32(%r14), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L3574
	movq	16(%rdx), %rdx
.L3574:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector16DeferReplacementEPNS1_4NodeES4_
	jmp	.L2927
.L3007:
	movq	%r13, %rcx
	movl	$8193, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector10VisitCheckEPNS1_4NodeENS1_4TypeEPNS1_18SimplifiedLoweringE
	jmp	.L2927
.L3056:
	movzbl	23(%r14), %edx
	movq	32(%r12), %rcx
	leaq	32(%r14), %rax
	movq	32(%r14), %rsi
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L4106
	leaq	16(%rsi), %rax
	movq	16(%rsi), %rsi
.L4106:
	movl	20(%rsi), %edx
	andl	$16777215, %edx
	leaq	(%rdx,%rdx,4), %rdx
	movq	24(%rcx,%rdx,8), %rdx
	testq	%rdx, %rdx
	jne	.L4089
	movq	8(%rsi), %rdx
.L4089:
	addq	$8, %rax
	movq	%rdx, -208(%rbp)
	movq	(%rax), %rsi
	movl	20(%rsi), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rax
	movq	24(%rcx,%rax,8), %rax
	testq	%rax, %rax
	jne	.L3467
	movq	8(%rsi), %rax
.L3467:
	movq	%rax, -176(%rbp)
	cmpq	$1031, %rdx
	je	.L3468
	leaq	-208(%rbp), %rdi
	movl	$1031, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L3476
	movq	-176(%rbp), %rax
.L3468:
	cmpq	$1031, %rax
	je	.L3475
	leaq	-176(%rbp), %rdi
	movl	$1031, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L3475
.L3476:
	cmpq	$3079, -208(%rbp)
	je	.L3470
	leaq	-208(%rbp), %rdi
	movl	$3079, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L3479
.L3470:
	cmpq	$3079, -176(%rbp)
	je	.L3477
	leaq	-176(%rbp), %rdi
	movl	$3079, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L3479
.L3477:
	movl	-6008(%rbp), %r8d
	testl	%r8d, %r8d
	je	.L3475
.L3479:
	cmpq	$1099, -208(%rbp)
	je	.L3484
	leaq	-208(%rbp), %rdi
	movl	$1099, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L3484
.L3488:
	cmpq	$3147, -208(%rbp)
	je	.L3485
	leaq	-208(%rbp), %rdi
	movl	$3147, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L3491
.L3485:
	cmpq	$3147, -176(%rbp)
	je	.L3489
	leaq	-176(%rbp), %rdi
	movl	$3147, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L3491
.L3489:
	cmpl	$0, -6008(%rbp)
	je	.L3482
.L3491:
	movq	(%r12), %rax
	movq	16(%rax), %rax
	cmpb	$5, 16(%rax)
	jne	.L3498
	movq	344(%r12), %rax
	movq	384(%rax), %rsi
	cmpq	-208(%rbp), %rsi
	je	.L3497
	leaq	-208(%rbp), %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L3498
	movq	344(%r12), %rax
.L3497:
	movq	384(%rax), %rsi
	cmpq	%rsi, -176(%rbp)
	je	.L3495
	leaq	-176(%rbp), %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L3498
.L3495:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector15VisitInt64BinopEPNS1_4NodeE
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	0(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder13Int64LessThanEv@PLT
	movl	$5, %ecx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler18SimplifiedLowering5DoMinEPNS1_4NodeEPKNS1_8OperatorENS0_21MachineRepresentationE
	jmp	.L2927
	.p2align 4,,10
	.p2align 3
.L3059:
	movzbl	23(%r14), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L3390
	leaq	40(%r14), %rax
.L3391:
	movq	(%rax), %rax
	movb	$4, -5296(%rbp)
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$2, -5292(%rbp)
	movl	$4294967295, %ecx
	movl	$4, %edx
	movq	8(%rax), %r15
	movl	$0, -5288(%rbp)
	movb	$0, -5284(%rbp)
	movq	$0, -5280(%rbp)
	movl	$-1, -5272(%rbp)
	movb	$4, -5264(%rbp)
	pushq	-5272(%rbp)
	movb	$2, -5260(%rbp)
	pushq	-5280(%rbp)
	movl	$0, -5256(%rbp)
	pushq	-5288(%rbp)
	movb	$0, -5252(%rbp)
	pushq	-5296(%rbp)
	movq	$0, -5248(%rbp)
	movl	$-1, -5240(%rbp)
	pushq	-5240(%rbp)
	pushq	-5248(%rbp)
	pushq	-5256(%rbp)
	pushq	-5264(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE
	addq	$64, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector16MaskShiftOperandEPNS1_4NodeENS1_4TypeE
	movq	0(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32ShrEv@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler22RepresentationSelector14ChangeToPureOpEPNS1_4NodeEPKNS1_8OperatorE
	jmp	.L2927
.L3057:
	movzbl	23(%r14), %edx
	movq	32(%r12), %rcx
	leaq	32(%r14), %rax
	movq	32(%r14), %rsi
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L4104
	leaq	16(%rsi), %rax
	movq	16(%rsi), %rsi
.L4104:
	movl	20(%rsi), %edx
	andl	$16777215, %edx
	leaq	(%rdx,%rdx,4), %rdx
	movq	24(%rcx,%rdx,8), %rdx
	testq	%rdx, %rdx
	jne	.L4087
	movq	8(%rsi), %rdx
.L4087:
	addq	$8, %rax
	movq	%rdx, -208(%rbp)
	movq	(%rax), %rsi
	movl	20(%rsi), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rax
	movq	24(%rcx,%rax,8), %rax
	testq	%rax, %rax
	jne	.L3423
	movq	8(%rsi), %rax
.L3423:
	movq	%rax, -176(%rbp)
	cmpq	$1031, %rdx
	je	.L3424
	leaq	-208(%rbp), %rdi
	movl	$1031, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L3432
	movq	-176(%rbp), %rax
.L3424:
	cmpq	$1031, %rax
	je	.L3431
	leaq	-176(%rbp), %rdi
	movl	$1031, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L3431
.L3432:
	cmpq	$3079, -208(%rbp)
	je	.L3426
	leaq	-208(%rbp), %rdi
	movl	$3079, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L3435
.L3426:
	cmpq	$3079, -176(%rbp)
	je	.L3433
	leaq	-176(%rbp), %rdi
	movl	$3079, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L3435
.L3433:
	movl	-6008(%rbp), %r9d
	testl	%r9d, %r9d
	je	.L3431
.L3435:
	cmpq	$1099, -208(%rbp)
	je	.L3440
	leaq	-208(%rbp), %rdi
	movl	$1099, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L3440
.L3444:
	cmpq	$3147, -208(%rbp)
	je	.L3441
	leaq	-208(%rbp), %rdi
	movl	$3147, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L3447
.L3441:
	cmpq	$3147, -176(%rbp)
	je	.L3445
	leaq	-176(%rbp), %rdi
	movl	$3147, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L3447
.L3445:
	cmpl	$0, -6008(%rbp)
	je	.L3438
.L3447:
	movq	(%r12), %rax
	movq	16(%rax), %rax
	cmpb	$5, 16(%rax)
	jne	.L3454
	movq	344(%r12), %rax
	movq	384(%rax), %rsi
	cmpq	-208(%rbp), %rsi
	je	.L3453
	leaq	-208(%rbp), %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L3454
	movq	344(%r12), %rax
.L3453:
	movq	384(%rax), %rsi
	cmpq	-176(%rbp), %rsi
	je	.L3451
	leaq	-176(%rbp), %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L3454
.L3451:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector15VisitInt64BinopEPNS1_4NodeE
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	0(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder13Int64LessThanEv@PLT
	movl	$5, %ecx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler18SimplifiedLowering5DoMaxEPNS1_4NodeEPKNS1_8OperatorENS0_21MachineRepresentationE
	jmp	.L2927
	.p2align 4,,10
	.p2align 3
.L2992:
	call	_ZN2v88internal8compiler19ExternalArrayTypeOfEPKNS1_8OperatorE@PLT
	movl	%eax, %edi
	call	_ZN2v88internal8compiler12_GLOBAL__N_134MachineRepresentationFromArrayTypeENS0_17ExternalArrayTypeE
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$8, -2416(%rbp)
	movl	%eax, %ebx
	movb	$5, -2412(%rbp)
	movl	$1, -2408(%rbp)
	movb	$0, -2404(%rbp)
	movq	$0, -2400(%rbp)
	movl	$-1, -2392(%rbp)
	pushq	-2392(%rbp)
	pushq	-2400(%rbp)
	pushq	-2408(%rbp)
	pushq	-2416(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12ProcessInputEPNS1_4NodeEiNS1_7UseInfoE
	addq	$32, %rsp
	movq	%r14, %rsi
	movb	$5, -2384(%rbp)
	movb	$5, -2380(%rbp)
	movl	$1, %edx
	movq	%r12, %rdi
	movl	$1, -2376(%rbp)
	movb	$0, -2372(%rbp)
	movq	$0, -2368(%rbp)
	movl	$-1, -2360(%rbp)
	pushq	-2360(%rbp)
	pushq	-2368(%rbp)
	pushq	-2376(%rbp)
	pushq	-2384(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12ProcessInputEPNS1_4NodeEiNS1_7UseInfoE
	addq	$32, %rsp
	movq	%r14, %rsi
	movb	$5, -2352(%rbp)
	movb	$5, -2348(%rbp)
	movl	$2, %edx
	movq	%r12, %rdi
	movl	$1, -2344(%rbp)
	movb	$0, -2340(%rbp)
	movq	$0, -2336(%rbp)
	movl	$-1, -2328(%rbp)
	pushq	-2328(%rbp)
	pushq	-2336(%rbp)
	pushq	-2344(%rbp)
	pushq	-2352(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12ProcessInputEPNS1_4NodeEiNS1_7UseInfoE
	addq	$32, %rsp
	movq	%r14, %rsi
	movb	$1, -2320(%rbp)
	movb	$1, -2316(%rbp)
	movl	$3, %edx
	movq	%r12, %rdi
	movl	$0, -2312(%rbp)
	movb	$0, -2308(%rbp)
	movq	$0, -2304(%rbp)
	movl	$-1, -2296(%rbp)
	pushq	-2296(%rbp)
	pushq	-2304(%rbp)
	pushq	-2312(%rbp)
	pushq	-2320(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12ProcessInputEPNS1_4NodeEiNS1_7UseInfoE
	movl	$4, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	addq	$32, %rsp
	call	_ZN2v88internal8compiler22RepresentationSelector22ProcessRemainingInputsEPNS1_4NodeEi
	movl	20(%r14), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rdx
	movq	32(%r12), %rax
	leaq	(%rax,%rdx,8), %rdx
	movl	120(%r12), %eax
	testl	%eax, %eax
	je	.L3626
	cmpl	$1, %eax
	jne	.L2927
	movb	%bl, 1(%rdx)
	jmp	.L2927
.L2993:
	call	_ZN2v88internal8compiler19ExternalArrayTypeOfEPKNS1_8OperatorE@PLT
	movl	%eax, %edi
	call	_ZN2v88internal8compiler12_GLOBAL__N_134MachineRepresentationFromArrayTypeENS0_17ExternalArrayTypeE
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$8, -2544(%rbp)
	movl	%eax, %ebx
	movb	$5, -2540(%rbp)
	movl	$1, -2536(%rbp)
	movb	$0, -2532(%rbp)
	movq	$0, -2528(%rbp)
	movl	$-1, -2520(%rbp)
	pushq	-2520(%rbp)
	pushq	-2528(%rbp)
	pushq	-2536(%rbp)
	pushq	-2544(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12ProcessInputEPNS1_4NodeEiNS1_7UseInfoE
	addq	$32, %rsp
	movq	%r14, %rsi
	movb	$8, -2512(%rbp)
	movb	$5, -2508(%rbp)
	movl	$1, %edx
	movq	%r12, %rdi
	movl	$1, -2504(%rbp)
	movb	$0, -2500(%rbp)
	movq	$0, -2496(%rbp)
	movl	$-1, -2488(%rbp)
	pushq	-2488(%rbp)
	pushq	-2496(%rbp)
	pushq	-2504(%rbp)
	pushq	-2512(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12ProcessInputEPNS1_4NodeEiNS1_7UseInfoE
	addq	$32, %rsp
	movq	%r14, %rsi
	movb	$5, -2480(%rbp)
	movb	$5, -2476(%rbp)
	movl	$2, %edx
	movq	%r12, %rdi
	movl	$1, -2472(%rbp)
	movb	$0, -2468(%rbp)
	movq	$0, -2464(%rbp)
	movl	$-1, -2456(%rbp)
	pushq	-2456(%rbp)
	pushq	-2464(%rbp)
	pushq	-2472(%rbp)
	pushq	-2480(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12ProcessInputEPNS1_4NodeEiNS1_7UseInfoE
	addq	$32, %rsp
	movq	%r14, %rsi
	movb	$5, -2448(%rbp)
	movb	$5, -2444(%rbp)
	movl	$3, %edx
	movq	%r12, %rdi
	movl	$1, -2440(%rbp)
	movb	$0, -2436(%rbp)
	movq	$0, -2432(%rbp)
	movl	$-1, -2424(%rbp)
	pushq	-2424(%rbp)
	pushq	-2432(%rbp)
	pushq	-2440(%rbp)
	pushq	-2448(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12ProcessInputEPNS1_4NodeEiNS1_7UseInfoE
	movl	$4, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	addq	$32, %rsp
	call	_ZN2v88internal8compiler22RepresentationSelector22ProcessRemainingInputsEPNS1_4NodeEi
	movl	20(%r14), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rdx
	movq	32(%r12), %rax
	leaq	(%rax,%rdx,8), %rdx
	movl	120(%r12), %eax
	testl	%eax, %eax
	je	.L3624
	cmpl	$1, %eax
	jne	.L2927
	movb	%bl, 1(%rdx)
	jmp	.L2927
.L3052:
	movl	$1031, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector13BothInputsAreEPNS1_4NodeENS1_4TypeE
	testb	%al, %al
	je	.L3294
	movl	$2, %esi
	movl	%r15d, %edi
	call	_ZN2v88internal8compiler10Truncation11LessGeneralENS2_14TruncationKindES3_@PLT
	testb	%al, %al
	jne	.L4120
.L3294:
	movl	$1099, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector13BothInputsAreEPNS1_4NodeENS1_4TypeE
	testb	%al, %al
	je	.L3293
	movq	8(%r14), %rax
	movq	%rax, -144(%rbp)
	cmpq	$1099, %rax
	je	.L3295
	leaq	-144(%rbp), %rdi
	movl	$1099, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L3295
	movl	$2, %esi
	movl	%r15d, %edi
	call	_ZN2v88internal8compiler10Truncation11LessGeneralENS2_14TruncationKindES3_@PLT
	testb	%al, %al
	jne	.L4121
.L3293:
	movq	(%r14), %rdi
	call	_ZN2v88internal8compiler21NumberOperationHintOfEPKNS1_8OperatorE@PLT
	movl	$1031, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler22RepresentationSelector13BothInputsAreEPNS1_4NodeENS1_4TypeE
	testb	%al, %al
	je	.L3299
	testb	$-3, %bl
	je	.L4122
	movl	$1099, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector13BothInputsAreEPNS1_4NodeENS1_4TypeE
.L3301:
	cmpb	$2, %bl
	ja	.L3302
	movl	$2, %esi
	movl	%r15d, %edi
	call	_ZN2v88internal8compiler10Truncation11LessGeneralENS2_14TruncationKindES3_@PLT
	testb	%al, %al
	jne	.L4123
	cmpb	$1, %bl
	jne	.L4124
.L3302:
	movb	$13, -144(%rbp)
	movl	$7263, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$5, -140(%rbp)
	movl	$13, %edx
	movl	$1, -136(%rbp)
	movb	$5, -132(%rbp)
	movq	$0, -128(%rbp)
	movl	$-1, -120(%rbp)
	pushq	-120(%rbp)
	pushq	-128(%rbp)
	pushq	-136(%rbp)
	pushq	-144(%rbp)
	pushq	-120(%rbp)
	pushq	-128(%rbp)
	pushq	-136(%rbp)
	pushq	-144(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE
	addq	$64, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	(%r14), %rax
	movq	128(%r12), %rdi
	movzwl	16(%rax), %esi
	call	_ZN2v88internal8compiler21RepresentationChanger18Float64OperatorForENS1_8IrOpcode5ValueE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler22RepresentationSelector14ChangeToPureOpEPNS1_4NodeEPKNS1_8OperatorE
	jmp	.L2927
	.p2align 4,,10
	.p2align 3
.L3035:
	movb	$4, -4400(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$2, -4396(%rbp)
	movl	$4, %edx
	movl	$0, -4392(%rbp)
	movb	$0, -4388(%rbp)
	movq	$0, -4384(%rbp)
	movl	$-1, -4376(%rbp)
	pushq	-4376(%rbp)
	pushq	-4384(%rbp)
	pushq	-4392(%rbp)
	pushq	-4400(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movzbl	23(%r14), %eax
	movq	32(%r14), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L3533
	movq	16(%rdx), %rdx
.L3533:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector16DeferReplacementEPNS1_4NodeES4_
	jmp	.L2927
.L2960:
	movb	$6, -784(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$5, -780(%rbp)
	movl	$7, %edx
	movl	$1, -776(%rbp)
	movb	$0, -772(%rbp)
	movq	$0, -768(%rbp)
	movl	$-1, -760(%rbp)
	movb	$5, -752(%rbp)
	pushq	-760(%rbp)
	movb	$5, -748(%rbp)
	pushq	-768(%rbp)
	movl	$1, -744(%rbp)
	pushq	-776(%rbp)
	movb	$0, -740(%rbp)
	pushq	-784(%rbp)
	movq	$0, -736(%rbp)
	movl	$-1, -728(%rbp)
	pushq	-728(%rbp)
	pushq	-736(%rbp)
	pushq	-744(%rbp)
	pushq	-752(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE
	addq	$64, %rsp
	jmp	.L2927
.L3003:
	movb	$8, -688(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$5, -684(%rbp)
	movl	$8, %edx
	movl	$1, -680(%rbp)
	movb	$0, -676(%rbp)
	movq	$0, -672(%rbp)
	movl	$-1, -664(%rbp)
	pushq	-664(%rbp)
	pushq	-672(%rbp)
	pushq	-680(%rbp)
	pushq	-688(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	jmp	.L2927
.L3000:
	movzbl	23(%r14), %eax
	movq	32(%r14), %rdx
	leaq	32(%r14), %rbx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L3632
	movq	16(%rdx), %rdx
.L3632:
	movl	20(%rdx), %eax
	movq	32(%r12), %r9
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rax
	leaq	(%r9,%rax,8), %rax
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L3633
	movq	8(%rdx), %rax
.L3633:
	movb	$8, -144(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$5, -140(%rbp)
	movl	$7, %edx
	movl	$1, -136(%rbp)
	movb	$0, -132(%rbp)
	movq	$0, -128(%rbp)
	movl	$-1, -120(%rbp)
	pushq	-120(%rbp)
	pushq	-128(%rbp)
	pushq	-136(%rbp)
	pushq	-144(%rbp)
	pushq	-120(%rbp)
	pushq	-128(%rbp)
	pushq	-136(%rbp)
	pushq	-144(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE
	addq	$64, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	cmpq	$75431937, -176(%rbp)
	je	.L3636
	leaq	-176(%rbp), %r15
	movl	$75431937, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L3636
	cmpq	$385, -176(%rbp)
	je	.L3637
	movl	$385, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L3637
	movl	$385, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L2927
	movq	0(%r13), %rax
	movl	$1, %esi
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder15ConvertReceiverENS0_19ConvertReceiverModeE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	jmp	.L2927
	.p2align 4,,10
	.p2align 3
.L3001:
	movb	$7, -464(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$5, -460(%rbp)
	movl	$1, %edx
	movl	$1, -456(%rbp)
	movb	$6, -452(%rbp)
	movq	$0, -448(%rbp)
	movl	$-1, -440(%rbp)
	pushq	-440(%rbp)
	pushq	-448(%rbp)
	pushq	-456(%rbp)
	pushq	-464(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	jmp	.L2927
.L2996:
	testb	%r15b, %r15b
	je	.L3550
	movb	$4, -3088(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$2, -3084(%rbp)
	movl	$8, %edx
	movl	$0, -3080(%rbp)
	movb	$0, -3076(%rbp)
	movq	$0, -3072(%rbp)
	movl	$-1, -3064(%rbp)
	movb	$8, -3056(%rbp)
	pushq	-3064(%rbp)
	movb	$5, -3052(%rbp)
	pushq	-3072(%rbp)
	movl	$1, -3048(%rbp)
	pushq	-3080(%rbp)
	movb	$0, -3044(%rbp)
	pushq	-3088(%rbp)
	movq	$0, -3040(%rbp)
	movl	$-1, -3032(%rbp)
	pushq	-3032(%rbp)
	pushq	-3040(%rbp)
	pushq	-3048(%rbp)
	pushq	-3056(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE
	addq	$64, %rsp
	jmp	.L2927
.L3086:
	movl	120(%r12), %eax
	movq	%r15, %r8
	movb	$1, -144(%rbp)
	movb	$1, -140(%rbp)
	movl	$0, -136(%rbp)
	movb	$0, -132(%rbp)
	movq	$0, -128(%rbp)
	movl	$-1, -120(%rbp)
	testl	%eax, %eax
	je	.L3121
	cmpl	$2, %eax
	jne	.L3123
	pushq	-120(%rbp)
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	pushq	-128(%rbp)
	movq	%r12, %rdi
	pushq	-136(%rbp)
	pushq	-144(%rbp)
	movq	%r15, -6016(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12ConvertInputEPNS1_4NodeEiNS1_7UseInfoENS1_4TypeE
	movq	-6016(%rbp), %r8
	addq	$32, %rsp
.L3123:
	movl	20(%r14), %eax
	movq	32(%r12), %rsi
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rax
	leaq	(%rsi,%rax,8), %rax
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	jne	.L3124
	movq	8(%r14), %rdx
.L3124:
	movq	-6008(%rbp), %rax
	movb	%bl, %r8b
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%r8d, %ecx
	salq	$32, %rax
	orq	%rax, %rcx
	call	_ZN2v88internal8compiler22RepresentationSelector19GetOutputInfoForPhiEPNS1_4NodeENS1_4TypeENS1_10TruncationE
	movl	%eax, %ebx
	movl	20(%r14), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rdx
	movq	32(%r12), %rax
	leaq	(%rax,%rdx,8), %rdx
	movl	120(%r12), %eax
	testl	%eax, %eax
	je	.L3125
	cmpl	$1, %eax
	jne	.L3127
	movb	%bl, 1(%rdx)
	movl	120(%r12), %eax
.L3127:
	cmpl	$2, %eax
	je	.L4125
	movl	-6008(%rbp), %ecx
	movb	%bl, -144(%rbp)
	movb	%r15b, -140(%rbp)
	movl	%ecx, -136(%rbp)
	movb	$0, -132(%rbp)
	movq	$0, -128(%rbp)
	movl	$-1, -120(%rbp)
	testl	%eax, %eax
	jne	.L2927
.L3131:
	pushq	-120(%rbp)
	movl	$1, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	pushq	-128(%rbp)
	pushq	-136(%rbp)
	pushq	-144(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12EnqueueInputEPNS1_4NodeEiNS1_7UseInfoE
	movl	120(%r12), %eax
	addq	$32, %rsp
.L3134:
	movb	%bl, -144(%rbp)
	movl	-6008(%rbp), %ebx
	movb	%r15b, -140(%rbp)
	movl	%ebx, -136(%rbp)
	movb	$0, -132(%rbp)
	movq	$0, -128(%rbp)
	movl	$-1, -120(%rbp)
	testl	%eax, %eax
	jne	.L4126
	pushq	-120(%rbp)
	movl	$2, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	pushq	-128(%rbp)
	pushq	-136(%rbp)
	pushq	-144(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12EnqueueInputEPNS1_4NodeEiNS1_7UseInfoE
	addq	$32, %rsp
	jmp	.L2927
.L3084:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector11VisitInputsEPNS1_4NodeE
	movl	20(%r14), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rdx
	movq	32(%r12), %rax
	leaq	(%rax,%rdx,8), %rdx
	movl	120(%r12), %eax
	testl	%eax, %eax
	je	.L3751
	cmpl	$1, %eax
	jne	.L2927
	movb	$7, 1(%rdx)
	jmp	.L2927
.L2994:
	testb	%r15b, %r15b
	je	.L3550
	call	_ZN2v88internal8compiler15ElementAccessOfEPKNS1_8OperatorE@PLT
	movb	$5, -2928(%rbp)
	cmpb	$1, (%rax)
	movb	$5, -2924(%rbp)
	movl	$1, -2920(%rbp)
	movzbl	16(%rax), %edx
	movb	$0, -2916(%rbp)
	movq	$0, -2912(%rbp)
	movl	$-1, -2904(%rbp)
	je	.L4127
	movb	$5, -2896(%rbp)
.L4093:
	pushq	-2904(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	pushq	-2912(%rbp)
	movb	$5, -2892(%rbp)
	pushq	-2920(%rbp)
	movl	$1, -2888(%rbp)
	pushq	-2928(%rbp)
	movb	$0, -2884(%rbp)
	movq	$0, -2880(%rbp)
	movl	$-1, -2872(%rbp)
	pushq	-2872(%rbp)
	pushq	-2880(%rbp)
	pushq	-2888(%rbp)
	pushq	-2896(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE
	addq	$64, %rsp
	jmp	.L2927
.L3044:
	movb	$4, -4976(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$2, -4972(%rbp)
	movl	$4, %edx
	movl	$0, -4968(%rbp)
	movb	$0, -4964(%rbp)
	movq	$0, -4960(%rbp)
	movl	$-1, -4952(%rbp)
	pushq	-4952(%rbp)
	pushq	-4960(%rbp)
	pushq	-4968(%rbp)
	pushq	-4976(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	(%r14), %rax
	movq	128(%r12), %rdi
	movzwl	16(%rax), %esi
	call	_ZN2v88internal8compiler21RepresentationChanger17Uint32OperatorForENS1_8IrOpcode5ValueE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	jmp	.L2927
.L3045:
	movzbl	23(%r14), %eax
	movq	32(%r14), %rdx
	leaq	32(%r14), %rbx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L3404
	movq	16(%rdx), %rdx
.L3404:
	movl	20(%rdx), %eax
	movq	32(%r12), %r10
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rax
	leaq	(%r10,%rax,8), %rax
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L3405
	movq	8(%rdx), %rax
.L3405:
	movq	%rax, -144(%rbp)
	cmpq	$3079, %rax
	je	.L3406
	leaq	-144(%rbp), %r15
	movl	$3079, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L3406
	cmpq	$3147, -144(%rbp)
	je	.L3407
	movl	$3147, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L3407
	movq	344(%r12), %rax
	movq	360(%rax), %rsi
	cmpq	-144(%rbp), %rsi
	je	.L3412
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L3412
	movb	$13, -5008(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$4, -5004(%rbp)
	movl	$13, %edx
	movl	$0, -5000(%rbp)
	movb	$0, -4996(%rbp)
	movq	$0, -4992(%rbp)
	movl	$-1, -4984(%rbp)
	pushq	-4984(%rbp)
	pushq	-4992(%rbp)
	pushq	-5000(%rbp)
	pushq	-5008(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	(%r14), %rax
	movq	128(%r12), %rdi
	movzwl	16(%rax), %esi
	call	_ZN2v88internal8compiler21RepresentationChanger18Float64OperatorForENS1_8IrOpcode5ValueE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	jmp	.L2927
	.p2align 4,,10
	.p2align 3
.L3074:
	movb	$0, -208(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$5, -204(%rbp)
	movl	$8, %edx
	movl	$1, -200(%rbp)
	movb	$0, -196(%rbp)
	movq	$0, -192(%rbp)
	movl	$-1, -184(%rbp)
	pushq	-184(%rbp)
	pushq	-192(%rbp)
	pushq	-200(%rbp)
	pushq	-208(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	jmp	.L2927
.L3075:
	movb	$0, -240(%rbp)
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$5, -236(%rbp)
	movl	$1, -232(%rbp)
	movb	$0, -228(%rbp)
	movq	$0, -224(%rbp)
	movl	$-1, -216(%rbp)
	pushq	-216(%rbp)
	pushq	-224(%rbp)
	pushq	-232(%rbp)
	pushq	-240(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12ProcessInputEPNS1_4NodeEiNS1_7UseInfoE
	movl	20(%r14), %eax
	addq	$32, %rsp
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rdx
	movq	32(%r12), %rax
	leaq	(%rax,%rdx,8), %rdx
	movl	120(%r12), %eax
	testl	%eax, %eax
	je	.L3762
	cmpl	$1, %eax
	jne	.L2927
	movb	$0, 1(%rdx)
	jmp	.L2927
.L3047:
	call	_ZN2v88internal8compiler21NumberOperationHintOfEPKNS1_8OperatorE@PLT
	movl	%eax, %ebx
	movzbl	23(%r14), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L3393
	leaq	40(%r14), %rax
.L3394:
	movq	(%rax), %rax
	movq	344(%r12), %rdx
	movq	8(%rax), %rax
	movq	%rax, -208(%rbp)
	movq	312(%rdx), %rsi
	cmpq	%rax, %rsi
	je	.L3398
	leaq	-208(%rbp), %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L3397
.L3398:
	testb	$-3, %bl
	je	.L4128
.L3397:
	movl	$8396767, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector13BothInputsAreEPNS1_4NodeENS1_4TypeE
	testb	%al, %al
	jne	.L4129
	movl	%ebx, %esi
	leaq	-5136(%rbp), %rdi
	movl	$1, %ecx
	movq	$0, -176(%rbp)
	leaq	-176(%rbp), %rdx
	movl	$-1, -168(%rbp)
	call	_ZN2v88internal8compiler12_GLOBAL__N_130CheckedUseInfoAsWord32FromHintENS1_19NumberOperationHintERKNS1_14FeedbackSourceENS1_13IdentifyZerosE
	movq	-5128(%rbp), %rdx
	movq	%r14, %rsi
	movq	-5120(%rbp), %rcx
	movq	-5136(%rbp), %rax
	movq	-5112(%rbp), %rbx
	movq	%r12, %rdi
	movq	%rdx, -136(%rbp)
	pushq	%rbx
	pushq	%rcx
	pushq	%rdx
	pushq	%rax
	pushq	%rbx
	pushq	%rcx
	pushq	%rdx
	movl	$4, %edx
	pushq	%rax
	movq	%rcx, -128(%rbp)
	movl	$1031, %ecx
	movq	%rax, -144(%rbp)
	movq	%rbx, -120(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE
	addq	$64, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	-208(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector16MaskShiftOperandEPNS1_4NodeENS1_4TypeE
	movq	0(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32ShrEv@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler22RepresentationSelector14ChangeToPureOpEPNS1_4NodeEPKNS1_8OperatorE
	jmp	.L2927
.L3011:
	movq	%r13, %rcx
	movl	$16385, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector10VisitCheckEPNS1_4NodeENS1_4TypeEPNS1_18SimplifiedLoweringE
	jmp	.L2927
.L2999:
	movl	$8396767, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector7InputIsEPNS1_4NodeENS1_4TypeE.isra.0
	testb	%al, %al
	je	.L3717
	movl	$2, %esi
	movl	%r15d, %edi
	call	_ZN2v88internal8compiler10Truncation11LessGeneralENS2_14TruncationKindES3_@PLT
	testb	%al, %al
	jne	.L4130
.L3717:
	movl	$8396767, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector7InputIsEPNS1_4NodeENS1_4TypeE.isra.0
	testb	%al, %al
	je	.L3715
	movl	$4, %esi
	movl	%r15d, %edi
	call	_ZN2v88internal8compiler10Truncation11LessGeneralENS2_14TruncationKindES3_@PLT
	testb	%al, %al
	jne	.L4131
.L3715:
	movl	$209682431, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector7InputIsEPNS1_4NodeENS1_4TypeE.isra.0
	testb	%al, %al
	je	.L4132
	movb	$8, -592(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$5, -588(%rbp)
	movl	$8, %edx
	movl	$1, -584(%rbp)
	movb	$0, -580(%rbp)
	movq	$0, -576(%rbp)
	movl	$-1, -568(%rbp)
	pushq	-568(%rbp)
	pushq	-576(%rbp)
	pushq	-584(%rbp)
	pushq	-592(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movzbl	23(%r14), %eax
	movq	32(%r14), %rcx
	leaq	32(%r14), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L3722
	leaq	16(%rcx), %rdx
.L3722:
	movq	(%rdx), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector16DeferReplacementEPNS1_4NodeES4_
	jmp	.L2927
.L2988:
	call	_ZN2v88internal8compiler19ExternalArrayTypeOfEPKNS1_8OperatorE@PLT
	movl	%eax, %edi
	call	_ZN2v88internal8compiler12_GLOBAL__N_134MachineRepresentationFromArrayTypeENS0_17ExternalArrayTypeE
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$8, -2128(%rbp)
	movl	%eax, %r13d
	movb	$5, -2124(%rbp)
	movl	$1, -2120(%rbp)
	movb	$0, -2116(%rbp)
	movq	$0, -2112(%rbp)
	movl	$-1, -2104(%rbp)
	pushq	-2104(%rbp)
	pushq	-2112(%rbp)
	pushq	-2120(%rbp)
	pushq	-2128(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12ProcessInputEPNS1_4NodeEiNS1_7UseInfoE
	addq	$32, %rsp
	movq	%r14, %rsi
	movb	$5, -2096(%rbp)
	movb	$5, -2092(%rbp)
	movl	$1, %edx
	movq	%r12, %rdi
	movl	$1, -2088(%rbp)
	movb	$0, -2084(%rbp)
	movq	$0, -2080(%rbp)
	movl	$-1, -2072(%rbp)
	pushq	-2072(%rbp)
	pushq	-2080(%rbp)
	pushq	-2088(%rbp)
	pushq	-2096(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12ProcessInputEPNS1_4NodeEiNS1_7UseInfoE
	addq	$32, %rsp
	movq	%r14, %rsi
	movb	$5, -2064(%rbp)
	movb	$5, -2060(%rbp)
	movl	$2, %edx
	movq	%r12, %rdi
	movl	$1, -2056(%rbp)
	movb	$0, -2052(%rbp)
	movq	$0, -2048(%rbp)
	movl	$-1, -2040(%rbp)
	pushq	-2040(%rbp)
	pushq	-2048(%rbp)
	pushq	-2056(%rbp)
	pushq	-2064(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12ProcessInputEPNS1_4NodeEiNS1_7UseInfoE
	addq	$32, %rsp
	leaq	-2032(%rbp), %rdi
	movl	%r13d, %esi
	call	_ZN2v88internal8compiler12_GLOBAL__N_135TruncatingUseInfoFromRepresentationENS0_21MachineRepresentationE
	movl	$3, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	pushq	-2008(%rbp)
	pushq	-2016(%rbp)
	pushq	-2024(%rbp)
	pushq	-2032(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12ProcessInputEPNS1_4NodeEiNS1_7UseInfoE
	addq	$32, %rsp
	movq	%r14, %rsi
	movb	$1, -2000(%rbp)
	movb	$1, -1996(%rbp)
	movl	$4, %edx
	movq	%r12, %rdi
	movl	$0, -1992(%rbp)
	movb	$0, -1988(%rbp)
	movq	$0, -1984(%rbp)
	movl	$-1, -1976(%rbp)
	pushq	-1976(%rbp)
	pushq	-1984(%rbp)
	pushq	-1992(%rbp)
	pushq	-2000(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12ProcessInputEPNS1_4NodeEiNS1_7UseInfoE
	movl	$5, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	addq	$32, %rsp
	call	_ZN2v88internal8compiler22RepresentationSelector22ProcessRemainingInputsEPNS1_4NodeEi
	movl	20(%r14), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rdx
	movq	32(%r12), %rax
	leaq	(%rax,%rdx,8), %rdx
	movl	120(%r12), %eax
	testl	%eax, %eax
	je	.L3630
	cmpl	$1, %eax
	jne	.L2927
	movb	$0, 1(%rdx)
	jmp	.L2927
.L2989:
	call	_ZN2v88internal8compiler19ExternalArrayTypeOfEPKNS1_8OperatorE@PLT
	movl	%eax, %edi
	call	_ZN2v88internal8compiler12_GLOBAL__N_134MachineRepresentationFromArrayTypeENS0_17ExternalArrayTypeE
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$8, -2288(%rbp)
	movl	%eax, %r13d
	movb	$5, -2284(%rbp)
	movl	$1, -2280(%rbp)
	movb	$0, -2276(%rbp)
	movq	$0, -2272(%rbp)
	movl	$-1, -2264(%rbp)
	pushq	-2264(%rbp)
	pushq	-2272(%rbp)
	pushq	-2280(%rbp)
	pushq	-2288(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12ProcessInputEPNS1_4NodeEiNS1_7UseInfoE
	addq	$32, %rsp
	movq	%r14, %rsi
	movb	$8, -2256(%rbp)
	movb	$5, -2252(%rbp)
	movl	$1, %edx
	movq	%r12, %rdi
	movl	$1, -2248(%rbp)
	movb	$0, -2244(%rbp)
	movq	$0, -2240(%rbp)
	movl	$-1, -2232(%rbp)
	pushq	-2232(%rbp)
	pushq	-2240(%rbp)
	pushq	-2248(%rbp)
	pushq	-2256(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12ProcessInputEPNS1_4NodeEiNS1_7UseInfoE
	addq	$32, %rsp
	movq	%r14, %rsi
	movb	$5, -2224(%rbp)
	movb	$5, -2220(%rbp)
	movl	$2, %edx
	movq	%r12, %rdi
	movl	$1, -2216(%rbp)
	movb	$0, -2212(%rbp)
	movq	$0, -2208(%rbp)
	movl	$-1, -2200(%rbp)
	pushq	-2200(%rbp)
	pushq	-2208(%rbp)
	pushq	-2216(%rbp)
	pushq	-2224(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12ProcessInputEPNS1_4NodeEiNS1_7UseInfoE
	addq	$32, %rsp
	movq	%r14, %rsi
	movb	$5, -2192(%rbp)
	movb	$5, -2188(%rbp)
	movl	$3, %edx
	movq	%r12, %rdi
	movl	$1, -2184(%rbp)
	movb	$0, -2180(%rbp)
	movq	$0, -2176(%rbp)
	movl	$-1, -2168(%rbp)
	pushq	-2168(%rbp)
	pushq	-2176(%rbp)
	pushq	-2184(%rbp)
	pushq	-2192(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12ProcessInputEPNS1_4NodeEiNS1_7UseInfoE
	addq	$32, %rsp
	leaq	-2160(%rbp), %rdi
	movl	%r13d, %esi
	call	_ZN2v88internal8compiler12_GLOBAL__N_135TruncatingUseInfoFromRepresentationENS0_21MachineRepresentationE
	movl	$4, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	pushq	-2136(%rbp)
	pushq	-2144(%rbp)
	pushq	-2152(%rbp)
	pushq	-2160(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12ProcessInputEPNS1_4NodeEiNS1_7UseInfoE
	movl	$5, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	addq	$32, %rsp
	call	_ZN2v88internal8compiler22RepresentationSelector22ProcessRemainingInputsEPNS1_4NodeEi
	movl	20(%r14), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rdx
	movq	32(%r12), %rax
	leaq	(%rax,%rdx,8), %rdx
	movl	120(%r12), %eax
	testl	%eax, %eax
	je	.L3628
	cmpl	$1, %eax
	jne	.L2927
	movb	$0, 1(%rdx)
	jmp	.L2927
.L2964:
	movq	%r13, %rcx
	movl	$262529, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector13VisitObjectIsEPNS1_4NodeENS1_4TypeEPNS1_18SimplifiedLoweringE
	jmp	.L2927
.L3077:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector11VisitUnusedEPNS1_4NodeE
	jmp	.L2927
.L3078:
	movb	$0, -6000(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$0, -5996(%rbp)
	movl	$8, %edx
	movl	$0, -5992(%rbp)
	movb	$0, -5988(%rbp)
	movq	$0, -5984(%rbp)
	movl	$-1, -5976(%rbp)
	pushq	-5976(%rbp)
	pushq	-5984(%rbp)
	pushq	-5992(%rbp)
	pushq	-6000(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	jmp	.L2927
.L3079:
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitCallEPNS1_4NodeEPNS1_18SimplifiedLoweringE
	jmp	.L2927
.L3080:
	movl	20(%r14), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rdx
	movq	32(%r12), %rax
	leaq	(%rax,%rdx,8), %rdx
	movl	120(%r12), %eax
	testl	%eax, %eax
	je	.L3743
	cmpl	$1, %eax
	jne	.L2927
	movb	$7, 1(%rdx)
	jmp	.L2927
.L3081:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector16VisitObjectStateEPNS1_4NodeE
	jmp	.L2927
.L2952:
	movb	$8, -176(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$5, -172(%rbp)
	movl	$8, %edx
	movl	$1, -168(%rbp)
	movb	$0, -164(%rbp)
	movq	$0, -160(%rbp)
	movl	$-1, -152(%rbp)
	pushq	-152(%rbp)
	pushq	-160(%rbp)
	pushq	-168(%rbp)
	pushq	-176(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	jmp	.L2927
.L2953:
	movb	$4, -3408(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$2, -3404(%rbp)
	movl	$4, %edx
	movl	$0, -3400(%rbp)
	movb	$0, -3396(%rbp)
	movq	$0, -3392(%rbp)
	movl	$-1, -3384(%rbp)
	pushq	-3384(%rbp)
	pushq	-3392(%rbp)
	pushq	-3400(%rbp)
	pushq	-3408(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	jmp	.L2927
.L2954:
	movzbl	23(%r14), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L3755
	leaq	40(%r14), %rax
.L3756:
	movq	(%rax), %rdx
	movq	32(%r12), %rcx
	movl	20(%rdx), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rax
	leaq	(%rcx,%rax,8), %rax
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L3757
	movq	8(%rdx), %rax
.L3757:
	movq	%rax, -176(%rbp)
	cmpq	$3147, %rax
	je	.L3758
	leaq	-176(%rbp), %rdi
	movl	$3147, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L3758
	movb	$8, -144(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$5, -140(%rbp)
	movl	$6, %edx
	movl	$1, -136(%rbp)
	movb	$0, -132(%rbp)
	movq	$0, -128(%rbp)
	movl	$-1, -120(%rbp)
	pushq	-120(%rbp)
	pushq	-128(%rbp)
	pushq	-136(%rbp)
	pushq	-144(%rbp)
	pushq	-120(%rbp)
	pushq	-128(%rbp)
	pushq	-136(%rbp)
	pushq	-144(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE
	addq	$64, %rsp
	jmp	.L2927
.L3091:
	movl	20(%r14), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rdx
	movq	32(%r12), %rax
	leaq	(%rax,%rdx,8), %rdx
	movl	120(%r12), %eax
	testl	%eax, %eax
	je	.L3099
	cmpl	$1, %eax
	jne	.L2927
	movb	$4, 1(%rdx)
	jmp	.L2927
.L3027:
	movl	$513, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector7InputIsEPNS1_4NodeENS1_4TypeE.isra.0
	testb	%al, %al
	je	.L3644
	movb	$1, -1968(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$1, -1964(%rbp)
	movl	$4, %edx
	movl	$0, -1960(%rbp)
	movb	$0, -1956(%rbp)
	movq	$0, -1952(%rbp)
	movl	$-1, -1944(%rbp)
	pushq	-1944(%rbp)
	pushq	-1952(%rbp)
	pushq	-1960(%rbp)
	pushq	-1968(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movzbl	23(%r14), %ecx
	movq	32(%r14), %rax
	movq	%r14, %rsi
	movq	%r12, %rdi
	andl	$15, %ecx
	leaq	16(%rax), %rdx
	leaq	32(%r14), %rax
	cmpl	$15, %ecx
	cmove	%rdx, %rax
	movq	(%rax), %rdx
	call	_ZN2v88internal8compiler22RepresentationSelector16DeferReplacementEPNS1_4NodeES4_
	jmp	.L2927
.L3088:
	movl	20(%r14), %eax
	movsd	48(%rdi), %xmm0
	movl	120(%r12), %edx
	andl	$16777215, %eax
	comisd	.LC11(%rip), %xmm0
	leaq	(%rax,%rax,4), %rcx
	movq	32(%r12), %rax
	leaq	(%rax,%rcx,8), %rax
	jb	.L3105
	movsd	.LC12(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L3105
	movabsq	$-9223372036854775808, %rcx
	movq	%xmm0, %rbx
	cmpq	%rcx, %rbx
	je	.L3105
	cvttsd2sil	%xmm0, %ecx
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L3105
	jne	.L3105
	testl	%edx, %edx
	je	.L3109
	cmpl	$1, %edx
	jne	.L3111
	movb	$6, 1(%rax)
.L3111:
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	0(%r13), %rdi
	movq	%rcx, %rsi
	salq	$32, %rsi
	call	_ZN2v88internal8compiler12MachineGraph14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler22RepresentationSelector16DeferReplacementEPNS1_4NodeES4_
	jmp	.L2927
	.p2align 4,,10
	.p2align 3
.L3023:
	movb	$5, -3856(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$5, -3852(%rbp)
	movl	$6, %edx
	movl	$1, -3848(%rbp)
	movb	$0, -3844(%rbp)
	movq	$0, -3840(%rbp)
	movl	$-1, -3832(%rbp)
	movb	$8, -3824(%rbp)
	pushq	-3832(%rbp)
	movb	$5, -3820(%rbp)
	pushq	-3840(%rbp)
	movl	$1, -3816(%rbp)
	pushq	-3848(%rbp)
	movb	$0, -3812(%rbp)
	pushq	-3856(%rbp)
	movq	$0, -3808(%rbp)
	movl	$-1, -3800(%rbp)
	pushq	-3800(%rbp)
	pushq	-3808(%rbp)
	pushq	-3816(%rbp)
	pushq	-3824(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE
	addq	$64, %rsp
	jmp	.L2927
.L3024:
	movb	$5, -3920(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$5, -3916(%rbp)
	movl	$4, %edx
	movl	$1, -3912(%rbp)
	movb	$0, -3908(%rbp)
	movq	$0, -3904(%rbp)
	movl	$-1, -3896(%rbp)
	movb	$8, -3888(%rbp)
	pushq	-3896(%rbp)
	movb	$5, -3884(%rbp)
	pushq	-3904(%rbp)
	movl	$1, -3880(%rbp)
	pushq	-3912(%rbp)
	movb	$0, -3876(%rbp)
	pushq	-3920(%rbp)
	movq	$0, -3872(%rbp)
	movl	$-1, -3864(%rbp)
	pushq	-3864(%rbp)
	pushq	-3872(%rbp)
	pushq	-3880(%rbp)
	pushq	-3888(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE
	addq	$64, %rsp
	jmp	.L2927
.L3025:
	movb	$6, -4016(%rbp)
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$5, -4012(%rbp)
	movl	$1, -4008(%rbp)
	movb	$0, -4004(%rbp)
	movq	$0, -4000(%rbp)
	movl	$-1, -3992(%rbp)
	pushq	-3992(%rbp)
	pushq	-4000(%rbp)
	pushq	-4008(%rbp)
	pushq	-4016(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12ProcessInputEPNS1_4NodeEiNS1_7UseInfoE
	addq	$32, %rsp
	movq	%r14, %rsi
	movb	$8, -3984(%rbp)
	movb	$5, -3980(%rbp)
	movl	$1, %edx
	movq	%r12, %rdi
	movl	$1, -3976(%rbp)
	movb	$0, -3972(%rbp)
	movq	$0, -3968(%rbp)
	movl	$-1, -3960(%rbp)
	pushq	-3960(%rbp)
	pushq	-3968(%rbp)
	pushq	-3976(%rbp)
	pushq	-3984(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12ProcessInputEPNS1_4NodeEiNS1_7UseInfoE
	addq	$32, %rsp
	movq	%r14, %rsi
	movb	$8, -3952(%rbp)
	movb	$5, -3948(%rbp)
	movl	$2, %edx
	movq	%r12, %rdi
	movl	$1, -3944(%rbp)
	movb	$0, -3940(%rbp)
	movq	$0, -3936(%rbp)
	movl	$-1, -3928(%rbp)
	pushq	-3928(%rbp)
	pushq	-3936(%rbp)
	pushq	-3944(%rbp)
	pushq	-3952(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12ProcessInputEPNS1_4NodeEiNS1_7UseInfoE
	movl	20(%r14), %eax
	addq	$32, %rsp
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rdx
	movq	32(%r12), %rax
	leaq	(%rax,%rdx,8), %rdx
	movl	120(%r12), %eax
	testl	%eax, %eax
	je	.L3556
	cmpl	$1, %eax
	jne	.L2927
	movb	$7, 1(%rdx)
	jmp	.L2927
.L3026:
	cmpl	$2, 120(%r12)
	jne	.L3155
	movzbl	23(%r14), %eax
	movq	32(%r14), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L3156
	movq	16(%rdx), %rdx
.L3156:
	movl	20(%rdx), %eax
	movq	32(%r12), %rcx
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rax
	leaq	(%rcx,%rax,8), %rax
	movzbl	1(%rax), %eax
	cmpb	$1, %al
	je	.L4133
	leal	-7(%rax), %edx
	cmpb	$1, %dl
	jbe	.L4134
	subl	$10, %eax
	cmpb	$1, %al
	jbe	.L4135
	movq	0(%r13), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler22RepresentationSelector16DeferReplacementEPNS1_4NodeES4_
	jmp	.L2927
.L3021:
	movb	$4, -3760(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$2, -3756(%rbp)
	movl	$7, %edx
	movl	$0, -3752(%rbp)
	movb	$0, -3748(%rbp)
	movq	$0, -3744(%rbp)
	movl	$-1, -3736(%rbp)
	pushq	-3736(%rbp)
	pushq	-3744(%rbp)
	pushq	-3752(%rbp)
	pushq	-3760(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	jmp	.L2927
.L3020:
	movb	$5, -3728(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$5, -3724(%rbp)
	movl	$7, %edx
	movl	$1, -3720(%rbp)
	movb	$0, -3716(%rbp)
	movq	$0, -3712(%rbp)
	movl	$-1, -3704(%rbp)
	movb	$8, -3696(%rbp)
	pushq	-3704(%rbp)
	movb	$5, -3692(%rbp)
	pushq	-3712(%rbp)
	movl	$1, -3688(%rbp)
	pushq	-3720(%rbp)
	movb	$0, -3684(%rbp)
	pushq	-3728(%rbp)
	movq	$0, -3680(%rbp)
	movl	$-1, -3672(%rbp)
	pushq	-3672(%rbp)
	pushq	-3680(%rbp)
	pushq	-3688(%rbp)
	pushq	-3696(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE
	addq	$64, %rsp
	jmp	.L2927
.L3092:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector11VisitReturnEPNS1_4NodeE
	movl	20(%r14), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rdx
	movq	32(%r12), %rax
	leaq	(%rax,%rdx,8), %rdx
	movl	120(%r12), %eax
	testl	%eax, %eax
	je	.L3753
	cmpl	$1, %eax
	jne	.L2927
	movb	$8, 1(%rdx)
	jmp	.L2927
.L3008:
	call	_ZN2v88internal8compiler17CheckParametersOfEPKNS1_8OperatorE@PLT
	movl	$16417, %esi
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler22RepresentationSelector7InputIsEPNS1_4NodeENS1_4TypeE.isra.0
	testb	%al, %al
	je	.L3575
	movb	$8, -3184(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$5, -3180(%rbp)
	movl	$7, %edx
	movl	$1, -3176(%rbp)
	movb	$0, -3172(%rbp)
	movq	$0, -3168(%rbp)
	movl	$-1, -3160(%rbp)
	pushq	-3160(%rbp)
	pushq	-3168(%rbp)
	pushq	-3176(%rbp)
	pushq	-3184(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movzbl	23(%r14), %ecx
	movq	32(%r14), %rax
	movq	%r14, %rsi
	movq	%r12, %rdi
	andl	$15, %ecx
	leaq	16(%rax), %rdx
	leaq	32(%r14), %rax
	cmpl	$15, %ecx
	cmove	%rdx, %rax
	movq	(%rax), %rdx
	call	_ZN2v88internal8compiler22RepresentationSelector16DeferReplacementEPNS1_4NodeES4_
	jmp	.L2927
.L3031:
	movl	$134217729, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector7InputIsEPNS1_4NodeENS1_4TypeE.isra.0
	testb	%al, %al
	je	.L3512
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitNoopEPNS1_4NodeENS1_10TruncationE
	jmp	.L2927
.L3085:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector8VisitPhiEPNS1_4NodeENS1_10TruncationEPNS1_18SimplifiedLoweringE
	jmp	.L2927
.L3022:
	movb	$4, -3792(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$2, -3788(%rbp)
	movl	$7, %edx
	movl	$0, -3784(%rbp)
	movb	$0, -3780(%rbp)
	movq	$0, -3776(%rbp)
	movl	$-1, -3768(%rbp)
	pushq	-3768(%rbp)
	pushq	-3776(%rbp)
	pushq	-3784(%rbp)
	pushq	-3792(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	jmp	.L2927
.L3029:
	movl	$3, %esi
	movl	%r15d, %edi
	call	_ZN2v88internal8compiler10Truncation11LessGeneralENS2_14TruncationKindES3_@PLT
	testb	%al, %al
	je	.L3555
	movb	$5, -4080(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$3, -4076(%rbp)
	movl	$5, %edx
	movl	$0, -4072(%rbp)
	movb	$7, -4068(%rbp)
	movq	$0, -4064(%rbp)
	movl	$-1, -4056(%rbp)
	pushq	-4056(%rbp)
	pushq	-4064(%rbp)
	pushq	-4072(%rbp)
	pushq	-4080(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	(%r12), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int64ConstantEl@PLT
	movq	%rax, %rbx
	movq	0(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Int64SubEv@PLT
	movq	%rbx, %r8
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	%rax, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector25ChangeUnaryToPureBinaryOpEPNS1_4NodeEPKNS1_8OperatorEiS4_
	jmp	.L2927
.L3030:
	call	_ZN2v88internal8compiler27NumberOperationParametersOfEPKNS1_8OperatorE@PLT
	movzbl	(%rax), %esi
	cmpb	$2, %sil
	ja	.L4136
	leaq	8(%rax), %rdx
	leaq	-1744(%rbp), %rdi
	movl	$1, %ecx
	call	_ZN2v88internal8compiler12_GLOBAL__N_130CheckedUseInfoAsWord32FromHintENS1_19NumberOperationHintERKNS1_14FeedbackSourceENS1_13IdentifyZerosE
	movl	$1099, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	pushq	-1720(%rbp)
	movl	$4, %edx
	pushq	-1728(%rbp)
	pushq	-1736(%rbp)
	pushq	-1744(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
.L3654:
	cmpl	$2, 120(%r12)
	jne	.L2927
	movzbl	23(%r14), %eax
	movq	32(%r14), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L3658
	movq	16(%rdx), %rdx
.L3658:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector16DeferReplacementEPNS1_4NodeES4_
	jmp	.L2927
.L3033:
	movzbl	23(%r14), %eax
	movq	32(%r14), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L3516
	movq	16(%rdx), %rdx
.L3516:
	movl	20(%rdx), %eax
	movq	32(%r12), %rsi
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rax
	leaq	(%rsi,%rax,8), %rax
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L3517
	movq	8(%rdx), %rax
.L3517:
	movq	%rax, -144(%rbp)
	cmpq	$3167, %rax
	je	.L3518
	leaq	-144(%rbp), %rdi
	movl	$3167, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L3518
	movb	$13, -4624(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$4, -4620(%rbp)
	movl	$13, %edx
	movl	$1, -4616(%rbp)
	movb	$0, -4612(%rbp)
	movq	$0, -4608(%rbp)
	movl	$-1, -4600(%rbp)
	pushq	-4600(%rbp)
	pushq	-4608(%rbp)
	pushq	-4616(%rbp)
	pushq	-4624(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	(%r14), %rax
	movq	128(%r12), %rdi
	movzwl	16(%rax), %esi
	call	_ZN2v88internal8compiler21RepresentationChanger18Float64OperatorForENS1_8IrOpcode5ValueE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	jmp	.L2927
	.p2align 4,,10
	.p2align 3
.L3034:
	movzbl	23(%r14), %eax
	movq	32(%r14), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L3534
	movq	16(%rdx), %rdx
.L3534:
	movl	20(%rdx), %eax
	movq	32(%r12), %r15
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rax
	leaq	(%r15,%rax,8), %rax
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L3535
	movq	8(%rdx), %rax
.L3535:
	movq	344(%r12), %rdx
	movq	%rax, -144(%rbp)
	movq	112(%rdx), %rsi
	cmpq	%rsi, %rax
	je	.L3536
	leaq	-144(%rbp), %r15
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L3536
	cmpq	$7175, -144(%rbp)
	je	.L3537
	movl	$7175, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L3537
	cmpq	$7243, -144(%rbp)
	je	.L3542
	movl	$7243, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L3542
	movq	344(%r12), %rax
	movq	336(%rax), %rsi
	cmpq	%rsi, -144(%rbp)
	je	.L3545
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L3545
	movb	$13, -4240(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$4, -4236(%rbp)
	movl	$13, %edx
	movl	$1, -4232(%rbp)
	movb	$0, -4228(%rbp)
	movq	$0, -4224(%rbp)
	movl	$-1, -4216(%rbp)
	pushq	-4216(%rbp)
	pushq	-4224(%rbp)
	pushq	-4232(%rbp)
	pushq	-4240(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18SimplifiedLowering22DoNumberToUint8ClampedEPNS1_4NodeE
	jmp	.L2927
	.p2align 4,,10
	.p2align 3
.L3028:
	movl	$3, %esi
	movl	%r15d, %edi
	call	_ZN2v88internal8compiler10Truncation11LessGeneralENS2_14TruncationKindES3_@PLT
	testb	%al, %al
	je	.L3554
	movb	$5, -144(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$3, -140(%rbp)
	movl	$5, %edx
	movl	$0, -136(%rbp)
	movb	$7, -132(%rbp)
	movq	$0, -128(%rbp)
	movl	$-1, -120(%rbp)
	pushq	-120(%rbp)
	pushq	-128(%rbp)
	pushq	-136(%rbp)
	pushq	-144(%rbp)
	pushq	-120(%rbp)
	pushq	-128(%rbp)
	pushq	-136(%rbp)
	pushq	-144(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE
	addq	$64, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	0(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Int64AddEv@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler22RepresentationSelector14ChangeToPureOpEPNS1_4NodeEPKNS1_8OperatorE
	jmp	.L2927
.L3032:
	movb	$5, -4784(%rbp)
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$3, -4780(%rbp)
	movl	$0, -4776(%rbp)
	movb	$0, -4772(%rbp)
	movq	$0, -4768(%rbp)
	movl	$-1, -4760(%rbp)
	pushq	-4760(%rbp)
	pushq	-4768(%rbp)
	pushq	-4776(%rbp)
	pushq	-4784(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12ProcessInputEPNS1_4NodeEiNS1_7UseInfoE
	movl	20(%r14), %eax
	addq	$32, %rsp
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rdx
	movq	32(%r12), %rax
	leaq	(%rax,%rdx,8), %rdx
	movl	120(%r12), %eax
	testl	%eax, %eax
	je	.L3513
	cmpl	$1, %eax
	jne	.L2927
	movb	$5, 1(%rdx)
	jmp	.L2927
.L3009:
	movq	%r13, %rcx
	movl	$75432321, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector10VisitCheckEPNS1_4NodeENS1_4TypeEPNS1_18SimplifiedLoweringE
	jmp	.L2927
.L3090:
	movl	20(%r14), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rdx
	movq	32(%r12), %rax
	leaq	(%rax,%rdx,8), %rdx
	movl	120(%r12), %eax
	testl	%eax, %eax
	je	.L3101
	cmpl	$1, %eax
	jne	.L2927
	movb	$5, 1(%rdx)
	jmp	.L2927
.L2951:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector11VisitInputsEPNS1_4NodeE
	movl	20(%r14), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rdx
	movq	32(%r12), %rax
	leaq	(%rax,%rdx,8), %rdx
	movl	120(%r12), %eax
	testl	%eax, %eax
	je	.L3741
	cmpl	$1, %eax
	jne	.L2927
	movb	$7, 1(%rdx)
	jmp	.L2927
.L3010:
	movq	%r13, %rcx
	movl	$75431937, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector10VisitCheckEPNS1_4NodeENS1_4TypeEPNS1_18SimplifiedLoweringE
	jmp	.L2927
.L3087:
	movl	20(%r14), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rdx
	movq	32(%r12), %rax
	leaq	(%rax,%rdx,8), %rdx
	movl	120(%r12), %eax
	testl	%eax, %eax
	je	.L3117
	cmpl	$1, %eax
	jne	.L3119
	movb	$5, 1(%rdx)
	movl	120(%r12), %eax
.L3119:
	cmpl	$2, %eax
	jne	.L2927
	movq	(%r14), %rax
	movq	0(%r13), %rdi
	movq	48(%rax), %rsi
	call	_ZN2v88internal8compiler12MachineGraph14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler22RepresentationSelector16DeferReplacementEPNS1_4NodeES4_
	jmp	.L2927
.L3089:
	movl	20(%r14), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rdx
	movq	32(%r12), %rax
	leaq	(%rax,%rdx,8), %rdx
	movl	120(%r12), %eax
	testl	%eax, %eax
	je	.L3103
	cmpl	$1, %eax
	jne	.L2927
	movb	$5, 1(%rdx)
	jmp	.L2927
.L3049:
	movl	$8396767, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector13BothInputsAreEPNS1_4NodeENS1_4TypeE
	testb	%al, %al
	je	.L3373
	movzbl	23(%r14), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L3374
	leaq	40(%r14), %rax
.L3375:
	movq	(%rax), %rax
	movb	$4, -5552(%rbp)
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$2, -5548(%rbp)
	movl	$4294967295, %ecx
	movl	$4, %edx
	movq	8(%rax), %r15
	movl	$0, -5544(%rbp)
	movb	$0, -5540(%rbp)
	movq	$0, -5536(%rbp)
	movl	$-1, -5528(%rbp)
	movb	$4, -5520(%rbp)
	pushq	-5528(%rbp)
	movb	$2, -5516(%rbp)
	pushq	-5536(%rbp)
	movl	$0, -5512(%rbp)
	pushq	-5544(%rbp)
	movb	$0, -5508(%rbp)
	pushq	-5552(%rbp)
	movq	$0, -5504(%rbp)
	movl	$-1, -5496(%rbp)
	pushq	-5496(%rbp)
	pushq	-5504(%rbp)
	pushq	-5512(%rbp)
	pushq	-5520(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE
	addq	$64, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector16MaskShiftOperandEPNS1_4NodeENS1_4TypeE
	movq	0(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32ShlEv@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler22RepresentationSelector14ChangeToPureOpEPNS1_4NodeEPKNS1_8OperatorE
	jmp	.L2927
.L2997:
	movb	$5, -3120(%rbp)
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$5, -3116(%rbp)
	movl	$1, -3112(%rbp)
	movb	$0, -3108(%rbp)
	movq	$0, -3104(%rbp)
	movl	$-1, -3096(%rbp)
	pushq	-3096(%rbp)
	pushq	-3104(%rbp)
	pushq	-3112(%rbp)
	pushq	-3120(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12ProcessInputEPNS1_4NodeEiNS1_7UseInfoE
	movl	$1, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	addq	$32, %rsp
	call	_ZN2v88internal8compiler22RepresentationSelector22ProcessRemainingInputsEPNS1_4NodeEi
	movl	20(%r14), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rdx
	movq	32(%r12), %rax
	leaq	(%rax,%rdx,8), %rdx
	movl	120(%r12), %eax
	testl	%eax, %eax
	je	.L3577
	cmpl	$1, %eax
	jne	.L2927
	movb	$7, 1(%rdx)
	jmp	.L2927
.L2998:
	movb	$8, -4208(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$5, -4204(%rbp)
	movl	$7, %edx
	movl	$1, -4200(%rbp)
	movb	$0, -4196(%rbp)
	movq	$0, -4192(%rbp)
	movl	$-1, -4184(%rbp)
	pushq	-4184(%rbp)
	pushq	-4192(%rbp)
	pushq	-4200(%rbp)
	pushq	-4208(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	jmp	.L2927
.L2955:
	movb	$7, -496(%rbp)
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$5, -492(%rbp)
	movl	$4294967295, %ecx
	movl	$1, -488(%rbp)
	movb	$6, -484(%rbp)
	movq	$0, -480(%rbp)
	movl	$-1, -472(%rbp)
	pushq	-472(%rbp)
	pushq	-480(%rbp)
	pushq	-488(%rbp)
	pushq	-496(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	jmp	.L2927
.L2956:
	movzbl	23(%r14), %eax
	movq	32(%r12), %rdx
	leaq	32(%r14), %rbx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L3723
	movq	16(%rbx), %rcx
	movl	20(%rcx), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rax
	movq	24(%rdx,%rax,8), %rax
	testq	%rax, %rax
	jne	.L4095
	movq	8(%rcx), %rax
.L4095:
	movq	%rax, -176(%rbp)
	leaq	24(%rbx), %rax
.L3728:
	movq	(%rax), %rcx
	movl	20(%rcx), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rax
	movq	24(%rdx,%rax,8), %rax
	testq	%rax, %rax
	jne	.L3729
	movq	8(%rcx), %rax
.L3729:
	movb	$8, -432(%rbp)
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$5, -428(%rbp)
	movl	$1, -424(%rbp)
	movb	$0, -420(%rbp)
	movq	$0, -416(%rbp)
	movl	$-1, -408(%rbp)
	pushq	-408(%rbp)
	pushq	-416(%rbp)
	pushq	-424(%rbp)
	pushq	-432(%rbp)
	movq	%rax, -144(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12ProcessInputEPNS1_4NodeEiNS1_7UseInfoE
	addq	$32, %rsp
	movq	%r14, %rsi
	movb	$8, -400(%rbp)
	movb	$5, -396(%rbp)
	movl	$1, %edx
	movq	%r12, %rdi
	movl	$1, -392(%rbp)
	movb	$0, -388(%rbp)
	movq	$0, -384(%rbp)
	movl	$-1, -376(%rbp)
	pushq	-376(%rbp)
	pushq	-384(%rbp)
	pushq	-392(%rbp)
	pushq	-400(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12ProcessInputEPNS1_4NodeEiNS1_7UseInfoE
	addq	$32, %rsp
	movq	%r14, %rsi
	movb	$4, -368(%rbp)
	movb	$2, -364(%rbp)
	movl	$2, %edx
	movq	%r12, %rdi
	movl	$0, -360(%rbp)
	movb	$0, -356(%rbp)
	movq	$0, -352(%rbp)
	movl	$-1, -344(%rbp)
	pushq	-344(%rbp)
	pushq	-352(%rbp)
	pushq	-360(%rbp)
	pushq	-368(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12ProcessInputEPNS1_4NodeEiNS1_7UseInfoE
	addq	$32, %rsp
	movq	%r14, %rsi
	movb	$4, -336(%rbp)
	movb	$2, -332(%rbp)
	movl	$3, %edx
	movq	%r12, %rdi
	movl	$0, -328(%rbp)
	movb	$0, -324(%rbp)
	movq	$0, -320(%rbp)
	movl	$-1, -312(%rbp)
	pushq	-312(%rbp)
	pushq	-320(%rbp)
	pushq	-328(%rbp)
	pushq	-336(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12ProcessInputEPNS1_4NodeEiNS1_7UseInfoE
	movl	$4, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	addq	$32, %rsp
	call	_ZN2v88internal8compiler22RepresentationSelector22ProcessRemainingInputsEPNS1_4NodeEi
	movl	20(%r14), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rdx
	movq	32(%r12), %rax
	leaq	(%rax,%rdx,8), %rdx
	movl	120(%r12), %eax
	testl	%eax, %eax
	je	.L3730
	cmpl	$1, %eax
	jne	.L3732
	movb	$7, 1(%rdx)
	movl	120(%r12), %eax
.L3732:
	cmpl	$2, %eax
	jne	.L2927
	cmpq	$1, -176(%rbp)
	je	.L3738
	cmpq	$1, -144(%rbp)
	je	.L3738
	leaq	-176(%rbp), %rdi
	call	_ZNK2v88internal8compiler4Type3MaxEv@PLT
	leaq	-144(%rbp), %rdi
	movsd	%xmm0, -6008(%rbp)
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	comisd	-6008(%rbp), %xmm0
	jbe	.L2927
.L3738:
	movzbl	23(%r14), %eax
	addq	$8, %rbx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L3740
	movq	32(%r14), %rax
	leaq	24(%rax), %rbx
.L3740:
	movq	(%rbx), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector16DeferReplacementEPNS1_4NodeES4_
	jmp	.L2927
.L2957:
	movb	$8, -144(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$5, -140(%rbp)
	movl	$7, %edx
	movl	$1, -136(%rbp)
	movb	$0, -132(%rbp)
	movq	$0, -128(%rbp)
	movl	$-1, -120(%rbp)
	pushq	-120(%rbp)
	pushq	-128(%rbp)
	pushq	-136(%rbp)
	pushq	-144(%rbp)
	pushq	-120(%rbp)
	pushq	-128(%rbp)
	pushq	-136(%rbp)
	pushq	-144(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE
	addq	$64, %rsp
	jmp	.L2927
.L2959:
	movb	$4, -4176(%rbp)
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$2, -4172(%rbp)
	movl	$0, -4168(%rbp)
	movb	$0, -4164(%rbp)
	movq	$0, -4160(%rbp)
	movl	$-1, -4152(%rbp)
	pushq	-4152(%rbp)
	pushq	-4160(%rbp)
	pushq	-4168(%rbp)
	pushq	-4176(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12ProcessInputEPNS1_4NodeEiNS1_7UseInfoE
	addq	$32, %rsp
	movq	%r14, %rsi
	movb	$8, -4144(%rbp)
	movb	$5, -4140(%rbp)
	movl	$1, %edx
	movq	%r12, %rdi
	movl	$1, -4136(%rbp)
	movb	$0, -4132(%rbp)
	movq	$0, -4128(%rbp)
	movl	$-1, -4120(%rbp)
	pushq	-4120(%rbp)
	pushq	-4128(%rbp)
	pushq	-4136(%rbp)
	pushq	-4144(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12ProcessInputEPNS1_4NodeEiNS1_7UseInfoE
	addq	$32, %rsp
	movq	%r14, %rsi
	movb	$8, -4112(%rbp)
	movb	$5, -4108(%rbp)
	movl	$2, %edx
	movq	%r12, %rdi
	movl	$1, -4104(%rbp)
	movb	$0, -4100(%rbp)
	movq	$0, -4096(%rbp)
	movl	$-1, -4088(%rbp)
	pushq	-4088(%rbp)
	pushq	-4096(%rbp)
	pushq	-4104(%rbp)
	pushq	-4112(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12ProcessInputEPNS1_4NodeEiNS1_7UseInfoE
	movl	20(%r14), %eax
	addq	$32, %rsp
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rdx
	movq	32(%r12), %rax
	leaq	(%rax,%rdx,8), %rdx
	movl	120(%r12), %eax
	testl	%eax, %eax
	je	.L3552
	cmpl	$1, %eax
	jne	.L2927
	movb	$7, 1(%rdx)
	jmp	.L2927
.L2962:
	movb	$5, -848(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$5, -844(%rbp)
	movl	$6, %edx
	movl	$1, -840(%rbp)
	movb	$0, -836(%rbp)
	movq	$0, -832(%rbp)
	movl	$-1, -824(%rbp)
	pushq	-824(%rbp)
	pushq	-832(%rbp)
	pushq	-840(%rbp)
	pushq	-848(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	jmp	.L2927
.L3053:
	movl	$1103, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector13BothInputsAreEPNS1_4NodeENS1_4TypeE
	testb	%al, %al
	je	.L3260
	movq	8(%r14), %rax
	movq	%rax, -208(%rbp)
	cmpq	$1099, %rax
	je	.L3264
	leaq	-208(%rbp), %rdi
	movl	$1099, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L3264
	movq	8(%r14), %rax
	movq	%rax, -176(%rbp)
	cmpq	$1031, %rax
	je	.L3264
	leaq	-176(%rbp), %rdi
	movl	$1031, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L3264
	movl	$2, %esi
	movl	%r15d, %edi
	call	_ZN2v88internal8compiler10Truncation11LessGeneralENS2_14TruncationKindES3_@PLT
	testb	%al, %al
	je	.L3260
	movq	8(%r14), %rax
	movq	344(%r12), %rdx
	movq	%rax, -144(%rbp)
	movq	400(%rdx), %rsi
	cmpq	%rsi, %rax
	je	.L3264
	leaq	-144(%rbp), %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L3264
.L3260:
	movq	(%r14), %rdi
	call	_ZN2v88internal8compiler21NumberOperationHintOfEPKNS1_8OperatorE@PLT
	movzbl	23(%r14), %edx
	movl	%eax, %ebx
	leaq	32(%r14), %rax
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L4137
	movq	32(%r14), %rsi
	movq	32(%r12), %rdx
	movl	20(%rsi), %ecx
	andl	$16777215, %ecx
	leaq	(%rcx,%rcx,4), %rcx
	movq	24(%rdx,%rcx,8), %r13
	testq	%r13, %r13
	jne	.L3776
	movq	8(%rsi), %r13
.L3776:
	addq	$8, %rax
.L3272:
	movq	(%rax), %rcx
	movl	20(%rcx), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rax
	movq	24(%rdx,%rax,8), %r15
	testq	%r15, %r15
	jne	.L3273
	movq	8(%rcx), %r15
.L3273:
	movl	$1099, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector13BothInputsAreEPNS1_4NodeENS1_4TypeE
	movl	%eax, %r8d
	movl	%ebx, %eax
	andl	$-3, %eax
	testb	%r8b, %r8b
	je	.L3274
	testb	%al, %al
	je	.L4138
.L3275:
	movb	$13, -144(%rbp)
	movl	$7263, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$5, -140(%rbp)
	movl	$13, %edx
	movl	$1, -136(%rbp)
	movb	$5, -132(%rbp)
	movq	$0, -128(%rbp)
	movl	$-1, -120(%rbp)
	pushq	-120(%rbp)
	pushq	-128(%rbp)
	pushq	-136(%rbp)
	pushq	-144(%rbp)
	pushq	-120(%rbp)
	pushq	-128(%rbp)
	pushq	-136(%rbp)
	pushq	-144(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE
	addq	$64, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	(%r14), %rax
	movq	128(%r12), %rdi
	movzwl	16(%rax), %esi
	call	_ZN2v88internal8compiler21RepresentationChanger18Float64OperatorForENS1_8IrOpcode5ValueE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler22RepresentationSelector14ChangeToPureOpEPNS1_4NodeEPKNS1_8OperatorE
	jmp	.L2927
	.p2align 4,,10
	.p2align 3
.L2963:
	movl	20(%r14), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rdx
	movq	32(%r12), %rax
	leaq	(%rax,%rdx,8), %rdx
	movl	120(%r12), %eax
	testl	%eax, %eax
	je	.L3699
	cmpl	$1, %eax
	jne	.L2927
	movb	$5, 1(%rdx)
	jmp	.L2927
.L2968:
	movq	%r13, %rcx
	movl	$75431937, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector13VisitObjectIsEPNS1_4NodeENS1_4TypeEPNS1_18SimplifiedLoweringE
	jmp	.L2927
.L3037:
	movb	$4, -4464(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$2, -4460(%rbp)
	movl	$4, %edx
	movl	$0, -4456(%rbp)
	movb	$0, -4452(%rbp)
	movq	$0, -4448(%rbp)
	movl	$-1, -4440(%rbp)
	pushq	-4440(%rbp)
	pushq	-4448(%rbp)
	pushq	-4456(%rbp)
	pushq	-4464(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movzbl	23(%r14), %eax
	movq	32(%r14), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L3532
	movq	16(%rdx), %rdx
.L3532:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector16DeferReplacementEPNS1_4NodeES4_
	jmp	.L2927
.L3038:
	movzbl	23(%r14), %eax
	movq	32(%r14), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L3523
	movq	16(%rdx), %rdx
.L3523:
	movl	20(%rdx), %eax
	movq	32(%r12), %rcx
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rax
	leaq	(%rcx,%rax,8), %rax
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L3524
	movq	8(%rdx), %rax
.L3524:
	movq	%rax, -144(%rbp)
	cmpq	$7247, %rax
	je	.L3525
	leaq	-144(%rbp), %r15
	movl	$7247, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L3525
	cmpq	$3167, -144(%rbp)
	je	.L3526
	movl	$3167, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L3526
	movb	$13, -4496(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$4, -4492(%rbp)
	movl	$1, %edx
	movl	$0, -4488(%rbp)
	movb	$0, -4484(%rbp)
	movq	$0, -4480(%rbp)
	movl	$-1, -4472(%rbp)
	pushq	-4472(%rbp)
	pushq	-4480(%rbp)
	pushq	-4488(%rbp)
	pushq	-4496(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18SimplifiedLowering13DoNumberToBitEPNS1_4NodeE
	jmp	.L2927
	.p2align 4,,10
	.p2align 3
.L3041:
	movb	$13, -4592(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$4, -4588(%rbp)
	movl	$13, %edx
	movl	$1, -4584(%rbp)
	movb	$0, -4580(%rbp)
	movq	$0, -4576(%rbp)
	movl	$-1, -4568(%rbp)
	pushq	-4568(%rbp)
	pushq	-4576(%rbp)
	pushq	-4584(%rbp)
	pushq	-4592(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	(%r14), %rax
	movq	128(%r12), %rdi
	movzwl	16(%rax), %esi
	call	_ZN2v88internal8compiler21RepresentationChanger18Float64OperatorForENS1_8IrOpcode5ValueE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	jmp	.L2927
.L3048:
	movl	$8396767, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector13BothInputsAreEPNS1_4NodeENS1_4TypeE
	testb	%al, %al
	je	.L3383
	movzbl	23(%r14), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L3384
	leaq	40(%r14), %rax
.L3385:
	movq	(%rax), %rax
	movb	$4, -5392(%rbp)
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$2, -5388(%rbp)
	movl	$4294967295, %ecx
	movl	$4, %edx
	movq	8(%rax), %r15
	movl	$0, -5384(%rbp)
	movb	$0, -5380(%rbp)
	movq	$0, -5376(%rbp)
	movl	$-1, -5368(%rbp)
	movb	$4, -5360(%rbp)
	pushq	-5368(%rbp)
	movb	$2, -5356(%rbp)
	pushq	-5376(%rbp)
	movl	$0, -5352(%rbp)
	pushq	-5384(%rbp)
	movb	$0, -5348(%rbp)
	pushq	-5392(%rbp)
	movq	$0, -5344(%rbp)
	movl	$-1, -5336(%rbp)
	pushq	-5336(%rbp)
	pushq	-5344(%rbp)
	pushq	-5352(%rbp)
	pushq	-5360(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE
	addq	$64, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector16MaskShiftOperandEPNS1_4NodeENS1_4TypeE
	movq	0(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32SarEv@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler22RepresentationSelector14ChangeToPureOpEPNS1_4NodeEPKNS1_8OperatorE
	jmp	.L2927
.L3043:
	movb	$13, -4880(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$4, -4876(%rbp)
	movl	$12, %edx
	movl	$1, -4872(%rbp)
	movb	$0, -4868(%rbp)
	movq	$0, -4864(%rbp)
	movl	$-1, -4856(%rbp)
	pushq	-4856(%rbp)
	pushq	-4864(%rbp)
	pushq	-4872(%rbp)
	pushq	-4880(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	(%r14), %rax
	movq	128(%r12), %rdi
	movzwl	16(%rax), %esi
	call	_ZN2v88internal8compiler21RepresentationChanger18Float64OperatorForENS1_8IrOpcode5ValueE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	jmp	.L2927
.L3036:
	movb	$8, -4432(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$5, -4428(%rbp)
	movl	$7, %edx
	movl	$1, -4424(%rbp)
	movb	$0, -4420(%rbp)
	movq	$0, -4416(%rbp)
	movl	$-1, -4408(%rbp)
	pushq	-4408(%rbp)
	pushq	-4416(%rbp)
	pushq	-4424(%rbp)
	pushq	-4432(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	jmp	.L2927
.L3042:
	movl	$1099, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector7InputIsEPNS1_4NodeENS1_4TypeE.isra.0
	testb	%al, %al
	je	.L3515
	movb	$4, -4720(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$2, -4716(%rbp)
	movl	$4, %edx
	movl	$0, -4712(%rbp)
	movb	$0, -4708(%rbp)
	movq	$0, -4704(%rbp)
	movl	$-1, -4696(%rbp)
	pushq	-4696(%rbp)
	pushq	-4704(%rbp)
	pushq	-4712(%rbp)
	pushq	-4720(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18SimplifiedLowering9Int32SignEPNS1_4NodeE
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler22RepresentationSelector16DeferReplacementEPNS1_4NodeES4_
	jmp	.L2927
.L3063:
	movzbl	23(%r14), %edx
	movq	32(%r12), %rcx
	leaq	32(%r14), %rax
	movq	32(%r14), %rsi
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L4102
	leaq	16(%rsi), %rax
	movq	16(%rsi), %rsi
.L4102:
	movl	20(%rsi), %edx
	andl	$16777215, %edx
	leaq	(%rdx,%rdx,4), %rdx
	movq	24(%rcx,%rdx,8), %rdx
	testq	%rdx, %rdx
	jne	.L4084
	movq	8(%rsi), %rdx
.L4084:
	addq	$8, %rax
	movq	%rdx, -240(%rbp)
	movq	(%rax), %rsi
	movl	20(%rsi), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rax
	movq	24(%rcx,%rax,8), %rax
	testq	%rax, %rax
	jne	.L3339
	movq	8(%rsi), %rax
.L3339:
	movq	%rax, -208(%rbp)
	cmpq	$7175, %rdx
	je	.L3340
	leaq	-240(%rbp), %rdi
	movl	$7175, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L3341
	movq	-208(%rbp), %rax
.L3340:
	cmpq	$7175, %rax
	je	.L3345
	leaq	-208(%rbp), %rdi
	movl	$7175, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L3341
.L3345:
	movl	$2, %esi
	movl	%r15d, %edi
	call	_ZN2v88internal8compiler10Truncation11LessGeneralENS2_14TruncationKindES3_@PLT
	testb	%al, %al
	jne	.L3343
	movl	20(%r14), %eax
	movq	32(%r12), %rbx
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rax
	leaq	(%rbx,%rax,8), %rax
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L3348
	movq	8(%r14), %rax
.L3348:
	movq	%rax, -144(%rbp)
	cmpq	$1031, %rax
	je	.L3343
	leaq	-144(%rbp), %rdi
	movl	$1031, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L3341
.L3343:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector26VisitWord32TruncatingBinopEPNS1_4NodeE
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18SimplifiedLowering9Uint32ModEPNS1_4NodeE
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler22RepresentationSelector16DeferReplacementEPNS1_4NodeES4_
	jmp	.L2927
	.p2align 4,,10
	.p2align 3
.L3065:
	movl	20(%r14), %ecx
	movq	32(%r14), %rdi
	leaq	32(%r14), %rbx
	movl	%ecx, %eax
	shrl	$24, %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L3276
	movq	16(%rdi), %rdi
.L3276:
	movl	20(%rdi), %edx
	movq	32(%r12), %rsi
	andl	$16777215, %edx
	leaq	(%rdx,%rdx,4), %rdx
	movq	24(%rsi,%rdx,8), %rdx
	testq	%rdx, %rdx
	jne	.L3277
	movq	8(%rdi), %rdx
.L3277:
	movq	%rdx, -272(%rbp)
	cmpq	$1103, %rdx
	je	.L3278
	leaq	-272(%rbp), %rdi
	movl	$1103, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L3279
	movl	20(%r14), %ecx
	movq	32(%r12), %rsi
	movl	%ecx, %eax
	shrl	$24, %eax
	andl	$15, %eax
.L3278:
	addq	$8, %rbx
	cmpl	$15, %eax
	jne	.L3281
	movq	32(%r14), %rax
	leaq	24(%rax), %rbx
.L3281:
	movq	(%rbx), %rdx
	movl	20(%rdx), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rax
	movq	24(%rsi,%rax,8), %rax
	testq	%rax, %rax
	jne	.L3282
	movq	8(%rdx), %rax
.L3282:
	movq	%rax, -240(%rbp)
	cmpq	$1103, %rax
	je	.L3283
	leaq	-240(%rbp), %rdi
	movl	$1103, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L3279
	movl	20(%r14), %ecx
	movq	32(%r12), %rsi
.L3283:
	andl	$16777215, %ecx
	leaq	(%rcx,%rcx,4), %rax
	movq	24(%rsi,%rax,8), %rax
	testq	%rax, %rax
	jne	.L3284
	movq	8(%r14), %rax
.L3284:
	movq	%rax, -208(%rbp)
	cmpq	$1099, %rax
	je	.L3287
	leaq	-208(%rbp), %rdi
	movl	$1099, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L3287
	movl	20(%r14), %eax
	andl	$16777215, %eax
	imulq	$40, %rax, %rax
	addq	32(%r12), %rax
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L3288
	movq	8(%r14), %rax
.L3288:
	movq	%rax, -176(%rbp)
	cmpq	$1031, %rax
	je	.L3287
	leaq	-176(%rbp), %rdi
	movl	$1031, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L3287
	movl	$2, %esi
	movl	%r15d, %edi
	call	_ZN2v88internal8compiler10Truncation11LessGeneralENS2_14TruncationKindES3_@PLT
	testb	%al, %al
	je	.L3279
	movl	20(%r14), %eax
	andl	$16777215, %eax
	imulq	$40, %rax, %rax
	addq	32(%r12), %rax
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L3289
	movq	8(%r14), %rax
.L3289:
	movq	344(%r12), %rdx
	movq	%rax, -144(%rbp)
	movq	400(%rdx), %rsi
	cmpq	%rsi, %rax
	je	.L3287
	leaq	-144(%rbp), %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L3287
.L3279:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector17VisitFloat64BinopEPNS1_4NodeE
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	(%r14), %rax
	movq	128(%r12), %rdi
	movzwl	16(%rax), %esi
	call	_ZN2v88internal8compiler21RepresentationChanger18Float64OperatorForENS1_8IrOpcode5ValueE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler22RepresentationSelector14ChangeToPureOpEPNS1_4NodeEPKNS1_8OperatorE
	jmp	.L2927
	.p2align 4,,10
	.p2align 3
.L3051:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector29VisitSpeculativeNumberModulusEPNS1_4NodeENS1_10TruncationEPNS1_18SimplifiedLoweringE
	jmp	.L2927
.L3064:
	movzbl	23(%r14), %eax
	movq	32(%r14), %rcx
	leaq	32(%r14), %rbx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L3305
	movq	16(%rcx), %rcx
.L3305:
	movl	20(%rcx), %edx
	movq	32(%r12), %rsi
	andl	$16777215, %edx
	leaq	(%rdx,%rdx,4), %rdx
	movq	24(%rsi,%rdx,8), %rdx
	testq	%rdx, %rdx
	jne	.L3306
	movq	8(%rcx), %rdx
.L3306:
	movq	%rdx, -208(%rbp)
	cmpq	$1031, %rdx
	je	.L3307
	leaq	-208(%rbp), %rdi
	movl	$1031, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L3308
	movzbl	23(%r14), %eax
	movq	32(%r12), %rsi
	andl	$15, %eax
.L3307:
	cmpl	$15, %eax
	je	.L3309
	leaq	8(%rbx), %rax
.L3310:
	movq	(%rax), %rdx
	movl	20(%rdx), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rax
	movq	24(%rsi,%rax,8), %rax
	testq	%rax, %rax
	jne	.L3311
	movq	8(%rdx), %rax
.L3311:
	movq	%rax, -176(%rbp)
	cmpq	$1031, %rax
	je	.L3315
	leaq	-176(%rbp), %rdi
	movl	$1031, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L3308
.L3315:
	movl	$2, %esi
	movl	%r15d, %edi
	call	_ZN2v88internal8compiler10Truncation11LessGeneralENS2_14TruncationKindES3_@PLT
	testb	%al, %al
	jne	.L3313
	movl	20(%r14), %eax
	movq	32(%r12), %rdx
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rax
	leaq	(%rdx,%rax,8), %rax
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L3317
	movq	8(%r14), %rax
.L3317:
	movq	%rax, -144(%rbp)
	cmpq	$1031, %rax
	je	.L3313
	leaq	-144(%rbp), %rdi
	movl	$1031, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L3308
.L3313:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector26VisitWord32TruncatingBinopEPNS1_4NodeE
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18SimplifiedLowering9Uint32DivEPNS1_4NodeE
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler22RepresentationSelector16DeferReplacementEPNS1_4NodeES4_
	jmp	.L2927
	.p2align 4,,10
	.p2align 3
.L3061:
	movzbl	23(%r14), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L3370
	leaq	40(%r14), %rax
.L3371:
	movq	(%rax), %rax
	movb	$4, -5616(%rbp)
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$2, -5612(%rbp)
	movl	$4294967295, %ecx
	movl	$4, %edx
	movq	8(%rax), %r15
	movl	$0, -5608(%rbp)
	movb	$0, -5604(%rbp)
	movq	$0, -5600(%rbp)
	movl	$-1, -5592(%rbp)
	movb	$4, -5584(%rbp)
	pushq	-5592(%rbp)
	movb	$2, -5580(%rbp)
	pushq	-5600(%rbp)
	movl	$0, -5576(%rbp)
	pushq	-5608(%rbp)
	movb	$0, -5572(%rbp)
	pushq	-5616(%rbp)
	movq	$0, -5568(%rbp)
	movl	$-1, -5560(%rbp)
	pushq	-5560(%rbp)
	pushq	-5568(%rbp)
	pushq	-5576(%rbp)
	pushq	-5584(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE
	addq	$64, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector16MaskShiftOperandEPNS1_4NodeENS1_4TypeE
	movq	0(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32ShlEv@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler22RepresentationSelector14ChangeToPureOpEPNS1_4NodeEPKNS1_8OperatorE
	jmp	.L2927
.L3073:
	movzbl	23(%r14), %edx
	movq	32(%r12), %rcx
	leaq	32(%r14), %rax
	movq	32(%r14), %rsi
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L4098
	leaq	16(%rsi), %rax
	movq	16(%rsi), %rsi
.L4098:
	movl	20(%rsi), %edx
	andl	$16777215, %edx
	leaq	(%rdx,%rdx,4), %rdx
	movq	24(%rcx,%rdx,8), %rdx
	testq	%rdx, %rdx
	jne	.L4075
	movq	8(%rsi), %rdx
.L4075:
	addq	$8, %rax
	movq	%rdx, -208(%rbp)
	movq	(%rax), %rsi
	movl	20(%rsi), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rax
	movq	24(%rcx,%rax,8), %rax
	testq	%rax, %rax
	jne	.L3170
	movq	8(%rsi), %rax
.L3170:
	movq	%rax, -176(%rbp)
	cmpq	$3079, %rdx
	je	.L3171
	leaq	-208(%rbp), %rdi
	movl	$3079, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L3179
	movq	-176(%rbp), %rax
.L3171:
	cmpq	$3079, %rax
	je	.L3178
	leaq	-176(%rbp), %rdi
	movl	$3079, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L3178
.L3179:
	cmpq	$7175, -208(%rbp)
	je	.L3173
	leaq	-208(%rbp), %rdi
	movl	$7175, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L3182
.L3173:
	cmpq	$7175, -176(%rbp)
	je	.L3180
	leaq	-176(%rbp), %rdi
	movl	$7175, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L3182
.L3180:
	movq	344(%r12), %rax
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	312(%rax), %rdx
	call	_ZN2v88internal8compiler22RepresentationSelector16OneInputCannotBeEPNS1_4NodeENS1_4TypeE
	testb	%al, %al
	jne	.L3178
.L3182:
	cmpq	$3147, -208(%rbp)
	je	.L3187
	leaq	-208(%rbp), %rdi
	movl	$3147, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L3187
.L3191:
	cmpq	$7243, -208(%rbp)
	je	.L3188
	leaq	-208(%rbp), %rdi
	movl	$7243, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L3194
.L3188:
	cmpq	$7243, -176(%rbp)
	je	.L3192
	leaq	-176(%rbp), %rdi
	movl	$7243, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L3194
.L3192:
	movq	344(%r12), %rax
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	312(%rax), %rdx
	call	_ZN2v88internal8compiler22RepresentationSelector16OneInputCannotBeEPNS1_4NodeENS1_4TypeE
	testb	%al, %al
	jne	.L3185
.L3194:
	movb	$13, -144(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$4, -140(%rbp)
	movl	$1, %edx
	movl	$0, -136(%rbp)
	movb	$0, -132(%rbp)
	movq	$0, -128(%rbp)
	movl	$-1, -120(%rbp)
	pushq	-120(%rbp)
	pushq	-128(%rbp)
	pushq	-136(%rbp)
	pushq	-144(%rbp)
	pushq	-120(%rbp)
	pushq	-128(%rbp)
	pushq	-136(%rbp)
	pushq	-144(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE
	addq	$64, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	(%r14), %rax
	movq	128(%r12), %rdi
	movzwl	16(%rax), %esi
	call	_ZN2v88internal8compiler21RepresentationChanger18Float64OperatorForENS1_8IrOpcode5ValueE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	jmp	.L2927
	.p2align 4,,10
	.p2align 3
.L3060:
	movzbl	23(%r14), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L3380
	leaq	40(%r14), %rax
.L3381:
	movq	(%rax), %rax
	movb	$4, -5456(%rbp)
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$2, -5452(%rbp)
	movl	$4294967295, %ecx
	movl	$4, %edx
	movq	8(%rax), %r15
	movl	$0, -5448(%rbp)
	movb	$0, -5444(%rbp)
	movq	$0, -5440(%rbp)
	movl	$-1, -5432(%rbp)
	movb	$4, -5424(%rbp)
	pushq	-5432(%rbp)
	movb	$2, -5420(%rbp)
	pushq	-5440(%rbp)
	movl	$0, -5416(%rbp)
	pushq	-5448(%rbp)
	movb	$0, -5412(%rbp)
	pushq	-5456(%rbp)
	movq	$0, -5408(%rbp)
	movl	$-1, -5400(%rbp)
	pushq	-5400(%rbp)
	pushq	-5408(%rbp)
	pushq	-5416(%rbp)
	pushq	-5424(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE
	addq	$64, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector16MaskShiftOperandEPNS1_4NodeENS1_4TypeE
	movq	0(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32SarEv@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler22RepresentationSelector14ChangeToPureOpEPNS1_4NodeEPKNS1_8OperatorE
	jmp	.L2927
.L2966:
	movq	%r13, %rcx
	movl	$16417, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector13VisitObjectIsEPNS1_4NodeENS1_4TypeEPNS1_18SimplifiedLoweringE
	jmp	.L2927
.L2970:
	movq	%r13, %rcx
	movl	$68288513, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector13VisitObjectIsEPNS1_4NodeENS1_4TypeEPNS1_18SimplifiedLoweringE
	jmp	.L2927
.L2971:
	movb	$13, -912(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$4, -908(%rbp)
	movl	$1, %edx
	movl	$1, -904(%rbp)
	movb	$0, -900(%rbp)
	movq	$0, -896(%rbp)
	movl	$-1, -888(%rbp)
	pushq	-888(%rbp)
	pushq	-896(%rbp)
	pushq	-904(%rbp)
	pushq	-912(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	jmp	.L2927
.L2969:
	movq	%r13, %rcx
	movl	$7263, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector13VisitObjectIsEPNS1_4NodeENS1_4TypeEPNS1_18SimplifiedLoweringE
	jmp	.L2927
.L2965:
	movq	%r13, %rcx
	movl	$8193, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector13VisitObjectIsEPNS1_4NodeENS1_4TypeEPNS1_18SimplifiedLoweringE
	jmp	.L2927
.L2967:
	movb	$8, -880(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$5, -876(%rbp)
	movl	$1, %edx
	movl	$1, -872(%rbp)
	movb	$0, -868(%rbp)
	movq	$0, -864(%rbp)
	movl	$-1, -856(%rbp)
	pushq	-856(%rbp)
	pushq	-864(%rbp)
	pushq	-872(%rbp)
	pushq	-880(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	jmp	.L2927
.L3070:
	movb	$8, -144(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$5, -140(%rbp)
	movl	$1, %edx
	movl	$1, -136(%rbp)
	movb	$0, -132(%rbp)
	movq	$0, -128(%rbp)
	movl	$-1, -120(%rbp)
	pushq	-120(%rbp)
	pushq	-128(%rbp)
	pushq	-136(%rbp)
	pushq	-144(%rbp)
	pushq	-120(%rbp)
	pushq	-128(%rbp)
	pushq	-136(%rbp)
	pushq	-144(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE
	addq	$64, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	0(%r13), %rax
	movq	16(%rax), %rdi
	cmpb	$4, 16(%rdi)
	je	.L4139
	call	_ZN2v88internal8compiler22MachineOperatorBuilder11Word64EqualEv@PLT
	movq	%rax, %rsi
.L3549:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	jmp	.L2927
.L3068:
	movb	$8, -144(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$5, -140(%rbp)
	movl	$7, %edx
	movl	$1, -136(%rbp)
	movb	$0, -132(%rbp)
	movq	$0, -128(%rbp)
	movl	$-1, -120(%rbp)
	pushq	-120(%rbp)
	pushq	-128(%rbp)
	pushq	-136(%rbp)
	pushq	-144(%rbp)
	pushq	-120(%rbp)
	pushq	-128(%rbp)
	pushq	-136(%rbp)
	pushq	-144(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE
	addq	$64, %rsp
	jmp	.L2927
.L2991:
	call	_ZN2v88internal8compiler13FieldAccessOfEPKNS1_8OperatorE@PLT
	movdqu	(%rax), %xmm2
	movaps	%xmm2, -144(%rbp)
	movdqu	16(%rax), %xmm3
	movaps	%xmm3, -128(%rbp)
	movdqu	32(%rax), %xmm4
	movaps	%xmm4, -112(%rbp)
	movq	48(%rax), %rax
	movq	%rax, -96(%rbp)
	movzbl	23(%r14), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L3581
	leaq	40(%r14), %rax
.L3582:
	movq	(%rax), %rbx
	movzbl	-112(%rbp), %r13d
	movl	20(%rbx), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rdx
	movq	32(%r12), %rax
	leaq	(%rax,%rdx,8), %rdx
	cmpb	$8, %r13b
	je	.L4140
.L3584:
	movl	-140(%rbp), %eax
	movq	-120(%rbp), %rcx
	movq	%rbx, %r9
	movq	%r12, %rdi
	movzbl	-144(%rbp), %esi
	movzbl	1(%rdx), %r8d
	movl	%r13d, %edx
	movl	%eax, -6008(%rbp)
	movl	%esi, %r15d
	call	_ZN2v88internal8compiler22RepresentationSelector19WriteBarrierKindForENS1_14BaseTaggednessENS0_21MachineRepresentationENS1_4TypeES4_PNS1_4NodeE
	movl	%eax, %ebx
	testb	%al, %al
	je	.L3588
	cmpb	$1, %r15b
	jne	.L3588
	movl	-6008(%rbp), %r11d
	movl	$2, %eax
	testl	%r11d, %r11d
	cmove	%eax, %ebx
.L3588:
	cmpb	$1, -144(%rbp)
	je	.L4141
	movb	$5, -2992(%rbp)
.L4092:
	movb	$5, -2988(%rbp)
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$1, -2984(%rbp)
	movb	$0, -2980(%rbp)
	movq	$0, -2976(%rbp)
	movl	$-1, -2968(%rbp)
	pushq	-2968(%rbp)
	pushq	-2976(%rbp)
	pushq	-2984(%rbp)
	pushq	-2992(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12ProcessInputEPNS1_4NodeEiNS1_7UseInfoE
	addq	$32, %rsp
	leaq	-2960(%rbp), %rdi
	movl	%r13d, %esi
	call	_ZN2v88internal8compiler12_GLOBAL__N_135TruncatingUseInfoFromRepresentationENS0_21MachineRepresentationE
	movl	$1, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	pushq	-2936(%rbp)
	pushq	-2944(%rbp)
	pushq	-2952(%rbp)
	pushq	-2960(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12ProcessInputEPNS1_4NodeEiNS1_7UseInfoE
	movl	$2, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	addq	$32, %rsp
	call	_ZN2v88internal8compiler22RepresentationSelector22ProcessRemainingInputsEPNS1_4NodeEi
	movl	20(%r14), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rdx
	movq	32(%r12), %rax
	leaq	(%rax,%rdx,8), %rdx
	movl	120(%r12), %eax
	testl	%eax, %eax
	je	.L3591
	cmpl	$1, %eax
	jne	.L3593
	movb	$0, 1(%rdx)
	movl	120(%r12), %eax
.L3593:
	cmpl	$2, %eax
	jne	.L2927
	cmpb	%bl, -110(%rbp)
	jbe	.L2927
	movq	(%r12), %rax
	movb	%bl, -110(%rbp)
	leaq	-144(%rbp), %rsi
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	jmp	.L2927
.L3093:
	movb	$4, -5904(%rbp)
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$2, -5900(%rbp)
	movl	$0, -5896(%rbp)
	movb	$0, -5892(%rbp)
	movq	$0, -5888(%rbp)
	movl	$-1, -5880(%rbp)
	pushq	-5880(%rbp)
	pushq	-5888(%rbp)
	pushq	-5896(%rbp)
	pushq	-5904(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12ProcessInputEPNS1_4NodeEiNS1_7UseInfoE
	addq	$32, %rsp
	movq	%r14, %rdi
	movb	$0, -5872(%rbp)
	movb	$0, -5868(%rbp)
	movl	$0, -5864(%rbp)
	movb	$0, -5860(%rbp)
	movq	$0, -5856(%rbp)
	movl	$-1, -5848(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15PastEffectIndexEPNS1_4NodeE@PLT
	pushq	-5848(%rbp)
	movq	%r14, %rsi
	movq	%r12, %rdi
	pushq	-5856(%rbp)
	movl	%eax, %edx
	pushq	-5864(%rbp)
	pushq	-5872(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12EnqueueInputEPNS1_4NodeEiNS1_7UseInfoE
	addq	$32, %rsp
	jmp	.L2927
.L3094:
	movb	$1, -5968(%rbp)
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$1, -5964(%rbp)
	movl	$0, -5960(%rbp)
	movb	$0, -5956(%rbp)
	movq	$0, -5952(%rbp)
	movl	$-1, -5944(%rbp)
	pushq	-5944(%rbp)
	pushq	-5952(%rbp)
	pushq	-5960(%rbp)
	pushq	-5968(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12ProcessInputEPNS1_4NodeEiNS1_7UseInfoE
	addq	$32, %rsp
	movq	%r14, %rdi
	movb	$0, -5936(%rbp)
	movb	$0, -5932(%rbp)
	movl	$0, -5928(%rbp)
	movb	$0, -5924(%rbp)
	movq	$0, -5920(%rbp)
	movl	$-1, -5912(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15PastEffectIndexEPNS1_4NodeE@PLT
	pushq	-5912(%rbp)
	movq	%r14, %rsi
	movq	%r12, %rdi
	pushq	-5920(%rbp)
	movl	%eax, %edx
	pushq	-5928(%rbp)
	pushq	-5936(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12EnqueueInputEPNS1_4NodeEiNS1_7UseInfoE
	addq	$32, %rsp
	jmp	.L2927
.L3095:
	movl	20(%r14), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rdx
	movq	32(%r12), %rax
	leaq	(%rax,%rdx,8), %rdx
	movl	120(%r12), %eax
	testl	%eax, %eax
	je	.L3096
	cmpl	$1, %eax
	jne	.L2927
	movb	$8, 1(%rdx)
	jmp	.L2927
.L2990:
	call	_ZN2v88internal8compiler15ElementAccessOfEPKNS1_8OperatorE@PLT
	movdqu	(%rax), %xmm5
	movaps	%xmm5, -144(%rbp)
	movq	16(%rax), %rax
	movq	%rax, -128(%rbp)
	movzbl	23(%r14), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L3597
	leaq	48(%r14), %rax
.L3598:
	movq	(%rax), %rbx
	movzbl	-128(%rbp), %r13d
	movl	20(%rbx), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rdx
	movq	32(%r12), %rax
	leaq	(%rax,%rdx,8), %r15
	cmpb	$8, %r13b
	je	.L4142
.L3600:
	movzbl	-144(%rbp), %esi
	movq	%rbx, %r9
	movl	%r13d, %edx
	movq	%r12, %rdi
	movzbl	1(%r15), %r8d
	movq	-136(%rbp), %rcx
	call	_ZN2v88internal8compiler22RepresentationSelector19WriteBarrierKindForENS1_14BaseTaggednessENS0_21MachineRepresentationENS1_4TypeES4_PNS1_4NodeE
	cmpb	$1, -144(%rbp)
	movl	%eax, %ebx
	je	.L4143
	movb	$5, -2864(%rbp)
.L4094:
	movb	$5, -2860(%rbp)
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$1, -2856(%rbp)
	movb	$0, -2852(%rbp)
	movq	$0, -2848(%rbp)
	movl	$-1, -2840(%rbp)
	pushq	-2840(%rbp)
	pushq	-2848(%rbp)
	pushq	-2856(%rbp)
	pushq	-2864(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12ProcessInputEPNS1_4NodeEiNS1_7UseInfoE
	addq	$32, %rsp
	movq	%r14, %rsi
	movb	$5, -2832(%rbp)
	movb	$5, -2828(%rbp)
	movl	$1, %edx
	movq	%r12, %rdi
	movl	$1, -2824(%rbp)
	movb	$0, -2820(%rbp)
	movq	$0, -2816(%rbp)
	movl	$-1, -2808(%rbp)
	pushq	-2808(%rbp)
	pushq	-2816(%rbp)
	pushq	-2824(%rbp)
	pushq	-2832(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12ProcessInputEPNS1_4NodeEiNS1_7UseInfoE
	addq	$32, %rsp
	leaq	-2800(%rbp), %rdi
	movl	%r13d, %esi
	call	_ZN2v88internal8compiler12_GLOBAL__N_135TruncatingUseInfoFromRepresentationENS0_21MachineRepresentationE
	movl	$2, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	pushq	-2776(%rbp)
	pushq	-2784(%rbp)
	pushq	-2792(%rbp)
	pushq	-2800(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12ProcessInputEPNS1_4NodeEiNS1_7UseInfoE
	movl	$3, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	addq	$32, %rsp
	call	_ZN2v88internal8compiler22RepresentationSelector22ProcessRemainingInputsEPNS1_4NodeEi
	movl	20(%r14), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rdx
	movq	32(%r12), %rax
	leaq	(%rax,%rdx,8), %rdx
	movl	120(%r12), %eax
	testl	%eax, %eax
	je	.L3606
	cmpl	$1, %eax
	jne	.L3608
	movb	$0, 1(%rdx)
	movl	120(%r12), %eax
.L3608:
	cmpl	$2, %eax
	jne	.L2927
	cmpb	%bl, -126(%rbp)
	jbe	.L2927
	movq	(%r12), %rax
	movb	%bl, -126(%rbp)
	leaq	-144(%rbp), %rsi
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder12StoreElementERKNS1_13ElementAccessE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	jmp	.L2927
.L3013:
	call	_ZN2v88internal8compiler21CheckMapsParametersOfEPKNS1_8OperatorE@PLT
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	16(%rax), %rdx
	movl	24(%rax), %eax
	movb	$7, -528(%rbp)
	movb	$5, -524(%rbp)
	movq	%rdx, -512(%rbp)
	xorl	%edx, %edx
	movl	$1, -520(%rbp)
	movb	$6, -516(%rbp)
	movl	%eax, -504(%rbp)
	pushq	-504(%rbp)
	pushq	-512(%rbp)
	pushq	-520(%rbp)
	pushq	-528(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	jmp	.L2927
.L3014:
	movb	$1, -3312(%rbp)
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$1, -3308(%rbp)
	movl	$0, -3304(%rbp)
	movb	$0, -3300(%rbp)
	movq	$0, -3296(%rbp)
	movl	$-1, -3288(%rbp)
	pushq	-3288(%rbp)
	pushq	-3296(%rbp)
	pushq	-3304(%rbp)
	pushq	-3312(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12ProcessInputEPNS1_4NodeEiNS1_7UseInfoE
	movl	$1, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	addq	$32, %rsp
	call	_ZN2v88internal8compiler22RepresentationSelector22ProcessRemainingInputsEPNS1_4NodeEi
	movl	20(%r14), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rdx
	movq	32(%r12), %rax
	leaq	(%rax,%rdx,8), %rdx
	movl	120(%r12), %eax
	testl	%eax, %eax
	je	.L3566
	cmpl	$1, %eax
	jne	.L2927
	movb	$0, 1(%rdx)
	jmp	.L2927
.L3012:
	movzbl	23(%r14), %eax
	movq	32(%r14), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L3568
	movq	16(%rdx), %rdx
.L3568:
	movl	20(%rdx), %eax
	movq	32(%r12), %rbx
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rax
	leaq	(%rbx,%rax,8), %rax
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L3569
	movq	8(%rdx), %rax
.L3569:
	movq	%rax, -144(%rbp)
	cmpq	$7263, %rax
	je	.L3570
	leaq	-144(%rbp), %rdi
	movl	$7263, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L3570
	movb	$8, -3280(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$5, -3276(%rbp)
	movl	$8, %edx
	movl	$1, -3272(%rbp)
	movb	$0, -3268(%rbp)
	movq	$0, -3264(%rbp)
	movl	$-1, -3256(%rbp)
	pushq	-3256(%rbp)
	pushq	-3264(%rbp)
	pushq	-3272(%rbp)
	pushq	-3280(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	jmp	.L2927
.L3069:
	testb	%r15b, %r15b
	je	.L3550
	movl	$7263, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector13BothInputsAreEPNS1_4NodeENS1_4TypeE
	testb	%al, %al
	je	.L3551
	movb	$13, -144(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$4, -140(%rbp)
	movl	$1, %edx
	movl	$1, -136(%rbp)
	movb	$0, -132(%rbp)
	movq	$0, -128(%rbp)
	movl	$-1, -120(%rbp)
	pushq	-120(%rbp)
	pushq	-128(%rbp)
	pushq	-136(%rbp)
	pushq	-144(%rbp)
	pushq	-120(%rbp)
	pushq	-128(%rbp)
	pushq	-136(%rbp)
	pushq	-144(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE
	addq	$64, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	0(%r13), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder15NumberSameValueEv@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	jmp	.L2927
	.p2align 4,,10
	.p2align 3
.L3076:
	movl	20(%r14), %eax
	movq	32(%r12), %rsi
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rax
	leaq	(%rsi,%rax,8), %rax
	movq	24(%rax), %rbx
	testq	%rbx, %rbx
	jne	.L3745
	movq	8(%r14), %rbx
.L3745:
	movq	%r15, %rcx
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector19GetOutputInfoForPhiEPNS1_4NodeENS1_4TypeENS1_10TruncationE
	movb	%r15b, -140(%rbp)
	movb	%al, -144(%rbp)
	movl	%eax, %r13d
	movl	-6008(%rbp), %eax
	movb	$0, -132(%rbp)
	movl	%eax, -136(%rbp)
	movl	120(%r12), %eax
	movq	$0, -128(%rbp)
	movl	$-1, -120(%rbp)
	testl	%eax, %eax
	je	.L4144
	cmpl	$2, %eax
	je	.L4145
.L3747:
	movl	$1, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector22ProcessRemainingInputsEPNS1_4NodeEi
	movl	20(%r14), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rdx
	movq	32(%r12), %rax
	leaq	(%rax,%rdx,8), %rdx
	movl	120(%r12), %eax
	testl	%eax, %eax
	je	.L3748
	cmpl	$1, %eax
	jne	.L2927
	movb	%r13b, 1(%rdx)
	jmp	.L2927
.L2980:
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2935:
	movq	(%r14), %rdi
	movzwl	16(%rdi), %eax
	jmp	.L2930
.L4136:
	leal	-3(%rsi), %edx
	cmpb	$1, %dl
	ja	.L3654
	movq	8(%rax), %rdx
	movl	16(%rax), %eax
	movb	$5, -1708(%rbp)
	movb	$13, -1712(%rbp)
	movl	$1, -1704(%rbp)
	cmpb	$4, %sil
	je	.L3656
	movb	$4, -1700(%rbp)
	movq	%rdx, -1696(%rbp)
	movl	%eax, -1688(%rbp)
.L3657:
	movl	$4294967295, %ecx
	movl	$13, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	pushq	-1688(%rbp)
	pushq	-1696(%rbp)
	pushq	-1704(%rbp)
	pushq	-1712(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	jmp	.L3654
.L2933:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler14NodeProperties31FindSuccessfulControlProjectionEPNS1_4NodeE@PLT
	movq	%rax, -6016(%rbp)
	jmp	.L2934
.L3525:
	movb	$4, -4560(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$2, -4556(%rbp)
	movl	$1, %edx
	movl	$0, -4552(%rbp)
	movb	$0, -4548(%rbp)
	movq	$0, -4544(%rbp)
	movl	$-1, -4536(%rbp)
	pushq	-4536(%rbp)
	pushq	-4544(%rbp)
	pushq	-4552(%rbp)
	pushq	-4560(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18SimplifiedLowering17DoIntegral32ToBitEPNS1_4NodeE
	jmp	.L2927
.L3660:
	movb	$0, -1616(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$0, -1612(%rbp)
	movl	$1, %edx
	movl	$0, -1608(%rbp)
	movb	$0, -1604(%rbp)
	movq	$0, -1600(%rbp)
	movl	$-1, -1592(%rbp)
	pushq	-1592(%rbp)
	pushq	-1600(%rbp)
	pushq	-1608(%rbp)
	pushq	-1616(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	0(%r13), %rdi
	movl	$1, %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler22RepresentationSelector16DeferReplacementEPNS1_4NodeES4_
	jmp	.L2927
.L3406:
	movb	$4, -5104(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$2, -5100(%rbp)
	movl	$4, %edx
	movl	$0, -5096(%rbp)
	movb	$0, -5092(%rbp)
	movq	$0, -5088(%rbp)
	movl	$-1, -5080(%rbp)
	pushq	-5080(%rbp)
	pushq	-5088(%rbp)
	pushq	-5096(%rbp)
	pushq	-5104(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movzbl	23(%r14), %eax
	movq	32(%r14), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L3411
	movq	16(%rdx), %rdx
.L3411:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector16DeferReplacementEPNS1_4NodeES4_
	jmp	.L2927
.L3570:
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitNoopEPNS1_4NodeENS1_10TruncationE
	jmp	.L2927
.L3668:
	movb	$0, -1456(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$0, -1452(%rbp)
	movl	$1, %edx
	movl	$0, -1448(%rbp)
	movb	$0, -1444(%rbp)
	movq	$0, -1440(%rbp)
	movl	$-1, -1432(%rbp)
	pushq	-1432(%rbp)
	pushq	-1440(%rbp)
	pushq	-1448(%rbp)
	pushq	-1456(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	0(%r13), %rdi
	movl	$1, %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler22RepresentationSelector16DeferReplacementEPNS1_4NodeES4_
	jmp	.L2927
.L3684:
	movb	$0, -1168(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$0, -1164(%rbp)
	movl	$1, %edx
	movl	$0, -1160(%rbp)
	movb	$0, -1156(%rbp)
	movq	$0, -1152(%rbp)
	movl	$-1, -1144(%rbp)
	pushq	-1144(%rbp)
	pushq	-1152(%rbp)
	pushq	-1160(%rbp)
	pushq	-1168(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	0(%r13), %rdi
	movl	$1, %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler22RepresentationSelector16DeferReplacementEPNS1_4NodeES4_
	jmp	.L2927
.L3676:
	movb	$0, -1328(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$0, -1324(%rbp)
	movl	$1, %edx
	movl	$0, -1320(%rbp)
	movb	$0, -1316(%rbp)
	movq	$0, -1312(%rbp)
	movl	$-1, -1304(%rbp)
	pushq	-1304(%rbp)
	pushq	-1312(%rbp)
	pushq	-1320(%rbp)
	pushq	-1328(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	0(%r13), %rdi
	movl	$1, %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler22RepresentationSelector16DeferReplacementEPNS1_4NodeES4_
	jmp	.L2927
.L3613:
	movb	$4, -2672(%rbp)
	movl	$2, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$2, -2668(%rbp)
	movl	$0, -2664(%rbp)
	movb	$0, -2660(%rbp)
	movq	$0, -2656(%rbp)
	movl	$-1, -2648(%rbp)
	pushq	-2648(%rbp)
	pushq	-2656(%rbp)
	pushq	-2664(%rbp)
	pushq	-2672(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12ProcessInputEPNS1_4NodeEiNS1_7UseInfoE
	addq	$32, %rsp
	cmpl	$2, 120(%r12)
	jne	.L3617
	movq	(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder23StoreSignedSmallElementEv@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	jmp	.L3617
.L3692:
	movb	$0, -1040(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$0, -1036(%rbp)
	movl	$1, %edx
	movl	$0, -1032(%rbp)
	movb	$0, -1028(%rbp)
	movq	$0, -1024(%rbp)
	movl	$-1, -1016(%rbp)
	pushq	-1016(%rbp)
	pushq	-1024(%rbp)
	pushq	-1032(%rbp)
	pushq	-1040(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	0(%r13), %rdi
	movl	$1, %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler22RepresentationSelector16DeferReplacementEPNS1_4NodeES4_
	jmp	.L2927
.L3758:
	movb	$4, -304(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$2, -300(%rbp)
	movl	$5, %edx
	movl	$0, -296(%rbp)
	movb	$0, -292(%rbp)
	movq	$0, -288(%rbp)
	movl	$-1, -280(%rbp)
	movb	$8, -272(%rbp)
	pushq	-280(%rbp)
	movb	$5, -268(%rbp)
	pushq	-288(%rbp)
	movl	$1, -264(%rbp)
	pushq	-296(%rbp)
	movb	$0, -260(%rbp)
	pushq	-304(%rbp)
	movq	$0, -256(%rbp)
	movl	$-1, -248(%rbp)
	pushq	-248(%rbp)
	pushq	-256(%rbp)
	pushq	-264(%rbp)
	pushq	-272(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE
	addq	$64, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	0(%r13), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder34FindOrderedHashMapEntryForInt32KeyEv@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	jmp	.L2927
.L3536:
	movb	$4, -4368(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$2, -4364(%rbp)
	movl	$4, %edx
	movl	$0, -4360(%rbp)
	movb	$0, -4356(%rbp)
	movq	$0, -4352(%rbp)
	movl	$-1, -4344(%rbp)
	pushq	-4344(%rbp)
	pushq	-4352(%rbp)
	pushq	-4360(%rbp)
	pushq	-4368(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movzbl	23(%r14), %eax
	movq	32(%r14), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L3541
	movq	16(%rdx), %rdx
.L3541:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector16DeferReplacementEPNS1_4NodeES4_
	jmp	.L2927
.L3518:
	movb	$13, -4656(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$4, -4652(%rbp)
	movl	$13, %edx
	movl	$1, -4648(%rbp)
	movb	$0, -4644(%rbp)
	movq	$0, -4640(%rbp)
	movl	$-1, -4632(%rbp)
	pushq	-4632(%rbp)
	pushq	-4640(%rbp)
	pushq	-4648(%rbp)
	pushq	-4656(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movzbl	23(%r14), %eax
	movq	32(%r14), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L3522
	movq	16(%rdx), %rdx
.L3522:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector16DeferReplacementEPNS1_4NodeES4_
	jmp	.L2927
.L3431:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector26VisitWord32TruncatingBinopEPNS1_4NodeE
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	0(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder14Uint32LessThanEv@PLT
	movl	$4, %ecx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler18SimplifiedLowering5DoMaxEPNS1_4NodeEPKNS1_8OperatorENS0_21MachineRepresentationE
	jmp	.L2927
.L3475:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector26VisitWord32TruncatingBinopEPNS1_4NodeE
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	0(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder14Uint32LessThanEv@PLT
	movl	$4, %ecx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler18SimplifiedLowering5DoMinEPNS1_4NodeEPKNS1_8OperatorENS0_21MachineRepresentationE
	jmp	.L2927
.L3105:
	testl	%edx, %edx
	je	.L3113
	cmpl	$1, %edx
	jne	.L2927
	movb	$8, 1(%rax)
	jmp	.L2927
.L3364:
	movq	(%r14), %rdi
	call	_ZN2v88internal8compiler21NumberOperationHintOfEPKNS1_8OperatorE@PLT
	leaq	-208(%rbp), %rdx
	movl	$1, %ecx
	movq	%r13, %rdi
	movq	$0, -208(%rbp)
	movl	%eax, %esi
	movl	$-1, -200(%rbp)
	call	_ZN2v88internal8compiler12_GLOBAL__N_130CheckedUseInfoAsWord32FromHintENS1_19NumberOperationHintERKNS1_14FeedbackSourceENS1_13IdentifyZerosE
	movq	-144(%rbp), %rax
	movq	-136(%rbp), %rdx
	movq	-128(%rbp), %rcx
	movq	-120(%rbp), %rbx
	movq	%rax, -176(%rbp)
	pushq	%rbx
	movq	%rdx, -168(%rbp)
	pushq	%rcx
	movq	%rcx, -160(%rbp)
	pushq	%rdx
	movq	%rbx, -152(%rbp)
	pushq	%rax
	pushq	%rbx
	pushq	%rcx
	pushq	%rdx
	pushq	%rax
	jmp	.L4085
.L3178:
	movb	$4, -144(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$2, -140(%rbp)
	movl	$1, %edx
	movl	$0, -136(%rbp)
	movb	$0, -132(%rbp)
	movq	$0, -128(%rbp)
	movl	$-1, -120(%rbp)
	pushq	-120(%rbp)
	pushq	-128(%rbp)
	pushq	-136(%rbp)
	pushq	-144(%rbp)
	pushq	-120(%rbp)
	pushq	-128(%rbp)
	pushq	-136(%rbp)
	pushq	-144(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE
	addq	$64, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	(%r14), %rax
	movq	128(%r12), %rdi
	movzwl	16(%rax), %esi
	call	_ZN2v88internal8compiler21RepresentationChanger17Uint32OperatorForENS1_8IrOpcode5ValueE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	jmp	.L2927
.L3264:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector26VisitWord32TruncatingBinopEPNS1_4NodeE
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	(%r14), %rax
	movq	128(%r12), %rdi
	movzwl	16(%rax), %esi
	call	_ZN2v88internal8compiler21RepresentationChanger16Int32OperatorForENS1_8IrOpcode5ValueE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler22RepresentationSelector14ChangeToPureOpEPNS1_4NodeEPKNS1_8OperatorE
	jmp	.L2927
.L3207:
	movb	$4, -144(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$2, -140(%rbp)
	movl	$1, %edx
	movl	$0, -136(%rbp)
	movb	$0, -132(%rbp)
	movq	$0, -128(%rbp)
	movl	$-1, -120(%rbp)
	pushq	-120(%rbp)
	pushq	-128(%rbp)
	pushq	-136(%rbp)
	pushq	-144(%rbp)
	pushq	-120(%rbp)
	pushq	-128(%rbp)
	pushq	-136(%rbp)
	pushq	-144(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE
	addq	$64, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	(%r14), %rax
	movq	128(%r12), %rdi
	movzwl	16(%rax), %esi
	call	_ZN2v88internal8compiler21RepresentationChanger17Uint32OperatorForENS1_8IrOpcode5ValueE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	jmp	.L2927
.L3227:
	movb	$4, -144(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$2, -140(%rbp)
	movl	$1, %edx
	movl	$0, -136(%rbp)
	movb	$0, -132(%rbp)
	movq	$0, -128(%rbp)
	movl	$-1, -120(%rbp)
	pushq	-120(%rbp)
	pushq	-128(%rbp)
	pushq	-136(%rbp)
	pushq	-144(%rbp)
	pushq	-120(%rbp)
	pushq	-128(%rbp)
	pushq	-136(%rbp)
	pushq	-144(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE
	addq	$64, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	(%r14), %rax
	movq	128(%r12), %rdi
	movzwl	16(%rax), %esi
	call	_ZN2v88internal8compiler21RepresentationChanger17Uint32OperatorForENS1_8IrOpcode5ValueE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler22RepresentationSelector14ChangeToPureOpEPNS1_4NodeEPKNS1_8OperatorE
	jmp	.L2927
.L3254:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector26VisitWord32TruncatingBinopEPNS1_4NodeE
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	(%r14), %rax
	movq	128(%r12), %rdi
	movzwl	16(%rax), %esi
	call	_ZN2v88internal8compiler21RepresentationChanger16Int32OperatorForENS1_8IrOpcode5ValueE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler22RepresentationSelector14ChangeToPureOpEPNS1_4NodeEPKNS1_8OperatorE
	jmp	.L2927
.L3287:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector26VisitWord32TruncatingBinopEPNS1_4NodeE
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	(%r14), %rax
	movq	128(%r12), %rdi
	movzwl	16(%rax), %esi
	call	_ZN2v88internal8compiler21RepresentationChanger16Int32OperatorForENS1_8IrOpcode5ValueE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler22RepresentationSelector14ChangeToPureOpEPNS1_4NodeEPKNS1_8OperatorE
	jmp	.L2927
.L3341:
	cmpq	$7243, -240(%rbp)
	je	.L3353
	leaq	-240(%rbp), %rdi
	movl	$7243, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L3353
.L3354:
	movl	-6008(%rbp), %eax
	movb	$13, -176(%rbp)
	movl	$4294967295, %ecx
	movl	$13, %edx
	movb	$4, -172(%rbp)
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, -168(%rbp)
	movb	$0, -164(%rbp)
	movq	$0, -160(%rbp)
	movl	$-1, -152(%rbp)
	movb	$13, -144(%rbp)
	movb	$4, -140(%rbp)
	movl	$0, -136(%rbp)
	movb	$0, -132(%rbp)
	movq	$0, -128(%rbp)
	movl	$-1, -120(%rbp)
	pushq	-120(%rbp)
	pushq	-128(%rbp)
	pushq	-136(%rbp)
	pushq	-144(%rbp)
	pushq	-152(%rbp)
	pushq	-160(%rbp)
	pushq	-168(%rbp)
	pushq	-176(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE
	addq	$64, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	(%r14), %rax
	movq	128(%r12), %rdi
	movzwl	16(%rax), %esi
	call	_ZN2v88internal8compiler21RepresentationChanger18Float64OperatorForENS1_8IrOpcode5ValueE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler22RepresentationSelector14ChangeToPureOpEPNS1_4NodeEPKNS1_8OperatorE
	jmp	.L2927
.L3308:
	movzbl	23(%r14), %eax
	movq	32(%r14), %rsi
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L3318
	movq	16(%rsi), %rsi
.L3318:
	movl	20(%rsi), %edx
	movq	32(%r12), %rcx
	andl	$16777215, %edx
	leaq	(%rdx,%rdx,4), %rdx
	movq	24(%rcx,%rdx,8), %rdx
	testq	%rdx, %rdx
	jne	.L3320
	movq	8(%rsi), %rdx
.L3320:
	movq	%rdx, -208(%rbp)
	cmpq	$1099, %rdx
	je	.L3321
	leaq	-208(%rbp), %rdi
	movl	$1099, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L3322
	movzbl	23(%r14), %eax
	movq	32(%r12), %rcx
	andl	$15, %eax
.L3321:
	addq	$8, %rbx
	cmpl	$15, %eax
	jne	.L3324
	movq	32(%r14), %rax
	leaq	24(%rax), %rbx
.L3324:
	movq	(%rbx), %rdx
	movl	20(%rdx), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rax
	movq	24(%rcx,%rax,8), %rax
	testq	%rax, %rax
	jne	.L3325
	movq	8(%rdx), %rax
.L3325:
	movq	%rax, -176(%rbp)
	cmpq	$1099, %rax
	je	.L3329
	leaq	-176(%rbp), %rdi
	movl	$1099, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L3322
.L3329:
	movl	$2, %esi
	movl	%r15d, %edi
	call	_ZN2v88internal8compiler10Truncation11LessGeneralENS2_14TruncationKindES3_@PLT
	testb	%al, %al
	jne	.L3327
	movl	20(%r14), %eax
	movq	32(%r12), %r15
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rax
	leaq	(%r15,%rax,8), %rax
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L3331
	movq	8(%r14), %rax
.L3331:
	movq	%rax, -144(%rbp)
	cmpq	$1099, %rax
	je	.L3327
	leaq	-144(%rbp), %rdi
	movl	$1099, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L3322
.L3327:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector26VisitWord32TruncatingBinopEPNS1_4NodeE
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18SimplifiedLowering8Int32DivEPNS1_4NodeE
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler22RepresentationSelector16DeferReplacementEPNS1_4NodeES4_
	jmp	.L2927
.L3575:
	movq	(%rbx), %rdx
	movl	8(%rbx), %eax
	movb	$7, -3152(%rbp)
	movq	%r14, %rsi
	movb	$5, -3148(%rbp)
	movl	$4294967295, %ecx
	movq	%r12, %rdi
	movq	%rdx, -3136(%rbp)
	movl	$7, %edx
	movl	$1, -3144(%rbp)
	movb	$6, -3140(%rbp)
	movl	%eax, -3128(%rbp)
	pushq	-3128(%rbp)
	pushq	-3136(%rbp)
	pushq	-3144(%rbp)
	pushq	-3152(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	jmp	.L2927
.L3554:
	movb	$7, -144(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$5, -140(%rbp)
	movl	$7, %edx
	movl	$1, -136(%rbp)
	movb	$7, -132(%rbp)
	movq	$0, -128(%rbp)
	movl	$-1, -120(%rbp)
	pushq	-120(%rbp)
	pushq	-128(%rbp)
	pushq	-136(%rbp)
	pushq	-144(%rbp)
	pushq	-120(%rbp)
	pushq	-128(%rbp)
	pushq	-136(%rbp)
	pushq	-144(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE
	addq	$64, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	0(%r13), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9BigIntAddEv@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	jmp	.L2927
.L3555:
	movb	$7, -4048(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$5, -4044(%rbp)
	movl	$7, %edx
	movl	$1, -4040(%rbp)
	movb	$7, -4036(%rbp)
	movq	$0, -4032(%rbp)
	movl	$-1, -4024(%rbp)
	pushq	-4024(%rbp)
	pushq	-4032(%rbp)
	pushq	-4040(%rbp)
	pushq	-4048(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	0(%r13), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder12BigIntNegateEv@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler22RepresentationSelector14ChangeToPureOpEPNS1_4NodeEPKNS1_8OperatorE
	jmp	.L2927
.L3572:
	movq	%rdx, -3200(%rbp)
	movq	%r14, %rsi
	movl	$4294967295, %ecx
	movl	$6, %edx
	movb	$6, -3216(%rbp)
	movq	%r12, %rdi
	movb	$5, -3212(%rbp)
	movl	$1, -3208(%rbp)
	movb	$1, -3204(%rbp)
	movl	%eax, -3192(%rbp)
	pushq	-3192(%rbp)
	pushq	-3200(%rbp)
	pushq	-3208(%rbp)
	pushq	-3216(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	jmp	.L3573
.L3563:
	movb	$7, -3344(%rbp)
	movb	$5, -3340(%rbp)
	movl	$1, -3336(%rbp)
	movb	$6, -3332(%rbp)
	movq	$0, -3328(%rbp)
	movl	$-1, -3320(%rbp)
	pushq	-3320(%rbp)
	pushq	-3328(%rbp)
	pushq	-3336(%rbp)
	pushq	-3344(%rbp)
	jmp	.L4090
.L3138:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector11VisitInputsEPNS1_4NodeE
	movl	20(%r14), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rdx
	movq	32(%r12), %rax
	leaq	(%rax,%rdx,8), %rdx
	movl	120(%r12), %eax
	testl	%eax, %eax
	je	.L3143
	cmpl	$1, %eax
	jne	.L2927
	movb	$7, 1(%rdx)
	jmp	.L2927
.L3512:
	movb	$8, -4816(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$5, -4812(%rbp)
	movl	$7, %edx
	movl	$1, -4808(%rbp)
	movb	$0, -4804(%rbp)
	movq	$0, -4800(%rbp)
	movl	$-1, -4792(%rbp)
	pushq	-4792(%rbp)
	pushq	-4800(%rbp)
	pushq	-4808(%rbp)
	pushq	-4816(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	jmp	.L2927
.L3515:
	movb	$13, -4688(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$4, -4684(%rbp)
	movl	$13, %edx
	movl	$1, -4680(%rbp)
	movb	$0, -4676(%rbp)
	movq	$0, -4672(%rbp)
	movl	$-1, -4664(%rbp)
	pushq	-4664(%rbp)
	pushq	-4672(%rbp)
	pushq	-4680(%rbp)
	pushq	-4688(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18SimplifiedLowering11Float64SignEPNS1_4NodeE
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler22RepresentationSelector16DeferReplacementEPNS1_4NodeES4_
	jmp	.L2927
.L3644:
	movl	$16417, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector7InputIsEPNS1_4NodeENS1_4TypeE.isra.0
	testb	%al, %al
	je	.L3646
	movb	$8, -1936(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$5, -1932(%rbp)
	movl	$8, %edx
	movl	$1, -1928(%rbp)
	movb	$0, -1924(%rbp)
	movq	$0, -1920(%rbp)
	movl	$-1, -1912(%rbp)
	pushq	-1912(%rbp)
	pushq	-1920(%rbp)
	pushq	-1928(%rbp)
	pushq	-1936(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14StringToNumberEv@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	jmp	.L2927
.L4126:
	cmpl	$2, %eax
	jne	.L2927
	pushq	-120(%rbp)
	xorl	%ecx, %ecx
	movl	$2, %edx
	movq	%r14, %rsi
	pushq	-128(%rbp)
	movq	%r12, %rdi
	pushq	-136(%rbp)
	pushq	-144(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12ConvertInputEPNS1_4NodeEiNS1_7UseInfoENS1_4TypeE
	addq	$32, %rsp
	jmp	.L2927
.L3155:
	movb	$0, -5808(%rbp)
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$1, -5804(%rbp)
	movl	$0, -5800(%rbp)
	movb	$0, -5796(%rbp)
	movq	$0, -5792(%rbp)
	movl	$-1, -5784(%rbp)
	pushq	-5784(%rbp)
	pushq	-5792(%rbp)
	pushq	-5800(%rbp)
	pushq	-5808(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12ProcessInputEPNS1_4NodeEiNS1_7UseInfoE
	movl	20(%r14), %eax
	addq	$32, %rsp
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rdx
	movq	32(%r12), %rax
	leaq	(%rax,%rdx,8), %rdx
	movl	120(%r12), %eax
	testl	%eax, %eax
	je	.L3162
	cmpl	$1, %eax
	jne	.L2927
	movb	$1, 1(%rdx)
	jmp	.L2927
.L3383:
	movq	(%r14), %rdi
	call	_ZN2v88internal8compiler21NumberOperationHintOfEPKNS1_8OperatorE@PLT
	movl	%eax, %esi
	movzbl	23(%r14), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L3388
	leaq	40(%r14), %rax
.L3389:
	movq	(%rax), %rax
	leaq	-5328(%rbp), %rdi
	movl	$1, %ecx
	leaq	-176(%rbp), %rdx
	movq	8(%rax), %r15
	movq	$0, -176(%rbp)
	movl	$-1, -168(%rbp)
	call	_ZN2v88internal8compiler12_GLOBAL__N_130CheckedUseInfoAsWord32FromHintENS1_19NumberOperationHintERKNS1_14FeedbackSourceENS1_13IdentifyZerosE
	movq	-5320(%rbp), %rdx
	movq	%r14, %rsi
	movq	-5312(%rbp), %rcx
	movq	-5328(%rbp), %rax
	movq	-5304(%rbp), %rbx
	movq	%r12, %rdi
	movq	%rdx, -136(%rbp)
	pushq	%rbx
	pushq	%rcx
	pushq	%rdx
	pushq	%rax
	pushq	%rbx
	pushq	%rcx
	pushq	%rdx
	movl	$4, %edx
	pushq	%rax
	movq	%rcx, -128(%rbp)
	movl	$1099, %ecx
	movq	%rax, -144(%rbp)
	movq	%rbx, -120(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE
	addq	$64, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector16MaskShiftOperandEPNS1_4NodeENS1_4TypeE
	movq	0(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32SarEv@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler22RepresentationSelector14ChangeToPureOpEPNS1_4NodeEPKNS1_8OperatorE
	jmp	.L2927
.L3373:
	movq	(%r14), %rdi
	call	_ZN2v88internal8compiler21NumberOperationHintOfEPKNS1_8OperatorE@PLT
	movl	%eax, %esi
	movzbl	23(%r14), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L3378
	leaq	40(%r14), %rax
.L3379:
	movq	(%rax), %rax
	leaq	-5488(%rbp), %rdi
	movl	$1, %ecx
	leaq	-176(%rbp), %rdx
	movq	8(%rax), %r15
	movq	$0, -176(%rbp)
	movl	$-1, -168(%rbp)
	call	_ZN2v88internal8compiler12_GLOBAL__N_130CheckedUseInfoAsWord32FromHintENS1_19NumberOperationHintERKNS1_14FeedbackSourceENS1_13IdentifyZerosE
	movq	-5480(%rbp), %rdx
	movq	%r14, %rsi
	movq	-5472(%rbp), %rcx
	movq	-5488(%rbp), %rax
	movq	-5464(%rbp), %rbx
	movq	%r12, %rdi
	movq	%rdx, -136(%rbp)
	pushq	%rbx
	pushq	%rcx
	pushq	%rdx
	pushq	%rax
	pushq	%rbx
	pushq	%rcx
	pushq	%rdx
	movl	$4, %edx
	pushq	%rax
	movq	%rcx, -128(%rbp)
	movl	$1099, %ecx
	movq	%rax, -144(%rbp)
	movq	%rbx, -120(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE
	addq	$64, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector16MaskShiftOperandEPNS1_4NodeENS1_4TypeE
	movq	0(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32ShlEv@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler22RepresentationSelector14ChangeToPureOpEPNS1_4NodeEPKNS1_8OperatorE
	jmp	.L2927
.L3295:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector26VisitWord32TruncatingBinopEPNS1_4NodeE
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18SimplifiedLowering8Int32DivEPNS1_4NodeE
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler22RepresentationSelector16DeferReplacementEPNS1_4NodeES4_
	jmp	.L2927
.L3353:
	cmpq	$7243, -208(%rbp)
	je	.L3351
	leaq	-208(%rbp), %rdi
	movl	$7243, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L3354
.L3351:
	movl	$2, %esi
	movl	%r15d, %edi
	call	_ZN2v88internal8compiler10Truncation11LessGeneralENS2_14TruncationKindES3_@PLT
	testb	%al, %al
	jne	.L3355
	movl	20(%r14), %eax
	movq	32(%r12), %r11
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rax
	leaq	(%r11,%rax,8), %rax
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L3358
	movq	8(%r14), %rax
.L3358:
	movq	%rax, -176(%rbp)
	cmpq	$1099, %rax
	je	.L3355
	leaq	-176(%rbp), %rdi
	movl	$1099, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L3355
	cmpl	$0, -6008(%rbp)
	jne	.L3354
	movl	20(%r14), %eax
	andl	$16777215, %eax
	imulq	$40, %rax, %rax
	addq	32(%r12), %rax
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L3359
	movq	8(%r14), %rax
.L3359:
	movq	%rax, -144(%rbp)
	cmpq	$3147, %rax
	je	.L3355
	leaq	-144(%rbp), %rdi
	movl	$3147, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L3354
.L3355:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector26VisitWord32TruncatingBinopEPNS1_4NodeE
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18SimplifiedLowering8Int32ModEPNS1_4NodeE
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler22RepresentationSelector16DeferReplacementEPNS1_4NodeES4_
	jmp	.L2927
.L4132:
	movb	$8, -560(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$5, -556(%rbp)
	movl	$8, %edx
	movl	$1, -552(%rbp)
	movb	$0, -548(%rbp)
	movq	$0, -544(%rbp)
	movl	$-1, -536(%rbp)
	pushq	-536(%rbp)
	pushq	-544(%rbp)
	pushq	-552(%rbp)
	pushq	-560(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	jmp	.L2927
.L3621:
	movl	$4294967295, %eax
	movq	%rax, 16(%rdx)
	jmp	.L2927
.L3551:
	movb	$8, -144(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$5, -140(%rbp)
	movl	$7, %edx
	movl	$1, -136(%rbp)
	movb	$0, -132(%rbp)
	movq	$0, -128(%rbp)
	movl	$-1, -120(%rbp)
	pushq	-120(%rbp)
	pushq	-128(%rbp)
	pushq	-136(%rbp)
	pushq	-144(%rbp)
	pushq	-120(%rbp)
	pushq	-128(%rbp)
	pushq	-136(%rbp)
	pushq	-144(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE
	addq	$64, %rsp
	jmp	.L2927
.L3380:
	movq	32(%r14), %rax
	addq	$24, %rax
	jmp	.L3381
.L3558:
	movl	$4294967295, %eax
	movq	%rax, 16(%rdx)
	jmp	.L2927
.L3560:
	movl	$4294967295, %eax
	movq	%rax, 16(%rdx)
	jmp	.L2927
.L3103:
	movl	$4294967295, %eax
	movq	%rax, 16(%rdx)
	jmp	.L2927
.L3361:
	leaq	16(%rdx), %rax
	movq	16(%rdx), %rdx
	movq	8(%rdx), %rdx
	movq	%rdx, -176(%rbp)
	cmpq	$8396767, %rdx
	jne	.L3366
.L3365:
	addq	$8, %rax
	jmp	.L3367
.L3577:
	movl	$4294967295, %eax
	movq	%rax, 16(%rdx)
	jmp	.L2927
.L3556:
	movl	$4294967295, %eax
	movq	%rax, 16(%rdx)
	jmp	.L2927
.L3370:
	movq	32(%r14), %rax
	addq	$24, %rax
	jmp	.L3371
.L3626:
	movl	$4294967295, %eax
	movq	%rax, 16(%rdx)
	jmp	.L2927
.L3751:
	movl	$4294967295, %eax
	movq	%rax, 16(%rdx)
	jmp	.L2927
.L3755:
	movq	32(%r14), %rax
	addq	$24, %rax
	jmp	.L3756
.L3753:
	movl	$4294967295, %eax
	movq	%rax, 16(%rdx)
	jmp	.L2927
.L3513:
	movq	$134217729, 16(%rdx)
	jmp	.L2927
.L3741:
	movl	$4294967295, %eax
	movq	%rax, 16(%rdx)
	jmp	.L2927
.L3610:
	movq	32(%r14), %rax
	addq	$32, %rax
	jmp	.L3611
.L3099:
	movl	$4294967295, %eax
	movq	%rax, 16(%rdx)
	jmp	.L2927
.L3101:
	movl	$4294967295, %eax
	movq	%rax, 16(%rdx)
	jmp	.L2927
.L3117:
	movl	$4294967295, %eax
	movq	%rax, 16(%rdx)
	movl	120(%r12), %eax
	jmp	.L3119
.L3393:
	movq	32(%r14), %rax
	addq	$24, %rax
	jmp	.L3394
.L3630:
	movl	$4294967295, %eax
	movq	%rax, 16(%rdx)
	jmp	.L2927
.L3096:
	movl	$4294967295, %eax
	movq	%rax, 16(%rdx)
	jmp	.L2927
.L3591:
	movl	$4294967295, %eax
	movq	%rax, 16(%rdx)
	movl	120(%r12), %eax
	jmp	.L3593
.L3581:
	movq	32(%r14), %rax
	addq	$24, %rax
	jmp	.L3582
.L4141:
	movb	$8, -2992(%rbp)
	jmp	.L4092
.L4140:
	movq	24(%rdx), %rax
	testq	%rax, %rax
	jne	.L3585
	movq	8(%rbx), %rax
.L3585:
	movq	%rdx, -6008(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZN2v88internal8compiler10BitsetType11SignedSmallEv@PLT
	movq	-6008(%rbp), %rdx
	movl	%eax, %esi
	orl	$1, %esi
	cmpq	%rsi, -176(%rbp)
	je	.L3586
	leaq	-176(%rbp), %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	movq	-6008(%rbp), %rdx
	testb	%al, %al
	je	.L3584
.L3586:
	movl	$6, %r13d
	jmp	.L3584
.L3624:
	movl	$4294967295, %eax
	movq	%rax, 16(%rdx)
	jmp	.L2927
.L3552:
	movl	$4294967295, %eax
	movq	%rax, 16(%rdx)
	jmp	.L2927
.L3730:
	movl	$4294967295, %eax
	movq	%rax, 16(%rdx)
	movl	120(%r12), %eax
	jmp	.L3732
.L3723:
	movq	32(%r14), %rax
	movq	32(%rax), %rsi
	addq	$16, %rax
	movl	20(%rsi), %ecx
	andl	$16777215, %ecx
	leaq	(%rcx,%rcx,4), %rcx
	movq	24(%rdx,%rcx,8), %rcx
	testq	%rcx, %rcx
	jne	.L4096
	movq	8(%rsi), %rcx
.L4096:
	movq	%rcx, -176(%rbp)
	addq	$24, %rax
	jmp	.L3728
.L3125:
	movl	$4294967295, %eax
	movq	%rax, 16(%rdx)
	movl	120(%r12), %eax
	jmp	.L3127
.L3121:
	pushq	-120(%rbp)
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	pushq	-128(%rbp)
	pushq	-136(%rbp)
	pushq	-144(%rbp)
	movq	%r15, -6016(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12EnqueueInputEPNS1_4NodeEiNS1_7UseInfoE
	movq	-6016(%rbp), %r8
	addq	$32, %rsp
	jmp	.L3123
.L3762:
	movl	$4294967295, %eax
	movq	%rax, 16(%rdx)
	jmp	.L2927
.L3606:
	movl	$4294967295, %eax
	movq	%rax, 16(%rdx)
	movl	120(%r12), %eax
	jmp	.L3608
.L4143:
	movb	$8, -2864(%rbp)
	jmp	.L4094
.L3743:
	movl	$4294967295, %eax
	movq	%rax, 16(%rdx)
	jmp	.L2927
.L3566:
	movl	$4294967295, %eax
	movq	%rax, 16(%rdx)
	jmp	.L2927
.L3748:
	movl	$4294967295, %eax
	movq	%rax, 16(%rdx)
	jmp	.L2927
.L4144:
	pushq	-120(%rbp)
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	pushq	-128(%rbp)
	pushq	-136(%rbp)
	pushq	-144(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12EnqueueInputEPNS1_4NodeEiNS1_7UseInfoE
	addq	$32, %rsp
	jmp	.L3747
.L3115:
	movl	$4294967295, %eax
	movq	%rax, 16(%rdx)
	jmp	.L2927
.L3216:
	leaq	16(%rsi), %rax
	movq	16(%rsi), %rsi
	movl	20(%rsi), %edx
	andl	$16777215, %edx
	leaq	(%rdx,%rdx,4), %rdx
	movq	24(%rcx,%rdx,8), %rdx
	testq	%rdx, %rdx
	jne	.L4079
	movq	8(%rsi), %rdx
.L4079:
	movq	%rdx, -240(%rbp)
	addq	$8, %rax
	jmp	.L3221
.L3145:
	movl	$4, %esi
	movl	%r15d, %edi
	call	_ZN2v88internal8compiler10Truncation11LessGeneralENS2_14TruncationKindES3_@PLT
	testb	%al, %al
	movl	20(%r14), %eax
	je	.L3149
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rdx
	movq	32(%r12), %rax
	leaq	(%rax,%rdx,8), %rax
	movl	120(%r12), %edx
	testl	%edx, %edx
	je	.L3150
	cmpl	$1, %edx
	jne	.L3152
	movb	$13, 1(%rax)
.L3152:
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18SimplifiedLowering39DoJSToNumberOrNumericTruncatesToFloat64EPNS1_4NodeEPNS1_22RepresentationSelectorE
	jmp	.L2927
	.p2align 4,,10
	.p2align 3
.L3390:
	movq	32(%r14), %rax
	addq	$24, %rax
	jmp	.L3391
.L4142:
	movq	24(%r15), %rax
	testq	%rax, %rax
	jne	.L3601
	movq	8(%rbx), %rax
.L3601:
	movq	%rax, -176(%rbp)
	call	_ZN2v88internal8compiler10BitsetType11SignedSmallEv@PLT
	movl	%eax, %esi
	orl	$1, %esi
	cmpq	%rsi, -176(%rbp)
	je	.L3602
	leaq	-176(%rbp), %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L3600
.L3602:
	movl	$6, %r13d
	jmp	.L3600
.L3597:
	movq	32(%r14), %rax
	addq	$32, %rax
	jmp	.L3598
.L3699:
	movl	$4294967295, %eax
	movq	%rax, 16(%rdx)
	jmp	.L2927
.L3628:
	movl	$4294967295, %eax
	movq	%rax, 16(%rdx)
	jmp	.L2927
.L3760:
	movl	$4294967295, %eax
	movq	%rax, 16(%rdx)
	jmp	.L2927
.L3225:
	cmpq	$3147, -208(%rbp)
	je	.L3229
	leaq	-208(%rbp), %rdi
	movl	$3147, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L3235
.L3229:
	movb	$4, -144(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$2, -140(%rbp)
	movl	$1, %edx
	movl	$0, -136(%rbp)
	movb	$0, -132(%rbp)
	movq	$0, -128(%rbp)
	movl	$-1, -120(%rbp)
	pushq	-120(%rbp)
	pushq	-128(%rbp)
	pushq	-136(%rbp)
	pushq	-144(%rbp)
	pushq	-120(%rbp)
	pushq	-128(%rbp)
	pushq	-136(%rbp)
	pushq	-144(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE
	addq	$64, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	(%r14), %rax
	movq	128(%r12), %rdi
	movzwl	16(%rax), %esi
	call	_ZN2v88internal8compiler21RepresentationChanger16Int32OperatorForENS1_8IrOpcode5ValueE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler22RepresentationSelector14ChangeToPureOpEPNS1_4NodeEPKNS1_8OperatorE
	jmp	.L2927
.L4129:
	movb	$4, -5200(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$2, -5196(%rbp)
	movl	$4, %edx
	movl	$0, -5192(%rbp)
	movb	$0, -5188(%rbp)
	movq	$0, -5184(%rbp)
	movl	$-1, -5176(%rbp)
	movb	$4, -5168(%rbp)
	pushq	-5176(%rbp)
	movb	$2, -5164(%rbp)
	pushq	-5184(%rbp)
	movl	$0, -5160(%rbp)
	pushq	-5192(%rbp)
	movb	$0, -5156(%rbp)
	pushq	-5200(%rbp)
	movq	$0, -5152(%rbp)
	movl	$-1, -5144(%rbp)
	pushq	-5144(%rbp)
	pushq	-5152(%rbp)
	pushq	-5160(%rbp)
	pushq	-5168(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE
	addq	$64, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	-208(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector16MaskShiftOperandEPNS1_4NodeENS1_4TypeE
	movq	0(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32ShrEv@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler22RepresentationSelector14ChangeToPureOpEPNS1_4NodeEPKNS1_8OperatorE
	jmp	.L2927
.L3205:
	cmpq	$3147, -176(%rbp)
	je	.L3209
	leaq	-176(%rbp), %rdi
	movl	$3147, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L3215
.L3209:
	movb	$4, -144(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$2, -140(%rbp)
	movl	$1, %edx
	movl	$0, -136(%rbp)
	movb	$0, -132(%rbp)
	movq	$0, -128(%rbp)
	movl	$-1, -120(%rbp)
	pushq	-120(%rbp)
	pushq	-128(%rbp)
	pushq	-136(%rbp)
	pushq	-144(%rbp)
	pushq	-120(%rbp)
	pushq	-128(%rbp)
	pushq	-136(%rbp)
	pushq	-144(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE
	addq	$64, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	(%r14), %rax
	movq	128(%r12), %rdi
	movzwl	16(%rax), %esi
	call	_ZN2v88internal8compiler21RepresentationChanger16Int32OperatorForENS1_8IrOpcode5ValueE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	jmp	.L2927
.L3322:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector17VisitFloat64BinopEPNS1_4NodeE
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	(%r14), %rax
	movq	128(%r12), %rdi
	movzwl	16(%rax), %esi
	call	_ZN2v88internal8compiler21RepresentationChanger18Float64OperatorForENS1_8IrOpcode5ValueE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler22RepresentationSelector14ChangeToPureOpEPNS1_4NodeEPKNS1_8OperatorE
	jmp	.L2927
.L3508:
	movzbl	23(%r14), %eax
	movq	32(%r14), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L3511
	movq	16(%rdx), %rdx
.L3511:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector16DeferReplacementEPNS1_4NodeES4_
	jmp	.L2927
.L3309:
	movq	32(%r14), %rax
	addq	$24, %rax
	jmp	.L3310
.L3636:
	movzbl	23(%r14), %eax
	movq	32(%r14), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L3639
	movq	16(%rdx), %rdx
.L3639:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector16DeferReplacementEPNS1_4NodeES4_
	jmp	.L2927
.L3187:
	cmpq	$3147, -176(%rbp)
	je	.L3185
	leaq	-176(%rbp), %rdi
	movl	$3147, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L3191
.L3185:
	movb	$4, -144(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$2, -140(%rbp)
	movl	$1, %edx
	movl	$0, -136(%rbp)
	movb	$0, -132(%rbp)
	movq	$0, -128(%rbp)
	movl	$-1, -120(%rbp)
	pushq	-120(%rbp)
	pushq	-128(%rbp)
	pushq	-136(%rbp)
	pushq	-144(%rbp)
	pushq	-120(%rbp)
	pushq	-128(%rbp)
	pushq	-136(%rbp)
	pushq	-144(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE
	addq	$64, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	(%r14), %rax
	movq	128(%r12), %rdi
	movzwl	16(%rax), %esi
	call	_ZN2v88internal8compiler21RepresentationChanger16Int32OperatorForENS1_8IrOpcode5ValueE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	jmp	.L2927
.L4128:
	movl	$2, %esi
	movl	%r15d, %edi
	call	_ZN2v88internal8compiler10Truncation11LessGeneralENS2_14TruncationKindES3_@PLT
	testb	%al, %al
	jne	.L3397
	movl	%ebx, %esi
	leaq	-5232(%rbp), %rdi
	movl	$1, %ecx
	movq	$0, -176(%rbp)
	leaq	-176(%rbp), %rdx
	movl	$-1, -168(%rbp)
	call	_ZN2v88internal8compiler12_GLOBAL__N_130CheckedUseInfoAsWord32FromHintENS1_19NumberOperationHintERKNS1_14FeedbackSourceENS1_13IdentifyZerosE
	movq	-5224(%rbp), %rdx
	movq	%r14, %rsi
	movq	-5216(%rbp), %rcx
	movq	-5232(%rbp), %rax
	movq	-5208(%rbp), %rbx
	movq	%r12, %rdi
	movq	%rdx, -136(%rbp)
	pushq	%rbx
	pushq	%rcx
	pushq	%rdx
	pushq	%rax
	pushq	%rbx
	pushq	%rcx
	pushq	%rdx
	movl	$4, %edx
	pushq	%rax
	movq	%rcx, -128(%rbp)
	movl	$1027, %ecx
	movq	%rax, -144(%rbp)
	movq	%rbx, -120(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE
	addq	$64, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	%r14, %rdi
	movl	$1, %esi
	call	_ZN2v88internal8compiler4Node11RemoveInputEi@PLT
	movq	(%r12), %rax
	leaq	-144(%rbp), %rsi
	movq	376(%rax), %rdi
	movq	$0, -144(%rbp)
	movl	$-1, -136(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder20CheckedUint32ToInt32ERKNS1_14FeedbackSourceE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	jmp	.L2927
	.p2align 4,,10
	.p2align 3
.L3646:
	movl	$2, %esi
	movl	%r15d, %edi
	call	_ZN2v88internal8compiler10Truncation11LessGeneralENS2_14TruncationKindES3_@PLT
	testb	%al, %al
	je	.L3647
	movl	$8396767, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector7InputIsEPNS1_4NodeENS1_4TypeE.isra.0
	testb	%al, %al
	je	.L3648
	movb	$4, -1904(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$2, -1900(%rbp)
	movl	$4, %edx
	movl	$0, -1896(%rbp)
	movb	$0, -1892(%rbp)
	movq	$0, -1888(%rbp)
	movl	$-1, -1880(%rbp)
	pushq	-1880(%rbp)
	pushq	-1888(%rbp)
	pushq	-1896(%rbp)
	pushq	-1904(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movzbl	23(%r14), %eax
	movq	32(%r14), %rcx
	leaq	32(%r14), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L3649
	leaq	16(%rcx), %rdx
.L3649:
	movq	(%rdx), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector16DeferReplacementEPNS1_4NodeES4_
	jmp	.L2927
	.p2align 4,,10
	.p2align 3
.L3484:
	cmpq	$1099, -176(%rbp)
	je	.L3482
	leaq	-176(%rbp), %rdi
	movl	$1099, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L3488
.L3482:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector26VisitWord32TruncatingBinopEPNS1_4NodeE
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	0(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder13Int32LessThanEv@PLT
	movl	$4, %ecx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler18SimplifiedLowering5DoMinEPNS1_4NodeEPKNS1_8OperatorENS0_21MachineRepresentationE
	jmp	.L2927
.L3146:
	movl	$4294967295, %eax
	movq	%rax, 16(%rdx)
	movl	120(%r12), %eax
	jmp	.L3148
.L3113:
	movl	$4294967295, %ebx
	movq	%rbx, 16(%rax)
	jmp	.L2927
.L4112:
	movb	$8, -3024(%rbp)
	jmp	.L4091
.L4127:
	movb	$8, -2896(%rbp)
	jmp	.L4093
.L3440:
	cmpq	$1099, -176(%rbp)
	je	.L3438
	leaq	-176(%rbp), %rdi
	movl	$1099, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L3444
.L3438:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector26VisitWord32TruncatingBinopEPNS1_4NodeE
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	0(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder13Int32LessThanEv@PLT
	movl	$4, %ecx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler18SimplifiedLowering5DoMaxEPNS1_4NodeEPKNS1_8OperatorENS0_21MachineRepresentationE
	jmp	.L2927
.L4125:
	movq	(%r14), %rdi
	call	_ZN2v88internal8compiler18SelectParametersOfEPKNS1_8OperatorE@PLT
	cmpb	(%rax), %bl
	je	.L4073
	movzbl	1(%rax), %edx
	movq	0(%r13), %rax
	movl	%ebx, %esi
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6SelectENS0_21MachineRepresentationENS1_10BranchHintE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
.L4073:
	movl	-6008(%rbp), %ecx
	movl	120(%r12), %eax
	movb	%bl, -144(%rbp)
	movb	%r15b, -140(%rbp)
	movl	%ecx, -136(%rbp)
	movb	$0, -132(%rbp)
	movq	$0, -128(%rbp)
	movl	$-1, -120(%rbp)
	testl	%eax, %eax
	je	.L3131
	cmpl	$2, %eax
	jne	.L2927
	pushq	-120(%rbp)
	xorl	%ecx, %ecx
	movl	$1, %edx
	movq	%r14, %rsi
	pushq	-128(%rbp)
	movq	%r12, %rdi
	pushq	-136(%rbp)
	pushq	-144(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12ConvertInputEPNS1_4NodeEiNS1_7UseInfoENS1_4TypeE
	movl	120(%r12), %eax
	addq	$32, %rsp
	jmp	.L3134
.L3526:
	movb	$13, -4528(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$4, -4524(%rbp)
	movl	$1, %edx
	movl	$0, -4520(%rbp)
	movb	$0, -4516(%rbp)
	movq	$0, -4512(%rbp)
	movl	$-1, -4504(%rbp)
	pushq	-4504(%rbp)
	pushq	-4512(%rbp)
	pushq	-4520(%rbp)
	pushq	-4528(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18SimplifiedLowering20DoOrderedNumberToBitEPNS1_4NodeE
	jmp	.L2927
.L4111:
	testb	$1, %al
	jne	.L2980
	movl	120(%r12), %ecx
	testl	%ecx, %ecx
	je	.L4146
	subl	$1, %ecx
	movq	32(%r12), %rdx
	movl	20(%r14), %eax
	je	.L4147
	xorl	$251658240, %eax
	movq	32(%r14), %rcx
	testl	$251658240, %eax
	jne	.L4080
	leaq	16(%rcx), %rbx
	movq	16(%rcx), %rcx
.L4080:
	movl	20(%rcx), %eax
	addq	$8, %rbx
	andl	$16777215, %eax
	imulq	$40, %rax, %rax
	movzbl	1(%rdx,%rax), %eax
	subl	$6, %eax
	cmpb	$2, %al
	ja	.L3240
	movq	(%rbx), %rax
	movl	20(%rax), %eax
	andl	$16777215, %eax
	imulq	$40, %rax, %rax
	movzbl	1(%rdx,%rax), %eax
	subl	$6, %eax
	cmpb	$2, %al
	ja	.L3240
	movb	$6, -144(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$5, -140(%rbp)
	movl	$1, %edx
	movl	$0, -136(%rbp)
	movb	$1, -132(%rbp)
	movq	$0, -128(%rbp)
	movl	$-1, -120(%rbp)
	pushq	-120(%rbp)
	pushq	-128(%rbp)
	pushq	-136(%rbp)
	pushq	-144(%rbp)
	pushq	-120(%rbp)
	pushq	-128(%rbp)
	pushq	-136(%rbp)
	pushq	-144(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE
	movq	(%r14), %rax
	addq	$64, %rsp
	movq	128(%r12), %rdi
	movzwl	16(%rax), %esi
	call	_ZN2v88internal8compiler21RepresentationChanger23TaggedSignedOperatorForENS1_8IrOpcode5ValueE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler22RepresentationSelector14ChangeToPureOpEPNS1_4NodeEPKNS1_8OperatorE
	jmp	.L2927
	.p2align 4,,10
	.p2align 3
.L3537:
	movb	$4, -4336(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$2, -4332(%rbp)
	movl	$4, %edx
	movl	$0, -4328(%rbp)
	movb	$0, -4324(%rbp)
	movq	$0, -4320(%rbp)
	movl	$-1, -4312(%rbp)
	pushq	-4312(%rbp)
	pushq	-4320(%rbp)
	pushq	-4328(%rbp)
	pushq	-4336(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18SimplifiedLowering26DoUnsigned32ToUint8ClampedEPNS1_4NodeE
	jmp	.L2927
.L3274:
	testb	%al, %al
	jne	.L3275
	leaq	-5712(%rbp), %rdi
	leaq	-176(%rbp), %rdx
	movl	$1, %ecx
	movl	%ebx, %esi
	movq	$0, -176(%rbp)
	movl	$-1, -168(%rbp)
	call	_ZN2v88internal8compiler12_GLOBAL__N_130CheckedUseInfoAsWord32FromHintENS1_19NumberOperationHintERKNS1_14FeedbackSourceENS1_13IdentifyZerosE
	movq	-5704(%rbp), %rdx
	movq	-5696(%rbp), %rsi
	movl	$1099, %ecx
	movq	-5688(%rbp), %rdi
	movq	-5712(%rbp), %rax
	movq	%rdx, -136(%rbp)
	pushq	%rdi
	pushq	%rsi
	pushq	%rdx
	pushq	%rax
	pushq	%rdi
	pushq	%rsi
	pushq	%rdx
	movl	$4, %edx
	pushq	%rax
	movq	%rsi, -128(%rbp)
	movq	%r14, %rsi
	movq	%rdi, -120(%rbp)
	movq	%r12, %rdi
	movq	%rax, -144(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE
	addq	$64, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movl	-6008(%rbp), %edx
	movq	%r15, %r8
	movq	%r13, %rcx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector22LowerToCheckedInt32MulEPNS1_4NodeENS1_10TruncationENS1_4TypeES6_.isra.0
	jmp	.L2927
.L3614:
	movb	$13, -2640(%rbp)
	movl	$2, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$4, -2636(%rbp)
	movl	$1, -2632(%rbp)
	movb	$0, -2628(%rbp)
	movq	$0, -2624(%rbp)
	movl	$-1, -2616(%rbp)
	pushq	-2616(%rbp)
	pushq	-2624(%rbp)
	pushq	-2632(%rbp)
	pushq	-2640(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12ProcessInputEPNS1_4NodeEiNS1_7UseInfoE
	addq	$32, %rsp
	cmpl	$2, 120(%r12)
	jne	.L3617
	movq	(%r14), %rdi
	call	_ZN2v88internal8compiler20DoubleMapParameterOfEPKNS1_8OperatorE@PLT
	movq	%rax, %rsi
	movq	(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder31TransitionAndStoreNumberElementENS0_6HandleINS0_3MapEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	jmp	.L3617
.L3407:
	movb	$4, -5072(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$2, -5068(%rbp)
	movl	$4, %edx
	movl	$0, -5064(%rbp)
	movb	$0, -5060(%rbp)
	movq	$0, -5056(%rbp)
	movl	$-1, -5048(%rbp)
	pushq	-5048(%rbp)
	pushq	-5056(%rbp)
	pushq	-5064(%rbp)
	pushq	-5072(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18SimplifiedLowering8Int32AbsEPNS1_4NodeE
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler22RepresentationSelector16DeferReplacementEPNS1_4NodeES4_
	jmp	.L2927
.L3139:
	movl	$4294967295, %eax
	movq	%rax, 16(%rdx)
	movl	120(%r12), %eax
	jmp	.L3141
.L3378:
	movq	32(%r14), %rax
	addq	$24, %rax
	jmp	.L3379
.L3143:
	movl	$4294967295, %eax
	movq	%rax, 16(%rdx)
	jmp	.L2927
.L3374:
	movq	32(%r14), %rax
	addq	$24, %rax
	jmp	.L3375
.L3388:
	movq	32(%r14), %rax
	addq	$24, %rax
	jmp	.L3389
.L3656:
	movb	$5, -1700(%rbp)
	movq	%rdx, -1696(%rbp)
	movl	%eax, -1688(%rbp)
	jmp	.L3657
.L3149:
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rdx
	movq	32(%r12), %rax
	leaq	(%rax,%rdx,8), %rax
	movl	120(%r12), %edx
	testl	%edx, %edx
	je	.L3153
	cmpl	$1, %edx
	jne	.L2927
	movb	$8, 1(%rax)
	jmp	.L2927
.L3384:
	movq	32(%r14), %rax
	addq	$24, %rax
	jmp	.L3385
.L4130:
	movb	$4, -656(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$2, -652(%rbp)
	movl	$4, %edx
	movl	$0, -648(%rbp)
	movb	$0, -644(%rbp)
	movq	$0, -640(%rbp)
	movl	$-1, -632(%rbp)
	pushq	-632(%rbp)
	pushq	-640(%rbp)
	pushq	-648(%rbp)
	pushq	-656(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movzbl	23(%r14), %eax
	movq	32(%r14), %rcx
	leaq	32(%r14), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L3720
	leaq	16(%rcx), %rdx
.L3720:
	movq	(%rdx), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector16DeferReplacementEPNS1_4NodeES4_
	jmp	.L2927
.L3162:
	movl	$4294967295, %eax
	movq	%rax, 16(%rdx)
	jmp	.L2927
.L4118:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector11VisitUnusedEPNS1_4NodeE
	jmp	.L2927
.L3256:
	movq	344(%r12), %rax
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	384(%rax), %rdx
	call	_ZN2v88internal8compiler22RepresentationSelector13BothInputsAreEPNS1_4NodeENS1_4TypeE
	testb	%al, %al
	je	.L3258
	movq	8(%r14), %rax
	movq	344(%r12), %rdx
	movq	%rax, -144(%rbp)
	movq	384(%rdx), %rsi
	cmpq	%rax, %rsi
	je	.L3259
	leaq	-144(%rbp), %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L3258
.L3259:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector15VisitInt64BinopEPNS1_4NodeE
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	(%r14), %rax
	movq	128(%r12), %rdi
	movzwl	16(%rax), %esi
	call	_ZN2v88internal8compiler21RepresentationChanger16Int64OperatorForENS1_8IrOpcode5ValueE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler22RepresentationSelector14ChangeToPureOpEPNS1_4NodeEPKNS1_8OperatorE
	jmp	.L2927
.L4145:
	pushq	-120(%rbp)
	movq	%rbx, %rcx
	xorl	%edx, %edx
	movq	%r14, %rsi
	pushq	-128(%rbp)
	movq	%r12, %rdi
	pushq	-136(%rbp)
	pushq	-144(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12ConvertInputEPNS1_4NodeEiNS1_7UseInfoENS1_4TypeE
	addq	$32, %rsp
	jmp	.L3747
.L4131:
	movb	$13, -624(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$4, -620(%rbp)
	movl	$13, %edx
	movl	$1, -616(%rbp)
	movb	$0, -612(%rbp)
	movq	$0, -608(%rbp)
	movl	$-1, -600(%rbp)
	pushq	-600(%rbp)
	pushq	-608(%rbp)
	pushq	-616(%rbp)
	pushq	-624(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movzbl	23(%r14), %eax
	movq	32(%r14), %rcx
	leaq	32(%r14), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L3721
	leaq	16(%rcx), %rdx
.L3721:
	movq	(%rdx), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector16DeferReplacementEPNS1_4NodeES4_
	jmp	.L2927
.L3299:
	movl	$1099, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector13BothInputsAreEPNS1_4NodeENS1_4TypeE
	testb	%al, %al
	je	.L3301
	testb	$-3, %bl
	jne	.L3301
	movb	$4, -144(%rbp)
	movb	$2, -140(%rbp)
	movl	$0, -136(%rbp)
	movb	$0, -132(%rbp)
	movq	$0, -128(%rbp)
	movl	$-1, -120(%rbp)
	pushq	-120(%rbp)
	pushq	-128(%rbp)
	pushq	-136(%rbp)
	pushq	-144(%rbp)
	pushq	-120(%rbp)
	pushq	-128(%rbp)
	pushq	-136(%rbp)
	pushq	-144(%rbp)
.L4082:
	movl	$1099, %ecx
	movl	$4, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE
	addq	$64, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector23ChangeToInt32OverflowOpEPNS1_4NodeE
	jmp	.L2927
.L3682:
	movb	$13, -1264(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$4, -1260(%rbp)
	movl	$1, %edx
	movl	$1, -1256(%rbp)
	movb	$0, -1252(%rbp)
	movq	$0, -1248(%rbp)
	movl	$-1, -1240(%rbp)
	pushq	-1240(%rbp)
	pushq	-1248(%rbp)
	pushq	-1256(%rbp)
	pushq	-1264(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	0(%r13), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder15NumberIsIntegerEv@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	jmp	.L2927
.L4135:
	movq	(%r12), %rdi
	call	_ZN2v88internal8compiler7JSGraph13FalseConstantEv@PLT
	movq	%r14, %rdi
	movq	%rax, %rdx
	movq	(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11AppendInputEPNS0_4ZoneEPS2_@PLT
	movq	0(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder11Word32EqualEv@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	jmp	.L2927
.L4137:
	movq	32(%r14), %rax
	movq	32(%r12), %rdx
	movq	16(%rax), %rsi
	movl	20(%rsi), %ecx
	andl	$16777215, %ecx
	imulq	$40, %rcx, %rcx
	movq	24(%rdx,%rcx), %r13
	testq	%r13, %r13
	jne	.L3777
	movq	8(%rsi), %r13
.L3777:
	addq	$24, %rax
	jmp	.L3272
.L3647:
	movl	$4, %esi
	movl	%r15d, %edi
	call	_ZN2v88internal8compiler10Truncation11LessGeneralENS2_14TruncationKindES3_@PLT
	testb	%al, %al
	je	.L3650
	movl	$8396767, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector7InputIsEPNS1_4NodeENS1_4TypeE.isra.0
	testb	%al, %al
	je	.L3651
	movb	$13, -1840(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$4, -1836(%rbp)
	movl	$13, %edx
	movl	$1, -1832(%rbp)
	movb	$0, -1828(%rbp)
	movq	$0, -1824(%rbp)
	movl	$-1, -1816(%rbp)
	pushq	-1816(%rbp)
	pushq	-1824(%rbp)
	pushq	-1832(%rbp)
	pushq	-1840(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movzbl	23(%r14), %eax
	movq	32(%r14), %rcx
	leaq	32(%r14), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L3652
	leaq	16(%rcx), %rdx
.L3652:
	movq	(%rdx), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector16DeferReplacementEPNS1_4NodeES4_
	jmp	.L2927
.L3666:
	movb	$13, -1552(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$4, -1548(%rbp)
	movl	$1, %edx
	movl	$1, -1544(%rbp)
	movb	$0, -1540(%rbp)
	movq	$0, -1536(%rbp)
	movl	$-1, -1528(%rbp)
	pushq	-1528(%rbp)
	pushq	-1536(%rbp)
	pushq	-1544(%rbp)
	pushq	-1552(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	0(%r13), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14NumberIsFiniteEv@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	jmp	.L2927
.L3690:
	movb	$13, -1104(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$4, -1100(%rbp)
	movl	$1, %edx
	movl	$1, -1096(%rbp)
	movb	$0, -1092(%rbp)
	movq	$0, -1088(%rbp)
	movl	$-1, -1080(%rbp)
	pushq	-1080(%rbp)
	pushq	-1088(%rbp)
	pushq	-1096(%rbp)
	pushq	-1104(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder17NumberIsMinusZeroEv@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	jmp	.L2927
.L3698:
	movb	$13, -976(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$4, -972(%rbp)
	movl	$1, %edx
	movl	$1, -968(%rbp)
	movb	$0, -964(%rbp)
	movq	$0, -960(%rbp)
	movl	$-1, -952(%rbp)
	pushq	-952(%rbp)
	pushq	-960(%rbp)
	pushq	-968(%rbp)
	pushq	-976(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder11NumberIsNaNEv@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	jmp	.L2927
.L3674:
	movb	$13, -1392(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$4, -1388(%rbp)
	movl	$1, %edx
	movl	$1, -1384(%rbp)
	movb	$0, -1380(%rbp)
	movq	$0, -1376(%rbp)
	movl	$-1, -1368(%rbp)
	pushq	-1368(%rbp)
	pushq	-1376(%rbp)
	pushq	-1384(%rbp)
	pushq	-1392(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	0(%r13), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder19NumberIsSafeIntegerEv@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	jmp	.L2927
.L4134:
	movq	(%r12), %rdi
	call	_ZN2v88internal8compiler7JSGraph13FalseConstantEv@PLT
	movq	%r14, %rdi
	movq	%rax, %rdx
	movq	(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11AppendInputEPNS0_4ZoneEPS2_@PLT
	movq	0(%r13), %rax
	movq	16(%rax), %rdi
	cmpb	$4, 16(%rdi)
	je	.L4148
	call	_ZN2v88internal8compiler22MachineOperatorBuilder11Word64EqualEv@PLT
	movq	%rax, %rsi
.L3160:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	jmp	.L2927
.L4133:
	movq	(%r12), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%r14, %rdi
	movq	%rax, %rdx
	movq	(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11AppendInputEPNS0_4ZoneEPS2_@PLT
	movq	0(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder11Word32EqualEv@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	jmp	.L2927
.L4120:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector26VisitWord32TruncatingBinopEPNS1_4NodeE
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18SimplifiedLowering9Uint32DivEPNS1_4NodeE
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler22RepresentationSelector16DeferReplacementEPNS1_4NodeES4_
	jmp	.L2927
.L3109:
	movl	$4294967295, %ebx
	movq	%rbx, 16(%rax)
	jmp	.L3111
.L4110:
	movq	32(%r14), %rax
	addq	$16, %rax
	jmp	.L3365
.L4114:
	movb	$0, -1296(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$5, -1292(%rbp)
	movl	$1, %edx
	movl	$1, -1288(%rbp)
	movb	$0, -1284(%rbp)
	movq	$0, -1280(%rbp)
	movl	$-1, -1272(%rbp)
	pushq	-1272(%rbp)
	pushq	-1280(%rbp)
	pushq	-1288(%rbp)
	pushq	-1296(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	0(%r13), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler22RepresentationSelector16DeferReplacementEPNS1_4NodeES4_
	jmp	.L2927
.L4113:
	movb	$0, -1008(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$5, -1004(%rbp)
	movl	$1, %edx
	movl	$1, -1000(%rbp)
	movb	$0, -996(%rbp)
	movq	$0, -992(%rbp)
	movl	$-1, -984(%rbp)
	pushq	-984(%rbp)
	pushq	-992(%rbp)
	pushq	-1000(%rbp)
	pushq	-1008(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	0(%r13), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler22RepresentationSelector16DeferReplacementEPNS1_4NodeES4_
	jmp	.L2927
.L4116:
	movb	$0, -1424(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$5, -1420(%rbp)
	movl	$1, %edx
	movl	$1, -1416(%rbp)
	movb	$0, -1412(%rbp)
	movq	$0, -1408(%rbp)
	movl	$-1, -1400(%rbp)
	pushq	-1400(%rbp)
	pushq	-1408(%rbp)
	pushq	-1416(%rbp)
	pushq	-1424(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	0(%r13), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler22RepresentationSelector16DeferReplacementEPNS1_4NodeES4_
	jmp	.L2927
.L4117:
	movb	$0, -1136(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$5, -1132(%rbp)
	movl	$1, %edx
	movl	$1, -1128(%rbp)
	movb	$0, -1124(%rbp)
	movq	$0, -1120(%rbp)
	movl	$-1, -1112(%rbp)
	pushq	-1112(%rbp)
	pushq	-1120(%rbp)
	pushq	-1128(%rbp)
	pushq	-1136(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	0(%r13), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler22RepresentationSelector16DeferReplacementEPNS1_4NodeES4_
	jmp	.L2927
.L4115:
	movb	$0, -1584(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$5, -1580(%rbp)
	movl	$1, %edx
	movl	$1, -1576(%rbp)
	movb	$0, -1572(%rbp)
	movq	$0, -1568(%rbp)
	movl	$-1, -1560(%rbp)
	pushq	-1560(%rbp)
	pushq	-1568(%rbp)
	pushq	-1576(%rbp)
	pushq	-1584(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	0(%r13), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler22RepresentationSelector16DeferReplacementEPNS1_4NodeES4_
	jmp	.L2927
.L3648:
	movb	$8, -1872(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$5, -1868(%rbp)
	movl	$4, %edx
	movl	$1, -1864(%rbp)
	movb	$0, -1860(%rbp)
	movq	$0, -1856(%rbp)
	movl	$-1, -1848(%rbp)
	pushq	-1848(%rbp)
	pushq	-1856(%rbp)
	pushq	-1864(%rbp)
	pushq	-1872(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder22PlainPrimitiveToWord32Ev@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	jmp	.L2927
.L4139:
	call	_ZN2v88internal8compiler22MachineOperatorBuilder11Word32EqualEv@PLT
	movq	%rax, %rsi
	jmp	.L3549
.L4122:
	movb	$4, -144(%rbp)
	movl	$1031, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$2, -140(%rbp)
	movl	$4, %edx
	movl	$0, -136(%rbp)
	movb	$0, -132(%rbp)
	movq	$0, -128(%rbp)
	movl	$-1, -120(%rbp)
	pushq	-120(%rbp)
	pushq	-128(%rbp)
	pushq	-136(%rbp)
	pushq	-144(%rbp)
	pushq	-120(%rbp)
	pushq	-128(%rbp)
	pushq	-136(%rbp)
	pushq	-144(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE
	addq	$64, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	(%r14), %rax
	movq	128(%r12), %rdi
	movzwl	16(%rax), %esi
	call	_ZN2v88internal8compiler21RepresentationChanger25Uint32OverflowOperatorForENS1_8IrOpcode5ValueE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	jmp	.L2927
.L3650:
	movb	$8, -1776(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$5, -1772(%rbp)
	movl	$8, %edx
	movl	$1, -1768(%rbp)
	movb	$0, -1764(%rbp)
	movq	$0, -1760(%rbp)
	movl	$-1, -1752(%rbp)
	pushq	-1752(%rbp)
	pushq	-1760(%rbp)
	pushq	-1768(%rbp)
	pushq	-1776(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	jmp	.L2927
.L3150:
	movl	$4294967295, %ebx
	movq	%rbx, 16(%rax)
	jmp	.L3152
.L3498:
	movl	-6008(%rbp), %eax
	movb	$13, -144(%rbp)
	movl	$4294967295, %ecx
	movl	$13, %edx
	movb	$4, -140(%rbp)
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, -136(%rbp)
	movb	$0, -132(%rbp)
	movq	$0, -128(%rbp)
	movl	$-1, -120(%rbp)
	pushq	-120(%rbp)
	pushq	-128(%rbp)
	pushq	-136(%rbp)
	pushq	-144(%rbp)
	pushq	-120(%rbp)
	pushq	-128(%rbp)
	pushq	-136(%rbp)
	pushq	-144(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE
	addq	$64, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	cmpq	$3167, -208(%rbp)
	je	.L3502
	leaq	-208(%rbp), %rdi
	movl	$3167, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L3502
.L3504:
	movq	(%r14), %rax
	movq	128(%r12), %rdi
	movzwl	16(%rax), %esi
	call	_ZN2v88internal8compiler21RepresentationChanger18Float64OperatorForENS1_8IrOpcode5ValueE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	jmp	.L2927
.L3153:
	movl	$4294967295, %ebx
	movq	%rbx, 16(%rax)
	jmp	.L2927
.L3637:
	movzbl	23(%r14), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L3642
	addq	$8, %rbx
.L3643:
	movq	(%rbx), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector16DeferReplacementEPNS1_4NodeES4_
	jmp	.L2927
.L3454:
	movl	-6008(%rbp), %eax
	movb	$13, -144(%rbp)
	movl	$4294967295, %ecx
	movl	$13, %edx
	movb	$4, -140(%rbp)
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, -136(%rbp)
	movb	$0, -132(%rbp)
	movq	$0, -128(%rbp)
	movl	$-1, -120(%rbp)
	pushq	-120(%rbp)
	pushq	-128(%rbp)
	pushq	-136(%rbp)
	pushq	-144(%rbp)
	pushq	-120(%rbp)
	pushq	-128(%rbp)
	pushq	-136(%rbp)
	pushq	-144(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE
	addq	$64, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	cmpl	$1, -6008(%rbp)
	sbbq	%rsi, %rsi
	andl	$2048, %esi
	addq	$1119, %rsi
	cmpq	-208(%rbp), %rsi
	je	.L3459
	leaq	-208(%rbp), %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L3459
.L3460:
	movq	(%r14), %rax
	movq	128(%r12), %rdi
	movzwl	16(%rax), %esi
	call	_ZN2v88internal8compiler21RepresentationChanger18Float64OperatorForENS1_8IrOpcode5ValueE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	jmp	.L2927
.L4119:
	movb	$13, -720(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$4, -716(%rbp)
	movl	$13, %edx
	movl	$1, -712(%rbp)
	movb	$0, -708(%rbp)
	movq	$0, -704(%rbp)
	movl	$-1, -696(%rbp)
	pushq	-696(%rbp)
	pushq	-704(%rbp)
	pushq	-712(%rbp)
	pushq	-720(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movzbl	23(%r14), %eax
	movq	32(%r14), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L3707
	leaq	16(%rdx), %rbx
.L3707:
	movq	(%rbx), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector16DeferReplacementEPNS1_4NodeES4_
	jmp	.L2927
.L4108:
	call	__stack_chk_fail@PLT
.L4124:
	leaq	-176(%rbp), %rdx
	movl	$1, %ecx
	movl	%ebx, %esi
	movq	$0, -176(%rbp)
	leaq	-5648(%rbp), %rdi
	movl	$-1, -168(%rbp)
	call	_ZN2v88internal8compiler12_GLOBAL__N_130CheckedUseInfoAsWord32FromHintENS1_19NumberOperationHintERKNS1_14FeedbackSourceENS1_13IdentifyZerosE
	movq	-5648(%rbp), %rax
	movq	-5640(%rbp), %rdx
	movq	-5632(%rbp), %rcx
	movq	-5624(%rbp), %rbx
	movq	%rax, -144(%rbp)
	pushq	%rbx
	movq	%rdx, -136(%rbp)
	pushq	%rcx
	movq	%rcx, -128(%rbp)
	pushq	%rdx
	movq	%rbx, -120(%rbp)
	pushq	%rax
	pushq	%rbx
	pushq	%rcx
	pushq	%rdx
	pushq	%rax
	jmp	.L4082
.L3618:
	movb	$8, -2608(%rbp)
	movl	$2, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$5, -2604(%rbp)
	movl	$1, -2600(%rbp)
	movb	$0, -2596(%rbp)
	movq	$0, -2592(%rbp)
	movl	$-1, -2584(%rbp)
	pushq	-2584(%rbp)
	pushq	-2592(%rbp)
	pushq	-2600(%rbp)
	pushq	-2608(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector12ProcessInputEPNS1_4NodeEiNS1_7UseInfoE
	addq	$32, %rsp
	cmpl	$2, 120(%r12)
	jne	.L3617
	movq	(%r14), %rdi
	call	_ZN2v88internal8compiler18FastMapParameterOfEPKNS1_8OperatorE@PLT
	movq	-144(%rbp), %rdx
	movq	%rax, %rsi
	movq	(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder34TransitionAndStoreNonNumberElementENS0_6HandleINS0_3MapEEENS1_4TypeE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	jmp	.L3617
.L3412:
	movb	$13, -5040(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$4, -5036(%rbp)
	movl	$13, %edx
	movl	$0, -5032(%rbp)
	movb	$0, -5028(%rbp)
	movq	$0, -5024(%rbp)
	movl	$-1, -5016(%rbp)
	pushq	-5016(%rbp)
	pushq	-5024(%rbp)
	pushq	-5032(%rbp)
	pushq	-5040(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movzbl	23(%r14), %eax
	movq	32(%r14), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L3416
	leaq	16(%rdx), %rbx
.L3416:
	movq	(%rbx), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector16DeferReplacementEPNS1_4NodeES4_
	jmp	.L2927
.L3542:
	movb	$4, -4304(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$2, -4300(%rbp)
	movl	$4, %edx
	movl	$0, -4296(%rbp)
	movb	$0, -4292(%rbp)
	movq	$0, -4288(%rbp)
	movl	$-1, -4280(%rbp)
	pushq	-4280(%rbp)
	pushq	-4288(%rbp)
	pushq	-4296(%rbp)
	pushq	-4304(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18SimplifiedLowering24DoSigned32ToUint8ClampedEPNS1_4NodeE
	jmp	.L2927
.L3240:
	leaq	-5744(%rbp), %rdi
	xorl	%ecx, %ecx
	leaq	-176(%rbp), %rdx
	movq	$0, -176(%rbp)
	movl	$-1, -168(%rbp)
	call	_ZN2v88internal8compiler12_GLOBAL__N_130CheckedUseInfoAsWord32FromHintENS1_19NumberOperationHintERKNS1_14FeedbackSourceENS1_13IdentifyZerosE
	movq	-5744(%rbp), %rax
	movq	%r14, %rsi
	movq	-5736(%rbp), %rdx
	movq	-5728(%rbp), %rcx
	movq	-5720(%rbp), %rbx
	movq	%r12, %rdi
	movq	%rdx, -136(%rbp)
	pushq	%rbx
	pushq	%rcx
	pushq	%rdx
	pushq	%rax
	pushq	%rbx
	pushq	%rcx
	pushq	%rdx
	movl	$1, %edx
	pushq	%rax
	movq	%rcx, -128(%rbp)
	movl	$4294967295, %ecx
	movq	%rax, -144(%rbp)
	movq	%rbx, -120(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE
	movq	(%r14), %rax
	addq	$64, %rsp
	movq	128(%r12), %rdi
	movzwl	16(%rax), %esi
	call	_ZN2v88internal8compiler21RepresentationChanger16Int32OperatorForENS1_8IrOpcode5ValueE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler22RepresentationSelector14ChangeToPureOpEPNS1_4NodeEPKNS1_8OperatorE
	jmp	.L2927
.L4109:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18SimplifiedLowering12Float64RoundEPNS1_4NodeE
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler22RepresentationSelector16DeferReplacementEPNS1_4NodeES4_
	jmp	.L2927
.L4146:
	leaq	-5776(%rbp), %rdi
	leaq	-176(%rbp), %rdx
	movq	$0, -176(%rbp)
	movl	$-1, -168(%rbp)
	call	_ZN2v88internal8compiler12_GLOBAL__N_130CheckedUseInfoAsWord32FromHintENS1_19NumberOperationHintERKNS1_14FeedbackSourceENS1_13IdentifyZerosE
	movq	-5768(%rbp), %rdx
	movq	%r14, %rsi
	movq	-5760(%rbp), %rcx
	movq	-5776(%rbp), %rax
	movq	-5752(%rbp), %rbx
	movq	%r12, %rdi
	movq	%rdx, -136(%rbp)
	pushq	%rbx
	pushq	%rcx
	pushq	%rdx
	pushq	%rax
	pushq	%rbx
	pushq	%rcx
	pushq	%rdx
	movl	$1, %edx
	pushq	%rax
	movq	%rcx, -128(%rbp)
	movl	$4294967295, %ecx
	movq	%rax, -144(%rbp)
	movq	%rbx, -120(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE
	addq	$64, %rsp
	jmp	.L2927
.L4123:
	movl	%ebx, %esi
	leaq	-5680(%rbp), %rdi
	movl	$1, %ecx
	movq	$0, -176(%rbp)
	leaq	-176(%rbp), %rdx
	movl	$-1, -168(%rbp)
	call	_ZN2v88internal8compiler12_GLOBAL__N_130CheckedUseInfoAsWord32FromHintENS1_19NumberOperationHintERKNS1_14FeedbackSourceENS1_13IdentifyZerosE
	movq	-5672(%rbp), %rdx
	movq	%r14, %rsi
	movq	-5664(%rbp), %rcx
	movq	-5680(%rbp), %rax
	movq	-5656(%rbp), %rbx
	movq	%r12, %rdi
	movq	%rdx, -136(%rbp)
	pushq	%rbx
	pushq	%rcx
	pushq	%rdx
	pushq	%rax
	pushq	%rbx
	pushq	%rcx
	pushq	%rdx
	movl	$4, %edx
	pushq	%rax
	movq	%rcx, -128(%rbp)
	movl	$4294967295, %ecx
	movq	%rax, -144(%rbp)
	movq	%rbx, -120(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE
	addq	$64, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18SimplifiedLowering8Int32DivEPNS1_4NodeE
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler22RepresentationSelector16DeferReplacementEPNS1_4NodeES4_
	jmp	.L2927
.L3651:
	movb	$8, -1808(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$5, -1804(%rbp)
	movl	$13, %edx
	movl	$1, -1800(%rbp)
	movb	$0, -1796(%rbp)
	movq	$0, -1792(%rbp)
	movl	$-1, -1784(%rbp)
	pushq	-1784(%rbp)
	pushq	-1792(%rbp)
	pushq	-1800(%rbp)
	pushq	-1808(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder23PlainPrimitiveToFloat64Ev@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	jmp	.L2927
.L4148:
	call	_ZN2v88internal8compiler22MachineOperatorBuilder11Word32EqualEv@PLT
	movq	%rax, %rsi
	jmp	.L3160
.L4138:
	movb	$4, -144(%rbp)
	movl	$1099, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$2, -140(%rbp)
	movl	$4, %edx
	movl	$0, -136(%rbp)
	movb	$0, -132(%rbp)
	movq	$0, -128(%rbp)
	movl	$-1, -120(%rbp)
	pushq	-120(%rbp)
	pushq	-128(%rbp)
	pushq	-136(%rbp)
	pushq	-144(%rbp)
	pushq	-120(%rbp)
	pushq	-128(%rbp)
	pushq	-136(%rbp)
	pushq	-144(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector10VisitBinopEPNS1_4NodeENS1_7UseInfoES5_NS0_21MachineRepresentationENS1_4TypeE
	addq	$64, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movl	-6008(%rbp), %edx
	movq	%r15, %r8
	movq	%r13, %rcx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector22LowerToCheckedInt32MulEPNS1_4NodeENS1_10TruncationENS1_4TypeES6_.isra.0
	jmp	.L2927
.L4121:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector26VisitWord32TruncatingBinopEPNS1_4NodeE
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18SimplifiedLowering8Int32DivEPNS1_4NodeE
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler22RepresentationSelector16DeferReplacementEPNS1_4NodeES4_
	jmp	.L2927
.L3642:
	movq	32(%r14), %rax
	leaq	24(%rax), %rbx
	jmp	.L3643
.L3502:
	cmpl	$1, -6008(%rbp)
	sbbq	%rsi, %rsi
	andl	$2048, %esi
	addq	$1119, %rsi
	cmpq	-176(%rbp), %rsi
	je	.L3503
	leaq	-176(%rbp), %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L3504
.L3503:
	movq	0(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder22Float64LessThanOrEqualEv@PLT
	movl	$13, %ecx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler18SimplifiedLowering5DoMinEPNS1_4NodeEPKNS1_8OperatorENS0_21MachineRepresentationE
	jmp	.L2927
.L3459:
	cmpq	$3167, -176(%rbp)
	je	.L3457
	leaq	-176(%rbp), %rdi
	movl	$3167, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L3460
.L3457:
	movq	0(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder15Float64LessThanEv@PLT
	movl	$13, %ecx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler18SimplifiedLowering5DoMaxEPNS1_4NodeEPKNS1_8OperatorENS0_21MachineRepresentationE
	jmp	.L2927
.L3545:
	movb	$13, -4272(%rbp)
	movl	$4294967295, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$4, -4268(%rbp)
	movl	$13, %edx
	movl	$1, -4264(%rbp)
	movb	$0, -4260(%rbp)
	movq	$0, -4256(%rbp)
	movl	$-1, -4248(%rbp)
	pushq	-4248(%rbp)
	pushq	-4256(%rbp)
	pushq	-4264(%rbp)
	pushq	-4272(%rbp)
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitUnopEPNS1_4NodeENS1_7UseInfoENS0_21MachineRepresentationENS1_4TypeE
	addq	$32, %rsp
	cmpl	$2, 120(%r12)
	jne	.L2927
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18SimplifiedLowering23DoIntegerToUint8ClampedEPNS1_4NodeE
	jmp	.L2927
.L4147:
	andl	$16777215, %eax
	imulq	$40, %rax, %rax
	movb	$1, 1(%rdx,%rax)
	jmp	.L2927
	.cfi_endproc
.LFE23059:
	.size	_ZN2v88internal8compiler22RepresentationSelector9VisitNodeEPNS1_4NodeENS1_10TruncationEPNS1_18SimplifiedLoweringE, .-_ZN2v88internal8compiler22RepresentationSelector9VisitNodeEPNS1_4NodeENS1_10TruncationEPNS1_18SimplifiedLoweringE
	.section	.rodata._ZN2v88internal8compiler22RepresentationSelector23RunTypePropagationPhaseEv.str1.1,"aMS",@progbits,1
.LC31:
	.string	"--{Type propagation phase}--\n"
.LC32:
	.string	" visit #%d: %s\n"
.LC33:
	.string	"  ==> output "
	.section	.text._ZN2v88internal8compiler22RepresentationSelector23RunTypePropagationPhaseEv,"axG",@progbits,_ZN2v88internal8compiler22RepresentationSelector23RunTypePropagationPhaseEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler22RepresentationSelector23RunTypePropagationPhaseEv
	.type	_ZN2v88internal8compiler22RepresentationSelector23RunTypePropagationPhaseEv, @function
_ZN2v88internal8compiler22RepresentationSelector23RunTypePropagationPhaseEv:
.LFB22985:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$440, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	jne	.L4218
.L4150:
	movq	40(%r13), %rdx
	movq	32(%r13), %rax
	movl	$1, 120(%r13)
	cmpq	%rax, %rdx
	je	.L4155
	.p2align 4,,10
	.p2align 3
.L4154:
	movb	$0, (%rax)
	addq	$40, %rax
	cmpq	%rax, %rdx
	jne	.L4154
.L4155:
	movq	0(%r13), %rax
	leaq	232(%r13), %r14
	movq	%r14, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	movl	$0, -408(%rbp)
	movq	%rax, -416(%rbp)
	leaq	-416(%rbp), %rax
	movq	%rax, %rsi
	movq	%rax, -440(%rbp)
	call	_ZNSt5dequeIN2v88internal8compiler22RepresentationSelector9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_
	movq	0(%r13), %rax
	movq	32(%r13), %rcx
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm2
	movq	(%rax), %rax
	movq	16(%rax), %rax
	movl	20(%rax), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rax
	leaq	(%rcx,%rax,8), %rax
	movb	$1, (%rax)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	296(%r13), %rcx
	movq	%rax, %xmm4
	punpcklqdq	%xmm4, %xmm2
	movaps	%xmm2, -464(%rbp)
	cmpq	264(%r13), %rcx
	je	.L4153
	movq	%rbx, -432(%rbp)
	movq	%r13, %r15
	movq	%r14, %r13
	.p2align 4,,10
	.p2align 3
.L4152:
	cmpq	%rcx, 304(%r15)
	je	.L4219
.L4157:
	movq	-16(%rcx), %r14
	movslq	-8(%rcx), %rax
	movq	$-32, %rdi
	leaq	32(%r14), %r10
	movq	%rax, %rdx
	subq	%r14, %rdi
	leaq	(%r10,%rax,8), %rsi
.L4163:
	movzbl	23(%r14), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L4158
	cmpl	%edx, %eax
	jle	.L4160
	movq	%rsi, %rax
.L4162:
	movq	(%rax), %r11
	addl	$1, %edx
	addq	$8, %rsi
	movl	20(%r11), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %r8
	movq	32(%r15), %rax
	movl	%edx, -8(%rcx)
	leaq	(%rax,%r8,8), %rax
	cmpb	$0, (%rax)
	jne	.L4163
	movb	$1, (%rax)
	movq	-440(%rbp), %rsi
	movq	%r13, %rdi
	movq	%r11, -416(%rbp)
	movl	$0, -408(%rbp)
	call	_ZNSt5dequeIN2v88internal8compiler22RepresentationSelector9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_
.L4164:
	movq	296(%r15), %rcx
	cmpq	%rcx, 264(%r15)
	jne	.L4152
	movq	-432(%rbp), %rbx
	movq	%r15, %r13
.L4153:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm3
	movq	%rax, %xmm5
	punpcklqdq	%xmm5, %xmm3
	movaps	%xmm3, -464(%rbp)
	jmp	.L4185
	.p2align 4,,10
	.p2align 3
.L4182:
	movb	4(%r14), %bl
	movl	8(%r14), %eax
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	%r13, %rdi
	salq	$32, %rax
	movl	%ebx, %ebx
	orq	%rax, %rbx
	movq	%rbx, %rdx
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitNodeEPNS1_4NodeENS1_10TruncationEPNS1_18SimplifiedLoweringE
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	jne	.L4220
.L4192:
	testb	%r15b, %r15b
	jne	.L4221
.L4185:
	movq	168(%r13), %rax
	cmpq	%rax, 200(%r13)
	je	.L4149
	movq	184(%r13), %rcx
	movq	(%rax), %r12
	leaq	-8(%rcx), %rdx
	cmpq	%rdx, %rax
	je	.L4178
	addq	$8, %rax
	movq	%rax, 168(%r13)
.L4179:
	movl	20(%r12), %eax
	movq	%r12, %rsi
	movq	%r13, %rdi
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rdx
	movq	32(%r13), %rax
	leaq	(%rax,%rdx,8), %r14
	movb	$2, (%r14)
	call	_ZN2v88internal8compiler22RepresentationSelector18UpdateFeedbackTypeEPNS1_4NodeE
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	movl	%eax, %r15d
	je	.L4182
	movq	(%r12), %rax
	movl	20(%r12), %esi
	leaq	.LC32(%rip), %rdi
	movq	8(%rax), %rdx
	andl	$16777215, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L4182
	.p2align 4,,10
	.p2align 3
.L4158:
	movq	(%r10), %rax
	cmpl	%edx, 8(%rax)
	jle	.L4160
	leaq	(%rdi,%rsi), %r8
	leaq	16(%rax,%r8), %rax
	jmp	.L4162
	.p2align 4,,10
	.p2align 3
.L4220:
	xorl	%eax, %eax
	leaq	.LC33(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	je	.L4192
	leaq	-320(%rbp), %r9
	leaq	-400(%rbp), %r10
	movq	%r9, %rdi
	movq	%r9, -432(%rbp)
	movq	%r10, -424(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	-424(%rbp), %r10
	movq	stdout(%rip), %rdx
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%rax, -320(%rbp)
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	xorl	%eax, %eax
	movq	%r10, %rdi
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movw	%ax, -96(%rbp)
	movq	$0, -104(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rcx
	movq	-424(%rbp), %r10
	leaq	40(%rcx), %rdi
	movq	%rcx, -400(%rbp)
	movq	%rdi, -320(%rbp)
	movzbl	1(%r14), %esi
	movq	%r10, %rdi
	call	_ZN2v88internallsERSoNS0_21MachineRepresentationE@PLT
	movdqa	-464(%rbp), %xmm1
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rcx
	leaq	-336(%rbp), %rdi
	movq	%rcx, -320(%rbp)
	movaps	%xmm1, -400(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	-432(%rbp), %r9
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rdi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rdi, -400(%rbp)
	movq	%r9, %rdi
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	je	.L4192
	xorl	%eax, %eax
	leaq	.LC16(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	testb	%r15b, %r15b
	je	.L4185
.L4221:
	movq	24(%r12), %r12
	leaq	136(%r13), %r14
	testq	%r12, %r12
	jne	.L4190
	jmp	.L4185
	.p2align 4,,10
	.p2align 3
.L4188:
	movq	(%r12), %r12
	testq	%r12, %r12
	je	.L4185
.L4190:
	movl	16(%r12), %edx
	movl	%edx, %eax
	shrl	%eax
	andl	$1, %edx
	leaq	3(%rax,%rax,2), %rax
	leaq	(%r12,%rax,8), %rax
	jne	.L4187
	movq	(%rax), %rax
.L4187:
	movq	%rax, -416(%rbp)
	movl	20(%rax), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rdx
	movq	32(%r13), %rax
	leaq	(%rax,%rdx,8), %rax
	cmpb	$2, (%rax)
	jne	.L4188
	movb	$3, (%rax)
	movq	216(%r13), %rax
	movq	200(%r13), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L4189
	movq	-416(%rbp), %rax
	movq	%rax, (%rdx)
	addq	$8, 200(%r13)
	jmp	.L4188
	.p2align 4,,10
	.p2align 3
.L4178:
	movq	144(%r13), %rax
	testq	%rax, %rax
	je	.L4180
	cmpq	$64, 8(%rax)
	ja	.L4181
.L4180:
	movq	176(%r13), %rax
	movq	$64, 8(%rax)
	movq	144(%r13), %rdx
	movq	%rdx, (%rax)
	movq	%rax, 144(%r13)
.L4181:
	movq	192(%r13), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 192(%r13)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 176(%r13)
	movq	%rdx, 184(%r13)
	movq	%rax, 168(%r13)
	jmp	.L4179
	.p2align 4,,10
	.p2align 3
.L4219:
	movq	320(%r15), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L4157
	.p2align 4,,10
	.p2align 3
.L4160:
	movq	296(%r15), %rax
	movq	304(%r15), %rdx
	cmpq	%rdx, %rax
	je	.L4165
	subq	$16, %rax
	movq	%rax, 296(%r15)
.L4166:
	movl	20(%r14), %eax
	movq	%r14, %rsi
	movq	%r15, %rdi
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rdx
	movq	32(%r15), %rax
	leaq	(%rax,%rdx,8), %rbx
	movb	$2, (%rbx)
	call	_ZN2v88internal8compiler22RepresentationSelector18UpdateFeedbackTypeEPNS1_4NodeE
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	movb	%al, -424(%rbp)
	jne	.L4222
.L4169:
	movb	4(%rbx), %r12b
	movl	8(%rbx), %eax
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	%r15, %rdi
	salq	$32, %rax
	movl	%r12d, %r12d
	orq	%rax, %r12
	movq	%r12, %rdx
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitNodeEPNS1_4NodeENS1_10TruncationEPNS1_18SimplifiedLoweringE
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	jne	.L4223
.L4193:
	cmpb	$0, -424(%rbp)
	je	.L4164
	movq	24(%r14), %rbx
	leaq	136(%r15), %r14
	testq	%rbx, %rbx
	jne	.L4172
	jmp	.L4164
	.p2align 4,,10
	.p2align 3
.L4224:
	movb	$3, (%rax)
	movq	216(%r15), %rax
	movq	200(%r15), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L4175
	movq	-416(%rbp), %rax
	movq	%rax, (%rdx)
	addq	$8, 200(%r15)
	.p2align 4,,10
	.p2align 3
.L4174:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L4164
.L4172:
	movl	16(%rbx), %edx
	movl	%edx, %eax
	shrl	%eax
	andl	$1, %edx
	leaq	3(%rax,%rax,2), %rax
	leaq	(%rbx,%rax,8), %rax
	jne	.L4173
	movq	(%rax), %rax
.L4173:
	movq	%rax, -416(%rbp)
	movl	20(%rax), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rdx
	movq	32(%r15), %rax
	leaq	(%rax,%rdx,8), %rax
	cmpb	$2, (%rax)
	jne	.L4174
	jmp	.L4224
	.p2align 4,,10
	.p2align 3
.L4149:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4225
	addq	$440, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4223:
	.cfi_restore_state
	xorl	%eax, %eax
	leaq	.LC33(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	je	.L4193
	leaq	-320(%rbp), %r10
	leaq	-400(%rbp), %r11
	movq	%r10, %rdi
	movq	%r10, -472(%rbp)
	movq	%r11, -448(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	-448(%rbp), %r11
	xorl	%edx, %edx
	pxor	%xmm0, %xmm0
	movw	%dx, -96(%rbp)
	movq	stdout(%rip), %rdx
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movq	%r11, %rdi
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	%rax, -320(%rbp)
	movq	$0, -104(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rcx
	movq	-448(%rbp), %r11
	movq	%rcx, -400(%rbp)
	addq	$40, %rcx
	movq	%rcx, -320(%rbp)
	movzbl	1(%rbx), %esi
	movq	%r11, %rdi
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rbx
	call	_ZN2v88internallsERSoNS0_21MachineRepresentationE@PLT
	movdqa	-464(%rbp), %xmm6
	leaq	-336(%rbp), %rdi
	movq	%rbx, -320(%rbp)
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rbx
	movaps	%xmm6, -400(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	-472(%rbp), %r10
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rbx, -400(%rbp)
	movq	%rax, -320(%rbp)
	movq	%r10, %rdi
	call	_ZNSt8ios_baseD2Ev@PLT
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	je	.L4193
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L4193
	.p2align 4,,10
	.p2align 3
.L4222:
	movq	(%r14), %rax
	movl	20(%r14), %esi
	leaq	.LC32(%rip), %rdi
	movq	8(%rax), %rdx
	andl	$16777215, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L4169
.L4218:
	leaq	.LC31(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L4150
.L4165:
	movq	240(%r15), %rax
	testq	%rax, %rax
	je	.L4167
	cmpq	$32, 8(%rax)
	ja	.L4168
.L4167:
	movq	$32, 8(%rdx)
	movq	240(%r15), %rax
	movq	%rax, (%rdx)
	movq	%rdx, 240(%r15)
.L4168:
	movq	320(%r15), %rax
	leaq	-8(%rax), %rdx
	movq	%rdx, 320(%r15)
	movq	-8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 304(%r15)
	addq	$496, %rax
	movq	%rdx, 312(%r15)
	movq	%rax, 296(%r15)
	jmp	.L4166
.L4189:
	movq	-440(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	jmp	.L4188
.L4175:
	movq	-440(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	jmp	.L4174
.L4225:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22985:
	.size	_ZN2v88internal8compiler22RepresentationSelector23RunTypePropagationPhaseEv, .-_ZN2v88internal8compiler22RepresentationSelector23RunTypePropagationPhaseEv
	.section	.text._ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm,"axG",@progbits,_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	.type	_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm, @function
_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm:
.LFB27276:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$8, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	shrq	$6, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	1(%rsi), %rbx
	addq	$3, %rsi
	subq	$8, %rsp
	cmpq	$8, %rsi
	cmovb	%rax, %rsi
	movq	%rsi, 24(%rdi)
	movq	(%rdi), %rdi
	salq	$3, %rsi
	movq	16(%rdi), %rdx
	movq	24(%rdi), %rax
	subq	%rdx, %rax
	cmpq	%rax, %rsi
	ja	.L4240
	addq	%rdx, %rsi
	movq	%rsi, 16(%rdi)
.L4228:
	movq	24(%r12), %rax
	movq	%rdx, 16(%r12)
	subq	%rbx, %rax
	shrq	%rax
	leaq	(%rdx,%rax,8), %r15
	leaq	(%r15,%rbx,8), %r14
	cmpq	%r14, %r15
	jnb	.L4229
	movq	%r15, %rbx
	jmp	.L4234
	.p2align 4,,10
	.p2align 3
.L4230:
	movq	(%r12), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L4241
	leaq	512(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L4231:
	movq	%rax, (%rbx)
	addq	$8, %rbx
	cmpq	%rbx, %r14
	jbe	.L4229
.L4234:
	movq	8(%r12), %rax
	testq	%rax, %rax
	je	.L4230
	cmpq	$63, 8(%rax)
	jbe	.L4230
	movq	(%rax), %rdx
	addq	$8, %rbx
	movq	%rdx, 8(%r12)
	movq	%rax, -8(%rbx)
	cmpq	%rbx, %r14
	ja	.L4234
.L4229:
	movq	%r15, 56(%r12)
	movq	(%r15), %rdx
	andl	$63, %r13d
	leaq	512(%rdx), %rax
	movq	%rdx, 40(%r12)
	movq	%rax, 48(%r12)
	leaq	-8(%r14), %rax
	movq	%rax, 88(%r12)
	movq	-8(%r14), %rax
	movq	%rdx, 32(%r12)
	movq	%rax, 72(%r12)
	leaq	512(%rax), %rcx
	leaq	(%rax,%r13,8), %rax
	movq	%rcx, 80(%r12)
	movq	%rax, 64(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4241:
	.cfi_restore_state
	movl	$512, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L4231
	.p2align 4,,10
	.p2align 3
.L4240:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L4228
	.cfi_endproc
.LFE27276:
	.size	_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm, .-_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	.section	.text._ZNSt11_Deque_baseIN2v88internal8compiler22RepresentationSelector9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm,"axG",@progbits,_ZNSt11_Deque_baseIN2v88internal8compiler22RepresentationSelector9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt11_Deque_baseIN2v88internal8compiler22RepresentationSelector9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	.type	_ZNSt11_Deque_baseIN2v88internal8compiler22RepresentationSelector9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm, @function
_ZNSt11_Deque_baseIN2v88internal8compiler22RepresentationSelector9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm:
.LFB27292:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$8, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	shrq	$5, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	1(%rsi), %rbx
	addq	$3, %rsi
	subq	$8, %rsp
	cmpq	$8, %rsi
	cmovb	%rax, %rsi
	movq	%rsi, 24(%rdi)
	movq	(%rdi), %rdi
	salq	$3, %rsi
	movq	16(%rdi), %rdx
	movq	24(%rdi), %rax
	subq	%rdx, %rax
	cmpq	%rax, %rsi
	ja	.L4256
	addq	%rdx, %rsi
	movq	%rsi, 16(%rdi)
.L4244:
	movq	24(%r12), %rax
	movq	%rdx, 16(%r12)
	subq	%rbx, %rax
	shrq	%rax
	leaq	(%rdx,%rax,8), %r15
	leaq	(%r15,%rbx,8), %r14
	cmpq	%r14, %r15
	jnb	.L4245
	movq	%r15, %rbx
	jmp	.L4250
	.p2align 4,,10
	.p2align 3
.L4246:
	movq	(%r12), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L4257
	leaq	512(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L4247:
	movq	%rax, (%rbx)
	addq	$8, %rbx
	cmpq	%rbx, %r14
	jbe	.L4245
.L4250:
	movq	8(%r12), %rax
	testq	%rax, %rax
	je	.L4246
	cmpq	$31, 8(%rax)
	jbe	.L4246
	movq	(%rax), %rdx
	addq	$8, %rbx
	movq	%rdx, 8(%r12)
	movq	%rax, -8(%rbx)
	cmpq	%rbx, %r14
	ja	.L4250
.L4245:
	movq	%r15, 56(%r12)
	movq	(%r15), %rdx
	andl	$31, %r13d
	salq	$4, %r13
	leaq	512(%rdx), %rax
	movq	%rdx, 40(%r12)
	movq	%rax, 48(%r12)
	leaq	-8(%r14), %rax
	movq	%rax, 88(%r12)
	movq	-8(%r14), %rax
	movq	%rdx, 32(%r12)
	addq	%rax, %r13
	leaq	512(%rax), %rcx
	movq	%rax, 72(%r12)
	movq	%r13, 64(%r12)
	movq	%rcx, 80(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4257:
	.cfi_restore_state
	movl	$512, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L4247
	.p2align 4,,10
	.p2align 3
.L4256:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L4244
	.cfi_endproc
.LFE27292:
	.size	_ZNSt11_Deque_baseIN2v88internal8compiler22RepresentationSelector9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm, .-_ZNSt11_Deque_baseIN2v88internal8compiler22RepresentationSelector9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	.section	.rodata._ZN2v88internal8compiler18SimplifiedLowering13LowerAllNodesEv.str1.1,"aMS",@progbits,1
.LC35:
	.string	"--{Propagation phase}--\n"
.LC36:
	.string	" visit #%d: %s (trunc: %s)\n"
	.section	.rodata._ZN2v88internal8compiler18SimplifiedLowering13LowerAllNodesEv.str1.8,"aMS",@progbits,1
	.align 8
.LC37:
	.string	"--{Simplified lowering phase}--\n"
	.section	.rodata._ZN2v88internal8compiler18SimplifiedLowering13LowerAllNodesEv.str1.1
.LC38:
	.string	"simplified lowering"
	.section	.text._ZN2v88internal8compiler18SimplifiedLowering13LowerAllNodesEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18SimplifiedLowering13LowerAllNodesEv
	.type	_ZN2v88internal8compiler18SimplifiedLowering13LowerAllNodesEv, @function
_ZN2v88internal8compiler18SimplifiedLowering13LowerAllNodesEv:
.LFB23078:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-752(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$760, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %rdx
	movq	(%rdi), %rsi
	movq	%r14, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler21RepresentationChangerC1EPNS1_7JSGraphEPNS1_12JSHeapBrokerE@PLT
	movq	104(%r12), %rax
	movq	16(%r12), %r15
	movq	%rax, -760(%rbp)
	movq	88(%r12), %rax
	movq	%r15, -520(%rbp)
	movq	%rax, -768(%rbp)
	movq	80(%r12), %rax
	movq	%rax, -776(%rbp)
	movq	8(%r12), %rax
	movq	%rax, -784(%rbp)
	movq	(%r12), %rax
	movq	%rax, -528(%rbp)
	movq	(%rax), %rax
	movl	28(%rax), %eax
	movq	%rax, -512(%rbp)
	cmpq	$53687091, %rax
	ja	.L4368
	leaq	(%rax,%rax,4), %rdx
	xorl	%esi, %esi
	movq	%r15, -504(%rbp)
	movq	$0, -496(%rbp)
	salq	$3, %rdx
	movq	$0, -488(%rbp)
	movq	$0, -480(%rbp)
	testq	%rax, %rax
	je	.L4260
	movq	16(%r15), %rax
	movq	24(%r15), %rdi
	movq	%rdx, %rsi
	subq	%rax, %rdi
	cmpq	%rdi, %rdx
	ja	.L4369
	addq	%rax, %rsi
	movq	%rsi, 16(%r15)
.L4262:
	leaq	(%rax,%rdx), %rsi
	movq	%rax, -496(%rbp)
	movdqa	.LC34(%rip), %xmm0
	movq	%rsi, -480(%rbp)
	.p2align 4,,10
	.p2align 3
.L4263:
	xorl	%ecx, %ecx
	movb	$0, 4(%rax)
	addq	$40, %rax
	movw	%cx, -40(%rax)
	movl	$0, -32(%rax)
	movups	%xmm0, -24(%rax)
	movb	$0, -8(%rax)
	cmpq	%rsi, %rax
	jne	.L4263
.L4260:
	movq	%r14, -400(%rbp)
	leaq	-720(%rbp), %r14
	pxor	%xmm0, %xmm0
	movq	%rsi, -488(%rbp)
	movq	%r14, %rdi
	xorl	%esi, %esi
	movaps	%xmm0, -688(%rbp)
	movaps	%xmm0, -672(%rbp)
	movaps	%xmm0, -656(%rbp)
	movaps	%xmm0, -640(%rbp)
	movq	%r15, -472(%rbp)
	movq	$0, -464(%rbp)
	movq	$0, -456(%rbp)
	movq	$0, -448(%rbp)
	movq	%r15, -440(%rbp)
	movq	$0, -432(%rbp)
	movq	$0, -424(%rbp)
	movq	$0, -416(%rbp)
	movl	$0, -408(%rbp)
	movq	%r15, -720(%rbp)
	movq	$0, -712(%rbp)
	movq	$0, -704(%rbp)
	movq	$0, -696(%rbp)
	call	_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	cmpq	$0, -704(%rbp)
	pxor	%xmm0, %xmm0
	je	.L4370
	leaq	-624(%rbp), %rdi
	xorl	%esi, %esi
	movdqa	-720(%rbp), %xmm5
	movaps	%xmm0, -592(%rbp)
	movaps	%xmm0, -576(%rbp)
	movaps	%xmm5, -624(%rbp)
	movaps	%xmm0, -560(%rbp)
	movaps	%xmm0, -544(%rbp)
	movq	$0, -608(%rbp)
	movq	$0, -600(%rbp)
	call	_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	movdqa	-592(%rbp), %xmm6
	movq	-680(%rbp), %rdi
	movdqa	-576(%rbp), %xmm7
	movq	-712(%rbp), %rax
	movdqa	-560(%rbp), %xmm5
	movq	-664(%rbp), %r11
	movq	-688(%rbp), %xmm3
	movq	-648(%rbp), %r8
	movaps	%xmm6, -688(%rbp)
	movq	-632(%rbp), %rsi
	movq	-704(%rbp), %r10
	movdqa	-544(%rbp), %xmm6
	movq	-696(%rbp), %r9
	movq	-720(%rbp), %xmm4
	movq	-608(%rbp), %rdx
	movq	%r10, -376(%rbp)
	movq	-672(%rbp), %xmm2
	movaps	%xmm7, -672(%rbp)
	movq	-656(%rbp), %xmm1
	movq	%rax, %xmm7
	movq	-640(%rbp), %xmm0
	movaps	%xmm5, -656(%rbp)
	movq	%rdi, %xmm5
	punpcklqdq	%xmm7, %xmm4
	punpcklqdq	%xmm5, %xmm3
	movq	%r8, %xmm7
	movq	%rsi, %xmm5
	movq	-600(%rbp), %rcx
	movaps	%xmm6, -640(%rbp)
	movq	%r11, %xmm6
	punpcklqdq	%xmm7, %xmm1
	punpcklqdq	%xmm5, %xmm0
	punpcklqdq	%xmm6, %xmm2
	movq	%rdx, -704(%rbp)
	movq	%rcx, -696(%rbp)
	movq	%r9, -368(%rbp)
	movups	%xmm4, -392(%rbp)
	movups	%xmm3, -360(%rbp)
	movups	%xmm2, -344(%rbp)
	movups	%xmm1, -328(%rbp)
	movups	%xmm0, -312(%rbp)
	testq	%rdx, %rdx
	je	.L4265
	movq	-632(%rbp), %rdi
	movq	-664(%rbp), %rsi
	addq	$8, %rdi
	cmpq	%rsi, %rdi
	jbe	.L4266
	.p2align 4,,10
	.p2align 3
.L4269:
	testq	%rax, %rax
	je	.L4267
	cmpq	$64, 8(%rax)
	ja	.L4268
.L4267:
	movq	(%rsi), %rax
	movq	$64, 8(%rax)
	movq	-712(%rbp), %rdx
	movq	%rdx, (%rax)
	movq	%rax, -712(%rbp)
.L4268:
	addq	$8, %rsi
	cmpq	%rsi, %rdi
	ja	.L4269
	movq	-696(%rbp), %rcx
	movq	-704(%rbp), %rdx
.L4266:
	leaq	0(,%rcx,8), %rax
	cmpq	$15, %rax
	jbe	.L4265
	movq	%rcx, 8(%rdx)
	movq	$0, (%rdx)
.L4265:
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	%r15, -720(%rbp)
	movaps	%xmm0, -688(%rbp)
	movaps	%xmm0, -672(%rbp)
	movaps	%xmm0, -656(%rbp)
	movaps	%xmm0, -640(%rbp)
	movq	$0, -712(%rbp)
	movq	$0, -704(%rbp)
	movq	$0, -696(%rbp)
	call	_ZNSt11_Deque_baseIN2v88internal8compiler22RepresentationSelector9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	cmpq	$0, -704(%rbp)
	pxor	%xmm0, %xmm0
	je	.L4371
	leaq	-624(%rbp), %rdi
	xorl	%esi, %esi
	movdqa	-720(%rbp), %xmm6
	movaps	%xmm0, -592(%rbp)
	movaps	%xmm0, -576(%rbp)
	movaps	%xmm6, -624(%rbp)
	movaps	%xmm0, -560(%rbp)
	movaps	%xmm0, -544(%rbp)
	movq	$0, -608(%rbp)
	movq	$0, -600(%rbp)
	call	_ZNSt11_Deque_baseIN2v88internal8compiler22RepresentationSelector9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	movq	-680(%rbp), %r9
	movdqa	-560(%rbp), %xmm6
	movq	-688(%rbp), %xmm3
	movq	-664(%rbp), %r8
	movq	-656(%rbp), %xmm1
	movq	-648(%rbp), %rcx
	movaps	%xmm6, -656(%rbp)
	movq	%r9, %xmm6
	movq	-672(%rbp), %xmm2
	punpcklqdq	%xmm6, %xmm3
	movq	-632(%rbp), %rdx
	movups	%xmm3, -264(%rbp)
	movq	%r8, %xmm3
	movdqa	-592(%rbp), %xmm7
	movdqa	-576(%rbp), %xmm5
	punpcklqdq	%xmm3, %xmm2
	movq	-712(%rbp), %rax
	movq	-640(%rbp), %xmm0
	movups	%xmm2, -248(%rbp)
	movq	%rcx, %xmm2
	movq	-720(%rbp), %xmm4
	movq	-704(%rbp), %r11
	punpcklqdq	%xmm2, %xmm1
	movq	-696(%rbp), %r10
	movq	-608(%rbp), %rsi
	movaps	%xmm7, -688(%rbp)
	movq	-600(%rbp), %rdi
	movdqa	-544(%rbp), %xmm7
	movaps	%xmm5, -672(%rbp)
	movq	%rax, %xmm5
	movups	%xmm1, -232(%rbp)
	movq	%rdx, %xmm1
	punpcklqdq	%xmm5, %xmm4
	punpcklqdq	%xmm1, %xmm0
	movq	%rsi, -704(%rbp)
	movq	%rdi, -696(%rbp)
	movq	%r11, -280(%rbp)
	movq	%r10, -272(%rbp)
	movaps	%xmm7, -640(%rbp)
	movups	%xmm4, -296(%rbp)
	movups	%xmm0, -216(%rbp)
	testq	%rsi, %rsi
	je	.L4272
	movq	-632(%rbp), %rcx
	movq	-664(%rbp), %rdx
	addq	$8, %rcx
	cmpq	%rdx, %rcx
	jbe	.L4273
	.p2align 4,,10
	.p2align 3
.L4276:
	testq	%rax, %rax
	je	.L4274
	cmpq	$32, 8(%rax)
	ja	.L4275
.L4274:
	movq	(%rdx), %rax
	movq	$32, 8(%rax)
	movq	-712(%rbp), %rsi
	movq	%rsi, (%rax)
	movq	%rax, -712(%rbp)
.L4275:
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	ja	.L4276
	movq	-696(%rbp), %rdi
	movq	-704(%rbp), %rsi
.L4273:
	leaq	0(,%rdi,8), %rax
	cmpq	$15, %rax
	jbe	.L4272
	movq	%rdi, 8(%rsi)
	movq	$0, (%rsi)
.L4272:
	movq	-776(%rbp), %xmm0
	movhps	-768(%rbp), %xmm0
	movups	%xmm0, -200(%rbp)
	call	_ZN2v88internal8compiler9TypeCache3GetEv@PLT
	movq	-784(%rbp), %rsi
	leaq	-176(%rbp), %rdi
	movq	%rax, -184(%rbp)
	movq	-528(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdx
	call	_ZN2v88internal8compiler14OperationTyperC1EPNS1_12JSHeapBrokerEPNS0_4ZoneE@PLT
	movq	-760(%rbp), %rax
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	movq	%rax, -64(%rbp)
	jne	.L4372
.L4278:
	movq	-528(%rbp), %rax
	movq	-496(%rbp), %rdx
	movl	$0, -408(%rbp)
	movq	(%rax), %rax
	movq	16(%rax), %rax
	movq	%rax, -624(%rbp)
	movl	20(%rax), %eax
	andl	$16777215, %eax
	leaq	(%rax,%rax,4), %rax
	leaq	(%rdx,%rax,8), %rax
	movb	$3, (%rax)
	movq	-456(%rbp), %rsi
	cmpq	-448(%rbp), %rsi
	je	.L4279
	movq	-624(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, -456(%rbp)
.L4280:
	movq	-312(%rbp), %rax
	movq	-328(%rbp), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L4281
	movq	-624(%rbp), %rax
	movq	%rax, (%rdx)
	movq	-328(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, -328(%rbp)
.L4282:
	movq	-360(%rbp), %rax
	leaq	-624(%rbp), %rcx
	leaq	-528(%rbp), %r15
	movq	%rcx, -760(%rbp)
	cmpq	%rdx, %rax
	je	.L4292
	movq	%r12, -768(%rbp)
	movq	%r15, %r12
	.p2align 4,,10
	.p2align 3
.L4283:
	movq	(%rax), %r14
	movq	-344(%rbp), %rdi
	movl	20(%r14), %edx
	andl	$16777215, %edx
	leaq	(%rdx,%rdx,4), %rcx
	movq	-496(%rbp), %rdx
	leaq	(%rdx,%rcx,8), %r15
	leaq	-8(%rdi), %rdx
	cmpq	%rdx, %rax
	je	.L4286
	addq	$8, %rax
	movq	%rax, -360(%rbp)
.L4287:
	movzbl	4(%r15), %edx
	movl	8(%r15), %eax
	movb	$2, (%r15)
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	jne	.L4373
	movb	%dl, %bl
	salq	$32, %rax
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movl	%ebx, %ebx
	movq	%r12, %rdi
	orq	%rax, %rbx
	movq	%rbx, %rdx
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitNodeEPNS1_4NodeENS1_10TruncationEPNS1_18SimplifiedLoweringE
	movq	-360(%rbp), %rax
	cmpq	%rax, -328(%rbp)
	jne	.L4283
.L4367:
	movq	%r12, %r15
	movq	-768(%rbp), %r12
.L4292:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler22RepresentationSelector23RunTypePropagationPhaseEv
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	jne	.L4374
.L4285:
	movl	$2, -408(%rbp)
	movq	-464(%rbp), %r14
	cmpq	-456(%rbp), %r14
	je	.L4300
	.p2align 4,,10
	.p2align 3
.L4301:
	movq	(%r14), %r8
	movl	20(%r8), %esi
	andl	$16777215, %esi
	cmpb	$0, _ZN2v88internal25FLAG_trace_representationE(%rip)
	movl	%esi, %eax
	leaq	(%rax,%rax,4), %rdx
	movq	-496(%rbp), %rax
	leaq	(%rax,%rdx,8), %rcx
	jne	.L4375
.L4296:
	movq	-200(%rbp), %rdi
	movq	%r8, %rsi
	movq	%rcx, -768(%rbp)
	movq	%r8, -760(%rbp)
	call	_ZNK2v88internal8compiler19SourcePositionTable17GetSourcePositionEPNS1_4NodeE@PLT
	movq	-200(%rbp), %r9
	movq	-760(%rbp), %r8
	testb	$1, %al
	movq	-768(%rbp), %rcx
	movq	16(%r9), %rdx
	movq	%rdx, -616(%rbp)
	jne	.L4297
	movq	%rax, %rsi
	movq	%rax, %rdx
	shrq	%rsi
	shrq	$31, %rdx
	andl	$1073741823, %esi
	movzwl	%dx, %edx
	orl	%edx, %esi
	je	.L4298
.L4297:
	movq	%rax, 16(%r9)
.L4298:
	movq	-192(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L4299
	movq	24(%rbx), %rax
	movl	20(%r8), %edx
	movq	%r15, %rdi
	addq	$8, %r14
	movq	48(%rbx), %rsi
	movq	16(%rbx), %xmm0
	movq	%r9, -792(%rbp)
	movq	%rax, -760(%rbp)
	movq	40(%rbx), %rax
	andl	$16777215, %edx
	movq	%rsi, 16(%rbx)
	movl	32(%rbx), %r10d
	movq	%r8, %rsi
	movq	%rax, -768(%rbp)
	leaq	.LC38(%rip), %rax
	movq	%rax, 24(%rbx)
	movl	$1, 32(%rbx)
	movq	%rdx, 40(%rbx)
	movb	4(%rcx), %r13b
	movl	8(%rcx), %edx
	movq	%r12, %rcx
	movl	%r10d, -776(%rbp)
	movq	%xmm0, -784(%rbp)
	movl	%r13d, %r13d
	salq	$32, %rdx
	orq	%rdx, %r13
	movq	%r13, %rdx
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitNodeEPNS1_4NodeENS1_10TruncationEPNS1_18SimplifiedLoweringE
	movq	-768(%rbp), %rax
	movq	-784(%rbp), %xmm0
	movl	-776(%rbp), %r10d
	movq	-792(%rbp), %r9
	movhps	-760(%rbp), %xmm0
	movq	%rax, 40(%rbx)
	movl	%r10d, 32(%rbx)
	movups	%xmm0, 16(%rbx)
	movq	-616(%rbp), %rax
	movq	%rax, 16(%r9)
	cmpq	%r14, -456(%rbp)
	jne	.L4301
.L4300:
	movq	-432(%rbp), %rbx
	cmpq	-424(%rbp), %rbx
	je	.L4295
	.p2align 4,,10
	.p2align 3
.L4294:
	movq	(%rbx), %r12
	movq	8(%rbx), %r13
	addq	$16, %rbx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node11ReplaceUsesEPS2_@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node4KillEv@PLT
	movq	-424(%rbp), %rdx
	cmpq	%rdx, %rbx
	je	.L4295
	movq	%rbx, %rax
	.p2align 4,,10
	.p2align 3
.L4302:
	cmpq	8(%rax), %r12
	je	.L4376
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L4302
	cmpq	%rbx, %rdx
	jne	.L4294
	.p2align 4,,10
	.p2align 3
.L4295:
	movq	-280(%rbp), %rax
	testq	%rax, %rax
	je	.L4306
	movq	-208(%rbp), %rcx
	movq	-240(%rbp), %rdx
	addq	$8, %rcx
	cmpq	%rdx, %rcx
	jbe	.L4307
	movq	-288(%rbp), %rax
	.p2align 4,,10
	.p2align 3
.L4310:
	testq	%rax, %rax
	je	.L4308
	cmpq	$32, 8(%rax)
	ja	.L4309
.L4308:
	movq	(%rdx), %rax
	movq	$32, 8(%rax)
	movq	-288(%rbp), %rsi
	movq	%rsi, (%rax)
	movq	%rax, -288(%rbp)
.L4309:
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	ja	.L4310
	movq	-280(%rbp), %rax
.L4307:
	movq	-272(%rbp), %rdx
	leaq	0(,%rdx,8), %rcx
	cmpq	$15, %rcx
	jbe	.L4306
	movq	%rdx, 8(%rax)
	movq	$0, (%rax)
.L4306:
	movq	-376(%rbp), %rax
	testq	%rax, %rax
	je	.L4258
	movq	-304(%rbp), %rdi
	movq	-336(%rbp), %rdx
	leaq	8(%rdi), %rcx
	cmpq	%rdx, %rcx
	jbe	.L4313
	movq	-384(%rbp), %rax
	.p2align 4,,10
	.p2align 3
.L4316:
	testq	%rax, %rax
	je	.L4314
	cmpq	$64, 8(%rax)
	ja	.L4315
.L4314:
	movq	(%rdx), %rax
	movq	$64, 8(%rax)
	movq	-384(%rbp), %rsi
	movq	%rsi, (%rax)
	movq	%rax, -384(%rbp)
.L4315:
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	ja	.L4316
	movq	-376(%rbp), %rax
.L4313:
	movq	-368(%rbp), %rdx
	leaq	0(,%rdx,8), %rcx
	cmpq	$15, %rcx
	jbe	.L4258
	movq	%rdx, 8(%rax)
	movq	$0, (%rax)
.L4258:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4377
	addq	$760, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4376:
	.cfi_restore_state
	movq	%r13, 8(%rax)
	movq	-424(%rbp), %rdx
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L4302
	cmpq	%rbx, %rdx
	jne	.L4294
	jmp	.L4295
	.p2align 4,,10
	.p2align 3
.L4299:
	movb	4(%rcx), %r13b
	movl	8(%rcx), %eax
	movq	%r8, %rsi
	movq	%r12, %rcx
	movq	%r15, %rdi
	movq	%r9, -760(%rbp)
	addq	$8, %r14
	salq	$32, %rax
	movl	%r13d, %r13d
	orq	%rax, %r13
	movq	%r13, %rdx
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitNodeEPNS1_4NodeENS1_10TruncationEPNS1_18SimplifiedLoweringE
	movq	-616(%rbp), %rax
	movq	-760(%rbp), %r9
	movq	%rax, 16(%r9)
	cmpq	%r14, -456(%rbp)
	jne	.L4301
	jmp	.L4300
	.p2align 4,,10
	.p2align 3
.L4375:
	movq	(%r8), %rax
	leaq	.LC32(%rip), %rdi
	movq	%rcx, -768(%rbp)
	movq	%r8, -760(%rbp)
	movq	8(%rax), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-768(%rbp), %rcx
	movq	-760(%rbp), %r8
	jmp	.L4296
	.p2align 4,,10
	.p2align 3
.L4286:
	movq	-384(%rbp), %rax
	testq	%rax, %rax
	je	.L4288
	cmpq	$64, 8(%rax)
	ja	.L4289
.L4288:
	movq	-352(%rbp), %rax
	movq	$64, 8(%rax)
	movq	-384(%rbp), %rdx
	movq	%rdx, (%rax)
	movq	%rax, -384(%rbp)
.L4289:
	movq	-336(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, -336(%rbp)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, -352(%rbp)
	movq	%rdx, -344(%rbp)
	movq	%rax, -360(%rbp)
	jmp	.L4287
	.p2align 4,,10
	.p2align 3
.L4373:
	movq	-760(%rbp), %rdi
	movb	%dl, -624(%rbp)
	movl	%eax, -620(%rbp)
	call	_ZNK2v88internal8compiler10Truncation11descriptionEv@PLT
	movl	20(%r14), %esi
	leaq	.LC36(%rip), %rdi
	movq	%rax, %rcx
	movq	(%r14), %rax
	andl	$16777215, %esi
	movq	8(%rax), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movb	4(%r15), %bl
	movl	8(%r15), %eax
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	salq	$32, %rax
	movl	%ebx, %ebx
	orq	%rax, %rbx
	movq	%rbx, %rdx
	call	_ZN2v88internal8compiler22RepresentationSelector9VisitNodeEPNS1_4NodeENS1_10TruncationEPNS1_18SimplifiedLoweringE
	movq	-360(%rbp), %rax
	cmpq	-328(%rbp), %rax
	jne	.L4283
	jmp	.L4367
	.p2align 4,,10
	.p2align 3
.L4370:
	movdqa	-720(%rbp), %xmm1
	movq	-696(%rbp), %rax
	movq	$0, -376(%rbp)
	movdqa	-688(%rbp), %xmm2
	movdqa	-672(%rbp), %xmm3
	movdqa	-656(%rbp), %xmm7
	movups	%xmm1, -392(%rbp)
	movdqa	-640(%rbp), %xmm1
	movq	%rax, -368(%rbp)
	movups	%xmm2, -360(%rbp)
	movups	%xmm3, -344(%rbp)
	movups	%xmm7, -328(%rbp)
	movups	%xmm1, -312(%rbp)
	jmp	.L4265
	.p2align 4,,10
	.p2align 3
.L4371:
	movdqa	-720(%rbp), %xmm2
	movq	-696(%rbp), %rax
	movq	$0, -280(%rbp)
	movdqa	-688(%rbp), %xmm3
	movdqa	-672(%rbp), %xmm7
	movdqa	-656(%rbp), %xmm5
	movdqa	-640(%rbp), %xmm6
	movq	%rax, -272(%rbp)
	movups	%xmm2, -296(%rbp)
	movups	%xmm3, -264(%rbp)
	movups	%xmm7, -248(%rbp)
	movups	%xmm5, -232(%rbp)
	movups	%xmm6, -216(%rbp)
	jmp	.L4272
	.p2align 4,,10
	.p2align 3
.L4374:
	leaq	.LC37(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L4285
	.p2align 4,,10
	.p2align 3
.L4372:
	leaq	.LC35(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L4278
	.p2align 4,,10
	.p2align 3
.L4369:
	movq	%r15, %rdi
	movq	%rdx, -792(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-792(%rbp), %rdx
	jmp	.L4262
	.p2align 4,,10
	.p2align 3
.L4279:
	leaq	-624(%rbp), %rdx
	leaq	-472(%rbp), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L4280
	.p2align 4,,10
	.p2align 3
.L4281:
	leaq	-624(%rbp), %rsi
	leaq	-392(%rbp), %rdi
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	movq	-328(%rbp), %rdx
	jmp	.L4282
.L4377:
	call	__stack_chk_fail@PLT
.L4368:
	leaq	.LC29(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE23078:
	.size	_ZN2v88internal8compiler18SimplifiedLowering13LowerAllNodesEv, .-_ZN2v88internal8compiler18SimplifiedLowering13LowerAllNodesEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler18SimplifiedLoweringC2EPNS1_7JSGraphEPNS1_12JSHeapBrokerEPNS0_4ZoneEPNS1_19SourcePositionTableEPNS1_15NodeOriginTableENS0_24PoisoningMitigationLevelEPNS0_11TickCounterE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler18SimplifiedLoweringC2EPNS1_7JSGraphEPNS1_12JSHeapBrokerEPNS0_4ZoneEPNS1_19SourcePositionTableEPNS1_15NodeOriginTableENS0_24PoisoningMitigationLevelEPNS0_11TickCounterE, @function
_GLOBAL__sub_I__ZN2v88internal8compiler18SimplifiedLoweringC2EPNS1_7JSGraphEPNS1_12JSHeapBrokerEPNS0_4ZoneEPNS1_19SourcePositionTableEPNS1_15NodeOriginTableENS0_24PoisoningMitigationLevelEPNS0_11TickCounterE:
.LFB28345:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE28345:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler18SimplifiedLoweringC2EPNS1_7JSGraphEPNS1_12JSHeapBrokerEPNS0_4ZoneEPNS1_19SourcePositionTableEPNS1_15NodeOriginTableENS0_24PoisoningMitigationLevelEPNS0_11TickCounterE, .-_GLOBAL__sub_I__ZN2v88internal8compiler18SimplifiedLoweringC2EPNS1_7JSGraphEPNS1_12JSHeapBrokerEPNS0_4ZoneEPNS1_19SourcePositionTableEPNS1_15NodeOriginTableENS0_24PoisoningMitigationLevelEPNS0_11TickCounterE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler18SimplifiedLoweringC2EPNS1_7JSGraphEPNS1_12JSHeapBrokerEPNS0_4ZoneEPNS1_19SourcePositionTableEPNS1_15NodeOriginTableENS0_24PoisoningMitigationLevelEPNS0_11TickCounterE
	.hidden	_ZTCN2v88internal12StdoutStreamE0_So
	.weak	_ZTCN2v88internal12StdoutStreamE0_So
	.section	.rodata._ZTCN2v88internal12StdoutStreamE0_So,"aG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTCN2v88internal12StdoutStreamE0_So, @object
	.size	_ZTCN2v88internal12StdoutStreamE0_So, 80
_ZTCN2v88internal12StdoutStreamE0_So:
	.quad	80
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-80
	.quad	-80
	.quad	0
	.quad	0
	.quad	0
	.hidden	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE
	.weak	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE
	.section	.rodata._ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE,"aG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE, @object
	.size	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE, 80
_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE:
	.quad	80
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-80
	.quad	-80
	.quad	0
	.quad	0
	.quad	0
	.weak	_ZTTN2v88internal12StdoutStreamE
	.section	.data.rel.ro.local._ZTTN2v88internal12StdoutStreamE,"awG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTTN2v88internal12StdoutStreamE, @object
	.size	_ZTTN2v88internal12StdoutStreamE, 48
_ZTTN2v88internal12StdoutStreamE:
	.quad	_ZTVN2v88internal12StdoutStreamE+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_So+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_So+64
	.quad	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE+64
	.quad	_ZTVN2v88internal12StdoutStreamE+64
	.weak	_ZTVN2v88internal12StdoutStreamE
	.section	.data.rel.ro.local._ZTVN2v88internal12StdoutStreamE,"awG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTVN2v88internal12StdoutStreamE, @object
	.size	_ZTVN2v88internal12StdoutStreamE, 80
_ZTVN2v88internal12StdoutStreamE:
	.quad	80
	.quad	0
	.quad	0
	.quad	_ZN2v88internal12StdoutStreamD1Ev
	.quad	_ZN2v88internal12StdoutStreamD0Ev
	.quad	-80
	.quad	-80
	.quad	0
	.quad	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.quad	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.weak	_ZTVN2v88internal23CallInterfaceDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal23CallInterfaceDescriptorE,"awG",@progbits,_ZTVN2v88internal23CallInterfaceDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal23CallInterfaceDescriptorE, @object
	.size	_ZTVN2v88internal23CallInterfaceDescriptorE, 48
_ZTVN2v88internal23CallInterfaceDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC2:
	.long	0
	.long	0
	.align 8
.LC11:
	.long	0
	.long	-1042284544
	.align 8
.LC12:
	.long	4290772992
	.long	1105199103
	.align 8
.LC17:
	.long	0
	.long	1072693248
	.align 8
.LC18:
	.long	0
	.long	1071644672
	.align 8
.LC19:
	.long	0
	.long	-1074790400
	.align 8
.LC20:
	.long	0
	.long	1081073664
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC34:
	.quad	4294967295
	.quad	0
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
